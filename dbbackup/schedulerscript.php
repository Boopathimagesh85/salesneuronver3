<?php

include_once(dirname(dirname(__FILE__)).'/'.'leadlicence.php');
error_reporting(0);
$finaljeweldb = '';
$export_db=$CONF["sql_db"];
$con=mysqli_connect($CONF["sql_host"],$CONF["sql_user"],$CONF["sql_passwd"])or die(mysqli_error($con));
mysqli_select_db($con,$export_db)or die(mysqli_error($con));
{
	//get all databases list
	$i=0;
	$mdbqy = mysqli_query($con,'select userplaninfoid,databasenameinfo from userplaninfo where activationstatus="Yes" AND status=1 group by databasenameinfo') or die(mysqli_error($con));
	while($datas = mysqli_fetch_array($mdbqy,MYSQLI_ASSOC)) {
		$cdbname[$i] = $datas['databasenameinfo'];
		$i++;
	}
	foreach($cdbname as $dbname) {
		$finaljeweldb = $dbname;
		$chkdb = mysqli_query($con,'SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = "'.$dbname.'" ');
		$count = mysqli_num_rows($chkdb);
		if($count>=1 ) {
			mysqli_select_db($con,$dbname) or die(mysqli_error($con));
			$query = mysqli_query($con,"select * from backupsetting ORDER BY backupsettingid DESC limit 1") or die('retrieving information failed');
			$result=mysqli_fetch_array($query,MYSQLI_ASSOC);
		}
	}
}

$dir=strtoupper($result['backuplocation']);
$dir=trim($dir);

//Create a temporary file (backup folder, file name in backup)
chmod('create.bat', 0777);
$fh = fopen('create.bat','w');
$l='mkdir '.$dir.'\backup';
fwrite($fh,$l);
fclose($fh);
$fh1 = fopen('del.bat','w');
$l1='RD /S /Q '.$dir.'\backup';
fwrite($fh1,$l1);
fclose($fh1);

//for zip
$stpath=$dir."\\";
$exppath=$dir."\\backup\\";
$dbbppwd="bethechangekalam";
$_PMBP_EXPORT_DIR="backup\\";

if(basename($_SERVER['SCRIPT_FILENAME'])=="schedulerscript.php") {
	$prepath=$dir."\\";
} else {
	$prepath="";
}

define('PMBP_EXPORT_DIR',$prepath.$_PMBP_EXPORT_DIR);

@umask(0000);
@mkdir($_PMBP_EXPORT_DIR,0777);
$files = glob($prepath.$_PMBP_EXPORT_DIR.'*'); // get all file names
foreach($files as $file) { // iterate files
  if(is_file($file)) {
	unlink($file); // delete all the fields in the folder
  }
}
$local_timezone = 'Asia/Calcutta';
date_default_timezone_set($local_timezone);
define('BD_DATE_FORMAT',"%x %X");
$time=time();
$mailstatus=-1;
$select['date']=array(BD_DATE_FORMAT=>strftime(BD_DATE_FORMAT,$time),
        "%x %X"=>strftime("%x %X",$time),"%x %H:%M"=>strftime("%x %H:%M",$time),
        "%m/%d/%y %X"=>strftime("%m/%d/%y %X",$time),"%m/%d/%y %H:%M"=>strftime("%m/%d/%y %H:%M",$time),
        "%b %d %Y %X"=>strftime("%b %d %Y %X",$time),"%b %d %Y %H:%M"=>strftime("%b %d %Y %H:%M",$time),
        "%Y/%m/%d %X"=>strftime("%Y/%m/%d %X",$time),"%Y/%m/%d %H:%M"=>strftime("%Y/%m/%d %H:%M",$time));

$options = array(
'db_host'=> $CONF["sql_host"],  //mysql host
'db_uname' => $CONF["sql_user"],  //user
'db_password' => $CONF["sql_passwd"], //pass
'db_to_backup' => $finaljeweldb, //database name
'db_backup_path' => $exppath, //where to backup
'db_exclude_tables' => array() //tables to exclude
);

//database back script start here
if($result['schedulerenable'] == 'Yes' && $finaljeweldb != '') {
	$return=-1;
	exec('c:\WINDOWS\system32\cmd.exe START /B /C create.bat /q',$out,$return);
	$backup = backup_mysql_database($options);
	// set backupfile name
	$time = time();
	$date = date('Y-m-d', time());
	$timesave = date('h-i-s', time());
	$local_time = date("Y-m-d_h-i-s-A");
	$backupfilename = $finaljeweldb."_".$local_time.".sql";
	$execute_path = $exppath.'\\'.$backupfilename;
	file_put_contents($execute_path, $backup);
	if($backup!="DB_ERROR" && $backup!="") {
		$date = date('Y-m-d', time());
		$time = date('h:i:s', time());
		$createdate=date('Y-m-d h:i:s', time());
		$userid = $result['createuserid'];
		$status = $result['status'];
		//ZIP ARCHIVE
		$encryptedfilename = $finaljeweldb."_".$local_time."";
		$encrypted = $dir."\\".$encryptedfilename;
		$password = "bethechangekalam"; //ZIP PASSWORD
		$zip = new ZipArchive();
		if($zip->open($encrypted.".zip", ZipArchive::CREATE) === TRUE) {
			######Password generation openssl############
			$strong = "bethechangekalam"; //setup acc
			$cbc_size = 16; //highest 16bytes 128bits aes enc
			$secret = openssl_random_pseudo_bytes($cbc_size, $strong);
			##########Get a hex-encoded representation of the key####
			$binarysecret = bin2hex($secret); //THIS SAVE IN db
			$baseName = basename($encrypted);
			$opensslpath = "C:\Users\kumaresan\Downloads\openssl-0.9.8k_X64\bin\openssl.exe";
			$command = "openssl enc -aes-256-cbc -salt -pass pass:$secret -p -in $execute_path -out $encrypted";
			$outt = shell_exec($command);
			//var_dump($outt);print_r($outt); To see the value
			if (!$zip->setPassword($password)) {
				print_r('set password failed');
				throw new RuntimeException('Set password failed');
			}
			// Add files to the zip file
			$zip->addFile($execute_path,$backupfilename);
			$zip->close();
		}
		$backupname = $finaljeweldb."_".$local_time.".zip";
		//insert in to db
		$bytes = filesize($prepath.$backupname);
		$file_size=formatSizeUnits($bytes);
		$qy="INSERT INTO backup (`type`, `name`, `time`, `date`, `size`,`industryid`, `createuserid`, `lastupdateuserid`, `createdate`, `lastupdatedate`, `status`) VALUES (1,'".$backupname."','".$time."', '".$date."','".$file_size."','".$result['industryid']."',".$userid.",".$userid.",'".$createdate."','".$createdate."','".$status."')";
		mysqli_query($con,$qy)or die(mysql_error());
		
		$return1=-1;
		@unlink($prepath.'backup/'.$backup);
		exec('c:\WINDOWS\system32\cmd.exe START /B /C del.bat /q',$outt,$return1);
		echo 'Success';
	} else {
		@unlink($prepath.'backup/'.$backup);
		//echo $backup;
	}
}
//database back script end here

//conversion of file size
function formatSizeUnits($bytes) {
	if ($bytes >= 1073741824) {
		$bytes = number_format($bytes / 1073741824, 2) . ' GB';
	} elseif ($bytes >= 1048576) {
		$bytes = number_format($bytes / 1048576, 2) . ' MB';
	} elseif ($bytes >= 1024) {
		$bytes = number_format($bytes / 1024, 2) . ' KB';
	} elseif ($bytes > 1) {
		$bytes = $bytes . ' bytes';
	} elseif ($bytes == 1) {
		$bytes = $bytes . ' byte';
	} else {
		$bytes = '0 byte';
	}
	return $bytes;
}

//Retrieve database and dump into a file (output is encrypted code)
function backup_mysql_database($options) {
	$mtables = array();
	$contents = "-- Database: `".$options['db_to_backup']."` --\n";
	$mysqli = new mysqli($options['db_host'], $options['db_uname'], $options['db_password'], $options['db_to_backup']);
	if ($mysqli->connect_error)  {
		die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
	}
	//For fetching complete Tables
	$results = $mysqli->query("SHOW TABLES");
	while($row = $results->fetch_all()) {
		if (!in_array($row, $options['db_exclude_tables'])) {
			$mtables = $row;
		}
	}
	foreach($mtables as $table1) {
		foreach($table1 as $table) {
			$contents .= "-- Table `".$table."` --\n";
			$contents .="\n";
			$results = $mysqli->query("SHOW CREATE TABLE ".$table) or die($mysqli->error);
			while($row = $results->fetch_assoc()) {
				$contents .= $row['Create Table']."; \n\n";  
			}
			$results = $mysqli->query("SELECT * FROM ".$table);
			$row_count = $results->num_rows;
			$fields = $results->fetch_fields();
			$fields_count = count($fields);
			$insert_head = "INSERT INTO `".$table."` (";
			for($i=0; $i < $fields_count; $i++) {
				$insert_head  .= "`".$fields[$i]->name."`";
				if($i < $fields_count-1) {
					$insert_head  .= ', ';
				}
			}
			$insert_head .=  ")";
			$insert_head .= " VALUES\n";  
			if(($row_count>0) && ($row_count!=0)) {
				$r = 0;
				while($row = $results->fetch_array()) {
					if(($r % 400)  == 0) {
						$contents .= $insert_head;
					}
					$contents .= "(";
					for($i=0; $i < $fields_count; $i++) {
						if(!empty($row[$i])) {
							$row_content =  str_replace("\n","\\n",$mysqli->real_escape_string($row[$i]));
						} else if($row[$i] == '0') {
							$row_content ='0';
						} else{
							$row_content ='';
						}
						switch($fields[$i]->type) {
							case 8: case 3:
							$contents .=  $row_content;
							break;
							default:
							$contents .= "'". $row_content ."'";
						}
						if($i < $fields_count-1) {
							$contents  .= ', ';
						}
					}
					if(($r+1) == $row_count || ($r % 400) == 399) {  //for fast script execution/performance divided 
						$contents .= ");\n\n";
					} else {
						$contents .= "),\n";
					}
					$r++;
				}
			}
		}
	}
	// return $contents; //for testing without gzip compress
	$fileContents=gzcompress($contents);
	return $fileContents;
}

?>