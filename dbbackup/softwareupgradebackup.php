<?php

$internet_check = is_connected();
if($internet_check == true) {
	echo "SUCCESS";
	die();
	{// Session Start to take backup 
		session_start();
		$appfolder = str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
		$appfolder = explode('/',$appfolder); //explode(DIRECTORY_SEPARATOR,$appfolder); 
		$foldername = $_SERVER['DOCUMENT_ROOT'].'/'.$appfolder[1].'/snsessionlog/';
		$cisess_cookie = $_COOKIE['sn_ci_session'];
		$filepath = $foldername.'sn_ci_session'.$cisess_cookie;
		$fp = fopen($filepath,'r+b')or die('file does not exit');
		$data = fread($fp,filesize($filepath));
		session_decode($data);
		$data = $_SESSION;
		$industryid = $data['industryids']['industryid'];
		session_destroy();
	}
	
	$snsourcefoldername = '';
	$snsourcefolderlocname = '';
	$snfolderbackupname = '';
	$snmandatoryfolderbackup = '';
	$snnewversionlocfilename = '';
	$snnewversionfilename = '';
	$snsourcefoldername = $_SERVER['DOCUMENT_ROOT'];
	$snsourcefolderlocname = $_SERVER['DOCUMENT_ROOT'].'/'.$appfolder[1];
	$errorlogfile = $_SERVER['DOCUMENT_ROOT'].'/'.$appfolder[1].'/dbschema/error_log.txt';
	$latestversionname = '';
	
	// Require Files to be added if any needed
	require_once('../leadlicence.php');
	$all_masterdb = $CONF["sql_db"];
	
	require_once('mailer/class.phpmailer.php');
	require_once("config_data.php");
	require_once("dbdump.php");
	include('../SFTP/Net/SFTP.php');
	
	
	// Latest Version details
	$con = mysqli_connect($CONF["sql_host"],$CONF["sql_user"],$CONF["sql_passwd"])or die(mysqli_error($con));
	mysqli_select_db($con,'all_releasedetails')or die(mysqli_error($con));
	$query_version = " SELECT versionno FROM latestversion WHERE companyid = ".$_POST['compid']." ORDER BY latestversionid DESC LIMIT 1 ";
	$result_data = mysqli_query($con,$query_version)or die(mysqli_error($con));
	$result_version = mysqli_fetch_array($result_data,MYSQLI_ASSOC);
	$latestversionname = $result_version['versionno'];
	
	// Connecting to jeweldb and retrieve the backup location from Company table
	$export_db = $CONF["sql_db"];
	$con = mysqli_connect($CONF["sql_host"],$CONF["sql_user"],$CONF["sql_passwd"])or die(mysqli_error($con));
	mysqli_select_db($con,$export_db)or die(mysqli_error($con));
	$q = " SELECT softwareupgradefolder FROM company WHERE companyid = ".$_POST['compid']." ORDER BY companyid DESC LIMIT 1 ";
	$res = mysqli_query($con,$q)or die(mysqli_error($con));
	$result = mysqli_fetch_array($res,MYSQLI_ASSOC);
	$dir = strtoupper($_SERVER['DOCUMENT_ROOT'].'/'.$result['softwareupgradefolder']);
	$dir = trim($dir);
	
	//Create a temporary file (backup folder, file name in backup)
	chmod('create.bat', 0777);
	$fh = fopen('create.bat','w');
	$l='mkdir '.$dir.'\backup';
	fwrite($fh,$l);
	fclose($fh);
	$fh1 = fopen('del.bat','w');
	$l1='RD /S /Q '.$dir.'\backup';
	fwrite($fh1,$l1);
	fclose($fh1);

	//for zip
	$stpath = $dir."\\";
	$exppath = $dir."\\backup\\";
	$dbbppwd = "bethechangekalam";
	$_PMBP_EXPORT_DIR = "backup\\";

	if(basename($_SERVER['SCRIPT_FILENAME']) == "softwareupgradebackup.php") {
		$prepath = $dir."\\";
	} else {
		$prepath = "";
	}
	
	define('PMBP_EXPORT_DIR',$prepath.$_PMBP_EXPORT_DIR);

	@umask(0000);
	@mkdir($_PMBP_EXPORT_DIR,0777);
	$files = glob($prepath.$_PMBP_EXPORT_DIR.'*'); // get all file names
	foreach($files as $file) { // iterate files
		if(is_file($file)) {
			unlink($file); // delete all the fields in the folder
		}
	}
	$local_timezone = 'Asia/Calcutta';
	date_default_timezone_set($local_timezone);
	define('BD_DATE_FORMAT',"%x %X");
	$time=time();
	$mailstatus=-1;
	$select['date']=array(BD_DATE_FORMAT=>strftime(BD_DATE_FORMAT,$time),
        "%x %X"=>strftime("%x %X",$time),"%x %H:%M"=>strftime("%x %H:%M",$time),
        "%m/%d/%y %X"=>strftime("%m/%d/%y %X",$time),"%m/%d/%y %H:%M"=>strftime("%m/%d/%y %H:%M",$time),
        "%b %d %Y %X"=>strftime("%b %d %Y %X",$time),"%b %d %Y %H:%M"=>strftime("%b %d %Y %H:%M",$time),
        "%Y/%m/%d %X"=>strftime("%Y/%m/%d %X",$time),"%Y/%m/%d %H:%M"=>strftime("%Y/%m/%d %H:%M",$time));

	$options = array(
		'db_host'=> $CONF["sql_host"],  //mysql host
		'db_uname' => $CONF["sql_user"],  //user
		'db_password' => $CONF["sql_passwd"], //pass
		'db_to_backup' => $export_db, //database name
		'db_backup_path' => $exppath, //where to backup
		'db_exclude_tables' => array() //tables to exclude
	);
	
	// Backup of Two Database
	$backupdb = array($all_masterdb,$export_db);
	foreach($backupdb as $dbvalue) {
		//database back script start here
		if(isset($_POST['zip']) && isset($_POST['backupuser'])) {
			$return=-1;
			exec('c:\WINDOWS\system32\cmd.exe START /B /C create.bat /q',$out,$return);
			$backup = backup_mysql_database($options);
			// set backupfile name
			$time = time();
			$date = date('Y-m-d', time());
			$timesave = date('h-i-s', time());
			$local_time = date("Y-m-d_h-i-s-A");
			$backupfilename = $dbvalue."_".$local_time.".sql";
			$execute_path = $exppath.'\\'.$backupfilename;
			file_put_contents($execute_path, $backup);
			if($backup != "DB_ERROR" && $backup != "") {
				$date = date('Y-m-d', time());
				$time = date('h:i:s', time());
				$createdate=date('Y-m-d h:i:s', time());
				$userid = $_POST['backupuser'];
				
				//ZIP ARCHIVE
				$encryptedfilename = $dbvalue."_".$local_time."";
				$encrypted = $dir."\\".$encryptedfilename;
				$password = "bethechangekalam"; //ZIP PASSWORD
				$zip = new ZipArchive();
				if($zip->open($encrypted.".zip", ZipArchive::CREATE) === TRUE) {
					
					######Password generation openssl############
					$strong = "bethechangekalam"; //setup acc
					$cbc_size = 16; //highest 16bytes 128bits aes enc
					$secret = openssl_random_pseudo_bytes($cbc_size, $strong);
					
					##########Get a hex-encoded representation of the key####
					$binarysecret = bin2hex($secret); //THIS SAVE IN db
					$baseName = basename($encrypted);
					$command = "openssl enc -aes-256-cbc -salt -pass pass:$secret -p -in $execute_path -out $encrypted";
					$outt = shell_exec($command);
					//var_dump($outt);print_r($outt); To see the value
					
					if (!$zip->setPassword($password)) {
						print_r('set password failed');
						throw new RuntimeException('Set password failed');
					}
					
					// Add files to the zip file
					$zip->addFile($execute_path,$backupfilename);
					$zip->close();
				}
				$backupname = $dbvalue."_".$local_time.".zip";
				
				$return1=-1;
				@unlink($prepath.'backup/'.$backup);
				exec('c:\WINDOWS\system32\cmd.exe START /B /C del.bat /q',$out,$return1);
				//echo 'Success';
			} else {
				@unlink($prepath.'backup/'.$backup);
				//echo $backup;
			}
		}
	}
	//database back script end here

	// Folder Backup scripts starts here
	if($snsourcefolderlocname != '') {
		// set backupfile name
		$time = time();
		$date = date('Y-m-d', time());
		$timesave = date('h-i-s', time());
		$local_time = date("Y-m-d_h-i-s-A");
		
		// Source and Destination Files to be copy from one place to another
		$snfolderbackupname = $stpath.$appfolder[1].'_'.$local_time;
		$snmandatoryfolderbackup = $stpath.$appfolder[1].'_custfiles_'.$local_time;
		
		$finalcustomcopy = custom_copy($snsourcefolderlocname, $snfolderbackupname);
		
		// Backup billshtml, printtemplate, termscondition, uploads. If any folder to be backup pls add here
		if($finalcustomcopy == 'SUCCESS') {
			
			// Make the destination directory if not exist
			@mkdir($snmandatoryfolderbackup);
			
			@mkdir($snmandatoryfolderbackup.'/billshtml');
			custom_copy($snsourcefolderlocname.'/billshtml', $snmandatoryfolderbackup.'/billshtml');
			@mkdir($snmandatoryfolderbackup.'/printtemplate');
			custom_copy($snsourcefolderlocname.'/printtemplate', $snmandatoryfolderbackup.'/printtemplate');
			@mkdir($snmandatoryfolderbackup.'/termscondition');
			custom_copy($snsourcefolderlocname.'/termscondition', $snmandatoryfolderbackup.'/termscondition');
			@mkdir($snmandatoryfolderbackup.'/uploads');
			custom_copy($snsourcefolderlocname.'/uploads', $snmandatoryfolderbackup.'/uploads');
			
		}
	}
	// Folder Backup scripts ends here
	
	// FTP Connection Code Starts Here
	{
		// set up basic connection FTP
		$sftp = new Net_SFTP('139.162.6.63');
		if (!$sftp->login('root', 'Bakery2020%')) {
			exit('Login Failed');
		}
		
		$new_verion_file_name = '';
		$zip = new ZipArchive;
		$zipfileloc_name = $snsourcefoldername."\\"."salesneuronver".$latestversionname.".rar";
		if($zip->open($zipfileloc_name, ZipArchive::CREATE) === TRUE) {
			$sftp->pwd();
			$get_path = $sftp->pwd();
			$remotepath = '/var/www/html/data_html/salesneuron/snbuild';
			$list = $sftp->nlist($remotepath);
			if($list === false) {
				die("Error listing directory ".$remotepath);
			}
			$prefix = 'salesneuronver'.$latestversionname;
			$matches = preg_grep("/^$prefix.*/i", $list);
			if (count($matches) != 1) {
				die("No file or more than one file matches the pattern: ".implode(",", $matches));
			}
			$matches = array_values($matches);
			$filename = $matches[0];
			$filepath = $remotepath."/".$filename;
			$new_verion_file_name = $filename;
			if (!$sftp->get($filepath, $filename)) {
				die("Error downloading file ".$filepath);
			}
			$sftp->get($filepath, $zipfileloc_name);
			$zip->close();
		}
		{ // UNRAR files of FTP Downloaded file.
		
			// Specify location where the rar file is
			$snnewversionlocfilename = $snsourcefoldername.'/'.$latestversionname;
			$snnewversionfilename = $snsourcefoldername.'/'.$filename;
			
			$archive = RarArchive::open($snnewversionfilename);
			$entries = $archive->getEntries();
			foreach ($entries as $entry) {
				$entry->extract($snsourcefoldername);
			}
			$archive->close();
		}
		{ // COPY PASTE those 4 mandatory customer folder & files.
		
			@mkdir($snnewversionlocfilename.'/billshtml');
			custom_copy($snsourcefolderlocname.'/billshtml', $snnewversionlocfilename.'/billshtml');
			@mkdir($snnewversionlocfilename.'/printtemplate');
			custom_copy($snsourcefolderlocname.'/printtemplate', $snnewversionlocfilename.'/printtemplate');
			@mkdir($snnewversionlocfilename.'/termscondition');
			custom_copy($snsourcefolderlocname.'/termscondition', $snnewversionlocfilename.'/termscondition');
			@mkdir($snnewversionlocfilename.'/uploads');
			custom_copy($snsourcefolderlocname.'/uploads', $snnewversionlocfilename.'/uploads');
			
		}
	}
	// FTP Connection Code Ends Here


	// DB Scheme Sync According to the new masterdb and jeweldb
	/*** DB Scheme Codes Start Here ***/
	
	error_reporting(E_ALL); // Report all errors
	ini_set('memory_limit', '1024M');//can remove if setup in php.ini
	ini_set('max_execution_time', 600); //can remove if setup in php.ini
	
	$dbname = array('all_masterdb','jeweldb');
	$all_masterdb = $_SERVER['DOCUMENT_ROOT'].'/'.$appfolder[1].'/dbschema/dbschema_masterdb.sql';
	$all_jeweldb = $_SERVER['DOCUMENT_ROOT'].'/'.$appfolder[1].'/dbschema/dbschema_jeweldb.sql';	
	file_put_contents($errorlogfile, "");
	
	// Insert Error Log Details
	function errorlogdetails($templine,$errorlogfile) {
		$content = file_get_contents($errorlogfile);
		$content .= PHP_EOL . $templine;
		file_put_contents($errorlogfile, $content);
	}	
	
	// Jeweldb & Masterdb Update Query
	foreach($dbname as $sqldatabase) {
		// Name of the file
		if($sqldatabase == 'all_masterdb') {
			$filename = $all_masterdb;
		} else if($sqldatabase == 'jeweldb') {
			$filename = $all_jeweldb;
		}
		
		$filesize = filesize($filename);
		if($filesize > 0) {
			// MySQL host
			$mysql_host = $CONF["sql_host"];
			// MySQL username
			$mysql_username = $CONF["sql_user"];
			// MySQL password
			$mysql_password = $CONF["sql_passwd"];
			// Database name
			$mysql_database = $sqldatabase;

			// Connect to MySQL server
			//mysqli_connect($mysql_host, $mysql_username, $mysql_password) or die('Error connecting to MySQL server: ' . mysql_error());
			$con = mysqli_connect($mysql_host, $mysql_username, $mysql_password)or die(mysqli_error($con));
			
			// Select database
			//mysqli_select_db($mysql_database) or die('Error selecting MySQL database: ' . mysql_error());
			mysqli_select_db($con,$mysql_database)or die(mysqli_error($con));
			
			// Temporary variable, used to store current query
			$templine = '';
			
			// Read in entire file
			$lines = file($filename);
			
			// Loop through each line
			foreach ($lines as $line) {
			
				// Skip it if it's a comment
				if (substr($line, 0, 2) == '--' || $line == '')
					continue;

				// Add this line to the current segment
				$templine .= $line;
				// If it has a semicolon at the end, it's the end of the query
				if (substr(trim($line), -1, 1) == ';') {
					
					// Perform the query
					//mysqli_query($con, $templine) or print('Error performing query \'<strong>' . $templine . '\': ' . mysqli_error() . '<br /><br />');
					mysqli_query($con, $templine) or errorlogdetails($templine,$errorlogfile);
					// Reset temp variable to empty
					$templine = '';
				}
			}
		}
	}
	/*** DB Scheme Codes Ends Here ***/
	
	$errorlogfilesize = filesize($errorlogfile);
	if($errorlogfilesize > 0)  {
		echo "ERROR IN QUERY";
	} else {
		echo "SUCCESS";
	}
	
	//Retrieve database and dump into a file (output is encrypted code)
	function backup_mysql_database($options) {
		$mtables = array();
		$contents = "-- Database: `".$options['db_to_backup']."` --\n";
		$mysqli = new mysqli($options['db_host'], $options['db_uname'], $options['db_password'], $options['db_to_backup']);
		if ($mysqli->connect_error)  {
			die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
		}
		//For fetching complete Tables
		$results = $mysqli->query("SHOW TABLES");
		while($row = $results->fetch_all()) {
			if (!in_array($row, $options['db_exclude_tables'])) {
				$mtables = $row;
			}
		}
		foreach($mtables as $table1) {
			foreach($table1 as $table) {
				$contents .= "-- Table `".$table."` --\n";
				$contents .="\n";
				$results = $mysqli->query("SHOW CREATE TABLE ".$table) or die($mysqli->error);
				while($row = $results->fetch_assoc()) {
					$contents .= $row['Create Table']."; \n\n";  
				}
				$results = $mysqli->query("SELECT * FROM ".$table);
				$row_count = $results->num_rows;
				$fields = $results->fetch_fields();
				$fields_count = count($fields);
				$insert_head = "INSERT INTO `".$table."` (";
				for($i=0; $i < $fields_count; $i++) {
					$insert_head  .= "`".$fields[$i]->name."`";
					if($i < $fields_count-1) {
						$insert_head  .= ', ';
					}
				}
				$insert_head .=  ")";
				$insert_head .= " VALUES\n";  
				if(($row_count>0) && ($row_count!=0)) {
					$r = 0;
					while($row = $results->fetch_array()) {
						if(($r % 400)  == 0) {
							$contents .= $insert_head;
						}
						$contents .= "(";
						for($i=0; $i < $fields_count; $i++) {
							if(!empty($row[$i])) {
								$row_content =  str_replace("\n","\\n",$mysqli->real_escape_string($row[$i]));
							} else if($row[$i] == '0') {
								$row_content ='0';
							} else{
								$row_content ='';
							}
							switch($fields[$i]->type) {
								case 8: case 3:
								$contents .=  $row_content;
								break;
								default:
								$contents .= "'". $row_content ."'";
							}
							if($i < $fields_count-1) {
								$contents  .= ', ';
							}
						}
						if(($r+1) == $row_count || ($r % 400) == 399) {  //for fast script execution/performance divided 
							$contents .= ");\n\n";
						} else {
							$contents .= "),\n";
						}
						$r++;
					}
				}
			}
		}
		// return $contents; //for testing without gzip compress
		$fileContents=gzcompress($contents);
		return $fileContents;
	}

	#########DECRYPTION SCRIPT########
	if(isset($_GET['decryptionid'])) {
		$stpath = $stpath;
		$files = glob($prepath.$_PMBP_EXPORT_DIR.'*.sql'); // get all file names
		foreach($files as $file) { // iterate files
		  if(is_file($file))
			unlink($file); // delete file
		}
		$id=$_GET['decryptionid'];
		$userid=$_GET['updateuserid'];
		$backupid=explode(',',$id);
		if ($con=PMBP_mysql_connect()) {
			mysqli_select_db($con,$export_db);
			$updatedate=date('Y-m-d h:i:s', time());
			foreach($backupid as $dataid) {
				$qy="select `name` from  backup  where `backupid`=".$dataid;
				$ex_qy=mysqli_query($con,$qy)or die(mysql_error());
				$res=mysqli_fetch_array($ex_qy,MYSQLI_ASSOC)or die(mysql_error());
				$unzipfilename = $res['name'];
				$sqlfile = str_replace('.zip', '.sql', $unzipfilename);
				$zip = zip_open($stpath.$res['name']);
				if ($zip) {
					while ($zip_entry = zip_read($zip)) {
						$fp = fopen($stpath.zip_entry_name($zip_entry), "w");
						if (zip_entry_open($zip, $zip_entry, "r")) {
							$buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
							fwrite($fp,"$buf");
							zip_entry_close($zip_entry);
							fclose($fp);
						}
					}
					zip_close($zip);
				}
				$strong = "bethechangekalam"; //setup acc
				$binarysecret = bin2hex($strong);
				//decrypt the .snr file into a readable sql file
				$command1="openssl aes-256-cbc -d -in $unzipfilename -out $sqlfile -k $binarysecret";
				//execute it
				$output1 = shell_exec($command1);
				//ungzip compression now
				$getcontents=file_get_contents($stpath.$sqlfile);
				$sqlnewdecryptedfile= $stpath."decryptedfile.sql";
				//Clear the contents in file
				$f = @fopen($sqlnewdecryptedfile, "r+");
				if ($f !== false) {
					ftruncate($f, 0);
					fclose($f);
				}
				file_put_contents($sqlnewdecryptedfile, gzuncompress($getcontents));
				echo 'decrypted';
			}
		} else {
			echo 'failed';
		}
	}
	
	// Folder Copy from Source to Destination
	function custom_copy($src, $dst) {
		// open the source directory
		$dir = opendir($src);
		
		// Make the destination directory if not exist
		@mkdir($dst);
		
		// Loop through the files in source directory 
		while($file = readdir($dir)) {
			if(($file != '.' ) && ($file != '..' )) {
				if(is_dir($src . '/' . $file)) { 
					// Recursively calling custom copy function
					// for sub directory 
					custom_copy($src . '/' . $file, $dst . '/' . $file);
				} else {
					copy($src . '/' . $file, $dst . '/' . $file);
				}
			}
		}
		closedir($dir);
		return 'SUCCESS';
	}
} else {
	echo 'NO INTERNET';
}

// Find out Internet is connected or not
function is_connected() {
    $connected = @fsockopen("www.google.com", 80);
	
	//website, port  (try 80 or 443)
    if ($connected){
        $is_conn = true; //action when connected
        fclose($connected);
    } else {
        $is_conn = false; //action in connection failure
    }
    return $is_conn;
}

?>