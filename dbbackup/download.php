<?php
require_once("config_data.php");
$export_db=$CONF["sql_db"];
$con=mysqli_connect($CONF["sql_host"],$CONF["sql_user"],$CONF["sql_passwd"]) or die(mysql_error());
mysqli_select_db($con,$export_db) or die(mysql_error());
//fetch backup location
$q="select * from backupsetting ORDER BY backupsettingid DESC limit 1";
$res=mysqli_query($con,$q)or die(mysqli_error($con));
$result=mysqli_fetch_array($res,MYSQLI_ASSOC);
$dir=strtoupper($result['backuplocation']);
$dir=trim($dir);
if(basename($_SERVER['SCRIPT_FILENAME'])=="download.php") {
	//$prepath=dirname($GLOBALS['_SERVER']['SCRIPT_FILENAME'])."/".$_PMBP_EXPORT_DIR;
	//$prepath=$dir.":\\vardaanbackup\\";
	$prepath=$dir."\\";
} else {
	$prepath="";
}
$files = glob($prepath.'vardaanbackupdownload_*.*'); // get all file names
foreach($files as $file) { // iterate files
  if(is_file($file))
	unlink($file); // delete file
}
if(isset($_POST['downloadid'])) {
	$id=$_POST['downloadid'];
	$backupid=explode(',',$id);
	$file_names = array();
	if ($con=PMBP_mysql_connect()) {
		mysqli_select_db($con,$export_db);
		foreach($backupid as $dataid) {
			$qy="select `name` from `backup`  where `backupid`=".$dataid;
			$ex_qy=mysqli_query($con,$qy)or die(mysql_error());
			while($res=mysqli_fetch_array($ex_qy,MYSQLI_ASSOC)) {
				$file_names[] = $res['name'];
			}
		}
		$local_timezone = 'Asia/Calcutta';
		date_default_timezone_set($local_timezone);
		$date = date('Y-m-d', time());
		$timesave = date('h-i-s', time());
		$archive_file_name="vardaanbackupdownload_"."_".$date."_".$timesave.".zip";
		$file_path=$prepath;
		$a=zipFilesAndDownload($file_names,$archive_file_name,$file_path);
		downloadzipfile($file_path,$a);
	}
}
function PMBP_mysql_connect() {
	global $CONF;
	$res = mysqli_connect($CONF['sql_host'],$CONF['sql_user'],$CONF['sql_passwd']);
	return $res;
}
function zipFilesAndDownload($file_names,$archive_file_name,$file_path)
{
	$zip = new ZipArchive();
	//create the file and throw the error if unsuccessful
	if ($zip->open($file_path.$archive_file_name, ZIPARCHIVE::CREATE )!==TRUE) {
    	exit("cannot open <$archive_file_name>\n");
	}
	//add each files of $file_name array to archive
	foreach($file_names as $files) {
  		$zip->addFile($file_path.$files,$files);
	}
	$zip->close();
	return $archive_file_name;
}
function downloadzipfile($file_path,$fname) {
	# send the file to the browser as a download
    header('Content-disposition: attachment; filename='.$fname.'');
    header('Content-type: application/zip');
    readfile($file_path.$fname);
}
?>