<?php
// used variables
require_once("config_data.php");
$_PMBP_EXPORT_DIR="restore/";
if(basename($GLOBALS['_SERVER']['SCRIPT_FILENAME'])=="backup.php")
{
	//$prepath=dirname($GLOBALS['_SERVER']['SCRIPT_FILENAME'])."/";
	$prepath="C:/backup/";
} 
else
{
	$prepath="";
} 
define('PMBP_EXPORT_DIR',$prepath.$_PMBP_EXPORT_DIR);
define('BI_BROKEN_ZIP',"The ZIP file seems to be broken");
define('IM_ERROR',"%d error(s) occured. You can use 'empty database' to be sure the database does not contain any tables.");
define('BI_WRONG_FILE',"Stopped at line %s.<br>The current query includes more than %s dump lines. That happens if your backup file was created
by some tool which didn't place a semicolon followed by a linebreak at the end of each query, or if your backup file contains extended inserts.");
define('F_MAIL_3',"coudn't be read");
define('F_MAIL_4',"MySQL backup from");
define('F_MAIL_5',"Mail coudn't be sent");
define('F_MAIL_6',"Files succesfully sent by email to");
define('IM_SUCCESS',"Successfully imported");
define('PMBP_EX_NO_AVAILABLE',"Database %s is not available");
if (!isset($_GET['import'])) $_GET['import']=FALSE;
if (!isset($_GET['imp_ALL'])) $_GET['imp_ALL']=FALSE;
if (!isset($_GET['del'])) $_GET['del']=FALSE;
if (!isset($_GET['del_ALL'])) $_GET['del_ALL']=FALSE;
if (!isset($_GET['empty_ALL'])) $_GET['empty_ALL']=FALSE;
if (!isset($_GET['del_all'])) $_GET['del_all']=FALSE;
if (!isset($_GET['empty_all'])) $_GET['empty_all']=FALSE;
$import_db=$CONF['sql_db'];

// if first use or no db-connection possible

// if importing sql
if ($_GET['import'])
{
    if ($_GET['import'])
	{
		$id=$_GET['import'];
		$backupid=explode(',',$id);
		if ($con=PMBP_mysql_connect())
		{
			@mysql_select_db($import_db);
			foreach($backupid as $dataid)
			{
				$qy="select `Name` from `vardaan`.`backup`  where `BackupId`=".$dataid;
				$ex_qy=mysql_query($qy)or die(mysql_error());
				$res=mysql_fetch_array($ex_qy)or die(mysql_error());
				$import_files[]=$res['Name'];
			}
		}
		else
		{
			echo "Connection Failure";
		}
    }
    if (isset($import_files))
	{
		$error="";
        // set php timelimit
        @set_time_limit($CONF['timelimit']);
        @ignore_user_abort(TRUE);
		$qy="select `Size`,`Name` from `backup`  where `BackupId`=".$dataid;
		$ex_qy=mysql_query($qy)or die(mysql_error());
		$res=mysql_fetch_array($ex_qy)or die(mysql_error());
		$file_size=$res['Size'];
		$backup=$res['Name'];
        // import each file
        foreach($import_files as $import_file)
		{
		    // check if a db with the name of the backup file still exists and select it
           //$dbname=PMBP_file_info("db",PMBP_EXPORT_DIR.$import_file);
		   ///$db=@mysql_select_db($dbname=PMBP_file_info("db",PMBP_EXPORT_DIR.$import_file));
			
		    $db=@mysql_select_db($import_db);
            if (!$db)
			{
            	//echo PMBP_addOutput(sprintf(PMBP_EX_NO_AVAILABLE,$dbname),"red");
				echo sprintf(PMBP_EX_NO_AVAILABLE,$db_select);
            }
			else
			{
            	// extract zip file
			    if (PMBP_file_info("comp",PMBP_EXPORT_DIR.$import_file)=="zip")
				{
					include_once("pclzip.lib.php");
					$pclzip = new PclZip(PMBP_EXPORT_DIR.$import_file);
					$extracted_file=$pclzip->extractByIndex(0,PMBP_EXPORT_DIR,"");
					if ($pclzip->error_code!=0) 
						$error="plczip: ".$pclzip->error_string."<br>".BI_BROKEN_ZIP."!";
					$import_file=substr($import_file,0,strlen($import_file)-4);
					unset($pclzip);
			    }
				// execute sql queries
				if (!$error)
				{
					if ($file=@gzopen(PMBP_EXPORT_DIR.$import_file,"r")) 
					{
						$succ=extract(PMBP_exec_sql($file,$con),EXTR_OVERWRITE);
					}
					else 
						$error=F_MAIL_3;
					if ($file) 
						@gzclose($file);
					if (file_exists(PMBP_EXPORT_DIR.$import_file.".zip")) 
						unlink(PMBP_EXPORT_DIR.$import_file);
					$import_file.=".zip";
				}
				else
				{
					return $error;
				}
				if($error)
				{
					//echo PMBP_addOutput($import_file.": ".$error,"red_left");
					echo $error;
				}
			}
        }
		$date = date('Y-m-d', time());
		$time = date('h:i:s', time());
		$createdate=date('Y-m-d h:i:s', time());
		$userid=$_GET['updateuserid'];
		$status=$_GET['status'];
		$log_qy="INSERT INTO `vardaan`.`transactionlog` (`TransactionLogId`, `CreateDate`, `LastUpdateUserId`, `Status`) VALUES (NULL,'".$createdate."','".$userid."', '".$status."')";
		$st=mysql_query($log_qy)or die(mysql_error());
		$logid=0;
		if($st==1)
		{
			$maxid=mysql_query("select MAX(TransactionLogId) as maxlogid from transactionlog")or die(mysql_error());
			$rs=mysql_fetch_array($maxid)or die(mysql_error());
			$logid=$rs['maxlogid'];
			$qy="INSERT INTO `vardaan`.`restore`(`RestoreId`,`Name`,`Time`, `Date`,`Size`,`TransactionLogId`,`Status`)VALUES(NULL,'".$backup."','".$time."', '".$date."','".$file_size."','".$logid."','".$status."')";
			$sta=mysql_query($qy)or die(mysql_error());
			if($sta==1)
			{
				echo IM_SUCCESS;
			}
			else
			{
				echo $error;
			}
		}
    }

}

// function to execute the sql queries provided by the file handler $file
// $file can be a gzopen() or open() handler, $con is the database connection
// $linespersession says how many lines should be executed; if false, all lines will be executed
function PMBP_exec_sql($file,$con,$linespersession=false,$noFile=false)
{
    $query="";
    $queries=0;
    $error="";
    if (isset($_GET["totalqueries"])) $totalqueries=$_GET["totalqueries"]; else $totalqueries=0;
    if (isset($_GET["start"])) $linenumber=$_GET["start"]; else $linenumber=$_GET['start']=0;
    if (!$linespersession) $_GET['start']=1;
    $inparents=false;
    $querylines=0;

    // $tableQueries and $insertQueries only count this session
    $tableQueries=0;
    $insertQueries=0;

    // stop if a query is longer than 300 lines long
    $max_query_lines=100000000;

    // lines starting with these strings are comments and will be ignored
	$comment[0]="#";
	$comment[1]="-- ";

    while (($linenumber<$_GET["start"]+$linespersession || $query!="") && ($dumpline=gzgets($file,65536)))               
    {
    	// increment $_GET['start'] when $linespersession was not set
    	// so all lines of $file will be exeuted at once
    	if (!$linespersession) $_GET['start']++;
    	  
        // handle DOS and Mac encoded linebreaks
        $dumpline=preg_replace("/\r\n$/","\n",$dumpline);
        $dumpline=preg_replace("/\r$/","\n",$dumpline);

        // skip comments and blank lines only if NOT in parents    
        if (!$inparents) {
            $skipline=false;
            foreach ($comment as $comment_value) {
                if (!$inparents && (trim($dumpline)=="" || strpos ($dumpline,$comment_value)===0)) {
                    $skipline=true;
                    break;
                }
            }
            if ($skipline) {
                $linenumber++;
                continue;
            }
        }

        // remove double back-slashes from the dumpline prior to count the quotes ('\\' can only be within strings)  
        $dumpline_deslashed=str_replace("\\\\","",$dumpline);

        // count ' and \' in the dumpline to avoid query break within a text field ending by ;
        // please don't use double quotes ('"')to surround strings, it wont work
        $parents=substr_count($dumpline_deslashed,"'")-substr_count($dumpline_deslashed,"\\'");
        if ($parents%2!=0) $inparents=!$inparents;

        // add the line to query
        $query.=$dumpline;

        // don't count the line if in parents (text fields may include unlimited linebreaks)  
        if (!$inparents) $querylines++;
          
        // stop if query contains more lines as defined by $max_query_lines    
        if ($querylines>$max_query_lines) {
            $error=sprintf(BI_WRONG_FILE."\n",$linenumber,$max_query_lines);
            break;
        }

        // execute query if end of query detected (; as last character) AND NOT in parents
        if (preg_match("/;$/",trim($dumpline)) && !$inparents)
		{
            if (!mysql_query(trim($query),$con))
			{
                $error=SQ_ERROR." ".($linenumber+1)."<br>".nl2br(htmlentities(trim($query)))."\n<br>".htmlentities(mysql_error());
                break;
            }
            if (strtolower(substr(trim($query),0,6))=="insert") 
				$tableQueries++;
			elseif (strtolower(substr(trim($query),0,12))=="create table") 
				$insertQueries++; 
				$totalqueries++;
				$queries++;
				$query="";
				$querylines=0;
        }            
        $linenumber++;
    }
    return array("queries"=>$queries,"totalqueries"=>$totalqueries,"linenumber"=>$linenumber,"error"=>$error,"tableQueries"=>$tableQueries,"insertQueries"=>$insertQueries);
}

// in dependency on $mode different modes can be selected (see below)
function PMBP_file_info($mode,$path)
{
    $filename=preg_replace("#.*/#","",$path);
    $parts=explode(".",$filename);

    switch($mode) {
    
        // returns the name of the database a $path backup file belongs to
        case "db":
            return $parts[0];

        // returns the creation timestamp $path backup file
        case "time":
            return $parts[1];
        
        // returns "gz" if $path backup file is gziped
        case "gzip":
            if (isset($parts[3])) if ($parts[3]=="gz") return $parts[3];
        break;
        
        // returns "zip" if $path backup file is ziped
        case "zip":
            if (isset($parts[3])) if ($parts[3]=="zip") return $parts[3];
        break;
        
        // returns type of compression of $path backup file or no
        case "comp":
            if (PMBP_file_info("gzip",$path)) return "gzip"; elseif (PMBP_file_info("zip",$path)) return "zip"; else return F_NO;

        // returns the size of $path backup file
        case "size":
            return filesize($path);

        // returns yes if the backup file contains 'drop table if exists' or no if not
        case "drop":
            while ($line=PMBP_getln($path)) {
                $line=trim($line);
                if (strtolower(substr($line,0,20))=="drop table if exists"){
                	PMBP_getln($path,true);
                	return F_YES;
                } else {
                	$drop=F_NO;
                } 
            }
            PMBP_getln($path,true);
            return $drop;        
        
        // returns yes if the $path backup files contains tables or no if not
        case "tables":
            while ($line=PMBP_getln($path)) {
                $line=trim($line);
                if (strtolower(substr($line,0,12))=="create table"){
                	PMBP_getln($path,true);
                	return F_YES;
                } else {
                	$table=F_NO;
                } 
            }
            PMBP_getln($path,true);
            return $table;

        // returns yes if the $path backup files contains data or no if not
        case "data":
            while ($line=PMBP_getln($path)) {
                $line=trim($line);
                if (strtolower(substr($line,0,6))=="insert") {
                	PMBP_getln($path,true);
                	return F_YES;
                } else {
                	$data=F_NO;
                }
            }
            PMBP_getln($path,true);
            return $data;
        
        // returns the comment stored to the backup file
        case "comment":
            while ($line=PMBP_getln($path)) {
                $line=trim($line);
                if (isset($comment) && substr($line,0,1)=="#") {
                	$comment.=substr($line,2)."<br>";
                } elseif(isset($comment) && substr($line,0,1)!="#") {
                	PMBP_getln($path,true);
                	return $comment;
                }
                if ($line=="# comment:") $comment=FALSE;
            }
            PMBP_getln($path,true);
            if (isset($comment)) return $comment; else return FALSE;
    }
}
// returns the content of the [gziped] $path backup file line by line
function PMBP_getln($path, $close=false, $org_path=false)
{
    if (!isset($GLOBALS['lnFile'])) $GLOBALS['lnFile']=null;
    if (!$org_path) $org_path=$path; else $org_path=PMBP_EXPORT_DIR.$org_path;
        
    // gz file
    if(PMBP_file_info("gzip",$org_path)=="gz") {            
    	if (!$close) {
	        if ($GLOBALS['lnFile']==null) {
	            $GLOBALS['lnFile']=gzopen($path, "r");
	        }

	        if (!gzeof($GLOBALS['lnFile'])) {
	           return gzgets($GLOBALS['lnFile']);
	        } else {
	            $close=true;
	        }
    	}

        if ($close) {
			// remove the file handler
			@gzclose($GLOBALS['lnFile']);
            $GLOBALS['lnFile']=null;
            return null;
        }

    // zip file
    } elseif(PMBP_file_info("zip",$org_path)=="zip"){
		if (!$close) {
			if ($GLOBALS['lnFile']==null) {
				// try to guess the filename of the packed file
				// known problem: ZIP file xyz.sql.zip contains file abc.sql which already exists with different content! 
				if(!file_exists(substr($org_path,0,strlen($org_path)-4)))
				{
					// extract the file
					include_once("pclzip.lib.php");
					$pclzip = new PclZip($path);
					$extracted_file=$pclzip->extract(PMBP_EXPORT_DIR,"");
			        if ($pclzip->error_code!=0)
					{
			        	// print pclzip error message
			        	//echo PMBP_addOutput("pclzip: ".$pclzip->error_string."<br>".BI_BROKEN_ZIP."!","red");
						echo BI_BROKEN_ZIP;
				        return false;
			        } 
					else
					{
						unset($pclzip);
			        }
				}
			}

			// read the extracted file
			echo $line=PMBP_getln(substr($org_path,0,strlen($org_path)-4));
			if ($line==null) $close=true;
				else return $line;
    	}

		// remove the temporary file
    	if ($close) {
    		@fclose($GLOBALS['lnFile']);
    		$GLOBALS['lnFile']=null;
    		@unlink(substr($org_path,0,strlen($org_path)-4));
    		return null;
    	}
    // sql file
    } 
	else 
	{
		if (!$close) {
	        if ($GLOBALS['lnFile']==null) {
	            $GLOBALS['lnFile']=fopen($path, "r");
	        }
	        
	        if (!feof($GLOBALS['lnFile'])) {
	           return fgets($GLOBALS['lnFile']);
	        } else {
	            $close=true;
	        }
		}
		if ($close) {
			// remove the file handler
			@fclose($GLOBALS['lnFile']);
            $GLOBALS['lnFile']=null;
            return null;
        }
    }
}
function PMBP_mysql_connect() {
	global $CONF;
	$res = @mysql_connect($CONF['sql_host'],$CONF['sql_user'],$CONF['sql_passwd']);
	return $res;
}
?>