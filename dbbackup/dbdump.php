<?php
function PMBP_dump($db,$tables,$data,$drop,$zip,$comment)
{
    global $CONF;
    global $PMBP_SYS_VAR;
    $error=FALSE;
    $local_timezone = 'Asia/Calcutta';
	date_default_timezone_set($local_timezone);
    // set max string size before writing to file
    if (@ini_get("memory_limit")) {
    	$max_size=500000*substr(ini_get("memory_limit"), 0, -1);
    } else {
    	ini_set("memory_limit",$PMBP_SYS_VAR['memory_limit']/1024000);
    	$max_size=500000*($PMBP_SYS_VAR['memory_limit']/1024000);
    }
    // set backupfile name
    $time=time();
	$date = date('Y-m-d', time());
	$timesave = date('h-i-s', time());
	$local_time = date("Y-m-d_h-i-s-A");
    if ($zip=="") {
		//$backupfile=$db."_".$date."_".$timesave.".sql.gz";
		$backupfile=$db."_".$local_time.".sql.gz";
	} else {
		//$backupfile=$db."_".$date."_".$timesave.".sql";
		$backupfile=$db."_".$local_time.".sql";
	}
    $backupfile=PMBP_EXPORT_DIR.$backupfile;

    if ($con=PMBP_mysql_connect()) {
		$time_info=strftime($CONF['date'],$time);
        //create comment
        $out="# MySQL dump of database '".$db."' on host '".$CONF['sql_host']."'\n";
        $out.="# backup date and time: ".$time_info."\n";
        // write users comment
        if ($comment) {
            $out.="# comment:\n";
            $comment=preg_replace("'\n'","\n# ","# ".$comment);
            foreach(explode("\n",$comment) as $line)
				$out.=$line."\n";
				$out.="\n";
        }
        // set and log character set
		$characterSet = PMBP_set_character_set($con);
		$out.="### used character set: " . $characterSet . " ###\n";
		$out.="set names " . $characterSet . ";\n\n";
        // print "use database" if more than one database is available
        // if (count(PMBP_get_db_list())>1)
		// {
            // $out.="CREATE DATABASE IF NOT EXISTS `".$db."`;\n\n";
            // $out.="USE `".$db."`;\n";
        // }
        
        // select db
        mysqli_select_db($con,$db);
        // get auto_increment values and names of all tables
        $res=mysqli_query($con,"show table status")or die(mysqli_error($con));
        $all_tables=array();
        $all_views=array();
        while($row=mysqli_fetch_array($res,MYSQLI_ASSOC)) {
        	if($row['Comment']=='VIEW' && sizeof($row['Engine'])==0) {
				$all_views[]=$row;        		
        	} else {
				$all_tables[]=$row;	
        	}
        }
        // get table structures
        foreach ($all_tables as $table) {
            $res1=mysqli_query($con,"SHOW CREATE TABLE `".$table['Name']."`");
            $tmp=mysqli_fetch_array($res1,MYSQLI_ASSOC);
            $table_sql[$table['Name']]=$tmp["Create Table"];
        }
        // find foreign keys
        $fks=array();
        if (isset($table_sql)) {
            foreach($table_sql as $tablenme=>$table) {
                $tmp_table=$table;
                // save all tables, needed for creating this table in $fks
                while (($ref_pos=strpos($tmp_table," REFERENCES "))>0) {
                    $tmp_table=substr($tmp_table,$ref_pos+12);
                    $ref_pos=strpos($tmp_table,"(");
                    $fks[$tablenme][]=substr($tmp_table,0,$ref_pos);
                }
            }
        }
        // order $all_tables and check for ring constraints
        $all_tables_copy = $all_tables;
        $all_tables=PMBP_order_sql_tables($all_tables,$fks);
		$ring_contraints = false;

		// ring constraints found
        if ($all_tables===false) {
        	$ring_contraints = true;
        	$all_tables = $all_tables_copy;
        	$out.="\n# ring constraints workaround\n";
        	$out.="SET FOREIGN_KEY_CHECKS=0;\n"; 
			$out.="SET AUTOCOMMIT=0;\n";
			$out.="START TRANSACTION;\n"; 
        }
        unset($all_tables_copy);
        // as long as no error occurred
        if (!$error) {
            foreach ($all_tables as $row) {
                $tablename=$row['Name'];
                $auto_incr[$tablename]=$row['Auto_increment'];

                // don't backup tables in $PMBP_SYS_VAR['except_tables']
                if (in_array($tablename,explode(",",$PMBP_SYS_VAR['except_tables']))) {
                    continue;
                }
                $out.="\n\n";
                // export tables
                if ($tables) {
                    $out.="### structure of table `".$tablename."` ###\n\n";
                    if ($drop) $out.="DROP TABLE IF EXISTS `".$tablename."`;\n\n";
                    $out.=$table_sql[$tablename];

                    // add auto_increment value
                    if ($auto_incr[$tablename]) {
                        $out.=" AUTO_INCREMENT=".$auto_incr[$tablename];
                    }
                    $out.=";";
                }
                $out.="\n\n\n";

                // export data
                if ($data && !$error) {
                    $out.="### data of table `".$tablename."` ###\n\n";
                    // check if field types are NULL or NOT NULL
                    $res3=mysqli_query($con,"show columns from `".$tablename."`");
                    $res2=mysqli_query($con,"select * from `".$tablename."`");
                    if ($res2) {
	                    for ($j=0;$j<mysqli_num_rows($res2);$j++) {
	                        $out .= "insert into `".$tablename."` values (";
	                        $row2=mysqli_fetch_row($res2);
	                        // run through each field
	                        for ($k=0;$k<$nf=mysqli_num_fields($res2);$k++) {
	                            // identify null values and save them as null instead of ''
	                            if (is_null($row2[$k])) $out .="null"; else $out .="'".mysqli_real_escape_string($con,$row2[$k])."'";
	                            if ($k<($nf-1)) $out .=", ";
	                        }
	                        $out .=");\n";
	                        // if saving is successful, then empty $out, else set error flag
	                        if (strlen($out)>$max_size) {
	                            if ($out=PMBP_save_to_file($backupfile,$zip,$out,"a"))
									$out="";
								else
									$error=TRUE;
	                        }
	                    }
                    } else {
                    	echo "MySQL error: ".mysqli_error($con);
                    	@unlink(PMBP_EXPORT_DIR.$backupfile);
                    	return FALSE;
                    }
                // an error occurred! Try to delete file and return error status
                } elseif ($error) {
                   @unlink(PMBP_EXPORT_DIR.$backupfile);
                    return FALSE;
                }
                // if saving is successful, then empty $out, else set error flag
                if (strlen($out)>$max_size) {
                    if ($out=PMBP_save_to_file($backupfile,$zip,$out,"a"))
						$out="";
					else
						$error=TRUE;
                }
            }
            // views
            if(sizeof($all_views)>0) $out.="\n\n### create statements for views ###\n\n";
            foreach($all_views as $row) {
            	$tablename=$row['Name'];
            	$res4=mysqli_query($con,"show create view `".$tablename."`");
            	if($res4) {
	            	while($row4=mysqli_fetch_array($res4,MYSQLI_ASSOC)) {
	            		$out.="### create statement of view `".$tablename."` ###\n";
	            		$out .= $row4['Create View'];
	            		$out .= ";\n\n";
	            	}
            	} else {
                	echo "MySQL error: ".mysqli_error($con);
                	@unlink(PMBP_EXPORT_DIR.$backupfile);
                	return FALSE;
                }
            	// if saving is successful, then empty $out, else set error flag
				if (strlen($out)>$max_size) {
                    if ($out=PMBP_save_to_file($backupfile,$zip,$out,"a")) 
						$out=""; 
					else 
						$error=TRUE;
                }
            }
        // an error occurred! Try to delete file and return error status
        } else {
            @unlink($backupfile);
            return FALSE;
        }
        // if db contained ring constraints        
		if ($ring_contraints) {
			$out.="\n\n# ring constraints workaround\n";
			$out .= "SET FOREIGN_KEY_CHECKS=1;\n"; 
			$out .= "COMMIT;\n"; 
		}
		// save to file
        if ($backupfile=PMBP_save_to_file($backupfile,$zip,$out,"a")) {
            if ($zip!="zip") {
				return basename($backupfile);
			}
        } else {
           @unlink($backupfile);
           return FALSE;
        }
    } else {
        return "DB_ERROR";
    }
exit();
}

function PMBP_mysql_connect() {
	global $CONF;
	$res = mysqli_connect($CONF['sql_host'],$CONF['sql_user'],$CONF['sql_passwd']);
	return $res;
}

function PMBP_set_character_set($con) {
	$characterSet = PMBP_get_character_set($con);
	mysqli_query($con,"set names " . $characterSet);
	return $characterSet;
}
function PMBP_get_character_set($con) {
	$res = mysqli_query($con,"SHOW VARIABLES LIKE 'character_set_database'");
	$obj=mysqli_fetch_array($res,MYSQLI_ASSOC);
	if($obj['Value']) {
		return $obj['Value'];	
	} else {
		return "utf8";
	}
}
// orders the tables in $tables according to the constraints in $fks
// $fks musst be filled like this: $fks[tablename][0]=needed_table1; $fks[tablename][1]=needed_table2; ...
function PMBP_order_sql_tables($tables,$fks) {
    // do not order if no contraints exist
    if (!count($fks)) return $tables;
    // order
    $new_tables=array();
    $existing=array();
    $modified=TRUE;
    while(count($tables) && $modified==TRUE) {
        $modified=FALSE;
        foreach($tables as $key=>$row) {
            // delete from $tables and add to $new_tables
            if (isset($fks[$row['Name']])) {
                foreach($fks[$row['Name']] as $needed) {
                    // go to next table if not all needed tables exist in $existing
                    if(!in_array($needed,$existing)) continue 2;
                }
            }
            // delete from $tables and add to $new_tables
            $existing[]=$row['Name'];
            $new_tables[]=$row;
            prev($tables);
            unset($tables[$key]);
            $modified=TRUE;
        }
    }
    if (count($tables)) {
        // probably there are 'circles' in the constraints, because of that no proper backups can be created
        // This will be fixed sometime later through using 'alter table' commands to add the constraints after generating the tables.
        // Until now I just add the lasting tables to $new_tables, return them and print a warning
        foreach($tables as $row)
			$new_tables[]=$row;
//        echo PMBP_addOutput("THIS DATABASE SEEMS TO CONTAIN 'RING CONSTRAINTS'. pMBP DOES NOT SUPPORT THEM. PROBABLY THE FOLLOWING BACKUP IS BROKEN!","red_left");
        return false;
    }
    return $new_tables;
}
// saves the string in $fileData to the file $backupfile as gz file or not ($zip)
// returns backup file name if name has changed (zip), else TRUE. If saving failed, return value is FALSE
function PMBP_save_to_file($backupfile,$zip,&$fileData,$mode) {
	// save to a gzip file
    if ($zip=="") {
        if ($zp=@gzopen($backupfile,$mode."wb9")) {
            @gzwrite($zp,$fileData);
            @gzclose($zp);            
            return $backupfile;
        } else {
            return FALSE;
        }
    // save to a plain text file (uncompressed)
    } else {
        if ($zp=@fopen($backupfile,$mode)) {
            @fwrite($zp,$fileData);
            @fclose($zp);
            return $backupfile;
        } else {
            return FALSE;
        }
    }
}
?>