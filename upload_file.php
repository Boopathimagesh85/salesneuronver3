<?php
//If directory doesnot exists create it.
$output_dir = "printtemplate/tagtemp/"; 
//$maxsize    = 5242880;
$maxsize    = 52428800;
$acceptable = array(
	'image/jpeg',
	'image/jpg',
	'image/png',
	'image/bmp',
	'image/gif',
	'image/psd',
	'image/tiff',
	'image/tiff',
	'image/jp2',
	'image/vnd.wap.wbmp',
	'image/xbm',
	'image/vnd.microsoft.icon'
);
$fileCount = 0;
if(isset($_FILES["myfile"])) {
	$error =$_FILES["myfile"]["error"]; {
		if(!is_array($_FILES["myfile"]['name'])) { 
			//$fileCount = count($_FILES["myfile"]['name']);//single file
			$fileName = $_FILES["myfile"]["name"];
			$fname = str_replace(',', '', $fileName);
			//$name = explode('.',$fileName);
			//$newfileName = $name[0].'_'.time().'.'.$name[1];
			$pos = strripos($fname,'.');
			//$newfileName = substr_replace($fname, '_'.time(), $pos, 0);
			$type= $_FILES['myfile']['type'];
			$size= $_FILES['myfile']['size']; 
			$fsize = $size / 1024;
			$fsize = round($fsize,2);
			if((in_array($_FILES['myfile']['type'], $acceptable)) && (!empty($_FILES["myfile"]["type"]))) {
				if(($_FILES['myfile']['size'] >= $maxsize) || ($_FILES["myfile"]["size"] == 0)) {
					$errors = 'Size';
					echo $errors;
				} else {
					move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir. $fname);
					$secure = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "");
					$root = "http".$secure."://".$_SERVER['HTTP_HOST'];
					$root .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']); 
					$prnfile= file_get_contents($root.'printtemplate/tagtemp/'.$fname.''); 
					echo $fname.','.$fsize.' KB'.','.$type.','.$output_dir.$fname.'*'.$prnfile;
				}				
			     } else {
					move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir. $fname);
					$secure = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "");
					$root = "http".$secure."://".$_SERVER['HTTP_HOST'];
					$root .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']); 
					$prnfile= file_get_contents($root.'printtemplate/tagtemp/'.$fname.''); 
					echo $fname.','.$fsize.' KB'.','.$type.','.$output_dir.$fname.'*'.$prnfile;
			}
      	} else { //multiple file
			$fileCount = count($_FILES["myfile"]['name']);
			if($fileCount > 10) {
				for($i=0; $i < $fileCount; $i++) {
					$fileName = $_FILES["myfile"]["name"][$i];
					$fname = str_replace(',', '', $fileName);
					$type= $_FILES['myfile']['type'][$i];
					$size= $_FILES['myfile']['size'][$i]; 
					$fsize = ($size) / 1024;
					$fsize = round($fsize,2);
					$pos = strripos($fileName,'.');
					//$newfileName = substr_replace($fname, '_'.time(), $pos, 0);
					if((in_array($_FILES['myfile']['type'], $acceptable)) && (!empty($_FILES["myfile"]["type"]))) {
						if(($_FILES['myfile']['size'] >= $maxsize) || ($_FILES["myfile"]["size"] == 0)) {
							$errors = 'Size';
							echo $errors;
						} else {
							move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir. $fname);
							echo $fname.','.$fsize.' KB'.','.$type.','.$output_dir.$fname;
						}				
					} else {
						move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir. $fname);
						echo $fname.','.$fsize.' KB'.','.$type.','.$output_dir.$fname;
					}
				}
			} else {
				echo 'MaxFile';
			}
		}
	}
}

?>
