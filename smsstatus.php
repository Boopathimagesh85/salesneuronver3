<?php
	include_once('leadlicence.php');
	error_reporting(0);
	//data base config
	$mashost = $CONF["sql_host"];
	$masuser = $CONF["sql_user"];
	$maspasswd = $CONF["sql_passwd"];
	$masdb=$CONF["sql_db"];
	$con=mysqli_connect($mashost,$masuser,$maspasswd)or die(mysqli_error($con));
	mysqli_select_db($con,$masdb)or die(mysqli_error($con));
	{
		//get all databases list
		$i=0;
		$mdbqy = mysqli_query($con,'select userplaninfoid,databasenameinfo from userplaninfo where activationstatus="Yes" AND status=1 group by databasenameinfo') or die(mysqli_error($con));
		while($datas = mysqli_fetch_array($mdbqy,MYSQLI_ASSOC)) {
			$cdbname[$i] = $datas['databasenameinfo'];
			$i++;
		}
		foreach($cdbname as $dbname) {
			$chkdb = mysqli_query($con,'SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = "'.$dbname.'" ');
			$count = mysqli_num_rows($chkdb);
			if( $count>=1 ) {
				mysqli_select_db($con,$dbname) or die(mysqli_error($con));
				//fetch all message id from client db
				$tabchk = tableexitcheck($con,$dbname,'crmcommunicationlog');
				if($tabchk>=1) {
					$tblcolmnchk = tablecolumnexitcheck($con,$dbname,'crmcommunicationlog','groupid');
					if($tblcolmnchk>=1) {
						$midqy = mysqli_query($con,'select crmcommunicationlogid,groupid,smssendtypeid,smssettingsid from crmcommunicationlog where groupid !="" AND status=1')or die(mysqli_error($con));
						while($middata = mysqli_fetch_array($midqy,MYSQLI_ASSOC)) {
							$id = $middata['groupid'];
							$typeid = $middata['smssendtypeid'];
							$settingsid = $middata['smssettingsid'];
							if($typeid == 2) {
								$apikey = apikeyfetch($con,$settingsid,$typeid);//'13285h5r27e0u53ongr33';
								$statusurl = "http://alerts.sinfini.com/api/v3/index.php?method=sms.status&api_key=".$apikey."&id=".$id."&format=json";
								$chk=curl_init();
								curl_setopt($chk, CURLOPT_URL, $statusurl);
								curl_setopt($chk, CURLOPT_RETURNTRANSFER, true);
								$statusoutput=curl_exec($chk);
								curl_close($chk);
								$result = explode(' ',trim($statusoutput) );
								if($result) {
									//update data sets
									if($result['data']['id']==$id) {
										$upqy = mysqli_query($con,'UPDATE crmcommunicationlog SET communicationstatus="'.$result['data']['status'].'",place="'.$result['data']['location'].'",operator='.$result['data']['provider'].' WHERE groupid="'.$result['data']['id'].'"');
									}
								}
							}
						}
					}
				}	
			}
		}
	}
	//check the table status
	function tableexitcheck($con,$database,$newtable) {
		$counttab=0;
		$tabexits = mysqli_query($con,"SELECT COUNT(*) AS tabcount FROM information_schema.tables WHERE table_schema = '".$database."'AND table_name = '".$newtable."'");
		$counttab = mysqli_num_rows($tabexits);
		return $counttab;
	}
	//check the table column name status
	function tablecolumnexitcheck($con,$dbname,$tblname,$fieldname) {
		$countcolmn=0;
		$colmnexits = mysqli_query($con,"SELECT COLUMN_NAME FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '".$dbname."' AND TABLE_NAME = '".$tblname."' AND COLUMN_NAME = '".$fieldname."'");
		$countcolmn = mysqli_num_rows($colmnexits);
		return $countcolmn;
	}
	//api based on the settings and type(transaction/promotion/global)
	function apikeyfetch($con,$settingsid,$typeid) {
		$apikey = '';
		$result = mysqli_query($con,"SELECT smsprovidersettings.apikey from smsprovidersettings join smssettings on smssettings.smsprovidersettingsid=smsprovidersettings.smsprovidersettingsid where smssettingsid = $settingsid AND smssendtypeid = $typeid AND smsprovidersettings.status = 1");
		while($apidata = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
			$apikey = $apidata['apikey'];
		}
		return $apikey;
	}	
?>