<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Api extends REST_Controller {
	function __construct() {
		parent::__construct();
                        $this->load->model('Model_baseapi');
	}

	function lead_get() {

                    $lead_id = $this->uri->segment(3);
                    $head =  $this->input->get_request_header('X-API-KEY', TRUE);
                    $user_db =  $this->Model_baseapi->getdbmodel($head) ;
                    $dbset = $this->Model_baseapi->changedb($user_db);
                    $this->load->model('Model_lead');
                    $lead =  $this->Model_lead->get_by(array('leadid' => $lead_id, 'status' => 1));
                    if (isset($lead['leadid'])){
                        $this->response( array('status' => 'success', 'message' => $lead));
                    }
                    else{
                        $this->response( array('status' => 'failure', 'message' => 'The Specified student could not be found'), REST_Controller::HTTP_NOT_FOUND);//404);
                    }
	}
}