<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Document extends REST_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Model_baseapi');
        $this->load->helper('my_api');
        //$this->load->model('Lead_model');
        //$this->load->library('form_validation');
    }

    /**
     * [list_get Get the Document id from URL and return single Document data]
     * @return [array] [single lead data]
     */
    function list_get() {
        $user_details = $this->Model_baseapi->load_model();
        $this->load->model('document_model');
        $doc_id = $this->uri->segment(4);
        $document =  $this->document_model->get(array('documentid' => $doc_id, 'status' => 1, 'industryid' => $user_details['industryid']));
        if ($document->crmfileinfoid){
            $this->response( array('status' => 'success', 'message' => $document));
        }
        else{
            $this->response( array('status' => 'failure', 'message' => 'The Specified data could not be found'), REST_Controller::HTTP_NOT_FOUND);
        }
    }

    /**
     * [lists_get lists all active Document data]
     * @return [array] [all active Document data]
     */
    function lists_get() {
        $user_details = $this->Model_baseapi->load_model();
        $this->load->model('document_model');
        $document =  $this->document_model->get_all(array('status' => 1, 'industryid' => $user_details['industryid']));
        if (isset($document)){
            $this->response( array('status' => 'success', 'message' => $document));
        }
        else{
            $this->response( array('status' => 'failure', 'message' => 'The Specified data could not be found'), REST_Controller::HTTP_NOT_FOUND);
        }
    }

    /**
     * [create_put Gets data through put method and sends to base for validation and insert]
     * @return [mixed] [if sucess returns created data, if not throws error]
     */
    function create_post() {
        $user = $this->Model_baseapi->load_model();
        $this->load->model('document_model');
        $rul  = $this->document_model->validate_array();
        $temp = $rul['create_post'];
        $field = $this->form_validation->get_field_names($temp);
        $data = remove_unknown_fields($this->post(), $field);
        $data = $this->document_model->create_timestamp($data);
        $data['employeeid'] = $user['user_details']->employeeid;
        $data['createuserid'] = $user['user_details']->employeeid;
        $data['lastupdateuserid'] = $user['user_details']->employeeid;
        $data['industryid'] = $user['industryid'];
        $data['createdate'] = date('Y-m-d H:i:s');
        if(isset($_POST['keywords'])){
            $keywords = $_POST['keywords'];
        }else{
            $keywords = '';
        }
        // print_r($_POST);
        // print_r($_FILES); die;
            $files = $_FILES['userfile'];
            //print_r($files); die;
            if ($_FILES) {
                $config = array(
                    'upload_path' => 'uploads/',
                    'allowed_types' => 'gif|jpg|png|pdf|psd|tmp',
                    'max_size' => '5120'
                );

                    $this->load->library('upload', $config);

                        foreach ($files['name'] as $key => $image) {
                            $_FILES['userfile[]']['name']= $files['name'][$key];
                            $_FILES['userfile[]']['type']= $files['type'][$key];
                            $_FILES['userfile[]']['tmp_name']= $files['tmp_name'][$key];
                            $_FILES['userfile[]']['error']= $files['error'][$key];
                            $_FILES['userfile[]']['size']= $files['size'][$key];

                            $fileName = $image;
                            $userfile[] = $fileName;
                            $pos = strripos($fileName,'.');
                            $newfileName = substr_replace($fileName, '_'.time(), $pos, 0);
                            $config['file_name'] = $newfileName;

                                    $this->upload->initialize($config);
                                    $this->form_validation->set_data($data);
                                    $this->form_validation->set_rules($temp);
                                    if ($this->form_validation->run() === TRUE) {
                                    //    echo "true";
                                        $this->load->model('Document_model');
                                if ($this->upload->do_upload('userfile')) {
                                    $result = $this->upload->data();
                                        $document = array(
                                                'commonid'=>1,
                                                'moduleid'=>203,
                                                'filename'=>$result[$key]['file_name'],
                                                'filename_size'=>$result[$key]['file_size']." KB",
                                                'filename_type'=>$result[$key]['file_type'],
                                                'filename_path'=>'uploads/'.$result[$key]['file_name'],
                                                'filename_fromid'=>2,
                                                'foldernameid'=>6,
                                                'keywords'=> $keywords,
                                                'createdate'=>$data['createdate'],
                                                'lastupdatedate'=>$data['createdate'],
                                                'createuserid'=>$data['createuserid'],
                                                'lastupdateuserid'=>$data['createuserid'],
                                                'status'=>1
                                            );
                                            //print_r($document); die;
                                            //$return_id =  $this->Model_baseapi->insert_with_validation($data, $temp, 'Contact_model');
                                            
                                            $id = $this->document_model->insert($document);
                                             if(isset($id)){
                                                    $this->response( array('status' => 'success', 'id' => $id,  'message' => 'Image uploaded Successfully'));
                                                    }else{
                                                        $this->response( array('status' => 'failed', 'message' => 'Failed to upload Image'));
                                                    }
                                }

                                } else {
                                    return $this->response(array('status' => 'failed', 'error' => $this->form_validation->get_errors_as_array()));                                    
                                }
                          }
            }
        }
      /*  switch ($return['status']) {
            case 'validation':
                $this->response(array('code' => REST_Controller::HTTP_BAD_REQUEST, 'status'=>'failure','message'=>$return['error'] ));
            break;
            case 'success':
                $this->response(array('code' => REST_Controller::HTTP_CREATED, 'status'=>'success', 'message'=> 'created', 'data'=> $return['data']));
            break;
            case 'error':
                $this->response(array('error' => array('code' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, 'message' => 'Internal Server Error Occured. Cannot Perform Create Operation.')));
            break;
            default:
                $this->response(array('error' => array('code' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, 'message' => 'Internal Server Error Occured. Cannot Perform Create Operation.')));
            break;
         }*/


    /**
     * [update_post Gets data through post method with the data-id to be updated and sends to base for validation and update]
     * @return [mixed] [if sucess returns updated data, if not throws error]
     */
    function update_post() {
        $user = $this->Model_baseapi->load_model();
        $document_id = $this->uri->segment(4);
        if($document_id) {
            $this->load->model('document_model');
            $rul  = $this->document_model->validate_array();
            $temp = $rul['update_post'];
            $field = $this->form_validation->get_field_names($temp);
            $data = remove_unknown_fields($this->post(), $field);
            $data['employeeid'] = $user['user_details']->employeeid;
            $data['lastupdateuserid'] = $user['user_details']->employeeid;

            $return =  $this->Model_baseapi->update_with_validation($data, $temp, $this->document_model->table, $document_id);

            switch ($return['status']) {
                case 'validation':
                    $this->response(array('code' => REST_Controller::HTTP_BAD_REQUEST, 'status'=>'failure','message'=>$return['error'] ));
                break;
                case 'success':
                    $this->response(array('code' => REST_Controller::HTTP_OK, 'status'=>'success', 'message'=> 'updated', 'data'=> $return['data']));
                break;
                case 'error':
                    $this->response(array('error' => array('code' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, 'message' => 'Internal Server Error Occured. Cannot Perform Update Operation.')));
                break;
                default:
                    $this->response(array('error' => array('code' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, 'message' => 'Internal Server Error Occured. Cannot Perform Update Operation.')));
                break;
            }
        } else {
            $this->response( array('code' => REST_Controller::HTTP_NOT_FOUND, 'status' => 'failure', 'message' => 'Missing parameter in the URL'));
        }
    }

    /**
     * [delete_delete Deletes the document]
     * @return [array] [deleted document ID]
     */
    function delete_delete() {
        $user_db_select = $this->Model_baseapi->load_model();
        $this->load->model('document_model');
        $document_id = $this->uri->segment(4);
        $data = array('status' => 0);
        $this->db->where('documentid', $document_id);
        $id = $this->db->update($this->document_model->table,$data);
        if (isset($id)){
            $this->response( array('status' => 'success', 'message' => 'Record Deleted Successfully'));
        }
        else{
            $this->response( array('status' => 'failure', 'message' => 'Record could not be deleted'), REST_Controller::HTTP_NOT_FOUND);
        }
    }

    function media_put() {
        $user_db_select = $this->Model_baseapi->load_model();
        $document_id = $this->uri->segment(4);
   /*         $putdata = fopen("php://input", "r");
            //print_r(file_get_content($this->put())); die;
            $head =  $this->input->get_request_header('CONTENT_TYPE');
            $result = [];
            $rawPost = file_get_contents('php://input');
            mb_parse_str($rawPost, $result);
           // print_r($result); //die;
            $file = [];
            $string = implode(" ",$result);
            $whatIWant = substr($string, strpos($string, ": ") + 1);
            $words=str_word_count($whatIWant, 1);
             $file['mimetype']   = $words[0]."/".$words[1];
            $filename = substr($string, strpos($string, '="') + 1);
            $temp =  explode('"', $filename);
            ///print_r($this->put());
            $file['filename'] = $temp[1];
            //$file['mimetype']  = explode(" ", $whatIWant);
            $file['ext'] = get_extension($file['mimetype']);
            $pos = strripos($file['filename'],'.');
            $newfileName = substr_replace($file['filename'], '_'.time(), $pos, 0);
            $file['filename_ws'] = $newfileName;*/

            //$size = filesize("php://input");
            $file = [];
            $file['filename_ws'] = 'image_'.time().'.jpg';
            $putdata = fopen("php://input", "r");
            /* Open a file for writing */
            $fp = fopen("uploads/".$file['filename_ws']."", "w");
            /* Read the data 1 KB at a time
               and write to the file */
            while ($data = fread($putdata, 1024*50))
              fwrite($fp, $data);

                        $data = array(
                                'documentlogo_path' => $file['filename_ws'],
                                'documentlogo' => 'image.jpg',
                        );

                        $this->db->where('crmfileinfoid', $document_id);
                        $id =  $this->db->update('crmfileinfo',$data);
                        if(isset($id)){
                        $this->response( array('code' => '201', 'status' => 'success', 'message' => 'Image uploaded Successfully'));
                        }else{
                            $this->response( array('status' => 'failed', 'message' => 'Failed to upload Image'));
                        }

   /*         $filesize = fstat($fp);
            $file['size'] = round(($filesize['size'] / 1024), 2);
            $file['size'] = $file['size'] ."KB";

            $array = array(
                'documentlogo'         => $file['filename'],
                'documentlogo_size'         => $file['size'],
                'documentlogo_type'         => $file['mimetype'],
                'documentlogo_path'         => $file['filename'],
                );
                */


            fclose($fp);
            fclose($putdata);
    }
}