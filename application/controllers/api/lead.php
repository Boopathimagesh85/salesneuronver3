<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Lead extends REST_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Model_baseapi');
        //$this->load->model('Lead_model');
        //$this->load->library('form_validation');
    }
    /**
     * [list_get Get the lead id from URL and return single lead data]
     * @return [array] [single lead data]
     */
    function list_get() {
        $user_details = $this->Model_baseapi->load_model();
        $this->load->model('Lead_model');
        $lead_id = $this->uri->segment(4);
        $lead =  $this->Lead_model->get(array('leadid' => $lead_id, 'status' => 1, 'industryid' => $user_details['industryid']));
        if ($lead->leadid){
            $this->response( array('status' => 'success', 'message' => $lead));
        }
        else{
            $this->response( array('status' => 'failure', 'message' => 'The Specified data could not be found'), REST_Controller::HTTP_NOT_FOUND);
        }
    }

    /**
     * [lists_get lists all active lead data]
     * @return [array] [all active lead data]
     */
    function lists_get() {
        $user_details = $this->Model_baseapi->load_model();
        $this->load->model('Lead_model');
        $lead =  $this->Lead_model->get_all(array('status' => 1, 'industryid' => $user_details['industryid']));
        if ($lead){
            $this->response( array('status' => 'success', 'message' => $lead));
        }
        else{
            $this->response( array('status' => 'failure', 'message' => 'The Specified data could not be found'), REST_Controller::HTTP_NOT_FOUND);
        }
    }

    /**
     * [create_put Gets data through put method and sends to base for validation and insert]
     * @return [mixed] [if sucess returns created data, if not throws error]
     */
    function create_put() {
        $user = $this->Model_baseapi->load_model();
        $this->load->model('Lead_model');
        $rul  = $this->Lead_model->validate_array();
        $temp = $rul['create_put'];
        $field = $this->form_validation->get_field_names($temp);
        $data = remove_unknown_fields($this->put(), $field);
        $data['employeeid'] = $user['user_details']->employeeid;
        $data['createuserid'] = $user['user_details']->employeeid;
        $data['lastupdateuserid'] = $user['user_details']->employeeid;
        $data['industryid'] = $user['industryid'];

        if(isset($data['primary'])){
            $primary = $data['primary'];
            unset($data['primary']);
            $address = array('primary' => $primary);
        }
        if(isset($data['secondary'])) {
            $secondary = $data['secondary'];
            unset($data['secondary']);
            $address = array_merge( array(
                                                        'primary'=>$primary
                                                ), array(
                                                        'secondary'=>$secondary
                                                ));
        }

        $return =  $this->Model_baseapi->insert_with_validation($data, $temp, 'Lead_model');
        //Address insert
        
        if(!empty($address)){
            $add['createuserid'] = $data['employeeid'];
            $add['lastupdateuserid'] = $data['employeeid'];
            $add['status'] = 1;
            $add['createdate'] = date('Y-m-d H:i:s');
            $add['lastupdatedate'] = date('Y-m-d H:i:s');
            $id = $this->Model_baseapi->addressinsert($this->Lead_model->table, $address, $return['data']['Lead_modelid'], $add);
            if($id){
                $return['data'] = array_merge($return['data'], array('address_ids' => $id));
            }
        }

        switch ($return['status']) {
            case 'validation':
                $this->response(array('code' => REST_Controller::HTTP_BAD_REQUEST, 'status'=>'failure','message'=>$return['error'] ));
            break;
            case 'success':
                $this->response(array('code' => REST_Controller::HTTP_CREATED, 'status'=>'success', 'message'=> 'created', 'data'=> $return['data']));
            break;
            case 'error':
                $this->response(array('error' => array('code' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, 'message' => 'Internal Server Error Occured. Cannot Perform Create Operation.')));
            break;
            default:
                $this->response(array('error' => array('code' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, 'message' => 'Internal Server Error Occured. Cannot Perform Create Operation.')));
            break;
         }
    }

    /**
     * [update_post Gets data through post method with the data-id to be updated and sends to base for validation and update]
     * @return [mixed] [if sucess returns updated data, if not throws error]
     */
    function update_post() {
        $user = $this->Model_baseapi->load_model();
        $lead_id = $this->uri->segment(4);
        if($lead_id) {
            $this->load->model('Lead_model');
            $rul  = $this->Lead_model->validate_array();
            $temp = $rul['update_post'];
            $field = $this->form_validation->get_field_names($temp);
            $data = remove_unknown_fields($this->post(), $field);
            $data['employeeid'] = $user['user_details']->employeeid;
            $data['lastupdateuserid'] = $user['user_details']->employeeid;

        if(isset($data['primary'])){
            $primary = $data['primary'];
            unset($data['primary']);
            $address = array('primary' => $primary);
        }

        if(isset($data['secondary'])) {
            $secondary = $data['secondary'];
            unset($data['secondary']);
            $address = array_merge( array(
                                                        'primary'=>$primary
                                                ), array(
                                                        'secondary'=>$secondary
                                                ));
        }

            $return =  $this->Model_baseapi->update_with_validation($data, $temp, $this->Lead_model->table, $lead_id);


        if(!empty($address)){
            $add['lastupdateuserid'] = $data['employeeid'];
            $add['status'] = 1;
            $add['lastupdatedate'] = date('Y-m-d H:i:s');
            $id = $this->Model_baseapi->addressupdate($this->Lead_model->table, $address, $lead_id, $add);
            if($id == true){
                $return['data']['address'] = 'Address Updated';
            }else{
                $return['data']['address'] = 'Address Not Updated';
            }
        }

            switch ($return['status']) {
                case 'validation':
                    $this->response(array('code' => REST_Controller::HTTP_BAD_REQUEST, 'status'=>'failure','message'=>$return['error'] ));
                break;
                case 'success':
                    $this->response(array('code' => REST_Controller::HTTP_OK, 'status'=>'success', 'message'=> 'updated', 'data'=> $return['data']));
                break;
                case 'error':
                    $this->response(array('error' => array('code' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, 'message' => 'Internal Server Error Occured. Cannot Perform Update Operation.')));
                break;
                default:
                    $this->response(array('error' => array('code' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, 'message' => 'Internal Server Error Occured. Cannot Perform Update Operation.')));
                break;
            }
        } else {
            $this->response( array('code' => REST_Controller::HTTP_NOT_FOUND, 'status' => 'failure', 'message' => 'Missing parameter in the URL'));
        }
    }

    /**
     * [delete_delete Deletes the lead]
     * @return [array] [deleted lead ID]
     */
    function delete_delete() {
        $user_db_select = $this->Model_baseapi->load_model();
        $this->load->model('Lead_model');
        $lead_id = $this->uri->segment(4);
        $data = array('status' => 0);
        $this->db->where('leadid', $lead_id);
        $id = $this->db->update($this->Lead_model->table,$data);
        if (isset($id)){
            $this->response( array('status' => 'success', 'message' => 'Record Deleted Successfully'));
        }
        else{
            $this->response( array('status' => 'failure', 'message' => 'Record could not be deleted'), REST_Controller::HTTP_NOT_FOUND);
        }
    }
}