<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Module : Model_baseapi
 */
class Model_baseapi extends MY_Model {
        function __construct() {
                parent::__construct();
                $this->load->library('form_validation');
                $this->protected = array('leadid', 'leadnumber');
        }

        //public $protected_attributes = array('leadid', 'leadnumber');
       /**
        * [load_model API KEY validate and get user and change db]
        * @return [type] [User DB]
        */
      public function load_model() {
            $head =  $this->input->get_request_header('X-API-KEY', TRUE);
            $user =  $this->getdbmodel($head);
            $this->changedb($user['db']);
            $user['user_details'] = $this->db->get_where('employee', array('emailid' => $user['mail']))->first_row();
            return $user;
        }

        /**
         * [getdbmodel Get user database from api key]
         * @param  [string] $apikey [API Key from header]
         * @return [string]         [User Database name]
         */
        public function getdbmodel($apikey) {
            $query = $this->db->get_where('keys', array('key' => $apikey), 1)->row();
            $mail = $query->user_email;
            $dbquery = $this->db->get_where('userplaninfo', array('registeremailid' => $mail), 1)->row();
            if(!$dbquery){
                return json_encode(array('code' => 402, 'status' => 'failed', 'message' => 'Invalid API Key'));
            }
            $dbname = $dbquery->databasenameinfo;
            $industryid = $dbquery->industryid;
            $gsetting = $this->db->get_where('generalsetting', array('companyid' => $dbquery->companyid), 1)->row();
            //print_r($gsetting); die;
            if($gsetting->accountexpire === "Yes"){
                print_r(json_encode(array('code' => 402, 'status' => 'failed', 'message' => 'Your Account has been Expired. Please Renew your account')));
                die;
            }
            $user = array('db' => $dbname, 'mail' => $mail, 'industryid' => $industryid);
            return $user;    //$dbname; //array('db' => $dbname, 'mail' => $mail);
        }

        /**
         * [changedb Switchs database connection]
         * @param  [string] $database [current database]
         * @return [string]           [db changed]
         */
        public function changedb($database) {
            $this->db->db_select($database);
        }

       /**
        * [modulefieldegeneration Generate modulefields from module id]
        * @param  [int] $moduleid [module ID]
        * @return [array]           [fields and its properties]
        */
       public function  modulefieldegeneration($moduleid){
            $userroleid = 2;//$this->userroleid;
            $i=0;
            $moduletabsecfldsgrparray = array();
            $this->db->select('modulefield.modulefieldid,modulefield.columnname,modulefield.tablename,modulefield.uitypeid,modulefield.fieldname,modulefield.fieldlabel,modulefield.readonly,modulefield.defaultvalue,modulefield.maximumlength,modulefield.mandatory,modulefield.active,modulefield.moduletabsectionid,modulefield.sequence,moduletabsection.moduletabsectionname,moduletabgroup.moduletabgroupid,modulefield.moduletabid,modulefield.multiple,modulefield.ddparenttable,modulefield.parenttable,modulefield.fieldcolumntype,module.modulename,module.modulelink,modulefield.decimalsize,moduletabsection.moduletabsectiontypeid,modulefield.fieldcustomvalidation,userrolemodulefiled.fieldactive,userrolemodulefiled.fieldreadonly,modulefield.unique,uitype.uitypename',false);//,userrolemodulefiled.fieldactive,userrolemodulefiled.fieldreadonly
            $this->db->from('modulefield');
            $this->db->join('uitype','uitype.uitypeid=modulefield.uitypeid');
            $this->db->join('userrolemodulefiled','userrolemodulefiled.modulefieldid=modulefield.modulefieldid','left outer');
            $this->db->join('moduletabsection','moduletabsection.moduletabsectionid=modulefield.moduletabsectionid');
            $this->db->join('moduletabgroup','moduletabgroup.moduletabgroupid=moduletabsection.moduletabgroupid');
            $this->db->join('module','module.moduleid=moduletabgroup.moduleid');
            $this->db->where("modulefield.moduletabid", $moduleid);
            $this->db->where("userrolemodulefiled.moduleid", $moduleid);
            $this->db->where("FIND_IN_SET('$userroleid',moduletabgroup.userroleid)>",0);
            $this->db->where("FIND_IN_SET('$userroleid',moduletabsection.userroleid)>",0);
            $this->db->where("FIND_IN_SET('$userroleid',modulefield.userroleid)>",0);
            $this->db->where('userrolemodulefiled.userroleid',$userroleid);
            $this->db->where_in('modulefield.status',array(1,10));
            $this->db->where('userrolemodulefiled.status',1);
            $this->db->where('moduletabsection.status',1);
            $this->db->where('moduletabgroup.status',1);
            $this->db->order_by('moduletabgroup.sequence','asc');
            //$this->db->order_by('moduletabgroup.moduletabgroupid','asc');
            $this->db->order_by('moduletabsection.sequence','asc');
            //$this->db->order_by('moduletabsection.moduletabsectionid','asc');
            $this->db->order_by('modulefield.sequence','asc');
            $this->db->group_by('modulefield.modulefieldid');
            //$this->db->limit(3, 1);
            $querytbg = $this->db->get();
            foreach($querytbg->result() as $rows) {
                $moduletabsecfldsgrparray[$i] = array('colname'=>$rows->columnname,'tabname'=>$rows->tablename,'uitypeid'=>$rows->uitypeid,'fieldname'=>$rows->fieldname,'filedlabel'=>$rows->fieldlabel,'readmode'=>$rows->fieldreadonly,'defvalue'=>$rows->defaultvalue,'maxlength'=>$rows->maximumlength,'mandmode'=>$rows->mandatory,'fieldmode'=>$rows->fieldactive,'tabsectionid'=>$rows->moduletabsectionid,'fieldseq'=>$rows->sequence,'tabsectionname'=>$rows->moduletabsectionname,'modtabgrpid'=>$rows->moduletabgroupid,'moduleid'=>$rows->moduletabid,'modulename'=>$rows->modulename,'multiple'=>$rows->multiple,'ddparenttable'=>$rows->ddparenttable,'parenttable'=>$rows->parenttable,'fieldcolmtypeid'=>$rows->fieldcolumntype,'modlink'=>$rows->modulelink,'decisize'=>$rows->decimalsize,'sectiontypeid'=>$rows->moduletabsectiontypeid,'customrule'=>$rows->fieldcustomvalidation,'modfieldid'=>$rows->modulefieldid,'modfieldunique'=>$rows->unique,'uitypename'=>$rows->uitypename);
                $i++;
            }
            return $moduletabsecfldsgrparray;
       }

       /**
        * [insert_with_validation Insert operation after validation]
        * @param  [array] $data   [It contains the data that are sent through put method. Unknown fields are removed before passing here]
        * @param  [array] $fields [It contains the fields with validation array]
        * @param  [string] $table  [table to which the data to be inserted]
        * @return [array]         [returns the insert operation status success or failed or unknown error]
        */
       public function insert_with_validation($data, $fields, $table) {
             $this->form_validation->set_data($data);
                $this->form_validation->set_rules($fields);
                  if ($this->form_validation->run() === TRUE) {
                    $this->load->model($table);
                        $id = $this->{$table}->insert($data);
                       if($id){
                           $return = array($table.'id' => $id);
                           $tem = array_merge($return, $data);
                           $created = array('data' => $tem, 'status' => 'success');
                            return $created;
                        }else{
                            $error  = array('status' => 'internal');
                            return $error;
                        }
                    }else{
                        $error  = array('status' => 'validation', 'error' => $this->form_validation->get_errors_as_array());
                        return $error;
                    }
       }

        /**
        * [update_with_validation Update operation after validation]
        * @param  [array] $data   [It contains the data that are sent through post method. Unknown fields are removed before passing here]
        * @param  [array] $fields [It contains the fields with validation array]
        * @param  [string] $table  [table to which the data to be updated]
        * @param  [int] $id  [ID of the data to be updated]
        * @return [array]         [returns the update operation status success or failed or unknown error]
        */
       public function update_with_validation($data, $fields, $table, $id) {
             $this->form_validation->set_data($data);
                $this->form_validation->set_rules($fields);
                  if ($this->form_validation->run() === TRUE) {
                        $this->db->where($table.'id', $id);
                        $this->db->update($table,$data);
                       if($id){
                           $return = array($table.'id' => $id);
                           $tem = array_merge($return, $data);
                           $created = array('data' => $tem, 'status' => 'success');
                            return $created;
                        }else{
                            $error  = array('status' => 'internal');
                            return $error;
                        }
                    }else{
                        $error  = array('status' => 'validation', 'error' => $this->form_validation->get_errors_as_array());
                        return $error;
                    }
       }

       /**
        * [validaterulesformat Generates array format according to MY_Model library validation - Used in model]
        * @param  [array] $modulefields [all module fields with its properties]
        * @return [array]        [Array format for validation]
        */
       public function validaterulesformat($modulefields) {
            foreach ($modulefields as $key => $value) {
                $rules = array();
                if ($value['mandmode'] == "Yes"){
                 $rules[] = 'required';
                }
                if ($value['modfieldunique'] == "Yes"){
                     $rules[] = 'unique';
                }
                if ($value['maxlength']){
                    $rules[] = 'max_length['.$value["maxlength"].']';
                }
                if ($value['uitypeid']){
                    switch ($value['uitypeid']) {
                        case 4:
                            $rules[] = 'integer';
                            break;
                        case 8:
                            $rules[] = 'integer';
                            break;
                        case 10:
                            $rules[] = 'valid_email';
                            break;
                        case 12:
                            $rules[] = 'valid_url';
                            break;
                        default:
                            break;
                    }
                }
                $rule = ! empty($rules) ? implode('|', $rules) : '';
                $arr[]= array('field' => $value['colname'], 'label' => $value['filedlabel'], 'rules' => $rule);
            }
            return $arr;
       }

       public function addressinsert($module, $addressdata, $modid, $add){

        foreach ($addressdata as $key => $value) {
            if($key == "primary"){
                $type = 4;
            }else{
                $type   = 5;
            }
                    $value['address'] = ((isset($value['address']))? $value['address'] : '' );
                    $value['pincode'] = ((isset($value['pincode']))? $value['pincode'] : '' );
                    $value['city'] = ((isset($value['city']))? $value['city'] : '' );
                    $value['state'] = ((isset($value['state']))? $value['state'] : '' );
                    $value['country'] = ((isset($value['country']))? $value['country'] : '' );

                        $ins_address = array(
                            ''.$module.'id'=>$modid,
                            'addresstypeid'=>$type,
                            'addresssourceid'=>$type,
                            'addressmethod'=>$type,
                            'address'=>$value['address'],
                            'pincode'=>$value['pincode'],
                            'city'=>$value['city'],
                            'state'=>$value['state'],
                            'country'=>$value['country']
                        );
                        $address = array_merge($ins_address, $add);
                        //print_r($address); die;
            $this->db->insert($module."address", $address);
            $id[] = $this->db->insert_id();
        }
        return $id;
       }


       public function addressupdate($module, $addressdata, $modid, $add){

        foreach ($addressdata as $key => $value) {
            if($key == "primary"){
                $type = 4;
            }else{
                $type   = 5;
            }
                    $value['address'] = ((isset($value['address']))? $value['address'] : '' );
                    $value['pincode'] = ((isset($value['pincode']))? $value['pincode'] : '' );
                    $value['city'] = ((isset($value['city']))? $value['city'] : '' );
                    $value['state'] = ((isset($value['state']))? $value['state'] : '' );
                    $value['country'] = ((isset($value['country']))? $value['country'] : '' );

                        $ins_address = array(
                            ''.$module.'id'=>$modid,
                            'addresstypeid'=>$type,
                            'addresssourceid'=>1,
                            'addressmethod'=>$type,
                            'address'=>$value['address'],
                            'pincode'=>$value['pincode'],
                            'city'=>$value['city'],
                            'state'=>$value['state'],
                            'country'=>$value['country']
                        );
                        $address = array_merge($ins_address, $add);

                        $whr = array('addressmethod'=>$type, $module.'id'=>$modid);
                        $this->db->where($whr);
                        if(!$this->db->update($module.'address', $address)){
                            return false;
                        }
                       
            //$this->db->insert($module."address", $address);
            //$id[] = $this->db->insert_id();
        }
        return true;
       }
       

}