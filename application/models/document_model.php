<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Module : document
*
*/
class document_model extends MY_Model {

    protected $_myVar;

    public $table  = 'crmfileinfo';

    public $primary_key = 'crmfileinfoid';

    function __construct() {
        parent::__construct();
        $this->table = 'crmfileinfo';
        $this->primary_key = 'crmfileinfoid';
        $this->load->model('Model_baseapi');
        $this->load->helper('my_api');
        $documentmodulefields =  $this->Model_baseapi->modulefieldegeneration("207");
        $rules_arr =  $this->Model_baseapi->validaterulesformat($documentmodulefields);
        $this->_myVar = $this->rules_get($rules_arr);
    }

    public $protected = array('crmfileinfoid');

    protected $return_type = 'array';

    protected $after_get = array('remove_sesitive_data');

    protected $before_create = array('create_timestamp'); //prep_data

    protected $before_update = array('update_timestamp');

    protected  function remove_sesitive_data($student) {
        unset($student['password']);
        unset($student['ip_address']);
        return $student;
    }

    protected  function prep_data($student) {
        $student['password'] = md5($student['password']);
        $student['ip_address'] = $this->input->ip_address();
        $student['created_timestamp'] = date('Y-m-d H:i:s');
        return $student;
    }

    public  function create_timestamp($data){
        $data['createdate'] = date('Y-m-d H:i:s');
        $data['lastupdatedate'] = date('Y-m-d H:i:s');
        $data['status'] = 1;
        return $data;
    }

    public  function rules_get($rules_arr){
        $rules_arr  = array('create_post' => $rules_arr);
        $new_plan = array();
        foreach ($rules_arr['create_post'] as $item)
        {
          $new_plan[$item['field']] = $item;
        }
        $rules_arr  = array('create_post' => $new_plan);
        return $rules_arr;
    }

    public function validate_array() {
          // Once the property has been set in the constructor, it keeps its value for the whole object :
          $access = $this->_myVar;
          return $access;
    }
}