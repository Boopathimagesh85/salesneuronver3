<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Module : Lead
*
*/
class Lead_model extends MY_Model {

    protected $_myVar;

    public $table  = 'lead';

    public $primary_key = 'leadid';

    function __construct() {
        parent::__construct();
        $this->table = 'lead';
        $this->primary_key = 'leadid';
        $this->load->model('Model_baseapi');
        $this->load->helper('my_api');
        $leadmodulefields =  $this->Model_baseapi->modulefieldegeneration("201");
        $rules_arr =  $this->Model_baseapi->validaterulesformat($leadmodulefields);
        $this->_myVar = $this->rules_get($rules_arr);
    }

    public $fillable = array();

    public $protected = array();

    protected $return_type = 'array';

    //protected $after_get = array('remove_sesitive_data');

    protected $before_create = array('create_timestamp'); //prep_data

    protected $before_update = array('update_timestamp');

    protected  function remove_sesitive_data($data) {
        unset($data['password']);
        unset($data['ip_address']);
        return $data;
    }

    protected  function prep_data($data) {
        $data['password'] = md5($data['password']);
        $data['ip_address'] = $this->input->ip_address();
        $data['createdate'] = date('Y-m-d H:i:s');
        $data['lastupdatedate'] = date('Y-m-d H:i:s');
        return $data;
    }

    public  function create_timestamp($data){
        $data['createdate'] = date('Y-m-d H:i:s');
        $data['lastupdatedate'] = date('Y-m-d H:i:s');
        $data['status'] = 1;
        return $data;
    }

    public  function rules_get($rules_arr){
        $rules_arr  = array('create_put' => $rules_arr);
        $new_plan = array();
        foreach ($rules_arr['create_put'] as $item)
        {
          $new_plan[$item['field']] = $item;
        }
        $rules_arr  = array('create_put' => $new_plan, 'update_post' => $new_plan,);
        return $rules_arr;
    }

    public function validate_array() {
          // Once the property has been set in the constructor, it keeps its value for the whole object :
          $access = $this->_myVar;
          return $access;
    }
}