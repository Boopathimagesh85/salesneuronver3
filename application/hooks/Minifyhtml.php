<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Minifyhtml Class
 * Will Minify the HTML. Reducing network latency, enhancing compression, and faster browser loading and execution.
 * 
 * @category	Output
 * @author      John Gerome
 * @link	    https://github.com/johngerome/CodeIgniter-Minifyhtml-hooks
 */

		
 
class Minifyhtml {
    
    /**
     * Responsible for sending final output to browser
     */
	 
	// experimental inline javascript minifier
	 
	 function compress($buffer)
	 {
		 ini_set("pcre.recursion_limit", "16777"); // for memory limits

		$inline_scripts=array();
		if (preg_match_all('|([removed]]*?&gt;.*?<\/script>)|is', $buffer, $pock))
		{
			foreach ($pock[1] as $key=>$content)
			{
				$inline_scripts['INLINE_SCRIPT_'.$key]=$content;
			}
			$buffer=str_replace(array_values($inline_scripts), array_keys($inline_scripts), $buffer);
		}

		$search = array(
		'/\>[^\S ]+/s',
		'/[^\S ]+\</s',
		'/(\s)+/s',
		);
		$replace = array(
		'>',
		'<',
		'\\1',
		);

		$buffer = preg_replace($search, $replace, $buffer);

		if (count($inline_scripts)>0)
		{
			$buffer=  str_replace(array_keys($inline_scripts), array_values($inline_scripts), $buffer);
			
		}
			
		return $buffer;	
		 
	 }
		
	 
    function output()
    {		
	
			ini_set("pcre.recursion_limit", "16777"); // for memory limits

			$CI =& get_instance();
			$buffer = $CI->output->get_output();
			
			$pattern = '/(?:(?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:(?<!\:|\\\|\')\/\/.*))/';
			$buffer = preg_replace($pattern, '',$buffer);

			$re = '%            # Collapse ws everywhere but in blacklisted elements.
				(?>             # Match all whitespans other than single space.
				  [^\S ]\s*     # Either one [\t\r\n\f\v] and zero or more ws,
				| \s{2,}        # or two or more consecutive-any-whitespace.
				) # Note: The remaining regex consumes no text at all...
				(?=             # Ensure we are not in a blacklist tag.
				  (?:           # Begin (unnecessary) group.
					(?:         # Zero or more of...
					  [^<]++    # Either one or more non-"<"
					| <         # or a < starting a non-blacklist tag.
								# Skip Script and Style Tags
					  (?!/?(?:textarea|pre|script|style)\b)
					)*+         # (This could be "unroll-the-loop"ified.)
				  )             # End (unnecessary) group.
				  (?:           # Begin alternation group.
					<           # Either a blacklist start tag.
								# Dont foget the closing tags 
					(?>textarea|pre|script|style)\b
				  | \z          # or end of file.
				  )             # End alternation group.
				)  # If we made it here, we are not in a blacklist tag.
				%ix';
			$buffer = preg_replace($re, " ", $buffer);
			// patterns to try for removing various bugs
			// $pattern = '/(?:(?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:(?<!\:|\\\|\')\/\/.*))/';
			// $pattern = '/(?:(?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:(?<!\:|\\\|\'|\")\/\/.*))/';
			// $pattern = '/(?:(?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:(?<!\:|\\\|\')\/\/.*))/';
		    // $pattern = '/\/\*[\s\S]*?\*\/|([^\\:]|^)\/\/.*|<!--[\s\S]*?-->$/';


		//	$buffer = preg_replace($pattern, '',$buffer);
		//	$js_minified = $this->compress($buffer);
		//  $pattern = '/(?:(?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:(?<!\:|\\\|\')\/\/.*))/';
		//  $js_minified = preg_replace($pattern, '', $js_minified);
		//	$CI->output->set_output($js_minified);
		//	$buffer = $this->compress($buffer);
		
		
			$CI->output->set_output($buffer);
				
			$CI->output->_display();
    }
	
		
}
?>
