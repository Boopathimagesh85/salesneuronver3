<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboardbuilder extends CI_Controller {
	function __construct() {
    	parent::__construct();	
		$this->load->model('Base/Basefunctions');
		$this->load->model('Dashboardbuilder/Dashboardbuildermodel');
		$this->load->helper('directory');
		$this->load->helper('file');
    }
	public function index() {
		$moduleid = array(240);
		sessionchecker($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['moduledata'] = $this->Dashboardbuildermodel->getmodulelist($moduleid);
		$data['dashboardfolder'] = $this->Basefunctions->simpledropdown('dashboardfolder','dashboardfoldername,dashboardfolderid','dashboardfoldername');
		$data['sourcereport']= $this->Dashboardbuildermodel->loadsummaryreportname();
		$this->load->view('Dashboardbuilder/dashboardbuilderview',$data);
	}
	//dashboard data create
	public function dashboradbuildercreate() {
		$this->Dashboardbuildermodel->dashboraddatabuildmodel();
	}
	//dashboard data update
	public function dashboradbuilderupdate() {
		$this->Dashboardbuildermodel->dashboradbuilderupdatemodel();
	}
	//dashboard-chart-source report grouping drop down
	public function loadgroupdropdowns() {
		$this->Dashboardbuildermodel->loadgroupdropdowns();
	}
	//report liat load
	public function loadreportlist() {
		$this->Dashboardbuildermodel->loadreportlist();
	}
	/*
	* Dashboard Data Filter Menu(accordian)
	*/
	public function dashboardwidgetmenu() {		
		$data=$this->Dashboardbuildermodel->dashboardwidgetmenu();
		$previd = ' ';
		$m=1;
		foreach($data->result() as $info){			
			$currentid = $info->widgetgroupid;				
			if($previd != $currentid)
			{
				if($previd != " ")
				{
					echo '</div>';
				}
				echo '<li class="fbuilderaccli fbuilderaccclick fortrigger'.$m.'" data-elementcat="'.$m.'" style="margin-top:0">
							<div class="large-6 medium-6 small-6 column">
								<label class="fbuilderaccheader">
									'.$info->widgetgroupname.'
								</label>
							</div>
							<div class="large-6 medium-6 small-6 column" style="text-align:right;line-height:2.2rem;">
								<span id="" class="toggleicons accplusbtn" title=""><i class="material-icons">account_circle</i></span>
							</div>
						</li>
						<div id="" class="bgcolorfbelements hidedisplay elementlist'.$m.' hideotherelements">';	
				$previd = $info->widgetgroupid;					
			}
			//?core-li
			echo '<li>
						<div class="predefinedwidget" draggable="true" id="'.$info->widgetname.'">
							<span class="captionstyle">'.$info->widgetlabel.'</span>
						</div>
				 </li>';
			$m++;
		}
	}
	/*
	*
	*/
	public function regeneratedraggable() {
		$this->Dashboardbuildermodel->regeneratedraggable();
	}
	/*
	* Retrieves the related moduleid list of the given module 
	*/
	public function relatedmodulelistfetchmodel() {
		$this->Dashboardbuildermodel->relatedmodulelistfetchmodel();
	}
	/*
	* Retrieves the  
	*/
	public function customwidgetdatafield() {
		$this->Dashboardbuildermodel->customwidgetdatafield();
	}
	//data field data fetch
	public function datafielddatafetch() {
		$this->Dashboardbuildermodel->datafielddatafetchmodel();
	}
	//dashboard builder edit data fetch
	public function dashboardbuildereditdatafetch() {
		$this->Dashboardbuildermodel->dashboardbuildereditdatafetchmodel();
	}
	//dashboard row data fetch
	public function dashboardbuilderrowdatafetch() {
		$this->Dashboardbuildermodel->dashboardbuilderrowdatafetchmodel();
	}
	//dashboard widget data fetch
	public function dashboardbuilderwidgetdatafetch() {
		$this->Dashboardbuildermodel->dashboardbuilderwidgetdatafetchmodel();
	}
}