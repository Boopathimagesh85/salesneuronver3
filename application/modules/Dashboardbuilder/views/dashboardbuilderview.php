<!DOCTYPE HTML>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
	<style>
		#leftsidepanel .select2-container .select2-choice{font-size:0.8rem !important}
		#draganddropcontainer li{list-style-type: none}
		.fbuilderleftcation i {
		    position: relative;
		    top: 12px;
		}
    </style>
</head>
<body class="hidedisplay">
	<div class="row" style="max-width:100%">
		<div class="off-canvas-wrap" data-offcanvas>
			<div class="inner-wrap add-custom-css-for-builder">
				<div class="large-9 medium-8 column" id="leftsidepanel" style="background-color:#eceff1;">
					<div class="large-12 column fbuilderleftcation" style="height:51px; line-height:46px;">
						<span class="gridcaptionpos" style="top:-0.2rem;position:relative;">
						<span><span class="titlehide">Dashboard &nbsp;</span>Builder</span>
						</span>
					</div>
					<div class="scrollbarclass dropareadiv" style="overflow-y:scroll;height:41rem;position:relative;width:100%;background-color:#f5f5f5;">
						<form name="newmodulegenform" id="newmodulegenform" method="POST" class="large-12 column paddingzero">
							<span ><ul id="spanforaddtab" class="dbuilderul"> </ul>
							</span>
						</form>
					</div>
				</div>
				<div class="large-3 medium-4 column"  id="draganddropcontainer" style="" class='draganddropcontainer'>
					<div class="large-12 column fbuilderleftcation  paddingzero" style="height:50px;">
						<div class="large-4 medium-4 column">
							<span id="previewcaption" class="hidedisplay" style="top:16px;position:relative;">Preview </span>
						</div>
						<div class="large-8 medium-8 column foriconcolor mainaction" style="text-align:right; float: right;"> 
							<span id="dbsupporticon" class="icon-box"><i class="icon-w icon-question"></i></span>
							<span id="dbdatasavebtn" name="dbdatasavebtn" title="Save" class="icon-box"><i class="material-icons">save</i><span class="actiontitle">Save</span></span>
							<span id="dbdataupdateicon" name="dbdataupdateicon" title="Update" class="icon-box"><i class="material-icons">save</i><span class="actiontitle">Save</span></span>
							<span id="normalviewpreview" title="Full View" class="icon-box"><i class="material-icons">visibility</i><span class="actiontitle">Preview</span></span>
							<span id="fullviewpreview" title="Full View" class="icon-box"><i class="material-icons">visibility</i><span class="actiontitle">Preview</span></span>
							<span id=dashboardclose class="icon-box" title="Save"><i class="material-icons">close</i><span class="actiontitle">Close</span></span>
						</div>
					</div>
					<div class="large-12 columns tabgroupstyle">
						<ul class="tabs fbuildertab fortabicons" data-tab="">
							<li class="tab-title active" id="settingstabid">
								<span class="link-for-tab"><a href="#">Module</a></span>
							</li>
							<li class="tab-title" id="widgettabid">
								<span class="link-for-tab"><a href="#">Widgets</a></span>
							</li>
							<li class="tab-title" id="propertiestabid">
								<span class="link-for-tab"><a href="#">Settings</a></span>
							</li>
							<span id="blockspan" class="blockspan"></span>
						</ul>
					</div>
					<div class="large-12 column steppervertical"  id="dbuildercontainer1" style="padding:0">
						<form name="dbdatainfoform" id="dbdatainfoform">
							<span id="dashboarddatainfoformvalidate" class="validationEngineContainer dbdatainfo clearmainform">
							<span id="dashboardudatedatainfoformvalidate" class="validationEngineContainer dbdatainfo clearmainform">
								<ul class="dbuildersettings">
									<li>
										<div class="static-field large-12 columns">
											<label>Module Name<span class="mandatoryfildclass">*</span></label>
											<select class="chzn-select validate[required]" data-prompt-position="topLeft:14,36" id="modulename" name="modulename">
												<option value="1">Home</option>
												<?php
													foreach($moduledata as $key):
												?>
													<option value="<?php echo $key->moduleid;?>" data-parenttable="<?php echo $key->modulemastertable;?>" ><?php echo $key->menuname;?></option>
												<?php endforeach;?>
											</select>
										</div>
									</li>
									<li>
										<div class="input-field large-12 columns">
											<input type="text" id="dashboardname" name="dashboardname" class="validate[required,funcCall[dashboardnamecheck]]" value=""></input>
											<label for="dashboardname">Dashboard Name<span class="mandatoryfildclass">*</span></label>
										</div>
									</li>
									<li>
										<div class="static-field large-12 columns">
											<label>Folder Name<span class="mandatoryfildclass">*</span></label>
											<select class="chzn-select validate[required]" data-prompt-position="topLeft:14,36"  id="dbfoldername" name="dbfoldername">
												<option></option>
												<?php
													foreach($dashboardfolder as $key):
												?>
												<option value="<?php echo $key->dashboardfolderid;?>" ><?php echo $key->dashboardfoldername;?></option>
												<?php endforeach;?>
											</select>
										</div>
									</li>
									<li>
										<div class="input-field large-12 columns">
											<input type="text" id="dbdescription" name="dbdescription" value=""></input>
											<label for="dbdescription">Description</label>
										</div>
									</li>
									<li style="margin-top: 1.5rem;">
									    <span style="margin-right: 3.5rem; display:none;" id="minidashboardcheckspan">
											<input type="checkbox" class="filled-in validate[]" id="dbsetmini" name="dbsetmini" value="No"></input><label for="dbsetmini"><span style="margin-left: 0.2rem;"> Mini Dashboard</span></label>
											<input type="hidden" name="dbsetminihidden" id="dbsetminihidden" value="No" />
										</span>
										 <span class="input-field" style="margin-right: 3.5rem; display:none;" id="miniwidgetcountspan">
											<input type="text" class="" id="miniwidgetcount" name="miniwidgetcount" value=""></input>
											<label for="miniwidgetcount">Widget Count<span class="mandatoryfildclass">*</span></label>
										    <input type="hidden" name="miniwidgetcounthidden" id="miniwidgetcounthidden" value="" />
										</span>  
										<span style="margin-right: 3.5rem;padding-left: 0.9rem;">
											<input type="checkbox" class="filled-in validate[]" id="dbsetdefault" name="dbsetdefault" value="No"></input><label for="dbsetdefault"><span style="margin-left: 0.2rem;"> Default</span></label>
											<input type="hidden" name="dbsetdefaulthidden" id="dbsetdefaulthidden" value="No" />
										</span>
										<span style="padding-left: 0.9rem;">
											<input type="checkbox" class="filled-in validate[]" id="dbsetactive" name="dbsetactive" value="Yes" checked="checked" ></input><label for="dbsetactive"><span style="margin-left: 0.2rem;">Active</span></label>
											<input type="hidden" name="dbsetactivehidden" id="dbsetactivehidden" value="Yes" />
										</span>
									</li>
									<input id='dashboardid' type='hidden' name='dashboardid' />
									<input id='dashboardrowid' type='hidden' name='dashboardrowid' />
								</ul>
							</span>
							</span>
						</form>
					</div>
					<div class="large-12 column hidedisplay scrollbarclass steppervertical steppervertical1" id="dbuildercontainer2"  style="padding:0;height:36rem;overflow-y:auto;overflow-x:hidden;">
						<div id="fullwidget" class=''>
						<ul class="dbuildersettings">
							<li>
								<label style="padding-left: 15px;">Layout</label>
								<ul>
								<li id="addonecolumn" style="font-size:1rem;padding-right:1rem;width:4rem;float:left;text-align:center;cursor:pointer;line-height:1;" class="layout-bg"> <span style="display:block;"><i class="material-icons">view_array</i></span>One</li>
								<li id="addtwocolumn" class="layout-bg" style="font-size:1rem;padding-right:1rem;width:5rem;float:left;text-align:center;cursor:pointer;line-height:1;" ><span style="display:block;"><i class="material-icons">view_compact</i></span> Two</li>
								<li id="addthreecolumn" class="layout-bg" style="font-size:1rem;padding-right:1rem;width:5rem;float:left;text-align:center;cursor:pointer;line-height:1;"><span style="display:block;"><i class="material-icons">view_column</i></span>Three</li>
								</ul>
							</li><br>
							<li>
								<div class="static-field large-12 columns">
									<label>&nbsp;</label>
									<select class="chzn-select" id="selecttype"> 
										<option value="1">Select Widgets</option>
										<option value="2">Create Chart Widgets</option>
									</select>
								</div>
							</li>
							<li>
								<div class="input-field large-12 columns">
									<input type="text"  id="dashboardwidgetfilter" name=""> </input>
									<label for="dashboardwidgetfilter">Filter</label>
									<span id="appendwidgetvalue"></span>
								</div>
							</li>
							<li>
								<label>&nbsp;</label>
							</li>
						</ul>
						<ul class="dbuildersettings dashboardwidgetslist forborderbottomstyle search-list-widgets" style="margin:0;padding:0px 1.99rem" id="existingchart">
							<li class="fbuilderaccli fbuilderaccclick fortrigger1" data-elementcat="1" style="margin-top:0;">
								<div class="large-8 medium-8 small-6 column">
									<label class="fbuilderaccheader">
										Marketing & Sales Widgets
									</label>
								</div>
								<div class="large-4 medium-4 small-6 column" style="text-align:right; line-height:2.2rem;">
									<span id="" class="toggleicons material-icons accplusbtn" title=""> add </span>
								</div>
							</li>
							<div id="" class="bgcolorfbelements hidedisplay elementlist1 hideotherelements">
								<li>
									<div class="predefinedwidget" draggable="true" id="ibproductwidget">
										<span class="captionstyle"> Product List </span>
									</div>
								</li>
								<li>
									<div class="predefinedwidget" draggable="true" id="ibpricebookwidget">
										<span class="captionstyle"> PriceBook List </span>
									</div>
								</li>
								<li>
									<div class="predefinedwidget" draggable="true" id="ibaccountswidget">
										<span class="captionstyle"> Accounts List </span>
									</div>
								</li>
								<li>
									<div class="predefinedwidget" draggable="true" id="ibcontactswidget">
										<span class="captionstyle"> Contacts </span>
									</div>
								</li>
								<li>
									<div class="predefinedwidget" draggable="true" id="ibdocumentswidget">
										<span class="captionstyle"> Documents List </span>
									</div>
								</li>
								<li>
									<div class="predefinedwidget" draggable="true" id="ibactivitieswidget">
										<span class="captionstyle"> Activities List </span>
									</div>
								</li>
								<li>
									<div class="predefinedwidget" draggable="true" id="ibtaskwidget">
										<span class="captionstyle"> Task List </span>
									</div>
								<li>
								<li>
									<div class="predefinedwidget" draggable="true" id="ibcalendarwidget">
										<span class="captionstyle"> Calendar </span>
									</div>
								</li>
								<li>
									<div class="predefinedwidget" draggable="true" id="ibcampaignwidget">
										<span class="captionstyle"> Campaign List </span>
									</div>
								</li>
								<li>
									<div class="predefinedwidget" draggable="true" id="ibleadwidget">
										<span class="captionstyle"> Lead </span>
									</div>
								</li>
								<li>
									<div class="predefinedwidget" draggable="true" id="ibleadstatuswidget">
										<span class="captionstyle"> Lead Status </span>
									</div>
								</li>
								<li>
									<div class="predefinedwidget" draggable="true" id="ibopportunitywidget">
										<span class="captionstyle"> Opportunity </span>
									</div>
								</li>
								<li>
									<div class="predefinedwidget" draggable="true" id="ibquotewidget">
										<span class="captionstyle"> Quote </span>
									</div>
								</li>
								<li>
									<div class="predefinedwidget" draggable="true" id="ibinvoicewidget">
										<span class="captionstyle"> Invoice </span>
									</div>
								</li>																
							</div>
							<li class="fbuilderaccli fbuilderaccclick fortrigger2" data-elementcat="2">
								<div class="large-6 medium-6 small-6 column">
									<label class="fbuilderaccheader">
										Support Widgets
									</label>
								</div>
								<div class="large-6 medium-6 small-6 column" style="text-align:right; line-height:2.2rem;">
									<span id="" class="toggleicons material-icons accplusbtn" title=""> add </span>
								</div>
							</li>
							<div id="" class="bgcolorfbelements hidedisplay elementlist2 hideotherelements">
								
								<li>
									<div class="predefinedwidget" draggable="true" id="ibticketswidget">
										<span class="captionstyle"> Tickets List </span>
									</div>
								</li>
								<li>
									<div class="predefinedwidget" draggable="true" id="ibsolutioncatwidget">
										<span class="captionstyle"> Solutions Category</span>
									</div>
								</li>
								<li>
									<div class="predefinedwidget" draggable="true" id="ibsolutionwidget">
										<span class="captionstyle"> Solutions </span>
									</div>
								</li>
								<li>
									<div class="predefinedwidget" draggable="true" id="ibcontractwidget">
										<span class="captionstyle"> Contract</span>
									</div>
								</li>
							</div>
							<li class="fbuilderaccli fbuilderaccclick fortrigger3" data-elementcat="3">
								<div class="large-6 medium-6 small-6 column">
									<label class="fbuilderaccheader">
										Inventory Widgets
									</label>
								</div>
								<div class="large-6 medium-6 small-6 column" style="text-align:right; line-height:2.2rem;">
									<span id="" class="toggleicons material-icons accplusbtn" title=""> add </span>
								</div>
							</li>
							<div id="" class="bgcolorfbelements hidedisplay elementlist3 hideotherelements">				
								<li>
									<div class="predefinedwidget" draggable="true" id="ibpurchaseorderwidget">
										<span class="captionstyle"> Purchase Order </span>
									</div>
								</li>
								<li>
									<div class="predefinedwidget" draggable="true" id="ibsalesorderwidget">
										<span class="captionstyle"> Sales Order </span>
									</div>
								</li>
							</div>
							<li class="fbuilderaccli fbuilderaccclick fortrigger4" data-elementcat="4">
								<div class="large-6 medium-6 small-6 column">
									<label class="fbuilderaccheader">
										Other Widgets
									</label>
								</div>
								<div class="large-6 medium-6 small-6 column" style="text-align:right; line-height:2.2rem;">
									<span id="" class="toggleicons material-icons accplusbtn" title=""> add </span>
								</div>
							</li>
							<div id="" class="bgcolorfbelements hidedisplay elementlist4 hideotherelements">
								<li>
									<div class="predefinedwidget" draggable="true" id="ibmaillogwidget">
										<span class="captionstyle" style="left: 0rem;"> Mail Log </span>
									</div>
								</li>
								<li>
									<div class="predefinedwidget" draggable="true" id="ibnotificationwidget">
										<span class="captionstyle"> Notification List </span>
									</div>
								</li>
								<li>
									<div class="predefinedwidget" draggable="true" id="ibconversationwidget">
										<span class="captionstyle"> Conversation </span>
									</div>
								</li>
								
								<li>
									<div class="predefinedwidget" draggable="true" id="ibtimeanddatewidget">
										<span class="captionstyle"> Time And Date </span>
									</div>
								</li>
								
							</div>
						</ul>
						<ul class="dbuildersettings hidedisplay forborderbottomstyle" style="" id="tocreatechart">
							<li class="fbuilderaccli fbuilderaccclick fortrigger1" data-elementcat="1" style="margin-top:0">
								<div class="large-6 medium-6 small-6 column">
									<label class="fbuilderaccheader">
										Pie Chart
									</label>
								</div>
								<div class="large-6 medium-6 small-6 column" style="text-align:right; line-height:2.2rem;">
									<span id="" class="toggleicons material-icons accplusbtn" title=""> add </span>
								</div>
							</li>
							<div id="" class="dbuilderchrttype bgcolorfbelements hidedisplay elementlist1 hideotherelements">
								<ul class="elementiconsul">
									<li> 
										<div class="charttitlecolor" draggable="true" id="piechart">
											<img draggable="false" src="<?php echo base_url();?>img/builders/pie/pie.png" height='50' width='50' />
											<div class="charttitlecolor"> Pie </div>
										</div>
									</li>
									<li>
										<div class="charttitlecolor" draggable="true" id="donutchart">
											<img draggable="false" src="<?php echo base_url();?>img/builders/pie/donut.png" height='50' width='50' />
											<div class="charttitlecolor"> Donut </div>
										</div>
									</li>
									  <li>
										<div class="charttitlecolor" draggable="true" id="funnelchart">
											<img  draggable="false" id="" src="<?php echo base_url();?>img/builders/pie/funnel.png" height='50' width='50' />
											<div class="charttitlecolor"> Funnel </div>
										</div>										
									</li>
									<li>
										<div class="charttitlecolor" draggable="true" id="pyramidchart">
											<img  draggable="false" src="<?php echo base_url();?>img/builders/pie/pyramid.png" height='50' width='50' />
											<div class="charttitlecolor"> Pyramid </div>	
										</div>										
									</li>
								</ul>
							</div>							
							<li class="fbuilderaccli fbuilderaccclick fortrigger2" data-elementcat="2">
								<div class="large-6 medium-6 small-6 column">
									<label class="fbuilderaccheader">
										Column Chart
									</label>
								</div>
								<div class="large-6 medium-6 small-6 column" style="text-align:right; line-height:2.2rem;">
									<span id="" class="toggleicons material-icons accplusbtn" title=""> add </span>
								</div>
							</li>
							<div id="" class="dbuilderchrttype bgcolorfbelements hidedisplay elementlist2 hideotherelements">
								<ul class="elementiconsul">
									<li> 
										<div class="charttitlecolor" draggable="true" id="columnchart">
											<img draggable="false" src="<?php echo base_url();?>img/builders/column/column.png" height='50' width='50' />
											<div class="charttitlecolor"> Column </div>
										</div>
									</li>									
									<li>
										<div class="charttitlecolor" draggable="true" id="stackedchart">
										<img draggable="false" src="<?php echo base_url();?>img/builders/column/stacked.png" height='50' width='50' />
										<div class="charttitlecolor"> Stacked </div>
										</div>
									</li> 
								</ul>
							</div>
							
						    <li class="fbuilderaccli fbuilderaccclick fortrigger3" data-elementcat="3">
								<div class="large-6 medium-6 small-6 column">
									<label class="fbuilderaccheader">
										Gauge Chart
									</label>
								</div>
								<div class="large-6 medium-6 small-6 column" style="text-align:right; line-height:2.2rem;">
									<span id="" class="toggleicons material-icons accplusbtn" title=""> add </span>
								</div>
							</li>
							<div id="" class="dbuilderchrttype bgcolorfbelements hidedisplay elementlist3 hideotherelements">
								<ul class="elementiconsul">
									<li> 
										<div class="charttitlecolor" draggable="true" id="anuglargaugechart">
											<img draggable="false" src="<?php echo base_url();?>img/builders/others/gauge.png" height='50' width='50' />
											<div class="charttitlecolor"> Gauge </div>
										</div>
									</li>
								</ul>
							</div>
							<li class="fbuilderaccli fbuilderaccclick fortrigger4" data-elementcat="4">
								<div class="large-6 medium-6 small-6 column">
									<label class="fbuilderaccheader">
										Bubbles Chart
									</label>
								</div>
								<div class="large-6 medium-6 small-6 column" style="text-align:right; line-height:2.2rem;">
									<span id="" class="toggleicons material-icons accplusbtn" title=""> add </span>
								</div>
							</li>
							<div id="" class="dbuilderchrttype bgcolorfbelements hidedisplay elementlist4 hideotherelements">
								<ul class="elementiconsul">
									<li> 
										<div class="charttitlecolor" draggable="true" id="bubblechart">
											<img draggable="false" src="<?php echo base_url();?>img/builders/others/bubble.png" height='50' width='50' />
											<div class="charttitlecolor"> Bubble </div>
										</div>
									</li>	
								</ul>
							</div>
							<li class="fbuilderaccli fbuilderaccclick fortrigger6" data-elementcat="6">
								<div class="large-6 medium-6 small-6 column">
									<label class="fbuilderaccheader">
										Area Chart
									</label>
								</div>
								<div class="large-6 medium-6 small-6 column" style="text-align:right; line-height:2.2rem;">
									<span id="" class="toggleicons material-icons accplusbtn" title=""> add </span>
								</div>
							</li>
							<div id="" class="dbuilderchrttype bgcolorfbelements hidedisplay elementlist6 hideotherelements">
								<ul class="elementiconsul">
									<li> 
										<div class="charttitlecolor" draggable="true" id="areachart">
											<img draggable="false" src="<?php echo base_url();?>img/builders/others/area.png" height='50' width='50' />
											<div class="charttitlecolor"> Area </div>
										</div>
									</li>	
								</ul>
							</div>
							<li class="fbuilderaccli fbuilderaccclick fortrigger7" data-elementcat="7">
								<div class="large-6 medium-6 small-6 column">
									<label class="fbuilderaccheader">
										Line Chart
									</label>
								</div>
								<div class="large-6 medium-6 small-6 column" style="text-align:right; line-height:2.2rem;">
									<span id="" class="toggleicons material-icons accplusbtn" title=""> add </span>
								</div>
							</li>
							<div id="" class="dbuilderchrttype bgcolorfbelements hidedisplay elementlist7 hideotherelements">
								<ul class="elementiconsul">
									<li> 
										<div class="charttitlecolor" draggable="true" id="linechart">
											<img draggable="false" src="<?php echo base_url();?>img/builders/others/line.png" height='50' width='50' />
											<div class="charttitlecolor"> Line </div>
										</div>
									</li>	
								</ul>
							</div>
						</ul>
						</div>
						<div id="miniwidget" class='hidedisplay'>
						<ul class="dbuildersettings">
						 <li>
								<div class="static-field large-12 columns">
									<label>&nbsp;</label>
									<select class="chzn-select" id="selecttypemini"> 
										<option value="1">Select Widgets</option>
										<option value="2">Create Chart Widgets</option>
									</select>
								</div>
							</li>
							<li>
								<div class="input-field large-12 columns">
									<input type="text"  id="dashboardwidgetfiltermini" name=""> </input>
									<label for="dashboardwidgetfiltermini">Filter</label>
									<span id="appendwidgetvaluemini"></span>
								</div>
							</li>
							<li>
								<label>&nbsp;</label>
							</li>
						</ul>
							<ul class="dbuildersettings dashboardwidgetslist forborderbottomstyle search-list-widgets" style="margin:0;padding:0" id="existingchart">
							<li class="fbuilderaccli fbuilderaccclick fortrigger1" data-elementcat="1" style="margin-top:0">
								<div class="large-8 medium-8 small-6 column">
									<label class="fbuilderaccheader">
										Marketing & Sales Widgets
									</label>
								</div>
								<div class="large-4 medium-4 small-6 column" style="text-align:right; line-height:2.2rem;">
									<span id="" class="toggleicons icon24  icon24-plus-square-o accplusbtn" title=""> </span>
								</div>
							</li>
							<div id="" class="bgcolorfbelementsmini hidedisplay elementlist1 hideotherelements">
								<li>
									<div class="predefinedwidget" draggable="true" id="ibdocumentswidgetmini">
										<span class="captionstyle"> Documents List </span>
									</div>
								</li>
								<li>
									<div class="predefinedwidget" draggable="true" id="ibtaskwidgetmini">
										<span class="captionstyle"> Task List </span>
									</div>
								<li>
																							
							</div>
																			
							<li class="fbuilderaccli fbuilderaccclick fortrigger2" data-elementcat="2">
								<div class="large-6 medium-6 small-6 column">
									<label class="fbuilderaccheader">
										Other Widgets
									</label>
								</div>
								<div class="large-6 medium-6 small-6 column" style="text-align:right; line-height:2.2rem;">
									<span id="" class="toggleicons icon24  icon24-plus-square-o accplusbtn" title=""> </span>
								</div>
							</li>
							<div id="" class="bgcolorfbelementsmini hidedisplay elementlist2 hideotherelements">
							    <li>
									<div class="predefinedwidget" draggable="true" id="ibnotificationwidgetmini">
										<span class="captionstyle"> Notification List </span>
									</div>
								</li>
							</div>
						</ul>
				 	 </div>
					</div>
					<div class="large-12 column hidedisplay" id="dbuildercontainer3"  style="padding:0;overflow-y:scroll">
						<div id="chartsettings">
							<form name="" id="">
							<span id="individualchartvalidation" class="validationEngineContainer clearchartform">
								<ul class="dbuildersettings" style="">
								<li>
									<div class="input-field large-12 columns">
										<input type="text" id="chatytype" disabled value="" class="" name="chatytype"> </input>
										<label for="chatytype">Chart Type<span class="mandatoryfildclass">*</span></label>
										<input type="hidden" id="currentchartnum" disabled value="" name="currentchartnum"> </input>
									</div>
								</li>
								<li>
									<div class="input-field large-12 columns">
										<input type="text" id="charttitle" value="" class="validate[required]" name="charttitle"> </input>
										<label for="charttitle">Chart Title<span class="mandatoryfildclass">*</span></label>
									</div>
								</li>
								<li>
									<label>&nbsp;</label>
								</li>
								</ul>
								<div>						
								<ul class="dbuildersettings forborderbottomstyle" style="">
									<li class="fbuilderaccli chartfbuilderaccclick fortrigger1" data-elementcat="1">
										<div class="large-6 medium-6 small-6 column">
											<label class="fbuilderaccheader">
												Data Source
											</label>
										</div>
										<div class="large-6 medium-6 small-6 column" style="text-align:right; line-height:2.2rem;">
											<span id="" class="toggleicons icon24  icon24-plus-square-o accplusbtn" title=""> </span>
										</div>
									</li>
									<div class="bgcolorfbelements hidedisplay elementlist1 hideotherelements">						
									<li id="">
										<div class="static-field large-12 columns">
											<label>Source Report<span class="mandatoryfildclass">*</span></label>
											<select class="validate[required] chzn-select" data-prompt-position="topLeft:14,36" id="reportid" name="reportid"> 
												<option value=""></option>
												<?php foreach($sourcereport as $key):?>
												<option value="<?php echo $key->reportid;?>">
												<?php echo $key->reportname;?></option>
												<?php endforeach;?>										
											</select>
										</div>
									</li>
									<li id="">
										<div class="static-field large-12 columns">
											<label>Group By<span class="mandatoryfildclass">*</span></label>
											<select class="chzn-select" id="reportgroupbyid" data-prompt-position="topLeft:14,36" name="reportgroupbyid"> 								
												<option value=""></option>					
											</select>
											<input type="hidden" id="reportgroupbyidtext"/>
										</div>
									</li>
									<li id="chartsplitby">
										<div class="static-field large-12 columns">
											<label>Split By<span class="mandatoryfildclass">*</span></label>
											<select class="chzn-select" id="reportsplitbyid" name="reportsplitbyid"> 
												<option value=""></option>									
											</select>
											<input type="hidden" id="reportsplitbyidtext"/>
										</div>
									</li>									
									<li id="">
										<div class="input-field large-12 columns">
											<input type="checkbox" class="filled-in" id="recordcount" value="true" name="recordcount" checked="checked" /> 
											<label for="recordcount">Record Count</label>
										</div>
									</li>
									<li id="datafield" style="display:none;">
										<div class="static-field large-12 columns">
											<label>Data Field<span class="mandatoryfildclass star"></span></label>
											<span style="margin-right: 3.5rem;">
												<select class="chzn-select" id="aggregatefieldid" name="aggregatefieldid"> 
												<option value=""></option>								
												</select>
											</span>
										</div>
									</li>
									<li id="operationlabel" style="display:none;">
										<div class="static-field large-12 columns">
											<label>Operation <span class="mandatoryfildclass star"></span></label>
											<span>
												<select class="chzn-select" id="operation" name="operation"> 
												<option value=""></option>
												<option value="SUM">SUM</option>								
												<option value="AVG">AVG</option>								
												<option value="MIN">MIN</option>								
												<option value="MAX">MAX</option>	
												</select>
											</span>
										</div>
									</li>
									<li>
									<label>&nbsp;</label>
								   </li>
									</div>
									<li class="fbuilderaccli chartfbuilderaccclick fortrigger2" data-elementcat="2">
										<div class="large-6 medium-6 small-6 column">
											<label class="fbuilderaccheader">
												Chart Apperance
											</label>
										</div>
										<div class="large-6 medium-6 small-6 column" style="text-align:right; line-height:2.2rem;">
											<span id="" class="toggleicons icon24  icon24-plus-square-o accplusbtn" title=""> </span>
										</div>
									</li>
									<div class="bgcolorfbelements hidedisplay elementlist2 hideotherelements">
									<li id="attrchartview">
										<div class="input-field large-12 columns">
											<input type="checkbox" class="filled-in" id="chartview" value="false" name="chartview"> </input>
											<label for="chartview">3D View</label>
										</div>
									</li>
									<li id="selectmodelabel">
										<div class="static-field large-12 columns">
											<label>Chart Mode</label>
											<select class="chzn-select" id="selectmode" name="selectmode"> 
												<option value=""></option>
												<option value="horizontal">Horizontal</option>
												<option value="vertical">Vertical</option>
											</select>
										</div>
									</li>
									<li id="selecttype">
										<div class="static-field large-12 columns">
											<label>Stack Type</label>
											<select class="chzn-select" id="stacktype" name="stacktype"> 
												<option value=""></option>
												<option value="normal">normal</option>
												<option value="100%">100%</option>
											</select>
										</div>
									</li>	
									<li id="chartaspectlabel">
										<div class="static-field large-12 columns">
											<label>Chart Aspect</label>
											<span style="margin-right: 3.5rem;">
											<select class="chzn-select" id="chartaspect" name="chartaspect"> 
												<option value=""></option>
											</select>
											</span>
										</div>
									</li>	
									<li id="attrchartangle">
										<div class="input-field large-12 columns">
											<input type="text" id="anglevalue" value="" name="anglevalue"> </input>
											<label for="anglevalue">Angle</label>
										</div>
									</li>
									<span id="attrchartleg">
									<li>
										<div class="input-field large-12 columns">
											<input type="checkbox" class="filled-in" id="legendtype" value="false" name="legendtype"> </input>
											<label for="legendtype">Legend</label>
										</div>
									</li>
									<li id="showifchecked" class="hidedisplay">
										<div class="static-field large-12 columns">
											<label>Legend style</label>
											<select class="chzn-select" id="legendstyle" name="legendstyle"> 
												<option value="circle">Circle</option>
												<option value="star5">Star5</option>
												<option value="inherit">Square</option>
											</select>
										</div>
									</li>
									</span>
									<li>
									<label>&nbsp;</label>
									</li>
									</div>
									<li>
										<label>&nbsp;</label>
										<input type="button" id="createchart" value="Create" class="btn" name=""> </input>
									</li>
								</ul>
								</div>
							</span>
							</form>
						</div>
						<div id="customwidgetsettings">
							<form name="" id="">
								<span id="customwidgetvalidation" class="validationEngineContainer">
									<ul class="dbuildersettings" style="">
									<li>
										<label>&nbsp;</label>
								  	</li>
									<li>
										<div class="input-field large-12 columns">
											<input type="text" id="currentmodulename" value="" name="" readonly> </input>
											<label for="currentmodulename">Module Name</label>
										</div>
									</li>
									<li>
										<div class="input-field large-12 columns">
											<input type="text" id="customwidgetname" value="" name="customwidgetname"> </input>
											<label for="customwidgetname">Widget Name</label>
										</div>
									</li>
									<li>
										<div class="static-field large-12 columns">
											<label>Related Module</label>
											<select class="chzn-select" id="customrelatedmodule" name="" multiple> 
												<option value=""></option>					
											</select>
										</div>
									</li>
									<li>
										<div class="static-field large-12 columns">
											<label>Data Fields<span class="mandatoryfildclass">* MAX(10 fields)</span></label>
											<select class="chzn-select validate[required]" id="customdatafields" name="" multiple> 
												<option value=""></option>				
											</select>
											<input type="hidden" id="relatedcolumnid" value="" name="relatedcolumnid"> </input>
										</div>
									</li>
									<li>
										<label>&nbsp;</label>
									</li>
									<li>
										<label>&nbsp;</label>
										<input type="button" id="customwidgetsubmit" value="Create" class="btn" name=""> </input>
									</li>
									</ul>					
								</span>
							</form>
						</div>
						<div id="commonwidget">
							<form name="" id="">
								<span id="commonwidgetvalidation" class="validationEngineContainer">
									<ul class="dbuildersettings" style="">
										<li>
											<label>&nbsp;</label>
									  	</li>
										<li>
											<div class="input-field large-12 columns">
												<input type="text" id="commonwidgetname" value="" name="commonwidgetname"> </input>
												<label for="commonwidgetname">Widget Name</label>
											</div>
										</li>
										<li>
											<input type="hidden" id="chartid" value="" name="chartid"> </input>
											<label>&nbsp;</label>
											<input type="button" id="commonwidgetsubmit" value="Change" class="btn" name=""> </input>
										</li>
									</ul>
								</span>
							</form>
						</div>
					</div>
				</div>
				<?php
					$this->load->view('Base/basedeleteformforcombainedmodules');
				?>
			</div>
		</div>
	</div>
</body>
	<?php $this->load->view('Base/bottomscript');
	$this->load->view('Base/overlay'); 
	$this->load->view('Base/modulelist');?>
		<!-- js Files -->	
	<script src="<?php echo base_url();?>js/Dashboardbuilder/dashboardbuilder.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/zingchart/zingchart.min.js" type="text/javascript"></script>
	<script>zingchart.MODULESDIR="js/plugins/zingchart/modules/";
	</script>
	<script src="<?php echo base_url();?>js/plugins/sortable.js" type="text/javascript"></script>
</html>