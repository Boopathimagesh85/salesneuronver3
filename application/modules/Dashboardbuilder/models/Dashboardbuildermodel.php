<?php
Class Dashboardbuildermodel extends CI_Model {
	public function __construct() {
		parent::__construct(); 
    }
	public function getmodulelist($moduleid) {
		$industryid = $this->Basefunctions->industryid;
		return $this->db->select('moduleid,modulename,menuname,modulemastertable')
					->from('module')
					->where('status',1)
					->where("FIND_IN_SET('240',module.moduleprivilegeid) >", 0)
					->where("FIND_IN_SET('$industryid',module.industryid) >", 0)
					->get()->result();
	}
	//default credential data
	public function defaultvalueget() {
		$defdata['createdate'] = date($this->Basefunctions->datef);
		$defdata['lastupdatedate'] = date($this->Basefunctions->datef);
		$defdata['createuserid'] = $this->Basefunctions->userid;
		$defdata['lastupdateuserid'] = $this->Basefunctions->userid;
		$defdata['status'] = 1;
		return $defdata;
	}
	//data board data create model
	public function dashboraddatabuildmodel() {
		$defdataarr = $this->defaultvalueget();
		$dashbrdname = $_POST['dashboardname'];
		$moduleid = $_POST['modulename'];
		$dashbrddesc = $_POST['dbdescription'];
		$dashbrdfldname = $_POST['dbfoldername'];
		$dashbrdactive = $_POST['dbsetactivehidden'];
		$dashbrddefault = $_POST['dbsetdefaulthidden'];
		if($_POST['dbsetminihidden'] == 'Yes'){
			$_POST['miniwidgetcount'] = $_POST['miniwidgetcount'];
		}else{
			$_POST['miniwidgetcount'] = 0;
		}
		//dash board data insertion
		$dashrow = $_POST['chartrowinfo'];
		$dashchart = $_POST['chartnameinfo'];
		$cdata = 'True';
		foreach($dashchart as $chartinfo) {
			$chartdata = explode(',',$chartinfo);
			if(count($chartdata) == '') {
				$cdata = 'False';
			} else if($chartdata[1] == 'pie') {
				if(count($chartdata) <= 12){
					$cdata = 'False';
				}
			} else if($chartdata[1] == 'donut') {
				if(count($chartdata) <= 12){
					$cdata = 'False';
				}
			} else if($chartdata[1] == 'pyramid') {
				if(count($chartdata) <= 11){
					$cdata = 'False';
				}
			} else if($chartdata[1] == 'funnel') {
				if(count($chartdata) <= 11){
					$cdata = 'False';
				}
			} else if($chartdata[1] == 'column') {
				if(count($chartdata) <= 12){
					$cdata = 'False';
				}
			} else if($chartdata[1] == 'stacked') {
				if(count($chartdata) < 10){
					$cdata = 'False';
				}
			} else if($chartdata[1] == 'bubble') {
				if(count($chartdata) < 10){
					$cdata = 'False';
				}
			} else if($chartdata[1] == 'angulargauge') {
				if(count($chartdata) < 10){
					$cdata = 'False';
				}
			}			
			else if($chartdata[1] == 'wordcloud') {
				if(count($chartdata) < 7){
					$cdata = 'False';
				}
			}
			else if($chartdata[1] == 'line') {
				if(count($chartdata) <= 12){
					$cdata = 'False';
				}
			}
			else if($chartdata[1] == 'area') {
				if(count($chartdata) <= 12){
					$cdata = 'False';
				}
			}
		}
	if($cdata != 'False') {
		//dashboard data insert
		$dashbrddata = array(
			'moduleid'=>$moduleid,
			'dashboardname'=>ucwords($dashbrdname),
			'dashboardfolderid'=>ucwords($dashbrdfldname),
			'description'=>ucwords($dashbrddesc),
			'setmini'=>$_POST['dbsetminihidden'],
			'miniwidgetcount'=>$_POST['miniwidgetcount'],
			'setdefault'=>$dashbrddefault,
			'active'=>$dashbrdactive,
			'industryid'=>$this->Basefunctions->industryid,
			'branchid'=>$this->Basefunctions->branchid
		);
		$newdata = array_merge($dashbrddata,$defdataarr);
		$this->db->insert( 'dashboard', $newdata );
		$dashboardid = $this->db->insert_id();
		//set default update
		$this->setdefaultupdate($moduleid,$dashbrddefault,$dashboardid);
		//notification entry (Audit log)
		$empid = $this->Basefunctions->logemployeeid;
		$notimsg = $this->Basefunctions->notificationtemplatedatafetch(240,$dashboardid,'dashboard','2');
		if($notimsg != '') {
			$this->Basefunctions->notificationcontentadd($dashboardid,'Added',$notimsg,1,240);
		}
		foreach($dashrow as $rowinfo) {
			$rowdata = explode(',',$rowinfo);
			//dashboard row insertion
			$dashbrdrowdata = array(
								'dashboardrowname'=>ucwords($rowdata[2]),
								'dashboardid'=>$dashboardid,
								'dashboardrowtype'=>$rowdata[1]
							);
			$newrowdata = array_merge($dashbrdrowdata,$defdataarr);
			$this->db->insert( 'dashboardrow', $newrowdata );
			$dashboardrowid = $this->db->insert_id();
			//chart data insertion
			$reportgroup=array();
			foreach($dashchart as $chartinfo) {
				$chartdata = explode(',',$chartinfo);
				$angle = 0;
				$condname = "";
				$condvalue = "";
				$count = "0";
				if( $rowdata[0] == $chartdata[0] ) {
					switch($chartdata[1]) {
						case 'pie' :
							//pie chart
							$title = $chartdata[3];
							$charttype = 'piechart';
							$viewtype = $chartdata[4];
							$angle = $chartdata[5];							
							$legend = $chartdata[6];
							$legendtype = $chartdata[7];
							$parenttable = 'source';
							$childtable = 'lead';
							$basecolor = '';
							$rotate = 'false';
							$theme = '';
							//chart-group-data
							$reportgroup=array('reportid'=>$chartdata[8],'groupfieldid'=>$chartdata[9],'calculationfieldid'=>$chartdata[11],'operation'=>$chartdata[12],'countstatus'=>$chartdata[10]);				
							break;
						case 'donut' :
							//Donut chart
							$title = $chartdata[3];
							$charttype = 'donutchart';
							$viewtype = $chartdata[4];
							$angle = $chartdata[5];							
							$legend = $chartdata[6];
							$legendtype = $chartdata[7];
							$parenttable = 'source';
							$childtable = 'lead';
							$basecolor = '';
							$rotate = 'false';
							$theme = '';
							//chart-group-data
							$reportgroup=array('reportid'=>$chartdata[8],'groupfieldid'=>$chartdata[9],'calculationfieldid'=>$chartdata[11],'operation'=>$chartdata[12],'countstatus'=>$chartdata[10]);				
							break;
						case 'pyramid' :
							//pyramid chart
							$title = $chartdata[3];
							$charttype = 'pyramidchart';
							$viewtype = 'false';
							$angle = 0;							
							$legend = $chartdata[5];
							$legendtype = $chartdata[6];
							$parenttable = 'source';
							$childtable = 'lead';
							$basecolor = '';
							$rotate = 'false';
							$theme = $chartdata[4];
							//chart-group-data
							$reportgroup=array('reportid'=>$chartdata[7],'groupfieldid'=>$chartdata[8],'calculationfieldid'=>$chartdata[10],'operation'=>$chartdata[11],'countstatus'=>$chartdata[9]);				
							break;
						case 'funnel' :
							//funnel chart
							$title = $chartdata[3];
							$charttype = 'funnelchart';
							$viewtype = 'false';
							$angle = 0;							
							$legend = $chartdata[5];
							$legendtype = $chartdata[6];
							$parenttable = 'source';
							$childtable = 'lead';
							$basecolor = '';
							$rotate = 'false';
							$theme = $chartdata[4];
							//chart-group-data
							$reportgroup=array('reportid'=>$chartdata[7],'groupfieldid'=>$chartdata[8],'calculationfieldid'=>$chartdata[10],'operation'=>$chartdata[11],'countstatus'=>$chartdata[9]);				
							break;
						#column and bar charts.
						case 'column' :
							//column chart
							$title = $chartdata[3];
							$charttype = 'columnchart';
							$viewtype = $chartdata[4];
							$angle = 0;							
							$legend = $chartdata[7];
							$legendtype = $chartdata[8];
							$parenttable = 'source';
							$childtable = 'lead';
							$basecolor = $chartdata[6];
							$rotate = 'false';
							$theme = $chartdata[5];
							//chart-group-data
							$reportgroup=array('reportid'=>$chartdata[9],'groupfieldid'=>$chartdata[10],'calculationfieldid'=>$chartdata[12],'operation'=>$chartdata[13],'countstatus'=>$chartdata[11]);				
							break;
						case 'wordcloud' :
							//wordcloud chart
							$viewtype = 'false';
							$charttype = 'wordcloudchart';
							$theme = '';
							$angle = 0;
							$title = $chartdata[3];
							$legend = 'false';							
							$legendtype = '';
							$basecolor = $chartdata[4];
							$rotate = 'false';
							$parenttable = 'source';
							$childtable = 'lead';
							//chart-group-data
							$reportgroup=array('reportid'=>$chartdata[5],'groupfieldid'=>$chartdata[6],'countstatus'=>$chartdata[7]);
							break;
						case 'area' :
							//area chart
							$title = $chartdata[3];
							$charttype = 'areachart';
							$viewtype = $chartdata[4];
							$angle = 0;							
							$legend = $chartdata[7];
							$legendtype = $chartdata[8];
							$parenttable = 'source';
							$childtable = 'lead';
							$basecolor = $chartdata[6];
							$rotate = 'false';
							$theme = $chartdata[5];
							//chart-group-data
							$reportgroup=array('reportid'=>$chartdata[9],'groupfieldid'=>$chartdata[10],'calculationfieldid'=>$chartdata[12],'operation'=>$chartdata[13],'countstatus'=>$chartdata[11]);				
							break;
						case 'line' :
							//line chart
							$title = $chartdata[3];
							$charttype = 'linechart';
							$viewtype = $chartdata[4];
							$angle = 0;							
							$legend = $chartdata[7];
							$legendtype = $chartdata[8];
							$parenttable = 'source';
							$childtable = 'lead';
							$basecolor = $chartdata[6];
							$rotate = 'false';
							$theme = $chartdata[5];
							//chart-group-data
							$reportgroup=array('reportid'=>$chartdata[9],'groupfieldid'=>$chartdata[10],'calculationfieldid'=>$chartdata[12],'operation'=>$chartdata[13],'countstatus'=>$chartdata[11]);				
							break;	
						case 'stacked' :
							//stacked chart
							$title = $chartdata[3];
							$charttype = 'stackedchart';
							$viewtype = 'false';
							$angle = 0;							
							$legend = $chartdata[6];
							$legendtype = $chartdata[7];
							$parenttable = 'source';
							$childtable = 'lead';
							$basecolor = '';
							$rotate = $chartdata[5];
							$theme = $chartdata[4];
							//chart-group-data
							$reportgroup=array('reportid'=>$chartdata[8],'groupfieldid'=>$chartdata[9],'calculationfieldid'=>$chartdata[12],'operation'=>$chartdata[13],'countstatus'=>$chartdata[11],'splitbyid'=>$chartdata[10]);				
							break;	
						case 'angulargauge' :
							//Gauge chart
							$title = $chartdata[3];
							$charttype = 'angulargaugechart';
							$viewtype = 'false';
							$angle = 0;							
							$legend = $chartdata[4];
							$legendtype = $chartdata[5];
							$parenttable = 'source';
							$childtable = 'lead';
							$basecolor = '';
							$rotate = 'false';
							$theme = '';
							//chart-group-data
							$reportgroup=array('reportid'=>$chartdata[6],'groupfieldid'=>$chartdata[7],'calculationfieldid'=>$chartdata[9],'operation'=>$chartdata[10],'countstatus'=>$chartdata[8]);				
							break;
						case 'bubble' :
							//bubble chart
							$title = $chartdata[3];
							$charttype = 'bubblechart';
							$viewtype = 'false';
							$theme = $chartdata[4];
							$angle = 0;							
							$legend = 'false';
							$legendtype = 'false';
							$basecolor = $chartdata[5];
							$rotate = 'false';
							$parenttable = 'source';
							$childtable = 'lead';
							//chart-group-data
							$reportgroup=array('reportid'=>$chartdata[6],'groupfieldid'=>$chartdata[7],'calculationfieldid'=>$chartdata[10],'operation'=>$chartdata[11],'countstatus'=>$chartdata[9],'splitbyid'=>$chartdata[8]);
							break;
						case 'ibtaskwidget':
							//task widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibtaskwidgetmini':
							//task widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibdocumentswidget':
							//Document widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibdocumentswidgetmini':
							//Document widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibmaillogwidget':
							//Mail log widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[9])) {
								$title = $chartdata[9];
							} else {
								$title = 'Mail Log';
							}
							$parenttable = '';
							$legend = '';
							$childtable = '';
							$legendtype = '';
							$condname = '';
							$condvalue = '';
							$count = 0;
							break;
						case 'ibsendsmslogwidget':
							//Quick Send SMS widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[2])) {
								$title = $chartdata[2];
							} else {
								$title = 'Quick SMS';
							}
							$parenttable = '';
							$legend = '';
							$childtable = '';
							$legendtype = '';
							$condname = '';
							$condvalue = '';
							$count = 0;
							break;
						case 'ibsendsmslogwidget':
							//Quick Send SMS widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[9])) {
								$title = $chartdata[9];
							} else {
								$title = 'Quick SMS';
							}
							$parenttable = '';
							$legend = '';
							$childtable = '';
							$legendtype = '';
							$condname = '';
							$condvalue = '';
							$count = 0;
							break;
						case 'ibclicktocallwidget':
							//Click To Call widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[2])) {
								$title = $chartdata[2];
							} else {
								$title = 'Click To Call';
							}
							$parenttable = '';
							$legend = '';
							$childtable = '';
							$legendtype = '';
							$condname = '';
							$condvalue = '';
							$count = 0;
							break;
						case 'ibsmslogwidget':
							//SMS log widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[9])) {
								$title = $chartdata[9];
							} else {
								$title = 'SMS Log';
							}
							$parenttable = '';
							$legend = '';
							$childtable = '';
							$legendtype = '';
							$condname = '';
							$condvalue = '';
							$count = 0;
							break;
						case 'ibreceivesmslogwidget':
							//Receive SMS log widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[9])) {
								$title = $chartdata[9];
							} else {
								$title = 'Receive SMS Log';
							}
							$parenttable = '';
							$legend = '';
							$childtable = '';
							$legendtype = '';
							$condname = '';
							$condvalue = '';
							$count = 0;
							break;
						case 'ibactivitieswidget':
							//Activity widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibinboundcalllogwidget':
							//inbound widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = 'Inbound Call Log';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'iboutboundcalllogwidget':
							//outbound widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = 'Outbound Call Log';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibopportunitywidget':
							//Opportunities widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibcontactswidget':
							//Contacts widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibinvoicewidget':
							//Invoices widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibpaymentwidget':
							//Payment widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibquotewidget':
							//Quotes widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibaccountswidget':
							//Account widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibticketswidget':
							//Tickets widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibcampaignwidget':
							//Campaign widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibsalesorderwidget':
							//Sales Order widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibpurchaseorderwidget':
							//Sales Order widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibmaterialrequisitionwidget':
							//Sales Order widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibsolutionwidget':
							//Solutions widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibsolutioncatwidget':
							//Solutions Category widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibcontractwidget':
							//Contracts widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibleadwidget':
							//Leads widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibproductwidget':
							//Leads widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibpricebookwidget':
							//Leads widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibconversationwidget':
							//Solutions Category widget 
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[2])) {
								$title = $chartdata[2];
							} else {
								$title = 'Conversation';
							}
							$parenttable = '';
							$legend = '';
							$childtable ='';
							$legendtype = '';
							$count = 0;
							break;
						case 'ibconversationwidgetmini':
							//Solutions Category widget mini
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[2])) {
								$title = $chartdata[2];
							} else {
								$title = 'Conversation';
							}
							$parenttable = '';
							$legend = '';
							$childtable ='';
							$legendtype = '';
							$count = 0;
							break;
						case 'ibnotificationwidget':
							//Solutions Category widget ibnotificationwidget ibleadstatuswidget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[2])) {
								$title = $chartdata[2];
							} else {
								$title = 'Notifications';
							}
							$parenttable = '';
							$legend = '';
							$childtable ='';
							$legendtype = '';
							$count = 0;
							break;
						case 'ibnotificationwidgetmini':
							//Solutions Category widget ibnotificationwidget ibleadstatuswidget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[2])) {
								$title = $chartdata[2];
							} else {
								$title = 'Notifications';
							}
							$parenttable = '';
							$legend = '';
							$childtable ='';
							$legendtype = '';
							$count = 0;
							break;
						case 'ibleadstatuswidget':
							//lead status based widget-
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[2])) {
								$title = $chartdata[2];
							} else {
								$title = 'Lead Status';
							}
							$parenttable = '';
							$legend = '';
							$childtable ='';
							$legendtype = '';
							$count = 0;
							break;
						case 'ibcustomwidget':
							$titlename = $chartdata[2];
							if($chartdata[2] == 'undefined' OR $chartdata[2] =='' OR $chartdata[2] == null){
								$titlename = 'CustomWidget';
							}
							//custom widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							$title = $titlename;
							$parenttable = $chartdata[3];
							$legend = $chartdata[4];
							$childtable = '';
							$legendtype = '';
							$count = 0;
							break;
						case 'ibtimeanddatewidget':
							//lead status based widget-
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[2])) {
								$title = $chartdata[2];
							} else {
								$title = 'Time And Date';
							}
							$parenttable = '';
							$legend = '';
							$childtable ='';
							$legendtype = '';
							$count = 0;
							break;
						case 'ibcalendarwidget':
							//lead status based widget-
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[2])) {
								$title = $chartdata[2];
							} else {
								$title = 'Calendar';
							}
							$parenttable = '';
							$legend = '';
							$childtable ='';
							$legendtype = '';
							$count = 0;
							break;						
						default :
							$viewtype = 0;
							$charttype = '';
							$theme = '';
							$angle = '';
							$title = '';
							$legend = '';
							$legendtype = '';
							$basecolor = '';
							$rotate = '';
							$parenttable = '';
							$childtable = '';	
							break;
					}
					//chart data insertion
					$dashchartdata = array(
						'dashboardrowid'=>$dashboardrowid,
						'dashboardfieldtype'=>$charttype,
						'dashboardfiledname'=>$title,
						'fieldviewtype'=>$viewtype,
						'fieldtheme'=>$theme,
						'fieldrotate'=>$rotate,
						'fieldbasecolor'=>$basecolor,
						'fieldangle'=>$angle,
						'fieldlegend'=>$legend,
						'fieldlegendtype'=>$legendtype,
						'fieldparenttablename'=>$parenttable,
						'fieldchildtablename'=>$childtable,
						'fieldconditionname'=>$condname,
						'fieldconditionvalue'=>$condvalue,
						'fieldrowcount'=>$count,									
						'status'=>1
					);
					$newchartdata=array_merge($dashchartdata,array_filter($reportgroup));
					$this->db->insert('dashboardfield',$newchartdata);
				}
			}
		}
		echo 'Success';
	}else {
		echo 'Fail';
	}
	}
	
	/**
	Loads the summary report name to the source report fields
	*/
	public function loadsummaryreportname()
	{
		$data=$this->db->select('reportname,reportid')
					->from('report')
					->where('reporttypeid',3)
					->where('status',$this->Basefunctions->activestatus)
					->get()->result();
		return $data;
	}
	/**
	Load the source report groupind dropdown options
	*/
	public function loadgroupdropdowns() {
		$reportid=trim($_GET['id']);		
		$group=array();
		$calc=array();
		$g=0;
		$c=0;
		//report moduleid
		$data=$this->db->select('moduleid')->from('report')->where('reportid',$reportid)->get()->result();
		foreach($data as $in){
			$moduleid = $in->moduleid;
		}
		//fetch the clauses
		$groupfetchdata=$this->db->select('report.groupbyviewcreationcolumnid,report.sortby')
						->from('report')
						->where('report.reportid',$reportid)
						->get();
		if($groupfetchdata->num_rows() > 0) {
			foreach($groupfetchdata->result() as $groupinfo) {
				$groupid = $groupinfo->groupbyviewcreationcolumnid;
				$agroupid = explode(",",$groupid);
			}
		}
		$fetchdata=$this->db->select('viewcreationcolumns.viewcreationcolumnname,viewcreationcolumns.viewcreationcolumnid')
					->from('viewcreationcolumns')
					->where_in('viewcreationcolumns.viewcreationcolumnid',$agroupid)
					->get();
		$checkunique=array();
		if($fetchdata->num_rows() > 0)
		{			
			foreach($fetchdata->result() as $info)
			{			
				if(in_array($info->viewcreationcolumnid,$checkunique)){
				}
				else{
				$group[]=array('dataname'=>$info->viewcreationcolumnname,'datasid'=>$info->viewcreationcolumnid);	
				$checkunique[]=$info->viewcreationcolumnid;
				}
			}			
		}	
	    /* since report calcualtion column does not exits we have removed
		$datam=$this->db->select('reportcalculation.viewcreationcolumnid,viewcreationcolumns.viewcreationcolmodelname,aggregatemethod,viewcreationcolmodeltable')
						->from('reportcalculation')
						->join('viewcreationcolumns','viewcreationcolumns.viewcreationcolumnid=reportcalculation.viewcreationcolumnid')
						->where('reportcalculation.reportid',$reportid)
						->get();
		$colmtemparray=array();
		$calc=array();	
		if($datam->num_rows() > 0)
		{
			foreach($datam->result() as $info)
			{	
				if(in_array($info->viewcreationcolumnid,$colmtemparray)){
				}
				else{
				$colmtemparray[$c]=$info->viewcreationcolumnid;
				$calc[$c]=array('dataname'=>$info->viewcreationcolmodelname,'datasid'=>$info->viewcreationcolumnid);
				$c++;
				}
			}					
		} */		
	
		echo json_encode (array('group'=>$group,'calculation'=>$calc));
	}
	public function loadreportlist()
	{
		$group =array();
		$g=0;
		$moduleid=trim($_GET['moduleid']);
		$data=$this->db->select('reportname,reportid');
		$this->db->from('report');
		$this->db->where_in('reporttypeid',array(2,3));//3-summary report type
		$this->db->where('status',1);
		if($moduleid == 1){			
		}
		else{
			$this->db->where('moduleid',$moduleid);
		}
		$this->db->order_by('reportname','asc');
		$data=$this->db->get();
		//group by 
		if($data->num_rows() > 0)
		{			
			foreach($data->result() as $info)
			{			
				$group[$g]=array('dataname'=>$info->reportname,'datasid'=>$info->reportid);
				$g++;
			}			
		}
		echo json_encode($group);
	}
	/*
	* Dashboard Data Filter Menu(accordian)
	*/
	public function dashboardwidgetmenu(){
		$moduleid=trim($_POST['moduleid']);
		if($moduleid == 1){
			$wh = '1=1';
			$data = $this->db->query("
								SELECT widget.widgetname,widget.widgetid,widget.widgetgroupid,widgetgroup.widgetgroupname,widget.widgetlabel
								FROM  widget
								JOIN  widgetgroup on widgetgroup.widgetgroupid=widget.widgetgroupid
								WHERE ".$wh." AND widget.widgetgroupid NOT IN (1) AND widget.status = 1
								ORDER BY widget.widgetgroupid ASC								
								");
		}else if($moduleid > 1){
			$wh = 'relatedwidget.moduleid = '.$moduleid.'';
			$data = $this->db->query("
								SELECT widget.widgetname,widget.widgetid,widget.widgetgroupid,widgetgroup.widgetgroupname,widget.widgetlabel
								FROM  widget
								JOIN  widgetgroup on widgetgroup.widgetgroupid=widget.widgetgroupid
								INNER JOIN  relatedwidget on relatedwidget.widgetid=widget.widgetid
								WHERE ".$wh." AND widget.widgetgroupid NOT IN (1) AND widget.status = 1
								ORDER BY widget.widgetgroupid ASC								
								");
		}
		
		return $data;
	}
	public function regeneratedraggable(){
		$moduleid=trim($_POST['moduleid']);
		if($moduleid == 1){
			$wh = '1=1';
			$data = $this->db->query("
								SELECT widget.widgetname,widget.widgetid,widget.widgetgroupid,widgetgroup.widgetgroupname,widget.widgetlabel
								FROM  widget
								JOIN  widgetgroup on widgetgroup.widgetgroupid=widget.widgetgroupid
								WHERE ".$wh." AND widget.widgetgroupid NOT IN (1) AND widget.status = 1
								ORDER BY widget.widgetgroupid ASC								
								");
		}else if($moduleid > 1){
			$wh = 'relatedwidget.moduleid = '.$moduleid.'';
			$data = $this->db->query("
								SELECT widget.widgetname,widget.widgetid,widget.widgetgroupid,widgetgroup.widgetgroupname,widget.widgetlabel
								FROM  widget
								JOIN  widgetgroup on widgetgroup.widgetgroupid=widget.widgetgroupid
								INNER JOIN  relatedwidget on relatedwidget.widgetid=widget.widgetid
								WHERE ".$wh." AND widget.widgetgroupid NOT IN (1) AND widget.status = 1
								ORDER BY widget.widgetgroupid ASC								
								");
		}
		foreach($data->result() as $info){
			$widgetsarray[]=$info->widgetname;
		}
		echo json_encode($widgetsarray);
	}
	/*
	*Retrieves the related moduled list of the given module-refered from printtemplates 
	*/
	public function relatedmodulelistfetchmodel() {
		$moddatas = array();
		$relmodids = array();
		$moduleid = trim($_POST['moduleid']);		
		$relmoduleids = $this->db->select('modulerelationid,relatedmoduleid')->from('modulerelation')->where('moduleid',$moduleid)->where('status',1)->get();
		foreach($relmoduleids->result() as $reldata) {
			$relmodids[] = $reldata->relatedmoduleid;
		}
		$i=0;
		$modids = implode(',',$relmodids);
		$moduleids = ( ($modids!="")? $modids.','.$moduleid : $moduleid );
		$modids = explode(',',$moduleids);		
		$allmoduleids = implode(',',array_unique($modids));
		$genmod = array();
		$modinfo = $this->db->query('select module.moduleid,module.modulename from module join moduleinfo ON moduleinfo.moduleid=module.moduleid where module.moduleid IN ('.$allmoduleids.') AND module.status=1 AND moduleinfo.status=1 group by moduleinfo.moduleid',false);
		foreach($modinfo->result() as $moddata) {
			$modtype = ( ($moddata->moduleid==$moduleid)?'':((in_array($moddata->moduleid,$genmod))?'GEN:':'REL:') );
			$moddatas[$i] = array('datasid'=>$moddata->moduleid,'dataname'=>$moddata->modulename,'datatype'=>$modtype);
			$i++;
		}
		$datas = (($i==0)? json_encode(array('fail'=>'FAILED')) : json_encode($moddatas));
		echo $datas;
	}
	public function customwidgetdatafield(){
		$i=0;
		$j=0;
		$h=0;
		$jj=0;
		$salstatus = array(1,10);
		$tablname='';
		$salutable=array('lead','leadaddress','leadcf','contact','contactaddress','contactcf','employee','employeeaddress','employeecf');
		$dname = 'fieldlabel';
		$did = 'modulefieldid';
		$dataattrname1 = 'columnname';
		$dataattrname2 = 'tablename';
		$table = 'modulefield';
		$whfield = 'moduletabid';
		$module= explode(",",trim($_POST['moduleid']));
		if($_POST['relatedmoduleids'] !='' AND $_POST['relatedmoduleids'] != 'null'){
			$relatedmodule=explode(",",trim($_POST['relatedmoduleids']));
			$mergemodule=array_filter(array_merge($module,$relatedmodule));
		} else{
			$mergemodule=array_filter($module);
		}
		$mulcond = explode(",",$whfield);
		$mulcondval = array_filter($mergemodule);
		$this->db->select("$dname,$did,$dataattrname1,$dataattrname2,module.modulename,module.moduleid,modulefield.fieldlabel");
		$this->db->from($table);
		$this->db->join('module','module.moduleid=modulefield.moduletabid');
		$this->db->where_not_in('modulefield.uitypeid',array(15,16,22,23,24,28));
		if(count($mulcondval) > 0) {
			$this->db->where_in('modulefield.moduletabid',$mulcondval);
			$i++;
		}			
		$this->db->order_by('module.modulename','asc');
		$this->db->where('modulefield.status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result()as $row) {
				$fname = $row->$dataattrname1;
				$dataname = explode(' ',$row->$dname);
				
				if(in_array('Primary',$dataname)) $fname = 'PR-'.trim($fname);
				if(in_array('Secondary',$dataname)) $fname = 'SE-'.trim($fname);
				if(in_array('Billing',$dataname)) $fname = 'BI-'.trim($fname);
				if(in_array('Shipping',$dataname)) $fname = 'SH-'.trim($fname);
				if(in_array('Landmark',$dataname)){$fname =$row->$dataattrname1;}
				if($row->$dname!='') {
					$data[$j]=array('datasid'=>$row->$did,'dataname'=>$row->$dname,'otherdataattrname1'=>$fname,'otherdataattrname2'=>$row->$dataattrname2,'modulefield'=>$row->fieldlabel,'moduleid'=>$row->moduleid,'modulename'=>ucwords($row->modulename));
					$j++;
					if($fname == 'salutationid'){
						$h=1;
						$tablname = $row->$dataattrname2;
					}
					$jj = $j;
				}
			}
			if($h==0) {
				$i=0;
				$this->db->select("$dname,$did,$dataattrname1,$dataattrname2,module.modulename,module.moduleid,modulefield.fieldlabel");
				$this->db->from($table);
				$this->db->join('module','module.moduleid=modulefield.moduletabid');
				$this->db->where_not_in('modulefield.uitypeid',array(15,16,22,23,24,28));
				$this->db->where($dataattrname1,'salutationid');
				if(count($mulcondval) > 0) {
					$this->db->where_in('modulefield.moduletabid',$mulcondval);
					$i++;
				}
				$this->db->where('modulefield.status',10);
				$this->db->order_by('module.modulename','asc');
				$saresult = $this->db->get();
				if($saresult->num_rows() >0) {
					foreach($saresult->result()as $row) {
						$data[]=array('datasid'=>$row->$did,'dataname'=>$row->$dname,'otherdataattrname1'=>$row->$dataattrname1,'otherdataattrname2'=>$row->$dataattrname2,'modulefield'=>$row->fieldlabel,'moduleid'=>$row->moduleid,'modulename'=>ucwords($row->modulename));
					}
				}
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//datafield drop down value get
	public function datafielddatafetchmodel() {
		$i=0;
		$j=0;
		$h=0;
		$jj=0;
		$moduleid = $_POST['moduleid'];
		$datafield = $_POST['datafiled'];
		$impdatafield = explode(',',$datafield);
		$dname = 'fieldlabel';
		$did = 'modulefieldid';
		$dataattrname1 = 'columnname';
		$dataattrname2 = 'tablename';
		$table = 'modulefield';
		$whfield = 'moduletabid';
		for($i=0;$i<count($impdatafield);$i++) {
			$this->db->select("$dname,$did,$dataattrname1,$dataattrname2,module.modulename,module.moduleid,modulefield.fieldlabel");
			$this->db->join('module','module.moduleid=modulefield.moduletabid');
			$this->db->from($table);
			$this->db->where($table.'.'.$table.'id',$impdatafield[$i]);
			$this->db->where($table.'.status',1);
			$result = $this->db->get();
			if($result->num_rows() >0) {
				foreach($result->result()as $row) {
					$fname = $row->$dataattrname1;
					$dataname = explode(' ',$row->$dname);
					
					if(in_array('Primary',$dataname)) $fname = 'PR-'.trim($fname);
					if(in_array('Secondary',$dataname)) $fname = 'SE-'.trim($fname);
					if(in_array('Billing',$dataname)) $fname = 'BI-'.trim($fname);
					if(in_array('Shipping',$dataname)) $fname = 'SH-'.trim($fname);
					if(in_array('Landmark',$dataname)){$fname =$row->$dataattrname1;}
					if($row->$dname!='') {
						$selectedmoduleid = $row->moduleid;
						if($selectedmoduleid == $moduleid){
							$value = $row->$did.'#'.$row->$dataattrname2.'.'.$fname.'_'.$row->fieldlabel;
						} else {
							$value = $row->$did.'#'.'REL:'.$row->$dataattrname2.'.'.$fname.'_'.$row->fieldlabel;
						}
						$data[$j]= $value;
						$j++;
					}
				}
			}
		}
		echo implode(',',$data);
	}
	//dashboard	edit data fetch
	public function dashboardbuildereditdatafetchmodel() {
		$dashboardid = $_GET['dashboardid'];
		$this->db->select('dashboard.dashboardname,dashboard.moduleid,dashboard.dashboardfolderid,dashboard.description,dashboard.setdefault,dashboard.active,dashboard.setmini,dashboard.miniwidgetcount');
		$this->db->from('dashboard');
		$this->db->where('dashboard.dashboardid',$dashboardid);
		$this->db->where('dashboard.status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result()as $row) {
				$data = array('dashboardname'=>$row->dashboardname,'mid'=>$row->moduleid,'folderid'=>$row->dashboardfolderid,'description'=>$row->description,'setdefault'=>$row->setdefault,'active'=>$row->active,'dbsetmini'=>$row->setmini,'miniwidgetcount'=>$row->miniwidgetcount);
			}
			echo json_encode($data);
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}
	}
	//dashboard builder row data fetch
	public function dashboardbuilderrowdatafetchmodel() {
		$dashboardid = $_GET['dashboardid'];
		$i=0;
		$this->db->select('dashboardrow.dashboardrowtype,dashboardrow.dashboardrowid');
		$this->db->from('dashboardrow');
		$this->db->where('dashboardrow.dashboardid',$dashboardid);
		$this->db->where('dashboardrow.status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result()as $row) {
				$data['rowtype'][$i] = $row->dashboardrowtype;
				$data['rowid'][$i] = $row->dashboardrowid;
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}
	}
	//dashboard builder widget data fetch
	public function dashboardbuilderwidgetdatafetchmodel() {
		$dashboardid = $_GET['dashboardid'];
		$rowid = $_GET['rowid'];
		$data =array();
		$i=0;
		$exrowid = explode(',',$rowid);
		for($j=0;$j<count($exrowid);$j++) {
			$this->db->select('dashboardfield.dashboardfieldtype,dashboardfield.dashboardfiledname,dashboardfield.fieldviewtype,dashboardfield.fieldtheme,dashboardfield.fieldrotate,dashboardfield.fieldbasecolor,dashboardfield.fieldangle,dashboardfield.fieldlegend,dashboardfield.fieldlegendtype,dashboardfield.fieldparenttablename,dashboardfield.fieldchildtablename,dashboardfield.fieldconditionname,dashboardfield.fieldconditionvalue,dashboardfield.fieldrowcount,dashboardfield.reportid,dashboardfield.groupfieldid,dashboardfield.calculationfieldid,dashboardfield.operation,dashboardfield.countstatus,dashboardfield.splitbyid,dashboardfield.twopointid');
			$this->db->from('dashboardfield');
			$this->db->where_in('dashboardfield.dashboardrowid',array($exrowid[$j]));
			$this->db->where('dashboardfield.status',1);
			$result = $this->db->get();
			if($result->num_rows() >0) {
				foreach($result->result()as $row) {
					$data[$i] = array('widgetname'=>$row->dashboardfieldtype,'filedname'=>$row->dashboardfiledname,'viewtype'=>$row->fieldviewtype,'theme'=>$row->fieldtheme,'rotate'=>$row->fieldrotate,'basecolor'=>$row->fieldbasecolor,'angle'=>$row->fieldangle,'legend'=>$row->fieldlegend,'legendtype'=>$row->fieldlegendtype,'parenttbl'=>$row->fieldparenttablename,'childtable'=>$row->fieldchildtablename,'conditionname'=>$row->fieldconditionname,'condtionvalue'=>$row->fieldconditionvalue,'rowcount'=>$row->fieldrowcount,'reportid'=>$row->reportid,'groupfiled'=>$row->groupfieldid,'calculationfield'=>$row->calculationfieldid,'operation'=>$row->operation,'countstatus'=>$row->countstatus,'splitbyid'=>$row->splitbyid,'twopointid'=>$row->twopointid);
					$i++;
				}
			} 
		}
		if(!empty($data)) {
			echo json_encode($data);
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}
	}
	//default value set array for update
	public function updatedefaultvalueget() {
		$defdata['lastupdatedate'] = date($this->Basefunctions->datef);
		$defdata['lastupdateuserid'] = $this->Basefunctions->userid;
		return $defdata;
	}
	//dashboard update function
	public function dashboradbuilderupdatemodel() {
		$defdataarr = $this->defaultvalueget();
		$updatedefarray = $this->updatedefaultvalueget();
		$dashboardid = $_POST['dashboardid'];
		$dashboardrowid = $_POST['dashboardrowid'];
		$dashbrdname = $_POST['dashboardname'];
		$moduleid = $_POST['modulename'];
		$dashbrddesc = $_POST['dbdescription'];
		$dashbrdfldname = $_POST['dbfoldername'];
		$dashbrdactive = $_POST['dbsetactivehidden'];
		$dashbrddefault = $_POST['dbsetdefaulthidden'];
		if($_POST['dbsetminihidden'] == 'Yes'){
			$_POST['miniwidgetcount'] = $_POST['miniwidgetcount'];
		}else{
			$_POST['miniwidgetcount'] = 0;
		}
		//dash board data insertion
		$dashrow = $_POST['chartrowinfo'];
		$dashchart = $_POST['chartnameinfo'];
		$cdata = 'True';
		foreach($dashchart as $chartinfo) {
			$chartdata = explode(',',$chartinfo);
			if($chartdata[1] == 'pie') {
				if(count($chartdata) <= 12){
					$cdata = 'False';
				}
			} else if($chartdata[1] == 'donut') {
				if(count($chartdata) <= 12){
					$cdata = 'False';
				}
			} else if($chartdata[1] == 'pyramid') {
				if(count($chartdata) <= 11){
					$cdata = 'False';
				}
			} else if($chartdata[1] == 'funnel') {
				if(count($chartdata) <= 11){
					$cdata = 'False';
				}
			} else if($chartdata[1] == 'column') {
				if(count($chartdata) <= 12){
					$cdata = 'False';
				}
			} else if($chartdata[1] == 'cylinder3d') {
				if(count($chartdata) <= 15){
					$cdata = 'False';
				}
			} else if($chartdata[1] == 'stacked') {
				if(count($chartdata) < 10){
					$cdata = 'False';
				}
			} else if($chartdata[1] == 'bubble') {
				if(count($chartdata) < 10){
					$cdata = 'False';
				}
			}
			else if($chartdata[1] == 'angulargauge') {
				if(count($chartdata) < 10){
					$cdata = 'False';
				}
			} 
			else if($chartdata[1] == 'wordcloud') {
				if(count($chartdata) <= 15){
					$cdata = 'False';
				}
			}
			else if($chartdata[1] == 'line') {
				if(count($chartdata) <= 12){
					$cdata = 'False';
				}
			}
			else if($chartdata[1] == 'area') {
				if(count($chartdata) <= 12){
					$cdata = 'False';
				}
			}
		} 
		if($cdata !== 'False') {
			//remove old data set in dashboard row
			$statsinfo = array('status'=>0);
			$dbrowdata = array_merge($statsinfo,$updatedefarray);
			$this->db->where_in('dashboardrow.dashboardid',array($dashboardid));
			$this->db->update('dashboardrow',$dbrowdata);
			//remove old data set dashboard filed 
			$explode = explode(',',$dashboardrowid);
			for($i=0;$i<count($explode);$i++) {
				$this->db->where_in('dashboardfield.dashboardrowid',array($explode[$i]));
				$this->db->update('dashboardfield',$statsinfo);
			}
			//dashboard data update
			$dashbrddata = array(
				'moduleid'=>$moduleid,
				'dashboardname'=>ucwords($dashbrdname),
				'dashboardfolderid'=>ucwords($dashbrdfldname),
				'setmini'=>$_POST['dbsetminihidden'],
				'miniwidgetcount'=>$_POST['miniwidgetcount'],	
				'description'=>ucwords($dashbrddesc),
				'setdefault'=>$dashbrddefault,
				'active'=>$dashbrdactive
			);
			$newdata = array_merge($dashbrddata,$updatedefarray);
			$this->db->where( 'dashboard.dashboardid', $dashboardid );
			$this->db->update( 'dashboard', $newdata );
			$this->setdefaultupdate($moduleid,$dashbrddefault,$dashboardid);
			//notification entry (Audit log)
			$empid = $this->Basefunctions->logemployeeid;
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch(240,$dashboardid,'dashboard','3');
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($dashboardid,'Updated',$notimsg,$empid,240);
			}
		foreach($dashrow as $rowinfo) {
			$rowdata = explode(',',$rowinfo);
			//dashboard row insertion
			$dashbrdrowdata = array(
						'dashboardrowname'=>ucwords($rowdata[2]),
						'dashboardid'=>$dashboardid,
						'dashboardrowtype'=>$rowdata[1]
					);
			$newrowdata = array_merge($dashbrdrowdata,$defdataarr);
			$this->db->insert( 'dashboardrow', $newrowdata );
			$dashboardrowid = $this->db->insert_id();
			//chart data insertion
			$reportgroup=array();
			foreach($dashchart as $chartinfo) {
				$chartdata = explode(',',$chartinfo);
				$angle = 0;
				$condname = "";
				$condvalue = "";
				$count = "0";				
				if( $rowdata[0] == $chartdata[0] ) {
					switch($chartdata[1]) {
						case 'pie' :
							//pie chart
							$title = $chartdata[3];
							$charttype = 'piechart';
							$viewtype = $chartdata[4];
							$angle = $chartdata[5];							
							$legend = $chartdata[6];
							$legendtype = $chartdata[7];
							$parenttable = 'source';
							$childtable = 'lead';
							$basecolor = '';
							$rotate = 'false';
							$theme = '';
							//chart-group-data
							$reportgroup=array('reportid'=>$chartdata[8],'groupfieldid'=>$chartdata[9],'calculationfieldid'=>$chartdata[11],'operation'=>$chartdata[12],'countstatus'=>$chartdata[10]);				
							break;
						case 'donut' :
							//Donut chart
							$title = $chartdata[3];
							$charttype = 'donutchart';
							$viewtype = $chartdata[4];
							$angle = $chartdata[5];							
							$legend = $chartdata[6];
							$legendtype = $chartdata[7];
							$parenttable = 'source';
							$childtable = 'lead';
							$basecolor = '';
							$rotate = 'false';
							$theme = '';
							//chart-group-data
							$reportgroup=array('reportid'=>$chartdata[8],'groupfieldid'=>$chartdata[9],'calculationfieldid'=>$chartdata[11],'operation'=>$chartdata[12],'countstatus'=>$chartdata[10]);				
							break;
						case 'pyramid' :
							//pyramid chart
							$title = $chartdata[3];
							$charttype = 'pyramidchart';
							$viewtype = 'false';
							$angle = 0;							
							$legend = $chartdata[5];
							$legendtype = $chartdata[6];
							$parenttable = 'source';
							$childtable = 'lead';
							$basecolor = '';
							$rotate = 'false';
							$theme = $chartdata[4];
							//chart-group-data
							$reportgroup=array('reportid'=>$chartdata[7],'groupfieldid'=>$chartdata[8],'calculationfieldid'=>$chartdata[10],'operation'=>$chartdata[11],'countstatus'=>$chartdata[9]);				
							break;
						case 'funnel' :
							//funnel chart
							$title = $chartdata[3];
							$charttype = 'funnelchart';
							$viewtype = 'false';
							$angle = 0;							
							$legend = $chartdata[5];
							$legendtype = $chartdata[6];
							$parenttable = 'source';
							$childtable = 'lead';
							$basecolor = '';
							$rotate = 'false';
							$theme = $chartdata[4];
							//chart-group-data
							$reportgroup=array('reportid'=>$chartdata[7],'groupfieldid'=>$chartdata[8],'calculationfieldid'=>$chartdata[10],'operation'=>$chartdata[11],'countstatus'=>$chartdata[9]);				
							break;
						#column and bar charts.
						case 'column' :
							//column chart
							$title = $chartdata[3];
							$charttype = 'columnchart';
							$viewtype = $chartdata[4];
							$angle = 0;							
							$legend = $chartdata[7];
							$legendtype = $chartdata[8];
							$parenttable = 'source';
							$childtable = 'lead';
							$basecolor = $chartdata[6];
							$rotate = 'false';
							$theme = $chartdata[5];
							//chart-group-data
							$reportgroup=array('reportid'=>$chartdata[9],'groupfieldid'=>$chartdata[10],'calculationfieldid'=>$chartdata[12],'operation'=>$chartdata[13],'countstatus'=>$chartdata[11]);				
							break;
						case 'cylinder3d' :
							//cylinder3d chart
							$title = $chartdata[3];
							$charttype = 'cylinder3dchart';
							$viewtype = 'false';
							$angle = 0;							
							$legend = $chartdata[4];
							$legendtype = $chartdata[5];
							$parenttable = 'source';
							$childtable = 'lead';
							$basecolor = '';
							$rotate = 'false';
							$theme = '';
							//chart-group-data
							$reportgroup=array('reportid'=>$chartdata[6],'groupfieldid'=>$chartdata[7],'calculationfieldid'=>$chartdata[9],'operation'=>$chartdata[10],'countstatus'=>$chartdata[8]);				
							break;
						case 'wordcloud' :
							//wordcloud chart
							$viewtype = 'false';
							$charttype = 'wordcloudchart';
							$theme = $chartdata[4];
							$angle = $chartdata[5];
							$title = $chartdata[6];
							$legend = $chartdata[7];
							$legtype = explode('|',$chartdata[8]);
							$legendtype = implode(',',$legtype);
							$basecolor = $chartdata[9];
							$rotate = 'false';
							$parenttable = 'source';
							$childtable = 'lead';
							//chart-group-data
							$reportgroup=array('reportid'=>$chartdata[10],'groupfieldid'=>$chartdata[11],'calculationfieldid'=>$chartdata[13],'operation'=>$chartdata[14],'countstatus'=>$chartdata[12]);
							break;
						case 'line' :
							//line chart
							$title = $chartdata[3];
							$charttype = 'linechart';
							$viewtype = $chartdata[4];
							$angle = 0;							
							$legend = $chartdata[7];
							$legendtype = $chartdata[8];
							$parenttable = 'source';
							$childtable = 'lead';
							$basecolor = $chartdata[6];
							$rotate = 'false';
							$theme = $chartdata[5];
							//chart-group-data
							$reportgroup=array('reportid'=>$chartdata[9],'groupfieldid'=>$chartdata[10],'calculationfieldid'=>$chartdata[12],'operation'=>$chartdata[13],'countstatus'=>$chartdata[11]);				
							break;
						case 'area' :
							//area chart
							$title = $chartdata[3];
							$charttype = 'areachart';
							$viewtype = $chartdata[4];
							$angle = 0;							
							$legend = $chartdata[7];
							$legendtype = $chartdata[8];
							$parenttable = 'source';
							$childtable = 'lead';
							$basecolor = $chartdata[6];
							$rotate = 'false';
							$theme = $chartdata[5];
							//chart-group-data
							$reportgroup=array('reportid'=>$chartdata[9],'groupfieldid'=>$chartdata[10],'calculationfieldid'=>$chartdata[12],'operation'=>$chartdata[13],'countstatus'=>$chartdata[11]);				
							break;
						case 'stacked' :
							//stacked chart
							$title = $chartdata[3];
							$charttype = 'stackedchart';
							$viewtype = 'false';
							$angle = 0;							
							$legend = $chartdata[6];
							$legendtype = $chartdata[7];
							$parenttable = 'source';
							$childtable = 'lead';
							$basecolor = '';
							$rotate = $chartdata[5];
							$theme = $chartdata[4];
							//chart-group-data
							$reportgroup=array('reportid'=>$chartdata[8],'groupfieldid'=>$chartdata[9],'calculationfieldid'=>$chartdata[12],'operation'=>$chartdata[13],'countstatus'=>$chartdata[11],'splitbyid'=>$chartdata[10]);				
							break;	
						case 'bubble' :
							//bubble chart
							$title = $chartdata[3];
							$charttype = 'bubblechart';
							$viewtype = 'false';
							$theme = $chartdata[4];
							$angle = 0;
							$legend = 'false';
							$legendtype = 'false';
							$basecolor = $chartdata[5];
							$rotate = 'false';
							$parenttable = 'source';
							$childtable = 'lead';
							//chart-group-data
							$reportgroup=array('reportid'=>$chartdata[6],'groupfieldid'=>$chartdata[7],'calculationfieldid'=>$chartdata[10],'operation'=>$chartdata[11],'countstatus'=>$chartdata[9],'splitbyid'=>$chartdata[8]);
							break;
						case 'ibtaskwidget':
							//task widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibtaskwidgetmini':
							//task widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibdocumentswidget':
							//Document widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
					   case 'ibdocumentswidgetmini':
							//Document widget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibmaillogwidget':
							//Mail log widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[9])) {
								$title = $chartdata[9];
							} else {
								$title = '';
							}
							$parenttable = '';
							$legend = '';
							$childtable = '';
							$legendtype = '';
							$condname = '';
							$condvalue = '';
							$count = 0;
							break;
						case 'ibsendsmslogwidget':
							//Quick Send SMS widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[9])) {
								$title = $chartdata[9];
							} else {
								$title = '';
							}
							$parenttable = '';
							$legend = '';
							$childtable = '';
							$legendtype = '';
							$condname = '';
							$condvalue = '';
							$count = 0;
							break;
						case 'ibsendsmslogwidget':
							//Quick Send SMS widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[9])) {
								$title = $chartdata[9];
							} else {
								$title = '';
							}
							$parenttable = '';
							$legend = '';
							$childtable = '';
							$legendtype = '';
							$condname = '';
							$condvalue = '';
							$count = 0;
							break;
						case 'ibclicktocallwidget':
							//Click To Call widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[2])) {
								$title = $chartdata[2];
							} else {
								$title = '';
							}
							$parenttable = '';
							$legend = '';
							$childtable = '';
							$legendtype = '';
							$condname = '';
							$condvalue = '';
							$count = 0;
							break;
						case 'ibsmslogwidget':
							//SMS log widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[2])) {
								$title = $chartdata[2];
							} else {
								$title = '';
							}
							$parenttable = '';
							$legend = '';
							$childtable = '';
							$legendtype = '';
							$condname = '';
							$condvalue = '';
							$count = 0;
							break;
						case 'ibreceivesmslogwidget':
							//Receive SMS log widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[9])) {
								$title = $chartdata[9];
							} else {
								$title = '';
							}
							$parenttable = '';
							$legend = '';
							$childtable = '';
							$legendtype = '';
							$condname = '';
							$condvalue = '';
							$count = 0;
							break;
						case 'ibactivitieswidget':
							//Activity widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibinboundcalllogwidget':
							//inbound widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'iboutboundcalllogwidget':
							//outbound widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = 'Outbound Call Log';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibopportunitywidget':
							//Opportunities widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibcontactswidget':
							//Contacts widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibinvoicewidget':
							//Invoices widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibpaymentwidget':
							//Payment widget
							$viewtype ='false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibquotewidget':
							//Quotes widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibaccountswidget':
							//Account widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibticketswidget':
							//Tickets widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibcampaignwidget':
							//Campaign widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibsalesorderwidget':
							//Sales Order widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibpurchaseorderwidget':
							//Sales Order widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibmaterialrequisitionwidget':
							//Sales Order widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibsolutionwidget':
							//Solutions widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibsolutioncatwidget':
							//Solutions Category widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibcontractwidget':
							//Contracts widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibleadwidget':
							//Leads widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibproductwidget':
							//Leads widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibpricebookwidget':
							//Leads widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[7])) {
								$title = $chartdata[7];
							} else {
								$title = '';
							}
							$parenttable = $chartdata[2];
							$legend = $chartdata[3];
							$childtable = $chartdata[4];
							$legendtype = $chartdata[5];
							$count = $chartdata[6];
							break;
						case 'ibconversationwidget':
							//Solutions Category widget 
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[2])) {
								$title = $chartdata[2];
							} else {
								$title = 'Conversation';
							}
							$parenttable = '';
							$legend = '';
							$childtable ='';
							$legendtype = '';
							$count = 0;
							break;
						case 'ibconversationwidgetmini':
							//Solutions Category widget mini
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[2])) {
								$title = $chartdata[2];
							} else {
								$title = 'Conversation';
							}
							$parenttable = '';
							$legend = '';
							$childtable ='';
							$legendtype = '';
							$count = 0;
							break;
						case 'ibnotificationwidget':
							//Solutions Category widget ibnotificationwidget ibleadstatuswidget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[2])) {
								$title = $chartdata[2];
							} else {
								$title = 'Notification';
							}
							$parenttable = '';
							$legend = '';
							$childtable ='';
							$legendtype = '';
							$count = 0;
							break;
						case 'ibnotificationwidgetmini':
							//Solutions Category widget ibnotificationwidget ibleadstatuswidget
							$viewtype = 0;
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[2])) {
								$title = $chartdata[2];
							} else {
								$title = 'Notification';
							}
							$parenttable = '';
							$legend = '';
							$childtable ='';
							$legendtype = '';
							$count = 0;
							break;
						case 'ibleadstatuswidget':
							//lead status based widget-
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							if(isset($chartdata[2])) {
								$title = $chartdata[2];
							} else {
								$title = 'Lead Status';
							}
							$parenttable = '';
							$legend = '';
							$childtable ='';
							$legendtype = '';
							$count = 0;
							break;
						case 'ibcustomwidget':
							$titlename = $chartdata[2];
							if($chartdata[2] == 'undefined' OR $chartdata[2] =='' OR $chartdata[2] == null){
								$titlename = 'CustomWidget';
							}
							//custom widget
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							$title = $titlename;
							$parenttable = $chartdata[3];
							$legend = $chartdata[4];
							$childtable = '';
							$legendtype = '';
							$count = 0;
							break;
						case 'ibtimeanddatewidget':
							//lead status based widget-
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							$title = 'Time And Date';
							$parenttable = '';
							$legend = '';
							$childtable ='';
							$legendtype = '';
							$count = 0;
							break;
						case 'ibcalendarwidget':
							//lead status based widget-
							$viewtype = 'false';
							$charttype = $chartdata[1];
							$basecolor = '';
							$theme = '';
							$rotate = '';
							$title = 'Calendar';
							$parenttable = '';
							$legend = '';
							$childtable ='';
							$legendtype = '';
							$count = 0;
							break;						
						case 'angulargauge' :
							//angulargauge chart
							$title = $chartdata[3];
							$charttype = 'angulargaugechart';
							$viewtype = 'false';
							$angle = 0;							
							$legend = $chartdata[4];
							$legendtype = $chartdata[5];
							$parenttable = 'source';
							$childtable = 'lead';
							$basecolor = '';
							$rotate = 'false';
							$theme = '';
							//chart-group-data
							$reportgroup=array('reportid'=>$chartdata[6],'groupfieldid'=>$chartdata[7],'calculationfieldid'=>$chartdata[9],'operation'=>$chartdata[10],'countstatus'=>$chartdata[8]);				
							break;
						default :
							$viewtype = 'false';
							$charttype = '';
							$theme = '';
							$angle = '';
							$title = '';
							$legend = '';
							$legendtype = '';
							$basecolor = '';
							$rotate = '';
							$parenttable = '';
							$childtable = '';	
							break;
					}
					//chart data insertion
					$dashchartdata = array(
						'dashboardrowid'=>$dashboardrowid,
						'dashboardfieldtype'=>$charttype,
						'dashboardfiledname'=>$title,
						'fieldviewtype'=>$viewtype,
						'fieldtheme'=>$theme,
						'fieldrotate'=>$rotate,
						'fieldbasecolor'=>$basecolor,
						'fieldangle'=>$angle,
						'fieldlegend'=>$legend,
						'fieldlegendtype'=>$legendtype,
						'fieldparenttablename'=>$parenttable,
						'fieldchildtablename'=>$childtable,
						'fieldconditionname'=>$condname,
						'fieldconditionvalue'=>$condvalue,
						'fieldrowcount'=>$count,									
						'status'=>1
					);
					$newchartdata=array_merge($dashchartdata,array_filter($reportgroup));
					$this->db->insert('dashboardfield',$newchartdata);
				}
			}
		}
			echo 'Success';
		} else {
			echo 'Fail';
		}
	}
	//set default update
	public function setdefaultupdate($moduleid,$dashbrddefault,$dashboardid) {
		if($dashbrddefault != 'No') {
			$updatearray = array('setdefault'=>'No');
			$this->db->where_not_in('dashboard.dashboardid',$dashboardid);
			$this->db->where('dashboard.moduleid',$moduleid);
			$this->db->update('dashboard',$updatearray);
		}
	}
}
?>