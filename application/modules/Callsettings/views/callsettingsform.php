<!-- For Date Formatter -->
<?php 
$CI =& get_instance();
$appdateformat = $CI->Basefunctions->appdateformatfetch(); ?>
<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<span class="gridviewdivforsh">
				<div class="large-12 columns paddingzero formheader">
				<?php
					$this->load->view('Base/mainviewaddformheader');
				?>
				</div>
				<div class="large-12 columns addformunderheader">&nbsp;</div>
				<div class="large-12 columns scrollbarclass addformcontainer smssettingscontainer">
					<div class="row mblhidedisplay">&nbsp;</div>
					<form method="POST" name="dataaddform" class="" action ="" id="dataaddform" enctype="multipart/form-data">
						<span id="formaddwizard" class="validationEngineContainer" >
							<span id="formeditwizard" class="validationEngineContainer">
								<?php
									//function call for form fields generation
									formfieldstemplategenerator($modtabgrp,'');
								?>
							</span>
						</span>
						<!--hidden values-->
						<?php
							echo hidden('primarydataid','');
							echo hidden('sortorder','');
							echo hidden('sortcolumn','');
							$value = '';
							echo hidden('resctable',$value);
							$modid = $this->Basefunctions->getmoduleid($filedmodids);
							echo hidden('viewfieldids',$modid);
						?>
					</form>
				</div>
			</span>			
		</div>
	</div>
</div>

<style type="text/css">
#callsettingssetaddgrid1width {
	height:800px !important;
}
#callsettingssetaddgrid1 {
	height:760px !important;
}
.large-12 .columns .scrollbarclass .addformcontainer .smssettingscontainer {
	overflow:hidden;
}
.rppdropdown  {
	left:50% !important;
}
</style>
