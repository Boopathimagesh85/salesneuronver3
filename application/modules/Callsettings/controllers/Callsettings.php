<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Callsettings extends MX_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Callsettings/Callsettingsmodel');
	}
    //first basic hitting view
    public function index() {
    	$moduleid = array(30);
    	sessionchecker($moduleid);
		/*action*/
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(30);
		$viewmoduleid = array(30);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
      	$this->load->view('Callsettings/callsettingsview',$data);
	}
	
	//show hide fields
	public function callcredentialadd() {
		$this->Callsettingsmodel->callcredentialaddmodel();
	}
	//call credential grid data fetch
	public function callcredentialgriddatafetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'callsettings.callsettingsid') : 'callsettings.callsettingsid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('callservicetypename','providername','callservicemodename','callnumbertypename','mobilenumber','username','password','apikey'),'colmodelindex'=>array('callservicetype.callservicetypename','provider.providername','callservicemode.callservicemodename','callnumbertype.callnumbertypename','callsettings.mobilenumber','callsettings.username','callsettings.password','callsettings.apikey'),'coltablename'=>array('callservicetype','provider','callservicemode','callnumbertype','callsettings','callsettings','callsettings','callsettings'),'uitype'=>array('17','17','17','17','11','2','2','2'),'colname'=>array('Call Service Type','Provider Name','Service Mode','Number Type','Number','User Name','Password','apikey'),'colsize'=>array('200','200','200','200','200','200','200','200'));
		$result=$this->Callsettingsmodel->callcredentialgriddatafetchgridxmlview($primarytable,$sortcol,$sortord,$pagenum,$rowscount);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($result,$page,'');
		$device = $this->Basefunctions->deviceinfo();
		//if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Call Settings List',$width,$height);
		//} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Call Settings List',$width,$height);
		//}
		echo json_encode($datas);
	}
	//sms credential data fetch
	public function callcredentialeditdatafetch() {
		$this->Callsettingsmodel->callcredentialeditdatafetchmodel();
	}
	//sms credential update
	public function callcredentialupdate() {
		$this->Callsettingsmodel->callcredentialupdatemodel();
	}
	//sms credential delete
	public function callcredentialdelete() {
		$this->Callsettingsmodel->callcredentialdeletemodel();
	}
	//service mode dd reload
	public function servicemodeddreload() {
		$this->Callsettingsmodel->servicemodeddreloadmodel();
	}
}