<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Callsettingsmodel extends CI_Model{
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//sms settings credential add
	public function callcredentialaddmodel() {
		//master company id get\
		$mcid = $this->usermastercompanyid();
		$numtypename = $_POST['numtypename'];
		$servicename = $_POST['servicename'];
		$modename = $_POST['modename'];
		$provider = $_POST['provider'];
		$provider = $_POST['provider'];
		$provider = $_POST['provider'];
		$cusername = $_POST['username'];
		$cpassword = $_POST['password'];
		$apikeys = $_POST['apikey'];
		$mobilenumber = $_POST['mobilenumber'];
		//service type
		if(isset($_POST['servicetype'])) {
			$servicetype = $_POST['servicetype']; 
			if($servicetype == ''){
				$servicetype = 1;
			}
		} else {
			$servicetype = 1;
		}
		//service mode
		if(isset($_POST['servicemode'])){
			$servicemode = $_POST['servicemode']; 
			if($servicemode == '') {
				$servicemode = 1;
			}
		} else {
			$servicemode = 1;
		}
		//number type 
		if(isset($_POST['numbertype'])){
			$numbertype = $_POST['numbertype'];
			if($numbertype == '' || $numbertype == 'null') {
				$numbertype = 1;
			}
		} else {
			$numbertype = 1;
		}
		$date= date($this->Basefunctions->datef);
		$userid= $this->Basefunctions->userid;
		$tstatus = $this->Basefunctions->activestatus;
		$callsettings=array( 
				'mastercompanyid'=>$mcid,
				'callservicetypeid'=>$servicetype,
				'providerid'=>$provider,
				'callservicemodeid'=>$servicemode,
				'callnumbertypeid'=>$numbertype,
				'mobilenumber'=>$mobilenumber,
				'username'=>$cusername,
				'password'=>$cpassword,
				'apikey'=>$apikeys,
				'industryid'=>$this->Basefunctions->industryid,
				'branchid'=>$this->Basefunctions->branchid,
				'createdate'=>$date,
				'lastupdatedate'=>$date,
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
				'status'=>$tstatus
			);
		$this->db->insert('callsettings',$callsettings);
		$datacontent = "<p>Dear Sir/Madam,</p>
			<p>This is automatic mail sent from&nbsp;<a href='http://salesneuron.com/' target='_blank'>salesneuron.com</a>&nbsp;regarding Call Settings request for one of the customers.</p>
			<p>Below is the Call Settings information:</p>";
		$datacontent .= "<p> Service Type : ".$servicename." </p><p> Service Mode : ".$modename." </p><p> Number Type : ".$numtypename." </p><p> Number : ".$mobilenumber." </p><p> User Name : ".$cusername." </p><p> Password : ".$cpassword." </p><p> API Key : ".$apikeys." </p>
			<p>Please check the details and approve the Call Settings at the earliest. Reply to this mail with your status update.</p>
			<p>Looking forward for approval at the earliest. Please call on +91 72000 70822 for any verification/doubts clarifications.</p><br/><p>Regards,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
			<p><strong>Customer Success Team</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p><p>    Sales Neuron    <strong>AUC VENTURES PVT LTD</strong></p><p>No:6, Palani Andavar Koil Street,Villivakkam,Chennai-600049.&nbsp;   </p><p>Landline : 044-42639131&nbsp;| &nbsp;Mobile : +91 7200070822|&nbsp;<a href='http://www.aucventures.com/' target='_blank'>www.salesneuron.com</a></p>";
		$tomailid = array(array('email'=>'arvind@aucventures.com'),array('email'=>'gowtham@aucventures.com','type' => 'cc'));
		$message = array(
				'html' => $datacontent,
				'text' => '',
				'subject' => 'Regards Credentials Details',
				'from_email' => 'arvind@salesneuron.com',
				'from_name' => 'Arvind',
				'to' => $tomailid,
				'track_opens' => true,
				'track_clicks' => true,
				'inline_css' => true,
				'url_strip_qs' => true,
				'important' => true,
				'auto_text' => true,
				'auto_html' => true,
			);
		$send_at ='';		
		$this->mailsentfunction($message,$send_at);
		echo 'TRUE'; 
	}
	//subscriber master company
	public function usermastercompanyid()	{
		$mcid = 1;
		$empid = $this->Basefunctions->userid;
		$compquery = $this->db->query('select company.companyid,company.mastercompanyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.'')->result();
		foreach($compquery as $key) {
			$mcid = $key->mastercompanyid;
		}
		return $mcid;
	}
	//inline user data base set
	public function inlineuserdatabaseactive($databasename) {
		$host = $this->db->hostname;
		$user = $this->db->username;
		$passwd = $this->db->password;
		$dbconfig['hostname'] = $host;
		$dbconfig['username'] = $user;
		$dbconfig['password'] = $passwd;
		$dbconfig['database'] = $databasename;
		$dbconfig['dbdriver'] = 'mysql';
		$dbconfig['dbprefix'] = '';
		$dbconfig['pconnect'] = TRUE;
		$dbconfig['db_debug'] = TRUE;
		$dbconfig['cache_on'] = FALSE;
		$dbconfig['cachedir'] = '';
		$dbconfig['char_set'] = 'utf8';
		$dbconfig['dbcollat'] = 'utf8_general_ci';
		$dbconfig['swap_pre'] = '';
		$dbconfig['autoinit'] = TRUE;
		$dbconfig['stricton'] = FALSE;
		return $import_db = $this->load->database($dbconfig, TRUE);
	}
	//call setting grid view
	public function callcredentialgriddatafetchgridxmlview($tablename,$sortcol,$sortord,$pagenum,$rowscount) {
		$dataset = 'callsettings.callsettingsid,callservicetype.callservicetypename,provider.providername,callservicemode.callservicemodename,callnumbertype.callnumbertypename,callsettings.mobilenumber,callsettings.username,callsettings.password,callsettings.apikey';
		$join =' LEFT OUTER JOIN status ON status.status=callsettings.status';
		$join .=' LEFT OUTER JOIN callnumbertype ON callnumbertype.callnumbertypeid=callsettings.callnumbertypeid';
		$join .=' LEFT OUTER JOIN callservicemode ON callservicemode.callservicemodeid=callsettings.callservicemodeid';
		$join .=' LEFT OUTER JOIN callservicetype ON callservicetype.callservicetypeid=callsettings.callservicetypeid';
		$join .=' LEFT OUTER JOIN provider ON provider.providerid=callsettings.providerid';
		$status = $tablename.'.status IN (1)';
		$industry = $tablename.'.industryid IN ('.$this->Basefunctions->industryid.')';
		$where = '1=1';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		//query
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$where.' AND '.$status.' AND '.$industry.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		return $data;
	}
	//sms credential edit data fetch
	public function callcredentialeditdatafetchmodel() {
		$primaryid = $_POST['datarowid'];
		$this->db->select('SQL_CALC_FOUND_ROWS  callsettings.callnumbertypeid,callsettings.callservicemodeid,callsettings.providerid,callsettings.callservicetypeid,callsettings.mobilenumber,callsettings.callsettingsid,callsettings.username,callsettings.password,callsettings.apikey',false);
		$this->db->from('callsettings');
		$this->db->where('callsettings.callsettingsid',$primaryid);
		$this->db->where_not_in('callsettings.status',array(3,0));
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data = array('numbertype'=>$row->callnumbertypeid,'servicemode'=>$row->callservicemodeid,'provider'=>$row->providerid,'servicetype'=>$row->callservicetypeid,'mobile'=>$row->mobilenumber,'username'=>$row->username,'password'=>$row->password,'apikey'=>$row->apikey);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//sms credential update
	public function callcredentialupdatemodel(){
		//master company id get\
		$mcid = $this->usermastercompanyid();
		$primaryid = $_POST['primaryid'];
		$provider = $_POST['provider'];
		$apikey = $_POST['apikey'];
		$username = $_POST['username'];
		$password = $_POST['password'];
		if(isset($_POST['servicemode'])){
			$servicemode = $_POST['servicemode']; 
			if($servicemode == ''){
				$servicemode = 1;
			}
		} else {
			$servicemode = 1;
		}
		if(isset($_POST['numbertype'])){
			$numbertype = $_POST['numbertype'];
			if($numbertype == '' || $numbertype == 'null'){
				$numbertype = 1;
			}
		} else {
			$numbertype = 1;
		}
		if(isset($_POST['servicetype'])){
			$servicetype = $_POST['numbertype'];
			if($servicetype == '' || $servicetype == 'null'){
				$servicetype = 1;
			}
		} else {
			$servicetype = 1;
		}
		$mobilenumber = $_POST['mobile'];
		$date= date($this->Basefunctions->datef);
		$userid= $this->Basefunctions->userid;
		$tstatus = $this->Basefunctions->activestatus;
		$callsettings=array( 
				'mastercompanyid'=>$mcid,
				'callservicetypeid'=>$servicetype,
				'providerid'=>$provider,
				'callservicemodeid'=>$servicemode,
				'callnumbertypeid'=>$numbertype,
				'mobilenumber'=>$mobilenumber,
				'username'=>$username,
				'password'=>$password,
				'apikey'=>$apikey,
				'createdate'=>$date,
				'lastupdatedate'=>$date,
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
				'status'=>$tstatus
			);
		$this->db->where('callsettings.callsettingsid',$primaryid);
		$this->db->update('callsettings',$callsettings);
		echo 'TRUE';
	}
	//sms credential delete
	public function callcredentialdeletemodel() {
		$primaryid = $_POST['primaryid'];
		$callsettings=array( 
				'status'=>'0',
				'lastupdatedate'=>date($this->Basefunctions->datef),
				'lastupdateuserid'=>$this->Basefunctions->userid
			);
		$this->db->where('callsettings.callsettingsid',$primaryid);
		$this->db->update('callsettings',$callsettings);
		echo 'TRUE';
	}
	//mail notification sent
	public function mailsentfunction($message,$send_at) {
		try {
			$mandrill = new Mandrill('X9WnMXpxhC8YEOUPuQ3oZw');
			$async = true; //for bulk message
			$ip_pool = '';
			$result = $mandrill->messages->send($message, $async, $ip_pool, $send_at);
		}catch(Mandrill_Error $e) {
			// Mandrill errors are thrown as exceptions
			echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			throw $e;
		}
	}
	//service mode dd load-
	public function servicemodeddreloadmodel(){
		$i = 0;
		$table= $_GET['table'];
		$filedid= $_GET['filedid'];
		$filedname= $_GET['filedname'];
		$this->db->select($filedid.','.$filedname);
		$this->db->from($table);
		$this->db->where($table.'.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data[$i] = array('datasid'=>$row->$filedid,'dataname'=>$row->$filedname);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}	
	}
}