<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Categorymodel extends CI_Model{
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//create counter
	public function newdatacreatemodel() {
		$industryid = $this->Basefunctions->industryid;
		if($industryid == 3){
			$category_level = $this->Basefunctions->get_company_settings('category_level'); //category_level
			$cloneproductid  = 1;
			if($_POST['parentcategoryid'] > 0){
				$level = $this->Basefunctions->gettreelevel($_POST['parentcategoryid'],'category');
			}else{
				$level = 1;
			}
		}
		 //table and fields information	
		$formfieldsname = explode(',',$_POST['categoryelementsname']);
		$formfieldstable = explode(',',$_POST['categoryelementstable']);
		$formfieldscolmname = explode(',',$_POST['categoryelementscolmn']);
		$categoryelementpartable = explode(',',$_POST['categoryelementspartabname']);
		if($industryid == 3){
			if($level <= $category_level)
			{
			//filter unique parent table
			$partablename =  $this->Crudmodel->filtervalue($categoryelementpartable);
			//filter unique fields table
			$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
			$tableinfo = explode(',',$fildstable);
			$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
			$setdefault = $_POST['defaultcategory'];
			$primaryid = $this->db->insert_id();
			$this->setdefaultupdate($setdefault,$primaryid);
			}else{
				echo 'LEVEL'; exit;
			}
		}else{
			//filter unique parent table
			$partablename =  $this->Crudmodel->filtervalue($categoryelementpartable);
			//filter unique fields table
			$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
			$tableinfo = explode(',',$fildstable);
			$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
			$setdefault = $_POST['defaultcategory'];
			$primaryid = $this->db->insert_id();
			$this->setdefaultupdate($setdefault,$primaryid);
		}
		if($industryid == 3){
			$sortorder = array(
					'categorysortorder'=>$_POST['categorysortorder']
			);
			$this->db->where('categoryid',$primaryid);
			$this->db->update('category',$sortorder);
			$level  =0;
			if($_POST['parentcategoryid'] > 0){
				$level = $this->Basefunctions->gettreelevel($_POST['parentcategoryid'],'category');
				$cloneproductid = $this->Basefunctions->singlefieldfetch('parentproductid','categoryid','category',$_POST['parentcategoryid']);
				$supercategoryid = $this->getsupercategoryid($_POST['parentcategoryid']);
			} else {
				$level = 1;
				$supercategoryid = $result;
			}
			$this->Basefunctions->updatechargecloneforchild($primaryid,$cloneproductid); 
			$update_last=array(
					'categorylevel'=>$level,
					'parentproductid'=>$cloneproductid,
					'supercategoryid'=>$supercategoryid
			);
			$this->db->where('categoryid',$primaryid);
			$this->db->update('category',$update_last);
		}
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity = ''.$user.' created Category - '.$_POST['categoryname'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Created',$activity ,$userid,8);
		echo 'TRUE';
	}
	//retrive counter data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['categoryelementsname']);
		$formfieldstable = explode(',',$_GET['categoryelementstable']);
		$formfieldscolmname = explode(',',$_GET['categoryelementscolmn']);
		$categoryelementpartable = explode(',',$_GET['categoryelementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($categoryelementpartable);
		$primaryid = $_GET['categoryprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$sndefault = $this->Basefunctions->singlefieldfetch('sndefault','categoryid','category',$primaryid); //Check default value
		if($sndefault == 1) {
			$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
			echo $result;
		} else {
			echo json_encode(array("fail"=>'sndefault'));
		}
	}
	//update counter
	public function datainformationupdatemodel() {
		$industryid = $this->Basefunctions->industryid;
		//get category level
		$editcategorylevel = $this->geteditcategorylevel($_POST['categoryprimarydataid']);
		if($industryid == 3){
			$category_level = $this->Basefunctions->get_company_settings('category_level'); //category_level
			if($_POST['parentcategoryid'] > 0){
				$level = $this->Basefunctions->gettreelevel($_POST['parentcategoryid'],'category');
			}else{
				$level = 1;
			}
		}
		//table and fields information
		$formfieldsname = explode(',',$_POST['categoryelementsname']);
		$formfieldstable = explode(',',$_POST['categoryelementstable']);
		$formfieldscolmname = explode(',',$_POST['categoryelementscolmn']);
		$categoryelementpartable = explode(',',$_POST['categoryelementspartabname']);
		if($industryid == 3){
			if($level <= $category_level)
			{
				if($_POST['parentcategoryid'] == 0){ // parent category only
					$info=0;
				}else{
					if($_POST['categoryprimarydataid'] > $_POST['parentcategoryid']){ // child - parent category
						$info=0;
					}else{ // parent - child category
						$info=$this->Basefunctions->singlefieldfetch('COUNT(categoryid)','parentcategoryid','category',$_POST['parentcategoryid']);
					}
				}
				if($info<=0)
				{
					//filter unique parent table
					$partablename =  $this->Crudmodel->filtervalue($categoryelementpartable);
					//filter unique fields table
					$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
					$tableinfo = explode(',',$fildstable);
					$primaryid = $_POST['categoryprimarydataid'];
					$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
					$setdefault = $_POST['defaultcategory'];
					$this->setdefaultupdate($setdefault,$primaryid);
				}
				else
				{
					echo "FAILURE"; exit;
				}
			}else{
				echo 'LEVEL'; exit;
			}
		}else{
			//filter unique parent table
			$partablename =  $this->Crudmodel->filtervalue($categoryelementpartable);
			//filter unique fields table
			$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
			$tableinfo = explode(',',$fildstable);
			$primaryid = $_POST['categoryprimarydataid'];
			$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
			$setdefault = $_POST['defaultcategory'];
			$this->setdefaultupdate($setdefault,$primaryid);
				
		}
		if($industryid == 3) {
			$sortorder = array(
					'categorysortorder'=>$_POST['categorysortorder']
			);
			$this->db->where('categoryid',$primaryid);
			$this->db->update('category',$sortorder);
			$level  =0;
			$cloneproductid = 1;
			if($_POST['parentcategoryid'] > 0){
				$level = $this->Basefunctions->gettreelevel($_POST['parentcategoryid'],'category');
				$cloneproductid = $this->Basefunctions->singlefieldfetch('parentproductid','categoryid','category',$_POST['parentcategoryid']);
				$supercategoryid = $this->getsupercategoryid($_POST['parentcategoryid']);
			} else {
				$level = 1;
				$supercategoryid = $primaryid;
			}
			$this->Basefunctions->updatechargecloneforchild($primaryid,$cloneproductid); 
			$update_last=array(
					'categorylevel'=>$level,
					'parentproductid'=>$cloneproductid,
					'supercategoryid'=>$supercategoryid
			);
			$this->db->where('categoryid',$primaryid);
			$this->db->update('category',$update_last);
			
		}
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$categoryname = $this->Basefunctions->singlefieldfetch('categoryname','categoryid','category',$primaryid); //Category name
		$activity = ''.$user.' updated Category - '.$categoryname.'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$activity ,$userid,8);
		echo $result;
	}
	//tree level generate
	public function getsupercategoryid($id) {
		$parentcatgid = $this->Basefunctions->singlefieldfetch('parentcategoryid','categoryid','category',$id);
		if($parentcatgid == 0) {
			return $id;
		} else {
			return $this->getsupercategoryid($parentcatgid);
		}
	}
	//set default update
	public function setdefaultupdate($setdefault,$primaryid) {
		if($setdefault != 'No') {
			$updatearray = array('defaultcategory'=>'No');
			$this->db->where_not_in('category.categoryid',$primaryid);
			$this->db->update('category',$updatearray);
		}
	}
	//get edit category level
	public function geteditcategorylevel($primaryid,$level=array()) {
		$this->db->select('categoryid,categorylevel');
		$this->db->from('category');
		$this->db->where('category.parentcategoryid',$primaryid);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$level[] = $row->categorylevel;
				$this->geteditcategorylevel($row->categoryid,$level);
			}
		} else {
			/* print_r($level);
			$maxlevel =  max($level); */
			return $level;
		}
		
	}
	//delete tree counter
	public function deleteoldtreeinformation() {
		$formfieldstable = explode(',',$_GET['categoryelementstable']);
		$parenttable = explode(',',$_GET['categoryparenttable']);
		$id = $_GET['categoryprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//delete function
		$id = $_GET['categoryprimarydataid'];
		$parent = $id;
		$sndefault = $this->Basefunctions->singlefieldfetch('sndefault','categoryid','category',$id); //Check default value
		if($sndefault == 2) {
			echo 'sndefault';
		} else {
			$this->Crudmodel->treedatadeletefunction($partabname,$id,$parent);
			//audit-log
			$user = $this->Basefunctions->username;
			$userid = $this->Basefunctions->logemployeeid;
			$categoryname = $this->Basefunctions->singlefieldfetch('categoryname','categoryid','category',$id); //Category name
			$activity = ''.$user.' deleted Category - '.$categoryname.'';
			$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,8);
			echo "TRUE";
		}
	}
	//delete counter
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['categoryelementstable']);
		$categoryparenttable = explode(',',$_GET['categoryparenttable']);
		$id = $_GET['categoryprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($categoryparenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				echo "TRUE";
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			echo "TRUE";
		}
	} 
	public function getcategoryparent() {
		$parentcategoryid='';
		$categoryname='';
		$id=$_GET['id'];
		$data=$this->db->select('a.parentcategoryid as parentcategoryids,b.categoryname as categorynames')
				->from('category as a')
				->join('category as b','a.parentcategoryid=b.categoryid')
				->where('a.categoryid',$id)
				->get()->result();
		foreach($data as $in) {
			$parentcategoryid=$in->parentcategoryids;
			$categoryname=$in->categorynames;
		}
		$a=array('parentcategoryid'=>$parentcategoryid,'categoryname'=>$categoryname);
		echo json_encode($a);
	}
	// Loose stone category check
	public function loosestonecheck() {
		echo $this->Basefunctions->singlefieldfetch('loosestone','categoryid','category',$_POST['primaryid']);
	}
}