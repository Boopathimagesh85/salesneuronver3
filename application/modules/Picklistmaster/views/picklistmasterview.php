<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
</head>
<style type="text/css">
	#picklistdependencyadvanceddrop, #picklistadvanceddrop{
		left: -82.1406px !important;
	}
	.tabs .dbtab-title, .multitabs .dbtab-title {
		height:38px !important;
	}
	.data-content {
		width: 1000px !important;
	}
	.gridcontent {
	height: 500px !important;
	}
	#picklistaddgrid {
		height:500px !important;
	}
</style>
<body>
<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<?php
				$modid = '211,212';
				$dataset['gridtitle'] = $gridtitle['title'];
				$dataset['titleicon'] = $gridtitle['titleicon'];
				$dataset['formtype'] = 'multipleaddform';
				$dataset['multimoduleid'] = $modid;
				$this->load->view('Base/mainviewaddformheader',$dataset);
			?>
			<div class="large-12 columns scrollbarclass pickmastertouchcontainer paddingzero">
				<?php
				
					//function call for dash board form fields generation
					picklistdashboardformfieldsgeneratorwithgrid($moddbfrmfieldsgen,1,$modid);
				?>
				
			</div>			
		</div>
	</div>
</div>
<?php
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$this->load->view('Base/overlaymobile');
		} else {
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
	?>
</body>
	<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<?php $this->load->view('Base/bottomscript'); ?>
	<script src="<?php echo base_url();?>js/plugins/sortable.js" type="text/javascript"></script>
	<script>
		//Tabgroup Dropdown More
		//fwgautocollapse();
		$("#tab1").click(function() {
			$(".tabclass").addClass('hidedisplay');
			$("#tab1class").removeClass('hidedisplay');
			resetFields();
			mastertabid=1;
			masterfortouch = 0;
		});
		$("#tab2").click(function()	{
			$(".tabclass").addClass('hidedisplay');
			$("#tab2class").removeClass('hidedisplay');
			resetFields();
			$("#picklistddmoduleid").focus();
			mastertabid=2;
			masterfortouch = 0;
		});
		$(document).ready(function(){
			$("#tabgropdropdown").change(function(){
				var tabgpid = $("#tabgropdropdown").val();
				$('.dbsidebaricons[data-dbsubform="'+tabgpid+'"]').trigger('click');
			});
			{//for keyboard global variable
				 viewgridview = 0;
				 innergridview = 1;
			}
		});	
		{//enable support icon
			$(".supportoverlay").css("display","inline-block")
		}	
	</script>
</html>