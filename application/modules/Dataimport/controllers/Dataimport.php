<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dataimport extends CI_Controller {
	function __construct() {
    	parent::__construct();
    	$this->load->helper('formbuild');
		$this->load->model('Dataimport/Dataimportmodel');
		$this->load->model('Base/Basefunctions');
    }
	public function index() {
		$moduleid = array(253);
		sessionchecker($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['filetype'] = $this->Basefunctions->simpledropdownwithcond('filetypeid','filetypename,setdefault','filetype','fieldtypeid','3');
		$data['module'] = $this->Basefunctions->simpledropdownwithcond('moduleid','modulename,menuname','module','moduleprivilegeid',253);
		$data['leadsource'] = $this->Basefunctions->simpledropdownwithgrp('source','sourceid,sourcename','sourcename','sourcename');
		$data['leadassign'] = $this->Basefunctions->userspecificgroupdropdownvalfetch();
		$this->load->view('Dataimport/dataimportview',$data);
	}
	//file header read
	public function dataimportcolumninformationfetch() {
		$this->Dataimportmodel->dataimportcolumninformationfetchmodel();
	}
	//internal header read
	public function datacolumninformationfetch() {
		$moduleid = $_GET['moduleid'];
		$data = $this->Dataimportmodel->datacolumninformationfetchmodel($moduleid);
		echo json_encode($data);
	}
	//column match grid information fetch
	public function dataimportcolumnmatchgridinfo() {
		$this->Dataimportmodel->dataimportcolumnmatchgridinfomodel();
	}
	//new data creation from file
	public function newuserdatacreationfromfile() {
		$this->Dataimportmodel->newuserdatacreationfromfilemodel();
	}
	/* Field mapping grid header information */ 
	public function fieldmapheaderinformationfetch() {
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$modname = $_GET['modulename'];
		$colinfo = $this->Dataimportmodel->fieldmapheaderinformationfetchmodel($modid);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height);
		} else {
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height);
		}
		echo json_encode($datas);
	}
	/* Confirm grid header information */
	public function confirmgridheaderinformationfetch() {
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$modname = $_GET['modulename'];
		$colinfo = $this->Dataimportmodel->confirmgridheaderinformationfetchmodel($modid);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height);
		} else {
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height);
		}
		echo json_encode($datas);
	}
}