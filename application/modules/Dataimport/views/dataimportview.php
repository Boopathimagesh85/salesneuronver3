<!DOCTYPE HTML>
<html lang="en">
<head>
<!-- Base Header File -->
<?php
	$this->load->view('Base/headerfiles');
	$device = $this->Basefunctions->deviceinfo();
	$defaultrecordview = $this->Basefunctions->get_company_settings('mainviewdefaultview');
	echo hidden('mainviewdefaultview',$defaultrecordview);
?>
</head>
<body class="hidedisplay">
	<div class="row" style="max-width:100%">
		<div class="off-canvas-wrap" data-offcanvas>
			<div class="inner-wrap">
				<div class="gridviewdivforsh" style="" id="dataimportview">
					<?php
						$device = $this->Basefunctions->deviceinfo();
						$dataset['gridtitle'] = $gridtitle['title'];
						$dataset['titleicon'] = $gridtitle['titleicon'];
						$dataset['gridid'] = 'datimportviewgrid';
						$dataset['gridwidth'] = 'datimportviewgridwidth';
						$dataset['gridfooter'] = 'datimportviewgridfooter';
						$dataset['formtype'] = 'dataimport';
						$this->load->view('Base/mainviewheader',$dataset);
						$dataset['moduleid'] = '253';
						$this->load->view('Base/singlemainviewformwithgrid.php',$dataset);
					?>
				</div>
			</div>
		</div>
	</div>
	<div id="importdetailsform" class="hidedisplay">
		<?php
			$this->load->view('dataimportform'); 
		?>
	</div>
		</div>
	</div>
	<?php
		if($device=='phone') {
			$this->load->view('Base/overlaymobile');
		} else {
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
	?>
</body>
	<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js File -->
	<script src="<?php echo base_url();?>js/Dataimport/dataimport.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/filecomputer/fileupload.min.js" type="text/javascript"></script>
	<!--For Tablet and Mobile view Dropdown Script-->
	<script>
		$(document).ready(function(){
			$("#tabgropdropdown").change(function(){
				var tabgpid = $("#tabgropdropdown").val(); 
				$('.sidebariconstab[data-subform="'+tabgpid+'"]').trigger('click');
			});
			//for touch
			mastertabid = 1;
			$("#tab1").click(function()	{mastertabid=1;	});
			$("#tab2").click(function()	{mastertabid=2;	});
			$("#tab3").click(function()	{mastertabid=3; });			
		});			
	</script>
</html>