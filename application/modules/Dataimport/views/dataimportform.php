<?php
	$device = $this->Basefunctions->deviceinfo();
	$dataset['gridtitle']=$gridtitle;
	$dataset['titleicon']=$titleicon;
	$dataset['moduelid']=253;
	$dataset['formtype']= 'stepper';
	$this->load->view('Base/mainviewaddformheader',$dataset);
?>
<div class="large-12 columns addformunderheader">&nbsp;</div>
<div class="large-12 columns scrollbarclass addformcontainer dataimporttouchcontainer">
	<div class="row">&nbsp;</div>
	<div id="subformspan1" class="hiddensubform">
		<form method="POST" name="dataimportfileform" action ="" id="dataimportfileform">
			<span id="adddataimportfiletabvalidate" class="validationEngineContainer" >
				<span id="editdataimportfiletabvalidate" class="validationEngineContainer">
					<div class="large-4 columns  paddingbtm">
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
							<div class="large-12 columns headerformcaptionstyle">Import Settings</div>
							<div class="static-field large-12 columns">
								<label>Module Name<span class="mandatoryfildclass">*</span></label>
								<select id="importmoduleid" class="chzn-select validate[required]" data-prompt-position="topLeft:14,36" name="importmoduleid" data-placeholder="Select module" tabindex="101">
									<option value="">Select</option>
									<?php foreach($module as $key):
									?>
										<option value="<?php echo $key->moduleid; ?>"><?php echo $key->modulename; ?> </option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="static-field  large-12 columns">
								<label>File Type<span class="mandatoryfildclass">*</span></label>
								<select id="importfiletypeid" data-prompt-position="topLeft:14,36" class="chzn-select validate[required]" name="importfiletypeid" tabindex="101">
									<?php foreach($filetype as $key):
										$optselect="";
										if($key->setdefault == 'Yes') {
											$optselect = 'selected="selected"';
										}
									?>
										<option value="<?php echo $key->filetypename; ?>" <?php echo $optselect; ?>><?php echo $key->filetypename; ?> </option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="static-field large-12 columns">
								<label>Character Encoding</label>
								<select id="importcharencode" class="chzn-select" name="importcharencode" tabindex="102">
									<option value="UTF-8" selected='selected'>UTF-8 (Unicode)</option>
									<option value="UTF-16">UTF-16 (Unicode)</option>									
								</select>
							</div>
							<div class="static-field large-12 columns">
								<label>Delimiter</label>
								<select id="importdelimiter" class="chzn-select" tabindex="103" name="importdelimiter">
									<option value=",">comma</option>
									<option value=";">semicolon</option>
								</select>
							</div>
							<div class="large-12 columns">&nbsp;</div>
						</div>
					</div>
					<div class="large-4 columns  paddingbtm">
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
							<div class="large-12 columns headerformcaptionstyle">Additional Details</div>
							<div class="static-field large-12 columns hidedisplay" id="impleadsourcespan">
								<label>Lead Source</label>
								<select id="importleadsource" class="chzn-select" name="importleadsource" data-placeholder="Select source" tabindex="104">
									<option value="">Select source</option>
									<?php foreach($leadsource as $key):?>
										<option value="<?php echo $key->sourceid; ?>"><?php echo $key->sourcename; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="static-field large-12 columns hidedisplay" id="impassigntospan">
								<label>Assign To</label>
								<select id="leadassign" class="chzn-select"  name="leadassign" data-placeholder="Select" tabindex="104">
									<option value="">Select</option>
									<?php
										$prev = ' ';
										for($i=0;$i<count($leadassign);$i++) {
											if($leadassign[$i]['CName'] != '') {
												$cur = $leadassign[$i]['PId'];
												$a ="";
												if($prev != $cur ) {
													if($prev != " ") {
														echo '</optgroup>';
													}
													echo '<optgroup  label="'.$leadassign[$i]['PName'].'" class="'.str_replace(' ','',$leadassign[$i]['PName']).'">';
													$prev = $leadassign[$i]['PId'];
													$a = "pclass";
												}
												echo '<option value="'.$leadassign[$i]['PId'].':'.$leadassign[$i]['CId'].'" >'.$leadassign[$i]['CName'].'</option>';
											}
										}
									?>
								</select>
							</div>
							<div class="static-field large-12 columns">
								<label>Duplicate Records</label>
								<select id="importduplicate" class="chzn-select" data-placeholder="Select" tabindex="105" name="importduplicate">
									<option value="">Select</option>
									<option value="Skip">Skip</option>
									<option value="Overwrite">Overwrite</option>
								</select>
							</div>
							<div class="static-field large-12 columns">
								<label>Merge Records Criteria<span class="mandatoryfildclass" id="mergcritmandoptspan"></span></label>
								<select id="dataimpmergerec" class="chzn-select"  data-placeholder="Select" id="dataimpmergerec" multiple="multiple" data-prompt-position="topLeft:14,36" tabindex="106">
								</select>
								<!-- hidden fields -->
								<input type="hidden" name="mergefildvalueset" id="mergefildvalueset" value="" />
							</div>
							<div class="large-12 columns">&nbsp;</div>
						</div>
					</div>
					<div class="large-4 columns  paddingbtm">
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
							<div class="large-12 columns headerformcaptionstyle">Select File</div>
							<div class="large-12 columns">
								<label>Select File<span class="mandatoryfildclass">*</span></label>
								<span id="dataimportfileuploader" style="display: none;">upload</span>
								<input type="button" class="btn validate[required]" id="dataimportfileuploadtrigger" value="Browse" tabindex="108"/>
								<div><span id="dataimpfnamesapn" style="font-size:0.8rem; color:black;"></span></div>
							</div>
							<div class="large-12 columns">&nbsp;</div>
						</div>
						<!-- hidden fields -->
						<input type="hidden" name="dataimportfilename" id="dataimportfilename" value="" />
					</div>
				</span>
			</span>
		</form>
	</div>
	<div id="subformspan2" class="hidedisplay hiddensubform">
		<form method="POST" name="importdatamapform" action ="" id="importdatamapform">
			<span id="importdatamapaddformvalidate" class="validationEngineContainer" >
				<span id="importdatamapeditformvalidate" class="validationEngineContainer">
					<div class="large-4 columns  paddingbtm borderstyle"  style="background:#fff;top: -5px;left: 5px;">
						<div class="large-12 columns clearmapdataform" style="padding-left: 0 !important; padding-right: 0 !important;background: #fff">
							<div class="large-12 columns headerformcaptionstyle" style="top:0px !important;">Field Mapping</div>
							<div class="static-field large-12 columns">
								<label>Imported Column Name<span class="mandatoryfildclass">*</span></label>
								<select id="dataimportcolnames" class="chzn-select validate[required]" name="dataimportcolnames" data-placeholder='Select' data-prompt-position="topLeft:14,36" tabindex="109">
								</select>
							</div>
							<div class="static-field large-12 columns">
								<label>Internal Column Name<span class="mandatoryfildclass">*</span></label>
								<select id="dataimpinternalcolname" class="chzn-select validate[required]" name="dataimpinternalcolname" data-placeholder='Select' data-prompt-position="topLeft:14,36" tabindex="110">
								</select>
							</div>
							<div class="large-12 columns" style="text-align:right">
								<label></label>
								<input id="importdatamapaddbtn" class="btn" type="button" value="Submit" tabindex="111" name="importdatamapaddbtn">
							</div>
							<div class="large-12 columns">&nbsp;</div>
						</div>
					</div>
				</span>
			</span>
		</form>
		<div class="large-8 columns paddingbtm">
			<div class="large-12 columns paddingzero">
				<div class="large-12 columns viewgridcolorstyle paddingzero borderstyle">
					<div class="large-12 columns headerformcaptionstyle" style=" height:auto; padding: 0.2rem;">
						<span class="large-6 medium-6 small-12 columns lefttext small-only-text-center" style="top:0px !important;">Field Mapping List</span>
						<span class="large-6 medium-6 small-12 columns innergridicon righttext small-only-text-center">
							<span id="colmapdelicon" class="icon icon-bin deleteiconclass " title="Delete"> </span>
						</span>
					</div>
					<?php
						$device = $this->Basefunctions->deviceinfo();
						if($device=='phone') {
							echo '<div class="large-12 columns paddingzero forgetinggridname" id="fieldmappingaddgridwidth"><div class="row-content inner-gridcontent" id="fieldmappingaddgrid">
								<!-- Table header content , data rows & pagination for mobile-->
							</div>
							<footer class="inner-gridfooter footercontainer" id="fieldmappingaddgridfooter">
								<!-- Footer & Pagination content -->
							</footer></div>';
						} else {
							echo '<div class="large-12 columns forgetinggridname" id="fieldmappingaddgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="fieldmappingaddgrid" style="max-width:2000px; height:420px;top:0px;">
							<!-- Table content[Header & Record Rows] for desktop-->
							</div>
							<!--<div class="inner-gridfooter footer-content footercontainer" id="fieldmappingaddgridfooter">
								 Footer & Pagination content 
							</div></div>-->';
						}
					?>
				</div>
			</div>
		</div>
		<!-- hidden fileds -->
		<input type="hidden" name="importcolnameinfo" id="importcolnameinfo" value="" />
	</div>
	</div>
	<div id="subformspan3" class="hidedisplay hiddensubform"> 
		<form method="POST" name="" class="" action ="" id="">
			<span id="" class="validationEngineContainer" >
				<span id="" class="validationEngineContainer">
					<div class="large-4 columns  paddingbtm">
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #617d8a">
							<div class="large-12 columns headerformcaptionstyle">Send Notification</div>
							<div class="large-12 columns">
								<input type="checkbox" id="emptyid" class="filled-in" tabindex="112">
								<label for="emptyid">Mail Notification</label>
							</div>
							<div class="large-12 columns" style="text-align:right">
								<label></label>
								<input id="dataimportconfirmbtn" class="btn" type="button" value="Submit" tabindex="113" name="dataimportconfirmbtn">
							</div>
							<div class="large-12 columns">&nbsp;</div>
						</div>
					</div>
				</span>
			</span>
		</form>
		<div class="large-8 columns paddingbtm">
			<div class="large-12 columns paddingzero">
				<div class="large-12 columns viewgridcolorstyle paddingzero borderstyle">
					<div class="large-12 columns headerformcaptionstyle" style="height:auto; padding: 0.2rem;">
						<span class="large-6 medium-6 small-12 columns lefttext small-only-text-center">Field Mapping List</span>
						<span class="large-6 medium-6 small-12 columns innergridicon righttext small-only-text-center">
						</span>
					</div>
					<?php
						if($device=='phone') {
							echo '<div class="large-12 columns paddingzero forgetinggridname" id="confirmgridwidth"><div class="row-content inner-gridcontent" id="confirmgrid">
								<!-- Table header content , data rows & pagination for mobile-->
							</div>
							<footer class="inner-gridfooter footercontainer" id="confirmgridfooter">
								<!-- Footer & Pagination content -->
							</footer></div>';
						} else {
							echo '<div class="large-12 columns forgetinggridname" id="confirmgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="confirmgrid" style="max-width:2000px; height:420px;top:0px;">
							<!-- Table content[Header & Record Rows] for desktop-->
							</div>
							<!-- <div class="inner-gridfooter footer-content footercontainer" id="confirmgridfooter">
								Footer & Pagination content 
							</div></div>-->';
						}
					?>
				</div>
			</div>
		</div>
	</div>
</div>

<style type="text/css">
.large-12 .columns .paddingzero .gridview-container {
	top:-1px !important;
}
</style>