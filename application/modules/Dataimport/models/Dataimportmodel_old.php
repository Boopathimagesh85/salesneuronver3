<?php
Class Dataimportmodel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->model('Base/Crudmodel');
		$this->load->library('csvreader');
    }
	//filter values
	public function filtervalue($fields) {
		$fieldsinfo = array_unique( $fields ) ;
		$fieldname = implode(',',$fieldsinfo);
		return $fieldname;
	}
	//file content read
	public function dataimportcolumninformationfetchmodel() {
		$filePath = $_GET['fname'];
		$delimiter = $_GET['delm'];
		$data = $this->csvreader->parse_lineheader($filePath,$delimiter);
		echo json_encode($data);
	}
	//default value set array for add
	public function defaultvalueget() {
		$defdata['createdate'] = date($this->Basefunctions->datef);
		$defdata['lastupdatedate'] = date($this->Basefunctions->datef);
		$defdata['createuserid'] = $this->Basefunctions->userid;
		$defdata['lastupdateuserid'] = $this->Basefunctions->userid;
		$defdata['status'] = 1;
		return $defdata;
	}
	//primary key information fetch
	public function primaryinfo($table) {
		$primarydata = $this->db->query("SHOW KEYS FROM ".$table." WHERE Key_name = 'PRIMARY'")->result();
		foreach($primarydata as $key) {
			$primarykey = $key->Column_name;
		}
		return $primarykey;
	}
	//internal filed label fetch
	public function datacolumninformationfetchmodel($moduleid) {
		$i=0;
		$data = array();
		$this->db->select("moduletabid,columnname,tablename,uitypeid,fieldlabel,parenttable,moduletabsection.moduletabsectionid,moduletabsection.moduletabsectionname");
		$this->db->from('modulefield');
		$this->db->join('module','module.moduleid=modulefield.moduletabid');
		$this->db->join('moduletabsection','moduletabsection.moduletabsectionid=modulefield.moduletabsectionid');
		$this->db->where_in('moduletabid',$moduleid);
		$this->db->where_not_in('modulefield.status',array(0,3,4,10));
		$this->db->where('moduletabsection.status',1);
		$this->db->order_by('moduletabsection.moduletabsectionid','asc');
		$this->db->order_by('moduletabsection.sequence','asc');
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result()as $row) {
				$data[$i]=array('moduletabid'=>$row->moduletabid,'columnname'=>$row->columnname,'tablename'=>$row->tablename,'uitypeid'=>$row->uitypeid,'internalcolumnname'=>$row->fieldlabel,'parenttable'=>$row->parenttable,'sectionid'=>$row->moduletabsectionid,'sectionname'=>$row->moduletabsectionname);
				$i++;
			}
		}
		return $data;
	}
	//column match grid information fetch
	public function dataimportcolumnmatchgridinfomodel() {
		$data = array();
		$moduleid = $_GET['moduleid'];
		$filePath = $_GET['fname'];
		$delimiter = $_GET['delm'];
		$intcoldata = $this->datacolumninformationfetchmodel($moduleid);
		$impcoldata = $this->csvreader->parse_lineheader($filePath,$delimiter);
		$i=0;
		foreach($intcoldata as $colinformation) {
			$colname = $colinformation['internalcolumnname'];
			if( in_array($colname,$impcoldata) ) {
				$colinformation['importcolumnname']=$colname;
				$data[$i] = $colinformation;
				$i++;
			}
		}
		echo json_encode($data);
	}
	//new data creation from file
	public function newuserdatacreationfromfilemodel() {
		ini_set("memory_limit",-1);
		$moduleid = $_POST['importmoduleid'];
		$filetype = $_POST['importfiletypeid'];
		$filepath = $_POST['dataimportfilename'];
		if( (isset($_POST['importdelimiter'])) && $_POST['importdelimiter'] != "" ) {
			$delimiter = $_POST['importdelimiter'];
		} else {
			$delimiter = ',';
		}
		//grid datas
		$griddatacount = $_POST['count'];
		$griddata = $_POST['gridfielddata'];
		$filecontent = array();
		$filecontent = $this->csvreader->parse_lines($filepath,$delimiter);
		//custom fields
		$leadsource = $_POST['importleadsource'];
		$leadassign = $_POST['leadassign'];
		//merge datas
		$mergeoption = $_POST['importduplicate'];
		$mergefields = $_POST['mergefildvalueset'];
		$mergelabeldata = explode(',',$mergefields);
		//grid data manipulation
		$fieldname = array();
		$fieldtable = array();
		$columnname = array();
		$fieldpartable = array();
		$fielduitype = array();
		$fieldlabel = array();
		$formdata=json_decode( $griddata, true );
		for( $gd=0; $gd<$griddatacount; $gd++ ) {
			$fieldname[] = $formdata[$gd]['importcolumnname'];
			$fieldtable[] = $formdata[$gd]['tablename'];
			$columnname[] = $formdata[$gd]['columnname'];
			$fieldpartable[] = $formdata[$gd]['parenttable'];
			$fielduitype[] = $formdata[$gd]['uitypeid'];
			$fieldlabel[] = $formdata[$gd]['internalcolumnname'];
		}
		//data arrange for insertion
		$parenttable = $this->filtervalue($fieldpartable);
		$tablenames = $this->filtervalue($fieldtable);
		$tableinfo = explode(',',$tablenames);
		//record counts
		$totalrecords = count($filecontent);
		$skiprecords = 0;
		$createrecords = 0;
		$overwriterecords = 0;
		$mergerecords = 0;
		//default value get
		$defdataarr = $this->defaultvalueget();
		//parent table primary name
		$primaryname = $this->primaryinfo($parenttable);
		//address module ids
		$addrmodid = array(4,201,202,203,216,217,226,227,230,83,92,74,81,91,75,82,85,94,87,96,86,95,78,80);
		$priaddrmodid = array(4,201,203,83,92,75,82);
		$billaddrmodid = array(202,216,217,226,227,230,74,81,91,85,94,87,96,86,95,78,80);
		$dduitypeid = array(17,18,19,20,21,23,25,26,27,28,29);
		foreach($filecontent as $information) {
			$pdata = array();
			$pmergedata = array();
			$m=0;
			foreach($tableinfo as $tblname) {
				${'$cdata'.$m} = array();
				${'$caddrdata'.$m} = array();
				${'$cmerge'.$m} = array();
				${'$addrmerge'.$m} = array();
				$i = 0;
				foreach($fieldname as $fname) {
					//parent table insertion
					if( trim($parenttable) === trim($fieldtable[$i]) && trim($parenttable) === trim($tblname) ) {
						if( isset($information[$fname]) ) {
							$name = explode('_',$fname);
							if(in_array($fielduitype[$i],$dduitypeid)) {
								if( $information[$fname] == '' ) {
									if($fielduitype[$i] == 21) {
										$id = 0;
									} else {
										$id = 1;
									}
								} else {
									$id = $this->checklookuptabdata($fieldtable[$i],$columnname[$i],$information[$fname],$fielduitype[$i],$moduleid);
								}
								$pdata[$columnname[$i]] = $id;
							} else {
								if( in_array('password',$name) ) {
									$hash = password_hash($information[$fname], PASSWORD_DEFAULT);
									$pdata[$columnname[$i]] = $hash ;
								} else {
									if( ctype_alpha($information[$fname]) ) {
										$pdata[$columnname[$i]] = ucwords($information[$fname]);
									} else {
										$pdata[$columnname[$i]] = $information[$fname];
									}
								}
							}
							if( in_array($fieldlabel[$i],$mergelabeldata) ) {
								$pmergedata[$columnname[$i]] = $pdata[$columnname[$i]];
							}
						}
					} else if( trim($fieldtable[$i]) === trim($tblname) && trim($parenttable) !== trim($fieldtable[$i]) ) {
						//child table (address table or other child table insertion)
						if( in_array($moduleid,$addrmodid) ) { //child address module and other child table
							if( in_array($moduleid,$priaddrmodid) ) {
								$addtype = array('Primary','Secondary');
							} else if( in_array($moduleid,$billaddrmodid) ) {
								$addtype = array('Billing','Shipping');
							}
							$h=0;
							$addname = explode(' ',$fieldlabel[$i]);
							$addtypefiledchk="";
							$addtypefiledchk = $this->tablefieldnamecheck($tblname,'addresstypeid');
							if($addtypefiledchk == 'true') {//check its address table
								foreach($addtype as $type) {
									if( isset($information[$fname]) && in_array($type,$addname) ) {
										$name = explode('_',$fname);
										if( in_array($fielduitype[$i],$dduitypeid) ) {
											if( $information[$fname] == '' ) {
												if($fielduitype[$i] == 21) {
													$id = 0;
												} else {
													$id = 1;
												}
											} else {
												$id = $this->checklookuptabdata($fieldtable[$i],$columnname[$i],$information[$fname],$fielduitype[$i],$moduleid);
											}
											${'$caddrdata'.$h}[$columnname[$i]] = $id;
										} else {
											if( in_array('password',$name) ) {
												$hash = password_hash($information[$fname], PASSWORD_DEFAULT);
												${'$caddrdata'.$h}[$columnname[$i]] = $hash ;
											} else {
												if( ctype_alpha($information[$fname]) ) {
													${'$caddrdata'.$h}[$columnname[$i]] = ucwords($information[$fname]);
												} else {
													${'$caddrdata'.$h}[$columnname[$i]] = $information[$fname];
												}
											}
										}
										if( in_array($fieldlabel[$i],$mergelabeldata) ) {
											${'$addrmerge'.$h}[$columnname[$i]] = ${'$caddrdata'.$h}[$columnname[$i]];
										}
									}
									$h++;
								}
							} else { //its not address table
								if( isset($information[$fname]) ) {
									$name = explode('_',$fname);
									if( in_array($fielduitype[$i],$dduitypeid) ) {
										if( $information[$fname] == '' ) {
											if($fielduitype[$i] == 21) {
												$id = 0;
											} else {
												$id = 1;
											}
										} else {
											$id = $this->checklookuptabdata($fieldtable[$i],$columnname[$i],$information[$fname],$fielduitype[$i],$moduleid);
										}
										${'$cdata'.$m}[$columnname[$i]] = $id;
									} else {
										if( in_array('password',$name) ) {
											$hash = password_hash($information[$fname], PASSWORD_DEFAULT);
											${'$cdata'.$m}[$columnname[$i]] = $hash ;
										} else {
											if($fielduitype[$i]=='8') {
												$date = $this->Basefunctions->ymddateconversion( trim($information[$fname]) );
												${'$cdata'.$m}[$columnname[$i]] = $date;
											} else if( ctype_alpha($information[$fname]) ) {
												${'$cdata'.$m}[$columnname[$i]] = ucwords($information[$fname]);
											} else {
												${'$cdata'.$m}[$columnname[$i]] = $information[$fname];
											}
										}
									}
									if( in_array($fieldlabel[$i],$mergelabeldata) ) {
										${'$cmerge'.$m}[$columnname[$i]] = ${'$cdata'.$m}[$columnname[$i]];
									}
								}
							}
						} else if( trim($fieldtable[$i]) === trim($tblname) && trim($parenttable) !== trim($fieldtable[$i]) ) {
							//child table (other child table insertion)
							if( isset( $information[$fname] ) ) {
								$name = explode('_',$fname);
								if( in_array($fielduitype[$i],$dduitypeid) ) {
									if( $information[$fname] == '' ) {
										if($fielduitype[$i] == 21) {
											$id = 0;
										} else {
											$id = 1;
										}
									} else {
										$id = $this->checklookuptabdata($fieldtable[$i],$columnname[$i],$information[$fname],$fielduitype[$i],$moduleid);
									}
									${'$cdata'.$m}[$columnname[$i]] = $id;
								} else {
									if( in_array('password',$name) ) {
										$hash = password_hash($information[$fname], PASSWORD_DEFAULT);
										${'$cdata'.$m}[$columnname[$i]] = $hash ;
									} else {
										if($fielduitype[$i]=='8') {
											$date = $this->Basefunctions->ymddateconversion( trim($information[$fname]) );
											${'$cdata'.$m}[$columnname[$i]] = $date;
										} else if( ctype_alpha($information[$fname]) ) {
											${'$cdata'.$m}[$columnname[$i]] = ucwords($information[$fname]);
										} else {
											${'$cdata'.$m}[$columnname[$i]] = $information[$fname];
										}
									}
								}
								if( in_array($fieldlabel[$i],$mergelabeldata) ) {
									${'$cmerge'.$m}[$columnname[$i]] = ${'$cdata'.$m}[$columnname[$i]];
								}
							}
						} 
					}
					$i++;
				}
				$m++;
			}
			//perform import opn
			if($mergeoption == "Skip") { //Skip the records based on condition
				$data="";
				$recordcount = array();
				$m=0;
				foreach($tableinfo as $tblname) {
					if($tblname == $parenttable) {
						if(count($pmergedata)>0) {
							$data = $this->checkrecordduplication($parenttable,$pmergedata,$primaryname);
						}
					} else if( in_array($moduleid,$addrmodid) ) { //child address table
						$addtype = array('Billing','Shipping');
						$h=0;
						$addtypefiledchk="";
						$addtypefiledchk = $this->tablefieldnamecheck($tblname,'addresstypeid');
						if($addtypefiledchk == 'true') {
							foreach($addtype as $type) { //check in address table
								if( isset(${'$addrmerge'.$h}) ) {
									if(count(${'$addrmerge'.$h})>0) {
										$data = $this->checkrecordduplication($tblname,${'$addrmerge'.$h},$primaryname);
									}
								}
								$h++;
							}
						} else {
							if( isset(${'$cmerge'.$m}) ) { //other child table
								if(count(${'$cmerge'.$m})>0) {
									$data = $this->checkrecordduplication($tblname,${'$cmerge'.$m},$primaryname);
								}
							}
						}
					} else {
						if( isset(${'$cmerge'.$m}) ) {
							if(count(${'$cmerge'.$m})>0) {
								$data = $this->checkrecordduplication($tblname,${'$cmerge'.$m},$primaryname);
							}
						}
					}
					if($data != '') {
						$recordcount[] = $data;
					}
					$m++;
				}
				//record insertion
				if( count($recordcount) <= 0 ) {
					//auto number chk
					$autonumbers = $this->autonumbergenerate($parenttable,$moduleid);
					if(count($autonumbers)>0) {
						foreach($autonumbers as $autonumber) {
							$pdata[$autonumber['fname']]=$autonumber['value'];
						}
					}
					//parent table insertion
					$newpardata = array_merge($pdata,$defdataarr);
					$newpardata['industryid'] = $this->Basefunctions->industryid;
					$newpardata['branchid'] = $this->Basefunctions->branchid;
					//data insertion
					$this->db->insert( $parenttable, $newpardata );
					$primaryid = $this->db->insert_id();
					//lead number generate
					if( $moduleid == '201') {
						$leadupdata = array();
						if($leadsource != "") {
							$leadupdata['sourceid']=$leadsource;
						}
						if($leadassign != "") {
							$laid = explode(':',$leadassign);
							$leadupdata['employeetypeid']=$laid[0];
							$leadupdata['employeeid']=$laid[1];
						} else {
							$leadupdata['employeetypeid'] = 1;
							$leadupdata['employeeid'] = $this->Basefunctions->logemployeeid;
						}
						$this->db->where($primaryname,$primaryid);
						$this->db->update( $parenttable,$leadupdata );
					}
					//child table insertion
					if( in_array($moduleid,$addrmodid) ) { //child address table
						if( in_array($moduleid,$priaddrmodid) ) {
							$addtype = array('Primary','Secondary');
							$addtypeid = array(2,3);
						} else if( in_array($moduleid,$billaddrmodid) ) {
							$addtype = array('Billing','Shipping');
							$addtypeid = array(4,5);
						}
						$addmethod = array(4,5);
						$m=0;
						foreach( $tableinfo as $tblname ) {
							$h=0;
							$addtypefiledchk = "";
							$addtypefiledchk = $this->tablefieldnamecheck($tblname,'addresstypeid');
							if($addtypefiledchk == 'true') {
								foreach($addtype as $type) { //insert address table data
									$cnewdata = array();
									if( isset(${'$caddrdata'.$h}) ) {
										if(count(${'$caddrdata'.$h}) > 0 ) {
											if( strcmp( trim($parenttable),trim($tblname) ) != 0 ) {
												//auto number chk
												$autonumbers = $this->autonumbergenerate($tblname,$moduleid);
												if(count($autonumbers)>0) {
													foreach($autonumbers as $autonumber) {
														${'$caddrdata'.$h}[$autonumber['fname']]=$autonumber['value'];
													}
												}
												//address values
												${'$caddrdata'.$h}['addresstypeid']=$addtypeid[$h];
												${'$caddrdata'.$h}['addressmethod']=$addmethod[$h];
												${'$caddrdata'.$h}[$primaryname]=$primaryid;
												$cnewdata = array_merge(${'$caddrdata'.$h},$defdataarr);
												//data insertion
												$this->db->insert( $tblname, array_filter($cnewdata) );
												unset(${'$caddrdata'.$h}['addresstypeid']);
												unset(${'$caddrdata'.$h}['addressmethod']);
											}
										}
									}
									$h++;
								}
							} else if($addtypefiledchk != 'true') {
								//other child table
								$cnewdata = array();
								if( isset(${'$cdata'.$m}) ) {
									if(count(${'$cdata'.$m}) > 0 ) {
										if( strcmp( trim($parenttable),trim($tblname) ) != 0 ) {
											//auto number chk
											$autonumbers = $this->autonumbergenerate($tblname,$moduleid);
											if(count($autonumbers)>0) {
												foreach($autonumbers as $autonumber) {
													${'$cdata'.$m}[$autonumber['fname']]=$autonumber['value'];
												}
											}
											${'$cdata'.$m}[$primaryname]=$primaryid;
											$cnewdata = array_merge(${'$cdata'.$m},$defdataarr);
											//data insertion
											$this->db->insert( $tblname, $cnewdata );
										}
									}
								}
							}
							$m++;
						}
					} else {
						$m=0;
						foreach( $tableinfo as $tblname ) {
							$cnewdata = array();
							if( isset(${'$cdata'.$m}) ) {
								if(count(${'$cdata'.$m}) > 0 ) {
									if( strcmp( trim($parenttable),trim($tblname) ) != 0 ) {
										//auto number chk
										$autonumbers = $this->autonumbergenerate($tblname,$moduleid);
										if(count($autonumbers)>0) {
											foreach($autonumbers as $autonumber) {
												${'$cdata'.$m}[$autonumber['fname']]=$autonumber['value'];
											}
										}
										${'$cdata'.$m}[$primaryname]=$primaryid;
										$cnewdata = array_merge(${'$cdata'.$m},$defdataarr);
										//data insertion
										$this->db->insert( $tblname, $cnewdata );
									}
								}
							}
							$m++;
						}
					}
					$createrecords++;
				} else {
					$skiprecords++;
				}
			} else if($mergeoption == "Overwrite") {  //Override the records based on condition
				$recordcount = array();
				$m=0;
				foreach($tableinfo as $tblname) {
					$data="";
					if($tblname == $parenttable) {
						if(count($pmergedata)>0) {
							$data = $this->checkrecordduplication($parenttable,$pmergedata,$primaryname);
						}
					} else if( in_array($moduleid,$addrmodid) ) { //child address table
						$addtype = array('Billing','Shipping');
						$addtypefiledchk="";
						$addtypefiledchk = $this->tablefieldnamecheck($tblname,'addresstypeid');
						if($addtypefiledchk == 'true') {
							$h=0;
							foreach($addtype as $type) {
								if( isset(${'$addrmerge'.$h}) ) {
									if(count(${'$addrmerge'.$h})>0) {
										$data = $this->checkrecordduplication($tblname,${'$addrmerge'.$h},$primaryname);
									}
								}
								$h++;
							}
						} else {
							if( isset(${'$cmerge'.$m}) ) { //other child table
								if(count(${'$cmerge'.$m})>0) {
									$data = $this->checkrecordduplication($tblname,${'$cmerge'.$m},$primaryname);
								}
							}
						}
					} else {
						if( isset(${'$cmerge'.$m}) ) {
							if(count(${'$cmerge'.$m})>0) {
								$data = $this->checkrecordduplication($tblname,${'$cmerge'.$m},$primaryname);
							}
						}
					}
					if($data != "") {
						$recordcount[] = $data;
					}
					$m++;
				}
				if( count($recordcount) > 0 ) {
					foreach($recordcount as $rowids) {
						$ids = explode(',',$rowids);
						foreach($ids as $id) {
							//parent table overwrite
							if( $moduleid == '201') {
								unset($pdata['leadnumber']);
								if($leadsource != "") { $pdata['sourceid']=$leadsource;}
								if($leadassign != "") { 
									$laid = explode(':',$leadassign);
									$pdata['employeetypeid']=$laid[0];
									$pdata['employeeid']=$laid[1];
								} else {
									$pdata['employeetypeid'] = 1;
									$pdata['employeeid'] = $this->Basefunctions->logemployeeid;
								}
							}
							$newpardata = array_merge($pdata,$defdataarr);
							$this->db->where($primaryname,$id);
							$this->db->update($parenttable,$newpardata);

							//child table insertion
							if( in_array($moduleid,$addrmodid) ) { //child address table
								if( in_array($moduleid,$priaddrmodid) ) {
									$addtype = array('Primary','Secondary');
									$addmethod = array(2,3);
								} else if( in_array($moduleid,$billaddrmodid) ) {
									$addtype = array('Billing','Shipping');
									$addmethod = array(4,5);
								}
								$m=0;
								foreach( $tableinfo as $tblname ) { //child table insertion
									$addtypefiledchk = "";
									$addtypefiledchk = $this->tablefieldnamecheck($tblname,'addresstypeid');
									if($addtypefiledchk == 'true') {
										$h=0;
										foreach($addtype as $type) { //insert child address table
											$cnewdata = array();
											if( isset(${'$caddrdata'.$h}) ) {
												if(count(${'$caddrdata'.$h}) > 0 ) {
													if( strcmp( trim($parenttable),trim($tblname) ) != 0 ) {
														${'$caddrdata'.$h}['addresstypeid']=$addmethod[$h];
														$wherearray=array('addresstypeid'=>$addmethod[$h],$primaryname=>$id);
														$cnewdata = array_merge(${'$caddrdata'.$h},$defdataarr);
														//data update
														$this->db->where($wherearray);
														$this->db->update($tblname,$cnewdata);
														unset(${'$caddrdata'.$h}['addresstypeid']);
													}
												}
											}
											$h++;
										}
									} else if($addtypefiledchk != 'true') {
										$cnewdata = array();
										if( isset(${'$cdata'.$m}) ) {
											if(count(${'$cdata'.$m}) > 0 ) {
												if( strcmp( trim($parenttable),trim($tblname) ) != 0 ) {
													$cnewdata = array_merge(${'$cdata'.$m},$defdataarr);
													//data update
													$this->db->where($primaryname,$id);
													$this->db->update($tblname,$cnewdata);
												}
											}
										}
									}
									$m++;
								}
							} else {
								$m=0;
								foreach( $tableinfo as $tblname ) {
									$cnewdata = array();
									if( isset(${'$cdata'.$m}) ) {
										if(count(${'$cdata'.$m}) > 0 ) {
											if( strcmp( trim($parenttable),trim($tblname) ) != 0 ) {
												$cnewdata = array_merge(${'$cdata'.$m},$defdataarr);
												//data update
												$this->db->where($primaryname,$id);
												$this->db->update($tblname,$cnewdata);
											}
										}
									}
									$m++;
								}
							}
						}
						$overwriterecords++;
					}
				} else {
					//parent table insertion
					//auto number chk
					$autonumbers = $this->autonumbergenerate($parenttable,$moduleid);
					if(count($autonumbers)>0) {
						foreach($autonumbers as $autonumber) {
							$pdata[$autonumber['fname']]=$autonumber['value'];
						}
					}
					$newpardata = array_merge($pdata,$defdataarr);
					//data insertion
					$this->db->insert( $parenttable, $newpardata );
					$newpardata['industryid'] = $this->Basefunctions->industryid;
					$newpardata['branchid'] = $this->Basefunctions->branchid;
					$primaryid = $this->db->insert_id();
					//lead number generate
					if( $moduleid == '201') {
						$leadupdata = array();
						if($leadsource != "") {
							$leadupdata['sourceid']=$leadsource;
						}
						if($leadassign != "") {
							$laid = explode(':',$leadassign);
							$leadupdata['employeetypeid']=$laid[0];
							$leadupdata['employeeid']=$laid[1];
						} else {
							$leadupdata['employeetypeid'] = 1;
							$leadupdata['employeeid'] = $this->Basefunctions->logemployeeid;
						}
						$this->db->where($primaryname,$primaryid);
						$this->db->update( $parenttable,$leadupdata );
					}
					//child table insertion
					if( in_array($moduleid,$addrmodid) ) { //child address table
						if( in_array($moduleid,$priaddrmodid) ) {
							$addtype = array('Primary','Secondary');
							$addtypeid = array(2,3);
						} else if( in_array($moduleid,$billaddrmodid) ) {
							$addtype = array('Billing','Shipping');
							$addtypeid = array(4,5);
						}
						$addmethod = array(4,5);
						$m=0;
						foreach( $tableinfo as $tblname ) {
							$addtypefiledchk="";
							$addtypefiledchk = $this->tablefieldnamecheck($tblname,'addresstypeid');
							$h=0;
							if($addtypefiledchk == 'true') {
								foreach($addtype as $type) {
									$cnewdata = array();
									if( isset(${'$caddrdata'.$h}) ) {
										if(count(${'$caddrdata'.$h}) > 0 ) {
											if( strcmp( trim($parenttable),trim($tblname) ) != 0 ) {
												$addtypefiledchk="";
												$addtypefiledchk = $this->tablefieldnamecheck($tblname,'addresstypeid');
												if($addtypefiledchk == 'true') {
													${'$caddrdata'.$h}['addresstypeid']=$addtypeid[$h];
													${'$caddrdata'.$h}['addressmethod']=$addmethod[$h];
												}
												//auto number chk
												$autonumbers = $this->autonumbergenerate($tblname,$moduleid);
												if(count($autonumbers)>0) {
													foreach($autonumbers as $autonumber) {
														${'$caddrdata'.$h}[$autonumber['fname']]=$autonumber['value'];
													}
												}
												${'$caddrdata'.$h}[$primaryname]=$primaryid;
												$cnewdata = array_merge(${'$caddrdata'.$h},$defdataarr);
												//data insertion
												$this->db->insert( $tblname, array_filter($cnewdata) );
												unset(${'$caddrdata'.$h}['addresstypeid']);
												unset(${'$caddrdata'.$h}['addressmethod']);
											}
										}
									}
									$h++;
								}
							} else if($addtypefiledchk != 'true') {
								$cnewdata = array();
								if( isset(${'$cdata'.$m}) ) {
									if(count(${'$cdata'.$m}) > 0 ) {
										if( strcmp( trim($parenttable),trim($tblname) ) != 0 ) {
											//auto number chk
											$autonumbers = $this->autonumbergenerate($tblname,$moduleid);
											if(count($autonumbers)>0) {
												foreach($autonumbers as $autonumber) {
													${'$cdata'.$m}[$autonumber['fname']]=$autonumber['value'];
												}
											}
											${'$cdata'.$m}[$primaryname]=$primaryid;
											$cnewdata = array_merge(${'$cdata'.$m},$defdataarr);
											//data insertion
											$this->db->insert( $tblname, $cnewdata );
										}
									}
								}
							}
						}
						$m++;
					} else {
						$m=0;
						foreach( $tableinfo as $tblname ) {
							$cnewdata = array();
							if( isset(${'$cdata'.$m}) ) {
								if(count(${'$cdata'.$m}) > 0 ) {
									if( strcmp( trim($parenttable),trim($tblname) ) != 0 ) {
										//auto number chk
										$autonumbers = $this->autonumbergenerate($tblname,$moduleid);
										if(count($autonumbers)>0) {
											foreach($autonumbers as $autonumber) {
												${'$cdata'.$m}[$autonumber['fname']]=$autonumber['value'];
											}
										}
										${'$cdata'.$m}[$primaryname]=$primaryid;
										$cnewdata = array_merge(${'$cdata'.$m},$defdataarr);
										//data insertion
										$this->db->insert( $tblname, $cnewdata );
									}
								}
							}
							$m++;
						}
					}
					$createrecords++;
				}
			} else { //insert new records
				//auto number chk
				$autonumbers = $this->autonumbergenerate($parenttable,$moduleid);
				if(count($autonumbers)>0) {
					foreach($autonumbers as $autonumber) {
						$pdata[$autonumber['fname']]=$autonumber['value'];
					}
				}
				//parent table insertion
				$newpardata = array_merge($pdata,$defdataarr);
				$newpardata['industryid'] = $this->Basefunctions->industryid;
				$newpardata['branchid'] = $this->Basefunctions->branchid;
				$this->db->insert( $parenttable, $newpardata );
				//data insertion id
				$primaryid = $this->db->insert_id();
				//lead data update
				if( $moduleid == '201') {
					$leadupdata = array();
					if($leadsource != "") { $leadupdata['sourceid']=$leadsource;}
					if($leadassign != "") {
						$laid = explode(':',$leadassign);
						$leadupdata['employeetypeid']=$laid[0];
						$leadupdata['employeeid']=$laid[1];
					} else {
						$leadupdata['employeetypeid'] = 1;
						$leadupdata['employeeid'] = $this->Basefunctions->logemployeeid;
					}
					$this->db->where($primaryname,$primaryid);
					$this->db->update( $parenttable,$leadupdata );
				}
				//child table insertion
				if( in_array($moduleid,$addrmodid) ) { //child address table
					if( in_array($moduleid,$priaddrmodid) ) {
						$addtype = array('Primary','Secondary');
						$addtypeid = array(2,3);
					} else if( in_array($moduleid,$billaddrmodid) ) {
						$addtype = array('Billing','Shipping');
						$addtypeid = array(4,5);
					}
					$addmethod = array(4,5);
					$m=0;
					foreach( $tableinfo as $tblname ) {
						$h=0;
						$addtypefiledchk = "";
						$addtypefiledchk = $this->tablefieldnamecheck($tblname,'addresstypeid');
						if($addtypefiledchk == 'true') { //address table insertion
							foreach($addtype as $type) {
								$cnewdata = array();
								if( isset(${'$caddrdata'.$h}) ) {
									if(count(${'$caddrdata'.$h}) > 0 ) {
										if( strcmp( trim($parenttable),trim($tblname) ) != 0 ) {
											//auto number chk
											$autonumbers = $this->autonumbergenerate($tblname,$moduleid);
											if(count($autonumbers)>0) {
												foreach($autonumbers as $autonumber) {
													${'$caddrdata'.$h}[$autonumber['fname']]=$autonumber['value'];
												}
											}
											${'$caddrdata'.$h}['addresstypeid']=$addtypeid[$h];
											${'$caddrdata'.$h}['addressmethod']=$addmethod[$h];
											${'$caddrdata'.$h}[$primaryname]=$primaryid;
											$cnewdata = array_merge(${'$caddrdata'.$h},$defdataarr);
											//data insertion
											$this->db->insert($tblname,$cnewdata);
											unset(${'$caddrdata'.$h}['addresstypeid']);
											unset(${'$caddrdata'.$h}['addressmethod']);
										}
									}
								}
								$h++;
							}
						} else { //other child table insertion
							$cnewdata = array();
							if( isset(${'$cdata'.$m}) ) {
								if(count(${'$cdata'.$m}) > 0 ) {
									if( strcmp( trim($parenttable),trim($tblname) ) != 0 ) {
									//auto number chk
										$autonumbers = $this->autonumbergenerate($tblname,$moduleid);
										if(count($autonumbers)>0) {
											foreach($autonumbers as $autonumber) {
												${'$cdata'.$m}[$autonumber['fname']]=$autonumber['value'];
											}
										}
										${'$cdata'.$m}[$primaryname]=$primaryid;
										$cnewdata = array_merge(${'$cdata'.$m},$defdataarr);
										//data insertion
										$this->db->insert( $tblname, $cnewdata );
									}
								}
							}
						}
						$m++;
					}
				} else { //other child table
					$m=0;
					foreach( $tableinfo as $tblname ) {
						$cnewdata = array();
						if( isset(${'$cdata'.$m}) ) {
							if(count(${'$cdata'.$m}) > 0 ) {
								if( strcmp( trim($parenttable),trim($tblname) ) != 0 ) {
								//auto number chk
									$autonumbers = $this->autonumbergenerate($tblname,$moduleid);
									if(count($autonumbers)>0) {
										foreach($autonumbers as $autonumber) {
											${'$cdata'.$m}[$autonumber['fname']]=$autonumber['value'];
										}
									}
									${'$cdata'.$m}[$primaryname]=$primaryid;
									$cnewdata = array_merge(${'$cdata'.$m},$defdataarr);
									//data insertion
									$this->db->insert( $tblname, $cnewdata );
								}
							}
						}
						$m++;
					}
				}
				$createrecords++;
			}
		}
		{//activity log entry
			$userid = $this->Basefunctions->logemployeeid;
			$date = date($this->Basefunctions->datef);
			$unimportrecord = $totalrecords-($createrecords+$skiprecords+$overwriterecords);
			$arr = array(
				'activitylogname'=>'New data add via file',
				'moduleid'=>$moduleid,
				'activitylogtype'=>3,
				'processtime'=>$date,
				'employeeid'=>$userid,
				'totalrecords'=>$totalrecords,
				'createrecords'=>$createrecords,
				'skiprecords'=>$skiprecords,
				'overwriterecords'=>$overwriterecords,
				'mergerecords'=>$mergerecords,
				'unimportrecords'=>$unimportrecord,
				'industryid'=>$this->Basefunctions->industryid,
				'branchid'=>$this->Basefunctions->branchid,
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
				'createdate'=>$date,
				'lastupdatedate'=>$date,
				'status'=>1
			);
			$this->db->insert('activitylog',array_filter($arr));
			//notification import trigger_error
			if($createrecords != 0) { $create = "Created Records->".$createrecords." "; } else {$create = "";}
			if($overwriterecords != 0) { $over = "Overwrite Records->".$overwriterecords." "; } else {$over = "";}
			if($skiprecords != 0) { $skip = "Skipped Records->".$skiprecords." "; } else {$skip = "";}
			if($mergerecords != 0) { $merge = "Merged Records->".$mergerecords." "; } else {$merge = "";}
			$message = $create."".$over."".$skip."".$merge;
			$empid = $this->Basefunctions->logemployeeid;
			$modulename = $this->Basefunctions->generalinformaion('module','modulename','moduleid',$moduleid);
			$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
			$notimsg = $empname." "."Imported a ".$modulename." - ".$message;
			$this->Basefunctions->notificationcontentadd(1,'Import',$notimsg,1,$moduleid); 
		}
		if( $totalrecords>0 ) {
			unlink($filepath);
		}
		echo "TRUE";
	}
	//check record duplication
	public function checkrecordduplication($tablename,$valueset,$primaryid) {
		$id="";
		$tabid = $tablename."id";
		$this->db->select("$primaryid")->from($tablename);
		foreach($valueset as $key => $value) {
			$this->db->where($key,$value);
		}
		$this->db->where($tablename.'.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result()as $row) {
				if($id == "") {
					$id=$row->$primaryid;
				} else {
					$rowids=$row->$primaryid;
					$id .= ','.$rowids;
				}
			}
		}
		return $id;
	}
	//table field name check
	public function tablefieldnamecheck($tblname,$fieldname) {
		$result = 'false';
		$fields = $this->db->field_data($tblname);
		foreach ($fields as $field) {
			$name =  $field->name;
			if($fieldname == $name) {
				$result = 'true';
			}
		}
		return $result;
	}
	//table name check
	public function tableexitcheck($database,$newtable) {
		$counttab=0;
		$tabexits = $this->db->query("SELECT COUNT(*) AS tabcount FROM information_schema.tables WHERE table_schema = '".$database."'AND table_name = '".$newtable."'");
		foreach($tabexits->result() as $tkey) { 
			$counttab = $tkey->tabcount;
		};
		return $counttab;
	}
	//look up table value check
	public function checklookuptabdata($table,$columnname,$colvalue,$uitypeid,$moduleid) {
		$id = 1;
		$tablename = substr($columnname,0,-2);
		$tabid = $tablename.'id';
		$tabfieldname = $tablename.'name';
		$industryid=$this->Basefunctions->industryid;
		$multiple = $this->checkmultipletype('modulefield','multiple',$tabid,$moduleid);
		if( !is_numeric($colvalue) ) {
			$database = $this->db->database;
			$tabchk = $this->tableexitcheck($database,$tablename);
			if($tabchk!=0) {
				if($uitypeid != 21 ) {
					$filedchk = "";
					$filedchk = $this->tablefieldnamecheck($tablename,'moduleid');// moduleid check
					$industrychk = $this->tablefieldnamecheck($tablename,'industryid'); // industry check
					if($filedchk == 'false') {
						if($industrychk == 'false') {
							$result = $this->db->select("$tabfieldname,$tabid")->from($tablename)->where($tablename.'.'.$tabfieldname,$colvalue)->where($tablename.'.status',1)->get();
						} else {
							$result = $this->db->select("$tabfieldname,$tabid")->from($tablename)->where($tablename.'.'.$tabfieldname,$colvalue)->where($tablename.'.status',1)->where("FIND_IN_SET('".$industryid."',industryid) >", 0)->get();
						}
					} else {
						if($industrychk == 'false') {
							$result = $this->db->select("$tabfieldname,$tabid")->from($tablename)->where($tablename.'.'.$tabfieldname,$colvalue)->where($tablename.'.status',1)->where("FIND_IN_SET('".$moduleid."',".$tablename.".moduleid) >", 0)->get();
						} else {
							if($tablename == 'designationtype'){
								$result = $this->db->select("$tabfieldname,$tabid")->from($tablename)->where($tablename.'.'.$tabfieldname,$colvalue)->where($tablename.'.status',1)->where("FIND_IN_SET('".$moduleid."',".$tablename.".moduleid) >", 0)->where("FIND_IN_SET('".$industryid."',industryid) >", 0)->get();
							} else {
								$result = $this->db->select("$tabfieldname,$tabid")->from($tablename)->where($tablename.'.'.$tabfieldname,$colvalue)->where($tablename.'.status',1)->where("FIND_IN_SET('".$moduleid."',".$tablename.".moduleid) >", 0)->where("FIND_IN_SET('".$industryid."',industryid) >", 0)->get();
							}
							
						}
					}
					if($result->num_rows() > 0) {
						foreach($result->result()as $row) {
							$id=$row->$tabid;
							return $id;
						}
					} else {
						$multipleid = [];
						if($multiple == 'Yes') {
							$mulddvalue = explode(';',$colvalue);
							for($j=0;$j<count($mulddvalue);$j++) {
								$multipleid[$j] = $this->checkvalueexist($tablename,$mulddvalue[$j],$tabid,$tabfieldname,$filedchk,$industrychk,$uitypeid,$moduleid);
							}
							$id = implode(',',$multipleid);
							if($id) {
								return $id;
							} else {
								$id = 1;
								return $id;
							}
							if($tablename!= 'module' && $tablename != 'moduleinfo' && $tablename != 'employee') {
								//default value get
								$defdataarr = $this->defaultvalueget();
								$newdata = array_merge($data,$defdataarr);
								//industry check
								if($industrychk != 'false') {
									$newdata['industryid'] = $this->Basefunctions->industryid;
								}
								//data insertion
								$this->db->insert( $tablename, array_filter($newdata) );
								$id = $this->db->insert_id();
								return $id;
							} else {
								return $id;
							}
						} else {
							if($uitypeid == 17 || $uitypeid == 25 || $uitypeid == 18) {
								if($filedchk == 'false') {
									$data = array($tabfieldname=>$colvalue,'userroleid'=>$this->Basefunctions->userroleid);
								} else {
									$data = array($tabfieldname=>$colvalue,'userroleid'=>$this->Basefunctions->userroleid,'moduleid'=>$moduleid);
								}
							} else {
								if($filedchk == 'false') {
									$data = array($tabfieldname=>$colvalue);
								} else {
									$data = array($tabfieldname=>$colvalue,'moduleid'=>$moduleid);
								}
							}
							if($tablename!= 'module' && $tablename != 'moduleinfo' && $tablename != 'employee') {
								//default value get
								$defdataarr = $this->defaultvalueget();
								$newdata = array_merge($data,$defdataarr);
								//industry check
								if($industrychk != 'false') {
									$newdata['industryid'] = $this->Basefunctions->industryid;
								}
								//data insertion
								$this->db->insert( $tablename, array_filter($newdata) );
								$id = $this->db->insert_id();
								return $id;
							} else {
								return $id;
							}
						}
						
					}
				} else {
					$id = 0;
					return $id;
				}
			} else {
				if($uitypeid == 21) {
					$id = 0;
					return $id;
				} else {
					return $id;
				}
			}
		} else {
			if($uitypeid == 21) {
				$id = 0;
				return $id;
			} else {
				return $id;
			}
		}
	}
	public function checkmultipletype($modulefield,$multiple,$tabid,$moduleid) {
		$data=0;
		$this->db->select($multiple,false);
		$this->db->from($modulefield);
		//$this->db->where('status',1);
		$this->db->where('moduletabid',$moduleid);
		$this->db->like('columnname',$tabid);
		$this->db->limit(1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result()as $row) {
				$data=$row->$multiple;
			}
		}
		return $data;
	}
	//check the multiple value exist ot nt
	public function checkvalueexist($tablename,$mulddvalue,$tabid,$tabfieldname,$filedchk,$industrychk,$uitypeid,$moduleid) {
		$industryid = $this->Basefunctions->industryid;
		if($filedchk == 'false') {
			if($industrychk == 'false') {
				$result = $this->db->select("$tabfieldname,$tabid")->from($tablename)->where($tablename.'.'.$tabfieldname,$mulddvalue)->where($tablename.'.status',1)->get();
			} else {
				$result = $this->db->select("$tabfieldname,$tabid")->from($tablename)->where($tablename.'.'.$tabfieldname,$mulddvalue)->where($tablename.'.status',1)->where("FIND_IN_SET('".$industryid."',industryid) >", 0)->get();
			}
		} else {
			if($industrychk == 'false') {
				$result = $this->db->select("$tabfieldname,$tabid")->from($tablename)->where($tablename.'.'.$tabfieldname,$mulddvalue)->where($tablename.'.status',1)->where("FIND_IN_SET('".$moduleid."',".$tablename.".moduleid) >", 0)->get();
			} else {
				$result = $this->db->select("$tabfieldname,$tabid")->from($tablename)->where($tablename.'.'.$tabfieldname,$mulddvalue)->where($tablename.'.status',1)->where("FIND_IN_SET('".$moduleid."',".$tablename.".moduleid) >", 0)->where("FIND_IN_SET('".$industryid."',industryid) >", 0)->get();
			}
		}
		if($result->num_rows() > 0) {
			foreach($result->result()as $row) {
				$id=$row->$tabid;
				return $id;
			}
		} else {
			if($uitypeid == 17 || $uitypeid == 25 || $uitypeid == 18) {
				if($filedchk == 'false') {
					$data = array($tabfieldname=>$mulddvalue,'userroleid'=>$this->Basefunctions->userroleid);
				} else {
					$data = array($tabfieldname=>$mulddvalue,'userroleid'=>$this->Basefunctions->userroleid,'moduleid'=>$moduleid);
				}
			} else {
				if($filedchk == 'false') {
					$data = array($tabfieldname=>$mulddvalue);
				} else {
					$data = array($tabfieldname=>$mulddvalue,'moduleid'=>$moduleid);
				}
			}
			if($tablename!= 'module' && $tablename != 'moduleinfo' && $tablename != 'employee') {
				//default value get
				$defdataarr = $this->defaultvalueget();
				$newdata = array_merge($data,$defdataarr);
				//industry check
				if($industrychk != 'false') {
					$newdata['industryid'] = $this->Basefunctions->industryid;
				}
				//data insertion
				$this->db->insert( $tablename, array_filter($newdata) );
				$id = $this->db->insert_id();
				return $id;
			} else {
				return $id;
			}
		}
	}
	/* Field mapping grid header information */
	public function fieldmapheaderinformationfetchmodel($moduleid) {
		$fnames = array('importcolumnname','internalcolumnname','columnname','tablename','uitypeid','parenttable','moduletabid');
		$flabels = array('Imported Column Name','Internal Column Name','Column Name','Table Name','Ui TypeId','Parent Table','Module TabId');
		$colmodnames = array('importcolumnname','internalcolumnname','columnname','tablename','uitypeid','parenttable','moduletabid');
		$colindexnames = array('activitylog','activitylog','activitylog','activitylog','activitylog','activitylog','activitylog','activitylog');
		$uitypes = array('2','2','2','2','2','2','2','2');
		$viewtypes = array('1','1','0','0','0','0','0','0');
		$dduitypeids = array('17','18','19','20','23','25','26','27','28','29');
		$data = array();
		$i=0;
		$j=0;
		foreach($fnames as $fname) {
			if(in_array($uitypes[$j],$dduitypeids)) {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j].'name';
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]='0';
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]='2';
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			} else {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			}
			$j++;
		}
		return $data;
	}
	/* Field mapping grid header information */
	public function confirmgridheaderinformationfetchmodel($moduleid) {
		$data['fieldname'][0]='unmappedimpcolname';
		$data['fieldlabel'][0]='Un Mapped Column Name';
		$data['colmodelname'][0]='unmappedimpcolname';
		$data['colmodelindex'][0]='unmappedimpcolname';
		$data['colmodelviewtype'][0]='1';
		$data['colmodeltype'][0]='text';
		$data['colmodeluitype'][0]='2';
		$data['colmoduleid'][0]=$moduleid;
		return $data;
	}
	//auto number generate
	public function autonumbergenerate($table,$moduleid) {
		$datasets = array();
		$i=0;
		$data=$this->db->select('modulefieldid,columnname,uitypeid,tablename')->from('modulefield')->where('moduletabid',$moduleid)->where('tablename',$table)->where('uitypeid',14)->get();
		foreach($data->result() as $info) {
			$filedname = $info->columnname;
			$modulefieldid = $info->modulefieldid;
			$number = $this->randomnumbergenerator($moduleid,$table,$filedname,$modulefieldid);
			$datasets[$i]=array('fname'=>$filedname,'value'=>$number);
			$i++;
		}
		return $datasets;
	}
	//random number generation
	public function randomnumbergenerator($moduleid,$table,$filedname,$modulefieldid) {
		$num = "";
		$tableid=$table.'id';
		$data=$this->db->select('suffix,startingnumber,incrementby,random,prefix')->from('serialnumbermaster')->where('moduleid',$moduleid)->where('modulefieldid',$modulefieldid)->get();
		foreach($data->result() as $info) {
			$suffix=$info->suffix;
			$prefix=$info->prefix;
			$startnumber=$info->startingnumber;
			$incrementby=$info->incrementby;
			$random=$info->random;
		}
		if($prefix == "") {
			$prefix=" ";
		}
		if($random != 'Yes') { // serial number
			$datatow = $this->db->query("select $filedname from $table where status!=3 ORDER BY LENGTH($filedname) DESC,$filedname DESC limit 0,1 ");
			if($datatow->num_rows() > 0) {
				foreach($datatow->result() as $next) {
					$datanum = trim($next->$filedname);
					if($datanum<$startnumber) {
						$num=$prefix.$startnumber.$suffix;
						return trim($num);
					} else {
						$incrementby = ($incrementby=='')?$incrementby:'0';
						if($suffix == "") {
							$prenumber = explode($prefix,$datanum);
							if($incrementby == '0') {
								if(count($prenumber) < '2') {
									$prenum = $prenumber[0]+1;
									$num = $prefix.$prenum;
									return trim($num);
								} else {
									$prenum = $prenumber[1]+1;
									$num = $prefix.$prenum;
									return trim($num);
								}
							} else {
								if(count($prenumber) < '2') {
									$prenum = $prenumber[0]+$incrementby;
									$num = $prefix.$prenum;
									return trim($num);
								} else {
									$prenum = $prenumber[1]+$incrementby;
									$num = $prefix.$prenum;
									return trim($num);
								}
							}
						} else {
							$data = trim($next->$filedname);
							$number = explode($suffix,$data);
							$prenumber = explode($prefix,$number[0]);
							if($incrementby == '0') {
								if(count($prenumber) < '2') {
									$prenum = $prenumber[0]+1;
									$num = $prefix.$prenum.$suffix;
									return trim($num);
								} else {
									$prenum = $prenumber[1]+1;
									$num = $prefix.$prenum.$suffix;
									return trim($num);
								}
							} else {
								if(count($prenumber) < '2') {
									$prenum = $prenumber[0]+$incrementby;
									$num = $prefix.$prenum.$suffix;
									return trim($num);
								} else {
									$prenum = $prenumber[1]+$incrementby;
									$num = $prefix.$prenum.$suffix;
									return trim($num);
								}
							}
						}
					}
				}
			} else {
				$num=$prefix.$startnumber.$suffix;
				return trim($num);
			}			
		} else {			
			//randomcall
			$num=$this->fetchrandomnumber($filedname,$table,$tableid,$suffix,$prefix);
			return trim($num);
		}
	}
	//generate the random number//
	public function fetchrandomnumber($fieldnumber,$table,$tableid,$suffix,$prefix) {
		$uid=time()+mt_rand(1,99999999);
		$alphanumber=$prefix.$uid.$suffix;
		$data=$this->db->select($fieldnumber)
					->from($table)
					->where($tableid,$alphanumber)
					->get()->num_rows();
        if($data > 0) {
			$this->fetchrandomnumber($fieldnumber,$table,$tableid,$suffix,$prefix);			
		} else {
			return $alphanumber;
		}
	}
}
?> 