<?php
Class User extends CI_Model {
	//login
	public function login($username, $password,$domainname,$databasename,$status,$mascompanyid,$logintype,$signupstatus,$planid,$industryid) {
		$user_db = $this->inlineuserdatabaseactive($databasename);
		$user_db->select('employee.employeeid,employee.username,employee.password,employee.emailid');
		$user_db->from('employee');
		$user_db->where('emailid = '."'".$username."'");
	    $user_db->limit(1);
		$query = $user_db->get();
		if($query->num_rows() < 1) {
			$loginarray=array('status'=>'Account ID is invalid');
			return json_encode($loginarray);
		} else {
			foreach($query->result() as $row) {
				$loguserid = $row->employeeid;
				$array1 = array(
					'id' => $row->employeeid,
					'username' => $row->username,
					'password' => $row->password
				);
			}
			// check to see whether password matches
			$passwdchk = '';
			if($logintype=='general') {
				$passwdchk = password_verify($password, $array1['password']);
			} else {
				$passwdchk = '1';
			}
			if($passwdchk!='') {
				foreach($query->result() as $row) {
					$userid=$row->employeeid;
					$sess_array = array(
						'id' => $row->employeeid,
						'username' => $row->username,
						'empid' => $row->employeeid
					);
				}
				$this->session->set_userdata('logged_in', $sess_array);
				//update account expire status
				if($status=='Expired') {
					$this->db->where('companyid',$mascompanyid);
					$this->db->update('generalsetting',array('accountexpire'=>'Yes'));
				}
				//login history maintenance
				$loghistoryid = $this->loginhistorycreate($userid,'Success',$user_db,$industryid);
				$loghistory=array('loghistoryid'=>$loghistoryid);
				$this->session->set_userdata('loghistory', $loghistory);
				//email information fetch for roundcube login
				$branch=$user_db->select('employee.branchid,employee.userroleid,employee.emailid')->from('employee')->where('employee.employeeid',$userid)->get()->result();
				foreach($branch as $info) {
					$branchid=$info->branchid;
					$UserRoleId=$info->userroleid;
					$email=$info->emailid;
				}				
				//mail setting retrieves
				$msemail='';
				$xmz='';
				$msetting=$user_db->select('emailusername,emailpassword')->from('mailsetting')->where('createuserid',$userid)->where('status',1)->limit(1)->get();
				foreach($msetting->result() as $info) {
					$msemail=$info->emailusername;
					$xmz=$info->emailpassword;
				}
				//branch information
				$brancharray=array('BranchId'=>$branchid);
				$this->session->set_userdata('BranchIds', $brancharray);
				$userrole=array('UserRoleId'=>$UserRoleId);
				$this->session->set_userdata('UserRoleId', $userrole);
				//Plan Info
				$planinfo=array('plandataid'=>$planid);
				$this->session->set_userdata('planids', $planinfo);//$industryid
				//industry id
				$industryidinfo=array('industryid'=>$industryid);
				$this->session->set_userdata('industryids', $industryidinfo);
				//database name
				$this->session->set_userdata('UserDataname', $databasename);
				$newdate =  date($this->Basefunctions->datef);
				//set account status
				$accstatus = (($status=='Expired')?'inactive':'active');
				$this->session->set_userdata('UserAccount',$accstatus);
				//final status
				$loginarray=array('em'=>$msemail,'xm'=>$xmz,'status'=>$status,'email'=>$email,'domainname'=>$domainname,'signupstatus'=>$signupstatus,'industryid'=>$industryid);
				return json_encode($loginarray);
			} else {
				//login history maintenance
				$this->loginhistorycreate($loguserid,'Failed',$user_db,$industryid);
				$loginarray=array('status'=>'Password is wrong','domainname'=>'','email'=>$username);
				return json_encode($loginarray);
			}
		}
	}
	//login user info fetch
	public function loginuserinfo($username) {
		$this->inlinemasterdbenable();
		$tzone = $this->Basefunctions->employeetimezoneset($this->Basefunctions->userid);
		date_default_timezone_set($tzone['name']);
		$current_date = date($this->Basefunctions->datef);
		$this->db->select('userplaninfo.userplaninfoid,userplaninfo.registeremailid,userplaninfo.companyid,userplaninfo.expiredate,userplaninfo.domainnameinfo,userplaninfo.databasenameinfo,userplaninfo.activationstatus,userplaninfo.signupstatus,userplaninfo.industry,userplaninfo.industryid,userplaninfo.installmode,userplaninfo.vardaancompanyid');
		$this->db->from('userplaninfo');
		$this->db->where('userplaninfo.registeremailid',$username);
		$this->db->where('userplaninfo.status',1);
	    $this->db->limit(1);
		$query = $this->db->get();
		if($query->num_rows() >= 1) {
			foreach($query->result() as $row) {
				$expdate = $row->expiredate;
				$actstatus = $row->activationstatus;
				$mascompid = $row->companyid;
				if($current_date <= $expdate) {
					if($actstatus=='Yes') {
						$loginarray = array('status'=>'success','domainname'=>$row->domainnameinfo,'dbname'=>$row->databasenameinfo,'mascompanyid'=>$mascompid,'signupstatus'=>$row->signupstatus,'industry'=>$row->industry,'industryid'=>$row->industryid);
					} else {
						$loginarray=array('status'=>'Not Verified');
					}
				} else {
					$this->db->where('companyid',$mascompid);
					$this->db->update('generalsetting',array('accountexpire'=>'Yes'));
					$loginarray=array('status'=>'Expired','domainname'=>$row->domainnameinfo,'dbname'=>$row->databasenameinfo,'mascompanyid'=>$mascompid,'signupstatus'=>$row->signupstatus,'industry'=>$row->industry,'industryid'=>$row->industryid);
				}
			}
		} else {
			$statusinfo = $this->Basefunctions->generalinformaion('userplaninfo','status','registeremailid',$username);
			if($statusinfo=='5') {
				$loginarray=array('status'=>'Account Is Cancelled');
			} else {
				$loginarray=array('status'=>'Account ID is invalid');
			}
		}
		return json_encode($loginarray);
	}
	//sign up user mail id verification
	public function emailidverification($mail) {
		$this-> db->select('userplaninfo.userplaninfoid,userplaninfo.registeremailid');
		$this->db->from('userplaninfo');
		$this->db->where('userplaninfo.registeremailid',$mail);
	    $this->db->limit(1);
		$query = $this->db->get();
		if($query->num_rows() >= 1) {
			return "Your account already activated!";
		} else {
			return "true";
		}
	}
	//signup email verification
	public function signupemailverification($mail,$tzones) {
		$this->inlinemasterdbenable();
		date_default_timezone_set($tzones);
		$this->db->select('userplaninfo.userplaninfoid,userplaninfo.registeremailid,userplaninfo.activationstatus,userplaninfo.activationdate');
		$this->db->from('userplaninfo');
		$this->db->where('userplaninfo.registeremailid',$mail);
	    $this->db->limit(1);
		$query = $this->db->get();
		if($query->num_rows() >= 1) {
			foreach($query->result() as $datas) {
				$status = $datas->activationstatus;
				if($status=='Yes') {
					return 'activated';
				} else {
					$actdate = $datas->activationdate;
					$userplaninfoid = $datas->userplaninfoid;
					$expdate=date('Y-m-d H:i:s', strtotime($actdate.'+ 7days'));
					if($actdate <= $expdate) {
						$uploginfo = array('activationstatus'=>'Yes');
						$this->db->where('userplaninfoid',$userplaninfoid);
						$this->db->update('userplaninfo',$uploginfo);
						return 'success';
					} else {
						return 'expired';
					}
				}
			}
		} else {
			return 'fail';
		}
	}
	//new signup procedure 
	public function newcompanycreate($name,$companyname,$email,$password,$planid,$zone,$signuptype,$signupimgpath,$industryid,$mobileno) {
		$secure = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "");
		$domain = $signuptype=='general' ? preg_replace('/[^A-Za-z0-9]/','',strtolower($companyname)) : preg_replace('/[^A-Za-z0-9]/','',strtolower($name));
		$dname=substr($domain,0,6);
		$dname=str_pad($dname,6,"sn");
		$datef="Y-m-d H:i:s";
		$demodata = 'Yes';
		if($_SERVER['HTTP_HOST']=='localhost') {
			$domainname = "http".$secure."://".$_SERVER['HTTP_HOST']."/salesneuronsite/crm/";
		} else {
			$hname = $_SERVER['HTTP_HOST'];
			$hnames = explode('.',$hname);
			$cnt = count($hnames);
			$host = $hnames[($cnt-2)].'.'.$hnames[($cnt-1)];
			$domainname = "http".$secure."://".$host."/crm/";
		}
		//declare result array set
		$result=array('status'=>'false','username'=>'','password'=>'','domainname'=>'','database'=>'');
		//time zone
		$ctzoneid = 308;
		$tzone = 'UTC';
		$ctimedatas = $this->db->select('timezone.timezoneid,timezone.displaytimezone',false)->from('timezone')->where('timezone.displaytimezone',$zone)->get();
		foreach($ctimedatas->result() as $ctimedata) {
			$ctzoneid = $ctimedata->timezoneid;
			$tzone =  $ctimedata->displaytimezone;
		}
		date_default_timezone_set($tzone);
		//fetch user image information
		if($signupimgpath!='' && $signupimgpath!='undefined') {
			if($signuptype=="facebook") {
				$path = explode('?',$signupimgpath);
				$exten = explode('.',$path[0]);
				$extension = 'image/'.$exten[count($exten)-1];
				$fnames = explode('/',$path[0]);
				$fname = $fnames[count($fnames)-1];
			} else {
				$exten = explode('.',$signupimgpath);
				$extension = 'image/'.$exten[count($exten)-1];
				$fnames = explode('/',$signupimgpath);
				$fname = $fnames[count($fnames)-1];
			}
			$fromid = '4';
		} else {
			$extension = '';
			$fname = '';
			$fromid = '1';
		}
		$signtypestatus = ($signuptype=="general") ?'Yes' :'No';
		//check email is available
		$emchk = $this->uniqueemail($email);
		if($emchk == 'TRUE') {
			//user plan activation log in master db
			$databasename = strtolower($dname);
			$usrplanifoid = $this->userplanactivationinfolog($planid,1,$email,$domainname,$databasename,$signuptype,$signtypestatus,$industryid,$mobileno);
			{//create data in to master data base
				//create new company
				$companydata=array('companyname'=>$companyname,'contactperson'=>$name,'emailid'=>$email,'createdate'=>date($datef),'lastupdatedate'=>date($datef),'currencyid'=>2,'dateformatid'=>3,'timezoneid'=>$ctzoneid,'createuserid'=>1,'lastupdateuserid'=>1,'status'=>1);
				$this->db->insert('company',$companydata);
				$companyid=$this->db->insert_id();
				$mastercompanyid=$companyid;
				//user database name set
				$databasename = $dname.$mastercompanyid;
				//create new branch
				$branchname = $companyname==''? 'H.O Office' : $companyname;
				$branchdata=array('branchname'=>$branchname,'companyid'=>$companyid,'branchtypeid'=>2,'emailid'=>$email,'createdate'=>date($datef),'lastupdatedate'=>date($datef),'createuserid'=>1,'lastupdateuserid'=>1,'status'=>1);
				$this->db->insert('branch',$branchdata);
				$branchid=$this->db->insert_id();
				//create new employee
				$hash = password_hash($password, PASSWORD_DEFAULT);
				$employeedata=array('employeename'=>$name,'branchid'=>$branchid,'userroleid'=>2,'emailid'=>$email,'password'=>$hash,'timezoneid'=>$ctzoneid,'createdate'=>date($datef),'lastupdatedate'=>date($datef),'createuserid'=>1,'lastupdateuserid'=>1,'status'=>1);
				$this->db->insert('employee',$employeedata);
				$empid = $this->db->insert_id();
				//empoyee default address creattion
				/* $empaddrtypeid = array(2,3);
				$empmethodid = array(4,5);
				for($i=0; $i<count($empaddrtypeid);$i++) {
					$employeeaddress=array('employeeid'=>$empid ,'addresstypeid'=>$empaddrtypeid[$i],'addressmethod'=>$empmethodid[$i],'createdate'=>date($datef),'lastupdatedate'=>date($datef),'createuserid'=>$empid,'lastupdateuserid'=>$empid,'status'=>1);
					$this->db->insert('employeeaddress',$employeeaddress);
				} */
				//update bill information
				$billinfo = array('mastercompanyid'=>$mastercompanyid,'planid'=>$planid,'planduration'=>'7','billingusers'=>3,'billingcost'=>'0.00','billingaddress'=>'','billingcity'=>'','billingstate'=>'','billingcountry'=>'','billingpostalcode'=>'','shippingaddress'=>'','shippingcity'=>'','shippingstate'=>'','shippingcountry'=>'','shippingpostalcode'=>'','createdate'=>date($datef),'lastupdatedate'=>date($datef),'createuserid'=>$empid,'lastupdateuserid'=>$empid,'status'=>1);
				$this->db->insert('billinginformation',$billinfo);
				//update default add-ons credit
				$addonsdata=array(array('mastercompanyid'=>$mastercompanyid,'addonstypeid'=>1,'addonavailablecredit'=>'1','status'=>1),array('mastercompanyid'=>$mastercompanyid,'addonstypeid'=>2,'addonavailablecredit'=>'100','status'=>1),array('mastercompanyid'=>$mastercompanyid,'addonstypeid'=>3,'addonavailablecredit'=>'100','status'=>1));
				$this->db->insert_batch('salesneuronaddonscredit',$addonsdata);
				//update company,branch log userids info
				$this->db->update('company',array('createuserid'=>$empid,'lastupdateuserid'=>$empid),array('companyid'=>$companyid));
				$this->db->update('branch',array('createuserid'=>$empid,'lastupdateuserid'=>$empid),array('branchid'=>$branchid));
				$this->db->update('employee',array('createuserid'=>$empid,'lastupdateuserid'=>$empid),array('employeeid'=>$empid));
				//Mandrill account creation
				/* $this->load->library('Mandrill','X9WnMXpxhC8YEOUPuQ3oZw');
				$mandrill = new Mandrill('X9WnMXpxhC8YEOUPuQ3oZw');
				if($companyname) {
					$cname = preg_replace('/[^A-Za-z0-9]/','',strtolower($companyname));
				} else {
					$cname = preg_replace('/[^A-Za-z0-9]/','',strtolower($name));
				}
				$result = $mandrill->subaccounts->add($cname.$mastercompanyid,$companyname,'',100);
				$mandaccoutid = $result['id']; */
				//insert mandrill account info
				$mandaccoutid = '';
				$this->db->insert('generalsetting',array('companyid'=>$mastercompanyid,'mandrillaccountnumber'=>$mandaccoutid,'status'=>1,'demodata'=>'Yes','planupgrade'=>'No'));
			}
			/*********plan activation*********/
			{
				//fetch moduleids based on plan
				$planmoduleids = $this->planinformationdatafetch($planid,$industryid);
				//create database
				if($demodata == 'Yes') {
					if($industryid == '1') {//For Normal CRM
						$datafile = "database/datadb.sql";
					} else if($industryid == '2') {//For Hospital Relationship Mgnt
						$datafile = "database/datadb.sql";
					} else if($industryid == '3') {//For Vardaan - Jewell
						$datafile = "database/datadb.sql";
					} else {//Other Normal CRM
						$datafile = "database/datadb.sql";
					}
				} else {
					if($industryid == '1') {//For Normal CRM
						$datafile = "database/cleandb.sql";
					} else if($industryid == '2') {//For Hospital Relationship Mgnt
						$datafile = "database/cleandb.sql";
					} else if($industryid == '3') {//For Vardaan - Jewell
						$datafile = "database/cleandb.sql";
					} else {//Other Normal CRM
						$datafile = "database/cleandb.sql";
					}
				}
				$this->load->dbforge();
				$this->load->dbutil();
				$dbcreate = 'No';
				$dbcheck = $this->dbutil->database_exists($databasename);
				if($dbcheck == '') {
					$this->dbforge->create_database($databasename);
					$dbcreate = 'Yes';
				} else {
					$dbcreate = 'Yes';
				}
				if ($dbcreate == 'Yes') {
					//activate user data base
					$import_db = $this->inlineuserdatabaseactive($databasename);
					if($dbcheck == '') {
						//create content to user data base
						$importres = $this->doImport($databasename,$datafile,$compress=false,$import_db);
					}
				
					if($importres =='true') {
					//plan activation
					{//update user database
						/* $host = $this->db->hostname;
						$user = $this->db->username;
						$passwd = $this->db->password;
					$db['otherdb']['hostname'] = $host;
					$db['otherdb']['username'] = $user;
					$db['otherdb']['password'] = $passwd;
					$db['otherdb']['database'] = $databasename;
					$db['otherdb']['dbdriver'] = "mysql";
					$db['otherdb']['dbprefix'] = "";
					$db['otherdb']['pconnect'] = TRUE;
					$db['otherdb']['db_debug'] = FALSE;
					$db['otherdb']['cache_on'] = FALSE;
					$db['otherdb']['cachedir'] = "";
					$db['otherdb']['char_set'] = "utf8";
					$db['otherdb']['dbcollat'] = "utf8_general_ci";
					$db['otherdb']['swap_pre'] = "";
					$db['otherdb']['autoinit'] = TRUE;
					$db['otherdb']['stricton'] = FALSE; */
					$host = $this->db->hostname;
					$user = $this->db->username;
					$passwd = $this->db->password;
					$dbconfig['hostname'] = $host;
					$dbconfig['username'] = $user;
					$dbconfig['password'] = $passwd;
					$dbconfig['database'] = $databasename;
					$dbconfig['dbdriver'] = 'mysqli';
					$dbconfig['dbprefix'] = '';
					$dbconfig['pconnect'] = TRUE;
					$dbconfig['db_debug'] = TRUE;
					$dbconfig['cache_on'] = FALSE;
					$dbconfig['cachedir'] = '';
					$dbconfig['char_set'] = 'utf8';
					$dbconfig['dbcollat'] = 'utf8_general_ci';
					$dbconfig['swap_pre'] = '';
					$dbconfig['autoinit'] = TRUE;
					$dbconfig['stricton'] = FALSE;
					$otherdb =  $this->load->database($dbconfig, TRUE);
						
					//$otherdb = $this->load->database($db['otherdb'], TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

						$companydata=array('companyname'=>$companyname,'contactperson'=>$name,'emailid'=>$email,'timezoneid'=>$ctzoneid,'industryid'=>$industryid,'createdate'=>date($datef),'lastupdatedate'=>date($datef),'currencyid'=>2,'dateformatid'=>3,'createuserid'=>1,'lastupdateuserid'=>1,'status'=>1,'mastercompanyid'=>$mastercompanyid);
						$otherdb->where('companyid','1');
						$otherdb->insert('company',$companydata);
						$companyid=$otherdb->insert_id();
						//create new branch
						$branchname = $companyname==''? 'H.O Office' : $companyname;
						$branchdata=array('branchname'=>$branchname,'companyid'=>$companyid,'branchtypeid'=>2,'emailid'=>$email,'industryid'=>$industryid,'createdate'=>date($datef),'lastupdatedate'=>date($datef),'createuserid'=>1,'lastupdateuserid'=>1,'status'=>1);
						$otherdb->insert('branch',$branchdata);
						$branchid=$otherdb->insert_id();
						
						//create new employee
						$hash = password_hash($password, PASSWORD_DEFAULT);
						$employeedata=array('employeename'=>$name,'branchid'=>$branchid,'userroleid'=>2,'emailid'=>$email,'timezoneid'=>$ctzoneid,'password'=>$hash,'employeeimage_fromid'=>$fromid,'employeeimage'=>$fname,'employeeimage_type'=>$extension,'employeeimage_path'=>$signupimgpath,'salestransactiontypeid'=>11,'industryid'=>$industryid,'createdate'=>date($datef),'lastupdatedate'=>date($datef),'createuserid'=>1,'lastupdateuserid'=>1,'status'=>1);
						$otherdb->insert('employee',$employeedata);
						$empid = $otherdb->insert_id();
						//empoyee default address creattion
						$empaddrtypeid = array(2,3);
						$empmethodid = array(4,5);
						for($i=0; $i<count($empaddrtypeid);$i++) {
							$employeeaddress=array('employeeid'=>$empid ,'addresstypeid'=>$empaddrtypeid[$i],'addressmethod'=>$empmethodid[$i],'createdate'=>date($datef),'lastupdatedate'=>date($datef),'createuserid'=>$empid,'lastupdateuserid'=>$empid,'status'=>1);
							$otherdb->insert('employeeaddress',$employeeaddress);
						}
						//update bill information
						$billinfo = array('mastercompanyid'=>$mastercompanyid,'planid'=>$planid,'planduration'=>'7','billingusers'=>3,'billingcost'=>'0.00','billingaddress'=>'','billingcity'=>'','billingstate'=>'','billingcountry'=>'','billingpostalcode'=>'','shippingaddress'=>'','shippingcity'=>'','shippingstate'=>'','shippingcountry'=>'','shippingpostalcode'=>'','createdate'=>date($datef),'lastupdatedate'=>date($datef),'createuserid'=>$empid,'lastupdateuserid'=>$empid,'status'=>1);
						$otherdb->insert('billinginformation',$billinfo);
						//update default add-ons credit
						$addonsdatas=array(array('mastercompanyid'=>$mastercompanyid,'addonstypeid'=>1,'addonavailablecredit'=>'1','status'=>1),array('mastercompanyid'=>$mastercompanyid,'addonstypeid'=>2,'addonavailablecredit'=>'100','status'=>1),array('mastercompanyid'=>$mastercompanyid,'addonstypeid'=>3,'addonavailablecredit'=>'100','status'=>1));
						$otherdb->insert_batch('salesneuronaddonscredit',$addonsdatas);
						//update company,branch log userids info
						$otherdb->update('company',array('createuserid'=>$empid,'lastupdateuserid'=>$empid),array('companyid'=>$companyid));
						$otherdb->update('branch',array('createuserid'=>$empid,'lastupdateuserid'=>$empid),array('branchid'=>$branchid));
						$otherdb->update('employee',array('createuserid'=>$empid,'lastupdateuserid'=>$empid),array('employeeid'=>$empid));
						//insert mandrill account info
						$import_db->insert('generalsetting',array('companyid'=>$companyid,'mandrillaccountnumber'=>$mandaccoutid,'status'=>1,'demodata'=>'Yes','planupgrade'=>'No'));
						//activate user plan
						$uparesult = $this->userplanactivation($planmoduleids,$otherdb);
						
					}
					//user plan activation log in master db
					$this->updateuserplanactivationinfolog($usrplanifoid,$mastercompanyid,$domainname,$databasename);
					
					//sign up user information
					$signuplog = $this->signupupuserlogcreate($usrplanifoid,$name,$email,'Success');
					
					$result=array('status'=>'true','username'=>$email,'password'=>$password,'domainname'=>$domainname,'database'=>$databasename,'mastercompanyid'=>$mastercompanyid);
					
				  }
				}
			}
			return $result;
		} else {
			return "Emailfail";
		}
	}
	//login history maintenance
	public function loginhistorycreate($userid,$status,$user_db,$industryid) {
		$browsername = $_SERVER['HTTP_USER_AGENT'];
		$localip = $_SERVER['REMOTE_ADDR'];
		$osversion = PHP_OS;
		$datas = preg_split("/[()]+/", $browsername);
		if(!isset($datas[1]))
        {
            $datas[1] = "Non-Browser Plugin";
        }
		$osname = $datas[1];
		$tzone=$this->Basefunctions->employeetimezoneset($userid);
		date_default_timezone_set($tzone['name']);
		$date = date($this->Basefunctions->datef);
		$loginfo = array(
			'employeeid'=>$userid,
			'ipaddress'=>$localip,
			'signintime'=>$date,
			'signouttime'=>'0000-00-00 00:00:00',
			'loginstatus'=>$status,
			'browsername'=>$browsername,
			'platformname'=>$osname,
			'industryid'=>$industryid,
			'status'=>1
		);
		$user_db->insert('loginhistory',$loginfo);
		$primaryid = $user_db->insert_id();
		return $primaryid;
	}
	//check user name.
	public function checkusername($username) {
		$this->inlinemasterdbenable();
		$tzone = $this->Basefunctions->employeetimezoneset($this->Basefunctions->userid);
		date_default_timezone_set($tzone['name']);
		$current_date = date($this->Basefunctions->datef);
		$this->db->select('userplaninfo.userplaninfoid,userplaninfo.registeremailid,userplaninfo.expiredate');
		$this->db->from('userplaninfo');
		$this->db->where('userplaninfo.registeremailid',$username);
	    $this->db->limit(1);
		$query = $this->db->get();
		if($query->num_rows() >= 1) {
			foreach($query->result() as $row) {
				$expdate = $row->expiredate;
				if($current_date <= $expdate) {
					$loginarray = array('status'=>'success');
				} else {
					$loginarray = array('status'=>'success');
				}
			}
		} else {
			$loginarray=array('status'=>'Invalid email id');
		}
		return json_encode($loginarray);
	}
	public function checksecurityinfo($userid,$securityanswer) {
		$this ->db->select('employeeid,securityanswer');
		$this ->db->from('employee');
		$this ->db->where('employeeid = '."'".$userid."'");
		$this ->db->where('securityanswer = '."'".$securityanswer."'");
	    $this ->db->limit(1);
		$query = $this ->db->get();
		if($query -> num_rows() == 1) {
			return true;
		} else {
			return false;
		}
	}
	//update new password
	public function newpassword($userid,$password) {
		$result = array('status'=>'false');
		$this->db->select('userplaninfo.userplaninfoid,userplaninfo.registeremailid,userplaninfo.expiredate,userplaninfo.domainnameinfo,userplaninfo.databasenameinfo');
		$this->db->from('userplaninfo');
		$this->db->where('userplaninfo.registeremailid',$userid);
	    $this->db->limit(1);
		$query = $this->db->get();
		if($query->num_rows() >= 1) {
			foreach($query->result() as $row) {
				$dbname = $row->databasenameinfo;
				$result['status'] = $this->updateuserpassword($userid,$dbname,$password);
				$result['database'] = $dbname;
				return $result;
			}
		} else {
			return $result;
		}
	}
	//password update
	public function updateuserpassword($userid,$dbname,$password) {
		$user_db = $this->inlineuserdatabaseactive($dbname);
		$hash = password_hash($password, PASSWORD_DEFAULT);
		$updateinfo=array(
			'password'=>$hash
		);
		$user_db->where('emailid',$userid);
		$user_db->update('employee',$updateinfo);
		return 'true';
	}
	//unique email check
	public function uniqueemail($value) {
		$data = 'FAIL';
		$this->db->select('userplaninfoid,registeremailid');
		$this->db->from('userplaninfo');
		$this->db->where('registeremailid',$value);
		$row=$this->db->get();
		if($row->num_rows() > 0) {
			$data = 'FAIL';
		} else if($row->num_rows() == 0) {
			$data = 'TRUE';
		}
		return $data;
	}
	//account information udpate model
	public function accountinfoupdatemodel() {
		//fetch master company id
		$companyname=isset($_POST['companyname'])?$_POST['companyname']:'';
		$password=isset($_POST['pwd'])?$_POST['pwd']:'';
		$emailid = isset($_POST['email'])?$_POST['email']:'';
		$mascompanyid=1;
		$branchid = $this->Basefunctions->branchid;
		$mcdata = $this->db->select('company.companyid,company.mastercompanyid')->from('company')->join('branch','branch.companyid=company.companyid')->where('branch.branchid',$branchid)->get();
		foreach($mcdata->result() as $datas) {
			$mascompanyid = $datas->mastercompanyid;
			$companyid = $datas->companyid;
		}
		if($mascompanyid!=1 && $companyname!='') {
			$masdbname = $this->db->masterdb;
			$userdb = $this->db->database;
			$masdb = $this->inlineuserdatabaseactive($masdbname);
			//update company name in master db
			$masdb->where('companyid',$mascompanyid);
			$masdb->update('company',array('companyname'=>$companyname));
			//update user plan info
			$masdb->where('companyid',$mascompanyid);
			$masdb->update('userplaninfo',array('signupstatus'=>'Yes'));
			{
				$clientdb = $this->inlineuserdatabaseactive($userdb);
				//update company name in client db
				$clientdb->where('companyid',$companyid);
				$clientdb->update('company',array('companyname'=>$companyname));
				//update employee password
				$hash = password_hash($password, PASSWORD_DEFAULT);
				$clientdb->where('emailid',$emailid);
				$clientdb->update('employee',array('password'=>$hash));
				redirect('/Home','refresh');
			}
		} else {
			redirect('/Login/logout','refresh');
		}
	}
	//unique domain name check
	public function uniquedomaincheckmodel($domain) {
		$dname = preg_replace('/[^A-Za-z0-9]/','',$domain);
		$domainname = strtolower($dname);
		$this->db->select('userplaninfoid,databasenameinfo');
		$this->db->from('userplaninfo');
		$this->db->where('databasenameinfo',$domainname);
		$row=$this->db->get();
		if($row->num_rows() > 0) {
			return 'fail';
		} else if($row->num_rows() == 0) {
			return 'true';
		}
	}
	//plan information data fetch
	public function planinformationdatafetch($planid,$industryid) {
		$this->inlinemasterdbenable(); //main data base select
		$dataset = "1";
		$data=$this->db->select('planinfo.moduleid')->from('planinfo')->join('plan','plan.planid=planinfo.planid')->where('planinfo.planid',$planid)->where('planinfo.industryid',$industryid)->get();
		foreach($data->result() as $info) {
			$dataset = $info->moduleid;
		}
		return $dataset;
	}
	//user plan activation in company creation
	public function userplanactivation($planmoduleids,$import_db) {
		$userrole = 2;
		//fetch user created external links
		$j=0;
		$userextmod = array();
		$data=$import_db->query('select module.moduleid from module where moduletypeid="2" AND status IN (1)');
		foreach($data->result() as $info) {
			$userextmod[$j] = $info->moduleid;
			$j++;
		}
		//module id based on user role
		$i=0;
		$usermod = array();
		$data=$import_db->query('select moduleinfo.moduleinfoid,moduleinfo.moduleid from moduleinfo where moduleinfo.userroleid="'.$userrole.'" AND status=1');
		foreach($data->result() as $info) {
			$usermod[$i] = $info->moduleid;
			$i++;
		}
		//fetch sub module ids
		$dashid = array();
		$dashmodules = $import_db->query('select module.moduleid,module.moduledashboardid from module where moduleid IN ('.$planmoduleids.') AND moduledashboardid!=1 AND moduledashboardid!="" ',false);
		foreach($dashmodules->result() as $dashrow) {
			$dashid[] = $dashrow->moduledashboardid;
		}
		//get submodule ids based userrole
		$dashids = implode(',',$dashid);
		$dashids = ( ($dashids=="") ? '1' : $dashids );
		$moddashids = array();
		$dashmodules = $import_db->query('select moduleinfo.moduleid from moduleinfo where userroleid="'.$userrole.'" AND moduleid IN ('.$dashids.') AND status=1',false);
		foreach($dashmodules->result() as $dashrow) {
			$moddashids[] = $dashrow->moduleid;
		}
		//activate modules
		$planmodids = explode(',',$planmoduleids);
		$modids = array_intersect($planmodids,$usermod);
		$usrmodids = array_merge($modids,$userextmod);
		$usermodids = array_merge($usrmodids,$moddashids);
		$usermodules = implode(',',$usermodids);
		if($usermodules == "") {
			$usermodules = 1;
		}
		$moduletables = array('module','moduleinfo');
		$moduletabid = array('moduleid','moduleid');
		$i = 0;
		//diable modules
		foreach($moduletables as $table) {
			$fnamechk = $this->tablefieldnamecheck($table,'userroleid',$import_db);
			if($fnamechk == 'true') {
				$import_db->query('UPDATE '.$table.' SET status=0 WHERE '.$moduletabid[$i].' != 1 AND status != 2  AND userroleid='.$userrole.' ');
			} else {
				$import_db->query('UPDATE '.$table.' SET status=0 WHERE moduletypeid!="2" AND '.$moduletabid[$i].' != 1 AND status != 2 ');
			}
			$i++;
		}
		//activate modules based on user plan activation
		$i = 0;
		foreach($moduletables as $table) {
			$fnamechk = $this->tablefieldnamecheck($table,'userroleid',$import_db);
			if($fnamechk == 'true') {
				$import_db->query('UPDATE '.$table.' SET status=1 WHERE '.$moduletabid[$i].' IN ('.$usermodules.') AND userroleid='.$userrole.' AND status=0');

				$import_db->query('UPDATE '.$table.' SET status=0 WHERE '.$moduletabid[$i].' NOT IN ('.$usermodules.') AND userroleid='.$userrole.' AND status=0');
			} else {
				$import_db->query('UPDATE '.$table.' SET status=1 WHERE '.$moduletabid[$i].' IN ('.$planmoduleids.') AND status=0');

				$import_db->query('UPDATE '.$table.' SET status=0 WHERE moduletypeid!="2" AND '.$moduletabid[$i].' NOT IN ('.$planmoduleids.') AND status=0');
			}
			$i++;
		}
		return 'true';
	}
	//user plan activation during the login
	public function userplanactivationinlogin($planmoduleids,$dbname,$loginusername,$industryid) {
		$import_db = $this->inlineuserdatabaseactive($dbname);
		$userrole = 2;
		$data=$import_db->query('select employee.employeeid,employee.userroleid from employee where employee.emailid="'.$loginusername.'"');
		foreach($data->result() as $info) {
			$userrole = $info->userroleid;
		}
		//fetch user created external links
		$j=0;
		$userextmod = array();
		$data=$import_db->query('select module.moduleid from module where moduletypeid="2" AND status IN (1)');
		foreach($data->result() as $info) {
			$userextmod[$j] = $info->moduleid;
			$j++;
		}
		//module id based on user role
		$i=0;
		$usermod = array();
		$data=$import_db->query('select moduleinfo.moduleinfoid,moduleinfo.moduleid from moduleinfo where moduleinfo.userroleid="'.$userrole.'" AND status=1');
		foreach($data->result() as $info) {
			$usermod[$i] = $info->moduleid;
			$i++;
		}
		//fetch sub module ids
		$dashid = array();
		$dashmodules = $import_db->query('select module.moduleid,module.moduledashboardid from module where moduleid IN ('.$planmoduleids.') AND moduledashboardid!=1 AND moduledashboardid!="" ',false);
		foreach($dashmodules->result() as $dashrow) {
			$dashid[] = $dashrow->moduledashboardid;
		}
		//get submodule ids based userrole
		$dashids = implode(',',$dashid);
		$dashids = ( ($dashids=="") ? '1' : $dashids );
		$moddashids = array();
		$dashmodules = $import_db->query('select moduleinfo.moduleid from moduleinfo where userroleid="'.$userrole.'" AND moduleid IN ('.$dashids.') AND status=1',false);
		foreach($dashmodules->result() as $dashrow) {
			$moddashids[] = $dashrow->moduleid;
		}
		
		//activate modules
		$planmodids = explode(',',$planmoduleids);
		$modids = array_intersect($planmodids,$usermod);
		$usrmodids = array_merge($modids,$userextmod);
		$usermodids = array_merge($usrmodids,$moddashids);
		$usermodules = implode(',',$usermodids);
		if($usermodules == "") {
			$usermodules = 1;
		}
		$moduletables = array('module','moduleinfo');
		$moduletabid = array('moduleid','moduleid');
		$i = 0;
		//diable modules
		foreach($moduletables as $table) {
			$fnamechk = $this->tablefieldnamecheck($table,'userroleid',$import_db);
			if($fnamechk == 'true') {
				$import_db->query('UPDATE '.$table.' SET status=0 WHERE '.$moduletabid[$i].' != 1 AND status != 2  AND userroleid='.$userrole.' ');
			} else {
				$import_db->query('UPDATE '.$table.' SET status=0 WHERE moduletypeid!="2" AND '.$moduletabid[$i].' != 1 AND status != 2 ');
			}
			$i++;
		}
		//activate modules based on user plan activation
		$i = 0;
		foreach($moduletables as $table) {
			$fnamechk = $this->tablefieldnamecheck($table,'userroleid',$import_db);
			if($fnamechk == 'true') {
				$import_db->query('UPDATE '.$table.' SET status=1 WHERE '.$moduletabid[$i].' IN ('.$usermodules.') AND userroleid='.$userrole.' AND status=0');

				$import_db->query('UPDATE '.$table.' SET status=0 WHERE '.$moduletabid[$i].' NOT IN ('.$usermodules.') AND userroleid='.$userrole.' AND status=0');
			} else {
				$import_db->query('UPDATE '.$table.' SET status=1 WHERE '.$moduletabid[$i].' IN ('.$planmoduleids.') AND status=0');

				$import_db->query('UPDATE '.$table.' SET status=0 WHERE moduletypeid!="2" AND '.$moduletabid[$i].' NOT IN ('.$planmoduleids.') AND status=0');
			}
			$i++;
		}
		return 'true';
	}
	//table field name check
	public function tablefieldnamecheck($tblname,$fieldname,$import_db) {
		$result = 'false';
		$fields = $import_db->field_data($tblname);
		foreach ($fields as $field) {
			$name =  $field->name;
			if($fieldname == $name) {
				$result = 'true';
			}
		}
		return $result;
	}
	//user plan activation log
	public function userplanactivationinfolog($planid,$companyid,$email,$domainname,$databasename,$signuptype,$signupstatus,$industryid,$mobileno) {
		$this->inlinemasterdbenable(); //main data base select
		$activationdate = date($this->Basefunctions->datef);
		$expiredate = date($this->Basefunctions->datef, strtotime('7 days'));
		$planloginfo = array(
			'industryid'=>$industryid,
			'planid'=>$planid,
			'companyid'=>$companyid,
			'registeremailid'=>$email,
			'registermobileno'=>$mobileno,
			'accountcreationdate'=>$activationdate,
			'activationdate'=>$activationdate,
			'expiredate'=>$expiredate,
			'accountclosedate'=>'0000-00-00 00:00:00',
			'domainnameinfo'=>$domainname,
			'databasenameinfo'=>$databasename,
			'activationstatus'=>'No',
			'signuptype'=>$signuptype,
			'signupstatus'=>$signupstatus,
			'status'=>1
		);
		$this->db->insert('userplaninfo',$planloginfo);
		$primaryid = $this->db->insert_id();
		return $primaryid;
	}
	//user plan activation log
	public function updateuserplanactivationinfolog($userplaninfoid,$companyid,$domainname,$databasename) {
		$this->inlinemasterdbenable(); //main data base select
		$uploginfo = array(
			'companyid'=>$companyid,
			'domainnameinfo'=>$domainname,
			'databasenameinfo'=>$databasename
		);
		$this->db->where('userplaninfoid',$userplaninfoid);
		$this->db->update('userplaninfo',$uploginfo);
	}
	//user sign up log maintain
	public function signupupuserlogcreate($usrplanifoid,$name,$email,$signupsts) {
		$this->inlinemasterdbenable(); //main data base select
		$activationdate = date($this->Basefunctions->datef);
		$browsername = $_SERVER['HTTP_USER_AGENT'];
		$localip = $_SERVER['REMOTE_ADDR'];
		$datas = preg_split("/[()]+/", $browsername);
		$osname = $datas[1];
		$signuploginfo = array(
			'userplaninfoid'=>$usrplanifoid,
			'signupusername'=>$name,
			'registeremailid'=>$email,
			'signupdate'=>$activationdate,
			'ipaddress'=>$localip,
			'browsername'=>$browsername,
			'platformname'=>$osname,
			'signupstatus'=>$signupsts,
			'status'=>1
		);
		$this->db->insert('signupuserinfo',$signuploginfo);
		$primaryid = $this->db->insert_id();
		return $primaryid;
	}
	//Import SQL file into selected database
	public function doImport($database,$filename,$compress,$import_db) {
		if($filename != "") {
			$import_db->query("SET FOREIGN_KEY_CHECKS = 0");
			$import = $this->importSql($filename,$compress,$import_db);
			$import_db->query("SET FOREIGN_KEY_CHECKS = 1");
			if($import == true) 
			{
				return 'true';
			}
			else 
			{
				return 'false';
			}
		}
	}
	//execute query
	public function importSql($file,$compress,$import_db) {
		//reading file content
		if ($compress) {
			$lines = gzfile($file);
		} else {
			$lines = file($file);
		}
		$x = 0;
		$importSql = "";
		$procent = 0;
		foreach ($lines as $line) {
			// Importing SQL
			$importSql .= $line;
			if ( substr(trim($line), strlen(trim($line))-1) == ";" ) {
				$query = $import_db->query($importSql);
				if (!$query) return false;
				$importSql = "";
			}
		}
		return true;
	}
	//free plan information data fetch
	public function freeplaninformationdatafetch($planid,$masdbname) {
		$masdb=$this->inlineuserdatabaseactive($masdbname); //main data base select
		$dataset = "1";
		$data=$masdb->select('planinfo.moduleid')->from('planinfo')->join('plan','plan.planid=planinfo.planid')->where('planinfo.planid',$planid)->get();
		foreach($data->result() as $info) {
			$dataset = $info->moduleid;
		}
		return $dataset;
	}
	//user plan activation if account expire
	public function userplanactivationinaccexpire($planmoduleids) {
		$this->inlinemasterdbenable();
		//fetch employee information
		$userrole = $this->Basefunctions->userroleid;
		//fetch user created external links
		$j=0;
		$userextmod = array();
		$data=$this->db->query('select module.moduleid from module where moduletypeid="2" AND status IN (1)');
		foreach($data->result() as $info) {
			$userextmod[$j] = $info->moduleid;
			$j++;
		}
		//module id based on user role
		$i=0;
		$usermod = array();
		$data=$this->db->query('select moduleinfo.moduleinfoid,moduleinfo.moduleid from moduleinfo where moduleinfo.userroleid="'.$userrole.'" AND status=1');
		foreach($data->result() as $info) {
			$usermod[$i] = $info->moduleid;
			$i++;
		}
		//fetch sub module ids
		$dashid = array();
		$dashmodules = $this->db->query('select module.moduleid,module.moduledashboardid from module where moduleid IN ('.$planmoduleids.') AND moduledashboardid!=1 AND moduledashboardid!="" ',false);
		foreach($dashmodules->result() as $dashrow) {
			$dashid[] = $dashrow->moduledashboardid;
		}
		//get submodule ids based userrole
		$dashids = implode(',',$dashid);
		$dashids = ( ($dashids=="") ? '1' : $dashids );
		$moddashids = array();
		$dashmodules = $this->db->query('select moduleinfo.moduleid from moduleinfo where userroleid="'.$userrole.'" AND moduleid IN ('.$dashids.') AND status=1',false);
		foreach($dashmodules->result() as $dashrow) {
			$moddashids[] = $dashrow->moduleid;
		}
		//activate modules
		$planmodids = explode(',',$planmoduleids);
		$modids = array_intersect($planmodids,$usermod);
		$usrmodids = array_merge($modids,$userextmod);
		$usermodids = array_merge($usrmodids,$moddashids);
		$usermodules = implode(',',$usermodids);
		if($usermodules == "") {
			$usermodules = 1;
		}
		$moduletables = array('module','moduleinfo');
		$moduletabid = array('moduleid','moduleid');
		$i = 0;
		//diable modules
		foreach($moduletables as $table) {
			$fnamechk = $this->tablefieldnamecheck($table,'userroleid',$import_db);
			if($fnamechk == 'true') {
				$this->db->query('UPDATE '.$table.' SET status=0 WHERE '.$moduletabid[$i].' != 1 AND status != 2  AND userroleid='.$userrole.' ');
			} else {
				$this->db->query('UPDATE '.$table.' SET status=0 WHERE moduletypeid!="2" AND '.$moduletabid[$i].' != 1 AND status != 2 ');
			}
			$i++;
		}
		//activate modules based on user plan activation
		$i = 0;
		foreach($moduletables as $table) {
			$fnamechk = $this->tablefieldnamecheck($table,'userroleid',$import_db);
			if($fnamechk == 'true') {
				$this->db->query('UPDATE '.$table.' SET status=1 WHERE '.$moduletabid[$i].' IN ('.$usermodules.') AND userroleid='.$userrole.' AND status=0');

				$this->db->query('UPDATE '.$table.' SET status=0 WHERE '.$moduletabid[$i].' NOT IN ('.$usermodules.') AND userroleid='.$userrole.' AND status=0');
			} else {
				$this->db->query('UPDATE '.$table.' SET status=1 WHERE '.$moduletabid[$i].' IN ('.$planmoduleids.') AND status=0');

				$this->db->query('UPDATE '.$table.' SET status=0 WHERE moduletypeid!="2" AND '.$moduletabid[$i].' NOT IN ('.$planmoduleids.') AND status=0');
			}
			$i++;
		}
		return 'true';
	}
	//inline default data base set
	public function inlinemasterdbenable() {
		$active_group = 'default';
		$query_builder = TRUE;
		$host = $this->db->hostname;
		$user = $this->db->username;
		$passwd = $this->db->password;
		$masdb=$this->db->masterdb;
		$db['default']['hostname'] = $host;
		$db['default']['username'] = $user;
		$db['default']['password'] = $passwd;
		$db['default']['database'] = $masdb;
		$db['default']['dbdriver'] = 'mysqli';
		$db['default']['dbprefix'] = '';
		$db['default']['pconnect'] = TRUE;
		$db['default']['db_debug'] = TRUE;
		$db['default']['cache_on'] = FALSE;
		$db['default']['cachedir'] = '';
		$db['default']['char_set'] = 'utf8';
		$db['default']['dbcollat'] = 'utf8_general_ci';
		$db['default']['swap_pre'] = '';
		$db['default']['autoinit'] = TRUE;
		$db['default']['stricton'] = FALSE;
		$db['default']['masterdb'] = $masdb;
		$this->load->database('default',TRUE);
	}
	//inline user data base set
	public function inlineuserdatabaseactive($databasename) {
		$host = $this->db->hostname;
		$user = $this->db->username;
		$passwd = $this->db->password;
		$dbconfig['hostname'] = $host;
		$dbconfig['username'] = $user;
		$dbconfig['password'] = $passwd;
		$dbconfig['database'] = $databasename;
		$dbconfig['dbdriver'] = 'mysqli';
		$dbconfig['dbprefix'] = '';
		$dbconfig['pconnect'] = TRUE;
		$dbconfig['db_debug'] = TRUE;
		$dbconfig['cache_on'] = FALSE;
		$dbconfig['cachedir'] = '';
		$dbconfig['char_set'] = 'utf8';
		$dbconfig['dbcollat'] = 'utf8_general_ci';
		$dbconfig['swap_pre'] = '';
		$dbconfig['autoinit'] = TRUE;
		$dbconfig['stricton'] = FALSE;
		return $import_db = $this->load->database($dbconfig, TRUE);
	}
	//check login date expire check
	public function loginexpiredatecheck($tdate,$username,$current_date) {
		//expire date fetch
		$logexpdate = $this->db->select('activationdate,expiredate')->from('userplaninfo')->where('registeremailid',$username)->where('expiredate >=',$current_date)->where('status',1)->get();
		$logexpdatecount = $logexpdate->num_rows();
		//check already logged in on that day
		$tlogchk = $this->db->select('logindate,loginstatus')->from('logindayhistory')->where('logindate',$tdate)->where('status',1)->get();
		$tlogchkcount = $tlogchk->num_rows();
		if($logexpdatecount>=1) {//license not expired
			if($tlogchkcount>=1) {//check re login on same day
				//check user changed/unchanged system date.
				$tlogchkcount1 = $this->logindayhistorycheck($tdate);
				if($tlogchkcount1>=1) {
					return 'FALSE';
				} else {
					//check if user reset the date as last day of product expire
					$loghischk = $this->loginhistorylogincheck($current_date);
					if($loghischk>=1) {
						return 'TRUE';
					} else {
						return 'FALSE';
					}
				}
			} else {//new login on that day
				//check user changed/unchanged system date.
				$tlogchkcount1 = $this->logindayhistorycheck($tdate);
				if($tlogchkcount1>=1) {
					return 'FALSE';
				} else {
					$this->db->insert('logindayhistory',array('logindate'=>$tdate,'loginstatus'=>'Yes','status'=>1));
					return 'TRUE';
				}
			}
		} else { //license expired
			return 'FALSE';
		}
	}
	//check if user reset the date as last day of product expire
	public function loginhistorylogincheck($current_date) {
		$loghischk = $this->db->select('loginhistoryid')->from('loginhistory')->where("(signintime >= '$current_date' OR signouttime >= '$current_date')")->get();
		$loghischkcount = $loghischk->num_rows();
		return $loghischkcount;
	}
	//check user changed/unchanged system date.
	public function logindayhistorycheck($tdate) {
		$tlogchk = $this->db->select('logindate,loginstatus')->from('logindayhistory')->where('logindate >',$tdate)->where('status',1)->get();
		$tlogchkcount = $tlogchk->num_rows();
		return $tlogchkcount;
	}
	public function showhidemodulefields($moduleid,$fieldis,$status,$dbname)
	{	$devdb = $this->inlineuserdatabaseactive($dbname);
		$devdb->query('UPDATE modulefield SET status='.$status.' 
		WHERE moduletabid='.$moduleid.' AND modulefieldid IN ("'.trim($fieldis, '"').'")');
		return 'TRUE';
	}
}
?>