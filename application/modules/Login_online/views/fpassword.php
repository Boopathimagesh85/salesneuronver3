<html>
<head>  
	<title>Sales Neuron</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<?php echo base_url();?>img/favi.ico"/>
	<link href="<?php echo base_url();?>css/erpcustom.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>css/allinonecss.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/foundation/normalize.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/foundation/foundation.css"/>
	<script src="<?php echo base_url();?>js/plugins/allpluginheader.min.js"></script>
	<script src="<?php echo base_url();?>js/plugins/allpluginbottom.min.js"></script>
	<script src="<?php echo base_url();?>js/foundation/vendor/modernizr.js"></script>	
	<script>
		var base_url = '<?php echo base_url();?>';
		$(document).ready(function() {
			{// Foundation Initialization
				$(document).foundation();
			}
			$('body').fadeIn(1000);
			$('#username').focus();
			/* On enter login trigger*/
			$('#loginpassword,#password').keypress(function(e){	
				if(e.which == 13) {
					$("#loginsubmit").trigger('click');
				}
			});
			$('#forgetusername').keypress(function(e){	
				if(e.which == 13) {
					$("#forgotsubmit").trigger('click');
				}
			});
			$('#securityanswer').keypress(function(e){	
				if(e.which == 13) {
					$("#secanswersubmit").trigger('click');
				}
			});
			$('#retypepassword').keypress(function(e){
				if(e.which == 13) {
					$("#changepasswordsubmit").trigger('click');
				}
			});
			/*Alert Close*/	
			$("#alertscloseup").click(function()
			{
				$("#alerts").fadeOut();
			});
			{/* login button  */
				$("#loginsubmit").click(function(){
					$("#loginindexvalidationspan").validationEngine('validate');
				});
				jQuery("#loginindexvalidationspan").validationEngine({
					onSuccess: function() {
					   var login= $("#loginform").serialize();
						var amp = '&';
						var loginuser = amp + login;
						var domain=base_url;
						$.ajax({  
							type: "POST",
							dataType:'json',
							url:base_url+"Login/verifylogin",
							data:"logindata="+ loginuser,             
							success: function(data) {
								if(data.status == 'success') {
									$("#normalbg").animate({left:'-18%'},250);
									$("#normalbg").fadeOut(20);	
									var dname = data.domainname
									domain=dname;
									window.location.replace(domain+'home');
								} else {
									$('#username').validationEngine('showPrompt',data.status, 'error', 'topLeft',true);
								}
							 },
						});
					},
					onFailure: function() {
						alertpopup('Enter Correct Username/Password');
					}	
				});
			}
			{/* forgotpassword submit button  */
				$("#forgotsubmit").click(function(){
					$("#forgotscreen").validationEngine('validate');
				});
				jQuery("#forgotscreen").validationEngine({
					onSuccess: function() {
						var forgotpass= $("#forgotpasswordform").serialize();
						var amp = '&';
						var loginpass = amp + forgotpass ;
						$.ajax({
							type: "POST",
							url:base_url+"Login/verifyusername",
							data:"logindata="+ loginpass, 
							dataType:'json',
							success: function(data) {
								if((data.status) == 'success') {
									var emailid = $('#forgetusername').val();
									sendpasswordlink(emailid);
								} else {
									$('#forgetusername').validationEngine('showPrompt',data.status, 'error', 'topLeft',true);
								}
							}, 
						});
					},
					onFailure: function() {
					}			
				});
											
			}
			{/* forgotpassword loginscreen button  */
				$("#forgotlink").click(function(){ 	
					$("#headingchange").text('Forgot Password');
					$(".loginformdiv").hide();
					$(".forgetpasswordformdiv").fadeIn();
					$("#forgetusername").focus();
					
				});
			}
		
			{/* security question submit button  */
				$("#secanswersubmit").click(function()
				{
					$("#verifysecurityform").validationEngine('validate');
				});
				jQuery("#verifysecurityform").validationEngine({
					onSuccess: function() {
						var securitypass= $("#verifysecurityform").serialize();
						var amp = '&';
						var loginpass = amp + securitypass ;
						$.ajax({  
							type: "POST",
							url:base_url+"Login/verifysecurityinfo",
							data:"logindata="+ loginpass, 
							dataType:'json',				
							success: function(data) {                
								if((data.fail) == 'FAILED') {
									var msg='Invalid Security Answer';
									$('#securityanswer').validationEngine('showPrompt',msg, 'error', 'topLeft',true);
								} else {
									$("#headingchange").text('Change Password');
									$(".securityqaformdiv").hide();
									$(".changepasswordformdiv").fadeIn();
									jQuery('#credentialuserid').val(data.userid);
									$("#newpassword").focus();
								}
							},
						});
					},
					onFailure: function() {
					}			
				});	
			}		
			{/* password reset  submit button  */
				$("#changepasswordsubmit").click(function() {
					$("#changepasswordform").validationEngine('validate');
				});
				jQuery("#changepasswordform").validationEngine({
					onSuccess: function() {
						var securitypass= $("#changepasswordform").serialize();
						var amp = '&';
						var loginpass = amp + securitypass ;
						$.ajax({  
							type: "POST",
							url:base_url+"Login/changepassword",
							data:"logindata="+ loginpass, 
							success: function(msg) {
								if(msg == 'TRUE') {
									//
									window.location = base_url;
								} else {
									var msg = 'Password update fail';
									$('#newpassword').validationEngine('showPrompt',msg, 'error', 'topLeft',true);
								}
							},
						});
					},
					onFailure: function() {
					}			
				});	
			}
			{// on Click Logo Come to log In screen
				$("#homescreenlogo").click(function() {
					$(".forgetpasswordformdiv,.securityqaformdiv,.changepasswordformdiv").hide();
					$(".loginformdiv").fadeIn();
				});
			}
			{/* Signup link  */
				$("#signuplink").click(function() { 	
					$("#headingchange").text('Sign Up');
					$(".loginformdiv").hide();
					$(".signupformdiv").fadeIn();
					$("#signupname").focus();
				});
			}
		});
		//new email
		function uniqueemail() {
			var signupemail=$('#signupemail').val();
			var status=0;
			$.ajax({
				url:base_url+"Login/uniqueemail?signupemail="+signupemail,
				async:false,
				success: function(msg) {
					if(msg == 'FAIL') {
						status=1;
					}
				},
			});
			if(status==1) {
				return "email exist";
			}
		}
		//unique domain name
		function uniquedomain() {
			var signupdomain=$('#userdomain').val();
			var status=0;
			$.ajax({
				url:base_url+"Login/uniquedomaincheck?domainname="+signupdomain,
				async:false,
				success: function(msg) {
					if(msg == 'fail') {
						status = 1;
					}
				},
			});
			if(status==1) {
				return "Domain name exist";
			}
		}
		//send forget password link
		function sendpasswordlink(emailid) {
			var amp = '&';
			var info = amp+'emailid='+emailid+amp+'mailtype=fpassword';
			info
			$.ajax({
				type: "POST",
				url:base_url+"datamail.php",
				data:"logindata="+ info,
				dataType:'json',
				success: function(msg) {
					if(msg=='success') {
					}
				}, 
			});
		}
		function alertpopup(txtmsg) {
			$(".alertinputstyle").val(txtmsg);
			$('#alerts').fadeIn('fast');
		}
		//access mailer
		function emaillogin(email,password) {
			var file='erpmail/rclogin.php';
			$('body').append('<form id="emaillogin" style="display:none;" name="emaillogin" method="post" action="'+base_url+''+file+'"><input type="hidden" name="email" id="email"/><input type="hidden" name="pp" id="pp" value=""/><input type="hidden" name="action" id="action" value=""/><input type="submit"/></form>');
			$('#action').val('login');
			$('#pp').val(password);
			$('#email').val(email);
			$('#emaillogin').submit();
		}
	</script>
</head>
<body style='background:#dce9f1 !important'>
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row" style="max-width:93.5%">
		<div class="large-12 columns">
			<div class="large-4 columns large-centered" style="text-align:center">
				<img src="<?php echo base_url();?>/img/homepagelogo.png" id="homescreenlogo" style="width:20rem;cursor:pointer"/>
			</div>
		</div>
		<div class="large-12 columns">&nbsp;</div>
		<div class="large-12 columns">&nbsp;</div>
		<div class="large-12 columns">
			<div class="large-4 columns large-centered ">
				<div class="large-12 columns paddingzero" style="background:#f5f5f5;">
					<div class="large-12 columns headerformcaptionstyle" id="headingchange">Reset your Password</div>
					<form id="changepasswordform" action="" class="changepasswordformdiv">
						<span id="changepassword" class="validationEngineContainer">	
							<div class="large-12 columns">
								<label>Type New Password</label>
								<input type="password" id="newpassword" name="newpassword" value="" class="validate[required,minSize[6],maxSize[30]]" data-prompt-position="topLeft">
							</div>
							<div class="large-12 columns">
								<label>Retype New Password</label>
								<input type="password" id="retypepassword" name="retypepassword" value="" class="validate[required,minSize[6],maxSize[30],equals[newpassword]]" data-prompt-position="topLeft">
							</div>
							<input type="hidden" id ="credentialuserid"  value="<?php echo $emailid;?>"  name="userid" class=""/>
							<div class="large-12 columns">&nbsp;</div>
							<div class="large-12 columns centertext">							
								<input type="button" class="button homepageloginbtn" value="Submit" id="changepasswordsubmit"/>
							</div>
							<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
							<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
						</span>
					</form>
					<div class="large-12 columns" style="text-align:center;">
						<span></span>
					</div>
				</div>
			</div>	
		</div>
	</div>
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay" id="successalerts">	
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>		
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="large-3 medium-6 small-10 large-centered medium-centered small-centered columns">
				<div class="row">&nbsp;</div>
				<div class="row">
				<div class="large-12 columns alertsheadercaptionstyle" style="text-align:left">
					<div class="small-12 columns"><span style="color:#ffffff;padding-right:0.5rem"><i class="material-icons">warning</i> </span> Alert</div>
				</div>
				<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
					<div class="large-12 large-centered columns" style="background:#f5f5f5;text-align:center">
						<span class="alertinputstyle">Company Create Successfully !!! Redirecting To Home Page..</span>
						<div class="large-12 column">&nbsp;</div>
					</div>
				</div>
				<div class="row">&nbsp;</div>
			</div>
		</div>
	</div>
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay" id="failurealerts">
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>		
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="large-3 medium-6 small-10 large-centered medium-centered small-centered columns">
				<div class="row">&nbsp;</div>
				<div class="row">
				<div class="large-12 columns alertsheadercaptionstyle" style="text-align:left">
					<div class="small-12 columns"><span style="color:#ffffff;padding-right:0.5rem"><i class="material-icons">warning</i></span> Alert</div>
				</div>
				<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
					<div class="large-12 large-centered columns" style="background:#f5f5f5;text-align:center">
						<span class="alertinputstyle">Sorry Company Not Created !!!</span>
						<div class="large-12 column">&nbsp;</div>
					</div>
				</div>		
				<div class="row">&nbsp;</div>
			</div>
		</div>
	</div>
</body>   
</html>