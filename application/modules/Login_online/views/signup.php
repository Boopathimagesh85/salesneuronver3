<html>
<head>  
	<title>Sales Neuron</title>
	<link rel="shortcut icon" href="<?php echo base_url();?>img/favi.ico"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/allinonecss.css"  media="screen" />
	<script src="<?php echo base_url();?>js/plugins/allpluginheader.min.js"></script>
	<script src="<?php echo base_url();?>js/plugins/allpluginbottom.min.js"></script>
	<script>
		var base_url = '<?php echo base_url();?>';
		$(document).ready(function() {
			$('body').fadeIn(1000);
			$('#username').focus();
			/* On enter login trigger*/
			$('#loginpassword,#password').keypress(function(e){	
				if(e.which == 13) {
					$("#loginsubmit").trigger('click');
				}
			});
			$('#forgetusername').keypress(function(e){	
				if(e.which == 13) {
					$("#forgotsubmit").trigger('click');
				}
			});
			$('#securityanswer').keypress(function(e){	
				if(e.which == 13) {
					$("#secanswersubmit").trigger('click');
				}
			});
			$('#retypepassword').keypress(function(e){
				if(e.which == 13) {
					$("#changepasswordsubmit").trigger('click');
				}
			});
			/*Alert Close*/	
			$("#alertscloseup").click(function() {
				$("#alerts").fadeOut();
			});
			{/* Signup link  */
				$("#signuplink").click(function() { 	
					$("#headingchange").text('Sign Up');
					$(".loginformdiv").hide();
					$(".signupformdiv").fadeIn();
					$("#signupname").focus();
				});
			}
			{/* Signup link*/
				$("#signupsubmit").mousedown(function(e) {
					if(e.which == 1 || e.which === undefined) {
						$("#newcompanycreate").validationEngine('validate');
					}
				});
				/* Signup form validation*/
				jQuery("#newcompanycreate").validationEngine({
					onSuccess: function() {
						$('#signupsubmit').prop("disabled",true);
						$("#signupprocessoverlay").show();
						$('#signupform').submit();
					},
					onFailure: function() {
						$('#signupsubmit').prop("disabled",false);
					}
				});
				//demo data check box events
				$('#demodata').click(function(){
					if($('#demodata').is(':checked')) {
						$('#hiddemodata').val('Yes');
					} else {
						$('#hiddemodata').val('No');
					}
				});
			}
			{ //user time zone set
				var offset = (new Date()).getTimezoneOffset();
				var timezones = {
					'-12': 'Pacific/Kwajalein',
					'-11': 'Pacific/Samoa',
					'-10': 'Pacific/Honolulu',
					'-9': 'America/Juneau',
					'-8': 'America/Los_Angeles',
					'-7': 'America/Denver',
					'-6': 'America/Mexico_City',
					'-5': 'America/New_York',
					'-4': 'America/Caracas',
					'-3.5': 'America/St_Johns',
					'-3': 'America/Argentina/Buenos_Aires',
					'-2': 'Atlantic/Azores',
					'-1': 'Atlantic/Azores',
					'0': 'Europe/London',
					'1': 'Europe/Paris',
					'2': 'Europe/Helsinki',
					'3': 'Europe/Moscow',
					'3.5': 'Asia/Tehran',
					'4': 'Asia/Baku',
					'4.5': 'Asia/Kabul',
					'5': 'Asia/Karachi',
					'5.5': 'Asia/Calcutta',
					'6': 'Asia/Colombo',
					'7': 'Asia/Bangkok',
					'8': 'Asia/Singapore',
					'9': 'Asia/Tokyo',
					'9.5': 'Australia/Darwin',
					'10': 'Pacific/Guam',
					'11': 'Asia/Magadan',
					'12': 'Asia/Kamchatka' 
				};
				var datazone =  timezones[-offset / 60];
				$('#hidzonedata').val(datazone);
			}
		});
		//new email
		function uniqueemail() {
			var signupemail=$('#signupemail').val();
			var status=0;
			$.ajax({
				url:base_url+"Login/uniqueemail?signupemail="+signupemail,
				async:false,
				success: function(msg) {
					if(msg == 'FAIL') {
						status=1;
					}
				},
			});
			if(status==1) {
				return "email exist";
			}
		}
		//unique domain name
		function uniquedomain() {
			var signupdomain=$('#userdomain').val();
			if(signupdomain!='salesneuron') {
				var status=0;
				$.ajax({
					url:base_url+"Login/uniquedomaincheck?domainname="+signupdomain,
					async:false,
					success: function(msg) {
						if(msg == 'fail') {
							status = 1;
						}
					},
				});
				if(status==1) {
					return "Domain name exist";
				}
			} else {
				return "Domain name exist";
			}
		}
		function alertpopup(txtmsg) {
			$(".alertinputstyle").val(txtmsg);
			$('#alerts').fadeIn('fast');
		}
		//access mailer
		function emaillogin(email,password) {
			var file='erpmail/rclogin.php';
			$('body').append('<form id="emaillogin" style="display:none;" name="emaillogin" method="post" action="'+base_url+''+file+'"><input type="hidden" name="email" id="email"/><input type="hidden" name="pp" id="pp" value=""/><input type="hidden" name="action" id="action" value=""/><input type="submit"/></form>');
			$('#action').val('login');
			$('#pp').val(password);
			$('#email').val(email);
			$('#emaillogin').submit();
		}
	</script>
	<style>
		.weak{
			background-color: #FFB78C;
			border: 1px solid #FF853C!important;
			width:25%!important;
			-webkit-transition: border .25s linear, color .25s linear;
			-moz-transition: border .25s linear, color .25s linear;
			-o-transition: border .25s linear, color .25s linear;
			transition: border .25s linear, color .25s linear;
			-webkit-backface-visibility: hidden;
		}
		.strong{
			background-color: #FFEC8B;
			border: 1px solid #FC0!important;
			width:50%!important;
			-webkit-transition: border .25s linear, color .25s linear;
			-moz-transition: border .25s linear, color .25s linear;
			-o-transition: border .25s linear, color .25s linear;
			transition: border .25s linear, color .25s linear;
			-webkit-backface-visibility: hidden;
		}
		.stronger{
			background-color: #C3FF88;
			border: 1px solid #8DFF1C!important;
			width:75%!important;
			-webkit-transition: border .25s linear, color .25s linear;
			-moz-transition: border .25s linear, color .25s linear;
			-o-transition: border .25s linear, color .25s linear;
			transition: border .25s linear, color .25s linear;
			-webkit-backface-visibility: hidden;
		}
		.strongest{
			background-color: #B6FF6C;
			border: 1px solid #8DFF1C!important;
			width:100%!important;
			-webkit-transition: border .25s linear, color .25s linear;
			-moz-transition: border .25s linear, color .25s linear;
			-o-transition: border .25s linear, color .25s linear;
			transition: border .25s linear, color .25s linear;
			-webkit-backface-visibility: hidden;
		}
		.default{
		}
		#passwdmeter{
			transition:all 1s ease;
		}
	</style>
</head>
<body style='background:#dce9f1 !important; overflow-y: scroll;'>
	<div class="row">&nbsp;</div>
	<div class="row" style="max-width:93.5%;">
		<div class="large-12 columns">
			<div class="large-4 columns large-centered" style="text-align:center">
				<img src="<?php echo base_url();?>/img/homepagelogo.png" id="homescreenlogo" style="width:20rem;cursor:pointer"/>
			</div>
		</div>
		<div class="large-12 columns">&nbsp;</div>
		<div class="large-12 columns">
			<div class="large-4 columns large-centered ">
				<div class="large-12 columns paddingzero" style="background:#f5f5f5;">
					<div class="large-12 columns headerformcaptionstyle" id="headingchange">Sign Up</div>
					<div class="large-12 columns" style="text-align:center;">
						<span style="color:#D95C5C; font-family:segoe ui; font-size:0.9em;"></span>
					</div>
					<form id="signupform" accept-charset="utf-8" action="<?php echo base_url(); ?>Login/newsignupcreate" class="signupformdiv" method="POST">
						<span class="validationEngineContainer" id="newcompanycreate">
							<div class="large-12 small-6 medium-12 columns" style="display:none;">
								<label>Name<span class="mandatoryfildclass">*</span></label>
								<input type="text" id="signupname" name="signupname" value="" class="validate[]" data-prompt-position="topLeft">
							</div>
							<div class="large-12 small-6 medium-12 columns">
								<label>Company Name<span class="mandatoryfildclass">*</span></label>
								<input type="text" id="signupcompany" name="signupcompany" value="" class="validate[required]" data-prompt-position="topLeft">
							</div>
							<div class="large-12 small-6 medium-12 columns">
								<label>Email Address<span class="mandatoryfildclass">*</span></label>
								<input type="text" id="signupemail" name="signupemail"  value="<?php echo $emailid; ?>" class="validate[required,custom[email],funcCall[uniqueemail]]" data-prompt-position="topLeft" />
							</div>
							<div class="large-12 small-6 medium-12 columns">
								<label>Password<span class="mandatoryfildclass">*</span></label>
								<input type="password" id="signuppassword" name="signuppassword" value="" class="validate[required,minSize[6],maxSize[30]]" data-prompt-position="topLeft">
							</div>
							<div class="large-12 small-6 medium-12 columns" style="display:none;">
								<label>Confirm Password<span class="mandatoryfildclass">*</span></label>
								<input type="password" id="signupconfirm" name="signupconfirm" value="" class="validate[]" data-prompt-position="topLeft">
							</div>
							<div class="large-12 small-6 medium-12 columns" style="display:none;">
								<div class="large-5 medium-5 small-5 columns"  style="padding-right:0; padding-left:0">
									<label>Domain Name<span class="mandatoryfildclass">*</span></label>
									<input type="text" id="userdomain" name="userdomain" value="" class="validate[]" data-prompt-position="topLeft">
								</div>
								<div class="large-7 medium-7 small-7 large-centered columns" style="padding-left:0.3em">
									<label>&nbsp;</label>
									<label><span class="mandatoryfildclass" style="font-size:1.1em" title=".salesneuron.com/crm/">.salesneuron.com/crm/</span></label>
								</div>
							</div>
							<div class="large-12 small-6 medium-12 columns" style="display:none;">
								<label>Demo Data</label>
								<input type="checkbox" id="demodata" name="demodata" value="No" class="" data-prompt-position="topLeft">
							</div>
							<div class="large-12 small-6 medium-12 columns centertext">					
								<div class="large-12 columns hpforgertpwdstyle" style="font-size:0.8em; line-height:1.5">By clicking "Sign Up", you agree to the <a href="../termsofservice.pdf" target="_blank">Terms of Service </a>& <a href="../privacypolicy.pdf" target="_blank">Privacy Policy</a>.</div>
							</div>
							<div class="large-12 columns">&nbsp;</div>
							<div class="large-12 columns">&nbsp;</div>
							<div class="large-12 columns centertext">
								<input type="button" class="button homepageloginbtn" name="signupsubmit" value="Sign Up" id="signupsubmit"/>
							</div>
							<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
						</span>
						<!-- hidden fields -->
						<input type="hidden" name="hiddemodata" id="hiddemodata" value="No" />
						<input type="hidden" name="hidplandata" id="hidplandata" value="<?php if($planid!='') { echo $planid; } else { echo "3"; } ?>" />
						<input type="hidden" name="hidzonedata" id="hidzonedata" value="<?php if($zonedata!='') { echo $zonedata; } else { echo "Asia/Calcutta"; } ?>" />
					</form>
				</div>
			</div>	
		</div>
	</div>
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay" id="successalerts">	
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>		
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="large-3 medium-6 small-10 large-centered medium-centered small-centered columns">
				<div class="row">&nbsp;</div>
				<div class="row">
				<div class="large-12 columns alertsheadercaptionstyle" style="text-align:left">
					<div class="small-12 columns"><span style="color:#ffffff;padding-right:0.5rem"><i class="material-icons">warning</i> </span> Alert</div>
				</div>
				<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
					<div class="large-12 large-centered columns" style="background:#f5f5f5;text-align:center">
						<span class="alertinputstyle">New crm workspace created successfully !!! Redirecting To Home Page..</span>
						<div class="large-12 column">&nbsp;</div>
					</div>
				</div>
				<div class="row">&nbsp;</div>
			</div>
		</div>
	</div>
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay" id="failurealerts">	
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>		
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="large-3 medium-6 small-10 large-centered medium-centered small-centered columns">
				<div class="row">&nbsp;</div>
				<div class="row">
				<div class="large-12 columns alertsheadercaptionstyle" style="text-align:left">
					<div class="small-12 columns"><span style="color:#ffffff;padding-right:0.5rem"><i class="material-icons">warning</i></span> Alert</div>
				</div>
				<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
					<div class="large-12 large-centered columns" style="background:#f5f5f5;text-align:center">
						<span class="alertinputstyle">Sorry new workspace not created !!!</span>
						<div class="large-12 column">&nbsp;</div>
					</div>
				</div>
				<div class="row">&nbsp;</div>
			</div>
		</div>
	</div>
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay" id="signupprocessoverlay">	
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>		
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="large-4 medium-6 small-10 large-centered medium-centered small-centered columns">
				<div class="row">&nbsp;</div>
				<div class="row">
					<div class="large-12 columns alertsheadercaptionstyle" style="text-align:left">
						<div class="small-12 columns"><span style="color:#ffffff;padding-right:0.5rem"><i class="material-icons">warning</i></span>Workspace Creation</div>
					</div>
					<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
					<div class="large-12 large-centered columns" style="background:#f5f5f5;text-align:center">
						<div class="large-12 columns">
							<img src="<?php echo base_url();?>/img/homepagelogo.png" id="" style="width:10rem;cursor:pointer"/>
						</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">
							<span style="color:#D95C5C; font-family:segoe ui; font-size:1.1em;">Congratulations ! </span>
						</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">
							<span class="alertinputstyle">You have successfully signed up.</span>
						</div>
						<div class="large-12 column">&nbsp;</div>
						<div class="large-12 columns">
							<span class="alertinputstyle">Please wait as we set up your new crm workspace</span>
						</div>
						<div class="large-12 column">&nbsp;</div>
						<div class="large-12 columns">
							<span class="" style="font-size:2rem;color:#546e7a"><i class="material-icons">refresh</i></span>
						</div>
						<div class="large-12 column">&nbsp;</div>
					</div>
				</div>			
				<div class="row">&nbsp;</div>
			</div>
		</div>
	</div>
</body>   
</html>