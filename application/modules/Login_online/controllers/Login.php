<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller 
{
	function __construct() {
    	parent::__construct();	
		$this->load->model('Login/User');
		$this->load->model('Base/Basefunctions');
    }
	//import file format
	public $utf8 = true;
    //Home Page Screen
	public function index() {
		if($this->session->userdata('logged_in') == NULL) {
			$secure = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "");
			if($_SERVER['HTTP_HOST']=='localhost') {
				$this->load->view('Login/index');
			} else {
				$root = "http".$secure."://".$_SERVER['HTTP_HOST'].'/login.php';
				redirect($root,'location');
			}
		} else {
			$checkindustry = $this->session->userdata('industryids');
			if($checkindustry['industryid'] == 3) {
				redirect(base_url().'Home','location');
			} else {
				redirect(base_url().'Home','location');
			}
			
		}
	}
	/*********************
		Signup process
	*********************/
	//new signup create //direct signup form site
	public function newsignupcreate() {
		$secure = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "");
		$name = ( isset($_POST['signupcompany'])? trim($_POST['signupcompany']) : '');
		$cname = ( isset($_POST['signupcompany'])? trim($_POST['signupcompany']) : '');
		$email = ( isset($_POST['signupemail'])? trim($_POST['signupemail']) : '');
		$mobileno = ( isset($_POST['signupmobileno'])? trim($_POST['signupmobileno']) : '');
		$password = ( isset($_POST['signuppassword'])? trim($_POST['signuppassword']) : '');
		$industryid = ( isset($_POST['industryid'])? trim($_POST['industryid']) : '1');
		$planid = ( isset($_POST['hidplandata'])? trim($_POST['hidplandata']) : '1');
		$zone = ( isset($_POST['hidzonedata'])? trim($_POST['hidzonedata']) : 1);
		$signuptype = ( isset($_POST['signuptype'])? trim($_POST['signuptype']) : 'general');
		$signupimg = ( isset($_POST['signupuserimg'])? trim($_POST['signupuserimg']) : '');
		
		if( ($cname!='' && $email!='' && $password!='' && $signuptype=='general') || ($email!='' && $name!='' && $signuptype!='general') ) {
			$data=$this->User->newcompanycreate($name,$cname,$email,$password,$planid,$zone,$signuptype,$signupimg,$industryid,$mobileno);
			//direct login
			if(is_array($data)) {
				$newdata=$this->User->login($data['username'],$data['password'],$data['domainname'],$data['database'],'success',$data['mastercompanyid'],$signuptype,'Yes',$planid,$industryid);
				$logindataset = json_decode($newdata,true);
				if($logindataset['status'] == 'success') {
					//send account verification mail
					//$this->load->helper('phpmailer');
					//$this->sendverificationmail($email,$zone,$name);
					if($_SERVER['HTTP_HOST']!='localhost') {
						//echo 'a1'; die();

						redirect(''.$logindataset['domainname'].'Home/','refresh');
					} else {
						//echo 'a2'; die();
						$root = str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
						redirect('http'.$secure.'://'.$_SERVER['HTTP_HOST'].$root.'Home/','location');
					}
				} else {
					 //echo 'a3'; die();
					//echo $_SERVER['HTTP_HOST']; die();
					//redirect('http'.$secure.'://'.$_SERVER['HTTP_HOST'].'/','location');
					$root = str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
					redirect('http'.$secure.'://'.$_SERVER['HTTP_HOST'].$root.'Home/','location');
				}
			} else {
				//echo 'a4'; die();
				echo $data;
			}
		} else {
			header('Location:http'.$secure.'://'.$_SERVER['HTTP_HOST'].'/');
		}
	}
	//signup confimation
	public function confirmation() {
		$this->signuplogout();
		sleep(1);
		$error='na';
		$secure = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "");
		if( isset($_GET['SN']) ) {
			$datasets = $_GET['SN'];
			$datas = base64_decode($datasets);
			$decoddatasets =  strrev($datas);
			$userdatas = explode('a1o8eg4',$decoddatasets);
			//mail id
			$dataemail = $userdatas[0];
			$email =  explode('sns9s',$dataemail);
			$usermail = base64_decode($email[1]);
			//date
			$expdate = $userdatas[1];
			$datesets =  explode('sns9s',$expdate);
			$date = base64_decode($datesets[1]);
			//time zone
			$zoneinfo = $userdatas[2];
			$zonesets =  explode('sns9s',$zoneinfo);
			$tzones = base64_decode($zonesets[1]);
			date_default_timezone_set($tzones);
			$curdate = date("YmdHis");
			//check email validity
			if($curdate <= $date) {
				$check = $this->User->signupemailverification($usermail,$tzones);
				if($check=='activated') {
					$error='aa';
				} else if($check=='success') {
					$error='as';
					//send welcome mail
					$userdb = $this->Basefunctions->generalinformaion('userplaninfo','databasenameinfo','registeremailid',$usermail);
					//userdatabase connect
					$empname = 'User';
					$clientdb = $this->User->inlineuserdatabaseactive($userdb);
					$empdata = $clientdb->select('employeename')->from('employee')->where('emailid',$usermail)->get();
					foreach($empdata->result() as $data){
						$empname = $data->employeename;
					}
					$this->load->helper('phpmailer');
					$this->sendwelcomemail($usermail,$empname);
				} else if($check=='expired') {
					$error='ae';
				} else {
					$error='na';
				}
			} else {
				$error = 'le';
			}
			if($_SERVER['HTTP_HOST']=='localhost') {
				header('Location:http'.$secure.'://'.$_SERVER['HTTP_HOST'].'/salesneuronsite/emailconfirmation.php?st='.$error.'&em='.$usermail.'');
			} else {
				header('Location:http'.$secure.'://'.$_SERVER['HTTP_HOST'].'/emailconfirmation.php?st='.$error.'&em='.$usermail.'');
			}
		} else {//redirect to sales neuron page
			header('Location:http'.$secure.'://'.$_SERVER['HTTP_HOST']);
		}
	}
	//clear session during login
	public function signuplogout() {
		$this->session->unset_userdata('logged_in');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('cookieflag');
		$this->session->unset_userdata('userid');
		$this->session->unset_userdata('loghistory');
		$this->session->unset_userdata('UserDataname');
		$this->session->unset_userdata('BranchIds');
		$this->session->unset_userdata('UserRoleId');
		$this->session->unset_userdata('UserAccount');
		unset($this->session->userdata);
		$this->session->sess_destroy();
		//cookie helper
		$this->load->helper('cookie');
		if($_SERVER['HTTP_HOST']!='localhost') {
			//delete cookie
			$cookiedomain = $this->config->item('cookie_domain');
			$userdomain = $_SERVER['HTTP_HOST'];
			$cookieexptime = $this->config->item('sess_expiration');
			setcookie('sn_ci_session','',1,'/',$userdomain,0);
			setcookie('sn_ci_session','',1,'/',$cookiedomain,0);
		} else {
			//delete cookie
			$cookieexptime = $this->config->item('sess_expiration');
			setcookie('sn_ci_session','',1,'/','',0);
		}
		return 'yes';
	}
	//clear data sets
	public function dataclear() {
		$s = $this->signuplogout();
		sleep(1);
		echo $s;
	}
	/********************
		Login Process
	********************/
	//login verification
	public function verifylogin() {
		$username=isset($_POST['username'])?$_POST['username']:'';
		$password=isset($_POST['password'])?$_POST['password']:'';
		$ssologtype = (isset($_POST['logintype'])?$_POST['logintype']:'general');
		$type = (isset($_POST['ltype'])?$_POST['ltype']:'');
		$loginfo = $this->User->loginuserinfo($username);
		$userinfo = json_decode($loginfo,true);
		if($userinfo['status'] == 'success' || $userinfo['status'] == 'Expired') {
			//check and update plan
			$planid = '';
			$planmoduleids=''; 
			// Check Industry & InstallMode.
			if($userinfo['industry']=='VAR' && $userinfo['industry']=='Offline') {//Vardaan Only
				$dbname=$this->db->database;
			} else if($userinfo['industry']=='VAR' && $userinfo['industry']=='Online') {//SN Only
				$dbname=$userinfo['dbname'];
			} else if($userinfo['industry']=='SN') {//SN Only
				$dbname=$userinfo['dbname'];
			} else {//SN + VAR 
				$dbname=$userinfo['dbname'];
			}
			//fetch user plan info
			$planid = $this->loginuserplanidfetch(trim($username));
			sleep(1);
			//fetch user plan module ids
			if($planid!='') {
				$planmoduleids = $this->User->planinformationdatafetch($planid,$userinfo['industryid']);
				sleep(1);
			}
			if($planmoduleids!='') {
				//activate user plan
				$actlog='false';
				$actlog = $this->User->userplanactivationinlogin($planmoduleids,$dbname,trim($username),$userinfo['industryid']);
				sleep(1);
				if($actlog=='true') {
					$loginresult = $this->User->login($username,$password,$userinfo['domainname'],$dbname,$userinfo['status'],$userinfo['mascompanyid'],$ssologtype,$userinfo['signupstatus'],$planid,$userinfo['industryid']);
				}				
			}
		} else {
			$loginresult = json_encode(array('status'=>$userinfo['status'],'email'=>$username));
		}
		if(($_SERVER['HTTP_HOST']!='localhost') || ($_SERVER['HTTP_HOST']=='localhost' && $type=='site')) {
			$dataset = json_decode($loginresult,true);
			if($dataset['status']=='success') {
				if($dataset['signupstatus']=='No') {
					redirect(''.$dataset['domainname'].'Home/accountsettings','refresh');
				} else {
					if($userinfo['industryid'] == 3) {
						redirect(''.$dataset['domainname'].'/','refresh');
					} else{
						redirect(''.$dataset['domainname'].'/','refresh');
					}
				}
			} else if($dataset['status']=='Expired') {
				if($dataset['signupstatus']=='No') {
					redirect(''.$dataset['domainname'].'Home/accountsettings','refresh');
				} else {
					redirect(''.$dataset['domainname'].'Home/accountexpire','refresh');
				}
			} else {
				$secure = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "");
				$url = "http".$secure."://".$_SERVER['HTTP_HOST'];
				if($_SERVER['HTTP_HOST']=='localhost') {
					$url.= '/salesneuronsite/login.php?err='.$dataset['status'].'&em='.$dataset['email'].'';
				} else {
					$url.= '/login.php?err='.$dataset['status'].'&em='.$dataset['email'].'';
				}
				redirect("".$url."","refresh");
			} 
		} else {
			echo $loginresult;
		}
	}
	//fetch login user plan info
	public function loginuserplanidfetch($loginusername) {
		$this->User->inlinemasterdbenable();
		$userplanid = 1;
		$data=$this->db->query('select userplaninfo.userplaninfoid,userplaninfo.planid from userplaninfo join plan ON plan.planid=userplaninfo.planid where userplaninfo.registeremailid="'.$loginusername.'"');
		foreach($data->result() as $info) {
			$userplanid = $info->planid;
		}
		return $userplanid;
	}
	//user free plan activation
	public function accountexpire() {
		//fetch master company id
		$mascompanyid=1;
		$branchid = $this->Basefunctions->branchid;
		$mcdata = $this->db->select('company.companyid,company.mastercompanyid')->from('company')->join('branch','branch.companyid=company.companyid')->where('branch.branchid',$branchid)->get();
		foreach($mcdata->result() as $datas) {
			$mascompanyid = $datas->mastercompanyid;
		}
		//activate free plan
		if($mascompanyid!=1) {
			$planid=1;
			$masdbname = $this->db->masterdb;
			$userdb = $this->db->database;
			$masdb = $this->User->inlineuserdatabaseactive($masdbname);
			$actdate = date($this->Basefunctions->datef);
			$expdate=date($this->Basefunctions->datef, strtotime('+36 month',strtotime($actdate)));
			//free plan update
			$masdb->where('companyid',$mascompanyid);
			$masdb->update('userplaninfo',array('planid'=>$planid,'activationdate'=>$actdate,'expiredate'=>$expdate));
			//update plans in client database
			{
				//fetch user plan module ids
				$planmoduleids = $this->User->freeplaninformationdatafetch($planid,$masdbname);
				sleep(1);
				//active modules based new free plan
				$actlog='false';
				$actlog = $this->User->userplanactivationinaccexpire($planmoduleids);
				sleep(1);
				if($actlog=='true') {
					{//update account expire status
						//[master db]
						$masdb->where('companyid',$mascompanyid);
						$masdb->update('generalsetting',array('accountexpire'=>'No'));
						//[client db]
						$clientdb = $this->User->inlineuserdatabaseactive($userdb);
						$clientdb->where('companyid',$mascompanyid);
						$clientdb->update('generalsetting',array('accountexpire'=>'No'));
						//update session
						$this->session->set_userdata('UserAccount','active');
					}
					redirect('/Home/','refresh');
				} else {
					redirect('/Home/accountexpire', 'refresh');
				}
			}
		}
	}
	//user name verification
	public function verifyusername() {
		$forgetusername = $_POST['forgetusername'];
		$result = $this->User->checkusername($forgetusername);
		echo $result;
	}
	//question verification
	public function verifysecurityinfo() {
		$userid = $this->input->post('userid');
		$securityanswer = $this->input->post('securityanswer');
		$result = $this->User->checksecurityinfo($userid,$securityanswer);
		if($result == true) {
			echo json_encode(array('userid'=>$userid));
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}
	}
	//change new password
	public function changepassword() {
		$userid = $this->input->post('userid');
		$newpassword = $this->input->post('newpassword');
		$result = $this->User->newpassword($userid,$newpassword);
		$hname = (($_SERVER['HTTP_HOST']=='localhost')?$_SERVER['HTTP_HOST'].'/salesneuronsite':$_SERVER['HTTP_HOST']);
		$secure = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "");
		if($result['status'] == 'true') {
			/* send password updated confirmation mail */
			$clientdb = $this->User->inlineuserdatabaseactive($result['database']);
			$empdata = $clientdb->select('employeename')->from('employee')->where('emailid',$userid)->get();
			foreach($empdata->result() as $data) {
				$empname = $data->employeename;
			}
			$this->load->helper('phpmailer');
			$this->sendpasswordupdatemail($userid,$empname);
			redirect('http'.$secure.'://'.$hname.'/login.php','refresh');
		} else {
			redirect('http'.$secure.'://'.$hname.'/login.php','refresh');
		}
	}
	//check signup mail id
	public function uniqueemail() {
		$value=strtolower($_GET['signupemail']);
		$result=$this->User->uniqueemail($value);
		echo $result;
	}
	//Account settings update
	public function accountinfoupdate() {
		$this->User->accountinfoupdatemodel();
	}
	//check unique domainname
	public function uniquedomaincheck() {
		$domain=strtolower(trim($_GET['domainname']));
		$result=$this->User->uniquedomaincheckmodel($domain);
		echo $result;
	}
	//logout
	public function logout() {
		$loghistory=$this->session->userdata('loghistory');
		$logdata = array(
			'signouttime'=>date($this->Basefunctions->datef)
		);
		$this->db->where('loginhistoryid',$loghistory['loghistoryid']);
		$this->db->update('loginhistory',$logdata);
		sleep(1);
		$this->session->unset_userdata('logged_in');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('cookieflag');
		$this->session->unset_userdata('userid');
		$this->session->unset_userdata('loghistory');
		$this->session->unset_userdata('UserDataname');
		$this->session->unset_userdata('BranchIds');
		$this->session->unset_userdata('UserRoleId');
		$this->session->unset_userdata('UserAccount');
		unset($this->session->userdata);
		$this->session->sess_destroy();
		//cookie helper
		//$this->load->helper('cookie');
		if($_SERVER['HTTP_HOST']!='localhost') {
			$secure = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "");
			$hname = $_SERVER['HTTP_HOST'];
			$hnames = explode('.',$hname);
			$cnt = count($hnames);
			$host = $hnames[($cnt-2)].'.'.$hnames[($cnt-1)];
			//delete cookie
			$cookiedomain = $this->config->item('cookie_domain');
			$userdomain=$_SERVER['HTTP_HOST'];
			$cookieexptime = $this->config->item('sess_expiration');
			setcookie('sn_ci_session',addslashes(serialize(array())),1,'/',$userdomain,0);
			setcookie('sn_ci_session',addslashes(serialize(array())),1,'/',$cookiedomain,0);
			sleep(1);
			redirect('http'.$secure.'://'.$host.'/login.php');
		} else {
			//delete cookie
			$cookieexptime = $this->config->item('sess_expiration');
			setcookie('sn_ci_session',addslashes(serialize(array())),1,'/','',0);
			redirect(base_url().'Login/','location');
		}
	}
	//new signup //direct signup
	public function newsignup() {
		$data['date'] = '';
		$data['error']='';
		$data['emailid'] = (isset($_POST['signupuseremail'])?$_POST['signupuseremail']:'');
		$data['planid'] = (isset($_POST['signupplan'])?$_POST['signupplan']:'3');
		$data['zonedata'] = (isset($_POST['usertimezone'])?$_POST['usertimezone']:'UTC');
		date_default_timezone_set($data['zonedata']);
		$secure = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "");
		if($data['emailid']!='' && $data['planid']!='') {
			$this->signuplogout();
			sleep(1);
			$this->load->view('Login/signup',$data);
		} else { //redirect to sales neuron page
			$this->signuplogout();
			sleep(1);
			header('Location:http'.$secure.'://'.$_SERVER['HTTP_HOST'].'/');
		}
	}
	//old method - for referrence
	//Forgot password
	public function fpassword() {
		$secure = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "");
		$data['emailid'] = '';
		$tzone = $this->Basefunctions->employeetimezoneset($this->Basefunctions->userid);
		date_default_timezone_set($tzone['name']);
		$curdate = date("YmdHis");
		if( isset($_GET['SN']) ) {
			$datasets = $_GET['SN'];
			$datas = base64_decode($datasets);
			$decoddatasets =  strrev($datas);
			$userdatas = explode('a1o8eg4',$decoddatasets);
			//mail id
			$dataemail = $userdatas[0];
			$email =  explode('sns9s',$dataemail);
			$usermail = base64_decode($email[1]);
			$data['emailid'] = $usermail;
			//check email validity
			$this->load->view('Login/fpassword',$data);
		} else { //redirect to sales neuron page
			header('Location:http'.$secure.'://salesneuron.com/');
		}
	}
	//verification mail send
	public function sendverificationmail($email,$zone,$cname) {
		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->SMTPAuth   = true;
		$mail->SMTPSecure = "tls";
		$mail->SMTPDebug = 4;
		$mail->Username   = "AKIAJKKOMYZMUDJBMOAQ";
		$mail->Password   = "AuPZiKeRhl2OeloXEDnfDtYkkjrevNYN1QyYlCAoRZPX";
		$mail->Host       = "email-smtp.us-west-2.amazonaws.com";
		$mail->Port       = 587;
		$mail->SetFrom('arvind@aucventures.com','Sales Neuron');
		$mail->Subject   = "Sales Neuron: Sign up Confirmation";
		$expdate = date("YmdHis", strtotime('+7 days'));
		$datasets = "uesns9s".base64_encode($email)."a1o8eg4expsns9s".base64_encode($expdate)."a1o8eg4tzsns9s".base64_encode($zone);
		$datas = strrev($datasets);
		$urldatasets = base64_encode($datas);
		$secure = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "");
		$root = (($_SERVER['HTTP_HOST']!='localhost')?"http".$secure."://".$_SERVER['HTTP_HOST'] : "http".$secure."://".$_SERVER['HTTP_HOST'].'/salesneuronsite');
		//new
		$body='<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
<title>Destiny</title>';
		$body.="
<link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
<style type='text/css'>
div, p, a, li, td { -webkit-text-size-adjust:none; }
*{
-webkit-font-smoothing: antialiased;
-moz-osx-font-smoothing: grayscale;
}
.ReadMsgBody
{width: 100%; background-color: #f5f5f5;}
.ExternalClass
{width: 100%; background-color: #f5f5f5;}
body{width: 100%; height: 100%; background-color: #f5f5f5; margin:0; padding:0; -webkit-font-smoothing: antialiased;}
html{width: 100%; background-color: #f5f5f5;}
@font-face {font-family: 'proxima_novalight';src: url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_nova_rgregular'; src: url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_novasemibold';src: url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_nova_rgbold';src: url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_novathin';src: url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_novaextrabold';src: url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.woff2') format('woff2'),url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
p {padding: 0!important; margin-top: 0!important; margin-right: 0!important; margin-bottom: 0!important; margin-left: 0!important; }
.hover:hover {opacity:0.85;filter:alpha(opacity=85); }
.jump:hover {
	opacity:0.75;
	filter:alpha(opacity=75);
	padding-top: 10px!important;
}
a#rotator img {
-webkit-transition: all 1s ease-in-out;
-moz-transition: all 1s ease-in-out; 
-o-transition: all 1s ease-in-out; 
-ms-transition: all 1s ease-in-out; 
}
a#rotator img:hover { 
-webkit-transform: rotate(360deg); 
-moz-transform: rotate(360deg); 
-o-transform: rotate(360deg);
-ms-transform: rotate(360deg); 
}
@-webkit-keyframes spaceboots {
	0% { -webkit-transform: translate(2px, 1px) rotate(0deg); }
	10% { -webkit-transform: translate(-1px, -2px) rotate(-1deg); }
	20% { -webkit-transform: translate(-3px, 0px) rotate(1deg); }
	30% { -webkit-transform: translate(0px, 2px) rotate(0deg); }
	40% { -webkit-transform: translate(1px, -1px) rotate(1deg); }
	50% { -webkit-transform: translate(-1px, 2px) rotate(-1deg); }
	60% { -webkit-transform: translate(-3px, 1px) rotate(0deg); }
	70% { -webkit-transform: translate(2px, 1px) rotate(-1deg); }
	80% { -webkit-transform: translate(-1px, -1px) rotate(1deg); }
	90% { -webkit-transform: translate(2px, 2px) rotate(0deg); }
	100% { -webkit-transform: translate(1px, -2px) rotate(-1deg); }
}
#shake:hover,
#shake:focus {
	-webkit-animation-name: spaceboots;
	-webkit-animation-duration: 0.8s;
	-webkit-transform-origin:50% 50%;
	-webkit-animation-iteration-count: infinite;
	-webkit-animation-timing-function: linear;
}
.image600 img {width: 600px; height: auto;}
.image595 img {width: 595px; height: auto;}
.icon27 img {width: 27px; height: auto;}
.icon24 img {width: 24px; height: auto;}
.image254 img {width: 254px; height: auto;}
.image176 img {width: 176px; height: auto;}
.image260 img {width: 260px; height: auto;}
.avatar96 img {width: 96px; height: auto;}
.icons46 img {width: 46px; height: auto;}
.image248 img {width: 246px; height: auto;}
#logo img {width: 120px; height: auto;}
#icon18 img {width: 18px; height: auto;}
</style>
<!-- @media only screen and (max-width: 640px) 
{*/
-->
<style type='text/css'> @media only screen and (max-width: 640px){
		body{width:auto!important;}
		table[class=full] {width: 100%!important; clear: both; }
		table[class=mobile] {width: 100%!important; padding-left: 30px; padding-right: 30px; clear: both; }
		table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
		td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
		*[class=erase] {display: none;}
		*[class=buttonScale] {float: none!important; text-align: center!important; display: inline-block!important; clear: both;}
		.h60 {height: 60px!important;}
		.h30 {height: 30px!important;}
		.h10 {height: 10px!important;}
		.h15 {height: 15px!important;}
		td[class=image600] img {width: 100%!important; text-align: center!important; clear: both; }
		td[class=image595] img {width: 100%!important; text-align: center!important; clear: both; }
		table[class=sponsor] {text-align:center; float:none; width:70%!important;}
} </style>
<!--
@media only screen and (max-width: 479px) 
{
-->
<style type='text/css'>
@media only screen and (max-width: 479px){
	body{width:auto!important;}
	table[class=full] {width: 100%!important; clear: both; }
	table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
	table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
	td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
	*[class=erase] {display: none;}
	*[class=buttonScale] {float: none!important; text-align: center!important; display: inline-block!important; clear: both;}
	.h60 {height: 60px!important;}
	.h30 {height: 30px!important;}
	.h10 {height: 10px!important;}
	.h15 {height: 15px!important;}
	td[class=image600] img {width: 100%!important; text-align: center!important; clear: both; }
	td[class=image595] img {width: 100%!important; text-align: center!important; clear: both; }
	table[class=sponsor] {text-align:center; float:none; width:100%!important;}
	td[class=font10] {font-size: 10px!important;}
	span[class=font38] {font-size: 38px!important; line-height: 42px!important;}
}
}</style>
</head>";
		$body.='
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix">
<div class="ui-sortable" id="sort_them">
<!-- Seperator 2 + Social -->
<table class="full" align="center" bgcolor="#009688" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td  style="padding:20px;" id="sep2"align="center">
			<!-- Wrapper -->
			<table class="mobile" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td align="center">
						<!-- Wrapper -->
						<table class="full" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
							<tr>
								<td align="center" width="100%">
									<!-- SORTABLE -->
									<div class="sortable_inner ui-sortable">
									<!-- Space -->
									<table class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="40" width="100%"></td>
										</tr>
									</table><!-- End Space -->
									<!-- Text -->
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td style="font-size: 26px; color: rgb(255, 255, 255); text-align: center; font-family: Helvetica,Arial,sans-serif; line-height: 32px; font-weight: bold; vertical-align: top;" class="fullCenter" align="center" width="100%">
												<!--[if !mso]><!--><span style="font-family: '; $body.="'Titillium Web'"; $body.=', sans-serif; font-weight:normal;"><!--<![endif]--><p cu-identify="element_06079989344851364" >Sign Up Confirmation<br><!--[if !mso]><!--></span><!--<![endif]--><p>
											</td>
										</tr>
										<tr>
											<td height="05" width="100%"></td>
										</tr>
										<!-- Space -->
										<table class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
											<tr>
												<td height="10" width="100%"></td>
											</tr>
										</table><!-- End Space -->
										<tr>
											<td style="font-size: 26px; color: rgb(255, 255, 255); text-align: center; font-family: Helvetica,Arial,sans-serif; line-height: 32px; font-weight: bold; vertical-align: top;" class="fullCenter" align="center" width="100%">
												<!--[if !mso]><!--><span style="font-family:'; $body.="'proxima_novasemibold'";$body.=', Helvetica; font-weight:normal;"><!--<![endif]--><p cu-identify="element_06079989344851364" >
												<img width="170" height="100" style="padding-right:10px;padding-bottom:10px;" src="'.$root.'/images/homepagelogo.png"><br><!--[if !mso]><!--></span><!--<![endif]--><p>
											</td>
										</tr>
										<tr>
											<td height="05" width="100%"></td>
										</tr>
										<tr>
											<td style="font-size: 26px; color: rgb(255, 255, 255); text-align: center; font-family: Helvetica,Arial,sans-serif; line-height: 32px; font-weight: bold; vertical-align: top;" class="fullCenter" align="center" width="100%">
												<!--[if !mso]><!--><span style="font-family:'; $body.="'Titillium Web'";$body.=', sans-serif; font-weight:normal;"><!--<![endif]--><p cu-identify="element_06079989344851364" >Hi '.$cname.',<br><!--[if !mso]><!--></span><!--<![endif]--><p>
											</td>
										</tr>
									</table>
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="15" width="100%">
											</td>
										</tr>
									</table>
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td style="font-size: 14px; color: rgb(255, 255, 255); text-align: center; font-family: Helvetica,Arial,sans-serif; line-height: 22px; font-weight: bold; vertical-align: top;" class="fullCenter" align="center" width="100%">												
												<!--[if !mso]><!--><span style="font-family:'; $body.="'proxima_nova_rgregular'"; $body.=', Helvetica; font-weight:normal;"><!--<![endif]--><p cu-identify="element_05107549716118674" ><div style="text-align: center;"><font size="3"><span style="font-family: Verdana;">Almost There! Please click the below button to activate your account <br>and experience the benefits of sales neuron</span><br><span style="font-family: Verdana;"></span></font></div><!--[if !mso]><!--></span><!--<![endif]--><p>
											</td>
										</tr>
									</table>									
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="15" width="100%">
											</td>
										</tr>
									</table>									
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<!----------------- Button Center ----------------->
										<tr>
											<td class="buttonScale" align="center" width="auto">
												<table class="buttonScale"style="border-radius: 25px; padding-left: 35px; padding-right: 35px; background-color: rgb(0, 150, 136);" id="shake" align="center" bgcolor="#009688" border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td style="font-weight: bold; font-family: Helvetica,Arial,sans-serif; color: rgb(255, 255, 255); background-color: rgb(0, 150, 136);" align="center" bgcolor="#ffffff" height="52" width="auto">
															<!--[if !mso]><!--><span style=" border: 1px solid #ffffff;border-radius: 8px; font-family:'; $body.="'proxima_nova_rgbold'";$body.=',Helvetica;font-weight: normal;padding: 8px; background-color:#ffffff;"><!--<![endif]-->
																<a cu-identify="element_0753161873513802" href="'.$root.'/crm/Login/confirmation?SN='.$urldatasets.'" style="color: #009688; font-size: 18px; text-decoration: none; line-height: 34px; width: 100%;">Activate NOW</a>
															<!--[if !mso]><!--></span><!--<![endif]-->
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<!----------------- End Button Center ----------------->
									</table>
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="80" width="100%">
											</td>
										</tr>
									</table>									
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td style="font-size: 14px; color: rgb(255, 255, 255); text-align: center; font-family: Helvetica,Arial,sans-serif; line-height: 22px; font-weight: bold; vertical-align: top;" class="fullCenter" align="center" width="100%">
												<p cu-identify="element_03207356464405948" >Did not Sign Up? Please ignore this mail.<br>
											</td>
										</tr>
									</table>									
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="0" width="100%">
											</td>
										</tr>
									</table>									
									<!-- Social Icons -->
									<div style="display: none" id="element_09872123869190068"></div>
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="0" width="100%">
											</td>
										</tr>
									</table>
									</div>
									<!-- END SORTABLE -->
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table><!-- End Wrapper --> 
		</div>
		</td>
	</tr>
</table>
<!-- End Wrapper -->
<!-- Wrapper 3 -->
<div style="display: none" id="element_03663119396258956"></div>
<!-- End Wrapper 3 -->
<!-- Wrapper 28 -->
<div style="display: none" id="element_08147460135100794"></div><!-- Wrapper 28 -->
<!-- Wrapper 28 -->
<div style="display: none;" id="element_046737467804971156"></div><!-- Wrapper 28 -->
<!-- Wrapper 3 -->
<div style="display: none" id="element_010546669432505962"></div>
<!-- End Wrapper 3 -->
<!-- Wrapper Footer -->
<table style="background-color:#e1e1e1;" class="full" align="center" bgcolor="#f1f4f6" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td align="center" valign="top" width="100%">
			<!-- Wrapper -->
			<table class="mobile" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td align="center">
						<!-- Space -->
						<table class="full" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td height="20" width="100%"></td>
							</tr>
						</table><!-- End Space -->
						<!-- Wrapper -->
						<table class="full" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
							<tr>
								<td align="center" width="100%">
									<!-- Icon 1 -->
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: center;" class="fullCenter" align="left" border="0" cellpadding="0" cellspacing="0" width="400">
										<tr>
											<td style="text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 13px; color: #aaacaf; line-height: 22px;" class="fullCenter" valign="middle" width="100%">
												<span style="font-family:'; $body.="'proxima_nova_rgregular'";$body.=', Helvetica; font-weight: normal;">
													<span style="color: #787b80;">© 2015 </span>,<a href="http://www.aucventures.com" style="text-decoration: none; color: #787b80;">AUC Ventures Pvt Ltd</a> , All Rights Reserved
													<span style="font-family:'; $body.="'proxima_nova_rgregular'";$body.=', Helvetica;"></span>
												</span>
											</td>
										</tr>
									</table>
									<!-- Space -->
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" align="left" border="0" cellpadding="0" cellspacing="0" width="1">
										<tr>
											<td height="12" width="100%"></td>
										</tr>
									</table>
									<!-- Social Icons Footer -->
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: right;" class="fullCenter" align="right" border="0" cellpadding="0" cellspacing="0" width="120">
										<tr>
											<td id="icon18" width="100%">
												&nbsp;
												<a href="http://facebook.com/salesneuron" style="text-decoration: none;" >
													<img src="http://rocketway.net/themebuilder/products/templates/destiny/images/footer_icon2.png" style="width: 18px; height:auto;" alt=""  class="hover" border="0" height="auto" width="18">
												</a>
												&nbsp;
												<a href="https://twitter.com/salesneuron" style="text-decoration: none;" >
													<img src="http://rocketway.net/themebuilder/products/templates/destiny/images/footer_icon3.png" style="width: 18px; height:auto;" alt=""  class="hover" border="0" height="auto" width="18">
												</a>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table><!-- End Wrapper -->
						<!-- Space -->
						<table class="full" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td height="20" width="100%"></td>
							</tr>
						</table><!-- End Space -->
					</td>
				</tr>
			</table><!-- End Wrapper -->
		</div>
		</td>
	</tr>
</table><!-- Wrapper Footer -->
</div>
</body></html>	<style>body{ background: none !important; } </style>';
		$newbody = $body;
		$mail->IsHTML(true);
		$mail->MsgHTML ($newbody);
		$mail->AddAddress(trim($email));
		$mail->AddBCC('arvind@aucventures.com');
		$mail->AltBody="This is text only alternative body.";
		if($mail->send()) {
			return 'success';
		} else {
			return 'fail';
		}
	}
	//password updated notification mail
	public function sendpasswordupdatemail($email,$empname) {
		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->SMTPAuth   = true;
		$mail->SMTPSecure = "tls";
		$mail->SMTPDebug = 4;
		$mail->Username   = "AKIAJKKOMYZMUDJBMOAQ";
		$mail->Password   = "AuPZiKeRhl2OeloXEDnfDtYkkjrevNYN1QyYlCAoRZPX";
		$mail->Host       = "email-smtp.us-west-2.amazonaws.com";
		$mail->Port       = 587;
		$mail->SetFrom('arvind@aucventures.com','Sales Neuron');
		$mail->Subject   = "Sales Neuron:Password Update Notification";
		//path
		$secure = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "");
		$root = ( ($_SERVER['HTTP_HOST']!='localhost')?"http".$secure."://".$_SERVER['HTTP_HOST'] : "http".$secure."://".$_SERVER['HTTP_HOST'].'/salesneuronsite' );
		//new
		$body='<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
<title>Destiny</title>';
		$body.="
<link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
<style type='text/css'>
div, p, a, li, td { -webkit-text-size-adjust:none; }
*{
-webkit-font-smoothing: antialiased;
-moz-osx-font-smoothing: grayscale;
}
.ReadMsgBody
{width: 100%; background-color: #f5f5f5;}
.ExternalClass
{width: 100%; background-color: #f5f5f5;}
body{width: 100%; height: 100%; background-color: #f5f5f5; margin:0; padding:0; -webkit-font-smoothing: antialiased;}
html{width: 100%; background-color: #f5f5f5;}
@font-face {font-family: 'proxima_novalight';src: url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_nova_rgregular'; src: url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_novasemibold';src: url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_nova_rgbold';src: url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_novathin';src: url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_novaextrabold';src: url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.woff2') format('woff2'),url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
p {padding: 0!important; margin-top: 0!important; margin-right: 0!important; margin-bottom: 0!important; margin-left: 0!important; }
.hover:hover {opacity:0.85;filter:alpha(opacity=85); }
.jump:hover {
	opacity:0.75;
	filter:alpha(opacity=75);
	padding-top: 10px!important;
}
a#rotator img {
-webkit-transition: all 1s ease-in-out;
-moz-transition: all 1s ease-in-out; 
-o-transition: all 1s ease-in-out; 
-ms-transition: all 1s ease-in-out; 
}
a#rotator img:hover { 
-webkit-transform: rotate(360deg); 
-moz-transform: rotate(360deg); 
-o-transform: rotate(360deg);
-ms-transform: rotate(360deg); 
}
@-webkit-keyframes spaceboots {
	0% { -webkit-transform: translate(2px, 1px) rotate(0deg); }
	10% { -webkit-transform: translate(-1px, -2px) rotate(-1deg); }
	20% { -webkit-transform: translate(-3px, 0px) rotate(1deg); }
	30% { -webkit-transform: translate(0px, 2px) rotate(0deg); }
	40% { -webkit-transform: translate(1px, -1px) rotate(1deg); }
	50% { -webkit-transform: translate(-1px, 2px) rotate(-1deg); }
	60% { -webkit-transform: translate(-3px, 1px) rotate(0deg); }
	70% { -webkit-transform: translate(2px, 1px) rotate(-1deg); }
	80% { -webkit-transform: translate(-1px, -1px) rotate(1deg); }
	90% { -webkit-transform: translate(2px, 2px) rotate(0deg); }
	100% { -webkit-transform: translate(1px, -2px) rotate(-1deg); }
}
#shake:hover,
#shake:focus {
	-webkit-animation-name: spaceboots;
	-webkit-animation-duration: 0.8s;
	-webkit-transform-origin:50% 50%;
	-webkit-animation-iteration-count: infinite;
	-webkit-animation-timing-function: linear;
}
.image600 img {width: 600px; height: auto;}
.image595 img {width: 595px; height: auto;}
.icon27 img {width: 27px; height: auto;}
.icon24 img {width: 24px; height: auto;}
.image254 img {width: 254px; height: auto;}
.image176 img {width: 176px; height: auto;}
.image260 img {width: 260px; height: auto;}
.avatar96 img {width: 96px; height: auto;}
.icons46 img {width: 46px; height: auto;}
.image248 img {width: 246px; height: auto;}
#logo img {width: 120px; height: auto;}
#icon18 img {width: 18px; height: auto;}
</style>
<!-- @media only screen and (max-width: 640px) 
{*/
-->
<style type='text/css'> @media only screen and (max-width: 640px){
		body{width:auto!important;}
		table[class=full] {width: 100%!important; clear: both; }
		table[class=mobile] {width: 100%!important; padding-left: 30px; padding-right: 30px; clear: both; }
		table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
		td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
		*[class=erase] {display: none;}
		*[class=buttonScale] {float: none!important; text-align: center!important; display: inline-block!important; clear: both;}
		.h60 {height: 60px!important;}
		.h30 {height: 30px!important;}
		.h10 {height: 10px!important;}
		.h15 {height: 15px!important;}
		td[class=image600] img {width: 100%!important; text-align: center!important; clear: both; }
		td[class=image595] img {width: 100%!important; text-align: center!important; clear: both; }
		table[class=sponsor] {text-align:center; float:none; width:70%!important;}
} </style>
<!--
@media only screen and (max-width: 479px) 
{
-->
<style type='text/css'>
@media only screen and (max-width: 479px){
	body{width:auto!important;}
	table[class=full] {width: 100%!important; clear: both; }
	table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
	table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
	td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
	*[class=erase] {display: none;}
	*[class=buttonScale] {float: none!important; text-align: center!important; display: inline-block!important; clear: both;}
	.h60 {height: 60px!important;}
	.h30 {height: 30px!important;}
	.h10 {height: 10px!important;}
	.h15 {height: 15px!important;}
	td[class=image600] img {width: 100%!important; text-align: center!important; clear: both; }
	td[class=image595] img {width: 100%!important; text-align: center!important; clear: both; }
	table[class=sponsor] {text-align:center; float:none; width:100%!important;}
	td[class=font10] {font-size: 10px!important;}
	span[class=font38] {font-size: 38px!important; line-height: 42px!important;}
}
}</style>
</head>";
		$body.='
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix">
<div class="ui-sortable" id="sort_them">
<!-- Seperator 2 + Social -->
<table class="full" align="center" bgcolor="#009688" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td  style="padding:20px;" id="sep2"align="center">
			<!-- Wrapper -->
			<table class="mobile" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td align="center">
						<!-- Wrapper -->
						<table class="full" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
							<tr>
								<td align="center" width="100%">
									<!-- SORTABLE -->
									<div class="sortable_inner ui-sortable">
									<!-- Space -->
									<table class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="40" width="100%"></td>
										</tr>
									</table><!-- End Space -->
									<!-- Text -->
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td style="font-size: 26px; color: rgb(255, 255, 255); text-align: center; font-family: Helvetica,Arial,sans-serif; line-height: 32px; font-weight: bold; vertical-align: top;" class="fullCenter" align="center" width="100%">
												<!--[if !mso]><!--><span style="font-family: '; $body.="'Titillium Web'"; $body.=', sans-serif; font-weight:normal;"><!--<![endif]--><p cu-identify="element_06079989344851364" >Password Changed Notification<br><!--[if !mso]><!--></span><!--<![endif]--><p>
											</td>
										</tr>
										<!-- Space -->
										<table class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
											<tr>
												<td height="10" width="100%"></td>
											</tr>
										</table><!-- End Space -->
										<tr>
											<td style="font-size: 26px; color: rgb(255, 255, 255); text-align: center; font-family: Helvetica,Arial,sans-serif; line-height: 32px; font-weight: bold; vertical-align: top;" class="fullCenter" align="center" width="100%">
												<!--[if !mso]><!--><span style="font-family:'; $body.="'proxima_novasemibold'";$body.=', Helvetica; font-weight:normal;"><!--<![endif]--><p cu-identify="element_06079989344851364" >
												<img width="170" height="100" style="padding-right:10px;padding-bottom:10px;" src="'.$root.'/images/homepagelogo.png"><br><!--[if !mso]><!--></span><!--<![endif]--><p>
											</td>
										</tr>
										<tr>
											<td height="05" width="100%"></td>
										</tr>
										<tr>
											<td style="font-size: 26px; color: rgb(255, 255, 255); text-align: center; font-family: Helvetica,Arial,sans-serif; line-height: 32px; font-weight: bold; vertical-align: top;" class="fullCenter" align="center" width="100%">
												<!--[if !mso]><!--><span style="font-family:'; $body.="'Titillium Web'";$body.=', sans-serif; font-weight:normal;"><!--<![endif]--><p cu-identify="element_06079989344851364" >Hi '.$empname.',<br><!--[if !mso]><!--></span><!--<![endif]--><p>
											</td>
										</tr>
									</table>
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="15" width="100%">
											</td>
										</tr>
									</table>
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td style="font-size: 14px; color: rgb(255, 255, 255); text-align: center; font-family: Helvetica,Arial,sans-serif; line-height: 22px; font-weight: bold; vertical-align: top;" class="fullCenter" align="center" width="100%">												
												<!--[if !mso]><!--><span style="font-family:'; $body.="'proxima_nova_rgregular'"; $body.=', Helvetica; font-weight:normal;"><!--<![endif]--><p cu-identify="element_05107549716118674" ><div style="text-align: center;"><font size="3"><span style="font-family: Verdana;">Congratulations! Your password has been successfully changed.Login to your account and start working.</span><br><span style="font-family: Verdana;"></span></font></div><!--[if !mso]><!--></span><!--<![endif]--><p>
											</td>
										</tr>
									</table>									
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="15" width="100%">
											</td>
										</tr>
									</table>									
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<!----------------- Button Center ----------------->
										<tr>
											<td class="buttonScale" align="center" width="auto">
												<table class="buttonScale"style="border-radius: 25px; padding-left: 35px; padding-right: 35px; background-color: rgb(0, 150, 136);" id="shake" align="center" bgcolor="#009688" border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td style="font-weight: bold; font-family: Helvetica,Arial,sans-serif; color: rgb(255, 255, 255); background-color: rgb(0, 150, 136);" align="center" bgcolor="#ffffff" height="52" width="auto">
															<!--[if !mso]><!--><span style="border-radius: 8px; font-family:'; $body.="'proxima_nova_rgbold'";$body.=',Helvetica;font-weight: normal;padding: 8px;"><!--<![endif]-->
																<a cu-identify="element_0753161873513802" href="#" style="color: #009688; font-size: 18px; text-decoration: none; line-height: 34px; width: 100%;"></a>
															<!--[if !mso]><!--></span><!--<![endif]-->
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<!----------------- End Button Center ----------------->
									</table>
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="0" width="100%">
											</td>
										</tr>
									</table>									
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="0" width="100%">
											</td>
										</tr>
									</table>
									<!-- Social Icons -->
									<div style="display: none" id="element_09872123869190068"></div>
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="0" width="100%">
											</td>
										</tr>
									</table>
									</div>
									<!-- END SORTABLE -->
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table><!-- End Wrapper --> 
		</div>
		</td>
	</tr>
</table>
<!-- End Wrapper -->
<!-- Wrapper 3 -->
<div style="display: none" id="element_03663119396258956"></div>
<!-- End Wrapper 3 -->
<!-- Wrapper 28 -->
<div style="display: none" id="element_08147460135100794"></div><!-- Wrapper 28 -->
<!-- Wrapper 28 -->
<div style="display: none;" id="element_046737467804971156"></div><!-- Wrapper 28 -->
<!-- Wrapper 3 -->
<div style="display: none" id="element_010546669432505962"></div>
<!-- End Wrapper 3 -->
<!-- Wrapper Footer -->
<table style="background-color:#e1e1e1;" class="full" align="center" bgcolor="#f1f4f6" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td align="center" valign="top" width="100%">
			<!-- Wrapper -->
			<table class="mobile" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td align="center">
						<!-- Space -->
						<table class="full" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td height="20" width="100%"></td>
							</tr>
						</table><!-- End Space -->
						<!-- Wrapper -->
						<table class="full" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
							<tr>
								<td align="center" width="100%">
									<!-- Icon 1 -->
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: center;" class="fullCenter" align="left" border="0" cellpadding="0" cellspacing="0" width="400">
										<tr>
											<td style="text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 13px; color: #aaacaf; line-height: 22px;" class="fullCenter" valign="middle" width="100%">
												<span style="font-family:'; $body.="'proxima_nova_rgregular'";$body.=', Helvetica; font-weight: normal;">
													<span style="color: #787b80;">© 2015 </span>,<a href="http://www.aucventures.com" style="text-decoration: none; color: #787b80;">AUC Ventures Pvt Ltd</a> , All Rights Reserved
													<span style="font-family:'; $body.="'proxima_nova_rgregular'";$body.=', Helvetica;"></span>
												</span>
											</td>
										</tr>
									</table>
									<!-- Space -->
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" align="left" border="0" cellpadding="0" cellspacing="0" width="1">
										<tr>
											<td height="12" width="100%"></td>
										</tr>
									</table>
									<!-- Social Icons Footer -->
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: right;" class="fullCenter" align="right" border="0" cellpadding="0" cellspacing="0" width="120">
										<tr>
											<td id="icon18" width="100%">
												&nbsp;
												<a href="http://facebook.com/salesneuron" style="text-decoration: none;" >
													<img src="http://rocketway.net/themebuilder/products/templates/destiny/images/footer_icon2.png" style="width: 18px; height:auto;" alt=""  class="hover" border="0" height="auto" width="18">
												</a>
												&nbsp;
												<a href="https://twitter.com/salesneuron" style="text-decoration: none;" >
													<img src="http://rocketway.net/themebuilder/products/templates/destiny/images/footer_icon3.png" style="width: 18px; height:auto;" alt=""  class="hover" border="0" height="auto" width="18">
												</a>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table><!-- End Wrapper -->
						<!-- Space -->
						<table class="full" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td height="20" width="100%"></td>
							</tr>
						</table><!-- End Space -->
					</td>
				</tr>
			</table><!-- End Wrapper -->
		</div>
		</td>
	</tr>
</table><!-- Wrapper Footer -->
</div>
</body></html>	<style>body{ background: none !important; } </style>';
		$newbody = $body;
		$mail->IsHTML(true);
		$mail->MsgHTML ($newbody);
		$mail->AddAddress(trim($email));
		$mail->AddBCC('arvind@aucventures.com');
		$mail->AltBody="This is text only alternative body.";
		if($mail->send()) {
			return 'success';
		} else {
			return 'fail';
		}
	}
	//send welcome mail
	public function sendwelcomemail($email,$empname) {
		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->SMTPAuth   = true;
		$mail->SMTPSecure = "tls";
		$mail->SMTPDebug = 4;
		$mail->Username   = "AKIAJKKOMYZMUDJBMOAQ";
		$mail->Password   = "AuPZiKeRhl2OeloXEDnfDtYkkjrevNYN1QyYlCAoRZPX";
		$mail->Host       = "email-smtp.us-west-2.amazonaws.com";
		$mail->Port       = 587;
		$mail->SetFrom('arvind@aucventures.com','Sales Neuron');
		$mail->Subject   = "Sales Neuron:Welcome Message";
		//path
		$secure = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "");
		$root = ( ($_SERVER['HTTP_HOST']!='localhost')?"http".$secure."://".$_SERVER['HTTP_HOST'] : "http".$secure."://".$_SERVER['HTTP_HOST'].'/salesneuronsite' );
		//new
		$body='<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
<title>Destiny</title>';
		$body.="
<link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
<style type='text/css'>
div, p, a, li, td { -webkit-text-size-adjust:none; }
*{
-webkit-font-smoothing: antialiased;
-moz-osx-font-smoothing: grayscale;
}
.ReadMsgBody
{width: 100%; background-color: #f5f5f5;}
.ExternalClass
{width: 100%; background-color: #f5f5f5;}
body{width: 100%; height: 100%; background-color: #f5f5f5; margin:0; padding:0; -webkit-font-smoothing: antialiased;}
html{width: 100%; background-color: #f5f5f5;}
@font-face {font-family: 'proxima_novalight';src: url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_nova_rgregular'; src: url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_novasemibold';src: url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_nova_rgbold';src: url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_novathin';src: url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_novaextrabold';src: url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.woff2') format('woff2'),url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
p {padding: 0!important; margin-top: 0!important; margin-right: 0!important; margin-bottom: 0!important; margin-left: 0!important; }
.hover:hover {opacity:0.85;filter:alpha(opacity=85); }
.jump:hover {
	opacity:0.75;
	filter:alpha(opacity=75);
	padding-top: 10px!important;
}
a#rotator img {
-webkit-transition: all 1s ease-in-out;
-moz-transition: all 1s ease-in-out; 
-o-transition: all 1s ease-in-out; 
-ms-transition: all 1s ease-in-out; 
}
a#rotator img:hover { 
-webkit-transform: rotate(360deg); 
-moz-transform: rotate(360deg); 
-o-transform: rotate(360deg);
-ms-transform: rotate(360deg); 
}
@-webkit-keyframes spaceboots {
	0% { -webkit-transform: translate(2px, 1px) rotate(0deg); }
	10% { -webkit-transform: translate(-1px, -2px) rotate(-1deg); }
	20% { -webkit-transform: translate(-3px, 0px) rotate(1deg); }
	30% { -webkit-transform: translate(0px, 2px) rotate(0deg); }
	40% { -webkit-transform: translate(1px, -1px) rotate(1deg); }
	50% { -webkit-transform: translate(-1px, 2px) rotate(-1deg); }
	60% { -webkit-transform: translate(-3px, 1px) rotate(0deg); }
	70% { -webkit-transform: translate(2px, 1px) rotate(-1deg); }
	80% { -webkit-transform: translate(-1px, -1px) rotate(1deg); }
	90% { -webkit-transform: translate(2px, 2px) rotate(0deg); }
	100% { -webkit-transform: translate(1px, -2px) rotate(-1deg); }
}
#shake:hover,
#shake:focus {
	-webkit-animation-name: spaceboots;
	-webkit-animation-duration: 0.8s;
	-webkit-transform-origin:50% 50%;
	-webkit-animation-iteration-count: infinite;
	-webkit-animation-timing-function: linear;
}
.image600 img {width: 600px; height: auto;}
.image595 img {width: 595px; height: auto;}
.icon27 img {width: 27px; height: auto;}
.icon24 img {width: 24px; height: auto;}
.image254 img {width: 254px; height: auto;}
.image176 img {width: 176px; height: auto;}
.image260 img {width: 260px; height: auto;}
.avatar96 img {width: 96px; height: auto;}
.icons46 img {width: 46px; height: auto;}
.image248 img {width: 246px; height: auto;}
#logo img {width: 120px; height: auto;}
#icon18 img {width: 18px; height: auto;}
</style>
<!-- @media only screen and (max-width: 640px) 
{*/
-->
<style type='text/css'> @media only screen and (max-width: 640px){
		body{width:auto!important;}
		table[class=full] {width: 100%!important; clear: both; }
		table[class=mobile] {width: 100%!important; padding-left: 30px; padding-right: 30px; clear: both; }
		table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
		td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
		*[class=erase] {display: none;}
		*[class=buttonScale] {float: none!important; text-align: center!important; display: inline-block!important; clear: both;}
		.h60 {height: 60px!important;}
		.h30 {height: 30px!important;}
		.h10 {height: 10px!important;}
		.h15 {height: 15px!important;}
		td[class=image600] img {width: 100%!important; text-align: center!important; clear: both; }
		td[class=image595] img {width: 100%!important; text-align: center!important; clear: both; }
		table[class=sponsor] {text-align:center; float:none; width:70%!important;}
} </style>
<!--
@media only screen and (max-width: 479px) 
{
-->
<style type='text/css'>
@media only screen and (max-width: 479px){
	body{width:auto!important;}
	table[class=full] {width: 100%!important; clear: both; }
	table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
	table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
	td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
	*[class=erase] {display: none;}
	*[class=buttonScale] {float: none!important; text-align: center!important; display: inline-block!important; clear: both;}
	.h60 {height: 60px!important;}
	.h30 {height: 30px!important;}
	.h10 {height: 10px!important;}
	.h15 {height: 15px!important;}
	td[class=image600] img {width: 100%!important; text-align: center!important; clear: both; }
	td[class=image595] img {width: 100%!important; text-align: center!important; clear: both; }
	table[class=sponsor] {text-align:center; float:none; width:100%!important;}
	td[class=font10] {font-size: 10px!important;}
	span[class=font38] {font-size: 38px!important; line-height: 42px!important;}
}
}</style>
</head>";
		$body.='
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix">
<div class="ui-sortable" id="sort_them">
<!-- Seperator 2 + Social -->
<table class="full" align="center" bgcolor="#009688" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td  style="padding:20px;" id="sep2"align="center">
			<!-- Wrapper -->
			<table class="mobile" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td align="center">
						<!-- Wrapper -->
						<table class="full" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
							<tr>
								<td align="center" width="100%">
									<!-- SORTABLE -->
									<div class="sortable_inner ui-sortable">
									<!-- Space -->
									<table class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="40" width="100%"></td>
										</tr>
									</table><!-- End Space -->
									<!-- Text -->
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td style="font-size: 26px; color: rgb(255, 255, 255); text-align: center; font-family: Helvetica,Arial,sans-serif; line-height: 32px; font-weight: bold; vertical-align: top;" class="fullCenter" align="center" width="100%">
												<!--[if !mso]><!--><span style="font-family: '; $body.="'Titillium Web'"; $body.=', sans-serif; font-weight:normal;"><!--<![endif]--><p cu-identify="element_06079989344851364">Welcome<br><!--[if !mso]><!--></span><!--<![endif]--><p>
											</td>
										</tr>
										<!-- Space -->
										<table class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
											<tr>
												<td height="10" width="100%"></td>
											</tr>
										</table><!-- End Space -->
										<tr>
											<td style="font-size: 26px; color: rgb(255, 255, 255); text-align: center; font-family: Helvetica,Arial,sans-serif; line-height: 32px; font-weight: bold; vertical-align: top;" class="fullCenter" align="center" width="100%">
												<!--[if !mso]><!--><span style="font-family:'; $body.="'proxima_novasemibold'";$body.=', Helvetica; font-weight:normal;"><!--<![endif]--><p cu-identify="element_06079989344851364" >
												<img width="170" height="100" style="padding-right:10px;padding-bottom:10px;" src="'.$root.'/images/homepagelogo.png"><br><!--[if !mso]><!--></span><!--<![endif]--><p>
											</td>
										</tr>
										<tr>
											<td height="05" width="100%"></td>
										</tr>
										<tr>
											<td style="font-size: 26px; color: rgb(255, 255, 255); text-align: center; font-family: Helvetica,Arial,sans-serif; line-height: 32px; font-weight: bold; vertical-align: top;" class="fullCenter" align="center" width="100%">
												<!--[if !mso]><!--><span style="font-family:'; $body.="'Titillium Web'";$body.=', sans-serif; font-weight:normal;"><!--<![endif]--><p cu-identify="element_06079989344851364" >Hi '.$empname.',<br><!--[if !mso]><!--></span><!--<![endif]--><p>
											</td>
										</tr>
									</table>
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="15" width="100%">
											</td>
										</tr>
									</table>
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td style="font-size: 14px; color: rgb(255, 255, 255); text-align: center; font-family: Helvetica,Arial,sans-serif; line-height: 22px; font-weight: bold; vertical-align: top;" class="fullCenter" align="center" width="100%">												
												<!--[if !mso]><!--><span style="font-family:'; $body.="'proxima_nova_rgregular'"; $body.=', Helvetica; font-weight:normal;"><!--<![endif]--><p cu-identify="element_05107549716118674" ><div style="text-align: center;"><font size="3"><span style="font-family: Verdana;">Congratulations! You have successfully activated your account. <br>Please click the below button to get started  and enjoy the benefits of sales neuron.</span><br><span style="font-family: Verdana;"></span></font></div><!--[if !mso]><!--></span><!--<![endif]--><p>
											</td>
										</tr>
									</table>									
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="15" width="100%">
											</td>
										</tr>
									</table>									
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<!----------------- Button Center ----------------->
										<tr>
											<td class="buttonScale" align="center" width="auto">
												<table class="buttonScale"style="border-radius: 25px; padding-left: 35px; padding-right: 35px; background-color: rgb(0, 150, 136);" id="shake" align="center" bgcolor="#009688" border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td style="font-weight: bold; font-family: Helvetica,Arial,sans-serif; color: rgb(255, 255, 255); background-color: rgb(0, 150, 136);" align="center" bgcolor="#ffffff" height="52" width="auto">
															<!--[if !mso]><!--><span style="border: 1px solid #ffffff; border-radius: 8px; font-family:'; $body.="'proxima_nova_rgbold'";$body.=',Helvetica;font-weight: normal;padding: 8px;  background-color:#ffffff;"><!--<![endif]-->
																<a cu-identify="element_0753161873513802" href="'.$root.'/login.php" style="color: #009688; font-size: 18px; text-decoration: none; line-height: 34px; width: 100%;">Let’s Get Started</a>
															<!--[if !mso]><!--></span><!--<![endif]-->
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<!----------------- End Button Center ----------------->
									</table>
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="0" width="100%">
											</td>
										</tr>
									</table>									
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="0" width="100%">
											</td>
										</tr>
									</table>
									<!-- Social Icons -->
									<div style="display: none" id="element_09872123869190068"></div>
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="0" width="100%">
											</td>
										</tr>
									</table>
									</div>
									<!-- END SORTABLE -->
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table><!-- End Wrapper --> 
		</div>
		</td>
	</tr>
</table>
<!-- End Wrapper -->
<!-- Wrapper 3 -->
<div style="display: none" id="element_03663119396258956"></div>
<!-- End Wrapper 3 -->
<!-- Wrapper 28 -->
<div style="display: none" id="element_08147460135100794"></div><!-- Wrapper 28 -->
<!-- Wrapper 28 -->
<div style="display: none;" id="element_046737467804971156"></div><!-- Wrapper 28 -->
<!-- Wrapper 3 -->
<div style="display: none" id="element_010546669432505962"></div>
<!-- End Wrapper 3 -->
<!-- Wrapper Footer -->
<table style="background-color:#e1e1e1;" class="full" align="center" bgcolor="#f1f4f6" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td align="center" valign="top" width="100%">
			<!-- Wrapper -->
			<table class="mobile" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td align="center">
						<!-- Space -->
						<table class="full" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td height="20" width="100%"></td>
							</tr>
						</table><!-- End Space -->
						<!-- Wrapper -->
						<table class="full" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
							<tr>
								<td align="center" width="100%">
									<!-- Icon 1 -->
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: center;" class="fullCenter" align="left" border="0" cellpadding="0" cellspacing="0" width="400">
										<tr>
											<td style="text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 13px; color: #aaacaf; line-height: 22px;" class="fullCenter" valign="middle" width="100%">
												<span style="font-family:'; $body.="'proxima_nova_rgregular'";$body.=', Helvetica; font-weight: normal;">
													<span style="color: #787b80;">© 2015 </span>,<a href="http://www.aucventures.com" style="text-decoration: none; color: #787b80;">AUC Ventures Pvt Ltd</a> , All Rights Reserved
													<span style="font-family:'; $body.="'proxima_nova_rgregular'";$body.=', Helvetica;"></span>
												</span>
											</td>
										</tr>
									</table>
									<!-- Space -->
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" align="left" border="0" cellpadding="0" cellspacing="0" width="1">
										<tr>
											<td height="12" width="100%"></td>
										</tr>
									</table>
									<!-- Social Icons Footer -->
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: right;" class="fullCenter" align="right" border="0" cellpadding="0" cellspacing="0" width="120">
										<tr>
											<td id="icon18" width="100%">
												&nbsp;
												<a href="http://facebook.com/salesneuron" style="text-decoration: none;" >
													<img src="http://rocketway.net/themebuilder/products/templates/destiny/images/footer_icon2.png" style="width: 18px; height:auto;" alt=""  class="hover" border="0" height="auto" width="18">
												</a>
												&nbsp;
												<a href="https://twitter.com/salesneuron" style="text-decoration: none;" >
													<img src="http://rocketway.net/themebuilder/products/templates/destiny/images/footer_icon3.png" style="width: 18px; height:auto;" alt=""  class="hover" border="0" height="auto" width="18">
												</a>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table><!-- End Wrapper -->
						<!-- Space -->
						<table class="full" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td height="20" width="100%"></td>
							</tr>
						</table><!-- End Space -->
					</td>
				</tr>
			</table><!-- End Wrapper -->
		</div>
		</td>
	</tr>
</table><!-- Wrapper Footer -->
</div>
</body></html>	<style>body{ background: none !important; } </style>';
		$newbody = $body;
		$mail->IsHTML(true);
		$mail->MsgHTML ($newbody);
		$mail->AddAddress(trim($email));
		$mail->AddBCC('arvind@aucventures.com');
		$mail->AltBody="This is text only alternative body.";
		if($mail->send()) {
			return 'success';
		} else {
			return 'fail';
		}
	}
}