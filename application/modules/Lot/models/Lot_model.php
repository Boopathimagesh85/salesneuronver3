<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lot_model extends CI_Model
{
    private $lotmodule = 44;
    public function __construct()
    {
        parent::__construct();
		$this->load->model( 'Base/Basefunctions');
		$this->load->model( 'Base/Crudmodel');
    }
	/**	*	checklotstatus lot	*/
	public function checklotstatus($lotid) 
	{
		$rowsdata = $this->db->select('lotid')
							 ->from('itemtag')
							 ->where('lotid',$lotid)
							 ->where_in('status',array(1,5))
							 ->get()
							 ->num_rows();
		if($rowsdata > 0){
			echo 'no';
		} else {
			echo 'yes';
		}
	}		
	/**	*create lot	*/
	public function lotcreate()  
	{		
	    $loclingtime = $this->Basefunctions->get_company_settings('lockingtime');
		$updatingtime = $this->Basefunctions->get_company_settings('updatingtime');
		$currenttime = time("H:i:s");
		$ctime = new DateTime();
		$ctime->setTimestamp($currenttime);
		$ltime  = new DateTime();
		if($loclingtime == '00:00:00') {
			$ltime->setTimestamp($currenttime);
		} else {
			$ltime->setTimestamp(strtotime($loclingtime));
		}
		if($ctime > $ltime) {
			$lotdate = date("Y-m-d",strtotime("tomorrow"));
			$lotdate = $this->Basefunctions->checkgivendateisholidayornot($lotdate);
		} else {
			$lotdate=$this->Basefunctions->ymd_format($_POST['lotdate']);
		}
		$lotserialrow = $this->Basefunctions->singlefieldfetch('moduleid','moduleid','varserialnumbermaster',$this->lotmodule);// check lot number format in serial number master
		if($lotserialrow == 1){
			 echo 'FAIL';
		}else{
			$serialnumber = $this->Basefunctions->varrandomnumbergenerator($this->lotmodule,'lotnumber',1,'');
			if(empty($_POST['accountid'])){
				$_POST['accountid'] = 1;
			}else{
				$_POST['accountid'] = $_POST['accountid'];
			}
			if(empty($_POST['product'])){
				$_POST['product'] = 1;
			}else{
				$_POST['product'] = $_POST['product'];
			}
			if(empty($_POST['salesdetailid'])){
				$_POST['salesdetailid'] = 1;
			}else{
				$_POST['salesdetailid'] = $_POST['salesdetailid'];
			}
			if(empty($_POST['billno'])){
				$_POST['billno'] = '';
				$purchase = 'No';
			}else{
				$_POST['billno'] = $_POST['billno'];
				$purchase = 'Yes';
			}
			if(empty($_POST['proparentcategoryid'])){
				$_POST['proparentcategoryid'] = '1';
			}else{
				$_POST['proparentcategoryid'] = $_POST['proparentcategoryid'];
			}
			if(isset($_POST['pieces'])){
				$_POST['pieces'] = $_POST['pieces'];
			}else{
				$_POST['pieces'] = 0;
			}
			if(isset($_POST['suppliernumber'])){
				$_POST['suppliernumber'] = $_POST['suppliernumber'];
			}else{
				$_POST['suppliernumber'] = '';
			}
			if(empty($_POST['stoneapplicable'])) {
				$_POST['stoneapplicable'] = 'No';
			} else {
				$_POST['stoneapplicable'] = $_POST['stoneapplicable'];
			}
			if($_POST['caratweight'] > 0) {
				$loosestone = 'Yes';
			} else {
				$loosestone = 'No';
			}
			$insert=array(
							'lotnumber'=>$serialnumber,
							'lotdate'=>$lotdate,
					        'lottypeid'=>$_POST['lotcreationopt'],
							'categoryid'=>$_POST['proparentcategoryid'],
					        'productid'=>$_POST['product'],
							'purityid'=>$_POST['purityid'],						
							'grossweight'=>$_POST['grossweight'],						
							'stoneweight'=>$_POST['stoneweight'],						
							'netweight'=>$_POST['netweight'],
							'caratweight'=>$_POST['caratweight'],
							'pieces'=>$_POST['pieces'],						
							'description'=>$_POST['description'],
							'accountid'=>$_POST['accountid'],
					        'salesdetailid'=>$_POST['salesdetailid'],
							'billnumber'=>$_POST['billno'],
							'billrefno'=>$_POST['suppliernumber'],
							'stoneapplicable'=>$_POST['stoneapplicable'],
							'loosestone'=>$loosestone,
							'purchase'=>$purchase,
							'industryid'=>$this->Basefunctions->industryid,
							'status'=>$this->Basefunctions->activestatus,
							'branchid'=>$this->Basefunctions->branchid
						);
			if($ctime > $ltime) {
				$insert = array_merge($insert,$this->Crudmodel->nextdaydefaultvalueget());
			} else {
				$insert = array_merge($insert,$this->Crudmodel->defaultvalueget());
			}
			$this->db->insert('lot',array_filter($insert));
			$primaryid = $this->db->insert_id();
			if($purchase == 'Yes') {
				$lotid = array('lotid'=>$primaryid);
				$this->db->where('salesdetailid',$_POST['salesdetailid']);
				$this->db->update('salesdetail',$lotid);
			}
			// Loose Stone Update
			if($_POST['caratweight'] > 0) {
				$loosestonectwtupdate = array(
					'diacaratweight' => $_POST['caratweight'],
					'diapieces' => $_POST['pieces']
				);
			} else {
				$loosestonectwtupdate = array(
					'diacaratweight' => 0,
					'diapieces' => 0
				);
			}
			$this->db->where('lotid',$primaryid);
			$this->db->update('lot',$loosestonectwtupdate);
			//audit-log
			$userid = $this->Basefunctions->logemployeeid;
			$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
			$activity = ''.$user.' created Lot - '.$serialnumber.'';
			$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,$this->lotmodule);
			echo 'SUCCESS';
		}
	}
	/**	*retrieve lot
	*return JSON lot-data*/
	public function lotretrive() {	
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2);//weight round-off
		$dia_round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',8);
		$id=$_GET['primaryid'];
		$this->db->select('lot.lotid,lot.lotnumber,lot.lotdate,lot.productid,lot.purityid,ROUND(lot.grossweight,'.$round.') as grossweight, ROUND(lot.stoneweight,'.$round.') as stoneweight, ROUND(lot.netweight,'.$round.') as netweight, ROUND(lot.caratweight,'.$dia_round.') as caratweight, lot.description,lot.accountid,lot.pieces,lot.lottypeid,lot.categoryid,category.categoryname,lot.lotused,lot.stoneapplicable',false);
		$this->db->from('lot');
		$this->db->join('category','category.categoryid=lot.categoryid');
		$this->db->where('lotid',$id);
		$this->db->limit(1);
		$info=$this->db->get()->row();
		//check whether the lot it used in tag
		$tagrecords = 'no';
		$tagweight = 0;
		$tagsumpieces =0;
		$tagrecord = $this->db->select('itemtagid, ROUND(SUM(itemtag.grossweight),'.$round.') as grossweight, ROUND(SUM(itemtag.netweight),'.$round.') as netweight, ROUND(SUM(itemtag.caratweight),'.$dia_round.') as caratweight, ROUND(SUM(itemtag.stoneweight),'.$round.') as stoneweight,SUM(itemtag.pieces) as sumpieces',false)
								->from('itemtag')
								->where('lotid',$id)
								->where_not_in('status',array(0,3))
								->get();
		if($tagrecord->num_rows() > 0) {
			$tagrecords = 'yes';
			foreach($tagrecord->result() as $inform) {
				$tagweight = $inform->grossweight;
				$tagsumpieces = $inform->sumpieces;
				$stoneweight = $inform->stoneweight;
				$tagnetweight = $inform->netweight;
				$tagcaratweight = $inform->caratweight;
			}
		}
		$jsonarray=array(
							'lotnumber' => $info->lotnumber,
				            'product' => $info->productid,
							'purityid' => $info->purityid,							
							'lotdate'=>date("d-m-Y", strtotime($info->lotdate)),
							'grossweight' => $info->grossweight,
							'stoneweight' => $info->stoneweight,
							'netweight' => $info->netweight,
							'caratweight' => $info->caratweight,
							'pieces' => $info->pieces,
							'description' => $info->description,
							'accountid' => $info->accountid,
							'lotprimaryid' => $info->lotid,
							'taggedlot'=>$tagrecords,
							'tagweight'=>$tagweight,
							'tagstoneweight'=>$stoneweight,
							'tagnetweight'=>$tagnetweight,
							'tagcaratweight'=>$tagcaratweight,
							'tagsumpieces'=>$tagsumpieces,
							'lotcreationopt'=>$info->lottypeid,
							'proparentcategoryid'=>$info->categoryid,
							'prodparentcategoryname'=>$info->categoryname,
							'lotused'=>$info->lotused,
							'stoneapplicable'=>$info->stoneapplicable
						);	
		echo json_encode($jsonarray);
	}
	/**	*update lot	*/
	public function lotupdate() {
		$loclingtime = $this->Basefunctions->get_company_settings('lockingtime');
		$updatingtime = $this->Basefunctions->get_company_settings('updatingtime');
		$currenttime = time("H:i:s");
		$ctime = new DateTime();
		$ctime->setTimestamp($currenttime);
		$ltime  = new DateTime();
		if($loclingtime == '00:00:00') {
			$ltime->setTimestamp($currenttime);
		} else {
			$ltime->setTimestamp(strtotime($loclingtime));
		}
		if($ctime > $ltime) {
			$lotdate = date("Y-m-d",strtotime("tomorrow"));
			$lotdate = $this->Basefunctions->checkgivendateisholidayornot($lotdate);
		} else {
			$lotdate=$this->Basefunctions->ymd_format($_POST['lotdate']);
		}
		$id=$_POST['primaryid'];
		if(empty($_POST['accountid'])){
			$_POST['accountid'] = 1;
		}else{
			$_POST['accountid'] = $_POST['accountid'];
		}
		if(isset($_POST['pieces'])){
				$_POST['pieces'] = $_POST['pieces'];
			}else{
				$_POST['pieces'] = 0;
			}
		if(empty($_POST['product'])){
			$_POST['product'] = 1;
		}else{
			$_POST['product'] = $_POST['product'];
		}
		if(empty($_POST['proparentcategoryid'])){
			$_POST['proparentcategoryid'] = '1';
		}else{
			$_POST['proparentcategoryid'] = $_POST['proparentcategoryid'];
		}
		if(empty($_POST['stoneapplicable'])) {
			$_POST['stoneapplicable'] = 'No';
		} else {
			$_POST['stoneapplicable'] = $_POST['stoneapplicable'];
		}
		if($_POST['caratweight'] > 0) {
			$loosestone = 'Yes';
		} else {
			$loosestone = 'No';
		}
		$update=array(						
						'purityid' => $_POST['purityid'],
						'categoryid'=>$_POST['proparentcategoryid'],
				        'productid'=>$_POST['product'],
						'lotdate' => $lotdate,
						'grossweight' => $_POST['grossweight'],						
						'stoneweight' => $_POST['stoneweight'],						
						'netweight' => $_POST['netweight'],				
						'caratweight' => $_POST['caratweight'],				
						'pieces' => $_POST['pieces'],						
						'description' => $_POST['description'],
						'accountid'=>$_POST['accountid'],
						'stoneapplicable'=>$_POST['stoneapplicable'],
						'loosestone'=>$loosestone,
						'industryid'=>$this->Basefunctions->industryid,
						'branchid'=>$this->Basefunctions->branchid
					);
					
		if($ctime > $ltime) {
			$update=array_merge($update,$this->Crudmodel->nextdayupdatedefaultvalueget());
		} else {
			$update=array_merge($update,$this->Crudmodel->updatedefaultvalueget());
		}
	    $this->db->where('lotid',$id);
		$this->db->update('lot',$update);
		// Loose Stone Update
		if($_POST['caratweight'] > 0) {
			$loosestonectwtupdate = array(
				'diacaratweight' => $_POST['caratweight'],
				'diapieces' => $_POST['pieces']
			);
		} else {
			$loosestonectwtupdate = array(
				'diacaratweight' => 0,
				'diapieces' => 0
			);
		}
		$this->db->where('lotid',$id);
		$this->db->update('lot',$loosestonectwtupdate);
		//lot close
		$this->editlotopenclose($id);
		//audit-log
		$lotnumber = $this->Basefunctions->singlefieldfetch('lotnumber','lotid','lot',$id);
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' updated Lot - '.$lotnumber.'';
		$this->Basefunctions->notificationcontentadd($id,'Updated',$activity ,$userid,$this->lotmodule);
		echo 'SUCCESS';
	}
	/**	*retrieve lot grid	 errorgrossweight*/
	public function lotview($tablename,$sortcol,$sortord,$pagenum,$rowscount,$filter=array()) {
		$industryid=$this->Basefunctions->industryid;
		$round =$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2);//weight round-off
		$filtervalarray =array();
		if(!empty($filter)) {
			$fconditonid = explode('|',$filter[0]);
			$filtercondition = explode('|',$filter[1]);
			$filtervalue = explode('|',$filter[2]);
		} else {
			$fconditonid = array();
			$filtercondition = array();
			$filtervalue = array();
		}
		$filtervalarray = $this->Basefunctions->filtergenerateconditioninfo($fconditonid,$filtercondition,$filtervalue);
		if(count($filtervalarray) > 0) {
			$filterwherecondtion = $this->Basefunctions->filtergeneratewhereclause($filtervalarray['filterarray'],'');
			if($filterwherecondtion == '') {
				$filterwherecondtion = "  1=1";
			} else {
				$filterwherecondtion = "  ".$filterwherecondtion;
			}
		} else {
			$filterwherecondtion = "  1=1";
		}
		$dataset ='lotid,lotnumber,lot.productid,lot.purityid,ROUND(lot.grossweight,'.$round.') as grossweight,ROUND(lot.stoneweight,'.$round.') as stoneweight,ROUND(lot.netweight,'.$round.') as netweight,ROUND(lot.caratweight,2) as caratweight,lot.description,product.productname,purity.purityname,lot.lotdate,status.statusname,account.accountname,lot.purchase,lot.pieces,lottype.lottypename,category.categoryname,lot.billnumber,lot.billrefno,lot.receiveorder';
		
		$join=' LEFT OUTER JOIN purity ON purity.purityid=lot.purityid';
		$join .=' LEFT OUTER JOIN category ON category.categoryid=lot.categoryid';
		$join .=' LEFT OUTER JOIN product ON product.productid=lot.productid';
		$join .=' LEFT OUTER JOIN lottype ON lottype.lottypeid=lot.lottypeid';
		$join .=' LEFT OUTER JOIN account ON account.accountid=lot.accountid';
		$join .=' LEFT OUTER JOIN status ON status.status=lot.status';
		$status=$tablename.'.status IN (1,11) AND '.$tablename.'.industryid IN('.$industryid.') ';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		/* query */
		$result = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$filterwherecondtion.' AND '.$status.' GROUP BY lot.lotid ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$lotpiecestatus = $this->Basefunctions->get_company_settings('lotpieces');
		$data=array();
		$i=0;
		foreach($result->result() as $row) {
			$pendingweight = $this->lotpendingweight($row->lotid);
			$stockweight = $this->lotstockweight($row->lotid);
			$lotid = $row->lotid;
			$lotnumber =$row->lotnumber;
			$lotdate =$row->lotdate;
			$statusname =$row->statusname;
			$categoryname = $row->categoryname;
			$productname =$row->productname;
			$purityname =$row->purityname;
			$grossweight =$row->grossweight;
			$stoneweight =$row->stoneweight;
			$caratweight =$row->caratweight;
			$netweight =$row->netweight;				
			$pendinggrossweight =$pendingweight['grossweight'];
			$pendingnetweight =$pendingweight['netweight'];
			$pendingcaratweight =$pendingweight['caratweight'];	
			$pendingpieces =$pendingweight['pieces'];	
			$stockgrossweight =$stockweight['stockgrossweight'];
			$stocknetweight =$stockweight['stocknetweight'];
			$stockcaratweight =$stockweight['stockcaratweight'];
			$stockpieces =$stockweight['stockpieces'];
			$accountname =$row->accountname;
			$description =$row->description;
			$purchase = $row->purchase;
			$pieces = $row->pieces;
			$billnumber = $row->billnumber;
			$billrefno = $row->billrefno;
			$lottypename =$row->lottypename;
			$receiveorder =$row->receiveorder;
			if($lotpiecestatus == 'Yes') {
				$data[$i]=array('id'=>$lotid,$billrefno,$lotnumber,$lotdate,$statusname,$billnumber,$categoryname,$grossweight,$pendinggrossweight,$stockgrossweight,$netweight,$pendingnetweight,$stocknetweight,$caratweight,$pendingcaratweight,$stockcaratweight,$pieces,$pendingpieces,$stockpieces,$accountname,$description,$purchase,$lottypename,$productname,$purityname,$stoneweight,$receiveorder);
			} else {			
				$data[$i]=array('id'=>$lotid,$billrefno,$lotnumber,$lotdate,$statusname,$billnumber,$categoryname,$grossweight,$pendinggrossweight,$stockgrossweight,$netweight,$pendingnetweight,$stocknetweight,$caratweight,$pendingcaratweight,$stockcaratweight,$accountname,$description,$purchase,$lottypename,$productname,$purityname,$stoneweight,$receiveorder);
			}
			$i++;
		}
		$finalresult=array('0'=>$data,'1'=>$page,'2'=>'json');
		return $finalresult;
	}
	public function lotpendingweight($lotid)
	{
		$data = $this->lotdropdown($lotid);
		if($data->num_rows() > 0)
		{
			foreach($data->result() as $info)
			{
				if($info->currentgrossweight>=0){
					$pendingweight = array(
									'grossweight'=>$info->currentgrossweight,
									'netweight'=>$info->currentnetweight,
									'caratweight'=>$info->currentcaratweight,
									'pieces'=>$info->currentpieces
								);
				}else{
					$pendingweight = array(
							'grossweight'=>0,
							'netweight'=>0,
							'caratweight'=>0,
							'pieces'=>$info->currentpieces
					);
				}
			}
		}
		else
		{
			$pendingweight = array(
									'grossweight'=>0,
									'netweight'=>0,
									'caratweight'=>0,
									'pieces'=>0
								);
		}
		return $pendingweight;
	}
	public function lotstockweight($lotid) //Stock weight
	{
		$data = $this->lotdropdown($lotid);
		if($data->num_rows() > 0) {
			foreach($data->result() as $info) {
				if($info->stockgrossweight >= 0){
					$stockweight = array(
									'stockgrossweight'=>$info->stockgrossweight,
									'stocknetweight'=>$info->stocknetweight,
									'stockcaratweight'=>$info->stockcaratweight,
									'stockpieces'=>$info->stockpieces
								);
				} else {
					$stockweight = array(
							'stockgrossweight'=>0,
							'stocknetweight'=>0,
							'stockcaratweight'=>0,
							'stockpieces'=>$info->stockpieces
					);
				}
			}
		} else {
			$stockweight = array(
									'stockgrossweight'=>0,
									'stocknetweight'=>0,
									'stockcaratweight'=>0,
									'stockpieces'=>0
								);
		}
		return $stockweight;
	}
	/**	*inactivate-delete lot grid	*/
	public function lotdelete()
	{
		$id=$_GET['primaryid'];
		//inactivate lot
		$delete = $this->Basefunctions->delete_log();
		$this->db->where('lotid',$id);
		$this->db->update('lot',$delete);		
		//audit-log
		$lotnumber = $this->Basefunctions->singlefieldfetch('lotnumber','lotid','lot',$id);
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' deleted Lot - '.$lotnumber.'';
		$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,$this->lotmodule);
		echo 'SUCCESS';
	}
	/**
	*auto lot-close open operations
	*/
	public function autolotopenclose($lotid) {
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2);//weight round-off
		$dia_weight_round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',8);
		$current_lot_data = $this->lotdropdown($lotid);
		{ // Retrieve Loose Stone
			$loosestonedata = 'No';
			$info = $this->db->query("SELECT lot.loosestone as loosestone FROM lot WHERE lot.lotid IN ($lotid) LIMIT 1");
			foreach($info->result() as $row) {
				$loosestonedata = $row->loosestone;
			}
		}
		$update_close_status = 0;
		if($current_lot_data->num_rows() > 0) {
			foreach($current_lot_data->result() as $info) {
				$currentnetweight = $info->currentnetweight;
				$currentgrossweight = $info->currentgrossweight;
				$currentcaratweight = $info->currentcaratweight;
				$lotgrossweight = $info->grossweight;
				$lotnetweight = $info->netweight;
				$lotcaratweight = $info->caratweight;
				$currentpieces = $info->currentpieces;
				$stockgrossweight = $info->stockgrossweight;
				$stocknetweight = $info->stocknetweight;
				$stockcaratweight = $info->stockcaratweight;
				$stockpieces = $info->stockpieces;
				$lotproductid = $info->productid;
			}
			$lotpieces = $this->Basefunctions->get_company_settings('lotpieces');
			$lottolerancepercent = $this->Basefunctions->get_company_settings('lottolerancepercent');
			$lottolerancevalue = $this->Basefunctions->get_company_settings('lottolerancevalue');
			$lottolerancediactvalue = $this->Basefunctions->get_company_settings('lottolerancediactvalue');
			{ // To check price tag concept. Validte with pieces not with weight
				if(empty($lotproductid) || $lotproductid == '1' || $lotproductid == 'null') {
					$lotproductid = 1;
					$itemtagtypeid = 1;
				} else {
					$itemtagtypeid = $this->Basefunctions->singlefieldfetch('tagtypeid','productid','product',$lotproductid);
				}
			}
			if($lotpieces == 'Yes'){  // lot pieces  & netweight are based lot closed
				if($lotproductid > 1 && $itemtagtypeid == 5) { //Validate pieces for price tag
					if($currentpieces <= 0) {
						$closelot_data = array(
							'lotused'=>$this->Basefunctions->activestatus,
							'status'=>$this->Basefunctions->closestatus,
							'createdate' => date($this->Basefunctions->datef),
							'lastupdatedate' => date($this->Basefunctions->datef),
							'createuserid' => $this->Basefunctions->userid,
							'lastupdateuserid' => $this->Basefunctions->userid
						);
						$this->db->where('lotid',$lotid);
						$this->db->update('lot',$closelot_data);
					} else {
						$openlot_data = array(
							'status'=>$this->Basefunctions->activestatus,
							'lotused'=>$this->Basefunctions->activestatus
						);
						$openlot_data = array_merge($openlot_data ,$this->Crudmodel->defaultvalueget());
						$this->db->where('lotid',$lotid);
						$this->db->update('lot',$openlot_data);
					}
				} else {
					if($lottolerancepercent == 1) { //If Lot Percentage concept is yes
						$update_errorgrsweight = ROUND(($lotgrossweight - $stockgrossweight),$round);
						$update_errornetweight = ROUND(($lotnetweight - $stocknetweight),$round);
						$update_errorcaratweight = ROUND(($lotcaratweight - $stockcaratweight),$dia_weight_round);
						$current_errorgrsweight = ROUND(abs($lotgrossweight - $stockgrossweight),$round);
						$current_errornetweight = ROUND(abs($lotnetweight - $stocknetweight),$round);
						$current_errorcaratweight = ROUND(abs($lotcaratweight - $stockcaratweight),$dia_weight_round);
						$lotpercentagevalue = (($lotgrossweight)*($lottolerancevalue/100));
						$lotdiapercentagevalue = (($lotcaratweight)*($lottolerancediactvalue/100));
						if($loosestonedata == 'Yes' && ($update_errorcaratweight <= $lotdiapercentagevalue || $currentpieces <= 0)) {
							$update_close_status = 1;
						} else if($loosestonedata == 'No' && $current_errorgrsweight <= $lotpercentagevalue && $current_errornetweight <= $lotpercentagevalue || $currentpieces <= 0) {
							$update_close_status = 1;
						}
						//close the lot & update the error weights & pieces
						if($update_close_status == 1) {
							$closelot_data = array(
								'lotused'=>$this->Basefunctions->activestatus,
								'status'=>$this->Basefunctions->closestatus,
								'createdate' => date($this->Basefunctions->datef),
								'lastupdatedate' => date($this->Basefunctions->datef),
								'createuserid' => $this->Basefunctions->userid,
								'lastupdateuserid' => $this->Basefunctions->userid
							);
							$this->db->where('lotid',$lotid);
							$this->db->update('lot',$closelot_data);
							$finalerrorweight = array( //Update pending weight or error weight
								'errorgrossweight'=>$update_errorgrsweight,
								'errornetweight'=>$update_errornetweight
							);
							$this->db->where('lotid',$lotid);
							$this->db->update('lot',$finalerrorweight);
						} else { //active lot & zero error weights
							$openlot_data = array(
								'status'=>$this->Basefunctions->activestatus,
								'lotused'=>$this->Basefunctions->activestatus
							);
							$openlot_data = array_merge($openlot_data ,$this->Crudmodel->defaultvalueget());
							$this->db->where('lotid',$lotid);
							$this->db->update('lot',$openlot_data);
						}
					} else { //If lot percentage concept is no
						if($loosestonedata == 'Yes' && $currentcaratweight == 0) {
							$update_close_status = 1;
						} else if($loosestonedata == 'No' && $currentnetweight == 0 || $currentpieces == 0) {
							$update_close_status = 1;
						}
						if($update_close_status == 1) { //close the lot & update the error weights & pieces
							$closelot_data = array(
								'lotused'=>$this->Basefunctions->activestatus,
								'status'=>$this->Basefunctions->closestatus,
								'createdate' => date($this->Basefunctions->datef),
								'lastupdatedate' => date($this->Basefunctions->datef),
								'createuserid' => $this->Basefunctions->userid,
								'lastupdateuserid' => $this->Basefunctions->userid
							);
							$this->db->where('lotid',$lotid);
							$this->db->update('lot',$closelot_data);
							
						} else { //active lot & zero error weights
							$openlot_data = array(
								'status'=>$this->Basefunctions->activestatus,
								'lotused'=>$this->Basefunctions->activestatus
							);
							$openlot_data = array_merge($openlot_data ,$this->Crudmodel->defaultvalueget());
							$this->db->where('lotid',$lotid);
							$this->db->update('lot',$openlot_data);
						}
					}
				}
			} else {   // only net weight based lot closed
				if($lotproductid > 1 && $itemtagtypeid == 5) { //Validate pieces for price tag
					if($currentpieces <= 0) {
						$closelot_data = array(
							'lotused'=>$this->Basefunctions->activestatus,
							'status'=>$this->Basefunctions->closestatus,
							'createdate' => date($this->Basefunctions->datef),
							'lastupdatedate' => date($this->Basefunctions->datef),
							'createuserid' => $this->Basefunctions->userid,
							'lastupdateuserid' => $this->Basefunctions->userid
						);
						$this->db->where('lotid',$lotid);
						$this->db->update('lot',$closelot_data);
					} else {
						$openlot_data = array(
							'status'=>$this->Basefunctions->activestatus,
							'lotused'=>$this->Basefunctions->activestatus
						);
						$openlot_data = array_merge($openlot_data ,$this->Crudmodel->defaultvalueget());
						$this->db->where('lotid',$lotid);
						$this->db->update('lot',$openlot_data);
					}
				} else {
					if($lottolerancepercent == 1) { //If Lot Percentage concept is yes
						$update_errorgrsweight = ROUND(($lotgrossweight - $stockgrossweight),$round);
						$update_errornetweight = ROUND(($lotnetweight - $stocknetweight),$round);
						$update_errorcaratweight = ROUND(($lotcaratweight - $stockcaratweight),$dia_weight_round);
						$current_errornetweight = ROUND(abs($lotnetweight - $stocknetweight),$round);
						$current_errorcaratweight = ROUND(abs($lotcaratweight - $stockcaratweight),$dia_weight_round);
						$lotpercentagevalue = (($lotgrossweight)*($lottolerancevalue/100));
						$lotdiapercentagevalue = (($lotcaratweight)*($lottolerancediactvalue/100));
						if($loosestonedata == 'Yes' && ($update_errorcaratweight <= $lotdiapercentagevalue)) {
							$update_close_status = 1;
						} else if($loosestonedata == 'No' && $current_errornetweight <= $lotpercentagevalue) {
							$update_close_status = 1;
						}
						//close the lot & update the error weights & pieces
						if($update_close_status == 1) {
							$closelot_data = array(
								'status'=>$this->Basefunctions->closestatus,
								'lotused'=>$this->Basefunctions->activestatus,
								'createdate' => date($this->Basefunctions->datef),
								'lastupdatedate' => date($this->Basefunctions->datef),
								'createuserid' => $this->Basefunctions->userid,
								'lastupdateuserid' => $this->Basefunctions->userid
							);
							$this->db->where('lotid',$lotid);
							$this->db->update('lot',$closelot_data);
							$finalerrorweight = array(
								'errorgrossweight'=>$update_errorgrsweight,
								'errornetweight'=>$update_errornetweight
							);
							$this->db->where('lotid',$lotid);
							$this->db->update('lot',$finalerrorweight);
						} else { //active lot & zero error weights
							$openlot_data = array(
								'status'=>$this->Basefunctions->activestatus,
								'lotused'=>$this->Basefunctions->activestatus
							);
							$openlot_data = array_merge($openlot_data ,$this->Crudmodel->defaultvalueget());
							$this->db->where('lotid',$lotid);
							$this->db->update('lot',$openlot_data);
						}
					} else {
						if($loosestonedata == 'Yes' && $currentcaratweight == 0) {
							$update_close_status = 1;
						} else if($loosestonedata == 'No' && $currentnetweight == 0) {
							$update_close_status = 1;
						}
						if($update_close_status == 1) { //close the lot & update the error weights & pieces
							$closelot_data = array(
								'status'=>$this->Basefunctions->closestatus,
								'lotused'=>$this->Basefunctions->activestatus,
								'createdate' => date($this->Basefunctions->datef),
								'lastupdatedate' => date($this->Basefunctions->datef),
								'createuserid' => $this->Basefunctions->userid,
								'lastupdateuserid' => $this->Basefunctions->userid
							);
							$this->db->where('lotid',$lotid);
							$this->db->update('lot',$closelot_data);
						} else { //active lot & zero error weights
							$openlot_data = array(
								'status'=>$this->Basefunctions->activestatus,
								'lotused'=>$this->Basefunctions->activestatus
							);
							$openlot_data = array_merge($openlot_data ,$this->Crudmodel->defaultvalueget());
							$this->db->where('lotid',$lotid);
							$this->db->update('lot',$openlot_data);
						}
					}
				}
			}
		} else { //if they is not tagentry for tag's. //specialcase-when a last tag is deleted
			$openlot_data = array(
				'status'=>$this->Basefunctions->activestatus,
				'lotused'=>0
			);
			$openlot_data = array_merge($openlot_data ,$this->Crudmodel->defaultvalueget());
			$this->db->where('lotid',$lotid);
			$this->db->update('lot',$openlot_data);
		}
	}
	public function editlotopenclose($lotid) {		
		$current_lot_data = $this->lotdropdown($lotid);
		$loosestonedata = $this->Basefunctions->singlefieldfetch('loosestone','lotid','lot',$lotid);
		$update_close_status = 0;
		if($current_lot_data->num_rows() > 0) {
			foreach($current_lot_data->result() as $info) {
				$currentnetweight = $info->currentnetweight;
				$currentgrossweight = $info->currentgrossweight;
				$currentcaratweight = $info->currentcaratweight;
				$lotgrossweight = $info->grossweight;
				$lotnetweight = $info->netweight;
				$lotcaratweight = $info->caratweight;
				$currentpieces = $info->currentpieces;
				$stockgrossweight = $info->stockgrossweight;
				$stocknetweight = $info->stocknetweight;
				$stockcaratweight = $info->stockcaratweight;
				$stockpieces = $info->stockpieces;
			}
			$lotpieces = $this->Basefunctions->get_company_settings('lotpieces');
			if($lotpieces == 'Yes') {  // lot pieces  & netweight are based lot closed
				if($loosestonedata == 'Yes' && ($currentcaratweight == 0 || $currentpieces == 0)) {
					$update_close_status = 1;
				} else if($loosestonedata == 'No' && $currentnetweight == 0 || $currentpieces == 0) {
					$update_close_status = 1;
				}
				//close the lot & update the error weights & pieces
				if($update_close_status == 1) {
					$closelot_data = array(
											'lotused'=>$this->Basefunctions->activestatus,
											'status'=>$this->Basefunctions->closestatus,
											'createdate' => date($this->Basefunctions->datef),
											'lastupdatedate' => date($this->Basefunctions->datef),
											'createuserid' => $this->Basefunctions->userid,
											'lastupdateuserid' => $this->Basefunctions->userid
										  );
					$this->db->where('lotid',$lotid);
					$this->db->update('lot',$closelot_data);
				}
			} else {   // only net weight based lot closed
				if($loosestonedata == 'Yes' && $currentcaratweight <= 0) {
					$update_close_status = 1;
				} else if($loosestonedata == 'No' && $currentnetweight <= 0) {
					$update_close_status = 1;
				}
				//close the lot & update the error weights & pieces
				if($update_close_status == 1) {
					$closelot_data = array(
											'status'=>$this->Basefunctions->closestatus,
											'lotused'=>$this->Basefunctions->activestatus,
											'createdate' => date($this->Basefunctions->datef),
											'lastupdatedate' => date($this->Basefunctions->datef),
											'createuserid' => $this->Basefunctions->userid,
											'lastupdateuserid' => $this->Basefunctions->userid
											);
					$this->db->where('lotid',$lotid);
					$this->db->update('lot',$closelot_data);
				}
			}
		}
		else //if they is not tagentry for tag's. //specialcase-when a last tag is deleted
		{ 
			$openlot_data = array(
										'status'=>$this->Basefunctions->activestatus,
										'lotused'=>0
								);
			$openlot_data = array_merge($openlot_data ,$this->Crudmodel->defaultvalueget());
			$this->db->where('lotid',$lotid);
			$this->db->update('lot',$openlot_data);
		}
	}
	/*
	*lotdropdown
	*/
	public function lotdropdown($lotid) //?issue
	{
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2);//weight round-off
		$dia_weight_round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',8);
		$wh1 = ' lot.status = 1 ';
		$limit = '';
		if($lotid != '')
		{	
			$wh1 = ' lot.lotid = '.$lotid. ' ';
		}
		if($lotid != '')
		{	
			$limit = ' limit  1 ';
		}		
		$info = $this->db->query("
							SELECT lot.lotid,lotnumber,lotdate,lot.purityid,lot.productid,purity.purityname,
							ROUND((lot.grossweight + lot.errorgrossweight - coalesce(SUM(itemtag.grossweight),0)),$round) as currentgrossweight,
							ROUND((lot.netweight + lot.errornetweight - coalesce(SUM(itemtag.netweight),0)),$round) as currentnetweight,
							ROUND((lot.caratweight + lot.errorcaratweight - coalesce(SUM(itemtag.caratweight),0)),$dia_weight_round) as currentcaratweight,
							ROUND((lot.pieces + lot.errorpieces - coalesce(SUM(itemtag.pieces),0)),0) as currentpieces,
							coalesce(ROUND(SUM(itemtag.grossweight),$round),0) as stockgrossweight,
							coalesce(ROUND(SUM(itemtag.netweight),$round),0) as stocknetweight,
							coalesce(ROUND(SUM(itemtag.caratweight),$dia_weight_round),0) as stockcaratweight,
							coalesce(ROUND(SUM(itemtag.pieces),0),0) as stockpieces,
							ROUND(lot.grossweight,$round) as grossweight,
							ROUND(lot.netweight,$round) as netweight,
							ROUND(lot.caratweight,$dia_weight_round) as caratweight
							FROM lot
							JOIN purity ON purity.purityid = lot.purityid
							LEFT JOIN itemtag ON itemtag.lotid = lot.lotid
							WHERE $wh1 AND (itemtag.status in (1,5,12,15) or itemtag.status is null)
							GROUP BY lot.lotid $limit
						");
		return $info;
	}	
	// manual close - lot
	public function manualclose() {
		$lotid = $_GET['primaryid'];
		$closelot_data = array(
						'status'=>$this->Basefunctions->closestatus,
						'lastupdatedate' => date($this->Basefunctions->datef),
						'lastupdateuserid' => $this->Basefunctions->userid
				);
					$this->db->where('lotid',$lotid);
					$this->db->update('lot',$closelot_data);
		echo 'SUCCESS';
	}
	// manual open - lot
	public function manualopen() {
		$lotid = $_GET['primaryid'];
		$openlot_data = array(
						'status'=>$this->Basefunctions->activestatus,
						'lastupdatedate' => date($this->Basefunctions->datef),
						'lastupdateuserid' => $this->Basefunctions->userid
				);
					$this->db->where('lotid',$lotid);
					$this->db->update('lot',$openlot_data);
		echo 'SUCCESS';
	}
	public function loadbillnumber()
	{
		$this->db->select('sales.salesnumber,sales.salesid');
		$this->db->from('sales');
		$this->db->join('salesdetail','salesdetail.salesid = sales.salesid');
		$this->db->where('salesdetail.lotid',1);
		$this->db->where('salesdetail.lottypeid',3);
		$this->db->where_in('salesdetail.stocktypeid',array(17,80,81)); // P.amt,P.purewt,P.amtpurewt
		$this->db->where('salesdetail.status',$this->Basefunctions->activestatus);
		$this->db->group_by("sales.salesid");
		$salesdata=$this->db->get();
		if($salesdata->num_rows()>0){
			foreach($salesdata->result() as $info)
			{
				$salesarray[] = array(
						'billno'=>$info->salesnumber,
						'salesid'=>$info->salesid
				);
			}
	
		}else{
			$salesarray ='';
		}
		echo json_encode($salesarray);
	}
	//place order overlay inner grid work
	public function purchaseviewdynamicdatainfofetch($tablename,$sortcol,$sortord,$pagenum,$rowscount,$orderid) {
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$dia_round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',8);
		$dataset='sales.billrefno,salesdetail.salesdetailid,account.accountname,account.accountid,product.productid,product.productname,category.categoryname,category.categoryid,ROUND(salesdetail.grossweight,'.$round.') as grossweight,ROUND(salesdetail.grossweight,'.$round.') as grossweight, ROUND(salesdetail.stoneweight,'.$round.') as stoneweight, ROUND(salesdetail.netweight,'.$round.') as netweight, ROUND(salesdetail.caratweight,'.$dia_round.') as caratweight, salesdetail.pieces,purity.purityname,purity.purityid';
		$join =' LEFT JOIN account ON account.accountid=sales.accountid';
		$join .=' LEFT JOIN salesdetail ON salesdetail.salesid=sales.salesid';
		$join .=' LEFT JOIN product ON product.productid=salesdetail.productid';
		$join .=' LEFT JOIN purity ON purity.purityid=salesdetail.purityid';
		$join .=' LEFT JOIN lot ON lot.lotid=salesdetail.lotid';
		$join .=' LEFT JOIN category ON category.categoryid=salesdetail.categoryid';
		$status=$tablename.'.status='.$this->Basefunctions->activestatus.' and salesdetail.stocktypeid in (17,80,81) and salesdetail.lotid = 1 and sales.salesid="'.$orderid.'"';
		$extracond = '';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		/* query */
		$result = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '. $status.$extracond.' ORDER BY '.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$data=array();
		$i=1;
		foreach($result->result() as $row) {
			$protid =$row->salesdetailid;
			$purityid =$row->purityid;
			$purityname =$row->purityname;
			$categoryid =$row->categoryid;
			$categoryname =$row->categoryname;
			$productid =$row->productid;
			$productname =$row->productname;
			$grossweight =$row->grossweight;
			$stoneweight =$row->stoneweight;
			$netweight =$row->netweight;
			$caratweight =$row->caratweight;
			$pieces =$row->pieces;
			$accountname = $row->accountname;
			$accountid = $row->accountid;
			$billrefno = $row->billrefno;
			$data[$i]=array('id'=>$protid,$purityid,$purityname,$categoryid,$categoryname,$productid,$productname,$grossweight,$stoneweight,$netweight,$caratweight,$pieces,$accountname,$accountid,$billrefno);
			$i++;
		}
		$finalresult=array('0'=>$data,'1'=>$page,'2'=>'json');
		return $finalresult;
	}
	// get purity based on category
	public function getpuritybasedoncategory() {
		$categoryid = $_POST['primaryid'];
		$this->db->select('GROUP_CONCAT(DISTINCT(purityid)) as purity',false);
		$this->db->from('product');
		$this->db->where('product.categoryid',$categoryid);
		$this->db->where('product.status',$this->Basefunctions->activestatus);
		$productdata=$this->db->get();
		if($productdata->num_rows()>0){
			foreach($productdata->result() as $info)
			{
				$productarray =$info->purity;
			}
	
		}else{
			$productarray =1;
		}
		$uniqueproductarray = array_unique(explode(",", $productarray)); 
		$implodearray = implode(",", $uniqueproductarray);
		echo json_encode($implodearray);
	}
	//tree create model - for jewel industry
	public function jewel_treecreatemodel($tablename) {
		$data = array();
		$this->db->select('category.categoryid  AS Id,category.categoryname AS Name,category.parentcategoryid AS ParentId,category.categorylevel AS level');
		$this->db->from('category');
		//$this->db->join('category','product.categoryid=category.categoryid');
		//$this->db->where('product.status',1);
		$this->db->where('category.status',1);
		$this->db->where('category.categoryid >',10);
		$this->db->order_by('category.categoryid','ASC');
		$result = $this->db->get();		
		foreach($result->result() as $row) {
			$data[$row->Id] = array('id'=>$row->Id,'name'=>$row->Name,'pid'=>$row->ParentId,'level'=>$row->level);
		}
		$itemsByParent = array();
		foreach ($data as $item){
			if (!isset($itemsByParent[$item['pid']])) {
				$itemsByParent[$item['pid']] = array();
			}
			$itemsByParent[$item['pid']][] = $item;
		}	
		
		return $itemsByParent;
	}
	/*
	*	product_dd-level based dropdown values
	*/
	public function product_dd() {
		$industryid = $this->Basefunctions->industryid;
		$info =$this->db->query("SELECT `product`.`productname`, `product`.`productid`, `product`.`categoryid`,`product`.`weightcalculationtypeid`, `category`.`categoryid`, `category`.`categoryname`,`category`.`taxmasterid` as categorytaxid,`category`.`categorylevel`, `product`.`counterid` as counterid, `product`.`bullionapplicable`, `product`.`taxable`, group_concat(`tax`.`taxname`, '-',`tax`.`taxid`) as taxid,group_concat(distinct(`tax`.`taxid`)) as taxdataid,`taxmaster`.`taxmasterid` as taxmasterid, `product`.`purityid`,`product`.`chargeid`,`product`.`purchasechargeid`,`product`.`stoneapplicable`,`product`.`size`,`product`.`productstonecalctypeid`,`product`.`tagtypeid` FROM `product` JOIN `category` ON `category`.`categoryid` = `product`.`categoryid` JOIN `taxmaster` ON `taxmaster`.`taxmasterid` = `product`.`taxmasterid` LEFT JOIN `tax` ON `tax`.`taxmasterid` = `taxmaster`.`taxmasterid` JOIN `charge` ON `charge`.`chargeid` = `product`.`chargeid` JOIN `purchasecharge` ON `purchasecharge`.`purchasechargeid` = `product`.`purchasechargeid` WHERE `product`.`status` = 1 and `product`.`productid` not in(2,3) and `product`.`categoryid` not in(3,4) and `product`.`bullionapplicable`= 'No' and `product`.`industryid` = ".$industryid." GROUP BY `product`.`productid` ORDER BY `product`.`productid` DESC");
		return $info;
	}
	//Update grossweight applied in error grossweight field
	public function updateerrorweightform() {
		$id = $_POST['primaryid'];
		$update=array(
						'errorgrossweight' => $_POST['errorgrossweight'],
						'errornetweight' => $_POST['errornetweight'],
						'errorcaratweight' => $_POST['errorcaratweight'],
						'errorpieces' => $_POST['errorpieces'],
						'industryid'=>$this->Basefunctions->industryid,
						'branchid'=>$this->Basefunctions->branchid
					);
	    $this->db->where('lotid',$id);
		$this->db->update('lot',$update);
		//lot close
		$this->autolotopenclose($id);
		//audit-log
		$lotnumber = $this->Basefunctions->singlefieldfetch('lotnumber','lotid','lot',$id);
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' updated Lot - '.$lotnumber.'';
		$this->Basefunctions->notificationcontentadd($id,'Updated',$activity ,$userid,$this->lotmodule);
		echo 'SUCCESS';
	}
}