<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lot extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->helper('formbuild'); 
		$this->load->model('Lot_model');
		$this->load->model('Base/Basefunctions');
    }  
    public function index() {
		$moduleid = array(44);
		sessionchecker($moduleid);
		//action		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['account'] = $this->Basefunctions->accountgroup_dd('16');
		$data['filedmodids'] = $moduleid;
		//form
		$data['mainlicense'] = array(
				'productidshow'=>$this->Basefunctions->get_company_settings('productidshow'),
				'lotdata'=>$this->Basefunctions->get_company_settings('lotdata'),
				'lottype'=>$this->Basefunctions->get_company_settings('lot'),
				'stoneweightcheck'=>$this->Basefunctions->get_company_settings('stoneweightcheck')
				);
		$data['product'] = $this->Lot_model->product_dd();
		$data['purity']=$this->Basefunctions->purity_groupdropdown();
		$data['lotpieces'] = $this->Basefunctions->get_company_settings('lotpieces');
		$data['adminpassword'] = $this->Basefunctions->get_company_settings('adminpassword');
		$data['weight_round'] =$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2);//weight round-off
		$data['dia_weight_round'] =$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',8); //Diamond weight round off
		$data['lottype']=$this->Basefunctions->simpledropdownwithcond('lottypeid','lottypename','lottype','lottypeid','2,3');
		$this->load->view('lotcreationview',$data);
	}
	/**	*create lot	*/
	public function lotcreate() {  
    	$this->Lot_model->lotcreate();
    }
	/**	*retrieve lot	*/
	public function lotretrive() {  
    	$this->Lot_model->lotretrive();
    }
	/**	*update lot		*/
	public function lotupdate() {
		$this->Lot_model->lotupdate();
	}
	/**	*delete lot		*/
	public function lotdelete() {	
		$this->Lot_model->lotdelete();
	}
	/**	*checklotstatus	*/
	public function checklotstatus() {
		$lotid = $_GET['primaryid'];
		$this->Lot_model->checklotstatus($lotid);
	}
	/**	*retrieve lot	*/
	public function lotview() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'lot.lotid') : 'lot.lotid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$lotpiecestatus = $this->Basefunctions->get_company_settings('lotpieces');
		if($lotpiecestatus == 'Yes') {
			$colinfo = array('colmodelname'=>array('billrefno','lotnumber','lotdate','status','purchaseno','categoryname','grossweight','pendinggrossweight','stockgrossweight','netweight','pendingnetweight','stocknetweight','caratweight','pendingcaratweight','stockcaratweight','pieces','pendingpieces','stockpieces','accountid','description','purchase','lottype','productname','purityname','stoneweight','receiveorder'),'colmodelindex'=>array('lot.billrefno','lot.lotnumber','lot.lotdate','status.statusname','lot.billnumber','category.categoryname','lot.grossweight','lot.pendinggrossweight','lot.stockgrossweight','lot.netweight','lot.pendingnetweight','lot.stocknetweight','lot.caratweight','lot.pendingcaratweight','lot.stockcaratweight','lot.pieces','lot.pendingpieces','lot.stockpieces','account.accountname','lot.description','lot.purchase','lottype.lottypename','product.productname','purity.purityname','lot.stoneweight','lot.receiveorder'),'coltablename'=>array('lot','lot','lot','status','lot','category','lot','lot','lot','lot','lot','lot','lot','lot','lot','lot','lot','lot','lot','account','lot','lot','lottype','product','purity','lot','lot'),'uitype'=>array('2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2'),'colname'=>array('Sup No','Lot No','Date','Status','Pur. No','Category','Gross Wt','P.G.Wt','Tag.G.Wt','Net Wt','P.N.Wt','Tag.N.Wt','Carat Wt.','P.Ct.Wt.','Tag Ct.Wt','Pieces','P.Pcs','Tag.Pcs','Account','Desc','Purchase','Lot Type','Product','Purity','Stone Wt','Receive Order'),'colsize'=>array('100','100','150','80','150','150','90','90','90','90','90','90','90','90','90','80','80','80','150','100','100','120','150','111','90','120'));
		} else {
			$colinfo = array('colmodelname'=>array('billrefno','lotnumber','lotdate','status','purchaseno','categoryname','grossweight','pendinggrossweight','stockgrossweight','netweight','pendingnetweight','stocknetweight','caratweight','pendingcaratweight','stockcaratweight','accountid','description','purchase','lottype','productname','purityname','stoneweight','receiveorder'),'colmodelindex'=>array('lot.billrefno','lot.lotnumber','lot.lotdate','status.statusname','lot.billnumber','category.categoryname','lot.grossweight','lot.pendinggrossweight','lot.stockgrossweight','lot.netweight','lot.pendingnetweight','lot.stocknetweight','lot.caratweight','lot.pendingcaratweight','lot.stockcaratweight','account.accountname','lot.description','lot.purchase','lottype.lottypename','product.productname','purity.purityname','lot.stoneweight','lot.receiveorder'),'coltablename'=>array('lot','lot','lot','status','lot','category','lot','lot','lot','lot','lot','lot','lot','lot','lot','lot','account','lot','lot','lottype','product','purity','lot','lot'),'uitype'=>array('2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2'),'colname'=>array('Sup No','Lot No','Date','Status','Pur. No','Category','Gross Wt','P.G.Wt','Tag.G.Wt','Net Wt','P.N.Wt','Tag.N.Wt','Carat Wt.','P.Ct.Wt.','Tag Ct.Wt','Account','Desc','Purchase','Lot Type','Product','Purity','Stone Wt','Receive Order'),'colsize'=>array('100','100','150','80','150','150','90','90','90','90','90','90','90','90','90','150','100','100','120','150','111','90','120'));
		}
		$filterid = isset($_GET['filter']) ? $_GET['filter'] : '';
		$conditionname = isset($_GET['conditionname']) ? $_GET['conditionname'] : '';
		$filtervalue = isset($_GET['filtervalue']) ? $_GET['filtervalue'] : '';
		$filter = array('0'=>$filterid,'1'=>$conditionname,'2'=>$filtervalue);
		$result=$this->Lot_model->lotview($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$filter);
		$device = $this->Basefunctions->deviceinfo();
		/* if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'lot',$width,$height);
		}else { */
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'lot',$width,$height);
		//}
		echo json_encode($datas);
	}
	/**	*return the lot's pending weights	*/
	public function lotpendingweight($lotid)
	{
		$data = $this->Lot_model->lotdropdown($lotid);
		if($data->num_rows() > 0)
		{
			foreach($data->result() as $info)
			{
				if($info->currentgrossweight>=0){
				$pendingweight = array(
									'grossweight'=>$info->currentgrossweight,
									'netweight'=>$info->currentnetweight
								);
				}else{
					$pendingweight = array(
							'grossweight'=>0,
							'netweight'=>0
					);
				}
			}
		}
		else
		{
			$pendingweight = array(
									'grossweight'=>0,
									'netweight'=>0
								);
		}
		return $pendingweight;
	}	
	// lot manual close 
	public function manualclose()
	{
		$this->Lot_model->manualclose();
	}
	// lot manual open 
	public function manualopen()
	{
		$this->Lot_model->manualopen();
	}
	// load purchase billno
	public function loadbillnumber()
	{
		$this->Lot_model->loadbillnumber();
	}
	/* Fetch Main grid header,data,footer information */
	public function purchasegridinformationfetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$purchaseid = $_GET['purchasebillid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'sales.salesid') : 'sales.salesid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colid'=>array('1','1','1','1','1','1','1','1','1','1','1','1','1','1'),'colname'=>array('Purityid','Purity','Categoryid','Category','Productid','Product','Gross Wt','Stone Wt','Net Wt','Carat Wt','Pieces','Vendor','Vendorid','Ref No'),'colicon'=>array('','','','','','','','','','','','','',''),'colsize'=>array('100','120','100','200','100','200','100','100','100','100','100','100','1','100'),'colmodelname'=>array('innerpurityid','purity','innercategoryid','category','innerproductid','product','innergrosswt','innerstonewt','innernetwt','innercaratwt','innerpieces','inneraccountid','accountid','billrefno'),'colmodelindex'=>array('purity.purityid','purity.purityname','category.categoryid','category.categoryname','product.productid','product.productname','salesdetail.grossweight','salesdetail.stoneweight','salesdetail.netweight','salesdetail.caratweight','salesdetail.pieces','account.accountname','account.accountid','sales.billrefno'),'coltablename'=>array('purity','purity','category','category','product','product','salesdetail','salesdetail','salesdetail','salesdetail','salesdetail','account','account','sales'),'uitype'=>array('2','2','2','2','2','2','2','2','2','2','2','2','2','2'),'modname'=>array('Purityid','Purity','Categoryid','Category','Productid','Product','Gross Wt','Stone Wt','Net Wt','Carat Wt','Pieces','Vendor','Vendorid','Bill Refno'));
		$result=$this->Lot_model->purchaseviewdynamicdatainfofetch($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$purchaseid);
		$device = $this->Basefunctions->deviceinfo();
		//if($device=='phone') {
		//	$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'purcahsedetail',$width,$height);
		//} else {
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'purcahsedetail',$width,$height);
		//}
		echo json_encode($datas);
	}
	// get purity based on category
	public function getpuritybasedoncategory() {
		$this->Lot_model->getpuritybasedoncategory();
	}
	// update errorgrossweight in selected lot
	public function updateerrorweightform() {
		$this->Lot_model->updateerrorweightform();
	}
}