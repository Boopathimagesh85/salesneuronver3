<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('Base/headerfiles'); ?>
	<?php $this->load->view('Base/overlay'); ?>
	<?php $this->load->view('Base/basedeleteform');	
		$this->load->view('Base/modulelist');?>	
<style type="text/css">
.large-12 .columns .paddingzero .gridview-container {
	top:-8px !important;
}
</style>

</head>
<body>
	<div class="row" style="max-width:100%">
		<div class="off-canvas-wrap" data-offcanvas>
			<div class="inner-wrap">
				<!-- Add Form -->
				<?php $this->load->view('lotcreationform'); ?>
				<input type="hidden" value="<?php echo $weight_round?>" id="roundweight"/>					
			</div>
		</div>
	</div>
	<?php
		$this->load->view('Base/basedeleteformforcombainedmodules');
	?>
</body>
<div class="large-12 columns" style="position:absolute;"><div class="overlay" id="viewdeleteoverlayconform" style="z-index:120;"></div></div>
<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
<?php $this->load->view('Base/bottomscript'); ?>
<script src="<?php echo base_url();?>js/plugins/treemenu/modernizr.custom.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/plugins/treemenu/jquery.dlmenu.js" type="text/javascript"></script>
<?php include 'js/Lot/lot.php' ?>	
</html>
