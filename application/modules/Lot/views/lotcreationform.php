<div id="lotcreationview" class="gridviewdivforsh">
	<?php
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['gridid'] = 'lotgrid';
		$dataset['gridwidth'] = 'lotgridwidth';
		$dataset['gridfooter'] = 'lotgridfooter';
		$dataset['viewtype'] = 'disable';
		$dataset['moduleid'] = '44';
		$this->load->view('Base/singlemainviewformwithgrid.php',$dataset);
		$defaultrecordview = $this->Basefunctions->get_company_settings('mainviewdefaultview');
		echo hidden('mainviewdefaultview',$defaultrecordview);
	?>
</div>
<input type="hidden" id="lotdata" name="lotdata" value="<?php echo $mainlicense['lotdata']; ?>">
<input type="hidden" id="lottype" name="lottype" value="<?php echo $mainlicense['lottype']; ?>">
<input type="hidden" id="stoneweightcheck" name="stoneweightcheck" value="<?php echo $mainlicense['stoneweightcheck']; ?>">
<input type="hidden" id="weight_round" name="weight_round" value="<?php echo $weight_round; ?>">
<input type="hidden" id="dia_weight_round" name="dia_weight_round" value="<?php echo $dia_weight_round; ?>">
<input type="hidden" id="validate_caratwt" name="validate_caratwt" value="validate[required,decval[<?php echo $dia_weight_round; ?>],custom[number],funcCall[checktagweight],min[0.1],maxSize[15]] weightcalculate">
<input type="hidden" id="validate_grosswt" name="validate_grosswt" value="validate[required,decval[<?php echo $weight_round; ?>],custom[number],funcCall[checktagweight],min[0.1],maxSize[15]] weightcalculate">
<div id="lotcreationaddformdiv" class="singlesectionaddform">
	<div class="large-12 columns addformunderheader">&nbsp; </div>
	<div class="large-12 columns addformcontainer">
		<div class="row mblhidedisplay">&nbsp;</div>
		<div class="closed effectbox-overlay effectbox-default" id="groupsectionoverlay">
			 <div class="large-12 columns mblnopadding" style="padding-left:0px !important;">
			  <form method="POST" name="lotform" class="lotform"  id="lotform" style="height: 100% !important;">
				<div id="addlotvalidate" class="validationEngineContainer"  style="height: 100% !important;">
					<div id="editlotvalidate" class="validationEngineContainer"  style="height: 100% !important;">
					<div id="subformspan1" class="hiddensubform"  style="height: 100% !important;">
						<div class="large-4 large-offset-8 columns" style="padding-right: 0em !important;height:100% !important;padding-left:0px !important;">
							<div class="large-12 columns cleardataform border-modal-8px" style="padding-left: 0 !important; padding-right: 0 !important;height: 107% !important">
								<div class="large-12 columns sectionheaderformcaptionstyle">Lot Creation</div>
								<div class="large-12 columns sectionpanel">
									<div class="static-field large-6 columns lotdatehide">
										<label>Date<span class="mandatoryfildclass">*</span></label>					
										<input id="lotdate" class="fordatepicicon validate[required]" type="text" data-dateformater="dd-mm-yy" data-prompt-position="topLeft" readonly="readonly" tabindex="" name="lotdate">
									</div>
									<div class="static-field small-6 columns">
										<label>Lot Type<span class="mandatoryfildclass">*</span></label>
										<select data-validation-engine="" class="chzn-select" id="lotcreationopt" name="lotcreationopt" data-prompt-position="topLeft:14,36">
											<?php foreach($lottype as $key):?>
												<option value="<?php echo $key->lottypeid;?>">
												<?php echo $key->lottypename;?></option>
											<?php endforeach;?>	
										</select>
									</div>
									<div class="static-field small-6 columns purchasebill hidedisplay">
										<label>Purchase Billno<span class="mandatoryfildclass">*</span></label>
										<select data-validation-engine="" class="chzn-select" id="purchasebillno" name="purchasebillno" >
											<option value=""></option>
										</select>
									</div>
								<div class="large-12  medium-12 small-12 columns categorytreediv" id="categorydivhid">
									<label>Category</label>
									<?php 
										$mandlab = "";
										$tablename = 'category' ;
										$mandatoryopt = ""; //mandatory option set
										echo divopen( array("id"=>"dl-promenucategory","class"=>"dl-menuwrapper","style"=>"z-index:2;margin-bottom: 1rem") );
										$type="button";
										echo '<button name="treebutton" type="button" id="prodcategorylistbutton" class="btn dl-trigger treemenubtnclick" tabindex="" style="height:2.2rem;color:#ffffff;font-size: 1.45rem;padding: 0.1rem;padding-top:0.3rem;background-color:#546e7a;"><i class="material-icons">format_indent_increase</i></button>';
										$txtoptions = array("style"=>"width:83%;display:inline;position:absolute;height:2.12rem;padding-left: 5px;","readonly"=>"readonly","tabindex"=>"","class"=>"");
										echo text('prodparentcategoryname','',$txtoptions);
										echo '<ul class="dl-menu maintreegenerate large-12 medium-6 small-12 column" id="prodcategorylistuldata">';
										$dataset = $this->Lot_model->jewel_treecreatemodel($tablename);
										$this->Basefunctions->createTree($dataset,0);
										echo close('ul');
										echo close('div');
									?>							
									<input type="hidden" id="proparentcategoryid" name="proparentcategoryid" value="">	
								</div>
								<div class="static-field large-12  medium-12 small-12  columns" id="productdivid">
									<label>Product</label>	
									<select id="product" name="product" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="111">
										<option data-categoryid="1" data-purityid="" data-stone='No' value="">Select</option>
										<?php 
											foreach($product->result() as $key):
										?>
										<option value="<?php echo $key->productid;?>" data-categoryid="<?php echo $key->categoryid;?>" data-purityid="<?php echo $key->purityid;?>" data-stone="<?php echo $key->stoneapplicable; ?>">
										<?php if($mainlicense['productidshow'] == 'YES') { echo $key->productid.' - '.$key->productname; } else { 
										echo $key->productname; } ?></option>
										<?php endforeach;?>
									</select>
								</div>					
								<div class="clearlotdata">
								<div class="static-field large-6  medium-6 small-6  columns" id="purityiddivid">
									<label>Purity</label>						
									<select id="purityid" name="purityid" class="chzn-select" data-prompt-position="topLeft:14,36">
									<option value=""></option> 
									<?php $prev = ' ';
									foreach($purity->result() as $key):
									$cur = $key->metalid;
									$a="";
									if($prev != $cur )
									{
										if($prev != " ")
										{
											 echo '</optgroup>';
										}
										echo '<optgroup  label="'.$key->metalname.'">';
										$prev = $key->metalid;
									}
									?>
									<option value="<?php echo $key->purityid;?>"><?php echo $key->purityname;?></option>
									<?php endforeach;?>
									</select>
								</div>
								<div class="input-field small-6 columns" id="caratweight_div">
									<input type="text" class="validate[required,decval[<?php echo $dia_weight_round; ?>],custom[number],funcCall[checktagweight],min[0.1],maxSize[15]] weightcalculate" id="caratweight" data-calc="" name="caratweight" value="" tabindex="">
									<label for="caratweight">Carat Weight<span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="input-field small-6 columns" id="grossweight_div">
									<input type="text" class="validate[required,decval[<?php echo $weight_round; ?>],custom[number],funcCall[checktagweight],min[0.1],maxSize[15]] weightcalculate" id="grossweight" data-calc="GWT" name="grossweight" value="" tabindex="">
									<label for="grossweight">Gross Weight<span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="input-field small-6 columns" id="stoneweight_div">	
									<input type="text" class="weightcalculate" data-calc="SWT" data-validategreat="grossweight" id="stoneweight" name="stoneweight" value="" tabindex="">
									<label for="stoneweight">Stone Weight<span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="static-field small-6 columns" id="stoneapplicablediv"> 
									<label>Stone Applicable<span class="mandatoryfildclass">*</span></label>								
									<select id="stoneapplicable" name="stoneapplicable" class="chzn-select validate[required]" data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="102">
										<option value="No">No</option>
										<option value="Yes">Yes</option>
									</select>
						        </div>
								<div class="input-field small-6 columns" id="netweight_div">
									<input type="text" class="weightcalculate" data-calc="NWT" data-validategreat="grossweight" id="netweight" name="netweight" value="" tabindex="" readonly>
									<label for="netweight">Net Weight<span class="mandatoryfildclass">*</span></label>
								</div>
								<?php if($lotpieces == 'Yes') { ?>
								<div class="input-field small-6 columns"> 
								<input type="text" id="pieces" name="pieces" value="" tabindex="116" class="validate[required,funcCall[checktagpieces],custom[onlyWholeNumber],maxSize[10],min[1]]">
							     <label for="pieces">Pieces<span class="mandatoryfildclass">*</span></label>
						        </div>
						        <?php } ?>
								<div class="static-field small-6 columns" id="accountiddivid">
									<label>Vendor</label>						
									<select id="accountid" name="accountid" class="chzn-select" data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="107">
									<option value="">Select</option>
									<?php $prev = ' ';
									foreach($account->result() as $key):
									$cur = $key->accounttypeid;
									$a="";
									if($prev != $cur )
									{
										if($prev != " ")
										{
											 echo '</optgroup>';
										}
										echo '<optgroup  label="'.$key->accounttypename.'">';
										$prev = $key->accounttypeid;
									}
									?>
									<option value="<?php echo $key->accountid;?>"><?php echo $key->accountname;?></option>
									<?php endforeach;?>
									</select>
								</div>
								<div class="input-field large-12 columns">
									<textarea name="description" class="materialize-textarea validate[maxSize[200]]" id="description" rows="3" cols="40"></textarea>
									<label for="description">Description</label>
								</div>
								</div>	
								</div>
								<div class="divider large-12 columns"></div>
								<div class="large-12 columns sectionalertbuttonarea" style="text-align:right;padding-bottom: 15px;">
									<input type="button" class="alertbtnyes addkeyboard" id="lotcreate" name="" value="Save" tabindex="">
									<input type="button" class="alertbtnyes updatekeyboard" id="lotupdate" name="" value="Save" tabindex="" style="display: none;">	
									<input type="button" class="alertbtnno addsectionclose"  value="Close" name="cancel" tabindex="">
									<input type ="hidden" id="tagweight"/>
									<input type ="hidden" id="tagnetweight"/>
									<input type ="hidden" id="tagstoneweight"/>
									<input type ="hidden" id="tagsumpieces"/>
								</div>
							</div>
						</div>
						<input type="hidden" id="lotprimaryid" name="lotprimaryid"/>
						<input type ="hidden" id="lotsortcolumn" name="lotsortcolumn"/>
						<input type ="hidden" id="lotsortorder" name="lotsortorder"/>
						<input type ="hidden" id="suppliernumber" name="suppliernumber"/>
						<input type="hidden" id="salesdetailid" name="salesdetailid" value="">
					</div>
					</div>	
				</div>	
			</form>
			</div>
		</div>
	</div>
</div>
<div class="large-6 columns" style="position:absolute;">
	<div class="overlay" id="purchasedetailoverlay" style="overflow-y: auto;overflow-x: hidden;z-index:40;">		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="large-10 medium-10 small-11 large-centered medium-centered small-centered columns">
			<span id="purchaseformconditionvalidation" class="validationEngineContainer">
				<div class="large-12 columns paddingzero borderstyle">
					<div class="large-12 columns sectionheaderformcaptionstyle">Purchase Detail</div>
					<?php
						$device = $this->Basefunctions->deviceinfo();
						if($device=='phone') {
							echo '<div class="large-12 columns paddingzero forgetinggridname" id="purchasedetailgridwidth"><div id="purchasedetailgrid" class=" inner-row-content inner-gridcontent" style="max-width:2000px; height:240px;top:0px;">
								<!-- Table header content , data rows & pagination for mobile-->
							</div>
							<!--<footer class="inner-gridfooter footercontainer" id="purchasedetailgridfooter">
								 Footer & Pagination content 
							</footer>--></div>';
						} else {
							echo '<div class="large-12 medium-12 small-12 columns forgetinggridname" id="purchasedetailgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="purchasedetailgrid" style="max-width:2000px; height:240px;top:0px;">
							<!-- Table content[Header & Record Rows] for desktop-->
							</div>
							<!-- <div class="inner-gridfooter footer-content footercontainer" id="purchasedetailgridfooter">
								Footer & Pagination content 
							</div>--></div>';
						}
					?>
					<div class="large-12 columns" style="background:#fff">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
							<span class="firsttab" tabindex="1000"></span>
							<input type="button" id="purchasedetailsubmit" name="" value="Submit" class="alertbtnyes" tabindex="1001">	
							<input type="button" id="purchasedetailclose" name="" value="Cancel" tabindex="1002" class="alertbtnno  alertsoverlaybtn" >
							<span class="lasttab" tabindex="1003"></span>
						</div>
					</div>
				</div>

			</span>
		</div>
	</div>
</div>
<!-- Error Gross Weight Overlay-->
<div class="large-12 columns large-offset-4" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="errorweightoverlay" style="overflow-y: scroll;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>
		<div class="">
		<?php 
		if($device=='phone') { ?>
			<div class="alert-panel" style="background-color:#465a63;position:relative;left:26% !important;">
		<?php  } else { ?>
		<div class="alert-panel" style="background-color:#465a63;position:relative;left:40% !important;">
		<?php } ?>
				<form method="POST" name="errorweightform"  id="errorweightform" action="" enctype="" class="validationEngineContainer errorweightform">
					<span id="errorweightformvalidate" class="validationEngineContainer"> 
						<span id="errorweightformvalidate" class="validationEngineContainer"> 
						<div class="alertmessagearea">
						<div class="alert-title">Error Weight Form</div>
						<span class="firsttab" tabindex="999"></span>
						<div class="large-12" id="errorgrossweight_div">
							<label>Gross Weight<span class="mandatoryfildclass">*</span></label>
							<input type="text" id="errorgrossweight" name="errorgrossweight" data-validation-engine="" class="" value="" tabindex="1000" >
						</div>
						<div class="large-12" id="errornetweight_div">
							<label>Net Weight<span class="mandatoryfildclass">*</span></label>
							<input type="text" id="errornetweight" name="errornetweight" data-validation-engine="" class="" value="" tabindex="1001" >
						</div>
						<div class="large-12" id="errorcaratweight_div">
							<label>Carat Weight<span class="mandatoryfildclass">*</span></label>
							<input type="text" id="errorcaratweight" name="errorcaratweight" data-validation-engine="" class="" value="" tabindex="1002" >
						</div>
						<div class="large-12" id="errorpieces_div">
							<label>Pieces<span class="mandatoryfildclass">*</span></label>
							<input type="text" id="errorpieces" name="errorpieces" data-validation-engine="validate[required,custom[number],maxSize[10]]" class="" value="" tabindex="1003" >
						</div>
						<div class="large-12">
							<label>Password<span class="mandatoryfildclass">*</span></label>
							<input type="password" id="vadminpassword" name="vadminpassword" data-validation-engine="validate[]" class="" value="" tabindex="1004" >
						</div>
						</div>
						<div class="alertbuttonarea">
							<input type="button" class="alertbtnyes" value="Submit" name="formerrorweightsave" tabindex="1001" id="formerrorweightsave">
							<input type="button" class="flloop alertbtnno" value="Cancel" name="formerrorweightcancel" tabindex="1002" id="formerrorweightcancel">
							<span class="lasttab" tabindex="1003"></span>
						</div>
						<!-- hidden fields -->
						<input type="hidden" id="adminpassword" name="adminpassword" value="<?php echo $adminpassword; ?>">
						</span>
					</span>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
$('#closelotform').click(function(){
	window.location =base_url+'Itemtag';
});
</script>