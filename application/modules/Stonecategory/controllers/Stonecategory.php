
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Stonecategory extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Base/Basefunctions');
		$this->load->model('Stonecategory/Stonecategorymodel');	
		$this->load->model('Base/Crudmodel');		
    }
    //first basic hitting view
    public function index()
    {
		$moduleid = array(47);
		sessionchecker($moduleid);
		$data['filedmodids'] = $moduleid;
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['actionassign'] = $this->Basefunctions->actionassign($moduleid);
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($moduleid);
		$data['stonecategorylevel'] = $this->Basefunctions->get_company_settings('stone_level'); //stone level
		$data['stonetype']=$this->Basefunctions->simpledropdown('stonetype','stonetypeid,stonetypename','stonetypeid');
		$this->load->view('Stonecategory/stonecategoryview',$data);
    }
    //stone create
    public function stonecategorycreate()
    {
    	$this->Stonecategorymodel->stonecategorycreate();
    }
    /*category treegrid reloading*/
    public function categorytreedatafetchfun()
    {
    	$tablename = 'stonecategory' ;
    	echo '<button class="btn dl-trigger fa fa-outdent" tabindex="102" style="height:2.2rem;color:#ffffff;font-size: 1.45rem;padding: 0.1rem;padding-top:0.1rem;background-color:#546e7a;" id="categorylistbutton"><i class="material-icons">format_indent_increase</i></button>';
    	echo '<input type="text" style="width:83%;display:inline;position:absolute;height:2.12rem;padding-left: 5px;color:#ffffff" value="" id="prodparentcategoryname" readonly>';
    	echo '<ul class="dl-menu maintreegenerate  large-12 medium-6 small-6 columns" id="prodcategorylistuldata">';
    	$dataset = $this->Basefunctions->jewel_treecreatemodel($tablename);
    	$this->Basefunctions->createTree($dataset,0);
    	echo '</ul>';
    }
    /*stone category treegrid reloading based on stone type*/
    public function stonecategorytreedatafetchfun()
    {
    	$tablename = 'stonecategory' ;
    	$manopt = $_POST['mandval'];
    	$lablname = 'Parent Category';
    	$stonetype = $_POST['stonetype'];
    	$mandlab = "";
    	if($manopt!='') {
    		$mandlab = "<span class='mandatoryfildclass'>*</span>";
    	}
    	echo '<label>'.$lablname.$mandlab.'</label>';
    	echo divopen( array("id"=>"dl-promenucategory","class"=>"dl-menuwrapper","style"=>"z-index:2;margin-bottom: 1rem") );
    	echo '<button name="treebutton" type="button" id="prodcategorylistbutton" class="btn dl-trigger treemenubtnclick" tabindex="102" style="height:2.2rem;color:#ffffff;font-size: 1.45rem;padding: 0.1rem;padding-top:0.3rem;background-color:#546e7a;"><i class="material-icons">format_indent_increase</i></button>';
		$txtoptions = array("style"=>"width:83%;display:inline;position:absolute;height:2.12rem;padding-left: 5px;","readonly"=>"readonly","tabindex"=>"101","class"=>"".$manopt."");
    	echo text('prodparentcategoryname','',$txtoptions);
    	echo hidden('proparentcategoryid','');
    	echo '<ul class="dl-menu maintreegenerate large-12 medium-6 small-12 column" id="prodcategorylistuldata">';
    	$dataset = $this->Stonecategorymodel->stonetreecreatemodel($tablename,$stonetype);
    	$dataset = $this->Basefunctions->createTree($dataset,0);
		$this->createTree($dataset,0);
    	echo close('ul');
    	echo close('div');
		echo '<input type="hidden" value="'.$manopt.'" id="treevalidationcheck" name="treevalidationcheck">';
		echo '<input type="hidden" value="'.$lablname.'" id="treefiledlabelcheck" name="treefiledlabelcheck">';
    }
    public function stonecategoryretrive(){
    	$this->Stonecategorymodel->stonecategoryretrive();
    }
    public function stonecategoryupdate(){
    	$this->Stonecategorymodel->stonecategoryupdate();
    }
    public function stonecategorydelete(){
    	$id=$_GET['primaryid'];
    	$this->Stonecategorymodel->stonecategorydelete($id);
    }
	//reload the category list element
	public function getcounterparentid() {
		$this->Stonecategorymodel->getcategoryparent();
	}
	//tree creation
	public function createTree($items = array(), $parentId = 0,$removeid = 0) {
		echo " <ul class='dl-submenu'> ";
		if($removeid == 0){
			echo '<li><a href="#" style="font-weight:bold;">Remove<i class="material-icons parentclear">close</i></a></li>';
		}
		if($items && count($items) > 0 ) {
			foreach ($items[$parentId] as $item) {
				echo '<li data-listname="'.$item['name'].'" data-level="'.$item['level'].'" data-listid="'.$item['id'].'" ><a href="#">'.$item['name'].'</a>';//level title
				$curId = $item['id'];
				//if there are children
				if (!empty($items[$curId])) {
					$this->createTree($items, $curId,1);
				}
				echo '</li>';
			}
		}
		echo '</ul>';
	}
}