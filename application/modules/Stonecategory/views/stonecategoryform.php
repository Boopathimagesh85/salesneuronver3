<div id="stonecategoryview" class="gridviewdivforsh">
	<?php
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['gridid'] = 'stonegrid';
		$dataset['gridwidth'] = 'stonegridwidth';
		$dataset['gridfooter'] = 'stonegridfooter';
		$dataset['moduleid'] = '47';
		$this->load->view('Base/singlemainviewformwithgrid.php',$dataset);
		$defaultrecordview = $this->Basefunctions->get_company_settings('mainviewdefaultview');
		echo hidden('mainviewdefaultview',$defaultrecordview);
	?>
</div>
<div id="stonecategoryformdiv" class="singlesectionaddform">
<div class="large-12 columns addformunderheader">&nbsp; </div>
<div class="large-12 columns scrollbarclass addformcontainer">
	<div class="row mblhidedisplay">&nbsp;</div>
	<div id="subformspan1" class=""> 
	<div class="closed effectbox-overlay effectbox-default" id="stonecategorysectionoverlay">
			<div class="large-12 columns mblnopadding" style="padding-left:0px !important;">
				<form method="POST" name="stoneform" class="stoneform"  id="stoneform" style="height: 100% !important;">
					<div id="stoneaddwizard" class="validationEngineContainer" style="height: 100% !important;">		
					<div id="stoneeditwizard" class="validationEngineContainer" style="height: 100% !important;">				
						<div class="large-4 large-offset-8 columns" style="padding-right:0px !important;height: 100% !important;padding-left:0px !important;">
							<div class="large-12 columns cleardataform border-modal-8px" style="padding-left: 0 !important; padding-right: 0 !important;">
								<div class="large-12 columns sectionheaderformcaptionstyle" >Stone Category</div>
								<div class="large-12 columns sectionpanel">
								<div class="static-field large-12 columns ">
									<label>Stone Type<span class="mandatoryfildclass">*</span></label>								
									<select id="stonetype" name="stonetype" class="chzn-select validate[required]"  data-prompt-position="topLeft:14,36" tabindex="100">
										<option value=""></option>
										<?php foreach($stonetype as $key):?>
										<option value="<?php echo $key->stonetypeid;?>">
											<?php echo $key->stonetypename;?></option>
									<?php endforeach;?>		
									</select>
								</div>
								<input id="maxcategorylevel" value="<?php echo $stonecategorylevel; ?>" type="hidden"/>
								<div class="large-12  medium-12 small-12 columns categorytreediv" id="categorydivhid">
									<label>Parent Category</label>
									<?php 
									$mandlab = "";
									$tablename = 'stonecategory' ;
									$mandatoryopt = ""; //mandatory option set
									echo divopen( array("id"=>"dl-promenucategory","class"=>"dl-menuwrapper","style"=>"z-index:2;margin-bottom: 1rem") );
									$type="button";
									echo '<button name="treebutton" type="button" id="prodcategorylistbutton" class="btn dl-trigger treemenubtnclick" tabindex="" style="height:2.2rem;color:#ffffff;font-size: 1.45rem;padding: 0.1rem;padding-top:0.3rem;background-color:#546e7a;"><i class="material-icons">format_indent_increase</i></button>';
									$txtoptions = array("style"=>"width:83%;display:inline;position:absolute;height:2.12rem;padding-left: 5px;","readonly"=>"readonly","tabindex"=>"","class"=>"");
									echo text('prodparentcategoryname','',$txtoptions);
									echo '<ul class="dl-menu maintreegenerate large-12 medium-6 small-12 column" id="prodcategorylistuldata">';
									$dataset = $this->Basefunctions->jewel_treecreatemodel($tablename);
									$this->Basefunctions->createTree($dataset,0);
									echo close('ul');
									echo close('div');
									?>							
									<input type="hidden" id="proparentcategoryid" name="proparentcategoryid" value="">
								</div>
								<div class="input-field large-6 columns ">		
									<input type="text" class="validate[required,maxSize[100],funcCall[stonecategorycheck]]" id="stonecategoryname" name="stonecategoryname" value="" tabindex="103"> 
									<label for="stonecategoryname">Category Name <span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="input-field large-6 columns ">
									<input type="text" class="validate[maxSize[50],funcCall[stoneshortnamecheck]]" name="shortname" id="shortname" tabindex="104">
									<label for="shortname">Short Name</label>	
								</div>
								<div class="input-field large-6 columns">
									<input type="checkbox" class="checkboxcls radiochecksize filled-in" tabindex="104"   name="loosestonecboxid" id="loosestonecboxid" data-hidname="loosestonecbox" value="">
									<label for="loosestonecboxid">Loose Stone</label>
									<input name="loosestonecbox" type="hidden" id="loosestonecbox" value="" data-defvalattr="No">
								</div>
								<div class="input-field large-12 columns">
									<textarea class="materialize-textarea validate[maxSize[200]]"  name="description" id="description" tabindex="105"></textarea>
									<label for="description">Description</label>
								</div>
								<div class="input-field large-6 columns end hidedisplay">
										<input type="checkbox" class="filled-in checkboxcls radiochecksize" tabindex="106" name="setdefaultcboxid" id="setdefaultcboxid" data-hidname="setdefault" value="">
										<label for="setdefaultcboxid">Default</label>
										<input name="setdefault" type="hidden" id="setdefault" value="" data-defvalattr="No">
							       </div>
							    </div>
							    <div class="divider large-12 columns"></div>
								<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">
									<input type="hidden" id="stonecategoryprimaryid" name="stonecategoryprimaryid"/>
									<input type="hidden" id="stonesortcolumn" name="stonesortcolumn" value=""/>
									<input type="hidden" id="stonesortorder" name="stonesortorder" value=""/>
								    <input type="hidden" id="stonecategoryprimaryname" name="stonecategoryprimaryname"/>
								    <input type="hidden" id="stonecategoryshortprimaryname" name="stonecategoryshortprimaryname"/>
									<input type="button" class="alertbtnyes" id="stonecreate" name="" value="Save" tabindex="107">
									<input type="button" class="alertbtnyes" id="stoneupdate" name="" value="Save" tabindex="108" style="display: none">
									<input type="button" class="alertbtnno addsectionclose"  value="Close" name="cancel" tabindex="109">
								</div>
							</div>
						</div>	
					</div>	
					</div>	
				</form>
			</div>
		</div>
	</div>
</div>
<script>
$('#closeproductmasterform').click(function(){
			window.location =base_url+'Stone';
		});		
	</script>
</html>