<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('Base/headerfiles'); ?>
	<?php $this->load->view('Base/basedeleteform'); ?>
	<?php 
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$this->load->view('Base/overlaymobile');
		} else {
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
	?>
</head>
<body>
	<div class="row" style="max-width:100%">
		<div class="off-canvas-wrap" data-offcanvas>
			<div class="inner-wrap">
				<!-- Add Form -->
				<div id="stonecategoryformdiv" class="">
					<?php $this->load->view('stonecategoryform'); ?>
				</div>
				<!-- close the off-canvas menu -->
				<a class="exit-off-canvas"></a>									
			</div>
		</div>
	</div>
	<?php
		$this->load->view('Base/basedeleteformforcombainedmodules');
	?>	
</body>
<div class="large-12 columns" style="position:absolute;"><div class="overlay" id="viewdeleteoverlayconform" style="z-index:120;"></div></div>
<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
<?php $this->load->view('Base/bottomscript'); ?>
<script src="<?php echo base_url();?>js/plugins/treemenu/modernizr.custom.min.js" type="text/javascript"></script>  
<script src="<?php echo base_url();?>js/plugins/treemenu/jquery.dlmenu.js" type="text/javascript"></script> 
<?php include 'js/Stonecategory/stonecategory.php' ?>		
</html>