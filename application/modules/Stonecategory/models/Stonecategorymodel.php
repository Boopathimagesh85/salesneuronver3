<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Stonecategorymodel extends CI_Model
{
   private $stonemoduleid = 47;	
	public function __construct()
    {
        parent::__construct();
		$this->load->model( 'Base/Basefunctions');
		$this->load->model( 'Base/Crudmodel');
    }
    public function stonecategorycreate(){ // stone category
    	$stone_level = $this->Basefunctions->get_company_settings('stone_level'); //stone_level
    	$level  =0;
		$loosestone = 'No';
    	if($_POST['proparentcategoryid'] > 0){
    		$level = $this->gettreelevel($_POST['proparentcategoryid'],'stonecategory');
    	} else {
    		$level = 0;
    	}
    	if($level <= $stone_level)
    	{
			if($_POST['loosestonecbox'] == 'Yes') {
				$loosestone = 'Yes';
			}
			if($_POST['setdefault']=='Yes'){
			$query = $this->db->query("SELECT GROUP_CONCAT(stonecategoryid) as stonecategoryid  FROM stonecategory where stonetypeid=".$_POST['stonetype']." and status=1");
		    $result=$query->row();
			$stonecategoryid=explode(',',$result->stonecategoryid);
			$update_last=array(
						'default'=>'No',
						);
			$update_last = array_merge($update_last,$this->Crudmodel->updatedefaultvalueget());
			$this->db->where_in('stonecategoryid',$stonecategoryid);
			$this->db->update('stonecategory',array_filter($update_last));
			}
			$insert=array(
					'stonetypeid'=>$_POST['stonetype'],
					'stonecategoryname'=>$_POST['stonecategoryname'],
					'parentstonecategoryid'=>$_POST['proparentcategoryid'],
					'stonecategorylevel'=>$level,
					'shortname'=>$_POST['shortname'],
					'loosestone'=>$loosestone,
					'description'=>$_POST['description'],
					'default'=>$_POST['setdefault'],
					'industryid'=>$this->Basefunctions->industryid,
					'status'=>$this->Basefunctions->activestatus,
					'branchid'=>$this->Basefunctions->branchid
					);
			$insert = array_merge($insert,$this->Crudmodel->defaultvalueget());
			$this->db->insert('stonecategory',array_filter($insert));
			$primaryid = $this->db->insert_id();
			if($loosestone == 'Yes') {
				if($_POST['proparentcategoryid'] == 0) {
					$catgparentid = 0;
				} else {
					$catgparentid = $this->Basefunctions->singlefieldfetch('categoryid','stonecategoryid','stonecategory',$_POST['proparentcategoryid']);
				}
				$categoryinsert = array(
					'categoryname'=>$_POST['stonecategoryname'],
					'categorysuffix'=>$_POST['shortname'],
					'categorysortorder'=>0,
					'loosestone'=>$loosestone,
					'description'=>$_POST['description'],
					'parentcategoryid'=>$catgparentid,
					'categorylevel'=>$level,
					'parentproductid'=>1,
					'gstrates'=>'No',
					'industryid'=>$this->Basefunctions->industryid,
					'status'=>$this->Basefunctions->activestatus,
					'branchid'=>$this->Basefunctions->branchid
				);
				$categoryinsert = array_merge($categoryinsert,$this->Crudmodel->defaultvalueget());
				$this->db->insert('category',array_filter($categoryinsert));
				$catgprimaryid = $this->db->insert_id();

				// Update Supercategoryid in Category table
				if($_POST['proparentcategoryid'] == 0) {
					$updatesupercatgid = array(
						'supercategoryid'=>$catgprimaryid,
					);
				} else {
					$getsupercatgid = $this->Basefunctions->singlefieldfetch('supercategoryid','categoryid','category',$catgparentid);
					$updatesupercatgid = array(
						'supercategoryid'=>$getsupercatgid,
					);
				}
				$updatesupercatgid = array_merge($updatesupercatgid,$this->Crudmodel->updatedefaultvalueget());
				$this->db->where_in('categoryid',$catgprimaryid);
				$this->db->update('category',array_filter($updatesupercatgid));
				
				// Update Category Primary Id in Stonecategory table
				$updatecatgid = array(
					'categoryid'=>$catgprimaryid,
				);
				$updatecatgid = array_merge($updatecatgid,$this->Crudmodel->updatedefaultvalueget());
				$this->db->where_in('stonecategoryid',$primaryid);
				$this->db->update('stonecategory',array_filter($updatecatgid));

			}
			//audit-log
			$userid = $this->Basefunctions->logemployeeid;
			$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
			$activity = ''.$user.' created stone category - '.$_POST['stonecategoryname'].'';
			$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,$this->stonemoduleid);
			echo 'SUCCESS';
    	}else{
    		echo 'LEVEL';
    	}
    }
    public function stonecategoryupdate()
    {
    	$id=$_POST['primaryid'];
		$stone_level = $this->Basefunctions->get_company_settings('stone_level'); //stone_level
    	$level  =0;
		$loosestone = 'No';
    	if($_POST['proparentcategoryid'] > 0){
    		$level = $this->gettreelevel($_POST['proparentcategoryid'],'stonecategory');
    	} else {
    		$level =1;
    	}
		if($_POST['proparentcategoryid']=='') {
			$_POST['proparentcategoryid']=0;
		}
		if($_POST['setdefault']=='Yes'){
			$query = $this->db->query("SELECT GROUP_CONCAT(stonecategoryid) as stonecategoryid  FROM stonecategory where stonetypeid=".$_POST['stonetype']." and status=1");
		    $result=$query->row();
			$stonecategoryid=explode(',',$result->stonecategoryid);
			$update_last=array(
						'default'=>'No',
						);
			$update_last = array_merge($update_last,$this->Crudmodel->updatedefaultvalueget());
			$this->db->where_in('stonecategoryid',$stonecategoryid);
			$this->db->update('stonecategory',array_filter($update_last));
		}
		if($_POST['loosestonecbox'] == 'Yes') {
			$loosestone = 'Yes';
		}
		if($_POST['proparentcategoryid'] == 0){ // parent category only
			$info=0;
		} else {
			if($id > $_POST['proparentcategoryid']){ // child - parent category
				$info=0;
			} else { // parent - child category
				$info=$this->Basefunctions->singlefieldfetch('COUNT(stonecategoryid)','parentstonecategoryid','stonecategory',$_POST['proparentcategoryid']);
			}
		}
		if($info<=0) {
			$update_last = array(
				'stonetypeid'=>$_POST['stonetype'],
				'stonecategoryname'=>$_POST['stonecategoryname'],
				'parentstonecategoryid'=>$_POST['proparentcategoryid'],
				'stonecategorylevel'=>$level,
				'shortname'=>$_POST['shortname'],
				'description'=>$_POST['description'],
				'industryid'=>$this->Basefunctions->industryid,
				'default'=>$_POST['setdefault'],
				'branchid'=>$this->Basefunctions->branchid
			);
			$update_last = array_merge($update_last,$this->Crudmodel->updatedefaultvalueget());
			$this->db->where('stonecategoryid', $id);
			$this->db->update('stonecategory',$update_last);

			if($loosestone == 'Yes') {
				if($_POST['proparentcategoryid'] == 0) {
					$catgparentid = 0;
				} else {
					$catgparentid = $this->Basefunctions->singlefieldfetch('categoryid','stonecategoryid','stonecategory',$_POST['proparentcategoryid']);
				}
				$categoryid = $this->Basefunctions->singlefieldfetch('categoryid','stonecategoryid','stonecategory',$id);
				$categoryupdate = array(
					'categoryname'=>$_POST['stonecategoryname'],
					'categorysuffix'=>$_POST['shortname'],
					'categorysortorder'=>'0',
					'loosestone'=>$loosestone,
					'description'=>$_POST['description'],
					'parentcategoryid'=>$catgparentid,
					'categorylevel'=>$level,
					'parentproductid'=>1,
					'gstrates'=>'No',
					'industryid'=>$this->Basefunctions->industryid,
					'status'=>$this->Basefunctions->activestatus,
					'branchid'=>$this->Basefunctions->branchid
				);
				$categoryupdate = array_merge($categoryupdate,$this->Crudmodel->updatedefaultvalueget());
				$this->db->where_in('categoryid',$categoryid);
				$this->db->update('category',array_filter($categoryupdate));

				// Update Supercategoryid in Category table
				if($_POST['proparentcategoryid'] == 0) {
					$updatesupercatgid = array(
						'supercategoryid'=>$categoryid,
					);
				} else {
					$getsupercatgid = $this->Basefunctions->singlefieldfetch('supercategoryid','categoryid','category',$catgparentid);
					$updatesupercatgid = array(
						'supercategoryid'=>$getsupercatgid,
					);
				}
				$updatesupercatgid = array_merge($updatesupercatgid,$this->Crudmodel->updatedefaultvalueget());
				$this->db->where_in('categoryid',$categoryid);
				$this->db->update('category',array_filter($updatesupercatgid));
				
				// Update Category Primary Id in Stonecategory table
				$updatecatgid = array(
					'categoryid'=>$categoryid,
				);
				$updatecatgid = array_merge($updatecatgid,$this->Crudmodel->updatedefaultvalueget());
				$this->db->where_in('stonecategoryid',$id);
				$this->db->update('stonecategory',array_filter($updatecatgid));
			}
			//audit-log
			$userid = $this->Basefunctions->logemployeeid;
			$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
			$activity = ''.$user.' updated stone category - '.$_POST['stonecategoryname'].'';
			$this->Basefunctions->notificationcontentadd($id,'Updated',$activity ,$userid,$this->stonemoduleid);
			echo 'SUCCESS';
		} else {
		/*
		1. Check the edited stone category is already mapped as parent category
		*/
				echo "FAILURE";
		}
    }
    public function gettreelevel($id,$table){
    	$info=$this->db->select($table.'level')
    	->from($table)
    	->where($table.'id',$id)
    	->get()
    	->row();
    	$field = $table.'level';
    	return ($info->$field + 1);
    }
    // retrieve stone category
    public function stonecategoryretrive()
    {
    	$id=$_GET['primaryid'];
		$table = 'stone';		   
    	$this->db->select('a.stonecategoryid,a.stonecategoryname,a.stonetypeid,a.parentstonecategoryid,a.shortname,a.description,a.default,a.loosestone,b.stonecategoryname as parentstonecategoryname');
    	$this->db->from('stonecategory as a');
    	$this->db->join('stonecategory as b','b.stonecategoryid=a.parentstonecategoryid','left outer');
    	$this->db->where('a.stonecategoryid',$id);
    	$info=$this->db->get()->row();
    	$jsonarray=array(
    			'stonetype'=>$info->stonetypeid,
    			'proparentcategoryid'=>$info->parentstonecategoryid,
    			'prodparentcategoryname'=>$info->parentstonecategoryname,
    			'stonecategoryname'=>$info->stonecategoryname,
    			'stonecategoryprimaryid'=>$info->stonecategoryid,
    			'stonecategoryprimaryname'=>$info->stonecategoryname,
    			'stonecategoryshortprimaryname'=>$info->shortname,
    			'shortname'=>$info->shortname,
    			'description'=>$info->description,
    			'setdefault'=>$info->default,
				'loosestone'=>$info->loosestone,
				'checkproduct' => $this->Basefunctions->generalinformaion($table,'stonecategoryid','stonecategoryid',$id)	   
       	);
    	echo json_encode($jsonarray);
    }
    public function stonecategorydelete($id)
    {
		$categoryid = $this->Basefunctions->singlefieldfetch('categoryid','stonecategoryid','stonecategory',$id);
    	$delete = $this->Basefunctions->delete_log();
    	$this->db->where('stonecategoryid',$id);
    	$this->db->update('stonecategory',$delete);
		// Category Delete
		$this->db->where('categoryid',$categoryid);
    	$this->db->update('category',$delete);
    	//audit-log
    	$stone_category = $this->Basefunctions->singlefieldfetch('stonecategoryname','stonecategoryid','stonecategory',$id);
    	$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
    	$activity = ''.$user.' deleted stone - '.$stone_category.'';
    	$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,$this->stonemoduleid);
    }
    //stone type based tree create model
    public function stonetreecreatemodel($tablename,$stonetype)
    {
    	$id = $tablename.'id';
    	$name = $tablename.'name';
    	$pdataid = 'parent'.$tablename.'id';
    	$level = $tablename.'level';
    	$data = array();
    	$this->db->select( ''.$id.' AS Id'.','.''.$name.' AS Name'.','.$pdataid.' AS ParentId'.','.$level.' AS level'.','.'loosestone as lstchck');
    	$this->db->from($tablename);
    	$this->db->where(''.$tablename.'.status',1);
    	if($stonetype != ''){
    	  $this->db->where('stonecategory.stonetypeid',$stonetype);
    	}
    	
    	$this->db->order_by(''.$tablename.'.'.$id.'','ASC');
    	$result = $this->db->get();
    	foreach($result->result() as $row) {
    		$data[$row->Id] = array('id'=>$row->Id,'name'=>$row->Name,'pid'=>$row->ParentId,'level'=>$row->level,'lstchck'=>$row->lstchck);
    	}
    	$itemsByParent = array();
    	foreach ($data as $item){
    		if (!isset($itemsByParent[$item['pid']])) {
    			$itemsByParent[$item['pid']] = array();
    		}
    		$itemsByParent[$item['pid']][] = $item;
    	}//
    	return $itemsByParent;
    }
	public function getcategoryparent() {
		$parentcategoryid='';
		$categoryname='';
		$id=$_GET['id'];
		$data=$this->db->select('a.parentstonecategoryid as parentcategoryids,b.stonecategoryname as categorynames')
				->from('stonecategory as a')
				->join('stonecategory as b','a.parentstonecategoryid=b.stonecategoryid')
				->where('a.stonecategoryid',$id)
				->get()->result();
		foreach($data as $in) {
			$parentcategoryid=$in->parentcategoryids;
			$categoryname=$in->categorynames;
		}
		$a=array('parentcategoryid'=>$parentcategoryid,'categoryname'=>$categoryname);
		echo json_encode($a);
	}
}