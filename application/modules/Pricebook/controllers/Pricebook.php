<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pricebook extends MX_Controller{
    public function __construct() {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Pricebook/Pricebookmodel');	
    }
    //first basic hitting view
    public function index() {        
    	$moduleid = array(218);
    	sessionchecker($moduleid);
		/*action*/
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(218);
		$viewmoduleid = array(218);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Pricebook/pricebookview',$data);
	}
	//create price book
	public function newdatacreate() {  
    	$this->Pricebookmodel->newdatacreatemodel();
    }
	//information fetch price book
	public function fetchformdataeditdetails() {
		$moduleid = 218;
		$this->Pricebookmodel->informationfetchmodel($moduleid);
	}
	//update price book
    public function datainformationupdate() {
        $this->Pricebookmodel->datainformationupdatemodel();
    }
	//delete price book
    public function deleteinformationdata() {
		$moduleid = 218;
	   $this->Pricebookmodel->deleteoldinformation($moduleid);
    } 
	//price book name check
	public function pricebooknamecheck() {
		 $this->Pricebookmodel->pricebooknamecheckmodel();
	}
	//currency conversion dd load
	public function currencyddload() {
		$this->Pricebookmodel->currencyddloadmodel();
	}
}