<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pricebookmodel extends CI_Model {
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//price book create
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['pricebookelementsname']);
		$formfieldstable = explode(',',$_POST['pricebookelementstable']);
		$formfieldscolmname = explode(',',$_POST['pricebookelementscolmn']);
		$elementpartable = explode(',',$_POST['pricebookelementspartabname']);
		 //filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
		$primaryid = $this->Basefunctions->maximumid('pricebook','pricebookid');
		//updatethe territory
		if($_POST['territory'] != '' ){		
			$compoundon=array('territoryid'=>trim($_POST['territory']));
			$this->db->where('pricebookid',$primaryid);
			$this->db->update('pricebook',$compoundon);
		}
		//notification entry
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		if(isset($_POST['employeeid'])){
			$assignid = $_POST['employeeid'];if($assignid != ''){$assignid = $assignid;}else{$assignid = 1;}
		}else{$assignid = 1;}
		$pbname = $_POST['pricebookname'];
		if($assignid == '1'){
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch(218,$primaryid,$partablename,2);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,218);
			}
		}else {
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch(218,$primaryid,$partablename,2);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,218);
			}
		}
		echo 'TRUE';
	}
	//Retrieve price book data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['pricebookelementsname']);
		$formfieldstable = explode(',',$_GET['pricebookelementstable']);
		$formfieldscolmname = explode(',',$_GET['pricebookelementscolmn']);
		$elementpartable = explode(',',$_GET['pricebookelementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['pricebookprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		//retrieve TERRITORY data
		$mdata=$this->db->query("SELECT territoryid FROM pricebook
						  WHERE pricebookid = $primaryid");
		foreach($mdata->result() as $info){
			$compounddata['territoryid']=$info->territoryid;
		}
		$result = json_decode($result,true);
		$cresult= array_merge($result,$compounddata);		
		$result=json_encode($cresult);	
		echo $result;
	}
	//price book update
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['pricebookelementsname']);
		$formfieldstable = explode(',',$_POST['pricebookelementstable']);
		$formfieldscolmname = explode(',',$_POST['pricebookelementscolmn']);
		$elementpartable = explode(',',$_POST['pricebookelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['pricebookprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		//updatethe territory
		if($_POST['territory'] != '' ){		
			$compoundon=array('territoryid'=>trim($_POST['territory']));
			$this->db->where('pricebookid',$primaryid);
			$this->db->update('pricebook',$compoundon);
		}
		//notification entry
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		if(isset($_POST['employeeid'])){
			$assignid = $_POST['employeeid'];if($assignid != ''){$assignid = $assignid;}else{$assignid = 1;}
		}else{$assignid = 1;}
		$pbname = $_POST['pricebookname'];
		if($assignid == '1'){
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch(218,$primaryid,$partablename,3);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,218);
			}
		}else {
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch(218,$primaryid,$partablename,3);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,218);
			}
		}
		echo $result;
	}
	//price book delete
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['pricebookelementstable']);
		$parenttable = explode(',',$_GET['pricebookelementspartable']);
		$id = $_GET['pricebookprimarydataid'];
		$filename = $this->Basefunctions->generalinformaion('pricebook','pricebookname','pricebookid',$id);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=array('pricebookdetail');
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				//deleted file name
				$notimsg = $this->Basefunctions->notificationtemplatedatafetch(218,$id,$partabname,4);
				if($notimsg != '') {
					$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,1,218);
				}
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				echo 'TRUE';
			}
		} else {
			//deleted file name
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch(218,$id,$partabname,4);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,1,218);
			}
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			echo 'TRUE';
		}
	} 
	//price book name check 
	public function pricebooknamecheckmodel() {
		$name = $_GET['pbname'];
		$this->db->select('pricebookname');
		$this->db->from('pricebook');
		$this->db->where('pricebook.pricebookname',$name);
		$this->db->where('pricebook.status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			echo "TRUE";
		} else {
			echo "False";
		}
	}
	//currency dd load
	public function currencyddloadmodel() {
		$i=0;
		$this->db->select('currencyid,CONCAT(currencycountry, " - ",currencyname) as currency',false);
		$this->db->from('currency');
		$this->db->where('currency.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->currencyid,'dataname'=>$row->currency);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
}