<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('Base/headerfiles'); ?>	
</head>
<body>
<?php
		$dataset['gridenable'] = "1"; //0-no  1-yes
		$dataset['griddisplayid'] = "pricebookaddformview"; //add form-div id
		$dataset['maingridtitle'] = $gridtitle['title'];   // form header
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];  //grid header
		$dataset['spanattr'] = array();     //
		$dataset['gridtableid'] = "pricebookaddgrid"; //grid id
		$dataset['griddivid'] = "pricebookaddgridnav"; //grid pagination
		$this->load->view('pricebookform',$dataset); 
		$this->load->view('Base/basedeleteform');
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$this->load->view('Base/overlaymobile');
		} else {
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
?>	
</body>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js Files -->	
	<script src="<?php echo base_url();?>js/Pricebook/pricebook.js" type="text/javascript"></script> 
</html>
 
