<?php if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );
//Controller Class
class Branch extends MX_Controller{
	public function __construct() {
		parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Branch/Branchmodel');
		$this->load->view('Base/formfieldgeneration');
	}
	//Default Function
	public function index() {
		$moduleid = array(3);
		sessionchecker($moduleid);
		//action
		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(3);
		$viewmoduleid = array(3);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);	
		$data['defaultstateid']=$this->Basefunctions->singlefieldfetch('stateid','branchid','branch',$this->Basefunctions->branchid);
		$data['defaultcountryid']=$this->Basefunctions->singlefieldfetch('countryid','branchid','branch',$this->Basefunctions->branchid);
		$this->load->view('Branch/branchview',$data);
	}
	//Create New data
	public function newdatacreate() {  
    	$this->Branchmodel->newdatacreatemodel();
    }
	//information fetch
	public function fetchformdataeditdetails() {
		$moduleid = array(3);
		$this->Branchmodel->informationfetchmodel($moduleid);
	}
	//Update old data
    public function datainformationupdate() {
        $this->Branchmodel->datainformationupdatemodel();
    }
}