<?php if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );
//Model to Perform Insert Update Read and Delete Operations
class Branchmodel extends CI_Model{
	public function __construct() {
		parent::__construct();
		$this->load->model('Base/Basefunctions');	
		$this->load->model('Base/Crudmodel');
	}
	//To Create New Company,Branch Details
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		//Restrict branch creation based on plan - table checking on jeweldb [billinginformation]
		$branchcreation='0';
		$mcid = $this->usercompanyid();
		$this->db->select('billinginformationid,billingbranch');
		$this->db->from('billinginformation');
		$this->db->where('billinginformation.mastercompanyid',$mcid);
		$purbranchcount=$this->db->get();
		foreach($purbranchcount->result() as $branchdata) {
			$branchcreation=$branchdata->billingbranch;
		}
		//check created branch count
		$this->db->select('branchid');
		$this->db->from('branch');
		$this->db->where('branch.status',1);
		$createdbranchcount=$this->db->get();
		$branchcounts = $createdbranchcount->num_rows();
		if($branchcreation!=0 && $branchcreation>$branchcounts) {
			$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
			$primaryid = $this->db->insert_id();
			//audit-log
			$userid = $this->Basefunctions->logemployeeid;
			$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
			$activity = ''.$user.' created Branch - '.$_POST['branchname'].'';
			$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,3);
			echo 'TRUE';
		} else {
			echo 'Branch Creation limit is exist. Please buy more branch.';
		}
	}
	//information fetch
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['dataprimaryid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	//update data information
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['primarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' created Branch - '.$_POST['branchname'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,3);
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' updated Branch - '.$_POST['branchname'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$activity ,$userid,3);
		echo $result;
	}
	//subscriber master company
	public function usercompanyid()	{
		$mcid = 1;
		$empid = $this->Basefunctions->userid;
		$compquery = $this->db->query('select company.companyid,company.mastercompanyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.'')->result();
		foreach($compquery as $key) {
			$mcid = $key->mastercompanyid;
		}
		return $mcid;
	}
}