<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cashcountermodel extends CI_Model{
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//create counter
	public function newdatacreatemodel() {
		//table and fields information	
		$formfieldsname = explode(',',$_POST['cashcounterelementsname']);
		$formfieldstable = explode(',',$_POST['cashcounterelementstable']);
		$formfieldscolmname = explode(',',$_POST['cashcounterelementscolmn']);
		$elementpartable = explode(',',$_POST['cashcounterelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
		$primaryid = $this->db->insert_id();
		$password = $_POST['password'];
		$userpassword = preg_replace('/[^A-Za-z0-9]/', '',$password);
		$userpassword = strtolower($userpassword);
		$hash = password_hash($userpassword, PASSWORD_DEFAULT);
		$hasharray=array('password'=>$hash);
		$this->db->where('cashcounterid',$primaryid);
		$this->db->update('cashcounter',$hasharray);
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity = ''.$user.' created CashCounter - '.$_POST['cashcountername'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Created',$activity ,$userid,111);
		echo 'TRUE';
	}
	//retrive counter data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['cashcounterelementsname']);
		$formfieldstable = explode(',',$_GET['cashcounterelementstable']);
		$formfieldscolmname = explode(',',$_GET['cashcounterelementscolmn']);
		$elementpartable = explode(',',$_GET['cashcounterelementspartabname']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['cashcounterprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	//update counter
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['cashcounterelementsname']);
		$formfieldstable = explode(',',$_POST['cashcounterelementstable']);
		$formfieldscolmname = explode(',',$_POST['cashcounterelementscolmn']);
		$elementpartable = explode(',',$_POST['cashcounterelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['cashcounterprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		//password insertion
		$password = $_POST['password'];
		$oldpassword = $_POST['oldpassword'];
		if($password != $oldpassword) {
			$userpassword = preg_replace('/[^A-Za-z0-9]/', '',$password);
			$userpassword = strtolower($userpassword);
			$hash = password_hash($userpassword, PASSWORD_DEFAULT);
			$hasharray=array('password'=>$hash);
			$this->db->where('cashcounterid',$primaryid);
			$this->db->update('cashcounter',$hasharray);
		}
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$cashcountername = $this->Basefunctions->singlefieldfetch('cashcountername','cashcounterid','cashcounter',$primaryid); //Cash counter name
		$activity = ''.$user.' updated CashCounter - '.$cashcountername.'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$activity ,$userid,111);
		echo $result;
	}
	//check this counter is a parent counter or not
	public function checkparentcashcounter($primaryid) {
		$this->db->select('cashcounterid');
		$this->db->from('cashcounter');
		$this->db->where('parentcashcounterid',$primaryid);
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			return "TRUE";
		} else {
			return "FALSE";
		}
	}
	//delete counter
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['cashcounterelementstable']);
		$parenttable = explode(',',$_GET['cashcounteelementspartable']);
		$id = $_GET['cashcounterprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$check = $this->checkparentcashcounter($id);
				if($check != "TRUE") {
					$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
					echo "TRUE";
				} else {
					echo "Parent";
				}
			}
		} else {
			$check = $this->checkparentcashcounter($id);
			if($check != "TRUE") {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				echo "TRUE";
			} else {
				echo "Parent";
			}
		}
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$cashcountername = $this->Basefunctions->singlefieldfetch('cashcountername','cashcounterid','cashcounter',$id); //Cash counter name
		$activity = ''.$user.' deleted CashCounter - '.$cashcountername.'';
		$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,111);
	}
	public function getcounterparent() {
		$parentcounterid='';
		$countername='';
		$counterdefaultid='';
		$id=$_GET['id'];
		$data=$this->db->select('a.parentcashcounterid as parentcashcounterids,b.cashcountername as counternames')
		->from('cashcounter as a')
		->join('cashcounter as b','a.parentcashcounterid=b.cashcounterid')
		->where('a.cashcounterid',$id)
		->get()->result();
		foreach($data as $in) {
			$parentcounterid=$in->parentcashcounterids;
			$countername=$in->counternames;
		}
		$a=array('parentcashcounterid'=>$parentcounterid,'cashcountername'=>$countername);
		echo json_encode($a);
	}
	// load accountname without/with assigned
	public function loadaccountname() {
		$result=array();
		$tablename = $_POST['table'];
		if($_POST['datastatus'] == 1) {
			$data = $this->db->query("SELECT account.accountid, account.accountname FROM `account` LEFT JOIN ".$tablename." ON ".$tablename.".accountid=account.accountid and ".$tablename.".status = 1 WHERE ".$tablename.".`accountid` is null and account.status = 1 and account.accounttypeid = 19 GROUP BY account.accountid");
		}else if($_POST['datastatus'] == 0) {
			$data = $this->db->query("SELECT account.accountid, account.accountname FROM `account` JOIN ".$tablename." ON ".$tablename.".accountid=account.accountid and ".$tablename.".status = 1 WHERE ".$tablename.".status = 1 and account.accounttypeid = 19 GROUP BY account.accountid");
		}
		$i=0;
		if($data->num_rows() > 0){
			foreach($data->result() as $user_name)
			{
				$result[]=array(
						'accountid' => $user_name->accountid,
						'accountname' => $user_name->accountname,
				);
				$i++;
			}
		}else{
			$result='';
		}
		echo json_encode($result);
	}
	// load username without/with assigned
	public function loadusername() {
		$result=array();
		$tablename = $_POST['table'];
		if($_POST['datastatus'] == 1) {
			$data = $this->db->query("SELECT employee.employeeid, employee.employeename FROM `employee` LEFT JOIN ".$tablename." ON ".$tablename.".employeeid=employee.employeeid and ".$tablename.".status = 1 WHERE ".$tablename.".`employeeid` is null and employee.status = 1 GROUP BY employee.employeeid");
		}else if($_POST['datastatus'] == 0) {
			$data = $this->db->query("SELECT employee.employeeid, employee.employeename FROM `employee` JOIN ".$tablename." ON ".$tablename.".employeeid=employee.employeeid and ".$tablename.".status = 1 WHERE ".$tablename.".status = 1 GROUP BY employee.employeeid");
		}
		$i=0;
		if($data->num_rows() > 0){
			foreach($data->result() as $user_name)
			{
				$result[]=array(
						'employeeid' => $user_name->employeeid,
						'employeename' => $user_name->employeename,
				);
				$i++;
			}
		}else{
			$result='';
		}
		echo json_encode($result);
	}
}