<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cashcounter extends MX_Controller{
    public function __construct() {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Cashcounter/Cashcountermodel');	
    }
    //first basic hitting view
    public function index() {        
    	$moduleid = array(111);
    	sessionchecker($moduleid);
		/*action*/
		$data['actionassign'] = $this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['branchid'] = $this->Basefunctions->branchid;
		$this->load->view('Cashcounter/cashcounterview',$data);
	}
	//create storage
	public function newdatacreate() {  
    	$this->Cashcountermodel->newdatacreatemodel();
    }
	//information fetchstorage
	public function fetchformdataeditdetails() {
		$moduleid = array(111);
		$this->Cashcountermodel->informationfetchmodel($moduleid);
	}
	//update storage
    public function datainformationupdate() {
        $this->Cashcountermodel->datainformationupdatemodel();
    }
	//delete storage
    public function deleteinformationdata() {
    	$moduleid = array(111);
		$this->Cashcountermodel->deleteoldinformation($moduleid);
    }
    //reload the storage list element
    public function getcounterparentid() {
    	$this->Cashcountermodel->getcounterparent();
    }
	//new tree create
    public function treedatafetchfun() {
    	$tablename = $_POST['tabname'];
    	$manopt = $_POST['mandval'];
    	$lablname = $_POST['fieldlabl'];
    	$mandlab = "";
    	if($manopt!='') {
    		$mandlab = "<span class='mandatoryfildclass'>*</span>";
    	}
    	echo '<label>'.$lablname.$mandlab.'</label>';
    	echo divopen( array("id"=>"dl-menu","class"=>"dl-menuwrapper","style"=>"z-index:2;margin-bottom: 1rem") );
    	$type="button";
    	echo '<button name="treebutton" type="'.$type.'" id="treebutton" class="dl-trigger treemenubtnclick" tabindex="102" style="height:2.2rem;color:#ffffff;font-size: 1.45rem;padding: 0.1rem;padding-top:0.3rem;background-color:#546E7A;"><i class="material-icons">format_indent_increase</i></button>';
    	$txtoptions = array("style"=>"width:83%;display:inline;position:absolute;height:2.12rem;padding-left: 5px;","readonly"=>"readonly","tabindex"=>"101","class"=>"".$manopt."");
    	echo text('parent'.$tablename.'','',$txtoptions);
    	echo hidden('parent'.$tablename.'id','');
    	echo '<ul class="dl-menu maintreegenerate large-12 medium-6 small-12 column" id="cashcounterlistuldata">';
    	$dataset = $this->Basefunctions->jewel_treecreatemodel($tablename);
		$this->createTree($dataset,0);
    	echo close('ul');
    	echo close('div');
    	echo '<input type="hidden" value="'.$manopt.'" id="treevalidationcheck" name="treevalidationcheck">';
    	echo '<input type="hidden" value="'.$lablname.'" id="treefiledlabelcheck" name="treefiledlabelcheck">';
    }
	//tree creation
	public function createTree($items = array(), $parentId = 0,$removeid = 0) {
		echo " <ul class='dl-submenu'> ";
		if($removeid == 0){
			echo '<li><a href="#" style="font-weight:bold;">Remove<i class="material-icons parentclear">close</i></a></li>';
		}
		if(count($items) > 0 ) {
			foreach ($items[$parentId] as $item) {
				echo '<li data-listname="'.$item['name'].'" data-level="'.$item['level'].'" data-listid="'.$item['id'].'" ><a href="#">'.$item['name'].'</a>';//level title
				$curId = $item['id'];
				//if there are children
				if (!empty($items[$curId])) {
					$this->createTree($items, $curId,1);
				}
				echo '</li>';
			}
		}
		echo '</ul>';
	}
	//load accountname with/without assigned
	public function loadaccountname() {
		$this->Cashcountermodel->loadaccountname();
	}
	//load username except assigned
	public function loadusername() {
		$this->Cashcountermodel->loadusername();
	}
}