<html>
<head>  
	<title>Sales Neuron</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<?php echo base_url();?>img/favi.ico"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/allinonecss.css"  media="screen" />
	<script src="<?php echo base_url();?>js/plugins/allpluginheader.min.js"></script>
	<script src="<?php echo base_url();?>js/plugins/allpluginbottom.min.js"></script>
	<script type="text/javascript">
		<?php
		$CI = &get_instance();
		$device = $CI->Basefunctions->deviceinfo();
		if($device=='phone') { 
			$devicename = 'phone';
		} else {
			$devicename = 'desktop';
		}	?>
		var deviceinfo = '<?php echo $devicename; ?>';
   </script>	
   <script>
		var base_url = '<?php echo base_url();?>';
		$(document).ready(function() {
			{// Foundation Initialization
				$(document).foundation();
			}
			$('body').fadeIn(1000);
			$('#installusername').focus();
			{/* login button  */
				$('#installsubmit').prop("disabled",false);
				$("#installsubmit").click(function(){
					$("#loginindexvalidationspan").validationEngine('validate');
				});
				$("#loginindexvalidationspan").validationEngine({
					onSuccess: function() {
						$("#processoverlay").show();
						$('#installsubmit').prop("disabled", true);
						var login= $("#loginform").serialize();
						var amp = '&';
						var loginuser = amp + login;
						var domain=base_url;
						$.ajax({
							type:"POST",
							dataType:'json',
							url:base_url+"Installer/activatecompany",
							data:"logindata="+ loginuser,
							async:false,
							cache:false,
							success: function(data) {
								if(data.status == 'success') {
									$("#normalbg").animate({left:'-18%'},250);
									$("#normalbg").fadeOut(20);
									var dname = data.domainname
									var domain=dname;
									$("#processoverlay").hide();
									alert('Software Installed Successfully');
									$('#installusername,#licensekey').val('');
									//window.location.replace(domain+'Home');
									//window.location.reload();
								} else if(data.status == 'Expired') {
									$("#processoverlay").hide();
									if(data.industryid == 3){
										$("#normalbg").animate({left:'-18%'},250);
										$("#normalbg").fadeOut(20);
										$('#installsubmit').prop("disabled", false);
										$('#reactivationoverlay').show();
									} else {
										var dname = data.domainname
										domain=dname;
										window.location.replace(domain+'Home/accountexpire');
									}
								} else {
									$("#processoverlay").hide();
									if(data.index == 0) {
										$('#installusername').validationEngine('showPrompt',data.status, 'error', 'bottomLeft',true);
									}else if(data.index == 1) {
										$('#licensekey').validationEngine('showPrompt',data.status, 'error', 'bottomLeft',true);
									}
									$('#installsubmit').prop("disabled", false);
								}
							},
						});
					},
					onFailure: function() {
						alertpopup('Enter Correct Account ID/Password');
						$("#processoverlay").hide();
					}
				});
			}
			
					
			
		});
	</script>
</head>
<body style='background:#dce9f1 !important'>
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row" style="max-width:93.5%">
		<div class="large-12 columns">
			<div class="large-4 columns large-centered" style="text-align:center">
				<img src="<?php echo base_url();?>/img/homepagelogo.png" id="homescreenlogo" style="cursor:pointer"/>
			</div>
		</div>
		<div class="large-12 columns">&nbsp;</div>
		<div class="large-12 columns">&nbsp;</div>
		<div class="large-12 columns">
			<div class="large-4 small-12 columns large-centered ">
				<div class="large-12 columns paddingzero" style="background:#617d8a;">
					<div class="large-12 columns headerformcaptionstyle" id="headingchange">Installer</div>
					<!-- login form -->
					<form id="loginform" action="" class="loginformdiv">
						<span class="validationEngineContainer" id="loginindexvalidationspan">
							<div class="input-field large-12 columns">
								<input type="text" id="installusername" name="installusername" value="" class="validate[required,custom[email],maxSize[50]]" data-prompt-position="bottomLeft">
								<label for="installusername">Email Address<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="input-field large-12 columns">
								<input type="text" id="licensekey" name="licensekey" value="" class="validate[required,maxSize[50]]" data-prompt-position="bottomLeft">
								<label for="licensekey">Licenese key<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="large-12 columns">&nbsp;</div>
							<div class="large-12 columns centertext">							
								<input type="button" class="btn homepageloginbtn" value="Activate" id="installsubmit" name="installsubmit" />
							</div>
							<div class="large-12 columns">&nbsp;</div>
							<div class="large-12 columns">&nbsp;</div>
							<div class="large-12 columns" style="background:#617d8a ">&nbsp;</div>
							<div class="large-12 columns" style="background:#617d8a ">&nbsp;</div>
						</span>
					</form>
					
					
					<div class="large-12 columns mailprocessdiv centertext hidedisplay">
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">
							<span id="processspinner" class="fa  fa-spinner fa-spin" title="processing"> </span>
							<span id="processing" class="" style="font-family: Segoe UI; font-size: 0.9rem; color:gray;" title="Processing"> Processing...</span>
						</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns backtoforgetpassword hidedisplay">							
							<input type="button" class="btn homepageloginbtn" value="back" id="backtoforgetpassword"/>
						</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div class="large-12 columns">
						<form id="verifysecurityform" action="" class="hidedisplay securityqaformdiv">
							<span id="securityques" class="validationEngineContainer">			
								<div class="large-12 columns">
									<label>Select Security Question</label>
									<input type="text" id="securityquest" name="securityquest" value="" class="validate[required,maxSize[30]]" data-prompt-position="bottomLeft" readonly>
								</div>
								<div class="large-12 columns">
									<label>Enter Answer</label>
									<input type="text" id="securityanswer" name="securityanswer" value="" class="validate[required,custom[onlyLetterNumber],maxSize[30]]" data-prompt-position="bottomLeft">
								</div>
								<input type="hidden" id ="userid"  value=""  name="userid" class=""/>
								<div class="large-12 columns">&nbsp;</div>
								<div class="large-12 columns centertext">							
									 <input type="button" class="btn homepageloginbtn" value="Submit" id="secanswersubmit"/>
								</div>	
								<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
								<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
							</span>
						</form>
						<!-- forgot password-->
						<form id="changepasswordform" action="" class="hidedisplay changepasswordformdiv">
							<span id="changepassword" class="validationEngineContainer">	
								<div class="large-12 columns">
									<label>Type New Password</label>
									<input type="password" id="newpassword" name="newpassword" value="" class="validate[required,minSize[6],maxSize[30]]" data-prompt-position="bottomLeft">
								</div>
								<div class="large-12 columns">
									<label>Retype New Password</label>
									<input type="password" id="retypepassword" name="retypepassword" value="" class="validate[required,minSize[6],maxSize[30],equals[newpassword]]" data-prompt-position="bottomLeft">
								</div>			
								<input type="hidden" id ="credentialuserid"  value=""  name="userid" class=""/>
								<div class="large-12 columns">&nbsp;</div>
								<div class="large-12 columns centertext">							
									 <input type="button" class="btn homepageloginbtn" value="Submit" id="changepasswordsubmit"/>
								</div>	
								<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
								<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
							</span>
						</form>
						<div class="large-12 columns" style="text-align:center;">
							<span></span>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay" id="failurealerts">	
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>		
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="large-3 medium-6 small-10 large-centered medium-centered small-centered columns overlayborder">
				<div class="row">&nbsp;</div>
				<div class="row">
				<div class="large-12 columns alertsheadercaptionstyle" style="text-align:left">
					<div class="small-12 columns"><span style="color:#ffffff;padding-right:0.5rem"><i class="material-icons">warning</i></span> Alert</div>
				</div>
				<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
					<div class="large-12 large-centered columns" style="background:#f5f5f5;text-align:center">
						<span class="alertinputstyle">Sorry Company Not Created !!!</span>
						<div class="large-12 column">&nbsp;</div>
					</div>
				</div>			
				<div class="row">&nbsp;</div>
			</div>
		</div>
	</div>
	<!--- process overlay-->
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay" id="processoverlay" style="overflow-y: hidden;overflow-x: hidden;z-index:100;">		
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div><div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="large-2 columns end large-centered">
				<div class="large-12 columns card-panel" style="text-align: left; background: #fff;padding-bottom: 0px; ">
					<div class="small-12 large-12 medium-12 columns paddingzero">
						<span><div class="spinner"></div></span><span style="display:inline-block;height:35px;vertical-align:middle;text-align: center;width: 100%;">&nbsp;&nbsp;&nbsp;  Processing...</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>   
</html>