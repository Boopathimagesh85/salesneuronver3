<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Installer extends CI_Controller 
{
	function __construct() {
    	parent::__construct();	
		$this->load->model('Installer/Installermodel');
		$this->load->model('Login/User');
		$this->load->model('Base/Basefunctions');
	}
	//import file format
	public $utf8 = true;
    //Home Page Screen
	public function index() {
		$this->load->view('Installer/installerview');
	}
	// Activate company
	public function activatecompany() {
		$datef="Y-m-d H:i:s";
		$companydata=array();
		$branchdata=array();
		$employeedata=array();
		$clientcompanydata=array();
		$clientbranchdata=array();
		$clientemployeedata=array();
		$planloginfo = array();
		$planmoduleids = '';
		$installusername=isset($_POST['installusername'])?$_POST['installusername']:'';
		$licensekey=isset($_POST['licensekey'])?$_POST['licensekey']:'';
		$loginfo = $this->Installermodel->checkuserdata($installusername,$licensekey);
		$userinfo = json_decode($loginfo,true);
		if($userinfo['status'] == 'success') {
			$companyid = $userinfo['companyid'];
			$planid = $userinfo['planid'];
			$mobileno = $userinfo['mobileno'];
			$hostname = $userinfo['hostname'];
			$hash = password_hash($licensekey, PASSWORD_DEFAULT);
			// server db
			$host = $this->db->hostname;
			$user = $this->db->username;
			$passwd = $this->db->password;
			$dbconfig['hostname'] = $host;
			$dbconfig['username'] = $user;
			$dbconfig['password'] = $passwd;
			$dbconfig['database'] = 'snservermasterdb';
			$dbconfig['dbdriver'] = 'mysqli';
			$dbconfig['dbprefix'] = '';
			$dbconfig['pconnect'] = TRUE;
			$dbconfig['db_debug'] = TRUE;
			$dbconfig['cache_on'] = FALSE;
			$dbconfig['cachedir'] = '';
			$dbconfig['char_set'] = 'utf8';
			$dbconfig['dbcollat'] = 'utf8_general_ci';
			$dbconfig['swap_pre'] = '';
			$dbconfig['autoinit'] = TRUE;
			$dbconfig['stricton'] = FALSE;
			$serverdb =  $this->load->database($dbconfig, TRUE);
			// company data
			$serverdb->select('companyname,emailid,contactperson,companyid,timezoneid');
			$serverdb->from('company');
			$serverdb->where('companyid',$companyid);
			$serverdb->where('status',1);
			$querycompany = $serverdb->get();
			if($querycompany->num_rows() >= 1) {
				foreach($querycompany->result() as $row) {
					$companyname = $row->companyname;
					$name = $row->contactperson;
					$email = $row->emailid;
					$ctzoneid = $row->timezoneid;
					$companydata=array('companyname'=>$companyname,'contactperson'=>$name,'emailid'=>$email,'hostname'=>$hostname,'timezoneid'=>$ctzoneid,'createdate'=>date($datef),'lastupdatedate'=>date($datef),'currencyid'=>2,'dateformatid'=>3,'createuserid'=>1,'lastupdateuserid'=>1,'status'=>1);
													
				}
			}
			// branch data
			$serverdb->select('branchname,emailid,branchid');
			$serverdb->from('branch');
			$serverdb->where('companyid',$companyid);
			$serverdb->where('status',1);
			$querybranch = $serverdb->get();
			if($querybranch->num_rows() >= 1) {
				foreach($querybranch->result() as $row) {
					$branchname = $row->branchname;
					$emailid = $row->emailid;
					$branchid = $row->branchid;
					$branchdata=array('branchname'=>$branchname,'companyid'=>$companyid,'branchtypeid'=>2,'emailid'=>$emailid,'createdate'=>date($datef),'lastupdatedate'=>date($datef),'createuserid'=>1,'lastupdateuserid'=>1,'status'=>1);
				}
			}
			// employee data
			$serverdb->select('employeename,branchid,emailid');
			$serverdb->from('employee');
			$serverdb->where('branchid',$branchid);
			$serverdb->where('status',1);
			$queryemployee = $serverdb->get();
			if($queryemployee->num_rows() >= 1) {
				foreach($queryemployee->result() as $row) {
					$employeename = $row->employeename;
					$employeeemailid = $row->emailid;
					$branchid = $row->branchid;
					$employeedata=array('employeename'=>$employeename,'branchid'=>$branchid,'userroleid'=>2,'emailid'=>$employeeemailid,'password'=>$hash,'timezoneid'=>$ctzoneid,'createdate'=>date($datef),'lastupdatedate'=>date($datef),'createuserid'=>1,'lastupdateuserid'=>1,'status'=>1);
				}
			}
			$activationdate = date($this->Basefunctions->datef);
			$expiredate = date($this->Basefunctions->datef, strtotime('7 days'));
			$planloginfo = array(
				'industryid'=>3,
				'planid'=>$planid,
				'companyid'=>$companyid,
				'registeremailid'=>$email,
				'registermobileno'=>$mobileno,
				'accountcreationdate'=>$activationdate,
				'activationdate'=>$activationdate,
				'expiredate'=>$expiredate,
				'accountclosedate'=>'0000-00-00 00:00:00',
				//'domainnameinfo'=>$domainname,
				'databasenameinfo'=>'sep9_sndatadb',
				'activationstatus'=>'Yes',
				'status'=>1
			);
			$serverdb->where('registeremailid',$installusername);
			$serverdb->update('userplaninfo',array('activationstatus'=>'Yes'));
		
		
			// master db
			$host = $this->db->hostname;
			$user = $this->db->username;
			$passwd = $this->db->password;
			$dbconfig['hostname'] = $host;
			$dbconfig['username'] = $user;
			$dbconfig['password'] = $passwd;
			$dbconfig['database'] = 'sep9_snmasterdb';
			$dbconfig['dbdriver'] = 'mysqli';
			$dbconfig['dbprefix'] = '';
			$dbconfig['pconnect'] = TRUE;
			$dbconfig['db_debug'] = TRUE;
			$dbconfig['cache_on'] = FALSE;
			$dbconfig['cachedir'] = '';
			$dbconfig['char_set'] = 'utf8';
			$dbconfig['dbcollat'] = 'utf8_general_ci';
			$dbconfig['swap_pre'] = '';
			$dbconfig['autoinit'] = TRUE;
			$dbconfig['stricton'] = FALSE;
			$masterdb =  $this->load->database($dbconfig, TRUE);
			// create master company
			$masterdb->insert('company',$companydata);
			$insertcompanyid=$masterdb->insert_id();
			//create master  branch
			$masterdb->insert('branch',$branchdata);
			$insertbranchid=$masterdb->insert_id();
			//create master  employee
			$masterdb->insert('employee',$employeedata);
			$empid = $masterdb->insert_id();
			//update company,branch log userids info
			$masterdb->where('companyid',$insertcompanyid);
			$masterdb->update('company',array('createuserid'=>$empid,'lastupdateuserid'=>$empid));
			
			$masterdb->where('branchid',$insertbranchid);
			$masterdb->update('branch',array('createuserid'=>$empid,'lastupdateuserid'=>$empid));
			
			$masterdb->where('employeeid',$empid);
			$masterdb->update('employee',array('createuserid'=>$empid,'lastupdateuserid'=>$empid));
			// master user plan info insert
			$masterdb->insert('userplaninfo',$planloginfo);
			
			//fetch moduleids based on plan
			$planmoduleids = $this->Installermodel->planinformationdatafetch($planid,3,$masterdb);
			//print_r($planmoduleids); die();
			// client db
			$host = $this->db->hostname;
			$user = $this->db->username;
			$passwd = $this->db->password;
			$dbconfig['hostname'] = $host;
			$dbconfig['username'] = $user;
			$dbconfig['password'] = $passwd;
			$dbconfig['database'] = 'sep9_sndatadb';
			$dbconfig['dbdriver'] = 'mysqli';
			$dbconfig['dbprefix'] = '';
			$dbconfig['pconnect'] = TRUE;
			$dbconfig['db_debug'] = TRUE;
			$dbconfig['cache_on'] = FALSE;
			$dbconfig['cachedir'] = '';
			$dbconfig['char_set'] = 'utf8';
			$dbconfig['dbcollat'] = 'utf8_general_ci';
			$dbconfig['swap_pre'] = '';
			$dbconfig['autoinit'] = TRUE;
			$dbconfig['stricton'] = FALSE;
			$clientdb =  $this->load->database($dbconfig, TRUE);
			
			// client company create
			$clientcompanydata=array('companyname'=>$companyname,'contactperson'=>$name,'emailid'=>$email,'hostname'=>$hostname,'timezoneid'=>$ctzoneid,'createdate'=>date($datef),'lastupdatedate'=>date($datef),'currencyid'=>2,'dateformatid'=>3,'createuserid'=>1,'lastupdateuserid'=>1,'status'=>1,'mastercompanyid'=>$insertcompanyid);
			$clientdb->insert('company',$clientcompanydata);
			$clientinsertcompanyid=$clientdb->insert_id();
			$clientbranchdata=array('branchname'=>$branchname,'companyid'=>$companyid,'branchtypeid'=>2,'emailid'=>$email,'industryid'=>3,'createdate'=>date($datef),'lastupdatedate'=>date($datef),'createuserid'=>1,'lastupdateuserid'=>1,'status'=>1);
			$clientdb->insert('branch',$clientbranchdata);
			$clientinsertbranchid=$clientdb->insert_id();
			
			$clientemployeedata=array('employeename'=>$name,'branchid'=>$branchid,'userroleid'=>2,'emailid'=>$email,'timezoneid'=>$ctzoneid,'password'=>$hash,'salestransactiontypeid'=>11,'industryid'=>3,'createdate'=>date($datef),'lastupdatedate'=>date($datef),'createuserid'=>1,'lastupdateuserid'=>1,'status'=>1);
			$clientdb->insert('employee',$clientemployeedata);
			$clientempid = $clientdb->insert_id();
			
			//update client db company,branch log userids info
			$clientdb->where('companyid',$clientinsertcompanyid);
			$clientdb->update('company',array('createuserid'=>$clientempid,'lastupdateuserid'=>$clientempid));
			
			$clientdb->where('branchid',$clientinsertbranchid);
			$clientdb->update('branch',array('createuserid'=>$clientempid,'lastupdateuserid'=>$clientempid));
			
			$clientdb->where('employeeid',$clientempid);
			$clientdb->update('employee',array('createuserid'=>$clientempid,'lastupdateuserid'=>$clientempid));
			
			$planmoduleids = explode(',',$planmoduleids);
			//activate user plan
			$uparesult = $this->User->userplanactivation($planmoduleids,$clientdb);
			$loginresult = json_encode(array('status'=>$userinfo['status']));
			
		}else {
			$loginresult = json_encode(array('status'=>$userinfo['status'],'email'=>$installusername,'index'=>$userinfo['index']));
		}
		echo $loginresult;
	}
	
}