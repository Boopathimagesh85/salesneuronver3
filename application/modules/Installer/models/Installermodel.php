<?php
Class Installermodel extends CI_Model {
	//login
	public function checkuserdata($installusername,$licensekey) {
		$host = $this->db->hostname;
		$user = $this->db->username;
		$passwd = $this->db->password;
		$dbconfig['hostname'] = $host;
		$dbconfig['username'] = $user;
		$dbconfig['password'] = $passwd;
		$dbconfig['database'] = 'snservermasterdb';
		$dbconfig['dbdriver'] = 'mysqli';
		$dbconfig['dbprefix'] = '';
		$dbconfig['pconnect'] = TRUE;
		$dbconfig['db_debug'] = TRUE;
		$dbconfig['cache_on'] = FALSE;
		$dbconfig['cachedir'] = '';
		$dbconfig['char_set'] = 'utf8';
		$dbconfig['dbcollat'] = 'utf8_general_ci';
		$dbconfig['swap_pre'] = '';
		$dbconfig['autoinit'] = TRUE;
		$dbconfig['stricton'] = FALSE;
		$serverdb =  $this->load->database($dbconfig, TRUE);
		$serverdb->select('userplaninfo.userplaninfoid,userplaninfo.registeremailid,userplaninfo.activationstatus,userplaninfo.planid,userplaninfo.registermobileno');
		$serverdb->from('userplaninfo');
		$serverdb->where('userplaninfo.registeremailid',$installusername);
		$serverdb->where('userplaninfo.status',1);
	    $serverdb->limit(1);
		$query = $serverdb->get();
		if($query->num_rows() >= 1) {
			foreach($query->result() as $row) {
				$actstatus = $row->activationstatus;
				$planid = $row->planid;
				$mobileno = $row->registermobileno;
				if($actstatus=='No') {
					$serverdb->select('company.emailid,company.companyid,company.hostname');
					$serverdb->from('company');
					$serverdb->where('company.emailid',$installusername);
					$serverdb->where('company.licensekey',$licensekey);
					$serverdb->where('company.status',1);
					$serverdb->limit(1);
					$querycompany = $serverdb->get();
					if($querycompany->num_rows() >= 1) {
						foreach($querycompany->result() as $row) {
							$loginarray = array('status'=>'success','companyid'=>$row->companyid,'planid'=>$planid,'mobileno'=>$mobileno,'hostname'=>$row->hostname);
						}
					}else {
						$loginarray = array('status'=>'License key is invalid','index'=>1);
					}
				} else if($actstatus=='Yes') {
					$loginarray=array('status'=>'Already Activated','index'=>0);
				}
			}
		}else {
			$statusinfo = $this->Basefunctions->generalinformaion('userplaninfo','status','registeremailid',$installusername);
			if($statusinfo=='5') {
				$loginarray=array('status'=>'Account Is Cancelled','index'=>0);
			} else {
				$loginarray=array('status'=>'Account ID is invalid','index'=>0);
			}
		}
		return json_encode($loginarray);
	}
	//plan information data fetch
	public function planinformationdatafetch($planid,$industryid,$masterdb) {
		$dataset = "1";
		$data=$masterdb->select('planinfo.moduleid')->from('planinfo')->join('plan','plan.planid=planinfo.planid')->where('planinfo.planid',$planid)->where('planinfo.industryid',$industryid)->get();
		foreach($data->result() as $info) {
			$dataset = $info->moduleid;
		}
		return $dataset;
	}
}
?>