<?php
Class Modulebuildermodel extends CI_Model {
	public function __construct() {
		parent::__construct(); 
    }
	//default values
	public function defaultvalueget() {
		$defdata['createdate'] = date($this->Basefunctions->datef);
		$defdata['lastupdatedate'] = date($this->Basefunctions->datef);
		$defdata['createuserid'] = $this->Basefunctions->userid;
		$defdata['lastupdateuserid'] = $this->Basefunctions->userid;
		$defdata['status'] = 1;
		return $defdata;
	}
	//drop down condition field name fetch
	public function ddcondfieldnamefetchmodel() {
		$id = $_GET['id'];
		$i=0;
		$this->db->select('modulefield.modulefieldid,modulefield.fieldlabel,modulefield.columnname,modulefield.tablename,modulefield.parenttable,modulefield.uitypeid');
		$this->db->from('modulefield');
		$this->db->join('uitype','uitype.uitypeid = modulefield.uitypeid');
		$this->db->where('moduletabid',$id);
		$this->db->where('modulefield.status',1);
		$this->db->where('uitype.status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result()as $row) {
				$data[$i]=array('datasid'=>$row->modulefieldid,'dataname'=>$row->fieldlabel,'datacolmname'=>$row->columnname,'datatblname'=>$row->tablename,'datapartable'=>$row->parenttable,'datauitype'=>$row->uitypeid);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//module name checking
	public function modulenamecheckmodel() {
		$modname = $_POST['modulename'];
		if($modname!= "") {
			$modulename = preg_replace('/[^A-Za-z0-9]/', '', $modname);
			$result = $this->db->select('modulemastertable')->from('module')->where('module.modulemastertable',$modulename)->where('module.status',1)->get();
			if($result->num_rows() > 0) {
				echo "True";
			} else { echo "False"; }
		} else { echo "False"; }
	}
	//drop down table name check
	public function picklistnamecheckmodel() {
		$database = $this->db->database;
		$pklistname = $_POST['picklistname'];
		if($pklistname!= "") {
			$picklistname = preg_replace('/[^A-Za-z0-9]/', '', $pklistname);
			$result = $this->tableexitcheck($database,$picklistname);
			if( $result > 0 ) {
				echo "True";
			} else { echo "False"; }
		} else { echo "False"; }
	}
	//table name check
	public function tableexitcheck($database,$newtable) {
		$counttab=0;
		$tabexits = $this->db->query("SELECT COUNT(*) AS tabcount FROM information_schema.tables WHERE table_schema = '".$database."'AND table_name = '".$newtable."'");
		foreach($tabexits->result() as $tkey) { 
			$counttab = $tkey->tabcount;
		};
		return $counttab;
	}
	//table field data type check
	public function tablefieldtypecheck($tblname,$fieldname) {
		$fields = $this->db->field_data($tblname);
		foreach ($fields as $field) {
			$name =  $field->name;
			if($fieldname == $name) {
				return $field->type;
			}
		}
	}
	//table field name check
	public function tablefieldnamecheck($tblname,$fieldname) {
		$result = 'false';
		$fields = $this->db->field_data($tblname);
		foreach ($fields as $field) {
			$name =  $field->name;
			if($fieldname == $name) {
				$result = 'true';
			}
		}
		return $result;
	}
	//check drop down table type
	public function checkdropdowntabletype($database,$tblcolumname) {
		$tblname=substr(strtolower($tblcolumname),0,-2);
		$result = $this->tableexitcheck($database,$tblname);
		if( $result > 0 ) {
			$tabexits = $this->db->query("SELECT COUNT(*) AS tabcount FROM information_schema.COLUMNS WHERE table_schema = '".$database."'AND table_name = '".$tblname."' AND column_name='setdefault'");
			foreach($tabexits->result() as $tkey) {
				if($tkey->tabcount > 0) {
					return "17";
				} else { return "19"; }
			}
		} else { return "17"; }
	}
	//new tab group creation
    public function tapgroupcreatemodel($moduleid,$tabgrptype,$tabgrpseq,$tabgrpname) {
		$dataarr = array(
			'moduleid'=>$moduleid,
			'moduletabgroupname'=>$tabgrpname,
			'usermoduletabgroupname'=>$tabgrpname,
			'moduletabgrouptemplateid'=>$tabgrptype,
			'sequence'=>$tabgrpseq,
			'userroleid'=>$this->Basefunctions->userroleid,
		);
		$defdataarr = $this->defaultvalueget();
		$newdata = array_merge($dataarr,$defdataarr);
		$this->db->insert('moduletabgroup', array_filter($newdata));
		$tabgrpid = $this->db->insert_id();
		return $tabgrpid;
    }
	//new tab section creation
    public function tapgroupsectioncreatemodel($moduleid,$tabgrpid,$tabsecseq,$tabsecname) {
        $dataarr = array(
            'moduleid'=>$moduleid,
            'moduletabgroupid'=>$tabgrpid,
            'moduletabsectionname'=>$tabsecname,
        	'sequence'=>$tabsecseq,
			'userroleid'=>$this->Basefunctions->userroleid,
        );
		$defdataarr = $this->defaultvalueget();
		$newdata = array_merge($dataarr,$defdataarr);
		$this->db->insert( 'moduletabsection', array_filter($newdata) );
		$tabsecid = $this->db->insert_id();
		return $tabsecid;
    }
	//new module generation
	public function modulegen($modulename) {
		$moduleid=0;
		if($modulename!="") {
			$modname = preg_replace('/[^A-Za-z0-9]/', '', $modulename);
			$parenttable = strtolower($modname);
			$modlink = ucfirst($parenttable);
			$dataarr = array(
				'modulename'=>ucwords($modulename),
				'menuname'=>ucwords($modulename),
				'menucategoryid'=>10,
				'parentmoduleid'=>120,
				'modulecategorytypeid'=>1,
				'modulelink'=>$modlink,
				'modulemastertable'=>$parenttable,
				'moduleprivilegeid'=>'211,212,253,251,207,256,258,259,252',
				'moduletypeid'=>2,
				'industryid'=>$this->Basefunctions->industryid,
			);
			$defdataarr = $this->defaultvalueget();
			$newdata = array_merge($dataarr,$defdataarr);
			$this->db->insert( 'module', array_filter($newdata) );
			$moduleid = $this->db->insert_id();
			$datainfo = array(
				'moduleid'=>$moduleid,
				'userroleid'=>$this->Basefunctions->userroleid
			);
			$newdatainfo = array_merge($datainfo,$defdataarr);
			$this->db->insert( 'moduleinfo', $newdatainfo );
			return $moduleid;
		} else { return $moduleid; }
	}
	//dynamic view module creation
	public function viewmodulegen($modulename) {
		$viewmoduleid=0;
		if($modulename!="") {
			$dataarr = array(
				'viewcreationmodulename'=>ucwords($modulename),
				'status'=>1
			);
			$this->db->insert( 'viewcreationmodule', $dataarr );
			$viewmoduleid = $this->db->insert_id();
			return $viewmoduleid;
		}
	}
	//action assign for module
	public function moduleactioncreate($moduleid,$modactions) {
		$tbg=array('moduleid'=>$moduleid,'toolbarnameid'=>$modactions,'status'=>1);
		$this->db->insert('toolbargroup',$tbg);
		$tbp=array('moduleid'=>$moduleid,'toolbarnameid'=>$modactions,'userroleid'=>$this->Basefunctions->userroleid,'status'=>1);
		$this->db->insert('usertoolbarprivilege',$tbp);
		return 'done';
	}
	//form elements field type
	public function gencolumntype($coltypeval) {
		if($coltypeval == 'Yes') { return 2; } else { return 1; }
	}
	//module builder create
	public function modbuildercreatemodel() {
		$userversion = $_POST['userversion'];
		$tabgrupinfo = $_POST['spantabgrpinfo'];
		$tabsecinfo = $_POST['spantabsectinfo'];		
		if(isset($_POST['frmelmattrinfo'])) {
			$frmeleminfo = $_POST['frmelmattrinfo'];
		} else {
			$frmeleminfo = array();
		}		
		$userroleid = $this->Basefunctions->userroleid;
		$uid = $this->Basefunctions->userid;
		$date = date($this->Basefunctions->datef);
		$viewcolid = array();
		$dynamicgridname = array();
		$resttab = array();
		$editorfname = array();
		$editortabsecids = array();
		$editortablenames = array();
		$defdataarr = $this->defaultvalueget();
		$modulename = $_POST['modulename'];
		$modaction = $_POST['moduleaction'];
		$moduleid = $this->modulegen($modulename);
		$modaction = $this->moduleactioncreate($moduleid,$modaction);
		$viewmoduleid = $this->viewmodulegen($modulename);
		$parenttable = preg_replace('/[^A-Za-z0-9]/', '', $modulename);
		$maintable = strtolower($parenttable);
		$parenttable = $maintable;
		$database = $this->db->database;
		if($moduleid != "0" && $modaction == 'done') {
			$previoustab="";
			foreach($tabgrupinfo as $tabgrpvals) {
				$tabgrpvalue = explode(',',$tabgrpvals);				
				if($tabgrpvalue[0] == '2' && $tabgrpvalue[1] == '0'){
					$sectionwithgrid = 'sectionwithgrid';
				}else{
					$sectionwithgrid = 'sectionwithoutgrid';
				}
				$tabgroupid = $this->tapgroupcreatemodel($moduleid,$tabgrpvalue[0],$tabgrpvalue[1],$tabgrpvalue[2]);
				if($tabgrpvalue[0] == '2') {
					$dynamicgridname[$tabgroupid] = $tabgrpvalue[2];
				}
				$grptabname = preg_replace('/[^A-Za-z0-9]/', '', $tabgrpvalue[2]);
				$tabgrpid = $tabgrpvalue[1];
				$modname = preg_replace('/[^A-Za-z0-9]/', '', $modulename);
				$modulename = strtolower($parenttable);
				$editorcolname = $modulename.strtolower($grptabname).'_editorfilename';
				foreach($tabsecinfo as $tabsecvals) {
					$tabsecvalue = explode(',',$tabsecvals);
					if($tabgrpid == $tabsecvalue[0] ) {
						$tabsecid = $tabsecvalue[1];
						$tabsectionid = $this->tapgroupsectioncreatemodel($moduleid,$tabgroupid,$tabsecid,$tabsecvalue[2]);
						foreach($frmeleminfo as $frmsecelem) {
							$frmsecelements = explode(',',$frmsecelem);
							//check current section form elements
							if($tabsecid == $frmsecelements[0] ) {
								//new column name generation
								$fieldname = $frmsecelements[2];
								$forkeycons = '';
								if($userversion == 2) {
									$modfildmaxid = $this->Basefunctions->maximumid('modulefield','modulefieldid');
									$fname = preg_replace('/[^A-Za-z0-9]/', '', $fieldname);
									$tblcolumname = strtolower($fname.($modfildmaxid + 1));
								} else {
									$fname = preg_replace('/[^A-Za-z0-9]/', '', $fieldname);
									$tblcolumname = strtolower($fname);
								}
								if($frmsecelements[1] == '4' || $frmsecelements[1] == '5' ){
									$tblcolumname = strtolower($fname);
									if($tblcolumname == 'integer' || $tblcolumname == 'decimal') {
										$tblcolumname = $tblcolumname.$tabsectionid;
									} else {
										$tblcolumname = $tblcolumname;
									}
								}
								//name generation for password field
								if($frmsecelements[1] == '22' ) {
									$datafieldname = $tblcolumname.'_password';
								} else {
									$datafieldname = $tblcolumname;
									if($datafieldname=='Name') {
										$datafieldname=$tblcolumname."cf";
									}
								}
								{//data type assign
									if($frmsecelements[1] == '3') {
										$datatype = 'text NULL';
									} else if($frmsecelements[1] == '4') {
										$datatype = 'int UNSIGNED  NOT NULL DEFAULT "0"';
									} else if($frmsecelements[1] == '5') {
										$datatype = 'decimal(20,'.$frmsecelements[5].') NOT NULL DEFAULT "0"';
									} else if($frmsecelements[1] == '17' || $frmsecelements[1] == '26') {
										if($frmsecelements[1] == '17') {
											if($frmsecelements[9] == "No") {
												$tabname = $tblcolumname;
												$fldname = $tblcolumname.'id';
												$tabchk = $this->tableexitcheck($database,$tabname);
												if($tabchk == 0) {
													$datatype = 'int UNSIGNED  NOT NULL DEFAULT "1"';
												} else {
													$type = $this->tablefieldtypecheck($tabname,$fldname);
													$datatype = $type.' UNSIGNED  NOT NULL DEFAULT "1"';
												}
											} else {
												$datatype = 'varchar(100) NOT NULL';
											}
										} else if($frmsecelements[1] == '26') {
											if($frmsecelements[12] == "No") {
												$tabname = $frmsecelements[3];
												$fldname = $frmsecelements[3].'id';
												$tabchk1 = $this->tableexitcheck($database,$tabname);
												if($tabchk1 == 0) {
													$datatype = 'int UNSIGNED NOT NULL DEFAULT "1"';
												} else {
													$type = $this->tablefieldtypecheck($tabname,$fldname);
													$datatype = $type.' UNSIGNED NOT NULL DEFAULT "1"';
												}
											} else {
												$datatype = 'varchar(100) NOT NULL DEFAULT " "';
											}
										}
									} else {
										$datatype = 'varchar(100) NOT NULL DEFAULT " "';;
									}
								}
								//drop down table generation
								if($frmsecelements[1] == '17') {
									$ddtabname = $tblcolumname;
									$colname = $tblcolumname.'name';
									$colid = $ddtabname.'id';
									$tabcheck2 = $this->tableexitcheck($database,$ddtabname);
									if($tabcheck2 == 0) {
										$this->db->query( 'CREATE TABLE '.$ddtabname.'( '.$colid.' int UNSIGNED NOT NULL AUTO_INCREMENT,`'.$colname.'` varchar(100),moduleid varchar(100),userroleid varchar(150),industryid varchar(100),branchid int UNSIGNED NOT NULL DEFAULT "1",sortorder tinyint,setdefault varchar(10),createdate datetime NOT NULL,lastupdatedate datetime NOT NULL,createuserid mediumint UNSIGNED NOT NULL,lastupdateuserid mediumint UNSIGNED NOT NULL,status tinyint UNSIGNED NOT NULL,PRIMARY KEY ('.$colid.') )' );
										//default entry
										$this->db->query('INSERT INTO '.$ddtabname.' ('.$colid.','.$colname.',moduleid,userroleid,industryid,branchid,sortorder,setdefault,createdate,lastupdatedate,createuserid,lastupdateuserid,status) VALUES(0,"","1","1",0,0,0,0,"'.$date.'","'.$date.'",'.$uid.','.$uid.',3)');
										//module relation entry
										$this->db->query('INSERT INTO modulerelation(moduleid,parenttable,parentfieldname,relatedmoduleid,relatedtable,relatedfieldname,status) VALUES('.$moduleid.',"'.$parenttable.'","'.$colid.'",'.$moduleid.',"'.$ddtabname.'","'.$colid.'",1)');
										
									} else {
										//module relation entry
										$this->db->query('INSERT INTO modulerelation(moduleid,parenttable,parentfieldname,relatedmoduleid,relatedtable,relatedfieldname,status) VALUES('.$moduleid.',"'.$parenttable.'","'.$colid.'",'.$moduleid.',"'.$ddtabname.'","'.$colid.'",1)');
									}
									$datas=array();
									if($frmsecelements[3] != "" && $frmsecelements[3] != " ") {
										$datas = explode('|',$frmsecelements[3]);
										if($frmsecelements[11] == "Yes") {
											sort($datas);
										}
										$sorder = 1;
										foreach($datas as $value) {
											$def = 0;
											if($frmsecelements[4] == $value) {
												$def = 1;
											}
											$industryid = $this->Basefunctions->industryid;
											$this->db->query('INSERT INTO '.$ddtabname.' ('.$colid.','.$colname.',moduleid,userroleid,industryid,branchid,sortorder,setdefault,createdate,lastupdatedate,createuserid,lastupdateuserid,status) VALUES(0,"'.$value.'","'.$moduleid.'","'.$userroleid.'","'.$industryid.'",0,'.$sorder.','.$def.',"'.$date.'","'.$date.'",'.$uid.','.$uid.',1)');
											$sorder++;
										}
									}
									$tblcolumname = $colid;
									$datafieldname = $colid;
									if($frmsecelements[9] == "No") {
										$forkeycons = ' ,FOREIGN KEY ('.$tblcolumname.') REFERENCES '.$ddtabname.' ('.$tblcolumname.')';
									}
									$fkeytable = $ddtabname;
									$fkeycolm = $tblcolumname;
								}
								//relation component
								if($frmsecelements[1] == '26') {
									$tblcolumname = $frmsecelements[3].'id';
									$datafieldname = $frmsecelements[3].'id';
									if($frmsecelements[12] == "No") {
										$forkeycons = ' ,FOREIGN KEY ('.$tblcolumname.') REFERENCES '.$frmsecelements[3].' ('.$tblcolumname.')';
									}
									$fkeytable = $frmsecelements[3];
									$fkeycolm = $tblcolumname;
								}
								$gridtable = $maintable.strtolower($grptabname).'details';
								if($tabgrpvalue[0]==2) { //main table and grid table creation
									if($sectionwithgrid == 'sectionwithgrid'){					
										//check table is available
										$tabcheck = $this->tableexitcheck($database,$maintable);
										if($tabcheck == 0) {
											$this->db->query( 'CREATE TABLE '.$maintable.' ( '.$maintable.'id int UNSIGNED NOT NULL AUTO_INCREMENT,`'.$tblcolumname.'` '.$datatype.',industryid varchar(100),branchid int UNSIGNED NOT NULL DEFAULT "1",createdate datetime NOT NULL,lastupdatedate datetime NOT NULL,createuserid mediumint UNSIGNED NOT NULL,lastupdateuserid mediumint UNSIGNED NOT NULL,status tinyint UNSIGNED NOT NULL,PRIMARY KEY ('.$maintable.'id) '.$forkeycons.' )' );
											//default entry
											$this->db->query('INSERT INTO '.$maintable.' VALUES(0,1,0,0,"'.$date.'","'.$date.'",'.$uid.','.$uid.',3)');
										}else{
											$this->db->query( 'ALTER TABLE '.$maintable.' ADD COLUMN `'.$tblcolumname.'` '.$datatype.' AFTER '.$maintable.'id' );
											
										}
										$moduletabname = $maintable;
									} else{//main table and grid table creation
										//check table is available
										$tabcheck = $this->tableexitcheck($database,$maintable);
										if($tabcheck == 0) {
											$this->db->query( 'CREATE TABLE '.$maintable.' ( '.$maintable.'id int UNSIGNED NOT NULL AUTO_INCREMENT,industryid varchar(100),branchid int UNSIGNED NOT NULL DEFAULT "1",createdate datetime NOT NULL,lastupdatedate datetime NOT NULL,createuserid mediumint UNSIGNED NOT NULL,lastupdateuserid mediumint UNSIGNED NOT NULL,status tinyint UNSIGNED NOT NULL,PRIMARY KEY ('.$maintable.'id))' );
											//default entry
											$this->db->query('INSERT INTO '.$maintable.' VALUES(0,0,0,"'.$date.'","'.$date.'",'.$uid.','.$uid.',3)');
										}									
										//grid table creation
										$currenttab = strtolower($grptabname);
										if($currenttab!=$previoustab) {
											$previoustab = strtolower($grptabname);
											$tabcheck1 = $this->tableexitcheck($database,$gridtable);
											if($tabcheck1 == 0) {
												$this->db->query( 'CREATE TABLE '.$gridtable.' ( '.$gridtable.'id int UNSIGNED NOT NULL AUTO_INCREMENT,'.$maintable.'id int UNSIGNED NOT NULL,`'.$tblcolumname.'` '.$datatype.',createdate datetime NOT NULL,lastupdatedate datetime NOT NULL,createuserid mediumint UNSIGNED NOT NULL,lastupdateuserid mediumint UNSIGNED NOT NULL,status tinyint UNSIGNED NOT NULL,PRIMARY KEY ('.$gridtable.'id) '.$forkeycons.' )' );
												//default entry
												$this->db->query('INSERT INTO '.$gridtable.' VALUES(0,1,1,"'.$date.'","'.$date.'",'.$uid.','.$uid.',3)');
											} else {
												$colnamechk = $this->tablefieldnamecheck($gridtable,$tblcolumname);
												if($colnamechk == 'false') {
													$this->db->query( 'ALTER TABLE '.$gridtable.' ADD COLUMN `'.$tblcolumname.'` '.$datatype.' AFTER '.$maintable.'id' );
													if($forkeycons!="") {
														$this->db->query( 'ALTER TABLE '.$gridtable.' ADD   FOREIGN KEY('.$fkeycolm.') REFERENCES '.$fkeytable.'('.$fkeycolm.')' );
													}
												}
											}
										} else {
											$colnamechk = $this->tablefieldnamecheck($gridtable,$tblcolumname);
											if($colnamechk == 'false') {
												$this->db->query( 'ALTER TABLE '.$gridtable.' ADD COLUMN `'.$tblcolumname.'` '.$datatype.' AFTER '.$maintable.'id' );
												if($forkeycons!="") {
													$this->db->query( 'ALTER TABLE '.$gridtable.' ADD  FOREIGN KEY('.$fkeycolm.') REFERENCES '.$fkeytable.'('.$fkeycolm.')' );
												}
											}
										}
										$moduletabname = $gridtable;
										$resttab[] = $gridtable;
									}
								} else {//main table and editor information creation
									//check table is available
									$tabcheck = $this->tableexitcheck($database,$maintable);
									if($tabcheck == 0) {
										if( $tabgrpvalue[0]==3 && !in_array($editorcolname,$editorfname) ) {
											array_push($editorfname,$editorcolname);
											array_push($editortabsecids,$tabsectionid);
											array_push($editortablenames,$maintable);
											$this->db->query( 'CREATE TABLE '.$maintable.' ( '.$maintable.'id int UNSIGNED NOT NULL AUTO_INCREMENT,`'.$tblcolumname.'` '.$datatype.','.$editorcolname.' varchar(100) NOT NULL DEFAULT "1",industryid varchar(100),branchid int UNSIGNED NOT NULL DEFAULT "1",createdate datetime NOT NULL,lastupdatedate datetime NOT NULL,createuserid mediumint UNSIGNED NOT NULL,lastupdateuserid mediumint UNSIGNED NOT NULL,status tinyint UNSIGNED NOT NULL,PRIMARY KEY ('.$maintable.'id) '.$forkeycons.' )' );
											//default entry
											$this->db->query('INSERT INTO '.$maintable.' VALUES(0,"1","",0,0,"'.$date.'","'.$date.'",'.$uid.','.$uid.',3)');
										} else {
											$this->db->query( 'CREATE TABLE '.$maintable.' ( '.$maintable.'id int UNSIGNED NOT NULL AUTO_INCREMENT,`'.$tblcolumname.'` '.$datatype.',industryid varchar(100),branchid int UNSIGNED NOT NULL DEFAULT "1",createdate datetime NOT NULL,lastupdatedate datetime NOT NULL,createuserid mediumint UNSIGNED NOT NULL,lastupdateuserid mediumint UNSIGNED NOT NULL,status tinyint UNSIGNED NOT NULL,PRIMARY KEY ('.$maintable.'id)  '.$forkeycons.' )' );
											//default entry
											$this->db->query('INSERT INTO '.$maintable.' VALUES(0,"1",0,0,"'.$date.'","'.$date.'",'.$uid.','.$uid.',3)');
										}
									} else {
										if( $tabgrpvalue[0]==3 && !in_array($editorcolname,$editorfname) ) {
											array_push($editorfname,$editorcolname);
											array_push($editortabsecids,$tabsectionid);
											array_push($editortablenames,$maintable);
											$colnamechk = $this->tablefieldnamecheck($maintable,$tblcolumname);
											if($colnamechk == 'false') {
												$this->db->query( 'ALTER TABLE '.$maintable.' ADD COLUMN `'.$tblcolumname.'` '.$datatype.' AFTER '.$maintable.'id, ADD COLUMN '.$editorcolname.' varchar(100) NOT NULL DEFAULT " "  AFTER '.$tblcolumname.' ' );
											}
										} else {
											$colnamechk = $this->tablefieldnamecheck($maintable,$tblcolumname);
											if($colnamechk == 'false') {
												$this->db->query( 'ALTER TABLE '.$maintable.' ADD COLUMN `'.$tblcolumname.'` '.$datatype.' AFTER '.$maintable.'id' );
											}
										}
										if($forkeycons!="") {
											$this->db->query( 'ALTER TABLE '.$maintable.' ADD  FOREIGN KEY('.$fkeycolm.') REFERENCES '.$fkeytable.'('.$fkeycolm.')' );
										}
									}
									$moduletabname = $maintable;
								 }
								//sequence check
								$seqid = $this->Basefunctions->maxidwithcond('modulefield','sequence','moduletabsectionid',$tabsectionid);
								$seqnum = ++$seqid;

								//module data define
								$decisize = '0';
								$ddmultiple = 'No';
								$maxlength = 100;
								$ddpartable = '';
								$uitypeid = $frmsecelements[1];
								switch($frmsecelements[1]) {
									case 2: //text box
										$defvalue = $frmsecelements[3]; 
										$maxlength = $frmsecelements[4];
										$descp = $frmsecelements[5];
										$mandatory = $frmsecelements[6];
										$active = $frmsecelements[7];
										$readonly = $frmsecelements[8];
										$columntye = $this->gencolumntype($frmsecelements[9]);
										break;

									case 3: //text area
										$defvalue = $frmsecelements[3]; 
										$maxlength = $frmsecelements[4];
										$decisize = $frmsecelements[5];
										$descp = $frmsecelements[6];
										$mandatory = $frmsecelements[7];
										$active = $frmsecelements[8];
										$readonly = $frmsecelements[9];
										$columntye = $this->gencolumntype($frmsecelements[10]);
										break;

									case 4://integer
										$defvalue = $frmsecelements[3]; 
										$maxlength = $frmsecelements[4];
										$descp = $frmsecelements[5];
										$mandatory = $frmsecelements[6];
										$active = $frmsecelements[7];
										$readonly = $frmsecelements[8];
										$columntye = $this->gencolumntype($frmsecelements[9]);
										break;

									case 5://decimal
										$defvalue = $frmsecelements[3];
										$maxlength = $frmsecelements[4];
										$decisize = $frmsecelements[5];
										$descp = $frmsecelements[6];
										$mandatory = $frmsecelements[7];
										$active = $frmsecelements[8];
										$readonly = $frmsecelements[9];
										$columntye = $this->gencolumntype($frmsecelements[10]);
										break;

									case 6://percent
										$defvalue = $frmsecelements[3];
										$maxlength = $frmsecelements[4];
										$decisize = $frmsecelements[5];
										$descp = $frmsecelements[6];
										$mandatory = $frmsecelements[7];
										$active = $frmsecelements[8];
										$readonly = $frmsecelements[9];
										$columntye = $this->gencolumntype($frmsecelements[10]);
										break;

									case 7://currency
										$defvalue = $frmsecelements[3];
										$maxlength = $frmsecelements[4];
										$decisize = $frmsecelements[5];
										$descp = $frmsecelements[6];
										$mandatory = $frmsecelements[7];
										$active = $frmsecelements[8];
										$readonly = $frmsecelements[9];
										$columntye = $this->gencolumntype($frmsecelements[10]);
										break;

									case 8://Date
										$defvalue = $frmsecelements[3];
										$descp = $frmsecelements[4];
										$mandatory = $frmsecelements[5];
										$active = $frmsecelements[6];
										$readonly = $frmsecelements[7];
										$columntye = $this->gencolumntype($frmsecelements[8]);
										$ddpartable = $frmsecelements[9].'|'.$frmsecelements[10];
										break;

									case 9://Time
										$defvalue = $frmsecelements[3];
										$descp = $frmsecelements[4];
										$mandatory = $frmsecelements[5];
										$active = $frmsecelements[6];
										$readonly = $frmsecelements[7];
										$columntye = $this->gencolumntype($frmsecelements[8]);
										break;

									case 10://Email
										$defvalue = $frmsecelements[3];
										$descp = $frmsecelements[4];
										$mandatory = $frmsecelements[5];
										$active = $frmsecelements[6];
										$readonly = $frmsecelements[7];
										$columntye = $this->gencolumntype($frmsecelements[8]);
										break;

									case 11: //phone
										$defvalue = $frmsecelements[3];
										$descp = $frmsecelements[4];
										$mandatory = $frmsecelements[5];
										$active = $frmsecelements[6];
										$readonly = $frmsecelements[7];
										$columntye = $this->gencolumntype($frmsecelements[8]);
										break;

									case 12: //URL
										$defvalue = $frmsecelements[3];
										$descp = $frmsecelements[4];
										$mandatory = $frmsecelements[5];
										$active = $frmsecelements[6];
										$readonly = $frmsecelements[7];
										$columntye = $this->gencolumntype($frmsecelements[8]);
										break;

									case 13: //check box
										$descp = $frmsecelements[3];
										$mandatory = $frmsecelements[4];
										$active = $frmsecelements[5];
										$readonly = $frmsecelements[6];
										$defvalue = $frmsecelements[7];
										$columntye = $this->gencolumntype($frmsecelements[8]);
										break;

									case 14: //Auto Number
										$defvalue = '';
										$prefix = $frmsecelements[3];
										$autonum = $frmsecelements[4];
										$suffix = $frmsecelements[5];
										$atuonumtype = $frmsecelements[6];
										$descp = $frmsecelements[7];
										$mandatory = $frmsecelements[8];
										$active = $frmsecelements[9];
										$readonly = $frmsecelements[10];
										$columntye = $this->gencolumntype($frmsecelements[11]);
										break;

									case 15: //File Upload
										$defvalue = '';
										$descp = $frmsecelements[3];
										$mandatory = $frmsecelements[4];
										$active = $frmsecelements[5];
										$readonly = $frmsecelements[6];
										$columntye = $this->gencolumntype($frmsecelements[7]);
										break;

									case 16: //Image
										$defvalue = '';
										$descp = $frmsecelements[3];
										$mandatory = $frmsecelements[4];
										$active = $frmsecelements[5];
										$readonly = $frmsecelements[6];
										$columntye = $this->gencolumntype($frmsecelements[7]);
										break;

									case 17://picklist
										$uitypeid = $this->checkdropdowntabletype($database,$tblcolumname);
										$defvalue = $frmsecelements[4]; 
										$descp = $frmsecelements[5];
										$mandatory = $frmsecelements[6];
										$active = $frmsecelements[7];
										$readonly = $frmsecelements[8];
										$ddmultiple = $frmsecelements[9];
										$columntye = $this->gencolumntype($frmsecelements[10]);
										break;

									case 22: //password
										$defvalue = $frmsecelements[3]; 
										$maxlength = $frmsecelements[4];
										$descp = $frmsecelements[5];
										$mandatory = $frmsecelements[6];
										$active = $frmsecelements[7];
										$readonly = $frmsecelements[8];
										$columntye = $this->gencolumntype($frmsecelements[9]);
										break;

									case 24: //Text Editor
										$defvalue = '';
										$descp = $frmsecelements[3];
										$mandatory = $frmsecelements[4];
										$active = $frmsecelements[5];
										$readonly = $frmsecelements[6];
										$columntye = $this->gencolumntype($frmsecelements[7]);
										break;
										
									case 26: //Relation
										$uitypeid = 26;
										$defvalue = '';
										$condname = $frmsecelements[6];
										$condvalue = $frmsecelements[7];
										$condinfo = $frmsecelements[9].'|'.$condname.'|'.$condvalue;
										$ddpartable = $condinfo;
										$descp = $frmsecelements[8];
										$mandatory = $frmsecelements[10];
										$active = $frmsecelements[11];
										$readonly = $frmsecelements[12];
										$ddmultiple = $frmsecelements[13];
										$columntye = $this->gencolumntype($frmsecelements[14]);
										break;

									default :
										break;
								}
								//field create function
								$fildscolumn=array(
									'moduletabid'=>$moduleid,
									'columnname'=>$tblcolumname,
									'tablename'=>$moduletabname,
									'uitypeid'=>$uitypeid,
									'moduletabsectionid'=>$tabsectionid,
									'fieldname'=>$datafieldname,
									'fieldlabel'=>ucwords($frmsecelements[2]),
									'ddparenttable'=>$ddpartable,
									'parenttable'=>$parenttable,
									'readonly'=>$readonly,
									'defaultvalue'=>$defvalue,
									'maximumlength'=>$maxlength,
									'decimalsize'=>$decisize,
									'mandatory'=>$mandatory,
									'active'=>$active,
									'sequence'=>$seqnum,
									'multiple'=>$ddmultiple,
									'fieldcolumntype'=>$columntye,
									'description'=>$descp,
									'userroleid'=>$this->Basefunctions->userroleid
								);
								$newdata = array_merge($fildscolumn,$defdataarr);
								$this->db->insert('modulefield',$newdata);
								$modulefieldid = $this->db->insert_id();
								//insert userrole field mode
								$fildsmode = array(
									'userroleid'=>$this->Basefunctions->userroleid,
									'moduleid'=>$moduleid,
									'modulefieldid'=>$modulefieldid,
									'fieldactive'=>$active,
									'fieldreadonly'=>$readonly
								);
								$newdmodata = array_merge($fildsmode,$defdataarr);
								$this->db->insert('userrolemodulefiled',$newdmodata);
								//view creation fields	
								if($frmsecelements[1] == '17') {
									$tblcolumname = $colname;
									$datatable = $ddtabname;
									$jointable = $ddtabname;
									if($tabgrpvalue[0]==2) {
										if($sectionwithgrid == 'sectionwithgrid'){
											$partable = $datatable;
										}else{
											$partable = $gridtable;
										}											
									} else {
										$partable = $maintable;
									}
								} else {
									$jointable = $maintable;
									if($tabgrpvalue[0]==2) {
										$datatable = $gridtable;
										$partable = $maintable;
									} else {
										$datatable = $maintable;
										$partable = $maintable;
									}
								}
								//auto number table insertion
								if($frmsecelements[1] == '14' ) {
									$angdata = array(
										'moduleid'=>$moduleid,
										'modulefieldid'=>$modulefieldid,
										'suffix'=>$suffix,
										'prefix'=>$prefix,
										'startingnumber'=>$autonum,
										'random'=>$atuonumtype,
										'incrementby'=>0,
										'nextnumber'=>0
									);
									$newangdata = array_merge($angdata,$defdataarr);
									$this->db->insert('serialnumbermaster',$newangdata);
								}
								if($sectionwithgrid == 'sectionwithgrid'){
									//view creation column data insertion
									$uitypeids=array(22,24);
									$viewcolumn=array(
											'viewcreationmoduleid'=>$viewmoduleid,
											'uitypeid'=>$uitypeid,
											'moduletabsectionid'=>$tabsectionid,
											'viewcreationcolmodelname'=>$tblcolumname,
											'viewcreationcolmodelaliasname'=>$tblcolumname,
											'viewcreationcolmodelindexname'=>$tblcolumname,
											'viewcreationcolmodeltable'=>$partable,
											'viewcreationcolmodeljointable'=>$partable,
											'viewcreationcolumnname'=>ucwords($frmsecelements[2]),
											'viewcreationparenttable'=>$partable,
											'viewcreationjoincolmodelname'=>$jointable.'id',
											'viewcreationcolumnviewtype'=>1,
											'viewcreationtype'=>1,
											'viewcreationcolmodelcondname'=>'',
											'viewcreationcolmodelcondvalue'=>'',
											'status'=>1);
									$viewcolumnmerge = array_merge($viewcolumn,$defdataarr);
									$this->db->insert('viewcreationcolumns',$viewcolumnmerge);
									$viewcolid[] = $this->db->insert_id();
								}else{
									//view creation column data insertion
									$uitypeids=array(22,24);
									if(!in_array($uitypeid,$uitypeids)) {
										$viewcolumn=array(
												'viewcreationmoduleid'=>$viewmoduleid,
												'uitypeid'=>$uitypeid,
												'moduletabsectionid'=>$tabsectionid,
												'viewcreationcolmodelname'=>$tblcolumname,
												'viewcreationcolmodelaliasname'=>$tblcolumname,
												'viewcreationcolmodelindexname'=>$tblcolumname,
												'viewcreationcolmodeltable'=>$datatable,
												'viewcreationcolmodeljointable'=>$datatable,
												'viewcreationcolumnname'=>ucwords($frmsecelements[2]),
												'viewcreationparenttable'=>$partable,
												'viewcreationjoincolmodelname'=>$jointable.'id',
												'viewcreationcolumnviewtype'=>1,
												'viewcreationtype'=>1,
												'viewcreationcolmodelcondname'=>'',
												'viewcreationcolmodelcondvalue'=>'',
												'status'=>1);
										$viewcolumnmerge = array_merge($viewcolumn,$defdataarr);
										$this->db->insert('viewcreationcolumns',$viewcolumnmerge);
										if($tabgrpvalue[0]!=2) {
											$viewcolid[] = $this->db->insert_id();
										}
									}
								}
								
							}
						}
					}
				}
			}
			$ii=0;
			foreach($editorfname as $colname) {
				$seqid = $this->Basefunctions->maxidwithcond('modulefield','sequence','moduletabsectionid',$editortabsecids[$ii]);
				$seqnum = ++$seqid;
				$fildscolumn=array(
					'moduletabid'=>$moduleid,
					'columnname'=>$colname,
					'tablename'=>$editortablenames[$ii],
					'uitypeid'=>3,
					'fieldname'=>$colname,
					'fieldlabel'=>ucwords('File Name'),
					'defaultvalue'=>'',
					'maximumlength'=>100,
					'decimalsize'=>0,
					'mandatory'=>'No',
					'active'=>'No',
					'readonly'=>'No',
					'sequence'=>$seqnum,
					'multiple'=>'No',
					'parenttable'=>$editortablenames[$ii],
					'ddparenttable'=>'',
					'fieldcolumntype'=>1,
					'description'=>'',
					'moduletabsectionid'=>$editortabsecids[$ii],
					'userroleid'=>$this->Basefunctions->userroleid
				);
				$newdata = array_merge($fildscolumn,$defdataarr);
				$newdata['status'] = 10;
				$this->db->insert('modulefield',$newdata);
				$editormodulefieldid = $this->db->insert_id();
				//insert userrole field mode
				$editorfildsmode = array(
						'userroleid'=>$this->Basefunctions->userroleid,
						'moduleid'=>$moduleid,
						'modulefieldid'=>$editormodulefieldid,
						'fieldactive'=>'No',
						'fieldreadonly'=>'Yes'
				);
				$editornewdmodata = array_merge($editorfildsmode,$defdataarr);
				$this->db->insert('userrolemodulefiled',$editornewdmodata);
				$ii++;
			}
			if(count($viewcolid) > 0) {
				$viewids = array_slice($viewcolid,0,10); 
				$vids = implode(',',$viewids);
				$colcount = count(explode(',',$vids));
				$colsize = array();
				for($k=0;$k<$colcount;$k++) {
					$colsize[$k] = 200;
				}
				$columnsize = implode(',',$colsize);
				//view insertion
				$vdatas=array(
					'viewcreationname'=>ucwords($modulename),
					'viewcreationcolumnid'=>$vids,
					'viewcreationmoduleid'=>$viewmoduleid,
					'viewaspublic'=>'Yes',
					'viewcolumnsize'=>$columnsize,
					'createdate' => date($this->Basefunctions->datef),
					'lastupdatedate' => date($this->Basefunctions->datef),
					'createuserid' => 1,
					'lastupdateuserid' => $this->Basefunctions->userid,
					'status' => 1
				);
				$this->db->insert('viewcreation',$vdatas);
				$vmaxid = $this->db->insert_id();
				$defview=array(
					'employeeid'=>$this->Basefunctions->userid,
					'moduleid'=>$moduleid,
					'viewcreationid'=>$vmaxid,
					'Status'=>1
				);
				$this->db->insert('datapreference',$defview);
			}
			if($sectionwithgrid == 'sectionwithgrid') {
				$jsresult = $this->gridscriptgenerate($modulename,1,$dynamicgridname,$moduleid,$moduletabname);
				$contresult = $this->gridcontroller($modulename,$moduleid);
				$modlresult = $this->gridmodelgenerate($modulename);
				$viewresult = $this->gridmainviewgenerate($modulename,$moduleid);
				$viewcfresult = $this->gridformgenerate($modulename,$resttab);
			} else {
				$jsresult = $this->scriptgenerate($modulename,1,$dynamicgridname,$moduleid,$moduletabname);
				$contresult = $this->controllergenerate($modulename,$moduleid,$dynamicgridname);
				$modlresult = $this->modelgenerate($modulename,$dynamicgridname);
				$viewresult = $this->mainviewgenerate($modulename,$moduleid);
				$viewcfresult = $this->datacreationformgenerate($modulename,$resttab);
			}
			echo "Success";
		} else { echo "Fail"; }
	}
	//js file
	public function scriptgenerate($modulename,$type,$dynamicgridname,$modlid,$moduletabname) {
		$modname = preg_replace('/[^A-Za-z0-9]/', '', $modulename);
		$modlname = ucfirst($modname);
		$jfoldpath = "js".DIRECTORY_SEPARATOR.$modlname;
		if(!is_dir($jfoldpath)) {
			$dirsucc = mkdir($jfoldpath,0777,TRUE);
		}
		$jfname=$modname.".js";
		$file=$jfoldpath.DIRECTORY_SEPARATOR.$jfname;
		$cont="$(document).ready(function(){".PHP_EOL;
		$cont.="\tMETHOD ='ADD';".PHP_EOL;
		$cont.="\t$(document).foundation();".PHP_EOL;
		$cont.="\tmaindivwidth();".PHP_EOL;
		$cont.="\t{".PHP_EOL;
		$cont.="\t\t".$modname."addgrid();".PHP_EOL;		
		if(count($dynamicgridname)>0) {
			$m=1;
			foreach($dynamicgridname as $tabgrpname) {
				$name=substr(strtolower($tabgrpname),0,3);
				$grdtabname = preg_replace('/[^A-Za-z0-9]/', '', $name);
				$grdname = strtolower($grdtabname);
				$cont.="\t\t".$modname.$grdname."addgrid".$m."();".PHP_EOL;				
				$m++;
			}
		}		
		$cont.="\t}".PHP_EOL;
		$cont.="\t{".PHP_EOL;
		$cont.="\t\tvar editorname = $('#editornameinfo').val();".PHP_EOL;
		$cont.="\t\tfroalaarray=[editorname,'html.set',''];".PHP_EOL;
		$cont.="\t}".PHP_EOL;
		$cont.=$this->dynamicformclose($modname);
		$cont.=$this->dynamicdatareload($modname);
		$cont.=$this->acteventgenerate($modname,$type,$modlid);
		$cont.=$this->dataeventgenerate($modname,$type);
		if(count($dynamicgridname)>0) {
			$m=1;
			foreach($dynamicgridname as $tabid => $tabgrpname) {
				$name=substr(strtolower($tabgrpname),0,3);
				$grdtabname = preg_replace('/[^A-Za-z0-9]/', '', $name);
				$grdname = strtolower($grdtabname);
				$cont.="$('#".$modname.$grdtabname."ingrideditspan".$m."').removeAttr('style');".PHP_EOL;
				$cont.=$this->dynamicformgridactiongen($modname,$modlid,$grdname,$tabid,$m);
				$m++;
			}
			$cont.=$this->dynamicfrmtogrddatagenerate();
		}
		$cont.=$this->checkboxeventgenerate();
		$cont.=$this->filtergenerationcode($modname,$moduletabname);
		$cont.="});".PHP_EOL;
		$cont.=$this->validationfunctiongenerate();
		$cont.=$this->dynamicgdgenerate($modname,$moduletabname);		
		if(count($dynamicgridname)>0) {
			$m=1;
			foreach($dynamicgridname as $tabid => $tabgrpname) {
				$name=substr(strtolower($tabgrpname),0,3);
				$grdtabname = preg_replace('/[^A-Za-z0-9]/', '', $name);
				$grdname = strtolower($grdtabname);
				$cont.=$this->dynamiclocalgdgenerate($modname,$modlid,$grdname,$tabid,$m);				
				$m++;
			}
		}
		$cont.="function refreshgrid() {".PHP_EOL;
		$cont.="\t\tvar page = $(this).data('pagenum');".PHP_EOL;
		$cont.="\t\tvar rowcount = $('ul#".$modname."spgnumcnt li .page-text .active').data('rowcount');".PHP_EOL;
		$cont.="\t\t".$modname."addgrid(page,rowcount);".PHP_EOL;
		$cont.="}".PHP_EOL;
		$cont.="{".PHP_EOL;
		$cont.="\tfunction clearformgriddata() {".PHP_EOL;
		$cont.="\t\t\tvar gridname = $('#gridnameinfo').val();".PHP_EOL;
		$cont.="\t\t\tif(gridname){".PHP_EOL;
		$cont.="\t\t\tvar gridnames = gridname.split(',');".PHP_EOL;
		$cont.="\t\t\tvar datalength = gridnames.length;".PHP_EOL;
		$cont.="\t\t\tfor(var j=0;j<datalength;j++) {".PHP_EOL;
		$cont.="\t\t\t\tcleargriddata(gridnames[j]);".PHP_EOL;
		$cont.="\t\t\t}".PHP_EOL;
		$cont.="\t\t}".PHP_EOL;
		$cont.="\t}".PHP_EOL;
		$cont.="}".PHP_EOL;
		$cont.="function ".$modname."clonedatafetchfun(datarowid) {".PHP_EOL;
		$cont.="\t\tclearformgriddata();".PHP_EOL;
		$cont.="\t\t$('.addbtnclass').removeClass('hidedisplay');".PHP_EOL;
		$cont.="\t\t$('.updatebtnclass').addClass('hidedisplay');".PHP_EOL;
		$cont.="\t\t$('#formclearicon').hide();".PHP_EOL;
		$cont.="\t\tresetFields();".PHP_EOL;			
		$cont.="\t\t".$modname."dataupdateinfofetchfun(datarowid);".PHP_EOL;
		$cont.="\t\tfirstfieldfocus();".PHP_EOL;
		//For Keyboard Short cut Variables
		$cont.="\t\taddformupdate = 1;".PHP_EOL;
		$cont.="\t\tviewgridview = 0;".PHP_EOL;
		$cont.="\t\taddformview = 1;".PHP_EOL;
		//For Left Menu Confirmation
		$cont.="\t\tdiscardmsg = 1;".PHP_EOL;
		$cont.="}".PHP_EOL;
		$cont.=$this->dynamicdataaddfungen($modname,$type);
		$cont.=$this->dynamicdataupinfofungen($modname,$dynamicgridname);
		$cont.=$this->dynamicdataupdatefungen($modname,$type);
		$cont.=$this->dynamicdatadelfungen($modname);
		$cont.=$this->viewsuccreload();
		//$cont.="</script>";
		write_file($file,$cont);
	}
	//controller
	public function controllergenerate($modulename,$moduleid,$dynamicgridname) {
		$modname = preg_replace('/[^A-Za-z0-9]/', '', $modulename);
		$modlname = ucfirst($modname);
		$cpath = 'application'.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.$modlname.DIRECTORY_SEPARATOR.'controllers';
		if(!is_dir($cpath)) {
			$dirsucc = mkdir($cpath,0777,TRUE);
		}
		$a="$";
		$cfname=$modlname.".php";
		$cfile=$cpath.DIRECTORY_SEPARATOR.$cfname;
		$ccont="<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');".PHP_EOL;
		$ccont.="class ".ucwords($modlname)." extends MX_Controller {".PHP_EOL;
		$ccont.="\tfunction __construct() {".PHP_EOL;
		$ccont.="\t\tparent::__construct();".PHP_EOL;
		$ccont.="\t\t".$a."this->load->helper('formbuild');".PHP_EOL;
		$ccont.="\t\t".$a."this->load->model('".$modlname."/".$modlname."model');".PHP_EOL;
		$ccont.="\t\t".$a."this->load->view('Base/formfieldgeneration');".PHP_EOL;		
		$ccont.="\t}".PHP_EOL;
		$ccont.=$this->indexgenerate($modlname,$modname,$moduleid);
		$ccont.=$this->actionfungenerate($modlname,$moduleid,$dynamicgridname);
		$ccont.="}";
		write_file($cfile,$ccont);
	}
	//model generate
	public function modelgenerate($modulename,$dynamicgridname) {
		$modname = preg_replace('/[^A-Za-z0-9]/', '', $modulename);
		$modlname = ucfirst($modname);
		$mpath = 'application'.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.$modlname.DIRECTORY_SEPARATOR.'models';
		if(!is_dir($mpath)) {
			$dirsucc = mkdir($mpath,0777,TRUE);
		}
		$a="$";
		$mfname=$modlname."model.php";
		$mfile=$mpath.DIRECTORY_SEPARATOR.$mfname;
		$mcont="<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');".PHP_EOL;
		$mcont.="class ".ucwords($modlname)."model extends CI_Model {".PHP_EOL;
		$mcont.="\tpublic function __construct() {".PHP_EOL;
		$mcont.="\t\tparent::__construct();".PHP_EOL;
		$mcont.="\t\t".$a."this->load->model('Base/Basefunctions');".PHP_EOL;
		$mcont.="\t\t".$a."this->load->model('Base/Crudmodel');".PHP_EOL;		
		$mcont.="\t}".PHP_EOL;
		$mcont.=$this->datacreatemodelgenerate();
		$mcont.=$this->datainfogetmodelgenerate();
		$mcont.=$this->dataupdatemodelgenerate();
		$mcont.=$this->datadeletemodelgenerate();
		if(count($dynamicgridname)>0) {
			$m=1;
			foreach($dynamicgridname as $tabid => $tabgrpname) {
				$name=substr(strtolower($tabgrpname),0,3);
				$grdtabname = preg_replace('/[^A-Za-z0-9]/', '', $name);
				$tabgrpname = preg_replace('/[^A-Za-z0-9]/', '', $tabgrpname);
				$grdname = strtolower($modlname).$name."addgrid".$m;
				$mcont.="public function ".$grdname."detailfetchmodel(){".PHP_EOL;
				$mcont.="\t".$a."formfieldsname = explode(',',".$a."_GET['fieldname']);".PHP_EOL;
				$mcont.="\t".$a."id =".$a."_GET['primarydataid'];".PHP_EOL;
				$mcont.="\t".$a."i=0;".PHP_EOL;
				$mcont.="\t".$a."data = ".$a."this->db->query('select '.".$a."_GET['fieldname'].' from " .strtolower($modlname).strtolower($tabgrpname)."details where " .strtolower($modlname)."id ='.".$a."id.' and status=1');".PHP_EOL;
				$mcont.="\tforeach (".$a."data->result() as ".$a."vals) {".PHP_EOL;
				$mcont.="\t\tforeach(".$a."formfieldsname as ".$a."datanaems) {".PHP_EOL;
				$mcont.="\t\t\t".$a."datafield[]=".$a."vals->".$a."datanaems;".PHP_EOL;
				$mcont.="\t\t}".PHP_EOL;
				$mcont.="\t\t".$a."productdetail->rows[".$a."i]['id'] = ".$a."i;".PHP_EOL;
				$mcont.="\t\t".$a."productdetail->rows[".$a."i]['cell']= ".$a."datafield;".PHP_EOL;
				$mcont.="\t\t".$a."datafield =[];".PHP_EOL;
				$mcont.="\t\t".$a."i++;".PHP_EOL;
				$mcont.="\t}".PHP_EOL;
				$mcont.="\techo  json_encode(".$a."productdetail);".PHP_EOL;
				$mcont.="\t}".PHP_EOL;
				$m++;
			}
		}		
		$mcont.="}";
		write_file($mfile,$mcont);
	}
	//model generate
	public function gridmodelgenerate($modulename) {
		$modname = preg_replace('/[^A-Za-z0-9]/', '', $modulename);
		$modlname = ucfirst($modname);
		$mpath = 'application'.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.$modlname.DIRECTORY_SEPARATOR.'models';
		if(!is_dir($mpath)) {
			$dirsucc = mkdir($mpath,0777,TRUE);
		}
		$a="$";
		$mfname=$modlname."model.php";
		$mfile=$mpath.DIRECTORY_SEPARATOR.$mfname;
		$mcont="<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');".PHP_EOL;
		$mcont.="class ".ucwords($modlname)."model extends CI_Model {".PHP_EOL;
		$mcont.="\tpublic function __construct() {".PHP_EOL;
		$mcont.="\t\tparent::__construct();".PHP_EOL;
		$mcont.="\t\t".$a."this->load->model('Base/Basefunctions');".PHP_EOL;
		$mcont.="\t\t".$a."this->load->model('Base/Crudmodel');".PHP_EOL;
		$mcont.="\t}".PHP_EOL;
		$mcont.=$this->griddatacreatemodelgenerate($modulename);
		$mcont.=$this->datainfogetmodelgenerate();
		$mcont.=$this->griddataupdatemodelgenerate($modulename);
		$mcont.=$this->datadeletemodelgenerate();
		$mcont.="}";
		write_file($mfile,$mcont);
	}
	//main view generate
	public function mainviewgenerate($modulename,$moduleid) {
		$modname = preg_replace('/[^A-Za-z0-9]/', '', $modulename);
		$modlname = ucfirst($modname);
		$vpath = 'application'.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.$modlname.DIRECTORY_SEPARATOR.'views';
		if(!is_dir($vpath)) {
			$dirsucc = mkdir($vpath,0777,TRUE);
		}
		$a="$";
		$vfname=$modname."view.php";
		$vfile=$vpath.DIRECTORY_SEPARATOR.$vfname;
		$vcont="<!DOCTYPE html>".PHP_EOL;
		$vcont.="<html lang='en'>".PHP_EOL;
		$vcont.=$this->viewheadcreate($modname);
		$vcont.=$this->viewbodycreate($modname,$moduleid);
		$vcont.="</html>".PHP_EOL;
		write_file($vfile,$vcont);
	}
	//creation form generate
	public function datacreationformgenerate($modulename,$resttab) {
		$modname = preg_replace('/[^A-Za-z0-9]/', '', $modulename);
		$modlname = ucfirst($modname);
		$vpath = 'application'.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.$modlname.DIRECTORY_SEPARATOR.'views';
		if(!is_dir($vpath)) {
			$dirsucc = mkdir($vpath,0777,TRUE);
		}
		$a="$";
		$vcfname=$modname."creationform.php";
		$vcfile=$vpath.DIRECTORY_SEPARATOR.$vcfname;
		$vcfcont="<div class='large-12 columns paddingzero formheader'>".PHP_EOL;
		$vcfcont.="\t<?php".PHP_EOL;
		$vcfcont.="\t\t".$a."this->load->view('Base/mainviewaddformheader');".PHP_EOL;
		$vcfcont.="\t?>".PHP_EOL;
		//$vcfcont.=$this->vcformhedergenerate($modname);
		//$vcfcont.=$this->vcformtabgrpgenerate($modname);
		$vcfcont.="</div>".PHP_EOL;
		$vcfcont.="<div class='large-12 columns addformunderheader'>&nbsp;</div>".PHP_EOL;
		$vcfcont.=$this->vcformcontgenerate($modname,$resttab);
		write_file($vcfile,$vcfcont);
	}
	public function dynamicformclose($modlname) {
		$dfccont="\t{".PHP_EOL;
		$dfccont.="\t\tvar addclose".$modlname."creation =['closeaddform','".$modlname."creationview','".$modlname."creationformadd'];".PHP_EOL;
		$dfccont.="\t\taddclose(addclose".$modlname."creation);".PHP_EOL;
		$dfccont.="\t}".PHP_EOL;
		return $dfccont;
	}
	public function dynamicdatareload($modlname) {
		$ddrcont="\t{".PHP_EOL;
		$ddrcont.="\t\t$('#dynamicdddataview').change(function(){".PHP_EOL;
		$ddrcont.="\t\t\t".$modlname."addgrid();".PHP_EOL;
		$ddrcont.="\t\t});".PHP_EOL;
		$ddrcont.="\t}".PHP_EOL;
		return $ddrcont;
	}
	public function acteventgenerate($modlname,$type,$modlid) {
		$actcont="\t{".PHP_EOL;
		$actcont.="\t\tfortabtouch = 0;".PHP_EOL;
		$actcont.="\t}".PHP_EOL;
		$actcont.="\t{".PHP_EOL;
		$addname=$this->eventname($modlname,$type,1);
		$actcont.="\t\t$('#".$addname."').click(function(){".PHP_EOL;
		$sldupname=$this->slideupevent($modlname);
		$actcont.="\t\t\t".$sldupname.PHP_EOL;
		$actcont.="\t\t\tresetFields();".PHP_EOL;	
		$actcont.="\t\t\tclearformgriddata();".PHP_EOL;
		$btnshow=$this->addremoveclass('.addbtnclass',1);
		$actcont.="\t\t\t".$btnshow.PHP_EOL;
		$btnhide=$this->addremoveclass('.updatebtnclass',0);
		$actcont.="\t\t\t".$btnhide.PHP_EOL;
		$actcont.="\t\t\tshowhideiconsfun('addclick','".$modlname."creationformadd');".PHP_EOL;
		$actcont.="\t\t\tfirstfieldfocus();".PHP_EOL;
		$actcont.="\t\t\tvar elementname = $('#elementsname').val();".PHP_EOL;
		$actcont.="\t\t\telementdefvalueset(elementname);".PHP_EOL;
		$actcont.="\t\t\trandomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');".PHP_EOL;
		$actcont.="\t\t\tfroalaset(froalaarray);".PHP_EOL;
		$actcont.="\t\t\tMaterialize.updateTextFields();".PHP_EOL;
		$actcont.="\t\t\t$('#tabgropdropdown').select2('val','1');".PHP_EOL;
		$actcont.="\t\t\tfortabtouch = 0;".PHP_EOL;
		$actcont.="\t\t\tviewgridview = 0;".PHP_EOL;
		$actcont.="\t\t\taddformview = 1;".PHP_EOL;
		$actcont.="\t\t\tOperation = 0;".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$editname=$this->eventname($modlname,$type,2);
		$actcont.="\t\t$('#".$editname."').click(function(){".PHP_EOL;
		$actcont.="\t\t\tvar datarowid = $('#".$modlname."addgrid div.gridcontent div.active').attr('id');".PHP_EOL;
		$actcont.="\t\t\tif (datarowid) {".PHP_EOL;
		$actcont.="\t\t\t\tclearformgriddata();".PHP_EOL;
		$sldupname=$this->slideupevent($modlname);
		$actcont.="\t\t\t\t".$sldupname.PHP_EOL;
		$btnshow=$this->addremoveclass('.updatebtnclass',1);
		$actcont.="\t\t\t\t".$btnshow.PHP_EOL;
		$btnhide=$this->addremoveclass('.addbtnclass',0);
		$actcont.="\t\t\t\t".$btnhide.PHP_EOL;
		$actcont.="\t\t\t\tfroalaset(froalaarray);".PHP_EOL;
		$actcont.="\t\t\t\t".$modlname."dataupdateinfofetchfun(datarowid);".PHP_EOL;
		$actcont.="\t\t\t\tfirstfieldfocus();".PHP_EOL;
		$actcont.="\t\t\t\tshowhideiconsfun('editclick','".$modlname."creationformadd');".PHP_EOL;
		$actcont.="\t\t\t\tfortabtouch = 1;".PHP_EOL;
		$actcont.="\t\t\t\tOperation = 1;".PHP_EOL;
		$actcont.="\t\t\t\tMaterialize.updateTextFields();".PHP_EOL;
		$actcont.="\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\talertpopup('Please select a row');".PHP_EOL;
		$actcont.="\t\t\t}".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$delname=$this->eventname($modlname,$type,3);
		$actcont.="\t\t$('#".$delname."').click(function(){".PHP_EOL;
		$actcont.="\t\t\tvar datarowid = $('#".$modlname."addgrid div.gridcontent div.active').attr('id');".PHP_EOL;
		$actcont.="\t\t\tif (datarowid) {".PHP_EOL;
		$actcont.="\t\t\t\t$('#basedeleteoverlay').fadeIn();".PHP_EOL;
		$actcont.="\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\talertpopup('Please select a row');".PHP_EOL;
		$actcont.="\t\t\t}".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$reloadname=$this->eventname($modlname,$type,4);
		$actcont.="\t\t$('#".$reloadname."').click(function(){".PHP_EOL;
		$actcont.="\t\t\trefreshgrid();".PHP_EOL;
		$actcont.="\t\t\tMaterialize.updateTextFields();".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$actcont.="\t\t$(' window ').resize(function(){".PHP_EOL;
		$actcont.="\t\t\tmaingridresizeheightset(".$modlname."addgrid);".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$actcont.="\t\t$('#formclearicon').click(function(){".PHP_EOL;
		$actcont.="\t\t\tvar elementname = $('#elementsname').val();".PHP_EOL;
		$actcont.="\t\t\telementdefvalueset(elementname);".PHP_EOL;
		$actcont.="\t\t\trandomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');".PHP_EOL;
		$actcont.="\t\t\tfroalaset(froalaarray);".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$actcont.="\t\t$('#basedeleteyes').click(function(){".PHP_EOL;
		$actcont.="\t\t\tvar datarowid = $('#".$modlname."addgrid div.gridcontent div.active').attr('id');".PHP_EOL;
		$actcont.="\t\t\t".$modlname."recorddelete(datarowid);".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$detailname=$this->eventname($modlname,$type,5);
		$actcont.="\t\t$('#".$detailname."').click(function() {".PHP_EOL;
		$actcont.="\t\t\tvar datarowid = $('#".$modlname."addgrid div.gridcontent div.active').attr('id');".PHP_EOL;
		$actcont.="\t\t\tif (datarowid) {".PHP_EOL;
		$actcont.="\t\t\t\t$('#processoverlay').show();".PHP_EOL;
		$actcont.="\t\t\t\t$('.addbtnclass').addClass('hidedisplay');".PHP_EOL;
		$actcont.="\t\t\t\t$('.updatebtnclass').removeClass('hidedisplay');".PHP_EOL;
		$actcont.="\t\t\t\t$('#formclearicon').hide();".PHP_EOL;
		$actcont.="\t\t\t\tresetFields();".PHP_EOL;
		$actcont.="\t\t\t\tfroalaset(froalaarray);".PHP_EOL;
		$actcont.="\t\t\t\t".$modlname."dataupdateinfofetchfun(datarowid);".PHP_EOL;
		$actcont.="\t\t\t\tshowhideiconsfun('summryclick','".$modlname."creationformadd');".PHP_EOL;
		$actcont.="\t\t\t\tMaterialize.updateTextFields();".PHP_EOL;
		$actcont.="\t\t\t\t$('#processoverlay').hide();".PHP_EOL;
		$actcont.="\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\talertpopup('Please select a row');".PHP_EOL;
		$actcont.="\t\t\t}".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$actcont.="\t\t$('#editfromsummayicon').click(function() {".PHP_EOL;
		$actcont.="\t\t\t\tshowhideiconsfun('editfromsummryclick','".$modlname."creationformadd');".PHP_EOL;
		$actcont.="\t\t\t\tfirstfieldfocus();".PHP_EOL;
		$actcont.="\t\t\t\tMaterialize.updateTextFields();".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$dashboardname=$this->eventname($modlname,$type,6);
		$actcont.="\t\t$('#".$dashboardname."').click(function() {".PHP_EOL;
		$actcont.="\t\t\tvar datarowid = $('#".$modlname."addgrid div.gridcontent div.active').attr('id');".PHP_EOL;
		$actcont.="\t\t\tif (datarowid) {".PHP_EOL;
		$actcont.="\t\t\t\t$('#processoverlay').show();".PHP_EOL;
		$actcont.="\t\t\t\t$.getScript(base_url+'js/plugins/uploadfile/jquery.uploadfile.min.js');".PHP_EOL;
		$actcont.="\t\t\t\t$.getScript(base_url+'js/plugins/zingchart/zingchart.min.js');".PHP_EOL;
		$actcont.="\t\t\t\t$.getScript('js/Home/home.js',function(){".PHP_EOL;
		$actcont.="\t\t\t\t\tdynamicchartwidgetcreate(datarowid);".PHP_EOL;
		$actcont.="\t\t\t\t});".PHP_EOL;
		$actcont.="\t\t\t\t	$('#dashboardcontainer').removeClass('hidedisplay');".PHP_EOL;
		$actcont.="\t\t\t\t	$('.actionmenucontainer').addClass('hidedisplay');".PHP_EOL;
		$actcont.="\t\t\t\t	$('.fullgridview').addClass('hidedisplay');".PHP_EOL;
		$actcont.="\t\t\t\t	$('.footercontainer').addClass('hidedisplay');".PHP_EOL;
		$actcont.="\t\t\t\t	$('.forgetinggridname').addClass('hidedisplay');".PHP_EOL;
		$actcont.="\t\t\t\t	setTimeout(function() {".PHP_EOL;
		$actcont.="\t\t\t\t	$('#processoverlay').hide();".PHP_EOL;
		$actcont.="\t\t\t},1000);".PHP_EOL;
		$actcont.="\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\talertpopup('Please select a row');".PHP_EOL;
		$actcont.="\t\t\t}".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$actcont.="\t\t".PHP_EOL;
		$printname=$this->eventname($modlname,$type,7);
		$actcont.="\t\t$('#".$printname."').click(function() {".PHP_EOL;
		$actcont.="\t\t\tvar datarowid = $('#".$modlname."addgrid div.gridcontent div.active').attr('id');".PHP_EOL;
		$actcont.="\t\t\tif (datarowid) {".PHP_EOL;
		$actcont.="\t\t\t\t$('#printpdfid').val(datarowid);".PHP_EOL;
		$actcont.="\t\t\t\t$('#templatepdfoverlay').fadeIn();".PHP_EOL;
		$actcont.="\t\t\t\tvar viewfieldids = $('#viewfieldids').val();".PHP_EOL;
		$actcont.="\t\t\t\tpdfpreviewtemplateload(viewfieldids);".PHP_EOL;
		$actcont.="\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\talertpopup('Please select a row');".PHP_EOL;
		$actcont.="\t\t\t}".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$clonename=$this->eventname($modlname,$type,8);
		$actcont.="\t\t$('#".$clonename."').click(function() {".PHP_EOL;
		$actcont.="\t\t\tvar datarowid = $('#".$modlname."addgrid div.gridcontent div.active').attr('id');".PHP_EOL;
		$actcont.="\t\t\tif (datarowid) {".PHP_EOL;
		$actcont.="\t\t\t\t$('#processoverlay').show();".PHP_EOL;
		$actcont.="\t\t\t\t$('.addbtnclass').removeClass('hidedisplay');".PHP_EOL;
		$actcont.="\t\t\t\t$('.updatebtnclass').addClass('hidedisplay');".PHP_EOL;
		$actcont.="\t\t\t\t$('.editformsummmarybtnclass').addClass('hidedisplay');".PHP_EOL;
		$actcont.="\t\t\t\tshowhideiconsfun('editclick','".$modlname."creationformadd');".PHP_EOL;
		$actcont.="\t\t\t\t$('#formclearicon').hide();".PHP_EOL;
		$actcont.="\t\t\t\t".$modlname."clonedatafetchfun(datarowid);".PHP_EOL;
		$actcont.="\t\t\t\t$('#tabgropdropdown').select2('val','1');".PHP_EOL;
		$actcont.="\t\t\t\trandomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');".PHP_EOL;
		$actcont.="\t\t\t\tOperation = 0;".PHP_EOL;
		$actcont.="\t\t\t\tMaterialize.updateTextFields();".PHP_EOL;
		$actcont.="\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\t	alertpopup('Please select a row');".PHP_EOL;
		$actcont.="\t\t\t}".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$smsname=$this->eventname($modlname,$type,9);
		$actcont.="\t\t$('#".$smsname."').click(function() {".PHP_EOL;
		$actcont.="\t\t\tvar datarowid = $('#".$modlname."addgrid div.gridcontent div.active').attr('id');".PHP_EOL;
		$actcont.="\t\t\tif (datarowid) {".PHP_EOL;
		$actcont.="\t\t\t\tvar viewfieldids = $('#viewfieldids').val();".PHP_EOL;
		$actcont.="\t\t\t\t$.ajax({".PHP_EOL;
		$actcont.="\t\t\t\t\turl:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,".PHP_EOL;
		$actcont.="\t\t\t\t\tdataType:'json',".PHP_EOL;
		$actcont.="\t\t\t\t\tasync:false,".PHP_EOL;
		$actcont.="\t\t\t\t\tcache:false,".PHP_EOL;
		$actcont.="\t\t\t\t\tsuccess :function(data) {".PHP_EOL;
		$actcont.="\t\t\t\t\t\tmobilenumber = [];".PHP_EOL;
		$actcont.="\t\t\t\t\t\tfor(var i=0;i<data.length;i++) {".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\tif(data[i] != '') {".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\tmobilenumber.push(data[i]);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t}".PHP_EOL;
		$actcont.="\t\t\t\t\t\t}".PHP_EOL;
		$actcont.="\t\t\t\t\t\tif(mobilenumber.length > 1) {".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t$('#mobilenumbershow').show();".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t$('#smsmoduledivhid').hide();".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t$('#smsrecordid').val(datarowid);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t$('#smsmoduleid').val(viewfieldids);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\tmobilenumberload(mobilenumber,'smsmobilenumid');".PHP_EOL;
		$actcont.="\t\t\t\t\t\t}else if(mobilenumber.length == 1) {".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\tsessionStorage.setItem('mobilenumber',mobilenumber);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\tsessionStorage.setItem('viewfieldids',viewfieldids);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\tsessionStorage.setItem('recordis',datarowid);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\twindow.location = base_url+'Sms';".PHP_EOL;
		$actcont.="\t\t\t\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\talertpopup('Invalid mobile number');".PHP_EOL;
		$actcont.="\t\t\t\t\t\t}".PHP_EOL;
		$actcont.="\t\t\t\t\t},".PHP_EOL;
		$actcont.="\t\t\t\t});".PHP_EOL;
		$actcont.="\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\talertpopup('Please select a row');".PHP_EOL;
		$actcont.="\t\t\t}".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$mailname=$this->eventname($modlname,$type,10);
		$actcont.="\t\t$('#".$mailname."').click(function() {".PHP_EOL;			
		$actcont.="\t\t\tvar datarowid = $('#".$modlname."addgrid div.gridcontent div.active').attr('id');".PHP_EOL;
		$actcont.="\t\t\tif (datarowid)".PHP_EOL;
		$actcont.="\t\t\t{".PHP_EOL;
		$actcont.="\t\t\t\tvar emailtosend = getvalidemailid(datarowid,'".$modlname."','emailid','','');".PHP_EOL;
		$actcont.="\t\t\t\tsessionStorage.setItem('forcomposemailid',emailtosend.emailid);".PHP_EOL;
		$actcont.="\t\t\t\tsessionStorage.setItem('forsubject',emailtosend.subject);".PHP_EOL;
		$actcont.="\t\t\t\tsessionStorage.setItem('moduleid','".$modlid."');".PHP_EOL;
		$actcont.="\t\t\t\tsessionStorage.setItem('recordid',datarowid);".PHP_EOL;
		$actcont.="\t\t\t\tvar fullurl = 'erpmail/?_task=mail&_action=compose';".PHP_EOL;
		$actcont.="\t\t\t\twindow.open(''+base_url+''+fullurl+'');".PHP_EOL;		
		$actcont.="\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\talertpopup('Please select a row');".PHP_EOL;
		$actcont.="\t\t\t}".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$actcont.="\t\t$('#smsmobilenumid').change(function() {".PHP_EOL;
		$actcont.="\t\t\tvar data = $('#smsmobilenumid').select2('data');".PHP_EOL;
		$actcont.="\t\t\tvar finalResult = [];".PHP_EOL;
		$actcont.="\t\t\tfor( item in $('#smsmobilenumid').select2('data') ) {".PHP_EOL;
		$actcont.="\t\t\t\tfinalResult.push(data[item].id);".PHP_EOL;
		$actcont.="\t\t\t};".PHP_EOL;
		$actcont.="\t\t\tvar selectid = finalResult.join(',');".PHP_EOL;
		$actcont.="\t\t\t$('#smsmobilenum').val(selectid);".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$actcont.="\t\t$('#mobilenumbersubmit').click(function() {".PHP_EOL;
		$actcont.="\t\t\tvar datarowid = $('#smsrecordid').val();".PHP_EOL;
		$actcont.="\t\t\tvar viewfieldids =$('#smsmoduleid').val();".PHP_EOL;
		$actcont.="\t\t\tvar mobilenumber =$('#smsmobilenum').val();".PHP_EOL;
		$actcont.="\t\t\tif(mobilenumber != '' || mobilenumber != null) {".PHP_EOL;
		$actcont.="\t\t\t\tsessionStorage.setItem('mobilenumber',mobilenumber);".PHP_EOL;
		$actcont.="\t\t\t\tsessionStorage.setItem('viewfieldids',viewfieldids);".PHP_EOL;
		$actcont.="\t\t\t\tsessionStorage.setItem('recordis',datarowid);".PHP_EOL;
		$actcont.="\t\t\t\twindow.location = base_url+'Sms';".PHP_EOL;
		$actcont.="\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\talertpopup('Please Select the mobile number...');".PHP_EOL;
		$actcont.="\t\t\t}".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$actcont.="\t\t$('#clicktocallclose').click(function() {".PHP_EOL;
		$actcont.="\t\t\t$('#clicktocallovrelay').hide();".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$actcont.="\t\t$('#mobilenumberoverlayclose').click(function() {".PHP_EOL;
		$actcont.="\t\t\t$('#mobilenumbershow').hide();".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$outgoingcallname=$this->eventname($modlname,$type,11);
		$actcont.="\t\t$('#".$outgoingcallname."').click(function() {".PHP_EOL;			
		$actcont.="\t\t\tvar datarowid = $('#".$modlname."addgrid div.gridcontent div.active').attr('id');".PHP_EOL;
		$actcont.="\t\t\tif (datarowid) {".PHP_EOL;
		$actcont.="\t\t\t\tvar viewfieldids = $('#viewfieldids').val();".PHP_EOL;
		$actcont.="\t\t\t\t	$.ajax({".PHP_EOL;
		$actcont.="\t\t\t\t\turl:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,".PHP_EOL;
		$actcont.="\t\t\t\t\tdataType:'json',".PHP_EOL;
		$actcont.="\t\t\t\t\tasync:false,".PHP_EOL;
		$actcont.="\t\t\t\t\tcache:false,".PHP_EOL;
		$actcont.="\t\t\t\t\tsuccess :function(data) {".PHP_EOL;
		$actcont.="\t\t\t\t\t\tmobilenumber = [];".PHP_EOL;
		$actcont.="\t\t\t\t\t\tif(data != 'No mobile number') {".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\tfor(var i=0;i<data.length;i++){".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\tif(data[i] != ''){".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\t\tmobilenumber.push(data[i]);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\t}".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t}".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\tif(mobilenumber.length > 1) {".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\t$('#c2cmobileoverlay').show();".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\t$('#callmoduledivhid').hide();".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\t$('#calcount').val(mobilenumber.length);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\t$('#callrecordid').val(datarowid);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\t$('#callmoduleid').val(viewfieldids);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\tmobilenumberload(mobilenumber,'callmobilenum');".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t} else if(mobilenumber.length == 1){".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\tclicktocallfunction(mobilenumber);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\talertpopup('Invalid mobile number');".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t}".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\t$('#c2cmobileoverlay').show();".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\t$('#callmoduledivhid').show();".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\t$('#callrecordid').val(datarowid);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\t$('#callmoduleid').val(viewfieldids);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\tmoduledropdownload(viewfieldids,datarowid,'callmodule');".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t}".PHP_EOL;
		$actcont.="\t\t\t\t\t},".PHP_EOL;
		$actcont.="\t\t\t\t	});".PHP_EOL;
		$actcont.="\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\t	alertpopup('Please select a row');".PHP_EOL;
		$actcont.="\t\t\t}".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$actcont.="\t\t$('#c2cmobileoverlayclose').click(function() {".PHP_EOL;
		$actcont.="\t\t\t$('#c2cmobileoverlay').hide".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$actcont.="\t\t$('#callnumbersubmit').click(function() {".PHP_EOL;
		$actcont.="\t\t\tvar mobilenum = $('#callmobilenum').val();".PHP_EOL;
		$actcont.="\t\t\tif(mobilenum == '' || mobilenum == null) {".PHP_EOL;
		$actcont.="\t\t\t\talertpopup('Please Select the mobile number to call');".PHP_EOL;
		$actcont.="\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\tclicktocallfunction(mobilenum);".PHP_EOL;
		$actcont.="\t\t\t}".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$custname=$this->eventname($modlname,$type,12);
		//Customize icon
		$actcont.="\t\t$('#".$custname."').click(function() {".PHP_EOL;
		$actcont.="\t\t\tvar viewfieldids = $('#viewfieldids').val();".PHP_EOL;
		$actcont.="\t\t\tsessionStorage.setItem('customizeid',viewfieldids);".PHP_EOL;
		$actcont.="\t\t\twindow.location = base_url+'Formeditor';".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$importname=$this->eventname($modlname,$type,13);
		$actcont.="\t\t$('#".$importname."').click(function(){".PHP_EOL;
		$actcont.="\t\t\tvar viewfieldids = $('#viewfieldids').val();".PHP_EOL;
		$actcont.="\t\t\tsessionStorage.setItem('importmoduleid',viewfieldids);".PHP_EOL;
		$actcont.="\t\t\twindow.location = base_url+'Dataimport';".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$datamaniname=$this->eventname($modlname,$type,14);
		//data manipulation icon
		$actcont.="\t\t$('#".$datamaniname."').click(function() {".PHP_EOL;
		$actcont.="\t\t\tvar viewfieldids = $('#viewfieldids').val();".PHP_EOL;
		$actcont.="\t\t\tsessionStorage.setItem('datamanipulateid',viewfieldids);".PHP_EOL;
		$actcont.="\t\t\twindow.location = base_url+'Massupdatedelete';".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$findname=$this->eventname($modlname,$type,15);
		//Find Duplication icon
		$actcont.="\t\t$('#".$findname."').click(function() {".PHP_EOL;
		$actcont.="\t\t\tvar viewfieldids = $('#viewfieldids').val();".PHP_EOL;
		$actcont.="\t\t\tsessionStorage.setItem('finddubid',viewfieldids);".PHP_EOL;
		$actcont.="\t\t\twindow.location = base_url+'Findduplicates';".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$actcont.="\t}".PHP_EOL;
		return $actcont;
	}
	//main view filter generate
	public function filtergenerationcode($modname,$moduletabname){
		$cont="{".PHP_EOL;
		$cont.="filtername = [];".PHP_EOL;
		$cont.="\t$('#filteraddcondsubbtn').click(function() {".PHP_EOL;
		$cont.="\t\t$('#filterformconditionvalidation').validationEngine('validate');".PHP_EOL;
		$cont.="\t});".PHP_EOL;
		$cont.="\t$('#filterformconditionvalidation').validationEngine({".PHP_EOL;
		$cont.="\t\tonSuccess: function() {".PHP_EOL;
		$cont.="\t\t\tmainviewfilterwork(".$modname."addgrid);".PHP_EOL;
		$cont.="\t\t},".PHP_EOL;
		$cont.="\t\tonFailure: function() {".PHP_EOL;
		$cont.="\t\t\tvar dropdownid =[];".PHP_EOL;
		$cont.="\t\t\tdropdownfailureerror(dropdownid);".PHP_EOL;
		$cont.="\t\t\talertpopup(validationalert);".PHP_EOL;
		$cont.="\t\t}".PHP_EOL;
		$cont.="\t});".PHP_EOL;
		$cont.="}".PHP_EOL;
		return $cont;
	}
	public function dataeventgenerate($modlname,$type) {
		$datacont="\t{".PHP_EOL;
		$datasave=$this->dataeventname($modlname,$type,1);
		$datacont.="\t\t$('#".$datasave."').click(function(){".PHP_EOL;
		$datacont.="\t\t\t$('.ftab').trigger('click');".PHP_EOL;
		$datacont.="\t\t\t$('#".$modlname."formaddwizard').validationEngine('validate');".PHP_EOL;
		$datacont.="\t\t});".PHP_EOL;
		$datacont.="\t\t$('#".$modlname."formaddwizard').validationEngine({".PHP_EOL;
		$datacont.="\t\t\tonSuccess: function() {".PHP_EOL;
		$datacont.="\t\t\t\t$('#".$datasave."').attr('disabled','disabled');".PHP_EOL;
		$datacont.="\t\t\t\t".$modlname."newdataaddfun();".PHP_EOL;
		$datacont.="\t\t\t},".PHP_EOL;
		$datacont.="\t\t\tonFailure: function() {".PHP_EOL;
		$datacont.="\t\t\t\t$('#tabgropdropdown').select2('val','1');".PHP_EOL;
		$datacont.="\t\t\t\talertpopup(validationalert);".PHP_EOL;
		$datacont.="\t\t\t}".PHP_EOL;
		$datacont.="\t\t});".PHP_EOL;
		$dataupdate=$this->dataeventname($modlname,$type,2);
		$datacont.="\t\t$('#".$dataupdate."').click(function(){".PHP_EOL;
		$datacont.="\t\t\t$('.ftab').trigger('click');".PHP_EOL;
		$datacont.="\t\t\t$('#".$modlname."formeditwizard').validationEngine('validate');".PHP_EOL;
		$datacont.="\t\t});".PHP_EOL;
		$datacont.="\t\t$('#".$modlname."formeditwizard').validationEngine({".PHP_EOL;
		$datacont.="\t\t\tonSuccess: function() {".PHP_EOL;
		$datacont.="\t\t\t\t$('#".$dataupdate."').attr('disabled','disabled');".PHP_EOL;
		$datacont.="\t\t\t\t".$modlname."dataupdatefun();".PHP_EOL;
		$datacont.="\t\t\t},".PHP_EOL;
		$datacont.="\t\t\tonFailure: function() {".PHP_EOL;
		$datacont.="\t\t\t\t$('#tabgropdropdown').select2('val','1');".PHP_EOL;
		$datacont.="\t\t\t\talertpopup(validationalert);".PHP_EOL;
		$datacont.="\t\t\t}".PHP_EOL;
		$datacont.="\t\t});".PHP_EOL;
		$datacont.="\t}".PHP_EOL;
		return $datacont;
	}
	public function dynamicfrmtogrddatagenerate() {
		$dataftgcont="\t{".PHP_EOL;
		$dataftgcont.="\t\tgriddataid = 0;".PHP_EOL;
		$dataftgcont.="\t\tgridynamicname = '';".PHP_EOL;
		$dataftgcont.="\t\tvar gridname = $('#gridnameinfo').val();".PHP_EOL;
		$dataftgcont.="\t\tvar gridnames = gridname.split(',');".PHP_EOL;
		$dataftgcont.="\t\tvar datalength = gridnames.length;".PHP_EOL;
		$dataftgcont.="\t\tfor(var j=0;j<datalength;j++) {".PHP_EOL;
		$dataftgcont.="\t\t\tgridformtogridvalidatefunction(gridnames[j]);".PHP_EOL;
		$dataftgcont.="\t\t}".PHP_EOL;
		$dataftgcont.="\t\t$('.frmtogridbutton').click(function(){".PHP_EOL;
		$dataftgcont.="\t\t\tvar i = $(this).data('frmtogriddataid');".PHP_EOL;
		$dataftgcont.="\t\t\tvar gridname = $(this).data('frmtogridname');".PHP_EOL;
		$dataftgcont.="\t\t\tgridformtogridvalidatefunction(gridname);".PHP_EOL;
		$dataftgcont.="\t\t\tgriddataid = i;".PHP_EOL;
		$dataftgcont.="\t\t\tgridynamicname = gridname;".PHP_EOL;
		$dataftgcont.="\t\t\t$('#'+gridname+'validation').validationEngine('validate');".PHP_EOL;
		$dataftgcont.="\t\t});".PHP_EOL;	
		$dataftgcont.="\t}".PHP_EOL;
		return $dataftgcont;
	}
	public function checkboxeventgenerate() {
		$a="$";
		$datachkbcont="\t{".PHP_EOL;
		$datachkbcont.="\t\t$('.checkboxcls').click(function(){".PHP_EOL;
		$datachkbcont.="\t\t\tvar name = $(this).data('hidname');".PHP_EOL;
		$datachkbcont.="\t\t\tif ($(this).is(':checked')) {".PHP_EOL;
		$datachkbcont.="\t\t\t\t$('#'+name+'').val('Yes');".PHP_EOL;
		$datachkbcont.="\t\t\t} else {".PHP_EOL;
		$datachkbcont.="\t\t\t\t$('#'+name+'').val('No');".PHP_EOL;
		$datachkbcont.="\t\t\t}".PHP_EOL;
		$datachkbcont.="\t\t});".PHP_EOL;
		$datachkbcont.="\t}".PHP_EOL;
		return $datachkbcont;
	}
	//filter code generation for form with grid
	public function fwgfiltergeneratefunction($modlname) {
		$a="$";
		$filterdata='';
		$filterdata.="\t{".PHP_EOL;
		$filterdata.="\t\t$('#".$modlname."viewtoggle').click(function(){".PHP_EOL;
		$filterdata.="\t\t\tif($('.".$modlname."filterslide').is(':visible')){".PHP_EOL;
		$filterdata.="\t\t\t\t$('div.".$modlname."filterslide').addClass('filterview-moveleft');".PHP_EOL;
		$filterdata.="\t\t\t} else {".PHP_EOL;
		$filterdata.="\t\t\t\t$('div.".$modlname."filterslide').removeClass('filterview-moveleft');".PHP_EOL;
		$filterdata.="\t\t\t}".PHP_EOL;
		$filterdata.="\t\t});".PHP_EOL;
		$filterdata.="\t\t$('#".$modlname."closefiltertoggle').click(function(){".PHP_EOL;
		$filterdata.="\t\t\t$('div.".$modlname."filterslide').removeClass('filterview-moveleft');".PHP_EOL;
		$filterdata.="\t\t});".PHP_EOL;
		$filterdata.="\t\t$('#".$modlname."filterddcondvaluedivhid').hide();".PHP_EOL;
		$filterdata.="\t\t$('#".$modlname."filterdatecondvaluedivhid').hide();".PHP_EOL;
		$filterdata.="\t\t{".PHP_EOL;
		$filterdata.="\t\t\t$('#".$modlname."filtercondvalue').focusout(function(){".PHP_EOL;
		$filterdata.="\t\t\t\tvar value = $('#".$modlname."filtercondvalue').val();".PHP_EOL;
		$filterdata.="\t\t\t\t$('#".$modlname."finalfiltercondvalue').val(value);".PHP_EOL;
		$filterdata.="\t\t\t});".PHP_EOL;
		$filterdata.="\t\t\t$('#".$modlname."filterddcondvalue').change(function(){".PHP_EOL;
		$filterdata.="\t\t\t\tvar value = $('#".$modlname."filterddcondvalue').val();".PHP_EOL;
		$filterdata.="\t\t\t\tif($('#s2id_".$modlname."filterddcondvalue').hasClass('select2-container-multi')){".PHP_EOL;
		$filterdata.="\t\t\t\t\t".$modlname."mainfiltervalue=[];".PHP_EOL;
		$filterdata.="\t\t\t\t\t$('#".$modlname."filterddcondvalue option:selected').each(function(){".PHP_EOL;
		$filterdata.="\t\t\t\t\t\t$('#".$modlname."finalfiltercondvalue').val(value);".PHP_EOL;
		$filterdata.="\t\t\t\t\t\tvar value =$(this).attr('data-ddid');".PHP_EOL;
		$filterdata.="\t\t\t\t\t\t".$modlname."mainfiltervalue.push(value);".PHP_EOL;
		$filterdata.="\t\t\t\t\t\t$('#".$modlname."finalfiltercondvalue').val(".$modlname."mainfiltervalue);".PHP_EOL;
		$filterdata.="\t\t\t\t\t});".PHP_EOL;
		$filterdata.="\t\t\t\t\t$('#".$modlname."filterfinalviewconid').val(value);".PHP_EOL;
		$filterdata.="\t\t\t\t} else {".PHP_EOL;
		$filterdata.="\t\t\t\t\t$('#".$modlname."finalfiltercondvalue').val(value);".PHP_EOL;
		$filterdata.="\t\t\t\t\t$('#".$modlname."filterfinalviewconid').val(value);".PHP_EOL;
		$filterdata.="\t\t\t\t}".PHP_EOL;
		$filterdata.="\t\t\t});".PHP_EOL;
		$filterdata.="\t\t}".PHP_EOL;
		$filterdata.="\t\t".$modlname."filtername=[];".PHP_EOL;
		$filterdata.="\t\t$('#".$modlname."filteraddcondsubbtn').click(function(){".PHP_EOL;
		$filterdata.="\t\t\t$('#".$modlname."filterformconditionvalidation').validationEngine('validate')".PHP_EOL;
		$filterdata.="\t\t});".PHP_EOL;
		$filterdata.="\t\t$('#".$modlname."filterformconditionvalidation').validationEngine({".PHP_EOL;
		$filterdata.="\t\t\tonSuccess: function(){".PHP_EOL;
		$filterdata.="\t\t\t\tdashboardmodulefilter(".$modlname."addgrid,'".$modlname."');".PHP_EOL;
		$filterdata.="\t\t\t},".PHP_EOL;
		$filterdata.="\t\t\tonFailure: function(){".PHP_EOL;
		$filterdata.="\t\t\t\tvar dropdownid =[];".PHP_EOL;
		$filterdata.="\t\t\t\tdropdownfailureerror(dropdownid);".PHP_EOL;
		$filterdata.="\t\t\t\talertpopup(validationalert);".PHP_EOL;
		$filterdata.="\t\t\t}".PHP_EOL;
		$filterdata.="\t\t});".PHP_EOL;
		$filterdata.="\t}".PHP_EOL;
		return $filterdata;
	}
	public function validationfunctiongenerate() {		
		$datavfgcont="function gridformtogridvalidatefunction(gridnamevalue){".PHP_EOL;
		$datavfgcont.="\t$('#'+gridnamevalue+'validation').validationEngine({".PHP_EOL;
		$datavfgcont.="\t\tonSuccess: function() {".PHP_EOL;
		$datavfgcont.="\t\t\tvar i = griddataid;".PHP_EOL;
		$datavfgcont.="\t\t\tvar gridname = gridynamicname;".PHP_EOL;
		$datavfgcont.="\t\t\tvar coldatas = $('#gridcolnames'+i+'').val();".PHP_EOL;
		$datavfgcont.="\t\t\tvar columndata = coldatas.split(',');".PHP_EOL;
		$datavfgcont.="\t\t\tvar coluiatas = $('#gridcoluitype'+i+'').val();".PHP_EOL;
		$datavfgcont.="\t\t\tvar columnuidata = coluiatas.split(',');".PHP_EOL;
		$datavfgcont.="\t\t\tvar datalength = columndata.length;".PHP_EOL;
		$datavfgcont.="\t\t\tif(METHOD == 'ADD' || METHOD == ''){ //ie add operation".PHP_EOL;
		$datavfgcont.="\t\t\t\t	formtogriddata(gridname,METHOD,'last','');".PHP_EOL;
		$datavfgcont.="\t\t\t}".PHP_EOL;
		$datavfgcont.="\t\t\tif(METHOD == 'UPDATE'){  //ie edit operation".PHP_EOL;
		$datavfgcont.="\t\t\t\t	var selectedrow = $('#'+gridname+' div.gridcontent div.active').attr('id');".PHP_EOL;
		$datavfgcont.="\t\t\t\t	formtogriddata(gridname,METHOD,'',selectedrow);".PHP_EOL;
		$datavfgcont.="\t\t\t}".PHP_EOL;
		$datavfgcont.="\t\t\tdatarowselectevt();".PHP_EOL;
		$datavfgcont.="\t\t\tMETHOD = 'ADD';".PHP_EOL;
		$datavfgcont.="\t\t\tvar hideoverlay = $('#griddataoverlayhide'+i+'').val();".PHP_EOL;
		$datavfgcont.="\t\t\tvar hideoverlaydata = hideoverlay.split(',');".PHP_EOL;
		$datavfgcont.="\t\t\t$('#'+hideoverlaydata[1]+'').hide();".PHP_EOL;
		$datavfgcont.="\t\t\t$('#'+hideoverlaydata[0]+'').show();".PHP_EOL;
		$datavfgcont.="\t\t\tclearform('gridformclear');".PHP_EOL;
		$datavfgcont.="\t\t\t$('#'+hideoverlaydata[2]+'').trigger('click');".PHP_EOL;		
		$datavfgcont.="\t\t\tgriddataid = 0;".PHP_EOL;
		$datavfgcont.="\t\t\tgridynamicname = '';".PHP_EOL;
		$datavfgcont.="\t\t},".PHP_EOL;
		$datavfgcont.="\t\tonFailure: function() {".PHP_EOL;			
		$datavfgcont.="\t\t}".PHP_EOL;
		$datavfgcont.="\t});".PHP_EOL;
		$datavfgcont.="}".PHP_EOL;
		return $datavfgcont;
	}
	public function dynamicgdgenerate($modlname,$moduletabname) {
		$gcont="function ".$modlname."addgrid(page,rowcount) {".PHP_EOL;
		$gcont.="\tif(Operation == 1){".PHP_EOL;
		$gcont.="\t\tvar rowcount = $('#".$modlname."pgrowcount').val();".PHP_EOL;
		$gcont.="\t\tvar page = $('.paging').data('pagenum');".PHP_EOL;
		$gcont.="\t} else {".PHP_EOL;
		$gcont.="\tvar rowcount = $('#".$modlname."pgrowcount').val();".PHP_EOL;
		$gcont.="\t\tpage = typeof page == 'undefined' ? 1 : page;".PHP_EOL;
		$gcont.="\t\trowcount = typeof rowcount == 'undefined' ? 100 : rowcount;".PHP_EOL;
		$gcont.="\t}".PHP_EOL;
		$gcont.="\tvar wwidth = $(window).width();".PHP_EOL;
		$gcont.="\tvar wheight = $(window).height();".PHP_EOL;
		$gcont.="\tvar sortcol = $('#sortcolumn').val();".PHP_EOL;
		$gcont.="\tvar sortord = $('#sortorder').val();".PHP_EOL;
		$gcont.="\tif(sortcol){".PHP_EOL;
		$gcont.="\t\tsortcol = sortcol;".PHP_EOL;
		$gcont.="\t} else {".PHP_EOL;
		$gcont.="\t\tvar sortcol = $('.".$modlname."sheadercolsort').hasClass('datasort') ? $('.".$modlname."sheadercolsort.datasort').data('sortcolname') : '';".PHP_EOL;
		$gcont.="\t}".PHP_EOL;
		$gcont.="\tif(sortord){".PHP_EOL;
		$gcont.="\t\tsortord = sortord;".PHP_EOL;
		$gcont.="\t} else {".PHP_EOL;
		$gcont.="\tvar sortord =  $('.".$modlname."sheadercolsort').hasClass('datasort') ? $('.".$modlname."sheadercolsort.datasort').data('sortorder') : '';".PHP_EOL;
		$gcont.="\t}".PHP_EOL;
		$gcont.="\tvar headcolid = $('.".$modlname."sheadercolsort').hasClass('datasort') ? $('.".$modlname."sheadercolsort.datasort').attr('id') : '0';".PHP_EOL;
		$gcont.="\tvar userviewid = $('#dynamicdddataview').find('option:selected').data('dynamicdddataviewid');".PHP_EOL;
		$gcont.="\tvar viewfieldids = $('#viewfieldids').val();".PHP_EOL;
		$gcont.="\tvar filterid = $('#filterid').val();".PHP_EOL;
		$gcont.="\tvar conditionname = $('#conditionname').val();".PHP_EOL;
		$gcont.="\tvar filtervalue = $('#filtervalue').val();".PHP_EOL;
		$gcont.="\t$.ajax({".PHP_EOL;
		$gcont.="\t\turl:base_url+'Base/gridinformationfetch?viewid='+userviewid+'&maintabinfo=".$modlname."&primaryid=".$modlname."id&viewfieldids='+viewfieldids+'&page='+page+'&records='+rowcount+'&width='+wwidth+'&height='+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,".PHP_EOL;
		$gcont.="\t\tcontentType:'application/json; charset=utf-8',".PHP_EOL;
		$gcont.="\t\tdataType:'json',".PHP_EOL;
		$gcont.="\t\tasync:false,".PHP_EOL;
		$gcont.="\t\tcache:false,".PHP_EOL;
		$gcont.="\t\tsuccess :function(data) {".PHP_EOL;
		$gcont.="\t\t\t$('#".$modlname."addgrid').empty();".PHP_EOL;
		$gcont.="\t\t\t$('#".$modlname."addgrid').append(data.content);".PHP_EOL;
		$gcont.="\t\t\t$('#".$modlname."addgridfooter').empty();".PHP_EOL;
		$gcont.="\t\t\t$('#".$modlname."addgridfooter').append(data.footer);".PHP_EOL;
		$gcont.="\t\t\tdatarowselectevt();".PHP_EOL;
		$gcont.="\t\t\tcolumnresize('".$modlname."addgrid');".PHP_EOL;
		$gcont.="\t\t\t{//sorting".PHP_EOL;
		$gcont.="\t\t\t\t$('.".$modlname."sheadercolsort').click(function(){".PHP_EOL;
		$gcont.="\t\t\t\t\t$('#processoverlay').show();".PHP_EOL;
		$gcont.="\t\t\t\t\t$('.".$modlname."sheadercolsort').removeClass('datasort');".PHP_EOL;
		$gcont.="\t\t\t\t\t$(this).addClass('datasort');".PHP_EOL;
		$gcont.="\t\t\t\t\tvar page = $(this).data('pagenum');".PHP_EOL;
		$gcont.="\t\t\t\t\tvar rowcount = $('.pagerowcount').data('rowcount');".PHP_EOL;
		$gcont.="\t\t\t\t\t".$modlname."addgrid(page,rowcount);".PHP_EOL;
		$gcont.="\t\t\t\t\tvar sortcol = $('.".$modlname."headercolsort').hasClass('datasort') ? $('.accountsheadercolsort.datasort').data('sortcolname') : '';".PHP_EOL;
		$gcont.="\t\t\t\t\tvar sortord =  $('.".$modlname."headercolsort').hasClass('datasort') ? $('.accountsheadercolsort.datasort').data('sortorder') : '';".PHP_EOL;
		$gcont.="\t\t\t\t\t$('#sortorder').val(sortord);".PHP_EOL;
		$gcont.="\t\t\t\t\t$('#sortcolumn').val(sortcol);".PHP_EOL;
		$gcont.="\t\t\t\t\t$('#processoverlay').hide();".PHP_EOL;
		$gcont.="\t\t\t\t});".PHP_EOL;
		$gcont.="\t\t\t\tsortordertypereset('".$modlname."sheadercolsort',headcolid,sortord);".PHP_EOL;
		$gcont.="\t\t\t}".PHP_EOL;
		$gcont.="\t\t\t{//pagination".PHP_EOL;
		$gcont.="\t\t\t\t$('.pvpagnumclass').click(function(){".PHP_EOL;
		$gcont.="\t\t\t\t\tOperation = 0;".PHP_EOL;
		$gcont.="\t\t\t\t\t$('#processoverlay').show();".PHP_EOL;
		$gcont.="\t\t\t\t\tvar page = $(this).data('pagenum');".PHP_EOL;
		$gcont.="\t\t\t\t\tvar rowcount = $('.pagerowcount').data('rowcount');".PHP_EOL;
		$gcont.="\t\t\t\t\t".$modlname."addgrid(page,rowcount);".PHP_EOL;
		$gcont.="\t\t\t\t\t$('#processoverlay').hide();".PHP_EOL;
		$gcont.="\t\t\t\t});".PHP_EOL;
		$gcont.="\t\t\t\t$('#".$modlname."pgrowcount').change(function(){".PHP_EOL;
		$gcont.="\t\t\t\t\tOperation = 0;".PHP_EOL;
		$gcont.="\t\t\t\t\t$('#processoverlay').show();".PHP_EOL;
		$gcont.="\t\t\t\t\tvar rowcount = $(this).val();".PHP_EOL;
		$gcont.="\t\t\t\t\t$('.pagerowcount').attr('data-rowcount',rowcount);".PHP_EOL;
		$gcont.="\t\t\t\t\t$('.pagerowcount').text(rowcount);".PHP_EOL;
		$gcont.="\t\t\t\t\tvar page = 1;".PHP_EOL;
		$gcont.="\t\t\t\t\t".$modlname."addgrid(page,rowcount);".PHP_EOL;
		$gcont.="\t\t\t\t\t$('#processoverlay').hide();".PHP_EOL;
		$gcont.="\t\t\t\t});".PHP_EOL;
		$gcont.="\t\t\t}".PHP_EOL;
		$gcont.="\t\t\t\t\t$('#".$modlname."pgrowcount').material_select();".PHP_EOL;
		$gcont.="\t\t},".PHP_EOL;
		$gcont.="\t});".PHP_EOL;
		$gcont.="}".PHP_EOL;
		return $gcont;
	}
	public function dynamicformgridactiongen($modlname,$modlid,$name,$tabid,$m) {
		$formgrid = "\t{".PHP_EOL;
		$formgrid.="\t\t$('#".strtolower($modlname).$name."ingridadd".$m."').click(function(){".PHP_EOL;
		$formgrid.= "\t\t\tformheight();".PHP_EOL;
		$formgrid.= "\t\t\t$('#".strtolower($modlname).$name."overlay').removeClass('closed');".PHP_EOL;
		$formgrid.= "\t\t\t$('#".strtolower($modlname).$name."overlay').addClass('effectbox');".PHP_EOL;
		$formgrid.= "\t\t\tclearform('gridformclear');".PHP_EOL;
		$formgrid.= "\t\t\tMaterialize.updateTextFields();".PHP_EOL;
		$formgrid.= "\t\t\tfirstfieldfocus();".PHP_EOL;
		$formgrid.="\t\t});".PHP_EOL;
		$formgrid.="\t\t$('#".strtolower($modlname).$name."cancelbutton').click(function(){".PHP_EOL;
		$formgrid.= "\t\t\t$('#".strtolower($modlname).$name."overlay').removeClass('effectbox');".PHP_EOL;
		$formgrid.= "\t\t\t$('#".strtolower($modlname).$name."overlay').addClass('closed');".PHP_EOL;
		$formgrid.= "\t\t\t$('#".strtolower($modlname).$name."updatebutton').hide();".PHP_EOL;
		$formgrid.= "\t\t\t$('#".strtolower($modlname).$name."addbutton').show();".PHP_EOL;
		$formgrid.="\t\t});".PHP_EOL;
		$formgrid.= "\t}".PHP_EOL;
		$formgrid.= "\t{".PHP_EOL;
		$formgrid.= "\t\t\t$('#".strtolower($modlname).$name."updatebutton').removeClass('updatebtnclass');".PHP_EOL;
		$formgrid.= "\t\t\t$('#".strtolower($modlname).$name."updatebutton').removeClass('hidedisplayfwg');".PHP_EOL;
		$formgrid.= "\t\t\t$('#".strtolower($modlname).$name."addbutton').removeClass('addbtnclass');".PHP_EOL;
		$formgrid.= "\t\t\t$('#".strtolower($modlname).$name."updatebutton').hide();".PHP_EOL;
		$formgrid.= "\t}".PHP_EOL;
		$formgrid.= "\t{".PHP_EOL;
		$formgrid.="\t\t$('#".strtolower($modlname).$name."ingriddel".$m."').click(function(){".PHP_EOL;
		$formgrid.="\t\t\tvar datarowid = $('#".strtolower($modlname).$name."addgrid".$m." div.gridcontent div.active').attr('id');".PHP_EOL;
		$formgrid.="\t\t\tif(datarowid) {".PHP_EOL;
		$formgrid.="\t\t\t\tdeletegriddatarow('".strtolower($modlname).$name."addgrid".$m."',datarowid);".PHP_EOL;
		$formgrid.="\t\t\t} else {".PHP_EOL;
		$formgrid.="\t\t\t\talertpopup('Please select a row');".PHP_EOL;
		$formgrid.="\t\t\t}".PHP_EOL;
		$formgrid.="\t\t});".PHP_EOL;
		$formgrid.="\t}".PHP_EOL;
		return $formgrid;
	}
	public function dynamiclocalgdgenerate($modlname,$modlid,$name,$tabid,$m) {
		$lgcont="function ".$modlname.$name."addgrid".$m."() {".PHP_EOL;
		$lgcont.="\tvar wwidth = $('#".$modlname.$name."addgrid".$m."').width();".PHP_EOL;
		$lgcont.="\tvar wheight = $('#".$modlname.$name."addgrid".$m."').height();".PHP_EOL;
		$lgcont.="\t$.ajax({".PHP_EOL;
		$lgcont.="\t\turl:base_url+'Base/localgirdheaderinformationfetch?tabgroupid=".$tabid."&moduleid=".$modlid."&width='+wwidth+'&height='+wheight+'&modulename=".$modlname.$name."addgrid".$m."',".PHP_EOL;
		$lgcont.="\t\tdataType:'json',".PHP_EOL;
		$lgcont.="\t\tasync:false,".PHP_EOL;
		$lgcont.="\t\tcache:false,".PHP_EOL;
		$lgcont.="\t\tsuccess :function(data) {".PHP_EOL;
		$lgcont.="\t\t\t$('#".strtolower($modlname).$name."addgrid".$m."').empty();".PHP_EOL;
		$lgcont.="\t\t\t$('#".strtolower($modlname).$name."addgrid".$m."').append(data.content);".PHP_EOL;
		$lgcont.="\t\t\t$('#".strtolower($modlname).$name."addgrid".$m."footer').empty();".PHP_EOL;
		$lgcont.="\t\t\t$('#".strtolower($modlname).$name."addgrid".$m."footer').append(data.footer);".PHP_EOL;
		$lgcont.="\t\t\tdatarowselectevt();".PHP_EOL;
		$lgcont.="\t\t\tcolumnresize('".strtolower($modlname).$name."addgrid".$m."');".PHP_EOL;		
		$lgcont.="\t\t},".PHP_EOL;
		$lgcont.="\t});".PHP_EOL;
		$lgcont.="}".PHP_EOL;
		return $lgcont;
	}		
	public function dynamicdataaddfungen($modlname,$type) {
		$dacont="function ".$modlname."newdataaddfun() {".PHP_EOL;
		$dacont.="\tvar amp = '&';".PHP_EOL;
		$dacont.="\tvar addgriddata='';".PHP_EOL;
		$dacont.="\tvar gridname = $('#gridnameinfo').val();".PHP_EOL;
		$dacont.="\tif(gridname != '') {".PHP_EOL;
		$dacont.="\t\tvar gridnames  = [];".PHP_EOL;
		$dacont.="\t\tgridnames = gridname.split(',');".PHP_EOL;
		$dacont.="\t\tvar datalength = gridnames.length;".PHP_EOL;
		$dacont.="\t\tvar noofrows=0;".PHP_EOL;
		$dacont.="\t\tvar addgriddata = ''".PHP_EOL;
		$dacont.="\t\tif(deviceinfo != 'phone'){".PHP_EOL;
		$dacont.="\t\t\tfor(var j=0;j<datalength;j++) {".PHP_EOL;
		$dacont.="\t\t\t\tif(j!=0) {".PHP_EOL;
		$dacont.="\t\t\t\t\taddgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );".PHP_EOL;
		$dacont.="\t\t\t\t\tnoofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;".PHP_EOL;
		$dacont.="\t\t\t\t} else {".PHP_EOL;
		$dacont.="\t\t\t\t\taddgriddata = getgridrowsdata(gridnames[j]);".PHP_EOL;
		$dacont.="\t\t\t\t\tnoofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;".PHP_EOL;
		$dacont.="\t\t\t\t}".PHP_EOL;
		$dacont.="\t\t\t}".PHP_EOL;
		$dacont.="\t\t}".PHP_EOL;
		$dacont.="\t\telse{".PHP_EOL;
		$dacont.="\t\t\tfor(var j=0;j<datalength;j++) {".PHP_EOL;
		$dacont.="\t\t\t\tif(j!=0) {".PHP_EOL;
		$dacont.="\t\t\t\t\taddgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );".PHP_EOL;
		$dacont.="\t\t\t\t\tnoofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;".PHP_EOL;
		$dacont.="\t\t\t\t} else {".PHP_EOL;
		$dacont.="\t\t\t\t\taddgriddata = getgridrowsdata(gridnames[j]);".PHP_EOL;
		$dacont.="\t\t\t\t\tnoofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;".PHP_EOL;
		$dacont.="\t\t\t\t}".PHP_EOL;
		$dacont.="\t\t\t}".PHP_EOL;
		$dacont.="\t\t}".PHP_EOL;
		$dacont.="\t}".PHP_EOL;		
		$dacont.="\tvar sendformadddata = JSON.stringify(addgriddata);".PHP_EOL;
		$dacont.="\tvar tandcdata = 0;".PHP_EOL;
		$dacont.="\tvar noofrows = noofrows;".PHP_EOL;
		$dacont.="\tvar elementsname = $('#elementsname').val();".PHP_EOL;
		$dacont.="\tvar elementstable = $('#elementstable').val();".PHP_EOL;
		$dacont.="\tvar elementscolmn = $('#elementscolmn').val();".PHP_EOL;
		$dacont.="\tvar elementspartabname = $('#elementspartabname').val();".PHP_EOL;
		$dacont.="\tvar griddatapartabnameinfo = $('#griddatapartabnameinfo').val();".PHP_EOL;
		$dacont.="\tvar resctable = $('#resctable').val();".PHP_EOL;
		$dacont.="\tvar editorname = $('#editornameinfo').val();".PHP_EOL;
		$dacont.="\tvar editordata = froalaeditoradd(editorname);".PHP_EOL;		
		$dacont.="\tvar formdata = $('#".$modlname."dataaddform').serialize();".PHP_EOL;
		$dacont.="\tvar datainformation = amp + formdata;".PHP_EOL;
		$dacont.="\t$.ajax({".PHP_EOL;
		$dacont.="\t\turl:base_url+'".ucfirst($modlname)."/newdatacreate',".PHP_EOL;
		$dacont.="\t\tdata: 'datas=' + datainformation+amp+'griddatas='+sendformadddata+amp+'numofrows='+noofrows+amp+'elementsname='+elementsname+amp+'elementstable='+elementstable+amp+'elementscolmn='+elementscolmn+amp+'elementspartabname='+elementspartabname+amp+'resctable='+resctable+amp+'griddatapartabnameinfo='+griddatapartabnameinfo+amp+'tandcdata='+tandcdata,".PHP_EOL;
		$dacont.="\t\ttype:'POST',".PHP_EOL;
		$dacont.="\t\tsuccess: function(msg) {".PHP_EOL;
		$dacont.="\t\t\tvar nmsg =  $.trim(msg);".PHP_EOL;
		$dacont.="\t\t\tif (nmsg == 'TRUE') {".PHP_EOL;
		$dacont.="\t\t\t\t$('.ftab').trigger('click');".PHP_EOL;
		$dacont.="\t\t\t\tresetFields();".PHP_EOL;
		$dacont.="\t\t\t\t$('#".$modlname."creationformadd').hide();".PHP_EOL;
		$dacont.="\t\t\t\t$('#".$modlname."creationview').fadeIn(1000);".PHP_EOL;
		$dacont.="\t\t\t\trefreshgrid();".PHP_EOL;
		$dacont.="\t\t\t\tclearformgriddata();".PHP_EOL;
		$datasave=$this->dataeventname($modlname,$type,1);
		$dacont.="\t\t\t\t$('#".$datasave."').attr('disabled',false);".PHP_EOL;
		$dacont.="\t\t\t\talertpopup(savealert);".PHP_EOL;
		$dacont.="\t\t\t} else if (nmsg == 'false') {".PHP_EOL;
		$dacont.="\t\t\t}".PHP_EOL;
		$dacont.="\t\t},".PHP_EOL;
		$dacont.="\t});".PHP_EOL;
		$dacont.="}".PHP_EOL;
		return $dacont;
	}
	public function dynamicdataupinfofungen($modlname,$dynamicgridname) {
		$duifcont="function ".$modlname."dataupdateinfofetchfun(datarowid) {".PHP_EOL;
		$duifcont.="\tvar elementsname = $('#elementsname').val();".PHP_EOL;
		$duifcont.="\tvar elementstable = $('#elementstable').val();".PHP_EOL;
		$duifcont.="\tvar elementscolmn = $('#elementscolmn').val();".PHP_EOL;
		$duifcont.="\tvar elementpartable = $('#elementspartabname').val();".PHP_EOL;
		$duifcont.="\tvar resctable = $('#resctable').val();".PHP_EOL;
		$duifcont.="\t$.ajax({".PHP_EOL;
		$duifcont.="\t\turl:base_url+'".ucfirst($modlname)."/fetchformdataeditdetails?dataprimaryid='+datarowid+'&elementsname='+elementsname+'&elementstable='+elementstable+'&elementscolmn='+elementscolmn+'&elementspartabname='+elementpartable+'&resctable='+resctable,".PHP_EOL;
		$duifcont.="\t\tdataType:'json',".PHP_EOL;
		$duifcont.="\t\tasync:false,".PHP_EOL;
		$duifcont.="\t\tsuccess: function(data) {".PHP_EOL;
		$duifcont.="\t\t\tif((data.fail) == 'Denied') {".PHP_EOL;
		$duifcont.="\t\t\t\talertpopup('Permission denied');".PHP_EOL;
		$duifcont.="\t\t\t\t$('.ftab').trigger('click');".PHP_EOL;
		$duifcont.="\t\t\t\tresetFields();".PHP_EOL;
		$duifcont.="\t\t\t\t$('#".$modlname."creationformadd').hide();".PHP_EOL;
		$duifcont.="\t\t\t\t$('#".$modlname."creationview').fadeIn(1000);".PHP_EOL;
		$duifcont.="\t\t\t\trefreshgrid();".PHP_EOL;
		$dataupdate=$this->dataeventname($modlname,1,2);
		$duifcont.="\t\t\t\t$('#".$dataupdate."').attr('disabled',false);".PHP_EOL;
		$duifcont.="\t\t\t} else {".PHP_EOL;
		$duifcont.="\t\t\t\taddslideup('".$modlname."creationview','".$modlname."creationformadd');".PHP_EOL;
		$duifcont.="\t\t\t\tvar txtboxname = elementsname + ',primarydataid';".PHP_EOL;
		$duifcont.="\t\t\t\tvar textboxname = {};".PHP_EOL;
		$duifcont.="\t\t\t\ttextboxname = txtboxname.split(',');".PHP_EOL;
		$duifcont.="\t\t\t\tvar dropdowns = [];".PHP_EOL;
		$duifcont.="\t\t\t\ttextboxsetvaluenew(textboxname,textboxname,data,dropdowns);".PHP_EOL;
		$duifcont.="\t\t\t\teditordatafetch(froalaarray,fdata);".PHP_EOL;
		if(count($dynamicgridname) > 0){
		$duifcont.="\t\t\t\t".$modlname."griddetail(datarowid);".PHP_EOL;
		}
		$duifcont.="\t\t\t\tMaterialize.updateTextFields();".PHP_EOL;
		$duifcont.="\t\t\t\t$('#processoverlay').hide();".PHP_EOL;
		$duifcont.="\t\t\t}".PHP_EOL;
		$duifcont.="\t\t}".PHP_EOL;
		$duifcont.="\t});".PHP_EOL;
		$duifcont.="}".PHP_EOL;
		$duifcont.="function editordatafetch(froalaarray,data) {".PHP_EOL;
		$duifcont.="\tvar filename = [];".PHP_EOL;
		$duifcont.="\tvar editorname = froalaarray[0].split(',');".PHP_EOL;
		$duifcont.="\tfor(var i=0;i<editorname.length;i++) {".PHP_EOL;
		$duifcont.="\t\tfilename[i] =  fdata[editorname[i]+'filename'];".PHP_EOL;
		$duifcont.="\t}".PHP_EOL;
		$duifcont.="\tif(filename.length > 0) {".PHP_EOL;
		$duifcont.="\t\t$.ajax({".PHP_EOL;
		$duifcont.="\t\t\turl:base_url+'Base/editervaluefetch',".PHP_EOL;
		$duifcont.="\t\t\t\tdata:'filename='+filename,".PHP_EOL;
		$duifcont.="\t\t\t\ttype:'POST',".PHP_EOL;
		$duifcont.="\t\t\t\tdataType:'json',".PHP_EOL;
		$duifcont.="\t\t\t\tasync:false,".PHP_EOL;
		$duifcont.="\t\t\t\tcache:false,".PHP_EOL;
		$duifcont.="\t\t\t\tsuccess: function(data) {".PHP_EOL;
		$duifcont.="\t\t\t\t\tfor(var j=0;j<data.length;j++) {".PHP_EOL;
		$duifcont.="\t\t\t\t\t\t$('#'+editorname[j]+'filename').val();".PHP_EOL;
		$duifcont.="\t\t\t\t\t\tfroalaedit(data[j],editorname[j]);".PHP_EOL;
		$duifcont.="\t\t\t\t\t}".PHP_EOL;
		$duifcont.="\t\t\t\t},".PHP_EOL;
		$duifcont.="\t\t});".PHP_EOL;
		$duifcont.="\t}".PHP_EOL;
		$duifcont.="}".PHP_EOL;
		$duifcont.="function ".$modlname."griddetail(pid) {".PHP_EOL;
		$duifcont.="\tif(pid !='') {".PHP_EOL;
		if(count($dynamicgridname)>0) {
			$m=1;
			foreach($dynamicgridname as $tabid => $tabgrpname) {
				$name=substr(strtolower($tabgrpname),0,3);
				$grdtabname = preg_replace('/[^A-Za-z0-9]/', '', $name);
				$grdname = strtolower($modlname).$name."addgrid".$m;
				$duifcont.="\t\tvar  ".$grdname."field=[];".PHP_EOL;
				$duifcont.="\t\t$('#".$grdname." .inline-list li').each(function(){".PHP_EOL;
				$duifcont.="\t\t\t".$grdname."field.push($(this).attr('data-fieldname'));".PHP_EOL;
				$duifcont.="\t\t});".PHP_EOL;
				$duifcont.="\t\t$.ajax({".PHP_EOL;
				$duifcont.="\t\t\turl:base_url+'".ucfirst($modlname)."/".$grdname."detailfetch?fieldname='+".$grdname."field+'&primarydataid='+pid,".PHP_EOL;
				$duifcont.="\t\t\t\t\tdataType:'json',".PHP_EOL;
				$duifcont.="\t\t\t\t\tasync:false,".PHP_EOL;
				$duifcont.="\t\t\t\t\tcache:false,".PHP_EOL;
				$duifcont.="\t\t\t\t\tsuccess: function(data) {".PHP_EOL;
				$duifcont.="\t\t\t\t\t\t\tif((data.fail) == 'FAILED') {".PHP_EOL;
				$duifcont.="\t\t\t\t\t\t\t} else {".PHP_EOL;
				$duifcont.="\t\t\t\t\t\t\t\tloadinlinegriddata('".$grdname."',data.rows,'json');".PHP_EOL;
				$duifcont.="\t\t\t\t\t\t\t\tdatarowselectevt();".PHP_EOL;
				$duifcont.="\t\t\t\t\t\t\t\tcolumnresize('".$grdname."');".PHP_EOL;
				$duifcont.="\t\t\t\t\t\t\t}".PHP_EOL;
				$duifcont.="\t\t\t\t\t},".PHP_EOL;
				$duifcont.="\t\t});".PHP_EOL;
				$m++;
			}
		}
		$duifcont.="\t}".PHP_EOL;
		$duifcont.="}".PHP_EOL;
		return $duifcont;
	}
	public function dynamicdataupdatefungen($modlname,$type) {
		$dacont="function ".$modlname."dataupdatefun() {".PHP_EOL;		
		$dacont.="\tvar amp = '&';".PHP_EOL;
		$dacont.="\tvar gridname = $('#gridnameinfo').val();".PHP_EOL;
		$dacont.="\tif(gridname != ''){".PHP_EOL;
		$dacont.="\t\tvar gridnames = gridname.split(',');".PHP_EOL;
		$dacont.="\t\tvar datalength = gridnames.length;".PHP_EOL;
		$dacont.="\t\tvar noofrows=0;".PHP_EOL;
		$dacont.="\t\tvar addgriddata='';".PHP_EOL;
		$dacont.="\t\tif(deviceinfo != 'phone'){".PHP_EOL;
		$dacont.="\t\t\tfor(var j=0;j<datalength;j++) {".PHP_EOL;
		$dacont.="\t\t\t\tif(j!=0) {".PHP_EOL;
		$dacont.="\t\t\t\t\taddgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));".PHP_EOL;
		$dacont.="\t\t\t\t\tnoofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;".PHP_EOL;
		$dacont.="\t\t\t\t} else {".PHP_EOL;
		$dacont.="\t\t\t\t\taddgriddata = getgridrowsdata(gridnames[j]);".PHP_EOL;
		$dacont.="\t\t\t\t\tnoofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;".PHP_EOL;
		$dacont.="\t\t\t\t}".PHP_EOL;
		$dacont.="\t\t\t}".PHP_EOL;
		$dacont.="\t\t}else{".PHP_EOL;
		$dacont.="\t\t\tfor(var j=0;j<datalength;j++) {".PHP_EOL;
		$dacont.="\t\t\t\tif(j!=0) {".PHP_EOL;
		$dacont.="\t\t\t\t\taddgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));".PHP_EOL;
		$dacont.="\t\t\t\t\tnoofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;".PHP_EOL;
		$dacont.="\t\t\t\t} else {".PHP_EOL;
		$dacont.="\t\t\t\t\taddgriddata = getgridrowsdata(gridnames[j]);".PHP_EOL;
		$dacont.="\t\t\t\t\tnoofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;".PHP_EOL;
		$dacont.="\t\t\t\t}".PHP_EOL;
		$dacont.="\t\t\t}".PHP_EOL;
		$dacont.="\t\t}".PHP_EOL;
		$dacont.="\t}".PHP_EOL;
		$dacont.="\tvar sendformadddata = JSON.stringify(addgriddata);".PHP_EOL;
		$dacont.="\tvar noofrows = noofrows;".PHP_EOL;
		$dacont.="\tvar editorname = $('#editornameinfo').val();".PHP_EOL;
		$dacont.="\tvar editordata = froalaeditordataget(editorname);".PHP_EOL;
		$dacont.="\tvar resctable = $('#resctable').val();".PHP_EOL;
		$dacont.="\tvar formdata = $('#".$modlname."dataaddform').serialize();".PHP_EOL;
		$dacont.="\tvar datainformation = amp + formdata;".PHP_EOL;
		$dacont.="\tvar dataset = '';".PHP_EOL;
		$dacont.="\tif(gridname != ''){".PHP_EOL;
		$dacont.="\t\tdataset = 'datas=' + datainformation+amp+'griddatas='+sendformadddata+amp+'numofrows='+noofrows;".PHP_EOL;
		$dacont.="\t}else{".PHP_EOL;
		$dacont.="\t\tdataset = 'datas=' + datainformation;".PHP_EOL;
		$dacont.="\t}".PHP_EOL;
		$dacont.="\t$.ajax({".PHP_EOL;
		$dacont.="\t\turl: base_url+'".ucfirst($modlname)."/datainformationupdate',".PHP_EOL;
		$dacont.="\t\tdata:dataset,".PHP_EOL;
		$dacont.="\t\ttype: 'POST',".PHP_EOL;
		$dacont.="\t\tsuccess: function(msg) {".PHP_EOL;
		$dacont.="\t\t\tvar nmsg =  $.trim(msg);".PHP_EOL;
		$dacont.="\t\t\tif (nmsg == 'TRUE') {".PHP_EOL;
		$dacont.="\t\t\t\t$('.ftab').trigger('click');".PHP_EOL;
		$dacont.="\t\t\t\tresetFields();".PHP_EOL;
		$dacont.="\t\t\t\t$('#".$modlname."creationformadd').hide();".PHP_EOL;
		$dacont.="\t\t\t\t$('#".$modlname."creationview').fadeIn(1000);".PHP_EOL;
		$dacont.="\t\t\t\trefreshgrid();".PHP_EOL;
		$dacont.="\t\t\t\tclearformgriddata();".PHP_EOL;
		$dataupdate=$this->dataeventname($modlname,$type,2);
		$dacont.="\t\t\t\t$('#".$dataupdate."').attr('disabled',false);".PHP_EOL;
		$dacont.="\t\t\t\talertpopup(savealert);".PHP_EOL;
		$dacont.="\t\t\t} else if (nmsg == 'false') {".PHP_EOL;
		$dacont.="\t\t\t}".PHP_EOL;
		$dacont.="\t\t},".PHP_EOL;
		$dacont.="\t});".PHP_EOL;
		$dacont.="}".PHP_EOL;
		return $dacont;
	}
	public function dynamicdatadelfungen($modlname) {
		$ddecont="function ".$modlname."recorddelete(datarowid) {".PHP_EOL;
		$ddecont.="\tvar elementstable = $('#elementstable').val();".PHP_EOL;
		$ddecont.="\tvar elementspartable = $('#elementspartabname').val();".PHP_EOL;
		$ddecont.="\t$.ajax({".PHP_EOL;
		$ddecont.="\t\turl: base_url + '".ucfirst($modlname)."/deleteinformationdata?primarydataid='+datarowid+'&elementstable='+elementstable+'&parenttable='+elementspartable,".PHP_EOL;
		$ddecont.="\t\tasync:false,".PHP_EOL;
		$ddecont.="\t\tsuccess: function(msg) {".PHP_EOL;
		$ddecont.="\t\t\tvar nmsg =  $.trim(msg);".PHP_EOL;
		$ddecont.="\t\t\tif (nmsg == 'TRUE') {".PHP_EOL;
		$ddecont.="\t\t\t\trefreshgrid();".PHP_EOL;
		$ddecont.="\t\t\t\t$('#basedeleteoverlay').fadeOut();".PHP_EOL;
		$ddecont.="\t\t\t\talertpopup('Deleted successfully');".PHP_EOL;
		$ddecont.="\t\t\t} else if (nmsg == 'Denied') {".PHP_EOL;
		$ddecont.="\t\t\t\trefreshgrid();".PHP_EOL;
		$ddecont.="\t\t\t\t$('#basedeleteoverlay').fadeOut();".PHP_EOL;
		$ddecont.="\t\t\t\talertpopup('Permission denied');".PHP_EOL;
		$ddecont.="\t\t\t}".PHP_EOL;
		$ddecont.="\t\t}".PHP_EOL;
		$ddecont.="\t});".PHP_EOL;
		$ddecont.="}".PHP_EOL;
		return $ddecont;
	}
	public function viewsuccreload() {
		$vrcont="function viewcreatesuccfun(viewname) {".PHP_EOL;
		$vrcont.="\tvar viewmoduleids = $('#viewcreatemoduleid').val();".PHP_EOL;
		$vrcont.="\tdropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);".PHP_EOL;
		$vrcont.="\tif(viewname != 'dontset') {".PHP_EOL;
		$vrcont.="\t\tsetTimeout(function() {".PHP_EOL;
		$vrcont.="\t\t\t$('#dynamicdddataview').select2('val',viewname);".PHP_EOL;
		$vrcont.="\t\t\t$('#dynamicdddataview').trigger('change');".PHP_EOL;
		$vrcont.="\t\t}, 1000);".PHP_EOL;
		$vrcont.="\t\tcleargriddata('viewcreateconditiongrid');".PHP_EOL;
		$vrcont.="\t} else {".PHP_EOL;
		$vrcont.="\t\t$('#dynamicdddataview').trigger('change');".PHP_EOL;
		$vrcont.="\t}".PHP_EOL;
		$vrcont.="}".PHP_EOL;
		return $vrcont;
	}
	public function indexgenerate($modlname,$modname,$moduleid) {
		$a="$";
		$midname='$moduleid';
		$icont="\tpublic function index() {".PHP_EOL;
		$icont.="\t\t".$midname."= array(".$moduleid.");".PHP_EOL;
		$icont.="\t\tsessionchecker(".$midname.");".PHP_EOL;
		$icont.="\t\t".$a."data['actionassign']=".$a."this->Basefunctions->actionassign(".$midname.");".PHP_EOL;
		$icont.="\t\t".$a."data['modtabgrp']=".$a."this->Basefunctions->moduuletabgroupgeneration(".$midname.");".PHP_EOL;
		$icont.="\t\t".$a."data['modtabsecfrmfkdsgrp']=".$a."this->Basefunctions->moduuletabgrpsecfieldsgeneration(".$midname.");".PHP_EOL;
		$icont.="\t\t".$a."data['gridtitle']=".$a."this->Basefunctions->gridtitleinformationfetch(".$midname.");".PHP_EOL;
		$icont.="\t\t".$a."viewfieldsmoduleids = $midname;".PHP_EOL;
		$icont.="\t\t".$a."viewmoduleid = $midname;".PHP_EOL;
		$icont.="\t\t".$a."data['moduleids']=".$a."viewmoduleid;".PHP_EOL;
		$icont.="\t\t".$a."data['filedmodids']=".$a."viewfieldsmoduleids;".PHP_EOL;
		$icont.="\t\t".$a."data['dynamicviewdd']=".$a."this->Basefunctions->dataviewbydropdown(".$a."viewmoduleid);".PHP_EOL;
		$icont.="\t\t".$a."data['dataviewcol']=".$a."this->Basefunctions->dataviewdropdowncolumns(".$a."viewfieldsmoduleids);".PHP_EOL;
		$icont.="\t\t".$a."this->load->view('".$modlname."/".$modname."view',".$a."data);".PHP_EOL;
		$icont.="\t}".PHP_EOL;
		return $icont;
	}
	public function actionfungenerate($modlname,$moduleid,$dynamicgridname) {
		$a="$";
		$accont="\tpublic function newdatacreate() {".PHP_EOL;
		$accont.="\t\t".$a."this->".$modlname."model->newdatacreatemodel();".PHP_EOL;
		$accont.="\t}".PHP_EOL;
		$accont.="\tpublic function fetchformdataeditdetails() {".PHP_EOL;
		$accont.="\t\t".$a."moduleid= array(".$moduleid.");".PHP_EOL;
		$accont.="\t\t".$a."this->".$modlname."model->informationfetchmodel(".$a."moduleid);".PHP_EOL;
		$accont.="\t}".PHP_EOL;
		$accont.="\tpublic function datainformationupdate() {".PHP_EOL;
		$accont.="\t\t".$a."this->".$modlname."model->datainformationupdatemodel();".PHP_EOL;
		$accont.="\t}".PHP_EOL;
		$accont.="\tpublic function deleteinformationdata() {".PHP_EOL;
		$accont.="\t\t".$a."moduleid= array(".$moduleid.");".PHP_EOL;
		$accont.="\t\t".$a."this->".$modlname."model->deleteoldinformation(".$a."moduleid);".PHP_EOL;
		$accont.="\t}".PHP_EOL;
		if(count($dynamicgridname)>0) {
			$m=1;
			foreach($dynamicgridname as $tabid => $tabgrpname) {
				$name=substr(strtolower($tabgrpname),0,3);
				$grdtabname = preg_replace('/[^A-Za-z0-9]/', '', $name);
				$grdname = strtolower($modlname).$name."addgrid".$m;
				$accont.="\tpublic function ".$grdname."detailfetch() {".PHP_EOL;
				$accont.="\t\t".$a."this->".$modlname."model->".$grdname."detailfetchmodel();".PHP_EOL;
				$accont.="\t}".PHP_EOL;
				$m++;
			}
		}
		return $accont;
	}
	public function datacreatemodelgenerate() {
		$a="$";
		$mdccont="\tpublic function newdatacreatemodel() {".PHP_EOL;
		$mdccont.=$this->postdatainfoorganizemodel();
		$mdccont.="\t\t".$a."gridfieldpartabname = explode(',',".$a."_POST['griddatapartabnameinfo']);".PHP_EOL;
		$mdccont.="\t\t".$a."gridrows = explode(',',".$a."_POST['numofrows']);".PHP_EOL;
		$mdccont.="\t\t".$a."girddata = ".$a."_POST['griddatas'];".PHP_EOL;
		$mdccont.="\t\t".$a."girddatainfo = json_decode(".$a."girddata, true);".PHP_EOL;
		$mdccont.="\t\t".$a."gridpartablename =  ".$a."this->Crudmodel->filtervalue(".$a."gridfieldpartabname);".PHP_EOL;
		$mdccont.="\t\t".$a."tableinfo = explode(',',".$a."fieldstable);".PHP_EOL;
		$mdccont.="\t\t".$a."restricttable = explode(',',".$a."_POST['resctable']);".PHP_EOL;
		$mdccont.="\t\t".$a."primaryid = ".$a."this->Crudmodel->datainsertwithrestrict(".$a."partablename,".$a."tableinfo,".$a."formfieldsname,".$a."formfieldstable,".$a."formfieldscolmname,".$a."restricttable);".PHP_EOL;
		$mdccont.="\t\t".$a."primaryname = ".$a."this->Crudmodel->primaryinfo(".$a."partablename);".PHP_EOL;
		$mdccont.="\t\tif(count(".$a."girddatainfo)>0 && ".$a."girddatainfo!= '') {".PHP_EOL;
		$mdccont.="\t\t\t".$a."gresult = ".$a."this->Crudmodel->griddatainsert(".$a."primaryname,".$a."gridpartablename,".$a."girddatainfo,".$a."gridrows,".$a."primaryid);".PHP_EOL;
		$mdccont.="\t\t}".PHP_EOL;
		$mdccont.="\t\techo 'TRUE';".PHP_EOL;
		$mdccont.="\t}".PHP_EOL;
		return $mdccont;
	}
	public function griddatacreatemodelgenerate($modulename) {
		$a="$";
		$mdccont="\tpublic function newdatacreatemodel() {".PHP_EOL;
		$mdccont.="\t\t".$a."formfieldsname = explode(',',".$a."_POST['".$modulename."elementsname']);".PHP_EOL;
		$mdccont.="\t\t".$a."formfieldstable = explode(',',".$a."_POST['".$modulename."elementstable']);".PHP_EOL;
		$mdccont.="\t\t".$a."formfieldscolmname = explode(',',".$a."_POST['".$modulename."elementscolmn']);".PHP_EOL;
		$mdccont.="\t\t".$a."categoryelementpartable = explode(',',".$a."_POST['".$modulename."elementspartabname']);".PHP_EOL;
		$mdccont.="\t\t".$a."partablename =  ".$a."this->Crudmodel->filtervalue(".$a."categoryelementpartable);".PHP_EOL;
		//filter unique fields table
		$mdccont.="\t\t".$a."fildstable = ".$a."this->Crudmodel->filtervalue(".$a."formfieldstable);".PHP_EOL;
		$mdccont.="\t\t".$a."tableinfo = explode(',',".$a."fildstable);".PHP_EOL;
		$mdccont.="\t\tif(isset(".$a."_POST['".$modulename."resctable'])){".PHP_EOL;
		$mdccont.="\t\t\t".$a."restricttable = explode(',',".$a."_POST['".$modulename."resctable']);".PHP_EOL;
		$mdccont.="\t\t} else {".PHP_EOL;
		$mdccont.="\t\t\t".$a."restricttable = array();".PHP_EOL;
		$mdccont.="\t\t}".PHP_EOL;
		$mdccont.="\t\t".$a."result = ".$a."this->Crudmodel->datainsertwithrestrict(".$a."partablename,".$a."tableinfo,".$a."formfieldsname,".$a."formfieldstable,".$a."formfieldscolmname,".$a."restricttable);".PHP_EOL;		
		$mdccont.="\t\techo 'TRUE';".PHP_EOL;
		$mdccont.="\t}".PHP_EOL;
		return $mdccont;
	}	
	public function griddataupdatemodelgenerate($modulename) {
		$a="$";		
		$mducont="\tpublic function datainformationupdatemodel() {".PHP_EOL;
		$mducont.="\t\t".$a."formfieldsname = explode(',',".$a."_POST['".$modulename."elementsname']);".PHP_EOL;
		$mducont.="\t\t".$a."formfieldstable = explode(',',".$a."_POST['".$modulename."elementstable']);".PHP_EOL;
		$mducont.="\t\t".$a."formfieldscolmname = explode(',',".$a."_POST['".$modulename."elementscolmn']);".PHP_EOL;
		$mducont.="\t\t".$a."categoryelementpartable = explode(',',".$a."_POST['".$modulename."elementspartabname']);".PHP_EOL;
		$mducont.="\t\t".$a."partablename =  ".$a."this->Crudmodel->filtervalue(".$a."categoryelementpartable);".PHP_EOL;
		$mducont.="\t\t".$a."fildstable = ".$a."this->Crudmodel->filtervalue(".$a."formfieldstable);".PHP_EOL;
		$mducont.="\t\t".$a."tableinfo = explode(',',".$a."fildstable);".PHP_EOL;
		$mducont.="\t\t".$a."primaryid = ".$a."_POST['".$modulename."primarydataid'];".PHP_EOL;
		$mducont.="\t\tif(isset(".$a."_POST['".$modulename."resctable'])){".PHP_EOL;
		$mducont.="\t\t\t".$a."restricttable = explode(',',".$a."_POST['".$modulename."resctable']);".PHP_EOL;
		$mducont.="\t\t} else {".PHP_EOL;
		$mducont.="\t\t\t".$a."restricttable = array();".PHP_EOL;
		$mducont.="\t\t}".PHP_EOL;
		$mducont.="\t\t".$a."result = ".$a."this->Crudmodel->dataupdatewithrestrict(".$a."partablename,".$a."tableinfo,".$a."formfieldsname,".$a."formfieldstable,".$a."formfieldscolmname,".$a."primaryid,".$a."restricttable);".PHP_EOL;
		$mducont.="\t\techo ".$a."result;".PHP_EOL;
		$mducont.="\t}".PHP_EOL;
		return $mducont;
	}
	public function datainfogetmodelgenerate() {
		$a="$";
		$mifcont="\tpublic function informationfetchmodel(".$a."moduleid) {".PHP_EOL;
		$mifcont.=$this->getdatainfoorganizemodel();
		$mifcont.="\t\t".$a."primaryid = ".$a."_GET['dataprimaryid'];".PHP_EOL;
		$mifcont.="\t\t".$a."primaryname = ".$a."this->Crudmodel->primaryinfo(".$a."partablename);".PHP_EOL;
		$mifcont.="\t\t".$a."result = ".$a."this->Crudmodel->dbdatafetchwithrestrict(".$a."formfieldsname,".$a."formfieldscolmname,".$a."primaryname,".$a."primaryid,".$a."fieldstable,".$a."partablename,".$a."formfieldstable,".$a."restricttable,".$a."moduleid);".PHP_EOL;
		$mifcont.="\t\techo ".$a."result;".PHP_EOL;
		$mifcont.="\t}".PHP_EOL;
		return $mifcont;
	}
	public function dataupdatemodelgenerate() {
		$a="$";
		$mducont="\tpublic function datainformationupdatemodel() {".PHP_EOL;
		$mducont.=$this->postdatainfoorganizemodel();
		$mducont.="\t\t".$a."tableinfo = explode(',',".$a."fieldstable);".PHP_EOL;
		$mducont.="\t\t".$a."primaryid = ".$a."_POST['primarydataid'];".PHP_EOL;
		$mducont.="\t\t".$a."restricttable = explode(',',".$a."_POST['resctable']);".PHP_EOL;
		$mducont.="\t\t".$a."this->Crudmodel->dataupdatewithrestrict(".$a."partablename,".$a."tableinfo,".$a."formfieldsname,".$a."formfieldstable,".$a."formfieldscolmname,".$a."primaryid,".$a."restricttable);".PHP_EOL;
		$mducont.="\t\techo 'TRUE';".PHP_EOL;
		$mducont.="\t}".PHP_EOL;
		return $mducont;
	}
	public function datadeletemodelgenerate() {
		$a="$";
		$mddcont="\tpublic function deleteoldinformation(".$a."moduleid) {".PHP_EOL;
		$mddcont.="\t\t".$a."formfieldstable = explode(',',".$a."_GET['elementstable']);".PHP_EOL;
		$mddcont.="\t\t".$a."parenttable = explode(',',".$a."_GET['parenttable']);".PHP_EOL;
		$mddcont.="\t\t".$a."id = ".$a."_GET['primarydataid'];".PHP_EOL;
		$mddcont.="\t\t".$a."tablename =  ".$a."this->Crudmodel->filtervalue(".$a."formfieldstable);".PHP_EOL;
		$mddcont.="\t\t".$a."partabname =  ".$a."this->Crudmodel->filtervalue(".$a."parenttable);".PHP_EOL;
		$mddcont.="\t\t".$a."primaryname = ".$a."this->Crudmodel->primaryinfo(".$a."partabname);".PHP_EOL;
		$mddcont.="\t\t".$a."ctable=".$a."this->Crudmodel->foreignkey(".$a."primaryname,".$a."partabname);".PHP_EOL;
		$mddcont.="\t\t".$a."ruleid = ".$a."this->Basefunctions->moduleruleidfetch(".$a."moduleid);".PHP_EOL;
		$mddcont.="\t\tif(".$a."ruleid == 1 || ".$a."ruleid == 2) {".PHP_EOL;
		$mddcont.="\t\t\t".$a."chek = ".$a."this->Basefunctions->checkrecordcreateduser(".$a."partabname,".$a."primaryname,".$a."id,".$a."moduleid);".PHP_EOL;
		$mddcont.="\t\t\tif(".$a."chek == 0) {".PHP_EOL;
		$mddcont.="\t\t\t\techo 'Denied';".PHP_EOL;
		$mddcont.="\t\t\t} else {".PHP_EOL;
		$mddcont.="\t\t\t\t".$a."this->Crudmodel->outerdeletefunction(".$a."partabname,".$a."primaryname,".$a."ctable,".$a."id);".PHP_EOL;
		$mddcont.="\t\t\t\techo 'TRUE';".PHP_EOL;
		$mddcont.="\t\t\t}".PHP_EOL;
		$mddcont.="\t\t} else {".PHP_EOL;
		$mddcont.="\t\t\t".$a."this->Crudmodel->outerdeletefunction(".$a."partabname,".$a."primaryname,".$a."ctable,".$a."id);".PHP_EOL;
		$mddcont.="\t\t\techo 'TRUE';".PHP_EOL;
		$mddcont.="\t\t}".PHP_EOL;
		$mddcont.="\t}".PHP_EOL;
		return $mddcont;
	}
	public function postdatainfoorganizemodel() {
		$a="$";
		$diomcont="\t\t".$a."formfieldsname = explode(',',".$a."_POST['elementsname']);".PHP_EOL;
		$diomcont.="\t\t".$a."formfieldstable = explode(',',".$a."_POST['elementstable']);".PHP_EOL;
		$diomcont.="\t\t".$a."formfieldscolmname = explode(',',".$a."_POST['elementscolmn']);".PHP_EOL;
		$diomcont.="\t\t".$a."elementpartable = explode(',',".$a."_POST['elementspartabname']);".PHP_EOL;
		$diomcont.="\t\t".$a."partablename = ".$a."this->Crudmodel->filtervalue(".$a."elementpartable);".PHP_EOL;
		$diomcont.="\t\t".$a."fieldstable = ".$a."this->Crudmodel->filtervalue(".$a."formfieldstable);".PHP_EOL;
		return $diomcont;
	}
	public function getdatainfoorganizemodel() {
		$a="$";
		$diomcont="\t\t".$a."formfieldsname = explode(',',".$a."_GET['elementsname']);".PHP_EOL;
		$diomcont.="\t\t".$a."formfieldstable = explode(',',".$a."_GET['elementstable']);".PHP_EOL;
		$diomcont.="\t\t".$a."formfieldscolmname = explode(',',".$a."_GET['elementscolmn']);".PHP_EOL;
		$diomcont.="\t\t".$a."elementpartable = explode(',',".$a."_GET['elementspartabname']);".PHP_EOL;
		$diomcont.="\t\t".$a."restricttable = explode(',',".$a."_GET['resctable']);".PHP_EOL;
		$diomcont.="\t\t".$a."partablename = ".$a."this->Crudmodel->filtervalue(".$a."elementpartable);".PHP_EOL;
		$diomcont.="\t\t".$a."fieldstable = ".$a."this->Crudmodel->filtervalue(".$a."formfieldstable);".PHP_EOL;
		return $diomcont;
	}
	public function viewheadcreate($modlname) {
		$a="$";
		$vhcont="\t<head>".PHP_EOL;
		$vhcont.="\t\t<?php ".$a."this->load->view('Base/headerfiles'); ?>".PHP_EOL;
		$vhcont.="\t</head>".PHP_EOL;
		return $vhcont;
	}
	public function viewbodycreate($modlname,$moduleid) {
		$a="$";
		$vbcont="\t<body class=''>".PHP_EOL;
		$vbcont.="\t\t<?php".PHP_EOL;
		$vbcont.="\t\t\t".$a."device = ".$a."this->Basefunctions->deviceinfo();".PHP_EOL;
		$vbcont.="\t\t\t".$a."dataset['gridenable'] = 'yes';".PHP_EOL;
		$vbcont.="\t\t\t".$a."dataset['moduleid']=".$a."moduleids;".PHP_EOL;
		$vbcont.="\t\t\t".$a."dataset['griddisplayid'] = '".$modlname."creationview';".PHP_EOL;
		$vbcont.="\t\t\t".$a."dataset['gridtitle'] = ".$a."gridtitle['title'];".PHP_EOL;
		$vbcont.="\t\t\t".$a."dataset['titleicon'] = ".$a."gridtitle['titleicon'];".PHP_EOL;
		$vbcont.="\t\t\t".$a."dataset['spanattr'] = array();".PHP_EOL;
		$vbcont.="\t\t\t".$a."dataset['gridtableid'] = '".$modlname."addgrid';".PHP_EOL;
		$vbcont.="\t\t\t".$a."dataset['griddivid'] = '".$modlname."addgridnav';".PHP_EOL;
		$vbcont.="\t\t\t".$a."dataset['forminfo'] = array(array('id'=>'".$modlname."creationformadd','class'=>'hidedisplay','formname'=>'".$modlname."creationform'));".PHP_EOL;
		$vbcont.="\t\t\tif(".$a."device=='phone') {".PHP_EOL;
		$vbcont.="\t\t\t\t".$a."this->load->view('Base/gridmenuheadermobile',".$a."dataset);".PHP_EOL;
		$vbcont.="\t\t\t\t".$a."this->load->view('Base/overlaymobile');".PHP_EOL;
		$vbcont.="\t\t\t\t".$a."this->load->view('Base/viewselectionoverlay');".PHP_EOL;
		$vbcont.="\t\t\t} else {".PHP_EOL;
		$vbcont.="\t\t\t\t".$a."this->load->view('Base/gridmenuheader',".$a."dataset);".PHP_EOL;
		$vbcont.="\t\t\t\t".$a."this->load->view('Base/overlay');".PHP_EOL;
		$vbcont.="\t\t\t}".PHP_EOL;
		$vbcont.="\t\t\t".$a."this->load->view('Base/basedeleteform');".PHP_EOL;
		$vbcont.="\t\t\t".$a."this->load->view('Base/modulelist');".PHP_EOL;
		$vbcont.="\t\t?>".PHP_EOL;
		$vbcont.="\t</body>".PHP_EOL;
		$vbcont.="\t<?php ".$a."this->load->view('Base/bottomscript'); ?>".PHP_EOL;
		$vbcont.="\t<script src='<?php echo base_url();?>js/".ucfirst($modlname)."/".$modlname.".js' type='text/javascript'></script>".PHP_EOL;
		return $vbcont;
	}
	public function vcformhedergenerate($modlname) {
		$a="$";
		$vcfhcont="<?php".PHP_EOL;
		$vcfhcont.="\t".$a."device = ".$a."this->Basefunctions->deviceinfo();".PHP_EOL;
		$vcfhcont.="\tif(".$a."device=='phone') {".PHP_EOL;
		$vcfhcont.="\t\techo '<div class=\"large-12 columns headercaptionstyle headerradius paddingzero\">".PHP_EOL;
		$vcfhcont.="\t\t\t<div class=\"large-6 medium-6 small-12 columns headercaptionleft\">".PHP_EOL;
		$vcfhcont.="\t\t\t\t<span data-activates=\"slide-out\" class=\"headermainmenuicon  button-collapse\"  tilte=\"Menu\" ><i class=\"material-icons\">menu</i></span>".PHP_EOL;
		$vcfhcont.="\t\t\t\t<span class=\"gridcaptionpos\"><span>'.".$a."gridtitle.'</span></span>".PHP_EOL;
		$vcfhcont.="\t\t\t</div>';".PHP_EOL;
		$vcfhcont.="\t\t\t".$a."this->load->view('Base/formheadericons');".PHP_EOL;
		$vcfhcont.="\t\techo '</div>';".PHP_EOL;
		$vcfhcont.="\t} else {".PHP_EOL;
		$vcfhcont.="\t\techo '<div class=\"large-12 columns headercaptionstyle headerradius paddingzero\">".PHP_EOL;
		$vcfhcont.="\t\t\t<div class=\"large-6 medium-6 small-6 columns headercaptionleft\">".PHP_EOL;
		$vcfhcont.="\t\t\t\t<span data-activates=\"slide-out\" class=\"headermainmenuicon button-collapse\"  tilte=\"Menu\"><i class=\"material-icons\">menu</i></span>".PHP_EOL;
		$vcfhcont.="\t\t\t\t<span class=\"gridcaptionpos\"><span>'.".$a."gridtitle.'</span></span>".PHP_EOL;
		$vcfhcont.="\t\t\t</div>';".PHP_EOL;
		$vcfhcont.="\t\t\t".$a."this->load->view('Base/formheadericons');".PHP_EOL;
		$vcfhcont.="\t\techo '</div>';".PHP_EOL;
		$vcfhcont.="\t}".PHP_EOL;
		$vcfhcont.="?>".PHP_EOL;
		return $vcfhcont;
	}
	public function vcformtabgrpgenerate($modlname) {
		$a="$";
		$vcftgcont="\t<?php if(".$a."device!='phone') {?>".PHP_EOL;
		$vcftgcont.="<div class='large-12 columns tabgroupstyle desktoptabgroup'>".PHP_EOL;
		$vcftgcont.="\t<?php".PHP_EOL;
		$vcftgcont.="\t\t".$a."ulattr = array('class'=>'tabs');".PHP_EOL;
		$vcftgcont.="\t\t".$a."uladdinfo = 'data-tab';".PHP_EOL;
		$vcftgcont.="\t\t".$a."tabgrpattr = array('class'=>'tab-title sidebaricons');".PHP_EOL;
		$vcftgcont.="\t\t".$a."tabstatus = 'active';".PHP_EOL;
		$vcftgcont.="\t\t".$a."dataname = 'subform';".PHP_EOL;
		$vcftgcont.="\t\techo tabgroupgenerate(".$a."ulattr,".$a."uladdinfo,".$a."tabgrpattr,".$a."tabstatus,".$a."dataname,".$a."modtabgrp);".PHP_EOL;
		$vcftgcont.="\t\t".$a."dropdowntabval = array();".PHP_EOL;
		$vcftgcont.="\t\t".$a."m=1;".PHP_EOL;
		$vcftgcont.="\t\tforeach(".$a."modtabgrp as ".$a."value) {".PHP_EOL;
		$vcftgcont.="\t\t\t".$a."dropdowntabval[".$a."m]=".$a."value['tabgrpname'];".PHP_EOL;
		$vcftgcont.="\t\t\t".$a."m++;".PHP_EOL;
		$vcftgcont.="\t\t}".PHP_EOL;
		$vcftgcont.="\t?>".PHP_EOL;
		$vcftgcont.="</div>".PHP_EOL;
		$vcftgcont.="\t<?php } else {?>".PHP_EOL;
		$vcftgcont.="<div class='large-12 columns tabgroupstyle mobiletabgroup'>".PHP_EOL;
		$vcftgcont.="\t<?php".PHP_EOL;
		$vcftgcont.="\t\t".$a."ulattr = array('class'=>'tabs');".PHP_EOL;
		$vcftgcont.="\t\t".$a."uladdinfo = 'data-tab';".PHP_EOL;
		$vcftgcont.="\t\t".$a."tabgrpattr = array('class'=>'tab-title sidebaricons');".PHP_EOL;
		$vcftgcont.="\t\t".$a."tabstatus = 'active';".PHP_EOL;
		$vcftgcont.="\t\t".$a."dataname = 'subform';".PHP_EOL;
		$vcftgcont.="\t\techo mobiletabgroupgenerate(".$a."ulattr,".$a."uladdinfo,".$a."tabgrpattr,".$a."tabstatus,".$a."dataname,".$a."modtabgrp);".PHP_EOL;
		$vcftgcont.="\t\t".$a."dropdowntabval = array();".PHP_EOL;
		$vcftgcont.="\t\t".$a."m=1;".PHP_EOL;
		$vcftgcont.="\t\tforeach(".$a."modtabgrp as ".$a."value) {".PHP_EOL;
		$vcftgcont.="\t\t\t".$a."dropdowntabval[".$a."m]=".$a."value['tabgrpname'];".PHP_EOL;
		$vcftgcont.="\t\t\t".$a."m++;".PHP_EOL;
		$vcftgcont.="\t\t}".PHP_EOL;
		$vcftgcont.="\t?>".PHP_EOL;
		$vcftgcont.="</div>".PHP_EOL;
		$vcftgcont.="\t<?php }?>".PHP_EOL;
		//$vcftgcont.="<div class='large-12 columns tabgroupstyle centertext show-for-medium-down tabgrpddstyle' >".PHP_EOL;
		//$vcftgcont.="\t<span class='tabmoiconcontainer'>".PHP_EOL;
		//$vcftgcont.="\t\t<select id='tabgropdropdown' class='chzn-select' style='width:40%'>".PHP_EOL;
		//$vcftgcont.="\t\t\t<?php".PHP_EOL;
		//$vcftgcont.="\t\t\t\tforeach(".$a."dropdowntabval as ".$a."tabid=>".$a."tabname) {".PHP_EOL;
		//$vcftgcont.="\t\t\t\t\techo '<option value='.".$a."tabid.'>'.".$a."tabname.'</option>';".PHP_EOL;
		//$vcftgcont.="\t\t\t\t}".PHP_EOL;
		//$vcftgcont.="\t\t\t".PHP_EOL;
		//$vcftgcont.="\t\t</select>".PHP_EOL;
		//$vcftgcont.="\t</span>".PHP_EOL;
		//$vcftgcont.="</div>".PHP_EOL;
		//$vcftgcont.="<script>".PHP_EOL;
		//$vcftgcont.="\t$(document).ready(function(){".PHP_EOL;
		//$vcftgcont.="\t\t$('#tabgropdropdown').change(function(){".PHP_EOL;
		//$vcftgcont.="\t\t\tvar tabgpid = $('#tabgropdropdown').val(); ".PHP_EOL;
		//$vcftgcont.="\t\t\t$('.sidebaricons[data-subform='+tabgpid+']').trigger('click');".PHP_EOL;
		//$vcftgcont.="\t\t});".PHP_EOL;
		//$vcftgcont.="\t});".PHP_EOL;
		//$vcftgcont.="</script>".PHP_EOL;
		return $vcftgcont;
	}
	public function vcformcontgenerate($modlname,$resttab) {
		$a="$";
		$restablinfo = array_unique( $resttab );
		$tablname = implode(',',$restablinfo);
		$vcfccont="<div class='large-12 columns scrollbarclass addformcontainer tabtouchaddcontainer'>".PHP_EOL;
		$vcfccont.="\t<div class='row'>&nbsp;</div>".PHP_EOL;
		$vcfccont.="\t<form method='POST' name='".$modlname."dataaddform' class='' action ='' id='".$modlname."dataaddform' enctype='multipart/form-data'>".PHP_EOL;
		$vcfccont.="\t\t<span id='".$modlname."formaddwizard' class='validationEngineContainer'>".PHP_EOL;
		$vcfccont.="\t\t\t<span id='".$modlname."formeditwizard' class='validationEngineContainer'>".PHP_EOL;
		$vcfccont.="\t\t\t\t<?php".PHP_EOL;
		$vcfccont.="\t\t\t\t\tformfieldstemplategenerator(".$a."modtabgrp);".PHP_EOL;
		$vcfccont.="\t\t\t\t?>".PHP_EOL;
		$vcfccont.="\t\t\t</span>".PHP_EOL;
		$vcfccont.="\t\t</span>".PHP_EOL;
		$vcfccont.="\t\t<?php".PHP_EOL;
		$vcfccont.="\t\t\techo hidden('primarydataid','');".PHP_EOL;
		$vcfccont.="\t\t\t".$a."value = '".$tablname."';".PHP_EOL;
		$vcfccont.="\t\t\techo hidden('resctable',".$a."value);".PHP_EOL;
		$vcfccont.="\t\t\t".$a."val = implode(',',".$a."filedmodids);".PHP_EOL;
		$vcfccont.="\t\t\t".$a."modid = ".$a."this->Basefunctions->getmoduleid(".$a."filedmodids);".PHP_EOL;
		$vcfccont.="\t\t\techo hidden('viewfieldids',".$a."modid);".PHP_EOL;
		$vcfccont.="\t\t?>".PHP_EOL;
		$vcfccont.="\t</form>".PHP_EOL;
		$vcfccont.="</div>".PHP_EOL;
		return $vcfccont;
	}
	public function gridcontroller($modulename,$moduleid){
		$modname = preg_replace('/[^A-Za-z0-9]/', '', $modulename);
		$modlname = ucfirst($modname);
		$cpath = 'application'.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.$modlname.DIRECTORY_SEPARATOR.'controllers';
		if(!is_dir($cpath)) {
			$dirsucc = mkdir($cpath,0777,TRUE);
		}
		$a="$";
		$cfname=$modlname.".php";
		$cfile=$cpath.DIRECTORY_SEPARATOR.$cfname;
		$ccont="<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');".PHP_EOL;
		$ccont.="class ".ucwords($modlname)." extends MX_Controller {".PHP_EOL;
		$ccont.="\tfunction __construct() {".PHP_EOL;
		$ccont.="\t\tparent::__construct();".PHP_EOL;	
		//$ccont.="\t\t".$a."this->load->view('Base/formfieldgeneration');".PHP_EOL;
		$ccont.="\t\t".$a."this->load->model('".$modlname."/".$modlname."model');".PHP_EOL;
		$ccont.="\t\t".$a."this->load->helper('formbuild');".PHP_EOL;
		$ccont.="\t}".PHP_EOL;
		$ccont.=$this->gridindexgenerate($modlname,$modname,$moduleid);
		$ccont.=$this->gridactionfungenerate($modlname,$moduleid);
		$ccont.="}";
		write_file($cfile,$ccont);
	}
	public function gridindexgenerate($modlname,$modname,$moduleid) {
		$a="$";
		$midname='$moduleid';
		$icont="\tpublic function index() {".PHP_EOL;
		$icont.="\t\t".$midname."= array(".$moduleid.");".PHP_EOL;
		$icont.="\t\tsessionchecker(".$midname.");".PHP_EOL;
		$icont.="\t\t".$a."data['actionassign']=".$a."this->Basefunctions->actionassign(".$midname.");".PHP_EOL;
		$icont.="\t\t".$a."data['modtabgrp']=".$a."this->Basefunctions->moduuletabgroupgeneration(".$midname.");".PHP_EOL;
		$icont.="\t\t".$a."data['modtabsecfrmfkdsgrp']=".$a."this->Basefunctions->moduuletabgrpsecfieldsgeneration(".$midname.");".PHP_EOL;
		$icont.="\t\t".$a."data['gridtitle']=".$a."this->Basefunctions->gridtitleinformationfetch(".$midname.");".PHP_EOL;
		$icont.="\t\t".$a."viewfieldsmoduleids = $midname;".PHP_EOL;
		$icont.="\t\t".$a."viewmoduleid = $midname;".PHP_EOL;
		$icont.="\t\t".$a."data['moduleids']=".$a."viewmoduleid;".PHP_EOL;
		$icont.="\t\t".$a."data['filedmodids']=".$a."viewfieldsmoduleids;".PHP_EOL;
		$icont.="\t\t".$a."data['dynamicviewdd']=".$a."this->Basefunctions->dataviewbydropdown(".$a."viewmoduleid);".PHP_EOL;
		$icont.="\t\t".$a."data['dataviewcol']=".$a."this->Basefunctions->dataviewdropdowncolumns(".$a."viewfieldsmoduleids);".PHP_EOL;
		$icont.="\t\t".$a."this->load->view('".$modlname."/".$modname."view',".$a."data);".PHP_EOL;
		$icont.="\t}".PHP_EOL;
		return $icont;
	}
	public function gridactionfungenerate($modlname,$moduleid) {
		$a="$";
		$accont="\tpublic function newdatacreate() {".PHP_EOL;
		$accont.="\t\t".$a."this->".ucfirst($modlname)."model->newdatacreatemodel();".PHP_EOL;
		$accont.="\t}".PHP_EOL;
		$accont.="\tpublic function fetchformdataeditdetails() {".PHP_EOL;
		$accont.="\t\t".$a."moduleid= array(".$moduleid.");".PHP_EOL;
		$accont.="\t\t".$a."this->".ucfirst($modlname)."model->informationfetchmodel(".$a."moduleid);".PHP_EOL;
		$accont.="\t}".PHP_EOL;
		$accont.="\tpublic function datainformationupdate() {".PHP_EOL;
		$accont.="\t\t".$a."this->".ucfirst($modlname)."model->datainformationupdatemodel();".PHP_EOL;
		$accont.="\t}".PHP_EOL;
		$accont.="\tpublic function deleteinformationdata() {".PHP_EOL;
		$accont.="\t\t".$a."moduleid= array(".$moduleid.");".PHP_EOL;
		$accont.="\t\t".$a."this->".ucfirst($modlname)."model->deleteoldinformation(".$a."moduleid);".PHP_EOL;
		$accont.="\t}".PHP_EOL;
		return $accont;
	}
	// grid main view generate
	public function gridmainviewgenerate($modulename,$moduleid) {
		$modname = preg_replace('/[^A-Za-z0-9]/', '', $modulename);
		$modlname = ucfirst($modname);
		$viewfilename = strtolower($modname);
		$vpath = 'application'.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.$modlname.DIRECTORY_SEPARATOR.'views';
		if(!is_dir($vpath)) {
			$dirsucc = mkdir($vpath,0777,TRUE);
		}
		$a="$";
		$vfname=$viewfilename."view.php";
		$vfile=$vpath.DIRECTORY_SEPARATOR.$vfname;
		$vcont="<!DOCTYPE html>".PHP_EOL;
		$vcont.="<html lang='en'>".PHP_EOL;
		$vcont.=$this->gridviewheadcreate($modlname);
		$vcont.=$this->gridviewbodycreate($modlname,$moduleid);
		$vcont.="</html>".PHP_EOL;
		write_file($vfile,$vcont);
	}
	public function gridviewheadcreate($modlname) {
		$a="$";
		$vhcont="\t<head>".PHP_EOL;
		$vhcont.="\t\t<?php ".$a."this->load->view('Base/headerfiles'); ?>".PHP_EOL;		
		$vhcont.="\t</head>".PHP_EOL;
		return $vhcont;
	}
	public function gridviewbodycreate($modlname,$moduleid) {
		$modulename = strtolower($modlname);		
		$a="$";
		$vbcont="\t<body >".PHP_EOL;
		$vbcont.="\t\t<?php".PHP_EOL;
		$vbcont.="\t\t\t".$a."device = ".$a."this->Basefunctions->deviceinfo();".PHP_EOL;
		$vbcont.="\t\t\t".$a."dataset['gridenable'] = '1';".PHP_EOL;		
		$vbcont.="\t\t\t".$a."dataset['griddisplayid'] = '".$modulename."creationview';".PHP_EOL;
		$vbcont.="\t\t\t".$a."dataset['maingridtitle'] =  ".$a."gridtitle['title'];".PHP_EOL;   // form header
		$vbcont.="\t\t\t".$a."dataset['gridtitle'] = ".$a."gridtitle['title'];".PHP_EOL;
		$vbcont.="\t\t\t".$a."dataset['titleicon'] = ".$a."gridtitle['titleicon'];".PHP_EOL;
		$vbcont.="\t\t\t".$a."dataset['spanattr'] = array();".PHP_EOL;
		$vbcont.="\t\t\t".$a."dataset['gridtableid'] = '".$modulename."addgrid';".PHP_EOL;
		$vbcont.="\t\t\t".$a."dataset['griddivid'] = '".$modulename."addgridnav';".PHP_EOL;	
		$vbcont.="\t\t\t".$a."this->load->view('".$modlname."/".$modulename."creationform',".$a."dataset);".PHP_EOL;
		$vbcont.="\t\t\t".$a."this->load->view('Base/basedeleteform');".PHP_EOL;
		$vbcont.="\t\t\tif(".$a."device=='phone') {".PHP_EOL;		
		$vbcont.="\t\t\t\t".$a."this->load->view('Base/overlaymobile');".PHP_EOL;
		$vbcont.="\t\t\t} else {".PHP_EOL;		
		$vbcont.="\t\t\t\t".$a."this->load->view('Base/overlay');".PHP_EOL;
		$vbcont.="\t\t\t}".PHP_EOL;
		$vbcont.="\t\t\t".$a."this->load->view('Base/modulelist');".PHP_EOL;
		$vbcont.="\t\t?>".PHP_EOL;
		$vbcont.="\t</body>".PHP_EOL;
		$vbcont="\t<div id='supportoverlayappend' class='large-12 columns paddingzero' style='position:absolute;'></div>".PHP_EOL;
		$vbcont="\t<div id='notificationoverlayappend' class='large-12 columns paddingzero' style='position:absolute;'></div>".PHP_EOL;
		$vbcont.="\t<?php ".$a."this->load->view('Base/bottomscript'); ?>".PHP_EOL;
		$vbcont.="\t<script src='<?php echo base_url();?>js/".$modlname."/".$modulename.".js' type='text/javascript'></script>".PHP_EOL;
		return $vbcont;
	}
	public function gridformgenerate($modulename,$resttab) {
		$modname = preg_replace('/[^A-Za-z0-9]/', '', $modulename);
		$modlname = ucfirst($modname);
		$formname = strtolower($modname);
		$vpath = 'application'.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.$modlname.DIRECTORY_SEPARATOR.'views';
		if(!is_dir($vpath)) {
			$dirsucc = mkdir($vpath,0777,TRUE);
		}
		$a="$";
		$vcfname=$formname."creationform.php";
		$vcfile=$vpath.DIRECTORY_SEPARATOR.$vcfname;
		$vcfcont=$this->commongridformgenerate($modlname);		
		write_file($vcfile,$vcfcont);
	}
	public function commongridformgenerate($modlname) {
		$formname = strtolower($modlname);
		$a="$";
		$vhcont="<div class=\"row\" style=\"max-width:100%\">".PHP_EOL;
		$vhcont.="<div class=\"off-canvas-wrap\" data-offcanvas>".PHP_EOL;
		$vhcont.="<div class=\"inner-wrap\">".PHP_EOL;
		$vhcont.="<div id=\" ".$formname."creationformadd\" class=\"gridviewdivforsh\">".PHP_EOL;
		$vhcont.="\t<?php".PHP_EOL;
		$vhcont.="\t\t".$a."this->load->view('Base/singlemainviewformwithgrid');".PHP_EOL;
		$vhcont.="\t\t".$a."this->load->view('Base/formfieldgeneration');".PHP_EOL;
					//function call for form fields generation
		$vhcont.="\t\tformfieldsgeneratorwithgrid(".$a."modtabsecfrmfkdsgrp,".$a."filedmodids);".PHP_EOL;
		$vhcont.="\t?>".PHP_EOL;
		$vhcont.="</div>".PHP_EOL;
		$vhcont.="</div>".PHP_EOL;
		$vhcont.="</div>".PHP_EOL;
		$vhcont.="</div>".PHP_EOL;
		$vhcont.="<div>".PHP_EOL;
		$vhcont.="\t<?php ".$a."this->load->view('Base/viewselectionoverlay'); ?> ".PHP_EOL;
		$vhcont.="</div>".PHP_EOL;		
		return $vhcont;
	}	
	public function gridscriptgenerate($modulename,$type,$dynamicgridname,$modlid,$moduletabname) {
		$modname = preg_replace('/[^A-Za-z0-9]/', '', $modulename);
		$modlname = ucfirst($modname);
		$jsname = strtolower($modlname);
		$jfoldpath = "js".DIRECTORY_SEPARATOR.$modlname;
		if(!is_dir($jfoldpath)) {
			$dirsucc = mkdir($jfoldpath,0777,TRUE);
		}
		$jfname=$jsname.".js";
		$file=$jfoldpath.DIRECTORY_SEPARATOR.$jfname;
		$cont="$(document).ready(function(){".PHP_EOL;		
		$cont.="\t$(document).foundation();".PHP_EOL;
		$cont.="\tmaindivwidth();".PHP_EOL;
		$cont.="\t{".PHP_EOL;
		$cont.="\t\t".$modname."addgrid();".PHP_EOL;
		$cont.="\t}".PHP_EOL;
		$cont.=$this->dynamicformclose($modname);
		$cont.=$this->dynamicdatareload($modname);
		$cont.="\t{".PHP_EOL;
		$cont.="\t\t$( window ).resize(function() {".PHP_EOL;
		$cont.="\t\t\tmaingridresizeheightset('".$modname."addgrid');".PHP_EOL;
		$cont.="\t\t\tsectionpanelheight('groupsectionoverlay');".PHP_EOL;
		$cont.="\t\t});".PHP_EOL;
		$cont.="\t}".PHP_EOL;
		$cont.=$this->gridacteventgenerate($modname,$type,$modlid);
		$cont.=$this->griddataeventgenerate($modname,$type);		
		$cont.=$this->checkboxeventgenerate();
		$cont.=$this->fwgfiltergeneratefunction($modname);
		$cont.="});".PHP_EOL;
		$cont.=$this->validationfunctiongenerate();
		$cont.=$this->griddynamicgdgenerate($modname,$moduletabname);		
		$cont.="function refreshgrid() {".PHP_EOL;
		$cont.="\t\tvar page = $(this).data('pagenum');".PHP_EOL;
		$cont.="\t\tvar rowcount = $('ul#".$modname."spgnumcnt li .page-text .active').data('rowcount');".PHP_EOL;
		$cont.="\t\t".$modname."addgrid(page,rowcount);".PHP_EOL;
		$cont.="}".PHP_EOL;
		$cont.="function ".$modname."clonedatafetchfun(datarowid) {".PHP_EOL;
		$cont.="\t\t$('.addbtnclass').removeClass('hidedisplay');".PHP_EOL;
		$cont.="\t\t$('.updatebtnclass').addClass('hidedisplay');".PHP_EOL;
		$cont.="\t\t$('#formclearicon').hide();".PHP_EOL;
		$cont.="\t\tresetFields();".PHP_EOL;
		$cont.="\t\t".$modname."dataupdateinfofetchfun(datarowid);".PHP_EOL;
		$cont.="\t\tfirstfieldfocus();".PHP_EOL;
		//For Keyboard Short cut Variables
		$cont.="\t\taddformupdate = 1;".PHP_EOL;
		$cont.="\t\tviewgridview = 0;".PHP_EOL;
		$cont.="\t\taddformview = 1;".PHP_EOL;
		//For Left Menu Confirmation
		$cont.="\t\tdiscardmsg = 1;".PHP_EOL;
		$cont.="}".PHP_EOL;
		$cont.=$this->griddynamicdataaddfungen($modname,$type);
		$cont.=$this->griddynamicdataupinfofungen($modname);
		$cont.=$this->griddynamicdataupdatefungen($modname,$type);
		$cont.=$this->griddynamicdatadelfungen($modname);
		$cont.=$this->viewsuccreload();
		write_file($file,$cont);
	}
	public function griddataeventgenerate($modlname,$type) {
		$datacont="\t{".PHP_EOL;		
		$datacont.="\t\t$('#".strtolower($modlname)."savebutton').click(function(){".PHP_EOL;
		$datacont.="\t\t\t$('.ftab').trigger('click');".PHP_EOL;
		$datacont.="\t\t\t$('#".strtolower($modlname)."formaddwizard').validationEngine('validate');".PHP_EOL;
		$datacont.="\t\t});".PHP_EOL;
		$datacont.="\t\t$('#".strtolower($modlname)."formaddwizard').validationEngine({".PHP_EOL;
		$datacont.="\t\t\tonSuccess: function() {".PHP_EOL;
		$datacont.="\t\t\t\t$('#".strtolower($modlname)."savebutton').attr('disabled','disabled');".PHP_EOL;
		$datacont.="\t\t\t\t".$modlname."newdataaddfun();".PHP_EOL;
		$datacont.="\t\t\t},".PHP_EOL;
		$datacont.="\t\t\tonFailure: function() {".PHP_EOL;
		$datacont.="\t\t\t\t$('#tabgropdropdown').select2('val','1');".PHP_EOL;
		$datacont.="\t\t\t\talertpopup(validationalert);".PHP_EOL;
		$datacont.="\t\t\t}".PHP_EOL;
		$datacont.="\t\t});".PHP_EOL;		
		$datacont.="\t\t$('#".strtolower($modlname)."dataupdatesubbtn').click(function(){".PHP_EOL;
		$datacont.="\t\t\t$('.ftab').trigger('click');".PHP_EOL;
		$datacont.="\t\t\t$('#".strtolower($modlname)."formeditwizard').validationEngine('validate');".PHP_EOL;
		$datacont.="\t\t});".PHP_EOL;
		$datacont.="\t\t$('#".strtolower($modlname)."formeditwizard').validationEngine({".PHP_EOL;
		$datacont.="\t\t\tonSuccess: function() {".PHP_EOL;
		$datacont.="\t\t\t\t$('#".strtolower($modlname)."dataupdatesubbtn').attr('disabled','disabled');".PHP_EOL;
		$datacont.="\t\t\t\t".$modlname."dataupdatefun();".PHP_EOL;
		$datacont.="\t\t\t},".PHP_EOL;
		$datacont.="\t\t\tonFailure: function() {".PHP_EOL;
		$datacont.="\t\t\t\t$('#tabgropdropdown').select2('val','1');".PHP_EOL;
		$datacont.="\t\t\t\talertpopup(validationalert);".PHP_EOL;
		$datacont.="\t\t\t}".PHP_EOL;
		$datacont.="\t\t});".PHP_EOL;
		$datacont.="\t}".PHP_EOL;
		return $datacont;
	}
	public function griddynamicdataupinfofungen($modlname) {
		$duifcont="function ".$modlname."dataupdateinfofetchfun(datarowid) {".PHP_EOL;
		$duifcont.="\tvar elementsname = $('#".strtolower($modlname)."elementsname').val();".PHP_EOL;
		$duifcont.="\tvar elementstable = $('#".strtolower($modlname)."elementstable').val();".PHP_EOL;
		$duifcont.="\tvar elementscolmn = $('#".strtolower($modlname)."elementscolmn').val();".PHP_EOL;
		$duifcont.="\tvar elementpartable = $('#".strtolower($modlname)."elementspartabname').val();".PHP_EOL;
		$duifcont.="\tvar resctable = $('#resctable').val();".PHP_EOL;
		$duifcont.="\t$.ajax({".PHP_EOL;
		$duifcont.="\t\turl:base_url+'".ucfirst($modlname)."/fetchformdataeditdetails?dataprimaryid='+datarowid+'&elementsname='+elementsname+'&elementstable='+elementstable+'&elementscolmn='+elementscolmn+'&elementspartabname='+elementpartable+'&resctable='+resctable,".PHP_EOL;
		$duifcont.="\t\tdataType:'json',".PHP_EOL;
		$duifcont.="\t\tasync:false,".PHP_EOL;
		$duifcont.="\t\tsuccess: function(data) {".PHP_EOL;
		$duifcont.="\t\t\tif((data.fail) == 'Denied') {".PHP_EOL;
		$duifcont.="\t\t\t\talertpopup('Permission denied');".PHP_EOL;
		$duifcont.="\t\t\t\t$('.ftab').trigger('click');".PHP_EOL;
		$duifcont.="\t\t\t\tresetFields();".PHP_EOL;
		$duifcont.="\t\t\t\t$('#".$modlname."creationformadd').hide();".PHP_EOL;
		$duifcont.="\t\t\t\t$('#".$modlname."creationview').fadeIn(1000);".PHP_EOL;
		$duifcont.="\t\t\t\trefreshgrid();".PHP_EOL;
		$dataupdate=$this->dataeventname($modlname,1,2);
		$duifcont.="\t\t\t\t$('#".$dataupdate."').attr('disabled',false);".PHP_EOL;
		$duifcont.="\t\t\t} else {".PHP_EOL;
		$duifcont.="\t\t\t\taddslideup('".$modlname."creationview','".$modlname."creationformadd');".PHP_EOL;
		$duifcont.="\t\t\t\tvar txtboxname = elementsname + ',".strtolower($modlname)."primarydataid';".PHP_EOL;
		$duifcont.="\t\t\t\tvar textboxname = {};".PHP_EOL;
		$duifcont.="\t\t\t\ttextboxname = txtboxname.split(',');".PHP_EOL;
		$duifcont.="\t\t\t\tvar dataname = elementsname + ',primarydataid';".PHP_EOL;
		$duifcont.="\t\t\t\tvar datasname = {};".PHP_EOL;
		$duifcont.="\t\t\t\tdatasname = dataname.split(',');".PHP_EOL;
		$duifcont.="\t\t\t\tvar dropdowns = [];".PHP_EOL;
		$duifcont.="\t\t\t\ttextboxsetvaluenew(textboxname,datasname,data,dropdowns);".PHP_EOL;
		$duifcont.="\t\t\t\tMaterialize.updateTextFields();".PHP_EOL;
		$duifcont.="\t\t\t\t$('#processoverlay').hide();".PHP_EOL;
		$duifcont.="\t\t\t}".PHP_EOL;
		$duifcont.="\t\t}".PHP_EOL;
		$duifcont.="\t});".PHP_EOL;
		$duifcont.="}".PHP_EOL;
		return $duifcont;
	}
	public function griddynamicgdgenerate($modlname,$moduletabname) {
		$gcont="function ".$modlname."addgrid(page,rowcount) {".PHP_EOL;
		$gcont.="\tpage = typeof page == 'undefined' ? 1 : page;".PHP_EOL;
		$gcont.="\trowcount = typeof rowcount == 'undefined' ? 100 : rowcount;".PHP_EOL;
		$gcont.="\tvar wwidth = $(window).width();".PHP_EOL;
		$gcont.="\tvar wheight = $(window).height();".PHP_EOL;
		$gcont.="\tvar sortcol = $('#".$modlname."sortcolumn').val();".PHP_EOL;
		$gcont.="\tvar sortord = $('#".$modlname."sortorder').val();".PHP_EOL;
		$gcont.="\tif(sortcol){".PHP_EOL;
		$gcont.="\t\tsortcol = sortcol;".PHP_EOL;
		$gcont.="\t} else {".PHP_EOL;
		$gcont.="\t\tvar sortcol = $('.".$modlname."sheadercolsort').hasClass('datasort') ? $('.".$modlname."sheadercolsort.datasort').data('sortcolname') : '';".PHP_EOL;
		$gcont.="\t}".PHP_EOL;
		$gcont.="\tif(sortord){".PHP_EOL;
		$gcont.="\t\tsortord = sortord;".PHP_EOL;
		$gcont.="\t} else {".PHP_EOL;
		$gcont.="\t\tvar sortord =  $('.".$modlname."sheadercolsort').hasClass('datasort') ? $('.".$modlname."sheadercolsort.datasort').data('sortorder') : '';".PHP_EOL;
		$gcont.="\t}".PHP_EOL;
		$gcont.="\tvar headcolid = $('.".$modlname."sheadercolsort').hasClass('datasort') ? $('.".$modlname."sheadercolsort.datasort').attr('id') : '0';".PHP_EOL;
		$gcont.="\tvar userviewid = $('#dynamicdddataview').find('option:selected').data('dynamicdddataviewid');".PHP_EOL;
		$gcont.="\tvar viewfieldids = $('#".strtolower($modlname)."viewfieldids').val();".PHP_EOL;
		$gcont.="\tvar filterid = $('#".strtolower($modlname)."filterid').val();".PHP_EOL;
		$gcont.="\tvar conditionname = $('#".strtolower($modlname)."conditionname').val();".PHP_EOL;
		$gcont.="\tvar filtervalue = $('#".strtolower($modlname)."filtervalue').val();".PHP_EOL;
		$gcont.="\t$.ajax({".PHP_EOL;
		$gcont.="\t\turl:base_url+'Base/gridinformationfetch?viewid='+userviewid+'&maintabinfo=".$modlname."&primaryid=".$modlname."id&viewfieldids='+viewfieldids+'&page='+page+'&records='+rowcount+'&width='+wwidth+'&height='+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,".PHP_EOL;
		$gcont.="\t\tcontentType:'application/json; charset=utf-8',".PHP_EOL;
		$gcont.="\t\tdataType:'json',".PHP_EOL;
		$gcont.="\t\tasync:false,".PHP_EOL;
		$gcont.="\t\tcache:false,".PHP_EOL;
		$gcont.="\t\tsuccess :function(data) {".PHP_EOL;
		$gcont.="\t\t\t$('#".strtolower($modlname)."addgrid').empty();".PHP_EOL;
		$gcont.="\t\t\t$('#".strtolower($modlname)."addgrid').append(data.content);".PHP_EOL;
		$gcont.="\t\t\t$('#".strtolower($modlname)."addgridfooter').empty();".PHP_EOL;
		$gcont.="\t\t\t$('#".strtolower($modlname)."addgridfooter').append(data.footer);".PHP_EOL;
		$gcont.="\t\t\tdatarowselectevt();".PHP_EOL;
		$gcont.="\t\t\tcolumnresize('".strtolower($modlname)."addgrid');".PHP_EOL;
		$gcont.="\t\t\tmaingridresizeheightset('".$modlname."addgrid');".PHP_EOL;
		$gcont.="\t\t\t{//sorting".PHP_EOL;
		$gcont.="\t\t\t\t$('.".$modlname."sheadercolsort').click(function(){".PHP_EOL;
		$gcont.="\t\t\t\t\t$('#processoverlay').show();".PHP_EOL;		
		$gcont.="\t\t\t\t\t$('.".$modlname."sheadercolsort').removeClass('datasort');".PHP_EOL;
		$gcont.="\t\t\t\t\t$(this).addClass('datasort');".PHP_EOL;
		$gcont.="\t\t\t\t\tvar page = $(this).data('pagenum');".PHP_EOL;
		$gcont.="\t\t\t\t\tvar rowcount = $('ul#".$modlname."spgnumcnt li .page-text .active').data('rowcount');".PHP_EOL;
		$gcont.="\t\t\t\t\tvar sortcol = $('.".$modlname."sheadercolsort').hasClass('datasort') ? $('.".$modlname."sheadercolsort.datasort').data('sortcolname') : '';".PHP_EOL;
		$gcont.="\t\t\t\t\tvar sortord = $('.".$modlname."sheadercolsort').hasClass('datasort') ? $('.".$modlname."sheadercolsort.datasort').data('sortorder') : '';".PHP_EOL;
		$gcont.="\t\t\t\t\t$('#".$modlname."sortcolumn').val();".PHP_EOL;
		$gcont.="\t\t\t\t\t$('#".$modlname."sortorder').val();".PHP_EOL;
		$gcont.="\t\t\t\t\t".$modlname."addgrid(page,rowcount);".PHP_EOL;
		$gcont.="\t\t\t\t\t$('#processoverlay').hide();".PHP_EOL;
		$gcont.="\t\t\t\t});".PHP_EOL;
		$gcont.="\t\t\t\tsortordertypereset('".$modlname."sheadercolsort',headcolid,sortord);".PHP_EOL;
		$gcont.="\t\t\t}".PHP_EOL;
		$gcont.="\t\t\t{//pagination".PHP_EOL;
		$gcont.="\t\t\t\t$('.pvpagnumclass').click(function(){".PHP_EOL;
		$gcont.="\t\t\t\t\t$('#processoverlay').show();".PHP_EOL;
		$gcont.="\t\t\t\t\tvar page = $(this).data('pagenum');".PHP_EOL;
		$gcont.="\t\t\t\t\tvar rowcount = $('.pagerowcount').data('rowcount');".PHP_EOL;
		$gcont.="\t\t\t\t\t".$modlname."addgrid(page,rowcount);".PHP_EOL;
		$gcont.="\t\t\t\t\t$('#processoverlay').hide();".PHP_EOL;
		$gcont.="\t\t\t\t});".PHP_EOL;
		$gcont.="\t\t\t\t$('#".$modlname."pgrowcount').click(function(){".PHP_EOL;
		$gcont.="\t\t\t\t\t$('#processoverlay').show();".PHP_EOL;
		$gcont.="\t\t\t\t\tvar rowcount = $('this').val();".PHP_EOL;
		$gcont.="\t\t\t\t\t$('.pagerowcount').attr('data-rowcount',rowcount);".PHP_EOL;
		$gcont.="\t\t\t\t\t$('.pagerowcount').text(rowcount);".PHP_EOL;
		$gcont.="\t\t\t\t\tvar page = $('#prev').data('pagenum');".PHP_EOL;
		$gcont.="\t\t\t\t\t".$modlname."addgrid(page,rowcount);".PHP_EOL;
		$gcont.="\t\t\t\t\t$('#processoverlay').hide();".PHP_EOL;
		$gcont.="\t\t\t\t});".PHP_EOL;
		$gcont.="\t\t\t}".PHP_EOL;
		$gcont.="\t\t\tmaingridresizeheightset('".$modlname."addgrid');".PHP_EOL;
		$gcont.="\t\t\t$('#".$modlname."pgrowcount').material_select();".PHP_EOL;
		$gcont.="\t\t},".PHP_EOL;
		$gcont.="\t});".PHP_EOL;
		$gcont.="}".PHP_EOL;
		return $gcont;
	}
	public function gridacteventgenerate($modlname,$type,$modlid) {
		$actcont="\t{".PHP_EOL;
		$actcont.="\t\tfortabtouch = 0;".PHP_EOL;
		$actcont.="\t}".PHP_EOL;
		$actcont.="\t{".PHP_EOL;
		$addname=$this->eventname($modlname,$type,1);
		$actcont.="\t\t$('#".$addname."').click(function(){".PHP_EOL;
		$sldupname=$this->slideupevent($modlname);
		//$actcont.="\t\t\t".$sldupname.PHP_EOL;
		$actcont.="\t\t\tresetFields();".PHP_EOL;
		$actcont.="\t\t\tsectionpanelheight('groupsectionoverlay');".PHP_EOL;
		$actcont.="\t\t\t$('#groupsectionoverlay').removeClass('closed');".PHP_EOL;
		$actcont.="\t\t\t$('#groupsectionoverlay').addClass('effectbox');".PHP_EOL;	
		$actcont.="\t\t\t$('#".strtolower($modlname)."dataupdatesubbtn').hide().removeClass('hidedisplayfwg');".PHP_EOL;		
		$actcont.="\t\t\t$('#".strtolower($modlname)."savebutton').show();".PHP_EOL;
		$actcont.="\t\t\tshowhideiconsfun('addclick','".$modlname."creationformadd');".PHP_EOL;
		$actcont.="\t\t\tfirstfieldfocus();".PHP_EOL;
		$actcont.="\t\t\tvar elementname = $('#".strtolower($modlname)."elementsname').val();".PHP_EOL;
		$actcont.="\t\t\telementdefvalueset(elementname);".PHP_EOL;
		$actcont.="\t\t\trandomnumbergenerate('".strtolower($modlname)."anfieldnameinfo','".strtolower($modlname)."anfieldnameidinfo','".strtolower($modlname)."anfieldnamemodinfo','".strtolower($modlname)."anfieldtabinfo');".PHP_EOL;
		$actcont.="\t\t\t$('#tabgropdropdown').select2('val','1');".PHP_EOL;
		$actcont.="\t\t\tfortabtouch = 0;".PHP_EOL;
		$actcont.="\t\t\tMaterialize.updateTextFields();".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$editname=$this->eventname($modlname,$type,2);
		$actcont.="\t\t$('#".$editname."').click(function(){".PHP_EOL;
		$actcont.="\t\t\tvar datarowid = $('#".strtolower($modlname)."addgrid div.gridcontent div.active').attr('id');".PHP_EOL;
		$actcont.="\t\t\tif (datarowid) {".PHP_EOL;
		$actcont.="\t\t\t\t$('#groupsectionoverlay').removeClass('closed');".PHP_EOL;
		$actcont.="\t\t\t\t$('#groupsectionoverlay').addClass('effectbox');".PHP_EOL;
		$sldupname=$this->slideupevent($modlname);
		//$actcont.="\t\t\t\t".$sldupname.PHP_EOL;
		$actcont.="\t\t\t\t$('#".strtolower($modlname)."dataupdatesubbtn').show().removeClass('hidedisplayfwg');".PHP_EOL;
		$actcont.="\t\t\t\t$('#".strtolower($modlname)."savebutton').hide();".PHP_EOL;
		$actcont.="\t\t\t\t".$modlname."dataupdateinfofetchfun(datarowid);".PHP_EOL;
		$actcont.="\t\t\t\tfirstfieldfocus();".PHP_EOL;
		$actcont.="\t\t\t\tshowhideiconsfun('editclick','".$modlname."creationformadd');".PHP_EOL;
		$actcont.="\t\t\t\tfortabtouch = 1;".PHP_EOL;
		$actcont.="\t\t\t\tMaterialize.updateTextFields();".PHP_EOL;
		$actcont.="\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\talertpopup('Please select a row');".PHP_EOL;
		$actcont.="\t\t\t}".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$delname=$this->eventname($modlname,$type,3);
		$actcont.="\t\t$('#".$delname."').click(function(){".PHP_EOL;
		$actcont.="\t\t\tvar datarowid = $('#".strtolower($modlname)."addgrid div.gridcontent div.active').attr('id');".PHP_EOL;
		$actcont.="\t\t\tif (datarowid) {".PHP_EOL;
		$actcont.="\t\t\t\t$('#basedeleteoverlay').fadeIn();".PHP_EOL;
		$actcont.="\t\t\t$('#".strtolower($modlname)."primarydataid').val(datarowid);".PHP_EOL;
		$actcont.="\t\t\t\t$('#basedeleteyes').focus();".PHP_EOL;
		$actcont.="\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\talertpopup('Please select a row');".PHP_EOL;
		$actcont.="\t\t\t}".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$reloadname=$this->eventname($modlname,$type,4);
		$actcont.="\t\t$('#".$reloadname."').click(function(){".PHP_EOL;
		$actcont.="\t\t\trefreshgrid();".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$actcont.="\t\t$('#formclearicon').click(function(){".PHP_EOL;
		$actcont.="\t\t\tvar elementname = $('#elementsname').val();".PHP_EOL;
		$actcont.="\t\t\telementdefvalueset(elementname);".PHP_EOL;
		$actcont.="\t\t\trandomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$actcont.="\t\t$('#basedeleteyes').click(function(){".PHP_EOL;
		$actcont.="\t\t\tvar datarowid = $('#".strtolower($modlname)."addgrid div.gridcontent div.active').attr('id');".PHP_EOL;
		$actcont.="\t\t\t".$modlname."recorddelete(datarowid);".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$detailname=$this->eventname($modlname,$type,5);
		$actcont.="\t\t$('#".$detailname."').click(function() {".PHP_EOL;
		$actcont.="\t\t\tvar datarowid = $('#".$modlname."addgrid div.gridcontent div.active').attr('id');".PHP_EOL;
		$actcont.="\t\t\tif (datarowid) {".PHP_EOL;
		$actcont.="\t\t\t\t$('#groupsectionoverlay').removeClass('closed');".PHP_EOL;
		$actcont.="\t\t\t\t$('#groupsectionoverlay').addClass('effectbox');".PHP_EOL;
		$actcont.="\t\t\t\t$('.addbtnclass').addClass('hidedisplay');".PHP_EOL;
		$actcont.="\t\t\t\t$('.updatebtnclass').removeClass('hidedisplay');".PHP_EOL;
		$actcont.="\t\t\t\t$('#formclearicon').hide();".PHP_EOL;
		$actcont.="\t\t\t\tresetFields();".PHP_EOL;
		$actcont.="\t\t\t\t".$modlname."dataupdateinfofetchfun(datarowid);".PHP_EOL;
		$actcont.="\t\t\t\tshowhideiconsfun('summryclick','".$modlname."creationformadd');".PHP_EOL;
		$actcont.="\t\t\t\tMaterialize.updateTextFields();".PHP_EOL;
		$actcont.="\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\talertpopup('Please select a row');".PHP_EOL;
		$actcont.="\t\t\t}".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$actcont.="\t\t$('#editfromsummayicon').click(function() {".PHP_EOL;
		$actcont.="\t\t\t\tshowhideiconsfun('editfromsummryclick','".$modlname."creationformadd');".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;		
		$actcont.="\t\t".PHP_EOL;
		$printname=$this->eventname($modlname,$type,7);
		$actcont.="\t\t$('#".$printname."').click(function() {".PHP_EOL;
		$actcont.="\t\t\tvar datarowid = $('#".$modlname."addgrid div.gridcontent div.active').attr('id');".PHP_EOL;
		$actcont.="\t\t\tif (datarowid) {".PHP_EOL;
		$actcont.="\t\t\t\t$('#printpdfid').val(datarowid);".PHP_EOL;
		$actcont.="\t\t\t\t$('#templatepdfoverlay').fadeIn();".PHP_EOL;
		$actcont.="\t\t\t\tvar viewfieldids = $('#viewfieldids').val();".PHP_EOL;
		$actcont.="\t\t\t\tpdfpreviewtemplateload(viewfieldids);".PHP_EOL;
		$actcont.="\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\talertpopup('Please select a row');".PHP_EOL;
		$actcont.="\t\t\t}".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$clonename=$this->eventname($modlname,$type,8);
		$actcont.="\t\t$('#".$clonename."').click(function() {".PHP_EOL;
		$actcont.="\t\t\tvar datarowid = $('#".$modlname."addgrid div.gridcontent div.active').attr('id');".PHP_EOL;
		$actcont.="\t\t\tif (datarowid) {".PHP_EOL;
		$actcont.="\t\t\t\t$('#groupsectionoverlay').removeClass('closed');".PHP_EOL;
		$actcont.="\t\t\t\t$('#groupsectionoverlay').addClass('effectbox');".PHP_EOL;
		$actcont.="\t\t\t\t$('#processoverlay').show();".PHP_EOL;
		$actcont.="\t\t\t\t".$modlname."clonedatafetchfun(datarowid);".PHP_EOL;
		$actcont.="\t\t\t\t$('#tabgropdropdown').select2('val','1');".PHP_EOL;
		$actcont.="\t\t\t\trandomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');".PHP_EOL;
		$actcont.="\t\t\t\tMaterialize.updateTextFields();".PHP_EOL;
		$actcont.="\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\t	alertpopup('Please select a row');".PHP_EOL;
		$actcont.="\t\t\t}".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$smsname=$this->eventname($modlname,$type,9);
		$actcont.="\t\t$('#".$smsname."').click(function() {".PHP_EOL;
		$actcont.="\t\t\tvar datarowid = $('#".$modlname."addgrid div.gridcontent div.active').attr('id');".PHP_EOL;
		$actcont.="\t\t\tif (datarowid) {".PHP_EOL;
		$actcont.="\t\t\t\tvar viewfieldids = $('#viewfieldids').val();".PHP_EOL;
		$actcont.="\t\t\t\t$.ajax({".PHP_EOL;
		$actcont.="\t\t\t\t\turl:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,".PHP_EOL;
		$actcont.="\t\t\t\t\tdataType:'json',".PHP_EOL;
		$actcont.="\t\t\t\t\tasync:false,".PHP_EOL;
		$actcont.="\t\t\t\t\tcache:false,".PHP_EOL;
		$actcont.="\t\t\t\t\tsuccess :function(data) {".PHP_EOL;
		$actcont.="\t\t\t\t\t\tmobilenumber = [];".PHP_EOL;
		$actcont.="\t\t\t\t\t\tfor(var i=0;i<data.length;i++) {".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\tif(data[i] != '') {".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\tmobilenumber.push(data[i]);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t}".PHP_EOL;
		$actcont.="\t\t\t\t\t\t}".PHP_EOL;
		$actcont.="\t\t\t\t\t\tif(mobilenumber.length > 1) {".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t$('#mobilenumbershow').show();".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t$('#smsmoduledivhid').hide();".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t$('#smsrecordid').val(datarowid);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t$('#smsmoduleid').val(viewfieldids);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\tmobilenumberload(mobilenumber,'smsmobilenumid');".PHP_EOL;
		$actcont.="\t\t\t\t\t\t}else if(mobilenumber.length == 1) {".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\tsessionStorage.setItem('mobilenumber',mobilenumber);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\tsessionStorage.setItem('viewfieldids',viewfieldids);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\tsessionStorage.setItem('recordis',datarowid);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\twindow.location = base_url+'Sms';".PHP_EOL;
		$actcont.="\t\t\t\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\talertpopup('Invalid mobile number');".PHP_EOL;
		$actcont.="\t\t\t\t\t\t}".PHP_EOL;
		$actcont.="\t\t\t\t\t},".PHP_EOL;
		$actcont.="\t\t\t\t});".PHP_EOL;
		$actcont.="\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\talertpopup('Please select a row');".PHP_EOL;
		$actcont.="\t\t\t}".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$mailname=$this->eventname($modlname,$type,10);
		$actcont.="\t\t$('#".$mailname."').click(function() {".PHP_EOL;
		$actcont.="\t\t\tvar datarowid = $('#".$modlname."addgrid div.gridcontent div.active').attr('id');".PHP_EOL;
		$actcont.="\t\t\tif (datarowid)".PHP_EOL;
		$actcont.="\t\t\t{".PHP_EOL;
		$actcont.="\t\t\t\tvar emailtosend = getvalidemailid(datarowid,'".$modlname."','emailid','','');".PHP_EOL;
		$actcont.="\t\t\t\tsessionStorage.setItem('forcomposemailid',emailtosend.emailid);".PHP_EOL;
		$actcont.="\t\t\t\tsessionStorage.setItem('forsubject',emailtosend.subject);".PHP_EOL;
		$actcont.="\t\t\t\tsessionStorage.setItem('moduleid','".$modlid."');".PHP_EOL;
		$actcont.="\t\t\t\tsessionStorage.setItem('recordid',datarowid);".PHP_EOL;
		$actcont.="\t\t\t\tvar fullurl = 'erpmail/?_task=mail&_action=compose';".PHP_EOL;
		$actcont.="\t\t\t\twindow.open(''+base_url+''+fullurl+'');".PHP_EOL;
		$actcont.="\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\talertpopup('Please select a row');".PHP_EOL;
		$actcont.="\t\t\t}".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$outgoingcallname=$this->eventname($modlname,$type,11);
		$actcont.="\t\t$('#".$outgoingcallname."').click(function() {".PHP_EOL;
		$actcont.="\t\t\tvar datarowid = $('#".$modlname."addgrid div.gridcontent div.active').attr('id');".PHP_EOL;
		$actcont.="\t\t\tif (datarowid) {".PHP_EOL;
		$actcont.="\t\t\t\tvar viewfieldids = $('#viewfieldids').val();".PHP_EOL;
		$actcont.="\t\t\t\t	$.ajax({".PHP_EOL;
		$actcont.="\t\t\t\t\turl:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,".PHP_EOL;
		$actcont.="\t\t\t\t\tdataType:'json',".PHP_EOL;
		$actcont.="\t\t\t\t\tasync:false,".PHP_EOL;
		$actcont.="\t\t\t\t\tcache:false,".PHP_EOL;
		$actcont.="\t\t\t\t\tsuccess :function(data) {".PHP_EOL;
		$actcont.="\t\t\t\t\t\tmobilenumber = [];".PHP_EOL;
		$actcont.="\t\t\t\t\t\tif(data != 'No mobile number') {".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\tfor(var i=0;i<data.length;i++){".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\tif(data[i] != ''){".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\t\tmobilenumber.push(data[i]);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\t}".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t}".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\tif(mobilenumber.length > 1) {".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\t$('#c2cmobileoverlay').show();".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\t$('#callmoduledivhid').hide();".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\t$('#calcount').val(mobilenumber.length);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\t$('#callrecordid').val(datarowid);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\t$('#callmoduleid').val(viewfieldids);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\tmobilenumberload(mobilenumber,'callmobilenum');".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t} else if(mobilenumber.length == 1){".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\tclicktocallfunction(mobilenumber);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\talertpopup('Invalid mobile number');".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t}".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\t$('#c2cmobileoverlay').show();".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\t$('#callmoduledivhid').show();".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\t$('#callrecordid').val(datarowid);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\t$('#callmoduleid').val(viewfieldids);".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t\tmoduledropdownload(viewfieldids,datarowid,'callmodule');".PHP_EOL;
		$actcont.="\t\t\t\t\t\t\t}".PHP_EOL;
		$actcont.="\t\t\t\t\t},".PHP_EOL;
		$actcont.="\t\t\t\t	});".PHP_EOL;
		$actcont.="\t\t\t} else {".PHP_EOL;
		$actcont.="\t\t\t\t	alertpopup('Please select a row');".PHP_EOL;
		$actcont.="\t\t\t}".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$custname=$this->eventname($modlname,$type,12);
		//Customize icon
		$actcont.="\t\t$('#".$custname."').click(function() {".PHP_EOL;
		$actcont.="\t\t\tvar viewfieldids = $('#viewfieldids').val();".PHP_EOL;
		$actcont.="\t\t\tsessionStorage.setItem('customizeid',viewfieldids);".PHP_EOL;
		$actcont.="\t\t\twindow.location = base_url+'Formeditor';".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$importname=$this->eventname($modlname,$type,13);
		$actcont.="\t\t$('#".$importname."').click(function(){".PHP_EOL;
		$actcont.="\t\t\tvar viewfieldids = $('#viewfieldids').val();".PHP_EOL;
		$actcont.="\t\t\tsessionStorage.setItem('importmoduleid',viewfieldids);".PHP_EOL;
		$actcont.="\t\t\twindow.location = base_url+'Dataimport';".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$datamaniname=$this->eventname($modlname,$type,14);
		//data manipulation icon
		$actcont.="\t\t$('#".$datamaniname."').click(function() {".PHP_EOL;
		$actcont.="\t\t\tvar viewfieldids = $('#viewfieldids').val();".PHP_EOL;
		$actcont.="\t\t\tsessionStorage.setItem('datamanipulateid',viewfieldids);".PHP_EOL;
		$actcont.="\t\t\twindow.location = base_url+'Massupdatedelete';".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$findname=$this->eventname($modlname,$type,15);
		//Find Duplication icon
		$actcont.="\t\t$('#".$findname."').click(function() {".PHP_EOL;
		$actcont.="\t\t\tvar viewfieldids = $('#viewfieldids').val();".PHP_EOL;
		$actcont.="\t\t\tsessionStorage.setItem('finddubid',viewfieldids);".PHP_EOL;
		$actcont.="\t\t\twindow.location = base_url+'Findduplicates';".PHP_EOL;
		$actcont.="\t\t});".PHP_EOL;
		$actcont.="\t}".PHP_EOL;
		return $actcont;
	}
	public function griddynamicdataaddfungen($modlname,$type) {
		$dacont="function ".$modlname."newdataaddfun() {".PHP_EOL;
		$dacont.="\tvar amp = '&';".PHP_EOL;
		$dacont.="\tvar formdata = $('#".strtolower($modlname)."addform').serialize();".PHP_EOL;
		$dacont.="\tvar datainformation = amp + formdata;".PHP_EOL;
		$dacont.="\t$.ajax({".PHP_EOL;
		$dacont.="\t\turl: base_url + '".ucfirst($modlname)."/newdatacreate',".PHP_EOL;
		$dacont.="\t\t\tdata: 'datas=' + datainformation,".PHP_EOL;
		$dacont.="\t\t\ttype: 'POST',".PHP_EOL;
		$dacont.="\t\t\tcache:false,".PHP_EOL;
		$dacont.="\t\t\tsuccess: function(msg) {".PHP_EOL;
		$dacont.="\t\t\t\tvar nmsg =  $.trim(msg);".PHP_EOL;
		$dacont.="\t\t\t\tif (nmsg == 'TRUE'){".PHP_EOL;
		$dacont.="\t\t\t\t\trefreshgrid();".PHP_EOL;
		$dacont.="\t\t\t\t\talertpopup(savealert);".PHP_EOL;
		$dacont.="\t\t\t\t\tresetFields();".PHP_EOL;
		$dacont.="\t\t\t\t\t$('.addsectionclose').trigger('click');".PHP_EOL;
		$dacont.="\t\t\t\t\t\taddformview = 0;".PHP_EOL;
		$dacont.="\t\t\t\t}".PHP_EOL;
		$dacont.="\t\t\t},".PHP_EOL;
		$dacont.="\t})".PHP_EOL;
		$dacont.="}".PHP_EOL;
		return $dacont;
	}
	public function griddynamicdataupdatefungen($modlname,$type) {
		$dacont="function ".$modlname."dataupdatefun() {".PHP_EOL;
		$dacont.="\tvar amp = '&';".PHP_EOL;	
		$dacont.="\tvar formdata = $('#".strtolower($modlname)."addform').serialize();".PHP_EOL;
		$dacont.="\tvar datainformation = amp + formdata;".PHP_EOL;		
		$dacont.="\t$.ajax({".PHP_EOL;
		$dacont.="\t\turl: base_url+'".ucfirst($modlname)."/datainformationupdate',".PHP_EOL;
		$dacont.="\t\tdata: 'datas=' + datainformation,".PHP_EOL;
		$dacont.="\t\ttype: 'POST',".PHP_EOL;
		$dacont.="\t\tsuccess: function(msg) {".PHP_EOL;
		$dacont.="\t\t\tvar nmsg =  $.trim(msg);".PHP_EOL;
		$dacont.="\t\t\tif (nmsg == 'TRUE') {".PHP_EOL;
		$dacont.="\t\t\t\t$('.ftab').trigger('click');".PHP_EOL;
		$dacont.="\t\t\t\tresetFields();".PHP_EOL;		
		$dacont.="\t\t\t\t$('.addsectionclose').trigger('click');".PHP_EOL;
		$dacont.="\t\t\t\trefreshgrid();".PHP_EOL;
		$dacont.="\t\t\t\t$('#".strtolower($modlname)."dataupdatesubbtn').attr('disabled',false);".PHP_EOL;
		$dacont.="\t\t\t\talertpopup(savealert);".PHP_EOL;
		$dacont.="\t\t\t} else if (nmsg == 'false') {".PHP_EOL;
		$dacont.="\t\t\t}".PHP_EOL;
		$dacont.="\t\t},".PHP_EOL;
		$dacont.="\t});".PHP_EOL;
		$dacont.="}".PHP_EOL;
		return $dacont;
	}
	public function griddynamicdatadelfungen($modlname) {
		$ddecont="function ".$modlname."recorddelete(datarowid) {".PHP_EOL;
		$ddecont.="\tvar elementstable = $('#".strtolower($modlname)."elementstable').val();".PHP_EOL;
		$ddecont.="\tvar elementspartable = $('#".strtolower($modlname)."elementspartabname').val();".PHP_EOL;
		$ddecont.="\t$.ajax({".PHP_EOL;
		$ddecont.="\t\turl: base_url + '".ucfirst($modlname)."/deleteinformationdata?primarydataid='+datarowid+'&elementstable='+elementstable+'&parenttable='+elementspartable,".PHP_EOL;
		$ddecont.="\t\tasync:false,".PHP_EOL;
		$ddecont.="\t\tsuccess: function(msg) {".PHP_EOL;
		$ddecont.="\t\t\tvar nmsg =  $.trim(msg);".PHP_EOL;
		$ddecont.="\t\t\tif (nmsg == 'TRUE') {".PHP_EOL;
		$ddecont.="\t\t\t\trefreshgrid();".PHP_EOL;
		$ddecont.="\t\t\t\t$('#basedeleteoverlay').fadeOut();".PHP_EOL;
		$ddecont.="\t\t\t\talertpopup('Deleted successfully');".PHP_EOL;
		$ddecont.="\t\t\t} else if (nmsg == 'Denied') {".PHP_EOL;
		$ddecont.="\t\t\t\trefreshgrid();".PHP_EOL;
		$ddecont.="\t\t\t\t$('#basedeleteoverlay').fadeOut();".PHP_EOL;
		$ddecont.="\t\t\t\talertpopup('Permission denied');".PHP_EOL;
		$ddecont.="\t\t\t}".PHP_EOL;
		$ddecont.="\t\t}".PHP_EOL;
		$ddecont.="\t});".PHP_EOL;
		$ddecont.="}".PHP_EOL;
		return $ddecont;
	}
	public function eventname($modlname,$type,$etype) {
		switch($type) {
			case 1 == $type && $etype==1 :
				return "addicon";
				break;
			case 1 == $type && $etype==2 :
				return "editicon";
				break;
			case 1 == $type && $etype==3 :
				return "deleteicon";
				break;
			case 1 == $type && $etype==4 :
				return "reloadicon";
				break;
			case 1 == $type && $etype==5 :
				return "detailedviewicon";
				break;
			case 1 == $type && $etype==6 :
				return "dashboardicon";
				break;
			case 1 == $type && $etype==7 :
				return "printicon";
				break;
			case 1 == $type && $etype==8 :
				return "cloneicon";
				break;
			case 1 == $type && $etype==9 :
				return "smsicon";
				break;
			case 1 == $type && $etype==10 :
				return "mailicon";
				break;
			case 1 == $type && $etype==11 :
				return "outgoingcallicon";
				break;
			case 1 == $type && $etype==12 :
				return "customizeicon";
				break;
			case 1 == $type && $etype==13 :
				return "importicon";
				break;
			case 1 == $type && $etype==14 :
				return "datamanipulationicon";
				break;
			case 1 == $type && $etype==15 :
				return "findduplicatesicon";
				break;
			case 2 == $type && $etype==1 :
				return $modlname."addicon";
				break;
			case 2 == $type && $etype==2 :
				return $modlname."editicon";
				break;
			case 2 == $type && $etype==3 :
				return $modlname."deleteicon";
				break;
			case 2 == $type && $etype==4 :
				return $modlname."reloadicon";
				break;
			default:
				break;
		}
	}
	public function dataeventname($modlname,$type,$etype) {
		switch($type) {
			case 1 == $type && $etype==1 :
				return "dataaddsbtn";
				break;
			case 1 == $type && $etype==2 :
				return "dataupdatesubbtn";
				break;
			case 2 == $type && $etype==1 :
				return $modlname."savebutton";
				break;
			case 2 == $type && $etype==2 :
				return $modlname."dataupdatesubbtn";
				break;
			default:
				break;
		}
	}
	public function slideupevent($modlname) {
		$slcont="addslideup('".$modlname."creationview','".$modlname."creationformadd');";
		return $slcont;
	}
	public function addremoveclass($name,$type) {
		switch($type) {
			case 0:
				$hide="$('".$name."').addClass('hidedisplay')";
				return $hide;
				break;
			case 1:
				$show="$('".$name."').removeClass('hidedisplay')";
				return $show;
				break;
			default:
				break;
		}
	}
}
?>