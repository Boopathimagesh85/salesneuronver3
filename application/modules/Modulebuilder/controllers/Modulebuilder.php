<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Modulebuilder extends CI_Controller {
	function __construct() {
    	parent::__construct();	
		$this->load->model('Base/Basefunctions');
		$this->load->model('Modulebuilder/Modulebuildermodel');
		$this->load->helper('directory');
		$this->load->helper('file');
    }
	public function index() {
		$moduleid = array(239);
		sessionchecker($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$this->load->view('Modulebuilder/modulebuilderview',$data);
	}
	//condition drop down value fetch
	public function ddcondfieldnamefetch() {
		$this->Modulebuildermodel->ddcondfieldnamefetchmodel();
	}
	//module name check
	public function modulenamecheck() {
		$this->Modulebuildermodel->modulenamecheckmodel();
	}
	//pick list name check
	public function picklistnamecheck() {
		$this->Modulebuildermodel->picklistnamecheckmodel();
	}
	//new module create
	public function modbuildercreate() {
		$this->Modulebuildermodel->modbuildercreatemodel();
	}
}