<!DOCTYPE HTML>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
	<!-- <link href="<?php //echo base_url();?>css/font-awesome.css" rel="stylesheet" type="text/css"> -->
	<style>
	[draggable] {
	  -moz-user-select: none;
	  -khtml-user-select: none;
	 * -webkit-user-select: none;
	  -o-user-select: none;
	  user-select: none;
	}
	[draggable] * {
	  -moz-user-drag: none;
	  -khtml-user-drag: none;
	 * -webkit-user-drag: none;
	  -o-user-drag: none;
	  user-drag: none;
	}
	.sortable{list-style-type: none;margin-left:0;}
	#spanforaddsection{list-style-type: none;margin-left:0;}
	
	.hidecontainer{list-style-type: none;margin:0;}
	#s2id_modulecreactions ul li div{color:#ffffff !important}
	#droptarget1 select{margin:0 !important;height:2rem !important}
	.elementsgroupcls{overflow-y:auto;overflow-x:hidden;}
	
	.steppervertical{height:612px;overflow-x:hidden;overflow-y:auto;background-color:#eceff1; padding: 10px;}	
	.steppercontent ul{margin:0 auto;}	
	.steppercontent {padding:20px 0}	
	*,
	*:before,
	*:after {
	    box-sizing: border-box;
	}
	.step { position: relative; min-height: 32px /* circle-size */ ; }
	.step > div:first-child { position: static; height: 0; }
	.step > div:last-child { margin-left: 32px ; *padding-left: 16px; }
	.circle { background: #546e7a; width: 32px; height: 32px; line-height: 32px; border-radius: 16px; position: relative; color: white; text-align: center; }
	.line { position: absolute; border-left: 1px solid gainsboro; left: 16px; bottom: 10px; top: 42px; }
	.step:last-child .line { display: none; }
	.title { line-height: 32px;  padding-left:12px;cursor:pointer;font-size: 16px;font-weight: bold; }
	.fbuilderaccheader{color:#000;font-size:0.9rem;}
	.elementiconsul div{color: #546e7a;font-size: 0.8rem;}
	.fbuildericonsize {
	    color: #949494 !important;
	    font-size: 1.5rem !important;
	}
	.bgcolorfbelements{
		padding-bottom:1rem;
	}
	.elementiconsul li {
	    width: 45%;
	    margin:2% 1% 2% 1%;
	    *box-shadow:0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
	    box-shadow:0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 0px 0px 0 rgba(0, 0, 0, 0.12);
	    padding-top:0.6rem;
	    padding-bottom:0.5rem;
	    cursor:move;
	    background: #fff;
	}
	.elementiconsul > li:hover{
		background-color:#eceff1;
	}
	.columnselectionclass{box-shadow:0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 5px 0 rgba(0, 0, 0, 0.12)}
	.sortable-ghost .columnselectionclass,.sortable-ghost .hovertoremove{opacity:0.7;background-color:#e0e0e0;}
	.fbuilderleftcation i {
	    position: relative;
	    top: 12px;
	}
	.dropareadiv .foriconcolor i{top:7px;}
	@media only screen and (min-device-width: 768px) and (max-device-width:1024px){
	.titlehide{
	display:none;
		}
	}
    </style>
</head>
<body class="hidedisplay">
	<div class="row" style="max-width:100%">
		<div class="off-canvas-wrap" data-offcanvas>
			<div class="inner-wrap">
				<div class="large-12 columns paddingzero studiocontainer">
					<div class="large-9 medium-9 column" id="leftsidepanel" style="background-color:#617d8a;">							
						<div class="large-12 column fbuilderleftcation" style="height:51px;line-height:46px;">
						<!-- 	<span data-activates="slide-out" class="headermainmenuicon button-collapse" title="Menu"><i class="material-icons">menu</i></span>	-->	
							<span class="gridcaptionpos" style="top:-0.2rem;position:relative;">
							<span><?php //echo $gridtitle['title']; ?><span class="titlehide">Module &nbsp;</span>Builder</span>
							</span>
						</div>
						<!-- <div class="large-12 columns tabgroupstyle" style="height:35px;">
							<ul class="tabs fbuildertab fortabicons" data-tab="">
								<span id="blockspan" class="blockspan"></span>
							</ul>
						</div> -->
						<div class="dropareadiv" style="overflow-y:scroll;height:38rem;position:relative;width:100%;background-color:#fff ;">
						<form name="newmodulegenform" id="newmodulegenform" method="POST" class="large-12 column paddingzero">
							<span id="spanforaddtab">
							</span>
							<input type="hidden" name="userversion" id="userversion" value="1" />
						</form>
					</div>
				</div>
				<?php
					$device = $this->Basefunctions->deviceinfo();
					if($device=='phone') {
						$this->load->view('Base/overlaymobile');
					} else {
						$this->load->view('Base/overlay');
					} 
					$this->load->view('Base/modulelist');
					$appdateformat = $this->Basefunctions->appdateformatfetch();
				?>
				<input type="hidden" name="dataformatter" id="dataformatter" value="<?php echo $appdateformat;?>" />
				<div class="large-3 medium-3 column"  id="droptarget1">
					<div class="large-12 column fbuilderleftcation mainaction" style="height:50px;padding:0;">
						<div class="large-6 medium-6 column"> 
							<span id="previewcaption" class="hidedisplay" style="top:16px;position:relative;">Preview </span>
						</div>					
						<div class="large-6 medium-6 column foriconcolor" style="text-align: right;float: right; margin-right: 0px;"> 
							<span id="modbuildersavebtn" class="icon-box"><i class="material-icons" title="Save">save</i><span class="actiontitle"> Save</span></span>
							<span id="modulebuilddelicon" class="icon-box"><i class="material-icons" title="Reload">refresh</i><span class="actiontitle"> Reload</span></span>
							<span id="fullviewpreview" class="icon-box"><i class="material-icons">visibility</i><span class="actiontitle"> Preview</span></span>
							<span id="normalviewpreview" class="icon-box"><i class="material-icons">visibility</i><span class="actiontitle"> Preview</span></span>
							<?php
								//user menu
								$this->load->view('Base/usermenugenerate');
							?>
						</div>
					</div>
					
					<div class="large-12 columns steppervertical">
							<div class="step">
							    <div>
							        <div class="circle">1</div>
							        <div class="line"></div>
							    </div>
							    <div>
							        <div class="title  steppercat steppercategory1" data-steppercat="1">Module <span class="titlehide">Details</span> <i class=" icon24build icon24build-chevron-down3 toggleicons" style="position:absolute;right:5px;top:8px;"></i></div>
							         <div class="steppercontent">
							        	<ul class="stepperul1 hidedisplay ">
								        	<li style="display:inline-block;width:100%" class="">
												<div class="large-12 column"  id="fbuildercat1" style="padding:0">
													<ul class="fbulderheadermenu">
													   <!-- ><li class="fbuilderaccli fbuilderaccclick fortrigger1" data-elementcat="1">
															<div class="large-6 medium-6 small-6 column">
																<label class="fbuilderaccheader">
																Module Details
																</label>
															</div>
															<div class="large-6 medium-6 small-6 column" style="text-align:right">
																<span id="" class="toggleicons fa fa-minus-square-o accplusbtn" title=""> </span>
															</div>
														</li>-->
														<div id="" class="bgcolorfbelements elementlist1 hideotherelements large-12 columns">
															<ul class="elementiconsula">
																<span  id="formmodulenamvalidate" class="validationEngineContainer">
																	<div class="input-field large-12 columns">
																		<input name="newmodulecrename" type="text" id="newmodulecrename" class="validate[required,funcCall[modulenamecheck]]" value="" tabindex="101"/>
																		<label for="newmodulecrename">Module Name <span id="" class="mandatoryfildclass">*</span></label>
																	</div>
																	<div class="static-field large-12 columns">
																		<label>Action Menu <span id="" class="mandatoryfildclass">*</span></label>
																		<select name="moduleactionname" data-placeholder="Select Actions" data-prompt-position="topLeft:14,140" id="moduleactionname" class="validate[required] chzn-select dropdownchange" multiple="multiple" tabindex="102">
																			<option></option>
																			<?php
																			$i=1;
																			$CI =& get_instance();
																			$data = $CI->Basefunctions->simplegroupdropdown('toolbarnameid','toolbarname','toolbarname','industryid','1');
																			foreach($data as $key):
																			?>
																				<option value="<?php echo $key->toolbarnameid;?>" ><?php echo $key->toolbarname;?></option>
																			<?php
																				$i++;
																			endforeach;
																			?>
																		</select>
																		<input name="modactionslist" type="hidden" id="modactionslist" value="" />
																	</div>
																	<div class="large-12 medium-12 small-12 columns ">
																		<input id="nextstep" name=""  class="btn" tabindex="103" value="Next"  type="button" style="margin-top:1rem;">
																	</div>
																</span>									
															</ul>
														</div>
													</ul>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="step">
							    <div>
							        <div class="circle">2</div>
							        <div class="line"></div>
							    </div>
							    <div>
							        <div class="title steppercat steppercategory2" data-steppercat="2">Template <span class="titlehide"> Details</span> <i class=" icon24build icon24build-chevron-down3 toggleicons" style="position:absolute;right:5px;top:8px;"></i></div>
							         <div class="steppercontent">
							        	<ul class="stepperul2 hidedisplay stepperulhideotherelements">
								        	<li style="display:inline-block;width:100%" class="">
												<div class="large-12 column"  id="fbuildercat2" style="padding:0">
													<ul class="fbulderheadermenu">
														<div id="getformdetails" class="" style="background-color:#eceff1;width:100%;padding:0 0.9375rem;height:100px">
															<label>Form Type<span id="" class="mandatoryfildclass">*</span></label>
															<select name="formtype" data-placeholder="Select Type" data-prompt-position="topLeft:14,36" id="formtype" class="validate[required] chzn-select" tabindex="104">
															<option></option>
															<option value="1" >Overlay Form</option>
															<option value="2" >Normal Form</option>
															</select>
														</div>
														<div id="elementsgroups" class="hidedisplay elementsgroupcls">
														<li class="fbuilderaccli fbuilderaccclick fortrigger2" data-elementcat="2">
															<div class="large-12 medium-12 small-6 columns paddingzero">
																<label class="fbuilderaccheader">
																Tab Group
																</label>
															</div>
															<!-- <div class="large-6 medium-6 small-6 column" style="text-align:right">
																<span id="" class="toggleicons fa fa-plus-square-o accplusbtn" title=""> </span>
															</div> -->
														</li>
														<div id="" class="bgcolorfbelements elementlist2 hideotherelements">
															<ul class="elementiconsul">
																<li draggable="true" id="withoutgridgen">
																	<span class="icon24build icon24build-view-module fbuildericonsize"> </span>
																	<div> Tab Group </div>
																</li>
															</ul>
														</div>
														<li class="fbuilderaccli fbuilderaccclick fortrigger3" data-elementcat="3">
															<div class="large-12 medium-12 small-6 columns paddingzero">
																<label class="fbuilderaccheader">
																Template
																</label>
															</div>
															<!-- <div class="large-6 medium-6 small-6 column" style="text-align:right">
																 <span id="" class="toggleicons fa fa-plus-square-o accplusbtn" title=""> </span>
															</div> -->
														</li>
														<div id="" class="bgcolorfbelements  elementlist3 hideotherelements">
															<ul class="elementiconsul">
																<li draggable="true" id="sectiongen">
																	<span class="icon24build icon24build-crop-portrait fbuildericonsize"> </span>
																	<div> Section </div>
																</li>
																<li draggable="true" id="sectionwithgridgen">
																	<span class="icon24build icon24build-view-grid fbuildericonsize"> </span>
																	<div> Section With Grid </div>	
																</li>
																<li draggable="true" id="sectionwitheditorgen">
																	<span class="icon24build icon24build-view-grid fbuildericonsize"> </span>
																	<div> Section With Editor </div>
																</li>
															</ul>
														</div>
														<li class="fbuilderaccli fbuilderaccclick fortrigger4" data-elementcat="4">
															<div class="large-12 medium-12 small-6 columns paddingzero">
																<label class="fbuilderaccheader">
																Form Elements
																</label>
															</div>
															<!-- <div class="large-6 medium-6 small-6 column" style="text-align:right">
																 <span id="" class="toggleicons fa fa-plus-square-o accplusbtn" title=""> </span>
															</div> -->
														</li>
														<div id="forproppanelshow" class="bgcolorfbelements  elementlist4 hideotherelements">
															<ul class="elementiconsul">
																<li draggable="true" id="textboxgen">
																	<span class="icon24build icon24build-checkbox-blank-outline fbuildericonsize"> </span>
																	<div> Text Box </div>
																</li>
																<li draggable="true" id="textareagen">  
																	<span class="icon24build icon24build-message-text fbuildericonsize"> </span>
																	<div> Text Area </div>
																</li>
															</ul>
															<ul class="elementiconsul">
																<li draggable="true" id="integerboxgen"> 
																	<span class="icon24build icon24build-numeric fbuildericonsize"> </span>
																	<div> Integer </div>
																</li>
																<li draggable="true" id="decimalboxgen"> 
																	<span class="icon24build icon24build-decimal-decrease fbuildericonsize"> </span>
																	<div> Decimal </div>
																</li>
															</ul>
															<ul class="elementiconsul">
																<li draggable="true" id="percentgen">
																	<span class="icon24build icon24build-percent fbuildericonsize"> </span>
																	<div> Percent </div>
																</li>
																<li draggable="true" id="dropdowngen">
																	<span class="icon24build icon24build-view-list fbuildericonsize"> </span>
																	<div> Picklist </div>										
																</li>
															</ul>
															<ul class="elementiconsul">
																<li draggable="true" id="currencygen">
																	<span class="icon24build icon24build-currency-inr fbuildericonsize"> </span>
																	<div> Currency </div>
																</li>
																<li draggable="true" id="dategen">
																	<span class="icon24build icon24build-calendar-today fbuildericonsize"> </span>
																	<div> Date </div>	
																</li>
															</ul>
															<ul class="elementiconsul">
																<li draggable="true" id="timegen">
																	<span class="icon24build icon24build-clock fbuildericonsize"> </span>
																	<div> Time </div>	
																</li>
																<li draggable="true" id="emailgen">
																	<span class="icon24build icon24build-email-outline fbuildericonsize"> </span>
																	<div> E-Mail </div>										
																</li>
															</ul>
															<ul class="elementiconsul">
																<li draggable="true" id="phonegen">
																	<span class="icon24build icon24build-phone fbuildericonsize"> </span>
																	<div> Phone </div>										
																</li>
																<li draggable="true" id="urlgen">
																	<span class="icon24build icon24build-link fbuildericonsize"> </span>
																	<div> URL </div>
																</li>
															</ul>
															<ul class="elementiconsul">
																<li draggable="true" id="texteditorgen">
																	<span class="icon24build icon24build-file-document-box fbuildericonsize"> </span>
																	<div> Text Editor </div>
																</li>
																<li draggable="true" id="autonumbergen">
																	<span class="icon24build icon24build-credit-card fbuildericonsize"> </span>
																	<div> Auto Number </div>
																</li>
															</ul>
															<ul class="elementiconsul">
																<li draggable="true" id="checkboxgen">
																	<span class="icon24build icon24build-checkbox-marked-outline fbuildericonsize"> </span>
																	<div> Check Box </div>
																</li>
																<li draggable="true" id="passwordgen">
																	<span class="icon24build icon24build-lock-open fbuildericonsize"> </span>
																	<div> Password </div>
																</li>
															</ul>
														</div>	
														</div>						
													</ul>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="step">
							    <div>
							        <div class="circle">3</div>
							        <div class="line"></div>
							    </div>
							    <div>
							        <div class="title steppercat steppercategory1" data-steppercat="3" >Properties<i class=" icon24build icon24build-chevron-down3 toggleicons" style="position:absolute;right:5px;top:8px;"></i></div>
							        <div class="steppercontent">
							        	<ul class="stepperul3 stepperulhideotherelements hidedisplay">
								        	<li style="display:inline-block;">
												<div class="large-12 column propertiesdataclear" id="fbuildercat3"  style="padding:0">
													<div class="large-12 columns propertytext">
														Please Select any field from right side 
													</div>
													<ul class="fbulderheadermenu">
														<!-- <li class="fbuilderaccli fbuilderaccclick fortrigger5" id="elepropertiesliid" data-elementcat="5">
															<div class="large-6 medium-6 small-6 column">
																<label class="fbuilderaccheader">
																Properties
																</label>
															</div>
															<div class="large-6 medium-6 small-6 column" style="text-align:right">
																 <span id="" class="toggleicons fa fa-plus-square-o accplusbtn" title=""> </span>
															</div>
														</li> -->
														<div id="propertypanel" class="bgcolorfbelements hidedisplay elementlist5 hideotherelements propertiesclear validationEngineContainer">
															<ul style="padding-right: 1.2rem;">
																<li>
																	<div class="input-field large-12 columns ">
																		<input type="text" id="frmelemattrlablname" name="frmelemattrlablname" class="validate[required,funcCall[picklisttabvalidate]]"> </input>
																		<label for="frmelemattrlablname">Label Name</label>
																	</div>
																</li>
																<li id="dataplvalattr" class="hidedisplay">
																	<div class="static-field large-12 columns ">
																	<input type="text" id="frmelemdatavalue" name="frmelemdatavalue"> </input>
																	<label for="frmelemdatavalue">Value</label>
																	</div>
																</li>
																<li id="attrmodopt" class="hidedisplay">
																	<div class="static-field large-12 columns">
																	<label>Module</label>
																	<select name="attrmodulename" data-placeholder="Select" class="chzn-select" id="attrmodulename">
																		<option value="">select</option>
																		<?php
																			$CI =& get_instance();
																			$modulename=$CI->Basefunctions->simpledropdown('module','moduleid,modulename,modulemastertable','modulename');
																			foreach($modulename as $key):
																			if( $key->modulemastertable!="" ) {
																		?>
																				<option value="<?php echo $key->modulemastertable;?>" data-datamoduleid="<?php echo $key->moduleid;?>" ><?php echo $key->modulename;?></option>
																		<?php
																			}
																			endforeach;
																		?>
																	</select>
																	</div>
																</li>
																<li id="attrdispfldname" class="hidedisplay">
																	<div class="static-field large-12 columns">
																	<label>Field Name</label>
																	<select name="frmdispelefiledname" data-placeholder="Select" class="chzn-select" id="frmdispelefiledname">
																		<option></option>
																	</select>
																	</div>
																</li>
																<li id="attrfldname" class="hidedisplay">
																	<div class="static-field large-12 columns">
																	<label>Condition Field Name</label>
																	<select name="frmelefiledname" data-placeholder="Select" class="chzn-select" id="frmelefiledname">
																		<option></option>
																	</select>
																	</div>
																</li>
																<li id="attrmodfldcond" class="hidedisplay">
																	<div class="static-field large-12 columns">
																	<label>Condition</label>
																	<select data-placeholder="Select"  class="chzn-select" name="frmelecondname" id="frmelecondname">
																		<option value="">select</option>
																		<option value="Equalto">Equalto</option>
																		<option value="NotEqual">NotEqual</option>
																		<option value="Startwith">Startwith</option>
																		<option value="Endwith">Endwith</option>
																		<option value="Middle">Middle</option>
																	</select>
																	</div>
																</li>
																<li id="attrmodfldval" class="hidedisplay">
																	<div class="input-field large-12 columns">
																	<input type="text" id="frmelmcondval" name="frmelmcondval"> </input>
																	<label for="frmelmcondval">Value</label>
																	</div>
																</li>
																<li id="defval" class="">
																	<div class="input-field large-12 columns">
																	<input type="text" id="frmelemattrdefname" name="frmelemattrdefname"> </input>
																	<label for="frmelemattrdefname">Default Value</label>
																	</div>
																</li>
																<li id="dddefval" class="hidedisplay">
																	<div class="static-field large-12 columns">
																	<label>Default Value</label>
																	<select name="dddefaultvalue" data-placeholder="Select" class="chzn-select" id="dddefaultvalue">
																	</select>
																	</div>
																</li>
																<li id="datedefval" class="hidedisplay">
																	<div class="input-field large-12 columns">
																	<input type="text" id="dateaddfielddefval" value="" name="dateaddfielddefval" /> </input>
																	<label for="dateaddfielddefval">Default Date</label>
																	</div>
																	<?php
																		$appdateformat = $this->Basefunctions->appdateformatfetch();
																	echo "<script>
																		$(document).ready(function(){
																			$('#dateaddfielddefval').datetimepicker({
																				dateFormat: '".$appdateformat."',
																				showTimepicker :false,
																				minDate: 0,
																				onSelect: function () {
																					var checkdate = $(this).val();
																					if(checkdate != '') {
																						$(this).next('span').remove('.btmerrmsg'); 
																						$(this).removeClass('error');
																					}
																				}
																			});
																			//$('#dateaddfielddefval').datetimepicker('setDate', '' );
																		});
																	</script>";
																	?>
																</li>
																<li id="timedefval" class="hidedisplay">
																	<div class="input-field large-12 columns">
																	<input type="text" id="timeaddfielddefval" name="timeaddfielddefval">
																	<label for="timeaddfielddefval">Default Time</label>
																	</div>
																</li>
																<li id="attrdatalen" class="hidedisplay">
																	<div class="input-field large-12 columns">
																	<input type="text" id="frmelemattrlength" name="frmelemattrlength" class="validate[funcCall[lengthvalidate]]"></input>
																	<label for="frmelemattrlength">Length</label>
																	</div>
																</li>
																<li id="attrdatadecilen" class="hidedisplay">
																	<div class="input-field large-12 columns">
																	<input type="text" id="frmelemdecilength" name="frmelemdecilength"></input>
																	<label for="frmelemdecilength">Decimal</label>
																</li>
																<li id="attrrowlen" class="hidedisplay">
																	<div class="input-field large-12 columns">
																	<input type="text" id="frmelemattrrows" name="frmelemattrrows"> </input>
																	<label for="frmelemattrrows">Rows</label>
																	</div>
																</li>
																<li id="attrdataprefix" class="hidedisplay">
																	<div class="input-field large-12 columns">
																	<input type="text" id="frmelemprefix" name="frmelemprefix"></input>
																	<label for="frmelemprefix">Prefix</label>
																	</div>
																</li>
																<li id="attrdatastartnum" class="hidedisplay">
																	<div class="input-field large-12 columns">
																	<input type="text" id="frmelemstartnum" name="frmelemstartnum"></input>
																	<label for="frmelemstartnum">Start Number</label>
																	</div>
																</li>
																<li id="attrdatasuffix" class="hidedisplay">
																	<div class="input-field large-12 columns">
																	<input type="text" id="frmelemsuffix" name="frmelemsuffix"> </input>
																	<label for="frmelemsuffix">Suffix</label>
																	</div>
																</li>
																<li id="attrdatarandomopt" class="hidedisplay">
																	<div class="static-field large-12 columns">
																	<label>Number Type</label>
																	<select name="attrautonumbertype" data-placeholder="Select" class="chzn-select" id="attrautonumbertype">
																		<option></option>
																		<option value="No">Serial</option>
																		<option value="Yes">Random</option>
																	</select>
																	</div>
																</li>
																<li>
																	<div class="input-field large-12 columns">
																	<input type="text" id="frmelemattrdesc" name="frmelemattrdesc"> </input>
																	<label for="frmelemattrdesc">Description</label>
																	</div>
																</li>
															</ul>
															<ul style="padding-right: 1.2rem;">
																<li>
																	<span class="columns">
																		<input type="checkbox" class="filled-in validate[]" id="fildmandatory" name="fildmandatory" value="No"></input><label for="fildmandatory" style="margin:0.2rem 0 0 0.2rem;"> Mandatory</label>
																	</span>
																	<span class="columns">
																		<input type="checkbox" class="filled-in validate[required]" id="fieldactive" name="fieldactive" value="Yes" checked="checked" ></input><label for="fieldactive" style="margin:0.2rem 0 0 0.2rem;">Active</label>
																	</span>
																</li>
																<li>
																	<span id="featuredatespan" class="hidedisplay columns">
																		<input type="checkbox" class="filled-in filled-in validate[]" id="featuredate" name="featuredate" value="Yes" checked="checked"></input><label for="featuredate" style="margin:0.2rem 0 0 0.2rem;">Future Date</label>
																	</span>
																	<span id="pastdatespan" class="hidedisplay columns">
																		<input type="checkbox" class="filled-in validate[]" id="pastdate" name="pastdate" value="No"></input><label for="pastdate" style="margin:0.2rem 0 0 0.2rem;">Past Date</label>
																	</span>
																</li>
																<li>
																	<span  id="readonlyattr" class="columns">
																		<input type="checkbox" class="filled-in validate[]" id="fieldreadonly" name="fieldreadonly" value="No"></input><label for="fieldreadonly" style="margin:0.2rem 0 0 0.2rem;">Read Only</label>
																	</span>
																	<span id="columntypeattr"  class="columns">
																		<input type="checkbox" class="filled-in  validate[]" id="fieldcolumntype" name="fieldcolumntype" value="No"></input><label for="fieldcolumntype" style="margin:0.2rem 0 0 0.2rem;">Two Column</label>
																	</span>
																</li>
																<li>
																	<span id="mulchkopt" class="hidedisplay columns">
																		<input type="checkbox" class="filled-in validate[]" id="addfieldmultiple" name="addfieldmultiple" value="No"></input><label for="addfieldmultiple" style="margin:0.2rem 0 0 0.2rem;">Multiple</label>
																	</span>
																	<span style=""  id="attrsortopt" class="hidedisplay columns">
																		<input type="checkbox" class="filled-in validate[]" id="addalphasortorder" name="addalphasortorder" value="No"></input><label for="addalphasortorder" style="margin:0.2rem 0 0 0.2rem;">Alphabetical Sort</label>
																	</span>
																</li>
																<li>
																	<span id="attrchkboxsetdef" class="hidedisplay columns">
																		<input type="checkbox" class="filled-in validate[]" id="addfieldsetdef" name="addfieldsetdef" value="No"></input><label for="addfieldsetdef" style="margin:0.2rem 0 0 0.2rem;">Set Default</label>
																	</span>
																</li>
															</ul>
														</div>
													</ul>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
				</div>
				<!-- hidden fields -->
				<input name="elemidentiryid" type="hidden" id="elemidentiryid" value="" />
				</div>
			</div>
		</div>
	</div>
	<!--Reload Alert Overlay-->
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay alertsoverlay overlayalerts" id="reloadrestriction">	
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>		
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="alert-panel">
				<div class="alertmessagearea">
					<div class="alert-title">Confirmation</div>
					<div class="alert-message">
						Do you want Reload this page?
					</div>
				</div>
				<div class="alertbuttonarea">
					<span class="firsttab" tabindex="1000"></span>
					<input type="button" id="reloadpageyes" name="" value="Yes" tabindex="1001" class="alertbtn ffield commodyescls" >
					<input type="button" id="basedeleteno" name="" value="No" tabindex="1002" class="alertbtn flloop alertsoverlaybtn" >		
					<span class="lasttab" tabindex="1003"></span>
				</div>
			</div>
		</div>
	</div>
</body>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js File -->
	<script src="<?php echo base_url();?>js/Modulebuilder/modulebuilder.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/sortable.js" type="text/javascript"></script>
	<script>
		$(document).ready(function(){
			$('#timeaddfielddefval').datetimepicker({
				showTimepicker: true,
				timeOnly:true,
				timeFormat: 'HH:mm'
			});
		});
	</script>
</html>

