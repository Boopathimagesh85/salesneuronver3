<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboardsmodel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	//dashboard create folder
	public function createdashboardfolder() {
		if(!isset($_POST['reportfoldersetpublic'])) {
			$_POST['reportfoldersetpublic']='No';
		}
		if(!isset($_POST['reportfoldersetdefault'])) {
			$_POST['reportfoldersetdefault']='No';
		}
		if(isset($_POST['reportfoldersetpublic']) and $_POST['reportfoldersetpublic'] == '') {
			$_POST['reportfoldersetpublic']='No';
		}
		if(!isset($_POST['reportfoldersetdefault'])and $_POST['reportfoldersetdefault'] == '') {
			$_POST['reportfoldersetdefault']='No';
		}
		//insert script 
		$insertarray=array('dashboardfoldername'=>trim($_POST['reportsfoldername']),'setaspublic'=>$_POST['reportfoldersetpublic'],'setdefault'=>$_POST['reportfoldersetdefault'],'description'=>trim($_POST['reportfolderdescription']),'createdate'=>date($this->Basefunctions->datef),'lastupdatedate'=>date($this->Basefunctions->datef),'industryid'=>$this->Basefunctions->industryid,'createuserid'=>$this->Basefunctions->userid,'lastupdateuserid'=>$this->Basefunctions->userid,'status'=>$this->Basefunctions->activestatus);
		$this->db->insert('dashboardfolder',$insertarray);
		echo true;
	}
	//dashboard update folder-dashboardfolderid
	public function updatedashboardfolder() {
		$reportfolderid=$_POST['editprimarydataid'];
		if(!isset($_POST['reportfoldersetpublic'])) {
			$_POST['reportfoldersetpublic']='No';
		}
		if(!isset($_POST['reportfoldersetdefault'])) {
			$_POST['reportfoldersetdefault']='No';
		}
		if(isset($_POST['reportfoldersetpublic']) and $_POST['reportfoldersetpublic'] == '') {
			$_POST['reportfoldersetpublic']='No';
		}
		if(!isset($_POST['reportfoldersetdefault'])and $_POST['reportfoldersetdefault'] == '') {
			$_POST['reportfoldersetdefault']='No';
		}
		//insert script
		$updatearray=array('dashboardfoldername'=>trim($_POST['reportsfoldername']),'setaspublic'=>$_POST['reportfoldersetpublic'],'setdefault'=>$_POST['reportfoldersetdefault'],'description'=>trim($_POST['reportfolderdescription']),'industryid'=>$this->Basefunctions->industryid,'lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid,'status'=>$this->Basefunctions->activestatus);
		$this->db->where('dashboardfolderid',$reportfolderid);
		$this->db->update('dashboardfolder',$updatearray);
		echo true;
	}
	//delete dashboard folder
	public function deletedashboardfolder() {
		$dashboardfolderid=$_GET['dashboardfolderid'];
		$updatearray=array('lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid,'status'=>$this->Basefunctions->deletestatus);
		$this->db->where('dashboardfolderid',$dashboardfolderid);
		$this->db->update('dashboardfolder',$updatearray);
		$this->db->where('dashboardfolderid',$dashboardfolderid);
		$this->db->update('dashboard',$updatearray);
		echo true;
	}
	public function getdashboarddetailsmodel($id) {
		$this->db->select('dashboardfolder.dashboardfoldername,dashboardfolder.description,dashboardfolder.setaspublic,dashboardfolder.setdefault');
		$this->db->from('dashboardfolder');
		$this->db->where('dashboardfolder.dashboardfolderid',$id);
		$result=$this->db->get()->result();
		foreach ($result as $row){
			$getreportinfo=array('reportfoldername'=>$row->dashboardfoldername,'description'=>$row->description,'setaspublic'=>$row->setaspublic,'setdefault'=>$row->setdefault);
		}
		echo json_encode($getreportinfo);
	}
	//list dashboard menu
	public function loaddashboardfoldername() {
		$userid = $this->Basefunctions->logemployeeid;
		$jsondata=array();
		$this->db->select('dashboardfolderid,dashboardfoldername,description,setaspublic,setdefault');
		$this->db->from('dashboardfolder');	
		$this->db->where('( dashboardfolder.createuserid = '.$userid.' AND dashboardfolder.status =1) OR ( dashboardfolder.setaspublic = "Yes" AND dashboardfolder.status =1 )');
		$this->db->order_by('sortorder','asc');
		$data=$this->db->get();
		if($data->num_rows() > 0){
			foreach($data->result() as $info){
				$jsondata[]=array('result'=>true,'dashboardfoldername'=>$info->dashboardfoldername,'dashboardfolderid'=>$info->dashboardfolderid);
			}
		}		
		echo   json_encode($jsondata);
	}
	//view dashboard folder
	public function viewdashboardfolder($tablename,$sortcol,$sortord,$pagenum,$rowscount) {
		$dataset = 'dashboardfolderid,dashboardfoldername,description,setaspublic,setdefault';
		$status = 'status NOT IN (0,3)';
		/* pagination */
		$query = 'select '.$dataset.' from dashboardfolder WHERE '.$status.' ORDER BY'.' '.$sortcol.' '.$sortord;
		$page = $this->Basefunctions->generatepage($query,$pagenum,$rowscount);
		/* query */
		$data = $this->db->query('select '.$dataset.' from dashboardfolder WHERE '.$status.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$page['start'].','.$page['records'].'');
		$finalresult=array($data,$page,'');
		return $finalresult;
	}
	//view dashboard list
	public function dashboardlist($tablename,$sortcol,$sortord,$pagenum,$rowscount,$filter=array()) {
		$pagenum -= 1;
		$filtervalarray =array();
		if(!empty($filter)) {
			$fconditonid = explode('|',$filter[0]);
			$filtercondition = explode('|',$filter[1]);
			$filtervalue = explode('|',$filter[2]);
		} else {
			$fconditonid = array();
			$filtercondition = array();
			$filtervalue = array();
		}
		$filtervalarray = $this->Basefunctions->filtergenerateconditioninfo($fconditonid,$filtercondition,$filtervalue);
		if(count($filtervalarray) > 0) {
			$filterwherecondtion = $this->Basefunctions->filtergeneratewhereclause($filtervalarray['filterarray'],'');
			if($filterwherecondtion == '') {
				$filterwherecondtion = " AND 1=1";
			} else {
				$filterwherecondtion = " AND ".$filterwherecondtion;
			}
		} else {
			$filterwherecondtion = " AND 1=1";
		}
		$start = $pagenum * $rowscount;
		$dashfolder='1=1';
		if(isset($_GET['dashboardfolderid'])) {
			if($_GET['dashboardfolderid'] > 0) {		
				$dashfolder='dashboard.dashboardfolderid='.$_GET['dashboardfolderid'].'';
			}
		}
		$industryid = $this->Basefunctions->industryid;
		$userid=$this->Basefunctions->logemployeeid;
		$this->db->select('SQL_CALC_FOUND_ROWS dashboardid,dashboardid,dashboardfoldername,dashboardname,dashboard.description,modulename',false);
		$this->db->from('dashboard');
		$this->db->join('dashboardfolder','dashboardfolder.dashboardfolderid=dashboard.dashboardfolderid');
		$this->db->join('module','module.moduleid=dashboard.moduleid');
		$this->db->where('( dashboardfolder.createuserid = '.$userid.' AND dashboardfolder.status =1 AND dashboard.status =1 AND dashboard.industryid in ('.$industryid.') AND '.$dashfolder.') OR ( dashboardfolder.setaspublic = "Yes" AND dashboardfolder.status =1 AND dashboard.status =1 AND dashboard.industryid  in ('.$industryid.') AND '.$dashfolder.')');
		$this->db->order_by($sortcol,$sortord);
        $this->db->limit($rowscount,$start);
		return $this->db->get();
	}
	//dashboarddelete
	public function dashboarddelete() {
		$d_fieldids=array();
		$dashboardid=trim($_GET['primarydataid']);
		$deletearray=array('lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid,'status'=>$this->Basefunctions->deletestatus);
		$this->db->where('dashboardid',$dashboardid);
		$this->db->update('dashboard',$deletearray);
		//dashboardrow
		$this->db->where('dashboardid',$dashboardid);
		$this->db->update('dashboardrow',$deletearray);
		//dashboard rowselects
		$mmr=$this->db->select('dashboardrowid')->from('dashboardrow')->where('dashboardid',$dashboardid)->get()->result();
		foreach($mmr as $info) {
			$d_fieldids[]=$info->dashboardrowid;
		}
		//dashboard field
		$this->db->where_in('dashboardrowid',$d_fieldids);
		$this->db->update('dashboardrow',$deletearray);
		echo true;
	}
	//folder data fetch for drop down
	public function folderloadbydropdown() {
		$data = array();
		$i=0;
		$this->db->select('dashboardfoldername,dashboardfolderid,setdefault');
		$this->db->from('dashboardfolder');
		$this->db->where('dashboardfolder.status',1);
		$result = $this->db->get();
		return $result->result();
	}
}