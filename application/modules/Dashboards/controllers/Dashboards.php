<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboards extends MX_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->helper('formbuild');
        $this->load->view('Base/formfieldgeneration');
		$this->load->model('Dashboards/Dashboardsmodel');
		$this->load->model('Base/Basefunctions');
    }
    //first basic hitting view
    public function index() {
		$moduleid = array(268);
		sessionchecker($moduleid);
		//action
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(268);
		$viewmoduleid = array(268);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['folderdata']=$this->Dashboardsmodel->folderloadbydropdown();
		$this->load->view('Dashboards/dashboardsview',$data); 
    }
	//dashboard create folder
	public function createdashboardfolder()
	{
		$this->Dashboardsmodel->createdashboardfolder();
	}
	//dashboard update folder
	public function updatedashboardfolder()
	{
		$this->Dashboardsmodel->updatedashboardfolder();
	}
	//dashboard delete folder
	public function deletedashboardfolder()
	{
		$this->Dashboardsmodel->deletedashboardfolder();
	}
	//dashboard folder menu list
	public function loaddashboardfoldername(){
		$this->Dashboardsmodel->loaddashboardfoldername();
	}
	//dashboard delete
	public function dashboarddelete()
	{
		$this->Dashboardsmodel->dashboarddelete();
	}
	public function getdashboarddetails(){
		$value = $this->Dashboardsmodel->getdashboarddetailsmodel($_GET['reportid']);
	}
	//dashboard view folder
	public function viewdashboardfolder()
	{
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'dashboardfolder.dashboardfolderid') : 'dashboardfolder.dashboardfolderid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'ASC') : 'ASC';
		$colinfo = array('colmodelname'=>array('dashboardfoldername','description','setaspublic','setdefault'),'colmodelindex'=>array('dashboardfoldername','description','setaspublic','setdefault'),'coltablename'=>array('dashboardfolder','dashboardfolder','dashboardfolder','dashboardfolder'),'uitype'=>array('2','2','2','2'),'colname'=>array('Folder Name','Description','Public','Default'),'colsize'=>array('200','200','200','200'));
		$result=$this->Dashboardsmodel->viewdashboardfolder($primarytable,$sortcol,$sortord,$pagenum,$rowscount);
		$device = $this->Basefunctions->deviceinfo();
	//	if($device=='phone') {
		//	$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Report List',$width,$height);
		//} else {
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Report List',$width,$height);
		//}
		echo json_encode($datas);
	}
	//dashboard main list
	//view report list
	public function viewdashboardlist()
	{
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'dashboard.dashboardid') : 'dashboard.dashboardid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('dashboardfoldername','dashboardname','modulename','description'),'colmodelindex'=>array('dashboardfolder.dashboardfoldername','dashboard.dashboardname','module.modulename','dashboard.description'),'coltablename'=>array('dashboardfolder','dashboard','module','dashboard'),'uitype'=>array('2','2','2','2'),'colname'=>array('Dashboard Folder','Dashboard Name','Module Name','Description'),'colsize'=>array('200','200','200','300'));
		$filterid = isset($_GET['filter']) ? $_GET['filter'] : '';
		$conditionname = isset($_GET['conditionname']) ? $_GET['conditionname'] : '';
		$filtervalue = isset($_GET['filtervalue']) ? $_GET['filtervalue'] : '';
		$filter = array('0'=>$filterid,'1'=>$conditionname,'2'=>$filtervalue);
		$result=$this->Dashboardsmodel->dashboardlist($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$filter);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($result,$page,'');
		$device = $this->Basefunctions->deviceinfo();
	//	if($device=='phone') {
		//	$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Dashboard View',$width,$height);
		//} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Dashboard View',$width,$height);
		//}
		echo json_encode($datas);
	}
}