<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('Base/headerfiles'); ?>
</head>
<body>
	<div class="row" style="max-width:100%">
		<div class="off-canvas-wrap" data-offcanvas>
			<div class="inner-wrap">
				<div class="gridviewdivforsh" id="reportsview">
					<?php
						$device = $this->Basefunctions->deviceinfo();
						$dataset['gridtitle'] = $gridtitle['title'];
						$dataset['titleicon'] = $gridtitle['titleicon'];
						$dataset['formtype'] = 'reportdashbaord';
						$dataset['gridid'] = 'dasboardsgrid';
						$dataset['gridwidth'] = 'dasboardsgridwidth';
						$dataset['gridfooter'] = 'dasboardsgridfooter';
						$this->load->view('Base/mainviewheader',$dataset);
						$dataset['moduleid'] = '268';
						$this->load->view('Base/singlemainviewformwithgrid',$dataset);
					?>
				</div>
				</div>
				</div>
				<div class="hidedisplay" id="folderoverlaydiv">
					<?php $this->load->view('folderform'); ?>
				</div>
				<!-- Add Form -->
				<div id="reportsformdiv" class="">
				</div>
				<?php
					$this->load->view('Base/basedeleteform');
					$this->load->view('dashboarddeleteform');
					if($device=='phone') {
						$this->load->view('Base/overlaymobile');
					} else {
						$this->load->view('Base/overlay');
					}
					$this->load->view('Base/modulelist');
				?>							
			</div>
		</div>
	</div>
</body>
<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js Files -->	
	<script src="<?php echo base_url();?>js/Dashboards/dashboards.js" type="text/javascript"></script> 
</html>
