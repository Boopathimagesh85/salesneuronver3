<?php
	$device = $this->Basefunctions->deviceinfo();
	$dataset['gridtitle'] = 'Folder';
	$dataset['titleicon'] = 'folder';
	$modtabgroup[0] = array("tabpgrpid"=>"1","tabgrpname"=>"Folder","moduleid"=>"268","templateid"=>"2");
	$dataset['modtabgrp'] = $modtabgroup;
	$dataset['moduleid'] = '268';
	$dataset['action'][0] = array('actionid'=>'groupcloseaddform','actiontitle'=>'Close','actionmore'=>'No','ulname'=>'folder');
	$this->load->view('Base/formwithgridmenuheader.php',$dataset);
?>	
<div class="large-12 columns paddingzero">
	<div class="" id="folderoverlaydivid" >	
		<div class="large-12 columns addformunderheader">&nbsp;</div>
		<div class="large-12 columns scrollbarclass addformcontainer">
		<form id="reportfolderform" name="reportfolderform" class="cleardataform" style="background: #f2f3fa !important;">
		<div id="addreportfoldervalidate" class="validationEngineContainer">
		<div id="editreportfoldervalidate" class="validationEngineContainer">
		<div class="large-12 medium-12 small-12 large-centered medium-centered small-centered columns cleardataform paddingzero">
			<div class="large-12 column paddingzero">
				<?php if($device=='phone') {?>
				<div class="closed effectbox-overlay effectbox-default" id="groupsectionoverlay">
				<?php }?>
				<div class="row mblhidedisplay">&nbsp;</div>
				<div class="large-4 columns end paddingbtm">
					<div class="large-12 columns cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;">
						<?php if($device=='phone') {?>
						<div class="large-12 columns sectionheaderformcaptionstyle">Folder Details</div>
						<div class="large-12 columns sectionpanel">
							<?php  } else { ?>
								<div class="large-12 columns headerformcaptionstyle">Folder Details </div>
						<?php  	} ?> 
						<div class="input-field large-12 columns">
							<input type="text" id="reportsfoldername" name="reportsfoldername" value="" class="validate[required]"/>
							<label for="reportsfoldername">Folder Name<span class="mandatoryfildclass">*</span></label>
						</div>
						<div class="input-field large-12 columns">
							<textarea id="reportfolderdescription" class="materialize-textarea" name="reportfolderdescription" value=""> </textarea>
							<label for="reportfolderdescription">Description</label>
						</div>
						<div class="large-6 columns">
							<input type="checkbox" data-hidname='reportfoldersetpublic' id="reportfoldersetpublic" name="reportfoldersetpublic" class="checkboxcls" value=""/>
							<label for="reportfoldersetpublic">Set As Public</label>
							<input type="hidden" id="editprimarydataid" name="editprimarydataid"/>
						</div>
						<div class="large-6 columns">
							<input type="checkbox" id="reportfoldersetdefault" name="reportfoldersetdefault"  data-hidname='reportfoldersetdefault' class="checkboxcls" value=""/>
							<label for="reportfoldersetdefault">Default</label>
						</div>
						<?php if($device=='phone') { ?>
						</div>
						<div class="divider large-12 columns"></div>
						<div class="large-12 columns submitkeyboard sectionalertbuttonarea" style="text-align: right">
								<input id="addreportfolderbtn" class="alertbtn addkeyboard" type="button" value="Submit" name="addreportfolderbtn" tabindex="103">
								<input id="editreportfolderbtn" class="alertbtn hidedisplay updatekeyboard" type="button" value="Submit" name="editreportfolderbtn" tabindex="103" style="display: none;">
								<input id="addsectionclose"class="alertbtn addsectionclose cancelkeyboard" type="button" value="Cancel" name="cancel" tabindex="104">
								
							</div>
								
						<?php } else { ?> 
						<div class="large-12 columns" style="text-align: right">
							<label> </label>
							<input id="addreportfolderbtn" class="btn" type="button" value="Submit" name="" tabindex="103">
							<input id="editreportfolderbtn" class="btn" type="button" value="Submit" name="" tabindex="104" >
						</div>
						<?php } ?>
					</div>
				</div>
				<?php if($device=='phone') {?>
				</div>
				<?php }?> 
				<div class="large-8 columns paddingbtm">
					<div class="large-12 columns paddingzero">
						<div class="large-12 columns viewgridcolorstyle paddingzero">
							<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0 0.4rem;">
								<span class="large-6 medium-6 small-6 columns lefttext">Folder List</span>
								<span class="large-6 medium-6 small-6 columns innergridicon righttext" style="text-align:right">
										<?php if($device=='phone') {?>
										  <span id="folderaddicon" class="icon-box" title="Add" ><i class="material-icons">add</i></span>
										  <?php }?> 
										<span  title="Edit" id="reportfolderediticon"><i class="material-icons">edit</i> </span>
										<span  title="Refresh" id="dashboardfolderrefreshicon"><i class="material-icons">refresh</i>  </span>
										<span  title="Delete" id="dashboardfolderdeleteicon"><i class="material-icons">delete</i>  </span>
								</span>
							</div>
							<?php
							$device = $this->Basefunctions->deviceinfo();
							if($device=='phone') {
								echo '<div class="large-12 columns paddingzero forgetinggridname" id="folderoperationgridwidth"><div id="folderoperationgrid" style="max-width:2000px; height:420px;top:0px;">
									<!-- Table header content , data rows & pagination for mobile-->
								</div>
								<footer class="inner-gridfooter footercontainer" id="folderoperationgridfooter">
									<!-- Footer & Pagination content -->
								</footer></div>';
							} else {
								echo '<div class="large-12 columns forgetinggridname" id="folderoperationgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="folderoperationgrid" style="max-width:2000px; height:420px;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
								<div class="inner-gridfooter footer-content footercontainer" id="folderoperationgridfooter">
									<!-- Footer & Pagination content -->
								</div></div>';
							}
						?>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
		</div>
		</form>
		</div>
	</div>
</div>
