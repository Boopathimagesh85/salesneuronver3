<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Backup extends MX_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model('Backup/Backupmodel');
		$this->load->helper('formbuild');
		$this->load->model('Base/Basefunctions');
    }
    public function index() {
		$moduleid = array(46);
		sessionchecker($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['bpsetting']=$this->Backupmodel->backupsettingslistmodel();
		$data['schedulermethod'] = $this->Basefunctions->simpledropdown('schedulermethod','schedulermethodid,schedulermethodname','schedulermethodname');
		$data['schedulertype'] = $this->Basefunctions->simpledropdown('schedulertype','schedulertypeid,schedulertypename','schedulertypename');
		$this->load->view('Backup/backupview',$data);
	}
	//create / update backup settings
	public function addbackupsettings() {
		$this->Backupmodel->addbackupsettingsdetail();
	}
	//create / update backup location
	public function addbackuplocation() {
		$this->Backupmodel->addbackuplocationmodel();
	}
	//list drive folder name
	public function listdfname() {
		$this->Backupmodel->listdfnamemodel();
	}
	public function gridinformationfetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$chkbox = $_GET['checkbox'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'backup.backupid') : 'backup.backupid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
	
		$colinfo = array('colmodelname'=>array('Name','Date','Time','Size','employeename'),'colmodelindex'=>array('backup.Name','backup.Date','backup.Time','backup.Size','employee.employeename'),'coltablename'=>array('backup','backup','backup','backup','employee'),'uitype'=>array('2','2','2','2','2'),'colname'=>array('Name','Date','Time','Size','EmployeeName'),'colsize'=>array('140','70','50','50','70'));
	
		$result=$this->Backupmodel->viewdynamicdatainfofetch($primarytable,$sortcol,$sortord,$pagenum,$rowscount);
		$device = $this->Basefunctions->deviceinfo();
		//if($device=='phone') {
			//$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Backup',$width,$height,$chkbox);
		//} else {
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Backup',$width,$height,$chkbox);
		//}
		echo json_encode($datas);
	}
	//Retrieve scheduler information
	public function schedulerinformation() {
		$this->Backupmodel->schedulerinformationmodel();
	}
	//scheduler information update
	public function schedulerinformationupdate() {
		$this->Backupmodel->schedulerinformationupdatemodel();
	}
}