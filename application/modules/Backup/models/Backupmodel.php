<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Backupmodel extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
	//fetch backup settings info
	public function backupsettingslistmodel() {
		$setdata=$this->db->select('backupsettingid')->from('backupsetting')->where('status',1)->get();
		$datacount = $setdata->num_rows();
		if($datacount >= 1) {
			foreach($setdata->result() as $data) {
				$datas = $this->db->select('backuplocation,mailenable,emailid,backupsettingid,schedulerenable')->from('backupsetting')->where('backupsettingid',$data->backupsettingid)->get();
				foreach($datas->result() as $settings) {
					$loc = explode(':\\',$settings->backuplocation);
					$datasets=array('location'=>$loc[0],'folder'=>$settings->backuplocation,'emailenable'=>$settings->mailenable,'emailid'=>$settings->emailid,'settingsid'=>$settings->backupsettingid,'schedulerenable'=>$settings->schedulerenable);
				}
			}
		} else {
			$datasets=array('location'=>'','folder'=>'','emailenable'=>'No','emailid'=>'','settingsid'=>'1','schedulerenable'=>'No');
		}
		return $datasets;
	}
	//create/update backup settings
	public function addbackupsettingsdetail() {
		$cdate=date('Y-m-d H:i:s');
		$mailenable = $_POST['emailopt'];
		$emailid = $_POST['emailid'];
		$userid = $this->Basefunctions->userid;
		//check settings
		$setdata=$this->db->select('backupsettingid')->from('backupsetting')->where('status',1)->get();
		$datacount = $setdata->num_rows();
		if($datacount < 1) {
			$data=array(
				'backuplocation'=>'',
				'cronenable'=>'No',
				'crontype'=>'1',
				'crontime'=>'',
				'cronservicename'=>'',
				'mailenable'=>$mailenable,
				'emailid'=>$emailid,
				'ftpenable'=>'No',
				'ftpservername'=>'',
				'ftpusername'=>'',
				'ftppassword'=>'',
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
				'createdate'=>$cdate,
				'lastupdatedate'=>$cdate,
				'status'=>1
			);
			$this->db->insert('backupsetting',$data);
		} else {
			$bcid=$this->Basefunctions->maximumid('backupsetting','backupsettingid');
			$updatedata=array(
				'cronenable'=>'No',
				'crontype'=>'1',
				'crontime'=>'',
				'cronservicename'=>'',
				'mailenable'=>$mailenable,
				'emailid'=>$emailid,
				'ftpenable'=>'No',
				'ftpservername'=>'',
				'ftpusername'=>'',
				'ftppassword'=>'',
				'lastupdateuserid'=>$userid,
				'lastupdatedate'=>$cdate,
				'status'=>1
			);
			$this->db->where('backupsettingid',$bcid);
			$this->db->update('backupsetting',$updatedata);
		}
		echo 'True';
	}
	//backup location creation/updation
	public function addbackuplocationmodel() {
		$cdate=date('Y-m-d H:i:s');
		$loc = $_POST['locfoldername'];
		$userid = $this->Basefunctions->userid;
		//check settings
		$setdata=$this->db->select('backupsettingid')->from('backupsetting')->where('status',1)->get();
		$datacount = $setdata->num_rows();
		if($datacount < 1) {
			$data=array(
				'backuplocation'=>$loc,
				'cronenable'=>'No',
				'crontype'=>'1',
				'crontime'=>'',
				'cronservicename'=>'',
				'mailenable'=>'No',
				'emailid'=>'',
				'ftpenable'=>'No',
				'ftpservername'=>'',
				'ftpusername'=>'',
				'ftppassword'=>'',
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
				'createdate'=>$cdate,
				'lastupdatedate'=>$cdate,
				'status'=>1
			);
			$this->db->insert('backupsetting',$data);
		} else {
			$bcid=$this->Basefunctions->maximumid('backupsetting','backupsettingid');
			$updatedata=array(
				'backuplocation'=>$loc,
				'lastupdateuserid'=>$userid,
				'lastupdatedate'=>$cdate,
				'status'=>1
			);
			$this->db->where('backupsettingid',$bcid);
			$this->db->update('backupsetting',$updatedata);
		}
		echo 'True';
	}
	//list all folder name in drive
	public function listdfnamemodel() {
		$drivename = $_GET['dname'];
		$opt='<option value=""></option>';
		$dirs = glob($drivename.':\*', GLOB_ONLYDIR);
		foreach($dirs as $val) {
			$opt.='<option value="'.$val.'">'.$val.'</option>';
		}
		echo $opt;
	}
	public function viewdynamicdatainfofetch($tablename,$sortcol,$sortord,$pagenum,$rowscount) {
	
		$dataset ='backup.backupid,backup.Name,backup.Date,backup.Time,backup.Size,employee.employeename';
		$join='LEFT OUTER JOIN employee ON employee.employeeid=backup.createuserid';
		$cuscondition='';
		$status='backup.status='.$this->Basefunctions->activestatus.'';
	
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		/* query */
		$result = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$cuscondition.' '. $status.' ORDER BY '.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$data=array();
		$i=0;
		foreach($result->result() as $row) {
			$backupid = $row->backupid;
			$backupname = $row->Name;
			$backupdate = $row->Date;
			$backuptime = $row->Time;
			$backupsize = $row->Size;
			$employeename = $row->employeename;
			$data[$i]=array('id'=>$backupid,$backupname,$backupdate,$backuptime,$backupsize,$employeename);
			$i++;
		}
		$finalresult=array('0'=>$data,'1'=>$page,'2'=>'json');
		return $finalresult;
	
	}
	//Retrieve scheduler information
	public function schedulerinformationmodel() {
		$schmethodid = $_GET['schmethodid'];
		$scheduleroption =  $this->Basefunctions->singlefieldfetch('schedulerenable','backupsettingid','backupsetting',2); //amount round-off
		$this->db->select('scheduler.schedulerid,scheduler.schedulername,schedulermethod.schedulermethodname,scheduler.schedulertypeid,schedulertype.schedulertypename,scheduler.intervaltime');
		$this->db->from('scheduler');
		$this->db->join('schedulermethod','schedulermethod.schedulermethodid=scheduler.schedulermethodid');
		$this->db->join('schedulertype','schedulertype.schedulertypeid=scheduler.schedulertypeid');
		$this->db->where('scheduler.status',$this->Basefunctions->activestatus);
		$this->db->where('scheduler.schedulermethodid',$schmethodid);
		$this->db->limit(1);
		$info = $this->db->get();
		if($info->num_rows() > 0) {
			foreach($info->result() as $row) {
				$acc_array = array(
									'schedulerid' => $row->schedulerid,
									'schedulername' => $row->schedulername,
									'schedulermethodname' => $row->schedulermethodname,
									'schedulertypename' => $row->schedulertypename,
									'schedulertypeid' => $row->schedulertypeid,
									'intervaltime' => $row->intervaltime,
									'scheduleroption' => $scheduleroption,
										);
			}
		} else {
			$acc_array = array(
								'schedulerid' => '',
								'schedulername' => '',
								'schedulermethodname' => '',
								'schedulertypename' => '',
								'schedulertypeid' => '',
								'intervaltime' => '',
								'scheduleroption' => $scheduleroption,
									);
		}
		echo json_encode($acc_array);
	}
	//Scheduler information insert / update
	public function schedulerinformationupdatemodel() {
		$userid = $this->Basefunctions->userid;
		$cdate=date('Y-m-d H:i:s');
		$schedulerid = $_POST['hiddenschedulerid'];
		$schedulerenable = $_POST['schedulerenable'];
		$bcid=$this->Basefunctions->maximumid('backupsetting','backupsettingid');
		$updatedata=array(
			'schedulerenable'=>$schedulerenable,
			'lastupdateuserid'=>$userid,
			'lastupdatedate'=>$cdate,
			'status'=>1
		);
		$this->db->where('backupsettingid',$bcid);
		$this->db->update('backupsetting',$updatedata);
		
		$data=array(
			'schedulername'=>$_POST['schedulename'],
			'schedulermethodid'=>$_POST['schedulermethodid'],
			'schedulertypeid'=>$_POST['schedulertypeid'],
			'intervaltime'=>$_POST['intervaltime'],
			'createuserid'=>$userid,
			'lastupdateuserid'=>$userid,
			'createdate'=>$cdate,
			'lastupdatedate'=>$cdate,
			'status'=>1
		);
		if($schedulerid == '' ||$schedulerid == '1' ) {
			$this->db->insert('scheduler',$data);
		} else {
			$this->db->where('schedulerid',$schedulerid);
			$this->db->update('scheduler',$data);
		}
		echo 'True';
	}
}