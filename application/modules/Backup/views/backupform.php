<style type="text/css">
	#backupadvanceddrop{
		left: -54.1406px !important;
	}
		.gridcontent {
	height: 77vh !important;
	}
	#backupgridfooter {
	    position: relative;
    top: 331px;
	}
	.rppdropdown {
		left:50% !important;
	}
</style>
<?php
	$device = $this->Basefunctions->deviceinfo();
	$dataset['gridtitle'] = $gridtitle['title'];
	$dataset['titleicon'] = $gridtitle['titleicon'];
	$dataset['moduleid'] = '46';
	$dataset['action'][0] = array('actionid'=>'backuplocation','actiontitle'=>'Location','actionmore'=>'Yes','ulname'=>'backup');
	$this->load->view('Base/formwithgridmenuheader.php',$dataset);
?>
<div class="large-12 columns addformunderheader">&nbsp; </div>
<div class="large-12 columns addformcontainer" style="overflow: hidden;">
	<!-- More Action DD start -- Gowtham -->
		<ul id="backupadvanceddrop" class="multinavaction-drop arrow_box" style="white-space: nowrap; position: absolute; top: 34.8px; left:-55.1406px !important; opacity: 1; display: none;">
			<li id="backupcreate"><span class="icon-box"><i class="material-icons addiconclass " title="Backup">backup</i>Backup</span></li>
			<li id="backupdownload"><span class="icon-box"><i class="material-icons downloadiconclass " title="Download">file_download</i>Download</span></li>
			<li id="backupdelete"><span class="icon-box"><i class="material-icons" title="Delete">delete</i>Delete</span></li>
			<li id="backupsettingsmailoverlay"><span class="icon-box"><i class="material-icons" title="E-Mail">mail_outline</i>Email Settings</span></li>
			<li id="sendemailbackup"><span class="icon-box"><i class="material-icons" title="E-Mail">file_download</i>Email Backup</span></li>
			<li id="decryptionscript"><span class="icon-box"><i class="material-icons" title="E-Mail">file_download</i>Decryption</span></li>
			<li id="schedulesettings"><span class="icon-box"><i class="material-icons" title="Settings">settings</i>Schedule</span></li>
		</ul>
	<!-- More Action DD End -- Gowtham -->
	<div class="row mblhidedisplay">&nbsp;</div>
	<div id="subformspan1" class="hiddensubform" style="height:100% !important;"> 
		<?php
			$device = $this->Basefunctions->deviceinfo();
			if($device=='phone') {
				echo '<div class="large-12 columns paddingbtm" >	
						<div class="large-12 columns viewgridcolorstyle" style="padding-left:0;padding-right:0;">	
							<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0rem 0rem; height:auto; line-height:1.8rem;">	
								<span class="large-6  medium-6 small-5 columns" style="text-align:left">Backup List</span>
								<span class="large-6 medium-6 small-7 columns innergridicons" style="text-align:right">
									<span class="locationiconclass icon-box" title="Location" id="backuplocation"><i class="material-icons">folder_open</i></span>
									<span class="addiconclass icon-box" title="Backup" id="backupcreate"><i class="material-icons">restore</i></span>
									<span class="downloadiconclass icon-box" title="Download" id="backupdownload"><i class="material-icons">cloud_download</i></span>
									<span class="deleteiconclass icon-box" title="Delete" id="backupdelete"><i class="material-icons">delete</i></span>
									<span class="icon-box" title="Save" id="backupsettingsmailoverlay"><i class="material-icons">mail_outline</i> </span>
								</span>
							</div>';
				echo '<div class="large-12 columns paddingzero forgetinggridname" id="backupgridwidth"><div class=" inner-row-content inner-gridcontent" id="backupgrid" style="height:420px;top:0px;">
					<!-- Table header content , data rows & pagination for mobile-->
				</div>
				<footer class="inner-gridfooter footercontainer" id="backupgridfooter">
					<!-- Footer & Pagination content -->
				</footer></div></div></div>';
			} else {
				echo '<div class="large-12 columns paddingbtm" style="padding-right: 0.6em;height:100% !important;top:-10px !important;">	
						<div class="large-12 columns viewgridcolorstyle borderstyle" style="padding-left:0;padding-right:0;height:100% !important;">	
							<!--<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0rem 0rem; height:auto; line-height:1.8rem;">	
								<span class="large-6  medium-6 small-12 columns small-only-text-center" style="text-align:left">Backup List<span class="fwgpaging-box backupgridfootercontainer"></span></span>
								<span class="large-6 medium-6 small-12 columns innergridicons small-only-text-center" style="text-align:right">
									<span class="locationiconclass icon-box" title="Location" id="backuplocation"><i class="material-icons">folder_open</i></span>
									<span class="addiconclass icon-box" title="Backup" id="backupcreate"><i class="material-icons">restore</i></span>
									<span class="downloadiconclass icon-box" title="Download" id="backupdownload"><i class="material-icons">cloud_download</i></span>
									<span class="deleteiconclass icon-box" title="Delete" id="backupdelete"><i class="material-icons">delete</i></span>
									<span class="icon-box" title="Save" id="backupsettingsmailoverlay"><i class="material-icons">mail_outline</i> </span>
								</span>
							</div>-->';
				echo '<div class="large-12 columns forgetinggridname" id="backupgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="backupgrid" style="max-width:2000px; height:450px;top:0px;">
				<!-- Table content[Header & Record Rows] for desktop-->
				</div>
				<div class="inner-gridfooter footer-content footercontainer" id="backupgridfooter">
					<!-- Footer & Pagination content -->
				</div></div></div></div>';
			}
		?>				
	</div>
	<div id="subformspan2" class="hiddensubform hidedisplay"> 
		<form id="backupsettingsform" name="backupsettingsform" class="" >
			<div name="backupsettingswizard" id="backupsettingswizard" class="validationEngineContainer">
				<!--<div class="large-4 columns paddingbtm">
					<div class="large-12 columns" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff">
						<div class="large-12 columns headerformcaptionstyle">Auto Backup Settings</div>
						<div class="large-12 columns">
						<label>Auto Backup</label>
						<input type="checkbox" id="" name="" class="checkboxcls radiochecksize " data-hidname="taxable" tabindex="101" value="">
						</div>
						<div class="large-12 columns">
							<label>Backup Timing</label>						
							<select id="" name="" class="chzn-select validate[required]" tabindex="102">
							<option value=""></option>
							</select>
						</div>
						<div class="large-12 columns">
							<label>Hours / Minutes</label>						
							<input type="text" class="" id="" name="" value="" tabindex="103">
						</div>
						<div class="large-12 columns"> &nbsp; </div>
					</div>
				</div>-->
				<!--<div class="large-4 columns end paddingbtm">	
					<div class="large-12 columns cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;">
						<div class="large-12 columns headerformcaptionstyle">Mail Settings</div>
						<div class="input-field large-12 columns">
							<input type="checkbox" id="emailoption" name="emailoption" class="filled-in checkboxcls radiochecksize " data-hidname="emailopt" tabindex="104" value="" <?php //echo (($bpsetting['emailenable']=='Yes')?'checked="checked"':''); ?> />
							<label for="emailoption">Mail</label>
							<input type="hidden" name="emailopt" id="emailopt" value="<?php  //echo $bpsetting['emailenable']; ?>" />
						</div>
						<div class="input-field large-12 columns">
							<input type="text" class="" id="emailid" name="emailid" value="<?php //echo $bpsetting['emailid']; ?>" tabindex="105">
							<label for="emailid">Email ID</label>
						</div>
						<div class="large-12 columns"> &nbsp; </div>
					</div>
				</div>
				<!--<div class="large-4 columns paddingbtm">	
					<div class="large-12 columns" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff">
						<div class="large-12 columns headerformcaptionstyle">FTP Settings</div>
						<div class="large-12 columns">
							<label>FTP</label>
							<input type="checkbox" id="" name="" class="checkboxcls radiochecksize " data-hidname="taxable" tabindex="106" value="">
						</div>
						<div class="large-12 columns">
							<label>FTP Server</label>						
							<input type="text" class="" id="" name="" value="" tabindex="107">
						</div>
						<div class="large-12 columns">
							<label>FTP User Name</label>						
							<input type="text" class="" id="" name="" value="" tabindex="108">
						</div>
						<div class="large-12 columns">
							<label>FTP Password</label>						
							<input type="text" class="" id="" name="" value="" tabindex="109">
						</div>
						<div class="large-12 columns"> &nbsp; </div>
					</div>
				</div>-->
			</div>
		</form>
		<!--hidden fields-->
		<input type="hidden" name="bkupsettingid" id="bkupsettingid" value="<?php echo $bpsetting['settingsid']; ?>" />
		<input type="hidden" name="updateuser" id="updateuser" value="<?php echo $this->Basefunctions->userid; ?>" />
		<form id="databasebackupform" style="display:none;" name="databasebackupform" method="post">
			<input type="hidden" name="tables" id="tables" value="on"/>
			<input type="hidden" name="data" id="data" value="on"/>
			<input type="hidden" name="drop" id="drop" value="on"/>
			<input type="hidden" name="zip" id="zip" value="gzip"/>
			<input type="hidden" name="comments" id="comments" value="Data base backup"/>
			<input type="hidden" name="backupuser" id="backupuser" value="<?php echo $this->Basefunctions->userid;?>"/>
			<input type="hidden" name="backupstatus" id="backupstatus" value="<?php echo $this->Basefunctions->activestatus;?>"/>
			<input type="submit" name="backupsub" id="backupsub"/>
		</form>
	</div>
</div>

<!--Backup Overlay overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="backuplocationovrelay" style="overflow-y: scroll;overflow-x: hidden;">
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="large-6 medium-6 large-centered medium-centered columns" >
			<form method="POST" name="locationsetform" style="" id="locationsetform" action="" enctype="" class="">
				<span id="locationvalidationwizard" class="validationEngineContainer"> 
					<div class="alert-panel" style="background: #617d8a;">
						<div class="alertmessagearea">
							<div class="alert-title">Backup Location</div>
							<div class="alert-message">
								<span class="firsttab" tabindex="1000"></span>
								<div class="static-field overlayfield">
									<label>Drive Name<span class="mandatoryfildclass">*</span></label>
									<select class="chzn-select validate[required] ffieldd" id="drivename" name="drivename" data-prompt-position="topLeft:14,36" tabindex="1001" data-placeholder="Select">
										<option value=""></option>
										<?php
											$fso = new COM('Scripting.FileSystemObject');
											foreach ($fso->Drives as $drive) {
										?>
												<option value="<?php echo $drive->DriveLetter;?>"><?php echo  $drive->DriveLetter;?></option>
										<?php
											}
										?>
									</select>
								</div>
								<div class="static-field overlayfield">
									<label>Folder Name<span class="mandatoryfildclass">*</span></label>
									<select class="chzn-select validate[required]" id="locfoldername" name="locfoldername" data-prompt-position="topLeft:14,36" tabindex="1002" data-placeholder="Select">
										<option value=""></option>
									</select>
								</div>
							</div>
						</div>
						<div class="alertbuttonarea">
							<input id="locationsubmit" class="alertbtnyes" type="button" value="Submit" name="locationsubmit" tabindex="1003">
							<input type="button" id="backupoverlayclose" name="" value="Cancel" tabindex="1004" class="alertbtnno flloop  alertsoverlaybtn cancelkeyboard" >
							<span class="lasttab" tabindex="1005"></span>
							<!-- hidden fields -->
							<input type="hidden" name="backuploc" id="backuploc" value="<?php echo $bpsetting['folder'];?>" />
							<input type="hidden" name="backupdrive" id="backupdrive" value="<?php echo $bpsetting['location'];?>" />
						</div>
					</div>
				</span>
			</form>
		</div>
	</div>
</div>
 <!--- process overlay-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="processoverlay" style="overflow-y: hidden;overflow-x: hidden">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div><div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-2 columns end large-centered">
			<div class="large-12 columns alertsheadercaptionstyle" style="text-align: left; background: #546E7A;">
				<div class="small-12 large-12 medium-12 columns ">
					<span><div class="spinner"></div></span> &nbsp; Processing...
				</div>
			</div>
		</div>
	</div>
</div>	
<!--Backup Mail Setting Extension form elements---->
	<div class="large-6 columns" style="position:absolute;">
		<div class="overlay" id="salescommentoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40;">	
			<div class="row" style="padding-top:100px;">&nbsp;</div>
			<div class="alert-panel" style="background: #617d8a;">
				<div class="alertmessagearea">
					<div class="alert-title">Confirmation</div>
					<div class="alert-message">
						<span class="firsttab" tabindex="1000"></span>
						<div class="input-field overlayfield">
							<input type="checkbox" id="emailoption" name="emailoption" class="filled-in checkboxcls radiochecksize ffield" data-hidname="emailopt" tabindex="1001" value="" <?php echo (($bpsetting['emailenable']=='Yes')?'checked="checked"':''); ?> />
							<label for="emailoption">Mail</label>
							<input type="hidden" name="emailopt" id="emailopt" value="<?php echo $bpsetting['emailenable']; ?>" />
						</div>
						<div class="input-field overlayfield">
							<input type="text" class="" id="emailid" name="emailid" value="<?php echo $bpsetting['emailid']; ?>" tabindex="1002">
							<label for="emailid">Email ID</label>
							<input type="hidden" name="hiddenemailid" id="hiddenemailid" value="<?php  echo $bpsetting['emailid']; ?>" />
						</div>
					</div>
				</div>
				<div class="alertbuttonarea">
					<input type="button" id="backupsettingsupdate" name="" value="Submit" tabindex="1003" class="alertbtn flloop  " >
					<input type="button" id="backupsettingsclose" name="" value="Cancel" tabindex="1004" class="alertbtn flloop  cancelkeyboard" >
					<span class="lasttab" tabindex="1005"></span>
				</div>
			</div>
		</div>
	</div>
<!--Schedule Setting Extension form elements---->
	<div class="large-6 columns" style="position:absolute;">
		<div class="overlay" id="schedulesettingsoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40;">	
			<div class="row" style="padding-top:100px;">&nbsp;</div>
			<form method="POST" name="schedulerform" style="" id="schedulerform" action="" enctype="" class="">
				<span id="schedulervalidationwizard" class="validationEngineContainer"> 
					<div class="alert-panel" style="background: #617d8a;">
						<div class="alertmessagearea">
							<div class="alert-title">Schedule Settings Overlay</div>
							<div class="alert-message">
								<div class="input-field overlayfield">
									<input type="checkbox" id="schedulerenable" name="schedulerenable" class="filled-in checkboxcls radiochecksize ffield" data-hidname="schedulerenable" tabindex="1001" value="" <?php echo (($bpsetting['schedulerenable']=='Yes')?'checked="checked"':''); ?> />
									<label for="schedulerenable">Scheduler Enable</label>
								</div>
								<div class="static-field overlayfield">
									<label>Scheduler Method<span class="mandatoryfildclass">*</span></label>						
									<select id="schedulermethodid" name="schedulermethodid" class="chzn-select" tabindex="103" data-validation-engine="validate[required]" data-placeholder="Select" data-prompt-position="topLeft:14,36">
										<option value=""></option>
										<?php foreach($schedulermethod as $key):?>
										<option data-name="<?php echo $key->schedulermethodname;?>" value="<?php echo $key->schedulermethodid;?>">
											<?php echo $key->schedulermethodname;?></option>
										<?php endforeach;?>	 
									</select>
								</div>
								<div class="static-field overlayfield">
									<input type="text" class="" id="schedulename" name="schedulename" data-validation-engine="validate[required]" value="" tabindex="1002">
									<label for="schedulename">Schedule Name</label>
								</div>
								<div class="static-field overlayfield">
									<label>Scheduler Type<span class="mandatoryfildclass">*</span></label>						
									<select id="schedulertypeid" name="schedulertypeid" class="chzn-select" tabindex="103" data-validation-engine="validate[required]" data-placeholder="Select" data-prompt-position="topLeft:14,36">
										<option value=""></option>
										<?php foreach($schedulertype as $key):?>
										<option data-name="<?php echo $key->schedulertypename;?>" value="<?php echo $key->schedulertypeid;?>">
											<?php echo $key->schedulertypename;?></option>
										<?php endforeach;?>	 
									</select>
								</div>
								<div class="static-field overlayfield">
									<input type="text" class="" id="intervaltime" name="intervaltime" data-validation-engine="validate[required]" value="" tabindex="1002">
									<label for="intervaltime">Interval</label>
								</div>
								<input type="hidden" name="hiddenschedulerid" id="hiddenschedulerid" value="" />
							</div>
						</div>
						<div class="alertbuttonarea">
							<input type="button" id="schedulerformsubmit" name="" value="Submit" tabindex="1003" class="alertbtn flloop  " >
							<input type="button" id="schedulerformclose" name="" value="Cancel" tabindex="1004" class="alertbtn flloop  cancelkeyboard" >
							<span class="lasttab" tabindex="1005"></span>
						</div>
					</div>
				</span>
			</form>
		</div>
	</div>