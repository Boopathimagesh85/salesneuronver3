<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Printtemplates extends MX_Controller {
	function __construct() {
		parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Base/Basefunctions');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Printtemplates/Printtemplatesmodel');
		$this->load->library('pdf');
		$this->load->library('convert');
	}
	public function index() {
		
		$moduleid= array(251);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp']=$this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp']=$this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids']=$viewmoduleid;
		$data['filedmodids']=$viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Printtemplates/printtemplatesview',$data);
	}
	public function newdatacreate() {
		$this->Printtemplatesmodel->newdatacreatemodel();
	}
	public function fetchformdataeditdetails() {
		$moduleid= 251;
		$this->Printtemplatesmodel->informationfetchmodel($moduleid);
	}
	public function datainformationupdate() {
		$this->Printtemplatesmodel->datainformationupdatemodel();
	}
	public function deleteinformationdata() {
		$moduleid= 251;
		$this->Printtemplatesmodel->deleteoldinformation($moduleid);
	}
	//merge field name value fetch
	public function fetchmaildddatawithmultiplecond() {
        $this->Printtemplatesmodel->fetchmaildddatawithmultiplecondmodel();
    }
	//fetch amount to word merge field name value
	public function fetchamtworddatawithmultiplecond() {
        $this->Printtemplatesmodel->fetchamtworddatawithmultiplecondmodel();
    }
	//editor value fetch
	public function editervaluefetch() {
		$filename = $_GET['filename']; 
		$newfilename = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]);
			echo json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			echo json_encode($tccontent);
		}
	}
	//print template folder add
	public function printtemplatefolderadd(){
		$this->Printtemplatesmodel->printtemplatefolderaddmodel();
	}
	//print template folder value get
	public function printtemplatefoldervalueget() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'foldername.foldernameid') : 'foldername.foldernameid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('foldernamename','description','setdefault'),'colmodelindex'=>array('foldername.foldernamename','foldername.description','status.statusname'),'coltablename'=>array('foldername','foldername','status'),'uitype'=>array('2','2','2'),'colname'=>array('Folder Name','Description','Default'),'colsize'=>array('200','200','200'));
		$result=$this->Printtemplatesmodel->printtemplatefoldervaluegetgridxmlview($primarytable,$sortcol,$sortord,$pagenum,$rowscount);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($result,$page,'');
		$device = $this->Basefunctions->deviceinfo();
		//if($device=='phone') {
			//$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Print Template Folder',$width,$height);
		//} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Print Template Folder',$width,$height);
		//}
		echo json_encode($datas);
	}
	//print template folder data fetch
	public function printtemplatefolderdatafetch() {
		$this->Printtemplatesmodel->printtemplatefolderdatafetchmodel();
	}
	public function printtemplatefolderupdate() {
		$this->Printtemplatesmodel->printtemplatefolderupdatemodel();
	}
	public function folderdeleteoperation() {
		$this->Printtemplatesmodel->folderdeleteoperationmodel();
	}
	//template file pdf preview
	public function templatefilepdfpreview() { 
		$print_data=array('templateid'=> $_POST['templateid'], 'Templatetype'=>'Printtemp','primaryset'=>'sales.salesid','primaryid'=>$_POST['primaryid']);
		$this->Printtemplatesmodel->generateprinthtmlfile($print_data);
	}
	//template content preview
	public function templatecontentpreview() {
		$content = $this->Printtemplatesmodel->templatecontentpreviewmodel();
		echo $content;
	}
	//print template unique name chk
	public function printtemplateuniquename() {
		$this->Printtemplatesmodel->printtemplateuniquenamemodel();
	}
	//print template folder unique name chk
	public function printtemplatefolderuniquename() {
		$this->Printtemplatesmodel->printtemplatefolderuniquenamemodel();
	}
	//fetch related module list
	public function relatedmodulelistfetch() {
		$this->Basefunctions->relatedmodulelistfetchforprint();
	}
	public function getformatdata()
	{
		$this->Printtemplatesmodel->getformatdata();
	}
	public function whereclauseload() {
		$this->Printtemplatesmodel->whereclauseload();
	}
	//fetch drop down value info
	public function fetchdddataviewddval() {
		$this->Printtemplatesmodel->fetchdddataviewddvalmodel();
	}
	//folder unique name restriction
	public function folderuniquedynamicviewnamecheck(){
		$this->Printtemplatesmodel->folderuniquedynamicviewnamecheckmodel();
	}
	public function createeditorfile() {
		$this->Printtemplatesmodel->createeditorfile();
	}
	//editor value fetch 	
	public function gettemplatesource() {
		$filename = trim($_GET['templatename']); 
		$newfilename = explode('/',$filename);
		if(read_file('termscondition/Epson/'.DIRECTORY_SEPARATOR.$newfilename[2])) {
			$tccontent = trim(file_get_contents('termscondition/Epson/'.DIRECTORY_SEPARATOR.$newfilename[2]));
			echo $tccontent;
		} else {
			$tccontent = "Fail";
			echo $tccontent;
		}
	}
}