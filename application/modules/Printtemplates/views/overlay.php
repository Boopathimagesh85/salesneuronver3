<!-- Template Print Overlay  Overlay-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="templatepdfoverlay" style="overflow-y: scroll;overflow-x: hidden">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="large-4 medium-6 large-centered medium-centered columns" >
			<form method="POST" name="pdfviewoverlayform" style="" id="pdfviewoverlayform" action="" enctype="" class="clearformtaxoverlay">
				<span id="pdfviewoverlayformvalidation" class="validationEngineContainer"> 
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-10 columns" style="background:none"> PDF</div>
							<div class="small-2 columns" id="pdfclose" style="text-align:right;cursor:pointer;background:none"><span class=""><i class="material-icons">cancel</i></span></div>
						</div>
						<div class="static-field large-12 columns">
							<label>Select Template<span class="mandatoryfildclass">*</span></label>
							<select data-placeholder="Select" data-prompt-position="topRight:0,0" class="chzn-select dropdownchange ffieldd validate[required]" tabindex="2" name="pdftemplateprview" id="pdftemplateprview">
							</select>
						</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div style="background:#f5f5f5" class="row">
						<div class="small-4 medium-4 large-4 columns">
							<input type="button" class="btn formbuttonsalert" value="Preview" name="pdffilepreviewintab" id="pdffilepreviewintab">	
						</div>
						<div class="small-4 medium-4 large-4 columns">
							<input type="button" class="btn  formbuttonsalert" value="Save" name="pdffilesaveintab" id="pdffilesaveintab">	
						</div>
  						<div class="small-4 medium-4 large-4 columns">
							<input type="button" class="btn formbuttonsalert" value="Cancel" name="pdfpreviewcancel" id="pdfpreviewcancel">	
						</div>
						<div class="medium-4 large-4 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>

					<div class="row">&nbsp;</div>
					<!-- hidden fields--> 
					<input type="hidden" name="taxcatid" id="taxcatid" />
				</span>
			</form>
		</div>
	</div>
</div>
<!-- Template Print Overlay  Overlay-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="templateprintoverlay" style="overflow-y: scroll;overflow-x: hidden">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="large-4 medium-6 large-centered medium-centered columns" >
			<form method="POST" name="" style="" id="" action="" enctype="" class="">
				<span id="" class="validationEngineContainer"> 
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-10 columns" style="background:none">Print</div>
							<div class="small-2 columns" id="printclose" style="text-align:right;cursor:pointer;background:none"><span class=""><i class="material-icons">cancel</i></span></div>
						</div>
						<div class="static-field large-12 columns">             
							<label>Select Template</label>
							<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd" tabindex="2" name="printtemplatepreview" id="printtemplatepreview">
							</select>                  
						</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div style="background:#f5f5f5" class="row">
						<div class="small-4 medium-4 large-4 columns">
							<input type="button" class="btn formbuttonsalert" value="Preview" name="" id="">	
						</div>
						<div class="small-4 medium-4 large-4 columns">
							<input type="button" class="btn formbuttonsalert" value="Print" name="" id="">	
						</div>
  						<div class="small-4 medium-4 large-4 columns">
							<input type="button" class="btn formbuttonsalert" value="Cancel" name="printpreviewcancel" id="printpreviewcancel">	
						</div>
						<div class="medium-4 large-4 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>

					<div class="row">&nbsp;</div>
					<!-- hidden fields -->
					<input type="hidden" name="taxcatid" id="taxcatid" />
				</span>
			</form>
		</div>
	</div>
</div>