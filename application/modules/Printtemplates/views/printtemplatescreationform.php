<style type="text/css">
	#printtemplatesfoladdgrid1 .gridcontent{
        height: 68vh !important;
    }
    #printtemplatesfoladdgrid1
    {
    height: 75vh !important;
    }
    #printtemplatesfoladdgrid1width 
    {
    height: 82vh !important;
    }
	.padding-space-open-for-form {
		padding-left:13px !important;
	}
	#printtemplatesfoladdgrid1footer {
		display:none;
	}
</style>
<div class="large-12 columns paddingzero formheader">
	<?php
		$this->load->view('Base/mainviewaddformheader');
	?>
</div>
<div class='large-12 columns addformunderheader'>&nbsp;</div>
<div class='large-12 columns scrollbarclass addformcontainer tabtouchaddcontainer'>
	<div class='row mblhidedisplay'>&nbsp;</div>
	<form method='POST' name='printtemplatesdataaddform' class='' action ='' id='printtemplatesdataaddform' enctype='multipart/form-data'>
		<span id='printtemplatesformaddwizard' class='validationEngineContainer'>
			<span id='printtemplatesformeditwizard' class='validationEngineContainer'>
				<?php
					formfieldstemplategenerator($modtabgrp,'');
				?>
			</span>
		</span>
		<span id="taglogomulitplefileuploader" style="display:none;">upload</span>
		<?php
			echo hidden('primarydataid');
			echo hidden('sortorder','');
			echo hidden('sortcolumn','');
			$value = 'foldername';
			echo hidden('resctable',$value);
			$modid = $this->Basefunctions->getmoduleid($filedmodids);
			echo hidden('viewfieldids',$modid);
			echo hidden('printpdfid','2');
		?>
	</form>
</div>
<div>
	<?php $this->load->view('Printtemplates/overlay');?>
</div>
