<!-- Amount to words field selection overlay-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="moneyconvertoverlay" style="overflow-y: scroll;overflow-x: hidden;">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="" >
			<form method="POST" name="" style="" id="" action="" enctype="" class="clearformformatoroverlay">
				<span id="moneyconvertvalidation" class="validationEngineContainer"> 
					<div class="alert-panel">
						<div class="alertmessagearea">
							<div class="alert-title">Formator</div>
							<div class="alert-message">
								<span class="firsttab" tabindex="1000"></span>
								<div class="static-field overlayfield">
									<label>Formator Type</label>
									<select id="curencyformatype" class="chzn-select ffieldd" data-placeholder="Select" data-prompt-position="topLeft:14,36" tabindex="1001" name="curencyformatype" title="">
										</option value="">Select</option>
										<option value="CW">Currency To Word</option>
										<!--<option value="CF">2-Currency Formator</option>-->
										<option value="CS">Currency Symbol</option>
									</select>
								</div>
								<div class="static-field overlayfield">
									<label>Field Name</label>
									<select id="amttowordfieldname" class="chzn-select " data-placeholder="Select" data-prompt-position="topLeft:14,36" tabindex="1002" name="amttowordfieldname" title="">
									</select>
								</div>
							</div>
						</div>
						<div class="alertbuttonarea">
							<input id="saveformatorword" class="alertbtn " type="button" tabindex="1003"  name="saveformatorword" value="Save" >
							<input type="button" id="formatorclose" name="" value="Cancel" tabindex="1004" class="alertbtn flloop  alertsoverlaybtn" >
							<span class="lasttab" tabindex="1005"></span>
						</div>
					</div>
				</span>
			</form>
		</div>
	</div>
</div>