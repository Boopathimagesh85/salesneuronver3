<!DOCTYPE html>
<html lang='en'>
	<head>
		<?php $this->load->view('Base/headerfiles'); ?>
	</head>
	<style type="text/css">
		#printernamedivhid , #widthdivhid{
			margin-top: 1rem;
		}
	</style>
<body class='hidedisplay'>
	<?php
		$moduleid = implode(',',$moduleids);
		$device = $this->Basefunctions->deviceinfo();
		$dataset['gridenable'] = 'yes';
		$dataset['modulename']='Printtemplates';
		$dataset['griddisplayid'] = 'printtemplatescreationview';
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['spanattr'] = array();
		$dataset['gridtableid'] = 'printtemplatesaddgrid';
		$dataset['griddivid'] = 'printtemplatesaddgridnav';
		$dataset['moduleid'] = $moduleids;
		$dataset['forminfo'] = array(array('id'=>'printtemplatescreationformadd','class'=>'hidedisplay','formname'=>'printtemplatescreationform'),array('id'=>'printpreviewform','class'=>'hidedisplay','formname'=>'printpreviewform'));
		if($device=='phone') {
			$this->load->view('Base/gridmenuheadermobile',$dataset);
			$this->load->view('Base/overlaymobile');
			$this->load->view('Base/viewselectionoverlay');
		} else {
			$this->load->view('Base/gridmenuheader',$dataset);
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
		$this->load->view('Base/basedeleteform');
		$this->load->view('previewoverlay');
	?>
	</body>
	<?php $this->load->view('Base/bottomscript'); ?>
	<script src="<?php echo base_url();?>js/plugins/mention/mention.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/mention/bootstrap-typeahead.js" type="text/javascript"></script>
	<script src='<?php echo base_url();?>js/Printtemplates/printtemplates.js' type='text/javascript'></script>
	<script src="<?php echo base_url();?>js/plugins/filecomputer/fileupload.min.js" type="text/javascript"></script>
	<!--For Tablet and Mobile view Dropdown Script-->
	<script>
		$(document).ready(function(){
			$("#tabgropdropdown").change(function(){
				var tabgpid = $("#tabgropdropdown").val(); 
				$('.sidebaricons[data-subform="'+tabgpid+'"]').trigger('click');
			});
		});
	</script>
</html>