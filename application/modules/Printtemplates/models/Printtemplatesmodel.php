<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Printtemplatesmodel extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->model('Base/Crudmodel');
		$this->load->model('Reportsview/Reportsmodel');
	}
	public function newdatacreatemodel() {
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		$partablename = $this->Crudmodel->filtervalue($elementpartable);
		$fieldstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fieldstable);
		$restricttable = explode(',',$_POST['resctable']);
		$templatename = $_POST['templatename'];
		$printtypeid = $_POST['printtypeid'];
		$primaryid = $this->Crudmodel->datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable);
		// remaining default template is unset 
		if($_POST['setasdefault'] == 'Yes') {
			$updaterevert=array(
				'setasdefault' => 'No'		
			);
			$this->db->where('printtemplatesid !=',$primaryid);
			$this->db->where('salestransactiontypeid =',$_POST['salestransactiontypeid']);
			$this->db->update('printtemplates',$updaterevert);
		}
		// printtemplate file create
		$this->updateprinttemplatedata($primaryid,$templatename,$printtypeid);
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' created Print Templates - '.$templatename.'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,251);
		echo 'TRUE';
	}
	public function informationfetchmodel($moduleid) {
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementspartabname']);
		$partablename = $this->Crudmodel->filtervalue($elementpartable);
		$fieldstable = $this->Crudmodel->filtervalue($formfieldstable);
		$primaryid = $_GET['dataprimaryid'];
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$fieldstable,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	public function datainformationupdatemodel() {
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		$partablename = $this->Crudmodel->filtervalue($elementpartable);
		$fieldstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fieldstable);
		$primaryid = $_POST['primarydataid'];
		$restricttable = explode(',',$_POST['resctable']);
		$templatename = $_POST['templatename'];
		$printtypeid = $_POST['printtypeid'];
		$this->Crudmodel->dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable);
		if(isset($_POST['salestransactiontypeid'])) {
			$_POST['salestransactiontypeid'] = $_POST['salestransactiontypeid'];
		} else {
			$_POST['salestransactiontypeid'] = 1;
		}
		// remaining default template is unset 
		if($_POST['setasdefault'] == 'Yes') {
			$updaterevert=array(
			  'setasdefault' => 'No'		
			);
			$this->db->where('printtemplatesid !=',$primaryid);
			$this->db->where('salestransactiontypeid =',$_POST['salestransactiontypeid']);
			$this->db->update('printtemplates',$updaterevert);
		}
		// printtemplate file create
		$this->updateprinttemplatedata($primaryid,$templatename,$printtypeid);
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' updated Print Templates - '.$templatename.'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$activity ,$userid,251);
		echo 'TRUE';
	}
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primarydataid'];
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$printtempname = $this->Basefunctions->singlefieldfetch('printtemplatesname','printtemplatesid','printtemplates',$id);
		$activity = ''.$user.' deleted Print Templates - '.$printtempname.'';
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				// delete printtemplate file
				$this->deleteprinttemplatedata($id);
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				$this->Basefunctions->notificationcontentadd($id,'Updated',$activity ,$userid,251);
				echo "TRUE";
			}
		} else {
			// delete printtemplate file
			$this->deleteprinttemplatedata($id);
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			$this->Basefunctions->notificationcontentadd($id,'Updated',$activity ,$userid,251);
			echo "TRUE";
		}
	}
	//drop down value set with multiple condition
	public function fetchmaildddatawithmultiplecondmodel() {
		$i=0;
		$j=0;
		$h=0;
		$jj=0;
		$salstatus = array(1,10);
		$tablname='';
		$salutable=array('lead','leadaddress','leadcf','contact','contactaddress','contactcf','employee','employeeaddress','employeecf');
		$dname=$_GET['dataname'];
		$did=$_GET['dataid'];
		$dataattrname1=$_GET['othername1'];
		$dataattrname2=$_GET['othername2'];
		$table=$_GET['datatab'];
		$whfield=$_GET['whdatafield'];
		$whdata=$_GET['whval'];
		$mulcond=explode(",",$whfield);
		$mulcondval=explode(",",$whdata);
		$this->db->select("$dname,$did,$dataattrname1,$dataattrname2");
		$this->db->from($table);
		foreach($mulcond AS $condname) {
			if($mulcondval[$i] != "0" && $mulcondval[$i] != "") {
				$this->db->where($condname,$mulcondval[$i]);
				$this->db->where_not_in('uitypeid',array(15,16));
			}
			$i++;
		}
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result()as $row) {
				$fname = $row->$dataattrname1;
				$dataname = explode(' ',$row->$dname);
				if(in_array('Primary',$dataname)) $fname = 'PR-'.trim($fname);
				if(in_array('Secondary',$dataname)) $fname = 'SE-'.trim($fname);
				if(in_array('Billing',$dataname)) $fname = 'BI-'.trim($fname);
				if(in_array('Shipping',$dataname)) $fname = 'SH-'.trim($fname);
				if($row->$dname!='') {
					if($fname != 'addresssourceid') {
						$data[$j]=array('datasid'=>$row->$did,'dataname'=>$row->$dname,'otherdataattrname1'=>$fname,'otherdataattrname2'=>$row->$dataattrname2);
						$j++;
					}
					if($fname == 'salutationid') {
						$h=1;
						$tablname = $row->$dataattrname2;
					}
					$jj = $j;
				}
			}
			if($h==0) {
				$i=0;
				$this->db->select("$dname,$did,$dataattrname1,$dataattrname2");
				$this->db->from($table);
				$this->db->where($dataattrname1,'salutationid');
				foreach($mulcond AS $condname) {
					if($mulcondval[$i] != "0" && $mulcondval[$i] != "") {
						$this->db->where($condname,$mulcondval[$i]);
					}
					$i++;
				}
				$this->db->where('status',10);
				$saresult = $this->db->get();
				if($saresult->num_rows() >0) {
					foreach($saresult->result()as $row) {
						if($row->$dataattrname1 != 'addresssourceid') {
							$data[]=array('datasid'=>$row->$did,'dataname'=>$row->$dname,'otherdataattrname1'=>$row->$dataattrname1,'otherdataattrname2'=>$row->$dataattrname2);
						}
					}
				}
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//drop down value set with multiple condition
	public function fetchamtworddatawithmultiplecondmodel() {
		$i=0;
		$j=0;
		$dname=$_GET['dataname'];
		$did=$_GET['dataid'];
		$dataattrname1=$_GET['othername1'];
		$dataattrname2=$_GET['othername2'];
		$table=$_GET['datatab'];
		$whfield=$_GET['whdatafield'];
		$whdata=$_GET['whval'];
		$mulcond=explode(",",$whfield);
		$mulcondval=explode(",",$whdata);
		$this->db->select("$dname,$did,$dataattrname1,$dataattrname2");
		$this->db->from($table);
		foreach($mulcond AS $condname) {
			if($mulcondval[$i] != "0" && $mulcondval[$i] != "") {
				$val = explode('|',$mulcondval[$i]);
				if(is_array($val) && count($val) > 1) {
					$this->db->where_in($condname,$mulcondval[$i]);
				} else {
					$this->db->where($condname,$mulcondval[$i]);
				}
			}
			$i++;
		}
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result()as $row) {
				$fname = $row->$dataattrname1;
				$dataname = explode(' ',$row->$dname);
				if(in_array('Primary',$dataname)) $fname = 'PR-'.trim($fname);
				if(in_array('Secondary',$dataname)) $fname = 'SE-'.trim($fname);
				if(in_array('Billing',$dataname)) $fname = 'BI-'.trim($fname);
				if(in_array('Shipping',$dataname)) $fname = 'SH-'.trim($fname);
				if($row->$dname!='') {
					$data[$j]=array('datasid'=>$row->$did,'dataname'=>$row->$dname,'otherdataattrname1'=>$fname,'otherdataattrname2'=>$row->$dataattrname2);
					$j++;
				}
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//print template folder add
	public function printtemplatefolderaddmodel() {
		$foldername = $_POST['foldername'];
		$description = $_POST['description'];
		$default = $_POST['setdefault'];
		$date = date($this->Basefunctions->datef);
		// set default no for all print folder
		if($_POST['setdefault']=='Yes'){
			$update_last=array(
					'setdefault'=>'No',
			);
			$update_last = array_merge($update_last,$this->Crudmodel->updatedefaultvalueget());
			$this->db->where_in('moduleid',251);
			$this->db->where('status',$this->Basefunctions->activestatus);
			$this->db->update('foldername',array_filter($update_last));
		}
		$folderarray = array(
						'foldernamename'=>$foldername,
						'description'=>$description,
						'setdefault'=>$default,
						'moduleid'=>251,
						'industryid'=>$this->Basefunctions->industryid,
						'userroleid'=>$this->Basefunctions->userroleid,
						'createdate'=>$date,
						'lastupdatedate'=>$date,
						'createuserid'=>$this->Basefunctions->userid,
						'lastupdateuserid'=>$this->Basefunctions->userid,
						'status'=>$this->Basefunctions->activestatus
					);
		$this->db->insert('foldername',$folderarray);
		echo "TRUE";
	}
	//print template folder update
	public function printtemplatefolderupdatemodel(){
		$foldernameid = $_POST['foldernameid'];
		$foldername = $_POST['foldername'];
		$description = $_POST['description'];
		$default = $_POST['setdefault'];
		// set default no for all print folder
		if($_POST['setdefault']=='Yes'){
			$update_last=array(
					'setdefault'=>'No',
			);
			$update_last = array_merge($update_last,$this->Crudmodel->updatedefaultvalueget());
			$this->db->where_in('moduleid',251);
			$this->db->where('status',$this->Basefunctions->activestatus);
			$this->db->update('foldername',array_filter($update_last));
		}
		$date = date($this->Basefunctions->datef);
		$folderarray = array(
						'foldernamename'=>$foldername,
						'description'=>$description,
						'setdefault'=>$default,
						'userroleid'=>$this->Basefunctions->userroleid,
						'lastupdatedate'=>$date,
						'lastupdateuserid'=>$this->Basefunctions->userid,
					);
		$this->db->where('foldername.foldernameid',$foldernameid);
		$this->db->update('foldername',$folderarray);
		echo "TRUE";
	}
	//Folder main grid view
	public function printtemplatefoldervaluegetgridxmlview($tablename,$sortcol,$sortord,$pagenum,$rowscount) {
		$dataset = 'foldername.foldernamename,foldername.foldernameid,foldername.description,foldername.setdefault';
		$join =' LEFT OUTER JOIN status ON status.status=foldername.status';
		$status = $tablename.'.status IN (1,2)';
		$where = $tablename.'.moduleid IN (251)';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		//query
		$data = $this->db->query('select '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$where.' AND '.$status.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		return $data;
	}
	//print template folder data fetch
	public function printtemplatefolderdatafetchmodel() {
		$folderid = $_GET['folderid'];
		$this->db->select('SQL_CALC_FOUND_ROWS  foldername.foldernamename,foldername.foldernameid,foldername.description,foldername.setdefault',false);
		$this->db->from('foldername');
		$this->db->where('foldername.foldernameid',$folderid);
		$this->db->where('foldername.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data=array('id'=>$row->foldernameid,'foldername'=>$row->foldernamename,'description'=>$row->description,'setdefault'=>$row->setdefault);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//fetch related module list
	public function relatedmodulelistfetchmodel() {
		$moddatas = array();
		$relmodids = array();
		$moduleid = $_GET['mid'];
		$genmod = array(2,3,265);
		$relmoduleids = $this->db->select('modulerelationid,relatedmoduleid')->from('modulerelation')->where('moduleid',$moduleid)->get();
		foreach($relmoduleids->result() as $reldata) {
			$relmodids[] = $reldata->relatedmoduleid;
		}
		$i=0;
		$modids = implode(',',$relmodids);
		$moduleids = ( ($modids!="")? $modids.','.$moduleid : $moduleid );
		$modids = explode(',',$moduleids);
		$allmodid = array_merge($modids,$genmod);
		$allmoduleids = implode(',',array_unique($allmodid));
		$modinfo = $this->db->query('select module.moduleid,module.modulename from module join moduleinfo ON moduleinfo.moduleid=module.moduleid where module.moduleid IN ('.$allmoduleids.') AND module.status=1 AND moduleinfo.status=1 group by moduleinfo.moduleid',false);
		foreach($modinfo->result() as $moddata) {
			$modtype = ( ($moddata->moduleid==$moduleid)?'':((in_array($moddata->moduleid,$genmod))?'GEN:':'REL:') );
			$moddatas[$i] = array('datasid'=>$moddata->moduleid,'dataname'=>$moddata->modulename,'datatype'=>$modtype);
			$i++;
		}
		$datas = ( ($i==0)? json_encode(array('fail'=>'FAILED')) : json_encode($moddatas) );
		echo $datas;
	}
	//print template folder delete operation
	public function folderdeleteoperationmodel() {
		$folderid = $_GET['id'];
		$date = date($this->Basefunctions->datef);
		$folderarray = array(
						'lastupdatedate'=>$date,
						'lastupdateuserid'=>$this->Basefunctions->userid,
						'status'=>0
					);
		$this->db->where('foldername.foldernameid',$folderid);
		$this->db->update('foldername',$folderarray);
		echo "TRUE";
	}
	//table field name check
	public function tablefieldnamecheck($tblname,$fieldname) {
		$result = 'false';
		$fields = $this->db->field_data($tblname);
		foreach ($fields as $field) {
			$name =  $field->name;
			if($fieldname == $name) {
				$result = 'true';
			}
		}
		return $result;
	}
	// label print function - directly called from respective models
	public function generatlabelprint($print_data)
	{ 
		$templateid='';
	    $tagid='';
		$templateid=$print_data['templateid'];
		$tagid=$print_data['primaryid'];
		$print_data['labelprint'] = 'labelprint';
		$info = $this->db->select('fieldid,filelocation,printername,printcmd,prnfile,moduleid')
		->from('tagtemplate')
		->where('tagtemplateid',$templateid)
		->limit(1)
		->get()
		->row();
		$fieldids = explode(',',$info->fieldid);
		$filelocation = $info->filelocation;
		$printername=$info->printername;
		$moduleid = $info->moduleid;
		$cmd=$info->printcmd;
		$prn_file=explode('/',$info->prnfile);
		$parenttable = $this->Basefunctions->generalinformaion('module','modulemastertable','moduleid',$moduleid);
		//read the files
		$filename = trim($filelocation);
		$newfilename = explode('/',$filename);
		$computername = gethostname(); //gets computer name
		//if(read_file('printtemplate/tagprint/'.DIRECTORY_SEPARATOR.$newfilename[2]))
		if(read_file($filename))
		{
			//$tccontent = trim(file_get_contents('printtemplate/tagprint/'.DIRECTORY_SEPARATOR.$newfilename[2]));
			$tccontent = trim(file_get_contents($filename));
			if($tccontent != '')
			{
				// test print for tag template
				if(empty($tagid)) {
					$bodyhtml =  $tccontent;
				}
				else 
				{
					$tccontent = preg_replace('~\|~','ssspipe890', $tccontent);
					$bodyhtml = $this->generateprintingdata($tccontent,$parenttable,$print_data,$moduleid);
					preg_match_all ("/#.*#/U", $bodyhtml,$contentmatch); //content match
					foreach($contentmatch as $mccontent) { //check table cell contain multiple elements 
						foreach($mccontent as $cellmatachdata)
						{ 
							$re = "/[\\#]/m"; 
							$subst = " "; 
							$result = preg_replace($re, $subst, $cellmatachdata);
							$mtext = explode('f=',$result);
							eval("\$mresult = \"$mtext[1]\";");
							$mresult = eval("return $mtext[1];");
							$bodyhtml = str_replace($cellmatachdata,$mresult,$bodyhtml);
						} 
					}
				}
				$data = $bodyhtml;
				/* software use in drive */
					chmod('printdata.prn', 0777);
					$fh = fopen('printdata.prn','w') or die('fileopenerror');
					fwrite($fh,$data);
					fclose($fh);				
					//bat files updates.
					//$computername = gethostname(); //gets computer name
					//$computername = 'ADMIN-PC';
				
					//$batchdata = "COPY printdata.prn \\\\".$computername."\\".$printername."";					
					$batchdata = "COPY printdata.prn \\\\".$printername."";					
					chmod('cmdprint.bat', 0777);
					$fh = fopen('cmdprint.bat','w') or die('fileopenerror');
					fwrite($fh,$batchdata);
					fclose($fh);
					//exec('c:\WINDOWS\system32\cmd.exe START /B /C cmdprint.bat /q',$out,$return);		
					exec($cmd,$out,$return);	
				/* software use pen drive  */
				/*chmod(''.$cmd.':\printdata.prn', 0777);
				$fh = fopen(''.$cmd.':\printdata.prn','w') or die('fileopenerror');
				fwrite($fh,$data);
				fclose($fh);
				exec(''.$cmd.':\cmd.exe START /B /C '.$cmd.':\cmdprint.bat /q',$out,$return);*/
			}
		}
		return 'SUCCESS';
	}
	//template content preview
	public function templatecontentpreviewmodel() {
		$templateid = $_POST['templateid'];
		$headerfile = "";
		$contentfile = "";
		$footerfile = "";
		$this->db->select('printtemplates.printtemplatestemplate_editorfilename');
		$this->db->from('printtemplates');
		$this->db->where('printtemplates.printtemplatesid',$templateid);
		$result = $this->db->get();
		foreach($result->result() as $rowdata) {
			$contentfile = $rowdata->printtemplatestemplate_editorfilename;
		}
		//html content
		$bodycontent = $this->filecontentfetch($contentfile);
		$htmlcontent = $bodycontent;
		return $htmlcontent;
	}
	//preview and print pdf
	public function generateprinthtmlfile($print_data)  {
		//defaults				
		$printdetails['marginleft'] ='2';
		$printdetails['marginright'] ='2';
		$printdetails['margintop'] =  '2';
		$printdetails['marginbottom'] = '2';
		$printdetails['orientation'] = 'L';
		$printdetails['pageformat'] = 'A4';
		$printdetails['moduleid'] = '1';
		$printdetails['templatename'] = '';
		$printdetails['formattype'] = '';
		$printdetails['headerspacing'] =   '';
		$printdetails['footerspacing'] =    '';
		$printdetails['height'] =  '';
		$printdetails['width'] =  '';
		$printdetails['printtypeid'] =  '';
		$printdetails['headername'] = '';
		$printdetails['footername'] = '';
		$printdetails['cssfilename'] = '';
		$htmldatacontents='';
		$hdata ='';
		$fdata ='';
		$templateid = ( (isset($print_data['templateid']))? $print_data['templateid'] : 1);
		$id = ( (isset($print_data['primaryid']))? $print_data['primaryid'] : 1);
		$pdfmoduleid = ( (isset($print_data['pdfmoduleid']))? $print_data['pdfmoduleid'] : 1);
		$randomnumber = ( (isset($print_data['randomnumber']))? $print_data['randomnumber'] : '1');
		$viewtype = ( (isset($print_data['viewtype']))? $print_data['viewtype'] : 'print');
		$this->db->select('printtemplates.printtemplatestemplate_editorfilename,printtemplates.moduleid,printtemplates.printtemplatesname,printtemplates.printtypeid,printtemplates.headerfilename,printtemplates.footerfilename,cssfilename');
		$this->db->from('printtemplates');
		$this->db->where_in('printtemplates.printtemplatesid',$templateid);
		$result = $this->db->get();
		
		foreach($result->result() as $rowdata) {
			$printdetails['contentfile'] =  $rowdata->printtemplatestemplate_editorfilename;
			$printdetails['moduleid'] =  $rowdata->moduleid;
			$printdetails['templatename'] = $rowdata->printtemplatesname;
			$printdetails['printtypeid'] = $rowdata->printtypeid;
			$printdetails['snumber'] = $print_data['snumber'];
			
			$printdetails['headername'] =  $rowdata->headerfilename;
			$printdetails['footername'] =  $rowdata->footerfilename;
			$printdetails['cssfilename'] =  $rowdata->cssfilename;
			/*$printdetails['marginleft'] =  $rowdata->marginleft;
			$printdetails['marginright'] =  $rowdata->marginright;
			$printdetails['margintop'] =  $rowdata->margintop;
			$printdetails['marginbottom'] =  $rowdata->marginbottom;
			$printdetails['orientation'] =  $rowdata->orientationname;
			$printdetails['pageformat'] =  $rowdata->pageformatname;


			$printdetails['formattype'] = $rowdata->printformatname;
			$printdetails['headerspacing'] =  $rowdata->headerspacing;
			$printdetails['footerspacing'] =   $rowdata->footerspacing;
			$printdetails['height'] = $rowdata->height;
			$printdetails['width'] = $rowdata->width;	
			$printdetails['unit'] = $rowdata->pageunitname; */
			
		}
		
		$parenttable = $this->Basefunctions->generalinformaion('module','modulemastertable','moduleid',$printdetails['moduleid']);
		if($printdetails['headername'] != '') {

				$hcontent = $this->filecontentfetch($printdetails['headername']);
				$hcontent = preg_replace('~a10s~','&nbsp;', $hcontent);
				$hcontent = preg_replace('~<br>~','<br />', $hcontent);
				$hcontent = $this->removespecialchars($hcontent);
				$hhtml = $this->generateprintingdata($hcontent,$parenttable,$print_data,$printdetails['moduleid']);
				$hhtml = $this->addspecialchars($hhtml);
				$hhtmldatacontents = '';
				$hhtmldatacontentsnew=  preg_replace('#\<0\>(.+?)\<\/0\>#s', '', $hhtml);
				$hhtmldatacontents .=  preg_replace('/<([0-9]+)>/', '', $hhtmldatacontentsnew);
				//$hhtmldatacontents .= '</body></html>'; 
				$hdata = $hhtmldatacontents; 
				/* $my_file = 'printtemplate/printtempheader.html';
				$handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file);
				fwrite($handle, $hdata);
				fclose($handle); */
				
		}
		if($printdetails['footername'] != '') {
			
			$fcontent = $this->filecontentfetch($printdetails['footername']);
			$fcontent = preg_replace('~a10s~','&nbsp;', $fcontent);
			$fcontent = preg_replace('~<br>~','<br />', $fcontent);
			$fcontent = $this->removespecialchars($fcontent);
			$fhtml = $this->generateprintingdata($fcontent,$parenttable,$print_data,$printdetails['moduleid']);
			$fhtml = $this->addspecialchars($fhtml);
			$fhtmldatacontents = '';
			$fhtmldatacontentsnew=  preg_replace('#\<0\>(.+?)\<\/0\>#s', '', $fhtml);
			$fhtmldatacontents .=  preg_replace('/<([0-9]+)>/', '', $fhtmldatacontentsnew);
			//$fhtmldatacontents .= '</body></html>'; 
			$fdata = $fhtmldatacontents; 
			/* $my_file = 'printtemplate/printtempfooter.html';
			$handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file);
			fwrite($handle, $fdata);
			fclose($handle); */
			
		}
		
		//body data
		$bodycontent = $this->filecontentfetch($printdetails['contentfile']);
		$bodycontent = preg_replace('~a10s~','&nbsp;', $bodycontent);
		$bodycontent = preg_replace('~<br>~','<br />', $bodycontent);
		$bodycontent = $this->removespecialchars($bodycontent);
		$bodyhtml = $this->generateprintingdata($bodycontent,$parenttable,$print_data,$printdetails['moduleid']);
		$bodyhtml = $this->addspecialchars($bodyhtml);
		$htmldatacontents = '';
		$htmldatacontentsnew=  preg_replace('#\<0\>(.+?)\<\/0\>#s', '', $bodyhtml);
		$htmldatacontents .=  preg_replace('/<([0-9]+)>/', '', $htmldatacontentsnew);
		if($printdetails['printtypeid'] == 3 || $printdetails['printtypeid'] == 6) {  // print option FOR HTML NEW TAB
			if($hdata == '' ) {
				if($printdetails['cssfilename'] != '') {
					$csscontent = $this->filecontentfetch($printdetails['cssfilename']);
				} else {
					$csscontent = '<html><head><style type="text/css" media="print">
								@page 
								{
									size:  auto;   /* auto is the initial value */
									margin: 0mm;  /* this affects the margin in the printer settings */
								}
								html
								{
									background-color: #FFFFFF; 
									margin: 0px;  /* this affects the margin on the html before sending to printer */
								}
								body
								{
									margin: 0mm 0mm 0mm 0mm; /* margin you want for the content */
								}
								.printinghidedisplay{display:block;}
								</style><style>.printhide0{display:none;} .printhide{display:none;}</style></head><body onload="window.print();">';
				}
				
				$htmldatacontents = $csscontent.'  '.$htmldatacontents.' </body></html>';
				
			} else {
				if($printdetails['cssfilename'] != '') {
					$csscontent = $this->filecontentfetch($printdetails['cssfilename']);
				} else {
					$csscontent = '';
				}
				$htmldatacontents = $csscontent.'    '.$hdata.'         '.$htmldatacontents.'         '.$fdata;
			}
		}
		$data = $htmldatacontents;
		
		if($printdetails['printtypeid']== 5) { //epson js
			$data = str_replace("<table>","",$data);
			$data = str_replace("<tbody>","",$data);
			$data = str_replace("<tr>","",$data);
			$data = str_replace("<td>","",$data);
			$data = str_replace("</tbody>","",$data);
			$data = str_replace("</tr>","",$data);
			$data = str_replace("</td>","",$data);
			$data = str_replace("</table>","",$data);
			$data = preg_replace("/<zrm0>(.*)<\/zrm>/U", "", $data);
			$data = str_replace("</zrm>","",$data);// zrm is our defined keyword,we use it to remove unwated code blocks similar to html printhide0 concept
			$data = str_replace("<zrm1>","",$data);
			$data = str_replace("<zrm>","",$data);
			return $data;
		} 
		else if($printdetails['printtypeid']== 3) { //html new tab
			if($viewtype == 'print') {
				$roundweight=$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); // weight round
				if(isset($print_data['snumber']) && $print_data['snumber'] != '') {
					//$print_data['snumber'] = str_replace("/","",$print_data['snumber']);
					$my_file = 'billshtml/'.$print_data['snumber'].$randomnumber.'.html';
				} else {
					$a = $printdetails['moduleid'].'-'.$print_data['primaryid']; 
					$my_file = 'billshtml/'.$a.$randomnumber.'.html';
				}
				$handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file);
				fwrite($handle, $data);
				fclose($handle);
				echo 'SUCCESS';
			} else {
				echo $data;
			}
		}
		else if($printdetails['printtypeid']== 6) { //silent html
			echo  $data;
		}
		else if($printdetails['printtypeid']== 4) { //excel
		
		}
		else if($printdetails['printtypeid']== 2) { // GENERATE PDF WITH WKHTML
		
			$base = explode('/',$_SERVER['SCRIPT_NAME']);
			$root = $base[1];
			$path = str_replace("/","\\",$_SERVER["DOCUMENT_ROOT"]);
			$hostpath = $path.'\\'.$root.'\\printtemplate\\receipt.pdf';
			$wkhtmlexepath = $path.'\\'.$root.'\\wkhtmltopdf\\wkhtmltopdf.exe';  
			$this->generatepdfwithwkhtml($printdetails,$wkhtmlexepath,$root,$hostpath);
		}
	}
	public function generatepdfwithwkhtml($printdetails,$wkhtmlexepath,$root,$hostpath)
	{
		
		if($printdetails['formattype']  == 'Auto') // for automatic predefined paper size 
		 {
		   $command = '"'.$wkhtmlexepath.'" -s '.$printdetails['pageformat'] .' -O  '.$printdetails['orientation'].'  --margin-top '.$printdetails['margintop'].$printdetails['unit'].' --margin-bottom '.$printdetails['marginbottom'].$printdetails['unit'].' --margin-right '.$printdetails['marginright'].$printdetails['unit'].' --margin-left '.$printdetails['marginleft'].$printdetails['unit'].'  --header-spacing  '.$printdetails['headerspacing'].'  --footer-spacing '.$printdetails['footerspacing'].' --header-html  http://localhost/'.$root.'/printtemplate/printtempheader.html --footer-html  http://localhost/'.$root.'/printtemplate/printtempfooter.html  http://localhost/'.$root.'/billshtml/'.$printdetails['snumber'].'.html '.$hostpath.'';
		 } 
		 else // for manual  paper size 
		 {
		   $command = '"'.$wkhtmlexepath.'" --page-height '.$height.$unit.' --page-width '.$width.$unit.' -O  '.$printdetails['orientation'].'  --margin-top '.$printdetails['margintop'].$printdetails['unit'].' --margin-bottom '.$printdetails['marginbottom'].$printdetails['unit'].' --margin-right '.$printdetails['marginright'].$printdetails['unit'].' --margin-left '.$printdetails['marginleft'].$printdetails['unit'].'  --header-spacing  '.$printdetails['headerspacing'].'  --footer-spacing '.$printdetails['footerspacing'].' --header-html  http://localhost/'.$root.'/printtemplate/printtempheader.html --footer-html  http://localhost/'.$root.'/printtemplate/printtempfooter.html  http://localhost/'.$root.'/billshtml/'.$printdetails['snumber'].'.html '.$hostpath.'';
		}
		exec($command,$out,$return);
	}
	//fetch inside the table tr row content
	public function generateprintingdata($datacontent,$parenttable,$print_data,$moduleid,$type='print') {
		$htmlcontent = $datacontent;
		$finalcont = $datacontent;
		$tabpattern = "/<table ?.*>.*<\/table>/U";
		preg_match_all($tabpattern,$datacontent,$tabmatches);
		foreach($tabmatches as $tabmatch) { //contain more than one table(s)
			$tabcontent="";
			foreach($tabmatch as $tabcontentmatach) {//contain single table
				$tabcontent = $tabcontentmatach;
				$trpattern = "/<tr ?.*>.*<\/tr>/U";
				preg_match_all($trpattern,$tabcontentmatach, $trmatches);
				foreach($trmatches as $trmatch) {
					$prevtrcontent="";
					foreach($trmatch as $trdata) {
						$prevtrcontent = $trdata;
						$tdpattern = "/<td ?.*>.*<\/td>/U";
						preg_match_all($tdpattern,$trdata, $tdmatches);
						foreach($tdmatches as $tdmatch) {//contain multiple td in single row
							$matchelement = array();
							$matcheledata = array();
							foreach($tdmatch as $tddata) { //if table cell contain single value
									$pretdcontent = $tddata;
									preg_match_all ("/{.*}/U", $tddata, $contentmatch); //content match
									foreach($contentmatch as $mcontent) { //individual td data
										foreach($mcontent as $cellmatachdata) { //fetch data for td matches
											if( $cellmatachdata == '{S.Number}' ) {
												$displaydata = 'ss890ss';
											} else {
												$displaydata = $this->mergcontinformationfetchmodel($cellmatachdata,$parenttable,$print_data,$moduleid);
											}
											$matchelement[] = $cellmatachdata;
											$matcheledata[] = $displaydata;
										}
									}
								}
								//multiple content mapping
								$datas = array();
								$slnumkey = array_search('ss890ss',$matcheledata,false);
								$sno = 1;
								$sm = 0;
								//convert to row data based on column
								foreach($matcheledata as $key => $matchvals) {
									$m=0;
									$sno = 1;
									if($slnumkey !== false) { //sno available
										if(is_array($matchvals)) {
											foreach($matchvals as $val) {
												$datas[$m][$slnumkey] = $sno;
												$datas[$m][$key] = $val;
												$m++;
												$sno++;
											}
										} else if($matchvals != 'ss890ss') {
											$datas[$m][$key] = $matchvals;
											$m++;
										}
									} else { //sno is not available
										if(is_array($matchvals)) {
											foreach($matchvals as $val) {
												$datas[$m][$key] = $val;
												$m++;
											}
										} else {
											$datas[$m][$key] = $matchvals;
											$m++;
										}
									}
								}
								$trdatasets = "";
								$trdataset = "";
								if(count($datas) > 0) {
									foreach($datas as $matchdata) { //each row
										$trdataset = $trdata;
										$h=0;
										for($i=0;$i<count($matchdata);$i++) {
											if(isset($matchdata[$i])) {
												if(is_array($matchdata[$i])) {
													if(count($matchdata)>1) {
														$data = implode('<br/>',$matchdata[$i]);
													} else {
														$data = implode(',',$matchdata[$i]);
													}
												} else {
													$data = $matchdata[$i];
												}
												$trdataset = preg_replace('~'.$matchelement[$h].'~',$data,$trdataset); //td data replace to row
												$h++;
											}
										}
										$trdatasets .= $trdataset;
									}
									$trdata =  $trdatasets;
								}
								$tabcontentmatach = preg_replace('~'.$prevtrcontent.'~',$trdata,$tabcontentmatach);//row data replace to table
							//}
							$tabcontentmatach = preg_replace('~'.$prevtrcontent.'~',$trdata,$tabcontentmatach);//row data replace to table
						}
						$tabcontentmatach = preg_replace('~'.$prevtrcontent.'~',$trdata,$tabcontentmatach);//row data replace to table
					}
					$htmlcontent = preg_replace('~'.$tabcontent.'~',$tabcontentmatach,$htmlcontent); //table data replace to content
					$finalcont = $htmlcontent;
				}
 			}
		}
		preg_match_all ("/{.*}/U", $finalcont, $contmatch);
		foreach($contmatch as $bocontent) {
			foreach($bocontent as $bomatch) {
				if($bomatch != "{S.Number}") {
					if($type == 'print') {
						$htmldata = $this->mergcontinformationfetchmodel($bomatch,$parenttable,$print_data,$moduleid);
						//$bomatch = str_replace("|","",$bomatch);
						//$finalcont = str_replace("|","",$finalcont);
						$finalcont = preg_replace('~'.$bomatch.'~',implode(',',$htmldata),$finalcont);
					} else {
						$data = explode('.',$bomatch);
						$viewcolid = str_replace('{','',$data[0]);
						$htmldata = $this->Printtemplatesmodel->mergcontinformationfetchmodel($bomatch,$parenttable,$print_data,$moduleid);
						$linkable = $this->Basefunctions->generalinformaion('viewcreationcolumns','linkable','viewcreationcolumnid',$viewcolid);
						if($linkable == 'Yes') {
							$fieldmodid = $this->Basefunctions->generalinformaion('viewcreationcolumns','linkmoduleid','viewcreationcolumnid',$viewcolid);
							$fieldlink = $this->Basefunctions->generalinformaion('module','modulelink','moduleid',$fieldmodid);
							$fieldtable = $this->Basefunctions->generalinformaion('module','modulemastertable','moduleid',$fieldmodid);
							$datarowid = $this->Basefunctions->generalinformaion($parenttable,$fieldtable.'id',$parenttable.'id',$print_data['primaryid']);
							$htmldata = '<span class="tealcolor notiredirecturl" data-datarowid='.$datarowid. ' data-redirecturl='.base_url().$fieldlink.'>'.implode(',',$htmldata).'</span>';
						} else {
							$htmldata = implode(',',$htmldata);
						}
						$bomatch = str_replace("|","",$bomatch);
						$finalcont = str_replace("|","",$finalcont);
						$finalcont = preg_replace('~'.$bomatch.'~',$htmldata,$finalcont);
					}
				} else {
					$finalcont = preg_replace('~'.$bomatch.'~','',$finalcont);
				}
			}
		}
		if(isset($print_data['labelprint'])){
			if($print_data['labelprint'] == 'labelprint'){
				
			}
		}else {
			$finalcont = $this->removespecialchars($finalcont);
			preg_match_all ("/zyx.*zyx/U", $finalcont, $contmatch);
			foreach($contmatch as $bocontent) {
				foreach($bocontent as $bomatch) {
					$mergetexttworound = str_replace("zyx","",$bomatch);
					//print_r($mergetexttworound);
					$htmldata = $this->mergcontinformationfetchmodel($mergetexttworound,$parenttable,$print_data,$moduleid);
					//$bomatch = str_replace("|","",$bomatch);
					//$finalcont = str_replace("|","",$finalcont);
					$finalcont = preg_replace('~'.$bomatch.'~',implode(',',$htmldata),$finalcont);
			 }
			 $finalcont = str_replace("zyx","",$finalcont);
			}
			 preg_match_all ("/{.*}/U", $finalcont, $contmatch);
			 foreach($contmatch as $bocontent) {
				foreach($bocontent as $bomatch) {
					$htmldata = $this->mergcontinformationfetchmodel($bomatch,$parenttable,$print_data,$moduleid);
					//$bomatch = str_replace("|","",$bomatch);
					//$finalcont = str_replace("|","",$finalcont);
					$finalcont = preg_replace('~'.$bomatch.'~',implode(',',$htmldata),$finalcont);
			 }
			}
		}
		return $finalcont;
	}
	//merge content data fetch
	public function mergcontinformationfetchmodel($mergetext,$parenttable,$print_data,$moduleid) {
		
		$roundweight=$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); // weight round
		$roundamount=$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); // amount round
		$roundrate=$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); // rate round
		$roundmelting=$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); // melting round
		
		$mergetext = rtrim($mergetext, "}");
		$mergetext = ltrim($mergetext, "{");	
		$formatortype = "1";
		$primaryid = $print_data['primaryid'];
		$mergeblock1 = explode("ssspipe890",$mergetext);
		$mergegp = '';
		$wheretext ='';
		$ordertext ='';
		$mergetextlength = 0;
		$firstpart = explode(".",$mergeblock1[0]);
		$vcolid = $firstpart[0];
		$wordsconvert = 0;
		//print_r($vcolid);
		if (strpos($mergetext, 'ssspipe890') !== false) {
			for($i = 1;$i < count($mergeblock1);$i++) {
				$eachblocksplit = explode(":",$mergeblock1[$i]);
				$eachblockfirst = strtolower($eachblocksplit[0]);
				switch($eachblockfirst) {
					case 'w':
						$mergewhere = explode(",",$eachblocksplit[1]);
						break;
					case 'o':
						$mergeorder = explode(",",$eachblocksplit[1]);
						break;
					case 't':
						$mergegp = $eachblocksplit[1];
						break;
					case 'c':
						$mergetextlength = $eachblocksplit[1];
						break;
					case 'k':
						$mergerkeywords = explode(",",$eachblocksplit[1]);
						break;
					case 'v':
						$mergerkeyvalue = explode(",",$eachblocksplit[1]);
						break;
					case 'words':
						$wordsconvert = 1;
						break;
				}
			}
		}
		$addljoincolid = '';
		if(isset($mergewhere[0]))
		{
			foreach($mergewhere AS $mergewheretext) 
			{
				
				$mergewheretext = strtoupper($mergewheretext);
				$mergewheretextquery = $this->Basefunctions->generalinformaion('whereclausecreation','wherequery','whereclauseid',$mergewheretext);
				$mergewheretextquery = explode("|",$mergewheretextquery);
				$addljoincolid = $addljoincolid.','.$mergewheretextquery[0];
				$wheretext = $wheretext." and ".$mergewheretextquery[1];
				$wheretext = strtolower($wheretext);
				
			}
		}
		if(isset($mergerkeywords[0]))
		{
			for($i = 0;$i < count($mergerkeywords);$i++) {
				$keywordtext = strtolower($mergerkeywords[$i]);
				$keyvaluetext = strtolower($mergerkeyvalue[$i]);
				$keyvaluetext = rtrim($keyvaluetext, " ");
				$keyvaluetext = ltrim($keyvaluetext, " ");
				$wheretext = str_replace($keywordtext,$keyvaluetext,$wheretext);
			}
			
		}
		if(isset($mergeorder[0]))
		{
			$ordertext = 'order by  ';
			foreach($mergeorder AS $mergeordertext) 
			{
				$mergeordertext = strtoupper($mergeordertext);
				$mergeordertextquery = $this->Basefunctions->generalinformaion('whereclausecreation','wherequery','whereclauseid',$mergeordertext);
				$mergeordertextquery = explode("|",$mergeordertextquery);
				$addljoincolid = $addljoincolid.','.$mergeordertextquery[0];
				$ordertext = $ordertext." ".$mergeordertextquery[1]." asc  ,";
				
			}
		}
		$ordertext = rtrim($ordertext, ",");
		if($mergegp == 'y')
		{
			$data=$this->db->select('viewcreationcolumns.viewcreationcolmodeltable,viewcreationcolumns.viewcreationparenttable,viewcreationcolumns.viewcreationjoincolmodelname,viewcreationtype')
			->from('viewcreationcolumns')
			->where('viewcreationcolumns.viewcreationcolumnid',$vcolid)
			->where('viewcreationcolumns.status',$this->Basefunctions->activestatus)
			->get();
			
			if($data->num_rows() > 0)
			{			
				foreach($data->result() as $info)
				{
					$viewcreationcolmodeltable=$info->viewcreationcolmodeltable;
					if (preg_match('/as/',$viewcreationcolmodeltable)){
						$keywords = preg_split('/as/', $viewcreationcolmodeltable);
						$mergegp = 'group by '.$keywords[1].'.'.trim($keywords[0]).'id';
					}else{
						$mergegp = 'group by '.$info->viewcreationparenttable.'.'.$info->viewcreationparenttable.'id';
					}
			   }
			}
		}
		else
		{
			$mergegp = '';
		}
		if(isset($print_data['where'])){
			if(isset($print_data['parentid'])){
				if($vcolid == '1794' || $vcolid == '1790') {
					$wherecond='chitentry.chitbookno ="'.$print_data['chitbookno'].'" AND';
				}else {
					$wherecond=$print_data['where'].'= '.$print_data['parentid'].' AND';
				}					
			 }
			else{
				if($vcolid == '1794' || $vcolid == '1790') {
					$wherecond='chitentry.chitbookno ="'.$print_data['chitbookno'].'" AND';
				}else {
					$wherecond=$print_data['where'].'= '.$print_data['primaryid'].' AND';
				}
			}
		}else{
			$wherecond=' 1=1 AND';
		} 
		if(isset($print_data['stockdate']))
		{
			$fromdate = $print_data['stockdate'];
			$todate = $print_data['stocktodate'];
			if($fromdate != '') { 
				$fdate =  $this->Basefunctions->ymddateconversion($fromdate);
			} else {
				$fdate = date("Y-m-d");
			}
			if($todate != '') { 
				$tdate =  $this->Basefunctions->ymddateconversion($todate);
			} else {
				$tdate = date("Y-m-d");
			}
			if($fromdate != $todate) {
				$fromdate = $fromdate.' to '.$todate;
			}
			$fbetdate = "BETWEEN '".$fdate."' AND '".$tdate."'";
			$fromdatewhere = '"'.$fdate.'"';
			$kkdailydatestart = $this->Basefunctions->ymddateconversion($fdate);
			$kkdailydateend = $this->Basefunctions->ymddateconversion($todate);
		}
		if(isset($print_data['htmlaccountid'])) {
			$accountidcheck = $print_data['htmlaccountid'];
			if($accountidcheck > 1) {
				$htmlaccountid = $accountidcheck;
			} else {
				$htmlaccountid = "";
			}
		} else {
			$htmlaccountid = "";
		}
		$selecttype = $this->Basefunctions->singlefieldfetch('selecttype','viewcreationcolumnid','viewcreationcolumns',$vcolid); //select type
		$CI = & get_instance();
		$CI->load->library('convert');  
		
		if($selecttype == 3) { //manualquery
			$selectquery = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',$vcolid); //selectquery
			$subprimary = 'salesdetailid'; 
			$data = $this->db->select('GROUP_CONCAT('.$subprimary.') as output')
			->from('salesdetail')
			->where_in('salesid',$primaryid)
			->where_in('stocktypeid',array(11,12,13,17,19,20,80,81))
			->get()
			->result();
			foreach($data as $info){
				$output = $info->output;
			}
			$output = explode(',',$output);
			for($l=0;$l<count($output);$l++) {
				$selectquery = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',$vcolid);
				$selectquery = str_replace("primaryid",$output[$l],$selectquery);
				$newjdata = $this->db->query($selectquery);
				if($newjdata->num_rows() > 0) {
					foreach($newjdata->result() as $jkey) {
						$manualset[$l] = $jkey->columdata;
					}
				}/*  else {
					$manualset[$l] = '0';
				} */
			}
			if(empty($manualset)) {
				$manualset = '0';
			}
		} else if($selecttype == 7) { // live html for billing etc 
			$tdhtmldata = '';
			$this->db->select('*');
			$this->db->from('livehtmltable');
			$this->db->where('livehtmltable.viewcreationcolumnid',$vcolid);
			$result = $this->db->get();
			foreach($result->result() as $rowdata) {
				
				$headerhtml = $rowdata->headerhtml;
				$tdhtml = $rowdata->tdhtml;
				$footterhtml = $rowdata->footterhtml;
				$headercolname = $rowdata->headercolname;
				$foottercolname = $rowdata->foottercolname;
				$tdquery = $rowdata->tdquery;
				$footterquery = $rowdata->footterquery;
		
				$headercolnamearray = explode(',',$headercolname);
				$foottercolnamearray = explode(',',$foottercolname);
				
				$tdquery = str_replace("dailydateformat",$fbetdate,$tdquery);
				$tdquery = str_replace("kkdailydatestart",$kkdailydatestart,$tdquery);
				$tdquery = str_replace("kkdailydateend",$kkdailydateend,$tdquery);
				$tdquery = str_replace("fromdate",$fromdatewhere,$tdquery);
				$tdquery = str_replace("roundamount",$roundamount,$tdquery);
				$tdquery = str_replace("roundweight",$roundweight,$tdquery);
				$tdquery = str_replace("roundrate",$roundrate,$tdquery);
				$tdquery = str_replace("roundmelting",$roundmelting,$tdquery);
				$tdquery = str_replace("primaryid",$primaryid,$tdquery);
				$tdquery = str_replace("htmlaccountid",$htmlaccountid,$tdquery);
				if(isset($mergerkeywords[0]))
				{
					$checkprimaryid = 0;
					for($i = 0;$i < count($mergerkeywords);$i++) {
						if($mergerkeywords[$i] != '' && $mergerkeyvalue[$i] != ' ') {
							$keywordtext = strtolower($mergerkeywords[$i]);
							$keyvaluetext = strtolower($mergerkeyvalue[$i]);
							$keyvaluetext = rtrim($keyvaluetext, " ");
							$keyvaluetext = ltrim($keyvaluetext, " ");
							$tdquery = str_replace($keywordtext,$keyvaluetext,$tdquery);
							$checkprimaryid++;
						}
					}
					if($checkprimaryid == 0) {
						$tdquery = '';
					}
				}
				if($tdquery != '') {
					$newjdata = $this->db->query($tdquery);
					if($newjdata->num_rows() > 0) {
						foreach($newjdata->result() as $jkey) {
							$tdhtmldataeach ='';
							$tdhtmldataeach = $tdhtml;
							for($i=0;$i<count($headercolnamearray);$i++) {
								$hcolnarray = $headercolnamearray[$i];
								$replacedata = $jkey->$hcolnarray;
								if($wordsconvert == 1){ 
									$replacedata = $CI->convert->numtowordconvert($replacedata,"Rupees","Paise");
								}
								$tdhtmldataeach = str_replace($headercolnamearray[$i],$replacedata,$tdhtmldataeach);
							}
							$tdhtmldata =  $tdhtmldata.' '.$tdhtmldataeach;
						}
					} else {
						$tdhtmldata = "";
					}
				} else {
					$tdhtmldata =  $tdhtmldata.' '.$tdhtml;
				}
				if($footterquery != '' && $tdhtmldata != ''){
				$footterquery = str_replace("dailydateformat",$fbetdate,$footterquery);
				$footterquery = str_replace("kkdailydatestart",$kkdailydatestart,$footterquery);
				$footterquery = str_replace("kkdailydateend",$kkdailydateend,$footterquery);
				$footterquery = str_replace("roundamount",$roundamount,$footterquery);
				$footterquery = str_replace("roundweight",$roundweight,$footterquery);
				$footterquery = str_replace("roundrate",$roundrate,$footterquery);
				$footterquery = str_replace("roundmelting",$roundmelting,$footterquery);
				$footterquery = str_replace("primaryid",$primaryid,$footterquery);
				$footterquery = str_replace("htmlaccountid",$htmlaccountid,$footterquery);
				if(isset($mergerkeywords[0]))
				{
					for($i = 0;$i < count($mergerkeywords);$i++) {
						$keywordtext = strtolower($mergerkeywords[$i]);
						$keyvaluetext = strtolower($mergerkeyvalue[$i]);
						$keyvaluetext = rtrim($keyvaluetext, " ");
						$keyvaluetext = ltrim($keyvaluetext, " ");
						$footterquery = str_replace($keywordtext,$keyvaluetext,$footterquery);
					}
				}
				$newjdatafootter = $this->db->query($footterquery);
					if($newjdatafootter->num_rows() > 0) {
						foreach($newjdatafootter->result() as $jkey) {
							$tdhtmldataeach ='';
							$tdhtmldataeach = $footterhtml;
							for($i=0;$i<count($foottercolnamearray);$i++) {
								$fcolnarray = $foottercolnamearray[$i];
								if($wordsconvert == 1){ 
									$jkey->$fcolnarray = $CI->convert->numtowordconvert($jkey->$fcolnarray,"Rupees","Paise");
								}
								$tdhtmldataeach = str_replace($foottercolnamearray[$i],$jkey->$fcolnarray,$tdhtmldataeach);
							}
							$tdhtmldata =  $tdhtmldata.' '.$tdhtmldataeach;
						} 
					}
				}
				else{ 
					if($tdhtmldata == ''){
						$tdhtmldata = '';
					}else{
						$tdhtmldata =  $tdhtmldata.' '.$footterhtml;
					}
				}
				if($tdhtmldata == ''){
					$tdhtmldata = '';
				}else{
					$tdhtmldata =  $headerhtml.' '.$tdhtmldata;
				}
				$tdhtmldata = str_replace("0000-00-00",'',$tdhtmldata); 
				$manualset[] = $tdhtmldata;
			}
		} else if($selecttype == 6) { //manualquery - stone detail display bill
			
			$selectquery = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',$vcolid);
				$selectquery = str_replace("primaryid",$primaryid,$selectquery);
				$newjdata = $this->db->query($selectquery);
				if($newjdata->num_rows() > 0) {
					foreach($newjdata->result() as $jkey) {
						$stdet = '';
						$stonede = array();
						$stonecwt = array();
						$stonert = array();
						$stoneta = array();
						$stonede = explode('*',$jkey->stonename);
						$stonecwt = explode('*',$jkey->caratweight);
						$stonert = explode('*',$jkey->stonerate);
						$stoneta = explode('*',$jkey->totalamount);
						
						if($stoneta[0] != 0)
						{
							
						$stdet ='<div style="text-align: left ;width:100%;"><div style="float: left;font-size: 12px;width:50%;">Stone Name </div>
							<div style="float: left;font-size: 12px;width:15%;">Carat Wt</div>
							<div style="float: left;font-size: 12px;width:15%;">Rate</div>
							<div style="float: left;font-size: 12px;width:20%;">Total Amt</div>';
						for($si=0;$si<count($stonede);$si++) {
							
							$stdet = $stdet.' <div style="float: left;font-size: 12px;width:50%;">'.$stonede[$si].' </div>
							<div style="float: left;font-size: 12px;width:15%;">'.$stonecwt[$si].'</div>
							<div style="float: left;font-size: 12px;width:15%;">'.$stonert[$si].'</div>
							<div style="float: left;font-size: 12px;width:20%;">'.$stoneta[$si].'</div>';
							
						}
							$stdet = $stdet.' <div style="float: left;font-size: 12px;width:50%;">Total </div>
							<div style="float: left;font-size: 12px;width:15%;">'.array_sum($stonecwt).'</div>
							<div style="float: left;font-size: 12px;width:15%;"> - </div>
							<div style="float: left;font-size: 12px;width:20%;">'.array_sum($stoneta).'</div>';
						  $stdet = $stdet.'</div>';
						  $manualset[] = $stdet;
						}
						 else {
						$manualset[] = '';
					}
				}
				} else {
					$manualset[] = '';
				}
				
		} else if($selecttype == 5) { //for daily stock report
			
			if($vcolid == 1536) {
				$manualset[] = $fromdate;
			} else if($vcolid == 4473) {
				$manualset[] =$this->getcashinhandamount($fdate,$tdate);
			} else if($vcolid == 4481) {
				$manualset[] =$this->getfinaldailyreprotamount($fdate,$tdate);
			}else {
				$selectquery = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',$vcolid); //selectquery
				$selectquery = str_replace("dailydateformat",$fbetdate,$selectquery);
				$newjdata = $this->db->query($selectquery);
				if($newjdata->num_rows() > 0) {
					foreach($newjdata->result() as $jkey) {
						if($jkey->columdata != null) {
							if($wordsconvert == 1){ 
								$jkey->columdata = $CI->convert->numtowordconvert($jkey->columdata,"Rupees","Paise");
							}
							$jkey->columdata = $jkey->columdata;
						} else {
							if($wordsconvert == 1){ 
							$jkey->columdata = '-';
							}
							$jkey->columdata = "0.00";
						}
						$manualset[] = $jkey->columdata;
					}
				} else {
					$manualset[] = '0';
				}
			}
		} else if($selecttype == 8) { //DIRECT FULL QUERY
			
			$selectquery = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',$vcolid); //selectquery
			$selectquery = str_replace("primaryid",$primaryid,$selectquery);
			$selectquery = str_replace("roundamount",$roundamount,$selectquery);
			$selectquery = str_replace("roundweight",$roundweight,$selectquery);
			$selectquery = str_replace("roundrate",$roundrate,$selectquery);
			$selectquery = str_replace("roundmelting",$roundmelting,$selectquery);
			$newjdata = $this->db->query($selectquery);
			if($newjdata->num_rows() > 0) {
				foreach($newjdata->result() as $jkey) {
					if($jkey->columdata != null) {
						if($wordsconvert == 1){ 
							$jkey->columdata = $CI->convert->numtowordconvert($jkey->columdata,"Rupees","Paise");
						}
						$jkey->columdata = $jkey->columdata;
					} else {
						$jkey->columdata = "";
					}
					$manualset[] = $jkey->columdata;
				}
			} else {
				$manualset[] = '';
			}
		}else if($selecttype == 10){ //html table generate based on db 
			
			$tdhtmldata = '';
			$this->db->select('*');
			$this->db->from('livehtmltable');
			$this->db->where('livehtmltable.viewcreationcolumnid',$vcolid);
			$result = $this->db->get();
			foreach($result->result() as $rowdata) {
				
				$headerhtml = $rowdata->headerhtml;
				$tdhtml = $rowdata->tdhtml;
				$footterhtml = $rowdata->footterhtml;
				$headercolname = $rowdata->headercolname;
				$foottercolname = $rowdata->foottercolname;
				$tdquery = $rowdata->tdquery;
				$footterquery = $rowdata->footterquery;
				$groupingcolname = $rowdata->groupingcolname;
				
				$headercolnamearray = explode(',',$headercolname);
				$foottercolnamearray = explode(',',$foottercolname);
				$groupingcolnamearray = explode(',',$groupingcolname);
				$tdquery = str_replace("dailydateformat",$fbetdate,$tdquery);
				$tdquery = str_replace("kkdailydatestart",$kkdailydatestart,$tdquery);
				$tdquery = str_replace("kkdailydateend",$kkdailydateend,$tdquery);
				$tdquery = str_replace("fromdate",$fromdatewhere,$tdquery);
				$tdquery = str_replace("roundamount",$roundamount,$tdquery);
				$tdquery = str_replace("roundweight",$roundweight,$tdquery);
				$tdquery = str_replace("roundrate",$roundrate,$tdquery);
				$tdquery = str_replace("roundmelting",$roundmelting,$tdquery);
				if($tdquery != ''){
				$newjdata = $this->db->query($tdquery);
					if($newjdata->num_rows() > 0) {
					foreach($newjdata->result() as $jkey) {
						$tdhtmldataeach ='';
						$nextdatastrong = '0';
						$lastrowstrong = '0';
						$tdhtmldataeach = $tdhtml;
						for($i=0;$i<count($headercolnamearray);$i++) {
							$hcolnarray = $headercolnamearray[$i];
							$hcolnarrayzero = $headercolnamearray[0];
							if($wordsconvert == 1){ 
								$jkey->$hcolnarray = $CI->convert->numtowordconvert($jkey->$hcolnarray,"Rupees","Paise");
							}
							if($lastrowstrong == '1')
							{
								$replacedata =  '<span style="font-size: 20px;"><strong>'.$jkey->$hcolnarray.'</strong></span>' ;
							}
							else if($nextdatastrong == '1')
							{
								$replacedata =  '<span style="font-size: 16px;"><strong>'.$jkey->$hcolnarray.'</strong></span>' ;
							}
							else{
								$replacedata = $jkey->$hcolnarray;
							}
								
						   $tdhtmldataeach = str_replace($headercolnamearray[$i],$replacedata,$tdhtmldataeach);
						   if($jkey->$hcolnarray == '' && in_array($headercolnamearray[$i],$groupingcolnamearray))
							{
								$nextdatastrong = '1';
							}
							if($jkey->$hcolnarrayzero == ''){
								$lastrowstrong = '1';
							}
						}
						$tdhtmldata =  $tdhtmldata.' '.$tdhtmldataeach;
					} 
					}
					else{
						$tdhtmldata = "";
					}
				}else{ 
					$tdhtmldata =  $tdhtmldata.' '.$tdhtml;
				}
				if($footterquery != '' && $tdhtmldata != ''){
				$footterquery = str_replace("dailydateformat",$fbetdate,$footterquery);
				$footterquery = str_replace("roundamount",$roundamount,$footterquery);
				$footterquery = str_replace("roundweight",$roundweight,$footterquery);
				$footterquery = str_replace("roundrate",$roundrate,$footterquery);
				$footterquery = str_replace("roundmelting",$roundmelting,$footterquery);
				$newjdatafootter = $this->db->query($footterquery);
					if($newjdatafootter->num_rows() > 0) {
						foreach($newjdatafootter->result() as $jkey) {
							$tdhtmldataeach ='';
							$tdhtmldataeach = $footterhtml;
							for($i=0;$i<count($foottercolnamearray);$i++) {
								$fcolnarray = $foottercolnamearray[$i];
								if($wordsconvert == 1){ 
									$jkey->$fcolnarray = $CI->convert->numtowordconvert($jkey->$fcolnarray,"Rupees","Paise");
								}
								$tdhtmldataeach = str_replace($foottercolnamearray[$i],$jkey->$fcolnarray,$tdhtmldataeach);
							}
							$tdhtmldata =  $tdhtmldata.' '.$tdhtmldataeach;
						} 
					}
				}
				else{ 
					if($tdhtmldata == ''){
						$tdhtmldata = '';
					}else{
						$tdhtmldata =  $tdhtmldata.' '.$footterhtml;
					}
				}
				if($tdhtmldata == ''){
					$tdhtmldata = '';
				}else{
					$tdhtmldata =  $headerhtml.' '.$tdhtmldata;
				}
				$manualset[] = $tdhtmldata;
			}
		}else if($selecttype == 11) { // for passing on daily report overlay form date to html
			$manualset[] = $fromdate;
		}else {
			/* $newjdata = $this->db->query('SELECT SQL_CALC_FOUND_ROWS  CONCAT(RPAD(productname,27," ")," ",RPAD(shortname,27," ")) as columdata  FROM `product` ORDER BY `productname` LIMIT 5 '); */
			$selectstmt =$this->Basefunctions->newgenerateselectstmt($vcolid,'PT','',''); 
			$fselect = explode(" as ",$selectstmt['Selectstmt']);
			if($mergegp != ''){
			   $selectstmt['Selectstmt'] = 'sum(('.$fselect[0].')) as columdata';
			}else{
			   $selectstmt['Selectstmt'] = '('.$fselect[0].') as columdata';
			}
			$addljoincolidfinal = explode(",",$addljoincolid);
			$addljoincolidfinal[0]=$vcolid;
			if(isset($print_data['join'])){
			 $join_stmt =$print_data['join'];
			// $join_stmt .= $this->Basefunctions->newgeneratejoinquery($moduleid,$addljoincolidfinal,$parenttable);
			}else{
				//$join_stmt = $this->Reportsmodel->generatejoinquery($moduleid,$addljoincolidfinal,$parenttable);
				$join_stmt = $this->Basefunctions->newgeneratejoinquery($moduleid,$addljoincolidfinal,$parenttable);
			}
			/* if(isset($print_data['where'])) {
				$newjdata = $this->db->query('SELECT SQL_CALC_FOUND_ROWS  '.$selectstmt['Selectstmt'].' from '.$parenttable.' '. $join_stmt .' WHERE '.$wherecond.' '.$parenttable.'.status NOT IN (0,3)'.$wheretext.' '.$mergegp.' '.$ordertext);
			} else {
				if(count($fselect) > 1) {
				$newjdata = $this->db->query('SELECT SQL_CALC_FOUND_ROWS  '.$selectstmt['Selectstmt'].' from '.$parenttable.' '. $join_stmt .' WHERE '.$wherecond.' '.$parenttable.'.status NOT IN (0,3) AND '.$parenttable.'.'.$parenttable.'id='.$primaryid .' '.$wheretext.' '.$mergegp.' '.$ordertext);
				} else if(count($fselect) == 1) {
					$empdata = explode(",",$fselect[0]);
					if($empdata[0] == 'employee.employeename') {
						$newjdata = $this->db->query('SELECT SQL_CALC_FOUND_ROWS  '.$fselect[0].' as columdata from '.$parenttable.' '. $join_stmt .' WHERE '.$wherecond.' '.$parenttable.'.status NOT IN (0,3) AND '.$parenttable.'.'.$parenttable.'id='.$primaryid .' '.$wheretext.' '.$mergegp.' '.$ordertext);
					} else {
						$newjdata = $this->db->query('SELECT SQL_CALC_FOUND_ROWS  '.$selectstmt['Selectstmt'].' from '.$parenttable.' '. $join_stmt .' WHERE '.$wherecond.' '.$parenttable.'.status NOT IN (0,3) AND '.$parenttable.'.'.$parenttable.'id='.$primaryid .' '.$wheretext.' '.$mergegp.' '.$ordertext);
					}
				}
			} */
			// Kumaresan - To retrieve all the records (used for notification & workflows)
			if(isset($print_data['where'])) {
				$newjdata = $this->db->query('SELECT SQL_CALC_FOUND_ROWS  '.$selectstmt['Selectstmt'].' from '.$parenttable.' '. $join_stmt .' WHERE '.$wherecond.' '.$parenttable.'.status NOT IN (100)'.$wheretext.' '.$mergegp.' '.$ordertext);
			} else {
				if(count($fselect) > 1) {
				$newjdata = $this->db->query('SELECT SQL_CALC_FOUND_ROWS  '.$selectstmt['Selectstmt'].' from '.$parenttable.' '. $join_stmt .' WHERE '.$wherecond.' '.$parenttable.'.status NOT IN (100) AND '.$parenttable.'.'.$parenttable.'id='.$primaryid .' '.$wheretext.' '.$mergegp.' '.$ordertext);
				} else if(count($fselect) == 1) {
					$empdata = explode(",",$fselect[0]);
					if($empdata[0] == 'employee.employeename') {
						$newjdata = $this->db->query('SELECT SQL_CALC_FOUND_ROWS  '.$fselect[0].' as columdata from '.$parenttable.' '. $join_stmt .' WHERE '.$wherecond.' '.$parenttable.'.status NOT IN (100) AND '.$parenttable.'.'.$parenttable.'id='.$primaryid .' '.$wheretext.' '.$mergegp.' '.$ordertext);
					} else {
						$newjdata = $this->db->query('SELECT SQL_CALC_FOUND_ROWS  '.$selectstmt['Selectstmt'].' from '.$parenttable.' '. $join_stmt .' WHERE '.$wherecond.' '.$parenttable.'.status NOT IN (100) AND '.$parenttable.'.'.$parenttable.'id='.$primaryid .' '.$wheretext.' '.$mergegp.' '.$ordertext);
					}
				}
			}
		}
		if($selecttype == 3  || $selecttype == 5 || $selecttype == 6 || $selecttype == 8 || $selecttype == 7){
			if($mergetextlength > 0){
				// display text based on text length
				$manualset = substr($manualset,0,$mergetextlength);
				$resultset = str_pad($manualset, $mergetextlength);
			}else {
				$resultset = $manualset;
				
			}
		} else if($selecttype == 4) {
			include_once './barcode.php';
			$barcode = '';
			foreach($newjdata->result() as $jkey) {
				if(isset($jkey->columdata)) {
					$barcode = code128BarCode(stripslashes($jkey->columdata));
					ob_start();
					imagepng($barcode);
					$output_img	= ob_get_clean();
					$barcode = '<img src="data:image/png;base64,' . base64_encode($output_img) . '" />'; 
				}
				$resultset[] = $barcode;
			}
		} else if($selecttype == 12) {
			include_once './phpqrcode/qrlib.php';
			$qrcode = '';
			foreach($newjdata->result() as $jkey) {
				if(isset($jkey->columdata)) {
					ob_start();
					imagepng(QRcode::png(stripslashes($jkey->columdata)));
					$output_img	= ob_get_clean();
					$qrcode = '<img src="data:image/png;base64,'.base64_encode($output_img).'" />';
				}
				$resultset[] = $qrcode;
			}
		} else if($selecttype == 10) {
			$resultset = $manualset;
		}  
		else if($selecttype == 11) {
			$resultset = $manualset;
		} else {
			if($newjdata->num_rows() >0) { 
				foreach($newjdata->result() as $jkey) {
					if(isset($jkey->columdata)) {
						if (is_null($jkey->columdata) || $jkey->columdata == "") {
							$jkey->columdata = "";
						}
						/* if($vcolid != 4192) {
							if($jkey->columdata == '0') {
								$jkey->columdata = "";
							}
						} */
						if($vcolid == '1883' || $vcolid == '4442' || $vcolid == '2' || $wordsconvert == 1) {
                            $jkey->columdata = $CI->convert->numtowordconvert($jkey->columdata,"Rupees","Paise");
                        }
						//print_r($mergetextlength);die();
						if($mergetextlength > 0){
							// display text based on text length
							$jkey->columdata = substr($jkey->columdata,0,$mergetextlength);
							$resultset[] = str_pad($jkey->columdata, $mergetextlength);
						}else {
							$resultset[] = $jkey->columdata;
						}
					} else {
						$resultset[] ='';
					}
				}
			} else {
				$resultset[] = " ";
			}
		}
		return $resultset; 
	}
	//remove special chars
	public function removespecialchars($content) {
		$symbols = array('+','-','/','%','~','!','@','#','$','^','&','*','_','=','|','(',')');
		foreach($symbols as $symbol) {
			switch($symbol) {
				case '(':
					$content = preg_replace('~\(~','890sss', $content);
					break;
				case ')':
					$content = preg_replace('~\)~','sss890', $content);
					break;
				case '+':
					$content = preg_replace('~\+~','ssplus890', $content);
					break;
				case '~':
					$content = preg_replace('~\~~','ssstilt890', $content);
					break;
				case '!':
					$content = preg_replace('~\!~','sssastr890', $content);
					break;
				case '@':
					$content = preg_replace('~\@~','sssat890', $content);
					break;
				case '#':
					$content = preg_replace('~\#~','ssshash890', $content);
					break;
				case '^':
					$content = preg_replace('~\^~','ssscap890', $content);
					break;
				case '*':
					$content = preg_replace('~\*~','sssstar890', $content);
					break;
				case '=':
					$content = preg_replace('~\=~','ssseqw890', $content);
					break;
				case '|':
					$content = preg_replace('~\|~','ssspipe890', $content);
					break;
			}
		}
		return $content;
	}
	//remove special chars
	public function addspecialchars($content) {
		$symbols = array('+','-','/','%','~','!','@','#','$','^','&','*','_','=','|','(',')');
		foreach($symbols as $symbol) {
			switch($symbol) {
				case '(':
					$content = preg_replace('~890sss~','(', $content);
					break;
				case ')':
					$content = preg_replace('~sss890~',')', $content);
					break;
				case '+':
					$content = preg_replace('~ssplus890~','+', $content);
					break;
				case '~':
					$content = preg_replace('~ssstilt890~','~', $content);
					break;
				case '!':
					$content = preg_replace('~sssastr890~','!', $content);
					break;
				case '@':
					$content = preg_replace('~sssat890~','@', $content);
					break;
				case '#':
					$content = preg_replace('~ssshash890~','#', $content);
					break;
				case '^':
					$content = preg_replace('~ssscap890~','^', $content);
					break;
				case '*':
					$content = preg_replace('~sssstar890~','*', $content);
					break;
				case '=':
					$content = preg_replace('~ssseqw890~','=', $content);
					break;
				case '|':
					$content = preg_replace('~ssspipe890~','|', $content);
					break;
			}
		}
		return $content;
	}
	//get editor file content
	public function geteditorfilecontent($fname) {
		$tccontent="";
		$newfilename = explode('/',$fname);
		if(read_file($newfilename[0].DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = read_file($newfilename[0].DIRECTORY_SEPARATOR.$newfilename[1]);
		}
		return $tccontent;
	}
	//table name check
	public function tableexitcheck($database,$newtable) {
		$counttab=0;
		$tabexits = $this->db->query("SELECT COUNT(*) AS tabcount FROM information_schema.tables WHERE table_schema = '".$database."'AND table_name = '".$newtable."'");
		foreach($tabexits->result() as $tkey) { 
			$counttab = $tkey->tabcount;
		};
		return $counttab;
	}
	//relation module ids
	public function fetchrelatedmodulelist($moduleid) {
		$relmodids = array();
		$relmoduleids = $this->db->select('modulerelationid,relatedmoduleid')->from('modulerelation')->where('moduleid',$moduleid)->get();
		foreach($relmoduleids->result() as $reldata) {
			$relmodids[] = $reldata->relatedmoduleid;
		}
		return $relmodids;
	}
	//fetch parent table information
	public function fetchfieldparenttable($tablname,$fieldname) {
		$pardata = array();
		$datas = $this->db->query('SELECT modulefieldid,uitypeid,parenttable,moduletabid  FROM modulefield WHERE columnname LIKE "%'.$fieldname.'%" AND tablename LIKE "%'.$tablname.'%" AND status IN (1,10)',false);
		foreach($datas->result() as $data) {
			$pardata = array('uitype'=>$data->uitypeid,'partab'=>$data->parenttable,'modid'=>$data->moduletabid);
		}
		return $pardata;
	}
	//fetch main table join information
	public function fetchjoincolmodelname($relpartable,$moduleid) {
		$joindata = array();
		$datas = $this->db->query('SELECT modulerelationid,parentfieldname,parenttable FROM modulerelation WHERE moduleid = '.$moduleid.' AND relatedtable LIKE "%'.$relpartable.'%" AND status=1',false);
		foreach($datas->result() as $data) {
			$joindata = array('relid'=>$data->modulerelationid,'joinfieldname'=>$data->parentfieldname,'jointabl'=>$data->parenttable);
		}
		return $joindata;
	}
	//fetch relation and view type 3 field information
	public function viewfieldsinformation($tablename,$tabfield,$modid) {
		$datasets = array();
		$viewfieldinfo = $this->db->query('SELECT viewcreationcolumnid,viewcreationparenttable,viewcreationjoincolmodelname,viewcreationcolmodelcondname FROM viewcreationcolumns WHERE viewcreationparenttable LIKE "%'.$tablename.'%" AND viewcreationcolmodelcondname LIKE "%'.$tabfield.'%" AND viewcreationtype=3 AND viewcreationmoduleid='.$modid.' AND status=1');
		foreach($viewfieldinfo->result() as $viewdata) {
			$datasets = array('viewcolid'=>$viewdata->viewcreationcolumnid,'joinfieldname'=>$viewdata->viewcreationjoincolmodelname,'condfiledname'=>$viewdata->viewcreationcolmodelcondname);
		}
		return $datasets;
	}
	//fetch currency information
	public function currencyinformation($parenttable,$id,$moduleid) {
		$datasets = array();
		$colmodeltable = "";
		$cid = 1;
		//modulerelation
		$datasets = $this->db->select('viewcreationcolumnid,viewcreationcolmodeltable,viewcreationjoincolmodelname,viewcreationparenttable,viewcreationtype,viewcreationcolmodelcondname',false)->from('viewcreationcolumns')->where('viewcreationmoduleid',$moduleid)->where('viewcreationjoincolmodelname','currencyid')->where('status',1)->get();
		if($datasets->num_rows()>0) {
			foreach($datasets->result() as $data) {
				$colname =  $data->viewcreationcolmodeltable;
				$colmodeltable = $data->viewcreationcolmodeltable;
				$joincolmodelname= $data->viewcreationjoincolmodelname;
				$colmodelpartable = $data->viewcreationparenttable;
				$viewtype = $data->viewcreationtype;
				$viewcolcondname = $data->viewcreationcolmodelcondname;
			}
		}
		if($colmodeltable!= "") {
			$joinq="";
			if($viewtype==3) {
				if($colmodelpartable!="currency") {
					if($colmodelpartable == $parenttable) {
						$joinq = 'LEFT OUTER JOIN '.$colmodelpartable.' ON '.$colmodelpartable.'.'.$viewcolcondname.'='.$colmodeltable.'.'.$joincolmodelname;
					} else {
						$joinq = 'LEFT OUTER JOIN '.$colmodelpartable.' ON '.$colmodelpartable.'.'.$viewcolcondname.'='.$colmodeltable.'.'.$joincolmodelname;
						$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$colmodelpartable.'.'.$parenttable.'id';
					}
				}
			} else {
				if($colmodelpartable!="currency") {
					if($colmodelpartable == $parenttable) {
						$joinq = 'LEFT OUTER JOIN '.$colmodelpartable.' ON '.$colmodelpartable.'.'.$joincolmodelname.'='.$colmodeltable.'.'.$joincolmodelname;
					} else {
						$joinq = 'LEFT OUTER JOIN '.$colmodelpartable.' ON '.$colmodelpartable.'.'.$joincolmodelname.'='.$colmodeltable.'.'.$joincolmodelname;
						$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$colmodelpartable.'.'.$parenttable.'id';
					}
				}
			}
			$curdatasets = $this->db->query('select currency.currencyid from currency '.$joinq.' where '.$parenttable.'id='.$id.'')->result();
			foreach($curdatasets as $data) {
				$cid = $data->currencyid;
			}
		} else {
			$empid = $this->Basefunctions->userid;
			$compquery = $this->db->query('select company.companyid,company.currencyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.'')->result();
			foreach($compquery as $key) {
				$cid = $key->currencyid;
			}
		}
		if($cid == '' || $cid == 1) {
			$empid = $this->Basefunctions->userid;
			$compquery = $this->db->query('select company.companyid,company.currencyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.'')->result();
			foreach($compquery as $key) {
				$cid = $key->currencyid;
			}
		}
		$curdatasets = $this->db->select('currencyid,currencyname,currencycode,symbol,currencycountry,subunit',false)->from('currency')->where('currencyid',$cid)->get();
		foreach($curdatasets->result() as $datas) {
			$datasets = array('id'=>$datas->currencyid,'name'=>$datas->currencyname,'code'=>$datas->currencycode,'symbol'=>$datas->symbol,'country'=>$datas->currencycountry,'subcode'=>$datas->subunit);
		}
		return $datasets;
	}
	//read file
	public function filecontentfetch($filename) {
		$filecontent = "";
		if( file_exists($filename) && is_readable($filename) ) {
			$newfilename = explode('/',$filename);
			$myfile = @fopen('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1], "r");
			$filecontent = fread($myfile,filesize('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]));
			fclose($myfile);
		}
		return $filecontent;
	}
	//print template unique name check
	public function printtemplateuniquenamemodel() {
		$printtempname = $_POST['printtempname'];
		if($printtempname != "") {
			$result = $this->db->select('printtemplates.printtemplatesid')->from('printtemplates')->where('printtemplatesname',$printtempname)->where('printtemplates.status',1)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->printtemplatesid;
				}
			} else { echo "False"; }
		} else { echo "False"; }
	}
	//print template folder unique name check
	public function printtemplatefolderuniquenamemodel() {
		$foldername = $_POST['foldername'];
		if($foldername != "") {
			$result = $this->db->select('foldername.foldernameid')->from('foldername')->where('foldernamename',$foldername)->where('foldername.moduleid',251)->where('foldername.status',1)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->foldernameid;
				}
			} else { echo "False"; }
		} else {
			echo "False";
		}
	}
	public function getformatdata() {
		$format = trim($_POST['fomattype']);
		$formatdata=$this->db->where('pageformatid',$format)->get('pageformat');
		if($formatdata->num_rows() > 0) {
			$format_row=$formatdata->row();
			$pageformat_array=array(
					'height'=>$format_row->height,
					'width'=>$format_row->width
			);
		} else {
			$pageformat_array='NO';
		}
		echo json_encode($pageformat_array);
	}
	
	public function whereclauseload(){
		$printfield=[];
		$moduleid=$_POST['moduleid'];
		$this->db->select('whereclauseid,whereclausecreationid,moduleid,description');
		$this->db->from('whereclausecreation');
		$this->db->where('whereclausecreation.moduleid',$moduleid);
		$this->db->where('whereclausecreation.status',$this->Basefunctions->activestatus);
		$data = $this->db->get()->result();
		foreach($data as $info)
		{
			$printfield[] = array(
					'pid'=>$info->whereclausecreationid,
					'id'=>$info->whereclauseid,
			);
		}
		echo json_encode($printfield);
	}
	public function snmergcontinformationfetchmodel($mergegrp,$parenttable,$id,$moduleid) {
		$relmodids = $this->fetchrelatedmodulelist($moduleid);
		$primaryid = $id;
		$database = $this->db->database;
		$resultset = array();
		$mergeindval = $mergegrp;
		$formatortype = "1";
		$id = $primaryid;
		//parent child field concept
		$elname = array('parentcategoryid','parentstoragecategoryid','parentaccountid');
		$fname = array('categoryname','storagecategoryname','accountname');
		$fnameid = array('categoryid','storagecategoryid','accountid');
		$tname = array('category','storagecategory','account');
		$empid = $this->Basefunctions->userid;
		$newdata = array();
		$tempval = substr($mergeindval,1,(strlen($mergeindval)-2));
		$newval = explode('.',$tempval);
		$reltabname = explode(':',$newval[0]);
		$table = $reltabname[0];
		$uitypeid = 2;
		//chk editor field name
		$chkeditfname = explode('_',$newval[1]);
		$editorfname = ( (count($chkeditfname) >= 2)? $chkeditfname[1] : '');
		if($table!='REL' && $table!='GEN') { //parent table fields information
			$tablename = trim($reltabname[0]);
			$tabfield = $newval[1];
			//field fetch parent table fetch
			$fieldnamechk = explode('-',$tabfield);
			$fldname = ( (count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
			$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
			$uitypeid = ( (count($fieldinfo) > 0)? $fieldinfo['uitype'] : 1);
			if($fldname != 'attributesetid') {
				//get user company & branch id
				if($tablename == 'company' || $tablename == 'branch') {
					if($tablename == 'company') {
						$compquery = $this->db->query('select company.companyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND company.status=1')->result();
						foreach($compquery as $key) {
							$id = $key->companyid;
						}
					} else if($tablename == 'branch') {
						$brquery = $this->db->query('select branch.branchid from branch join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND branch.status=1')->result();
						foreach($brquery as $key) {
							$id = $key->branchid;
						}
					}
				}
				//check its parent child field concept
				if(in_array($fldname,$elname)) {
					$key = array_search($fldname,$elname);
					$pid = $id;
					if($key != NULL) {
						//parent id get
						$result = $this->db->select("$tname[$key].$elname[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$id)->where($tname[$key].'.status',1)->get();
						if($result->num_rows() > 0) {
							foreach($result->result()as $row) {
								$pid=$row->$elname[$key];
							}
							if($pid!=0) {
								//parent name
								$result = $this->db->select("$tname[$key].$fname[$key],$tname[$key].$fnameid[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$pid)->where($tname[$key].'.status',1)->get();
								if($result->num_rows() > 0) {
									foreach($result->result()as $row) {
										$resultset[]=$row->$fname[$key];
									}
								}
							}
						}
					}
				}
				else {
					//field fetch parent table fetch
					$fieldnamechk = explode('-',$tabfield);
					$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
					$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
					if(count($fieldinfo)>0) {
						$uitypeid = $fieldinfo['uitype'];
						//fetch parent table join id
						$relpartable = $fieldinfo['partab'];
						$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
						$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
						$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
						//fetch original field of picklist data in view creation
						$viewfieldinfo = $this->viewfieldsinformation($tablename,$tabfield,$fieldinfo['modid']);
						$tabfieldname = ( (count($viewfieldinfo)>0)? $viewfieldinfo['joinfieldname'] : $tabfield );
						$jointabfieldid = ( (count($viewfieldinfo)>0)? $viewfieldinfo['condfiledname'] : $tabfield );
						//join relation table and main table
						$joinq="";
						if($relpartable!=$tablename) {
							$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id AND '.$relpartable.'.status=1';
						}
						//main table join
						if($mainjointable!=$parenttable) {
							$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id AND '.$relpartable.'.status=1';
						}
						//if its picklist data
						$newfield = substr( $tabfieldname,( strlen($tabfieldname)-2 ));
						$tabname = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
						$tbachk = $this->tableexitcheck($database,$tabname);
						if( $newfield == "id" && $tbachk != 0 ) {
							$newtable = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
							$newjfield = $newtable."name";
							$whtable = (($tablename=="company" || $tablename=="branch")? $tablename : $parenttable);
							$newjdata = $this->db->query("SELECT ".$newtable.".".$newtable."id AS columdataid,".$newtable.".".$newjfield." AS columdata FROM ".$newtable." LEFT OUTER JOIN ".$tablename." ON ".$tablename.".".$tabfieldname." = ".$newtable.".".$tabfieldname."".$joinq." WHERE ".$tablename.".".$whtable."id = ".$id." AND ".$newtable.".status=1 AND ".$tablename.".status=1",false);
							if($newjdata->num_rows() >0) {
								foreach($newjdata->result() as $jkey) {
									$resultset[] = $jkey->columdata;
								}
							} else {
								$resultset[] = " ";
							}
						}
						else {
							//main table data sets information fetching
							$fldcountchk = explode('-',$tabfield);
							$tablcolumncheck = $this->tablefieldnamecheck($tablename,$parenttable."id");
							if( ( $parenttable == $tablename && (count($fldcountchk)) < 2 ) || ( $tablcolumncheck == 'false' && (count($fldcountchk)) < 2 ) ) {
								$newdata = $this->db->select("".$tabfield." AS columdata")->from($tablename)->where($tablename.".".$tablename."id",$id)->where($tablename.".status",1)->get()->result();
							}
							else {
								$cond = "";
								$addfield = explode('-',$tabfield);
								if( (count($addfield)) >= 2 ) {
									$datafiledname=$addfield[1];
									if(in_array('PR',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=2';
									} else if(in_array('SE',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=3';
									} else if(in_array('BI',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=4';
									} else if(in_array('SH',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=5';
									} else if(in_array('CW',$addfield)) {
										$formatortype = "CW";
									} else if(in_array('CF',$addfield)) {
										$formatortype = "CF";
									} else if(in_array('CS',$addfield)) {
										$formatortype = "CS";
									}
									if($tablename!=$parenttable) {
										$tablcolumncheck = $this->tablefieldnamecheck($tablename,"addresstypeid");
										$cond = ( ($tablcolumncheck == 'true')? $cond : ' ');
										$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename." JOIN ".$parenttable." ON ".$parenttable.".".$parenttable."id = ".$tablename.".".$parenttable."id WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1".$cond."")->result();
									} else {
										$newdata = $this->db->select(''.$datafiledname.' AS columdata',false)->from($tablename)->where($tablename.'id',$id)->where($tablename.'.status',1)->get()->result();
									}
								}
								else {
									$newdata = $this->db->query("SELECT ".$tabfield." AS columdata FROM ".$tablename." JOIN ".$parenttable." ON ".$parenttable.".".$parenttable."id = ".$tablename.".".$parenttable."id WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1")->result();
								}
							}
							foreach($newdata as $key) {
								if($tabfield == 'companylogo') {
									$imgname = $key->columdata;
									if(filter_var($imgname,FILTER_VALIDATE_URL)) {
										$resultset[] = '<img src="'.$imgname.'" />';
									} else {
										if( file_exists($imgname) ) {
											$resultset[] = '<img src="'.$imgname.'" style="width:11em; height:3em" />';
										} else {
											$resultset[] = " ";
										}
									}
								}
								else {
									$currencyinfo = $this->currencyinformation($parenttable,$id,$moduleid);
									if($formatortype=="CW") { //convert to word
										if(count($currencyinfo)>0) {
											$resultset[] = $this->convert->numtowordconvert($key->columdata,$currencyinfo['name'],$currencyinfo['subcode']);
										} else {
											$resultset[] = $this->convert->numtowordconvert($key->columdata);
										}
									} else if($formatortype=="CF") { //currency format
										if(count($currencyinfo)>0) {
											$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
										} else {
											$resultset[] = $this->convert->formatcurrency($key->columdata);
										}
									} else if($formatortype=="CS") { //currency with symbol
										if(count($currencyinfo)>0) {
											$amt = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
											$resultset[] = $currencyinfo['symbol'].' '.$amt;
										} else {
											$resultset[] = $currencyinfo['symbol'].' '.$this->convert->formatcurrency($key->columdata);
										}
									} else {
										if(is_numeric($key->columdata)) {
											if($uitypeid == '4' || $uitypeid == '5' || $uitypeid == '7') {
												if(count($currencyinfo)>0) {
													$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
												} else {
													$resultset[] = $this->convert->formatcurrency($key->columdata);
												}
											} else {
												$resultset[] = $key->columdata;
											}
										} else {
											if($editorfname=='editorfilename') {
												if( file_exists($key->columdata) ) {
													$datas = $this->geteditorfilecontent($key->columdata);
													$datas = preg_replace('~a10s~','&nbsp;', $datas);
													$datas = preg_replace('~<br>~','<br />', $datas);
													$resultset[] = $datas;
												} else {
													$resultset[] = '';
												}
											} else {
												$resultset[] = $key->columdata;
											}
										}
									}
								}
							}
						}
					}
				}
			}
			else {
				//attribute value fetch for non relational modules
				$fieldnamechk = explode('-',$tabfield);
				$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
				$fieldinfo = $this->fetchfieldparenttable($table,$fldname);
				if(count($fieldinfo)>0) {
					$uitypeid = $fieldinfo['uitype'];
					//fetch parent table join id
					$relpartable = $fieldinfo['partab'];
					$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
					$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
					$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
					//join relation table and main table
					$joinq="";
					if($parenttable!=$table) {
						$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$table.'.'.$parenttable.'id';
					}
					//fetch attribute name and values
					$i=0;
					$datasets = array();
					$attrsetids = $this->db->query(" SELECT ".$table.".".$table."id as Id,".$table.".attributesetid as attrsetid,".$table.".attributes as attributeid,".$table.".attributevalues as attributevalueid FROM ".$table.$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$mainjointable.".status=1 AND ".$table.".status=1",false)->result();
					foreach($attrsetids as $attrset) {
						$datasets[$i] = array('id'=>$attrset->Id,'attrsetid'=>$attrset->attrsetid,'attrnames'=>$attrset->attributeid,'attrvals'=>$attrset->attributevalueid);
						$i++;
					}
					$resultset = array();
					foreach($datasets as $set) {
						$valid = explode('_',$set['attrvals']);
						$nameid = explode('_',$set['attrnames']);
						$n=1;
						$vi = 0;
						$attrdatasets = array();
						for($h=0;$h<count($valid);$h++) {
							$attrval = '';
							$attrname = '';
							if($valid[$h]!='') {
								//attribute value fetch
								$attrvaldata = $this->db->query(' SELECT attributevalueid,attributevaluename FROM attributevalue WHERE attributevalueid='.$valid[$h].' AND status=1',false)->result();
								foreach($attrvaldata as $valsets) {
									$attrval = $valsets->attributevaluename;
								}
								//attribute name fetch
								$attrnamedata = $this->db->query(' SELECT attributeid,attributename FROM attribute WHERE attributeid='.$nameid[$h].' AND status=1',false)->result();
								foreach($attrnamedata as $namesets) {
									$attrname = $namesets->attributename;
								}
							}
							$attrset = "";
							if( $attrval!='' ) {
								$attrset = $n.'.'.$attrname.': '.$attrval;
								++$n;
							}
							$attrdatasets[$vi] = $attrset;
							$vi++;
						}
						$resultset[] = implode('<br />',$attrdatasets);
					}
				}
			}
		}
		else if($table=='REL') { //related module fields information fetch
			$tablename = trim($reltabname[1]);
			$tabfield = trim($newval[1]);
			$fieldnamechk = explode('-',$tabfield);
			$fldname = ( (count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
			if($fldname != 'attributesetid') {
				//get user company & branch id
				if($tablename == 'company' || $tablename == 'branch') {
					if($tablename == 'company') {
						$compquery = $this->db->query('select company.companyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND company.status=1')->result();
						foreach($compquery as $key) {
							$id = $key->companyid;
						}
					} else if($tablename == 'branch') {
						$brquery = $this->db->query('select branch.branchid from branch join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND branch.status=1')->result();
						foreach($brquery as $key) {
							$id = $key->branchid;
						}
					}
				}
				//check its parent child field concept
				$fldnamechk = explode('-',$tabfield);
				$fldname = ( (count($fldnamechk) >= 2)? $fldnamechk[1] : $tabfield);
				if(in_array($fldname,$elname)) {
					$key = array_search($fldname,$elname);
					$pid = $id;
					if($key != NULL) {
						//parent id get
						$result = $this->db->select("$tname[$key].$elname[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$id)->where($tname[$key].'.status',1)->get();
						if($result->num_rows() > 0) {
							foreach($result->result()as $row) {
								$pid=$row->$elname[$key];
							}
							if($pid!=0) {
								//parent name
								$result = $this->db->select("$tname[$key].$fname[$key],$tname[$key].$fnameid[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$pid)->where($tname[$key].'.status',1)->get();
								if($result->num_rows() > 0) {
									foreach($result->result()as $row) {
										$resultset[]=$row->$fname[$key];
									}
								}
							}
						}
					}
				}
				else {
					//field fetch parent table fetch
					$fieldnamechk = explode('-',$tabfield);
					$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
					$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
					if(count($fieldinfo)>0) {
						$uitypeid = $fieldinfo['uitype'];
						//fetch parent table join id
						$relpartable = $fieldinfo['partab'];
						$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
						$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
						$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
						//fetch original field of picklist data in view creation
						$viewfieldinfo = $this->viewfieldsinformation($tablename,$tabfield,$fieldinfo['modid']);
						$tabfieldname = ( (count($viewfieldinfo)>0)? $viewfieldinfo['joinfieldname'] : $tabfield );
						$jointabfieldid = ( (count($viewfieldinfo)>0)? $viewfieldinfo['condfiledname'] : $tabfield );
						//join relation table and main table
						$joinq="";
						if($relpartable!=$tablename) {
							$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id AND '.$relpartable.'.status=1';
						}
						//main table join
						if($mainjointable==$parenttable) {
							$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id AND '.$mainjointable.'.status=1';
						} else {
							$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id AND '.$mainjointable.'.status=1';
							$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$mainjointable.'.'.$parenttable.'id AND '.$parenttable.'.status=1';
						}
						//if its picklist data
						$newfield = substr( $tabfieldname,( strlen($tabfieldname)-2 ));
						$tabname = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
						$tblchk = $this->tableexitcheck($database,$tabname);
						if( $newfield == "id" && $tblchk != 0 ) {
							$newtable = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
							$newjfield = $newtable."name";
							$whtable = (($tablename=="company" || $tablename=="branch")? $tablename : $parenttable);
							$newjdata = $this->db->query("SELECT ".$newtable.".".$newtable."id AS columdataid,".$newtable.".".$newjfield." AS columdata FROM ".$newtable." LEFT OUTER JOIN ".$tablename." ON ".$tablename.".".$jointabfieldid." = ".$newtable.".".$tabfieldname."".$joinq." WHERE ".$parenttable.".".$whtable."id = ".$id." AND ".$mainjointable.".status=1 AND ".$newtable.".status=1 AND ".$tablename.".status=1");
							if($newjdata->num_rows() >0) {
								foreach($newjdata->result() as $jkey) {
									$resultset[] = $jkey->columdata;
								}
							} else {
								$resultset[] = " ";
							}
						}
						else {
							//main table data sets information fetching
							$fldcountchk = explode('-',$tabfield);
							$tablcolumncheck = $this->tablefieldnamecheck($tablename,$parenttable."id");
							if( ( $parenttable == $tablename && (count($fldcountchk)) < 2 ) || ( $tablcolumncheck == 'false' && (count($fldcountchk)) < 2 ) ) {
								$newdata = $this->db->query("SELECT ".$tablename.".".$tabfield." AS columdata FROM ".$tablename."".$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1")->result();
							}
							else {
								$cond = "";
								$addfield = explode('-',$tabfield);
								if( (count($addfield)) >= 2 ) {
									$datafiledname=$addfield[1];
									if(in_array('PR',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=2';
									} else if(in_array('SE',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=3';
									} else if(in_array('BI',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=4';
									} else if(in_array('SH',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=5';
									} else if(in_array('CW',$addfield)) {
										$formatortype = "CW";
									} else if(in_array('CF',$addfield)) {
										$formatortype = "CF";
									} else if(in_array('CS',$addfield)) {
										$formatortype = "CS";
									}
									$tablcolumncheck = $this->tablefieldnamecheck($tablename,"addresstypeid");
									$cond = ( ($tablcolumncheck == 'true')? $cond : ' ');
									if($tablename!=$parenttable) {
										$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename."".$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1".$cond."")->result();
									} else {
										$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename." WHERE ".$tablename."id = ".$id." AND ".$tablename.".status=1")->result();
									}
								}
								else {
									$newdata = $this->db->query("SELECT ".$tabfield." AS columdata FROM ".$tablename."".$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1".$cond."")->result();
								}
							}
							foreach($newdata as $key) {
								if($tabfield == 'companylogo') {
									$imgname = $key->columdata;
									if(filter_var($imgname, FILTER_VALIDATE_URL)) {
										$resultset[] = '<img src="'.$imgname.'" />';
									} else {
										if( file_exists($imgname) ) {
											$resultset[] = '<img src="'.$imgname.'" style="width:11em; height:3em" />';
										} else {
											$resultset[] = " ";
										}
									}
								}
								else {
									$currencyinfo = $this->currencyinformation($parenttable,$id,$moduleid);
									if($formatortype=="CW") { //convert to word
										if(count($currencyinfo)>0) {
											$resultset[] = $this->convert->numtowordconvert($key->columdata,$currencyinfo['name'],$currencyinfo['subcode']);
										} else {
											$resultset[] = $this->convert->numtowordconvert($key->columdata);
										}
									} else if($formatortype=="CF") { //currenct format
										if(count($currencyinfo)>0) {
											$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
										} else {
											$resultset[] = $this->convert->formatcurrency($key->columdata);
										}
									} else if($formatortype=="CS") { //currency with symbol
										if(count($currencyinfo)>0) {
											$amt = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
											$resultset[] = $currencyinfo['symbol'].' '.$amt;
										} else {
											$resultset[] = $currencyinfo['symbol'].' '.$this->convert->formatcurrency($key->columdata);
										}
									} else {
										if(is_numeric($key->columdata)) {
											if( $uitypeid == '4' || $uitypeid == '5' || $uitypeid == '7' ) {
												if(count($currencyinfo)>0) {
													$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
												} else {
													$resultset[] = $this->convert->formatcurrency($key->columdata);
												}
											} else {
												$resultset[] = $key->columdata;
											}
										} else {
											if($editorfname=='editorfilename') {
												if( file_exists($key->columdata) ) {
													$datas = $this->geteditorfilecontent($key->columdata);
													$datas = preg_replace('~a10s~','&nbsp;', $datas);
													$datas = preg_replace('~<br>~','<br />', $datas);
													$resultset[] = $datas;
												} else {
													$resultset[] = '';
												}
											} else {
												$resultset[] = $key->columdata;
											}
										}
									}
								}
							}
						}
					}
				}
			}
			else {
				//attribute value fetch
				$fieldnamechk = explode('-',$tabfield);
				$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
				$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
				if(count($fieldinfo)>0) {
					$uitypeid = $fieldinfo['uitype'];
					//fetch parent table join id
					$relpartable = $fieldinfo['partab'];
					$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
					$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
					$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
					//join relation table and main table
					$joinq="";
					if($relpartable!=$tablename) {
						$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id';
					}
					//main table join
					if($mainjointable==$parenttable) {
						$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id';
					} else {
						$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id';
						$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$mainjointable.'.'.$parenttable.'id';
					}
					//fetch attribute name and values
					$i=0;
					$datasets = array();
					$attrsetids = $this->db->query(" SELECT ".$tablename.".".$tablename."id as Id,".$tablename.".attributesetid as attrsetid,".$tablename.".attributes as attributeid,".$tablename.".attributevalues as attributevalueid FROM ".$tablename.$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$mainjointable.".status=1 AND ".$tablename.".status=1",false)->result();
					foreach($attrsetids as $attrset) {
						$datasets[$i] = array('id'=>$attrset->Id,'attrsetid'=>$attrset->attrsetid,'attrnames'=>$attrset->attributeid,'attrvals'=>$attrset->attributevalueid);
						$i++;
					}
					$resultset = array();
					foreach($datasets as $set) {
						$valid = explode('_',$set['attrvals']);
						$nameid = explode('_',$set['attrnames']);
						$n=1;
						$vi = 0;
						$attrdatasets = array();
						for($h=0;$h<count($valid);$h++) {
							$attrval = '';
							$attrname = '';
							if($valid[$h]!='') {
								//attribute value fetch
								$attrvaldata = $this->db->query(' SELECT attributevalueid,attributevaluename FROM attributevalue WHERE attributevalueid='.$valid[$h].' AND status=1',false)->result();
								foreach($attrvaldata as $valsets) {
									$attrval = $valsets->attributevaluename;
								}
								//attribute name fetch
								$attrnamedata = $this->db->query(' SELECT attributeid,attributename FROM attribute WHERE attributeid='.$nameid[$h].' AND status=1',false)->result();
								foreach($attrnamedata as $namesets) {
									$attrname = $namesets->attributename;
								}
							}
							$attrset = "";
							if( $attrval!='' ) {
								$attrset = $n.'.'.$attrname.': '.$attrval;
								++$n;
							}
							$attrdatasets[$vi] = $attrset;
							$vi++;
						}
						$resultset[] = implode('<br />',$attrdatasets);
					}
				}
			}
		}
		else if($table=='GEN') {
			$tablename = trim($reltabname[1]);
			$tabfield = $newval[1];
			//field fetch parent table fetch
			$fieldnamechk = explode('-',$tabfield);
			$fldname = ( (count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
			$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
			$uitypeid = ( (count($fieldinfo) > 0)? $fieldinfo['uitype'] : 1);
			if($fldname != 'attributesetid') {
				//get user company & branch id
				$tabinfo = array('company','companycf','branch','branchcf');
				if( in_array($tablename,$tabinfo) ) {
					if($tablename == 'company' || $tablename == 'companycf') {
						$compquery = $this->db->query('select company.companyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND company.status=1')->result();
						foreach($compquery as $key) {
							$id = $key->companyid;
						}
					} else if($tablename == 'branch' || $tablename == 'branchcf') {
						$brquery = $this->db->query('select branch.branchid from branch join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND branch.status=1')->result();
						foreach($brquery as $key) {
							$id = $key->branchid;
						}
					} else { //employee
						$id = $this->Basefunctions->userid;
					}
				} else { //employee
					$id = $this->Basefunctions->userid;
				}
				//check its parent child field concept
				if(in_array($fldname,$elname)) {
					$key = array_search($fldname,$elname);
					$pid = $id;
					if($key != NULL) {
						//parent id get
						$result = $this->db->select("$tname[$key].$elname[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$id)->where($tname[$key].'.status',1)->get();
						if($result->num_rows() > 0) {
							foreach($result->result()as $row) {
								$pid=$row->$elname[$key];
							}
							if($pid!=0) {
								//parent name
								$result = $this->db->select("$tname[$key].$fname[$key],$tname[$key].$fnameid[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$pid)->where($tname[$key].'.status',1)->get();
								if($result->num_rows() > 0) {
									foreach($result->result()as $row) {
										$resultset[]=$row->$fname[$key];
									}
								}
							}
						}
					}
				}
				else {
					//field fetch parent table fetch
					$fieldnamechk = explode('-',$tabfield);
					$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
					$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
					$moduleid = $fieldinfo['modid'];
					if(count($fieldinfo)>0) {
						$uitypeid = $fieldinfo['uitype'];
						//fetch parent table join id
						$relpartable = $fieldinfo['partab'];
						$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
						$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $relpartable );
						$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
						//fetch original field of picklist data in view creation
						$viewfieldinfo = $this->viewfieldsinformation($tablename,$tabfield,$fieldinfo['modid']);
						$tabfieldname = ( (count($viewfieldinfo)>0)? $viewfieldinfo['joinfieldname'] : $tabfield );
						$jointabfieldid = ( (count($viewfieldinfo)>0)? $viewfieldinfo['condfiledname'] : $tabfield );
						//join relation table and main table
						$joinq="";
						if($relpartable!=$tablename) {
							$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id';
						}
						//if its picklist data
						$newfield = substr( $tabfieldname,( strlen($tabfieldname)-2 ));
						$tabname = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
						$tbachk = $this->tableexitcheck($database,$tabname);
						if( $newfield == "id" && $tbachk != 0 ) {
							$newtable = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
							$newjfield = $newtable."name";
							$whtable = (($tablename=="company" || $tablename=="branch")? $tablename : $relpartable);
							$newjdata = $this->db->query("SELECT ".$newtable.".".$newtable."id AS columdataid,".$newtable.".".$newjfield." AS columdata FROM ".$newtable." LEFT OUTER JOIN ".$tablename." ON ".$tablename.".".$tabfieldname." = ".$newtable.".".$tabfieldname."".$joinq." WHERE ".$tablename.".".$whtable."id = ".$id." AND ".$newtable.".status=1 AND ".$tablename.".status=1",false);
							if($newjdata->num_rows() >0) {
								foreach($newjdata->result() as $jkey) {
									$resultset[] = $jkey->columdata;
								}
							} else {
								$resultset[] = " ";
							}
						}
						else {
							//main table data sets information fetching
							$fldcountchk = explode('-',$tabfield);
							$tablcolumncheck = $this->tablefieldnamecheck($tablename,$relpartable."id");
							if( ( $relpartable == $tablename && (count($fldcountchk)) < 2 ) || ( $tablcolumncheck == 'false' && (count($fldcountchk)) < 2 ) ) {
								$newdata = $this->db->select("".$tabfield." AS columdata")->from($tablename)->where($tablename.".".$tablename."id",$id)->where($tablename.".status",1)->get()->result();
							}
							else {
								$cond = "";
								$addfield = explode('-',$tabfield);
								if( (count($addfield)) >= 2 ) {
									$datafiledname=$addfield[1];
									if(in_array('PR',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=2';
									} else if(in_array('SE',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=3';
									} else if(in_array('BI',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=4';
									} else if(in_array('SH',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=5';
									} else if(in_array('CW',$addfield)) {
										$formatortype = "CW";
									} else if(in_array('CF',$addfield)) {
										$formatortype = "CF";
									} else if(in_array('CS',$addfield)) {
										$formatortype = "CS";
									}
									if($tablename!=$relpartable) {
										$tablcolumncheck = $this->tablefieldnamecheck($tablename,"addresstypeid");
										$cond = ( ($tablcolumncheck == 'true')? $cond : ' ');
										$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename." JOIN ".$relpartable." ON ".$relpartable.".".$relpartable."id = ".$tablename.".".$relpartable."id WHERE ".$relpartable.".".$relpartable."id = ".$id." AND ".$tablename.".status=1 AND ".$relpartable.".status=1".$cond."")->result();
									} else {
										$newdata = $this->db->select(''.$datafiledname.' AS columdata',false)->from($tablename)->where($tablename.'id',$id)->where($tablename.'.status',1)->get()->result();
									}
								}
								else {
									$newdata = $this->db->query("SELECT ".$tabfield." AS columdata FROM ".$tablename." JOIN ".$relpartable." ON ".$relpartable.".".$relpartable."id = ".$tablename.".".$relpartable."id WHERE ".$relpartable.".".$relpartable."id = ".$id." AND ".$tablename.".status=1 AND ".$relpartable.".status=1")->result();
								}
							}
							foreach($newdata as $key) {
								if($tabfield == 'companylogo') {
									$imgname = $key->columdata;
									if(filter_var($imgname,FILTER_VALIDATE_URL)) {
										$resultset[] = '<img src="'.$imgname.'" />';
									} else {
										if( file_exists($imgname) ) {
											$resultset[] = '<img src="'.$imgname.'" style="width:11em; height:3em" />';
										} else {
											$resultset[] = " ";
										}
									}
								}
								else {
									if($formatortype=="CW") { //convert to word
										$currencyinfo = $this->currencyinformation($tablename,$id,$moduleid);
										if(count($currencyinfo)>0) {
											$resultset[] = $this->convert->numtowordconvert($key->columdata,$currencyinfo['name'],$currencyinfo['subcode']);
										} else {
											$resultset[] = $this->convert->numtowordconvert($key->columdata);
										}
									} else if($formatortype=="CF") { //currenct format
										$currencyinfo = $this->currencyinformation($tablename,$id,$moduleid);
										if(count($currencyinfo)>0) {
											$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
										} else {
											$resultset[] = $this->convert->formatcurrency($key->columdata);
										}
									} else if($formatortype=="CS") { //currency with symbol
										$currencyinfo = $this->currencyinformation($tablename,$id,$moduleid);
										if(count($currencyinfo)>0) {
											$amt = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
											$resultset[] = $currencyinfo['symbol'].' '.$amt;
										} else {
											$resultset[] = $currencyinfo['symbol'].' '.$this->convert->formatcurrency($key->columdata);
										}
									} else {
										$currencyinfo = $this->currencyinformation($tablename,$id,$moduleid);
										if(is_numeric($key->columdata)) {
											if($uitypeid == '4' || $uitypeid == '5' || $uitypeid == '7') {
												if(count($currencyinfo)>0) {
													$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
												} else {
													$resultset[] = $this->convert->formatcurrency($key->columdata);
												}
											} else {
												$resultset[] = $key->columdata;
											}
										} else {
											if($editorfname=='editorfilename') {
												if( file_exists($key->columdata) ) {
													$datas = $this->geteditorfilecontent($key->columdata);
													$datas = preg_replace('~a10s~','&nbsp;', $datas);
													$datas = preg_replace('~<br>~','<br />', $datas);
													$resultset[] = $datas;
												} else {
													$resultset[] = '';
												}
											} else {
												$resultset[] = $key->columdata;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	  return $resultset;
	}
	public function fetchdddataviewddvalmodel() {
		$i=0;
		$mvalu ="";
		$whfield ="";
		$dname=$_GET['dataname'];
		$did=$_GET['dataid'];
		$table=$_GET['datatab'];
		if( isset($_GET['whdatafield']) && isset($_GET['whval']) ) {
			$whfield=$_GET['whdatafield'];
			$whdata=$_GET['whval'];
			if($whdata != "") {
				$mvalu=explode(",",$whdata);
			}
		}
		$default='setdefault';
		$this->db->select("$dname,$did,$default");
		$this->db->from($table);
		$this->db->where('moduleid',251);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$this->db->order_by($did,'desc');
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result()as $row) {
				$data[$i]=array('datasid'=>$row->$did,'dataname'=>$row->$dname,'default'=>$row->$default);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	
	//get final daily reprot amount
	public function getfinaldailyreprotamount($fdate,$tdate) {
		//for sales amount
		$todaysdate = "BETWEEN '".$fdate."' AND '".$tdate."'";
		$sales = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4457);
		$sales = str_replace("dailydateformat",$todaysdate,$sales);
		$newjdata = $this->db->query($sales);
		if($newjdata->num_rows() > 0) {
			foreach($newjdata->result() as $jkey) {
				$salesamount = $jkey->columdata;
			}
		} else {
			$salesamount = '0';
		}
		//for sales return amount
		$salesreturn = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4512);
		$salesreturn = str_replace("dailydateformat",$todaysdate,$salesreturn);
		$salesreturnjdata = $this->db->query($salesreturn);
		if($salesreturnjdata->num_rows() > 0) {
			foreach($salesreturnjdata->result() as $salesreturnjkey) {
				$salesreturnamount = $salesreturnjkey->columdata;
			}
		} else {
			$salesreturnamount = '0';
		}
		//for old jewel amount
		$old = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4466);
		$old = str_replace("dailydateformat",$todaysdate,$old);
		$oldjdata = $this->db->query($old);
		if($oldjdata->num_rows() > 0) {
			foreach($oldjdata->result() as $oldjkey) {
				$oldamount = $oldjkey->columdata;
			}
		} else {
			$oldamount = '0';
		}
		//for Discount amount
		$discount = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4467);
		$discount = str_replace("dailydateformat",$todaysdate,$discount);
		$discountjdata = $this->db->query($discount);
		if($discountjdata->num_rows() > 0) {
			foreach($discountjdata->result() as $discountjkey) {
				$discountamount = $discountjkey->columdata;
			}
		} else {
			$discountamount = '0';
		}
		//for SR Tax amount
		$srtax = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4517);
		$srtax = str_replace("dailydateformat",$todaysdate,$srtax);
		$srtaxjdata = $this->db->query($srtax);
		if($srtaxjdata->num_rows() > 0) {
			foreach($srtaxjdata->result() as $srtaxjkey) {
				$srtaxamount = $srtaxjkey->columdata;
			}
		} else {
			$srtaxamount = '0';
		}
		//for Tax amount
		$tax = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4468);
		$tax = str_replace("dailydateformat",$todaysdate,$tax);
		$taxjdata = $this->db->query($tax);
		if($taxjdata->num_rows() > 0) {
			foreach($taxjdata->result() as $taxjkey) {
				$taxamount = $taxjkey->columdata;
			}
		} else {
			$taxamount = '0';
		}
		//for Chit total amount
		$chit = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4499);
		$chit = str_replace("dailydateformat",$todaysdate,$chit);
		$chitjdata = $this->db->query($chit);
		if($chitjdata->num_rows() > 0) {
			foreach($chitjdata->result() as $chitjkey) {
				$chitamount = $chitjkey->columdata;
			}
		} else {
			$chitamount = '0';
		}
		//for customer balance amount
		$customerbalance = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4478);
		$customerbalance = str_replace("dailydateformat",$todaysdate,$customerbalance);
		$cusbaljdata = $this->db->query($customerbalance);
		if($cusbaljdata->num_rows() > 0) {
			foreach($cusbaljdata->result() as $cusbaljkey) {
				$customerbalanceamount = $cusbaljkey->columdata;
			}
		} else {
			$customerbalanceamount = '0';
		}
		//for Receipt total amount
		$receipt = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4479);
		$receipt = str_replace("dailydateformat",$todaysdate,$receipt);
		$receiptjdata = $this->db->query($receipt);
		if($receiptjdata->num_rows() > 0) {
			foreach($receiptjdata->result() as $receiptjkey) {
				$receipttotalamount = $receiptjkey->columdata;
			}
		} else {
			$receipttotalamount = '0';
		}
		//for card amount
		$card = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4474);
		$card = str_replace("dailydateformat",$todaysdate,$card);
		$cardjdata = $this->db->query($card);
		if($cardjdata->num_rows() > 0) {
			foreach($cardjdata->result() as $cardjkey) {
				$cardamount = $cardjkey->columdata;
			}
		} else {
			$cardamount = '0';
		}
		//for cheque amount
		$cheque = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4475);
		$cheque = str_replace("dailydateformat",$todaysdate,$cheque);
		$chequejdata = $this->db->query($cheque);
		if($chequejdata->num_rows() > 0) {
			foreach($chequejdata->result() as $chequejkey) {
				$chequeamount = $chequejkey->columdata;
			}
		} else {
			$chequeamount = '0';
		}
		//for dd amount
		$dd = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4476);
		$dd = str_replace("dailydateformat",$todaysdate,$dd);
		$ddjdata = $this->db->query($dd);
		if($ddjdata->num_rows() > 0) {
			foreach($ddjdata->result() as $ddjkey) {
				$ddamount = $ddjkey->columdata;
			}
		} else {
			$ddamount = '0';
		}
		//for to cheque amount - old purchase
		$tocheque = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4494);
		$tocheque = str_replace("dailydateformat",$todaysdate,$tocheque);
		$tochequejdata = $this->db->query($tocheque);
		if($tochequejdata->num_rows() > 0) {
			foreach($tochequejdata->result() as $tochequejkey) {
				$tochequeamount = $tochequejkey->columdata;
			}
		} else {
			$tochequeamount = '0';
		}
		//for Chit card amount
		$chitcard = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4471);
		$chitcard = str_replace("dailydateformat",$todaysdate,$chitcard);
		$chitcardjdata = $this->db->query($chitcard);
		if($chitcardjdata->num_rows() > 0) {
			foreach($chitcardjdata->result() as $chitcardjkey) {
				$chitcardamount = $chitcardjkey->columdata;
			}
		} else {
			$chitcardamount = '0';
		}
		//for Chit cheque amount
		$chitcheque = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4470);
		$chitcheque = str_replace("dailydateformat",$todaysdate,$chitcheque);
		$chitchequejdata = $this->db->query($chitcheque);
		if($chitchequejdata->num_rows() > 0) {
			foreach($chitchequejdata->result() as $chitchequejkey) {
				$chitchequeamount = $chitchequejkey->columdata;
			}
		} else {
			$chitchequeamount = '0';
		}
		//for Chit NEFT amount
		/* $chitneft = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4603);
		$chitneft = str_replace("dailydateformat",$todaysdate,$chitneft);
		$chitneftjdata = $this->db->query($chitneft);
		if($chitneftjdata->num_rows() > 0) {
			foreach($chitneftjdata->result() as $chitneftjkey) {
				$chitneftamount = $chitneftjkey->columdata;
			}
		} else { */
			$chitneftamount = '0';
		//}
		//for Receipt card amount
		$receiptcard = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4497);
		$receiptcard = str_replace("dailydateformat",$todaysdate,$receiptcard);
		$receiptcardjdata = $this->db->query($receiptcard);
		if($receiptcardjdata->num_rows() > 0) {
			foreach($receiptcardjdata->result() as $receiptcardjkey) {
				$receiptcardamount = $receiptcardjkey->columdata;
			}
		} else {
			$receiptcardamount = '0';
		}
		//for Receipt cheque amount
		$receiptcheque = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4498);
		$receiptcheque = str_replace("dailydateformat",$todaysdate,$receiptcheque);
		$receiptchequejdata = $this->db->query($receiptcheque);
		if($receiptchequejdata->num_rows() > 0) {
			foreach($receiptchequejdata->result() as $receiptchequejkey) {
				$receiptchequeamount = $receiptchequejkey->columdata;
			}
		} else {
			$receiptchequeamount = '0';
		}
		//for order amount
		$takeorder = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4513);
		$takeorder = str_replace("dailydateformat",$todaysdate,$takeorder);
		$takeorderjdata = $this->db->query($takeorder);
		if($takeorderjdata->num_rows() > 0) {
			foreach($takeorderjdata->result() as $takeorderjkey) {
				$takeorderamount = $takeorderjkey->columdata;
			}
		} else {
			$takeorderamount = '0';
		}
		//for order card amount
		$takeordercard = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4514);
		$takeordercard = str_replace("dailydateformat",$todaysdate,$takeordercard);
		$takeordercardjdata = $this->db->query($takeordercard);
		if($takeordercardjdata->num_rows() > 0) {
			foreach($takeordercardjdata->result() as $takeordercardjkey) {
				$takeordercardamount = $takeordercardjkey->columdata;
			}
		} else {
			$takeordercardamount = '0';
		}
		//for order card amount
		$takeordercheque = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4515);
		$takeordercheque = str_replace("dailydateformat",$todaysdate,$takeordercheque);
		$takeorderchequejdata = $this->db->query($takeordercheque);
		if($takeorderchequejdata->num_rows() > 0) {
			foreach($takeorderchequejdata->result() as $takeorderchequejkey) {
				$takeorderchequamount = $takeorderchequejkey->columdata;
			}
		} else {
			$takeorderchequamount = '0';
		}
		//for order advance amount
		$advancecash = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4531);
		$advancecash = str_replace("dailydateformat",$todaysdate,$advancecash);
		$advancecashjdata = $this->db->query($advancecash);
		if($advancecashjdata->num_rows() > 0) {
			foreach($advancecashjdata->result() as $advancecashjkey) {
				$advancecashamount = $advancecashjkey->columdata;
			}
		} else {
			$advancecashamount = '0';
		}
		//for order issue amount
		/* $orderissuecash = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4607);
		$orderissuecash = str_replace("dailydateformat",$todaysdate,$orderissuecash);
		$orderissuecashjdata = $this->db->query($orderissuecash);
		if($orderissuecashjdata->num_rows() > 0) {
			foreach($advancecashjdata->result() as $orderissuecashjkey) {
				$orderissuecashamount = $orderissuecashjkey->columdata;
			}
		} else { */
			$orderissuecashamount = '0';
		//}
		//final total
		$finalamount = $salesamount-$salesreturnamount-$oldamount-$discountamount+$taxamount-$srtaxamount+$chitamount-$customerbalanceamount+$receipttotalamount-$receiptcardamount-$receiptchequeamount-$cardamount-$chequeamount-$ddamount+$tochequeamount-$chitcardamount-$chitchequeamount+$takeorderamount-$takeordercardamount-$takeorderchequamount-$advancecashamount-$chitneftamount-$orderissuecashamount;
		return $finalamount;
	}
	//
	public function getcashinhandamount($fdate,$tdate) {
		$todaysdate = "BETWEEN '".$fdate."' AND '".$tdate."'";
		//for Chit amount
		$chit = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4469);
		$chit = str_replace("dailydateformat",$todaysdate,$chit);
		$chitjdata = $this->db->query($chit);
		if($chitjdata->num_rows() > 0) {
			foreach($chitjdata->result() as $chitjkey) {
				$chitamount = $chitjkey->columdata;
			}
		} else {
			$chitamount = '0';
		}
		//for sales cash amount
		$cash = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4473);
		$cash = str_replace("dailydateformat",$todaysdate,$cash);
		$cusbaljdata = $this->db->query($cash);
		if($cusbaljdata->num_rows() > 0) {
			foreach($cusbaljdata->result() as $cusbaljkey) {
				$cashamount = $cusbaljkey->columdata;
			}
		} else {
			$cashamount = '0';
		}
		//for Receipt amount
		$receipt = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4496);
		$receipt = str_replace("dailydateformat",$todaysdate,$receipt);
		$receiptjdata = $this->db->query($receipt);
		if($receiptjdata->num_rows() > 0) {
			foreach($receiptjdata->result() as $receiptjkey) {
				$receiptcashamount = $receiptjkey->columdata;
			}
		} else {
			$receiptcashamount = '0';
		}
		//for order cash amount
		$ordercash = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',4516);
		$ordercash = str_replace("dailydateformat",$todaysdate,$ordercash);
		$ordercashjdata = $this->db->query($ordercash);
		if($ordercashjdata->num_rows() > 0) {
			foreach($ordercashjdata->result() as $ordercashjkey) {
				$ordercashamount = $ordercashjkey->columdata;
			}
		} else {
			$ordercashamount = '0';
		}
		//final total
		$cashinhand = $cashamount+$chitamount+$receiptcashamount+$ordercashamount;
		return $cashinhand;
	}
	//folder unique name check
	public function folderuniquedynamicviewnamecheckmodel(){
		$industryid = $this->Basefunctions->industryid;
		$primaryid=$_POST['primaryid'];
		$accname=$_POST['accname'];
		$elementpartable = explode(',',$_POST['elementspartabname']);
		$partable =  'foldername';
		$ptid = $partable.'id';
		$ptname = $partable.'name';
		if($accname != "") {
			$result = $this->db->select($ptname.','.$ptid)->from($partable)->where($partable.'.'.$ptname,$accname)->where($partable.'.moduleid',251)->where($partable.'.status',1)->where("FIND_IN_SET('".$industryid."',industryid) >", 0)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->$ptid;
				} 
			} else { echo "False"; }
		}else { echo "False"; }
	}
	// print template file update
	public function updateprinttemplatedata($primaryid,$templatename,$printtypeid) {
		$printtemplatename = $this->Basefunctions->singlefieldfetch('printtemplatestemplate_editorfilename','printtemplatesid','printtemplates',$primaryid);
		if($printtypeid == 5) {
			$printtemplatename = 'termscondition/'.$templatename.'.txt';
			copy($printtemplatename, 'termscondition/Epson/'.$templatename.'.txt');
			$filelocation='termscondition/Epson/'.$templatename.'.txt';
		} else if($printtypeid == 3 || $printtypeid == 6) {
			copy($printtemplatename, 'termscondition/'.$templatename.'.html');
			$filelocation='termscondition/'.$templatename.'.html';
		}
		array_map( "unlink", glob( $printtemplatename ) );
		$fileupdate=array(
			  'printtemplatestemplate_editorfilename' => $filelocation		
			);
		$this->db->where('printtemplatesid',$primaryid);
		$this->db->update('printtemplates',$fileupdate);
	}
	// print_template files
	public function deleteprinttemplatedata($primaryid) {
		$printtemplatename = $this->Basefunctions->singlefieldfetch('printtemplatestemplate_editorfilename','printtemplatesid','printtemplates',$primaryid);
		array_map( "unlink", glob( $printtemplatename ) );
	}
	// Create Editor File
	public function createeditorfile() {
		$prn = trim($_POST['prn']);
		$filename = trim($_POST['filename']);
		$this->generatefile($prn,$filename);
	}
	//create a new files
	public function generatefile($tempcontent,$temp_id) {
		$print_file_name = '';
		if($tempcontent != '') {
			$print_file_name = 'termscondition/'.trim($temp_id).'.txt';
			@chmod($print_file_name,0766);
			@write_file($print_file_name,$tempcontent);
		}
		return $print_file_name;
	}
}