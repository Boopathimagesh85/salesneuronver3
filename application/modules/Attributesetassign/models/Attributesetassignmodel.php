<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Attributesetassignmodel extends CI_Model{
    public function __construct()  {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//attributesetassign create
	public function newdatacreatemodel() {
		//check the same combination issue
		$attributesetname=$_POST['attributesetassignname'];
		$attributesetshortname=$_POST['attributesetshortname'];
		$attributesetdescription=$_POST['attributesetdescription'];
		$attributeid=$_POST['multiattributeid'];	
		$insert=array('attributesetassignname'=>$attributesetname,'shortname'=>$attributesetshortname,'description'=>$attributesetdescription,'attributeid'=>$attributeid,'industryid'=>$this->Basefunctions->industryid,'createdate'=>date($this->Basefunctions->datef),'lastupdatedate'=>date($this->Basefunctions->datef),'createuserid'=>$this->Basefunctions->userid,'lastupdateuserid'=>$this->Basefunctions->userid,'status'=>$this->Basefunctions->activestatus);
		$this->db->insert('attributesetassign',$insert);
		echo 'TRUE';		
	}
	//Retrive attributesetassign data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['attributesetassignelementsname']);
		$formfieldstable = explode(',',$_GET['attributesetassignelementstable']);
		$formfieldscolmname = explode(',',$_GET['attributesetassignelementscolmn']);
		$elementpartable = explode(',',$_GET['attributesetassignelementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['attributesetassignprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	//attributesetassign update
	public function datainformationupdatemodel() {
		//check the same combination issue
		$attributesetname=$_POST['attributesetassignname'];
		$attributesetshortname=$_POST['attributesetshortname'];
		$attributesetdescription=$_POST['attributesetdescription'];
		$attributesetassignprimarydataid=$_POST['attributesetassignprimarydataid'];
		$attributeid=$_POST['multiattributeid'];			
		$update=array('attributesetassignname'=>$attributesetname,'shortname'=>$attributesetshortname,'description'=>$attributesetdescription,'attributeid'=>$attributeid,'createdate'=>date($this->Basefunctions->datef),'lastupdatedate'=>date($this->Basefunctions->datef),'createuserid'=>$this->Basefunctions->userid,'lastupdateuserid'=>$this->Basefunctions->userid,'status'=>$this->Basefunctions->activestatus);
		$this->db->where('attributesetassignid',$attributesetassignprimarydataid);
		$this->db->update('attributesetassign',$update);		
		echo 'TRUE';
	}
	//attributesetassign delete
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['attributesetassignelementstable']);
		$parenttable = explode(',',$_GET['attributesetassignparenttable']); 
		$id = $_GET['attributesetassignprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				echo "TRUE";
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			echo "TRUE";
		}
	} 	
}