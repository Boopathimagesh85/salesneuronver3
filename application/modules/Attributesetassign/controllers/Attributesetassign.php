<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Attributesetassign extends MX_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Attributesetassign/Attributesetassignmodel');	
    }
    //first basic hitting view
    public function index() {        
    	$moduleid = array(15);
    	sessionchecker($moduleid);
		//action
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(15);
		$viewmoduleid = array(15);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
      	$this->load->view('Attributesetassign/attributesetassignview',$data);
	}
	//create itemcounter
	public function newdatacreate() {  
    	$this->Attributesetassignmodel->newdatacreatemodel();
    }
	//information fetchitemcounter
	public function fetchformdataeditdetails() {
		$moduleid = 15;
		$this->Attributesetassignmodel->informationfetchmodel($moduleid);
	}
	//update itemcounter
    public function datainformationupdate() {
        $this->Attributesetassignmodel->datainformationupdatemodel();
    }
	//delete itemcounter
    public function deleteinformationdata() {
        $moduleid = 15;
		$this->Attributesetassignmodel->deleteoldinformation($moduleid);
    } 
}