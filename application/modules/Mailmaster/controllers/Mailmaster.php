<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Mailmaster extends MX_Controller{
	//To load the Register View
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Mailmaster/Mailmastermodel');
    }
	public function index() {
		$moduleid = array(209);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(209);
		$viewmoduleid = array(209);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Mailmaster/mailmasterview',$data);
	}
	//Create New data
	public function newdatacreate() {
    	$this->Mailmastermodel->newdatacreatemodel();
    }
	//information fetch
	public function fetchformdataeditdetails() {
		$moduleid = 209;
		$this->Mailmastermodel->informationfetchmodel($moduleid);
	}
	//Update old data
    public function datainformationupdate() {
        $this->Mailmastermodel->datainformationupdate();
    }
	//delete old informtion
    public function datainformationdata() {
		$moduleid = 209;
        $this->Mailmastermodel->deleteoldinformation($moduleid);
    }
	//field name value fetch
	public function fetchmaildddatawithmultiplecond() {
        $this->Mailmastermodel->fetchmaildddatawithmultiplecondmodel();
    }
	//editor value fetch 	
	public function editervaluefetch() {
		$filename = $_GET['filename'];
		$file = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$file[1])) {
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$file[1]);
			echo json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			echo json_encode($tccontent);
		}
	}
	//document grid value fetch
	public function emaildocumentdetailfetch() {
        $this->Mailmastermodel->emaildocumentdetailfetchmodel();
    }
	//Related module get
	public function relatedmoduleidget() {
		//$this->Mailmastermodel->relatedmoduleidgetmodel();
		$this->Basefunctions->relatedmodulelistfetchforprint();
	}
	//template content preview
	public function templatecontentpreview() {
		$content = $this->Mailmastermodel->templatecontentpreviewmodel();
		echo $content;
	}
	/* Local non-live grid heder information fetch */
	public function localgirdheaderinformationfetch() {
		$moduleid = $_GET['moduleid'];
		$tabgrpid = $_GET['tabgroupid'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modname = $_GET['modulename'];
		$colinfo = $this->Mailmastermodel->localgridheaderinformationfetchmodel($moduleid,$tabgrpid);
		$device = $this->Basefunctions->deviceinfo();
		//if($device=='phone') {
			//$datas = $this->Mailmastermodel->mobilelocalviewgridheadergenerate($colinfo,$modname,$width,$height);
		//} else {
			$datas = $this->Mailmastermodel->localviewgridheadergenerate($colinfo,$modname,$width,$height);
		//}
		echo json_encode($datas);
	}
	public function modulefieldload() {
		$viewfieldsmoduleids =array_filter(explode(',',$_POST['moduleid']));
		$viewfieldsreletedmoduleids =array_filter(explode(',',$_POST['relatedmoduleid']));
		$all_moduleid=array_merge($viewfieldsmoduleids,$viewfieldsreletedmoduleids);
		$data = $this->Mailmastermodel->modulefieldload($all_moduleid);
		echo $data;
	}
	//Email Template name unique value
	public function emailtemplatesnameunique(){
		$this->Mailmastermodel->emailtemplatesnameuniquemodel();
	}
	//Folder insertion
	public function mailmasterfolderinsert() {
		$this->Mailmastermodel->mailmasterfolderinsertmodel();
	}
	//Folder Grid View
	public function mailmasterfoldernamegridfetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'foldername.foldernameid') : 'foldername.foldernameid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('foldernamename','description','setdefault','public'),'colmodelindex'=>array('foldername.foldernamename','foldername.description','foldername.setdefault','foldername.public'),'coltablename'=>array('foldername','foldername','foldername','foldername'),'uitype'=>array('2','2','2','2'),'colname'=>array('Folder Name','Description','Default','Public'),'colsize'=>array('200','200','200','200'));
		$result=$this->Mailmastermodel->mailmasterfoldernamegridxmlview($primarytable,$sortcol,$sortord,$pagenum,$rowscount);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($result,$page,'');
		$device = $this->Basefunctions->deviceinfo();
///	if($device=='phone') {
	//		$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Folder List',$width,$height);
		//} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Folder List',$width,$height);
		//}
		echo json_encode($datas);
	}
	//Folder data fetch
	public function mailmasterfolderdatafetch() {
		$this->Mailmastermodel->mailmasterfolderdatafetchmodel();
	}
	//Folder Update
	public function mailmasterfolderupdate() {
		$this->Mailmastermodel->mailmasterfolderupdatemodel();
	}
	//Folder Delete
	public function folderdeleteoperation() {
		$this->Mailmastermodel->folderdeleteoperationmodel();
	}
	//Folder drop down Fetch Data
	public function fetchdddataviewddval() {
		$this->Mailmastermodel->fetchdddataviewddvalmodel();
	}
	//unique name restriction
	public function folderuniquenamecheck(){
		$this->Mailmastermodel->folderuniquenamecheckmodel();
	}
}