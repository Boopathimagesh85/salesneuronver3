<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Model File
class mailmastermodel extends CI_Model{    
    public function __construct() {		
		parent::__construct(); 
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    } 
	//To Create New Company,Branch Details
	public function newdatacreatemodel() {
		$folderid = $_POST['emailfoldernameid'];
		$tname = $_POST['emailtemplatesname'];
		$listid = $_POST['emaillisttypeid'];
		$subject = $_POST['subject'];
		$emailcampaigntypeid = $_POST['emailcampaigntypeid'];
		if(isset($_POST['moduleid'])) {
			$moduleid = $_POST['moduleid'];
		} else {
			$moduleid = 1;
		}
		if($listid == 2) {
			if($moduleid == 1) {
				$moduleid = 271;
			}
		}
		$description = $_POST['emaildescription'];
		$signature = $_POST['addonsignature'];
		//template types
		$date = date($this->Basefunctions->datef);
		$tempcontent = $_POST['emailtemplatesemailtemplate_editorfilename'];
		$html_file_name = 'template_'.trim($tname)."_".time().".html";
		$filename = 'termscondition/'.$html_file_name;
		$fp = fopen('termscondition/'.$html_file_name, "w");
		fwrite($fp, $tempcontent);
		fclose($fp);
		$tandc= array(
			'foldernameid'=>$folderid,
			'emailtemplatesname'=>$tname,
			'emaillisttypeid'=>$listid,
			'emailcampaigntypeid'=>$emailcampaigntypeid,
			'description'=>$description,
			'subject'=>$subject,
			'emailtemplatesemailtemplate_editorfilename'=>$filename,
			'moduleid'=>$moduleid,
			'addonsignature'=>$signature,
			'industryid'=>$this->Basefunctions->industryid,
			'branchid'=>$this->Basefunctions->branchid,
			'createdate'=>$date,
			'lastupdatedate'=>$date,
			'createuserid'=>$this->Basefunctions->userid,
			'lastupdateuserid'=>$this->Basefunctions->userid,
			'status'=>$this->Basefunctions->activestatus
		);
		$tcdata = array_filter($tandc);
		$this->db->insert('emailtemplates',$tcdata);
		//primary key
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $this->db->insert_id();
		$defdataarr = $this->Crudmodel->defaultvalueget();
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$m=0;
		$h=1;
		$attachmentgrid = 0;
		$gridfieldpartabname = explode(',',$_POST['griddatapartabnameinfo']);
		$gridrows = explode(',',$_POST['numofrows']);
		$attachment_totrows = $gridrows[1] + 1;
		$griddata = $_POST['griddatas'];	
		$griddatainfo = json_decode($griddata, true);
		foreach($gridrows as $rowcount) {
			$gridfieldsname = explode(',',$_POST['gridcolnames'.$h]);
			//filter grid unique fields table
			$gridfieldtabname = explode(',',$_POST['griddatatabnameinfo'.$h]);
			$gridfieldstable = $this->Crudmodel->filtervalue($gridfieldtabname);
			$gridtableinfo = explode(',',$gridfieldstable);
			$i = $attachmentgrid;
			for($i;$i<$attachment_totrows;$i++) {
				foreach( $gridtableinfo as $gdtblname ) {
					${'$gcdata'.$m} = array();
					$t=0;
					foreach($gridfieldsname AS $gdfldsname) {
						if(trim($gdtblname) != '') {
							if(strcmp( trim($gridfieldtabname[$t]),trim($gdtblname) ) == 0 ) {
								if( isset( $griddatainfo[$i][$gdfldsname] ) ) {
									$name = explode('_',$gdfldsname);
									if( ctype_alpha($griddatainfo[$i][$gdfldsname]) && $griddatainfo[$i][$gdfldsname] != "" ) {
										${'$gcdata'.$m}[$gdfldsname] = ucwords( $griddatainfo[$i][$gdfldsname] );
									} else if( $griddatainfo[$i][$gdfldsname] != "" ) {
										${'$gcdata'.$m}[$gdfldsname] = $griddatainfo[$i][$gdfldsname];
									}
								}
							}
						}
					}
					if(trim($gdtblname) != '') {
						if(count(${'$gcdata'.$m})>0) {
							${'$gcdata'.$m}[$primaryname]=$primaryid;
							$gnewdata = array_merge(${'$gcdata'.$m},$defdataarr);
							$gnewdata['commonid']=$primaryid;
							$gnewdata['moduleid']='209';
							if(!isset($gnewdata['employeeid'])){
								$gnewdata['employeeid']='1';}
								//unset the hidden variables
								$removehidden=array('crmfileinfoid','emailtemplatesid');
								for($mk=0;$mk<count($removehidden);$mk++){
									unset($gnewdata[$removehidden[$mk]]);
								}
								$inarr = array_filter($gnewdata);
								$cnt = substr_count($inarr['filename'], ',');
								if($cnt >= 1){
									$uname = explode(',' , $inarr['filename']);
									$usize = explode(',' , $inarr['filename_size']);
									$upath = explode(',' , $inarr['filename_path']);
									$utype = explode(',' , $inarr['filename_type']);
									if(isset($inarr['filename_fromid'])){
										$ffid = explode(',' , $inarr['filename_fromid']);
									}
									foreach($uname as $key => $value){
										$inarr['filename'] = $value;
										$inarr['filename_size'] = $usize[$key];
										$inarr['filename_path'] = $upath[$key];
										$inarr['filename_type'] = $utype[$key];
										$inarr['industryid'] = $this->Basefunctions->industryid;
										$inarr['branchid'] = $this->Basefunctions->branchid;
										if(isset($inarr['filename_fromid'])){
											$inarr['filename_fromid'] = $ffid[$key];
										}
										$this->db->insert($gdtblname,$inarr);
									}
								} else {
									$inarr['industryid'] = $this->Basefunctions->industryid;
									$inarr['branchid'] = $this->Basefunctions->branchid;
									$this->db->insert($gdtblname,$inarr);
								}
						}
						$m++;
					}
				}
			}
			$attachmentgrid++;
			$h++;
		} 
		echo "TRUE";
	}
	//information fetch
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementpartable']);
		$restricttable = explode(',',$_GET['resctable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['dataprimaryid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetchwithrestrict($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$restricttable,$moduleid);
		echo $result;
	}
	//update data information
	public function datainformationupdate() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_POST['primarydataid'];
		$folderid = $_POST['emailfoldernameid'];
		$tname = $_POST['emailtemplatesname'];
		$listid = $_POST['emaillisttypeid'];
		$subject = $_POST['subject'];
		$emailcampaigntypeid = $_POST['emailcampaigntypeid'];
		if(isset($_POST['moduleid'])) {
			$moduleid = $_POST['moduleid'];
		} else {
			$moduleid = 1;
		}
		if($listid == 2) {
			if($moduleid == 1) {
				$moduleid = 271;
			}
		}
		$description = $_POST['emaildescription'];
		$signature = $_POST['addonsignature'];
		//template types
		$date = date($this->Basefunctions->datef);
		$tempcontent = $_POST['emailtemplatesemailtemplate_editorfilename'];
		$html_file_name = 'template_'.trim($tname)."_".time().".html";
		$filename = 'termscondition/'.$html_file_name;
		$fp = fopen('termscondition/'.$html_file_name, "w");
		fwrite($fp, $tempcontent);
		fclose($fp);
		$tandc= array(
			'foldernameid'=>$folderid,
			'emailtemplatesname'=>$tname,
			'emaillisttypeid'=>$listid,
			'emailcampaigntypeid'=>$emailcampaigntypeid,
			'description'=>$description,
			'subject'=>$subject,
			'emailtemplatesemailtemplate_editorfilename'=>$filename,
			'moduleid'=>$moduleid,
			'addonsignature'=>$signature,
			'createdate'=>$date,
			'lastupdatedate'=>$date,
			'createuserid'=>$this->Basefunctions->userid,
			'lastupdateuserid'=>$this->Basefunctions->userid,
			'status'=>$this->Basefunctions->activestatus
			);
		$this->db->where('emailtemplates.emailtemplatesid',$primaryid);
		$this->db->update('emailtemplates',$tandc);
		//retrieve all the documents
		$crmfileinfoiddata = $this->retrievealldocuments($primaryid);
		//primary key
		$defdataarr = $this->Crudmodel->defaultvalueget();
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$m=0;
		$h=1;
		$attachmentgrid = 0;
		$gridfieldpartabname = explode(',',$_POST['griddatapartabnameinfo']);
		$gridrows = explode(',',$_POST['numofrows']);
		$attachment_totrows = $gridrows[1] + 1;
		$griddata = $_POST['griddatas'];
		$griddatainfo = json_decode($griddata, true);
		foreach($gridrows as $rowcount) {
			$gridfieldsname = explode(',',$_POST['gridcolnames'.$h]);
			//filter grid unique fields table
			$gridfieldtabname = explode(',',$_POST['griddatatabnameinfo'.$h]);
			$gridfieldstable = $this->Crudmodel->filtervalue($gridfieldtabname);
			$gridtableinfo = explode(',',$gridfieldstable);
			$i = $attachmentgrid;
			for($i;$i<$attachment_totrows;$i++) {
				foreach( $gridtableinfo as $gdtblname ) {
					${'$gcdata'.$m} = array();
					$t=0;
					foreach($gridfieldsname AS $gdfldsname) {
						if(trim($gdtblname) != '') {
							if(strcmp( trim($gridfieldtabname[$t]),trim($gdtblname) ) == 0 ) {
								if( isset( $griddatainfo[$i][$gdfldsname] ) ) {
									$name = explode('_',$gdfldsname);
									if( ctype_alpha($griddatainfo[$i][$gdfldsname]) && $griddatainfo[$i][$gdfldsname] != "" ) {
										${'$gcdata'.$m}[$gdfldsname] = ucwords( $griddatainfo[$i][$gdfldsname] );
									} else if( $griddatainfo[$i][$gdfldsname] != "" ) {
										${'$gcdata'.$m}[$gdfldsname] = $griddatainfo[$i][$gdfldsname];
									}
								}
							}
						}
					}
					if(trim($gdtblname) != '') {
						if(count(${'$gcdata'.$m})>0) {
							${'$gcdata'.$m}['commonid']=$primaryid;
							${'$gcdata'.$m}['moduleid']=209;
							$gnewdata = array_merge(${'$gcdata'.$m},$defdataarr);
							//unset the hidden variables
							$removehidden=array('emailtemplatesid');
							for($mk=0;$mk<count($removehidden);$mk++){
								unset($gnewdata[$removehidden[$mk]]);
							}
							$inarr = array_filter($gnewdata);
							$cnt = substr_count($inarr['filename'], ',');
							if(isset($inarr['crmfileinfoid'])) {
								$inarr['crmfileinfoid'] = $inarr['crmfileinfoid'];
								if (($key = array_search($inarr['crmfileinfoid'], $crmfileinfoiddata)) !== false) {
									unset($crmfileinfoiddata[$key]);
								}
							} else {
								$inarr['crmfileinfoid'] = 0;
							}
							if($inarr['crmfileinfoid'] == 0) {
								unset($inarr['crmfileinfoid']);
								if($cnt >= 1){
									$uname = explode(',' , $inarr['filename']);
									$usize = explode(',' , $inarr['filename_size']);
									$upath = explode(',' , $inarr['filename_path']);
									$utype = explode(',' , $inarr['filename_type']);
									if(isset($inarr['filename_fromid'])){
										$ffid = explode(',' , $inarr['filename_fromid']);
									}
									foreach($uname as $key => $value){
										$inarr['filename'] = $value;
										$inarr['filename_size'] = $usize[$key];
										$inarr['filename_path'] = $upath[$key];
										$inarr['filename_type'] = $utype[$key];
										if(isset($inarr['filename_fromid'])){
											$inarr['filename_fromid'] = $ffid[$key];
										}
										$inarr['industryid'] = $this->Basefunctions->industryid;
										$inarr['branchid'] = $this->Basefunctions->branchid;
										$this->db->insert($gdtblname,$inarr);
									}
								}  else {
									$inarr['industryid'] = $this->Basefunctions->industryid;
									$inarr['branchid'] = $this->Basefunctions->branchid;
									$this->db->insert($gdtblname,$inarr);
								}
							}
						}
						$m++;
					}
				}
			}
			$attachmentgrid++;
			$h++;
		}
		$deleteddocuments = count($crmfileinfoiddata);
		if($deleteddocuments > 0) {
			$delete_sub_data=array('lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid,'status'=>$this->Basefunctions->deletestatus);
			//delete the sub tables(crmfileinfo)
			$this->db->where_in('moduleid','209');
			$this->db->where_in('crmfileinfoid',$crmfileinfoiddata);
			$this->db->update('crmfileinfo',$delete_sub_data);
		}
		echo "TRUE";
	}
	// Retrieve documents for specific primaryid
	public function retrievealldocuments($primaryid) {
		$resultdata = array();
		$this->db->select('crmfileinfo.crmfileinfoid');
		$this->db->from('crmfileinfo');
		$this->db->where('crmfileinfo.moduleid','209');
		$this->db->where('crmfileinfo.commonid',$primaryid);
		$result = $this->db->get()->result();
		$countableresult = count($result);
		if($countableresult > 0) {
			foreach($result as $rowdata) {
				$resultdata[] = $rowdata->crmfileinfoid;
			}
		}
		return $resultdata;
	}
	//delete old information
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				$this->unlinkdocumentsfromlocaldrive($_GET['primarydataid']);
				echo "TRUE";
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			$this->unlinkdocumentsfromlocaldrive($_GET['primarydataid']);
			echo "TRUE";
		}
	}
	// Remove documents from local drive
	public function unlinkdocumentsfromlocaldrive($primaryid) {
		$this->db->select('crmfileinfo.filename_path');
		$this->db->from('crmfileinfo');
		$this->db->where('crmfileinfo.moduleid','209');
		$this->db->where('crmfileinfo.commonid',$primaryid);
		$result = $this->db->get()->result();
		$countableresult = count($result);
		if($countableresult > 0) {
			foreach($result as $rowdata) {
				unlink($rowdata->filename_path);
			}
		}	
	}
	//template content preview
	public function templatecontentpreviewmodel() {
		$templateid = $_POST['templateid'];
		$headerfile = "";
		$contentfile = "";
		$footerfile = "";
		$this->db->select('emailtemplates.emailtemplatesemailtemplate_editorfilename');
		$this->db->from('emailtemplates');
		$this->db->where('emailtemplates.emailtemplatesid',$templateid);
		$result = $this->db->get();
		foreach($result->result() as $rowdata) {
			$contentfile = $rowdata->emailtemplatesemailtemplate_editorfilename;
		}
		//html content
		$bodycontent = $this->filecontentfetch($contentfile);
		return $bodycontent;
	}
	//read file
	public function filecontentfetch($filename) {
		$filecontent = "";
		if( file_exists($filename) && is_readable($filename) ) {
			$newfilename = explode('/',$filename);
			$myfile = @fopen('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1], "r");
			$filecontent = fread($myfile,filesize('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]));
			fclose($myfile);
		}
		return $filecontent;
	}
	//drop down value set with multiple condition
	public function fetchmaildddatawithmultiplecondmodel() {
		$i=0;
		$dname=$_GET['dataname'];
		$did=$_GET['dataid'];
		$dataattrname1=$_GET['othername1'];
		$dataattrname2=$_GET['othername2'];
		$table=$_GET['datatab'];
		$whfield=$_GET['whdatafield'];
		$whdata=$_GET['whval'];
		$mulcond=explode(",",$whfield);
		$mulcondval=explode(",",$whdata);
		$i=0;
		$this->db->select("$dname,$did,$dataattrname1,$dataattrname2");
		$this->db->from($table);
		foreach($mulcond AS $condname) {
			if($mulcondval[$i] != "0" && $mulcondval[$i] != "") {
				if($mulcondval[0] == '235' || $mulcondval[0] == '232' || $mulcondval[0] == '234'){
					$mid = $this->mastermoduleidget($mulcondval[0]);
					$this->db->select("$dname,$did,$dataattrname1,$dataattrname2");
					$this->db->from($table);
					$this->db->where_in($condname,$mid);
				} else {
					$this->db->where($condname,$mulcondval[$i]);
				}
			}
			$i++;
		}
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {		
			foreach($result->result()as $row) {
				$data[$i]=array('datasid'=>$row->$did,'dataname'=>$row->$dname,'otherdataattrname1'=>$row->$dataattrname1,'otherdataattrname2'=>$row->$dataattrname2);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//document details fetch
	public function emaildocumentdetailfetchmodel() {
		//$productdetail = '';
		$documentid=trim($_GET['primarydataid']);
		$this->db->select('crmfileinfoid,commonid,filename,crmfileinfo.filename_size,crmfileinfo.filename_type,crmfileinfo.filename_path,foldername.foldernamename,crmfileinfo.keywords,crmfileinfo.foldernameid,crmfileinfo.filename_fromid');
		$this->db->from('crmfileinfo');
		$this->db->join('foldername','foldername.foldernameid=crmfileinfo.foldernameid');
		$this->db->where('crmfileinfo.moduleid',209);
		$this->db->where_in('crmfileinfo.commonid',$documentid);
		$this->db->where('crmfileinfo.status',$this->Basefunctions->activestatus);
		$data=$this->db->get()->result();	
		$j=0;
		foreach($data as $value) {			
			$productdetail->rows[$j]['id']=$value->crmfileinfoid;
			$productdetail->rows[$j]['cell']=array(
													$value->crmfileinfoid,
													$value->foldernamename,
													$value->foldernameid,
													$value->keywords,
													$value->filename_fromid,
													$value->filename,
													$value->filename_size,
													$value->filename_type,
													$value->filename_path,
												);						
			$j++;		
		}
		echo  json_encode($productdetail);
	}
	//master module id get
	public function mastermoduleidget($mulcondval) {
		$data = '';
		$this->db->select('moduledashboardid');
		$this->db->from('module');
		$this->db->where('module.moduleid',$mulcondval);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = $row->moduledashboardid;
			}
			return $data;
		} else {
			return $data;
		}
	}
	//Related Modules id get
	public function relatedmoduleidgetmodel() {
		$moddatas = array();
		$relmodids = array();
		$moduleid = $_POST['moduleid'];
		$genmod = array(2,3,265);
		$relmoduleids = $this->db->select('modulerelationid,relatedmoduleid')->from('modulerelation')->where('moduleid',$moduleid)->get();
		foreach($relmoduleids->result() as $reldata) {
			$relmodids[] = $reldata->relatedmoduleid;
		}
		$i=0;
		$modids = implode(',',$relmodids);
		$moduleids = ( ($modids!="")? $modids.','.$moduleid : $moduleid );
		$modids = explode(',',$moduleids);
		$allmodid = array_merge($modids,$genmod);
		$allmoduleids = implode(',',array_unique($allmodid));
		$modinfo = $this->db->query('select module.moduleid,module.modulename from module join moduleinfo ON moduleinfo.moduleid=module.moduleid where module.moduleid IN ('.$allmoduleids.') AND module.status=1 AND moduleinfo.status=1 group by moduleinfo.moduleid',false);
		foreach($modinfo->result() as $moddata) {
			$modtype = ( ($moddata->moduleid==$moduleid)?'':((in_array($moddata->moduleid,$genmod))?'GEN:':'REL:') );
			$moddatas[$i] = array('datasid'=>$moddata->moduleid,'dataname'=>$moddata->modulename,'datatype'=>$modtype);
			$i++;
		}
		$datas = ( ($i==0)? json_encode(array('fail'=>'FAILED')) : json_encode($moddatas) );
		echo $datas;
	}
	function localviewgridheadergenerate($colinfo,$modulename,$width,$height,$checkbox = 'false') {
		$CI =& get_instance();
		/* width & height set */
		$addcolsize = 0;
		$reccount = count($colinfo['colmodelname']);
		$columnswidth = 5;
		$reccount = 0;
		$active =0;
		foreach ($colinfo['colmodelviewtype'] as $type) {
			$columnswidth += $type=='1'? 150 : 0;
			$reccount++;
			if($type == '1') {
				$active++;
			}
		}
		$extrasize = ($columnswidth>$width)?0 : ($width-$columnswidth);
		$addcolsize = $extrasize!=0 ? round($extrasize/$active) : 0;
		$columnswidth = ($columnswidth>$width)?$columnswidth : $width;
		
		/* height set */
		if($height<=420) {
			$datarowheight = $height-35;
		} else {
			$datarowheight = $height-35;
		}
		/* header generation */
		if($checkbox == 'true') {
			$columnswidth = $columnswidth+35;
		}
		$mdivopen ='<div class="grid-view">';
		$header = '<div class="header-caption" style="width:'.$columnswidth.'px;"><ul class="inline-list">';
		$i=0;
		$m=0;
		$modname = strtolower(preg_replace('/[^A-Za-z0-9]/', '', $modulename));
		foreach ($colinfo['fieldlabel'] as $headname) {
			if($checkbox == 'true' && $m==0) {
				$header .='<li data-uitype="13" data-fieldname="gridcheckboxfield" data-width="35" data-class="gridcheckboxclass" data-viewtype="1" style="width:35px;" id="'.$modname.'headcheckbox"><input type="checkbox" id="'.$modname.'_headchkbox" name="'.$modname.'_headchkbox" class="'.$modname.'_headchkboxclass headercheckbox" /></li>';
				$m=1;
			}
			$cmodname = strtolower($colinfo['colmodelname'][$i]);
			$cmodindex = strtolower($colinfo['colmodelindex'][$i]);
			$cfield = strtolower($colinfo['fieldname'][$i]);
			$viewtype = ($colinfo['colmodelviewtype'][$i]=='0') ? ' display:none':'';
			$header .='<li id="'.$modname.'headcol'.$i.'" class="'.$cmodname.'-class '.$modname.'headercolsort" data-class="'.$cmodname.'-class" data-sortcolname="'.$cmodindex.'" data-sortorder="ASC" data-uitype="'.$colinfo['colmodeluitype'][$i].'" data-fieldname="'.$cfield.'" style="width:'.(150+$addcolsize).'px;'.$viewtype.'" data-width="'.(150+$addcolsize).'" data-viewtype="'.$colinfo['colmodelviewtype'][$i].'" data-position="'.$i.'">'.$headname.'</li>';
			$i++;
		}
		$header .='</ul></div>';
		$content = '<div class="gridcontent" style="height:'.$datarowheight.'px; overflow:auto;"><div class="data-content wrappercontent" style="width:'.$columnswidth.'px;">&nbsp;</div></div';
		$mdivclose = '</div>';
		$datas = $mdivopen.$header.$content.$mdivclose;
		$footer = '';
		/* Footer generation */
		$footer = '<div class="summary-blue">';
		$footer .= '</div>';
		return array('content'=>$datas,'footer'=>$footer);
	}
	//local inner grid mobile view
	public  function mobilelocalviewgridheadergenerate($colinfo,$modulename,$width,$height,$checkbox = 'false') {
		$CI =& get_instance();
		/* Height set */
		if($height > '460') {
			$height = $height-105;
		}else {
			$height = $height-128;
		}		
		$mdivopen ='<div class="grid-view">';
		$content = '<div class="header-caption" style="display:none">';
		$i=0;
		$m=0;
		$modname = strtolower(preg_replace('/[^A-Za-z0-9]/', '', $modulename));
		foreach ($colinfo['fieldlabel'] as $headname) {
			if($checkbox == 'true' && $m==0) {		
				$content .='<span data-uitype="13" data-fieldname="gridcheckboxfield" data-class="gridcheckboxclass" style="width:35px;" data-viewtype="1" id="'.$modname.'headcheckbox"<input type="checkbox" id="'.$modname.'_headchkbox" name="'.$modname.'_headchkbox" class="content"/></span>';
				$m=1;
			}
			$cmodname = strtolower($colinfo['colmodelname'][$i]);
			$cmodindex = strtolower($colinfo['colmodelindex'][$i]);
			$cfield = strtolower($colinfo['fieldname'][$i]);			
			$viewtype = ($colinfo['colmodelviewtype'][$i]=='0') ? ' display:none':'';			
			$content .= '<span id="'.$modname.'headcol'.$i.'"  data-class="'.$cmodname.'-class" data-sortcolname="'.$cmodindex.'" data-sortorder="ASC" data-uitype="'.$colinfo['colmodeluitype'][$i].'" data-fieldname="'.$cfield.'" class="'.$cmodname.'-class '.$modname.'headercolsort content" style="width:150px;'.$viewtype.'"  data-viewtype="'.$colinfo['colmodelviewtype'][$i].'" data-position="'.$i.'" data-label="'.$headname.' ">'.$headname.':</span>';			
			$i++;
		}		
		$content .='</div><div class="row-content gridcontent" style="height:'.$height.'px;"><div class="wrappercontent"></div></div>';
		$mdivclose = '</div>';
		$datas = $content;
		/* Footer generation */
		$footer = '<div class="summary-blue">';
		$footer .= '</div>';
		return array('content'=>$datas,'footer'=>$footer);
	}
	/* Local non-live grid header information fetch model */
	public function localgridheaderinformationfetchmodel($moduleid,$tabgroupid) {
		$userroleid = $this->Basefunctions->userroleid;
		$i=0;
		$dduitypeids = array('17','18','19','20','23','25','26','27','28','29');
		$data = array();
		$this->db->select('modulefield.modulefieldid,modulefield.fieldname,modulefield.columnname,modulefield.fieldlabel,modulefield.uitypeid,modulefield.status');
		$this->db->from('modulefield');
		$this->db->join('uitype','uitype.uitypeid=modulefield.uitypeid');
		$this->db->join('moduletabsection','moduletabsection.moduletabsectionid=modulefield.moduletabsectionid');
		$this->db->join('moduletabgroup','moduletabgroup.moduletabgroupid=moduletabsection.moduletabgroupid');
		$this->db->join('module','module.moduleid=moduletabgroup.moduleid');
		$this->db->where("moduletabgroup.moduletabgroupid", $tabgroupid);
		$this->db->where("modulefield.moduletabid", $moduleid);
		$this->db->where("FIND_IN_SET('$userroleid',moduletabsection.userroleid) >", 0);
		$this->db->where("FIND_IN_SET('$userroleid',modulefield.userroleid) >", 0);
		$this->db->where_in('modulefield.status',array(1,10));
		$this->db->where('moduletabsection.moduletabsectiontypeid',1);
		$this->db->where('moduletabsection.status',1);
		$this->db->order_by('moduletabsection.sequence','asc');
		$this->db->order_by('modulefield.sequence','asc');
		$showfield = $this->db->get();
		if($showfield->num_rows() >0) {
			foreach($showfield->result() as $show) {
				if( in_array($show->uitypeid,$dduitypeids) ) {
					$data['fieldname'][$i]=$show->fieldname;
					$data['fieldlabel'][$i]=$show->fieldlabel;
					$data['colmodelname'][$i]=$show->columnname.'name';
					$data['colmodelindex'][$i]=$show->columnname;
					$data['colmodelviewtype'][$i]='1';
					$data['colmodeltype'][$i]='text';
					$data['colmodeluitype'][$i]=$show->uitypeid;
					$data['colmoduleid'][$i]=$moduleid;
					$i++;
					$data['fieldname'][$i]=$show->fieldname;
					$data['fieldlabel'][$i]=$show->fieldlabel;
					$data['colmodelname'][$i]=$show->columnname;
					$data['colmodelindex'][$i]=$show->columnname;
					$data['colmodelviewtype'][$i]='0';
					$data['colmodeltype'][$i]='text';
					$data['colmodeluitype'][$i]='2';
					$data['colmoduleid'][$i]=$moduleid;
					$i++;
				} else {
					if($show->status == 10) {
						$viewtype = '0';
					} else{
						$viewtype = '1';
					}
					$data['fieldname'][$i]=$show->fieldname;
					$data['fieldlabel'][$i]=$show->fieldlabel;
					$data['colmodelname'][$i]=$show->columnname;
					$data['colmodelindex'][$i]=$show->columnname;
					$data['colmodelviewtype'][$i]=$viewtype;
					$data['colmodeltype'][$i]='text';
					$data['colmodeluitype'][$i]=$show->uitypeid;
					$data['colmoduleid'][$i]=$moduleid;
					$i++;
				}
			}
		}
		return $data;
	}
	//merge module based form fields load
	public function modulefieldload($moduleid) {
		$printfield=[];
		$printfield1=[];
		$result_array=[];
		$this->db->select('viewcreationmoduleid,viewcreationcolmodelname,viewcreationcolmodelaliasname,viewcreationcolmodelindexname,viewcreationcolumnname,fieldprintname,viewcreationcolumnid');
		$this->db->from('viewcreationcolumns');
		$this->db->where_in('viewcreationcolumns.viewcreationmoduleid',$moduleid);
		$this->db->where('viewcreationcolumns.status',$this->Basefunctions->activestatus);
		$this->db->order_by('viewcreationcolumns.viewcreationcolumnid','asc');
		$data = $this->db->get()->result();
		$optgroupid=1;
		foreach($data as $info)
		{
			$printfield[] = array(
					'pname'=>'Field Column',
					'optgroupid'=>1,
					'pid'=>$info->viewcreationcolumnid,
					'id'=>$info->viewcreationmoduleid,
					'label'=>$info->viewcreationcolumnname,
					'key'=>'{'.$info->fieldprintname.'}',
			);
		}
		return json_encode($printfield);
	}
	//Folder main grid view
	public function mailmasterfoldernamegridxmlview($tablename,$sortcol,$sortord,$pagenum,$rowscount) {
		$dataset = 'foldername.foldernamename,foldername.foldernameid,foldername.description,foldername.setdefault,foldername.public';
		$join ='';
		$status = $tablename.'.status IN (1)';
		$where = $tablename.'.moduleid IN (209)';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		//query
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$where.' AND '.$status.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		return $data;
	}
	//Folder Insertion
	public function mailmasterfolderinsertmodel() {
		$foldername = $_POST['foldername'];
		$description = $_POST['description'];
		$default = $_POST['setdefault'];
		$ppublic = $_POST['ppublic'];
		$date = date($this->Basefunctions->datef);
		$folderarray = array(
				'foldernamename'=>$foldername,
				'description'=>$description,
				'moduleid'=>'209',
				'setdefault'=>$default,
				'public'=>$ppublic,
				'userroleid'=>$this->Basefunctions->userroleid,
				'industryid'=>$this->Basefunctions->industryid,
				'createdate'=>$date,
				'lastupdatedate'=>$date,
				'createuserid'=>$this->Basefunctions->userid,
				'lastupdateuserid'=>$this->Basefunctions->userid,
				'status'=>$this->Basefunctions->activestatus
		);
		$this->db->insert('foldername',$folderarray);
		$folderid = $this->db->insert_id();
		$this->setdefaultupdate(209,$default,$folderid);
		echo "TRUE";
	}
	//set default update
	public function setdefaultupdate($moduleid,$default,$folderid) {
		if($default != 'No') {
			$updatearray = array('setdefault'=>'No');
			$this->db->where_not_in('foldername.foldernameid',$folderid);
			$this->db->where('foldername.moduleid',$moduleid);
			$this->db->update('foldername',$updatearray);
		}
	}
	//Folder Edit Data Fetch
	public function mailmasterfolderdatafetchmodel() {
		$folderid = $_GET['datarowid'];
		$this->db->select('SQL_CALC_FOUND_ROWS  foldername.foldernamename,foldername.foldernameid,foldername.description,foldername.setdefault,foldername.public',false);
		$this->db->from('foldername');
		$this->db->where('foldername.foldernameid',$folderid);
		$this->db->where('foldername.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data=array('id'=>$row->foldernameid,'foldername'=>$row->foldernamename,'description'=>$row->description,'setdefault'=>$row->setdefault,'ppublic'=>$row->public);
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//Folder Update
	public function mailmasterfolderupdatemodel() {
		$foldernameid = $_POST['foldernameid'];
		$foldername = $_POST['foldername'];
		$description = $_POST['description'];
		$default = $_POST['setdefault'];
		$ppublic = $_POST['ppublic'];
		$date = date($this->Basefunctions->datef);
		$folderarray = array(
				'foldernamename'=>$foldername,
				'description'=>$description,
				'setdefault'=>$default,
				'public'=>$ppublic,
				'userroleid'=>$this->Basefunctions->userroleid,
				'industryid'=>$this->Basefunctions->industryid,
				'lastupdatedate'=>$date,
				'lastupdateuserid'=>$this->Basefunctions->userid,
				'status'=>$this->Basefunctions->activestatus
		);
		$this->db->where('foldername.foldernameid',$foldernameid);
		$this->db->update('foldername',$folderarray);
		$this->setdefaultupdate(209,$default,$foldernameid);
		echo "TRUE";
	}
	//Folder Delete
	public function folderdeleteoperationmodel() {
		$folderid = $_GET['id'];
		$date = date($this->Basefunctions->datef);
		$folderarray = array(
				'lastupdatedate'=>$date,
				'lastupdateuserid'=>$this->Basefunctions->userid,
				'status'=>0
		);
		$this->db->where('foldername.foldernameid',$folderid);
		$this->db->update('foldername',$folderarray);
		echo "TRUE";
	}
	//Folder Drop Down Reload function
	public function fetchdddataviewddvalmodel() {
		$loguserid = $this->Basefunctions->userid;
		$i = 0;
		$fieldname = $_GET['dataname'];
		$fieldid = $_GET['dataid'];
		$tablename = $_GET['datatab'];
		$moduleid = $_GET['moduleid'];
		$this->db->select("$fieldid,$fieldname,setdefault");
		$this->db->from("$tablename");
		$this->db->where("FIND_IN_SET('$moduleid',".$tablename.".moduleid) >", 0);
		if($loguserid != 2){
			$this->db->where_not_in("$tablename".'.public','No');
		}
		$this->db->where("$tablename".'.status',1);
		$result=$this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data[$i]=array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname,'setdefault'=>$row->setdefault);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//Email Template Unique name
	public function emailtemplatesnameuniquemodel(){
		$industryid = $this->Basefunctions->industryid;
		$emailtemplatesname = $_POST['emailtemplatesname'];
		if($emailtemplatesname != "") {
			$result = $this->db->select('emailtemplates.emailtemplatesid')->from('emailtemplates')->where('emailtemplates.emailtemplatesname',$emailtemplatesname)->where("FIND_IN_SET('$industryid',emailtemplates.industryid) >", 0)->where('emailtemplates.status',1)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->emailtemplatesid;
				}
			} else {
				echo "False";
			}
		} else {
			echo "False";
		}
	}
} 