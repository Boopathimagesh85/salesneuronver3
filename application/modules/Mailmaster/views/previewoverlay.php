<!-- Template Print Overlay  Overlay-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="previewoverlayoverlay" style="overflow-y: scroll;overflow-x: hidden">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="large-8 medium-8 large-centered medium-centered columns" >
			<form method="POST" name="" style="" id="" action="" enctype="" class="clearformtaxoverlay borderstyle">
				<span id="pdfviewoverlayformvalidation" class="validationEngineContainer"> 
					<div class="large-12 columns headercaptionstyle headerradius paddingzero">
						<div class="large-6 medium-6 small-6 columns headercaptionleft">
							Preview
						</div>
						<div class="large-5 medium-6 small-6 columns addformheadericonstyle righttext small-only-text-center">
							<span title="Close" id="previewclose"><i class="material-icons" title="Close">close</i></span>
						</div>
					</div>
					<div class="large-12 columns paddingzero" style="background:#e0e0e0">
						<div class="row">&nbsp;</div>
						<div class="large-12 columns end paddingbtm">
							<div class="row" style="background:#f5f5f5">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-12 columns">Preview</div>
						</div>
							</div>
							<div style="background:#f5f5f5" class="row">
								<iframe id="templatepreviewframe" name="templatepreviewframe" frameborder='0' width='100%' height='400' allowtransparency='true'></iframe>
								<div class="large-12 columns">&nbsp;</div>
							</div>
						</div>
					</div>
					<div class="row">&nbsp;</div>
					<div class="row">&nbsp;</div>					
				</span>
			</form>
		</div>
	</div>
</div>