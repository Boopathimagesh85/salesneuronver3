<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Selectivebackupmodel extends CI_Model
{
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Crudmodel');
    }
	public function viewdynamicdatainfofetch($tablename,$sortcol,$sortord,$pagenum,$rowscount) {
	
		$dataset ='selectivebackup.selectivebackupid,selectivebackup.filename,selectivebackup.location,selectivebackup.Date,selectivebackup.Time,employee.employeename';
		$join='LEFT OUTER JOIN employee ON employee.employeeid=selectivebackup.createuserid';
		$cuscondition='';
		$status='selectivebackup.status in (1,20)';
	
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		/* query */
		$result = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$cuscondition.' '. $status.' ORDER BY '.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$data=array();
		$i=0;
		foreach($result->result() as $row) {
			$backupid = $row->selectivebackupid;
			$location = $row->location;
			$backupname = $row->filename;
			$backupdate = $row->Date;
			$backuptime = $row->Time;
			$employeename = $row->employeename;
			$data[$i]=array('id'=>$backupid,$backupname,$location,$backupdate,$backuptime,$employeename);
			$i++;
		}
		$finalresult=array('0'=>$data,'1'=>$page,'2'=>'json');
		return $finalresult;
	
	}
	
	public function accledgerupdate() {
	   $maxaccledgerid = $_POST['maxaccledgerid'];
	   $accountidaccc = array();
	   $accountid = array();
	   $date = date("Y-m-d");
	   $salesnumber = 'CUM'.$date.'';
	   $data = $this->db->query('select accountledger.creditno,accountledger.accountid,accountledger.salesid,accountledger.salesdetailid,accountledger.paymentirtypeid from accountledger where accountledger.creditno != "" and accountledger.entrymode = 4 and accountledger.status = 1 and accountledger.accountledgerid > '.$maxaccledgerid.''); 
		foreach ($data->result() as $jvalue) {
			$accountid[] = $jvalue->accountid;
			$salesid = $jvalue->salesid;
			$salesdetailid = $jvalue->salesdetailid;
			$paymentirtypeid = $jvalue->paymentirtypeid;
			$this->Salesmodel->creditnobalancesummary($salesid,$salesdetailid,$salesnumber,$jvalue->accountid,$jvalue->creditno,$paymentirtypeid);
		}
		$dataaccc = $this->db->query('select accountledger.accountid from accountledger where accountledger.entrymode = 5 and accountledger.status = 1 and accountledger.accountledgerid > '.$maxaccledgerid.''); 
		foreach ($dataaccc->result() as $jvalueaccc) {
			$accountidaccc[] = $jvalueaccc->accountid;
		}
		$finalaccarray = array_filter(array_unique(array_merge ($accountid,$accountidaccc),SORT_NUMERIC));
		foreach ($finalaccarray as $value) {
			$this->Salesmodel->accountbalancesummary(1,1,$salesnumber,$value,$date,11);		
		}
		echo 'Success';		
	}
	public function accledgeroperation() {
	  $adminpassword = $_POST['adminpassword'];
	  $accledgerop = $_POST['accledgerop'];
	  $dbmode = $this->Basefunctions->get_company_settings('dbmode');
	  $accledgermode = $this->Basefunctions->get_company_settings('accledgermode');
	  $creditnos = array();
	  $accountid = array();
	  $tobedelaccountid = array();
	  $creditnosstr = '';
	  $tobedelaccountidstr = '';
	  $this->db->select("companysettingid");
	  $this->db->from("companysetting");
	  $this->db->where("adminpassword",$adminpassword);
	  $this->db->where('companysettingid',1);
	  $userquerys = $this->db->get();
	  if($userquerys->num_rows() >0) 
	  {
		  if($dbmode == 1 && $accledgermode == 1 && $accledgerop == 1)  //remove all acc ledger data from client db
		  {
			$data = $this->db->query('select accountledger.creditno,accountledger.accountid from accountledger where accountledger.creditno != "" and accountledger.entrymode = 4 and finalamtissue  = 0 and finalamtreceipt  = 0 and finalweightissue  = 0 and finalweightreceipt  = 0 and accountledger.status = 1'); 
			foreach ($data->result() as $jvalue) {
				$creditnos[] = '"'.$jvalue->creditno.'"';
				$accountid[] = $jvalue->accountid;
			}
			$creditnosstr = implode(',',$creditnos);
			$this->db->query("DELETE FROM accountledger WHERE  accountledger.creditno in (".$creditnosstr.")");
			$accountid = array_filter(array_unique($accountid,SORT_NUMERIC));
			$date = date("Y-m-d");
			$salesnumber = 'CUM'.$date.'';
			foreach ($accountid as $accvalue) {
				$this->Salesmodel->accountbalancesummary(1,1,$salesnumber,$accvalue,$date,11);		
			}
			$dataacc = $this->db->query('select distinct(accountledger.accountid) from accountledger where  accountledger.entrymode = 5 and finalamtissue  = 0 and finalamtreceipt  = 0 and finalweightissue  = 0 and finalweightreceipt  = 0 and accountledger.status = 1'); 
			foreach ($dataacc->result() as $jaccvalue) {
				$tobedelaccountid[] = $jaccvalue->accountid;
			}
			$tobedelaccountidstr = implode(',',$tobedelaccountid);
			$this->db->query("DELETE FROM accountledger WHERE  accountledger.accountid in (".$tobedelaccountidstr.")");
		  }else  if($dbmode == 1 && $accledgermode == 1 && $accledgerop == 2){
			  $this->db->query("DELETE FROM accountledger join account on account.accountid = accountledger.accountid WHERE account.accounttypeid  not in (6,16)");
		  }
	  }else 
	  {
	    echo 'WRONG';
	  }
	}
	public function performanceboostmodel() {
	
		$salesidcount = 0;
		$salesidexcludecount = 0;
		$salesdetailidtobedelcount = 0;
		$tdi = 0;
		$itemtagid = array();
		$itemtagno = array();
		$salesid  = array();
		$salesidexclude = array();
		$salesdetailidtobedel = array();
		$dbmode = $this->Basefunctions->get_company_settings('dbmode');
		$accledgermode = $this->Basefunctions->get_company_settings('accledgermode');
		$adminpassword = $_POST['adminpassword'];
		$performanceop = $_POST['performanceop'];
		$this->db->select("companysettingid");
		$this->db->from("companysetting");
		$this->db->where("adminpassword",$adminpassword);
		$this->db->where('companysettingid',1);
		$userquerys = $this->db->get();
		if($userquerys->num_rows() >0) 
		{
			$this->db->select("createdate");
			$this->db->from("selectivebackup");
			$this->db->where('status',1);
			$this->db->order_by('selectivebackupid',"desc");
			$this->db->limit(1,0);
			$querys = $this->db->get();
			if($querys->num_rows() > 0) 
			{
				$cdate = $querys->row()->createdate;
				//$cdate = '2017-01-18 00:09:18';
				if($performanceop == '1' || $performanceop == '3')  //all or deletestatus and log
				{ 
				  $this->db->query("DELETE FROM itemtag WHERE  itemtag.status in (0) and lastupdatedate < '".$cdate."'");
				  $this->db->query("DELETE FROM stoneentry WHERE  stoneentry.status in (0) and lastupdatedate < '".$cdate."'");
				  $this->db->query("DELETE FROM sales WHERE  sales.status in (0) and lastupdatedate < '".$cdate."'");
				  $this->db->query("DELETE FROM salesdetail WHERE  salesdetail.status in (0) and lastupdatedate < '".$cdate."'");
				  $this->db->query("DELETE FROM salesdetailtax WHERE  salesdetailtax.status in (0) and lastupdatedate < '".$cdate."'");
				  $this->db->query("DELETE FROM salesstoneentry WHERE  salesstoneentry.status in (0) and lastupdatedate < '".$cdate."'");
				  $this->db->query("DELETE FROM lot WHERE  lot.status in (0) and lastupdatedate < '".$cdate."'");
				  $this->db->query("DELETE FROM accountledger WHERE  accountledger.status in (0) and lastupdatedate < '".$cdate."'");
				  $nlog = $this->db->query("DELETE FROM notificationlog where lastupdatedate < '".$cdate."'");
				}
				if($performanceop == '2' || $performanceop == '3')  //all or estiamte alone
				{
				  $estimatecount = 0;
				  $estimateid = array();
				  $allestimatedata = $this->db->query("select salesid  from sales WHERE sales.status in (1) and sales.salestransactiontypeid in (16) and (sales.lastupdatedate < '".$cdate."')");
					foreach($allestimatedata->result() as $allestimatedatarow) {
						$estimateid[$estimatecount] = $allestimatedatarow->salesid;
						$estimatecount++;
					}
				  if($estimatecount > 0)
				  {
					$estimateidstr = implode(',',$estimateid);
					$this->db->query("DELETE FROM sales WHERE  sales.salesid in (".$estimateidstr.")");
					$this->db->query("DELETE FROM salesdetail WHERE  salesdetail.salesid in (".$estimateidstr.")");
					$this->db->query("DELETE FROM salesdetailtax WHERE  salesdetailtax.salesid in (".$estimateidstr.")");
					$this->db->query("DELETE FROM salesstoneentry WHERE  salesstoneentry.salesid in (".$estimateidstr.")"); 
				  }
				}
				if($performanceop == '3')  //all 
				{
				   { // stock entry and lot
						$this->db->query("DELETE FROM lot WHERE  lot.status in (0,2,5,11) and ( lot.lastupdatedate < '".$cdate."')");
						$this->db->query("DELETE FROM itemtag WHERE  itemtag.status in (0) and (itemtag.lastupdatedate < '".$cdate."') ");
						$this->db->query("DELETE FROM stoneentry WHERE  stoneentry.status in (0) and (stoneentry.lastupdatedate < '".$cdate."') ");
						
						$tagdata = $this->db->query("select itemtagid,itemtagnumber from itemtag WHERE itemtag.status in (2,5,11,6,12,13,18,19) and (itemtag.lastupdatedate < '".$cdate."')");
						foreach($tagdata->result() as $tagdatarow) {
							$itemtagid[$tdi] = $tagdatarow->itemtagid;
							$itemtagno[$tdi] = '"'.$tagdatarow->itemtagnumber.'"';
							$tdi++;
						}
						if($tdi > 0){
							$whintagno = implode(',',$itemtagno);
							$transferdata = $this->db->query("select itemtagid  from itemtag WHERE itemtag.status in (1) and itemtag.tagentrytypeid in (2,3) and itemtag.transfertagno in (".$whintagno.") and (itemtag.lastupdatedate < '".$cdate."')");
							foreach($transferdata->result() as $transferdatarow) {
								$itemtagid[$tdi] = $transferdatarow->itemtagid;
								$tdi++;
							}
						}
					}
				   {// purchase get salesid whose lot is still active
						$sdi = 0;
						$autolotsaleswh = '0';
						$autolotsalesid = $this->db->query("select salesid  from sales join lot on lot.billnumber = sales.salesnumber WHERE sales.status in (1) and lot.status in (1) and sales.salestransactiontypeid in (9) and (sales.lastupdatedate < '".$cdate."') group by sales.salesid ");
						foreach($autolotsalesid->result() as $autolotsalesrow) {
							$autolotsales[$sdi] = $autolotsalesrow->salesid;
							$sdi++;
						}
						if($sdi > 0){
							$autolotsaleswh = implode(',',$autolotsales);
							$purchaseid = $this->db->query("select salesid  from sales  WHERE sales.status in (1) and sales.salestransactiontypeid in (9) and sales.salesid not in (".$autolotsaleswh.") and (sales.lastupdatedate < '".$cdate."')");
							foreach($purchaseid->result() as $purchaseidrow) {
								$salesid[$salesidcount] = $purchaseidrow->salesid;
								$salesidcount++;
							}
						}
				   }
				   {// find all appout id whose salesdetail has tag that are still in approval
						
						$sdi = 0;
						$appoutactive = '';
						$appoutdata = $this->db->query("select distinct(sales.salesid) as salesid from sales join salesdetail on salesdetail.salesid = sales.salesid  WHERE sales.status in (1) and salesdetail.status in (1) and salesdetail.stocktypeid in (62) and salesdetail.approvalstatus = 1  and (sales.lastupdatedate < '".$cdate."') group by sales.salesid ");
						foreach($appoutdata->result() as $appoutdatarow) {
							$salesidexclude[$salesidexcludecount] = $appoutdatarow->salesid;
							$salesidexcludecount++;
							$appoutsalesdetail = $this->db->query("select GROUP_CONCAT(salesdetail.salesdetailid) as salesdetailid from sales join salesdetail on salesdetail.salesid = sales.salesid  WHERE sales.status in (1) and salesdetail.status in (1) and salesdetail.stocktypeid in (62) and sales.salesid ='".$appoutdatarow->salesid."' and ( sales.lastupdatedate < '".$cdate."') group by sales.salesid");
							foreach($appoutsalesdetail->result() as $appoutsalesdetailrow) {
								$appoutsalesdetailactive[$sdi] = $appoutsalesdetailrow->salesdetailid;
								$sdi++;
							}
						}
						if($sdi > 0){
							$appoutsalesdetailactivestr = implode(',',$appoutsalesdetailactive);
							// approval return - whose approval is still active
							$appoutreturndata = $this->db->query("select distinct(sales.salesid) as salesid from sales join salesdetail on salesdetail.salesid = sales.salesid  WHERE sales.status in (1) and salesdetail.status in (1) and salesdetail.stocktypeid in (65) and salesdetail.presalesdetailid in (".$appoutsalesdetailactivestr.") group by sales.salesid ");
							foreach($appoutreturndata->result() as $appoutreturndatarow) {
								$salesidexclude[$salesidexcludecount] = $appoutreturndatarow->salesid;
								$salesidexcludecount++;
							}
						}
					}
				   {// take order removal
						// all take,place,receive order whose orderstatus is cancel or delivered to be deleted
						$torderdata = $this->db->query("select GROUP_CONCAT(salesdetail.salesdetailid) as salesdetailid  from sales join salesdetail on salesdetail.salesid = sales.salesid  WHERE sales.status in (1) and salesdetail.status in (1) and salesdetail.stocktypeid in (75,76,82) and salesdetail.orderstatusid in (4,6) and (sales.lastupdatedate < '".$cdate."') group by sales.salesid");
						foreach($torderdata->result() as $torderdatarow) {
							$salesdetailidtobedel[$salesdetailidtobedelcount] = $torderdatarow->salesdetailid;
							$salesdetailidtobedelcount++;
						}
						$resttorderdata = $this->db->query("select GROUP_CONCAT(salesdetail.salesdetailid) as salesdetailid  from sales join salesdetail on salesdetail.salesid = sales.salesid  WHERE sales.status in (1) and salesdetail.status in (1) and sales.salestransactiontypeid = 20 and salesdetail.stocktypeid not in (75,76,82) and (sales.lastupdatedate < '".$cdate."') group by sales.salesid");
						foreach($resttorderdata->result() as $resttorderdatarow) {
							$salesdetailidtobedel[$salesdetailidtobedelcount] = $resttorderdatarow->salesdetailid;
							$salesdetailidtobedelcount++;
						}
						$allorderdata = $this->db->query("select  sales.salesid,count(salesdetail.salesdetailid) as sdid from sales left outer join salesdetail on salesdetail.salesid = sales.salesid  WHERE sales.status in (1) and salesdetail.status in (1) and sales.salestransactiontypeid in (20,21) and (sales.lastupdatedate < '".$cdate."') group by sales.salesid");
						foreach($allorderdata->result() as $allorderdatarow) {
							if($allorderdatarow->sdid == 0){
								$salesid[$salesidcount] = $allorderdatarow->salesid;
								$salesidcount++;
							}
						}
				   }
				   {// all sales of sales,estimate,wtsales,payments,journal,contra,appout with excluded ids,
					if($salesidexcludecount > 0){
						$salesidexcludestr = implode(',',$salesidexclude);
					}else{
						$salesidexcludestr = '0';
					}
					$allsalesdata = $this->db->query("select salesid  from sales WHERE sales.status in (1) and sales.salestransactiontypeid in (11,13,22,23,24,25,18) and sales.salesid not in (".$salesidexcludestr.") and (sales.lastupdatedate < '".$cdate."')");
					foreach($allsalesdata->result() as $allsalesrow) {
						$salesid[$salesidcount] = $allsalesrow->salesid;
						$salesidcount++;
					}
				   }
				   $salesdetailidstr = implode(',',$salesdetailidtobedel);
				   $salesidstr = implode(',',$salesid);
				   $itemtagidstr = implode(',',$itemtagid);
					if($itemtagidstr == ''){$itemtagidstr = 0;};
					if($salesdetailidstr == ''){$salesdetailidstr = 0;};
					if($salesidstr == ''){$salesidstr = 0;};
				   { // cummulative stock table insertion
					$partialtagselectgwt = " COALESCE(sum(itemtag.grossweight),0) ";
					$partialtagselectnwt = " COALESCE(sum(itemtag.netweight),0) ";
					$pagwt = "";
					$panwt = "";
					$supagwt = "";
					$supanwt = "";
					$transfertagwhere = "";
					$startdate='';
					$enddate='';
					$datewh='';
					$filterwherecondtion='';
					$whereString='';
					$grouporder='';
					$join_stmt='';
					$join_stmt='';
					$mode = "2";
					$whereclauses = '';
					$mainquery = $this->Basefunctions->stockreportquerygeneratefunction($startdate,$enddate,$datewh,$filterwherecondtion,$whereString,$grouporder,$join_stmt,$transfertagwhere,$partialtagselectgwt,$partialtagselectnwt,$pagwt,$panwt,$supagwt,$supanwt,$salesidstr,$itemtagidstr,$mode,$whereclauses);
					$data = $this->db->query($mainquery);
					$cstock = $this->db->query('TRUNCATE TABLE cummulativestock'); 
					foreach ($data->result() as $jvalue) {
						if($jvalue->openinggrossweight >= 0){
							$ingrossweight = $jvalue->openinggrossweight;$outgrossweight = 0;
						}
						else {
							$outgrossweight = abs($jvalue->openinggrossweight);$ingrossweight = 0;
						}
						
						if($jvalue->openingnetweight >= 0){
							$innetweight = $jvalue->openingnetweight;$outnetweight = 0;
						} else {
							$outnetweight = abs($jvalue->openingnetweight);$innetweight = 0;
						}
						
						if($jvalue->openingpieces >= 0){
							$inpieces = $jvalue->openingpieces;$outpieces = 0;
						} else {
							$outpieces = abs($jvalue->openingpieces);$inpieces = 0;
						}
						
						if($jvalue->openingitemrate >= 0){
							$initemrate = $jvalue->openingitemrate;$outitemrate = 0;
						} else {
							$outitemrate = abs($jvalue->openingitemrate);$initemrate = 0;
						}
						
						$currentstockdata = array(
						'stockdate'=>date('Y-m-d', strtotime($cdate)),
						'branchid'=>$jvalue->branchid,
						'purityid'=>$jvalue->purityid,
						'productid'=>$jvalue->productid,
						'counterid'=>$jvalue->counterid,
						'stockreporttypeid'=>$jvalue->stockreporttypeid,
						'ingrossweight'=>$ingrossweight,
						'innetweight'=>$innetweight,
						'inpieces'=>$inpieces,
						'initemrate'=>$initemrate,
						'outgrossweight'=>$outgrossweight,
						'outnetweight'=>$outnetweight,
						'outpieces'=>$outpieces,
						'outitemrate'=>$outitemrate,
						'industryid'=>$this->Basefunctions->industryid,
						'createdate'=>$cdate,
						'lastupdatedate'=>$cdate,
						'createuserid'=>$this->Basefunctions->userid,
						'lastupdateuserid'=>$this->Basefunctions->userid,
						'status'=>1
						);
						$this->db->insert('cummulativestock', $currentstockdata);
					}
				  } 
				   
				    { // delete the data in tables
					$this->db->query("DELETE FROM itemtag WHERE  itemtag.itemtagid in (".$itemtagidstr.")");
					$this->db->query("DELETE FROM stoneentry WHERE  stoneentry.itemtagid in (".$itemtagidstr.")");
					$this->db->query("DELETE FROM sales WHERE  sales.salesid in (".$salesidstr.")");
					$this->db->query("DELETE FROM salesdetail WHERE  salesdetail.salesid in (".$salesidstr.")");
					$this->db->query("DELETE FROM salesdetailtax WHERE  salesdetailtax.salesid in (".$salesidstr.")");
					$this->db->query("DELETE FROM salesstoneentry WHERE  salesstoneentry.salesid in (".$salesidstr.")"); 
					$this->db->query("DELETE FROM salesdetail WHERE  salesdetail.salesdetailid in (".$salesdetailidstr.")"); 
				   }
				   { // acc ledger delete all data if slave db(2) and accledgermode is removeall (2)
					if($dbmode != 1 && $accledgermode == 2)  //remove all acc ledger data from client db
					{
						 $this->db->query("DELETE FROM accountledger WHERE  lastupdatedate < '".$cdate."'");
					}
				   } 
				}
				echo 'Success';				
			}
		}	
		else 
		{
		 echo 'WRONG';
		}
	}
}