<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('Base/headerfiles'); ?>	
	<link href="<?php echo base_url();?>css/jqgrid.min.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="row" style="max-width:100%">
		<div class="off-canvas-wrap" data-offcanvas>
			<div class="inner-wrap">
				<!-- Add Form -->
				<div id="backupformdiv" class="">
					<?php $this->load->view('selectivebackupform'); ?>
				</div>
				<?php
				$device = $this->Basefunctions->deviceinfo();
				if($device=='phone') {
					$this->load->view('Base/overlaymobile');
				} else {
					$this->load->view('Base/overlay');
				}
				$this->load->view('Base/modulelist');
		        ?>
				<!-- Off Canvas Menu -->
			<!--	<aside class="left-off-canvas-menu">
					<?php //$this->load->view('Base/modulelist'); ?>
				</aside> -->
				<!-- close the off-canvas menu -->
				<!-- <a class="exit-off-canvas"></a> -->
			</div>
		</div>
	</div>
</body>
<?php $this->load->view('Base/bottomscript'); ?>
<script src="<?php echo base_url();?>js/plugins/jqgrid/jquery.jqGrid.src.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/Selectivebackup/selectivebackup.js" type="text/javascript"></script>
</html>