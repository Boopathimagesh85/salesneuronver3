<style type="text/css">
	#backupadvanceddrop{
		left: -54.1406px !important;
	}
</style>
<?php
	$device = $this->Basefunctions->deviceinfo();
	$dataset['gridtitle'] = $gridtitle['title'];
	$dataset['titleicon'] = $gridtitle['titleicon'];
	$dataset['moduleid'] = '60';
	$dataset['action'][0] = array('actionid'=>'backuplocation','actiontitle'=>'Create','actionmore'=>'Yes','ulname'=>'backup');
	$this->load->view('Base/formwithgridmenuheader.php',$dataset);
?>
<div class="large-12 columns addformunderheader">&nbsp; </div>
<div class="large-12 columns addformcontainer">
	<!-- More Action DD start -- Gowtham -->
		<ul id="backupadvanceddrop" class="multinavaction-drop arrow_box" style="white-space: nowrap; position: absolute; top: 34.8px; left:-55.1406px !important; opacity: 1; display: none;">
			<li id="backupdelete"><span class="icon-box"><i class="material-icons deleteiconclass" title="Delete">delete</i>Delete</span></li>
			<li id="backupupload"><span class="icon-box"><i class="material-icons datamanipulationiconclass" title="Upload">import_export</i>Upload</span></li>
			<li id="performanceboost" title="Performance Booster"><span class="icon-box"><i class="material-icons stonesettingiconclass" title="Performance Booster">add_circle</i>Perf-Boost</span></li>
			<li id="accledgerarchive" title="Acc Ledger Archive"><span class="icon-box"><i class="material-icons stonesettingiconclass" title="Performance Booster">add_circle</i>Acc Ledger Archive</span></li>
		</ul>
	<!-- More Action DD End -- Gowtham -->
	<div class="row mblhidedisplay">&nbsp;</div>
	<div id="subformspan1" class="hiddensubform"> 
		<?php
			$device = $this->Basefunctions->deviceinfo();
			if($device=='phone') {
				echo '<div class="large-12 columns paddingbtm" >	
						<div class="large-12 columns viewgridcolorstyle" style="padding-left:0;padding-right:0;">	
							<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0rem 0rem; height:auto; line-height:1.8rem;">	
								<span class="large-6  medium-6 small-5 columns" style="text-align:left">Backup List</span>
								<span class="large-6 medium-6 small-7 columns innergridicons" style="text-align:right">
									<span class="locationiconclass icon-box" title="Location" id="backuplocation"><i class="material-icons">folder_open</i></span>
									<span class="addiconclass icon-box" title="Backup" id="backupcreate"><i class="material-icons">restore</i></span>
									<span class="downloadiconclass icon-box" title="Download" id="backupdownload"><i class="material-icons">cloud_download</i></span>
									<span class="deleteiconclass icon-box" title="Delete" id="backupdelete"><i class="material-icons">delete</i></span>
									<span class="icon-box" title="Save" id="backupsettingsmailoverlay"><i class="material-icons">mail_outline</i>  </span>
								</span>
							</div>';
				echo '<div class="large-12 columns paddingzero forgetinggridname" id="backupgridwidth"><div class=" inner-row-content inner-gridcontent" id="backupgrid" style="height:420px;top:0px;">
					<!-- Table header content , data rows & pagination for mobile-->
				</div>
				<footer class="inner-gridfooter footercontainer" id="backupgridfooter">
					<!-- Footer & Pagination content -->
				</footer></div></div></div>';
			} else {
				echo '<div class="large-12 columns paddingbtm" style="padding-right: 0.6em;">	
						<div class="large-12 columns viewgridcolorstyle borderstyle" style="padding-left:0;padding-right:0;">	
							<!--<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0rem 0rem; height:auto; line-height:1.8rem;">	
								<span class="large-6  medium-6 small-12 columns small-only-text-center" style="text-align:left">Backup List<span class="fwgpaging-box backupgridfootercontainer"></span></span>
								<span class="large-6 medium-6 small-12 columns innergridicons small-only-text-center" style="text-align:right">
									<span class="locationiconclass icon-box" title="Location" id="backuplocation"><i class="material-icons">folder_open</i></span>
									<span class="addiconclass icon-box" title="Backup" id="backupcreate"><i class="material-icons">restore</i></span>
									<span class="downloadiconclass icon-box" title="Download" id="backupdownload"><i class="material-icons">cloud_download</i></span>
									<span class="deleteiconclass icon-box" title="Delete" id="backupdelete"><i class="material-icons">delete</i></span>
									<span class="icon-box" title="Save" id="backupsettingsmailoverlay"><i class="material-icons">mail_outline</i>  </span>
								</span>
							</div>-->';
				echo '<div class="large-12 columns forgetinggridname" id="backupgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="backupgrid" style="max-width:2000px; height:450px;top:0px;">
				<!-- Table content[Header & Record Rows] for desktop-->
				</div>
				<div class="inner-gridfooter footer-content footercontainer" id="backupgridfooter">
					<!-- Footer & Pagination content -->
				</div></div></div></div>';
			}
		?>				
	</div>
	
</div>
<input type="hidden" name="updateuser" id="updateuser" value="<?php echo $this->Basefunctions->userid; ?>" />
<!--Backup Overlay overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="backuplocationovrelay" style="overflow-y: scroll;overflow-x: hidden;">
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="large-6 medium-6 large-centered medium-centered columns" >
			<form method="POST" name="locationsetform" style="" id="locationsetform" action="" enctype="" class="">
				<span id="locationvalidationwizard" class="validationEngineContainer"> 
					<div class="alert-panel" style="background: #617d8a;">
						<div class="alertmessagearea">
							<div id="selbackupheader" class="alert-title">Backup Location</div>
							<div class="alert-message">
								<span class="firsttab" tabindex="1000"></span>
								<div class="static-field overlayfield drivenamedd-div">
									<label>Drive Name<span class="mandatoryfildclass">*</span></label>
									<select class="chzn-select validate[required] ffieldd" id="drivename" name="drivename" data-prompt-position="topLeft:14,36" tabindex="1001" data-placeholder="Select">
										<option value=""></option>
										<?php
											$fso = new COM('Scripting.FileSystemObject');
											foreach ($fso->Drives as $drive) {
										?>
												<option value="<?php echo $drive->DriveLetter;?>"><?php echo  $drive->DriveLetter;?></option>
										<?php
											}
										?>
									</select>
								</div>
								<div class="static-field overlayfield"> 
								<input type="password" id="adminpassword" name="adminpassword" value="" tabindex="116" class="validate[required]">
							     <label for="adminpassword">Password<span class="mandatoryfildclass">*</span></label>
						        </div>
								<div class="static-field overlayfield performanceop-div">
									<label>Performance Boost Mode<span class="mandatoryfildclass">*</span></label>
									<select class="chzn-select validate[required] ffieldd" id="performanceop" name="performanceop" data-prompt-position="topLeft:14,36" tabindex="1001" data-placeholder="Select">
										<option value="1">Log-Deleted Items</option>
										<option value="2">Estimates</option>
										<option value="3">All</option>
									</select>
								</div>
								<div class="static-field overlayfield accledgerop-div">
									<label>Account Ledger Archive<span class="mandatoryfildclass">*</span></label>
									<select class="chzn-select validate[required] ffieldd" id="accledgerop" name="accledgerop" data-prompt-position="topLeft:14,36" tabindex="1001" data-placeholder="Select">
										<option value="1">Delete All Closed Credit Nos</option>
										<option value="2">Delete All Entry Expect Credit Nos Entry</option>
									</select>
								</div>
							</div>
						</div>
						<div class="alertbuttonarea">
							<input id="locationsubmit" class="alertbtn" type="button" value="Submit" name="locationsubmit" tabindex="1003">
							<input type="button" id="backupoverlayclose" name="" value="Cancel" tabindex="1004" class="alertbtn flloop  alertsoverlaybtn cancelkeyboard" >
							<span class="lasttab" tabindex="1005"></span>
							<!-- hidden fields -->
							<input type="hidden" name="backupopmode" id="backupopmode" value="0" />
							<input type="hidden" name="backupdrive" id="backupdrive" value="<?php echo $location;?>" />
							<input type="hidden" name="backupuser" id="backupuser" value="<?php echo $this->Basefunctions->userid;?>"/>
							<input type="hidden" name="backupstatus" id="backupstatus" value="<?php echo $this->Basefunctions->activestatus;?>"/>
						</div>
					</div>
				</span>
			</form>
		</div>
	</div>
</div>
 <!--- process overlay-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="processoverlay" style="overflow-y: hidden;overflow-x: hidden">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div><div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-2 columns end large-centered">
			<div class="large-12 columns alertsheadercaptionstyle" style="text-align: left; background: #546E7A;">
				<div class="small-12 large-12 medium-12 columns ">
					 &nbsp; Processing...
				</div>
			</div>
		</div>
	</div>
</div>	
	