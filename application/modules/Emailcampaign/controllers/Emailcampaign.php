<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Emailcampaign extends MX_Controller{
	//To load the Register View
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->library('pdf');
		$this->load->library('Mandrill','X9WnMXpxhC8YEOUPuQ3oZw');
		$this->load->model('Emailcampaign/Emailcampaignmodel');
    }
	public function index() {
		$moduleid = array(29);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(29);
		$viewmoduleid = array(29);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Emailcampaign/emailcampaignview',$data);
	}
	//mass update default view fetch
	public function defaultviewfetch() {
		$mid = $_GET['modid'];
		$viewid = $this->Emailcampaignmodel->defaultviewfetchmodel($mid);
		echo $viewid;
	}
	//parent table get
	public function parenttableget() {
		$this->Emailcampaignmodel->parenttablegetmodel();
	}
	//fetch json data information
	public function emailsubscribervalueget() {
		$groupid = $_GET['emaillistsid'];
		$segmentid = $_GET['segmentid'];
		$typeid = $_GET['typeid'];
		$autoupdate = $_GET['autoupdate'];
		$segsubscriberid = $_GET['subscriberid'];
		$creationid = $_GET['viewid'];
		$moduleid = $_GET['moduleid'];
		$information = $this->Basefunctions->gridinformationfetchmodel($creationid);
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$chkbox = $_GET['checkbox'];
		$size = array();
		for($i=0;$i<count($information['colmodelname']);$i++) {
			$size[$i] = '200';
		}
		$colmodelname = $information['colmodelname'];
		$colmodelaliasname = $information['colmodelaliasname'];
		$coltablename = $information['coltablename'];
		$colmodeluitype = $information['colmodeluitype'];
		$colname = $information['colname'];
		$colsize = $size;
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : $primarytable.'.'.$primaryid) : $primarytable.'.'.$primaryid;
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>$colmodelname,'colmodelindex'=>$colmodelaliasname,'coltablename'=>$coltablename,'uitype'=>$colmodeluitype,'colname'=>$colname,'colsize'=>$colsize);
		$result=$this->Emailcampaignmodel->emailsubscribervaluegetgridxmlview($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$information,$moduleid,$groupid,$segmentid,$typeid,$segsubscriberid,$autoupdate);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($result,$page,'');
		$device = $this->Basefunctions->deviceinfo();
		//if($device=='phone') {
			//$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Invite User List',$width,$height,$chkbox);
		//} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Invite User List',$width,$height,$chkbox);
		//}
		echo json_encode($datas);
	}
	//work flow condition header information fetch
	public function maillogdatafetch() {
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$modname = $_GET['modulename'];
		$colinfo = $this->Emailcampaignmodel->maillogdatafetchmodel($modid);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height);
		} else {
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height);
		}
		echo json_encode($datas);
	}
	//fetch json data information
	public function gridvalinformationfetch() {
		$emaillistsid = $_GET['emaillistsid'];
		$segmentid = $_GET['segmentid'];
		$typeid = $_GET['typeid'];
		$autoupdate = $_GET['autoupdate'];
		$subscriberid = $_GET['subscriberid'];
		$creationid = $_GET['viewid'];
		$rowid = $_GET['parentid'];
		$viewcolmoduleids = $_GET['viewfieldids'];
		$grid=$this->Basefunctions->getpagination();
		$result=$this->Emailcampaignmodel->viewdynamicdatainfofetch($grid['sidx'],$grid['sord'],$grid['start'],$grid['limit'],$grid['wh'],$creationid,$rowid,$viewcolmoduleids,$emaillistsid,$segmentid,$typeid,$subscriberid,$autoupdate);
		$pageinfo=$this->Basefunctions->page();
		ob_clean();
		header("Content-type: text/xml;charset=utf-8");
		$s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .=  "<rows>";
        $s .= "<page>".$_GET['page']."</page>";
        $s .= "<total>".$pageinfo['totalpages']."</total>";
        $s .= "<records>".$pageinfo['count']."</records>";
		$count = 0;
		if(count($result[0]) > 0) {
			$count = count($result[0]['colmodelname']);
		}
		foreach($result[1]->result() as $row) {
			$s .= "<row id='". $row->$rowid ."'>";
			for($i=0;$i<$count;$i++) {
				$data = preg_replace('~&~','&amp;', $row->$result[0]['colmodelname'][$i]);
				$s .= "<cell><![CDATA[".$data."]]></cell>";
			}
			$s .= "</row>";
		}
		$s .= "</rows>";  
		echo $s;
	}
	//segment based subscriber id get
	public function relatedsubscriberidget() {  
    	$this->Emailcampaignmodel->relatedsubscriberidgetmodel();
    }
	//folder value get
	public function folderddvalget() {  
    	$this->Emailcampaignmodel->folderddvalgetmodel();
    }
	//editor value fetch 	
	public function editervaluefetch() {
		$filename = $_GET['filename']; 
		$newfilename = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]);
			echo json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			echo json_encode($tccontent);
		}
	}
	//signature editor value fetch 	
	public function sigeditervaluefetch() {
		$filename = $_GET['filename']; 
		$newfilename = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]);
			echo json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			echo json_encode($tccontent);
		}
	}
	//default template get
	public function defeulttemplatevalueget() {
		$this->Emailcampaignmodel->templateddvalgetmodel();
	}
	//default signature get
	public function defeultsignaturevalueget() {
		$this->Emailcampaignmodel->defeultsignaturevaluegetmodel();
	}
	//mail from anme and from mail id get
	public function mailidandfromnameget() {
		$this->Emailcampaignmodel->mailidandfromnamegetmodel();
	}
	// mail schedule time check
	public function scheduletimecalculate() {
		$this->Emailcampaignmodel->scheduletimecalculatemodel();
	}
	// mail sent
	public function campaignmailsent() {
		$this->Emailcampaignmodel->campaignmailsentmodel();
	}
	//template content preview
	public function templatecontentpreview() {
		$content = $this->Emailcampaignmodel->templatecontentpreviewmodel();
		echo $content;
	}
	//template file pdf preview
	public function templatefilepdfpreview() {
		$this->Emailcampaignmodel->templatefilepdfpreviewmodel();
	}
	//segment drop down load
	public function segmentddload() {
		$this->Emailcampaignmodel->segmentddloadmodel();
	}
	// merge template content information
	public function mailmergcontinformationfetch() {
		$this->Emailcampaignmodel->mailmergcontinformationfetchmodel();
	}
	// test campaign mail sent
	public function testmailcampaignsent() {
		$this->Emailcampaignmodel->testmailcampaignsentmodel();
	}
	//mail log overlay
	public function getmaillogdata() {
		$this->Emailcampaignmodel->getmaillogdatamodel();
	}
	//excel report
	public function exceldataexport() {
		$this->Emailcampaignmodel->exceldataexportmodel();
	}
	//resent mail data
	public function resentmaildata() {
		$this->Emailcampaignmodel->resentmaildatamodel();
	}
}