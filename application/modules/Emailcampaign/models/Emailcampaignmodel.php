<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'vendor/autoload.php';
//require 'aws.phar';
//require 'aws/aws-autoloader.php';

use Aws\S3\S3Client;
use Aws\Ses\SesClient;
use Aws\Exception\AwsException;

//Model File
class emailCampaignmodel extends CI_Model{    
    public function __construct() {
		parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//default view fetch based on module
	public function defaultviewfetchmodel($mid) {
		$data = "";
		$this->db->select('viewcreationid');
		$this->db->from('viewcreation');
		$this->db->where('viewcreation.viewcreationmoduleid',$mid);
		$this->db->where('viewcreation.createuserid',1);
		$this->db->where('viewcreation.lastupdateuserid',1);
		$this->db->where('viewcreation.status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$data = $row->viewcreationid;
			}
		}
		return $data;
	}
	//parent table name fetch
	public function parenttablegetmodel() {
		$mid = $_GET['moduleid'];
		$this->db->select('parenttable');
		$this->db->from('modulefield');
		$this->db->where('modulefield.moduletabid',$mid);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$data = $row->parenttable;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//From application - subscriber list view
	public function emailsubscribervaluegetgridxmlview($tablename,$sortcol,$sortord,$pagenum,$rowscount,$colinfo,$viewcolmoduleids,$listid,$segmentid,$typeid,$segsubscriberid,$autoupdate) {
		$count = count($colinfo['colmodelindex']);
		$dataset ='';
		$x = 0;
		$joinq = '';
		$ptab = array($tablename);
		$jtab = array($tablename);
		$maintable = $tablename;
		$selvalue = $tablename.'.'.$tablename.'id';
		for($k=0;$k<$count;$k++) {
			if($k==0) {
				$dataset = $colinfo['colmodelindex'][$k];
			} else{
				$dataset .= ','.$colinfo['colmodelindex'][$k];
			}
			if($x != 0) { $con = "AND"; }
			//parent join check
			$ptabjoin = "";
			$ptabjoin = $colinfo['colmodelparenttable'][$k];
			if(in_array($ptabjoin,$ptab)) {
				if(!in_array($colinfo['coltablename'][$k],$jtab) && $colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
					array_push($jtab,trim($colinfo['coltablename'][$k]));
					//employee
					$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid LIKE "%1%"';
					//group
					$joinq .= ' LEFT OUTER JOIN employeegroup ON FIND_IN_SET(employeegroup.employeegroupid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid  LIKE "%2%"';
					//user role and sub ordinate
					$joinq .= ' LEFT OUTER JOIN userrole ON FIND_IN_SET(userrole.userroleid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND ('.$maintable.'.employeetypeid LIKE "%3%" OR '.$maintable.'.employeetypeid LIKE "%4%")';
				} else {
					if($colinfo['colmodeldatatype'][$k] == 1) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						}
					} else if($colinfo['colmodeldatatype'][$k] == 2) {
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else if($colinfo['colmodeldatatype'][$k] == 3) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						}
					}
				}
			} else {
				if($colinfo['colmodeldatatype'][$k] == 1) {
					$result = $this->Basefunctions->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.','.$maintable.'.'.$primaryid.') > 0 AND '.$ptabjoin.'.status=1';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 1) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
									}
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 2) {
					$joinresult = $this->parentjointableinfo($ptabjoin,$viewcolmoduleids);
					if($joinresult->num_rows() >0) {
						foreach($joinresult->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
							//condition name
							$condcolname = $show->viewcreationcolmodelcondname;
							//condition value
							$condcolvalue = $show->viewcreationcolmodelcondvalue;
						}
						//restrict repeated parent join
						if(in_array($destab,$ptab)) {
							if($destab != $soucrcetab) {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							} else {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							}
						}
						//parent table condition
						if($condcolname != "" && $condcolvalue != "") {
							$attrcond .= " ".$con." ".$srcjointab.'.'.$condcolname."=".$condcolvalue;
							array_push($attrcondarr,$srcjointab.'.'.$joincolname);
							$x = 1;
						}
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] = $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.'='.$maintable.'.'.$primaryid.') > 0 AND '.$ptabjoin.'.status=1';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 2) {
								//attribute join
								if(!in_array($colinfo['coltablename'][$k],$jtab)) {
									array_push($jtab,trim($colinfo['coltablename'][$k]));
									$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
								}
								//condition
								if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
									$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
									array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
									$x = 1;
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 3) {
					$result = $this->Basefunctions->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						}
					}  else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.','.$maintable.'.'.$primaryid.') > 0 AND '.$ptabjoin.'.status=1';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 3) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
									}
								}
							}
						}
					}
				}
			}
			$dduitypeids = array('17','18','19','23','25','26','27','28','29');
			//fields fetch
			if($colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->Basefunctions->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].', GROUP_CONCAT(DISTINCT userrole.userrolename) AS rolename, GROUP_CONCAT(DISTINCT employeegroup.employeegroupname) AS empgrpname';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k].',userrole.userrolename AS rolename,employeegroup.employeegroupname AS empgrpname';
				}
			} else if( in_array($colinfo['colmodeluitype'][$k],$dduitypeids) ) {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->Basefunctions->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].'';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
				}
			} else {
				$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
			}
		}
		$status = $tablename.'.status IN (1)';
		$where = '1=1';
		if($tablename == 'employee') {
			$status = $tablename.'.status NOT IN (3)';
		} else {
			$status = $tablename.'.status IN (1)';
		}
		if($segmentid != '') {
			$segmnt = 'segments.segmentsid='.$segmentid.' AND ';
			$segmntjoin = 'LEFT JOIN segments ON campaigngroups.campaigngroupsid=segments.campaigngroupsid';
		}else {
			$segmnt = '1=1 AND';
			$segmntjoin = '';
		}
		if($listid != '') {
			$maillistid = 'subscribers.campaigngroupsid='.$listid.' AND ';
		}else {
			$maillistid = 'subscribers.campaigngroupsid=0 AND ';
		}
		if($typeid != ''){
			$grouptype = '1=1 AND';//'subscribers.smsgrouptypeid='.$typeid.' AND ';
		}else {
			$grouptype = '1=1 AND';
		}
		if($autoupdate != 'Yes') {
			if($segsubscriberid != '') {
				$segsubscriberid = $segsubscriberid;
				$segmentsubscribercondition = 'AND subscribers.subscribersid IN ('.$segsubscriberid.')';
			} else {
				$segmentsubscribercondition = 'AND 1=1';
			}
		} else {
			$segmentsubscribercondition = 'AND 1=1';
		}
		$industryid = $this->Basefunctions->industryid;
		$industrycond = 'AND subscribers.industryid='.$industryid.'';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		//query
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$selvalue.' from '.$tablename.' '.$joinq.' '.$segmntjoin.' WHERE '.$where.' '.$segmentsubscribercondition.' AND '.$maillistid.' '.$grouptype.' '.$segmnt.' '.$status.' '.$industrycond.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		return $data;
	}
	//segment based subscriber id get
	public function relatedsubscriberidgetmodel() {
		$segmentid = $_POST['segmentid'];
		$industryid = $this->Basefunctions->industryid;
		$data= '';
		$this->db->select('subscribersid,autoupdate');
		$this->db->from('segments');
		$this->db->where('segments.segmentsid',$segmentid);
		$this->db->where('segments.status',1);
		$this->db->where("FIND_IN_SET('$industryid',segments.industryid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data = array('subscriberid'=>$row->subscribersid,'autoupdate'=>$row->autoupdate);
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//sms signature drop down value fetch
	public function smssignatureddvalfetchmodel() {
		$i=0;
		$industryid = $this->Basefunctions->industryid;
		$userid = $this->Basefunctions->userid;
		$this->db->select('signatureid,signaturename,smssignature');
		$this->db->from('signature');
		$this->db->where('signature.employeeid',$userid);
		$this->db->where('signature.status',1);
		$this->db->where("FIND_IN_SET('$industryid',signature.industryid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data[$i] = array('datasid'=>$row->signatureid,'dataname'=>$row->signaturename,'smssig'=>$row->smssignature);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//sms merge template content information
	public function mailmergcontinformationfetchmodel() {
		$this->load->model('Printtemplates/Printtemplatesmodel');
		$resultset = '';
		$moduleid = $_POST['moduleid'];
		$parenttable = $_POST['parenttablename'];
		$recordid = $_POST['recordid'];
		$mergerid = explode(',',$recordid);
		$bodycontent = $_POST['content'];
		//$mergegrp = explode(',',$mergetemp);
		$primaryset = $parenttable.'.'.$parenttable.'id';
		for($i=0;$i<count($mergerid);$i++) {
			$print_data=array('templateid'=>1,'Templatetype'=>'Printtemp','primaryset'=>$primaryset,'primaryid'=>$mergerid[$i]);
			$bodycontent = preg_replace('~a10s~','&nbsp;', $bodycontent);
			$bodycontent = preg_replace('~<br>~','<br />', $bodycontent);
			$bodycontent = $this->Printtemplatesmodel->removespecialchars($bodycontent);
			$bodyhtml = $this->Printtemplatesmodel->generateprintingdata($bodycontent,$parenttable,$print_data,$moduleid);
			$bodyhtml = $this->Printtemplatesmodel->addspecialchars($bodyhtml);
			if($resultset != ''){
				$resultset = $resultset.'|||'.$bodyhtml;
			} else {
				$resultset = $bodyhtml;
			}
		}
		echo json_encode($resultset);
	}
	//folder value fetch
	public function folderddvalgetmodel() {
		$i=0;
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('foldernameid,foldernamename');
		$this->db->from('foldername');
		$this->db->where('foldername.moduleid','209');
		$this->db->where('foldername.status','1');
		$this->db->where("FIND_IN_SET('$industryid',foldername.industryid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->foldernameid,'dataname'=>$row->foldernamename);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//folder value fetch
	public function segmentddloadmodel() {
		$i=0;
		$industryid = $this->Basefunctions->industryid;
		$listid = $_GET['listid'];
		$this->db->select('segmentsid,segmentsname');
		$this->db->from('segments');
		$this->db->where('segments.campaigngroupsid',$listid);
		$this->db->where('segments.status','1');
		$this->db->where("FIND_IN_SET('$industryid',segments.industryid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->segmentsid,'dataname'=>$row->segmentsname);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//template value fetch
	public function templateddvalgetmodel() {
		$i=0;
		$industryid = $this->Basefunctions->industryid;
		$folderid =$_GET['folderid'];
		$typeid =$_GET['typeid'];
		$fromappmod =$_GET['moduleid'];
		$fmodule = explode(',',$fromappmod);
		if(count($fmodule) == '1') {
			$moduleid = $fmodule[0];
		} else {
			$moduleid = '271';
		}
		if($typeid == 2) {
			if($moduleid == '1') {
				$moduleid = '271';
			}
		}
		$tabname ='';
		$this->db->select('emailtemplatesid,emailtemplatesname,emailtemplatesemailtemplate_editorfilename,moduleid');
		$this->db->from('emailtemplates');
		$this->db->where('emailtemplates.foldernameid',$folderid);
		$this->db->where('emailtemplates.status','1');
		$this->db->where("FIND_IN_SET('$industryid',emailtemplates.industryid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$moduleid = $row->moduleid;
				$tabname = $this->Basefunctions->generalinformaion('module','modulemastertable','moduleid',$moduleid);
				$data[$i] = array('datasid'=>$row->emailtemplatesid,'dataname'=>$row->emailtemplatesname,'tempname'=>$row->emailtemplatesemailtemplate_editorfilename,'moduleid'=>$row->moduleid,'modulename'=>$tabname);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//template value fetch
	public function defeultsignaturevaluegetmodel() {
		$i=0;
		$industryid = $this->Basefunctions->industryid;
		$loguserid = $this->Basefunctions->logemployeeid;
		;$this->db->select('signatureid,signaturename,employeesignature_editorfilename');
		$this->db->from('signature');
		$this->db->where('signature.employeeid',$loguserid);
		$this->db->where('signature.status','1');
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->signatureid,'dataname'=>$row->signaturename,'tempname'=>$row->employeesignature_editorfilename);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//module table name get
	public function moduletablenamefetch($moduleid) {
		$i=0;
		$this->db->select('modulemastertable');
		$this->db->from('module');
		$this->db->where('module.moduleid',$moduleid);
		$this->db->where('module.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data= $row->modulemastertable;
			}
		}else{ $data = ''; }
		return $data;
	}
	//email schedule time check
	public function scheduletimecalculatemodel() {
		$scheduledate = $_GET['date'];
		$scheduletime = $_GET['time'];
		$sdate = $scheduledate.' '.$scheduletime;
		date_default_timezone_set("Asia/Kolkata");
		$currentdate = date('d-m-Y H:i:s', time());
		if(strtotime($sdate) > strtotime($currentdate)) {
			$data = 'True';
			echo json_encode($data);
		} else {
			$currenttime = date('H:i:s', time());
			if(strtotime($scheduletime) >= strtotime($currenttime)) {
				$diff = $scheduletime - $currenttime;
				if($diff = '1') {
					$data = 'True';
				} else if($diff = '0') {
					 $data = 'True';
				} else {
					$data = 'True';
				}
			} else {
				$data = 'False';
			}
			echo json_encode($data);
		}
	}
	//mail list based from name and from mail id
	public function mailidandfromnamegetmodel() {
		$listid = $_POST['listid'];
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('fromname,emailid,emaillisttypeid,moduleid,subject');
		$this->db->from('campaigngroups');
		$this->db->where('campaigngroups.campaigngroupsid',$listid);
		$this->db->where('campaigngroups.status',1);
		$this->db->where("FIND_IN_SET('$industryid',campaigngroups.industryid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data = array('fname'=>$row->fromname,'fmail'=>$row->emailid,'typeid'=>$row->emaillisttypeid,'moduleid'=>$row->moduleid,'subject'=>$row->subject);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//campaign mail sent
	public function campaignmailsentmodel() {
		ini_set('max_execution_time', 300);
		$subaccountcreditis = $this->subaccountcreditscheck();
		if($subaccountcreditis != 0) {
			$subaccountid = $this->subaccountdetailsfetchmodel();
			$campname = $_POST['emailcampaignname'];
			$camptype = $_POST['emailcampaigntypeid'];
			$campfname = $_POST['fromname'];
			$campfmailid = $_POST['frommailid'];
			$subject = $_POST['subject'];
			//print_r($camptype); die();
			if(isset($_POST['mailtrackingid'])){
				$camptracking = $_POST['mailtrackingid'];
				$mailtracking = implode(',',$camptracking);
				if($mailtracking == '') {
					$mailtracking = '1';
				}
			} else {
				$mailtracking = '1';
			}
			if(isset($_POST['emailsegmentsid'])){
				$segmentid = $_POST['emailsegmentsid'];
				if($segmentid == '') {
					$segmentid = '1';
				}
			} else {
				$segmentid = '1';
			}
			if(isset($_POST['campaigngroupsid'])){
				$camplist = $_POST['campaigngroupsid'];
				if($camplist == '') {
					$camplist = '1';
				}
			} else {
				$camplist = '1';
			}
			$camptsubcounte = $_POST['totalsubscribercount'];
			$campvalsubcount = $_POST['validsubscribercount'];
			$scheduledate = $_POST['communicationdate'];
			$scheduletime = $_POST['communicationtime'];
			$correctdate = $this->Basefunctions->ymddateconversion($scheduledate);
			$sdate = $correctdate.' '.$scheduletime;
			if(isset($_POST['emailtemplatesid'])){
				$templateid = $_POST['emailtemplatesid'];
				if($templateid == '') {
					$templateid = '1';
				}
			} else {
				$templateid = '1';
			}
			//editor data 
			$content = $_POST['defaultsubseditorval'];
			$recordcontent = explode('|||',$content);
			$attachment = $this->temapltebasedattachmentget($templateid);
			$currentuserid=$this->Basefunctions->userid;
			$date=date($this->Basefunctions->datef);
			if($correctdate != ''){
				$cdate = $correctdate;
			} else {
				$cdate = '0000-00-00';
			}
			$mailcamp = array(
				'emailcampaignname'=>$campname,
				'emailcampaigntypeid'=>$camptype,
				'campaigngroupsid'=>$camplist,
				'segmentsid'=>$segmentid,
				'fromname'=>$campfname,
				'frommailid'=>$campfmailid,
				'mailtrackingid'=>$mailtracking,
				'emailtemplatesid'=>$templateid,
				'totalsubscribercount'=>$camptsubcounte,
				'validsubscribercount'=>$campvalsubcount,
				'scheduledate'=>$cdate,
				'scheduletime'=>$scheduletime,
				'industryid'=>$this->Basefunctions->industryid,
				'branchid'=>$this->Basefunctions->branchid,
				'createdate'=>date($this->Basefunctions->datef),
				'lastupdatedate'=>date($this->Basefunctions->datef),
				'createuserid'=>$this->Basefunctions->userid,
				'lastupdateuserid'=>$this->Basefunctions->userid,
				'status'=>$this->Basefunctions->activestatus
			);
			$this->db->insert('emailcampaign',$mailcamp);
			$mailcapmpaignid = $this->db->insert_id();
			// Create an SesClient. Change the value of the region parameter if you're using an AWS Region other than US West (Oregon). Change the value of the profile parameter if you want to use a profile in your credentials file other than the default.
			/* catch (AwsException $e) {
				// output error message if fails
				/* echo $e->getMessage();
				echo("The email was not sent. Error message: ".$e->getAwsErrorMessage()."\n");
				echo "\n";
			} */
			$char_set = 'UTF-8';
			$SesClient = new SesClient([
				'version'     => 'latest',
				'region'      => 'us-west-2',
				'credentials' => [
					'key'    => 'AKIAJBBGVK7CJSF34IZA',
					'secret' => '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',
				],
			]);
			//record count
			$rcount = count($recordcontent);
			if($rcount != '1') {
				$emailseubscriberid =$_POST['selectedsegmentsubscriberid'];
				$subscriberid = explode(',',$emailseubscriberid);
				for($i=0;$i < count($subscriberid);$i++) {
					$mid = $this->Basefunctions->generalinformaion('subscribers','emailid','subscribersid',$subscriberid[$i]);
					if($mid != '') {
						$recipient_emails = [$mid];
						if(!empty($attachment)){
							try {
								$result = $SesClient->sendEmail([
									'Destination' => ['ToAddresses' => $recipient_emails,],
									'ReplyToAddresses' => [$campfmailid],
									'from_name' => [$campfname],
									'Source' => $campfmailid,
									'Message' => [
										'Body' => ['Html' => ['Charset' => $char_set,'Data' => $recordcontent[$i],],'Text' => ['Charset' => $char_set,'Data' =>  '',],],
										'Subject' => ['Charset' => $char_set,'Data' => $subject,],
										],
									'track_opens' => true,
									'track_clicks' => true,
									'important' => true,
									'url_strip_qs' => true,
									'inline_css' => true,
									'auto_text' => true,
									'merge' => true,
									'auto_html' => true,
									'attachments' => $attachment,
								]);
							} catch (AwsException $e) {
								$e->getMessage();
								$result['MessageId'] = $e->getAwsErrorMessage();
							}
						} else {
							try {
								$result = $SesClient->sendEmail([
									'Destination' => ['ToAddresses' => $recipient_emails,],
									'ReplyToAddresses' => [$campfmailid],
									'from_name' => [$campfname],
									'Source' => $campfmailid,
									'Message' => [
										'Body' => ['Html' => ['Charset' => $char_set,'Data' => $recordcontent[$i],],'Text' => ['Charset' => $char_set,'Data' =>  '',],],
										'Subject' => ['Charset' => $char_set,'Data' => $subject,],
										],
									'track_opens' => true,
									'track_clicks' => true,
									'important' => true,
									'url_strip_qs' => true,
									'inline_css' => true,
									'auto_text' => true,
									'merge' => true,
									'auto_html' => true,
								]);
							} catch (AwsException $e) {
								$e->getMessage();
								$result['MessageId'] = $e->getAwsErrorMessage();
							}
						}
						$sql = $this->db->query("INSERT INTO crmmaillog (communicationfrom,communicationto,subject,employeeid,moduleid,commonid,emailtemplatesid,signatureid,communicationdate,mailstatus,opencount,clickcount,messageuniqueid,createdate,lastupdatedate,createuserid,lastupdateuserid,status) VALUES ('".$campfmailid."','".$mid."','".$subject."','".$currentuserid."','29','".$mailcapmpaignid."','".$templateid."','".$templateid."','".$date."','".$result['@metadata']['statusCode']."','0','0','".$result['MessageId']."','".$date."','".$date."','".$currentuserid."','".$currentuserid."','1')");
						$primaryid = $this->Basefunctions->maximumid('crmmaillog','crmmaillogid');
						//notification log entry
						$notimsg = $campfname." "."Sent a Mail to"." ".$mid." and subject is"." ".$subject;
						$this->Basefunctions->notificationcontentadd($primaryid,'Mail',$notimsg,1,242);
						$credit = $this->db->query("UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = `addonavailablecredit` -1 WHERE `addonstypeid` = '3'");
						//master company id get - master db update
						$mastercompanyid = $this->Basefunctions->generalinformaion('company','mastercompanyid','companyid',2);
						$CI =& get_instance();
						$CI->load->database();
						$hostname = $CI->db->hostname; 
						$username = $CI->db->username; 
						$password = $CI->db->password; 
						$masterdb = $CI->db->masterdb; 
						$con = mysqli_connect($hostname,$username,$password,$masterdb);
						// Check connection
						if (mysqli_connect_errno())	{
							echo "Failed to connect to MySQL: " . mysqli_connect_error();
						}
						mysqli_query($con,"UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = `addonavailablecredit` - 1 WHERE `addonstypeid` = 3 and `mastercompanyid` ='".$mastercompanyid."'");
						mysqli_close($con);
					}
				}
			} else {
				$emailseubscriberid =$_POST['selectedsegmentsubscriberid'];
				$subscriberid = explode(',',$emailseubscriberid);
				for($i=0;$i < count($subscriberid);$i++) {
					$mid = $this->Basefunctions->generalinformaion('subscribers','emailid','subscribersid',$subscriberid[$i]);
					if($mid != '') {
						$recipient_emails = [$mid];
						if(!empty($attachment)){
							try {
								$result = $SesClient->sendEmail([
									'Destination' => ['ToAddresses' => $recipient_emails,],
									'ReplyToAddresses' => [$campfmailid],
									'from_name' => [$campfname],
									'Source' => $campfmailid,
									'Message' => [
										'Body' => ['Html' => ['Charset' => $char_set,'Data' => $content,],'Text' => ['Charset' => $char_set,'Data' =>  '',],],
										'Subject' => ['Charset' => $char_set,'Data' => $subject,],
										],
									'track_opens' => true,
									'track_clicks' => true,
									'important' => true,
									'url_strip_qs' => true,
									'inline_css' => true,
									'auto_text' => true,
									'merge' => true,
									'auto_html' => true,
									'attachments' => $attachment,
								]);
							} catch (AwsException $e) {
								$e->getMessage();
								$messageId = $e->getAwsErrorMessage();
							}
						} else {
							try {
								$result = $SesClient->sendEmail([
									'Destination' => ['ToAddresses' => $recipient_emails,],
									'ReplyToAddresses' => [$campfmailid],
									'from_name' => [$campfname],
									'Source' => $campfmailid,
									'Message' => [
										'Body' => ['Html' => ['Charset' => $char_set,'Data' => $content,],'Text' => ['Charset' => $char_set,'Data' =>  '',],],
										'Subject' => ['Charset' => $char_set,'Data' => $subject,],
										],
									'track_opens' => true,
									'track_clicks' => true,
									'important' => true,
									'url_strip_qs' => true,
									'inline_css' => true,
									'auto_text' => true,
									'merge' => true,
									'auto_html' => true,
								]);
							} catch (AwsException $e) {
								$e->getMessage();
								$messageId = $e->getAwsErrorMessage();
							}
						}
						$sql = $this->db->query("INSERT INTO crmmaillog (communicationfrom,communicationto,subject,employeeid,moduleid,commonid,emailtemplatesid,signatureid,communicationdate,mailstatus,opencount,clickcount,messageuniqueid,createdate,lastupdatedate,createuserid,lastupdateuserid,status) VALUES ('".$campfmailid."','".$mid."','".$subject."','".$currentuserid."','29','".$mailcapmpaignid."','".$templateid."','".$templateid."','".$date."','".$result['@metadata']['statusCode']."','0','0','".$result['MessageId']."','".$date."','".$date."','".$currentuserid."','".$currentuserid."','1')");
						//notification log entry
						$primaryid = $this->Basefunctions->maximumid('crmmaillog','crmmaillogid');
						$notimsg = $campfname." "."Sent a Mail to"." - ".$mid." and subject is"." ".$subject;
						$this->Basefunctions->notificationcontentadd($primaryid,'Mail',$notimsg,1,242);
						$credit = $this->db->query("UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = `addonavailablecredit` -1 WHERE `addonstypeid` = '3'");
						//master company id get - master db update
						$mastercompanyid = $this->Basefunctions->generalinformaion('company','mastercompanyid','companyid',2);
						//Master DB update
						$CI =& get_instance();
						$CI->load->database();
						$hostname = $CI->db->hostname; 
						$username = $CI->db->username; 
						$password = $CI->db->password; 
						$masterdb = $CI->db->masterdb; 
						$con = mysqli_connect($hostname,$username,$password,$masterdb);
						// Check connection
						if (mysqli_connect_errno())	{
							echo "Failed to connect to MySQL: " . mysqli_connect_error();
						}
						mysqli_query($con,"UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = `addonavailablecredit` - 1 WHERE `addonstypeid` = 3 and mastercompanyid ='".$mastercompanyid."'");
						mysqli_close($con);
					}
				}
			} 
			echo 'TRUE'; 
		} else {
			echo 'COUNT';
		}
	}
	//mail notification sent
	public function mailsentfunction($message,$send_at) {
		try {
			$mandrill = new Mandrill('X9WnMXpxhC8YEOUPuQ3oZw');
			$async = true; //for bulk message
			$ip_pool = '';
			$result = $mandrill->messages->send($message, $async, $ip_pool, $send_at);
			return $result;
		} catch(Mandrill_Error $e) {
			// Mandrill errors are thrown as exceptions
			echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			throw $e;
		}
	}
	//template content preview
	public function templatecontentpreviewmodel() {
		$templateid = $_POST['templateid'];
		$headerfile = "";
		$contentfile = "";
		$footerfile = "";
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('printtemplates.printtemplatestemplate_editorfilename,printheader.printheader_editorfilename,printfooter.printfooter_editorfilename');
		$this->db->from('printtemplates');
		$this->db->join('printheader','printheader.printheaderid=printtemplates.printheaderid','left outer');
		$this->db->join('printfooter','printfooter.printfooterid=printtemplates.printfooterid','left outer');
		$this->db->where('printtemplates.printtemplatesid',$templateid);
		$this->db->where("FIND_IN_SET('$industryid',printtemplates.industryid) >", 0);
		$result = $this->db->get();
		foreach($result->result() as $rowdata) {
			$contentfile = $rowdata->printtemplatestemplate_editorfilename;
		}
		//html content
		$bodycontent = $this->filecontentfetch($contentfile);
		$htmlcontent = $headercontent.$bodycontent.$footercontent;
		return $htmlcontent;
	}
	//preview and print pdf
	public function templatefilepdfpreviewmodel() {
		$headerfile = "";
		$contentfile = "";
		$footerfile = "";
		$moduleid = 1;
		$marginleft = 10;
		$marginright = 10;
		$margintop = 10;
		$marginbottom = 20;
		$orientation ='P';
		$pageformat = 'A4';
		$industryid = $this->Basefunctions->industryid;
		$templateid = ( (isset($_POST['templateid']))? $_POST['templateid'] : 1);
		$content = ( (isset($_POST['content']))? $_POST['content'] : '');
		$templatename = ( (isset($_POST['templatename']))? $_POST['templatename'] :'Printpdf');
		$option = ( (isset($_POST['templatoption']))? $_POST['templatoption'] :'');
		$id = ( (isset($_POST['primaryid']))? $_POST['primaryid'] :1);
		//echo $id;
		$this->db->select('emailtemplates.emailtemplatesemailtemplate_editorfilename,emailtemplates.moduleid');
		$this->db->from('emailtemplates');
		$this->db->where('emailtemplates.emailtemplatesid',$templateid);
		$this->db->where("FIND_IN_SET('$industryid',emailtemplates.industryid) >", 0);
		$result = $this->db->get();
		foreach($result->result() as $rowdata) {
			$contentfile = $rowdata->emailtemplatesemailtemplate_editorfilename;
			$moduleid =  $rowdata->moduleid;
		}
		//pdf generation
		$pdf = new TCPDF($orientation, PDF_UNIT, $pageformat, true, 'UTF-8', false);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetTitle('PDF Reports');
		$pdf->SetPrintFooter(false);
		$pdf->SetPrintHeader(false);
		$pdf->SetMargins($marginleft,$margintop,$marginright,false);// set margins
		$pdf->SetAutoPageBreak(TRUE, 20);//auto page break
		$pdf->SetTextColor(82,82,83);//font color
		$pdf->SetLineWidth(0.2);
		$pdf->SetFont('dejavusans', '', 10);
		$pdf->AddPage($orientation,$pageformat);
		//fetch parent table
		$this->db->select('modulename,moduleid,modulemastertable');
		$this->db->from('module');
		$this->db->where('moduleid',$moduleid);
		$result = $this->db->get();
		foreach($result->result() as $row) {
			$parenttable = $row->modulemastertable;
		}
		//html content
		$bodycontent = $content;
		//header data
		//body data
		$bodycontent = preg_replace('~a10s~','&nbsp;', $bodycontent);
		$bodycontent = preg_replace('~<br>~','<br />', $bodycontent);
		$bodycontent = $this->removespecialchars($bodycontent);
		$bodyhtml = $this->gettablerowmappedcontent($bodycontent,$parenttable,$id,$moduleid);
		$bodyhtml = $this->addspecialchars($bodyhtml);
//create html content
		$htmldatacontents = <<<EOD
<style>
  table,td {
	padding:2px 2px 2px 2px;
  }
  th{
	padding-top:7px;
	align:"center";
  }
  tr{
	page-break-inside:avoid;
  }
</style>
$headerhtml $bodyhtml $footerhtml
EOD;
		//exit();
		ob_end_clean();
		error_reporting(0); // disable errors
		$pdf->writeHTML($htmldatacontents, true, false, true, false, '');
		$tempname = preg_replace('/[^A-Za-z0-9]/', '', $templatename);
		$name ='templates'.DIRECTORY_SEPARATOR.$tempname.'_'.date("YmdHis").mt_rand(1,999999).'.pdf';
		$pdf->Output($name,$option);
}
	//fetch inside the table tr row content
	public function gettablerowmappedcontent($datacontent,$parenttable,$id,$moduleid) {
		$htmlcontent = $datacontent;
		$finalcont = $datacontent;
		$tabpattern = "/<table ?.*>.*<\/table>/U";
		preg_match_all($tabpattern,$datacontent,$tabmatches);
		foreach($tabmatches as $tabmatch) { //contain more than one table(s)
			$tabcontent="";
			foreach($tabmatch as $tabcontentmatach) {//contain single table
				$tabcontent = $tabcontentmatach;
				$trpattern = "/<tr ?.*>.*<\/tr>/U";
				preg_match_all($trpattern,$tabcontentmatach, $trmatches);
				foreach($trmatches as $trmatch) {
					$prevtrcontent="";
					foreach($trmatch as $trdata) {
						$prevtrcontent = $trdata;
						$tdpattern = "/<td ?.*>.*<\/td>/U";
						preg_match_all($tdpattern,$trdata, $tdmatches);
						foreach($tdmatches as $tdmatch) {//contain multiple td in single row
							$matchelement = array();
							$matcheledata = array();
							$prevmatchelem = array();
							$tabrowcellcount = 0;
							$tdmatchcountcheck = "true";
							//check row contain multiple td cells
							foreach($tdmatch as $tddata) { //check cell td contents
								preg_match_all ("/{.*}/U", $tddata,$contentmatch); //content match
								foreach($contentmatch as $mccontent) { //check table cell contain multiple elements 
									if(count($mccontent) >= 1) {
										$tabrowcellcount++;
									}
								}
							}
							$tdmatchcountcheck = ( (count($tdmatch)>1 && $tabrowcellcount>1)? "true":"false" );
							if($tdmatchcountcheck == "false") { //if table cell contain multiple value and single row td
								foreach($tdmatch as $tddata) {
									$pretdcontent = $tddata;
									preg_match_all ("/{.*}/U", $tddata, $contentmatch); //content match
									foreach($contentmatch as $mcontent) { //individual td data
										foreach($mcontent as $cellmatachdata) { //fetch data for td matches
											if( $cellmatachdata != '{S.Number}' ) {
												$displaydata = $this->mergcontinformationfetchmodel($cellmatachdata,$parenttable,$id,$moduleid);
												if(count($displaydata)>1) {
													$data = $displaydata[0];
												} else {
													$data = $displaydata[0];
												}
											} else {
												$data = " ";
											}
											$tddata = preg_replace('~'.$cellmatachdata.'~',$data, $tddata); //cell data replace
										}
									}
									$trdata = preg_replace('~'.$pretdcontent.'~',$tddata,$trdata); //td data replace to row
								}
								$tabcontentmatach = preg_replace('~'.$prevtrcontent.'~',$trdata,$tabcontentmatach);//row data replace to table
							} else {
								foreach($tdmatch as $tddata) { //if table cell contain single value
									$pretdcontent = $tddata;
									preg_match_all ("/{.*}/U", $tddata, $contentmatch); //content match
									foreach($contentmatch as $mcontent) { //individual td data
										foreach($mcontent as $cellmatachdata) { //fetch data for td matches
											if( $cellmatachdata == '{S.Number}' ) {
												$displaydata = 'ss890ss';
											} else {
												$displaydata = $this->mergcontinformationfetchmodel($cellmatachdata,$parenttable,$id,$moduleid);
											}
											$matchelement[] = $cellmatachdata;
											$matcheledata[] = $displaydata;
										}
									}
								}
								//multiple content mapping
								$datas = array();
								$slnumkey = array_search('ss890ss',$matcheledata,false);
								$sno = 1;
								$sm = 0;
								//convert to row data based on column
								foreach($matcheledata as $key => $matchvals) {
									$m=0;
									$sno = 1;
									if($slnumkey !== false) { //sno available
										if(is_array($matchvals)) {
											foreach($matchvals as $val) {
												$datas[$m][$slnumkey] = $sno;
												$datas[$m][$key] = $val;
												$m++;
												$sno++;
											}
										} else if($matchvals != 'ss890ss') {
											$datas[$m][$key] = $matchvals;
											$m++;
										}
									} else { //sno is not available
										if(is_array($matchvals)) {
											foreach($matchvals as $val) {
												$datas[$m][$key] = $val;
												$m++;
											}
										} else {
											$datas[$m][$key] = $matchvals;
											$m++;
										}
									}
								}
								$trdatasets = "";
								$trdataset = "";
								if(count($datas) > 0) {
									foreach($datas as $matchdata) { //each row
										$trdataset = $trdata;
										$h=0;
										for($i=0;$i<count($matchdata);$i++) {
										//foreach($matchdata as $val) { //each column
											if(isset($matchdata[$i])) {
												if(is_array($matchdata[$i])) {
													if(count($matchdata)>1) {
														$data = implode('<br/>',$matchdata[$i]);
													} else {
														$data = implode(',',$matchdata[$i]);
													}
												} else {
													$data = $matchdata[$i];
												}
												$trdataset = preg_replace('~'.$matchelement[$h].'~',$data,$trdataset); //td data replace to row
												$h++;
											}
										//}
										}
										$trdatasets .= $trdataset;
									}
									$trdata =  $trdatasets;
								}
								$tabcontentmatach = preg_replace('~'.$prevtrcontent.'~',$trdata,$tabcontentmatach);//row data replace to table
							}
							$tabcontentmatach = preg_replace('~'.$prevtrcontent.'~',$trdata,$tabcontentmatach);//row data replace to table
						}
						$tabcontentmatach = preg_replace('~'.$prevtrcontent.'~',$trdata,$tabcontentmatach);//row data replace to table
					}
					$htmlcontent = preg_replace('~'.$tabcontent.'~',$tabcontentmatach,$htmlcontent); //table data replace to content
					$finalcont = $htmlcontent;
				}
 			}
		}
		preg_match_all ("/{.*}/U", $finalcont, $contmatch);
		foreach($contmatch as $bocontent) {
			foreach($bocontent as $bomatch) {
				if($bomatch != "{S.Number}") {
					$htmldata = $this->mergcontinformationfetchmodel($bomatch,$parenttable,$id,$moduleid);
					$finalcont = preg_replace('~'.$bomatch.'~',implode(',',$htmldata),$finalcont);
				} else {
					$finalcont = preg_replace('~'.$bomatch.'~','',$finalcont);
				}
			}
		}
		return $finalcont;
		//echo $finalcont;
	}
	//read file
	public function filecontentfetch($filename) {
		$filecontent = "";
		if( file_exists($filename) && is_readable($filename) ) {
			$newfilename = explode('/',$filename);
			$myfile = @fopen('templates'.DIRECTORY_SEPARATOR.$newfilename[1], "r");
			$filecontent = fread($myfile,filesize('templates'.DIRECTORY_SEPARATOR.$newfilename[1]));
			fclose($myfile);
		}
		return $filecontent;
	}
	//remove special chars
	public function removespecialchars($content) {
		$symbols = array('+','-','/','%','~','!','@','#','$','^','&','*','_','=','|','(',')');
		foreach($symbols as $symbol) {
			switch($symbol) {
				case '(':
					$content = preg_replace('~\(~','890sss', $content);
					break;
				case ')':
					$content = preg_replace('~\)~','sss890', $content);
					break;
				case '+':
					$content = preg_replace('~\+~','ssplus890', $content);
					break;
				case '~':
					$content = preg_replace('~\~~','ssstilt890', $content);
					break;
				case '!':
					$content = preg_replace('~\!~','sssastr890', $content);
					break;
				case '@':
					$content = preg_replace('~\@~','sssat890', $content);
					break;
				case '#':
					$content = preg_replace('~\#~','ssshash890', $content);
					break;
				case '^':
					$content = preg_replace('~\^~','ssscap890', $content);
					break;
				case '*':
					$content = preg_replace('~\*~','sssstar890', $content);
					break;
				case '=':
					$content = preg_replace('~\=~','ssseqw890', $content);
					break;
				case '|':
					$content = preg_replace('~\|~','ssspipe890', $content);
					break;
			}
		}
		return $content;
	}
	//remove special chars
	public function addspecialchars($content) {
		$symbols = array('+','-','/','%','~','!','@','#','$','^','&','*','_','=','|','(',')');
		foreach($symbols as $symbol) {
			switch($symbol) {
				case '(':
					$content = preg_replace('~890sss~','(', $content);
					break;
				case ')':
					$content = preg_replace('~sss890~',')', $content);
					break;
				case '+':
					$content = preg_replace('~ssplus890~','+', $content);
					break;
				case '~':
					$content = preg_replace('~ssstilt890~','~', $content);
					break;
				case '!':
					$content = preg_replace('~sssastr890~','!', $content);
					break;
				case '@':
					$content = preg_replace('~sssat890~','@', $content);
					break;
				case '#':
					$content = preg_replace('~ssshash890~','#', $content);
					break;
				case '^':
					$content = preg_replace('~ssscap890~','^', $content);
					break;
				case '*':
					$content = preg_replace('~sssstar890~','*', $content);
					break;
				case '=':
					$content = preg_replace('~ssseqw890~','=', $content);
					break;
				case '|':
					$content = preg_replace('~ssspipe890~','|', $content);
					break;
			}
		}
		return $content;
	}
	//merge content data fetch
	public function mergcontinformationfetchmodel($mergegrp,$parenttable,$id,$moduleid) {
		$relmodids = $this->fetchrelatedmodulelist($moduleid);
		$primaryid = $id;
		$database = $this->db->database;
		$resultset = array();
		$mergeindval = $mergegrp;
		$formatortype = "1";
		$id = $primaryid;
		//parent child field concept
		$elname = array('parentcategoryid','parentstoragecategoryid','parentaccountid');
		$fname = array('categoryname','storagecategoryname','accountname');
		$fnameid = array('categoryid','storagecategoryid','accountid');
		$tname = array('category','storagecategory','account');
		
		$empid = $this->Basefunctions->userid;
			$newdata = array();
			$tempval = substr($mergeindval,1,(strlen($mergeindval)-2));
			$newval = explode('.',$tempval);
			$reltabname = explode(':',$newval[0]);
			$table = $reltabname[0];
			$uitypeid = 2;
			//chk editor field name
			$chkeditfname = explode('_',$newval[1]);
			$editorfname = ( (count($chkeditfname) >= 2)? $chkeditfname[1] : '');
			if($table!='REL' && $table!='GEN') { //parent table fields information
				$tablename = trim($reltabname[0]);
				$tabfield = $newval[1];
				//field fetch parent table fetch
				$fieldnamechk = explode('-',$tabfield);
				$fldname = ( (count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
				$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
				$uitypeid = ( (count($fieldinfo) > 0)? $fieldinfo['uitype'] : 1);
				if($fldname != 'attributesetid') {
					//check its parent child field concept
					if(in_array($fldname,$elname)) {
						$key = array_search($fldname,$elname);
						$pid = $id;
						if($key != NULL) {
							//parent id get
							$result = $this->db->select("$tname[$key].$elname[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$id)->where($tname[$key].'.status',1)->get();
							if($result->num_rows() > 0) {
								foreach($result->result()as $row) {
									$pid=$row->$elname[$key];
								}
								if($pid!=0) {
									//parent name
									$result = $this->db->select("$tname[$key].$fname[$key],$tname[$key].$fnameid[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$pid)->where($tname[$key].'.status',1)->get();
									if($result->num_rows() > 0) {
										foreach($result->result()as $row) {
											$resultset[]=$row->$fname[$key];
										}
									}
								}
							}
						}
					}
					else {
						//field fetch parent table fetch
						$fieldnamechk = explode('-',$tabfield);
						$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
						$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
						if(count($fieldinfo)>0) {
							$uitypeid = $fieldinfo['uitype'];
							//fetch parent table join id
							$relpartable = $fieldinfo['partab'];
							$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
							$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
							$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
							//fetch original field of picklist data in view creation 
							$viewfieldinfo = $this->viewfieldsinformation($tablename,$tabfield,$fieldinfo['modid']);
							$tabfieldname = ( (count($viewfieldinfo)>0)? $viewfieldinfo['joinfieldname'] : $tabfield );
							$jointabfieldid = ( (count($viewfieldinfo)>0)? $viewfieldinfo['condfiledname'] : $tabfield );
							//join relation table and main table
							$joinq="";
							if($relpartable!=$tablename) {
								$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id AND '.$relpartable.'.status=1';
							}
							//main table join
							if($mainjointable!=$parenttable) {
								$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id AND '.$relpartable.'.status=1';
							}
							//if its picklist data
							$newfield = substr( $tabfieldname,( strlen($tabfieldname)-2 ));
							$tabname = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
							$tbachk = $this->tableexitcheck($database,$tabname);
							if( $newfield == "id" && $tbachk != 0 ) {
								$newtable = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
								$newjfield = $newtable."name";
								$whtable = (($tablename=="company" || $tablename=="branch")? $tablename : $parenttable);
								$newjdata = $this->db->query("SELECT ".$newtable.".".$newtable."id AS columdataid,".$newtable.".".$newjfield." AS columdata FROM ".$newtable." LEFT OUTER JOIN ".$tablename." ON ".$tablename.".".$tabfieldname." = ".$newtable.".".$tabfieldname."".$joinq." WHERE ".$tablename.".".$whtable."id = ".$id." AND ".$newtable.".status=1 AND ".$tablename.".status=1",false);
								if($newjdata->num_rows() >0) {
									foreach($newjdata->result() as $jkey) {
										$resultset[] = $jkey->columdata;
									}
								} else {
									$resultset[] = " ";
								}
							}
							else {
								//main table data sets information fetching
								$fldcountchk = explode('-',$tabfield);
								$tablcolumncheck = $this->tablefieldnamecheck($tablename,$parenttable."id");
								if( ( $parenttable == $tablename && (count($fldcountchk)) < 2 ) || ( $tablcolumncheck == 'false' && (count($fldcountchk)) < 2 ) ) {
									$newdata = $this->db->select("".$tabfield." AS columdata")->from($tablename)->where($tablename.".".$tablename."id",$id)->where($tablename.".status",1)->get()->result();
								}
								else {
									$cond = "";
									$addfield = explode('-',$tabfield);
									if( (count($addfield)) >= 2 ) {
										$datafiledname=$addfield[1];
										if(in_array('PR',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=2';
										} else if(in_array('SE',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=3'; 
										} else if(in_array('BI',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=4';
										} else if(in_array('SH',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=5';
										} else if(in_array('CW',$addfield)) {
											$formatortype = "CW";
										} else if(in_array('CF',$addfield)) {
											$formatortype = "CF";
										} else if(in_array('CS',$addfield)) {
											$formatortype = "CS";
										}
										if($tablename!=$parenttable) {
											$tablcolumncheck = $this->tablefieldnamecheck($tablename,"addresstypeid");
											$cond = ( ($tablcolumncheck == 'true')? $cond : ' ');
											$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename." JOIN ".$parenttable." ON ".$parenttable.".".$parenttable."id = ".$tablename.".".$parenttable."id WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1".$cond."")->result();
										} else {
											$newdata = $this->db->select(''.$datafiledname.' AS columdata',false)->from($tablename)->where($tablename.'id',$id)->where($tablename.'.status',1)->get()->result();
										}
									}
									else {
										$newdata = $this->db->query("SELECT ".$tabfield." AS columdata FROM ".$tablename." JOIN ".$parenttable." ON ".$parenttable.".".$parenttable."id = ".$tablename.".".$parenttable."id WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1")->result();
									}
								}
								foreach($newdata as $key) {
									if($tabfield == 'companylogo') {
										$imgname = $key->columdata;
										if(filter_var($imgname,FILTER_VALIDATE_URL)) {
											$resultset[] = '<img src="'.$imgname.'" />';
										} else {
											if( file_exists($imgname) ) {
												$resultset[] = '<img src="'.$imgname.'" style="width:11em; height:3em" />';
											} else {
												$resultset[] = " ";
											}
										}
									}
									else {
										$currencyinfo = $this->currencyinformation($parenttable,$id,$moduleid);
										if($formatortype=="CW") { //convert to word
											if(count($currencyinfo)>0) {
												$resultset[] = $this->convert->numtowordconvert($key->columdata,$currencyinfo['name'],$currencyinfo['subcode']);
											} else {
												$resultset[] = $this->convert->numtowordconvert($key->columdata);
											}
										} else if($formatortype=="CF") { //currenct format
											if(count($currencyinfo)>0) {
												$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
											} else {
												$resultset[] = $this->convert->formatcurrency($key->columdata);
											}
										} else if($formatortype=="CS") { //currency with symbol
											if(count($currencyinfo)>0) {
												$amt = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
												$resultset[] = $currencyinfo['symbol'].' '.$amt;
											} else {
												$resultset[] = $currencyinfo['symbol'].' '.$this->convert->formatcurrency($key->columdata);
											}
										} else {
											if(is_numeric($key->columdata)) {
												if($uitypeid == '4' || $uitypeid == '5' || $uitypeid == '7') {
													if(count($currencyinfo)>0) {
														$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
													} else {
														$resultset[] = $this->convert->formatcurrency($key->columdata);
													}
												} else {
													$resultset[] = $key->columdata;
												}
											} else {
												if($editorfname=='editorfilename') {
													if( file_exists($key->columdata) ) {
														$datas = $this->geteditorfilecontent($key->columdata);
														$datas = preg_replace('~a10s~','&nbsp;', $datas);
														$datas = preg_replace('~<br>~','<br />', $datas);
														$resultset[] = $datas;
													} else {
														$resultset[] = '';
													}
												} else {
													$resultset[] = $key->columdata;
												}
											}
										}
									}
								}
							}
						}
					}
				} 
				else {
					//attribute value fetch for non relational modules
					$fieldnamechk = explode('-',$tabfield);
					$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
					$fieldinfo = $this->fetchfieldparenttable($table,$fldname);
					if(count($fieldinfo)>0) {
						$uitypeid = $fieldinfo['uitype'];
						//fetch parent table join id
						$relpartable = $fieldinfo['partab'];
						$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
						$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
						$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
						//join relation table and main table
						$joinq="";
						if($parenttable!=$table) {
							$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$table.'.'.$parenttable.'id';
						}
						//fetch attribute name and values
						$i=0;
						$datasets = array();
						$attrsetids = $this->db->query(" SELECT ".$table.".".$table."id as Id,".$table.".attributesetid as attrsetid,".$table.".attributes as attributeid,".$table.".attributevalues as attributevalueid FROM ".$table.$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$mainjointable.".status=1 AND ".$table.".status=1",false)->result();
						foreach($attrsetids as $attrset) {
							$datasets[$i] = array('id'=>$attrset->Id,'attrsetid'=>$attrset->attrsetid,'attrnames'=>$attrset->attributeid,'attrvals'=>$attrset->attributevalueid);
							$i++;
						}
						$resultset = array();
						foreach($datasets as $set) {
							$valid = explode('_',$set['attrvals']);
							$nameid = explode('_',$set['attrnames']);
							$n=1;
							$vi = 0;
							$attrdatasets = array();
							for($h=0;$h<count($valid);$h++) {
								$attrval = '';
								$attrname = '';
								if($valid[$h]!='') {
									//attribute value fetch
									$attrvaldata = $this->db->query(' SELECT attributevalueid,attributevaluename FROM attributevalue WHERE attributevalueid='.$valid[$h].' AND status=1',false)->result();
									foreach($attrvaldata as $valsets) {
										$attrval = $valsets->attributevaluename;
									}
									//attribute name fetch
									$attrnamedata = $this->db->query(' SELECT attributeid,attributename FROM attribute WHERE attributeid='.$nameid[$h].' AND status=1',false)->result();
									foreach($attrnamedata as $namesets) {
										$attrname = $namesets->attributename;
									}
								}
								$attrset = "";
								if( $attrval!='' ) {
									$attrset = $n.'.'.$attrname.': '.$attrval;
									++$n;
								}
								$attrdatasets[$vi] = $attrset;
								$vi++;
							}
							$resultset[] = implode('<br />',$attrdatasets);
						}
					}
				}
			} 
			else if($table=='REL') { //related module fields information fetch
				$tablename = trim($reltabname[1]);
				$tabfield = trim($newval[1]);
				$fieldnamechk = explode('-',$tabfield);
				$fldname = ( (count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
				if($fldname != 'attributesetid') {
					//get user company & branch id
					//check its parent child field concept
					$fldnamechk = explode('-',$tabfield);
					$fldname = ( (count($fldnamechk) >= 2)? $fldnamechk[1] : $tabfield);
					if(in_array($fldname,$elname)) {
						$key = array_search($fldname,$elname);
						$pid = $id;
						if($key != NULL) {
							//parent id get
							$result = $this->db->select("$tname[$key].$elname[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$id)->where($tname[$key].'.status',1)->get();
							if($result->num_rows() > 0) {
								foreach($result->result()as $row) {
									$pid=$row->$elname[$key];
								}
								if($pid!=0) {
									//parent name
									$result = $this->db->select("$tname[$key].$fname[$key],$tname[$key].$fnameid[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$pid)->where($tname[$key].'.status',1)->get();
									if($result->num_rows() > 0) {
										foreach($result->result()as $row) {
											$resultset[]=$row->$fname[$key];
										}
									}
								}
							}
						}
					}
					else {
						//field fetch parent table fetch
						$fieldnamechk = explode('-',$tabfield);
						$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
						$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
						if(count($fieldinfo)>0) {
							$uitypeid = $fieldinfo['uitype'];
							//fetch parent table join id
							$relpartable = $fieldinfo['partab'];
							$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
							$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
							$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
							//fetch original field of picklist data in view creation 
							$viewfieldinfo = $this->viewfieldsinformation($tablename,$tabfield,$fieldinfo['modid']);
							$tabfieldname = ( (count($viewfieldinfo)>0)? $viewfieldinfo['joinfieldname'] : $tabfield );
							$jointabfieldid = ( (count($viewfieldinfo)>0)? $viewfieldinfo['condfiledname'] : $tabfield );
							//join relation table and main table
							$joinq="";
							if($relpartable!=$tablename) {
								$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id AND '.$relpartable.'.status=1';
							}
							//main table join
							if($mainjointable==$parenttable) {
								$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id AND '.$mainjointable.'.status=1';
							} else {
								$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id AND '.$mainjointable.'.status=1';
								$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$mainjointable.'.'.$parenttable.'id AND '.$parenttable.'.status=1';
							}
							//if its picklist data
							$newfield = substr( $tabfieldname,( strlen($tabfieldname)-2 ));
							$tabname = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
							$tblchk = $this->tableexitcheck($database,$tabname);
							if( $newfield == "id" && $tblchk != 0 ) {
								$newtable = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
								$newjfield = $newtable."name";
								$whtable = (($tablename=="company" || $tablename=="branch")? $tablename : $parenttable);
								$newjdata = $this->db->query("SELECT ".$newtable.".".$newtable."id AS columdataid,".$newtable.".".$newjfield." AS columdata FROM ".$newtable." LEFT OUTER JOIN ".$tablename." ON ".$tablename.".".$jointabfieldid." = ".$newtable.".".$tabfieldname."".$joinq." WHERE ".$parenttable.".".$whtable."id = ".$id." AND ".$mainjointable.".status=1 AND ".$newtable.".status=1 AND ".$tablename.".status=1");
								if($newjdata->num_rows() >0) {
									foreach($newjdata->result() as $jkey) {
										$resultset[] = $jkey->columdata;
									}
								} else {
									$resultset[] = " ";
								}
							}
							else {
								//main table data sets information fetching
								$fldcountchk = explode('-',$tabfield);
								$tablcolumncheck = $this->tablefieldnamecheck($tablename,$parenttable."id");
								if( ( $parenttable == $tablename && (count($fldcountchk)) < 2 ) || ( $tablcolumncheck == 'false' && (count($fldcountchk)) < 2 ) ) {
									$newdata = $this->db->query("SELECT ".$tablename.".".$tabfield." AS columdata FROM ".$tablename."".$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1")->result();
								}
								else {
									$cond = "";
									$addfield = explode('-',$tabfield);
									if( (count($addfield)) >= 2 ) {
										$datafiledname=$addfield[1];
										if(in_array('PR',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=2';
										} else if(in_array('SE',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=3'; 
										} else if(in_array('BI',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=4';
										} else if(in_array('SH',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=5';
										} else if(in_array('CW',$addfield)) {
											$formatortype = "CW";
										} else if(in_array('CF',$addfield)) {
											$formatortype = "CF";
										} else if(in_array('CS',$addfield)) {
											$formatortype = "CS";
										}
										$tablcolumncheck = $this->tablefieldnamecheck($tablename,"addresstypeid");
										$cond = ( ($tablcolumncheck == 'true')? $cond : ' ');
										if($tablename!=$parenttable) {
											$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename."".$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1".$cond."")->result();
										} else {
											$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename." WHERE ".$tablename."id = ".$id." AND ".$tablename.".status=1")->result();
										}
									}
									else {
										$newdata = $this->db->query("SELECT ".$tabfield." AS columdata FROM ".$tablename."".$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1".$cond."")->result();
									}
								}
								foreach($newdata as $key) {
									if($tabfield == 'companylogo') {
										$imgname = $key->columdata;
										if(filter_var($imgname, FILTER_VALIDATE_URL)) {
											$resultset[] = '<img src="'.$imgname.'" />';
										} else {
											if( file_exists($imgname) ) {
												$resultset[] = '<img src="'.$imgname.'" style="width:11em; height:3em" />';
											} else {
												$resultset[] = " ";
											}
										}
									}
									else {
										$currencyinfo = $this->currencyinformation($parenttable,$id,$moduleid);
										if($formatortype=="CW") { //convert to word
											if(count($currencyinfo)>0) {
												$resultset[] = $this->convert->numtowordconvert($key->columdata,$currencyinfo['name'],$currencyinfo['subcode']);
											} else {
												$resultset[] = $this->convert->numtowordconvert($key->columdata);
											}
										} else if($formatortype=="CF") { //currenct format
											if(count($currencyinfo)>0) {
												$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
											} else {
												$resultset[] = $this->convert->formatcurrency($key->columdata);
											}
										} else if($formatortype=="CS") { //currency with symbol
											if(count($currencyinfo)>0) {
												$amt = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
												$resultset[] = $currencyinfo['symbol'].' '.$amt;
											} else {
												$resultset[] = $currencyinfo['symbol'].' '.$this->convert->formatcurrency($key->columdata);
											}
										} else {
											if(is_numeric($key->columdata)) {
												if( $uitypeid == '4' || $uitypeid == '5' || $uitypeid == '7' ) {
													if(count($currencyinfo)>0) {
														$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
													} else {
														$resultset[] = $this->convert->formatcurrency($key->columdata);
													}
												} else {
													$resultset[] = $key->columdata;
												}
											} else {
												if($editorfname=='editorfilename') {
													if( file_exists($key->columdata) ) {
														$datas = $this->geteditorfilecontent($key->columdata);
														$datas = preg_replace('~a10s~','&nbsp;', $datas);
														$datas = preg_replace('~<br>~','<br />', $datas);
														$resultset[] = $datas;
													} else {
														$resultset[] = '';
													}
												} else {
													$resultset[] = $key->columdata;
												}
											}
										}
									}
								}
							}
						}
					}
				} 
				else {
					//attribute value fetch
					$fieldnamechk = explode('-',$tabfield);
					$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
					$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
					if(count($fieldinfo)>0) {
						$uitypeid = $fieldinfo['uitype'];
						//fetch parent table join id
						$relpartable = $fieldinfo['partab'];
						$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
						$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
						$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
						//join relation table and main table
						$joinq="";
						if($relpartable!=$tablename) {
							$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id';
						}
						//main table join
						if($mainjointable==$parenttable) {
							$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id';
						} else {
							$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id';
							$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$mainjointable.'.'.$parenttable.'id';
						}
						//fetch attribute name and values
						$i=0;
						$datasets = array();
						$attrsetids = $this->db->query(" SELECT ".$tablename.".".$tablename."id as Id,".$tablename.".attributesetid as attrsetid,".$tablename.".attributes as attributeid,".$tablename.".attributevalues as attributevalueid FROM ".$tablename.$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$mainjointable.".status=1 AND ".$tablename.".status=1",false)->result();
						foreach($attrsetids as $attrset) {
							$datasets[$i] = array('id'=>$attrset->Id,'attrsetid'=>$attrset->attrsetid,'attrnames'=>$attrset->attributeid,'attrvals'=>$attrset->attributevalueid);
							$i++;
						}
						$resultset = array();
						foreach($datasets as $set) {
							$valid = explode('_',$set['attrvals']);
							$nameid = explode('_',$set['attrnames']);
							$n=1;
							$vi = 0;
							$attrdatasets = array();
							for($h=0;$h<count($valid);$h++) {
								$attrval = '';
								$attrname = '';
								if($valid[$h]!='') {
									//attribute value fetch
									$attrvaldata = $this->db->query(' SELECT attributevalueid,attributevaluename FROM attributevalue WHERE attributevalueid='.$valid[$h].' AND status=1',false)->result();
									foreach($attrvaldata as $valsets) {
										$attrval = $valsets->attributevaluename;
									}
									//attribute name fetch
									$attrnamedata = $this->db->query(' SELECT attributeid,attributename FROM attribute WHERE attributeid='.$nameid[$h].' AND status=1',false)->result();
									foreach($attrnamedata as $namesets) {
										$attrname = $namesets->attributename;
									}
								}
								$attrset = "";
								if( $attrval!='' ) {
									$attrset = $n.'.'.$attrname.': '.$attrval;
									++$n;
								}
								$attrdatasets[$vi] = $attrset;
								$vi++;
							}
							$resultset[] = implode('<br />',$attrdatasets);
						}
					}
				}
			}
			else if($table=='GEN') {
				$tablename = trim($reltabname[1]);
				$tabfield = $newval[1];
				//field fetch parent table fetch
				$fieldnamechk = explode('-',$tabfield);
				$fldname = ( (count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
				$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
				$uitypeid = ( (count($fieldinfo) > 0)? $fieldinfo['uitype'] : 1);
				if($fldname != 'attributesetid') {
					//get user company & branch id
					$tabinfo = array('company','companycf','branch','branchcf');
					if( in_array($tablename,$tabinfo) ) {
						if($tablename == 'company' || $tablename == 'companycf') {
							$compquery = $this->db->query('select company.companyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND company.status=1')->result();
							foreach($compquery as $key) {
								$id = $key->companyid;
							}
						} else if($tablename == 'branch' || $tablename == 'branchcf') {
							$brquery = $this->db->query('select branch.branchid from branch join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND branch.status=1')->result();
							foreach($brquery as $key) {
								$id = $key->branchid;
							}
						} else { //employee
							$id = $this->Basefunctions->userid;
						}
					} else { //employee
						$id = $this->Basefunctions->userid;
					}
					//check its parent child field concept
					if(in_array($fldname,$elname)) {
						$key = array_search($fldname,$elname);
						$pid = $id;
						if($key != NULL) {
							//parent id get
							$result = $this->db->select("$tname[$key].$elname[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$id)->where($tname[$key].'.status',1)->get();
							if($result->num_rows() > 0) {
								foreach($result->result()as $row) {
									$pid=$row->$elname[$key];
								}
								if($pid!=0) {
									//parent name
									$result = $this->db->select("$tname[$key].$fname[$key],$tname[$key].$fnameid[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$pid)->where($tname[$key].'.status',1)->get();
									if($result->num_rows() > 0) {
										foreach($result->result()as $row) {
											$resultset[]=$row->$fname[$key];
										}
									}
								}
							}
						}
					}
					else {
						//field fetch parent table fetch
						$fieldnamechk = explode('-',$tabfield);
						$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
						$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
						$moduleid = $fieldinfo['modid'];
						if(count($fieldinfo)>0) {
							$uitypeid = $fieldinfo['uitype'];
							//fetch parent table join id
							$relpartable = $fieldinfo['partab'];
							$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
							$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $relpartable );
							$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
							//fetch original field of picklist data in view creation 
							$viewfieldinfo = $this->viewfieldsinformation($tablename,$tabfield,$fieldinfo['modid']);
							$tabfieldname = ( (count($viewfieldinfo)>0)? $viewfieldinfo['joinfieldname'] : $tabfield );
							$jointabfieldid = ( (count($viewfieldinfo)>0)? $viewfieldinfo['condfiledname'] : $tabfield );
							//join relation table and main table
							$joinq="";
							if($relpartable!=$tablename) {
								$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id';
							}
							//if its picklist data
							$newfield = substr( $tabfieldname,( strlen($tabfieldname)-2 ));
							$tabname = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
							$tbachk = $this->tableexitcheck($database,$tabname);
							if( $newfield == "id" && $tbachk != 0 ) {
								$newtable = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
								$newjfield = $newtable."name";
								$whtable = (($tablename=="company" || $tablename=="branch")? $tablename : $relpartable);
								$newjdata = $this->db->query("SELECT ".$newtable.".".$newtable."id AS columdataid,".$newtable.".".$newjfield." AS columdata FROM ".$newtable." LEFT OUTER JOIN ".$tablename." ON ".$tablename.".".$tabfieldname." = ".$newtable.".".$tabfieldname."".$joinq." WHERE ".$tablename.".".$whtable."id = ".$id." AND ".$newtable.".status=1 AND ".$tablename.".status=1",false);
								if($newjdata->num_rows() >0) {
									foreach($newjdata->result() as $jkey) {
										$resultset[] = $jkey->columdata;
									}
								} else {
									$resultset[] = " ";
								}
							}
							else {
								//main table data sets information fetching
								$fldcountchk = explode('-',$tabfield);
								$tablcolumncheck = $this->tablefieldnamecheck($tablename,$relpartable."id");
								if( ( $relpartable == $tablename && (count($fldcountchk)) < 2 ) || ( $tablcolumncheck == 'false' && (count($fldcountchk)) < 2 ) ) {
									$newdata = $this->db->select("".$tabfield." AS columdata")->from($tablename)->where($tablename.".".$tablename."id",$id)->where($tablename.".status",1)->get()->result();
								}
								else {
									$cond = "";
									$addfield = explode('-',$tabfield);
									if( (count($addfield)) >= 2 ) {
										$datafiledname=$addfield[1];
										if(in_array('PR',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=2';
										} else if(in_array('SE',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=3'; 
										} else if(in_array('BI',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=4';
										} else if(in_array('SH',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=5';
										} else if(in_array('CW',$addfield)) {
											$formatortype = "CW";
										} else if(in_array('CF',$addfield)) {
											$formatortype = "CF";
										} else if(in_array('CS',$addfield)) {
											$formatortype = "CS";
										}
										if($tablename!=$relpartable) {
											$tablcolumncheck = $this->tablefieldnamecheck($tablename,"addresstypeid");
											$cond = ( ($tablcolumncheck == 'true')? $cond : ' ');
											$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename." JOIN ".$relpartable." ON ".$relpartable.".".$relpartable."id = ".$tablename.".".$relpartable."id WHERE ".$relpartable.".".$relpartable."id = ".$id." AND ".$tablename.".status=1 AND ".$relpartable.".status=1".$cond."")->result();
										} else {
											$newdata = $this->db->select(''.$datafiledname.' AS columdata',false)->from($tablename)->where($tablename.'id',$id)->where($tablename.'.status',1)->get()->result();
										}
									}
									else {
										$newdata = $this->db->query("SELECT ".$tabfield." AS columdata FROM ".$tablename." JOIN ".$relpartable." ON ".$relpartable.".".$relpartable."id = ".$tablename.".".$relpartable."id WHERE ".$relpartable.".".$relpartable."id = ".$id." AND ".$tablename.".status=1 AND ".$relpartable.".status=1")->result();
									}
								}
								foreach($newdata as $key) {
									if($tabfield == 'companylogo') {
										$imgname = $key->columdata;
										if(filter_var($imgname,FILTER_VALIDATE_URL)) {
											$resultset[] = '<img src="'.$imgname.'" />';
										} else {
											if( file_exists($imgname) ) {
												$resultset[] = '<img src="'.$imgname.'" style="width:11em; height:3em" />';
											} else {
												$resultset[] = " ";
											}
										}
									}
									else {
										if($formatortype=="CW") { //convert to word
											$currencyinfo = $this->currencyinformation($tablename,$id,$moduleid);
											if(count($currencyinfo)>0) {
												$resultset[] = $this->convert->numtowordconvert($key->columdata,$currencyinfo['name'],$currencyinfo['subcode']);
											} else {
												$resultset[] = $this->convert->numtowordconvert($key->columdata);
											}
										} else if($formatortype=="CF") { //currenct format
											$currencyinfo = $this->currencyinformation($tablename,$id,$moduleid);
											if(count($currencyinfo)>0) {
												$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
											} else {
												$resultset[] = $this->convert->formatcurrency($key->columdata);
											}
										} else if($formatortype=="CS") { //currency with symbol
											$currencyinfo = $this->currencyinformation($tablename,$id,$moduleid);
											if(count($currencyinfo)>0) {
												$amt = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
												$resultset[] = $currencyinfo['symbol'].' '.$amt;
											} else {
												$resultset[] = $currencyinfo['symbol'].' '.$this->convert->formatcurrency($key->columdata);
											}
										} else {
											$currencyinfo = $this->currencyinformation($tablename,$id,$moduleid);
											if(is_numeric($key->columdata)) {
												if($uitypeid == '4' || $uitypeid == '5' || $uitypeid == '7') {
													if(count($currencyinfo)>0) {
														$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
													} else {
														$resultset[] = $this->convert->formatcurrency($key->columdata);
													}
												} else {
													$resultset[] = $key->columdata;
												}
											} else {
												if($editorfname=='editorfilename') {
													if( file_exists($key->columdata) ) {
														$datas = $this->geteditorfilecontent($key->columdata);
														$datas = preg_replace('~a10s~','&nbsp;', $datas);
														$datas = preg_replace('~<br>~','<br />', $datas);
														$resultset[] = $datas;
													} else {
														$resultset[] = '';
													}
												} else {
													$resultset[] = $key->columdata;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		//}
		return $resultset;
	}
	//table name check
	public function tableexitcheck($database,$newtable) {
		$counttab=0;
		$tabexits = $this->db->query("SELECT COUNT(*) AS tabcount FROM information_schema.tables WHERE table_schema = '".$database."'AND table_name = '".$newtable."'");
		foreach($tabexits->result() as $tkey) { 
			$counttab = $tkey->tabcount;
		};
		return $counttab;
	}
	//fetch parent table information
	public function fetchfieldparenttable($tablname,$fieldname) {
		$pardata = array();
		$datas = $this->db->query('SELECT modulefieldid,uitypeid,parenttable,moduletabid  FROM modulefield WHERE columnname LIKE "%'.$fieldname.'%" AND tablename LIKE "%'.$tablname.'%" AND status IN (1,10)',false);
		foreach($datas->result() as $data) {
			$pardata = array('uitype'=>$data->uitypeid,'partab'=>$data->parenttable,'modid'=>$data->moduletabid);
		}
		return $pardata;
	}
	//fetch related module information
	public function fetchrelatedmodulelist($moduleid) {
		$relmodids = array();
		$relmoduleids = $this->db->select('modulerelationid,relatedmoduleid')->from('modulerelation')->where('moduleid',$moduleid)->get();
		foreach($relmoduleids->result() as $reldata) {
			$relmodids[] = $reldata->relatedmoduleid;
		}
		return $relmodids;
	}
	//fetch main table join information
	public function fetchjoincolmodelname($relpartable,$moduleid) {
		$joindata = array();
		$datas = $this->db->query('SELECT modulerelationid,parentfieldname,parenttable FROM modulerelation WHERE moduleid = '.$moduleid.' AND relatedtable LIKE "%'.$relpartable.'%" AND status=1',false);
		foreach($datas->result() as $data) {
			$joindata = array('relid'=>$data->modulerelationid,'joinfieldname'=>$data->parentfieldname,'jointabl'=>$data->parenttable);
		}
		return $joindata;
	}
	//fetch relation and view type 3 field information
	public function viewfieldsinformation($tablename,$tabfield,$modid) {
		$datasets = array();
		$viewfieldinfo = $this->db->query('SELECT viewcreationcolumnid,viewcreationparenttable,viewcreationjoincolmodelname,viewcreationcolmodelcondname FROM viewcreationcolumns WHERE viewcreationparenttable LIKE "%'.$tablename.'%" AND viewcreationcolmodelcondname LIKE "%'.$tabfield.'%" AND viewcreationtype=3 AND viewcreationmoduleid='.$modid.' AND status=1');
		foreach($viewfieldinfo->result() as $viewdata) {
			$datasets = array('viewcolid'=>$viewdata->viewcreationcolumnid,'joinfieldname'=>$viewdata->viewcreationjoincolmodelname,'condfiledname'=>$viewdata->viewcreationcolmodelcondname);
		}
		return $datasets;
	}
	//fetch currency information
	public function currencyinformation($parenttable,$id,$moduleid) {
		$datasets = array();
		$colmodeltable = "";
		$cid = 1;
		//modulerelation
		$datasets = $this->db->select('viewcreationcolumnid,viewcreationcolmodeltable,viewcreationjoincolmodelname,viewcreationparenttable,viewcreationtype,viewcreationcolmodelcondname',false)->from('viewcreationcolumns')->where('viewcreationmoduleid',$moduleid)->where('viewcreationjoincolmodelname','currencyid')->where('status',1)->get();
		if($datasets->num_rows()>0) {
			foreach($datasets->result() as $data) {
				$colname =  $data->viewcreationcolmodeltable;
				$colmodeltable = $data->viewcreationcolmodeltable;
				$joincolmodelname= $data->viewcreationjoincolmodelname;
				$colmodelpartable = $data->viewcreationparenttable;
				$viewtype = $data->viewcreationtype;
				$viewcolcondname = $data->viewcreationcolmodelcondname;
			}
		}
		if($colmodeltable!= "") {
			$joinq="";
			if($viewtype==3) {
				if($colmodelpartable!="currency") {
					if($colmodelpartable == $parenttable) {
						$joinq = 'LEFT OUTER JOIN '.$colmodelpartable.' ON '.$colmodelpartable.'.'.$viewcolcondname.'='.$colmodeltable.'.'.$joincolmodelname;
					} else {
						$joinq = 'LEFT OUTER JOIN '.$colmodelpartable.' ON '.$colmodelpartable.'.'.$viewcolcondname.'='.$colmodeltable.'.'.$joincolmodelname;
						$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$colmodelpartable.'.'.$parenttable.'id';
					}
				}
			} else {
				if($colmodelpartable!="currency") {
					if($colmodelpartable == $parenttable) {
						$joinq = 'LEFT OUTER JOIN '.$colmodelpartable.' ON '.$colmodelpartable.'.'.$joincolmodelname.'='.$colmodeltable.'.'.$joincolmodelname;
					} else {
						$joinq = 'LEFT OUTER JOIN '.$colmodelpartable.' ON '.$colmodelpartable.'.'.$joincolmodelname.'='.$colmodeltable.'.'.$joincolmodelname;
						$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$colmodelpartable.'.'.$parenttable.'id';
					}
				}
			}
			$curdatasets = $this->db->query('select currency.currencyid from currency '.$joinq.' where '.$parenttable.'id='.$id.'')->result();
			foreach($curdatasets as $data) {
				$cid = $data->currencyid;
			}
		} else {
			$empid = $this->Basefunctions->userid;
			$compquery = $this->db->query('select company.companyid,company.currencyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.'')->result();
			foreach($compquery as $key) {
				$cid = $key->currencyid;
			}
		}
		$curdatasets = $this->db->select('currencyid,currencyname,currencycode,symbol,currencycountry,subunit',false)->from('currency')->where('currencyid',$cid)->get();
		foreach($curdatasets->result() as $datas) {
			$datasets = array('id'=>$datas->currencyid,'name'=>$datas->currencyname,'code'=>$datas->currencycode,'symbol'=>$datas->symbol,'country'=>$datas->currencycountry,'subcode'=>$datas->subunit);
		}
		return $datasets;
	}
	//table field name check
	public function tablefieldnamecheck($tblname,$fieldname) {
		$result = 'false';
		$fields = $this->db->field_data($tblname);
		foreach ($fields as $field) {
			$name =  $field->name;
			if($fieldname == $name) {
				$result = 'true';
			}
		}
		return $result;
	}
	//campaign mail sent
	public function testmailcampaignsentmodel() {
		$subaccountcreditis = $this->subaccountcreditscheck();
		if($subaccountcreditis != 0) {
			$subaccountid = $this->subaccountdetailsfetchmodel();
			$campfname = $_POST['fromname'];
			$campfmailid = $_POST['frommailid'];
			if(isset($_POST['mailtrackingid'])){
				$camptracking = $_POST['mailtrackingid'];
				$mailtracking = implode(',',$camptracking);
				if($mailtracking == '') {
					$mailtracking = '1';
				}
			} else {
				$mailtracking = '1';
			}
			if(isset($_POST['emailtemplatesid'])){
				$templateid = $_POST['emailtemplatesid'];
				if($templateid == '') {
					$templateid = '1';
				}
			} else {
				$templateid = '1';
			}
			//template based attachment get
			$date = date($this->Basefunctions->datef);
			$currentuserid = $this->Basefunctions->userid;
			$attachment = $this->temapltebasedattachmentget($templateid);
			$templatecontent = $_POST['defaultsubseditorval'];
			$campfname = $_POST['fromname'];
			$campfmailid = $_POST['frommailid'];
			$subject = $_POST['subject'];
			$scheduledate = $_POST['communicationdate'];
			$scheduletime = $_POST['communicationtime'];
			$tomailid = $_POST['testmailid'];
			$char_set = 'UTF-8';
			$SesClient = new SesClient([
				'version'     => 'latest',
				'region'      => 'us-west-2',
				'credentials' => [
					'key'    => 'AKIAJBBGVK7CJSF34IZA',
					'secret' => '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',
				],
			]);
			$mailid = array(array( 'email' => $tomailid ));
			$recipient_emails = [$tomailid];
			if(!empty($attachment)){
			try {
					$result = $SesClient->sendEmail([
						'Destination' => ['ToAddresses' => $recipient_emails,],
						'ReplyToAddresses' => [$campfmailid],
						'from_name' => [$campfname],
						'Source' => $campfmailid,
						'Message' => [
							'Body' => ['Html' => ['Charset' => $char_set,'Data' => $templatecontent,],'Text' => ['Charset' => $char_set,'Data' =>  '',],],
							'Subject' => ['Charset' => $char_set,'Data' => $subject,],
							],
						'track_opens' => true,
						'track_clicks' => true,
						'important' => true,
						'url_strip_qs' => true,
						'inline_css' => true,
						'auto_text' => true,
						'merge' => true,
						'auto_html' => true,
						'attachments' => $attachment,
					]);
				} catch (AwsException $e) {
					$e->getMessage();
					$result['MessageId'] = $e->getAwsErrorMessage();
				}
			} else {
				try {
					$result = $SesClient->sendEmail([
						'Destination' => ['ToAddresses' => $recipient_emails,],
						'ReplyToAddresses' => [$campfmailid],
						'from_name' => [$campfname],
						'Source' => $campfmailid,
						'Message' => [
							'Body' => ['Html' => ['Charset' => $char_set,'Data' => $templatecontent,],'Text' => ['Charset' => $char_set,'Data' =>  '',],],
							'Subject' => ['Charset' => $char_set,'Data' => $subject,],
							],
						'track_opens' => true,
						'track_clicks' => true,
						'important' => true,
						'url_strip_qs' => true,
						'inline_css' => true,
						'auto_text' => true,
						'merge' => true,
						'auto_html' => true,
					]);
				} catch (AwsException $e) {
					$e->getMessage();
					$result['MessageId'] = $e->getAwsErrorMessage();
				}
			}
			$sql = $this->db->query("INSERT INTO crmmaillog (communicationfrom,communicationto,subject,employeeid,moduleid,commonid,emailtemplatesid,signatureid,communicationdate,mailstatus,opencount,clickcount,messageuniqueid,createdate,lastupdatedate,createuserid,lastupdateuserid,status) VALUES ('".$campfmailid."','".$tomailid."','".$subject."','".$currentuserid."','29','1','".$templateid."','".$templateid."','".$date."','".$result['@metadata']['statusCode']."','0','0','".$result['MessageId']."','".$date."','".$date."','".$currentuserid."','".$currentuserid."','1')");
			//notification log entry
			$primaryid = $this->Basefunctions->maximumid('crmmaillog','crmmaillogid');
			$notimsg = $campfname." "."Sent a Test Mail to"." - ".$tomailid." Subject"." - ".$subject;
			$this->Basefunctions->notificationcontentadd($primaryid,'Mail',$notimsg,1,242);
			$credit = $this->db->query("UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = `addonavailablecredit` -1 WHERE `addonstypeid` = 3");
			//master company id get - master db update
			$mastercompanyid = $this->Basefunctions->generalinformaion('company','mastercompanyid','companyid',2);
			//Master DB update
			$CI =& get_instance();
			$CI->load->database();
			$hostname = $CI->db->hostname; 
			$username = $CI->db->username; 
			$password = $CI->db->password; 
			$masterdb = $CI->db->masterdb; 
			$con = mysqli_connect($hostname,$username,$password,$masterdb);
			// Check connection
			if (mysqli_connect_errno())	{
				echo "Failed to connect to MySQL: " . mysqli_connect_error();
			}
			mysqli_query($con,"UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = `addonavailablecredit` - 1 WHERE `addonstypeid` = 3 and mastercompanyid ='".$mastercompanyid."'");
			mysqli_close($con);
			echo 'TRUE'; 
		} else {
			echo 'COUNT';
		}
	}
	//get editor file content
	public function geteditorfilecontent($fname) {
		$tccontent="";
		$newfilename = explode('/',$fname);
		if(read_file($newfilename[0].DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = read_file($newfilename[0].DIRECTORY_SEPARATOR.$newfilename[1]);
		}
		return $tccontent;
	}
	//mail notification sent
	public function testmailsentfunction($message,$send_at) {
		try {
			$mandrill = new Mandrill('X9WnMXpxhC8YEOUPuQ3oZw');//
			$async = true; //for bulk message
			$ip_pool = '';
			$result = $mandrill->messages->send($message, $async, $ip_pool, $send_at);
			return $result;
		} catch(Mandrill_Error $e) {
			// Mandrill errors are thrown as exceptions
			echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			throw $e;
		}
	}
	//template based attachment get
	public function temapltebasedattachmentget($tempid) {
		$i=0;
		$data = array();
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('filename,filename_path,filename_type');
		$this->db->from('crmfileinfo');
		$this->db->where('crmfileinfo.moduleid',209);
		$this->db->where('crmfileinfo.commonid',$tempid);
		$this->db->where("FIND_IN_SET('$industryid',crmfileinfo.industryid) >", 0);		
		$this->db->where('crmfileinfo.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$filecontent = $this->newfilecontentfetch($row->filename_path);
				$encodefile = base64_encode($filecontent);
				$data[$i] = array('type'=>$row->filename_type,'name'=>$row->filename,'content'=>$encodefile);
				$i++;
			}
		}
		return $data;
	}
	//read file
	public function newfilecontentfetch($filename) {
		$filecontent = "";
		if( file_exists($filename) && is_readable($filename) ) {
			$newfilename = explode('/',$filename);
			$myfile = @fopen('uploads'.DIRECTORY_SEPARATOR.$newfilename[1], "r");
			$filecontent = fread($myfile,filesize('uploads'.DIRECTORY_SEPARATOR.$newfilename[1]));
			fclose($myfile);
		}
		return $filecontent;
	}
	//sub account credits check
	public function subaccountcreditscheck() {
		$this->db->select('addonavailablecredit');
		$this->db->from('salesneuronaddonscredit');
		$this->db->where('addonstypeid',3);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = $row->addonavailablecredit;
			}
		} else {
			$data = '0';
		}
		return $data;
	}
	//check the mail add ons added or not 
	public function subaccountdetailsfetchmodel() {
		$data = '';
		$this->db->select('generalsettingid,mandrillaccountnumber');
		$this->db->from('generalsetting');
		$this->db->where('generalsetting.companyid',2);
		$this->db->where('generalsetting.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = $row->mandrillaccountnumber;
			}
		} else {
			$data = '';
		}
		return $data;
	}
	//segment criteria fetch
	public function segmentcreteriadatafetch($segmentid) {
		$i = 0;
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('viewcreationcolumns.viewcreationcolmodelindexname,viewcreationcolumns.viewcreationcolmodeljointable,segmentscriteria.viewcreationconditionname,segmentscriteria.viewcreationconditionvalue,segmentscriteria.viewcreationandorvalue');
		$this->db->from('segmentscriteria');
		$this->db->join('viewcreationcolumns','viewcreationcolumns.viewcreationcolumnid=segmentscriteria.viewcreationcolumnid');
		$this->db->where('segmentscriteria.segmentsid',$segmentid);
		$this->db->where("FIND_IN_SET('$industryid',segmentscriteria.industryid) >", 0);
		$this->db->where('segmentscriteria.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data[$i] = array('indexname'=>$row->viewcreationcolmodelindexname,'jointable'=>$row->viewcreationcolmodeljointable,'critreiavalue'=>$row->viewcreationconditionvalue,'conditionidname'=>$row->viewcreationconditionname,'massandorcondidname'=>$row->viewcreationandorvalue);
				$i++;
			}
		} else {
			$data = '';
		}
		return $data;
	}
	//work flow condition header information fetch
	public function maillogdatafetchmodel($moduleid) {
		$fnames = array('modulename','emailcampaignname','communicationfrom','communicationto','subject','mailstatus','opencount','clickcount','communicationdate','crmmaillogid');
		$flabels = array('Module','Campaign Name','From','To','Subject','Mail Status','Open Count','Click Count','Date','Mail LogId');
		$colmodnames = array('modulename','emailcampaignname','communicationfrom','communicationto','subject','mailstatus','opencount','clickcount','communicationdate','crmmaillogid');
		$colindexnames = array('module','emailcampaign','crmmaillog','crmmaillog','crmmaillog','crmmaillog','crmmaillog','crmmaillog','crmmaillog','crmmaillog');
		$uitypes = array('17','2','2','2','2','2','2','2','2','2');
		$viewtypes = array('0','1','1','1','1','1','1','1','1','0');
		$dduitypeids = array('17','18','19','20','23','25','26','27','28','29');
		$data = array();
		$i=0;
		$j=0;
		foreach($fnames as $fname) {
			if(in_array($uitypes[$j],$dduitypeids)) {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j].'name';
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]='1';
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]='2';
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			} else {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			}
			$j++;
		}
		return $data;
	}
	//mail log overlay
	public function getmaillogdatamodel()	{
		$productdetail = '';
		$recordid = $_GET['recordid'];
		$this->db->select('SQL_CALC_FOUND_ROWS module.modulename,module.moduleid,emailcampaign.emailcampaignname,communicationfrom,communicationto,subject,mailstatus,opencount,clickcount,communicationdate,crmmaillogid',false);
		$this->db->from('crmmaillog');
		$this->db->join('module','module.moduleid=crmmaillog.moduleid');			
		$this->db->join('emailcampaign','emailcampaign.emailcampaignid=crmmaillog.commonid');			
		$this->db->where('crmmaillog.moduleid',29);
		$this->db->where('crmmaillog.commonid',$recordid);
		$this->db->where('crmmaillog.status',$this->Basefunctions->activestatus);
		$data=$this->db->get()->result();
		$j=0;
		foreach($data as $value) {
			$productdetail->rows[$j]['id']=$value->crmmaillogid;
			$productdetail->rows[$j]['cell']=array(
					$value->moduleid,
					$value->modulename,
					$value->emailcampaignname,
					$value->communicationfrom,
					$value->communicationto,
					$value->subject,
					$value->mailstatus,
					$value->opencount,
					$value->clickcount,
					$value->communicationdate,
					$value->crmmaillogid
			);
			$j++;
		}
		if($productdetail == '') {
			$productdetail = array('fail'=>'FAILED');
		}
		echo  json_encode($productdetail);
	}
	//audit log excel data export
	public function auditlogexportdata($primaryid) {
		$format = $this->Basefunctions->phpmysqlappdateformatfetch();
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('SQL_CALC_FOUND_ROWS module.modulename,emailcampaign.emailcampaignname,campaigngroups.campaigngroupsname,emailcampaign.fromname,emailcampaign.frommailid,emailtemplates.emailtemplatesname,signature.signaturename,crmmaillog.communicationfrom,crmmaillog.communicationto,crmmaillog.subject,crmmaillog.mailstatus,crmmaillog.opencount,crmmaillog.clickcount',false);
		$this->db->from('emailcampaign');
		$this->db->join('campaigngroups','campaigngroups.campaigngroupsid = emailcampaign.campaigngroupsid','left outer');
		$this->db->join('crmmaillog','crmmaillog.commonid = emailcampaign.emailcampaignid','left outer');#from employee
		$this->db->join('emailtemplates','emailtemplates.emailtemplatesid = crmmaillog.signatureid','left outer');
		$this->db->join('signature','signature.signatureid = crmmaillog.emailtemplatesid','left outer');
		$this->db->join('module','module.moduleid = crmmaillog.moduleid','left outer');
		$this->db->where_in('crmmaillog.moduleid',array('29'));
		$this->db->where_in('crmmaillog.commonid',array($primaryid));
		$this->db->where("FIND_IN_SET('$industryid',emailcampaign.industryid) >", 0);
		$this->db->where_in('emailcampaign.status',array(1));
		$result=$this->db->get();
		return $result;
	}
	//Export data here
	public function exceldataexportmodel() {
		$this->load->library('excel');
		$coldata['colname']=array('Module Name','Email Campaign','Email List','From Name','From MailId','Email template','signatureid','Communication From','Communication To','Subject','Status','Open Count','Click Count');
		$coldata['columndbname']=array('modulename','emailcampaignname','campaigngroupsname','fromname','frommailid','emailtemplatesname','signaturename','communicationfrom','communicationto','subject','mailstatus','opencount','clickcount');
		$title = 'AuditLog';
		$name = preg_replace('/[^A-Za-z0-9]/', '', $title);
		$fname = strtolower($name);
		$primaryid = $_POST['primaryid'];
		$result=$this->auditlogexportdata($primaryid);//main part
		$this->excel->setActiveSheetIndex(0);
		$sheet = $this->excel->getActiveSheet();
		$sheet->setTitle($title);
		$count = 0;
		$celtitle = $coldata['colname'];
		$titlecount = count($celtitle);
		$dataname = $coldata['columndbname'];
		$baseurl=$this->config->item('base_url');
		//header
		//sl.NO
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(13);
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->setCellValue('A1','Sl.No');
		$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(50);
		$x=1;
		$h='B';
		for($i=0;$i<$titlecount;$i++) {
			$this->excel->getActiveSheet()->getStyle($h.$x)->getFont()->setSize(13);
			$this->excel->getActiveSheet()->getStyle($h.$x)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue($h.$x,$celtitle[$i]);
			$this->excel->getActiveSheet()->getColumnDimension($h)->setAutoSize(50);
			$h++;
		}
		$m=1;
		$tot_rows = 1;
		$rows=2;
		foreach($result->result() as $row) {
			$c='B';
			$this->excel->getActiveSheet()->getStyle('A'.$rows)->getFont()->setBold(false);
			$this->excel->getActiveSheet()->setCellValue('A'.$rows,$m);
			$this->excel->getActiveSheet()->getStyle('A'.$rows)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			if ($rows % 2 == 0) {
				$this->excel->getActiveSheet()->getStyle('A'.$rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$this->excel->getActiveSheet()->getStyle('A'.$rows)->getFill()->getStartColor()->setARGB('e4f3fd');
			}
			$m++;
			for($i=0;$i<$titlecount;$i++) {
				if($dataname[$i] == 'createdate') {
					$date=explode(' ',$row->$dataname[$i]);
					$datas = $this->Emailcampaignmodel->customerdateset($date[0]);
				} else if($dataname[$i] == 'notificationmessage') {
					$message = $row->$dataname[$i];
					$datas = $this->Auditlogmodel->excelmergecontentfetch($message,$baseurl,$row->modulelink,$row->modulemastertable,$row->modulename,$row->moduleid,$row->commonid);
				} else {
					$datas = $row->$dataname[$i];
				}
				$this->excel->getActiveSheet()->getStyle($c.$rows)->getFont()->setBold(false);
				$this->excel->getActiveSheet()->getStyle($c.$rows)->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->setCellValue($c.$rows,trim($datas));
				$this->excel->getActiveSheet()->getStyle($c.$rows)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				if( is_numeric( trim( $datas ) ) ) {
					$this->excel->getActiveSheet()->getStyle($c.$rows)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_GENERAL);
					$this->excel->getActiveSheet()->getStyle($c.$rows)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				}
				if ($rows % 2 == 0) {
					$this->excel->getActiveSheet()->getStyle($c.$rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
					$this->excel->getActiveSheet()->getStyle($c.$rows)->getFill()->getStartColor()->setARGB('e4f3fd');
				}
				$c++;
			}
			$rows++;
			$tot_rows++;
		}
		#
		ob_clean();
		$time = date("d-m-Y-g-i-s-a");
		$today = date("d-m-Y");
		$today_time=date('g:i:s a');
		//xlsx format
		$filename=$fname.'_'.$time.'.xlsx';
		header('Content-Type: application/vnd.ms-excel');
		header("Content-type:application/octetstream");
		header("Content-Disposition:attachment;filename=".$filename."");
		header("Content-Transfer-Encoding:binary");
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
		$objWriter->save('php://output');
	}
	//customer based date set
	public function customerdateset($date) {
		$dateformat = $this->Basefunctions->phpmysqlappdateformatfetch();
		$convertdate = date($dateformat['phpformat'], strtotime($date));
		return $convertdate;
	}
	//merge content for excel report
	public function excelmergecontentfetch($message,$baseurl,$modulelink,$parenttable,$modulename,$moduleid,$primaryid) {
		$this->load->model('Printtemplates/Printtemplatesmodel');
		$finalcont = $message;
		preg_match_all ("/{.*}/U", $message, $contmatch);
		foreach($contmatch as $bocontent) {
			foreach($bocontent as $bomatch) {
				if($bomatch != "{S.Number}") {
					$htmldata = $this->Printtemplatesmodel->snmergcontinformationfetchmodel($bomatch,$parenttable,$primaryid,$moduleid);
					$exp = explode(':',$bomatch);
					if($exp[0] == '{REL') {
						$explode = explode('.',$exp[1]);
						$reltabname = $explode[0];
						$relfieldname = explode('}',$explode[1]);
						$moddatafetch = $this->moduledatafetch($reltabname);
						$relrecordid = $this->Basefunctions->generalinformaion($parenttable,$reltabname.'id',$parenttable.'id',$primaryid);
						$field = $this->Basefunctions->generalinformaion($reltabname,$relfieldname[0],$reltabname.'id',$relrecordid);
						$data = $field;
					} else if($exp[0] == '{GEN') {
						$explode = explode('.',$exp[1]);
						$gentabname = $explode[0];
						$genfieldname = explode('}',$explode[1]);
						$moddatafetch = $this->moduledatafetch($gentabname);
						$genrecordid = $this->Basefunctions->generalinformaion($parenttable,'createuserid',$parenttable.'id',$primaryid);
						$field = $this->Basefunctions->generalinformaion($gentabname,$genfieldname[0],$gentabname.'id',$genrecordid);
						$data = $field;
					} else {
						$data = implode(',',$htmldata);
					}
					$finalcont = preg_replace('~'.$bomatch.'~',$data,$finalcont);
				} else {
					$finalcont = preg_replace('~'.$bomatch.'~','',$finalcont);
				}
			}
		}
		$content = preg_replace('~<p>~','', $finalcont);
		$content = preg_replace('~a10s~',' ', $content);
		$content = preg_replace('~<br>~','', $content);
		$content = preg_replace('~</p>~','', $content);
		return $content;
	}
	//module informationfetch
	public function moduledatafetch($reltabname) {
		$this->db->select("modulelink,moduleid");
		$this->db->from("module");
		$this->db->like("module.modulemastertable",$reltabname);
		$this->db->where("module.status",1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('mid'=>$row->moduleid,'link'=>$row->modulelink);
			}
		} else{
			$data = '';
		}
		return $data;
	}
	//resnt mail data
	public function resentmaildatamodel() {
		$id = $_POST['datarowid'];
		$resentmailid = $_POST['mailid'];
		$this->db->select('commonid,crmmaillog.emailtemplatesid,fromname,signatureid,communicationfrom,communicationto,subject');
		$this->db->from('crmmaillog');
		$this->db->join('emailcampaign','emailcampaign.emailcampaignid=crmmaillog.commonid');
		$this->db->where('crmmaillog.crmmaillogid',$id);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$campaignid = $row->commonid;
				$templateid = $row->emailtemplatesid;
				$signatureid = $row->signatureid;
				$fromname = $row->fromname;
				$communicationfrom = $row->communicationfrom;
				$communicationto = $row->communicationto;
				$subject = $row->subject;
			}
		}
		$tempdata = $this->editorfilenamefetch($templateid);
		$signature = $this->signatureeditervaluefetch($signatureid);
		$content = $tempdata.$signature;
		$subaccountcreditis = $this->subaccountcreditscheck();
		if($subaccountcreditis != 0) {
			$subaccountid = $this->subaccountdetailsfetchmodel();
			//template based attachment get
			$date = date($this->Basefunctions->datef);
			$currentuserid = $this->Basefunctions->userid;
			$attachment = $this->temapltebasedattachmentget($templateid);
			$mailid = array(array( 'email' => $resentmailid ));
			if(!empty($attachment)) {
				$message = array(
						'html' => $content,
						'text' => '',
						'subject' => $subject,
						'from_email' => $communicationfrom,
						'from_name' => $fromname,
						'to' => $mailid,
						'track_opens' => true,
						'track_clicks' => true,
						'inline_css' => true,
						'url_strip_qs' => true,
						'important' => true,
						'auto_text' => true,
						'auto_html' => true,
						'attachments' => $attachment,
				);
				$send_at ='';
				$result =  $this->testmailsentfunction($message,$send_at);
			} else {
				$message = array(
						'html' => $content,
						'text' => '',
						'subject' => $subject,
						'from_email' => $communicationfrom,
						'from_name' => $fromname,
						'to' => $mailid,
						'track_opens' => true,
						'track_clicks' => true,
						'inline_css' => true,
						'url_strip_qs' => true,
						'important' => true,
						'auto_text' => true,
						'auto_html' => true,
				);
				$send_at ='';
				$result = $this->testmailsentfunction($message,$send_at);
			}
			$sql = $this->db->query("update crmmaillog mailstatus=".$result[0]['status'].",opencount=0,clickcount=0,messageuniqueid=".$result[0]['_id'].",communicationto=".$resentmailid." where crmmaillogid=".$id."");
			//notification log entry
			$primaryid = $this->Basefunctions->maximumid('crmmaillog','crmmaillogid');
			$notimsg = $campfname." "."Resent a Mail to"." - ".$resentmailid." Subject"." - ".$subject;
			$this->Basefunctions->notificationcontentadd($primaryid,'Mail',$notimsg,1,242);
			$credit = $this->db->query("UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = `addonavailablecredit` -1 WHERE `addonstypeid` = 3");
			//master company id get - master db update
			$mastercompanyid = $this->Basefunctions->generalinformaion('company','mastercompanyid','companyid',2);
			//Master DB update
			$CI =& get_instance();
			$CI->load->database();
			$hostname = $CI->db->hostname;
			$username = $CI->db->username;
			$password = $CI->db->password;
			$masterdb = $CI->db->masterdb;
			$con = mysqli_connect($hostname,$username,$password,$masterdb);
			// Check connection
			if (mysqli_connect_errno())	{
				echo "Failed to connect to MySQL: " . mysqli_connect_error();
			}
			mysqli_query($con,"UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = `addonavailablecredit` - 1 WHERE `addonstypeid` = 3 and mastercompanyid ='".$mastercompanyid."'");
			mysqli_close($con);
			echo 'TRUE';
		} else {
			echo 'COUNT';
		}
	}
	public function editorfilenamefetch($templateid) {
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('emailtemplatesemailtemplate_editorfilename');
		$this->db->from('emailtemplates');
		$this->db->where('emailtemplates.emailtemplatesid',$templateid);
		$this->db->where("FIND_IN_SET('$industryid',emailtemplates.industryid) >", 0);
		$template = $this->db->get();
		if($template->num_rows() > 0) {
			foreach($template->result() as $row) {
				$filename = $row->emailtemplatesemailtemplate_editorfilename;
			}
		}
		$content = $this->editervaluefetch($filename);
		return $content; 
	}
	//editor value fetch
	public function editervaluefetch($filename) {
		$file = explode('/',$filename);
		if(read_file('templates'.DIRECTORY_SEPARATOR.$file[1])) {
			$tccontent = read_file('templates'.DIRECTORY_SEPARATOR.$file[1]);
			return json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			return json_encode($tccontent);
		}
	}
	//signature content fetch
	public function signatureeditervaluefetch($signatureid){
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('userssignature_editorfilename');
		$this->db->from('signature');
		$this->db->where('signature.signatureid',$signatureid);
		$this->db->where("FIND_IN_SET('$industryid',signature.industryid) >", 0);
		$template = $this->db->get();
		if($template->num_rows() > 0) {
			foreach($template->result() as $row) {
				$filename = $row->userssignature_editorfilename;
			}
		}
		$content = $this->sigeditervaluefetch($filename);
		return $content;
	}
	//editor value fetch
	public function sigeditervaluefetch($filename) {
		$file = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$file[1])) {
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$file[1]);
			return json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			return json_encode($tccontent);
		}
	}
}      