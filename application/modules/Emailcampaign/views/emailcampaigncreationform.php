<style type="text/css">
.innergridpaddingbtm{
	top:20px !important;
}
.innergridpaddingbtm .footer-contentnew {
	display:none;
}
</style>
<div class="large-12 columns paddingzero formheader">
	<?php
		$this->load->view('Base/mainviewaddformheader');
	?>
</div>
<div class="large-12 columns addformunderheader">&nbsp;</div>
<div class="large-12 columns scrollbarclass addformcontainer smsmastertouchcontainer">
	<div class="row mblhidedisplay">&nbsp;</div>
	<form method="POST" name="dataaddform" class="" action ="" id="dataaddform" enctype="multipart/form-data">
		<span id="formaddwizard" class="validationEngineContainer" >
			<span id="formeditwizard" class="validationEngineContainer">
				<?php
					//function call for form fields generation
					$innergridtype = 'form';
					formfieldstemplategenerator($modtabgrp,'',$innergridtype);
				?>
			</span>
		</span>
		<!--hidden values-->
		<?php
			echo hidden('primarydataid','');
			echo hidden('sortorder','');
			echo hidden('sortcolumn','');
			echo hidden('segmentsubscriberid');
			echo hidden('selectedsegmentsubscriberid');
			echo hidden('grouptypeid');
			echo hidden('groupmoduleid');
			echo hidden('defaultsubseditorval');
			echo hidden('subject');
			echo hidden('editordata');
			echo hidden('autoupdate');
			$value = '';
			echo hidden('resctable',$value);
			$modid = $this->Basefunctions->getmoduleid($filedmodids);
			echo hidden('viewfieldids',$modid);
		?>
	</form>
</div>