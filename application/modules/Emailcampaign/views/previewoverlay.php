<!-- Template Print Overlay  Overlay-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="previewoverlayoverlay" style="overflow-y: scroll;overflow-x: hidden">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="large-8 medium-8 large-centered medium-centered columns overlayborder" >
			<form method="POST" name="" style="" id="" action="" enctype="" class="clearformtaxoverlay">
				<span id="pdfviewoverlayformvalidation" class="validationEngineContainer"> 
					<div class="large-12 columns headercaptionstyle headerradius paddingzero">
						<div class="large-6 medium-6 small-6 columns headercaptionleft">
							Preview
						</div>
						<div class="large-5 medium-6 small-6 columns addformheadericonstyle righttext small-only-text-center">
							<span title="Close" id="previewclose"><i class="material-icons" title="Close">close</i></span>
						</div>
					</div>
					<div class="large-12 columns paddingzero" style="background:#e0e0e0">
						<div class="row">&nbsp;</div>
						<div class="large-12 columns end paddingbtm">
							<div class="row" style="background:#f5f5f5">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-12 columns">Preview</div>
						</div>
							</div>
							<div style="background:#f5f5f5" class="row">
								<iframe id="templatepreviewframe" name="templatepreviewframe" frameborder='0' width='100%' height='400' allowtransparency='true'></iframe>
								<div class="large-12 columns">&nbsp;</div>
							</div>
						</div>
					</div>
					<div class="row">&nbsp;</div>
					<div class="row">&nbsp;</div>					
				</span>
			</form>
		</div>
	</div>
</div>

<!-- Amount to words field selection overlay-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="moneyconvertoverlay" style="overflow-y: scroll;overflow-x: hidden">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="large-6 medium-6 large-centered medium-centered columns overlayborder" >
			<form method="POST" name="" style="" id="" action="" enctype="" class="clearformformatoroverlay">
				<span id="moneyconvertvalidation" class="validationEngineContainer"> 
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-10 columns" style="background:none">Formatter</div>
							<div class="small-2 columns" id="formatorclose" style="text-align:right;cursor:pointer;background:none"><i class="material-icons">cancel</i></div>
						</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div style="background:#f5f5f5" class="row">
						<div class="large-12 medium-12 small-12 columns">
							<div class="large-6 medium-6 columns">
								<label>Formator Type</label>
								<select id="curencyformatype" class="chzn-select " data-placeholder="Select" data-prompt-position="topLeft:14,36" tabindex="-1" name="curencyformatype" title="">
									</option value="">Select</option>
									<option value="CW">1-Currency To Word</option>
									<option value="CS">2-Currency Symbol</option>
								</select>
							</div>
							<div class="large-6 medium-6 columns">
								<label>Field Name</label>
								<select id="amttowordfieldname" class="chzn-select " data-placeholder="Select" data-prompt-position="topLeft:14,36" tabindex="-1" name="amttowordfieldname" title="">
								</select>
							</div>
						</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 medium-12 small-12 columns">
							<div class="large-4 medium-4 small-4 columns">&nbsp;</div>
							<div class="large-4 medium-4 small-4 columns">
								<input id="saveformatorword" class="btn formbuttonsalert" type="button" name="saveformatorword" value="Save" style="background: none repeat scroll 0% 0% rgb(37, 116, 169); border-color: rgb(37, 116, 169);">
							</div>
							<div class="large-4 medium-4 small-4 columns">&nbsp;</div>
						</div>
					</div>
					<div class="row" style="background:#f5f5f5">&nbsp;</div>
				</span>
			</form>
		</div>
	</div>
</div>