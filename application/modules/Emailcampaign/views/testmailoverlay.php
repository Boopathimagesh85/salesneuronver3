<!-- Template Print Overlay  Overlay-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="testmailoverlayoverlay" style="overflow-y: scroll;overflow-x: hidden;">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-4 medium-6 large-centered medium-centered columns" >
			<form method="POST" name="testmailform" style="" id="testmailform" action="" enctype="" class="cleartestmailoverlayform">
				<span id="testmailoverlayvalidation" class="validationEngineContainer"> 
					<div class="alert-panel">
						<div class="alertmessagearea">
							<div class="alert-title">Confirmation</div>
							<div class="alert-message">
							<span>Do you want to send a test mail?</span>
							<div class="input-field ">   
								<span class="firsttab" tabindex="1000"></span>          
								<input type="email" id="testmailid" class="validate[required] ffield" tabindex="1001" name="testmailid" value="" />
								<label for="testmailid">Mail Id<span class="mandatoryfildclass">*</span></label>
							</div>
							</div>
						</div>
						<div class="alertbuttonarea">
							<input type="button" id="testmailyesbtn" name="testmailyesbtn" tabindex="1002" value="Yes" class="alertbtnyes" >
							<input type="button" id="testmailnobtn" name="testmailnobtn" tabindex="1003" value="No" class="flloop alertbtnno" >	
							<span class="lasttab" tabindex="1005"></span>
						</div>
					</div>
				</span>
			</form>
		</div>
	</div>
</div>