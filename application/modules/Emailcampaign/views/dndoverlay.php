<!--Mail Log Grid---->
<div class="large-6 columns" style="position:absolute;">
	<div class="overlay" id="maillogoverlay" style="overflow-hidden: scroll;overflow-x: hidden;z-index:40">		
		<div class="row hide-for-small-only">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="large-10 medium-10 large-centered medium-centered columns">
			<div class="large-12 columns headercaptionstyle headerradius paddingzero borderstyle" style="background-color: #FFFFFF">
				<div class="large-6 medium-6 small-6 columns headercaptionleft" style="line-height:1.9rem;">
					Mail Log
					<input id="campaignid" type="hidden" name="campaignid" value=""/>
				</div>
				<div class="large-5 medium-6 small-6 columns addformheadericonstyle righttext" style="line-height:2.1rem;">
					<span id="maillogcsv" class="icon-box" title="XL"><i class="icon-w icon-file-excel"></i></span>
					<span id="resendmail" class="icon-box" title="Resend"><i class="icon-w icon-reply"></i></span>
					<span id="maillogclose" class="icon-box" title="Close"><i class="material-icons">close</i></span>
				</div>
			</div>
			<?php
				$device = $this->Basefunctions->deviceinfo();
				if($device=='phone') {
					echo '<div class="large-12 columns paddingzero forgetinggridname" id="mailloggridwidth"><div class="desktop row-content inner-gridcontent" id="mailloggrid" style="height:420px;top:0px;">
						<!-- Table header content , data rows & pagination for mobile-->
					</div>
					<footer class="inner-gridfooter footercontainer" id="mailloggridfooter">
						<!-- Footer & Pagination content -->
					</footer></div>';
				} else {
					echo '<div class="large-12 columns forgetinggridname" id="mailloggridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent borderstyle" id="mailloggrid" style="max-width:2000px; height:420px;top:0px;">
					<!-- Table content[Header & Record Rows] for desktop-->
					</div>
					<div class="inner-gridfooter footer-content footercontainer" id="mailloggridfooter">
						<!-- Footer & Pagination content -->
					</div></div>';
				}
			?>
		</div>
	</div>
</div>
<!-- mail resent overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="resentmailoverlay" style="overflow-y: scroll;overflow-x: hidden">		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-4 medium-6 large-centered medium-centered columns overlayborder" >
			<form method="POST" name="resentmailform" style="" id="resentmailform" action="" enctype="" class="clearresentmailoverlayform">
				<span id="resentmailoverlayvalidation" class="validationEngineContainer"> 
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-10 columns" style="background:none">Resent Mail Confirmation</div>
							<div class="small-1 columns" id="resaentmailclose" style="text-align:right;cursor:pointer;background:none">X</div>
						</div>
						<div class="large-12 medium-12 columns small-12 ">  
							<div class="large-12 columns">&nbsp;</div>
							<div class="large-12 medium-12 columns small-12">             
								<label>Mail Id<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="large-12 medium-12 columns small-12">             
								<input type="email" id="resentmailid" class="validate[required]" name="resentmailid" value="" />
								<input id="maillogid" type="hidden" name="maillogid" value=""/>
							</div>
						</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 medium-12 columns small-12 centertext">
							<label>Do you want to resent a mail</label>
						</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 medium-12 columns small-12 ">  
							<div class="large-6 medium-6 columns small-6">             
								<input type="button" id="resentmailyesbtn" name="resentmailyesbtn" value="Yes" class="btn formbuttonsalert" >	
							</div>
							<div class="large-6 medium-6 columns small-6 ">             
								<input type="button" id="resentmailnobtn" name="resentmailnobtn" value="No" class="btn formbuttonsalert" >
							</div>
						</div>
						<div class="medium-4 large-4 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div class="row">&nbsp;</div>
				</span>
			</form>
		</div>
	</div>
</div>