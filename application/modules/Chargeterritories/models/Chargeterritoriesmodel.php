<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Chargeterritoriesmodel extends CI_Model{
    private $chargeterritoriesmoduleid =21;
	public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//chargeterritories create
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['chargeterritorieselementsname']);
		$formfieldstable = explode(',',$_POST['chargeterritorieselementstable']);
		$formfieldscolmname = explode(',',$_POST['chargeterritorieselementscolmn']);
		$elementpartable = explode(',',$_POST['chargeterritorieselementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		//check the rate exists already.
		$id=$this->db->select('chargeterritoriesid')
					 ->from('chargeterritories')
					 ->where(array('additionalchargetypeid'=>$_POST['additionalchargetypeid'],'territoryid'=>$_POST['territoryid'],'status'=>$this->Basefunctions->activestatus))
					 ->limit(1)
					 ->get();
		if($id->num_rows() > 0){
			//if records exits previously update the records
			foreach($id->result() as $info){
				$primaryid = $info->chargeterritoriesid;
			}
			$value_array=array('value'=>trim($_POST['regionvalue']),'lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid);
			$this->db->where('chargeterritoriesid',$primaryid);
			$this->db->update('chargeterritories',$value_array);
		} else {
			//enter new records
			$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
		}
		echo 'TRUE';
	}
	//Retrive chargeterritories data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['chargeterritorieselementsname']);
		$formfieldstable = explode(',',$_GET['chargeterritorieselementstable']);
		$formfieldscolmname = explode(',',$_GET['chargeterritorieselementscolmn']);
		$elementpartable = explode(',',$_GET['chargeterritorieselementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['chargeterritoriesprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	//chargeterritories update
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['chargeterritorieselementsname']);
		$formfieldstable = explode(',',$_POST['chargeterritorieselementstable']);
		$formfieldscolmname = explode(',',$_POST['chargeterritorieselementscolmn']);
		$elementpartable = explode(',',$_POST['chargeterritorieselementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['chargeterritoriesprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		$chargeprimaryid = $_POST['chargeterritoriesprimarydataid'];
		$additionalchargetypeid = $_POST['additionalchargetypeid'];
		$territoryid = $_POST['territoryid'];
		$ludate = date($this->Basefunctions->datef);
		$luuserid= $this->Basefunctions->userid;
		$delete=array('status'=>0,'lastupdatedate'=>$ludate,'lastupdateuserid'=>$luuserid);
		$this->db->where_in('additionalchargetypeid',$additionalchargetypeid);
		$this->db->where_in('territoryid',$territoryid);
		$this->db->where_not_in('chargeterritoriesid',$chargeprimaryid);
		$this->db->update('chargeterritories',$delete);
		echo $result;
	}
	//chargeterritories delete
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['chargeterritorieselementstable']);
		$parenttable = explode(',',$_GET['chargeterritoriesparenttable']);
		$id = $_GET['chargeterritoriesprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				echo "TRUE";
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			echo "TRUE";
		}
	} 
	/**
	* Dynamically retrives the charges names
	*/
	public function getchargename(){
		$industryid = $this->Basefunctions->industryid;
		$charge=array();
		$data=$this->db->query("SELECT additionalchargetypeid,additionalchargetypename
							   FROM additionalchargetype
							   WHERE taxmodeid = 3 AND status = 1 AND FIND_IN_SET('$industryid',additionalchargetype.industryid) >0 ");
		foreach($data->result() as $inf){
			$charge[]=array('id'=>$inf->additionalchargetypeid,'name'=>$inf->additionalchargetypename);
		}
		echo json_encode($charge);
	}
	//get charge type 
	public function getchergetypemodel() {
		$chageid = $_GET['chargeid'];
		$this->db->select('calculationtypeid');
		$this->db->from('additionalchargetype');
		$this->db->where('additionalchargetypeid',$chageid);
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$chargetype = $row->calculationtypeid;
			}
		} else {
			$chargetype = '';
		}
		echo json_encode($chargetype);
	}
}