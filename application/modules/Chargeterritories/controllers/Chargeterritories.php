<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Chargeterritories extends MX_Controller{
	private $chargeterritoriesmoduleid =21;
    public function __construct()    {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Chargeterritories/Chargeterritoriesmodel');	
    }
    //first basic hitting view
    public function index()    {        
    	$moduleid = array($this->chargeterritoriesmoduleid);
    	sessionchecker($moduleid);
		/*action*/
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids =$moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Chargeterritories/chargeterritoriesview',$data);
	}
	//create chargeterritories
	public function newdatacreate() {  
    	$this->Chargeterritoriesmodel->newdatacreatemodel();
    }
	//information fetchchargeterritories
	public function fetchformdataeditdetails() {
		$this->Chargeterritoriesmodel->informationfetchmodel($this->chargeterritoriesmoduleid);
	}
	//update chargeterritories
    public function datainformationupdate() {
        $this->Chargeterritoriesmodel->datainformationupdatemodel();
    }
	//delete chargeterritories
    public function deleteinformationdata() {
      $this->Chargeterritoriesmodel->deleteoldinformation($this->chargeterritoriesmoduleid);
    }
	//get the charge name
	public function getchargename() {
		$this->Chargeterritoriesmodel->getchargename();
	}
	//get chage type
	public function getchergetype() {
		$this->Chargeterritoriesmodel->getchergetypemodel();
	}
}