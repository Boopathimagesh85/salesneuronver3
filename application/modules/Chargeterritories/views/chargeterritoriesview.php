<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('Base/headerfiles'); ?>
</head>
<body>
<?php
		$dataset['gridenable'] = "1"; //0-no  1-yes
		$dataset['griddisplayid'] = "chargeterritoriesaddformview"; //add form-div id
		$dataset['maingridtitle'] = $gridtitle['title'];   // form header
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];  //grid header
		$dataset['spanattr'] = array();     //
		$dataset['gridtableid'] = "chargeterritoriesaddgrid"; //grid id
		$dataset['griddivid'] = "chargeterritoriesaddgridnav"; //grid pagination
		$this->load->view('Chargeterritories/chargeterritoriesform',$dataset); 
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$this->load->view('Base/overlaymobile');
		} else {
			$this->load->view('Base/overlay');
		}
?>	
</body>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js Files -->	
	<script src="<?php echo base_url();?>js/Chargeterritories/chargeterritories.js" type="text/javascript"></script> 	
</html>