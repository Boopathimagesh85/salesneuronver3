<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Taxmaster extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Taxmaster/Taxmastermodel');
    }
    //first basic hitting view
    public function index()
    {        
    	$industryid = $this->Basefunctions->industryid;
    	if($industryid == 3) {
    	 $moduleid = array(133);
    	}else{
    	 $moduleid = array(234);
    	}
    	sessionchecker($moduleid);
		//action
    	$data['gstapplicable']=$this->Basefunctions->get_company_settings('gstapplicable');
		$data['moddashboardtabgrp'] = $this->Basefunctions->moddashboardtabgrpgeneration($moduleid);
		$data['moddbfrmfieldsgen'] = $this->Basefunctions->dashboardmodsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$this->load->view('Taxmaster/taxmasterview',$data);
	}
}