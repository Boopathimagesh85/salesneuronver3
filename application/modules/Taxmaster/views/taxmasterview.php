<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>	
</head>
<style type="text/css">
	#taxcategoryadvanceddrop, #taxnameadvanceddrop{
		left: -82.1406px !important;
	}
	.gridcontent{
		height: 71vh !important;
	}
	#taxcategoryaddgrid {
	height: 77vh !important;
	}
	#taxnameaddgrid {
	height: 77vh !important;
	}
	.tabs .dbtab-title, .multitabs .dbtab-title {
		height:38px !important;
	}
	
</style>
<body>
<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<?php
				$modid = '220,221,20';
				$dataset['gridtitle'] = $gridtitle['title'];
				$dataset['titleicon'] = $gridtitle['titleicon'];
				$dataset['formtype'] = 'multipleaddform';
				$dataset['multimoduleid'] = $modid;
				$this->load->view('Base/mainviewaddformheader',$dataset);
			?>
			<div class="large-12 columns paddingzero scrollbarclass taxmastertouch">
				<div class="mastermodules">
				<?php
					//function call for dash board form fields generation
					dashboardformfieldsgeneratorwithgrid($moddbfrmfieldsgen,1,$modid);
				?>
				<input type="hidden" name="gstapplicable" id="gstapplicable" value="<?php echo $gstapplicable;?>"/>		
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	$device = $this->Basefunctions->deviceinfo();
	if($device=='phone') {
		$this->load->view('Base/overlaymobile');
	} else {
		$this->load->view('Base/overlay');
	}
	$this->load->view('Base/modulelist');
?>	
</body>
	<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<?php $this->load->view('Base/bottomscript'); ?>
	<script>
		$("#tab1").click(function()	{
			$('#taxcategorydataupdatesubbtn').hide();
			$('#taxcategoryreloadicon,#taxcategorysavebutton').show();
			resetFields(); mastertabid=1; masterfortouch = 0;
			$(".tabclass").addClass('hidedisplay');
			$("#tab1class").removeClass('hidedisplay');
		});
		$("#tab2").click(function()
		{
			$('#taxnamedataupdatesubbtn').hide();
			$('#taxnamereloadicon,#taxnamesavebutton').show();
			taxcategorydatareload();
			$(".tabclass").addClass('hidedisplay');
			$("#tab2class").removeClass('hidedisplay');
			taxnameaddgrid();
			resetFields();
			 mastertabid= 2; 
			 masterfortouch = 0;
		});
		$("#tab3").click(function()
		{
			$('#taxterritoriesdataupdatesubbtn').hide();
			$('#taxterritoriesreloadicon,#taxterritoriessavebutton').show();
			$(".tabclass").addClass('hidedisplay');
			$("#tab3class").removeClass('hidedisplay');
			taxterritoriesaddgrid();
			loadtaxname();
			resetFields();
			mastertabid= 3; 
			masterfortouch = 0;
		});
		$(document).ready(function(){
			{//for keyboard global variable
				 viewgridview = 0;
				 innergridview = 1;
			 }
		});		
		{//enable support icon
			$(".supportoverlay").css("display","inline-block")
		}	
	</script>
</html>