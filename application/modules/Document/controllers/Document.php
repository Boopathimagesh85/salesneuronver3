<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Document extends MX_Controller{
	//To load the Register View
	function __construct(){
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->helper('download');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Document/Documentsmodel');
    }
	public function index()	{
		$moduleid = array(207);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);		
		$viewfieldsmoduleids = array(207);
		$viewmoduleid = array(207);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Document/documentsview',$data);
	}
	//Create New data
	public function newdatacreate() {
    	$this->Documentsmodel->newdatacreatemodel();
    }
	//delete old information
    public function deleteinformationdata(){
    	$moduleid = array(207);
        $this->Documentsmodel->deleteoldinformation($moduleid);
    } 
	//download documents value fetch
	public function docdownvalfetch(){
		$this->Documentsmodel->docdownvalfetchmodel();
	}
	//module name based table name
	public function tablenameget(){
		$this->Documentsmodel->tablenamegetmodel();
	}
	//record name fetch
	public function recordnamefetch(){
		$this->Documentsmodel->recordnamefetchmodel();
	}
	public function getuploadid(){
		$this->Documentsmodel->getuploadidmodel();
	}
	//download function 
	public function download(){
		$this->Documentsmodel->downloadmodel();
	}
	//Folder insertion
	public function documentsfolderinsert() {
		$this->Documentsmodel->documentsfolderinsertmodel();
	}
	//Folder Grid View
	public function documentsfoldernamegridfetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'foldername.foldernameid') : 'foldername.foldernameid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('foldernamename','description','setdefault','public'),'colmodelindex'=>array('foldername.foldernamename','foldername.description','foldername.setdefault','foldername.public'),'coltablename'=>array('foldername','foldername','foldername','foldername'),'uitype'=>array('2','2','2','2'),'colname'=>array('Folder Name','Description','Default','Public'),'colsize'=>array('200','200','200','200'));
		$result=$this->Documentsmodel->documentsfoldernamegridxmlview($primarytable,$sortcol,$sortord,$pagenum,$rowscount);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($result,$page,'');
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Folder List',$width,$height);
		} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Folder List',$width,$height);
		}
		echo json_encode($datas);
	}
	//Folder data fetch
	public function documentsfolderdatafetch() {
		$this->Documentsmodel->documentsfolderdatafetchmodel();
	}
	//Folder Update
	public function documentsfolderupdate() {
		$this->Documentsmodel->documentsfolderupdatemodel();
	}
	//Folder Delete
	public function folderdeleteoperation() {
		$this->Documentsmodel->folderdeleteoperationmodel();
	}
	//Folder drop down Fetch Data
	public function fetchdddataviewddval() {
		$this->Documentsmodel->fetchdddataviewddvalmodel();
	}
	//Folder Unique Name
	public function foldernameunique() {
		$this->Documentsmodel->foldernameuniquemodel();
	}
}
