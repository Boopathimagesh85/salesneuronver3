<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
</head>
<body>
	<?php
		$moduleid = implode(',',$moduleids);
		$device = $this->Basefunctions->deviceinfo();
		$dataset['gridenable'] = "yes"; //no
		$dataset['griddisplayid'] = "documentsview";
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['spanattr'] = array();
		$dataset['gridtableid'] = "documentsaddgrid";
		$dataset['griddivid'] = "documentsaddgridnav";
		$dataset['moduleid'] = $moduleids;
		$dataset['forminfo'] = array(array('id'=>'documentsaddform','class'=>'hidedisplay','formname'=>'documentsaddform'));
		if($device=='phone') {
			$this->load->view('Base/gridmenuheadermobile',$dataset);
			$this->load->view('Base/overlaymobile');
			$this->load->view('Base/viewselectionoverlay');
		} else {
			$this->load->view('Base/gridmenuheader',$dataset);
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
		$this->load->view('documentdeleteform');
		$this->load->view('documentsalesneuronoverlay');
	?>
</body>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js Files -->
	<script src="<?php echo base_url();?>js/plugins/say-cheese.js" type="text/javascript"></script> 
	<script src="<?php echo base_url();?>js/Document/document.js" type="text/javascript"></script> 
	<!--For Tablet and Mobile view Dropdown Script-->
	<script>
		$(document).ready(function() {			
			$("#tabgropdropdown").change(function() {
				var tabgpid = $("#tabgropdropdown").val();
				$('.sidebaricons[data-subform="'+tabgpid+'"]').trigger('click');
				$('.addformcontainer').animate({scrollTop:0},'slow');
			});
		});
	</script>
</html>