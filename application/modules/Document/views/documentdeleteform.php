<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="basedeleteoverlay">	
		<div class="row">&nbsp;</div>
		<div class="overlaybackground">
			<div class="alert-panel">
				<div class="alertmessagearea">
				<div class="row">&nbsp;</div>
					<div class="alert-message">Delete this data?</div>
				</div>
				<div class="alertbuttonarea">
					<span class="firsttab" tabindex="1000"></span>
					<input type="button" id="basedeleteyes" name="" value="Delete" tabindex="1001" class="alertbtnyes ffield " >	
					<input type="button" id="basedeleteno" name="" value="Cancel" tabindex="1002" class="alertbtnno flloop  alertsoverlaybtn" >
					<input type="hidden" id="geventid" name="gdeleventid" value="" class="" >
					<span class="lasttab" tabindex="1003"></span>
				</div>
				<div class="row">&nbsp;</div>
			</div>
		</div>
	</div>
</div>


<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="folderbasedeleteoverlayforcommodule">
		<div class="row">&nbsp;</div>
		<div class="alert-panel">
			<div class="alertmessagearea">
				<div class="alert-title">Alert</div>
				<div class="alert-message basedelalerttxt">Delete this data?</div>
			</div>
			<div class="alertbuttonarea">
				<span class="firsttab" tabindex="1000"></span>
				<input type="button" id="folderdeleteyes" name="folderdeleteyes" value="Yes" tabindex="1001" class="alertbtn ffield commodyescls">
				<input type="button" id="folderdeleteno" name="folderdeleteno" tabindex="1002" value="No" class="alertbtn flloop alertsoverlaybtn" >
				<span class="lasttab" tabindex="1003"></span>
			</div>
		</div>
	</div>
</div>