<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="salesneurondocumentoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-10 medium-10 small-12 large-centered medium-centered small-centered columns cleardataform">
			<div class="large-12 columns headercaptionstyle headerradius " >
				<div class="large-6 medium-6 small-8 columns headercaptionleft">
					From Application
				</div>
				<div class="large-5 medium-6 small-12 columns addformheadericonstyle righttext small-only-text-center" style="text-align:right;padding-right:0 !important">
					<span class="fa fa-search searchiconclass " title="Search" id="docinlinesearch"> </span>
					<span class="fa fa-floppy-o" title="Submit" id="docfoldersbt"> </span>
					<span class="fa fa-times" title="Close" id="closeoverlay"> </span>
				</div>
			</div>
			<div class="row">&nbsp;</div>
			<div class="large-12 column paddingzero"  style="background:#f5f5f5">
				<form method="POST" name="" class=""  id="">
					<div id="" class="validationEngineContainer">
						<div class="large-4 medium-4 columns" style="padding-bottom:1rem" id="leftpanearea">	
							<div class="large-12 columns" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff">
								<div class="large-12 column padding-space-open-for-form"  id="folderlistarea">
									<div class="large-12 columns headerformcaptionstyle" style="padding-left:0;padding-right:0;padding-top:0.2rem;">
										<div class="large-10 medium-9 small-10 column" style="padding-left:0;line-height: 1.3"> Folders </div>
									</div>
									<div class="large-12 columns paddingzero">
										<div style="margin:1rem">
											<input type="text" id="docuemntfoldersearch" class="searchiconimg" value=""/>
										</div>
										<ul id="appendreportvalue" class="appendvaluesearchrep">
			
										</ul>							
									</div>
									
									<ul class="reportsfolderlist search-list-report" id="maindcocumentfolder">
										<li class="repfolderlistulstyle">
											<label class="repfolderlistheader" data-parentreportfolderid='all'>
												<i class="fa fa-folder-o fa-1x" style="color:#575757;padding-right: 0.7rem;"></i>
												<span>Uploads</span>
											</label>
										</li>
									</ul>
									<div class="large-12 columns">&nbsp;</div>
								</div>
							</div>
						</div>
					</div>	
				</form>
				<div class="large-8 medium-8 columns" style="padding-bottom:1rem" id="reportarea">	
					<div class="large-12 columns viewgridcolorstyle padding-space-open-for-form" >	
						<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0 0.4rem;">	
							<span>Document List</span>
						</div>
						<div class="large-12 columns forgetinggridname" id="reportsgridwidth" style="padding-left:0;padding-right:0;">
							<table id="documentsfoldergrid"> </table> 
							<div id="documentsfoldergriddiv" class="w100"></div>  		
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="documentpreviewoverlay" style="overflow-y: scroll;overflow-x: hidden">	
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-10 medium-10 large-centered medium-centered columns" >
			<div class="row">&nbsp;</div>
			<div class="row" style="background:#f5f5f5">
				<div class="large-12 columns headerformcaptionstyle">
					<div class="small-10 columns" style="background:none">Preview</div>
					<div class="small-1 columns" id="previewclose" style="text-align:right;cursor:pointer;background:none">X</div>
				</div>
				<div class="large-12 columns">&nbsp;</div>
				<div class="large-12 columns">&nbsp;</div>
			</div>
			<div style="background:#f5f5f5" class="row">
				<iframe id="templatepreviewframe" name="templatepreviewframe" frameborder='0' width='100%' height='450' allowtransparency='true' src=""></iframe>
				<div class="large-12 columns">&nbsp;</div>
			</div>
			<div class="row">&nbsp;</div>
		</div>
	</div>
</div>