<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Model File
class Documentsmodel extends CI_Model{    
    public function __construct(){		
		parent::__construct(); 
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');	
    }  
	//To Create New documents Details
	public function newdatacreatemodel() {
		$cdate = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$status	= $this->Basefunctions->activestatus;
		//industry based moduleid
		$indmoduleid = $_POST['viewfieldids'];
		$folname = $_POST['foldernameid'];
		$keywords = $_POST['keywords'];
		if(isset($_POST['documentsmoduleid'])) {
			$docmoduleid = $_POST['documentsmoduleid'];
		} else { 
			$docmoduleid = ''; 
		}
		if($docmoduleid != ""){ 
			$moduleid = $docmoduleid; 
		} else { 
			$moduleid = 1; 
		}
		if(isset($_POST['documentsmoduleid']) && !empty($_POST['documentsmoduleid'])) {
			$commonid = $_POST['commonid'];
		} else { 
			$commonid = 1; 
		}
		$uploadid = $_POST['filename_fromid'];
		$filename = $_POST['filename'];
		$flesize = $_POST['filename_size'];
		$filetype = $_POST['filename_type'];
		$filepath = $_POST['filename_path'];
		$fname = explode(',',$filename);
		$fsize = explode(',',$flesize);
		$ftype = explode(',',$filetype);
		$fpath = explode(',',$filepath);
		$fromid = explode(',',$uploadid);
		$count = count($fname);
		//prepare user notification*
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		//looping for multi insertion
		for($i=0;$i < $count;$i++){
			if($fname[$i] != '') {
				$document = array(
					'commonid'=>$commonid,
					'moduleid'=>$moduleid,
					'filename'=>$fname[$i],
					'filename_size'=>$fsize[$i],
					'filename_type'=>$ftype[$i],
					'filename_path'=>$fpath[$i],
					'filename_fromid'=>$fromid[$i],
					'foldernameid'=>$folname,
					'keywords'=>$keywords,
					'industryid'=>$this->Basefunctions->industryid,	
					'branchid'=>$this->Basefunctions->branchid,
					'createdate'=>$cdate,
					'lastupdatedate'=>$cdate,
					'createuserid'=>$userid,
					'lastupdateuserid'=>$userid,
					'status'=>$status
				);
				$this->db->insert('crmfileinfo',$document);
				//notification log entry
				$primaryid = $this->db->insert_id();
				$notimsg = $this->Basefunctions->notificationtemplatedatafetch($indmoduleid,$primaryid,'crmfileinfo',2);
				if($notimsg != '') {
					$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,1,$indmoduleid);
				}
			}
			{//workflow management -- for data create
				$this->Basefunctions->workflowmanageindatacreatemode($indmoduleid,$primaryid,'crmfileinfo');
			}
		}
		echo "TRUE";
	}
	//delete old information
	public function deleteoldinformation($moduleid){
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$msg='False';
		$id = $_GET['primarydataid'];
		$filename = $this->Basefunctions->generalinformaion('crmfileinfo','filename','crmfileinfoid',$id);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//industrybased moduleid fetch
		$modid = $this->Basefunctions->getmoduleid($moduleid);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		{//workflow management -- for data delete
			$this->Basefunctions->workflowmanageindatadeletemode($modid,$id,$primaryname,$partabname);
		}
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				$msg='Denied';
			} else {
				//notification log
				$notimsg = $this->Basefunctions->notificationtemplatedatafetch($modid,$id,$partabname,4);
				if($notimsg != '') {
					$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,1,$modid);
				}
				$filename_pathh = $this->Basefunctions->singlefieldfetch('filename_path','crmfileinfoid','crmfileinfo',$id); //default record
				if($filename_pathh != '') {
					unlink($filename_pathh);
				}
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				$msg='TRUE';
			}
		} else {
			//notification log
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch($modid,$id,$partabname,4);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,1,$modid);
			}
			$filename_pathh = $this->Basefunctions->singlefieldfetch('filename_path','crmfileinfoid','crmfileinfo',$id); //default record
			if($filename_pathh != '') {
				unlink($filename_pathh);
			}
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			$msg='TRUE';
		}
		echo $msg;
	}
	//documents download value fetch
	public function docdownvalfetchmodel(){
		$id = $_POST['id'];
		$data = array();
		$this->db->select('crmfileinfo.filename_path,crmfileinfo.filename_fromid,crmfileinfo.filename_size,crmfileinfo.filename,crmfileinfo.filename_type',false);
		$this->db->from('crmfileinfo');
		$this->db->where('crmfileinfo.crmfileinfoid',$id);
		$result = $this->db->get();
		if($result ->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('mode'=>$row->filename_fromid,'path'=>$row->filename_path,'size'=>$row->filename_size,'name'=>$row->filename,'type'=>$row->filename_type);
				echo json_encode($data); 			
			}
		} else {
			echo json_encode(array('failed'=>'Failure'));
		}
	}
	//table name fetch function
	public function tablenamegetmodel() {
		$id = $_GET['moduleid'];
		$this->db->select('fieldname,tablename',false);
		$this->db->from('primaryfieldmapping');
		$this->db->where('primaryfieldmapping.moduleid',$id);
		$this->db->where('primaryfieldmapping.status',1);
		$result = $this->db->get();
		if($result ->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('fieldname'=>$row->fieldname,'tablename'=>$row->tablename);
			}
			echo json_encode($data);
		} else {
			echo json_encode(array('failed'=>'Failure'));
		}	
	}
	//user name fetch
	public function recordnamefetchmodel() {
		$i =0;
		$tablename = $_GET['tabname'];
		$fieldname = $_GET['fieldname'];
		$fieldid = $tablename.'id';
		$industryid = $this->Basefunctions->industryid;
		if($tablename == 'lead' || $tablename == 'contact' || $tablename == 'employee') {
			$this->db->select($fieldid.','.$fieldname.',CONCAT(salutation.salutationname, '.$fieldname.', lastname) AS name', FALSE);
			$this->db->from($tablename);
			$this->db->join('salutation','salutation.salutationid='.$tablename.'.salutationid','left outer');
		} else {
			$this->db->select($fieldname.' AS name ,'.$fieldid.','.$fieldname);
			$this->db->from($tablename);
		}
		$this->db->where($tablename.'.'.'status',1);
		$this->db->where("FIND_IN_SET('".$industryid."',$tablename.industryid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$data[$i] = array('id'=>$row->$fieldid,'name'=>$row->name,'fieldname'=>$fieldname);
				$i++;
			}
			echo json_encode($data); 			
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}
	}
	public function getuploadidmodel(){
		$id = $_GET['id'];
		$this->db->select('filename_fromid');
		$this->db->from('crmfileinfo');
		$this->db->where('crmfileinfo.crmfileinfoid',$id);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				$data = $row->filename_fromid;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}
	}
	//download file
	public function downloadmodel() {
		$filename = trim($_POST['filename']);
		$path = trim($_POST['path']);
		$size = trim($_POST['filesize']);
		$type = trim($_POST['filetype']);
		$mime = (($type!="")?$type:'application/force-download');
		$fd = @fopen ($path, "r");
		$fsize= filesize($path);
		//exit;
		//ob_clean();
		ob_end_clean();
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private");
		header("Content-Description: File Transfer");
		header("Content-Type: $mime");
		header("Content-Disposition: attachment;filename=\"$filename\"");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length:$fsize");
		while(!feof($fd)) {
			$buffer = fread($fd,$fsize);
			echo $buffer;
		}
		fclose ($fd);
		exit;
	}
	//Folder main grid view
	public function documentsfoldernamegridxmlview($tablename,$sortcol,$sortord,$pagenum,$rowscount) {
		$dataset = 'foldername.foldernamename,foldername.foldernameid,foldername.description,foldername.setdefault,foldername.public';
		$join ='';
		$status = $tablename.'.status IN (1)';
		$where = $tablename.'.moduleid IN (207)';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		//query
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$where.' AND '.$status.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		return $data;
	}
	//Folder Insertion
	public function documentsfolderinsertmodel() {
		$foldername = $_POST['foldername'];
		$description = $_POST['description'];
		$default = $_POST['setdefault'];
		$ppublic = $_POST['ppublic'];
		$date = date($this->Basefunctions->datef);
		$folderarray = array(
						'foldernamename'=>$foldername,
						'description'=>$description,
						'moduleid'=>'207',
						'setdefault'=>$default,
						'public'=>$ppublic,
						'userroleid'=>$this->Basefunctions->userroleid,
						'industryid'=>$this->Basefunctions->industryid,
						'createdate'=>$date,
						'lastupdatedate'=>$date,
						'createuserid'=>$this->Basefunctions->userid,
						'lastupdateuserid'=>$this->Basefunctions->userid,
						'status'=>$this->Basefunctions->activestatus
					);
		$this->db->insert('foldername',$folderarray);
		$folderid = $this->db->insert_id();
		$this->setdefaultupdate(207,$default,$folderid);
		echo "TRUE";
	}
	//set default update
	public function setdefaultupdate($moduleid,$default,$folderid) {
		if($default != 'No') {
			$updatearray = array('setdefault'=>'No');
			$this->db->where_not_in('foldername.foldernameid',$folderid);
			$this->db->where('foldername.moduleid',$moduleid);
			$this->db->update('foldername',$updatearray);
		}
	}
	//Folder Edit Data Fetch
	public function documentsfolderdatafetchmodel() {
		$folderid = $_GET['datarowid'];
		$this->db->select('SQL_CALC_FOUND_ROWS  foldername.foldernamename,foldername.foldernameid,foldername.description,foldername.setdefault,foldername.public',false);
		$this->db->from('foldername');
		$this->db->where('foldername.foldernameid',$folderid);
		$this->db->where('foldername.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data=array('id'=>$row->foldernameid,'foldername'=>$row->foldernamename,'description'=>$row->description,'setdefault'=>$row->setdefault,'ppublic'=>$row->public);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//Folder Update
	public function documentsfolderupdatemodel() {
		$foldernameid = $_POST['foldernameid'];
		$foldername = $_POST['foldername'];
		$description = $_POST['description'];
		$default = $_POST['setdefault'];
		$ppublic = $_POST['ppublic'];
		$date = date($this->Basefunctions->datef);
		$folderarray = array(
						'foldernamename'=>$foldername,
						'description'=>$description,
						'setdefault'=>$default,
						'public'=>$ppublic,
						'userroleid'=>$this->Basefunctions->userroleid,
						'industryid'=>$this->Basefunctions->industryid,
						'lastupdatedate'=>$date,
						'lastupdateuserid'=>$this->Basefunctions->userid,
						'status'=>$this->Basefunctions->activestatus
					);
		$this->db->where('foldername.foldernameid',$foldernameid);
		$this->db->update('foldername',$folderarray);
		$this->setdefaultupdate(207,$default,$foldernameid);
		echo "TRUE";
	}
	//Folder Delete
	public function folderdeleteoperationmodel() {
		$folderid = $_GET['id'];
		$sndefault = $this->Basefunctions->singlefieldfetch('sndefault','foldernameid','foldername',$folderid); //default record
		if($sndefault != '2') {
			$date = date($this->Basefunctions->datef);
			$folderarray = array(
							'lastupdatedate'=>$date,
							'lastupdateuserid'=>$this->Basefunctions->userid,
							'status'=>0
						);
			$this->db->where('foldername.foldernameid',$folderid);
			$this->db->update('foldername',$folderarray);
			echo "TRUE";
		} else {
			echo "DEFAULT";
		}
	}	
	//Folder Drop Down Reload function
	public function fetchdddataviewddvalmodel() {
		$loguserid = $this->Basefunctions->userid;
		$i = 0;
		$fieldname = $_GET['dataname'];
		$fieldid = $_GET['dataid'];
		$tablename = $_GET['datatab'];
		$moduleid = $_GET['moduleid'];
		$this->db->select("$fieldid,$fieldname,setdefault");
		$this->db->from("$tablename");
		$this->db->where("FIND_IN_SET('$moduleid',".$tablename.".moduleid) >", 0);
		if($loguserid != 2){
			$this->db->where_not_in("$tablename".'.public','No');
		}
		$this->db->where("$tablename".'.status',1);
		$result=$this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data[$i]=array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname,'setdefault'=>$row->setdefault);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		} 
	}
	
	//common id data fetch
	public function commondatafetch($documentid) {
		$commonname = '';
		$this->db->select('moduleid,commonid');
		$this->db->from('crmfileinfo');
		$this->db->where('crmfileinfo.crmfileinfoid',$documentid);
		$cresult = $this->db->get();
		if($cresult->num_rows() > 0) {
			foreach($cresult->result() as $row) {
				$moduleid = $row->moduleid;
				$commonid = $row->commonid;
			}
		}
		
		$this->db->select('modulemastertable');
		$this->db->from('module');
		$this->db->where('module.moduleid',$moduleid);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$mastertable = $row->modulemastertable;
			}
		}
		$mastertablename = $mastertable.'name';
		$mastertableid = $mastertable.'id';
		$this->db->select("$mastertablename");
		$this->db->from("$mastertable");
		$this->db->where("$mastertable".'.'."$mastertableid",$commonid);
		$this->db->where("$mastertable".'.status',1);
		$master = $this->db->get();
		if($master->num_rows() > 0) {
			foreach($master->result() as $row) {
				$commonname = $row->$mastertablename;
			}
		}
		return $commonname;
	}
	//Folder name unique model
	public function foldernameuniquemodel(){
		$industryid = $this->Basefunctions->industryid;
		$foldernamename = $_POST['foldernamename'];
		if($foldernamename != "") {
			$result = $this->db->select('foldername.foldernameid')->from('foldername')->where('foldername.foldernamename',$foldernamename)->where('foldername.moduleid','207')->where("FIND_IN_SET('$industryid',foldername.industryid) >", 0)->where('foldername.status','1')->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->foldernameid;
				}
			} else {
				echo "False";
			}
		} else {
			echo "False";
		}
	}
}      