<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Smssettings extends MX_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Smssettings/Smssettingsmodel');
	  }
    //first basic hitting view
    public function index() {
    	$moduleid = array(269);
    	sessionchecker($moduleid);
		/*action*/
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(269);
		$viewmoduleid = array(269);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Smssettings/smssettingsview',$data);
	}
	//show hide fields
	public function smscredentialadd() {
		$this->Smssettingsmodel->smscredentialaddmodel();
	}
	//sms credential grid data fetch
	public function smscredentialgriddatafetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'smsprovidersettings.smsprovidersettingsid') : 'smsprovidersettings.smsprovidersettingsid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('smsservicetypename','providername','smsprovidersettingsname','smssendtypename','username','password','virtualmobilenumbertypename','longnumbertypename','virtualmobilenumber','apikey','statusname'),'colmodelindex'=>array('smsservicetype.smsservicetypename','provider.providername','smsprovidersettings.smsprovidersettingsname','smsprovidersettings.username','smsprovidersettings.password','virtualmobilenumbertype.virtualmobilenumbertypename','longnumbertype.longnumbertypename','smsprovidersettings.virtualmobilenumber','smssendtype.smssendtypename','smsprovidersettings.apikey','status.statusname'),'coltablename'=>array('smsservicetype','provider','smsprovidersettings','smssendtype','smsprovidersettings','smsprovidersettings','virtualmobilenumbertype','longnumbertype','smsprovidersettings','smsprovidersettings','status'),'uitype'=>array('2','17','17','2','2','2','17','17','11','17','2','2'),'colname'=>array('SMS Service Type',"Provider Name", "Settings Name",'Transaction Mode', "User Name", "Password",'Virtual Mobile Number Type','Number Type','Virtual Mobile number','API Key', "Status"),'colsize'=>array('200','200','200','200','200','200','200','200','200','200','200'));
		$result=$this->Smssettingsmodel->smscredentialgriddatafetchgridxmlview($primarytable,$sortcol,$sortord,$pagenum,$rowscount);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($result,$page,'');
		$device = $this->Basefunctions->deviceinfo();
		//if($device=='phone') {
	//		$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'SMS Credential List',$width,$height);
		//} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'SMS Credential List',$width,$height);
		//}
		echo json_encode($datas);
	}
	//sms sender id grid data fetch
	public function smssenderidgriddatafetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'smssettings.smssettingsid') : 'smssettings.smssettingsid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('smsprovidersettingsname','senderid','apikey','setdefault','statusname'),'colmodelindex'=>array('smsprovidersettings.smsprovidersettingsname','smssettings.senderid','smssettings.apikey','smssettings.setdefault','smssettings.statusname'),'coltablename'=>array('smsprovidersettings','smssettings','smssettings','smssettings','status'),'uitype'=>array('19','2','2','13','2'),'colname'=>array("Settings Name", "SenderId Name", "API Key", "Set Default", "Status"),'colsize'=>array('200','200','200','200','200'));
		$result=$this->Smssettingsmodel->smssenderidgriddatafetchgridxmlview($primarytable,$sortcol,$sortord,$pagenum,$rowscount);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($result,$page,'');
		$device = $this->Basefunctions->deviceinfo();
	//	if($device=='phone') {
		//	$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'SMS Senderid List',$width,$height);
		//} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'SMS Senderid List',$width,$height);
		//}
		echo json_encode($datas);
	}
	//sms credential data fetch
	public function smscredentialeditdatafetch() {
		$this->Smssettingsmodel->smscredentialeditdatafetchmodel();
	}
	//sms credential update
	public function smscredentialupdate() {
		$this->Smssettingsmodel->smscredentialupdatemodel();
	}
	//sms credential delete
	public function smscredentialdelete() {
		$this->Smssettingsmodel->smscredentialdeletemodel();
	}
	//sms sender id add
	public function smssenderidadd() {
		$this->Smssettingsmodel->smssenderidaddmodel();
	}
	//sms sender data fetch
	public function smssenderiddatafetch() {
		$this->Smssettingsmodel->smssenderiddatafetchmodel();
	}
	//sms sender update
	public function smssenderidupdate() {
		$this->Smssettingsmodel->smssenderidupdatemodel();
	}
	//sms sender delete
	public function smssenderiddelete() {
		$this->Smssettingsmodel->smssenderiddeletemodel();
	}
	//sms settings name dd reload
	public function smsstttingsnameddreload() {
		$this->Smssettingsmodel->smsstttingsnameddreloadmodel();
	}
	//sms keyword Add
	public function smskeywordadd() {
		$this->Smssettingsmodel->smskeywordaddmodel();
	}
	//sms keyword Update
	public function smskeyworupdate() {
		$this->Smssettingsmodel->smskeyworupdatemodel();
	}
	//sms keyword grid Load
	public function smskeywordgriddatafetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'smskeywords.smskeywordsid') : 'smskeywords.smskeywordsid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('smskeywordsname','smsprovidersettingsname','description','autoreplymessage','forwardto','forwardemail','keywordvalidity','senderid','autoreplyonexpiry','statusname'),'colmodelindex'=>array('smskeywords.smskeywordsname','smsprovidersettings.smsprovidersettingsname','smskeywords.description','smskeywords.autoreplymessage','smskeywords.forwardto','smskeywords.forwardemail','smskeywords.keywordvalidity','smssettings.senderid','smskeywords.autoreplyonexpiry','status.statusname'),'coltablename'=>array('smskeywords','smsprovidersettings','smskeywords','smskeywords','smskeywords','smskeywords','smskeywords','smssettings','smskeywords','status'),'uitype'=>array('2','17','2','2','2','2','2','19','2','2'),'colname'=>array("Keywords Name", "Settings Name", "Description", "Auto Reply Message", "Forward To", "Forward Mail", "Keyword Validity", "Sender Id", "Auto Reply on Expiry", "Status"),'colsize'=>array('200','200','200','200','200','200','200','200','200','200'));
		$result=$this->Smssettingsmodel->smskeywordgriddatafetchgridxmlview($primarytable,$sortcol,$sortord,$pagenum,$rowscount);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($result,$page,'');
		$device = $this->Basefunctions->deviceinfo();
	//	if($device=='phone') {
		//	$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'SMS Keywords List',$width,$height);
		//} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'SMS Keywords List',$width,$height);
		//}
		echo json_encode($datas);
	}
	//sms keyword data fetch
	public function smskeyworddatafetch() {
		$this->Smssettingsmodel->smskeyworddatafetchmodel();
	}
	//sms keyword delete
	public function smskeyworddelete() {
		$this->Smssettingsmodel->smskeyworddeletemodel();
	}
	//senderid dd value fetch based on the settings name
	public function smsssenderddreload() {
		$this->Smssettingsmodel->smsssenderddreloadmodel();
	}
	//default sender id get
	public function defaultsettingsenderidget() {
		$this->Smssettingsmodel->defaultsettingsenderidgetmodel();
	}
	//sender id drop down data fetch based on the settings name
	public function senderdddatafetch() {
		$this->Smssettingsmodel->senderdddatafetchmodel();
	}
	//account credit detail fetch
	public function accountcresitdetailsfetch() {
		$this->Smssettingsmodel->accountcresitdetailsfetchmodel();
	}
	//sender id default set
	public function smsdefaultsenderidset() {
		$this->Smssettingsmodel->smsdefaultsenderidsetmodel();
	}
}