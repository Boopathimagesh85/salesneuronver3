<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'vendor/autoload.php';
//require 'aws.phar';
//require 'aws/aws-autoloader.php';

use Aws\S3\S3Client;
use Aws\Ses\SesClient;
use Aws\Exception\AwsException;

class Smssettingsmodel extends CI_Model {
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//sms settings credential add
	public function smscredentialaddmodel() {
		//master company id get
		$mastercompanyid = $this->Basefunctions->generalinformaion('company','mastercompanyid','companyid',2);
		$provider = $_POST['provider'];
		$settingsname = $_POST['settingsname'];
		$cusername = $_POST['username'];
		$cpassword = $_POST['password'];
		if(isset($_POST['smstype'])){
			$smstype = $_POST['smstype']; 
			if($smstype == ''){
				$smstype = 1;
			}
		} else {
			$smstype = 1;
		}
		if(isset($_POST['tranactionmode'])){
			$trasaction = $_POST['tranactionmode']; 
			if($trasaction == ''){
				$trasaction = 1;
			}
		} else {
			$trasaction = 1;
		}
		if(isset($_POST['virtualmobtype'])){
			$virtualmobtype = $_POST['virtualmobtype'];
			if($virtualmobtype == '' || $virtualmobtype == 'null'){
				$virtualmobtype = 1;
			}
		} else {
			$virtualmobtype = 1;
		}
		if(isset($_POST['longnumbertypeid'])){
			$longnumbertype = $_POST['longnumbertypeid'];
			if($longnumbertype == '' || $longnumbertype == 'null'){
				$longnumbertype = 1;
			}
		} else {
			$longnumbertype = 1;
		}
		$apikeys = $_POST['apikey'];
		$vmntype = $_POST['vmntype'];
		$longnumtype = $_POST['longnumype'];
		$date= date($this->Basefunctions->datef);
		$userid= $this->Basefunctions->userid;
		$tstatus = $this->Basefunctions->activestatus;
		$virtualmobile = $_POST['virtualmobile'];
		$smssettings=array( 
				'mastercompanyid'=>$mastercompanyid,
				'providerid'=>$provider,
				'smsprovidersettingsname'=>$settingsname,
				'username'=>$cusername,
				'password'=>$cpassword,
				'smsservicetypeid'=>$smstype,
				'smssendtypeid'=>$trasaction,
				'apikey'=>$apikeys,
				'virtualmobilenumbertypeid'=>$virtualmobtype,
				'longnumbertypeid'=>$longnumbertype,
				'virtualmobilenumber'=>$virtualmobile,
				'industryid'=>$this->Basefunctions->industryid,
				'branchid'=>$this->Basefunctions->branchid,
				'createdate'=>date($this->Basefunctions->datef),
				'lastupdatedate'=>date($this->Basefunctions->datef),
				'createuserid'=>$this->Basefunctions->userid,
				'lastupdateuserid'=>$this->Basefunctions->userid,
				'status'=>$this->Basefunctions->activestatus
			);
		$this->db->insert('smsprovidersettings',$smssettings);
		//file create for - long code with out keyword(general concept)
		if($longnumbertype == '3') {
			$contents = '';
			$ourFileName = $settingsname.".php";
			$ourFileHandle = fopen($ourFileName, 'w') or die("can't open file");
			$filename = 'longcodetriggerapi.txt';
			if(file_exists(realpath($filename))){
				$fcon = fopen(realpath($filename),'r');
				$contents = fread($fcon,filesize(realpath($filename)));
				fclose($fcon);
			} else {
			}
			$txt = "<?php $"."companyid =".$mastercompanyid.";";
			$newcontents = $txt.$contents;
			fwrite($ourFileHandle, $newcontents);
			fclose($ourFileHandle);
		}
		$char_set = 'UTF-8';
		$SesClient = new SesClient([
			'version'     => 'latest',
			'region'      => 'us-west-2',
			'credentials' => [
				'key'    => 'AKIAJBBGVK7CJSF34IZA',
				'secret' => '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',
			],
		]);
		if($longnumbertype == '3' || $longnumbertype == '2') {	//mail sent to arvind@salesneuron.com
			$username = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$this->Basefunctions->userid);
			$companyname = $this->getcompanyname();
			$datacontent = "<p>Dear Sir/Madam,</p>
				<p>This is automatic mail sent from&nbsp;<a href='http://salesneuron.com/' target='_blank'>salesneuron.com</a>&nbsp;regarding VMN - Long Code Number request for one of the customers.</p>
				<p>Below is the Long Code Number information:</p>";
			$datacontent .= "<p>Created By : ".$username." </p> </p><p>Company Name : ".$companyname." </p><p> Settings Name : ".$settingsname." </p><p> User Name : ".$cusername." </p><p> Password : ".$cpassword." </p><p> VMN Type : ".$vmntype." </p><p> Long Number Type : ".$longnumtype." </p><p> VMN Number : ".$virtualmobile." </p>
				<p>Please check the details and approve the VMN Number at the earliest. Reply to this mail with your status update.</p>
				<p>Looking forward for approval at the earliest. Please call on +91 72000 70822 for any verification/doubts clarifications.</p><br/><p>Regards,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
				<p><strong>Customer Success Team</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p><p>    Sales Neuron    <strong>AUC VENTURES PVT LTD</strong></p><p>No:6, Palani Andavar Koil Street,Villivakkam,Chennai-600049.&nbsp;   </p><p>Landline : 044-42639131&nbsp;| &nbsp;Mobile : +91 7200070822|&nbsp;<a href='http://www.aucventures.com/' target='_blank'>www.salesneuron.com</a></p>";
			$tomailid = array(array('email'=>'arvind@aucventures.com'),array('email'=>'kumaresan@aucventures.com','type' => 'cc'));
			try {
				$result = $SesClient->sendEmail([
					'Destination' => ['ToAddresses' => ['arvind@aucventures.com'],],
					'ReplyToAddresses' => ['arvind@aucventures.com'],
					'from_name' => ['Arvind'],
					'Source' => 'arvind@aucventures.com',
					'Message' => [
						'Body' => ['Html' => ['Charset' => $char_set,'Data' => $datacontent,],'Text' => ['Charset' => $char_set,'Data' =>  '',],],
						'Subject' => ['Charset' => $char_set,'Data' => 'Regards Credentials Details',],
						],
					'track_opens' => true,
					'track_clicks' => true,
					'important' => true,
					'url_strip_qs' => true,
					'inline_css' => true,
					'auto_text' => true,
					'auto_html' => true,
				]);
			} catch (AwsException $e) {
				$e->getMessage();
				$result['MessageId'] = $e->getAwsErrorMessage();
			}
		}	
		echo 'TRUE';
	}
	//sms provider settings grid view
	public function smscredentialgriddatafetchgridxmlview($tablename,$sortcol,$sortord,$pagenum,$rowscount) {
		$dataset = 'smsprovidersettings.smsprovidersettingsname,smsprovidersettings.smsprovidersettingsid,provider.providername,smsprovidersettings.username,password,status.statusname,smsservicetype.smsservicetypename,virtualmobilenumbertype.virtualmobilenumbertypename,smsprovidersettings.virtualmobilenumber,smsprovidersettings.apikey,smssendtype.smssendtypename,longnumbertype.longnumbertypename';
		$join =' LEFT OUTER JOIN status ON status.status=smsprovidersettings.status';
		$join .=' LEFT OUTER JOIN longnumbertype ON longnumbertype.longnumbertypeid=smsprovidersettings.longnumbertypeid';
		$join .=' LEFT OUTER JOIN smssendtype ON smssendtype.smssendtypeid=smsprovidersettings.smssendtypeid';
		$join .=' LEFT OUTER JOIN virtualmobilenumbertype ON virtualmobilenumbertype.virtualmobilenumbertypeid=smsprovidersettings.virtualmobilenumbertypeid';
		$join .=' LEFT OUTER JOIN smsservicetype ON smsservicetype.smsservicetypeid=smsprovidersettings.smsservicetypeid';
		$join .=' LEFT OUTER JOIN provider ON provider.providerid=smsprovidersettings.providerid';
		$status = $tablename.'.status IN (1)';
		$status .= ' AND '.$tablename.'.industryid IN ('.$this->Basefunctions->industryid.')';
		$where = '1=1';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		//query
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$where.' AND '.$status.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		return $data;
	}
	//sms credential edit data fetch
	public function smscredentialeditdatafetchmodel() {
		$primaryid = $_POST['datarowid'];
		$this->db->select('SQL_CALC_FOUND_ROWS  smsprovidersettings.smsprovidersettingsname,smsprovidersettings.smsprovidersettingsid,smsprovidersettings.providerid,smsprovidersettings.username,smsprovidersettings.password,smsservicetypeid,virtualmobilenumbertypeid,virtualmobilenumber,smsprovidersettings.apikey,smssendtype.smssendtypename',false);
		$this->db->from('smsprovidersettings');
		$this->db->join('smssendtype','smssendtype.smssendtypeid=smsprovidersettings.smssendtypeid');
		$this->db->where('smsprovidersettings.smsprovidersettingsid',$primaryid);
		$this->db->where_not_in('smsprovidersettings.status',array(3,0));
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data = array('settname'=>$row->smsprovidersettingsname,'primaryid'=>$row->smsprovidersettingsid,'provider'=>$row->providerid,'user'=>$row->username,'pass'=>$row->password,'smsservicetype'=>$row->smsservicetypeid,'vmtype'=>$row->virtualmobilenumbertypeid,'virtualmobilenumber'=>$row->virtualmobilenumber);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//sms credential update
	public function smscredentialupdatemodel(){
		$primaryid = $_POST['primaryid'];
		$provider = $_POST['provider'];
		$settingsname = $_POST['settingsname'];
		$username = $_POST['username'];
		$password = $_POST['password'];
		if(isset($_POST['smstype'])){
			$smstype = $_POST['smstype']; 
			if($smstype == ''){
				$smstype = 1;
			}
		} else {
			$smstype = 1;
		}
		if(isset($_POST['virtualmobtype'])){
			$virtualmobtype = $_POST['virtualmobtype'];
			if($virtualmobtype == '' || $virtualmobtype == 'null'){
				$virtualmobtype = 1;
			}
		} else {
			$virtualmobtype = 1;
		}
		$virtualmobile = $_POST['virtualmobile'];
		$smssettings=array( 
				'providerid'=>$provider,
				'smsprovidersettingsname'=>$settingsname,
				'username'=>$username,
				'password'=>$password,
				'smsservicetypeid'=>$smstype,
				'virtualmobilenumbertypeid'=>$virtualmobtype,
				'virtualmobilenumber'=>$virtualmobile,
				'lastupdatedate'=>date($this->Basefunctions->datef),
				'lastupdateuserid'=>$this->Basefunctions->userid,
				'status'=>$this->Basefunctions->activestatus
			);
		$this->db->where('smsprovidersettings.smsprovidersettingsid',$primaryid);
		$this->db->update('smsprovidersettings',$smssettings);
		echo 'TRUE';
	}
	//sms credential delete
	public function smscredentialdeletemodel() {
		$primaryid = $_POST['primaryid'];
		$smssettings=array( 
				'status'=>'0',
				'lastupdatedate'=>date($this->Basefunctions->datef),
				'lastupdateuserid'=>$this->Basefunctions->userid
			);
		$this->db->where('smsprovidersettings.smsprovidersettingsid',$primaryid);
		$this->db->update('smsprovidersettings',$smssettings);
		echo 'TRUE';
	}
	//sms sender id add
	public function smssenderidaddmodel() {
		$settingsid = $_POST['settingsid'];
		$senderid = strtoupper($this->input->post('senderid'));
		$setdefault = $_POST['setdefault'];
		$filename = $_POST['filename'];
		$filesize = $_POST['filesize'];
		$filetype = $_POST['filetype'];
		$filepath = $_POST['filepath'];
		$empid = $this->Basefunctions->userid;
		$smssenderid =array( 
				'smsprovidersettingsid'=>$settingsid,
				'senderid'=>$senderid,
				'apikey'=>'',
				'setdefault'=>$setdefault,
				'filename_fromid'=>'1',
				'filename'=>$filename,
				'filename_size'=>$filesize,
				'filename_type'=>$filetype,
				'filename_path'=>$filepath,
				'industryid'=>$this->Basefunctions->industryid,
				'branchid'=>$this->Basefunctions->branchid,
				'createdate'=>date($this->Basefunctions->datef),
				'lastupdatedate'=>date($this->Basefunctions->datef),
				'createuserid'=>$this->Basefunctions->userid,
				'lastupdateuserid'=>$this->Basefunctions->userid,
				'status'=>$this->Basefunctions->activestatus
			);
		$this->db->insert('smssettings',$smssenderid);
		$sender = $this->db->insert_id();
		$this->setdefaultupdate($settingsid,$setdefault,$sender);
		$filecontent = $this->newfilecontentfetch($filepath);
		$encodefile = base64_encode($filecontent);
		$attachment = array(array('type'=>$filetype,'name'=>$filename,'content'=>$encodefile));
		$username = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$this->Basefunctions->userid);
		$companyname = $this->getcompanyname();
		$char_set = 'UTF-8';
		$SesClient = new SesClient([
			'version'     => 'latest',
			'region'      => 'us-west-2',
			'credentials' => [
				'key'    => 'AKIAJBBGVK7CJSF34IZA',
				'secret' => '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',
			],
		]);
		{//mail sent to arvind@salesneuron.com
			$datacontent = "<p>Dear Sir/Madam,</p>
				<p>This is automatic mail sent from&nbsp;<a href='http://salesneuron.com/' target='_blank'>salesneuron.com</a>&nbsp;regarding sender id request for one of the customers.</p>
				<p>Below is the Sender id information:</p>";
			$datacontent .= "<p>Created By : ".$username." </p> </p><p>Company Name : ".$companyname." </p><p> SENDER ID : ".$_POST['senderid']." </p>
				<p>Attached is the document uploaded by the client for approval purpose of sender id.</p>
				<p>Please check the details and approve the sender id at the earliest. Reply to this mail with your status update.</p>
				<p>Looking forward for approval at the earliest. Please call on +91 72000 70822 for any verification/doubts clarifications.</p><br/><p>Regards,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
				<p><strong>Customer Success Team</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p><p>    Sales Neuron    <strong>AUC VENTURES PVT LTD</strong></p><p>No:6, Palani Andavar Koil Street,Villivakkam,Chennai-600049.&nbsp;   </p><p>Landline : 044-42639131&nbsp;| &nbsp;Mobile : +91 7200070822|&nbsp;<a href='http://www.aucventures.com/' target='_blank'>www.salesneuron.com</a></p>";
			$tomailid = array(array('email'=>'arvind@aucventures.com'),array('email'=>'gowtham@aucventures.com','type' => 'cc'));
			try {
				$result = $SesClient->sendEmail([
					'Destination' => ['ToAddresses' => ['arvind@aucventures.com'],],
					'ReplyToAddresses' => ['arvind@aucventures.com'],
					'from_name' => ['Arvind'],
					'Source' => 'arvind@aucventures.com',
					'Message' => [
						'Body' => ['Html' => ['Charset' => $char_set,'Data' => $datacontent,],'Text' => ['Charset' => $char_set,'Data' =>  '',],],
						'Subject' => ['Charset' => $char_set,'Data' => 'Regards Sender ID Approval',],
						],
					'track_opens' => true,
					'track_clicks' => true,
					'important' => true,
					'url_strip_qs' => true,
					'inline_css' => true,
					'auto_text' => true,
					'auto_html' => true,
					'attachments' => $attachment,
				]);
			} catch (AwsException $e) {
				$e->getMessage();
				$result['MessageId'] = $e->getAwsErrorMessage();
			}
		}
		echo 'TRUE';
	}
	//read file
	public function newfilecontentfetch($filename) {
		$filecontent = "";
		if( file_exists($filename) && is_readable($filename) ) {
			$newfilename = explode('/',$filename);
			$myfile = @fopen('uploads'.DIRECTORY_SEPARATOR.$newfilename[1], "r");
			$filecontent = fread($myfile,filesize('uploads'.DIRECTORY_SEPARATOR.$newfilename[1]));
			fclose($myfile);
		}
		return $filecontent;
	}
	//mail notification sent
	public function mailsentfunction($message,$send_at) {
		try {
			$mandrill = new Mandrill('X9WnMXpxhC8YEOUPuQ3oZw');
			$async = true; //for bulk message
			$ip_pool = '';
			$result = $mandrill->messages->send($message, $async, $ip_pool, $send_at);
		} catch(Mandrill_Error $e) {
			// Mandrill errors are thrown as exceptions
			echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			throw $e;
		}
	}
	//sms provider settings grid view
	public function smssenderidgriddatafetchgridxmlview($tablename,$sortcol,$sortord,$pagenum,$rowscount) {
		$dataset = 'smssettings.smssettingsid,smsprovidersettings.smsprovidersettingsname,smssettings.apikey,smssettings.setdefault,smssettings.senderid,status.statusname';
		$join =' LEFT OUTER JOIN status ON status.status=smssettings.status';
		$join .=' LEFT OUTER JOIN smsprovidersettings ON smsprovidersettings.smsprovidersettingsid=smssettings.smsprovidersettingsid';
		$status = $tablename.'.status IN (1)';
		$status .= ' AND '.$tablename.'.industryid IN ('.$this->Basefunctions->industryid.')';
		$where = '1=1';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		//query
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$where.' AND '.$status.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		return $data;
	}
	//sms sender id data fetch
	public function smssenderiddatafetchmodel() {
		$primaryid = $_POST['datarowid'];
		$this->db->select('smssettings.smssettingsid,smssettings.smsprovidersettingsid,smssettings.apikey,smssettings.setdefault,smssettings.senderid');
		$this->db->from('smssettings');
		$this->db->where('smssettings.smssettingsid',$primaryid);
		$this->db->where_not_in('smssettings.status',array(3,0));
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data = array('prosettingsid'=>$row->smsprovidersettingsid,'apikey'=>$row->apikey,'senderid'=>$row->senderid,'setdefault'=>$row->setdefault);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//sms sender update 
	public function smssenderidupdatemodel() {
		$primaryid = $_POST['primaryid'];
		$settingsid = $_POST['settingsid'];
		$senderid = $_POST['senderid'];
		$setdefault = $_POST['setdefault'];
		$smssenderid =array( 
				'smsprovidersettingsid'=>$settingsid,
				'senderid'=>$senderid,
				'apikey'=>'',
				'setdefault'=>$setdefault,
				'lastupdatedate'=>date($this->Basefunctions->datef),
				'lastupdateuserid'=>$this->Basefunctions->userid,
				'status'=>$this->Basefunctions->activestatus
			);
		$this->db->where('smssettings.smssettingsid',$primaryid);
		$this->db->update('smssettings',$smssenderid);
		$this->setdefaultupdate($settingsid,$setdefault,$primaryid);
		echo 'TRUE';
	}
	//sms sender id delete
	public function smssenderiddeletemodel() {
		$primaryid = $_POST['primaryid'];
		$smssettings=array( 
				'status'=>'0',
				'lastupdatedate'=>date($this->Basefunctions->datef),
				'lastupdateuserid'=>$this->Basefunctions->userid
			);
		$this->db->where('smssettings.smssettingsid',$primaryid);
		$this->db->update('smssettings',$smssettings);
		echo 'TRUE';
	}
	//sms settings drop down reload
	public function smsstttingsnameddreloadmodel() {
		$i=0;
		$smstype = $_GET['smstype'];
		$this->db->select('smsprovidersettingsname,smsprovidersettingsid');
		$this->db->from('smsprovidersettings');
		if($smstype != '') {
			$this->db->where('smsprovidersettings.smsservicetypeid',$smstype);
		}
		$this->db->where('smsprovidersettings.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->smsprovidersettingsid,'dataname'=>$row->smsprovidersettingsname);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//sms keyword add
	public function smskeywordaddmodel() {
		$settingsid = $_POST['settingsid'];
		$keywordname = $_POST['keywordname'];
		$description = $_POST['description'];
		$autoreplymessage = $_POST['autoreplymessage'];
		$forwardto = $_POST['forwardto'];
		$forwardemail = $_POST['forwardemail'];
		$keywordvalidity = $_POST['keywordvalidity'];
		if(isset($_POST['smssettingsid'])){
			$smssettingsid = $_POST['smssettingsid']; 
			if($smssettingsid == ''){
				$smssettingsid = 1;
			}
		} else {
			$smssettingsid = 1;
		}
		$date = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$autoreplyonexpiry = $_POST['autoreplyonexpiry'];
		$kewordstatus = '';
		$tstatus = $this->Basefunctions->activestatus;
		$smskeywords =array( 
				'smsprovidersettingsid'=>$settingsid,
				'smskeywordsname'=>$keywordname,
				'description'=>$description,
				'autoreplymessage'=>$autoreplymessage,
				'forwardto'=>$forwardto,
				'forwardemail'=>$forwardemail,
				'keywordvalidity'=>$this->Basefunctions->ymddateconversion($keywordvalidity),'smssettingsid'=>$smssettingsid,
				'autoreplyonexpiry'=>$autoreplyonexpiry,
				'kewordstatus'=>'',
				'industryid'=>$this->Basefunctions->industryid,
				'branchid'=>$this->Basefunctions->branchid,
				'createdate'=>date($this->Basefunctions->datef),
				'lastupdatedate'=>date($this->Basefunctions->datef),
				'createuserid'=>$this->Basefunctions->userid,
				'lastupdateuserid'=>$this->Basefunctions->userid,
				'status'=>$this->Basefunctions->activestatus
			);
		$this->db->insert('smskeywords',$smskeywords);
		//master company id get
		$mastercompanyid = $this->Basefunctions->generalinformaion('company','mastercompanyid','companyid',2);
		$username = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$this->Basefunctions->userid);
		$companyname = $this->getcompanyname();
		{//mail sent to arvind@salesneuron.com
			$datacontent = "<p>Dear Sir/Madam,</p>
				<p>This is automatic mail sent from&nbsp;<a href='http://salesneuron.com/' target='_blank'>salesneuron.com</a>&nbsp;regarding keyword request for one of the customers.</p>
				<p>Below is the Sender id information:</p>";
			$datacontent .= "<p>Created By : ".$username." </p> </p><p>Company Name : ".$companyname." </p><p> KeyWord : ".$keywordname." </p><p> Auto Reply Message : ".$autoreplymessage." </p><p> ForwardTo : ".$forwardto." </p><p> Forward Email : ".$forwardemail." </p><p> KeyWord Validity : ".$keywordvalidity." </p><p> Auto Reply on Expiry : ".$autoreplyonexpiry." </p>
				<p>Please check the details and approve the keyword at the earliest. Reply to this mail with your status update.</p>
				<p>Looking forward for approval at the earliest. Please call on +91 72000 70822 for any verification/doubts clarifications.</p><br/><p>Regards,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
				<p><strong>Customer Success Team</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p><p>    Sales Neuron    <strong>AUC VENTURES PVT LTD</strong></p><p>No:6, Palani Andavar Koil Street,Villivakkam,Chennai-600049.&nbsp;   </p><p>Landline : 044-42639131&nbsp;| &nbsp;Mobile : +91 7200070822|&nbsp;<a href='http://www.aucventures.com/' target='_blank'>www.salesneuron.com</a></p>";
			$tomailid = array(array('email'=>'arvind@aucventures.com'),array('email'=>'gowtham@aucventures.com','type' => 'cc'));
			$message = array(
					'html' => $datacontent,
					'text' => '',
					'subject' => 'Regards Keyword Details',
					'from_email' => 'arvind@salesneuron.com',
					'from_name' => 'Arvind',
					'to' => $tomailid,
					'track_opens' => true,
					'track_clicks' => true,
					'inline_css' => true,
					'url_strip_qs' => true,
					'important' => true,
					'auto_text' => true,
					'auto_html' => true,
				);
			$send_at ='';		
			$this->mailsentfunction($message,$send_at);
		}
		echo 'TRUE';
	}
	//retrieve getcompanyname
	public function getcompanyname() {
		$userid=$this->Basefunctions->userid;
		$data=$this->db->select('company.companyname')
		->from('company')
		->join('branch','branch.companyid=company.companyid')
		->join('employee','employee.branchid=branch.branchid')
		->where('employee.employeeid',$userid)
		->limit(1)
		->get();
		foreach($data->result() as $info) {
			$companyname = $info->companyname;
		}
		return $companyname;
	}
	//sms grid data fetch
	public function smskeywordgriddatafetchgridxmlview($tablename,$sortcol,$sortord,$pagenum,$rowscount) {
		$dateformat = $this->Basefunctions->phpmysqlappdateformatfetch();
		$mysqlformat = $dateformat['mysqlformat'];
		$dataset = 'smskeywords.smskeywordsid,smskeywords.smskeywordsname,smsprovidersettings.smsprovidersettingsname,smskeywords.description,autoreplymessage,forwardto,forwardemail,DATE_FORMAT(keywordvalidity,"'.$mysqlformat.'") AS keywordvalidity,smssettings.senderid,autoreplyonexpiry,status.statusname';
		$join =' LEFT OUTER JOIN status ON status.status=smskeywords.status';
		$join .=' LEFT OUTER JOIN smsprovidersettings ON smsprovidersettings.smsprovidersettingsid=smskeywords.smsprovidersettingsid';
		$join .=' LEFT OUTER JOIN smssettings ON smssettings.smssettingsid=smskeywords.smssettingsid';
		$status = $tablename.'.status IN (1)';
		$status .= ' AND '.$tablename.'.industryid IN ('.$this->Basefunctions->industryid.')';
		$where = '1=1';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		//query
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$where.' AND '.$status.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		return $data;
	}
	//sms keyword edit data fetch
	public function smskeyworddatafetchmodel() {
		$id = $_POST['datarowid'];
		$this->db->select('smskeywords.smskeywordsid,smskeywords.smskeywordsname,smskeywords.smsprovidersettingsid,smskeywords.description,autoreplymessage,forwardto,forwardemail,keywordvalidity,smskeywords.smssettingsid,autoreplyonexpiry');
		$this->db->from('smskeywords');
		$this->db->where('smskeywords.smskeywordsid',$id);
		$this->db->where_not_in('smskeywords.status',array(3,0));
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data = array('datarowid'=>$row->smskeywordsid,'keywordname'=>$row->smskeywordsname,'settingsid'=>$row->smsprovidersettingsid,'description'=>$row->description,'autoreplymessage'=>$row->autoreplymessage,'forwardto'=>$row->forwardto,'forwardemail'=>$row->forwardemail,'keywordvalidity'=>$row->keywordvalidity,'smssettingsid'=>$row->smssettingsid,'autoreplyonexpiry'=>$row->autoreplyonexpiry);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//sms keyword update
	public function smskeyworupdatemodel() {
		$id = $_POST['id'];
		$settingsid = $_POST['settingsid'];
		$keywordname = $_POST['keywordname'];
		$description = $_POST['description'];
		$autoreplymessage = $_POST['autoreplymessage'];
		$forwardto = $_POST['forwardto'];
		$forwardemail = $_POST['forwardemail'];
		$keywordvalidity = $_POST['keywordvalidity'];
		$smssettingsid = $_POST['smssettingsid'];
		$autoreplyonexpiry = $_POST['autoreplyonexpiry'];
		$smskeywords =array( 
				'smsprovidersettingsid'=>$settingsid,
				'smskeywordsname'=>$keywordname,
				'description'=>$description,
				'autoreplymessage'=>$autoreplymessage,
				'forwardto'=>$forwardto,
				'forwardemail'=>$forwardemail,
				'keywordvalidity'=>$keywordvalidity,
				'smssettingsid'=>$smssettingsid,
				'autoreplyonexpiry'=>$autoreplyonexpiry,
				'kewordstatus'=>'',
				'createdate'=>date($this->Basefunctions->datef),
				'lastupdatedate'=>date($this->Basefunctions->datef),
				'createuserid'=>$this->Basefunctions->userid,
				'lastupdateuserid'=>$this->Basefunctions->userid,
				'status'=>$this->Basefunctions->activestatus
			);
		$this->db->where('smskeywords.smskeywordsid',$id);
		$this->db->update('smskeywords',$smskeywords);
		echo 'TRUE';
	}
	//sms keyword delete
	public function smskeyworddeletemodel() {
		$primaryid = $_POST['primaryid'];
		$smskeywords=array( 
				'status'=>0,
				'lastupdatedate'=>date($this->Basefunctions->datef),
				'lastupdateuserid'=>$this->Basefunctions->userid
			);
		$this->db->where('smskeywords.smskeywordsid',$primaryid);
		$this->db->update('smskeywords',$smskeywords);
		echo 'TRUE';
	}
	//sender dd value fetch based on the settings name
	public function smsssenderddreloadmodel() {
		$i=0;
		$settnameid = $_GET['settnameid'];
		$this->db->select('smssettingsid,senderid,apikey');
		$this->db->from('smssettings');
		$this->db->where('smssettings.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->smssettingsid,'dataname'=>$row->senderid,'apikey'=>$row->apikey);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//default sender id and setting name get
	public function defaultsettingsenderidgetmodel() {
		$this->db->select('smsprovidersettingsid,smssettingsid');
		$this->db->from('smssettings');
		$this->db->where('smssettings.setdefault','Yes');
		$this->db->where('smssettings.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data = array('settingsid'=>$row->smsprovidersettingsid,'senderid'=>$row->smssettingsid);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//sender id drop down data fetch based on the settings name
	public function senderdddatafetchmodel() {
		$settingsid = $_POST['setnameid'];
		$i=0;
		$userid = $this->Basefunctions->userid;
		$this->db->select('smssettingsid,senderid,apikey');
		$this->db->from('smssettings');
		$this->db->where('smssettings.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data[$i] = array('datasid'=>$row->smssettingsid,'dataname'=>$row->senderid,'apikey'=>$row->apikey);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//account credit details fetch function 
	public function accountcresitdetailsfetchmodel() {
		$addontype = $_POST['addontype'];
		$this->db->select('addonavailablecredit');
		$this->db->from('salesneuronaddonscredit');
		$this->db->where('salesneuronaddonscredit.addonstypeid',$addontype);
		$this->db->where('salesneuronaddonscredit.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$credit = $row->addonavailablecredit;
			}
		} else {
			$credit = '0';
		}
		echo json_encode($credit);
	}
	public function accountcresitdetailsfetchmodel_local() {
		$addontype = $_POST['addontype'];
		$this->db->select('addonavailablecredit');
		$this->db->from('salesneuronaddonscredit');
		$this->db->where('salesneuronaddonscredit.addonstypeid',$addontype);
		$this->db->where('salesneuronaddonscredit.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$credit = $row->addonavailablecredit;
			}
		} else {
			$credit = '0';
		}
		echo json_encode($credit);
	}
	//default sender id set
	public function smsdefaultsenderidsetmodel() {
		$setnameid = $_POST['settingsnameid'];
		$defsenderid = $_POST['defsenderid'];
		$defset = array(
			'setdefault'=>'Yes',
			'lastupdatedate'=>date($this->Basefunctions->datef),
			'lastupdateuserid'=>$this->Basefunctions->userid,
			'status'=>$this->Basefunctions->activestatus
		);
		$this->db->where('smssettings.smssettingsid',$defsenderid);
		$this->db->where('smssettings.smsprovidersettingsid',$setnameid);
		$this->db->update('smssettings',$defset);
		
		//change another default to No
		$defunset = array(
			'setdefault'=>'No',
			'lastupdatedate'=>date($this->Basefunctions->datef),
			'lastupdateuserid'=>$this->Basefunctions->userid,
			'status'=>$this->Basefunctions->activestatus
		);
		$this->db->where_not_in('smssettings.smssettingsid',array($defsenderid));
		$this->db->where_in('smssettings.status',array(1));
		$this->db->update('smssettings',$defunset);
		
		echo 'TRUE';
	}
	//set default update
	public function setdefaultupdate($settingsid,$setdefault,$sender) {
		if($setdefault != 'No') {
			$updatearray = array('setdefault'=>'No');
			$this->db->where_not_in('smssettings.smssettingsid',$sender);
			$this->db->update('smssettings',$updatearray);
		}
	}
}