<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('Base/headerfiles'); ?>
</head>
<body>
<?php
		$dataset['gridenable'] = "1"; //0-no  1-yes
		$dataset['griddisplayid'] = "smssettingsaddformview"; //add form-div id
		$dataset['maingridtitle'] = $gridtitle['title'];   // form header
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];  //grid header
		$dataset['spanattr'] = array();     //
		$dataset['gridtableid'] = "smssettingsaddgrid"; //grid id
		$dataset['griddivid'] = "smssettingsgridnav"; //grid pagination
		$this->load->view('smssettingsform',$dataset); 
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$this->load->view('Base/overlaymobile');
		} else {
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
		$this->load->view('Base/basedeleteform');
?>	
</body>
	<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js Files -->
	<script src="<?php echo base_url();?>js/Smssettings/smssettings.js" type="text/javascript"></script> 
	<script>
		$("#tab1").click(function()	{resetFields(); mastertabid=1; masterfortouch=0; });
		$("#tab2").click(function()	{mastertabid=2;	masterfortouch=0;});		
	</script>
</html>

<style type="text/css">
#smssettingscreaddgrid1width {
	height:800px !important;
}

#smssettingssenaddgrid2width {
	height:800px !important;
}

#smssettingscreaddgrid3width {
	height:800px !important;
}
#smssettingscreaddgrid1 {
	height:760px !important;
}
#smssettingscreaddgrid2 {
	height:760px !important;
}
#smssettingscreaddgrid3 {
	height:760px !important;
}
.rppdropdown  {
	left:50% !important;
}
</style>
