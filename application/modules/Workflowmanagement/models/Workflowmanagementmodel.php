<?php
Class Workflowmanagementmodel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->model('Base/Crudmodel');
		//$this->load->library('csvreader');
    }
	//rule criteria condition field name
	public function rulecondfiledsdropdownmodel() {
		$mid = $_GET['ids'];
		$i=0;
		if($mid!='') {
			$this->db->select('modulefield.modulefieldid,modulefield.fieldlabel,modulefield.uitypeid');
			$this->db->from('modulefield');
			$this->db->join('uitype','uitype.uitypeid=modulefield.uitypeid');
			$this->db->where('modulefield.moduletabid',$mid);
			$this->db->where_not_in('modulefield.uitypeid',array(14,15,16,22,23,24));
			$this->db->where('modulefield.status',1);
			$datasets = $this->db->get();
			if($datasets->num_rows()>0) {
				foreach($datasets->result() as $row) {
					$data[$i]=array('dataids'=>$row->modulefieldid,'label'=>$row->fieldlabel,'uitype'=>$row->uitypeid);
					$i++;
				}
				echo json_encode($data);
			} else {
				echo json_encode(array("fail"=>'FAILED'));
			}
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//data field name list
	public function fielddropdownloadmodel() {
		$mid = $_GET['ids'];
		$type = $_GET['type'];
		$i=0;
		if($mid!='' && $type!='') {
			$this->db->select('modulefield.modulefieldid,modulefield.fieldlabel,modulefield.uitypeid');
			$this->db->from('modulefield');
			$this->db->join('uitype','uitype.uitypeid=modulefield.uitypeid');
			$this->db->where('modulefield.moduletabid',$mid);
			if($type=='2') {
				$this->db->where_in('modulefield.uitypeid',array(8));
			}
			$this->db->where_not_in('modulefield.uitypeid',array(14,15,16,22,23,24));
			$this->db->where('modulefield.status',1);
			$datasets = $this->db->get();
			if($datasets->num_rows()>0) {
				foreach($datasets->result() as $row) {
					$data[$i]=array('dataids'=>$row->modulefieldid,'label'=>$row->fieldlabel,'uitype'=>$row->uitypeid);
					$i++;
				}
				echo json_encode($data);
			} else {
				echo json_encode(array("fail"=>'FAILEDd'));
			}
		} else {
			echo json_encode(array("fail"=>'FAILEDd'));
		}
	}
	//Data Rule field name fetch
	public function ruleactionfieldnamefetchmodel() {
		$mid = $_GET['ids'];
		$mid = $this->relatedmoduleidfetch($mid);
		$type = $_GET['type'];
		$i=0;
		if($mid!='' && $type!='') {
			$this->db->select('modulefield.modulefieldid,modulefield.fieldlabel,modulefield.uitypeid,modulefield.moduletabid,module.modulename',true);
			$this->db->from('modulefield');
			$this->db->join('uitype','uitype.uitypeid=modulefield.uitypeid');
			$this->db->join('module','module.moduleid=modulefield.moduletabid');
			if($type=='sms' || $type=='call') {
				$this->db->where_in('modulefield.uitypeid',array(11));
			} else if($type=='email') {
				$this->db->where_in('modulefield.uitypeid',array(10));
			} else if($type=='desktop') {
				$this->db->where_in('modulefield.uitypeid',array(20));
			}
			$this->db->where_in('modulefield.moduletabid',$mid);
			$this->db->where('modulefield.status',1);
			$this->db->where_in('uitype.status',array(1,3));
			$datasets = $this->db->get();
			if($datasets->num_rows()>0) {
				foreach($datasets->result() as $row) {
					$data[$i]=array('dataids'=>$row->modulefieldid,'label'=>$row->fieldlabel,'uitype'=>$row->uitypeid,'modname'=>$row->modulename,'modid'=>$row->moduletabid);
					$i++;
				}
				echo json_encode($data);
			} else {
				echo json_encode(array("fail"=>'FAILED'));
			}
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	// load related module
	public function relatedmoduleidfetch($moduleid) {
		$moddatas = array();
		$relmodids = array();
		$relmoduleids = $this->db->select('relatedmoduleid')->from('modulerelation')->where('moduleid',$moduleid)->get();
		foreach($relmoduleids->result() as $reldata) {
			$relmodids[] = $reldata->relatedmoduleid;
		}
		$relmodids[] = 4;
		return array_unique($relmodids);
	}
	
	//Rule alert email templates fetch
	public function emailtemplateinformationfetchmodel() {
		$modid = $_GET['ids'];
		$i=0;
		$data=array();
		$this->db->select('emailtemplatesid,emailtemplatesname,emaillisttypeid');
		$this->db->from('emailtemplates');
		$this->db->where('emaillisttypeid',3);
		$this->db->where('moduleid',$modid);
		$this->db->where('status',1);
		$datas = $this->db->get();
		if($datas->num_rows()>0) {
			foreach($datas->result() as $row) {
				$data[$i]=array('dataids'=>$row->emailtemplatesid,'datanames'=>$row->emailtemplatesname);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//Rule alert email templates fetch
	public function desktoptemplateinformationfetchmodel() {
		$modid = $_GET['ids'];
		$i=0;
		$data=array();
		$this->db->select('templatesid,templatesname');
		$this->db->from('templates');
		$this->db->where_in('templates.templatetypeid',array(4));
		$this->db->where('templates.moduleid',$modid);
		$this->db->where('status',1);
		$datas = $this->db->get();
		if($datas->num_rows()>0) {
			foreach($datas->result() as $row) {
				$data[$i]=array('dataids'=>$row->templatesid,'datanames'=>$row->templatesname);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//condition fieldname picklist values fetch
	public function modulefiledpicklistvalfetchmodel() {
		$fieldid = $_GET['fieldid'];
		$modid = $_GET['moduleid'];
		$uitypeid = $_GET['uitype'];
		$datas = $this->db->select('modulefieldid,columnname,ddparenttable,defaultvalue')->from('modulefield')->where('modulefield.modulefieldid',$fieldid)->where('modulefield.uitypeid',$uitypeid)->where('status',1)->get();
		if($datas->num_rows()>0) {
			foreach($datas->result() as $data) {
				$tablename = substr($data->columnname,0,-2);
				$tabid = $tablename.'id';
				$tabfieldname = $tablename.'name';
				$conddata = $data->ddparenttable;
				$defvalue = $data->defaultvalue;
				if($uitypeid=='17') {
					echo $ddowndata = $this->dynamicdropdownvalfetch($modid,$tablename,$tabfieldname,$tabid);
				} else if($uitypeid=='18') {
					echo $ddowndata = $this->dynamicgroupdropdownvalfetch($modid,$tablename,$conddata,$tabfieldname);
				} else if($uitypeid=='19') {
					echo $ddowndata = $this->dynamicdropdownvalfetchparent($tablename,$tabfieldname,$tabid,$defvalue);
				} else if($uitypeid=='25') {
					echo $ddowndata = $this->dynamicdropdownvalfetchparent($tablename,$tabfieldname,$tabid,$defvalue);
				} else if($uitypeid=='20') {
					echo $ddowndata = $this->userspecificgroupdropdownvalfetch();
				} else if($uitypeid=='23') {
					$dropdowndata = array();
					$attrdata = $this->attributeviewdataddfetch($modid);
					$i=0;
					foreach($attrdata as $key) {
						$attributeid = $key['Id'];
						$ddowndata = $this->dynamicattributedropdownvalfetch($attributeid);
						foreach($ddowndata as $data):
							$dropdowndata[$i]=array('datasid'=>$data->Id,'dataname'=>$data->Name);
							$i++;
						endforeach;
					}
					if(!empty($dropdowndata)) {
						echo json_encode($dropdowndata);
					} else {
						echo json_encode(array("fail"=>'FAILED'));
					}
				} else if($uitypeid=='26') {
					echo $ddowndata = $this->dynamicdropdownvalfetchwithcond($tablename,$tabfieldname,$tabid,$conddata);
				} else if($uitypeid=='27') {
					echo $ddowndata = $this->dynamicmoduledropdownvalfetch($modid);
				} else if($uitypeid=='28') {
					$conddatas = explode('|',$conddata);
					echo $ddowndata = $this->dynamicdropdownvalfetchwithusercond($conddatas[0],$conddatas[1],$conddatas[2],$conddatas[3],$conddatas[4]);
				} else if($uitypeid=='29') {
					echo $ddowndata = $this->parentdynamicgroupdropdownvalfetch($modid,$tablename,$conddata,$tabfieldname);
				} else {
					echo json_encode(array("fail"=>'FAILED'));
				}
			}
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//new work flow create model
	public function newworkflowcreatemodel() {
		$modid = $_POST['newrulemoduleid'];
		$rulename = $_POST['newrulename'];
		$descp = $_POST['ruledescription'];
		$trigtype = $_POST['recordtriggertype'];
		$active = $_POST['ruleactive'];
		$userid = $this->Basefunctions->userid;
		$cdate = date($this->Basefunctions->datef);
		$status = $active=='Yes'?1:2;
		//work flow main data sets
		$newdata = array(
			'moduleid'=>$modid,
			'workflowname'=>$rulename,
			'description'=>$descp,
			'workflowtriggertypeid'=>$trigtype,
			'workflowactive'=>$active,
			'industryid'=>$this->Basefunctions->industryid,
			'branchid'=>$this->Basefunctions->branchid,
			'createuserid'=>$userid,
			'lastupdateuserid'=>$userid,
			'createdate'=>$cdate,
			'lastupdatedate'=>$cdate,
			'status'=>$status
		);
		$this->db->insert('workflow',$newdata);
		$workflowid = $this->db->insert_id();
		//Rule trigger
		if($trigtype=='1') {
			$frequency = $_POST['recordacttrigtypeid'];
			if($frequency == '4') {
				$fields = $_POST['rulesdatafields'];
				$fieldids = implode(',',$fields);
			} else {
				$fieldids = '1';
			}
			$execfreq = $_POST['fieldvalchktypeid'];
			$executiontype = "";
			$exectime = "";
			$trg_repeatevery = '';
			$trg_executiononday = '';
		} else if($trigtype=='2') {
			$fieldids = $_POST['datefieldids'];
			$frequency = $_POST['datefldfrequencytype'];
			$executiontype = $_POST['datefldexecutiontype'];
			$execfreq = $_POST['afterbeforetime'];
			$exectime = $_POST['datefldexectiontime'];
			$trg_repeatevery = $_POST['repeateveryday'];
			if($frequency == 'Weekly') {
				$trg_executiononday = implode(',',array_filter($_POST['recurrencedayid']));
			} else if($frequency == 'Monthly') {
				$trg_executiononday = $_POST['recurrencemonthdayid'];
			} else {
				$trg_executiononday = '';
			}
		}
		$ruledata = array(
			'workflowid'=>$workflowid,
			'triggerfieldid'=>$fieldids,
			'triggerfrequencytype'=>$frequency,
			'triggerrepeatevery'=>$trg_repeatevery,
			'triggerexecutiononday'=>$trg_executiononday,
			'triggerexecutiontype'=>$executiontype,
			'triggerexecutionfreqency'=>$execfreq,
			'triggerexecutiontime'=>$exectime,
			'createuserid'=>$userid,
			'lastupdateuserid'=>$userid,
			'createdate'=>$cdate,
			'lastupdatedate'=>$cdate,
			'status'=>1
		);
		$this->db->insert('workflowtrigger',$ruledata);
		//Rule criteria
		$girddata = $_POST['critgriddata'];
		$cricount = $_POST['count'];
		$girddatainfo = json_decode($girddata,true);
		for($i=0;$i<=($cricount-1);$i++) {
			$conddatas = array(
				'workflowid'=>$workflowid,
				'modulefieldid'=>$girddatainfo[$i]['rulecrtfieldnameid'],
				'conditionname'=>$girddatainfo[$i]['rulecrtfieldcond'],
				'conditionvalue'=>$girddatainfo[$i]['rulecritcondval'],
				'conditionandorvalue'=>$girddatainfo[$i]['rulecrtandorcond'],
				'conditionuitypeid'=>$girddatainfo[$i]['rulecrituitypeid'],
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
				'createdate'=>$cdate,
				'lastupdatedate'=>$cdate,
				'status'=>1				
			);
			$this->db->insert('workflowtriggercondition',$conddatas);
		}
		{//Rule alert create
			$alertsave = $_POST['alertsave'];
			if($alertsave=='Yes') {
				$alerttype = $_POST['rulealerttype'];
				if($alerttype=='email') { // Email Work
					$finaladdrecipient = '';
					if(is_array($_POST['rulealertadduserrecep'])) {
						$checkrulealertadduserrecep = explode(',',implode(',',$_POST['rulealertadduserrecep']));
						$_POST['rulealertadduserrecep'] = array();
						foreach($checkrulealertadduserrecep as $recdata) {
							if($recdata != '') {
								array_push($_POST['rulealertadduserrecep'],$recdata);
							}
						}
						$subaddrecipient = implode(',',array_filter($_POST['rulealertadduserrecep']));
						$addrecipient = $this->retrieveallrecipientemailids($subaddrecipient);
						$_POST['rulealertadduserrecep'] = implode(',',array_filter($_POST['rulealertadduserrecep']));
					} else {
						$_POST['rulealertadduserrecep'] = '';
					}
					if($_POST['rulealertadduserrecep'] == '') {
						$finaladdrecipient = $_POST['rulealertaddemailrecp'];
					} else {
						if($_POST['rulealertaddemailrecp'] == '' || $_POST['rulealertaddemailrecp'] == 1) { // Nothing will happen
							$finaladdrecipient = $_POST['rulealertaddemailrecp'];
						} else {
							$finaladdrecipient = array();
							$newdata = explode(',',$_POST['rulealertaddemailrecp']);
							foreach($newdata as $ndata) {
								if(!in_array($ndata, $addrecipient, true)) {
									array_push($finaladdrecipient,$ndata);
								}
							}
							$finaladdrecipient = implode(',',$finaladdrecipient);
						}
					}
					$alertdata = array(
						'workflowid'=>$workflowid,
						'workflowalerttype'=>$alerttype,
						'workflowalertname'=>$_POST['rulealertname'],
						'workflowtransactionmode'=>1,
						'workflowtransactiontype'=>1,
						'workflowsenderid'=>1,
						'workflowtemplateid'=>$_POST['rulealertemailtempid'],
						'workflowalertrecipient'=>$_POST['rulealertemailrecipient'],
						'workflowalertadditonalrecipient'=>$_POST['rulealertadduserrecep'],
						'workflowmanualalertadditonalrecipient'=>$finaladdrecipient,
						'workflowcallsoundid'=>1,
						'createuserid'=>$userid,
						'lastupdateuserid'=>$userid,
						'createdate'=>$cdate,
						'lastupdatedate'=>$cdate,
						'status'=>1
					);
					$this->db->insert('workflowruleactionalert',$alertdata);
				} else if($alerttype=='sms') { // SMS Work
					$finaladdrecipient = '';
					if(is_array($_POST['rulealertadduserrecep'])) {
						$checkrulealertadduserrecep = explode(',',implode(',',$_POST['rulealertadduserrecep']));
						$_POST['rulealertadduserrecep'] = array();
						foreach($checkrulealertadduserrecep as $recdata) {
							if($recdata != '') {
								array_push($_POST['rulealertadduserrecep'],$recdata);
							}
						}
						$subaddrecipient = implode(',',array_filter($_POST['rulealertadduserrecep']));
						$addrecipient = $this->retrieveallrecipientmobilenos($subaddrecipient);
						$_POST['rulealertadduserrecep'] = implode(',',array_filter($_POST['rulealertadduserrecep']));
					} else {
						$_POST['rulealertadduserrecep'] = '';
					}
					if($_POST['rulealertadduserrecep'] == '') {
						$finaladdrecipient = $_POST['rulealertsmsaddrecep'];
					} else {
						if($_POST['rulealertsmsaddrecep'] == '' || $_POST['rulealertsmsaddrecep'] == 1) { // Nothing will happen
							$finaladdrecipient = $_POST['rulealertsmsaddrecep'];
						} else {
							$finaladdrecipient = array();
							$newdata = explode(',',$_POST['rulealertsmsaddrecep']);
							foreach($newdata as $ndata) {
								if(!in_array($ndata, $addrecipient, true)) {
									array_push($finaladdrecipient,$ndata);
								}
							}
							$finaladdrecipient = implode(',',$finaladdrecipient);
						}
					}
					$alertdata = array(
						'workflowid'=>$workflowid,
						'workflowalerttype'=>$alerttype,
						'workflowalertname'=>$_POST['rulealertname'],
						'workflowtransactionmode'=>$_POST['rulealertsmstransmode'],
						'workflowtransactiontype'=>$_POST['rulealertsmstypeid'],
						'workflowsenderid'=>$_POST['rulealertsmssendid'],
						'workflowtemplateid'=>$_POST['rulealertsmstemp'],
						'workflowalertrecipient'=>$_POST['rulealertsmsrecep'],
						'workflowalertadditonalrecipient'=>$_POST['rulealertadduserrecep'],
						'workflowmanualalertadditonalrecipient'=>$finaladdrecipient,
						'workflowcallsoundid'=>1,
						'createuserid'=>$userid,
						'lastupdateuserid'=>$userid,
						'createdate'=>$cdate,
						'lastupdatedate'=>$cdate,
						'status'=>1
					);
					$this->db->insert('workflowruleactionalert',$alertdata);
				} else if($alerttype=='call') {
					if($_POST['rulealertcallsoundfile'] == '') {
						$soundid = 1;
					} else {
						$soundid = $_POST['rulealertcallsoundfile'];
					}
					$alertdata = array(
						'workflowid'=>$workflowid,
						'workflowalerttype'=>$alerttype,
						'workflowalertname'=>$_POST['rulealertname'],
						'workflowtransactionmode'=>$_POST['rulealertcalltransmode'],
						'workflowtransactiontype'=>$_POST['rulealertcallnumbertype'],
						'workflowsenderid'=>$_POST['rulealertcallernumber'],
						'workflowtemplateid'=>'1',
						'workflowalertrecipient'=>$_POST['rulealertcallrecpnum'],
						'workflowalertadditonalrecipient'=>$_POST['rulealertcalladdrecpnum'],
						'workflowcallsoundid'=>$soundid,
						'createuserid'=>$userid,
						'lastupdateuserid'=>$userid,
						'createdate'=>$cdate,
						'lastupdatedate'=>$cdate,
						'status'=>1
					);
					$this->db->insert('workflowruleactionalert',$alertdata);
				} else if($alerttype=='desktop') {
					if(is_array($_POST['rulealertadduserrecep'])) {
						$checkrulealertadduserrecep = explode(',',implode(',',$_POST['rulealertadduserrecep']));
						$_POST['rulealertadduserrecep'] = array();
						foreach($checkrulealertadduserrecep as $recdata) {
							if($recdata != '') {
								array_push($_POST['rulealertadduserrecep'],$recdata);
							}
						}
						$_POST['rulealertadduserrecep'] = implode(',',array_filter($_POST['rulealertadduserrecep']));
					} else {
						$_POST['rulealertadduserrecep'] = '';
					}
					$alertdata = array(
						'workflowid'=>$workflowid,
						'workflowalerttype'=>$alerttype,
						'workflowtransactionmode'=>1,
						'workflowtransactiontype'=>1,
						'workflowsenderid'=>1,
						'workflowalertname'=>$_POST['rulealertname'],
						'workflowtemplateid'=>$_POST['rulealertusertemp'],
						'workflowalertrecipient'=>$_POST['rulealertuserrecep'],
						'workflowalertadditonalrecipient'=>$_POST['rulealertadduserrecep'],
						'workflowmanualalertadditonalrecipient'=>'',
						'workflowcallsoundid'=>1,
						'createuserid'=>$userid,
						'lastupdateuserid'=>$userid,
						'createdate'=>$cdate,
						'lastupdatedate'=>$cdate,
						'status'=>1
					);
					$this->db->insert('workflowruleactionalert',$alertdata);
				}
			}
		}
		{//Rule action assign task
			$tasksave = $_POST['tasksave'];
			if($tasksave=='Yes') {
				//$notifyassigne = $_POST['tasknotifyassignee']!= '' ? $_POST['tasknotifyassignee'] : 'No';
				if(isset($_POST['tasknotifyassignee'])) {
					$notifyassigne = $_POST['tasknotifyassignee'];
				} else {
					$notifyassigne = 'No';
				}
				$taskdata = array(
					'workflowid'=>$workflowid,
					'workflowruleactiontaskname'=>$_POST['assigntaskname'],
					'moduleid'=>$modid,
					'taskalertduedate'=>$_POST['assigntaskduedate'],
					'taskalerttype'=>$_POST['assigntasktimetype'],
					'taskalertdays'=>$_POST['assigntaskdays'],
					'taskstatusid'=>$_POST['taskstatusid'],
					'taskpriorityid'=>$_POST['taskpriorityid'],
					'taskassignedto'=>$_POST['assigntaskemployeeid'],
					'notifyassignee'=>$notifyassigne,
					'description'=>$_POST['taskdescription'],
					'createuserid'=>$userid,
					'lastupdateuserid'=>$userid,
					'createdate'=>$cdate,
					'lastupdatedate'=>$cdate,
					'status'=>1
				);
				$this->db->insert('workflowruleactiontask',$taskdata);
			}
		}
		{//Rule action field update
			$fldupdatedatastatus = $_POST['fieldupdatesave'];
			if($fldupdatedatastatus == 'Yes') {
				$fieldupdatedata = array(
					'workflowid'=>$workflowid,
					'moduleid'=>$modid,
					'workflowruleactionfieldname'=>$_POST['ruleupdatefieldsubject'],
					'modulefieldid'=>$_POST['ruleupdatefieldid'],
					'uitypeid'=>$_POST['ruleupdatefielduitype'],
					'value'=>$_POST['fieldupdatevalue'],
					'createuserid'=>$userid,
					'lastupdateuserid'=>$userid,
					'createdate'=>$cdate,
					'lastupdatedate'=>$cdate,
					'status'=>1
				);
				$this->db->insert('workflowruleactionfield',$fieldupdatedata);
			}
		}
		echo 'Success';
	}
	// Retrieve Emailids from users or roles or groups list
	public function retrieveallrecipientemailids($rulealertadduserrecep) {
		$dataarray = array();
		$dataexplode = explode(',',$rulealertadduserrecep);
		foreach($dataexplode as $result) {
			if($result != '') {
				$id = $result[0];
				if($id == 1) { // Employee or users
					$emailid =  $this->Basefunctions->singlefieldfetch('emailid','employeeid','employee',substr($result, 2));
					if($emailid != '') {
						if(!in_array($emailid, $dataarray, true)) {
							array_push($dataarray,$emailid);
						}
					}
				} else if($id == 2) { // Groups
					$groupid = substr($result, 2);
					if($groupid == 2) {
						$groupsemailid =  $this->retrievegroupsemailid($groupid);
						foreach($groupsemailid as $eid) {
							foreach($eid as $emailid) {
								if(!in_array($emailid, $dataarray, true)) {
									array_push($dataarray,$emailid);
								}
							}
						}
					}
				} else if($id == 3) { // Roles
					$roleid = substr($result, 2);
					if($roleid != 1) {
						$rolesemailid =  $this->retrievegroupsemailid($roleid);
						foreach($rolesemailid as $eid) {
							foreach($eid as $emailid) {
								if(!in_array($emailid, $dataarray, true)) {
									array_push($dataarray,$emailid);
								}
							}
						}
					}
				}  else if($id == 4) { // Role and Subordinates
					$rolesubid = substr($result, 2);
					if($rolesubid != 1) {
						$rolesubemailid =  $this->retrievegroupsemailid($rolesubid);
						foreach($rolesubemailid as $eid) {
							foreach($eid as $emailid) {
								if(!in_array($emailid, $dataarray, true)) {
									array_push($dataarray,$emailid);
								}
							}
						}
					}
				}
			}
		}
		return $dataarray;
	}
	// Retrieve Mobile Numbers from users or roles or groups list
	public function retrieveallrecipientmobilenos($rulealertadduserrecep) {
		$dataarray = array();
		$dataexplode = explode(',',$rulealertadduserrecep);
		foreach($dataexplode as $result) {
			if($result != '') {
				$id = $result[0];
				if($id == 1) { // Employee or users
					$mobileno =  $this->Basefunctions->singlefieldfetch('mobilenumber','employeeid','employee',substr($result, 2));
					if($mobileno != '') {
						if(!in_array($mobileno, $dataarray, true)) {
							array_push($dataarray,$mobileno);
						}
					}
				} else if($id == 2) { // Groups
					$groupid = substr($result, 2);
					if($groupid == 2) {
						$groupsmobileno =  $this->retrievegroupsmobileno($groupid);
						foreach($groupsmobileno as $eid) {
							foreach($eid as $mobileno) {
								if(!in_array($mobileno, $dataarray, true)) {
									array_push($dataarray,$mobileno);
								}
							}
						}
					}
				} else if($id == 3) { // Roles
					$roleid = substr($result, 2);
					if($roleid != 1) {
						$rolesmobileno =  $this->retrievegroupsmobileno($roleid);
						foreach($rolesmobileno as $eid) {
							foreach($eid as $mobileno) {
								if(!in_array($mobileno, $dataarray, true)) {
									array_push($dataarray,$mobileno);
								}
							}
						}
					}
				}  else if($id == 4) { // Role and Subordinates
					$rolesubid = substr($result, 2);
					if($rolesubid != 1) {
						$rolesubmobileno =  $this->retrievegroupsmobileno($rolesubid);
						foreach($rolesubmobileno as $eid) {
							foreach($eid as $mobileno) {
								if(!in_array($mobileno, $dataarray, true)) {
									array_push($dataarray,$mobileno);
								}
							}
						}
					}
				}
			}
		}
		return $dataarray;
	}
	// Retrieve multiple emailids
	public function retrievegroupsemailid($groupid) {
		$data = array();
		$this->db->select("emailid");
		$this->db->from("employee");
		$this->db->where_in("userroleid",$groupid);
		$this->db->where("employee.status",1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$data[] =array($row->emailid);
			}
		}
		return $data;
	}
	// Retrieve multiple mobileno
	public function retrievegroupsmobileno($groupid) {
		$data = array();
		$this->db->select("mobilenumber");
		$this->db->from("employee");
		$this->db->where_in("userroleid",$groupid);
		$this->db->where("employee.status",1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$data[] =array($row->mobilenumber);
			}
		}
		return $data;
	}
	//work flow active
	public function activateworkflowdatamodel() {
		$workflowid = $_GET['primaryid'];
		$data = array('workflowactive'=>'Yes','status'=>1);
		//update
		$this->db->where('workflowid',$workflowid);
		$this->db->update('workflow',$data);
		echo 'TRUE';
	}
	//work flow inactive
	public function inactivateworkflowdatamodel() {
		$workflowid = $_GET['primaryid'];
		$data = array('workflowactive'=>'No','status'=>2);
		//update
		$this->db->where('workflowid',$workflowid);
		$this->db->update('workflow',$data);
		echo 'TRUE';
	}
	//work flow condition header information fetch
	public function workflowconditioninformationfetchmodel($moduleid) {
		$fnames = array('rulecrtandorcond','rulecrtfieldname','rulecrtfieldnameid','rulecrtfieldcond','rulecritcondval','rulecrituitypeid','workflowcolstatus');
		$flabels = array('AND / OR','Field Name','Field Name Id','Condition','Value','Ui Type','Status');
		$colmodnames = array('rulecrtandorcond','rulecrtfieldname','rulecrtfieldnameid','rulecrtfieldcond','rulecritcondval','rulecrituitypeid','workflowcolstatus');
		$colindexnames = array('workflow','workflow','workflow','workflow','workflow','workflow','workflow');
		$uitypes = array('2','2','2','2','2','2','2');
		$viewtypes = array('1','1','0','1','1','0','0');
		$dduitypeids = array('17','18','19','20','23','25','26','27','28','29');
		$data = array();
		$i=0;
		$j=0;
		foreach($fnames as $fname) {
			if(in_array($uitypes[$j],$dduitypeids)) {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j].'name';
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]='0';
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]='2';
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			} else {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			}
			$j++;
		}
		return $data;
	}
	//check box value get
	public function checkboxvaluegetmodel(){
		$checkdata = 'Yes,No';
		$checkid = '1,2';
		$cdata = explode(',',$checkdata);
		$cid = explode(',',$checkid);
		for($i=0;$i<count($cdata);$i++){
			$data[$i] = array('datasid'=>$cid[$i],'dataname'=>$cdata[$i]);
			}
			echo json_encode($data);
	}
	//call number fetch based on number type
	public function callnumberfetchmodel() {
		$numbertype = $_GET['numbertype'];
		$dropdowndata=array();
		$this->db->select('callsettingsid,mobilenumber');
		$this->db->from('callsettings');
		$this->db->where('callnumbertypeid',$numbertype);
		$this->db->where('callservicetypeid',3);
		$this->db->where('status',1);
		$datas=$this->db->get();
		if($datas->num_rows()>0) {
			foreach($datas->result() as $data) {
				$dropdowndata[] = array('datasid'=>$data->callsettingsid,'dataname'=>$data->mobilenumber);
			}
			echo json_encode($dropdowndata);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//call alert template fetch
	public function calltemplatefetchmodel() {
		$transmode = $_GET['transactionmode'];
		$this->db->select('calltemplatesid,calltemplatesname,moduleid');
		$this->db->from('calltemplates');
		$this->db->where('smssendtypeid',$transmode);
		$this->db->where('status',1);
		$datas = $this->db->get();
		if($datas->num_rows() > 0) {
			foreach($datas->result() as $row) {
				$data[] = array('datasid'=>$row->calltemplatesid,'dataname'=>$row->calltemplatesname,'moduleid'=>$row->moduleid);
			}
		} else {
			$data = array("fail"=>'FAILED');
		}
		echo json_encode($data);
	}
	//dynamic drop down generation value fetch
	public function dynamicdropdownvalfetch($moduleid,$tablename,$tabfieldname,$fieldprimaryid) {
		$mnamechk = $this->Basefunctions->tablefieldnamecheck($tablename,'moduleid');
		$unamechk = $this->Basefunctions->tablefieldnamecheck($tablename,'userroleid');
		$sonamechk = $this->Basefunctions->tablefieldnamecheck($tablename,'sortorder');
		$userroleid = $this->Basefunctions->userroleid;
        $dropdowndata = array();
        $this->db->select(''.$fieldprimaryid.' AS Id'.','.''.$tabfieldname.' AS Name');
        $this->db->from($tablename);
		if($mnamechk == "true") {
			$this->db->where("FIND_IN_SET('".$moduleid."',moduleid) >", 0);
		}
		if($unamechk == "true") {
			$this->db->where("FIND_IN_SET('".$userroleid."',userroleid) >", 0);
		}
		$this->db->where(''.$tablename.'.status', 1);
		if($sonamechk=="true") {
			$this->db->order_by('sortorder','asc');
		}
        $datas=$this->db->get();
		if($datas->num_rows()>0) {
			$i=0;
			foreach($datas->result() as $data) {
				$dropdowndata[$i]=array('datasid'=>$data->Id,'dataname'=>$data->Name);
				$i++;
			}
			return json_encode($dropdowndata);
		} else {
			return json_encode(array("fail"=>'FAILED'));
		}
    }
	//dynamic drop down generation value fetch
    public function dynamicgroupdropdownvalfetch($moduleid,$tablename1,$tablename2,$tabfieldname) {
		$userroleid = $this->Basefunctions->userroleid;
		$dropdowndata=array();
		$fieldprimaryid = $tablename1.'id';
		$parentprimaryid = $tablename2.'id';
		$parentprimaryname = $tablename2.'name';
		$join = ''.$tablename2.'.'.$parentprimaryid.'='.$tablename1.'.'.$parentprimaryid.'';
        $this->db->select(''.$fieldprimaryid.' AS CId'.','.''.$tabfieldname.' AS CName ,'.$tablename2.'.'.$parentprimaryid.' AS PId'.','.''.$tablename2.'.'.$parentprimaryname.' AS PName');
        $this->db->from($tablename1);
		$this->db->join($tablename2,$join);
        $this->db->where("FIND_IN_SET('".$moduleid."',".$tablename1.".moduleid) >", 0);
        $this->db->where("FIND_IN_SET('".$userroleid."',".$tablename1.".userroleid) >", 0); 
		$this->db->where("FIND_IN_SET('".$moduleid."',".$tablename2.".moduleid) >", 0);
        $this->db->where("FIND_IN_SET('".$userroleid."',".$tablename2.".userroleid) >", 0);
		$this->db->where(''.$tablename1.'.status', 1);
		$this->db->order_by(''.$tablename1.'.'.$parentprimaryid.'','asc');
        $this->db->order_by(''.$tablename1.'.sortorder','asc');
        $datas=$this->db->get();
        if($datas->num_rows()>0) {
			$i=0;
			foreach($datas->result() as $data) {
				$dropdowndata[$i]=array('CId'=>$data->CId,'CName'=>$data->CName,'PId'=>$data->PId,'PName'=>$data->PName);
				$i++;
			}
			return json_encode($dropdowndata);
		} else {
			return json_encode(array("fail"=>'FAILED'));
		}
    }
	//dynamic drop down generation value fetch for parent
    public function dynamicdropdownvalfetchparent($tablename,$tabfieldname,$fieldprimaryid,$default) {
		$level = "";
		if( isset($default) && $default!='null' && $default!='') {
			$level = $default;
		}
		if( ($tablename=='lead' && $tabfieldname=='leadname') || ($tablename=='lead' && $tabfieldname=='lastname') || ($tablename=='contact' && $tabfieldname=='contactname') || ($tablename=='contact' && $tabfieldname=='lastname') ) {
			$this->db->select(''.$fieldprimaryid.' AS Id'.','.''.$tabfieldname.' AS Name,salutation.salutationname AS SName,lastname AS LName');
			$this->db->from($tablename);
			$this->db->join('salutation','salutation.salutationid='.$tablename.'.salutationid','left outer');
			if($level != "") {
				$this->db->where(''.$tablename.'.'.$tablename.'level <=',$default);
			}
			$this->db->where(''.$tablename.'.status',1);
			$datas=$this->db->get();
			if($datas->num_rows()>0) {
				$i=0;
				foreach($datas->result() as $data) {
					$sname = (($key->SName!='')? $key->SName.' ':'');
					$lname = (($key->LName!='')? ' '.$key->LName:'');
					$dropdowndata[$i]=array('datasid'=>$data->Id,'dataname'=>$data->$sname.$key->Name.$lname);
					$i++;
				}
				return json_encode($dropdowndata);
			} else {
				return json_encode(array("fail"=>'FAILED'));
			}
		} else {
			$this->db->select(''.$fieldprimaryid.' AS Id'.','.''.$tabfieldname.' AS Name');
			$this->db->from($tablename);
			if($level != "") {
				$this->db->where(''.$tablename.'.'.$tablename.'level <=',$default);
			}
			$this->db->where(''.$tablename.'.status',1);
			$datas=$this->db->get();
			if($datas->num_rows()>0) {
				$i=0;
				foreach($datas->result() as $data) {
					$dropdowndata[$i]=array('datasid'=>$data->Id,'dataname'=>$data->Name);
					$i++;
				}
				return json_encode($dropdowndata);
			} else {
				return json_encode(array("fail"=>'FAILED'));
			}
		}
    }
	//dynamic drop down generation value fetch
    public function userspecificgroupdropdownvalfetch() {
		$dropdowndata = array();
		$i=0;
		$tablename = array('employee','employeegroup','userrole','userrole');
		$groupname = array('Users','Groups','Roles','Role and Subordinates');
		$groupkey = array('U','G','R','SR');
		$m=0;
		$pid = 1;
		foreach($tablename as $key => $tabname) {
			$name = $tabname.'name';
			$id = $tabname.'id';
			if($tabname=='employee') {
				$this->db->select(''.$id.' AS Id'.','.''.$name.' AS Name,lastname AS LName,salutationname AS SName');
				$this->db->from($tabname);
				$this->db->join('salutation','salutation.salutationid='.$tabname.'.salutationid','left outer');
				$this->db->where(''.$tabname.'.status',1);
				$this->db->order_by(''.$tabname.'.'.$id.'', 'ASC');
				$result=$this->db->get()->result();
				foreach($result as $row) {
					$sname = (($row->SName!='')? $row->SName.' ':'');
					$lname = (($row->LName!='')? ' '.$row->LName:'');
					$dropdowndata[$i]=array('CId'=>$row->Id,'CName'=>$sname.$row->Name.$lname,'PId'=>$pid,'PName'=>$groupname[$m],'DDval'=>$groupkey[$m].'-'.$row->Id);
					$i++;
				}
			} else {
				$this->db->select(''.$id.' AS Id'.','.''.$name.' AS Name');
				$this->db->from($tabname);
				$this->db->where(''.$tabname.'.status', 1);
				$this->db->order_by(''.$tabname.'.'.$id.'', 'ASC');
				$result=$this->db->get()->result();
				foreach($result as $row) {
					$dropdowndata[$i]=array('CId'=>$row->Id,'CName'=>$row->Name,'PId'=>$pid,'PName'=>$groupname[$m],'DDval'=>$groupkey[$m].'-'.$row->Id);
					$i++;
				}
			}
			$m++;
			$pid++;
		}
		if(!empty($dropdowndata)) {
			return json_encode($dropdowndata);
		} else {
			return json_encode(array("fail"=>'FAILED'));
		}
    }
	//attribute set name fetch
	public function attributeviewdataddfetch($moduleid) {
		$data = array();
		$i=0;
		$this->db->select("attribute.attributename,attribute.attributeid");
		$this->db->from("attribute");
		$this->db->join("attributesetassign","attributesetassign.attributeid=attribute.attributeid");
		$this->db->join("attributeset","attributeset.attributesetid=attributesetassign.attributesetid");
		$this->db->join("moduleattributesetting","moduleattributesetting.attributesetid=attributeset.attributesetid");
		$this->db->where("moduleattributesetting.moduleid",$moduleid);
		$this->db->where("moduleattributesetting.status",1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result()as $row) {
				$data[$i]=array('Id'=>$row->attributeid,'Name'=>$row->attributename);
				$i++;
			}
		}
		return $data;
	}
	//attribute value fetch by using attribute name and id
	public function dynamicattributedropdownvalfetch($attributeid) {
        $this->db->select('attributevalueid AS Id,attributevaluename AS Name');
        $this->db->from('attributevalue');
        $this->db->join("attribute","attribute.attributeid=attributevalue.attributeid");
        $this->db->where("FIND_IN_SET('".$attributeid."',attributevalue.attributeid) >", 0);
		$this->db->where('attributevalue.status', 1);
        $query=$this->db->get();
        return $query->result();
	}
	//dynamic drop down generation value fetch with condition (Relation component)
    public function dynamicdropdownvalfetchwithcond($tablename,$tabfieldname,$fieldprimaryid,$conddata) {
		$condinfo = explode('|',$conddata);
		$whcond = "1=1";
		$join="";
		$wcond="";
		$partable =  $condinfo[0];
		$tabfieldname = $condinfo[1];
		$tabname = $condinfo[2];
		$uitypeid = $condinfo[3];
		$modid = $this->Basefunctions->generalinformaion('modulefield','moduletabid','parenttable',$partable);
		$moduleid = ( ($modid!=0)? $modid : 1 );
		//conditions datas
		$condfname = $condinfo[4];
		$condtablname = $condinfo[5];
		$conduitypeid = $condinfo[6];
		$condname = $condinfo[7];
		$condvale = $condinfo[8];
		$dropdowndata = array();
		if( $partable != '' && $tabfieldname != '' && $tabname != '' && $uitypeid != '' ) {
			if($uitypeid == 17 || $uitypeid == 18 || $uitypeid == 19 || $uitypeid == 20 || $uitypeid == 25 || $uitypeid == 26 || $uitypeid == 28 || $uitypeid == 29) {
				$ftablename = substr($tabfieldname,0,-2);
				$ftabfildname = $ftablename.'name';
				$ftabid = $ftablename.'id';
				$tabname = $ftablename;
				if($condfname!='' && $condname!='' && $condvale!='') {
					if($conduitypeid == 17 || $conduitypeid == 18 || $conduitypeid == 19 || $conduitypeid == 20 || $conduitypeid == 25 || $conduitypeid == 26) {
						$condfldtablname = substr($condfname,0,-2);
						$condfldtabid = $condfldtablname.'id';
						$condfldname = $condfldtablname.'name';
						if($ftablename!=$tabname) {
							$join = $join.' LEFT OUTER JOIN '.$tabname.' ON '.$tabname.'.'.$ftabid.'='.$ftablename.'.'.$ftabid;
						}
						if($condtablname!=$tabname) {
							$tabid = $tabname.'id';
							$join = $join.' LEFT OUTER JOIN '.$condtablname.' ON '.$condtablname.'.'.$tabid.'='.$tabname.'.'.$tabid;
							if($condfname!=$tabfieldname) {
								$join = $join.' LEFT OUTER JOIN '.$condfldtablname.' ON '.$condfldtablname.'.'.$condfldtabid.'='.$condtablname.'.'.$condfldtabid;
							}
							$conditionvalarray = array( 0 => array($condfldtablname,$condfldname,$condname,$condvale,'AND') );
							$wcond = $this->Basefunctions->whereclausegeneration($conditionvalarray);
						} else {
							if($condfname!=$tabfieldname) {
								$join = $join.' LEFT OUTER JOIN '.$condfldtablname.' ON '.$condfldtablname.'.'.$condfldtabid.'='.$condtablname.'.'.$condfldtabid;
							}
							$conditionvalarray = array( 0 => array($condfldtablname,$condfldname,$condname,$condvale,'AND') );
							$wcond = $this->Basefunctions->whereclausegeneration($conditionvalarray);
						}
					} else {
						if($condtablname!=$tabname) {
							$tabid = $tabname.'id';
							$join = $join.' LEFT OUTER JOIN '.$condtablname.' ON '.$condtablname.'.'.$tabid.'='.$tabname.'.'.$tabid;
						}
						$conditionvalarray = array( 0=>array($condtablname,$condfname,$condname,$condvale,'AND') );
						$wcond = $this->Basefunctions->whereclausegeneration($conditionvalarray);
					}
				} else {
					if($ftablename!=$tabname) {
						$join = $join.' LEFT OUTER JOIN '.$tabname.' ON '.$tabname.'.'.$ftabid.'='.$ftablename.'.'.$ftabid;
					}
				}
				$mnamechk = $this->Basefunctions->tablefieldnamecheck($ftablename,'moduleid');
				$modcond = ( ($mnamechk == "true") ? 'AND '.$ftablename.'.moduleid='.$moduleid : '');
				$query = $this->db->query('select '.$ftablename.'.'.$ftabid.' AS Id'.','.$ftablename.'.'.$ftabfildname.' AS Name from '.$ftablename.''.$join.' where '.$whcond.' '.$wcond.''.$modcond.' AND '.$ftablename.'.status NOT IN (0,3) ORDER BY '.$ftablename.'.'.$ftabid.'');
				$dataset = $query->result();
			} else {
				$mnamechk = $this->Basefunctions->tablefieldnamecheck($ftablename,'moduleid');
				$modcond = ( ($mnamechk == "true") ? 'AND '.$ftablename.'.moduleid='.$moduleid : '');
				$tabid = $tabname.'id';
				$query = $this->db->query( 'select '.$tabname.'.'.$tabid.' AS Id'.','.$tabname.'.'.$tabfieldname.' AS Name from '.$tabname.' where '.$whcond.' '.$wcond.''.$modcond.' AND '.$tabname.'.status NOT IN (0,3) ORDER BY '.$tabname.'.'.$tabid.'');
				$dataset = $query->result();
			}
			$i=0;
			foreach($dataset as $data) {
				$dropdowndata[$i]=array('datasid'=>$data->Id,'dataname'=>$data->Name);
				$i++;
			}
			return json_encode($dropdowndata);
		}
    }
	//module drop down show
    public function dynamicmoduledropdownvalfetch($moduleid) {
		$userroleid = $this->Basefunctions->userroleid;
        $dropdowndata = array();
        $this->db->select('module.moduleid AS Id ,module.menuname AS Name');
        $this->db->from('module');
        $this->db->join('moduleinfo','moduleinfo.moduleid=module.moduleid');
        $this->db->where("FIND_IN_SET('".$moduleid."',moduleprivilegeid) >", 0);
        $this->db->where("FIND_IN_SET('".$userroleid."',moduleinfo.userroleid) >", 0);
		$this->db->where_in('module.status', array(0,1));
        $this->db->order_by('modulename','asc');
        $datas=$this->db->get();
		if($datas->num_rows()>0) {
			$i=0;
			foreach($datas->result() as $data) {
				$dropdowndata[$i]=array('datasid'=>$data->Id,'dataname'=>$data->Name);
				$i++;
			}
			return json_encode($dropdowndata);
		} else {
			return json_encode(array("fail"=>'FAILED'));
		}
    }
	//dynamic drop down generation value fetch with specific condition
    public function dynamicdropdownvalfetchwithusercond($tablename,$tabfieldname,$fieldprimaryid,$condid,$condvalue) {
		$userroleid = $this->Basefunctions->userroleid;
        $dropdowndata = array();
        $this->db->select(''.$fieldprimaryid.' AS Id'.','.''.$tabfieldname.' AS Name');
        $this->db->from($tablename);
        $this->db->where("FIND_IN_SET('".$condvalue."',".$condid.") >", 0);
		$this->db->where(''.$tablename.'.status', 1);
        $datas= $this->db->get();
		if($datas->num_rows()>0) {
			$i=0;
			foreach($datas->result() as $data) {
				$dropdowndata[$i]=array('datasid'=>$data->Id,'dataname'=>$data->Name);
				$i++;
			}
			return json_encode($dropdowndata);
		} else {
			return json_encode(array("fail"=>'FAILED'));
		}
    }
	//parent dynamic drop down generation value fetch
    public function parentdynamicgroupdropdownvalfetch($moduleid,$tablename1,$tablename2,$tabfieldname) {
		$userroleid = $this->Basefunctions->userroleid;
		$dropdowndata = array();
		$fieldprimaryid = $tablename1.'id';
		$parentprimaryid = $tablename2.'id';
		$parentprimaryname = $tablename2.'name';
		$join = ''.$tablename2.'.'.$parentprimaryid.'='.$tablename1.'.'.$parentprimaryid.'';
        $this->db->select(''.$tablename2.'.'.$parentprimaryid.' AS PId'.','.''.$tablename2.'.'.$parentprimaryname.' AS PName,'.$fieldprimaryid.' AS CId'.','.''.$tabfieldname.' AS CName');
        $this->db->from($tablename1);
		$this->db->join($tablename2,$join);
		$this->db->where(''.$tablename1.'.status', 1);
		$this->db->where(''.$tablename2.'.status', 1);
        $this->db->order_by(''.$tablename1.'.'.$parentprimaryid.'','asc');
        $this->db->order_by(''.$tablename1.'.'.$fieldprimaryid.'','asc');
        $datas= $this->db->get();
       if($datas->num_rows()>0) {
			$i=0;
			foreach($datas->result() as $data) {
				$dropdowndata[$i]=array('CId'=>$data->CId,'CName'=>$data->CName,'PId'=>$data->PId,'PName'=>$data->PName);
				$i++;
			}
			return json_encode($dropdowndata);
		} else {
			return json_encode(array("fail"=>'FAILED'));
		}
    }
    //workflow edit details fetch
    public function fetchformdataeditdetailsmodel() {
    	$dataprimaryid = $_GET['dataprimaryid'];
		$this->db->select('workflow.workflowid,workflow.moduleid,workflow.workflowname,workflow.description,workflow.workflowtriggertypeid,workflowactive,workflowtrigger.triggerfieldid,workflowtrigger.triggerfrequencytype,workflowtrigger.triggerexecutionfreqency,workflowtrigger.triggerexecutiontype,workflowtrigger.triggerexecutiontime,workflowalerttype,workflowalertname,workflowtransactionmode,workflowtransactiontype,workflowsenderid,workflowtemplateid,workflowalertrecipient,workflowalertadditonalrecipient,workflowmanualalertadditonalrecipient,workflowcallsoundid,workflowruleactiontaskname,taskalertduedate,taskalerttype,taskalertdays,taskstatusid,taskpriorityid,taskassignedto,notifyassignee,workflowruleactiontask.description as taskdesciption,workflowruleactionfieldname,workflowruleactionfield.modulefieldid as alertfiledmodfieldid,workflowruleactionfield.uitypeid as fielduitypeid,value,modulefield.fieldlabel,workflowtrigger.triggerrepeatevery,workflowtrigger.triggerexecutiononday');
    	$this->db->from('workflow');
    	$this->db->join('workflowtrigger','workflowtrigger.workflowid=workflow.workflowid');
    	$this->db->join('workflowruleactionalert','workflowruleactionalert.workflowid=workflow.workflowid','left outer');
    	$this->db->join('workflowruleactiontask','workflowruleactiontask.workflowid=workflow.workflowid','left outer');
    	$this->db->join('workflowruleactionfield','workflowruleactionfield.workflowid=workflow.workflowid','left outer');
    	$this->db->join('modulefield','modulefield.modulefieldid=workflowruleactionfield.modulefieldid','left outer');
    	$this->db->where('workflow.workflowid',$dataprimaryid);
    	$this->db->where('workflow.status',1);
    	$datas= $this->db->get();
    	if($datas->num_rows()>0) {
    		foreach($datas->result() as $data) {
    			$workflowdata=array('id'=>$data->workflowid,
    					'name'=>$data->workflowname,
    					'moduleid'=>$data->moduleid,
    					'description'=>$data->description,
    					'active'=>$data->workflowactive,
    					'triggertype'=>$data->workflowtriggertypeid,
    					'triggerfield'=>$data->triggerfieldid,
    					'triggerfrequency'=>$data->triggerfrequencytype,
    					'excufreq'=>$data->triggerexecutionfreqency,
    					'exectype'=>$data->triggerexecutiontype,
    					'exectime'=>$data->triggerexecutiontime,
    					'triggerrepeatevery'=>$data->triggerrepeatevery,
    					'triggerexecutiononday'=>$data->triggerexecutiononday,
    					'alerttype'=>$data->workflowalerttype,
    					'alertname'=>$data->workflowalertname,
    					'actionmode'=>$data->workflowtransactionmode,
    					'actiontype'=>$data->workflowtransactiontype,
    					'senderid'=>$data->workflowsenderid,
    					'templateid'=>$data->workflowtemplateid,
    					'recipient'=>$data->workflowalertrecipient,
    					'additonalrecipient'=>$data->workflowalertadditonalrecipient,
    					'manadditonalrecipient'=>$data->workflowmanualalertadditonalrecipient,
    					'callsoundid'=>$data->workflowcallsoundid,
    					'taskname'=>$data->workflowruleactiontaskname,
    					'duedate'=>$data->taskalertduedate,
    					'taskalerttype'=>$data->taskalerttype,
    					'taskalertdays'=>$data->taskalertdays,
    					'taskstatusid'=>$data->taskstatusid,
    					'taskpriorityid'=>$data->taskpriorityid,
    					'taskassignedto'=>$data->taskassignedto,
    					'notifyassignee'=>$data->notifyassignee,
    					'taskdesciption'=>$data->taskdesciption,
    					'actionfieldname'=>$data->workflowruleactionfieldname,
    					'modulefieldid'=>$data->alertfiledmodfieldid,
    					'fieldlabel'=>$data->fieldlabel,
    					'uitypeid'=>$data->fielduitypeid,
    					'value'=>$data->value);
    		}
    		echo json_encode($workflowdata);
    	} else {
    		echo json_encode(array("fail"=>'FAILED'));
    	}
    }
    //workflow update
    public function updateworkflowcreatemodel() {
    	$primaryid = $_POST['primaryid'];
    	$modid = $_POST['newrulemoduleid'];
    	$rulename = $_POST['newrulename'];
    	$descp = $_POST['ruledescription'];
    	$trigtype = $_POST['recordtriggertype'];
    	$active = $_POST['ruleactive'];
    	$userid = $this->Basefunctions->userid;
    	$cdate = date($this->Basefunctions->datef);
    	$status = $active=='Yes'?1:2;
    	//work flow main data sets
    	$newdata = array(
    		'moduleid'=>$modid,
    		'workflowname'=>$rulename,
    		'description'=>$descp,
    		'workflowtriggertypeid'=>$trigtype,
    		'workflowactive'=>$active,
    		'createuserid'=>$userid,
    		'lastupdateuserid'=>$userid,
    		'createdate'=>$cdate,
    		'lastupdatedate'=>$cdate,
    		'status'=>$status
    	);
    	$this->db->where('workflowid',$primaryid);
    	$this->db->update('workflow',$newdata);
    	//Rule trigger
    	if($trigtype=='1') {
    		$frequency = $_POST['recordacttrigtypeid'];
    		if($frequency == '4') {
    			$fields = $_POST['rulesdatafields'];
    			$fieldids = implode(',',$fields);
    		} else {
    			$fieldids = '1';
    		}
    		$execfreq = $_POST['fieldvalchktypeid'];
    		$executiontype="";
    		$exectime="";
    	} else if($trigtype=='2') {
    		$fieldids = $_POST['datefieldids'];
    		$frequency = $_POST['datefldfrequencytype'];
    		$executiontype = $_POST['datefldexecutiontype'];
    		$execfreq = $_POST['afterbeforetime'];
    		$exectime = $_POST['datefldexectiontime'];
			$trg_repeatevery = $_POST['repeateveryday'];
			if($frequency == 'Weekly') {
				$trg_executiononday = implode(',',array_filter($_POST['recurrencedayid']));
			} else if($frequency == 'Monthly') {
				$trg_executiononday = $_POST['recurrencemonthdayid'];
			} else {
				$trg_executiononday = '';
			}
    	}
    	$ruledata = array(
    		'workflowid'=>$primaryid,
    		'triggerfieldid'=>$fieldids,
    		'triggerfrequencytype'=>$frequency,
    		'triggerrepeatevery'=>$trg_repeatevery,
    		'triggerexecutiononday'=>$trg_executiononday,
    		'triggerexecutiontype'=>$executiontype,
    		'triggerexecutionfreqency'=>$execfreq,
    		'triggerexecutiontime'=>$exectime,
    		'createuserid'=>$userid,
    		'lastupdateuserid'=>$userid,
    		'createdate'=>$cdate,
    		'lastupdatedate'=>$cdate,
    		'status'=>1
    	);
    	$this->db->where('workflowid',$primaryid);
    	$this->db->update('workflowtrigger',$ruledata);
    	//Rule criteria
    	$girddata = $_POST['critgriddata'];
    	$cricount = $_POST['count'];
    	$girddatainfo = json_decode($girddata,true);
    	for($i=0;$i<=($cricount-1);$i++) {
    		if($girddatainfo[$i]['workflowcolstatus'] == "2") {
	    		$conddatas = array(
    				'workflowid'=>$primaryid,
    				'modulefieldid'=>$girddatainfo[$i]['rulecrtfieldnameid'],
    				'conditionname'=>$girddatainfo[$i]['rulecrtfieldcond'],
    				'conditionvalue'=>$girddatainfo[$i]['rulecritcondval'],
    				'conditionandorvalue'=>$girddatainfo[$i]['rulecrtandorcond'],
    				'conditionuitypeid'=>$girddatainfo[$i]['rulecrituitypeid'],
    				'createuserid'=>$userid,
    				'lastupdateuserid'=>$userid,
    				'createdate'=>$cdate,
    				'lastupdatedate'=>$cdate,
    				'status'=>1
	    		);
	    		$this->db->insert('workflowtriggercondition',$conddatas);
    		}
    	}
    	//delete condition
    	$delecondids = $_POST['workflowconrowcolids'];
    	$deletecondid = explode(',',$delecondids);
    	foreach($deletecondid as $conrowid) {
    		if($conrowid != 0){
    			$updatedcondata=array('lastupdatedate'=>$cdate,'lastupdateuserid'=>$userid,'status'=>0);
    			$this->db->update('workflowtriggercondition',$updatedcondata,array('workflowtriggercondition'=>$conrowid));
    		}
    	}
    	
    	{//Rule alert create
    		$wherearray=array('workflowid'=>$primaryid);
    		$alertcheck = $this->workflowrulealertcheck('workflowruleactionalert',$wherearray,'workflowid');
	    	$alertsave = $_POST['alertsave'];
	    	if($alertsave=='Yes') {
	    		$alerttype = $_POST['rulealerttype'];
	    		if($alerttype=='email') {
					$finaladdrecipient = '';
					if(is_array($_POST['rulealertadduserrecep'])) {
						$checkrulealertadduserrecep = explode(',',implode(',',$_POST['rulealertadduserrecep']));
						$_POST['rulealertadduserrecep'] = array();
						foreach($checkrulealertadduserrecep as $recdata) {
							if($recdata != '') {
								array_push($_POST['rulealertadduserrecep'],$recdata);
							}
						}
						$subaddrecipient = implode(',',array_filter($_POST['rulealertadduserrecep']));
						$addrecipient = $this->retrieveallrecipientemailids($subaddrecipient);
						$_POST['rulealertadduserrecep'] = implode(',',array_filter($_POST['rulealertadduserrecep']));
					} else {
						$_POST['rulealertadduserrecep'] = '';
					}
					if($_POST['rulealertadduserrecep'] == '') {
						$finaladdrecipient = $_POST['rulealertaddemailrecp'];
					} else {
						if($_POST['rulealertaddemailrecp'] == '' || $_POST['rulealertaddemailrecp'] == 1) { // Nothing will happen
							$finaladdrecipient = $_POST['rulealertaddemailrecp'];
						} else {
							$finaladdrecipient = array();
							$newdata = explode(',',$_POST['rulealertaddemailrecp']);
							foreach($newdata as $ndata) {
								if(!in_array($ndata, $addrecipient, true)) {
									array_push($finaladdrecipient,$ndata);
								}
							}
							$finaladdrecipient = implode(',',$finaladdrecipient);
						}
					}
	    			$alertdata = array(
	    					'workflowid'=>$primaryid,
	    					'workflowalerttype'=>$alerttype,
	    					'workflowalertname'=>$_POST['rulealertname'],
	    					'workflowtransactionmode'=>1,
	    					'workflowtransactiontype'=>1,
	    					'workflowsenderid'=>1,
	    					'workflowtemplateid'=>$_POST['rulealertemailtempid'],
	    					'workflowalertrecipient'=>$_POST['rulealertemailrecipient'],
	    					'workflowalertadditonalrecipient'=>$_POST['rulealertadduserrecep'],
	    					'workflowmanualalertadditonalrecipient'=>$finaladdrecipient,
	    					'workflowcallsoundid'=>1,
	    					'createuserid'=>$userid,
	    					'lastupdateuserid'=>$userid,
	    					'createdate'=>$cdate,
	    					'lastupdatedate'=>$cdate,
	    					'status'=>1
	    			);
	    			if($alertcheck  == 'TRUE'){
		    			$this->db->where('workflowid',$primaryid);
		    			$this->db->update('workflowruleactionalert',$alertdata);
	    			} else{
	    				$this->db->insert('workflowruleactionalert',$alertdata);
	    			}
	    		} else if($alerttype=='sms') {
					$finaladdrecipient = '';
					if(is_array($_POST['rulealertadduserrecep'])) {
						$checkrulealertadduserrecep = explode(',',implode(',',$_POST['rulealertadduserrecep']));
						$_POST['rulealertadduserrecep'] = array();
						foreach($checkrulealertadduserrecep as $recdata) {
							if($recdata != '') {
								array_push($_POST['rulealertadduserrecep'],$recdata);
							}
						}
						$subaddrecipient = implode(',',array_filter($_POST['rulealertadduserrecep']));
						$addrecipient = $this->retrieveallrecipientmobilenos($subaddrecipient);
						$_POST['rulealertadduserrecep'] = implode(',',array_filter($_POST['rulealertadduserrecep']));
					} else {
						$_POST['rulealertadduserrecep'] = '';
					}
					if($_POST['rulealertadduserrecep'] == '') {
						$finaladdrecipient = $_POST['rulealertsmsaddrecep'];
					} else {
						if($_POST['rulealertsmsaddrecep'] == '' || $_POST['rulealertsmsaddrecep'] == 1) { // Nothing will happen
							$finaladdrecipient = $_POST['rulealertsmsaddrecep'];
						} else {
							$finaladdrecipient = array();
							$newdata = explode(',',$_POST['rulealertsmsaddrecep']);
							foreach($newdata as $ndata) {
								if(!in_array($ndata, $addrecipient, true)) {
									array_push($finaladdrecipient,$ndata);
								}
							}
							$finaladdrecipient = implode(',',$finaladdrecipient);
						}
					}
	    			$alertdata = array(
	    					'workflowid'=>$primaryid,
	    					'workflowalerttype'=>$alerttype,
	    					'workflowalertname'=>$_POST['rulealertname'],
	    					'workflowtransactionmode'=>$_POST['rulealertsmstransmode'],
	    					'workflowtransactiontype'=>$_POST['rulealertsmstypeid'],
	    					'workflowsenderid'=>$_POST['rulealertsmssendid'],
	    					'workflowtemplateid'=>$_POST['rulealertsmstemp'],
	    					'workflowalertrecipient'=>$_POST['rulealertsmsrecep'],
	    					'workflowalertadditonalrecipient'=>$_POST['rulealertsmsaddrecep'],
							'workflowmanualalertadditonalrecipient'=>$finaladdrecipient,
	    					'workflowcallsoundid'=>1,
	    					'createuserid'=>$userid,
	    					'lastupdateuserid'=>$userid,
	    					'createdate'=>$cdate,
	    					'lastupdatedate'=>$cdate,
	    					'status'=>1
	    			);
	    		if($alertcheck  == 'TRUE'){
		    			$this->db->where('workflowid',$primaryid);
		    			$this->db->update('workflowruleactionalert',$alertdata);
	    			} else{
	    				$this->db->insert('workflowruleactionalert',$alertdata);
	    			}
	    		} else if($alerttype=='call') {
	    			if($_POST['rulealertcallsoundfile'] == '') {
	    				$soundid = 1;
	    			} else {
	    				$soundid = $_POST['rulealertcallsoundfile'];
	    			}
	    			$alertdata = array(
	    					'workflowid'=>$primaryid,
	    					'workflowalerttype'=>$alerttype,
	    					'workflowalertname'=>$_POST['rulealertname'],
	    					'workflowtransactionmode'=>$_POST['rulealertcalltransmode'],
	    					'workflowtransactiontype'=>$_POST['rulealertcallnumbertype'],
	    					'workflowsenderid'=>$_POST['rulealertcallernumber'],
	    					'workflowtemplateid'=>$_POST['rulealertcalltemp'],
	    					'workflowalertrecipient'=>$_POST['rulealertcallrecpnum'],
	    					'workflowalertadditonalrecipient'=>$_POST['rulealertcalladdrecpnum'],
	    					'workflowcallsoundid'=>$soundid,
	    					'createuserid'=>$userid,
	    					'lastupdateuserid'=>$userid,
	    					'createdate'=>$cdate,
	    					'lastupdatedate'=>$cdate,
	    					'status'=>1
	    			);
	    			if($alertcheck  == 'TRUE'){
		    			$this->db->where('workflowid',$primaryid);
		    			$this->db->update('workflowruleactionalert',$alertdata);
	    			} else{
	    				$this->db->insert('workflowruleactionalert',$alertdata);
	    			}
	    		} else if($alerttype=='desktop') {
	    			if(is_array($_POST['rulealertadduserrecep'])) {
						$checkrulealertadduserrecep = explode(',',implode(',',$_POST['rulealertadduserrecep']));
						$_POST['rulealertadduserrecep'] = array();
						foreach($checkrulealertadduserrecep as $recdata) {
							if($recdata != '') {
								array_push($_POST['rulealertadduserrecep'],$recdata);
							}
						}
						$_POST['rulealertadduserrecep'] = implode(',',array_filter($_POST['rulealertadduserrecep']));
					} else {
						$_POST['rulealertadduserrecep'] = '';
					}
	    			$alertdata = array(
	    					'workflowid'=>$primaryid,
	    					'workflowalerttype'=>$alerttype,
	    					'workflowtransactionmode'=>1,
	    					'workflowtransactiontype'=>1,
	    					'workflowsenderid'=>1,
	    					'workflowalertname'=>$_POST['rulealertname'],
	    					'workflowtemplateid'=>$_POST['rulealertusertemp'],
	    					'workflowalertrecipient'=>$_POST['rulealertuserrecep'],
	    					'workflowalertadditonalrecipient'=>$_POST['rulealertadduserrecep'],
	    					'workflowmanualalertadditonalrecipient'=>'',
	    					'workflowcallsoundid'=>1,
	    					'createuserid'=>$userid,
	    					'lastupdateuserid'=>$userid,
	    					'createdate'=>$cdate,
	    					'lastupdatedate'=>$cdate,
	    					'status'=>1
	    			);
	    			if($alertcheck  == 'TRUE'){
		    			$this->db->where('workflowid',$primaryid);
		    			$this->db->update('workflowruleactionalert',$alertdata);
	    			} else{
	    				$this->db->insert('workflowruleactionalert',$alertdata);
	    			}
	    		}
	    	}
    	}
    	{//Rule action assign task
	    	$tasksave = $_POST['tasksave'];
	    	if($tasksave=='Yes') {
	    		$alertcheck = $this->workflowrulealertcheck('workflowruleactiontask',$wherearray,'workflowid');
				if(isset($_POST['tasknotifyassignee'])) {
					$notifyassigne = $_POST['tasknotifyassignee'];
				} else {
					$notifyassigne = 'No';
				}
	    		//$notifyassigne = $_POST['tasknotifyassignee']!= '' ? $_POST['tasknotifyassignee'] : 'No';
	    		$taskdata = array(
	    				'workflowid'=>$primaryid,
	    				'workflowruleactiontaskname'=>$_POST['assigntaskname'],
	    				'moduleid'=>$modid,
	    				'taskalertduedate'=>$_POST['assigntaskduedate'],
	    				'taskalerttype'=>$_POST['assigntasktimetype'],
	    				'taskalertdays'=>$_POST['assigntaskdays'],
	    				'taskstatusid'=>$_POST['taskstatusid'],
	    				'taskpriorityid'=>$_POST['taskpriorityid'],
	    				'taskassignedto'=>$_POST['assigntaskemployeeid'],
	    				'notifyassignee'=>$notifyassigne,
	    				'description'=>$_POST['taskdescription'],
	    				'createuserid'=>$userid,
	    				'lastupdateuserid'=>$userid,
	    				'createdate'=>$cdate,
	    				'lastupdatedate'=>$cdate,
	    				'status'=>1
	    		);
	    		if($alertcheck  == 'TRUE'){
	    			$this->db->where('workflowid',$primaryid);
	    			$this->db->update('workflowruleactiontask',$taskdata);
	    		} else{
	    			$this->db->insert('workflowruleactiontask',$taskdata);
	    		}
	    	}
    	}
    	{//Rule action field update
	    	$fldupdatedatastatus = $_POST['fieldupdatesave'];
	    	if($fldupdatedatastatus == 'Yes') {
	    		$alertcheck = $this->workflowrulealertcheck('workflowruleactionalert',$wherearray,'workflowid');
	    		$fieldupdatedata = array(
	    				'workflowid'=>$primaryid,
	    				'moduleid'=>$modid,
	    				'workflowruleactionfieldname'=>$_POST['ruleupdatefieldsubject'],
	    				'modulefieldid'=>$_POST['ruleupdatefieldid'],
	    				'uitypeid'=>$_POST['ruleupdatefielduitype'],
	    				'value'=>$_POST['fieldupdatevalue'],
	    				'createuserid'=>$userid,
	    				'lastupdateuserid'=>$userid,
	    				'createdate'=>$cdate,
	    				'lastupdatedate'=>$cdate,
	    				'status'=>1
	    		);
	    		if($alertcheck == 'TRUE') {
	    			$this->db->where('workflowid',$primaryid);
	    			$this->db->update('workflowruleactionfield',$fieldupdatedata);
	    		} else {
	    			$this->db->insert('workflowruleactionfield',$fieldupdatedata);
	    		}
	    		
	    	}
    	} 
    	echo 'Success';
    }
    //check the record is available or not based on the workflow
    public function workflowrulealertcheck($table,$wherearray,$primaryname) {
    	$this->db->select($primaryname);
    	$this->db->from($table);
    	$this->db->where($wherearray);
    	$this->db->where($table.'.status',1);
    	$result = $this->db->get();
    	if($result->num_rows() >0) {
    		return 'TRUE';
    	} else {
    		return 'FALSE';
    	}
    }
    //rule creteria grid information fetch
    public function rulecreteriagridfetchfrunmodel() {
    	$productdetail = '';
    	$viewid = $_GET['workflowid'];
    	$this->db->select('SQL_CALC_FOUND_ROWS workflowtriggercondition.workflowtriggercondition,workflowtriggercondition.conditionandorvalue,modulefield.fieldlabel,modulefield.modulefieldid,workflowtriggercondition.conditionname,workflowtriggercondition.conditionvalue,workflowtriggercondition.conditionuitypeid,workflowtriggercondition.status',false);
    	$this->db->from('workflowtriggercondition');
    	$this->db->join('workflow','workflow.workflowid=workflowtriggercondition.workflowid');
    	$this->db->join('modulefield','modulefield.modulefieldid=workflowtriggercondition.modulefieldid');
    	$this->db->where_not_in('workflowtriggercondition.status',array(3,0));
    	$this->db->where('workflowtriggercondition.workflowid',$viewid);
    	$data=$this->db->get()->result();
    	$j=0;
    	foreach($data as $value) {
    		$productdetail->rows[$j]['id']=$value->workflowtriggercondition;
    		$productdetail->rows[$j]['cell']=array(
    				$value->conditionandorvalue,
    				$value->fieldlabel,
    				$value->modulefieldid,
    				$value->conditionname,
    				$value->conditionvalue,
    				$value->conditionuitypeid,
    				$value->status
    		);
    		$j++;
    	}
    	if($productdetail == '') {
    		$productdetail = array('fail'=>'FAILED');
    	}
    	echo  json_encode($productdetail);
    }
    //sender id drop down value fetch function
    public function smssenderddvalfetchmodel() {
    	$typeid = $_GET['typeid'];
    	$i=0;
    	$userid = $this->Basefunctions->userid;
    	$this->db->select('smssettings.smssettingsid,smssettings.senderid,smssettings.apikey as settingapikey,smsprovidersettings.smssendtypeid');
    	$this->db->from('smsprovidersettings');
    	$this->db->join('smssettings','smssettings.smsprovidersettingsid=smsprovidersettings.smsprovidersettingsid');
    	$this->db->where_in('smsprovidersettings.smssendtypeid',$typeid);
    	$this->db->where('smssettings.status',1);
    	$result = $this->db->get();
    	if($result->num_rows() > 0) {
    		foreach($result->result() as $row){
    			$data[$i] = array('datasid'=>$row->smssettingsid,'dataname'=>$row->senderid,'apikey'=>$row->settingapikey);
    			$i++;
    		}
    		echo json_encode($data);
    	} else {
    		echo json_encode(array("fail"=>'FAILED'));
    	}
    }
    //For SMS Send using SMS Widget
    public function smstemplateddvalfetchmodel() {
    	$sendsmstype = $_GET['smssendtype'];
    	$senderid = $_GET['senderid'];
    	$i=0;
    	$userid = $this->Basefunctions->userid;
    	$this->db->select('templatesid,templatesname,smsgrouptypeid,moduleid');
    	$this->db->from('templates');
    	if($sendsmstype == '2'){
    		$this->db->where_in('templates.smssettingsid',array($senderid));
    	}
    	if($sendsmstype != '4') {
    		$this->db->where_in('templates.smssendtypeid',array($sendsmstype));
    	}
    	$this->db->where_in('templates.smsgrouptypeid',array(3,4));
    	$this->db->where('templates.templatetypeid',2);
    	$this->db->where('templates.status',1);
    	$result = $this->db->get();
    	if($result->num_rows() > 0) {
    		foreach($result->result() as $row) {
    			$data[$i] = array('datasid'=>$row->templatesid,'dataname'=>$row->templatesname,'typeid'=>$row->smsgrouptypeid,'moduleid'=>$row->moduleid);
    			$i++;
    		}
    		echo json_encode($data);
    	} else {
    		echo json_encode(array("fail"=>'FAILED'));
    	}
    }
}
?>