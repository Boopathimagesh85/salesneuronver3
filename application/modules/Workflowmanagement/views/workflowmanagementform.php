<style type="text/css">
.innergridpaddingbtm {
	top:20px !important;	
}
</style>
<div class="large-12 columns paddingzero formheader">
	<?php
		$device = $this->Basefunctions->deviceinfo();
		$dataset['gridtitle']=$gridtitle;
		$dataset['titleicon']=$titleicon;
		$dataset['moduelid']=36;
		$dataset['formtype']= 'stepper';
		$this->load->view('Base/mainviewaddformheader',$dataset);
	?>
</div>
<div class="large-12 columns addformunderheader">&nbsp;</div>
<div class="large-12 columns scrollbarclass  stepperformcontainer mblnopadding">
	<div class="row mblhidedisplay">&nbsp;</div>
	<div id="subformspan1" class="hiddensubform">
		<div class="large-12 columns mblnopadding">
			<form id="newruleinfoform" name="ruleinfoform" method="post">
				<span id="ruleinfovalidate" class="validationEngineContainer cleardataform">
					<span id="ruleinfoupdatevalidate" class="validationEngineContainer ">
						<div class="large-4 columns paddingbtm large-offset-4 " >
							<div class="large-12 columns cleardataform paddingzero borderstyle">
								<div class="large-12 columns headerformcaptionstyle">Basic Information</div>
								<div class="static-field large-12 medium-12 small-12 columns">
									<label>Module Name<span class="mandatoryfildclass">*</span></label>
									<select id="newrulemoduleid" class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" name="newrulemoduleid" data-placeholder="Select" tabindex="101">
										<option value=""></option>
										<?php
											foreach($module as $data):
												$name = (($data->menuname!='')?$data->menuname:$data->modulename);
												echo '<option value="'.$data->moduleid.'" data-modname="'.$name.'">'.$name.'</option>';
											endforeach;
										?>
									</select>
								</div>
								<div class="input-field large-12 medium-12 small-12 columns">
									<input id="newrulename" type="text" class="validate[required,funcCall[workflownamecheck]]" name="newrulename" maxlength="100" value="" tabindex="102"> <label for="newrulename">Rule Name<span class="mandatoryfildclass">*</span></label>
									<input id="primaryid" type="hidden" class="" name="primaryid" value="" tabindex="102">									
								</div>
								<div class="input-field large-12 medium-12 small-12 columns">
									<textarea id="ruledescription" class="materialize-textarea" maxlength="200" name="ruledescription" rows="3" tabindex="103"></textarea>
									<label for="ruledescription">Description</label>
								</div>
								<div class="large-12 medium-12 small-12 columns">
									<div class="static-field large-6 medium-6 small-6 columns paddingzero ">
										<label style="left:0px;">Rule Trigger Type<span class="mandatoryfildclass">*</span></label>
										<select id="recordtriggertype" class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" name="recordtriggertype" data-placeholder="Select" tabindex="104">
											<option value=""></option>
											<option value="1">A Record Action</option>
											<option value="2">A Date Field Value</option>
										</select>
									</div>
									<div class="large-6 medium-6 small-6 columns hidedisplay" >
										<input id="ruleactivecls" class="filled-in" type="checkbox" name="ruleactivecls" value="No" tabindex="104">
										<label for="aaa">Active</label>
										<input id="ruleactive" type="hidden" data-defvalattr="No" value="No" name="ruleactive">
									</div>
								</div>
								<div>&nbsp;</div>
								<div  class="large-12 columns"style="text-align:right;">
									<label></label>
									<input id="informationsbtn" class="btn" type="button" name="informationsbtn" value="Next" tabindex="105">
								</div>
							</div>
						</div>
					</span>
				</span>
			</form>
		</div>
	</div>
	<div id="subformspan2" class="hidedisplay hiddensubform">
		<div class="large-12 columns mblnopadding" style="background: #f2f3fa;">
			<form id="recordbasdactionform" name="recordbasdactionform" method="post">
				<span id="recordactionvalidate" class="validationEngineContainer cleardataform">
					<span id="recordaction" class="hidedisplay">
						<div class="large-4 columns paddingbtm large-offset-4" >
							<div class="large-12 columns cleardataform paddingzero borderstyle">
								<div class="large-12 columns headerformcaptionstyle">Rule Trigger</div>
								<div class="dataruleradiobtndiv datarulechkbtnclass large-12 columns" style="cursor:pointer" data-radid="datacreate">
									<input type="radio" name="dataruleradiobtn" id="datacreate" class="datarulechkbtnclass with-gap" value="1" tabindex="106"><label for="datacreate"> Create </label>
								</div>
								<div class="dataruleradiobtndiv datarulechkbtnclass large-12 columns" style="cursor:pointer" data-radid="dataupdate">
									<input type="radio" name="dataruleradiobtn" id="dataupdate" class="datarulechkbtnclass with-gap" value="2" tabindex="107"><label for="dataupdate"> Edit </label> 
								</div>
								<div class="dataruleradiobtndiv datarulechkbtnclass large-12 columns" style="cursor:pointer" data-radid="datacreateupdate with-gap">
									<input type="radio" name="dataruleradiobtn" id="datacreateupdate" class="datarulechkbtnclass with-gap" value="3" tabindex="108" checked><label for="datacreateupdate"> Create Or Edit </label>
								</div>
								<div class="dataruleradiobtndiv datarulechkbtnclass large-12 columns" style="cursor:pointer" data-radid="datafieldupdate">
									<input type="radio" name="dataruleradiobtn" id="datafieldupdate" class="datarulechkbtnclass with-gap" value="4" tabindex="109"><label for="datafieldupdate"> Field Update </label>
								</div>
								<div id="selectfields" class="hidedisplay large-12 columns">
									<div class="static-field  large-12 columns">
										<label>Select Fields<span class="mandatoryfildclass">*</span></label>
										<select id="rulesdatafields" class="chzn-select" data-prompt-position="bottomLeft:14,36" name="rulesdatafields[]" data-placeholder="Select" tabindex="110" multiple>
											<option></option>
										</select>
									</div>
									<div class="large-12 columns">
										<input type="radio" name="fieldvalchktype" id="fieldallvalchktype" class="fldvalchkbtnclass with-gap" value="All" checked tabindex="111">
										<!--<label for="fieldallvalchktype"> Execute the rule when all the selected fields are updated</label><br>-->
										<label for="fieldallvalchktype"> When all the selected fields are updated</label><br>
										<input type="radio" name="fieldvalchktype" id="fieldanyvalchktype" class="fldvalchkbtnclass with-gap" value="Any" tabindex="112">
										<!--<label for="fieldanyvalchktype"> Execute the rule when any selected field is updated </label>-->
										<label for="fieldanyvalchktype"> When any selected field is updated </label>
									</div>
								</div>
								<div class="dataruleradiobtndiv datarulechkbtnclass large-12 columns" style="cursor:pointer" data-radid="datadelete">
									<input type="radio" name="dataruleradiobtn" id="datadelete" class="datarulechkbtnclass with-gap" value="5" tabindex="113"><label for="datadelete"> Delete </label>
								</div>
								<div class="large-12 columns" style="text-align:right;">
									<label></label>
									<input id="recordactionsbtn" class="btn alertbtnyes" type="button" name="" value="Next" tabindex="114">
								</div>
								<!--hidden fields-->
								<input type="hidden" name="recordacttrigtypeid" id="recordacttrigtypeid" value="3" />
								<input type="hidden" name="fieldvalchktypeid" id="fieldvalchktypeid" value="All" />
							</div>
						</div>
					</span>
				</span>
			</form>
			<form id="datebaseactionform" name="datebaseactionform" method="post">
				<span id="dateactionvalidate" class="validationEngineContainer cleardataform">
					<span id="datefieldvalue" class="hidedisplay">
						<div class="large-4 columns paddingbtm large-offset-4" >
							<div class="large-12 columns cleardataform paddingzero borderstyle">
								<div class="large-12 columns headerformcaptionstyle">Rule Trigger</div>
								<div class="static-field large-12 columns">
									<label>Field Name<span class="mandatoryfildclass">*</span></label>
									<select id="datefieldids" class="chzn-select validate[required]" data-prompt-position="bottomLeft:0,36" name="datefieldids" data-placeholder="Select" tabindex="112">
										<option>select</option>
									</select>
								</div>
								<div class="static-field large-12 columns">
									<label>Frequency<span class="mandatoryfildclass">*</span></label>
									<select id="datefldfrequencytype" class="chzn-select validate[required]" data-prompt-position="bottomLeft:0,36" name="datefldfrequencytype" data-placeholder="Select" tabindex="113">
										<option></option>
										<option value="Once" data-type="day(s)">Once</option>
										<option value="Daily" data-type="day(s)">Daily</option>
										<option value="Weekly" data-type="day(s)">Weekly</option>
										<option value="Monthly" data-type="day(s)">Monthly</option>
										<option value="Yearly" data-type="day(s)">Yearly</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label>Repeat Every<span class="mandatoryfildclass">*</span></label>
									<input id="repeateveryday" type="text" class="validate[required,custom[integer],maxSize[3],min[1]]" name="repeateveryday" value="" tabindex="102">
								</div>
								<div class="input-field large-6 columns">
									<input id="datefldexectiontime" class="validate[required] fortimepicicon time" type="text" data-prompt-position="bottomLeft"  tabindex="115" name="datefldexectiontime">
									<label for="datefldexectiontime">Time Of Execution<span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="static-field large-12 columns" id="weeklydiv">
									<label>On<span class="mandatoryfildclass " id="counter_req">*</span></label>
									<select id="recurrencedayid" class="chzn-select" name="recurrencedayid[]" data-placeholder="Select" tabindex="141" data-prompt-position="bottomLeft:14,36" multiple="multiple">
										<option></option>
										<?php
											foreach($recurrenceday as $key):
										?>
											<option value="<?php echo $key->recurrencedayid;?>" data-recurrencedayidhidden="<?php echo $key->recurrencedayname;?>" ><?php echo $key->recurrencedayname;?></option>
											<?php endforeach;?>
									</select>
								</div>
								<div class="static-field large-12 columns" id="monthlydiv">
									<label>On Day<span class="mandatoryfildclass " id="counter_req">*</span></label>						
									<select id="recurrencemonthdayid" name="recurrencemonthdayid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="1004">
										<option></option>
										<?php
											foreach($recurrenceonthe as $key):
										?>
											<option value="<?php echo $key->recurrenceonthename;?>" data-id="<?php echo $key->recurrenceontheid;?>" ><?php echo $key->recurrenceonthename;?></option>
											<?php endforeach;?>
									</select>
								</div>
								<div class="static-field large-12 columns">
									<label>Execution Type<span class="mandatoryfildclass">*</span></label>
									<select id="datefldexecutiontype" class="chzn-select validate[required]" data-prompt-position="bottomLeft:0,36" name="datefldexecutiontype" data-placeholder="Select" tabindex="114">
										<option></option>
										<option value="On">On</option>
										<option value="Before">Before</option>
										<option value="After">After</option>
									</select>
								</div>
								<div class="input-field large-12 columns hidedisplay" id="afterbeforesapn">
									<input type="text" name="afterbeforetime" id="afterbeforetime" value="" />
									<label for="afterbeforetime" id="afterbeforelab"></label>
								</div>
								<div  class="large-12 columns" style="text-align:right;">
									<label></label>
									<input id="datefieldvaluesbtn" class="btn alertbtnyes" type="button" name="datefieldvaluesbtn" value="Next">
								</div>
							</div>
						</div>
					</span>
				</span>
			</form>
		</div>
	</div>
	<div id="subformspan3" class="hidedisplay hiddensubform">
		<div class="large-12 columns mblnopadding" style="background:#f2f3fa;">
			<span id="criteria" class="">
				<form id="rulecriteriaaddform" name="rulecriteriaaddform">
					<span id="rulecriteriavalidate" class="rulecriteriacondclear validationEngineContainer cleardataform">
						<div class="large-4 columns paddingbtm" >
							<div class="large-12 columns cleardataform paddingzero borderstyle">
								<div class="large-12 columns headerformcaptionstyle">Rule Trigger</div>
								<div class="static-field large-12 columns">
									<label>Field Name<span class="mandatoryfildclass">*</span></label>
									<select id="rulecrtfieldname" class="chzn-select validate[required]" data-prompt-position="bottomLeft:0,36" name="rulecrtfieldname" data-placeholder="Select" tabindex="116">
										<option></option>
									</select>
									<input type="hidden" name="rulecrtfieldnameid" id="rulecrtfieldnameid" value="" />
								</div>
								<div class="static-field large-12 columns">
									<label>Condition<span class="mandatoryfildclass">*</span></label>
									<select id="rulecrtfieldcond" class="chzn-select validate[required]" data-prompt-position="bottomLeft:0,36" name="rulecrtfieldcond" data-placeholder="Select" tabindex="117">
										<option></option>
										<option value="Equalto">Equalto</option>
										<option value="NotEqual">NotEqual</option>
										<option value="Startwith">Startwith</option>
										<option value="Endwith">Endwith</option>
										<option value="Middle">Middle</option>
									</select>
								</div>
								<div id="rulecrdfieldvaldivhid" class="input-field  large-12 columns">
									<input id="rulecrdfieldval" type="text" class="validate[required,maxSize[100]]" name="rulecrdfieldval" value="" tabindex="118" />
									<label for="rulecrdfieldval">Value<span class="mandatoryfildclass">*</span></label>
								</div>
								<div id="rulecrddatefieldvaldivhid" class="hidedisplay input-field  large-12 columns">
									<?php
										$CI =& get_instance();
										$appdateformat = $CI->Basefunctions->appdateformatfetch();
									?>
									<input id="rulecrddatefieldval" type="text" class="validate[required]" name="rulecrddatefieldval" value="" data-dateformater="<?php echo $appdateformat; ?>" tabindex="119" />
									<label for="rulecrddatefieldval">Value<span class="mandatoryfildclass">*</span></label>
								</div>
								<div id="rulecrdtimefieldvaldivhid" class="hidedisplay input-field  large-12 columns">
									
									<input id="rulecrdtimefieldval" type="text" class="validate[required]" name="rulecrdtimefieldval" value="" tabindex="120" />
									<label for="rulecrdtimefieldval">Value<span class="mandatoryfildclass">*</span></label>
								</div>
								<div id="rulecrdddfieldvaldivhid" class="static-field hidedisplay  large-12 columns">
									<label>Value<span class="mandatoryfildclass">*</span></label>
									<select id="rulecrdddfieldval" class="chzn-select validate[required]" name="rulecrdddfieldval" data-placeholder="Select" tabindex="121">
										<option></option>
									</select>
								</div>
								<div id="rulecrtandorconddivhid" class="static-field  large-12 columns">
									<label>AND/OR <span class="mandatoryfildclass andormandclass"></span></label>
									<select id="rulecrtandorcond" class="chzn-select" data-prompt-position="bottomLeft:14,36" name="rulecrtandorcond" data-placeholder="Select" tabindex="122">
										<option></option>
										<option value="AND">AND</option>
										<option value="OR">OR</option>
									</select>
								</div>
								<div style="text-align:right;" class="large-12 columns">
									<label></label>
									<input id="rulecritsubbtn" class="btn alertbtnyes" type="button" name="rulecritsubbtn" value="Submit" tabindex="123" />
								</div>
								<!--hidden fields-->
								<input type="hidden" name="rulecritcondval" id="rulecritcondval" value="" />
								<input type="hidden" name="rulecrituitypeid" id="rulecrituitypeid" value="" />
								<input type="hidden" name="workflowconrowcolids" id="workflowconrowcolids" value="0" />
								<input type="hidden" name="workflowcolstatus" id="workflowcolstatus" value="2" />
							</div>
						</div>	
					</span>
				</form>
				<div class="large-8 columns" >
					<div class="large-12 columns viewgridcolorstyle  borderstyle" style="padding-left:0;padding-right:0;top:20px;">
						<div class="large-12 columns headerformcaptionstyle" style="height:auto; padding: 0.2rem 0 0rem;">
							<span class="large-6 medium-6 small-6 columns lefttext ">Criteria Condition</span>
							<span class="large-6 medium-6 small-6 columns innergridicon righttext" style="margin-top: 4px;margin-bottom: -3px;">
								<span id="rulecritconddel" class="deleteiconclass " title="Delete"><i class="material-icons">delete</i></span>
							</span>
						</div>
						<?php
							$device = $this->Basefunctions->deviceinfo();
							if($device=='phone') {
								echo '<div class="large-12 columns paddingzero forgetinggridname" id="workflowmanagementinnergridwidth"><div class="row-content inner-gridcontent" id="workflowmanagementinnergrid">
									<!-- Table header content , data rows & pagination for mobile-->
								</div>
								<footer class="inner-gridfooter footercontainer" id="workflowmanagementinnergridfooter">
									<!-- Footer & Pagination content -->
								</footer></div>';
							} else {
								echo '<div class="large-12 columns forgetinggridname" id="workflowmanagementinnergridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="workflowmanagementinnergrid" style="max-width:2000px; height:240px;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
								<div class="inner-gridfooter footer-content footercontainer" id="workflowmanagementinnergridfooter">
									<!-- Footer & Pagination content -->
								</div></div>';
							}
						?>
					</div>
					<div class="large-12">&nbsp;</div>
					<div style="text-align:right;">
						<label></label>
						<input id="alertnextbtn" class="btn alertbtnyes" type="button" name="alertnextbtn" value="Next" tabindex="124" />
					</div>
				</div>
				<div class="large-12">&nbsp;</div>
			</span>
		</div>
	</div>
	<div id="subformspan4" class="hidedisplay hiddensubform">
		<div class="large-12 columns mblnopadding" style="background:#f2f3fa;">
			<form id="rulealertaddform" name="rulealertaddform">
				<span id="rulealertadddatavalidate" class="rulealertdataclear validationEngineContainer cleardataform">
					<span id="alertullist" class="hidedisplay1">
						<div class="large-4 columns paddingbtm" >
							<div class="large-12 columns cleardataform paddingzero borderstyle">
								<div class="large-12 columns headerformcaptionstyle">Send Alerts</div>
								<span id="actions" class="" style="">
									<div class="static-field large-12 columns">
										<label>Alert type<span class="mandatoryfildclass">*</span></label>
										<select id="rulealerttype" class="chzn-select validate[required]" name="rulealerttype" data-placeholder="Select" tabindex="122" data-prompt-position="bottomLeft:14,36">
											<option></option>
											<option value="email">E-mail</option>
											<option value="sms">SMS</option>
											<option value="call">Call</option>
											<option value="desktop">Desktop</option>
										</select>
									</div>
									<div class="input-field large-12 columns">
										<input id="rulealertname" type="text" name="rulealertname" class="validate[required]" maxlength="100" value="" tabindex="123" />
										<label for="rulealertname">Alert name<span class="mandatoryfildclass">*</span></label>
									</div>
									<div class="large-12 columns">
										<label>Module : <span class="alertmodsapn"></span></label> 
									</div>
									<div class="large-12 columns">&nbsp;</div>
								</span>
								<span id="emailfields" class="hidedisplay">
									<div class="static-field large-12 columns">
										<label>Email Templates<span class="mandatoryfildclass">*</span></label>
										<select id="rulealertemailtempid" class="chzn-select mailmandclass" name="rulealertemailtempid" data-placeholder="Select" tabindex="124" data-prompt-position="bottomLeft:14,36">
											<option></option>
										</select>
									</div>
									<div class="static-field large-12 columns">
										<label>Email Recipients<span class="mandatoryfildclass">*</span></label>
										<select id="rulealertemailrecipient" class="chzn-select mailmandclass" name="rulealertemailrecipient" data-placeholder="Select" tabindex="125" data-prompt-position="bottomLeft:14,36">
											<option></option>
										</select>
									</div>
									<div class="static-field large-12 columns">
										<label>Additional User Recipients</label>
										<select id="rulealertadduserrecep" class="chzn-select" name="rulealertadduserrecep[]" data-placeholder="Select" tabindex="141" data-prompt-position="bottomLeft:14,36" multiple="multiple">
											<option></option>
											<?php
												$prev = ' ';
												for($i=0;$i<count($employee);$i++) {
													if($employee[$i]['CName'] != '') {
														$cur = $employee[$i]['PId'];
														$a ="";
														if($prev != $cur ) {
															if($prev != " ") {
																echo '</optgroup>';
															}
															echo '<optgroup  label="'.$employee[$i]['PName'].'" class="'.str_replace(' ','',$employee[$i]['PName']).'">';
															$prev = $employee[$i]['PId'];
															$a = "pclass";
														}
														echo '<option value="'.$employee[$i]['PId'].':'.$employee[$i]['CId'].'" >'.$employee[$i]['CName'].'</option>';
													}
												}
											?>
										</select>
									</div>
									<div class="input-field large-12 columns">
										<textarea id="rulealertaddemailrecp" class="materialize-textarea" name="rulealertaddemailrecp" rows="3" tabindex="126"></textarea>
										<label for="rulealertaddemailrecp">Additional Email Recipients</label>
									</div>
								</span>
								<span id="smsfields" class="hidedisplay">
									<div class="static-field large-12 columns">
										<label>Transaction Mode<span class="mandatoryfildclass">*</span></label>
										<select id="rulealertsmstransmode" class="chzn-select smsmandclass" name="rulealertsmstransmode" data-placeholder="Select" tabindex="127" data-prompt-position="bottomLeft:14,36">
											<option></option>
											<?php
												foreach($smstransmode as $data):
													echo '<option value="'.$data->smssendtypeid.'">'.$data->smssendtypename.'</option>';
												endforeach;
											?>
										</select>
									</div>
									<div class="static-field large-6 columns">
										<label>Sender Id<span class="mandatoryfildclass">*</span></label>
										<select id="rulealertsmssendid" class="chzn-select smsmandclass" name="rulealertsmssendid" data-placeholder="Select" tabindex="128" data-prompt-position="bottomLeft:14,36">
											<option></option>
										</select>
									</div>
									<div class="static-field large-6 columns">
										<label>SMS Type<span class="mandatoryfildclass">*</span></label>
										<select id="rulealertsmstypeid" class="chzn-select smsmandclass" name="rulealertsmstypeid" data-placeholder="Select" tabindex="129" data-prompt-position="bottomLeft:14,36">
											<option></option>
											<?php
												foreach($smstype as $data):
													echo '<option value="'.$data->smstypeid.'">'.$data->smstypename.'</option>';
												endforeach;
											?>
										</select>
									</div>
									<div class="static-field large-12 columns">
										<label>SMS Templates<span class="mandatoryfildclass">*</span></label>
										<select id="rulealertsmstemp" class="chzn-select smsmandclass" name="rulealertsmstemp" data-placeholder="Select" tabindex="126" data-prompt-position="bottomLeft:14,36">
											<option></option>
										</select>
									</div>
									<div class="static-field large-12 columns">
										<label>SMS Recipients<span class="mandatoryfildclass">*</span></label>
										<select id="rulealertsmsrecep" class="chzn-select smsmandclass" name="rulealertsmsrecep" data-placeholder="Select" tabindex="130" data-prompt-position="bottomLeft:14,36">
											<option></option>
										</select>
									</div>
									<div class="static-field large-12 columns">
										<label>Additional User Recipients</label>
										<select id="rulealertadduserrecep" class="chzn-select" name="rulealertadduserrecep[]" data-placeholder="Select" tabindex="141" data-prompt-position="bottomLeft:14,36" multiple="multiple">
											<option></option>
											<?php
												$prev = ' ';
												for($i=0;$i<count($employee);$i++) {
													if($employee[$i]['CName'] != '') {
														$cur = $employee[$i]['PId'];
														$a ="";
														if($prev != $cur ) {
															if($prev != " ") {
																echo '</optgroup>';
															}
															echo '<optgroup  label="'.$employee[$i]['PName'].'" class="'.str_replace(' ','',$employee[$i]['PName']).'">';
															$prev = $employee[$i]['PId'];
															$a = "pclass";
														}
														echo '<option value="'.$employee[$i]['PId'].':'.$employee[$i]['CId'].'" >'.$employee[$i]['CName'].'</option>';
													}
												}
											?>
										</select>
									</div>
									<div class="input-field large-12 columns">
										<textarea id="rulealertsmsaddrecep" class="materialize-textarea" name="rulealertsmsaddrecep" rows="3" tabindex="131"></textarea>
										<label for="rulealertsmsaddrecep">Additional Recipients</label>
									</div>
								</span>
								<span id="callfields" class="hidedisplay">
									<div class="static-field large-12 columns">
										<label>Transaction Mode<span class="mandatoryfildclass">*</span></label>
										<select id="rulealertcalltransmode" class="chzn-select callmandclass" name="rulealertcalltransmode" data-placeholder="Select" tabindex="133" data-prompt-position="bottomLeft:14,36">
											<option></option>
											<?php
												foreach($callmode as $data):
													echo '<option value="'.$data->smssendtypeid.'">'.$data->smssendtypename.'</option>';
												endforeach;
											?>
										</select>
									</div>
									<div class="static-field large-6 columns">
										<label>Number Type<span class="mandatoryfildclass">*</span></label>
										<select id="rulealertcallnumbertype" class="chzn-select callmandclass" name="rulealertcallnumbertype" data-placeholder="Select" tabindex="134" data-prompt-position="bottomLeft:14,36">
											<option></option>
											<?php
												foreach($numbertype as $data):
													echo '<option value="'.$data->callnumbertypeid.'">'.$data->callnumbertypename.'</option>';
												endforeach;
											?>
										</select>
									</div>
									<div class="static-field large-6 columns">
										<label>Caller Number<span class="mandatoryfildclass">*</span></label>
										<select id="rulealertcallernumber" class="chzn-select callmandclass" name="rulealertcallernumber" data-placeholder="Select" tabindex="135" data-prompt-position="bottomLeft:14,36">
											<option></option>
										</select>
									</div>
									<div class="static-field large-12 columns hidedisplay">
										<label>Call Templates<span class="mandatoryfildclass">*</span></label>
										<select id="rulealertcalltemp" class="chzn-select" name="rulealertcalltemp" data-placeholder="Select" tabindex="132" data-prompt-position="bottomLeft:14,36">
											<option></option>
											<?php
												foreach($calltempl as $data):
													echo '<option value="'.$data->templatesid.'">'.$data->templatesname.'</option>';
												endforeach;
											?>
										</select>
									</div>
									<div class="static-field large-12 columns">
										<label>Sound file<span class="mandatoryfildclass">*</span></label>
										<select id="rulealertcallsoundfile" class="chzn-select callmandclass" name="rulealertcallsoundfile" data-placeholder="Select" tabindex="136">
											<option></option>
											<?php
												foreach($sound as $data):
													echo '<option value="'.$data->soundsid.'">'.$data->soundsname.'</option>';
												endforeach;
											?>
										</select>
									</div>
									<div class="static-field large-12 columns">
										<label>Receiver Number<span class="mandatoryfildclass">*</span></label>
										<select id="rulealertcallrecpnum" class="chzn-select callmandclass" name="rulealertcallrecpnum" data-placeholder="Select" tabindex="137" data-prompt-position="bottomLeft:14,36">
											<option></option>
										</select>
									</div>
									<div class="input-field large-12 columns">
										<textarea id="rulealertcalladdrecpnum" class="materialize-textarea" name="rulealertcalladdrecpnum" rows="3" tabindex="138"></textarea>
										<label for="rulealertcalladdrecpnum">Additional Receiver Number</label>
									</div>
								</span>
								<span id="desktopfields" class="hidedisplay">
									<div class="static-field large-12 columns">
										<label>User Templates<span class="mandatoryfildclass">*</span></label>
										<select id="rulealertusertemp" class="chzn-select usrmandclass" name="rulealertusertemp" data-placeholder="Select" tabindex="139" data-prompt-position="bottomLeft:14,36">
											<option></option>
										</select>
									</div>
									<div class="static-field large-12 columns">
										<label>User Recipients<span class="mandatoryfildclass">*</span></label>
										<select id="rulealertuserrecep" class="chzn-select usrmandclass" name="rulealertuserrecep" data-placeholder="Select" tabindex="140" data-prompt-position="bottomLeft:14,36">
											<option></option>
										</select>
									</div>
									<div class="static-field large-12 columns">
										<label>Additional User Recipients</label>
										<select id="rulealertadduserrecep" class="chzn-select" name="rulealertadduserrecep[]" data-placeholder="Select" tabindex="141" data-prompt-position="bottomLeft:14,36" multiple="multiple">
											<option></option>
											<?php
												$prev = ' ';
												for($i=0;$i<count($employee);$i++) {
													if($employee[$i]['CName'] != '') {
														$cur = $employee[$i]['PId'];
														$a ="";
														if($prev != $cur ) {
															if($prev != " ") {
																echo '</optgroup>';
															}
															echo '<optgroup  label="'.$employee[$i]['PName'].'" class="'.str_replace(' ','',$employee[$i]['PName']).'">';
															$prev = $employee[$i]['PId'];
															$a = "pclass";
														}
														echo '<option value="'.$employee[$i]['PId'].':'.$employee[$i]['CId'].'" >'.$employee[$i]['CName'].'</option>';
													}
												}
											?>
										</select>
									</div>
								</span>
								<div class="large-12  columns" style="text-align:right">
								<input id="rulesendalertsave" class="btn alertbtnyes" type="button" name="rulesendalertsave" value="Agree" />
								<span>&nbsp;</span>
								<input id="rulesendalertcancel" class="btn alertbtnno" type="button" name="rulesendalertcancel" value="Cancel" />
								</div>
								<div class="large-12 columns">&nbsp;</div>
							</div>
						</div>
					</span>
				</span>
				<!-- hidden fields -->
				<input name="alertsave" type="hidden" id="alertsave" value="No" />
			</form>
			<form id="ruletaskaddform" name="ruletaskaddform">
				<span id="ruletaskadddatavalidate" class="ruletaskdataclear validationEngineContainer">
					<span id="assigntasks" class="hidedisplay2">
							<div class="large-4 columns paddingbtm" >
							<div class="large-12 columns cleardataform paddingzero borderstyle">
							<div class="large-12 columns headerformcaptionstyle">Assign Tasks</div>
							<div class="large-12 columns">
								<label>Module : <span class="alertmodsapn"></span></label> 
							</div>
							<div class="input-field large-12 columns">
								<input id="assigntaskname" type="text" name="assigntaskname" class="validate[required]" maxlength="100" value="" tabindex="" />
								<label for="assigntaskname">Task Name<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="large-12 columns">
								<div class="static-field large-5 medium-5 small-5 columns paddingtwo">
									<label style="left: 0px;">Due Date<span class="mandatoryfildclass">*</span></label>
									<select id="assigntaskduedate" class="chzn-select validate[required]" name="assigntaskduedate" data-placeholder="Select" tabindex="" data-prompt-position="bottomLeft:14,36">
										<option></option>
									</select>
								</div>
								<div class="static-field large-5 medium-5 small-3 columns paddingtwo">
									<label>&nbsp;</label>
									<select id="assigntasktimetype" class="chzn-select validate[required]" name="assigntasktimetype" data-placeholder="Select" tabindex="" data-prompt-position="bottomLeft:14,36">
										<option></option>
										<option value="plus">plus</option>
										<!--<option value="minus">minus</option>-->
									</select>
								</div>
								<div class="input-field large-2 medium-2 small-2 columns paddingtwo">
									<input id="assigntaskdays" type="text" name="assigntaskdays" class="validate[required]" value="" tabindex="" />
									<label for="assigntaskdays">day(s)</label>
								</div>
								<div class="large-2 medium-2 small-2 columns paddingtwo">
								</div>
							</div>
							<div class="static-field large-12 columns">
								<label>Task Status<span class="mandatoryfildclass">*</span></label>
								<select id="taskstatusid" class="chzn-select validate[required]" name="taskstatusid" data-placeholder="Select" tabindex="" data-prompt-position="bottomLeft:14,36">
									<option></option>
									<?php
										foreach($taskstatus as $data):
											echo '<option value="'.$data->crmstatusid.'">'.$data->crmstatusname.'</option>';
										endforeach;
									?>
								</select>
							</div>
							<div class="static-field large-12 columns">
								<label>Task Priority<span class="mandatoryfildclass">*</span></label>
								<select id="taskpriorityid" class="chzn-select validate[required]" name="taskpriorityid" data-placeholder="Select" tabindex="" data-prompt-position="bottomLeft:14,36">
									<option></option>
									<?php
										foreach($taskpri as $data):
											echo '<option value="'.$data->priorityid.'">'.$data->priorityname.'</option>';
										endforeach;
									?>
								</select>
							</div>
							<div class="static-field large-12 columns">
								<label>Assigned To<span class="mandatoryfildclass">*</span></label>
								<select id="assigntaskemployeeid" class="chzn-select validate[required]" name="assigntaskemployeeid" data-placeholder="Select" tabindex="" data-prompt-position="bottomLeft:14,36">
									<option></option>
									<?php
										foreach($taskassign as $data):
											echo '<option value="'.$data->employeeid.'">'.$data->employeename.' '.$data->lastname.'</option>';
										endforeach;
									?>
								</select>
							</div>
							<div class="large-12 columns">
								<input name="tasknotifyassignee" class="filled-in" type="checkbox" id="tasknotifyassignee" value="Yes" tabindex="" />
								<label for="tasknotifyassignee">Notify Assignee</label>
							</div>
							<div class="input-field large-12 columns">
								<textarea name="taskdescription" id="taskdescription" class="materialize-textarea" maxlength="200" tabindex=""></textarea>
								<label for="taskdescription">Description</label>
							</div>
							<div class="large-12  columns" style="text-align:right">
								<input id="ruleassigntasksave" class="btn alertbtnyes" type="button" name="ruleassigntasksave" value="Agree" />
								<span>&nbsp;</span>
								<input id="ruleassigntaskcancel" class="btn alertbtnno" type="button" name="ruleassigntaskcancel" value="Cancel" />
							</div>
							</div>
						</div>
					</span>
				</span>
				<!-- hidden fields -->
				<input name="tasksave" type="hidden" id="tasksave" value="No" />
			</form>
			<form id="ruleupdatefieldaddform" name="ruleupdatefieldaddform">
				<span id="ruleupdatefieldadddatavalidate" class="ruleupdatefielddataclear validationEngineContainer cleardataform">
						<span id="updatefields" class="hidedisplay3">
							<div class="large-4 columns paddingbtm" >
								<div class="large-12 columns cleardataform paddingzero borderstyle">
									<div class="large-12 columns headerformcaptionstyle">Update fields</div>
									<div class="large-12 columns">
										<label>Module : <span class="alertmodsapn"></span></label> 
									</div>
									<div class="large-12 columns">
										<label>Name<span class="mandatoryfildclass">*</span></label>
										<input id="ruleupdatefieldsubject" type="text" name="ruleupdatefieldsubject" class="validate[required]" value="" tabindex="" />
									</div>
									<div class="large-12 columns">
										<div class="static-field large-6 medium-6 small-5 columns paddingtwo">
											<label style="left:0px;">Update<span class="mandatoryfildclass">*</span></label>
											<select id="ruleupdatefieldname" class="chzn-select validate[required]" name="ruleupdatefieldname" data-placeholder="Select" tabindex="" data-prompt-position="bottomLeft:14,36">
												<option></option>
											</select>
											<input type="hidden" name="ruleupdatefieldid" id="ruleupdatefieldid" value="" />
											<input type="hidden" name="ruleupdatefielduitype" id="ruleupdatefielduitype" value="" />
										</div>
										<div class="large-1 medium-1 small-1 columns paddingtwo">
											<label>&nbsp;</label>
											<span style="padding-left:7px!important;">=</span>
										</div>
										<div id="ruleupdatefieldvaluedivhid" class="input-field large-5 medium-5 small-5 columns paddingtwo">
											<input id="ruleupdatefieldvalue" type="text" class="" name="ruleupdatefieldvalue" value="" tabindex="" />
											<label for="ruleupdatefieldvalue">Value</label>
										</div>
										<div id="ruleupdatedatefieldvaldivhid" class="input-field large-5 medium-5 small-5 columns paddingtwo hidedisplay">
											<?php
												$CI =& get_instance();
												$appdateformat = $CI->Basefunctions->appdateformatfetch();
											?>
											<input id="ruleupdatedatefieldval" type="text" class="" name="ruleupdatedatefieldval" value="" data-dateformater="<?php echo $appdateformat; ?>" tabindex="" />
											<label for="ruleupdatedatefieldval">Value</label>
										</div>
										<div id="ruleupdatetimevaluedivhid" class="input-field large-5 medium-5 small-5 columns paddingtwo hidedisplay">
											<input id="ruleupdatetimevalue" type="text" class="" name="ruleupdatetimevalue" value="" tabindex="" />
											<label for="ruleupdatetimevalue">Value</label>
										</div>
										<div id="ruleupdateddfieldvaldivhid" class="static-field large-5 medium-5 small-5 columns paddingtwo hidedisplay">
											<label>Value<span class="mandatoryfildclass">*</span></label>
											<select id="ruleupdateddfieldval" class="chzn-select" name="ruleupdateddfieldval" data-placeholder="Select" tabindex="" data-prompt-position="bottomLeft:14,36">
												<option></option>
											</select>
										</div>
									</div>
									<div class="large-12  columns" style="text-align:right;margin-top: 25px;">
										<input id="ruleupdatefldssave" class="btn alertbtnyes" type="button" name="ruleupdatefldssave" value="Agree" />
										<span>&nbsp;</span>
										<input id="ruleupdatefldcancel" class="btn alertbtnno" type="button" name="ruleupdatefldcancel" value="Cancel" />
									</div>
								</div>
							</div>
						</span>
					<!-- hidden fields -->
					<input name="fieldupdatevalue" type="hidden" id="fieldupdatevalue" value="" />
				</span>
				<!-- hidden fields -->
				<input name="fieldupdatesave" type="hidden" id="fieldupdatesave" value="No" />
			</form>
		</div>
	</div>
</div>
