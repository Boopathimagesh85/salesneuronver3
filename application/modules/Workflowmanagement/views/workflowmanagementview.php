<!DOCTYPE HTML>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
	<style type="text/css">
	#subformspan1 .mblnopadding{
		background:#f2f3fa;
	}
	.row .mblhidedisplay {
		background:#f2f3fa;
	}
	</style>
</head>
<body class="hidedisplay">
	<div class="row" style="max-width:100%">
		<div class="off-canvas-wrap" data-offcanvas>
			<div class="inner-wrap">
				<div class="gridviewdivforsh" style="" id="workflowmanagementview">
					<?php
						$device = $this->Basefunctions->deviceinfo();
						$dataset['gridtitle'] = $gridtitle['title'];
						$dataset['titleicon'] = $gridtitle['titleicon'];
						$dataset['gridid'] = 'workflowmanagementviewgrid';
						$dataset['gridwidth'] = 'workflowmanagementviewgridwidth';
						$dataset['gridfooter'] = 'workflowmanagementviewgridfooter';
						$dataset['formtype'] = 'workflow';
						$this->load->view('Base/mainviewheader',$dataset);
						$dataset['moduleid']='36';
						$this->load->view('Base/singlemainviewformwithgrid.php',$dataset);
					?>
				</div></div></div>
				<div id="workflowform" class="hidedisplay">
					<?php
						$this->load->view('workflowmanagementform');
					?>
				</div>
				<?php
					$this->load->view('Base/basedeleteformforcombainedmodules');
					$this->load->view('Base/basedeleteform');
					if($device=='phone') {
						$this->load->view('Base/overlaymobile');
					} else {
						$this->load->view('Base/overlay');
					}
					$this->load->view('Base/modulelist');
				?>
			</div>
		</div>
	</div>
</body>
<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
<?php $this->load->view('Base/bottomscript'); ?>
<!-- js File -->
<script src="<?php echo base_url();?>js/Workflowmanagement/workflowmanagement.js" type="text/javascript"></script>
<!-- For Tablet and Mobile view Dropdown Script -->
<script>
	$(document).ready(function(){
		$("#tabgropdropdown").change(function(){
			var tabgpid = $("#tabgropdropdown").val(); 
			$('.sidebariconstab[data-subform="'+tabgpid+'"]').trigger('click');
		});
	});
</script>
</html>