<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Workflowmanagement extends CI_Controller {
	function __construct() {
    	parent::__construct();
    	$this->load->helper('formbuild');
		$this->load->model('Workflowmanagement/Workflowmanagementmodel');
		$this->load->model('Base/Basefunctions');
    }
	public function index() {
		$moduleid = array(36);
		sessionchecker($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['module'] = $this->Basefunctions->simpledropdownwithcondorder('moduleid','modulename,menuname','module','moduleprivilegeid',36,'menuname');
		$data['smstransmode'] = $this->Basefunctions->simpledropdown('smssendtype','smssendtypeid,smssendtypename','smssendtypename');
		$data['smstype'] = $this->Basefunctions->simpledropdown('smstype','smstypeid,smstypename','smstypename');
		$data['smssenderid'] = $this->Basefunctions->simpledropdown('smssettings','smssettingsid,senderid','smssettingsid');
		$data['callmode'] = $this->Basefunctions->simpledropdownwithcondorder('smssendtypeid','smssendtypeid,smssendtypename','smssendtype','moduleid',39,'sortorder');
		$data['numbertype'] = $this->Basefunctions->simpledropdownwithcondorder('callnumbertypeid','callnumbertypeid,callnumbertypename','callnumbertype','moduleid',30,'sortorder');
		$data['taskpri'] = $this->Basefunctions->simpledropdownwithcondorder('priorityid','priorityid,priorityname','priority','moduleid',206,'sortorder');
		$data['taskstatus'] = $this->Basefunctions->simpledropdownwithcondorder('crmstatusid','crmstatusid,crmstatusname','crmstatus','moduleid',206,'sortorder');
		$data['calltempl'] = $this->Basefunctions->simpledropdownwithcondorder('templatesid','templatesid,templatesname','templates','templatetypeid',3,'templatesname');
		$data['sound'] = $this->Basefunctions->simpledropdown('sounds','soundsid,soundsname','soundsname');
		$data['taskassign'] = $this->Basefunctions->simpledropdownwithcondorder('employeeid','employeeid,employeename,lastname','employee','','','employeename');
		$data['employee'] = $this->Basefunctions->userspecificgroupdropdownvalfetch();
		$data['recurrenceday']=$this->Basefunctions->simpledropdownwithcond('recurrencedayid','recurrencedayname','recurrenceday','moduleid','36');
		$data['recurrenceonthe']=$this->Basefunctions->simpledropdownwithcond('recurrenceontheid','recurrenceonthename','recurrenceonthe','moduleid','36');
		$this->load->view('Workflowmanagement/workflowmanagementview',$data);
	}
	//condition fields
	public function rulecondfiledsdropdown() {
		$this->Workflowmanagementmodel->rulecondfiledsdropdownmodel();
	}
	//data fields
	public function fielddropdownload() {
		$this->Workflowmanagementmodel->fielddropdownloadmodel();
	}
	//Data Rule field name fetch
	public function ruleactionfieldnamefetch() {
		$this->Workflowmanagementmodel->ruleactionfieldnamefetchmodel();
	}
	//Data rule email template
	public function emailtemplateinformationfetch() {
		$this->Workflowmanagementmodel->emailtemplateinformationfetchmodel();
	}
	//Data rule desktop template
	public function desktoptemplateinformationfetch() {
		$this->Workflowmanagementmodel->desktoptemplateinformationfetchmodel();
	}
	//condition fieldname picklist values fetch
	public function modulefiledpicklistvalfetch() {
		$this->Workflowmanagementmodel->modulefiledpicklistvalfetchmodel();
	}
	//check box value get
	public function checkboxvalueget(){
		$this->Workflowmanagementmodel->checkboxvaluegetmodel();
	}
	//call alert from number fetch based on number type
	public function callnumberfetch() {
		$this->Workflowmanagementmodel->callnumberfetchmodel();
	}
	//call alert template fetch
	public function calltemplatefetch() {
		$this->Workflowmanagementmodel->calltemplatefetchmodel();
	}
	//new work flow create
	public function newworkflowcreate() {
		$this->Workflowmanagementmodel->newworkflowcreatemodel();
	}
	//work flow active
	public function activateworkflowdata() {
		$this->Workflowmanagementmodel->activateworkflowdatamodel();
	}
	//work flow inactive
	public function inactivateworkflowdata() {
		$this->Workflowmanagementmodel->inactivateworkflowdatamodel();
	}
	//work flow condition header information fetch
	public function workflowconditioninformationfetch() {
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$modname = $_GET['modulename'];
		$colinfo = $this->Workflowmanagementmodel->workflowconditioninformationfetchmodel($modid);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height);
		} else {
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height);
		}
		echo json_encode($datas);
	}
	//workflow edit details fetch
	public function fetchformdataeditdetails() {
		$this->Workflowmanagementmodel->fetchformdataeditdetailsmodel();
	}
	//workflow update
	public function updateworkflowcreate() {
		$this->Workflowmanagementmodel->updateworkflowcreatemodel();
	}
	//rule creteria grid data fetch
	public function rulecreteriagridfetchfrun() {
		$this->Workflowmanagementmodel->rulecreteriagridfetchfrunmodel();
	}
	//sms sender id value fetch
	public function smssenderddvalfetch() {
		$this->Workflowmanagementmodel->smssenderddvalfetchmodel();
	}
	//For template value fetch
	public function smstemplateddvalfetch() {
		$this->Workflowmanagementmodel->smstemplateddvalfetchmodel();
	}
}