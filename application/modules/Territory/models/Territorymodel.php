<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Territorymodel extends CI_Model{    
	public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//territory create
	public function newdatacreatemodel() {		
		//table and fields information
		$formfieldsname = explode(',',$_POST['territoryelementsname']);
		$formfieldstable = explode(',',$_POST['territoryelementstable']);
		$formfieldscolmname = explode(',',$_POST['territoryelementscolmn']);
		$elementpartable = explode(',',$_POST['territoryelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
		echo 'TRUE';
	}
	//Retrive territory data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['territoryelementsname']);
		$formfieldstable = explode(',',$_GET['territoryelementstable']);
		$formfieldscolmname = explode(',',$_GET['territoryelementscolmn']);
		$elementpartable = explode(',',$_GET['territoryelementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['territoryprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	//territory update
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['territoryelementsname']);
		$formfieldstable = explode(',',$_POST['territoryelementstable']);
		$formfieldscolmname = explode(',',$_POST['territoryelementscolmn']);
		$elementpartable = explode(',',$_POST['territoryelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['territoryprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		echo $result;
	}
	//territory delete
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['territoryelementstable']);
		$parenttable = explode(',',$_GET['territoryparenttable']);
		$id = $_GET['territoryprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				echo "TRUE";
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			echo "TRUE";
		}
	} 	
}