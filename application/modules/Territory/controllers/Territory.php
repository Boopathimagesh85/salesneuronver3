<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Territory extends MX_Controller{
    private $territorymoduleid=19;
	public function __construct() {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Territory/Territorymodel');	
    }
    //first basic hitting view
    public function index() {        
    	$moduleid = array($this->territorymoduleid);
    	sessionchecker($moduleid);
		//action
		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Territory/territoryview',$data);
	}
	//create territory
	public function newdatacreate() {
    	$this->Territorymodel->newdatacreatemodel();
    }
	//information fetchterritory
	public function fetchformdataeditdetails() {
		$this->Territorymodel->informationfetchmodel($this->territorymoduleid);
	}
	//update territory
    public function datainformationupdate() {
        $this->Territorymodel->datainformationupdatemodel();
    }
	//delete territory
    public function deleteinformationdata() {  
		$this->Territorymodel->deleteoldinformation($this->territorymoduleid);
    } 
}