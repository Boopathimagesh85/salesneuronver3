<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Productaddon extends MX_Controller{
    public function __construct(){
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Productaddon/Productaddonmodel');	
		$this->load->model('Itemtag/Itemtagmodel');	
    }
    //first basic hitting view
    public function index() {
    	$moduleid = array(57);
    	sessionchecker($moduleid);
		//action
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		/* Product Addon
			*/
			$data['purity']=$this->Basefunctions->purity_groupdropdown();	
			$data['planstatus'] = $this->Basefunctions->planid;
			//counter-license.
			$data['mainlicense'] = array(
											'counter'=>$this->Basefunctions->get_company_settings('counter'),
					'chargedataid'=>$this->Basefunctions->get_company_settings('chargeid'),
					'purchasechargedataid'=>$this->Basefunctions->get_company_settings('purchasechargeid'),
					'chargecalcoption'=>$this->Basefunctions->get_company_settings('chargecalcoption'),
					'chargecalculation'=>$this->Basefunctions->get_company_settings('chargecalculation'),
					'productidshow'=>$this->Basefunctions->get_company_settings('productidshow'),
					'taxapplicable'=>$this->Basefunctions->get_company_settings('taxapplicable'),
					'businesschargeid'=>$this->Basefunctions->get_company_settings('businesschargeid'),
					'businesspurchasechargeid'=>$this->Basefunctions->get_company_settings('businesspurchasechargeid'),
					'stone'=>$this->Basefunctions->get_company_settings('stone'),
					'purchasewastage'=>$this->Basefunctions->get_company_settings('purchasewastage')
										); 
			$data['accounttype'] = $this->Basefunctions->simpledropdownwithcond('accounttypeid','accounttypename','accounttype','accounttypeid','16,6');
			$data['accountgroup']=$this->Basefunctions->simpledropdown_var('accountgroup','accountgroupid,accountgroupname,accounttypeid','accountgroupname');
			$data['weight_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
			$data['melting_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',4); //melting round-off
			$data['amount_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); 
			$data['jewelstatus']=1;
			$data['product'] = $this->Itemtagmodel->product_dd();
			$data['purity'] = $this->Basefunctions->purity_groupdropdown();
			$data['categoryid'] = $this->Basefunctions->get_company_settings('categoryid');
			$data['categoryname'] = $this->Basefunctions->singlefieldfetch('categoryname','categoryid','category',$data['categoryid']);
			$data['category']=$this->Basefunctions->simpledropdown('category','categoryid,categoryname','categoryname');
			$data['mode'] = $this->Basefunctions->simpledropdownwithcond('salestransactiontypeid','salestransactiontypename','salestransactiontype','salestransactiontypeid','9,11');
		$this->load->view('Productaddon/productaddonview',$data);
	}
	public function createproductaddon() 
	{  
    	$this->Productaddonmodel->createproductaddon();
    }
	/**	*delete product addon    */
	public function productchargedelete() 
	{  
    	$this->Productaddonmodel->productchargedelete();
    }
	/**	*retrieve product addon    */
	public function productchargeretrieve() 
	{  
    	$this->Productaddonmodel->productchargeretrieve();
    }
	/**	*update product addon    */
	public function productchargeupdate() 
	{  
    	$this->Productaddonmodel->productchargeupdate();
    }
    public function gridinformationfetch() {
    	$primarytable = $_GET['maintabinfo'];
    	$primaryid = $_GET['primaryid'];
    	$pagenum = $_GET['page'];
    	$rowscount = $_GET['records'];
    	$width = $_GET['width'];
    	$height = $_GET['height'];
    	$modid = $_GET['moduleid'];
    	$account_id=$_GET['filter'];
    
    	$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'productcharge.productchargeid') : 'productcharge.productchargeid';
    	$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
        $purchasewastagestatus = $this->Basefunctions->get_company_settings('purchasewastage');    
    	if($purchasewastagestatus == 'Yes') {
			$colinfo = array('colmodelname'=>array('categoryname','productname','purityname','accounttypename','accountgroupname','startweight','endweight','chargewastageweight','chargedetails','salestransactiontypeid'),'colmodelindex'=>array('category.categoryname','product.productname','purity.purityname','accounttype.accounttypename','accountgroup.accountgroupname','productcharge.chargedetails','productcharge.startweight','productcharge.endweight','productcharge.chargewastageweight','productcharge.salestransactiontypeid'),'coltablename'=>array('category','product','purity','accounttype','accountgroup','productcharge','productcharge','productcharge','salestransactiontype'),'uitype'=>array('19','19','19','17','19','2','2','2','2','19'),'colname'=>array('Category','Product','Purity','Account Type','Account group','Start Wt','End Wt','Wst wt','Charge Details','Mode'),'colsize'=>array('150','150','115','158','170','126','100','115','310','150'));
		}else{
			$colinfo = array('colmodelname'=>array('categoryname','productname','purityname','accounttypename','accountgroupname','startweight','endweight','chargedetails','salestransactiontypeid'),'colmodelindex'=>array('category.categoryname','product.productname','purity.purityname','accounttype.accounttypename','accountgroup.accountgroupname','productcharge.chargedetails','productcharge.startweight','productcharge.endweight','productcharge.salestransactiontypeid'),'coltablename'=>array('category','product','purity','accounttype','accountgroup','productcharge','productcharge','salestransactiontype'),'uitype'=>array('19','19','19','17','19','2','2','2','19'),'colname'=>array('Category','Product','Purity','Account Type','Account group','Start Wt','End Wt','Charge Details','Mode'),'colsize'=>array('150','150','115','158','170','126','115','310','150'));
		}
		$filterid = isset($_GET['filter']) ? $_GET['filter'] : '';
		$conditionname = isset($_GET['conditionname']) ? $_GET['conditionname'] : '';
		$filtervalue = isset($_GET['filtervalue']) ? $_GET['filtervalue'] : '';
		$filter = array('0'=>$filterid,'1'=>$conditionname,'2'=>$filtervalue);
    	$result=$this->Productaddonmodel->viewdynamicdatainfofetch($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$account_id,$filter);
    	$device = $this->Basefunctions->deviceinfo();
    	//if($device=='phone') {
    		//$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'producttype',$width,$height);
    	//} else {
    		$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'producttype',$width,$height);
    //	}
    	echo json_encode($datas);
    }
    public function getmoduleid(){
    	$modulearray = array(9,89,90,98);
    	$data=$this->db->select("moduleid")->from("module")->where_in('moduleid',$modulearray)->where('status',1)->limit(1)->get();
    	foreach($data->result() as $info){
    		$moduleid = $info->moduleid;
    	}
    	echo $moduleid;
    }
	 public function loadproduct()
    {   
	     $data=array();
    	$productdetails=$this->Basefunctions->product_dd();
		foreach($productdetails->result()as $row) {
								$data[]=array('productid'=>$row->productid,'productname'=>$row->productname,'counterid'=>$row->counterid,'purityid'=>$row->purityid,'chargeid'=>$row->chargeid,'purchasechargeid'=>$row->purchasechargeid);

			}
			echo json_encode($data);
    }
    //parent account name fetch
    public function parentproductnamefetch() {
    	$this->Productaddonmodel->parentproductnamefetchmodel();
    }
    public function loadchargedproduct()
    {
    	$this->Productaddonmodel->loadchargedproduct();
    }
    //product charge grid data fetch
    public function productchargecopygridgirdheaderinformationfetch() {
    	$width = $_GET['width'];
    	$height = $_GET['height'];
    	$modname = $_GET['modulename'];
    	$moduleid = $_GET['moduleid'];
    	$checkbox = 'true';
    	$colinfo = $this->Productaddonmodel->productchargecopyheaderinformationfetchmodel($moduleid);
    	$device = $this->Basefunctions->deviceinfo();
    	//if($device=='phone') {
    		///$datas = mobileviewformtogriddatagenerate($colinfo,$modname,$width,$height);
    	//} else {
    		$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height,$checkbox);
    	//}
    	echo json_encode($datas);
    }
    //product charge grid overlay fetch
    public function chargeproductgriddatafetch() {
    	$this->Productaddonmodel->chargeproductgriddatafetchmodel();
    }
    public function productchargecopy() {
    	$this->Productaddonmodel->productchargecopymodel();
    }
    public function chargedetails() {
    	$this->Productaddonmodel->chargedetails();
    }
	// get max charge details
	public function getmaxcharge() {
    	$this->Productaddonmodel->getmaxcharge();
    }
	// check product charge combination 
	public function checkproductaddon() {
		$this->Productaddonmodel->checkproductaddon();
	}
}