<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('Base/headerfiles'); ?>
</head>
<body>
<?php
	$dataset['gridenable'] = "1"; //0-no  1-yes
	$dataset['griddisplayid'] = "productaddonformdiv"; //add form-div id
	$dataset['maingridtitle'] = $gridtitle['title'];   // form header
	$dataset['gridtitle'] = $gridtitle['title'];
	$dataset['titleicon'] = $gridtitle['titleicon'];  //grid header
	$dataset['spanattr'] = array();     //
	$dataset['gridtableid'] = "producttypegrid"; //grid id
	$dataset['griddivid'] = "producttypegridnav"; //grid pagination
	$this->load->view('productaddonform',$dataset);
	$device = $this->Basefunctions->deviceinfo();
	if($device=='phone') {
		$this->load->view('Base/overlaymobile');
	} else {
		$this->load->view('Base/overlay');
	}
	$this->load->view('Base/modulelist');
?>	
</body>
	<div class="large-12 columns" style="position:absolute;"><div class="overlay" id="viewdeleteoverlayconform" style="z-index:120;"></div></div>
	<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js Files -->	
	<script src="<?php echo base_url();?>js/Productaddon/productaddon.js" type="text/javascript"></script> 
</html>