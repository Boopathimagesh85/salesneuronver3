<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<div id="productaddonview" class="gridviewdivforsh">
				<?php
					$loggedinuserroleid = $this->Basefunctions->userroleid;
					$dataset['gridtitle'] = $gridtitle;
					$dataset['titleicon'] = $titleicon;
					$dataset['modtabgrp'] = $modtabgrp;
					$dataset['gridid'] = 'producttypegrid';
					$dataset['gridwidth'] = 'producttypegridwidth';
					$dataset['gridfooter'] = 'producttypegridfooter';
					$dataset['viewtype'] = 'disable';
					$dataset['moduleid'] = '57';
					$this->load->view('Base/singlemainviewformwithgrid.php',$dataset);
					$defaultrecordview = $this->Basefunctions->get_company_settings('mainviewdefaultview');
					echo hidden('mainviewdefaultview',$defaultrecordview);
				?>
			</div>
		<div id="productaddonformdiv" class="singlesectionaddform">
		<input type ="hidden" id="amount_round" name="amount_round" value="<?php echo $amount_round; ?>">
		<input type ="hidden" id="weight_round" name="weight_round" value="<?php echo $weight_round; ?>">
		<input type ="hidden" id="purchasewastagestatus" name="purchasewastagestatus" value="<?php echo $mainlicense['purchasewastage']; ?>">
		<input type ="hidden" id="chargecalcoption" name="chargecalcoption" value="<?php echo $mainlicense['chargecalcoption']; ?>">
		<input type ="hidden" id="chargecalculation" name="chargecalculation" value="<?php echo $mainlicense['chargecalculation']; ?>">
		<div class="large-12 columns addformunderheader">&nbsp;</div>
		<div class="large-12 columns salesaddformcontainer">
			<div class="row mblhidedisplay">&nbsp;</div>
			<div id="productaddoncharge" class="hiddensubform">
				<div class="">
					<div class="closed effectbox-overlay effectbox-default" id="groupsectionoverlay">
					<form method="POST" name="productaddonform" class="productaddonform"  id="productaddonform" style="height: 100% !important;">
						<div id="productaddonvalidate" class="validationEngineContainer" style="height: 100% !important;">			
						<div id="productupdateonvalidate" class="validationEngineContainer" style="height: 100% !important;">			
							<div class="large-4 large-offset-8 columns" style="padding-right:0px !important;height:100% !important;">	
								<div class="large-12 columns cleardataform border-modal-8px" style="padding-left: 0 !important; padding-right: 0 !important;">
								<div class="large-12 columns sectionheaderformcaptionstyle">Product Addon</div>
									<div class="large-12 columns sectionpanel">
									<div class="static-field large-8 columns ">
										<label>Product Name<span class="mandatoryfildclass" id="productid_req">*</span></label>								
										<select id="chargeproductid" name="chargeproductid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="100">
											<option value="" data-purityid="0"></option>
											<?php foreach($product->result() as $key):
											?>
											<option value="<?php echo $key->productid;?>" data-counterid="<?php echo $key->counterid;?>" data-productname="<?php echo $key->productname;?>" data-purityid="<?php echo $key->purityid;?>" data-chargeid="<?php echo $key->chargeid;?>" data-purchasechargeid="<?php echo $key->purchasechargeid;?>"> <?php if($mainlicense['productidshow'] == 'YES') { echo $key->productid.' - '.$key->productname; } else { echo $key->productname;} ?></option>
											<?php endforeach;?>
										</select>
									</div>
									<div class="static-field large-4 columns ">
										<label>Purity<span class="mandatoryfildclass" id="purityid_req">*</span></label>
										<select id="addon_purityid" name="addon_purityid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="102">
											<option value=""></option>
											<?php $prev = ' ';
											foreach($purity->result() as $key):
											$cur = $key->metalid;
											$a="";
											if($prev != $cur )
											{
												if($prev != " ")
												{
													 echo '</optgroup>';
												}
												echo '<optgroup  label="'.$key->metalname.'">';
												$prev = $key->metalid;
											}
											?>
											<option data-melting="<?php echo $key->melting;?>" data-purityname="<?php echo $key->purityname;?>" value="<?php echo $key->purityid;?>"><?php echo $key->purityname;?></option>
											<?php endforeach;?>
										</select>
									</div>
									<div class="static-field large-6 columns hideaccount">
										<label>Account Type<span class="mandatoryfildclass" id="accounttypeid_req">*</span></label>						
										<select id="accounttypeid" name="accounttypeid" class="chzn-select" data-validation-engine="validate[required]" tabindex="103" >
											<?php foreach($accounttype as $key):?>
											<option value="<?php echo $key->accounttypeid;?>" data-label="<?php echo $key->accounttypename;?>">
											<?php echo $key->accounttypename;?></option>
											<?php endforeach;?>		
										</select>
									</div>
									 <div class="static-field large-6 columns hideaccount">
										<label>Account Group<span class="mandatoryfildclass" id="chargeaccountgroup_req">*</span></label>						
										<select class="chzn-select" id="chargeaccountgroup" name="chargeaccountgroup" value="" data-validation-engine="validate[required]" tabindex="104" data-prompt-position="topLeft:14,36">								
											<option class="groupall" data-accountype="1" data-accountgroupname ="ALL" value="1">ALL</option>
											<?php foreach($accountgroup as $key):?>
											<option data-accountype ="<?php echo $key->accounttypeid;?>" data-accountgroupname ="<?php echo $key->accountgroupname;?>" value="<?php echo $key->accountgroupid;?>">
												<?php echo $key->accountgroupname;?></option>
										<?php endforeach;?>	
										</select>							
									</div>
									<div class="static-field large-6 columns modediv hidedisplay">
										<label>Mode<span class="mandatoryfildclass">*</span></label>						
										<select id="salestransactiontypeid" name="salestransactiontypeid" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="105">
										<option value=""></option>
										<?php foreach($mode as $key):
										?>
											<option value="<?php echo $key->salestransactiontypeid;?>" ><?php echo $key->salestransactiontypename;?></option>
											<?php endforeach;?>						
										</select>
									</div>	
									<div class="input-field large-6 columns ">
										<input type="text" class="" id="chargestartweight" name="chargestartweight" value="" tabindex="106" data-validation-engine="validate[required,decval[<?php echo $weight_round?>],custom[number],min[0.001]]" readonly>
										<label for="chargestartweight">Start Net Weight<span class="mandatoryfildclass">*</span></label>
									</div>
									<div class="input-field large-6 columns ">
										<input type="text" class="" data-validategreat="chargestartweight" id="chargeendweight" name="chargeendweight" value="" tabindex="107" data-validation-engine="validate[required,decval[<?php echo $weight_round?>],custom[number],funcCall[checkgreaterval],min[0.001]]">
										<label for="chargeendweight">End Net Weight<span class="mandatoryfildclass">*</span></label>
									</div>
									<div class="input-field large-6 columns hidedisplay" id="chargewastageweightdiv">
										<input type="text" class="" id="chargewastageweight" name="chargewastageweight" value="" tabindex="108" data-validation-engine="">
										<label for="chargeendweight">Wastage Weight<span class="mandatoryfildclass">*</span></label>
									</div>
									<div class="input-field large-6 columns chargedetailsdiv charge3 hidedisplay">
										<input type="text"  chargeid="" keywordcalc="" class="chargedata" keyword="" id="wastage" name="wastage" value="" tabindex="112" >
										<label id="charge3" for="wastage" title="Wastage"><span id="wastagespan" spankeyword="">Wastage</span>
										<?php if($loggedinuserroleid == 2) { ?>
											<i id="wastageiconchange" class="material-icons waves-effect waves-circle addonchargeiconchange" keywordarray="" keywordcalc="">details</i><span class="mandatoryfildclass">*</span>
										<?php } ?>
										</label>
									</div>
									<div class="input-field large-6 columns chargedetailsdiv charge2 hidedisplay">
										<input type="text"  chargeid="" keywordcalc="" class="chargedata" keyword="" id="making" name="making" value="" tabindex="112" >
										<label id="charge2" for="Making" title="Making"><span id="makingspan" spankeyword="">Making</span>
										<?php if($loggedinuserroleid == 2) { ?>
											<i id="makingiconchange" class="material-icons waves-effect waves-circle addonchargeiconchange" keywordarray="">details</i><span class="mandatoryfildclass">*</span>
										<?php } ?>
										</label>
									</div>
									<div class="input-field large-6 columns chargedetailsdiv charge6 hidedisplay">
										<input type="text"  chargeid="" keywordcalc="" class="chargedata" keyword="" id="flat" name="flat" value="" tabindex="112" >
										<label id="charge6" for="Flat" title="Flat"><span id="flatspan" spankeyword="">Flat</span>
										<?php if($loggedinuserroleid == 2) { ?>
											<i id="flaticonchange" class="material-icons waves-effect waves-circle addonchargeiconchange" keywordarray="">details</i><span class="mandatoryfildclass">*</span>
										<?php } ?>
										</label>
									</div>
									<div class="input-field large-6 columns chargedetailsdiv charge4 hidedisplay">
										<input type="text"  chargeid=""  keywordcalc="" class="chargedata" keyword="" id="hallmark" name="hallmark" value="" tabindex="112" >
										<label id="charge4" for="hallmark" title="Hallmark"><span id="hallmarkspan" spankeyword="">Hallmark</span>
										<?php if($loggedinuserroleid == 2) { ?>
											<i id="hallmarkiconchange" class="material-icons waves-effect waves-circle addonchargeiconchange" keywordarray="">details</i><span class="mandatoryfildclass">*</span>
										<?php } ?>
										</label>
									</div>
									<?php if($mainlicense['chargecalcoption'] == 1 || $mainlicense['chargecalcoption'] ==  3 ) { ?>
									<div class="input-field large-6 columns chargedetailsdiv charge5 hidedisplay">
										<input type="text" chargeid="11" minid="cflatmin" maxid="cflatmax" keywordcalc="CC-FT.MIN" class="chargedata" keyword="CC-FT" id="cflatmin" name="cflatmin" value="" tabindex="118" >
										<label for="cflatmin" title="Certification Flat Min">Certification Flat Min<span class="mandatoryfildclass">*</span></label>
									</div>
									<?php } ?>
									<?php if($mainlicense['chargecalcoption'] == 2 || $mainlicense['chargecalcoption'] ==  3 ) { ?>
									<div class="input-field large-6 columns chargedetailsdiv charge5 hidedisplay">
										<input type="text" chargeid="11" minid="cflatmin" maxid="cflatmax" keywordcalc="CC-FT.MAX" class="chargedata max" keyword="CC-FT" id="cflatmax" name="cflatmax" value="" tabindex="118" >
										<label for="cflatmax" title="Certification Flat Max">Certification Flat Max<span class="mandatoryfildclass">*</span></label>
									</div>
									<?php } ?>
									<?php if($mainlicense['chargecalcoption'] == 1 || $mainlicense['chargecalcoption'] ==  3 ) { ?>
									<div class="input-field large-6 columns chargedetailsdiv charge15 hidedisplay">
										<input type="text" chargeid="15" minid="touchmin" maxid="touchmax" keywordcalc="TV.MIN" class="chargedata" keyword="TV" id="touchmin" name="touchmin" value="" tabindex="120" >
										<label for="touchmin" title="Touch Min">Touch Min<span class="mandatoryfildclass">*</span></label>
									</div>
									<?php } ?>
									<?php if($mainlicense['chargecalcoption'] == 2 || $mainlicense['chargecalcoption'] ==  3 ) { ?>
									<div class="input-field large-6 columns chargedetailsdiv charge15 hidedisplay">
										<input type="text" chargeid="15" minid="touchmin" maxid="touchmax" keywordcalc="TV.MAX" class="chargedata" keyword="TV" id="touchmax" name="touchmax" value="" tabindex="120" >
										<label for="touchmax" title="Touch Max">Touch Max<span class="mandatoryfildclass">*</span></label>
									</div>
									<?php } ?>
									<div class="row">&nbsp;</div>
									</div>
									<div class="divider large-12 columns"></div>
									<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">
										<input type ="hidden" id="primaryproductchargeid" name="primaryproductchargeid"/>
										<input type ="hidden" id="editproductid" name="editproductid"/>
										<input type ="hidden" id="producttypesortcolumn" name="producttypesortcolumn"/>
										<input type ="hidden" id="producttypesortorder" name="producttypesortorder"/>
										<input type="button" class="alertbtnyes addkeyboard" id="productchargeadd" name="productchargeadd" value="Save" tabindex="">
										<input type="button" class="alertbtnyes updatekeyboard" id="productchargeupdate" name="" value="Save" tabindex="" style="display: none;">
										<input type="button" class="alertbtnno addsectionclose"  value="Close" name="cancel" tabindex="">
									</div>
								</div>
							</div>
						</div>
						</div>
					</form>
					</div>
				</div>
		   </div>
		</div>
	</div>
		</div>			
	</div>
</div>
<!----Product charge copy overlay----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="productaddoncopyalertsdouble" style="overflow-y: auto;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="large-8 medium-8 small-11 large-centered medium-centered small-centered columns borderstyle" >
			<form method="POST" name="billnumberoverlayform" style="" id="billnumberoverlayform" action="" enctype="" class=" clearformbillnumberoverlay">
				<span id="billnumberoverlayvalidation" class="validationEngineContainer"> 
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 
						columns sectionheaderformcaptionstyle">
							Product Details
						</div>
						<div class="large-12 columns" style="background-color: #fff;color:black;">
						<div class="static-field large-4 columns">
							<label>Copy From<span class="mandatoryfildclass"></span></label>								
							<select id="chargedproduct" name="chargedproduct" class="chzn-select" data-prompt-position="topLeft:14,36" tabindex="100">
								<option value=""></option>
								
							</select>
						</div>
						<div class="static-field large-4 columns">
							<label>Copy To<span class="mandatoryfildclass">*</span></label>
							<select data-validation-engine="validate[required]" class="chzn-select" id="copytopcat" name="copytopcat" data-prompt-position="topLeft:14,36">
									<option value="0">Product</option>
									<option value="1">Category</option>
							</select>
						</div>
						<div class="static-field large-4 columns" id="cateogrydiv">
								<label>Category</label>						
								<select id="clonecategoryid" name="clonecategoryid" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="101">
									<option value=""></option>
									 <?php $prev = ' ';
										foreach($category as $key):
									?> 
									<option data-name="<?php echo $key->categoryname;?>" value="<?php echo $key->categoryid;?>"><?php echo $key->categoryname;?></option>
									<?php  endforeach;?>						
								</select>
						</div>
						</div>
						<div id="productaddonchargegrid">
					<?php
							$device = $this->Basefunctions->deviceinfo();
							if($device=='phone') {
								echo '<div class="large-12 columns paddingzero forgetinggridname" id="productchargecopygridwidth"><div id="productchargecopygrid" class="inner-row-content inner-gridcontent" style="height:300px;">
									<!-- Table header content , data rows & pagination for mobile-->
								</div>
								<!--<footer class="inner-gridfooter footercontainer" id="productchargecopygridfooter">
									 Footer & Pagination content 
								</footer>--></div>';
							} else {
								echo '<div class="large-12 columns forgetinggridname" id="productchargecopygridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="productchargecopygrid" style="max-width:2000px; height:300px;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
							<!--<div class="inner-gridfooter footer-content footercontainer" id="productchargecopygridfooter">
									 Footer & Pagination content 
								</div>--></div>';
							}
					?></div></div>
					<div class="row" style="background:#fff;">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
							<input type="button" id="productchargecopygridsubmit" name="productchargecopygridsubmit" value="Submit" class="alertbtnyes">
							<input type="button" id="productchargeclose" name="productchargeclose" value="Cancel" class="alertbtnno">
						</div>
					</div>
					<!-- hidden fields -->
					<input type="hidden" name="chargebasedproductid" id="chargebasedproductid" />
				</span>
			</form>
		</div>
	</div>
</div>
	<style type="text/css">
@media (min-width:320px) and (max-width:480px) {
.rvcdropdown {
	display:none;
}
.rppdropdown {
	left:30% !important;
}
}
</style>