<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Productaddonmodel extends CI_Model{
    public function __construct(){
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	public function createproductaddon()
	{
		$productid = $_POST['chargeproductid'];
		$purityid =$_POST['multipurityid'];
		$multiaccountgroup=$_POST['multiaccountgroup'];
		$chargecategoryid = '';
		if(isset($_POST['accounttypeid']) && $_POST['accounttypeid']!=''){
			$_POST['accounttypeid']=$_POST['accounttypeid'];
		}
		else{
			$_POST['accounttypeid']=1;
		}
		if(isset($_POST['chargewastageweight'])){
			$_POST['chargewastageweight']=$_POST['chargewastageweight'];
		}
		else{
			$_POST['chargewastageweight']=1;
		}
		$charegiddata = explode(',',$_POST['chargeiddata']);
		$chargedetails = $this->db->select('chargecategoryid')->from('charge')->where_in('chargeid',$charegiddata)->get();
		foreach($chargedetails->result() as $info){
			$chargecategoryid .= $info->chargecategoryid.',';
		}
		$chargecategoryid = rtrim($chargecategoryid,',');
		$chargearray = array(
								'productid'=>$productid,
								'purityid'=>$purityid,
								'accounttypeid'=>$_POST['accounttypeid'],
								'accountgroupid'=>$multiaccountgroup,
								'startweight'=>$_POST['chargestartweight'],
								'endweight'=>$_POST['chargeendweight'],
								'chargedetails'=>$_POST['chargedata'],
								'chargeid'=>$_POST['chargeiddata'],
								'chargecategoryid'=>$chargecategoryid,
								'chargeamount'=>$_POST['chargevalues'],
								'salestransactiontypeid'=>$_POST['salestransactiontypeid'],
								'chargewastageweight'=>$_POST['chargewastageweight'],
								'industryid'=>$this->Basefunctions->industryid
						);
						$chargearray = array_merge($chargearray,$this->Crudmodel->defaultvalueget());
						$this->db->insert('productcharge',array_filter($chargearray));
						$primaryid = $this->db->insert_id();
						//audit-log
						$user = $this->Basefunctions->username;
						$userid = $this->Basefunctions->logemployeeid;
						$activity = ''.$user.' created ProductCharge';
						$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,9);
						echo 'SUCCESS';
	}
	/**	*productchargedelete	*/
	public function productchargedelete()
	{
		$primaryid = array_filter(explode(',',$_GET['primaryid']));
		if(count($primaryid) > 0)
		{
			    	$this->db->where_in('productchargeid',$primaryid);
					$this->db->update('productcharge',$this->Basefunctions->delete_log());
					//audit log
					$user = $this->Basefunctions->username;
					$userid = $this->Basefunctions->logemployeeid;
					$activity = ''.$user.' deleted ProductCharge';	
					$this->Basefunctions->notificationcontentadd(1,'Deleted',$activity ,$userid,9);
				
		}
		echo 'SUCCESS';
	}
	public function productchargeretrieve()
	{
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2);
		$primary_id = $_GET['primaryid'];
				$this->db->select('product.productname,ROUND(productcharge.startweight,'.$round.') as startweight,ROUND(productcharge.endweight,'.$round.') as endweight,productcharge.productchargeid,productcharge.accountgroupid,productcharge.purityid,productcharge.productid,productcharge.accounttypeid,productcharge.chargedetails,productcharge.salestransactiontypeid,productcharge.chargewastageweight',false);
				$this->db->from('productcharge');
				$this->db->join('product','product.productid=productcharge.productid');		
				$this->db->where('productcharge.productchargeid',$primary_id);				
				$this->db->limit(1);				
				$info=$this->db->get()->row();			
				
				$jsonarray=array(
							'primaryproductchargeid' => $info->productchargeid,
							'editproductid' => $info->productid,
							'chargeproductid' => $info->productid,
							'chargeproductname' => $info->productname,
						    'accounttypeid'=> $info->accounttypeid,
							'chargeaccountgroup'=> $info->accountgroupid,
							'addon_purityid' => $info->purityid,
							'chargestartweight' =>($info->startweight +0),							
							'chargeendweight' =>($info->endweight +0),
							'chargedetails' => $info->chargedetails,
						    'salestransactiontypeid' => $info->salestransactiontypeid,
						    'chargewastageweight' => $info->chargewastageweight
						);
				echo json_encode($jsonarray);
	}
	/**	*create product addon (ROL/CHARGE/DISCOUNT)	*/
	public function productchargeupdate()
	{
		$chargecategoryid = '';
		$primaryid = $_POST['primaryproductchargeid'];
		$productid = trim($_POST['chargeproductid']);	
		if(isset($_POST['chargewastageweight'])){
			$_POST['chargewastageweight']=$_POST['chargewastageweight'];
		}
		else{
			$_POST['chargewastageweight']=1;
		}	
		$charegiddata = explode(',',$_POST['chargeiddata']);
		$chargedetails = $this->db->select('chargecategoryid')->from('charge')->where_in('chargeid',$charegiddata)->get();
		foreach($chargedetails->result() as $info){
			$chargecategoryid .= $info->chargecategoryid.',';
		}
		$chargecategoryid = rtrim($chargecategoryid,',');
		$multiaccountgroup=$_POST['multiaccountgroup'];
		$chargearray = array(
								'productid' => $productid,
								'purityid' => $_POST['multipurityid'],
				                'accounttypeid'=>$_POST['accounttypeid'],
								'accountgroupid'=>$multiaccountgroup,
								'startweight'=>$_POST['chargestartweight'],
								'endweight'=>$_POST['chargeendweight'],
								'chargeid'=>$_POST['chargeiddata'],
								'chargecategoryid'=>$chargecategoryid,
								'chargeamount'=>$_POST['chargevalues'],
								'chargedetails'=>$_POST['chargedata'],
								'salestransactiontypeid'=>$_POST['salestransactiontypeid'],
								'chargewastageweight'=>$_POST['chargewastageweight'],
								'industryid'=>$this->Basefunctions->industryid
							);
		$chargearray = array_merge($chargearray,$this->Crudmodel->defaultvalueget());
		$this->db->where_in('productchargeid',$primaryid);
		$this->db->update('productcharge',array_filter($chargearray));
		
         echo 'SUCCESS';
				
	}
	public function getproductbasedpurity($productid)
	{
		$this->db->select('product.purityid as purityid');
		$this->db->from('product');
		$this->db->where_in('product.productid',$productid);
		$this->db->where('product.industryid',$this->Basefunctions->industryid);
		$this->db->where('product.status',$this->Basefunctions->activestatus);
		$result=$this->db->get()->row();
		return $result->purityid;
	}
	public function viewdynamicdatainfofetch($tablename,$sortcol,$sortord,$pagenum,$rowscount,$accountid,$filter=array()) {
		$round =$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2);//weight round-off
		$filtervalarray =array();
		if(!empty($filter)) {
			$fconditonid = explode('|',$filter[0]);
			$filtercondition = explode('|',$filter[1]);
			$filtervalue = explode('|',$filter[2]);
		} else {
			$fconditonid = array();
			$filtercondition = array();
			$filtervalue = array();
		}
		$filtervalarray = $this->Basefunctions->filtergenerateconditioninfo($fconditonid,$filtercondition,$filtervalue);
		if(count($filtervalarray) > 0) {
			$filterwherecondtion = $this->Basefunctions->filtergeneratewhereclause($filtervalarray['filterarray'],'');
			if($filterwherecondtion == '') {
				$filterwherecondtion = " AND 1=1";
			} else {
				$filterwherecondtion = " AND ".$filterwherecondtion;
			}
		} else {
			$filterwherecondtion = " AND 1=1";
		}
		$dataset ='productcharge.productchargeid,ROUND(productcharge.startweight,'.$round.') as startweight,ROUND(productcharge.endweight,'.$round.') as endweight,product.productname,purity.purityname,accounttype.accounttypename,productcharge.accounttypeid,productcharge.accountgroupid,productcharge.chargedetails,accountgroup.accountgroupname,salestransactiontype.salestransactiontypename,productcharge.chargewastageweight,category.categoryname';
		$join=' LEFT OUTER JOIN product ON product.productid=productcharge.productid';
		$join.=' LEFT OUTER JOIN category ON category.categoryid=product.categoryid';
		$join.=' LEFT OUTER JOIN purity ON purity.purityid=productcharge.purityid';
		$join.=' LEFT OUTER JOIN accounttype ON accounttype.accounttypeid=productcharge.accounttypeid';
		$join.=' LEFT OUTER JOIN accountgroup ON accountgroup.accountgroupid=productcharge.accountgroupid';
		$join.=' LEFT OUTER JOIN salestransactiontype ON salestransactiontype.salestransactiontypeid=productcharge.salestransactiontypeid';
		$cuscondition=$tablename.'.industryid ='.$this->Basefunctions->industryid.' ';
		$status=$tablename.'.status='.$this->Basefunctions->activestatus.'';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		/* query */
		$result = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$cuscondition.' '.$filterwherecondtion.' AND '. $status.' ORDER BY '.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$data=array();
		$i=0;
		foreach($result->result() as $row) {
			$productchargeid = $row->productchargeid;
			$productname =$row->productname;
			$purityname =$row->purityname;
			$startweight =$row->startweight;
			$endweight =$row->endweight;
			$chargewastageweight =$row->chargewastageweight;
			// check group all or not
			if($row->accountgroupid == 1) {
				$accountgroupname='ALL';
			} else {
				$accountgroupname=$row->accountgroupname;
			}
			if($row->accounttypeid == 1) {
				$accounttypename='ALL';
			} else {
				$accounttypename=$row->accounttypename;
			}
			$chargedetails =$row->chargedetails;
			$salestransactiontypename =$row->salestransactiontypename;
			$categoryname =$row->categoryname;
			$purchasewastagestatus = $this->Basefunctions->get_company_settings('purchasewastage');    
	    	if($purchasewastagestatus == 'Yes') {
	    		$data[$i] = array('id'=>$productchargeid,$categoryname,$productname,$purityname,$accounttypename,$accountgroupname,$startweight,$endweight,$chargewastageweight,$chargedetails,$salestransactiontypename);
			} else {
				$data[$i] = array('id'=>$productchargeid,$categoryname,$productname,$purityname,$accounttypename,$accountgroupname,$startweight,$endweight,$chargedetails,$salestransactiontypename);
			}
			$i++;
		}
		$finalresult=array('0'=>$data,'1'=>$page,'2'=>'json');
		return $finalresult;
	}
	//parent account name fetch
	public function parentproductnamefetchmodel() {
		$id=$_GET['id'];
		$data=$this->db->select('category.categoryid,category.categoryname')
		->from('product')
		->join('category','category.categoryid=product.categoryid')
		->where('product.productid',$id)
		->where('category.status',1)
		->get();
		if($data->num_rows() >0) {
			foreach($data->result() as $in)	{
				$parentcategoryid=$in->categoryid;
				$categoryname=$in->categoryname;
			}
			$a = array('parentcategoryid'=>$parentcategoryid,'categoryname'=>$categoryname);
		} else {
			$a = array('parentcategoryid'=>'0','categoryname'=>'');
		}
		echo json_encode($a);
	}
	public function loadchargedproduct()
	{
		$result=array();
		$data = $this->db->select('productcharge.productid,product.chargeid,product.productname,product.purityid')
		->from('productcharge')
		->join('product','product.productid=productcharge.productid','left')
		->where('productcharge.status',1)
		->where('product.status',1)
		->group_by('product.productid')
		->get();
		 if($data->num_rows() >0) {
			foreach($data->result() as $row) {
				$result[]=array('productid'=>$row->productid,'productname'=>$row->productname,'chargeid'=>$row->chargeid,'purityid'=>$row->purityid);
			}
		}else{
		   $result ='';
		}
		echo json_encode($result);
	}
	//bill bumber grid data load
	public function productchargecopyheaderinformationfetchmodel($moduleid) {
		$fnames = array('productid','categoryid','categoryname','productname');
		$flabels = array('ProductId','Categoryid','Category','Product');
		$colmodnames = array('productid','categoryid','categoryname','productname');
		$colindexnames = array('product','category','category','product');
		$uitypes = array('2','2','2','2');
		$viewtypes = array('0','0','1','1');
		$dduitypeids = array('17','18','20','19','23','25','26','27','28','29');
		$data = array();
		$i=0;
		$j=0;
		foreach($fnames as $fname) {
			if( in_array($uitypes[$j],$dduitypeids) ) {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j].'name';
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]='0';
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]='2';
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			} else {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			}
			$j++;
		}
		return $data;
	}
	//product charge grid data load
	public function chargeproductgriddatafetchmodel() {
		$chargeid = $_POST['chargeid'];
		$purityid = $_POST['purityid'];
		$productid = $_POST['productid'];
		$charegdetail = '';
		$j=1;
		$data=$this->db->select('category.categoryname,category.categoryid,product.productid,product.productname')
		->from('product')
		->join('category','category.categoryid=product.categoryid','left outer')
		->where_in('product.purityid',$purityid)
		->where_in('product.chargeid',$chargeid)
		->where_not_in('product.productid',array($productid))
		->where('product.status',$this->Basefunctions->activestatus)
		->get();
		if($data->num_rows()>0){
		foreach($data->result() as $value){
			$charegdetail->rows[$j]['id']=$value->productid;
			$charegdetail->rows[$j]['cell']=array(
					$value->productid,
					$value->categoryid,
					$value->categoryname,
					$value->productname
				);
			$j++;
		}
		}else{
			$charegdetail = '';
		}
		echo  json_encode($charegdetail);
	}
	public function productchargecopymodel(){
		$mainproductid = $_POST['mainproductid'];
		$subproductid = $_POST['subproductid'];
		$clonecategoryid = $_POST['clonecategoryid'];
		$copytopcat = $_POST['copytopcat'];			
		$arraysubproduct = explode(',',$subproductid);
		if($copytopcat == 1){
			$this->Basefunctions->updatechargecloneforchild($clonecategoryid,$mainproductid); 
			$update_last=array(
				'parentproductid'=>$mainproductid
			);
			$this->db->where('categoryid',$clonecategoryid);
			$this->db->update('category',$update_last);	
			
		}else if($copytopcat == 0){
			foreach($arraysubproduct as $subproduct){
				$update_last=array(
				'parentproductid'=>$mainproductid
				);
				$this->db->where('productid',$subproduct);
				$this->db->update('product',$update_last);	
			}
		}
		// inactive exising product charge for selected product
		$delete_lot = $this->Basefunctions->delete_log();
		$this->db->where_in('productid',$arraysubproduct);
		$this->db->update('productcharge',$delete_lot);
		/* // get product charge combination for main product
		$data =$this->db->select('productcharge.accountgroupid,productcharge.accounttypeid,productcharge.purityid,productcharge.startweight,productcharge.endweight,productcharge.chargeamount,productcharge.chargeid,productcharge.chargecategoryid,productcharge.chargeprintkeyword,productcharge.chargedetails,productcharge.salestransactiontypeid,productcharge.chargewastageweight')
		->from('productcharge')
		->where_in('productcharge.productid',$mainproductid)
		->where('productcharge.status',$this->Basefunctions->activestatus)
		->get();
		$i = 0;
		$j = 0;
		foreach($data->result() as $value){
			foreach($arraysubproduct as $subproduct){
				$chargearray = array(
						'productid'=>$subproduct,
						'purityid'=>$value->purityid,
						'accounttypeid'=>$value->accounttypeid,
						'accountgroupid'=>$value->accountgroupid,
						'startweight'=>$value->startweight,
						'endweight'=>$value->endweight,
						'chargedetails'=>$value->chargedetails,
						'chargeid'=>$value->chargeid,
						'chargecategoryid'=>$value->chargecategoryid,
						'chargeprintkeyword'=>$value->chargeprintkeyword,
						'chargeamount'=>$value->chargeamount,
						'salestransactiontypeid'=>$value->salestransactiontypeid,
						'chargewastageweight'=>$value->chargewastageweight,
						'industryid'=>$this->Basefunctions->industryid
				);
				$chargearray = array_merge($chargearray,$this->Crudmodel->defaultvalueget());
				$this->db->insert('productcharge',array_filter($chargearray));
				$primaryid = $this->db->insert_id();
				$j++;
			}
			$i++;
		} */
		echo 'SUCCESS';
	}
	public function chargedetails() {
		$chargeid = $_POST['chargeid'];
		$data=$this->db->query("select GROUP_CONCAT(chargecategoryid,':',chargename,':',chargekeyword,':',chargeid) as chargekeyword
				FROM charge
				WHERE chargeid IN ($chargeid)"
				);
		foreach($data->result() as $info) {
			$charge_array= array(
					'chargekeyword'=>$info->chargekeyword
			);
		}
		echo json_encode($charge_array);
	}
	public function getmaxcharge()
	{
		$purityid=$_POST['purityid'];
		$productid=$_POST['productid'];
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2);
		$accountgroupid=$_POST['accountgroupid'];
		$accounttypeid=$_POST['accounttypeid'];
		$startrange = 0;
		if($round == 1)
		{
			$startrange = 0.1;
		}
		else if($round == 2)
		{
			$startrange = 0.01;
		}
		else if($round == 3)
		{
			$startrange = 0.001;
		}
		else if($round == 4)
		{
			$startrange = 0.0001;
		}
		$table = 'productcharge';
		$primaryid = $table.'id';
      	$this->db->select_max('endweight');
      	$this->db->from($table);
		$this->db->where('purityid',$purityid);
		$this->db->where('productid',$productid);
		$this->db->where('productcharge.industryid',$this->Basefunctions->industryid);
		$this->db->where('productcharge.accounttypeid',$accounttypeid);
		$this->db->where('productcharge.accountgroupid',$accountgroupid);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$res=$this->db->get();
		if($res->num_rows() > 0) {
			$data['endweight'] =$res->row()->endweight+$startrange;
		} else {
			$data['endweight'] = $startrange;
		}
		echo json_encode($data);
	}
	public function checkproductaddon() {
		$datarowid = $_POST['datarowid'];
		$table = 'productcharge';
		$this->db->select('productid,purityid,accounttypeid,endweight');
      	$this->db->from($table);
		$this->db->where('productchargeid',$datarowid);
		$checkres=$this->db->get();
		$productid = $checkres->row()->productid;
		$purityid = $checkres->row()->purityid;
		$accounttypeid = $checkres->row()->accounttypeid;
		$endweight = $checkres->row()->endweight;
		$primaryid = $table.'id';
      	$this->db->select('endweight');
      	$this->db->from($table);
		$this->db->where('purityid',$purityid);
		$this->db->where('productid',$productid);
		$this->db->where('endweight >',$endweight);
		$this->db->where('productcharge.industryid',$this->Basefunctions->industryid);
		$this->db->where('productcharge.accounttypeid',$accounttypeid);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$res=$this->db->get();
		if($res->num_rows() > 0) {
			$count = $res->num_rows();
		} else {
			$count = 0;
		}
		echo json_encode($count);
	}
	
}