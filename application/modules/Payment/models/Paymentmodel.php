<?php
class Paymentmodel extends CI_Model{
	function __construct() {
		parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
	}
	public function gettransactionnumbers() {
		$i =0;
		$moduleid = trim($_GET['moduleid']);
		$modulename = strtolower(trim($_GET['modulename']));
		$fieldid = $modulename.'id';
		$fieldnumber = $modulename.'number';
		$this->db->select($fieldnumber.' AS number ,'.$fieldid);
		$this->db->from($modulename);
		$this->db->where('balanceamount >',0);
		$this->db->where($modulename.'.'.'status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$data[$i] = array('id'=>$row->$fieldid,'number'=>$row->number);
				$i++;
			}
			echo json_encode($data); 			
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}
	}
	//payment create
	public function newdatacreatemodel() {	
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		$moduleid = trim($_POST['moduleid']);
		$transactionid = trim($_POST['transactionid']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$defdataarr = $this->Crudmodel->defaultvalueget();
		$industry = array('industryid'=>$this->Basefunctions->industryid,
			'branchid'=>$this->Basefunctions->branchid);
		$defdataarr = array_merge($industry,$defdataarr);	
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);	
		//get the old payment records
		$oldinvoicepaymentid=array();
		$deletedpaymentid=array();
		$invoicepayment=$this->db->select('paymentid')
									->from('payment')
									->where('moduleid',$moduleid)
									->where('transactionid',$transactionid)
									->where('status',$this->Basefunctions->activestatus)
									->get();
		foreach($invoicepayment->result() as $info) {
			$oldinvoicepaymentid[]=$info->paymentid;
		}
		//
		$gridfieldpartabname = explode(',',$_POST['griddatapartabnameinfo']);
		$gridrows = explode(',',$_POST['numofrows']);
		$griddata = $_POST['griddatas'];
		$griddatainfo = json_decode($griddata, true);	
		//filter unique grid parent table
		$gridpartablename =  $this->Crudmodel->filtervalue($gridfieldpartabname);			
		for($lm=0;$lm < count($griddatainfo);$lm++) {
			$newpaymentid[]=$griddatainfo[$lm]['paymentid'];
		}
		//find deleted payments
		for($m=0;$m < count ($oldinvoicepaymentid);$m++) {
			if(in_array($oldinvoicepaymentid[$m],$newpaymentid)) {			
			} else {
				$deletedpaymentid[]=$oldinvoicepaymentid[$m];
			}
		}
		if(count($deletedpaymentid) > 0) {
			for($k=0;$k<count($deletedpaymentid);$k++) {
				$this->Crudmodel->outerdeletefunction('payment','paymentid','',$deletedpaymentid[$k]);
			}
		}		
		/*payment detail*/
		$m=0;
		$h=1;
		foreach($gridrows as $rowcount) {
			$gridfieldsname = explode(',',$_POST['gridcolnames'.$h]);
			//filter grid unique fields table
			$gridfieldtabname = explode(',',$_POST['griddatatabnameinfo'.$h]);
			$gridfieldstable = $this->Crudmodel->filtervalue($gridfieldtabname);
			$gridtableinfo = explode(',',$gridfieldstable);
			for($i=0;$i<$rowcount;$i++) {
				foreach( $gridtableinfo as $gdtblname ) {
					${'$gcdata'.$m} = array();
					$t=0;
					foreach($gridfieldsname AS $gdfldsname) {
						if(strcmp( trim($gridfieldtabname[$t]),trim($gdtblname) ) == 0 ) {
							if( isset( $griddatainfo[$i][$gdfldsname] ) ) {
								$name = explode('_',$gdfldsname);
								if( ctype_alpha($griddatainfo[$i][$gdfldsname]) && $griddatainfo[$i][$gdfldsname] != "" ) {									
									${'$gcdata'.$m}[$gdfldsname] = ucwords( $griddatainfo[$i][$gdfldsname] );
								} else if( $griddatainfo[$i][$gdfldsname] != "" ) {									
									${'$gcdata'.$m}[$gdfldsname] = $griddatainfo[$i][$gdfldsname];							
								}
							}
						}
					}
					if(count(${'$gcdata'.$m})>0) {
						$gnewdata = array_merge(${'$gcdata'.$m},$defdataarr);
						//unset the hidden variables
						$removehidden=array('paymentid');						
						unset($gnewdata[$removehidden[0]]);
						if($griddatainfo[$i]['paymentid'] == 0){
								$this->db->insert($gdtblname,array_filter($gnewdata));
						}
						if($griddatainfo[$i]['paymentid'] > 0){
							$this->db->where('paymentid',$griddatainfo[$i]['paymentid']);
							$this->db->update($gdtblname,array_filter($gnewdata));
						}
					}
					$m++;
				}
			}
			$h++;
		}
		/*module summary update*/
		$moduletable=trim(strtolower($_POST['modulename']));
		$summary_data=array('paidamount'=>trim($_POST['paidamount']),'writeoffamount'=>trim($_POST['writeoffamount']),
							'balanceamount'=>trim($_POST['balanceamount']),
							'lastupdatedate'=>date($this->Basefunctions->datef),
							'lastupdateuserid'=>$this->Basefunctions->userid);
		$this->db->where($moduletable.'id',$transactionid);
		$this->db->update($moduletable,$summary_data);
		echo true;
	}
	//Retrieve payment data for edit
	public function informationfetchmodel($primarydataid) {		
		$data=$this->db->select('transactionid,payment.moduleid,paymentid,paymentnumber,paymentdate,paymenttypename,paymentmethodname,paymentamount,payment.paymentreferencenumber,payment.bankname,paymentstatusname,payment.referencedate,paymentdescription,payment.paymentmethodid,payment.paymenttypeid,payment.paymentstatusid,payment.payeename')//payment.paymentto,
					->from('payment')
					->join('paymentmethod','paymentmethod.paymentmethodid=payment.paymentmethodid')
					->join('paymenttype','paymenttype.paymenttypeid=payment.paymenttypeid')
					->join('paymentstatus','paymentstatus.paymentstatusid=payment.paymentstatusid')
					->where('payment.status',$this->Basefunctions->activestatus)
					->where('payment.paymentid',$primarydataid)
					->get()->result();
		$j=0;		
		foreach($data as $value) {			
			if((in_array($value->moduleid, [226,86,95]))){
				$modulename = 'Invoice';
				$tablename = 'invoice';
				$number = 'invoicenumber';
				$id = 'invoiceid'; 
			}	
			$paymentnumber = $this->db->select($number)
							->from($tablename)
							->where($id,$value->transactionid)
							->get()->result();
			foreach ($paymentnumber as $tempnumber){
				$finalnumber = $tempnumber->$number;
			}
			if($value->paymentdescription){
				$description = $value->paymentdescription;
			}else{
				$description = '';
			}
			$paymentdetail->rows[$j]['id']=$value->paymentid;
			$paymentdetail->rows[$j]['cell']=array(
													$modulename,
													$value->moduleid,
													$finalnumber,
													$value->transactionid,
													$value->paymentdate,
													$value->paymenttypename,
													$value->paymenttypeid,
													$value->paymentmethodname,
													$value->paymentmethodid,
													$value->paymentamount,
													$value->paymentreferencenumber,
													$value->paymentstatusname,
													$value->paymentstatusid,
													$value->bankname,													
													$value->referencedate,
													$value->payeename,
													$description,
													$value->paymentid
												);						
			$j++;		
		}
		echo  json_encode($paymentdetail);
	}
	//payment update
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['primarydataid'];
		$restricttable = explode(',',$_POST['resctable']);
		$this->Crudmodel->dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable);
		//notification entry
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
			if(isset($_POST['employeeid'])) {
			$assignid = $_POST['employeeid'];
			$emptypes = $_POST['employeetypeid'];
			$empdataids = array();
			$assignempids = array();
			if($assignid != '') {
				$i = 0;
				$m=0;
				$empiddatas = explode(',',$assignid);
				foreach($empiddatas as $empids) {
					$emptype = $emptypes[$m];
					if($emptype == 1) {
						$empdataids[$i] = $empids;
						$i++;
					} else if($emptype == 2) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromgroup($empids);
						$i++;
					} else if($emptype == 3) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromroles($empids);
						$i++;
					} else if($emptype == 4) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromrolesandsubroles($empids);
						$i++;
					}
					$m++;
				}
			} else {
				$assignid = 1;
			}
		} else {
			$assignid = 1;
		}
		$tickname = $_POST['paymentnumber'];
		if($assignid == '1'){
			$assignto = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$assignid);
			$notimsg = $empname." "."Updated a payment"." - ".$tickname;
			$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,28);
		} else {
			foreach($empdataids as $empidinfo) {
				if(is_array($empidinfo)) { //group of employees
					foreach($empidinfo as $empid) {
						if( !in_array($empid,$assignempids) ) {
							array_push($assignempids,$empid);
							$assigntoemp = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
							$notimsg = $empname." "."Updated a payment"." - ".$tickname." - Assigned To ".$assigntoemp;
							$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$empid,28);
						}
					}
				} else { //individual employees
					if( !in_array($empidinfo,$assignempids) ) {
						array_push($assignempids,$empidinfo);
						$assigntoemp = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empidinfo);
						$notimsg = $empname." "."Updated a payment"." - ".$tickname." - Assigned To ".$assigntoemp;
						$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$empidinfo,28);
					}
				}
			}
		}
		echo 'TRUE';
	}
	//payment delete
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primarydataid'];
		$filename = $this->Basefunctions->generalinformaion('payment','paymentnumber','paymentid',$id);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				//notification log
				$empid = $this->Basefunctions->logemployeeid;
				//deleted file name
				$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
				$notimsg = $empname." "."Deleted a payment "." - ".$filename;
				$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,1,28);
				echo 'TRUE';
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			//notification log
			$empid = $this->Basefunctions->logemployeeid;
			//deleted file name
			$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
			$notimsg = $empname." "."Deleted a payment "." - ".$filename;
			$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,1,28);
			echo 'TRUE';
		}
	}
	//Contact drop down value fetch
	public function contactnamefetchvalmodel() {
		$id =$_GET['accountid'];
		$k=0;
		$this->db->select('contactname,contactid',false);
		$this->db->from('contact');
		$this->db->where('contact.accountid',$id);
		$this->db->where('contact.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0)
		{
			foreach($result->result() as $row)
			{
				$data[$k] = array('id'=>$row->contactid,'name'=>$row->contactname);
				$k++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//account details fetch
	public function accountdetailfetchvalmodel() {
		$id =$_GET['accountid'];
		$this->db->select('mobilenumber,emailid',false);
		$this->db->from('account');
		$this->db->where('account.accountid',$id);
		$this->db->where('account.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0)
		{
			foreach($result->result() as $row)
			{
				$data = array('mobile'=>$row->mobilenumber,'email'=>$row->emailid);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//account name based contact name
	public function fetchcontatdatawithmultiplecondmodel() {
		$i = 0;
		$accid = $_GET['accid'];
		$this->db->select('contactname,contactid',false);
		$this->db->from('contact');
		$this->db->where('contact.accountid',$accid);
		$this->db->where('contact.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				$data[$i] = array('datasid'=>$row->contactid,'dataname'=>$row->contactname); 
			}
			echo json_encode($data);
		}
		else{
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	public function retrievepaymentsummary() {
		$transactionid=trim($_GET['transactionid']);
		$moduleid =trim($_GET['moduleid']);
		$modulename=trim($_GET['modulename']);
		//summary details
		$s_data=$this->db->select('grandtotal,totalpayable,writeoffamount,paidamount,balanceamount')->from($modulename)->where($modulename.'id',$transactionid)->limit(1)->get();
		foreach($s_data->result() as $info){
			$summary=array('grandtotal'=>$info->grandtotal,'totalpayable'=>$info->totalpayable,
							'writeoffamount'=>$info->writeoffamount,'balanceamount'=>$info->balanceamount,'paidamount'=>$info->paidamount);
		}
		echo  json_encode($summary);
	}
	public function retrievepaymentdetail() {
		$transactionid=trim($_GET['transactionid']);
		$moduleid =trim($_GET['moduleid']);		
		$modulename=trim($_GET['modulename']);
		$transactionnumber=trim($_GET['transactionnumber']);		
		//payment details
		$data=$this->db->select('paymentid,paymentnumber,paymentdate,paymenttypename,paymentmethodname,paymentamount,payment.paymentreferencenumber,payment.bankname,paymentstatusname,payment.referencedate,paymentdescription,payment.paymentmethodid,payment.paymenttypeid,payment.paymentstatusid,payment.payeename')//payment.paymentto,
					->from('payment')		
					->join('paymentmethod','paymentmethod.paymentmethodid=payment.paymentmethodid')
					->join('paymenttype','paymenttype.paymenttypeid=payment.paymenttypeid')
					->join('paymentstatus','paymentstatus.paymentstatusid=payment.paymentstatusid')
					->where('payment.moduleid',$moduleid)
					->where('payment.status',$this->Basefunctions->activestatus)
					->where('payment.transactionid',$transactionid)
					->get()->result();
		$j=0;
		foreach($data as $value) {
			if($value->paymentdescription){
				$description = $value->paymentdescription;
			}else{
				$description = '';
			}			
			$productdetail->rows[$j]['id']=$j;
			$productdetail->rows[$j]['cell']=array(													
													$modulename,
													$moduleid,
													$transactionnumber,
													$transactionid,
													$value->paymentdate,
													$value->paymenttypename,
													$value->paymenttypeid,
													$value->paymentmethodname,
													$value->paymentmethodid,
													$value->paymentamount,
													$value->paymentreferencenumber,
													$value->paymentstatusname,
													$value->paymentstatusid,
													$value->bankname,													
													$value->referencedate,
													$value->payeename,													
													$description,
													$value->paymentid
												);						
			$j++;
		}
		if($productdetail != ''){
			echo  json_encode($productdetail);
		}else{
			echo  json_encode(array("fail"=>'FAILED'));
		}
		
	}
}