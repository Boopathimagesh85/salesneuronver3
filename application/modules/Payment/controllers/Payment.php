<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class payment extends MX_Controller{
    //Constructor Function To Load Models
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Payment/Paymentmodel');	
	}
	//Default View 
	public function index() {
		$moduleid = array(28);
		sessionchecker($moduleid);
		//action
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(28);
		$viewmoduleid = array(28);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['dashboradviewdata'] = $this->Basefunctions->dashboarddataviewbydropdown($moduleid);
        $this->load->view('Payment/paymentview',$data);	
	}
	//create Campaign
	public function newdatacreate() {  
    	$this->Paymentmodel->newdatacreatemodel();
    }
	//information fetch
	public function fetchformdataeditdetails() {
		$primarydataid = $_GET['primarydataid'];
		$this->Paymentmodel->informationfetchmodel($primarydataid);
	}
	//update Campaign
    public function datainformationupdate() {
        $this->Paymentmodel->datainformationupdatemodel();
    }
	//delete Campaign
    public function deleteinformationdata() {
        $moduleid = 28;
		$this->Paymentmodel->deleteoldinformation($moduleid);
    }  
	//contact name fetch model
	public function contactnamefetchval() {
		$this->Paymentmodel->contactnamefetchvalmodel();
	}
	public function accountdetailfetchval() {
		$this->Paymentmodel->accountdetailfetchvalmodel();
	}	
	//editer valuefetch editervaluefetch	
	public function editervaluefetch() {
		$filename = $_GET['filename']; 
		$newfilename = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]);
			echo json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			echo json_encode($tccontent);
		}
	}
	//account name based contact name
	public function fetchcontatdatawithmultiplecond() {
		$this->Paymentmodel->fetchcontatdatawithmultiplecondmodel();
	}
	//gettransactionnumbers
	public function gettransactionnumbers() {
		$this->Paymentmodel->gettransactionnumbers();
	}
	//retrievepaymentdetail
	public function retrievepaymentdetail() {
		$this->Paymentmodel->retrievepaymentdetail();
	}
	//retrievepaymentsummary
	public function retrievepaymentsummary() {
		$this->Paymentmodel->retrievepaymentsummary();
	}
}