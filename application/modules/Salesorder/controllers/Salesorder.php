<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Salesorder extends MX_Controller{
	//To load the Register View
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Salesorder/Salesordermodel');
		$this->load->view('Base/formfieldgeneration');
    }
	public function index() {
		$moduleid = array(87,96,217);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['calculationtype']=$this->Basefunctions->simpledropdown('calculationtype','calculationtypeid,calculationtypename','calculationtypename');
		$data['additionalcategory']=$this->Basefunctions->simpledropdown('additionalchargecategory','additionalchargecategoryid,additionalchargecategoryname','additionalchargecategoryname');
		$data['category']=$this->Salesordermodel->taxcategory();
		$data['adjusttype']=$this->Basefunctions->simpledropdown('adjustmenttype','adjustmenttypeid,adjustmenttypename','adjustmenttypename');
		$data['conv_currency']=$this->Basefunctions->simpledropdown('currency','currencyid,currencyname,currencycountry','currencyname');
		$data['dashboradviewdata'] = $this->Basefunctions->dashboarddataviewbydropdown($moduleid);
		$this->load->view('Salesorder/salesorderview',$data);	   
	}
	/*	*Create New salesorder	*/
	public function salesordercreate() {  
    	$this->Salesordermodel->salesordercreatemodel();
    }
	//information fetch
	public function fetchformdataeditdetails() {	
		$moduleid = array(87,96,217);
		$this->Salesordermodel->informationfetchmodel($moduleid);
	}
	//Update old data
    public function datainformationupdate() {
        $this->Salesordermodel->datainformationupdatemodel();
    }
	//delete old information
    public function salesorderdelete() {	
    	$moduleid = array(87,96,217);
        $this->Salesordermodel->salesorderdeletemodel($moduleid);
    }
	//primary and secondary address value fetch
	public function primaryaddressvalfetch() {
		$this->Salesordermodel->primaryaddressvalfetchmodel();
	}		
	//so
	public function getquotenumber() {
		$this->Salesordermodel->getquotenumber();
	}
	public function salesorderdetailfetch() {
		$this->Salesordermodel->summarydetail();
	}
	public function salesorderproductdetailfetch() {
		$this->Salesordermodel->retrievesoproductdetail();
	}
	public function retrievepaymentdetail() {
		$this->Salesordermodel->retrievepaymentdetail();
	}
	//drop down value set with multiple condition
	public function fetchmaildddatawithmultiplecond() {
		$this->Salesordermodel->fetchmaildddatawithmultiplecondmodel();
	} 
	//terms and condition data fetch
	public function termsandcontdatafetch() {
		$this->Salesordermodel->termsandcontdatafetchmodel();
	}
	//editor value fetch
	public function getsalesordernumber() {
		$this->Salesordermodel->getsonumber();
	} 	
	//editor value fetch 	
	public function editervaluefetch() {
		$filename = $_GET['filename']; 
		$newfilename = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]);
			echo json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			echo json_encode($tccontent);
		}
	}	
	//retrieves basic salesorder data
	public function loadquote() {
		$this->Salesordermodel->loadquote();
	}
	//retrieves basic salesorder data
	public function getmoduledata() {
		$this->Salesordermodel->getmoduledata();
	}
	//retrieves salesorder address data	
	public function  getsoaddressdetail() {
		$parentid=$_GET['dataprimaryid'];
		$addressdetail=$this->Basefunctions->crmaddressfetch($parentid,'salesorderid','salesorderaddress');
	}
	/*	* salesorder stage-data retrival for Lost-Cancel-Draft-Convert	*/
	public function salesordercurrentstage(){
		$salesorderid = trim($_GET['salesorderid']);
		$data=$this->Salesordermodel->salesordercurrentstage($salesorderid);
		echo json_encode($data);
	}
	//retrieves salesorder product data
	public function getsoproductdetail() {
		$id=$_GET['primarydataid'];
		$this->Salesordermodel->getsoproductdetail($id);
	}
	//retrieve salesorder summary data
	public function getsosummarydata() {
		$id=$_GET['dataprimaryid'];
		$this->Salesordermodel->getsosummarydetail($id);
	}
	//cancel Salesordermodel
	public function canceldata() {
		$this->Salesordermodel->canceldata();		
	}
	public function getmoduleid(){
		$modulearray = array(87,96,217);
		$data=$this->db->select("moduleid")->from("module")->where_in('moduleid',$modulearray)->where('status',1)->limit(1)->get();
		foreach($data->result() as $info){
			$moduleid = $info->moduleid;
		}
		echo $moduleid;
	}
	//book invocie
	public function booksalesorder() {
		$this->Salesordermodel->booksalesorder();
	}
	//product storage fetch - instock
	public function productstoragefetchfun() {
		$productid = $_GET['productid'];
		$totalstock = $this->Salesordermodel->productstoragefetchfunmodel($productid);
		echo json_encode($totalstock);
	}
	public function invoicecurrentstage(){
		$invoiceid = trim($_GET['invoiceid']);
		$data=$this->Salesordermodel->invoicecurrentstage($invoiceid);
		echo json_encode($data);
	}
}