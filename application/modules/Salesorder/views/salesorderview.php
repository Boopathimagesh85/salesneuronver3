<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
</head>
<body class="hidedisplay">
	<?php
		$moduleid = implode(',',$moduleids);
		$device = $this->Basefunctions->deviceinfo();
		$dataset['gridenable'] = "yes"; //no
		$dataset['griddisplayid'] = "salesordercreationview";
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['spanattr'] = array();
		$dataset['gridtableid'] = "salesorderaddgrid";
		$dataset['griddivid'] = "salesorderaddgriddiv";
		$dataset['moduleid'] = $moduleids;
		$dataset['forminfo'] = array(array('id'=>'salesordercreationformadd','class'=>'hidedisplay','formname'=>'salesordercreationform'));
		if($device=='phone') {
			$this->load->view('Base/gridmenuheadermobile',$dataset);
			$this->load->view('Base/overlaymobile');
			$this->load->view('Base/viewselectionoverlay');
		} else {
			$this->load->view('Base/gridmenuheader',$dataset);
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
		$this->load->view('Base/basedeleteform');
		$this->load->view('Base/basecancelform');
		$this->load->view('Base/basestopform');
		$this->load->view('Purchaseorder/stopquote');
	?>	
<!-- Convert Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay" id="converttooverlay" style="overflow-y: scroll;overflow-x: hidden;">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="large-3 medium-6 large-centered medium-centered columns" >
			<form method="POST" name="" style="" id="" action="" enctype="" class="">
				<span id="" class="validationEngineContainer"> 
					<div class="alert-panel">
						<div class="alertmessagearea">
							<div class="alert-title">Convert To</div>
							<div class="alert-message">
								<span class="firsttab" tabindex="1000"></span>
								<div class="static-field overlayfield">             
									<label>Convert To</label>
									<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="validate[required] chzn-select dropdownchange ffieldd" tabindex="1001" name="converttoid" id="converttoid">
									<option value=""></option>
									<option value="249">Convert To PurchaseRequisition</option>
									<option value="226">Convert To Invoice</option>							
									</select>                  
								</div>
							</div>
						</div>
						<div class="alertbuttonarea">
							<input type="button" id="converttosumbit" name="converttosumbit" value="Submit" tabindex="1002" class="alertbtn" >	
							<input type="button" id="converttoovclose" name="" value="Cancel" tabindex="1003" class="alertbtn flloop  alertsoverlaybtn" >
							<span class="lasttab" tabindex="1004"></span>
						</div>
					</div>
				</span>
			</form>
		</div>
	</div>
</div>
</body>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js File -->
	<script src="<?php echo base_url();?>js/Salesorder/salesorder.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/Base/crmbase.js" type="text/javascript"></script>
	<!--For Tablet and Mobile view Dropdown Script-->
	<script>
		$(document).ready(function(){
			$("#tabgropdropdown").change(function(){
				var tabgpid = $("#tabgropdropdown").val(); 
				$('.sidebaricons[data-subform="'+tabgpid+'"]').trigger('click');
			});
		});
	</script>
</html>