<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Model File
class Salesordermodel extends CI_Model{
    public  $salesordermodule=217;
	private $cancelstage  = 43;	
	private $draftstage	  = 41;	
	private $bookedstage  = 42;
	public function __construct() {		
		parent::__construct(); 
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');	
    }
	//retrieves basic salesorder data
	public function loadquote() {
		$industryid = $_GET['industryid'];
		$quote=array();
		$this->db->select("quoteid,quotenumber");
		$this->db->from('quote');		
		$this->db->where('crmstatusid',33);//33->booked status
		$this->db->where('industryid',$industryid);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$this->db->order_by('quoteid',"asc");
		$result = $this->db->get();
		foreach($result->result() as $info){
			$quote[] = array('id'=>$info->quoteid,'value'=>$info->quotenumber);
		}
		echo json_encode($quote);
	}
	//load only direct tax category
	public function taxcategory(){
		$this->db->select("taxmastername,taxmasterid");
		$this->db->from('taxmaster');		
		$this->db->where('taxapplytypeid',2);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$this->db->order_by('taxmastername',"asc");
		$result = $this->db->get();
		return $result->result();
	}
	public function getsonumber() {
		$array=array();
		$i=0;
		$data=$this->db->select('salesordernumber,salesorderid')->from('salesorder')->where('status',$this->Basefunctions->activestatus)->get();
		foreach($data->result() as $value) {
			$array[$i] = array('id'=>$value->salesorderid,'name'=>$value->salesordernumber);
			$i++;
		}
		echo json_encode($array);
	}
	//To Create Sales Order Details
	public function salesordercreatemodel() {
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		$gridfieldpartabname = explode(',',$_POST['griddatapartabnameinfo']);
		$defdataarr = $this->Crudmodel->defaultvalueget();
		$gridrows = explode(',',$_POST['numofrows']);
		$griddata = $_POST['griddatas'];	
		$griddatainfo = json_decode($griddata, true);		
		$date = date($this->Basefunctions->datef);
		//industry based moduleid
		$moduleid = $_POST['viewfieldids'];
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fieldstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fieldstable);
		//filter unique grid parent table
		$gridpartablename =  $this->Crudmodel->filtervalue($gridfieldpartabname);
		$restricttable = explode(',',$_POST['resctable']);
		//Retrive the salesorder autoNumber
		$anfieldnameinfo = $_POST['anfieldnameinfo'];
		$anfieldnameidinfo = $_POST['anfieldnameidinfo'];
		$anfieldtabinfo = $_POST['anfieldtabinfo'];
		$anfieldnamemodinfo = $_POST['anfieldnamemodinfo'];
		$randomnum = $this->Basefunctions->randomnumbergenerator($anfieldnamemodinfo,$anfieldtabinfo,$anfieldnameinfo,$anfieldnameidinfo);
		$_POST['salesordernumber']=trim($randomnum);
		$primaryid = $this->Crudmodel->datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable);
		if(isset($_POST['taxmasterid'])){
				$taxmode = 3;
			}else{
				$taxmode = 2;
			}	
		$softwareindustryid = $this->Basefunctions->industryid;
		if($softwareindustryid == 2){
			$addamountmode= '';
		}else{
			if(isset($_POST['additionalchargecategoryid'])){
				$addamountmode= 3;
			}else{
				$addamountmode= 2;
			}
		}
		if(isset($_POST['quotenumber'])){
			$quotenumber=$_POST['quotenumber'];
		}else{
			$quotenumber= '';
		}
		//insert the tax/add/discount mode detail
		//summary discount type-adjustment type
		$adjustmenttypeid = '';
		$discounttypeid = '';
		$discountvalue = '';
		$salesorder_discount = json_decode($_POST['groupdiscountdata'], true);
		$salesorder_adjustment = json_decode($_POST['groupadjustmentdata'], true);		
		if(count($salesorder_discount) > 0){
			if(isset($salesorder_discount['typeid']) AND isset($salesorder_discount['value'])){
				$discounttypeid = $salesorder_discount['typeid'];
				$discountvalue = $salesorder_discount['value'];
			}
		}
		if(count($salesorder_adjustment) > 0){
			if(isset($salesorder_adjustment['typeid'])){
				$adjustmenttypeid=$salesorder_adjustment['typeid'];
			}
		}
		//update the salesorder records with mode/overlay types
		$otherdetail=array('taxmodeid'=>$taxmode,'additionalchargemodeid'=>$addamountmode,'groupdiscounttypeid'=>$discounttypeid,'groupdiscountpercent'=>$discountvalue,'adjustmenttypeid'=>$adjustmenttypeid,'quotenumber'=>$quotenumber);
		$this->db->where('salesorderid',$primaryid);
		$this->db->update('salesorder',array_filter($otherdetail));
		//salesorder billing & shipping address
		$arrname=array('billing','shipping');		
		$this->Crudmodel->addressdatainsert($arrname,$partablename,$primaryid);
		$grouptaxdetail=json_decode($_POST['grouptaxgriddata'],true);
		$groupchargedetail=json_decode($_POST['groupchargegriddata'],true);
		//Group Tax Insert
		if($taxmode == 3 && count($grouptaxdetail) > 0) {								
			$tax_category_id=$grouptaxdetail['id'];
			$tax_data=$grouptaxdetail['data'];
			for($m=0;$m < count($tax_data);$m++) {
				$tax_array = array('moduleid'=>$_POST['salesordermodule'],'singlegrouptypeid'=>$taxmode,'taxmasterid'=>$tax_category_id,'taxid'=>$tax_data[$m]['taxid'],'taxvalue'=>$tax_data[$m]['rate'],'taxamount'=>$tax_data[$m]['amount'],'id'=>$primaryid);
				$tax_array = array_filter(array_merge($tax_array,$defdataarr));
				$this->db->insert('moduletaxdetail',$tax_array);
			}			
		}
		//Group Charge Insert
		if($addamountmode == 3 && count($groupchargedetail) > 0) {			
			$charge_category_id=$groupchargedetail['id'];
			$charge_data=$groupchargedetail['data'];
			for($m=0;$m<count($charge_data);$m++) {
				$charge_array=array('moduleid'=>$_POST['salesordermodule'],'singlegrouptypeid'=>$addamountmode,'additionalchargecategoryid'=>$charge_category_id,'additionalchargetypeid'=>$charge_data[$m]['additionalchargetypeid'],'calculationtypeid'=>$charge_data[$m]['calculationtypeid'],'amount'=>$charge_data[$m]['amount'],'value'=>$charge_data[$m]['value'],'id'=>$primaryid);
				$charge_array= array_filter(array_merge($charge_array,$defdataarr));
				$this->db->insert('modulechargedetail',$charge_array);
			}
		}
		//grid data insertion
		//primary key
		$defdataarr = $this->Crudmodel->defaultvalueget();
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		//productdetail insert
		$m=0;
		$h=1;
		$colcount = 0;
		foreach($gridrows as $rowcount) {
			$colcount += $rowcount;
			$gridfieldsname = explode(',',$_POST['gridcolnames'.$h]);
			//filter grid unique fields table
			$gridfieldtabname = explode(',',$_POST['griddatatabnameinfo'.$h]);
			$gridfieldstable = $this->Crudmodel->filtervalue($gridfieldtabname);
			$gridtableinfo = explode(',',$gridfieldstable);
			for($i=($colcount-$rowcount);$i<=($colcount-1);$i++) {
				foreach( $gridtableinfo as $gdtblname ) {
					${'$gcdata'.$m} = array();
					$t=0;
					foreach($gridfieldsname AS $gdfldsname) {
						if(strcmp( trim($gridfieldtabname[$t]),trim($gdtblname) ) == 0 ) {							
							if( isset( $griddatainfo[$i][$gdfldsname] ) ) {								
								$name = explode('_',$gdfldsname);
								if(ctype_alpha($griddatainfo[$i][$gdfldsname]) && $griddatainfo[$i][$gdfldsname] != '' ) {
									${'$gcdata'.$m}[$gdfldsname] = ucwords( $griddatainfo[$i][$gdfldsname] );
								} else  {
									${'$gcdata'.$m}[$gdfldsname] = $griddatainfo[$i][$gdfldsname];
								}								
							}
						}
					}
					if(count(${'$gcdata'.$m})>0) {
						${'$gcdata'.$m}[$primaryname]=$primaryid;
						$gnewdata = array_merge(${'$gcdata'.$m},$defdataarr);
						if($gdtblname == 'payment'){
							$removehidden=array('paymentaccountid','paymentid','salesorderid');
							for($mk=0;$mk < count($removehidden);$mk++){						
								unset($gnewdata[$removehidden[$mk]]);
							}							
							$paymentmodule = array('moduleid'=>$_POST['salesordermodule'],'transactionid'=>$primaryid);
							$gnewdata = array_merge($gnewdata,$paymentmodule);
						}
						if($gdtblname == 'salesorderdetail'){							
							//individual
							$individualtaxdetail=json_decode($gnewdata['taxgriddata'],true);						
							$individualaddamountdetail=json_decode($gnewdata['chargegriddata'],true);
							//unset the hidden variables
							$removehidden=array('salesorderdetailid','taxgriddata','chargegriddata','discountdata');
							for($mk=0;$mk < count($removehidden);$mk++){						
								unset($gnewdata[$removehidden[$mk]]);
							}								
						}						
						//data insertion					
						$this->db->insert( $gdtblname, array_filter($gnewdata) );						
						if($gdtblname == 'salesorderdetail'){
						$salesorderdetailid=$this->db->insert_id();		
						//product detail discount type						
						$discounttypeid = '';
						$discountvalue = '';
						$quote_discount = json_decode($griddatainfo[$i]['discountdata'], true);						
						if(count($quote_discount) > 0){
							if(isset($quote_discount['typeid']) AND isset($quote_discount['value'])){
							$discounttypeid = $quote_discount['typeid'];
							$discountvalue = $quote_discount['value'];
							//update the product detail records with mode/overlay types
							$productdetail=array('discounttypeid'=>$discounttypeid,'discountpercent'=>$discountvalue);
							$this->db->where('salesorderdetailid',$salesorderdetailid);
							$this->db->update('salesorderdetail',array_filter($productdetail));
							}
						} 
						//individual tax data insertion
						if($taxmode == 2 && count($individualtaxdetail) > 0) {
							if(isset($individualtaxdetail['id']) AND isset($individualtaxdetail['data'])){
								$tax_category_id=$individualtaxdetail['id'];
								$tax_data=$individualtaxdetail['data'];
								for($m=0;$m < count($tax_data);$m++) {
									$tax_array=array('moduleid'=>$_POST['salesordermodule'],'singlegrouptypeid'=>$taxmode,'taxmasterid'=>$tax_category_id,'taxid'=>$tax_data[$m]['taxid'],
									'taxvalue'=>$tax_data[$m]['rate'],'taxamount'=>$tax_data[$m]['amount'],'id'=>$salesorderdetailid);
									$tax_array= array_filter(array_merge($tax_array,$defdataarr));
									$this->db->insert('moduletaxdetail',$tax_array);
								}
							}						
						}						
						if($addamountmode == 2 && count($individualaddamountdetail) > 0) {						
							if(isset($individualaddamountdetail['id']) AND isset($individualaddamountdetail['data'])){
								$charge_category_id=$individualaddamountdetail['id'];
								$charge_data=$individualaddamountdetail['data'];
								for($m=0;$m<count($charge_data);$m++) {
									$charge_array=array('moduleid'=>$_POST['salesordermodule'],'singlegrouptypeid'=>$addamountmode,'additionalchargecategoryid'=>$charge_category_id,'additionalchargetypeid'=>$charge_data[$m]['additionalchargetypeid'],'calculationtypeid'=>$charge_data[$m]['calculationtypeid'],'amount'=>$charge_data[$m]['amount'],'value'=>$charge_data[$m]['value'],'id'=>$salesorderdetailid);
									$charge_array= array_filter(array_merge($charge_array,$defdataarr));
									$this->db->insert('modulechargedetail',$charge_array);
								}
							}							
						}
						}
					}
					$m++;
				}
			}
			$h++;
		}
		//notification entry
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		if(isset($_POST['employeeid'])) {
			$assignid = $_POST['employeeid'];
			$emptypes = $_POST['employeetypeid'];
			$empdataids = array();
			$assignempids = array();
			if($assignid != '') {
				$i = 0;
				$m=0;
				$empiddatas = explode(',',$assignid);
				$emptypeids = explode(',',$emptypes);
				foreach($empiddatas as $empids) {
					$emptype = $emptypeids[$m];
					if($emptype == 1) {
						$empdataids[$i] = $empids;
						$i++;
					} else if($emptype == 2) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromgroup($empids);
						$i++;
					} else if($emptype == 3) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromroles($empids);
						$i++;
					} else if($emptype == 4) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromrolesandsubroles($empids);
						$i++;
					}
					$m++;
				}
			} else {
				$assignid = 1;
			}
		} else {
			$assignid = 1;
		}
		$sonum = $_POST['salesordernumber'];
		if($assignid == '1'){
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,2);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,$moduleid);
			}
		} else {
			foreach($empdataids as $empidinfo) {
				if(is_array($empidinfo)) { //group of employees
					foreach($empidinfo as $empid) {
						if(!in_array($empid,$assignempids)) {
							array_push($assignempids,$empid);
							$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,2);
							if($notimsg != '') {
								$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,$moduleid);
							}
						}
					}
				} else { //individual employes
					if(!in_array($empidinfo,$assignempids)){
						array_push($assignempids,$empidinfo);
						$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,2);
						if($notimsg != '') {
							$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,$moduleid);
						}
					}
				}
			}
		}
		{//workflow management -- for data create
			$this->Basefunctions->workflowmanageindatacreatemode($moduleid,$primaryid,$partablename);
		}
		echo 'TRUE';
	}
	public function termsandconditionnamefetchandupdate($primaryid,$tandcid) {
		$this->db->select('salesorderstermsandconditions_editorfilename');
		$this->db->from('salesorder');
		$this->db->where('salesorder.salesorderid',$primaryid);
		$result=$this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = $row->salesorderstermsandconditions_editorfilename;
			}
		}
		if($tandcid != "") {
			$tcdata = array('termsandconditionstermsandconditions_editorfilename'=>$data);
			$this->db->where('termsandcondition.termsandconditionid',$tandcid);
			$this->db->update('termsandcondition',$tcdata);
		}
	}
	//information fetch
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementpartable']);
		$restricttable = array('salesorderdetail','payment','termsandcondition');
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['dataprimaryid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetchwithrestrict($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$restricttable,$moduleid);
		//?retrieve summary data
		$ext_data=$this->db->select('pricebookcurrencyid,currentcurrencyid,pricebook_currencyconvrate,taxmodeid,additionalchargemodeid,groupdiscounttypeid,groupdiscountpercent,adjustmenttypeid,adjustmentamount,writeoffamount,paidamount,balanceamount,totalpayable')
							->from('salesorder')
							->where('salesorderid',$primaryid)
							->limit(1)
							->get();
		foreach($ext_data->result() as $value){
			$detail_parameter=array('id'=>$primaryid,'taxmode'=>$value->taxmodeid,
									'chargemode'=>$value->additionalchargemodeid,'discounttype'=>$value->groupdiscounttypeid,'discountpercent'=>$value->groupdiscountpercent,'adjustmenttype'=>$value->adjustmenttypeid,'adjustmentvalue'=>$value->adjustmentamount,'paidamount'=>$value->paidamount,'writeoffamount'=>$value->writeoffamount,'balanceamount'=>$value->balanceamount,'totalpayable'=>$value->totalpayable,'pricebookcurrencyid'=>$value->pricebookcurrencyid,'currentcurrencyid'=>$value->currentcurrencyid,'pricebook_currencyconvrate'=>$value->pricebook_currencyconvrate);
		}
		$this->load->model( 'Quote/Quotemodel' );		
		$summary['summary']=$this->Quotemodel->quote_groupdetail($_GET['salesordermodule'],$detail_parameter);
		$paymentsummary = array('paidamount'=>$detail_parameter['paidamount'],'writeoffamount'=>$detail_parameter['writeoffamount'],'balanceamount'=>$detail_parameter['balanceamount'],'totalpayable'=>$detail_parameter['totalpayable']);
		$summary['summary']=array_merge($summary['summary'],$paymentsummary);
		$result = json_decode($result,true);
		$cresult= array_merge($result,$summary);
		$result=json_encode($cresult);
		echo $result;
	}
	//update data information
	public function datainformationupdatemodel() {	
		if(isset($_POST['termsandconditionid'])){
			if($_POST['termsandconditionid'] == ''){
				$_POST['termsandconditionid'] = 1;
				$_POST['salesorderdeliverydetails_editorfilename'] = '';
			}						
		}
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$defdataarr = $this->Crudmodel->defaultvalueget();
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['primarydataid'];
		//industry based moduleid
		$moduleid = $_POST['viewfieldids'];
		{//fetch old values -- work flow
			$condstatvals=$this->Basefunctions->workflowmanageolddatainfofetch($moduleid,$primaryid);
		}
		$restricttable = explode(',',$_POST['resctable']);
		$result = $this->dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable);
		if(isset($_POST['taxmasterid'])){
				$taxmode = 3;
			}else{
				$taxmode = 2;
			}	
		$softwareindustryid = $this->Basefunctions->industryid;
		if($softwareindustryid == 2){
			$addamountmode= '';
		}else{
			if(isset($_POST['additionalchargecategoryid'])){
				$addamountmode= 3;
			}else{
				$addamountmode= 2;
			}
		}
		//insert the tax/add/discount mode detail
		//summary discount type-adjustment type
		$adjustmenttypeid = '';
		$discounttypeid = '';
		$discountvalue = '';
		$salesorder_discount = json_decode($_POST['groupdiscountdata'], true);
		$salesorder_adjustment = json_decode($_POST['groupadjustmentdata'], true);
		if(count($salesorder_discount) > 0){
			if(isset($salesorder_discount['typeid']) AND isset($salesorder_discount['value'])){
				$discounttypeid = $salesorder_discount['typeid'];
				$discountvalue = $salesorder_discount['value'];
			}
		}
		if(count($salesorder_adjustment) > 0){
			if(isset($salesorder_adjustment['typeid']) ){
				$adjustmenttypeid=$salesorder_adjustment['typeid'];
			}
		}
		//update the quote records with mode/overlay types
		$otherdetail=array('taxmodeid'=>$taxmode,'additionalchargemodeid'=>$addamountmode,'groupdiscounttypeid'=>$discounttypeid,'groupdiscountpercent'=>$discountvalue,'adjustmenttypeid'=>$adjustmenttypeid);
		$this->db->where('salesorderid',$primaryid);
		$this->db->update('salesorder',array_filter($otherdetail));
		//address update
		$arrname=array('billing','shipping');	
		$this->Crudmodel->addressdataupdate($arrname,$partablename,$primaryid);
		{
		//product detail update
		//get the old product detail
		$oldsalesorderdetailid = array();
		$newsalesorderdetailid = $newpaymentid = array();
		$quotedetail=$this->db->select('salesorderdetailid')
								->from('salesorderdetail')
								->where('salesorderid',$primaryid)
								->where('status',$this->Basefunctions->activestatus)
								->get();
		foreach($quotedetail->result() as $info) {
			$oldsalesorderdetailid[]=$info->salesorderdetailid;
		}		
		$gridfieldpartabname = explode(',',$_POST['griddatapartabnameinfo']);
		$gridrows = explode(',',$_POST['numofrows']);
		$griddata = $_POST['griddatas'];
		$griddatainfo = json_decode($griddata, true);	
		//filter unique grid parent table
		$gridpartablename =  $this->Crudmodel->filtervalue($gridfieldpartabname);
		$invcount = $paycount = 0;
		for($lm=0;$lm < count($griddatainfo);$lm++) {
			if(isset($griddatainfo[$lm]['salesorderdetailid'])){
				$newsalesorderdetailid[$invcount] = $griddatainfo[$lm]['salesorderdetailid'];
				$invcount++;
			} 
			if (isset($griddatainfo[$lm]['paymentid'])){	
				$newpaymentid[$paycount] = $griddatainfo[$lm]['paymentid'];
				$paycount++;
			}			
		}
		$deletedsalesorderdetailid=ARRAY();
		//find deleted records
		for($m=0;$m < count ($oldsalesorderdetailid);$m++) {
			if(in_array($oldsalesorderdetailid[$m],$newsalesorderdetailid))
			{			
			} else {
				$deletedsalesorderdetailid[]=$oldsalesorderdetailid[$m];
			}
		}
		if(count($deletedsalesorderdetailid) > 0) {
			//delete productdetail and further tables
			for($k=0;$k<count($deletedsalesorderdetailid);$k++) {
				$this->Crudmodel->outerdeletefunction('salesorderdetail','salesorderdetailid','',$deletedsalesorderdetailid[$k]);
			}
			//delete the additional/tax/discount if exits
			$updateloginfo = $this->Crudmodel->updatedefaultvalueget();
			$moduledelete=array('status'=>$this->Basefunctions->deletestatus);
			$final=array_merge($moduledelete,$updateloginfo);
			$moduledetailtable=array('moduletaxdetail','modulechargedetail');
			//delete the sub tables(moduletaxdetail,modulechargedetail)
			foreach($moduledetailtable as $table) {
				$this->db->where('moduleid',$_POST['salesordermodule']);
				$this->db->where('singlegrouptypeid',2);
				$this->db->where_in('id',$deletedsalesorderdetailid);
				$this->db->update($table,$final);
			}			
		}
		//remove deleted payments in salesorder
		//get the old payment records
		$oldsalesorderpaymentid=array();
		$deletedpaymentid=array();
		$salesorderpayment=$this->db->select('paymentid')
									->from('payment')
									->where('moduleid',$_POST['salesordermodule'])
									->where('transactionid',$primaryid)
									->where('status',$this->Basefunctions->activestatus)
									->get();
		foreach($salesorderpayment->result() as $info) {
			$oldsalesorderpaymentid[]=$info->paymentid;
		}
		//moduledetail update		
		//payment detail
		for($m=0;$m < count ($oldsalesorderpaymentid);$m++) {
			if(in_array($oldsalesorderpaymentid[$m],$newpaymentid)) {			
			} else {
				$deletedpaymentid[]=$oldsalesorderpaymentid[$m];
			}
		}
		if(count($deletedpaymentid) > 0) {
			for($k=0;$k<count($deletedpaymentid);$k++) {
				$this->Crudmodel->outerdeletefunction('payment','paymentid','',$deletedpaymentid[$k]);
			}
		}
		$m=0;
		$h=1;
		$colcount = 0;
		foreach($gridrows as $rowcount) {
			$colcount += $rowcount;
			$gridfieldsname = explode(',',$_POST['gridcolnames'.$h]);			
			//filter grid unique fields table
			$gridfieldtabname = explode(',',$_POST['griddatatabnameinfo'.$h]);
			$gridfieldstable = $this->Crudmodel->filtervalue($gridfieldtabname);
			$gridtableinfo = explode(',',$gridfieldstable);
			for($i=($colcount-$rowcount);$i<=($colcount-1);$i++) {
				foreach( $gridtableinfo as $gdtblname ) {
					${'$gcdata'.$m} = array();
					$t=0;
					foreach($gridfieldsname AS $gdfldsname) {
						if(strcmp( trim($gridfieldtabname[$t]),trim($gdtblname) ) == 0 ) {							
							if( isset( $griddatainfo[$i][$gdfldsname] ) ) {								
								$name = explode('_',$gdfldsname);
								if(ctype_alpha($griddatainfo[$i][$gdfldsname]) && $griddatainfo[$i][$gdfldsname] != '' ) {
									${'$gcdata'.$m}[$gdfldsname] = ucwords( $griddatainfo[$i][$gdfldsname] );
								} else  {
									${'$gcdata'.$m}[$gdfldsname] = $griddatainfo[$i][$gdfldsname];
								}								
							}
						}
					}
					if(count(${'$gcdata'.$m})>0) {
						${'$gcdata'.$m}[$primaryname]=$primaryid;
						$gnewdata = array_merge(${'$gcdata'.$m},$defdataarr);
						if($gdtblname == 'payment'){
							unset($gnewdata['paymentaccountid']);
							unset($gnewdata['salesorderid']);
							$paymentmodule = array('moduleid'=>$_POST['salesordermodule'],'transactionid'=>$primaryid);
							$gnewdata = array_merge($gnewdata,$paymentmodule);
						}
						if($gdtblname == 'salesorderdetail'){							
							$individualtaxdetail=json_decode($gnewdata['taxgriddata'],true);						
							$individualaddamountdetail=json_decode($gnewdata['chargegriddata'],true);
							//unset the hidden variables
							$removehidden=array('salesorderdetailid','taxgriddata','chargegriddata','discountdata');
							for($mk=0;$mk < count($removehidden);$mk++){						
								unset($gnewdata[$removehidden[$mk]]);
							}							
						}
						if($gdtblname == 'salesorderdetail'){
							if($griddatainfo[$i]['salesorderdetailid'] == 0){ //new-data
								//data insertion
								$this->db->insert( $gdtblname, array_filter($gnewdata) );
								$salesorderdetailid=$this->db->insert_id();
							}
							if($griddatainfo[$i]['salesorderdetailid'] > 0){ //update-existing data
								$this->db->where('salesorderid',$griddatainfo[$i]['salesorderdetailid']);
								$this->db->update($gdtblname,array_filter($gnewdata));
								$salesorderdetailid=$griddatainfo[$i]['salesorderdetailid'];
								//remove old data.
								$delete_sub_data=array('lastupdateuserid'=>$this->Basefunctions->userid,'status'=>$this->Basefunctions->deletestatus);
								$moduledetailtable=array('moduletaxdetail','modulechargedetail');
								//delete the sub tables(moduletaxdetail,modulechargedetail)
								foreach($moduledetailtable as $table)
								{
									$this->db->where('moduleid',$_POST['salesordermodule']);
									$this->db->where('singlegrouptypeid',2);
									$this->db->where_in('id',$griddatainfo[$i]['salesorderdetailid']);
									$this->db->update($table,$delete_sub_data);
								}
							}
							//product detail discount type			 quote			
							$discounttypeid = '';
							$discountvalue = '';
							$salesorder_discount = json_decode($griddatainfo[$i]['discountdata'], true);						
							if(count($salesorder_discount) > 0){
								if(isset($salesorder_discount['typeid']) AND isset($salesorder_discount['value'])){
								$discounttypeid = $salesorder_discount['typeid'];
								$discountvalue = $salesorder_discount['value'];
								//update the product detail records with mode/overlay types
								$productdetail=array('discounttypeid'=>$discounttypeid,'discountpercent'=>$discountvalue);
								$this->db->where('salesorderdetailid',$salesorderdetailid);
								$this->db->update('salesorderdetail',array_filter($productdetail));
								}
							}
							//individual tax data insertion
							if($taxmode == 2 && count($individualtaxdetail) > 0) {
								if(isset($individualtaxdetail['id']) AND isset($individualtaxdetail['data'])){
									$tax_category_id=$individualtaxdetail['id'];
									$tax_data=$individualtaxdetail['data'];
									for($m=0;$m < count($tax_data);$m++)
									{
										$tax_array=array('moduleid'=>$_POST['salesordermodule'],'singlegrouptypeid'=>$taxmode,'taxmasterid'=>$tax_category_id,'taxid'=>$tax_data[$m]['taxid'],
										'taxvalue'=>$tax_data[$m]['rate'],'taxamount'=>$tax_data[$m]['amount'],'id'=>$salesorderdetailid);
										$tax_array= array_filter(array_merge($tax_array,$defdataarr));
										$this->db->insert('moduletaxdetail',$tax_array);
									}
								}							
							}						
							if($addamountmode == 2 && count($individualaddamountdetail) > 0) {						
								if(isset($individualaddamountdetail['id']) AND isset($individualaddamountdetail['data'])){
								$charge_category_id=$individualaddamountdetail['id'];
								$charge_data=$individualaddamountdetail['data'];
								for($m=0;$m<count($charge_data);$m++)
								{
									$charge_array=array('moduleid'=>$_POST['salesordermodule'],'singlegrouptypeid'=>$addamountmode,'additionalchargecategoryid'=>$charge_category_id,'additionalchargetypeid'=>$charge_data[$m]['additionalchargetypeid'],'calculationtypeid'=>$charge_data[$m]['calculationtypeid'],'amount'=>$charge_data[$m]['amount'],'value'=>$charge_data[$m]['value'],'id'=>$salesorderdetailid);
									$charge_array= array_filter(array_merge($charge_array,$defdataarr));
									$this->db->insert('modulechargedetail',$charge_array);
								}
								}
							}
						}
						if($gdtblname == 'payment'){
							if($griddatainfo[$i]['paymentid'] == 0){
								$this->db->insert($gdtblname,array_filter($gnewdata));
							}
							if($griddatainfo[$i]['paymentid'] > 0){
								$this->db->where('paymentid',$griddatainfo[$i]['paymentid']);
								$this->db->update($gdtblname,array_filter($gnewdata));
							}
						}	
					}
					$m++;
				}
			}
			$h++;
		}
		}
		//summary update	
		//group tax
		$grouptaxdetail=json_decode($_POST['grouptaxgriddata'],true);
		$groupchargedetail=json_decode($_POST['groupchargegriddata'],true);
		$deletearray=array('lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid,'status'=>$this->Basefunctions->deletestatus);
		//resets the moduletaxdetail/modulechargedetail entries
		$moduledetailtable=array('moduletaxdetail','modulechargedetail');
		//delete the sub tables(moduletaxdetail,modulechargedetail)
		foreach($moduledetailtable as $table) {
			$this->db->where('moduleid',$_POST['salesordermodule']);
			$this->db->where('singlegrouptypeid',3);
			$this->db->where_in('id',$primaryid);
			$this->db->update($table,$deletearray);
		}
		if($taxmode == 3 && count($grouptaxdetail) > 0) {			
			//insert new data
			if(isset($grouptaxdetail['id']) AND isset($grouptaxdetail['data'])){
				$tax_category_id=$grouptaxdetail['id'];
				$tax_data=$grouptaxdetail['data'];
				for($m=0;$m < count($tax_data);$m++) {
					$tax_array=array('moduleid'=>$_POST['salesordermodule'],'singlegrouptypeid'=>$taxmode,'taxmasterid'=>$tax_category_id,'taxid'=>$tax_data[$m]['taxid'],
					'taxvalue'=>$tax_data[$m]['rate'],'taxamount'=>$tax_data[$m]['amount'],'id'=>$primaryid);
					$tax_array= array_filter(array_merge($tax_array,$defdataarr));
					$this->db->insert('moduletaxdetail',$tax_array);
				}					
			}
		}				
		if($addamountmode == 3 && count($groupchargedetail) > 0) {				
			//insert new data
			if(isset($groupchargedetail['id']) AND isset($groupchargedetail['data'])){
			$charge_category_id=$groupchargedetail['id'];
			$charge_data=$groupchargedetail['data'];
			for($m=0;$m<count($charge_data);$m++) {
				$charge_array=array('moduleid'=>$_POST['salesordermodule'],'singlegrouptypeid'=>$addamountmode,'additionalchargecategoryid'=>$charge_category_id,'additionalchargetypeid'=>$charge_data[$m]['additionalchargetypeid'],'calculationtypeid'=>$charge_data[$m]['calculationtypeid'],'amount'=>$charge_data[$m]['amount'],'value'=>$charge_data[$m]['value'],'id'=>$primaryid);
				$charge_array = array_filter(array_merge($charge_array,$defdataarr));
				$this->db->insert('modulechargedetail',$charge_array);
			}
			}
		} 
		//notification entry
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		if(isset($_POST['employeeid'])) {
			$assignid = $_POST['employeeid'];
			$emptypes = $_POST['employeetypeid'];
			$empdataids = array();
			$assignempids = array();
			if($assignid != '') {
				$i = 0;
				$m=0;
				$empiddatas = explode(',',$assignid);
				$emptypeids = explode(',',$emptypes);
				foreach($empiddatas as $empids) {
					$emptype = $emptypeids[$m];
					if($emptype == 1) {
						$empdataids[$i] = $empids;
						$i++;
					} else if($emptype == 2) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromgroup($empids);
						$i++;
					} else if($emptype == 3) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromroles($empids);
						$i++;
					} else if($emptype == 4) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromrolesandsubroles($empids);
						$i++;
					}
					$m++;
				}
			} else {
				$assignid = 1;
			}
		} else {
			$assignid = 1;
		}
		$sonum = $_POST['salesordernumber'];
		if($assignid == '1'){
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch($_POST['salesordermodule'],$primaryid,$partablename,3);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,$_POST['salesordermodule']);
			}
		} else {
			foreach($empdataids as $empidinfo) {
				if(is_array($empidinfo)) { //group of employees
					foreach($empidinfo as $empid) {
						if( !in_array($empid,$assignempids) ) {
							array_push($assignempids,$empid);
							$notimsg = $this->Basefunctions->notificationtemplatedatafetch($_POST['salesordermodule'],$primaryid,$partablename,3);
							if($notimsg != '') {
								$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,$_POST['salesordermodule']);
							}
						}
					}
				} else { //individual employees
					if( !in_array($empidinfo,$assignempids) ) {
						array_push($assignempids,$empidinfo);
						$notimsg = $this->Basefunctions->notificationtemplatedatafetch($_POST['salesordermodule'],$primaryid,$partablename,3);
						if($notimsg != '') {
							$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,$_POST['salesordermodule']);
						}
					}
				}
			}
		}
		{//workflow management for data update
			$this->Basefunctions->workflowmanageindataupdatemode($moduleid,$primaryid,$partablename,$condstatvals);
		}
		echo true;
	}
	//delete old information
	public function salesorderdeletemodel($moduleid) {
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primarydataid'];
		$msg='False';
		$filename = $this->Basefunctions->generalinformaion('salesorder','salesordernumber','salesorderid',$id);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		{//workflow management -- for data delete
			$this->Basefunctions->workflowmanageindatadeletemode(217,$id,$primaryname,$partabname);
		}
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				$msg='Denied';
			} else {
				//module detail
				$this->Basefunctions->deletetaxaddchargedetail($_GET['salesordermodule'],'salesorder',$id);
				//payment delete
				$this->Basefunctions->deletemodulepayment($_GET['salesordermodule'],$id);
				//notification log entry
				$notimsg = $this->Basefunctions->notificationtemplatedatafetch($_GET['salesordermodule'],$id,$partabname,4);
				if($notimsg != '') {
					$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,1,$_GET['salesordermodule']);
				}
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				$msg="TRUE";
			}
		} else {
			//module detail
			$this->Basefunctions->deletetaxaddchargedetail($_GET['salesordermodule'],'salesorder',$id);
			//payment delete
			$this->Basefunctions->deletemodulepayment($_GET['salesordermodule'],$id);
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch($_GET['salesordermodule'],$id,$partabname,4);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,1,$_GET['salesordermodule']);
			}
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			$msg="TRUE";
		}
		echo $msg;		
	}
	//primary and secondary address value fetch
	public function primaryaddressvalfetchmodel() {
		$accid = $_GET['dataprimaryid'];	
		$this->db->select('salesorderaddress.addresssourceid,salesorderaddress.address,salesorderaddress.pincode,salesorderaddress.city,salesorderaddress.state,salesorderaddress.country');
		$this->db->from('salesorderaddress');
		$this->db->where('salesorderaddress.salesorderid',$accid);
		$this->db->where('salesorderaddress.status',1);
		$this->db->order_by('salesorderaddressid','asc');
		$result = $this->db->get();
		$arrname=array('billing','shipping');
		if($result->num_rows() >0) {
			$m=0;
			foreach($result->result() as $row) {
				$data[] = array($arrname[$m].'addresstype'=>$row->addresssourceid,
								$arrname[$m].'address'=>$row->address,
								$arrname[$m].'pincode'=>$row->pincode,
								$arrname[$m].'city'=>$row->city,
								$arrname[$m].'state'=>$row->state,
								$arrname[$m].'country'=>$row->country);
				$m++;
			}
			$finalarray=array_merge($data[0],$data[1]);
			echo json_encode($finalarray);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//data update with restriction
	public function dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable) {
		//generate value array
		$m=0;
		$pdata = array();
	    foreach( $tableinfo as $tblname ) {
			${'$cdata'.$m} = array();
			$i=0;
			if( !in_array(trim($tblname),$restricttable) ) {
				foreach( $formfieldscolmname as $fcolvalue ) {
					if( strcmp( trim($partablename),trim($formfieldstable[$i]) ) == 0 ) { //ptable
						if( isset( $_POST[$formfieldsname[$i]] ) ) {
							$name = explode('_',$formfieldsname[$i]);
							if( !is_array($_POST[$formfieldsname[$i]]) ) {
								if( ctype_alpha($_POST[$formfieldsname[$i]]) ) {
									$txtstring1 = "";
									if( in_array('password',$name) ) {
										$hash = password_hash($_POST[$formfieldsname[$i]], PASSWORD_DEFAULT);
										$pdata[$fcolvalue] = $hash ;
									} else if( in_array('editorfilename',$name) ) {
										$txtstring1 = trim($_POST[$formfieldsname[$i]]);
										$pdata[$fcolvalue] = $this->Crudmodel->generatefile($txtstring1);
									}  else {
										$pdata[$fcolvalue] = ucwords( $_POST[$formfieldsname[$i]] );
									}
								} else {
									$txtstring2 = "";
									if( in_array('password',$name) ) {
										$hash = password_hash($_POST[$formfieldsname[$i]], PASSWORD_DEFAULT);
										$pdata[$fcolvalue] = $hash ;
									} else if( in_array('editorfilename',$name) ) {
										$txtstring2 = trim($_POST[$formfieldsname[$i]]);
										$pdata[$fcolvalue] = $this->Crudmodel->generatefile($txtstring2);
									} else {
										$pdata[$fcolvalue] = $_POST[$formfieldsname[$i]];
									}
								}
							} else {
								$pdata[$fcolvalue] = implode(',',$_POST[$formfieldsname[$i]]);
							}
						}
					} else if( strcmp( trim($formfieldstable[$i]),trim($tblname) ) == 0 && strcmp( trim($partablename),trim($formfieldstable[$i]) ) != 0 ) { //ctable
						if( isset( $_POST[$formfieldsname[$i]] ) ) {
							$name = explode('_',$formfieldsname[$i]);
							if( !is_array($_POST[$formfieldsname[$i]]) ) {
								if( ctype_alpha($_POST[$formfieldsname[$i]]) ) {
									$txtstring3 = "";
									if( in_array('password',$name) ) {
										$hash = password_hash($_POST[$formfieldsname[$i]], PASSWORD_DEFAULT);
										${'$cdata'.$m}[$fcolvalue] = $hash ;
									} else if( in_array('editorfilename',$name) ) {
										$txtstring3 = trim($_POST[$formfieldsname[$i]]);
										${'$cdata'.$m}[$fcolvalue] = $this->Crudmodel->generatefile($txtstring3);
									} else {
										${'$cdata'.$m}[$fcolvalue] = ucwords( $_POST[$formfieldsname[$i]] );
									}
								} else {
									$txtstring4 = "";
									if( in_array('password',$name) ) {
										$hash = password_hash($_POST[$formfieldsname[$i]], PASSWORD_DEFAULT);
										${'$cdata'.$m}[$fcolvalue] = $hash ;
									} else if( in_array('editorfilename',$name) ) {
										$txtstring4 = trim($_POST[$formfieldsname[$i]]);
										${'$cdata'.$m}[$fcolvalue] = $this->Crudmodel->generatefile($txtstring4);
									} else {
										${'$cdata'.$m}[$fcolvalue] = $_POST[$formfieldsname[$i]];
									}
								}
							} else {
								${'$cdata'.$m}[$fcolvalue] = implode(',',$_POST[$formfieldsname[$i]]);
							}
						}
					}
					$i++;
				}
			}
			$m++;
		}
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		{//parent table update
			$defdataarr = $this->Crudmodel->updatedefaultvalueget();
			$newdata = array_merge($pdata,$defdataarr);
			//update information
			$this->db->where($primaryname,$primaryid);
			$this->db->update($partablename,$newdata);
		}
		{//child table update
			$m=0;
			foreach( $tableinfo as $tblname ) {
				$defdataarr = $this->Crudmodel->defaultvalueget();
				$cnewdata = array();
				if(count(${'$cdata'.$m}) > 0 ) {
					if( strcmp( trim($partablename),trim($tblname) ) != 0 ) {
						if( !in_array(trim($tblname),$restricttable) ) {
							//update default value
							$defdataarr = $this->Crudmodel->updatedefaultvalueget();
							$cnewdata = array_merge(${'$cdata'.$m},$defdataarr);
							//update information
							$this->db->where($primaryname,$primaryid);
							$this->db->update($tblname,array_filter($cnewdata));
						}
					}
				}
				$m++;
			}
		}
		return 'TRUE';
	}
	public function retrievesoproductdetail() {
		$salesorderid=trim($_GET['primarydataid']);
		$this->db->select('salesorderdetailid,salesorderdetail.salesorderid,productname,salesorderdetail.productid,salesorderdetail.instock,salesorderdetail.quantity,salesorderdetail.unitprice,salesorderdetail.sellingprice,salesorderdetail.grossamount,salesorderdetail.chargeamount,salesorderdetail.taxamount,salesorderdetail.discountamount,salesorderdetail.netamount,salesorderdetail.descriptiondetail,salesorder.taxmodeid,salesorder.additionalchargemodeid,salesorderdetail.discounttypeid,salesorderdetail.discountpercent,salesorderdetail.pretaxtotal');
		$this->db->from('salesorderdetail');
		$this->db->join('salesorder','salesorder.salesorderid=salesorderdetail.salesorderid');
		$this->db->join('product','product.productid=salesorderdetail.productid');
		$this->db->where('salesorderdetail.salesorderid',$salesorderid);
		$this->db->where('salesorderdetail.status',$this->Basefunctions->activestatus);
		$data=$this->db->get();
		if($data->num_rows() > 0){
			$this->load->model( 'Quote/Quotemodel' );		   
			$j=0;		
			foreach($data->result() as $value) {	
				$detail_parameter=array('id'=>$value->salesorderdetailid,'taxmode'=>$value->taxmodeid,
										'chargemode'=>$value->additionalchargemodeid,'discounttype'=>$value->discounttypeid,'discountpercent'=>$value->discountpercent);
				$detailarray=$this->Quotemodel->quote_individualdetail($detail_parameter,$_GET['salesordermodule']);
				$productdetail->rows[$j]['id'] = $value->salesorderdetailid;
				$softwareindustryid = $this->Basefunctions->industryid;
				if($softwareindustryid == 2){
					$productdetail->rows[$j]['cell']=array(
							$value->productname,
							$value->productid,
							$value->instock,
							$value->quantity,
							$value->unitprice,
							$value->sellingprice,
							$value->grossamount,
							$value->discountamount,
							$value->pretaxtotal,
							$value->taxamount,
							$value->netamount,
							$detailarray['tax'],
							$detailarray['addcharge'],
							$detailarray['discount'],
							$value->salesorderdetailid
					);
				}else{
					$productdetail->rows[$j]['cell']=array(
							$value->productname,
							$value->productid,
							$value->instock,
							$value->quantity,
							$value->unitprice,
							$value->sellingprice,
							$value->grossamount,
							$value->discountamount,
							$value->pretaxtotal,
							$value->taxamount,
							$value->chargeamount,
							$value->netamount,
							$detailarray['tax'],
							$detailarray['addcharge'],
							$detailarray['discount'],
							$value->salesorderdetailid
					);
				}									
				$j++;		
			}
		}
		echo  json_encode($productdetail);
	}
	public function retrievepaymentdetail() {
		$salesorderid=trim($_GET['primarydataid']);		
		$moduleid = $this->salesordermodule;
		$data=$this->db->select('paymentid,paymentnumber,paymentdate,paymenttypename,paymentmethodname,paymentamount,payment.paymentreferencenumber,payment.bankname,paymentstatusname,payment.referencedate,paymentdescription,payment.paymentmethodid,payment.paymenttypeid,payment.paymentstatusid,payment.paymentto,payment.payeename')
					->from('payment')		
					->join('paymentmethod','paymentmethod.paymentmethodid=payment.paymentmethodid')
					->join('paymenttype','paymenttype.paymenttypeid=payment.paymenttypeid')
					->join('paymentstatus','paymentstatus.paymentstatusid=payment.paymentstatusid')
					->where('payment.moduleid',$this->salesordermodule)
					->where('payment.status',$this->Basefunctions->activestatus)
					->where('payment.transactionid',$salesorderid)
					->get();
		$j=0;
		if($data->num_rows() >  0){
			foreach($data->result() as $value) {
				if($value->paymentdescription){
					$description = $value->paymentdescription;
				}else{
					$description = '';
				}
				$productdetail->rows[$j]['id']=$value->paymentid;
				$productdetail->rows[$j]['cell']=array(
														$value->paymentdate,
														$value->paymentto,
														$value->paymenttypename,
														$value->paymenttypeid,
														$value->paymentmethodname,
														$value->paymentmethodid,
														$value->paymentamount,
														$value->paymentreferencenumber,
														$value->paymentstatusname,
														$value->paymentstatusid,
														$value->bankname,													
														$value->referencedate,
														$value->payeename,
														$description,
														$value->paymentid
													);						
				$j++;		
			}
			echo  json_encode($productdetail);
		} else {
			echo  '';
		}
	}
	//retrieve the summary overlay detail 
	public function summarydetail() {
		$quoteid=$_GET['dataprimaryid'];
		$data=$this->db->select('writeoffamount,balanceamount,netamount,paidamount,taxmodeid,discountmodeid,additionalchargemodeid,taxmasterid,additionalchargecategoryid,adjustmenttypeid,adjustmentamount,mname')
						->from('salesorder')
						->where('salesorder.salesorderid',$quoteid)
						->join('salesordersummary','salesordersummary.salesorderid=salesorder.salesorderid')
						->get();
		$row=$data->row();
		$taxmodeid=$row->taxmodeid;
		$discountmodeid=$row->discountmodeid;
		$addchargemodeid=$row->additionalchargemodeid;
		$adjustmenttype=$row->adjustmenttypeid;
		$adjustmentvalue=$row->adjustmentamount;
		$salesorderid=1;
		$sonumber=$this->db->select('salesorderid')
								->from('salesorder')
								->where('salesordernumber',$row->mname)
								->limit(1)->get()->result();
		foreach($sonumber as $inf) {
			$salesorderid=$inf->salesorderid;
		}
		$modedetail=array('taxmodeid'=>$taxmodeid,'discountmodeid'=>$discountmodeid,'additionalchargemodeid'=>$addchargemodeid,'taxmasterid'=>$row->taxmasterid,
						  'additionalchargecategoryid'=>$row->additionalchargecategoryid,'adjustmenttype'=>$adjustmenttype,'adjustmentvalue'=>$adjustmentvalue,
						  'summarynetamount'=>$row->netamount,'paidamount'=>$row->paidamount,'writeoffamount'=>$row->writeoffamount,'balanceamount'=>$row->balanceamount,'mname'=>$salesorderid						  
						  );
		
		$taxarray=array();
		$discountarray=array();
		$addamountarray=array();	
		$summarydetail=array('tax'=>$taxarray,'discount'=>$discountarray,'addcharge'=>$addamountarray,'mode'=>$modedetail);
		echo json_encode($summarydetail);
	}
	//terms and condition data fetch
	public function termsandcontdatafetchmodel() {
		$id = $_GET['id'];
		$this->db->select('termsandconditionstermsandconditions_editorfilename');
		$this->db->from('termsandcondition');
		$this->db->where('termsandcondition.termsandconditionid',$id);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$data = $row->termsandconditionstermsandconditions_editorfilename;
			}
			echo json_encode($data);
		}
	}
	//drop down value set with multiple condition
	public function fetchmaildddatawithmultiplecondmodel() {
		$i=0;
		$dname=$_GET['dataname'];
		$did=$_GET['dataid'];
		$dataattrname1=$_GET['othername1'];
		$dataattrname2=$_GET['othername2'];
		$table=$_GET['datatab'];
		$whfield=$_GET['whdatafield'];
		$whdata=$_GET['whval'];
		$mulcond=explode(",",$whfield);
		$mulcondval=explode(",",$whdata);
		$i=0;
		$this->db->select("$dname,$did,$dataattrname1,$dataattrname2");
		$this->db->from($table);
		foreach($mulcond AS $condname) {
			if($mulcondval[$i] != "0" && $mulcondval[$i] != "") {
				$this->db->where($condname,$mulcondval[$i]);
			}
			$i++;
		}
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {		
			foreach($result->result()as $row) {
				$data[$i]=array('datasid'=>$row->$did,'dataname'=>$row->$dname,'otherdataattrname1'=>$row->$dataattrname1,'otherdataattrname2'=>$row->$dataattrname2);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//retrieves basic so data
	//Get module data
	public function getmoduledata() {
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementpartable']);
		$restricttable = array('');
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['dataprimaryid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetchwithrestrict($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$restricttable);
		echo $result;
	}
	//Get salesorder product detail
	public function getsoproductdetail($salesorderid) {
		$this->db->select('salesorderdetailid,salesorderdetail.salesorderid,productname,salesorderdetail.productid,salesorderdetail.instock,salesorderdetail.quantity,salesorderdetail.unitprice,salesorderdetail.sellingprice,salesorderdetail.grossamount,salesorderdetail.additionalamount,salesorderdetail.taxamount,salesorderdetail.discountamount,salesorderdetail.netprice,salesorderdetail.descriptiondetail,salesorder.taxmodeid,salesorder.discountmodeid,salesorder.additionalchargemodeid');
		$this->db->from('salesorderdetail');
		$this->db->join('salesorder','salesorder.salesorderid=salesorderdetail.salesorderid');
		$this->db->join('product','product.productid=salesorderdetail.productid');
		$this->db->where('salesorder.salesorderid',$salesorderid);
		$this->db->where('salesorderdetail.status',$this->Basefunctions->activestatus);
		$data=$this->db->get();	
		$this->load->model('Salesorder/Salesordermodel');
		if($data->num_rows() > 0){		
			$j=0;
			foreach($data->result() as $value) {			
				$detailarray=$this->Salesordermodel->soindividualdetail($value->salesorderdetailid,$value->taxmodeid,$value->discountmodeid,$value->additionalchargemodeid);
				$productdetail->rows[$j]['id']=$j;
				$productdetail->rows[$j]['cell']=array(
														$value->productname,
														$value->productid,
														$value->instock,
														$value->quantity,
														$value->unitprice,
														$value->sellingprice,
														$value->grossamount,
														$value->discountamount,
														$value->taxamount,
														$value->additionalamount,
														$value->netprice,
														$detailarray['tax'],
														$detailarray['addcharge'],
														$detailarray['discount'],
														0
													);						
				$j++;			
			}
		}
		else{
			$productdetail='';
		}
		echo  json_encode($productdetail);
	}
	public function getsosummarydetail() {
		$salesorderid=$_GET['dataprimaryid'];
		$data=$this->db->select('netamount,taxmodeid,discountmodeid,additionalchargemodeid,taxmasterid,additionalchargecategoryid,adjustmenttypeid,adjustmentamount,salesorder.salesorderid,salesordersummary.grossamount,salesordersummary.taxamount,salesordersummary.discountamount,salesordersummary.additionalchargeamount,salesordersummary.netamount')
						->from('salesorder')
						->where('salesorder.salesorderid',$salesorderid)
						->join('salesordersummary','salesordersummary.salesorderid=salesorder.salesorderid')
						->limit(1)
						->get();
		$row=$data->row();
		
		$taxmodeid=$row->taxmodeid;
		$discountmodeid=$row->discountmodeid;
		$addchargemodeid=$row->additionalchargemodeid;
		$adjustmenttype=$row->adjustmenttypeid;
		$adjustmentvalue=$row->adjustmentamount;
		;
		
		$modedetail=array('taxmodeid'=>$taxmodeid,'discountmodeid'=>$discountmodeid,'additionalchargemodeid'=>$addchargemodeid,'taxmasterid'=>$row->taxmasterid,
						  'additionalchargecategoryid'=>$row->additionalchargecategoryid,'adjustmenttype'=>$adjustmenttype,'adjustmentvalue'=>$adjustmentvalue,
						  'summarynetamount'=>$row->netamount,
						  'summarygrossamount'=>$row->grossamount,
						  'summarytaxamount'=>$row->taxamount,
						  'summaryadditionalchargeamount'=>$row->additionalchargeamount,
						  'summarydiscountamount'=>$row->discountamount );
		
		$taxarray=array();
		$discountarray=array();
		$addamountarray=array();
		//tax charge
		if($taxmodeid == 3) {
			$taxdata=$this->db->select('taxname,moduletaxdetailid,singlegrouptypeid,moduletaxdetail.taxmasterid,taxruleid,moduletaxdetail.taxmasterdetailid,moduletaxdetail.taxvalue,moduletaxdetail.taxamount')
							->from('moduletaxdetail')
							->join('taxmasterdetail','taxmasterdetail.taxmasterdetailid=moduletaxdetail.taxmasterdetailid')
							->where('moduletaxdetail.moduleid',$this->salesordermodule)
							->where('moduletaxdetail.singlegrouptypeid',$taxmodeid)
							->where('moduletaxdetail.status',$this->Basefunctions->activestatus)
							->where_in('id',$salesorderid)							
							->get();
							
			if($taxdata->num_rows() > 0) {
				foreach($taxdata->result() as $info)
				{
					$taxarray[]=array('moduletaxdetailid'=>$info->moduletaxdetailid,'singlegrouptypeid'=>$info->singlegrouptypeid,'taxmasterid'=>$info->taxmasterid,'taxmasterdetailid'=>$info->taxmasterdetailid,'taxvalue'=>$info->taxvalue,'taxamount'=>$info->taxamount,'taxname'=>$info->taxname,'taxruleid'=>$info->taxruleid);
				}	
			}		
		}	
		if($addchargemodeid == 3) {
			$adddata=$this->db->select('moduleaddchargedetailid,moduleaddchargedetail.moduleid,singlegrouptypeid,moduleaddchargedetail.additionalchargecategoryid,moduleaddchargedetail.additionalchargetypeid,moduleaddchargedetail.calculationtypeid,moduleaddchargedetail.amount,moduleaddchargedetail.value,id,additionalchargetypename')
							->from('moduleaddchargedetail')
							->join('additionalchargetype','additionalchargetype.additionalchargetypeid=moduleaddchargedetail.additionalchargetypeid')
							->where('moduleaddchargedetail.moduleid',$this->salesordermodule)
							->where('singlegrouptypeid',$addchargemodeid)
							->where_in('id',$salesorderid)
							->get();
			if($adddata->num_rows() > 0) {
				foreach($adddata->result() as $info) {
					$addamountarray[]=array('addname'=>$info->additionalchargetypename,'moduleaddchargedetailid'=>$info->moduleaddchargedetailid,'singlegrouptypeid'=>$info->singlegrouptypeid,'additionalchargecategoryid'=>$info->additionalchargecategoryid,'additionalchargetypeid'=>$info->additionalchargetypeid,'calculationtypeid'=>$info->calculationtypeid,'amount'=>$info->amount,'value'=>$info->value);
				}	
			}
		}		
		//discount details
		if($discountmodeid == 3) {
			$discountdata=$this->db->select('modulediscountdetailid,calculationtypeid,discountvalue,id,singlegrouptypeid')
							->from('modulediscountdetail')
							->where('moduleid',$this->salesordermodule)
							->where('singlegrouptypeid',$discountmodeid)
							->where_in('id',$salesorderid)
							->get();
			if($discountdata->num_rows() > 0){
				foreach($discountdata->result() as $info) {
					$discountarray[]=array('calculationtypeid'=>$info->calculationtypeid,'singlegrouptypeid'=>$info->singlegrouptypeid,'discountvalue'=>$info->discountvalue,'modulediscountdetailid'=>$info->modulediscountdetailid);
				}	
			}
		}		
		$summarydetail=array('tax'=>$taxarray,'discount'=>$discountarray,'addcharge'=>$addamountarray,'mode'=>$modedetail);
		echo json_encode($summarydetail);
	}
	//cancel
	public function canceldata() {
		$primaryid=$_GET['primarydataid'];	
		$dbdata=$this->salesordercurrentstage($primaryid); //retrieves the existing stage		
		$prev_stage=$dbdata['salesorderstage'];
		$current_stage = $this->cancelstage;
		if($prev_stage == $this->cancelstage)//if both on same stage then return to draft
		{
			$current_stage=$this->draftstage;
		}		
		$updatearray=array('lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid,'crmstatusid'=>	$current_stage);
		$this->db->where('salesorderid',$primaryid);
		$this->db->update('salesorder',$updatearray);
		//Notification On Cancel-Return to Draft
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		$invnum = $this->Basefunctions->generalinformaion('salesorder','salesordernumber','salesorderid',$primaryid);
		if($prev_stage != $this->cancelstage)  { 
			$notimsg = $empname." Changed The salesorder Named To Cancel -".$invnum."";$val = "Cancelled";
		} else {
			$notimsg = $empname." Changed The salesorder Named To Draft -".$invnum."";$val = "Active"; 
		}
		$this->Basefunctions->notificationcontentadd($primaryid,$val,$notimsg,1,$_GET['salesordermodule']); 
		echo TRUE;
	}
	/*	* salesorder stage-data retrival for Lost-Cancel-Draft-Convert	*/
	public function salesordercurrentstage($salesorderid){
		$data=$this->db->select('crmstatusid')
					->from('salesorder')
					->where('salesorderid',$salesorderid)
					->where('status',$this->Basefunctions->activestatus)
					->limit(1)
					->get();
		foreach($data->result() as $info){
			$salesorderstage=$info->crmstatusid;
		}
		$array=array('salesorderstage'=>$salesorderstage);
		return $array;
	}
	//bookquote the quote
	public function booksalesorder() {	
		$primaryid=$_GET['primarydataid'];	
		$dbdata=$this->salesordercurrentstage($primaryid); //retrieves the existing stage		
		$prev_stage=$dbdata['salesorderstage'];
		$current_stage = $this->bookedstage;
		if($prev_stage == $this->bookedstage)//if both on same stage then return to draft
		{
			$current_stage=$this->draftstage;
		}		
		$updatearray=array('lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid,'crmstatusid'=>	$current_stage);
		$this->db->where('salesorderid',$primaryid);
		$this->db->update('salesorder',$updatearray);
		//Notification On Cancel-Return to Draft
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		$salesordernum = $this->Basefunctions->generalinformaion('salesorder','salesordernumber','salesorderid',$primaryid);
		if($prev_stage != $this->bookedstage)  { 
			$notimsg = $empname." Changed The salesorder Named To Booked -".$salesordernum."";$val = "Booked";
		} else {
			$notimsg = $empname." Changed The salesorder Named To Draft -".$salesordernum."";$val = "Active"; 
		}
		$this->Basefunctions->notificationcontentadd($primaryid,$val,$notimsg,1,$_GET['salesordermodule']); 
		echo TRUE;
	}
	//product instock fetch
	public function productstoragefetchfunmodel($productid){
		$this->load->model('Product/Productmodel');
		$totalstock = $this->Productmodel->productstoragefetchfunmodel($productid);
		return $totalstock;
	}
	/*	* invoice stage-data retrival for Lost-Cancel-Draft-Convert	*/
	public function invoicecurrentstage($invoiceid){
		$data=$this->db->select('crmstatusid')
					->from('salesorder')
					->where('salesorderid',$invoiceid)
					->where('status',$this->Basefunctions->activestatus)
					->limit(1)
					->get();
		foreach($data->result() as $info){
			$salesorderstage=$info->crmstatusid;
		}
		$array=array('salesorderstage'=>$salesorderstage);
		return $array;
	}
}