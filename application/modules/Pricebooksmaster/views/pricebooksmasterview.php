<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
</head>
<body>
<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<?php
				$moduleid = '218,219';
				$dataset['gridtitle'] = $gridtitle['title'];
				$dataset['titleicon'] = $gridtitle['titleicon'];
				$dataset['formtype'] = 'multipleaddform';
				$dataset['multimoduleid'] = $moduleid;
				$this->load->view('Base/mainviewaddformheader',$dataset);
			?>
			<div class="large-12 columns paddingzero scrollbarclass pricebookmastertouch mastermodules">
				<div class="mastermodules">
				<?php
					
					//function call for dash board form fields generation
					dashboardformfieldsgeneratorwithgrid($moddbfrmfieldsgen,1,$moduleid);
				?>			
				</div>	
			</div>			
		</div>
	</div>
</div>
<?php
	$device = $this->Basefunctions->deviceinfo();
	if($device=='phone') {
		$this->load->view('Base/overlaymobile');
	} else {
		$this->load->view('Base/overlay');
	}
	$this->load->view('Base/modulelist');
?>
</body>
	<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<?php $this->load->view('Base/bottomscript'); ?>
	<script>
		//Tabgroup Dropdown More
		// fwgautocollapse();
		$("#tab1").click(function()	{
			$('#pricebookdataupdatesubbtn').hide();
			$('#pricebooksavebutton,#pricebookdeleteicon').show();
			$(".tabclass").addClass('hidedisplay');
			$("#tab1class").removeClass('hidedisplay');
			resetFields(); mastertabid=1; masterfortouch = 0;
			});
		$("#tab2").click(function()
		{
			$('#pricebookdetaildataupdatesubbtn').hide();
			$('#pricebookdetailsavebutton,#pricebookdetaildeleteicon').show();
			$(".tabclass").addClass('hidedisplay');
			$("#tab2class").removeClass('hidedisplay');
			dynamicloaddropdown('pricebookid','pricebookid','pricebookname','pricebookid','pricebook','','');
			setTimeout(function(){
				pricebookdetailaddgrid();
			},100);
			resetFields();
			mastertabid=2; 
			masterfortouch = 0;
		});
		$(document).ready(function(){
			$("#tabgropdropdown").change(function(){
				var tabgpid = $("#tabgropdropdown").val();
				$('.dbsidebaricons[data-dbsubform="'+tabgpid+'"]').trigger('click');
			});
			{//for keyboard global variable
				 viewgridview = 0;
				 innergridview = 1;
			 }
		});	
	</script>
</html>