<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pricebooksmaster extends MX_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Pricebooksmaster/Pricebooksmastermodel');
    }
    //first basic hitting view
    public function index() {        
    	$moduleid = array(233);
    	sessionchecker($moduleid);
		//action
		$data['moddashboardtabgrp'] = $this->Basefunctions->moddashboardtabgrpgeneration($moduleid);
		$data['moddbfrmfieldsgen'] = $this->Basefunctions->dashboardmodsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$this->load->view('Pricebooksmaster/pricebooksmasterview',$data);
	}
}