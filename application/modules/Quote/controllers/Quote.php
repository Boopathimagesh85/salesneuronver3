<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Quote extends MX_Controller{
	//To load the Register View
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Quote/Quotemodel');
		$this->load->view('Base/formfieldgeneration');
		$this->load->helper('file');
    }
	public function index() {
		$moduleid = array(85,94,216);//$this->quotemodule;
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['calculationtype']=$this->Basefunctions->simpledropdown('calculationtype','calculationtypeid,calculationtypename','calculationtypename');
		$data['conv_currency']=$this->Basefunctions->simpledropdown('currency','currencyid,currencyname,currencycountry','currencyname');
		$data['additionalcategory']=$this->Basefunctions->simpledropdown('additionalchargecategory','additionalchargecategoryid,additionalchargecategoryname','additionalchargecategoryname');
		$data['category']=$this->Quotemodel->taxcategory();
		$data['adjusttype']=$this->Basefunctions->simpledropdown('adjustmenttype','adjustmenttypeid,adjustmenttypename','adjustmenttypename');
		$data['dashboradviewdata'] = $this->Basefunctions->dashboarddataviewbydropdown($moduleid);
		$this->load->view('Quote/quoteview',$data);	
	}
	/*	* currency with json encode-special use-decimal-dataattribute	*/
	public function specialcurrency() {
		$array = array();
		$data=$this->Basefunctions->simpledropdown('currency','currencyid,currencyname,decimalplaces','currencyname');
		foreach($data as $info){
				$array[]=array('currencyid'=>$info->currencyid,'currencyname'=>$info->currencyname,
								'decimal'=>$info->decimalplaces);
		}
		echo json_encode($array);
	}
	//Create New data
	public function newdatacreate() {  
    	$this->Quotemodel->newdatacreatemodel();
    }
	//information fetch
	public function fetchformdataeditdetails() {
		$moduleid = array(85,94,216);
		$this->Quotemodel->informationfetchmodel($moduleid);
	}
	//Update old data
    public function datainformationupdate() {
        $this->Quotemodel->datainformationupdatemodel();
    }
	//delete old information
    public function deleteinformationdata() {
    	$moduleid = array(85,94,216);
        $this->Quotemodel->deleteoldinformation($moduleid);
    }
	//primary and secondary address value fetch
	public function primaryaddressvalfetch() {
		$this->Quotemodel->primaryaddressvalfetchmodel();
	}
	//drop down value set with multiple condition
	public function fetchmaildddatawithmultiplecond() {
		$this->Quotemodel->fetchmaildddatawithmultiplecondmodel();
	}
	//quote-summary detail
	public function quotedetailfetch() {
		$this->Quotemodel->summarydetail();
	}
	public function quoteproductdetailfetch() {
		$this->Quotemodel->retrievequoteproductdetail();
	}
	//terms and condition data fetch
	public function termsandcontdatafetch() {
		$this->Quotemodel->termsandcontdatafetchmodel();
	}
	//editor value fetch 	
	public function editervaluefetch() {
		$filename = $_GET['filename']; 
		$newfilename = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]);
			echo json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			echo json_encode($tccontent);
		}
	}
	//cancel quote
	public function canceldata() {
		$this->Quotemodel->canceldata();
	}
	//lost the quote
	public function lostquote() {
		$this->Quotemodel->lostquote();
	}
	//bookquote
	public function bookquote() {
		$this->Quotemodel->bookquote();
	}
	//convert to so check
	public function checkconvertso()
	{
		$this->Quotemodel->checkconvertso();
	}
	//t and cid fetch
	public function tandcidfetch() {
		$this->Quotemodel->tandcidfetchmodel();
	}
	/* Charge gird header load */
	public function chargegirdheaderinformationfetch() {
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modname = $_GET['modulename'];
		$moduleid = $_GET['moduleid'];
		$colinfo = $this->Quotemodel->chargegirdheaderinformationfetchmodel($moduleid);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobileviewformtogriddatagenerate($colinfo,$modname,$width,$height);
		} else {
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height);
		}
		echo json_encode($datas);
	}
	//charge category load
	public function chargecategoryload() {
		$this->Quotemodel->chargecategoryload();
	}
	/* Charge gird header load */
	public function taxgirdheaderinformationfetch() {
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modname = $_GET['modulename'];
		$moduleid = $_GET['moduleid'];
		$colinfo = $this->Quotemodel->taxgirdheaderinformationfetchmodel($moduleid);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobileviewformtogriddatagenerate($colinfo,$modname,$width,$height);
		} else {
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height);
		}
		echo json_encode($datas);
	}
	//tax master load
	public function taxmasterload() {
		$this->Quotemodel->taxmasterload();
	}
	//get conversion rates
	public function getconversionrate(){
		$fromcurrency=trim($_GET['fromcurrency']);
		$tocurrency=trim($_GET['tocurrency']);
		$quotedate = $_GET['quotedate'];
		$result=$this->Basefunctions->getcurrencyconversionrate($fromcurrency,$tocurrency,$quotedate);
		echo json_encode(array('rate'=>$result));
	}
	//get account depend data
	public function accountdependddata(){
		$this->Quotemodel->accountdependddata();
	}	
	//get the pricebook currency and conversion rates
	public function pricebookcurrency(){
		$this->Quotemodel->pricebookcurrency();
	}
	/*	* Quote stage-data retrival for Lost-Cancel-Draft-Convert	*/
	public function quotecurrentstage(){
		$quoteid = trim($_GET['quoteid']);
		$data=$this->Quotemodel->quotecurrentstage($quoteid);
		echo json_encode($data);
	}
	public function quotecurrentstatus(){
		$quoteid = trim($_GET['quoteid']);
		$data=$this->Quotemodel->quotecurrentstatus($quoteid);
		echo json_encode($data);
	}
	//retrieve dropdown options
	public function getdropdownoptions() {
		$table=$_GET['table'];		
		$this->Quotemodel->getdropdownoptions($table);
	}
	/*	*get module number	*/
	public function getmodulenumber(){
		$number = '';
		$table=trim($_GET['table']);
		$id=trim($_GET['id']);
		$field=$table.'number';
		$primaryfield=$table.'id';
		$data=$this->db->select("$field")->from($table)->where($primaryfield,$id)->limit(1)->get();
		foreach($data->result() as $info){
			$number = $info->$field;
		}
		echo $number;
	}	
	public function getmoduleid(){
		$modulearray = array(85,94,216);
		$data=$this->db->select("moduleid")->from("module")->where_in('moduleid',$modulearray)->where('status',1)->limit(1)->get();
		foreach($data->result() as $info){
			$moduleid = $info->moduleid;
		}
		echo $moduleid;
	}
	//product storage fetch - instock
	public function productstoragefetchfun() {
		$productid = $_GET['productid'];
		$totalstock = $this->Quotemodel->productstoragefetchfunmodel($productid);
		echo json_encode($totalstock);
	}
}