<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Model File
class Quotemodel extends CI_Model{
	private $cancelstage  = 35;
	private $loststage    = 34;
	private $draftstage	  = 32;	
	private $bookedstage  = 33;
	public function __construct() {	
		parent::__construct(); 
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	/**	*Create Quotation.	*/
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		$gridfieldpartabname = explode(',',$_POST['griddatapartabnameinfo']);
		$gridrows = explode(',',$_POST['numofrows']);
		$griddata = $_POST['griddatas'];	
		$griddatainfo = json_decode($griddata, true);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fieldstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fieldstable);
		//filter unique grid parent table
		$gridpartablename =  $this->Crudmodel->filtervalue($gridfieldpartabname);
		$restricttable = explode(',',$_POST['resctable']);
		if(!isset($_POST['termsandconditionid'])){
			$_POST['termsandconditionid'] = 1; 
		}
		//industry based moduleid
		$moduleid = $_POST['viewfieldids'];
		//Retrieve the Quote autoNumber
		$anfieldnameinfo = $_POST['anfieldnameinfo'];
		$anfieldnameidinfo = $_POST['anfieldnameidinfo'];
		$anfieldtabinfo = $_POST['anfieldtabinfo'];
		$anfieldnamemodinfo = $_POST['anfieldnamemodinfo'];
		$randomnum = $this->Basefunctions->randomnumbergenerator($anfieldnamemodinfo,$anfieldtabinfo,$anfieldnameinfo,$anfieldnameidinfo);
		$_POST['quotenumber']=trim($randomnum);
		//dynamic Insertion
		$primaryid = $this->Crudmodel->datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable);	
		if(isset($_POST['taxmasterid'])){
			$taxmode = 3;
		}else{
			$taxmode = 2;
		}
		$softwareindustryid = $this->Basefunctions->industryid;
		if($softwareindustryid == 2){
			$addamountmode= '';
		}else{
			if(isset($_POST['additionalchargecategoryid'])){
				$addamountmode= 3;
			}else{
				$addamountmode= 2;
			}
		}	
		//insert the tax/add/discount mode detail
		//summary discount type-adjustment type
		$adjustmenttypeid = '';
		$discounttypeid = '';
		$discountvalue = '';
		$quote_discount = json_decode($_POST['groupdiscountdata'], true);
		$quote_adjustment = json_decode($_POST['groupadjustmentdata'], true);		
		if(count($quote_discount) > 0){
			if(isset($quote_discount['typeid']) AND isset($quote_discount['value'])){
				$discounttypeid = $quote_discount['typeid'];
				$discountvalue = $quote_discount['value'];
			}
		}
		if(count($quote_adjustment) > 0){
			if(isset($quote_adjustment['typeid'])){
				$adjustmenttypeid=$quote_adjustment['typeid'];
			}
		}		
		//update the quote records with mode/overlay types
		$otherdetail=array('taxmodeid'=>$taxmode,'additionalchargemodeid'=>$addamountmode,'groupdiscounttypeid'=>$discounttypeid,'groupdiscountpercent'=>$discountvalue,'adjustmenttypeid'=>$adjustmenttypeid);
		$filterArray = array_filter($otherdetail);		
		if(count($filterArray) != 0){
		$this->db->where('quoteid',$primaryid);
		$this->db->update('quote',$filterArray);
		}
		//address
		$arrname=array('billing','shipping');	
		$this->Crudmodel->addressdatainsert($arrname,$partablename,$primaryid);		
		$date = date($this->Basefunctions->datef);	
		//grid data insertion
		//primary key
		$defdataarr = $this->Crudmodel->defaultvalueget();
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$m=0;
		$h=1;
		$grouptaxdetail=json_decode($_POST['grouptaxgriddata'],true);
		$groupchargedetail=json_decode($_POST['groupchargegriddata'],true);
		//Group Tax Insert
		if($taxmode == 3 && count($grouptaxdetail) > 0) {								
			$tax_category_id=$grouptaxdetail['id'];
			$tax_data=$grouptaxdetail['data'];
			for($m=0;$m < count($tax_data);$m++) {
				$tax_array = array('moduleid'=>$_POST['quotemodule'],'singlegrouptypeid'=>$taxmode,'taxmasterid'=>$tax_category_id,'taxid'=>$tax_data[$m]['taxid'],'taxvalue'=>$tax_data[$m]['rate'],'taxamount'=>$tax_data[$m]['amount'],'id'=>$primaryid);
				$tax_array = array_filter(array_merge($tax_array,$defdataarr));
				$this->db->insert('moduletaxdetail',$tax_array);
			}
		}
		//Group Charge Insert
		if($addamountmode == 3 && count($groupchargedetail) > 0) {			
			$charge_category_id=$groupchargedetail['id'];
			$charge_data=$groupchargedetail['data'];
			for($m=0;$m<count($charge_data);$m++) {
				$charge_array=array('moduleid'=>$_POST['quotemodule'],'singlegrouptypeid'=>$addamountmode,'additionalchargecategoryid'=>$charge_category_id,'additionalchargetypeid'=>$charge_data[$m]['additionalchargetypeid'],'calculationtypeid'=>$charge_data[$m]['calculationtypeid'],'amount'=>$charge_data[$m]['amount'],'value'=>$charge_data[$m]['value'],'id'=>$primaryid);
				$charge_array= array_filter(array_merge($charge_array,$defdataarr));
				$this->db->insert('modulechargedetail',$charge_array);
			}		
		}
		foreach($gridrows as $rowcount) {
			$gridfieldsname = explode(',',$_POST['gridcolnames'.$h]);
			//filter grid unique fields table
			$gridfieldtabname = explode(',',$_POST['griddatatabnameinfo'.$h]);
			$gridfieldstable = $this->Crudmodel->filtervalue($gridfieldtabname);
			$gridtableinfo = explode(',',$gridfieldstable);
			for($i=0;$i<$rowcount;$i++) {
				foreach( $gridtableinfo as $gdtblname ) {
					$gridfieldsnames =array();
					$quotedetail=$this->db->select('columnname')
					->from('modulefield')
					->where('tablename',$gdtblname)
					->where('status',$this->Basefunctions->activestatus)
					->get();
					foreach($quotedetail->result() as $info) {
						$gridfieldsnames[]=$info->columnname;
					}
					${'$gcdata'.$m} = array();
					$t=0;
					foreach($gridfieldsnames AS $gdfldsname) {
						if(strcmp( trim($gridfieldtabname[$t]),trim($gdtblname) ) == 0 ) {
							if( isset( $griddatainfo[$i][$gdfldsname] ) ) {
								$name = explode('_',$gdfldsname);
								if( ctype_alpha($griddatainfo[$i][$gdfldsname]) && $griddatainfo[$i][$gdfldsname] != "" ) {									
									${'$gcdata'.$m}[$gdfldsname] = ucwords( $griddatainfo[$i][$gdfldsname] );
								} else if( $griddatainfo[$i][$gdfldsname] != "" ) {									
									${'$gcdata'.$m}[$gdfldsname] = $griddatainfo[$i][$gdfldsname];							
								}
							}
						}
					}
					if(count(${'$gcdata'.$m})>0) {
						${'$gcdata'.$m}[$primaryname]=$primaryid;
						$gnewdata = array_merge(${'$gcdata'.$m},$defdataarr);
						//individual
						$individualtaxdetail=json_decode($griddatainfo[$i]['taxgriddata'],true);$individualaddamountdetail=json_decode($griddatainfo[$i]['chargegriddata'],true);					
						//unset the hidden variables
						$removehidden=array('quotedetailid','taxgriddata','chargegriddata','discountdata');
						for($mk=0;$mk<count($removehidden);$mk++){						
							unset($gnewdata[$removehidden[$mk]]);
						}
						$this->db->insert($gdtblname,array_filter($gnewdata));
						$quotedetailid=$this->db->insert_id();					
						//product detail discount type						
						$discounttypeid = '';
						$discountvalue = '';
						$quote_discount = json_decode($griddatainfo[$i]['discountdata'], true);						
						if(count($quote_discount) > 0){
							if(isset($quote_discount['typeid']) AND isset($quote_discount['value'])){
							$discounttypeid = $quote_discount['typeid'];
							$discountvalue = $quote_discount['value'];
							//update the product detail records with mode/overlay types
							$productdetail=array('discounttypeid'=>$discounttypeid,'discountpercent'=>$discountvalue);
							$this->db->where('quotedetailid',$quotedetailid);
							$this->db->update('quotedetail',array_filter($productdetail));
							}
						}					
						//individual tax data insertion
						if($taxmode == 2 && count($individualtaxdetail) > 0) {
							if(isset($individualtaxdetail['id']) AND isset($individualtaxdetail['data'])){
								$tax_category_id=$individualtaxdetail['id'];
								$tax_data=$individualtaxdetail['data'];
								for($m=0;$m < count($tax_data);$m++) {
									$tax_array=array('moduleid'=>$_POST['quotemodule'],'singlegrouptypeid'=>$taxmode,'taxmasterid'=>$tax_category_id,'taxid'=>$tax_data[$m]['taxid'],
									'taxvalue'=>$tax_data[$m]['rate'],'taxamount'=>$tax_data[$m]['amount'],'id'=>$quotedetailid);
									$tax_array= array_filter(array_merge($tax_array,$defdataarr));
									$this->db->insert('moduletaxdetail',$tax_array);
								}
							}							
						}						
						if($addamountmode == 2 && count($individualaddamountdetail) > 0) {		
							if(isset($individualaddamountdetail['id']) AND isset($individualaddamountdetail['data'])){
								$charge_category_id=$individualaddamountdetail['id'];
								$charge_data=$individualaddamountdetail['data'];
								for($m=0;$m<count($charge_data);$m++) {
									$charge_array=array('moduleid'=>$_POST['quotemodule'],'singlegrouptypeid'=>$addamountmode,'additionalchargecategoryid'=>$charge_category_id,'additionalchargetypeid'=>$charge_data[$m]['additionalchargetypeid'],'calculationtypeid'=>$charge_data[$m]['calculationtypeid'],'amount'=>$charge_data[$m]['amount'],'value'=>$charge_data[$m]['value'],'id'=>$quotedetailid);
									$charge_array= array_filter(array_merge($charge_array,$defdataarr));
									$this->db->insert('modulechargedetail',$charge_array);
								}
							}
						}	
					}
					$m++;
				}
			}
			$h++;
		}
		//terms and condition update
		$tandcid = $_POST['termsandconditionid'];
		//notification entry
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		if(isset($_POST['employeeid'])) {
			$assignid = $_POST['employeeid'];
			$emptypes = $_POST['employeetypeid'];
			$empdataids = array();
			$assignempids = array();
			if($assignid != '') {
				$i = 0;
				$m=0;
				$empiddatas = explode(',',$assignid);
				$emptypeids = explode(',',$emptypes);
				foreach($empiddatas as $empids) {
					$emptype = $emptypeids[$m];
					if($emptype == 1) {
						$empdataids[$i] = $empids;
						$i++;
					} else if($emptype == 2) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromgroup($empids);
						$i++;
					} else if($emptype == 3) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromroles($empids);
						$i++;
					} else if($emptype == 4) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromrolesandsubroles($empids);
						$i++;
					}
					$m++;
				}
			} else {
				$assignid = 1;
			}
		} else {
			$assignid = 1;
		}
		$quotname = $_POST['quotenumber'];
		if($assignid == '1'){
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,2);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,$moduleid);
			}
		} else {
			foreach($empdataids as $empidinfo) {
				if(is_array($empidinfo)) { //group of employees
					foreach($empidinfo as $empid) {
						if( !in_array($empid,$assignempids) ) {
							array_push($assignempids,$empid);
							$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,2);
							if($notimsg != '') {
								$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,$moduleid);
							}
						}
					}
				} else { //individual employees
					if( !in_array($empidinfo,$assignempids) ) {
						array_push($assignempids,$empidinfo);
						$notimsg = $this->Basefunctions->notificationtemplatedatafetch(216,$primaryid,$partablename,2);
						if($notimsg != '') {
							$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,216);
						}
					}
				}
			}
		}
		{//workflow management -- for data create
			$this->Basefunctions->workflowmanageindatacreatemode($moduleid,$primaryid,$partablename);
		}
		echo 'TRUE';
	}
	public function termsandconditionnamefetchandupdate($primaryid,$tandcid){
		$this->db->select('quotationstermsandcondition_editorfilename');
		$this->db->from('quote');
		$this->db->where('quote.quoteid',$primaryid);
		$result=$this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = $row->quotationstermsandcondition_editorfilename;
			}
		}
		if($tandcid != "") {
			$tcdata = array('termsandconditionstermsandconditions_editorfilename'=>$data);
			$this->db->where('termsandcondition.termsandconditionid',$tandcid);
			$this->db->update('termsandcondition',$tcdata);
		}
	}
	//information fetch
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementpartable']);
		$restricttable = array('quotedetail','termsandcondition');
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['dataprimaryid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetchwithrestrict($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$restricttable,$moduleid);
		//?retrieve summary data
		$ext_data=$this->db->select('pricebookcurrencyid,currentcurrencyid,pricebook_currencyconvrate,taxmodeid,additionalchargemodeid,groupdiscounttypeid,groupdiscountpercent,adjustmenttypeid,adjustmentamount')
							->from('quote')
							->where('quoteid',$primaryid)
							->limit(1)
							->get();
		foreach($ext_data->result() as $value){
			$detail_parameter=array('id'=>$primaryid,'taxmode'=>$value->taxmodeid,
									'chargemode'=>$value->additionalchargemodeid,'discounttype'=>$value->groupdiscounttypeid,'discountpercent'=>$value->groupdiscountpercent,'adjustmenttype'=>$value->adjustmenttypeid,'adjustmentvalue'=>$value->adjustmentamount,'pricebookcurrencyid'=>$value->pricebookcurrencyid,'currentcurrencyid'=>$value->currentcurrencyid,'pricebook_currencyconvrate'=>$value->pricebook_currencyconvrate);
		}							
		$summary['summary']=$this->quote_groupdetail($_GET['quotemodule'],$detail_parameter);	
		$result = json_decode($result,true);
		$cresult= array_merge($result,$summary);
		$result=json_encode($cresult);
		echo $result;
	}
	//update data information
	public function datainformationupdatemodel(){
		if(isset($_POST['termsandconditionid'])){
			if($_POST['termsandconditionid'] == ''){
				$_POST['termsandconditionid'] = 1;
				$_POST['quotationstermsandcondition_editorfilename'] = '';
			}						
		}
		if(!isset($_POST['contactid'])){
			$_POST['contactid'] = 1;
		}
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//industry based moduleid
		$moduleid = $_POST['viewfieldids'];
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['primarydataid'];
		{//fetch old values -- work flow
			$condstatvals=$this->Basefunctions->workflowmanageolddatainfofetch($moduleid,$primaryid);
		}
		$restricttable = explode(',',$_POST['resctable']);
		$result = $this->dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable);
		if(isset($_POST['taxmasterid'])){
			$taxmode = 3;
		}else{
			$taxmode = 2;
		}
		$softwareindustryid = $this->Basefunctions->industryid;
		if($softwareindustryid == 2){
			$addamountmode= '';
		}else{
			if(isset($_POST['additionalchargecategoryid'])){
				$addamountmode= 3;
			}else{
				$addamountmode= 2;
			}
		}
		//insert the tax/add/discount mode detail
		//summary discount type-adjustment type
		$adjustmenttypeid = '';
		$discounttypeid = '';
		$discountvalue = '';
		$quote_discount = json_decode($_POST['groupdiscountdata'], true);
		$quote_adjustment = json_decode($_POST['groupadjustmentdata'], true);
		
		if(count($quote_discount) > 0){
			if(isset($quote_discount['typeid']) AND isset($quote_discount['value'])){
				$discounttypeid = $quote_discount['typeid'];
				$discountvalue = $quote_discount['value'];
			}
		}
		if(count($quote_adjustment) > 0){
			if(isset($quote_adjustment['typeid']) ){
				$adjustmenttypeid=$quote_adjustment['typeid'];
			}
		}
		//update the quote records with mode/overlay types
		$otherdetail=array('taxmodeid'=>$taxmode,'additionalchargemodeid'=>$addamountmode,'groupdiscounttypeid'=>$discounttypeid,'groupdiscountpercent'=>$discountvalue,'adjustmenttypeid'=>$adjustmenttypeid);
		if (array_filter($otherdetail)) {
			$this->db->where('quoteid',$primaryid);
			$this->db->update('quote',array_filter($otherdetail));
		}
		//address update
		$arrname=array('billing','shipping');
		$this->Crudmodel->addressdataupdate($arrname,$partablename,$primaryid);		
		//product detail update
		//get the old product detail
		$oldquotedetailid =array();
		$quotedetail=$this->db->select('quotedetailid')
								->from('quotedetail')
								->where('quoteid',$primaryid)
								->where('status',$this->Basefunctions->activestatus)
								->get();
		foreach($quotedetail->result() as $info) {
			$oldquotedetailid[]=$info->quotedetailid;
		}		
		$gridfieldpartabname = explode(',',$_POST['griddatapartabnameinfo']);
		$gridrows = explode(',',$_POST['numofrows']);
		$griddata = $_POST['griddatas'];
		$griddatainfo = json_decode($griddata, true);	
		//filter unique grid parent table
		$gridpartablename =  $this->Crudmodel->filtervalue($gridfieldpartabname);			
		for($lm=0;$lm < count($griddatainfo);$lm++) {
			$newquotedetailid[]=$griddatainfo[$lm]['quotedetailid'];
		}
		$deletedquotedetailid=ARRAY();
		//find deleted records
		for($m=0;$m < count ($oldquotedetailid);$m++) {
			if(in_array($oldquotedetailid[$m],$newquotedetailid)) {			
			} else {
				$deletedquotedetailid[]=$oldquotedetailid[$m];
			}
		}
		if(count($deletedquotedetailid) > 0) {
			//delete productdetail and further tables
			for($k=0;$k<count($deletedquotedetailid);$k++) {
				$this->Crudmodel->outerdeletefunction('quotedetail','quotedetailid','',$deletedquotedetailid[$k]);
			}
			//delete the additional/tax/discount if exits
			$updateloginfo = $this->Crudmodel->updatedefaultvalueget();
			$moduledelete=array('status'=>$this->Basefunctions->deletestatus,'lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid);
			$final=array_merge($moduledelete,$updateloginfo);
			$moduledetailtable=array('moduletaxdetail','modulechargedetail');
			//delete the sub tables(moduletaxdetail,modulechargedetail)
			foreach($moduledetailtable as $table) {
				$this->db->where('moduleid',$_POST['quotemodule']);
				$this->db->where('singlegrouptypeid',2);
				$this->db->where_in('id',$deletedquotedetailid);
				$this->db->update($table,$final);
			}			
		}
		//Insert the New quote records
		$defdataarr = $this->Crudmodel->defaultvalueget();
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$m=0;
		$h=1;
		foreach($gridrows as $rowcount) {
			$gridfieldsname = explode(',',$_POST['gridcolnames'.$h]);
			//filter grid unique fields table
			$gridfieldtabname = explode(',',$_POST['griddatatabnameinfo'.$h]);
			$gridfieldstable = $this->Crudmodel->filtervalue($gridfieldtabname);
			$gridtableinfo = explode(',',$gridfieldstable);
			for($i=0;$i<$rowcount;$i++) {
				foreach( $gridtableinfo as $gdtblname ) {
					${'$gcdata'.$m} = array();
					$t=0;
					foreach($gridfieldsname AS $gdfldsname) {
						if(strcmp( trim($gridfieldtabname[$t]),trim($gdtblname) ) == 0 ) {
							if( isset( $griddatainfo[$i][$gdfldsname] ) ) {
								$name = explode('_',$gdfldsname);
								if( ctype_alpha($griddatainfo[$i][$gdfldsname]) && $griddatainfo[$i][$gdfldsname] != "" ) {									
									${'$gcdata'.$m}[$gdfldsname] = ucwords( $griddatainfo[$i][$gdfldsname] );
								} else if( $griddatainfo[$i][$gdfldsname] != "" ) {									
									${'$gcdata'.$m}[$gdfldsname] = $griddatainfo[$i][$gdfldsname];							
								}
							}
						}
					}
					if(count(${'$gcdata'.$m})>0) {
						${'$gcdata'.$m}[$primaryname]=$primaryid;
						$gnewdata = array_merge(${'$gcdata'.$m},$defdataarr);
						//individual
						$individualtaxdetail=json_decode($griddatainfo[$i]['taxgriddata'],true);						
						$individualaddamountdetail=json_decode($griddatainfo[$i]['chargegriddata'],true);					
						//unset the hidden variables
						$removehidden=array('quotedetailid','taxgriddata','chargegriddata','discountdata');
						for($mk=0;$mk<count($removehidden);$mk++){						
							unset($gnewdata[$removehidden[$mk]]);
						}
						if($griddatainfo[$i]['quotedetailid'] == 0){ //new-data
							$this->db->insert($gdtblname,array_filter($gnewdata));
							$quotedetailid=$this->db->insert_id();
						}
						if($griddatainfo[$i]['quotedetailid'] > 0){ //update-existing data
							$this->db->where('quoteid',$griddatainfo[$i]['quotedetailid']);
							$this->db->update($gdtblname,array_filter($gnewdata));
							$quotedetailid=$griddatainfo[$i]['quotedetailid'];
							//remove old data.
							$delete_sub_data=array('lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid,'status'=>$this->Basefunctions->deletestatus);
							$moduledetailtable=array('moduletaxdetail','modulechargedetail');
							//delete the sub tables(moduletaxdetail,modulechargedetail)
							foreach($moduledetailtable as $table)
							{
								$this->db->where('moduleid',$_POST['quotemodule']);
								$this->db->where('singlegrouptypeid',2);
								$this->db->where_in('id',$griddatainfo[$i]['quotedetailid']);
								$this->db->update($table,$delete_sub_data);
							}
						}
						//product detail discount type						
						$discounttypeid = '';
						$discountvalue = '';
						$quote_discount = json_decode($griddatainfo[$i]['discountdata'], true);						
						if(count($quote_discount) > 0){
							if(isset($quote_discount['typeid']) AND isset($quote_discount['value'])){
							$discounttypeid = $quote_discount['typeid'];
							$discountvalue = $quote_discount['value'];
							//update the product detail records with mode/overlay types
							$productdetail=array('discounttypeid'=>$discounttypeid,'discountpercent'=>$discountvalue);
							$this->db->where('quotedetailid',$quotedetailid);
							$this->db->update('quotedetail',array_filter($productdetail));
							}
						}					
						//individual tax data insertion
						if($taxmode == 2 && count($individualtaxdetail) > 0) {								
							if(isset($individualtaxdetail['id']) AND isset($individualtaxdetail['data'])){
							$tax_array = array();
							$tax_category_id=$individualtaxdetail['id'];
							$tax_data=$individualtaxdetail['data'];
							for($m=0;$m < count($tax_data);$m++) {
								$tax_array=array('moduleid'=>$_POST['quotemodule'],'singlegrouptypeid'=>$taxmode,'taxmasterid'=>$tax_category_id,'taxid'=>$tax_data[$m]['taxid'],
								'taxvalue'=>$tax_data[$m]['rate'],'taxamount'=>$tax_data[$m]['amount'],'id'=>$quotedetailid);
								$tax_array= array_filter(array_merge($tax_array,$defdataarr));
								$this->db->insert('moduletaxdetail',$tax_array);
							}					
							}
						}						
						if($addamountmode == 2 && count($individualaddamountdetail) > 0) {
							if(isset($individualaddamountdetail['id']) AND isset($individualaddamountdetail['data'])){
							$charge_category_id=$individualaddamountdetail['id'];
							$charge_data=$individualaddamountdetail['data'];							
							for($m=0;$m<count($charge_data);$m++) {
								$charge_array=array('moduleid'=>$_POST['quotemodule'],'singlegrouptypeid'=>$addamountmode,'additionalchargecategoryid'=>$charge_category_id,'additionalchargetypeid'=>$charge_data[$m]['additionalchargetypeid'],'calculationtypeid'=>$charge_data[$m]['calculationtypeid'],'amount'=>$charge_data[$m]['amount'],'value'=>$charge_data[$m]['value'],'id'=>$quotedetailid);
								$charge_array = array_filter(array_merge($charge_array,$defdataarr));
								$this->db->insert('modulechargedetail',$charge_array);
							}
							}							
						}	
					}
					$m++;
				}
			}
			$h++;
		}
		//summary update	
		//group tax
		$grouptaxdetail=json_decode($_POST['grouptaxgriddata'],true);
		$groupchargedetail=json_decode($_POST['groupchargegriddata'],true);
		$deletearray=array('lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid,'status'=>$this->Basefunctions->deletestatus);
		//resets the moduletaxdetail/modulechargedetail entries
		$moduledetailtable=array('moduletaxdetail','modulechargedetail');
		//delete the sub tables(moduletaxdetail,modulechargedetail)
		foreach($moduledetailtable as $table) {
			$this->db->where('moduleid',$_POST['quotemodule']);
			$this->db->where('singlegrouptypeid',3);
			$this->db->where_in('id',$primaryid);
			$this->db->update($table,$deletearray);
		}
		if($taxmode == 3 && count($grouptaxdetail) > 0) {			
			//insert new data
			if(isset($grouptaxdetail['id']) AND isset($grouptaxdetail['data'])){
				$tax_category_id=$grouptaxdetail['id'];
				$tax_data=$grouptaxdetail['data'];
				for($m=0;$m < count($tax_data);$m++) {
					$tax_array=array('moduleid'=>$_POST['quotemodule'],'singlegrouptypeid'=>$taxmode,'taxmasterid'=>$tax_category_id,'taxid'=>$tax_data[$m]['taxid'],
					'taxvalue'=>$tax_data[$m]['rate'],'taxamount'=>$tax_data[$m]['amount'],'id'=>$primaryid);
					$tax_array= array_filter(array_merge($tax_array,$defdataarr));
					$this->db->insert('moduletaxdetail',$tax_array);
				}					
			}
		}				
		if($addamountmode == 3 && count($groupchargedetail) > 0) {				
			//insert new data
			if(isset($groupchargedetail['id']) AND isset($groupchargedetail['data'])){
			$charge_category_id=$groupchargedetail['id'];
			$charge_data=$groupchargedetail['data'];
			for($m=0;$m<count($charge_data);$m++) {
				$charge_array=array('moduleid'=>$_POST['quotemodule'],'singlegrouptypeid'=>$addamountmode,'additionalchargecategoryid'=>$charge_category_id,'additionalchargetypeid'=>$charge_data[$m]['additionalchargetypeid'],'calculationtypeid'=>$charge_data[$m]['calculationtypeid'],'amount'=>$charge_data[$m]['amount'],'value'=>$charge_data[$m]['value'],'id'=>$primaryid);
				$charge_array = array_filter(array_merge($charge_array,$defdataarr));
				$this->db->insert('modulechargedetail',$charge_array);
			}
			}
		} 
		//notification entry
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		if(isset($_POST['employeeid'])) {
			$assignid = $_POST['employeeid'];
			$emptypes = $_POST['employeetypeid'];
			$empdataids = array();
			$assignempids = array();
			if($assignid != '') {
				$i = 0;
				$m=0;
				$empiddatas = explode(',',$assignid);
				$emptypeids = explode(',',$emptypes);
				foreach($empiddatas as $empids) {
					$emptype = $emptypeids[$m];
					if($emptype == 1) {
						$empdataids[$i] = $empids;
						$i++;
					} else if($emptype == 2) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromgroup($empids);
						$i++;
					} else if($emptype == 3) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromroles($empids);
						$i++;
					} else if($emptype == 4) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromrolesandsubroles($empids);
						$i++;
					}
					$m++;
				}
			} else {
				$assignid = 1;
			}
		} else {
			$assignid = 1;
		}
		$quotname = $_POST['quotenumber'];
		if($assignid == '1'){
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,3);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,$moduleid);
			}
		} else {
			foreach($empdataids as $empidinfo) {
				if(is_array($empidinfo)) { //group of employees
					foreach($empidinfo as $empid) {
						if( !in_array($empid,$assignempids) ) {
							array_push($assignempids,$empid);
							$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,3);
							if($notimsg != '') {
								$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,$moduleid);
							}
						}
					}
				} else { //individual employees
					if( !in_array($empidinfo,$assignempids) ) {
						array_push($assignempids,$empidinfo);
						$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,3);
						if($notimsg != '') {
							$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,$moduleid);
						}
					}
				}
			}
		}
		{//workflow management for data update
			$this->Basefunctions->workflowmanageindataupdatemode($moduleid,$primaryid,$partablename,$condstatvals);
		}
		echo true;
	} 
	//delete old information
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primarydataid'];
		$msg='False';
		$filename = $this->Basefunctions->generalinformaion('quote','quotenumber','quoteid',$id);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//industrybased moduleid fetch
		$modid = $this->Basefunctions->getmoduleid($moduleid);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		foreach (array_keys($ctable,'salesorder',true) as $key) {
			unset($ctable[$key]);
		}		
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		{//workflow management -- for data delete
			$this->Basefunctions->workflowmanageindatadeletemode($modid,$id,$primaryname,$partabname);
		}
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				$msg='Denied';
			} else {				
				// module detail
				$this->Basefunctions->deletetaxaddchargedetail($_GET['quotemodule'],'quote',$id);							
				//notification log entry
				$empid = $this->Basefunctions->logemployeeid;
				$notimsg = $this->Basefunctions->notificationtemplatedatafetch($modid,$id,$partabname,4);
				if($notimsg != '') {
					$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,$modid);
				}
				$this->Crudmodel->outerdeletefunctionwithoutrestrict($partabname,$primaryname,$ctable,$id);
				$msg="TRUE";
			}
		} else {			
			// module detail
			$this->Basefunctions->deletetaxaddchargedetail($_GET['quotemodule'],'quote',$id);				
			//notification log entry
			$empid = $this->Basefunctions->logemployeeid;
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch($modid,$id,$partabname,4);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,$modid);
			}
			$this->Crudmodel->outerdeletefunctionwithoutrestrict($partabname,$primaryname,$ctable,$id);
			$msg="TRUE";
		}
		echo $msg;
	}	
	//primary and secondary address value fetch
	public function primaryaddressvalfetchmodel(){
		$accid = $_GET['dataprimaryid'];	
		$this->db->select('quoteaddress.addresssourceid,quoteaddress.address,quoteaddress.pincode,quoteaddress.city,quoteaddress.state,quoteaddress.country');
		$this->db->from('quoteaddress');
		$this->db->where('quoteaddress.quoteid',$accid);
		$this->db->where('quoteaddress.status',1);
		$this->db->order_by('quoteaddressid','asc');
		$this->db->limit(2);
		$result = $this->db->get();
		$arrname=array('billing','shipping');
		if($result->num_rows() >0) {
			$m=0;
			foreach($result->result() as $row) {
				$data[] = array($arrname[$m].'addresstype'=>$row->addresssourceid,
								$arrname[$m].'address'=>$row->address,
								$arrname[$m].'pincode'=>$row->pincode,
								$arrname[$m].'city'=>$row->city,
								$arrname[$m].'state'=>$row->state,
								$arrname[$m].'country'=>$row->country);
				$m++;
			}
			$finalarray=array_merge($data[0],$data[1]);
			echo json_encode($finalarray);
		} else {
		    echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//data update with restriction
	public function dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable) {
		//generate value array
		$m=0;
		$pdata = array();
		foreach( $tableinfo as $tblname ) {
			${'$cdata'.$m} = array();
			$i=0;
			if( !in_array(trim($tblname),$restricttable) ) {
				foreach( $formfieldscolmname as $fcolvalue ) {
					if( strcmp( trim($partablename),trim($formfieldstable[$i]) ) == 0 ) { //ptable
						if( isset( $_POST[$formfieldsname[$i]] ) ) {
							$name = explode('_',$formfieldsname[$i]);
							if( !is_array($_POST[$formfieldsname[$i]]) ) {
								if( ctype_alpha($_POST[$formfieldsname[$i]]) ) {
									$txtstring1 = "";
									if( in_array('password',$name) ) {
										$hash = password_hash($_POST[$formfieldsname[$i]], PASSWORD_DEFAULT);
										$pdata[$fcolvalue] = $hash ;
									} else if( in_array('editorfilename',$name) ) {
										$txtstring1 = trim($_POST[$formfieldsname[$i]]);
										$pdata[$fcolvalue] = $this->Crudmodel->generatefile($txtstring1);
									}  else {
										$pdata[$fcolvalue] = ucwords( $_POST[$formfieldsname[$i]] );
									}
								} else {
									$txtstring2 = "";
									if( in_array('password',$name) ) {
										$hash = password_hash($_POST[$formfieldsname[$i]], PASSWORD_DEFAULT);
										$pdata[$fcolvalue] = $hash ;
									} else if( in_array('editorfilename',$name) ) {
										$txtstring2 = trim($_POST[$formfieldsname[$i]]);
										$pdata[$fcolvalue] = $this->Crudmodel->generatefile($txtstring2);
									} else {
										$pdata[$fcolvalue] = $_POST[$formfieldsname[$i]];
									}
								}
							} else {
								$pdata[$fcolvalue] = implode(',',$_POST[$formfieldsname[$i]]);
							}
						}
					} else if( strcmp( trim($formfieldstable[$i]),trim($tblname) ) == 0 && strcmp( trim($partablename),trim($formfieldstable[$i]) ) != 0 ) { //ctable
						if( isset( $_POST[$formfieldsname[$i]] ) ) {
							$name = explode('_',$formfieldsname[$i]);
							if( !is_array($_POST[$formfieldsname[$i]]) ) {
								if( ctype_alpha($_POST[$formfieldsname[$i]]) ) {
									$txtstring3 = "";
									if( in_array('password',$name) ) {
										$hash = password_hash($_POST[$formfieldsname[$i]], PASSWORD_DEFAULT);
										${'$cdata'.$m}[$fcolvalue] = $hash ;
									} else if( in_array('editorfilename',$name) ) {
										$txtstring3 = trim($_POST[$formfieldsname[$i]]);
										${'$cdata'.$m}[$fcolvalue] = $this->Crudmodel->generatefile($txtstring3);
									} else {
										${'$cdata'.$m}[$fcolvalue] = ucwords( $_POST[$formfieldsname[$i]] );
									}
								} else {
									$txtstring4 = "";
									if( in_array('password',$name) ) {
										$hash = password_hash($_POST[$formfieldsname[$i]], PASSWORD_DEFAULT);
										${'$cdata'.$m}[$fcolvalue] = $hash ;
									} else if( in_array('editorfilename',$name) ) {
										$txtstring4 = trim($_POST[$formfieldsname[$i]]);
										${'$cdata'.$m}[$fcolvalue] = $this->Crudmodel->generatefile($txtstring4);
									} else {
										${'$cdata'.$m}[$fcolvalue] = $_POST[$formfieldsname[$i]];
									}
								}
							} else {
								${'$cdata'.$m}[$fcolvalue] = implode(',',$_POST[$formfieldsname[$i]]);
							}
						}
					}
					$i++;
				}
			}
			$m++;
		}
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		{//parent table update
			$defdataarr = $this->Crudmodel->updatedefaultvalueget();
			$newdata = array_merge($pdata,$defdataarr);
			//update information
			$this->db->where($primaryname,$primaryid);
			$this->db->update($partablename,array_filter($newdata));
		}
		{//child table update
			$m=0;
			foreach( $tableinfo as $tblname ) {
				$defdataarr = $this->Crudmodel->defaultvalueget();
				$cnewdata = array();
				if(count(${'$cdata'.$m}) > 0 ) {
					if( strcmp( trim($partablename),trim($tblname) ) != 0 ) {
						if( !in_array(trim($tblname),$restricttable) ) {
							//update default value
							$defdataarr = $this->Crudmodel->updatedefaultvalueget();
							$cnewdata = array_merge(${'$cdata'.$m},$defdataarr);
							//update information
							$this->db->where($primaryname,$primaryid);
							$this->db->update($tblname,array_filter($cnewdata));
						}
					}
				}
				$m++;
			}
		}
		return 'TRUE';
	}
	//dashboard task grid view
	public function quotepricebookviewmodel($sidx,$sord,$start,$limit,$wh)	{
		$pid = $_GET['productid'];
		$this->db->select('SQL_CALC_FOUND_ROWS pricebookdetail.pricebookdetailid,pricebook.pricebookname,product.productname,pricebookdetail.sellingprice,pricebook.description,currency.currencyname',false);
		$this->db->from('pricebookdetail');
		$this->db->join('product','product.productid=pricebookdetail.productid');
		$this->db->join('pricebook','pricebook.pricebookid=pricebookdetail.pricebookid');
		$this->db->join('currency','currency.currencyid=pricebook.currencyid');
		$this->db->where('pricebookdetail.Status',1);
		if($pid != '') {
			$this->db->where('pricebookdetail.productid',$pid);
		}
		if($wh != "1") {
			$this->db->where($wh);
		}
		$this->db->order_by($sidx,$sord);
        $this->db->limit($limit,$start);
		return $this->db->get();
	}
	/**
	* retrieve the quote product details
	*/	
	public function retrievequoteproductdetail() {
		$softwareindustryid = $this->Basefunctions->industryid;
		$quoteid=trim($_GET['primarydataid']);
		if($softwareindustryid == 4){
			$this->db->select('quotedetailid,quotedetail.quoteid,productname,quotedetail.productid,quotedetail.instock,quotedetail.quantity,quotedetail.unitprice,quotedetail.sellingprice,quotedetail.grossamount,quotedetail.chargeamount,quotedetail.taxamount,quotedetail.discountamount,quotedetail.netamount,quotedetail.descriptiondetail,quote.taxmodeid,quote.additionalchargemodeid,quotedetail.discounttypeid,quotedetail.discountpercent,quotedetail.pretaxtotal,quotedetail.durationid,quotedetail.joiningdate,quotedetail.expirydate,duration.durationname');
			$this->db->from('quotedetail');
			$this->db->join('quote','quote.quoteid=quotedetail.quoteid');
			$this->db->join('duration','duration.durationid=quotedetail.durationid');
			$this->db->join('product','product.productid=quotedetail.productid');
			$this->db->where('quotedetail.quoteid',$quoteid);
			$this->db->where('quotedetail.status',$this->Basefunctions->activestatus);
			$data=$this->db->get();	
		}else{
			$this->db->select('quotedetailid,quotedetail.quoteid,productname,quotedetail.productid,quotedetail.instock,quotedetail.quantity,quotedetail.unitprice,quotedetail.sellingprice,quotedetail.grossamount,quotedetail.chargeamount,quotedetail.taxamount,quotedetail.discountamount,quotedetail.netamount,quotedetail.descriptiondetail,quote.taxmodeid,quote.additionalchargemodeid,quotedetail.discounttypeid,quotedetail.discountpercent,quotedetail.pretaxtotal');
			$this->db->from('quotedetail');
			$this->db->join('quote','quote.quoteid=quotedetail.quoteid');
			$this->db->join('product','product.productid=quotedetail.productid');
			$this->db->where('quotedetail.quoteid',$quoteid);
			$this->db->where('quotedetail.status',$this->Basefunctions->activestatus);
			$data=$this->db->get();	
		}		
		if($data->num_rows() > 0) {
			$j=0;
			foreach($data->result() as $value) {
				$detail_parameter=array('id'=>$value->quotedetailid,'taxmode'=>$value->taxmodeid,
										'chargemode'=>$value->additionalchargemodeid,'discounttype'=>$value->discounttypeid,'discountpercent'=>$value->discountpercent);
				$detailarray=$this->quote_individualdetail($detail_parameter,$_GET['quotemodule']);
				$productdetail->rows[$j]['id'] = $value->quotedetailid;
				$softwareindustryid = $this->Basefunctions->industryid;
				if($softwareindustryid == 2){
					$productdetail->rows[$j]['cell']=array(
							$value->productname,
							$value->productid,
							$value->quantity,
							$value->sellingprice,
							$value->grossamount,
							$value->discountamount,
							$value->pretaxtotal,
							$value->taxamount,
							$value->netamount,
							$detailarray['tax'],
							$detailarray['discount'],
							$value->quotedetailid
					);
				}else if($softwareindustryid == 4){
					$productdetail->rows[$j]['cell']=array(
							$value->productname,
							$value->productid,
							$value->instock,
							$value->quantity,
							$value->durationname,
							$value->durationid,
							$value->joiningdate,
							$value->expirydate,
							$value->unitprice,
							$value->sellingprice,
							$value->grossamount,
							$value->discountamount,
							$value->pretaxtotal,
							$value->taxamount,
							$value->chargeamount,
							$detailarray['tax'],
							$value->netamount,
							$detailarray['addcharge'],
							$detailarray['discount'],
							$value->quotedetailid	
					);
				}else{
					$productdetail->rows[$j]['cell']=array(
							$value->productname,
							$value->productid,
							$value->instock,
							$value->quantity,
							$value->unitprice,
							$value->sellingprice,
							$value->grossamount,
							$value->discountamount,
							$value->pretaxtotal,
							$value->taxamount,
							$value->chargeamount,
							$value->netamount,
							$detailarray['tax'],
							$detailarray['addcharge'],
							$detailarray['discount'],
							$value->quotedetailid
					);
				}									
				$j++;		
			}
		}
		echo  json_encode($productdetail);
	}
	//retrieve the summary overlay detail 
	public function summarydetail() {
		$quoteid=$_GET['dataprimaryid'];
		$data=$this->db->select('leadid,contactid,taxmodeid,additionalchargemodeid,taxmasterid,additionalchargecategoryid,adjustmenttypeid,adjustmentamount,leadcontacttypeid')
						->from('quote')
						->where('quote.quoteid',$quoteid)						
						->get();
		$row=$data->row();
		
		$taxmodeid=$row->taxmodeid;		
		$addchargemodeid=$row->additionalchargemodeid;
		$adjustmenttype=$row->adjustmenttypeid;
		$adjustmentvalue=$row->adjustmentamount;
		if($row->leadcontacttypeid == 2) {
			$contactid=$row->contactid;
		}else{
			$contactid=$row->leadid;
		}		
		$modedetail=array('taxmodeid'=>$taxmodeid,'additionalchargemodeid'=>$addchargemodeid,'adjustmenttype'=>$adjustmenttype,'adjustmentvalue'=>$adjustmentvalue,'contactid'=>$contactid);
		
		$taxarray=array();		
		$addamountarray=array();
		$summarydetail=array('tax'=>$taxarray,'discount'=>$discountarray,'addcharge'=>$addamountarray,'mode'=>$modedetail);
		echo json_encode($summarydetail);
	}
	//terms and condition data fetch
	public function termsandcontdatafetchmodel() {
		$id = $_GET['id'];
		$this->db->select('termsandconditionstermsandconditions_editorfilename');
		$this->db->from('termsandcondition');
		$this->db->where('termsandcondition.termsandconditionid',$id);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$data = $row->termsandconditionstermsandconditions_editorfilename;
			}
			echo json_encode($data);
		}
	}
	//drop down value set with multiple condition
	public function fetchmaildddatawithmultiplecondmodel() {
		$i=0;
		$dname=$_GET['dataname'];
		$did=$_GET['dataid'];
		$dataattrname1=$_GET['othername1'];
		$dataattrname2=$_GET['othername2'];
		$table=$_GET['datatab'];
		$whfield=$_GET['whdatafield'];
		$whdata=$_GET['whval'];
		$mulcond=explode(",",$whfield);
		$mulcondval=explode(",",$whdata);
		$i=0;
		$this->db->select("$dname,$did,$dataattrname1,$dataattrname2");
		$this->db->from($table);
		foreach($mulcond AS $condname) {
			if($mulcondval[$i] != "0" && $mulcondval[$i] != "") {
				$this->db->where($condname,$mulcondval[$i]);
			}
			$i++;
		}
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {		
			foreach($result->result()as $row) {
				$data[$i]=array('datasid'=>$row->$did,'dataname'=>$row->$dname,'otherdataattrname1'=>$row->$dataattrname1,'otherdataattrname2'=>$row->$dataattrname2);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}	
	//get the detail of tax,additinalcharge,discount detail for individual records
	public function quote_individualdetail($param,$moduleid) {
		$discount_json='';
		$tax_json='';
		$charge_json='';
		if($param['discounttype'] > 1){			
			$discount_json = array('typeid'=>$param['discounttype'],'value'=>$param['discountpercent']);			
		}		
		if($param['taxmode'] > 1){			
			$tax_json=$this->gettaxdetail($moduleid,$param['taxmode'],$param['id']);
		}
		if($param['chargemode'] > 1){			
			$charge_json=$this->getchargedetail($moduleid,$param['chargemode'],$param['id']);			
		}			
	   $retarray = array('tax'=>json_encode($tax_json),'discount'=>json_encode($discount_json),'addcharge'=>json_encode($charge_json));
	   return $retarray;
	}
	public function quote_groupdetail($moduleid,$param) {
		$discount_json='';
		$tax_json='';
		$charge_json='';
		$adjustment_json='';
		if($param['discounttype'] > 1){			
			$discount_json = array('typeid'=>$param['discounttype'],'value'=>$param['discountpercent']);			
		}		
		if($param['taxmode'] > 1){			
			$tax_json=$this->gettaxdetail($moduleid,$param['taxmode'],$param['id']);
		}
		if($param['chargemode'] > 1){			
			$charge_json=$this->getchargedetail($moduleid,$param['chargemode'],$param['id']);			
		}
		if($param['adjustmenttype'] > 1){			
			$adjustment_json = array('typeid'=>$param['adjustmenttype'],'value'=>$param['adjustmentvalue']);		
		}		
	    $retarray = array('grouptax'=>json_encode($tax_json),'groupdiscount'=>json_encode($discount_json),'groupaddcharge'=>json_encode($charge_json),'groupadjustment'=>json_encode($adjustment_json));
	    return $retarray;
	}
	/**
	* Retrieve Tax details for the module records.
	* @param $moduleid - specified moduleid ex(Quote/so/po)
	* @param $mode     - mode of records(group/individual)
	* @param $id       - transaction id, ex(quoteid,soid,..)
	*/
	public function gettaxdetail($moduleid,$mode,$id) {
		$taxdata=$this->db->select('moduletaxdetailid,moduletaxdetail.singlegrouptypeid,moduletaxdetail.taxmasterid,moduletaxdetail.taxid,taxvalue,taxamount,taxruleid,taxname')
							->from('moduletaxdetail')
							->join('tax','tax.taxid=moduletaxdetail.taxid')
							->where('moduleid',$moduleid)
							->where('singlegrouptypeid',$mode)
							->where('moduletaxdetail.status',$this->Basefunctions->activestatus)
							->where_in('id',$id)
							->get();
		if($taxdata->num_rows() > 0){			
			foreach($taxdata->result() as $info)
			{
				$taxdataarray[]=array('moduletaxdetailid'=>$info->moduletaxdetailid,'taxid'=>$info->taxid,'rate'=>$info->taxvalue,'amount'=>$info->taxamount,'taxname'=>$info->taxname);
				$taxmasterid = $info->taxmasterid;
			}
			$taxdetails['id'] = $taxmasterid;
			$taxdetails['data']= $taxdataarray;			
		} 
		else {
			$taxdetails = '';
		}
		return $taxdetails;
	}
	/**
	* Retrieve Charge details for the module records.
	* @param $moduleid - specified moduleid ex(Quote/so/po)
	* @param $mode     - mode of records(group/individual)
	* @param $id       - transaction id, ex(quoteid,soid,..)
	*/
	public function getchargedetail($moduleid,$mode,$id) {
		$adddata=$this->db->select('modulechargedetailid,modulechargedetail.moduleid,modulechargedetail.singlegrouptypeid,modulechargedetail.additionalchargecategoryid,modulechargedetail.additionalchargetypeid,modulechargedetail.calculationtypeid,modulechargedetail.amount,modulechargedetail.value,id,additionalchargetypename,calculationtypename')
				->from('modulechargedetail')
				->join('additionalchargetype','additionalchargetype.additionalchargetypeid=modulechargedetail.additionalchargetypeid')
				->join('calculationtype','calculationtype.calculationtypeid=modulechargedetail.calculationtypeid')
				->where('modulechargedetail.moduleid',$moduleid)
				->where('modulechargedetail.singlegrouptypeid',$mode)
				->where('modulechargedetail.status',$this->Basefunctions->activestatus)
				->where_in('id',$id)
				->get();
		if($adddata->num_rows() > 0){
			foreach($adddata->result() as $info)
			{
				$chargedataarray[]=array('modulechargedetailid'=>$info->modulechargedetailid,'additionalchargetypeid'=>$info->additionalchargetypeid,'calculationtypeid'=>$info->calculationtypeid,'amount'=>$info->amount,'value'=>$info->value,'additionalchargetypename'=>$info->additionalchargetypename,'calculationtypename'=>$info->calculationtypename);
				$chargecategoryid=$info->additionalchargecategoryid;
				
			}
			$chargedetails['id']=$chargecategoryid;
			$chargedetails['data']=$chargedataarray;
		}
		else {
			$chargedetails = '';
		}
		return $chargedetails;
	}
	/*	*Cancel the given quote records and reset to draft stage.	*/
	public function canceldata() {	
		$primaryid=$_GET['primarydataid'];	
		$dbdata=$this->quotecurrentstage($primaryid); //retrieves the existing stage		
		$prev_stage=$dbdata['quotestage'];
		$current_stage = $this->cancelstage;
		if($prev_stage == $this->cancelstage)//if both on same stage then return to draft
		{
			$current_stage=$this->draftstage;
		}		
		$updatearray=array('lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid,'crmstatusid'=>	$current_stage);
		$this->db->where('quoteid',$primaryid);
		$this->db->update('quote',$updatearray);
		//Notification On Cancel-Return to Draft
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		$quotenum = $this->Basefunctions->generalinformaion('quote','quotenumber','quoteid',$primaryid);
		if($prev_stage != $this->cancelstage)  { 
			$notimsg = $empname." Changed The Quotation Named To Cancel -".$quotenum."";$val = "Cancelled";
		} else {
			$notimsg = $empname." Changed The Quotation Named To Draft -".$quotenum."";$val = "Active"; 
		}
		$this->Basefunctions->notificationcontentadd($primaryid,$val,$notimsg,1,216); 
		echo TRUE;
	}
	//lost the quote
	public function lostquote() {
		$primaryid=$_GET['primarydataid'];	
		$dbdata=$this->quotecurrentstage($primaryid); //retrieves the existing stage		
		$prev_stage=$dbdata['quotestage'];
		$current_stage = $this->loststage;
		if($prev_stage == $this->loststage)//if both on same stage then return to draft
		{
			$current_stage=$this->draftstage;
		}		
		$updatearray=array('lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid,'crmstatusid'=>	$current_stage,'lostreason'=>trim($_GET['reason']));
		$this->db->where('quoteid',$primaryid);
		$this->db->update('quote',$updatearray);
		//Notification on Lost of Quote
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		$quotenum = $this->Basefunctions->generalinformaion('quote','quotenumber','quoteid',$primaryid);
		if($prev_stage != $this->loststage)  {
			$notimsg = $empname." Changed The Quotation Named to Lost -".$quotenum."";$val = "Lost";
		} else { 
			$notimsg = $empname." Changed The Quotation Named to Active -".$quotenum.""; $val = "Active";
		}
		$this->Basefunctions->notificationcontentadd($primaryid,$val,$notimsg,1,216); 
		echo true;		
	}
	//bookquote the quote
	public function bookquote() {	
		$primaryid=$_GET['primarydataid'];	
		$dbdata=$this->quotecurrentstage($primaryid); //retrieves the existing stage		
		$prev_stage=$dbdata['quotestage'];
		$current_stage = $this->bookedstage;
		if($prev_stage == $this->bookedstage)//if both on same stage then return to draft
		{
			$current_stage=$this->draftstage;
		}		
		$updatearray=array('lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid,'crmstatusid'=>	$current_stage);
		$this->db->where('quoteid',$primaryid);
		$this->db->update('quote',$updatearray);
		//Notification On Cancel-Return to Draft
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		$quotenum = $this->Basefunctions->generalinformaion('quote','quotenumber','quoteid',$primaryid);
		if($prev_stage != $this->bookedstage)  { 
			$notimsg = $empname." Changed The Quotation Named To Booked -".$quotenum."";$val = "Booked";
		} else {
			$notimsg = $empname." Changed The Quotation Named To Draft -".$quotenum."";$val = "Active"; 
		}
		$this->Basefunctions->notificationcontentadd($primaryid,$val,$notimsg,1,216); 
		echo TRUE;
	}
	//convert to salesorder
	public function checkconvertso() {
		$primaryid=$_GET['primarydataid'];
		$dbdata=$this->db->select('status,convertso')->from('quote')->where('quoteid',$primaryid)->get()->result();
		foreach($dbdata as $info){
			$quotestatus=$info->status;
			$convertso=$info->convertso;
		}
		if($quotestatus == 1 and $convertso == 0) {
			echo true;
		} else {
			echo false;
		}
	}
	//terms and condition id fetch
	public function tandcidfetchmodel() {
		$primaryid=$_GET['dataprimaryid'];
		$this->db->select('termsandconditionid');
		$this->db->from('quote');
		$this->db->where('quote.quoteid',$primaryid);
		$this->db->where('quote.status',1);
		$this->db->limit(1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				$data = $row->termsandconditionid;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	/* Charge gird header load */
	public function chargegirdheaderinformationfetchmodel($moduleid) {
		$fnames = array('additionalchargetypeid','additionalchargetypename','calculationtypeid','calculationtypename','value','amount');
		$flabels = array('Id','Charge Name','Calc Id','Type','Value','Amount');
		$colmodnames = array('additionalchargetypeid','additionalchargetypename','calculationtypeid','calculationtypename','value','amount');
		$colindexnames = array('additionalchargetypeid','additionalchargetypename','calculationtypeid','calculationtypename','value','amount');
		$uitypes = array('2','2','2','2','2','2');
		$viewtypes = array('0','1','0','1','1','1');
		$dduitypeids = array('17','18','19','20','23','25','26','27','28','29');
		$data = array();
		$i=0;
		$j=0;
		foreach($fnames as $fname) {
			if( in_array($uitypes[$j],$dduitypeids) ) {
				$data['fieldname'][$i]=$fname;
   				$data['fieldlabel'][$i]=$flabels[$j];
    			$data['colmodelname'][$i]=$colmodnames[$j].'name';
    			$data['colmodelindex'][$i]=$colindexnames[$j];
    			$data['colmodelviewtype'][$i]=$viewtypes[$j];
    			$data['colmodeltype'][$i]='text';
    			$data['colmodeluitype'][$i]=$uitypes[$j];
    			$data['colmoduleid'][$i]=$moduleid;
    			$i++;
    			$data['fieldname'][$i]=$fname;
   				$data['fieldlabel'][$i]=$flabels[$j];
    			$data['colmodelname'][$i]=$colmodnames[$j];
    			$data['colmodelindex'][$i]=$colindexnames[$j];
    			$data['colmodelviewtype'][$i]='0';
    			$data['colmodeltype'][$i]='text';
    			$data['colmodeluitype'][$i]='2';
    			$data['colmoduleid'][$i]=$moduleid;
    			$i++;
    		} else {
    			$data['fieldname'][$i]=$fname;
   				$data['fieldlabel'][$i]=$flabels[$j];
    			$data['colmodelname'][$i]=$colmodnames[$j];
    			$data['colmodelindex'][$i]=$colindexnames[$j];
    			$data['colmodelviewtype'][$i]=$viewtypes[$j];
    			$data['colmodeltype'][$i]='text';
    			$data['colmodeluitype'][$i]=$uitypes[$j];
    			$data['colmoduleid'][$i]=$moduleid;
    			$i++;
    		}
    		$j++;
		}
		return $data;
	}
	//chargecategoryload
	public function chargecategoryload() {
		$chargesdetail='';
		$chargeids=array(1);
		$j=0;
		if(trim($_GET['typeid'] == 2)){ //individual
			$grossamount=trim($_GET['grossamount']);
			if($grossamount == '' AND !is_numeric($grossamount)) {
				$grossamount = 0;
			}
		} else { //group-
			$grossamount=trim($_GET['totalnetamount']);
			if($grossamount == '' AND !is_numeric($grossamount)) {
				$grossamount = 0;
			}
		}		
		//conversation rate check.
		$conversionrate = trim($_GET['conversionrate']);
		if(!is_numeric($conversionrate)) {
			$conversionrate=0.00;
		}
		if (in_array($_GET['transmethod'], [216,85,94])){
			$tansmethodid = array(8,9);
		}
		else if (in_array($_GET['transmethod'], [226,86,95])){
			$tansmethodid = array(8,9);
		}
		else if (in_array($_GET['transmethod'], [217,87,96])){
			$tansmethodid = array(8,9);
		}
		else if (in_array($_GET['transmethod'], [225,88,99])){
			$tansmethodid = array(7,9);
		}
		//two blocks ->one for with territory another with out territory
		//checks whether territory is given
		if(!is_numeric($_GET['territory'])) {
			$data=$this->db->select('additionalchargetypename,value,additionalchargecategoryname,additionalchargetypeid,calculationtypename,additionalchargetype.calculationtypeid')
						->from('additionalchargetype')						
						->join('additionalchargecategory','additionalchargecategory.additionalchargecategoryid=additionalchargetype.additionalchargecategoryid')
						->join('calculationtype','calculationtype.calculationtypeid=additionalchargetype.calculationtypeid')
						->where('additionalchargetype.additionalchargecategoryid',trim($_GET['chargecategoryid']))
						->where_in('additionalchargetype.transactionmethodid',$tansmethodid)
						->where('additionalchargetype.status',$this->Basefunctions->activestatus)
						->get();			
			foreach($data->result() as $value) {
				//calculate				
				$rate=(($value->value)/(1/$conversionrate));
				$rate=round($rate,2);
				$amount=$rate;
				$type=$value->calculationtypeid;
				if($type == 3){ //on percentage mode
					$amount =(($rate/100) * $grossamount);				
				}				
				$chargesdetail->rows[$j]['id']=$value->additionalchargetypeid;
				$chargesdetail->rows[$j]['cell']=array(													
														$value->additionalchargetypeid,
														$value->additionalchargetypename,
														$type,
														$value->calculationtypename,
														$rate,
														$amount
													);
				$j++;
			}
		} else if ($_GET['territory'] > 0) {
			//
			$data=$this->db->select('additionalchargetypename,chargeterritories.value,additionalchargecategoryname,additionalchargetype.additionalchargetypeid,calculationtypename,additionalchargetype.calculationtypeid')
						->from('additionalchargetype')				
						->join('additionalchargecategory','additionalchargecategory.additionalchargecategoryid=additionalchargetype.additionalchargecategoryid')
						->join('calculationtype','calculationtype.calculationtypeid=additionalchargetype.calculationtypeid')
						->join('chargeterritories','chargeterritories.additionalchargetypeid=additionalchargetype.additionalchargetypeid')
						->where('additionalchargetype.additionalchargecategoryid',trim($_GET['chargecategoryid']))
						->where_in('additionalchargetype.transactionmethodid',$tansmethodid)
						->where('additionalchargetype.status',$this->Basefunctions->activestatus)
						->where('chargeterritories.territoryid',trim($_GET['territory']))
						->get();
			foreach($data->result() as $value) {
				//calculate							
				$rate=(($value->value)/(1/$conversionrate));
				$rate=round($rate,2);
				$amount=$rate;
				$prate=$value->value;
				$type=$value->calculationtypeid;
				if($type == 3) {//on percentage mode
					$amount =(($prate/100) * $grossamount);				
				}
				$chargeids[]=$value->additionalchargetypeid;			
				$chargesdetail->rows[$j]['id']=$value->additionalchargetypeid;
				$chargesdetail->rows[$j]['cell']=array(
														$value->additionalchargetypeid,
														$value->additionalchargetypename,
														$type,$value->calculationtypename,
														$rate,
														$amount
												);						
				$j++;		
			}
			$data=$this->db->select('additionalchargetypename,value,additionalchargecategoryname,additionalchargetypeid,calculationtypename,additionalchargetype.calculationtypeid')
						->from('additionalchargetype')						
						->join('additionalchargecategory','additionalchargecategory.additionalchargecategoryid=additionalchargetype.additionalchargecategoryid')
						->join('calculationtype','calculationtype.calculationtypeid=additionalchargetype.calculationtypeid')
						->where_not_in('additionalchargetype.additionalchargetypeid',$chargeids)
						->where('additionalchargetype.additionalchargecategoryid',trim($_GET['chargecategoryid']))
						->where('additionalchargetype.status',$this->Basefunctions->activestatus)
						->get();
			
			foreach($data->result() as $value) {
				//calculate
				$amount=$value->value;
				$rate=$value->value;
				$type=$value->calculationtypeid;
				if($type == 3) { //on percentage mode
					$amount =(($rate/100) * $grossamount);		
				}
				$chargesdetail->rows[$j]['id']=$value->additionalchargetypeid;
				$chargesdetail->rows[$j]['cell']= array(
														$value->additionalchargetypeid,
														$value->additionalchargetypename,
														$type,$value->calculationtypename,
														$rate,
														$amount
												);			
				$j++;
			}
		}
		echo  json_encode($chargesdetail);
	}
	/* Charge gird header load */
	public function taxgirdheaderinformationfetchmodel($moduleid) {
		$fnames = array('taxid','taxname','rate','amount');
		$flabels = array('Id','Tax Name','Rate','Amount');
		$colmodnames = array('taxname','taxrate','taxid','amount');
		$colindexnames = array('tax','tax','tax','tax');
		$uitypes = array('2','2','2','2');
		$viewtypes = array('0','1','1','1');
		$dduitypeids = array('17','18','20','19','23','25','26','27','28','29');
		$data = array();
		$i=0;
		$j=0;
		foreach($fnames as $fname) {
			if( in_array($uitypes[$j],$dduitypeids) ) {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j].'name';
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]='0';
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]='2';
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			} else {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			}
			$j++;
		}
		return $data;
	}
	//load only direct tax category
	public function taxcategory(){
		$this->db->select("taxmastername,taxmasterid");
		$this->db->from('taxmaster');		
		$this->db->where('taxapplytypeid',2);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$this->db->order_by('taxmastername',"asc");
		$result = $this->db->get();
		return $result->result();
	}
	//taxmasterload
	public function taxmasterload(){
		$taxdetail='';
		if(trim($_GET['type']) == 2){//individual
			$finalamount = trim($_GET['finalamount']);
			if($finalamount == '' AND !is_numeric($finalamount)){
				$finalamount = 0;
			}
		} else {		//group
			$finalamount = trim($_GET['totalnetamount']);
			if($finalamount == '' AND !is_numeric($finalamount)){
				$finalamount = 0;
			}
		}
		if (in_array($_GET['transmethod'], [216,85,94])){
			$tansmethodid = array(8,9);
		}
		else if (in_array($_GET['transmethod'], [226,86,95])){
			$tansmethodid = array(8,9);
		}
		else if (in_array($_GET['transmethod'], [217,87,96])){
			$tansmethodid = array(8,9);
		}
		else if (in_array($_GET['transmethod'], [225,88,99])){
			$tansmethodid = array(7,9);
		}
		$j=0;		
		//two blocks ->one for with territory another with out territory
		//checks whether territory is given
		if(!is_numeric($_GET['territory'])){
			$data=$this->db->select('tax.taxname,tax.taxrate,tax.taxid')
						->from('tax')
						->where('tax.taxmasterid',trim($_GET['taxmasterid']))
						->where_in('tax.transactionmethodid',$tansmethodid)
						->where('tax.status',$this->Basefunctions->activestatus)
						->get();			
			foreach($data->result() as $value){
			$amount=round((($value->taxrate/100)*$finalamount),2);
			$taxdetail->rows[$j]['id']=$value->taxid;
			$taxdetail->rows[$j]['cell']=array(														
												$value->taxid,
												$value->taxname,
												$value->taxrate,										
												$amount																
												);						
			$j++;		
			}
		} else if ($_GET['territory'] > 0){
			$taxids=array(1);
			$data=$this->db->select('tax.taxname,taxterritories.taxrate,tax.taxid')
								->from('tax')
								->join('taxterritories','taxterritories.taxid=tax.taxid')
								->where('tax.taxmasterid',trim($_GET['taxmasterid']))
								->where_in('tax.transactionmethodid',$tansmethodid)
								->where('taxterritories.territoryid',trim($_GET['territory']))
								->where('tax.status',$this->Basefunctions->activestatus)						
								->where('taxterritories.status',$this->Basefunctions->activestatus)						
								->get();						
			foreach($data->result() as $value){
				$taxids[]=$value->taxid;	
				$amount=round((($value->taxrate/100)*$finalamount),2);
				$taxdetail->rows[$j]['id']=$value->taxid;
				$taxdetail->rows[$j]['cell']=array(		
													$value->taxid,
													$value->taxname,
													$value->taxrate,
													$amount	
										);
				$j++;
			}
			$datam=$this->db->select('tax.taxname,tax.taxrate,tax.taxid')
								->from('tax')
								->where_not_in('tax.taxid',$taxids)
								->where_in('tax.transactionmethodid',$tansmethodid)
								->where('tax.taxmasterid',trim($_GET['taxmasterid']))
								->where('tax.status',$this->Basefunctions->activestatus)						
								->get();			
			foreach($datam->result() as $value){
				$amount=round((($value->taxrate/100)*$finalamount),2);
				$taxdetail->rows[$j]['id']=$value->taxid;
				$taxdetail->rows[$j]['cell']=array(	
													$value->taxid,
													$value->taxname,
													$value->taxrate,
													$amount	
										);
				$j++;		
			}			
		}	
		echo  json_encode($taxdetail);
	}
	/**	* Retrieves the conversion rate of the currency	*/
	public function getconversionrate(){
		$currencyid=trim($_GET['currencyid']);
		$date=date($this->Basefunctions->datef);
		//first get the basecurrency
		$currency_data=$this->Basefunctions->getdefaultcurrency();
		$base_currency = json_decode($currency_data,true);
		
		$base_currency = $base_currency['currency'];
		if($base_currency == $currencyid){
			$data=array('status'=>TRUE,'rate'=>1.00);
		}
		else{
			$conversion_rate = $this->db->select('multiplyrate')
										->from('currencyconversion')
										->where('currencyid',$base_currency) //basecurrency
										->where('currencytoid',$currencyid) //given currency
										->where('status',$this->Basefunctions->activestatus)
										->limit(1)
										->get();
			if($conversion_rate->num_rows() > 0){
				foreach($conversion_rate->result() as $conv){
				$data=array('status'=>TRUE,'rate'=>$conv->multiplyrate);	
				}
			}
			else {
				$data=array('status'=>FALSE,'rate'=>1.00);
			}
		}
		echo json_encode($data);
	}
	public function accountdependddata(){
		$array=array();
		$id=trim($_GET['id']);
		$table = trim($_GET['table']);
		$mtableid=$table.'id';
		$mtable=$table.'name';
		if($id){
			$data=$this->db->select("$mtable,$mtableid")
			->from($table)
			->where('accountid',$id)
			->where('status',1)
			->get();
		}else{
			$data=$this->db->select("$mtable,$mtableid")
			->from($table)
			->where('status',1)
			->get();
		}
		if($data->num_rows() > 0){
			foreach($data->result() as $row){
				$array[]=array('id'=>$row->$mtableid,'name'=>$row->$mtable);
			}
		}
		echo json_encode($array);
	}	
	/*	* Get the pricebook currency	*/
	public function pricebookcurrency(){
		$currency = trim($_GET['currency']);
		$pricebook = trim($_GET['pricebook']);
		$currencydata = $this->db->select('pricebook.currencyid')
							->from('pricebook')
							->where('pricebookid',$pricebook) //given currency
							->where('status',$this->Basefunctions->activestatus)
							->limit(1)
							->get();
		foreach($currencydata->result() as $info){
			$currencyid = $info->currencyid;
		}
		$conversationrate = 1;
		$conversion = $this->db->select('multiplyrate')
										->from('currencyconversion')
										->where('currencyid',$currencyid) //basecurrency
										->where('currencytoid',$currency) //given currency */
										->where('status',$this->Basefunctions->activestatus)
										->limit(1)
										->get();
		foreach($conversion->result() as $infoo){
			$conversationrate = $infoo->multiplyrate;
		}
		echo json_encode(array('currencyid'=>$currencyid,'rate'=>$conversationrate));
	}
	/*	* Quote stage-data retrival for Lost-Cancel-Draft-Convert	*/
	public function quotecurrentstage($quoteid){
		$data=$this->db->select('crmstatusid')
					->from('quote')
					->where('quoteid',$quoteid)
					->where('status',$this->Basefunctions->activestatus)
					->limit(1)
					->get();
		foreach($data->result() as $info){
			$quotestage=$info->crmstatusid;
		}
		$array=array('quotestage'=>$quotestage);
		return $array;
	}
	
	public function quotecurrentstatus($quoteid){
		$data=$this->db->select('status')
		->from('quote')
		->where('quoteid',$quoteid)
		->limit(1)
		->get();
		foreach($data->result() as $info){
			$quotestatus=$info->status;
		}
		$array=array('quotestatus'=>$quotestatus);
		return $array;
	}
	//retrieve lead/contact's
	public function getdropdownoptions($table) {
		$i=0;
		$id=$table.'id';
		$name=$table.'name';
		if($table == 'lead'){
			$account = $this->db->select('accountname')
			->from('account')
			->where('accountid',$_GET['accountid'])
			->limit(1)
			->get();
			foreach($account->result() as $infos){
				$accountname =$infos->accountname;
			}
		}
		$this->db->select(''.$id.' AS Id'.','.''.$name.' AS Name,salutation.salutationname AS SName,lastname AS LName');	
		$this->db->from($table);
		$this->db->join('salutation','salutation.salutationid='.$table.'.salutationid','left outer');
		$this->db->where($table.'.industryid',$this->Basefunctions->industryid);
		if(isset($_GET['accountid']) AND $table == 'contact'){
			if($_GET['accountid'] > 0){
			$this->db->where('accountid',$_GET['accountid']);
			}
		}
		if(isset($_GET['accountid']) AND $table == 'lead'){
			if($_GET['accountid'] > 0){
			$this->db->where('accountname like','%'.$accountname.'%');
			}
		}
		$this->db->where_in(''.$table.'.status',array(1,8));
		$data=$this->db->get();
		if($data->num_rows() > 0) {
			foreach($data->result() as $info) {
				$sname = (($info->SName!='')? $info->SName.' ':'');
				$lname = (($info->LName!='')? ' '.$info->LName:'');
				$datam[$i] = array('id'=>$info->Id,'name'=>$sname.$info->Name.$lname);
				$i++;
			}
			echo json_encode($datam);
		}
		else{
			echo json_encode(array('fail'=>'FAILED'));
		}
	}
	//product instock fetch
	public function productstoragefetchfunmodel($productid){
		$this->load->model('Product/Productmodel');
		$totalstock = $this->Productmodel->productstoragefetchfunmodel($productid);
		return $totalstock;
	}
}