<html>
<head>  
	<title>Sales Neuron</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<?php echo base_url();?>img/favi.ico"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/erpcustom.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/foundation/foundation.css"/>
	<link rel='stylesheet' type='text/css' href='<?php echo base_url();?>css/froala/font-awesome.css' >
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/allinonecss.min.css"  media="screen" />
	<script src="<?php echo base_url();?>js/plugins/allpluginheader.min.js"></script>
	<script src="<?php echo base_url();?>js/plugins/allpluginbottom.min.js"></script>	
	<script type="text/javascript">
		<?php
		$CI = &get_instance();
		$device = $CI->Basefunctions->deviceinfo();
		if($device=='phone') { 
			$devicename = 'phone';
		} else {
			$devicename = 'desktop';
		}	?>
		var deviceinfo = '<?php echo $devicename; ?>';
	</script>	
	<body>

<main class="cd-main-content">	
<div class="container signuppage features"  id="signupdataformdiv" style="padding-top:40px;">
	<div class="row">
		<div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
			<div class="panel panel-info1" style="margin-bottom:0px; background-color:#f6f6f6;">
				<div class="panel-heading">
					<h3 class="panel-title">SIGN UP FOR FREE TRIAL</h3>
				</div>
				<div class="ssologinicon col-md-12 col-sm-12"> 
					<ul class="subheader" style="margin-bottom:0px;margin-left:0;"><li class="subhead">QUICK SIGN UP WITH</li></ul> 
						<div class="col-lg-6 col-md-12 col-sm-6 col-xs-12" style="line-height:40px;">
							<span class="">
								<button id="" class="btn-social btn-info btn-facebook facebooklogicon" onclick="flogin();" name="loginsubmit"><span class="socicofb">Facebook</span></button>
							</span>
						</div>
						<div class="col-lg-6 col-md-12 col-sm-6 col-xs-12" style="line-height:40px;">
							<span class="">
								<button id="" class="btn-social btn-info btn-google googlelogicon" name="loginsubmit"><span class="socicogoogle">Google</span></button>
							</span>
						</div>
						<span class="g-signin2" data-onsuccess="onSignIn" data-onFailure="onSignInfailure" data-theme="dark" style="display:none;"></span>
			            <fb:login-button scope="public_profile,email,user_location,user_photos" onlogin="checkLoginState();" style="display:none;"></fb:login-button>
					
					<ul class="subheader" style="margin-bottom:0px;margin-left:0;"><li class="subhead">---(OR)---</li></ul> 
				</div>
				 <script type="in/Login"></script>
				<div class="panel-body">
					<form id="newsignupform" name="newsignupform" action="<?php echo base_url()."crm/Login/newsignupcreate"; ?>" class="loginformdiv" method="POST" enctype="application/x-www-form-urlencoded">
						<div class="row">&nbsp;</div>
						<input id="signupname" class="form-control input-box" type="text" placeholder="Name" name="signupname" data-rule-required="true">
						<div class="row">&nbsp;</div>
						<input id="signupcompany" class="form-control input-box" type="text" placeholder="Company Name" name="signupcompany" data-rule-required="true">
						<div class="row">&nbsp;</div>
						<input id="signupemail" class="form-control input-box" type="text" placeholder="Email Address" name="signupemail" data-rule-email="true" data-rule-required="true">
						<div class="row">&nbsp;</div>
						<input id="signuppassword" class="form-control input-box" type="password" placeholder="Password" name="signuppassword" data-rule-required="true" data-rule-maxlength="20" data-rule-minlength="6">
						<div class="row">&nbsp;</div>
						<div class="col-lg-12" style="line-height:40px;">
							<div class="col-lg-12">
								<span class="terms"><h6> By clicking "Sign Up", you agree to the
									<a target="_blank" href="<?php echo base_url().'termsofservice.php'; ?>">Terms of Service </a> &
									<a target="_blank" href="<?php echo base_url().'privacypolicy.php'; ?>">Privacy Policy</a>.
								</h6></span>
							</div>
						</div>
						<div class="col-lg-12">
							<button name="signupsubmit" id="signupsubmit" class="loginsubmitbutton btn-info">Sign Up</button>
						</div>
						<div class="row">&nbsp;</div>
						<input type="hidden" name="hiddemodata" id="hiddemodata" value="No" />
						<input type="hidden" name="hidplandata" id="hidplandata" value="<?php echo ((isset($_POST['signupplanid']))?$_POST['signupplanid']:'4'); ?>" />
						<input type="hidden" name="hidzonedata" id="hidzonedata" value="" />
						<input type="hidden" name="signuptype" id="signuptype" value="general" />
						<input type="hidden" name="signupuserimg" id="signupuserimg" value="" />
					</form>
					<input type="hidden" name="baseurl" id="baseurl" value="<?php echo base_url();?>" />
					<input type="hidden" name="dataclearurl" id="dataclearurl" value="<?php echo base_url().'crm/Login/';?>" />
					<div id="dynamicforms" style="display:none;"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</main>
<div class="container overlay" style="display:none;" id="signupprocessdiv">
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">
		<div class="col-md-4 text-left">
		</div>
	</div>
	<div class="row">
		<div class="col-md-3 col-sm-3">
			<div id="" class=""></div>
		</div>
		<div class="col-md-6 col-sm-6" >
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
		    <div class="panel panel-info1">
				<div class="panel-heading">
					<h3 class="panel-title">Thank you for signing up</h3>
				</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>
				<img alt="" src="images/220.gif">
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>
				<div class="panel-body"> Please wait as we set up your work space</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-3">
			<div id="" class=""></div>
		</div>
	</div>
</div>
<?php include("footer.php");?>	
</body>
<script type="text/javascript">
	$(document).ready(function () {
		$('#signupsubmit').click(function(e){
			cisessionreset();
			$("#newsignupform").validate({
				submitHandler: function(form) {
					var signupemail=$('#signupemail').val();
					var status=0;
					var curl = $('#dataclearurl').val();
					$.ajax({
						url:curl+"uniqueemail?signupemail="+signupemail,
						async:false,
						cache:false,
						success: function(data) {
							var msg = $.trim(data);
							if(msg == 'FAIL') {
								status=1;
							}
						},
					});
					if(status==1) {
						$('#signupemail').tooltip("destroy").data("title", "Email address already exist").addClass("error").tooltip();
						e.preventDefault();
					} else {
						$('#signupprocessdiv').fadeIn();
						$('#signupdataformdiv').fadeOut();
						form.submit();
						console.clear();
					}
				},
				showErrors: function(errorMap, errorList) {
					// Clean up any tooltips for valid elements
					$.each(this.validElements(), function (index, element) {
						var $element = $(element);
						$element.data("title", "")
						.removeClass("error")
						.tooltip("destroy");
					});
					// Create new tooltips for invalid elements
					$.each(errorList, function (index, error) {
						var $element = $(error.element);
						$element.tooltip("destroy")
						.data("title", error.message)
						.addClass("error")
						.tooltip();
					});
				},
			});
		});
		/* google signup icon click */
		$('.googlelogicon').click(function(){
			$('.g-signin2 div span.abcRioButtonContents').trigger('click');
		});
	});
	{
		/* google sso signup */
		function onSignIn(googleUser) {
		    // Useful data for your client-side scripts:
		    var profile = googleUser.getBasicProfile();
		    var signupemail = profile.getEmail();
		    var signupname = profile.getName();
		    var imgurl = profile.getImageUrl()!='undefined' ? profile.getImageUrl() : '';
		    var url = $('#baseurl').val();
		    //clear session
		    cisessionreset();
		    var curl = $('#dataclearurl').val();
			$.ajax({
				url:curl+"uniqueemail?signupemail="+signupemail,
				async:false,
				cache:false,
				success: function(data) {
					var msg = $.trim(data);
					if(msg == 'FAIL') {
						signOut();
						$('<form action="'+url+'crm/Login/verifylogin" name=apploginform" id="apploginform"  method="POST" enctype="application/x-www-form-urlencoded"><input id="username" type="text" name="username" value="'+signupemail+'" /><input type="hidden" name="logintype" id="logintype" value="google" /><input type="hidden" name="ltype" id="ltype" value="site" /></form>').appendTo('#dynamicforms').submit().remove();
						console.clear();
					} else {
						signOut();
						var planid = $('#hidplandata').val();
						var timezone = $('#hidzonedata').val();
						$('<form action="'+url+'crm/Login/newsignupcreate" name=appsignupform" id="appsignupform"  method="POST" enctype="application/x-www-form-urlencoded"><input id="signupname" type="text" name="signupname" value="'+signupname+'"><input id="signupemail" type="text" name="signupemail" value="'+signupemail+'"><input type="hidden" name="hiddemodata" id="hiddemodata" value="No" /><input type="hidden" name="hidplandata" id="hidplandata" value="'+planid+'" /><input type="hidden" name="hidzonedata" id="hidzonedata" value="'+timezone+'" /><input type="hidden" name="signuptype" id="signuptype" value="google" /><input type="hidden" name="signupuserimg" id="signupuserimg" value="'+imgurl+'" /></form>').appendTo('#dynamicforms').submit().remove();
						$('#signupdataformdiv').fadeOut();
						$('#signupprocessdiv').fadeIn();
						console.clear();
					}
				},
			});
		}
		function onSignInfailure(error) {
			console.log(error);
		}
		function signOut() {
		    var auth2 = gapi.auth2.getAuthInstance();
		    auth2.signOut().then(function () {
		       auth2.disconnect();
		    });
		  }
	}
	{/* face book login */
		//After login
		function statusChangeCallback(response) {
			if (response.status === 'connected') {
				FB.api('/me?fields=name,email,picture',function(response) {
					var emailid = response.email;
					var name = response.name;
					var imgurl = response.picture.data.url;
					var url = $('#baseurl').val();
				    //clear session
				    cisessionreset();
				    var curl = $('#dataclearurl').val();
					$.ajax({
						url:curl+"uniqueemail?signupemail="+emailid,
						async:false,
						cache:false,
						success: function(data) {
							var msg = $.trim(data);
							if(msg == 'FAIL') {
								if(emailid!='undefined') {
									$('<form action="'+url+'crm/Login/verifylogin" name="apploginform" id="apploginform"  method="POST" enctype="application/x-www-form-urlencoded"><input id="username" type="text" name="username" value="'+emailid+'" /><input type="hidden" name="logintype" id="logintype" value="facebook" /><input type="hidden" name="ltype" id="ltype" value="site" /></form>').appendTo('#dynamicforms').submit().remove();
								} else {
									alert('You entered facebook account emailid is invalid.');
								}
								console.clear();
							} else {
								if(emailid!='undefined') {
									var timezone = $('#hidzonedata').val();
									$('<form action="'+url+'crm/Login/newsignupcreate" name="appsignupform" id="appsignupform"  method="POST" enctype="application/x-www-form-urlencoded"><input id="signupname" type="text" name="signupname" value="'+name+'"><input id="signupemail" type="text" name="signupemail" value="'+emailid+'"><input type="hidden" name="hiddemodata" id="hiddemodata" value="No" /><input type="hidden" name="hidplandata" id="hidplandata" value="4" /><input type="hidden" name="hidzonedata" id="hidzonedata" value="'+timezone+'" /><input type="hidden" name="signuptype" id="signuptype" value="facebook" /><input type="hidden" name="signupuserimg" id="signupuserimg" value="'+imgurl+'" /></form>').appendTo('#dynamicforms').submit().remove();
									$('#signupprocessdiv').fadeIn();
								} else {
									alert('You entered facebook account emailid is invalid.');
								}
								console.clear();
							}
						},
					});
				});
				FB.logout(function(response) {//user is now logged out
				});
			} else if (response.status === 'not_authorized') {
				alert('Please provide valid credentials.');
				console.clear();
			}
		}
		//This function is called when someone finishes with the Login Button
		function checkLoginState() {
			FB.getLoginStatus(function(response) {
				statusChangeCallback(response);
			});
		}
		//login from custom button
		function flogin() {
			FB.login(function(response) {
				statusChangeCallback(response);
			},{'scope':'email'});
		}
		//facebook account settings
		window.fbAsyncInit = function() {
			FB.init({
				appId      : '851486518310995',//'710509099079311',
				cookie     : true,
				xfbml      : false,
				version    : 'v2.2'
			});
		};
	}
</script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
</html>