<html>
<head>  
	<title>Sales Neuron</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<?php echo base_url();?>img/favi.ico"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/allinonecss.css"  media="screen" />
	<script src="<?php echo base_url();?>js/plugins/allpluginheader.min.js"></script>
	<script src="<?php echo base_url();?>js/plugins/allpluginbottom.min.js"></script>
	<script type="text/javascript">
		<?php
		$CI = &get_instance();
		$device = $CI->Basefunctions->deviceinfo();
		if($device=='phone') { 
			$devicename = 'phone';
		} else {
			$devicename = 'desktop';
		}	?>
		var deviceinfo = '<?php echo $devicename; ?>';
   </script>	
   <script>
		var base_url = '<?php echo base_url();?>';
		$(document).ready(function() {
			{// Foundation Initialization
				$(document).foundation();
			}
			$('body').fadeIn(1000);
			$('#username').focus();
			/* On enter login trigger*/
			$('#loginpassword,#password').keypress(function(e){	
				if(e.which == 13) {
					$("#loginsubmit").trigger('click');
				}
			});
			$('#forgetusername').keypress(function(e){	
				if(e.which == 13) {
					$("#forgotsubmit").trigger('click');
				}
			});
			$('#securityanswer').keypress(function(e){	
				if(e.which == 13) {
					$("#secanswersubmit").trigger('click');
				}
			});
			$('#retypepassword').keypress(function(e){
				if(e.which == 13) {
					$("#changepasswordsubmit").trigger('click');
				}
			});
			/*Alert Close*/	
			$("#alertscloseup").click(function(){
				$("#alerts").fadeOut();
			});
			{/* login button  */
				$('#loginsubmit').prop("disabled",false);
				$("#loginsubmit").click(function(){
					$("#loginindexvalidationspan").validationEngine('validate');
				});
				$("#loginindexvalidationspan").validationEngine({
					onSuccess: function() {
						$("#processoverlay").show();
						$('#loginsubmit').prop("disabled", true);
						var login= $("#loginform").serialize();
						var amp = '&';
						var loginuser = amp + login;
						var domain=base_url;
						$.ajax({
							type:"POST",
							dataType:'json',
							url:base_url+"Login/verifylogin",
							data:"logindata="+ loginuser,
							async:false,
							cache:false,
							success: function(data) {
								if(data.status == 'success') {
									$("#normalbg").animate({left:'-18%'},250);
									$("#normalbg").fadeOut(20);
									var dname = data.domainname
									var domain=dname;
									$("#processoverlay").hide();
									window.location.replace(domain+'Home');
									window.location.reload();
								} else if(data.status == 'Expired') {
									$("#processoverlay").hide();
									if(data.industryid == 3){
										$("#normalbg").animate({left:'-18%'},250);
										$("#normalbg").fadeOut(20);
										$('#loginsubmit').prop("disabled", false);
										$('#reactivationoverlay').show();
									} else {
										var dname = data.domainname
										domain=dname;
										window.location.replace(domain+'Home/accountexpire');
									}
								} else {
									$("#processoverlay").hide();
									$('#username').validationEngine('showPrompt',data.status, 'error', 'bottomLeft',true);
									$('#loginsubmit').prop("disabled", false);
								}
							},
						});
					},
					onFailure: function() {
						alertpopup('Enter Correct Account ID/Password');
						$("#processoverlay").hide();
					}
				});
			}
			{/* forgotpassword submit button  */
				$("#forgotsubmit").click(function(){
					$("#forgotscreen").validationEngine('validate');
					
				});
				$("#forgotscreen").validationEngine({
					onSuccess: function() {
						var forgotpass= $("#forgotpasswordform").serialize();
						var amp = '&';
						var loginpass = amp + forgotpass;
						$.ajax({
							type: "POST",
							url:base_url+"Login/verifyusername",
							data:"logindata="+ loginpass, 
							dataType:'json',
							success: function(data) {
								if((data.status) == 'success') {
									var emailid = $('#forgetusername').val();
									sendpasswordlink(emailid);
									$("#headingchange").text('');
									$(".loginformdiv").hide();
									$(".forgetpasswordformdiv").hide();
									$(".mailprocessdiv").fadeIn();
								} else {
									$('#forgetusername').validationEngine('showPrompt',data.status, 'error', 'bottomLeft',true);
								}
							}, 
						});
					},
					onFailure: function() {
					}			
				});
			}
			{/* forgotpassword loginscreen button  */
				$("#forgotlink").click(function(){
					$('#forgetusername').val('');
					$("#headingchange").text('Forgot Password');
					$(".loginformdiv").hide();
					$(".forgetpasswordformdiv").fadeIn();
				});
				$("#forgotcancel").click(function(){
					$('#forgetusername').val('');
					$("#headingchange").text('Login');
					$(".loginformdiv").fadeIn();
					$(".forgetpasswordformdiv").hide();
				});
			}
			{//  On failed Mail processing submit button backtoforgetpassword -GRA
				$("#backtoforgetpassword").click(function(){
					$('#forgetusername').val('');
					$("#headingchange").text('Forgot Password');
					$(".mailprocessdiv").hide();
					$(".forgetpasswordformdiv").fadeIn();
				});
			}
			{// After mail sent success move to login page submit button
				$("#submitlogin").click(function(){
					$('#username').val('');
					$('#password').val('');
					$("#headingchange").text('Login');
					$(".forgotpasswordsendsuccessdiv").hide();
					$(".loginformdiv").fadeIn();
				});
			}
			{/* security question submit button  */
				$("#secanswersubmit").click(function(){
					$("#verifysecurityform").validationEngine('validate');
				});
				$("#verifysecurityform").validationEngine({
					onSuccess: function() {
						var securitypass= $("#verifysecurityform").serialize();
						var amp = '&';
						var loginpass = amp + securitypass ;
						$.ajax({  
							type: "POST",
							url:base_url+"Login/verifysecurityinfo",
							data:"logindata="+ loginpass, 
							dataType:'json',
							success: function(data) {
								if((data.fail) == 'FAILED') {
									var msg='Invalid Security Answer';
									$('#securityanswer').validationEngine('showPrompt',msg, 'error', 'bottomLeft',true);
								} else {
									$("#headingchange").text('Change Password');
									$(".securityqaformdiv").hide();
									$(".changepasswordformdiv").fadeIn();
									$('#credentialuserid').val(data.userid);
									$("#newpassword").focus();
								}
							},
						});
					},
					onFailure: function() {
					}
				});	
			}		
			{/* password reset  submit button  */
				$("#changepasswordsubmit").click(function() {
					$("#changepasswordform").validationEngine('validate');
				});
				jQuery("#changepasswordform").validationEngine({
					onSuccess: function() {
						var securitypass= $("#changepasswordform").serialize();
						var amp = '&';
						var loginpass = amp + securitypass ;
						$.ajax({
							type: "POST",
							url:base_url+"Login/changepassword",
							data:"logindata="+ loginpass,
							success: function(msg) {
								if(msg == 'TRUE') {
									window.location=base_url;
								} else {
									var msg = 'Weak Password';
									$('#newpassword').validationEngine('showPrompt',msg, 'error', 'bottomLeft',true);
								}
							},
						});
					},
					onFailure: function() {
					}
				});	
			}
			{// on Click Logo Come to log In screen
				$("#homescreenlogo").click(function() {
					$(".forgetpasswordformdiv,.securityqaformdiv,.changepasswordformdiv").hide();
					$(".loginformdiv").fadeIn();
				});
			}
			{/* Signup link  */
				$("#signuplink").click(function() {
					$("#headingchange").text('Sign Up');
					$(".loginformdiv").hide();
					$(".signupformdiv").fadeIn();
					$("#signupname").focus();
				});
			}
			
			{//product reactivation activation
				$('#prodactivationclose').click(function(){
					$('#reactivationoverlay').hide();
				});
				//product reactivation
				$('#productrenewalbtn').click(function(){
					$("#productreactivatevalidate").validationEngine('validate');
				});
				$("#productreactivatevalidate").validationEngine({
					onSuccess: function() {
						$('#productrenewalbtn').prop("disabled", true);
						var uname = $('#username').val();
						var activatedata=$("#productreactivateform").serialize();
						var amp='&';
						var datas=amp+activatedata+amp+'username='+uname;
						$.ajax({
							type:"POST",
							url:base_url+"Installation/productreactivation",
							data:"datasets="+datas,
							dataType:'json',
							success: function(data) {
								if(data.status=='TRUE') {
									$('#reactivationoverlay').hide();
									$('#loginsubmit').trigger('click');
									$('#productrenewalbtn').prop("disabled", false);
								} else {
									$('#productrenewalbtn').prop("disabled", false);
									alertpopup(data.status);
								}
							},
						});
					},
					onFailure: function() {
						alertpopup('Please enter mandatory field values.');
					}
				});
			}
			
			
		});
		//send forget password link
		function sendpasswordlink(emailid) {
			var amp = '&';
			var info = amp+'emailid='+emailid+amp+'mailtype=fpassword';
			$.ajax({
				type: "POST",
				url:base_url+"datamail.php",
				data:"logindata="+ info,
				success: function(msg) {
					if(msg=='success') {
						// after mail success
						$(".mailprocessdiv").hide();
						$(".forgotpasswordsendokdiv").fadeIn();
					} else {//fail
						$("#processing").text('Password reset link has failed to deliver to your email address.');
						$("#processspinner").hide();
						$(".backtoforgetpassword").show();
					}
				}, 
			});
		}
		function alertpopup(txtmsg) {
			$(".alertinputstyle").val(txtmsg);
			$('#alerts').fadeIn('fast');
		}
		//access mailer
		function emaillogin(mail,p,domain) {
			var file='erpmail/rclogin.php';
			$('body').append('<form id="emaillogin" style="display:none;" name="emaillogin" method="post" action="'+base_url+''+file+'"><input type="hidden" name="mdomain" id="mdomain"/><input type="hidden" name="email" id="email"/><input type="hidden" name="pp" id="pp" value=""/><input type="hidden" name="action" id="action" value=""/><input type="submit"/></form>');
			$('#action').val('login');
			$('#pp').val(p);
			$('#email').val(mail);
			$('#mdomain').val(domain);
			$('#emaillogin').submit();
		}
	</script>
</head>
<?php 
$device = $this->Basefunctions->deviceinfo();?>
	
<body style='background:#f2f3fa !important'>
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row" style="max-width:93.5%;top: 20% !important;position: relative;left: 0% !important;">
		<div class="large-12 columns">
		<?php 	if($device=='phone') { ?>
			<div class="large-4 medium-8 small-8 small-offset-1 columns" style="position: relative;left: 13% !important;">
				<img src="<?php echo base_url();?>/img/homepagelogo.png" id="homescreenlogo" style="cursor:pointer"/>
			</div>
		<?php } else  { ?>
			<div class="large-4 medium-4 small-4 columns large-centered" style=" position: relative;left: 7.5% !important;">
				<img src="<?php echo base_url();?>/img/homepagelogo.png" id="homescreenlogo" style="cursor:pointer"/>
			</div>
		<?php } ?>
		</div>
		<div class="large-12 columns">&nbsp;</div>
		<div class="large-12 columns">&nbsp;</div>
		<div class="large-12 columns">
			<div class="large-4 small-12 columns large-centered ">
				<div class="large-12 columns paddingzero borderstyle" style="background:#fff;width: 100% !important;">
					<div class="large-12 columns headerformcaptionstyle" id="headingchange" style="text-align:center;font-size:1.2rem;top:10px;position:relative;">Login Engine</div>
					<!-- login form -->
					<form id="loginform" action="" class="loginformdiv">
						<span class="validationEngineContainer" id="loginindexvalidationspan">
							<div class="input-field large-12 columns">
								<input type="text" id="username" name="username" value="" class="validate[required,custom[email],maxSize[50]]" data-prompt-position="bottomLeft">
								<label for="username">Email Address<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="input-field large-12 columns">
								<input type="password" id="password" name="password" value="" class="validate[required,maxSize[30]]" data-prompt-position="bottomLeft">
								<label for="password">Password<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="large-12 columns">&nbsp;</div>
							<div class="large-12 columns centertext">							
								<input type="button" class="btn alertbtnyes" value="Login" id="loginsubmit" name="loginsubmit" />
							</div>
							<div class="large-12 columns centertext">							
								<div class="large-12 columns hpforgertpwdstyle"><span id="forgotlink">Forgot Password</span></div>
							</div>
						</span>
					</form>
					<form id="forgotpasswordform" action="" class="forgetpasswordformdiv hidedisplay">
						<span class="validationEngineContainer" id="forgotscreen">
							<div class="input-field large-12 columns">
								<input type="text" id="forgetusername" name="forgetusername" value="" class="validate[required,custom[email],maxSize[50]]" data-prompt-position="bottomLeft">
								<label for="forgetusername">Enter Your Email Address</label>
							</div>
							<div class="large-12 columns">&nbsp;</div>
							<div class="large-12  columns">	
								<div class="large-6 columns centertext">
									<div class="large-12 columns">
										<input type="button" class="btn homepageloginbtn" value="Submit" id="forgotsubmit"/>
									</div>
								</div>
								<div class="large-6 columns centertext">	
									<div class="large-12 columns">
										<input type="button" class="btn homepageloginbtn" value="Cancel" id="forgotcancel"/>
									</div>
								</div>
							</div>
							<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
							<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
						</span>
					</form>
					<div class="large-12 columns forgotpasswordsendsuccessdiv centertext hidedisplay">
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns" style="font-family: Segoe UI; font-size: 0.9rem; color:gray;">
							Password reset link has been sent to your email address.
							<div class="large-12 columns">&nbsp;</div>
						</div>
						<div class="large-12 columns">
							<div class="large-12 columns">							
								<input type="button" class="btn homepageloginbtn" value="Login" id="submitlogin"/>
							</div>
						</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div class="large-12 columns mailprocessdiv centertext hidedisplay">
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">
							<span id="processspinner" class="fa  fa-spinner fa-spin" title="processing"> </span>
							<span id="processing" class="" style="font-family: Segoe UI; font-size: 0.9rem; color:gray;" title="Processing"> Processing...</span>
						</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns backtoforgetpassword hidedisplay">							
							<input type="button" class="btn homepageloginbtn" value="back" id="backtoforgetpassword"/>
						</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div class="large-12 columns">
						<form id="verifysecurityform" action="" class="hidedisplay securityqaformdiv">
							<span id="securityques" class="validationEngineContainer">			
								<div class="large-12 columns">
									<label>Select Security Question</label>
									<input type="text" id="securityquest" name="securityquest" value="" class="validate[required,maxSize[30]]" data-prompt-position="bottomLeft" readonly>
								</div>
								<div class="large-12 columns">
									<label>Enter Answer</label>
									<input type="text" id="securityanswer" name="securityanswer" value="" class="validate[required,custom[onlyLetterNumber],maxSize[30]]" data-prompt-position="bottomLeft">
								</div>
								<input type="hidden" id ="userid"  value=""  name="userid" class=""/>
								<div class="large-12 columns">&nbsp;</div>
								<div class="large-12 columns centertext">							
									 <input type="button" class="btn homepageloginbtn" value="Submit" id="secanswersubmit"/>
								</div>	
								<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
								<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
							</span>
						</form>
						<!-- forgot password-->
						<form id="changepasswordform" action="" class="hidedisplay changepasswordformdiv">
							<span id="changepassword" class="validationEngineContainer">	
								<div class="large-12 columns">
									<label>Type New Password</label>
									<input type="password" id="newpassword" name="newpassword" value="" class="validate[required,minSize[6],maxSize[30]]" data-prompt-position="bottomLeft">
								</div>
								<div class="large-12 columns">
									<label>Retype New Password</label>
									<input type="password" id="retypepassword" name="retypepassword" value="" class="validate[required,minSize[6],maxSize[30],equals[newpassword]]" data-prompt-position="bottomLeft">
								</div>			
								<input type="hidden" id ="credentialuserid"  value=""  name="userid" class=""/>
								<div class="large-12 columns">&nbsp;</div>
								<div class="large-12 columns centertext">							
									 <input type="button" class="btn homepageloginbtn" value="Submit" id="changepasswordsubmit"/>
								</div>	
								<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
								<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
							</span>
						</form>
						<div class="large-12 columns" style="text-align:center;">
							<span></span>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<div class="large-12 columns" style="position:absolute;">
			<div class="overlay" id="successalerts">	
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>		
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>
				<div class="large-3 medium-6 small-10 large-centered medium-centered small-centered columns overlayborder">
					<div class="row">&nbsp;</div>
					<div class="row">
						<div class="large-12 columns alertsheadercaptionstyle" style="text-align:left">
							<div class="small-12 columns"><span style="color:#ffffff;padding-right:0.5rem"><i class="material-icons">warning</i> </span> Alert</div>
						</div>
						<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
						<div class="large-12 large-centered columns" style="background:#f5f5f5;text-align:center">
							<span class="alertinputstyle">Company Create Successfully !!! Redirecting To Home Page..</span>
							<div class="large-12 column">&nbsp;</div>
						</div>
					</div>			
					<div class="row">&nbsp;</div>
				</div>
			</div>
		</div>
	</div>
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay" id="failurealerts">	
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>		
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="large-3 medium-6 small-10 large-centered medium-centered small-centered columns overlayborder">
				<div class="row">&nbsp;</div>
				<div class="row">
				<div class="large-12 columns alertsheadercaptionstyle" style="text-align:left">
					<div class="small-12 columns"><span style="color:#ffffff;padding-right:0.5rem"><i class="material-icons">warning</i></span> Alert</div>
				</div>
				<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
					<div class="large-12 large-centered columns" style="background:#f5f5f5;text-align:center">
						<span class="alertinputstyle">Sorry Company Not Created !!!</span>
						<div class="large-12 column">&nbsp;</div>
					</div>
				</div>			
				<div class="row">&nbsp;</div>
			</div>
		</div>
	</div>
	<!-- Template Print Overlay  Overlay-->
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay" id="reactivationoverlay" style="overflow-y: scroll;overflow-x: hidden">		
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>		
			<div class="large-4 medium-6 large-centered medium-centered columns" >
				<form method="POST" name="productreactivateform" style="" id="productreactivateform" action="" enctype="" class="overlayborder">
					<span id="productreactivatevalidate" class="validationEngineContainer"> 
						<div class="row">&nbsp;</div>
						<div class="alert-panel">
							<div class="alertmessagearea">
								<div class="alert-title">Product Renewal</div>
								<div class="alert-message">
									<div class="input-field overlayfield ">  
										<span class="firsttab" tabindex="1000"></span>
								        <input type="text" id="userprodrenewalkey" name="userprodrenewalkey" class="validate[required] ffield" value="" tabindex="1001"/>
										<label for="userprodrenewalkey">Product Renewal Key<span class="mandatoryfildclass">*</span></label>
									</div>
									<div style="padding-top:10px;"><span class="mandatoryfildclass">*Your Product is expired.Please enter product renewal key</span></div>
								</div>
							</div>
							<div class="alertbuttonarea">
								<input type="button" class="alertbtn" value="Activate" name="productrenewalbtn" id="productrenewalbtn" tabindex="1002">
								<input type="button" class="alertbtn flloop" value="Cancel" name="cancel" id="prodactivationclose" tabindex="1003">
								<span class="lasttab" tabindex="1004"></span>
							</div>
						</div>
						<div class="row">&nbsp;</div>
					</span>
				</form>
			</div>
		</div>
	</div>
	<!--- process overlay-->
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay" id="processoverlay" style="overflow-y: hidden;overflow-x: hidden;z-index:100;">		
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div><div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="large-2 columns end large-centered">
				<div class="large-12 columns card-panel" style="text-align: left; background: #fff;padding-bottom: 0px; ">
					<div class="small-12 large-12 medium-12 columns paddingzero">
						<span><div class="spinner"></div></span><span style="display:inline-block;height:35px;vertical-align:middle;text-align: center;width: 100%;">&nbsp;&nbsp;&nbsp;  Processing...</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>   
</html>