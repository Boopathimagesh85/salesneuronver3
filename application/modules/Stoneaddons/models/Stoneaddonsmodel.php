<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Stoneaddonsmodel extends CI_Model
{
   	private $stoneaddonsmoduleid = 48;	
	public function __construct()
    {
        parent::__construct();
		$this->load->model( 'Base/Basefunctions' );
		$this->load->model('Base/Crudmodel');
    }
    public function stoneview($sidx,$sord,$start,$limit,$wh) {
    	$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$chargeid = $_GET['chargeid'];
    	if($chargeid == 0){
    		$this->db->select('SQL_CALC_FOUND_ROWS stone.stoneid,stone.stonename,stone.stonerate,stonesize.stonesizename',false);
    		$this->db->from('stone');
    		$this->db->join('stonesize','stonesize.stonesizeid = stone.stonesizeid');
    		$this->db->where_not_in('stone.status',array(3,0));
    		if($wh != "1") {
    			$this->db->where($wh);
    		}
    		$this->db->order_by($sidx,$sord);
    		$this->db->limit($limit,$start);
    		$result=$this->db->get();
    	}else{
    		$result=$this->db->query('SELECT stone.stoneid, stone.stonename,stone.stonerate,ROUND(stonechargedetail.applyrate,'.$amountround.') as  stoneamount,stonesize.stonesizename FROM `stone` 
LEFT JOIN stonechargedetail ON stonechargedetail.stoneid=stone.stoneid and stonechargedetail.stonechargeid='.$chargeid.' AND `stonechargedetail`.`status` NOT IN (3, 0)
LEFT JOIN stonesize on stonesize.stonesizeid=stone.stonesizeid and stonesize.status=1 WHERE `stone`.`status` NOT IN (3, 0) ORDER BY `stone`.`stoneid` asc LIMIT '.$limit.' OFFSET '.$start.' ');
    	}
    	return $result;
}
	public function viewdynamicdatainfofetch($tablename,$sortcol,$sortord,$pagenum,$rowscount,$chargeid)
	{
		$chargeid = $_GET['filter'];
		$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		if($chargeid==0) {
			$dataset ='stone.stoneid,CONCAT(stone.stonename,"/",stonesize.stonesizename) as stonename,ROUND(stone.purchaserate,'.$amountround.') as  purchaserate,ROUND(stone.stonerate,'.$amountround.') as  stonerate,ROUND(stone.purchaserate,'.$amountround.') as applyrate';
			$join =' LEFT OUTER JOIN stonesize ON stonesize.stonesizeid = stone.stonesizeid and stonesize.status=1';
		} else {
			$dataset ='stone.stoneid,CONCAT(stone.stonename,"/",stonesize.stonesizename) as stonename,ROUND(stone.purchaserate,'.$amountround.') as  purchaserate,ROUND(stone.stonerate,'.$amountround.') as  stonerate,ROUND(IFNULL(stonechargedetail.applyrate,stone.purchaserate),'.$amountround.') as  applyrate ';
			$join =' LEFT JOIN stonechargedetail ON stonechargedetail.stoneid=stone.stoneid and stonechargedetail.stonechargeid='.$chargeid.' AND `stonechargedetail`.`status` NOT IN (3, 0)';
			$join .=' LEFT JOIN stonesize on stonesize.stonesizeid=stone.stonesizeid and stonesize.status=1';
		}
		$status=$tablename.'.status='.$this->Basefunctions->activestatus.'';	
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		/* query */
		$result = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '. $status.' ORDER BY '.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$data=array();
		$i=0;
		foreach($result->result() as $row) {
			$stoneid = $row->stoneid;
			$stonename = $row->stonename;
			$stonerate =$row->stonerate;
			$applyrate =$row->applyrate;
			$purchaserate =$row->purchaserate;
			$data[$i]=array('id'=>$stoneid,$stonename,$purchaserate,$stonerate,$applyrate);
			$i++;
		}
		$finalresult=array('0'=>$data,'1'=>$page,'2'=>'json');
		return $finalresult;
	}
    public function stonechargecreate(){
		$chargeid='';
		$charge='';
    	$stonecharge=$_POST['stonecharge'];
    	// insert stone charge - parent table
    	$insert_charge=array(
    			'stonechargename' => $_POST['stonecharge'],
    			'industryid'=>$this->Basefunctions->industryid,
				'branchid'=>$this->Basefunctions->branchid
    	);
    	$insert_charge = array_merge($insert_charge,$this->Crudmodel->defaultvalueget());
    	$this->db->insert('stonecharge',array_filter($insert_charge));
    	$stonechargeid = $this->db->insert_id();
    	
    	$rowstoneid=$_POST['allstoneid'];
    	$allstoneid = explode(',',$rowstoneid);
    	$griddata=$_POST['stoneaddongriddata'];
    	$formdata=json_decode( $griddata, true );
		$count = count($formdata);
		for( $i=0; $i <$count ; $i++ ) {
			$stoneid = $formdata[$i]['stoneid'];
			//$purchaserate = $formdata[$i]['purchaserate'];
			$stonerate = floatval($formdata[$i]['basicrate']); //Purchase rate
			$applyrate = floatval($formdata[$i]['applyrate']); //Apply rate
    		if($stonerate == $applyrate || $applyrate == 0){  // unchanges(0) rate or base rate & apply rate are same
    			
    		} else if($stonerate != $applyrate) { // changes rate if purchase rate is not equal to apply rate, then it will insert into the database
				$insert_chargedetail=array(
    					'stonechargeid'=>$stonechargeid,
    					'stoneid'=>$stoneid,
    					'applyrate'=>$applyrate,
    					'status' =>$this->Basefunctions->activestatus
    			);
    			$insert_chargedetail = array_merge($insert_chargedetail,$this->Crudmodel->defaultvalueget());
    			$this->db->insert('stonechargedetail',array_filter($insert_chargedetail));
    		} else{
    			
    		}
    	}
    	// notification log
    	$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
    	$activity = ''.$user.' created stone charge-'.$_POST['stonecharge'].'';
    	$this->Basefunctions->notificationcontentadd($stonechargeid,'Added',$activity ,$userid,$this->stoneaddonsmoduleid);
    	echo 'SUCCESS';
    }
    public function stoneaddonsretrieve(){
    	$id=$_GET['primaryid'];
    	$this->db->select('stonecharge.stonechargeid,stonecharge.stonechargename');
    	$this->db->from('stonecharge');
    	$this->db->where('stonechargeid',$id);
    	$info=$this->db->get()->row();
    	$jsonarray=array(
    			'stonechargeprimaryid'=>$info->stonechargeid,
    			'stonechargeprimaryname'=>$info->stonechargename,
    			'stonecharge'=>$info->stonechargename
    			);
    	echo json_encode($jsonarray);
    }
    public function stoneaddonsupdate(){
    	$id=$_POST['primaryid'];
		$update_charge=array(
    			'stonechargename' => $_POST['stonecharge'],
    			'industryid'=>$this->Basefunctions->industryid,
				'branchid'=>$this->Basefunctions->branchid
    	  	);
    	$update_charge = array_merge($update_charge,$this->Crudmodel->updatedefaultvalueget());
    	$this->db->where('stonechargeid', $id);
    	$this->db->update('stonecharge',$update_charge);
    	
    	$rowstoneid=$_POST['allstoneid'];
    	$allstoneid = explode(',',$rowstoneid);
    	$griddata=$_POST['stoneaddongriddata'];
    	$formdata=json_decode( $griddata, true );
    	$count = count($formdata);
    	for( $i=0; $i <$count ; $i++ ) {
    		$stoneid = $formdata[$i]['stoneid'];
    		$stonerate = $formdata[$i]['basicrate'];
    		$applyrate = $formdata[$i]['applyrate'];
    		$updatedraterow=$this->db->where('stonechargeid',$id)->where('stoneid',$stoneid)->get('stonechargedetail');
    		$updatedratedata=$updatedraterow->row();
    		if($stonerate == $applyrate || $applyrate == 0){  // unchanges(0) rate or base rate & apply rate are same
    		       if($updatedraterow->num_rows() > 0){ // updated rate changed into deleted status
    		    	$delete = $this->Crudmodel->deletedefaultvalueget();
    		    	$this->db->where('stonechargedetailid',$updatedratedata->stonechargedetailid);
    		    	$this->db->update('stonechargedetail',$delete);
    		    }else{
    		    	
    		    }
    		}else if($stonerate < $applyrate) { // changes rate if purchase rate is not equal to apply rate, then it will update into database
    			if($updatedraterow->num_rows() > 0) {  // already apply rate will be changed
    				$update_chargedetail=array(
    						'applyrate'=>$applyrate,
    						'status'=>1
    							);
    				$update_chargedetail = array_merge($update_chargedetail,$this->Crudmodel->updatedefaultvalueget());
    				$this->db->where('stonechargedetailid', $updatedratedata->stonechargedetailid);
    				$this->db->where('stonechargeid',$id);
    				$this->db->where('stoneid', $stoneid);
    				$this->db->update('stonechargedetail',$update_chargedetail);
    				// notification log
    				$user = $this->Basefunctions->username;
    				$userid = $this->Basefunctions->logemployeeid;
    				$activity = ''.$user.' updated stone charge-'.$_POST['stonecharge'].'';
    				$this->Basefunctions->notificationcontentadd($id,'Updated',$activity ,$userid,$this->stoneaddonsmoduleid);
    			} else {  // new apply rate inserted
    				$insert_chargedetail=array(
    						'stonechargeid'=>$id,
    						'stoneid'=>$stoneid,
    						'applyrate'=>$applyrate
    			    );
    				$insert_chargedetail = array_merge($insert_chargedetail,$this->Crudmodel->defaultvalueget());
    				$this->db->insert('stonechargedetail',array_filter($insert_chargedetail));
    			}
    		}else{
    		}
    	}
		// notification log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' updated stone charge-'.$_POST['stonecharge'].'';
		$this->Basefunctions->notificationcontentadd($id,'Updated',$activity ,$userid,$this->stoneaddonsmoduleid);
    	echo 'SUCCESS';
    }
    public function stonechargedelete(){
    	$id=$_GET['primaryid'];
    	//delete stone charge
    	$delete = $this->Crudmodel->deletedefaultvalueget();
    	$this->db->where('stonechargeid',$id);
    	$this->db->update('stonecharge',$delete);
    	
    	// deleted stone charge detail also
    	$this->db->where('stonechargeid',$id);
    	$this->db->update('stonechargedetail',$delete);
    	
    	$updatedefault=array('stonechargeid'=>1);
    	$this->db->where('stonechargeid',$id);
    	$this->db->update('account',$updatedefault);
    	// notification log
    	$stonecharge = $this->Basefunctions->singlefieldfetch('stonechargename','stonechargeid','stonecharge',$id);
    	$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
    	$activity = ''.$user.' deleted stone charge-'.$stonecharge.'';
    	$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,$this->stoneaddonsmoduleid);
    	echo 'SUCCESS';
    }
    public function stoneaccountcharge(){
    	// default set for stonecharge id
    	$updatedefault=array('stonechargeid'=>1);
		$updatedatetime = array_merge($updatedefault,$this->Crudmodel->updatedefaultvalueget());
		/*For account*/
		$this->db->where('stonechargeid',$_POST['chargeid']);
		$this->db->update('account',$updatedatetime);
			/*For Accountgroup*/
		$this->db->where('stonechargeid',$_POST['chargeid']);
		$this->db->update('accountgroup',$updatedatetime);
		if(!empty($_POST['accountgroup'])){
			if($_POST['accountgroup'] != '1') {
				$accountgroup = $this->Basefunctions->singlefieldfetch('accountid','accountgroupid','accountgroup',$_POST['accountgroup']); 
				//$accountid = substr($accountgroup,2);
				$accountid = explode(',',$accountgroup);
				// Charge id update in account for account group based
				$update=array('stonechargeid'=>$_POST['chargeid']);
				$updateacc = array_merge($update,$this->Crudmodel->updatedefaultvalueget());
				$this->db->where_in('accountid',$accountid);
				$this->db->update('account',$updateacc);
				// Stonechargeid updated in accountgroup
				$updateaccgrp = array('stonechargeid'=>$_POST['chargeid']);
				$updateaccgrpdatetime = array_merge($updateaccgrp,$this->Crudmodel->updatedefaultvalueget());
				$this->db->where_in('accountgroupid',$_POST['accountgroup']);
				$this->db->update('accountgroup',$updateaccgrpdatetime);
			}
			//audit-log
			$userid = $this->Basefunctions->logemployeeid;
			$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
			$stchargename = $this->Basefunctions->singlefieldfetch('stonechargename','stonechargeid','stonecharge',$_POST['chargeid']);
			$activity = ''.$user.' updated StoneCharge - '.$stchargename.'';
			$this->Basefunctions->notificationcontentadd($_POST['chargeid'],'Updated',$activity ,$userid,48);
       	}
		if(!empty($_POST['account'])){
			if($_POST['account'] != 1) {
				// Charge id update in account
				$account=explode(',',$_POST['account']);
				$update=array('stonechargeid'=>$_POST['chargeid']);
				$updateall = array_merge($update,$this->Crudmodel->updatedefaultvalueget());
				$this->db->where_in('accountid',$account);
				$this->db->update('account',$updateall);
				//audit-log
				$userid = $this->Basefunctions->logemployeeid;
				$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
				$stchargename = $this->Basefunctions->singlefieldfetch('stonechargename','stonechargeid','stonecharge',$_POST['chargeid']);
				$activity = ''.$user.' updated StoneCharge - '.$stchargename.'';
				$this->Basefunctions->notificationcontentadd($_POST['chargeid'],'Updated',$activity ,$userid,48);
			}
	    }
    	echo 'SUCCESS';
    }
    public function stonechargedetails() {
    	$chargeid=$_GET['primaryid'];
    	$chargename=$this->Basefunctions->singlefieldfetch('stonechargename','stonechargeid','stonecharge',$_GET['primaryid']);
    	$this->db->select('accountid,accountname,stonechargeid');
    	$this->db->from('account');
		$this->db->where_in('accounttypeid',array(6));
    	$this->db->where_in('stonechargeid',array(1,$chargeid));
    	$this->db->where_not_in('status',array(3,0,2));
    	$info=$this->db->get();
    	foreach($info->result() as $data){
    		$accountdetails[]=array(
    				'accountid'=>$data->accountid,
    				'accountname'=>$data->accountname,
    				'stonechargeid'=>$data->stonechargeid
    		);
    	}
    	$accountdetails['chargename']=$chargename;
    	echo json_encode($accountdetails);
    }
    public function getchargedetailssetdd() {
    	$chargeid=$_GET['primaryid'];
    	$accountid='';
    	$this->db->select('accountid');
    	$this->db->from('account');
    	$this->db->where('stonechargeid',$chargeid);
		$this->db->where_in('accounttypeid',array(6));
    	$this->db->where_not_in('status',array(3,0,2));
    	$info=$this->db->get();
    	foreach($info->result() as $data){
    				$accountid.=$data->accountid.',';
  	 	}
    	$accountid = rtrim($accountid,','); 
    	echo json_encode($accountid);
    }
    public function getaccountgroup() {
    	$accountdetails = array ();
    	$accountgroupdetails = array();
    	$chargeid=$_POST['primaryid'];
    	$this->db->select('accountid');
    	$this->db->from('account');
		$this->db->where_in('accounttypeid',array(6));
    	//$this->db->where_in('stonechargeid',array(1,$chargeid));
    	$this->db->where('status',$this->Basefunctions->activestatus);
    	$info=$this->db->get();
    	foreach($info->result() as $data){
    		array_push($accountdetails,$data->accountid);
    	}
    	$this->db->select('accountgroupid,accountgroupname');
    	$this->db->from('accountgroup');
    	$this->db->where("FIND_IN_SET('1',accountid) >", 0);
    	$i=0;
    	foreach($accountdetails as $wdata) {
    		$this->db->or_where("FIND_IN_SET('$accountdetails[$i]',accountid) >", 0);
    		$i++;
    	}
    	$this->db->where('status',$this->Basefunctions->activestatus);
    	$accountgroup=$this->db->get();
    	foreach($accountgroup->result() as $data){
    		$accountgroupdetails[]=array(
    				'accountgroupid'=>$data->accountgroupid,
    				'accountgroupname'=>$data->accountgroupname,
    			);
    	}
    	echo json_encode($accountgroupdetails);
    }
    public function setaccountgroup() {
    	$accountdetails = array ();
    	$accountgroupid = '';
    	$chargeid=$_POST['primaryid'];
    	$this->db->select('accountid');
    	$this->db->from('account');
    	$this->db->where('stonechargeid',$chargeid);
    	$this->db->where_not_in('status',array(3,0,2));
    	$info=$this->db->get();
    	foreach($info->result() as $data){
    		array_push($accountdetails,$data->accountid);
    	}
    	$this->db->select('accountgroupid,accountgroupname');
    	$this->db->from('accountgroup');
    	$i=0;
    	foreach($accountdetails as $wdata) {
    		$this->db->where("FIND_IN_SET('$accountdetails[$i]',accountid) >", 0);
    		$i++;
    	}
    	$this->db->where('status',$this->Basefunctions->activestatus);
    	$accountgroup=$this->db->get();
    	if($accountgroup->num_rows()>0){
	    	foreach($accountgroup->result() as $data){
	    		$accountgroupid.=$data->accountgroupid.',';
	    	}
	    	$this->db->select('accountid');
	    	$this->db->from('account');
	    	$this->db->where('stonechargeid',$chargeid);
	    	$this->db->where('accountgroupid',1);
	    	$this->db->where_not_in('status',array(3,0,2));
	    	$info_nongroup=$this->db->get();
	    	if($info_nongroup->num_rows()>0){
	    	   $accountgroupid = '';
	    	}else if(count($accountdetails) == 0){
	    		$accountgroupid = '';
	    	}else{
	    	  $accountgroupid = rtrim($accountgroupid,',');
	    	}
    	}
    	else{
    		$accountgroupid='';
    	}
    	echo json_encode($accountgroupid);
    }
}