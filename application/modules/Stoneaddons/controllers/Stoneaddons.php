<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Stoneaddons extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Stoneaddons/Stoneaddonsmodel');
		$this->load->model('Base/Basefunctions');
    }
	//first basic hitting view
    public function index()
    {	
		$moduleid = array(48);
		sessionchecker($moduleid);
		$userid = $this->Basefunctions->userid;
		$data['account'] = $this->Basefunctions->simpledropdown_var('account','accountid,accountname','accountid');
		//$data['account'] = $this->Basefunctions->simpledropdownwithcond('accountid','accountname','account','accounttypeid','6');
		//action		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['rate_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //rate round-off
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$this->load->view('stoneaddonsview',$data);
    }
    public function stoneview() {
    	$grid=$this->Basefunctions->getpagination();
    	$result=$this->Stoneaddonsmodel->stoneview($grid['sidx'],$grid['sord'],$grid['start'],$grid['limit'],$grid['wh']);
    	$pageinfo=$this->Basefunctions->page();
    	header("Content-type: text/xml;charset=utf-8");
    	$s = "<?xml version='1.0' encoding='utf-8'?>";
    	$s .=  "<rows>";
    	$s .= "<page>".$_GET['page']."</page>";
    	$s .= "<total>".$pageinfo['totalpages']."</total>";
    	$s .= "<records>".$pageinfo['count']."</records>";
    	ob_clean();
    	foreach($result->result() as $row) {
    		$s .= "<row id='". $row->stoneid."'>";
    		$s .= "<cell><![CDATA[".$row->stoneid."]]></cell>";
    		$s .= "<cell><![CDATA[".$row->stonename."/".$row->stonesizename."]]></cell>";
    		$s .= "<cell><![CDATA[".$row->stonerate."]]></cell>";
    		if(!isset($row->applyrate) || $row->applyrate == ''){
    		$individualrate=0;
    		}else{
    			$individualrate = $row->applyrate;
    		}
    		$s .= "<cell><![CDATA[".$individualrate."]]></cell>";
    		$s .= "</row>";
    	}
    	$s .= "</rows>";
    	echo $s;
    }
	/* Fetch Main grid header,data,footer information */
	public function gridinformationfetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$stonechargeid=$_GET['filter'];
		
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'stone.stoneid') : 'stone.stoneid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		
		$colinfo = array('colid'=>array('1','1','1','1'),'colname'=>array('Stone Name','Purchase Rate','Sales Rate','Apply Rate'),'colicon'=>array('','','',''),'colsize'=>array('200','200','200','200'),'colmodelname'=>array('stonename','purchaserate','stonerate','applyrate'),'colmodelindex'=>array('stone.stonename','stone.purchaserate','stone.stonerate','stonechargedetail.applyrate'),'coltablename'=>array('stone','stone','stone','stone'),'uitype'=>array('2','2','2','2'),'modname'=>array('Stone Master','Stone Master','Stone Master','Stone Master'));
		
		$result=$this->Stoneaddonsmodel->viewdynamicdatainfofetch($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$stonechargeid,$colinfo);
		$device = $this->Basefunctions->deviceinfo();
		//if($device=='phone') {
			//$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'stonecharge',$width,$height);
		//} else {
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'stonecharge',$width,$height,'TEXT');
		//}
		echo json_encode($datas);
	}
    public function stonechargecreate(){
    	$this->Stoneaddonsmodel->stonechargecreate();
    }
    public function stoneaddonsretrieve(){
    	$this->Stoneaddonsmodel->stoneaddonsretrieve();
    }
    public function stoneaddonsupdate(){
    	$this->Stoneaddonsmodel->stoneaddonsupdate();
    }
    public function stonechargedelete(){
    	$this->Stoneaddonsmodel->stonechargedelete();
    }
    public function stoneaccountcharge(){
    	$this->Stoneaddonsmodel->stoneaccountcharge();
    }
    public function stonechargedetails(){
    	$this->Stoneaddonsmodel->stonechargedetails();
    }
    public function stonechargedetailsset(){
    	$this->Stoneaddonsmodel->getchargedetailssetdd();
    }
    public function getaccountgroup(){
    	$this->Stoneaddonsmodel->getaccountgroup();
    }
    public function setaccountgroup(){
    	$this->Stoneaddonsmodel->setaccountgroup();
    }
}