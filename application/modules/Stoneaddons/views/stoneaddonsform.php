<style type="text/css">
	#stonechargegrid .gridcontent{
		height: 65vh !important;
	}
	#stonecharge{
		color: black !important;
	}
	.stonechargegridfootercontainer {
		display:none;
	}
</style>
<?php
	$device = $this->Basefunctions->deviceinfo();
	$dataset['gridtitle'] = $gridtitle;
	$dataset['titleicon'] = $titleicon;
	$this->load->view('Base/mainformwithgridmenuheader.php',$dataset);
	$defaultrecordview = $this->Basefunctions->get_company_settings('mainviewdefaultview');
	echo hidden('mainviewdefaultview',$defaultrecordview);
?>
<div class="large-12 columns addformunderheader">&nbsp; </div>
<div class="large-12 columns addformcontainer">
	<div class="row mblhidedisplay">&nbsp;</div>
	<div id="subformspan1" class="hiddensubform"> 
		<form method="POST" name="stoneaddonsform" class="stoneaddonsform"  id="stoneaddonsform">
			<div id="stoneaddonaddwizard" class="validationEngineContainer">				
			<div id="stoneaddoneditwizard" class="validationEngineContainer">
			<div id="cloneformvalidation" class="validationEngineContainer">				
				<?php
	if($device=='phone') 
	{
		?>	
				<div class="large-4 columns" style="padding-bottom:1rem !important;">	
					<div class="large-12 columns cleardataform" style="padding:0px !important; box-shadow:none;background:#f2f3fa !important;">
						<div class="large-12 columns headerformcaptionstyle" style="background:#f2f3fa !important;">Stone Charge Details</div>
	<?php } else { ?>
	<div class="large-4 columns paddingbtm" style="padding-bottom:1rem !important;">	
					<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
						<div class="large-12 columns headerformcaptionstyle" >Stone Charge Details</div>
	
	<?php  } ?>
						<div class="input-field large-12 columns">
							<input type="text" class="validate[required,funcCall[chargenamecheck],maxSize[100],custom[onlyLetterNumberSp]]" id="stonecharge" name="stonecharge" value="" data-table="stonecharge" data-validatefield="stonechargename" data-primaryid="">
							<label for="stonecharge">Stone Charge Name<span class="mandatoryfildclass">*</span></label>
							<input type="hidden" name="txteditchargeid" id="txteditchargeid" value="" />
						</div>
						<input type="hidden" id="stonechargeprimaryid" name="stonechargeprimaryid"/>
						   <input type="hidden" id="stonechargeprimaryname" name="stonechargeprimaryname"/>
						   <input type="hidden" id="stonechargesortorder" name="stonechargesortorder"/>
						   <input type="hidden" id="stonechargesortcolumn" name="stonechargesortcolumn"/>
						    <input type="hidden" id="stoneaddonsortorder" name="stoneaddonsortorder"/>
						   <input type="hidden" id="stoneaddonsortcolumn" name="stoneaddonsortcolumn"/>
						   <input type="hidden" value="<?php echo $rate_round; ?>" id="rate_round" name="rate_round"/>
						   <input type="hidden" name="viewactiontype" id="viewactiontype" value="0" />
						   <input type="hidden" id="viewfieldids" name="viewfieldids" value="48" />
					<div class="large-12 columns "> &nbsp; </div>
					</div>
				</div>	
				</div>
			</div>	
			</div>	
		</form>
		<input type="hidden" id="stonechargeid" name="stonechargeid"/>
		<div class="large-8 columns paddingbtm ">	
			<div class="large-12 columns viewgridcolorstyle borderstyle" style="padding-left:0;padding-right:0;">	
				<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0rem 0rem; height:auto; line-height:2.3rem;">	
					<div class="large-6  medium-6 small-12 columns small-only-text-center" style="text-align:left">Stone Charge List</div>
					</div>
				<?php
					$device = $this->Basefunctions->deviceinfo();
					if($device=='phone') {
						echo '<div class="large-12 columns paddingzero forgetinggridname" id="stonechargegridwidth"><div class=" inner-row-content inner-gridcontent" id="stonechargegrid" style="height:70vh !important;top:0px;">
							<!-- Table header content , data rows & pagination for mobile-->
						</div>
						<footer class="inner-gridfooter footercontainer" id="stonechargegridfooter">
							<!-- Footer & Pagination content -->
						</footer></div>';
					} else {
						echo '<div class="large-12 columns forgetinggridname" id="stonechargegridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="stonechargegrid" style="max-width:2000px; height:70vh !important;top:0px;">
						<!-- Table content[Header & Record Rows] for desktop-->
						</div>
						<div class="inner-gridfooter footer-content footercontainer" id="stonechargegridfooter" style="display:none;">
							<!-- Footer & Pagination content -->
						</div></div>';
					}
				?>	
			</div>
		</div>
		</div>
		</div>
