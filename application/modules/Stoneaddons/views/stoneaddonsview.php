<!DOCTYPE html>
<html lang="en">
<head>
	<style>
		input[type=text].rowtext{height: 1.2rem; margin: 0;color: #222;}
		input:not([type]):focus:not([readonly]), input[type=text]:focus:not([readonly]), input[type=password]:focus:not([readonly]), input[type=email]:focus:not([readonly]), input[type=url]:focus:not([readonly]), input[type=time]:focus:not([readonly]), input[type=date]:focus:not([readonly]), input[type=datetime]:focus:not([readonly]), input[type=datetime-local]:focus:not([readonly]), input[type=tel]:focus:not([readonly]), input[type=number]:focus:not([readonly]), input[type=search]:focus:not([readonly]), textarea.materialize-textarea:focus:not([readonly])
            {
                color:#000000 !important;
            }
            }
	</style>
	<?php $this->load->view('Base/headerfiles'); ?>
	<?php $this->load->view('Base/basedeleteform'); ?>
	
    </head>
<body>
	<?php
		$moduleid = implode(',',$moduleids);
		$device = $this->Basefunctions->deviceinfo();
		$dataset['gridenable'] = "yes"; //no
		$dataset['griddisplayid'] = "stoneaddonscreationview";
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['spanattr'] = array();
		$dataset['gridtableid'] = "stoneaddonsgrid";
		$dataset['moduleid'] = $moduleids;
		$dataset['griddivid'] = "stoneaddonsgriddiv";
		$dataset['forminfo'] = array(array('id'=>'stoneaddonsformadd','class'=>'hidedisplay','formname'=>'stoneaddonsform'));
		if($device=='phone') {
			$this->load->view('Base/gridmenuheadermobile',$dataset);
			$this->load->view('Base/overlaymobile');
			$this->load->view('Base/viewselectionoverlay');
		} else {
			$this->load->view('Base/gridmenuheader',$dataset);
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
		$this->load->view('Base/basedeleteform');
	?>
	<!--View Creation Overlay-->
				<div class="hidedisplay" id="viewcreationformdiv">
				</div>
	<?php
		$this->load->view('Base/basedeleteformforcombainedmodules');
	?>	
	
	</body>
	<?php $this->load->view('Base/bottomscript'); ?>
	<script src="<?php echo base_url();?>js/Stoneaddons/stoneaddons.js" type="text/javascript"></script>
	<!----stone account overlay----->
	<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="stoneaccountoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40;">		
		<div class="row sectiontoppadding">&nbsp;</div>
		<div class="large-4 medium-4 small-12 large-centered medium-centered small-centered columns">
		<form id="stoneaccountform" name="stoneaccountform" class="">
			<div id="stoneaccountvalidate" class="validationEngineContainer">
				<div class="border-modal-8px" style="background-color:#fff !important;transition: box-shadow .25s;border-radius: 8px;0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);">
					<div class="alertmessagearea">
					<div class="alert-title">Stone Account</div>
					<div class="alert-message">
						<span class="firsttab" tabindex="1000"></span>
						<div class="input-field overlayfield">
							<label>Stone Charge</label>
							<input type="text" class="" id="accountstonecharge" name="accountstonecharge" value="" readonly>
							<input type="hidden" id="chargeid" name="chargeid"/>
						</div>
						<div class="static-field overlayfield">
							<label>Account Group</label>								
							<select id="accountgroup" name="accountgroup" class="chzn-select"  data-prompt-position="topLeft:14,36" tabindex="106">
							</select>
						</div>
						<div class="static-field overlayfield accountdiv">
							<label>Account</label>								
							<select id="account" name="account" class="chzn-select dropdownchange"  data-prompt-position="topLeft:14,36" tabindex="106" multiple>
							</select>
						</div>
					</div>
					</div>
					<div class="alertbuttonarea">
						<input type="button" id="stoneaccountbtn" name="" tabindex="1001" value="Save" class="alertbtnyes" >
						<input type="button" id="closestonechargeoverlay" name="" value="Close" tabindex="1002" class="flloop alertbtnno alertsoverlaybtn" >
						<span class="lasttab" tabindex="1003"></span>
					</div>
				</div>
			</div>
		</form>
		</div>		
	</div>
</div>
</html>
