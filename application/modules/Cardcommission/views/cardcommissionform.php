<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<div class="gridviewdivforsh">
				<?php
					$this->load->view('Base/singlemainviewformwithgrid');
					$this->load->view('Base/formfieldgeneration');
					//function call for form fields generation
					formfieldsgeneratorwithgrid($modtabsecfrmfkdsgrp,$filedmodids);
					$defaultrecordview = $this->Basefunctions->get_company_settings('mainviewdefaultview');
					echo hidden('mainviewdefaultview',$defaultrecordview);
				?>
			</div>			
		</div>
	</div>
</div>
<!--Overlay-->
<div>
	<?php $this->load->view('Base/viewselectionoverlay'); ?>
</div>