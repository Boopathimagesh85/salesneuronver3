<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('Base/headerfiles'); ?>	
</head>
<body>
<?php
	$dataset['gridenable'] = "1"; //0-no  1-yes
	$dataset['griddisplayid'] = "cardcommissionaddformview"; //add form-div id
	$dataset['maingridtitle'] = $gridtitle['title'];  // form header
	$dataset['gridtitle'] = $gridtitle['title'];
	$dataset['titleicon'] = $gridtitle['titleicon']; //grid header
	$dataset['spanattr'] = array();     //
	$dataset['gridtableid'] = "cardcommissionaddgrid"; //grid id
	$dataset['griddivid'] = "cardcommissionaddgridnav"; //grid pagination
	$this->load->view('cardcommissionform',$dataset); 
	$this->load->view('Base/basedeleteform');
	$device = $this->Basefunctions->deviceinfo();
	if($device=='phone') {
		$this->load->view('Base/overlaymobile');
	} else {
		$this->load->view('Base/overlay');
	}
	$this->load->view('Base/modulelist');
?>	
</body>
	<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js Files -->	
	<script src="<?php echo base_url();?>js/Cardcommission/cardcommission.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/treemenu/modernizr.custom.min.js" type="text/javascript"></script>  
	<script>
		$(function() {
			$('#dl-menu').dlmenu({
				animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
			});
		});
	</script>
</html>