<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cardcommission extends MX_Controller{
    public function __construct() {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Cardcommission/Cardcommissionmodel');	
    }
    //first basic hitting view
    public function index() {        
    	$moduleid = array(107);
    	sessionchecker($moduleid);
		/*action*/
		$data['actionassign'] = $this->Basefunctions->actionassign($moduleid); 
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Cardcommission/cardcommissionview',$data);
	}
	//create storage
	public function newdatacreate() {  
    	$this->Cardcommissionmodel->newdatacreatemodel();
    }
	//information fetchstorage
	public function fetchformdataeditdetails() {
		$moduleid = array(107);
		$this->Cardcommissionmodel->informationfetchmodel($moduleid);
	}
	//update storage
    public function datainformationupdate() {
        $this->Cardcommissionmodel->datainformationupdatemodel();
    }
	//delete storage
    public function deleteinformationdata() {
    	$moduleid = array(107);
		$this->Cardcommissionmodel->deleteoldinformation($moduleid);
    }
	//load accountname with/without assigned
	public function loadaccountname() {
		$this->Cardcommissionmodel->loadaccountname();
	}
}