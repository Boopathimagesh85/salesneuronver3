<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cardcommissionmodel extends CI_Model{
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//create counter
	public function newdatacreatemodel() {
		$industryid = $this->Basefunctions->industryid;
		//table and fields information	
		$formfieldsname = explode(',',$_POST['cardcommissionelementsname']);
		$formfieldstable = explode(',',$_POST['cardcommissionelementstable']);
		$formfieldscolmname = explode(',',$_POST['cardcommissionelementscolmn']);
		$elementpartable = explode(',',$_POST['cardcommissionelementspartabname']);
		//filter unique parent table
	   	$partablename =  $this->Crudmodel->filtervalue($elementpartable);
	   	//filter unique fields table
	   	$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
	   	$tableinfo = explode(',',$fildstable);
	   	$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
		$primaryid = $this->db->insert_id();
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity = ''.$user.' created CardCommission - '.$_POST['cardcommissionname'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,107);
		echo 'TRUE';
	}
	//retrive counter data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['cardcommissionelementsname']);
		$formfieldstable = explode(',',$_GET['cardcommissionelementstable']);
		$formfieldscolmname = explode(',',$_GET['cardcommissionelementscolmn']);
		$elementpartable = explode(',',$_GET['cardcommissionelementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['cardcommissionprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	//update counter
	public function datainformationupdatemodel() {
		$industryid = $this->Basefunctions->industryid;
		//table and fields information
		$formfieldsname = explode(',',$_POST['cardcommissionelementsname']);
		$formfieldstable = explode(',',$_POST['cardcommissionelementstable']);
		$formfieldscolmname = explode(',',$_POST['cardcommissionelementscolmn']);
		$elementpartable = explode(',',$_POST['cardcommissionelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['cardcommissionprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity = ''.$user.' updated CardCommission - '.$_POST['cardcommissionname'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$activity ,$userid,107);
		echo $result;
	}
	//delete counter
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['cardcommissionelementstable']);
		$parenttable = explode(',',$_GET['cardcommissionparenttable']);
		$id = $_GET['cardcommissionprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$cardcommissionname = $this->Basefunctions->singlefieldfetch('cardcommissionname','cardcommissionid','cardcommission',$id);
		$activity = ''.$user.' deleted CardCommission - '.$cardcommissionname.'';
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,107);
				echo "TRUE";
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,107);
			echo "TRUE";
		}
	}
	// load accountname without/with assigned
	public function loadaccountname() {
		$result=array();
		$tablename = $_POST['table'];
		if($_POST['datastatus'] == 1) {
			$data = $this->db->query("SELECT account.accountid, account.accountname FROM `account` LEFT JOIN ".$tablename." ON ".$tablename.".accountid=account.accountid and ".$tablename.".status = 1 WHERE ".$tablename.".`accountid` is null and account.status = 1 and account.accounttypeid = 18 GROUP BY account.accountid");
		}else if($_POST['datastatus'] == 0) {
			$data = $this->db->query("SELECT account.accountid, account.accountname FROM `account` JOIN ".$tablename." ON ".$tablename.".accountid=account.accountid and ".$tablename.".status = 1 WHERE ".$tablename.".status = 1 and account.accounttypeid = 18 GROUP BY account.accountid");
		}
		$i=0;
		if($data->num_rows() > 0){
			foreach($data->result() as $user_name)
			{
				$result[]=array(
						'accountid' => $user_name->accountid,
						'accountname' => $user_name->accountname,
				);
				$i++;
			}
		}else{
			$result='';
		}
		echo json_encode($result);
	}
}