<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sizemaster extends MX_Controller {
    public function __construct(){
        parent::__construct();
		$this->load->helper('formbuild'); 
		$this->load->model('Sizemaster_model');
		$this->load->model('Base/Basefunctions');
    }  
    public function index() {
		$moduleid = array(134);
		sessionchecker($moduleid);
		//action		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['account'] = $this->Basefunctions->accountgroup_dd('16');
		$data['filedmodids'] = $moduleid;
		//form
		$data['category']=$this->Basefunctions->simpledropdown('category','categoryid,categoryname','categoryname');
		$data['product'] = $this->Sizemaster_model->product_dd();
		$this->load->view('sizemastercreationview',$data);
	}
	/* Fetch Main grid header,data,footer information */
	public function gridinformationfetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$categoryid = $_GET['categoryid'];
		$productid = $_GET['productid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'product.productid') : 'product.productid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colid'=>array('1','1'),'colname'=>array('Category','Product'),'colicon'=>array('',''),'colsize'=>array('100','100'),'colmodelname'=>array('innercategory','innerproduct'),'colmodelindex'=>array('category.categoryname','product.productname'),'coltablename'=>array('product','product'),'uitype'=>array('2','2'),'modname'=>array('Category','Product'));
	
		$result=$this->Sizemaster_model->viewdynamicdatainfofetch($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$categoryid,$productid);
		$device = $this->Basefunctions->deviceinfo();
	//	if($device=='phone') {
		//	$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'sizemasterinner',$width,$height);
		//} else {
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'sizemasterinner',$width,$height,'true');
		//}
		echo json_encode($datas);
	}
	//sizemaster insert
	public function sizemastercreate() {
		$this->Sizemaster_model->sizemastercreatemodel();
	}
	//size master delete
	public function sizemasterdelete() {
		$this->Sizemaster_model->sizemasterdeletemodel();
	}
	//sizemaster retrive
	public function sizemasterretrive() {
		$this->Sizemaster_model->sizemasterretrivemodel();
	}
	//size master update
	public function sizemasterupdate() {
		$this->Sizemaster_model->sizemasterupdatemodel();
	}
	//unique name restriction
	public function uniqueshortdynamicviewnamecheck(){
		$this->Sizemaster_model->uniqueshortdynamicviewnamecheckmodel();
	}
}