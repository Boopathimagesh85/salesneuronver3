<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sizemaster_model extends CI_Model {
    private $lotmodule = 134;
    public function __construct() {
        parent::__construct();
		$this->load->model( 'Base/Basefunctions');
		$this->load->model( 'Base/Crudmodel');
    }
    //size master inner grid work
    public function viewdynamicdatainfofetch($tablename,$sortcol,$sortord,$pagenum,$rowscount,$categoryid,$productid) {
    	$dataset ='product.productid,category.categoryname,product.productname';
    	$join =' LEFT JOIN category ON category.categoryid=product.categoryid';
    	$status=$tablename.'.status='.$this->Basefunctions->activestatus.' and product.size="Yes"';
    	$extracond = '';
    	if($categoryid != '') {
    		$extracond .= ' AND product.categoryid='.$categoryid.'';
    	}
    	if($productid != '') {
    		$extracond .= ' AND product.productid='.$productid.'';
    	}
    	/* pagination */
    	$pageno = $pagenum-1;
    	$start = $pageno * $rowscount;
    	/* query */
    	$result = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '. $status.$extracond.' ORDER BY '.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
    	$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
    	$data=array();
    	$i=1;
    	foreach($result->result() as $row) {
    		$protid =$row->productid;
    		$categoryname =$row->categoryname;
    		$productname =$row->productname;
    		$data[$i]=array('id'=>$protid,$categoryname,$productname);
    		$i++;
    	}
    	$finalresult=array('0'=>$data,'1'=>$page,'2'=>'json');
    	return $finalresult;
    }
    //size master insert
    public function sizemastercreatemodel() {
    	$sizename = $_POST['sizename'];
    	$description = $_POST['description'];
    	$categoryid = $_POST['categoryid'];
    	$productid = $_POST['productid'];
    	$product = explode(',',$productid);
    	for($i=0;$i<count($product);$i++){
    		$check = $this->sizenamecombocheck($product[$i],$sizename);
    		if($check == 1) {
    			$sizemaster = array(
    				'sizemastername'=>$sizename,
    				'sizedescription'=>$description,
    				'categoryid'=>$this->Basefunctions->generalinformaion('product','categoryid','productid',$product[$i]),
    				'productid'=>$product[$i],
    				'industryid'=>$this->Basefunctions->industryid,
    				'branchid'=>$this->Basefunctions->branchid
    			);
    			$insertarray = array_merge($sizemaster,$this->Crudmodel->defaultvalueget());
    			$this->db->insert('sizemaster',array_filter($insertarray));
    		}
    	}
		/* //audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' created Size';
		$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,134); */
    	echo 'Success';
    }
    //combo check
    public function sizenamecombocheck($productid,$sizename) {
    	$data = 1;
    	$this->db->select('sizemasterid');
    	$this->db->from('sizemaster');
    	$this->db->where('sizemaster.productid',$productid);
    	$this->db->where('sizemaster.sizemastername',$sizename);
    	$this->db->where('sizemaster.status',1);
    	$result = $this->db->get();
    	if($result->num_rows() > 0) {
    		foreach($result->result() as $row) {
    			$data = $row->sizemasterid;
    		}
    	}
    	return $data;
    }
    //size master delete
    public function sizemasterdeletemodel() {
    	$primaryid = $_POST['primaryid'];
    	$delete = $this->Crudmodel->deletedefaultvalueget();
    	$this->db->where('sizemaster.sizemasterid',$primaryid);
    	$this->db->update('sizemaster',$delete);
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$sizename = $this->Basefunctions->singlefieldfetch('sizemastername','sizemasterid','sizemaster',$primaryid);
		$activity = ''.$user.' deleted Size - '.$sizename.'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Deleted',$activity ,$userid,134);
    	echo 'Success';
    }
    //size master data retrive
    public function sizemasterretrivemodel() {
    	$id = $_GET['primaryid'];
    	$this->db->select('productid,categoryid,sizemastername,sizedescription');
    	$this->db->from('sizemaster');
    	$this->db->where('sizemaster.sizemasterid',$id);
    	$result = $this->db->get();
    	if($result->num_rows() > 0) {
    		foreach($result->result() as $row) {
    			$data = array('productid'=>$row->productid,'categoryid'=>$row->categoryid,'name'=>$row->sizemastername,'description'=>$row->sizedescription);
    		}
    	} else {
    		$data = array('productid'=>'','categoryid'=>'','name'=>'','description'=>'');
    	}
    	echo json_encode($data);
    }
    //size master update
    public function sizemasterupdatemodel() {
    	$sizename = $_POST['editsizename'];
    	$description = $_POST['editdescription'];
    	$categoryid = $_POST['editcategoryid'];
    	$productid = $_POST['editproductid'];
    	$primaryid = $_POST['editsizemasterid'];
    	$sizemaster = array(
    		'sizemastername'=>$sizename,
    		'sizedescription'=>$description,
    		'categoryid'=>$categoryid,
    		'productid'=>$productid,
    		'industryid'=>$this->Basefunctions->industryid,
    		'branchid'=>$this->Basefunctions->branchid
    	);
    	$updaterray = array_merge($sizemaster,$this->Crudmodel->updatedefaultvalueget());
    	$this->db->where('sizemaster.sizemasterid',$primaryid);
    	$this->db->update('sizemaster',$updaterray);
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' updated Size - '.$sizename.'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$activity ,$userid,134);
    	echo 'Success';
    }
    //unique short name check
    public function uniqueshortdynamicviewnamecheckmodel(){
    	$industryid = $this->Basefunctions->industryid;
    	$primaryid=$_POST['primaryid'];
    	$accname=$_POST['accname'];
    	$productid = $_POST['productid'];
    	$elementpartable = explode(',',$_POST['elementspartabname']);
    	$partable =  $this->Basefunctions->filtervalue($elementpartable);
    	$ptid = $partable.'id';
    	$ptname = $_POST['fieldname'];
    	if($accname != "") {
    		$result = $this->db->select($ptname.','.$ptid)->from($partable)->where($partable.'.'.$ptname,$accname)->where($partable.'.status',1)->where($partable.'.productid',$productid)->where("FIND_IN_SET('".$industryid."',industryid) >", 0)->get();
    		if($result->num_rows() > 0) {
    			foreach($result->result()as $row) {
    				echo $row->$ptid;
    			}
    		} else {
    			echo "False"; 
    		}
    	} else {
    		echo "False";
    	}
    }
	// get size option products only
	public function product_dd()
	{
		$industryid = $this->Basefunctions->industryid;
		$info =$this->db->query("SELECT `product`.`productname`, `product`.`productid`, `product`.`categoryid`,`product`.`weightcalculationtypeid`, `category`.`categoryid`, `category`.`categoryname`,`category`.`taxmasterid` as categorytaxid,`category`.`categorylevel`, `product`.`counterid` as counterid, `product`.`bullionapplicable`, `product`.`taxable`, group_concat(`tax`.`taxname`, '-',`tax`.`taxid`) as taxid,group_concat(distinct(`tax`.`taxid`)) as taxdataid,`taxmaster`.`taxmasterid` as taxmasterid, `product`.`purityid`,`product`.`chargeid`,`product`.`purchasechargeid`,`product`.`stoneapplicable`,`product`.`size` FROM `product` JOIN `category` ON `category`.`categoryid` = `product`.`categoryid` JOIN `taxmaster` ON `taxmaster`.`taxmasterid` = `product`.`taxmasterid` LEFT JOIN `tax` ON `tax`.`taxmasterid` = `taxmaster`.`taxmasterid` JOIN `charge` ON `charge`.`chargeid` = `product`.`chargeid` JOIN `purchasecharge` ON `purchasecharge`.`purchasechargeid` = `product`.`purchasechargeid` WHERE `product`.`status` = 1 and `product`.`productid` not in(2,3) and `product`.`bullionapplicable`= 'No' and `product`.`size`= 'Yes' and `product`.`industryid` = ".$industryid." GROUP BY `product`.`productid` ORDER BY `product`.`productid` DESC");
		return $info;
	}
}