<!DOCTYPE html>
<html lang="en">
	<head>
		<?php $this->load->view('Base/headerfiles'); ?>
		<?php $this->load->view('Base/overlay'); ?>
		<?php 
			$this->load->view('Base/basedeleteform');	
		    $this->load->view('Base/modulelist');
		 ?>	
	</head>
	<body>
		<div class="row" style="max-width:100%">
			<div class="">
				<div class="inner-wrap">
					<!-- Add Form -->
					<?php $this->load->view('sizemastercreationform'); ?>
				</div>
			</div>
		</div>
		<?php
			$this->load->view('Base/basedeleteformforcombainedmodules');
		?>
	</body>
	<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js Files -->
	<script src="<?php echo base_url();?>js/Sizemaster/sizemaster.js" type="text/javascript"></script> 
	<script src="<?php echo base_url();?>js/plugins/treemenu/modernizr.custom.min.js" type="text/javascript"></script>  
	<script>
		$(function() {
			$('#dl-menu').dlmenu({
				animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
			});
		});
	</script>
</html>