<style type="text/css">
	#sizemasterinnergrid .gridcontent{
		height: 75vh !important;
	}
</style><div id="sizemastercreationview" class="gridviewdivforsh">
	<?php
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['gridid'] = 'sizemastergrid';
		$dataset['gridwidth'] = 'sizemastergridwidth';
		$dataset['gridfooter'] = 'sizemastergridfooter';
		$dataset['viewtype'] = 'disable';
		$dataset['moduleid'] = '134';
		$this->load->view('Base/singlemainviewformwithgrid.php',$dataset);
		$defaultrecordview = $this->Basefunctions->get_company_settings('mainviewdefaultview');
		echo hidden('mainviewdefaultview',$defaultrecordview);
	?>
</div>
</div>
</div>
</div>
<div id="sizemasterformadd" class="hidedisplay" style="">
	<div class="large-12 columns paddingzero formheader">
		<?php
		$device = $this->Basefunctions->deviceinfo();
			if($device=='phone') {
				echo '<div class="large-12 columns headercaptionstyle headerradius addformreloadtablet paddingzero">
					<div class="large-6 medium-6 small-6 columns headercaptionleft">
						<span class="gridcaptionpos"><span>'.$gridtitle['title'].'</span></span>
					</div>
					<div class="large-5 medium-6 small-6 columns addformheadericonstyle addformaction righttext">
					<span class="icon-box" id="sizemasterclose" ><i class="material-icons" title="Close">close</i></span>
						
					</div>';
			echo '</div>';
		} else {
			echo '<div class="large-12 columns headercaptionstyle headerradius addformreloadtablet paddingzero">
					<div class="large-6 medium-6 small-6 columns headercaptionleft">
						<span class="gridcaptionpos"><span>'.$gridtitle['title'].'</span></span>
					</div>
					<div class="large-6 medium-6 small-6 columns addformheadericonstyle addformaction righttext">
					<span id="sizemastersubmit" class="addbtnclass icon-box"><input id="" name="" tabindex="105" value="Save" class="alertbtnyes  ffield" type="button"></span>	 
						<span id="sizemasterclose" class="icon-box"><input id="" name="" tabindex="186" value="Close" class="alertbtnno  ffield" type="button"></span>
					</div>';
			echo '</div>';
		}
		?>
		<div class='large-12 columns addformunderheader'>&nbsp;</div>
		<!-- tab group creation -->
		<?php  if($device!='phone') {?>
			<div class="large-12 columns tabgroupstyle desktoptabgroup">
				<ul class="tabs" data-tab="">
					<li id="tab1" class="tab-title sidebaricons active ftabnew" data-subform="1">
						<span class="waves-effect waves-ripple waves-light">Size Master</span>
					</li>
				</ul>
			</div>
		<?php } else {?>
			<div class="large-12 columns tabgroupstyle mobiletabgroup">
				<ul class="tabs" data-tab="">
					<li id="tab1"  class="tab-title sidebaricons active ftabnew" data-subform="1">
						<span  class="waves-effect waves-ripple waves-light">Size Master</span>
					</li>
				</ul>
			</div>
		<?php }?>
	</div>
	<div class="large-12 columns addformunderheader">&nbsp; </div>
	<div class="large-12 columns addformcontainer padding-space-open-for-form" style="">
		<div class="row mblhidedisplay">&nbsp;</div>
		<form method="POST" name="sizemasterform" class=""  id="sizemasterform">
			<div id="addsizemastervalidate" class="validationEngineContainer" >
				<div id="subformspan1" class="hiddensubform"> 
				<?php
	if($device=='phone') 
	{
		?>	<div class="large-4 columns paddingbtm" style="padding:0px !important;">	
						<div class="large-12 columns cleardataform borderstyle" style="box-shadow:none;background:#f2f3fa !important;padding:0px !important;border: none;">
							<div class="large-12 columns headerformcaptionstyle" style="background:#f2f3fa !important;">Size Master Details</div>
					
	<?php } else { ?>
	<div class="large-4 columns paddingbtm" style="padding-right: 1.8rem;padding-bottom:1.5rem !important;">	
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
							<div class="large-12 columns headerformcaptionstyle" >Size Master Details</div>
		
	<?php }?>
							<div class="static-field large-12 columns">
								<label>Category</label>						
								<select id="categoryid" name="categoryid" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="101">
									<option value=""></option>
									 <?php $prev = ' ';
										foreach($category as $key):
									?> 
									<option data-name="<?php echo $key->categoryname;?>" value="<?php echo $key->categoryid;?>"><?php echo $key->categoryname;?></option>
									<?php  endforeach;?>						
								</select>
							</div>
							<div class="static-field large-12 columns ">
								<label>Product</label>						
								<select id="productid" name="productid" class="chzn-select dropdownchange" data-prompt-position="topLeft:14,36" tabindex="102">
								<option value=""></option> 
								<?php $prev = ' ';
								foreach($product->result() as $key):
								$cur = $key->categoryid;
								$a="";
								if($prev != $cur )
								{
									if($prev != " ")
									{
										 echo '</optgroup>';
									}
									echo '<optgroup  label="'.$key->categoryname.'">';
									$prev = $key->categoryid;
								}
								?>
								<option value="<?php echo $key->productid;?>"><?php echo $key->productname;?></option>
								<?php endforeach;?>
								</select>
							</div>
							<div class="input-field large-12 columns ">
								<input type="text" class="validate[required,maxSize[50]]" id="sizename" name="sizename" value="" tabindex="103">
								<label for="sizename">Size Name<span class="mandatoryfildclass">*</span></label>
							</div><div class="input-field large-12 columns">
								<textarea name="description" class="materialize-textarea validate[maxSize[200]]" id="description" rows="3" tabindex="104" cols="40"></textarea>
								<label for="description">Description</label>
							</div>
							<div class="divider large-12 columns"></div>
							<div class="large-2">
							</div>
								<?php
	if($device=='phone') 
	{
		?>	
							<div class="large-5 columns sectionalertbuttonarea" style="text-align:center;">
								<input id="submitsizeadd" type="button" class="alertbtnyes" name="" value="Save" tabindex="" style="margin-right: 7px;">	
							</div>  
							<div class="large-4 columns sectionalertbuttonarea" style="text-align:center;">
								<input id="resetfilter" type="button" class="alertbtnyes" name="" value="Reset" tabindex="" style="margin-right: 7px;">	
							</div>  
							
	<?php } else { ?>
	
							<div class="large-5 columns sectionalertbuttonarea">
								<input id="submitsizeadd" type="button" class="alertbtnyes" name="" value="Save" tabindex="" style="margin-right: 7px;">	
							</div>  
							<div class="large-4 columns sectionalertbuttonarea">
								<input id="resetfilter" type="button" class="alertbtnyes" name="" value="Reset" tabindex="" style="margin-right: 7px;">	
							</div>  
	
	<?php } ?>
						</div>
					</div>
					<div class="large-8 columns viewgridcolorstyle borderstyle" style="padding-left:0;padding-right:0;top: 20px;">	
						<?php
							$device = $this->Basefunctions->deviceinfo();
							if($device=='phone') {
								echo '<div class="large-12 columns paddingzero forgetinggridname" id="sizemasterinnergridwidth"><div id="sizemasterinnergrid" class="inner-row-content inner-gridcontent" style="height:81vh !important;">
									<!-- Table header content , data rows & pagination for mobile-->
								</div>
								<!--<footer class="inner-gridfooter footercontainer" id="sizemasterinnergridfooter">
									 Footer & Pagination content 
								</footer>--></div>';
							} else {
								echo '<div class="large-12 columns forgetinggridname" id="sizemasterinnergridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="sizemasterinnergrid" style="max-width:2000px; height:81vh !important;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
							<!--<div class="inner-gridfooter footer-content footercontainer" id="sizemasterinnergridfooter">
									 Footer & Pagination content 
								</div>--></div>';
							}
						?>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- Size Master Edit Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="sizemasteredit" name="sizemasteredit" style="overflow-y: scroll;overflow-x: hidden;z-index:40;">	
		<div class="large-10 medium-10 small-12 large-centered medium-centered small-centered columns">
			<div class="large-12 column paddingzero"  style="background:transparent">
				<div class="row sectiontoppadding">&nbsp;</div>
				<div class="large-4 columns paddingbtm end large-centered" id="sizemastereditsection">
					<form id="sizemastereditform" name="sizemastereditform" class="validationEngineContainer">
						<div class=" z-depth-5">
							<div class="large-12 columns paddingzero alert-panel" style="background-color:#617d8a">
								<div class="large-12 columns sectionheaderformcaptionstyle">Size Master</div>
									<div class="static-field large-12 columns">
									<label>Category</label>						
									<select id="editcategoryid" name="editcategoryid" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="101">
										<option value=""></option>
										 <?php $prev = ' ';
											foreach($category as $key):
										?> 
										<option data-name="<?php echo $key->categoryname;?>" value="<?php echo $key->categoryid;?>"><?php echo $key->categoryname;?></option>
										<?php  endforeach;?>						
									</select>
								</div>
								<div class="static-field large-12 columns ">
									<label>Product</label>						
									<select id="editproductid" name="editproductid" class="chzn-select dropdownchange" data-prompt-position="topLeft:14,36" tabindex="102">
									<option value=""></option> 
									<?php $prev = ' ';
									foreach($product->result() as $key):
									$cur = $key->categoryid;
									$a="";
									if($prev != $cur )
									{
										if($prev != " ")
										{
											 echo '</optgroup>';
										}
										echo '<optgroup  label="'.$key->categoryname.'">';
										$prev = $key->categoryid;
									}
									?>
									<option value="<?php echo $key->productid;?>"><?php echo $key->productname;?></option>
									<?php endforeach;?>
									</select>
								</div>
								<div class="input-field large-12 columns ">
									<input type="text" class="validate[required,funcCall[sizemasternamecheckonedit],maxSize[50]]" id="editsizename" name="editsizename" value="" tabindex="103">
									<label for="editsizename">Size Name<span class="mandatoryfildclass">*</span></label>
								</div><div class="input-field large-12 columns">
									<textarea name="editdescription" class="materialize-textarea validate[maxSize[200]]"  id="editdescription" rows="3" tabindex="104" cols="40"></textarea>
									<label for="editdescription">Description</label>
								</div>
								<input type="hidden" id="editsizemasterid" name="editsizemasterid" value="" />
								<div class="divider large-12 columns"></div>
								<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">
									<span id="sizemasterupdate" class="addbtnclass icon-box"><input id="" name="sizemasterupdate" tabindex="105" value="Save" class="alertbtnyes  ffield" type="button"></span>
									<span id="sizemastercancel" class="addbtnclass icon-box"><input id="" name="sizemastercancel" tabindex="106" value="Cancel" class="alertbtnno  ffield" type="button"></span>				
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>