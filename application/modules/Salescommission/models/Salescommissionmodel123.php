<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Salescommissionmodel extends CI_Model{
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//create counter
	public function newdatacreatemodel() {
		$industryid = $this->Basefunctions->industryid;
		//table and fields information	
		$formfieldsname = explode(',',$_POST['salescommissionelementsname']);
		$formfieldstable = explode(',',$_POST['salescommissionelementstable']);
		$formfieldscolmname = explode(',',$_POST['salescommissionelementscolmn']);
		$elementpartable = explode(',',$_POST['salescommissionelementspartabname']);
		//filter unique parent table
	   	$partablename =  $this->Crudmodel->filtervalue($elementpartable);
	   	//filter unique fields table
	   	$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
	   	$tableinfo = explode(',',$fildstable);
	   	$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
		$primaryid = $this->db->insert_id();
		echo 'TRUE';
	}
	//retrive counter data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['salescommissionelementsname']);
		$formfieldstable = explode(',',$_GET['salescommissionelementstable']);
		$formfieldscolmname = explode(',',$_GET['salescommissionelementscolmn']);
		$elementpartable = explode(',',$_GET['salescommissionelementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['salescommissionprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	//update counter
	public function datainformationupdatemodel() {
		$industryid = $this->Basefunctions->industryid;
		//table and fields information
		$formfieldsname = explode(',',$_POST['salescommissionelementsname']);
		$formfieldstable = explode(',',$_POST['salescommissionelementstable']);
		$formfieldscolmname = explode(',',$_POST['salescommissionelementscolmn']);
		$elementpartable = explode(',',$_POST['salescommissionelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['salescommissionprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		echo $result;
	}
	//delete counter
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['salescommissionelementstable']);
		$parenttable = explode(',',$_GET['salescommissionparenttable']);
		$id = $_GET['salescommissionprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				echo "TRUE";
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			echo "TRUE";
		}
	}
	/* *Retrieves the product records for product search grid-	 */
	public function getproductsearch($tablename,$sortcol,$sortord,$pagenum,$rowscount,$ordertype) {
		$dataset = 'product.productid,category.categoryname,product.productname';
		$join =' LEFT OUTER JOIN category ON category.categoryid=product.categoryid';
		$status = $tablename.'.status NOT IN (0,3)';
		if($ordertype){
			$order = 'product.counterid ='.$ordertype;
		} else {
			$order = '1=1 AND product.productid NOT IN (2,3)';
		}
		/* pagination */
		$query = 'select '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$status.' AND '.$order.' ORDER BY'.' '.$sortcol.' '.$sortord;
		$page = $this->Basefunctions->generatepage($query,$pagenum,$rowscount);
		/* query */
		$data = $this->db->query('select '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$status.' AND '.$order.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$page['start'].','.$page['records'].'');
		$finalresult=array($data,$page,'');	
		return $finalresult;
	}
	public function parentstoragenamefetch() {
		$id=$_GET['id'];
		$data=$this->db->select('counter.counterid,counter.countername')
		->from('salescommission')
		->join('counter','counter.counterid=salescommission.counterid')
		->where('salescommission.salescommissionid',$id)
		->where('counter.status',1)
		->get();
		if($data->num_rows() >0) {
			foreach($data->result() as $in)	{
				$parentcounterid=$in->counterid;
				$countername=$in->countername;
			}
			$a = array('parentcounterid'=>$parentcounterid,'countername'=>$countername);
		} else {
			$a = array('parentcounterid'=>'0','countername'=>'');
		}
		echo json_encode($a);
	}
}