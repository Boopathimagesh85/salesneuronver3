<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Salescommission extends MX_Controller{
    public function __construct() {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Salescommission/Salescommissionmodel');	
    }
    //first basic hitting view
    public function index() {        
    	$moduleid = array(108);
    	sessionchecker($moduleid);
		/*action*/
		$data['actionassign'] = $this->Basefunctions->actionassign($moduleid); 
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['counterstatus'] = $this->Basefunctions->get_company_settings('counter');
		$data['weight_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$this->load->view('Salescommission/salescommissionview',$data);
	}
	//create storage
	public function newdatacreate() {  
    	$this->Salescommissionmodel->newdatacreatemodel();
    }
	//information fetchstorage
	public function fetchformdataeditdetails() {
		$moduleid = 108;
		$this->Salescommissionmodel->informationfetchmodel($moduleid);
	}
	//update storage
    public function datainformationupdate() {
        $this->Salescommissionmodel->datainformationupdatemodel();
    }
	//delete storage
    public function deleteinformationdata() {
        $moduleid = 108;
		$this->Salescommissionmodel->deleteoldinformation($moduleid);
    }
    public function getproductsearch() {
    	$primarytable = $_GET['maintabinfo'];
    	$primaryid = $_GET['primaryid'];
    	$pagenum = $_GET['page'];
    	$rowscount = $_GET['records'];
    	$width = $_GET['width'];
    	$height = $_GET['height'];
    	$modid = $_GET['moduleid'];
    	$industryid = $_GET['industryid'];
    	if(isset($_GET['ordertype'])){
    		$ordertype = $_GET['ordertype'];
    	}else{
    		$ordertype = 1;
    	}
    	$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'product.productid') : 'product.productid';
    	$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'ASC') : 'ASC';
    	$colinfo = array('colmodelname'=>array('categoryname','productname'),'colmodelindex'=>array('category.categoryname','product.productname'),'coltablename'=>array('categoryname','productname'),'uitype'=>array('2','2'),'colname'=>array('Category','Product Name'),'colsize'=>array('200','200'));
    	$result=$this->Salescommissionmodel->getproductsearch($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$ordertype,$industryid);
    	$device = $this->Basefunctions->deviceinfo();
    //	if($device=='phone') {
    		//$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Product List',$width,$height);
    	//} else {
    		$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Product List',$width,$height);
    	//}
    	echo json_encode($datas);
    }
    //reload the storage list element
    public function parentstoragenamefetch() {
    	$this->Salescommissionmodel->parentstoragenamefetch();
    }
	//load username except salescommission assigned
	public function loadusername() {
		$this->Salescommissionmodel->loadusername();
	}
}