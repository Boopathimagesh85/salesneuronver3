<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rolesmodel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//fetch modules based on role
	public function fetchrolebasedmodulefetchmodel() {
		$roleid = $_GET['rid'];
		$datasets = array();
		$industryid = $this->Basefunctions->industryid;
		$i=0;
		$this->db->select('module.modulename,module.menuname,module.moduleid,menucategory.menucategoryname,menucategory.menucategoryid');
		$this->db->from('module');
		$this->db->join('moduleinfo','moduleinfo.moduleid=module.moduleid');
		$this->db->join('menucategory','menucategory.menucategoryid=module.menucategoryid');
		$this->db->where('moduleinfo.userroleid',$roleid);
		$this->db->where("FIND_IN_SET('247',module.moduleprivilegeid) >", 0);
		$this->db->where("FIND_IN_SET('$industryid',module.industryid) >", 0);
		$this->db->where('moduleinfo.status',1);
		$this->db->group_by('moduleinfo.moduleid');
		$this->db->order_by('menucategory.sortorder','ASC');
		$this->db->order_by('module.sortorder','ASC');
		$modresult = $this->db->get();
		foreach($modresult->result() as $data) {
			$name=(($data->menuname!="")?$data->menuname:$data->modulename);
			$datasets[$i] = array('datasid'=>$data->moduleid,'dataname'=>$name,'catename'=>$data->menucategoryname,'cateid'=>$data->menucategoryid);
			$i++;
		}
		echo json_encode($datasets);
	}
	//New user role create
	public function newdatacreatemodel() {
		ini_set('max_execution_time', 300);
		$parentroleid = 1;
		if( isset($_POST['parentroleid']) ) {
			$parentroleid = $_POST['parentroleid'];
			if($parentroleid == '') {
				$parentroleid = 1;
			}
		}
		$rolename = $_POST['rolename'];
		$roledescription = $_POST['roledescription'];
		$profileid = $_POST['profileid'];
		$date = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$status	= $this->Basefunctions->activestatus;
		$role = array(
			'userrolename'=>ucwords($rolename),
			'userparentrole'=>$parentroleid,
			'description'=>$roledescription,
			'profileid'=>$profileid,
			'industryid'=>$this->Basefunctions->industryid,
			'branchid'=>$this->Basefunctions->branchid,
			'createdate'=>$date,
			'lastupdatedate'=>$date,
			'createuserid'=>$userid,
			'lastupdateuserid'=>$userid,
			'status'=>$status
		);
		$this->db->insert('userrole',$role);
		$roleid = $this->db->insert_id();
		//assign profile information to new user role [module,fields,values]
		{
			//fetch module ids based on profileid
			$promoduleids = "";
			$this->db->select('profilemoduleinfo.profilemoduleinfoid,profilemoduleinfo.profileid,profilemoduleinfo.moduleid',false);
			$this->db->from('profilemoduleinfo');
			$this->db->where_not_in('profilemoduleinfo.status',array(3,0));
			$this->db->where_in('profilemoduleinfo.profileid',array($profileid));
			$result=$this->db->get();
			foreach($result->result() as $data) {
				$promoduleids = $data->moduleid;
			}
			$moduleids = explode(',',$promoduleids);
			//assign modules & toolbars and values to new role
			foreach($moduleids as $moduleid) {
				{//module-info insert
					$rolechk = $this->modulecheckinfo($moduleid,$roleid);
					if($rolechk=='FALSE' && $moduleid!= '1') {
						$moduleinfo = array('moduleid'=>$moduleid,'userroleid'=>$roleid,'createdate'=>$date,'lastupdatedate'=>$date,'createuserid'=>$userid,'lastupdateuserid'=>$userid,'status'=>$status);
						$this->db->insert('moduleinfo',$moduleinfo);
					} else {
						$moduleinfo = array('moduleid'=>$moduleid,'userroleid'=>$roleid,'lastupdatedate'=>$date,'lastupdateuserid'=>$userid,'status'=>$status);
						$this->db->where('moduleid',$moduleid);
						$this->db->where('userroleid',$roleid);
						$this->db->update('moduleinfo',$moduleinfo);
					}
					//user role tool-bar id insert
					$protoolbarid = $this->profiletollbaridget($moduleid,$profileid);
					$toolbarchk = $this->toolbarcheckinfo($moduleid,$roleid);
					if($toolbarchk == 'FALSE') {
						$usertoolbarsets = array('toolbarnameid'=>$protoolbarid,'userroleid'=>$roleid,'moduleid'=>$moduleid,'status'=>1);
						$this->db->insert('usertoolbarprivilege',$usertoolbarsets);
					} else {
						$usertoolbarsets = array('toolbarnameid'=>$protoolbarid,'userroleid'=>$roleid,'moduleid'=>$moduleid,'status'=>1);
						$this->db->where('moduleid',$moduleid);
						$this->db->where('userroleid',$roleid);
						$this->db->update('usertoolbarprivilege',$usertoolbarsets);
					}
				}
				{//module field and value inert to new user role - based on profile
					$modfieldinfos = $this->fetchprofilemodulefieldids($profileid,$moduleid);
					foreach($modfieldinfos as $modulefieldinfo) {
						$modulefieldid = $modulefieldinfo['filedid'];
						$fieldactvisible = $modulefieldinfo['fieldvisible'];
						$mode = explode(',',$fieldactvisible);
						$visible = $mode[0];
						$read = $mode[1];
						{//module field modes insertion/updation
							//update user role to module field table
							$chk = '';
							$chk = $this->modulefieldidcheck($roleid,$moduleid,$modulefieldid);
							if($chk == 'Fail') {
								//module field
								$this->db->query('update modulefield set userroleid = CONCAT(userroleid,",'.$roleid.'") where modulefield.modulefieldid='.$modulefieldid.' AND modulefield.moduletabid='.$moduleid.'');
							}
							$tabsecchk = '';
							$tabsecchk = $this->modulefieldidtabsecheck($roleid,$moduleid);
							if($tabsecchk == 'Fail') {
								//module tab section
								$this->db->query('update moduletabsection set userroleid = CONCAT(userroleid,",'.$roleid.'") where moduletabsection.moduleid='.$moduleid.'');
							}
							$tabgrpchk = '';
							$tabgrpchk = $this->modulefieldidtabgrpcheck($roleid,$moduleid);
							if($tabgrpchk == 'Fail') {
								//module tab group
								$this->db->query('update moduletabgroup set userroleid = CONCAT(userroleid,",'.$roleid.'") where moduletabgroup.moduleid='.$moduleid.'');
							}
							//create new restrictions
							$visiblevalues = array('modulefieldid'=>$modulefieldid,'moduleid'=>$moduleid,'userroleid'=>$roleid,'fieldactive'=>$visible,'fieldreadonly'=>$read,'createdate'=>$date,'lastupdatedate'=>$date,'createuserid'=>$userid,'lastupdateuserid'=>$userid,'status'=>$status);
							$this->db->insert('userrolemodulefiled',$visiblevalues);
						}
						{//field value insertion/updation
							$modfieldvalidinfos = $this->fetchprofilemodulevaluefieldid($profileid,$moduleid,$modulefieldid);
							$fieldvalueids = explode(',',$modfieldvalidinfos['valfieldid']);
							foreach($fieldvalueids as $fieldvalueid) {
								//fetch insert table info
								$fieldvalinfos = $this->fetchmodfiledtableinfo($modulefieldid);
								if(count($fieldvalinfos)>0) {
									$tableid = $fieldvalinfos['columnname'];
									$table = substr($tableid, 0, -2);
									$fieldname = $table.'name';
									$check = $this->userroleidcheckindropdown($table,$tableid,$fieldvalueid,$roleid,$moduleid);
									if($check == 'FALSE') {
										$this->db->query('update '.$table.' set userroleid = CONCAT(userroleid,",'.$roleid.'") where '.$table.'.'.$tableid.'='.$fieldvalueid.'');
									}
								}
							}
						}
					}
				}
			}
		}
		echo "TRUE";
	}
	//roles main grid view
	public function userrolegriddatafetchgridxmlview($sidx,$sord,$start,$limit,$wh) {
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('SQL_CALC_FOUND_ROWS  userrole.userroleid,ur.userrolename as urname,pr.userrolename as prname  ,profile.profilename,userrole.description,status.statusname',false);
		$this->db->from('userrole');
		$this->db->join('userrole as ur','ur.userroleid=userrole.userroleid');
		$this->db->join('userrole as pr','pr.userroleid=userrole.userparentrole');
		$this->db->join('profile','profile.profileid=userrole.profileid');
		$this->db->join('status','status.status=userrole.status');
		$this->db->where_not_in('userrole.status',array(3,0));
		$this->db->where("FIND_IN_SET('$industryid',userrole.industryid) >", 0);
		if($wh != "1") {
			$this->db->where($wh);
		}
		$this->db->order_by($sidx,$sord);
		$this->db->limit($limit,$start);
		$result=$this->db->get();
		return $result;
	}
	//fetch form edit details
	public function fetchformdataeditdetailsmodel() {
		$id = $_GET['dataprimaryid'];
		$this->db->select('userrole.userroleid,userrole.userparentrole,userrole.profileid,userrole.userrolename,userrole.description',false);
		$this->db->from('userrole');
		$this->db->where('userrole.userroleid',$id);
		$this->db->where_not_in('userrole.status',array(3,0));
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('userparentrole'=>$row->userparentrole,'userrolename'=>$row->userrolename,'description'=>$row->description,'profileid'=>$row->profileid);
			} 
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//role update function 
	public function dataupdatefunctionmodel() {
		if(isset($_POST['parentroleid'])) {
			$parentroleid = $_POST['parentroleid'];
			if($parentroleid == '') { $parentroleid = 1; }
		} else {
			$parentroleid = 1;
		}
		$rolename = $_POST['rolename'];
		$roledescription = $_POST['roledescription'];
		$profileid = $_POST['profileid'];
		$primaryid = $_POST['primaryid'];
		$cdate = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$status	= $this->Basefunctions->activestatus;
		$role = array(
			'userrolename'=>ucwords($rolename),
			'userparentrole'=>$parentroleid,
			'profileid'=>$profileid,
			'description'=>$roledescription,
			'createdate'=>$cdate,
			'lastupdatedate'=>$cdate,
			'createuserid'=>$userid,
			'lastupdateuserid'=>$userid,
			'status'=>$status
		);
		$this->db->where('userrole.userroleid',$primaryid);
		$this->db->update('userrole',$role);
		echo "TRUE";
	}
	//role delete function 
	public function roledeletefunctionmodel() {
		$cdate = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$primaryid = $_GET['primaryid'];
		$updateroleid = $_GET['updateroleid'];
		//update the employee to new role name
		$updatenewrole = array('userroleid'=>$updateroleid,'lastupdateuserid'=>$userid,'lastupdatedate'=>$cdate);
		$this->db->where_in('employee.userroleid',$primaryid);
		$this->db->update('employee',$updatenewrole);
		//update parent role
		$this->db->select('userparentrole',false);
		$this->db->from('userrole');
		$this->db->where('userrole.userroleid',$primaryid);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$parentrole =$row->userparentrole;
			}
		}
		$parentupdaterole = array('userparentrole'=>$parentrole,'lastupdateuserid'=>$userid,'lastupdatedate'=>$cdate);
		$this->db->where_in('userrole.userparentrole',$primaryid);
		$this->db->update('userrole',$parentupdaterole);
		//update the user role
		$updatestatus = array('status'=>0,'lastupdateuserid'=>$userid,'lastupdatedate'=>$cdate);
		$this->db->where('userrole.userroleid',$primaryid);
		$this->db->update('userrole',$updatestatus);
		echo "TRUE";
	}
	//roles main grid view
	public function rolebasedusernameshowgridxmlview($sidx,$sord,$start,$limit,$wh) {
		$userroleid = $_GET['userroleid'];
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('SQL_CALC_FOUND_ROWS  employee.employeeid,employee.employeename,employee.lastname,userrole.userrolename,status.statusname',false);
		$this->db->from('employee');
		$this->db->join('userrole','userrole.userroleid=employee.userroleid');
		$this->db->join('status','status.status=userrole.status');
		$this->db->where_not_in('employee.status',array(3,0));
		$this->db->where_in('employee.industryid',$industryid);
		if($wh != "1") {
			$this->db->where($wh);
		}
		if($userroleid != "") {
			$this->db->where('employee.userroleid',$userroleid);	
		}
		$this->db->order_by($sidx,$sord);
		$this->db->limit($limit,$start);
		$result=$this->db->get();
		return $result;  
	}
	//user role assign add
	public function userroleassignaddmodel() {
		$roleid = $_POST['roleid'];
		$empid = $_POST['multiuserid']; 
		$impempid = explode(',',$empid);
		$count = count($impempid); 
		$cdate = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$status	= $this->Basefunctions->activestatus;
		$emparray = array('userroleid'=>$roleid,'lastupdatedate'=>$cdate,'lastupdateuserid'=>$userid,'status'=>$status);
		for($i=0;$i<$count;$i++) {
			$this->db->where_in('employee.employeeid',$impempid[$i]);
			$this->db->update('employee',$emparray);
		}
		echo "TRUE"; 
	}
	//user role based drop down value get
	public function fetchdddatawithcondmodel() {
		$userroleid = $_GET['userroleid'];
		$i =0;
		$this->db->select('employee.employeeid,employee.lastname',false);
		$this->db->from('employee');
		$this->db->where('employee.userroleid',$userroleid);
		$this->db->where_not_in('employee.status',array(3,0));
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$data[$i]  = array('datasid'=>$row->employeeid,'dataname'=>$row->lastname);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	// on delete transfer dd data load
	public function deleteroleddvalfetchmodel() {
		$userroleid = $_GET['userroleid'];
		$i =0;
		$this->db->select('userrole.userroleid,userrole.userrolename',false);
		$this->db->from('userrole');
		$this->db->where_not_in('userrole.userroleid',$userroleid);
		$this->db->where_not_in('userrole.status',array(3,0));
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$data[$i]  = array('datasid'=>$row->userroleid,'dataname'=>$row->userrolename);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//Module data fetch
	public function rolebasedmodulelistmodel($roleid,$wh) {
		$profileid = $this->profileidget($roleid);
		$check = $this->checkroleidinmoduleinfo($roleid);
		$industryid = $this->Basefunctions->industryid;
		$moduleids = array();
		if($check == 'TRUE') {
			$this->db->select('SQL_CALC_FOUND_ROWS  moduleinfo.moduleinfoid,moduleinfo.userroleid,moduleinfo.moduleid',false);
			$this->db->from('moduleinfo');
			$this->db->join('module','module.moduleid=moduleinfo.moduleid');
			$this->db->join('menucategory','menucategory.menucategoryid=module.menucategoryid');
			$this->db->where_not_in('moduleinfo.status',array(3,2,0,10));
			$this->db->where('moduleinfo.userroleid',$roleid);
			$this->db->where("FIND_IN_SET('$industryid',module.industryid) >", 0);
			if($wh != "1") {
				$this->db->where($wh);
			}
			$result=$this->db->get();
			foreach($result->result() as $row) {
				$moduleids[] = $row->moduleid;
			}
			return $moduleids;
		} else {
			$this->db->select('SQL_CALC_FOUND_ROWS  profilemoduleinfo.profilemoduleinfoid,profilemoduleinfo.profileid,profilemoduleinfo.moduleid',false);
			$this->db->from('profilemoduleinfo');
			$this->db->join('module','module.moduleid=profilemoduleinfo.moduleid');
			$this->db->join('menucategory','menucategory.menucategoryid=module.menucategoryid');
			$this->db->where_not_in('profilemoduleinfo.status',array(3,2,0,10));
			$this->db->where_in('profilemoduleinfo.profileid',array($profileid));
			$this->db->where("FIND_IN_SET('$industryid',module.industryid) >", 0);
			if($wh != "1") {
				$this->db->where($wh);
			}
			$this->db->group_by('profilemoduleinfo.moduleid');
			$result=$this->db->get();
			return $result;
		}
	}
	//Dashboard Moduleid fetch based on userrole
	public function rolebaseddahsmodulelistmodel($roleid,$profileid) {
		$check = $this->checkroleidinmoduleinfo($roleid);
		$moduleids = array();
		if($check == 'TRUE') {
			$this->db->select('SQL_CALC_FOUND_ROWS  moduleinfo.moduleinfoid,moduleinfo.userroleid,moduleinfo.moduleid',false);
			$this->db->from('moduleinfo');
			$this->db->where_in('moduleinfo.status',array(1));
			$this->db->where('moduleinfo.userroleid',$roleid);
			$result=$this->db->get();
			foreach($result->result() as $row) {
				$moduleids[] = $row->moduleid;
			}
		} else {
			$this->db->select('SQL_CALC_FOUND_ROWS profilemoduleinfo.profilemoduleinfoid,profilemoduleinfo.moduleid',false);
			$this->db->from('profilemoduleinfo');
			$this->db->where_in('profilemoduleinfo.status',array(1));
			$this->db->where_in('profilemoduleinfo.profileid',array($profileid));
			$this->db->group_by('profilemoduleinfo.moduleid');
			$result=$this->db->get();
			foreach($result->result() as $row) {
				$moduleids[] = $row->moduleid;
			}
		}
		return $moduleids;
	}
	//profile id get function
	public function profileidget($roleid) {
		$this->db->select('profileid');
		$this->db->from('userrole');
		$this->db->where_in('userrole.userroleid',array($roleid));
		$this->db->where('userrole.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = $row->profileid;
			}
			return $data;
		}
	}
	//action name fetch
	public function actionnamefetch($toolbarids,$moduleids) {
		$action = array();
		$toolbarid = explode(',',$toolbarids);
		$count = count($toolbarid);
		$j=0;
		$add='';
		$edit='';
		$delete='';
		$view='';
		$custom=array();
		for($i=0;$i<$count;$i++) {
			$chk = $this->checkmoduleidintoolbar($toolbarid[$i],$moduleids);
			if($chk == 'true') {
				if($toolbarid[$i] == 2) {
					$add = $toolbarid[$i];
				} else if($toolbarid[$i] == 3) {
					$edit = $toolbarid[$i];
				} else if($toolbarid[$i] == 4) {
					$delete = $toolbarid[$i];
				} else if($toolbarid[$i] == 62) {
					$view = $toolbarid[$i];
				} else {
					$custom[$j] = $toolbarid[$i];
					$j++;
				}
			}
		}
		$cusimp = implode(',',$custom);
		$action = array($add,$view,$edit,$delete,$cusimp);
		return $action;
	}
	//check moduleid exit in toolbar
	public function checkmoduleidintoolbar($toolbarid,$moduleids) {
		$chk = "false";
		$modids=array_map('intval',array(1));
		if(is_array($moduleids)) {
			$moduleids[]='1';
			$modids = array_map('intval',$moduleids);
		} else {
			$modids = array_map('intval',explode(',',$moduleids));
		}
		$this->db->select("toolbarname.toolbarnameid",false);
		$this->db->from("toolbarname");
		$this->db->join("module","module.moduleid=toolbarname.moduleid");
		$this->db->where("toolbarname.toolbarnameid",$toolbarid);
		$this->db->where_in("toolbarname.moduleid",($modids));
		$this->db->where("toolbarname.status",1);
		$this->db->where_in("module.status",array(1,3));
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			$chk = "true";
		}
		return $chk;
	}
	//role id check
	public function checkroleidinmoduleinfo($roleid) {
		$data = 'FALSE';
		if($roleid != "") {
			$this->db->select('userroleid');
			$this->db->from('moduleinfo');
			$this->db->where('moduleinfo.userroleid',$roleid);
			$this->db->where('moduleinfo.status',1);
			$result = $this->db->get();
			if($result->num_rows() > 0) {
				$data = 'TRUE';
			} else {
				$data = 'FALSE';
			}
		}
		return $data;
	}
	//tool bar id get - profile based
	public function profiletollbaridget($modid,$profileid) {
		$data = "";
		$this->db->select('toolbarnameid');
		$this->db->from('profilemoduleaction');
		$this->db->where("profilemoduleaction.moduleid",$modid);
		$this->db->where('profilemoduleaction.profileid',$profileid);
		$this->db->where('profilemoduleaction.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data =$row->toolbarnameid;
			}
		}
		return $data;
	}
	//module field id get - profile based
	public function fetchprofilemodulefieldids($profileid,$moduleid) {
		$dataset = array();
		$i=0;
		$this->db->select('profilemodulefields.profilemodulefieldsid,profilemodulefields.modulefieldid,profilemodulefields.fieldvisibility',false);
		$this->db->from('profilemodulefields');
		$this->db->join('modulefield','modulefield.modulefieldid=profilemodulefields.modulefieldid');
		$this->db->where_in('profilemodulefields.moduleid',$moduleid);
		$this->db->where_in('profilemodulefields.profileid',$profileid);
		$this->db->where_not_in('profilemodulefields.status',array(3,0));
		$result=$this->db->get();
		foreach($result->result() as $data) {
			$dataset[$i] = array('filedid'=>$data->modulefieldid,'fieldvisible'=>$data->fieldvisibility);
			$i++;
		}
		return $dataset;
	}
	//module value field id info - profile based
	public function fetchprofilemodulevaluefieldid($profileid,$moduleid,$modulefieldid) {
		$dataset = array('valfieldid'=>1);
		$this->db->select('profilemodulevalues.profilemodulevaluesid,profilemodulevalues.modulefieldid,profilemodulevalues.fieldvaluesid',false);
		$this->db->from('profilemodulevalues');
		$this->db->join('modulefield','modulefield.modulefieldid=profilemodulevalues.modulefieldid');
		$this->db->where_in('profilemodulevalues.modulefieldid',$modulefieldid);
		$this->db->where_in('profilemodulevalues.moduleid',$moduleid);
		$this->db->where_in('profilemodulevalues.profileid',$profileid);
		$this->db->where_not_in('profilemodulevalues.status',array(3,0));
		$result=$this->db->get();
		foreach($result->result() as $data) {
			$dataset = array('valfieldid'=>$data->fieldvaluesid);
		}
		return $dataset;
	}
	//fetch data field column information
	public function fetchmodfiledtableinfo($modfieldid) {
		$data = array();
		$this->db->select('modulefield.modulefieldid,modulefield.columnname,modulefield.fieldlabel');
		$this->db->from('modulefield');
		$this->db->where_in('modulefield.uitypeid',array(17,18));
		$this->db->where('modulefield.modulefieldid',$modfieldid);
		$this->db->where('modulefield.status',1);
		$result = $this->db->get();
		foreach($result->result() as $row) {
			$data = array('columnname'=>$row->columnname);
		}
		return $data;	
	}
	//tool bar id get based on roles
	public function tollbaridgetbasedonroles($modid,$roleid) {
		$data = "";
		$this->db->select('toolbarnameid');
		$this->db->from('usertoolbarprivilege');
		$this->db->where("usertoolbarprivilege.userroleid",$roleid);
		$this->db->where("usertoolbarprivilege.moduleid",$modid);
		$this->db->where('usertoolbarprivilege.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data=$row->toolbarnameid;
			}
		}
		return $data;
	}
	//tool bar id get
	public function moduletollbaridget($moduleid) {
		$data="";
		$this->db->select('toolbarnameid');
		$this->db->from('toolbargroup');
		$this->db->where("toolbargroup.moduleid",$moduleid);
		$this->db->where('toolbargroup.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data =$row->toolbarnameid;
			}
		}
		return $data;
	}
	//module name get
	public function modulenameget($moduleid){
		$data=array('modname'=>'','catname'=>'');
		$this->db->select('modulename,menuname,menucategory.menucategoryname');
		$this->db->from('module');
		$this->db->join('menucategory','menucategory.menucategoryid=module.menucategoryid');
		$this->db->where_in("module.moduleid",$moduleid);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('modname'=>(($row->menuname == "")? $row->modulename : $row->menuname),'catname'=>$row->menucategoryname);
			}
		}
		return $data;
	}
	//check child module
	public function checkdashboardid($moduleid) {
		$modid = '';
		if($moduleid!='') {
			$result = $this->db->query('select moduledashboardid from module where module.moduleid='.$moduleid.' AND module.moduledashboardid != "" AND module.status NOT IN (0,2,3)',false);
			if($result->num_rows() > 0) {
				foreach($result->result() as $row) {
					$modid = (($row->moduledashboardid != 1)? '*' : '');
				}
			}
		}
		return $modid;
	}
	//module based action value fetch
	public function modulebasedactionvalue($useraction,$moduleaction) {
		$count = count($useraction);
		for($i=0;$i<$count;$i++) {
			if($moduleaction[$i] != '') {
				if($useraction[$i] == $moduleaction[$i]) {
					$value[$i] = 'true';
				} else {
					$value[$i] = 'false';
				}
			} else {
				$value[$i] = 'empty';
			}
		}
		return $value;
	}
	//role based module create / update
	public function rolemodulesubmitmodel() {
		$roleid=$_POST['roleid'];
		$rowmoduleid=$_POST['allmoduleid'];
		$allmoduleid = explode(',',$rowmoduleid);
		$griddata=$_POST['modulegriddata'];
		$formdata=json_decode( $griddata, true );
		$count = count($formdata);
		$status = $this->Basefunctions->activestatus;
		$date = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		for( $i=0; $i <$count ; $i++ ) {
			$moduleid = $allmoduleid[$i];
			$checkmodule = $formdata[$i]['moduleid'];
			$create = $formdata[$i]['create'];
			$read = $formdata[$i]['read'];
			$update = $formdata[$i]['update'];
			$delete = $formdata[$i]['delete'];
			$custom = $formdata[$i]['custom'];
			$customtoolbar = $formdata[$i]['customactions'];
			$submod = $formdata[$i]['submodules'];
			$submoddata = $formdata[$i]['submodulesdata'];
			$custoolbarid = "";
			if($customtoolbar != "") {
				$toolbaridsets = explode('||',$customtoolbar);
				$custoolbarid = $toolbaridsets[0];
			}
			$this->createupdatemoduletoolbar($moduleid,$roleid,$create,$read,$update,$delete,$custom,$custoolbarid,$checkmodule,$date,$userid,$status,$allmoduleid);
			$moduleid=1;$create='false';$read='false';$update='false';$delete='false';$custom='false';$custoolbarid=1;$checkmodule='1';
			if($submod == '*') {
				if($submod == '*' && $submoddata != "") {
					$subformdata=json_decode($submoddata,true);
					$subcount = count($subformdata);
					for( $j=0; $j <$subcount ; $j++ ) {
						$smoduleid = $subformdata[$j]['modid'];
						$scheckmodule = $subformdata[$j]['moduleid'];
						$screate = $subformdata[$j]['create'];
						$sread = $subformdata[$j]['read'];
						$supdate = $subformdata[$j]['update'];
						$sdelete = $subformdata[$j]['delete'];
						$scustom = $subformdata[$j]['custom'];
						$scustomtoolbar = $subformdata[$j]['customactions'];
						$scustoolbarid = "";
						if($scustomtoolbar != "") {
							$stoolbaridsets = explode('||',$scustomtoolbar);
							$scustoolbarid = $stoolbaridsets[0];
						}
						//create / update module and toolbar based on role
						$this->createupdatemoduletoolbar($smoduleid,$roleid,$screate,$sread,$supdate,$sdelete,$scustom,$scustoolbarid,$scheckmodule,$date,$userid,$status,$allmoduleid);
						$smoduleid=1;$screate='false';$sread='false';$supdate='false';$sdelete='false';$scustom='false';$scustoolbarid=1;$scheckmodule='1';
					}
				}
			}
		}
		echo 'TRUE';
	}
	//create / update module & toolbar based on role
	public function createupdatemoduletoolbar($moduleid=1,$roleid=1,$create=null,$read=null,$update=null,$delete=null,$custom=null,$custoolbarid=null,$checkmodule=null,$date=null,$userid=1,$status=0,$allmoduleid) {
		if($checkmodule == 'true') {
			$check = $this->modulecheckinfo($moduleid,$roleid);
			if($check == 'TRUE') {
				{//module-info update
					$moduleinfo = array('moduleid'=>$moduleid,'lastupdatedate'=>$date,'lastupdateuserid'=>$userid,'status'=>1);
					$this->db->where('moduleinfo.userroleid',$roleid);
					$this->db->where('moduleinfo.moduleid',$moduleid);
					$this->db->update('moduleinfo',$moduleinfo);
				}
				{//user role tool-bar id get
					$moduletoolbarid = $this->moduletollbaridget($moduleid);
					$moduleaction = $this->actionnamefetch($moduletoolbarid,$allmoduleid);
					$usertoolbarid = $this->userrolesetedtollbar($moduleaction,$create,$read,$update,$delete,$custom,$custoolbarid,$checkmodule);
					//update
					$usertbprivlge = array('toolbarnameid'=>$usertoolbarid,'status'=>1);
					$this->db->where('usertoolbarprivilege.userroleid',$roleid);
					$this->db->where('usertoolbarprivilege.moduleid',$moduleid);
					$this->db->update('usertoolbarprivilege',$usertbprivlge);
				}
			} else if($check == 'FALSE') {
				{//module-info insert
					$moduleinfo = array('moduleid'=>$moduleid,'userroleid'=>$roleid,'createdate'=>$date,'lastupdatedate'=>$date,'createuserid'=>$userid,'lastupdateuserid'=>$userid,'status'=>$status);
					$this->db->insert('moduleinfo',$moduleinfo);
				}
				{//user role tool-bar id get
					$moduletoolbarid = $this->moduletollbaridget($moduleid);
					$moduleaction = $this->actionnamefetch($moduletoolbarid,$allmoduleid);
					$usertoolbarid = $this->userrolesetedtollbar($moduleaction,$create,$read,$update,$delete,$custom,$custoolbarid,$checkmodule);
					//insert 
					$userpri = array('toolbarnameid'=>$usertoolbarid,'userroleid'=>$roleid,'moduleid'=>$moduleid,'status'=>1);
					$this->db->insert('usertoolbarprivilege',$userpri);
				}
			}
		} else if($checkmodule == 'false') {
			$check = $this->modulecheckinfo($moduleid,$roleid);
			if($check == 'TRUE') {
				{//moduleinfo update
					$this->db->where('moduleinfo.userroleid',$roleid);
					$this->db->where('moduleinfo.moduleid',$moduleid);
					$this->db->update('moduleinfo',array('lastupdatedate'=>$date,'lastupdateuserid'=>$userid,'status'=>0));
				}
				{//user role tool-bar id get
					//update
					$this->db->where('usertoolbarprivilege.userroleid',$roleid);
					$this->db->where('usertoolbarprivilege.moduleid',$moduleid);
					$this->db->update('usertoolbarprivilege',array('status'=>0));
				}
			} else if($check == 'FALSE') {
				{//moduleinfo insert
					$moduleinfo = array('moduleid'=>$moduleid,'userroleid'=>$roleid,'createdate'=>$date,'lastupdatedate'=>$date,'createuserid'=>$userid,'lastupdateuserid'=>$userid,'status'=>0);
					$this->db->insert('moduleinfo',$moduleinfo);
				}
				{//user role tool-bar id get
					$moduletoolbarid = $this->moduletollbaridget($moduleid);
					$moduleaction = $this->actionnamefetch($moduletoolbarid,$allmoduleid);
					$usertoolbarid = $this->userrolesetedtollbar($moduleaction,$create,$read,$update,$delete,$custom,$custoolbarid,$checkmodule);
					//insert
					$userpri = array('toolbarnameid'=>$usertoolbarid,'userroleid'=>$roleid,'moduleid'=>$moduleid,'status'=>0);
					$this->db->insert('usertoolbarprivilege',$userpri);
				}
			}
		}
	}
	//module check in module info table
	public function modulecheckinfo($moduleid,$roleid) {
		$result = 'FALSE';
		$this->db->select('moduleid');
		$this->db->from('moduleinfo');
		$this->db->where('moduleinfo.moduleid',$moduleid);
		$this->db->where('moduleinfo.userroleid',$roleid);
		$data = $this->db->get();
		foreach($data->result() as $row) {
			$result='TRUE';
		}
		return $result;	
	}
	//check usertoolbar is exist or not
	public function toolbarcheckinfo($moduleid,$roleid) {
		$result = 'FALSE';
		$this->db->select('usertoolbarprivilegeid');
		$this->db->from('usertoolbarprivilege');
		$this->db->where('usertoolbarprivilege.moduleid',$moduleid);
		$this->db->where('usertoolbarprivilege.userroleid',$roleid);
		$data = $this->db->get();
		foreach($data->result() as $row) {
			$result='TRUE';
		}
		return $result;
	}
	//user selected action get
	public function userrolesetedtollbar($moduleaction,$create,$read,$update,$delete,$custom,$custoolbarid,$checkmodule) {
		$datasets = "";
		if($checkmodule == 'true') {
			//create
			$caction = ($create == 'true'? $moduleaction[0] : '');
			//read
			$raction = ($read == 'true'? $moduleaction[1] : '');
			//update
			$uaction = ($update == 'true'? $moduleaction[2] : '');
			//delete
			$daction = ($delete == 'true'? $moduleaction[3] : '');
			//custom
			$cusaction = ($custom == 'true'? $custoolbarid : '');
			//total action
			$action = array($caction,$raction,$uaction,$daction,$cusaction); 
			$filter = array_filter($action);
			$datasets = implode(',',$filter);
		}
		return $datasets;
	}
	//default module id get
	public function defaultmoduleidget($wh) {
		$i=0;
		$industryid = $this->Basefunctions->industryid;
		$data = array();
		$this->db->select('moduleid');
		$this->db->from('module');
		$this->db->join('menucategory','menucategory.menucategoryid=module.menucategoryid');
		$this->db->where_not_in('module.status',array(0,2,3));
		if($wh != "1") {
			$this->db->where($wh);
		}
		$this->db->where("FIND_IN_SET('$industryid',module.industryid) >", 0);
		$this->db->order_by('menucategory.sortorder','ASC');
		$this->db->order_by('module.sortorder','ASC');
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = $row->moduleid;
				$i++;
			}
		}
		return $data;
	}
	//Role based module field fetch
	public function modulebasedfieldslistxmlview($roleid,$moduleid) {
		$this->db->select('SQL_CALC_FOUND_ROWS  modulefieldid,fieldlabel,readonly,active,fieldrestrict,moduletabid',false);
		$this->db->from('modulefield');
		$this->db->where_in('modulefield.moduletabid',$moduleid);
		$this->db->where_not_in('modulefield.status',array(3,0,10));
		$result=$this->db->get();
		return $result;
	}
	//fetch role based dashboard moduleid
	public function fetchdashboardmoduleid($moduleid) {
		$modid = 1;
		if($moduleid!='') {
			$result = $this->db->query('select moduleid,moduledashboardid from module where module.moduleid='.$moduleid.' AND module.moduledashboardid != "" AND module.status NOT IN (0,10)',false);
			if($result->num_rows() > 0) {
				foreach($result->result() as $row) {
					$modid = $row->moduledashboardid;
				}
			}
		}
		return $modid;
	}
	//check module field available
	public function fetchuserrolebasedmodulefieldmode($moduleid,$roleid,$fieldid) {
		$datasets = array('active'=>'No','readonly'=>'No');
		
		$this->db->select('userrolemodulefiledid,fieldactive,fieldreadonly',false);
		$this->db->from('userrolemodulefiled');
		$this->db->where_in('userrolemodulefiled.moduleid',$moduleid);
		$this->db->where('userrolemodulefiled.userroleid',$roleid);
		$this->db->where('userrolemodulefiled.modulefieldid',$fieldid);
		$result = $this->db->get();
		foreach($result->result() as $row) {
			$datasets = array('active'=>$row->fieldactive,'readonly'=>$row->fieldreadonly);
		}
		return $datasets;
	}
	//Role based module field insertion / updation
	public function rolemodulefieldsubmitbtnmodel() {
		$roleid=$_POST['roleid'];
		$griddata=$_POST['fieldgriddata'];
		$formdata=json_decode( $griddata, true );
		$count = count($formdata);
		$status = $this->Basefunctions->activestatus;
		$date = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$i=0;
		for( $i=0; $i <$count ; $i++ ) {
			$modulefieldid = $formdata[$i]['modulefieldid'];
			$modid = $formdata[$i]['moduleid'];
			$activemode = "false";
			$activemode = $formdata[$i]['active'];
			$datasets = $this->checkrestrictfieldvisiblestatus($modulefieldid);
			$readmode = 'No';
			$visiblemode = 'No';
			if( count($datasets)>0 ) {
				$readmode = $datasets['fread'];
				$visiblemode = $datasets['factive'];
			} else {
				$visible = $formdata[$i]['visible'];
				$readonly = $formdata[$i]['readonly'];
				$visiblemode = ($visible == 'true'? 'Yes':'No');
				$readmode = ($readonly == 'true'? 'Yes':'No');
			}
			if($activemode == "true") {
				//module field create/update
				$this->moduledfieldinformationcreateupdate($roleid,$modid,$modulefieldid,$visiblemode,$readmode,$date,$userid,$status);
			} else if($activemode == "false") {
				$fielddisable = array('fieldactive'=>$visiblemode,'fieldreadonly'=>$readmode,'lastupdatedate'=>$date,'lastupdateuserid'=>$userid,'status'=>0);
				$this->db->update('userrolemodulefiled',$fielddisable,array('modulefieldid'=>$modulefieldid,'moduleid'=>$modid,'userroleid'=>$roleid));
			}
		}
		//hidden module field create/update
		$datasets = $this->hiddenmodulebasedfieldslist($modid);
		foreach($datasets->result() as $row) {
			$modulefieldid = $row->modulefieldid;
			$visible = $row->active;
			$read = $row->readonly;
			$this->moduledfieldinformationcreateupdate($roleid,$modid,$modulefieldid,$visible,$read,$date,$userid,$status);
		}
		echo 'TRUE';
	}
	//module field create/update
	public function moduledfieldinformationcreateupdate($roleid,$modid,$modulefieldid,$visible,$read,$date,$userid,$status) {
		//check the module field
		$mchk = $this->modulefieldidcheck($roleid,$modid,$modulefieldid);
		if($mchk == 'True') {
			$check = $this->checkuserrolemodulefieldvalue($modid,$roleid,$modulefieldid);
			if($check == 'TRUE') {
				$visiblevalues = array('fieldactive'=>$visible,'fieldreadonly'=>$read,'lastupdatedate'=>$date,'lastupdateuserid'=>$userid,'status'=>$status);
				$this->db->where('userrolemodulefiled.modulefieldid',$modulefieldid);
				$this->db->where('userrolemodulefiled.moduleid',$modid);
				$this->db->where('userrolemodulefiled.userroleid',$roleid);
				$this->db->update('userrolemodulefiled',$visiblevalues);
				//tab section updation
				$tabsecid = $this->modulefieldtabsecidfetch($modulefieldid,$modid);
				$tabsecchk = "";
				$tabsecchk = $this->tabsectionuserrolecheck($tabsecid,$modid,$roleid);
				if($tabsecchk == 'Fail') {
					//module tab section
					$this->db->query('update moduletabsection set userroleid = CONCAT(userroleid,",'.$roleid.'") where moduletabsection.moduleid='.$modid.' AND moduletabsection.moduletabsectionid='.$tabsecid.'');
				}
				//tab group updation
				$tabgrpid = $this->modulefieldtabgrpidfetch($tabsecid,$modid);
				$tabgrpchk = "";
				$tabgrpchk = $this->tabgroupuserrolecheck($tabgrpid,$roleid,$modid);
				if($tabgrpchk == 'Fail') {
					//module tab group
					$this->db->query('update moduletabgroup set userroleid = CONCAT(userroleid,",'.$roleid.'") where moduletabgroup.moduleid='.$modid.' AND moduletabgroup.moduletabgroupid='.$tabgrpid.'');
				}
			} else {
				//update user role to module field table
				$chk = '';
				$chk = $this->modulefieldidcheck($roleid,$modid,$modulefieldid);
				if($chk == 'Fail') {
					//module field
					$this->db->query(' update modulefield set userroleid = CONCAT(userroleid,",'.$roleid.'") where modulefield.modulefieldid='.$modulefieldid.' AND modulefield.moduletabid='.$modid.' ');
				}
				//tab section updation
				$tabsecid = $this->modulefieldtabsecidfetch($modulefieldid,$modid);
				$tabsecchk = "";
				$tabsecchk = $this->tabsectionuserrolecheck($tabsecid,$modid,$roleid);
				if($tabsecchk == 'Fail') {
					//module tab section
					$this->db->query('update moduletabsection set userroleid = CONCAT(userroleid,",'.$roleid.'") where moduletabsection.moduleid='.$modid.' AND moduletabsection.moduletabsectionid='.$tabsecid.'');
				}
				//tab group updation
				$tabgrpid = $this->modulefieldtabgrpidfetch($tabsecid,$modid);
				$tabgrpchk = "";
				$tabgrpchk = $this->tabgroupuserrolecheck($tabgrpid,$roleid,$modid);
				if($tabgrpchk == 'Fail') {
					//module tab group
					$this->db->query('update moduletabgroup set userroleid = CONCAT(userroleid,",'.$roleid.'") where moduletabgroup.moduleid='.$modid.' AND moduletabgroup.moduletabgroupid='.$tabgrpid.'');
				}
				//create new restrictions
				$visiblevalues = array('modulefieldid'=>$modulefieldid,'moduleid'=>$modid,'userroleid'=>$roleid,'fieldactive'=>$visible,'fieldreadonly'=>$read,'createdate'=>$date,'lastupdatedate'=>$date,'createuserid'=>$userid,'lastupdateuserid'=>$userid,'status'=>$status);
				$this->db->insert('userrolemodulefiled',$visiblevalues);
			}
		} else {
			//update user role to module field table
			$chk = '';
			$chk = $this->modulefieldidcheck($roleid,$modid,$modulefieldid);
			if($chk == 'Fail') {
				//module field
				$this->db->query(' update modulefield set userroleid = CONCAT(userroleid,",'.$roleid.'") where modulefield.modulefieldid='.$modulefieldid.' AND modulefield.moduletabid='.$modid.' ');
			}
			//tab section updation
			$tabsecid = $this->modulefieldtabsecidfetch($modulefieldid,$modid);
			$tabsecchk = "";
			$tabsecchk = $this->tabsectionuserrolecheck($tabsecid,$modid,$roleid);
			if($tabsecchk == 'Fail') {
				//module tab section
				$this->db->query('update moduletabsection set userroleid = CONCAT(userroleid,",'.$roleid.'") where moduletabsection.moduleid='.$modid.' AND moduletabsection.moduletabsectionid='.$tabsecid.'');
			}
			//tab group updation
			$tabgrpid = $this->modulefieldtabgrpidfetch($tabsecid,$modid);
			$tabgrpchk = "";
			$tabgrpchk = $this->tabgroupuserrolecheck($tabgrpid,$roleid,$modid);
			if($tabgrpchk == 'Fail') {
				//module tab group
				$this->db->query('update moduletabgroup set userroleid = CONCAT(userroleid,",'.$roleid.'") where moduletabgroup.moduleid='.$modid.' AND moduletabgroup.moduletabgroupid='.$tabgrpid.'');
			}
			$check = $this->checkuserrolemodulefieldvalue($modid,$roleid,$modulefieldid);
			if($check == 'TRUE') {
				$visiblevalues = array('fieldactive'=>$visible,'fieldreadonly'=>$read,'lastupdatedate'=>$date,'lastupdateuserid'=>$userid,'status'=>$status);
				$this->db->where('userrolemodulefiled.modulefieldid',$modulefieldid);
				$this->db->where('userrolemodulefiled.moduleid',$modid);
				$this->db->where('userrolemodulefiled.userroleid',$roleid);
				$this->db->update('userrolemodulefiled',$visiblevalues);
			} else {
				//create new restrictions
				$visiblevalues = array('modulefieldid'=>$modulefieldid,'moduleid'=>$modid,'userroleid'=>$roleid,'fieldactive'=>$visible,'fieldreadonly'=>$read,'createdate'=>$date,'lastupdatedate'=>$date,'createuserid'=>$userid,'lastupdateuserid'=>$userid,'status'=>$status);
				$this->db->insert('userrolemodulefiled',$visiblevalues);
			}
		}
	}
	//Hidden module fields list
	public function hiddenmodulebasedfieldslist($moduleid) {
		$this->db->select('modulefieldid,readonly,active,fieldrestrict,moduletabid',false);
		$this->db->from('modulefield');
		$this->db->where_in('modulefield.moduletabid',$moduleid);
		$this->db->where_in('modulefield.status',array(10));
		$result=$this->db->get();
		return $result;
	}
	//check restricted field value
	public function checkrestrictfieldvisiblestatus($modfieldid) {
		$data = array();
		$resultset = $this->db->select('active,readonly')->from('modulefield')->where('fieldrestrict','Yes')->where('modulefieldid',$modfieldid)->get();
		if($resultset->num_rows() > 0) {
			foreach($resultset->result() as $row) {
				$data = array('factive'=>$row->active,'fread'=>$row->readonly);
			}
		}
		return $data;
	}
	//module field id check in modulefield
	public function modulefieldidcheck($roleid,$moduleid,$modulefieldid) {
		$data='Fail';
		$this->db->select('modulefieldid');
		$this->db->from('modulefield');
		$this->db->where('modulefield.modulefieldid',$modulefieldid);
		$this->db->where('modulefield.moduletabid',$moduleid);
		$this->db->where("FIND_IN_SET('$roleid',modulefield.userroleid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			$data='True';
		} else {
			$data='Fail';
		}
		return $data;
	}
	//module field id check in module tab section
	public function modulefieldidtabsecheck($roleid,$moduleid) {
		$data='Fail';
		$this->db->select('moduletabsectionid');
		$this->db->from('moduletabsection');
		$this->db->where('moduletabsection.moduleid',$moduleid);
		$this->db->where("FIND_IN_SET('$roleid',moduletabsection.userroleid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			$data='True';
		} else {
			$data='Fail';
		}
		return $data;
	}
	//check module section based on role
	public function tabsectionuserrolecheck($tabsecid,$modid,$roleid) {
		$data='';
		$this->db->select('moduletabsectionid');
		$this->db->from('moduletabsection');
		$this->db->where('moduletabsection.moduleid',$modid);
		$this->db->where('moduletabsection.moduletabsectionid',$tabsecid);
		$this->db->where("FIND_IN_SET('$roleid',moduletabsection.userroleid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			$data='True';
		} else {
			$data='Fail';
		}
		return $data;
	}
	//module field id check in module tab group
	public function modulefieldidtabgrpcheck($roleid,$moduleid) {
		$data='';
		$this->db->select('moduletabgroupid');
		$this->db->from('moduletabgroup');
		$this->db->where('moduletabgroup.moduleid',$moduleid);
		$this->db->where("FIND_IN_SET('$roleid',moduletabgroup.userroleid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			$data='True';
		} else {
			$data='Fail';
		}
		return $data;
	}
	//module field id check in module tab group
	public function tabgroupuserrolecheck($tabgrpid,$roleid,$moduleid) {
		$data='';
		$this->db->select('moduletabgroupid');
		$this->db->from('moduletabgroup');
		$this->db->where('moduletabgroup.moduleid',$moduleid);
		$this->db->where('moduletabgroup.moduletabgroupid',$tabgrpid);
		$this->db->where("FIND_IN_SET('$roleid',moduletabgroup.userroleid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			$data='True';
		} else {
			$data='Fail';
		}
		return $data;
	}
	//value drop down load-
	public function valuedropdownloadfunmodel() {
		$userroleid = $_GET['roleid'];
		$moduleid = $_GET['modid'];
		$dashboardmodid = $this->fetchdashboardmoduleid($moduleid);
		$moduleids = ( ($dashboardmodid!=1) ? $dashboardmodid : $_GET['modid'] );
		$modid = explode(',',$moduleids);
		$i=0;
		$this->db->select('modulefield.modulefieldid,modulefield.columnname,modulefield.fieldlabel,modulefield.moduletabid');
		$this->db->from('modulefield');
		$this->db->join('userrolemodulefiled','userrolemodulefiled.modulefieldid=modulefield.modulefieldid');
		$this->db->where_in('modulefield.moduletabid',$modid);
		$this->db->where_in('modulefield.uitypeid',array(17,18));
		$this->db->where("FIND_IN_SET('$userroleid',userrolemodulefiled.userroleid) >", 0);
		$this->db->where('modulefield.status',1);
		$this->db->where('userrolemodulefiled.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('columnname'=>$row->columnname,'datasid'=>$row->modulefieldid,'dataname'=>$row->fieldlabel,'modtabid'=>$row->moduletabid);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//profile value grid fetch
	public function modulefieldvaluegetxmlview($roleid,$modid,$tableid) {
		$result = 'false';
		$industryid = $this->Basefunctions->industryid;
		if($tableid != "" && $tableid != 'undefined' ) {
			$table = substr($tableid, 0, -2);
			$tablename = $table.'name';
			$this->db->select('SQL_CALC_FOUND_ROWS '.$tableid.' as fieldid,'.$tablename.' as fieldname,status',false);
			$this->db->from($table);
			$this->db->where("FIND_IN_SET('$modid',".$table.".moduleid) >", 0);
			$this->db->where("FIND_IN_SET('$industryid',".$table.".industryid) >", 0);
			$this->db->where_in($table.'.status',array(1));
			$result=$this->db->get();
		}
		return $result;
	}
	//field value check
	public function fieldvaluecheck($roleid,$modid,$table,$fieldvalue) {
		$data = "FALSE";
		if($table != "" && $table != 'undefined') {
			$fieldname = $table.'name';
			$filedid = $table.'id';
			$this->db->select($fieldname);
			$this->db->from($table);
			$this->db->where("FIND_IN_SET('$roleid',".$table.".userroleid) >", 0);
			$this->db->where("FIND_IN_SET('$modid',".$table.".moduleid) >", 0);
			$this->db->where($table.'.'.$filedid,$fieldvalue);
			$result = $this->db->get();
			if($result->num_rows() > 0) {
				foreach($result->result() as $row) {
					$data = "TRUE";
				}
			}
		}
		return $data;
	}
	//check module field in userrolemodule
	public function checkuserrolemodulefieldvalue($moduleid,$roleid,$modulefieldid) {
		$data = "FALSE";
		$this->db->select('userrolemodulefiledid');
		$this->db->from('userrolemodulefiled');
		$this->db->where('userrolemodulefiled.modulefieldid',$modulefieldid);
		$this->db->where('userrolemodulefiled.moduleid',$moduleid);
		$this->db->where('userrolemodulefiled.userroleid',$roleid);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			$data='TRUE';
		} else {
			$data='FALSE';
		}
		return $data;
	}
	//fetch section id based on module field
	public function modulefieldtabsecidfetch($modulefieldid,$modid) {
		$secid = 1;
		$secdata = $this->db->select('modulefield.moduletabsectionid')->from('modulefield')->where('modulefield.modulefieldid',$modulefieldid)->where('modulefield.moduletabid',$modid)->get();
		foreach($secdata->result() as $data) {
			$secid = $data->moduletabsectionid;
		}
		return $secid;
	}
	//fetch tab group id based on section
	public function modulefieldtabgrpidfetch($tabsecid,$modid) {
		$grpid = 1;
		$grpdata = $this->db->select('moduletabsection.moduletabgroupid')->from('moduletabsection')->where('moduletabsection.moduletabsectionid',$tabsecid)->where('moduletabsection.moduleid',$modid)->get();
		foreach($grpdata->result() as $data) {
			$grpid = $data->moduletabgroupid;
		}
		return $grpid;
	}
	//check module field available
	public function checkuserrolemodulefield($moduleid,$roleid,$fieldid) {
		$this->db->select('userrolemodulefiledid',false);
		$this->db->from('userrolemodulefiled');
		$this->db->where_in('userrolemodulefiled.moduleid',$moduleid);
		$this->db->where('userrolemodulefiled.userroleid',$roleid);
		$this->db->where('userrolemodulefiled.modulefieldid',$fieldid);
		$this->db->where('userrolemodulefiled.status',1);
		$result = $this->db->get();
		if($result->num_rows() >0){
			return 'TRUE';
		} else {
			return 'FALSE';
		}
	}
	//check modulefield
	public function modulecheckmodulefield($moduleid,$roleid) {
		$this->db->select('moduletabid');
		$this->db->from('modulefield');
		$this->db->where('modulefield.moduletabid',$moduleid);
		$this->db->where("FIND_IN_SET('$roleid',modulefield.userroleid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				return 'TRUE';
			}
		} else {
			return 'FALSE';
		}
	}
	//assign field values
	public function modulevaluesubmitmodel() {
		$tableid = $_POST['tableid'];
		$table = substr($tableid, 0, -2);
		$fieldname = $table.'name';
		$roleid = $_POST['roleid'];
		$moduleid = $_POST['modid'];
		$modulefield = $_POST['modulefield'];
		$fieldgriddata = $_POST['fieldgriddata'];
		$griddata=$_POST['fieldgriddata'];
		$formdata=json_decode( $griddata,true );
		$count = count($formdata);
		$date = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		for( $i=0; $i <$count ; $i++ ) {
			$fieldid = $formdata[$i]['fieldid'];
			$fstatus = $formdata[$i]['visible'];
			$status = ($fstatus == 'true'? '1':'2');
			if($status == '1') {
				$check = $this->userroleidcheckindropdown($table,$tableid,$fieldid,$roleid,$moduleid);
				if($check == 'FALSE') {
					$this->db->query('update '.$table.' set userroleid = CONCAT(userroleid,",'.$roleid.'") where '.$table.'.'.$tableid.'='.$fieldid.'');
				}
			} elseif($status == '2') {
				$check = $this->userroleidcheckindropdown($table,$tableid,$fieldid,$roleid,$moduleid);
				if($check == 'TRUE') {
					$this->db->query('update '.$table.' set userroleid = TRIM(BOTH "," FROM REPLACE(CONCAT(",", userroleid,","), ",'.$roleid.',", ",")) where '.$table.'.'.$tableid.'='.$fieldid.'');
				}
			}
		}
		echo 'TRUE';
	}
	//role id check in drop down list
	public function userroleidcheckindropdown($table,$tableid,$fieldid,$roleid,$moduleid) {
		$this->db->select($tableid);
		$this->db->from($table);
		$this->db->where($table.'.'.$tableid,$fieldid);
		$this->db->where("FIND_IN_SET('$roleid',".$table.".userroleid) >", 0);
		$this->db->where("FIND_IN_SET('$moduleid',".$table.".moduleid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				return 'TRUE';
			}
		} else {
			return 'FALSE';
		}
	}
	//fetch field value name
	public function fetchfieldvaluenames($table,$tableid,$fieldid) {
		$fieldval = "";
		$this->db->select($table.'name AS fname');
		$this->db->from($table);
		$this->db->where($table.'.'.$tableid,$fieldid);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$fieldval = $row->fname;
			}
		}
		return $fieldval;
	}
	//unique role name check
	public function roleuniquenamecheckmodel() {
		$dataset = "False";
		$userrolename = $_POST['name'];
		if($userrolename != "") {
			$result = $this->db->select('userrole.userroleid')->from('userrole')->where('userrolename',$userrolename)->where('userrole.status',1)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					$dataset = $row->userroleid;
				}
			}
		}
		echo $dataset;
	}
	//custom toolbar info generate
	public function customtoolbarinfogetmodel() {
		$moduleids = json_decode($_POST['modids'],true);
		$modids=array_map('intval',array(1));
		if(is_array($moduleids)) {
			$moduleids[]='1';
			$modids = array_map('intval',$moduleids);
		} else {
			$modids = array_map('intval',explode(',',$moduleids));
		}
		$toolbars = $_POST['toolbarids'];
		$toolbarids = explode('||',$toolbars);
		$usercustomaction = explode(',',$toolbarids[0]);
		$modcustomaction = array_map('intval', explode(',',$toolbarids[1]));
		$data = "";
		//get toolbar names
		$this->db->select('toolbarname.toolbarnameid,toolbarname.toolbarname,toolbarname.toolbartitle',false);
		$this->db->from('toolbarname');
		$this->db->join('module','module.moduleid=toolbarname.moduleid');
		$this->db->where_in('toolbarname.toolbarnameid',$modcustomaction);
		$this->db->where_in('toolbarname.moduleid',$modids);
		$this->db->where('toolbarname.status',1);
		$this->db->where_in('module.status',array(1,3));
		$toolbarset = $this->db->get();
		foreach($toolbarset->result() as $row) {
			$check = ( (in_array($row->toolbarnameid,$usercustomaction)) ? " checked='checked'" : ""  );
			$data .= '<div class="large-6 medium-6 small-12 columns end">
				<div class="large-12 columns">
					<input id="cusactchkbox'.$row->toolbarnameid.'"'.$check.' type="checkbox" class="checkboxcusact filled-in" tabindex="230" name="cusactchkbox'.$row->toolbarnameid.'" value="'.$row->toolbarnameid.'"><label for="cusactchkbox'.$row->toolbarnameid.'"'.$check.'>'.$row->toolbartitle.'</label>
				</div>
			</div>';
		}
		return json_encode( array('datasets'=>$data,'moduleaction'=>$toolbarids[1]) );
	}
	//sub module custom toolbar info generate
	public function subcustomtoolbarinfogetmodel() {
		$submoduleids = json_decode($_POST['submodids'],true);
		$moduleids = json_decode($_POST['modids'],true);
		$modid = array_merge($moduleids,$submoduleids);
		$modids=array_map('intval',array(1));
		if(is_array($moduleids)) {
			$moduleids[]='1';
			$modids = array_map('intval',$moduleids);
		} else {
			$modids = array_map('intval',explode(',',$moduleids));
		}
		$toolbars = $_POST['toolbarids'];
		$toolbarids = explode('||',$toolbars);
		$usercustomaction = explode(',',$toolbarids[0]);
		$modcustomaction = array_map('intval', explode(',',$toolbarids[1]));
		$data = "";
		//get toolbar names
		$this->db->select('toolbarname.toolbarnameid,toolbarname.toolbarname,toolbarname.toolbartitle',false);
		$this->db->from('toolbarname');
		$this->db->join('module','module.moduleid=toolbarname.moduleid');
		$this->db->where_in('toolbarname.toolbarnameid',$modcustomaction);
		$this->db->where_in('toolbarname.moduleid',$modids);
		$this->db->where('toolbarname.status',1);
		$this->db->where_in('module.status',array(1,3));
		$toolbarset = $this->db->get();
		foreach($toolbarset->result() as $row) {
			$check = ( (in_array($row->toolbarnameid,$usercustomaction)) ? " checked='checked'" : ""  );
			$data .= '<div class="large-6 medium-6 small-12 columns end">
				<div class="large-12 columns">
					<input id="cusactchkbox'.$row->toolbarnameid.'"'.$check.' type="checkbox" class="subcheckboxcusact filled-in" tabindex="230" name="cusactchkbox'.$row->toolbarnameid.'" value="'.$row->toolbarnameid.'"><label for="cusactchkbox'.$row->toolbarnameid.'"'.$check.'>'.$row->toolbartitle.'</label>
				</div>
			</div>';
		}
		return json_encode( array('datasets'=>$data,'moduleaction'=>$toolbarids[1]) );
	}
}