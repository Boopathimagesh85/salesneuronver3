<style type="text/css">
.padding-space-open-for-form {
    padding-left: 0.8em !important;
    padding-right: 0.6em !important;
}
.arrow_box1{
	left:-82.166px !important;
}
.arrow_box2{
	left:-62.166px !important;
}
.ui-jqgrid-view [type="checkbox"]:not(:checked), .ui-jqgrid-view [type="checkbox"]:checked {
	    position: relative;
	    left: auto;
	    opacity: 1 !important;
	    vertical-align: middle;
	}
	.headerformcaptionstyleforprofile, .headerformcaptionstyle {
	    font-size: 0.87rem;
	    font-weight: 500;
	    padding: 0.2em 0px !important;
	    background: #fff !important;
		color:black !important;
	}
	.ui-jqgrid .ui-jqgrid-hdiv {
	    position: relative;
	    margin: 0;
	    padding: 22px 0px;
	}
	.ui-jqgrid .ui-jqgrid-htable th div {
	    height: 26px;
	    margin-top: -6px;
	}
	.ui-jqgrid .ui-jqgrid-resize-ltr {
	    margin: -10px -2px -2px 0;
	}
	.ui-jqgrid tr:hover {
	    background-color: #fff;
	}
	.ui-widget-content .ui-state-default {
	    font-size: 15px;
	}
	.ui-jqgrid .ui-grid-ico-sort {
	    display: none;
	}
	.select2-container .select2-choice, .searchfilteroverlay .select2-container .select2-choice, .reportsearchfilteroverlay .select2-container .select2-choice
	{
	margin:0px !important;
	}
	.docscfcontentareaht73 
	{
	width:100% !important;
	}
	.docscfrowheaderht12
	{
		width:100% !important;
	}
	.singleaction-drop li {
		line-height:40px !important;
	}
</style>

<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<?php
				$device = $this->Basefunctions->deviceinfo();
				$dataset['gridtitle'] = $gridtitle['title'];
				$dataset['titleicon'] = $gridtitle['titleicon'];
				$dataset['moduleid'] = '247';
				$dataset['action'][0] = array('actionid'=>'roleaddicon','actiontitle'=>'Add','actionmore'=>'Yes','ulname'=>'createrole');
				$dataset['action'][1] = array('actionid'=>'useraddicon','actiontitle'=>'Add','actionmore'=>'Yes','ulname'=>'userrole');
				$dataset['action'][2] = array('actionid'=>'rolemodeulesbt','actiontitle'=>'Submit','actionmore'=>'Yes','ulname'=>'module');
				$dataset['action'][3] = array('actionid'=>'rolefieldsbtbtn','actiontitle'=>'Submit','actionmore'=>'Yes','ulname'=>'fields');
				$dataset['action'][4] = array('actionid'=>'rolevaluesubmit','actiontitle'=>'Submit','actionmore'=>'Yes','ulname'=>'fieldsvalue');
				$this->load->view('Base/formwithgridmenuheader.php',$dataset);
			?>	
			<div class="large-12 columns addformunderheader">&nbsp;</div>
			<div class="large-12 columns scrollbarclass addformcontainer rolestouch">
			<!-- More Action DD start -- Gowtham -->
			
			<ul id="createroleadvanceddrop" class="singleaction-drop arrow_box" style="white-space: nowrap; position: absolute;width:200px;top: 56px !important; opacity: 1; display: none;">
			<li id="peronalsettingicon" style="color:white;background-color:#2c2f48;height: 60px;    top: -8px; position: relative;border-top-left-radius: 4px;border-top-right-radius: 4px;"><i title="actionicons" id="actionicons" class="material-icons" style="font-size: 2rem;line-height: 1.9;color: #fff;">apps</i><span style="position:relative;top:-12px;color:#fff !important;font-size:0.9rem; font-family: 'Nunito',sans-serif !important;">Actions Panel</span></li>
				<li id="roleediticon"><span class="icon-box"><i class="material-icons editiconclass" title="Edit" style="font-size: 2rem;line-height: 1.9;color: black;">edit</i>edit</span></li>
				<li id="roledeleteicon"><span class="icon-box"><i class="material-icons deleteiconclass" title="Delete" style="font-size: 2rem;line-height: 1.9;color: black;">delete</i>delete</span></li>
				<li id="profileicon"><span class="icon-box"><i class="material-icons" title="Profile" style="font-size: 2rem;line-height: 1.9;color: black;">verified_user</i>Profile</span></li>
			</ul>
			
			<ul id="userroleadvanceddrop" class="singleaction-drop arrow_box " style="white-space: nowrap; position: absolute; width:200px;top: 56px !important; left: -62.1666px; opacity: 1; display: none;">
			<li id="peronalsettingicon" style="color:white;background-color:#2c2f48;height: 60px;    top: -8px; position: relative;border-top-left-radius: 4px;border-top-right-radius: 4px;"><i title="actionicons" id="actionicons" class="material-icons" style="font-size: 2rem;line-height: 1.9;color: #fff ;">apps</i><span style="position:relative;top:-12px;color:#fff  !important;font-size:0.9rem; font-family: 'Nunito',sans-serif !important;">Actions Panel</span></li>
				<li id="userreloadicon"><span class="icon-box"><i class="material-icons reloadiconclass" title="Reload" style="font-size: 2rem;line-height: 1.9;color: black;">refresh</i>Reload</span></li>
			</ul>
			<ul id="moduleadvanceddrop" class="singleaction-drop arrow_box" style="white-space: nowrap; position: absolute; width:200px;top: 56px !important;left: -62.1666px; opacity: 1; display: none;">
			<li id="peronalsettingicon" style="color:white;background-color:#2c2f48;height: 60px;    top: -8px; position: relative;border-top-left-radius: 4px;border-top-right-radius: 4px;"><i title="actionicons" id="actionicons" class="material-icons" style="font-size: 2rem;line-height: 1.9;color: #fff;">apps</i><span style="position:relative;top:-12px;color:#fff !important;font-size:0.9rem; font-family: 'Nunito',sans-serif !important;">Actions Panel</span></li>
				<li id="rolemodreload"><span class="icon-box"><i class="material-icons reloadiconclass" title="Reload" style="font-size: 2rem;line-height: 1.9;color: black;">refresh</i>Reload</span></li>
				<li id="rolemodsearchicon"><span class="icon-box"><i class="material-icons searchiconclass " title="Search" style="font-size: 2rem;line-height: 1.9;color: black;">search</i>Search</span></li>
			</ul>
			<ul id="fieldsadvanceddrop" class="singleaction-drop arrow_box" style="white-space: nowrap; position: absolute;width:200px;top: 56px !important; left: -62.1666px; opacity: 1; display: none;">
			<li id="peronalsettingicon" style="color:white;background-color:#2c2f48;height: 60px;    top: -8px; position: relative;border-top-left-radius: 4px;border-top-right-radius: 4px;"><i title="actionicons" id="actionicons" class="material-icons" style="font-size: 2rem;line-height: 1.9;color: #fff;">apps</i><span style="position:relative;top:-12px;color:#fff !important;font-size:0.9rem; font-family: 'Nunito',sans-serif !important;">Actions Panel</span></li>
				<li id="rolefieldreload"><span class="icon-box"><i class="material-icons reloadiconclass" title="Reload" style="font-size: 2rem;line-height: 1.9;color: black;">refresh</i>Reload</span></li>
			</ul>
			<ul id="fieldsvalueadvanceddrop" class="singleaction-drop arrow_box" style="white-space: nowrap; position: absolute; width:200px;top: 56px !important; left: -62.1666px; opacity: 1; display: none;">
			<li id="peronalsettingicon" style="color:white;background-color:#2c2f48;height: 60px;    top: -8px; position: relative;border-top-left-radius: 4px;border-top-right-radius: 4px;"><i title="actionicons" id="actionicons" class="material-icons" style="font-size: 2rem;line-height: 1.9;color: #fff;">apps</i><span style="position:relative;top:-12px;color:#fff !important;font-size:0.9rem; font-family: 'Nunito',sans-serif !important;">Actions Panel</span></li>
				<li id="rolefieldvalreload"><span class="icon-box"><i class="material-icons reloadiconclass" title="Reload" style="font-size: 2rem;line-height: 1.9;color: black;">refresh</i>Reload</span></li>
			</ul>
	<!-- More Action DD End -- Gowtham -->
				<div id="subformspan1" class="hiddensubform add-update-border">
				<div class="closed" id="rolessectionoverlay">
				<div class="row sectiontoppadding">&nbsp;</div>
					<div class="large-12 columns">
					<form method="POST" name="userrolecreateionform" class="" action ="" id="userrolecreateionform">
						<span id="userroleaddvalidation" class="validationEngineContainer" >
							<span id="userroleupdatevalidation" class="validationEngineContainer">
								<div class="large-4 columns paddingbtm large-offset-4">
									<div class="large-12 columns cleardataform border-modal-8px z-depth-5 borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
										<div class="large-12 columns sectionheaderformcaptionstyle">Basic Details</div>
										<div class="static-field large-12 columns" id="parentrolediv">
											<label>
												Parent Role Name<span class="mandatoryfildclass">*</span>
											</label>
											<select class="chzn-select validate[required]" id="parentroleid" name="parentroleid" data-prompt-position="bottomLeft:14,36" tabindex="100" data-placeholder="Select" style="    border-bottom: 1px solid #ffffff !important;">
												<option value=""></option>
												<?php foreach($rolename as $key):?>
													<option value="<?php echo $key->userroleid;?>" data-roleid="<?php echo $key->userroleid;?>"><?php echo $key->userrolename;?></option>
												<?php endforeach;?>
											</select>
										</div>
										
											
										
										<div class="input-field large-12 columns">
											<input id="rolename" class="validate[required,funcCall[roleuniquenamechk],maxSize[100]]" type="text" data-prompt-position="bottomLeft" tabindex="101" name="rolename">
											<label for="rolename">
												Role Name<span class="mandatoryfildclass">*</span>
											</label>
										</div>
										<div class="static-field large-12 columns">
											<label>
												Profile Name<span class="mandatoryfildclass">*</span>
											</label>
											<select class="chzn-select validate[required]" id="profileid" name="profileid" data-prompt-position="topLeft:14,36" tabindex="102" data-placeholder="Select"  style="border-bottom: 1px solid #ffffff !important;">
												<option value=""></option>
												<?php foreach($profile as $key):?>
													<option value="<?php echo $key->profileid;?>" data-profileid="<?php echo $key->profileid;?>"><?php echo $key->profilename;?></option>
												<?php endforeach;?>
											</select>
										</div>
										<div class="input-field large-12 columns">
											<textarea id="roledescription" class="materialize-textarea validate[maxSize[200]]" data-prompt-position="topLeft" tabindex="103" name="roledescription"></textarea>
											<label for="roledescription">Description</label>
										</div>
										<input type="hidden" name="primaryid" id="primaryid" value="" />
										<div class="divider large-12 columns"></div>
										<div class="large-12 columns submitkeyboard sectionalertbuttonarea" style="text-align: right">
											<input id="rolesubmitbtn" class="alertbtnyes addkeyboard" type="button" value="Submit" name="rolesubmitbtn" tabindex="104">
											<input id="roleupdatebtn" class="alertbtnyes updatekeyboard hidedisplay" type="button" value="Submit" name="roleupdatebtn" tabindex="105" style="display: none;">
											<input id="rolecancelbutton" class="alertbtnno cancelkeyboard" type="button" value="Cancel" name="rolecancelbutton" tabindex="106">
										</div>
									</div>
								</div>
							</span>
						</span>
					</form>
					</div>
					</div>
					<div class="row mblhidedisplay">&nbsp;</div>
					<div class="large-12 columns paddingbtm  padding-space-open-for-form">
						<div class="large-12 columns paddingzero">
							<div class="large-12 columns viewgridcolorstyle borderstyle" style="padding-left:0;padding-right:0;">
								 <div class="large-12 columns headerformcaptionstyle" style="height:auto;padding:0.4rem !important;">
									<span class="large-6 medium-6 small-12 columns lefttext small-only-text-center" style="top:1px !important;">Roles List</span>
								</div>
								<div class="large-12 columns forgetinggridname viewgridcolorstyle " id="rolesaddgridwidth" style="padding-left:0;padding-right:0;">	
									<table id="rolesaddgrid"></table>
									<div id="rolesaddgridnav"> </div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="subformspan2" class="hidedisplay hiddensubform">
				<div class="closed" id="usersectionoverlay">
				<div class="row sectiontoppadding">&nbsp;</div>
					<div class="large-12 columns">
					<form method="POST" name="userroleassignform" class="" action ="" id="userroleassignform">
						<span id="userroleassidnaddvalidation" class="validationEngineContainer" >
							<span id="userroleassidnupdatevalidationvalidation" class="validationEngineContainer">
								<div class="large-4 columns  paddingbtm large-offset-4">
									<div class="large-12 columns cleardataform border-modal-8px z-depth-5 borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
										<div class="large-12 columns sectionheaderformcaptionstyle">Basic Details</div>
										<div class="static-field large-12 columns">
											<label>
												User<span class="mandatoryfildclass">*</span>
											</label>
											<select class="chzn-select validate[required]" id="userroleempid" multiple name="userroleempid" data-prompt-position="topLeft:14,36" data-placeholder="Select" >
												<option value=""></option>
												<?php foreach($empname as $key):?>
													<option value="<?php echo $key->employeeid;?>" data-roleid="<?php echo $key->employeeid;?>"><?php echo $key->employeename;?></option>
												<?php endforeach;?>
											</select>
											<input type="hidden" name="multiuserid" id="multiuserid" value="" />
										</div>
										<div class="divider large-12 columns"></div>
										<div class="large-12 columns submitkeyboard sectionalertbuttonarea" style="text-align: right">
										<input id="userrolesubmitbutton" class="alertbtnyes addkeyboard" type="button" value="Submit" name="" tabindex="103">
										<input id="userroleupdatebutton" class="alertbtnyes updatekeyboard hidedisplay" type="button" value="Submit" name="" tabindex="104" style="display: none;">
										<input id="usercancelbutton" class="alertbtnno cancelkeyboard" type="button" value="Cancel" name="usercancelbutton" tabindex="106">
										</div>
									</div>
								</div>
							</span>
						</span>
					</form>
					</div>
					</div>
					<div class="row mblhidedisplay">&nbsp;</div>
					<div class="large-12 columns paddingbtm padding-space-open-for-form">
						<div class="large-12 columns paddingzero">
							<div class="large-12 columns viewgridcolorstyle borderstyle" style="padding-left:0;padding-right:0;">
								<div class="large-12 columns headerformcaptionstyleforprofile" style="height:auto;padding: 0.1rem 0 0;">
									<span class="large-6 medium-6 small-12 columns end" style="line-height:1.8">
										<span class="large-4 medium-4 small-4 columns" style="line-height:2.5">
											Role :
										</span>
										<span class="large-6 medium-8 small-8 end columns">
											<select class="chzn-select" id="assignuserroleid" name="assignuserroleid" data-placeholder="Select" data-prompt-position="topLeft:14,36">
												<option value=""></option>
												<?php foreach($rolename as $key):?>
													<option value="<?php echo $key->userroleid;?>" data-roleid="<?php echo $key->userroleid;?>"><?php echo $key->userrolename;?></option>
												<?php endforeach;?>
											</select>										
										</span>
									</span>
									<span class="large-6 medium-6 small-12 columns innergridicon righttext small-only-text-center" style="line-height:1.8">
									</span>
								</div>
								<div class="large-12 columns forgetinggridname " id="usersaddgridwidth" style="padding-left:0;padding-right:0;">	
									<table id="usersaddgrid"></table>
									<div id="usersaddgridnav"> </div>					
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row mblhidedisplay">&nbsp;</div>
				<div id="subformspan3" class="hidedisplay hiddensubform padding-space-open-for-form">
					<div class="large-12 columns paddingbtm paddingzero">
						<div class="large-12 columns paddingzero">
							<div class="large-12 columns viewgridcolorstyle borderstyle" style="padding-left:0;padding-right:0;">
								<div class="large-12 columns headerformcaptionstyleforprofile" style="height:auto;padding: 0.1rem 0 0;">
									<span class="large-6 medium-6 small-12 columns" style="line-height:1.8">
										<span class="large-4 medium-4 small-4 columns" style="line-height:2.5">
											Roles:
										</span>
										<span class="large-8 medium-8 small-8 end columns">
											<select class="chzn-select validate[required]" id="moduleroleid" name="moduleroleid" data-placeholder="Select" data-prompt-position="topLeft:14,36">
												<option value=""></option>
												<?php foreach($rolename as $key):?>
													<option value="<?php echo $key->userroleid;?>" data-roleid="<?php echo $key->userroleid;?>"><?php echo $key->userrolename;?></option>
												<?php endforeach;?>
											</select>
										</span>
									</span>
									<span class="large-6 medium-6 small-12 columns innergridicon righttext submitkeyboard small-only-text-center" style="line-height:1.8">
										<span id="moreactionbtn" class="material-icons" title="More Tools">query_builder</span>
										<span id="moremoduleicon" class="" title="Sub Module"><i class="material-icons">settings</i></span>
									</span>
								</div>
								<div class="large-12 forgetinggridname columns paddingzero" id="moduleaddgridwidth">
									<table id="moduleaddgrid"></table>
									<div id="moduleaddgridnav"> </div>												
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="subformspan4" class="hidedisplay hiddensubform padding-space-open-for-form">
					<div class="large-12 columns paddingbtm paddingzero">
						<div class="large-12 columns paddingzero">
							<div class="large-12 columns viewgridcolorstyle paddingzero borderstyle">
								<div class="large-12 columns headerformcaptionstyleforprofile" style="height:auto;padding: 0.1rem 0 0;">
									<span class="large-4 medium-6 small-12 columns" style="line-height:1.8rem">
										<span class="large-4 medium-5 small-5 columns" style="line-height:2.5">
											Roles :
										</span>
										<span class="large-7 medium-7 small-7 end columns">
											<select class="chzn-select validate[required]" id="fieldroleid" name="fieldroleid" data-placeholder="Select" data-prompt-position="topLeft:14,36">
												<option value=""></option>
												<?php foreach($rolename as $key):?>
													<option value="<?php echo $key->userroleid;?>" data-roleid="<?php echo $key->userroleid;?>"><?php echo $key->userrolename;?></option>
												<?php endforeach;?>
											</select>									
										</span>
									</span>
									<span class="large-4 medium-6 small-12 columns end" style="line-height:1.8rem">
										<span class="large-4 medium-5 small-5 columns" style="line-height:2.5">
											Module :
										</span>
										<span class="large-7 medium-7 small-7 end columns">
											<select data-prompt-position="topLeft:14,36" class="chzn-select validate[required]" id="fieldmoduleid" name="fieldmoduleid" data-placeholder="Select" >
												<option value=""></option>
												<?php foreach($module as $key):
													$name=(($key->menuname!="")?$key->menuname:$key->modulename);
												?>
													<option value="<?php echo $key->moduleid;?>" data-moduleid="<?php echo $key->moduleid;?>"><?php echo $name;?></option>
												<?php endforeach;?>
											</select>										
										</span>
									</span>
								</div>
								<div class="large-12 forgetinggridname columns paddingzero " id="fieldsaddgridwidth">
									<table id="fieldsaddgrid"></table>
									<div id="fieldsaddgridnav"> </div>												
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="subformspan5" class="hidedisplay hiddensubform padding-space-open-for-form">
					<div class="large-12 columns paddingbtm paddingzero">
						<div class="large-12 columns paddingzero">
							<div class="large-12 columns viewgridcolorstyle paddingzero borderstyle">
								<div class="large-12 columns headerformcaptionstyleforprofile" style="height:auto;padding: 0.1rem 0 0;">
									<span class="large-3 medium-6 small-12 columns" style="line-height:1.8">
										<span class="large-5 medium-5 small-5 columns" style="line-height:2.5">
											Roles :
										</span>
										<span class="large-7 medium-7 small-7 end columns">
											<select data-prompt-position="topLeft:14,36" class="chzn-select validate[required]" id="valueroleid" name="valueroleid" data-placeholder="Select" >
												<option value=""></option>
												<?php foreach($rolename as $key):?>
													<option value="<?php echo $key->userroleid;?>" data-roleid="<?php echo $key->userroleid;?>"><?php echo $key->userrolename;?></option>
												<?php endforeach;?>
											</select>										
										</span>
									</span>
									<span class="large-4 medium-6 small-12 columns end" style="line-height:1.8">
										<span class="large-4 medium-5 small-5 columns" style="line-height:2.5">
											Module :
										</span>
										<span class="large-6 medium-7 small-7 end columns">
											<select data-prompt-position="topLeft:14,36" class="chzn-select validate[required]" id="valuemoduleid" name="valuemoduleid" data-placeholder="Select" >
												<option value=""></option>
												<?php foreach($module as $key):
														$name=(($key->menuname!="")?$key->menuname:$key->modulename);
												?>
														<option value="<?php echo $key->moduleid;?>" data-moduleid="<?php echo $key->moduleid;?>"><?php echo $name;?></option>
												<?php endforeach;?>
											</select>											
										</span>
									</span>
									<span class="large-3 medium-6 small-12 columns end" style="line-height:1.8">
										<span class="large-6 medium-5 small-5 columns" style="line-height:2.5">
											Fields :
										</span>
										<span class="large-6 medium-7 small-7 end columns">
											<select data-prompt-position="topLeft:14,36" class="chzn-select validate[required]" id="valuefieldid" name="valuefieldid" data-placeholder="Select" >
												<option></option>
											</select>											
										</span>	
										<input type="hidden" name="selvalueid" id="selvalueid" value="" />
									</span>
								</div>
								<div class="large-12 forgetinggridname columns paddingzero " id="fieldvaluesaddgridwidth">
									<table id="fieldvaluesaddgrid"></table>
									<div id="fieldvaluesaddgridnav"> </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--Overlay For More Action Custom Tools-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="customtoolsoverlay" style="overflow-y: scroll;overflow-x: hidden">		
		<div class="large-4 medium-10 large-centered medium-centered columns" style="position:relative;top:150px;">
			<form method="POST" name="customactionform" style="" id="customactionform" action="" enctype="" class="">
				<span id="" class="validationEngineContainer">
					<div class="large-12 columns paddingzero border-modal-8px" style="background:#e0e0e0">
						<div class="large-12 columns end paddingzero">
							<div class="large-12 columns cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;">
								<div class="large-12 columns sectionheaderformcaptionstyle">More Action choice</div>
								<div class="large-12 columns sectionpanel">
								<div class="large-12 medium-12 small-12 columns end" style="">
									<div class="large-12 columns">
										<input id="checkuncheck"  class="filled-in" type="checkbox" tabindex="" name="checkuncheck" value=""><label for="checkuncheck" style="color:#ffffff;"> Check / UnCheck All</label>
									</div>
								</div>
								<div class="large-12 columns customactiondata" style="">
									<!-- custom toolbar icons -->
								</div>
								<div class="large-12 columns" style="">&nbsp;</div>
								</div>
								<div class="divider large-12 columns"></div>
						<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">
							<input type="button" class="alertbtnyes" value="Save" name="custactionverlaybtn" id="custactionverlaybtn">
										<input type="button" class="alertbtnno" value="Cancel" name="cusoverlayclose" id="cusoverlayclose">
						</div>
								<!-- hidden fields-->
								<input type="hidden" name="modulegriddatarowid" id="modulegriddatarowid" value="" />
								<input type="hidden" name="modulegriddataactionid" id="modulegriddataactionid" value="" />
							</div>
						</div>
				    </div>
					
				</span>
			</form>
		</div>
	</div>
</div>
<!--Overlay For Sub Modules Grid-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="submoduleoverlay" style="overflow-y: scroll;overflow-x: hidden">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-10 medium-10 large-centered medium-centered columns">
			<div class="row">&nbsp;</div>
			<div class="row border-modal-8px" style="background:#f5f5f5">
				<div class="large-12 columns headercaptionstyle" style="
     height: 35px !important;line-height: 35px; !important">
					<span class="large-6 medium-6 small-12 columns end">Sub Module
					</span>
					<span class="large-6 medium-6 small-12 columns addformheadericonstyle righttext small-only-text-center" style="top: 4px;">
						<span id="submodmoreactionbtn" class="editiconclass" title="More Tools"><i class="material-icons">settings</i></span>
						<span id="rolesubmodeulesbt" class="" title="Submit"><i class="material-icons">save</i></span>
						<span id="sbumodoverlayclose" class="" title="Close"><i class="material-icons">close</i></span>
					</span>
				</div>
				<div class="large-12 forgetinggridname columns paddingzero" id="submoduleaddgridwidth">
					<table id="submoduleaddgrid"></table>
		<div id="submoduleaddgridnav"> </div>
				</div>
			</div>
		</div>
		<!-- Hidden fields -->
		<input name="mastermodgridrowid" type="hidden" id="mastermodgridrowid" value="1" />
	</div>
</div>
<!--Overlay For More Action Custom Tools-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="subcustomtoolsoverlay" style="overflow-y: scroll;overflow-x: hidden">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-4 medium-6 large-centered medium-centered columns border-modal-8px">
			<form method="POST" name="subcustomactionform" style="" id="subcustomactionform" action="" enctype="" class="">
				<span id="" class="validationEngineContainer">
					<div class="large-12 columns headercaptionstyle headerradius paddingzero">
						<div class="large-6 medium-6 small-6 columns headercaptionleft">
							More Actions
						</div>
						<div class="large-5 medium-6 small-6 columns addformheadericonstyle righttext small-only-text-center">
							<span title="Close" id="subcusoverlayclose"><i class="material-icons" title="Close">close</i></span>
						</div>
					</div>
					<div class="large-12 columns paddingzero" style="background:#e0e0e0">
						<div class="row">&nbsp;</div>
						<div class="large-12 columns end paddingbtm">
							<div class="large-12 columns headerformcaptionstyle">More Action choice</div>
							<div class="large-12 medium-12 small-12 columns end" style="background:#f5f5f5">
								<div class="large-12 columns">
									<input id="subcheckuncheck" class="filled-in" type="checkbox" tabindex="" name="subcheckuncheck" value=""><label for="subcheckuncheck"> Check/UnCheck All</label>
								</div>
							</div>
							<div class="large-12 columns subcustomactiondata" style="background:#f5f5f5">
								<!-- custom toolbar icons -->
							</div>
							<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
							<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
							<div class="large-12 columns" style="background:#f5f5f5">
								<div class="small-4 medium-4 large-4 columns large-centered medium-centered small-centered">
									<input type="button" class="btn formbuttonsalert" value="Save" name="subcustactionverlaybtn" id="subcustactionverlaybtn">	
								</div>
							</div>
							<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
							<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
							<div class="row">&nbsp;</div>
							<!-- hidden fields-->
							<input type="hidden" name="submodulegriddatarowid" id="submodulegriddatarowid" value="" />
							<input type="hidden" name="submodulegriddataactionid" id="submodulegriddataactionid" value="" />
						</div>
					</div>
				</span>
			</form>
		</div>
	</div>
</div>