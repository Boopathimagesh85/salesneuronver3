<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="roledeleteformoverlay">	
		<div class="row sectiontoppadding">&nbsp;</div>
		<div class="large-3 medium-6 small-10 large-centered medium-centered small-centered columns">
		<span id="userroledeletevalidation" class="validationEngineContainer" >
			<div class="alert-panel cleardataform">
				<div class="alertmessagearea">
				<div class="alert-title">Transfer & Delete</div>
				<div class="alert-message">
				<span class="firsttab" tabindex="1000"></span>
				<div class="static-field overlayfield">
					<label>
						Transfer To Role Name<span class="mandatoryfildclass">*</span>
					</label>
					<select class="chzn-select validate[required] ffieldd" id="conformroleid" name="conformroleid" data-prompt-position="bottomLeft" data-placeholder="Select" tabindex="1001">
						<option value=""></option>
						<?php foreach($rolename as $key):?>
							<option value="<?php echo $key->userroleid;?>" data-roleid="<?php echo $key->userroleid;?>">
							<?php echo $key->userroleid;?>-<?php echo $key->userrolename;?></option>
						<?php endforeach;?>
					</select>
					<input type="hidden" name="delroleid" id="delroleid" value="" />
				</div>
				Delete this data?</div>
				</div>
				<div class="alertbuttonarea">
					<input type="button" id="basedeleteyes" name="" tabindex="1002" value="Yes" class="alertbtn" >
					<input type="button" id="basedeleteno" name="" value="No" tabindex="1003" class="flloop alertbtn alertsoverlaybtn" >
					<span class="lasttab" tabindex="1004"></span>
				</div>
			</div>
		</span>
		</div>
	</div>
</div>