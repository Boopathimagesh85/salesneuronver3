<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Roles extends MX_Controller 
{
    //Constructor Function To Load Models
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Roles/Rolesmodel');
		$this->load->view('Base/formfieldgeneration');		
	}
	//Default View 
	public function index() {
		$moduleid = array(247);
		sessionchecker($moduleid);
		$data['filedmodids'] = $moduleid;
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['rolename'] = $this->Basefunctions->simpledropdown('userrole','userroleid,userrolename','userrolename');
		$data['empname'] = $this->Basefunctions->simpledropdown('employee','employeeid,employeename','employeename');
		$data['profile'] = $this->Basefunctions->simpledropdown('profile','profileid,profilename','profilename');
		$data['module'] = $this->Basefunctions->simpledropdownwithmulticond('moduleid','menuname,modulename','module','menutype,moduleprivilegeid','Internal,247');
		$this->load->view('Roles/rolesview',$data);
	}
	//role based module id fetch
	public function fetchrolebasedmodulefetch() {
		$this->Rolesmodel->fetchrolebasedmodulefetchmodel();
	}
	//user role create
	public function newdatacreate() {
		$this->Rolesmodel->newdatacreatemodel();
	}
	//roles main grid view
	public function userrolegriddatafetch() {
		$grid=$this->Basefunctions->getpagination();
		$result=$this->Rolesmodel->userrolegriddatafetchgridxmlview($grid['sidx'],$grid['sord'],$grid['start'],$grid['limit'],$grid['wh']);
        $pageinfo=$this->Basefunctions->page();
		header("Content-type: text/xml;charset=utf-8");
		$s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .=  "<rows>";
        $s .= "<page>".$_GET['page']."</page>";
        $s .= "<total>".$pageinfo['totalpages']."</total>";
        $s .= "<records>".$pageinfo['count']."</records>";
		//ob_clean();
		foreach($result->result() as $row) {
			$s .= "<row id='". $row->userroleid."'>";
			$s .= "<cell><![CDATA[".$row->userroleid."]]></cell>";
			$s .= "<cell><![CDATA[".$row->prname."]]></cell>";
			$s .= "<cell><![CDATA[".$row->urname."]]></cell>";
			$s .= "<cell><![CDATA[".$row->profilename."]]></cell>";
			$s .= "<cell><![CDATA[".$row->description."]]></cell>";
			$s .= "<cell><![CDATA[".$row->statusname."]]></cell>";
			$s .= "</row>";
		}
		$s .= "</rows>";
		echo $s;
	}
	//roles data fetch
	public function fetchformdataeditdetails() {
		$this->Rolesmodel->fetchformdataeditdetailsmodel();
	}
	//role update function
	public function dataupdatefunction() {
		$this->Rolesmodel->dataupdatefunctionmodel();
	}
	//role Delete function
	public function roledeletefunction() {
		$this->Rolesmodel->roledeletefunctionmodel();
	}
	//roles main grid view
	public function rolebasedusernameshow() {
		$grid=$this->Basefunctions->getpagination();
		$result=$this->Rolesmodel->rolebasedusernameshowgridxmlview($grid['sidx'],$grid['sord'],$grid['start'],$grid['limit'],$grid['wh']);
        $pageinfo=$this->Basefunctions->page();
		header("Content-type: text/xml;charset=utf-8");
		$s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .=  "<rows>";
        $s .= "<page>".$_GET['page']."</page>";
        $s .= "<total>".$pageinfo['totalpages']."</total>";
        $s .= "<records>".$pageinfo['count']."</records>";
		//ob_clean();
		foreach($result->result() as $row) {
			$s .= "<row id='". $row->employeeid."'>";
			$s .= "<cell><![CDATA[".$row->employeeid."]]></cell>";
			$s .= "<cell><![CDATA[".$row->employeename."]]></cell>";
			$s .= "<cell><![CDATA[".$row->lastname."]]></cell>";
			$s .= "<cell><![CDATA[".$row->userrolename."]]></cell>";
			$s .= "<cell><![CDATA[".$row->statusname."]]></cell>";
			$s .= "</row>";
		}
		$s .= "</rows>";
		echo $s;
	}
	//user role based drop down value fetch
	public function fetchdddatawithcond() {
		$this->Rolesmodel->fetchdddatawithcondmodel();
	}
	//on role delete transfer dd data load
	public function deleteroleddvalfetch() {
		$this->Rolesmodel->deleteroleddvalfetchmodel();
	}
	//user role assign add
	public function userroleassignadd() {
		$this->Rolesmodel->userroleassignaddmodel();
	}
	//Module data view
	public function rolebasedmodulelist() {
		$roleid = $_GET['roleid'];
		$grid=$this->Basefunctions->getpagination();
		$result=$this->Rolesmodel->rolebasedmodulelistmodel($roleid,$grid['wh']);
		$pageinfo=$this->Basefunctions->page();
		header("Content-type: text/xml;charset=utf-8");
		$s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .=  "<rows>";
        $s .= "<page>".$_GET['page']."</page>";
        $s .= "<total>".$pageinfo['totalpages']."</total>";
        $s .= "<records>".$pageinfo['count']."</records>";
		//ob_clean();
		$check = $this->Rolesmodel->checkroleidinmoduleinfo($roleid);
		if($check == 'TRUE') {
			$moduleid = $result;
			$exmod = $moduleid;
			$defmodid = $this->Rolesmodel->defaultmoduleidget($grid['wh']);
			$count = count($defmodid);
			for($i=0;$i<$count;$i++) {
				//tool bar id get
				$usertoolbarid = $this->Rolesmodel->tollbaridgetbasedonroles($defmodid[$i],$roleid);//user tool bar privilege
				$moduletoolbarid = $this->Rolesmodel->moduletollbaridget($defmodid[$i]);//tool bar group 
				//user selected action
				$modulename = $this->Rolesmodel->modulenameget($defmodid[$i]);
				$dashmod = $this->Rolesmodel->checkdashboardid($defmodid[$i]);
				$useraction = $this->Rolesmodel->actionnamefetch($usertoolbarid,$exmod);
				$moduleaction = $this->Rolesmodel->actionnamefetch($moduletoolbarid,$exmod);
				$actionvalue = $this->Rolesmodel->modulebasedactionvalue($useraction,$moduleaction);
				$modstatus = 'false';
				$status = 'Un Assigned';
				if(in_array($defmodid[$i],$exmod)) {
					$modstatus = 'true';
					$status = 'Module Assigned';
				} else {
					$modstatus = 'false';
					$status = 'Module Un Assigned';
				}
				$s .= "<row id='".$defmodid[$i]."'>";
				$s .= "<cell><![CDATA[".$modulename['catname']."]]></cell>";
				$s .= "<cell><![CDATA[".$modulename['modname']." ".$dashmod."]]></cell>";
				$s .= "<cell><![CDATA[".$modstatus."]]></cell>";
				$j=0;
				foreach($actionvalue as $actval) {
					$actions = ( ($modstatus == 'false' && $actval != 'empty') ? 'false' : $actval );
					if($j!=4) {
						$s .= "<cell><![CDATA[".$actions."]]></cell>";
					}
					$j++;
				}
				if($moduleaction[4] != '') {
					$cusstatus = ( ($useraction[4]!="" && $modstatus != 'false') ? 'true':'false');
					$s .= "<cell><![CDATA[".$cusstatus."]]></cell>";
				} else {
					$s .= "<cell><![CDATA[empty]]></cell>";
				}
				$s .= "<cell><![CDATA[".$status."]]></cell>";
				$s .= ( ($moduleaction[4]!="" ) ? "<cell><![CDATA[".$useraction[4]."||".$moduleaction[4]."]]></cell>" : "<cell><![CDATA[ ]]></cell>" );
				$s .= "<cell><![CDATA[".$dashmod."]]></cell>";
				$s .= "<cell><![CDATA[]]></cell>";
				$s .= "</row>";
			}
		} else {
			foreach($result->result() as $row) {
				$moduleid = $row->moduleid;
				$profileid = $row->profileid;
				$exmod = explode(',',$moduleid);
				$defmodid = $this->Rolesmodel->defaultmoduleidget($grid['wh']);
				$count = count($defmodid);
				for($i=0;$i<$count;$i++) {
					$usertoolbarid = $this->Rolesmodel->profiletollbaridget($defmodid[$i],$profileid);//user tool bar privilege
					$moduletoolbarid = $this->Rolesmodel->moduletollbaridget($defmodid[$i]);//toolbar group 
					$modulename = $this->Rolesmodel->modulenameget($defmodid[$i]);//module name get
					$dashmod = $this->Rolesmodel->checkdashboardid($defmodid[$i]);
					$useraction = $this->Rolesmodel->actionnamefetch($usertoolbarid,$defmodid);//user selected module toolbar id
					$moduleaction = $this->Rolesmodel->actionnamefetch($moduletoolbarid,$defmodid);//default module toolbar id
					$actionvalue = $this->Rolesmodel->modulebasedactionvalue($useraction,$moduleaction);
					$modstatus = 'false';
					if(in_array($defmodid[$i],$exmod)) {
						$modstatus = 'true';
						$status = 'Profile Assigned';
					} else {
						$modstatus = 'false';
						$status = 'Profile Un Assigned';
					}
					$s .= "<row id='".$defmodid[$i]."'>";
					$s .= "<cell><![CDATA[".$modulename['catname']."]]></cell>";
					$s .= "<cell><![CDATA[".$modulename['modname']." ".$dashmod."]]></cell>";
					$s .= "<cell><![CDATA[".$modstatus."]]></cell>";
					$j=0;
					foreach($actionvalue as $actval) {
						$actions = ( ($modstatus == 'false' && $actval != 'empty') ? 'false' : $actval );
						if($j!=4) {
							$s .= "<cell><![CDATA[".$actions."]]></cell>";
						}
						$j++;
					}
					if($moduleaction[4] != '') {
						$cusstatus = ( ($useraction[4]!="" && $modstatus != 'false') ? 'true':'false');
						$s .= "<cell><![CDATA[".$cusstatus."]]></cell>";
					} else {
						$s .= "<cell><![CDATA[empty]]></cell>";
					}
					$s .= "<cell><![CDATA[".$status."]]></cell>";
					$s .= ( ($moduleaction[4]!="" ) ? "<cell><![CDATA[".$useraction[4]."||".$moduleaction[4]."]]></cell>" : "<cell><![CDATA[ ]]></cell>" );
					$s .= "<cell><![CDATA[".$dashmod."]]></cell>";
					$s .= "<cell><![CDATA[]]></cell>";
					$s .= "</row>";
				}
			}
		}
		$s .= "</rows>";
		echo $s;
	}
	//role based module submit
	public function rolemodulesubmit() {
		$this->Rolesmodel->rolemodulesubmitmodel();
	}
	//Module based Filed data view
	public function modulebasedfieldslist() {
		$roleid = $_GET['roleid'];
		$moduleid = $_GET['moduleid'];
		$dashboardmodid = $this->Rolesmodel->fetchdashboardmoduleid($moduleid);
		$moduleids = ( ($dashboardmodid!=1) ? $dashboardmodid : $_GET['moduleid'] );
		$modid = explode(',',$moduleids);
		$grid=$this->Basefunctions->getpagination();
		$result=$this->Rolesmodel->modulebasedfieldslistxmlview($roleid,$modid);
        $pageinfo=$this->Basefunctions->page();
		header("Content-type: text/xml;charset=utf-8");
		$s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .=  "<rows>";
        $s .= "<page>".$_GET['page']."</page>";
        $s .= "<total>".$pageinfo['totalpages']."</total>";
        $s .= "<records>".$pageinfo['count']."</records>";
		//ob_clean();
		foreach($result->result() as $row) {
			$check = $this->Rolesmodel->checkuserrolemodulefield($modid,$roleid,$row->modulefieldid);
			$fstatus = ($check == 'TRUE' ? 'Assigned':'Un Assigned');
			$fassign = ($check == 'TRUE' ? 'true':'false');
			if($check == 'TRUE') {
				$restrict = $row->fieldrestrict;
				$modfiledmode = $this->Rolesmodel->fetchuserrolebasedmodulefieldmode($modid,$roleid,$row->modulefieldid);
				if($restrict != 'Yes') {
					$read = $modfiledmode['readonly'];
					$visible = $modfiledmode['active'];
					$ronly = ($read == 'Yes' ? 'true':'false');
					$active = ($visible == 'Yes' ? 'true':'false');
				} else {
					$ronly = "empty";
					$active = "empty";
				}
			} else {
				$restrict = $row->fieldrestrict;
				$modfiledmode = $this->Rolesmodel->fetchuserrolebasedmodulefieldmode($modid,$roleid,$row->modulefieldid);
				if($restrict != 'Yes') {
					$read = $modfiledmode['readonly'];
					$visible = $modfiledmode['active'];
					$ronly = ($read == 'Yes' ? 'true':'false');
					$active = ($visible == 'Yes' ? 'true':'false');
				} else {
					$ronly = "empty";
					$active = "empty";
				}
			}
			$s .= "<row id='".$row->modulefieldid."'>";
			$s .= "<cell><![CDATA[".$row->modulefieldid."]]></cell>";
			$s .= "<cell><![CDATA[".$row->moduletabid."]]></cell>";
			$s .= "<cell><![CDATA[".$row->fieldlabel."]]></cell>";
			$s .= "<cell><![CDATA[".$fassign."]]></cell>";
			$s .= "<cell><![CDATA[".$active."]]></cell>";
			$s .= "<cell><![CDATA[".$ronly."]]></cell>";
			$s .= "<cell><![CDATA[".$fstatus."]]></cell>";
			$s .= "</row>";
		}
		$s .= "</rows>";
		echo $s;
	}
	//role- module based field insertion and updation
	public function rolemodulefieldsubmitbtn() {
		$this->Rolesmodel->rolemodulefieldsubmitbtnmodel();
	}
	//value drop down load function model
	public function valuedropdownloadfun() {
		$this->Rolesmodel->valuedropdownloadfunmodel();
	}
	//Module based Filed data view
	public function modulefieldvalueget() {
		$roleid = $_GET['roleid'];
		$modid = $_GET['modid'];
		$tablefieldid = $_GET['tableid'];
		$datatableid = $_GET['tableid'];
		$modulefieldid = $_GET['modulefieldid'];
		$result=$this->Rolesmodel->modulefieldvaluegetxmlview($roleid,$modid,$datatableid);
        $pageinfo=$this->Basefunctions->page();
		header("Content-type: text/xml;charset=utf-8");
		$s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .=  "<rows>";
        $s .= "<page>".$_GET['page']."</page>";
        $s .= "<total>".$pageinfo['totalpages']."</total>";
        $s .= "<records>".$pageinfo['count']."</records>";
		//ob_clean();
		if($result != 'false') {
			$table = substr($tablefieldid, 0, -2);
			foreach($result->result() as $row) {
				$check = $this->Rolesmodel->fieldvaluecheck($roleid,$modid,$table,$row->fieldid);
				$status = $row->status;
				$fvisible = ($check == 'TRUE' ? 'true':'false');
				$fstatus = ($check == 'TRUE' ? 'Assigned':'Un Assigned');
				$s .= "<row id='".$row->fieldid."'>";
				$s .= "<cell><![CDATA[".$row->fieldid."]]></cell>";
				$s .= "<cell><![CDATA[".$row->fieldname."]]></cell>";
				$s .= "<cell><![CDATA[".$fvisible."]]></cell>";
				$s .= "<cell><![CDATA[".$fstatus."]]></cell>";
				$s .= "</row>";
			}
		}
		$s .= "</rows>";
		echo $s;
	}
	//assign field values
	public function modulevaluesubmit() {
		$this->Rolesmodel->modulevaluesubmitmodel();
	}
	//role unique name check
	public function roleuniquenamecheck() {
		$this->Rolesmodel->roleuniquenamecheckmodel();
	}
	//custom toolbar information get
	public function customtoolbarinfoget() {
		$datas = $this->Rolesmodel->customtoolbarinfogetmodel();
		echo $datas;
	}
	//sub module custom toolbar information get
	public function subcustomtoolbarinfoget() {
		$datas = $this->Rolesmodel->subcustomtoolbarinfogetmodel();
		echo $datas;
	}
	//Module data view
	public function rolebasedsubmodulelist() {
		$roleid = $_GET['rid'];
		$parmoduleid = $_GET['mid'];
		$profileid = $this->Rolesmodel->profileidget($roleid);
		$dashboradmodids=$this->Rolesmodel->rolebaseddahsmodulelistmodel($roleid,$profileid);
		$pageinfo=$this->Basefunctions->page();
		header("Content-type: text/xml;charset=utf-8");
		$s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .=  "<rows>";
        $s .= "<page>".$_GET['page']."</page>";
        $s .= "<total>".$pageinfo['totalpages']."</total>";
        $s .= "<records>".$pageinfo['count']."</records>";
		//ob_clean();
		$defdashmodid = $this->Rolesmodel->fetchdashboardmoduleid($parmoduleid); //default sub moduleids
		$check = $this->Rolesmodel->checkroleidinmoduleinfo($roleid);
		if($check == 'TRUE') {
			$submodids= explode(',',$defdashmodid);
			$count = count($submodids);
			for($i=0;$i<$count;$i++) {
				//toolbar id get
				$usertoolbarid = $this->Rolesmodel->tollbaridgetbasedonroles($submodids[$i],$roleid);//user toolbar privilege
				$moduletoolbarid = $this->Rolesmodel->moduletollbaridget($submodids[$i]);//toolbar group 
				//user selected action
				$modulename = $this->Rolesmodel->modulenameget($submodids[$i]);
				$useraction = $this->Rolesmodel->actionnamefetch($usertoolbarid,$submodids);
				$moduleaction = $this->Rolesmodel->actionnamefetch($moduletoolbarid,$submodids);
				$actionvalue = $this->Rolesmodel->modulebasedactionvalue($useraction,$moduleaction);
				$modstatus = 'false';
				$status = 'Un Assigned';
				if(in_array($submodids[$i],$dashboradmodids)) {
					$modstatus = 'true';
					$status = 'Module Assigned';
				} else {
					$modstatus = 'false';
					$status = 'Module Un Assigned';
				}
				$s .= "<row id='".$submodids[$i]."'>";
				$s .= "<cell><![CDATA[".$submodids[$i]."]]></cell>";
				$s .= "<cell><![CDATA[".$modulename['catname']."]]></cell>";
				$s .= "<cell><![CDATA[".$modulename['modname']."]]></cell>";
				$s .= "<cell><![CDATA[".$modstatus."]]></cell>";
				$j=0;
				foreach($actionvalue as $actval) {
					$actions = ( ($modstatus == 'false' && $actval != 'empty') ? 'false' : $actval );
					if($j!=4) {
						$s .= "<cell><![CDATA[".$actions."]]></cell>";
					}
					$j++;
				}
				if($moduleaction[4] != '') {
					$cusstatus = ( ($useraction[4]!="" && $modstatus != 'false') ? 'true':'false');
					$s .= "<cell><![CDATA[".$cusstatus."]]></cell>";
				} else {
					$s .= "<cell><![CDATA[empty]]></cell>";
				}
				$s .= "<cell><![CDATA[".$status."]]></cell>";
				$s .= ( ($moduleaction[4]!="" ) ? "<cell><![CDATA[".$useraction[4]."||".$moduleaction[4]."]]></cell>" : "<cell><![CDATA[ ]]></cell>" );
				$s .= "</row>";
			}
		} else {
			$submodids= explode(',',$defdashmodid);
			$count = count($submodids);
			for($i=0;$i<$count;$i++) {
				$usertoolbarid = $this->Rolesmodel->profiletollbaridget($submodids[$i],$profileid);//user toolbar privilege
				$moduletoolbarid = $this->Rolesmodel->moduletollbaridget($submodids[$i]);//toolbar group 
				$modulename = $this->Rolesmodel->modulenameget($submodids[$i]);//module name get
				$useraction = $this->Rolesmodel->actionnamefetch($usertoolbarid,$submodids);//user selected module toolbar id
				$moduleaction = $this->Rolesmodel->actionnamefetch($moduletoolbarid,$submodids);//default module toolbar id
				$actionvalue = $this->Rolesmodel->modulebasedactionvalue($useraction,$moduleaction);
				$modstatus = 'false';
				if(in_array($submodids[$i],$dashboradmodids)) {
					$modstatus = 'true';
					$status = 'Profile Assigned';
				} else {
					$modstatus = 'false';
					$status = 'Profile Un Assigned';
				}
				$s .= "<row id='".$submodids[$i]."'>";
				$s .= "<cell><![CDATA[".$submodids[$i]."]]></cell>";
				$s .= "<cell><![CDATA[".$modulename['catname']."]]></cell>";
				$s .= "<cell><![CDATA[".$modulename['modname']."]]></cell>";
				$s .= "<cell><![CDATA[".$modstatus."]]></cell>";
				$j=0;
				foreach($actionvalue as $actval) {
					$actions = ( ($modstatus == 'false' && $actval != 'empty') ? 'false' : $actval );
					if($j!=4) {
						$s .= "<cell><![CDATA[".$actions."]]></cell>";
					}
					$j++;
				}
				if($moduleaction[4] != '') {
					$cusstatus = ( ($useraction[4]!="" && $modstatus != 'false') ? 'true':'false');
					$s .= "<cell><![CDATA[".$cusstatus."]]></cell>";
				} else {
					$s .= "<cell><![CDATA[empty]]></cell>";
				}
				$s .= "<cell><![CDATA[".$status."]]></cell>";
				$s .= ( ($moduleaction[4]!="" ) ? "<cell><![CDATA[".$useraction[4]."||".$moduleaction[4]."]]></cell>" : "<cell><![CDATA[ ]]></cell>" );
				$s .= "</row>";
			}
		}
		$s .= "</rows>";
		echo $s;
	}
}