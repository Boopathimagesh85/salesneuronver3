<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Schememodel extends CI_Model
{
   private $schememoduleid = 49;	
	public function __construct()
    {
        parent::__construct();
    }
	// scheme create
	public function schemecreate() {	
		$chitreg=array();
		$chitentry=array();
		if(isset($_POST['purityid'])) {
			$_POST['purityid']=$_POST['purityid']; 
		} else {
		   $_POST['purityid']=1;
		}
		if($_POST['purityid'] == 0) {
		    $_POST['purityid']=1;
		}
		if(isset($_POST['chitbooktokenpattern'])) {
			$_POST['chitbooktokenpattern'] = $_POST['chitbooktokenpattern']; 
		} else {
			$_POST['chitbooktokenpattern'] = '';
		}
		if(isset($_POST['intrestmodeid'])) {
			$_POST['intrestmodeid'] = $_POST['intrestmodeid']; 
		} else {
			$_POST['intrestmodeid'] = 1;
		}
	    if($_POST['chitreg'] != '') {
		     $chitreg=explode('-',$_POST['chitreg']); 
			 $regtemplateid=$chitreg[0];
			 $regtemplatetype=$chitreg[1];
			 $regtemplatename=$chitreg[2];
		} else {
			 $regtemplateid=0;
			 $regtemplatetype=0;
			 $regtemplatename='No Print';
		}
		if($_POST['chitentry'] != '') {
	         $chitentry=explode('-',$_POST['chitentry']);
			 $entrytemplateid=$chitentry[0];
			 $entrytemplatetype=$chitentry[1];
			 $entrytemplatename=$chitentry[2];
		} else {
			 $entrytemplateid=0;
			 $entrytemplatetype=0;
			 $entrytemplatename='No Print';
		}
		if(isset($_POST['averagemonths'])) {
			$_POST['averagemonths'] = $_POST['averagemonths']; 
		} else {
			$_POST['averagemonths'] = '0';
		}
 		$insert=array(
						'date'=>$this->Basefunctions->ymd_format($_POST['schemedate']),
						'chitschemename'=>$_POST['schemename'],
						'schemeprefix'=>$_POST['prefixvalue'],
                        'chitlookupsid'=>$_POST['chitype'],	
                        'purityid'=>$_POST['purityid'],	
						'noofmonths'=>$_POST['noofmonths'],	
						'luckydraw'=>$_POST['luckydraw'],	
						'baseamount'=>$_POST['baseamount'],	
                        'gift'=>$_POST['gift'],
						'chitbooknopattern'=>$_POST['chitbooknopattern'],	
                        'chitbooktokennopattern'=>$_POST['chitbooktokenpattern'],						
						'chitamounts'=>$_POST['chitamount'],
						'comments'=>$_POST['description'],						
						'regtemplatetype'=>$regtemplatetype,						
						'regtemplateid'=>$regtemplateid,						
						'regtemplatename'=>$regtemplatename,						
						'entrytemplateid'=>$entrytemplateid,						
						'entrytemplatetype'=>$entrytemplatetype,						
						'entrytemplatename'=>$entrytemplatename,
						'intrestmodeid'=>$_POST['intrestmodeid'],
						'interestrate'=>$_POST['interestrate'],
						'refundrate'=>$_POST['refundrate'],
						'minnoofmonths'=>$_POST['minnoofmonths'],
						'maxschemeamount'=>$_POST['maxschemeamount'],
						'accountid'=>$_POST['accountid'],
						'schemeapplyfromdate'=>$this->Basefunctions->ymd_format($_POST['schemeapplyfromdate']),
						'schemeapplytodate'=>$this->Basefunctions->ymd_format($_POST['schemeapplytodate']),
						'memberslimit'=>$_POST['memberslimit'],
						'variablemodeid'=>$_POST['variablemodeid'],
						'averagemonths'=>$_POST['averagemonths'],
 						'industryid'=>$this->Basefunctions->industryid,
 						'branchid'=>$this->Basefunctions->branchid,
						'status'=>$this->Basefunctions->activestatus
					);
		$insert = array_merge($insert,$this->Crudmodel->defaultvalueget());
	    $this->db->insert('chitscheme',$insert);
		$primaryid = $this->db->insert_id();
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' created Scheme - '.$_POST['schemename'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,$this->schememoduleid);
		echo 'SUCCESS';
	}
	/**	*scheme retrieve */
	public function schemeretrive()
	{
		$id=$_GET['primaryid'];
		$this->db->select('chitscheme.chitschemeid,chitscheme.date,chitscheme.chitschemename,chitscheme.schemeprefix,chitscheme.chitlookupsid,chitscheme.purityid,chitscheme.noofmonths,chitscheme.luckydraw,chitscheme.gift,chitscheme.baseamount,chitscheme.chitamounts,chitscheme.comments,chitscheme.chitbooknopattern,chitscheme.chitbooktokennopattern,chitscheme.regtemplatetype,chitscheme.regtemplateid,chitscheme.regtemplatename,chitscheme.entrytemplateid,chitscheme.entrytemplatetype,chitscheme.entrytemplatename,chitscheme.intrestmodeid,chitscheme.interestrate,chitscheme.minnoofmonths,chitscheme.maxschemeamount,chitscheme.accountid,chitscheme.schemeapplyfromdate,chitscheme.schemeapplytodate,chitscheme.memberslimit,chitscheme.variablemodeid,chitscheme.averagemonths,chitscheme.refundrate');
		$this->db->from('chitscheme');
		$this->db->where('chitschemeid',$id);
		$info=$this->db->get()->row();		
		$jsonarray=array(
							'schemedate'=>$this->Basefunctions->ymd_format($info->date),
							'schemename'=>$info->chitschemename,
							'prefixvalue'=>$info->schemeprefix,
							'chitype'=>$info->chitlookupsid,
							'purityid'=>$info->purityid,
							'noofmonths'=>$info->noofmonths,
							'luckydraw'=>$info->luckydraw,
							'baseamount'=>$info->baseamount,
							'gift'=>$info->gift,
							'chitbooknopattern'=>$info->chitbooknopattern,	
                            'chitbooktokennopattern'=>$info->chitbooktokennopattern,	
							'amount'=>$info->chitamounts,
							'description'=>$info->comments,
							'chitschemeid'=>$info->chitschemeid,
							'chitschemeprimaryname'=>$info->chitschemename,
							'chitschemesecondaryname'=>$info->schemeprefix,
							'chitregtemplateid'=>$info->regtemplateid,
							'chitentrytemplateid'=>$info->entrytemplateid,
							'intrestmodeid'=>$info->intrestmodeid,
							'interestrate'=>$info->interestrate,
							'refundrate'=>$info->refundrate,
							'minnoofmonths'=>$info->minnoofmonths,
							'maxschemeamount'=>$info->maxschemeamount,
							'accountid'=>$info->accountid,
							'schemeapplyfromdate'=>$this->Basefunctions->ymd_format($info->schemeapplyfromdate),
							'schemeapplytodate'=>$this->Basefunctions->ymd_format($info->schemeapplytodate),
							'memberslimit'=>$info->memberslimit,
							'variablemodeid'=>$info->variablemodeid,
							'averagemonths'=>$info->averagemonths,
							'ischitbook'=>$this->Basefunctions->generalinformaion('chitbook','chitschemeid','chitschemeid',$id)
						);
		echo json_encode($jsonarray);
	}
	/** *gift retrieve	*/
	public function giftretrive()
	{
		$id=$_GET['primaryid'];
		$this->db->select('chitgift.chitgiftid,chitgift.giftdate,chitgift.chitschemeid,chitgift.schemeamount,chitgift.chitlookupsid,chitgift.giftamount,chitgift.monthstart,chitgift.monthend,chitgift.chitgiftname,chitgift.giftcount,chitgift.giftdescription,chitgift.gifttemplateid');
		$this->db->from('chitgift');
		$this->db->where('chitgiftid',$id);
		$info=$this->db->get()->row();
     	$jsonarray=array(
							'giftdate'=>$this->Basefunctions->ymd_format($info->giftdate),
							'schemeid'=>$info->chitschemeid,
							'amountgift'=>$info->schemeamount,
							'typegift'=>$info->chitlookupsid,
							'prizeamount'=>$info->giftamount,
							'monthstart'=>$info->monthstart,
							'monthend'=>$info->monthend,
							'prizename'=>$info->chitgiftname,
							'noofprizes'=>$info->giftcount,
							'prizedescription'=>$info->giftdescription,
							'giftentrytemplateid'=>$info->gifttemplateid
						);
		echo json_encode($jsonarray);
	}
	// get  scheme amount
	public function schemeamountload() {
		$schemeamounts = $this->db->distinct()->select('chitamounts')->from('chitscheme')->where('status',$this->Basefunctions->activestatus)->get();
		$i=0;
		foreach($schemeamounts->result() as $schemeamountsdata) {
			$scheme_amount=explode(',',$schemeamountsdata->chitamounts);
			 $scheme_amount_count=count($scheme_amount); 
			for($j=0; $j < $scheme_amount_count; $j++){ 
				$dataschemenames[$i] =$scheme_amount[$j];
				$i++;
			}
		}
		$datas = array_map("unserialize", array_unique(array_map("serialize", $dataschemenames)));
		echo json_encode($datas);
		
	}
	// get  prize description
	public function prizedescload() {
		
		$giftdescription = $this->db->distinct()->select('giftdescription')->from('chitgift')->where('status',$this->Basefunctions->activestatus)->get();
		$i=0;
		foreach($giftdescription->result() as $giftdescriptiondata) {
			$gift_description=explode(',',$giftdescriptiondata->giftdescription);
			 $gift_description_count=count($gift_description); 
			for($j=0; $j < $gift_description_count; $j++){ 
				$giftdescriptionnames[$i] =$gift_description[$j];
				$i++;
			}
		}
		$datas_giftdescriptionnames = array_map("unserialize", array_unique(array_map("serialize", $giftdescriptionnames)));
		echo json_encode($datas_giftdescriptionnames);
	}
	/** *update scheme	*/
	public function schemeupdate()
	{
		if(isset($_POST['purityid'])) {
			$_POST['purityid']=$_POST['purityid']; 
		} else {
			$_POST['purityid']=1;
		}
		if($_POST['purityid'] == 0) {
			$_POST['purityid']=1;
		}
		if(isset($_POST['chitbooktokenpattern'])) {
			$_POST['chitbooktokenpattern']=$_POST['chitbooktokenpattern']; 
		} else {
			$_POST['chitbooktokenpattern']='';
		}
		if(isset($_POST['intrestmodeid'])) {
			$_POST['intrestmodeid']=$_POST['intrestmodeid']; 
		} else {
			$_POST['intrestmodeid']=1;
		}
		$id=$_POST['primaryid'];
		if($_POST['chitreg'] != '') {
		     $chitreg=explode('-',$_POST['chitreg']); 
			 $regtemplateid=$chitreg[0];
			 $regtemplatetype=$chitreg[1];
			 $regtemplatename=$chitreg[2];
		}
		else{
			 $regtemplateid=0;
			 $regtemplatetype=0;
			 $regtemplatename='No Print';
		}
		if($_POST['chitentry'] != '') {
	         $chitentry=explode('-',$_POST['chitentry']);
			 $entrytemplateid=$chitentry[0];
			 $entrytemplatetype=$chitentry[1];
			 $entrytemplatename=$chitentry[2];
		}else{
			$entrytemplateid=0;
			 $entrytemplatetype=0;
			 $entrytemplatename='No Print';
		}
		if(isset($_POST['averagemonths'])) {
			$_POST['averagemonths'] = $_POST['averagemonths']; 
		} else {
			$_POST['averagemonths'] = '0';
		}
		$update = array(
				'date'=>$this->Basefunctions->ymd_format($_POST['schemedate']),
				'chitschemename'=>$_POST['schemename'],
				'schemeprefix'=>$_POST['prefixvalue'],
				'chitlookupsid'=>$_POST['chitype'],
				'purityid'=>$_POST['purityid'],						
				'noofmonths'=>$_POST['noofmonths'],	
				'luckydraw'=>$_POST['luckydraw'],	
				'baseamount'=>$_POST['baseamount'],	
				'gift'=>$_POST['gift'],	
				'chitbooknopattern'=>$_POST['chitbooknopattern'],	
				'chitbooktokennopattern'=>$_POST['chitbooktokenpattern'],
				'chitamounts'=>$_POST['chitamount'],
				'comments'=>$_POST['description'],
				'regtemplatetype'=>$regtemplatetype,						
				'regtemplateid'=>$regtemplateid,						
				'regtemplatename'=>$regtemplatename,						
				'entrytemplateid'=>$entrytemplateid,						
				'entrytemplatetype'=>$entrytemplatetype,						
				'entrytemplatename'=>$entrytemplatename,
				'intrestmodeid'=>$_POST['intrestmodeid'],
				'interestrate'=>$_POST['interestrate'],
				'refundrate'=>$_POST['refundrate'],
				'minnoofmonths'=>$_POST['minnoofmonths'],
				'maxschemeamount'=>$_POST['maxschemeamount'],
				'accountid'=>$_POST['accountid'],
				'schemeapplyfromdate'=>$this->Basefunctions->ymd_format($_POST['schemeapplyfromdate']),
				'schemeapplytodate'=>$this->Basefunctions->ymd_format($_POST['schemeapplytodate']),
				'memberslimit'=>$_POST['memberslimit'],
				'variablemodeid'=>$_POST['variablemodeid'],
				'averagemonths'=>$_POST['averagemonths'],
				'branchid'=>$this->Basefunctions->branchid,
				'industryid'=>$this->Basefunctions->industryid
			 );
		$update = array_merge($update,$this->Crudmodel->updatedefaultvalueget());
	    //update scheme
		$this->db->where('chitschemeid',$id);
		$this->db->update('chitscheme',$update);
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' updated Scheme - '.$_POST['schemename'].'';
		$this->Basefunctions->notificationcontentadd($id,'Updated',$activity ,$userid,$this->schememoduleid);
		echo 'SUCCESS';
	}
	/**	*update gift	*/
	public function giftupdate()
	{
		$templateid=0;
		$templatetype=0;
		$templatename='';
		$id=$_POST['primaryid'];
		/* if($_POST['prizetemp'] != '') {
		     $prizereg=explode('-',$_POST['prizetemp']); 
			 $templateid=$prizereg[0];
			 $templatetype=$prizereg[1];
			 $templatename=$prizereg[2];
		}
		else{ */
			 $templateid=0;
			 $templatetype=0;
			 $templatename='No Print';
		//}
		if(empty($_POST['noofprizes'])){
			$_POST['noofprizes'] = 0;
		}else{
			$_POST['noofprizes'] = $_POST['noofprizes'];
		}
		$update = array(
						'giftdate'=>$this->Basefunctions->ymd_format($_POST['giftdate']),
						'chitschemeid'=>$_POST['schemeid'],
						'schemeamount'=>$_POST['amountgift'],
                        'chitlookupsid'=>$_POST['typegift'],	
						'giftamount'=>$_POST['prizeamount'],
						'chitgiftname'=>$_POST['prizename'],	
						'giftcount'=>$_POST['noofprizes'],
						'giftdescription'=>$_POST['prizedescription'],
						'gifttemplateid'=>$templateid,						
						'gifttemplatetype'=>$templatetype,						
						'gifttemplatename'=>$templatename,
						'industryid'=>$this->Basefunctions->industryid
				 );
		$update = array_merge($update,$this->Crudmodel->updatedefaultvalueget());
	    //update scheme
		$this->db->where('chitgiftid',$id);
		$this->db->update('chitgift',$update);
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' updated scheme gift/prize';
		$this->Basefunctions->notificationcontentadd($id,'Updated',$activity ,$userid,$this->schememoduleid);
		echo 'SUCCESS';
	}
	/**	*inactivate-delete scheme grid	*/
	public function schemedelete()
	{
		$id=$_GET['primaryid'];
		//inactivate metal
		$delete = $this->Basefunctions->delete_log();
		//audit-log
		$metal = $this->Basefunctions->singlefieldfetch('chitschemename','chitschemeid','chitscheme',$id);
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' deleted Scheme - '.$metal.'';
		$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,$this->schememoduleid);
		//audit-log
		$this->db->where('chitschemeid',$id);
		$this->db->update('chitscheme',$delete);
		echo 'SUCCESS';
	}
	/**	*inactivate-delete gift grid	*/
	public function giftdelete()
	{
		$id=$_GET['primaryid'];
		//inactivate metal
		$delete = $this->Basefunctions->delete_log();
		$this->db->where('chitgiftid',$id);
		$this->db->update('chitgift',$delete);
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' deleted scheme gift/prize';
		$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,$this->schememoduleid);
		echo 'SUCCESS';
	}
	// scheme gift create
	public function schemegiftcreate() 
	{		
	    /* if($_POST['prizetemp'] != '') {
		     $prizereg=explode('-',$_POST['prizetemp']); 
			 $templateid=$prizereg[0];
			 $templatetype=$prizereg[1];
			 $templatename=$prizereg[2];
		}
		else{ */
			 $templateid=0;
			 $templatetype=0;
			 $templatename='No Print';
		//}
		if(empty($_POST['noofprizes'])){
			$_POST['noofprizes'] = 0;
		}else{
			$_POST['noofprizes'] = $_POST['noofprizes'];
		}
		$insert=array(
						'giftdate'=>$this->Basefunctions->ymd_format($_POST['giftdate']), 
						'chitschemeid'=>$_POST['schemeid'],
						'schemeamount'=>$_POST['amountgift'],
                        'chitlookupsid'=>$_POST['typegift'],	
						'giftamount'=>$_POST['prizeamount'],	
						'chitgiftname'=>$_POST['prizename'],	
						'giftcount'=>$_POST['noofprizes'],
						'giftdescription'=>$_POST['prizedescription'],						
						'gifttemplateid'=>$templateid,						
						'gifttemplatetype'=>$templatetype,						
						'gifttemplatename'=>$templatename,
						'industryid'=>$this->Basefunctions->industryid,
						'status'=>$this->Basefunctions->activestatus
					);
		$insert = array_merge($insert,$this->Crudmodel->defaultvalueget());
	    $this->db->insert('chitgift',$insert);
		$primaryid = $this->db->insert_id();
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' created scheme gift/prize';
		$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,$this->schememoduleid);
		echo 'SUCCESS';
	}
	// get  prize description
	public function prizeheadernameload() {
		$prizeheadername = $this->db->distinct()->select('prizeheadername')->from('prizeentry')->where('status',$this->Basefunctions->activestatus)->get();
		$i=0;
		foreach($prizeheadername->result() as $prizeheadernamedata) {
			$prize_headername=explode(',',$prizeheadernamedata->prizeheadername);
			 $prize_headername_count=count($prize_headername); 
			for($j=0; $j < $prize_headername_count; $j++){ 
				$prizeheadernamenames[$i] =$prize_headername[$j];
				$i++;
			}
		}
		$datas_prizeheadernamenames = array_map("unserialize", array_unique(array_map("serialize", $prizeheadernamenames)));
		echo json_encode($datas_prizeheadernamenames);
	}
	public function getprizename() {
	$schemeid=$_GET['schemeid'];
	$prizeselectamount=$_GET['prizeselectamount'];
	$prizenamedata=array();
	$prizename=$this->db->where('chitschemeid',$schemeid)->where('chitlookupsid',5)->where_in('schemeamount',array(0,$prizeselectamount))->where('status',$this->Basefunctions->activestatus)->get('chitgift');
	$i=0;
	if($prizename->num_rows() > 0){
		foreach($prizename->result() as $prize_name)
		{
			$pendingentry=$this->db->where('chitschemeid',$schemeid)->where('chitgiftid',$prize_name->chitgiftid)->where('amount',$prizeselectamount)->where('status',$this->Basefunctions->activestatus)->get('prizeentry')->num_rows();
			if($pendingentry < $prize_name->giftcount) {
				$prizenamedata[]=array(
						'prizeid' => $prize_name->chitgiftid,
						'prizename' => $prize_name->chitgiftname,
						'noofprizes' => $prize_name->giftcount,
						'giftamount' => $prize_name->giftamount
				);
			}
			$i++;
		}
	}else{
		$prizenamedata='';
	}
	echo json_encode($prizenamedata);
	}
	// scheme gift create
	public function prizeentrycreate() 
	{
		$insert=array(
		'entrydate'=>$this->Basefunctions->ymd_format($_POST['prizeentrydate']),
		'chitschemeid'=>$_POST['prizeschemeid'],
		'amount'=>$_POST['prizeselectamount'],
		'noofprizes'=>1,
		'chitgiftid'=>$_POST['prizeentryname'],
		'prizeheadername'=>$_POST['prizeheadername'],
		'tokenno'=>$_POST['tokenno'],
		'chitbookno'=>$_POST['chitbookno'],
		'prizedescription'=>$_POST['prizedesc'],
		'giftamount'=>$_POST['prizeamount'],
		'industryid'=>$this->Basefunctions->industryid,
		'status'=>$this->Basefunctions->activestatus
		);
		$insert = array_merge($insert,$this->Crudmodel->defaultvalueget());
	    $this->db->insert('prizeentry',array_filter($insert));
		$primaryid = $this->db->insert_id();
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' created prize entry';
		$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,$this->schememoduleid);
		echo 'SUCCESS';
	}
	// get chitbook 
	public function getchitbookno() {
		$schemeid=$_GET['schemeid'];
		$tokenno=$_GET['tokenno'];
		$chitgiftid=$_GET['prizeid'];
		$transdate = date('Y-m', time());
		$tokennodata =$this->db->query('select `prizeentryid` from `prizeentry` where DATE_FORMAT(entrydate, "%Y-%m") = "'.$transdate.'" and tokenno = "'.$tokenno.'" and chitschemeid = "'.$schemeid.'" and chitgiftid = "'.$chitgiftid.'" and status = 1 ');
		if($tokennodata->num_rows() > 0)
		{
			$chitbookno='NO';
		}else {
			$chitbooknodata=$this->db->where('chitschemeid',$schemeid)->where("FIND_IN_SET('$tokenno',lasttokenno) >", 0)->where('status',$this->Basefunctions->activestatus)->get('chitbook');
			if($chitbooknodata->num_rows() > 0)
			{
				$chitbookno=$chitbooknodata->row()->chitbookno;	
			} else {
				$chitbookno='NO';
			}
		}
		echo json_encode($chitbookno);
	}
	public function getpendingentry() {
		$schemeid=$_GET['schemeid'];
		$prizegiftid=$_GET['prizegiftid'];
	    $prizeselectamount=$_GET['prizeselectamount'];
	    $prizeheadername=$_GET['prizeheadername'];
		$pendingentry=$this->db->where('chitschemeid',$schemeid)->where('chitgiftid',$prizegiftid)->where('amount',$prizeselectamount)->where('prizeheadername',$prizeheadername)->where('status',$this->Basefunctions->activestatus)->get('prizeentry')->num_rows();
		echo json_encode($pendingentry);
	}
	public function prizeentrydelete()
	{
		$id=$_GET['primaryid'];
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' deleted prize entry';
		$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,$this->schememoduleid);
		//inactivate prize entry
		$delete = $this->Basefunctions->delete_log();
		$this->db->where('prizeentryid',$id);
		$this->db->update('prizeentry',$delete);
		echo 'SUCCESS';
	}
	// get  scheme name
	public function prizeschemenameload() {
		$schemenames = $this->db->select('distinct(chitscheme.chitschemeid),chitscheme.chitschemename')->from('chitgift')->join('chitscheme','chitscheme.chitschemeid = chitgift.chitschemeid','left')->where('chitgift.chitlookupsid',5)->where('chitscheme.status',$this->Basefunctions->activestatus)->where('chitgift.status',$this->Basefunctions->activestatus)->get();
		$i=0;
		foreach($schemenames->result() as $schemenamesdata) {
			$dataschemenames[$i] = array('chitschemeid'=>$schemenamesdata->chitschemeid,'schemename'=>$schemenamesdata->chitschemename);
			$i++;
		}
		$datas = ( ($i==0)? json_encode(array('fail'=>'FAILED')) : json_encode($dataschemenames) );
		echo $datas;
	}
	public function chitamountcreate() 
	{	
		$this->db->select('chitamountid');
		$this->db->from('chitamount');
		$this->db->where('status',$this->Basefunctions->activestatus);
		$this->db->where('chitamount',$_POST['chitamount']);
		$this->db->where('amountsuffix',$_POST['amountsuffix']);
		$data=$this->db->get()->result();
		foreach($data as $info)
		{
			$this->db->where('chitamountid',$info->chitamountid);
			$this->db->update('chitamount',$this->Basefunctions->delete_log());
			//audit-log
			$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
			$userid = $this->Basefunctions->logemployeeid;
			$activity = ''.$user.' deleted chit amount';
$this->Basefunctions->notificationcontentadd($info->chitamountid,'Deleted',$activity ,$userid,$this->schememoduleid);
		}  
		
	    $insert=array(
							'chitamount'=>$_POST['chitamount'],
							'amountsuffix'=>$_POST['amountsuffix'],
							'industryid'=>$this->Basefunctions->industryid
						);
			$insert = array_merge($insert,$this->Crudmodel->defaultvalueget());
			$this->db->insert('chitamount',$insert);
			$primaryid = $this->db->insert_id();
			//audit-log
			$userid = $this->Basefunctions->logemployeeid;
			$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
			$activity = ''.$user.' created Chit amount';
			$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,$this->schememoduleid);
		    echo 'SUCCESS';
	}
	public function chitamountretrive()
	{
		$id=$_GET['primaryid'];
		$this->db->select('chitamount.chitamount,chitamount.amountsuffix');
		$this->db->from('chitamount');
		$this->db->where('chitamountid',$id);
		$info=$this->db->get()->row();
     	$jsonarray=array(
							'chitamount'=>$info->chitamount,
							'amountsuffix'=>$info->amountsuffix,
						);
		echo json_encode($jsonarray);
	}
	public function chitamountupdate() {   
	    $this->db->select('chitamountid');
		$this->db->from('chitamount');
		$this->db->where('status',$this->Basefunctions->activestatus);
		$this->db->where('chitamount',$_POST['chitamount']);
		$this->db->where('amountsuffix',$_POST['amountsuffix']);
		$data=$this->db->get()->result();
		foreach($data as $info) {
			$this->db->where('chitamountid',$info->chitamountid);
			$this->db->update('chitamount',$this->Basefunctions->delete_log());
			//audit-log
			$userid = $this->Basefunctions->logemployeeid;
			$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
			$activity = ''.$user.' deleted chit amount';
			$this->Basefunctions->notificationcontentadd($info->chitamountid,'Deleted',$activity ,$userid,$this->schememoduleid);
		}  
	    $insert=array(
					'chitamount'=>$_POST['chitamount'],
					'amountsuffix'=>$_POST['amountsuffix'],
					'status'=>$this->Basefunctions->activestatus
				);
		$insert = array_merge($insert,$this->Crudmodel->defaultvalueget());
		$this->db->insert('chitamount',$insert);
		$primaryid = $this->db->insert_id();
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' created Chit amount';
		$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,$this->schememoduleid);
		echo 'SUCCESS';
	}
	public function chitamountdelete()
	{
		$id=$_GET['primaryid'];
		//delete chti amount
		$delete = $this->Basefunctions->delete_log();
		$this->db->where('chitamountid',$id);
		$this->db->update('chitamount',$delete);
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' deleted chit amount';
		$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,$this->schememoduleid);
		echo 'SUCCESS';
	}
	 public function loadchitamount() {
    	$this->db->select('chitamount,amountsuffix');
    	$this->db->from('chitamount');
    	$this->db->where_not_in('status',array(3,2,0));
    	$info=$this->db->get();
    	foreach($info->result() as $data){
    		$chitamountdetails[]=array(
    				'chitamount'=>$data->chitamount,
    				'amountsuffix'=>$data->amountsuffix
    			);
    	}
    	echo json_encode($chitamountdetails);
    }
    public function schemenameloadbaseongift() {
    	$industryid=$this->Basefunctions->industryid;
    	$schemenames = $this->db->query("SELECT `chitschemeid`, `chitschemename` FROM `chitscheme` WHERE ( `luckydraw` = 'YES' OR `gift` = 'YES') AND `industryid` = '3' AND `status` = 1");
    	$i=0;
    	foreach($schemenames->result() as $schemenamesdata) {
    		$dataschemenames[$i] = array('chitschemeid'=>$schemenamesdata->chitschemeid,'schemename'=>$schemenamesdata->chitschemename);
    		$i++;
    	}
    	$datas = ( ($i==0)? json_encode(array('fail'=>'FAILED')) : json_encode($dataschemenames) );
    	echo $datas;
    }
	public function checkamountdata(){
		$chitamountid = $_GET['datarowid'];
		$this->db->select('chitamount,amountsuffix');
    	$this->db->from('chitamount');
    	$this->db->where('status',1);
    	$this->db->where('chitamountid',$chitamountid);
    	$info=$this->db->get();
    	foreach($info->result() as $data){
    		$chitamountdata = $data->chitamount.'-'.$data->amountsuffix;
    	}
		$this->db->select('chitschemeid');
    	$this->db->from('chitscheme');
    	$this->db->where('status',1);
    	$this->db->where("FIND_IN_SET('".$chitamountdata."',chitamounts) >", 0);
    	$res=$this->db->get()->num_rows();
		echo $res;
	}
	public function checkprizeentry() {
		$this->db->select('chitbookid');
		$this->db->from('chitbook');
		$this->db->where('status',$this->Basefunctions->activestatus);
		$this->db->where('chitschemeid',$_POST['schemeid']);
		$this->db->where('amount',$_POST['amountgift']);
		$data=$this->db->get()->num_rows();
		if($data < $_POST['value']) {
			echo 'FAIL';
		}else{
			echo 'SUCCESS';
		}
	}
	// Gift Or Prize - entries against respective module.
	public function giftdeletecheck() {
		$id = $_GET['datarowid'];
		$data = 'SUCCESS';
		$giftorprize = $this->Basefunctions->singlefieldfetch('chitlookupsid','chitgiftid','chitgift',$id);
		if($giftorprize == 4) { // Gift Details checking in Prize Issue Module
			$this->db->select('prizeissue.prizeissueid');
			$this->db->from('prizeissue');
			$this->db->where_in('prizeissue.chitgiftid',$id);
			$this->db->where('prizeissue.status',$this->Basefunctions->activestatus);
			$result = $this->db->get();
			if($result->num_rows() > 0) {
				$data = 'GIFT';
			}
		} else if($giftorprize == 5) { // Prize Details checking in Prize Entry Module
			$this->db->select('prizeentry.prizeentryid');
			$this->db->from('prizeentry');
			$this->db->where('prizeentry.chitgiftid',$id);
			$this->db->where('prizeentry.status',$this->Basefunctions->activestatus);
			$result = $this->db->get();
			if($result->num_rows() > 0) {
				$data = 'PRIZE';
			}
		}
		echo $data;
	}
}