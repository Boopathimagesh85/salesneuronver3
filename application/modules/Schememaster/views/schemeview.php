<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('Base/headerfiles'); ?>
	<?php $this->load->view('Base/basedeleteform'); ?>
	<?php $this->load->view('Base/modulelist'); ?>
	<?php 
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$this->load->view('Base/overlaymobile');
		} else {
			$this->load->view('Base/overlay');
		}
	?>
	<?php 
		$industryid = $this->Basefunctions->industryid;
		if($industryid == 3) {		
	?>
		<link href="<?php echo base_url();?>css/jqgrid.min.css" media="screen" rel="stylesheet" type="text/css" />
	<?php 
		}
	?>
	</head>
<body>
	<div class="row" style="max-width:100%">
		<div class="off-canvas-wrap" data-offcanvas>
			<div class="inner-wrap">
				<!-- Add Form -->
				<div id="productformdiv" class="">
					<?php $this->load->view('schemeform'); ?>
				</div>
				<!-- close the off-canvas menu -->
				<a class="exit-off-canvas"></a>									
			</div>
		</div>
	</div>
	<?php
		$this->load->view('Base/basedeleteformforcombainedmodules');
	?>	
</body>
<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
<script src="<?php echo base_url();?>js/plugins/filecomputer/fileupload.js" type="text/javascript"></script>
<?php $this->load->view('Base/bottomscript'); ?>
<?php include 'js/Schememaster/scheme.php' ?>
</html>
