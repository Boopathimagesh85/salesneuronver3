<style type="text/css">
	.gridcontent{
		height: 100% !important;
	}
	.grid-view {
		height: 100% !important;
	}
</style>
<?php
	$device = $this->Basefunctions->deviceinfo();
	$dataset['gridtitle'] = $gridtitle['title'];
	$dataset['titleicon'] = $gridtitle['titleicon'];
	$dataset['moduleid'] = '49';
	$dataset['action'][0] = array('actionid'=>'chitamountadd','actiontitle'=>'Add','actionmore'=>'Yes','ulname'=>'chitamount');
	$dataset['action'][1] = array('actionid'=>'schemeadd','actiontitle'=>'Add','actionmore'=>'Yes','ulname'=>'scheme');
	$dataset['action'][2] = array('actionid'=>'giftadd','actiontitle'=>'Add','actionmore'=>'Yes','ulname'=>'gift');
	$dataset['action'][3] = array('actionid'=>'prizeentryadd','actiontitle'=>'Add','actionmore'=>'Yes','ulname'=>'prize');
	$this->load->view('Base/formwithgridmenuheader.php',$dataset);
	$defaultrecordview = $this->Basefunctions->get_company_settings('mainviewdefaultview');
	echo hidden('mainviewdefaultview',$defaultrecordview);
?>
<div class="large-12 columns addformunderheader">&nbsp; </div>
<div class="large-12 columns salesaddformcontainer" style="overflow:hidden;">
   <input type="hidden" name="amount_round" id="amount_round" value="<?php echo $amount_round; ?>">
	<!-- More Action DD start -- Gowtham -->
		<ul id="chitamountadvanceddrop" class="navaction-drop arrow_box" style="width:200px;top: 56px !important;white-space: nowrap; position: absolute; opacity: 1; display: none;left: -125px !important;">
		<li id="actionspanelamount" style="color:white;background-color:#2c2f48;height: 60px;    top: -8px; position: relative;border-top-left-radius: 4px;border-top-right-radius: 4px;"><i title="actionicons" id="actionicons" class="material-icons" style="font-size: 2rem;line-height: 1.9;color: #fff;">apps</i><span style="position:relative;top:-12px;color:#fff !important;font-size:0.9rem;font-family:
			"Nunito, sans-serif;"">Actions Panel</span></li>
			<li id="chitamountdelete" style="line-height: 47px !important;"><i title="amountdelete" id="amountdelete" class="material-icons" style="font-size: 1.4rem;line-height: 1.0;color: #788db4;">delete</i><span style="top: -6px;position: relative;">Delete</span></li>
		</ul>
		<ul id="schemeadvanceddrop" class="navaction-drop arrow_box"  style="width:200px;top: 56px !important;white-space: nowrap; position: absolute; opacity: 1; display: none;left: -125px !important;">
		<li id="actionspanelscheme" style="color:white;background-color:#2c2f48;height: 60px;    top: -8px; position: relative;border-top-left-radius: 4px;border-top-right-radius: 4px;"><i title="actionicons" id="actionicons" class="material-icons" style="font-size: 2rem;line-height: 1.9;color: #fff;">apps</i><span style="position:relative;top:-12px;color:#fff !important;font-size:0.9rem;font-family:
			"Nunito, sans-serif;"">Actions Panel</span></li>
			<li id="schemeedit"><i title="schemeeditchk" id="schemeeditchk" class="material-icons" style="font-size: 1.4rem;line-height: 1.0;color: #788db4;">edit</i><span style="top: -6px;position: relative;">Edit</span></li>
			<li id="schemedelete"><i title="schemedeletechk" id="schemedeletechk" class="material-icons" style="font-size: 1.4rem;line-height: 1.0;color: #788db4;">delete</i><span style="top: -6px;position: relative;">Delete</span></li>
		</ul>
		<ul id="giftadvanceddrop" class="navaction-drop arrow_box"  style="width:200px;top: 56px !important;white-space: nowrap; position: absolute; opacity: 1; display: none;left: -125px !important;">
		<li id="actionspanelgift" style="color:white;background-color:#2c2f48;height: 60px;    top: -8px; position: relative;border-top-left-radius: 4px;border-top-right-radius: 4px;"><i title="actionicons" id="actionicons" class="material-icons" style="font-size: 2rem;line-height: 1.9;color: #fff;">apps</i><span style="position:relative;top:-12px;color:#fff !important;font-size:0.9rem;font-family:
			"Nunito, sans-serif;"">Actions Panel</span></li>
			<li id="giftedit"><i title="gifteditchk" id="gifteditchk" class="material-icons" style="font-size: 1.4rem;line-height: 1.0;color: #788db4;">edit</i><span style="top: -6px;position: relative;">Edit</span></li>
			<li id="giftdelete"><i title="giftdeletechk" id="giftdeletechk" class="material-icons" style="font-size: 1.4rem;line-height: 1.0;color: #788db4;">delete</i><span style="top: -6px;position: relative;">Delete</span></li>
		</ul>
		<ul id="prizeadvanceddrop" class="navaction-drop arrow_box" style="width:200px;top: 56px !important;white-space: nowrap; position: absolute; opacity: 1; display: none;left: -125px !important;">
		<li id="actionspanelprize" style="color:white;background-color:#2c2f48;height: 60px;    top: -8px; position: relative;border-top-left-radius: 4px;border-top-right-radius: 4px;"><i title="actionicons" id="actionicons" class="material-icons" style="font-size: 2rem;line-height: 1.9;color: #fff;">apps</i><span style="position:relative;top:-12px;color:#fff !important;font-size:0.9rem;font-family:
			"Nunito, sans-serif;"">Actions Panel</span></li>
			<li id="prizenetrydelete"><i title="prizedeletechk" id="prizedeletechk" class="material-icons" style="font-size: 1.4rem;line-height: 1.0;color: #788db4;">delete</i><span style="top: -6px;position: relative;">Delete</span></li>
		</ul>
	<!-- More Action DD End -- Gowtham -->
	<div class="row mblhidedisplay">&nbsp;</div>
	<div id="subformspan1" class="hiddensubform transitionhiddenform transitionhiddenform" style="height:100% !important;"> 
		<div class="closed effectbox-overlay effectbox-default" id="chitamountoverlay">
			<div class="large-12 columns mblnopadding">
			<form method="POST" name="chitamountform" class="chitamountform"  id="chitamountform" style="height:100% !important;">
				<div id="chitamountaddwizard" class="validationEngineContainer" style="height:100% !important;">				
				<div id="chitamounteditwizard" class="validationEngineContainer" style="height:100% !important;">			
					<div class="large-4 large-offset-8 columns" style="padding-right:0px !important;height:100% !important;padding-left:0px !important;">
						<div class="large-12 columns cleardataform border-modal-8px" style="padding-left: 0 !important; padding-right: 0 !important;">
							<div class="large-12 columns sectionheaderformcaptionstyle" >Chit Scheme Amount</div>
							<div class="large-12 columns sectionpanel">
							<div class="input-field large-12 columns ">						
								<input type="text" class="validate[required,custom[integer],min[1],maxSize[10]]" id="chitamount" name="chitamount" value="" tabindex="101">
								<label for="chitamount">Chit Scheme Amount <span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="input-field large-12 columns ">				
								<input type="text" class="validate[required,maxSize[20],funcCall[checkamountsuffix]]" id="amountsuffix" name="amountsuffix" data-table="chitamount" data-validatefield="amountsuffix" data-validation-engine="" value="" tabindex="102" >
								<label for="amountsuffix">Amount Suffix <span class="mandatoryfildclass">*</span></label>
							</div>	
							</div>
							<div class="divider large-12 columns"></div>
							<div class="large-12 columns submitkeyboard sectionalertbuttonarea" style="text-align: right">
								<input type="hidden" name="chitschemeid" id="chitschemeid">
								<input type="hidden" value="" name="chitamountsortorder" id="chitamountsortorder">
								<input type="hidden" value="" name="chitamountsortcolumn" id="chitamountsortcolumn">
								<input type="button" class="alertbtnyes addkeyboard" id="chitamountcreate" name="" value="Save" tabindex="103">
								<input type="button" class="alertbtnyes updatekeyboard" id="chitamountupdate" name="" value="Save" tabindex="104" style="display: none">
								<input type="button" class="alertbtnno cancelkeyboard" id="chitamountcancel" name="" value="Close" tabindex="105">								
							</div>
						</div>
					</div>	
				</div>	
				</div>	
			</form>
			</div>
		</div>
		<div class="" style="padding-left:0;padding-right:0;height:100% !important;">
			<?php
					$device = $this->Basefunctions->deviceinfo();
					if($device=='phone') {
						echo '<div class="large-12 columns paddingzero forgetinggridname" id="chitamountgridwidth" style="height: 95% !important;"><div class=" inner-row-content inner-gridcontent" id="chitamountgrid" style="height:95% !important;top:0px;">
							<!-- Table header content , data rows & pagination for mobile-->
						</div>
						<footer class="inner-gridfooter footercontainer" id="chitamountgridfooter">
							<!-- Footer & Pagination content -->
						</footer></div>';
					} else {
						echo '<div class="large-12 columns forgetinggridname viewgridcolorstyle borderstyle" id="chitamountgridwidth" style="padding-left:0;padding-right:0;top:0px;position:relative;height:93% !important;"><div class="desktop row-content inner-gridcontent" id="chitamountgrid" style="max-width:2000px; height:95% !important;">
						<!-- Table content[Header & Record Rows] for desktop-->
						</div>
						<div class="inner-gridfooter footer-content footercontainer" id="chitamountgridfooter">
							<!-- Footer & Pagination content -->
						</div></div>';
					}
			?>
			</div>
	</div>
	<div id="subformspan2" class="hiddensubform hidedisplay transitionhiddenform" style="height: 100% !important;"> 
		<div class="closed effectbox-overlay effectbox-default" id="schemeoverlay" style="height: 100% !important;"> 
			<div class="large-12 columns mblnopadding">
				<form method="POST" name="schemeform" class="schemeform"  id="schemeform" style="height:100% !important;">
					<div id="schemeaddwizard" class="validationEngineContainer" style="height:100% !important;">				
					<div id="schemeeditwizard" class="validationEngineContainer" style="height:100% !important;">				
						<div class="large-4 large-offset-8 columns" style="padding-right:0px !important;height:100% !important;padding-left:0px !important;">
							<div class="large-12 columns cleardataform border-modal-8px" style="padding-left: 15px !important; padding-right: 15px !important;">
								<div class="large-12 columns sectionheaderformcaptionstyle" >Scheme</div>
								<div class="large-12 columns sectionpanel" style="background-color:#fff;">
								<div class="static-field large-6 columns hidedisplay">
									<label>Date<span class="mandatoryfildclass">*</span></label>								
									<input id="schemedate" class="fordatepicicon validate[required]"  type="text" data-dateformater="dd-mm-yy" data-prompt-position="bottomLeft:14,36" readonly="readonly" tabindex="101" name="schemedate" >
								</div>
								<div class="input-field large-6 columns ">	
									<input type="text" class="validate[required,funcCall[varcheckuniquename],maxSize[50]]" id="schemename" name="schemename" value="" tabindex="102" data-table="chitscheme" data-validatefield="chitschemename" data-validation-engine="" data-primaryid="">
									<label for="schemename"> Scheme Name <span class="mandatoryfildclass">*</span> </label>
								</div>
								<div class="input-field large-6 columns end" >				
									<input type="text" class="validate[required,funcCall[checkuniquenamenew],maxSize[20]]" id="prefixvalue" name="prefixvalue" value="" tabindex="103" data-table="chitscheme" data-validatefield="schemeprefix" data-primaryid="" >
									<label for="prefixvalue">Scheme Prefix <span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="static-field large-6 columns">
									<label>Start Date<span class="mandatoryfildclass">*</span></label>								
									<input id="schemeapplyfromdate" class="fordatepicicon validate[required]"  type="text" data-dateformater="dd-mm-yy" data-prompt-position="bottomLeft:14,36" readonly="readonly" tabindex="101" name="schemeapplyfromdate" >
								</div>
								<div class="static-field large-6 columns">
									<label>Activation Date<span class="mandatoryfildclass">*</span></label>								
									<input id="schemeapplytodate" class="fordatepicicon validate[required]"  type="text" data-dateformater="dd-mm-yy" data-prompt-position="bottomLeft:14,36" readonly="readonly" tabindex="101" name="schemeapplytodate" >
								</div>
								<div class="input-field large-6 columns ">				
									<input type="text" class="validate[required,custom[onlyWholeNumber],min[1],maxSize[5]]" id="memberslimit" name="memberslimit" value="" tabindex="110" >
									<label for="memberslimit">Members Limit <span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="static-field large-6 columns ">
								    <label>Account<span class="mandatoryfildclass">*</span></label>								
									<select id="accountid" name="accountid" class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="104" data-placeholder="select">
									<option value=""></option>
									<?php foreach($accountid as $key):?>
										<option value="<?php echo $key->accountid;?>">
											<?php echo $key->accountname;?></option>
									<?php endforeach;?>		
									</select>
								</div>
								<div class="static-field large-6 columns ">
								    <label>Variable Payment Mode<span class="mandatoryfildclass">*</span></label>								
									<select id="variablemodeid" name="variablemodeid" class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="104" data-placeholder="select">
										<option value="0">No</option>
										<option value="1">Yes</option>
									</select>
								</div>
								<div class="static-field large-6 columns ">
								    <label>Chit Type<span class="mandatoryfildclass">*</span></label>								
									<select id="chitype" name="chitype" class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="104" data-placeholder="select">
									<option value=""></option>
									<?php foreach($chittypes as $key):?>
										<option value="<?php echo $key->chitlookupsid;?>">
											<?php echo $key->chitlookupsname;?></option>
									<?php endforeach;?>		
									</select>
								</div>
								<div id="purityiddivhid" class="static-field large-6 columns hidedisplay">
									<label>Purity<span class="mandatoryfildclass" id="purityid_req">*</span></label>				
									<select id="purityid" name="purityid" class="chzn-select validate[required]"  readonly data-prompt-position="bottomLeft:14,36" tabindex="105">
										<option value=""></option>
										<?php $prev = ' ';
										foreach($purity->result() as $key):
										$cur = $key->metalid;
										$a="";
										if($prev != $cur )
										{
											if($prev != " ")
											{
												 echo '</optgroup>';
											}
											echo '<optgroup  label="'.$key->metalname.'">';
											$prev = $key->metalid;
										}
										?>
										<option data-melting="<?php echo $key->melting;?>" value="<?php echo $key->purityid;?>"><?php echo $key->purityname;?></option>
										<?php endforeach;?>
									</select>
								</div>
								<div id="intrestmodeiddivhid" class="static-field large-6 columns hidedisplay">
								   <label>Mode<span class="mandatoryfildclass">*</span></label>								
									<select id="intrestmodeid" name="intrestmodeid" class="chzn-select" data-prompt-position="bottomLeft:14,36" data-placeholder="select" tabindex="106">
										<?php foreach($interestmode as $key):?>
										<option value="<?php echo $key->intrestmodeid;?>" data-attr-value="<?php echo $key->comments;?>" data-interestorgram="<?php echo $key->interestorgram;?>">
											<?php echo $key->intrestmodename;?></option>
									<?php endforeach;?>
									</select>
								</div>
								<div class="static-field large-6 columns ">
								    <label>Gift<span class="mandatoryfildclass">*</span></label>								
									<select id="gift" name="gift" class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" data-placeholder="select" tabindex="112">
										<option value="NO">NO</option>
										<option value="YES">YES</option>
									</select>
								</div>
								<div class="static-field large-6 columns ">
								   <label>Lucky Draw<span class="mandatoryfildclass">*</span></label>								
									<select id="luckydraw" name="luckydraw" class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" data-placeholder="select" tabindex="111">
									<option value="NO">NO</option>
									<option value="YES">YES</option>	
									</select>
								</div>
								<div class="input-field large-6 columns hidedisplay" id="interestratedivhid">				
									<input type="text" class="" id="interestrate" name="interestrate" value="0" tabindex="107" >
									<label for="interestrate">Interest Rate(%) <span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="input-field large-6 columns hidedisplay" id="refundratedivhid">				
									<input type="text" class="" id="refundrate" name="refundrate" value="0" tabindex="107" >
									<label for="refundrate">Refund Rate(%) <span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="input-field large-6 columns hidedisplay" id="maxschemeamountdivhid">				
									<input type="text" class="validate[required,custom[integer]]" id="maxschemeamount" name="maxschemeamount" value="" tabindex="108" >
									<label for="maxschemeamount">Max Scheme Amount<span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="input-field large-6 columns hidedisplay" id="minnoofmonthsdivhid">				
									<input type="text" class="validate[required,custom[onlyWholeNumber],min[1]]" id="minnoofmonths" name="minnoofmonths" value="" tabindex="109" >
									<label for="minnoofmonths">Min Months <span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="input-field large-6 columns" id="averagemonthsdivhid">
									<input type="text" class="" id="averagemonths" name="averagemonths" value="" tabindex="110" >
									<label for="averagemonths">Average Months <span class="mandatoryfildclass">*</span></label>
								</div>
								
								<div class="input-field large-6 columns ">				
									<input type="text" class="validate[required,custom[onlyWholeNumber],min[1],maxSize[5]]" id="noofmonths" name="noofmonths" value="" tabindex="110" >
									<label for="noofmonths">Total Months <span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="static-field large-6 columns" id="chitbooktokenpattern_div">
								    <label>Tokenno Pattern<span class="mandatoryfildclass">*</span></label>								
									<select id="chitbooktokenpattern" name="chitbooktokenpattern" class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" data-placeholder="select" tabindex="113">
										<option value="scheme token">Scheme Token</option>
										<option value="scheme amount token">Scheme Amount Token</option>
									</select>
								</div>
								<div id="baseamountdivhid" class="input-field large-6 columns hidedisplay">
									<input type="text" class="validate[required,custom[onlyWholeNumber],maxSize[10],min[1]]" id="baseamount" name="baseamount" value="0" tabindex="114">
									<label for="baseamount">Base Amount <span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="static-field large-12 columns " id="amountdivhid">
								    <label>Chit Amounts<span class="mandatoryfildclass">*</span></label>			
									<select id="amount" name="amount" class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" multiple tabindex="115">	
									</select>
									<input type="hidden" name="chitamounthide" id="chitamounthide"/>
								    </div>
								<div class="static-field large-12 columns ">
									<label>Chitbook No Pattern <span class="mandatoryfildclass">*</span></label>
									<input type="text" class="validate[required]" id="chitbooknopattern" name="chitbooknopattern" value="" data-prompt-position="bottomLeft:14,36" tabindex="116">
								</div>										
								<div class="static-field large-6 columns ">
									<label>Reg Print Template</label>						
									<select id="chitregtemplateid" name="chitregtemplateid" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="117">	
										<option value="0">No Print</option>
										<?php foreach($chitbook_reg as $key):?>
										<option value="<?php echo $key->id;?>" data-printtype="<?php echo $key->id.'-'.$key->templatetype.'-'.$key->name;?>">
										<?php echo $key->name;?></option>
										<?php endforeach;?>	
									</select>
								</div>
								<div class="static-field large-6 columns ">
									<label>ChitEntry Print Template</label>						
									<select id="chitentrytemplateid" name="chitentrytemplateid" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="118">	
										<option value="0">No Print</option>
										<?php foreach($chitbook_chitentry as $key):?>
										<option value="<?php echo $key->id;?>" data-printtype="<?php echo $key->id.'-'.$key->templatetype.'-'.$key->name;?>">
										<?php echo $key->name;?></option>
										<?php endforeach;?>	
									</select>
								</div>
								<div class="input-field large-12 columns ">
									<textarea name="description" id="description" class="materialize-textarea validate[maxSize[200]]" rows="3" cols="40" tabindex="119" data-prompt-position="bottomLeft:1,50"></textarea>
									<label for="description">Description</label>
		                        </div>
		                        </div>
		                        <div class="divider large-12 columns"></div>
								<div class="large-12 columns submitkeyboard sectionalertbuttonarea" style="text-align: right;background-color:#fff;">
									<!--schemeprimaryid,schemeprimaryname important -->	
									<input type="hidden" value="" name="updateschemeid" id="updateschemeid">
									<input type="hidden" value="" name="schemesortorder" id="schemesortorder">
									<input type="hidden" value="" name="schemesortcolumn" id="schemesortcolumn">
									<input type="hidden" name="chitschemeprimaryname" id="chitschemeprimaryname">
									<input type="hidden" name="chitschemesecondaryname" id="chitschemesecondaryname">
									<input type="hidden" name="chitinteresttype" id="chitinteresttype" value="<?php echo $chitinteresttype; ?>">
									<input type="button" class="alertbtnyes addkeyboard" id="schemecreate" name="" value="Save" tabindex="120">
									<input type="button" class="alertbtnyes updatekeyboard" id="schemeupdate" name="" value="Save" tabindex="121" style="display: none">
									<input type="button" class="alertbtnno cancelkeyboard" id="schemecancel" name="" value="Close" tabindex="122">
								</div>
							</div>
						</div>	
					</div>	
					</div>	
				</form>
			</div>
		</div>
		<div class="" style="padding-left:0;padding-right:0;height:100% !important;">
			<?php
					$device = $this->Basefunctions->deviceinfo();
					if($device=='phone') {
						echo '<div class="large-12 columns paddingzero forgetinggridname" id="schemegridwidth" style="height:95% !important;"><div class=" inner-row-content inner-gridcontent" id="schemegrid" style="height:95% !important;top:0px;">
							<!-- Table header content , data rows & pagination for mobile-->
						</div>
						<footer class="inner-gridfooter footercontainer" id="schemegridfooter">
							<!-- Footer & Pagination content -->
						</footer></div>';
					} else {
						echo '<div class="large-12 columns forgetinggridname viewgridcolorstyle borderstyle" id="schemegridwidth" style="padding-left:0;padding-right:0;height: 95% !important;"><div class="desktop row-content inner-gridcontent" id="schemegrid" style="max-width:2000px; height:93% !important;top:0px;">
						<!-- Table content[Header & Record Rows] for desktop-->
						</div>
						<div class="inner-gridfooter footer-content footercontainer" id="schemegridfooter">
							<!-- Footer & Pagination content -->
						</div></div>';
					}
			?>
		</div>
	</div>
	<div id="subformspan3" class="hiddensubform hidedisplay transitionhiddenform" style="height: 100% !important;"> 
		<div class="closed effectbox-overlay effectbox-default" id="giftoverlay"  style="height: 100% !important;"> 
			<div class="large-12 columns mblnopadding"  style="height: 100% !important;"> 
				<form method="POST" name="giftform" class="giftform"  id="giftform" style="height:100% !important;">
					<div id="giftaddwizard" class="validationEngineContainer" style="height:100% !important;">			
					<div id="gifteditwizard" class="validationEngineContainer" style="height:100% !important;">			
						<div class="large-4 large-offset-8 columns" style="padding-right:0px !important;height:100% !important;padding-left:0px !important;">	
							<div class="large-12 columns cleardataform border-modal-8px" style="padding-left: 0 !important; padding-right: 0 !important;">
								<div class="large-12 columns sectionheaderformcaptionstyle">Gift And Prize</div>
								<div class="large-12 columns sectionpanel">
								<div class="static-field large-6 columns giftdatediv" style="padding-bottom: 4px;">
									<label>Date<span class="mandatoryfildclass">*</span></label>								
									<input id="giftdate" class="fordatepicicon validate[required]"  type="text" data-dateformater="dd-mm-yy" data-prompt-position="bottomLeft" readonly="readonly" tabindex="101" name="giftdate" >
								</div>
								<div class="static-field large-6 columns ">
									<label>Scheme Name<span class="mandatoryfildclass">*</span></label>								
								<select id="schemeid" name="schemeid" class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" data-placeholder="select">
									<option></option>
								</select>
								</div>
								<div class="static-field large-6 columns ">
									<label>Amount<span class="mandatoryfildclass">*</span></label>								
									<select id="amountgift" name="amountgift" data-amountprefix="" class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" data-placeholder="select">
									<option></option>
									</select>
									</div>
								<div class="static-field large-6 columns ">
									<label>Type<span class="mandatoryfildclass">*</span></label>								
									<select id="typegift" name="typegift" class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" data-placeholder="select">
									<option value="">Select</option>
									<?php foreach($gifttypes as $key):?>
										<option value="<?php echo $key->chitlookupsid;?>">
											<?php echo $key->chitlookupsname;?></option>
									<?php endforeach;?>		>
									</select>
								</div>
								<!--<div class="static-field large-6 columns giftentrytemplateid">
									<label>Prize Template</label>						
									<select id="giftentrytemplateid" name="giftentrytemplateid" class="chzn-select" data-prompt-position="bottomLeft:14,36">	
										<option value="0">No Print</option>
										<?php //foreach($schemeentry as $key):?>
										<option value="<?php //echo $key->id;?>" data-printtype="<?php //echo $key->id.'-'.$key->templatetype.'-'.$key->name;?>">
										<?php //echo $key->name;?></option>
										<?php //endforeach;?>	
									</select>
								</div>-->
								<div class="input-field large-6 columns ">
									<input type="text"  id="prizeamount" name="prizeamount" value="" tabindex="" class="validate[required,custom[integer],maxSize[10]]">
									<label for="prizeamount">Gift/Prize amount<span class="mandatoryfildclass">*</span></label>
								</div>
		                        <div class="input-field large-6 columns ">
									<input type="text" class="validate[required,custom[onlyLetterSp],maxSize[100],funcCall[giftprizeuniquename]]" id="prizename" name="prizename" value="" tabindex="" >
									<label for="prizename">Gift/Prize Name<span class="mandatoryfildclass">*</span></label>
								</div>	
		                        <div class="input-field large-6 columns noofprizes">
									<input type="text" class="validate[]" id="noofprizes" name="noofprizes" value="" tabindex="" >
									<label for="noofprizes">No Of Prizes</label>
								</div>	
								<!--<div class="static-field large-12 columns ">
									<label>Gift/Prize Description<span class="mandatoryfildclass">*</span></label>
									<input type="text" class="validate[required]" id="prizedescription" name="prizedescription" value="" tabindex=""data-prompt-position="bottomLeft:14,36">
		                        </div>-->
								<div class="input-field large-12 columns">
									<textarea name="prizedescription" class="materialize-textarea validate[maxSize[200]]" id="prizedescription" rows="3" cols="40" tabindex="" data-validation-engine="" data-prompt-position="bottomLeft:1,50"></textarea>
									<label for="prizedescription">Gift/Prize Description</label>
								</div>	
		                        </div>
								<div class="divider large-12 columns"></div>
								<div class="large-12 columns submitkeyboard sectionalertbuttonarea" style="text-align: right">
									<input type="hidden" id="giftpriceid" name="giftpriceid"/>
									<input type="hidden" value="" name="giftsortorder" id="giftsortorder">
									<input type="hidden" value="" name="giftsortcolumn" id="giftsortcolumn">
									<input type="hidden" id="schememonths" name="schememonths"/>
									<input type="hidden" id="schemebaseamount" name="schemebaseamount"/>
									<input type="button" class="alertbtnyes addkeyboard" id="giftcreate" name="" value="Save" tabindex="">
									<input type="button" class="alertbtnyes updatekeyboard" id="giftupdate" name="" value="Save" tabindex="" style="display: none;">							
									<input type="button" class="alertbtnno cancelkeyboard" id="giftcancel" name="" value="Close" tabindex="118">
								</div>
							</div>
						</div>
					</div>
					</div>
				</form>
				</div>
			</div>	
			<div class="" style="padding-left:0;padding-right:0;height:100% !important;">
				<?php
						$device = $this->Basefunctions->deviceinfo();
						if($device=='phone') {
							echo '<div class="large-12 columns paddingzero forgetinggridname" id="giftgridwidth" style="height:95% !important;"><div class=" inner-row-content inner-gridcontent" id="giftgrid" style="height:530px;top:0px;height:95% !important;">
								<!-- Table header content , data rows & pagination for mobile-->
							</div>
							<footer class="inner-gridfooter footercontainer" id="giftgridfooter">
								<!-- Footer & Pagination content -->
							</footer></div>';
						} else {
							echo '<div class="large-12 columns forgetinggridname viewgridcolorstyle borderstyle" id="giftgridwidth" style="padding-left:0;padding-right:0;height: 95% !important;"><div class="desktop row-content inner-gridcontent" id="giftgrid" style="max-width:2000px; height:93% !important;top:0px;">
							<!-- Table content[Header & Record Rows] for desktop-->
							</div>
							<div class="inner-gridfooter footer-content footercontainer" id="giftgridfooter">
								<!-- Footer & Pagination content -->
							</div></div>';
						}
				?>
			</div>
	</div>
	<div id="subformspan4" class="hiddensubform hidedisplay transitionhiddenform" style="height:100% !important;"> 
		<div class="closed effectbox-overlay effectbox-default" id="prizeentryoverlay" style="height:100% !important;">
			<div class="large-12 columns mblnopadding">
				<form method="POST" name="prizeentryform" class="prizeentryform"  id="prizeentryform" style="height:100% !important;">
					<div id="prizeentryformaddwizard" class="validationEngineContainer" style="height:100% !important;">			
						<div class="large-4 large-offset-8 columns" style="padding-right:0px !important;height:100% !important;padding-left:0px !important;">
							<div class="large-12 columns cleardataform border-modal-8px" style="padding-left: 0 !important; padding-right: 0 !important;">
								<div class="large-12 columns sectionheaderformcaptionstyle">Prize Entry</div>
								<div class="large-12 columns sectionpanel">
								<div class="static-field large-6 columns prizeentrydatediv" style="padding-bottom: 4px;">
									<label>Date<span class="mandatoryfildclass">*</span></label>								
									<input id="prizeentrydate" class="fordatepicicon validate[required]"  type="text" data-dateformater="dd-mm-yy" data-prompt-position="bottomLeft:14,36" readonly="readonly" tabindex="101" name="prizeentrydate" >
								</div>
								<div class="static-field large-6 columns ">
									<label>Scheme Name<span class="mandatoryfildclass">*</span></label>								
								<select id="prizeschemeid" name="prizeschemeid" class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" data-placeholder="select">
									<option></option>
								</select>
								</div>
								<div class="static-field large-6 columns ">
									<label>Amount<span class="mandatoryfildclass">*</span></label>								
									<select id="prizeselectamount" name="prizeselectamount" data-amountprefix="" class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" data-placeholder="select">
									<option></option>
									</select>
									</div>
									<div class="static-field large-12 columns ">
									<label>Prize Print Name<span class="mandatoryfildclass">*</span></label>
									<input type="text"  id="prizeheadername" name="prizeheadername" value="" tabindex="" class="validate[required]" data-prompt-position="bottomLeft:14,36" >
								    </div>
									 <div class="static-field large-6 columns ">
									<label>Prize Name<span class="mandatoryfildclass">*</span></label>								
									<select id="prizeentryname" name="prizeentryname" data-noofprizes="" class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" data-placeholder="select">
									<option></option>
									</select>
								</div>	
								<div class="input-field large-6 columns ">
									<input type="text" class="" id="prizecount" name="prizecount" value="" tabindex="" readonly>
									<label for="prizecount">No Of Prizes</label>	
								</div>
		                         <div class="input-field large-6 columns ">
		                           
								<input type="text" class="" id="pendingentry" name="pendingentry" value="" tabindex=""  readonly>
								<label for="pendingentry">Pending Entry</label>
		                        </div>	
								<div class="input-field large-6 columns ">
		                           
								<input type="text" class="validate[required]" id="tokenno" name="tokenno" value="" tabindex="" data-prompt-position="bottomLeft">
								<label for="tokenno">Token No<span class="mandatoryfildclass">*</span></label>
		                        </div>	
								<div class="input-field large-6 columns ">
								<input type="text" class="validate[required]" id="chitbookno" name="chitbookno" value="" tabindex="" data-prompt-position="bottomLeft" readonly>
								<label for="chitbookno">Chitbook No<span class="mandatoryfildclass">*</span></label>
		                        </div>
								<div class="input-field large-12 columns ">
									<textarea name="prizedesc" class="materialize-textarea validate[maxSize[200]]" id="prizedesc" rows="3" cols="40" tabindex="" data-validation-engine="" data-prompt-position="bottomLeft:1,50"></textarea>
									<label for="prizedesc">Prize Desc</label>
								</div>
		                        </div>						
								<div class="divider large-12 columns"></div>
								<div class="large-12 columns submitkeyboard sectionalertbuttonarea" style="text-align: right">
									<input type="button" class="alertbtnyes addkeyboard" id="prizeentrycreate" name="" value="Save" tabindex="">
									<input type="button" class="alertbtnno cancelkeyboard" id="prizeentrycancel" name="" value="Close" tabindex="118">
								</div>
							</div>
							<input type="hidden" value="" name="giftsortorder" id="giftsortorder">
							<input type="hidden" value="" name="giftsortcolumn" id="giftsortcolumn">
						</div>
					</div>
				</form>
			</div>
		</div>
			<div class="" style="padding-left:0;padding-right:0;height:100% !important;">
				<?php
						$device = $this->Basefunctions->deviceinfo();
						if($device=='phone') {
							echo '<div class="large-12 columns paddingzero forgetinggridname" id="prizenetrygridwidth" style="height:95% !important;"><div class=" inner-row-content inner-gridcontent" id="prizenetrygrid" style="height:95% !important;top:0px;">
								<!-- Table header content , data rows & pagination for mobile-->
							</div>
							<footer class="inner-gridfooter footercontainer" id="prizenetrygridfooter">
								<!-- Footer & Pagination content -->
							</footer></div>';
						} else {
							echo '<div class="large-12 columns forgetinggridname viewgridcolorstyle borderstyle" id="prizenetrygridwidth" style="padding-left:0;padding-right:0;height: 93% !important;"><div class="desktop row-content inner-gridcontent" id="prizenetrygrid" style="max-width:2000px; height:95% !important;top:0px;">
							<!-- Table content[Header & Record Rows] for desktop-->
							</div>
							<div class="inner-gridfooter footer-content footercontainer" id="prizenetrygridfooter">
								<!-- Footer & Pagination content -->
							</div></div>';
						}
				?>
			</div>
		</div>
	</div>