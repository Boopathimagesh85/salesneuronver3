<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Schememaster extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');	
		$this->load->model('Schememodel');		
    }
    //first basic hitting view
    public function index()
    {	
    	$moduleid = array(49);
		sessionchecker($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['chittypes']=$this->Basefunctions->simpledropdownlookup('chitlookups','chitlookupsid,chittypegroup,chitlookupsname','chitlookupsid',1);
		$data['gifttypes']=$this->Basefunctions->simpledropdownlookup('chitlookups','chitlookupsid,chittypegroup,chitlookupsname','chitlookupsid',2);
		$data['interestmode']=$this->Basefunctions->simpledropdownwithcond('intrestmodeid','interestorgram,intrestmodename,comments','intrestmode','moduleid','49');
		$data['schemenames']=$this->Basefunctions->simpledropdown('chitscheme','chitschemeid,chitschemename','chitschemeid');
		$data['purity'] = $this->Basefunctions->purity_groupdropdown();
		$data['chitbook_reg']=$this->Basefunctions->dynamicgroupdropdownmultiple(153,'printtemplates','tagtemplate'); 
		$data['chitbook_chitentry']=$this->Basefunctions->dynamicgroupdropdownmultiple(51,'printtemplates','tagtemplate'); 
		$data['schemeentry']=$this->Basefunctions->dynamicgroupdropdownmultiple(34,'printtemplates','tagtemplate');
		$data['amount_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$data['accountid']=$this->Basefunctions->simpledropdownwithcond('accountid','accountid,accountname','account','accounttypeid','20');
		$data['chitinteresttype']= $this->Basefunctions->get_company_settings('chitinteresttype'); //Chit Interest type - Amount/Percentage
		$this->load->view('schemeview',$data);
    }
	//scheme create
	public function schemecreate() {  
    	$this->Schememodel->schemecreate();
    }
	//scheme  gift create
	public function schemegiftcreate() {  
    	$this->Schememodel->schemegiftcreate();
    }
	// scheme name load
	public function schemenameload() {
		$this->Basefunctions->schemenameload();
	}
	public function schemenameloadbaseongift() {
		$this->Schememodel->schemenameloadbaseongift();
	}
	// scheme amount load
	public function schemeamountload() {
			$this->Schememodel->schemeamountload();
	}
	// scheme amount load
	public function prizedescload() {
			$this->Schememodel->prizedescload();
	}
	/**	*delete scheme	*/
	public function schemedelete() {	
		$this->Schememodel->schemedelete();
	}
	/**	*delete gift	*/
	public function giftdelete() {	
		$this->Schememodel->giftdelete();
	}
	/**	*retrieve scheme	*/
	public function schemeamountretrive() {  
    	$this->Basefunctions->schemeamountretrive();
    }
	/**	*retrieve scheme names	*/
	public function schemeretrive() {  
    	$this->Schememodel->schemeretrive();
    }
	/**	*retrieve scheme names	*/
	public function giftretrive() {  
    	$this->Schememodel->giftretrive();
    }
	/**	*update scheme	*/
	public function schemeupdate() {
		$this->Schememodel->schemeupdate();
	}
	/**	*update gift */
	public function giftupdate() {
		$this->Schememodel->giftupdate();
	}
	// header name load
	public function prizeheadernameload() {
			$this->Schememodel->prizeheadernameload();
	}
	//get prize name
	public function getprizename() {
			$this->Schememodel->getprizename();
	}
	//prize entry create
	public function prizeentrycreate() {  
    	$this->Schememodel->prizeentrycreate();
    }
	//get chitbookno
	public function getchitbookno() {  
    	$this->Schememodel->getchitbookno();
    }
	// get pending prize entry
	//get chitbookno
	public function getpendingentry() {  
    	$this->Schememodel->getpendingentry();
    }
	// delete prize entry
	public function prizeentrydelete() {  
    	$this->Schememodel->prizeentrydelete();
    }
	// prize scheme name 
	public function prizeschemenameload() {
			$this->Schememodel->prizeschemenameload();
	}
	public function chitamountcreate() {
			$this->Schememodel->chitamountcreate();
	}
	public function chitamountretrive() {
			$this->Schememodel->chitamountretrive();
	}
	public function chitamountupdate() {
			$this->Schememodel->chitamountupdate();
	}
	public function chitamountdelete() {
			$this->Schememodel->chitamountdelete();
	}
	public function loadchitamount() {
			$this->Schememodel->loadchitamount();
	}
	// check chit amount data
	public function checkamountdata() {
			$this->Schememodel->checkamountdata();
	}
	// check no of customer based on scheme with amount
	public function checkprizeentry() {  
    	$this->Schememodel->checkprizeentry();
    }
	// Gift Or Prize - entries against respective module.
	public function giftdeletecheck() {  
    	$this->Schememodel->giftdeletecheck();
    }
}