<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Widgetdatamapping extends CI_Controller {
	function __construct() {
    	parent::__construct();
		$this->load->model('Widgetdatamapping/Widgetdatamappingmodel');
		$this->load->helper('formbuild');
		$this->load->model('Base/Basefunctions');
    }
	public function index() {
		$moduleid =array(23);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Widgetdatamapping/widgetdatamappingview',$data);
	}
	//Create New data
	public function modulefieldname() {
		$moduleid=trim($_GET['moduleid']);
    	$this->Widgetdatamappingmodel->simplegroupdropdown('modulefieldid','fieldlabel','modulefield','moduletabid',$moduleid,'uitypeid','columnname');
    }
	//Create New data
	public function newdatacreate() {  
    	$this->Widgetdatamappingmodel->newdatacreatemodel();
    }//Edit data
	public function editdatacreate() {  
    	$this->Widgetdatamappingmodel->editdatacreatemodel();
    }
	//Delete New data
	public function deleteinformationdata() {  
    	$this->Widgetdatamappingmodel->deleteinformationdatamodel();
    }
	//dropdown datafetch	
	public function fetchdddatawithcond() {  
    	$this->Widgetdatamappingmodel->fetchdddatawithcondmodel();
    }
	//data fetch
	public function datafetchineditform() {  
    	$this->Widgetdatamappingmodel->datafetchineditformmodel();
    }
	public function accountfieldnamecheck() {  
    	$this->Widgetdatamappingmodel->accountfieldnamecheckmodel();
    }
	//widget data laod
	public function widgetdatalaodfun() {
		$this->Widgetdatamappingmodel->widgetdatalaodfunmodel();
	}
}