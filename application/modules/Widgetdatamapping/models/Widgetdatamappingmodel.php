<?php
Class Widgetdatamappingmodel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->model('Base/Crudmodel');
    }
	public function simplegroupdropdown($did,$dname,$table,$whfield,$whdata,$uitype,$columnname) {
		$i=0;
		$this->db->select("$dname,$did,$uitype");
		$this->db->from($table);
		if($whdata != '') {
			$this->db->where_in($whfield,$whdata);
		}
		if($whdata == 216){
			$this->db->where_not_in('modulefield.modulefieldid',array(450,451,452,453,454,455,456,457,458,459,460,461));
		}
		if($whdata == 201){
			$this->db->where_not_in('modulefield.modulefieldid',array(189,190,191,192,193,195,196,197,198,199));
		}
		if($whdata == 202){
			$this->db->where_not_in('modulefield.modulefieldid',array(230,231,232,233,234,235,236,237,238,239,240,1322,1323));
		}
		if($whdata == 203){
			$this->db->where_not_in('modulefield.modulefieldid',array(269,270,271,272,273,274,275,276,277,278,279,1330,1331));
		}
		$this->db->where_not_in('uitypeid',array(15,16,12,13,27,21,22,23,24,14));
		$this->db->where_not_in('active','No');
		$this->db->where('status',1);
		$result = $this->db->get();
		
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				$data[$i] = array('datasid'=>$row->$did,'dataname'=>$row->$dname,'uitype'=>$row->$uitype);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
    }
	//new data add form
	public function newdatacreatemodel(){
		$cdate = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$status	= $this->Basefunctions->activestatus;
		$moduleid = $_POST['moduleid'];
		$widgetid = $_POST['widgetid'];
		$frommodulefieldid = $_POST['frommodulefieldid'];
		$tomodulefieldid = $_POST['tomodulefieldid'];
		//check previous data existance
		$where_array=array('moduleid'=>$moduleid,'widgetid'=>$widgetid,'frommodulefieldid'=>$frommodulefieldid,'status'=>1);
		$data_chk = $this->db->select('widgetdatamappingid')->from('widgetdatamapping')->where($where_array)->limit(1)->get();
		
		if($data_chk->num_rows() == 0){
			$leadmap = array(
					'moduleid'=>$moduleid,
					'widgetid'=>$widgetid,
					'frommodulefieldid'=>$frommodulefieldid,
					'tomodulefieldid'=>$tomodulefieldid,
					'industryid'=>$this->Basefunctions->industryid,
					'branchid'=>$this->Basefunctions->branchid,
					'createdate'=>$cdate,
					'lastupdatedate'=>$cdate,
					'createuserid'=>$userid,
					'lastupdateuserid'=>$userid,
					'status'=>$status
				);
				$this->db->insert('widgetdatamapping',$leadmap);
		}else{
			foreach($data_chk->result() as $info){
				$widgetdatamappingid = $info->widgetdatamappingid;
			}
			$widgetmap = array(
				'frommodulefieldid'=>$frommodulefieldid,
				'tomodulefieldid'=>$tomodulefieldid,
				'lastupdatedate'=>$cdate,
				'lastupdateuserid'=>$userid,
				'status'=>$status
			);
			$this->db->where('widgetdatamapping.widgetdatamappingid',$widgetdatamappingid);
			$this->db->update('widgetdatamapping',$widgetmap);
		}
		echo "TRUE";
	}
	public function checkleadfieldid($leadid){
		$data ="False";
		$this->db->select('leadid,leadconversionmappingid');
		$this->db->from('leadconversionmapping');
		$this->db->where_in('leadconversionmapping.leadid',$leadid);
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				$data = $row->leadconversionmappingid;
			}
			return $data;
		}
		else{ return $data; }
	}
	//edit operation
	public function editdatacreatemodel(){
		$cdate = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$status	= $this->Basefunctions->activestatus;
		$leadid = $_POST['leadfieldid'];
		$accountid = $_POST['accountfieldid'];
		if($accountid != ""){$accid = $accountid;}else{$accid = 1;}
		$contactid = $_POST['contactfieldid'];
		if($contactid != ""){$contid = $contactid;}else{$contid = 1;}
		$opportunityid = $_POST['opprtunityfieldid'];
		if($opportunityid != ""){$oppid = $opportunityid;}else{$oppid = 1;}
		$editid = $_POST['editid'];
		$leadmap = array(
				'leadid'=>$leadid,
				'accountid'=>$accid,
				'contactid'=>$contid,
				'opportunityid'=>$oppid,
				'lastupdatedate'=>$cdate,
				'lastupdateuserid'=>$userid,
				'status'=>$status
			);
			$this->db->where('leadconversionmapping.leadconversionmappingid',$editid);
			$this->db->update('leadconversionmapping',$leadmap);
			echo "TRUE";
	}	
	//delete information
	public function deleteinformationdatamodel(){
		$partabname = 'widgetdatamapping';
		$primaryname = 'widgetdatamappingid';
		$id = $_GET['primarydataid'];
		$ctable= '';
		$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
		echo "TRUE";
	}
	//drop down value fetch 
	public function fetchdddatawithcondmodel(){
		$id = $_GET['fieldid'];
		$name = $_GET['fieldname'];
		$tablename = $_GET['tablename'];
		$moduleid = $_GET['moduleid'];
		$value = $_GET['value'];
		$whfield = $_GET['moduletabid'];
		$i=0;
		$this->db->select("$name,$id");
		$this->db->from($tablename);
		if($moduleid != '') {
			$this->db->where_in($whfield,$moduleid);
		}
		$this->db->where_in('uitypeid',array($value,26));
		$this->db->where_not_in('uitypeid',array(15,16));
		$this->db->where_not_in('active','No');
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				$data[$i] = array('datasid'=>$row->$id,'dataname'=>$row->$name);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//data fetch
	public function datafetchineditformmodel(){
		$id = $_GET['primarydataid'];
		$this->db->select('leadid,accountid,contactid,opportunityid');
		$this->db->from('leadconversionmapping');
		$this->db->where('leadconversionmapping.leadconversionmappingid',$id);
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				$data = array('leadid'=>$row->leadid,'accountid'=>$row->accountid,'contactid'=>$row->contactid,'opportunityid'=>$row->opportunityid);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//account-contact-opportunity field name check
	public function accountfieldnamecheckmodel(){
		$fieldid = $_POST['fieldid'];
		$primaryid = $_POST['primaryid'];
		$tablename = $_POST['tablename'];
		$tableid = $_POST['tableid'];
		if($fieldid != "") {
			$result = $this->db->select($tableid)->from($tablename)->where($tablename.'.'.$tableid,$fieldid)->where($tablename.'.status',1)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->$tableid;
				} 
			} else {
				echo "False";
			}
		} else {
			echo "False";
		}
	}
	//Widget data load
	public function widgetdatalaodfunmodel() {
		$moduleid = $_GET['moduleid'];
		$this->db->select('widget.widgetid,widget.widgetname,widget.moduleid,widget.widgetlabel');
		$this->db->from('relatedwidget');
		$this->db->join('widget','widget.widgetid=relatedwidget.widgetid');
		$this->db->where('widget.status',1);
		$this->db->where_in('relatedwidget.moduleid',$moduleid);
		$this->db->where_not_in('widget.moduleid',1);
		$this->db->order_by('widget.widgetname','asc');
		$result = $this->db->get();
		$i=0;
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->widgetid,'dataname'=>$row->widgetname,'widgetmoduleid'=>$row->moduleid,'widgetlabel'=>$row->widgetlabel,);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
}
?>