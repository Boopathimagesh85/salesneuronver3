<div class="large-12 columns paddingzero formheader">
	<?php
	$device = $this->Basefunctions->deviceinfo();
		$this->load->view('Base/mainviewaddformheader');
		$defaultrecordview = $this->Basefunctions->get_company_settings('mainviewdefaultview');
		echo hidden('mainviewdefaultview',$defaultrecordview);
		echo hidden('hiddencomputername',gethostbyaddr($_SERVER['REMOTE_ADDR']));

	?>
</div>
<div class="large-12 columns addformunderheader">&nbsp; </div>
<div class="large-12 columns scrollbarclass addformcontainer tabtouchcontainerheader">
	<div class="row mblhidedisplay">&nbsp;</div>
	<div id="subformspan1" class="hiddensubform">
		<form method="" name="tagprintform" class="cleardataform" id="tagprintform">
		<div id="tagprintvalidate" class="validationEngineContainer" >
		<div id="updatetagprintvalidate" class="validationEngineContainer" >
		<?php
		if($device=='phone') {
		?>
			<div class="large-4 columns" style="padding: 0px !important;">
				<div class="large-12 columns cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;background: #f3f2fa !important;box-shadow:none;">
					<div class="large-12 columns headerformcaptionstyle" style="background: #f3f2fa !important;">Tag Print Settings</div>
							
		<?php } else { ?>
	
			<div class="large-4 columns  paddingbtm">
				<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff">
					<div class="large-12 columns headerformcaptionstyle">Tag Print Settings</div>
		<?php } ?>
						<div class="input-field large-6 columns">
							<input id="templatename" class="" type="text" data-prompt-position="bottomLeft" tabindex="100" name="templatename"  data-table="tagtemplate" data-validatefield="tagtemplatename" data-validation-engine="validate[required,funcCall[varcheckuniquename],maxSize[100]]" data-primaryid="">
							<label for="templatename">Template Name<span class="mandatoryfildclass">*</span></label>
						</div>
						<div class="input-field large-6 columns">
							<input id="printername" class="" type="text" tabindex="106" name="printername" data-validation-engine="validate[required]" maxlength="200" data-errormessage="please enter shared printer name">
							<label for="printername">Printer Name<span class="mandatoryfildclass">*</span></label>
						</div>
						<div class="input-field large-6 columns">
							<input type="checkbox" class="checkboxcls radiochecksize filled-in" tabindex="112"   name="tagsetdefaultcboxid" id="tagsetdefaultcboxid" data-hidname="tagsetdefault" value="">
							<label for="tagsetdefaultcboxid">Default</label>
							<input name="tagsetdefault" type="hidden" id="tagsetdefault" value="" data-defvalattr="No">
					   </div>
						
						<div class="static-field large-6 columns">
							<label>Module<span class="mandatoryfildclass">*</span></label>
							<select class="chzn-select" data-placeholder="Select" data-prompt-position="bottomLeft:14,36" id="module" name="module" tabindex="107" data-validation-engine="validate[required]">
							<option value=""></option> 
								<?php foreach($module as $key):?>
								<option  value="<?php echo $key->moduleid;?>" ><?php echo $key->modulename;?></option>
								<?php endforeach;?>	
							</select>
						</div>
						<div class="static-field large-12 columns">
							<label>Related Module</label>
							<select class="chzn-select" data-prompt-position="" multiple id="relatedmoduleid" tabindex="108" name="relatedmoduleid">
								<option value=""></option>									
							</select>
							<input type="hidden" name="relatedmodule" id="relatedmodule">
						</div>
						<div class="static-field large-12 columns">
							<label>Field</label>
							<select class="chzn-select" data-placeholder="Select" data-prompt-position="bottomLeft" id="modulefield" name="modulefield" tabindex="109">
							<option value=""></option>
							</select>
						</div>
						<div class="static-field large-12 columns">
							<label>Where Clause</label>
							<select class="chzn-select" data-prompt-position="" id="whereclauseid" tabindex="108" name="whereclauseid">
								<option value=""></option>									
							</select>
						</div>
						<div class="input-field large-12 columns">
							<input id="mergetext" class="" type="text" data-prompt-position="bottomLeft" tabindex="110" name="mergetext" readonly>
							<label for="mergetext">Merge Text</label>
						</div>
						<div class="input-field large-6 columns">
							<input id="computername" class="" type="text" tabindex="106" name="computername" data-validation-engine="" maxlength="200" readonly>
							<label for="computername">Computer Name</label>
						</div>
						<div class="large-6 medium-6 small-6 columns">
							<label>Upload PRN File</label>
							<div id="tagfileuploadfromid" style="text-align:center; border:1px solid  #ffffff;">
							<span  class="" title="Click to upload" style="padding:2px; color:#2574a9; font-size:26px;"><span id=""><i class="material-icons">file_upload</i></span></span></div>
						</div>
						<input id="tagimage" type="hidden" name="tagimage" value="" tabindex="114">
						<span id="taglogomulitplefileuploader" style="display:none;">upload</span>
						<div class="input-field large-12 columns hidedisplay">
							<input id="printcommand" class="" type="text" data-prompt-position="bottomLeft" tabindex="111" name="printcommand" value="" data-validation-engine="validate[required]">
							<label for="printcommand">Print Command<span class="mandatoryfildclass">*</span></label>
						</div>
						<div class="static-field large-12 columns hidedisplay" >
							<label>Pre - Template</label>
							<select class="chzn-select" data-placeholder="Select" style="width:100%;display:inline-block" data-prompt-position="bottomLeft" id="printtemplate" name="printtemplate" tabindex="113">
							<option value=""></option>
							</select>
						</div>
						<div class="input-field large-6 medium-6 small-6 columns hidedisplay">
							<input id="prnfilename" class="" type="text" data-prompt-position="bottomLeft" tabindex="-1" name="prnfilename" value="" data-validation-engine="" readonly>
							<label for="prnfilename">PRN File Name</label>
						</div>
						<div class="large-12 columns hidedisplay">							
							<div id="tagsampleimage" class="large-12 columns uploadcontainerstyle" style="height:10rem; border:1px solid #CCCCCC">								
							</div>
						</div>
						<div class="large-6 medium-6 small-6 columns hidedisplay">
							<label>Upload Image File</label>
							<div id="tagimageuploadfromidbtw" style="text-align:center; border:1px solid  #ffffff;">
							<span  class="" title="Click to upload" style="padding:2px; color:#2574a9; font-size:26px;"><i class="material-icons">file_upload</i></span></div>
						</div>
						<input id="uploadimage" type="hidden" name="uploadimage" value="" tabindex="117">
						<span id="tagimagemulitplefileuploaderbtw" style="display:none;">upload</span>
						<div class="input-field large-6 medium-6 small-6 columns hidedisplay">
							<input id="barimagename" class="" type="text" data-prompt-position="bottomLeft" tabindex="-1" name="barimagename" value="" data-validation-engine="" readonly>
							<label for="barimagename">Image Name</label>
						</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns hidedisplay">							
							<div id="barsampleimage" class="large-12 columns uploadcontainerstyle" style="height:10rem; border:1px solid #CCCCCC">								
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="large-8 columns paddingbtm texteditor-container">
				<div class="large-12 columns paddingzero borderstyle cleardataform">
					<div class="large-12 columns viewgridcolorstyle" style="padding-left:0;padding-right:0;margin-top: 0px !important;">
						<div class="large-12 columns headerformcaptionstyle" style="padding: 6px 0px 0px 13px;">
							<span>Tag Editor</span>
						</div>
						<div class="large-12 columns" style="padding-left:0;padding-right:0;height:450px;background-color:#fff;">
							<textarea class="" name="tageditor"  id="tageditor" rows="20" cols="40" tabindex="118" spellcheck="false" data-validation-engine="" data-errormessage="please enter printer's prn format" style="height:450px;width:100% !important;"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
			//hidden text box view columns field module ids
			$modid = $this->Basefunctions->getmoduleid($filedmodids);
			echo hidden('viewfieldids',$modid);
			echo hidden('moduleviewid',$modid);
			echo hidden('sortorder','');
			echo hidden('sortcolumn','');
			echo hidden('primaryid','');
		?>
		</form>
	</div>
</div>