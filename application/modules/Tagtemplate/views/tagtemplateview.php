<!DOCTYPE HTML>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
	<?php $this->load->view('Base/basedeleteform'); ?>
</head>
    <body>
	<?php
		$moduleid = implode(',',$moduleids);
		$device = $this->Basefunctions->deviceinfo();
		$dataset['gridenable'] = "yes"; //no
		$dataset['griddisplayid'] = "tagtemplatecreationview";
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['spanattr'] = array();
		$dataset['gridtableid'] = "tagprintviewgrid";
		$dataset['griddivid'] = "tagprintviewgridnav";
		$dataset['moduleid'] = $moduleids;
		$dataset['forminfo'] = array(array('id'=>'tagtemplateformmadd','class'=>'hidedisplay','formname'=>'tagtemplateform'));
		if($device=='phone') {
			$this->load->view('Base/gridmenuheadermobile',$dataset);
			$this->load->view('Base/overlaymobile');
			$this->load->view('Base/viewselectionoverlay');
		} else {
			$this->load->view('Base/gridmenuheader',$dataset);
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
		$this->load->view('Base/basedeleteform');
	?>	
	<?php
		$this->load->view('Base/basedeleteformforcombainedmodules');
	?>	
	</body>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js File -->
	<script src="<?php echo base_url();?>js/plugins/filecomputer/fileupload.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/mention/mention.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/mention/bootstrap-typeahead.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/Tagtemplate/tagtemplate.js" type="text/javascript"></script>
</html>