<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tagtemplate extends CI_Controller {
	function __construct() 
	{
    	parent::__construct();
    	$this->load->helper('formbuild');
		$this->load->model('Tagtemplate/Tagtemplatemodel');
		$this->load->model('Base/Basefunctions');
    }
	public function index() 
	{		
		$moduleid = array(38);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['module']=$this->Basefunctions->simpledropdownwithcond('moduleid','modulename','module','moduleid',$this->Basefunctions->jewelmodules);
		//view
		$data['tagimage'] =$this->Tagtemplatemodel->simpledropdownwithcond('tagtemplateid','imgfile','tagtemplate','moduleid',50); 
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;	
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid); 
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Tagtemplate/tagtemplateview',$data);		
	}
	public function modulefieldload()
	{
		$viewfieldsmoduleids =array_filter(explode(',',$_POST['moduleid']));
		$viewfieldsreletedmoduleids =array_filter(explode(',',$_POST['relatedmoduleid']));
		$all_moduleid=array_merge($viewfieldsmoduleids,$viewfieldsreletedmoduleids);
		$data = $this->Tagtemplatemodel->modulefieldload($all_moduleid);
		echo $data;
	}
	public function tagtemplates()
	{		
		$data = $this->Tagtemplatemodel->tagtemplates();
		echo $data;
	}
	//editor value fetch 	
	public function gettemplatesource() 
	{
		$filename = trim($_GET['templatename']); 
		$newfilename = explode('/',$filename);
		if(read_file('printtemplate/tagprint/'.DIRECTORY_SEPARATOR.$newfilename[2])) {
			$tccontent = trim(file_get_contents('printtemplate/tagprint/'.DIRECTORY_SEPARATOR.$newfilename[2]));
			echo $tccontent;
		} else {
			$tccontent = "Fail";
			echo $tccontent;
		}
	}
	/*	*create tag template	*/
	public function createtagtemplate()
	{
		$this->Tagtemplatemodel->createtagtemplate();
	}
	/*	*delete tag template	*/
	public function tagprintdelete()
	{
		$this->Tagtemplatemodel->tagprintdelete();
	}
	/*	*retrieve tag template	*/
	public function retrievetemplate()
	{
		$this->Tagtemplatemodel->retrievetemplate();
	}
	/*	*update tag template	*/
	public function updatetagtemplate()
	{
		$this->Tagtemplatemodel->updatetagtemplate();
	}
	public function printerbrandname()
	{
		$this->Tagtemplatemodel->printerbrandname();
	}
	public function printermodelno()
	{
		$this->Tagtemplatemodel->printermodelno();
	}
	public function printertagno()
	{
		$this->Tagtemplatemodel->printertagno();
	}
	public function getmodule()
	{
		$this->Tagtemplatemodel->getmodule();
	}
	public function getcmd()
	{
		$this->Tagtemplatemodel->getcmd();
	}
	public function setdefaulttemplate()
	{
		$this->Tagtemplatemodel->setdefaulttemplate();
	}
	public function loadtagimage()
	{
		$tagimage =$this->Basefunctions->simpledropdownwithcond('tagtemplateid','imgfile','tagtemplate','moduleid',50);
          echo '<ul id="lightSlider" class="gallery list-unstyled cS-hidden">';
				$i=0;
				foreach($tagimage as $data ) {
					$i++;
				    echo '<li data-thumb="'.base_url().''.$data->imgfile.'"><image id="tagprintpreimg'.$i.'" src="'.base_url().''.$data->imgfile.'" data-id="'.$data->tagtemplateid.'" data-count="'.$i.'" alt="tagimage"></li>';
					}
				
				echo '</ul>';
	}
	public function loadautocompletedata()
	{
		$this->Tagtemplatemodel->loadautocompletedata();
	}
	public function checkmapping()
	{
		$this->Tagtemplatemodel->checkmapping();
	}
}