<?php
Class Tagtemplatemodel extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
    private $tagtemplatemodule = 38;
	//load the fields based on the tag
	public function modulefieldload($moduleid)
	{
		$printfield=[];
		$printfield1=[];
		$result_array=[];
		    $this->db->select('viewcreationmoduleid,viewcreationcolmodelname,viewcreationcolmodelaliasname,viewcreationcolmodelindexname,viewcreationcolumnname,fieldprintname,viewcreationcolumnid');
			$this->db->from('viewcreationcolumns');
			$this->db->where_in('viewcreationcolumns.viewcreationmoduleid',$moduleid);
			$this->db->where_not_in('viewcreationcolumns.moduletabsectionid',array(0));
			$this->db->where('viewcreationcolumns.status',$this->Basefunctions->activestatus);		
			$this->db->order_by('viewcreationcolumns.viewcreationcolumnid','asc');
			$data = $this->db->get()->result();
			$optgroupid=1;
			foreach($data as $info)
			{
					$printfield[] = array(
							              'pname'=>'Field Column',
							              'optgroupid'=>1,
										  'pid'=>$info->viewcreationcolumnid,
										  'id'=>$info->viewcreationmoduleid,
										  'label'=>$info->viewcreationcolumnname,			
										  'key'=>'{'.$info->fieldprintname.'}',			
									   );
			}
		return json_encode($printfield);
	}
	//tag print templates
	public function tagtemplates()
	{
		$model = '';
		$size = '';	
		$data = $this->db->select('tagtemplateid,tagtemplatename,image,filelocation')
					->from('tagtemplate')
					->where('status',1)
					->get();
					
		if($data->num_rows() > 0)
		{
			foreach($data->result() as $info)
			{
				$tagtemplate[]=array(
										'tagtemplateid'=>$info->tagtemplateid,
										'tagtemplatename'=>$info->tagtemplatename,
										'image'=>$info->image,
										'filelocation'=>$info->filelocation
									);
			}
		}
		else
		{
			$tagtemplate = array();
		}
		return json_encode($tagtemplate);
	}
	//create new prints
	public function createtagtemplate()
	{		
		$columnids = '';
		//retrieve fields from the prn's.
		$prn = trim($_POST['tageditor']);
		$insert = array(
							'tagtemplatename' =>trim($_POST['templatename']),//
							'fieldid' => $columnids,
							'moduleid' => trim($_POST['module']),
							'printername' => trim($_POST['printername']),
							'printcmd' => trim($_POST['printcommand']),
							'industryid'=>$this->Basefunctions->industryid,
							'branchid'=>$this->Basefunctions->branchid
						);
		$insert = array_filter(array_merge($insert,$this->Crudmodel->defaultvalueget()));
		$this->db->insert('tagtemplate',$insert);
		$primaryid = $this->db->insert_id();
		// move file from tagtemp to tagprint 
		if(!empty($_POST['tagimage']))
		{
			    $tagtemplateprn = explode('/',$_POST['tagimage']);
				$exe=explode('.',$tagtemplateprn[2]);
				$prn_file=$primaryid.'.'.$exe[1];		
				copy('printtemplate/tagtemp/'.$tagtemplateprn[2], 'printtemplate/tagprint/'.$prn_file.''); 
				$prn_filelocation='printtemplate/tagprint/'.$prn_file.'';
		} else {
			    $prn_filelocation='';
		}
		if(!empty($_POST['tagbtw'])) {
				$tagtemplatebtw = explode('/',$_POST['tagbtw']);
				$exe_btw=explode('.',$tagtemplatebtw[2]);
			   $prn_file_btw=$primaryid.'.'.$exe_btw[1];		
				copy('printtemplate/tagtemp/'.$tagtemplatebtw[2], 'printtemplate/tagprint/'.$prn_file_btw.''); 
				$btw_filelocation='printtemplate/tagprint/'.$prn_file_btw.'';
		} else {
			    $btw_filelocation='';
		}
		// move image from tagtemp to tagprint 
		if(!empty($_POST['uploadimage']))
		{
			    $imagetemplateprn = explode('/',$_POST['uploadimage']);
				$exe=explode('.',$imagetemplateprn[2]);
				$img_file=$primaryid.'.'.$exe[1];		
				copy('printtemplate/tagtemp/'.$imagetemplateprn[2], 'printtemplate/tagprint/'.$img_file.''); 
				$img_filelocation='printtemplate/tagprint/'.$img_file.'';
		} else {
			    $img_filelocation='';
		}
		$files = glob('printtemplate/tagtemp/*'); 
		foreach($files as $file){ // iterate files
			  if(is_file($file))
				unlink($file); // delete file
        }
		$updates=array(
							'prnfile'=>$prn_filelocation,
							'btwfile'=>$btw_filelocation,
							'imgfile'=>$img_filelocation,
							'default'=>$_POST['tagsetdefault'],
							'filelocation' => $this->generatefile($prn,$primaryid)//file creation
		);
		$this->db->where('tagtemplateid',$primaryid);
		$this->db->update('tagtemplate',$updates);
		// remaining default template is unset 
		if($_POST['tagsetdefault'] == 'Yes') {
			$updaterevert=array(
			  'default' => 'No'		
			);
				$this->db->where('tagtemplateid !=',$primaryid);
				$this->db->update('tagtemplate',$updaterevert);
		}
		// notification log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' created Tagtemplate - '.trim($_POST['templatename']).'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,$this->tagtemplatemodule);
		echo 'SUCCESS';
	}
	//create a new files
	public function generatefile($tempcontent,$temp_id) 
	{
		$print_file_name='';		
		if($tempcontent!='') 
		{
			$print_file_name = 'printtemplate/tagprint/'.trim($temp_id).'.txt';
			@chmod($print_file_name,0766);
			@write_file($print_file_name,$tempcontent);			
		}
		return $print_file_name;
	}
	//tag template's
	public function tagprintdelete()
	{
		$id=$_GET['primaryid'];
		$createuserid = $this->Basefunctions->singlefieldfetch('createuserid','tagtemplateid','tagtemplate',$id);
		if($createuserid != 1)
		{
			//delete tagtemplate-main
			$delete = $this->Crudmodel->deletedefaultvalueget();
			$this->db->where('tagtemplateid',$id);
			$this->db->update('tagtemplate',$delete);
			// remove prn,txt,btw,image file from print template
			$files = 'printtemplate/tagprint/'.$id.'.*';			
			array_map( "unlink", glob( $files ) );
			// notification log
			$tagtemplatename = $this->Basefunctions->singlefieldfetch('tagtemplatename','tagtemplateid','tagtemplate',$id);
			$userid = $this->Basefunctions->logemployeeid;
			$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
			$activity = ''.$user.' deleted Tag Template - '.$tagtemplatename.'';
			$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,$this->tagtemplatemodule);
			echo 'SUCCESS';
		}
		else
		{
			echo 'DEFAULT';
		}
	}
	/*	* retrieve printt template edit	*/
	public function retrievetemplate()
	{
		$id = trim($_GET['primarydataid']);
		$moduleid = $this->Basefunctions->singlefieldfetch('moduleid','tagtemplateid','tagtemplate',$id);
		if($moduleid == 50) { // stock entry
			$table = 'itemtag';
			$whfield = 'tagtemplateid';
			$this->db->select($table."id");
			$this->db->from($table);
			$this->db->where($whfield,$id);
			$this->db->where('status',$this->Basefunctions->activestatus);
			$info_last=$this->db->get();
		}else if($moduleid == 91) { // chitbook
			$this->db->select('chitbook.chitbookid');
			$this->db->from('chitbook');
			$this->db->join('chitscheme','chitbook.chitschemeid = chitscheme.chitschemeid');
			$this->db->where('chitscheme.regtemplateid',$id);
			$this->db->where('chitscheme.status',$this->Basefunctions->activestatus);
			$this->db->where('chitbook.status',$this->Basefunctions->activestatus);
			$info_last=$this->db->get();
		}else if($moduleid == 51) { // chit entry
			$this->db->select('chitentry.chitentryid');
			$this->db->from('chitentry');
			$this->db->join('chitscheme','chitentry.chitschemeid = chitscheme.chitschemeid');
			$this->db->where('chitscheme.entrytemplateid',$id);
			$this->db->where('chitscheme.status',$this->Basefunctions->activestatus);
			$this->db->where('chitentry.status',$this->Basefunctions->activestatus);
			$info_last=$this->db->get();
		}		
		$this->db->select('tagtemplate.tagtemplatename,tagtemplate.fieldid,tagtemplate.image,tagtemplate.filelocation,tagtemplate.moduleid,tagtemplate.default,tagtemplate.printername,tagtemplate.printcmd,tagtemplate.prnfile,tagtemplate.btwfile,tagtemplate.imgfile');
		$this->db->from('tagtemplate');
		$this->db->where('tagtemplate.tagtemplateid',$id);
		$this->db->where('tagtemplate.status',$this->Basefunctions->activestatus);
		$result = $this->db->get();	
		if($result->num_rows() > 0)
		{
			foreach($result->result() as $row)
			{
				$data = array(
								'printername'=>$row->printername,
								'printcommand'=>$row->printcmd,
								'prnfilename'=>$row->prnfile,
								'tagsetdefault'=>$row->default,
								'btwfilename'=>$row->btwfile,
								'barimagename'=>$row->imgfile,
								'module'=>$row->moduleid,
								'templatename'=>$row->tagtemplatename,
								'filelocation'=>$row->filelocation,
								'image'=>$row->image
							);
			}
			echo json_encode($data);
		} 
		else 
		{
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	/*	*update the print template	*/
	public function updatetagtemplate()
	{
		$tagtemplateid = $_POST['tagtemplateid'];
		$columnids = '';
		//retrieve fields from the prn's.
		$prn = trim($_POST['tageditor']);		
		// move file from tagtemp to tagprint 
		$tagtemplateprn = $_POST['tagimage'];
		if(!empty($_POST['tagimage']))
		{
				$newfilename = explode('/',$tagtemplateprn);
				$exe=explode('.',$newfilename[2]);
				$prn_file=$tagtemplateid.'.'.$exe[1];		
				copy('printtemplate/tagtemp/'.$newfilename[2], 'printtemplate/tagprint/'.$prn_file.''); 
				$prn_filelocation='printtemplate/tagprint/'.$prn_file.'';
		} else {
			    $prn_filelocation='';
		}
		if(isset($_POST['tagbtw'])) {
			$_POST['tagbtw'] = $_POST['tagbtw'];
		} else {
			$_POST['tagbtw'] = '';
		}
		$tagtemplatebtw = $_POST['tagbtw'];
		if(!empty($_POST['tagbtw'])) {
				$newfile_btw = explode('/',$tagtemplatebtw);
				$exe_btw=explode('.',$newfile_btw[2]);
			   $prn_file_btw=$tagtemplateid.'.'.$exe_btw[1];		
				copy('printtemplate/tagtemp/'.$newfile_btw[2], 'printtemplate/tagprint/'.$prn_file_btw.''); 
				$btw_filelocation='printtemplate/tagprint/'.$prn_file_btw.'';
		} else {
			    $btw_filelocation='';
		}
		// move image from tagtemp to tagprint 
		if(!empty($_POST['uploadimage']))
		{
			    $imagetemplateprn = explode('/',$_POST['uploadimage']);
				$exe=explode('.',$imagetemplateprn[2]);
				$img_file=$tagtemplateid.'.'.$exe[1];		
				copy('printtemplate/tagtemp/'.$imagetemplateprn[2], 'printtemplate/tagprint/'.$img_file.''); 
				$img_filelocation='printtemplate/tagprint/'.$img_file.'';
		} else {
			    $img_filelocation='';
		}
		$files = glob('printtemplate/tagtemp/*'); 
		foreach($files as $file){ // iterate files
			  if(is_file($file))
				unlink($file); // delete file
        }
		$updates = array(
							'tagtemplatename' =>trim($_POST['templatename']),//
							'fieldid' => $columnids,
							'filelocation' => $this->generatefile($prn,$tagtemplateid),//file creation
							'moduleid' => trim($_POST['module']),
							'printcmd' => trim($_POST['printcommand']),
							'prnfile'=>$prn_filelocation,
							'btwfile'=>$btw_filelocation,
							'imgfile'=>$img_filelocation,
							'default'=>$_POST['tagsetdefault'],
							'printername' => trim($_POST['printername']),
							'industryid'=>$this->Basefunctions->industryid,
							'branchid'=>$this->Basefunctions->branchid
						);
		$updates = array_filter(array_merge($updates,$this->Crudmodel->updatedefaultvalueget()));
		$this->db->where('tagtemplateid',$tagtemplateid);
		$this->db->update('tagtemplate',$updates);
		// unset default template other
		if($_POST['tagsetdefault'] == 'Yes') {
			$updaterevert=array(
			  'default' => 'No'		
			);
				$this->db->where('tagtemplateid !=',$tagtemplateid);
				$this->db->update('tagtemplate',$updaterevert);
		}
		// notification log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' Updated Tagtemplate - '.trim($_POST['templatename']).'';
		$this->Basefunctions->notificationcontentadd($tagtemplateid,'Added',$activity ,$userid,$this->tagtemplatemodule);
		echo 'SUCCESS';
	}
	public function getmodule() {
		$this->db->select('moduleid,modulename');
		$this->db->from('module');
		$this->db->where("FIND_IN_SET('TP',showin) >", 0);
		$this->db->where("FIND_IN_SET('$this->Basefunctions->industryid',module.industryid) >", 0);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$getmodule=$this->db->get()->result();
		$i=0;
		foreach($getmodule as $getmodule_data) {
			$result[]=array(
			'moduleid' => $getmodule_data->moduleid,
			'modulename' => $getmodule_data->modulename
			);
		}
		echo json_encode($result);
	}
	public function getcmd()
	{
		$cmd=$this->db->where('status',3)->get('tagtemplate')->row()->printcmd;
		echo json_encode($cmd);
	}
	public function setdefaulttemplate(){
		$templateid = $_POST['tagtemplateid'];
		// specific template is set default
        $update=array(
          'default' => 'Yes'		
		);
			$this->db->where('tagtemplateid',$templateid);
			$this->db->update('tagtemplate',$update);
		// remaining template is set undefault
		$updaterevert=array(
          'default' => 'No'		
		);
			$this->db->where('tagtemplateid !=',$templateid);
			$this->db->update('tagtemplate',$updaterevert);
		echo 'SUCCESS';
	}
	public function loadautocompletedata(){
		$result_array=[];
		$round_array=[];
		$viewcreation_Columnarray=[];
		$this->db->select('viewcreationcolumns.viewcreationcolumnid,viewcreationcolumns.viewcreationcolumnname,viewcreationcolumns.viewcreationcolmodeltable,viewcreationcolumns.viewcreationcolmodelname');
		$this->db->from('viewcreationcolumns');
		$this->db->where('status',$this->Basefunctions->activestatus);
		$viewcreationarray=$this->db->get();
		foreach($viewcreationarray->result() as $data){
			$viewcreation_Columnarray[]=array('name' =>$data->viewcreationcolumnname,'username'=>'VC'.'.'.$data->viewcreationcolumnid.'.'.$data->viewcreationcolumnname);
		}
		$this->db->select('mergetext.mergetextid,mergetext.mergetextname');
		$this->db->from('mergetext');
		$this->db->where('status',$this->Basefunctions->activestatus);
		$info_last=$this->db->get();
		foreach($info_last->result() as $data){
			$round_array[]=array('name' =>$data->mergetextname,'username'=>'MT'.'.'.$data->mergetextid.'.'.$data->mergetextname);
		}
		$result_array=array_merge($viewcreation_Columnarray,$round_array);
		echo json_encode($result_array);
	}
	//simple drop down value fetch with condition
	public function simpledropdownwithcond($did,$dname,$table,$whfield,$whdata) {
		$i=0;
		$mvalu=explode(",",$whdata);
		$this->db->select("$dname,$did");
		$this->db->from($table);
		if($whdata != '') {
			if(count($mvalu)>=2) {
				$this->db->where_in($whfield,$mvalu);
			} else {
				$this->db->where("FIND_IN_SET('$whdata',$whfield) >", 0);
			}
		}
		$this->db->where('status',1);
		$this->db->order_by($dname,"asc");
		$result = $this->db->get();
		return $result->result();
	}
	public function checkmapping() {
		$info_last = FALSE;
		$templateid = $_GET['datarowid'];
		$moduleid = $this->Basefunctions->singlefieldfetch('moduleid','tagtemplateid','tagtemplate',$templateid);
		if($moduleid == 50) { // stock entry
			$table = 'itemtag';
			$whfield = 'tagtemplateid';
			$this->db->select($table."id");
			$this->db->from($table);
			$this->db->where($whfield,$templateid);
			$this->db->where('status',$this->Basefunctions->activestatus);
			$info_last=$this->db->get();
		}else if($moduleid == 91) { // chitbook
			$this->db->select('chitbook.chitbookid');
			$this->db->from('chitbook');
			$this->db->join('chitscheme','chitbook.chitschemeid = chitscheme.chitschemeid');
			$this->db->where('chitscheme.regtemplateid',$templateid);
			$this->db->where('chitscheme.status',$this->Basefunctions->activestatus);
			$this->db->where('chitbook.status',$this->Basefunctions->activestatus);
			$info_last=$this->db->get();
		}else if($moduleid == 51) { // chit entry
			$this->db->select('chitentry.chitentryid');
			$this->db->from('chitentry');
			$this->db->join('chitscheme','chitentry.chitschemeid = chitscheme.chitschemeid');
			$this->db->where('chitscheme.entrytemplateid',$templateid);
			$this->db->where('chitscheme.status',$this->Basefunctions->activestatus);
			$this->db->where('chitentry.status',$this->Basefunctions->activestatus);
			$info_last=$this->db->get();
		}
		if ($info_last !== FALSE) {
			if ($info_last->num_rows() > 0) {
				echo json_encode($info_last->num_rows());
			}  else {
				echo json_encode(0);
			}
		} else {
			echo json_encode(0);
		}
	}
}