<?php
Class Stocktakingmodel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->model('Base/Crudmodel');
		$this->load->library('csvreader');
    }
	// Expected details at the time of load button
	public function expecteddetailsmodel() {
		$weightround =  $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$counter_level = $this->Basefunctions->get_company_settings('counter_level');
		$category_level = $this->Basefunctions->get_company_settings('category_level');
		$branchid = $_POST['branchid'];
		$purityid = $_POST['purityid'];
		if(empty($_POST['categoryid'])) {
			$categoryid ='';
		}else {
			$categoryid = $_POST['categoryid'];
		}
		if(empty($_POST['storageid'])) {
			$storageid ='';
		}else {
			$storageid = $_POST['storageid'];
		}
		if(empty($_POST['productid'])) {
			$productid ='';
		}else {
			$productid = $_POST['productid'];
		}
		if(empty($_POST['lotnumberids'])) {
			$lotnumberids ='';
		}else {
			$lotnumberids = $_POST['lotnumberids'];
		}
		$startdate = $_POST['startdate'];
		$enddate = $_POST['enddate'];
		if($startdate != '') { 
			$startdate =  $this->Basefunctions->ymddateconversion($startdate);
		} else {
			$startdate = date("Y-m-d");
		}
		if($enddate != '') { 
			$enddate =  $this->Basefunctions->ymddateconversion($enddate);
		} else {
			$enddate = date("Y-m-d");
		}
		if($startdate==$enddate){
			$stime = '00:00:00';
			$etime = '23:59:59';
		} else {
			$stime = '00:00:00';
			$etime = '23:59:59';
		}
		$sessionmodeid = $_POST['sessionmodeid'];
		$counterselect = '';
		$categoryselect = '';
		// counter join
		$k = 0;
		if(!empty($storageid)) {
			if($counter_level > 1) {
				for($i=$counter_level; $i >=1 ; $i--) {
					if($counter_level == $i) {
						$counterjoin = "LEFT JOIN counter as counter$i ON counter$i.counterid = itemtag.counterid";
						$counterselect = "counter$i.countername";
						$storagewhere = " AND (itemtag.counterid in ($storageid) ";
					}else {
						$k = $i + 1;
						$counterjoin .= " LEFT JOIN counter as counter$i ON counter$i.counterid = counter$k.parentcounterid";
						$storagewhere = $storagewhere." or counter$i.counterid in ($storageid) ";
						
					}
				}
				$storagewhere .= ')';
			} else {
				$counterjoin = "LEFT JOIN counter as counter ON counter.counterid = itemtag.counterid";
				$counterselect = "counter.countername";
				$storagewhere = " AND itemtag.counterid in ($storageid) ";
			}
		}else {
			$counterjoin = "LEFT JOIN counter as counter ON counter.counterid = itemtag.counterid";
			$counterselect = "counter.countername";
			$storagewhere = "";
		}
		// category join
		$j = 0;
		if(!empty($categoryid)) {
			if($category_level > 1) {
				for($m=$category_level; $m >=1 ; $m--) {
					if($category_level == $m) {
						$categoryjoin = "LEFT JOIN category as category$m ON category$m.categoryid = product.categoryid";
						$categoryselect = "category$m.categoryname";
						$categorywhere = "AND (product.categoryid in ($categoryid)";
					}else {
						$j = $m + 1;
						$categoryjoin .= " LEFT JOIN category as category$m ON category$m.categoryid = category$j.parentcategoryid";
						$categorywhere = $categorywhere." or category$m.categoryid in ($categoryid) ";
					}
				}
			}
			$categorywhere .= ')';
		}else {
			$categoryjoin = "LEFT JOIN category as category ON category.categoryid = product.categoryid";
			$categoryselect = "category.categoryname";
			$categorywhere = "";
		} 
		
		if(!empty($productid)) {
			$productwhere = "AND itemtag.productid in ($productid)";
		}else {
			$productwhere ="AND 1=1";
		}
		$lotjoin = "LEFT JOIN lot as lot ON lot.lotid = itemtag.lotid";
		if(!empty($lotnumberids)) {
			$lotselect = "lot.lotid,lot.lotnumber";
			$lotwhere = "AND lot.lotid in ($lotnumberids)";
		}else {
			$lotselect = "lot.lotid,lot.lotnumber";
			$lotwhere = "";
		}
		
		$data = $this->db->query("SELECT `product`.`productname`, `itemtag`.`itemtagnumber`, `itemtag`.`grossweight`, `itemtag`.`netweight`, `itemtag`.`productid`, `itemtag`.`counterid`, `itemtag`.`rfidtagno`, `product`.`categoryid`, `purity`.`purityname`,$counterselect,$categoryselect,$lotselect FROM `itemtag` JOIN `product` ON `product`.`productid` = `itemtag`.`productid` JOIN `purity` ON `purity`.`purityid` = `itemtag`.`purityid` $categoryjoin  $counterjoin $lotjoin WHERE `itemtag`.`itemtagnumber` != '' AND `itemtag`.`purityid` IN(".$purityid.") AND `itemtag`.`status` = 1 AND `itemtag`.`branchid` = ".$branchid." AND `itemtag`.`tagtypeid` IN(2, 4, 5) $storagewhere $productwhere $categorywhere $lotwhere group by itemtag.itemtagid");
		
		if($data->num_rows()>0){
			foreach($data->result() as $info){
				if($_POST['rfidbarcode']  == 1){
					$barcodeid = $info->rfidtagno;
				}else{
					$barcodeid = $info->itemtagnumber;
				}
				$result[] =array('productname'=>$info->productname,'barcodeid'=>$barcodeid,'grosswt'=>number_format((float)$info->grossweight, $weightround, '.', ''),'netwt'=>number_format((float)$info->netweight, $weightround, '.', ''),'productid'=>$info->productid,'counterid'=>$info->counterid,'categoryid'=>$info->categoryid,'purityname'=>$info->purityname,'countername'=>$info->countername,'categoryname'=>$info->categoryname,'rfidtagno'=>$info->rfidtagno,'itemtagnumber'=>$info->itemtagnumber,'lotid'=>$info->lotid,'lotnumber'=>$info->lotnumber);	
					
			}
		}else{
			$result = '';
		}
		echo json_encode($result);
	}
	public function conflictdata(){
		$weightround =  $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$_POST['modeid'] = 1;
		$this->db->select('product.productname,itemtag.itemtagnumber,itemtag.rfidtagno,itemtag.grossweight,itemtag.netweight,itemtag.productid,itemtag.counterid,product.categoryid');
        $this->db->join('product','product.productid = itemtag.productid');
		$this->db->join('counter','counter.counterid = itemtag.counterid');
        $this->db->where('itemtag.status',$this->Basefunctions->activestatus);
		if($_POST['rfidbarcode']  == 1){
			$this->db->where('itemtag.rfidtagno',$_POST['barcodeid']);
		}else{
			$this->db->where('itemtag.itemtagnumber',$_POST['barcodeid']);
		}
		$this->db->from('itemtag');
        $data = $this->db->get();
		$info = $data->row();
		if($data->num_rows()>0){
				if($_POST['rfidbarcode']  == 1){
					$barcodeid = $info->rfidtagno;
				}else{
					$barcodeid = $info->itemtagnumber;
				}
				$result =array('productname'=>$info->productname,'barcodeid'=>$barcodeid,'grosswt'=>number_format((float)$info->grossweight, $weightround, '.', ''),'netwt'=>number_format((float)$info->netweight, $weightround, '.', ''),'productid'=>$info->productid,'counterid'=>$info->counterid,'categoryid'=>$info->categoryid,'status'=>1);	
		}else{
			$result =array('productname'=>'','barcodeid'=>$_POST['barcodeid'],'grosswt'=>'','netwt'=>'','productid'=>'','counterid'=>'','categoryid'=>'','status'=>0);
		}
		echo json_encode($result);
	}
    public function stocktakingcreate(){
		$stocktakingid = $_POST['stocktakingid'];
		if(!empty($_POST['prevstocktakingsessionid'])){
			$newscannedarray = array('stocktakingsessionid'=>$_POST['stocktakingsessionid'],'status' => 2);  // scanned status
				$this->db->where('stocktakingid',$stocktakingid);
				$this->db->where_in('barcodeid',explode(',',$_POST['newscannedid']));
				$this->db->update('stocktakingdetail',$newscannedarray);
			$stocksessionarray = array('status' => 2);  // scanned status 
			$this->db->where('stocktakingsessionid',$_POST['stocktakingsessionid']);
			$this->db->update('stocktakingsession',$newscannedarray);
			$takingdata = $this->db->where('stocktakingid',$stocktakingid)->where_not_in('status',2)->get('stocktakingdetail')->num_rows();
			 if($takingdata == 0){  // all data scanned for particular stocktaking id
				$stocktakingarray = array('stocktakingstatus' => 0);  // scanned status 
				$this->db->where('stocktakingid',$stocktakingid);
				$this->db->update('stocktaking',$stocktakingarray); 	 
			}
		}else{
			$barcodeid = explode(',',$_POST['expectedid']);
			$barcodecount = count($barcodeid);
			for($i=0; $i<$barcodecount; $i++){
						$stocktaking_detail = array(
							'stocktakingid'=>$stocktakingid,
							'stocktakingsessionid'=>$_POST['stocktakingsessionid'],
							'barcodeid'=>$barcodeid[$i],
							'status'=>3,  
							'createdate' => date($this->Basefunctions->datef),
							'lastupdatedate' => date($this->Basefunctions->datef),
							'createuserid' => $this->Basefunctions->userid,
							'lastupdateuserid' => $this->Basefunctions->userid
							);
				$this->db->insert('stocktakingdetail',array_filter($stocktaking_detail));
			}
			$scannedarray = array('status' => 2);  // scanned status
			$this->db->where('stocktakingid',$stocktakingid);
			$this->db->where_in('barcodeid',explode(',',$_POST['scannedid']));
			$this->db->update('stocktakingdetail',$scannedarray);
	
			$stocksessionarray = array('status' => 2);  // scanned status 
			$this->db->where('stocktakingsessionid',$_POST['stocktakingsessionid']);
			$this->db->update('stocktakingsession',$stocksessionarray);
		}
		if($_POST['conflicttrackid'] == 2) {
			if(!empty($_POST['conflictid'])){
				$barcode_conflictid = explode(',',$_POST['conflictid']);
				$barcodeconflictcount = count($barcode_conflictid);
				for($j=0; $j<$barcodeconflictcount; $j++){
					$stocktaking_conflict_detail = array(
								'stocktakingid'=>$stocktakingid,
								'stocktakingsessionid'=>$_POST['stocktakingsessionid'],
								'barcodeid'=>$barcode_conflictid[$j],
								'status'=>4,   // conflict status
								'createdate' => date($this->Basefunctions->datef),
								'lastupdatedate' => date($this->Basefunctions->datef),
								'createuserid' => $this->Basefunctions->userid,
								'lastupdateuserid' => $this->Basefunctions->userid
					 );
					 $this->db->insert('stocktakingdetail',array_filter($stocktaking_conflict_detail));
				}
			}
		}
		$stocktakingsessionid = $_POST['stocktakingsessionid'];
		$summaryupdate = $this->db->query("select count(CASE WHEN status in (2) then stocktakingdetailid end) as scannedcount,count(CASE WHEN status in (3) then stocktakingdetailid end) as missingcount,count(CASE WHEN status in (4) then stocktakingdetailid end) as conflictcount from stocktakingdetail where stocktakingsessionid = $stocktakingsessionid and status !=0");
		if($summaryupdate->num_rows() > 0) {
			foreach($summaryupdate->result() as $infoo) {
				$carray = array(
					'scannedcount'=>$infoo->scannedcount,
					'missingcount'=>$infoo->missingcount,
					'expectedcount'=>$infoo->scannedcount+$infoo->missingcount,
					'conflictcount'=>$infoo->conflictcount
				);
				$this->db->where('stocktakingsessionid',$_POST['stocktakingsessionid']);
				$this->db->update('stocktakingsession',$carray);
			}
		}
		$this->stocktakingcountupdate($stocktakingid);
		$takingdata = $this->db->where('stocktakingid',$stocktakingid)->where_not_in('status',2)->get('stocktakingdetail')->num_rows();
		if($takingdata == 0){  // all data scanned for particular stocktaking id
			$stocktakingarray = array('stocktakingstatus' => 0);  // scanned status 
			$this->db->where('stocktakingid',$stocktakingid);
			$this->db->update('stocktaking',$stocktakingarray); 	 
		}
		//}
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' created Stock Taking';
		$this->Basefunctions->notificationcontentadd($stocktakingsessionid,'Added',$activity ,$userid,53);
		echo 'SUCCESS';
	}
	// stock taking count update
	public function stocktakingcountupdate($stocktakingid) {
		$allsummaryupdate = $this->db->query("select sum(expectedcount) as expectedcount,sum(scannedcount) as scannedcount,sum(missingcount) as missingcount,sum(conflictcount) as conflictcount  from stocktakingsession where stocktakingid = $stocktakingid and status !=0");
		if($allsummaryupdate->num_rows() > 0) {
			foreach($allsummaryupdate->result() as $infoo) {
				$allcarray = array(
					'scannedcount'=>$infoo->scannedcount,
					'missingcount'=>$infoo->missingcount,
					'expectedcount'=>$infoo->expectedcount,
					'conflictcount'=>$infoo->conflictcount
				);
				$this->db->where('stocktakingid',$stocktakingid);
				$this->db->update('stocktaking',$allcarray);
			}
		}
	}
	public function stocktakingdelete(){
	        $id=$_POST['primaryid'];
			$sessionid = $_POST['stocktakingsessionid'];
			$delete = $this->Basefunctions->delete_log();
			if($_POST['deletemode'] == 2) { // All Data
				$this->db->where('stocktakingid',$id);  // stock taking delete
				$this->db->update('stocktaking',$delete);
				$this->db->where('stocktakingid',$id);  // stock taking session inactive
				$this->db->update('stocktakingsession',$delete);
				$this->db->where('stocktakingid',$id);  // stock taking details inactive
				$this->db->update('stocktakingdetail',$delete);
			}else if($_POST['deletemode'] == 3) {
				$this->db->where('stocktakingsessionid',$sessionid);  // stock taking session inactive
				$this->db->update('stocktakingsession',$delete);
				$this->db->where('stocktakingsessionid',$sessionid);  // stock taking details inactive
				$this->db->update('stocktakingdetail',$delete);
				$this->stocktakingcountupdate($id);
				/* // all session data are inactive delete main table entry
				$data = $this->db->select('stocktakingsessionid')
								->from('stocktakingsession')
								->where('stocktakingid',$id)
								->where('status',2)
								->get();
				 if($data->num_rows() == 0) {
					 $this->db->where('stocktakingid',$id);  // stock taking delete
					 $this->db->update('stocktaking',$delete,$this->Crudmodel->updatedefaultvalueget());
				 } */
			}
			echo 'SUCCESS';
	}
	public function stocktakingsessioncreate(){
		$_POST['purity'] = trim($_POST['purity'],",");
		$_POST['sessiontypeid'] = trim($_POST['sessiontypeid'],",");
		if($_POST['stocksessionmethodid'] == 2 && $_POST['stocksessiondeviceno'] != '') {
			$_POST['stocksessiondeviceno'] = $_POST['macaddress'];
		} else {
			$_POST['stocksessiondeviceno'] = $_POST['stocksessiondeviceno'];
		}
		$header = array(
				'stocktakingdate' => $this->Basefunctions->ymd_format($_POST['stocktakinsessiondate']),
				'stocksessionmodeid' => $_POST['stocksessionmodeid'],
				'purityid' => $_POST['purity'],
				'branchid'=>$_POST['branchid'],
				'branchid'=>$_POST['branchid'],
				'employeeid'=>$_POST['employeeid'],
				'stocktakingstatus'=>1,
				'stocktakingnumber'=>$_POST['stocktakingsessionid'],
				'stocktakingtypeid'=>$_POST['sessiontypeid'],
				'itemtagdatatypeid'=>$_POST['itemtagdatatypeid'],
				'deviceno'=>$_POST['stocksessiondeviceno'],
				'rfiddeviceid'=>$_POST['rfiddeviceid'],
				'connectioncheck'=>$_POST['connectioncheck'],
				'startstoptrigger'=>$_POST['startstoptrigger'],
				'stocksessionmethodid'=>$_POST['stocksessionmethodid'],
				'conflicttrackid'=>$_POST['conflicttrackid'],
				'industryid'=>$this->Basefunctions->industryid
			);
			$header_insert = array_merge($header,$this->Crudmodel->defaultvalueget());
			$this->db->insert('stocktaking',array_filter($header_insert));
			$stocktakingid = $this->db->insert_id();
			//audit-log
			$userid = $this->Basefunctions->logemployeeid;
			$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
			$activity = ''.$user.' created StockTaking Session - '.$_POST['stocktakingsessionid'].'';
			$this->Basefunctions->notificationcontentadd($stocktakingid,'Added',$activity ,$userid,103);
			echo 'SUCCESS';
	}
	public function loadstockcode()
	{
		$result=array();
		$data = $this->db->select('stocktaking.stocktakingid,stocktaking.stocktakingnumber,stocktaking.stocktakingtypeid,stocktaking.branchid,stocktaking.purityid,stocktaking.stocksessionmodeid,stocktaking.deviceno,stocktaking.stocksessionmethodid,stocktaking.conflicttrackid,stocktaking.itemtagdatatypeid,stocktaking.rfiddeviceid,connectioncheck,startstoptrigger')
		->from('stocktaking')
		->where('stocktaking.status',1)
		->group_by('stocktaking.stocktakingid')
		->order_by('stocktaking.stocktakingid','DESC')
		->get();
		 if($data->num_rows() >0) {
			foreach($data->result() as $row) {
				$result[]=array('stocktakingnumber'=>$row->stocktakingnumber,'stocktakingtypeid'=>$row->stocktakingtypeid,'branchid'=>$row->branchid,'purityid'=>$row->purityid,'stocktakingid'=>$row->stocktakingid,'stocksessionmodeid'=>$row->stocksessionmodeid,'deviceno'=>$row->deviceno,'stocksessionmethodid'=>$row->stocksessionmethodid,'conflicttrackid'=>$row->conflicttrackid,'itemtagdatatypeid'=>$row->itemtagdatatypeid,'rfiddeviceid'=>$row->rfiddeviceid,'connectioncheck'=>$row->connectioncheck,'startstoptrigger'=>$row->startstoptrigger);
			}
		} else {
		   $result ='';
		}
		echo json_encode($result);
	}
	// load session data
	public function loadsessiondata()
	{
		$stocktakingid = $_POST['stocktakingid'];
		$result=array();
		$data = $this->db->select('stocktakingsession.stocktakingsessionid,stocktaking.stocktakingnumber,product.productname,counter.countername,category.categoryname')
		->join('stocktaking','stocktaking.stocktakingid = stocktakingsession.stocktakingid')
		->join('product','product.productid = stocktakingsession.productid')
		->join('counter','counter.counterid = stocktakingsession.counterid')
		->join('category','category.categoryid = stocktakingsession.categoryid')
		->from('stocktakingsession')
		->where('stocktakingsession.status !=',0)
		->where('stocktakingsession.stocktakingid',$stocktakingid)
		->group_by('stocktakingsession.stocktakingsessionid')
		->get();
		 if($data->num_rows() >0) {
			foreach($data->result() as $row) {
				$result[]=array('stocktakingnumber'=>$row->stocktakingnumber,'stocktakingsessionid'=>$row->stocktakingsessionid,'productname'=>$row->productname,'countername'=>$row->countername,'categoryname'=>$row->categoryname);
			
			}
		}else{
		   $result ='';
		}
		echo json_encode($result);
	}
	public function checksessiontable(){
		$empname = 'admin';
		$empcreatedate = '';
		$stocktakingid = $_POST['stocktakingid'];
		if(empty($_POST['storageid'])) {
		   $_POST['storageid'] = 1;
		} else {
		   $_POST['storageid'] = $_POST['storageid'];
		}
		if(empty($_POST['categoryid'])) {
		   $_POST['categoryid'] = 1;
		} else {
		   $_POST['categoryid'] = $_POST['categoryid'];
		}
		if(empty($_POST['productid'])) {
		   $_POST['productid'] = 1;
		} else {
		   $_POST['productid'] = $_POST['productid'];
		}
		if(empty($_POST['lotnumberids'])) {
		   $_POST['lotnumberids'] = 1;
		} else {
		   $_POST['lotnumberids'] = $_POST['lotnumberids'];
		}
		$statusrow = $this->db->where('stocktakingid',$stocktakingid)->where_in('counterid',$_POST['storageid'])->where('categoryid',$_POST['categoryid'])->where('productid',$_POST['productid'])->where_in('lotid',$_POST['lotnumberids'])->where('status',$this->Basefunctions->activestatus)->get('stocktakingsession')->num_rows();
		$scannedrow = $this->db->where('stocktakingid',$stocktakingid)->where_in('counterid',$_POST['storageid'])->where('categoryid',$_POST['categoryid'])->where('productid',$_POST['productid'])->where_in('lotid',$_POST['lotnumberids'])->where('status',2)->get('stocktakingsession')->num_rows();
		if($scannedrow > 0 ) {
			$this->db->select('stocktakingsession.stocktakingsessionid',false);
			$this->db->from('stocktakingsession');
			$this->db->where('stocktakingsession.stocktakingid',$stocktakingid);
			$this->db->where_in('stocktakingsession.counterid',$_POST['storageid']);
			$this->db->where('stocktakingsession.categoryid',$_POST['categoryid']);
			$this->db->where('stocktakingsession.productid',$_POST['productid']);
			$this->db->where('stocktakingsession.status',2);
			$tagscannedarray=$this->db->get();
			if($tagscannedarray->num_rows() > 0) {
				foreach($tagscannedarray->result() as $data){
					$stocktakingsessionid = $data->stocktakingsessionid;
				}
			}
		}
		$inactiverow = $this->db->where('stocktakingid',$stocktakingid)->where_in('counterid',$_POST['storageid'])->where('categoryid',$_POST['categoryid'])->where('productid',$_POST['productid'])->where_in('lotid',$_POST['lotnumberids'])->where('status',0)->get('stocktakingsession')->num_rows();
		$result = array('active'=>$statusrow,'scanned'=>$scannedrow,'inactive'=>$inactiverow,'empname'=>$empname,'empcreatedate'=>$empcreatedate);
		$newscannedarray = array('connectrequest'=>'C');
		$masterdb =  $this->Basefunctions->inlineuserdatabaseactive($this->db->masterdb);
		$masterdb->where('rfiddevice.status','1');
		$masterdb->where('rfiddevice.macaddress',$_POST['deviceno']);
		$masterdb->update('rfiddevice',$newscannedarray);
		echo json_encode($result);
	}
	public function getscanneddata(){
		$stocktakingid = $_POST['stocktakingid'];
		if(empty($_POST['storageid'])) {
		   $_POST['storageid'] = 1;
		} else {
		   $_POST['storageid'] = $_POST['storageid'];
		}
		if(empty($_POST['categoryid'])) {
		   $_POST['categoryid'] = 1;
		} else {
		   $_POST['categoryid'] = $_POST['categoryid'];
		}
		if(empty($_POST['productid'])) {
		   $_POST['productid'] = 1;
		} else {
		   $_POST['productid'] = $_POST['productid'];
		}
		$weightround =  $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$itemtagid = '';
				$this->db->select('stocktakingdetail.barcodeid,stocktakingdetail.status,stocktakingdetail.stocktakingsessionid');
				$this->db->from('stocktakingdetail');
				$this->db->join('stocktakingsession','stocktakingsession.stocktakingsessionid = stocktakingdetail.stocktakingsessionid','left outer');
				$this->db->where('stocktakingdetail.stocktakingid',$stocktakingid);
				$this->db->where_in('stocktakingsession.counterid',$_POST['storageid']);
				$this->db->where('stocktakingsession.categoryid',$_POST['categoryid']);
				$this->db->where('stocktakingsession.productid',$_POST['productid']);
				$this->db->where_in('stocktakingdetail.status',array(3,2));
				$data = $this->db->get();
				if($data->num_rows() >0) {
				foreach($data->result() as $row) {
					$this->db->select('product.productname,itemtag.itemtagnumber,itemtag.grossweight,itemtag.netweight,itemtag.productid,itemtag.counterid,itemtag.rfidtagno,product.categoryid,purity.purityname,counter.countername,category.categoryname');
					$this->db->from('itemtag');
					$this->db->join('product','product.productid = itemtag.productid');
					$this->db->join('counter','counter.counterid = itemtag.counterid');
					$this->db->join('purity','purity.purityid = itemtag.purityid');
					$this->db->join('category','category.categoryid = product.categoryid');
					$this->db->where('itemtag.status',$this->Basefunctions->activestatus);
					if($_POST['rfidbarcode']  == 1){
						$this->db->where('itemtag.rfidtagno',$row->barcodeid);
					}else{
						$this->db->where('itemtag.itemtagnumber',$row->barcodeid);
					}
					$datastock = $this->db->get();
					if($datastock->num_rows()>0){
					foreach($datastock->result() as $info){
					
					if($_POST['rfidbarcode']  == 1){
						$barcodeid = $info->rfidtagno;
					}else{
						$barcodeid = $info->itemtagnumber;
					}
					$result[] =array('productname'=>$info->productname,'barcodeid'=>$barcodeid,'grosswt'=>number_format((float)$info->grossweight, $weightround, '.', ''),'netwt'=>number_format((float)$info->netweight, $weightround, '.', ''),'productid'=>$info->productid,'counterid'=>$info->counterid,'categoryid'=>$info->categoryid,'status'=>$row->status,'stocktakingsessionid'=>$row->stocktakingsessionid,'purityname'=>$info->purityname,'countername'=>$info->countername,'categoryname'=>$info->categoryname,'rfidtagno'=>$info->rfidtagno,'itemtagnumber'=>$info->itemtagnumber);	
				}
			}else{
				$result = '';
			}
				}
		}else{
			$result = '';
		}
		echo json_encode($result);
	}
	public function createstocksession() {
		$categoryid = $_POST['categoryid'];
		$modeid = $_POST['modeid'];
		$connectioncheck = $_POST['connectioncheck'];
		$startstoptrigger = $_POST['startstoptrigger'];
		$deviceno = $_POST['deviceno'];
		if(empty($_POST['lotid'])) {
			$_POST['lotid'] = 1;
		} else {
			$_POST['lotid'] = $_POST['lotid'];
		}
		$multiplecountername = '';
		$searchString = ',';
		 if( strpos($_POST['counterid'], $searchString) !== false ) {
			 $counterid = explode(',',$_POST['counterid']);
			$this->db->select('GROUP_CONCAT(DISTINCT(countername) SEPARATOR ",") as countername ' );
			$this->db->from('counter');
			$this->db->where_in('counterid',$counterid);
			$result = $this->db->get();
			if($result->num_rows() > 0) {
				foreach($result->result() as $info) {
					$countername = $info->countername;
				}
				$rfidfinalno  = explode(', ',$countername);
			   $resultval = array_filter($rfidfinalno);
			   $multiplecountername  = implode(',',$resultval);
			}
		 }
		$header = array(
			'date' => $this->Basefunctions->ymd_format($_POST['date']),
			'stocktakingid' => $_POST['stocktakingid'],
			'employeeid'=>$_POST['employeeid'],
			'counterid'=>$_POST['counterid'],
			'multiplecountername'=>$multiplecountername,
			'categoryid'=>$categoryid,
			'lotid'=>$_POST['lotid'],
			'productid'=>$_POST['productid'],
			'comment'=>$_POST['description'],
			'startdate'=>$this->Basefunctions->ymd_format($_POST['startdate']),
			'enddate'=>$this->Basefunctions->ymd_format($_POST['enddate']),
		);
		$header_insert = array_merge($header,$this->Crudmodel->defaultvalueget());
		$this->db->insert('stocktakingsession',array_filter($header_insert));
		$stocktakingid = $this->db->insert_id();
		if($modeid == 2 && $startstoptrigger == 'Yes') {
			$messagerfid = 'clear';
		} else if($modeid == 2 && $startstoptrigger == 'No') {
			$messagerfid = 'nothing';
		}
		if($modeid == 2){
			$masterdb =  $this->Basefunctions->inlineuserdatabaseactive($this->db->masterdb);
			$insert = array(
					'macaddress'=>$deviceno,
					'scanned'=>0,
					'missing'=>0,
					'expected'=>0,
					'conflict'=>0,
					'message'=>$messagerfid,
					'appmessage'=>'',
					'createdate'=>date($this->Basefunctions->datef),
					'lastupdatedate'=>date($this->Basefunctions->datef),
					'createuserid'=>$this->Basefunctions->userid,
					'lastupdateuserid'=>$this->Basefunctions->userid,
					'status'=>$this->Basefunctions->activestatus
			);
			$masterdb->insert('rfidsummary',array_filter($insert));
			$primaryid = $masterdb->insert_id();
		}
		echo $stocktakingid;
	}
	public function getbarcodedata(){
		$sessionid = $_POST['data'];
		$data = [];
		if(isset($_POST['barcodedata'])) {
			$barcodedata = explode(",",$_POST['barcodedata']);
        }else{
			$barcodedata = '';	
		}
		$masterdb = $this->Basefunctions->inlineuserdatabaseactive($this->db->masterdb); //master data base select
		$masterdb->select('GROUP_CONCAT(rfidstock.tagid) as tagid',false);
		$masterdb->from('rfidstock');
		$masterdb->where('sessionid',$sessionid);
		$masterdb->where_not_in('tagid',$barcodedata);
		$tagarray=$masterdb->get();
		if($tagarray->num_rows() > 0) {
			foreach($tagarray->result() as $data){
				$result ['barcodedata'] = $data->tagid;
			}
		}else{
              $result ['barcodedata'] = '';
		} 
		$masterdb->select('GROUP_CONCAT(rfidstock.tagid) as tagid',false);
		$masterdb->from('rfidstock');
		$masterdb->where('sessionid',$sessionid);
		$tagbarcodearray=$masterdb->get();
		if($tagbarcodearray->num_rows() > 0) {
			foreach($tagbarcodearray->result() as $info){
				$result ['barcodedataarray'] = $info->tagid;
				}
		}else{
              $result ['barcodedataarray'] = '';
		} 
		echo json_encode($result);		
	}
	//bill bumber grid data load
	public function Stocktakingdetailsgridheaderinformationfetchmodel($moduleid) {
		$fnames = array('id','Itemtagnumber','rfidtagno','grossweight','category','productname','purityname','countername','tagimage','stoneweight','netweight','lotnumber');
		$flabels = array('Id','Itemtag Number','RFID','G.Wt','Category','Product','Purity','Storage','Image','S.Wt','N.Wt','Lot Number');
		$colmodnames = array('id','itemtagnumber','rfidtagno','grossweight','categoryid','productid','purityid','countername','tagimage','stoneweight','netweight','lotnumber');
		$colindexnames = array('category','itemtag','itemtag','itemtag','category','product','purity','category','itemtag','itemtag','itemtag','lot');
		$uitypes = array('2','2','2','2','2','2','2','2','2','2','2','2');
		$viewtypes = array('1','1','1','1','1','1','1','1','1','1','1','1');
		$dduitypeids = array('17','18','20','19','23','25','26','27','28','29','30','31');
		$data = array();
		$i=0;
		$j=0;
		foreach($fnames as $fname) {
			if( in_array($uitypes[$j],$dduitypeids) ) {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j].'name';
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]='0';
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]='2';
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			} else {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			}
			$j++;
		}
		return $data;
	}
	//stock session data load
	public function Stocksessiondetailsgridheaderinformationfetchmodel($moduleid) {
		$fnames = array('id','sessionid','product','counter','category','expectedcount','scannedcount','missingcount','conflictcount','multiplecounter');
		$flabels = array('Id','sessionid','Product','Counter','Category','Expected Count','Scanned Count','Missing Count','Conflict Count','Multiple Counter');
		$colmodnames = array('id','sessionid','product','counter','category','expectedcount','scannedcount','missingcount','conflictcount','multiplecounter');
		$colindexnames = array('stocktakingsession','stocktakingsession','product','counter','category','stocktakingsession','stocktakingsession','stocktakingsession','stocktakingsession','counter');
		$uitypes = array('2','2','2','2','2','2','2','2','2','2');
		$viewtypes = array('1','0','1','1','1','1','1','1','1','1');
		$dduitypeids = array('17','18','20','19','23','25','26','27','28','29');
		$data = array();
		$i=0;
		$j=0;
		foreach($fnames as $fname) {
			if( in_array($uitypes[$j],$dduitypeids) ) {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j].'name';
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]='0';
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]='2';
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			} else {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			}
			$j++;
		}
		return $data;
	}
	//stock taking detail grid data load
	public function stocktakingdetailsdatafetchmodel() {
		$id = $_POST['ids'];
		$mode = $_POST['mode'];
		$rfidbarcode = $_POST['rfidbarcode'];
		$newconflictarray = array();
		$oldconflictarray = array();
		$conflictdiffarray = array();
	    $oldconflictarray = array_filter($id);
		for($k=0; $k <count($oldconflictarray);$k++) {
		  if(isset($oldconflictarray[$k])) {
			  $completeconflictdata[$k] = "'".$oldconflictarray[$k]."'";
		  }
	    }
		$stocksessionmodeid = $_POST['stocksessionmodeid'];
		$charegdetail = '';
		$j=1;
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$this->db->select('itemtag.itemtagid,product.productname,purity.purityname,counter.countername,itemtag.itemtagnumber,itemtag.rfidtagno,itemtag.grossweight,itemtag.stoneweight,itemtag.netweight,category.categoryname,lot.lotid,lot.lotnumber,itemtag.tagimage');
		$this->db->join('product','product.productid=itemtag.productid','left outer');
		$this->db->join('category','category.categoryid=product.categoryid','left outer');
		$this->db->join('purity','purity.purityid=itemtag.purityid','left outer');
		$this->db->join('counter','counter.counterid=itemtag.counterid','left outer');
		$this->db->join('lot','lot.lotid=itemtag.lotid','left outer');
		if($rfidbarcode == 0) {
			$this->db->where_in('itemtag.itemtagnumber',$id);
		} else {
			$this->db->where_in('itemtag.rfidtagno',$id);
		}
		if($stocksessionmodeid == 2) {
			$this->db->where('itemtag.status',$this->Basefunctions->activestatus);
		} else {
			$this->db->where('itemtag.status',12);
		}
		$this->db->from('itemtag');
		$data=$this->db->get();
		
		if($data->num_rows()>0){
			foreach($data->result() as $value){
				if($rfidbarcode == 0) {
					$finalid = $value->itemtagnumber;
				} else {
					$finalid = $value->rfidtagno;
				}
				if($value->lotnumber == '') {
					$value->lotnumber = '';
				} else {
					$value->lotnumber = $value->lotnumber;
				}
				array_push($newconflictarray,"'".$finalid."'");
				$charegdetail->rows[$j]['id']=$value->itemtagid;
				$charegdetail->rows[$j]['cell']=array(
						$j,
						$value->itemtagnumber,
						$value->rfidtagno,
						number_format((float)$value->grossweight, $round, '.', ''),
						$value->categoryname,
						$value->productname,
						$value->purityname,
						$value->countername,
						$value->tagimage,
						number_format((float)$value->stoneweight, $round, '.', ''),
						number_format((float)$value->netweight, $round, '.', ''),
						$value->lotnumber,
				);
				$j++;
			}
		}	
		$conflictdiffarray = array_diff($completeconflictdata,$newconflictarray);
		for($i=0; $i<count($conflictdiffarray); $i++) {
			$charegdetail->rows[$j]['id']=$i+1;
			if($rfidbarcode == 0) {
				$tagno = str_replace("'","",$conflictdiffarray[$i]);
				$rfidno = '';
			} else {
				$tagno = '';
				$rfidno =str_replace("'","",$conflictdiffarray[$i]);
			}
			$charegdetail->rows[$j]['cell']=array(
					$j,
					$tagno,
					$rfidno,
					0,
					'',
					'',
					'',
					'',
					'',
					0,
					0,
					'',
				);
			$j++;
		}
		echo  json_encode($charegdetail);
	}
	//Sum of stock taking details data fetch
	public function sumofstocktakingdetailsdatafetchmodel() {
		$id = $_POST['ids'];
		$mode = $_POST['mode'];
		$rfidbarcode = $_POST['rfidbarcode'];
		$stocksessionmodeid = $_POST['stocksessionmodeid'];
		$dataresult = '';
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$this->db->select('ROUND(sum(itemtag.grossweight),'.$round.') as sumgrossweight,ROUND(sum(itemtag.stoneweight),'.$round.') as sumstoneweight, ROUND(sum(itemtag.netweight),'.$round.') as sumnetweight,ROUND(sum(itemtag.pieces),0) as sumpieces');
		if($rfidbarcode == 0) {
			$this->db->where_in('itemtag.itemtagnumber',$id);
		} else {
			$this->db->where_in('itemtag.rfidtagno',$id);
		}
		if($stocksessionmodeid == 2) {
			$this->db->where('itemtag.status',$this->Basefunctions->activestatus);
		} else {
			$this->db->where('itemtag.status',12);
		}
		$this->db->from('itemtag');
		$data=$this->db->get();
		if($data->num_rows()>0){
			foreach($data->result() as $infosum) {
				$sumgrossweight = $infosum->sumgrossweight;
				$sumstoneweight = $infosum->sumstoneweight;
				$sumnetweight = $infosum->sumnetweight;
				$sumpieces = $infosum->sumpieces;
			}
		} else {
			$sumgrossweight = '0';
			$sumstoneweight = '0';
			$sumnetweight = '0';
			$sumpieces = '0';
		}
		$dataresult = array(
			'sumgrossweight'=>$sumgrossweight,
			'sumstoneweight'=>$sumstoneweight,
			'sumnetweight'=>$sumnetweight,
			'sumpieces'=>$sumpieces,
		);
		echo json_encode($dataresult);
	}
	//stock session detail grid data load
	public function stocksessiondetailsdatamodel() {
		$stocktakingid = $_POST['stocktakingid'];
		$charegdetail = '';
		$j=1;
		$this->db->select('stocktakingsession.stocktakingsessionid,product.productname,counter.countername,category.categoryname,stocktakingsession.expectedcount,stocktakingsession.scannedcount,stocktakingsession.missingcount,stocktakingsession.conflictcount,stocktakingsession.multiplecountername');
		$this->db->join('product','product.productid=stocktakingsession.productid','left outer');
		$this->db->join('category','category.categoryid=stocktakingsession.categoryid','left outer');
		$this->db->join('counter','counter.counterid=stocktakingsession.counterid','left outer');
		$this->db->where('stocktakingsession.stocktakingid',$stocktakingid);
		$this->db->where_in('stocktakingsession.status',array(1,2));
		$this->db->from('stocktakingsession');
		$data=$this->db->get();
		
		if($data->num_rows()>0){
			foreach($data->result() as $value){
				$charegdetail->rows[$j]['id']=$value->stocktakingsessionid;
				$charegdetail->rows[$j]['cell']=array(
						$j,
						$value->stocktakingsessionid,
						$value->productname,
						$value->countername,
						$value->categoryname,
						$value->expectedcount,
						$value->scannedcount,
						$value->missingcount,
						$value->conflictcount,
						$value->multiplecountername
					);
				$j++;
			}
		}else {
			$charegdetail = '';
		}			
		//echo 
		echo  json_encode($charegdetail);
	}
	public function getrfidvishal() {
		$deviceno = '';
		$maxrfid = '';
		$rfidtagnodata = array();
		$rfidtagnonewdata = array();
		if(isset($_POST['maxrfid'])) {
			$maxrfid = $_POST['maxrfid'];
		}
		if(isset($_POST['deviceno'])) {
			$deviceno = $_POST['deviceno'];
		}
		$rfcount = 0;
		$carray = array();
		$diffcarray = array();
		$maxrf = 0;
		$rfno = array();
		$result = array();
		$resultval = array();
		$rfidfinalno = array();
		$newrfidarray = array();
		$oldrfidarray = array();
		$diffrfidarray = array();
		if($deviceno != '') {
			$masterdb =  $this->Basefunctions->inlineuserdatabaseactive($this->db->masterdb);
			$masterdb->select('count(rfidstockid) as rfidstockcount' );
			$masterdb->from('rfidstock');
			$masterdb->where('devicename',$deviceno);
			$resultcount = $masterdb->get();
			if($resultcount->num_rows() > 0) {
				foreach($resultcount->result() as $infocount) {
					$rfcount = $infocount->rfidstockcount;
				}
			} else {
				$rfcount = 0;
			}
			//$masterdb->select('max(rfidstockid) as rfidstockid,GROUP_CONCAT(DISTINCT(tagserialno) SEPARATOR ",") as rfidtagno ' );
			$masterdb->select('max(rfidstockid) as rfidstockid,GROUP_CONCAT((CASE WHEN rfidref.tagserialno is null  THEN  rfidstock.tagserialno  ELSE  rfidref.tagno END)  SEPARATOR ",") as rfidtagno ' );
			$masterdb->from('rfidstock');
			$masterdb->join('rfidref','rfidref.tagserialno = rfidstock.tagserialno','left outer');
			$masterdb->where('devicename',$deviceno);
			$masterdb->where('rfidstockid >',$maxrfid);
			$result = $masterdb->get();
			if($result->num_rows() > 0) {
				foreach($result->result() as $info) {
					$maxrf = $info->rfidstockid;
					$rfno = $info->rfidtagno;
				}
				$rfidfinalno  = explode(', ',$rfno);
				$resultval = array_filter($rfidfinalno);
				$vishal  = implode("','",$resultval);
				$vishal = "'".$vishal."'";
				$oldrfidarray = explode(',',$vishal);
				for($k=0; $k <count($oldrfidarray); $k++) {
					//$rfidtagnodata[$k] = "'".$oldrfidarray[$k]."'";
					$rfidtagnodata[$k] = $oldrfidarray[$k];
				}
			} else {
				$rfno  = '';
				$maxrf = '';
			}
		    if($rfno != '') {
				$data=$this->db->query("SELECT GROUP_CONCAT(DISTINCT(itemtagnumber) SEPARATOR ',') as itemtagnumber,GROUP_CONCAT(DISTINCT(rfidtagno) SEPARATOR ',') as rfidtagno 
					FROM `itemtag` where  itemtagid in (SELECT max(itemtagid) as maxr from itemtag where rfidtagno in (".$vishal.") group by rfidtagno) order by itemtagid ");
					if($data->num_rows() > 0) {
						foreach($data->result() as $infoo) {
							$carray['orginal'] = array(
								'tagno'=>$infoo->rfidtagno,
								'maxrf'=>$maxrf,
								'rfcount'=>$rfcount
							);
							$newrfidarray = explode(',',$infoo->rfidtagno);
						}
						for($l=0; $l <count($newrfidarray); $l++) {
							$rfidtagnonewdata[$l] = "'".$newrfidarray[$l]."'";
						}
						$diffrfidarray = array_values(array_diff($rfidtagnodata,$rfidtagnonewdata));
						for($i=0; $i < count($diffrfidarray); $i++) {
							$carray['diffarray'][$i] = array( 
								'tagno'=>str_replace("'","",$diffrfidarray[$i]),
								'maxrf'=>$maxrf,
								'rfcount'=>$rfcount
							);
						}
					} else {
						$carray['orginal'] = array(
							'tagno'=>'',
							'maxrf'=>$maxrfid,
							'rfcount'=>$rfcount
						);
					}
		    } else {
			    $carray['orginal'] = array(
					'tagno'=>'',
					'maxrf'=>$maxrfid,
					'rfcount'=>$rfcount
				);
		    }
		} else {
		    $carray['orginal'] = array(
				'tagno'=>'',
				'maxrf'=>$maxrfid,
				'rfcount'=>$rfcount
			);
		}
		echo json_encode($carray);
	}
	public function clearexistdetails() {
		$deviceno = $_POST['deviceno'];
		$masterdb =  $this->Basefunctions->inlineuserdatabaseactive($this->db->masterdb);
		$masterdb->where('devicename',$deviceno);
		$masterdb->delete('rfidstock'); 
	}
	//check product storage
	public function checkproductstorage(){
		$storagename = $_POST['storagename'];
		$this->db->select('counter.counterid,counter.countername');
		$this->db->from('counter');
		$this->db->where('status',1);
		$this->db->where('storagetypeid',2);
		$this->db->where('counterdefault',0);
		$this->db->where('counterdefaultid',0);
		if(is_numeric($storagename)) {
			$this->db->where('counterid',$storagename);
		} else {
			$this->db->where('countername',$storagename);
		}
		$storage = $this->db->get();
		$j=0;
		$storagedetail='';
		if($storage->num_rows() > 0) {
			foreach($storage->result() as $value){
				$storagedetail->statusid[$j]['id']=1;
				$storagedetail->rows[$j]['id']=$value->counterid;
				$storagedetail->rows[$j]['cell']=array(
						$value->counterid,
						$value->countername
						
				);
			}
		}else {
			$storagedetail->statusid[$j]['id']=0;
		}
		echo json_encode($storagedetail);
	}
	// get storage data based on rfid
	public function getrfidstorage() {
		$deviceno = '';
		$maxrfid = '';
		if(isset($_POST['maxrfid'])) {
			$maxrfid = $_POST['maxrfid'];
		}
		if(isset($_POST['deviceno'])) {
			$deviceno = $_POST['deviceno'];
		}
		$rfcount = 0;
		$carray = array();
		$maxrf = 0;
		$rfno = array();
		$result = array();
		$resultval = array();
		$rfidfinalno = array();
		$j=0;
		$storagedetail='';
		if($deviceno != '') {
			$masterdb =  $this->Basefunctions->inlineuserdatabaseactive($this->db->masterdb);
			$masterdb->select('count(rfidstockid) as rfidstockcount' );
			$masterdb->from('rfidstock');
			$masterdb->where('devicename',$deviceno);
			$resultcount = $masterdb->get();
			if($resultcount->num_rows() > 0) {
				foreach($resultcount->result() as $infocount) {
					$rfcount = $infocount->rfidstockcount;
				}
			} else {
				$rfcount = 0;
			}
			$masterdb->select('max(rfidstockid) as rfidstockid,GROUP_CONCAT((CASE WHEN rfidref.tagserialno is null  THEN  rfidstock.tagserialno  ELSE  rfidref.tagno END)  SEPARATOR ",") as rfidtagno ' );
			$masterdb->from('rfidstock');
			$masterdb->join('rfidref','rfidref.tagserialno = rfidstock.tagserialno','left outer');
			$masterdb->where('devicename',$deviceno);
			$masterdb->where('rfidstockid >',$maxrfid);
			$result = $masterdb->get();
			if($result->num_rows() > 0) {
				foreach($result->result() as $info) {
					$maxrf = $info->rfidstockid;
					$rfno = $info->rfidtagno;
			}
				$rfno = preg_replace("/[^0-9,.]/", "", $rfno);
			   $rfidfinalno  = explode(',',$rfno);
			   $resultval = array_filter($rfidfinalno);
			   $vishal  = implode(',',$resultval);
			} else {
				 $rfno  = '';
				 $maxrf = '';
			}
		    if($rfno != '') {	
				$storage=$this->db->query("SELECT counterid,countername FROM `counter` where rfidtagno in (".$vishal.") and status = 1 and storagetypeid = 2 and counterdefault = 0 and counterdefaultid = 0");
					if($storage->num_rows() > 0) {
						foreach($storage->result() as $value){
							$storagedetail->maxrfid[$j]['id']=$maxrf;
							$storagedetail->rows[$j]['id']=$value->counterid;
							$storagedetail->rows[$j]['cell']=array(
									$value->counterid,
									$value->countername
									
							);
							$j++;
						}
					} else {
						$storagedetail->maxrfid[$j]['id']=$maxrfid;
					}
		    } else {
			    $storagedetail->maxrfid[$j]['id']=$maxrfid;
		    }
		} else {
		    $storagedetail->maxrfid[$j]['id']=$maxrfid;
		}
		echo json_encode($storagedetail);
	}
	/* storage data grid load */
	public function storagedataheaderinformationfetchmodel($moduleid) {
		$fnames = array('counterid','countername');
		$flabels = array('Storage Id','Storage Name');
		$colmodnames = array('counterid','countername');
		$colindexnames = array('counter','counter');
		$uitypes = array('2','2');
		$viewtypes = array('1','1');
		$dduitypeids = array('17','18','20','19','23','25','26','27','28','29');
		$data = array();
		$i=0;
		$j=0;
		foreach($fnames as $fname) {
			if( in_array($uitypes[$j],$dduitypeids) ) {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j].'name';
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]='0';
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]='2';
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			} else {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			}
			$j++;
		}
		return $data;
	}
	// reset session status
	public function resetsession(){
		$currentsessionid = $_POST['currentsessionid'];
		$delete = $this->Basefunctions->delete_log();
		if(empty($_POST['viewscanstatus'])) {
			// inactive current session id
			$this->db->where('stocktakingsessionid',$currentsessionid);
			$this->db->update('stocktakingsession',$delete);
		}else {
			$scannedarray = array('status' => 2);  // scanned status
			// reset current session id
			$this->db->where('stocktakingsessionid',$currentsessionid);
			$this->db->update('stocktakingsession',$scannedarray);
		}
		$masterdb =  $this->Basefunctions->inlineuserdatabaseactive($this->db->masterdb);
		$deviceno = $_POST['deviceno'];
		$newscannedarray = array('status'=>0);  
		$masterdb->where('rfidsummary.status','1');
		$masterdb->where('rfidsummary.macaddress',$deviceno);
		$masterdb->update('rfidsummary',$newscannedarray);
		echo 'SUCCESS';
	}
	// Stock taking number retrieve
	public function retrievestocktakingnumber() {
		$datarowid = $_GET['datarowid'];
		$stocktakingnumber =  $this->Basefunctions->singlefieldfetch('stocktakingnumber','stocktakingid','stocktaking',$datarowid);
		echo json_encode($stocktakingnumber);
	}
	//retrieve lot number DD
	public function lotnumberdropdown() {
		return $this->db->query("SELECT lot.lotid,lot.lotnumber FROM `lot` where lot.lotused = 1 and status in(1,11)");
	}
	// RFID Device Detailed information
	public function viewdynamicdatainfofetch($tablename,$sortcol,$sortord,$pagenum,$rowscount) {
		$masterdb =  $this->Basefunctions->inlineuserdatabaseactive($this->db->masterdb);
		$dataset ='rfiddevice.rfiddeviceid,rfiddevice.macaddress,rfiddevice.bluetoothname,rfiddevice.devicename,rfiddevice.connectioncheck,rfiddevice.startstoptrigger';
		$status=$tablename.'.status='.$this->Basefunctions->activestatus.'';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		/* query */
		$resultcount = $masterdb->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' WHERE '. $status.' ORDER BY '.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$data=array();
		$i=0;
		if($resultcount->num_rows() > 0) {
			foreach($resultcount->result() as $row) {
				$rfiddeviceid = $row->rfiddeviceid;
				$macaddress =$row->macaddress;
				$bluetoothname =$row->bluetoothname;
				$devicename =$row->devicename;
				$connectioncheck =$row->connectioncheck;
				$startstoptrigger =$row->startstoptrigger;
				$data[$i]=array('id'=>$rfiddeviceid,$macaddress,$bluetoothname,$devicename,$connectioncheck,$startstoptrigger);
				$i++;
			}
		}
		$finalresult = array('0'=>$data,'1'=>$page,'2'=>'json');
		return $finalresult;
	}
	// Add RFID Device Details
	public function addrfiddevicedetails() {
		$masterdb =  $this->Basefunctions->inlineuserdatabaseactive($this->db->masterdb);
		$insert=array(
				'macaddress'=>$_POST['macaddress'],
				'bluetoothname'=>$_POST['bluetoothname'],
				'devicename'=>$_POST['devicename'],
				'connectioncheck'=>$_POST['connectioncheck'],
				'startstoptrigger'=>$_POST['startstoptrigger'],
				'status'=>$this->Basefunctions->activestatus
		);
		$masterdb->insert('rfiddevice',array_filter($insert));
		$primaryid = $masterdb->insert_id();
		echo 'SUCCESS';
	}
	// Edit and set RFID Device Information in form fields
	public function rfiddevicedetailsretrive() {
		$id = $_GET['primaryid'];
		$masterdb =  $this->Basefunctions->inlineuserdatabaseactive($this->db->masterdb);
		$masterdb->select('rfiddeviceid,macaddress,bluetoothname,devicename,connectioncheck,startstoptrigger');
		$masterdb->from('rfiddevice');
		$masterdb->where('rfiddeviceid',$id);
		$resultcount = $masterdb->get();
		if($resultcount->num_rows() > 0) {
			foreach($resultcount->result() as $infocount) {
				$jsonarray=array(
					'rfiddeviceid'=>$infocount->rfiddeviceid,
					'macaddress'=>$infocount->macaddress,
					'bluetoothname'=>$infocount->bluetoothname,
					'devicename'=>$infocount->devicename,
					'connectioncheck'=>$infocount->connectioncheck,
					'startstoptrigger'=>$infocount->startstoptrigger
				);
			}
		}
		echo json_encode($jsonarray);
	}
	// RFID Device Information update
	public function rfiddevicedetailsupdate() {
		$masterdb =  $this->Basefunctions->inlineuserdatabaseactive($this->db->masterdb);
		$id=$_POST['primaryid'];
		$update=array(
				'macaddress'=>$_POST['macaddress'],
				'bluetoothname'=>$_POST['bluetoothname'],
				'devicename'=>$_POST['devicename'],
				'connectioncheck'=>$_POST['connectioncheck'],
				'startstoptrigger'=>$_POST['startstoptrigger']
		);
		$masterdb->where('rfiddeviceid',$id);
		$masterdb->update('rfiddevice',$update);
		echo 'SUCCESS';
	}
	// RFID Device Information Delete
	public function rfiddeviceinformationdelete() {
		$masterdb =  $this->Basefunctions->inlineuserdatabaseactive($this->db->masterdb);
		$id = $_GET['primaryid'];
		$delete = array(
				'status'=>0
		);
		$masterdb->where('rfiddeviceid',$id);
		$masterdb->update('rfiddevice',$delete);
		echo 'SUCCESS';
	}
	// Load Device Number in drop down (Stock session form)
	public function rfiddeviceinformation() {
		$masterdb =  $this->Basefunctions->inlineuserdatabaseactive($this->db->masterdb);
		$masterdb->select('rfiddeviceid,macaddress,bluetoothname,devicename,connectioncheck,startstoptrigger,connectstatus,connectdatetime');
		$masterdb->from('rfiddevice');
		$masterdb->where('rfiddevice.status','1');
		$resultcount = $masterdb->get();
		return $resultcount;
	}
	// Get RFID Device table data information
	public function getrfiddevicetable() {
		$masterdb =  $this->Basefunctions->inlineuserdatabaseactive($this->db->masterdb);
		$deviceno = $_POST['deviceno'];
		
		// Retrieve App Message from RFID summary table - Masterdb
		$masterdb->select('message');
		$masterdb->from('rfidsummary');
		$masterdb->where('rfidsummary.status','1');
		$masterdb->where('rfidsummary.macaddress',$deviceno);
		$resultcount = $masterdb->get();
		if($resultcount->num_rows() > 0) {
			foreach($resultcount->result() as $info) {
				$message = $info->message;
			}
		} else {
			$message = '';
		}
		
		// Retrieve the data from rfiddevice table - Reduced 20 seconds from the current date with time
		$time = date("Y-m-d H:i:s", time() - 35);
		$masterdb->select('rfiddeviceid,connectstatus');
		$masterdb->from('rfiddevice');
		$masterdb->where('rfiddevice.macaddress',$deviceno);
		$masterdb->where('rfiddevice.connectdatetime >',$time);
		$resultcount = $masterdb->get();
		if($resultcount->num_rows() > 0) {
			foreach($resultcount->result() as $info) {
				$connectstatus = $info->connectstatus;
			}
		} else {
			$connectstatus = '';
		}
		$result = array('message'=>$message,'connectstatus'=>$connectstatus);
		echo json_encode($result);
	}
	// Cross Check if the device is connected
	public function checkduplicatedeviceisconnected() {
		$masterdb =  $this->Basefunctions->inlineuserdatabaseactive($this->db->masterdb);
		$deviceno = $_POST['deviceno'];
		$connectstatus = 'no';
		// Retrieve App Message from RFID summary table - Masterdb
		$masterdb->select('rfidsummaryid');
		$masterdb->from('rfidsummary');
		$masterdb->where('rfidsummary.status','1');
		$masterdb->where('rfidsummary.macaddress',$deviceno);
		$resultcount = $masterdb->get();
		if($resultcount->num_rows() > 0) {
			foreach($resultcount->result() as $info) {
				$connectstatus = '1';
			}
		} 
		$result = array('connectstatus'=>$connectstatus);
		echo json_encode($result);
	}
	// Update RFID Summary information
	public function updaterfidsummary() {
		$masterdb =  $this->Basefunctions->inlineuserdatabaseactive($this->db->masterdb);
		$deviceno = $_POST['deviceno'];
		$message = $_POST['message'];
		if($message == 'delete'){
			$newscannedarray = array('status'=>0);  
		}else{
			$devicename = substr($deviceno, 0, 7);
			if($devicename == 'RFD8500')
			{
				if($message == 'start'){
					$message = 'START';
				}else if($message == 'stop'){
					$message = 'STP';
				}else if($message == 'clear'){
					$message = 'PR';
				}
			}
			$newscannedarray = array('message'=>$message,'appmessage'=>$message);
		}
		$masterdb->where('rfidsummary.status','1');
		$masterdb->where('rfidsummary.macaddress',$deviceno);
		$masterdb->update('rfidsummary',$newscannedarray);
		$result = array('resultmessage'=>'1');
		echo json_encode($result);
	}
	// Export Overlay Details
	public function generateexcelexport() {
		$this->load->library('excel');
		$stocktakingid = $this->input->post('stocktakingid');
		$stocktakingvalue = $this->input->post('stocktakingvalue');
		$pdfgeneratename = $this->input->post('pdfgeneratename');
		$stockitemids = $this->input->post('stockitemids');
		$fname = strtoupper($pdfgeneratename);  //Titlte name
		{ //Header date & time display
			$today = date("d-m-Y");
			$timestamp = strtotime($today);
			$newDate = date('d-M-Y', $timestamp);
			$time = date("H:i:sa");
		}
		$this->excel->setActiveSheetIndex(0);
		$sheet = $this->excel->getActiveSheet();
		$sheet->setTitle($pdfgeneratename.'-'.$stocktakingvalue);
		// Report Name with Stock Taking Number
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(13);
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->setCellValue('A1',$pdfgeneratename.'-'.$stocktakingvalue);
		$this->excel->getActiveSheet()->mergeCells('A1:E1');
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(13);
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->setCellValue('F1',$newDate.' - '.$time);
		$this->excel->getActiveSheet()->mergeCells('F1:I1');
		{ // Body contents
			$bodycontentsheaderlabel = array('S. No','Barcode No','RFID Tag No','Category','Product Name','Storage Name','Purity','Gross Wt','Stone Wt','Net Wt');
			$cell = 'A';
			$rowvalue = 2;
			for($i=0;$i<count($bodycontentsheaderlabel);$i++) {
				$this->excel->getActiveSheet()->getStyle($cell.$rowvalue)->getFont()->setSize(12);
				$this->excel->getActiveSheet()->getStyle($cell.$rowvalue)->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue($cell.$rowvalue,$bodycontentsheaderlabel[$i]);
				$this->excel->getActiveSheet()->getColumnDimension($cell)->setAutoSize(100);
				$cell++;
			}
			$cell = 'A';
			$rowvalue = 3;
			if($pdfgeneratename == 'Conflict') {
				$girddata = $this->input->post('criteriagriddata');
				$girddatainfo = json_decode($girddata,true);
				$criteriacnt = $this->input->post('criteriacnt');
				$gridfieldarray = array('id','itemtagnumber','rfidtagno','category','productname','countername','purityname','grossweight','stoneweight','netweight');
				for($i=0;$i<=($criteriacnt-1);$i++) {
					foreach($gridfieldarray as $griddetails) {
						$this->excel->getActiveSheet()->getStyle($cell.$rowvalue)->getFont()->setSize(10);
						$this->excel->getActiveSheet()->getStyle($cell.$rowvalue)->getFont()->setBold(false);
						$this->excel->getActiveSheet()->setCellValue($cell.$rowvalue,$girddatainfo[$i][$griddetails]);
						$this->excel->getActiveSheet()->getColumnDimension($cell)->setAutoSize(100);
						$cell++;
					}
					$cell = 'A';
					$rowvalue++;
				}
			} else {
				$arrayvalue = array('','itemtagnumber','rfidtagno','categoryname','productname','countername','purityname','grossweight','stoneweight','netweight');
				$resultdata = $this->eachitemdetails($stockitemids);
				foreach($resultdata as $key) {
					for($i=0;$i<count($arrayvalue);$i++) {
						$this->excel->getActiveSheet()->getStyle($cell.$rowvalue)->getFont()->setSize(10);
						$this->excel->getActiveSheet()->getStyle($cell.$rowvalue)->getFont()->setBold(false);
						if($i == 0) {
							$this->excel->getActiveSheet()->setCellValue($cell.$rowvalue,$rowvalue-2);
						} else {
							$this->excel->getActiveSheet()->setCellValue($cell.$rowvalue,$key[$arrayvalue[$i]]);
						}
						$this->excel->getActiveSheet()->getColumnDimension($cell)->setAutoSize(100);
						$cell++;
					}
					$cell = 'A';
					$rowvalue++;
				}
			}
		}
		//Save details to export excel
		//ob_clean();
		$filename = $fname.'_'.$time.'.xlsx';
		header("Content-type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8;");
		header("Content-type:application/octetstream");
		header("Content-Disposition:attachment;filename=".$filename."");
		header("Content-Transfer-Encoding:binary");
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
		$objWriter->save('php://output');
	}
	// All IDS in Overlay
	public function eachitemdetails($stockitemids) {
		$output = array();
		$weightround =  $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		if($stockitemids != '') {
			$stockitemids = "'" . implode("', '", explode(",", $stockitemids)) ."'";
			$data = $this->db->query("SELECT itemtag.itemtagid, itemtag.itemtagnumber, itemtag.rfidtagno, category.categoryname, product.productname, counter.countername, purity.purityname, itemtag.grossweight, itemtag.stoneweight, itemtag.netweight FROM itemtag LEFT OUTER JOIN product ON product.productid = itemtag.productid LEFT OUTER JOIN category ON category.categoryid = product.categoryid LEFT OUTER JOIN counter ON counter.counterid = itemtag.counterid LEFT OUTER JOIN purity ON purity.purityid = itemtag.purityid WHERE itemtag.itemtagid IN ($stockitemids) order by itemtag.itemtagid DESC");
			if($data->num_rows() > 0) {
				foreach($data->result() as $info) {
					$output[] = array('itemtagnumber'=>$info->itemtagnumber,'rfidtagno'=>$info->rfidtagno,'categoryname'=>$info->categoryname,'productname'=>$info->productname,'countername'=>$info->countername,'purityname'=>$info->purityname,'grossweight'=>ROUND($info->grossweight,$weightround),'stoneweight'=>ROUND($info->stoneweight,$weightround),'netweight'=>ROUND($info->netweight,$weightround));
				}
			} else {
				$output[] = array('fail'=>'FAILED');
			}
		} else {
			$output[] = array('fail'=>'FAILED');
		}
		return $output;
	}
	// Close button reset Summary table
	public function closebuttonresetsession() {
		$masterdb =  $this->Basefunctions->inlineuserdatabaseactive($this->db->masterdb);
		$deviceno = $_POST['deviceno'];
		$newscannedarray = array('status'=>0);  
		$masterdb->where('rfidsummary.status','1');
		$masterdb->where('rfidsummary.macaddress',$deviceno);
		$masterdb->update('rfidsummary',$newscannedarray);
		echo 'SUCCESS';
	}
	// Close button reset Summary table
	public function alldeviceclearoverlay() {
		$masterdb =  $this->Basefunctions->inlineuserdatabaseactive($this->db->masterdb);
		$deviceno = $_POST['macaddress'];
		$newscannedarray = array('status'=>0);  
		$masterdb->where('rfidsummary.status','1');
		$masterdb->where('rfidsummary.macaddress',$deviceno);
		$masterdb->update('rfidsummary',$newscannedarray);
		echo 'SUCCESS';
	}
}
?>