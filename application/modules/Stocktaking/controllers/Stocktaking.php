<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Stocktaking extends CI_Controller {
	function __construct() {
    	parent::__construct();
    	$this->load->helper('formbuild');
		$this->load->model('Stocktaking/Stocktakingmodel');
		$this->load->model('Itemtag/Itemtagmodel');
		$this->load->model('Base/Basefunctions');
		$this->load->model('Sales/Salesmodel');
    }
	public function index() {
		$moduleid = array(103);
		sessionchecker($moduleid);
		//action		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		//view
		$viewfieldsmoduleids =$moduleid;
		$viewmoduleid = $moduleid;	
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd'] = $this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol'] = $this->Basefunctions->vardataviewdropdowncolumns($viewfieldsmoduleids);
		$data['filetype'] = $this->Basefunctions->simpledropdownwithcond('filetypeid','filetypename,setdefault','filetype','fieldtypeid','3');
		$data['branch'] = $this->Basefunctions->simpledropdown('branch','branchid,branchname','branchid');
		$data['user'] = $this->Basefunctions->simpledropdown('employee','employeeid,employeename,branchid','employeeid');
		$data['purity']=$this->Basefunctions->purity_groupdropdown();
		$data['stocktakingtype'] = $this->Basefunctions->simpledropdown('stocktakingtype','stocktakingtypeid,stocktakingtypename','stocktakingtypeid');
		$data['stocksessionmode'] = $this->Basefunctions->simpledropdown('stocksessionmode','stocksessionmodeid,stocksessionmodename','stocksessionmodeid');
		$data['lotnumber'] = $this->Stocktakingmodel->lotnumberdropdown();
		$data['product'] = $this->Itemtagmodel->productrfid_dd();
		$data['deviceno'] = $this->Basefunctions->get_company_settings('deviceno');
		$data['rfiddevice'] = $this->Stocktakingmodel->rfiddeviceinformation('rfiddevice');
		$data['employeeid'] = $this->Basefunctions->userid;
		$data['productidshow'] = $this->Basefunctions->get_company_settings('productidshow');
		$data['weight_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$data['counter'] = $this->Basefunctions->counter_groupdropdown_notdefault();
		$data['printtemplate']= $this->Salesmodel->simpledropdown('printtemplates','printtemplatesid,printtemplatesname,printtypeid,salestransactiontypeid,setasdefault','moduleid','103'); //Retrieve all the print templates
		$this->load->view('Stocktaking/stocktakingview',$data);
	}
	public function expecteddetails(){
		$this->Stocktakingmodel->expecteddetailsmodel();
	}
	public function serialnumbergenerate(){
		$serialnumber = $this->Basefunctions->varrandomnumbergenerator(103,'stocktakingnumber',1,''); echo $serialnumber;
	}
	public function conflictdata(){
		$this->Stocktakingmodel->conflictdata();
	}
	public function stocktakingcreate(){
		$this->Stocktakingmodel->stocktakingcreate();
	}
	public function stocktakingdelete(){
		$this->Stocktakingmodel->stocktakingdelete();
	}
	public function stocktakingsessioncreate(){
		$this->Stocktakingmodel->stocktakingsessioncreate();
	}
	public function loadstockcode(){
		$this->Stocktakingmodel->loadstockcode();
	}
	public function checksessiontable(){
		$this->Stocktakingmodel->checksessiontable();
	}
	public function getscanneddata(){
		$this->Stocktakingmodel->getscanneddata();
	}
	public function createstocksession(){
		$this->Stocktakingmodel->createstocksession();
	}
	public function getbarcodedata(){
		$this->Stocktakingmodel->getbarcodedata();
	}
	 //stock taking details data fetch
    public function Stocktakingdetailsgridheaderinformationfetch() {
    	$width = $_GET['width'];
    	$height = $_GET['height'];
    	$modname = $_GET['modulename'];
    	$moduleid = $_GET['moduleid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
    	$checkbox = 'false';
		$primarytable = $_GET['primarytable'];
		$primaryid = $_GET['primaryid'];
		$filterid = isset($_GET['filter']) ? $_GET['filter'] : '';
		$conditionname = isset($_GET['conditionname']) ? $_GET['conditionname'] : '';
		$filtervalue = isset($_GET['filtervalue']) ? $_GET['filtervalue'] : '';
		$filter = array('0'=>$filterid,'1'=>$conditionname,'2'=>$filtervalue);
		$footer = isset($_GET['footername']) ? $_GET['footername'] : 'empty';
		$sortcol = isset($_GET['sortcol']) ? $_GET['sortcol'] : '';
		$sortord = isset($_GET['sortord']) ? $_GET['sortord'] : '';
    	$colinfo = $this->Stocktakingmodel->Stocktakingdetailsgridheaderinformationfetchmodel($moduleid);
    	$device = $this->Basefunctions->deviceinfo();
    	if($device=='phone') {
    		$datas = mobileviewformtogriddatagenerate($colinfo,$modname,$width,$height);
    	} else {
    		$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height,$checkbox);
			//$datas = viewgriddatagenerate($colinfo,$primarytable,$primaryid,$width,$height,$footer);
    	}
    	echo json_encode($datas);
    }
	//stock session details data fetch
    public function Stocksessiondetailsgridheaderinformationfetch() {
    	$width = $_GET['width'];
    	$height = $_GET['height'];
    	$modname = $_GET['modulename'];
    	$moduleid = $_GET['moduleid'];
    	$checkbox = "true";
    	$colinfo = $this->Stocktakingmodel->Stocksessiondetailsgridheaderinformationfetchmodel($moduleid);
    	$device = $this->Basefunctions->deviceinfo();
		$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height,$checkbox);
    	/* if($device=='phone') {
    		$datas = mobileviewformtogriddatagenerate($colinfo,$modname,$width,$height);
    	} else {
    		$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height,$checkbox);
    	} */
		echo json_encode($datas);
    }
	public function stocktakingdetailsdatafetch(){
		$this->Stocktakingmodel->stocktakingdetailsdatafetchmodel();
	}
	public function sumofstocktakingdetailsdatafetch(){
		$this->Stocktakingmodel->sumofstocktakingdetailsdatafetchmodel();
	}
	public function getrfidvishal(){
		$this->Stocktakingmodel->getrfidvishal();
	}
	public function clearexistdetails(){
		$this->Stocktakingmodel->clearexistdetails();
	}
	//check product storage
	public function checkproductstorage(){
		$this->Stocktakingmodel->checkproductstorage();
	}
	/* storage data grid */
	public function storagedataheaderinformationfetch() {
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modname = $_GET['modulename'];
		$moduleid = $_GET['moduleid'];
		$colinfo = $this->Stocktakingmodel->storagedataheaderinformationfetchmodel($moduleid);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobileviewformtogriddatagenerate($colinfo,$modname,$width,$height);
		} else {
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height);
		}
		echo json_encode($datas);
	}
	// get storage data for rfid
	public function getrfidstorage(){
		$this->Stocktakingmodel->getrfidstorage();
	}
	// load stocksession data based on stocktaking id
	public function loadsessiondata(){
		$this->Stocktakingmodel->loadsessiondata();
	}
	// reset session status
	public function resetsession(){
		$this->Stocktakingmodel->resetsession();
	}
	// get stock session data
	public function stocksessiondetailsdata(){
		$this->Stocktakingmodel->stocksessiondetailsdatamodel();
	}
	// StockTaking Tagtemplate print icon action*/
	public function stktagtemplatereprint() {
		$storageid = trim($_GET['primaryid']);	
		$templateid = trim($_GET['templateid']);
		if($templateid > 1) {
			$print_data=array('templateid'=> $templateid, 'Templatetype'=>'Tagtemp','primaryset'=>'counter.counterid','primaryid'=>$storageid);
			$data = $this->Printtemplatesmodel->generatlabelprint($print_data);
			echo $data;
		} else {
			echo 'Kindly Edit the Print template of the tag in edit Screen.Since u have selected any print option earlier.';
			exit();
		}
	}
	// Stock taking number retrieve
	public function retrievestocktakingnumber() {
		$this->Stocktakingmodel->retrievestocktakingnumber();
	}
	/* Fetch Main grid header,data,footer information - RFID Device Grid */
	public function rfiddevicegridinformationfetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'rfiddevice.rfiddeviceid') : 'rfiddevice.rfiddeviceid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('macaddress','bluetoothname','devicename','connectioncheck','startstoptrigger'),'colmodelindex'=>array('rfiddevice.macaddress','rfiddevice.bluetoothname','rfiddevice.devicename','rfiddevice.connectioncheck','rfiddevice.startstoptrigger'),'coltablename'=>array('macaddress','bluetoothname','devicename','connectioncheck','startstoptrigger'),'uitype'=>array('2','2','2','2','2'),'colname'=>array('MAC Address','Bluetooth Name','Device Name','Connection Check','Start - Stop Trigger'),'colsize'=>array('150','150','150','100','100'));
		$result=$this->Stocktakingmodel->viewdynamicdatainfofetch($primarytable,$sortcol,$sortord,$pagenum,$rowscount);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'RFID Device',$width,$height);
		} else {
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'RFID Device',$width,$height);
		}
		echo json_encode($datas);
	}
	// Add RFID Device Details
	public function addrfiddevicedetails() {
		$this->Stocktakingmodel->addrfiddevicedetails();
	}
	// Edit and set RFID Device Information in form fields
	public function rfiddevicedetailsretrive() {
		$this->Stocktakingmodel->rfiddevicedetailsretrive();
	}
	// RFID Device Information update
	public function rfiddevicedetailsupdate() {
		$this->Stocktakingmodel->rfiddevicedetailsupdate();
	}
	// RFID Device Information Delete
	public function rfiddeviceinformationdelete() {
		$this->Stocktakingmodel->rfiddeviceinformationdelete();
	}
	// Get RFID Device table data information
	public function getrfiddevicetable() {
		$this->Stocktakingmodel->getrfiddevicetable();
	}
	// check duplicate device is connected
	public function checkduplicatedeviceisconnected() {
		$this->Stocktakingmodel->checkduplicatedeviceisconnected();
	}
	// check duplicate device is connected
	public function updaterfidsummary() {
		$this->Stocktakingmodel->updaterfidsummary();
	}
	// Export Overlay Details
	public function generateexcelexport() {
		$this->Stocktakingmodel->generateexcelexport();
	}
	// Close button clear summary table
	public function closebuttonresetsession() {
		$this->Stocktakingmodel->closebuttonresetsession();
	}
	// Device status change
	public function alldeviceclearoverlay() {
		$this->Stocktakingmodel->alldeviceclearoverlay();
	}
}