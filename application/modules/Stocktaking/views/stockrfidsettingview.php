<style type="text/css">
	#rfiddevicegrid .gridcontent{
		height: 63vh !important;
	}
	#rfiddeviceoverlayclose {
		left:-20px !important;
		position:relative;
		font-size: 0.9rem;
		text-transform: uppercase;
		display: inline-block;
		font-family: 'Nunito Semibold', sans-serif;
		background-color: #E57276;
		color: #fff;
		padding-left: 10px;
		padding-right: 10px;
		height: 30px;
		box-sizing: border-box;
		position: relative;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
		cursor: pointer;
		outline: 0;
		border: none;
		-webkit-tap-highlight-color: transparent;
		display: inline-block;
		white-space: nowrap;
		text-decoration: none;
		vertical-align: baseline;
		text-align: center;
		margin: 0;
		min-width: 64px;
		line-height: 19px;
		padding: 0 16px;
		border-radius: 4px;
		overflow: visible;
		transform: translate3d(0,0,0);
		transition: background .4s cubic-bezier(.25,.8,.25,1),box-shadow 280ms cubic-bezier(.4,0,.2,1);
	}
	#rfiddevicegridfooter .rppdropdown {
	left:62% !important;	
	}
</style>

<?php
	$device = $this->Basefunctions->deviceinfo();
	$dataset['gridtitle'] = 'RFID Device Details';
	$dataset['titleicon'] = 'material-icons library_books';
	$modtabgroup[0] = array("tabpgrpid"=>"1","tabgrpname"=>"RFID Device Details","moduleid"=>"103","templateid"=>"2");
	$dataset['modtabgrp'] = $modtabgroup;
	$dataset['moduleid'] = '103';
	$dataset['otype'] = 'overlay';
	$dataset['action'][0] = array('actionid'=>'rfiddeviceoverlayclose','actiontitle'=>'Close','actionmore'=>'No','ulname'=>'rfiddevice');
	$this->load->view('Base/formwithgridmenuheader.php',$dataset);
?>
<div class="" id="rfiddeviceoverlay">
	<div class="large-12 columns addformunderheader">&nbsp;</div>
	<div class="large-12 columns scrollbarclass addformcontainer">
		<div class="large-12 medium-12 small-12 large-centered medium-centered small-centered columns paddingzero">
			<div class="large-12 column paddingzero">
				<?php if($device=='phone') {?>
					<div class="closed effectbox-overlay effectbox-default" id="rfiddevicesectionoverlay">
				<?php }?> 
				<div class="row mblhidedisplay">&nbsp;</div>
				<form id="rfiddeviceform" name="rfiddeviceform" class="cleardataform">
				<div id="addrfiddeviceformvalidate" class="validationEngineContainer">
				<div id="editrfiddeviceformvalidate" class="validationEngineContainer">
					<div class="large-4 columns end paddingbtm">
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff;top:-4px;">
							<?php if($device=='phone') {?>
							<div class="large-12 columns sectionheaderformcaptionstyle">RFID Device Details</div>
							<div class="large-12 columns sectionpanel">
								<?php  } else { ?>
									<div class="large-12 columns headerformcaptionstyle">RFID Device Details </div>
							<?php  	} ?> 
							<div class="input-field large-12 columns">
								<input type="text" id="macaddress" name="macaddress" data-validation-engine="validate[required]" class="" >
								<label for="macaddress">MAC Address</label>
							</div>
							<div class="input-field large-6 columns">
								<input type="text" id="bluetoothname" name="bluetoothname" data-validation-engine="validate[required]" value="" class="" >
								<label for="bluetoothname">Bluetooth Name</label>
							</div>
							<div class="input-field large-6 columns">
								<input type="text" id="devicename" name="devicename" data-validation-engine="validate[required]" value="" class="" >
								<label for="devicename">Device Name</label>
							</div>
							<div class="static-field large-6 columns">
								<label>Connection Check<span class="mandatoryfildclass">*</span></label>
								<select id="connectioncheck" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" name="connectioncheck" data-placeholder="Yes / No" tabindex="107">
									<option value="No">No</option>
									<option value="Yes">Yes</option>
								</select>
							</div>
							<div class="static-field large-6 columns">
								<label>Start / Stop Trigger<span class="mandatoryfildclass">*</span></label>
								<select id="startstoptrigger" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" name="startstoptrigger" data-placeholder="Yes / No" tabindex="107">
									<option value="No">No</option>
									<option value="Yes">Yes</option>
								</select>
							</div>
							<?php if($device=='phone') { ?>
							</div>
							<div class="divider large-12 columns"></div>
							<div class="large-12 columns submitkeyboard sectionalertbuttonarea" style="text-align: right">
								<input id="addrfiddevicebtn" class="alertbtn addkeyboard" type="button" value="Submit" name="addrfiddevicebtn" tabindex="103">
								<input id="editrfiddevicebtn" class="alertbtn hidedisplay updatekeyboard" type="button" value="Submit" name="editrfiddevicebtn" tabindex="103" style="display: none;">
								<input id="rfiddeviceclosebtn"class="alertbtn addsectionclose cancelkeyboard" type="button" value="Cancel" name="cancel" tabindex="104">
								<input type="hidden" name="rfiddeviceid" id="rfiddeviceid">
							</div>
								
							<?php } else { ?> 
							<div class="large-12 columns" style="text-align: right">
								<label> </label>
								<input id="addrfiddevicebtn" class="btn" type="button" value="Submit" name="">
								<input id="editrfiddevicebtn" class="btn hidedisplay" type="button" value="Submit" name="">
								<input type="hidden" name="rfiddeviceid" id="rfiddeviceid">
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
				</div>
				</form>
				<?php if($device=='phone') {?>
				</div>
				<?php }?> 
				<div class="large-8 columns paddingbtm">
					<div class="large-12 columns paddingzero">
						<div class="large-12 columns viewgridcolorstyle paddingzero borderstyle" style="top: 4px;">
							<div class="large-12 columns headerformcaptionstyle" style="height:auto;padding: 0.2rem 0 0rem;">
								<span class="large-6 medium-6 small-6 columns text-left small-only-text-center" >RFID Device List</span>
								<span class="large-6 medium-6 small-6 columns innergridicon righttext small-only-text-center">
										<?php if($device=='phone') {?>
										  <span id="rfiddeviceaddicon" class="icon-box" title="Add" ><i class="material-icons">add</i></span>
										  <?php }?> 
										 <span id="rfiddeviceediticon" class="icon-box" title="Edit" ><i class="material-icons">edit</i></span>
								<span id="rfiddevicedeleteicon" class="icon-box" title="Delete"><i class="material-icons">delete</i> </span>
							</div>
					<?php
							$device = $this->Basefunctions->deviceinfo();
							if($device=='phone') {
								echo '<div class="large-12 columns paddingzero forgetinggridname" id="rfiddevicegridwidth"><div class=" inner-row-content inner-gridcontent" id="rfiddevicegrid" style="height:68vh !important;top:0px;">
									<!-- Table header content , data rows & pagination for mobile-->
								</div>
								<footer class="inner-gridfooter footercontainer" id="rfiddevicegridfooter">
									<!-- Footer & Pagination content -->
								</footer></div>';
							} else {
								echo '<div class="large-12 columns forgetinggridname" id="rfiddevicegridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="rfiddevicegrid" style="max-width:2000px; height:68vh !important;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
								<div class="inner-gridfooter footer-content footercontainer" id="rfiddevicegridfooter">
									<!-- Footer & Pagination content -->
								</div></div>';
							}
				?>
				    	</div>
					</div>
				</div>
			</div>
		</div>
	</div>		
</div>
	
	