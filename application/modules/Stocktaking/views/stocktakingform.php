<style type="text/css">
.desktop row-content inner-gridcontent .grid-view {
    height: 450px !important;
    position: relative;
}
</style>

<?php
	$device = $this->Basefunctions->deviceinfo();
	$dataset['gridtitle'] = $gridtitle;
	$dataset['titleicon'] = $titleicon;
	$this->load->view('Base/mainformwithgridmenuheader.php',$dataset);
	$CI =& get_instance();
	$appdateformat = $CI->Basefunctions->appdateformatfetch(); 
	$defaultrecordview = $this->Basefunctions->get_company_settings('mainviewdefaultview');
	echo hidden('mainviewdefaultview',$defaultrecordview);
?>
<input type="hidden" id="brmode" name="brmode" value="1">
<input type="hidden" id="conflictdatahidden" name="conflictdatahidden" value="1">
<input type="hidden" id="hiddenemployeeid" name="hiddenemployeeid" value="<?php echo $employeeid; ?>">
<input type="hidden" id="weight_round" name="weight_round" value="<?php echo $weight_round; ?>">
<input type="hidden" id="appmessage" name="appmessage" value="">
<input type="hidden" id="connectstatus" name="connectstatus" value="">
<input type="hidden" id="stocktakeformstatus" name="stocktakeformstatus" value="">
<div class="large-12 columns addformunderheader">&nbsp;</div>
<div class="large-12 columns scrollbarclass salesmdaddformcontainer stocktakingtouchcontainer">
	<div class="row mblhidedisplay">&nbsp;</div>
	<div id="subformspan1" class="hiddensubform">
		<form method="POST" name="stocktakingheaderform" action ="" id="stocktakingheaderform">
			<span id="adddataimportfiletabvalidate" class="validationEngineContainer" >
				<span id="editdataimportfiletabvalidate" class="validationEngineContainer">
					<div class="large-12 columns  paddingbtm" id="stocktakingheaderform" style="padding-left:0px !important;">
						<div class="large-12 columns cleardataform borderstyle toggle-headercontent" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5;bottom: 10px;">
							<div class="large-12 columns" style="">&nbsp;</div>
							<div class="large-12 columns paddingzero">
							<div class="static-field large-1 columns hidedisplay">
							<label>Date<span class="mandatoryfildclass">*</span></label>								
							<input id="date" class="fordatepicicon" data-validation-engine="validate[required]" type="text" data-dateformater="dd-mm-yy" data-prompt-position="topLeft" readonly="readonly" tabindex="101" name="date">
							</div>
							<div class="static-field large-2 columns">
								<label>Stocktaking Code<span class="mandatoryfildclass">*</span></label>
								<select id="stocktakingcode" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" name="stocktakingcode" data-placeholder="Search" tabindex="106">
									<option value=""></option>
								</select>
							</div>
							<div class="static-field large-1 columns hidedisplay">
								<label>Audit Person</label>
								<select class="chzn-select" data-validation-engine="validate[required]" id="employeeid" name="employeeid" data-placeholder="Search" tabindex="103">
									<?php foreach($user as $key):?>
										<option value="<?php echo $key->employeeid;?>" data-branchid="<?php echo $key->branchid;?>">
										<?php echo $key->employeename;?></option>
									<?php endforeach;?>
								</select>
							</div>
							<div class="static-field large-2 columns hidedisplay" id="stocktakingpurityidhdivid">
							<label>Purity<span class="mandatoryfildclass">*</span></label>						
							<select id="stocktakingpurityid" name="stocktakingpurityid" class="chzn-select dropdownchange" data-validation-engine="validate[required]" multiple data-prompt-position="topLeft:14,36" tabindex="103">
							<option value=""></option> 
							<?php $prev = ' ';
							foreach($purity->result() as $key):
							$cur = $key->metalid;
							$a="";
							if($prev != $cur )
							{
								if($prev != " ")
								{
									 echo '</optgroup>';
								}
								echo '<optgroup  label="'.$key->metalname.'">';
								$prev = $key->metalid;
							}
							?>
							<option value="<?php echo $key->purityid;?>"><?php echo $key->purityname;?></option>
							<?php endforeach;?>
							</select>
						</div>
							<div class="static-field large-2 columns storagemodeiddiv hidedisplay">
								<label>Storage Mode</label>
								<select id="storagemodeid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" name="storagemodeid" data-placeholder="Search Mode" tabindex="107" readonly="true">
									<option value="1"></option>
									<option value="2">RFID Storage</option>
									<option value="3">Single Storage</option>
									<option value="4">Multiple Storage</option>
									<option value="5">Parent Storage</option>
								</select>
							</div>
							<div class="static-field large-3 columns storagetreediv hidedisplay">
								<label id="storagelabelid">Storage</label>
								<?php 
								$mandlab = "";
								$tablename = 'counter' ;
								$mandatoryopt = ""; //mandatory option set
								echo divopen( array("id"=>"dl-promenustorage","class"=>"dl-menuwrapper","style"=>"z-index:2;margin-bottom: 1rem;margin-top:0.5rem;") );
								$type="button";
								echo '<button name="treebutton" type="button" id="prodstoragelistbutton" class="btn dl-trigger treemenubtnclick" tabindex="" style="height:2.2rem;color:#ffffff;font-size: 1.45rem;padding: 0.1rem;padding-top:0.3rem;background-color:#546e7a;width:25%"><i class="material-icons">format_indent_increase</i></button>';
								$txtoptions = array("style"=>"width:80%;display:inline;position:absolute;height:2.12rem;padding-left: 5px;","readonly"=>"readonly","tabindex"=>"","class"=>"");
								echo text('prodparentstoragename','',$txtoptions);
								echo '<ul class="dl-menu maintreegenerate large-12 medium-6 small-12 column" id="prodstoragelistuldata">';
								$dataset = $this->Basefunctions->jewel_treecreatemodel($tablename);
								$this->Basefunctions->stocktakecreateTree($dataset,0);
								echo close('ul');
								echo close('div');
								?>							
							<input type="hidden" id="proparentstorageid" name="proparentstorageid" value="">	
							</div>
							<div class="static-field large-3 columns counteridhide hidedisplay">
								<label>Storage</label>
								<select id="counterid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" name="counterid" data-placeholder="Search storage" tabindex="108">
									<option value=""></option>
									<?php foreach($counter->result() as $key):?>
										<option  value="<?php echo $key->counterid;?>">
										<?php echo $key->countername;?></option>
									<?php endforeach;?>
								</select>
							</div>
							<div class="static-field large-3 columns categorytreediv hidedisplay">
								<label>Category</label>
								<?php 
								$mandlab = "";
								$tablename = 'category' ;
								$mandatoryopt = ""; //mandatory option set
								echo divopen( array("id"=>"dl-promenucategory","class"=>"dl-menuwrapper","style"=>"z-index:2;margin-bottom: 1rem;margin-top:0.5rem;") );
								$type="button";
								echo '<button name="treebutton" type="button" id="prodcategorylistbutton" class="btn dl-trigger treemenubtnclick" tabindex="" style="height:2.2rem;color:#ffffff;font-size: 1.45rem;padding: 0.1rem;padding-top:0.3rem;background-color:#546e7a;width:25%"><i class="material-icons">format_indent_increase</i></button>';
								$txtoptions = array("style"=>"width:80%;display:inline;position:absolute;height:2.12rem;padding-left: 5px;","readonly"=>"readonly","tabindex"=>"","class"=>"");
								echo text('prodparentcategoryname','',$txtoptions);
								echo '<ul class="dl-menu maintreegenerate large-12 medium-6 small-12 column" id="prodcategorylistuldata">';
								$dataset = $this->Basefunctions->jewel_treecreatemodel($tablename);
								$this->Basefunctions->stocktakecreateTree($dataset,0);
								echo close('ul');
								echo close('div');
								?>							
							<input type="hidden" id="proparentcategoryid" name="proparentcategoryid" value="">	
							</div>
							
							<div class="static-field large-3 columns categoryidhide hidedisplay">
								<label>Category<span class="mandatoryfildclass">*</span></label>
								<select id="categoryid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" name="categoryid" data-placeholder="" multiple tabindex="108">
								</select>
							</div>
							<div class="static-field large-3 columns productiddiv hidedisplay">
								<label>Product<span class="mandatoryfildclass">*</span></label>
								<select id="productid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" name="productid" data-placeholder="Search Product" tabindex="108">
									<option value=""></option>
									<?php foreach($product->result() as $key):?>
										<option data-categoryid="<?php echo $key->categoryid;?>" data-counterid="<?php echo $key->counterid;?>" value="<?php echo $key->productid;?>">
										<?php if($productidshow == 'YES') { echo $key->productid.' - '.$key->productname; } else { echo $key->productname;} ?></option>
									<?php endforeach;?>
								</select>
							</div>
							<div class="static-field large-2 columns counteproductidhide hidedisplay end">
								<label>Type Value<span class="mandatoryfildclass">*</span></label>
								<select id="counterproductid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" name="counterproductid" data-placeholder="Search module" tabindex="108" multiple>
									<option value="">Select</option>
									<?php foreach($product->result() as $key):?>
										<option data-counterid="<?php echo $key->counterid;?>" value="<?php echo $key->productid;?>">
										<?php echo $key->productname;?></option>
									<?php endforeach;?>
								</select>
							</div>
							<div class="static-field large-3 columns lotnumberdiv hidedisplay">
								<label>Lot Number<span class="mandatoryfildclass">*</span></label>
								<select id="lotnumberid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" name="lotnumberid" multiple tabindex="108">
									<option value=""></option>
									<?php foreach($lotnumber->result() as $key):?>
										<option data-lotid="<?php echo $key->lotid;?>" value="<?php echo $key->lotid;?>" data-lotnumber="<?php echo $key->lotnumber;?>">
										<?php echo $key->lotnumber;?></option>
									<?php endforeach;?>
								</select>
							</div>
							<div class="static-field large-1 columns viewcondclear reportdatepickerdiv hidedisplay" id="startdatehdivid">
								<input type="text" class="fordatepicicon" data-dateformater="<?php echo $appdateformat; ?>" id="startdate" name="startdate">
								<label for="startdate">Start Date</label>
							</div>
							<div class="static-field large-1 columns viewcondclear reportdatepickerdiv hidedisplay" id="enddatehdivid">
								<input type="text" class="fordatepicicon" data-dateformater="<?php echo $appdateformat; ?>" id="enddate" name="enddate">
								<label for="enddate">End Date</label>
							</div>
							<div class="input-field large-2 columns hidedisplay">
									<input type="text" class="" data-validation-engine="" id="description" name="description" value="" tabindex="105" readonly>
									<label for="description">Comment</label>	
							</div>
							<div class="input-field large-1 columns" id="itemtagnumber-div">
									<input type="text" class="" id="itemtagnumber" name="itemtagnumber" value="" tabindex="106">
									<label for="itemtagnumber">Id</label>	
							 </div>
							 <div class="input-field large-1 columns hidedisplay" id="deviceno-div">
									<input type="text" class="" id="deviceno" name="deviceno" value="" tabindex="106">
									<label for="deviceno">Device No</label>	
							 </div>
							 <div class="input-field large-1 columns hidedisplay" id="rfidcountdb-div">
									<input type="text" class="" id="rfidcountdb" name="rfidcountdb" value="" readonly="readonly" tabindex="107">
									<label for="rfidcountdb">App Count</label>	
							 </div>
							 <div class="input-field large-2 small-3 columns loadprocessdiv">
									<input type="button" class="btn leftformsbtn" id="loadprocess" name="loadprocess" value="Load" tabindex="107">
							 </div>
							 <div class="input-field large-2 columns loadstoragediv hidedisplay">
									<input type="button" class="btn leftformsbtn" id="loadstorage" name="loadstorage" value="Storage" tabindex="107">
							 </div>
							 <div class="input-field large-2 small-3  columns" id="startprocess-div">
									<input type="button" class="btn leftformsbtn" id="startprocess" name="startprocess" value="Start" tabindex="107">
							 </div>
							  <div class="input-field large-2 columns hidedisplay end" id="stopprocess-div">
									<input type="button" class="btn leftformsbtn" id="stopprocess" name="stopprocess" value="Stop" tabindex="108">
							 </div>
							 <div class="input-field large-2 columns hidedisplay" id="clearprocess-div">
									<input type="button" class="btn leftformsbtn" id="clearprocess" name="clearprocess" value="Clear Session" tabindex="108">
							 </div>
							</div>
						</div>
					</div>
					<input type="hidden" id="primaryid" name="primaryid">
			</form>
					<div class="large-3 columns  paddingbtm hidedisplay" style="padding-left:0px !important;top: 20px;">
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
							<div class="large-12 columns headerformcaptionstyle">Expected</div>
							<div class="large-12 columns" style="height:350px;overflow-y:auto;overflow-x:hidden;" id="expecteddata">
							</div>
							<div class="large-12 columns">&nbsp;</div>
						</div>
					</div>
					<div class="large-3 columns  paddingbtm" style="padding-left:0px !important;top: 20px;padding-bottom:1.5rem !important;">
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
							<div class="large-12 columns headerformcaptionstyle">Scanned Nos</div>
							<div class="large-12 columns" style="height:350px;overflow-y:auto;overflow-x:hidden;" id="scannednos">
							</div>
							<div class="large-12 columns">&nbsp;</div>
						</div>
					</div>
					<div class="large-3 columns  paddingbtm" style="top:20px !important;padding-left: 0 !important;padding-bottom:1.5rem !important;">
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important;  padding-right: 0 !important;background: #f5f5f5">
							<div class="large-12 columns headerformcaptionstyle">Summary</div>
							 <div class="large-12 columns" style="height:350px;overflow-y:auto;overflow-x:hidden;" id="summarydata">
							 <div class="product-list detailedlistoverlay" id="stock-expected-count-overlay"><span class="pdt-name"> <span>Expected : </span><span class="stock-count stockgriddetail"  id="stock-expected-count">0 		 </span></span><span class="pdt-name"> <span>G.Wt : </span><span  id="stock-expected-grosswt">0</span><span> | </span><span>N.Wt : </span><span  id="stock-expected-netwt"> 0</span></span>
							 </div>
							 <div class="product-list detailedlistoverlay" id="stock-scanned-count-overlay"><span class="pdt-name"> <span>Scanned : </span><span  class="stock-count stockgriddetail" id="stock-scanned-count">0 		 </span></span><span class="pdt-name"> <span>G.Wt : </span><span  id="stock-scanned-grosswt">0</span><span> | </span><span>N.Wt : </span><span  id="stock-scanned-netwt"> 0</span></span>
							 </div>
							 <div class="product-list detailedlistoverlay" id="stock-missing-count-overlay"><span class="pdt-name"> <span>Missing : </span><span  class="stock-count stockgriddetail" id="stock-missing-count">0 		 </span></span><span class="pdt-name"> <span>G.Wt : </span><span  id="stock-missing-grosswt">0</span><span> | </span><span>N.Wt : </span><span  id="stock-missing-netwt"> 0</span></span>
							 </div>
							 <div class="product-list detailedlistoverlay" id="stock-conflict-count-overlay"><span class="pdt-name"> <span>Conflict : </span><span  class="stock-count stockgriddetail" id="stock-conflict-count">0 		 </span></span><!--<span class="pdt-name"> <span>G.Wt : </span><span  id="stock-conflict-grosswt">0</span><span> | </span><span>N.Wt : </span><span  id="stock-conflict-netwt"> 0</span></span>-->
							 </div>
							</div>
							<div class="large-12 columns">&nbsp;</div>
						</div>
					</div>
					<div class="large-3 columns  paddingbtm hidedisplay" style="padding-bottom:1rem !important;">
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
							<div class="large-12 columns headerformcaptionstyle">Scanned</div>
							 <div class="large-12 columns" style="height:350px;overflow-y:auto;overflow-x:hidden;" id="scanneddata">
							</div>
							<div class="large-12 columns">&nbsp;</div>
						</div>
					</div>
					<div class="large-6 columns  paddingbtm" style="top:20px;padding-left: 0px !important;padding-bottom:1rem !important;">
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
							<div class="large-12 columns headerformcaptionstyle">Missing</div>
							<div class="large-12 columns" style="height:350px;overflow-y:auto;overflow-x:hidden;" id="misseddata">
							</div>
							<div class="large-12 columns">&nbsp;</div>
						</div>
					</div>
					<div class="large-3 columns  paddingbtm hidedisplay">
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
							<div class="large-12 columns headerformcaptionstyle">Conflict</div>
							<div class="large-12 columns" style="height:350px;overflow-y:auto;overflow-x:hidden;" id="conflictdata">
							</div>
							<div class="large-12 columns">&nbsp;</div>
						</div>
					</div>
					<!--<div class="large-12 columns  paddingbtm" style="padding-left:0px !important;">
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
							<div class="large-12 columns" style="">&nbsp;</div>
							<!--<div class="large-3 columns end">
								<div class="stock-header">Total</div>
								<div class="stock-count" id="stock-expected-count"><span>0</span></div>
							</div>-->
							<!--<div class="large-3 columns end">
								<div class="stock-header">Scanned</div>
								<div class="stock-count stockgriddetail" id="stock-scanned-count"><span>0</span></div>
							</div>-->
							<!--<div class="large-3 columns end">
								<div class="stock-header">Missing</div>
								<div class="stock-count stockgriddetail" id="stock-missing-count"><span>0</span></div>
							</div>
							<div class="large-3 columns end">
								<div class="stock-header">Conflict</div>
								<div class="stock-count stockgriddetail" id="stock-conflict-count"><span>0</span></div>
							</div>
						</div>
					</div>-->
				</span>
			</span>
			<input type="hidden" id="stocktakingsessioncreationid" name="stocktakingsessioncreationid">
			<input type="hidden" id="prevstocktakingsessionid" name="prevstocktakingsessionid">
			<input type="hidden" id="barcodearray" name="barcodearray">
	</div>
</div>
<!----Stock Taking Detail overlay----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="stockentrydetailoverlay" style="overflow-y: auto;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="large-9 medium-9 small-9 large-centered medium-centered small-centered columns borderstyle" >
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 columns sectionheaderformcaptionstyle">
							Stock Taking Details
						</div>
					<?php
							$device = $this->Basefunctions->deviceinfo();
							if($device=='phone') {
								echo '<div class="large-12 columns paddingzero forgetinggridname" id="stocktakingdetailsgridwidth"><div id="stocktakingdetailsgrid" class="inner-row-content inner-gridcontent" style="height:450px;">
									<!-- Table header content , data rows & pagination for mobile-->
								</div>
								<footer class="inner-gridfooter footercontainer" id="stocktakingdetailsgridfooter">
									 Footer & Pagination content 
								</footer></div>';
							} else {
								echo '<div class="large-12 columns forgetinggridname" id="stocktakingdetailsgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="stocktakingdetailsgrid" style="max-width:2000px; height:450px;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
								<div class="inner-gridfooter footer-content footercontainer" id="stocktakingdetailsgridfooter">
									 Footer & Pagination content 
								</div></div>';
							}
						?></div>
					<div class="row" style="background:#ffffff">
						<div class="large-3 columns sumgrossweight" style="text-align: left;font-weight: bold;"></div>
						<div class="large-3 columns sumstoneweight" style="text-align: left;font-weight: bold;"></div>
						<div class="large-3 columns sumnetweight" style="text-align: left;font-weight: bold;"></div>
						<div class="large-3 columns sumpieces" style="text-align: left;font-weight: bold;"></div>
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
							<input type="button" id="stocktakingexportexcel" name="stocktakingexportexcel" value="Excel" class="alertbtn">
							<input type="button" id="stocktakingdetailgridclose" name="stocktakingdetailgridclose" value="Cancel" class="alertbtn">
						</div>
					</div>
			</div>
	</div>
</div>
<!----Storage name data overlay----->
	<div id="storagedataoverlay" class="closed effectbox-overlay effectbox-default">
		<div class="row sectiontoppadding">&nbsp;</div>
  		<div class="large-12 columns mblnopadding">
  		<form method="POST" name="storagedataform" class="storagedataform"  id="storagedataform">
			<div id="storagedatavalidate" class="validationEngineContainer" >			
				<div class="large-6 columns large-offset-3 paddingbtm">	
					<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
					<div class="large-12 columns sectionheaderformcaptionstyle">Storage Data</div>
					  <div class="large-12 columns sectionpanel">
						<div class="input-field large-4 columns storagenamediv">
								<input type="text" class="" id="storagename" name="storagename"  tabindex="106">
								<label for="storagename">Storage Name</label>	
						</div>
						<div class="input-field large-2 small-3 columns storageappenddiv">
								<input type="button" id="storageappend" name="storageappend" value="Add" class="alertbtnyes">	
						</div>
						<div class="input-field large-2 small-3 columns">
								<input type="button" id="storagedatadeleteicon" name="storagedatadeleteicon" value="Delete" class="alertbtnno">	
						</div>
						<div class="input-field large-2 small-3 columns storagestartdiv">
								<input type="button" id="storagestart" name="storagestart" value="Start" class="alertbtnyes">	
						</div>
						<div class="input-field large-2 columns storagestopdiv hidedisplay">
								<input type="button" id="storagestop" name="storagestop" value="Stop" class="alertbtnno">	
						</div>
						<div class="input-field large-3 columns">
								<input type="text" id="storagecount" name="storagecount" value="" readonly>
								<label for="storagecount">No.of Storage</label>	
						</div>
						 	<?php
							echo '<div class="large-12 columns forgetinggridname" id="storagedatagridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="storagedatagrid" style="max-width:2000px; height:300px;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
							<!--<div class="inner-gridfooter footer-content footercontainer" id="storagedatagridfooter">
									 Footer & Pagination content 
								</div>--></div>';
						?>	
						<div class="row">&nbsp;</div>
						</div>
						<div class="divider large-12 columns"></div>
						<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">					
							<input type="hidden" id="storagedatahidden" name="storagedatahidden">
							<input type="button" class="alertbtnyes addkeyboard" id="storagedatacreate" name="storagedatacreate" value="Submit" tabindex="">
							<input type="button" class="alertbtnno  ffield" id="storagedataclose" name="storagedataclose" value="Close" tabindex="">
						</div>
						</div>
				</div>
			</div>
		</form>
		</div>
	   </div>

	   <!-- conflict overlay yes/no -->
	   <div class="large-12 columns" style="position:absolute;">
		<div class="overlay alertsoverlay overlayalerts" id="conflictalert">	
			<div class="row">&nbsp;</div>
			<div class="row sectiontoppadding"></div>
			<div class="overlaybackground">
				<div class="row">&nbsp;</div>
				<div class="alert-panel">
					<div class="alertmessagearea">
					<div class="alert-message">Do you  want data push to conflict data?</div>
					</div>
					<div class="alert-panel">
					<div class="alertbuttonarea">
						<span class="firsttab" tabindex="1000"></span>
						<input type="button" id="conflictcloseyes" name="" tabindex="1001" value="Yes" class="alertbtnyes  ffield" >
						<input type="button" id="conflictcloseno" name="" value="No" tabindex="1002" class="flloop alertbtnno alertsoverlaybtn" >
						<span class="lasttab" tabindex="1003"></span>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
	
	<!-- clear session yes/no -->
	   <div class="large-12 columns" style="position:absolute;">
		<div class="overlay alertsoverlay overlayalerts" id="clearsessionalert">	
			<div class="row">&nbsp;</div>
			<div class="row sectiontoppadding"></div>
			<div class="overlaybackground">
				<div class="row">&nbsp;</div>
				<div class="alert-panel">
					<div class="alertmessagearea">
					<div class="alert-message">Do you want to clear this combination of data?</div>
					</div>
					<div>
					<div class="alertbuttonarea">
						<span class="firsttab" tabindex="1000"></span>
						<input type="button" id="clearsessioncloseyes" name="" tabindex="1001" value="Yes" class="alertbtnyes  ffield" >
						<input type="button" id="clearsessioncloseno" name="" value="No" tabindex="1002" class="flloop alertbtnno alertsoverlaybtn" >
						<span class="lasttab" tabindex="1003"></span>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>