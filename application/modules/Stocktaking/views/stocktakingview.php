<!DOCTYPE HTML>
<html lang="en">
<head>
<!-- Base Header File -->
<?php
	$this->load->view('Base/headerfiles');
	$device = $this->Basefunctions->deviceinfo();
?>
<style>
		@media screen and (min-width: 64em){
		.toggle-headercontent .large-2{width:12.5%;}
		}
		#expecteddata .product-list span{color:#546e7a;}
		#stock-expected-count span{color:#ffffff;}
		#scanneddata .product-list{border:1px solid #4caf50;}
		#scanneddata .product-list span{color:#4caf50;}
		#stock-scanned-count span{color:#4caf50;}
		#misseddata .product-list{border:1px solid #f44336;}
		#misseddata .product-list span{color:#f44336;}
		#stock-missing-count span{color:#f44336;}
		#conflictdata .product-list{border:1px solid #ff9800;}
		#conflictdata .product-list span{color:#ff9800;}
		#stock-conflict-count span{color:#ff9800;}
		div .conflictdummaydata{color:#f100ff;}
		div .conflictdbdata{color:#FFA500;}
</style>
</head>
<body >
		<?php
		$dataset['gridenable'] = "yes"; //no
		$dataset['griddisplayid'] = "stocktakingview";
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['spanattr'] = array();
		$dataset['gridtableid'] = "stocktakingviewgrid";
		$dataset['griddivid'] = "stocktakingviewgriddiv";
		$dataset['moduleid'] = $moduleids;
		$dataset['forminfo'] = array(array('id'=>'stocktakingform','class'=>'hidedisplay','formname'=>'stocktakingform'),array('id'=>'productaddonformdiv','class'=>'hidedisplay','formname'=>'stocksessionform'),array('id'=>'stockrfidsettingview','class'=>'hidedisplay','formname'=>'stockrfidsettingview'));
		if($device=='phone') {
			$this->load->view('Base/gridmenuheadermobile',$dataset);
			$this->load->view('Base/overlaymobile');
			$this->load->view('Base/viewselectionoverlay');
		} else {
			$this->load->view('Base/gridmenuheader',$dataset);
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/basedeleteform');
		$this->load->view('Base/modulelist');
	?>
	<!---- Stock Taking session delete overlay----->
	<div class="large-12 small-12 medium-12 columns" style="position:absolute;">
		<div class="overlay" id="stocktakingdeleteoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
			<div class="row sectiontoppadding">&nbsp;</div>
			<div class="large-4 medium-4 small-12 large-centered medium-centered small-centered columns">
				<div class="large-12 column paddingzero">
					<div class="row">&nbsp;</div>
					<form id="stocktakingdeleteform" name="stocktakingdeleteform" class="cleardataform">
						<div id="stocktakingdeletevalidate" class="validationEngineContainer">
							<div class="large-12 columns end paddingbtm nontagtransfer">
								<div class="large-12 columns cleardataform z-depth-5 borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff">
									<div class="large-12 columns sectionheaderformcaptionstyle">Delete Details</div>
									<div class="large-12 columns sectionpanel" style="padding: 15px 15px;">
										<div class="static-field large-12 columns">
											<label>Delete Mode</label>
											<select id="deletemode" name="deletemode" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="102" data-placeholder="select" >
											<option value=""></option>
											<option value="2">All Data</option>
											<option value="3">Stock Session Data</option>
											</select>
										</div>
										<div class="static-field large-12 columns sessiondataiddiv hidedisplay">
											<label>Session Data</label>
											<select id="sessiondataid" name="sessiondataid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="102" data-placeholder="select" >
											</select>
										</div>
									</div>
									<div class="divider large-12 columns"></div>
									<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">
										<input type="button" class="alertbtnyes leftformsbtn" id="takingdelete" name="" value="Delete" tabindex="114">
										<input type="button" class="alertbtnno addsectionclose" id="closedeleteoverlay" value="Cancel" name="cancel" tabindex="115">
									</div>
								 </div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!---- Stock Taking session delete overlay----->
	<div class="large-12 small-12 medium-12 columns" style="position:absolute;">
		<div class="overlay" id="deviceclearoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
			<div class="row sectiontoppadding">&nbsp;</div>
			<div class="large-4 medium-4 small-12 large-centered medium-centered small-centered columns">
				<div class="large-12 column paddingzero">
					<div class="row">&nbsp;</div>
					<form id="deviceclearform" name="deviceclearform" class="cleardataform">
						<div id="deviceclearvalidate" class="validationEngineContainer">
							<div class="large-12 columns end paddingbtm">
								<div class="large-12 columns cleardataform z-depth-5 borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff">
									<div class="large-12 columns sectionheaderformcaptionstyle">Device Details</div>
									<div class="large-12 columns sectionpanel" style="padding: 15px 15px;">
										<div class="static-field large-12 columns">
											<label>Device</label>
											<select id="devicemacaddress" name="devicemacaddress" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="102" data-placeholder="select" >
												<option value=""></option>
												<?php foreach($rfiddevice->result() as $key):?>
													<option value="<?php echo $key->devicename;?>" data-macaddress="<?php echo $key->macaddress;?>" data-bluetoothname="<?php echo $key->bluetoothname;?>" data-connectstatus="<?php echo $key->connectstatus;?>" data-connectdatetime="<?php echo $key->connectdatetime;?>" data-rfiddeviceid="<?php echo $key->rfiddeviceid;?>" >
													<?php echo $key->devicename;?></option>
												<?php endforeach;?>
											</select>
										</div>
										<div class="input-field large-12 columns">
											<input type="text" id="devmacaddress" name="devmacaddress" data-validation-engine="" class="" readonly >
											<label for="devmacaddress">MAC Address</label>
										</div>
										<div class="input-field large-6 columns">
											<input type="text" id="devbluetoothname" name="devbluetoothname" data-validation-engine="" value="" class="" readonly >
											<label for="devbluetoothname">Bluetooth Name</label>
										</div>
										<div class="input-field large-6 columns">
											<input type="text" id="devconnectstatus" name="devconnectstatus" data-validation-engine="" value="" class="" readonly >
											<label for="devconnectstatus">Connect Status</label>
										</div>
										<div class="input-field large-6 columns">
											<input type="text" id="devconnectrequestdatetime" name="devconnectrequestdatetime" data-validation-engine="" value="" class="" readonly >
											<label for="devconnectrequestdatetime">Connect Date & Time</label>
										</div>
									</div>
									<div class="divider large-12 columns"></div>
									<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">
										<input type="button" class="alertbtnyes leftformsbtn" id="deviceclearsubmit" name="" value="Clear" tabindex="114">
										<input type="button" class="alertbtnno addsectionclose" id="closedeviceclearverlay" value="Cancel" name="cancel" tabindex="115">
									</div>
								 </div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

<!----Stock session Detail overlay----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="stocksessiondetailoverlay" style="overflow-y: auto;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="large-10 medium-10 small-10 large-centered medium-centered small-centered columns" >
			<form method="POST" name="ordergridform" style="" id="ordergridform" action=""  class="clearformbordergridoverlay">
				<span id="ordergridvalidation" class="validationEngineContainer"> 
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 columns sectionheaderformcaptionstyle">
							Stock Session Details
						</div>
						<?php
							echo '<div class="large-12 columns forgetinggridname" id="stocksessiondetailsgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="stocksessiondetailsgrid" style="max-width:2000px; height:300px;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
							<!--<div class="inner-gridfooter footer-content footercontainer" id="stocksessiondetailsgridfooter">
									 Footer & Pagination content 
								</div>--></div>';
						?>
					</div>
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
							<input type="button" id="stocksessiondetailprint" name="stocksessiondetailprint" value="Print" class="alertbtnyes">
							<input type="button" id="stocksessiondetailgridclose" name="stocksessiondetailgridclose" value="Cancel" class="alertbtnno">
						</div>
					</div>
					<div class="row">&nbsp;</div>
					</span>
					 <input type="hidden" name="stocksessiondetailhidden" id="stocksessiondetailhidden" />
			</form>
		</div>
	</div>
</div>
</body>

    <div class="large-12 columns" style="position:absolute;">
		<div class="overlay alertsoverlay overlayalerts" id="scannedalert">	
			<div class="row">&nbsp;</div>
			<div class="row sectiontoppadding"></div>
			<div class="overlaybackground">
				<div class="row">&nbsp;</div>
				<div class="alert-panel">
					<div class="alertmessagearea">
					<div class="alert-message">This Combination is already scanned,Do you  want to view/scan it ?</div>
					</div>
					<div class="alert-panel">
					<div class="alertbuttonarea">
						<span class="firsttab" tabindex="1000"></span>
						<input type="button" id="scannedcloseyes" name="" tabindex="1001" value="Yes" class="alertbtnyes  ffield" >
						<input type="button" id="scannedcloseno" name="" value="No" tabindex="1002" class="flloop alertbtnno alertsoverlaybtn" >
						<span class="lasttab" tabindex="1003"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js File -->
	<script src="<?php echo base_url();?>js/Stocktaking/stocktaking.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/filecomputer/fileupload.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/treemenu/modernizr.custom.min.js" type="text/javascript"></script>  
    <script src="<?php echo base_url();?>js/plugins/treemenu/jquery.dlmenu.js" type="text/javascript"></script> 
	</html>