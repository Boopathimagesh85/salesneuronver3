<style type="text/css">
#sessionformgrid .gridcontent {
	 height: 100% !important;
}
.grid-view {
	height: 100% !important;
}
</style>

<?php
				$device = $this->Basefunctions->deviceinfo();
				$dataset['gridtitle'] = 'Stocktaking Session';
				$dataset['titleicon'] = 'material-icons add_circle';
				$modtabgroup[0] = array("tabpgrpid"=>"1","tabgrpname"=>"Stocktaking Session","moduleid"=>"103","templateid"=>"2");
				$dataset['modtabgrp'] = $modtabgroup;
				$dataset['moduleid'] = '103';
				$dataset['otype'] = 'overlay';
				$dataset['action'][0] = array('actionid'=>'groupcloseaddform','actiontitle'=>'Close','actionmore'=>'No','ulname'=>'stocksession');
				$this->load->view('Base/formwithgridmenuheader.php',$dataset);
?>
	<div class="large-12 columns addformunderheader">&nbsp;</div>
		<div class="large-12 columns scrollbarclass addformcontainer" style="height:100% !important;">
		<div class="row mblhidedisplay">&nbsp;</div>
		<div id="stocktakingsessionoverlay" class="closed effectbox-overlay effectbox-default">
  		<div class="large-12 columns mblnopadding">
  		<form method="POST" name="stocktakingsessionform" class="stocktakingsessionform"  id="stocktakingsessionform" style="height:100% !important;">
			<div id="stocktakingsessionvalidate" class="validationEngineContainer" style="height:100% !important;">			
				<div class="large-4 columns large-offset-8 paddingbtm" style="padding-right:0px !important;height:100% !important;">	
					<div class="large-12 columns cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;height:100% !important;">
					<div class="large-12 columns sectionheaderformcaptionstyle">Stock Taking Session</div>
					  <div class="large-12 columns sectionpanel">
					  <div class="static-field large-12 columns hidedisplay">
							<label>Date<span class="mandatoryfildclass">*</span></label>								
							<input id="stocktakinsessiondate" class="fordatepicicon" data-validation-engine="validate[required]" type="text" data-dateformater="dd-mm-yy" data-prompt-position="topLeft" readonly="readonly" tabindex="101" name="stocktakinsessiondate">
					  </div>
					  <div class="input-field large-12 columns hidedisplay">
							<input type="text" class="" data-validation-engine="validate[required]" id="stocktakingsessionid" name="stocktakingsessionid" value="" tabindex="105" readonly>
							<label for="stocktakingsessionid">Session Id</label>	
					  </div>
					  <div class="static-field large-12 columns hidedisplay">
							<label>Branch<span class="mandatoryfildclass">*</span></label>								
							<select id="branchid" name="branchid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="102">
								<option value=""></option>
								<?php foreach($branch as $key):?>
								<option value="<?php echo $key->branchid;?>">
								<?php echo $key->branchname;?></option>
								<?php endforeach;?>	 
							</select>
					  </div>
						<div class="static-field large-6 columns">
							<label>Mode<span class="mandatoryfildclass">*</span></label>
							<select id="stocksessionmodeid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" name="stocksessionmodeid" data-placeholder="select module" tabindex="107">
								<?php foreach($stocksessionmode as $key):?>
									<option value="<?php echo $key->stocksessionmodeid;?>">
									<?php echo $key->stocksessionmodename;?></option>
								<?php endforeach;?>
							</select>
						</div>
						<div class="static-field large-6 columns">
							<label>Method<span class="mandatoryfildclass">*</span></label>
							<select id="stocksessionmethodid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" name="stocksessionmethodid" data-placeholder="select module" tabindex="107">
								<option value="2">RFID</option>
								<option value="3">Barcode</option>
								<option value="4">RFID Tag Machine</option>
							</select>
						</div>
						<div class="static-field large-12 columns hidedisplay" id="stocksessiondeviceno_div">
								<label>Device No<span class="mandatoryfildclass">*</span></label>
								<select id="stocksessiondeviceno" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" name="stocksessiondeviceno" data-placeholder="select deviceno" tabindex="107">
									<option value=""></option>
									<?php foreach($rfiddevice->result() as $key):?>
										<option value="<?php echo $key->devicename;?>" data-macaddress="<?php echo $key->macaddress;?>" data-bluetoothname="<?php echo $key->bluetoothname;?>" data-connectioncheck="<?php echo $key->connectioncheck;?>" data-startstoptrigger="<?php echo $key->startstoptrigger;?>" data-rfiddeviceid="<?php echo $key->rfiddeviceid;?>" >
										<?php echo $key->devicename;?></option>
									<?php endforeach;?>
								</select>
						</div>
						<div class="static-field large-12 columns">
							<label>Data Type<span class="mandatoryfildclass">*</span></label>
							<select id="itemtagdatatypeid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" name="itemtagdatatypeid" data-placeholder="select module" tabindex="108">
								<option value="0">Itemtag Number</option>
								<option value="1">RFID</option>
							</select>
						</div>
					  <div class="static-field large-12 columns " id="purityidhdivid">
							<label>Purity<span class="mandatoryfildclass">*</span></label>						
							<select id="purityid" name="purityid" class="chzn-select dropdownchange" data-validation-engine="validate[required]" multiple data-prompt-position="topLeft:14,36" tabindex="103">
							<option value=""></option> 
							<?php $prev = ' ';
							foreach($purity->result() as $key):
							$cur = $key->metalid;
							$a="";
							if($prev != $cur )
							{
								if($prev != " ")
								{
									 echo '</optgroup>';
								}
								echo '<optgroup  label="'.$key->metalname.'">';
								$prev = $key->metalid;
							}
							?>
							<option value="<?php echo $key->purityid;?>"><?php echo $key->purityname;?></option>
							<?php endforeach;?>
							</select>
						</div>			
						<div class="static-field large-12 columns">
								<label>Type<span class="mandatoryfildclass">*</span></label>
								<select id="stocktakingsessiontypeid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" name="stocktakingsessiontypeid" data-placeholder="select module" tabindex="107" multiple>
									<option value=""></option>
									<?php foreach($stocktakingtype as $key):?>
										<option value="<?php echo $key->stocktakingtypeid;?>">
										<?php echo $key->stocktakingtypename;?></option>
									<?php endforeach;?>
								</select>
						</div>
						<div class="static-field large-12 columns">
								<label>Conflict Track<span class="mandatoryfildclass">*</span></label>
								<select id="conflicttrackid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" name="conflicttrackid" data-placeholder="select module" tabindex="107">
									<option value="2">Yes</option>
									<option value="3">No</option>
								</select>
						</div>
						<div class="input-field large-12 columns">
							<textarea name="comment" class="materialize-textarea" id="comment" rows="3" cols="40"></textarea>
							<label for="comment">Comment</label>
						</div>	
						<div class="row">&nbsp;</div>
						</div>
						<div class="divider large-12 columns"></div>
						<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">					
							<input type="button" class="alertbtnyes addkeyboard" id="stocktakingsessioncreate" name="stocktakingsessioncreate" value="Save" tabindex="">
							<input type="button" class="alertbtnno addsectionclose" name="" value="Close" tabindex="">
						</div>
						</div>
				</div>
			</div>
		</form>
		</div>
	   </div>
		<div class="large-12 columns paddingbtm">
					<div class="large-12 columns paddingzero" style="height:100% !important;">
						<div class="large-12 columns viewgridcolorstyle paddingzero borderstyle" style="height:98% !important;">
							<div class="large-12 columns headerformcaptionstyle " style="padding: 0.2rem;height:auto;">
								<span class="large-6 medium-6 small-6 columns lefttext" >Stocktaking Session List</span>
								<span class="large-6 medium-6 small-6 columns innergridicon righttext">
										 <span class="addiconclass" title="Add" id="sessionaddicon"><i class="material-icons">add</i></span>
										 <span class="canceliconclass" title="Close" id="sessionidclose"><i class="material-icons">cancel</i></span>
								</span>
							</div>
							<?php
					$device = $this->Basefunctions->deviceinfo();
					if($device=='phone') {
						echo '<div class="large-12 columns paddingzero forgetinggridname" id="sessionformgridwidth" style="height:100% !important;"><div class=" inner-row-content inner-gridcontent" id="sessionformgrid" style="height:86% !important;top:0px;">
							<!-- Table header content , data rows & pagination for mobile-->
						</div>
						<footer class="inner-gridfooter footercontainer" id="sessionformgridfooter">
							<!-- Footer & Pagination content -->
						</footer></div>';
					} else {
						echo '<div class="large-12 columns forgetinggridname " id="sessionformgridwidth" style="padding-left:0;padding-right:0;height:100% !important;"><div class="desktop row-content inner-gridcontent" id="sessionformgrid" style="max-width:2000px; height:86%!important;top:0px;">
						<!-- Table content[Header & Record Rows] for desktop-->
						</div>
						<div class="footer-content footercontainer" id="sessionformgridfooter">
							<!-- Footer & Pagination content -->
						</div></div>';
					}
		?>
						</div>
					</div>
				</div>
</div>
<!-- Total Sales Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="totalsalesoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">	
		<div class="row sectiontoppadding">&nbsp;</div>
		<div class="large-3 medium-10 large-centered medium-centered columns">
			<div class="alert-panel" style="background-color:#465a63">
					<div class="alertmessagearea">
						<div class="alert-title">Rfid Scanning</div>
						<form id="totalsalesoverlay" name="totalsalesoverlay" class="">
							
							<div style="" id="totalsalesvalidation" class="validationEngineContainer">
								<span class="firsttab" tabindex="1000"></span>
								<div class="input-field overlayfield">
									<input id="totaltags" class="" type="text" data-prompt-position="bottomLeft" tabindex="1002" name="totaltags" readonly>
									<label for="totaltags">Total Tags</label>
								</div>
								<div class="input-field overlayfield">
									<input id="loadtags" class="" type="text" data-prompt-position="bottomLeft" tabindex="1002" name="loadtags" readonly>
									<label for="loadtags">Loaded Tags</label>
								</div>
								<input type="hidden" id="totaltagshidden">
								<input type="hidden" id="totaltagsnohidden">
								<input type="hidden" id="allrfideviceno" value="<?php echo $deviceno;?>">
							</div>
						</form>
					</div>
					<div class="alertbuttonarea">
						<input type="button" id="totalsalesaddsbtn" name="" value="Submit" class="alertbtnyes" tabindex="1003">
						<input type="button" id="totalsalesclose" name="" value="Cancel" class="alertbtnno flloop alertsoverlaybtn" tabindex="1004" >
						<input type="button" id="totalsalescleartbl" name="" value="Clear" class="alertbtnno flloop alertsoverlaybtn" tabindex="1005" >
						<input type="button" id="totalsalesfetchtbl" name="" value="Fetch" class="alertbtnno flloop alertsoverlaybtn" tabindex="1006" >
						<span class="lasttab" tabindex="1007"></span>
					</div>
				</div>
			
		</div>
	</div>
</div>
