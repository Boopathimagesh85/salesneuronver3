<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Discountauthorizationmodel extends CI_Model{
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//create counter
	public function newdatacreatemodel() {
		$data = $this->checkexistdata();
		if($data > 0) {
			echo 'EXIST';
		}else {		
			$industryid = $this->Basefunctions->industryid;
			//table and fields information	
			$formfieldsname = explode(',',$_POST['discountauthorizationelementsname']);
			$formfieldstable = explode(',',$_POST['discountauthorizationelementstable']);
			$formfieldscolmname = explode(',',$_POST['discountauthorizationelementscolmn']);
			$elementpartable = explode(',',$_POST['discountauthorizationelementspartabname']);
			//filter unique parent table
			$partablename =  $this->Crudmodel->filtervalue($elementpartable);
			//filter unique fields table
			$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
			$tableinfo = explode(',',$fildstable);
			$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
			$primaryid = $this->db->insert_id();
			//password insertion
			$discount_password = $_POST['discauthpassword'];
			$userpassword = preg_replace('/[^A-Za-z0-9]/', '',$discount_password);
			$userpassword = strtolower($userpassword);
			$hash = password_hash($userpassword, PASSWORD_DEFAULT);
			$hasharray=array('dapassword'=>$hash);
			$this->db->where('discountauthorizationid',$primaryid);
			$this->db->update('discountauthorization',$hasharray);
			//audit-log
			$user = $this->Basefunctions->username;
			$userid = $this->Basefunctions->logemployeeid;
			$activity = ''.$user.' created DiscountAuthorization';
			$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,110);
			echo 'TRUE';
		}
	}
	public function checkexistdata(){
		$discountmode = $_POST['discountmodeid'];
		$userroleid = $_POST['userroleid'];
		$mdata=explode(",",$userroleid);
		if(isset($_POST['metalid'])) {
			$metalid = $_POST['metalid'];
		}else {
			$metalid = 1;
		}
		if(isset($_POST['purityid'])) {
			$purityid = $_POST['purityid'];
		}else {
			$purityid = 1;
		}
		$categoryid = $_POST['categoryid'];
		$discounttype = $_POST['discounttypeid'];
		$wherequery = '';
		$wherequery .= 'discountmodeid = "'.$discountmode.'" ';
		$wherequery .= 'AND discounttypeid = "'.$discounttype.'" ';
		$default = 1;
		if($discountmode == 2) {
			if(!empty($metalid)) {
				$wherequery .= 'AND metalid = "'.$metalid.'" ';
			}else{
				$wherequery .= 'AND metalid = "'.$default.'" ';
			}
			if(!empty($purityid)) {
				$wherequery .= 'AND purityid = "'.$purityid.'" ';
			}else{
				$wherequery .= 'AND purityid = "'.$default.'" ';
			}
			if(!empty($categoryid)) {
				$wherequery .= 'AND categoryid = "'.$categoryid.'" ';
			}else{
				$wherequery .= 'AND categoryid = "'.$default.'" ';
			}
		}
		for($i=0; $i<count($mdata); $i++) {
			if(count($mdata) > 1) {
				if($i == 0) {
					$wherequery .= "AND ( FIND_IN_SET(".(int)$mdata[$i].",userroleid)>0";
				}else {
					$wherequery .= " OR FIND_IN_SET(".(int)$mdata[$i].",userroleid)>0";
				}
			}else{
				$wherequery .= "AND ( FIND_IN_SET(".(int)$mdata[$i].",userroleid)>0";
			}
		}
		$wherequery .= ') AND status NOT IN (0,3)';
		$data = $this->db->query('select discountauthorizationid from discountauthorization WHERE '.$wherequery.' ')->num_rows();
		return $data;
	}
	//retrive counter data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['discountauthorizationelementsname']);
		$formfieldstable = explode(',',$_GET['discountauthorizationelementstable']);
		$formfieldscolmname = explode(',',$_GET['discountauthorizationelementscolmn']);
		$elementpartable = explode(',',$_GET['discountauthorizationelementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['discountauthorizationprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	//update counter
	public function datainformationupdatemodel() {
		$industryid = $this->Basefunctions->industryid;
		//table and fields information
		$formfieldsname = explode(',',$_POST['discountauthorizationelementsname']);
		$formfieldstable = explode(',',$_POST['discountauthorizationelementstable']);
		$formfieldscolmname = explode(',',$_POST['discountauthorizationelementscolmn']);
		$elementpartable = explode(',',$_POST['discountauthorizationelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['discountauthorizationprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		//password insertion
		$discount_password = $_POST['discauthpassword'];
		$userpassword = preg_replace('/[^A-Za-z0-9]/', '',$discount_password);
		$userpassword = strtolower($userpassword);
		$hash = password_hash($userpassword, PASSWORD_DEFAULT);
		$hasharray=array('dapassword'=>$hash);
		$this->db->where('discountauthorizationid',$primaryid);
		$this->db->update('discountauthorization',$hasharray);
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity = ''.$user.' updated DiscountAuthorization';
		$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$activity ,$userid,110);
		echo $result;
	}
	//check password is same or not
	public function checkpasswordisequal($primaryid,$newpassword) {
		$this->db->select('dapassword');
		$this->db->from('discountauthorization');
		$this->db->where('discountauthorizationid',$primaryid);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$oldpassowrd = $row->dapassword;
				if($oldpassowrd == $newpassword) {
					return 'True';
				} else {
					return 'False';
				}
			}
		} else {
			return 'False';
		}
	}
	//delete counter
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['discountauthorizationelementstable']);
		$parenttable = explode(',',$_GET['discountauthorizationparenttable']);
		$id = $_GET['discountauthorizationprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity = ''.$user.' deleted DiscountAuthorization';
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,110);
				echo "TRUE";
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,110);
			echo "TRUE";
		}
	}
	public function getcatalogparentid() {
		$parentcatalogid='';
		$catalogname='';
		$counterdefaultid='';
		$id=$_GET['id'];
		$categoryid = $this->Basefunctions->singlefieldfetch('categoryid','discountauthorizationid','discountauthorization',$id);
		$data=$this->db->select('categoryid,categoryname')
		->from('category')
		->where('category.categoryid',$categoryid)
		->get();
		if($data->num_rows() > 0){
			foreach($data->result() as $in) {
				$parentcatalogid=$in->categoryid;
				$catalogname=$in->categoryname;
			}
			$a=array('parentcatalogid'=>$parentcatalogid,'catalogname'=>$catalogname);
		}else{
			$a=array('parentcatalogid'=>'','catalogname'=>'');
		}
		echo json_encode($a);
	}
	//old password check
	public function oldpasswordcheckmodel() {
		$primaryid = $this->Basefunctions->logemployeeid;
		$pass = "";
		$oldpassword = $_GET['oldpassword'];
		$this->db->select('employee.password');
		$this->db->from('employee');
		$this->db->where('employee.employeeid',$primaryid);
		$this->db->where('employee.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$pass = $row->password;
			}
		}
		if (password_verify($oldpassword, $pass)) {
			echo "TRUE";
		} else {
			echo "Fail";
		}
	}
	public function loadpuritymodel() {
		$data = array();
		$i=0;
		$industryid = $this->Basefunctions->industryid;
		$info =$this->db->query("SELECT `metal`.`metalid`, `purity`.`purityid`, `purity`.`purityname` FROM `purity` JOIN `metal` ON `metal`.`metalid` = `purity`.`metalid` WHERE `purity`.`status` = 1 and `purity`.`industryid` = ".$industryid." GROUP BY `purity`.`purityid` ORDER BY `purity`.`purityid` DESC");
		foreach($info->result() as $row) {
			$data[$i] = array('metalid'=>$row->metalid,'purityid'=>$row->purityid,'purityname'=>$row->purityname);
			$i++;
		}
		echo json_encode($data);
	}
}