<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Discountauthorization extends MX_Controller{
    public function __construct() {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Discountauthorization/Discountauthorizationmodel');	
    }
    //first basic hitting view
    public function index() {        
    	$moduleid = array(110);
    	sessionchecker($moduleid);
		/*action*/
		$data['actionassign'] = $this->Basefunctions->actionassign($moduleid); 
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Discountauthorization/discountauthorizationview',$data);
	}
	//create storage
	public function newdatacreate() {  
    	$this->Discountauthorizationmodel->newdatacreatemodel();
    }
	//information fetchstorage
	public function fetchformdataeditdetails() {
		$moduleid = array(110);
		$this->Discountauthorizationmodel->informationfetchmodel($moduleid);
	}
	//update storage
    public function datainformationupdate() {
        $this->Discountauthorizationmodel->datainformationupdatemodel();
    }
	//delete storage
    public function deleteinformationdata() {
    	$moduleid = array(110);
		$this->Discountauthorizationmodel->deleteoldinformation($moduleid);
    }
    //reload the storage list element
    public function getcatalogparentid() {
    	$this->Discountauthorizationmodel->getcatalogparentid();
    }    
   	//old password validate
	public function oldpasswordcheck() {
		$this->Discountauthorizationmodel->oldpasswordcheckmodel();
	}
	public function loadpurity(){
		$this->Discountauthorizationmodel->loadpuritymodel();
	}
}