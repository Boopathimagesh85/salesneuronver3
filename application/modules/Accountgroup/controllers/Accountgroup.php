<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Accountgroup extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Accountgroup/Accountgroup_model');	
    }
	//first basic hitting view
    public function index()
    {
		$moduleid = array(41);
		sessionchecker($moduleid);
		//action		
		$data['moduleid']=$moduleid;
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['filedmodids'] = $moduleid;
		//form
		$data['account'] = $this->Accountgroup_model->accountgroup_dd();
		$data['accounttype']=$this->Basefunctions->simpledropdownwithcond('accounttypeid','accounttypeid,accounttypename','accounttype','accounttypeid','6,16,17');
		$this->load->view('Accountgroup/accountgroupview',$data);
	}
	/**
	*account groups creation
	*/
    public function create() 
    {  
    	$this->Accountgroup_model->create();
    }
	/**
	*account groups retrieve-for edit/view 
	*/
    public function retrive() 
    {  
    	$this->Accountgroup_model->retrive();
    }
	/**
	*account groups update
	*/
    public function update()
    {
		$this->Accountgroup_model->update();
    }
	/**
	*account groups delete
	*/
    public function delete() {
		$this->Accountgroup_model->delete();
	}
	public function accountmembersubmit(){
		$this->Accountgroup_model->accountmembersubmitmodel();
	}
	public function loadaccountgroup(){
		$this->Accountgroup_model->loadaccountgroup();
	}
	/* Fetch Main grid header,data,footer information */
	public function gridinformationfetch() {
		$creationid = $_GET['viewid'];
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$viewcolmoduleids = $_GET['viewfieldids'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$filter = $_GET['filter'];
		$sortcol = isset($_GET['sortcol']) ? $_GET['sortcol'] : '';
		$sortord = isset($_GET['sortord']) ? $_GET['sortord'] : '';
		$footer = isset($_GET['footername']) ? $_GET['footername'] : 'empty';
		$memberdata=$this->Accountgroup_model->get_accountmember($_GET['groupid']);
		$result = $this->Basefunctions->viewdynamicdatainfofetch($creationid,$primaryid,$viewcolmoduleids,$pagenum,$rowscount,$sortcol,$sortord,$filter);
		if($memberdata->num_rows() > 0){
			$checkboxarr=$memberdata->result()[0]; 
			$arrcheckbox=explode(",",$checkboxarr->accountid);
		} else {
			$arrcheckbox[]='';
		}
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobileviewgriddatagenerate($result,$primarytable,$primaryid,$width,$height);
		} else {
			$datas = chkviewgriddatagenerate($result,$primarytable,$primaryid,$width,$height,$footer,'true',$arrcheckbox);
		}
	    echo json_encode($datas);
	}
	public function accountgroupgridinformationfetch()
	{
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'accountgroup.accountgroupid') : 'accountgroup.accountgroupid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
	
		$colinfo = array('colmodelname'=>array('accountgroupname','accounttype','noofaccount','shortname','description'),'colmodelindex'=>array('accountgroup.accountgroupname','accounttype.accounttypename','account.noofaccount','accountgroup.shortname','accountgroup.description'),'coltablename'=>array('accountgroup','accounttype','accountgroup','accountgroup','accountgroup'),'uitype'=>array('2','19','2','2','2'),'colname'=>array('Account group','Account Type','Total Number of Members','Short Name','Description'),'colsize'=>array('100','100','50','50','100'));
	
		$result=$this->Accountgroup_model->accountgroupgridview($primarytable,$sortcol,$sortord,$pagenum,$rowscount);
		$device = $this->Basefunctions->deviceinfo();
		//if($device=='phone') {
			//$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'accountgroup',$width,$height);
		//}else {
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'accountgroup',$width,$height);
		//}
		echo json_encode($datas);
	}
}