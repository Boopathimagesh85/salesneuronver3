<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Accountgroup_model extends CI_Model
{
    private $accountgroupmodule = 41;
	public function __construct()
    {       
		parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }	
	/**	*	*/
	public function accountgroup_dd()
	{
		$industryid=$this->Basefunctions->industryid;
			$query = $this->db->query(
					   "SELECT account.accountname,account.accountid,accounttype.accounttypename,account.accounttypeid
						FROM account
						JOIN accounttype ON accounttype.accounttypeid = account.accounttypeid
						WHERE account.status =1 AND account.industryid = ".$industryid."
						ORDER BY account.accounttypeid asc
						");
		return $query;
	}
	/**	*	*/
	public function create() 
	{
		$insert = array(
						'accountgroupname' => $_POST['accountgroupname'],
						'shortname' => $_POST['shortname'],
						'accounttypeid' => $_POST['accounttypeid'],
						'description' => $_POST['description'],
						'accountid' => 1,
						'industryid'=>$this->Basefunctions->industryid,
						'status' => $this->Basefunctions->activestatus,
						'branchid'=>$this->Basefunctions->branchid												
					  );
		$insert = array_merge($insert,$this->Crudmodel->defaultvalueget());
	    $this->db->insert('accountgroup',array_filter($insert));
	    $primaryid = $this->db->insert_id();
	    //audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity = ''.$user.' created AccountGroup - '.$_POST['accountgroupname'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,$this->accountgroupmodule);
		echo 'SUCCESS';
	}
	/**	*	*/
	public function retrive()
	{
		$id = $_GET['primaryid'];
		$this->db->select('accountgroupname,shortname,accountid,description,accountgroup.accountgroupid,accounttypeid');
		$this->db->from('accountgroup');		
		$this->db->where('accountgroup.accountgroupid',$id);
		$this->db->limit(1);
		$info=$this->db->get()->row();
		
		$jsonarray=array(
							'shortname'=>$info->shortname,
							'description'=>$info->description,
							'primaryaccountgroupid'=>$info->accountgroupid,
							'accountgroupprimaryname'=>$info->accountgroupname,
							'accountgroupshortprimaryname'=>$info->shortname,
							'accountgroupname'=>$info->accountgroupname,							
							'accountid'=>$info->accountid,
							'accounttypeid'=>$info->accounttypeid
						);
		echo json_encode($jsonarray);
	}
	/**	*	*/
	public function update()
	{
		$id=$_POST['primaryid'];
		$update = array(
						'accountgroupname' => $_POST['accountgroupname'],
						'shortname' => $_POST['shortname'],
						'accounttypeid' => $_POST['accounttypeid'],
						'description' => $_POST['description'],
						'industryid'=>$this->Basefunctions->industryid,
						'branchid'=>$this->Basefunctions->branchid
					  );
		$update = array_merge($update,$this->Crudmodel->updatedefaultvalueget());
	    $this->db->where('accountgroupid',$id);
		$this->db->update('accountgroup',$update);
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity = ''.$user.' updated AccountGroup - '.$_POST['accountgroupname'].'';
		$this->Basefunctions->notificationcontentadd($id,'Updated',$activity ,$userid,$this->accountgroupmodule);
		echo 'SUCCESS';
	}
	/**
	*inactivate-delete accountgroup grid	
	*/
	public function delete()
	{
		$id=$_GET['primaryid'];
		//inactivate accountgroup
		$inactive = $this->Basefunctions->delete_log();
		$this->db->where('accountgroupid',$id);
		$this->db->update('accountgroup',$inactive);
		//audit-log
		$accountgroupdel = $this->Basefunctions->singlefieldfetch('accountgroupname','accountgroupid','accountgroup',$id);
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity = ''.$user.' deleted AccountGroup - '.$accountgroupdel.'';
		$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,$this->accountgroupmodule);	 
		echo 'SUCCESS';
	}
	/**
	*retrieve accountgroup grid	
	*/
	public function accountgroupview($sidx,$sord,$start,$limit,$wh)
	{
		$this->db->select('accountgroup.accountgroupname,accountgroup.shortname,accountgroup.accountid,accountgroup.description,accountgroup.accountgroupid,accounttype.accounttypename,account.accountname');
		$this->db->from('accountgroup');
		$this->db->join('account','account.accountid=accountgroup.accountid');
		$this->db->join('accounttype','accounttype.accounttypeid=accountgroup.accounttypeid');
		$this->db->where('accountgroup.status',$this->Basefunctions->activestatus);
		if($wh != "1")
		{
			$this->db->where($wh);
		}
		$this->db->order_by($sidx,$sord);
		$this->db->limit($limit,$start);
		$result=$this->db->get();
		return $result;
	}
	/**
	*
	*/
	// retrive account members
	public function accountmembersview($sidx,$sord,$start,$limit,$wh,$accountgroupid,$accounttypeid)
	{
		$acc=$accountgroupid.',1'; 
		$accountgroupid=explode(',',$acc); 
		$this->db->select('account.accountname,account.accountgroupid,account.accountid');
		$this->db->from('account');
		$this->db->where('account.status',$this->Basefunctions->activestatus);
		$this->db->where_in('account.accountgroupid',$accountgroupid);
		$this->db->where('account.accounttypeid',$accounttypeid);
		if($wh != "1")
		{
			$this->db->where($wh);
		}
		$this->db->order_by('account.accountid','desc');
		$this->db->limit($limit,$start);
		$result=$this->db->get();
		return $result;
	}
	public function get_accountmember($accountgroupid){
		$this->db->select('accountid');
		$this->db->from('accountgroup');
		$this->db->where('accountgroup.status',$this->Basefunctions->activestatus);
		$this->db->where('accountgroup.accountgroupid',$accountgroupid);
		$this->db->order_by('accountgroup.accountgroupid');
		$result=$this->db->get();
		return $result;
	}
	public function get_accountname($value)
	{
		$g_accountname = '';
		$groupmember = array_filter(explode(',',$value));
		if(count($groupmember) > 0)
		{
			$this->db->select('GROUP_CONCAT(accountname) AS name', FALSE);
			$this->db->from('account');		
			$this->db->where_in('account.accountid',$groupmember);	
			$this->db->where('account.status',$this->Basefunctions->activestatus);
			$info = $this->db->get();
			foreach($info->result() as $val){
				$g_accountname = $val->name;
			}
		}
		return $g_accountname ;
	}
	public function accountmembersubmitmodel() {
		$groupid=$_POST['groupid'];
		$rowmemberid=$_POST['allmemberid'];
		$allmemberid = explode(',',$rowmemberid); 
		$griddata=$_POST['membergriddata'];
		$formdata=json_decode( $griddata, true ); 
		$count = count($formdata); 
		$account_member=array();
		for( $i=0; $i <$count ; $i++ ) { 
			if(isset($allmemberid[$i]) && in_array($allmemberid[$i],$formdata)) // checked account
			{	$accid=$allmemberid[$i];
				array_push($account_member,$allmemberid[$i]);
			}
			else{// unchecked account
			 	$accid=$formdata[$i];
			}
		}
		$check_account = array();
		$uncheck_account = array();
		$check_accountdata= array();
		$uncheck_accountdata = array();
		$check_account=array_intersect($formdata,$allmemberid); 
		$check_accountdata = implode(',',$check_account);
		$check_accountdata = explode(',',$check_accountdata);
		$account_update=array('accountgroupid'=>$groupid);
		$this->db->where_in('accountid',$check_accountdata);
		$this->db->update('account',$account_update);
		$stonechargeid = $this->Basefunctions->singlefieldfetch('stonechargeid','accountgroupid','accountgroup',$groupid);
		if($stonechargeid != 1) { //Stonecharegeid update in account
			$accountst_update=array('stonechargeid'=>$stonechargeid);
			$this->db->where_in('accountid',$check_accountdata);
			$this->db->update('account',$accountst_update);
		}
		$uncheck_account=array_diff($formdata,$allmemberid);
		$uncheck_accountdata = implode(',',$uncheck_account);
		$uncheck_accountdata = explode(',',$uncheck_accountdata);
		$unaccount_update=array('accountgroupid'=>1,'stonechargeid'=>1);
		$this->db->where_in('accountid',$uncheck_accountdata);
		$this->db->update('account',$unaccount_update);
		
		$account_member=implode(',',$account_member); // account members updated in account group
		$account_members_update=array('accountid' => $account_member);
		$this->db->where('accountgroupid',$groupid);
		$this->db->update('accountgroup',$account_members_update);
		$this->db->update('accountgroup',$this->Crudmodel->updatedefaultvalueget());
		//audit-log
		$accountgroup = $this->Basefunctions->singlefieldfetch('stonechargename','stonechargeid','stonecharge',$groupid);
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity = ''.$user.' updated AccountGroup - '.$accountgroup.'';
		$this->Basefunctions->notificationcontentadd($groupid,'Updated',$activity ,$userid,$this->accountgroupmodule);
		echo 'success';
	}
	public function loadaccountgroup() {
    	$this->db->select('accountgroupid,accountgroupname,accounttypeid');
    	$this->db->from('accountgroup');
    	$this->db->where_not_in('status',array(3,0,2));
    	$info=$this->db->get();
    	foreach($info->result() as $data){
    		$accountgroupdetails[]=array(
    				'accountgroupid'=>$data->accountgroupid,
    				'accountgroupname'=>$data->accountgroupname,
    				'accounttypeid'=>$data->accounttypeid
    		);
    	}
		echo json_encode($accountgroupdetails);
    }
    public function accountgroupgridview($tablename,$sortcol,$sortord,$pagenum,$rowscount) {
    	$industryid=$this->Basefunctions->industryid;
    	$dataset ='accountgroup.accountgroupid,group_concat(distinct account.accountname) as accountname,group_concat(distinct account.accountid) as accountid,accountgroup.accountgroupname,accounttype.accounttypename,accountgroup.shortname,accountgroup.description';
    
    	$join=' LEFT OUTER JOIN accounttype ON accounttype.accounttypeid=accountgroup.accounttypeid';
    	$join .=' LEFT OUTER JOIN account ON FIND_IN_SET(account.accountid,accountgroup.accountid) > 0';
    	$status=$tablename.'.status IN (1) AND '.$tablename.'.industryid IN('.$industryid.')';
    	/* pagination */
    	$pageno = $pagenum-1;
    	$start = $pageno * $rowscount;
    	/* query */
    	$result = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$status.' GROUP BY accountgroup.accountgroupid ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
    	$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
    	$data=array();
    	$i=0;
    	foreach($result->result() as $row) {
    		$accountgroupname = $row->accountgroupname;
    		if($row->accountname == ''){
    			$noofaccount = 0;
    		}else{
	    		$accountname =  ltrim($row->accountid, ',');
	    		$accountname =  rtrim($accountname, ',');
				$noofaccount = count(explode(',',$accountname));
    		}
    		$accountgroupid = $row->accountgroupid;
    		$accounttypename =$row->accounttypename;
    		$shortname =$row->shortname;	
    		$description =$row->description;
    		$data[$i]=array('id'=>$accountgroupid,$accountgroupname,$accounttypename,$noofaccount,$shortname,$description);
    		$i++;
    	}
    	$finalresult=array('0'=>$data,'1'=>$page,'2'=>'json');
    	return $finalresult;
    }
}