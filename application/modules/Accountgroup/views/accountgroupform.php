<style type="text/css">
	#memberaddgrid .gridcontent {
	height: 69vh !important;
	}
	.gridcontent{
		height: 69vh !important;
	}
	.mblnopadding {
	    padding: 5px 5px !important;
	}
	.rppdropdown {
	position:relative;
	left: 50% !important;	
	}	
</style>
<?php
	$device = $this->Basefunctions->deviceinfo();
	$dataset['gridtitle'] = $gridtitle['title'];
	$dataset['titleicon'] = $gridtitle['titleicon'];
	$dataset['moduleid'] = $moduleid[0];
	$dataset['action'][0] = array('actionid'=>'groupaddicon','actiontitle'=>'Add','actionmore'=>'Yes','ulname'=>'accountgroup');
	$dataset['action'][1] = array('actionid'=>'rolemodeulesbt','actiontitle'=>'Submit','actionmore'=>'No','ulname'=>'groupmember');
	$this->load->view('Base/formwithgridmenuheader.php',$dataset);
?>
<!-- Hidden fields -->
<input type="hidden" id="Moduleid" name="Moduleid" value="<?php echo $moduleid[0]; ?>"/>
<div class="large-12 columns addformunderheader">&nbsp; </div>
<div class="large-12 columns addformcontainer padding-space-open-for-form" style="padding-left:1.1em !important;">
<!-- More Action DD start -- Gowtham -->
		<ul id="accountgroupadvanceddrop" class="navaction-drop arrow_box"  style="width:200px;top: 56px !important;white-space: nowrap; position: absolute; opacity: 1; display: none;left: -125px !important;">
		<li id="actionspanelgift" style="color:white;background-color:#2c2f48;height: 60px;    top: -8px; position: relative;border-top-left-radius: 4px;border-top-right-radius: 4px;"><i title="actionicons" id="actionicons" class="material-icons" style="font-size: 2rem;line-height: 1.9;color: #fff;">apps</i><span style="position:relative;top:-12px;color:#fff !important;font-size:0.9rem;font-family:
			"Nunito, sans-serif;"">Actions Panel</span></li>
			<li id="editicon"><i title="groupeditchk" id="groupeditchk" class="material-icons" style="font-size: 1.4rem;line-height: 1.0;color: #788db4;">edit</i><span style="top: -6px;position: relative;">Edit</span></li>
			<li id="deleteicon"><i title="groupdeletechk" id="groupdeletechk" class="material-icons" style="font-size: 1.4rem;line-height: 1.0;color: #788db4;">delete</i><span style="top: -6px;position: relative;">Delete</span></li>
		</ul>
	<!-- More Action DD End -- Gowtham -->
	<div class="row mblhidedisplay">&nbsp;</div>
	<div id="subformspan1" class="hiddensubform transitionhiddenform"> 
		<div class="closed effectbox-overlay effectbox-default " id="accountgroupsectionoverlay">
			<div class="large-12 columns mblnopadding" style="padding-top: 0px !important;padding-bottom: 0px !important;padding-right: 0px !important;padding-left: 0px !important;">
			<form method="POST" name="accountgroupform" class="accountgroupform"  id="accountgroupform" style="height:100% !important;">
				<div id="" class="validationEngineContainer" style="height:100% !important;">
				  <div id="editvalidate" class="validationEngineContainer" style="height:100% !important;">
					 	<div class="large-4 columns paddingbtm large-offset-8 cleardataform border-modal-8px" style="padding-right:0px !important;padding-left: 0px !important;">
							<div class="large-12 columns" style="padding-left: 0 !important; padding-right: 0 !important;">
								<div class="large-12 columns sectionheaderformcaptionstyle ">Account Group</div>		
								<div class="large-12 columns sectionpanel">
								<div class="input-field large-12 columns ">
								<input type="text" class="" id="accountgroupname" name="accountgroupname" value="" tabindex="100" data-table="accountgroup"  data-validatefield="accountgroupname" data-validation-engine="validate[required,funcCall[checkuniquename],maxSize[100]]" data-primaryid="">
								<label for="accountgroupname">Account Group<span class="mandatoryfildclass">*</span></label>
								<input type="hidden" id="accountgroupprimaryname" name="accountgroupprimaryname"/>
								<input type="hidden" id="primaryaccountgroupid" name="primaryaccountgroupid"/>
								</div>
								<div class="input-field large-12 columns ">
									<input type="text" class="" id="shortname" name="shortname" value="" tabindex="102" data-table="accountgroup"  data-validatefield="shortname" data-validation-engine="validate[maxSize[50],funcCall[accountgroupshortnamecheck]]">
									<label for="shortname">Short Name</label>
									<input type="hidden" id="accountgroupshortprimaryname" name="accountgroupshortprimaryname"/>	
								</div>
								<div class="static-field large-12 columns ">
									<label>Account Type<span class="mandatoryfildclass">*</span></label>						
									<select id="accounttypeid" name="accounttypeid" class="chzn-select" tabindex="103" data-validation-engine="validate[required]" data-placeholder="Select" data-prompt-position="topLeft:14,36">
										<option value=""></option>
										<?php foreach($accounttype as $key):?>
										<option data-name="<?php echo $key->accounttypename;?>" value="<?php echo $key->accounttypeid;?>">
											<?php echo $key->accounttypename;?></option>
										<?php endforeach;?>	 
									</select>
								</div>
								<div class="input-field large-12 columns ">
									<textarea name="description" class="materialize-textarea" id="description" rows="3" cols="40" tabindex="104" data-validation-engine="validate[maxSize[200]]" data-prompt-position="bottomLeft:1,50"></textarea>
									<label for="description">Description</label>	
								</div>
								</div>
								<input type="hidden" id="accountgroupsortcolumn" name="accountgroupsortcolumn" value=""/>
								<input type="hidden" id="accountgroupsortorder" name="accountgroupsortorder" value=""/>
								<div class="divider large-12 columns"></div>
								<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">
									<input type="button" class="alertbtnyes leftformsbtn" id="addicon" name="" value="Save" tabindex="105">
									<input type="button" class="alertbtnyes leftformsbtn" style="display:none" id="editsave" name="" value="Save" tabindex="106">
									<input type="button" class="alertbtnno addsectionclose"  value="Close" name="cancel" tabindex="107">
								</div>
							</div>
						</div>					
					
				  </div>	
				</div>	
			</form>
		  </div>	
		</div>
			<?php
						$device = $this->Basefunctions->deviceinfo();
						if($device=='phone') {
							echo '<div class="large-12 columns paddingbtm">
		  							<div class="large-12 columns paddingzero">';
							echo '<div class="large-12 columns viewgridcolorstyle" style="padding-left:0;padding-right:0;">
							     <div class="large-12 columns headerformcaptionstyle" style="height:auto;padding: 0.2rem 0 0rem;">	
									<span class="large-6  medium-6 small-6 columns small-only-text-center" style="text-align:left">Account Group List<span class="fwgpaging-box accountgroupgridfootercontainer"></span></span>
									<span class="large-6 medium-6 small-6 columns innergridicon" style="text-align:right">
										 	<span class="addiconclass" title="Add" id="groupaddicon"><i class="material-icons">add</i></span>
										 	<span id="editicon"  class="editiconclass" title="Edit"><i class="material-icons">edit</i></span>
											<span id="deleteicon" class="deleteiconclass"  title="Delete"><i class="material-icons">delete</i></span>
											<!--<span id="reloadicon" class="reloadiconclass" title="Reload"><i class="material-icons">refresh</i></span>
											<span id="searchicon" class="searchiconclass" title="Search"><i class="material-icons">search</i></span>-->
										
									</span>
								</div>';
							echo '<div class="large-12 columns paddingzero forgetinggridname" id="accountgroupgridwidth"><div class=" inner-row-content inner-gridcontent" id="accountgroupgrid" style="height:420px;top:0px;">
									<!-- Table header content , data rows & pagination for mobile-->
								</div>
								<footer class="inner-gridfooter footercontainer" id="accountgroupgridfooter">
									<!-- Footer & Pagination content -->
								</footer></div>';
							echo '</div></div></div>';
						} else {
							echo '<div class="" style="padding-left:0;padding-right:0;">
							<!--<div class="large-12 columns headerformcaptionstyle" style="height:auto;padding: 0.2rem 0 0rem;">
									<span class="large-6  medium-6 small-6 columns small-only-text-center" style="text-align:left">Account Group List<span class="fwgpaging-box accountgroupgridfootercontainer"></span></span>
									<span class="large-6 medium-6 small-6 columns innergridicon small-only-text-center" style="text-align:right">
										 	<span class="addiconclass" title="Add" id="groupaddicon"><i class="material-icons">add</i> </span>
										 	<span id="editicon"  class="editiconclass" title="Edit"><i class="material-icons">edit</i> </span>
											<span id="deleteicon" class="deleteiconclass"  title="Delete"><i class="material-icons">delete</i> </span>
											<span id="reloadicon" class="reloadiconclass" title="Reload"><i class="material-icons">refresh</i> </span>
											<span id="searchicon" class="searchiconclass" title="Search"><i class="material-icons">search</i> </span>
				
									</span>
								</div>-->';
							echo '<div class="large-12 columns forgetinggridname viewgridcolorstyle borderstyle" id="accountgroupgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="accountgroupgrid" style="max-width:2000px; height:82vh !important;top:0px;">
							<!-- Table content[Header & Record Rows] for desktop-->
							</div>
								<div class="footer-content footercontainer" id="accountgroupgridfooter"> 
									<!-- Footer & Pagination content -->
								</div>
							</div>';
							echo '</div>';
						}
					?>	
	</div>
	<div id="subformspan2" class="hidedisplay hiddensubform transitionhiddenform">
		<?php
			$device = $this->Basefunctions->deviceinfo();
			if($device=='phone') {
				echo '<div class="large-12 columns paddingbtm">
						<div class="large-12 columns paddingzero">
							<div class="large-12 columns viewgridcolorstyle" style="padding-left:0;padding-right:0;">
								<div class="large-12 columns headerformcaptionstyleforprofile" style="height:auto;padding: 0.1rem 0 0;">
									<span class="large-5 medium-6 small-5 columns mblnopadding" style="line-height:1.8">
										<span class="large-6 medium-4 small-4 columns" style="line-height:1.5">
											Type:
										</span>
										<span class="large-6 medium-8 small-8 end columns">
											<select class="chzn-select validate[required]" id="memberaccounttypeid" name="memberaccounttypeid" data-placeholder="Select" data-prompt-position="topLeft:14,36">
												<option value=""></option>';
												 foreach($accounttype as $key):?>
													<option value="<?php echo $key->accounttypeid;?>" >
													<?php echo $key->accounttypename;?></option>
												<?php endforeach;?>
									<?php	echo '</select>
										</span>
									</span>
									<span class="large-5 medium-6 small-6 columns mblnopadding" style="line-height:1.8">
										<span class="large-6 medium-4 small-4 columns" style="line-height:1.5">
											Group:
										</span>
										<span class="large-6 medium-8 small-8 end columns">
											<select class="chzn-select validate[required]" id="accountgroupid" name="accountgroupid" data-placeholder="Select" data-prompt-position="topLeft:14,36">
												<option value=""></option>
											</select>
										</span>
									</span>
									<input type="hidden" id="membergroupsortcolumn" name="membergroupsortcolumn" value=""/>
									<input type="hidden" id="membergroupsortorder" name="membergroupsortorder" value=""/>
									<span class="large-2 medium-12 small-1 columns innergridicon righttext submitkeyboard mblnopadding" style="line-height:1.8">
									<span id="rolemodeulesbt" class="" title="Submit"><i class="material-icons">save</i></span>
									</span>
								</div>';
				echo '<div class="large-12 columns paddingzero forgetinggridname" id="accountgroupgridwidth"><div class=" inner-row-content inner-gridcontent" id="memberaddgrid" style="height:420px;top:0px;">
					<!-- Table header content , data rows & pagination for mobile-->
				</div>
				<!--<footer class="inner-gridfooter footercontainer" id="memberaddgridgridfooter">-->
					<!-- Footer & Pagination content -->
				</footer></div></div></div></div>';
			} else {
				echo '<div class="large-12 columns viewgridcolorstyle borderstyle" style="padding-left:0;padding-right:0;">
								<div class="large-12 columns" style="height:auto;padding: 0.1rem 0 0;background: #fff !important;color: #ffffff;">
									<span class="large-5 medium-6 small-5 columns mblnopadding" style="line-height:1.8">
										<span class="large-6 medium-4 small-6 columns" style="line-height:1.5;padding-top: 3px;color:black;">
											Account Type:
										</span>
										<span class="large-6 medium-8 small-6 end columns" style="position:relative;top:-5px !important;">
											<select class="chzn-select validate[required]" id="memberaccounttypeid" name="memberaccounttypeid" data-placeholder="Select" data-prompt-position="topLeft:14,36">
												<option value=""></option>';
												 foreach($accounttype as $key):?>
													<option value="<?php echo $key->accounttypeid;?>" >
													<?php echo $key->accounttypename;?></option>
												<?php endforeach;?>
									<?php	echo '</select>
										</span>
									</span>
									<span class="large-5 medium-6 small-5 columns mblnopadding" style="line-height:1.8">
										<span class="large-6 medium-4 small-6 columns" style="line-height:1.5;padding-top: 3px;color:black;">
											Account Group:
										</span>
										<span class="large-6 medium-8 small-6 end columns" style="position:relative;top:-5px !important;">
											<select class="chzn-select validate[required]" id="accountgroupid" name="accountgroupid" data-placeholder="Select" data-prompt-position="topLeft:14,36">
												<option value=""></option>
											</select>
										</span>
									</span>
									<input type="hidden" id="membergroupsortcolumn" name="membergroupsortcolumn" value=""/>
									<input type="hidden" id="membergroupsortorder" name="membergroupsortorder" value=""/>
								</div>';
				echo '<div class="large-12 columns forgetinggridname viewgridcolorstyle" id="accountgroupgridwidth" style="box-shadow:none;padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="memberaddgrid" style="max-width:2000px; height:78vh !important;top:0px;">
				<!-- Table content[Header & Record Rows] for desktop-->
				</div>
				<div class="footer-content footercontainer" id="memberaddgridgridfooter">
					 <!--Footer & Pagination content-->
				</div></div></div>';
			}
		?>	
 	</div>
</div>