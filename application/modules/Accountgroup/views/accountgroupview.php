<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('Base/headerfiles'); ?>
</head>
<body>
	<div class="row" style="max-width:100%">
		<div class="off-canvas-wrap" data-offcanvas>
			<div class="inner-wrap">
				<!-- Add Form -->
				<div id="accountgroupaddformdiv" class="">
					<?php $this->load->view('accountgroupform'); ?>
				</div>
				<?php
				$device = $this->Basefunctions->deviceinfo();
				if($device=='phone') {
					$this->load->view('Base/overlaymobile');
				} else {
					$this->load->view('Base/overlay');
				}
				$this->load->view('Base/modulelist');
				$defaultrecordview = $this->Basefunctions->get_company_settings('mainviewdefaultview');
				echo hidden('mainviewdefaultview',$defaultrecordview);
		        ?>
				<!--View Selection and Delete Overlay-->			
					<?php  $this->load->view('Base/basedeleteform');?>		
			</div>
		</div>
	</div>
</body>
<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js Files -->
	<script src="<?php echo base_url();?>js/Accountgroup/accountgroup.js" type="text/javascript"></script>
</html>
