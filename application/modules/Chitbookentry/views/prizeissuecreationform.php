<style type="text/css">
	#prizeissuegrid .gridcontent{
		height: 63vh !important;
	}
	#prizeissuegridfooter .rppdropdown  {
		left:62% !important;
	}
	#groupcloseaddform {
		left:-20px !important;
		position:relative;
		font-size: 0.9rem;
    text-transform: uppercase;
    display: inline-block;
    font-family: 'Nunito Semibold', sans-serif;
    background-color: #E57276;
    color: #fff;
    padding-left: 10px;
    padding-right: 10px;
    height: 30px;
    box-sizing: border-box;
    position: relative;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    cursor: pointer;
    outline: 0;
    border: none;
    -webkit-tap-highlight-color: transparent;
    display: inline-block;
    white-space: nowrap;
    text-decoration: none;
    vertical-align: baseline;
    text-align: center;
    margin: 0;
    min-width: 64px;
    line-height: 19px;
    padding: 0 16px;
    border-radius: 4px;
    overflow: visible;
    transform: translate3d(0,0,0);
    transition: background .4s cubic-bezier(.25,.8,.25,1),box-shadow 280ms cubic-bezier(.4,0,.2,1);
	}
	#chitbookgridfooter .rppdropdown {
	left:62% !important;	
	}
</style>
<?php
				$device = $this->Basefunctions->deviceinfo();
				$dataset['gridtitle'] = 'Prize Issue Creation';
				$dataset['titleicon'] = 'add_shopping_cart';
				$modtabgroup[0] = array("tabpgrpid"=>"1","tabgrpname"=>"Prize Issue Creation","moduleid"=>"51","templateid"=>"2");
				$dataset['modtabgrp'] = $modtabgroup;
				$dataset['moduleid'] = '51';
				$dataset['otype'] = 'overlay';
				$dataset['action'][0] = array('actionid'=>'groupcloseaddform','actiontitle'=>'Close','actionmore'=>'No','ulname'=>'prizeissue');
				$this->load->view('Base/formwithgridmenuheader.php',$dataset);
?>
<div class="" id="prizeissue" style="">		
		<div class="large-12 columns addformunderheader">&nbsp;</div>
		<div class="large-12 columns scrollbarclass addformcontainer">
		<div class="large-12 medium-12 small-12 large-centered medium-centered small-centered columns paddingzero">
			<div class="large-12 column paddingzero">
				<?php if($device=='phone') {?>
				<div class="closed effectbox-overlay effectbox-default" id="prizeissuesectionoverlay">
				<?php }?> 
				<div class="row mblhidedisplay">&nbsp;</div>
				<form id="prizeissueform" name="prizeissueform" class="cleardataform">
				<div id="prizeissuevalidate" class="validationEngineContainer">
					<div class="large-4 columns end paddingbtm">
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff">
							<?php if($device=='phone') {?>
							<div class="large-12 columns sectionheaderformcaptionstyle">Prize Issue Details</div>
							<div class="large-12 columns sectionpanel">
								<?php  } else { ?>
									<div class="large-12 columns headerformcaptionstyle">Prize Issue Details</div>
							<?php  	} ?> 
							<div class="static-field large-6 columns ">
						    <label>Print Template<span class="mandatoryfildclass">*</span></label>			
							<select id="printtemplateissue" data-tags="true" name="printtemplateissue" tabindex="101"  class="chzn-select validate[required]" aria-hidden="true" data-prompt-position="bottomLeft:14,36" >
									<option value="YES">YES</option>
									<option value="NO">NO</option>
							</select> 
							</div>
							<div class="input-field large-6 columns ">				
							<input id="prizeissuedate" class="fordatepicicon validate[required]"  type="text" data-dateformater="dd-mm-yy" data-prompt-position="bottomLeft" readonly="readonly" tabindex="102" name="prizeissuedate" >
							<label for="prizeissuedate">Date<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="input-field large-12 columns">
								<input type="text" id="issuechitbookno" name="issuechitbookno" class="validate[required]" data-prompt-position="bottomLeft" tabindex="103">
							<label for="issuechitbookno">Chitbookno<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="input-field large-12 columns">
								<input type="text" id="issueschemename" name="issueschemename" class="" readonly tabindex="104">
								<label for="issueschemename">Scheme Name</label>
							</div>
							<div class="input-field large-6 columns">
								<input type="text" id="chitbookamount" name="chitbookamount" value="" class=""/ readonly tabindex="105">
								<label for="chitbookamount">Amount</label>
							</div>
							<div class="static-field large-6 columns ">
								<label>Type<span class="mandatoryfildclass">*</span></label>								
								<select id="typegift" name="typegift" class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" data-placeholder="select" tabindex="106">
								<option value="">Select</option>
								<?php foreach($gifttypes as $key):?>
									<option value="<?php echo $key->chitlookupsid;?>">
										<?php echo $key->chitlookupsname;?></option>
								<?php endforeach;?>		
								</select>
							</div>
							<div class="static-field large-12 columns ">
								<label>Gift And Prize<span class="mandatoryfildclass">*</span></label>								
								<select id="giftprize" name="giftprize" data-issuetype="" class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" data-placeholder="select" tabindex="107">
								</select>
							</div>
							<div class="static-field large-12 columns">
									<label for="issuecomment">Comment</label>
									<input type="text" id="issuecomment" name="issuecomment" value="" class="" data-prompt-position="bottomLeft:14,36" tabindex="108">
							</div>
							<?php if($device=='phone') { ?>
							</div>
							<div class="divider large-12 columns"></div>
							<div class="large-12 columns submitkeyboard sectionalertbuttonarea" style="text-align: right">
								<input id="prizeissuebtn" class="alertbtn addkeyboard" type="button" value="Submit" name="prizeissuebtn" tabindex="180">
								<input id="prizeissuesectionclose"class="alertbtn addsectionclose cancelkeyboard" type="button" value="Cancel" name="cancel" tabindex="181">
								<input type="hidden" name="issueschemeid" id="issueschemeid">
								<input type ="hidden" id="prizeissuesortcolumn" name="prizeissuesortcolumn"/>
								<input type ="hidden" id="prizeissuesortorder" name="prizeissuesortorder"/>
							</div>
								
							<?php } else { ?> 
							<div class="large-12 columns" style="text-align: right">
								<label> </label>
								<input type="hidden" name="issueschemeid" id="issueschemeid">
								<input type ="hidden" id="prizeissuesortcolumn" name="prizeissuesortcolumn"/>
								<input type ="hidden" id="prizeissuesortorder" name="prizeissuesortorder"/>
								<input id="prizeissuebtn" class="btn" type="button" value="Submit" name="" tabindex="109">
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
				</form>
				<?php if($device=='phone') {?>
				</div>
				<?php }?>
				<div class="large-8 columns paddingbtm">
					<div class="large-12 columns paddingzero">
						<div class="large-12 columns viewgridcolorstyle paddingzero borderstyle" style="top: 8px;">
							<div class="large-12 columns headerformcaptionstyle" style="height:auto;padding: 0.2rem 0 0rem;">
								<span class="large-6 medium-6 small-6 columns text-left" >Prize Issue List</span>
							   <span class="large-6 medium-6 small-6 columns innergridicon righttext">
										<?php if($device=='phone') {?>
										<span id="prizeissueaddicon" class="icon-box" title="Add" ><i class="material-icons">add</i></span>
										<?php }?> 
										<span class="icon-box" title="Delete" id="prizeissuedeleteicon"><i class="material-icons">delete</i></span>
										<span class="icon-box" title="Print" id="issueprinticon"><i class="material-icons">print</i> </span>
									</span>
							</div>
							<?php
						$device = $this->Basefunctions->deviceinfo();
						if($device=='phone') {
							echo '<div class="large-12 columns paddingzero forgetinggridname" id="prizeissuegridwidth"><div class=" inner-row-content inner-gridcontent" id="prizeissuegrid" style="height:70vh !important;top:0px;">
								<!-- Table header content , data rows & pagination for mobile-->
							</div>
							<footer class="inner-gridfooter footercontainer" id="prizeissuegridfooter">
								<!-- Footer & Pagination content -->
							</footer></div>';
						} else {
							echo '<div class="large-12 columns forgetinggridname" id="prizeissuegridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="prizeissuegrid" style="max-width:2000px; height:70vh !important;top:0px;">
							<!-- Table content[Header & Record Rows] for desktop-->
							</div>
							<div class="inner-gridfooter footer-content footercontainer" id="prizeissuegridfooter">
								<!-- Footer & Pagination content -->
							</div></div>';
						}
				?>
						</div>
					</div>
				</div>
			</div>
		</div>	
		</div>	
	</div>