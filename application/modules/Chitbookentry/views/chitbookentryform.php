<?php
	$device = $this->Basefunctions->deviceinfo();
	$dataset['gridtitle'] = $gridtitle;
	$dataset['titleicon'] = $titleicon;
	$this->load->view('Base/mainformwithgridmenuheader.php',$dataset);
	$defaultrecordview = $this->Basefunctions->get_company_settings('mainviewdefaultview');
	$defaultbankid = $this->Basefunctions->get_company_settings('defaultbankid');
	$defaultcashinhandid = $this->Basefunctions->get_company_settings('defaultcashinhandid');
	echo hidden('mainviewdefaultview',$defaultrecordview);
?>
<input type="hidden" id="chitrestrictsamemonth" name="chitrestrictsamemonth" value="<?php echo $chitrestrictsamemonth; ?>">
<input type="hidden" id="chituserroleauth" name="chituserroleauth" value="<?php echo $chituserroleauth; ?>">
<input type="hidden" id="chitcompletealert" name="chitcompletealert" value="<?php echo $chitcompletealert; ?>">
<input type="hidden" id="defaultbankid" name="defaultbankid" value="<?php echo $defaultbankid; ?>">
<input type="hidden" id="defaultcashinhandid" name="defaultcashinhandid" value="<?php echo $defaultcashinhandid; ?>">
<input type="hidden" id="chitbookbillamount" name="chitbookbillamount" value="">
<div class="large-12 columns addformunderheader">&nbsp; </div>
<div class="large-12 columns addformcontainer">
	<div class="row">&nbsp;</div>
	<div id="subformspan1" class="hiddensubform"> 
		<form method="POST" name="chitbookentryform" class="chitbookentryform"  id="chitbookentryform">
			<div id="chitbookentryaddwizard" class="validationEngineContainer">				
			<div id="chitbookentryeditwizard" class="validationEngineContainer">
			<?php if($device=='phone') {
				?>			
				<div class="large-4 columns">	
					<div class="large-12 columns cleardataform" style="padding:0px !important;box-shadow:none;background: #f2f3fa !important;">
						<div class="large-12 columns headerformcaptionstyle" style="background: #f2f3fa !important;">Header Details</div>
			<?php  } else { ?>
				<div class="large-4 columns paddingbtm">	
					<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
						<div class="large-12 columns headerformcaptionstyle" >Header Details</div>
	
			<?php } ?>
						<div class="static-field large-6 columns ">
							<label>Date<span class="mandatoryfildclass">*</span></label>								
							<input id="chitbookentrydate" class="fordatepicicon validate[required]"  type="text" data-dateformater="dd-mm-yy" data-prompt-position="topLeft" tabindex="101" name="chitbookentrydate" >
						</div>
						<div class="static-field large-6 columns ">
							<label>Reference Date<span class="mandatoryfildclass">*</span></label>								
							<input id="chitreferencedate" class="fordatepicicon validate[required]"  type="text" data-dateformater="dd-mm-yy" data-prompt-position="topLeft" tabindex="102" name="chitreferencedate" >
						</div>
						<div class="static-field large-6 columns">
							<label>Employee Name</label>
							<input type="text" class="" id="employeename" name="employeename" value="" tabindex="102" readonly>
							<input type="hidden" class="" id="employeeid" name="employeeid" value="" tabindex="" >
						</div>
						<div class="input-field large-6 columns hidedisplay">
							<input type="text" class="validate[required,min[1],custom[number]] chitbookamountcalc" id="noofmonthsentry" name="noofmonthsentry" value="" tabindex="103" data-calc="month" readonly>
							<label for="noofmonthsentry">Months<span class="mandatoryfildclass">*</span></label>
						</div>
						<div class="input-field large-6 columns changedate classchitbookno">
							<input type="text" class="validate[required] chitbookamountcalc chitratecalc" id="chitbookno" name="chitbookno" value="" tabindex="104" data-calc="chitbookno" >
							<label for="chitbookno">Chit Bookno<span class="mandatoryfildclass">*</span></label>
						</div>
						<div class="input-field large-6 columns">
							<input type="button" class="btn leftformsbtn fastsubmitchitentrybtn" id="chitentrysubmit" name="chitentrysubmit" value="SUBMIT" tabindex="105">
						</div>
						<div class="static-field large-12 columns  otherchit  changedate hidedisplay">
							<label>Other Chitbook Nos <span class="mandatoryfildclass">*</span></label>
							<select id="otherchitbookno" data-tags="true" name="otherchitbookno[]" data-chitbookid="" tabindex="106"  class="chzn-select chitbookamountcalc chitratecalc" aria-hidden="true" data-prompt-position="topLeft:14,36" multiple data-calc="otherchitbookno">
									<option value=""></option>
							</select> 
						</div> 
						<div class="input-field large-6 columns changedate">
							<input type="text" class="" id="ratedetails" name="ratedetails" value="" tabindex="107" readonly>
							<label for="ratedetails">Rate Details</label>	
						</div>
						<div class="static-field large-6 columns ">
						    <label>Print Template<span class="mandatoryfildclass">*</span></label>			
							<select id="printtemplate" data-tags="true" name="printtemplate" tabindex="108"  class="chzn-select validate[required]" aria-hidden="true" data-prompt-position="topLeft:14,36" >
									<option value="YES">YES</option>
									<option value="NO">NO</option>
							</select> 
						</div>
						<div class="input-field large-12 columns ">
							<textarea name="comments" class="materialize-textarea validate[maxSize[50]]" id="comments" rows="3" cols="40"></textarea>
							<label for="comments">Comments</label>	
						</div>
						<div class="large-12 columns "> &nbsp; </div>
					</div>
				</div>	
				<?php
				if($device=='phone') {
				?>	
				<div class="large-4 columns paddingbtm changedate">	
					<div class="large-12 columns cleardataform" style="padding:0px !important;box-shadow:none;background: #f2f3fa !important;">
						<div class="large-12 columns headerformcaptionstyle" style="background: #f2f3fa !important;">Payment Details</div>
						
				<?php } else { ?>
						<div class="large-4 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;top: 15px !important;">
						<div class="large-12 columns headerformcaptionstyle" >Payment Details</div>
	
				<?php } ?>
						
						<form method="POST" name="chitentryinnergridform" id="chitentryinnergridform" class="chitentryinnergridform">
							<div id="chitentryinnergridform" class="validationEngineContainer">
								<div id="stocktypeid-div" class="static-field large-6 medium-6 small-6 columns">
									<label>Type<span class="mandatoryfildclass" id="stocktypeid_req">*</span></label>
									<select id="stocktypeid" name="stocktypeid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="102">									
										<option value=""></option>
										<?php 
											foreach($stocktype->result() as $key):
											?>
										<option value="<?php echo $key->stocktypeid;?>" data-label="<?php echo $key->stocktypelabel;?>" data-stocktypenamehidden="<?php echo $key->stocktypename;?>" ><?php echo $key->stocktypename;?></option>
											<?php  endforeach;?>	
									</select>
								</div>
								<div id="paymentaccountid-div" class="static-field large-6 medium-6 small-6 columns">
									<label>Payment Account<span class="mandatoryfildclass" id="paymentaccountid_req">*</span></label>
									<select id="paymentaccountid" name="paymentaccountid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="102">									
									<option value=""></option>
									<?php 
										foreach($paymentaccount as $key):
											$paymentaccountvalue = $key->accountname;
									?>												
									<option value="<?php echo $key->accountid;?>" data-accountcatalogid="<?php echo $key->accountcatalogid;?>" data-paymentaccountnamehidden="<?php echo $paymentaccountvalue;?>"  data-accounttypeid="<?php echo $key->accounttypeid;?>"><?php echo $paymentaccountvalue;?></option>
									<?php  endforeach;?>	
								</select>
								</div>
								<div class="static-field large-6 medium-3 small-6 columns clearonpaymentstocktypechange" id="cardtypeid-div" >
									<label>Card Type<span class="mandatoryfildclass" id="cardtype_req"></span> </label>
									<select id="cardtypeid" name="cardtypeid" class="chzn-select" data-prompt-position="bottomLeft:14,36" data-validation-engine="" tabindex="128"> 
										<option value=""></option>
										<?php foreach($cardtype as $key):?>
										<option value="<?php echo $key->cardtypeid;?>" data-cardgroupid="<?php echo $key->cardgroupid;?>" data-cardtypehidden="<?php echo $key->cardtypename;?>" data-label="<?php echo $key->cardtypename;?>" data-cardtypename="<?php echo $key->cardtypename;?>">
											<?php echo $key->cardtypename;?></option>
										<?php endforeach;?>	 
									</select>
								</div>
								<div class="static-field large-6 medium-3 small-6 columns clearonpaymentstocktypechange" id="bankname-div">	
									<input type="text" class="" id="bankname" name="bankname" value="" tabindex="127" data-prompt-position="bottomLeft:14,36" data-validation-engine="" maxlength="50">
									<label for="bankname">Bank<span class="mandatoryfildclass" id="bankname_req"></span> </label>
								</div>
								 <div class="input-field large-6 medium-3 small-6 columns clearonpaymentstocktypechange" id="referenceno-div" >	
									<input type="text" class="" id="referenceno" name="referenceno" value="" tabindex="124" data-validation-engine="" maxlength="50">
									<label for="referenceno">Reference Number<span class="mandatoryfildclass" id="referenceno_req"></span> </label>
								</div>
								<div class="input-field large-6 medium-3 small-6 columns clearonpaymentstocktypechange" id="referencedate-div" >
									<input id="referencedate" class="fordatepicicon" type="text" name="referencedate" tabindex="125" readonly="readonly" data-prompt-position="bottomLeft" data-dateformater="dd-mm-yy">
									<label for="referencedate">Date<span class="mandatoryfildclass" id="referencedate_req"></span> </label>
							   </div>
							   <div class="input-field large-6 medium-3 small-6 columns clearonpaymentstocktypechange" id="approvalcode-div" >	
									<input type="text" class="" id="approvalcode" name="approvalcode" value="" tabindex="126" data-validation-engine="" maxlength="50">
									<label for="approvalcode">Appr Code<span class="mandatoryfildclass" id="approvalcode_req"></span> </label>
								</div>
								<div class="input-field large-6 medium-3 small-6 columns clearonpaymentstocktypechange"  id="paymentamount-div" >
									<input type="text" class="" keyword="" id="paymentamount" name="paymentamount" data-validation-engine="validate[validate[required,custom[number],min[1],maxSize[15]],decval[2]]" tabindex="133" value="0">
									<label for="paymentamount">Amount<span class="mandatoryfildclass" id="paymentamount_req"></span></label>
								</div>
								<input type="hidden" name="stocktypename" id="stocktypename" value="" >
								<input type="hidden" name="accountname" id="accountname" value="" >
								<input type="hidden" name="cardtypename" id="cardtypename" value="" >
							</div>
						</form>
						<?php
					if($device=='phone') {
					?>	
										
					<div class="large-12 columns " style="text-align:center">
							<label>&nbsp;</label>						
							<input type="button" class="btn leftformsbtn" id="chitentrydetailsubmit" name="chitentrydetailsubmit" value="SUBMIT" tabindex="116">
							<input type = "hidden" id = "schemeid" name="schemeid"/>
							<input type = "hidden" id = "chitlookupid" name="chitlookupid"/>
							<input type = "hidden" id = "bothschemeamount" name="bothschemeamount"/>
						    <input type = "hidden" id = "schemeprefix" name="schemeprefix"/>
						    <input type = "hidden" id = "schemechittype" name="schemechittype"/>
						    <input type = "hidden" id = "accountgroupid" name="accountgroupid"/>
						    <input type = "hidden" id = "primaryid" name="primaryid"/>
							
					</div>		
						
					<?php } else { ?>
						<div class="large-12 columns " style="text-align:right">
							<label>&nbsp;</label>						
							<input type="button" class="btn leftformsbtn" id="chitentrydetailsubmit" name="chitentrydetailsubmit" value="SUBMIT" tabindex="116">
							<input type = "hidden" id = "schemeid" name="schemeid"/>
							<input type = "hidden" id = "chitlookupid" name="chitlookupid"/>
							<input type = "hidden" id = "bothschemeamount" name="bothschemeamount"/>
						    <input type = "hidden" id = "schemeprefix" name="schemeprefix"/>
						    <input type = "hidden" id = "schemechittype" name="schemechittype"/>
						    <input type = "hidden" id = "accountgroupid" name="accountgroupid"/>
						    <input type = "hidden" id = "primaryid" name="primaryid"/>
							
						</div>	

				<?php } ?>	
					</div>
				</div>	
				<?php
			if($device=='phone') {
			?>	
				<div class="large-4 columns paddingbtm changedate">	
					<div class="large-12 columns cleardataform" style="padding:0px !important;box-shadow:none;background: #f2f3fa !important;">
						<div class="large-12 columns headerformcaptionstyle" style="background: #f2f3fa !important;">Chit History</div>
						
			<?php } else { ?>
			<div class="large-4 columns paddingbtm changedate">	
				<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
						<div class="large-12 columns headerformcaptionstyle" >Chit History</div>
			<?php } ?>
						<div class="input-field large-12 columns hidedisplay">		
							<input type="text" class="" id="accountname" name="accountname" value="" tabindex="117" readonly>
							<label for="accountname">Account Name<span class="mandatoryfildclass">*</span></label>
							<input type="hidden" class="" id="accountid" name="accountid" value="">
						</div>
						<div class="input-field large-12 columns">
						<input type="text" class="" id="chitbookname" name="chitbookname" value="" tabindex="117" readonly>
						<label for="chitbookname">Chitbook Name</label>
						</div>
						<div class="input-field large-6 columns">
							<input type="text" class="" id="currentamount" name="currentamount" value="" tabindex="118" readonly>
							<label for="currentamount">Current Amount</label>
						</div>
						<div class="input-field large-6 columns">
							<input type="text" class="" id="totalamount" name="totalamount" value="" tabindex="119" readonly>
							<label for="totalamount">Total Amount</label>	
						</div>
						
						<div class="input-field large-6 columns" id="currentmonthdiv">
							<input type="text" class="" id="currentmonth" name="currentmonth" value="" tabindex="118" readonly>
							<label for="currentmonth">Current Month</label>
						</div>
						<div class="input-field large-6 columns">
							<input type="text" class="" id="lastpaymentdate" name="lastpaymentdate" value="" tabindex="119" readonly>
							<label for="totalamount">Last Payment Date</label>	
						</div>	
						<div class="input-field large-12 columns ">
							<textarea name="prizehistorydetails" class="materialize-textarea" id="prizehistorydetails" rows="3" cols="40" readonly></textarea>
							<label for="comments">Prize History Details</label>	
						</div>
						<div class="large-12 columns "> &nbsp; </div>
					</div>
				</div>	
			</div>	
			</div>	
			<?php
				//hidden text box view columns field module ids
				$modid = $this->Basefunctions->getmoduleid($filedmodids);
				echo hidden('viewfieldids',$modid);
			?>
		</form>
		<div class="large-12 columns">&nbsp; </div>
		<div id="chitbookentryinnergriddiv" class="large-12 columns">	
			<div class="large-12 columns viewgridcolorstyle borderstyle" style="padding-left:0;padding-right:0;">
				<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0rem 0rem;line-height:1.8rem;height:auto;">	
					<div class="large-12 columns paddingzero">
						<div class="large-6  medium-6 small-6 columns lefttext">Payment Details</div>
						<div class="large-6 medium-6 small-6 columns innergridicons righttext" >
							<span class="" title="Delete" id="chitbookentrydetaildelete"><i class="material-icons">delete</i></span>
						</div>
					</div>
				</div>
				<div class="large-12 columns paddingzero">
				<?php
					$device = $this->Basefunctions->deviceinfo();
					if($device=='phone') {
						echo '<div class="large-12 columns paddingzero forgetinggridname" id="chitbookentryinnergridwidth"><div class=" inner-row-content inner-gridcontent" id="chitbookentryinnergrid" style="height:420px;top:0px;">
							<!-- Table header content , data rows & pagination for mobile-->
						</div>
						<footer class="inner-gridfooter footercontainer" style="height:0px !important;" id="chitbookentryinnergridfooter">
							<!-- Footer & Pagination content -->
						</footer></div>';
					} else {
						echo '<div class="large-12 columns forgetinggridname" id="chitbookentryinnergridwidth" style="padding-left:0;padding-right:0;">
						<div class="desktop row-content inner-gridcontent" id="chitbookentryinnergrid" style="max-width:2000px; height:240px;top:0px;">
						<!-- Table content[Header & Record Rows] for desktop-->
						</div>
						<div class="footer-content footercontainer"  style="height:0px !important;" id="chitbookentryinnergridfooter">
								<!-- Footer Previous / Next page -->
							</div>
						</div>';
					}
				?>	
				</div>
			</div>
			<div class="large-12 columns ">&nbsp;</div>
		</div>
	</div>
</div>

<!-- Base Cancel overlay for combined modules -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="basecanceloverlayforcommodule">
		<div class="row sectiontoppadding">&nbsp;</div>
		<div class="overlaybackground">
			<div class="alert-panel">
				<div class="alertmessagearea cancelmessagearea">
				<div class="row">&nbsp;</div>
				<div class="alert-message basedelalerttxt">Are you sure,you want to cancel this bill?</div>
				</div>
				<div class="alertbuttonarea">
					<span class="firsttab" tabindex="1000"></span>
					<input type="button" id="basecancelyes" name="" value="Cancel" tabindex="1001" class="alertbtnyes ffield commodyescls">
					<input type="button" id="basecancelno" name="" tabindex="1002" value="Cancel" class="alertbtnno flloop  alertsoverlaybtn" >
					<span class="lasttab" tabindex="1003"></span>
				</div>
				<div class="row">&nbsp;</div>
			</div>
		</div>
	</div>
</div>