<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('Base/headerfiles'); ?>
	<?php $this->load->view('Base/basedeleteform'); ?>
	<?php 
		$industryid = $this->Basefunctions->industryid;
		if($industryid == 3) {		
	?>
	<link href="<?php echo base_url();?>css/jqgrid.min.css" media="screen" rel="stylesheet" type="text/css" />
	<?php 
	 }
	?>
</head>
<body>
	<?php
		$device = $this->Basefunctions->deviceinfo();
		$dataset['gridenable'] = "yes"; //no
		$dataset['griddisplayid'] = "chitbookentrygriddisplay";
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['spanattr'] = array();
		$dataset['gridtableid'] = "chitbookentrygrid";
		$dataset['griddivid'] = "chitbookentrygriddiv";
		$dataset['moduleid'] = $moduleids;
		$dataset['forminfo'] = array(array('id'=>'chitbookentryformdiv','class'=>'hidedisplay','formname'=>'chitbookentryform'),array('id'=>'prizeissuecreationform','class'=>'hidedisplay','formname'=>'prizeissuecreationform'));
		if($device=='phone') {
			$this->load->view('Base/gridmenuheadermobile',$dataset);
			$this->load->view('Base/overlaymobile');
			$this->load->view('Base/viewselectionoverlay');
		} else {
			$this->load->view('Base/gridmenuheader',$dataset);
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
		$this->load->view('Base/basedeleteform');
	?>
	<!--View Creation Overlay-->
				<div class="hidedisplay" id="viewcreationformdiv">
				</div>
	<?php
		$this->load->view('Base/basedeleteformforcombainedmodules');
	?>	
	
	
</body>
<?php $this->load->view('Base/bottomscript'); ?>
<?php include 'js/Chitbookentry/chitbookentry.php' ?>	
	<script src="<?php echo base_url();?>js/plugins/filecomputer/fileupload.js" type="text/javascript"></script>
</html>
