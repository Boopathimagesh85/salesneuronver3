<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Chitbookentry extends MX_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Chitbookentry/Chitbookentrymodel');
		$this->load->model('Sales/Salesmodel');
    }
	//first basic hitting view
    public function index() {	
    	//$this->Chitbookentrymodel->allchitentrytotalweightupdate(1,100);
		//die();
		$moduleid = array(51);
		sessionchecker($moduleid);
		$userid = $this->Basefunctions->userid;
		//action		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['chitrestrictsamemonth'] = $this->Basefunctions->get_company_settings('chitrestrictsamemonth'); 
		$data['chituserroleauth'] = $this->Basefunctions->get_company_settings('chituserroleauth');
		$data['chitcompletealert'] = $this->Basefunctions->get_company_settings('chitcompletealert');
		//view
		$data['gifttypes']=$this->Basefunctions->simpledropdownlookup('chitlookups','chitlookupsid,chittypegroup,chitlookupsname','chitlookupsid',2);
		$data['stocktype'] = $this->Chitbookentrymodel->stocktypedropdown();
		$data['paymentaccount'] = $this->Salesmodel->paymentaccountnamefetch('accountid','accountname,accountcatalogid,accounttypeid,mobilenumber','account','accounttypeid','18,19');
		$data['cardtype'] = $this->Basefunctions->simpledropdownwithcond('cardtypeid','cardtypename,cardgroupid','cardtype','moduleid','52');
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$this->load->view('Chitbookentry/chitbookentryview',$data);
    }
	// get chitbook no,account name and group
	public function getchitbookdetails() {
		$this->Chitbookentrymodel->getchitbookdetails();
	}
	// get chit book amt based on month entry
	public function getcurrentamount() {
		$this->Chitbookentrymodel->getcurrentamount();
	}
	// get chit book rate
	public function getratedetails() {
		$this->Chitbookentrymodel->getratedetails();
	}
	// chit book entry create_function
	public function chitbookentrycreate() {
		$this->Chitbookentrymodel->chitbookentrycreate();
	}
	// chit book entry delete_function
	public function chitbookentrydelete() {
		$this->Chitbookentrymodel->chitbookentrydelete();
	}
	public function chitbookentryretrive() {
		$this->Chitbookentrymodel->chitbookentryretrive();
	}
	public function getschemedetails() {
		$this->Chitbookentrymodel->getschemedetails();
	}
	public function getprizedetails() {
		$this->Chitbookentrymodel->getprizedetails();
	}
	public function checkprizeissue() {
		$this->Chitbookentrymodel->checkprizeissue();
	}
	public function prizeissuecreate() {
		$this->Chitbookentrymodel->prizeissuecreate();
	}
	public function prizedescription()
	{
		$this->Chitbookentrymodel->prizedescription();
	}
	// chit book entry delete_function
	public function prizeissuedelete()
	{
		$this->Chitbookentrymodel->prizeissuedelete();
	}
	public function chitbookdata() {
		$this->Chitbookentrymodel->chitbookdata();
	}
	public function chitentrygroupprint() {
		$this->Chitbookentrymodel->chitentrygroupprint();
	}
	public function chitentryprint() {
		$this->Chitbookentrymodel->chitentryprint();
	}
	public function issuereprint() {
		$this->Chitbookentrymodel->issuereprint();
	}
	public function doublecheck() {
		$this->Chitbookentrymodel->doublecheck();
	}
	public function chitentrycheck() {
		$this->Chitbookentrymodel->chitentrycheck();
	}
	// Chitentry Re-print icon action*/
	public function chitentryreprint() {
		$storageid = trim($_GET['primaryid']);	
		$templateid = trim($_GET['templateid']);
		if($templateid > 1) {
			$print_data=array('templateid'=> $templateid, 'Templatetype'=>'Tagtemp','primaryset'=>'counter.counterid','primaryid'=>$storageid);
			$data = $this->Printtemplatesmodel->generatlabelprint($print_data);
			echo $data;
		} else {
			echo 'Kindly Edit the Print template of the tag in edit Screen.Since u have selected any print option earlier.';
			exit();
		}
	}
	// chit book entry cancel_function
	public function chitbookentrycancel() {
		$this->Chitbookentrymodel->chitbookentrycancel();
	}
	// work flow condition header information fetch
	public function chitbookentryinnergridheaderfetch() {
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$modname = $_GET['modulename'];
		$colinfo = $this->Chitbookentrymodel->chitbookentryinnergridheaderfetch($modid);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height);
		} else {
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height);
		}
		echo json_encode($datas);
	}
}