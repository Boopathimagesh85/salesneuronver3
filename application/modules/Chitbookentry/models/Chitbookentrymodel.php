<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Chitbookentrymodel extends CI_Model {
   	private $chitbookentrymoduleid = 51;	
	public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
		$this->load->model('Printtemplates/Printtemplatesmodel');	
    }
	public function getchitbookdetails() {
		$data=array();
		$chitbooknoarray=array();
		$totalavgamount = 0;
		$chitbookno=$_GET['primaryid']; 
		$chitrestrictsamemonth=$_GET['chitrestrictsamemonth'];
		$chituserroleauth=$_GET['chituserroleauth'];
		$chitbookentrydate=$this->Basefunctions->ymd_format($_GET['chitbookentrydate']); 
		$scanchitbook = 'yes'; 		
		$transdate = date('Y-m', time());
		if($chitrestrictsamemonth == 1) {
			$userroleid = $this->Basefunctions->userroleid;
			$chituserroleauth = explode(',',$chituserroleauth);
			$info =$this->db->query('select `chitentrydate` from `chitentry` where DATE_FORMAT(chitentrydate, "%Y-%m") = "'.$transdate.'" and chitbookno = "'.$chitbookno.'" and status = 1');
			if($info->num_rows() > 0){
				if( in_array($userroleid,$chituserroleauth) ) {
					$scanchitbook = 'yes';
				}else{
					$scanchitbook = 'No';	
				}
			}
		}
		if($scanchitbook == 'yes') {
			$chitbook_closed = '';
			$chitbook_status = '';
			$chitratedetails = '';
			$ichit = 0;
			$this->db->select('chitbook.status,chitbook.chitclosed,chitbook.chitschemeid,chitbook.accountid,chitbook.chitbookname,chitscheme.schemeprefix,chitscheme.chitlookupsid,chitbook.noofmonths,chitbook.amount,chitscheme.purityid,purity.purityname,chitbook.accountid,chitscheme.variablemodeid,chitscheme.averagemonths',false);
			$this->db->from('chitbook');
			$this->db->join('chitscheme','chitscheme.chitschemeid = chitbook.chitschemeid');
			$this->db->join('purity','purity.purityid = chitscheme.purityid');
			$this->db->where_in('chitbook.status',$this->Basefunctions->chitbookstatus);
			$this->db->where('chitbook.chitbookno',$chitbookno);
			$chittypedetails=$this->db->get();
			foreach($chittypedetails->result() as $chittypedetail) {
				$ichit++;
				$chitbook_closed=$chittypedetail->chitclosed;
				$chitbook_status=$chittypedetail->status;
				if($chitbook_status == 1 ) {
					$accountid=$chittypedetail->accountid;
					$schemeid=$chittypedetail->chitschemeid;
					$schemeprefix=$chittypedetail->schemeprefix;
					$schemechittype=$chittypedetail->chitlookupsid;
					$chittotalmonths=$chittypedetail->noofmonths;
					$currentamt = $chittypedetail->amount;
					$variablemodeid=$chittypedetail->variablemodeid;
					$averagemonths=$chittypedetail->averagemonths;
					$this->db->select('max(chitentrydate) as chitentrydate,count(chitentryid) as totalmonths,sum(entryamount) as totalchitamount,sum(entryweight) as totalentryweight',false);
					$this->db->where('chitbookno',$chitbookno);
					$this->db->where('status',1);
					$info=$this->db->get('chitentry');
					$difmon = $chittotalmonths - ($info->row()->totalmonths);
					$chitentrydate = $info->row()->chitentrydate;
					if($difmon <= 0 || $chitbook_closed == 1) { // all months done
						$data=array(
							'currentamount' =>0,
							'entryweight' =>$info->row()->totalentryweight,
							'totalamount' =>$info->row()->totalchitamount,
							'currentmonth'=>'completed',
							'pendingmonths'=>$difmon,
							'lastpaymentdate'=>$chitentrydate,
							'closed' =>1
						);
					} else if($chitbookentrydate < $chitentrydate) {
						$data=array(
							'currentamount' =>0,
							'entryweight' =>$info->row()->totalentryweight,
							'totalamount' =>$info->row()->totalchitamount,
							'currentmonth'=>'dateless',
							'pendingmonths'=>$difmon,
							'lastpaymentdate'=>$chitentrydate,
							'closed' =>1
						);
					} else {
						$data=array(
							'currentamount' =>$currentamt,
							'entryweight' =>$info->row()->totalentryweight,
							'totalamount' =>$info->row()->totalchitamount,
							'currentmonth'=>$info->row()->totalmonths+1,
							'pendingmonths'=>$difmon-1,
							'lastpaymentdate'=>$chitentrydate,
							'closed' =>0		
						);
						if($chittypedetail->purityid > 1) {
							$getrate =$this->Chitbookentrymodel->getratebasedondate($chittypedetail->purityid,$chitbookentrydate); 
							foreach($getrate as $current_rate){
								$chitratedetails=$chittypedetail->purityname.'-'.$current_rate;
							} 
						}
						if($averagemonths < $info->row()->totalmonths+1) {
							$this->db->select('sum(entryamount) as totalavgamount',false);
							$this->db->where('chitbookno',$chitbookno);
							$this->db->where('status',1);
							$this->db->where('monthentry <=',$averagemonths);
							$info=$this->db->get('chitentry');
							$totalavgamount = $info->row()->totalavgamount;
						}
					}
					$prizeapplicable = $this->Basefunctions->singlefieldfetch('gift','chitschemeid','chitscheme',$chittypedetail->chitschemeid);
					if($prizeapplicable == 'YES') {
						$data['prizehistorydetails'] = $this->retrieveschemeprizedetails($chittypedetail->chitschemeid,$chitbookno);
					} else {
						$data['prizehistorydetails'] = '';
					}
					$activationdate = $this->Basefunctions->singlefieldfetch('schemeapplytodate','chitschemeid','chitscheme',$chittypedetail->chitschemeid);
					$data['activationdate'] = $this->Basefunctions->ymd_format($activationdate);
					if(date('Y-m-d') >= date('Y-m-d',strtotime($activationdate))) {
						$data['activationdatedetails'] = 'YES';
					} else {
						$data['activationdatedetails'] = 'NO';
					}
					$data['schemeprefix']= $schemeprefix;
					$data['chitlookupid']= $schemechittype;
					$data['chitbookname']= $chittypedetail->chitbookname;
					$data['chitratedetails'] = $chitratedetails;
					$data['accountid'] = $accountid;
					$data['variablemodeid'] = $variablemodeid;
					$data['averagemonths'] = $averagemonths;
					$data['totalavgamount'] = ROUND($totalavgamount,0);
				} else if($chitbook_status == 11) {
					$data='cancel'; 
				} else if($chitbook_closed == 1) { 
					$data='months completed'; 
				} else {
					$data='invalid'; 
				}
			}
			if($ichit == 0) {
				$data='invalid'; 
			}
		} else {
			$data='invalid'; 
		}
		echo json_encode($data);
	}
	// Scheme Prize details
    public function retrieveschemeprizedetails($chitschemeid,$chitbookno) {
		$prizedetails = '';
		$this->db->select('prizeentry.prizeheadername,prizeentry.prizedescription,prizeentry.tokenno');
		$this->db->from('prizeentry');
		$this->db->where_in('prizeentry.chitschemeid',$chitschemeid);
		$this->db->where('prizeentry.chitbookno',$chitbookno);
		$this->db->where('prizeentry.status',$this->Basefunctions->activestatus);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				if($prizedetails == '') {
					$prizedetails = $row->prizeheadername.'-'.$row->prizedescription;
				} else {
					$prizedetails = $prizedetails.','.$row->prizeheadername.'-'.$row->prizedescription;
				}
			}
		}
		return $prizedetails;
	}
	public function getcurrentamount() {
		$chitentryarray=array();
		$chitentrytotal=0;
		$paidchitentry = 0;
		$totalcurrentamt = 0;
		$currentamt = 0;
		$allchitentrytotal = 0;
		$chitamount = 0;
		$monthentry=$_GET['monthentry'];   
		$chitamtdenom = 0;
		$chitbookno=$_GET['chitbookno'];
		$otherchitbookid=$_GET['otherchitbookid'];
		//echo $otherchitbookid;
		if($otherchitbookid != '' && $otherchitbookid != 0) {
		//echo "inside";
			$allchitbookno = $otherchitbookid;
			$allchitbooknocount = count($allchitbookno);
		//	echo $allchitbooknocount;
			$allchitbookno[$allchitbooknocount] = $chitbookno;
		} else {
			$allchitbooknocount = 1;
			$allchitbookno[1] = $chitbookno;
		}
	//	echo $allchitbooknocount ;
	//	die();
		for($i=1; $i <= $allchitbooknocount; $i++) {
			if(isset($allchitbookno[$i])) {
				if($allchitbookno[$i] != '') {
					$chitamount = $this->Basefunctions->singlefieldfetch('chitamount','chitbookno','chitentry',$allchitbookno[$i]);
					$chitschemeid = $this->db->where('chitbookno',$allchitbookno[$i])->get('chitbook')->row()->chitschemeid;
					$chitamtdenom = $this->db->where('chitbookno',$allchitbookno[$i])->get('chitbook')->row()->amount;
					$currentamt = $chitamtdenom ;
					$chittotalmonths = $this->db->join('chitscheme','chitscheme.chitschemeid = chitbook.chitschemeid')->where('chitbookno',$allchitbookno[$i])->get('chitbook')->row()->noofmonths; 
					$chitlookupsid = $this->db->join('chitscheme','chitscheme.chitschemeid = chitbook.chitschemeid')->where('chitbookno',$allchitbookno[$i])->get('chitbook')->row()->chitlookupsid;
				    $chitentrytotalamountrows=$this->db->where('chitbookno',$allchitbookno[$i])->where('status',$this->Basefunctions->activestatus)->get('chitentry')->num_rows();
					$difmon = $chittotalmonths - ($chitentrytotalamountrows +  $monthentry);
					if($difmon <0) {
						$calmonth = $monthentry +  $difmon;
					} else {
						$calmonth  = $monthentry;
					}
					$totalcurrentamt = $totalcurrentamt + ($currentamt*$calmonth);
					if($chitlookupsid == 6) { //for both type
						$this->db->select_sum('chitamount');
						$this->db->from('chitentry');
						$this->db->where('chitbookno',$allchitbookno[$i]);
						$query = $this->db->get();
						$chitentrytotal = $query->row()->chitamount;
						$allchitentrytotal = $allchitentrytotal + $chitentrytotal;
					} else { //for intrest and gram
						$chitentrytotal = $chitentrytotalamountrows * $chitamtdenom;
						$allchitentrytotal = $allchitentrytotal + $chitentrytotal;
					}
					
				}
			}
		} 
		$paidchitentry = $chitentrytotalamountrows * $chitamtdenom;
		$totalchitamt = $chittotalmonths * $chitamtdenom;
		if($paidchitentry >= $totalchitamt){
			$chitentryarray=array(
				'currentamount' =>0,
				'totalamount' =>0,
				'currentmonth'=>0,
				'lastpaymentdate'=>0,
				'closed' =>1
			);
		} else {
			$this->db->select_max('chitentrydate');
			$this->db->where('chitbookno',$chitbookno);
			$this->db->where('status',1);
			$info=$this->db->get('chitentry');
			$chitentryarray=array(
				'currentamount' =>$totalcurrentamt,
				'totalamount' =>$allchitentrytotal,
				'currentmonth'=>$chitentrytotalamountrows+1,
				'lastpaymentdate'=>$this->Basefunctions->ymd_format($info->row()->chitentrydate),
				'closed' =>0		
			);
		}
		echo json_encode($chitentryarray);
	}	
	// get chit book rate details
	public function getratedetails() {
		$chitratedetails=array();
		$chitbookno=$_GET['chitbookno'];
		$otherchitbookid=$_GET['otherchitbookid'];
		$chitbookentrydate=$this->Basefunctions->ymd_format($_GET['chitbookentrydate']); 
		if($otherchitbookid != 0 && $otherchitbookid != '') {
			$first_chitbook=array();
			$other_chitbook=array();
			$first_chitbook[]=$chitbookno;
			$otherchitbookcount=count($otherchitbookid);
			for($i=0; $i< $otherchitbookcount; $i++) {
				$other_chitbook[]=$otherchitbookid[$i];
			}
			$chitentrygroup_array = array_merge($other_chitbook,$first_chitbook);
			$this->db->select('chitscheme.chitschemeid,chitscheme.purityid,chitbook.chitschemeid,purity.purityid,purity.purityname',false);
			$this->db->join('chitscheme','chitscheme.chitschemeid = chitbook.chitschemeid');
			$this->db->join('purity','purity.purityid = chitscheme.purityid');
			$this->db->where_in('chitbookno',$chitentrygroup_array);
			$this->db->where('purity.purityid !=',1);
			$this->db->group_by('purity.purityid');
			$chitarray=$this->db->get('chitbook');
			$chitpurity=$chitarray->result(); 
			if($chitarray->num_rows() > 0)	{
				for($k=0; $k< count($chitpurity); $k++) {
					$getrate =$this->Basefunctions->getrate($chitpurity[$k]->purityid);
					foreach($getrate as $current_rate) {
						$purity_rate = $current_rate;
					}
					$chitratedetails[]=$chitpurity[$k]->purityname.'-'.$purity_rate;
				} 
			} else {
				$chitratedetails='';
			}
		} else {
			$this->db->select('chitscheme.chitschemeid,chitscheme.purityid,chitbook.chitschemeid,purity.purityid,purity.purityname',false);
			$this->db->join('chitscheme','chitscheme.chitschemeid = chitbook.chitschemeid');
			$this->db->join('purity','purity.purityid = chitscheme.purityid');
			$this->db->where('chitbookno',$chitbookno);
			$this->db->where('purity.purityid !=',1);
			$this->db->group_by('purity.purityid');
			$chit_data_purity=$this->db->get('chitbook');
			$chitpurity=$chit_data_purity->row(); 
			if($chit_data_purity->num_rows() > 0) {
				$getrate =$this->Basefunctions->getrate($chitpurity->purityid); 
				foreach($getrate as $current_rate){
					$purity_rate = $current_rate;
				} 
				$chitratedetails=$chitpurity->purityname.'-'.$purity_rate;
			} else {
				$chitratedetails=''; 
			}
		}
		echo json_encode($chitratedetails);
	}
	// chit book entry add
	public function chitbookentrycreate() 
	{	
		$loclingtime = $this->Basefunctions->get_company_settings('lockingtime');
		$updatingtime = $this->Basefunctions->get_company_settings('updatingtime');
		$currenttime = time("H:i:s");
		$ctime = new DateTime();
		$ctime->setTimestamp($currenttime);
		$ltime  = new DateTime();
		if($loclingtime == '00:00:00') {
			$ltime->setTimestamp($currenttime);
		} else {
			$ltime->setTimestamp(strtotime($loclingtime));
		}
		if($ctime > $ltime) {
			$chitbookentrydate = date("Y-m-d",strtotime("tomorrow"));
			$chitbookentrydate = $this->Basefunctions->checkgivendateisholidayornot($chitbookentrydate);
		} else {
			$chitbookentrydate=$this->Basefunctions->ymd_format($_POST['chitbookentrydate']);
		}
		if(isset($_POST['chitreferencedate'])) {
			$_POST['chitreferencedate'] = $this->Basefunctions->ymd_format($_POST['chitreferencedate']);
		} else {
			$_POST['chitreferencedate'] = $chitbookentrydate;
		}
		$rate_array=array();
		$array_rate=array();
		$chitbook_amt=0;
		$chitscheme_amt=0;
	    $schemechittype=0;
	    $interestmode=0;
		$purity_id=0;
		$chitschemeid=0;
		$entryamount=0;
		$entryweight=0;
		$getrate=0;
	    $round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
	    $schemeprefix=$_POST['schemeprefix'];
		$chitentrygroup=array(
			'chitentrygroupdate' => $chitbookentrydate,
			'accountid' => $_POST['accountid'],
			'employeeid' => $_POST['employeeid'],
			'noofmonthsentry' => $_POST['noofmonthsentry'],
			'printtemplateid' => $_POST['printtemplate'],
			'cashamount' => $_POST['cashamount'],
			'chequeamount' => $_POST['chequeamount'],
			'cardamount' => $_POST['cardamount'],
			'ddamount' => $_POST['ddamount'],
			'onlineamount' => $_POST['onlineamount'],
			'comments' => $_POST['comments'],
			'totalamount' => $_POST['currentamount'],
			'industryid'=>$this->Basefunctions->industryid
		);
		if($ctime > $ltime) {
			$chitentrygroup_insert = array_merge($chitentrygroup,$this->Crudmodel->nextdaydefaultvalueget());
		} else {
			$chitentrygroup_insert = array_merge($chitentrygroup,$this->Crudmodel->defaultvalueget());
		}
		$this->db->insert('chitentrygroup',array_filter($chitentrygroup_insert));
		$chitentrygroupid = $this->db->insert_id();
		$girddata = $_POST['criteriagriddata'];
		$cricount = $_POST['criteriacnt'];
		$girddatainfo = json_decode($girddata,true);
		$defdatedataset = $this->Crudmodel->nextdaydefaultvalueget();
		for($i=0;$i<=($cricount-1);$i++) {
			if($girddatainfo[$i]['cardtypeid'] =='' || $girddatainfo[$i]['cardtypeid'] == null) {
				$cardtype = "1";
			} else {
				$cardtype = $girddatainfo[$i]['cardtypeid'];
			}
			$payment_details = array(
				'chitentrygroupid'=>$chitentrygroupid,
				'stocktypeid'=>$girddatainfo[$i]['stocktypeid'],
				'accountid'=>$girddatainfo[$i]['accountid'],
				'paymentamount' => $girddatainfo[$i]['paymentamount'],
				'cardtypeid'=>$cardtype,
				'bankname'=>$girddatainfo[$i]['bankname'],
				'referencenumber' => $girddatainfo[$i]['referenceno'],
				'referencedate' => $this->Basefunctions->ymd_format($girddatainfo[$i]['referencedate']),
				'approvalcode' => $girddatainfo[$i]['approvalcode'],
				'status'=>$this->Basefunctions->activestatus
			);
			$payment_details = array_merge($payment_details,$defdatedataset);
			$this->db->insert('chitentrydetail',array_filter($payment_details));
		}
		$ratedetails=$_POST['ratedetails'];
		if(empty($ratedetails)) { 
			$array_rate[]='';
		} else {
			$rateexplode=explode(',',$ratedetails);
			for($h=0; $h<count($rateexplode); $h++) {
				$rate_array[]=explode('-',$rateexplode[$h]);
			} 
			for($k=0; $k<count($rate_array); $k++) {
				$array_rate[$rate_array[$k][0]]=$rate_array[$k][1];
			} 
		}

		$chitentrygroup_array[0]=$_POST['chitbookno'];
		$mp=0;
		for($i=0; $i < count($chitentrygroup_array); $i++) {
			//print_r($chitentrygroup_array[$i]);
			$this->db->select('chitbook.chitschemeid,chitbook.amount,chitscheme.chitschemeid,chitscheme.chitlookupsid,chitscheme.purityid,purity.purityid,chitscheme.noofmonths,chitscheme.intrestmodeid,entrytemplateid,entrytemplatetype,chitscheme.variablemodeid',false);
			$this->db->from('chitbook');
			$this->db->join('chitscheme','chitscheme.chitschemeid = chitbook.chitschemeid');
			$this->db->join('purity','purity.purityid = chitscheme.purityid');
			$this->db->where('chitbookno',$chitentrygroup_array[$i]);
			$chittypedetails = $this->db->get();  
			foreach($chittypedetails->result() as $chittypedetail) {
				$chitbook_amt=$chittypedetail->amount;
				$chitscheme_amt=$chittypedetail->amount;
				$schemechittype=$chittypedetail->chitlookupsid;
				$interestmode=$chittypedetail->intrestmodeid;
				$purity_id=$chittypedetail->purityid;
				$chitschemeid=$chittypedetail->chitschemeid;
				$noofmonthssch=$chittypedetail->noofmonths;
				$entrytemplateid=$chittypedetail->entrytemplateid;
				$entrytemplatetype=$chittypedetail->entrytemplatetype;
				$variablemodeid=$chittypedetail->variablemodeid;
			}
			if($schemechittype == 2) {
				if($variablemodeid == 0) {
					$entryamount = $chitscheme_amt;
				} else if($variablemodeid == 1) {
					$entryamount = $_POST['currentamount'];
				}
				$entryweight = 0;
				$getrate = 0;
			} else if($schemechittype == 3) {
				if($variablemodeid == 0) {
					$entryamount = $chitscheme_amt;
				} else if($variablemodeid == 1) {
					$entryamount = $_POST['currentamount'];
				}
				if(array_key_exists($purity_id,$array_rate)) {
					 $getrate =$array_rate[$purity_id];
				} else { 
					$bgetrate = $this->Chitbookentrymodel->getratebasedondate($purity_id,$chitbookentrydate);
				    $getrate = $bgetrate['rate'];
				}
				$entryweight = round($chitscheme_amt/$getrate,$round); 
			} else if($schemechittype == 6)  {
				if(array_key_exists($purity_id,$array_rate)) {
					 $getrate =$array_rate[$purity_id];
				} else { 
					$bgetrate = $this->Chitbookentrymodel->getratebasedondate($purity_id,$chitbookentrydate);
				    $getrate = $bgetrate['rate'];
				}				
				if($variablemodeid == 0) {
					$entryamount = $chitscheme_amt;
				} else if($variablemodeid == 1) {
					$entryamount = $_POST['currentamount'];
				}
				$entryweight = round($entryamount/$getrate,$round); 
			}
			$noofmonth=$_POST['noofmonthsentry'];
			$k=0;
			$skipentry = 0;
			for($j=1; $j<= $noofmonth; $j++) {
				$this->db->select('max(chitentryid) as lastchitentryid,max(chitentrydate) as lastchitdate,count(chitentryid) as totalchitentrymonths');
				$this->db->where('chitbookno',$chitentrygroup_array[$i]);
				$this->db->where('status',1);
				$info=$this->db->get('chitentry');
				$last_chitentryid=$info->row()->lastchitentryid;
				$last_chitentrydate=$info->row()->lastchitdate;
				$last_chitentrymonth=$info->row()->totalchitentrymonths;
				if($last_chitentrymonth == '0') {
					$last_chitentrydate = $chitbookentrydate;
					$payment_datediff=0;
					$last_chitentrymonth=1;
					$pdiff = 0;
				}else{
					$last_chitentrymonth=$last_chitentrymonth+1;
					if($last_chitentrymonth > $noofmonthssch) {
					  $skipentry = 1;
					}else{
					  $skipentry = 0;
					  $now = time(); 
					  $your_date = strtotime($last_chitentrydate);
					  $datediff = $now - $your_date;
					  $payment_datediff = floor(abs(strtotime($chitbookentrydate) - strtotime($last_chitentrydate) )/(60*60*24));
					  $pdiff = $payment_datediff;
					    /* $mon = 0;
						while( $payment_datediff >= 30 ) {
							$mon++;
							$payment_datediff = $payment_datediff - 30;
						}
						$mon = str_pad($mon,2,0,STR_PAD_LEFT);
						$payment_datediff = str_pad($payment_datediff,2,0,STR_PAD_LEFT);
						
						$pdiff = $mon.'M'.$payment_datediff.'D'; */
					}
				}
				if($skipentry == 0) {
					
					$this->db->where('chitschemeid',$chitschemeid);
					$lastentryrows = $this->db->get('chitentry')->num_rows();
					
					$lastentryrows = $lastentryrows+$k+1;
					$lastentryrows = str_pad($lastentryrows,1,0,STR_PAD_LEFT);
					$chitentryno = $schemeprefix.''.$lastentryrows;
					$chitentryarray=array(
						'chitentrygroupid' => $chitentrygroupid,
						'employeeid' => $_POST['employeeid'],
						'chitentrydate' => $chitbookentrydate,
						'chitreferencedate' => $_POST['chitreferencedate'],
						'chitbookno' => $chitentrygroup_array[$i],
						'chitentryno' => $chitentryno,
						'chitschemeid' => $chitschemeid,
						'chitamount' => $chitbook_amt,
						'interestamount' => 0,
						'entryamount' => $entryamount,
						'entryweight' => $entryweight,
						'rate' => $getrate,
						'purityid' => $purity_id,
						'monthentry' => $last_chitentrymonth,
						'paymentdatedifference' => $pdiff,
						'defaultercount' => 1
					); 
					if($ctime > $ltime) {
						$chitentryarray_insert = array_merge($chitentryarray,$this->Crudmodel->nextdaydefaultvalueget());
					} else {
						$chitentryarray_insert = array_merge($chitentryarray,$this->Crudmodel->defaultvalueget());
					}
					$this->db->insert('chitentry',array_filter($chitentryarray_insert));
					$lastchitentryid = $this->db->insert_id();
					$k++;
					$chitentryprintarray[$mp]=array(
						'chitentryid' => $lastchitentryid,
						'chitbookno' => $chitentrygroup_array[$i],
						'entrytemplateid' => $entrytemplateid,
						'entrytemplatetype' => $entrytemplatetype
					); 
					$mp++;
				} 
				$this->totalweightupdate($lastchitentryid,$chitentrygroup_array[$i],$chitschemeid);
			}
			//last month intrest rate entry
			if($interestmode == 4) { //if scheme is type c means
				$this->db->where('chitschemeid',$chitschemeid);
				$lastentryrows = $this->db->get('chitentry')->num_rows();
				
				$lastentryrows = $lastentryrows+1;
				$lastentryrows = str_pad($lastentryrows,1,0,STR_PAD_LEFT);
				$chitentryno = $schemeprefix.''.$lastentryrows;
				
				if($last_chitentrymonth == $noofmonthssch) {
					
					$chitentrygroup=array(
						'totalamount' => $_POST['currentamount']*2,
						'freemonthamount' => $_POST['currentamount'],
					);
					$this->db->where('chitentrygroupid',$chitentrygroupid);
					$this->db->update('chitentrygroup',$chitentrygroup);
					$chitentryarray=array(
						'chitentrygroupid' => $chitentrygroupid,
						'employeeid' => $_POST['employeeid'],
						'chitentrydate' => $chitbookentrydate,
						'chitreferencedate' => $_POST['chitreferencedate'],
						'chitbookno' => $chitentrygroup_array[$i],
						'chitentryno' => $chitentryno,
						'chitschemeid' => $chitschemeid,
						'chitamount' => $chitbook_amt,
						'interestamount' => 0,
						'entryamount' => $entryamount,
						'entryweight' => $entryweight,
						'rate' => $getrate,
						'purityid' => $purity_id,
						'monthentry' => $last_chitentrymonth+1,
						'paymentdatedifference' => $pdiff,
						'defaultercount' => 1,
						'freemonthamount' => $chitbook_amt
					);
					if($ctime > $ltime) {
						$chitentryarray_insert = array_merge($chitentryarray,$this->Crudmodel->nextdaydefaultvalueget());
					} else {
						$chitentryarray_insert = array_merge($chitentryarray,$this->Crudmodel->defaultvalueget());
					}		
					$this->db->insert('chitentry',array_filter($chitentryarray_insert));
					$lastchitentryid = $this->db->insert_id();
					$schemechittype = $this->Basefunctions->singlefieldfetch('chitlookupsid','chitschemeid','chitscheme',$chitschemeid);
					$chitentryprintarray[$mp]=array(
						'chitentryid' => $lastchitentryid,
						'chitbookno' => $chitentrygroup_array[$i],
						'entrytemplateid' => $entrytemplateid,
						'entrytemplatetype' => $entrytemplatetype
					); 
					$mp++;
					$this->totalweightupdate($lastchitentryid,$chitentrygroup_array[$i],$chitschemeid);
				}
			}
			if($last_chitentrymonth == $noofmonthssch) {
				$chitclosed = array('chitclosed'=>1);
				$this->db->where('chitbookno',$chitentrygroup_array[$i]);
			    $this->db->update('chitbook',$chitclosed);
			}
			{//workflow management -- for data create
				$this->Basefunctions->workflowmanageindatacreatemode($this->chitbookentrymoduleid,$chitentrygroupid,'chitentrygroup');
			}
		}
		// notification log
		/* $userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' created chitbookentry group';
		$this->Basefunctions->notificationcontentadd($chitentrygroupid,'Added',$activity ,$userid,$this->chitbookentrymoduleid); */
				
		if($_POST['printtemplate'] == 'YES') {
			/* $this->db->select('chitentryid,entrytemplateid,entrytemplatetype,chitbookno',false);
			$this->db->from('chitentry');
			$this->db->join('chitscheme','chitscheme.chitschemeid = chitentry.chitschemeid');
			$this->db->where('chitentrygroupid',$chitentrygroupid);
			$this->db->where('chitentry.status',1);
			$allchitentryid=$this->db->get();  
			foreach($allchitentryid->result() as $allchitentryid) {
				if($allchitentryid->entrytemplatetype == '2') {
					$print_data=array('templateid'=> $allchitentryid->entrytemplateid, 'Templatetype'=>'Chit Entry group temp','primaryset'=>'chitentry.chitentryid','primaryid'=>$allchitentryid->chitentryid,'parentid'=>$allchitentryid->chitentryid,'chitbookno'=>$allchitentryid->chitbookno,'where'=>'chitentry.chitentryid','join'=>'join chitentry on chitentry.chitentrygroupid = chitentrygroup.chitentrygroupid LEFT OUTER JOIN chitscheme on chitentry.chitschemeid = chitscheme.chitschemeid LEFT OUTER JOIN employee on employee.employeeid = chitentrygroup.employeeid LEFT OUTER JOIN account on account.accountid = chitentrygroup.accountid');
					$data = $this->Printtemplatesmodel->generatlabelprint($print_data);
				}
			} */
			for($q=0; $q< count($chitentryprintarray);$q++) {
				if($chitentryprintarray[$q]['entrytemplatetype'] == '2') {
					$print_data=array('templateid'=> $chitentryprintarray[$q]['entrytemplateid'], 'Templatetype'=>'Chit Entry group temp','primaryset'=>'chitentry.chitentryid','primaryid'=>$chitentryprintarray[$q]['chitentryid'],'parentid'=>$chitentryprintarray[$q]['chitentryid'],'chitbookno'=>$chitentryprintarray[$q]['chitbookno'],'where'=>'chitentry.chitentryid','join'=>'join chitentry on chitentry.chitentrygroupid = chitentrygroup.chitentrygroupid LEFT OUTER JOIN chitscheme on chitentry.chitschemeid = chitscheme.chitschemeid LEFT OUTER JOIN employee on employee.employeeid = chitentrygroup.employeeid LEFT OUTER JOIN account on account.accountid = chitentrygroup.accountid');
					$data = $this->Printtemplatesmodel->generatlabelprint($print_data);
				}
			}
		}
	    echo 'SUCCESS';
	}
	// total weight update for chit gram
	public function totalweightupdate($lastchitentryid,$chitentrygroup_array,$chitschemeid) {
				$this->db->select('SUM(entryweight) as entryweight,SUM(entryamount) as entryamount, SUM(chitamount) as chitamount',false);
				$this->db->from('chitentry');
				$this->db->where('chitbookno',$chitentrygroup_array);
				$this->db->where('chitschemeid',$chitschemeid);
				$this->db->where('chitentryid <=',$lastchitentryid);
				$this->db->where('status',1);
				$query = $this->db->get();
				$chitentrytotalwt = $query->row()->entryweight;
				$chitentryamount = $query->row()->entryamount;
				$chitchitamount = $query->row()->chitamount;
				$chittotalwt = array('totalweight'=>$chitentrytotalwt,'totalentryamount'=>$chitentryamount,'totalchitamount'=>$chitchitamount);
				$this->db->where('chitentryid',$lastchitentryid);
			    $this->db->update('chitentry',$chittotalwt);
	}
	public function allchitentrytotalweightupdate($chitentrystartno,$chitentryendno) {
		$this->db->select('chitentry.chitentryid,chitentry.chitschemeid,chitentry.chitbookno',false);
		$this->db->from('chitentry');
		$this->db->join('chitscheme','chitscheme.chitschemeid = chitentry.chitschemeid');
		$this->db->where('chitentry.status',$this->Basefunctions->activestatus);
		$this->db->where('chitscheme.chitlookupsid',3);
		$this->db->where('chitentryid >=',$chitentrystartno);
		$this->db->where('chitentryid <=',$chitentryendno);
		$allchitentryid=$this->db->get();
		foreach($allchitentryid->result() as $allchitentryid) {
			
				$this->db->select_sum('entryweight');
				$this->db->from('chitentry');
				$this->db->where('chitbookno',$allchitentryid->chitbookno);
				$this->db->where('chitschemeid',$allchitentryid->chitschemeid);
				$this->db->where('chitentryid <=',$allchitentryid->chitentryid);
				$this->db->where('status',1);
				$query = $this->db->get();
				$chitentrytotalwt = $query->row()->entryweight;
				$chittotalwt = array('totalweight'=>$chitentrytotalwt);
				$this->db->where('chitentryid',$allchitentryid->chitentryid);
				$this->db->update('chitentry',$chittotalwt);
		}
				
	}
	public function firstchitbookentry($date,$chitbookno,$schemeid,$accountid,$scheme_amount,$monthentry,$userid,$printtemplateid,$chitcashamount=0,$chitcardamount=0,$chitchequeamount=0,$chitinterestamount=0,$chitneftamount=0) {
		$loclingtime = $this->Basefunctions->get_company_settings('lockingtime');
		$updatingtime = $this->Basefunctions->get_company_settings('updatingtime');
		$currenttime = time("H:i:s");
		$ctime = new DateTime();
		$ctime->setTimestamp($currenttime);
		$ltime  = new DateTime();
		if($loclingtime == '00:00:00') {
			$ltime->setTimestamp($currenttime);
		} else {
			$ltime->setTimestamp(strtotime($loclingtime));
		}
		if($ctime > $ltime) {
			$date = date("Y-m-d",strtotime("tomorrow"));
			$date = $this->Basefunctions->checkgivendateisholidayornot($date);
		} else {
			$date=$date;
		}
  	    $round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$total_amount = $scheme_amount*$monthentry;
		if($monthentry == 1) {
			$totalintrestamt = $chitinterestamount;
			$chitcashamount = $chitcashamount;
			$chitcardamount = $chitcardamount;
			$chitchequeamount = $chitchequeamount;
			$chitneftamount = $chitneftamount;
		} else {
			$chitcashamount = $chitcashamount*$monthentry;
			$chitcardamount = $chitcardamount*$monthentry;
			$chitchequeamount = $chitchequeamount*$monthentry;
			$chitneftamount = $chitneftamount*$monthentry;
		}
		$chitentrygroup=array(
			'chitentrygroupdate' => $date,
			'accountid' => $accountid,
			'employeeid' => $userid,
			'noofmonthsentry' => $monthentry,
			'printtemplateid' => $printtemplateid,
			'cashamount' => $chitcashamount,
			'cardamount' => $chitcardamount,
			'chequeamount' => $chitchequeamount,
			'neftamount' => $chitneftamount,
			'totalamount' => $total_amount,
			'totalintrestamount' => $totalintrestamt,
			'industryid'=>$this->Basefunctions->industryid
		);
		if($ctime > $ltime) {
			$chitentrygroup_insert = array_merge($chitentrygroup,$this->Crudmodel->nextdaydefaultvalueget());
		} else {
			$chitentrygroup_insert = array_merge($chitentrygroup,$this->Crudmodel->defaultvalueget());
		}
		$this->db->insert('chitentrygroup',array_filter($chitentrygroup_insert));
	
		$chitentrygroupid = $this->db->insert_id();
		$this->db->where('chitschemeid',$schemeid);
		$lastentryrows = $this->db->get('chitentry')->num_rows();
		$lastentryrows=$lastentryrows+1;
		$lastentryrows = str_pad($lastentryrows,1,0,STR_PAD_LEFT);
		
		$this->db->select('chitbook.chitschemeid,chitbook.amount,chitscheme.chitschemeid,chitscheme.chitlookupsid,chitscheme.purityid,purity.purityid,chitscheme.schemeprefix,chitscheme.intrestmodeid,entrytemplateid,entrytemplatetype',false);
		$this->db->from('chitbook');
		$this->db->join('chitscheme','chitscheme.chitschemeid = chitbook.chitschemeid');
		$this->db->join('purity','purity.purityid = chitscheme.purityid');
		$this->db->where('chitbookno',$chitbookno);
		$chittypedetails=$this->db->get();  
		
		foreach($chittypedetails->result() as $chittypedetail) {
			$chitbook_amt=$chittypedetail->amount;
			$base_amt=$chittypedetail->amount;
			$schemechittype=$chittypedetail->chitlookupsid;
			$purity_id=$chittypedetail->purityid;
			$chitschemeid=$chittypedetail->chitschemeid;
			$schemeprefix=$chittypedetail->schemeprefix;
			$intrestmode =$chittypedetail->intrestmodeid;
			$entrytemplateid =$chittypedetail->entrytemplateid;
			$entrytemplatetype =$chittypedetail->entrytemplatetype;
		}
		if($schemechittype == 2) {
			$entryamount = $base_amt;
			$entryweight=0;
			$getrate=0;
		} else if($schemechittype == 3 || $schemechittype == 6) {
			$entryamount=$base_amt;
			$bgetrate = $this->Chitbookentrymodel->getratebasedondate($purity_id,$date);
			$getrate = $bgetrate['rate'];
			$entryweight = $base_amt/$getrate; 
			$entryweight = round($entryweight,$round); 
		}
		$otherscheme_amount = $scheme_amount;
		$mp=0;
		for($j=1; $j<= $monthentry; $j++) {
			if($j == 1) {
				if($schemechittype == 2) { //intrest
					if($intrestmode == 2) { // for type b
						$scheme_amount = $scheme_amount / 2;
						$intrestamt = $scheme_amount;
					} else {// for type a
						$scheme_amount = $scheme_amount;
						$intrestamt = 0;
					}
				} else { //gram and both
					$scheme_amount = $scheme_amount;
					$intrestamt = 0;
				}
			} else {
				$scheme_amount = $otherscheme_amount;
				$intrestamt = 0;
			}
			$chitentryno = $schemeprefix.''.$lastentryrows;
			$chitentryarray=array(
				'chitentrygroupid' => $chitentrygroupid,
				'employeeid' => $userid,
				'chitentrydate' => $date,
				'chitreferencedate' => $date,
				'chitbookno' => $chitbookno,
				'chitschemeid' => $schemeid,
				'monthentry' => $j,
				'chitamount' => $scheme_amount,
				'interestamount' => $intrestamt,
				'chitentryno' => $chitentryno,
				'entryamount' => $entryamount,
				'entryweight' => $entryweight,
				'rate' => $getrate,
				'purityid' => $purity_id,
				'paymentdatedifference' => '00M00D',
				'defaultercount' => 1,
				'status'=>$this->Basefunctions->activestatus		
			); 
			if($ctime > $ltime) {
				$chitentryarray_insert = array_merge($chitentryarray,$this->Crudmodel->nextdaydefaultvalueget());
			} else {
				$chitentryarray_insert = array_merge($chitentryarray,$this->Crudmodel->defaultvalueget());
			}
			$this->db->insert('chitentry',array_filter($chitentryarray_insert));
			$lastchitentryid = $this->db->insert_id();
			$lastentryrows++;
			$chitentryprintarray[$mp]=array(
				'chitentryid' => $lastchitentryid,
				'chitbookno' => $chitbookno,
				'entrytemplateid' => $entrytemplateid,
				'entrytemplatetype' => $entrytemplatetype
			); 
			$mp++;
			$this->totalweightupdate($lastchitentryid,$chitbookno,$schemeid);
		}
		if($printtemplateid == 'YES') {
			/* $this->db->select('chitentryid,entrytemplateid,entrytemplatetype,chitbookno',false);
			$this->db->from('chitentry');
			$this->db->join('chitscheme','chitscheme.chitschemeid = chitentry.chitschemeid');
			$this->db->where('chitentrygroupid',$chitentrygroupid);
			$this->db->where('chitentry.status',$this->Basefunctions->activestatus);
			$allchitentryid=$this->db->get();  
			foreach($allchitentryid->result() as $allchitentryid) {
				if($allchitentryid->entrytemplatetype == '2') {
					$print_data=array('templateid'=> $allchitentryid->entrytemplateid, 'Templatetype'=>'Chit Entry group temp','primaryset'=>'chitentry.chitentryid','primaryid'=>$allchitentryid->chitentryid,'parentid'=>$allchitentryid->chitentryid,'chitbookno'=>$allchitentryid->chitbookno,'where'=>'chitentry.chitentryid','join'=>'join chitentry on chitentry.chitentrygroupid = chitentrygroup.chitentrygroupid LEFT OUTER JOIN chitscheme on chitentry.chitschemeid = chitscheme.chitschemeid LEFT OUTER JOIN employee on employee.employeeid = chitentrygroup.employeeid LEFT OUTER JOIN account on account.accountid = chitentrygroup.accountid');
				$data = $this->Printtemplatesmodel->generatlabelprint($print_data);
				}
			} */
			for($q=0; $q< count($chitentryprintarray);$q++) {
				if($chitentryprintarray[$q]['entrytemplatetype'] == '2') {
					$print_data=array('templateid'=> $chitentryprintarray[$q]['entrytemplateid'], 'Templatetype'=>'Chit Entry group temp','primaryset'=>'chitentry.chitentryid','primaryid'=>$chitentryprintarray[$q]['chitentryid'],'parentid'=>$chitentryprintarray[$q]['chitentryid'],'chitbookno'=>$chitentryprintarray[$q]['chitbookno'],'where'=>'chitentry.chitentryid','join'=>'join chitentry on chitentry.chitentrygroupid = chitentrygroup.chitentrygroupid LEFT OUTER JOIN chitscheme on chitentry.chitschemeid = chitscheme.chitschemeid LEFT OUTER JOIN employee on employee.employeeid = chitentrygroup.employeeid LEFT OUTER JOIN account on account.accountid = chitentrygroup.accountid');
					$data = $this->Printtemplatesmodel->generatlabelprint($print_data);
				}
			}
		}
	}
	public function chitbookentrydelete() {
		$id=$_GET['primaryid'];
		// chit close revert
		$this->db->select('DISTINCT(chitbookno)',false);
		$this->db->from('chitentry');
		$this->db->where('chitentrygroupid',$id);
		$this->db->where('chitentry.status',$this->Basefunctions->activestatus);
		$chitbookno=$this->db->get()->row()->chitbookno;
        $chitclose = array('chitclosed'=>0);
		$this->db->where('chitbookno',$chitbookno);
		$this->db->update('chitbook',$chitclose);
		//-inactivate account
		$inactive = $this->Crudmodel->deletedefaultvalueget();
		$this->db->where('chitentrygroupid',$id);
		$this->db->update('chitentrygroup',$inactive);
		$this->db->where('chitentrygroupid',$id);
		$this->db->update('chitentry',$inactive);
		// notification log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' deleted chitbookentry group';
		$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,$this->chitbookentrymoduleid);
		echo 'SUCCESS';
	}
	public function chitbookentrycancel() {
		$id=$_GET['primaryid'];
		// chit close revert
		$this->db->select('DISTINCT(chitbookno)',false);
		$this->db->from('chitentry');
		$this->db->where('chitentrygroupid',$id);
		$this->db->where('chitentry.status',$this->Basefunctions->activestatus);
		$chitbookno = $this->db->get()->row()->chitbookno;
        $chitclose = array('chitclosed'=>0);
		$this->db->where('chitbookno',$chitbookno);
		$this->db->update('chitbook',$chitclose);
		//-inactivate account
		$inactive = $this->Crudmodel->canceldefaultvalueget();
		$this->db->where('chitentrygroupid',$id);
		$this->db->update('chitentrygroup',$inactive);
		$this->db->where('chitentrygroupid',$id);
		$this->db->update('chitentry',$inactive);
		// notification log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' cancelled chitbookentry group';
		$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,$this->chitbookentrymoduleid);
		echo 'SUCCESS';
	}
	public function chitbookentryretrive() {
		$chitbook_array=array();
		$id=$_GET['primaryid'];
		$this->db->select('chitentrygroup.chitentrygroupdate,chitentrygroup.employeeid,chitentrygroup.accountid,chitentrygroup.noofmonthsentry,chitentrygroup.printtemplateid,chitentrygroup.cashamount,chitentrygroup.chequeamount,chitentrygroup.cardamount,chitentrygroup.neftamount,chitentrygroup.neftdetails,chitentrygroup.chequedetails,chitentrygroup.carddetails,chitentrygroup.comments,chitentrygroup.totalamount,account.accountname,employee.employeename');
		$this->db->from('chitentrygroup');
		$this->db->join('account','account.accountid=chitentrygroup.accountid');
		$this->db->join('employee','employee.employeeid=chitentrygroup.employeeid');
		$this->db->where('chitentrygroup.chitentrygroupid',$id);
		$this->db->where('chitentrygroup.status',$this->Basefunctions->activestatus);
		$info=$this->db->get()->row(); 
		$chitbookno=$this->db->select('chitbookno')->where('chitentrygroupid',$id)->group_by('chitbookno')->get('chitentry')->result();  
		$accountgroupdetails=$this->db->where('accountid',$info->accountid)->get('accountgroup')->row();
		$accountgrouprows=$this->db->where('accountid',$info->accountid)->get('accountgroup')->num_rows();
		if($accountgrouprows != 0) {
			if($accountgroupdetails->accountgroupname == '') {
				$accountgroupname='';
			} else {
				$accountgroupname=$accountgroupdetails->accountgroupname;
			}	
		} else {
			$accountgroupname='';
		}
		$accountdetails=$this->db->where('accountid',$info->accountid)->get('account')->row();
		$i=0;
		foreach($chitbookno as $chitbooknoall) {
			$chitbook_array[]=$chitbooknoall->chitbookno;
			$i++;
		}
		$chitbook_implode=implode(',',$chitbook_array);
		$jsonarray=array(
			'chitbookentrydate'=>$info->chitentrygroupdate,
			'employeename'=>$info->employeename,
			'accountname'=>$info->accountname,
			'noofmonthsentry'=>$info->noofmonthsentry,
			'currentamount'=>$info->totalamount,
			'printtemplate'=>$info->printtemplateid,
			'cashamount'=>$info->cashamount,
			'cardamount'=>$info->cardamount,
			'chequeamount'=>$info->chequeamount,
			'chequedetail'=>$info->chequedetails,
			'neftamount'=>$info->neftamount,
			'neftdetail'=>$info->neftdetails,
			'carddetail'=>$info->carddetails,
			'comments'=>$info->comments,
			'chitbookno'=>$chitbook_implode
		);
		echo json_encode($jsonarray);
	}
	public function getschemedetails() {
		$chitbookno=$_GET['primaryid'];
		$this->db->select('chitbook.chitschemeid,chitbook.amount,chitscheme.chitschemeid,chitscheme.chitschemename');
		$this->db->from('chitbook');
		$this->db->join('chitscheme','chitscheme.chitschemeid=chitbook.chitschemeid');
		$this->db->where('chitbook.chitbookno',$chitbookno);
		$this->db->where('chitbook.status',$this->Basefunctions->activestatus);
		$getschemedata=$this->db->get();
		$schemedata=$getschemedata->row();
		if($getschemedata->num_rows() > 0) {
			$getscheme=array('schemeid' => $schemedata->chitschemeid, 'schemename' => $schemedata->chitschemename, 'amount' => $schemedata->amount );
		} else {
			$getscheme='NO';
		}
		echo json_encode($getscheme);
	}
	public function getprizedetails() {
		$prizerow=array();
		$prizerow1=array();
		$schemeid=$_GET['schemeid'];
		$amount=$_GET['amount'];
		$gifttype=$_GET['gifttype'];
		$chitbookno=$_GET['chitbookno'];
		$prizedetails=$this->db->where('chitschemeid',$schemeid)->where_in('schemeamount',array($amount,'ALL'))->where('chitlookupsid',$gifttype)->where('status',$this->Basefunctions->activestatus)->get('chitgift');
		if($prizedetails->num_rows() > 0) {
			$i=0;
			foreach($prizedetails->result() as $prizedataall) {
				$this->db->distinct()->select('prizeissue.chitgiftid,prizeissue.chitbookno,a.chitgiftid,a.chitgiftname');
				$this->db->from('prizeissue');
				$this->db->join('chitgift as a','a.chitgiftid=prizeissue.chitgiftid');
				$this->db->where('prizeissue.chitgiftid',$prizedataall->chitgiftid);
				$this->db->where('prizeissue.chitbookno',$chitbookno);
				$this->db->where('prizeissue.status',$this->Basefunctions->activestatus);
				$prizeissuedrow=$this->db->get();
				if($prizeissuedrow->num_rows()>0) {
					$j=0;
					foreach($prizeissuedrow->result() as $info) {
						$prizerow1[]=array('type'=>'Issued','chitgiftid'=>$info->chitgiftid,'prizename'=>$info->chitgiftname);
						$j++;				
					}			
				} else {
					$prizerow[]=array('type'=>'Not Issued','chitgiftid'=>$prizedataall->chitgiftid,'prizename'=>$prizedataall->chitgiftname,'prizeamount'=>$prizedataall->giftamount);
				}
				$i++;
			}
			$array_prize=array_merge($prizerow,$prizerow1);
		} else {
			$array_prize='NO';
		}
		echo json_encode($array_prize);
	}
	public function checkprizeissue() {
		$prizeid=$_GET['prizeid'];
		$chitbookno=$_GET['chitbookno'];
		$prizeissuedetails=$this->db->where('chitbookno',$chitbookno)->where('chitgiftid',$prizeid)->where('status',$this->Basefunctions->activestatus)->get('prizeissue');
	    if($prizeissuedetails->num_rows() > 0) {
			$prizeissuerow='YES';
		} else {
			$prizeissuerow='NO';
		}
		echo json_encode($prizeissuerow);
	}
	// prize issue create
	public function prizeissuecreate() {
		$insert=array(
			'issuedate' => $this->Basefunctions->ymd_format($_POST['prizeissuedate']),
			'chitbookno' => $_POST['issuechitbookno'],
			'chitschemeid' => $_POST['issueschemeid'],
			'chitamount' => $_POST['chitbookamount'],
			'chitlookupsid' => $_POST['typegift'],
			'chitgiftid' => $_POST['giftprize'],
			'giftamount' => $_POST['prizeamount'],
			'prizedescription' => $_POST['issuecomment'],
			'printtemplate' => $_POST['printtemplateissue'],
			'industryid'=>$this->Basefunctions->industryid,
			'status'=>$this->Basefunctions->activestatus		
		);
		$prizeissue_insert = array_merge($insert,$this->Crudmodel->defaultvalueget());
		$id=$this->db->insert('prizeissue',array_filter($prizeissue_insert));
		// notification log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' added prize issue';
		$this->Basefunctions->notificationcontentadd($id,'Added',$activity ,$userid,$this->chitbookentrymoduleid);
		echo 'SUCCESS';
	}
	public function prizedescription() {
		$prizeid=$_GET['prizeid'];
		$prizedescription = $this->db->distinct()->select('giftdescription')->from('chitgift')->where('chitgiftid',$prizeid)->where('status',$this->Basefunctions->activestatus)->get();
		 if($prizedescription->num_rows() > 0) {
			 $prizedescnames =$prizedescription->row()->giftdescription;
		 } else {
			 $prizedescnames ='NO';
		 }
		echo json_encode($prizedescnames);
	}
	public function prizeissuedelete() {
		$id=$_GET['primaryid']; 
		//-inactivate prizeissue
		$inactive = $this->Crudmodel->deletedefaultvalueget();
		$this->db->where('prizeissueid',$id);
		$this->db->update('prizeissue',$inactive);
		// notification log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' deleted prize issue';
		$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,$this->chitbookentrymoduleid);
		echo 'SUCCESS';
	}
	public function chitbookdata() {
		$id=$_GET['primaryid'];
		$chitbook_data=$this->db->where('chitbookid',$id)->get('chitbook')->row();
		$chitbookno=$chitbook_data->chitbookno;
		$chittotalamount=$this->db->select_sum('chitamount')->where('chitbookno',$chitbookno)->where('status',$this->Basefunctions->activestatus)->get('chitentry')->row()->chitamount;
		$chitbookrow=$this->db->where('chitbookno',$chitbookno)->where('status',$this->Basefunctions->activestatus)->get('chitentry')->num_rows();
	   if($chitbookrow > 0) {
		  	 $array_chitbook=array(
				'receiptcount' =>$chitbookrow ,
				'collectionamount' =>$chittotalamount
			);
	   } else {
		   $array_chitbook='';
	   }
		echo json_encode($array_chitbook);
	}
	public function chitentrygroupprint() {
		$data='Fail';
		$chitentrygroupid = trim($_GET['primaryid']);	
		$this->db->select('chitentryid,entrytemplateid,entrytemplatetype,chitbookno',false);
		$this->db->from('chitentry');
		$this->db->join('chitscheme','chitscheme.chitschemeid = chitentry.chitschemeid');
		$this->db->where('chitentrygroupid',$chitentrygroupid);
		$this->db->where('chitentry.status',$this->Basefunctions->activestatus);
		$allchitentryid=$this->db->get();
		foreach($allchitentryid->result() as $allchitentryid) {
			if($allchitentryid->entrytemplatetype == '2') {
				$print_data=array('templateid'=> $allchitentryid->entrytemplateid, 'Templatetype'=>'Chit Entry group temp','primaryset'=>'chitentry.chitentryid','primaryid'=>$allchitentryid->chitentryid,'parentid'=>$allchitentryid->chitentryid,'chitbookno'=>$allchitentryid->chitbookno,'where'=>'chitentry.chitentryid','join'=>'join chitentry on chitentry.chitentrygroupid = chitentrygroup.chitentrygroupid LEFT OUTER JOIN chitscheme on chitentry.chitschemeid = chitscheme.chitschemeid LEFT OUTER JOIN employee on employee.employeeid = chitentrygroup.employeeid LEFT OUTER JOIN account on account.accountid = chitentrygroup.accountid');
				$data = $this->Printtemplatesmodel->generatlabelprint($print_data);
			}
		}
		echo 'SUCCESS';
	}
	public function chitentryprint() {
		$data='Fail';
		$chitentrygroupid = trim($_GET['primaryid']);
		$chitentryid = trim($_GET['chitentryid']);
		$this->db->select('chitentryid,entrytemplateid,entrytemplatetype,chitbookno',false);
		$this->db->from('chitentry');
		$this->db->join('chitscheme','chitscheme.chitschemeid = chitentry.chitschemeid');
		$this->db->where('chitentryid',$chitentryid);
		$this->db->where('chitentry.status',$this->Basefunctions->activestatus);
		$allchitentryid=$this->db->get();
		foreach($allchitentryid->result() as $allchitentryid) {
			if($allchitentryid->entrytemplatetype == '2') {
				$print_data=array('templateid'=> $allchitentryid->entrytemplateid, 'Templatetype'=>'Chit Entry temp','primaryset'=>'chitentry.chitentryid','primaryid'=>$allchitentryid->chitentryid,'chitbookno'=>$allchitentryid->chitbookno,'where'=>'chitentry.chitentryid','join'=>'join chitentry on chitentry.chitentrygroupid = chitentrygroup.chitentrygroupid LEFT OUTER JOIN chitscheme on chitentry.chitschemeid = chitscheme.chitschemeid LEFT OUTER JOIN employee on employee.employeeid = chitentrygroup.employeeid LEFT OUTER JOIN account on account.accountid = chitentrygroup.accountid');
				$data = $this->Printtemplatesmodel->generatlabelprint($print_data);
			}
		}
		echo json_encode($data);
	}
	public function issuereprint() {
		$status='SUCCESS';
		echo json_encode($status);
	}
	public function chitentrycheck() {
		$data='Fail';
		$allchitentryrows = 0;
		$chitbookno = trim($_GET['chitbookno']);
		$monthentry = trim($_GET['monthentry']);
		$this->db->select('chitentryid',false);
		$this->db->from('chitentry');
		$this->db->where('chitbookno',$chitbookno);
		$this->db->where('chitentry.status',$this->Basefunctions->activestatus);
		$allchitentryrows=$this->db->get()->num_rows();
		if($allchitentryrows == $monthentry) {
			$data='SUCCESS';
		}
		echo json_encode($data);
	}
	//get the rate based on date and purity
	public function getratebasedondate($purity,$date) {		
		$data = $this->db->select('currentrate',false)
					->from('rate')
					->where('purityid',$purity)
					//->where('currentstatus',$this->activestatus)
					->where('status',$this->Basefunctions->activestatus)
					->where('DATE(createdate) <=',$date)
					->order_by('rate.rateid',"desc")
					->limit(1)
					->get();
		if($data->num_rows()) {
			foreach($data->result() as $info){
				$ratearr = array('rate'=>$info->currentrate);
			}
		} else {
			$ratearr = array('rate'=>1);
		}
		return $ratearr;
	}
	//split tag inner grid header fetch
	public function chitbookentryinnergridheaderfetch($moduleid) {
		$fnames = array('chitentrydetailid','stocktypeid','stocktypename','accountid','accountname','paymentamount','cardtypeid','cardtypename','bankname','referenceno','approvalcode','referencedate');
		$flabels = array('ChitEntry Detail Id','Stocktype ID','Payment Type','Account ID','Payment Account','Amount','Card Type ID','Card Type','Bank','Reference No','Approval Code','Reference Date');
		$colmodnames = array('chitentrydetailid','stocktypeid','stocktypename','accountid','accountname','paymentamount','cardtypeid','cardtypename','bankname','referenceno','approvalcode','referencedate');
		$colindexnames = array('chitentrydetail','chitentrydetail','chitentrydetail','chitentrydetail','chitentrydetail','chitentrydetail','chitentrydetail','chitentrydetail','chitentrydetail','chitentrydetail','chitentrydetail','chitentrydetail');
		$uitypes = array('2','2','2','2','2','2','2','2','2','2','2','2');
		$viewtypes = array('0','0','1','0','1','1','0','1','1','1','1','1');
		$dduitypeids = array('17','18','19','20','23','25','26','27','28','29');
		$data = array();
		$i=0;
		$j=0;
		foreach($fnames as $fname) {
			if(in_array($uitypes[$j],$dduitypeids)) {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j].'name';
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]='0';
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]='2';
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			} else {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			}
			$j++;
		}
		return $data;
	}
	/** *stocktypedropdown */
	public function stocktypedropdown() {
		$planid = $this->Basefunctions->planid;
		$userroleid = $this->Basefunctions->userroleid;
		$industryid = $this->Basefunctions->industryid;
		$data = $this->db->select('stocktype.stocktypeid,stocktype.stocktypename,stocktype.stocktypelabel,stocktype.shortname')
				->from('stocktype')
				->where("FIND_IN_SET('51',stocktype.moduleid) >", 0)
				->where("FIND_IN_SET('$userroleid',stocktype.userroleid) >", 0)
				->where("FIND_IN_SET('$planid',stocktype.planid) >", 0)
				->where("FIND_IN_SET('$industryid',stocktype.industryid) >", 0)
				->where('stocktype.status',$this->Basefunctions->activestatus)
				->order_by('stocktype.stocktypeid','asc')
				->get();
		return $data;
	}
}