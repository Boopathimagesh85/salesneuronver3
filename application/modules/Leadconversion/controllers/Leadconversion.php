<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Leadconversion extends CI_Controller {
	function __construct() {
    	parent::__construct();	
		$this->load->model('Leadconversion/Leadconversionmodel');
		$this->load->helper('formbuild');
		$this->load->model('Base/Basefunctions');
    }
	public function index() {
		$moduleid = array(254);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Leadconversion/leadconversionview',$data);
	}
	//Create New data
	public function newdatacreate(){  
    	$this->Leadconversionmodel->newdatacreatemodel();
    }
    //Delete New data
	public function deleteinformationdata(){  
    	$this->Leadconversionmodel->deleteinformationdatamodel();
    }
	//dropdown datafetch	
	public function fetchdddatawithcond(){  
    	$this->Leadconversionmodel->fetchdddatawithcondmodel();
    }
	public function accountfieldnamecheck(){  
    	$this->Leadconversionmodel->accountfieldnamecheckmodel();
    }
    public function checkfieldexist(){
    	$this->Leadconversionmodel->checkfieldexistmodel();
    }
    //Lead unique Field name check
    public function leaduniquefieldnamecheck(){
    	$this->Leadconversionmodel->leaduniquefieldnamecheckmodel();
    }
}