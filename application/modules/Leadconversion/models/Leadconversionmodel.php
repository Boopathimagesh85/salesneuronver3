<?php
Class Leadconversionmodel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->model('Base/Crudmodel');
    }
	public function simplegroupdropdown($did,$dname,$table,$whfield,$whdata,$uitype,$columnname) {
		$i=0;
		$this->db->select("$dname,$did,$uitype");
		$this->db->from($table);
		if($whdata != '') {
			$this->db->where_in($whfield,$whdata);
		}
		$this->db->where_not_in('uitypeid',array(15,16));
		$this->db->where_not_in('active','No');
		$this->db->where('status',1);
		$result = $this->db->get();
		return $result->result();
    }
	//new data add form
	public function newdatacreatemodel(){
		$cdate = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$status	= $this->Basefunctions->activestatus;
		$leadid = $_POST['leadfieldid'];
		$accountid = $_POST['accountfieldid'];
		if($accountid != ""){$accid = $accountid;}else{$accid = 1;}
		$contactid = $_POST['contactfieldid'];
		if($contactid != ""){$contid = $contactid;}else{$contid = 1;}
		$opportunityid = $_POST['opprtunityfieldid'];
		if($opportunityid != ""){$oppid = $opportunityid;}else{$oppid = 1;}
		$value = $this->checkleadfieldid($leadid);
		if($value == "False"){
			$leadmap = array(
					'leadid'=>$leadid,
					'accountid'=>$accid,
					'contactid'=>$contid,
					'opportunityid'=>$oppid,
					'industryid'=>$this->Basefunctions->industryid,
					'branchid'=>$this->Basefunctions->branchid,
					'createdate'=>$cdate,
					'lastupdatedate'=>$cdate,
					'createuserid'=>$userid,
					'lastupdateuserid'=>$userid,
					'status'=>$status
				);
				$this->db->insert('leadconversionmapping',$leadmap);
		} else { 
			$leadmap = array(
				'leadid'=>$leadid,
				'accountid'=>$accid,
				'contactid'=>$contid,
				'industryid'=>$this->Basefunctions->industryid,
				'branchid'=>$this->Basefunctions->branchid,
				'opportunityid'=>$oppid,
				'lastupdatedate'=>$cdate,
				'lastupdateuserid'=>$userid,
				'status'=>$status
			);
			$this->db->where('leadconversionmapping.leadconversionmappingid',$value);
			$this->db->update('leadconversionmapping',$leadmap);
		}
		echo "TRUE";
	}
	public function checkleadfieldid($leadid){
		$data ="False";
		$this->db->select('leadid,leadconversionmappingid');
		$this->db->from('leadconversionmapping');
		$this->db->where_in('leadconversionmapping.leadid',$leadid);
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				$data = $row->leadconversionmappingid;
			}
			return $data;
		} else{ 
			return $data; 
		}
	}
	public function checkfieldexistmodel(){
		$fieldname = $_GET['fieldname'];
		$fieldid = $_GET['fieldid'];
		if($fieldname == 'accountfieldid'){
			$fieldnewname = 'accountid';
		}elseif ($fieldname == 'contactfieldid'){
			$fieldnewname = 'contactid';
		}elseif ($fieldname == 'opprtunityfieldid'){
			$fieldnewname = 'opportunityid';
		}
		$this->db->select('leadconversionmappingid');
		$this->db->from('leadconversionmapping');
		$this->db->where($fieldnewname,$fieldid);
		$this->db->where_in('industryid',$this->Basefunctions->industryid);
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			echo "TRUE";
		}else{
			echo "FALSE";
		}
	}
	//delete information
	public function deleteinformationdatamodel(){
		$partabname = 'leadconversionmapping';
		$primaryname = 'leadconversionmappingid';
		$id = $_GET['primarydataid'];
		$ctable= '';
		$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
		echo "TRUE";
	}
	//drop down value fetch 
	public function fetchdddatawithcondmodel(){
		$id = $_GET['fieldid'];
		$name = $_GET['fieldname'];
		$tablename = $_GET['tablename'];
		$moduleid = $_GET['moduleid'];
		$value = $_GET['value'];
		$whfield = $_GET['moduletabid'];
		$i=0;
		$this->db->select("$name,$id");
		$this->db->from($tablename);
		if($moduleid != '') {
			$this->db->where_in($whfield,$moduleid);
		}
		$this->db->where_in('uitypeid',array($value,26));
		$this->db->where_not_in('uitypeid',array(15,16));
		$this->db->where_not_in('active','No');
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				$data[$i] = array('datasid'=>$row->$id,'dataname'=>$row->$name);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//account-contact-opportunity field name check
	public function accountfieldnamecheckmodel(){
		$fieldid = $_POST['fieldid'];
		$primaryid = $_POST['primaryid'];
		$tablename = $_POST['tablename'];
		$tableid = $_POST['tableid'];
		if($fieldid != "") {
			$result = $this->db->select($tableid)->from($tablename)->where($tablename.'.'.$tableid,$fieldid)->where($tablename.'.status',1)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->$tableid;
				} 
			} else { echo "False"; }
		}else { echo "False"; }
	}
	//Lead Unique Field name check
	public function leaduniquefieldnamecheckmodel(){
		$leadfieldid = $_POST['leadfieldid'];
		$industryid = $this->Basefunctions->industryid;
		if($leadfieldid != "") {
			$result = $this->db->select('leadconversionmapping.leadconversionmappingid')->from('leadconversionmapping')->where('leadconversionmapping.leadid',$leadfieldid)->where('leadconversionmapping.status',1)->where("FIND_IN_SET('$industryid',leadconversionmapping.industryid) >", 0)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->leadconversionmappingid;
				}
			} else {
				echo "False";
			}
		} else {
			echo "False";
		}
	}
}
?>