<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Picklist extends MX_Controller{
	function __construct() {
    	parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->helper('formbuild');
		$this->load->model('Picklist/Picklistmodel');
	}
	public function index() {
		$moduleid = array(211);
		sessionchecker(211);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$viewfieldsmoduleids = array(211);
		$viewmoduleid = array(211);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Picklist/picklistview',$data);
	}
	//data row sorting
	public function datarowsorting() {
		$this->Picklistmodel->datarowsortingmodel();
	}
	//new data create fun
	public function newdatacreate() {
		$this->Picklistmodel->newdatacreatemodel();
	}
	//fetch json data information
	public function dropdowntabdatafetch()	{
		$primarytable = $_GET['maintabinfo'];
		$fieldname = $primarytable.'name';
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$chkbox = $_GET['checkbox'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : $primarytable.'.'.$primaryid) : $primarytable.'.'.$primaryid;
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array($fieldname),'colmodelindex'=>array($primarytable.'.'.$fieldname),'coltablename'=>array($primarytable),'uitype'=>array('2'),'colname'=>array('Name'),'colsize'=>array('200'));
		$result=$this->Picklistmodel->dropdowntabdatafetchmodel($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$fieldname,$primaryid,$modid);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($result,$page,'');
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Picklist Value',$width,$height,$chkbox);
		} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Picklist Value',$width,$height,$chkbox);
		}
		echo json_encode($datas);
	}
	//picklist delete dunction
	public function deletepicklistvalue(){
		$tablenameid = $_POST['tabnameid'];
		$tablename = substr($tablenameid,0,-2);
		$selid = $_POST['id'];
		$this->Picklistmodel->deletepicklistvaluemodel($tablename,$tablenameid,$selid);
	}
	//edit pick list value - create user id check
	public function editpicklistuseridcheck(){
		$tablenameid = $_POST['tabnameid'];
		$tablename = substr($tablenameid,0,-2);
		$selid = $_POST['id'];
		$data = $this->Picklistmodel->createuseridcheck($tablename,$tablenameid,$selid);
		echo $data;
	}
	//picklist data updation
	public function picklistdataupdatefun(){
		$this->Picklistmodel->picklistdataupdatefunmodel();
	}
	//unique name restriction
	public function uniquedynamicviewnamecheck(){
		$moduleid=$_POST['moduleid'];
		$accname=trim($_POST['accname']);
		$elementpartableid = $_POST['elementspartabname'];
		$check = $this->Picklistmodel->uniquedynamicviewnamecheckmodel($moduleid,$accname,$elementpartableid);
		echo $check;
	}
}