<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<div class="large-12 columns headercaptionstyle headerradius">
				<div class="large-6 medium-6 small-12 columns headercaptionleft">
					<span class="fa fa-align-justify left-off-canvas-toggle headermainmenuicon"></span>
					<span class="gridcaptionpos"><?php echo span($maingridtitle,$spanattr); ?><span>-</span> <span id="viewgridheaderid"></span>
					</span>
				</div>
				<div class="large-6 medium-6 small-12 columns foriconcolor righttext small-only-text-center"> 
					<!-- Action menu generation -->
					<?php
						$size = 6;
						$value = array();
						$des = array();
						$title = array();
						foreach($actionassign as $key):
							$value[$key->toolbarnameid] = $key->toolbarname;
							$des[] = $key->description;
							$title[] = $key->toolbartitle;
						endforeach;
						echo actionmenu($value,$size,$des,$title,'picklist','icon-w ');
						//user menu
						$this->load->view('Base/usermenugenerate');
					?>
				</div>
			</div>
			<!-- tab group creation -->
			<div class="large-12 columns tabgroupstyle">
				 <?php
					$ulattr = array("class"=>"tabs");
					$uladdinfo = "data-tab";
					$tabgrpattr = array("class"=>"tab-title sidebaricons");
					$tabstatus = "active";
					$dataname = "subform";
					echo tabgroupgenerate($ulattr,$uladdinfo,$tabgrpattr,$tabstatus,$dataname,$modtabgrp);
				?>
			</div>
			<?php
				$this->load->view('Base/formfieldgeneration');
				//function call for form fields generation
				formfieldsgeneratorwithgrid($modtabsecfrmfkdsgrp,$filedmodids,'');
			?>
		</div>
	</div>
</div>