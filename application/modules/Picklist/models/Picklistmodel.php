<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Model File
class Picklistmodel extends CI_Model{
    public function __construct() {
		parent::__construct(); 
		$this->load->model('Base/Crudmodel');
    }
	//data sort order
	public function datarowsortingmodel() {
		$rowids = $_POST['rowids'];
		$tablename = $_POST['tablename'];
		$tableid = $_POST['primaryid'];
		$sortfield = $_POST['sortfield'];
		$dataids = explode(',',$rowids);
		$i=1;
		foreach($dataids as $id) {
			$this->db->query("UPDATE"." ".$tablename." SET ".$tablename.".".$sortfield."=".$i." WHERE ".$tablename.".".$tableid."=".$id );
			$i++;
		}
		echo "TRUE";
	}
	//data create model
	public function newdatacreatemodel() {
		$tablenameid = $_POST['tabnameid'];
		$tablename = substr($tablenameid,0,-2);
		$roleid = $this->fetchallroleids();
		$roleids = implode(',',$roleid);
		$moduleid = $_POST['picklistmoduleid'];
		$picklistname = trim($_POST['picklistvalue']);
		$check = $this->dynamicviewnamecheckmodel($moduleid,$picklistname,$tablenameid);
		if($check == 'False') {
			$dataarray = array(
				''.$tablename.'name'=>trim($_POST['picklistvalue']),'moduleid'=>$_POST['picklistmoduleid'],'userroleid'=>$roleids,'setdefault'=>'0','sortorder'=>'0'
			);
			$defdataarr = $this->Crudmodel->defaultvalueget();
			$newdata = array_merge($dataarray,$defdataarr);
			$newdata['industryid']=$this->Basefunctions->industryid;
			$this->db->insert( $tablename,$newdata);
			$primaryid = $this->db->insert_id(); //primary id
			$primaryname = $tablename.'id';
			$this->db->query(" UPDATE ".$tablename." SET sortorder = sortorder+".$primaryid." WHERE ".$primaryname." = ".$primaryid." ");
		} else {
			$pid = $check;
			$dataarray = array(
				''.$tablename.'name'=>trim($_POST['picklistvalue']),'moduleid'=>$_POST['picklistmoduleid'],'userroleid'=>$roleids,'setdefault'=>'0','sortorder'=>'0','status'=>'1'
			);
			$defdataarr = $this->Crudmodel->updatedefaultvalueget();
			$newdata = array_merge($dataarray,$defdataarr);
			$this->db->where($tablename.'.'.$tablenameid,$pid);
			$this->db->update($tablename,$newdata);
			$primaryname = $tablename.'id';
			$this->db->query(" UPDATE ".$tablename." SET sortorder = sortorder+".$pid." WHERE ".$primaryname." = ".$pid." ");
		}
		echo 'TRUE';
	}
	//fetch all role ids
	public function fetchallroleids() {
		$roleids = array();
		$i=0;
		$roledata = $this->db->select('userroleid')->from('userrole')->where('status',1)->get();
		foreach($roledata->result() as $data) {
			$roleids[$i]=$data->userroleid;
			$i++;
		}
		return $roleids;
	}
	public function dropdowntabdatafetchmodel($tablename,$sortcol,$sortord,$pagenum,$rowscount,$fieldname,$primaryid,$moduleid) {
		$data = array('','','');
		if($tablename != "") {
			$industryid = $this->Basefunctions->industryid;
			$dataset = $tablename.'id,'.$tablename.'name,setdefault';
			$userroleid = $this->Basefunctions->userroleid;
			$status = $tablename.'.status = 1';
			$module = " FIND_IN_SET( $moduleid, $tablename.moduleid )";
			$role = " FIND_IN_SET( $userroleid, $tablename.userroleid )";
			$industry = " FIND_IN_SET( $industryid, $tablename.industryid ) > 0";
			$join = ' LEFT OUTER JOIN module ON module.moduleid='.$tablename.'.moduleid';
			/* pagination */
			$pageno = $pagenum-1;
			$start = $pageno * $rowscount;
			//query
			$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$status.' AND '.$module.' AND '.$industry.' AND '.$role.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		}
		return $data;
	}
	//pick list delete function
	public function deletepicklistvaluemodel($tablename,$tablenameid,$selid) {
		$date = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$check = $this->createuseridcheck($tablename,$tablenameid,$selid);
		if($check == "TRUE") {
			$ststus = $tablename.".status=0";
			$updatedate =  $tablename.".lastupdatedate='".$date."'";
			$updateuser =  $tablename.".lastupdateuserid=".$userid;
			$update = $ststus.','.$updatedate.','.$updateuser;
			$value = $this->db->query("UPDATE"." ".$tablename." SET ".$update." WHERE ".$tablename.".".$tablenameid." IN (".$selid.")" );
			echo 'TRUE';
		} else {
			echo 'FALSE';
		}
	}
	//pick list updation
	public function picklistdataupdatefunmodel() {
		$pid = $_POST['id'];
		$tablenameid = $_POST['tabnameid'];
		$tablename = substr($tablenameid,0,-2);
		$roleid = $this->fetchallroleids();
		$roleids = implode(',',$roleid);
		$dataarray = array(
			''.$tablename.'name'=>trim($_POST['picklistvalue']),'moduleid'=>$_POST['picklistmoduleid'],'userroleid'=>$roleids,'setdefault'=>'0','sortorder'=>'0'
		);
		$defdataarr = $this->Crudmodel->updatedefaultvalueget();
		$newdata = array_merge($dataarray,$defdataarr);
		$this->db->where($tablename.'.'.$tablenameid,$pid);
		$this->db->update($tablename,$newdata);
		echo "TRUE";
	}
	//unique name check
	public function uniquedynamicviewnamecheckmodel($moduleid,$accname,$elementpartableid){
		$partable = substr($elementpartableid, 0, -2);
		$ptid = $partable.'id';
		$ptname = $partable.'name';
		if($accname != "") {
			$result = $this->db->select($ptname.','.$ptid)->from($partable)->where($partable.'.'.$ptname,$accname)->where("FIND_IN_SET(".$moduleid.",".$partable.".moduleid) >", 0)->where($partable.'.status',1)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					return $row->$ptid;
				} 
			} else { return "False"; }
		} else { return "False"; }
	}
	public function dynamicviewnamecheckmodel($moduleid,$accname,$elementpartableid){
		$partable = substr($elementpartableid, 0, -2);
		$ptid = $partable.'id';
		$ptname = $partable.'name';
		if($accname != "") {
			$result = $this->db->select($ptname.','.$ptid)->from($partable)->where($partable.'.'.$ptname,$accname)->where($partable.'.moduleid',$moduleid)->where_in($partable.'.status',array(1,0))->order_by($partable.'.'.$ptid,"desc")->limit(1, 0)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					return $row->$ptid;
				} 
			} else { return "False"; }
		} else { return "False"; }
	}
	//create user id check
	public function createuseridcheck($tablename,$tablenameid,$selid) {
		$data ='1';
		$ids = explode(',',$selid);
		$this->db->select($tablename.'.createuserid');
		$this->db->from($tablename);
		$this->db->where_in($tablename.'.'.$tablenameid,$ids);
		$result = $this->db->get();
		foreach($result->result() as $row) {
			$data = $row->createuserid;
		}
		if($data != '1') {
			return "TRUE";
		} else {
			return "FALSE";
		}
	}
}