<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Massupdatedelete extends MX_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Massupdatedelete/Massupdatedeletemodel');
		$this->load->library('excel');
    }
    //first basic hitting view
    public function index() {
		$moduleid = array(252);
		sessionchecker($moduleid);
		//action
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(252);
		$viewmoduleid = array(252);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
      	$this->load->view('Massupdatedelete/massupdatedeleteview',$data);
	}
	//mass update default view fetch
	public function defaultviewfetch() {
		$mid = $_GET['modid'];
		$viewid = $this->Massupdatedeletemodel->defaultviewfetchmodel($mid);
		echo $viewid;
	}
	//parent table get
	public function parenttableget() {
		$this->Massupdatedeletemodel->parenttablegetmodel();
	}
	//mass delete
	public function massdeletefunction() {
		$this->Massupdatedeletemodel->massdeletefunctionmodel();
	}
	//show hide fields
	public function showhidefiels() {
		$this->Massupdatedeletemodel->showhidefielsmodel();
	}
	//mass update
	public function massupdatefunction() {
		$this->Massupdatedeletemodel->massupdatefunctionmodel();
	}
	//field name based drop down value - picklist
	public function fieldnamebesdpicklistddvalue() {
		$this->Massupdatedeletemodel->fieldnamebesdpicklistddvaluemodel();
	}
	//field name based drop down value - main drop down
	public function fieldnamebesdddvalue() {
		$this->Massupdatedeletemodel->fieldnamebesdddvaluemodel();
	}
	//field name based drop down value
	public function fieldviewnamebespicklistdddvalue() {
		$this->Massupdatedeletemodel->fieldviewnamebespicklistdddvaluemodel();
	}
	//field name based drop down value
	public function viewfieldnamebesdddvalue() {
		$this->Massupdatedeletemodel->viewfieldnamebesdddvaluemodel();
	}
	//field name value fetch
	public function fetchmaildddatawithmultiplecond() {
        $this->Massupdatedeletemodel->fetchmaildddatawithmultiplecondmodel();
    }
	//fetch json data information
	public function gridvalinformationfetch() {
		$creationid = $_GET['viewid'];
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$viewcolmoduleids = $_GET['viewfieldids'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$footer = isset($_GET['footername']) ? $_GET['footername'] : '';
		$sortcol = isset($_GET['sortcol']) ? $_GET['sortcol'] : '';
		$sortord = isset($_GET['sortord']) ? $_GET['sortord'] : '';
		$result=$this->Massupdatedeletemodel->viewdynamicdatainfofetch($creationid,$primaryid,$viewcolmoduleids,$sortcol,$sortord,$pagenum,$rowscount);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobileviewgriddatagenerate($result,$primarytable,$primaryid,$width,$height,$checkbox='true');
		} else {
			$datas = viewgriddatagenerate($result,$primarytable,$primaryid,$width,$height,$footer,$checkbox='true');
		}
		echo json_encode($datas);
	}
	//view drop down load-
	public function viewdropdownload() {
		$this->Massupdatedeletemodel->viewdropdownloadmodel();
	}
	//check box value get
	public function newcheckboxvalueget() {
		$this->Massupdatedeletemodel->checkboxvaluegetmodel();
	}
	//excel data export
	public function exceldataexport() {
		$this->Massupdatedeletemodel->exceldataexportmodel();
	}
	//export default column info fetch
	public function exportcoldatafetch() {
		$mid = $_GET['modid'];
		$viewid = $this->Massupdatedeletemodel->defaultviewfetchmodel($mid);
		$viewcolids = $this->Massupdatedeletemodel->viewcreationcolumnids($viewid);
		echo $viewcolids;
	}
	//View drop down load function
	public function viewdropdownloadfun() {
		$this->Massupdatedeletemodel->viewdropdownloadfunmodel();
	}
}