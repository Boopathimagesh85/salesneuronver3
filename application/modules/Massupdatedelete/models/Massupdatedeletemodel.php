<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Massupdatedeletemodel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//mass update default view fetch
	public function defaultviewfetchmodel($mid){
		$data = "";
		$this->db->select('viewcreationid');
		$this->db->from('viewcreation');
		$this->db->where('viewcreation.viewcreationmoduleid',$mid);
		$this->db->where('viewcreation.createuserid',1);
		$this->db->where('viewcreation.lastupdateuserid',1);
		$this->db->where('viewcreation.status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row){
				$data = $row->viewcreationid;
			}
		}
		return $data;
	}
	//parent table name fetch
	public function parenttablegetmodel() {
		$mid = $_GET['moduleid'];
		$this->db->select('modulefield.parenttable,module.modulename');
		$this->db->from('modulefield');
		$this->db->join('module','module.moduleid=modulefield.moduletabid');
		$this->db->where('modulefield.moduletabid',$mid);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$ptable = $row->parenttable;
				$modname = strtolower(preg_replace('/[^A-Za-z0-9]/', '',$row->modulename));
			}
			echo json_encode(array('table'=>$ptable,'modname'=>$modname));
		} else {
		   echo json_encode(array('table'=>''));
		}
	}
	//mass delete
	public function massdeletefunctionmodel(){
		$moduleid = $_GET['moduleid'];
		$modulename = $_GET['modulename'];
		$parenttable = $_GET['parenttable'];
		$parentid = $parenttable.'id';
		$ids = $_GET['ids'];
		$explode = explode(',',$ids);
		$count =count($explode);
		$userid = $this->Basefunctions->userid;
		$cdate = date($this->Basefunctions->datef);
		$this->db->query("UPDATE"." ".$parenttable." SET "."status = 0,lastupdatedate='".$cdate."',lastupdateuserid=".$userid." WHERE ".$parenttable.".".$parentid." "."IN "."(".$ids.")" );
		//notification trigger in mass delete
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		$notimsg = $empname." "."MassDeleted".$count." - ".$modulename;
		$this->Basefunctions->notificationcontentadd($explode[0],'MassDeleted',$notimsg,1,$moduleid);
		echo "TRUE";
	}
	//show hide function ui-type value get
	public function showhidefielsmodel(){
		$fieldid = $_GET['fieldid'];
		$this->db->select('uitypeid');
		$this->db->from('modulefield');
		$this->db->where('modulefield.modulefieldid',$fieldid);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				$data = $row->uitypeid;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//mass update function
	public function massupdatefunctionmodel() {
		$ddvalue = $_GET['ddvalue'];
		$moduleid = $_GET['moduleid'];
		$modulename = $_GET['modulename'];
		$uitype = $_GET['uitype'];
		$parenttable = $_GET['parenttable'];
		$parentid = $parenttable.'id';
		$ids = $_GET['ids'];
		$explode = explode(',',$ids);
		$count =count($explode);
		if($uitype == '17' || $uitype == '18' || $uitype == '18' || $uitype == '19'|| $uitype == '20' || $uitype == '25' || $uitype == '26' || $uitype == '27') {
			$fieldname = $_GET['fieldname'];
			$table = substr($fieldname, 0, -4);
			$field = $table.'id';
			$where = " WHERE ".$parenttable.".".$parentid." "."IN "."(".$ids.")";
		} else if($uitype == '21'){
			$fieldname = $_GET['fieldname'];
			$table = substr($fieldname, 0, -4);
			$field = 'parent'.$table.'id';
			$where = " WHERE ".$parenttable.".".$parentid." "."IN "."(".$ids.")";
		} else if($uitype == '28') {
			$fieldname = $_GET['fieldname'];
			$aaa = explode(' ',$ddvalue);
			if($aaa[0] == 'Client') {
				$table = substr($fieldname, 0, -4);
				$field = 'designationtypeid';
				$where = " WHERE ".$parenttable.".".$parentid." "."IN "."(".$ids.")";
			}else{ 
				$table = substr($fieldname, 0, -4);
				$field = 'companypersondesignationid';
				$where = " WHERE ".$parenttable.".".$parentid." "."IN "."(".$ids.")"; }
		} else {
			$field = $_GET['fieldname'];
			$aaa = explode(' ',$ddvalue);
			if($aaa[0] == 'Billing' || $aaa[0] == 'Primary') {
				$tabename = $this->Basefunctions->generalinformaion('module','modulemastertable','moduleid',$moduleid);
				$childtable = $tabename.'address'; $pid = $tabename.'id';
				$where = " WHERE ".$childtable.".".$pid." "."IN "."(".$ids.")"." AND ".$childtable.".addressmethod=4";
				$parenttable = $childtable;
			} else if($aaa[0] == 'Shipping' || $aaa[0] == 'Secondary'){
				$tabename = $this->Basefunctions->generalinformaion('module','modulemastertable','moduleid',$moduleid);
				$childtable = $tabename.'address';$pid = $tabename.'id';
				$where = " WHERE ".$childtable.".".$pid." "."IN "."(".$ids.")"." AND ".$childtable.".addressmethod=5";
				$parenttable = $childtable;
			} else{
				$where = " WHERE ".$parenttable.".".$parentid." "."IN "."(".$ids.")";
			}
		}
		$fieldvalue = $_GET['fieldvalue'];
		$value = $field."='".$fieldvalue."'";
		$userid = $this->Basefunctions->userid;
		$cdate = date($this->Basefunctions->datef);
		$this->db->query("UPDATE"." ".$parenttable." SET ".$value.",lastupdatedate='".$cdate."',lastupdateuserid=".$userid." ".$where);
		//notification trigger in mass update
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		$notimsg = $empname." "."Mass Updated ".$count." - ".$modulename;
		$this->Basefunctions->notificationcontentadd($explode[0],'MassUpdate',$notimsg,1,$moduleid);
		echo "TRUE";
	}
	//field name based drop down value get - picklist 
	public function fieldnamebesdpicklistddvaluemodel(){
		$i=0;
		$moduleid = 'moduleid';
		$fieldname = $_GET['fieldid'];
		$mid = $_GET['moduleid'];
		$industryid = $this->Basefunctions->industryid;
		$table = substr($fieldname, 0, -4);
		$fieldid = $table.'id';
		$this->db->select($fieldid.','.$fieldname);
		$this->db->from($table);
		$this->db->where("FIND_IN_SET($mid,$table."."$moduleid) >", 0);
		$this->db->where("FIND_IN_SET($industryid,industryid) >", 0);
		$this->db->where('status',1);
		$reult =$this->db->get();
		if($reult->num_rows() > 0){
			foreach($reult->result() as $row){
				$data[$i] = array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//field name based drop down value get - main drop down
	public function fieldnamebesdddvaluemodel(){
		$i=0;
		$industryid = $this->Basefunctions->industryid;
		$fieldname = $_GET['fieldid'];
		$table = substr($fieldname, 0, -4);
		$fieldid = $table.'id';
		$this->db->select($fieldid.','.$fieldname);
		$this->db->from($table);
		$this->db->where("FIND_IN_SET($industryid,industryid) >", 0);
		$this->db->where('status',1);
		$reult =$this->db->get();
		if($reult->num_rows() > 0){
			foreach($reult->result() as $row){
				$data[$i] = array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//field name based drop down value get
	public function viewfieldnamebesdddvaluemodel(){
		$i=0;
		$moduleid = $_GET['moduleid'];
		$fieldname = $_GET['fieldname'];
		if($moduleid == 216 || $moduleid == 217 || $moduleid == 225 || $moduleid == 226){
			$table = substr($fieldname, 0, -6);
			$fieldid = $table.'id';
		} else {
			$table = substr($fieldname, 0, -4);
			$fieldid = $table.'id';
		}
		$this->db->select($fieldid.','.$fieldname);
		$this->db->from($table);
		if($moduleid == 205){
			$this->db->where('templatetypeid',2);
		}
		$this->db->where_in('industryid',$this->Basefunctions->industryid);
		$this->db->where('status',1);
		$reult =$this->db->get();
		if($reult->num_rows() > 0){
			foreach($reult->result() as $row){
				$data[$i] = array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//field name based drop down value get
	public function fieldviewnamebespicklistdddvaluemodel(){
		$i=0;
		$moduleid = 'moduleid';
		$mid = $_GET['moduleid'];
		$industryid = $this->Basefunctions->industryid;
		$fieldname = $_GET['fieldname'];
		$table = substr($fieldname, 0, -4);
		$fieldid = $table.'id';
		$this->db->select($fieldid.','.$fieldname);
		$this->db->from($table);
		$this->db->where("FIND_IN_SET($mid,$table."."$moduleid) >", 0);
		$this->db->where("FIND_IN_SET($industryid,industryid) >", 0);
		$this->db->where('status',1);
		$reult =$this->db->get();
		if($reult->num_rows() > 0){
			foreach($reult->result() as $row){
				$data[$i] = array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	 //fetch col name & col model information fetch model
    public function viewcreationcolumnids($creationid) {
		//fetch show col ids
		$this->db->select('viewcreation.viewcreationcolumnid,viewcreation.viewcreationid',false);
		$this->db->from('viewcreation');
		$this->db->where_in('viewcreation.viewcreationid',$creationid);
		$this->db->where('viewcreation.status',1);
		$result=$this->db->get()->result();
		foreach($result as $row) {
			$colids=$row->viewcreationcolumnid;
		}
		return json_encode($colids);
	}
	//check box value get
	public function checkboxvaluegetmodel() {
		$checkdata = 'Yes,No';
		$checkid = '1,2';
		$cdata = explode(',',$checkdata);
		$cid = explode(',',$checkid);
		for($i=0;$i<count($cdata);$i++){
			$data[$i] = array('datasid'=>$cid[$i],'dataname'=>$cdata[$i]);
		}
		echo json_encode($data);
	}
	//drop down value set with multiple condition
	public function fetchmaildddatawithmultiplecondmodel() {
		$i=0;
		$dname=$_GET['dataname'];
		$did=$_GET['dataid'];
		$dataattrname1=$_GET['othername1'];
		$dataattrname2=$_GET['othername2'];
		$table=$_GET['datatab'];
		$whfield=$_GET['whdatafield'];
		$whdata=$_GET['whval'];
		$mulcond=explode(",",$whfield);
		$mulcondval=explode(",",$whdata);
		$i=0;
		$this->db->select("$dname,$did,$dataattrname1,$dataattrname2");
		$this->db->from($table);
		foreach($mulcond AS $condname) {
			if($mulcondval[$i] != "0" && $mulcondval[$i] != "") {
				$this->db->where($condname,$mulcondval[$i]);
			}
			$i++;
		}
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {		
			foreach($result->result()as $row) {
				$data[$i]=array('datasid'=>$row->$did,'dataname'=>$row->$dname,'otherdataattrname1'=>$row->$dataattrname1,'otherdataattrname2'=>$row->$dataattrname2);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//view drop down data fetch function
	public function viewdropdownloadmodel() {
		$i=0;
		$ids = $_GET['ids'];
		$autonum = $_GET['autonum'];
		$this->db->select('viewcreationcolumnid,viewcreationcolmodeljointable,viewcreationcolmodelindexname,viewcreationcolumnname,uitypeid,moduletabsectionid,viewcreationparenttable,fieldrestrict');
		$this->db->from('viewcreationcolumns');
		$this->db->where('viewcreationcolumns.viewcreationmoduleid',$ids);
		if($autonum != 'yes') {
			$this->db->where_not_in('viewcreationcolumns.uitypeid',array(14));
		}
		$this->db->where_not_in('viewcreationcolumns.fieldrestrict',array(1));
		$this->db->where_in('viewcreationcolumns.viewcreationcolumnviewtype',array(1));
		$this->db->where('status',1);
		$result= $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('jointable'=>$row->viewcreationcolmodeljointable,'indexname'=>$row->viewcreationcolmodelindexname,'datasid'=>$row->viewcreationcolumnid,'dataname'=>$row->viewcreationcolumnname,'uitype'=>$row->uitypeid,'modtabsec'=>$row->moduletabsectionid,'parenttable'=>$row->viewcreationparenttable);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//fetch grid data information
    public function viewdynamicdatainfofetch($creationid,$primaryid,$viewcolmoduleids,$sortcol,$sortord,$pagenum,$rowscount) {
		$whereString="";
		$groupid="";
		$extracolinfo = array();
		$multiarray = array();
		$j=0;
		$count = $_GET['cricount'];
		$griddata = $_GET['griddata'];
		$formdata=json_decode( $griddata, true );
		for ($i=0;$i < $count;$i++) {
			$extracolinfo[$j] = $formdata[$i]['fieldid'];
			$multiarray[$j]=array(
				'0'=>$formdata[$i]['jointable'],
				'1'=>$formdata[$i]['indexname'],
				'2'=>$formdata[$i]['massconditionidname'],
				'3'=>$formdata[$i]['critreiavalue'],
				'4'=>$formdata[$i]['massandorcondidname'],
			);
			$j++;
		}
		//grid column title information fetch
		if($extracolinfo == "") {
			$colinfo = $this->Basefunctions->gridinformationfetchmodel($creationid);
		} else {
			$colinfo = $this->extragridinformationfetchmodel($creationid,$extracolinfo);
		}
		//header colids
		$headcolids = $this->Basefunctions->mobiledatarowheaderidfetch($creationid);
		//view moduleid
		$viewmodid = $this->Basefunctions->mobiledataviewmoduleidfetch($creationid);
		 //main table
		$maintable = $_GET['maintabinfo'];
		$selvalue = $maintable.'.'.$primaryid;
		$in = 0;
		//generate joins
		$counttable=0;
		if(count($colinfo)>0) {
			$counttable=count($colinfo['coltablename']);
		}
		$jtab = array($maintable);
		$ptab = array($maintable);
		$joinq = "";
		//attribute where condition
		$attrcond = "";
		$attrcondarr = array();
		$x = 0;
		$con = "";
		for($k=0;$k<$counttable;$k++) {
			if($x != 0) { $con = "AND"; }
			//parent join check
			$ptabjoin = "";
			$ptabjoin = $colinfo['colmodelparenttable'][$k];
			if(in_array($ptabjoin,$ptab)) {
				if(!in_array($colinfo['coltablename'][$k],$jtab) && $colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
					array_push($jtab,trim($colinfo['coltablename'][$k]));
					//employee
					$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid=1';
					//group
					$joinq .= ' LEFT OUTER JOIN employeegroup ON FIND_IN_SET(employeegroup.employeegroupid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid=2';
					//user role and sub ordinate
					$joinq .= ' LEFT OUTER JOIN userrole ON FIND_IN_SET(userrole.userroleid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND ('.$maintable.'.employeetypeid=3 OR '.$maintable.'.employeetypeid=4)';
				} else {
					if($colinfo['colmodeldatatype'][$k] == 1) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
							}
						}
					} else if($colinfo['colmodeldatatype'][$k] == 2) {
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else if($colinfo['colmodeldatatype'][$k] == 3) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0';
							}
						}
					}
				}
			} else {
				if($colinfo['colmodeldatatype'][$k] == 1) {
					$result = $this->Basefunctions->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
							}
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.','.$maintable.'.'.$primaryid.') > 0';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 1) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
									}
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 2) {
					$joinresult = $this->Basefunctions->parentjointableinfo($ptabjoin,$viewcolmoduleids);
					if($joinresult->num_rows() >0) {
						foreach($joinresult->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
							//condition name
							$condcolname = $show->viewcreationcolmodelcondname;
							//condition value
							$condcolvalue = $show->viewcreationcolmodelcondvalue;
						}
						//restrict repeated parent join
						if(in_array($destab,$ptab)) {
							if($destab != $soucrcetab) {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0';
								}
							} else {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0';
								}
							}
						}
						//parent table condition
						if($condcolname != "" && $condcolvalue != "") {
							$attrcond .= " ".$con." ".$srcjointab.'.'.$condcolname ."=". $condcolvalue;
							array_push($attrcondarr,$srcjointab.'.'.$joincolname);
							$x = 1;
						}
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] = $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.'='.$maintable.'.'.$primaryid.') > 0';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 2) {
								//attribute join
								if(!in_array($colinfo['coltablename'][$k],$jtab)) {
									array_push($jtab,trim($colinfo['coltablename'][$k]));
									$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0';
								}
								//condition
								if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
									$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
									array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
									$x = 1;
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 3) {
					$result = $this->Basefunctions->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0';
							}
						}
					}  else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.','.$maintable.'.'.$primaryid.') > 0';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 3) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0';
									}
								}
							}
						}
					}
				}
			}
			$dduitypeids = array('17','18','19','23','25','26','27','28','29');
			//fields fetch
			if($colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->Basefunctions->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].', GROUP_CONCAT(DISTINCT userrole.userrolename) AS rolename, GROUP_CONCAT(DISTINCT employeegroup.employeegroupname) AS empgrpname';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k].',userrole.userrolename AS rolename,employeegroup.employeegroupname AS empgrpname';
				}
			} else if( in_array($colinfo['colmodeluitype'][$k],$dduitypeids) ) {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->Basefunctions->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].'';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
				}
			} else {
				$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
			}
		}
		//attr condition generate
		for($xi=0; $xi < ( count($attrcondarr)-1 ); $xi++ ) {
			$attrcond .= " AND ".$attrcondarr[0].'='.$attrcondarr[$xi+1];
		}
		if( $attrcond == "" ) { $attrcond = "1=1";}
		//custom condition generate
		$cuscondition = " AND 1=1";
		if( isset($_GET['cuscondfieldsname']) ) {
			$fieldsname = explode(',',$_GET['cuscondfieldsname']);
			$condvalues = explode(',',$_GET['cuscondvalues']);
			$condtables = explode(',',$_GET['cuscondtablenames']);
			$condjoinid = explode(',',$_GET['cusjointableid']);
			$m=0;
			foreach($fieldsname AS $values) {
				if($condvalues[$m] != "") {
					if(in_array($condtables[$m],$ptab)) {
						$tabname = explode('.',$values);
						//restrict repeated join
						if(!in_array($condtables[$m],$jtab)) {
							array_push($jtab,$condtables[$m]);
							$joinq=$joinq.' LEFT OUTER JOIN '.$condtables[$m].' ON '.$tabname[0].'.'.$condjoinid[$m].'='.$maintable.'.'.$condjoinid[$m];
						}
					}
					$cuscondition .= " AND ".$values."=".$condvalues[$m];
					$m++;
				}
			}
		}
		//generate condition
		$ewh = 'AND 1=1';
		$conditionvalarray = $this->Basefunctions->conditioninfofetch($creationid);
		$c = count($conditionvalarray);
		$d = count($multiarray);
		if($c>0) {
			$whereString=$this->whereclausegeneration($conditionvalarray);
		} else {
			$whereString=$ewh;
		}
		if($d>0) {
			$extrawhereString=$this->extrawhereclausegeneration($multiarray,$extracolinfo);
		} else {
			$extrawhereString=$ewh;
		}
		if($maintable == 'employee') {
			$status = $maintable.'.status NOT IN (3)';
		} else {
			$status = $maintable.'.status IN (1)';
		}
		/* column sorting */
		$order = '';
		if($sortcol!='' && $sortord!='') {
			$order = 'ORDER BY'.' '.$sortcol.' '.$sortord;
		} else {
			$order = 'ORDER BY'.' '.$maintable.'.'.$maintable.'id DESC';
		}
		$actsts = $this->Basefunctions->activestatus;
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		$li= 'LIMIT '.$start.','.$rowscount;
		/* query statements */
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$selvalue.' from '.$maintable.' '.$joinq.' WHERE '.$attrcond.' '.$whereString.' '.$extrawhereString.' '.$cuscondition.' AND '.$status.' GROUP BY '.$maintable.'.'.$maintable.'id '.$order.' '.$li);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array('0'=>$colinfo,'1'=>$data,'2'=>$page,'3'=>array(),'4'=>$headcolids,'5'=>$viewmodid,'6'=>$creationid);
		return $finalresult;
    }
	//where condition generation
    public function whereclausegeneration($conditionvalarray) {
		$whereString="";
		$m=0;
		foreach ($conditionvalarray as $key) {
			if($m == 0) {
				$key[4] = "";
				$whereString .= "AND (";
			} else {
				if($key[4] != 'AND' && $key[4] != 'OR') {
					$key[4] = 'AND';
				}
			}
			$whereString.=$this->conditiongenerate($key);
			$m++;
		}
		if($whereString !="" ) {
			$whereString .= " )";
		}
		return $whereString;
    }
	//where condition generation
    public function extrawhereclausegeneration($conditionvalarray,$extracolinfo) {
    	$whereString="";
    	$m=0;
    	$count = count($conditionvalarray);
    	$braces = '';
    	foreach ($conditionvalarray as $key) {
    		$uitype = $this->Basefunctions->generalinformaion('viewcreationcolumns','uitypeid','viewcreationcolumnid',$extracolinfo[$m]);
    		if($uitype == 8) {
    			$key[3] =  $this->Basefunctions->ymddateconversion($key[3]);
    		} else {
    			$key[3] = $key[3];
    		}
    		if($m == 0) {
    			$key[4] = "";
    			$whereString .= " AND ".str_repeat("",$count-1)."";
    		} else {
    			if($key[4] != 'AND' and $key[4] != 'OR') {
    				$key[4] = 'AND';
    			}
    		}
    		if($m >= 1){
    			$braces = '';
    		}
    		$whereString.=$this->conditiongenerate($key,$braces);
    		$m++;
    	}
    	if($whereString !="" ) {
    		$whereString .= "";
    	}
    	return $whereString;
    }
	//where condition generation
    public function viewwhereclausegeneration($conditionvalarray) {
		$whereString="";
		$m=0;
		foreach ($conditionvalarray as $key) {
			if($m == 0) {
				$key[4] = "";
				$whereString .= " AND (";
			} else {
				if($key[4] != 'AND' and $key[4] != 'OR') {
					$key[4] = 'AND';
				}
			}
			if( $key[5] == 20 ) {
				//employee
				$whereString.=$this->conditiongenerate($key);
				//group
				$gkey = array(0=>'employeegroup',1=>'employeegroupname',2=>$key[2],3=>$key[3],4=>'OR');
				$whereString.=$this->conditiongenerate($gkey);
				//user role
				$ukey = array(0=>'userrole',1=>'userrolename',2=>$key[2],3=>$key[3],4=>'OR');
				$whereString.=$this->conditiongenerate($ukey);
			} else {
				$whereString.=$this->conditiongenerate($key);
			}
			$m++;
		}
		if($whereString !="") {
			$whereString .= ")";
		}
		return $whereString;
    }
	//condition generation
	public function conditiongenerate($key) {
		switch($key[2]) {
			case "Equalto":
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." = '".$key[3]."'";
				break;
			case "NotEqual": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." != '".$key[3]."'";
				break;
			case "Startwith":
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." LIKE '".$key[3].'%'."'";
				break;
			case "Endwith": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." LIKE '".'%'.$key[3]."'";
				break;
			case "Middle": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." LIKE '".'%'.$key[3].'%'."'";
				break;
			case "IN": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." IN ('".$key[3]."')";
				break;
			case "NOT IN": 
				return $whereString.=" ".$key[4]." ".$key[0].'.'.$key[1]." NOT IN ('".$key[3]."')";
				break;
			case "GreaterThan": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." > '" .$key[3]."'";
				break;
			case "LessThan": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." < '" .$key[3]."'";
				break;
			case "GreaterThanEqual": 
				return $whereString= " ".$key[4]." ".$key[0].'.'.$key[1]." >= '" .$key[3]."'";
				break;
			case "LessThanEqual": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." <= '" .$key[3]."'";
				break;
			case "Like":
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." LIKE '" .$key[3]."'";
				break;
			case "NOT LIKE":
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." NOT LIKE '" .$key[3]."'";
				break;
			default:
				return $whereString="";
				break;
		}
	}
	//fetch colname & colmodel information fetch model
    public function extragridinformationfetchmodel($creationid,$extracolinfo) {
		//fetch show col ids
		$this->db->select('viewcreation.viewcreationcolumnid,viewcreation.viewcreationid,viewcreationmodule.viewcreationmoduleid,viewcreationmodule.viewcreationmodulename,viewcreation.viewcolumnsize',false);
		$this->db->from('viewcreation');
		$this->db->join('viewcreationmodule','viewcreationmodule.viewcreationmoduleid=viewcreation.viewcreationmoduleid');
		$this->db->where_in('viewcreation.viewcreationid',$creationid);
		$this->db->where('viewcreation.status',1);
		$result=$this->db->get()->result();
		foreach($result as $row) {
			$colids=$row->viewcreationcolumnid;
			$modname = $row->viewcreationmodulename;
			$modid = $row->viewcreationmoduleid;
			$colsize = $row->viewcolumnsize;
		}
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$headcolids = $this->Basefunctions->mobiledatarowheaderidfetch($creationid);
			$vwcolids = explode(',',$colids);
			$vwmgcolids = array_merge($vwcolids,$headcolids);
			$viewcolids = array_unique($vwmgcolids);
		} else {
			$viewcolids = explode(',',$colids);
		}
		/* for fetch colname & colmodel fetch */
		$i=0;
		$m=0;
		$data = array();
		$colsizes = explode(',',$colsize);
		foreach($viewcolids as $colid) {
			$this->db->select("viewcreationcolumns.viewcreationcolumnid,
								viewcreationcolumns.viewcreationcolumnname,
								viewcreationcolumns.viewcreationcolmodelname,
								viewcreationcolumns.viewcreationcolmodelindexname,
								viewcreationcolumns.viewcreationcolmodeljointable,
								viewcreationcolumns.viewcreationcolumnid,
								viewcreationcolumns.viewcreationcolmodelaliasname,
								viewcreationcolumns.viewcreationcolmodeltable,
								viewcreationcolumns.viewcreationparenttable,
								viewcreationcolumns.viewcreationmoduleid,
								viewcreationcolumns.viewcreationjoincolmodelname,
								viewcreationcolumns.viewcreationcolumnviewtype,
								viewcreationcolumns.viewcreationtype,
								viewcreationcolumns.viewcreationcolmodelcondname,
								viewcreationcolumns.uitypeid,
								viewcreationcolumns.moduletabsectionid,
								viewcreationcolumns.viewcreationcolmodelcondvalue,
								viewcreationcolumns.viewcreationcolumnicon");
			$this->db->from('viewcreationcolumns');
			$this->db->where('viewcreationcolumns.viewcreationcolumnid',$colid);
			$this->db->where('viewcreationcolumns.status',1);
			$showfield = $this->db->get();
			if($showfield->num_rows() >0) {
				foreach($showfield->result() as $show) {
					$data['colid'][$i]=$show->viewcreationcolumnid;
					$data['colname'][$i]=$show->viewcreationcolumnname;
					$data['colicon'][$i]=$show->viewcreationcolumnicon;
					$data['colsize'][$i]=$colsizes[$m];
					$data['colmodelname'][$i]=$show->viewcreationcolmodelname;
					$data['colmodelaliasname'][$i]=$show->viewcreationcolmodelaliasname;
					$data['coltablename'][$i]=$show->viewcreationcolmodeltable;
					$data['coljointablename'][$i]=$show->viewcreationcolmodeljointable;
					$data['coljoinmodelname'][$i]=$show->viewcreationjoincolmodelname;
					$data['colmodelindex'][$i]=$show->viewcreationcolmodeljointable .'.'.$show->viewcreationcolmodelindexname;
					$data['colmodelparenttable'][$i]=$show->viewcreationparenttable;
					$data['colmodelviewtype'][$i]=$show->viewcreationcolumnviewtype;
					$data['colmodeldatatype'][$i]=$show->viewcreationtype;
					$data['colmodelcondname'][$i]=$show->viewcreationcolmodelcondname;
					$data['colmodelcondvalue'][$i]=$show->viewcreationcolmodelcondvalue;
					$data['colmodelmoduleid'][$i]=$show->viewcreationmoduleid;
					$data['colmodeluitype'][$i]=$show->uitypeid;
					$data['colmodelsectid'][$i]=$show->moduletabsectionid;
					$data['colmodeltype'][$i]='text';
					$data['modname'][$i] = $modname;
					$i++;
				}
			}
			$m++;
		}
		return $data;
    }
	 //fetch colname & colmodel information fetch based viewcolumnids 
    public function viewcolumndatainformationfetch($viewcolids) {
		$i=0;
		$data = array();
		$this->db->select("viewcreationcolumns.viewcreationcolumnname,
		viewcreationcolumns.viewcreationcolmodelname,
		viewcreationcolumns.viewcreationcolmodelindexname,
		viewcreationcolumns.viewcreationcolmodeljointable,
		viewcreationcolumns.viewcreationcolumnid,
		viewcreationcolumns.viewcreationcolmodelaliasname,
		viewcreationcolumns.viewcreationcolmodeltable,
		viewcreationcolumns.viewcreationparenttable,
		viewcreationcolumns.viewcreationmoduleid,
		viewcreationcolumns.viewcreationjoincolmodelname,
		viewcreationcolumns.viewcreationcolumnviewtype,
		viewcreationcolumns.viewcreationtype,
		viewcreationcolumns.viewcreationcolmodelcondname,
		viewcreationcolumns.uitypeid,
		viewcreationcolumns.moduletabsectionid,
		viewcreationcolumns.viewcreationcolmodelcondvalue");
		$this->db->from('viewcreationcolumns');
		$this->db->where_in('viewcreationcolumns.viewcreationcolumnid',$viewcolids);
		$this->db->where('viewcreationcolumns.status',1);
		$showfield = $this->db->get();
		if($showfield->num_rows() >0) {
			foreach($showfield->result() as $show) {
				$data['colname'][$i]=$show->viewcreationcolumnname;
				$data['colmodelname'][$i]=$show->viewcreationcolmodelname;
				$data['colmodelaliasname'][$i]=$show->viewcreationcolmodelaliasname;
				$data['coltablename'][$i]=$show->viewcreationcolmodeltable;
				$data['coljointablename'][$i]=$show->viewcreationcolmodeljointable;
				$data['coljoinmodelname'][$i]=$show->viewcreationjoincolmodelname;
				$data['colmodelindex'][$i]=$show->viewcreationcolmodeljointable .'.'.$show->viewcreationcolmodelindexname;
				$data['colmodelparenttable'][$i]=$show->viewcreationparenttable;
				$data['colmodelviewtype'][$i]=$show->viewcreationcolumnviewtype;
				$data['colmodeldatatype'][$i]=$show->viewcreationtype;
				$data['colmodelcondname'][$i]=$show->viewcreationcolmodelcondname;
				$data['colmodelcondvalue'][$i]=$show->viewcreationcolmodelcondvalue;
				$data['colmodelmoduleid'][$i]=$show->viewcreationmoduleid;
				$data['colmodeluitype'][$i]=$show->uitypeid;
				$data['colmodelsectid'][$i]=$show->moduletabsectionid;
				$data['colmodeltype'][$i]='text';
				$i++;
			}
		}
		return $data;
    }
	//excel data export
	public function exceldataexportmodel() {
		//datas
		$moduleid = $_POST['viewmoduleid'];
		$modulename = $_POST['viewmodulename'];
		$viewid = $_POST['viewid'];
		$parenttable = $_POST['parenttable'];
		$parenttabid = $_POST['parenttabid'];
		$curpagerowid = $_POST['currentpageids'];
		$allpageids = $_POST['allpageids'];
		$page = $_POST['pagenumber'];
		if($_POST['recordcounts'] != '' || $_POST['recordcounts'] != 'undefined'){
			$limit = $_POST['recordcounts'];
		}else{
			$limit = 0;
		}		
		$count = $_POST['conddatacount'];
		$griddata = $_POST['congriddatas'];
		$filetype = $_POST['exportfiletype'];
		$title = $modulename;
		$fname = preg_replace('/[^A-Za-z0-9]/', '', $title);
		$viewcolumnids = $_POST['viewcolids'];
		//data fetch
		$result = $this->exceldatainfofetch($viewid,$viewcolumnids,$parenttabid,$moduleid,$parenttable,$curpagerowid,$allpageids,$page,$limit,$count,$griddata);
		$today = date("d-m-Y");
		$today_time=date('g:i:s a');
		$this->excel->setActiveSheetIndex(0);
		$sheet = $this->excel->getActiveSheet();
		$sheet->setTitle($title);
		$count = 0;
		if(count($result[0]) > 0) {
			$count = count($result[0]['colmodelname']);
		}
		//header
		//sl.NO
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(13);
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);	
		$this->excel->getActiveSheet()->setCellValue('A1','Sl.No');
		$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(50);
		$x=1;
		$h='B';
		for($i=0;$i<$count;$i++) {
			$this->excel->getActiveSheet()->getStyle($h.$x)->getFont()->setSize(13);
			$this->excel->getActiveSheet()->getStyle($h.$x)->getFont()->setBold(true);	
			$this->excel->getActiveSheet()->setCellValue($h.$x,$result[0]['colname'][$i]);
			$this->excel->getActiveSheet()->getColumnDimension($h)->setAutoSize(50);
			$h++;
		}
		$m=1;
		$tot_rows = 1;
		$rows=2;
		foreach($result[1]->result() as $row) {
			$c='B';
			$this->excel->getActiveSheet()->getStyle('A'.$rows)->getFont()->setBold(false);
			$this->excel->getActiveSheet()->setCellValue('A'.$rows,$m);
			$this->excel->getActiveSheet()->getStyle('A'.$rows)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			if ($rows % 2 == 0) {
				$this->excel->getActiveSheet()->getStyle('A'.$rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$this->excel->getActiveSheet()->getStyle('A'.$rows)->getFill()->getStartColor()->setARGB('e4f3fd');
			}
			$m++;
			for($i=0;$i<$count;$i++) {
				$this->excel->getActiveSheet()->getStyle($c.$rows)->getFont()->setBold(false);
				$this->excel->getActiveSheet()->getStyle($c.$rows)->getAlignment()->setWrapText(true);
				$columnfieldname = $result[0]['colmodelname'][$i];
				$this->excel->getActiveSheet()->setCellValue($c.$rows,trim($row->$columnfieldname));
				$this->excel->getActiveSheet()->getStyle($c.$rows)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				if( is_numeric( trim( $row->$columnfieldname) ) ) {
					$this->excel->getActiveSheet()->getStyle($c.$rows)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_GENERAL);
					$this->excel->getActiveSheet()->getStyle($c.$rows)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				}
				if ($rows % 2 == 0) {
					$this->excel->getActiveSheet()->getStyle($c.$rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
					$this->excel->getActiveSheet()->getStyle($c.$rows)->getFill()->getStartColor()->setARGB('e4f3fd');
				}
				$c++;
				$columnfieldname = '';
			}
			$rows++;
			$tot_rows++;
		}
		{//activity log entry
			$userid = $this->Basefunctions->logemployeeid;
			$date = date($this->Basefunctions->datef);
			$arr = array(
				'activitylogname'=>'Data downloaded as excel file',
				'moduleid'=>$moduleid,
				'activitylogtype'=>2,
				'processtime'=>$date,
				'employeeid'=>$userid,
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
				'createdate'=>$date,
				'lastupdatedate'=>$date,
				'status'=>1
			);
			$this->db->insert('activitylog',array_filter($arr));
		}
		$today_time=date('g-i-s-a');
		//ob_clean();
		if($filetype == "xls") {
			$filename=$fname.'_'.$today.'_'.$today_time.'.xls';
			header('Content-Type: application/vnd.ms-excel');
			header("Content-type:application/octetstream");
			header("Content-Disposition:attachment;filename=".$filename."");
			header("Content-Transfer-Encoding:binary");
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel5');
		} else if($filetype == "xlsx") {
			$filename=$fname.'_'.$today.'_'.$today_time.'.xlsx';
			header("Content-type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8;");
			header("Content-type:application/octetstream");
			header("Content-Disposition:attachment;filename=".$filename."");
			header("Content-Transfer-Encoding:binary");
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
		} else {
			$filename=$fname.'_'.$today.'_'.$today_time.'.csv';
			header('Content-Type: application/vnd.ms-excel');
			header("Content-type:application/octetstream");
			header("Content-Disposition:attachment;filename=".$filename."");
			header("Content-Transfer-Encoding:binary");
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'CSV');
		}
		$objWriter->save('php://output');
		//notification trigger in export
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		$notimsg = $empname." "."Exported ".$modulename;
		$this->Basefunctions->notificationcontentadd(1,'Export',$notimsg,1,$moduleid);
	}
	//excel data fetch
    public function exceldatainfofetch($creationid,$viewcolumnids,$rowid,$viewcolmoduleids,$maintable,$curpagerowid,$allpageids,$page,$limit,$count,$griddata) {
		$whereString="";
		$groupid="";
		$extracolinfo = array();
		$multiarray = array();
		$j=0;
		$formdata=json_decode( $griddata, true );
		for ( $i=( $count-1 ); $i >=0 ; $i-- ) {
			$extracolinfo[$j] = $formdata[$i]['fieldid'];
			$multiarray[$j]=array(
				'0'=>$formdata[$i]['jointable'],
				'1'=>$formdata[$i]['indexname'],
				'2'=>$formdata[$i]['massconditionidname'],
				'3'=>$formdata[$i]['critreiavalue'],
				'4'=>$formdata[$i]['massandorcondidname']
			);
			$j++;
		}
		//grid column title information fetch
		$viewcoldataids = explode(',',$viewcolumnids);
		$colmodelinfo = $this->viewcolumndatainformationfetch($viewcoldataids);
		//for fetch value and condition generation
		$viewinfocolids = array_merge($viewcoldataids,$extracolinfo);
		$colinfo = $this->viewcolumndatainformationfetch($viewinfocolids);
		//main table
		$selvalue = $maintable.'.'.$rowid;
		$in = 0;
		//generate joins
		$counttable=0;
		if( count($colinfo)>0 ) {
			$counttable=count($colinfo['coltablename']);
		}
		$jtab = array($maintable);
		$ptab = array($maintable);
		$joinq = "";
		//attribute where condition
		$attrcond = "";
		$attrcondarr = array();
		$x = 0;
		$con = "";
		for($k=0;$k<$counttable;$k++) {
			if($x != 0) { $con = "AND"; }
			//parent join check
			$ptabjoin = "";
			$ptabjoin = $colinfo['colmodelparenttable'][$k];
			if(in_array($ptabjoin,$ptab)) {
				if(!in_array($colinfo['coltablename'][$k],$jtab) && $colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
					array_push($jtab,trim($colinfo['coltablename'][$k]));
					//employee
					$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid LIKE "%1%"';
					//group
					$joinq .= ' LEFT OUTER JOIN employeegroup ON FIND_IN_SET(employeegroup.employeegroupid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid  LIKE "%2%"';
					//user role and sub ordinate
					$joinq .= ' LEFT OUTER JOIN userrole ON FIND_IN_SET(userrole.userroleid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND ('.$maintable.'.employeetypeid LIKE "%3%" OR '.$maintable.'.employeetypeid LIKE "%4%")';
				} else {
					if($colinfo['colmodeldatatype'][$k] == 1) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
							}
						}
					} else if($colinfo['colmodeldatatype'][$k] == 2) {
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else if($colinfo['colmodeldatatype'][$k] == 3) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0';
							}
						}
					}
				}
			} else {
				if($colinfo['colmodeldatatype'][$k] == 1) {
					$result = $this->Basefunctions->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
							}
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$rowid.','.$maintable.'.'.$rowid.') > 0';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 1) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
									}
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 2) {
					$joinresult = $this->Basefunctions->parentjointableinfo($ptabjoin,$viewcolmoduleids);
					if($joinresult->num_rows() >0) {
						foreach($joinresult->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
							//condition name
							$condcolname = $show->viewcreationcolmodelcondname;
							//condition value
							$condcolvalue = $show->viewcreationcolmodelcondvalue;
						}
						//restrict repeated parent join
						if(in_array($destab,$ptab)) {
							if($destab != $soucrcetab) {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0';
								}
							} else {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0';
								}
							}
						}
						//parent table condition
						if($condcolname != "" && $condcolvalue != "") {
							$attrcond .= " ".$con." ".$srcjointab.'.'.$condcolname ."=". $condcolvalue;
							array_push($attrcondarr,$srcjointab.'.'.$joincolname);
							$x = 1;
						}
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] = $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$rowid.'='.$maintable.'.'.$rowid.') > 0';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 2) {
								//attribute join
								if(!in_array($colinfo['coltablename'][$k],$jtab)) {
									array_push($jtab,trim($colinfo['coltablename'][$k]));
									$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0';
								}
								//condition
								if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
									$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
									array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
									$x = 1;
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 3) {
					$result = $this->Basefunctions->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0';
							}
						}
					}  else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$rowid.','.$maintable.'.'.$rowid.') > 0';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 3) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0';
									}
								}
							}
						}
					}
				}
			}
			$dduitypeids = array('17','18','19','23','25','26','27','28','29');
			//fields fetch
			if($colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->Basefunctions->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].', GROUP_CONCAT(DISTINCT userrole.userrolename) AS rolename, GROUP_CONCAT(DISTINCT employeegroup.employeegroupname) AS empgrpname';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k].',userrole.userrolename AS rolename,employeegroup.employeegroupname AS empgrpname';
				}
			} else if( in_array($colinfo['colmodeluitype'][$k],$dduitypeids) ) {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->Basefunctions->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].'';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
				}
			} else {
				$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
			}
		}
		//attr condition generate
		for($xi=0; $xi < ( count($attrcondarr)-1 ); $xi++ ) {
			$attrcond .= " AND ".$attrcondarr[0].'='.$attrcondarr[$xi+1];
		}
		if( $attrcond == "" ) { $attrcond = "1=1";}
		//custom condition generate
		$cuscondition = " AND 1=1";
		if( isset($_GET['cuscondfieldsname']) ) {
			$fieldsname = explode(',',$_GET['cuscondfieldsname']);
			$condvalues = explode(',',$_GET['cuscondvalues']);
			$condtables = explode(',',$_GET['cuscondtablenames']);
			$condjoinid = explode(',',$_GET['cusjointableid']);
			$m=0;
			foreach($fieldsname AS $values) {
				if($condvalues[$m] != "") {
					if(in_array($condtables[$m],$ptab)) {
						$tabname = explode('.',$values);
						//restrict repeated join
						if(!in_array($condtables[$m],$jtab)) {
							array_push($jtab,$condtables[$m]);
							$joinq=$joinq.' LEFT OUTER JOIN '.$condtables[$m].' ON '.$tabname[0].'.'.$condjoinid[$m].'='.$maintable.'.'.$condjoinid[$m];
						}
					}
					$cuscondition .= " AND ".$values."=".$condvalues[$m];
					$m++;
				}
			}
		}
		//generate condition
		$w = '1=1';
		$ewh = 'AND 1=1';
		$conditionvalarray = $this->Basefunctions->conditioninfofetch($creationid);
		$c = count($conditionvalarray);
		$d = count($multiarray);
		if($c>0) {
			$whereString=$this->viewwhereclausegeneration($conditionvalarray);
		} else {
			$whereString=$ewh;
		}
		if($d>0) {
			$extrawhereString=$this->whereclausegeneration($multiarray);
		} else {
			$extrawhereString=$ewh;
		}
		if($maintable == 'employee') {
			$status = $maintable.'.status NOT IN (3)';
		} else {
			$status = $maintable.'.status IN (1)';
		}
		//pagination
		$li = "";
		//current page records
		if($curpagerowid == "" && $allpageids == 'No') {
			$start = $limit*$page - $limit;
			$li = 'LIMIT '.$start.','.$limit;
		}
		//selected  records
		$datarowids = " AND 1=1";
		if($curpagerowid != "" && $allpageids == 'No') {
			$datarowids = ' AND '.$maintable.'.'.$rowid .' IN ('.$curpagerowid .')';
		}
		//query statements
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$selvalue.' from '.$maintable.' '.$joinq.' WHERE '.$attrcond.' '.$whereString.' '.$extrawhereString.' '.$cuscondition.' '.$datarowids.' AND '.$status.' GROUP By '.$maintable.'.'.$maintable.'id ORDER BY'.' '.$maintable.'.'.$rowid.' DESC '.$li);
		$finalresult=array($colmodelinfo,$data);
		return $finalresult;
    }
    //view drop down load function
    public function viewdropdownloadfunmodel() {
    	$invitemoduleid = $_GET['invitemoduleid'];
    	$userid = $this->Basefunctions->userid;
    	$i = 0;
    	$this->db->select('viewcreationid,viewcreationname');
    	$this->db->from('viewcreation');
    	$this->db->where('viewcreation.viewcreationmoduleid',$invitemoduleid);
    	$this->db->where_in('viewcreation.createuserid',array(1,$userid));
    	$this->db->where('viewcreation.status',1);
    	$result = $this->db->get();
    	if($result->num_rows() >0) {
    		foreach($result->result() as $row) {
    			$data[$i] = array('datasid'=>$row->viewcreationid,'dataname'=>$row->viewcreationname);
    			$i++;
    		}
    		echo json_encode($data);
    	} else {
    		echo json_encode(array("fail"=>'FAILED'));
    	}
    }
}