<style type="text/css">

.innergridpaddingbtm
{
 top:20px !important;	
}
</style>
<!-- For Date Formatter -->
<?php 
$CI =& get_instance();
$appdateformat = $CI->Basefunctions->appdateformatfetch(); ?>
<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<span class="gridviewdivforsh">
				<?php
					$device = $this->Basefunctions->deviceinfo();
					$dataset['moduelid']=252;
					$dataset['formtype']= 'stepper';
					$this->load->view('Base/mainviewaddformheader',$dataset);
				?>
				<div class="large-12 columns addformunderheader">&nbsp;</div>
				<div class="large-12 columns scrollbarclass addformcontainer datamanipulationtouch">
					<div class="row">&nbsp;</div>
					<form method="POST" name="dataaddform" class="" action ="" id="dataaddform" enctype="multipart/form-data">
						<span id="formaddwizard" class="validationEngineContainer" >
							<span id="formeditwizard" class="validationEngineContainer">
								<?php
									//function call for form fields generation
									$innergridtype = 'form';
									formfieldstemplategenerator($modtabgrp,'',$innergridtype);
								?>
							</span>
						</span>
						<!--hidden values-->
						<?php
							echo hidden('primarydataid','');
							echo hidden('sortorder','');
							echo hidden('sortcolumn','');
							echo hidden('defaultviewcreationid','');
							echo hidden('parenttableid','');
							echo hidden('allrecordexport','No');
							echo hidden('viewcolumnids','');
							$value = '';
							echo hidden('resctable',$value);
							//hidden text box view columns field module ids
							$val = implode(',',$filedmodids);
							echo hidden('viewfieldids',$val);
						?>
					</form>
				</div>
			</span>			
		</div>
	</div>
</div>