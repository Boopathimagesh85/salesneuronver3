<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Model File
class Stockmanagementmodel extends CI_Model {    
    public  $salesordermodule=37;
	public function __construct() {		
		parent::__construct(); 
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
		$this->load->model('Stockmanagement/Tagprintmodel');			 
    }
	//product overlay grid value fetch
	public function producrgriddisplaymodel($tablename,$sortcol,$sortord,$pagenum,$rowscount,$poid) {
		$industryid = $this->Basefunctions->industryid;
		if($poid != ''){
			$dataset = 'product.productid,product.productname,category.categoryname,status.statusname';
			$join =' LEFT OUTER JOIN product ON product.productid=purchaseorderdetail.productid';
			$join .=' LEFT OUTER JOIN category ON category.categoryid=product.categoryid';
			$join .= ' LEFT OUTER JOIN status ON status.status=product.status';
			/* pagination */
			$query = 'select '.$dataset.' from purchaseorderdetail'.$join.' WHERE purchaseorderdetail.purchaseorderid = '.$poid.' ORDER BY'.' '.$sortcol.' '.$sortord;
			$page = $this->Basefunctions->generatepage($query,$pagenum,$rowscount);
			/* query */
			$data = $this->db->query('select '.$dataset.' from purchaseorderdetail'.$join.' WHERE purchaseorderdetail.purchaseorderid = '.$poid.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$page['start'].','.$page['records'].'');
			$finalresult=array($data,$page,'');
		}else{
			$dataset = 'product.productid,product.productname,category.categoryname,status.statusname';
			$join =' LEFT OUTER JOIN category ON category.categoryid=product.categoryid';
			$join .= ' LEFT OUTER JOIN status ON status.status=product.status';
			/* pagination */
			$query = 'select '.$dataset.' from product'.$join.' WHERE product.stockable="Yes" and product.industryid='.$industryid.' ORDER BY'.' '.$sortcol.' '.$sortord;
			$page = $this->Basefunctions->generatepage($query,$pagenum,$rowscount);
			/* query */
			$data = $this->db->query('select '.$dataset.' from product'.$join.' WHERE product.stockable="Yes" and product.industryid='.$industryid.'  ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$page['start'].','.$page['records'].'');
			$finalresult=array($data,$page,'');
		}			
		return $finalresult;
	}
	//purchase order data fetch
	public function purchaseorderdatafetchmodel() {
		$id = $_GET['poid'];
		$this->db->select('purchaseorderid,purchaseordernumber,purchaseorderdate,toaccountid,crmstatusid');
		$this->db->from('purchaseorder');
		$this->db->where('purchaseorder.purchaseorderid',$id);
		$this->db->where('purchaseorder.status',$this->Basefunctions->activestatus);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('date'=>$row->purchaseorderdate,'poto'=>$row->toaccountid,'postatus'=>$row->crmstatusid);
			}
			echo json_encode($data);
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}
	}
	//branch based product dd val fetch
	public function productddvalfetchmodel() {
		$poid = $_GET['poid'];
		$industryid = $this->Basefunctions->industryid;
		$i=0;
		if($poid != ''){
			$this->db->select('product.productid,product.productname');
			$this->db->from('purchaseorderdetail');
			$this->db->join('product','product.productid=purchaseorderdetail.productid');
			$this->db->where('purchaseorderdetail.purchaseorderid',$poid);
			$this->db->where('purchaseorderdetail.status',1);
			$this->db->where('product.status',1);
			$this->db->group_by('product.productid');
		}else{
			$this->db->select('product.productid,product.productname');
			$this->db->from('product');
			$this->db->where('product.status',1);
			$this->db->where_in('product.industryid',array($industryid));
			$this->db->group_by('product.productid');
		}		
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->productid,'dataname'=>$row->productname);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}
	}
	//product based data fetch
	public function productbaseddatafetchmodel() {
		$id = $_GET['productid'];
		$this->db->select('purchaseorderdetail.uomid,purchaseorderdetail.touomid,uom.uomname,purchaseorderdetail.conversionrate,purchaseorderdetail.quantity,purchaseorderdetail.netamount,product.weight,product.unitprice,product.categoryid,product.hasbatch,purchaseorderdetail.conversionquantity');
		$this->db->from('purchaseorderdetail');
		$this->db->join('product','product.productid=purchaseorderdetail.productid');
		$this->db->join('uom','uom.uomid=purchaseorderdetail.touomid');
		$this->db->where('purchaseorderdetail.productid',$id);
		$this->db->where('purchaseorderdetail.status',$this->Basefunctions->activestatus);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('uomid'=>$row->uomid,'touomid'=>$row->touomid,'uomname'=>$row->uomname,'conversionrate'=>$row->conversionrate,'conversionquantity'=>$row->conversionquantity,'quantity'=>$row->quantity,'weight'=>$row->weight,'unitprice'=>$row->unitprice,'categoryid'=>$row->categoryid,'hasbatch'=>$row->hasbatch);
			}
			echo json_encode($data);
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}
	}	
	//product based batch fetch	
	public function batchdatafetchmodel() {
		$productid = $_GET['productid'];
		$i=0;
		$date = date('Y-m-d');
		$this->db->select('batchid,batchname');
		$this->db->from('batch');
		$this->db->where('batch.productid like','%'.$productid.'%');
		$this->db->where('batch.expirydate >=',$date);
		$this->db->where('batch.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->batchid,'dataname'=>$row->batchname);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}
	}
	//stock management create_function
	public function stockmanagementcreatemodel() {
		$tagtypeid = $_POST['tagtypeid'];
		if($tagtypeid == 2) {
			$pieces = $_POST['quantity'];
			$quantity = 1;
		} else if($tagtypeid == 3){
			$pieces = 1;
			$quantity = $_POST['quantity'];
		}
		$serialnumber='1';
		$date = $_POST['stockmanagementdate'];
		if(isset($_POST['tagtemplateid'])){
			$tagtemplateid = $_POST['tagtemplateid'];
		}else{
			$tagtemplateid = 1;
		}
		$purchaseordernumber = $_POST['purchaseorderid'];
		$purchaseorderdate = $_POST['purchaseorderdate'];
		$softwareindustryid = $this->Basefunctions->industryid;
		if(isset($_POST['accountid'])) {
			$accountid = $_POST['accountid'];
		} else {
			$accountid = '1';
		}
		if(isset($_POST["crmstatusid"])){
			$crmstatusid = $_POST['crmstatusid'];
		} else {
			$crmstatusid = '1';
		}		
		$branchid = $_POST['branchid'];
		$productid = $_POST['productid'];
		if(isset($_POST['uomid'])){
			if($_POST['uomid'] === 'null' || $_POST['uomid'] == ''){
				$uomid = 1;
			}else{
				$uomid = $_POST['uomid'];
			}
		}else{
			$uomid = 1;
		}		
		if(isset($_POST['touomid'])){
			if($_POST['touomid'] === 'null' || $_POST['touomid'] == ''){
				$touomid = 1;
			}else{
				$touomid = $_POST['touomid'];
			}
		}else{
			$touomid = 1;
		}
		if(isset($_POST['uomconversionrate'])){
			$uomconversionrate = $_POST['uomconversionrate'];
		}else{
			$uomconversionrate = 0;
		}	
		if(isset($_POST['batchid'])) {
			$batchid = $_POST['batchid'];
		} else {
			$batchid = '1';
		}
		$purchaseprice = $_POST['purchaseprice'];
		if(isset($_POST['conversionquantity'])) {
			$conversionquantity = $_POST['conversionquantity'];
		} else{
			$conversionquantity = 0;
		}
		if(isset($_POST['counterid'])) {
			$counterid = $_POST['counterid'];
		} else{
			$counterid = 1;
		}		
		$receivedby = $_POST['receivedby'];
		$referrencenumber = $_POST['referrencenumber'];
		$comments = $_POST['comments'];
		$poid = $_POST['purchaseorderhideidid'];
		$userid = $this->Basefunctions->userid;
		$cdate = date($this->Basefunctions->datef);
		for($i=1;$i<=$pieces;$i++) {
			if($tagtypeid != 3) { //except the untag other tags are serial numbers
				$anfieldnameinfo = $_POST['anfieldnameinfo'];
				$anfieldnameidinfo = $_POST['anfieldnameidinfo'];
				$anfieldtabinfo = $_POST['anfieldtabinfo'];
				$anfieldnamemodinfo = $_POST['anfieldnamemodinfo'];
				$serialnumber = $this->Basefunctions->randomnumbergenerator($anfieldnamemodinfo,$anfieldtabinfo,$anfieldnameinfo,$anfieldnameidinfo);
			}
			$stockmanagement = array(
				'stockmanagementnumber'=>$serialnumber,
				'stockmanagementdate'=>$this->Basefunctions->ymddateconversion($date),
				'tagtypeid'=>$tagtypeid,
				'tagtemplateid'=>$tagtemplateid,
				'purchaseorderid'=>$purchaseordernumber,
				'purchaseorderdate'=>$this->Basefunctions->ymddateconversion($purchaseorderdate),
				'accountid'=>$accountid,
				'crmstatusid'=>$crmstatusid,
				'branchid'=>$branchid,
				'productid'=>$productid,
				'uomid'=>$uomid,
				'touomid'=>$touomid,
				'uomconvertrate'=>$uomconversionrate,
				'conversionquantity'=>$conversionquantity,
				'quantity'=>$quantity,
				'purchaseprice'=>$purchaseprice,
				'batchid'=>$batchid,
				'counterid'=>$counterid,
				'receivedby'=>$receivedby,
				'referrencenumber'=>$referrencenumber,
				'comments'=>$comments,
				'industryid'=>$this->Basefunctions->industryid,
				'branchid'=>$this->Basefunctions->branchid,
				'createdate'=>$cdate,
				'lastupdatedate'=>$cdate,
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
				'status'=>$this->Basefunctions->activestatus
			);
			$this->db->insert('stockmanagement',array_filter($stockmanagement));
			$primaryid = $this->db->insert_id();
			if(isset($_POST['tagtemplateid']) && $tagtypeid !=3) {		
				$tagtemplate = trim($_POST['tagtemplateid']);
				if($tagtypeid != 3 AND $tagtemplate > 1) {	
					$data = $this->Tagprintmodel->generattagprint($tagtemplate,$primaryid);
				}				
			}
		}
		//purchase order status change
		$status = array('crmstatusid'=>71,'lastupdatedate'=>$cdate,'lastupdateuserid'=>$userid);
		$this->db->where('purchaseorder.purchaseorderid',$poid);
		$this->db->update('purchaseorder',$status);
		{//workflow management -- for data create
			$this->Basefunctions->workflowmanageindatacreatemode(37,$primaryid,'stockmanagement');
		}
		echo 'TRUE';
	}
	//stock management delete operation
	public function stockmanagementdeletemodel() {
		$smid = $_GET['smid'];
		$userid = $this->Basefunctions->userid;
		$cdate = date($this->Basefunctions->datef);
		$status = array('status'=>0,'lastupdatedate'=>$cdate,'lastupdateuserid'=>$userid);
		$this->db->where('stockmanagement.stockmanagementid',$smid);
		$this->db->update('stockmanagement',$status);
		{//workflow management -- for data delete
			$this->Basefunctions->workflowmanageindatadeletemode(37,$smid,'stockmanagementid','stockmanagement');
		}
		echo 'TRUE';
	}
	//stock management edit data fetch
	public function stockmanagementeditdatafetchmodel() {
		$smid = $_GET['smid'];
		$dateformat = $this->Basefunctions->phpmysqlappdateformatfetch();
		$mysqlformat = $dateformat['mysqlformat'];
		$result = $this->db->query('select tagtypeid,tagtemplateid,purchaseorderid,accountid,crmstatusid,stockmanagement.branchid,productid,stockmanagement.uomid,touomid,uom.uomname,uomconvertrate,quantity,weight,batchid,counterid,receivedby,referrencenumber,purchaseprice,comments,DATE_FORMAT(stockmanagementdate,"'.$mysqlformat.'") AS smdate,DATE_FORMAT(purchaseorderdate,"'.$mysqlformat.'") AS podate,conversionquantity from stockmanagement join uom on (uom.uomid=stockmanagement.touomid) where stockmanagement.stockmanagementid='.$smid.' AND stockmanagement.status=1');
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('date'=>$row->smdate,'tagtype'=>$row->tagtypeid,'tagtemplate'=>$row->tagtemplateid,'ponumber'=>$row->purchaseorderid,'podate'=>$row->podate,'poto'=>$row->accountid,'postatus'=>$row->crmstatusid,'branch'=>$row->branchid,'product'=>$row->productid,'uom'=>$row->uomid,'touomid'=>$row->touomid,'touomname'=>$row->uomname,'convertrate'=>$row->uomconvertrate,'quantity'=>$row->quantity,'weight'=>$row->weight,'batch'=>$row->batchid,'price'=>$row->purchaseprice,'counterid'=>$row->counterid,'receivedby'=>$row->receivedby,'referrencenumber'=>$row->referrencenumber,'comments'=>$row->comments,'conversionquantity'=>$row->conversionquantity);
			}
			echo json_encode($data);
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}
	}
	//stock update
	public function stockmanagementupdatemodel() {
		$primaryid = $_POST['primarydataid'];
		{//fetch old values -- work flow
			$condstatvals=$this->Basefunctions->workflowmanageolddatainfofetch(37,$primaryid);
		}
		$tagtypeid = $_POST['tagtypeid'];
		if($tagtypeid == 2) {
			$pieces = $_POST['quantity'];
			$quantity = 1;
		} else if($tagtypeid == 3){
			$pieces = 1;
			$quantity = $_POST['quantity'];
		}
		$serialnumber='';
		$date = $_POST['stockmanagementdate'];
		if(isset($_POST['tagtemplateid'])){
			$tagtemplateid = $_POST['tagtemplateid'];
		}else{
			$tagtemplateid = 1;
		}
		if(isset($_POST['purchaseorderid'])) {
			$purchaseordernumber = $_POST['purchaseorderid'];
		}else{
			$purchaseordernumber = 1;
		}		
		$purchaseorderdate = $_POST['purchaseorderdate'];
		if(isset($_POST['accountid'])) {
			$accountid = $_POST['accountid'];
		} else {
			$accountid = '1';
		}
		if(isset($_POST['crmstatusid'])) {
			$crmstatusid = $_POST['crmstatusid'];
		}else{
			$crmstatusid = 1;
		}
		$branchid = $_POST['branchid'];
		$productid = $_POST['productid'];
		if(isset($_POST['uomid'])){
		if($_POST['uomid'] === 'null' || $_POST['uomid'] == ''){
				$uomid = 1;
			}else{
				$uomid = $_POST['uomid'];
			}
		}else{
			$uomid = 1;
		}		
		if(isset($_POST['touomid'])){
			if($_POST['touomid'] === 'null' || $_POST['touomid'] == ''){
				$touomid = 1;
			}else{
				$touomid = $_POST['touomid'];
			}
		}else{
			$touomid = 1;
		}
		$uomconversionrate = $_POST['uomconversionrate'];
		$conversionquantity = $_POST['conversionquantity'];
		$softwareindustryid = $this->Basefunctions->industryid;				
		if(isset($_POST['batchid'])) {
			$batchid = $_POST['batchid'];
		} else {
			$batchid = '1';
		}
		$purchaseprice = $_POST['purchaseprice'];
		if(isset($_POST['counterid'])) {
			$counterid = $_POST['counterid'];
		}else{
			$counterid = 1;
		}
		$receivedby = $_POST['receivedby'];
		$referrencenumber = $_POST['referrencenumber'];
		$comments = $_POST['comments'];
		$userid = $this->Basefunctions->userid;
		$cdate = date($this->Basefunctions->datef);
		for($i=0;$i<=$pieces;$i++) {
			$stockmanagement = array(
				'stockmanagementdate'=>$this->Basefunctions->ymddateconversion($date),
				'tagtypeid'=>$tagtypeid,
				'tagtemplateid'=>$tagtemplateid,
				'purchaseorderid'=>$purchaseordernumber,
				'purchaseorderdate'=>$this->Basefunctions->ymddateconversion($purchaseorderdate),
				'accountid'=>$accountid,
				'crmstatusid'=>$crmstatusid,
				'branchid'=>$branchid,
				'productid'=>$productid,
				'uomid'=>$uomid,
				'touomid'=>$touomid,
				'uomconvertrate'=>$uomconversionrate,
				'conversionquantity'=>$conversionquantity,
				'quantity'=>$quantity,
				'purchaseprice'=>$purchaseprice,
				'batchid'=>$batchid,
				'counterid'=>$counterid,
				'receivedby'=>$receivedby,
				'referrencenumber'=>$referrencenumber,
				'comments'=>$comments,
				'lastupdatedate'=>$cdate,
				'lastupdateuserid'=>$userid,
				'status'=>$this->Basefunctions->activestatus
			);
			$this->db->where('stockmanagement.stockmanagementid',$primaryid);
			$this->db->update('stockmanagement',array_filter($stockmanagement));
		}
		{//workflow management for data update
			$this->Basefunctions->workflowmanageindataupdatemode(37,$primaryid,'stockmanagement',$condstatvals);
		}
		echo 'TRUE';
	}
	//user based branch fetch
	public function userbasedbranchfetchmodel() {
		$branchid = $this->Basefunctions->branchid;
		echo json_encode($branchid);
	}
	//purchase order value fetch
	public function purchaseorderddvlfetchmodel() {
		$i=0;
		$this->db->select('purchaseorderid,purchaseordernumber');
		$this->db->from('purchaseorder');
		$this->db->where('purchaseorder.crmstatusid',48);
		$this->db->where('purchaseorder.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->purchaseorderid,'dataname'=>$row->purchaseordernumber);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}
	}
}