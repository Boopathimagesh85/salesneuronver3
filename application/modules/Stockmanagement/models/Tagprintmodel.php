<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tagprintmodel extends CI_Model
{
	public function __construct()
    {		
		parent::__construct();		
    }
	public function generattagprint($templateid,$tagid)
	{
		$info = $this->db->select('fieldid,filelocation')
								->from('tagtemplate')
								->where('tagtemplateid',$templateid)
								->limit(1)
								->get()
								->row();		
		$fieldids = explode(',',$info->fieldid);		
		$filelocation = $info->filelocation;
		$filename = trim($filelocation); 
		$newfilename = explode('/',$filename);
		if(read_file('templates'.DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = trim(file_get_contents('templates'.DIRECTORY_SEPARATOR.$newfilename[1]));				
			if($tccontent != '') {
				$tccontent = $this->gettablerowmappedcontent($tccontent,'stockmanagement',$tagid,37); 
				chmod('printdata.prn', 0777);
				$fh = fopen('printdata.prn','w') or die('fileopenerror');
				fwrite($fh,$tccontent);
				fclose($fh);				
				//bat files updates.
				$computername = gethostname(); //gets computer name
				$printername = 'Barcodesanjeevi';
				$batchdata = "COPY printdata.prn \\\\".$computername."\\".$printername."";					
				chmod('cmdprint.bat', 0777);
				$fh = fopen('cmdprint.bat','w') or die('fileopenerror');
				fwrite($fh,$batchdata);
				fclose($fh);
				exec('c:\WINDOWS\system32\cmd.exe START /B /C cmdprint.bat /q',$out,$return);				
			}
		}
		return 'SUCCESS';		
	}
	//fetch inside the table tr row content
	public function gettablerowmappedcontent($datacontent,$parenttable,$id,$moduleid) {
		$finalcont = $datacontent;
		preg_match_all ("/{.*}/U", $finalcont, $contmatch);
		foreach($contmatch as $bocontent) {
			foreach($bocontent as $bomatch) {
				if($bomatch != "{S.Number}") {
					$htmldata = $this->mergcontinformationfetchmodel($bomatch,$parenttable,$id,$moduleid);
					$finalcont = preg_replace('~'.$bomatch.'~',implode(',',$htmldata),$finalcont);
				} else {
					$finalcont = preg_replace('~'.$bomatch.'~','',$finalcont);
				}
			}
		}
		return $finalcont;
	}
	//merge content data fetch
	public function mergcontinformationfetchmodel($mergegrp,$parenttable,$id,$moduleid) {
		$relmodids = $this->fetchrelatedmodulelist($moduleid);
		$primaryid = $id;
		$database = $this->db->database;
		$resultset = array();
		$mergeindval = $mergegrp;
		$formatortype = "1";
		$id = $primaryid;
		//parent child field concept
		$elname = array('parentcategoryid','parentstoragecategoryid','parentaccountid');
		$fname = array('categoryname','storagecategoryname','accountname');
		$fnameid = array('categoryid','storagecategoryid','accountid');
		$tname = array('category','storagecategory','account');		
		$empid = $this->Basefunctions->userid;
		$newdata = array();
		$tempval = substr($mergeindval,1,(strlen($mergeindval)-2));
		$newval = explode('.',$tempval);
		$reltabname = explode(':',$newval[0]);
		$table = $reltabname[0];
		$uitypeid = 2;
		//chk editor field name
		$chkeditfname = explode('_',$newval[1]);
		$editorfname = ( (count($chkeditfname) >= 2)? $chkeditfname[1] : '');
		if($table!='REL' && $table!='GEN') { //parent table fields information
			$tablename = trim($reltabname[0]);
			$tabfield = $newval[1];
			//field fetch parent table fetch
			$fieldnamechk = explode('-',$tabfield);
			$fldname = ( (count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
			$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
			$uitypeid = ( (count($fieldinfo) > 0)? $fieldinfo['uitype'] : 1);
			if($fldname != 'attributesetid') {
				//get user company & branch id
				if($tablename == 'company' || $tablename == 'branch') {
					if($tablename == 'company') {
						$compquery = $this->db->query('select company.companyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND company.status=1')->result();
						foreach($compquery as $key) {
							$id = $key->companyid;
						}
					} else if($tablename == 'branch') {
						$brquery = $this->db->query('select branch.branchid from branch join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND branch.status=1')->result();
						foreach($brquery as $key) {
							$id = $key->branchid;
						}
					}
				}
				//check its parent child field concept
				if(in_array($fldname,$elname)) {
					$key = array_search($fldname,$elname);
					$pid = $id;
					if($key != NULL) {
						//parent id get
						$result = $this->db->select("$tname[$key].$elname[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$id)->where($tname[$key].'.status',1)->get();
						if($result->num_rows() > 0) {
							foreach($result->result()as $row) {
								$pid=$row->$elname[$key];
							}
							if($pid!=0) {
								//parent name
								$result = $this->db->select("$tname[$key].$fname[$key],$tname[$key].$fnameid[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$pid)->where($tname[$key].'.status',1)->get();
								if($result->num_rows() > 0) {
									foreach($result->result()as $row) {
										$resultset[]=$row->$fname[$key];
									}
								}
							}
						}
					}
				}
				else {
					//field fetch parent table fetch
					$fieldnamechk = explode('-',$tabfield);
					$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
					$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
					if(count($fieldinfo)>0) {
						$uitypeid = $fieldinfo['uitype'];
						//fetch parent table join id
						$relpartable = $fieldinfo['partab'];
						$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
						$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
						$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
						//fetch original field of picklist data in view creation 
						$viewfieldinfo = $this->viewfieldsinformation($tablename,$tabfield,$fieldinfo['modid']);
						$tabfieldname = ( (count($viewfieldinfo)>0)? $viewfieldinfo['joinfieldname'] : $tabfield );
						$jointabfieldid = ( (count($viewfieldinfo)>0)? $viewfieldinfo['condfiledname'] : $tabfield );
						//join relation table and main table
						$joinq="";
						if($relpartable!=$tablename) {
							$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id AND '.$relpartable.'.status=1';
						}
						//main table join
						if($mainjointable!=$parenttable) {
							$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id AND '.$relpartable.'.status=1';
						}
						//if its picklist data
						$newfield = substr( $tabfieldname,( strlen($tabfieldname)-2 ));
						$tabname = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
						$tbachk = $this->tableexitcheck($database,$tabname);
						if( $newfield == "id" && $tbachk != 0 ) {
							$newtable = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
							$newjfield = $newtable."name";
							$whtable = (($tablename=="company" || $tablename=="branch")? $tablename : $parenttable);
							$newjdata = $this->db->query("SELECT ".$newtable.".".$newtable."id AS columdataid,".$newtable.".".$newjfield." AS columdata FROM ".$newtable." LEFT OUTER JOIN ".$tablename." ON ".$tablename.".".$tabfieldname." = ".$newtable.".".$tabfieldname."".$joinq." WHERE ".$tablename.".".$whtable."id = ".$id." AND ".$newtable.".status=1 AND ".$tablename.".status=1",false);
							if($newjdata->num_rows() >0) {
								foreach($newjdata->result() as $jkey) {
									$resultset[] = $jkey->columdata;
								}
							} else {
								$resultset[] = " ";
							}
						}
						else {
							//main table data sets information fetching
							$fldcountchk = explode('-',$tabfield);
							$tablcolumncheck = $this->tablefieldnamecheck($tablename,$parenttable."id");
							if( ( $parenttable == $tablename && (count($fldcountchk)) < 2 ) || ( $tablcolumncheck == 'false' && (count($fldcountchk)) < 2 ) ) {
								$newdata = $this->db->select("".$tabfield." AS columdata")->from($tablename)->where($tablename.".".$tablename."id",$id)->where($tablename.".status",1)->get()->result();
							}
							else {
								$cond = "";
								$addfield = explode('-',$tabfield);
								if( (count($addfield)) >= 2 ) {
									$datafiledname=$addfield[1];
									if(in_array('PR',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=2';
									} else if(in_array('SE',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=3'; 
									} else if(in_array('BI',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=4';
									} else if(in_array('SH',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=5';
									} else if(in_array('CW',$addfield)) {
										$formatortype = "CW";
									} else if(in_array('CF',$addfield)) {
										$formatortype = "CF";
									} else if(in_array('CS',$addfield)) {
										$formatortype = "CS";
									}
									if($tablename!=$parenttable) {
										$tablcolumncheck = $this->tablefieldnamecheck($tablename,"addresstypeid");
										$cond = ( ($tablcolumncheck == 'true')? $cond : ' ');
										$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename." JOIN ".$parenttable." ON ".$parenttable.".".$parenttable."id = ".$tablename.".".$parenttable."id WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1".$cond."")->result();
									} else {
										$newdata = $this->db->select(''.$datafiledname.' AS columdata',false)->from($tablename)->where($tablename.'id',$id)->where($tablename.'.status',1)->get()->result();
									}
								}
								else {
									$newdata = $this->db->query("SELECT ".$tabfield." AS columdata FROM ".$tablename." JOIN ".$parenttable." ON ".$parenttable.".".$parenttable."id = ".$tablename.".".$parenttable."id WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1")->result();
								}
							}
							foreach($newdata as $key) {
								if($tabfield == 'companylogo') {
									$imgname = $key->columdata;
									if(filter_var($imgname,FILTER_VALIDATE_URL)) {
										$resultset[] = '<img src="'.$imgname.'" />';
									} else {
										if( file_exists($imgname) ) {
											$resultset[] = '<img src="'.$imgname.'" style="width:11em; height:3em" />';
										} else {
											$resultset[] = " ";
										}
									}
								}
								else {
									$currencyinfo = $this->currencyinformation($parenttable,$id,$moduleid);
									if($formatortype=="CW") { //convert to word
										if(count($currencyinfo)>0) {
											$resultset[] = $this->convert->numtowordconvert($key->columdata,$currencyinfo['name'],$currencyinfo['subcode']);
										} else {
											$resultset[] = $this->convert->numtowordconvert($key->columdata);
										}
									} else if($formatortype=="CF") { //currenct format
										if(count($currencyinfo)>0) {
											$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
										} else {
											$resultset[] = $this->convert->formatcurrency($key->columdata);
										}
									} else if($formatortype=="CS") { //currency with symbol
										if(count($currencyinfo)>0) {
											$amt = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
											$resultset[] = $currencyinfo['symbol'].' '.$amt;
										} else {
											$resultset[] = $currencyinfo['symbol'].' '.$this->convert->formatcurrency($key->columdata);
										}
									} else {
										if(is_numeric($key->columdata)) {
											if($uitypeid == '4' || $uitypeid == '5' || $uitypeid == '7') {
												if(count($currencyinfo)>0) {
													$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
												} else {
													$resultset[] = $this->convert->formatcurrency($key->columdata);
												}
											} else {
												$resultset[] = $key->columdata;
											}
										} else {
											if($editorfname=='editorfilename') {
												if( file_exists($key->columdata) ) {
													$datas = $this->geteditorfilecontent($key->columdata);
													$datas = preg_replace('~a10s~','&nbsp;', $datas);
													$datas = preg_replace('~<br>~','<br />', $datas);
													$resultset[] = $datas;
												} else {
													$resultset[] = '';
												}
											} else {
												$resultset[] = $key->columdata;
											}
										}
									}
								}
							}
						}
					}
				}
			} 
			else {
				//attribute value fetch for non relational modules
				$fieldnamechk = explode('-',$tabfield);
				$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
				$fieldinfo = $this->fetchfieldparenttable($table,$fldname);
				if(count($fieldinfo)>0) {
					$uitypeid = $fieldinfo['uitype'];
					//fetch parent table join id
					$relpartable = $fieldinfo['partab'];
					$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
					$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
					$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
					//join relation table and main table
					$joinq="";
					if($parenttable!=$table) {
						$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$table.'.'.$parenttable.'id';
					}
					//fetch attribute name and values
					$i=0;
					$datasets = array();
					$attrsetids = $this->db->query(" SELECT ".$table.".".$table."id as Id,".$table.".attributesetid as attrsetid,".$table.".attributes as attributeid,".$table.".attributevalues as attributevalueid FROM ".$table.$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$mainjointable.".status=1 AND ".$table.".status=1",false)->result();
					foreach($attrsetids as $attrset) {
						$datasets[$i] = array('id'=>$attrset->Id,'attrsetid'=>$attrset->attrsetid,'attrnames'=>$attrset->attributeid,'attrvals'=>$attrset->attributevalueid);
						$i++;
					}
					$resultset = array();
					foreach($datasets as $set) {
						$valid = explode('_',$set['attrvals']);
						$nameid = explode('_',$set['attrnames']);
						$n=1;
						$vi = 0;
						$attrdatasets = array();
						for($h=0;$h<count($valid);$h++) {
							$attrval = '';
							$attrname = '';
							if($valid[$h]!='') {
								//attribute value fetch
								$attrvaldata = $this->db->query(' SELECT attributevalueid,attributevaluename FROM attributevalue WHERE attributevalueid='.$valid[$h].' AND status=1',false)->result();
								foreach($attrvaldata as $valsets) {
									$attrval = $valsets->attributevaluename;
								}
								//attribute name fetch
								$attrnamedata = $this->db->query(' SELECT attributeid,attributename FROM attribute WHERE attributeid='.$nameid[$h].' AND status=1',false)->result();
								foreach($attrnamedata as $namesets) {
									$attrname = $namesets->attributename;
								}
							}
							$attrset = "";
							if( $attrval!='' ) {
								$attrset = $n.'.'.$attrname.': '.$attrval;
								++$n;
							}
							$attrdatasets[$vi] = $attrset;
							$vi++;
						}
						$resultset[] = implode('<br />',$attrdatasets);
					}
				}
			}
		} 
		else if($table=='REL') { //related module fields information fetch
			$tablename = trim($reltabname[1]);
			$tabfield = trim($newval[1]);
			$fieldnamechk = explode('-',$tabfield);
			$fldname = ( (count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
			if($fldname != 'attributesetid') {
				//get user company & branch id
				if($tablename == 'company' || $tablename == 'branch') {
					if($tablename == 'company') {
						$compquery = $this->db->query('select company.companyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND company.status=1')->result();
						foreach($compquery as $key) {
							$id = $key->companyid;
						}
					} else if($tablename == 'branch') {
						$brquery = $this->db->query('select branch.branchid from branch join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND branch.status=1')->result();
						foreach($brquery as $key) {
							$id = $key->branchid;
						}
					}
				}
				//check its parent child field concept
				$fldnamechk = explode('-',$tabfield);
				$fldname = ( (count($fldnamechk) >= 2)? $fldnamechk[1] : $tabfield);
				if(in_array($fldname,$elname)) {
					$key = array_search($fldname,$elname);
					$pid = $id;
					if($key != NULL) {
						//parent id get
						$result = $this->db->select("$tname[$key].$elname[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$id)->where($tname[$key].'.status',1)->get();
						if($result->num_rows() > 0) {
							foreach($result->result()as $row) {
								$pid=$row->$elname[$key];
							}
							if($pid!=0) {
								//parent name
								$result = $this->db->select("$tname[$key].$fname[$key],$tname[$key].$fnameid[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$pid)->where($tname[$key].'.status',1)->get();
								if($result->num_rows() > 0) {
									foreach($result->result()as $row) {
										$resultset[]=$row->$fname[$key];
									}
								}
							}
						}
					}
				}
				else {
					//field fetch parent table fetch
					$fieldnamechk = explode('-',$tabfield);
					$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
					$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
					if(count($fieldinfo)>0) {
						$uitypeid = $fieldinfo['uitype'];
						//fetch parent table join id
						$relpartable = $fieldinfo['partab'];
						$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
						$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
						$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
						//fetch original field of picklist data in view creation 
						$viewfieldinfo = $this->viewfieldsinformation($tablename,$tabfield,$fieldinfo['modid']);
						$tabfieldname = ( (count($viewfieldinfo)>0)? $viewfieldinfo['joinfieldname'] : $tabfield );
						$jointabfieldid = ( (count($viewfieldinfo)>0)? $viewfieldinfo['condfiledname'] : $tabfield );
						//join relation table and main table
						$joinq="";
						if($relpartable!=$tablename) {
							$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id AND '.$relpartable.'.status=1';
						}
						//main table join
						if($mainjointable==$parenttable) {
							$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id AND '.$mainjointable.'.status=1';
						} else {
							$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id AND '.$mainjointable.'.status=1';
							$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$mainjointable.'.'.$parenttable.'id AND '.$parenttable.'.status=1';
						}
						//if its picklist data
						$newfield = substr( $tabfieldname,( strlen($tabfieldname)-2 ));
						$tabname = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
						$tblchk = $this->tableexitcheck($database,$tabname);
						if( $newfield == "id" && $tblchk != 0 ) {
							$newtable = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
							$newjfield = $newtable."name";
							$whtable = (($tablename=="company" || $tablename=="branch")? $tablename : $parenttable);
							$newjdata = $this->db->query("SELECT ".$newtable.".".$newtable."id AS columdataid,".$newtable.".".$newjfield." AS columdata FROM ".$newtable." LEFT OUTER JOIN ".$tablename." ON ".$tablename.".".$jointabfieldid." = ".$newtable.".".$tabfieldname."".$joinq." WHERE ".$parenttable.".".$whtable."id = ".$id." AND ".$mainjointable.".status=1 AND ".$newtable.".status=1 AND ".$tablename.".status=1");
							if($newjdata->num_rows() >0) {
								foreach($newjdata->result() as $jkey) {
									$resultset[] = $jkey->columdata;
								}
							} else {
								$resultset[] = " ";
							}
						}
						else {
							//main table data sets information fetching
							$fldcountchk = explode('-',$tabfield);
							$tablcolumncheck = $this->tablefieldnamecheck($tablename,$parenttable."id");
							if( ( $parenttable == $tablename && (count($fldcountchk)) < 2 ) || ( $tablcolumncheck == 'false' && (count($fldcountchk)) < 2 ) ) {
								$newdata = $this->db->query("SELECT ".$tablename.".".$tabfield." AS columdata FROM ".$tablename."".$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1")->result();
							}
							else {
								$cond = "";
								$addfield = explode('-',$tabfield);
								if( (count($addfield)) >= 2 ) {
									$datafiledname=$addfield[1];
									if(in_array('PR',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=2';
									} else if(in_array('SE',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=3'; 
									} else if(in_array('BI',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=4';
									} else if(in_array('SH',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=5';
									} else if(in_array('CW',$addfield)) {
										$formatortype = "CW";
									} else if(in_array('CF',$addfield)) {
										$formatortype = "CF";
									} else if(in_array('CS',$addfield)) {
										$formatortype = "CS";
									}
									$tablcolumncheck = $this->tablefieldnamecheck($tablename,"addresstypeid");
									$cond = ( ($tablcolumncheck == 'true')? $cond : ' ');
									if($tablename!=$parenttable) {
										$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename."".$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1".$cond."")->result();
									} else {
										$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename." WHERE ".$tablename."id = ".$id." AND ".$tablename.".status=1")->result();
									}
								}
								else {
									$newdata = $this->db->query("SELECT ".$tabfield." AS columdata FROM ".$tablename."".$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1".$cond."")->result();
								}
							}
							foreach($newdata as $key) {
								if($tabfield == 'companylogo') {
									$imgname = $key->columdata;
									if(filter_var($imgname, FILTER_VALIDATE_URL)) {
										$resultset[] = '<img src="'.$imgname.'" />';
									} else {
										if( file_exists($imgname) ) {
											$resultset[] = '<img src="'.$imgname.'" style="width:11em; height:3em" />';
										} else {
											$resultset[] = " ";
										}
									}
								}
								else {
									$currencyinfo = $this->currencyinformation($parenttable,$id,$moduleid);
									if($formatortype=="CW") { //convert to word
										if(count($currencyinfo)>0) {
											$resultset[] = $this->convert->numtowordconvert($key->columdata,$currencyinfo['name'],$currencyinfo['subcode']);
										} else {
											$resultset[] = $this->convert->numtowordconvert($key->columdata);
										}
									} else if($formatortype=="CF") { //currenct format
										if(count($currencyinfo)>0) {
											$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
										} else {
											$resultset[] = $this->convert->formatcurrency($key->columdata);
										}
									} else if($formatortype=="CS") { //currency with symbol
										if(count($currencyinfo)>0) {
											$amt = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
											$resultset[] = $currencyinfo['symbol'].' '.$amt;
										} else {
											$resultset[] = $currencyinfo['symbol'].' '.$this->convert->formatcurrency($key->columdata);
										}
									} else {
										if(is_numeric($key->columdata)) {
											if( $uitypeid == '4' || $uitypeid == '5' || $uitypeid == '7' ) {
												if(count($currencyinfo)>0) {
													$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
												} else {
													$resultset[] = $this->convert->formatcurrency($key->columdata);
												}
											} else {
												$resultset[] = $key->columdata;
											}
										} else {
											if($editorfname=='editorfilename') {
												if( file_exists($key->columdata) ) {
													$datas = $this->geteditorfilecontent($key->columdata);
													$datas = preg_replace('~a10s~','&nbsp;', $datas);
													$datas = preg_replace('~<br>~','<br />', $datas);
													$resultset[] = $datas;
												} else {
													$resultset[] = '';
												}
											} else {
												$resultset[] = $key->columdata;
											}
										}
									}
								}
							}
						}
					}
				}
			} 
			else {
				//attribute value fetch
				$fieldnamechk = explode('-',$tabfield);
				$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
				$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
				if(count($fieldinfo)>0) {
					$uitypeid = $fieldinfo['uitype'];
					//fetch parent table join id
					$relpartable = $fieldinfo['partab'];
					$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
					$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
					$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
					//join relation table and main table
					$joinq="";
					if($relpartable!=$tablename) {
						$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id';
					}
					//main table join
					if($mainjointable==$parenttable) {
						$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id';
					} else {
						$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id';
						$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$mainjointable.'.'.$parenttable.'id';
					}
					//fetch attribute name and values
					$i=0;
					$datasets = array();
					$attrsetids = $this->db->query(" SELECT ".$tablename.".".$tablename."id as Id,".$tablename.".attributesetid as attrsetid,".$tablename.".attributes as attributeid,".$tablename.".attributevalues as attributevalueid FROM ".$tablename.$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$mainjointable.".status=1 AND ".$tablename.".status=1",false)->result();
					foreach($attrsetids as $attrset) {
						$datasets[$i] = array('id'=>$attrset->Id,'attrsetid'=>$attrset->attrsetid,'attrnames'=>$attrset->attributeid,'attrvals'=>$attrset->attributevalueid);
						$i++;
					}
					$resultset = array();
					foreach($datasets as $set) {
						$valid = explode('_',$set['attrvals']);
						$nameid = explode('_',$set['attrnames']);
						$n=1;
						$vi = 0;
						$attrdatasets = array();
						for($h=0;$h<count($valid);$h++) {
							$attrval = '';
							$attrname = '';
							if($valid[$h]!='') {
								//attribute value fetch
								$attrvaldata = $this->db->query(' SELECT attributevalueid,attributevaluename FROM attributevalue WHERE attributevalueid='.$valid[$h].' AND status=1',false)->result();
								foreach($attrvaldata as $valsets) {
									$attrval = $valsets->attributevaluename;
								}
								//attribute name fetch
								$attrnamedata = $this->db->query(' SELECT attributeid,attributename FROM attribute WHERE attributeid='.$nameid[$h].' AND status=1',false)->result();
								foreach($attrnamedata as $namesets) {
									$attrname = $namesets->attributename;
								}
							}
							$attrset = "";
							if( $attrval!='' ) {
								$attrset = $n.'.'.$attrname.': '.$attrval;
								++$n;
							}
							$attrdatasets[$vi] = $attrset;
							$vi++;
						}
						$resultset[] = implode('<br />',$attrdatasets);
					}
				}
			}
		}
		else if($table=='GEN') {
			$tablename = trim($reltabname[1]);
			$tabfield = $newval[1];
			//field fetch parent table fetch
			$fieldnamechk = explode('-',$tabfield);
			$fldname = ( (count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
			$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
			$uitypeid = ( (count($fieldinfo) > 0)? $fieldinfo['uitype'] : 1);
			if($fldname != 'attributesetid') {
				//get user company & branch id
				$tabinfo = array('company','companycf','branch','branchcf');
				if( in_array($tablename,$tabinfo) ) {
					if($tablename == 'company' || $tablename == 'companycf') {
						$compquery = $this->db->query('select company.companyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND company.status=1')->result();
						foreach($compquery as $key) {
							$id = $key->companyid;
						}
					} else if($tablename == 'branch' || $tablename == 'branchcf') {
						$brquery = $this->db->query('select branch.branchid from branch join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND branch.status=1')->result();
						foreach($brquery as $key) {
							$id = $key->branchid;
						}
					} else { //employee
						$id = $this->Basefunctions->userid;
					}
				} else { //employee
					$id = $this->Basefunctions->userid;
				}
				//check its parent child field concept
				if(in_array($fldname,$elname)) {
					$key = array_search($fldname,$elname);
					$pid = $id;
					if($key != NULL) {
						//parent id get
						$result = $this->db->select("$tname[$key].$elname[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$id)->where($tname[$key].'.status',1)->get();
						if($result->num_rows() > 0) {
							foreach($result->result()as $row) {
								$pid=$row->$elname[$key];
							}
							if($pid!=0) {
								//parent name
								$result = $this->db->select("$tname[$key].$fname[$key],$tname[$key].$fnameid[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$pid)->where($tname[$key].'.status',1)->get();
								if($result->num_rows() > 0) {
									foreach($result->result()as $row) {
										$resultset[]=$row->$fname[$key];
									}
								}
							}
						}
					}
				}
				else {
					//field fetch parent table fetch
					$fieldnamechk = explode('-',$tabfield);
					$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
					$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
					$moduleid = $fieldinfo['modid'];
					if(count($fieldinfo)>0) {
						$uitypeid = $fieldinfo['uitype'];
						//fetch parent table join id
						$relpartable = $fieldinfo['partab'];
						$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
						$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $relpartable );
						$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
						//fetch original field of picklist data in view creation 
						$viewfieldinfo = $this->viewfieldsinformation($tablename,$tabfield,$fieldinfo['modid']);
						$tabfieldname = ( (count($viewfieldinfo)>0)? $viewfieldinfo['joinfieldname'] : $tabfield );
						$jointabfieldid = ( (count($viewfieldinfo)>0)? $viewfieldinfo['condfiledname'] : $tabfield );
						//join relation table and main table
						$joinq="";
						if($relpartable!=$tablename) {
							$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id';
						}
						//if its picklist data
						$newfield = substr( $tabfieldname,( strlen($tabfieldname)-2 ));
						$tabname = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
						$tbachk = $this->tableexitcheck($database,$tabname);
						if( $newfield == "id" && $tbachk != 0 ) {
							$newtable = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
							$newjfield = $newtable."name";
							$whtable = (($tablename=="company" || $tablename=="branch")? $tablename : $relpartable);
							$newjdata = $this->db->query("SELECT ".$newtable.".".$newtable."id AS columdataid,".$newtable.".".$newjfield." AS columdata FROM ".$newtable." LEFT OUTER JOIN ".$tablename." ON ".$tablename.".".$tabfieldname." = ".$newtable.".".$tabfieldname."".$joinq." WHERE ".$tablename.".".$whtable."id = ".$id." AND ".$newtable.".status=1 AND ".$tablename.".status=1",false);
							if($newjdata->num_rows() >0) {
								foreach($newjdata->result() as $jkey) {
									$resultset[] = $jkey->columdata;
								}
							} else {
								$resultset[] = " ";
							}
						}
						else {
							//main table data sets information fetching
							$fldcountchk = explode('-',$tabfield);
							$tablcolumncheck = $this->tablefieldnamecheck($tablename,$relpartable."id");
							if( ( $relpartable == $tablename && (count($fldcountchk)) < 2 ) || ( $tablcolumncheck == 'false' && (count($fldcountchk)) < 2 ) ) {
								$newdata = $this->db->select("".$tabfield." AS columdata")->from($tablename)->where($tablename.".".$tablename."id",$id)->where($tablename.".status",1)->get()->result();
							}
							else {
								$cond = "";
								$addfield = explode('-',$tabfield);
								if( (count($addfield)) >= 2 ) {
									$datafiledname=$addfield[1];
									if(in_array('PR',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=2';
									} else if(in_array('SE',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=3'; 
									} else if(in_array('BI',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=4';
									} else if(in_array('SH',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=5';
									} else if(in_array('CW',$addfield)) {
										$formatortype = "CW";
									} else if(in_array('CF',$addfield)) {
										$formatortype = "CF";
									} else if(in_array('CS',$addfield)) {
										$formatortype = "CS";
									}
									if($tablename!=$relpartable) {
										$tablcolumncheck = $this->tablefieldnamecheck($tablename,"addresstypeid");
										$cond = ( ($tablcolumncheck == 'true')? $cond : ' ');
										$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename." JOIN ".$relpartable." ON ".$relpartable.".".$relpartable."id = ".$tablename.".".$relpartable."id WHERE ".$relpartable.".".$relpartable."id = ".$id." AND ".$tablename.".status=1 AND ".$relpartable.".status=1".$cond."")->result();
									} else {
										$newdata = $this->db->select(''.$datafiledname.' AS columdata',false)->from($tablename)->where($tablename.'id',$id)->where($tablename.'.status',1)->get()->result();
									}
								}
								else {
									$newdata = $this->db->query("SELECT ".$tabfield." AS columdata FROM ".$tablename." JOIN ".$relpartable." ON ".$relpartable.".".$relpartable."id = ".$tablename.".".$relpartable."id WHERE ".$relpartable.".".$relpartable."id = ".$id." AND ".$tablename.".status=1 AND ".$relpartable.".status=1")->result();
								}
							}
							foreach($newdata as $key) {
								if($tabfield == 'companylogo') {
									$imgname = $key->columdata;
									if(filter_var($imgname,FILTER_VALIDATE_URL)) {
										$resultset[] = '<img src="'.$imgname.'" />';
									} else {
										if( file_exists($imgname) ) {
											$resultset[] = '<img src="'.$imgname.'" style="width:11em; height:3em" />';
										} else {
											$resultset[] = " ";
										}
									}
								}
								else {
									if($formatortype=="CW") { //convert to word
										$currencyinfo = $this->currencyinformation($tablename,$id,$moduleid);
										if(count($currencyinfo)>0) {
											$resultset[] = $this->convert->numtowordconvert($key->columdata,$currencyinfo['name'],$currencyinfo['subcode']);
										} else {
											$resultset[] = $this->convert->numtowordconvert($key->columdata);
										}
									} else if($formatortype=="CF") { //currenct format
										$currencyinfo = $this->currencyinformation($tablename,$id,$moduleid);
										if(count($currencyinfo)>0) {
											$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
										} else {
											$resultset[] = $this->convert->formatcurrency($key->columdata);
										}
									} else if($formatortype=="CS") { //currency with symbol
										$currencyinfo = $this->currencyinformation($tablename,$id,$moduleid);
										if(count($currencyinfo)>0) {
											$amt = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
											$resultset[] = $currencyinfo['symbol'].' '.$amt;
										} else {
											$resultset[] = $currencyinfo['symbol'].' '.$this->convert->formatcurrency($key->columdata);
										}
									} else {
										$currencyinfo = $this->currencyinformation($tablename,$id,$moduleid);
										if(is_numeric($key->columdata)) {
											if($uitypeid == '4' || $uitypeid == '5' || $uitypeid == '7') {
												if(count($currencyinfo)>0) {
													$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
												} else {
													$resultset[] = $this->convert->formatcurrency($key->columdata);
												}
											} else {
												$resultset[] = $key->columdata;
											}
										} else {
											if($editorfname=='editorfilename') {
												if( file_exists($key->columdata) ) {
													$datas = $this->geteditorfilecontent($key->columdata);
													$datas = preg_replace('~a10s~','&nbsp;', $datas);
													$datas = preg_replace('~<br>~','<br />', $datas);
													$resultset[] = $datas;
												} else {
													$resultset[] = '';
												}
											} else {
												$resultset[] = $key->columdata;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	return $resultset;
	}
	//remove special chars
	public function removespecialchars($content) {
		$symbols = array('+','-','/','%','~','!','@','#','$','^','&','*','_','=','|','(',')');
		foreach($symbols as $symbol) {
			switch($symbol) {
				case '(':
					$content = preg_replace('~\(~','890sss', $content);
					break;
				case ')':
					$content = preg_replace('~\)~','sss890', $content);
					break;
				case '+':
					$content = preg_replace('~\+~','ssplus890', $content);
					break;
				case '~':
					$content = preg_replace('~\~~','ssstilt890', $content);
					break;
				case '!':
					$content = preg_replace('~\!~','sssastr890', $content);
					break;
				case '@':
					$content = preg_replace('~\@~','sssat890', $content);
					break;
				case '#':
					$content = preg_replace('~\#~','ssshash890', $content);
					break;
				case '^':
					$content = preg_replace('~\^~','ssscap890', $content);
					break;
				case '*':
					$content = preg_replace('~\*~','sssstar890', $content);
					break;
				case '=':
					$content = preg_replace('~\=~','ssseqw890', $content);
					break;
				case '|':
					$content = preg_replace('~\|~','ssspipe890', $content);
					break;
			}
		}
		return $content;
	}
	//remove special chars
	public function addspecialchars($content) {
		$symbols = array('+','-','/','%','~','!','@','#','$','^','&','*','_','=','|','(',')');
		foreach($symbols as $symbol) {
			switch($symbol) {
				case '(':
					$content = preg_replace('~890sss~','(', $content);
					break;
				case ')':
					$content = preg_replace('~sss890~',')', $content);
					break;
				case '+':
					$content = preg_replace('~ssplus890~','+', $content);
					break;
				case '~':
					$content = preg_replace('~ssstilt890~','~', $content);
					break;
				case '!':
					$content = preg_replace('~sssastr890~','!', $content);
					break;
				case '@':
					$content = preg_replace('~sssat890~','@', $content);
					break;
				case '#':
					$content = preg_replace('~ssshash890~','#', $content);
					break;
				case '^':
					$content = preg_replace('~ssscap890~','^', $content);
					break;
				case '*':
					$content = preg_replace('~sssstar890~','*', $content);
					break;
				case '=':
					$content = preg_replace('~ssseqw890~','=', $content);
					break;
				case '|':
					$content = preg_replace('~ssspipe890~','|', $content);
					break;
			}
		}
		return $content;
	}
	//get editor file content
	public function geteditorfilecontent($fname) {
		$tccontent="";
		$newfilename = explode('/',$fname);
		if(read_file($newfilename[0].DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = read_file($newfilename[0].DIRECTORY_SEPARATOR.$newfilename[1]);
		}
		return $tccontent;
	}
	//table name check
	public function tableexitcheck($database,$newtable) {
		$counttab=0;
		$tabexits = $this->db->query("SELECT COUNT(*) AS tabcount FROM information_schema.tables WHERE table_schema = '".$database."'AND table_name = '".$newtable."'");
		foreach($tabexits->result() as $tkey) { 
			$counttab = $tkey->tabcount;
		};
		return $counttab;
	}
	//relation module ids
	public function fetchrelatedmodulelist($moduleid) {
		$relmodids = array();
		$relmoduleids = $this->db->select('modulerelationid,relatedmoduleid')->from('modulerelation')->where('moduleid',$moduleid)->get();
		foreach($relmoduleids->result() as $reldata) {
			$relmodids[] = $reldata->relatedmoduleid;
		}
		return $relmodids;
	}
	//fetch parent table information
	public function fetchfieldparenttable($tablname,$fieldname) {
		$pardata = array();
		$datas = $this->db->query('SELECT modulefieldid,uitypeid,parenttable,moduletabid  FROM modulefield WHERE columnname LIKE "%'.$fieldname.'%" AND tablename LIKE "%'.$tablname.'%" AND status IN (1,10)',false);
		foreach($datas->result() as $data) {
			$pardata = array('uitype'=>$data->uitypeid,'partab'=>$data->parenttable,'modid'=>$data->moduletabid);
		}
		return $pardata;
	}
	//fetch main table join information
	public function fetchjoincolmodelname($relpartable,$moduleid) {
		$joindata = array();
		$datas = $this->db->query('SELECT modulerelationid,parentfieldname,parenttable FROM modulerelation WHERE moduleid = '.$moduleid.' AND relatedtable LIKE "%'.$relpartable.'%" AND status=1',false);
		foreach($datas->result() as $data) {
			$joindata = array('relid'=>$data->modulerelationid,'joinfieldname'=>$data->parentfieldname,'jointabl'=>$data->parenttable);
		}
		return $joindata;
	}
	//fetch relation and view type 3 field information
	public function viewfieldsinformation($tablename,$tabfield,$modid) {
		$datasets = array();
		$viewfieldinfo = $this->db->query('SELECT viewcreationcolumnid,viewcreationparenttable,viewcreationjoincolmodelname,viewcreationcolmodelcondname FROM viewcreationcolumns WHERE viewcreationparenttable LIKE "%'.$tablename.'%" AND viewcreationcolmodelcondname LIKE "%'.$tabfield.'%" AND viewcreationtype=3 AND viewcreationmoduleid='.$modid.' AND status=1');
		foreach($viewfieldinfo->result() as $viewdata) {
			$datasets = array('viewcolid'=>$viewdata->viewcreationcolumnid,'joinfieldname'=>$viewdata->viewcreationjoincolmodelname,'condfiledname'=>$viewdata->viewcreationcolmodelcondname);
		}
		return $datasets;
	}
	//fetch currency information
	public function currencyinformation($parenttable,$id,$moduleid) {
		$datasets = array();
		$colmodeltable = "";
		$cid = 1;
		//modulerelation
		$datasets = $this->db->select('viewcreationcolumnid,viewcreationcolmodeltable,viewcreationjoincolmodelname,viewcreationparenttable,viewcreationtype,viewcreationcolmodelcondname',false)->from('viewcreationcolumns')->where('viewcreationmoduleid',$moduleid)->where('viewcreationjoincolmodelname','currencyid')->where('status',1)->get();
		if($datasets->num_rows()>0) {
			foreach($datasets->result() as $data) {
				$colname =  $data->viewcreationcolmodeltable;
				$colmodeltable = $data->viewcreationcolmodeltable;
				$joincolmodelname= $data->viewcreationjoincolmodelname;
				$colmodelpartable = $data->viewcreationparenttable;
				$viewtype = $data->viewcreationtype;
				$viewcolcondname = $data->viewcreationcolmodelcondname;
			}
		}
		if($colmodeltable!= "") {
			$joinq="";
			if($viewtype==3) {
				if($colmodelpartable!="currency") {
					if($colmodelpartable == $parenttable) {
						$joinq = 'LEFT OUTER JOIN '.$colmodelpartable.' ON '.$colmodelpartable.'.'.$viewcolcondname.'='.$colmodeltable.'.'.$joincolmodelname;
					} else {
						$joinq = 'LEFT OUTER JOIN '.$colmodelpartable.' ON '.$colmodelpartable.'.'.$viewcolcondname.'='.$colmodeltable.'.'.$joincolmodelname;
						$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$colmodelpartable.'.'.$parenttable.'id';
					}
				}
			} else {
				if($colmodelpartable!="currency") {
					if($colmodelpartable == $parenttable) {
						$joinq = 'LEFT OUTER JOIN '.$colmodelpartable.' ON '.$colmodelpartable.'.'.$joincolmodelname.'='.$colmodeltable.'.'.$joincolmodelname;
					} else {
						$joinq = 'LEFT OUTER JOIN '.$colmodelpartable.' ON '.$colmodelpartable.'.'.$joincolmodelname.'='.$colmodeltable.'.'.$joincolmodelname;
						$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$colmodelpartable.'.'.$parenttable.'id';
					}
				}
			}
			$curdatasets = $this->db->query('select currency.currencyid from currency '.$joinq.' where '.$parenttable.'id='.$id.'')->result();
			foreach($curdatasets as $data) {
				$cid = $data->currencyid;
			}
		} else {
			$empid = $this->Basefunctions->userid;
			$compquery = $this->db->query('select company.companyid,company.currencyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.'')->result();
			foreach($compquery as $key) {
				$cid = $key->currencyid;
			}
		}
		$curdatasets = $this->db->select('currencyid,currencyname,currencycode,symbol,currencycountry,subunit',false)->from('currency')->where('currencyid',$cid)->get();
		foreach($curdatasets->result() as $datas) {
			$datasets = array('id'=>$datas->currencyid,'name'=>$datas->currencyname,'code'=>$datas->currencycode,'symbol'=>$datas->symbol,'country'=>$datas->currencycountry,'subcode'=>$datas->subunit);
		}
		return $datasets;
	}
	//table field name check
	public function tablefieldnamecheck($tblname,$fieldname) {
		$result = 'false';
		$fields = $this->db->field_data($tblname);
		foreach ($fields as $field) {
			$name =  $field->name;
			if($fieldname == $name) {
				$result = 'true';
			}
		}
		return $result;
	}
}