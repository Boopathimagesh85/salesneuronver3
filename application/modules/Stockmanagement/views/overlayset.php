<!--Product Search Grid---->
<div class="large-6 columns" style="position:absolute;">
	<div class="overlay" id="productsearchoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">	
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="large-6 medium-6 small-11 large-centered medium-centered small-centered columns overlayborder">
			<div class="row">
			<div class="large-12 columns sectionheaderformcaptionstyle" style="line-height:1rem;">
				Product Search
			</div>
			<?php
				$device = $this->Basefunctions->deviceinfo();
				if($device=='phone') {
					echo '<div class="large-12 columns paddingzero forgetinggridname" id="sproductsearchgridwidth"><div id="sproductsearchgrid" class=" inner-row-content inner-gridcontent" style="max-width:2000px; height:300px;top:0px;">
						<!-- Table header content , data rows & pagination for mobile-->
					</div>
					<!--<footer class="inner-gridfooter footercontainer" id="sproductsearchgridfooter">
						 Footer & Pagination content
					</footer> --></div>';
				} else {
					echo '<div class="large-12 columns forgetinggridname" id="sproductsearchgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="sproductsearchgrid" style="max-width:2000px; height:300px;top:0px;">
					<!-- Table content[Header & Record Rows] for desktop-->
					</div>
					<!-- <div class="inner-gridfooter footer-content footercontainer" id="sproductsearchgridfooter">
						Footer & Pagination content 
					</div>--></div>';
				}
			?>
			</div>
			<div class="row" style="background:#465a63">&nbsp;</div>
			<div class="row" style="background:#465a63">&nbsp;</div>
			<div class="row" style="background:#465a63">
				<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
					<span class="firsttab" tabindex="1000"></span>
					<input type="button" id="productsearchsubmit" name="" value="Submit" class="alertbtn" tabindex="1001">	
					<input type="button" id="productsearchclose" name="" value="Cancel" tabindex="1002" class="alertbtn  alertsoverlaybtn" >
					<span class="lasttab" tabindex="1003"></span>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- UOM Conversion Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="uomconvoverlay" style="overflow-y: scroll;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="" >
			<form method="POST" name="" style="" id="" action="" enctype="" class="">
				<span id="uomconv_validate" class="validationEngineContainer"> 
					<div class="alert-panel" style="background-color:#465a63">
						<div class="alertmessagearea">
							<div class="alert-title">UOM Conversion</div>
							<div class="alert-message" style="height:100%">
								<span class="firsttab" tabindex="1000"></span>
								<div class="static-field overlayfield"> 
									<label>Default UOM<span class="mandatoryfildclass">*</span></label>
									<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd" tabindex="1001" name="uomid" id="uomid">
										<option value=""></option>
										<?php foreach($uom_conv as $key):?>
											<option data-uomidhidden="<?php echo $key->uomname;?>" value="<?php echo $key->uomid;?>">
											<?php echo $key->uomname;?></option>
										<?php endforeach;?>	
									</select>
								</div>
								<div class="static-field overlayfield"> 
									<label>To UOM<span class="mandatoryfildclass">*</span></label>
									<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange" tabindex="1002" name="touomid" id="touomid">
										<option value=""></option>										
									</select>
								</div>
								<div class="input-field overlayfield">
									<input id="uomconversionrate" class="validate[maxSize[100]]" type="text" data-prompt-position="bottomLeft" name="uomconversionrate">
									<label for="uomconversionrate">UOM Conversion Rate</label>
								</div>
								<div class="input-field overlayfield">
									<input id="conversionquantity" class="validate[maxSize[100]]" type="text" data-prompt-position="bottomLeft" name="conversionquantity">
									<label for="conversionquantity">Conversion Quantity</label>
								</div>
							</div>
						</div>
						<div class="alertbuttonarea">
							<input type="button" id="uomconv_submit" name="" value="Submit" tabindex="1004" class="alertbtn flloop" >
							<input type="button" id="uomconvclose" name="" value="Cancel" tabindex="1005" class="flloop alertbtn alertsoverlaybtn" >
							<span class="lasttab" tabindex="1006"></span>
						</div>
					</div>
				</span>
			</form>
		</div>
	</div>
</div>
<!--Modulenumber Search Grid---->
<div class="large-6 columns" style="position:absolute;">
	<div class="overlay" id="posearchoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="large-6 medium-6 small-11 large-centered medium-centered small-centered columns overlayborder">
			<div class="row">
			<div class="large-12 columns sectionheaderformcaptionstyle" style="line-height:1rem;">
				Purchase Order Search
			</div>
			<?php
				$device = $this->Basefunctions->deviceinfo();
				if($device=='phone') {
					echo '<div class="large-12 columns paddingzero forgetinggridname" id="ponumbersearchgridwidth"><div id="ponumbersearchgrid" class=" inner-row-content inner-gridcontent" style="max-width:2000px; height:300px;top:0px;">
						<!-- Table header content , data rows & pagination for mobile-->
					</div>
					<!--<footer class="inner-gridfooter footercontainer" id="ponumbersearchgridfooter">
						 Footer & Pagination content
					</footer> --></div>';
				} else {
					echo '<div class="large-12 columns forgetinggridname" id="ponumbersearchgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="ponumbersearchgrid" style="max-width:2000px; height:300px;top:0px;">
					<!-- Table content[Header & Record Rows] for desktop-->
					</div>
					<!-- <div class="inner-gridfooter footer-content footercontainer" id="ponumbersearchgridfooter">
						Footer & Pagination content 
					</div>--></div>';
				}
			?>
			</div>
			<div class="row" style="background:#465a63">&nbsp;</div>
			<div class="row" style="background:#465a63">&nbsp;</div>
			<div class="row" style="background:#465a63">
				<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
					<span class="firsttab" tabindex="1000"></span>
					<input type="button" id="posearchsubmit" name="" value="Submit" class="alertbtn" tabindex="1001">	
					<input type="button" id="posearchclose" name="" value="Cancel" tabindex="1002" class="alertbtn  alertsoverlaybtn" >
					<span class="lasttab" tabindex="1003"></span>
				</div>
			</div>
		</div>
	</div>
</div>