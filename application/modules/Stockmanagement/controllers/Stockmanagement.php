<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Stockmanagement extends MX_Controller{
	public $stockmanagementmodule = 37;
	//To load the Register View
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Stockmanagement/Stockmanagementmodel');
		$this->load->view('Base/formfieldgeneration');
		$this->load->library('pdf');
		$this->load->library('convert');
    }
	public function index() {
		$moduleid = array($this->stockmanagementmodule);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array($this->stockmanagementmodule);
		$viewmoduleid = array($this->stockmanagementmodule);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['uom_conv']=$this->Basefunctions->simpledropdown('uom','uomid,uomname','uomname');
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['dashboradviewdata'] = $this->Basefunctions->dashboarddataviewbydropdown($moduleid);
		$this->load->view('Stockmanagement/stockmanagementview',$data);			
	}	
	//product grid data fetch
	public function producrgriddisplay() {		
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$poid = $_GET['poid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'product.productid') : 'product.productid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'ASC') : 'ASC';
		$colinfo = array('colmodelname'=>array('productname','categoryname','statusname'),'colmodelindex'=>array('product.productname','category.categoryname','status.status'),'coltablename'=>array('productname','categoryname','statusname'),'uitype'=>array('2','2','2'),'colname'=>array('Product Name','Category','Status'),'colsize'=>array('200','200','200'));
		$result=$this->Stockmanagementmodel->producrgriddisplaymodel($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$poid);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Product List',$width,$height);
		} else {
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Product List',$width,$height);
		}
		echo json_encode($datas);
	
	}
	//purchase order data fetch
	public function purchaseorderdatafetch() {
		$this->Stockmanagementmodel->purchaseorderdatafetchmodel();
	}
	//branch based product drop down value fetch
	public function productddvalfetch() {
		$this->Stockmanagementmodel->productddvalfetchmodel();
	}
	//product data fetch
	public function productbaseddatafetch() {
		$this->Stockmanagementmodel->productbaseddatafetchmodel();
	}	
	//product based branch fetch
	public function batchdatafetch() {
		$this->Stockmanagementmodel->batchdatafetchmodel();
	}
	//stock management create_function
	public function stockmanagementcreate() {
		$this->Stockmanagementmodel->stockmanagementcreatemodel();
	}
	//stock management delete
	public function stockmanagementdelete() {
		$this->Stockmanagementmodel->stockmanagementdeletemodel();
	}
	//stock management edit data fetch
	public function stockmanagementeditdatafetch() {
		$this->Stockmanagementmodel->stockmanagementeditdatafetchmodel();
	}
	//stock management update
	public function stockmanagementupdate() {
		$this->Stockmanagementmodel->stockmanagementupdatemodel();
	}
	//user based branch fetch
	public function userbasedbranchfetch() {
		$this->Stockmanagementmodel->userbasedbranchfetchmodel();
	}
	//purchase order drop down value fetch
	public function purchaseorderddvlfetch() {
		$this->Stockmanagementmodel->purchaseorderddvlfetchmodel();
	}
}