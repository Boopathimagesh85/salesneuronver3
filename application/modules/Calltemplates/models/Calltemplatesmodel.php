<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Model File
class calltemplatesmodel extends CI_Model
{    
    public function __construct() {
		parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//To Create New data
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$restricttable = explode(',',$_POST['resctable']);
		$result = $this->Crudmodel->datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable);
		echo 'TRUE';
	}
	//information fetch
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementpartable']);
		$restricttable = explode(',',$_GET['resctable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['dataprimaryid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetchwithrestrict($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$restricttable,$moduleid);
		echo $result;
	}
	//update data information
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['primarydataid'];
		$restricttable = explode(',',$_POST['resctable']);
		$result = $this->Crudmodel->dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable);
		echo $result;
	}
	//delete old information
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				echo "TRUE";
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			echo "TRUE";
		}
	}
	//drop down value set with multiple condition
	public function fetchmaildddatawithmultiplecondmodel() {
		$i=0;
		$dname=$_GET['dataname'];
		$did=$_GET['dataid'];
		$dataattrname1=$_GET['othername1'];
		$dataattrname2=$_GET['othername2'];
		$table=$_GET['datatab'];
		$whfield=$_GET['whdatafield'];
		$whdata=$_GET['whval'];
		$mulcond=explode(",",$whfield);
		$mulcondval=explode(",",$whdata);
		$i=0;
		$this->db->select("$dname,$did,$dataattrname1,$dataattrname2");
		$this->db->from($table);
		foreach($mulcond AS $condname) {
			if($mulcondval[$i] != "0" && $mulcondval[$i] != "") {
				$this->db->where($condname,$mulcondval[$i]);
			}
			$i++;
		}
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {		
			foreach($result->result()as $row) {
				$data[$i]=array('datasid'=>$row->$did,'dataname'=>$row->$dname,'otherdataattrname1'=>$row->$dataattrname1,'otherdataattrname2'=>$row->$dataattrname2);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//Folder Insertion
	public function calltemplatesfolderinsertmodel() {
		$foldername = $_POST['foldername'];
		$description = $_POST['description'];
		$default = $_POST['setdefault'];
		$date = date($this->Basefunctions->datef);
		$folderarray = array(
				'foldernamename'=>$foldername,
				'description'=>$description,
				'moduleid'=>'39',
				'setdefault'=>$default,
				'userroleid'=>$this->Basefunctions->userroleid,
				'createdate'=>$date,
				'lastupdatedate'=>$date,
				'createuserid'=>$this->Basefunctions->userid,
				'lastupdateuserid'=>$this->Basefunctions->userid,
				'status'=>$this->Basefunctions->activestatus
			);
		$this->db->insert('foldername',$folderarray);
		$primaryid = $this->db->insert_id();
		if($default == 'Yes') {
			$defultset = array(
					'setdefault'=>'No',
					'lastupdatedate'=>date($this->Basefunctions->datef),
					'lastupdateuserid'=>$this->Basefunctions->userid,
					'status'=>$this->Basefunctions->activestatus
				);
			$this->db->where_not_in('foldername.foldernameid',array($primaryid));
			$this->db->where_in('foldername.moduleid',array(39));
			$this->db->where_in('foldername.status',array(1));
			$this->db->update('foldername',$defultset);
		}
		echo "TRUE";
	}
	//Folder main grid view
	public function calltemplatesfoldernamegridfetchxmlview($tablename,$sortcol,$sortord,$pagenum,$rowscount) {
		$dataset = 'foldername.foldernamename,foldername.foldernameid,foldername.description,foldername.setdefault';
		$join ='';
		$status = $tablename.'.status IN (1)';
		$where = $tablename.'.moduleid IN (39)';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		//query
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$status.'AND '.$where.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		return $data;
	}
	//Folder Edit Data Fetch
	public function calltemplatefolderdatafetchmodel() {
		$folderid = $_GET['datarowid'];
		$this->db->select('SQL_CALC_FOUND_ROWS  foldername.foldernamename,foldername.foldernameid,foldername.description,foldername.setdefault',false);
		$this->db->from('foldername');
		$this->db->where('foldername.foldernameid',$folderid);
		$this->db->where('foldername.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data=array('id'=>$row->foldernameid,'foldername'=>$row->foldernamename,'description'=>$row->description,'setdefault'=>$row->setdefault);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//Folder Update
	public function calltemplatesfolderupdatemodel() {
		$foldernameid = $_POST['foldernameid'];
		$foldername = $_POST['foldername'];
		$description = $_POST['description'];
		$default = $_POST['setdefault'];
		$date = date($this->Basefunctions->datef);
		$folderarray = array(
				'foldernamename'=>$foldername,
				'description'=>$description,
				'setdefault'=>$default,
				'userroleid'=>$this->Basefunctions->userroleid,
				'createdate'=>$date,
				'lastupdatedate'=>$date,
				'createuserid'=>$this->Basefunctions->userid,
				'lastupdateuserid'=>$this->Basefunctions->userid,
				'status'=>$this->Basefunctions->activestatus
			);
		$this->db->where('foldername.foldernameid',$foldernameid);
		$this->db->update('foldername',$folderarray);
		if($default == 'Yes') {
			$defultset = array(
					'setdefault'=>'No',
					'lastupdatedate'=>date($this->Basefunctions->datef),
					'lastupdateuserid'=>$this->Basefunctions->userid,
					'status'=>$this->Basefunctions->activestatus
				);
			$this->db->where_not_in('foldername.foldernameid',array($foldernameid));
			$this->db->where_in('foldername.moduleid',array(39));
			$this->db->where_in('foldername.status',array(1));
			$this->db->update('foldername',$defultset);
		}
		echo "TRUE";
	}
	//Folder Delete
	public function folderdeleteoperationmodel() {
		$folderid = $_GET['id'];
		$date = date($this->Basefunctions->datef);
		$folderarray = array(
				'lastupdatedate'=>$date,
				'lastupdateuserid'=>$this->Basefunctions->userid,
				'status'=>0
			);
		$this->db->where('foldername.foldernameid',$folderid);
		$this->db->update('foldername',$folderarray);
		echo "TRUE";
	}
	//Folder Drop Down Reload function
	public function fetchdddataviewddvalmodel() {
		$i = 0;
		$fieldname = $_GET['dataname'];
		$fieldid = $_GET['dataid'];
		$tablename = $_GET['datatab'];
		$moduleid = $_GET['moduleid'];
		$this->db->select("$fieldid,$fieldname");
		$this->db->from("$tablename");
		$this->db->where_in("$tablename".'.moduleid',array($moduleid));
		$this->db->where("$tablename".'.status',1);
		$result=$this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data[$i]=array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		} 
	}
	//Related Modules id get
	public function relatedmoduleidgetmodel() {
		$moddatas = array();
		$relmodids = array();
		$moduleid = $_POST['moduleid'];
		$genmod = array(2,3,265);
		$relmoduleids = $this->db->select('modulerelationid,relatedmoduleid')->from('modulerelation')->where('moduleid',$moduleid)->get();
		foreach($relmoduleids->result() as $reldata) {
			$relmodids[] = $reldata->relatedmoduleid;
		}
		$i=0;
		$modids = implode(',',$relmodids);
		$moduleids = ( ($modids!="")? $modids.','.$moduleid : $moduleid );
		$modids = explode(',',$moduleids);
		$allmodid = array_merge($modids,$genmod);
		$allmoduleids = implode(',',array_unique($allmodid));
		$modinfo = $this->db->query('select module.moduleid,module.modulename from module join moduleinfo ON moduleinfo.moduleid=module.moduleid where module.moduleid IN ('.$allmoduleids.') AND module.status=1 AND moduleinfo.status=1 group by moduleinfo.moduleid',false);
		foreach($modinfo->result() as $moddata) {
			$modtype = ( ($moddata->moduleid==$moduleid)?'':((in_array($moddata->moduleid,$genmod))?'GEN:':'REL:') );
			$moddatas[$i] = array('datasid'=>$moddata->moduleid,'dataname'=>$moddata->modulename,'datatype'=>$modtype);
			$i++;
		}
		$datas = ( ($i==0)? json_encode(array('fail'=>'FAILED')) : json_encode($moddatas) );
		echo $datas;
	}
}