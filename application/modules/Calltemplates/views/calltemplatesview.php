<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
</head>
<body>
	<?php
		$device = $this->Basefunctions->deviceinfo();
		$dataset['gridenable'] = "yes"; //no
		$dataset['griddisplayid'] = "calltemplatescreationview";
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['spanattr'] = array();
		$dataset['gridtableid'] = "calltemplatesviewgrid";
		$dataset['griddivid'] = "calltemplatesviewgridnav";
		$dataset['moduleid'] = '39';
		$dataset['forminfo'] = array(array('id'=>'calltemplatescreationformadd','class'=>'hidedisplay','formname'=>'calltemplatescreationform'));
		$this->load->view('Base/basedeleteform');
		if($device=='phone') {
			$this->load->view('Base/gridmenuheadermobile',$dataset);
			$this->load->view('Base/overlaymobile');
			$this->load->view('Base/viewselectionoverlay');
		} else {
			$this->load->view('Base/gridmenuheader',$dataset);
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
	?>
</body>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js Files -->	
	<script src="<?php echo base_url();?>js/Calltemplates/calltemplates.js" type="text/javascript"></script> 	
	<!--For Tablet and Mobile view Dropdown Script-->
	<script>
		$(document).ready(function(){
			$("#tabgropdropdown").change(function(){
				var tabgpid = $("#tabgropdropdown").val(); 
				$('.sidebaricons[data-subform="'+tabgpid+'"]').trigger('click');
			});
		});
	</script>
	<script>
		$(document).ready(function(){
			$("#tab1").click(function()	{smsmastertabid=1; smsmasterfortouch = 0;});
			$("#tab2").click(function()	{smsmastertabid=2; smsmasterfortouch = 2;});				
		});		
	</script>
</html>