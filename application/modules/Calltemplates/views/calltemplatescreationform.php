<?php
$device = $this->Basefunctions->deviceinfo();
if($device=='phone') {
	echo '<div class="large-12 columns headercaptionstyle headerradius touchsmsreload paddingzero">
			<div class="large-6 medium-6 small-12 columns headercaptionleft">
				<span  data-activates="slide-out" class="headermainmenuicon button-collapse" title="Menu"><i class="material-icons">menu</i></span>
				<span class="gridcaptionpos"><span>'.$gridtitle.'</span></span>
			</div>';
			$this->load->view('Base/formheadericons');
	echo '</div>';
} else {
	echo '<div class="large-12 columns headercaptionstyle headerradius touchsmsreload paddingzero">
			<div class="large-6 medium-6 small-6 columns headercaptionleft">
				<span data-activates="slide-out" class="headermainmenuicon button-collapse" title="Menu"><i class="material-icons">menu</i></span>
				<span class="gridcaptionpos"><span>'.$gridtitle.'</span></span>
			</div>';
			$this->load->view('Base/formheadericons');
	echo '</div>';
}
?>
<!-- tab group creation -->
<div class="large-12 columns tabgroupstyle show-for-large-up">
	<?php
		$ulattr = array("class"=>"tabs");
		$uladdinfo = "data-tab";
		$tabgrpattr = array("class"=>"tab-title sidebaricons");
		$tabstatus = "active";
		$dataname = "subform";
		echo tabgroupgenerate($ulattr,$uladdinfo,$tabgrpattr,$tabstatus,$dataname,$modtabgrp);
		//Value Getting for Tablet and mobile Dropdown
		$dropdowntabval = array();
		$m=1;
		foreach($modtabgrp as $value) {
			$dropdowntabval[$m]=$value['tabgrpname'];
			$m++;
		}
	?>
</div>
<!--For Tablet and Mobile view Dropdown-->
<div class="large-12 columns tabgroupstyle centertext show-for-medium-down tabgrpddstyle">
	<select id="tabgropdropdown" class="chzn-select" style="width:40%">
		<?php
			foreach($dropdowntabval as $tabid=>$tabname) {
				echo '<option value="'.$tabid.'">'.$tabname.'</option>';
			}
		?>
	</select>
</div>
<div class="large-12 columns addformunderheader">&nbsp;</div>
<div class="large-12 columns scrollbarclass addformcontainer smsmastertouchcontainer">
	<div class="row">&nbsp;</div>
	<form method="POST" name="dataaddform" class="" action ="" id="dataaddform" enctype="multipart/form-data">
		<span id="formaddwizard" class="validationEngineContainer" >
			<span id="formeditwizard" class="validationEngineContainer">
				<?php
					//function call for form fields generation
					formfieldstemplategenerator($modtabgrp);
				?>
			</span>
		</span>
		<!--hidden values-->
		<?php
			echo hidden('primarydataid','');
			echo hidden('sortorder','');
			echo hidden('sortcolumn','');
			$value = '';
			echo hidden('resctable',$value);
			$val = implode(',',$filedmodids);
			echo hidden('viewfieldids',$val);
		?>
	</form>
</div>