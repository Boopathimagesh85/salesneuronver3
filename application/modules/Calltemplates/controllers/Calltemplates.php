<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Calltemplates extends MX_Controller
{
	//To load the Register View
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Calltemplates/Calltemplatesmodel');
	 }
	public function index() {
		sessionchecker(39);
		$moduleid = 39;
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(39);
		$viewmoduleid = array(39);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Calltemplates/calltemplatesview',$data);
	}
	//Create New data
	public function newdatacreate() {
    	$this->Calltemplatesmodel->newdatacreatemodel();
    }
	//information fetch
	public function fetchformdataeditdetails() {
		$moduleid = 39;
		$this->Calltemplatesmodel->informationfetchmodel($moduleid);
	}
	//Update old data
    public function datainformationupdate() {
        $this->Calltemplatesmodel->datainformationupdatemodel();
    }
	//delete old information
    public function deleteinformationdata() {
		$moduleid = 39;
        $this->Calltemplatesmodel->deleteoldinformation($moduleid);
    }
	//drop down value set with multiple condition
	public function fetchmaildddatawithmultiplecond() {
		$this->Calltemplatesmodel->fetchmaildddatawithmultiplecondmodel();
	}
	//Folder insertion
	public function calltemplatesfolderinsert() {
		$this->Calltemplatesmodel->calltemplatesfolderinsertmodel();
	}
	//Folder Grid View
	public function calltemplatesfoldernamegridfetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'foldername.foldernameid') : 'foldername.foldernameid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('foldernamename','description','setdefault'),'colmodelindex'=>array('foldername.foldernamename','foldername.description','foldername.setdefault'),'coltablename'=>array('foldername','foldername','foldername'),'uitype'=>array('2','2','2'),'colname'=>array('Folder Name','Description','Default'),'colsize'=>array('300','300','205'));
		$result=$this->Calltemplatesmodel->calltemplatesfoldernamegridfetchxmlview($primarytable,$sortcol,$sortord,$pagenum,$rowscount);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($result,$page,'');
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Folder List',$width,$height);
		} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Folder List',$width,$height);
		}
		echo json_encode($datas);
	}
	//Folder Data Fetch
	public function calltemplatefolderdatafetch() {
		$this->Calltemplatesmodel->calltemplatefolderdatafetchmodel();
	}
	//Folder Update
	public function calltemplatesfolderupdate() {
		$this->Calltemplatesmodel->calltemplatesfolderupdatemodel();
	}
	//Folder Delete
	public function folderdeleteoperation() {
		$this->Calltemplatesmodel->folderdeleteoperationmodel();
	}
	//Folder drop down Fetch Data
	public function fetchdddataviewddval() {
		$this->Calltemplatesmodel->fetchdddataviewddvalmodel();
	}
	//Related module get
	public function relatedmoduleidget() {
		$this->Calltemplatesmodel->relatedmoduleidgetmodel();
	}
	//editor value fetch
	public function editervaluefetch() {
		$filename = $_GET['filename'];
		$newfilename = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]);
			echo json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			echo json_encode($tccontent);
		}
	}
}