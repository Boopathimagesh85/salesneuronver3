<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Currencyconversionmodel extends CI_Model{
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//currency conversion create
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['currencyconversionelementsname']);
		$formfieldstable = explode(',',$_POST['currencyconversionelementstable']);
		$formfieldscolmname = explode(',',$_POST['currencyconversionelementscolmn']);
		$elementpartable = explode(',',$_POST['currencyconversionelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		//check the rate exists already.
		$id=$this->db->select('currencyconversionid')
		->from('currencyconversion')
		->where(array('currencyid'=>$_POST['currencyid'],'currencytoid'=>$_POST['currencytoid'],'status'=>$this->Basefunctions->activestatus))
		->limit(1)
		->get();
		if($id->num_rows() > 0) {
			//if records exits previously update the records
			foreach($id->result() as $info){
				$conversionid= $info->currencyconversionid;
			}
			$value_array=array('validfrom'=>trim($_POST['validfrom']),'validto'=>trim($_POST['validto']),'multiplyrate'=>trim($_POST['multiplyrate']),'lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid);
			$this->db->where('currencyconversionid',$conversionid);
			$this->db->update('currencyconversion',$value_array);
		}
		else{
			$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
		}
		echo 'TRUE';
	}
	//Retrive currency conversion data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['currencyconversionelementsname']);
		$formfieldstable = explode(',',$_GET['currencyconversionelementstable']);
		$formfieldscolmname = explode(',',$_GET['currencyconversionelementscolmn']);
		$elementpartable = explode(',',$_GET['currencyconversionelementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['currencyconversionprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	//currency conversion update
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['currencyconversionelementsname']);
		$formfieldstable = explode(',',$_POST['currencyconversionelementstable']);
		$formfieldscolmname = explode(',',$_POST['currencyconversionelementscolmn']);
		$elementpartable = explode(',',$_POST['currencyconversionelementspartabname']);
	    //filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['currencyconversionprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		$convprimaryid = $_POST['currencyconversionprimarydataid'];
		$currencyid = $_POST['currencyid'];
		$currencytoid = $_POST['currencytoid'];
		$ludate = date($this->Basefunctions->datef);
		$luuserid= $this->Basefunctions->userid;
		$delete=array('status'=>0,'lastupdatedate'=>$ludate,'lastupdateuserid'=>$luuserid);
		$this->db->where_in('currencyid',$currencyid);
		$this->db->where_in('currencytoid',$currencytoid);
		$this->db->where_not_in('currencyconversionid',$convprimaryid);
		$this->db->update('currencyconversion',$delete);
		echo $result; 
	}
	//currency conversion delete
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['currencyconversionelementstable']);
		$parenttable = explode(',',$_GET['currencyconversionelementspartable']);
		$id = $_GET['currencyconversionprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				echo "TRUE";
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			echo "TRUE";
		}
	} 	
	//currency dd load
	public function currencyddloadmodel() {
		$i=0;
		$this->db->select('currencyid,CONCAT(currencycountry) as currency',false);
		$this->db->from('currency');
		$this->db->where('currency.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->currencyid,'dataname'=>$row->currency);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		} 
	}
}