<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Currencyconversion extends MX_Controller{
    public function __construct() {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Currencyconversion/Currencyconversionmodel');	
    }
    //first basic hitting view
    public function index() {        
    	$moduleid = array(215);
    	sessionchecker(215);
		//action
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(215);
		$viewmoduleid = array(215);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
      	$this->load->view('Currencyconversion/currencyconversionview',$data);
	}
	//create currency conversion
	public function newdatacreate() {  
    	$this->Currencyconversionmodel->newdatacreatemodel();
    }
	//information fetch currency conversion
	public function fetchformdataeditdetails() {
		$moduleid = array(215);
		$this->Currencyconversionmodel->informationfetchmodel($moduleid);
	}
	//update currency conversion
    public function datainformationupdate() {
        $this->Currencyconversionmodel->datainformationupdatemodel();
    }
	//delete currency conversion
    public function deleteinformationdata() {
    	$moduleid = array(215);
	  	$this->Currencyconversionmodel->deleteoldinformation($moduleid);
    } 
	//currency conversion dd load
	public function currencyddload() {
        $this->Currencyconversionmodel->currencyddloadmodel();
    }
}