<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('Base/headerfiles'); ?>
	<style>
		#validfrom,#validto {
		background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAARJSURBVHja7FZNiJVVGH7O33e+737fd+937/XOOFdLRZwKLUMwNCIJC6pF0KJFhmG0cOeqJCjctDBctIgsNIjIqGgV1EYkISoqkxAzimlInElH5/7/zP1+zzlttPTOnZlVrnp35+W853mf53kP5xBjDP7roLgNcVtA+M2Lh/YcAwDcWU7fuNqxDmbKEGkRNeaGL1xq2ieGi9eVor3zC877cWI4Z8SsLiRHZhriFQD49uP9yzORjr8dMCi5yTmlQB2vsH3UPscrbFcKrOwm5wzM9boVmIw7jS1MBm/Nt9Kdvm3mSdreJcX4tXo727t2Fd891JSut7OqFCYmaXtX3q5Mz7fSB6uBOq3i9gEAF0aCpDQ4ea3NqoCCFFTaojI7iIzdC5UNIBjVJaMAtSuzUUz9OFWkA/ZIyQ1OAlgzEqQb8SpnBrZArI2hcUo8WyAFsNyckzglHqMmc6XRcUpkN+LVJeUiBKZoh19kYfM5LmQRzMAYA85YzDhLtNZcKeVQSkNGWaa1tpRWgls8BIhWKht4lv95M7QfXxKEEqDgu1N/9J0+FPo38pvGk7EgKFXDcNC5MKNnJyfURD5fGBss9OdbreZcqVxZJ4TlJnE0nWp6vh2ppUEAgAtLAtEtuV7EvmzORfc4FqYm3Paz3UH+s8ZCuNGV5CwIPXi5oU4BMavkyWHGaB9Qy19GY/Qi0RMtNjW6mTeIyWSxVMzFim9sdJUXJmQzJbTc7JlCs6c8DXYfgIVhD/koI4cTxZx6s+hZO4hRv6ZJ8mfZs46WfHEvMel3jLi/bHTZpwakxJC8p43YPXzGIpAoCsNxp3V/adWaPQa6Gw8676QJfiwEuXYapzONelN7+eIZz7cvR2H0e6vZWAjK46c5Fw7RZFaBiGUvIwBDCeap8A/9Nhs/DQCTE/b3nR75aOZiWCl6tJ93/YcbC9aJi/UwNxaIOW3Ivqkr5jiQYFOVfyI4fhqWa9gTIqVNPTf3FyWAI4kSlhU6ktcYBSyOrp/3246kbUYBTtEqFAr9nCSKEgAwdQArMkGSpBNJ1Du8ef2qr2B0e9Dv/WwBz2zZUNyQxNHVeq026/uFp1av91YncXSJMXt6g0N3U0rzKkvOKk1fXtETpbJOnJoXrzSylyxOejYxTwwSeqRfT3c6glzIlHq+H+HtTpTd5Vjkh5mafhLQX9+ov/uOxZ4My2Usy2pL293W6KrCXDNbK51cNYN8oNbJgl6EbUFQ9Aep2FrrZMVBTHeMGE61rFzagGRZyn3XOr5e0Alj1EAKdqbs63e9HHlUUHN+ek6cn1zDPiz62CqI+qbeXTyd2rBbYW9+4x/bdyyVXLXGAn5IStnVSlEDUgVMkxASA5AGtEKgawBio7UEZWMwBoRQk8Qxq3WzA3HGSqc+2M9HMpG69lqqKq/P1nEUJDbXB/GGiQYAMf8mrq/VP5sMAEaYkrr26pJM/v+trBR/DwDGn/6x/WnnnwAAAABJRU5ErkJggg=="); 
		background-repeat: no-repeat; 
		background-position: right center;
		padding-right: 17px;
		cursor:pointer;
	}
	</style>
</head>
<body>
<?php
		$dataset['gridenable'] = "1"; //0-no  1-yes
		$dataset['griddisplayid'] = "currencyconversionaddformview"; //add form-div id
		$dataset['maingridtitle'] = $gridtitle['title'];   // form header
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];  //grid header
		$dataset['spanattr'] = array();
		$dataset['gridtableid'] = "currencyconversionaddgrid"; //grid id
		$dataset['griddivid'] = "currencyconversionaddgridnav"; //grid pagination
		$this->load->view('currencyconversionform',$dataset); 
		$this->load->view('Base/basedeleteform');
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$this->load->view('Base/overlaymobile');
		} else {
			$this->load->view('Base/overlay');
		}
?>	
</body>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js Files -->	
	<script src="<?php echo base_url();?>js/Currencyconversion/currencyconversion.js" type="text/javascript"></script> 
</html>