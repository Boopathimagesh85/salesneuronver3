<!DOCTYPE HTML>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
	<style type="text/css">
.paging-box {
    left: -40% !important;
}
</style>
</head>
<body class="hidedisplay">
<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<div class="gridviewdivforsh" style="" id="">
				<div class="large-12 columns paddingzero viewheader">
					<?php
							$dataset['gridtitle'] = $gridtitle['title'];
							$dataset['titleicon'] = $gridtitle['titleicon'];
							$dataset['formtype'] = 'logaudmodform';
							$dataset['gridid'] = 'moduleeditorgrid';
							$dataset['gridwidth'] = 'moduleeditorgridwidth';
							$dataset['gridfooter'] = 'moduleeditorgridfooter';
							$this->load->view('Base/mainviewheader',$dataset);
							$dataset['moduleid'] = '244';
							$this->load->view('Base/singlemainviewformwithgrid',$dataset);
						?>
				</div>
			</div>			
		</div>
	</div>
</div>
<?php
	$device = $this->Basefunctions->deviceinfo();
	$this->load->view('moduleeditorbasedeleteform');
	if($device=='phone') {
		$this->load->view('Base/overlaymobile');
	} else {
		$this->load->view('Base/overlay');
	}
	$this->load->view('Base/modulelist');
?>
</body>
	<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js File -->
	<script src="<?php echo base_url();?>js/Moduleeditor/moduleeditor.js" type="text/javascript"></script>	
</html>