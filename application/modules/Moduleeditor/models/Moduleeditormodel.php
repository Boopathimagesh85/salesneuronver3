<?php
Class Moduleeditormodel extends CI_Model {
	public function __construct() {
		parent::__construct(); 
    }
	//default value set array for update
	public function updatedefaultvalueget() {
		$defdata['lastupdatedate'] = date($this->Basefunctions->datef);
		$defdata['lastupdateuserid'] = $this->Basefunctions->userid;
		return $defdata;
	}
	//print header main grid view
	public function modulelistgriddatafetchmodel($tablename,$sortcol,$sortord,$pagenum,$rowscount,$filter=array()) {
		$userrole = $this->Basefunctions->userroleid;
		$industryid = $this->Basefunctions->industryid;
		$filtervalarray =array();
		if(!empty($filter)) {
			$fconditonid = explode('|',$filter[0]);
			$filtercondition = explode('|',$filter[1]);
			$filtervalue = explode('|',$filter[2]);
		} else {
			$fconditonid = array();
			$filtercondition = array();
			$filtervalue = array();
		}
		$filtervalarray = $this->Basefunctions->filtergenerateconditioninfo($fconditonid,$filtercondition,$filtervalue);
		if(count($filtervalarray) > 0) {
			$filterwherecondtion = $this->Basefunctions->filtergeneratewhereclause($filtervalarray['filterarray'],'');
			if($filterwherecondtion == '') {
				$filterwherecondtion = " AND 1=1";
			} else {
				$filterwherecondtion = " AND ".$filterwherecondtion;
			}
		} else {
			$filterwherecondtion = " AND 1=1";
		}
		$dataset = 'module.moduleid,module.modulename,module.menutype,parentmodule.modulename as menucategoryname,module.menuname,status.statusname,userrole.userrolename,status.status';
		$join=' LEFT OUTER JOIN moduleinfo ON moduleinfo.moduleid=module.moduleid';
		$join.=' LEFT OUTER JOIN userrole ON userrole.userroleid=moduleinfo.userroleid';
		$join.=' LEFT OUTER JOIN module as parentmodule ON parentmodule.moduleid=module.parentmoduleid ';
		$join.=' LEFT OUTER JOIN status ON status.status=moduleinfo.status';
		$status = $tablename.'.status IN (1,2) AND moduleinfo.status IN (1,2)';
		$wherecond = 'moduleinfo.userroleid = '.$userrole.' AND module.menuviewtype=1 AND FIND_IN_SET('.$industryid.',module.industryid) >0 AND module.modulecategorytypeid !=2 ';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		/* query */
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$wherecond.' '.$filterwherecondtion.' AND '.$status.' ORDER BY module.sortorder ASC,'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($data,$page,'');
		return $finalresult;
	}
	//module list show
	public function modulelistgriddatafetchmodellll($sidx,$sord,$start,$limit,$wh,$datainfo) {
		$userrole = $this->Basefunctions->userroleid;
		$finalresult = array();
		$dataset = 'module.moduleid,module.modulename,module.menutype,menucategory.menucategoryname,module.menuname,status.statusname,userrole.userrolename,status.status';
		$tablename = 'module';
		if($tablename != "") {
			$colinfo = explode(',',$datainfo);
			$join=' LEFT OUTER JOIN moduleinfo ON moduleinfo.moduleid=module.moduleid';
			$join.=' LEFT OUTER JOIN userrole ON userrole.userroleid=moduleinfo.userroleid';
			$join.=' LEFT OUTER JOIN status ON status.status=moduleinfo.status';
			$status = $tablename.'.status IN (1,2) AND moduleinfo.status IN (1,2)';
			$w = '1=1';
			if($wh == "1") { $wh = $w; }
			$li= 'LIMIT '.$start.','.$limit;
			$actsts = $this->Basefunctions->activestatus;
			$wherecond = 'moduleinfo.userroleid = '.$userrole;
			$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$wh.' AND '.$wherecond.' AND '.$status.' GROUP BY module.moduleid ORDER BY'.' '.$sidx.' '.$sord.',module.sortorder asc '.$li);
			$finalresult=array($colinfo,$data);
		}
		return $finalresult;
	}
	//delete module list
	public function deletemodulelistdatamodel() {
		$userroleid = $this->Basefunctions->userroleid;
		$primaryid = $_GET['primaryid'];
		$primayname = 'moduleid';
		$tablname = 'moduleinfo';
		$tablenames = explode(',',$tablname);
		$deldataarr = array(
			'status'=>2
		);
		$defdataarr = $this->updatedefaultvalueget();
		$newdata = array_merge($deldataarr,$defdataarr);
		foreach($tablenames as $table) {
			if($table == 'moduleinfo') {
				$this->db->where('userroleid',$userroleid);
			}
			$this->db->where($primayname,$primaryid);
			$this->db->update($table,$newdata);
		}
		echo "TRUE";
	}
	//enable module list
	public function enablemodulelistdatamodel() {
		$userroleid = $this->Basefunctions->userroleid;
		$primaryid = $_GET['primaryid'];
		$primayname = 'moduleid';
		$tablname = 'moduleinfo';
		$tablenames = explode(',',$tablname);
		$deldataarr = array(
			'status'=>1
		);
		$defdataarr = $this->updatedefaultvalueget();
		$newdata = array_merge($deldataarr,$defdataarr);
		foreach($tablenames as $table) {
			if($table == 'moduleinfo') {
				$this->db->where('userroleid',$userroleid);
			}
			$this->db->where($primayname,$primaryid);
			$this->db->update($table,$newdata);
		}
		echo "TRUE";
	}
	//module edit permission check
	public function moduleeditorcheckmodel() {
		$mid = ( ( isset($_POST['mdata']))? $_POST['mdata'] : 1);
		$cid = ( ( isset($_POST['cdata']))? $_POST['cdata'] : 1);
		$status = 'False';
		if($mid!=1) {
			$dataset = $this->db->query("SELECT moduleid FROM module WHERE moduleprivilegeid LIKE '%".$mid."%' AND moduleid=".$cid." AND status=1");
			if($dataset->num_rows() > 0 ) {
					$status = 'True';
			}
		}
		echo $status;
	}
}
?>