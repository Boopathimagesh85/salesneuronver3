<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Moduleeditor extends CI_Controller {
	function __construct() {
    	parent::__construct();	
		$this->load->model('Base/Basefunctions');
		$this->load->model('Moduleeditor/Moduleeditormodel');
		$this->load->helper('formbuild');
    }
	public function index() {
		$moduleid = array(244);
		sessionchecker($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);	
		$this->load->view('Moduleeditor/moduleeditorview',$data);
	}
	//menu category data view
	public function modulelistgriddatafetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'module.moduleid') : 'module.moduleid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('menucategoryname','modulename','menutype','menuname','userrolename','statusname','status'),'colmodelindex'=>array('parentmodule.modulename','module.modulename','module.menutype','module.menuname','userrole.userrolename','status.statusname','status.status'),'coltablename'=>array('parentmodule','module','module','module','userrole','status','status'),'uitype'=>array('2','2','2','2','2','2','2'),'colname'=>array('Category','Module Name','Menu Type','Menu Name','User Role','Status','statusid'),'colsize'=>array('200','200','200','200','200','100','10'));
		$filterid = isset($_GET['filter']) ? $_GET['filter'] : '';
		$conditionname = isset($_GET['conditionname']) ? $_GET['conditionname'] : '';
		$filtervalue = isset($_GET['filtervalue']) ? $_GET['filtervalue'] : '';
		$filter = array('0'=>$filterid,'1'=>$conditionname,'2'=>$filtervalue);
		$result=$this->Moduleeditormodel->modulelistgriddatafetchmodel($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$filter);
		$device = $this->Basefunctions->deviceinfo();
		//if($device=='phone') {
			//$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Module Editor',$width,$height);
		//} else {
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Module Editor',$width,$height);
		//}
		echo json_encode($datas);
	}
	//delete module list
    public function deletemodulelistdata() {
        $this->Moduleeditormodel->deletemodulelistdatamodel();
    }
	//enable module list
    public function enablemodulelistdata() {
        $this->Moduleeditormodel->enablemodulelistdatamodel();
    }
	//module edit permission check
	public function moduleeditorcheck() {
		$this->Moduleeditormodel->moduleeditorcheckmodel();
	}
}