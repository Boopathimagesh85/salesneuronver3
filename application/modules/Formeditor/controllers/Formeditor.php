<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Formeditor extends CI_Controller {
	function __construct() {
    	parent::__construct();	
		$this->load->model('Base/Basefunctions');
		$this->load->model('Formeditor/Formeditormodel');
		$this->load->helper('directory');
		$this->load->helper('file');
    }
	public function index() {
		$moduleid = array(259);
		sessionchecker($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['modulename'] = $this->Basefunctions->simpledropdownwithmulticond('moduleid','menuname,modulename,modulemastertable,modulelink,moduleicon','module','menutype,moduleprivilegeid','Internal,259');
		$this->load->view('Formeditor/formeditorview',$data);
	}
	//form fields elements generation
	public function formfieldselements() {
		$this->Formeditormodel->formfieldselementsgen();
	}
	//condition drop down value fetch
	public function ddcondfieldnamefetch() {
		$this->Formeditormodel->ddcondfieldnamefetchmodel();
	}
	//pick list name check
	public function picklistnamecheck() {
		$this->Formeditormodel->picklistnamecheckmodel();
	}
	//label name check
	public function filednamecheck() {
		$this->Formeditormodel->filednamecheckmodel();
	}
	//modify existing module
	public function formdataedit() {
		$this->Formeditormodel->formdataeditmodel();
	}
	//fetching module field length
	public function fetchfieldlength() {
		$this->Formeditormodel->fetchfieldlengthmodel();
	}
	//module field name
	public function fetchmodulefieldname() {
		$this->Formeditormodel->fetchmodulefieldnamemodel();
	}
	//module based tollbar action details fetch
	public function modcustactionassign() {
		$this->Formeditormodel->modcustactionassignmodel();
	}
}