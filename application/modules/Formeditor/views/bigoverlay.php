<!--BigOverlay Recycle-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="recycleoverlay">
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-2 columns"> &nbsp;</div>
		<div class="large-8  columns">
			<div class="row" style="background:#f5f5f5">
			    <div class="large-12 columns">
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 medium-12 small-12 alertsheadercaptionstyle columns" style="background:#546e7a">
							<div class="small-6 columns">
								<span class="" style="color:#ffffff; padding-right:0.5rem"> Recycle</span>
							</div>
							<div class="small-6 columns">
								<span class="fa fa-times" style="color:#ffffff; padding-left:24.5rem;"> </span>
							</div>
						</div>
					</div>
					<div class="large-12 large-centered columns" style=" height: 25rem; overflow-y: auto;">
						<div class="row" style="background:#f5f5f5">&nbsp;</div>
						<div class="large-5 columns end">
							<div style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff" class="large-12 columns">
							<div class="large-12 columns headerformcaptionstyle" style="border-color: rgb(37, 116, 169);">Inactive Fields</div>
							</div>
							<div class="large-12 medium-12 small-12 columns">             
								<label>Account</label>
								<input type="text" id="" name="" value="" />             
							</div>
							<div class="large-12 medium-12 small-12 columns">             
								<label>Lead</label>
								<input type="text" id="" name="" value="" />             
							</div>
							<div class="large-12 medium-12 small-12 columns">             
								<label>Data</label>
								<input type="text" id="" name="" value="" />             
							</div>
							<div class="large-12 medium-12 small-12 columns">             
								<label>Form</label>
								<input type="text" id="" name="" value="" />             
							</div>
							<div class="large-12 medium-12 small-12 columns">             
								<label>Account</label>
								<input type="text" id="" name="" value="" />             
							</div>
							<div class="large-12 columns">&nbsp;</div>
							<div class="large-12 columns">&nbsp;</div>
						</div>
						<div class="large-2 columns">&nbsp;
						</div>
						<div class="large-5 columns end">
							<div style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff" class="large-12 columns">
							<div class="large-12 columns headerformcaptionstyle" style="border-color: rgb(37, 116, 169);">Removed Fields</div>
							</div>
							<div class="large-12 medium-12 small-12 columns">             
								<label>Campaign</label>
								<input type="text" id="" name="" value="" />             
							</div>
							<div class="large-12 medium-12 small-12 columns">             
								<label>Status</label>
								<input type="text" id="" name="" value="" />             
							</div>
							<div class="large-12 medium-12 small-12 columns">             
								<label>Quality</label>
								<input type="text" id="" name="" value="" />             
							</div>
							<div class="large-12 medium-12 small-12 columns">             
								<label>Document</label>
								<input type="text" id="" name="" value="" />             
							</div>
							<div class="large-12 medium-12 small-12 columns">             
								<label>Account</label>
								<input type="text" id="" name="" value="" />             
							</div>
							<div class="large-12 columns">&nbsp;</div>
							<div class="large-12 columns">&nbsp;</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="Large-2 columns">&nbsp;</div>
	</div>
 </div>
 <!-- move section form one tab group to another -->
 <div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="movesectotabgrpoverlay">
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-4 large-centered columns sectiondatamoveform">
			<span id="sectiondatamovevalidate" class="validationEngineContainer">
				<div class="alert-panel">
					<div class="alertmessagearea">
						<div class="alert-title">Move Section</div>
						<div class="alert-message">
							<span class="firsttab" tabindex="1000"></span>
							<div id="tabgroupdivhidid" class="static-field overlayfield">
								<label>Tab Groups List<span class="mandatoryfildclass">*</span></label>
								<select id="tabgrouplist" class="chzn-select validate[required] ffieldd" data-placeholder="Select" data-prompt-position="topLeft:14,36" tabindex="1001" name="tabgrouplist">
								</select>
							</div>
							<div id="tabsectiondivhidid" class="static-field overlayfield">
								<label>Tab Sections List</label>
								<select id="tabsectionlist" class="chzn-select" data-placeholder="Select" data-prompt-position="topLeft:14,36" tabindex="1002" name="tabsectionlist">
								</select>
							</div>
							<div id="tabsectionposdivhidid" class="static-field overlayfield">
								<label>Position</label>
								<select id="movepositionlist" class="chzn-select" data-placeholder="Select" data-prompt-position="topLeft:14,36" tabindex="1003" name="movepositionlist">
									<option value=""></option>
									<option value="prev">Previous</option>
									<option value="after">After</option>
								</select>
							</div>
						</div>
					</div>
					<div class="alertbuttonarea">
						<input type="button" class="alertbtn" value="Move" tabindex="1004" name="movesectionoverlaybtn" id="movesectionoverlaybtn">
						<input type="button" id="movesectotabgrpclose" name="" value="Cancel" tabindex="1005" class="alertbtn flloop  alertsoverlaybtn" >
						<span class="lasttab" tabindex="1006"></span>
					</div>
				</div>
			</span>
			<!-- hidden fields -->
			<input name="secdataliid" type="hidden" id="secdataliid" value="" />
		</div>
	</div>
 </div>
 <!-- move section elements form one tab group section to another section-->
 <div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="movefieldtotabgrpsecoverlay">
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-4 large-centered columns fielddatamoveform">
			<span id="elementsdatamovevalidate" class="validationEngineContainer">
				<div class="alert-panel">
					<div class="alertmessagearea">
						<div class="alert-title">Move Fields</div>
						<div class="alert-message">
							<span class="firsttab" tabindex="1000"></span>
							<div id="fieldtabgroupdivhidid" class="static-field overlayfield">
								<label>Tab Groups List<span class="mandatoryfildclass">*</span></label>
								<select id="fieldtabgrouplist" class="chzn-select validate[required] ffieldd" data-placeholder="Select" data-prompt-position="topLeft:14,36" tabindex="1001" name="fieldtabgrouplist">
								</select>
							</div>
							<div id="fieldtabsectiondivhidid" class="static-field overlayfield">
								<label>Tab Sections List<span class="mandatoryfildclass">*</span></label>
								<select id="fieldtabsectionlist" class="chzn-select validate[required]" data-placeholder="Select" data-prompt-position="topLeft:14,36" tabindex="1002" name="fieldtabsectionlist">
								</select>
							</div>
							<div id="tabsectionfielddivhidid" class="static-field overlayfield">
								<label>Section Fields List</label>
								<select id="tabsectionfieldlist" class="chzn-select" data-placeholder="Select" data-prompt-position="topLeft:14,36" tabindex="1003" name="tabsectionfieldlist">
								</select>
							</div>
							<div id="fieldtabsectionposdivhidid" class="static-field overlayfield">
								<label>Position</label>
								<select id="fieldmovepositionlist" class="chzn-select" data-placeholder="Select" data-prompt-position="topLeft:14,36" tabindex="1004" name="fieldmovepositionlist">
									<option value=""></option>
									<option value="prev">Previous</option>
									<option value="after">After</option>
								</select>
							</div>
						</div>
					</div>
					<div class="alertbuttonarea">
						<input type="button" class="alertbtn" value="Move" tabindex="1005" name="fieldsectionoverlaybtn" id="fieldsectionoverlaybtn">
						<input type="button" id="movefildtotabgrpsecclose" name="" value="Cancel" tabindex="1006" class="alertbtn flloop  alertsoverlaybtn" >
						<span class="lasttab" tabindex="1007"></span>
					</div>
				</div>
			</span>
			<!-- hidden fields -->
			<input name="fielddataliid" type="hidden" id="fielddataliid" value="" />
		</div>
	</div>
 </div>