<!DOCTYPE HTML>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php 
		$this->load->view('Base/headerfiles');
		echo '<link href="'.base_url().'css/froala/froala_all_css_infoinside.css" rel="stylesheet" type="text/css">';
		$this->load->view('bigoverlay');
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$this->load->view('Base/overlaymobile');
		} else {
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
		//user information
		$CI =& get_instance();
		$name='';
		$email='';
		$img="";
		$imgfromid = "";
		$path = "";
		$empid = "";
		$dataset=$CI->db->select('employeeid,employeename,lastname,emailid,employeeimage,employeeimage_fromid,employeeimage_path,salutation.salutationname')->from('employee')->join('salutation','salutation.salutationid=employee.salutationid')->where('employee.employeeid',$CI->Basefunctions->logemployeeid)->get();
		foreach($dataset->result() as $row) {
			$name=$row->salutationname.$row->employeename.' '.$row->lastname;
			$email=$row->emailid;
			$img=$row->employeeimage;
			$imgfromid = $row->employeeimage_fromid;
			$path = $row->employeeimage_path;
			$empid = $row->employeeid;
		}
	?>
	<style>
		[draggable] {
			  -moz-user-select: none;
			  -khtml-user-select: none;
			*  -webkit-user-select: none;
			  -o-user-select: none;
			  user-select: none;
		}
		[draggable] * {
			  -moz-user-drag: none;
			  -khtml-user-drag: none;
			 * -webkit-user-drag: none;
			  -o-user-drag: none;
			  user-drag: none;
		}
		.sortable{list-style-type: none; margin-left:0;}
		#spanforaddsection{list-style-type: none; margin-left:0;}
		#leftsidepanel .select2-container .select2-choice{font-size:0.8rem !important}
		.hidecontainer{list-style-type: none; margin:0;}
		#s2id_modulecreactions ul li div{color:#ffffff !important}
		#droptarget1 select{margin:0 !important; height:1.7rem !important;padding:0 0 0 0.5rem}

.steppervertical{height:612px;overflow-x:hidden;overflow-y:auto;background-color: #eceff1; padding: 10px;}	
.steppercontent ul{margin:0 auto;}	
.steppercontent {padding:20px 0}	
*,
*:before,
*:after {
    box-sizing: border-box;
}
.step { position: relative; min-height: 32px /* circle-size */ ; }
.step > div:first-child { position: static; height: 0; }
.step > div:last-child { margin-left: 32px ; *padding-left: 16px; }
.circle { background: #546e7a; width: 32px; height: 32px; line-height: 32px; border-radius: 16px; position: relative; color: white; text-align: center; }
.line { position: absolute; border-left: 1px solid gainsboro; left: 16px; bottom: 10px; top: 42px; }
.step:last-child .line { display: none; }
.title { line-height: 32px; padding-left:12px;cursor:pointer; }
.fbuilderaccheader{color:#000;font-size:0.8rem;}
.elementiconsul div{color: #546e7a;font-size: 0.8rem;}
.fbuildericonsize {
    color: #949494 !important;
    font-size: 1.5rem !important;
}
.bgcolorfbelements{
	padding-bottom:1rem;
}
.elementiconsul li {
    width: 45%;
    margin:2% 1% 2% 1%;
    *box-shadow:0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
    box-shadow:0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 0px 0px 0 rgba(0, 0, 0, 0.12);
    padding-top:0.6rem;
    padding-bottom:0.5rem;
    cursor:move;
    background: #fff;
}
.elementiconsul > li:hover{
	background-color:#eceff1;
}
.columnselectionclass{box-shadow:0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 5px 0 rgba(0, 0, 0, 0.12)}
.sortable-ghost .columnselectionclass,.sortable-ghost .hovertoremove{opacity:0.6;background-color:#e0e0e0;}
.sortable-fff{background-color:red !important}
.fbuilderleftcation i {
    position: relative;
    top: 2px;
}
.dropareadiv .foriconcolor i{top:7px;}
@media only screen and (min-device-width: 768px) and (max-device-width:1024px){
.titlehide{
display:none;
	}
}

</style>
</head>
<body class="hidedisplay">
	<div class="row" style="max-width:100%">
		<div class="off-canvas-wrap" data-offcanvas>
			<div class="inner-wrap">
				<div class="large-12 medium-12 small-12 columns paddingzero studiocontainer" style="background-color:#617d8a;">
					<div class="large-9 medium-9 small-9 columns" id="droptarget1" style="border: 1px solid #546e7a;border-bottom:none;padding:0">
						<div class="large-12  medium-12 small-12 column fbuilderleftcation" style="height:51px; line-height:46px;">
							<span data-activates="slide-out" class="headermainmenuicon button-collapse" title="Menu"><i class="material-icons">menu</i></span>		
							<span class="gridcaptionpos" style="position: relative;top: -0.2rem;">
								<span><span class="titlehide">Module &nbsp;</span> Customization</span>
							</span>
						</div>
						<div class="dropareadiv" style="overflow-y:scroll;height:50px;position:relative;width:100%;background-color:#fff;">
							<div class="large-12 medium-12 small-12 columns hidedisplay formheaderspan divider" style=""></div>
							<form name="newmodulegenform" id="newmodulegenform" method="POST" class="large-12 medium-12 small-12 columns paddingzero">
								<span id="spanforaddtab"> <!-- fields coding add here -->
								</span>
								<!-- hidden fields -->
								<input type="hidden" name="userversion" id="userversion" value="1" />
								<input type="hidden" name="deletedfieldids" id="deletedfieldids" value="0" />
								<input type="hidden" name="deletedsectionids" id="deletedsectionids" value="0" />
								<input type="hidden" name="deletedtabids" id="deletedtabids" value="0" />
							</form>
						</div>
					</div>
					<div class="large-3 medium-3 small-3 columns"  id="leftsidepanel" style="border:1px solid #546e7a;border-bottom:none;height:41rem;padding:0">
						<div class="large-12 column fbuilderleftcation paddingzero" style="height:50px;">
							<div class="large-6 medium-6 column">
							<span id="preheader" class="hidedisplay" style="top:16px;position:relative;">Preview </span></div>
							<div class="large-6 medium-6 column foriconcolor mainaction" style='text-align: right;margin-right:3px; float: right; padding-top: 8px;'>
								<span id="recycle" class="fa fa-list-alt actioniconstyle" title="Recycle"></span>
								<span id="modbuildersavebtn" class="icon-box" title="Save"><i class="material-icons">save</i><span class="actiontitle"> Save</span></span>
								<span id="fullviewpreview" class="icon-box" title="Full View"><i class="material-icons">visibility</i><span class="actiontitle"> Preview</span></span>
								<span id="normalviewpreview" class="icon-box" title="Normal View"><i class="material-icons">visibility_off</i><span class="actiontitle"> Preview</span></span>
								<span id="customizationclose" class="icon-box" title="Close"><i class="material-icons">close</i><span class="actiontitle"> Close</span></span>
							</div>
						</div>
						<div class="large-12 columns steppervertical">
							<div class="step">
							    <div>
							        <div class="circle">1</div>
							        <div class="line"></div>
							    </div>
							    <div>
							        <div class="title steppercat steppercategory1" data-steppercat="1">Module <span class="titlehide"> Details </span><i class=" icon24build icon24build-chevron-down3 toggleicons" style="position:absolute;right:5px;top:8px;"></i></div>
							        <div class="steppercontent">
							        	<ul class="stepperul1">
								        	<li style="display:inline-block;">
								        		<span id="formmodulenamvalidate" class="validationEngineContainer" >
												<div class="static-field large-12 medium-8 small-12 columns">
													<label>Module Name <span id="" class="mandatoryfildclass">*</span></label>
													<select name="newmodulecrename" data-prompt-position="bottomLeft:14,42" class="validate[required] chzn-select"  data-placeholder="Select" id="newmodulecrename"  tabindex="101">
														<option></option>
														<?php
															foreach($modulename as $key):
																if( $key->modulemastertable!="" ) {
																	$name = (($key->menuname!="")? $key->menuname : $key->modulename);
														?>
																<option value="<?php echo $key->moduleid;?>" data-datamastertable="<?php echo $key->modulemastertable;?>" data-datamodname="<?php echo $name;?>" data-modlink="<?php echo $key->modulelink;?>" data-modicon="<?php echo $key->moduleicon;?>"><?php echo $name;?></option>
														<?php
																}
															endforeach;
														?>
													</select>
												</div>
												<div class="static-field large-12 medium-8 small-12 columns">
													<label>Action Menu</label>
													<select data-placeholder="Select Actions" id="moduleactionname" class="chzn-select validate[required] dropdownchange" multiple name="moduleactionname" data-prompt-position="bottomLeft:14,100" tabindex="102">
														<option></option>
													</select>
													<input name="modactionslist" type="hidden" id="modactionslist" value="" />
												</div>
												<div class="large-12 medium-8 small-12 columns">
													<input type="checkbox" class="actchk filled-in" id="changemodname" name="changemodname" value="No" tabindex="103"></input><label  style="margin:0.2rem 0 0 0.2rem;" for="changemodname" >Change Module Name</label>
												</div>
												<div id="newmodnamediv" class="static-field large-12 medium-12 small-12 columns">
													<input type="text" id="newmodname" name="newmodname" class="validate[required]" tabindex="104"> </input><label>New Module Name<span id="" class="mandatoryfildclass">*</span></label>
												</div>
												<div class="large-12 medium-12 small-12 columns ">
													<input id="nextstep" name=""  class="btn" tabindex="105" value="Next"  type="button" style="margin-top:1rem;">
												</div>
											</span>
								        	</li>
							        	</ul>
							        	
							        </div>
							    </div>
							</div>
							<div class="step">
							    <div>
							        <div class="circle">2</div>
							        <div class="line"></div>
							    </div>
							    <div>
							        <div class="title steppercat steppercategory2" data-steppercat="2">Templates <i class=" icon24build icon24build-chevron-down3 toggleicons" style="position:absolute;right:5px;top:8px;"></i></div>
							         <div class="steppercontent">
							        	<ul class="stepperul2 hidedisplay stepperulhideotherelements">
								        	<li style="display:inline-block;width:100%" class="">
								        		<div class="large-12 medium-12 small-12 column"  id="fbuildercat2" style="padding:0">
													<ul class="fbulderheadermenu">
														<li class="fbuilderaccli fbuilderaccclick fortrigger2" data-elementcat="2">
															<div class="large-6 medium-12 small-6 columns paddingzero">
																<label class="fbuilderaccheader">Tab Group</label>
															</div>
														</li>
														<div id="" class="bgcolorfbelements  elementlist2 ">
															<ul class="elementiconsul">
																<li draggable="true" id="withoutgridgen">
																	<span class="icon24build icon24build-view-module fbuildericonsize"></span>
																	<div>Tab Group</div>
																</li>
															</ul>
														</div>
														<li class="fbuilderaccli fbuilderaccclick fortrigger3" data-elementcat="3">
															<div class="large-6 medium-12 small-6 columns paddingzero">
																<label class="fbuilderaccheader">Template</label>
															</div>
														</li>
														<div id="" class="bgcolorfbelements  elementlist3 ">
															<ul class="elementiconsul">
																<li draggable="true" id="sectiongen">
																	<span class="icon24build icon24build-crop-portrait fbuildericonsize"> </span>
																	<div>Section</div>
																</li>
																<li draggable="true" id="sectionwitheditorgen">
																	<span class="icon24build icon24build-view-grid fbuildericonsize"> </span>
																	<div>Section With Editor</div>
																</li>
															</ul>
														</div>
														<li class="fbuilderaccli fbuilderaccclick fortrigger4" data-elementcat="4">
															<div class="large-6 medium-12 small-6 columns paddingzero">
																<label class="fbuilderaccheader">Form Elements</label>
															</div>
														</li>
														<div id="forproppanelshow" class="bgcolorfbelements  elementlist4 ">
															<ul class="elementiconsul">
																<li draggable="true" id="textboxgen">
																	<span class="icon24build icon24build-checkbox-blank-outline fbuildericonsize"> </span>
																	<div> Text Box </div>
																</li>
																<li draggable="true" id="textareagen">  
																	<span class="icon24build icon24build-message-text fbuildericonsize"> </span>
																	<div> Text Area </div>
																</li>
															</ul>
															<ul class="elementiconsul">
																<li draggable="true" id="integerboxgen"> 
																	<span class="icon24build icon24build-numeric fbuildericonsize"> </span>
																	<div> Integer </div>
																</li>
																<li draggable="true" id="decimalboxgen"> 
																	<span class="icon24build icon24build-decimal-decrease fbuildericonsize"> </span>
																	<div> Decimal </div>
																</li>
															</ul>
															<ul class="elementiconsul">
																<li draggable="true" id="percentgen">
																	<span class="icon24build icon24build-percent fbuildericonsize"> </span>
																	<div> Percent </div>
																</li>
																<li draggable="true" id="dropdowngen">
																	<span class="icon24build icon24build-view-list fbuildericonsize"> </span>
																	<div> Picklist </div>										
																</li>
															</ul>
															<ul class="elementiconsul">
																<li draggable="true" id="currencygen">
																	<span class="icon24build icon24build-currency-inr fbuildericonsize"> </span>
																	<div> Currency </div>
																</li>
																<li draggable="true" id="dategen">
																	<span class="icon24build icon24build-calendar-today fbuildericonsize"> </span>
																	<div> Date </div>	
																</li>
															</ul>
															<ul class="elementiconsul">
																<li draggable="true" id="timegen">
																	<span class="icon24build icon24build-clock fbuildericonsize"> </span>
																	<div> Time </div>	
																</li>
																<li draggable="true" id="emailgen">
																	<span class="icon24build icon24build-email-outline fbuildericonsize"> </span>
																	<div> E-Mail </div>										
																</li>
															</ul>
															<ul class="elementiconsul">
																<li draggable="true" id="phonegen">
																	<span class="icon24build icon24build-phone fbuildericonsize"> </span>
																	<div> Phone </div>										
																</li>
																<li draggable="true" id="urlgen">
																	<span class="icon24build icon24build-link fbuildericonsize"> </span>
																	<div> URL </div>
																</li>
															</ul>
															<ul class="elementiconsul">
																<li draggable="true" id="texteditorgen">
																	<span class="icon24build icon24build-file-document-box fbuildericonsize"> </span>
																	<div> Text Editor </div>
																</li>
																<li draggable="true" id="autonumbergen">
																	<span class="icon24build icon24build-credit-card fbuildericonsize"> </span>
																	<div> Auto Number </div>
																</li>
															</ul>
															<ul class="elementiconsul">
																<li draggable="true" id="checkboxgen">
																	<span class="icon24build icon24build-checkbox-marked-outline fbuildericonsize"> </span>
																	<div> Check Box </div>
																</li>
																<li draggable="true" id="passwordgen">
																	<span class="icon24build icon24build-lock-open fbuildericonsize"> </span>
																	<div> Password </div>
																</li>
															</ul>
															<ul class="elementiconsul">
																<li draggable="true" id="ddrelationgen">
																	<span class="icon24build icon24build-vector-intersection fbuildericonsize"> </span>
																	<div> Relation </div>
																</li>
																<li draggable="true" id="tagscomponentgen">
																	<span class="icon24build icon24build-tag-multiple fbuildericonsize"> </span>
																	<div> Tags </div>
																</li>
															</ul>
															<ul class="elementiconsul">
																<li draggable="true" id="attachmentgen">
																	<span class="icon24build icon24build-attachment fbuildericonsize"> </span>
																	<div> Attachment </div>
																</li>
																<li draggable="true" id="imgattachgen">
																	<span class="icon24build icon24build-image fbuildericonsize"> </span>
																	<div> Image Upload </div>
																</li>
															</ul>
														</div>
													</ul>
												</div>
								        	</li>
								       	</ul>
								    </div>
							    </div>
							</div>
							<div class="step">
							    <div>
							        <div class="circle">3</div>
							        <div class="line"></div>
							    </div>
							    <div>
							        <div class="title steppercat steppercategory3" data-steppercat="3">Properties <i class=" icon24build icon24build-chevron-down3 toggleicons" style="position:absolute;right:5px;top:8px;"></i></div>
							         <div class="steppercontent">
							        	<ul class="stepperul3 hidedisplay stepperulhideotherelements">
								        	<li style="display:inline-block;width:100%">
								        		<div class="large-12 medium-12 small-12 columns propertiesdataclear" id="fbuildercat3"  style="padding:0">
													<div class="large-12 columns propertytext">
														Please Select any field from right side 
													</div>
													<ul class="fbulderheadermenu propertyhidedisplay">
														<div id="propertypanel" class="bgcolorfbelements hidedisplay elementlist5 hideotherelements propertiesclear validationEngineContainer">
															<ul style="padding-right: 1.2rem;margin-bottom: 1rem;">
																<li>
																	<div class="input-field large-12 columns">
																		<input type="text" id="frmelemattrlablname" name="frmelemattrlablname" class="validate[required,funcCall[labelnamevalidate]]"> </input>
																		<label for="frmelemattrlablname">Label Name</label>
																	</div>
																</li>
																<li id="dataplvalattr" class="hidedisplay">
																	<div class="static-field large-12 columns">
																		<input type="text" id="frmelemdatavalue" name="frmelemdatavalue"> </input>
																		<label>Value</label>
																	</div>
																</li>
																<li id="attrmodopt" class="hidedisplay">
																	<div class="static-field large-12 columns">
																	<label>Module</label>
																	<select name="attrmodulename" data-placeholder="Select" class="chzn-select" id="attrmodulename">
																		<option></option>
																		<?php
																			foreach($modulename as $key):
																			if( $key->modulemastertable!="" ) {
																				$name = (($key->menuname=="")?$key->modulename:$key->menuname);
																		?>
																				<option value="<?php echo $name;?>" data-datamoduleid="<?php echo $key->moduleid;?>"><?php echo $name;?></option>
																		<?php
																			}
																			endforeach;
																		?>
																	</select>
																	</div>
																</li>
																<li id="attrelefldname" class="hidedisplay">
																	<div class="static-field large-12 columns">
																	<label>Field Name</label>
																	<select name="elemntsfieldname" data-placeholder="Select" class="chzn-select" id="elemntsfieldname">
																		<option></option>
																	</select>
																	</div>
																</li>
																<li id="attrcheckshowhide" class="hidedisplay">
																	<div class="static-field large-12 columns">
																	<input type="checkbox" class="filled-in" id="elecheckshowhide" datafieldid="" name="elecheckshowhide" value="No"  ></input><label style="margin:0.2rem 0 0 0.2rem;" for="elecheckshowhide">Related Field Show/Hide</label>
																	</div>
																</li>										
																<li id="attrcheckshfldname" class="hidedisplay">
																	<div class="static-field large-12 columns">
																	<label>Field Name</label>
																	<select name="elecheckshfldname" data-placeholder="Select"  class="chzn-select" id="elecheckshfldname">
																		<option></option>
																	</select>
																	</div>
																</li>
																<li id="attrfldname" class="hidedisplay">
																	<div class="static-field large-12 columns">
																	<label>Condition Field Name</label>
																	<select name="frmelefiledname" data-placeholder="Select" class="chzn-select" id="frmelefiledname">
																		<option></option>
																	</select>
																	</div>
																</li>
																<li id="attrmodfldcond" class="hidedisplay">
																	<div class="static-field large-12 columns">
																	<label>Condition</label>
																	<select data-placeholder="Select"  class="chzn-select" name="frmelecondname" id="frmelecondname">
																		<option></option>
																		<option value="Equalto">Equalto</option>
																		<option value="NotEqual">NotEqual</option>
																		<option value="Startwith">Startwith</option>
																		<option value="Endwith">Endwith</option>
																		<option value="Middle">Middle</option>
																	</select>
																	</div>
																</li>
																<li id="attrmodfldval" class="hidedisplay">
																	<div class="input-field large-12 columns">
																	<input type="text" id="frmelmcondval" name="frmelmcondval"> </input>
																	<label for="frmelmcondval">Value</label>
																	</div>
																</li>
																<li id="defval" class="">
																	<div class="input-field large-12 columns">
																	<input type="text" id="frmelemattrdefname" name="frmelemattrdefname"> </input>
																	<label for="frmelemattrdefname">Default Value</label>
																	</div>
																</li>
																<li id="dddefval" class="hidedisplay">
																	<div class="static-field large-12 columns">
																	<label>Default Value</label>
																	<select name="dddefaultvalue" data-placeholder="Select" class="chzn-select" id="dddefaultvalue">
																	</select>
																	</div>
																</li>
																<li id="tagdefval" class="hidedisplay">
																	<div class="input-field large-12 columns">
																	<input type="text" id="tagdefaultvalue" name="tagdefaultvalue"> </input>
																	<label for="tagdefaultvalue">Default Value</label>
																	</div>
																</li>
																<li id="datedefval" class="hidedisplay">
																	<div class="input-field large-12 columns">
																	<input type="text" id="dateaddfielddefval" value="" name="dateaddfielddefval" /> </input>
																	<label for="dateaddfielddefval">Default Date</label>
																	</div>
																	<?php
																		$appdateformat = $this->Basefunctions->appdateformatfetch();
																		echo "<script>
																			$(document).ready(function(){
																				$('#dateaddfielddefval').datetimepicker({
																					dateFormat: '".$appdateformat."',
																					showTimepicker :false,
																					minDate: null,
																					changeMonth: null,
																					changeYear: null,
																					maxDate: null,
																					onSelect: function () {
																						var checkdate = $(this).val();
																						if(checkdate != '') {
																							$(this).next('span').remove('.btmerrmsg'); 
																							$(this).removeClass('error');
																						}
																					}
																				});
																			});
																		</script>";
																	?>
																</li>
																<li id="timedefval" class="hidedisplay">
																	<div class="input-field large-12 columns">
																	<input type="text" id="timeaddfielddefval" name="timeaddfielddefval" class="fortimepicicon">
																	<label for="timeaddfielddefval">Default Time</label>
																	</div>
																</li>
																<li id="attrdatalen" class="hidedisplay">
																	<div class="input-field large-12 columns">
																	<input type="text" id="frmelemattrlength" datafieldid="" name="frmelemattrlength"></input>
																	<label  for="frmelemattrlength">Length</label>
																	</div>
																</li>
																<li id="attrdatatextlen" class="hidedisplay">
																	<div class="input-field large-12 columns">
																	<input type="text" id="frmelemattrtextlength" datafieldid="" name="frmelemattrtextlength"></input>
																	<label  for="frmelemattrtextlength">Length</label>
																	</div>
																</li>
																<li id="attrdatadecilen" class="hidedisplay">
																	<div class="input-field large-12 columns">
																	<label>Decimal</label>
																	<input type="text" id="frmelemdecilength" name="frmelemdecilength"></input>
																	</div>
																</li>
																<li id="attrrowlen" class="hidedisplay">
																	<div class="input-field large-12 columns">
																	<input type="text" id="frmelemattrrows" name="frmelemattrrows"> </input>
																	<label for="frmelemattrrows">Rows</label>
																	</div>
																</li>
																<li id="attrdataprefix" class="hidedisplay">
																	<div class="input-field large-12 columns">
																	<input type="text" id="frmelemprefix" name="frmelemprefix"></input>
																	<label for="frmelemprefix">Prefix</label>
																	</div>
																</li>
																<li id="attrdatastartnum" class="hidedisplay">
																	<div class="input-field large-12 columns">
																	<input type="text" id="frmelemstartnum" name="frmelemstartnum"></input>
																	<label for="frmelemstartnum">Start Number</label>
																	</div>
																</li>
																<li id="attrdatasuffix" class="hidedisplay">
																	<div class="input-field large-12 columns">
																	<input type="text" id="frmelemsuffix" name="frmelemsuffix"> </input>
																	<label for="frmelemsuffix">Suffix</label>
																	</div>
																</li>
																<li id="attrdatarandomopt" class="hidedisplay">
																	<div class="static-field large-12 columns">
																	<label>Number Type</label>
																	<select name="attrautonumbertype" data-placeholder="Select" class="chzn-select" id="attrautonumbertype">
																		<option></option>
																		<option value="No">Serial</option>
																		<option value="Yes">Random</option>
																	</select>
																	</div>
																</li>
																<li id="attrdatafilesizeopt" class="hidedisplay">
																	<div class="input-field large-12 columns">
																	<div><input type="text" name="attrdatafilesize" id="attrdatafilesize" value="5" style="display: inline-block; width:90%"></input><label for="attrdatafilesize">File Size</label><span style="padding-left:2%;padding-top:1%;">Mb</span></div>
																	</div>
																</li>
																<li id="attrdataimgfiletypeopt" class="hidedisplay">
																	<div class="input-field large-12 columns">
																	<input type="text" name="attrdataimgfiletype" id="attrdataimgfiletype"></input>
																	<label for="attrdataimgfiletype">File Type</label>
																	</div>
																</li>
																<li>
																	<div class="input-field large-12 columns">
																	<input type="text" id="frmelemattrdesc" name="frmelemattrdesc"> </input>
																	<label for="frmelemattrdesc">Description</label>
																	</div>
																</li>
																<li>
																	<span class="columns">
																		<input type="checkbox" class="actchk filled-in" id="fildmandatory" name="fildmandatory" value="No"></input><label style="margin:0.2rem 0 0 0.2rem;" for="fildmandatory">Mandatory</label>
																	</span>
																	<span class="columns">
																		<input type="checkbox" class="filled-in" id="fieldactive" name="fieldactive" value="Yes" checked="checked" ></input><label style="margin:0.2rem 0 0 0.2rem;" for="fieldactive">Active</label>
																	</span>
																</li>
																<li>
																	<span class="columns" id="featuredatespan" class="hidedisplay">
																		<input type="checkbox" class="filled-in" id="featuredate" name="featuredate" value="Yes" checked="checked"></input><label style="margin:0.2rem 0 0 0.2rem;" for="featuredate">Future Date</label>
																	</span>
																	<span id="pastdatespan" class="hidedisplay columns">
																		<input type="checkbox" class="filled-in" id="pastdate" name="pastdate" value="No"></input><label style="margin:0.2rem 0 0 0.2rem;" for="pastdate">Past Date</label>
																	</span>
																	<span id="changeyearspan" class="hidedisplay columns">
																		<input type="checkbox" class="filled-in" id="changeyear" name="changeyear" value="No"></input><label style="margin:0.2rem 0 0 0.2rem;" for="changeyear">With Year</label>
																	</span>
																	<span id="currentdatespan" class="hidedisplay columns">
																		<input type="checkbox" class="filled-in" id="currentdate" name="currentdate" value="No"></input><label style="margin:0.2rem 0 0 0.2rem;" for="currentdate">Current Date</label>
																	</span>
																</li>
																<li>
																	<span id="readonlyattr" class="columns">
																		<input type="checkbox" class="actchk filled-in" id="fieldreadonly" name="fieldreadonly" value="No"></input><label style="margin:0.2rem 0 0 0.2rem;" for="fieldreadonly">Read Only</label>
																	</span>
																	<span id="columntypeattr" class="columns">
																		<input type="checkbox" class="actchk filled-in" id="fieldcolumntype" name="fieldcolumntype" value="No"></input><label style="margin:0.2rem 0 0 0.2rem;" for="fieldcolumntype">Two Column</label>
																	</span>
																</li>
																<li>
																	<span  id="mulchkopt" class="hidedisplay columns">
																		<input type="checkbox" class="actchk filled-in" id="addfieldmultiple" name="addfieldmultiple" value="No"></input><label style="margin:0.2rem 0 0 0.2rem;" for="addfieldmultiple">Multiple</label>
																	</span>
																	<span   id="attrsortopt" class="hidedisplay columns">
																		<input type="checkbox" class="actchk filled-in" id="addalphasortorder" name="addalphasortorder" value="No"></input><label style="margin:0.2rem 0 0 0.2rem;" for="addalphasortorder">Alphabetical Sort</label>
																	</span>
																</li>
																<li>
																	<span id="attrchkboxsetdef" class="hidedisplay columns">
																		<input type="checkbox" class="actchk filled-in" id="addfieldsetdef" name="addfieldsetdef" value="No"></input><label style="margin:0.2rem 0 0 0.2rem;" for="addfieldsetdef">Set Default</label>
																	</span>
																	<span id="attrchkboxunique" class="hidedisplay columns">
																		<input type="checkbox" class="actchk filled-in" id="addfieldunique" name="addfieldunique" value="No"></input><label style="margin:0.2rem 0 0 0.2rem;" for="addfieldunique">Unique</label>
																	</span>
																</li>
															</ul>
															<input type="hidden" name="oldlabelname" id="oldlabelname" value="" />
															<?php
																$appdateformat = $this->Basefunctions->appdateformatfetch();
															?>
															<input type="hidden" name="dataformatter" id="dataformatter" value="<?php echo $appdateformat;?>" />
														</div>
													</ul>
												</div>
								        	</li>
								        </ul>
								     </div>
							    </div>
							</div>
						</div>
					</div>
					<!-- hidden fields -->
					<input name="elemidentiryid" type="hidden" id="elemidentiryid" value="" />
				</div>
				<?php
					$this->load->view('Base/basedeleteformforcombainedmodules');
				?>
			</div>
		</div>
	</div>
	<!--Reload Alert Overlay-->
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay alertsoverlay overlayalerts" id="reloadrestriction">	
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>		
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="large-3 medium-6 small-10 large-centered medium-centered small-centered columns">
				<div class="row">&nbsp;</div>
				<div class="row">
					<div class="large-12 columns alertsheadercaptionstyle" style="text-align:left">
						<div class="small-12 columns"><span style="color:#ffffff;padding-right:0.5rem"><i class="material-icons">check_box</i> </span> Confirmation</div>
					</div>
					<div class="large-12 columns" style="background:#ffffff">&nbsp;</div>
					<div class="large-12 large-centered columns" style="background:#ffffff;text-align:center">
						<span class="" style="	color: #5e6d82;font-family: segoe ui;font-size: 0.9rem;">Do you want Reload a page?</span>
						<div class="large-12 column">&nbsp;</div>
					</div>
				</div>
				<div class="row" style="background:#ffffff">
					<div class="medium-2 large-2 columns"> &nbsp;</div>
					<div class="small-6  medium-4 large-4 columns">
						<input type="button" id="reloadpageyes" name="" value="Yes" class="btn formbuttonsalert commodyescls" >	
					</div>
					<div class="small-6 medium-4 large-4 columns">
						<input type="button" id="basedeleteno" name="" value="No" class="btn formbuttonsalert alertsoverlaybtn" >
					</div>
					<div class="medium-2 large-2 columns"> &nbsp;</div>
				</div>
				<div class="row" style="background:#ffffff">&nbsp;</div>
				<div class="row" style="background:#ffffff">&nbsp;</div>
			</div>
		</div>
	</div>
</body>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js File -->
	<script src="<?php echo base_url();?>js/Formeditor/formeditor.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/sortable.js" type="text/javascript"></script>
	<!-- Time picker Input Field-->
	<script>
		$(document).ready(function(){
			$('#timeaddfielddefval').timepicker({
				'step':15,
				'timeFormat': 'H:i',
				'scrollDefaultNow': true,
			});
		});
	</script>
</html>