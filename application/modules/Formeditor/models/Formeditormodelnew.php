<?php
Class Formeditormodel extends CI_Model {
	public function __construct() {
		parent::__construct(); 
    }
	//default value set array for update
	public function updatedefaultvalueget() {
		$defdata['lastupdatedate'] = date($this->Basefunctions->datef);
		$defdata['lastupdateuserid'] = $this->Basefunctions->userid;
		return $defdata;
	}
	//default values
	public function defaultvalueget() {
		$defdata['createdate'] = date($this->Basefunctions->datef);
		$defdata['lastupdatedate'] = date($this->Basefunctions->datef);
		$defdata['createuserid'] = $this->Basefunctions->userid;
		$defdata['lastupdateuserid'] = $this->Basefunctions->userid;
		$defdata['status'] = 1;
		return $defdata;
	}
	//form elements field type
	public function gencolumntype($coltypeval) {
		$type = ( ($coltypeval == 'Yes')? '2' : '1' );
		return $type;
	}
	
	public function getmoduleid($modulename){
		$data = '';
		$this->db->select('moduleid');
		$this->db->from('module');		
		$this->db->where('modulemastertable',$modulename);
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$data = $row->moduleid;
			}
		}
		return $data;
	}
	public function fetchfieldlengthmodel(){
		$datalength = '';
		$data = $this->db->query('select maximumlength from modulefield where modulefieldid='.$_GET['modulefieldid'].' AND status = 1');	
		if($data->num_rows() > 0){
			foreach ($data->result() as $vals) {
				$datalength = $vals->maximumlength;
			}
		}
		echo $datalength;
	}
	public function fetchmodulefieldnamemodel(){
		$i=0;
		$data = $this->db->query('select fieldlabel,modulefieldid,columnname from modulefield where moduletabid='.$_GET['moduleid'].' AND modulefieldid != '.$_GET['datafieldid'].' AND mandatory = "No" AND status = 1');
		if($data->num_rows() > 0){
			foreach ($data->result() as $vals) {
				$fielddata[$i] = array('modulefieldid'=>$vals->modulefieldid,'fieldlabel'=>$vals->fieldlabel,'columnname'=>$vals->columnname);
				$i++;
			}
		}
		echo json_encode($fielddata);
	}
	//form fields elements generation
	public function formfieldselementsgen() {
		$moduleid = $_POST['moduleid'];
		$modtabgrpinfo = $this->moduletabgroupgeneration($moduleid);
		$tabgrp = "";
		$tabgrpids = array();
		$tabsecids = array();
		$tabsecfieldids = array();
		$tabgrp = '<div class="large-12 column tabgroupstyle"><ul id="appendtab" class="tabs" data-tab="">';
		$m=0;
		foreach($modtabgrpinfo as $values) {
			$delmode = $this->tabgrpdelmodecheck($values['tabpgrpid']);
			$grprestrict = (($delmode >= 1)? "Yes" : "No");
			$tabgrp .= '<li class="tab-title sidebaricons">
				<span class="tabclsbtn icon24build icon24build-close2" style="opacity: 0;" data-datatabgroupid="'.$values['tabpgrpid'].'" data-tabgrprestrict="'.$grprestrict.'"></span>';
				if($m==0) {
					$tabgrp .= '<span class="spantabgrpelemgen activefbuilder" contenteditable="true" data-spantabgrpelemgennum="'.$m.'" data-tabgrpfieldid="'.$values['tabpgrpid'].'"  id="tabgrpblabel'.$m.'">'.$values['tabgrpname'].'</span>';
					$tabgrp .= '<input id="spantabgrpinfo'.$m.'" type="hidden" value="'.$values['templatetypeid'].','.$m.','.$values['tabgrpname'].','.$values['tabpgrpid'].'" name="spantabgrpinfo[]">';
					$tabgrpids[] = $m;
					$m++;
				} else {
					$tabgrp .= '<span class="spantabgrpelemgen" contenteditable="true" data-spantabgrpelemgennum="'.$m.'" data-tabgrpfieldid="'.$values['tabpgrpid'].'" id="tabgrpblabel'.$m.'">'.$values['tabgrpname'].'</span>';
					$tabgrp .= '<input id="spantabgrpinfo'.$m.'" type="hidden" value="'.$values['templatetypeid'].','.$m.','.$values['tabgrpname'].','.$values['tabpgrpid'].'" name="spantabgrpinfo[]">';
					$tabgrpids[] = $m;
					$m++;
				}
			$tabgrp .= '</li>';
		}
		$tabgrp .='</ul></div>';
		$tabgrp .= '<div id="forreaddtabs" class="large-12 column">&nbsp;</div>';
		$tabgrpid = 0;
		$tabsecid = 0;
		$tabfieldid = 0;
		$formdatas="";
		$formtabdatas="";
		$tabgrpstart="";
		foreach($modtabgrpinfo as $values) {
			$moduleid = $values['moduleid'];
			$templatetypeid = $values['templatetypeid'];
			$tabgroupid = $values['tabpgrpid'];
			$tabgrpname = $values['tabgrpname'];
			//check section is delete able
			if($tabgrpid == 0) {
				$tabgrpstart ='<ul id="spanforaddsection'.$tabgrpid.'" class="toappendlastcontainer hidecontainer appendtabcurrentplace currentab" data-tabgrptype="'.$templatetypeid.'" data-spantabgrpelemnum="'.$tabgrpid.'">'; //tab group creation (1 st group)
				
			} else {
				$tabgrpstart ='<ul id="spanforaddsection'.$tabgrpid.'" class="hidecontainer toappendlastcontainer hidedisplay" data-tabgrptype="'.$templatetypeid.'" data-spantabgrpelemnum="'.$tabgrpid.'">'; //tab group creation (other group)
				
			}
			$tabsecgrpdatas="";
			if($templatetypeid == '1') { //template 1 - without grid
				$modtabsecfrmfkdsgrp = $this->moduletabgrpsecfieldsgeneration($moduleid,$tabgroupid);
				//grouping tab group value
				$tempgrplevel=0;
				$newgrpkey=0;
				$tabgroup=array();
				foreach ($modtabsecfrmfkdsgrp as $key => $val) {
					if($tempgrplevel==$val['modtabgrpid']) {
						$tabgroup[$tempgrplevel][$newgrpkey]=$val;
					} else {
						$tabgroup[$val['modtabgrpid']][$newgrpkey]=$val;
					}
					$newgrpkey++;       
				}
				$tabsectiongrp = array();
				if(count($tabgroup) > 0) {
					//group tab section within tab group
					$tempgrpseclevel=0;
					$newgrpseckey=0;
					foreach($tabgroup as $key => $value) {
					   foreach($value as $key1 => $val) {
							if($tempgrpseclevel==$val['tabsectionid']) {
								$tabsectiongrp[$key][$tempgrpseclevel][$newgrpseckey]=$val;
							} else {
								$tabsectiongrp[$key][$val['tabsectionid']][$newgrpseckey]=$val;
							}
							$newgrpseckey++; 
						}
					}
				}
				//form section and elements generation
				$formtabdatas="";
				foreach($tabsectiongrp as $grpkey => $grpvalarr) {
					$prevgrpsecid = " ";
					$tabsection="";
					foreach($grpvalarr as $seckey => $secvalarr) { //section creation
						$tabsection .= '<li class="forrestrictsec newsortsec" id="secliattrid'.$tabsecid.'">';
						foreach($secvalarr as $fieldkey => $fieldval) { //form field creation
							$curgrpsecid = $fieldval['tabsectionid'];
							if($prevgrpsecid != $curgrpsecid) {
								$prevgrpsecid=$fieldval['tabsectionid'];
								//check section is delete able
								$delmode = $this->sectiongrpdelmodecheck($fieldval['tabsectionid']);
								$restrict = (($delmode >= 1)? "Yes" : "No");
								
								$tabsection .= '<div class="large-4 column end forselectsection paddingbtm">';
								$tabsection .= '<div id="spanforaddelements'.$tabsecid.'" class="large-12 columns cleardataform ddddd" data-forsorting="'.$tabsecid.'" data-spanforaddelements="'.$tabsecid.'" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff;padding-bottom:1rem">';
								$tabsection .= '<div class="large-12 columns headerformcaptionstyle spansectionelemgen" contenteditable="true" data-spansectionelemgennum="'.$tabsecid.'" data-sectionfieldresct="'.$restrict.'" data-sectionfieldid="'.$fieldval['tabsectionid'].'" id="tabseclabel'.$tabsecid.'" data-sectabgrpid="'.$tabgrpid.'">'.$fieldval['tabsectionname'].'</div>';
								$tabsection .= '<span class="icon24build icon24build-cursor-move secmovebtn acticonshover" data-datasecliattrid="'.$tabsecid.'" style="display: none;" title="Move"></span>';
								$tabsection .= '<span class="icon24build icon24build-close secremovebtn acticonshover" style="display: none;" data-sectionfieldid="'.$fieldval['tabsectionid'].'" data-sectionfieldresct="'.$restrict.'" title="Remove"></span>';
								$tabsection .= '<input id="spantabsectinfo'.$tabsecid.'" type="hidden" value="'.$tabgrpid.','.$tabsecid.','.$fieldval['tabsectionname'].','.$fieldval['tabsectionid'].'" name="spantabsectinfo[]">';
								$tabsection .= '<ul id="sortable'.$tabsecid.'" class="sortable"  data-datasectionattrid="'.$tabsecid.'">';
								$tabsecids[] = $tabsecid;
								$tabsecid++;
							}
							//function call for fields generation
							if($fieldval['colname'] != "salutationid") {
								$secfielddata = $this->formfieldsgenerate($fieldval,($tabsecid-1),$tabfieldid,$moduleid);
								$tabsection .= $secfielddata;
								$tabsecfieldids[] = $tabfieldid;
								$tabfieldid++;
							}
						}
						$tabsection .= '</ul>';
						$tabsection .= '</div>';
						$tabsection .= '</div>';
						$tabsection .= '</li>';
					}
					$formtabdatas.= $tabsection;
				}
				$tabsecgrpdatas .= $formtabdatas;
			} else if($templatetypeid == '2') {//template 2 - with grid
				$modtabsecfrmfkdsgrp = $this->moduletabgrpsecfieldsgeneration($moduleid,$tabgroupid);
				//grouping tab group value
				$tempgrplevel=0;
				$newgrpkey=0;
				$tabgroup=array();
				foreach ($modtabsecfrmfkdsgrp as $key => $val) {
					if($tempgrplevel==$val['modtabgrpid']) {
						$tabgroup[$tempgrplevel][$newgrpkey]=$val;
					} else {
						$tabgroup[$val['modtabgrpid']][$newgrpkey]=$val;
					}
					$newgrpkey++;       
				}
				$tabsectiongrp = array();
				if(count($tabgroup) > 0) {
					//group tab section within tab group
					$tempgrpseclevel=0;
					$newgrpseckey=0;
					foreach($tabgroup as $key => $value) {
					   foreach($value as $key1 => $val) {
							if($tempgrpseclevel==$val['tabsectionid']) {
								$tabsectiongrp[$key][$tempgrpseclevel][$newgrpseckey]=$val;
							} else {
								$tabsectiongrp[$key][$val['tabsectionid']][$newgrpseckey]=$val;
							}
							$newgrpseckey++; 
						}
					}
				}
				//form section and elements generation
				$formtabdatas="";
				foreach($tabsectiongrp as $grpkey => $grpvalarr) {
					$prevgrpsecid = " ";
					$tabsection="";
					foreach($grpvalarr as $seckey => $secvalarr) { //section creation
						$tabsection .= '<li class="forrestrict">';
						foreach($secvalarr as $fieldkey => $fieldval) { //form field creation
							$curgrpsecid = $fieldval['tabsectionid'];
							if($prevgrpsecid != $curgrpsecid) {
								$prevgrpsecid=$fieldval['tabsectionid'];
								//check section is delete able
								$delmode = $this->sectiongrpdelmodecheck($fieldval['tabsectionid']);
								if($delmode >= 1 ) {
									$restrict = "Yes";
								} else {
									$restrict = "No";
								}
								$tabsection .= '<div class="large-4 column end forselectsection paddingbtm">';
								$tabsection .= '<div id="spanforaddelements'.$tabsecid.'" class="large-12 columns cleardataform ddddd" data-forsorting="'.$tabsecid.'" data-spanforaddelements="'.$tabsecid.'" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff;padding-bottom:1rem">';
								$tabsection .= '<div class="large-12 columns headerformcaptionstyle spansectionelemgen" contenteditable="true" data-spansectionelemgennum="'.$tabsecid.'" data-sectionfieldid="'.$fieldval['tabsectionid'].'" data-sectionfieldresct="'.$restrict.'">'.$fieldval['tabsectionname'].'</div>';
								$tabsection .= '<input id="spantabsectinfo'.$tabsecid.'" type="hidden" value="'.$tabgrpid.','.$tabsecid.','.$fieldval['tabsectionname'].','.$fieldval['tabsectionid'].'" name="spantabsectinfo[]">';
								$tabsection .= '<ul id="sortable'.$tabsecid.'" class="sortable"  data-datasectionattrid="'.$tabsecid.'">';
								$tabsecids[] = $tabsecid;
								$tabsecid++;
							}
							//function call for fields generation
							if($fieldval['colname'] != "salutationid") {
								$secfielddata = $this->formfieldsgenerate($fieldval,($tabsecid-1),$tabfieldid,$moduleid);
								$tabsection .= $secfielddata;
								$tabsecfieldids[] = $tabfieldid;
								$tabfieldid++;
							}
						}
						$tabsection .= '</ul>';
						$tabsection .= '</div>';
						$tabsection .= '<div class="large-12 columns" style="background: #ffffff; text-align: right"><label> </label><input class="button dboardsbtn frmtogridbutton" type="button" name="" value="Submit"></div>';
						$tabsection .= '</div>';
						$tabsection .= '<div class="large-8 column"><img src="'.base_url().'img/builders/samplegrid.jpg"/>';
						$tabsection .= '</li>';
					}
					$formtabdatas.= $tabsection;
				}
				$tabsecgrpdatas .= $formtabdatas;
			} else if($templatetypeid == '3') { //template 3 - without editor
				$modtabsecfrmfkdsgrp = $this->moduletabgrpsecfieldsgeneration($moduleid,$tabgroupid);
				//grouping tab group value
				$tempgrplevel=0;
				$newgrpkey=0;
				$tabgroup=array();
				foreach ($modtabsecfrmfkdsgrp as $key => $val) {
					if($tempgrplevel==$val['modtabgrpid']) {
						$tabgroup[$tempgrplevel][$newgrpkey]=$val;
					} else {
						$tabgroup[$val['modtabgrpid']][$newgrpkey]=$val;
					}
					$newgrpkey++;       
				}
				$tabsectiongrp = array();
				if(count($tabgroup) > 0) {
					//group tab section within tab group
					$tempgrpseclevel=0;
					$newgrpseckey=0;
					foreach($tabgroup as $key => $value) {
					   foreach($value as $key1 => $val) {
							if($tempgrpseclevel==$val['tabsectionid']) {
								$tabsectiongrp[$key][$tempgrpseclevel][$newgrpseckey]=$val;
							} else {
								$tabsectiongrp[$key][$val['tabsectionid']][$newgrpseckey]=$val;
							}
							$newgrpseckey++; 
						}
					}
				}
				//form section and elements generation
				$formtabdatas="";
				foreach($tabsectiongrp as $grpkey => $grpvalarr) {
					$prevgrpsecid = " ";
					$tabsection="";
					foreach($grpvalarr as $seckey => $secvalarr) { //section creation
						$tabsection .= '<li class="forrestrict">';
						foreach($secvalarr as $fieldkey => $fieldval) { //form field creation
							$curgrpsecid = $fieldval['tabsectionid'];
							if($prevgrpsecid != $curgrpsecid) {
								$prevgrpsecid=$fieldval['tabsectionid'];
								//check section is delete able
								$delmode = $this->sectiongrpdelmodecheck($fieldval['tabsectionid']);
								if($delmode >= 1 ) {
									$restrict = "Yes";
								} else {
									$restrict = "No";
								}
								$tabsection .= '<div class="large-4 column end forselectsection paddingbtm">';
								$tabsection .= '<div id="spanforaddelements'.$tabsecid.'" class="large-12 columns cleardataform ddddd" data-forsorting="'.$tabsecid.'" data-spanforaddelements="'.$tabsecid.'" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff;padding-bottom:1rem">';
								$tabsection .= '<div class="large-12 columns headerformcaptionstyle spansectionelemgen" contenteditable="true" data-spansectionelemgennum="'.$tabsecid.'" data-sectionfieldid="'.$fieldval['tabsectionid'].'" data-sectionfieldresct="'.$restrict.'">'.$fieldval['tabsectionname'].'</div>';
								$tabsection .= '<input id="spantabsectinfo'.$tabsecid.'" type="hidden" value="'.$tabgrpid.','.$tabsecid.','.$fieldval['tabsectionname'].','.$fieldval['tabsectionid'].'" name="spantabsectinfo[]">';
								$tabsection .= '<ul id="sortable'.$tabsecid.'" class="sortable" data-datasectionattrid="'.$tabsecid.'">';
								$tabsecids[] = $tabsecid;
								$tabsecid++;
							}
							//function call for fields generation
							if($fieldval['colname'] != "salutationid") {
								$secfielddata = $this->formfieldsgenerate($fieldval,($tabsecid-1),$tabfieldid,$moduleid);
								$tabsection .= $secfielddata;
								$tabsecfieldids[] = $tabfieldid;
								$tabfieldid++;
							}
						}
						$tabsection .= '</ul>';
						$tabsection .= '</div>';
						$tabsection .= '</div>';
						$tabsection .= '<div class="large-8 column"><img src="'.base_url().'img/builders/sampleeditor.jpg"/>';
						$tabsection .= '</li>';
					}
					$formtabdatas.= $tabsection;
				}
				$tabsecgrpdatas .= $formtabdatas;
			}
			$tabgrpclose = '</ul>';
			$formdatas.= $tabgrpstart.$tabsecgrpdatas.$tabgrpclose;
			$tabgrpid++;
		}
		$inci = "<input name='inci' type='hidden' id='inci' value='".$tabsecid."' />";
		$iden = "<input name='iden' type='hidden' id='iden' value='".$tabfieldid."' />";
		$grp = "<input name='grp' type='hidden' id='grp' value='".$tabgrpid."' />";
		$cner = "<input name='cner' type='hidden' id='cner' value='1' />";
		$tabgrplabids = "<input name='tabgrplabelspanids' type='hidden' id='tabgrplabelspanids' value='".implode(',',$tabgrpids)."' />";
		$tabseclabids = "<input name='tabseclabelspanids' type='hidden' id='tabseclabelspanids' value='".implode(',',$tabsecids)."' />";
		$tabfieldlabids = "<input name='tabfieldlabelspanids' type='hidden' id='tabfieldlabelspanids' value='".implode(',',$tabsecfieldids)."' />";
		$hidbox = $inci.$iden.$grp.$cner.$tabgrplabids.$tabseclabids.$tabfieldlabids;
		$formlayouts = $tabgrp.$formdatas.$hidbox;
		/* Action icons */
		$actionassign = $this->Basefunctions->actionassign($moduleid);
		$icon='';
		$prev = " ";
		foreach($actionassign as $key):
		if($key->toolbarname!='') {
			$cur = $key->toolbarcategoryid;
			if($prev!=$cur) {
				if($prev != " ") {
					$icon.= '</li></ul>';
				}
				$icon.= '<ul class="actionicons-view">
							<li class="header-name">'.$key->toolbarcategoryname.'</li>
							<li class="action-icons">';
				$prev=$key->toolbarcategoryid;
			}
			$fname = preg_replace('/[^A-Za-z0-9]/', '',$key->toolbarname);
			$name = strtolower($fname);
			$icon.= '<span class="icon-box"><i id="'.$name.'icon" class="icon '.$key->description.' '.$name.'iconclass" title="'.$key->toolbartitle.'"></i></span>'.PHP_EOL;
		}
		endforeach;
		if($prev != " ") {
			$icon.= '</li></ul>';
		}
		echo json_encode(array('formdata'=>$formlayouts,'icons'=>$icon));
	}
	//public function random number info fetch
	public function randomnuminfo($moduleid,$modulefieldid) {
		$dataset = array('suffix'=>'','startnumber'=>'','incrementby'=>'','random'=>'','prefix'=>'');
		$data=$this->db->select('suffix,startingnumber,incrementby,random,prefix')->from('serialnumbermaster')->where('moduleid',$moduleid)->where('modulefieldid',$modulefieldid)->get();
		foreach($data->result() as $info) {
			$dataset = array('suffix'=>$info->suffix,'startnumber'=>$info->startingnumber,'incrementby'=>$info->incrementby,'random'=>$info->random,'prefix'=>$info->prefix);
		}
		return $dataset;
	}
	//form fields generation
	public function formfieldsgenerate($fieldval,$secids,$elemids,$moduleid) {
		//field column style
		$colmtypeid = $fieldval['fieldcolmtypeid'];
		$colmtype = "No";
		if($colmtypeid == 1) {
			$colmtype = "No";
			$colstyle = "large-12 medium-12 small-12 columns";
		} else if($colmtypeid == 2) {
			$colmtype = "Yes";
			$colstyle = "large-6 medium-6 small-6 end columns";
		}
		$mandatory = $fieldval['mandmode'];
		$active = $fieldval['fieldmode'];
		$readonly = $fieldval['readmode'];
		$defval = $fieldval['defvalue'];
		$desc = $fieldval['descrp'];
		$mandsapn = "";
		if($mandatory == 'Yes') {
			$mandsapn = '<span id="mandfildid'.$elemids.'" class="mandatoryfildclass">*</span>';
		}
		switch( $fieldval['uitypeid'] ) {
			case 2:
				//text box
				$textbox = '<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><input type="text" id="frmliveelm'.$elemids.'" value="'.$defval.'"><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="2" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',2,'.$fieldval['filedlabel'].','.$defval.','.$fieldval['maxlength'].','.$desc.','.$mandatory.','.$active.','.$readonly.','.$colmtype.','.$fieldval['fieldid'].'"></div></li>';
				return $textbox;
				break;

			case 3:
				//text area
				$textarea='<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><textarea id="frmliveelm'.$elemids.'" rows="3">'.$defval.'</textarea><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="3" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',3,'.$fieldval['filedlabel'].','.$defval.','.$fieldval['maxlength'].','.$fieldval['decisize'].','.$desc.','.$mandatory.','.$active.','.$readonly.','.$colmtype.','.$fieldval['fieldid'].'"></div></li>';
				return $textarea;
				break;

			case 4:
				//Integer
				$inttxtbox='<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><input type="text" id="frmliveelm'.$elemids.'" value="'.$defval.'"><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="4" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',4,'.$fieldval['filedlabel'].','.$defval.','.$desc.','.$mandatory.','.$active.','.$readonly.','.$colmtype.','.$fieldval['fieldid'].'"></div></li>';
				return $inttxtbox;
				break;

			case 5:
				//Decimal
				$dectxtbox='<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><input type="text" id="frmliveelm'.$elemids.'" value="'.$defval.'"><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="5" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',5,'.$fieldval['filedlabel'].','.$defval.','.$fieldval['decisize'].','.$desc.','.$mandatory.','.$active.','.$readonly.','.$colmtype.','.$fieldval['fieldid'].'"></div></li>';
				return $dectxtbox;
				break;

			case 6:
				//Percentage
				$pertxtbox='<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><input type="text" id="frmliveelm'.$elemids.'" value="'.$defval.'"><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="6" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',6,'.$fieldval['filedlabel'].','.$defval.','.$fieldval['decisize'].','.$desc.','.$mandatory.','.$active.','.$readonly.','.$colmtype.','.$fieldval['fieldid'].'"></div></li>';
				return $pertxtbox;
				break;

			case 7:
				//Currency
				$curtxtbox='<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><input type="text" id="frmliveelm'.$elemids.'" value="'.$defval.'"><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="7" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',7,'.$fieldval['filedlabel'].','.$defval.','.$fieldval['decisize'].','.$desc.','.$mandatory.','.$active.','.$readonly.','.$colmtype.','.$fieldval['fieldid'].'"></div></li>';
				return $curtxtbox;
				break;

			case 8:
				$daterest = $fieldval['ddparenttable'];
				$restrict = ( $daterest!=''?implode(',',(explode('|',$daterest))):"Yes,No" );
				//Date
				$datetxtbox='<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><input type="text" id="frmliveelm'.$elemids.'" value="'.$defval.'"><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="8" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',8,'.$fieldval['filedlabel'].','.$defval.','.$desc.','.$mandatory.','.$active.','.$readonly.','.$colmtype.','.$restrict.','.$fieldval['fieldid'].'"></div></li>';
				return $datetxtbox;
				break;

			case 9:
				//Time
				$timetxtbox='<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><input type="text" id="frmliveelm'.$elemids.'" value="'.$defval.'"><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="9" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',9,'.$fieldval['filedlabel'].','.$defval.','.$desc.','.$mandatory.','.$active.','.$readonly.','.$colmtype.','.$fieldval['fieldid'].'"></div></li>';
				return $timetxtbox;
				break;

			case 10:
				//Email
				$mailtxtbox='<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><input type="text" id="frmliveelm'.$elemids.'" value="'.$defval.'"><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="10" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',10,'.$fieldval['filedlabel'].','.$defval.','.$desc.','.$mandatory.','.$active.','.$readonly.','.$colmtype.','.$fieldval['modfieldunique'].','.$fieldval['fieldid'].'"></div></li>';
				return $mailtxtbox;
				break;

			case 11:
				//Phone
				$phonetxtbox='<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><input type="text" id="frmliveelm'.$elemids.'" value="'.$defval.'"><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="11" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',11,'.$fieldval['filedlabel'].','.$defval.','.$desc.','.$mandatory.','.$active.','.$readonly.','.$colmtype.','.$fieldval['modfieldunique'].','.$fieldval['fieldid'].'"></div></li>';
				return $phonetxtbox;
				break;

			case 12:
				//URL
				$urltxtbox='<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><input type="text" id="frmliveelm'.$elemids.'" value="'.$defval.'"><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="12" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',12,'.$fieldval['filedlabel'].','.$defval.','.$desc.','.$mandatory.','.$active.','.$readonly.','.$colmtype.','.$fieldval['fieldid'].'"></div></li>';
				return $urltxtbox;
				break;

			case 13:
				//Check box
				$chkattr = "";
				$chkval = "No";
				if($defval == 'Yes') {
					$chkattr = " checked='checked' ";
					$chkval = "Yes";
				} else {
					$chkval = "No";
				}	
				if($fieldval['ddparenttable'] != ''){
					$cond = explode('|',$fieldval['ddparenttable']);
					$hideshow = $cond[0];
					$fieldname = $cond[2];
				}else{
					$hideshow = 'No';
					$fieldname = '';
				}				
				$chkbox='<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><input type="checkbox" id="frmliveelm'.$elemids.'"'.$chkattr.'value="'.$chkval.'"><label for="frmliveelm'.$elemids.'"'.$chkattr.' id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="13" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',13,'.$fieldval['filedlabel'].','.$hideshow.',,' .$fieldname.','.$desc.','.$mandatory.','.$active.','.$readonly.','.$chkval.','.$colmtype.','.$fieldval['fieldid'].'"></div></li>'; 
				return $chkbox;
				break;

			case 14:
				//Auto Number
				$dataset = $this->randomnuminfo($moduleid,$fieldval['fieldid']);
				$autonumtxtbox='<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><input type="text" id="frmliveelm'.$elemids.'" value="'.$defval.'"><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="14" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',14,'.$fieldval['filedlabel'].','.$dataset['prefix'].','.$dataset['startnumber'].','.$dataset['suffix'].','.$dataset['random'].','.$desc.','.$mandatory.','.$active.','.$readonly.','.$colmtype.','.$fieldval['fieldid'].'"></div></li>';
				return $autonumtxtbox;
				break;

			case 15:
				//File Attachment
				$fattachbox='<li id="eleliattrid'.$elemids.'"><div class="large-12 medium-12 small-12 end columns frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><span class="icon-25b icon25b-display driveiconsize"></span><span class="icon-25b icon25b-dropbox driveiconsize"></span><!--<span class="fa fa-cloud driveiconsize"></span>--><div class="large-12 medium-12 small-12 column" style="height:10rem; border:1px solid #CCCCCC"></div><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="15" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',15,'.$fieldval['filedlabel'].','.$fieldval['maxlength'].','.$desc.','.$fieldval['multiple'].','.$mandatory.','.$active.','.$colmtype.','.$fieldval['fieldid'].'"></div></li>';
				return $fattachbox;
				break;

			case 16:
				//Image Attachment
				$imgattachbox='<li id="eleliattrid'.$elemids.'"><div class="large-12 medium-12 small-12 end columns frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><span class="icon-25b icon25b-display driveiconsize"></span><span class="icon-25b icon25b-dropbox driveiconsize"></span><!--<span class="fa fa-cloud driveiconsize"></span>--><div class="large-12 medium-12 small-12 column" style="height:10rem; border:1px solid #CCCCCC"></div><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="16" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',16,'.$fieldval['filedlabel'].','.$fieldval['maxlength'].','.$fieldval['customrule'].','.$desc.','.$mandatory.','.$active.','.$colmtype.','.$fieldval['fieldid'].'"></div></li>'; 
				return $imgattachbox;
				break;

			case 17:
				//simple drop down
				$value = array();
				$txtval = "";
				$tablename = substr($fieldval['colname'],0,-2);
				$tabid = $tablename.'id';
				$tabfieldname = $tablename.'name';
				$ddowndata = $this->Basefunctions->dynamicdropdownvalfetch($fieldval['moduleid'],$tablename,$tabfieldname,$tabid);
				foreach($ddowndata as $key):
					$value[$key->Id] = $key->Name;
					if( $key->defaultopt == 'Yes' || $key->defaultopt == '1' ) {
						$txtval = $key->Name;
					}
				endforeach;
				$ddval = implode('|',$value);
				$dropdownbox='<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><select data-placeholder="Select" class="chzn-select" id="frmliveelm'.$elemids.'"></select><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="17" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',17,'.$fieldval['filedlabel'].','.$ddval.','.$txtval.','.$desc.','.$mandatory.','.$active.','.$readonly.','.$fieldval['multiple'].','.$colmtype.',No,'.$fieldval['fieldid'].'"></div></li>';
				return $dropdownbox;
				break;

			case 18:
				//group drop down
				$grpdropdownbox='<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><select data-placeholder="Select" class="chzn-select" id="frmliveelm'.$elemids.'"></select><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="18" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',18,'.$fieldval['filedlabel'].',,,'.$desc.','.$mandatory.','.$active.','.$readonly.','.$fieldval['multiple'].','.$colmtype.',No,'.$fieldval['fieldid'].'"></div></li>';
				return $grpdropdownbox;
				break;

			case 19:
				//drop down for parent table
				$pardropdownbox='<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><select data-placeholder="Select" class="chzn-select" id="frmliveelm'.$elemids.'"></select><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="19" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',19,'.$fieldval['filedlabel'].',,,'.$desc.','.$mandatory.','.$active.','.$readonly.','.$fieldval['multiple'].','.$colmtype.',No,'.$fieldval['fieldid'].'"></div></li>';
				return $pardropdownbox;
				break;

			case 20:
				//group drop down with multiple data attr
				$grpddmulattrbox='<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><select data-placeholder="Select" class="chzn-select" id="frmliveelm'.$elemids.'"></select><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="20" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',20,'.$fieldval['filedlabel'].',,,'.$desc.','.$mandatory.','.$active.','.$readonly.','.$fieldval['multiple'].','.$colmtype.',No,'.$fieldval['fieldid'].'"></div></li>';
				return $grpddmulattrbox;
				break;

			case 21:
				//tree geneate
				$treegenbox='<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label>   <div id="dl-menu'.$elemids.'" class="dl-menuwrapper" style="z-index:2;margin-bottom: 1rem"><button id="treebutton" class="btn dl-trigger treemenubtnclick" style="height:2.2rem;color:#ffffff;font-size: 1.45rem;padding: 0.1rem;padding-top:0.3rem;background-color:#546e7a;" type="button" name="treebutton"><i class="material-icons">format_indent_increase</i></button><input id="frmliveelm'.$elemids.'" type="text" style="width:83%;display:inline;position:absolute;height:2.12rem;padding-left: 5px;" /><ul class="dl-menu maintreegenerate large-12 medium-6 small-12 column"></ul></div><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="21" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',21,'.$fieldval['filedlabel'].',,,'.$desc.','.$mandatory.','.$active.','.$readonly.','.$fieldval['multiple'].','.$colmtype.',No,'.$fieldval['fieldid'].'"></div></li>';
				return $treegenbox;
				break;
				
			case 22:
				//password text box
				$passwdbox = '<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><input type="password" id="frmliveelm'.$elemids.'" value="'.$defval.'"><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="22" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',22,'.$fieldval['filedlabel'].','.$desc.','.$defval.','.$mandatory.','.$active.','.$readonly.','.$colmtype.','.$fieldval['fieldid'].'"></div></li>';
				return $passwdbox;
				break;

			case 23:
				//attribute set view create
				$attrdata = $this->attributeviewdataddfetch($fieldval['moduleid']);
				foreach($attrdata as $key) {
					$txtval = "";
					$value = array();
					$ddval = implode('|',$value);
					$attrdropdownbox='<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$key['Name'].''.$mandsapn.'</label><select data-placeholder="Select" class="chzn-select" id="frmliveelm'.$elemids.'"></select><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="23" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',23,'.$key['Name'].',,,'.$desc.','.$mandatory.','.$active.','.$readonly.','.$fieldval['multiple'].','.$colmtype.',No,'.$fieldval['fieldid'].'"></div></li>';
					return $attrdropdownbox;
				}
				break;

			case 24:
				//Text editor
				$imgattachbox='<li id="eleliattrid'.$elemids.'"><div class="large-12 columns frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><img src="'.base_url().'img/texteditor.jpg" class=""/><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="24" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',24,'.$fieldval['filedlabel'].','.$desc.','.$mandatory.','.$active.','.$readonly.','.$colmtype.','.$fieldval['fieldid'].'"></div></li>'; 
				return $imgattachbox;
				break;

			case 25:
				//multiple same parent drop down in single form
				$txtval = "";
				$value = array();
				$ddval = implode('|',$value);
				$pardropdownbox='<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><select data-placeholder="Select" class="chzn-select" id="frmliveelm'.$elemids.'"></select><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="25" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',25,'.$fieldval['filedlabel'].',,,'.$desc.','.$mandatory.','.$active.','.$readonly.','.$fieldval['multiple'].','.$colmtype.',No,'.$fieldval['fieldid'].'"></div></li>';
				return $pardropdownbox;
				break;

			case 26:
				//relation drop down
				$cond = $fieldval['ddparenttable'];
				$defvals = explode('|',$fieldval['defvalue']);
				$pardropdownbox='<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><select data-placeholder="Select" class="chzn-select" id="frmliveelm'.$elemids.'"></select><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="26" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',26,'.$fieldval['filedlabel'].','.$defvals[0].','.$defvals[1].','.$defvals[2].','.$defvals[3].','.$defvals[4].','.$desc.','.$cond.','.$mandatory.','.$active.','.$readonly.','.$fieldval['multiple'].','.$colmtype.','.$fieldval['fieldid'].'"></div></li>';
				return $pardropdownbox;
				break;

			case 27:
				//Module drop down value fetch
				$txtval = "";
				$value = array();
				$ddval = implode('|',$value);
				$pardropdownbox='<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><select data-placeholder="Select" class="chzn-select" id="frmliveelm'.$elemids.'"></select><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="27" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',27,'.$fieldval['filedlabel'].',,,'.$desc.','.$mandatory.','.$active.','.$readonly.','.$fieldval['multiple'].','.$colmtype.',No,'.$fieldval['fieldid'].'"></div></li>';
				return $pardropdownbox;
				break;

			case 28:
				//drop down with user specific condition
				$txtval = "";
				$value = array();
				$ddval = implode('|',$value);
				$pardropdownbox='<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><select data-placeholder="Select" class="chzn-select" id="frmliveelm'.$elemids.'"></select><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="28" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',28,'.$fieldval['filedlabel'].',,,'.$desc.','.$mandatory.','.$active.','.$readonly.','.$fieldval['multiple'].','.$colmtype.',No,'.$fieldval['fieldid'].'"></div></li>';
				return $pardropdownbox;
				break;
				
			case 29:
				//group drop down for parent table
				$pargrpdropdownbox='<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><select data-placeholder="Select" class="chzn-select" id="frmliveelm'.$elemids.'"></select><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="29" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',29,'.$fieldval['filedlabel'].',,,'.$desc.','.$mandatory.','.$active.','.$readonly.','.$fieldval['multiple'].','.$colmtype.',No,'.$fieldval['fieldid'].'"></div></li>';
				return $pargrpdropdownbox;
				break;
				
			case 30:
					//tags component
					 $defvals = implode('|',explode(',',$defval));
					$textbox = '<li id="eleliattrid'.$elemids.'"><div class="'.$colstyle.' frmelediv hovertoremove" id="columndivid'.$elemids.'" data-frmeledivattr="'.$elemids.'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'.$elemids.'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'"></span></span><label id="frmelemlivespantit'.$elemids.'">'.$fieldval['filedlabel'].''.$mandsapn.'</label><input type="text" id="frmliveelm'.$elemids.'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'.$elemids.'" data-frmeleuitypeattr="30" data-datasectionid="'.$secids.'" data-datafieldid="'.$fieldval['fieldid'].'" data-datafieldrescopt="'.$fieldval['fieldres'].'" value="'.$secids.',30,'.$fieldval['filedlabel'].','.$defvals.','.$desc.','.$mandatory.','.$active.','.$colmtype.','.$fieldval['fieldid'].'"></div></li>';
					return $textbox;
					break;
				
			default :
				//default content
				break;
		}
	}
	//module based tollbar action details fetch
    public function modcustactionassignmodel() {
		$tollbarid = "";
		$i=0;
		$moduleid = $_POST['mid'];
		$userroleid = $this->Basefunctions->userroleid;
		$this->db->select('toolbargroup.toolbarnameid AS grouptoolbarid,usertoolbarprivilege.toolbarnameid AS toolbarid');
		$this->db->from('toolbargroup');
		$this->db->join('usertoolbarprivilege','usertoolbarprivilege.moduleid=toolbargroup.moduleid','left outer');
		$this->db->where("FIND_IN_SET('$userroleid',usertoolbarprivilege.userroleid) >", 0);
		$this->db->where("toolbargroup.moduleid",$moduleid);
		$this->db->where('toolbargroup.status',1);
		$this->db->where('usertoolbarprivilege.status',1);
		$query= $this->db->get();
		foreach ($query->result() as $row) {
			$tollbarid = $row->toolbarid;
			$grptollbarid = $row->grouptoolbarid;
		}
		$tollbararrid = explode(',',$grptollbarid);
		sort($tollbararrid);
		$this->db->select('description,toolbarname,toolbartitle,toolbarnameid');
		$this->db->from('toolbarname');
		$this->db->where_in('toolbarnameid',$tollbararrid);
		$this->db->where('toolbarname.status',1);
		//$this->db->order_by('toolbarwidth','asc');
		$tollbar = $this->db->get();
		if($tollbar->num_rows() >= 1) {
			foreach($tollbar->result() as $data) {
				$tollbardata[$i] = array('id'=>$data->toolbarnameid,'name'=>$data->toolbarname,'desc'=>$data->description,'title'=>$data->toolbartitle,'grptoolbarid'=>$tollbarid);
				$i++;
			}
			echo json_encode($tollbardata);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
    }
	//attribute value fetch by using attribute name and id
	public function dynamicattributedropdownvalfetch($attributeid) {
        $dropdowndata = array();
        $this->db->select('attributevalueid AS Id,attributevaluename AS Name');
        $this->db->from('attributevalue');
        $this->db->join("attribute","attribute.attributeid=attributevalue.attributeid");
        $this->db->where("FIND_IN_SET('".$attributeid."',attributevalue.attributeid) >", 0);
		$this->db->where('attributevalue.status', 1);
        $query= $this->db->get();
        return $query->result();
	}
	//attribute set name fetch
	public function attributeviewdataddfetch($moduleid) {
		$data = array();
		$i=0;
		$this->db->select("attribute.attributename,attribute.attributeid");
		$this->db->from("attribute");
		$this->db->join("attributesetassign","attributesetassign.attributeid=attribute.attributeid");
		$this->db->join("attributeset","attributeset.attributesetid=attributesetassign.attributesetid");
		$this->db->join("moduleattributesetting","moduleattributesetting.attributesetid=attributeset.attributesetid");
		$this->db->where("moduleattributesetting.moduleid",$moduleid);
		$this->db->where("moduleattributesetting.status",1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result()as $row) {
				$data[$i]=array('Id'=>$row->attributeid,'Name'=>$row->attributename);
				$i++;
			}
		}
		return $data;		
	}
	//tab group information fetch
	public function moduletabgroupgeneration($moduleid) {
		$userroleid = $this->Basefunctions->userroleid;
        $i=0;
        $modtabgrparray = array();
		$this->db->select('moduletabgroup.moduletabgroupid,moduletabgroup.moduleid,moduletabgroup.moduletabgroupname,moduletabgroup.moduletabgrouptemplateid');
		$this->db->from('moduletabgroup');
		$this->db->join('module','module.moduleid=moduletabgroup.moduleid');
		$this->db->where('moduletabgroup.moduleid',$moduleid);
		$this->db->where("FIND_IN_SET('$userroleid',moduletabgroup.userroleid) >", 0);
		$this->db->where('moduletabgroup.status',1);
		$this->db->order_by('moduletabgroup.sequence','asc');
		$querytbg = $this->db->get();
		foreach($querytbg->result() as $rows) {
			$modtabgrparray[$i] = array('tabpgrpid'=>$rows->moduletabgroupid,'tabgrpname'=>$rows->moduletabgroupname,'moduleid'=>$rows->moduleid,'templatetypeid'=>$rows->moduletabgrouptemplateid);
			$i++;
		}
        return $modtabgrparray;
    }
	//Module tab section and fields generation based on tab group
    public function moduletabgrpsecfieldsgeneration($moduleid,$tabgroupid) {
		$userroleid = $this->Basefunctions->userroleid;
        $i=0;
        $moduletabsecfldsgrparray = array();
		$this->db->select('modulefield.modulefieldid,modulefield.columnname,modulefield.tablename,modulefield.uitypeid,modulefield.fieldname,modulefield.fieldlabel,modulefield.readonly,modulefield.defaultvalue,modulefield.maximumlength,modulefield.mandatory,modulefield.active,modulefield.moduletabsectionid,modulefield.sequence,moduletabsection.moduletabsectionname,moduletabgroup.moduletabgroupid,modulefield.moduletabid,modulefield.multiple,modulefield.ddparenttable,modulefield.parenttable,modulefield.fieldcolumntype,module.modulename,module.modulelink,modulefield.decimalsize,moduletabsection.moduletabsectiontypeid,modulefield.fieldcustomvalidation,modulefield.description,modulefield.fieldrestrict,modulefield.unique');
		$this->db->from('modulefield');
		$this->db->join('uitype','uitype.uitypeid=modulefield.uitypeid');
		$this->db->join('moduletabsection','moduletabsection.moduletabsectionid=modulefield.moduletabsectionid');
		$this->db->join('moduletabgroup','moduletabgroup.moduletabgroupid=moduletabsection.moduletabgroupid');
		$this->db->join('module','module.moduleid=moduletabgroup.moduleid');
		$this->db->where("moduletabgroup.moduletabgroupid", $tabgroupid);
		$this->db->where("modulefield.moduletabid", $moduleid);
		$this->db->where("modulefield.active",'Yes');
		$this->db->where("FIND_IN_SET('$userroleid',moduletabsection.userroleid) >", 0);
		$this->db->where("FIND_IN_SET('$userroleid',modulefield.userroleid) >", 0);
		$this->db->where_in('modulefield.status',array(1));
		$this->db->where('moduletabsection.moduletabsectiontypeid',1);
		$this->db->where('moduletabsection.status',1);
		$this->db->order_by('moduletabsection.sequence','asc');
		$this->db->order_by('moduletabsection.moduletabsectionid','asc');
		$this->db->order_by('modulefield.sequence','asc');
		$querytbg = $this->db->get();
		foreach($querytbg->result() as $rows) {
			$moduletabsecfldsgrparray[$i] = array('fieldid'=>$rows->modulefieldid,'colname'=>$rows->columnname,'tabname'=>$rows->tablename,'uitypeid'=>$rows->uitypeid,'fieldname'=>$rows->fieldname,'filedlabel'=>$rows->fieldlabel,'readmode'=>$rows->readonly,'defvalue'=>$rows->defaultvalue,'maxlength'=>$rows->maximumlength,'mandmode'=>$rows->mandatory,'fieldmode'=>$rows->active,'tabsectionid'=>$rows->moduletabsectionid,'fieldseq'=>$rows->sequence,'tabsectionname'=>$rows->moduletabsectionname,'modtabgrpid'=>$rows->moduletabgroupid,'moduleid'=>$rows->moduletabid,'modulename'=>$rows->modulename,'multiple'=>$rows->multiple,'ddparenttable'=>$rows->ddparenttable,'parenttable'=>$rows->parenttable,'fieldcolmtypeid'=>$rows->fieldcolumntype,'modlink'=>$rows->modulelink,'decisize'=>$rows->decimalsize,'sectiontypeid'=>$rows->moduletabsectiontypeid,'customrule'=>$rows->fieldcustomvalidation,'descrp'=>$rows->description,'fieldres'=>$rows->fieldrestrict,'modfieldunique'=>$rows->unique);
			$i++;
		}
        return $moduletabsecfldsgrparray;
    }
	//check section have mandatory fields
	public function sectiongrpdelmodecheck($tabsectionid) {
		$dataset = 0;
		$result=$this->db->query('select count(fieldrestrict) AS opt from modulefield WHERE moduletabsectionid='.$tabsectionid.' AND fieldrestrict="Yes" AND modulefield.status=1')->result();
		foreach($result as $data) {
			$dataset = $data->opt;
		}
		return $dataset;
	}
	//check tab group have mandatory fields
	public function tabgrpdelmodecheck($tabgrpid) {
		$dataset = 0;
		$result=$this->db->query('select count(fieldrestrict) AS opt from modulefield join moduletabsection ON moduletabsection.moduletabsectionid=modulefield.moduletabsectionid join moduletabgroup ON moduletabgroup.moduletabgroupid=moduletabsection.moduletabgroupid WHERE moduletabgroup.moduletabgroupid='.$tabgrpid.' AND modulefield.fieldrestrict="Yes" AND modulefield.status=1')->result();
		foreach($result as $data) {
			$dataset = $data->opt;
		}
		return $dataset;
	}
	//drop down condition field name fetch
	public function ddcondfieldnamefetchmodel() {
		$id = $_GET['id'];
		$i=0;
		$this->db->select('modulefield.modulefieldid,modulefield.fieldlabel,modulefield.columnname,modulefield.tablename,modulefield.parenttable,modulefield.uitypeid');
		$this->db->from('modulefield');
		$this->db->join('uitype','uitype.uitypeid = modulefield.uitypeid');
		$this->db->where('moduletabid',$id);
		$this->db->where_not_in('uitype.uitypeid',array(15,16,22,24,26));
		$this->db->where('modulefield.status',1);
		$this->db->where_in('uitype.status',array(1,3));
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result()as $row) {
				$data[$i]=array('datasid'=>$row->modulefieldid,'dataname'=>$row->fieldlabel,'datacolmname'=>$row->columnname,'datatblname'=>$row->tablename,'datapartable'=>$row->parenttable,'datauitype'=>$row->uitypeid);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//pick list table name check
	public function picklistnamecheckmodel() {
		$database = $this->db->database;
		$pklistname = $_POST['picklistname'];
		if($pklistname!= "") {
			$picklistname = preg_replace('/[^A-Za-z0-9]/', '', $pklistname);
			$result = tableexitcheck($database,$picklistname);
			if( $result > 0 ) {
				echo "True";
			} else { echo "False"; }
		} else { echo "False"; }
	}
	//label name check
	public function filednamecheckmodel() {
		$moduleid=$_POST['moduleid'];
		$fieldname=$_POST['fieldname'];
		$elementpartableid = 'modulefieldid';
		$partable = substr($elementpartableid, 0, -2);
		$ptid = $partable.'id';
		$ptname = 'fieldlabel';
		if($fieldname != "") {
			$result = $this->db->select($ptname.','.$ptid)->from($partable)->where($partable.'.'.$ptname,$fieldname)->where($partable.'.moduletabid',$moduleid)->where($partable.'.status',1)->where_not_in($partable.'.status',array(3))->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->$ptid;
				}
			} else { echo "False"; }
		} else { echo "False"; }
	}
	//table name check
	public function tableexitcheck($database,$newtable) {
		$counttab=0;
		$tabexits = $this->db->query("SELECT COUNT(*) AS tabcount FROM information_schema.tables WHERE table_schema = '".$database."'AND table_name = '".$newtable."'");
		foreach($tabexits->result() as $tkey) { 
			$counttab = $tkey->tabcount;
		};
		return $counttab;
	}
	//table field data type check
	public function tablefieldtypecheck($tblname,$fieldname) {
		$fields = $this->db->field_data($tblname);
		foreach ($fields as $field) {
			$name =  $field->name;
			if($fieldname == $name) {
				return $field->type;
			}
		}
	}
	//table field name check
	public function tablefieldnamecheck($tblname,$fieldname) {
		$result = 'false';
		$fields = $this->db->field_data($tblname);
		foreach ($fields as $field) {
			$name =  $field->name;
			if($fieldname == $name) {
				$result = 'true';
			}
		}
		return $result;
	}
	//check drop down table type
	public function checkdropdowntabletype($database,$tblcolumname) {
		$tblname=substr(strtolower($tblcolumname),0,-2);
		$result = $this->tableexitcheck($database,$tblname);
		if( $result > 0 ) {
			$tabexits = $this->db->query("SELECT COUNT(*) AS tabcount FROM information_schema.COLUMNS WHERE table_schema = '".$database."'AND table_name = '".$tblname."' AND column_name='setdefault'");
			foreach($tabexits->result() as $tkey) {
				if($tkey->tabcount > 0) {
					return "17";
				} else { return "19"; }
			}
		} else { return "17"; }
	}
	//module filed value fetch
	public function modulefieldinformationfetch($tablename,$fieldname,$fieldid) {
		$dataset = "";
		$result=$this->db->query(' select '.$fieldname.' AS val from '.$tablename.' WHERE '.$tablename.'id='.$fieldid.' AND '.$tablename.'.status=1 ')->result();
		foreach($result as $data) {
			$dataset = $data->val;
		}
		return $dataset;
	}
	//new tab group creation
    public function tapgroupcreatemodel($moduleid,$tabgrptype,$tabgrpname,$sequence) {
		$dataarr = array(
			'moduleid'=>$moduleid,
			'moduletabgroupname'=>$tabgrpname,
			'usermoduletabgroupname'=>$tabgrpname,
			'moduletabgrouptemplateid'=>$tabgrptype,
			'userroleid'=>$this->Basefunctions->userroleid,
			'sequence'=>$sequence
		);
		$defdataarr = $this->defaultvalueget();
		$newdata = array_merge($dataarr,$defdataarr);
		$this->db->insert('moduletabgroup', array_filter($newdata) );
		$tabgrpid = $this->db->insert_id();
		return $tabgrpid;
    }
	//tab group update
	public function tapgroupupdatemodel($tabgrouptype,$tabgrpname,$tabgrpid,$sequence) {
		$dataarr = array(
			'usermoduletabgroupname'=>ucwords($tabgrpname),
			'moduletabgrouptemplateid'=>$tabgrouptype,
			'sequence'=>$sequence
		);
		$defdataarr = $this->updatedefaultvalueget();
		$newdata = array_merge($dataarr,$defdataarr);
		$this->db->where('moduletabgroupid',$tabgrpid);
		$this->db->update( 'moduletabgroup', array_filter($newdata) );
	}
	//tab section update
	public function tapgroupsectionupdatemodel($tabgrpsecname,$tabgrpid,$tabgrpsecid,$sequence) {
		$dataarr = array(
			'moduletabgroupid'=>$tabgrpid,
			'moduletabsectionname'=>ucwords($tabgrpsecname),
			'sequence'=>$sequence
		);
		$defdataarr = $this->updatedefaultvalueget();
		$newdata = array_merge($dataarr,$defdataarr);
		$this->db->where('moduletabsectionid',$tabgrpsecid);
		$this->db->update( 'moduletabsection', array_filter($newdata) );
	}
	//new tab section creation
    public function tapgroupsectioncreatemodel($moduleid,$tabgrpid,$tabsecname,$sequence) {
        $dataarr = array(
            'moduleid'=>$moduleid,
            'moduletabgroupid'=>$tabgrpid,
            'moduletabsectionname'=>$tabsecname,
			'userroleid'=>$this->Basefunctions->userroleid,
			'sequence'=>$sequence
        );
		$defdataarr = $this->defaultvalueget();
		$newdata = array_merge($dataarr,$defdataarr);
		$this->db->insert( 'moduletabsection', array_filter($newdata) );
		$tabsecid = $this->db->insert_id();
		return $tabsecid;
    }
	//form fields update
	public function formdataeditmodel() {
		$userversion = $_POST['userversion'];
		$tabgrupinfo = $_POST['spantabgrpinfo'];
		$tabsecinfo = $_POST['spantabsectinfo'];
		if(isset($_POST['frmelmattrinfo'])) {
			$frmeleminfo = $_POST['frmelmattrinfo'];
		} else {
			$frmeleminfo = array();
		}
		$delfieldid = $_POST['deletedfieldids'];
		$delsecid = $_POST['deletedsectionids'];
		$deltabid = $_POST['deletedtabids'];
		$userroleid = $this->Basefunctions->userroleid;
		$uid = $this->Basefunctions->userid;
		$date = date($this->Basefunctions->datef);
		$viewcolid = array();
		$dynamicgridname = array();
		$resttab = array();
		$editorfname = array();
		$editortabsecids = array();
		$editortablenames = array();
		$modulename = $_POST['modulename'];
		$moduleid = $_POST['moduleid'];
		$viewmoduleid = $moduleid;
		$modactionids = $_POST['actionids'];
		if(isset($_POST['newmodname'])) {
			$newmodname = $_POST['newmodname'];
		} else {
			$newmodname = '';
		}
		$parenttable = preg_replace('/[^A-Za-z0-9]/', '', $modulename);
		$maintable = strtolower($parenttable);
		$modname = strtolower($parenttable);
		$parenttable = $maintable;
		$database = $this->db->database;
		$tabseq = 1;
		if($newmodname != ''){
			$newmodnamearr=array('modulename'=>$newmodname,'menuname'=>$newmodname);
			$this->db->where('moduleid',$moduleid);
			$this->db->update( 'module',$newmodnamearr);
			$viewmodname=array('viewcreationmodulename'=>$newmodname);
			$this->db->where('viewcreationmoduleid',$moduleid);
			$this->db->update( 'viewcreationmodule',$viewmodname);
		}
		if($moduleid != "0") {
			$previoustab="";
			foreach($tabgrupinfo as $tabgrpvals) {
				//tab group function
				$tabgrpvalue = explode(',',$tabgrpvals);
				$grptabname = preg_replace('/[^A-Za-z0-9]/', '', $tabgrpvalue[2]);
				$editorcolname = $modname.strtolower($grptabname).'_editorfilename';
				$tabgrpchkid = $tabgrpvalue[3];
				if( $tabgrpchkid == 0 ) { //new tab group create
					$tabgroupid = $this->tapgroupcreatemodel($moduleid,$tabgrpvalue[0],$tabgrpvalue[2],$tabseq);
					$tabseq++;
				} else { //existing tab group update
					$tabgroupid = $tabgrpvalue[3];
					$this->tapgroupupdatemodel($tabgrpvalue[0],$tabgrpvalue[2],$tabgrpvalue[3],$tabseq);
					$tabseq++;
				}
				//tab section events
				$tabgrpid = $tabgrpvalue[1]; //match tab group and section
				$tabsecseq = 1; //tab section sequence
				foreach($tabsecinfo as $tabsecvals) {
					$tabsecvalue = explode(',',$tabsecvals);
					$tabsecid = $tabsecvalue[1];
					$tabsecchkid = $tabsecvalue[3];
					if($tabgrpid == $tabsecvalue[0] ) { //match tab group and section
						if( $tabsecchkid == 0 ) { //new tab section create
							$tabsectionid = $this->tapgroupsectioncreatemodel($moduleid,$tabgroupid,$tabsecvalue[2],$tabsecseq);
							$tabsecseq++;
						} else { //existing tab section update
							$tabsectionid = $tabsecchkid;
							$this->tapgroupsectionupdatemodel($tabsecvalue[2],$tabgroupid,$tabsectionid,$tabsecseq);
							$tabsecseq++;
						}
						//tab section field
						$sequmber = 1;
						foreach($frmeleminfo as $frmsecelem) {
							$frmsecelements = explode(',',$frmsecelem);
							//check current section form elements
							if($tabsecid == $frmsecelements[0] ) {
								//check section field is new / old
								$fieldchkid = $this->sectionfieldcheck($frmsecelements);
								if($fieldchkid == '0') { //new field creation
									$newfiled="No";
									$defdataarr = $this->defaultvalueget();
									//master table info
									$parentfield='';
									$parentfieldval='';
									$parenttable = $this->modulefieldinformationfetch('module','modulemastertable',$moduleid);
									if($parenttable == "") {
										$parenttable = preg_replace('/[^A-Za-z0-9]/', '', $modulename);
										$maintable = strtolower($parenttable);
										$maintableid = strtolower($parenttable);
										$customtab = "No";
									} else {
										$customtab = "Yes";
										$parentid = $parenttable.'id';
										$type = $this->tablefieldtypecheck($parenttable,$parentid);
										$parentfield = ','.$parenttable.'id '.$type.' UNSIGNED  NOT NULL DEFAULT "1"';
										$parentfieldval = ',1';
										$maintable = $parenttable.'cf';
										$maintableid = strtolower($parenttable);
									}
									//new column name generation
									$fieldname = $frmsecelements[2];
									$forkeycons = '';
									if($userversion == 2) {
										$modfildmaxid = $this->Basefunctions->maximumid('modulefield','modulefieldid');
										$fname = preg_replace('/[^A-Za-z0-9]/', '', $fieldname);
										if($fname=="name") {
											$fname = $fname."cf";
										}
										$tblcolumname = strtolower($fname.($modfildmaxid + 1));
									} else {
										$fname = preg_replace('/[^A-Za-z0-9]/', '', $fieldname);
										if($fname=="name") {
											$fname = $fname."cf";
										}
										$tblcolumname = strtolower($fname);
									}
									//name generation for password field
									if($frmsecelements[1] == '22' ) {
										$datafieldname = $tblcolumname.'_password';
									} else {
										if($tblcolumname=="name") {
											$datafieldname = $tblcolumname."cf";
										} else {
											$datafieldname = $tblcolumname;
										}
									}
									{//data type assign
										if($frmsecelements[1] == '3') {
											$datatype = 'text NULL';
										} else if($frmsecelements[1] == '4') {
											$datatype = 'int UNSIGNED  NOT NULL DEFAULT "0"';
										} else if($frmsecelements[1] == '5') {
											$datatype = 'decimal(20,'.$frmsecelements[4].') NOT NULL DEFAULT "0"';
										} else if($frmsecelements[1] == '15') {
											$datatype = 'text NULL';
										} else if($frmsecelements[1] == '16') {
											$datatype = 'text NULL';
										} else if($frmsecelements[1] == '17') {										
												if($frmsecelements[9] == "No") {
													$tabname = $tblcolumname;
													$fldname = $tblcolumname.'id';
													$tabchk = $this->tableexitcheck($database,$tabname);
													if($tabchk == 0) {
														$datatype = 'varchar(11) NOT NULL DEFAULT "1"';
													} else {
														$type = $this->tablefieldtypecheck($tabname,$fldname);
														$datatype = 'varchar(11) NOT NULL DEFAULT "1"';
													}
												} else {
													$datatype = 'varchar(250) NOT NULL DEFAULT "1"';
												}											
										} else if($frmsecelements[1] == '26') {
											$datatype = 'varchar(11) NOT NULL DEFAULT "1"';
										} else if($frmsecelements[1] == '30') {
											$datatype = 'text NULL';
										} else {
											$datatype = 'varchar(250) NOT NULL DEFAULT " "';
										}
									}
									if($frmsecelements[1] == '26'){
										$ddtabname = $tblcolumname;
										$colname = $tblcolumname.'name';
										$colid = $ddtabname.'id';
										$datas = explode('|',$frmsecelements[9]);
										$relatedmoduleid = $this->getmoduleid($datas[0]);
										$this->db->query('INSERT INTO modulerelation(moduleid,parenttable,parentfieldname,relatedmoduleid,relatedtable,relatedfieldname,status) VALUES('.$moduleid.',"'.$maintable.'","'.$colid.'",'.$relatedmoduleid.',"'.$datas[0].'","'.$datas[1].'",1)');
									}
									//drop down table generation
									if($frmsecelements[1] == '17') {
										$ddtabname = $tblcolumname;
										$colname = $tblcolumname.'name';
										$colid = $ddtabname.'id';
										$tabcheck2 = $this->tableexitcheck($database,$ddtabname);
										if($tabcheck2 == 0) {
											$this->db->query( 'CREATE TABLE '.$ddtabname.'( '.$colid.' int UNSIGNED NOT NULL AUTO_INCREMENT,`'.$colname.'` varchar(250),moduleid varchar(250),userroleid varchar(250),industryid varchar(250),branchid int UNSIGNED NOT NULL DEFAULT "1",sortorder int UNSIGNED NOT NULL,setdefault tinyint NOT NULL DEFAULT 0,createdate datetime NOT NULL,lastupdatedate datetime NOT NULL,createuserid mediumint UNSIGNED NOT NULL,lastupdateuserid mediumint UNSIGNED NOT NULL,status tinyint UNSIGNED NOT NULL,PRIMARY KEY ('.$colid.') )' );
											
											//default entry
											$this->db->query('INSERT INTO '.$ddtabname.' ('.$colid.','.$colname.',moduleid,userroleid,industryid,branchid,sortorder,setdefault,createdate,lastupdatedate,createuserid,lastupdateuserid,status) VALUES(0,"","1","1",0,0,0,0,"'.$date.'","'.$date.'",'.$uid.','.$uid.',3)');
											
											$this->db->query('INSERT INTO modulerelation(moduleid,parenttable,parentfieldname,relatedmoduleid,relatedtable,relatedfieldname,status) VALUES('.$moduleid.',"'.$maintable.'","'.$colid.'",'.$moduleid.',"'.$ddtabname.'","'.$colid.'",1)');
										}
										$datas=array();
										if($frmsecelements[3] != "" && $frmsecelements[3] != " ") {
											$datas = explode('|',$frmsecelements[3]);
											if($frmsecelements[11] == "Yes") {
												sort($datas);
											}
											$sorder = 1;
											foreach($datas as $value) {
												$def = 0;
												if($frmsecelements[4] == $value) {
													$def = '1';
												}
												$industryid = $this->Basefunctions->industryid;
												$this->db->query('INSERT INTO '.$ddtabname.' ('.$colid.','.$colname.',moduleid,userroleid,industryid,branchid,sortorder,setdefault,createdate,lastupdatedate,createuserid,lastupdateuserid,status) VALUES(0,"'.$value.'","'.$moduleid.'","'.$userroleid.'","'.$industryid.'",0,'.$sorder.',"'.$def.'","'.$date.'","'.$date.'",'.$uid.','.$uid.',1)');
												$sorder++;
											}
										}
										$tblcolumname = $colid;
										$datafieldname = $colid;
										if($frmsecelements[9] == "No") {
											$forkeycons = '';
										}
										$fkeytable = $ddtabname;
										$fkeycolm = $tblcolumname;
									}
									$gridtable = $maintable.strtolower($grptabname).'details';
									if($tabgrpvalue[0]==2) { //main table and grid table creation
										//check table is available
										$tabcheck = $this->tableexitcheck($database,$maintable);
										if($tabcheck == 0) {
											$this->db->query( 'CREATE TABLE '.$maintable.' ( '.$maintable.'id int UNSIGNED NOT NULL AUTO_INCREMENT '.$parentfield.',createdate datetime NOT NULL,lastupdatedate datetime NOT NULL,createuserid mediumint UNSIGNED NOT NULL,lastupdateuserid mediumint UNSIGNED NOT NULL,status tinyint UNSIGNED NOT NULL,PRIMARY KEY ('.$maintable.'id))' );
											//default entry
											$this->db->query('INSERT INTO '.$maintable.' VALUES(0'.$parentfieldval.',"'.$date.'","'.$date.'",'.$uid.','.$uid.',3)');
										}
										//grid table creation
										$currenttab = strtolower($grptabname);
										if($currenttab!=$previoustab) {
											$previoustab = strtolower($grptabname);
											$tabcheck1 = $this->tableexitcheck($database,$gridtable);
											if($tabcheck1 == 0) {
												$this->db->query( 'CREATE TABLE '.$gridtable.' ( '.$gridtable.'id int UNSIGNED NOT NULL AUTO_INCREMENT,'.$maintableid.'id int UNSIGNED NOT NULL,`'.$tblcolumname.'` '.$datatype.',createdate datetime NOT NULL,lastupdatedate datetime NOT NULL,createuserid mediumint UNSIGNED NOT NULL,lastupdateuserid mediumint UNSIGNED NOT NULL,status tinyint UNSIGNED NOT NULL,PRIMARY KEY ('.$gridtable.'id) '.$forkeycons.' )' );
												//default entry
												$this->db->query('INSERT INTO '.$gridtable.' VALUES(0,1,1,"'.$date.'","'.$date.'",'.$uid.','.$uid.',3)');
												$newfiled="Yes";
											} else {
												$colnamechk = $this->tablefieldnamecheck($gridtable,$tblcolumname);
												if($colnamechk == 'false') {
													$this->db->query( 'ALTER TABLE '.$gridtable.' ADD COLUMN `'.$tblcolumname.'` '.$datatype.' AFTER '.$maintableid.'id' );
													if($forkeycons!="") {
														$this->db->query( 'ALTER TABLE '.$gridtable.' ADD   FOREIGN KEY('.$fkeycolm.') REFERENCES '.$fkeytable.'('.$fkeycolm.')' );
													}
													$newfiled="Yes";
												}
											}
										} else {
											$colnamechk = $this->tablefieldnamecheck($gridtable,$tblcolumname);
											if($colnamechk == 'false') {
												$this->db->query( 'ALTER TABLE '.$gridtable.' ADD COLUMN `'.$tblcolumname.'` '.$datatype.' AFTER '.$maintableid.'id' );
												if($forkeycons!="") {
													$this->db->query( 'ALTER TABLE '.$gridtable.' ADD  FOREIGN KEY('.$fkeycolm.') REFERENCES '.$fkeytable.'('.$fkeycolm.')' );
												}
												$newfiled="Yes";
											}
										}
										$moduletabname = $gridtable;
										$resttab[] = $gridtable;
									} else {//main table and editor information creation
										//check table is available
										$tabcheck = $this->tableexitcheck($database,$maintable);
										if($tabcheck == 0) {
											if( $tabgrpvalue[0]==3 && !in_array($editorcolname,$editorfname) ) {
												array_push($editorfname,$editorcolname);
												array_push($editortabsecids,$tabsectionid);
												array_push($editortablenames,$maintable);
												if($frmsecelements[1]=='15' || $frmsecelements[1]=='16') {//if attachment component
													$this->db->query( 'CREATE TABLE '.$maintable.' ( '.$maintable.'id int UNSIGNED NOT NULL AUTO_INCREMENT '.$parentfield.',`'.$tblcolumname.'` '.$datatype.',`'.$tblcolumname.'_fromid` varchar(250) NOT NULL DEFAULT " ",`'.$tblcolumname.'_size` varchar(250) NOT NULL DEFAULT " ",`'.$tblcolumname.'_type` varchar(250) NOT NULL DEFAULT " ",`'.$tblcolumname.'_path` '.$datatype.','.$editorcolname.' varchar(250) NOT NULL DEFAULT " ",createdate datetime NOT NULL,lastupdatedate datetime NOT NULL,createuserid mediumint UNSIGNED NOT NULL,lastupdateuserid mediumint UNSIGNED NOT NULL,status tinyint UNSIGNED NOT NULL,PRIMARY KEY ('.$maintable.'id) '.$forkeycons.' )' );
													//default entry
													$this->db->query('INSERT INTO '.$maintable.' VALUES(0'.$parentfieldval.',"1","","","","","","'.$date.'","'.$date.'",'.$uid.','.$uid.',3)');
												} else { //othe component
													$this->db->query( 'CREATE TABLE '.$maintable.' ( '.$maintable.'id int UNSIGNED NOT NULL AUTO_INCREMENT '.$parentfield.',`'.$tblcolumname.'` '.$datatype.','.$editorcolname.' varchar(250) NOT NULL DEFAULT " ",createdate datetime NOT NULL,lastupdatedate datetime NOT NULL,createuserid mediumint UNSIGNED NOT NULL,lastupdateuserid mediumint UNSIGNED NOT NULL,status tinyint UNSIGNED NOT NULL,PRIMARY KEY ('.$maintable.'id) '.$forkeycons.' )' );
													//default entry
													$this->db->query('INSERT INTO '.$maintable.' VALUES(0'.$parentfieldval.',"1","","'.$date.'","'.$date.'",'.$uid.','.$uid.',3)');
												}
												$newfiled="Yes";
											} else {
												if($frmsecelements[1]=='15' || $frmsecelements[1]=='16') { //if attachment component
													$this->db->query( 'CREATE TABLE '.$maintable.' ( '.$maintable.'id int UNSIGNED NOT NULL AUTO_INCREMENT '.$parentfield.',`'.$tblcolumname.'` '.$datatype.',`'.$tblcolumname.'_fromid` varchar(250) NOT NULL DEFAULT " ",`'.$tblcolumname.'_size` varchar(250) NOT NULL DEFAULT " ",`'.$tblcolumname.'_type` varchar(250) NOT NULL DEFAULT " ",`'.$tblcolumname.'_path` '.$datatype.',createdate datetime NOT NULL,lastupdatedate datetime NOT NULL,createuserid mediumint UNSIGNED NOT NULL,lastupdateuserid mediumint UNSIGNED NOT NULL,status tinyint UNSIGNED NOT NULL,PRIMARY KEY ('.$maintable.'id)  '.$forkeycons.' )' );
													//default entry
													$this->db->query('INSERT INTO '.$maintable.' VALUES(0'.$parentfieldval.',"1","","","","","'.$date.'","'.$date.'",'.$uid.','.$uid.',3)');
												} else { //other component
													$this->db->query( 'CREATE TABLE '.$maintable.' ( '.$maintable.'id int UNSIGNED NOT NULL AUTO_INCREMENT '.$parentfield.',`'.$tblcolumname.'` '.$datatype.',createdate datetime NOT NULL,lastupdatedate datetime NOT NULL,createuserid mediumint UNSIGNED NOT NULL,lastupdateuserid mediumint UNSIGNED NOT NULL,status tinyint UNSIGNED NOT NULL,PRIMARY KEY ('.$maintable.'id)  '.$forkeycons.' )' );
													//default entry
													$this->db->query('INSERT INTO '.$maintable.' VALUES(0'.$parentfieldval.',"1","'.$date.'","'.$date.'",'.$uid.','.$uid.',3)');
												}
												$newfiled="Yes";
											}
										} else {
											if( $tabgrpvalue[0]==3 && !in_array($editorcolname,$editorfname) ) {
												array_push($editorfname,$editorcolname);
												array_push($editortabsecids,$tabsectionid);
												array_push($editortablenames,$maintable);
												$colnamechk = $this->tablefieldnamecheck($maintable,$tblcolumname);
												if($colnamechk == 'false') {
													$this->db->query( 'ALTER TABLE '.$maintable.' ADD COLUMN `'.$tblcolumname.'` '.$datatype.' AFTER '.$maintable.'id' );
												}
												if($frmsecelements[1]=='15' || $frmsecelements[1]=='16') { //attachment component
													$fieldnames = array('_size','_type','_path','_fromid');
													foreach($fieldnames as $fldname) {
														$colnamechk = $this->tablefieldnamecheck($maintable,$tblcolumname.$fldname);
														if($colnamechk == 'false') {
															if($fldname == '_path') {
															$this->db->query( 'ALTER TABLE '.$maintable.' ADD COLUMN `'.$tblcolumname.$fldname.'` '.$datatype.' AFTER '.$tblcolumname.'' );
															} else {
																$this->db->query( 'ALTER TABLE '.$maintable.' ADD COLUMN `'.$tblcolumname.$fldname.'` varchar(250) NOT NULL DEFAULT " " AFTER '.$tblcolumname.'' );
															}
														}
													}
												}
												//editor fields
												$edcolnamechk = $this->tablefieldnamecheck($maintable,$editorcolname);
												if($edcolnamechk == 'false') {
													$this->db->query( 'ALTER TABLE '.$maintable.' ADD COLUMN '.$editorcolname.' varchar(250) NOT NULL DEFAULT " "  AFTER '.$tblcolumname.' ' );
												}
												$newfiled="Yes";
											} else {
												$colnamechk = $this->tablefieldnamecheck($maintable,$tblcolumname);
												if($colnamechk == 'false') {
													$this->db->query( 'ALTER TABLE '.$maintable.' ADD COLUMN `'.$tblcolumname.'` '.$datatype.' AFTER '.$maintable.'id' );
												}
												if($frmsecelements[1]=='15' || $frmsecelements[1]=='16') { //attachment component
													$fieldnames = array('_size','_type','_path','_fromid');
													foreach($fieldnames as $fldname) {
														$colnamechk = $this->tablefieldnamecheck($maintable,$tblcolumname.$fldname);
														if($fldname == '_path') {
														$this->db->query( 'ALTER TABLE '.$maintable.' ADD COLUMN `'.$tblcolumname.$fldname.'` '.$datatype.' AFTER '.$tblcolumname.'' );
														} else {
															$this->db->query( 'ALTER TABLE '.$maintable.' ADD COLUMN `'.$tblcolumname.$fldname.'` varchar(250) NOT NULL DEFAULT " " AFTER '.$tblcolumname.'' );
														}
													}
												}
												$newfiled="Yes";
											}
											if($forkeycons!="") {
												$this->db->query( 'ALTER TABLE '.$maintable.' ADD  FOREIGN KEY('.$fkeycolm.') REFERENCES '.$fkeytable.'('.$fkeycolm.')' );
											}
										}
										$moduletabname = $maintable;
									}
									//module data define
									$decisize = '0';
									$ddmultiple = 'No';
									$maxlength = 100;
									$ddpartable = '';
									$readonly = 'No';
									$uitypeid = $frmsecelements[1];
									switch($frmsecelements[1]) {
										case 2: //text box
											$defvalue = $frmsecelements[3]; 
											$maxlength = $frmsecelements[4];
											$descp = $frmsecelements[5];
											$mandatory = $frmsecelements[6];
											$active = $frmsecelements[7];
											$readonly = $frmsecelements[8];
											$cusvalidation = '';
											$columntye = $this->gencolumntype($frmsecelements[9]);
											break;

										case 3: //text area
											$defvalue = $frmsecelements[3]; 
											$maxlength = $frmsecelements[4];
											$decisize = $frmsecelements[5];
											$descp = $frmsecelements[6];
											$mandatory = $frmsecelements[7];
											$active = $frmsecelements[8];
											$readonly = $frmsecelements[9];
											$cusvalidation = '';
											$columntye = $this->gencolumntype($frmsecelements[10]);
											break;

										case 4://integer
											$defvalue = $frmsecelements[3]; 
											$descp = $frmsecelements[4];
											$mandatory = $frmsecelements[5];
											$active = $frmsecelements[6];
											$readonly = $frmsecelements[7];
											$cusvalidation = '';
											$columntye = $this->gencolumntype($frmsecelements[8]);
											break;

										case 5://decimal
											$defvalue = $frmsecelements[3];
											$decisize = $frmsecelements[4];
											$descp = $frmsecelements[5];
											$mandatory = $frmsecelements[6];
											$active = $frmsecelements[7];
											$readonly = $frmsecelements[8];
											$cusvalidation = '';
											$columntye = $this->gencolumntype($frmsecelements[9]);
											break;

										case 6://percent
											$defvalue = $frmsecelements[3];
											$decisize = $frmsecelements[4];
											$descp = $frmsecelements[5];
											$mandatory = $frmsecelements[6];
											$active = $frmsecelements[7];
											$readonly = $frmsecelements[8];
											$cusvalidation = '';
											$columntye = $this->gencolumntype($frmsecelements[9]);
											break;

										case 7://currency
											$defvalue = $frmsecelements[3];
											$decisize = $frmsecelements[4];
											$descp = $frmsecelements[5];
											$mandatory = $frmsecelements[6];
											$active = $frmsecelements[7];
											$readonly = $frmsecelements[8];
											$cusvalidation = '';
											$columntye = $this->gencolumntype($frmsecelements[9]);
											break;

										case 8://Date
											$defvalue = $frmsecelements[3];
											$descp = $frmsecelements[4];
											$mandatory = $frmsecelements[5];
											$active = $frmsecelements[6];
											$readonly = $frmsecelements[7];
											$cusvalidation = '';
											$columntye = $this->gencolumntype($frmsecelements[8]);
											$ddpartable = $frmsecelements[9].'|'.$frmsecelements[10].'|'.$frmsecelements[11].'|'.$frmsecelements[12];
											break;

										case 9://Time
											$defvalue = $frmsecelements[3];
											$descp = $frmsecelements[4];
											$mandatory = $frmsecelements[5];
											$active = $frmsecelements[6];
											$readonly = $frmsecelements[7];
											$cusvalidation = '';
											$columntye = $this->gencolumntype($frmsecelements[8]);
											break;

										case 10://Email
											$defvalue = $frmsecelements[3];
											$descp = $frmsecelements[4];
											$mandatory = $frmsecelements[5];
											$active = $frmsecelements[6];
											$readonly = $frmsecelements[7];
											$cusvalidation = '';
											$columntye = $this->gencolumntype($frmsecelements[8]);
											break;

										case 11: //phone
											$defvalue = $frmsecelements[3];
											$descp = $frmsecelements[4];
											$mandatory = $frmsecelements[5];
											$active = $frmsecelements[6];
											$readonly = $frmsecelements[7];
											$cusvalidation = '';
											$columntye = $this->gencolumntype($frmsecelements[8]);
											break;

										case 12: //URL
											$defvalue = $frmsecelements[3];
											$descp = $frmsecelements[4];
											$mandatory = $frmsecelements[5];
											$active = $frmsecelements[6];
											$readonly = $frmsecelements[7];
											$cusvalidation = '';
											$columntye = $this->gencolumntype($frmsecelements[8]);
											break;

										case 13: //check box											
											$ddpartable = $frmsecelements[3].'|'.$frmsecelements[4].'|'.$frmsecelements[5];
											$descp = $frmsecelements[6];
											$mandatory = $frmsecelements[7];
											$active = $frmsecelements[8];
											$readonly = $frmsecelements[9];
											$defvalue = $frmsecelements[10];
											$cusvalidation = '';
											$columntye = $this->gencolumntype($frmsecelements[11]);
											break;

										case 14: //Auto Number
											$defvalue = '';
											$prefix = $frmsecelements[3];
											$autonum = $frmsecelements[4];
											$suffix = $frmsecelements[5];
											$atuonumtype = $frmsecelements[6];
											$descp = $frmsecelements[7];
											$mandatory = $frmsecelements[8];
											$active = $frmsecelements[9];
											$readonly = $frmsecelements[10];
											$cusvalidation = '';
											$columntye = $this->gencolumntype($frmsecelements[11]);
											break;

										case 15: //File Upload
											$defvalue = '';
											$maxlength = $frmsecelements[3];
											$descp = $frmsecelements[4];
											$ddmultiple = $frmsecelements[5];
											$mandatory = $frmsecelements[6];
											$active = $frmsecelements[7];
											$readonly = 'No';
											$cusvalidation = '';
											$columntye = $this->gencolumntype($frmsecelements[8]);
											break;

										case 16: //Image
											$defvalue = '';
											$maxlength = $frmsecelements[3];
											$cusvalidation = $frmsecelements[4];
											$descp = $frmsecelements[5];
											$mandatory = $frmsecelements[6];
											$active = $frmsecelements[7];
											$readonly = 'No';
											$columntye = $this->gencolumntype($frmsecelements[8]);
											break;

										case 17://picklist
											$uitypeid = $this->checkdropdowntabletype($database,$tblcolumname);
											$defvalue = $frmsecelements[4]; 
											$descp = $frmsecelements[5];
											$mandatory = $frmsecelements[6];
											$active = $frmsecelements[7];
											$readonly = $frmsecelements[8];
											$ddmultiple = $frmsecelements[9];
											$cusvalidation = '';
											$columntye = $this->gencolumntype($frmsecelements[10]);
											break;

										case 22: //password
											$defvalue = $frmsecelements[3]; 
											$descp = $frmsecelements[4];
											$mandatory = $frmsecelements[5];
											$active = $frmsecelements[6];
											$readonly = $frmsecelements[7];
											$cusvalidation = '';
											$columntye = $this->gencolumntype($frmsecelements[8]);
											break;

										case 24: //Text Editor
											$defvalue = '';
											$descp = $frmsecelements[3];
											$mandatory = $frmsecelements[4];
											$active = $frmsecelements[5];
											$readonly = $frmsecelements[6];
											$cusvalidation = '';
											$columntye = $this->gencolumntype($frmsecelements[7]);
											break;
											
										case 26: //Relation
											$uitypeid = 26;
											$defvalue = $frmsecelements[3].'|'.$frmsecelements[4].'|'.$frmsecelements[5].'|'.$frmsecelements[6].'|'.$frmsecelements[7];
											$condname = $frmsecelements[6];
											$condvalue = $frmsecelements[7];
											$condinfo = $frmsecelements[9].'|'.$condname.'|'.$condvalue;
											$ddpartable = $condinfo;
											$descp = $frmsecelements[8];
											$mandatory = $frmsecelements[10];
											$active = $frmsecelements[11];
											$readonly = $frmsecelements[12];
											$ddmultiple = $frmsecelements[13];
											$cusvalidation = '';
											$columntye = $this->gencolumntype($frmsecelements[14]);
											break;
											
										case 30: //Tags
											$defvalue = str_replace("|",",",$frmsecelements[3]);
											$descp = $frmsecelements[4];
											$mandatory = $frmsecelements[5];
											$active = $frmsecelements[6];
											$cusvalidation = '';
											$columntye = $this->gencolumntype($frmsecelements[7]);
											break;
										
										default :
											break;
									}
									//insert into modulefield table
									$fildscolumn=array(
										'moduletabid'=>$moduleid,
										'columnname'=>$tblcolumname,
										'tablename'=>$moduletabname,
										'uitypeid'=>$uitypeid,
										'moduletabsectionid'=>$tabsectionid,
										'fieldname'=>$datafieldname,
										'fieldlabel'=>ucwords($frmsecelements[2]),
										'ddparenttable'=>$ddpartable,
										'parenttable'=>$parenttable,
										'readonly'=>$readonly,
										'defaultvalue'=>$defvalue,
										'maximumlength'=>$maxlength,
										'decimalsize'=>$decisize,
										'mandatory'=>$mandatory,
										'active'=>$active,
										'sequence'=>$sequmber,
										'multiple'=>$ddmultiple,
										'fieldcolumntype'=>$columntye,
										'fieldcustomvalidation'=>$cusvalidation,
										'description'=>$descp,
										'userroleid'=>$userroleid
									);
									$newdata = array_merge($fildscolumn,$defdataarr);
									$this->db->insert('modulefield',$newdata);
									$modulefieldid = $this->db->insert_id();
									//insert to userrole modulefield table
									$userfieldcolm=array(
											'userroleid'=>$userroleid,
											'moduleid'=>$moduleid,
											'modulefieldid'=>$modulefieldid,
											'fieldactive'=>$active,
											'fieldreadonly'=>$readonly
									);
									$urolenewdata = array_merge($userfieldcolm,$defdataarr);
									$this->db->insert('userrolemodulefiled',$urolenewdata);
									
									//view join table data mapping
									if($frmsecelements[1] == '17') {
										$tblcolumname = $colname;
										$datatable = $ddtabname;
										$jointable = $ddtabname;
										$partable = (($tabgrpvalue[0]==2)?$gridtable:$maintable);
									} else if($frmsecelements[1] == '26') {
										$count = 0;
										$partable = $maintable;
										$datasets = explode('|',$frmsecelements[9]);
										if($datasets[0]!="") {
											$viewtype = 3;
											$reluitypeid = $datasets[3];
											if($reluitypeid == 17 || $reluitypeid == 18 || $reluitypeid == 19 || $reluitypeid == 20 || $reluitypeid == 25 || $reluitypeid == 26 || $reluitypeid == 28 || $reluitypeid == 29) {
												$jointable = substr($datasets[1],0,-2);
												$fieldname = $jointable.'name';
												$chekjointab = $this->checkviewcolmodeljointable($viewmoduleid,$jointable);
												$count = (($chekjointab!='')?$chekjointab:++$count);
												$colmodelname = $tblcolumname;
												$colmodelaliasname = $fieldname.' as '.$tblcolumname;
												$colmodelindexname = $fieldname;
												$colmodeltable = $jointable.' as '.$jointable.$count;
												$colmodeljointab = $jointable.$count;
												$colmodeljoincondname = $jointable.'id';
												$colmodelcondname = $tblcolumname;
											} else {
												$chekjointab = $this->checkviewcolmodeljointable($viewmoduleid,$datasets[2]);
												$count = (($chekjointab!='')?$chekjointab:++$count);
												$fieldname = $datasets[1];
												$colmodelname = $tblcolumname;
												$colmodelaliasname = $fieldname.' as '.$tblcolumname;
												$colmodelindexname = $fieldname;
												$colmodeltable = $datasets[2].' as '.$datasets[2].$count;
												$colmodeljointab = $datasets[2].$count;
												$colmodeljoincondname = $datasets[2].'id';
												$colmodelcondname = $tblcolumname;
											}
										} else {
											$jointable = (($customtab == 'Yes')?$parenttable:$maintable);
											$datatable = (($tabgrpvalue[0]==2)? $gridtable:$maintable);
											$colmodelname = $tblcolumname;
											$colmodelaliasname = $tblcolumname;
											$colmodelindexname = $tblcolumname;
											$colmodeltable = $datatable;
											$colmodeljointab = $datatable;
											$colmodeljoincondname = $jointable.'id';
											$colmodelcondname = "";
											$viewtype = 1;
										}
									} else {
										$jointable = (($customtab == 'Yes')?$parenttable:$maintable);
										if($tabgrpvalue[0]==2) {
											$datatable = $gridtable;
											$partable = $maintable;
										} else {
											$datatable = $maintable;
											$partable = $maintable;
										}
									}
									//auto number table insertion
									if($frmsecelements[1] == '14') {
										$angdata = array(
											'moduleid'=>$moduleid,
											'modulefieldid'=>$modulefieldid,
											'suffix'=>$suffix,
											'prefix'=>$prefix,
											'startingnumber'=>$autonum,
											'random'=>$atuonumtype,
											'incrementby'=>0,
											'nextnumber'=>0
										);
										$newangdata = array_merge($angdata,$defdataarr);
										$this->db->insert('serialnumbermaster',$newangdata);
									}
									//view creation column data insertion
									$uitypeids=array(22,24);
									if($uitypeid==26) {
										$viewrelcolumn=array(
												'viewcreationmoduleid'=>$viewmoduleid,
												'uitypeid'=>$uitypeid,
												'moduletabsectionid'=>$tabsectionid,
												'viewcreationcolmodelname'=>$colmodelname,
												'viewcreationcolmodelaliasname'=>$colmodelaliasname,
												'viewcreationcolmodelindexname'=>$colmodelindexname,
												'viewcreationcolmodeltable'=>$colmodeltable,
												'viewcreationcolmodeljointable'=>$colmodeljointab,
												'viewcreationcolumnname'=>ucwords($frmsecelements[2]),
												'viewcreationparenttable'=>$partable,
												'viewcreationjoincolmodelname'=>$colmodeljoincondname,
												'viewcreationcolumnviewtype'=>1,
												'viewcreationtype'=>$viewtype,
												'viewcreationcolmodelcondname'=>$colmodelcondname,
												'viewcreationcolmodelcondvalue'=>'',
												'status'=>1);
										$viewrelcolumnmerge = array_merge($viewrelcolumn,$defdataarr);
										$this->db->insert('viewcreationcolumns',$viewrelcolumnmerge);
										if($tabgrpvalue[0]!=2) {
											$viewcolid[] = $this->db->insert_id();
										}
									} else if(!in_array($uitypeid,$uitypeids)) {
										$viewcolumn=array(
												'viewcreationmoduleid'=>$viewmoduleid,
												'uitypeid'=>$uitypeid,
												'moduletabsectionid'=>$tabsectionid,
												'viewcreationcolmodelname'=>$tblcolumname,
												'viewcreationcolmodelaliasname'=>$tblcolumname,
												'viewcreationcolmodelindexname'=>$tblcolumname,
												'viewcreationcolmodeltable'=>$datatable,
												'viewcreationcolmodeljointable'=>$datatable,
												'viewcreationcolumnname'=>ucwords($frmsecelements[2]),
												'viewcreationparenttable'=>$partable,
												'viewcreationjoincolmodelname'=>$jointable.'id',
												'viewcreationcolumnviewtype'=>1,
												'viewcreationtype'=>1,
												'viewcreationcolmodelcondname'=>'',
												'viewcreationcolmodelcondvalue'=>'',
												'status'=>1);
										$viewcolumnmerge = array_merge($viewcolumn,$defdataarr);
										$this->db->insert('viewcreationcolumns',$viewcolumnmerge);
										if($tabgrpvalue[0]!=2) {
											$viewcolid[] = $this->db->insert_id();
										}
									}
									{
										//supporting fields for attachment components
										$fieldnames = array('_size','_type','_path','_fromid');
										$fieldlabel = array('Size','Type','Path','Upload File From');
										if($uitypeid=='15' || $uitypeid=='16') {
											$i=0;
											$date = date($this->Basefunctions->datef);
											$userid = $this->Basefunctions->userid;
											foreach($fieldnames as $fldname) {
												$sequmber++;
												$fieldscolumn=array(
														'moduletabid'=>$moduleid,
														'columnname'=>$tblcolumname.$fldname,
														'tablename'=>$moduletabname,
														'uitypeid'=>2,
														'moduletabsectionid'=>$tabsectionid,
														'fieldname'=>$datafieldname.$fldname,
														'fieldlabel'=>ucwords($fieldlabel[$i]),
														'ddparenttable'=>$ddpartable,
														'parenttable'=>$parenttable,
														'readonly'=>'Yes',
														'defaultvalue'=>'',
														'maximumlength'=>1000,
														'decimalsize'=>'',
														'mandatory'=>'No',
														'active'=>'No',
														'sequence'=>$sequmber,
														'multiple'=>'No',
														'fieldcolumntype'=>1,
														'description'=>$descp,
														'userroleid'=>$userroleid,
														'createuserid'=>$userid,
														'lastupdateuserid'=>$userid,
														'createdate'=>$date,
														'lastupdatedate'=>$date,
														'status'=>10
												);
												$this->db->insert('modulefield',$fieldscolumn);
												$modfieldid = $this->db->insert_id();
												//insert to userrole modulefield table
												$userfieldcolm=array(
														'userroleid'=>$userroleid,
														'moduleid'=>$moduleid,
														'modulefieldid'=>$modfieldid,
														'fieldactive'=>'No',
														'fieldreadonly'=>'Yes'
												);
												$urolenewdata = array_merge($userfieldcolm,$defdataarr);
												$this->db->insert('userrolemodulefiled',$urolenewdata);
												//view creation column insertion
												$viewcolumn=array(
														'viewcreationmoduleid'=>$viewmoduleid,
														'uitypeid'=>2,
														'moduletabsectionid'=>$tabsectionid,
														'viewcreationcolmodelname'=>$tblcolumname.$fldname,
														'viewcreationcolmodelaliasname'=>$tblcolumname.$fldname,
														'viewcreationcolmodelindexname'=>$tblcolumname.$fldname,
														'viewcreationcolmodeltable'=>$datatable,
														'viewcreationcolmodeljointable'=>$datatable,
														'viewcreationcolumnname'=>ucwords($fieldlabel[$i]),
														'viewcreationparenttable'=>$partable,
														'viewcreationjoincolmodelname'=>$jointable.'id',
														'viewcreationcolumnviewtype'=>1,
														'viewcreationtype'=>1,
														'viewcreationcolmodelcondname'=>'',
														'viewcreationcolmodelcondvalue'=>'',
														'status'=>1
												);
												$viewcolumnmerge = array_merge($viewcolumn,$defdataarr);
												$this->db->insert('viewcreationcolumns',$viewcolumnmerge);
												if($tabgrpvalue[0]!=2) {
													$viewcolid[] = $this->db->insert_id();
												}
												$i++;
											}
										}
									}
									$sequmber++;
								}
								else { //existing field update
									$modulefieldid = $fieldchkid;
									$this->modulefieldmanipulation($frmsecelements,$modulefieldid,$moduleid,$tabsectionid,$sequmber);
									$sequmber++;
								}
							}
						}
					}
				}
			}
			//update editor file info
			$ii=0;
			foreach($editorfname as $colname) {
				$seqid = $this->Basefunctions->maxidwithcond('modulefield','sequence','moduletabsectionid',$editortabsecids[$ii]);
				$seqnum = ++$seqid;
				$colnamechk = $this->checkcolumnexistinmodulefield($colname,$moduleid);
				if($colnamechk == 'false') {
					$fildscolumn=array(
						'moduletabid'=>$moduleid,
						'columnname'=>$colname,
						'tablename'=>$editortablenames[$ii],
						'uitypeid'=>3,
						'fieldname'=>$colname,
						'fieldlabel'=>ucwords('File Name'),
						'defaultvalue'=>'',
						'maximumlength'=>250,
						'decimalsize'=>0,
						'mandatory'=>'No',
						'active'=>'No',
						'readonly'=>'No',
						'sequence'=>$seqnum,
						'multiple'=>'No',
						'parenttable'=>$parenttable,
						'ddparenttable'=>'',
						'fieldcolumntype'=>1,
						'description'=>'',
						'moduletabsectionid'=>$editortabsecids[$ii],
						'userroleid'=>$userroleid,
					);
					$newdata = array_merge($fildscolumn,$defdataarr);
					$newdata['status'] = 10;
					$this->db->insert('modulefield',$newdata);
					$editormodulefieldid = $this->db->insert_id();
					//insert userrole field mode
					$editorfildsmode = array(
						'userroleid'=>$this->Basefunctions->userroleid,
						'moduleid'=>$moduleid,
						'modulefieldid'=>$editormodulefieldid,
						'fieldactive'=>'No',
						'fieldreadonly'=>'Yes'
					);
					$editornewdmodata = array_merge($editorfildsmode,$defdataarr);
					$this->db->insert('userrolemodulefiled',$editornewdmodata);
				} else {
					$editorcolname=array(
						'moduletabid'=>$moduleid,
						'columnname'=>$colname,
						'tablename'=>$editortablenames[$ii],
						'uitypeid'=>3,
						'fieldname'=>$colname,
						'fieldlabel'=>ucwords('File Name'),
						'defaultvalue'=>'',
						'maximumlength'=>250,
						'decimalsize'=>0,
						'mandatory'=>'No',
						'active'=>'No',
						'readonly'=>'No',
						'sequence'=>$seqnum,
						'multiple'=>'No',
						'parenttable'=>$parenttable,
						'ddparenttable'=>'',
						'fieldcolumntype'=>1,
						'description'=>'',
						'moduletabsectionid'=>$editortabsecids[$ii],
						'userroleid'=>$userroleid,
						'status'=>'1'
					);
					$updefdataarr = $this->updatedefaultvalueget();
					$editorcolnameinfo = array_merge($editorcolname,$updefdataarr);
					$this->db->where('moduletabid',$moduleid);
					$this->db->where('columnname',$colname);
					$this->db->where('tablename',$editortablenames[$ii]);
					$this->db->update('modulefield',$editorcolnameinfo);
				}
				$ii++;
			}
			$updefdataarr = $this->updatedefaultvalueget();
			$delcolumn = array('status'=>'0');
			$deldata = array_merge($delcolumn,$updefdataarr);
			//delete user deleted section form elements
			$delfields = explode(',',$delfieldid);
			foreach($delfields as $id) {
				if($id!=0){
					$this->db->where('modulefieldid',$id);
					$this->db->update('modulefield',$deldata);
					//delete view creation field
					{//fetch field information
						$datasets = $this->modulefieldinfodata($id);
						if( count($datasets) > 0 ) { //delete view creation field
							$this->db->query(' UPDATE viewcreationcolumns SET status="0" where viewcreationcolumnname = "'.$datasets["label"].'" AND uitypeid="'.$datasets["uitype"].'" AND moduletabsectionid = '.$datasets["modtabsec"].' AND viewcreationmoduleid="'.$moduleid.'" ');
						}
					}
				}
			}
			//delete user deleted section
			$delsections = explode(',',$delsecid);
			foreach($delsections as $sid) {
				if($sid!=0) {
					$this->db->where('moduletabsectionid',$sid);
					$this->db->update('moduletabsection',$deldata);
					//delete module fields
					$this->db->where('moduletabsectionid',$sid);
					$this->db->update('modulefield',$deldata);
					//delete view fileds also
					$this->db->where('moduletabsectionid',$sid);
					$this->db->update('viewcreationcolumns',$delcolumn);
				}
			}
			//delete user deleted tab group
			$deltabgrps = explode(',',$deltabid);
			foreach($deltabgrps as $tid) {
				if($tid!=0){
					//delete tab group
					$this->db->where('moduletabgroupid',$tid);
					$this->db->update('moduletabgroup',$deldata);
					//delete tab section
					$this->db->where('moduletabgroupid',$tid);
					$this->db->update('moduletabsection',$deldata);
					//fetch section ids
					$tabsec = $this->fetchtabsectionbasedongrp($tid);
					$secid = implode(',',$tabsec);
					//delete module fields
					$this->db->where('moduletabsectionid like','%'+$secid+'%');
					$this->db->update('modulefield',$deldata);
					//delete view fileds also
					$this->db->where_in('moduletabsectionid',array($secid));
					$this->db->update('viewcreationcolumns',$delcolumn);
				}
			}
			//update user toolbarprivilege group
			$this->db->query('update usertoolbarprivilege set toolbarnameid ="'.$modactionids.'" where usertoolbarprivilege.moduleid='.$moduleid.'');
			echo "Success";
		} else {
			echo "Fail";
		}
	}
	//check the if the column exist in module field
	public function checkcolumnexistinmodulefield($colname,$moduleid) {
		$dataid = 'false';
		$this->db->select('modulefieldid');
		$this->db->from('modulefield');
		$this->db->where('moduletabid',$moduleid);
		$this->db->where('columnname',$colname);
		$result = $this->db->get();
		foreach($result->result() as $data) {
			$dataid = $data->modulefieldid;
		}
		return $dataid;
	}
	//count view creation join table name
	public function checkviewcolmodeljointable($viewmodid,$tabname) {
		$result = $this->db->select('viewcreationcolumnid')->from('viewcreationcolumns')->where('viewcreationcolumns.viewcreationmoduleid',$viewmodid)->where('viewcreationcolumns.viewcreationcolmodeljointable',$tabname)->get();
		$count = $result->num_rows();
		return $count;
	}
	//fetch modulefieldid
	public function fetchmodulefieldid($moduleid,$tblcolumname,$moduletabname) {
		$dataid = 0;
		$this->db->select('modulefieldid');
		$this->db->from('modulefield');
		$this->db->where('moduletabid',$moduleid);
		$this->db->where('columnname',$tblcolumname);
		$this->db->where('tablename',$moduletabname);
		$result = $this->db->get();
		foreach($result->result() as $data) {
			$dataid = $data->modulefieldid;
		}
		return $dataid;
	}
	//fetch view creation column id
	public function fetchviewcreationcolmdid($moduleid,$tblcolumname,$moduletabname) {
		$dataid = 0;
		$this->db->select('viewcreationcolumnid');
		$this->db->from('viewcreationcolumns');
		$this->db->where('viewcreationmoduleid',$moduleid);
		$this->db->where('viewcreationcolmodelname',$tblcolumname);
		$this->db->where('viewcreationcolmodeltable',$moduletabname);
		$result = $this->db->get();
		foreach($result->result() as $data) {
			$dataid = $data->viewcreationcolumnid;
		}
		return $dataid;
	}
	
	//fetch tab group sections
	public function fetchtabsectionbasedongrp($tid) {
		$i=0;
		$dataset = array();
		$result=$this->db->query('select moduletabsectionid from moduletabsection JOIN moduletabgroup ON moduletabgroup.moduletabgroupid=moduletabsection.moduletabgroupid WHERE moduletabsection.moduletabgroupid='.$tid.' ')->result();
		foreach($result as $data) {
			$dataset[$i]=$data->moduletabsectionid;
			$i++;
		}
		return $dataset;
	}
	//drop down value check
	public function dropdownvaluecheck($tablename,$fieldname,$fieldid,$fieldvalue,$moduleid) {
		$dataset = 0;
		$result=$this->db->query('select count('.$fieldid.') AS opt from '.$tablename.' WHERE '.$fieldname.'="'.$fieldvalue.'" AND '.$tablename.'.status!=3 AND FIND_IN_SET(moduleid,'.$moduleid.')>0')->result();
		foreach($result as $data) {
			$dataset=$data->opt;
		}
		return $dataset;
	}
	//module field information fetch
	public function modulefieldinfodata($modulefieldid) {
		$dataset = array();
		$result=$this->db->query('select fieldlabel,uitypeid,moduletabsectionid from modulefield where modulefield.modulefieldid="'.$modulefieldid.'" ')->result();
		foreach($result as $data) {
			$dataset = array('label'=>$data->fieldlabel,'uitype'=>$data->uitypeid,'modtabsec'=>$data->moduletabsectionid);
		}
		return $dataset;
	}
	//module existing filed data update
	public function modulefieldmanipulation($frmsecelements = array(),$modulefieldid=null,$moduleid=null,$tabsectionid=null,$seqnum=null) {
		//module data define
		$database = $this->db->database;
		$uid = $this->Basefunctions->userid;
		$date = date($this->Basefunctions->datef);
		$userroleid = $this->Basefunctions->userroleid;
		$decisize = '0';
		$ddmultiple = 'No';
		$maxlength = 100;
		$ddpartable = '';
		$unique = 'No';
		$readonly = 'No';
		$uitypeid = $frmsecelements[1];
		$modfieldid = $modulefieldid;
		switch($frmsecelements[1]) {
			case 2: //text box
				$defvalue = $frmsecelements[3]; 
				$maxlength = $frmsecelements[4];
				$descp = $frmsecelements[5];
				$mandatory = $frmsecelements[6];
				$active = $frmsecelements[7];
				$readonly = $frmsecelements[8];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[10]);
				$columntye = $this->gencolumntype($frmsecelements[9]);
				$modfieldid = $frmsecelements[10];
				break;

			case 3: //text area
				$defvalue = $frmsecelements[3]; 
				$maxlength = $frmsecelements[4];
				$decisize = $frmsecelements[5];
				$descp = $frmsecelements[6];
				$mandatory = $frmsecelements[7];
				$active = $frmsecelements[8];
				$readonly = $frmsecelements[9];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[11]);
				$columntye = $this->gencolumntype($frmsecelements[10]);
				$modfieldid = $frmsecelements[11];
				break;

			case 4://integer
				$defvalue = $frmsecelements[3]; 
				$descp = $frmsecelements[4];
				$mandatory = $frmsecelements[5];
				$active = $frmsecelements[6];
				$readonly = $frmsecelements[7];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[9]);
				$columntye = $this->gencolumntype($frmsecelements[8]);
				$modfieldid = $frmsecelements[9];
				break;

			case 5://decimal
				$defvalue = $frmsecelements[3];
				$decisize = $frmsecelements[4];
				$descp = $frmsecelements[5];
				$mandatory = $frmsecelements[6];
				$active = $frmsecelements[7];
				$readonly = $frmsecelements[8];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[10]);
				$columntye = $this->gencolumntype($frmsecelements[9]);
				$modfieldid = $frmsecelements[10];
				break;

			case 6://percent
				$defvalue = $frmsecelements[3];
				$decisize = $frmsecelements[4];
				$descp = $frmsecelements[5];
				$mandatory = $frmsecelements[6];
				$active = $frmsecelements[7];
				$readonly = $frmsecelements[8];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[10]);
				$columntye = $this->gencolumntype($frmsecelements[9]);
				$modfieldid = $frmsecelements[10];
				break;

			case 7://currency
				$defvalue = $frmsecelements[3];
				$decisize = $frmsecelements[4];
				$descp = $frmsecelements[5];
				$mandatory = $frmsecelements[6];
				$active = $frmsecelements[7];
				$readonly = $frmsecelements[8];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[10]);
				$columntye = $this->gencolumntype($frmsecelements[9]);
				$modfieldid = $frmsecelements[10];
				break;

			case 8://Date
				$defvalue = $frmsecelements[3];
				$descp = $frmsecelements[4];
				$mandatory = $frmsecelements[5];
				$active = $frmsecelements[6];
				$readonly = $frmsecelements[7];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[13]);
				$columntye = $this->gencolumntype($frmsecelements[8]);
				$ddpartable = $frmsecelements[9].'|'.$frmsecelements[10].'|'.$frmsecelements[11].'|'.$frmsecelements[12];
				$modfieldid = $frmsecelements[13];
				break;

			case 9://Time
				$defvalue = $frmsecelements[3];
				$descp = $frmsecelements[4];
				$mandatory = $frmsecelements[5];
				$active = $frmsecelements[6];
				$readonly = $frmsecelements[7];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[9]);
				$columntye = $this->gencolumntype($frmsecelements[8]);
				$modfieldid = $frmsecelements[9];
				break;

			case 10://Email
				$defvalue = $frmsecelements[3];
				$descp = $frmsecelements[4];
				$mandatory = $frmsecelements[5];
				$active = $frmsecelements[6];
				$readonly = $frmsecelements[7];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[10]);
				$columntye = $this->gencolumntype($frmsecelements[8]);
				$unique = $frmsecelements[9];
				$modfieldid = $frmsecelements[10];
				break;

			case 11: //phone
				$defvalue = $frmsecelements[3];
				$descp = $frmsecelements[4];
				$mandatory = $frmsecelements[5];
				$active = $frmsecelements[6];
				$readonly = $frmsecelements[7];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[10]);
				$columntye = $this->gencolumntype($frmsecelements[8]);
				$unique = $frmsecelements[9];
				$modfieldid = $frmsecelements[10];
				break;

			case 12: //URL
				$defvalue = $frmsecelements[3];
				$descp = $frmsecelements[4];
				$mandatory = $frmsecelements[5];
				$active = $frmsecelements[6];
				$readonly = $frmsecelements[7];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[9]);
				$columntye = $this->gencolumntype($frmsecelements[8]);
				$modfieldid = $frmsecelements[9];
				break;

			case 13: //check box
				$ddpartable = $frmsecelements[3].'|'.$frmsecelements[4].'|'.$frmsecelements[5];
				$descp = $frmsecelements[6];
				$mandatory = $frmsecelements[7];
				$active = $frmsecelements[8];
				$readonly = $frmsecelements[9];
				$defvalue = $frmsecelements[10];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[12]);
				$columntye = $this->gencolumntype($frmsecelements[11]);
				$modfieldid = $frmsecelements[12];
				break;

			case 14: //Auto Number
				$defvalue = '';
				$prefix = $frmsecelements[3];
				$autonum = $frmsecelements[4];
				$suffix = $frmsecelements[5];
				$atuonumtype = $frmsecelements[6];
				$descp = $frmsecelements[7];
				$mandatory = $frmsecelements[8];
				$active = $frmsecelements[9];
				$readonly = $frmsecelements[10];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[12]);
				$columntye = $this->gencolumntype($frmsecelements[11]);
				$modfieldid = $frmsecelements[12];
				break;

			case 15: //File Upload
				$defvalue = '';
				$maxlength = $frmsecelements[3];
				$descp = $frmsecelements[4];
				$ddmultiple = $frmsecelements[5];
				$mandatory = $frmsecelements[6];
				$active = $frmsecelements[7];
				$readonly = 'No';
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[9]);
				$columntye = $this->gencolumntype($frmsecelements[8]);
				$modfieldid = $frmsecelements[9];
				break;
				
			case 16: //Image
				$defvalue = '';
				$maxlength = $frmsecelements[3];
				$cusvalidation = $frmsecelements[4];
				$descp = $frmsecelements[5];
				$mandatory = $frmsecelements[6];
				$active = $frmsecelements[7];
				$readonly = 'No';
				$columntye = $this->gencolumntype($frmsecelements[8]);
				$modfieldid = $frmsecelements[9];
				break;

			case 17://picklist
				$defvalue = $frmsecelements[4]; 
				$descp = $frmsecelements[5];
				$mandatory = $frmsecelements[6];
				$active = $frmsecelements[7];
				$readonly = $frmsecelements[8];
				$ddmultiple = $frmsecelements[9];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[12]);
				$columntye = $this->gencolumntype($frmsecelements[10]);
				$modfieldid = $frmsecelements[12];
				break;

			case 18://picklist dependency
				$descp = $frmsecelements[5];
				$mandatory = $frmsecelements[6];
				$active = $frmsecelements[7];
				$readonly = $frmsecelements[8];
				$ddmultiple = $frmsecelements[9];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[12]);
				$columntye = $this->gencolumntype($frmsecelements[10]);
				$defvalue = $this->modulefieldinformationfetch('modulefield','defaultvalue',$frmsecelements[12]);
				$ddpartable = $this->modulefieldinformationfetch('modulefield','ddparenttable',$frmsecelements[12]);
				$modfieldid = $frmsecelements[12];
				break;
				
			case 19://drop down for parent table
				$descp = $frmsecelements[5];
				$mandatory = $frmsecelements[6];
				$active = $frmsecelements[7];
				$readonly = $frmsecelements[8];
				$ddmultiple = $frmsecelements[9];
				$columntye = $this->gencolumntype($frmsecelements[10]);
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[12]);
				$defvalue = $this->modulefieldinformationfetch('modulefield','defaultvalue',$frmsecelements[12]);
				$ddpartable = $this->modulefieldinformationfetch('modulefield','ddparenttable',$frmsecelements[12]);
				$modfieldid = $frmsecelements[12];
				break;

			case 20://drop down for parent table
				$descp = $frmsecelements[5];
				$mandatory = $frmsecelements[6];
				$active = $frmsecelements[7];
				$readonly = $frmsecelements[8];
				$ddmultiple = $frmsecelements[9];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[12]);
				$columntye = $this->gencolumntype($frmsecelements[10]);
				$defvalue = $this->modulefieldinformationfetch('modulefield','defaultvalue',$frmsecelements[12]);
				$ddpartable = $this->modulefieldinformationfetch('modulefield','ddparenttable',$frmsecelements[12]);
				$modfieldid = $frmsecelements[12];
				break;

			case 21://Tree
				$descp = $frmsecelements[5];
				$mandatory = $frmsecelements[6];
				$active = $frmsecelements[7];
				$readonly = $frmsecelements[8];
				$ddmultiple = $frmsecelements[9];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[12]);
				$columntye = $this->gencolumntype($frmsecelements[10]);
				$defvalue = $this->modulefieldinformationfetch('modulefield','defaultvalue',$frmsecelements[12]);
				$ddpartable = $this->modulefieldinformationfetch('modulefield','ddparenttable',$frmsecelements[12]);
				$modfieldid = $frmsecelements[12];
				break;

			case 22: //password
				$defvalue = $frmsecelements[3]; 
				$descp = $frmsecelements[4];
				$mandatory = $frmsecelements[5];
				$active = $frmsecelements[6];
				$readonly = $frmsecelements[7];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[9]);
				$columntye = $this->gencolumntype($frmsecelements[8]);
				$modfieldid = $frmsecelements[9];
				break;

			case 23://attribute set view create
				$descp = $frmsecelements[5];
				$mandatory = $frmsecelements[6];
				$active = $frmsecelements[7];
				$readonly = $frmsecelements[8];
				$ddmultiple = $frmsecelements[9];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[12]);
				$columntye = $this->gencolumntype($frmsecelements[10]);
				$defvalue = $this->modulefieldinformationfetch('modulefield','defaultvalue',$frmsecelements[12]);
				$ddpartable = $this->modulefieldinformationfetch('modulefield','ddparenttable',$frmsecelements[12]);
				$modfieldid = $frmsecelements[12];
				break;

			case 24: //Text Editor
				$defvalue = '';
				$descp = $frmsecelements[3];
				$mandatory = $frmsecelements[4];
				$active = $frmsecelements[5];
				$readonly = $frmsecelements[6];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[8]);
				$columntye = $this->gencolumntype($frmsecelements[7]);
				$modfieldid = $frmsecelements[8];
				break;

			case 25://same drop down reputation
				$descp = $frmsecelements[5];
				$mandatory = $frmsecelements[6];
				$active = $frmsecelements[7];
				$readonly = $frmsecelements[8];
				$ddmultiple = $frmsecelements[9];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[12]);
				$columntye = $this->gencolumntype($frmsecelements[10]);
				$defvalue = $this->modulefieldinformationfetch('modulefield','defaultvalue',$frmsecelements[12]);
				$ddpartable = $this->modulefieldinformationfetch('modulefield','ddparenttable',$frmsecelements[12]);
				$modfieldid = $frmsecelements[12];
				break;

			case 26: //Relation
				$uitypeid = 26;
				$defvalue = $frmsecelements[3].'|'.$frmsecelements[4].'|'.$frmsecelements[5].'|'.$frmsecelements[6].'|'.$frmsecelements[7];
				$condname = $frmsecelements[6];
				$condvalue = $frmsecelements[7];
				$condinfo = $frmsecelements[9].'|'.$condname.'|'.$condvalue;
				$ddpartable = $condinfo;
				$descp = $frmsecelements[8];
				$mandatory = $frmsecelements[10];
				$active = $frmsecelements[11];
				$readonly = $frmsecelements[12];
				$ddmultiple = $frmsecelements[13];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[15]);
				$columntye = $this->gencolumntype($frmsecelements[14]);
				$modfieldid = $frmsecelements[15];
				break;

			case 27://Module drop down value fetch
				$descp = $frmsecelements[5];
				$mandatory = $frmsecelements[6];
				$active = $frmsecelements[7];
				$readonly = $frmsecelements[8];
				$ddmultiple = $frmsecelements[9];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[12]);
				$columntye = $this->gencolumntype($frmsecelements[10]);
				$defvalue = $this->modulefieldinformationfetch('modulefield','defaultvalue',$frmsecelements[12]);
				$ddpartable = $this->modulefieldinformationfetch('modulefield','ddparenttable',$frmsecelements[12]);
				$modfieldid = $frmsecelements[12];
				break;

			case 28://drop down with user specific condition
				$descp = $frmsecelements[5];
				$mandatory = $frmsecelements[6];
				$active = $frmsecelements[7];
				$readonly = $frmsecelements[8];
				$ddmultiple = $frmsecelements[9];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[12]);
				$columntye = $this->gencolumntype($frmsecelements[10]);
				$defvalue = $this->modulefieldinformationfetch('modulefield','defaultvalue',$frmsecelements[12]);
				$ddpartable = $this->modulefieldinformationfetch('modulefield','ddparenttable',$frmsecelements[12]);
				$modfieldid = $frmsecelements[12];
				break;
				
			case 29://parent picklist dependency
				$descp = $frmsecelements[5];
				$mandatory = $frmsecelements[6];
				$active = $frmsecelements[7];
				$readonly = $frmsecelements[8];
				$ddmultiple = $frmsecelements[9];
				$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[12]);
				$columntye = $this->gencolumntype($frmsecelements[10]);
				$defvalue = $this->modulefieldinformationfetch('modulefield','defaultvalue',$frmsecelements[12]);
				$ddpartable = $this->modulefieldinformationfetch('modulefield','ddparenttable',$frmsecelements[12]);
				$modfieldid = $frmsecelements[12];
				break;
				
			case 30: //Tags
					$defvalue = str_replace("|",",",$frmsecelements[3]);
					$descp = $frmsecelements[4];
					$mandatory = $frmsecelements[5];
					$active = $frmsecelements[6];
					$cusvalidation = $this->modulefieldinformationfetch('modulefield','fieldcustomvalidation',$frmsecelements[8]);
					$columntye = $this->gencolumntype($frmsecelements[7]);
					$modfieldid = $frmsecelements[8];
					break;

			default :
				break;
		}
		if($ddmultiple=='Yes' && $uitypeid!='15') {
			$modfieldtblname = $this->modulefieldinformationfetch('modulefield','tablename',$modfieldid);
			$modcolname = $this->modulefieldinformationfetch('modulefield','columnname',$modfieldid);
			if($modfieldtblname!='') {
				$this->db->query('ALTER TABLE '.$modfieldtblname.' CHANGE `'.$modcolname.'` `'.$modcolname.'` varchar(250) NOT NULL DEFAULT "1"',false);
			}
		}
		//drop down fields generation
		if($frmsecelements[1] == '17') {
			$colid = $this->modulefieldinformationfetch('modulefield','columnname',$frmsecelements[12]);
			$ddtabname = substr($colid,0,-2);
			$dataname = $ddtabname.'name';
			$datas=array();
			if($frmsecelements[3] != "" && $frmsecelements[3] != " ") {
				$datas = explode('|',$frmsecelements[3]);
				if($frmsecelements[11] == "Yes") {
					sort($datas);
				}
				$this->db->query('UPDATE '.$ddtabname.' SET status=0,setdefault=0 WHERE '.$colid.'!=1 AND FIND_IN_SET(moduleid,'.$moduleid.')>0');
				$sorder = 1;
				foreach($datas as $value) {
					$def = 0;
					if($frmsecelements[4] == $value) {
						$def = '1';
					}
					$industryid = $this->Basefunctions->industryid;
					$ddcheck = $this->dropdownvaluecheck($ddtabname,$dataname,$colid,$value,$moduleid);
					if($ddcheck == '0') {
						$this->db->query('INSERT INTO '.$ddtabname.' ( `'.$dataname.'`, `moduleid`,`userroleid`,`industryid`, `sortorder`, `setdefault`, `createdate`, `lastupdatedate`, `createuserid`, `lastupdateuserid`, `status` ) VALUES("'.$value.'","'.$moduleid.'","'.$userroleid.'","'.$industryid.'","'.$sorder.'","'.$def.'","'.$date.'","'.$date.'",'.$uid.','.$uid.',1)');
					} else {
						$this->db->query('UPDATE '.$ddtabname.' SET sortorder='.$sorder.',setdefault='.$def.',status=1 where '.$dataname.'="'.$value.'" AND moduleid="'.$moduleid.'"');
					}
					$sorder++;
				}
			}
		}
		$updefdataarr = $this->updatedefaultvalueget();
		//auto number table updation
		if($frmsecelements[1] == '14') {
			$angdata = array(
				'moduleid'=>$moduleid,
				'modulefieldid'=>$modfieldid,
				'suffix'=>$suffix,
				'prefix'=>$prefix,
				'startingnumber'=>$autonum,
				'random'=>$atuonumtype,
				'incrementby'=>0,
				'nextnumber'=>0
			);
			$newangdata = array_merge($angdata,$updefdataarr);
			$this->db->where('modulefieldid',$modfieldid);
			$this->db->where('moduleid',$moduleid);
			$this->db->update('serialnumbermaster',$newangdata);
		}
		$datasets = $this->modulefieldinfodata($modfieldid);
		//field data generate
		$upcolumn=array(
			'moduletabid'=>$moduleid,
			'uitypeid'=>$uitypeid,
			'moduletabsectionid'=>$tabsectionid,
			'fieldlabel'=>ucwords($frmsecelements[2]),
			'ddparenttable'=>$ddpartable,
			'fieldcustomvalidation'=>$cusvalidation,
			'readonly'=>$readonly,
			'defaultvalue'=>$defvalue,
			'maximumlength'=>$maxlength,
			'decimalsize'=>$decisize,
			'mandatory'=>$mandatory,
			'active'=>$active,
			'unique'=>$unique,
			'sequence'=>$seqnum,
			'multiple'=>$ddmultiple,
			'fieldcolumntype'=>$columntye,
			'description'=>$descp
		);
		$updatedata = array_merge($upcolumn,$updefdataarr);
		$this->db->where('modulefieldid',$modfieldid);
		$this->db->update('modulefield',$updatedata);		
		//update module field properties[user role modulefield]
		if($active!='') {
			$this->db->where('moduleid',$moduleid);
			$this->db->where('modulefieldid',$modfieldid);
			$this->db->update('userrolemodulefiled',array('fieldactive'=>$active,'fieldreadonly'=>$readonly));
		}		
		//upate view creation colname updation
		$this->db->query('UPDATE viewcreationcolumns SET viewcreationcolumnname="'.ucwords($frmsecelements[2]).'",status=1 where viewcreationcolumnname="'.$datasets["label"].'" AND uitypeid="'.$datasets["uitype"].'" AND moduletabsectionid ='.$datasets["modtabsec"].' AND viewcreationmoduleid="'.$moduleid.'" ');
	}
	//check section field is old/new
	public function sectionfieldcheck($frmsecelements) {
		$uitypeid = $frmsecelements[1];
		switch($frmsecelements[1]) {
			case 2: //text box
				$secfieldid = $frmsecelements[10];
				return $secfieldid;
				break;

			case 3: //text area
				$secfieldid = $frmsecelements[11];
				return $secfieldid;
				break;

			case 4://integer
				$secfieldid = $frmsecelements[9];
				return $secfieldid;
				break;

			case 5://decimal
				$secfieldid = $frmsecelements[10];
				return $secfieldid;
				break;

			case 6://percent
				$secfieldid = $frmsecelements[10];
				return $secfieldid;
				break;

			case 7://currency
				$secfieldid = $frmsecelements[10];
				return $secfieldid;
				break;

			case 8://Date
				$secfieldid = $frmsecelements[13];
				return $secfieldid;
				break;

			case 9://Time
				$secfieldid = $frmsecelements[9];
				return $secfieldid;
				break;

			case 10://Email
				$secfieldid = $frmsecelements[10];
				return $secfieldid;
				break;

			case 11: //phone
				$secfieldid = $frmsecelements[10];
				return $secfieldid;
				break;

			case 12: //URL
				$secfieldid = $frmsecelements[9];
				return $secfieldid;
				break;

			case 13: //check box
				$secfieldid = $frmsecelements[12];
				return $secfieldid;
				break;

			case 14: //Auto Number
				$secfieldid = $frmsecelements[12];
				return $secfieldid;
				break;

			case 15: //File Upload
				$secfieldid = $frmsecelements[9];
				return $secfieldid;
				break;

			case 16: //Image
				$secfieldid = $frmsecelements[9];
				return $secfieldid;
				break;

			case 17://picklist
				$secfieldid = $frmsecelements[12];
				return $secfieldid;
				break;
				
			case 18://group picklist
				$secfieldid = $frmsecelements[12];
				return $secfieldid;
				break;
	
			case 19://drop down for parent table
				$secfieldid = $frmsecelements[12];
				return $secfieldid;
				break;
	
			case 20://group drop down with multiple data attr
				$secfieldid = $frmsecelements[12];
				return $secfieldid;
				break;
				
			case 21://tree
				$secfieldid = $frmsecelements[12];
				return $secfieldid;
				break;

			case 22: //password
				$secfieldid = $frmsecelements[9];
				return $secfieldid;
				break;
	
			/* case 23://attribute set view create
				$secfieldid = $frmsecelements[11];
				return $secfieldid;
				break; */

			case 24: //Text Editor
				$secfieldid = $frmsecelements[8];
				return $secfieldid;
				break;
				
			case 25://multiple same parent drop down in single form
				$secfieldid = $frmsecelements[12];
				return $secfieldid;
				break;
	
			case 26: //Relation
				$secfieldid = $frmsecelements[15];
				return $secfieldid;
				break;
	
			case 27://Module drop down value fetch
				$secfieldid = $frmsecelements[12];
				return $secfieldid;
				break;
	
			case 28://drop down with user specific condition
				$secfieldid = $frmsecelements[12];
				return $secfieldid;
				break;
				
			case 29://parent group picklist
				$secfieldid = $frmsecelements[12];
				return $secfieldid;
				break;
				
			case 30://parent group picklist
					$secfieldid = $frmsecelements[8];
					return $secfieldid;
					break;
					
			default :
				break;
		}
	}
}
?>