<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//To Perform Insert Update Read and Delete Operations
class Leadmodel extends CI_Model{    
    function __construct() {
    	parent::__construct(); 
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
	}
	//To Create New lead creation
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$restricttable = explode(',',$_POST['resctable']);
		//industry based moduleid
		$moduleid = $_POST['viewfieldids'];
		//Retrive the Lead autoNumber
		$anfieldnameinfo = $_POST['anfieldnameinfo'];
		$anfieldnameidinfo = $_POST['anfieldnameidinfo'];
		$anfieldtabinfo = $_POST['anfieldtabinfo'];
		$anfieldnamemodinfo = $_POST['anfieldnamemodinfo'];
		$randomnum = $this->Basefunctions->randomnumbergenerator($anfieldnamemodinfo,$anfieldtabinfo,$anfieldnameinfo,$anfieldnameidinfo);
		$_POST['leadnumber']=trim($randomnum);
		//dynamic Insertion
		$primaryid = $this->Crudmodel->datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable);
		//address insertion
		$arrname=array('primary','secondary');
		$this->Crudmodel->addressdatainsert($arrname,$partablename,$primaryid);
		//notification entry (Audit log)
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		if(isset($_POST['employeeid'])) {
			$assignid = $_POST['employeeid'];
			$emptypes = $_POST['employeetypeid'];
			$empdataids = array();
			$assignempids = array();
			if($assignid != '') {
				$i = 0;
				$m=0;
				$empiddatas = explode(',',$assignid);
				$emptypeids = explode(',',$emptypes);
				foreach($empiddatas as $empids) {
					$emptype = $emptypeids[$m];
					if($emptype == 1) {
						$empdataids[$i] = $empids;
						$i++;
					} else if($emptype == 2) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromgroup($empids);
						$i++;
					} else if($emptype == 3) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromroles($empids);
						$i++;
					} else if($emptype == 4) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromrolesandsubroles($empids);
						$i++;
					}
					$m++;
				}
			} else {
				$assignid = 1;
			}
		} else {
			$assignid = 1;
		}
		$leadname = $_POST['lastname'];
		if($assignid == '1') {
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,2);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,$moduleid);
			}	
		} else {
			foreach($empdataids as $empidinfo) {
				if(is_array($empidinfo)) { //group of employees
					foreach($empidinfo as $empid) {
						if( !in_array($empid,$assignempids) ) {
							array_push($assignempids,$empid);
							$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,2);
							if($notimsg != '') {
								$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$empid,$moduleid);
							}
						}	
					}
				} else { //individual employees
					if( !in_array($empidinfo,$assignempids) ) {
						array_push($assignempids,$empidinfo);
						$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,2);
						if($notimsg != '') {
							$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$empidinfo,$moduleid);
						}
					}
				}
			}
		}
		{//workflow management -- for data create
			$this->Basefunctions->workflowmanageindatacreatemode($moduleid,$primaryid,$partablename);
		}
		echo 'TRUE';
	}
	//information fetch
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementpartable']);
		$restricttable = explode(',',$_GET['resctable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['dataprimaryid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetchwithrestrict($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$restricttable,$moduleid);
		echo $result;
	}
	//update data information
	public function datainformationupdatemodel(){
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['primarydataid'];
		$restricttable = explode(',',$_POST['resctable']);
		//industry based moduleid
		$moduleid = $_POST['viewfieldids'];
		{//fetch old values -- work flow
			$condstatvals=$this->Basefunctions->workflowmanageolddatainfofetch($moduleid,$primaryid);
		}
		$result = $this->Crudmodel->dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable);
		$arrname=array('primary','secondary');
		$this->Crudmodel->addressdataupdate($arrname,$partablename,$primaryid);	
		//notification log entry (Audit log)
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		if(isset($_POST['employeeid'])) {
			$assignid = $_POST['employeeid'];
			$emptypes = $_POST['employeetypeid'];
			$empdataids = array();
			$assignempids = array();
			if($assignid != '') {
				$i = 0;
				$m=0;
				$empiddatas = explode(',',$assignid);
				foreach($empiddatas as $empids) {
					$emptype = $emptypes[$m];
					if($emptype == 1) {
						$empdataids[$i] = $empids;
						$i++;
					} else if($emptype == 2) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromgroup($empids);
						$i++;
					} else if($emptype == 3) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromroles($empids);
						$i++;
					} else if($emptype == 4) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromrolesandsubroles($empids);
						$i++;
					}
					$m++;
				}
			} else {
				$assignid = 1;
			}
		} else {
			$assignid = 1;
		}
		$leadname = $_POST['lastname'];
		if($assignid == '1') {
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,3);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,$moduleid);
			}
		} else {
			foreach($empdataids as $empidinfo) {
				if(is_array($empidinfo)) { //group of employees
					foreach($empidinfo as $empid) {
						if( !in_array($empid,$assignempids) ) {
							array_push($assignempids,$empid);
							$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,3);
							if($notimsg != '') {
								$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,$moduleid);
							}
						}
					}
				} else { //individual employees
					if( !in_array($empidinfo,$assignempids) ) {
						array_push($assignempids,$empidinfo);
						$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,3);
						if($notimsg != '') {
							$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,$moduleid);
						}
					}
				}
			}
		}
		{//workflow management for data update
			$this->Basefunctions->workflowmanageindataupdatemode($moduleid,$primaryid,$partablename,$condstatvals);
		}
		echo 'TRUE';
	}
	//delete old information
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primarydataid'];
		$msg='False';
		$filename = $this->Basefunctions->generalinformaion('lead','lastname','leadid',$id);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//industrybased moduleid fetch
		$modid = $this->Basefunctions->getmoduleid($moduleid);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		{//workflow management -- for data delete
			$this->Basefunctions->workflowmanageindatadeletemode($modid,$id,$primaryname,$partabname);
		}
		if($ruleid == 0 || $ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				$msg='Denied';
			} else {
				//notification log
				$empid = $this->Basefunctions->logemployeeid;
				//deleted file name
				$notimsg = $this->Basefunctions->notificationtemplatedatafetch($modid,$id,$partabname,4);
				if($notimsg != '') {
					$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,$empid,$modid);
				}
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				$msg='TRUE';
			}
		} else {
			//notification log
			$empid = $this->Basefunctions->logemployeeid;
			//deleted file name
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch($modid,$id,$partabname,4);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,$empid,$modid);
			}
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			$msg='TRUE';
		}
		echo $msg;
	}
	//lead conversion denied check function
	public function ledconversiondeniedmodel($moduleid) {
		$id = $_GET['dataprimaryid'];
		$partabname = 'lead';
		$primaryname = 'leadid';
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 0 || $ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				echo 'TRUE';
			}
		} else {
			$check = $this->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($check == 'False') {
				echo 'Denied';
			} else {
				echo 'TRUE';
			}
		}
	}
	public function checkrecordcreateduser($partabname,$primaryname,$id,$moduleid) {
		$this->db->select($primaryname);
		$this->db->from($partabname);
		$this->db->where($partabname.'.'.$primaryname,$id);
		$this->db->where($partabname.'.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result()as $row) {
				$data = $row->$primaryname;
			} 
			return 'True';
		} else{
			return 'False';
		}
	}
	//primary and secondary address value fetch
	public function primaryaddressvalfetchmodel(){
		$accid = $_GET['dataprimaryid'];	
		$this->db->select('leadaddress.addresssourceid,leadaddress.address,leadaddress.pincode,leadaddress.city,leadaddress.state,leadaddress.country');
		$this->db->from('leadaddress');
		$this->db->where('leadaddress.leadid',$accid);
		$this->db->where('leadaddress.status',1);
		$this->db->order_by('leadaddressid','asc');
		$result = $this->db->get();
		$arrname=array('primary','secondary');
		if($result->num_rows() >0) {
			$m=0;
			foreach($result->result() as $row) {
				$data[] = array($arrname[$m].'addresstype'=>$row->addresssourceid,
								$arrname[$m].'address'=>$row->address,
								$arrname[$m].'pincode'=>$row->pincode,
								$arrname[$m].'city'=>$row->city,
								$arrname[$m].'state'=>$row->state,
								$arrname[$m].'country'=>$row->country);
				$m++;
			}
			if(count($data) != 1 ) { 
				$finalarray = array_merge($data[0],$data[1]);
			} else {
				$finalarray = $data[0];
			}
			echo json_encode($finalarray);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}	
	//lead data fetch
	public function leaddatafetchmodel(){
		$leadid = $_GET['leadid']; 
		$industryid = $_GET['industryid'];
		$softwareindustryid = $this->Basefunctions->industryid;
		if($softwareindustryid == 2){
			$this->db->select('leadname');
			$this->db->from('lead');
			$this->db->where('lead.leadid',$leadid);
			$this->db->where('lead.industryid',$industryid);
			$result = $this->db->get();
			if($result->num_rows() > 0) {
				foreach($result->result() as $row) {
					$data = array('lanme'=>$row->leadname,'accname'=>$row->leadname,'convertid'=>$leadid);
				}
				echo json_encode($data);
			} else {
				echo json_encode(array("fail"=>'FAILED'));
			}
		}else{
			$this->db->select('leadname, accountname');
			$this->db->from('lead');
			$this->db->where('lead.leadid',$leadid);
			$this->db->where('lead.industryid',$industryid);
			$result = $this->db->get();
			if($result->num_rows() > 0) {
				foreach($result->result() as $row) {
					$data = array('lanme'=>$row->leadname,'accname'=>$row->accountname,'convertid'=>$leadid);
				}
				echo json_encode($data);
			} else {
				echo json_encode(array("fail"=>'FAILED'));
			}
		}
	}
	//lead creation convert function
	public function leadconvertfunctionmodel() {
		$i=0;
		$softwareindustryid = $this->Basefunctions->industryid;
		$leadid = $_POST['convertid'];
		$userid = $this->Basefunctions->userid;	
		$date =	date($this->Basefunctions->datef);	
		$accountcheckboxid = $_POST['accountcheck'];
		$contactcheckboxid = $_POST['contactcheck'];
		$opportunitycheckboxid = $_POST['opportunitycheck'];
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		$leadname = $this->Basefunctions->generalinformaion('lead','leadname','leadid',$leadid);
		$primary =  array();
		$secondary = array();
		$acleadfielddata = array();
		$accpridata = array();
		$accleadparedata = array();
		$contpridata = array();
		$cleadpridata = array();
		$leadmerge = array();
		//lead employeetypeidget
		$leademptypeid = $this->Basefunctions->generalinformaion('lead','employeetypeid','leadid',$leadid);
		$defdataarr = $this->defaultvalueget();
		//used for get the lead conversion mapping data
		$this->db->select('SQL_CALC_FOUND_ROWS  leadconversionmapping.leadconversionmappingid,lf.columnname as leadfieldname,af.columnname as accountfieldname,cf.columnname as contactfieldname,of.columnname as opportunityfieldname,lf.tablename as laddtabname,lf.parenttable as ldpartabname,af.tablename as aaddtabname,af.parenttable as afpartabname,cf.tablename as caddtabname,cf.parenttable as cfpartabname,of.tablename as oaddtabname,of.parenttable as ofpartabname,lf.fieldname as laddfieldname,af.fieldname as aaddfieldname,cf.fieldname as caddfieldname,of.fieldname as oaddfieldname,af.uitypeid as accuitype,cf.uitypeid as contuitype,of.uitypeid as oppuitype',false);
		$this->db->from('leadconversionmapping');
		//lead data get
		$this->db->join('modulefield as lf','lf.modulefieldid=leadconversionmapping.leadid','left outer');
		//account data get
		$this->db->join('modulefield as af','af.modulefieldid=leadconversionmapping.accountid','left outer');
		//contact data get
		$this->db->join('modulefield as cf','cf.modulefieldid=leadconversionmapping.contactid','left outer');
		//opportunity data get
		$this->db->join('modulefield as of','of.modulefieldid=leadconversionmapping.opportunityid','left outer');
		$this->db->where_not_in('leadconversionmapping.status',array(3,0));
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
			//lead data
				$lead[$i] = $row->leadfieldname;
				$leadtabname[$i] = $row->laddtabname;
				$leadparenttable[$i] = $row->ldpartabname;
				$leadfieldname[$i] = $row->laddfieldname;
			//account data
				$account[$i] = $row->accountfieldname; // modulefield column name
				$accunttabname[$i] = $row->aaddtabname; // parent with child table name
				$accparenttable[$i] = $row->afpartabname; //parent table name
				$accountfieldname[$i] = $row->aaddfieldname; // modulefield filed name
				$accountuitype[$i] = $row->accuitype;//uitype
			//contact data
				$contact[$i] = $row->contactfieldname;
				$contacttabname[$i] = $row->caddtabname;
				$contparenttable[$i] = $row->cfpartabname;
				$contactfieldname[$i] = $row->caddfieldname;
				$contactuitype[$i] = $row->contuitype;//uitype
			//opportunity data
				$opportunity[$i] = $row->opportunityfieldname;
				$opptabname[$i] = $row->oaddtabname;
				$oppparenttable[$i] = $row->ofpartabname;
				$oppfieldname[$i] = $row->oaddfieldname;
				$oppuitype[$i] = $row->oppuitype;//uitype
				$i++;
			}
		}
		if($accountcheckboxid == 'yes') {
			$accountcolumn = array();
			$accleadcolumn = array();
			$accountfiled = array();
			$accleadfiled = array();
			$accountparenttbl = array();
			$leadparenttbl = array();
			$accountparchildtbl = array();
			$leadparchildtabl = array();
			$accuitype = array();
			$n=0;$m=0;
			foreach($account as $accfiled) {
				if($accfiled != '') {
					// module filed column data
					$accountcolumn[$m] =  $accfiled;
					$accleadcolumn[$m] = $lead[$n];
					//account uiytpe
					$accuitype[$m] = $accountuitype[$n];
					//modulefiled - field data
					$accountfiled[$m] = $accountfieldname[$n];
					$accleadfiled[$m] = $leadfieldname[$n];
					$m++;
				}
				$n++;
			}
			$m=0;
			foreach($accountfieldname as $acccol) {
				if(in_array($acccol,$accountfiled)) {
					$key = array_search ($acccol, $accountfieldname);
					//account parent table
					$accountparenttbl[$m] = $accparenttable[$key];
					$leadparenttbl[$m] = $leadparenttable[$key];
					//account child with parent
					$accountparchildtbl[$m] = $accunttabname[$key];
					$leadparchildtabl[$m] = $leadtabname[$key];
					$m++;
				}
			}
			//for account based lead data
			$leadparentable =  $this->Crudmodel->filtervalue($leadparenttbl);
			$accountpartablename =  $this->Crudmodel->filtervalue($accountparenttbl);
			$accounttablenames = $this->Crudmodel->filtervalue($accountparchildtbl);
			$tableinfo = explode(',',$accounttablenames);
			$lpfielddata = array();
			$lptabdata = array();
			$lcdata = array();
			$lparentdata = array();
			$lfielddata = array();
			$apdata = array();
			$accountparfield = array();
			$acctabname = array();
			$lcfielddata = array();
			$lctabdata = array();
			$acchild = array();
			$accchiletabname = array();
			$accountchildfield = array();
			$leadaddd = array();
			$leadadddfiled = array();
			$leadadddtable = array();
			$accadd = array();
			$accountaddfield = array();
			$accaddtabname = array();
			$auitypee = array();
			foreach($tableinfo as $tblname) {
				$n=0;$l=0;$m=0;$j=0;$k=0;
				foreach($accountfiled as $fname) {
					if( strcmp( trim($accountpartablename),trim($accountparchildtbl[$n]) ) == 0 ) { 
						//for lead
						$lparentdata[$m] =  $accleadcolumn[$n];
						$lpfielddata[$m] =  $accleadfiled[$n];
						$lptabdata[$m] =  $leadparchildtabl[$n];
						//for account
						$apdata[$m] = $accountcolumn[$n];
						$accountparfield[$m] = $accountfiled[$n];
						$acctabname[$m] = $accountparchildtbl[$n];
						$auitypee[$m] = $accuitype[$n];
						$m++;
					} else if( strcmp( trim($accountparchildtbl[$n]),trim($tblname) ) == 0 ) {
						if( $accountparchildtbl[$n] == 'accountaddress'){
							$leadaddd[$k] = $accleadcolumn[$n];
							$leadadddfiled[$k] = $accleadfiled[$n];
							$leadadddtable[$k] = $leadparchildtabl[$n];
							//for account
							$accadd[$k] = $accountcolumn[$n];
							$accountaddfield[$k] = $accountfiled[$n];
							$accaddtabname[$k] = $accountparchildtbl[$n];
							$k++;
						} else { 
							${'$cacccolmmn'.$l}[$j] = $accountcolumn[$n];
							${'$caccfield'.$l}[$j] = $accountfiled[$n];
							${'$cacctable'.$l}[$j] = $accountparchildtbl[$n]; 
							${'$caccuitype'.$l}[$j] = $accuitype[$n]; 
							//for lead
							${'$cleadcolmmn'.$l}[$j] = $accleadcolumn[$n];
							${'$cleadfield'.$l}[$j] = $accleadfiled[$n];
							${'$cleadtable'.$l}[$j] = $leadparchildtabl[$n]; 
							$j++;
						}
					}
					$n++;
				}
				$l++;
			}
			$cleadprimary = array();
			$cleadsecondary = array();
			$accountprimary = array();
			$accountsecondary = array();
			$leadataprimaryvalue= array();
			$leadatasecondaryvalue= array();
			$cleadtable = array();
			$cleadsectable = array();
			$count = count($accountaddfield);
			$ccp=0;
			$ccs=0;
			for($k=0;$k<$count;$k++) {
				$pri = 'billing';
				$pricount = strlen($pri);
				$lfpri = substr($accountaddfield[$k],0,$pricount);
				if($lfpri == $pri) {
					if(isset($accountaddfield[$k])){
						$accountprimary[$ccp] = $accadd[$k];
						$cleadprimary[$ccp] = $leadaddd[$k];
						$cleadtable[$ccp] = $leadadddtable[$k];
					}
					$ccp++;
				} else {
					if(isset($accountaddfield[$k])){
						$accountsecondary[$ccs] = $accadd[$k];
						$cleadsecondary[$ccs] = $leadaddd[$k];
						$cleadsectable[$ccs] = $leadadddtable[$k];
					}
					$ccs++;
				}
			}
			//Lead data fetch		
			if($lparentdata != '') {
				$leadparentdata = $this->leaddatagetfunctionone($leadid,$lparentdata,$lptabdata,$leadparentable);
			} else { $leadparentdata = array(); }
			if(!empty($leadparentdata)) {
				$accountmappeddatadata = $this->picklistdatamapping($leadparentable,$auitypee,$lparentdata,$leadparentdata,'91');
				$accountparentdata = array_combine($apdata,$accountmappeddatadata);
			} else {
				$accountparentdata	= array();
			}
			$accountparentdata['accountname']=$_POST['accname'];
			//Retrive the Account autoNumber
			if($softwareindustryid == 1){				
				$randomnum = $this->Basefunctions->randomnumbergenerator(202,'account','accountnumber',1364);
			}else if($softwareindustryid == 2){
				$randomnum = $this->Basefunctions->randomnumbergenerator(74,'account','accountnumber',1982);
			}else if($softwareindustryid == 3){
				$randomnum = $this->Basefunctions->randomnumbergenerator(91,'account','accountnumber',2875);
			}else if($softwareindustryid == 4){
				$randomnum = $this->Basefunctions->randomnumbergenerator(81,'account','accountnumber',2224);
			}else if($softwareindustryid == 5){
				$randomnum = $this->Basefunctions->randomnumbergenerator(129,'account','accountnumber',4072);
			} else {
				$randomnum = '';
			}			
			$accountparentdata['accountnumber']=trim($randomnum);
			$accountparentdata['industryid'] = $this->Basefunctions->industryid;
			$accountparentdata['branchid'] = $this->Basefunctions->branchid;
			$accountdata = array_merge($accountparentdata,$defdataarr);
			$this->db->insert('account',$accountdata);
			$accid = $this->db->insert_id();
			if($this->Basefunctions->industryid == '3') {
				$update=array(
					'accounttypeid'=>'6',
					'accountcatalogid'=>'33',
				);
				$update = array_merge($update);
				$this->db->where('accountid',$accid);
				$this->db->update('account',$update);
			}
			$this->employeetypeidupdate($leademptypeid,'account',$accid);
			$notimsg = $empname." "."Converted a Lead named into Account on -".$leadname."";
			$this->Basefunctions->notificationcontentadd($accid,'Convert',$notimsg,1,202);
			if(!empty($cleadprimary)) {
				$leadataprimaryvalue = $this->leadaddressprimarydatagetfunction($leadid,$cleadprimary,$cleadtable);
			}
			if(!empty($cleadsecondary)) {
				$leadatasecondaryvalue = $this->leadaddressscondarydatagetfunction($leadid,$cleadsecondary,$cleadsectable);
			}
			{// for account address table
				{//account primary address
					if(!empty($leadataprimaryvalue)) {
						$acountaddressarray = array_combine($accountprimary, $leadataprimaryvalue);
					} else {
						$acountaddressarray = array();
					}
					$newaccountaddddata = array_merge($acountaddressarray,$defdataarr);
					$accopridata['addressmethod'] = 4;
					$accopridata['addresstypeid'] = 2;
					$accopridata['accountid'] = $accid;
					$accoprimarydata = array_merge($newaccountaddddata,$accopridata);
					$this->db->insert('accountaddress',$accoprimarydata);
				}
				{//account secondary address
					if(!empty($leadatasecondaryvalue)) {
						$acountsecaddressarray = array_combine($accountsecondary, $leadatasecondaryvalue);
					} else {
						$acountsecaddressarray = array();
					}
					$newaccountsecaddddata = array_merge($acountsecaddressarray,$defdataarr);
					$accosecaddata['addressmethod'] = 5;
					$accosecaddata['addresstypeid'] = 3;
					$accosecaddata['accountid'] = $accid;
					$accountsecaddddata = array_merge($newaccountsecaddddata,$accosecaddata);
					$this->db->insert('accountaddress',$accountsecaddddata); 
				}
			}
			{//child table insertion
				$m=0;
				foreach( $tableinfo as $tblname ) {
					$defdataarr = $this->defaultvalueget();
					$caccdata = array();
					$cnewdata = array();
					$newdata = array();
					$leadcustomtdata = array();
					$cleadtbledata = array();
					$cleaddata = array();
					if(isset(${'$cacccolmmn0'})) {
						if(count(${'$cacccolmmn0'}) > 0 ) {
							if( strcmp( trim($accountpartablename),trim($tblname) ) != 0 ) {
								if($tblname != 'accountaddress') {
									$caccdata = ${'$cacccolmmn0'};
									$cleaddata = ${'$cleadcolmmn0'};
									$cleadtbledata = ${'$cleadtable0'};
									$leadcustomtdata = $this->leaddatagetfunctionone($leadid,$cleaddata,$cleadtbledata,$leadparentable);
									if($leadcustomtdata != '') {
										$cnewdata = array_combine($caccdata,$leadcustomtdata);
									}
									$newdata = array_merge($cnewdata,$defdataarr);
									//default value get
									$newdata['accountid'] = $accid;
									//data insertion
									$this->db->insert( $tblname, array_filter($newdata) );
								}
							}
						}
						$m++;
					}
				}
			}
		} else {
			$accid = 1;
		}
		if($contactcheckboxid == 'yes') {
			$contactcolumn = array();
			$contleadcolumn = array();
			$contactfiled = array();
			$contleadfiled = array();
			$contactparenttbl = array();
			$contleadparenttbl = array();
			$contactparchildtbl = array();
			$contleadparchildtabl = array();
			$contuitypeid = array();
			$n=0;$m=0;
			foreach($contact as $accfiled) {
				if($accfiled != '') {
					// module filed column data
					$contactcolumn[$m] =  $accfiled;
					$contleadcolumn[$m] = $lead[$n];
					//contact uitypeid
					$contuitypeid[$m] = $contactuitype[$n] ;
					//modulefiled - field data
					$contactfiled[$m] = $contactfieldname[$n];
					$contleadfiled[$m] = $leadfieldname[$n];
					$m++;
				}
				$n++;
			} 
			$m=0;
			foreach($contactfieldname as $acccol) {
				if(in_array($acccol,$contactfiled)) {
					$key = array_search ($acccol, $contactfieldname);
					//contact parent table
					$contactparenttbl[$m] = $contparenttable[$key];
					$contleadparenttbl[$m] = $leadparenttable[$key];
					//contact child with parent
					$contactparchildtbl[$m] = $contacttabname[$key];
					$contleadparchildtabl[$m] = $leadtabname[$key];
					$m++;
				}
			}
			//for contact based lead data
			$contleadparentable =  $this->Crudmodel->filtervalue($contleadparenttbl);
			$contactpartablename =  $this->Crudmodel->filtervalue($contactparenttbl);
			$contacttablenames = $this->Crudmodel->filtervalue($contactparchildtbl);
			$tableinfo = explode(',',$contacttablenames);
			$cpfielddata = array();
			$cptabdata = array();
			$lcdata = array();
			$cparentdata = array();
			$lfielddata = array();
			$cpdata = array();
			$contactparfield = array();
			$conttabname = array();
			$lcfielddata = array();
			$lctabdata = array();
			$acchild = array();
			$accchiletabname = array();
			$accountchildfield = array();
			$leadaddd = array();
			$leadadddfiled = array();
			$leadadddtable = array();
			$contadd = array();
			$contaddfield = array();
			$contaddtabname = array();
			$cuitype = array();
			foreach($tableinfo as $tblname) {
				$n=0;
				$l=0;
				$m=0;
				$j=0;
				$k=0;
				foreach($contleadfiled as $fname) {
					if( strcmp( trim($contactpartablename),trim($contactparchildtbl[$n]) ) == 0 ) { 
						//for lead
						$cparentdata[$m] =  $contleadcolumn[$n];
						$cpfielddata[$m] =  $contleadfiled[$n];
						$cptabdata[$m] =  $contleadparchildtabl[$n];
						//for contact
						$cpdata[$m] = $contactcolumn[$n];
						$contactparfield[$m] = $contactfiled[$n];
						$conttabname[$m] = $contactparchildtbl[$n];
						$cuitype[$m] = $contuitypeid[$n];
						$m++;
					} else if( strcmp( trim($contactparchildtbl[$n]),trim($tblname) ) == 0 ) {
						if( $contactparchildtbl[$n] == 'contactaddress'){
							$leadaddd[$k] = $contleadcolumn[$n];
							$leadadddfiled[$k] = $contleadfiled[$n];
							$leadadddtable[$k] = $contleadparchildtabl[$n];
							//for account
							$contadd[$k] = $contactcolumn[$n];
							$contaddfield[$k] = $contactfiled[$n];
							$contaddtabname[$k] = $contactparchildtbl[$n];
							$k++;
						} else { 
							${'$ccacccolmmn'.$l}[$j] = $contactcolumn[$n];
							${'$caccfield'.$l}[$j] = $contactfiled[$n];
							${'$cacctable'.$l}[$j] = $contactparchildtbl[$n]; 
							//for lead
							${'$ccleadcolmmn'.$l}[$j] = $contleadcolumn[$n];
							${'$cleadfield'.$l}[$j] = $contleadfiled[$n];
							${'$ccleadtable'.$l}[$j] = $contleadparchildtabl[$n]; 
							$j++;
						} 
					}
					$n++;
				}
				$l++;
			}
			$cleadprimary = array();
			$cleadsecondary = array();
			$contprimary = array();
			$contsecondary = array();
			$leadataprimaryvalue= array();
			$leadatasecondaryvalue= array();
			$ccleadtable = array();
			$ccleadsectable = array();
			$count = count($contaddfield);
			$ccp=0;
			$ccs=0;
			for($k=0;$k<$count;$k++) {
				$pri = 'primary';
				$pricount = strlen($pri);
				$lfpri = substr($contaddfield[$k],0,$pricount);
				if($lfpri == $pri) {
					if(isset($contaddfield[$k])){
						$contprimary[$ccp] = $contadd[$k];
						$cleadprimary[$ccp] = $leadaddd[$k];
						$ccleadtable[$ccp] = $leadadddtable[$k];
					}
					$ccp++;
				} else {
					if(isset($contaddfield[$k])){
						$contsecondary[$ccs] = $contadd[$k];
						$cleadsecondary[$ccs] = $leadaddd[$k];
						$ccleadsectable[$ccs] = $leadadddtable[$k];
					}
					$ccs++;
				}
			}
			//Contact parent table insertion 
			if($cparentdata != '') {
				$contleadparentdata = $this->leaddatagetfunctionone($leadid,$cparentdata,$cptabdata,$contleadparentable);
			} else {
				$contleadparentdata = array();
			}
			if(!empty($contleadparentdata)) {
				$contactmappeddatadata = $this->picklistdatamapping($contleadparentable,$cuitype,$cparentdata,$contleadparentdata,'203');
				$contactparentdata = array_combine($cpdata,$contactmappeddatadata);
			} else {
				$contactparentdata = array();
			}
			$contactparentdata['contactname']=$_POST['contname'];
			$contactparentdata['accountid']=$accid;
			//Retrive the Contact autoNumber
			if($softwareindustryid == 1){
				$randomnum = $this->Basefunctions->randomnumbergenerator(203,'contact','contactnumber',263);
			}else if($softwareindustryid == 2){
				$randomnum = $this->Basefunctions->randomnumbergenerator(75,'contact','contactnumber',1993);
			}else if($softwareindustryid == 3){
				$randomnum = $this->Basefunctions->randomnumbergenerator(203,'contact','contactnumber',263);
			}else if($softwareindustryid == 4){
				$randomnum = $this->Basefunctions->randomnumbergenerator(82,'contact','contactnumber',2235);
			}else if($softwareindustryid == 4){
				$randomnum = $this->Basefunctions->randomnumbergenerator(130,'contact','contactnumber',4079);
			} else {
				$randomnum = '';
			}			
			$contactparentdata['contactnumber'] = trim($randomnum);
			$contactparentdata['industryid'] = $this->Basefunctions->industryid;
			$contactparentdata['branchid'] = $this->Basefunctions->branchid;
			$contdata = array_merge($contactparentdata,$defdataarr);
			$this->db->insert('contact',$contdata);
			$contid = $this->db->insert_id();
			$notimsg = $empname." "."Converted a Lead named into Contact on -".$leadname."";
			$this->Basefunctions->notificationcontentadd($contid,'Convert',$notimsg,1,203); 
			if($cleadprimary != '') {
				$leadataprimaryvalue = $this->leadaddressprimarydatagetfunction($leadid,$cleadprimary,$ccleadtable);
			}
			if(!empty($cleadsecondary)) {
				$leadatasecondaryvalue = $this->leadaddressscondarydatagetfunction($leadid,$cleadsecondary,$ccleadsectable);
			}
			{// for contact address table
				{//contact primary address
					if(!empty($leadataprimaryvalue)) {
						$acountaddressarray = array_combine($contprimary, $leadataprimaryvalue);
					} else {
						$acountaddressarray = array();
					}
					$newaccountaddddata = array_merge($acountaddressarray,$defdataarr);
					$contopridata['addressmethod'] = 4;
					$contopridata['addresstypeid'] = 2;
					$contopridata['contactid'] = $contid;
					$accoprimarydata = array_merge($newaccountaddddata,$contopridata);
					$this->db->insert('contactaddress',$accoprimarydata);
				}
				{//contact secondary address
					if(!empty($leadatasecondaryvalue)) {
						$acountsecaddressarray = array_combine($contsecondary, $leadatasecondaryvalue);
					} else {
						$acountsecaddressarray = array();
					}
					$newaccountsecaddddata = array_merge($acountsecaddressarray,$defdataarr);
					$contosecaddata['addressmethod'] = 5;
					$contosecaddata['addresstypeid'] = 3;
					$contosecaddata['contactid'] = $contid;
					$accountsecaddddata = array_merge($newaccountsecaddddata,$contosecaddata);
					$this->db->insert('contactaddress',$accountsecaddddata); 
				}
			}
			{//child table insertion
				$m=0;
				foreach( $tableinfo as $tblname ) {
					$defdataarr = $this->defaultvalueget();
					$caccdata = array();
					$cnewdata = array();
					$newdata = array();
					$cleadcustomtdata = array();
					$cleadtbledata = array();
					$cleaddata = array();
					if(isset(${'$ccacccolmmn0'})) {
						if(count(${'$ccacccolmmn0'}) > 0 ) {
							if( strcmp( trim($contactpartablename),trim($tblname) ) != 0 ) {
								if($tblname != 'contactaddress') {
									$caccdata = ${'$ccacccolmmn0'};
									$cleaddata = ${'$ccleadcolmmn0'};
									$cleadtbledata = ${'$ccleadtable0'};
									$cleadcustomtdata = $this->leaddatagetfunctionone($leadid,$cleaddata,$cleadtbledata,$contleadparentable);
									if(!empty($cleadcustomtdata)) {
										$cnewdata = array_combine($caccdata,$cleadcustomtdata);
									}
									$newdata = array_merge($cnewdata,$defdataarr);
									//default value get
									$newdata['contactid'] = $contid;
									//data insertion
									$this->db->insert( $tblname, array_filter($newdata) );
								}
							}
						}
						$m++;
					}
				}
			}
		} else { $contid =1; }
		if($opportunitycheckboxid == 'yes'){
			$opportunitycolumn = array();
			$oppleadcolumn = array();
			$opportunityfiled = array();
			$oppleadfiled = array();
			$opportunityparenttbl = array();
			$oppleadparenttbl = array();
			$opportunityparchildtbl = array();
			$oppleadparchildtabl = array();
			$oppuitypeid = array();
			$n=0;$m=0;
			foreach($opportunity as $oppfiled) {
				if($oppfiled != '') {
					// module filed column data
					$opportunitycolumn[$m] =  $oppfiled;
					$oppleadcolumn[$m] = $lead[$n];
					//opportunity uitype id
					$oppuitypeid[$m] = $oppuitype[$n];
					//modulefiled - field data
					$opportunityfiled[$m] = $oppfieldname[$n];
					$oppleadfiled[$m] = $leadfieldname[$n];
					$m++;
				}
				$n++;
			} 
			$m=0;
			foreach($oppfieldname as $oppcol) {
				if(in_array($oppcol,$opportunityfiled)) {
					$key = array_search ($oppcol, $oppfieldname);
					//opportunity parent table
					$opportunityparenttbl[$m] = $oppparenttable[$key];
					$oppleadparenttbl[$m] = $leadparenttable[$key];
					//opportunity child with parent
					$opportunityparchildtbl[$m] = $opptabname[$key];
					$oppleadparchildtabl[$m] = $leadtabname[$key];
					$m++;
				}
			}
			//for opportunity based lead data
			$oppleadparentable =  $this->Crudmodel->filtervalue($oppleadparenttbl);
			$opportunitypartablename =  $this->Crudmodel->filtervalue($opportunityparenttbl);
			$opportunitytablenames = $this->Crudmodel->filtervalue($opportunityparchildtbl);
			$tableinfo = explode(',',$opportunitytablenames);
			$opplpfielddata = array();
			$opplptabdata = array();
			$lcdata = array();
			$opplparentdata = array();
			$lfielddata = array();
			$oppdata = array();
			$oppparfield = array();
			$opptabname = array();
			$lcfielddata = array();
			$lctabdata = array();
			$acchild = array();
			$accchiletabname = array();
			$accountchildfield = array();
			$ouitype = array();
			foreach($tableinfo as $tblname) {
				$n=0;$l=0;$m=0;$j=0;
				foreach($opportunityfiled as $fname) {
					if( strcmp( trim($opportunitypartablename),trim($opportunityparchildtbl[$n]) ) == 0 ) { 
						//for lead
						$opplparentdata[$m] =  $oppleadcolumn[$n];
						$opplpfielddata[$m] =  $oppleadfiled[$n];
						$opplptabdata[$m] =  $oppleadparchildtabl[$n];
						//for opportunity
						$oppdata[$m] = $opportunitycolumn[$n];
						$oppparfield[$m] = $opportunityfiled[$n];
						$opptabname[$m] = $opportunityparchildtbl[$n];
						$ouitype[$m] = $oppuitypeid[$n];
						$m++;
					} else if( strcmp( trim($opportunityparchildtbl[$n]),trim($tblname) ) == 0 ) {
						${'$ocleadcolmmn'.$l}[$j] = $oppleadcolumn[$n];
						${'$ocleadfield'.$l}[$j] = $oppleadfiled[$n];
						${'$ocleadtable'.$l}[$j] = $oppleadparchildtabl[$n];
						//for opp
						${'$oacccolmmn'.$l}[$j] = $opportunitycolumn[$n];
						${'$oaccfield'.$l}[$j] = $opportunityfiled[$n];
						${'$oacctable'.$l}[$j] = $opportunityparchildtbl[$n];
						$j++;  
					}
					$n++;
				}
				$l++;
			}
			//opportunity parent table insertion
			if(!empty($opplparentdata)) {
				$leadparentdata = $this->leaddatagetfunctionone($leadid,$opplparentdata,$opplptabdata,$oppleadparentable);
			} else { $leadparentdata = array(); }
			if(!empty($leadparentdata)) {
				$opportunitymappeddatadata = $this->picklistdatamapping($opplparentdata,$ouitype,$opplparentdata,$leadparentdata,'204');
				$opportuntiyparentdata = array_combine($oppdata,$opportunitymappeddatadata);
			} else { $opportuntiyparentdata = array();}
			$opportuntiyparentdata['opportunityname']=$_POST['oppname'];
			$opportuntiyparentdata['expectedclosedate']=$_POST['closedate'];
			$opportuntiyparentdata['crmstatusid']=$_POST['stage'];
			$opportuntiyparentdata['amount']=$_POST['amount'];
			$opportuntiyparentdata['nextstep']=$_POST['nextstep'];
			$opportuntiyparentdata['accountid']=$accid;
			$opportuntiyparentdata['contactid']=$contid;
			//Retrive the Opprotunity autoNumber
			if($softwareindustryid == 1){
				$randomnum = $this->Basefunctions->randomnumbergenerator(204,'opportunity','opportunitynumber',1365);
			}else if($softwareindustryid == 2){
				$randomnum = $this->Basefunctions->randomnumbergenerator(93,'opportunity','opportunitynumber',2955);
			}else if($softwareindustryid == 3){
				$randomnum = $this->Basefunctions->randomnumbergenerator(204,'opportunity','opportunitynumber',1365);
			}else if($softwareindustryid == 4){
				$randomnum = $this->Basefunctions->randomnumbergenerator(84,'opportunity','opportunitynumber',2340);
			}else if($softwareindustryid == 5){
				$randomnum = $this->Basefunctions->randomnumbergenerator(131,'opportunity','opportunitynumber',4089);
			} else {
				$randomnum = '';
			}
			$opportuntiyparentdata['opportunitynumber'] = trim($randomnum);
			$opportuntiyparentdata['industryid'] = $this->Basefunctions->industryid;
			$opportuntiyparentdata['branchid'] = $this->Basefunctions->branchid;
			$opportunitydata = array_merge($opportuntiyparentdata,$defdataarr);
			$this->db->insert('opportunity',$opportunitydata);
			$oppid = $this->db->insert_id();
			$this->employeetypeidupdate($leademptypeid,'opportunity',$oppid);
			$notimsg = $empname." "."Converted a Lead named into Opportunity on -".$leadname."";
			$this->Basefunctions->notificationcontentadd($oppid,'Convert',$notimsg,1,204);
			{//child table insertion
				$m=0;
				foreach( $tableinfo as $tblname ) {
					$defdataarr = $this->defaultvalueget();
					$caccdata = array();
					$cnewdata = array();
					$newdata = array();
					$oppleadcustomtdata = array();
					$cleadtbledata = array();
					$cleaddata = array();
					if(isset(${'$oacccolmmn0'})) {
						if(count(${'$oacccolmmn0'}) > 0 ) {
							if( strcmp( trim($opportunitypartablename),trim($tblname) ) != 0 ) {
								if($tblname != 'opportunity') {
									$caccdata = ${'$oacccolmmn0'};
									$cleaddata = ${'$ocleadcolmmn0'};
									$cleadtbledata = ${'$ocleadtable0'};
									
									$oppleadcustomtdata = $this->leaddatagetfunctionone($leadid,$cleaddata,$cleadtbledata,$oppleadparentable);
									if(!empty($oppleadcustomtdata)) {
											$cnewdata = array_combine($caccdata,$oppleadcustomtdata);
									} else {
										$cnewdata = array();
									}
									$newdata = array_merge($cnewdata,$defdataarr);
									//default value get
									$newdata['opportunityid'] = $oppid;
									//data insertion
									$this->db->insert( $tblname, array_filter($newdata) );
								}
							}
						}
					}
					$m++;
				}
			} 
		} else { $oppid = ''; }
		$leadfiles = $_POST['convertto'];
		$leadmoduleid = 201;
		if($softwareindustryid == 1){
			$leadmoduleid = 201;
		}else if($softwareindustryid == 2){
			$leadmoduleid = 92;
		}else if($softwareindustryid == 3){
			$leadmoduleid = 201;
		}else if($softwareindustryid == 4){
			$leadmoduleid = 83;
		}else if($softwareindustryid == 5){
			$leadmoduleid = 128;
		}
		if($leadfiles == 1) {
			$commonid = $accid;
			if($softwareindustryid == 1){
				$cmoduleid = '202';
			}else if($softwareindustryid == 2){
				$cmoduleid = '74';
			}else if($softwareindustryid == 3){
				$cmoduleid = '91';
			}else if($softwareindustryid == 4){
				$cmoduleid = '81';
			}else if($softwareindustryid == 5){
				$cmoduleid = '129';
			}			
		} else if($leadfiles == 2) {
			$commonid = $contid;
			if($softwareindustryid == 1){
				$cmoduleid = '203';
			}else if($softwareindustryid == 2){
				$cmoduleid = '75';
			}else if($softwareindustryid == 3){
				$cmoduleid = '203';
			}else if($softwareindustryid == 4){
				$cmoduleid = '82';
			}else if($softwareindustryid == 5){
				$cmoduleid = '130';
			}			
		}  else if($leadfiles == 3) {
			$commonid = $oppid;
			if($softwareindustryid == 1){
				$cmoduleid = '204';
			}else if($softwareindustryid == 2){
				$cmoduleid = '93';
			}else if($softwareindustryid == 3){
				$cmoduleid = '204';
			}else if($softwareindustryid == 4){
				$cmoduleid = '84';
			}else if($softwareindustryid == 5){
				$cmoduleid = '131';
			}			
		}
		//activity conversion
		$convert = array('commonid'=>$commonid,'moduleid'=>$cmoduleid,'lastupdateuserid'=>$userid,'lastupdatedate'=>$date);
		$this->db->where('crmactivity.commonid',$leadid);
		$this->db->where('crmactivity.moduleid',$leadmoduleid);
		$this->db->update('crmactivity',$convert);
		//document conversion
		$this->db->where('crmfileinfo.commonid',$leadid);
		$this->db->where('crmfileinfo.moduleid',$leadmoduleid);
		$this->db->update('crmfileinfo',$convert);
		//task conversion
		$this->db->where('crmtask.commonid',$leadid);
		$this->db->where('crmtask.moduleid',$leadmoduleid);
		$this->db->update('crmtask',$convert);
		//conversation conversion
		$this->db->where('conversation.commonid',$leadid);
		$this->db->where('conversation.moduleid',$leadmoduleid);
		$this->db->update('conversation',$convert); 
		//notification conversion
		$this->db->where('notificationlog.commonid',$leadid);
		$this->db->where('notificationlog.moduleid',$leadmoduleid);
		$this->db->update('notificationlog',$convert); 
		//lead convert data change
		$status = array('status'=>'8','lastupdateuserid'=>$userid,'lastupdatedate'=>$date);
		$this->db->where('lead.leadid',$leadid);
		$this->db->update('lead',$status);
		echo "TRUE";   
	}
	//for account and contact
	public function leaddatagetfunction($leadid,$leaddata){
		$j = 0;
		$data = "";
		$lead = implode(',',$leaddata);
		$count  = count($leaddata);
		$this->db->select($lead);
		$this->db->from('lead');
		$this->db->where('lead.leadid',$leadid);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				for($i=0;$i < $count;$i++){
					$data[$j] = $row->$leaddata[$i];
					$j++;
				}
			}
			return $data;
		}
	}
	//lead conversion data fetch based on the module with fields
	public function leaddatagetfunctionone($leadid,$leaddata,$leadparchitab,$leadparentable){
		$contparchitab = $this->Crudmodel->filtervalue($leadparchitab);
		$leadparentableid = $leadparentable.'id';
		$childdata = explode(',',$contparchitab);
		$j = 0;
		$data = array();
		$leatotdata = array();
		$datacount = count($leadparchitab);
		for($i=0;$i<$datacount;$i++) {
			$leatotdata[$i] = $leadparchitab[$i].'.'.$leaddata[$i];
		} 		
		$lead = implode(',',$leatotdata);
		$count  = count($leaddata);
		$this->db->select($lead);
		$this->db->from($leadparentable);
		for($i=0;$i<count($childdata);$i++) {
			if($childdata[$i] != $leadparentable) {
				$this->db->join($childdata[$i],$childdata[$i].'.'.$leadparentableid.'='.$leadparentable.'.'.$leadparentableid,'left outer');
			}
		}
		for($i=0;$i<count($childdata);$i++) {
			if($childdata[$i] != $leadparentable) {
				if($childdata[$i] == 'leadaddress'){
					$this->db->where('leadaddress.addressmethod=4');
				}
			}
		}
		$this->db->where($leadparentable.'.'.$leadparentableid,$leadid);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				for($i=0;$i < $count;$i++){
					$leaddataa = $leaddata[$i];
					if($row->$leaddataa != '') {
						$data[$j] = $row->$leaddataa;
					} else {
						$data[$j] = '';
					}
					$leaddataa = '';
					$j++;
				}
			}
		}
		return $data;
	}
	//lead address data fetch
	public function leadaddressprimarydatagetfunction($leadid,$lcdata,$cleadsectable){
		$j = 0;
		$data = array();
		$parenttable = 'lead';
		$join = '';
		$leadtable = array_unique($cleadsectable);
		foreach($leadtable as $jointable) {
			if($jointable != $parenttable) {
				if($join == '') {
					$join =' LEFT JOIN '.$jointable.' ON '.$jointable.'.'.$parenttable.'id ='.$parenttable.'.'.$parenttable.'id';
				} else {
					$join .= ' LEFT JOIN '.$jointable.' ON '.$jointable.'.'.$parenttable.'id ='.$parenttable.'.'.$parenttable.'id';
				}
			}
		}
		$leadarrayaddress = implode(',',$lcdata);
		$result = $this->db->query("select ".$leadarrayaddress." from ".$parenttable." ".$join." where ".$parenttable.'.'.$parenttable.'id='.$leadid." AND leadaddress.addressmethod=4");
		$count  = count($lcdata);
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				for($i=0;$i < $count;$i++){
					$lcdataa = $lcdata[$i];
					$data[$j] = $row->$lcdataa;
					$j++;
					$lcdataa = '';
				}
			}
		}
		return $data;
	}
	public function leadaddressscondarydatagetfunction($leadid,$lcdata,$ccleadsectable){
		$j = 0;
		$data = array();
		$count  = count($lcdata);
		$parenttable = 'lead';
		$join = '';
		$leadtable = array_unique($ccleadsectable);
		foreach($leadtable as $jointable) {
			if($jointable != $parenttable) {
				if($join == '') {
					$join =' LEFT JOIN '.$jointable.' ON '.$jointable.'.'.$parenttable.'id ='.$parenttable.'.'.$parenttable.'id';
				} else {
					$join .= ' LEFT JOIN '.$jointable.' ON '.$jointable.'.'.$parenttable.'id ='.$parenttable.'.'.$parenttable.'id';
				}
			}
		}
		$leadarrayaddress = implode(',',$lcdata);
		$result = $this->db->query("select ".$leadarrayaddress." from ".$parenttable." ".$join." where ".$parenttable.'.'.$parenttable.'id='.$leadid." AND leadaddress.addressmethod=5");
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				for($i=0;$i < $count;$i++) {
					$lcdataa = $lcdata[$i];
					$data[$j] = $row->$lcdataa;
					$j++;
					$lcdataa = '';
				}
			}
		}
		return $data;
	}
	//default value set array for add
	public function defaultvalueget() {
		$defdata['createdate'] = date($this->Basefunctions->datef);
		$defdata['lastupdatedate'] = date($this->Basefunctions->datef);
		$defdata['createuserid'] = $this->Basefunctions->userid;
		$defdata['lastupdateuserid'] = $this->Basefunctions->userid;
		$defdata['status'] = 1;
		return $defdata;
	} 
	//employee type id update
	public function employeetypeidupdate($leademptypeid,$table,$accid) {
		$tabid = $table.'id';
		$array = array('employeetypeid'=>$leademptypeid);
		$this->db->where($table.'.'.$tabid,$accid);
		$this->db->update($table,$array);
	}
	//pick list mapping
	public function picklistdatamapping($leadparentable,$auitypee,$leadfiled,$leaddata,$moduleid) {
		$count = count($auitypee);
		for($i=0;$i<$count;$i++) {
			if($auitypee[$i] == '17' || $auitypee[$i] == '18' || $auitypee[$i] == '17') {
				$colvalue = $this->picklistvalue($leadfiled[$i],$leaddata[$i]);
				$id = $this->checklookuptabdata($leadfiled[$i],$colvalue,$auitypee[$i],$moduleid);
				$leaddata[$i] = $id;
			}
		}
		return $leaddata; 
	}
	//pick list data get
	public function picklistvalue($leadfiled,$leaddata) {
		$data = '';
		$table = substr($leadfiled, 0, -2);
		$leadfiledname = $table.'name';
		$this->db->select($leadfiledname);
		$this->db->from($table);
		$this->db->where($leadfiled,$leaddata);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = $row->$leadfiledname;
			}
		}
		return $data;
	}
	//look up table value check
	public function checklookuptabdata($columnname,$colvalue,$uitypeid,$moduleid) {
		$id = 1;
		$tablename = substr($columnname,0,-2);
		$tabid = $tablename.'id';
		$tabfieldname = $tablename.'name';
		if( !is_numeric($colvalue) ) {
			$database = $this->db->database;
			$tabchk = $this->tableexitcheck($database,$tablename);
			if($tabchk!=0) {
				if($uitypeid != '21') {
					$filedchk = "";
					$filedchk = $this->tablefieldnamecheck($tablename,'moduleid');
					if($filedchk == 'false') {
						$result = $this->db->select("$tabfieldname,$tabid")->from($tablename)->where($tablename.'.'.$tabfieldname,$colvalue)->where($tablename.'.status',1)->get();
					} else {
						//$result = $this->db->select("$tabfieldname,$tabid")->from($tablename)->where($tablename.'.'.$tabfieldname,$colvalue)->where($tablename.'.status',1)->where($tablename.'.moduleid',$moduleid)->get();
						$result = $this->db->select("$tabfieldname,$tabid")->from($tablename)->where($tablename.'.'.$tabfieldname,$colvalue)->where($tablename.'.status',1)->where('FIND_IN_SET('.$moduleid.','.$tablename.'.moduleid) > 0')->get();
					}
					if($result->num_rows() > 0) {
						foreach($result->result()as $row) {
							$id=$row->$tabid;
							return $id;
						}
					} else {
						$id = 0;
						return $id;
					}
				} else {
					$id = 0;
					return $id;
				}
			} else {
				if($uitypeid == '21') {
					$id = 0;
					return $id;
				} else {
					return $id;
				}
			}
		} else {
			if($uitypeid == '21') {
				$id = 0;
				return $id;
			} else {
				return $id;
			}
		}
	}
	//table name check
	public function tableexitcheck($database,$newtable) {
		$counttab=0;
		$tabexits = $this->db->query("SELECT COUNT(*) AS tabcount FROM information_schema.tables WHERE table_schema = '".$database."'AND table_name = '".$newtable."'");
		foreach($tabexits->result() as $tkey) { 
			$counttab = $tkey->tabcount;
		};
		return $counttab;
	}
	//table field name check
	public function tablefieldnamecheck($tblname,$fieldname) {
		$result = 'false';
		$fields = $this->db->field_data($tblname);
		foreach ($fields as $field) {
			$name =  $field->name;
			if($fieldname == $name) {
				$result = 'true';
			}
		}
		return $result;
	}
	//lead status check model
	public function leaddatastatusgetmodel() {
		$leadid = $_GET['leadid'];
		$industryid = $_GET['industryid'];
		$softwareindustryid = $this->Basefunctions->industryid;
		if($softwareindustryid == 2){
			$this->db->select('lead.status,leadcf.leadtypeid');
			$this->db->from('lead');
			$this->db->join('leadcf','leadcf.leadid=lead.leadid');
			$this->db->where('lead.leadid',$leadid);
			$this->db->where('lead.industryid',$industryid);
			$this->db->where('lead.status',1);
			$result = $this->db->get();
			if($result->num_rows() > 0){
				foreach($result->result() as $row){
					$status = $row->status;
					$leadtype = $row->leadtypeid;
				}
				echo json_encode(array("status"=>$status,"leadtype"=>$leadtype));
			} else {
				echo json_encode(array("fail"=>'FAILED'));
			}
		}else{
			$this->db->select('status');
			$this->db->from('lead');
			$this->db->where('lead.leadid',$leadid);
			$this->db->where('lead.industryid',$industryid);
			$this->db->where('lead.status',1);
			$result = $this->db->get();
			if($result->num_rows() > 0){
				foreach($result->result() as $row){
					$data = $row->status;
				}
				echo json_encode($data);
			} else {
				echo json_encode(array("fail"=>'FAILED'));
			}
		}
		
	}	
}