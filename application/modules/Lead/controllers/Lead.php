<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class lead extends MX_Controller {
	// constructor function 
    public function __construct() {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
        $this->load->model('Lead/Leadmodel');
    }
	//To load the Register View
	public function index() {
		$moduleid = array(83,92,201,128);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view creation
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['stage']=$this->Basefunctions->simpledropdownwithcond('crmstatusid','crmstatusname','crmstatus','moduleid','204');
		$data['dashboradviewdata'] = $this->Basefunctions->dashboarddataviewbydropdown($moduleid);
		$this->load->view('Lead/leadcreationview',$data);
    }
	//Create New data
	public function newdatacreate() { 
    	$this->Leadmodel->newdatacreatemodel();
    }
	//information fetch
	public function fetchformdataeditdetails() {
		$moduleid = array(83,92,201,128);
		$this->Leadmodel->informationfetchmodel($moduleid);
	}
	//Update old data
    public function datainformationupdate() {
        $this->Leadmodel->datainformationupdatemodel();
    }
	//delete old information
    public function deleteinformationdata() {
		$moduleid = array(83,92,201,128);
        $this->Leadmodel->deleteoldinformation($moduleid);
    } 
	//denied in convert after convert lead
    public function ledconversiondenied() {
		$moduleid = array(83,92,201,128);
        $this->Leadmodel->ledconversiondeniedmodel($moduleid);
    } 
	//lead -clone function
	public function cloneleadinfo() {
        $this->Leadmodel->cloneleadinfomodel();
    }
	//primary address value fetch
	public function primaryaddressvalfetch() {
		$this->Leadmodel->primaryaddressvalfetchmodel();
	}
	//lead data fetch for convert
	public function leaddatafetch()	{
		$this->Leadmodel->leaddatafetchmodel();
	}
	//lead convert
	public function leadconvertfunction() {
		$this->Leadmodel->leadconvertfunctionmodel();
	}
	//lead status check
	public function leaddatastatusget() {
		$this->Leadmodel->leaddatastatusgetmodel();
	}
	//clone data
	public function viewdataclone() {
		$empid = $this->Basefunctions->userid;
		$vmaxid = $_POST['rowid'];
		$parenttable = 'opportunity';
		$childtabinfo = '';
		$fieldinfo = 'opportunityid';
		$fieldvalue = $vmaxid;
		$updatecloumn = 'opportunityname';
		$viewid = $this->Basefunctions->viewdataclonemodel($parenttable,$childtabinfo,$fieldinfo,$fieldvalue);
		if($updatecloumn!='') {
			$this->db->query(' UPDATE '.$parenttable.' SET '.$updatecloumn.' = concat('.$updatecloumn.'," clone") where '.$fieldinfo.' = '.$viewid.'');
		}
		echo 'TRUE';
	}
}