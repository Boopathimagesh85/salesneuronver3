<!--sms icon overlay -->
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay alertsoverlay overlayalerts" id="mobilenumbershow" style="overflow-y: scroll;overflow-x: hidden;">
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="large-3 medium-6 large-centered medium-centered columns paddingzero " >
				<form method="POST" name="convertoverlayform" style="" id="convertoverlayform" action="" enctype="" class="clearconvertoverlayform">
					<div class="alert-panel">
						<div class="alertmessagearea">
							<div class="alert-title">Select Mobile Number</div>
							<div class="alert-message">
								<span class="firsttab" tabindex="1000"></span>
								<div id="smsmoduledivhid" class="static-field overlayfield">  
									<label>Module<span class="mandatoryfildclass">*</span></label>
									<select class="chzn-select validate[required] ffieldd" data-placeholder="Select" data-prompt-position="topLeft:14,36" id="smsmodule" name="smsmodule" tabindex="1001">
										<option value="">Select</option>
									</select>
								</div>
								<div class="row">&nbsp;</div>
								<div class="static-field overlayfield">  
									<label>Mobile Number<span class="mandatoryfildclass">*</span></label>
									<select class="chzn-select validate[required]" multiple data-placeholder="Select" data-prompt-position="topLeft:14,36" id="smsmobilenumid" name="smsmobilenumid" tabindex="1002">
										<option value="">Select</option>
									</select>
								</div>
								<input type="hidden" name="smsrecordid" id="smsrecordid" />
								<input type="hidden" name="smsmoduleid" id="smsmoduleid" />
								<input type="hidden" name="smsmobilenum" id="smsmobilenum" />
							</div>
						</div>
						<div class="alertbuttonarea">
							<input type="button" id="mobilenumbersubmit" name="mobilenumbersubmit" value="Submit" tabindex="1003" class="alertbtnyes" >
							<input type="button" id="mobilenumberoverlayclose" name="" value="Cancel" tabindex="1004" class="alertbtnno flloop  alertsoverlaybtn" >
							<span class="lasttab" tabindex="1005"></span>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Call icon Overlay -->
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay alertsoverlay overlayalerts" id="c2cmobileoverlay" style="overflow-y: scroll;overflow-x: hidden;">
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="large-3 medium-6 large-centered medium-centered columns paddingzero" >
				<form method="POST" name="c2cmobileoverlayform" style="" id="c2cmobileoverlayform" action="" enctype="" class="clearconvertoverlayform">
					<div class="alert-panel">
						<div class="alertmessagearea">
							<div class="alert-title">Select Mobile Number</div>
							<div class="alert-message">
								<span class="firsttab" tabindex="1000"></span>
								<div id="callmoduledivhid" class="static-field overlayfield">  
									<label>Module<span class="mandatoryfildclass">*</span></label>
									<select class="chzn-select validate[required] ffieldd" data-placeholder="Select" data-prompt-position="topLeft:14,36" id="callmodule" name="callmodule" tabindex="1001">
										<option value="">Select</option>
									</select>
								</div>
								<div class="static-field overlayfield">  
									<label>Mobile Number<span class="mandatoryfildclass">*</span></label>
									<select class="chzn-select validate[required]" data-placeholder="Select" data-prompt-position="topLeft:14,36" id="callmobilenum" name="callmobilenum" tabindex="1002">
										<option value="">Select</option>
									</select>
								</div>
								<input type="hidden" name="callrecordid" id="callrecordid" />
								<input type="hidden" name="callmoduleid" id="callmoduleid" />
								<input type="hidden" name="calcount" id="calcount" />
							</div>
						</div>
						<div class="alertbuttonarea">
							<input type="button" id="callnumbersubmit" name="callnumbersubmit" value="Submit" tabindex="1003" class="alertbtnyes" >
							<input type="button" id="c2cmobileoverlayclose" name="" value="Cancel" tabindex="1004" class="alertbtnno flloop  alertsoverlaybtn" >
							<span class="lasttab" tabindex="1005"></span>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>