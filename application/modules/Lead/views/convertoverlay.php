<!-- Lead Convert Overlay -->
<!-- For Date Formatter -->
<?php 
$CI =& get_instance();
$accmoduleid = array(202,74,81,91,129);
$contmoduleid = array(203,75,82,130);
$oppmoduleid = array(84,93,204,131);
$accountid = $CI->Basefunctions->getmoduleid($accmoduleid);
$contactid = $CI->Basefunctions->getmoduleid($contmoduleid);
$opportunityid = $CI->Basefunctions->getmoduleid($oppmoduleid);
$accountname = $CI->Basefunctions->generalinformaion('module','menuname','moduleid',$accountid);
$contactname = $CI->Basefunctions->generalinformaion('module','menuname','moduleid',$contactid);
$opportunityname = $CI->Basefunctions->generalinformaion('module','menuname','moduleid',$opportunityid);
$appdateformat = $CI->Basefunctions->appdateformatfetch(); ?>
<div class="large-12 columns"  style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="convertleadovrelay" style="overlfow-x:hidden;overflow-y:auto;">
		<div class="row sectiontoppadding" style="padding-top:80px;">&nbsp;</div>
		<div class="">
		<div class="large-4 medium-6 large-centered medium-centered columns" >
			<form method="POST" name="leadconvertoverlayform" style="" id="leadconvertoverlayform" action="" enctype="" class="clearconvertoverlayform">
				<span id="convertoverlayvalidation" class="validationEngineContainer"> 
					<div class="row cleardataform borderstyle" style="">
						<div class="large-12 columns sectionheaderformcaptionstyle">
							<div class="large-12 columns" style="background:none">Lead Convert</div>
						</div>
						<div class="large-12 columns sectionpanel" style="max-height: 400px;overflow-x: hidden;overflow-y: scroll;">
						<div id="convertaccount" class="large-12 medium-12 columns small-12 paddingzero">  
							<div class="input-field large-6 medium-6 columns small-6 centertext">				
								<input type="checkbox" class="filled-in" checked id="acccheck" name="acccheck" value="yes" />
								<label for="acccheck" style="margin-top:0;"><?php echo $accountname;?></label>
							</div>
							<div class="input-field large-6 medium-6 columns small-6">
								<input type="text" id="accname" class="validate[required,funcCall[accountnamecheck]]" maxlength="100" name="accname" value="" />
								<label for="accname" id="accnamemanfield"><?php echo $accountname;?> Name <span class="mandatoryfildclass">*</span></label>
							</div>
						</div>
						<div id="convertcontact" class="large-12 medium-12 columns small-12 paddingzero">  
							<div class="input-field large-6 medium-6 columns small-6 centertext">
								<input type="checkbox" class="filled-in" checked id="contcheck" name="contcheck" value="yes" />
								<label for="contcheck" style="margin-top:0;"><?php echo $contactname;?></label>			
							</div>
							<div class="input-field large-6 medium-6 columns small-6 ">
								<input type="text" id="contname" class="validate[required]" maxlength="100" name="contname" value="" />
								<label for="contname" id="connamemanfield"><?php echo $contactname;?> Name <span class="mandatoryfildclass">*</span></label>
							</div>
						</div>
						<div class="large-12 medium-12 columns small-12 paddingzero">  
							<div class="input-field large-6 medium-6 columns small-7 centertext">
								<input type="checkbox" id="opportunitycheckbox" class="filled-in" name="opportunitycheckbox" value="no" />
								<label for="opportunitycheckbox" style="margin-top:0;"><?php echo $opportunityname;?></label>
							</div>
						</div>
						<div class="large-12 columns" style="padding-top:20px;">&nbsp;</div>
						<div id="opportunitydetailsdiv" class="hidedisplay large-12 columns">
							<div class="large-12 medium-12 columns small-12 paddingzero">  
								<div class="input-field large-6 medium-6 small-6 columns">
									<input type="text" id="oppname" class="validate[required,funcCall[opportunitynamecheck]]" maxlength="100" name="oppname" value="" />
									<label for="oppname" id="oppnamemanfield"><?php echo $opportunityname;?> Name <span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="static-field large-6 medium-6 small-6 columns">
									<label>Close Date <span class="mandatoryfildclass">*</span></label>
									<input type="text" id="closedate" data-dateformater="<?php echo $appdateformat; ?>" class="fordatepicicon validate[required]" name="closedate" value="" />
									
								</div>
							</div>	
							<div class="large-12 medium-12 columns small-12 paddingzero">  
								<div class="static-field large-6 medium-6 small-6 columns">             
									<label>Stage <span class="mandatoryfildclass">*</span></label>
									<select data-placeholder="Select" data-prompt-position="bottomLeft:0,35" class="chzn-select dropdownchange validate[required]" tabindex="2" name="stage" id="stage">
									<option value=''>Select</option>
									<?php foreach($stage as $key):?>
										<option value="<?php echo $key->crmstatusid;?>" >
										<?php echo $key->crmstatusname;?></option>
									<?php endforeach;?>	
									</select>                  
								</div>							
								<div class="input-field large-6 medium-6 small-6 columns">
									<input type="text" id="amount" class="validate[required,custom[number]]" name="amount" value="" maxlength="15" />
									<label for="amount">Amount <span class="mandatoryfildclass">*</span></label>
								</div>
							</div>	
							<div class="large-12 medium-12 columns small-12 paddingzero">  
								<div class="input-field large-12 medium-12 small-12 columns">
									<textarea name="nextstep" class="materialize-textarea" id="nextstep" rows="3" cols="40" tabindex="125" maxlength="200" ></textarea>
									<label for="nextstep">Next Step</label>									
									<input type="hidden" id="convertid" name="convertid" value="" />             
								</div>
							</div>
						</div>
						<div class="large-12 columns">
						<div class="static-field large-12 columns">             
							<label>Transfer Related Lead record To <span class="mandatoryfildclass">*</span></label>
							<select data-placeholder="Select" data-prompt-position="bottomLeft:0,35" class="chzn-select dropdownchange validate[required]" tabindex="2" name="convertto" id="convertto">
								<option value=''></option>
							</select>          
						</div>
						</div>
						<input type="hidden" name="elementsconvertname" id="elementsconvertname" />
						<input type="hidden" name="elementsconverttable" id="elementsconverttable" />
						<input type="hidden" name="elementsconvertcolmn" id="elementsconvertcolmn" />
						<input type="hidden" name="elementsconvertpartabname" id="elementsconvertpartabname" />
						</div>
						<div class="divider large-12 columns"></div>
						<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">
							<input type="button" id="convertsbtbtn" name="convertsbtbtn" value="Submit" class="alertbtnyes" >	
							<input type="button" id="leadconvertoverlayclose" name="leadconvertoverlayclose" value="Cancel" class="alertbtnno" >	
						</div>
					</div>
				</span>
			</form>
		</div>
		</div>
	</div>
</div>