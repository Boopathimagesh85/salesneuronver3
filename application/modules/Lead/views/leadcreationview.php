<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
	<?php 
		$industryid = $this->Basefunctions->industryid;
		if($industryid == 3) {
	?>
			<link href="<?php echo base_url();?>css/jqgrid.min.css" media="screen" rel="stylesheet" type="text/css" />
	<?php 
		}
	?>
</head>
<body class="">
	<?php
		$moduleid = implode(',',$moduleids);
		$device = $this->Basefunctions->deviceinfo();
		$dataset['gridenable'] = "yes"; //no
		$dataset['griddisplayid'] = "leadcreationview";
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['spanattr'] = array();
		$dataset['gridtableid'] = "leadcreationviewgrid";
		$dataset['griddivid'] = "leadcreationviewgriddiv";
		$dataset['moduleid'] = $moduleids;
		$dataset['forminfo'] = array(array('id'=>'leadcreationformadd','class'=>'hidedisplay','formname'=>'leadcreationform'));
		if($device=='phone') {
			$this->load->view('Base/gridmenuheadermobile',$dataset);
			$this->load->view('Base/overlaymobile');
			$this->load->view('Base/viewselectionoverlay');
		} else {
			$this->load->view('Base/gridmenuheader',$dataset);
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/basedeleteform');
		$this->load->view('convertoverlay');
		$this->load->view('leftpaneloverlay');
		$this->load->view('clicktocall');
		$this->load->view('Base/modulelist');
	?>
</body>
<?php $this->load->view('Base/bottomscript'); ?>
<!-- js File -->
<script src="<?php echo base_url();?>js/Lead/lead.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/plugins/uploadfile/jquery.uploadfile.min.js"></script>
<!--For Tablet and Mobile view Dropdown Script-->
<script>
	$(document).ready(function(){
		$("#tabgropdropdown").change(function(){
			var tabgpid = $("#tabgropdropdown").val(); 
			$('.sidebaricons[data-subform="'+tabgpid+'"]').trigger('click');
		});
	});
</script>
</html>