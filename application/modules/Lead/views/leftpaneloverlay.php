<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="leftpaneloverlay">
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-4 large-centered columns">
			<div class="row" style="background:#f5f5f5">
				<div class="large-12 alertsheadercaptionstyle columns" style="background:#546E7A">
					<div class="small-12 columns">
						<span style="color:#ffffff;padding-right:0.5rem"><i class="material-icons">check_box</i> </span>
							Confirmation
					</div>
				</div>
				<div class="large-12  columns"style="background:#f5f5f5">&nbsp;</div>
				<div class="large-12  columns"style="background:#f5f5f5">&nbsp;</div>
				<div class="large-12 large-centered columns" style="background:#f5f5f5;text-align:center">
					<span class="alertinputstyle">Current data is unsaved do you wish to continue ?</span>
					<div class="large-12 columns">&nbsp;</div>
					<div class="large-12 columns">&nbsp;</div>
				</div>
			</div>
			<div class="row" style="background:#f5f5f5">
			    <div class="large-12 columns">
					<div class="medium-2 large-2 columns">&nbsp;</div>
					<div class="large-4 small-6 medium-4 columns">
					<input  class="btn formbuttonsalert" id="" type="button"  value="Yes" name="" >
					</div>
					<div class="large-4 small-6 medium-4  columns">
					<input  class="btn formbuttonsalert" id="" type="button"  value="No" name="" >
					</div>
					<div class="medium-2 large-2 columns">&nbsp;</div>
				</div>
					<div class="large-12 columns">&nbsp;</div>
					<div class="large-12 columns">&nbsp;</div>
			</div>
		</div>
	</div>
 </div>