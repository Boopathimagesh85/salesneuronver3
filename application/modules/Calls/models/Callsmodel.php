<?php
Class Callsmodel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->model('Base/Crudmodel');
    }
	//dial street - inbound(missed-call and click-to-call)/outbound details fetch
	public function callsdetailsfetchmodel() {
		$apikey = 'e6eecfd00a34807749809bcdd3f09348';
		$date = date($this->Basefunctions->datef);
		$url = "http://api.dialstreet.com/v1/?api_key=e6eecfd00a34807749809bcdd3f09348&method=dial&output=xml";
		$ch=curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$output=curl_exec($ch);
		curl_close($ch);
		$dataset = $this->parse_response($output);
		for($i=0;$i<count($dataset);$i++) {	
			if($dataset[$i]['provider'] != '') {
				$type = 'Incoming Call';
			} else {
				$type = 'Click To Call';
			}
			$array[$i]= array(
				'calltype'=>$type,
				'caller'=>$dataset[$i]['callfrom'],
				'tonumber'=>$dataset[$i]['callto'],
				'billseconds'=>$dataset[$i]['billsec'],
				'location'=>$dataset[$i]['location'],
				'provider'=>$dataset[$i]['provider'],
				'credits'=>$dataset[$i]['credits'],
				'uniqid'=>$dataset[$i]['uniqid'],
				'callstatus'=>$dataset[$i]['status'],
				'duration'=>$dataset[$i]['duration'],
				'calltime'=>$dataset[$i]['date'],
				'createdate'=>$date,
				'lastupdatedate'=>$date,
				'createuserid'=>$this->Basefunctions->userid,
				'lastupdateuserid'=>$this->Basefunctions->userid,
				'status'=>$this->Basefunctions->activestatus
			);
		} 
		$this->db->insert_batch('calldetails',$array);
	}
	//fetch xml content
	public function parse_response($response) {
		$xml = new SimpleXMLElement($response);
		$result = array();
		$h=0;
		foreach ($xml->data->dial as $element) {
		  foreach($element as $key => $val) {
				$result[$h][(string)$key] = (string)$val;
			}
			$h++;
		}
		return $result;
	}
}	
?>