<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Calls extends CI_Controller {
	function __construct() {
    	parent::__construct();	
		$this->load->model('Calls/Callsmodel');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Base/Basefunctions');
    }
	public function index() {
		$moduleid = array(18);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view creation
		$viewfieldsmoduleids = array(18);
		$viewmoduleid = array(18);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dashboradviewdata'] = $this->Basefunctions->dashboarddataviewbydropdown($moduleid);
		$this->load->view('Calls/callsview',$data );
	}
	//calls details fetch from dial street
	public function callsdetailsfetch() {
		$this->Callsmodel->callsdetailsfetchmodel();
	}
}