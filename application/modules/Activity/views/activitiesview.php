<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Base Header File -->
	<!-- To Load Date & Time Picker form fields placed above (DOB Format)-->
	<?php $this->load->view('Base/headerfiles'); ?>
	
</head>
<body>
	<?php
		$moduleid = implode(',',$moduleids);
		$device = $this->Basefunctions->deviceinfo();
		$dataset['gridenable'] = "yes"; //no
		$dataset['griddisplayid'] = "activitycreationview";
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['spanattr'] = array();
		$dataset['gridtableid'] = "activityaddgrid";
		$dataset['griddivid'] = "activityaddgridnav";
		$dataset['moduleid'] = $moduleids;
		$dataset['forminfo'] = array(array('id'=>'activitycreationaddform','class'=>'hidedisplay','formname'=>'activitiesaddform'));
		if($device=='phone') {
			$this->load->view('Base/gridmenuheadermobile',$dataset);
			$this->load->view('Base/overlaymobile');
			$this->load->view('Base/viewselectionoverlay');
		} else {
			$this->load->view('Base/gridmenuheader',$dataset);
			$this->load->view('Base/overlay');
		}
		$this->load->view('activitymobileandemailidvalidationoverlay');
		$this->load->view('activityoverlayset');
		$this->load->view('Base/basedeleteform');
		$this->load->view('Base/modulelist');
	?>
	
<!--View Creation Js-->
<?php $this->load->view('Base/bottomscript'); ?>
<script src="<?php echo base_url();?>js/plugins/Developer/timepicker/jquery.timepicker.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/Activity/activity.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/plugins/uploadfile/jquery.uploadfile.min.js"></script>
<!--For Tablet and Mobile view Dropdown Script-->
<script>
$(document).ready(function() {
	$("#tabgropdropdown").change(function(){
		var tabgpid = $("#tabgropdropdown").val(); 
		$('.sidebaricons[data-subform="'+tabgpid+'"]').trigger('click');
	});
});
</script>
</body>
</html>