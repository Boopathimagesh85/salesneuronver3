<style type="text/css">
.innergridpaddingbtm {
	top:20px !important;
}
</style>
<div class="large-12 columns paddingzero formheader">
	<?php
		$this->load->view('Base/mainviewaddformheader');
	?>
</div>
<div class="large-12 columns addformunderheader">&nbsp;</div>
<div class="large-12 columns scrollbarclass addformcontainer tabtouchaddcontainer">
	<div class="row mblhidedisplay">&nbsp;</div>
	<form method="POST" name="dataaddform" class="" action ="" id="dataaddform" style="padding-left:0px !important;">
		<span id="formaddwizard" class="validationEngineContainer" >
			<span id="formeditwizard" class="validationEngineContainer">
				<?php
					//function call for form fields generation
					$innergridtype = 'form';
					formfieldstemplategenerator($modtabgrp,'',$innergridtype);
				?>
			</span>
		</span>
		<!--hidden values-->
		<?php
			echo hidden('primarydataid');
			echo hidden('sortorder','');
			echo hidden('sortcolumn','');
			echo hidden('repeatonmultipleid');
			echo hidden('remindbeforeid');
			echo hidden('invitetypeid');
			echo hidden('invitemobilenumber');
			echo hidden('inviteemailid');
			echo hidden('remindersmstemplatevalue');
			echo hidden('remindermailtemplatevalue');
			echo hidden('invitesmstemplatevalue');
			echo hidden('invitemailtemplatevalue');
			$value = 'crmrecurrence';
			echo hidden('resctable',$value);
			//hidden text box view columns field module ids
			$modid = $this->Basefunctions->getmoduleid($filedmodids);
			echo hidden('viewfieldids',$modid);
		?>
	</form>
	<?php
		
	?>
</div>