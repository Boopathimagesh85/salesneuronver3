<!-- Validate Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="activtymobemailvalidateovrelay" >		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="alert-panel">
			<div class="alertmessagearea">
				<div class="alert-title">Confirmation</div>
				<div class="alert-message">
					<span class="firsttab" tabindex="1000"></span>
					<div id="activitymobileshow" class="input-field">
						<textarea id="withoutmobilenum" class="ffield materialize-textarea" readonly="readonly" data-prompt-position="topLeft"  tabindex="1001" name="description"></textarea>
						<label for="withoutmobilenum">Invite Users Without Mobile Number</label>
					</div>
					<div id="activitymailshow" class="input-field">
						<textarea id="withoutmailid" class="materialize-textarea" readonly="readonly" data-prompt-position="topLeft" tabindex="1002" name="description"></textarea>
						<label for="withoutmailid">Invite Users Without EMail</label>
					</div>
					<div class="alertbuttonarea">
						<input type="button" id="actmobileemailcreate" name="actmobileemailcreate" tabindex="1003" value="Create" class="alertbtn" >
						<input type="button" id="actmobileemailupdate" name="actmobileemailupdate" value="SUBMIT" tabindex="1004" class="alertbtn   alertsoverlaybtn" >
						<input type="button" id="actmobileemailcancel" name="actmobileemailcancel" value="Cancel" tabindex="1005" class="alertbtn flloop  alertsoverlaybtn" >
					</div>
					<span class="lasttab" tabindex="1006"></span>
				</div>
			</div>
		</div>
	</div>
</div>