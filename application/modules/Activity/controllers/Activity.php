<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Activity extends MX_Controller{
	//To load the Register View
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Activity/Activitiesmodel');
    }
	public function index() {
		$moduleid = array(205,76,132);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['dashboradviewdata'] = $this->Basefunctions->dashboarddataviewbydropdown($moduleid);
		$this->load->view('Activity/activitiesview',$data);
	}
	//Create New data
	public function newdatacreate() {  
    	$this->Activitiesmodel->newdatacreatemodel();
    }
	//information fetch
	public function fetchformdataeditdetails() {
		$moduleid = array(205,76,132);
		$this->Activitiesmodel->informationfetchmodel($moduleid);
	}
	//Update old data
    public function datainformationupdate() {
        $this->Activitiesmodel->datainformationupdatemodel();
    }
	//delete old information
    public function deleteinformationdata() {
		$moduleid = array(205,76,132);
        $this->Activitiesmodel->deleteoldinformation($moduleid);
    } 
	//used for fetch the module in data bases
	public function dropdownvaluefecth() {
        $this->Activitiesmodel->dropdownvaluefecthmodel();
    }
	//Editor value fetch 	
	public function editervaluefetch() {
		$filename = $_GET['filename']; 
		$newfilename = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]);
			echo json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			echo json_encode($tccontent);
		}
	}
	//Editor value fetch - mail templates	
	public function maileditervaluefetch() {
		$filename = $_GET['filename']; 
		$newfilename = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]);
			echo json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			echo json_encode($tccontent);
		}
	}
	//activity validation
	public function activityvalidation() {
		$this->Activitiesmodel->activityvalidationmodel();
	}
	//module drop down load function
	public function moduledropdownloadfun() {
		$this->Activitiesmodel->moduledropdownloadfunmodel();
	}
	//View drop down load function
	public function viewdropdownloadfun() {
		$this->Activitiesmodel->viewdropdownloadfunmodel();
	}
	//parent table get
	public function parenttableget() {
		$this->Activitiesmodel->parenttablegetmodel();
	}
	//invite user grid data fetch
	public function inviteusergrigdatafetch() {
		$creationid = $_GET['viewid'];
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$viewcolmoduleids = $_GET['moduleid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$filter = '';
		$chkbox = $_GET['checkbox'];
		$footer = isset($_GET['footername']) ? $_GET['footername'] : '';
		$sortcol = isset($_GET['sortcol']) ? $_GET['sortcol'] : '';
		$sortord = isset($_GET['sortord']) ? $_GET['sortord'] : '';
		$result = $this->Basefunctions->viewdynamicdatainfofetch($creationid,$primaryid,$viewcolmoduleids,$pagenum,$rowscount,$sortcol,$sortord,$filter);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobileviewgriddatagenerate($result,$primarytable,$primaryid,$width,$height,$chkbox);
		} else {
			$datas = viewgriddatagenerate($result,$primarytable,$primaryid,$width,$height,$footer,$chkbox);
		}
		echo json_encode($datas);
	}
	//mobile number and email id check  - validation
	public function mobileandmailvalidation() {
		$this->Activitiesmodel->mobileandmailvalidationmodel();
	}
	//reminder validation
	public function reminderintervalcheck() {
		$this->Activitiesmodel->reminderintervalchecknmodel();
	}
	//recurrence onday data fetch
	public function recurrecneonday() {
		$this->Activitiesmodel->recurrecneondaynmodel();
	}
	//record name fetch
	public function recordnamefetch(){
		$this->Activitiesmodel->recordnamefetchmodel();
	}
	//template value fetch
	public function templateddvalfetch() {
		$this->Activitiesmodel->templateddvalfetchmodel();
	}
	//template val fetch on update
	public function templatenamefetch() {
		$this->Activitiesmodel->templatenamefetchmodel();
	}
	//insert google event id
	public function insertgoogleeventid() {
		$this->Activitiesmodel->insertgoogleeventidmodel();
	}
	//insert google event id
	public function insertgoogleeventidsingle() {
		$this->Activitiesmodel->insertgoogleeventidsinglemodel();
	}
	/*----- For industry ----*/
	public function getmobileandemailid() {
		$this->Activitiesmodel->getmobileandemailidmodel();
	}
	//to set template dropdown on edit
	public function gettemplatesid() {
		$this->Activitiesmodel->gettemplatesidmodel();
	}
	//get mobile number based on the name
	public function getmobilenumberemailid(){
		$this->Activitiesmodel->getmobilenumberemailidmodel();
	}
}