<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Model File
class Activitiesmodel extends CI_Model{    
    public function __construct() {
		parent::__construct(); 
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
		$this->load->model('Reportsview/Reportsmodel');
    }
	//To Create New activity Details
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$empid = $this->Basefunctions->logemployeeid;
		//activity data set
		$startdate = $_POST['activitystartdate'];
		$enddate = $_POST['activityenddate'];
		$count = 0;
		if(isset($_POST['actvitycommonid'])) {
			$commonid = $_POST['actvitycommonid'];
			if($commonid == ''){
				$commonid = 1;
			}
		} else { 
			$commonid = 1;
		}
		if(isset($_POST['actvitymoduleid'])) {
			$moduleid = $_POST['actvitymoduleid'];
			if($moduleid == '') {
				$moduleid = 1;
			}
		} else {
			$moduleid =1; 
		}
		$crmactivityname = $_POST['crmactivityname'];
		if(isset($_POST['activitytypeid'])) {
			$activitytypeid =$_POST['activitytypeid'];
			if($activitytypeid == ''){
				$activitytypeid = 1;
			}
		} else { 
			$activitytypeid =1; 
		}
		if(isset($_POST['actvitypriorityid'])){
			$priorityid = $_POST['actvitypriorityid'];
			if($priorityid == '') {
				$priorityid = 1;
			}
		} else { 
			$priorityid = 1; 
		}
		$emptypes = $_POST['employeetypeid'];
		if(isset($_POST['employeeid'])){
			$employeeid = $_POST['employeeid'];
			if($employeeid == '') {
				$employeeid = 1;
			}
		} else { 
			$employeeid = 1; 
		}
		if(isset($_POST['crmstatusid'])) {
			$leadstatusid = $_POST['crmstatusid'];
			if($leadstatusid == '') {
				$leadstatusid = 1;
			}
		} else { 
			$leadstatusid =1; 
		}
		if(isset($_POST['actvitycontactid'])){
			$contactid = $_POST['actvitycontactid'];
			if($contactid == '') {
				$contactid = 1;
			}
		} else { 
			$contactid =1; 
		}
		if(isset($_POST['location'])){
			$location = $_POST['location'];
		} else { 
			$location =''; 
		}
		if(isset($_POST['actvitydescription'])){
			$description = $_POST['actvitydescription'];
		} else { 
			$description =''; 
		}
		$activitystartdate = $startdate;
		$activityenddate = $_POST['activityenddate'];
		if(isset($_POST['activityinviteusers'])){
			$inviteusers = $_POST['activityinviteusers'];
			if($inviteusers == '') {
				$inviteusers = 1;
			}
		} else { 
			$inviteusers =1; 
		}
		if(isset($_POST['remindermodeid'])){
			$remmode = $_POST['remindermodeid'];
			$remindermode = implode(',',$remmode);
			if($remindermode == '') {
				$remindermode = 1;
			}
		} else { 
			$remindermode = 1; 
		}
		//invite mode ids
		if(isset($_POST['activityremindermodeid'])){
			$inviteremmode = $_POST['activityremindermodeid'];
			$inviteremindermode = implode(',',$inviteremmode);
			if($inviteremindermode == '') {
				$inviteremindermode = 1;
			}
		} else {
			$inviteremindermode = 1;
		}
		if(isset($_POST['activityreminderintervalid'])){
			$reminderinreval = $_POST['remindbeforeid'];
			if($reminderinreval == '') {
				$reminderinreval = 1;
			}
		} else { 
			$reminderinreval = 1; 
		}
		//reminder template update
		if(isset($_POST['reminderleadtemplateid'])){
			$remindersmstempid = $_POST['reminderleadtemplateid'];
			if($remindersmstempid != '') {
				$remsmstempid = $remindersmstempid;
			} else { 
				$remsmstempid = 1; 
			}
		} else { 
			$remsmstempid = 1;
		}
		if(isset($_POST['reminderemailtemplatesid'])){
			$reminderemailtempid = $_POST['reminderemailtemplatesid'];
			if($reminderemailtempid != '') {
				$rememailtempid = $reminderemailtempid;
			} else { 
				$rememailtempid = 1; 
			}
		} else {
			$rememailtempid = 1; 
		}
		$cronnotification = 0;
		$cronstatus = 1;
		$assignid = $_POST['employeeid'];
		$emptypes = $_POST['employeetypeid'];
		$starttime = $_POST['activitystarttime'];
		$endtime = $_POST['activityendtime'];
		$userid = $this->Basefunctions->userid;
		$cdate = date($this->Basefunctions->datef);
		//recurrence check
		$restricttable = explode(',',$_POST['resctable']);
		if(isset($_POST['recurrence'])){
			$recurrence = $_POST['recurrence'];
		} else {
			$recurrence = 'No';
		}
		if($recurrence != 'No') {
			$recurrencefreqid = $_POST['activityrecurrencefreqid'];
			if(isset($_POST['activityrecurrenceontheid'])) {
				$crmrecurrenceontheday = $_POST['activityrecurrenceontheid'];
				if($crmrecurrenceontheday != "") {
					$crmrecurrenceontheday  = $crmrecurrenceontheday;
				} else {
					$crmrecurrenceontheday = 2;
				}
			} else { 
				$crmrecurrenceontheday = 2; 
			}
			if(isset($_POST['activityrecurrenceontheid'])) {
				$crmrecurrencedayid = $_POST['repeatonmultipleid'];
				if($crmrecurrencedayid != "") {
					$crmrecurrencedayid  = $crmrecurrencedayid;
				} else {
					$crmrecurrencedayid = 1;
				}
			} else { 
				$crmrecurrencedayid = 1; 
			}
			if(isset($_POST['activityrecurrenceendsid'])) {
				$activityrecurrenceendsid = $_POST['activityrecurrenceendsid'];
				if($activityrecurrenceendsid != "") {
					$activityrecurrenceendsid  = $activityrecurrenceendsid;
				} else {
					$activityrecurrenceendsid = 1;
				}
			} else { 
				$activityrecurrenceendsid = 1; 
			}
			if(isset($_POST['activityafteroccurences'])){
				$activityafteroccurences = $_POST['activityafteroccurences'];
			} else { 
				$activityafteroccurences = 1; 
			}
			$activityrecurrencestartdate = $_POST['activitystartdate'];
			if(isset($_POST['activityrecurrenceuntil'])) {
				$crmrecurrenceuntil = $_POST['activityrecurrenceuntil'];
			} else {
				$crmrecurrenceuntil = '';
			}
			$crmrecurrenceeveryday = $_POST['activityrecurrenceeveryday'];
			//for recurrence activity
			$recurrenceendsid = $_POST['activityrecurrenceendsid'];
			$startdate = $_POST['activitystartdate'];
			$enddate = $_POST['activityrecurrenceuntil'];
			if($recurrenceendsid == '3' || $recurrenceendsid == '2') {
				$enddate = $_POST['activityenddate'];
			} else if($recurrenceendsid == '4') {
				$enddate = $_POST['activityrecurrenceuntil'];
			}
			$datetime1 = date_create($startdate);
			$datetime2 = date_create($enddate);
			if($recurrencefreqid == '14') { // For daily
				if($recurrenceendsid == '4') {
					$count = $datetime1->diff($datetime2)->days;
				} else if($recurrenceendsid == '3') {
					$datecount = $datetime1->diff($datetime2)->days;
					$count = $activityafteroccurences;
				} else if($recurrenceendsid == '2') {
					$day = 730;
					$sdate = strtotime("+".$day." day", strtotime($startdate));
					$enddate = date("d-m-Y", $sdate);
					$datetime2 = date_create($enddate);
					$count = $datetime1->diff($datetime2)->days;
				}
			} else if($recurrencefreqid == '15') { //for weekly
				if($recurrenceendsid == '2') {
					$day = 730;
					$sdate = strtotime("+".$day." day", strtotime($startdate));
					$enddate = date("d-m-Y", $sdate);
					$count = 1;
				} else if($recurrenceendsid == '3') {
					$ccc = $datetime1->diff($datetime2)->days;
					$count = 1;
				} else if($recurrenceendsid == '4') {
					$ccc = $datetime1->diff($datetime2)->days;
					$count = 1;
				}
			} else if($recurrencefreqid == '16') { //for monthly
				if($recurrenceendsid == '4') {
					$ts1 = strtotime($startdate);
					$ts2 = strtotime($enddate);
					$year1 = date('Y', $ts1);
					$year2 = date('Y', $ts2);
					$month1 = date('m', $ts1);
					$month2 = date('m', $ts2);
					$diff = (($year2 - $year1) * 12) + ($month2 - $month1);
					$count = $diff/$crmrecurrenceeveryday;
				} else if($recurrenceendsid == '3') { 
					$count = $activityafteroccurences;
				} else if($recurrenceendsid == '2') {
					$day = 730;
					$sdate = strtotime("+".$day." day", strtotime($startdate));
					$enddate = date("d-m-Y", $sdate);
					$ts1 = strtotime($startdate);
					$ts2 = strtotime($enddate);
					$year1 = date('Y', $ts1);
					$year2 = date('Y', $ts2);
					$month1 = date('m', $ts1);
					$month2 = date('m', $ts2);
					$diff = (($year2 - $year1) * 12) + ($month2 - $month1);
					$count = $diff;
				}
			} else if($recurrencefreqid == '19') { //for yearly
				if($recurrenceendsid == '4') {
					$ccc = $datetime1->diff($datetime2)->y;
					$count = $ccc/$crmrecurrenceeveryday; 
				} else if($recurrenceendsid == '3') { 
					$count = $activityafteroccurences;
				} else if($recurrenceendsid == '2') { 
					$day = 730;
					$sdate = strtotime("+".$day." day", strtotime($startdate));
					$enddate = date("d-m-Y", $sdate);
					$datetime2 = date_create($enddate);
					$count = $datetime1->diff($datetime2)->y; 
				}
			}
			$tempeventid = date('YmdHis');
			for($i=0;$i<$count;$i++) {
				$sdate = $this->getstartdate($crmrecurrenceeveryday,$startdate,$enddate,$recurrencefreqid,$crmrecurrenceontheday,$crmrecurrencedayid,$i);
				$rcount = count($sdate);
				for($j=0;$j<$rcount;$j++) {
					$rdate = explode(',',$sdate[$j]);
					$startdate = $rdate[0];
					$enddate = $rdate[1];
					$_POST['activitystartdate']=$startdate;
					$_POST['activityenddate']=$enddate;
					//invite section
					if(isset($_POST['leadtemplateid'])){
						$invitesmstempid = $_POST['leadtemplateid'];
						if($invitesmstempid != '') {
							$invsmstempid = $invitesmstempid;
						} else { $invsmstempid = 1; }
					} else { $invsmstempid = 1; }
					if(isset($_POST['emailtemplatesid'])){
						$inviteemailtempid = $_POST['emailtemplatesid'];
						if($inviteemailtempid != '') {
							$invemailtempid = $inviteemailtempid;
						} else { $invemailtempid = 1; }
					} else { $invemailtempid = 1; }
					$inviteremindermode = trim($inviteremindermode,",");
					//activity insert
					$activity =  array(
							'commonid'=>$commonid,
							'moduleid'=>$moduleid,
							'crmactivityname'=>$crmactivityname,
							'activitytypeid'=>$activitytypeid,
							'priorityid'=>$priorityid,
							'employeetypeid'=>$emptypes,
							'employeeid'=>$employeeid,
							'crmstatusid'=>$leadstatusid,
							'contactid'=>$contactid,
							'location'=>$location,
							'description'=>$description,
							'eventid'=>$tempeventid,
							'activitiesdescription_editorfilename'=>'',
							'activitystartdate'=>$this->Basefunctions->ymddateconversion($startdate),
							'activitystarttime'=>$starttime,
							'activityenddate'=>$this->Basefunctions->ymddateconversion($enddate),
							'activityendtime'=>$endtime,
							'remindermodeid'=>$remindermode,
							'reminderintervalid'=>$reminderinreval,
							'leadtemplateid'=>$remsmstempid,
							'emailtemplatesid'=>$rememailtempid,
							'leadid'=>$inviteusers,
							'invitemode' => $inviteremindermode,
							'invitesmstemplate' => $invsmstempid,
							'invitemailtemplate' => $invemailtempid,
							'cronnotification'=>$cronnotification,
							'cronstatus'=>$cronstatus,
							'createdate'=>$cdate,
							'lastupdatedate'=>$cdate,
							'createuserid'=>$userid,
							'lastupdateuserid'=>$userid,
							'status'=>$this->Basefunctions->activestatus
						);
					$primaryid = $this->Crudmodel->datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable);
					//invite section
					if(isset($_POST['leadtemplateid'])){
						$invitesmstempid = $_POST['leadtemplateid'];
						if($invitesmstempid != '') {
							$invsmstempid = $invitesmstempid;
						} else { $invsmstempid = 1; }
					} else { $invsmstempid = 1; }
					if(isset($_POST['emailtemplatesid'])){
						$inviteemailtempid = $_POST['emailtemplatesid'];
						if($inviteemailtempid != '') {
							$invemailtempid = $inviteemailtempid;
						} else { $invemailtempid = 1; }
					} else { $invemailtempid = 1; }
					if(isset($_POST['activityinvitemoduleid'])){
						$invitemodid = $_POST['activityinvitemoduleid'];
						if($invitemodid != '') {
							$invmodeid = $invitemodid;
						} else { $invmodeid = 1; }
					} else { $invmodeid = 1; }
					if(isset($_POST['inviteviewname'])){
						$inviteviewname = $_POST['inviteviewname'];
						if($inviteviewname != '') {
							$invviewname = $inviteviewname;
						} else { $invviewname = 1; }
					} else { $invviewname = 1; }
					$inviteremindermode = trim($inviteremindermode,",");
					$template= array('leadtemplateid'=>$remsmstempid,'emailtemplatesid'=>$rememailtempid, 'invitemode' => $inviteremindermode, 'invitesmstemplate' => $invsmstempid, 'invitemailtemplate' => $invemailtempid, 'inviteviewname' => $invviewname, 'leadid' => $invmodeid);
					$this->db->where_in('crmactivity.crmactivityid',array($primaryid));
					$this->db->update('crmactivity',$template);
					$recurrence = array(
							'recurrencefreqid'=>$recurrencefreqid,
							'recurrencedayid'=>$crmrecurrencedayid,
							'crmactivityid'=>$primaryid,
							'moduleid'=>$moduleid,
							'crmrecurrenceuntil'=>$this->Basefunctions->ymddateconversion($crmrecurrenceuntil),
							'crmrecurrenceeveryday'=>$crmrecurrenceeveryday,
							'recurrenceendsid'=>$recurrenceendsid,
							'afteroccurences'=>$activityafteroccurences,
							'createdate'=>$cdate,
							'lastupdatedate'=>$cdate,
							'createuserid'=>$userid,
							'lastupdateuserid'=>$userid,
							'status'=>$this->Basefunctions->activestatus
						);
					$this->db->insert('crmrecurrence',$recurrence);
					$actid[$i] = $primaryid;
					$activitys[$i] = $activity;
					$apikeyandsenderid = $this->defapikeyandsenderidget();
					$subaccountid = $this->subaccountdetailsfetchmodel();
					//reminder notification
					$industry = $this->Basefunctions->industryid;
					{ //notification entry
						$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
						if(isset($_POST['employeeid'])) {
							$assignid = $_POST['employeeid'];
							$emptypes = $_POST['employeetypeid'];
							$empdataids = array();
							$assignempids = array();
							if($assignid != '') {
								$k = 0;
								$m=0;
								$empiddatas = explode(',',$assignid);
								$emptypeids = explode(',',$emptypes);
								foreach($empiddatas as $empids) {
									$emptype = $emptypeids[$m];
									if($emptype == 1) {
										$empdataids[$k] = $empids;
										$k++;
									} else if($emptype == 2) {
										$empdataids[$k] = $this->Basefunctions->empidfetchfromgroup($empids);
										$k++;
									} else if($emptype == 3) {
										$empdataids[$k] = $this->Basefunctions->empidfetchfromroles($empids);
										$k++;
									} else if($emptype == 4) {
										$empdataids[$k] = $this->Basefunctions->empidfetchfromrolesandsubroles($empids);
										$k++;
									}
									$m++;
								}
							} else {
								$assignid = 1;
							}
						} else {
							$assignid = 1;
						}
						$activityname = $_POST['crmactivityname'];
						if($assignid == '1'){
							$notimsg = $this->Basefunctions->notificationtemplatedatafetch(205,$primaryid,$partablename,2);
							if($notimsg != '') {
								$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,205);
							}
						} else {
							foreach($empdataids as $empidinfo) {
								if(is_array($empidinfo)) { //group of employees
									foreach($empidinfo as $empid) {
										if( !in_array($empid,$assignempids) ) {
											array_push($assignempids,$empid);
											$notimsg = $this->Basefunctions->notificationtemplatedatafetch(205,$primaryid,$partablename,2);
											if($notimsg != '') {
												$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,205);
											}
										}
									}
								} else { //individual employees
									if( !in_array($empidinfo,$assignempids) ) {
										array_push($assignempids,$empidinfo);
										$notimsg = $this->Basefunctions->notificationtemplatedatafetch(205,$primaryid,$partablename,22);
										if($notimsg != '') {
											$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,205);
										}
									}
								}
							}
						}						
					}
					$notifypatient = (isset($_POST['notifypatient'])) ? $_POST['notifypatient'] : 'No';
					if($notifypatient != 'No') {
						$remindermodeid = $_POST['remindermodeid'];
						for($pat=0;$pat<count($remindermodeid);$pat++){
							if($remindermodeid[$pat] == 2) {
								$invitemobilenum = $_POST['mobilenumber'];
								$remindersmstempid = $_POST['reminderleadtemplateid'];
								if($remindersmstempid != ''){
									$print_data=array('templateid'=>$remindersmstempid,'Templatetype'=>'Printtemp','primaryset'=>'crmactivity.crmactivityid','primaryid'=>$primaryid);
									$datacontent = $this->smsgenerateprinthtmlfile($print_data);
									//	$this->Basefunctions->sendsmsinallmodules($apikeyandsenderid['apikey'],$apikeyandsenderid['senderid'],$invitemobilenum,$datacontent,'',$primaryid,'205');
								}
								
							} else if($remindermodeid[$pat] == '3') {
								$invitemail = $_POST['emailaddress'];
								$remindermailid = $_POST['reminderemailtemplatesid'];
								if($remindermailid != ''){
									$print_data=array('templateid'=>$remindermailid,'Templatetype'=>'Printtemp','primaryset'=>'crmactivity.crmactivityid','primaryid'=>$primaryid);
									$datacontent = $this->generateprinthtmlfile($print_data);
									$this->mailnotificationsent($invitemail,$primaryid,$activityname,$this->Basefunctions->ymddateconversion($startdate),$datacontent,$subaccountid);
								}
								
							}
						}
						
					}
					//invite user notification
					$date = $_POST['activitystartdate'];
					$gcaldate = $date; //For G cal
					$leadinviteid = $_POST['leadinviteusetid'];
					$contactinviteid = $_POST['contactinviteuserid'];
					$userinviteid = $_POST['userinviteuserid'];
					$groupinviteid = $_POST['groupinviteuserid'];
					$rolesinviteid = $_POST['rolesinviteuserid'];
					$randsinviteid = $_POST['randsinviteuserid'];
					//dashboard notification sent
					$time = $_POST['activitystarttime'];
					$invitetype = $_POST['invitetypeid'];
					if($invitetype != '') {
						$typeid = explode(',',$invitetype);
						for($l=0;$l < count($typeid);$l++) {
							if($typeid[$l] == '6') {
								$this->inviteusernotification($leadinviteid,$contactinviteid,$userinviteid,$groupinviteid,$rolesinviteid,$randsinviteid,$primaryid,$crmactivityname,$date);
							} else if($typeid[$l] == '2') {
								$invitemobilenum = $_POST['invitemobilenumber'];
								$invitealnotes = $_POST['leadtemplateid'];
								if($invitealnotes !=''){
									$print_data=array('templateid'=>$invitealnotes,'Templatetype'=>'Printtemp','primaryset'=>'crmactivity.crmactivityid','primaryid'=>$primaryid);
									$datacontent = $this->smsgenerateprinthtmlfile($print_data);
									//$this->Basefunctions->sendsmsinallmodules($apikeyandsenderid['apikey'],$apikeyandsenderid['senderid'],$invitemobilenum,$datacontent,'',$primaryid,'205');
								}
								
							} else if($typeid[$l] == '3') {
								$invitemail = $_POST['inviteemailid'];
								$invitemailnotes = $_POST['emailtemplatesid'];
								if($invitemailnotes !=''){
									$print_data=array('templateid'=>$invitemailnotes,'Templatetype'=>'Printtemp','primaryset'=>'crmactivity.crmactivityid','primaryid'=>$primaryid);
									$datacontent = $this->generateprinthtmlfile($print_data);
									$this->mailnotificationsent($invitemail,$primaryid,$activityname,$date,$datacontent,$subaccountid);
								}
								
							}
							
						}
					}
				}
			}
		} else {
			$activityname = $_POST['crmactivityname'];
			$primaryid = $this->Crudmodel->datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable);
			//reminder template update
			if(isset($_POST['reminderleadtemplateid'])){
				$remindersmstempid = $_POST['reminderleadtemplateid'];
				if($remindersmstempid != '') {
					$remsmstempid = $remindersmstempid;
				} else { $remsmstempid = 1; }
			} else { $remsmstempid = 1; }
			if(isset($_POST['reminderemailtemplatesid'])){
				$reminderemailtempid = $_POST['reminderemailtemplatesid'];
				if($reminderemailtempid != '') {
					$rememailtempid = $reminderemailtempid;
				} else { $rememailtempid = 1; }
			} else { $rememailtempid = 1; }
			//invite section
			if(isset($_POST['leadtemplateid'])){
				$invitesmstempid = $_POST['leadtemplateid'];
				if($invitesmstempid != '') {
					$invsmstempid = $invitesmstempid;
				} else { $invsmstempid = 1; }
			} else { $invsmstempid = 1; }
			if(isset($_POST['emailtemplatesid'])){
				$inviteemailtempid = $_POST['emailtemplatesid'];
				if($inviteemailtempid != '') {
					$invemailtempid = $inviteemailtempid;
				} else { $invemailtempid = 1; }
			} else { $invemailtempid = 1; }
			if(isset($_POST['activityinvitemoduleid'])){
				$invitemodid = $_POST['activityinvitemoduleid'];
				if($invitemodid != '') {
					$invmodeid = $invitemodid;
				} else { $invmodeid = 1; }
			} else { $invmodeid = 1; }
			if(isset($_POST['inviteviewname'])){
				$inviteviewname = $_POST['inviteviewname'];
				if($inviteviewname != '') {
					$invviewname = $inviteviewname;
				} else { $invviewname = 1; }
			} else { $invviewname = 1; }
			$inviteremindermode = trim($inviteremindermode,",");
			$template= array('leadtemplateid'=>$remsmstempid,'emailtemplatesid'=>$rememailtempid, 'invitemode' => $inviteremindermode, 'invitesmstemplate' => $invsmstempid, 'invitemailtemplate' => $invemailtempid, 'inviteviewname' => $invviewname, 'leadid' => $invmodeid,'commonid'=>$commonid);
			$this->db->where_in('crmactivity.crmactivityid',array($primaryid));
			$this->db->update('crmactivity',$template);
			//reminder notification
			$apikeyandsenderid = $this->defapikeyandsenderidget();
			$subaccountid = $this->subaccountdetailsfetchmodel();
			$industry = $this->Basefunctions->industryid;
			$notifypatient = (isset($_POST['notifypatient'])) ? $_POST['notifypatient'] : 'No';
			if($notifypatient != 'No') {
				$remindermodeid = $_POST['remindermodeid'];
				for($pat=0;$pat<count($remindermodeid);$pat++){
					if($remindermodeid[$pat] == 2) {
						$invitemobilenum = $_POST['mobilenumber'];
						$remindersmstempid = $_POST['reminderleadtemplateid'];
						if($remindersmstempid != ''){
							$print_data=array('templateid'=>$remindersmstempid,'Templatetype'=>'Printtemp','primaryset'=>'crmactivity.crmactivityid','primaryid'=>$primaryid);
							$datacontent = $this->smsgenerateprinthtmlfile($print_data);
							//	$this->Basefunctions->sendsmsinallmodules($apikeyandsenderid['apikey'],$apikeyandsenderid['senderid'],$invitemobilenum,$datacontent,'',$primaryid,'205');
						}
						
					} else if($remindermodeid[$pat] == '3') {
						$invitemail = $_POST['emailaddress'];
						$reminderemailtempid = $_POST['reminderemailtemplatesid'];
						if($reminderemailtempid != ''){
							$print_data=array('templateid'=>$reminderemailtempid,'Templatetype'=>'Printtemp','primaryset'=>'crmactivity.crmactivityid','primaryid'=>$primaryid);
							$datacontent = $this->generateprinthtmlfile($print_data);
							$this->mailnotificationsent($invitemail,$primaryid,$activityname,$this->Basefunctions->ymddateconversion($startdate),$datacontent,$subaccountid);
						}
					}
				}
			}
			//invite user notification
			$date = $_POST['activitystartdate'];
			$gcaldate = $date; //For G cal
			if(isset($_POST['leadinviteusetid'])){
				$leadinviteid = $_POST['leadinviteusetid'];
				$contactinviteid = $_POST['contactinviteuserid'];
				$userinviteid = $_POST['userinviteuserid'];
				$groupinviteid = $_POST['groupinviteuserid'];
				$rolesinviteid = $_POST['rolesinviteuserid'];
				$randsinviteid = $_POST['randsinviteuserid'];
				//dashboard notification sent
				$time = $_POST['activitystarttime'];
				$invitetype = $_POST['invitetypeid'];
				if($invitetype != '') {
					$typeid = explode(',',$invitetype);
					for($l=0;$l < count($typeid);$l++) {
						if($typeid[$l] == '6') {
							$this->inviteusernotification($leadinviteid,$contactinviteid,$userinviteid,$groupinviteid,$rolesinviteid,$randsinviteid,$primaryid,$crmactivityname,$date);
						} else if($typeid[$l] == '2') {
							$invitemobilenum = '';
							$invitesmstempid = $_POST['leadtemplateid'];
							if($invitesmstempid != ''){
								$print_data=array('templateid'=>$invitesmstempid,'Templatetype'=>'Printtemp','primaryset'=>'crmactivity.crmactivityid','primaryid'=>$primaryid);
								$datacontent = $this->smsgenerateprinthtmlfile($print_data);
								//	$this->Basefunctions->sendsmsinallmodules($apikeyandsenderid['apikey'],$apikeyandsenderid['senderid'],$invitemobilenum,$datacontent,'',$primaryid,'205');
							}
				
						} else if($typeid[$l] == '3') {
							$invitemail = '';
							$invitemailid = $_POST['emailtemplatesid'];
							if($invitemailid != '') {
								$print_data=array('templateid'=>$invitemailid,'Templatetype'=>'Printtemp','primaryset'=>'crmactivity.crmactivityid','primaryid'=>$primaryid);
								$datacontent = $this->generateprinthtmlfile($print_data);
								$this->mailnotificationsent($invitemail,$primaryid,$activityname,$date,$datacontent,$subaccountid);
							}
				
						}
					}
				}
			}
		}
		echo 'TRUE';
	 }
	public function getDaysShort($string){
		$ar[1] = array('SU');
		$ar[2] = array('MO');
		$ar[3] = array('TU');
		$ar[4] = array('WE');
		$ar[5] = array('TH');
		$ar[6] = array('FR');
		$ar[7] = array('SA');
		
		$arrFromString = explode(',', $string);
		$newString = '';
		foreach($ar as $intKey => $arr){
			foreach($arrFromString as $intNumber){
				if($intKey == $intNumber) {
					$newString .= $arr[0].',';
				}
			}
		}
		$newString = rtrim($newString,',');
		return $newString;
		
	}	
	//get freq name
	public function getFreqName($crmrecurrencefreqid){
	
		$this->db->select('recurrencefreqname',false);
		$this->db->from('recurrencefreq');
		$this->db->where('recurrencefreq.recurrencefreqid',$crmrecurrencefreqid);
		$result = $this->db->get();
		if($result ->num_rows() > 0) {
			foreach($result->result() as $row) {
				$recurname = $row->recurrencefreqname;
			}
		}
		return $recurname;		
	}
	//used to get the date based on the recurrence selection -- daily/weekly/monthly/yearly
	public function getstartdate($crmrecurrenceeveryday,$sdate,$enddate,$crmrecurrencefreqid,$crmrecurrenceontheday,$crmrecurrencedayid,$count) {
		if($crmrecurrencefreqid == 14) { // for daily
			if($count == 0) {
				$daydate =$sdate.','.$enddate;
				$daydate = array($daydate);
			} else {
				$sdate = strtotime("+".$crmrecurrenceeveryday." day", strtotime($sdate));
				$edate = strtotime("+".$crmrecurrenceeveryday." day", strtotime($enddate));
				$daydate =date("d-m-Y", $sdate).','.date('d-m-Y',$edate);
				$daydate = array($daydate);
			}
			return $daydate;
		} else if($crmrecurrencefreqid == 15) { //for weekly
			$ssdate = array();
			$weekdate = '';
			$datetime1 = date_create($sdate);
			$datetime2 = date_create($enddate);
			$ccc = $datetime1->diff($datetime2)->days;
			$newcount = round($ccc/7);//week number
			$dayvalue = explode(',',$crmrecurrencedayid);
			$weekday = $this->getweekday($dayvalue);
			$j=0;
			for($i=0;$i<$newcount;$i++) {
				$value = 7 * $crmrecurrenceeveryday;
				$num = $i * $value;
				$newdate = strtotime("+".$num." day", strtotime($sdate));
				$date = date("d-m-Y", $newdate);
				$dt = new DateTime($date);
				$dayname = $dt->format('l');
				$startdate[$j] = $date;
				$day[$j] = $dayname;
				$edate = strtotime("+".$num." day", strtotime($enddate));
				$endate = date("d-m-Y", $edate);
				$edt = new DateTime($endate);
				$edayname = $edt->format('l');
				$eenddate[$j] = $endate;
				$eday[$j] = $edayname; 
				$j++;
				$weekdate = $this->getweekdate($startdate,$weekday);
				$endweekdate = $this->getweekdate($eenddate,$weekday);
			} 
			$ccount = count($weekdate);
			$n=0;
			for($m=0;$m<$ccount;$m++) {
				$ssdate[$n] = $weekdate[$m].','.$endweekdate[$m];
				$n++;
			}
			return $ssdate;	  
		} else if($crmrecurrencefreqid == 16) { // for monthly
			if($count == 0) {
				$daydate =$sdate.','.$enddate;
				$mdate = array($daydate);
			} else {
				//for start date
				$month = explode('-',$sdate);
				if($month[1] == '01' || $month[1] == '03' || $month[1] == '05' || $month[1] == '07'|| $month[1] == '08' || $month[1] == '10' || $month[1] == '12'){
					$day = $crmrecurrenceeveryday * 31;
				} else if($month[1] == '04' || $month[1] == '06' || $month[1] == '09' || $month[1] == '11') {
					$day = $crmrecurrenceeveryday * 30;
				} else {
					if(($month[2] % 4) == 0) {
						$day = $crmrecurrenceeveryday * 29;
					} else {
						$day = $crmrecurrenceeveryday * 28;
					}
				}
				//for end date
				$endmonth = explode('-',$enddate);
				if($endmonth[1] == '01' || $endmonth[1] == '03' || $endmonth[1] == '05' || $endmonth[1] == '07'|| $endmonth[1] == '08' || $endmonth[1] == '10' || $endmonth[1] == '12'){
					$eday = $crmrecurrenceeveryday * 31;
				} else if($endmonth[1] == '04' || $endmonth[1] == '06' || $endmonth[1] == '09' || $endmonth[1] == '11') {
					$eday = $crmrecurrenceeveryday * 30;
				} else {
					if(($month[2] % 4) == 0) {
						$eday = $crmrecurrenceeveryday * 29;
					} else {
						$eday = $crmrecurrenceeveryday * 28;
					}
				}
				$reccuurday = $crmrecurrenceontheday - 1;
				//for start date
				$sdate = strtotime("+".$day." day", strtotime($sdate));
				$date = date("d-m-Y", $sdate);
				$date1 =  explode('-',$date);
				$sdata = $reccuurday.'-'.$date1[1].'-'.$date1[2];
				//for end date
				$enddate = strtotime("+".$eday." day", strtotime($enddate));
				$edate = date("d-m-Y", $enddate);
				$edate1 =  explode('-',$edate);
				$edata = $reccuurday.'-'.$edate1[1].'-'.$edate1[2];
				//final start and end date
				$daydate =$sdata.','.$edata;
				$mdate = array($daydate);
			}
			return $mdate;
		} else if($crmrecurrencefreqid == 19) {// for yearly
			if($count == 0) {
				$daydate =$sdate.','.$enddate;
				$daydate = array($daydate);
				return $daydate;
			} else {
				//for start date
				$year = explode('-',$sdate);
				if(($year[2] % 4) == 0) {
					$day = $crmrecurrenceeveryday * 366;
				} else {
					$day = $crmrecurrenceeveryday * 365;
				}
				$sdate = strtotime("+".$day." day", strtotime($sdate));
				//for end date
				$eyear = explode('-',$enddate);
				if(($eyear[2] % 4) == 0) {
					$eday = $crmrecurrenceeveryday * 366;
				} else {
					$eday = $crmrecurrenceeveryday * 365;
				}
				$enddate = strtotime("+".$eday." day", strtotime($enddate));
				$daydate =date("d-m-Y", $sdate).','.date("d-m-Y", $enddate);
				$ydate = array($daydate);
				return $ydate;
			}
		}
	}
	//get week day
	public function getweekday($dayvalue) { 
		$count = count($dayvalue);
		for($i=0;$i<$count;$i++) {
			if($dayvalue[$i] == 2){
				$weekday[$i] = 'Sunday';
			} else if($dayvalue[$i] == 3) {
				$weekday[$i] = 'Monday';
			} else if($dayvalue[$i] == 4) {
				$weekday[$i] = 'Wednesday';
			} else if($dayvalue[$i] == 5) {
				$weekday[$i] = 'Tuesday';
			} else if($dayvalue[$i] == 6) {
				$weekday[$i] = 'Thursday';
			} else if($dayvalue[$i] == 7) {
				$weekday[$i] = 'Friday';
			} else if($dayvalue[$i] == 8) {
				$weekday[$i] = 'Saturday';
			}
		}
		return $weekday;
	}
	//week date get based on the user data
	public function getweekdate($startdate,$weekday) {
		$count = count($startdate);
		$k=0;
		for($i=0;$i<$count;$i++) {
			for($j=0;$j<7;$j++) {
				$newdate = strtotime("+".$j." day", strtotime($startdate[$i]));
				$date = date("d-m-Y", $newdate);
				$dt = new DateTime($date);
				$dayname = $dt->format('l');
				if(in_array($dayname,$weekday)) {
					$wdate[$k] = $date;
					$k++;
				}
			}
		}
		return $wdate;
	}
	//information fetch
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['dataprimaryid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	//update data information
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['primarydataid'];
		{//fetch old values -- work flow
			$condstatvals=$this->Basefunctions->workflowmanageolddatainfofetch(205,$primaryid);
		}
		$restricttable = explode(',',$_POST['resctable']);
		$result = $this->Crudmodel->dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable);
		//notification entry
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		if(isset($_POST['actvitycommonid'])) {
			$commonid = $_POST['actvitycommonid'];
			if($commonid == ''){
				$commonid = 1;
			}
		} else {
			$commonid = 1;
		}
		if(isset($_POST['actvitymoduleid'])) {
			$moduleid = $_POST['actvitymoduleid'];
			if($moduleid == '') {
				$moduleid = 1;
			}
		} else {
			$moduleid =1;
		}
		if(isset($_POST['employeeid'])) {
			$assignid = $_POST['employeeid'];
			$emptypes = $_POST['employeetypeid'];
			$empdataids = array();
			$assignempids = array();
			if($assignid != '') {
				$i = 0;
				$m=0;
				$empiddatas = explode(',',$assignid);
				foreach($empiddatas as $empids) {
					$emptype = $emptypes[$m];
					if($emptype == 1) {
						$empdataids[$i] = $empids;
						$i++;
					} else if($emptype == 2) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromgroup($empids);
						$i++;
					} else if($emptype == 3) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromroles($empids);
						$i++;
					} else if($emptype == 4) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromrolesandsubroles($empids);
						$i++;
					}
					$m++;
				}
			} else {
				$assignid = 1;
			}
		} else {
			$assignid = 1;
		}
		$activityname = $_POST['crmactivityname'];
		if($assignid == '1'){
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch(205,$primaryid,$partablename,3);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,205);
			}
		} else {
			foreach($empdataids as $empidinfo) {
				if(is_array($empidinfo)) { //group of employees
					foreach($empidinfo as $empid) {
						if( !in_array($empid,$assignempids) ) {
							array_push($assignempids,$empid);
							$notimsg = $this->Basefunctions->notificationtemplatedatafetch(205,$primaryid,$partablename,3);
							if($notimsg != '') {
								$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,205);
							}
						}
					}
				} else { //individual employees
					if( !in_array($empidinfo,$assignempids) ) {
						array_push($assignempids,$empidinfo);
						$notimsg = $this->Basefunctions->notificationtemplatedatafetch(205,$primaryid,$partablename,3);
						if($notimsg != '') {
							$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,205);
						}
					}
				}
			}
		}
		//reminder template update
		if(isset($_POST['reminderleadtemplateid'])){
			$remindersmstempid = $_POST['reminderleadtemplateid'];
			if($remindersmstempid != '') {
				$remsmstempid = $remindersmstempid;
			} else { $remsmstempid = 1; }
		} else { $remsmstempid = 1; }
		if(isset($_POST['reminderemailtemplatesid'])){
			$reminderemailtempid = $_POST['reminderemailtemplatesid'];
			if($reminderemailtempid != '') {
				$rememailtempid = $reminderemailtempid;
			} else { $rememailtempid = 1; }
		} else { $rememailtempid = 1; }
		$template= array('leadtemplateid'=>$remsmstempid,'emailtemplatesid'=>$rememailtempid,'commonid'=>$commonid);
		$this->db->where_in('crmactivity.crmactivityid',array($primaryid));
		$this->db->update('crmactivity',$template);
		$startdate = $_POST['activitystartdate'];
		$apikeyandsenderid = $this->defapikeyandsenderidget();
		$subaccountid = $this->subaccountdetailsfetchmodel();
		//reminder sent
		$notifypatient = (isset($_POST['notifypatient'])) ? $_POST['notifypatient'] : 'No';
		if($notifypatient != 'No') {
			$remindermodeid = $_POST['remindermodeid'];
			for($pat=0;$pat<count($remindermodeid);$pat++){
				if($remindermodeid[$pat] == 2) {
					$invitemobilenum = $_POST['mobilenumber'];
					$remindersmstempid = $_POST['reminderleadtemplateid'];
					if($remindersmstempid != ''){
						$print_data=array('templateid'=>$remindersmstempid,'Templatetype'=>'Printtemp','primaryset'=>'crmactivity.crmactivityid','primaryid'=>$primaryid);
						$datacontent = $this->smsgenerateprinthtmlfile($print_data);
						//	$this->Basefunctions->sendsmsinallmodules($apikeyandsenderid['apikey'],$apikeyandsenderid['senderid'],$invitemobilenum,$datacontent,'',$primaryid,'205');
					}
		
				} else if($remindermodeid[$pat] == '3') {
					$invitemail = $_POST['emailaddress'];
					$reminderemailtempid = $_POST['reminderemailtemplatesid'];
					if($reminderemailtempid != ''){
						$print_data=array('templateid'=>$reminderemailtempid,'Templatetype'=>'Printtemp','primaryset'=>'crmactivity.crmactivityid','primaryid'=>$primaryid);
						$datacontent = $this->generateprinthtmlfile($print_data);
						$this->mailnotificationsent($invitemail,$primaryid,$activityname,$this->Basefunctions->ymddateconversion($startdate),$datacontent,$subaccountid);
					}
				}
			}
		}
		//invite user notification
		$date = $_POST['activitystartdate'];
		$enddate = $_POST['activityenddate'];
		$gstarttime = $_POST['activitystarttime'];
		$gendtime = $_POST['activityendtime'];
		$location = (isset($_POST['location']))? $_POST['location'] : '';
		$description = (isset($_POST['actvitydescription']))? $_POST['actvitydescription'] : '';
		if(isset($_POST['actvitydescription'])){
			$leadinviteid = $_POST['leadinviteusetid'];
			$contactinviteid = $_POST['contactinviteuserid'];
			$userinviteid = $_POST['userinviteuserid'];
			$groupinviteid = $_POST['groupinviteuserid'];
			$rolesinviteid = $_POST['rolesinviteuserid'];
			$randsinviteid = $_POST['randsinviteuserid'];
			//dashboard notification sent
			$apikeyandsenderid = $this->defapikeyandsenderidget();
			$subaccountid = $this->subaccountdetailsfetchmodel();
			$invitetype = $_POST['invitetypeid'];
			if($invitetype != '') {
				$typeid = explode(',',$invitetype);
				for($l=0;$l < count($typeid);$l++) {
					if($typeid[$l] == '6') {
						$this->inviteusernotification($leadinviteid,$contactinviteid,$userinviteid,$groupinviteid,$rolesinviteid,$randsinviteid,$primaryid,$activityname,$date);
					} else if($typeid[$l] == '2') {
						$invitemobilenum = $_POST['invitemobilenumber'];
						$remindersmstempid = $_POST['leadtemplateid'];
						if($remindersmstempid != ''){
							$print_data=array('templateid'=>$remindersmstempid,'Templatetype'=>'Printtemp','primaryset'=>'crmactivity.crmactivityid','primaryid'=>$primaryid);
							$datacontent = $this->smsgenerateprinthtmlfile($print_data);
							//	$this->Basefunctions->sendsmsinallmodules($apikeyandsenderid['apikey'],$apikeyandsenderid['senderid'],$invitemobilenum,$datacontent,'',$primaryid,'205');
						}
					} else if($typeid[$l] == '3') {
						$invitemail = $_POST['inviteemailid'];
						$mailnotes  = $_POST['emailtemplatesid'];
						if($mailnotes != ''){
							$print_data=array('templateid'=>$mailnotes,'Templatetype'=>'Printtemp','primaryset'=>'crmactivity.crmactivityid','primaryid'=>$primaryid);
							$datacontent = $this->generateprinthtmlfile($print_data);
							$this->mailnotificationsent($invitemail,$primaryid,$activityname,$date,$datacontent,$subaccountid);
						}
					}
				}
			}
		}
		echo "TRUE";
	}
	//delete old information
	public function deleteoldinformation($moduleid) {	
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primarydataid'];
		$msg='False';
		$eventid = $this->Basefunctions->generalinformaion('crmactivity','eventid','crmactivityid',$id);
		$filename = $this->Basefunctions->generalinformaion('crmactivity','crmactivityname','crmactivityid',$id);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		{//workflow management -- for data delete
			$this->Basefunctions->workflowmanageindatadeletemode(205,$id,$primaryname,$partabname);
		}
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				$msg='Denied';
			} else {
				//notification log
				$empid = $this->Basefunctions->logemployeeid;
				$notimsg = $this->Basefunctions->notificationtemplatedatafetch(205,$id,$partabname,4);
				if($notimsg != '') {
					$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,1,205);
				}
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				$msg = 'TRUE';
			}
		} else {
			//notification log
			$empid = $this->Basefunctions->logemployeeid;
			//deleted file name
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch(205,$id,$partabname,4);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,1,205);
			}
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			$msg = 'TRUE';
		}
		echo $msg;
	}
	//drop down value fetch based on the modules
	public function dropdownvaluefecthmodel() {
		$id = $_GET['moduleid'];
		$this->db->select('modulefieldid',false);
		$this->db->from('primaryfieldmapping');
		$this->db->where('primaryfieldmapping.moduleid',$id);
		$this->db->where('primaryfieldmapping.status',1);
		$result = $this->db->get();
		if($result ->num_rows() > 0) {
			foreach($result->result() as $row) {
				$mfid = $row->modulefieldid;
			}
		}
		$this->db->select('columnname,tablename',false);
		$this->db->from('modulefield');
		$this->db->where('modulefield.modulefieldid',$mfid);
		$this->db->where('modulefield.status',1);
		$result = $this->db->get();
		if($result ->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('filedname'=>$row->columnname,'tablename'=>$row->tablename);
			}
			echo json_encode($data);
		} else {
			echo json_encode(array('failed'=>'Failure'));
		}		
	}
	//record name fetch
	public function recordnamefetchmodel(){
		$i =0;
		$tablename = $_GET['tabname'];
		$fieldname = $_GET['fieldname'];
		$fieldid = $tablename.'id';
		$industryid = $this->Basefunctions->industryid;
		if($tablename == 'lead' || $tablename == 'contact' || $tablename == 'employee') {
			if($industryid == 5) {
				$this->db->select($fieldname.' AS name ,'.$fieldid.','.$fieldname);
				$this->db->from($tablename);
			} else {
				$this->db->select($fieldid.','.$fieldname.',CONCAT(salutation.salutationname, '.$fieldname.', lastname) AS name', FALSE);
				$this->db->from($tablename);
				$this->db->join('salutation','salutation.salutationid='.$tablename.'.salutationid','left outer');
			}
		} else {
			$this->db->select($fieldname.' AS name ,'.$fieldid.','.$fieldname);
			$this->db->from($tablename);
		}
		$this->db->where($tablename.'.'.'status',1);
		$this->db->where("FIND_IN_SET('".$industryid."',$tablename.industryid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$data[$i] = array('id'=>$row->$fieldid,'name'=>$row->name);
				$i++;
			}
			echo json_encode($data); 			
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}
	}
	//activity validation
	public function activityvalidationmodel() {
		$name = $_POST['name'];
		$date = $_POST['date'];
		$time = $_POST['time'];
		$this->db->select('crmactivity.crmactivityname,crmactivity.activitystartdate,crmactivity.activitystarttime');
		$this->db->from('crmactivity');
		$this->db->where('crmactivity.crmactivityname',$name);
		$this->db->where('crmactivity.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data = 'TRUE';
			}
			echo json_encode($data);
		} else {
			 echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//Default api key and sender id get
	public function defapikeyandsenderidget() {
		$data = '';
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('senderid,smsprovidersettings.apikey');
		$this->db->from('smssettings');
		$this->db->join('smsprovidersettings','smsprovidersettings.smsprovidersettingsid=smssettings.smsprovidersettingsid');
		$this->db->where('smssettings.smsprovidersettingsid',2);
		$this->db->where('smssettings.status',1);
		$this->db->where("FIND_IN_SET('".$industryid."',smssettings.industryid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data = array('senderid'=>$row->senderid,'apikey'=>$row->apikey);
			}
			return $data;
		} else {
		   return $data;
		}
	}
	//Login user mobile number get
	public function usermobilenuberget() {
		$userid = $this->Basefunctions->userid;
		$data = '';
		$this->db->select('mobilenumber');
		$this->db->from('employee');
		$this->db->where('employee.employeeid',$userid);
		$result = $this->db->get();
		if($result->num_rows()  > 0 ) {
			foreach($result->result() as $row) {
				$data = $row->mobilenumber;
			}
			return $data;
		} else {
		   return $data;
		}
	}
	//module drop down load function 
	public function moduledropdownloadfunmodel() {
		$industryid = $this->Basefunctions->industryid;
		if($industryid == 2){
			$moduleid = array('4');
			$modulename = array('User');
		} else if($industryid == 4) {
			$moduleid = array('82','201','4');
			$modulename = array('Members','Leads','User');
		} else  {
			$moduleid = array('203','201','4');
			$modulename = array('Contact','Leads','User');
		}
		$count = count($moduleid);
		if($count != 0){
			for($i=0;$i<$count;$i++){
				$data[$i] = array('datasid'=>$moduleid[$i],'dataname'=>$modulename[$i]);
			}
			echo json_encode($data);
		} else {
			 echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//view drop down load function 
	public function viewdropdownloadfunmodel() {
		$invitemoduleid = $_GET['invitemoduleid'];
		$userid = $this->Basefunctions->userid;
		$i = 0;
		$this->db->select('viewcreationid,viewcreationname');
		$this->db->from('viewcreation');
		$this->db->where('viewcreation.viewcreationmoduleid',$invitemoduleid);
		$this->db->where_in('viewcreation.createuserid',array(1,$userid));
		$this->db->where('viewcreation.status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->viewcreationid,'dataname'=>$row->viewcreationname);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//parent table name fetch
	public function parenttablegetmodel(){
		$mid = $_GET['moduleid'];
		if($mid == '1'){$mid = '247';}
		$this->db->select('modulemastertable');
		$this->db->from('module');
		$this->db->where('module.moduleid',$mid);
		$result = $this->db->get();
		if($result->num_rows() >0){
			foreach($result->result() as $row){
				$data = $row->modulemastertable;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//From application - subscriber list view
	public function inviteusergrigdatafetchgridxmlview($tablename,$sortcol,$sortord,$pagenum,$rowscount,$colinfo,$viewcolmoduleids) {
		$count = count($colinfo['colmodelindex']);
		$dataset ='';
		$x = 0;
		$joinq = '';
		$con = '';
		$attrcond = "";
		$attrcondarr = array();
		$ptab = array($tablename);
		$jtab = array($tablename);
		$maintable = $tablename;
		$selvalue = $tablename.'.'.$tablename.'id';
		for($k=0;$k<$count;$k++) {
			if($k==0) {
				$dataset = $colinfo['colmodelindex'][$k];
			} else{
				$dataset .= ','.$colinfo['colmodelindex'][$k];
			}
			if($x != 0) { $con = "AND"; }
			//parent join check
			$ptabjoin = "";
			$ptabjoin = $colinfo['colmodelparenttable'][$k];
			if(in_array($ptabjoin,$ptab)) {
				if(!in_array($colinfo['coltablename'][$k],$jtab) && $colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
					array_push($jtab,trim($colinfo['coltablename'][$k]));
					//employee
					$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid LIKE "%1%"';
					//group
					$joinq .= ' LEFT OUTER JOIN employeegroup ON FIND_IN_SET(employeegroup.employeegroupid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid  LIKE "%2%"';
					//user role and sub ordinate
					$joinq .= ' LEFT OUTER JOIN userrole ON FIND_IN_SET(userrole.userroleid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND ('.$maintable.'.employeetypeid LIKE "%3%" OR '.$maintable.'.employeetypeid LIKE "%4%")';
				} else {
					if($colinfo['colmodeldatatype'][$k] == 1) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						}
					} else if($colinfo['colmodeldatatype'][$k] == 2) {
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else if($colinfo['colmodeldatatype'][$k] == 3) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						}
					}
				}
			} else {
				if($colinfo['colmodeldatatype'][$k] == 1) {
					$result = $this->Basefunctions->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.','.$maintable.'.'.$primaryid.') > 0 AND '.$ptabjoin.'.status=1';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 1) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
									}
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 2) {
					$joinresult = $this->Basefunctions->parentjointableinfo($ptabjoin,$viewcolmoduleids);
					if($joinresult->num_rows() >0) {
						foreach($joinresult->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
							//condition name
							$condcolname = $show->viewcreationcolmodelcondname;
							//condition value
							$condcolvalue = $show->viewcreationcolmodelcondvalue;
						}
						//restrict repeated parent join
						if(in_array($destab,$ptab)) {
							if($destab != $soucrcetab) {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							} else {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							}
						}
						//parent table condition
						if($condcolname != "" && $condcolvalue != "") {
							$attrcond .= " ".$con." ".$srcjointab.'.'.$condcolname."=".$condcolvalue;
							array_push($attrcondarr,$srcjointab.'.'.$joincolname);
							$x = 1;
						}
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] = $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.'='.$maintable.'.'.$primaryid.') > 0 AND '.$ptabjoin.'.status=1';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 2) {
								//attribute join
								if(!in_array($colinfo['coltablename'][$k],$jtab)) {
									array_push($jtab,trim($colinfo['coltablename'][$k]));
									$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
								}
								//condition
								if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
									$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
									array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
									$x = 1;
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 3) {
					$result = $this->Basefunctions->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						}
					}  else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.','.$maintable.'.'.$primaryid.') > 0 AND '.$ptabjoin.'.status=1';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 3) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
									}
								}
							}
						}
					}
				}
			}
			$dduitypeids = array('17','18','19','23','25','26','27','28','29');
			//fields fetch
			if($colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->Basefunctions->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].', GROUP_CONCAT(DISTINCT userrole.userrolename) AS rolename, GROUP_CONCAT(DISTINCT employeegroup.employeegroupname) AS empgrpname';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k].',userrole.userrolename AS rolename,employeegroup.employeegroupname AS empgrpname';
				}
			} else if( in_array($colinfo['colmodeluitype'][$k],$dduitypeids) ) {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->Basefunctions->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].'';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
				}
			} else {
				$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
			}
		}
		$status = $tablename.'.status IN (1)';
		$where = '1=1';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		//query
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$selvalue.' from '.$tablename.' '.$joinq.' WHERE '.$where.' AND '.$status.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		return $data;
	}
	//invite user desktop notification sent
	public function inviteusernotification($leadinviteid,$contactinviteid,$userinviteid,$groupinviteid,$rolesinviteid,$randsinviteid,$primaryid,$activityname,$startdate) {
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		
		if($leadinviteid != '') {
			$leadexp = explode('|',$leadinviteid);
			$leadinvite = explode(',',$leadexp[1]);
			$lcount = count($leadinvite);
			for($i=0;$i<$lcount;$i++) {
				$invitemsg = $empname." "."Invited You On"." - ".$activityname.' Starts on '.$startdate;
				$this->Basefunctions->notificationcontentadd($primaryid,'Invite',$invitemsg,$leadinvite[$i],$leadexp[0]);
			}
		}
		if($contactinviteid != '') {
			$contexp = explode('|',$contactinviteid);
			$continvite = explode(',',$contexp[1]);
			$ccount = count($continvite);
			for($i=0;$i<$ccount;$i++) {
				$invitemsg = $empname." "."Invited You On"." - ".$activityname.' Starts on '.$startdate;
				$this->Basefunctions->notificationcontentadd($primaryid,'Invite',$invitemsg,$continvite[$i],$contexp[0]);
			}
		}
		if($userinviteid != '') {
			$userexp = explode('|',$userinviteid);
			$userinvite = explode(',',$userexp[1]);
			$ucount = count($userinvite);
			for($i=0;$i<$ucount;$i++) {
				$invitemsg = $empname." "."Invited You On"." - ".$activityname.' Starts on '.$startdate;
				$this->Basefunctions->notificationcontentadd($primaryid,'Invite',$invitemsg,$userinvite[$i],$userexp[0]);
			}
		} 
		if($groupinviteid != '') {
			$j=0;
			$grpexp = explode('|',$groupinviteid);
			$grpinvite = explode(',',$grpexp[1]);
			$gcount = count($grpinvite);
			for($i=0;$i<$gcount;$i++) {
				$empdataids[$j] = $this->Basefunctions->empidfetchfromgroup($grpinvite[$i]);
				$dcount = count($empdataids[$j]);
				for($k=0;$k<$dcount;$k++) {
					$invitemsg = $empname." "."Invited You On"." - ".$activityname.' Starts on '.$startdate;
					$this->Basefunctions->notificationcontentadd($primaryid,'Invite',$invitemsg,$empdataids[$j][$k],4);
				}
			}
		}
		if($rolesinviteid != '') {
			$j=0;
			$roleexp = explode('|',$rolesinviteid);
			$roleinvite = explode(',',$roleexp[1]);
			$rcount = count($roleinvite);
			for($i=0;$i<$rcount;$i++) {
				$emproledataids[$j] = $this->Basefunctions->empidfetchfromroles($roleinvite[$i]);
				$dcount = count($emproledataids[$j]);
				for($k=0;$k<$dcount;$k++) {
					$invitemsg = $empname." "."Invited You On"." - ".$activityname.' Starts on '.$startdate;
					$this->Basefunctions->notificationcontentadd($primaryid,'Invite',$invitemsg,$emproledataids[$j][$k],4);
				}
			}
		}
	}
	//mail sent
	public function mailnotificationsent($invitemail,$primaryid,$taskname,$date,$datacontent,$subaccountid ) {
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		$empmailid = $this->Basefunctions->generalinformaion('employee','emailid','employeeid',$empid);
		$mid=array();
		$mailid = explode(',',$invitemail);
		$lcount = count($mailid);
		for($i=0;$i<$lcount;$i++) {
			$mid[$i] = array( 'email' =>$mailid[$i] );
		}
		if($mid != '') {
			$message = array(
					'html' => $datacontent,
					'text' => '',
					'subject' => 'Activity Invitation Mail',
					'from_email' => $empmailid,
					'from_name' => $empname,
					'to' => $mid,
					'track_opens' => true,
					'track_clicks' => true,
					'inline_css' => true,
					'url_strip_qs' => true,
					'important' => true,
					'auto_text' => true,
					'auto_html' => true,
				);
			$send_at ='';		
			$this->mailsentfunction($message,$send_at);
			$ddate = date($this->Basefunctions->datef);
			$currentuserid = $this->Basefunctions->userid;
			$sql = $this->db->query("INSERT INTO crmmaillog (communicationfrom,communicationto,subject,employeeid,moduleid,commonid,emailtemplatesid,signatureid,communicationdate,createdate,lastupdatedate,createuserid,lastupdateuserid,status) VALUES ('".$empmailid."','".$invitemail."','Activity Invitation Mail','".$currentuserid."','205','".$primaryid."','1','1','".$ddate."','".$ddate."','".$ddate."','".$currentuserid."','".$currentuserid."','1')");
			//notification log entry
			$pprimaryid = $this->Basefunctions->maximumid('crmmaillog','crmmaillogid');
			$notimsg = $empname." "."Sent a Test Mail to"." - ".$invitemail;
			$this->Basefunctions->notificationcontentadd($pprimaryid,'Mail',$notimsg,1,242);
			$credits = $this->db->query("UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = `addonavailablecredit` -1 WHERE `addonstypeid` = '3'");
			//master company id get - master db update
			$mastercompanyid = $this->Basefunctions->generalinformaion('company','mastercompanyid','companyid',2);
			$CI =& get_instance();
			$CI->load->database();
			$hostname = $CI->db->hostname; 
			$username = $CI->db->username; 
			$password = $CI->db->password; 
			$masterdb = $CI->db->masterdb; 
			$con = mysqli_connect($hostname,$username,$password,$masterdb);
			// Check connection
			if (mysqli_connect_errno())	{
				echo "Failed to connect to MySQL: " . mysqli_connect_error();
			}
			mysqli_query($con,"UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = `addonavailablecredit` - $lcount WHERE `addonstypeid` = 3 and `mastercompanyid` ='".$mastercompanyid."'");
			mysqli_close($con);
		}
	}
	//mail notification sent
	public function mailsentfunction($message,$send_at) {
		require_once 'application/third_party/mandrill-api-php/src/Mandrill.php';
		try {
			$mandrill = new Mandrill('X9WnMXpxhC8YEOUPuQ3oZw');
			$async = true; //for bulk message
			$ip_pool = '';
			$result = $mandrill->messages->send($message, $async, $ip_pool, $send_at);
		} catch(Mandrill_Error $e) {
			// Mandrill errors are thrown as exceptions
			echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			throw $e;
		}
	}
	//mobile number and email check - validate
	public function mobileandmailvalidationmodel() {
		$leadinviteid = $_POST['leadid'];
		$contactinviteid = $_POST['contactid'];
		$userinviteid = $_POST['userid'];
		$groupinviteid = $_POST['groupid'];
		$rolesinviteid = $_POST['rolesid'];
		$randsinviteid = $_POST['randsid'];
		$emailid = array();$j=0;
		$mobilenumber = array();$l =0;
		$unmobleadname = array();
		$unmailleadname = array();
		if($leadinviteid != '') {
			$leadexp = explode('|',$leadinviteid);
			$leadinvite = explode(',',$leadexp[1]);
			$lcount = count($leadinvite);
			$k =0;
			$m=0;
			for($i=0;$i<$lcount;$i++) {
				$leademailid = $this->Basefunctions->generalinformaion('lead','emailid','leadid',$leadinvite[$i]);
				if($leademailid != '') {
					$emailid[$j] = $leademailid;
					$j++;
				} else {
					$leadfname = $this->Basefunctions->generalinformaion('lead','leadname','leadid',$leadinvite[$i]);
					$leadname = $this->Basefunctions->generalinformaion('lead','lastname','leadid',$leadinvite[$i]);
					$unmailleadname[$k] = $leadfname.' '.$leadname;
					$k++;
				}
				$leadmobilenum = $this->Basefunctions->generalinformaion('lead','mobilenumber','leadid',$leadinvite[$i]);
				if($leadmobilenum != '') {
					$mobilenumber[$l] = $leadmobilenum;
					$l++;
				} else {
					$leadfname = $this->Basefunctions->generalinformaion('lead','leadname','leadid',$leadinvite[$i]);
					$leadname = $this->Basefunctions->generalinformaion('lead','lastname','leadid',$leadinvite[$i]);
					$unmobleadname[$m] = $leadfname.' '.$leadname;
					$m++;
				}
			}
		}
		$unmobilecontact = array();
		$unemailcontact = array();
		if($contactinviteid != '') {
			$contexp = explode('|',$contactinviteid);
			$continvite = explode(',',$contexp[1]);
			$ccount = count($continvite);
			$a = 0;
			$b = 0;			
			for($i=0;$i<$ccount;$i++) {
				$contactemailid = $this->Basefunctions->generalinformaion('contact','emailid','contactid',$continvite[$i]);
				if($contactemailid != '') {
					$emailid[$j] = $contactemailid;
					$j++;
				} else {
					$contfname = $this->Basefunctions->generalinformaion('contact','contactname','contactid',$continvite[$i]);
					$contname = $this->Basefunctions->generalinformaion('contact','lastname','contactid',$continvite[$i]);
					$unemailcontact[$a] = $contfname.' '.$contname;
					$a++;
				}
				$contmobilenum = $this->Basefunctions->generalinformaion('contact','mobilenumber','contactid',$continvite[$i]);
				if($contmobilenum != '') {
					$mobilenumber[$l] = $contmobilenum;
					$l++;
				} else {
					$contfname = $this->Basefunctions->generalinformaion('contact','contactname','contactid',$continvite[$i]);
					$contname = $this->Basefunctions->generalinformaion('contact','lastname','contactid',$continvite[$i]);
					$unmobilecontact[$b] = $contfname.' '.$contname;
					$b++;
				}
			}
		}
		$unmobileuser = array();
		$unemailuser = array();
		if($userinviteid != '') {
			$userexp = explode('|',$userinviteid);
			$userinvite = explode(',',$userexp[1]);
			$ucount = count($userinvite);
			$d = 0;
			$c = 0;
			for($i=0;$i<$ucount;$i++) {
				$useremailid = $this->Basefunctions->generalinformaion('employee','emailid','employeeid',$userinvite[$i]);
				if($useremailid != '') {
					$emailid[$j] = $useremailid;
					$j++;
				} else {
					$userfname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$userinvite[$i]);
					$username = $this->Basefunctions->generalinformaion('employee','lastname','employeeid',$userinvite[$i]);
					$unemailuser[$d] = $userfname.' '.$username;
					$d++;
				}
				$usermobilenum = $this->Basefunctions->generalinformaion('employee','mobilenumber','employeeid',$userinvite[$i]);
				if($usermobilenum != '') {
					$mobilenumber[$l] = $usermobilenum;
					$l++;
				} else {
					$userfname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$userinvite[$i]);
					$username = $this->Basefunctions->generalinformaion('employee','lastname','employeeid',$userinvite[$i]);
					$unmobileuser[$c] = $userfname.' '.$username;
					$c++;
				}
			}
		}
		$unmobile = "";
		if(!empty($unmobleadname) || !empty($unmobilecontact) || !empty($unmobileuser)){
			
			$impunmobleadname = implode(',',$unmobleadname);
			$impunmobilecontact = implode(',',$unmobilecontact);
			$impununmobileuser = implode(',',$unmobileuser);
			$str = "";
			if($impunmobleadname){
				$str .= $impunmobleadname;
			}
			if($impunmobilecontact){
				$str .= $impunmobilecontact;
			}
			if($impununmobileuser){
				$str .= $impununmobileuser;
			}
			$unmobile = $str;
		}
		$umemailid = "";
		if(!empty($unmailleadname) || !empty($unemailcontact) || !empty($unemailuser)){
			$impunmailleadname = implode(',',$unmailleadname);
			$impunemailcontact = implode(',',$unemailcontact);
			$impunemailuser = implode(',',$unemailuser);
			$mstr = "";
			if($impunmailleadname){
				$mstr .= $impunmailleadname;
			}
			if($impunemailcontact){
				$mstr .= $impunemailcontact;
			}
			if($impunemailuser){
				$mstr .= $impunemailuser;
			}
			$umemailid = $mstr;
		}
		$data = array('mobilenum'=>$mobilenumber,'emailid'=>$emailid,'withoutmoble'=>$unmobile,'withoutmail'=>$umemailid);
		echo json_encode($data);
	}
	//reminder validation
	public function reminderintervalchecknmodel() {
		//reminder details
		$reminder = $_GET['reminderval'];
		$remindervalue = $this->remindervalueget($reminder);
		//data details
		$startdate = $_GET['sdate'];
		$currentdate = date('d-m-Y', time());
		$starttime = $_GET['stime'];
		date_default_timezone_set("Asia/Kolkata");
		$currentdate = date('d-m-Y H:i:s', time());
		$selectdate = $startdate.' '.$starttime;
		if(strtotime($startdate) > strtotime($currentdate)) {
			$data = 'True';
			echo json_encode($data);
		} else {
			$currenttime = date('H:i:s', time());
			if(strtotime($starttime) >= strtotime($currenttime)) {
				$diff = $starttime - $currenttime;
				if($diff = '1') {
					$time = strtotime($starttime);
					$time = $time - ($remindervalue * 60);
					$date = date("H:i:s", $time);
					if(strtotime($date) >= strtotime($currenttime)) {
						$datetime2 = new DateTime($starttime);
						$datetime1 = new DateTime($currenttime);
						$interval = $datetime2->diff($datetime1);
						$elapsed = $interval->format('%i');
						if($elapsed >= $remindervalue) {
							$data = 'True';
						} else {
							$data = 'False';
						}
					} else {
						$data = 'False';
					} 
				} else if($diff = '0') {
					$time = strtotime($starttime);
					$time = $time - ($remindervalue * 60);
					$date = date("H:i:s", $time);
					if(strtotime($date) >= strtotime($currenttime)) {
						$datetime2 = new DateTime($starttime);
						$datetime1 = new DateTime($currenttime);
						$interval = $datetime2->diff($datetime1);
						$elapsed = $interval->format('%i');
						if($elapsed >= $remindervalue) {
							$data = 'True';
						} else {
							$data = 'False';
						}
					} else {
						$data = 'False';
					} 
				} else {
					$data = 'True';
				}
			} else {
				$data = 'totalFalse';
			}
			echo json_encode($data);
		}  
	}
	//reminder value get
	public function remindervalueget($reminder) {
		$reminid = explode(',',$reminder);
		$industryid = $this->Basefunctions->industryid;
		for($i=0;$i<count($reminid);$i++) {
			$this->db->select('reminderintervalname');
			$this->db->from('reminderinterval');
			$this->db->where_in('reminderinterval.reminderintervalid',$reminid[$i]);
			$this->db->where("FIND_IN_SET('".$industryid."',reminderinterval.industryid) >", 0);
			$result = $this->db->get();
			if($result->num_rows() > 0) {
				foreach($result->result() as $row) {
					$data = $row->reminderintervalname;
				}
			}
		}
		return $data;
	}
	//recurrence data fetch
	public function recurrecneondaynmodel() {
		$i=0;
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('recurrenceontheid,recurrenceonthename');
		$this->db->from('recurrenceonthe');
		$this->db->where('recurrenceonthe.moduleid',205);
		$this->db->where('recurrenceonthe.status',1);
		$this->db->where("FIND_IN_SET('".$industryid."',recurrenceonthe.industryid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->recurrenceontheid,'dataname'=>$row->recurrenceonthename);
				$i++;
			}
			echo json_encode($data);
		} else {
			 echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//template name fetch
	public function templateddvalfetchmodel() {
		$i=0;
		$industryid = $this->Basefunctions->industryid;
		$tablename = $_GET['tablename'];
		$fieldid = $_GET['fieldid'];
		$fieldname = $_GET['fieldname'];
		$moduleid = $_GET['moduleid'];
		$type = $_GET['type'];
		$this->db->select("$fieldid,$fieldname,createuserid");
		$this->db->from("$tablename");
		$this->db->where("$tablename".'.moduleid',$moduleid);
		if($type == 'sms') {
			$this->db->where("$tablename".'.smssendtypeid',2);
			$this->db->where("$tablename".'.templatetypeid',2);
		}
		$this->db->where("FIND_IN_SET('".$industryid."',$tablename.industryid) >", 0);
		$this->db->where("$tablename".'.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				if($row->createuserid == 1) {
					$setdefault ='Yes';
				} else {
					$setdefault ='No';
				}
				$data[$i] = array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname,'setdefault'=>$setdefault);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}	
	}
	//template value fetch on update
	public function templatenamefetchmodel() {
		$id = $_GET['datarowid'];
		$this->db->select('emailtemplatesid,leadtemplateid');
		$this->db->from('crmactivity');
		$this->db->where('crmactivity.crmactivityid',$id);
		$result = $this->db->get();
		if($result->num_rows() >0 ) {
			foreach($result->result() as $row){
				$data = array('emailtemplatesid'=>$row->emailtemplatesid,'leadtemplateid'=>$row->leadtemplateid);
			}
			echo json_encode($data);
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}	
	}
	//check the mail add ons added or not 
	public function subaccountdetailsfetchmodel() {
		$data = '';
		$this->db->select('generalsettingid,mandrillaccountnumber');
		$this->db->from('generalsetting');
		$this->db->where('generalsetting.companyid',2);
		$this->db->where('generalsetting.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = $row->mandrillaccountnumber;
			}
		} else {
			$data = '';
		}
		return $data;
	}
	/* ---- For industry ---- */
	//patient based mobile and email id fetch
	public function getmobileandemailidmodel(){
		$data = '';
		$primaryid = $_GET['patientid'];
		$this->db->select('mobilenumber,emailid,landlinenumber');
		$this->db->from('contact');
		$this->db->where('contact.contactid',$primaryid);
		$this->db->where('contact.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('mobilenumber'=>$row->mobilenumber,'emailid'=>$row->emailid,'landlinenumber'=>$row->landlinenumber);
			}
		} else {
			$data = array('fail'=>'FAILED');
		}
		echo json_encode($data);
	}
	/* ---- For General ---- */
	public function gettemplatesidmodel(){
		$id = $_GET['actid'];
		if($id) {
			$this->db->select('leadtemplateid,emailtemplatesid,invitemode,invitesmstemplate,invitemailtemplate,inviteviewname,leadid');
			$this->db->from('crmactivity');
			$this->db->where('crmactivity.crmactivityid',$id);
			$result = $this->db->get()->row_array();
			$smsid = $result['leadtemplateid'];
			$emailid = $result['emailtemplatesid'];
			$invmodeid = $result['invitemode'];
			$invsmsid = $result['invitesmstemplate'];
			$inviteviewname = $result['inviteviewname'];
			$moduleid = $result['leadid'];
			$data = array(
						'status'=>'success',
						'smsid' => $smsid,
						'emailid' => $emailid,
						'invmodeid' => $invmodeid,
						'invsmsid' => $invsmsid,
						'invemailid' => $invemailid,
						'viewid' => $inviteviewname,
						'moduleid' => $moduleid,
					);
		} else {
			$data = array('status'=>'FAILED');
		}
		echo json_encode($data);
	}
	//preview and print pdf
	public function generateprinthtmlfile($print_data) {
		$this->load->model('Printtemplates/Printtemplatesmodel');
		//defaults
		$printdetails['moduleid'] = '1';
		$htmldatacontents='';
			
		$templateid = ( (isset($print_data['templateid']))? $print_data['templateid'] : 1);
		$id = ( (isset($print_data['primaryid']))? $print_data['primaryid'] : 1);
		$this->db->select('emailtemplates.emailtemplatesemailtemplate_editorfilename,emailtemplates.emailtemplatesid,emailtemplates.moduleid,emailtemplates.emailtemplatesname');
		$this->db->from('emailtemplates');
		$this->db->where('emailtemplates.emailtemplatesid',$templateid);
		$result = $this->db->get();
		foreach($result->result() as $rowdata) {
			$printdetails['contentfile'] =  $rowdata->emailtemplatesemailtemplate_editorfilename;
			$printdetails['moduleid'] =  $rowdata->moduleid;
			$printdetails['templatename'] = $rowdata->emailtemplatesname;
		}
		$parenttable = $this->Basefunctions->generalinformaion('module','modulemastertable','moduleid',$printdetails['moduleid']);
	
		//body data
		$bodycontent = $this->Printtemplatesmodel->filecontentfetch($printdetails['contentfile']);
		$bodycontent = preg_replace('~a10s~','&nbsp;', $bodycontent);
		$bodycontent = preg_replace('~<br>~','<br />', $bodycontent);
		$bodycontent = $this->Printtemplatesmodel->removespecialchars($bodycontent);
		$bodyhtml = $this->Printtemplatesmodel->generateprintingdata($bodycontent,$parenttable,$print_data,$printdetails['moduleid']);
		$bodyhtml = $this->Printtemplatesmodel->addspecialchars($bodyhtml);
		return $bodyhtml; 
	}
	//preview and print pdf
	public function smsgenerateprinthtmlfile($print_data) {
		$this->load->model('Printtemplates/Printtemplatesmodel');
		//defaults
		$printdetails['moduleid'] = '1';
		$htmldatacontents='';
			
		$templateid = ( (isset($print_data['templateid']))? $print_data['templateid'] : 1);
		$id = ( (isset($print_data['primaryid']))? $print_data['primaryid'] : 1);
		$this->db->select('templates.leadtemplatecontent_editorfilename,templates.templatesid,templates.moduleid,templates.templatesname');
		$this->db->from('templates');
		$this->db->where('templates.templatesid',$templateid);
		$result = $this->db->get();
		foreach($result->result() as $rowdata) {
			$printdetails['contentfile'] =  $rowdata->leadtemplatecontent_editorfilename;
			$printdetails['moduleid'] =  $rowdata->moduleid;
			$printdetails['templatename'] = $rowdata->templatesname;
		}
		$parenttable = $this->Basefunctions->generalinformaion('module','modulemastertable','moduleid',$printdetails['moduleid']);
	
		//body data
		$bodycontent = $this->Printtemplatesmodel->filecontentfetch($printdetails['contentfile']);
		$bodycontent = preg_replace('~a10s~','&nbsp;', $bodycontent);
		$bodycontent = preg_replace('~<br>~','<br />', $bodycontent);
		$bodycontent = $this->Printtemplatesmodel->removespecialchars($bodycontent);
		$bodyhtml = $this->Printtemplatesmodel->generateprintingdata($bodycontent,$parenttable,$print_data,$printdetails['moduleid']);
		$bodyhtml = $this->Printtemplatesmodel->addspecialchars($bodyhtml);
		return $bodyhtml;
	}
	//name based email id and mobile number
	public function getmobilenumberemailidmodel(){
		$commonid = $_GET['commonid'];
		$moduleid = $_GET['moduleid'];
		$this->db->select('modulemastertable');
		$this->db->from('module');
		$this->db->where('moduleid',$moduleid);
		$this->db->limit(1);
		$module_data = $this->db->get();
		if($module_data->num_rows() > 0){
			foreach($module_data->result() as $info){
				$moduletable=$info->modulemastertable;
			}
		}
		$table = $moduletable;
		$tableid = $moduletable.'id';
		$tablename = $moduletable.'name';
		$mcheck = $this->fieldexistcheck($table,'mobilenumber');
		if($mcheck == 'TRUE') {
			$mobilenumber = $this->Basefunctions->generalinformaion($table,'mobilenumber',$tableid,$commonid);
		} else { 
			$mobilenumber = '';
		}
		$echeck = $this->fieldexistcheck($table,'emailid');
		if($echeck == 'TRUE') {
			$emailid = $this->Basefunctions->generalinformaion($table,'emailid',$tableid,$commonid);
		} else {
			$emailid = '';
		}
		
		$array = array('mobile'=>$mobilenumber,'emailid'=>$emailid);
		echo json_encode($array);
	}
	//field exist check
	public function fieldexistcheck($table,$field){
		$dbname = $this->db->database;
		$value = $this->db->query("SELECT COLUMN_NAME FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '".$dbname."' AND TABLE_NAME = '".$table."' AND COLUMN_NAME = '".$field."'");
		if($value->num_rows() >0 ) {
			return "TRUE";
		} else {
			return "FALSE";
		}
	}
}