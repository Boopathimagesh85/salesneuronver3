<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Model File
class Picklistdepmodel extends CI_Model{    
    public function __construct(){		
		parent::__construct(); 
		$this->load->model('Base/Crudmodel');
    }
	//table field data type check
	public function tablefieldtypecheck($tblname,$fieldname) {
		$fields = $this->db->field_data($tblname);
		foreach ($fields as $field) {
			$name =  $field->name;
			if($fieldname == $name) {
				return $field->type;
			}
		}
	}
	//table field check
	public function tablefieldnamecheck($tblname,$fieldname) {
		$fields = $this->db->field_exists($fieldname,$tblname);
		if($fields == '1') {
			return 'Yes';
		} else {
			return 'No';
		}
	}
	//pick list dependency name get
	public function ddparentablegetmodel() {
		$id = $_GET['id'];
		$this->db->select('columnname');
		$this->db->from('modulefield');
		$this->db->where('modulefield.modulefieldid',$id);
		$result = $this->db->get();
		if($result->num_rows > 0) {
			foreach($result->result() as $row) {
				$data = $row->columnname;
			}
			echo $data;
		}
	}
	//pick list parent table value fetch
	public function sourcetablevaluefetchmodel() {
		$i = 0;
		$industryid = $this->Basefunctions->industryid;
		$tablenameid = $_GET['data'];
		$moduleid = $_GET['moduleid'];
		$tablename = substr($tablenameid,0,-2);
		$tablefieldname = $tablename.'name';
		$this->db->select($tablenameid.','.$tablefieldname);
		$this->db->from($tablename);
		$this->db->where('status',1);
		$this->db->where("FIND_IN_SET( $moduleid, $tablename.moduleid )");
		$this->db->where("FIND_IN_SET( $industryid, $tablename.industryid ) > ",0);
		$result = $this->db->get();
		foreach($result->result() as $row) {
			$data[$i] = array('datasid'=>$row->$tablenameid,'dataname'=>$row->$tablefieldname);
			$i++;
		}
		if($data !='') {
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	public function picklistdepgridinfomodel($tablename,$sortcol,$sortord,$pagenum,$rowscount,$moduleid,$sourcetblid,$targettablid) {
		$data = array();
		$sourcetblname = substr($sourcetblid,0,-2);
		$targettablname = substr($targettablid,0,-2);
		$selvalue = $sourcetblname.'.'.$sourcetblname.'id,'.$sourcetblname.'.'.$sourcetblname.'name,'.$targettablname.'.'.$targettablname.'id,'.$targettablname.'.'.$targettablname.'name,'.$targettablname.'.setdefault';
		if($sourcetblid != "" && $targettablid != "" && $sourcetblid != "undefined" && $targettablid != "undefined") {
			$fname = $this->tablefieldnamecheck($targettablname,$sourcetblid);
			if($fname == 'Yes') {
				$userroleid = $this->Basefunctions->userroleid;
				$industryid = $this->Basefunctions->industryid;
				$industry = "FIND_IN_SET( $industryid, $targettablname.industryid ) > 0";
				$status = $targettablname.'.status = 1 AND '.$sourcetblname.'.status = 1';
				$join = ' LEFT JOIN '.$sourcetblname.' ON '.$sourcetblname.'.'.$sourcetblname.'id = '.$targettablname.'.'.$sourcetblname.'id';
				/* pagination */
				$pageno = $pagenum-1;
				$start = $pageno * $rowscount;
				//query
				$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$selvalue.' from '.$targettablname.' '.$join.' WHERE '.$status.' AND '.$industry.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
			}
		}
		return $data;
	}
	public function picklistdepgridinfomodellll($sidx,$sord,$start,$limit,$wh,$moduleid,$sourcetblid,$targettablid) {
		$finalresult = array();
		//field organize
		$sourcetblname = substr($sourcetblid,0,-2);
		$targettablname = substr($targettablid,0,-2);
		$selvalue = $sourcetblname.'.'.$sourcetblname.'id,'.$sourcetblname.'.'.$sourcetblname.'name,'.$targettablname.'.'.$targettablname.'id,'.$targettablname.'.'.$targettablname.'name,'.$targettablname.'.setdefault';
		$colnames = $targettablname.'id,'.$sourcetblname.'name,'.$targettablname.'name,setdefault';
		$colinfo = explode(',',$colnames);
		if($sourcetblid != "" && $targettablid != "" && $sourcetblid != "undefined" && $targettablid != "undefined") {
			$fname = $this->tablefieldnamecheck($targettablname,$sourcetblid);
			if($fname == 'Yes') {
				$status = $targettablname.'.status = 1 AND '.$sourcetblname.'.status = 1';
				$sidxx = $targettablname.'.'.$targettablname.'id';
				$w = '1=1';
				$join = ' LEFT JOIN '.$sourcetblname.' ON '.$sourcetblname.'.'.$sourcetblname.'id = '.$targettablname.'.'.$sourcetblname.'id';
				$actsts = $this->Basefunctions->activestatus;
				$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$selvalue.' from '.$targettablname.' '.$join.' WHERE '.$status.' ORDER BY'.' '.$sidxx.' '.$sord);
				$finalresult=array($colinfo,$data);
			}
		}
		return $finalresult;
	} 
	//data create model
	public function newdatacreatemodel() {
		$tablenameid = $_POST['tabnameid'];
		$sourceid = $_POST['sourcevalue'];
		$moduleid = $_POST['picklistddmoduleid'];
		$sourcetabid = $_POST['sourcetableid'];
		$tablename = substr($tablenameid,0,-2);
		$sourcetab = substr($sourcetabid,0,-2);
		$value = $this->primarynamepd($tablename);
		if(!in_array($sourcetabid,$value)) {
			$type = $this->tablefieldtypecheck($sourcetab,$sourcetabid);
			$this->db->query("ALTER TABLE ".$tablename." ADD ".$sourcetabid." VARCHAR( 1000 ) NOT NULL DEFAULT '1' AFTER `moduleid`;");
		}
		$roleid = $this->fetchallroleids();
		$roleids = implode(',',$roleid);
		$date = date($this->Basefunctions->datef);
		$loginuserid = $this->Basefunctions->userid;
		$dataarray = array(''.$sourcetabid=>$sourceid,'moduleid'=>$_POST['picklistddmoduleid'],'userroleid'=>$roleids,'setdefault'=>'0','sortorder'=>'0','lastupdatedate'=>$date,'lastupdateuserid'=>$loginuserid);
		$this->db->where($tablename.'id',$_POST['targetvalue']);
		$this->db->update( $tablename,$dataarray);
		$primaryname = $tablename.'id';
		$this->db->query("UPDATE modulefield SET uitypeid = 18,ddparenttable = '".$sourcetab."',lastupdatedate='".$date."',lastupdateuserid='".$loginuserid."' WHERE columnname ='".$tablenameid."' AND moduletabid ='".$moduleid."'");
		echo "TRUE";  
	}
	//fetch all role ids
	public function fetchallroleids() {
		$roleids = array();
		$i=0;
		$roledata = $this->db->select('userroleid')->from('userrole')->where('status',1)->get();
		foreach($roledata->result() as $data) {
			$roleids[$i]=$data->userroleid;
			$i++;
		}
		return $roleids;
	}
	//for pick list dependency
	public function primarynamepd($table) {
		$database = $this->db->database;
		$i=0;
		$value = $this->db->query("SELECT `COLUMN_NAME` FROM `information_schema`.`COLUMNS` WHERE (`TABLE_SCHEMA` = '".$database."') AND (`TABLE_NAME` = '$table' )");
		foreach($value->result() as $row) {
			$result[$i] = $row->COLUMN_NAME;
			$i++;
		}
		return $result;
	}
	//pick list dependency value get
	public function picklistdepvaluegetfunmodel() {
		$tabnameid = $_POST['tabnameid']; 
		$tablename = substr($tabnameid,0,-2);
		$id = $_POST['id']; 
		$sourceid = $_POST['sourceid'];
		$this->db->select($sourceid,false);
		$this->db->from($tablename);
		$this->db->where($tablename.'.'.$tabnameid,$id);
		$result=$this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = $row->$sourceid;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//pick list updation
	public function picklistdepdataupdatefunmodel() {
		$pid = $_POST['picklistdependencyprimarydataid'];
		$tablenameid = $_POST['tabnameid'];
		$sourceid = $_POST['sourcevalue'];
		$moduleid = $_POST['picklistddmoduleid'];
		$sourcefieldid = $_POST['sourcefieldid'];
		$tablename = substr($tablenameid,0,-2);
		$sourcetab = substr($sourcefieldid,0,-2);
		$value = $this->primarynamepd($tablename);
		if(!in_array($sourcefieldid,$value)) {
			$this->db->query("ALTER TABLE ".$tablename." ADD ".$sourcefieldid." MEDIUMINT UNSIGNED NOT NULL DEFAULT '1' AFTER `moduleid`;");
		}
		$userroleid = $this->Basefunctions->userroleid;
		$dataarray = array(
			''.$sourcefieldid=>$sourceid,'moduleid'=>$_POST['picklistddmoduleid'],'userroleid'=>$userroleid,'setdefault'=>'0','sortorder'=>'0'
		);
		$defdataarr = $this->Crudmodel->updatedefaultvalueget();
		$newdata = array_merge($dataarray,$defdataarr);
		$this->db->where($tablename.'.'.$tablenameid,$pid);
		$this->db->update($tablename,$newdata);
		$this->db->query("UPDATE modulefield SET uitypeid = 18,ddparenttable = '".$sourcetab."' WHERE columnname ='".$tablenameid."' AND moduletabid =".$moduleid);
		echo 'success';
	}
	//pick list dependency delete function
	public function deletepicklistdepvaluemodel($tablename,$tablenameid,$selid)	{
		$date = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$check = $this->createuseridcheck($tablename,$tablenameid,$selid);
		if($check == "TRUE") {
			$ststus = $tablename.".status=0";
			$updatedate =  $tablename.".lastupdatedate='".$date."'";
			$updateuser =  $tablename.".lastupdateuserid=".$userid;
			$update = $ststus.','.$updatedate.','.$updateuser;
			$value = $this->db->query("UPDATE"." ".$tablename." SET ".$update." WHERE ".$tablename.".".$tablenameid." IN (".$selid.")" );
			echo 'TRUE';
		} else {
			echo 'FALSE';
		}
	}
	//create user id check
	public function createuseridcheck($tablename,$tablenameid,$selid) {
		$data ='1';
		$ids = explode(',',$selid);
		$this->db->select('createuserid');
		$this->db->from($tablename);
		$this->db->where_in($tablename.'.'.$tablenameid,$ids);
		$result = $this->db->get();
		foreach($result->result() as $row) {
			$data = $row->createuserid;
		}
		if($data != '1') {
			return "TRUE";
		} else {
			return "FALSE";
		}
	}
	//unique name check
	public function uniquedynamicviewnamecheckmodel() {
		$primaryid=$_POST['primaryid'];
		$accname=$_POST['accname'];
		$elementpartableid = $_POST['elementspartabname'];
		$partable = substr($elementpartableid, 0, -2);
		$ptid = $partable.'id';
		$ptname = $partable.'name';
		if($accname != "") {
			$result = $this->db->select($ptname.','.$ptid)->from($partable)->where($partable.'.'.$ptname,$accname)->where($partable.'.status',1)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->$ptid;
				}
			} else { echo "False"; }
		} else { echo "False"; } 
	}
	//data sort order
	public function datarowsortingmodel(){
		$rowids = $_POST['rowids'];
		$tablename = $_POST['tablename'];
		$tableid = $_POST['primaryid'];
		$sortfield = $_POST['sortfield'];
		$dataids = explode(',',$rowids);
		$i=1;
		foreach($dataids as $id) {
			$this->db->query("UPDATE"." ".$tablename." SET ".$tablename.".".$sortfield."=".$i." WHERE ".$tablename.".".$tableid."=".$id );
			$i++;
		}
		echo "TRUE";
	}
}     