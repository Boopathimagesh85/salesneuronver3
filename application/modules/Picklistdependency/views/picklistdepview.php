<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
</head>
<body>
	<?php
		$dataset['gridenable'] = "1"; //0-no  1-yes
		$dataset['griddisplayid'] = "picklistdepcreationview";
		$dataset['maingridtitle'] = $gridtitle['title'];
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['spanattr'] = array();
		$this->load->view('picklistdepaddform',$dataset); 
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$this->load->view('Base/overlaymobile');
		} else {
			$this->load->view('Base/overlay');
		}
	?>
</body>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js Files -->	
	<script src="<?php echo base_url();?>js/Picklistdependency/picklistdependency.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/sortable.js" type="text/javascript"></script>
</html>
