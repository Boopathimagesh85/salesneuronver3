<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Picklistdependency extends MX_Controller {
	//To load the Register View
	function __construct() {
    	parent::__construct();
    	$this->load->model('Base/Basefunctions');
		$this->load->helper('formbuild');
		$this->load->model('Picklistdependency/Picklistdepmodel');
    }
	public function index() {
		$moduleid = array(212);
		sessionchecker(212);
		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		
		$viewfieldsmoduleids = array(212);
		$viewmoduleid = array(212);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Picklistdependency/picklistdepview',$data);
	}
	//picklist dependency parent table name get
	public function ddparentableget() {
		$this->Picklistdepmodel->ddparentablegetmodel();
	}
	//parent table name value fetch
	public function sourcetablevaluefetch() {
		$this->Picklistdepmodel->sourcetablevaluefetchmodel();
	}
	//fetch json data information
	public function picklistdepgridinfo() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$chkbox = $_GET['checkbox'];
		$srctabnameid = $_GET['srctabname'];
		$tartabnameid = $_GET['tartabname'];
		$targettablename = substr($tartabnameid,0,-2);
		$sourcetblname = substr($srctabnameid,0,-2);
		$primaryid = $targettablename.'id';
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : $targettablename.'.'.$primaryid) : $targettablename.'.'.$primaryid;
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array($sourcetblname.'name',$targettablename.'name',$targettablename.'id'),'colmodelindex'=>array($sourcetblname.'.'.$sourcetblname.'name',$targettablename.'.'.$targettablename.'name',$targettablename.'.'.$targettablename.'id'),'coltablename'=>array($sourcetblname,$targettablename,$targettablename.'id'),'uitype'=>array('2','2','2'),'colname'=>array('Source Name','Target Name','Targetid'),'colsize'=>array('200','200','200'));
		$result=$this->Picklistdepmodel->picklistdepgridinfomodel($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$modid,$srctabnameid,$tartabnameid);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($result,$page,'');
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$targettablename,$targettablename.'id',$modid,'Picklist Dependency Value',$width,$height,$chkbox);
		} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$targettablename,$targettablename.'id',$modid,'Picklist Dependency Value',$width,$height,$chkbox);
		}
		echo json_encode($datas);
	}
	//new data create fun
	public function newdatacreate() {
		$this->Picklistdepmodel->newdatacreatemodel();
	}
	//pick list dependency value get
	public function picklistdepvaluegetfun() {
		$this->Picklistdepmodel->picklistdepvaluegetfunmodel();
	}
	//pick list dependency update
	public function picklistdepdataupdatefun() {
		$this->Picklistdepmodel->picklistdepdataupdatefunmodel();
	}
	//pick list delete dunction
	public function deletepicklistdepvalue() {
		$tablenameid = $_POST['tabnameid'];
		$tablename = substr($tablenameid,0,-2);
		$selid = $_POST['id'];
		$this->Picklistdepmodel->deletepicklistdepvaluemodel($tablename,$tablenameid,$selid);
	}
	//unique name restriction
	public function uniquedynamicviewnamecheck(){
		$this->Picklistdepmodel->uniquedynamicviewnamecheckmodel();
	}
	//data row sorting
	public function datarowsorting() {
		$this->Picklistdepmodel->datarowsortingmodel();
	}
}