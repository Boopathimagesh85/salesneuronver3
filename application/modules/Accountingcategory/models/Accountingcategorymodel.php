<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Accountingcategorymodel extends CI_Model{
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//create counter
	public function newdatacreatemodel() {
		$industryid = $this->Basefunctions->industryid;
		//table and fields information	
		$formfieldsname = explode(',',$_POST['accountingcategoryelementsname']);
		$formfieldstable = explode(',',$_POST['accountingcategoryelementstable']);
		$formfieldscolmname = explode(',',$_POST['accountingcategoryelementscolmn']);
		$elementpartable = explode(',',$_POST['accountingcategoryelementspartabname']);
		//filter unique parent table
	   	$partablename =  $this->Crudmodel->filtervalue($elementpartable);
	   	//filter unique fields table
	   	$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
	   	$tableinfo = explode(',',$fildstable);
	   	$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
		$primaryid = $this->db->insert_id();
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity = ''.$user.' created AccountingCategory - '.$_POST['accountcatalogname'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,'106');
		echo 'TRUE';
	}
	//retrive counter data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['accountcatalogelementsname']);
		$formfieldstable = explode(',',$_GET['accountcatalogelementstable']);
		$formfieldscolmname = explode(',',$_GET['accountcatalogelementscolmn']);
		$elementpartable = explode(',',$_GET['accountcatalogelementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['accountcatalogprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$sndefault = $this->Basefunctions->singlefieldfetch('sndefault','accountcatalogid','accountcatalog',$primaryid); //Check default value
		if($sndefault == 1) {
			$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
			echo $result;
		} else {
			echo json_encode(array("fail"=>'sndefault'));
		}
	}
	//update counter
	public function datainformationupdatemodel() {
		$industryid = $this->Basefunctions->industryid;
		//table and fields information
		$formfieldsname = explode(',',$_POST['accountingcategoryelementsname']);
		$formfieldstable = explode(',',$_POST['accountingcategoryelementstable']);
		$formfieldscolmname = explode(',',$_POST['accountingcategoryelementscolmn']);
		$elementpartable = explode(',',$_POST['accountingcategoryelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['accountingcategoryprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity = ''.$user.' updated AccountingCategory - '.$_POST['accountcatalogname'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$activity ,$userid,'106');
		echo $result;
	}
	//delete counter
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['accountcatalogelementstable']);
		$parenttable = explode(',',$_GET['accountcatalogparenttable']);
		$id = $_GET['accountcatalogprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$accountcatalogname = $this->Basefunctions->singlefieldfetch('accountcatalogname','accountcatalogid','accountcatalog',$id); //Check default value
		$activity = ''.$user.' deleted AccountingCategory - '.$accountcatalogname.'';
		$sndefault = $this->Basefunctions->singlefieldfetch('sndefault','accountcatalogid','accountcatalog',$id); //Check default value
		if($sndefault == 2) {
			echo 'sndefault';
		} else {
			if($ruleid == 1 || $ruleid == 2) {
				$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
				if($chek == 0) {
					echo 'Denied';
				} else {
					$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
					$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,'106');
					echo "TRUE";
				}
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,'106');
				echo "TRUE";
			}
		}
	}
	public function getcatalogparentid() {
		$parentcatalogid='';
		$catalogname='';
		$counterdefaultid='';
		$id=$_GET['id'];
		$data=$this->db->select('a.parentaccountcatalogid as parentaccountcatalogids,b.accountcatalogname as accountcatalogname')
		->from('accountcatalog as a')
		->join('accountcatalog as b','a.parentaccountcatalogid=b.accountcatalogid')
		->where('a.accountcatalogid',$id)
		->where('a.status',1)
		->get()->result();
		foreach($data as $in) {
			$parentcatalogid=$in->parentaccountcatalogids;
			$catalogname=$in->accountcatalogname;
		}
		$a=array('parentcatalogid'=>$parentcatalogid,'catalogname'=>$catalogname);
		echo json_encode($a);
	}
	public function setgroupledgertype() {
		$groupledgerid='';
		$groupledgertypeid='';
		$id=$_GET['id'];
		$data=$this->db->select('groupledgerid,groupledgertypeid')
		->from('accountcatalog')
		->where('accountcatalogid',$id)
		->get()->result();
		foreach($data as $in) {
			$groupledgerid=$in->groupledgerid;
			$groupledgertypeid=$in->groupledgertypeid;
		}
		$arraydata=array('groupledgerid'=>$groupledgerid,'groupledgertypeid'=>$groupledgertypeid);
		echo json_encode($arraydata);
	}
}