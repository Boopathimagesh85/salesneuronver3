<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends CI_Controller {
	function __construct() {
    	parent::__construct();
		$this->load->model('Base/Basefunctions');	
		$this->load->model('Home/Homemodel');
  }
    //Home Page Screen
	public function index() {
		sessionchecker(1);
		$moduleid = array(1);
		$data['dashboradviewdata'] = $this->Basefunctions->dashboarddataviewbydropdown($moduleid);
		$this->load->view('Home/index',$data);
	}
	//account expire -- user information
	public function accountexpire() {
		$data['userdata']=$this->Homemodel->subcribeuserinfo();
		$data['currency']=$this->Homemodel->subscribecurrency();
		$this->load->view('Home/accountexpire',$data);
	}
	//Account settings update
	public function accountsettings() {
		$this->load->view('Home/accountsettings');
	}
	//in line widget data delete
	public function inlinedatadelete() {
		$this->Homemodel->inlinedatadeletemodel();
	}
	//conversation create
	public function conversationcreate() {
		$this->Homemodel->conversationcreatemodel();
	}
	//simple widget generation
	public function getconversationdata() {
		$moduleid=$this->input->post('moduleid');
		$rowid=$this->input->post('rowid');
		$lastnode=$this->input->post('lastnode');
		$filter=$this->input->post('filter');
		$data=$this->Homemodel->getconversationdata($moduleid,$rowid,$lastnode,$filter);
		echo $data;
	}	
	//home dashboard activity show
	public function homedashboardactvity() {
		$this->Homemodel->homedashboardactvitymodel();
	}
	//chart Generation function
	public function getdetailsforbarchart() {
		$this->Homemodel->getdetailsforbarchartmodel();
	}
	//used for Circle lead quality
	public function totalleadquality() {
		$this->Homemodel->totalleadqualitymodel();
	}
	//total login user value
	public function totalloginuservalue() {
		$this->Homemodel->totalloginuservaluemodel();
	}
	//show notification details in home dash board
	public function homedbnotificationshow() {
		$this->Homemodel->homedbnotificationshowmodel();
	}
	//Top 10 products chart
	public function getdetailsforlinewithtrendproduct() {
		$this->Homemodel->getdetailsforlinewithtrendproductmodel();
	}
	//Notification overlay data
	public function notificationoverlayfetch() {
		$this->load->view('Base/notificationoverlay');
	}
	//support overlay fetch
	public function supportoverlayfetch(){
		$this->load->view('Base/supportoverlay');
	}
	//Notification data
	public function getnotificationdata()
	{
		$this->Homemodel->getnotificationdata();
	}
	//Notification data
	public function newgetnotificationdata()
	{
		$this->Homemodel->getnotificationdata();
	}
	//-getnotificationdatanodes
	public function getnotificationdatanodes()
	{
		$this->Homemodel->getnotificationdatanodes();
	}
	public function getuser()
	{
		$match=$this->input->post('q');
		$array=array();
		$baseurl=$this->config->base_url();
		if($match != null or $match != '') {
			//retrieve usernames
			$i=0;
			$userdata=$this->db->select('employeename,employeeid,employeeimage_path,employeeimage_fromid')
								 ->from('employee')
								 ->like('employeename',$match,'after')
								 ->where('status',$this->Basefunctions->activestatus)
								 ->get();	
			foreach($userdata->result() as $info){
				$employeesingleimg =$baseurl.'/img/blackuserimg.png';
				$employeegroupimg =$baseurl.'img/blackuserimg.png';
				$array[$i]['name']=$info->employeename;
				$employeename = str_replace(' ', '', $info->employeename);
				$array[$i]['id']=$info->employeeid.'-'.$employeename;
				if($info->employeeimage_path) {
					if($info->employeeimage_fromid == 2){
						$employeesingleimg = $baseurl.$info->employeeimage_path;
					} else{
						$employeesingleimg = $info->employeeimage_path;
					}
				}
				$array[$i]['avatar']= $employeesingleimg;
				$array[$i]['type']='individual';
				$i++;
			}
			//retrieve employeegroup
			$g_userdata=$this->db->select('employeegroupname,employeegroupid')
								 ->from('employeegroup')
								 ->like('employeegroupname',$match,'after')
								 ->where('status',$this->Basefunctions->activestatus)
								 ->get();	
			foreach($g_userdata->result() as $info){
				$array[$i]['name']=$info->employeegroupname;
				$employeegroup = str_replace(' ', '', $info->employeegroupname);
				$array[$i]['id']=$info->employeegroupid.':'.$employeegroup;
				$array[$i]['avatar']=$employeegroupimg;
				$array[$i]['type']='group';
				$array[$i]['moduleid']='';
				$i++;
			}
		}
		echo json_encode($array);
	}
	public function getconversationeditdata(){
		$rowid=$this->input->post('rowid');
		$array=$this->Homemodel->getconversationeditdata($rowid);
		echo json_encode($array);
	}
	public function updateconversationdata() {
		$rowid=$this->input->post('rowid');
		if($rowid > 0) {
			$this->Homemodel->updateconversationdata($rowid);
		} else {
			echo false;
		}
	}
	public function conversationdelete() {
		$rowid=$this->input->post('rowid');
		if($rowid > 0){
			$this->Homemodel->conversationdelete($rowid);
		} else {
			echo false;
		}
	}
	//create new reply-
	public function createreply(){	
		$this->Homemodel->createreply();		
	}
	//loads the comment data of a conversation-
	public function getconversationcomment(){	
		$this->Homemodel->getconversationcomment();		
	}	
	//download file
	public function download(){
		$path = $this->input->post('path');
		$fpath = $this->input->post('filepath');		
        ob_clean();
		$mime = 'application/force-download';
		header('Pragma: public');    
		header('Expires: 0');        
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Cache-Control: private',false);
		header('Content-Type: '.$mime);
		header('Content-Disposition: attachment; filename="'.basename($path).'"');
		header('Content-Transfer-Encoding: binary');
		header('Connection: close');		
		readfile(str_replace(" ", "%20", $path));		
		exit();       
	}
	//reset notification counter
	public function resetnotificationcounter()
	{
		$this->Homemodel->resetnotificationcounter();
	}
	//notification recent counter
	public function getnotificationcount()
	{
		$this->Homemodel->getnotificationcount();
	}
	//widget filter field values
	public function loadfilterfield()
	{
		$this->Homemodel->loadfilterfield();
	}
	//Filter based content data
	public function loadfilterdata()
	{
		$this->Homemodel->loadfilterdata();
	}
	//For template value fetch
	public function smstemplateddvalfetch() {
		$this->Homemodel->smstemplateddvalfetchmodel();
	}
	//For SMS send
	public function quickesmssend() {
		$this->Homemodel->quickesmssendmodel();
	}
	//mobile number fetch
	public function mobilenumfetch() {
		$this->Homemodel->mobilenumfetchmodel();
	}
	//sms log more data
	public function getsmslogmoredata() {
		$data = '';
		$moduleid = $_POST['moduleid'];
		$modname = $_POST['modname'];
		$rowid = $_POST['dashboardrowid'];
		$lastnode = $_POST['lastnode'];
		$data = $this->Homemodel->getsmslogdata($moduleid,$rowid,$lastnode,$modname);
		if($data != ''){
			echo $data;
		} else {
			echo $data;
		}
	}
	//inbound call /outbound call log more data
	public function getinboundcalllogmoredata() {
		$data = '';
		$moduleid = $_POST['moduleid'];
		$modname = $_POST['modname'];
		$rowid = $_POST['dashboardrowid'];
		$lastnode = $_POST['lastnode'];
		$data = $this->Homemodel->inboundcalllogdata($moduleid,$rowid,$lastnode,$modname);
		if($data != ''){
			echo $data;
		} else {
			echo $data;
		}
	}
	//outgoing call module dd load-
	public function moduleddvalfetch() {
		$this->Homemodel->moduleddvalfetchmodel();
	}
	//outgoing call record dd load-
	public function recordvaluefetch() {
		$this->Homemodel->recordvaluefetchmodel();
	}
	//related module data fetch
	public function relatedmoduledatafetch() {
		$this->Homemodel->relatedmoduledatafetchmodel();
	}
	//dashboard previous and next icon
	public function dashboardpreviousnextget() {
		$this->Homemodel->dashboardpreviousnextgetmodel();
	}
	//field name based drop down value
	public function customfieldnamebesdpicklistddvalue(){
		$this->Homemodel->customfieldnamebesdpicklistddvalue();
	}
	//field name based drop down value
	public function customeditfieldsubmit(){
		$this->Homemodel->customeditfieldsubmitmodel();
	}
	//notification mute and un mute
	public function notificationmuteunmuteoption() {
		$this->Homemodel->notificationmuteunmuteoptionmodel();
	}
	//sound type fetch
	public function notificationsoundtypefetch() {
		$this->Homemodel->notificationsoundtypefetchmodel();
	}
	//checkbox status
	public function changestatusbasedoncheckbox() {
		$this->Homemodel->changestatusbasedoncheckboxmodel();
	}
	//quick create in dashboard
	public function quickcreatefunction() {
		$this->Homemodel->quickcreatefunctionmodel();
	}
	//quick update in dashboard
	public function quickupdatefunction() {
		$this->Homemodel->quickupdatefunctionmodel();
	}
	//retrieve dropdown options
	public function getdropdownoptions() {
		$table=$_GET['table'];
		$this->Homemodel->getdropdownoptions($table);
	}
	//fetch task data
	public function fetchtaskdata() {
		$this->Homemodel->fetchtaskdatamodel();
	}
}