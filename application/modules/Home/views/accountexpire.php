<!DOCTYPE html>
<html lang="en">
<head>
<?php $this->load->view('Base/headerfiles'); ?>
<script>
$(document).ready(function(){
	//plan upgrade
	$('#subscribesubmit').click(function(){
		var chk = $('#check').val();
		var url = $('#url').val();
		var info = $('#customerinfo').val();
		if(chk!='') {
			window.location=url+'?sets='+info;
		}
	});
	//free plan upgrade
	$('#freeplansubmit').click(function(){
		var chk = $('#check').val();
		var url=$('#expireurl').val();
		if(chk!='') {
			window.location=url;
		}
	});
});
</script>
</head>
<body>
	<div class="large-12 columns" style="position:absolute;">
		<div class="" style="background-color: rgba(0, 0, 0, 0.30); position:fixed; top:0px; bottom:0px; left:0px; right:0px; z-index:41; height:100%; width:100%;display:block" id="accountexpireoverlay">	
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="alert-panel">
				<div class="alertmessagearea">
					<div class="alert-title">Account Expired</div>
					<div class="alert-message alertinputstyle">
						Your account has expired. Go to free plan or proceed with subscription purchase
					</div>
				</div>
				<div class="alertbuttonarea">
					<span class="firsttab" tabindex="1000"></span>
					<input type="button" id="subscribesubmit" name="subscribesubmit" tabindex="1001" value="Subscribe Now" class="alertbtn ffield  dynamicid" >
					<input type="button" id="freeplansubmit" name="freeplansubmit" value="Free Plan" tabindex="1002" class="alertbtn flloop  alertsoverlaybtn" >
					<span class="lasttab" tabindex="1003"></span>
				</div>
			</div>
		</div>
	</div>
	<!-- hidden fields-->
	<?php
		$CI=& get_instance();
		$empid = $CI->Basefunctions->logemployeeid;
		$datasets = $userdata;
		$curid = $currency['currid'];
		$infos = "uesn9s".base64_encode($datasets['email'])."a1o8cuidsn9s".base64_encode($curid)."a1o8eidsn9s".base64_encode($datasets['userid'])."a1o8tzsn9s".base64_encode($datasets['zone']);
		$datas = strrev($infos."a1o8optsn9s".base64_encode('product'));
		$urldatasets = base64_encode($datas);
		$secure = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "");
		if($_SERVER['HTTP_HOST']=='localhost') {
			$url = "http".$secure."://".$_SERVER['HTTP_HOST'].'/salesneuroncrm/Login/accountexpire';
			$root = "http".$secure."://".$_SERVER['HTTP_HOST']."/salesneuronsite/checkout.php";
		} else {
			$hname = explode('.',$_SERVER['HTTP_HOST']);
			$count = count($hname);
			$url = "http".$secure."://".$_SERVER['HTTP_HOST']."/crm/Login/accountexpire";
			$root = "http".$secure."://subscribe.salesneuron.".$hname[($count-1)]."/checkout.php";
		}
	?>
	<input type="hidden" name="url" id="url" value="<?php echo $root; ?>" />
	<input type="hidden" name="customerinfo" id="customerinfo" value="<?php echo $urldatasets; ?>" />
	<input type="hidden" name="check" id="check" value="<?php echo $datasets['email']; ?>" />
	<input type="hidden" name="expireurl" id="expireurl" value="<?php echo $url; ?>" />
</body>
</html> 