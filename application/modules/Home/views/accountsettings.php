<!DOCTYPE html>
<html lang="en">
<head>
<?php $this->load->view('Base/headerfiles'); ?>
<script>
$(document).ready(function(){
	$("#companyname").focus();
	//account settings form submit
	$("#accountsettingsubmit").click(function(){
		$("#accountsettingsvalidation").validationEngine('validate');
	});
	$("#accountsettingsvalidation").validationEngine({
		onSuccess: function() {
			$("#accountsettings").submit();
		},
		onFailure: function() {
			alertpopup(validationalert);
		}
	});
});
</script>
</head>
<body>
	<?php
		//instead of this ( This will replace (this->) pointer )
		$CI =& get_instance();
		$name='';
		$email='';
		$img="";
		$imgfromid = "";
		$path = "";
		$dataset=$CI->db->select('employeeid,employeename,lastname,emailid,employeeimage,employeeimage_fromid,employeeimage_path,salutation.salutationname')->from('employee')->join('salutation','salutation.salutationid=employee.salutationid')->where('employee.employeeid',$CI->Basefunctions->logemployeeid)->get();
		foreach($dataset->result() as $row) {
			$name=$row->salutationname.$row->employeename.$row->lastname;
			$img=$row->employeeimage;
			$imgfromid = $row->employeeimage_fromid;
			$path = $row->employeeimage_path;
			$email = $row->emailid;
		}
		$secure = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "");
		if($_SERVER['HTTP_HOST']=='localhost') {
			$url = "http".$secure."://".$_SERVER['HTTP_HOST'].'/salesneuronsite/crm/Login/accountinfoupdate';
		} else {
			$url = "http".$secure."://".$_SERVER['HTTP_HOST']."/crm/Login/accountinfoupdate";
		}
	?>
	<div class="large-12 medium-12 small-12 columns" style="position:absolute;">
		<div class="overlaymn"  id="accountsettingsoverlay">	
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<form id="accountsettings" name="accountsettings" action="<?php echo $url; ?>" method="POST" enctype="application/x-www-form-urlencoded">
				<div id="accountsettingsvalidation" class="large-4 medium-6 small-10 large-centered medium-centered small-centered columns validationEngineContainer">
					<div class="row">&nbsp;</div>
					<div class="row">&nbsp;</div>
					<div class="row">
						<div class="large-12 columns alertsheadercaptionstyle" style="text-align:left">
							<div class="small-12 columns"><span style="color:#ffffff;padding-right:0.5rem"><i class="material-icons">check_box</i></span>Account Settings</div>
						</div>
						<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
						<div class="large-12 large-centered columns" style="background:#f5f5f5;text-align:center">
							<div class="large-4 large-offset-4 medium-4 medium-offset-4 small-offset-4 small-4 column large-centered">
								<span style="width:45px; height:45px;border-radius:50%;"><img alt="" src="<?php $path=($path!='') ? ( $imgfromid=='2' ? ( file_exists($path) && is_readable($path) ? base_url().$path : base_url().'img/userimg.png' ) : $path ) : base_url().'img/userimg.png' ; echo $path; ?>" style="border-radius: 50%; display:block;height:100px;"></span><br><span style="text-align:left;"><lable><?php echo $name;?></lable></span>
							</div>
							<div class="large-12 medium-12 small-12 column" style="text-align:left; ">
								<div class="large-12 medium-12 small-12 columns">
									<label>Company Name<span class="mandatoryfildclass">*</span></label>
									<input type="text" id="companyname" name="companyname" value="" class="validate[required]" tabindex="1001"/>
								</div>
								<div class="large-12 medium-12 small-12 columns">
									<label>Password<span class="mandatoryfildclass">*</span></label>
									<input type="password" id="pwd" name="pwd" value="" class="validate[required,minSize[5]]" tabindex="1002"/>
								</div>
								<div class="large-12 medium-12 small-12 columns">
									<label>Confirm password<span class="mandatoryfildclass">*</span></label>
									<input type="password" id="cpwd" name="cpwd" value="" class="validate[required,equals[pwd]" tabindex="1003"/>
								</div>
							</div>
							<div class="large-12 medium-12 small-12 column">&nbsp;</div>
							<div class="large-12 medium-12 small-12 column">&nbsp;</div>
						</div>
					</div>
					<span class="firsttab"></span>
					<div class="row" style="background:#f5f5f5">
						<div class="medium-2 large-4 columns"> &nbsp;</div>
						<div class="small-6 medium-4 large-4 columns">
							<input type="button" id="accountsettingsubmit" name="accountsettingsubmit" tabindex="1004" value="Submit" class="btn ffield formbuttonsalert dynamicid">
						</div>
						<span class="lasttab"></span>
						<div class="medium-2 large-4 columns"> &nbsp;</div>
					</div>
					<div class="row" style="background:#f5f5f5">&nbsp;</div>
					<div class="row" style="background:#f5f5f5">&nbsp;</div>
				</div>
				<!-- hidden fields -->
				<input type="hidden" name="email" id="email" value="<?php echo $email;?>" />
			</form>
		</div>
	</div>
</body>
<?php $this->load->view('Base/bottomscript'); ?>
</html>
 