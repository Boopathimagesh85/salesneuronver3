<!DOCTYPE html>
<html lang="en">
<head>
<?php $this->load->view('Base/headerfiles'); ?>
</head>
<body style="background-color:#e0e0e0;">
	<?php
		//user information
		$CI =& get_instance();
		$name='';
		$email='';
		$img="";
		$imgfromid = "";
		$path = "";
		$empid = "";
		$dataset=$CI->db->select('employeeid,employeename,lastname,emailid,employeeimage,employeeimage_fromid,employeeimage_path,salutation.salutationname')->from('employee')->join('salutation','salutation.salutationid=employee.salutationid')->where('employee.employeeid',$CI->Basefunctions->logemployeeid)->get();
		foreach($dataset->result() as $row) {
			$name=$row->salutationname.$row->employeename.' '.$row->lastname;
			$email=$row->emailid;
			$img=$row->employeeimage;
			$imgfromid = $row->employeeimage_fromid;
			$path = $row->employeeimage_path;
			$empid = $row->employeeid;
		}
	?>
	<div class="row" style="max-width:100%;">
		<div class="off-canvas-wrap" data-offcanvas>
			<div class="inner-wrap">
				<?php
				 	$fdataset['gridtitle'] = 'Home';
				 	$this->load->view('Base/mainviewheader',$fdataset); 
				?>
				<div class="large-12 columns desktop actionmenucontainer headeraction-menu  " style="height:45px;padding:0px">
							<ul class="module-view">
								<li>
								</li>
							</ul>
							<ul class="toggle-view tabaction">
								<li class="action-icons">
									<?php
										//user menu
										$this->load->view('Base/usermenugenerate');
									?>
								</li>
							</ul>
				</div>
				<div class="large-12 columns" style="background-color:#e0e0e0"> &nbsp;</div>
				<div class="large-12 columns homedashboardcontainer mblnopadding" id="widgetdata" style="background-color:#e0e0e0"></div>
				<div class="large-12 columns"> &nbsp;</div>
				<div class="large-12 columns"></div>
				<div class="large-12 columns">&nbsp;</div>				
			</div>
		</div>
	</div>
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay" id="dbdeletealertscloseyn">
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>		
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="large-3 medium-6 small-10 large-centered medium-centered small-centered columns">
				<div class="row">&nbsp;</div>
				<div class="row">
					<div class="large-12 columns alertsheadercaptionstyle" style="text-align:left">
						<div class="small-12 columns"><span style="color:#ffffff;padding-right:0.5rem"><i class="material-icons">check_box</i>  </span> Confirmation</div>
					</div>
					<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
					<div class="large-12 large-centered columns" style="background:#f5f5f5;text-align:center">
						<span class="alertinputstyle">Delete the data ?</span>
						<div class="large-12 column">&nbsp;</div>
					</div>
				</div>
				<span class="firsttab" tabindex="1000"></span>
				<div class="row" style="background:#f5f5f5">
					<div class="medium-2 large-2 columns"> &nbsp;</div>
					<div class="small-6  medium-4 large-4 columns">
						<input type="button" id="" name="" tabindex="1001" value="Yes" class="btn ffield formbuttonsalert dynamicid" >	
					</div>
					<div class="small-6 medium-4 large-4 columns">
						<input type="button" id="dbdelalertsfcloseno" name="" value="No" tabindex="1002" class="btn flloop formbuttonsalert alertsoverlaybtn" >	
					</div>
					<span class="lasttab" tabindex="1003"></span>
					<div class="medium-2 large-2 columns"> &nbsp;</div>
				</div>
				<div class="row" style="background:#f5f5f5">&nbsp;</div>
				<div class="row" style="background:#f5f5f5">&nbsp;</div>
			</div>
		</div>
	</div>
	<!--Selection Overlay-->
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay" id="dashboardselectoverlay">
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="large-3 medium-6 small-10 large-centered medium-centered small-centered columns">
				<span id="metaloverlaywizard"> 
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#617d8a">
						<div class="large-12 columns sectionheaderformcaptionstyle">Dashboard Selection</div>
						<div class="large-12 columns">
							<label>Select Dashboard</label>
							<select class="chzn-select" id="dashboardviewid" name="dashboardviewid">
								<?php
								$prev = ' ';
								$selected = "";
								$userid = $this->Basefunctions->logemployeeid;
								foreach($dashboradviewdata as $key):
									$cur = $key->dashboardfoldername;
									$a ="";
									if($prev != $cur ) {
										if($prev != " ") {
											echo '</optgroup>';
										}
										echo '<optgroup  label="'.str_replace(' ','',$key->dashboardfoldername).'" class="'.str_replace(' ','',$key->dashboardfoldername).'">';
										$prev = $key->dashboardfoldername;
										$a = "pclass";
									}
									if($userid == $key->createuserid && $key->setdefault) {
										$selected = "selected=selected";
									}
									?>
									<option data-moduleid="" data-rowid="" value="<?php echo $key->dashboardid;?>" <?php echo $selected; ?> ><?php echo $key->dashboardname;?></option>
								<?php endforeach;?>
							</select>
						</div>
						<div class="large-12 columns text-right">
						<input type="button" id="dashboardselectioncloseid" name="" value="cancel" class="alertbtn  alertsoverlaybtn" >
						</div>
					</div>
					<div class="row" style="background:#617d8a">&nbsp;</div>
				</span>
			</div>
		</div>
	</div>
	<input type="hidden" id="notificationdatatype" />
	<?php
		$this->load->view('Base/bottomscript');
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$this->load->view('Base/overlaymobile');
		} else {
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
	?>
	<script>
		jQuery(document).ready(function(){
			var newheightaddformdb = $( window ).height();
			var addheightdb = parseInt(newheightaddformdb) - parseInt(47);
			$("#widgetdata").css('height',''+addheightdb+'px').css('overflow-y','scroll');
		});
	</script>
	<!--Widget Creation Area-->
	<script>
		$(document).ready(function(){
			dynamicchartwidgetcreate();
		});
	</script>
	<script src="<?php echo base_url();?>js/Home/home.js"></script>
	<script src="<?php echo base_url();?>js/plugins/uploadfile/jquery.uploadfile.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/zingchart/zingchart.min.js" type="text/javascript"></script>
</body>
<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
</html>
 