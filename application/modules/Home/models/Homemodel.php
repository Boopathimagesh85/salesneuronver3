<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class homemodel extends CI_Model {
	public function __construct() {		
		parent::__construct();
      	$this->load->library('convert');
    }
	//get emp information
	public function subcribeuserinfo() {
		$tzone = $this->Basefunctions->employeetimezoneset($this->Basefunctions->logemployeeid);
		$datasets = array('name'=>'','email'=>'','userid'=>'','zone'=>'');
		$dataset=$this->db->select('employeeid,employeename,lastname,emailid,salutation.salutationname')->from('employee')->join('salutation','salutation.salutationid=employee.salutationid')->where('employee.employeeid',$this->Basefunctions->logemployeeid)->get();
		foreach($dataset->result() as $row) {
			$name=$row->salutationname.$row->employeename.$row->lastname;
			$email=$row->emailid;
			$empid = $row->employeeid;
			$datasets = array('name'=>$name,'email'=>$email,'userid'=>$empid,'zone'=>$tzone['name']);
		}
		return $datasets;
	}
	//subscriber currency
	public function subscribecurrency()	{
		$cid = 120;
		$curinfo = array('currid'=>$cid,'cursymbol'=>'$','code'=>'USD');
		$empid = $this->Basefunctions->userid;
		$compquery = $this->db->query('select company.companyid,company.currencyid,currency.symbol,currency.currencycode from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid join currency ON currency.currencyid=company.currencyid where employeeid='.$empid.'')->result();
		foreach($compquery as $key) {
			$curinfo = array('currid'=>$key->currencyid,'cursymbol'=>$key->symbol,'code'=>$key->currencycode);
		}
		return $curinfo;
	}
	//default value set array for add
	public function defaultvalueget() {
		$defdata['createdate'] = date($this->Basefunctions->datef);
		$defdata['lastupdatedate'] = date($this->Basefunctions->datef);
		$defdata['createuserid'] = $this->Basefunctions->userid;
		$defdata['lastupdateuserid'] = $this->Basefunctions->userid;
		$defdata['status'] = 1;
		return $defdata;
	}
	//default value set array for update
	public function updatedefaultvalueget() {
		$defdata['lastupdatedate'] = date($this->Basefunctions->datef);
		$defdata['lastupdateuserid'] = $this->Basefunctions->userid;
		return $defdata;
	}
	//in line data delete
	public function inlinedatadeletemodel() {
		$primaryid = $_POST['tablevalue'];
		$primayname = $_POST['tablefield'];
		$tablname = $_POST['tablename'];
		$tablenames = explode(',',$tablname);
		$deldataarr = array(
			'status'=>0
		);
		$defdataarr = $this->updatedefaultvalueget();
		$newdata = array_merge($deldataarr,$defdataarr);
		foreach($tablenames as $table) {
			$this->db->where($primayname,$primaryid);
			$this->db->update($table,$newdata);
		}
		echo "TRUE";
	}
	//conversation create model
	public function conversationcreatemodel() {
		$numarray=array();
		$createemployee=$this->Basefunctions->logemployeeid;
		$message=$this->input->post('convmsg');
		$g_message=$message;
		$conversationtitle=$this->input->post('convtitle');
		$moduleid=$this->input->post('moduleid');
		$rowid=$this->input->post('rowid');
		$privacy=$this->input->post('privacy');
		$file=$this->input->post('file');
		$filename=$this->input->post('fname');
		if($rowid == '' or $rowid == null) { 
			$rowid == 1;
		}
		if($moduleid == '' or $moduleid == null) {
			$moduleid == 1;
		}
		
		$usertags = $grouptags ='';
		preg_match_all ("/@[0-9]\-/U",$message,$pat_array); //identify the individual patterns.
		
		//individual user array segregate
		$count = count($pat_array);
		if($count == 1) {
			$numarray=array();
			$usertags = $pat_array;			
			$stepone=str_replace('@','',$usertags[0]); //remove @
			$steptwo=str_replace('-','',$stepone); //remove -
			foreach($steptwo as $key=>$value ){
				if(is_numeric($value)){
					$numarray[]=$value;
				}
			}
			$usertags = implode(',',array_unique($numarray));
		}
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$this->Basefunctions->logemployeeid);
		
		//group user array segregate
		preg_match_all ("/@[0-9]\:/U",$g_message,$pat_arraytwo); //identify the group patterns
		$count = count($pat_arraytwo);
		if($count == 1) {
			$gnumarray=array();
			$grouptags = $pat_arraytwo;
			$stepone=str_replace('@','',$grouptags[0]); //remove @
			$steptwo=str_replace(':','',$stepone); //remove -
			foreach($steptwo as $key=>$value ) {
				if(is_numeric($value)){
					$gnumarray[]=$value;
				}
			}
			$uniquegroup = array_unique($gnumarray);
			$grouptags = implode(',',$uniquegroup);
		}
		$convertinsert=array('feedstype'=>1,'moduleid'=>$moduleid,'conversationtitle'=>$conversationtitle,'notificationmessage'=>$message,'employeeid'=>trim($usertags),'employeegroupid'=>trim($grouptags),'createuserid'=>$this->Basefunctions->logemployeeid,'lastupdateuserid'=>$this->Basefunctions->logemployeeid,'createdate'=>date($this->Basefunctions->datef),'lastupdatedate'=>date($this->Basefunctions->datef),'status'=>$this->Basefunctions->activestatus,'commonid'=>$rowid,'privacy'=>$privacy,'filename'=>$filename,'filepath'=>$file);
		$this->db->insert('notificationlog',array_filter($convertinsert));
		$conversationid=$this->db->insert_id();
		//notification log entry
		//remove current user entry
		if(count($numarray) > 0) {
			//print_r($numarray);
			if(($key = array_search($this->Basefunctions->logemployeeid,$numarray)) !== false) {
				unset($numarray[$key]);
			}
			foreach($numarray as $key=>$value) {
				$notification_array=array('notificationlogtypeid'=>'conversation','employeeid'=>$value,'notificationmessage'=>$empname.' mentioned u in a conversation','feedstype'=>2,'createdate'=>date($this->Basefunctions->datef),'lastupdatedate'=>date($this->Basefunctions->datef),'createuserid'=>$this->Basefunctions->logemployeeid,'lastupdateuserid'=>$this->Basefunctions->logemployeeid,'status'=>$this->Basefunctions->activestatus,'moduleid'=>267,'commonid'=>$conversationid,'flag'=>1);
				$this->db->insert('notificationlog',array_filter($notification_array));
			}
		} else {
			$employeedata = $this->generalnotificationtoalluser();
			if(empty($employeedata)) {
				for($i=0;$i<count($employeedata);$i++) {
					$notification_array=array('notificationlogtypeid'=>'conversation','employeeid'=>$employeedata[$i]['eid'],'notificationmessage'=>$empname.' mentioned u in a conversation','feedstype'=>2,'createdate'=>date($this->Basefunctions->datef),'lastupdatedate'=>date($this->Basefunctions->datef),'createuserid'=>$this->Basefunctions->logemployeeid,'lastupdateuserid'=>$this->Basefunctions->logemployeeid,'status'=>$this->Basefunctions->activestatus,'moduleid'=>267,'commonid'=>$conversationid,'flag'=>1);
					$this->db->insert('notificationlog',array_filter($notification_array));
				}
			}
		}
		if(count($uniquegroup) > 0) {
			for($lk=0;$lk<count($uniquegroup);$lk++) {
				$emp_group_data=$this->db->select('employeeid,employeegroupname')
											->from('employeegroup')
											->where('employeegroupid',$uniquegroup[$lk])
											->limit(1)
											->get();
				if($emp_group_data->num_rows() > 0) {
					foreach($emp_group_data->result() as $info) {
						$employeeids=$info->employeeid;
						$employeegroupname=$info->employeegroupname;			
					}
					$groupnumarray=explode(',',$employeeids);
					foreach($groupnumarray as $key=>$value) {		
						$notification_array=array('notificationlogtypeid'=>'conversation','employeeid'=>$value,'notificationmessage'=>$empname.'mentioned u '.$employeegroupname.' in a conversation','feedstype'=>1,'createdate'=>date($this->Basefunctions->datef),'lastupdatedate'=>date($this->Basefunctions->datef),'createuserid'=>$this->Basefunctions->logemployeeid,'lastupdateuserid'=>$this->Basefunctions->logemployeeid,'status'=>$this->Basefunctions->activestatus,'moduleid'=>267,'commonid'=>$conversationid,'flag'=>1);
						$this->db->insert('notificationlog',array_filter($notification_array));
					}
				}
			}
		}
		echo true;
	}
	//conversation information fetch
	public function getconversationdata($moduleid,$rowid,$lastnode,$datasourcetype) {
		if($rowid == null or $rowid =='') {
			$rowid =1;
		}
		if($moduleid == null or $moduleid =='') {
			$moduleid =1;
		}$empid=$this->Basefunctions->logemployeeid;
		if($datasourcetype == 'today') {
			$date = date("Y-m-d");
		} else if($datasourcetype == 'yesterday') {
			$currentdate = date("Y-m-d");
			$first_date = strtotime($currentdate);
			$ndate = strtotime('-1 day', $first_date);
			$date = date("Y-m-d",$ndate);
		} else if($datasourcetype == 'thisweek') {
			$currentdate = date("Y-m-d");
			$first_date = strtotime($currentdate);
			$ndate = strtotime('-7 day', $first_date);
			$date = date("Y-m-d",$ndate);
		} else if($datasourcetype == 'thismonth') {
			$currentdate = date("Y-m-d");
			$totaldays =  date('t');
			$first_date = strtotime($currentdate);
			$ndate = strtotime('-'.$totaldays.' day', $first_date);
			$date = date("Y-m-d",$ndate);
		} else {
			$date = '';
		}
		$datafield=array();
		$editable = 'no';
		$userid = $this->Basefunctions->logemployeeid;
		$now=date($this->Basefunctions->datef);
		$timezone=date($this->Basefunctions->datef);
		$tzone = $this->Basefunctions->employeetimezoneset($this->Basefunctions->userid);
		//CONVERT_TZ(NOW(),'US/Pacific','UTC')
		$this->db->select("notificationlog.notificationmessage,notificationlog.employeegroupid,conversationtitle,notificationlog.createdate,notificationlog.createuserid,abs(DATEDIFF(notificationlog.createdate,'".$now."'))AS DiffDate,TIMEDIFF(notificationlog.createdate,'".$now."') as difftime,notificationlog.notificationlogid,user.employeename as username,notificationlog.employeeid,employeeimage,employeeimage_fromid,employeeimage_path,notificationlog.filename,notificationlog.filepath,'".$now."' as currentdate",false);
		$this->db->from('notificationlog');
		$this->db->join('employee as user','user.employeeid=notificationlog.createuserid');
		$this->db->where('notificationlog.parentconversationid',0);
		$this->db->where('notificationlog.status',$this->Basefunctions->activestatus);
		$this->db->where('notificationlog.moduleid',$moduleid);
		if($moduleid > 1 and $rowid > 1) {
			$this->db->where('notificationlog.commonid',$rowid);
		}
		if($lastnode >0){
			$this->db->where('notificationlog.notificationlogid <',$lastnode);
		}
		if($moduleid !='267') {
			//$this->db->limit(10);
		}
		if($datasourcetype == 'all') {
			$this->db->where('notificationlog.createuserid',$empid);
		}
		if($datasourcetype == 'my') {
			$this->db->where('notificationlog.flag',0);
			$this->db->where('notificationlog.createuserid',$empid);
		}
		if($datasourcetype == 'today' || $datasourcetype == 'yesterday') {
			$this->db->where('notificationlog.createuserid',$empid);
			$this->db->like('notificationlog.createdate',$date);
		} else if($datasourcetype == 'thisweek' || $datasourcetype == 'thismonth') {
			$this->db->where('notificationlog.createuserid',$empid);
			$this->db->where("notificationlog.createdate BETWEEN '$date' AND '$currentdate'");
		}
		$this->db->where('notificationlog.feedstype',1);
		$this->db->order_by('notificationlog.notificationlogid','desc');
		$data=$this->db->get();
		foreach ($data->result() as $vals) {
				$value='';
				$editable = 'no';
				$duration=$vals->DiffDate;
				if($duration > 1){
					$value = date('d M Y',strtotime($vals->createdate));
					//echo $value;
				} else {
					$time=explode(':',$vals->difftime);	
					if(abs($time[0]) > 0) {	
						$value = abs($time[0]).' hrs ago';
					}
				 	else if(abs($time[1] > 0)) {
						$value = abs($time[1]).' min ago';
					}
					else if(abs($time[2] > 0)) {
						$value = abs($time[2]).' sec ago';
					} 
				}
				if($userid === $vals->createuserid) {
					$editable ='yes';
				} 
				$formattedoutput=$vals->notificationmessage;
			if(strpos($vals->notificationmessage,'@') === false) {
				$formattedoutput=$vals->notificationmessage;
				$m=0;
			} else { 
				$formattedoutput=$this->formatmessage($vals->notificationmessage,$vals->employeeid,$vals->employeegroupid);$m=1;
			}	
			$replycount = $this->getreplycount($vals->notificationlogid);
			$datafield[]=array('c_id'=>$vals->notificationlogid,'c_name'=>$formattedoutput,'userid'=>$vals->createuserid,'username'=>$vals->username,'employeeimage'=>'','c_date'=>date("d-m-y H:i:s",strtotime($vals->createdate)),'time'=>$value,'c_title'=>$vals->conversationtitle,'editable'=>$editable,'profile'=>$vals->employeeimage_path,'fromid'=>$vals->employeeimage_fromid,'logid'=>$userid,'file'=>$vals->filename,'filepath'=>$vals->filepath,'im'=>$m,'count'=>$replycount);			
		}
		return json_encode($datafield);
	} 
	//get reply count
	public function getreplycount($vals) {
		$data =0;
		$this->db->select('count(conversationid) as ccount');
		$this->db->from('conversation');
		$this->db->where_in('conversation.parentconversationid ',$vals);
		$this->db->where_not_in('conversation.status',array(0,3));
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = $row->ccount;
			}
			return $data;
		} else {
			return $data;
		}
	}
	public function formatmessage($message,$employee,$employeegroup) {
		$e=0;
		$employee=explode(',',$employee);
		$employeegroup=explode(',',$employeegroup);
		$validempid=array();
		$validempgrpid=array();
		#check the valid id exists then filter them
		$empvali_data=$this->db->select('employeeid')
							->from('employee')
							->where_in('employeeid',$employee)
							->get()->result();
		foreach($empvali_data as $inff){
			$validempid[]=$inff->employeeid;
		}
		$employee=$validempid;
		//
		#check the valid id exists then filter them
		$empvaligrp_data=$this->db->select('employeegroupid')
							->from('employeegroup')
							->where_in('employeegroupid',$employeegroup)
							->get()->result();
		foreach($empvaligrp_data as $inff) {
			$validempgrpid[]=$inff->employeegroupid;
		}
		$employeegroup=$validempgrpid;
		
		if(count($employee) > 0) {
		$employeeurl=$this->config->base_url();
		$employeeurl.='Employee';
			//retrieve employeename.
		    $emp_data=$this->db->select('employeeid,employeename')
							->from('employee')
							->where_in('employeeid',$employee)
							->get()->result();
			foreach($emp_data as $value) {
				$employeename[$e]=array('employeeid'=>$value->employeeid,'employeename'=>$value->employeename);
				$e++;
			}			
			foreach($employee as $key=>$value) {			
				$emp=str_replace(' ','',$employeename[$key]['employeename']); //remove -
				$format='@'.$value.'-'.$emp;
				$linktags="<a href='#' class='alertjusthere' data-rowid='".$value."' data-moduleid='4'>".$employeename[$key]['employeename']."</a>";
				$message=str_replace($format,$linktags,$message);				
			}		
		}
		$e=0;
		if(count($employeegroup) > 0) {
			$employeegrpurl=$this->config->base_url();
			$employeegrpurl.='users/groups';
			//retrieve employeegroupname.
		    $emp_grp_data=$this->db->select('employeegroupid,employeegroupname')
							->from('employeegroup')
							->where_in('employeegroupid',$employeegroup)
							->get()->result();
			foreach($emp_grp_data as $value) {
				$employeegrpname[$e]=array('employeegroupid'=>$value->employeegroupid,'employeegroupname'=>$value->employeegroupname);
				$e++;
			}			
			foreach($employeegroup as $key=>$value) {			
				$emp=str_replace(' ','',$employeegrpname[$key]['employeegroupname']); //remove -
				$format='@'.$value.':'.$emp;
				$linktags="<a href='#' class='alertjusthere' data-rowid='".$value."' data-moduleid='246' style='color:#5B9150';>".$employeegrpname[$key]['employeegroupname']."</a>";
				$message=str_replace($format,$linktags,$message);				
			}
		}	
		return $message;
	}
	//used for Circle lead quality
	public function totalleadqualitymodel() {	
		$parenttable=$_GET['parenttable'];
		$parentid=$_GET['parentid'];
		$parentname=$_GET['parentname'];
		$childtable=$_GET['childtable'];
		$childid=$_GET['childid'];
		$totalleadcount = 0;
		$result=$this->db->query("Select count($childtable.$childid) as totlead from $childtable Where Status=1");
		foreach($result->result() as $totid) {
			$totalleadcount = $totid->totlead;				
		}
		//join concept
		$result = $this->db->query("Select $parenttable.$parentid,$parenttable.$parentname from $parenttable Where Status=1");
		$i=0;
		$j=0;
		foreach($result->result() as $leadq) {
			$pname = $leadq->$parentname;
			$pid = $leadq->$parentid;
			$result=$this->db->query("Select count($childtable.$childid) as countpid from $childtable Where $parentid=$pid AND Status=1");
			foreach($result->result() as $show) {
				$value[$j]=array('count'=>$show->countpid,'name'=>$pname,'totalldcount'=>$totalleadcount);
				$j++;
			}
		}
		echo json_encode($value);
	}
	//dashboard activity show model
	public function homedashboardactvitymodel() {	
		$i = 0;
		$empid = $this->Basefunctions->logemployeeid;
		$this->db->select('leadactivity.LeadCreationId,leadactivity.LeadActivityId,leadactivity.ActivityTopic,leadactivity.ActivityType,leadactivity.ActivityPriority,leadactivity.ActivityStatus,leadactivity.Comments,leadactivity.ActivityDate,ab.EmployeeName,leadcreation.LeadCreationName',false);
		$this->db->from('leadactivity');
		$this->db->join('leadcreation','leadcreation.LeadCreationId=leadactivity.LeadCreationId');
		$this->db->join('usercredential','usercredential.UserCredentialId=leadactivity.LastUpdateUserId');
		$this->db->join('employee as ab','ab.EmployeeId=usercredential.EmployeeId');
		$this->db->join('employee as at','at.EmployeeId=leadactivity.EmployeeId');
		$this->db->where('leadactivity.Status',1);
		if($empid != '') {
			$this->db->where('leadactivity.EmployeeId',$empid);
		}
		$this->db->order_by('leadactivity.LeadActivityId','desc');
		$this->db->limit(3,0);
		$result = $this->db->get();
		if($result->num_rows() >0) {		
			foreach($result->result()as $row) {
				if($row->LeadCreationId !='1'){$leadname = $row->LeadCreationName;}
				else{ $leadname = "General"; }
				$data[$i]=array('atopic'=>$row->ActivityTopic,'acttype'=>$row->ActivityType,'actpri'=>$row->ActivityPriority,'actstat'=>$row->ActivityStatus,'actcmts'=>$row->Comments,'actdate'=>$row->ActivityDate,'actasgnby'=>$row->EmployeeName,'leadname'=>$leadname);
				$i++;
			}	
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//total login user value
	public function totalloginuservaluemodel() {
		$userid = $this->Basefunctions->logemployeeid;
		//echo $userid;
		$prenttableid=$_GET['parenttableid'];
		$prenttablename=$_GET['parenttable'];
		$childid=$_GET['childid'];
		$updateuserid=$_GET['updateuserId'];
		//total lead count
		$result2=$this->db->query("Select count($prenttableid) as totlead from $prenttablename Where Status=1");
		foreach($result2->result() as $totid) {
			$totalleadcount = $totid->totlead;				
		}
		//total leads assigned to me
		$result1 = $this->db->query("SELECT count($prenttableid) AS leadassignto FROM $prenttablename JOIN employee ON employee.EmployeeId=leadcreation.EmployeeId WHERE $childid = $userid AND $prenttablename.Status =1");
		foreach($result1->result() as $totassi) {
			$totassigncount = $totassi->leadassignto;				
		} 
		//total leads created by me
		$lcm=$this->db->query("Select count($prenttableid) as totloginlead from $prenttablename JOIN usercredential ON usercredential.UserCredentialId=leadcreation.LastUpdateUserId Where $updateuserid=$userid AND $prenttablename.Status=1");
		//print_r($result);
		if($lcm->num_rows() > 0) {
			foreach($lcm->result() as $totlogid) {
				$data =array('loginuser'=>$totlogid->totloginlead,'totuser'=>$totalleadcount,'leadassignto'=>$totassigncount);
			}
			echo json_encode($data);
		} else {
			 echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//notification show in lead dash board
	public function homedbnotificationshowmodel() {	
		$i=0;
		$empid = $this->Basefunctions->logemployeeid;
		$this->db->select('notificationlog.NotificationLogId,notificationlog.NotificationMessage,notificationlog.CreateDate',false);
		$this->db->from('notificationlog');
		$this->db->where('notificationlog.NotificationLogTypeId',1);
		$this->db->where_in('notificationlog.EmployeeId',array($empid,1));
		$this->db->where_not_in('notificationlog.Status',array(3));
		$this->db->order_by('notificationlog.NotificationLogId','desc');
		$this->db->limit(3,0);
		$result = $this->db->get();
		if($result->num_rows() >0) {		
			foreach($result->result()as $row) {
				$data[$i]=array('lcnotlogid'=>$row->NotificationLogId,'lcnotlogmsg'=>$row->NotificationMessage,'lcnotlogcdate'=>$row->CreateDate);
				$i++;
			}	
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	// Top 10 product CHart
	public function getdetailsforlinewithtrendproductmodel() {
		$i=0;
		$ptablename = $_GET['ptab'];
		$ptablefield = $_GET['ptabfield'];
		$ctablename = $_GET['ctab'];
		$countfield = $_GET['countfield'];
		$condifield = $_GET['confield'];
		$fields = explode(',',$ptablefield);
		$actsts = $this->Basefunctions->activestatus;
		$forjoin=' JOIN '.$ptablename.' ON '.$ptablename.'.'.$condifield.'='.$ctablename.'.'.$condifield;
		$data = $this->db->query('select count('.$countfield.') AS a, '.$ptablename.'.'.$fields[1].' from '.$ctablename.' '.$forjoin.' WHERE '.$ctablename.'.Status='.$actsts.' GROUP BY '.$ctablename.'.'.$fields[0].' ORDER BY a DESC LIMIT 0 , 10');
		foreach ($data->result() as $jvalue) {
			$datafield[$i]=array('captionname'=>$jvalue->ProductName,'counts'=>$jvalue->a);
			$i++;
		}
		echo json_encode($datafield);	
	}	
	//generate the list data
	public function getnotificationdata() {
		$notificationdata=array();
		$empid=$this->Basefunctions->logemployeeid;
		$datasourcetype=$this->input->post('datatype');
		if($datasourcetype == 'today') {
			$date = date("Y-m-d");
		} else if($datasourcetype == 'yesterday') {
			$currentdate = date("Y-m-d");
			$first_date = strtotime($currentdate);
			$ndate = strtotime('-1 day', $first_date);
			$date = date("Y-m-d",$ndate);
		} else if($datasourcetype == 'thisweek') {
			$r_date = $this->Basefunctions->datesystem('lastweek');
			$startdate=$r_date['start'];
			$enddate=$r_date['end'];
		} else if($datasourcetype == 'thismonth') {
			$r_date = $this->Basefunctions->datesystem('currentmonth');
			$startdate=$r_date['start'];
			$enddate=$r_date['end'];
		}  else if($datasourcetype == 'lastmonth') {
			$r_date = $this->Basefunctions->datesystem($datasourcetype);
			$startdate=$r_date['start'];
			$enddate=$r_date['end'];
		} else {
			$date = '';
		}
		$lastnode=$this->input->post('lastnode');
		$status=$this->input->post('status');
		$now=date($this->Basefunctions->datef);
			$this->db->select(" '".$now."' as nowdate,module.modulename,module.modulemastertable,notificationlog.notificationlogtypeid,notificationlog.moduleid,commonid,a.employeeid,notificationmessage,notificationlog.createdate,notificationlog.createuserid,abs(DATEDIFF(notificationlog.createdate,'".$now."'))AS DiffDate,TIMEDIFF(notificationlog.createdate,'".$now."') as difftime,a.employeename as fromemp,b.employeename as toemp,module.modulelink,notificationlog.commonid,notificationlog.notificationlogid,b.employeeimage_path",false);
			$this->db->from('notificationlog');
			$this->db->join('module','module.moduleid=notificationlog.moduleid');
			$this->db->join('employee as a','a.employeeid=notificationlog.employeeid');
			$this->db->join('employee as b','b.employeeid=notificationlog.createuserid');
			if(isset($_POST['moduleid']) and isset($_POST['rowid'])) {
				if($_POST['moduleid'] > 1) {
					$this->db->where('notificationlog.commonid',$_POST['rowid']);
					$this->db->where('notificationlog.moduleid',$_POST['moduleid']);
				}
			}
			if($lastnode != 0) {
				$this->db->where('notificationlog.notificationlogid <',$lastnode);
			}
			if($datasourcetype == 'newactivefeeds') {
				$this->db->where('notificationlog.flag',1);
				$this->db->where('notificationlog.employeeid',$empid);
			}
			if($datasourcetype == 'my') {
				$this->db->where('notificationlog.employeeid',$empid);
			}
			if($datasourcetype == 'today' || $datasourcetype == 'yesterday') {
				$this->db->where('notificationlog.employeeid',$empid);
				$this->db->like('notificationlog.createdate',$date);
			} else if($datasourcetype == 'thisweek' || $datasourcetype == 'thismonth'|| $datasourcetype == 'lastmonth') {
				$this->db->where('notificationlog.employeeid',$empid);
				$this->db->where("notificationlog.createdate BETWEEN '$startdate' AND '$enddate'");
			}
			if($status == 2) {
				$this->db->where("notificationlog.status","2");
			}
			$this->db->where_not_in('notificationlog.status',array(3));
			$this->db->order_by('notificationlog.notificationlogid','desc');
			if($lastnode == 0) {
				$this->db->limit(10);
			} else {
				$this->db->limit(5);
			}
			$data=$this->db->get();
			foreach($data->result() as $info) {
				$duration=$info->DiffDate;
				if($duration > 1) {
					$date=date_create($info->createdate);
					$value = date_format($date,"d M h:i A");
				} else {
					$time=explode(':',$info->difftime);					
					if(abs($time[0]) > 0) {	
						$value = abs($time[0]).' hrs ago';
					} else if(abs($time[1] > 0)) {
						$value = abs($time[1]).' min ago';
					} else if(abs($time[2] > 0)) {
						$value = abs($time[2]).' sec ago';
					} 
				}
				$identity=explode('-',$info->notificationmessage);
				if(isset($identity[1])) {
					$identity=trim($identity[1]);
				} else {
					$identity="";
				}	
				if($info->notificationlogtypeid == 'Added' || $info->notificationlogtypeid == 'Updated'|| $info->notificationlogtypeid == 'Deleted') {
					$parenttable = $info->modulemastertable;
					$primaryset = $parenttable.'.'.$parenttable.'id';
					$print_data=array('templateid'=>1,'Templatetype'=>'Printtemp','primaryset'=>$primaryset,'primaryid'=>$info->commonid);
					$message = $this->notificationprinthtmlfile($parenttable,$info->commonid,$print_data,$info->notificationmessage,$info->moduleid);
				} else {
					$message = $info->notificationmessage;
				}
				if( file_exists($info->employeeimage_path)) {
					$imagepath =base_url().$info->employeeimage_path;
				} else {
					$imagepath =base_url().'img/blackuserimg.png';
				}
				$device = $this->Basefunctions->deviceinfo();
				$notificationdata[]=array('message'=>$message,'duration'=>$value,'employeeonename'=>$info->fromemp,'employeeoneid'=>$info->employeeid,'employeetwoname'=>$info->toemp,'employeetwoid'=>$info->createuserid,'module'=>$info->modulename,'mastertable'=>$info->modulemastertable,'uniqueid'=>$info->commonid,'link'=>$info->modulelink,'moduleid'=>$info->moduleid,'operation'=>$info->notificationlogtypeid,'identity'=>$identity,'logid'=>$info->notificationlogid,'device'=>$device,'imagepath'=>$imagepath);		
			}
		echo json_encode($notificationdata);
	}
	//preview and print pdf
	public function notificationprinthtmlfile($parenttable,$recordid,$print_data,$mergetemp,$moduleid) {
		$this->load->model('Printtemplates/Printtemplatesmodel');
		//defaults
		$printdetails['moduleid'] = '1';
		$htmldatacontents='';
		$industryid = $this->Basefunctions->industryid;
		$printdetails['contentfile'] =$mergetemp;
		$printdetails['moduleid'] =$moduleid;
		$printdetails['templatename'] ='Notification content';
		$parenttable = $this->Basefunctions->generalinformaion('module','modulemastertable','moduleid',$moduleid);
		//body data
		$bodycontent = $mergetemp;
		$bodycontent = preg_replace('~a10s~','&nbsp;', $bodycontent);
		$bodycontent = preg_replace('~<br>~','<br />', $bodycontent);
		$bodycontent = $this->Printtemplatesmodel->removespecialchars($bodycontent);
		$bodyhtml = $this->Printtemplatesmodel->generateprintingdata($bodycontent,$parenttable,$print_data,$printdetails['moduleid'],$type="notification");
		$bodyhtml = $this->Printtemplatesmodel->addspecialchars($bodyhtml);
		return $bodyhtml;
	}
	//get conversation edit data-
	public function getconversationeditdata($rowid) {
			$this->db->select('notificationmessage,notificationlogid,moduleid,commonid,employeeid,filename,privacy,filepath');
			$this->db->from('notificationlog');					
			$this->db->where('notificationlog.notificationlogid',$rowid);
			$this->db->limit(1);
			$data=$this->db->get();
			foreach($data->result() as $inf) {
				$array=array('c_message'=>$inf->notificationmessage,'c_rowid'=>$inf->notificationlogid,'c_moduleid'=>$inf->moduleid,'c_commonid'=>$inf->commonid,'c_employeeid'=>$inf->employeeid,'c_file'=>$inf->filename, 'c_filepath'=>$inf->filepath);
			}
			return $array;
	}
	//update conversation data
	public function updateconversationdata($rowid) {
		$numarray=array();
		$createemployee=$this->Basefunctions->logemployeeid;
		$message = $this->input->post('c_message');
		$g_message = $message;
		$conversationtitle=$this->input->post('convtitle');
		$moduleid=$this->input->post('moduleid');
		$rowid=$this->input->post('rowid');
		$privacy=$this->input->post('privacy');
		$file=$this->input->post('file');
		if(isset($_POST['filename'])) {
			$filename=$_POST['filename'];
		} else {
			$filename= $file;
		}
		
		if($rowid == '' or $rowid == null){ $rowid == 1;}
		if($moduleid == '' or $moduleid == null){ $moduleid == 1;}
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$this->Basefunctions->logemployeeid);
		$usertags = $grouptags ='1';
		preg_match_all ("/@[0-9]\-/U",$message,$pat_array); //identify the individual patterns
		
		//individual user array segregate
		$count = count($pat_array);
		if($count == 1) {
			$numarray=array();
			$usertags = $pat_array;			
			$stepone=str_replace('@','',$usertags[0]); //remove @
			$steptwo=str_replace('-','',$stepone); //remove -
			foreach($steptwo as $key=>$value ) {
				if(is_numeric($value)){
					$numarray[]=$value;
				}
			}		
			$usertags = implode(',',array_unique($numarray));
		}		
		//group user array segregate
		preg_match_all ("/@[0-9]\:/U",$g_message,$pat_arraytwo); //identify the group patterns
		$count = count($pat_arraytwo);
		//print_r($pat_arraytwo);
		if($count == 1) {
			$numarray=array();
			$grouptags = $pat_arraytwo;			
			$stepone=str_replace('@','',$grouptags[0]); //remove @
			$steptwo=str_replace(':','',$stepone); //remove :
			foreach($steptwo as $key=>$value ) {
				if(is_numeric($value)) {
					$numarray[]=$value;
				}
			}		
			$grouptags = implode(',',array_unique($numarray));
		}
		if(!$usertags) {
			$usertags=1;
		}
		if(!$file) {
			$file=1;
			$filename=1;
		}
		$c_update=array('notificationmessage'=>$message,'employeeid'=>$usertags,'lastupdateuserid'=>$this->Basefunctions->logemployeeid,'lastupdatedate'=>date($this->Basefunctions->datef),'status'=>$this->Basefunctions->activestatus,'filename'=>$filename,'filepath'=>$file,'employeegroupid'=>$grouptags);
		$this->db->where('notificationlogid',$rowid);
		$this->db->update('notificationlog',$c_update);
		echo true;
	}
	//delete conversation data
	public function conversationdelete($rowid) {
		$updatearray=array('lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->logemployeeid,'status'=>$this->Basefunctions->deletestatus);
		$this->db->where('notificationlogid',$rowid);
		$this->db->update('notificationlog',$updatearray);
		//notification delete
		$this->db->where('commonid',$rowid);
		$this->db->where('moduleid',267);
		$this->db->update('notificationlog',$updatearray);
		//child delete
		$updatearray=array('lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->logemployeeid,'status'=>$this->Basefunctions->deletestatus);
		$this->db->where('parentconversationid',$rowid);
		$this->db->update('notificationlog',$updatearray);
		echo true;
	}
	//create reply
	public function createreply() {
		//retrieve 
		$rowid=$this->input->post('rowid');	
		$message=$this->input->post('message');
		$file=$this->input->post('file');
		$filename=$this->input->post('filename');
		$g_message = $message;
		$parentdata=$this->getconversationeditdata($rowid);	
	
		$numarray=array();
		$createemployee=$this->Basefunctions->logemployeeid;
		
		$moduleid=$parentdata['c_moduleid'];
	    $commonid=$parentdata['c_commonid'];
		
		$usertags='';
		preg_match_all ("/@(.*)\-/U", $message, $pat_array);
		$count=count($pat_array);
		if($count > 1) {
			$usertags = $pat_array[$count-1];
			foreach($usertags as $key=>$value ) {
				if(is_numeric($value)) {
					$numarray[]=$value;
				}
			}
			$usertags = implode(',',array_unique($numarray));
		}
		//group user array segregate
		preg_match_all ("/@[0-9]\:/U",$g_message,$pat_arraytwo); //identify the group patterns
		$count = count($pat_arraytwo);
		if($count == 1) {
			$gnumarray=array();
			$grouptags = $pat_arraytwo;			
			$stepone=str_replace('@','',$grouptags[0]); //remove @
			$steptwo=str_replace(':','',$stepone); //remove -
			foreach($steptwo as $key=>$value ) {
				if(is_numeric($value)){
					$gnumarray[]=$value;
				}
			}	
			$uniquegroup = array_unique($gnumarray);
			$grouptags = implode(',',$uniquegroup);
		}
		$convertinsert=array('feedstype'=>1,'moduleid'=>$moduleid,'notificationmessage'=>$message,'employeeid'=>$usertags,'employeegroupid'=>trim($grouptags),'createuserid'=>$this->Basefunctions->logemployeeid,'lastupdateuserid'=>$this->Basefunctions->logemployeeid,'createdate'=>date($this->Basefunctions->datef),'lastupdatedate'=>date($this->Basefunctions->datef),'status'=>$this->Basefunctions->activestatus,'commonid'=> $commonid,'parentconversationid'=>$rowid,'filename'=>$filename,'filepath'=>$file);
		$this->db->insert('notificationlog',array_filter($convertinsert));
		$conversationid=$this->db->insert_id();
		//notification log entry
		//remove current user entry
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$this->Basefunctions->logemployeeid);
		if(count($numarray) > 0) {
			if(($key = array_search($this->Basefunctions->logemployeeid,$numarray)) !== false) {
				unset($numarray[$key]);
			}
			foreach($numarray as $key=>$value) {
				$notification_array=array('notificationlogtypeid'=>'replied','employeeid'=>$value,'notificationmessage'=>$empname.' replied to ur conversation','createdate'=>date($this->Basefunctions->datef),'lastupdatedate'=>date($this->Basefunctions->datef),'createuserid'=>$this->Basefunctions->logemployeeid,'lastupdateuserid'=>$this->Basefunctions->logemployeeid,'status'=>$this->Basefunctions->activestatus,'moduleid'=>267,'commonid'=>$conversationid,'flag'=>1,'feedstype'=>2);
				$this->db->insert('notificationlog',array_filter($notification_array));
			}
		} else {
			$notification_array=array('notificationlogtypeid'=>'replied','employeeid'=>$this->Basefunctions->logemployeeid,'notificationmessage'=>$empname.' replied to ur conversation','createdate'=>date($this->Basefunctions->datef),'lastupdatedate'=>date($this->Basefunctions->datef),'createuserid'=>$this->Basefunctions->logemployeeid,'lastupdateuserid'=>$this->Basefunctions->logemployeeid,'status'=>$this->Basefunctions->activestatus,'moduleid'=>267,'commonid'=>$conversationid,'flag'=>1,'feedstype'=>2);
			$this->db->insert('notificationlog',array_filter($notification_array));
		}	
		if(count($uniquegroup) > 0) {
			for($lk=0;$lk<count($uniquegroup);$lk++) {
				$emp_group_data=$this->db->select('employeeid,employeegroupname')
											->from('employeegroup')
											->where('employeegroupid',$uniquegroup[$lk])
											->limit(1)
											->get();
				if($emp_group_data->num_rows() > 0) {
					foreach($emp_group_data->result() as $info){
						$employeeids=$info->employeeid;						
						$employeegroupname=$info->employeegroupname;						
					}
					$groupnumarray=explode(',',$employeeids);
					foreach($groupnumarray as $key=>$value) {					
						$notification_array=array('notificationlogtypeid'=>'conversation','employeeid'=>$value,'notificationmessage'=>$empname.'mentioned u '.$employeegroupname.' in a conversation','createdate'=>date($this->Basefunctions->datef),'lastupdatedate'=>date($this->Basefunctions->datef),'createuserid'=>$this->Basefunctions->logemployeeid,'lastupdateuserid'=>$this->Basefunctions->logemployeeid,'status'=>$this->Basefunctions->activestatus,'moduleid'=>267,'commonid'=>$conversationid,'flag'=>1,'feedstype'=>2);
						$this->db->insert('notificationlog',array_filter($notification_array));						
					}
				}
			}
		}		
		echo true;		
	}
	//loads the comment data of a conversation-
	public function getconversationcomment() {	
		$rowid=$this->input->post('rowid');	
		$datafield=array();
		$editable = 'no';
		$userid = $this->Basefunctions->logemployeeid;
		$now=date($this->Basefunctions->datef);
		$this->db->select("notificationmessage,employeegroupid,conversationtitle,notificationlog.createdate,notificationlog.createuserid,abs(DATEDIFF(notificationlog.createdate,'".$now."'))AS DiffDate,TIMEDIFF(notificationlog.createdate,'".$now."') as difftime,notificationlog.notificationlogid,user.employeename as username,notificationlog.employeeid,employeeimage,employeeimage_path,notificationlog.filename,notificationlog.filepath",false);
		$this->db->from('notificationlog');
		$this->db->join('employee as user','user.employeeid=notificationlog.createuserid');
		$this->db->where('notificationlog.parentconversationid',$rowid);
		$this->db->where('notificationlog.status',$this->Basefunctions->activestatus);		
		$this->db->order_by('notificationlog.notificationlogid','desc');
		$data=$this->db->get();
		foreach ($data->result() as $vals) {
			$value='';
			$editable = 'no';
			$duration=$vals->DiffDate;
			if($duration > 1){
				$value = date('M Y',$vals->DiffDate);
			} else {
				$time=explode(':',$vals->difftime);	
				if(abs($time[0]) > 0) {	
					$value = abs($time[0]).' hrs ago';
				} else if(abs($time[1] > 0)) {
					$value = abs($time[1]).' min ago';
				} else if(abs($time[2] > 0)) {
					$value = abs($time[2]).' sec ago';
				} 
			}
			if($userid === $vals->createuserid) {
				$editable ='yes';
			}
			$replycount = $this->getreplycount($vals->notificationlogid);
			$formattedoutput=$this->formatmessage($vals->notificationmessage,$vals->employeeid,$vals->employeegroupid);
			$datafield[]=array('c_id'=>$vals->notificationlogid,'c_name'=>$formattedoutput,'userid'=>$vals->createuserid,'username'=>$vals->username,'employeeimage'=>'','c_date'=>$vals->createdate,'time'=>$value,'c_title'=>$vals->conversationtitle,'editable'=>$editable,'profile'=>$vals->employeeimage_path,'logid'=>$userid,'count'=>$replycount,'filepath'=>$vals->filepath,'filename'=>$vals->filename);			
		}
		echo json_encode($datafield);
	}
	//reset-counter
	public function resetnotificationcounter() {
		$logemployeeid=$this->Basefunctions->logemployeeid;
		$where=array('employeeid'=>$logemployeeid,'status'=>1);
		$update=array('notificationlog.flag'=>0);
		$this->db->where($where);
		$this->db->update('notificationlog',$update);
		echo true;
	}
	//get the notification count
	public function getnotificationcount() {
		$notcount=0;
		$value=sessionchecktimeinterval(); //-check whether the session exits every 1-minute
		$date = date('Y-m-d H:i:s');
		if($value == 'logout') {
			echo json_encode(array('count'=>'logout'));
		} else {
			$logemployeeid=$this->Basefunctions->logemployeeid;
			if($logemployeeid > 0) {
				$count=$this->db->query("SELECT notificationlogid FROM notificationlog WHERE employeeid = $logemployeeid AND date(createdate) <= '$date' AND `flag` = 1 AND `status` = 2 ");
				$notcount = $count->num_rows();
			}
			echo json_encode(array('count'=>$notcount));
		}
	}
	//load filterwidget
	public function loadfilterfield() {		
		$moduleid=$_GET['moduleid'];
		$table=$_GET['table'];
		$data=$this->db->select('viewcreationcolumnid,viewcreationcolumnname,uitypeid,viewcreationcolmodelindexname,viewcreationjoincolmodelname')
						->from('viewcreationcolumns')
						->where('viewcreationmoduleid',$moduleid)
						->where('viewcreationcolumnviewtype !=',0)
						->where('status',$this->Basefunctions->activestatus)
						->get()->result();
		foreach($data as $info) {
			$filterfield[]=array('viewcreationcolumnname'=>$info->viewcreationcolumnname,'uitypeid'=>$info->uitypeid,'viewcreationcolmodelindexname'=>$info->viewcreationcolmodelindexname,'viewcreationcolumnid'=>$info-> viewcreationcolumnid,'viewcreationjoincolmodelname'=>$info->viewcreationjoincolmodelname);
		}
		echo json_encode($filterfield);		
	}
	public function dashboarddatainformationfetch($dashboardid ) {
		$userroleid = $this->Basefunctions->userroleid;
        $i=0;
        $dashboarddatainfo = array();
		$this->db->select('dashboardfield.dashboardfieldid,dashboardfield.dashboardrowid,dashboardfield.dashboardfieldtype,dashboardfield.dashboardfiledname,dashboardfield.fieldviewtype,dashboardfield.fieldtheme,dashboardfield.fieldrotate,dashboardfield.fieldbasecolor,dashboardfield.fieldangle,dashboardfield.fieldlegend,dashboardfield.fieldlegendtype,dashboardfield.fieldparenttablename,dashboardfield.fieldchildtablename,dashboardfield.fieldconditionname,dashboardfield.fieldconditionvalue,dashboardfield.fieldrowcount,dashboardfield.reportid,dashboardfield.groupfieldid,dashboardfield.calculationfieldid,dashboardfield.operation,dashboardfield.countstatus,dashboard.moduleid');
		$this->db->from('dashboardfield');
		$this->db->join('dashboardrow','dashboardrow.dashboardrowid=dashboardfield.dashboardrowid');
		$this->db->join('dashboard','dashboard.dashboardid=dashboardrow.dashboardid');	
		$this->db->where('dashboardfield.dashboardfieldid',$dashboardid);		
		$this->db->where('dashboardfield.status',1);
		$this->db->limit(1);
		$querytbg = $this->db->get();
		foreach($querytbg->result() as $rows) {
		
			$dashboarddatainfo = array('chartid'=>$rows->dashboardfieldid,'charttabid'=>$rows->dashboardrowid,'charttype'=>$rows->dashboardfieldtype,'charttitle'=>$rows->dashboardfiledname,'chartviewtype'=>$rows->fieldviewtype,'charttheme'=>$rows->fieldtheme,'chartrotate'=>$rows->fieldrotate,'chartcolor'=>$rows->fieldbasecolor,'chartangle'=>$rows->fieldangle,'chartlegend'=>$rows->fieldlegend,'chartlegendtype'=>$rows->fieldlegendtype,'ptablname'=>$rows->fieldparenttablename,'ctablname'=>$rows->fieldchildtablename,'condfield'=>$rows->fieldconditionname,'condfieldval'=>$rows->fieldconditionvalue,'rowcount'=>$rows->fieldrowcount,'moduleid'=>$rows->moduleid);
			$i++;
		}
        return $dashboarddatainfo;
	}
	//load filter data
	public function loadfilterdata() {		
		$endlimitid='';
		if(isset($_GET['limit'])) {
			$endlimitid=$_GET['limit'];
		}
		$rowid=$_GET['dashboardrowid'];
		$filter_table=$_GET['table'];
		if(isset($_GET['columnfield'])) {
			$filter_field=$_GET['columnfield'];
		} else {
			$filter_field='';
		}
		$filter_clause=$_GET['clause'];
		$filter_value=$_GET['filtervalue'];
		if(isset($_GET['uitype'])) {
			$uitype=$_GET['uitype'];
			if($uitype == 8) {
				$filter_value =$this->Basefunctions->ymddateconversion($filter_value);
			}
		}
		if($filter_table == '' or $filter_field=='' or $filter_value == '') {
			$filter=" 1=1";
		} else {
			$filter=" ".$filter_table.".".$filter_field." = ".$filter_value." ";
			if($filter_clause == "like"){
				$filter=" ".$filter_table.".".$filter_field." LIKE '%".$filter_value."%' ";			
			}
		}
		$dashboardid = $_GET['dashboardwidgetid'];
		$chartdata=$this->dashboarddatainformationfetch($dashboardid);
		//
		$tblname = $chartdata['ptablname'];
		$fieldname = $chartdata['chartlegend'];
		$jointblname = $chartdata['ctablname'];
		$joinfields = $chartdata['chartlegendtype'];
		$rowcount = $chartdata['rowcount'];
		$dashboardfieldid=$chartdata['chartid'];
		//
		$dashboarddata=array('moduleid'=>$chartdata['moduleid'],'charttype'=>$chartdata['charttype']);
		//		
		$tablename = implode(',',explode('|',$tblname));
		$fieldname = implode(',',explode('|',$fieldname));
		$jointablename = implode(',',explode('|',$jointblname));
		$joinfieldid = implode(',',explode('|',$joinfields));
		
		$data=$this->Basefunctions->widgetfilterdata($tablename,$fieldname,$jointablename,$joinfieldid,$rowcount,$filter,$dashboarddata,$rowid,$endlimitid);
		echo $data;
	}
	//SMS Communication log
	public function getsmslogdata($moduleid,$rowid,$lastnode,$modname) {	
		$notificationdata=array();
		$now=date($this->Basefunctions->datef);
			$this->db->select(" '".$now."' as nowdate,module.modulename,module.modulemastertable,notificationlog.notificationlogtypeid,notificationlog.moduleid,commonid,a.employeeid,notificationmessage,notificationlog.createdate,notificationlog.createuserid,abs(DATEDIFF(notificationlog.createdate,'".$now."'))AS DiffDate,TIMEDIFF(notificationlog.createdate,'".$now."') as difftime,a.employeename as fromemp,b.employeename as toemp,module.modulelink,notificationlog.commonid,notificationlog.notificationlogid",false);
			$this->db->from('notificationlog');
			$this->db->join('module','module.moduleid=notificationlog.moduleid');
			$this->db->join('employee as a','a.employeeid=notificationlog.employeeid');
			$this->db->join('employee as b','b.employeeid=notificationlog.createuserid');
			$this->db->where('notificationlog.notificationlogtypeid',$modname);
			if($moduleid > 1 and $rowid > 1) {
				$this->db->where('notificationlog.commonid',$rowid);
				$this->db->where('notificationlog.moduleid',$moduleid);
			}
			if($lastnode != 0) {
				$this->db->where('notificationlog.notificationlogid <',$lastnode);
			}			
			$this->db->order_by('notificationlog.notificationlogid','desc');
			if($lastnode == 0) {
				$this->db->limit(4);
			} else {
				$this->db->limit(5);
			}
			$data=$this->db->get();
			if($data->num_rows() > 0) {
				foreach($data->result() as $info) {
					$duration=$info->DiffDate;
					if($duration > 1) {
						$value = $duration.' days ago';
					} else {
						$time=explode(':',$info->difftime);					
						if(abs($time[0]) > 0){	
							$value = abs($time[0]).' hrs ago';
						}
						else if(abs($time[1] > 0)){
							$value = abs($time[1]).' min ago';
						}
						else if(abs($time[2] > 0)){
							$value = abs($time[2]).' sec ago';
						} 
					}
					$identity=$info->notificationmessage;
					$msg = $info->notificationmessage;
					if($msg != '') {
						$message = explode('^',$msg);
						$notimsg = $message[1];
					} else {$notimsg = '';}
					if($message[0] == 'Received') {
						$nmessage = explode('-',$msg);
						$mobilenumber = '9898989898';
						if($mobilenumber == $nmessage[1] ) {
							$notificationdata[]=array('message'=>$notimsg,'duration'=>$value,'employeeonename'=>$info->fromemp,'employeeoneid'=>$info->employeeid,'employeetwoname'=>$info->toemp,'employeetwoid'=>$info->createuserid,'module'=>$info->modulename,'mastertable'=>$info->modulemastertable,'uniqueid'=>$info->commonid,'link'=>$info->modulelink,'moduleid'=>$info->moduleid,'operation'=>$info->notificationlogtypeid,'identity'=>$identity,'logid'=>$info->notificationlogid);
						}
					} else {
						$notificationdata[]=array('message'=>$notimsg,'duration'=>$value,'employeeonename'=>$info->fromemp,'employeeoneid'=>$info->employeeid,'employeetwoname'=>$info->toemp,'employeetwoid'=>$info->createuserid,'module'=>$info->modulename,'mastertable'=>$info->modulemastertable,'uniqueid'=>$info->commonid,'link'=>$info->modulelink,'moduleid'=>$info->moduleid,'operation'=>$info->notificationlogtypeid,'identity'=>$identity,'logid'=>$info->notificationlogid);
					}
					
				}
				return json_encode($notificationdata);
			} else { 
				return json_encode($notificationdata); 
			} 
	}
	//Receive SMS Communication log
	public function receivegetsmslogdata($moduleid,$rowid,$lastnode,$modname) {	
		$notificationdata=array();
		$now=date($this->Basefunctions->datef);
		if($moduleid > 1 and $rowid > 1) {
			$maintable = $this->Basefunctions->generalinformaion('module','modulemastertable','moduleid',$moduleid);
			$maintableid = $maintable.'id';
			$mobilenumber = $this->Basefunctions->generalinformaion($maintable,'mobilenumber',$maintableid,$rowid);	
		} else {
			$mobilenumber = 'Empty';
		}
		$this->db->select(" '".$now."' as nowdate,module.modulename,module.modulemastertable,notificationlog.notificationlogtypeid,notificationlog.moduleid,commonid,a.employeeid,notificationmessage,notificationlog.createdate,notificationlog.createuserid,abs(DATEDIFF(notificationlog.createdate,'".$now."'))AS DiffDate,TIMEDIFF(notificationlog.createdate,'".$now."') as difftime,a.employeename as fromemp,b.employeename as toemp,module.modulelink,notificationlog.commonid,notificationlog.notificationlogid",false);
		$this->db->from('notificationlog');
		$this->db->join('module','module.moduleid=notificationlog.moduleid');
		$this->db->join('employee as a','a.employeeid=notificationlog.employeeid');
		$this->db->join('employee as b','b.employeeid=notificationlog.createuserid');
		$this->db->where('notificationlog.notificationlogtypeid',$modname);
		if($moduleid > 1 and $rowid > 1) {
			$this->db->where('notificationlog.commonid',$rowid);
			$this->db->where('notificationlog.moduleid',$moduleid);
			$this->db->like('notificationlog.notificationmessage', $mobilenumber);
		}
		if($lastnode != 0) {
			$this->db->where('notificationlog.notificationlogid <',$lastnode);
		}			
		$this->db->order_by('notificationlog.notificationlogid','desc');
		if($lastnode == 0) {
			$this->db->limit(4);
		} else {
			$this->db->limit(5);
		}
		$data=$this->db->get();
		if($data->num_rows() > 0) {
			foreach($data->result() as $info) {
				$duration=$info->DiffDate;
				if($duration > 1) {
					$value = $duration.' days ago';
				} else {
					$time=explode(':',$info->difftime);					
					if(abs($time[0]) > 0){	
						$value = abs($time[0]).' hrs ago';
					}
					else if(abs($time[1] > 0)){
						$value = abs($time[1]).' min ago';
					}
					else if(abs($time[2] > 0)){
						$value = abs($time[2]).' sec ago';
					} 
				}
				$identity=$info->notificationmessage;
				$msg = $info->notificationmessage;
				if($msg != '') {
					$message = explode('^',$msg);
					$notimsg = $message[1];
				} else {$notimsg = '';}
				if($message[0] == 'Received ') {
					$nmessage = explode('-',$msg);
					$notificationdata[]=array('message'=>$notimsg,'duration'=>$value,'employeeonename'=>$info->fromemp,'employeeoneid'=>$info->employeeid,'employeetwoname'=>$info->toemp,'employeetwoid'=>$info->createuserid,'module'=>$info->modulename,'mastertable'=>$info->modulemastertable,'uniqueid'=>$info->commonid,'link'=>$info->modulelink,'moduleid'=>$info->moduleid,'operation'=>$info->notificationlogtypeid,'identity'=>$identity,'logid'=>$info->notificationlogid);
				}
			}
			return json_encode($notificationdata);
		} else { 
			return json_encode($notificationdata); 
		} 
	}
	//inbound call /outbound call log more data
	public function inboundcalllogdata($moduleid,$rowid,$lastnode,$modname) {	
		$notificationdata=array();
		$now=date($this->Basefunctions->datef);
		if($moduleid > 1 and $rowid > 1) {
			$maintable = $this->Basefunctions->generalinformaion('module','modulemastertable','moduleid',$moduleid);
			$maintableid = $maintable.'id';
			$mobilenumber = $this->Basefunctions->generalinformaion($maintable,'mobilenumber',$maintableid,$rowid);	
		} else {
			$mobilenumber = 'Empty';
		}
		$this->db->select(" '".$now."' as nowdate,module.modulename,module.modulemastertable,notificationlog.notificationlogtypeid,notificationlog.moduleid,commonid,a.employeeid,notificationmessage,notificationlog.createdate,notificationlog.createuserid,abs(DATEDIFF(notificationlog.createdate,'".$now."'))AS DiffDate,TIMEDIFF(notificationlog.createdate,'".$now."') as difftime,a.employeename as fromemp,b.employeename as toemp,module.modulelink,notificationlog.commonid,notificationlog.notificationlogid",false);
		$this->db->from('notificationlog');
		$this->db->join('module','module.moduleid=notificationlog.moduleid');
		$this->db->join('employee as a','a.employeeid=notificationlog.employeeid');
		$this->db->join('employee as b','b.employeeid=notificationlog.createuserid');
		$this->db->where('notificationlog.notificationlogtypeid',$modname);
		if($moduleid > 1 and $rowid > 1) {
			$this->db->where('notificationlog.commonid',$rowid);
			$this->db->where('notificationlog.moduleid',$moduleid);
			$this->db->like('notificationlog.notificationmessage', $mobilenumber);
		}
		if($lastnode != 0) {
			$this->db->where('notificationlog.notificationlogid <',$lastnode);
		}			
		$this->db->order_by('notificationlog.notificationlogid','desc');
		if($lastnode == 0) {
			$this->db->limit(4);
		} else {
			$this->db->limit(5);
		}
		$data=$this->db->get();
		if($data->num_rows() > 0) {
			foreach($data->result() as $info) {
				$duration=$info->DiffDate;
				if($duration > 1) {
					$value = $duration.' days ago';
				} else {
					$time=explode(':',$info->difftime);					
					if(abs($time[0]) > 0){	
						$value = abs($time[0]).' hrs ago';
					}
					else if(abs($time[1] > 0)){
						$value = abs($time[1]).' min ago';
					}
					else if(abs($time[2] > 0)){
						$value = abs($time[2]).' sec ago';
					} 
				}
				$identity=$info->notificationmessage;
				$msg = $info->notificationmessage;
				if($msg != '') {
					$message = explode('^',$msg);
					$notimsg = $message[1];
				} else {$notimsg = '';}
				if($message[0] == 'Received ') {
					$nmessage = explode('-',$msg);
					$notificationdata[]=array('message'=>$notimsg,'duration'=>$value,'employeeonename'=>$info->fromemp,'employeeoneid'=>$info->employeeid,'employeetwoname'=>$info->toemp,'employeetwoid'=>$info->createuserid,'module'=>$info->modulename,'mastertable'=>$info->modulemastertable,'uniqueid'=>$info->commonid,'link'=>$info->modulelink,'moduleid'=>$info->moduleid,'operation'=>$info->notificationlogtypeid,'identity'=>$identity,'logid'=>$info->notificationlogid);
				}
			}
			return json_encode($notificationdata);
		} else { 
			return json_encode($notificationdata); 
		} 
	}
	//For SMS Send using SMS Widget 
	public function smstemplateddvalfetchmodel() {
		$sendsmstype = $_GET['smssendtype'];
		$senderid = $_GET['senderid'];
		$i=0;
		$userid = $this->Basefunctions->userid;
		$this->db->select('leadtemplateid,leadtemplatename,smsgrouptypeid,moduleid');
		$this->db->from('leadtemplate');
		if($sendsmstype == '2'){
			$this->db->where_in('leadtemplate.smssettingsid',array($senderid));
		}
		$this->db->where_in('leadtemplate.smssendtypeid',array($sendsmstype));
		$this->db->where_in('leadtemplate.smsgrouptypeid',array(3,4));
		$this->db->where('leadtemplate.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data[$i] = array('datasid'=>$row->leadtemplateid,'dataname'=>$row->leadtemplatename,'typeid'=>$row->smsgrouptypeid,'moduleid'=>$row->moduleid);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}	
	}
	//Quick SMS send
	public function quickesmssendmodel() {
		$schdate="";
		$msgsenddate = "";
		$custmobile=$_GET['mobilenumber'];
		$apikey = $_GET['apikey'];
		$textmsg=$_GET['quicksmspreview'];
		$datacontent = $_GET['datacontent'];
		
		if(isset($_GET['smstype'])) {
			$type = $_GET['smstype']; 
		} else { 
			$type = 2; 
		}
		$lengthofchar = strlen($textmsg);
		if($type == 2 || $type == 3) {
			$maxcount = 1000;
			if($lengthofchar <= '160') {
				$smscount = 1;
			} else if(($lengthofchar > '160') && ($lengthofchar <= '306')) {
				$smscount = 2;	
			} else if(($lengthofchar > '306') && ($lengthofchar <= '459')) {
				$smscount = 3;	
			} else if(($lengthofchar > '459') && ($lengthofchar <= '612')) {
				$smscount = 4;	
			} else if(($lengthofchar > '612') && ($lengthofchar <= '765')) {
				$smscount = 5;	
			} else if(($lengthofchar > '765')&& ($lengthofchar <= '922')) {
				$smscount = 6;	
			} else if(($lengthofchar > '922')) {
				$smscount = 7;	
			}
		} else if($type == 4) {
			$maxcount = 500;
			if($lengthofchar <= '70') {
				$smscount = 1;
			}else if(($lengthofchar > '70') && ($lengthofchar <= '134')) {
				$smscount = 2;	
			} else if(($lengthofchar > '134') && ($lengthofchar <= '201')) {
				$smscount = 3;	
			} else if(($lengthofchar > '201') && ($lengthofchar <= '268')) {
				$smscount = 4;	
			} else if(($lengthofchar > '268') && ($lengthofchar <= '335')) {
				$smscount = 5;	
			} else if(($lengthofchar > '335') && ($lengthofchar <= '402')) {
				$smscount = 6;	
			} else if(($lengthofchar > '402') && ($lengthofchar <= '469')) {
				$smscount = 7;	
			} else if(($lengthofchar > '469')) {
				$smscount = 8;	
			}
		}
		if($datacontent != 'null') {
			$mobnum = explode(',',$custmobile);
			$mobmessage = explode('|||',$textmsg);
			$mobcount = count($mobnum);
			$smscount = $smscount * $mobcount;
		} else {
			$mobnum = array($custmobile);
			$mobmessage = array($textmsg);
			$mobcount = '1';
			$newmobnum = explode(',',$custmobile);
			$newmobcount = count($newmobnum);
			$smscount = $smscount * $newmobcount;
		}
		//sms send type 
		$smssendtype = $_GET['smssendtype'];
		if($smssendtype == 2) {
			$xmlurl = "http://alerts.sinfini.com/api/xmlapi.php?data=";
			$newstatusurl = "http://alerts.sinfini.com/api/status.php?workingkey=";
			$apikey = '13285h5r27e0u53ongr33';
			$addontypeid = 2;
		} else if($smssendtype == 3) {
			$xmlurl = "http://promo.sinfini.com/api/xmlapi.php?data=";
			$newstatusurl = "http://promo.sinfini.com/api/status.php?workingkey=";
			$apikey = 'Ac9bb7caf562bf6bfa7f9aab4c83ffde9';
			$sendername = 'BULKSMS';
			$addontypeid = 4;
		} else if($smssendtype == 4) {
			$xmlurl = "http://global.sinfini.com/api/xmlapi.php?data=";
			$newstatusurl = "http://global.sinfini.com/api/status.php?workingkey=";
			$apikey = 'A1363f16118616942397968b9a9f736da';
			$sendername = 'AUCGLO';
			$addontypeid = 5;
		}
		$credits = $this->accountcresitdetailsfetchmodel($apikey,$addontypeid);
		if( $credits >= $smscount ) {
			//sms type id
			if($type == 2 ) { 
				$unicode = 'N'; 
				$flash = 'N';
			} else if($type == 3 ) { 
				$unicode = 'N'; 
				$flash = 'Y';
			} else if($type == 4 ) { 
				$unicode = 'Y'; 
				$flash = 'N';
			}
			//Record id
			if(isset($_GET['quickrecordid'])) {
				$recordid = $_GET['quickrecordid']; 
				if($recordid == '') { 
					$recordid = 1;
				}
			} else { 
				$recordid = 1;
			}
			//templateid
			if(isset($_GET['smstemplateid'])) {
				$leadtemplateid = $_GET['smstemplateid']; 
				if($leadtemplateid == '') { 
					$leadtemplateid = 1;
				}
			} else { 
				$leadtemplateid = 1;
			}
			//Sender Name
			if(isset($_GET['senderid'])) {
				$settingsid = $_GET['senderid']; 
				if($settingsid == '') { 
					$settingsid = '1';
				}
			} else { 
				$settingsid = '1';
			}
			//Sender Settings id
			if(isset($_GET['sendername'])) { 
				$sendername = $_GET['sendername']; 
			}  else { 
				$sendername = '';
			};
			//Signature id
			if(isset($_GET['quicksignature'])) {
				$signatureid = $_GET['quicksignature']; 
				if($signatureid == '') { 
					$signatureid = 1;
				}
			} else { 
				$signatureid = 1;
			}
			$date = $_GET['quickscheduledate'];
			$time = $_GET['quickscheduletime'];
			if($time == ''){ $time = '00:00:00';}
			if($date!='') {
				$schtimestamp = strtotime($date);
				$schdate = date("Y-m-d", $schtimestamp);
				$tosplitmins = explode(':',$time);
				$roundofhr = $tosplitmins[0];
				if ($tosplitmins[1] <= 15) { 
					$roundofmin = '15';
				}
				if ($tosplitmins[1] >= 16 && $tosplitmins[1] <=30) { 
					$roundofmin = '30';
				}
				if ($tosplitmins[1] >= 31 && $tosplitmins[1] <=45) {
					$roundofmin = '45';
				}
				if ($tosplitmins[1] >= 46 && $tosplitmins[1] <=60) {
					$roundofmin = '00'; $roundofhr = $tosplitmins[0] + 1;
				}
				if($roundofhr > '12') { 
					$roundofhr = $roundofhr - 12; 
					$ttt = 'PM';
				} else { 
					$roundofhr = $roundofhr; 
					$ttt = 'AM';
				}
				if($roundofhr < '9' ) { 
					$roundofhr = '0'.$roundofhr;
				} else {
					$roundofhr = $roundofhr;
				}
				$scheduledate = $schdate.' '.$roundofhr.':'.$roundofmin.' '.$ttt;
			} else {
				$scheduledate="";
			}
			if($scheduledate != '') {
				$communicationmode = 'SCHEDULE SMS';
			} else {
				$communicationmode = 'SENT SMS';
			}
			/**********  solution infini  **************/	
			//sms sending code
			for($i=0;$i<$mobcount;$i++) {
				$data='<?xml version="1.0" encoding="UTF-8"?>
				<xmlapi>
				<auth>
				<apikey>'.$apikey.'</apikey>
				</auth>
				<sendSMS>
				<to>'.$mobnum[$i].'</to>
				<text>'.$mobmessage[$i].'</text>
				<msgid>'.$sendername.'</msgid>
				<sender>'.$sendername.'</sender>
				</sendSMS>
				<response>Y</response>
				<unicode>'.$unicode.'</unicode>
				<flash>'.$flash.'</flash>
				<time>'.$scheduledate.'</time>
				</xmlapi>';
				$url = $xmlurl.urlencode($data)."&type=json";
				$ch=curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$output=curl_exec($ch);
				curl_close($ch);
				//fetch & update delivery status
				$datasets = $this->parse_response($output);
				if(!isset($datasets[0]['errcode'])) {
					sleep(15);
					foreach($datasets as $key => $value) {
						$statusurl = $newstatusurl.$apikey."&messageid=".$value['msgid']."";
						$chk=curl_init();
						curl_setopt($chk, CURLOPT_URL, $statusurl);
						curl_setopt($chk, CURLOPT_RETURNTRANSFER, true);
						$statusoutput=curl_exec($chk);
						curl_close($chk);
						$smsstatus = explode(' ',trim($statusoutput));
						$salesstatus = $this->statusvalueget($smsstatus[2]);
						//comm log
						$smslog = array( 
							'commonid'=>$recordid,
							'moduleid'=>1,
							'employeeid'=>$this->Basefunctions->userid,
							'smstypeid'=>$type,
							'smssettingsid'=>$settingsid,
							'leadtemplateid'=>$leadtemplateid,
							'signatureid'=>$signatureid,
							'crmcommunicationmode'=>$communicationmode,
							'communicationfrom'=>0,
							'communicationto'=>$smsstatus[1],
							'smscount'=>$smscount,
							'groupid'=>$smsstatus[0],
							'communicationstatus'=>$salesstatus,
							'communicationdate'=>date($this->Basefunctions->datef),
							'createdate'=>date($this->Basefunctions->datef),
							'lastupdatedate'=>date($this->Basefunctions->datef),
							'createuserid'=>$this->Basefunctions->userid,
							'lastupdateuserid'=>$this->Basefunctions->userid,
							'status'=>$this->Basefunctions->activestatus
						);
						$this->db->insert('crmcommunicationlog',$smslog);
						$primaryid = $this->Basefunctions->maximumid('crmcommunicationlog','crmcommunicationlogid');
						//notification log entry
						$empid = $this->Basefunctions->logemployeeid;
						$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
						$notimsg = $empname." "."Sent a SMS to"." - ".$smsstatus[1]." Status"." - ".$salesstatus;
						$this->Basefunctions->notificationcontentadd($primaryid,'SMS',$notimsg,1,210);
						if($smssendtype == 4) {
							$credit = $this->db->query("UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = `addonavailablecredit` - $smscount WHERE `addonstypeid` = $addontypeid");	
						}
						//master company id get - master db update
						$mastercompanyid = $this->Basefunctions->generalinformaion('company','mastercompanyid','companyid',2);
						$CI =& get_instance();
						$CI->load->database();
						$hostname = $CI->db->hostname; 
						$username = $CI->db->username; 
						$password = $CI->db->password; 
						$masterdb = $CI->db->masterdb; 
						$con = mysqli_connect($hostname,$username,$password,$masterdb);
						// Check connection
						if (mysqli_connect_errno())	{
							echo "Failed to connect to MySQL: " . mysqli_connect_error();
						}
						if($smssendtype == 4) {
							mysqli_query($con,"UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = `addonavailablecredit` - $smscount WHERE `addonstypeid` = $addontypeid and mastercompanyid ='".$mastercompanyid."'");
						}
						mysqli_close($con);				
					}
				} 
			}
			if(!isset($datasets[0]['errcode'])) {
				echo json_encode('success');
			} else {
				echo json_encode($datasets[0]['desc']);
			}
		} else {
			echo json_encode('Credits');
		}
	}
	//fetch xml content
	public function parse_response($response) {
		$xml = new SimpleXMLElement($response);
		$result = array();
		$h=0;
		foreach($xml as $key => $value) {
			if( $key == 'to' || $key == 'msgid' || $key == 'status' ) {
				$result[$h][(string)$key] = (string)$value;
				if( count($result[$h]) == '3' ) {
					$h++;
				}
			} else if($key == 'errcode' || $key == 'desc'){
				$result[$h][(string)$key] = (string)$value;
				if( count($result[$h]) == '2' ) {
					$h++;
				}
			}
		}
		return $result;
	}
	//mobile number fetch
	public function mobilenumfetchmodel() {
		$moduleid = $_GET['moduleid'];
		$recordid = $_GET['recordid'];
		$this->db->select('modulemastertable');
		$this->db->from('module');
		$this->db->where('module.moduleid',$moduleid);
		$this->db->where('module.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$tablename = $row->modulemastertable;
			}
		}else { $tablename = ''; }
		if($tablename != ''){
			$check = $this->tablefieldnamecheck($tablename,'mobilenumber');
			if($check == 'Yes') {
				$id = $tablename.'id';
				$this->db->select('mobilenumber');
				$this->db->from("$tablename");
				$this->db->where("$tablename".'.'."$id",$recordid);
				$this->db->where("$tablename".'.status',1);
				$result = $this->db->get();
				if($result->num_rows() > 0) {
					foreach($result->result() as $row) {
						$moblenum = $row->mobilenumber;
					}
				} else { $moblenum = ''; }
			} else { $moblenum = ''; }
		} else { $moblenum = ''; }
		echo $moblenum;
	}
	//Filed check
	public function tablefieldnamecheck($tblname,$fieldname) {
		$fields = $this->db->field_exists($fieldname,$tblname);
		if($fields == '1') {
			return 'Yes';
		} else {
			return 'No';
		}
	}
	//table name check
	public function tableexitcheck($database,$newtable) {
		$counttab=0;
		$tabexits = $this->db->query("SELECT COUNT(*) AS tabcount FROM information_schema.tables WHERE table_schema = '".$database."'AND table_name = '".$newtable."'");
		foreach($tabexits->result() as $tkey) { 
			$counttab = $tkey->tabcount;
		};
		return $counttab;
	}
	//fetch parent table information
	public function fetchfieldparenttable($tablname,$fieldname) {
		$pardata = array();
		$datas = $this->db->query('SELECT modulefieldid,uitypeid,parenttable,moduletabid  FROM modulefield WHERE columnname LIKE "%'.$fieldname.'%" AND tablename LIKE "%'.$tablname.'%" AND status IN (1,10)',false);
		foreach($datas->result() as $data) {
			$pardata = array('uitype'=>$data->uitypeid,'partab'=>$data->parenttable,'modid'=>$data->moduletabid);
		}
		return $pardata;
	}
	//custom data widget data fetch
	public function customdatawidget($fielddata,$parenttable,$id,$moduleid){
		$spliteddata = explode('|',$fielddata);
		$finaldata=array();
		for($mj=0;$mj < count($spliteddata);$mj++) {
			$datafield = explode('_',$spliteddata[$mj]);
			$ddatafield = explode('#',$datafield[0]);
			$data = $this->customwidgetdataretrieve($ddatafield[1],$parenttable,$id,$moduleid);
			$extradata = $this->customwidgeteditdataretrieve($ddatafield[0]);
			if(count($data) > 0){
				$final[$datafield[1]] = $data[0];
				$efinal[$datafield[1]] = $extradata;
			} else {
				$final[$datafield[1]] = '';
				$efinal[$datafield[1]] = '';
			}
		}
		$ffianl[0] = $final;
		$ffianl[1] = $efinal;
		return json_encode($ffianl);	
	}
	public function customwidgetdataretrieve($mergegrp,$parenttable,$id,$moduleid){
		$relmodids = $this->fetchrelatedmodulelist($moduleid);
		$primaryid = $id;
		$database = $this->db->database;
		$resultset = array();
		$mergeindval = $mergegrp;
		$formatortype = "1";
		$id = $primaryid;
		//parent child field concept
		$elname = array('parentcategoryid','parentstoragecategoryid','parentaccountid');
		$fname = array('categoryname','storagecategoryname','accountname');
		$fnameid = array('categoryid','storagecategoryid','accountid');
		$tname = array('category','storagecategory','account');
		$empid = $this->Basefunctions->userid;
		
		$newdata = array();
		
		$tempval = $mergeindval;
		$newval = explode('.',$tempval);
		$reltabname = explode(':',$newval[0]);
		$table = $reltabname[0];
		$uitypeid = 2;
		//chk editor field name
		$chkeditfname = explode('_',$newval[1]);
		$editorfname = ( (count($chkeditfname) >= 2)? $chkeditfname[1] : '');
		if($table!='REL' && $table!='GEN') { //parent table fields information
				$tablename = trim($reltabname[0]);
				$tabfield = $newval[1];
				
				//field fetch parent table fetch
				$fieldnamechk = explode('-',$tabfield);
				$fldname = ( (count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
				$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
				$uitypeid = ( (count($fieldinfo) > 0)? $fieldinfo['uitype'] : 1);
				if($fldname != 'attributesetid') {
					//get user company & branch id
					if($tablename == 'company' || $tablename == 'branch') {
						if($tablename == 'company') {
							$compquery = $this->db->query('select company.companyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND company.status=1')->result();
							foreach($compquery as $key) {
								$id = $key->companyid;
							}
						} else if($tablename == 'branch') {
							$brquery = $this->db->query('select branch.branchid from branch join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND branch.status=1')->result();
							foreach($brquery as $key) {
								$id = $key->branchid;
							}
						}
					}
					//check its parent child field concept
					if(in_array($fldname,$elname)) {
						$key = array_search($fldname,$elname);
						$pid = $id;
						if($key != NULL) {
							//parent id get
							$result = $this->db->select("$tname[$key].$elname[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$id)->where($tname[$key].'.status',1)->get();
							if($result->num_rows() > 0) {
								foreach($result->result()as $row) {
									$pid=$row->$elname[$key];
								}
								if($pid!=0) {
									//parent name
									$result = $this->db->select("$tname[$key].$fname[$key],$tname[$key].$fnameid[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$pid)->where($tname[$key].'.status',1)->get();
									if($result->num_rows() > 0) {
										foreach($result->result()as $row) {
											$resultset[]=$row->$fname[$key];
										}
									}
								}
							}
						}
					}
					else {
						if($fldname == 'employeeid') {
							$emptypeid = $this->Basefunctions->generalinformaion($parenttable,'employeetypeid',$parenttable.'id',$primaryid);
							$empgrpid = $this->Basefunctions->generalinformaion($parenttable,'employeeid',$parenttable.'id',$primaryid);
							if($emptypeid == '1') {
								$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empgrpid);
							}else if($emptypeid == '2') {
								$empname = $this->Basefunctions->generalinformaion('employeegroup','employeegroupname','employeegroupid',$empgrpid);
							} else {
								$empname = $this->Basefunctions->generalinformaion('userrole','userrolename','userroleid',$empgrpid);
							}
							$resultset[] = $empname;
						} else {
						//field fetch parent table fetch
						$fieldnamechk = explode('-',$tabfield);
						$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
						$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
							if(count($fieldinfo)>0) {
								$uitypeid = $fieldinfo['uitype'];
								//fetch parent table join id.
								$relpartable = $fieldinfo['partab'];
								
								$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
								$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
								$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
								//fetch original field of picklist data in view creation 
								$viewfieldinfo = $this->viewfieldsinformation($tablename,$tabfield,$fieldinfo['modid']);
								$tabfieldname = ( (count($viewfieldinfo)>0)? $viewfieldinfo['joinfieldname'] : $tabfield );
								$jointabfieldid = ( (count($viewfieldinfo)>0)? $viewfieldinfo['condfiledname'] : $tabfield );
								
								//join relation table and main table
								$joinq="";
								if($relpartable!=$tablename) {
									$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id AND '.$relpartable.'.status=1';
								}
								//main table join
								if($mainjointable!=$parenttable) {
									$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id AND '.$relpartable.'.status=1';
								}
								//if its picklist data
								$newfield = substr( $tabfieldname,( strlen($tabfieldname)-2 ));
								$tabname = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
								$tbachk = $this->tableexitcheck($database,$tabname);
								if( $newfield == "id" && $tbachk != 0 ) {
									$newtable = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
									$newjfield = $newtable."name";
									$whtable = (($tablename=="company" || $tablename=="branch")? $tablename : $parenttable);
								if($newtable == $tablename){ //used on same table parent-child relations(account/contact)
								$temptablename=$tablename.' as temp';
								$newjdata = $this->db->query("SELECT ".$newtable.".".$newtable."id AS columdataid,".$newtable.".".$newjfield." AS columdata 
									FROM ".$newtable." 
									LEFT OUTER JOIN ".$temptablename." ON ".$tablename.".".$fldname." = ".$newtable.".".$tabfieldname."".$joinq." WHERE temp.".$whtable."id = ".$id." AND ".$newtable.".status=1 AND temp.status=1",false); 
								} else {						
								$newjdata = $this->db->query("SELECT ".$newtable.".".$newtable."id AS columdataid,".$newtable.".".$newjfield." AS columdata 
									FROM ".$newtable." 
									LEFT OUTER JOIN ".$tablename." ON ".$tablename.".".$fldname." = ".$newtable.".".$tabfieldname."".$joinq." WHERE ".$tablename.".".$whtable."id = ".$id." AND ".$newtable.".status=1 AND ".$tablename.".status=1",false); 
								}
									if($newjdata->num_rows() >0) {
										foreach($newjdata->result() as $jkey) {
											$resultset[] = $jkey->columdata;
										}
									} else {
										$resultset[] = " ";
									}
								}
								else {	
									//main table data sets information fetching
									$fldcountchk = explode('-',$tabfield);
									$tablcolumncheck = $this->tablefieldnamecheck($tablename,$parenttable."id");
									if( ( $parenttable == $tablename && (count($fldcountchk)) < 2 ) || ( $tablcolumncheck == 'false' && (count($fldcountchk)) < 2 ) ) {
										$newdata = $this->db->select("".$tabfield." AS columdata")->from($tablename)->where($tablename.".".$tablename."id",$id)->where($tablename.".status",1)->get()->result();
									}
									else {
										$cond = "";
										$addfield = explode('-',$tabfield);
										if( (count($addfield)) >= 2 ) {
											$datafiledname=$addfield[1];
											if(in_array('PR',$addfield)) {
												$cond = ' AND '.$tablename.'.addresstypeid=2';
											} else if(in_array('SE',$addfield)) {
												$cond = ' AND '.$tablename.'.addresstypeid=3'; 
											} else if(in_array('BI',$addfield)) {
												$cond = ' AND '.$tablename.'.addresstypeid=4';
											} else if(in_array('SH',$addfield)) {
												$cond = ' AND '.$tablename.'.addresstypeid=5';
											} else if(in_array('CW',$addfield)) {
												$formatortype = "CW";
											} else if(in_array('CF',$addfield)) {
												$formatortype = "CF";
											} else if(in_array('CS',$addfield)) {
												$formatortype = "CS";
											}
											if($tablename!=$parenttable) {
												$tablcolumncheck = $this->tablefieldnamecheck($tablename,"addresstypeid");
												$cond = ( ($tablcolumncheck == 'true')? $cond : ' ');
												$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename." JOIN ".$parenttable." ON ".$parenttable.".".$parenttable."id = ".$tablename.".".$parenttable."id WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1".$cond."")->result();
											} else {
												$newdata = $this->db->select(''.$datafiledname.' AS columdata',false)->from($tablename)->where($tablename.'id',$id)->where($tablename.'.status',1)->get()->result();
											}
										}
										else {
											$newdata = $this->db->query("SELECT ".$tablename.".".$tabfield." AS columdata FROM ".$tablename." JOIN ".$parenttable." ON ".$parenttable.".".$parenttable."id = ".$tablename.".".$parenttable."id WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1")->result();
										}
									}
									foreach($newdata as $key) {
										if($tabfield == 'companylogo') {
											$imgname = $key->columdata;
											if(filter_var($imgname,FILTER_VALIDATE_URL)) {
												$resultset[] = '<img src="'.$imgname.'" />';
											} else {
												if( file_exists($imgname) ) {
													$resultset[] = '<img src="'.$imgname.'" style="width:11em; height:3em" />';
												} else {
													$resultset[] = " ";
												}
											}
										}
										else {
											$currencyinfo = $this->currencyinformation($parenttable,$id,$moduleid);
											if($formatortype=="CW") { //convert to word
												if(count($currencyinfo)>0) {
													$resultset[] = $this->convert->numtowordconvert($key->columdata,$currencyinfo['name'],$currencyinfo['subcode']);
												} else {
													$resultset[] = $this->convert->numtowordconvert($key->columdata);
												}
											} else if($formatortype=="CF") { //currenct format
												if(count($currencyinfo)>0) {
													$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
												} else {
													$resultset[] = $this->convert->formatcurrency($key->columdata);
												}
											} else if($formatortype=="CS") { //currency with symbol
												if(count($currencyinfo)>0) {
													$amt = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
													$resultset[] = $currencyinfo['symbol'].' '.$amt;
												} else {
													$resultset[] = $currencyinfo['symbol'].' '.$this->convert->formatcurrency($key->columdata);
												}
											} else {
												if(is_numeric($key->columdata)) {
													if($uitypeid == '4' || $uitypeid == '5' || $uitypeid == '7') {
														if(count($currencyinfo)>0) {
															$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
														} else {
															$resultset[] = $this->convert->formatcurrency($key->columdata);
														}
													} else {
														$resultset[] = $key->columdata;
													}
												} else {
													if($editorfname=='editorfilename') {
														if( file_exists($key->columdata) ) {
															$datas = $this->geteditorfilecontent($key->columdata);
															$datas = preg_replace('~a10s~','&nbsp;', $datas);
															$datas = preg_replace('~<br>~','<br />', $datas);
															$resultset[] = $datas;
														} else {
															$resultset[] = '';
														}
													} else {
														$resultset[] = $key->columdata;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				} 
		} 
		else if($table=='REL') { //related module fields information fetch
			$tablename = trim($reltabname[1]);
			$tabfield = trim($newval[1]);
			$fieldnamechk = explode('-',$tabfield);
			$fldname = ( (count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
			if($fldname != 'attributesetid') {
				//get user company & branch id
				if($tablename == 'company' || $tablename == 'branch') {
					if($tablename == 'company') {
						$compquery = $this->db->query('select company.companyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND company.status=1')->result();
						foreach($compquery as $key) {
							$id = $key->companyid;
						}
					} else if($tablename == 'branch') {
						$brquery = $this->db->query('select branch.branchid from branch join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND branch.status=1')->result();
						foreach($brquery as $key) {
							$id = $key->branchid;
						}
					}
				}
				//check its parent child field concept
				$fldnamechk = explode('-',$tabfield);
				$fldname = ( (count($fldnamechk) >= 2)? $fldnamechk[1] : $tabfield);
				if(in_array($fldname,$elname)) {
					$key = array_search($fldname,$elname);
					$pid = $id;
					if($key != NULL) {
						//parent id get
						$result = $this->db->select("$tname[$key].$elname[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$id)->where($tname[$key].'.status',1)->get();
						if($result->num_rows() > 0) {
							foreach($result->result()as $row) {
								$pid=$row->$elname[$key];
							}
							if($pid!=0) {
								//parent name
								$result = $this->db->select("$tname[$key].$fname[$key],$tname[$key].$fnameid[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$pid)->where($tname[$key].'.status',1)->get();
								if($result->num_rows() > 0) {
									foreach($result->result()as $row) {
										$resultset[]=$row->$fname[$key];
									}
								}
							}
						}
					}
				}
				else {
					if($fldname == 'employeeid') {
						$relationtableid = $this->Basefunctions->generalinformaion($parenttable,$reltabname[1].'id',$parenttable.'id',$primaryid);
						$emptypeid = $this->Basefunctions->generalinformaion($reltabname[1],'employeetypeid',$reltabname[1].'id',$relationtableid);
						$empgrpid = $this->Basefunctions->generalinformaion($reltabname[1],'employeeid',$reltabname[1].'id',$relationtableid);
						if($emptypeid == '1') {
							$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empgrpid);
						}else if($emptypeid == '2') {
							$empname = $this->Basefunctions->generalinformaion('employeegroup','employeegroupname','employeegroupid',$empgrpid);
						} else {
							$empname = $this->Basefunctions->generalinformaion('userrole','userrolename','userroleid',$empgrpid);
						} 
						$resultset[] = $empname;
					} else {
						//field fetch parent table fetch
						$fieldnamechk = explode('-',$tabfield);
						$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
						$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
						if(count($fieldinfo)>0) {
							$uitypeid = $fieldinfo['uitype'];
							//fetch parent table join id
							$relpartable = $fieldinfo['partab'];
							$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
							$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
							$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
							//fetch original field of picklist data in view creation 
							$viewfieldinfo = $this->viewfieldsinformation($tablename,$tabfield,$fieldinfo['modid']);
							$tabfieldname = ( (count($viewfieldinfo)>0)? $viewfieldinfo['joinfieldname'] : $tabfield );
							$jointabfieldid = ( (count($viewfieldinfo)>0)? $viewfieldinfo['condfiledname'] : $tabfield );
							//join relation table and main table
							$joinq="";
							if($relpartable!=$tablename) {
								$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id AND '.$relpartable.'.status=1';
							}
							//main table join
							if($mainjointable==$parenttable) {
								$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id AND '.$mainjointable.'.status=1';
							} else {
								$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id AND '.$mainjointable.'.status=1';
								$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$mainjointable.'.'.$parenttable.'id AND '.$parenttable.'.status=1';
							}
							//if its picklist data
							$newfield = substr( $tabfieldname,( strlen($tabfieldname)-2 ));
							$tabname = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
							$tblchk = $this->tableexitcheck($database,$tabname);
							if( $newfield == "id" && $tblchk != 0 ) {
								$newtable = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
								$newjfield = $newtable."name";
								$whtable = (($tablename=="company" || $tablename=="branch")? $tablename : $parenttable);
								$newjdata = $this->db->query("SELECT ".$newtable.".".$newtable."id AS columdataid,".$newtable.".".$newjfield." AS columdata FROM ".$newtable." LEFT OUTER JOIN ".$tablename." ON ".$tablename.".".$jointabfieldid." = ".$newtable.".".$tabfieldname."".$joinq." WHERE ".$parenttable.".".$whtable."id = ".$id." AND ".$mainjointable.".status=1 AND ".$newtable.".status=1 AND ".$tablename.".status=1");
								if($newjdata->num_rows() >0) {
									foreach($newjdata->result() as $jkey) {
										$resultset[] = $jkey->columdata;
									}
								} else {
									$resultset[] = " ";
								}
							}
							else {
								//main table data sets information fetching
								$fldcountchk = explode('-',$tabfield);
								$tablcolumncheck = $this->tablefieldnamecheck($tablename,$parenttable."id");
								if( ( $parenttable == $tablename && (count($fldcountchk)) < 2 ) || ( $tablcolumncheck == 'false' && (count($fldcountchk)) < 2 ) ) {
									$newdata = $this->db->query("SELECT ".$tablename.".".$tabfield." AS columdata FROM ".$tablename."".$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1")->result();
								}
								else {
									$cond = "";
									$addfield = explode('-',$tabfield);
									if( (count($addfield)) >= 2 ) {
										$datafiledname=$addfield[1];
										if(in_array('PR',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=2';
										} else if(in_array('SE',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=3'; 
										} else if(in_array('BI',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=4';
										} else if(in_array('SH',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=5';
										} else if(in_array('CW',$addfield)) {
											$formatortype = "CW";
										} else if(in_array('CF',$addfield)) {
											$formatortype = "CF";
										} else if(in_array('CS',$addfield)) {
											$formatortype = "CS";
										}
										$tablcolumncheck = $this->tablefieldnamecheck($tablename,"addresstypeid");
										$cond = ( ($tablcolumncheck == 'true')? $cond : ' ');
										if($tablename!=$parenttable) {
											$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename."".$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1".$cond."")->result();
										} else {
											$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename." WHERE ".$tablename."id = ".$id." AND ".$tablename.".status=1")->result();
										}
									}
									else {
										$newdata = $this->db->query("SELECT ".$tabfield." AS columdata FROM ".$tablename."".$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1".$cond."")->result();
									}
								}
								foreach($newdata as $key) {
									if($tabfield == 'companylogo') {
										$imgname = $key->columdata;
										if(filter_var($imgname, FILTER_VALIDATE_URL)) {
											$resultset[] = '<img src="'.$imgname.'" />';
										} else {
											if( file_exists($imgname) ) {
												$resultset[] = '<img src="'.$imgname.'" style="width:11em; height:3em" />';
											} else {
												$resultset[] = " ";
											}
										}
									}
									else {
										$currencyinfo = $this->currencyinformation($parenttable,$id,$moduleid);
										if($formatortype=="CW") { //convert to word
											if(count($currencyinfo)>0) {
												$resultset[] = $this->convert->numtowordconvert($key->columdata,$currencyinfo['name'],$currencyinfo['subcode']);
											} else {
												$resultset[] = $this->convert->numtowordconvert($key->columdata);
											}
										} else if($formatortype=="CF") { //currenct format
											if(count($currencyinfo)>0) {
												$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
											} else {
												$resultset[] = $this->convert->formatcurrency($key->columdata);
											}
										} else if($formatortype=="CS") { //currency with symbol
											if(count($currencyinfo)>0) {
												$amt = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
												$resultset[] = $currencyinfo['symbol'].' '.$amt;
											} else {
												$resultset[] = $currencyinfo['symbol'].' '.$this->convert->formatcurrency($key->columdata);
											}
										} else {
											if(is_numeric($key->columdata)) {
												if( $uitypeid == '4' || $uitypeid == '5' || $uitypeid == '7' ) {
													if(count($currencyinfo)>0) {
														$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
													} else {
														$resultset[] = $this->convert->formatcurrency($key->columdata);
													}
												} else {
													$resultset[] = $key->columdata;
												}
											} else {
												if($editorfname=='editorfilename') {
													if( file_exists($key->columdata) ) {
														$datas = $this->geteditorfilecontent($key->columdata);
														$datas = preg_replace('~a10s~','&nbsp;', $datas);
														$datas = preg_replace('~<br>~','<br />', $datas);
														$resultset[] = $datas;
													} else {
														$resultset[] = '';
													}
												} else {
													$resultset[] = $key->columdata;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			} 
				
		}
		return $resultset;
	}
	///////////////
	//fetch main table join information
	public function fetchjoincolmodelname($relpartable,$moduleid) {
		$joindata = array();
		$datas = $this->db->query('SELECT modulerelationid,parentfieldname,parenttable FROM modulerelation WHERE moduleid = '.$moduleid.' AND relatedtable LIKE "%'.$relpartable.'%" AND status=1',false);
		foreach($datas->result() as $data) {
			$joindata = array('relid'=>$data->modulerelationid,'joinfieldname'=>$data->parentfieldname,'jointabl'=>$data->parenttable);
		}
		return $joindata;
	}
	//fetch relation and view type 3 field information
	public function viewfieldsinformation($tablename,$tabfield,$modid) {
		$datasets = array();
		$viewfieldinfo = $this->db->query('SELECT viewcreationcolumnid,viewcreationparenttable,viewcreationjoincolmodelname,viewcreationcolmodelcondname FROM viewcreationcolumns WHERE viewcreationparenttable LIKE "%'.$tablename.'%" AND viewcreationcolmodelcondname LIKE "%'.$tabfield.'%" AND viewcreationtype=3 AND viewcreationmoduleid='.$modid.' AND status=1');
		foreach($viewfieldinfo->result() as $viewdata) {
			$datasets = array('viewcolid'=>$viewdata->viewcreationcolumnid,'joinfieldname'=>$viewdata->viewcreationjoincolmodelname,'condfiledname'=>$viewdata->viewcreationcolmodelcondname);
		}
		return $datasets;
	}
	//fetch currency information
	public function currencyinformation($parenttable,$id,$moduleid) {
		$datasets = array();
		$colmodeltable = "";
		$cid = 1;
		//modulerelation
		$datasets = $this->db->select('viewcreationcolumnid,viewcreationcolmodeltable,viewcreationjoincolmodelname,viewcreationparenttable,viewcreationtype,viewcreationcolmodelcondname',false)->from('viewcreationcolumns')->where('viewcreationmoduleid',$moduleid)->where('viewcreationjoincolmodelname','currencyid')->where('status',1)->get();
		if($datasets->num_rows()>0) {
			foreach($datasets->result() as $data) {
				$colname =  $data->viewcreationcolmodeltable;
				$colmodeltable = $data->viewcreationcolmodeltable;
				$joincolmodelname= $data->viewcreationjoincolmodelname;
				$colmodelpartable = $data->viewcreationparenttable;
				$viewtype = $data->viewcreationtype;
				$viewcolcondname = $data->viewcreationcolmodelcondname;
			}
		}
		if($colmodeltable!= "") {
			$joinq="";
			if($viewtype==3) {
				if($colmodelpartable!="currency") {
					if($colmodelpartable == $parenttable) {
						$joinq = 'LEFT OUTER JOIN '.$colmodelpartable.' ON '.$colmodelpartable.'.'.$viewcolcondname.'='.$colmodeltable.'.'.$joincolmodelname;
					} else {
						$joinq = 'LEFT OUTER JOIN '.$colmodelpartable.' ON '.$colmodelpartable.'.'.$viewcolcondname.'='.$colmodeltable.'.'.$joincolmodelname;
						$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$colmodelpartable.'.'.$parenttable.'id';
					}
				}
			} else {
				if($colmodelpartable!="currency") {
					if($colmodelpartable == $parenttable) {
						$joinq = 'LEFT OUTER JOIN '.$colmodelpartable.' ON '.$colmodelpartable.'.'.$joincolmodelname.'='.$colmodeltable.'.'.$joincolmodelname;
					} else {
						$joinq = 'LEFT OUTER JOIN '.$colmodelpartable.' ON '.$colmodelpartable.'.'.$joincolmodelname.'='.$colmodeltable.'.'.$joincolmodelname;
						$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$colmodelpartable.'.'.$parenttable.'id';
					}
				}
			}
			//echo $joinq;
			$curdatasets = $this->db->query('select currency.currencyid from currency '.$joinq.' where '.$parenttable.'id='.$id.'')->result();
			foreach($curdatasets as $data) {
				$cid = $data->currencyid;
			}
		} else {
			$empid = $this->Basefunctions->userid;
			$compquery = $this->db->query('select company.companyid,company.currencyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.'')->result();
			foreach($compquery as $key) {
				$cid = $key->currencyid;
			}
		}
		$curdatasets = $this->db->select('currencyid,currencyname,currencycode,symbol,currencycountry,subunit',false)->from('currency')->where('currencyid',$cid)->get();
		foreach($curdatasets->result() as $datas) {
			$datasets = array('id'=>$datas->currencyid,'name'=>$datas->currencyname,'code'=>$datas->currencycode,'symbol'=>$datas->symbol,'country'=>$datas->currencycountry,'subcode'=>$datas->subunit);
		}
		return $datasets;
	}
	//read file
	public function filecontentfetch($filename) {
		$filecontent = "";
		if( file_exists($filename) && is_readable($filename) ) {
			$newfilename = explode('/',$filename);
			$myfile = @fopen('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1], "r");
			$filecontent = fread($myfile,filesize('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]));
			fclose($myfile);
		}
		return $filecontent;
	}
	//account credit details fetch function 
	public function accountcresitdetailsfetchmodel($apikey,$addontypeid) {
		$this->db->select('addonavailablecredit');
		$this->db->from('salesneuronaddonscredit');
		$this->db->where('salesneuronaddonscredit.addonstypeid',$addontypeid);
		$this->db->where('salesneuronaddonscredit.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$credit = $row->addonavailablecredit;
			}
		} else {
			$credit = '0';
		}
		return $credit;
	}
	//sales neuron status value get
	public function statusvalueget($smsstatus) {
		$this->db->select('smssalesneuronname');
		$this->db->from('smsstatus');
		$this->db->where('smsstatus.smsstatusid',$smsstatus);
		$result = $this->db->get();
		if($result->num_rows() > 0 ) {
			foreach($result->result() as $row) {
				$data = $row->smssalesneuronname;
			}
			return $data;
		} else {
			return $smsstatus;
		}
	}
	//outgoing call dd load
	public function moduleddvalfetchmodel() {
		$i=0;
		$data = array();
		$this->db->select('moduleid,modulename');
		$this->db->from('module');
		$this->db->like('module.moduleprivilegeid','18');
		$this->db->where('module.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0 ) {
			foreach($result->result() as $row) {
				$data[$i] = array('dataname'=>$row->modulename,'datasid'=>$row->moduleid);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//outgoing call record dd fetch
	public function recordvaluefetchmodel() {
		$i=0;
		$moduleid = $_GET['mid'];
		$partab = $this->Basefunctions->generalinformaion('module','modulemastertable','moduleid',$moduleid);
		$fieldid = $partab.'id';
		if($moduleid == '216' || $moduleid == '217' || $moduleid == '225' || $moduleid == '226' || $moduleid == '227'|| $moduleid == '230' ) {
			$fieldname = $partab.'number';
		} else{
			$fieldname = $partab.'name';	
		}
		$this->db->select($fieldid.','.$fieldname);
		$this->db->from($partab);
		$this->db->where($partab.'.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0 ) {
			foreach($result->result() as $row) {
				$data[$i] = array('dataname'=>$row->$fieldname,'datasid'=>$row->$fieldid);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//related module date fetch
	public function relatedmoduledatafetchmodel() {
		$j=0;
		$moduleid = $_GET['moduleid'];
		$nmid = $this->Basefunctions->fetchlinkmoduleidfrommainmodule($moduleid);
		if(count($nmid) != 0) {
			for($i=0;$i < count($nmid);$i++) {
				$mobmodid = $this->Basefunctions->fetchonlymobilenumbermodules($nmid[$i]);
				if(!empty($mobmodid) || $mobmodid != '') {
					$module[$j] = $this->Basefunctions->generalinformaion('module','modulename','moduleid',$mobmodid);
					$mid[$j] = $mobmodid;
					$j++;
				}
			} 
			$j=1;
			for($k=0;$k<count($module);$k++) {
				$data[$k] = array('datasid'=>$j,'dataname'=>$module[$k],'moduleid'=>$mid[$k]);
				$j++;
			}
			echo json_encode($data);
		} else{ 
			$module = array();
			echo json_encode($module);
		} 
	}
    //general notification sent to all user in conversation
	public function generalnotificationtoalluser() {
		$i=0;
		$userid=$this->Basefunctions->logemployeeid;
		$data =array();
		$this->db->select('employeeid,employeename');
		$this->db->from('employee');
		$this->db->where_not_in('employee.employeeid',array($userid));
		$this->db->where('employee.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('eid'=>$row->employeeid,'ename'=>$row->employeename);
				$i++;
			}
		}
		return $data;
	}
	//dashboard previous and next icon
	public function dashboardpreviousnextgetmodel() {
		$moduleid = $_GET['moduleid'];
		$parenttable = explode(',',$_GET['parenttable']);
		$recordid = $_GET['recordid'];
		$action = $_GET['action'];
		//filter unique parent table
		$partablename =  $this->filtervalue($parenttable);
		$partableid = $partablename.'id';
		$this->db->select("$partableid");
		$this->db->from("$partablename");
		$this->db->where("$partablename".'.status',1);
		if($action == 'previous') {
			$this->db->where("$partablename.$partableid >", $recordid);
			$this->db->order_by("$partablename.$partableid", "asc"); 
		} else if($action == 'next') {
			$this->db->where("$partablename.$partableid <", $recordid);
			$this->db->order_by("$partablename.$partableid", "desc"); 
		}
		$this->db->where_in('industryid',array($this->Basefunctions->industryid));
		$this->db->limit(1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$recordid = $row->$partableid;
			}
		} else {
			$recordid = '0';
		}
		echo json_encode($recordid);
	}	
	//filter values
	public function filtervalue($fields) {
		$fieldsinfo = array_unique( $fields );
		$fieldname = implode(',',$fieldsinfo);
		return $fieldname;
	}
	//module informationfetch
	public function moduledatafetch($reltabname) {
		$this->db->select("modulelink,moduleid");
		$this->db->from("module");
		$this->db->like("module.modulemastertable",$reltabname);
		$this->db->where("module.status",1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('mid'=>$row->moduleid,'link'=>$row->modulelink);
			}
		} else{
			$data = '';
		}
		return $data;
	}
	//custom widget edit data field fetch
	public function customeditfieldsdatafetchmodel($fielddata,$parenttable,$id,$moduleid) {
		$spliteddata = explode('|',$fielddata);
		$finaldata=array();
		for($mj=0;$mj < count($spliteddata);$mj++) {
			$datafield = explode('_',$spliteddata[$mj]);
			$ddatafield = explode('#',$datafield[0]);
			$data = $this->customwidgeteditdataretrieve($ddatafield[0]);
			if(count($data) > 0){
				$final[$datafield[1]] = $data;
			} else {
				$final[$datafield[1]] = '';
			}
		}
		return json_encode($final);
	}
	//edit fields data fetch
	public function customwidgeteditdataretrieve($ddatafield) {
		$this->db->select('moduletabid,columnname,tablename,uitypeid,fieldname,fieldlabel,parenttable,mandatory');
		$this->db->from("modulefield");
		$this->db->where("modulefield.modulefieldid",$ddatafield);
		$this->db->where("modulefield.status",1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$moduleid = $row->moduletabid;
				$cloumnname = $row->columnname;
				$tablename = $row->tablename;
				$uitypeid = $row->uitypeid;
				$fieldname = $row->fieldname;
				$fieldlabel = $row->fieldlabel;
				$parenttable = $row->parenttable;
				$mandatory = $row->mandatory;
				$data = $moduleid.','.$cloumnname.','.$tablename.','.$uitypeid.','.$fieldname.','.$fieldlabel.','.$parenttable.','.$mandatory;
			}
		} else{
			$data = '';
		}
		return $data;
	}
	//field name based drop down value get - picklist
	public function customfieldnamebesdpicklistddvalue(){
		$i=0;
		$moduleid = 'moduleid';
		$fieldid = $_GET['fieldid'];
		$mid = $_GET['moduleid'];
		$table = substr($fieldid, 0, -2);
		$fieldname = $table.'name';
		$this->db->select($fieldid.','.$fieldname);
		$this->db->from($table);
		if(trim($_GET['uitype']) == 17){
			$this->db->where("FIND_IN_SET($mid,$table."."$moduleid) >", 0);
		}
		$this->db->where('status',1);
		$this->db->order_by($table.'.'.$fieldname);
		$reult =$this->db->get();
		if($reult->num_rows() > 0){
			foreach($reult->result() as $row){
				$data[$i] = array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//custom field inline edit submit
	public function customeditfieldsubmitmodel() {
		$value = $_GET['value'];
		$parenttable = explode(',',$_GET['parenttable']);
		$data= 'Fail';
		//filter unique parent table
		$partablename =  $this->filtervalue($parenttable);
		$recordid = $_GET['recordid'];
		$tablename = $_GET['tablename'];
		$columnname = $_GET['columnname'];
		$relatedtab = $_GET['relatedtab'];
		$fieldname = $_GET['fieldname'];
		if($partablename == $tablename) {
			$updatearray = array($columnname=>$value);
			$this->db->where($relatedtab.".".$relatedtab.'id',$recordid);
			$this->db->update($relatedtab,$updatearray);
			$data= 'Success';
		} else {
			$addressname =  substr($fieldname, 0, -7);
			if($addressname == 'billing' || $addressname == 'primary') {
				$relatedrecordid = $this->Basefunctions->generalinformaion($partablename,$relatedtab.'id',$partablename.'id',$recordid);
				$updatearray = array($columnname=>$value);
				$this->db->where('addressmethod','4');
				$this->db->where($tablename.".".$relatedtab.'id',$recordid);
				$this->db->update($tablename,$updatearray);
				$data= 'Success';
			} else if($addressname == 'shipping' || $addressname == 'secondary') {
				$relatedrecordid = $this->Basefunctions->generalinformaion($partablename,$relatedtab.'id',$partablename.'id',$recordid);
				$updatearray = array($columnname=>$value);
				$this->db->where('addressmethod','5');
				$this->db->where($tablename.".".$relatedtab.'id',$recordid);
				$this->db->update($tablename,$updatearray);
				$data= 'Success';
			} else {
				$relatedrecordid = $this->Basefunctions->generalinformaion($partablename,$relatedtab.'id',$partablename.'id',$recordid);
				$updatearray = array($columnname=>$value);
				$this->db->where($relatedtab.".".$relatedtab.'id',$recordid);
				$this->db->update($relatedtab,$updatearray);
				$data= 'Success';
			}
			
		}
		echo json_encode($data);
	}
	//notification mute and un mute option
	public function notificationmuteunmuteoptionmodel() {
		$soundtype = $_GET['soundtype'];
		$userid = $this->Basefunctions->logemployeeid;
		$array = array('notificationsoundmute'=>$soundtype);
		$this->db->where_in('datapreference.employeeid',array($userid));
		$this->db->update('datapreference',$array);
		echo json_encode('Success');
	}
	//notification sound type data fetch
	public function notificationsoundtypefetchmodel() {
		$userid = $this->Basefunctions->logemployeeid;
		$data = 'No';
		if($userid!='') {
			$this->db->select('notificationsoundmute');
			$this->db->from('datapreference');
			$this->db->where_in('datapreference.employeeid',array($userid));
			$this->db->limit(1);
			$result = $this->db->get();
			if($result->num_rows() > 0) {
				foreach($result->result() as $row) {
					$data = $row->notificationsoundmute;
				}
			}
		}
		echo json_encode($data);
	}
	//megre content fetch
	public function mergecontentfetch($message,$baseurl,$modulelink,$parenttable,$modulename,$moduleid,$primaryid) {
		$finalcont = $message;
		preg_match_all ("/{.*}/U", $message, $contmatch);
		foreach($contmatch as $bocontent) {
			foreach($bocontent as $bomatch) {
				if($bomatch != "{S.Number}") {
					$expbottom = explode('.',$bomatch);
					$relatedfield = [];
					if($expbottom[1] == 'commonid}' ) {
						$relatedfield = $this->relatedfieldfetch($bomatch,$parenttable,$primaryid,$moduleid);
						$relfiled = explode(',',$relatedfield);
						$bomatch = $relfiled[0];
						$parenttable = $relfiled[1];
						$primaryid = $relfiled[2];
						$moduleid = $relfiled[3];
					}
					$htmldata = $this->mergcontinformationfetchmodel($bomatch,$parenttable,$primaryid,$moduleid);
					$exp = explode(':',$bomatch);
					if($exp[0] == '{REL') {
						$explode = explode('.',$exp[1]);
						$reltabname = $explode[0];
						$relfieldname = explode('}',$explode[1]);
						$moddatafetch = $this->moduledatafetch($reltabname);
						$relrecordid = $this->Basefunctions->generalinformaion($parenttable,$reltabname.'id',$parenttable.'id',$primaryid);
						$field = $this->Basefunctions->generalinformaion($reltabname,$relfieldname[0],$reltabname.'id',$relrecordid);
						$data = "<a class='alertjusthere' data-rowid='".$relrecordid."' data-moduleid='".$moddatafetch['mid']."' data-mastertable='".$parenttable."' href='".$moddatafetch['link']."'>".$field."</a>";
					} else if($exp[0] == '{GEN') {
						$explode = explode('.',$exp[1]);
						$gentabname = $explode[0];
						$genfieldname = explode('}',$explode[1]);
						$moddatafetch = $this->moduledatafetch($gentabname);
						$genrecordid = $this->Basefunctions->generalinformaion($parenttable,'createuserid',$parenttable.'id',$primaryid);
						$field = $this->Basefunctions->generalinformaion($gentabname,$genfieldname[0],$gentabname.'id',$genrecordid);
						$data = "<a class='alertjusthere' data-rowid='".$genrecordid."' data-moduleid='".$moddatafetch['mid']."'href='".$moddatafetch['link']."'>".$field."</a>";
					} else {
						$data = "<a class='alertjusthere' data-rowid='".$primaryid."' data-moduleid='".$moduleid."' data-mastertable='".$parenttable."'  href='".$modulelink."'>".implode(',',$htmldata)."</a>";
					}
					if($expbottom[1] == 'commonid}' ) {
						$bomatch = '{.commonid}';
					}
					$finalcont = preg_replace('~'.$bomatch.'~',$data,$finalcont);
				} else {
					$finalcont = preg_replace('~'.$bomatch.'~','',$finalcont);
				}
			}
		}
		$content = preg_replace('~<p>~','', $finalcont);
		$content = preg_replace('~a10s~',' ', $content);
		$content = preg_replace('~<br>~','', $content);
		$content = preg_replace('~</p>~','', $content);
		return $content;
	}
	//relation module ids
	public function fetchrelatedmodulelist($moduleid) {
		$relmodids = array();
		$relmoduleids = $this->db->select('modulerelationid,relatedmoduleid')->from('modulerelation')->where('moduleid',$moduleid)->get();
		foreach($relmoduleids->result() as $reldata) {
			$relmodids[] = $reldata->relatedmoduleid;
		}
		return $relmodids;
	}
	//merge content data fetch
	public function mergcontinformationfetchmodel($mergegrp,$parenttable,$id,$moduleid) {
		$relmodids = $this->fetchrelatedmodulelist($moduleid);
		$primaryid = $id;
		$database = $this->db->database;
		$resultset = array();
		$mergeindval = $mergegrp;
		$formatortype = "1";
		$id = $primaryid;
		//parent child field concept
		$elname = array('parentcategoryid','parentstoragecategoryid','parentaccountid');
		$fname = array('categoryname','storagecategoryname','accountname');
		$fnameid = array('categoryid','storagecategoryid','accountid');
		$tname = array('category','storagecategory','account');
	
		$empid = $this->Basefunctions->userid;
		//foreach($mergegrp as $mergeindval) {
		$newdata = array();
		$tempval = substr($mergeindval,1,(strlen($mergeindval)-2));
		$newval = explode('.',$tempval);
		$reltabname = explode(':',$newval[0]);
		$table = $reltabname[0];
		$uitypeid = 2;
		//chk editor field name
		$chkeditfname = explode('_',$newval[1]);
		$editorfname = ( (count($chkeditfname) >= 2)? $chkeditfname[1] : '');
		if($table!='REL' && $table!='GEN') { //parent table fields information
			$tablename = trim($reltabname[0]);
			$tabfield = $newval[1];
			//field fetch parent table fetch
			$fieldnamechk = explode('-',$tabfield);
			$fldname = ( (count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
			$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
			$uitypeid = ( (count($fieldinfo) > 0)? $fieldinfo['uitype'] : 1);
			if($fldname != 'attributesetid') {
				//get user company & branch id
				if($tablename == 'company' || $tablename == 'branch') {
					if($tablename == 'company') {
						$compquery = $this->db->query('select company.companyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND company.status=1')->result();
						foreach($compquery as $key) {
							$id = $key->companyid;
						}
					} else if($tablename == 'branch') {
						$brquery = $this->db->query('select branch.branchid from branch join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND branch.status=1')->result();
						foreach($brquery as $key) {
							$id = $key->branchid;
						}
					}
				}
				//check its parent child field concept
				if(in_array($fldname,$elname)) {
					$key = array_search($fldname,$elname);
					$pid = $id;
					if($key != NULL) {
						//parent id get
						$result = $this->db->select("$tname[$key].$elname[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$id)->where($tname[$key].'.status',1)->get();
						if($result->num_rows() > 0) {
							foreach($result->result()as $row) {
								$pid=$row->$elname[$key];
							}
							if($pid!=0) {
								//parent name
								$result = $this->db->select("$tname[$key].$fname[$key],$tname[$key].$fnameid[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$pid)->where($tname[$key].'.status',1)->get();
								if($result->num_rows() > 0) {
									foreach($result->result()as $row) {
										$resultset[]=$row->$fname[$key];
									}
								}
							}
						}
					}
				}
				else {
					//field fetch parent table fetch
					$fieldnamechk = explode('-',$tabfield);
					$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
					$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
					if(count($fieldinfo)>0) {
						$uitypeid = $fieldinfo['uitype'];
						//fetch parent table join id
						$relpartable = $fieldinfo['partab'];
						$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
						$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
						$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
						//fetch original field of picklist data in view creation
						$viewfieldinfo = $this->viewfieldsinformation($tablename,$tabfield,$fieldinfo['modid']);
						$tabfieldname = ( (count($viewfieldinfo)>0)? $viewfieldinfo['joinfieldname'] : $tabfield );
						$jointabfieldid = ( (count($viewfieldinfo)>0)? $viewfieldinfo['condfiledname'] : $tabfield );
						//join relation table and main table
						$joinq="";
						if($relpartable!=$tablename) {
							$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id AND '.$relpartable.'.status=1';
						}
						//main table join
						if($mainjointable!=$parenttable) {
							$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id AND '.$relpartable.'.status=1';
						}
						//if its picklist data
						$newfield = substr( $tabfieldname,( strlen($tabfieldname)-2 ));
						$tabname = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
						$tbachk = $this->tableexitcheck($database,$tabname);
						if( $newfield == "id" && $tbachk != 0 ) {
							$newtable = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
							$newjfield = $newtable."name";
							$whtable = (($tablename=="company" || $tablename=="branch")? $tablename : $parenttable);
							$newjdata = $this->db->query("SELECT ".$newtable.".".$newtable."id AS columdataid,".$newtable.".".$newjfield." AS columdata FROM ".$newtable." LEFT OUTER JOIN ".$tablename." ON ".$tablename.".".$tabfieldname." = ".$newtable.".".$tabfieldname."".$joinq." WHERE ".$tablename.".".$whtable."id = ".$id." AND ".$newtable.".status=1 AND ".$tablename.".status=1",false);
							if($newjdata->num_rows() >0) {
								foreach($newjdata->result() as $jkey) {
									$resultset[] = $jkey->columdata;
								}
							} else {
								$resultset[] = " ";
							}
						}
						else {
							//main table data sets information fetching
							$fldcountchk = explode('-',$tabfield);
							$tablcolumncheck = $this->tablefieldnamecheck($tablename,$parenttable."id");
							if( ( $parenttable == $tablename && (count($fldcountchk)) < 2 ) || ( $tablcolumncheck == 'false' && (count($fldcountchk)) < 2 ) ) {
								$newdata = $this->db->select("".$tabfield." AS columdata")->from($tablename)->where($tablename.".".$tablename."id",$id)->where($tablename.".status",1)->get()->result();
							}
							else {
								$cond = "";
								$addfield = explode('-',$tabfield);
								if( (count($addfield)) >= 2 ) {
									$datafiledname=$addfield[1];
									if(in_array('PR',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=2';
									} else if(in_array('SE',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=3';
									} else if(in_array('BI',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=4';
									} else if(in_array('SH',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=5';
									} else if(in_array('CW',$addfield)) {
										$formatortype = "CW";
									} else if(in_array('CF',$addfield)) {
										$formatortype = "CF";
									} else if(in_array('CS',$addfield)) {
										$formatortype = "CS";
									}
									if($tablename!=$parenttable) {
										$tablcolumncheck = $this->tablefieldnamecheck($tablename,"addresstypeid");
										$cond = ( ($tablcolumncheck == 'true')? $cond : ' ');
										$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename." JOIN ".$parenttable." ON ".$parenttable.".".$parenttable."id = ".$tablename.".".$parenttable."id WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1".$cond."")->result();
									} else {
										$newdata = $this->db->select(''.$datafiledname.' AS columdata',false)->from($tablename)->where($tablename.'id',$id)->where($tablename.'.status',1)->get()->result();
									}
								}
								else {
									$newdata = $this->db->query("SELECT ".$tabfield." AS columdata FROM ".$tablename." JOIN ".$parenttable." ON ".$parenttable.".".$parenttable."id = ".$tablename.".".$parenttable."id WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1")->result();
								}
							}
							foreach($newdata as $key) {
								if($tabfield == 'companylogo') {
									$imgname = $key->columdata;
									if(filter_var($imgname,FILTER_VALIDATE_URL)) {
										$resultset[] = '<img src="'.$imgname.'" />';
									} else {
										if( file_exists($imgname) ) {
											$resultset[] = '<img src="'.$imgname.'" style="width:11em; height:3em" />';
										} else {
											$resultset[] = " ";
										}
									}
								}
								else {
									$currencyinfo = $this->currencyinformation($parenttable,$id,$moduleid);
									if($formatortype=="CW") { //convert to word
										if(count($currencyinfo)>0) {
											$resultset[] = $this->convert->numtowordconvert($key->columdata,$currencyinfo['name'],$currencyinfo['subcode']);
										} else {
											$resultset[] = $this->convert->numtowordconvert($key->columdata);
										}
									} else if($formatortype=="CF") { //currenct format
										if(count($currencyinfo)>0) {
											$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
										} else {
											$resultset[] = $this->convert->formatcurrency($key->columdata);
										}
									} else if($formatortype=="CS") { //currency with symbol
										if(count($currencyinfo)>0) {
											$amt = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
											$resultset[] = $currencyinfo['symbol'].' '.$amt;
										} else {
											$resultset[] = $currencyinfo['symbol'].' '.$this->convert->formatcurrency($key->columdata);
										}
									} else {
										if(is_numeric($key->columdata)) {
											if($uitypeid == '4' || $uitypeid == '5' || $uitypeid == '7') {
												if(count($currencyinfo)>0) {
													$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
												} else {
													$resultset[] = $this->convert->formatcurrency($key->columdata);
												}
											} else {
												$resultset[] = $key->columdata;
											}
										} else {
											if($editorfname=='editorfilename') {
												if( file_exists($key->columdata) ) {
													$datas = $this->geteditorfilecontent($key->columdata);
													$datas = preg_replace('~a10s~','&nbsp;', $datas);
													$datas = preg_replace('~<br>~','<br />', $datas);
													$resultset[] = $datas;
												} else {
													$resultset[] = '';
												}
											} else {
												$resultset[] = $key->columdata;
											}
										}
									}
								}
							}
						}
					}
				}
			}
			else {
				//attribute value fetch for non relational modules
				$fieldnamechk = explode('-',$tabfield);
				$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
				$fieldinfo = $this->fetchfieldparenttable($table,$fldname);
				if(count($fieldinfo)>0) {
					$uitypeid = $fieldinfo['uitype'];
					//fetch parent table join id
					$relpartable = $fieldinfo['partab'];
					$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
					$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
					$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
					//join relation table and main table
					$joinq="";
					if($parenttable!=$table) {
						$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$table.'.'.$parenttable.'id';
					}
					//fetch attribute name and values
					$i=0;
					$datasets = array();
					$attrsetids = $this->db->query(" SELECT ".$table.".".$table."id as Id,".$table.".attributesetid as attrsetid,".$table.".attributes as attributeid,".$table.".attributevalues as attributevalueid FROM ".$table.$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$mainjointable.".status=1 AND ".$table.".status=1",false)->result();
					foreach($attrsetids as $attrset) {
						$datasets[$i] = array('id'=>$attrset->Id,'attrsetid'=>$attrset->attrsetid,'attrnames'=>$attrset->attributeid,'attrvals'=>$attrset->attributevalueid);
						$i++;
					}
					$resultset = array();
					foreach($datasets as $set) {
						$valid = explode('_',$set['attrvals']);
						$nameid = explode('_',$set['attrnames']);
						$n=1;
						$vi = 0;
						$attrdatasets = array();
						for($h=0;$h<count($valid);$h++) {
							$attrval = '';
							$attrname = '';
							if($valid[$h]!='') {
								//attribute value fetch
								$attrvaldata = $this->db->query(' SELECT attributevalueid,attributevaluename FROM attributevalue WHERE attributevalueid='.$valid[$h].' AND status=1',false)->result();
								foreach($attrvaldata as $valsets) {
									$attrval = $valsets->attributevaluename;
								}
								//attribute name fetch
								$attrnamedata = $this->db->query(' SELECT attributeid,attributename FROM attribute WHERE attributeid='.$nameid[$h].' AND status=1',false)->result();
								foreach($attrnamedata as $namesets) {
									$attrname = $namesets->attributename;
								}
							}
							$attrset = "";
							if( $attrval!='' ) {
								$attrset = $n.'.'.$attrname.': '.$attrval;
								++$n;
							}
							$attrdatasets[$vi] = $attrset;
							$vi++;
						}
						$resultset[] = implode('<br />',$attrdatasets);
					}
				}
			}
		}
		else if($table=='REL') { //related module fields information fetch
			$tablename = trim($reltabname[1]);
			$tabfield = trim($newval[1]);
			$fieldnamechk = explode('-',$tabfield);
			$fldname = ( (count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
			if($fldname != 'attributesetid') {
				//get user company & branch id
				if($tablename == 'company' || $tablename == 'branch') {
					if($tablename == 'company') {
						$compquery = $this->db->query('select company.companyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND company.status=1')->result();
						foreach($compquery as $key) {
							$id = $key->companyid;
						}
					} else if($tablename == 'branch') {
						$brquery = $this->db->query('select branch.branchid from branch join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND branch.status=1')->result();
						foreach($brquery as $key) {
							$id = $key->branchid;
						}
					}
				}
				//check its parent child field concept
				$fldnamechk = explode('-',$tabfield);
				$fldname = ( (count($fldnamechk) >= 2)? $fldnamechk[1] : $tabfield);
				if(in_array($fldname,$elname)) {
					$key = array_search($fldname,$elname);
					$pid = $id;
					if($key != NULL) {
						//parent id get
						$result = $this->db->select("$tname[$key].$elname[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$id)->where($tname[$key].'.status',1)->get();
						if($result->num_rows() > 0) {
							foreach($result->result()as $row) {
								$pid=$row->$elname[$key];
							}
							if($pid!=0) {
								//parent name
								$result = $this->db->select("$tname[$key].$fname[$key],$tname[$key].$fnameid[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$pid)->where($tname[$key].'.status',1)->get();
								if($result->num_rows() > 0) {
									foreach($result->result()as $row) {
										$resultset[]=$row->$fname[$key];
									}
								}
							}
						}
					}
				}
				else {
					//field fetch parent table fetch
					$fieldnamechk = explode('-',$tabfield);
					$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
					$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
					if(count($fieldinfo)>0) {
						$uitypeid = $fieldinfo['uitype'];
						//fetch parent table join id
						$relpartable = $fieldinfo['partab'];
						$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
						$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
						$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
						//fetch original field of picklist data in view creation
						$viewfieldinfo = $this->viewfieldsinformation($tablename,$tabfield,$fieldinfo['modid']);
						$tabfieldname = ( (count($viewfieldinfo)>0)? $viewfieldinfo['joinfieldname'] : $tabfield );
						$jointabfieldid = ( (count($viewfieldinfo)>0)? $viewfieldinfo['condfiledname'] : $tabfield );
						//join relation table and main table
						$joinq="";
						if($relpartable!=$tablename) {
							$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id AND '.$relpartable.'.status=1';
						}
						//main table join
						if($mainjointable==$parenttable) {
							$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id AND '.$mainjointable.'.status=1';
						} else {
							$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id AND '.$mainjointable.'.status=1';
							$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$mainjointable.'.'.$parenttable.'id AND '.$parenttable.'.status=1';
						}
						//if its picklist data
						$newfield = substr( $tabfieldname,( strlen($tabfieldname)-2 ));
						$tabname = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
						$tblchk = $this->tableexitcheck($database,$tabname);
						if( $newfield == "id" && $tblchk != 0 ) {
							$newtable = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
							$newjfield = $newtable."name";
							$whtable = (($tablename=="company" || $tablename=="branch")? $tablename : $parenttable);
							$newjdata = $this->db->query("SELECT ".$newtable.".".$newtable."id AS columdataid,".$newtable.".".$newjfield." AS columdata FROM ".$newtable." LEFT OUTER JOIN ".$tablename." ON ".$tablename.".".$jointabfieldid." = ".$newtable.".".$tabfieldname."".$joinq." WHERE ".$parenttable.".".$whtable."id = ".$id." AND ".$mainjointable.".status=1 AND ".$newtable.".status=1 AND ".$tablename.".status=1");
							if($newjdata->num_rows() >0) {
								foreach($newjdata->result() as $jkey) {
									$resultset[] = $jkey->columdata;
								}
							} else {
								$resultset[] = " ";
							}
						}
						else {
							//main table data sets information fetching
							$fldcountchk = explode('-',$tabfield);
							$tablcolumncheck = $this->tablefieldnamecheck($tablename,$parenttable."id");
							if( ( $parenttable == $tablename && (count($fldcountchk)) < 2 ) || ( $tablcolumncheck == 'false' && (count($fldcountchk)) < 2 ) ) {
								$newdata = $this->db->query("SELECT ".$tablename.".".$tabfield." AS columdata FROM ".$tablename."".$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1")->result();
							}
							else {
								$cond = "";
								$addfield = explode('-',$tabfield);
								if( (count($addfield)) >= 2 ) {
									$datafiledname=$addfield[1];
									if(in_array('PR',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=2';
									} else if(in_array('SE',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=3';
									} else if(in_array('BI',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=4';
									} else if(in_array('SH',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=5';
									} else if(in_array('CW',$addfield)) {
										$formatortype = "CW";
									} else if(in_array('CF',$addfield)) {
										$formatortype = "CF";
									} else if(in_array('CS',$addfield)) {
										$formatortype = "CS";
									}
									$tablcolumncheck = $this->tablefieldnamecheck($tablename,"addresstypeid");
									$cond = ( ($tablcolumncheck == 'true')? $cond : ' ');
									if($tablename!=$parenttable) {
										$newdata = $this->db->query("SELECT ".$tablename.".".$datafiledname." AS columdata FROM ".$tablename."".$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1".$cond."")->result();
									} else {
										$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename." WHERE ".$tablename."id = ".$id." AND ".$tablename.".status=1")->result();
									}
								}
								else {
									$newdata = $this->db->query("SELECT ".$tablename.".".$tabfield." AS columdata FROM ".$tablename."".$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1".$cond."")->result();
								}
							}
							foreach($newdata as $key) {
								if($tabfield == 'companylogo') {
									$imgname = $key->columdata;
									if(filter_var($imgname, FILTER_VALIDATE_URL)) {
										$resultset[] = '<img src="'.$imgname.'" />';
									} else {
										if( file_exists($imgname) ) {
											$resultset[] = '<img src="'.$imgname.'" style="width:11em; height:3em" />';
										} else {
											$resultset[] = " ";
										}
									}
								}
								else {
									$currencyinfo = $this->currencyinformation($parenttable,$id,$moduleid);
									if($formatortype=="CW") { //convert to word
										if(count($currencyinfo)>0) {
											$resultset[] = $this->convert->numtowordconvert($key->columdata,$currencyinfo['name'],$currencyinfo['subcode']);
										} else {
											$resultset[] = $this->convert->numtowordconvert($key->columdata);
										}
									} else if($formatortype=="CF") { //currenct format
										if(count($currencyinfo)>0) {
											$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
										} else {
											$resultset[] = $this->convert->formatcurrency($key->columdata);
										}
									} else if($formatortype=="CS") { //currency with symbol
										if(count($currencyinfo)>0) {
											$amt = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
											$resultset[] = $currencyinfo['symbol'].' '.$amt;
										} else {
											$resultset[] = $currencyinfo['symbol'].' '.$this->convert->formatcurrency($key->columdata);
										}
									} else {
										if(is_numeric($key->columdata)) {
											if( $uitypeid == '4' || $uitypeid == '5' || $uitypeid == '7' ) {
												if(count($currencyinfo)>0) {
													$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
												} else {
													$resultset[] = $this->convert->formatcurrency($key->columdata);
												}
											} else {
												$resultset[] = $key->columdata;
											}
										} else {
											if($editorfname=='editorfilename') {
												if( file_exists($key->columdata) ) {
													$datas = $this->geteditorfilecontent($key->columdata);
													$datas = preg_replace('~a10s~','&nbsp;', $datas);
													$datas = preg_replace('~<br>~','<br />', $datas);
													$resultset[] = $datas;
												} else {
													$resultset[] = '';
												}
											} else {
												$resultset[] = $key->columdata;
											}
										}
									}
								}
							}
						}
					}
				}
			}
			else {
				//attribute value fetch
				$fieldnamechk = explode('-',$tabfield);
				$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
				$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
				if(count($fieldinfo)>0) {
					$uitypeid = $fieldinfo['uitype'];
					//fetch parent table join id
					$relpartable = $fieldinfo['partab'];
					$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
					$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
					$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
					//join relation table and main table
					$joinq="";
					if($relpartable!=$tablename) {
						$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id';
					}
					//main table join
					if($mainjointable==$parenttable) {
						$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id';
					} else {
						$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id';
						$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$mainjointable.'.'.$parenttable.'id';
					}
					//fetch attribute name and values
					$i=0;
					$datasets = array();
					$attrsetids = $this->db->query(" SELECT ".$tablename.".".$tablename."id as Id,".$tablename.".attributesetid as attrsetid,".$tablename.".attributes as attributeid,".$tablename.".attributevalues as attributevalueid FROM ".$tablename.$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$mainjointable.".status=1 AND ".$tablename.".status=1",false)->result();
					foreach($attrsetids as $attrset) {
						$datasets[$i] = array('id'=>$attrset->Id,'attrsetid'=>$attrset->attrsetid,'attrnames'=>$attrset->attributeid,'attrvals'=>$attrset->attributevalueid);
						$i++;
					}
					$resultset = array();
					foreach($datasets as $set) {
						$valid = explode('_',$set['attrvals']);
						$nameid = explode('_',$set['attrnames']);
						$n=1;
						$vi = 0;
						$attrdatasets = array();
						for($h=0;$h<count($valid);$h++) {
							$attrval = '';
							$attrname = '';
							if($valid[$h]!='') {
								//attribute value fetch
								$attrvaldata = $this->db->query(' SELECT attributevalueid,attributevaluename FROM attributevalue WHERE attributevalueid='.$valid[$h].' AND status=1',false)->result();
								foreach($attrvaldata as $valsets) {
									$attrval = $valsets->attributevaluename;
								}
								//attribute name fetch
								$attrnamedata = $this->db->query(' SELECT attributeid,attributename FROM attribute WHERE attributeid='.$nameid[$h].' AND status=1',false)->result();
								foreach($attrnamedata as $namesets) {
									$attrname = $namesets->attributename;
								}
							}
							$attrset = "";
							if( $attrval!='' ) {
								$attrset = $n.'.'.$attrname.': '.$attrval;
								++$n;
							}
							$attrdatasets[$vi] = $attrset;
							$vi++;
						}
						$resultset[] = implode('<br />',$attrdatasets);
					}
				}
			}
		}
		else if($table=='GEN') {
			$tablename = trim($reltabname[1]);
			$tabfield = $newval[1];
			//field fetch parent table fetch
			$fieldnamechk = explode('-',$tabfield);
			$fldname = ( (count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
			$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
			$uitypeid = ( (count($fieldinfo) > 0)? $fieldinfo['uitype'] : 1);
			if($fldname != 'attributesetid') {
				//get user company & branch id
				$tabinfo = array('company','companycf','branch','branchcf');
				if( in_array($tablename,$tabinfo) ) {
					if($tablename == 'company' || $tablename == 'companycf') {
						$compquery = $this->db->query('select company.companyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND company.status=1')->result();
						foreach($compquery as $key) {
							$id = $key->companyid;
						}
					} else if($tablename == 'branch' || $tablename == 'branchcf') {
						$brquery = $this->db->query('select branch.branchid from branch join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND branch.status=1')->result();
						foreach($brquery as $key) {
							$id = $key->branchid;
						}
					} else { //employee
						$id = $this->Basefunctions->userid;
					}
				} else { //employee
					$id = $this->Basefunctions->userid;
				}
				//check its parent child field concept
				if(in_array($fldname,$elname)) {
					$key = array_search($fldname,$elname);
					$pid = $id;
					if($key != NULL) {
						//parent id get
						$result = $this->db->select("$tname[$key].$elname[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$id)->where($tname[$key].'.status',1)->get();
						if($result->num_rows() > 0) {
							foreach($result->result()as $row) {
								$pid=$row->$elname[$key];
							}
							if($pid!=0) {
								//parent name
								$result = $this->db->select("$tname[$key].$fname[$key],$tname[$key].$fnameid[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$pid)->where($tname[$key].'.status',1)->get();
								if($result->num_rows() > 0) {
									foreach($result->result()as $row) {
										$resultset[]=$row->$fname[$key];
									}
								}
							}
						}
					}
				}
				else {
					//field fetch parent table fetch
					$fieldnamechk = explode('-',$tabfield);
					$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
					$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
					$moduleid = $fieldinfo['modid'];
					if(count($fieldinfo)>0) {
						$uitypeid = $fieldinfo['uitype'];
						//fetch parent table join id
						$relpartable = $fieldinfo['partab'];
						$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
						$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $relpartable );
						$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
						//fetch original field of picklist data in view creation
						$viewfieldinfo = $this->viewfieldsinformation($tablename,$tabfield,$fieldinfo['modid']);
						$tabfieldname = ( (count($viewfieldinfo)>0)? $viewfieldinfo['joinfieldname'] : $tabfield );
						$jointabfieldid = ( (count($viewfieldinfo)>0)? $viewfieldinfo['condfiledname'] : $tabfield );
						//join relation table and main table
						$joinq="";
						if($relpartable!=$tablename) {
							$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id';
						}
						//if its picklist data
						$newfield = substr( $tabfieldname,( strlen($tabfieldname)-2 ));
						$tabname = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
						$tbachk = $this->tableexitcheck($database,$tabname);
						if( $newfield == "id" && $tbachk != 0 ) {
							$newtable = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
							$newjfield = $newtable."name";
							$whtable = (($tablename=="company" || $tablename=="branch")? $tablename : $relpartable);
							$newjdata = $this->db->query("SELECT ".$newtable.".".$newtable."id AS columdataid,".$newtable.".".$newjfield." AS columdata FROM ".$newtable." LEFT OUTER JOIN ".$tablename." ON ".$tablename.".".$tabfieldname." = ".$newtable.".".$tabfieldname."".$joinq." WHERE ".$tablename.".".$whtable."id = ".$id." AND ".$newtable.".status=1 AND ".$tablename.".status=1",false);
							if($newjdata->num_rows() >0) {
								foreach($newjdata->result() as $jkey) {
									$resultset[] = $jkey->columdata;
								}
							} else {
								$resultset[] = " ";
							}
						}
						else {
							//main table data sets information fetching
							$fldcountchk = explode('-',$tabfield);
							$tablcolumncheck = $this->tablefieldnamecheck($tablename,$relpartable."id");
							if( ( $relpartable == $tablename && (count($fldcountchk)) < 2 ) || ( $tablcolumncheck == 'false' && (count($fldcountchk)) < 2 ) ) {
								$newdata = $this->db->select("".$tabfield." AS columdata")->from($tablename)->where($tablename.".".$tablename."id",$id)->where($tablename.".status",1)->get()->result();
							}
							else {
								$cond = "";
								$addfield = explode('-',$tabfield);
								if( (count($addfield)) >= 2 ) {
									$datafiledname=$addfield[1];
									if(in_array('PR',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=2';
									} else if(in_array('SE',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=3';
									} else if(in_array('BI',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=4';
									} else if(in_array('SH',$addfield)) {
										$cond = ' AND '.$tablename.'.addresstypeid=5';
									} else if(in_array('CW',$addfield)) {
										$formatortype = "CW";
									} else if(in_array('CF',$addfield)) {
										$formatortype = "CF";
									} else if(in_array('CS',$addfield)) {
										$formatortype = "CS";
									}
									if($tablename!=$relpartable) {
										$tablcolumncheck = $this->tablefieldnamecheck($tablename,"addresstypeid");
										$cond = ( ($tablcolumncheck == 'true')? $cond : ' ');
										$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename." JOIN ".$relpartable." ON ".$relpartable.".".$relpartable."id = ".$tablename.".".$relpartable."id WHERE ".$relpartable.".".$relpartable."id = ".$id." AND ".$tablename.".status=1 AND ".$relpartable.".status=1".$cond."")->result();
									} else {
										$newdata = $this->db->select(''.$datafiledname.' AS columdata',false)->from($tablename)->where($tablename.'id',$id)->where($tablename.'.status',1)->get()->result();
									}
								}
								else {
									$newdata = $this->db->query("SELECT ".$tabfield." AS columdata FROM ".$tablename." JOIN ".$relpartable." ON ".$relpartable.".".$relpartable."id = ".$tablename.".".$relpartable."id WHERE ".$relpartable.".".$relpartable."id = ".$id." AND ".$tablename.".status=1 AND ".$relpartable.".status=1")->result();
								}
							}
							foreach($newdata as $key) {
								if($tabfield == 'companylogo') {
									$imgname = $key->columdata;
									if(filter_var($imgname,FILTER_VALIDATE_URL)) {
										$resultset[] = '<img src="'.$imgname.'" />';
									} else {
										if( file_exists($imgname) ) {
											$resultset[] = '<img src="'.$imgname.'" style="width:11em; height:3em" />';
										} else {
											$resultset[] = " ";
										}
									}
								}
								else {
									if($formatortype=="CW") { //convert to word
										$currencyinfo = $this->currencyinformation($tablename,$id,$moduleid);
										if(count($currencyinfo)>0) {
											$resultset[] = $this->convert->numtowordconvert($key->columdata,$currencyinfo['name'],$currencyinfo['subcode']);
										} else {
											$resultset[] = $this->convert->numtowordconvert($key->columdata);
										}
									} else if($formatortype=="CF") { //currenct format
										$currencyinfo = $this->currencyinformation($tablename,$id,$moduleid);
										if(count($currencyinfo)>0) {
											$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
										} else {
											$resultset[] = $this->convert->formatcurrency($key->columdata);
										}
									} else if($formatortype=="CS") { //currency with symbol
										$currencyinfo = $this->currencyinformation($tablename,$id,$moduleid);
										if(count($currencyinfo)>0) {
											$amt = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
											$resultset[] = $currencyinfo['symbol'].' '.$amt;
										} else {
											$resultset[] = $currencyinfo['symbol'].' '.$this->convert->formatcurrency($key->columdata);
										}
									} else {
										$currencyinfo = $this->currencyinformation($tablename,$id,$moduleid);
										if(is_numeric($key->columdata)) {
											if($uitypeid == '4' || $uitypeid == '5' || $uitypeid == '7') {
												if(count($currencyinfo)>0) {
													$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
												} else {
													$resultset[] = $this->convert->formatcurrency($key->columdata);
												}
											} else {
												$resultset[] = $key->columdata;
											}
										} else {
											if($editorfname=='editorfilename') {
												if( file_exists($key->columdata) ) {
													$datas = $this->geteditorfilecontent($key->columdata);
													$datas = preg_replace('~a10s~','&nbsp;', $datas);
													$datas = preg_replace('~<br>~','<br />', $datas);
													$resultset[] = $datas;
												} else {
													$resultset[] = '';
												}
											} else {
												$resultset[] = $key->columdata;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		//}
		return $resultset;
	}
	public function relatedfieldfetch($mergegrp,$parenttable,$id,$moduleid) {
		$this->db->select('commonid,moduleid');
		$this->db->from($parenttable);
		$this->db->where($parenttable.'.'.$parenttable.'id',$id);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$relatedmoduleid = $row->moduleid;
				$commonid = $row->commonid;
			}
		}
		$relatedtable = $this->Basefunctions->generalinformaion('module','modulemastertable','moduleid',$relatedmoduleid);
		if($relatedmoduleid != 216 || $relatedmoduleid != 217 || $relatedmoduleid != 225 || $relatedmoduleid != 226 || $relatedmoduleid != 249 || $relatedmoduleid != 227){
			$value = '{'.$relatedtable.'.'.$relatedtable.'name}';
		} else {
			$value = '{'.$relatedtable.'.'.$relatedtable.'number}';
		}
		$relatedfiled = $value.','.$relatedtable.','.$commonid.','.$relatedmoduleid;
		return $relatedfiled;
	}
	//checkbox based status change
	public function changestatusbasedoncheckboxmodel() {
		$recordid = $_POST['rowid'];
		$checkboxvalue = $_POST['checkboxstatus'];
		$tablename = $_POST['tablename'];
		$primaryid = $tablename.'id';
		if($checkboxvalue== 'Yes'){
			$statusid = '67';
		} else {
			$statusid = '64';
		}
		$defdataarr = $this->updatedefaultvalueget();
		$updatearray = array('crmstatusid'=>$statusid);
		$updatedata = array_merge($updatearray,$defdataarr);
		$wherearray=array($primaryid=>$recordid);
		$this->db->where($wherearray);
		$this->db->update($tablename,$updatedata);
		echo json_encode('Success');
	}
	//quick create model
	public function quickcreatefunctionmodel() {
		$name = $_POST['name'];
		$date = $_POST['date'];
		$priority = $_POST['priority'];
		if($priority == ''){
			$priority = 1;
		}
		$tablename = $_POST['tablename'];
		$ymddate = $this->Basefunctions->ymddateconversion($date);
		$userid = $this->Basefunctions->userid;
		$industryid = $this->Basefunctions->industryid;
		$taskstatus = '64';
		$fieldname = $tablename.'name'; 
		$taskdata = array(
				$fieldname=>$name,
				'taskstartdate'=>$ymddate,
				'employeeid'=>$userid,
				'crmstatusid'=>$taskstatus,
				'priorityid'=>$priority,
				'industryid'=>$industryid
			);
		$defdataarr = $this->defaultvalueget();
		$insertdata = array_merge($taskdata,$defdataarr);
		$this->db->insert($tablename,$insertdata);
		echo json_encode('Success');
	}
	//quick update model
	public function quickupdatefunctionmodel() {
		$name = $_POST['name'];
		$date = $_POST['date'];
		$priority = $_POST['priority'];
		if($priority == ''){
			$priority = 1;
		}
		if(isset($_POST['status'])){
			$status = $_POST['status'];
		} else {
			$status = 1;
		}
		if(isset($_POST['assignto'])) {
			$assignto = $_POST['assignto'];
			$assin = explode(':',$assignto);
		} else {
			$assin[0] = 1;
			$assin[1] = 1;
		}
		if(isset($_POST['leadcontacttypeid'])) {
			$leadcontacttypeid = $_POST['leadcontacttypeid'];
		} else {
			$leadcontacttypeid = 1;
		}
		if(isset($_POST['taskcomminid'])) {
			$taskcomminid = $_POST['taskcomminid'];
			$contactid = 1;
		} else {
			$taskcomminid = 1;
			$leadid = 1;
		}
		if($leadcontacttypeid == 2) {
			$contactid = $taskcomminid;
		} else if($leadcontacttypeid == 3) {
			$leadid = $taskcomminid;
		} else{
			$leadid = 1;
			$contactid = 1;
		}
		$recordid = $_POST['recordid'];
		$tablename = $_POST['tablename'];
		$ymddate = $this->Basefunctions->ymddateconversion($date);
		$userid = $this->Basefunctions->userid;
		$industryid = $this->Basefunctions->industryid;
		$fieldname = $tablename.'name';
		$taskdata = array(
				$fieldname=>$name,
				'taskstartdate'=>$ymddate,
				'employeeid'=>$assin[1],
				'employeetypeid'=>$assin[0],
				'crmstatusid'=>$status,
				'priorityid'=>$priority,
				'industryid'=>$industryid,
				'leadcontacttypeid'=>$leadcontacttypeid,
				'contacttaskid'=>$contactid,
				'leadtaskid'=>$leadid,
				'contacttaskid'=>$taskcomminid,
		);
		$defdataarr = $this->updatedefaultvalueget();
		$updaedata = array_merge($taskdata,$defdataarr);
		$this->db->where($tablename.'.'.$tablename.'id',$recordid);
		$this->db->update($tablename,$updaedata);
		echo json_encode('Success');
	}
	//retrieve lead/contact's
	public function getdropdownoptions($table) {
		$i=0;
		$id=$table.'id';
		$name=$table.'name';
		$this->db->select(''.$id.' AS Id'.','.''.$name.' AS Name,salutation.salutationname AS SName,lastname AS LName');
		$this->db->from($table);
		$this->db->join('salutation','salutation.salutationid='.$table.'.salutationid','left outer');
		$this->db->where($table.'.industryid',$this->Basefunctions->industryid);
		$this->db->where_in(''.$table.'.status',array(1,8));
		$data=$this->db->get();
		if($data->num_rows() > 0) {
			foreach($data->result() as $info) {
				$sname = (($info->SName!='')? $info->SName.' ':'');
				$lname = (($info->LName!='')? ' '.$info->LName:'');
				$datam[$i] = array('id'=>$info->Id,'name'=>$sname.$info->Name.$lname);
				$i++;
			}
			echo json_encode($datam);
		}
		else{
			echo json_encode(array('fail'=>'FAILED'));
		}
	}
	//fetch task data
	public function fetchtaskdatamodel() {
		$id = $_GET['primaryid'];
		$this->db->select('crmtaskname,employeeid,employeetypeid,taskstartdate,priorityid,crmstatusid,leadtaskid,contacttaskid,leadcontacttypeid');
		$this->db->from('crmtask');
		$this->db->where('crmtask.status',1);
		$this->db->where('crmtask.crmtaskid',$id);
		$this->db->where('crmtask.industryid',$this->Basefunctions->industryid);
		$data = $this->db->get();
		if($data->num_rows() > 0) {
			foreach($data->result() as $info) {
				$array = array('taskname'=>$info->crmtaskname,'employeeid'=>$info->employeeid,'employeetypeid'=>$info->employeetypeid,'taskstartdate'=>$info->taskstartdate,'priorityid'=>$info->priorityid,'crmstatusid'=>$info->crmstatusid,'leadtaskid'=>$info->leadtaskid,'contacttaskid'=>$info->contacttaskid,'leadcontacttypeid'=>$info->leadcontacttypeid);
			}
			echo json_encode($array);
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}
	}
}