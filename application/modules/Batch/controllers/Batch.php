<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Batch extends MX_Controller{
    private $batchmoduleid= 264;
	public function __construct() {
        parent::__construct();
       	$this->load->helper('formbuild');
		$this->load->model('Batch/Batchmodel');	
    }
    //first basic hitting view
    public function index() {        
    	$moduleid = array($this->batchmoduleid);
    	sessionchecker($moduleid);
		//action
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Batch/batchview',$data);
	}
	//create batch
	public function newdatacreate() {  
    	$this->Batchmodel->newdatacreatemodel();
    }
	//information fetchbatch
	public function fetchformdataeditdetails() {
		$this->Batchmodel->informationfetchmodel($this->batchmoduleid);
	}
	//update batch
    public function datainformationupdate() {
        $this->Batchmodel->datainformationupdatemodel();
    }
	//delete batch
    public function deleteinformationdata() {
        $this->Batchmodel->deleteoldinformation($this->batchmoduleid);
    } 
}