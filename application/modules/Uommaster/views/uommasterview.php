<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
	<style>
		#validfrom,#validto {
		background-image: url("../img/dateicon.png");
		background-repeat: no-repeat; 
		background-position: right center;
		padding-right: 17px;
		cursor:pointer;
	}
	</style>
</head>
<body>
<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<?php
				$modid = '10,261';
				$dataset['gridtitle'] = $gridtitle['title'];
				$dataset['titleicon'] = $gridtitle['titleicon'];
				$dataset['formtype'] = 'multipleaddform';
				$dataset['multimoduleid'] = $modid;
				$this->load->view('Base/mainviewaddformheader',$dataset);
			?>
			<div class="large-12 columns paddingzero scrollbarclass">
				<div class="mastermodules">
				<?php
					
					//function call for dash board form fields generation
					dashboardformfieldsgeneratorwithgrid($moddbfrmfieldsgen,1,$modid);
				?>
				</div>
				<?php
					$device = $this->Basefunctions->deviceinfo();
					if($device=='phone') {
						$this->load->view('Base/overlaymobile');
					} else {
						$this->load->view('Base/overlay');
					}
					$this->load->view('Base/modulelist');
				?>				
			</div>			
		</div>
	</div>
</div>
</body>
	<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<?php $this->load->view('Base/bottomscript'); ?>
	<script>
		//Tabgroup Dropdown More
		// fwgautocollapse();	
		$("#tab1").click(function(){
			$('#unitofmeasuredataupdatesubbtn').hide();
			$('#unitofmeasuresavebutton,#unitofmeasuredeleteicon').show();
			$(".tabclass").addClass('hidedisplay');
			$("#tab1class").removeClass('hidedisplay');
			resetFields(); mastertabid=1; masterfortouch = 0;
		});
		$("#tab2").click(function()
		{
			$('#uomconversiondataupdatesubbtn').hide();
			$('#uomconversionaddicon,#uomconversionsavebutton,#uomconversiondeleteicon').show();
			$(".tabclass").addClass('hidedisplay');
			$("#tab2class").removeClass('hidedisplay');
			dynamicloaddropdown('uomid','uomid','uomname','uomid','uom','','');
			dynamicloaddropdown('uomtoid','uomid','uomname','uomid','uom','','');
			//uomconversionaddgrid();
			resetFields();
			mastertabid=2; 
			masterfortouch = 0;
		});
		$(document).ready(function(){
			$("#tabgropdropdown").change(function(){
				var tabgpid = $("#tabgropdropdown").val();
				$('.dbsidebaricons[data-dbsubform="'+tabgpid+'"]').trigger('click');
				uomconversionaddgrid();
			});
			{//for keyboard global variable
				 viewgridview = 0;
				 innergridview = 1;
			 }
		});
		$('#closeuommasterform').click(function(){
			window.location =base_url+'Product';
		});	
		
	</script>
</html>