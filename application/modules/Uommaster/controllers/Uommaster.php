<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Uommaster extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Uommaster/Uommastermodel');
    }
    //first basic hitting view
    public function index()
    {        
    	$moduleid = array(262);
    	sessionchecker($moduleid);
		/*action*/
		$data['moddashboardtabgrp'] = $this->Basefunctions->moddashboardtabgrpgeneration($moduleid);
		$data['moddbfrmfieldsgen'] = $this->Basefunctions->dashboardmodsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$this->load->view('Uommaster/uommasterview',$data);
	}
}