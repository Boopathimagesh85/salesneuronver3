<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Selectivebackup extends MX_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model('Selectivebackup/Selectivebackupmodel');
		$this->load->helper('formbuild');
		$this->load->model('Base/Basefunctions');
		$this->load->model('Sales/Salesmodel');
    }
    public function index() {
		
		$moduleid = array(60);
		sessionchecker($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['location']  = $this->Basefunctions->get_company_settings('locationdrive');
		$this->load->view('Selectivebackup/Selectivebackupview',$data);
	}
	public function gridinformationfetch() {
		
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$chkbox = $_GET['checkbox'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'selectivebackup.selectivebackupid') : 'selectivebackup.selectivebackupid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
	
		$colinfo = array('colmodelname'=>array('Name','location','Date','Time','employeename'),'colmodelindex'=>array('selectivebackup.filename','selectivebackup.location','selectivebackup.Date','selectivebackup.Time','employee.employeename'),'coltablename'=>array('selectivebackup','selectivebackup','selectivebackup','selectivebackup','employee'),'uitype'=>array('2','2','2','2','2'),'colname'=>array('File Name','Location','Date','Time','EmployeeName'),'colsize'=>array('140','100','70','50','70'));
	
		$result=$this->Selectivebackupmodel->viewdynamicdatainfofetch($primarytable,$sortcol,$sortord,$pagenum,$rowscount);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Backup',$width,$height,$chkbox);
		} else {
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Backup',$width,$height,$chkbox);
		}
		echo json_encode($datas);
	}
	// store backup
	public function performanceboost() {
		$this->Selectivebackupmodel->performanceboostmodel();
	}
	public function accledgerupdate() {
		$this->Selectivebackupmodel->accledgerupdate();
	}
}