<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Attributemodel extends CI_Model{
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//itemcounter create
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['attributeelementsname']);
		$formfieldstable = explode(',',$_POST['attributeelementstable']);
		$formfieldscolmname = explode(',',$_POST['attributeelementscolmn']);
		$elementpartable = explode(',',$_POST['attributeelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
		echo 'TRUE';
	}
	//Retrive itemcounter data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['attributeelementsname']);
		$formfieldstable = explode(',',$_GET['attributeelementstable']);
		$formfieldscolmname = explode(',',$_GET['attributeelementscolmn']);
		$elementpartable = explode(',',$_GET['attributeelementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['attributeprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	//itemcounter update
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['attributeelementsname']);
		$formfieldstable = explode(',',$_POST['attributeelementstable']);
		$formfieldscolmname = explode(',',$_POST['attributeelementscolmn']);
		$elementpartable = explode(',',$_POST['attributeelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['attributeprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		echo $result;
	}
	//itemcounter delete
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['attributeelementstable']);
		$parenttable = explode(',',$_GET['attributeelementspartable']);
		$id = $_GET['attributeprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				echo "TRUE";
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			echo "TRUE";
		}
	} 	
}