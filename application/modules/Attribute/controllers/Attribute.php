<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Attribute extends MX_Controller{
    public function __construct() {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Attribute/Attributemodel');
    }
    //first basic hitting view
    public function index() {        
    	$moduleid = array(12);
    	sessionchecker($moduleid);
		//action
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(12);
		$viewmoduleid = array(12);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Attribute/attributeview',$data);
	}
	//create itemcounter
	public function newdatacreate() {  
    	$this->Attributemodel->newdatacreatemodel();
    }
	//information fetchitemcounter
	public function fetchformdataeditdetails() {
		$moduleid = 12;
		$this->Attributemodel->informationfetchmodel($moduleid);
	}
	//update itemcounter
    public function datainformationupdate() {
        $this->Attributemodel->datainformationupdatemodel();
    }
	//delete itemcounter
    public function deleteinformationdata() {
        $moduleid = 12;
		$this->Attributemodel->deleteoldinformation($moduleid);
    } 
}