<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sharingrules extends MX_Controller 
{
    //Constructor Function To Load Models
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Sharingrules/Sharingrulesmodel');
		$this->load->view('Base/formfieldgeneration');		
	}
	//Default View 
	public function index() {
		$moduleid = array(263);
		sessionchecker($moduleid);
		//action
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(263);
		$viewmoduleid = array(263);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['ruleassign'] = $this->datasharingemplistfetch();
		$data['module'] = $this->datasharingmodulelistfetch();
		$this->load->view('Sharingrules/sharingrulesview',$data);
	}
	public function datasharingemplistfetch() {
		$data = array();
		$i=0;
		$tablename = array('employeegroup','userrole','userrole');
		$groupname = array('Groups','Roles','Role and Subordinates');
		$groupkey = array('G','R','SR');
		$m=0;
		$pid = 2;
		foreach($tablename as $key => $tabname) {
			$name = $tabname.'name';
			$id = $tabname.'id';
			$this->db->select(''.$id.' AS Id'.','.''.$name.' AS Name');
			$this->db->from($tabname);
			$this->db->where(''.$tabname.'.status', 1);
			$this->db->order_by(''.$tabname.'.'.$name.'', 'ASC');
			$result=$this->db->get()->result();
			foreach($result as $row) {
				$data[$i]=array('CId'=>$row->Id,'CName'=>$row->Name,'PId'=>$pid,'PName'=>$groupname[$m],'DDval'=>$groupkey[$m].'-'.$row->Id);
				$i++;
			}
			$m++;
			$pid++;
		}
		return $data;
    }
    public function datasharingmodulelistfetch() {
    	$i=0;
    	$data=array();
    	$this->db->select('module.moduleid,module.modulename,module.menuname,menucategory.menucategoryname,menucategory.menucategoryid');
    	$this->db->from('module');
    	$this->db->join('moduleinfo','moduleinfo.moduleid=module.moduleid');
    	$this->db->join('menucategory','menucategory.menucategoryid=module.menucategoryid');
    	$this->db->where('module.menutype','Internal');
    	$this->db->where("FIND_IN_SET('263',module.moduleprivilegeid) >", 0);
    	$this->db->where('module.status',1);
    	$this->db->where('moduleinfo.status',1);
    	$this->db->where('menucategory.status',1);
    	$this->db->order_by('menucategory.sortorder','asc');
    	$this->db->group_by('moduleinfo.moduleid');
   		$result=$this->db->get()->result();
		foreach($result as $row) {
			$data[$i]=array('CId'=>$row->moduleid,'CName'=>$row->modulename,'PId'=>$row->menucategoryid,'PName'=>$row->menucategoryname);
			$i++;
		}
		return $data;
    }
	//new data sharing rules crete and update
	public function datasharerulecreateupdate() {
		$this->Sharingrulesmodel->datasharerulecreateupdatemodel();
	}
	//general sharing rule list
	public function datasharingmodulelist() {
		$grid=$this->Basefunctions->getpagination();
		$result=$this->Sharingrulesmodel->datasharingmodulelistxmlview($grid['sidx'],$grid['sord'],$grid['wh']);
        $pageinfo=$this->Basefunctions->page();
		header("Content-type: text/xml;charset=utf-8");
		$s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .=  "<rows>";
        $s .= "<page>".$_GET['page']."</page>";
        $s .= "<total>".$pageinfo['totalpages']."</total>";
        $s .= "<records>".$pageinfo['count']."</records>";
		ob_clean();
		foreach($result->result() as $row) {
			$mode = $this->Sharingrulesmodel->fetchsharerulemodulemode($row->moduleid);
			$private = ( ($mode == 0)? 'Yes':'No' );
			$pubread = ( ($mode == 1)? 'Yes':'No' );
			$pubrce = ( ($mode == 2)? 'Yes':'No' );
			$pubrced = ( ($mode == 3)? 'Yes':'No' );
			$s .= "<row id='". $row->moduleid."'>";
			$s .= "<cell><![CDATA[".$row->moduleid."]]></cell>";
			$s .= "<cell><![CDATA[".$row->menucategoryname."]]></cell>";
			$s .= "<cell><![CDATA[".$row->menuname."]]></cell>";
			$s .= "<cell><![CDATA[".$private."]]></cell>";
			$s .= "<cell><![CDATA[".$pubread."]]></cell>";
			$s .= "<cell><![CDATA[".$pubrce."]]></cell>";
			$s .= "<cell><![CDATA[".$pubrced."]]></cell>";
			$s .= "</row>";
		}
		$s .= "</rows>";
		echo $s;
	}
	//new custom rule create
	public function newdatacreate() {
		$this->Sharingrulesmodel->newdatacreatemodel();
	}
	//custom rule data grid view
	public function customsharerulegriddatafetch() {
		$grid=$this->Basefunctions->getpagination();
		$result=$this->Sharingrulesmodel->customsharerulegriddataxmlview($grid['sidx'],$grid['sord'],$grid['start'],$grid['limit']);
        $pageinfo=$this->Basefunctions->page();
		header("Content-type: text/xml;charset=utf-8");
		$s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .=  "<rows>";
        $s .= "<page>".$_GET['page']."</page>";
        $s .= "<total>".$pageinfo['totalpages']."</total>";
        $s .= "<records>".$pageinfo['count']."</records>";
		ob_clean();
		foreach($result->result() as $row) {
			$accesstype = ( ($row->accesstype == '1')? 'Readonly' : 'Read,Create/Edit,Delete' );
			$s .= "<row id='". $row->datasharecustomruleid."'>";
			$s .= "<cell><![CDATA[".$row->menuname."]]></cell>";
			$s .= "<cell><![CDATA[".$row->dsfromempgrpname.$row->dsfromrolename."]]></cell>";
			$s .= "<cell><![CDATA[".$row->dstoempgrpname.$row->dstorolename."]]></cell>";
			$s .= "<cell><![CDATA[".$accesstype."]]></cell>";
			$s .= "<cell><![CDATA[".$row->superiorallow."]]></cell>";
			$s .= "<cell><![CDATA[".$row->statusname."]]></cell>";
			$s .= "</row>";
		}
		$s .= "</rows>";
		echo $s;
	}
	//custom rule data fetch
	public function fetchformdataeditdetails() {
		$this->Sharingrulesmodel->fetchformdataeditdetailsmodel();
	}
	//update custom rule data
	public function customruledataupdate() {
		$this->Sharingrulesmodel->customruledataupdatemodel();
	}
	//delete custom rule data
	public function customruledeletefunction() {
		$this->Sharingrulesmodel->customruledatadeletemodel();
	}
}