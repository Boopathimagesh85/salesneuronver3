<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sharingrulesmodel extends CI_Model {
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//data sharing module list
	public function datasharingmodulelistxmlview($sidx,$sord,$wh) {
		$this->db->select('module.moduleid,module.modulename,module.menuname,menucategory.menucategoryname');
		$this->db->from('module');
		$this->db->join('moduleinfo','moduleinfo.moduleid=module.moduleid');
		$this->db->join('menucategory','menucategory.menucategoryid=module.menucategoryid');
		$this->db->where('module.menutype','Internal');
		$this->db->where("FIND_IN_SET('263',moduleprivilegeid) >", 0);
		$this->db->where('module.status',1);
		$this->db->where('moduleinfo.status',1);
		if($wh!='1') {
			$this->db->where($wh);
		}
		$this->db->group_by('moduleinfo.moduleid');
		$this->db->order_by($sidx,$sord);
		$result = $this->db->get();
		return $result;
	}
	//new data sharing rules crete and update
	public function datasharerulecreateupdatemodel() {
		$griddata=$_POST['shareruledata'];
		$formdata=json_decode($griddata,true);
		$count = count($formdata);
		$date = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		for($i=0;$i<$count;$i++ ) {
			$modid = $formdata[$i]['moduleid'];
			$datasharemode = 0;
			if($formdata[$i]['private'] == 'Yes') {
				$datasharemode = 0;
			} else if($formdata[$i]['pubread'] == 'Yes') {
				echo $datasharemode = 1;
			} else if($formdata[$i]['pubreadcrdedit'] == 'Yes') {
				$datasharemode = 2;
			} else if($formdata[$i]['pubreadcrdedtdel'] == 'Yes') {
				$datasharemode = 3;
			}
			$check = $this->checksharerulemoduleid($modid);
			if($check == 'True') {
				$updatestatus = array('datashareruletype'=>$datasharemode,'lastupdateuserid'=>$userid,'lastupdatedate'=>$date);
				$this->db->where('datasharerule.moduleid',$modid);
				$this->db->update('datasharerule',$updatestatus);
			} else {
				$ruledata = array('moduleid'=>$modid,'datashareruletype'=>$datasharemode,'createuserid'=>$userid,'lastupdateuserid'=>$userid,'createdate'=>$date,'lastupdatedate'=>$date,'status'=>1);
				$this->db->insert('datasharerule',$ruledata);
			}
		}
		echo "TRUE";
	}
	//check data share module id
	public function checksharerulemoduleid($moduleid) {
		$datasharereult = $this->db->select('moduleid')->from('datasharerule')->where('datasharerule.moduleid',$moduleid)->get();
		if($datasharereult->num_rows() > 0) {
			return 'True';
		} else {
			return 'Fail';
		}
	}
	//check data share module id
	public function fetchsharerulemodulemode($moduleid) {
		$ruletype = 1;
		$datasharereult = $this->db->select('moduleid,datashareruletype')->from('datasharerule')->where('datasharerule.moduleid',$moduleid)->get();
		foreach($datasharereult->result() as $row) {
			$ruletype = $row->datashareruletype;
		}
		return $ruletype;
	}
	//New custom role create
	public function newdatacreatemodel() {
		$moduleid = $_POST['sharemoduleid'];
		$sharefrom = $_POST['datasharedfrom'];
		$sharefromtype = $_POST['datasharedfromtype'];
		$shareto = $_POST['datasharedto'];
		$sharetotype = $_POST['datasharedtotype'];
		$sharetype = $_POST['datasharetype'];
		$shareallow = $_POST['datasuperiorallow'];
		$date = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$status	= $this->Basefunctions->activestatus;
		$custrule = array(
			'moduleid'=>$moduleid,
			'recordsharedfrom'=>$sharefrom,
			'recordsharedfromtypeid'=>$sharefromtype,
			'recordsharedto'=>$shareto,
			'recordsharedtotypeid'=>$sharetotype,
			'accesstype'=>$sharetype,
			'superiorallow'=>$shareallow,
			'createdate'=>$date,
			'lastupdatedate'=>$date,
			'createuserid'=>$userid,
			'lastupdateuserid'=>$userid,
			'status'=>$status
		);
		$this->db->insert('datasharecustomrule',$custrule);
		echo 'True';
	}
	//custom share rule grid view
	public function customsharerulegriddataxmlview($sidx,$sord,$start,$limit) {
		$this->db->select('SQL_CALC_FOUND_ROWS  datasharecustomrule.datasharecustomruleid,module.menuname,datasharecustomrule.accesstype,datasharecustomrule.superiorallow,status.statusname,empgrpfrom.employeegroupname AS dsfromempgrpname,urolefrom.userrolename AS dsfromrolename,empgrpto.employeegroupname AS dstoempgrpname,uroleto.userrolename AS dstorolename',false);
		$this->db->from('datasharecustomrule');
		$this->db->join('module','module.moduleid=datasharecustomrule.moduleid');
		$this->db->join('employeegroup as empgrpfrom','empgrpfrom.employeegroupid=datasharecustomrule.recordsharedfrom AND datasharecustomrule.recordsharedfromtypeid=2','left outer');
		$this->db->join('userrole as urolefrom','urolefrom.userroleid=datasharecustomrule.recordsharedfrom AND (datasharecustomrule.recordsharedfromtypeid=3 OR datasharecustomrule.recordsharedfromtypeid=4','left outer');
		$this->db->join('employeegroup as empgrpto','empgrpto.employeegroupid=datasharecustomrule.recordsharedto AND datasharecustomrule.recordsharedtotypeid=2','left outer');
		$this->db->join('userrole as uroleto','uroleto.userroleid=datasharecustomrule.recordsharedto AND (datasharecustomrule.recordsharedtotypeid=3 OR datasharecustomrule.recordsharedtotypeid=4','left outer');
		$this->db->join('status','status.status=datasharecustomrule.status');
		$this->db->where_not_in('datasharecustomrule.status',array(3,0));
		$this->db->order_by($sidx,$sord);
        $this->db->limit($limit,$start);
		$result=$this->db->get();
		return $result;
	}
	//fetch custom rule edit details
	public function fetchformdataeditdetailsmodel() {
		$id = $_GET['dataprimaryid'];
		$this->db->select('datasharecustomruleid,moduleid,accesstype,superiorallow,recordsharedfromtypeid,recordsharedfrom,recordsharedtotypeid,recordsharedto',false);
		$this->db->from('datasharecustomrule');
		$this->db->where('datasharecustomrule.datasharecustomruleid',$id);
		$this->db->where_not_in('datasharecustomrule.status',array(3,0));
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('moduleid'=>$row->moduleid,'accesstype'=>$row->accesstype,'superiorallow'=>$row->superiorallow,'fromtypeid'=>$row->recordsharedfromtypeid,'totypeid'=>$row->recordsharedtotypeid,'sharedfrom'=>$row->recordsharedfrom,'sharedto'=>$row->recordsharedto);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//Custom rule data update
	public function customruledataupdatemodel() {
		$moduleid = $_POST['sharemoduleid'];
		$sharefrom = $_POST['datasharedfrom'];
		$sharefromtype = $_POST['datasharedfromtype'];
		$shareto = $_POST['datasharedto'];
		$sharetotype = $_POST['datasharedtotype'];
		$sharetype = $_POST['datasharetype'];
		$shareallow = $_POST['datasuperiorallow'];
		$date = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$status	= $this->Basefunctions->activestatus;
		$primaryid = $_POST['primaryid'];
		$custrule = array(
			'moduleid'=>$moduleid,
			'recordsharedfrom'=>$sharefrom,
			'recordsharedfromtypeid'=>$sharefromtype,
			'recordsharedto'=>$shareto,
			'recordsharedtotypeid'=>$sharetotype,
			'accesstype'=>$sharetype,
			'superiorallow'=>$shareallow,
			'lastupdatedate'=>$date,
			'lastupdateuserid'=>$userid,
			'status'=>$status
		);
		$this->db->where('datasharecustomrule.datasharecustomruleid',$primaryid);
		$this->db->update('datasharecustomrule',$custrule);
		echo 'True';
	}
	//Custom rule data delete
	public function customruledatadeletemodel() {
		$date = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$primaryid = $_GET['primaryid'];
		$delrule = array('status'=>0,'lastupdateuserid'=>$userid,'lastupdatedate'=>$date);
		$this->db->where('datasharecustomrule.datasharecustomruleid',$primaryid);
		$this->db->update('datasharecustomrule',$delrule);
		echo "TRUE";
	}
}