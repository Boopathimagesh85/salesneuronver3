<style type="text/css">
	#generaladvanceddrop {
		left: -61.3594px !important;
	}
   #customadvanceddrop {
		left: -82.3594px !important;
	}
	.ui-jqgrid-view [type="checkbox"]:not(:checked), .ui-jqgrid-view [type="checkbox"]:checked {
	    position: relative;
	    left: auto;
	    opacity: 1 !important;
	    vertical-align: middle;
	}
	.headerformcaptionstyleforprofile, .headerformcaptionstyle {
	    font-size: 0.87rem;
	    font-weight: 500;
	    padding: 0.2em 0px !important;
	    background: #465a63;
	}
	.ui-jqgrid .ui-jqgrid-hdiv {
	    position: relative;
	    margin: 0;
	    padding: 22px 0px;
	}
	.ui-jqgrid .ui-jqgrid-htable th div {
	    height: 26px;
	    margin-top: -6px;
	}
	.ui-jqgrid .ui-jqgrid-resize-ltr {
	    margin: -10px -2px -2px 0;
	}
	.ui-jqgrid tr:hover {
	    background-color: #fff;
	}
	.ui-widget-content .ui-state-default {
	    font-size: 15px;
	}
	.ui-jqgrid .ui-grid-ico-sort {
	    display: none;
	}
</style>
<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<?php
				$dataset['gridtitle'] = $gridtitle['title'];
				$dataset['titleicon'] = $gridtitle['titleicon'];
				$dataset['moduleid'] = '263';
				$dataset['action'][0] = array('actionid'=>'gensharerulesubbtn','actiontitle'=>'Submit','actionmore'=>'Yes','ulname'=>'general');
				$dataset['action'][1] = array('actionid'=>'ruleaddicon','actiontitle'=>'Add','actionmore'=>'Yes','ulname'=>'custom');
				$this->load->view('Base/formwithgridmenuheader.php',$dataset);
			?>
			<div class="large-12 columns addformunderheader">&nbsp;</div>
			<div class="large-12 columns scrollbarclass addformcontainer">
				<!-- More Action DD start -- Gowtham -->
					<ul id="generaladvanceddrop" class="multinavaction-drop arrow_box" style="white-space: nowrap; position: absolute; top: 34.8px; left: -62.1666px; opacity: 1; display: none;">
						<li id="generalrulesearchicon"><span class="icon-box"><i class="material-icons searchiconclass " title="Search">search</i>Search</span></li>
					</ul>
					<ul id="customadvanceddrop" class="multinavaction-drop arrow_box" style="white-space: nowrap; position: absolute; top: 34.8px; left: -62.1666px; opacity: 1; display: none;">
						<li id="ruleediticon"><span class="icon-box"><i class="material-icons editiconclass " title="Edit">edit</i>Edit</span></li>
						<li id="ruledeleteicon"><span class="icon-box"><i class="material-icons deleteiconclass " title="Delete">delete</i>Delete</span></li>
					</ul>
				<!-- More Action DD End -- Gowtham -->
				<div class="row">&nbsp;</div>
				<div id="subformspan1" class=" hiddensubform padding-space-open-for-form">
					<div class="large-12 columns paddingbtm paddingzero ">
						<div class="large-12 columns paddingzero">
							<div class="large-12 columns viewgridcolorstyle borderstyle" style="padding-left:0;padding-right:0;">
								<div class="large-12 columns headerformcaptionstyleforprofile" style="height:auto;padding: 0.2rem 0 0rem;">
									<span class="large-6 medium-6 small-12 columns lefttext small-only-text-center" style="line-height:1.8">General Rules List</span>
								</div>
								<div class="large-12 columns paddingzero  viewgridcolorstyle " id="generaladdgridwidth">	
									<table id="generaladdgrid"></table>
									<div id="generaladdgridnav"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="subformspan2" class="hidedisplay hiddensubform padding-space-open-for-form">
				<div class="closed" id="customrulesectionoverlay">
				<div class="large-12 columns ">
					<form method="POST" name="customrulecreateionform" class="" action ="" id="customrulecreateionform">
						<span id="customdatashareruleaddvalidation" class="validationEngineContainer" >
							<span id="customdatashareruleupdatevalidation" class="validationEngineContainer">
								<div class="large-4 columns paddingbtm large-offset-4">
									<div class="large-12 columns cleardataform border-modal-8px modal-top-margin-for-all" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
										<div class="large-12 columns sectionheaderformcaptionstyle">Custom Rule</div>
										<div class="static-field large-12 columns">
											<label>
												Module Name <span class="mandatoryfildclass">*</span>
											</label>
											<select class="chzn-select validate[required]" id="sharemoduleid" name="sharemoduleid" data-prompt-position="topLeft:14,36" tabindex="100" data-placeholder="Select">
												<option value=""></option>
												<?php
													$prev = ' ';
													for ($i=0;$i<count($module);$i++) {
														if($module[$i]['CName'] != '') {
															$cur = $module[$i]['PId'];
															$a ="";
															if($prev != $cur ) {
																if($prev != " ") {
																	echo '</optgroup>';
																}
																echo '<optgroup label="'.$module[$i]['PName'].'" class="'.str_replace(' ','',$module[$i]['PName']).'">';
																$prev = $module[$i]['PId'];
																$a = "pclass";
															}
															echo '<option value="'.$module[$i]['CId'].'" data-moduleid="'.$module[$i]['CId'].'" >'.$module[$i]['CName'].'</option>';
														}
													}
												?>
											</select>
										</div>
										<div class="static-field large-12 columns">
											<label>
												Records Shared From <span class="mandatoryfildclass">*</span>
											</label>
											<select class="chzn-select validate[required]" id="datasharedfromdd" name="datasharedfromdd" data-prompt-position="topLeft:14,36" tabindex="101" data-placeholder="Select">
												<option value=""></option>
												<?php
													$prev = ' ';
													for ($i=0;$i<count($ruleassign);$i++) {
														if($ruleassign[$i]['CName'] != '') {
															$cur = $ruleassign[$i]['PId'];
															$a ="";
															if($prev != $cur ) {
																if($prev != " ") {
																	echo '</optgroup>';
																}
																echo '<optgroup  label="'.$ruleassign[$i]['PName'].'" class="'.str_replace(' ','',$ruleassign[$i]['PName']).'">';
																$prev = $ruleassign[$i]['PId'];
																$a = "pclass";
															}
															echo '<option value="'.$ruleassign[$i]['PId'].':'.$ruleassign[$i]['CId'].'" data-sharefromtypeid="'.$ruleassign[$i]['PId'].'" data-sharefromid="'.$ruleassign[$i]['CId'].'">'.$ruleassign[$i]['CName'].'</option>';
														}
													}
												?>
												<input name="datasharedfrom" type="hidden" id="datasharedfrom" value="" />
												<input name="datasharedfromtype" type="hidden" id="datasharedfromtype" value="" />
											</select>
										</div>
										<div class="static-field large-12 columns">
											<label>
												Records Shared To <span class="mandatoryfildclass">*</span>
											</label>
											<select class="chzn-select validate[required]" id="datasharedtodd" name="datasharedtodd" data-prompt-position="topLeft:14,36" tabindex="102" data-placeholder="Select">
												<option value=""></option>
												<?php
													$prev = ' ';
													for ($i=0;$i<count($ruleassign);$i++) {
														if($ruleassign[$i]['CName'] != '') {
															$cur = $ruleassign[$i]['PId'];
															$a ="";
															if($prev != $cur ) {
																if($prev != " ") {
																	echo '</optgroup>';
																}
																echo '<optgroup  label="'.$ruleassign[$i]['PName'].'" class="'.str_replace(' ','',$ruleassign[$i]['PName']).'">';
																$prev = $ruleassign[$i]['PId'];
																$a = "pclass";
															}
															echo '<option value="'.$ruleassign[$i]['PId'].':'.$ruleassign[$i]['CId'].'" data-sharetotypeid="'.$ruleassign[$i]['PId'].'" data-sharetoid="'.$ruleassign[$i]['CId'].'" >'.$ruleassign[$i]['CName'].'</option>';
														}
													}
												?>
											</select>
											<input name="datasharedto" type="hidden" id="datasharedto" value="" />
											<input name="datasharedtotype" type="hidden" id="datasharedtotype" value="" />
										</div>
										<div class="static-field large-12 columns">
											<label>
												Access Type <span class="mandatoryfildclass">*</span>
											</label>
											<select class="chzn-select validate[required]" id="datasharetype" name="datasharetype" data-prompt-position="topLeft:14,36" tabindex="103" data-placeholder="Select" data-defvalattr="1">
												<option value="1">Readonly</option>
												<option value="3">Read,Create/Edit,Delete</option>
											</select>
										</div>
										<div class="input-field large-12 columns">
											<input name="datasuperiorallowcboxid" type="checkbox"  id="datasuperiorallowcboxid" class="filled-in checkboxcls radiochecksize" tabindex="104" data-hidname="datasuperiorallow" value="No">
											<label for="datasuperiorallowcboxid">Superior allowed</label>
											<input name="datasuperiorallow" type="hidden" id="datasuperiorallow" value="No" data-defvalattr="No" />
										</div>
										<div class="divider large-12 columns"></div>
										<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
											<input id="datasubmit" class="alertbtn addkeyboard" type="button" value="Submit" name="datasubmit" tabindex="105">
											<input id="dataupdate" class="alertbtn updatekeyboard hidedisplay" type="button" value="Submit" name="dataupdate" tabindex="105" style="display: none;">
											<input id="rulecancelbutton" class="alertbtn cancelkeyboard " type="button" value="Cancel" name="rulecancelbutton" tabindex="106">
										</div>
									</div>
								</div>
							</span>
						</span>
						<!--hidden fields-->
						<input name="primaryid" type="hidden" id="primaryid" value="" />
					</form>
					</div>
					</div>
					<div class="large-12 columns paddingbtm paddingzero">
						<div class="large-12 columns paddingzero">
							<div class="large-12 columns viewgridcolorstyle" style="padding-left:0;padding-right:0;">
								<div class="large-12 columns headerformcaptionstyle borderstyle" style="height:auto;padding: 0.2rem 0 0rem;">
									<span class="large-6 medium-6 small-12 columns lefttext small-only-text-center" style="">Custom Rule List</span>
								<div class="large-12 columns  viewgridcolorstyle " id="customaddgridwidth" style="padding-left:0;padding-right:0;">	
									<table id="customaddgrid"></table>
									<div id="customaddgridnav"> </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</div>
</div>
