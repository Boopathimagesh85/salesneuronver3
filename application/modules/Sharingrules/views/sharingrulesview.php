<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('Base/headerfiles'); ?>
	<link href="<?php echo base_url();?>css/jqgrid.min.css" media="screen" rel="stylesheet" type="text/css" />
	<style>
		#gbox_moduleaddgrid .ui-jqgrid-sortable{left:16px}
	</style>
</head>
<body>
	<?php
		$device = $this->Basefunctions->deviceinfo();
		$this->load->view('sharingrulesform');
		$this->load->view('Base/basedeleteform');
		if($device=='phone') {
			$this->load->view('Base/overlaymobile');
		} else {
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
	?>
	<script src="<?php echo base_url();?>js/plugins/jqgrid/jquery.jqGrid.src.min.js"></script>
	<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js Files -->
	<script src="<?php echo base_url();?>js/Sharingrules/sharingrules.js" type="text/javascript"></script>
</body>
</html>