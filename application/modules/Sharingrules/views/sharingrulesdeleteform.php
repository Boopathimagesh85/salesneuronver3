<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="roledeleteformoverlay" style="display:block;">	
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-3 medium-6 small-10 large-centered medium-centered small-centered columns">
		<span id="userroledeletevalidation" class="validationEngineContainer" >
			<div class="row">&nbsp;</div>
			<div class="row">
				<div class="large-12 columns alertsheadercaptionstyle" style="text-align:left">
					<div class="small-12 columns"><span style="color:#ffffff;padding-right:0.5rem"> </span> Transfer & Delete</div>
				</div>
				<div class="large-12 columns" style="background:#ffffff">&nbsp;</div>
				<div class="large-12 columns cleardataform" style="background:#ffffff">
					<select class="chzn-select validate[required]" id="conformroleid" name="conformroleid" data-prompt-position="topLeft" data-placeholder="Select">
						<option value=""></option>
						<?php foreach($rolename as $key):?>
							<option value="<?php echo $key->userroleid;?>" data-roleid="<?php echo $key->userroleid;?>"><?php echo $key->userroleid;?>-<?php echo $key->userrolename;?></option>
						<?php endforeach;?>
					</select>
					<input type="hidden" name="delroleid" id="delroleid" value="" />
				</div>
				<div class="large-12 columns" style="background:#ffffff">&nbsp;</div>
				<div class="large-12 large-centered columns" style="background:#ffffff;text-align:center">
					<span class="" style="	color: #5e6d82;font-family: segoe ui;font-size: 0.9rem;">Delete the data ?</span>
					<div class="large-12 column">&nbsp;</div>
				</div>
			</div>
			<span class="firsttab" tabindex="1000"></span>
			<div class="row" style="background:#ffffff">
				<div class="medium-2 large-2 columns"> &nbsp;</div>
				<div class="small-6  medium-4 large-4 columns">
					<input type="button" id="basedeleteyes" name="" value="Yes" tabindex="1001" class="btn ffield formbuttonsalert" >	
				</div>
				<div class="small-6 medium-4 large-4 columns">
					<input type="button" id="basedeleteno" name="" value="No" tabindex="1002" class="btn flloop formbuttonsalert alertsoverlaybtn" >	
				</div>
				<span class="lasttab" tabindex="1003"></span>
				<div class="medium-2 large-2 columns"> &nbsp;</div>
			</div>
			<div class="row" style="background:#ffffff">&nbsp;</div>
			<div class="row" style="background:#ffffff">&nbsp;</div>
		</span>
		</div>
	</div>
</div>