<!DOCTYPE html>
<html lang='en'>
	<head>
		<?php $this->load->view('Base/headerfiles'); ?>
	</head>
	<body class='hidedisplay'>
		<?php
			$moduleid = implode(',',$moduleids);
			$device = $this->Basefunctions->deviceinfo();
			$dataset['gridenable'] = 'yes';
			$dataset['modulename']='Smssubscribers';
			$dataset['griddisplayid'] = 'smssubscriberscreationview';
			$dataset['gridtitle'] = $gridtitle['title'];
			$dataset['titleicon'] = $gridtitle['titleicon'];
			$dataset['spanattr'] = array();
			$dataset['gridtableid'] = 'smssubscribersaddgrid';
			$dataset['griddivid'] = 'smssubscribersaddgridnav';
			$dataset['moduleid'] = $moduleids;
			$dataset['forminfo'] = array(array('id'=>'smssubscriberscreationformadd','class'=>'hidedisplay','formname'=>'smssubscriberscreationform'),array('id'=>'smssubscribersaddformadd','class'=>'hidedisplay','formname'=>'smssubscribersaddform'));
			if($device=='phone') {
				$this->load->view('Base/gridmenuheadermobile',$dataset);
				$this->load->view('Base/overlaymobile');
				$this->load->view('Base/viewselectionoverlay');
			} else {
				$this->load->view('Base/gridmenuheader',$dataset);
				$this->load->view('Base/overlay');
			}
			$this->load->view('Base/modulelist');
			$this->load->view('Base/basedeleteform');
		?>
	</body>
	<?php $this->load->view('Base/bottomscript'); ?>
	<script src='<?php echo base_url();?>js/Smssubscribers/smssubscribers.js' type='text/javascript'></script>
	<script>
	$(document).ready(function(){
		$('#tabgropdropdown').change(function(){
			var tabgpid = $('#tabgropdropdown').val();
			$('.sidebaricons[data-subform='+tabgpid+']').trigger('click');
		});
	});
</script>
</html>