<div class="large-12 columns paddingzero formheader">
<?php
$device = $this->Basefunctions->deviceinfo();
if($device=='phone') {
	echo '<div class="large-12 columns headercaptionstyle headerradius addformreloadtablet paddingzero">
			<div class="large-6 medium-6 small-6 columns headercaptionleft">
				
				<span class="gridcaptionpos"><span>'.$gridtitle.'</span></span>
			</div>
			<div class="large-5 medium-6 small-6 columns addformheadericonstyle addformaction righttext">
				<span class="icon-box" id="smsfrmappcloseform" title="Close"><i class="material-icons">close</i> </span>
			</div>';
	echo '</div>';
} else {
	echo '<div class="large-12 columns headercaptionstyle headerradius addformreloadtablet paddingzero">
			<div class="large-6 medium-6 small-6 columns headercaptionleft">
				
				<span class="gridcaptionpos"><span>'.$gridtitle.'</span></span>
			</div>
			<div class="large-6 medium-6 small-6 columns addformheadericonstyle addformaction righttext">
				<span class="icon-box" id="smsfrmappcloseform" title="Close"><i class="material-icons">close</i><span class="actiontitle">Close</span> </span>
			</div>';
	echo '</div>';
}
?>
<div class='large-12 columns addformunderheader'>&nbsp;</div>
<!-- tab group creation -->
<?php  if($device!='phone') {?>
	<div class="large-12 columns tabgroupstyle desktoptabgroup">
		<ul class="tabs" data-tab="">
			<li id="tab1" class="tab-title sidebaricons active ftabnew" data-subform="1">
				<span class="waves-effect waves-ripple waves-light">From Application</span>
			</li>
			<li class="morelabel tab-title sidebaricons hidedisplay" data-subform="x"><span class="waves-effect waves-ripple waves-light">More</span></li>
			<li id="moretab" class="tab-title moretab hidedisplay"><span class="dropdown-button" data-activates="tabgroupmoredropdown">More<i class="material-icons">details</i></span>
				<ul id="tabgroupmoredropdown" class="dropdown-content"></ul>
			</li>
		</ul>
	</div>
<?php } else {?>
	<div class="large-12 columns tabgroupstyle mobiletabgroup">
		<ul class="tabs" data-tab="">
			<li id="tab1"  class="tab-title sidebaricons active ftabnew" data-subform="1">
				<span  class="waves-effect waves-ripple waves-light">From Application</span>
			</li>
		</ul>
	</div>
<?php }?>
</div>
<div class='large-12 columns addformunderheader'>&nbsp;</div>
<div class='large-12 columns scrollbarclass addformcontainer tabtouchaddcontainer'>
	<div class='row'>&nbsp;</div>
	<form method='POST' name='smssubscribersappdataaddform' class='' action ='' id='smssubscribersappdataaddform' enctype='multipart/form-data'>
		<div id="subformspan1" class="">
			<span id="smssubscriberallicationvalidationadd" class="validationEngineContainer" >
				<span id="smssubscriberallicationvalidationupdate" class="validationEngineContainer">
					<div class="large-4 columns paddingbtm">
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff;">
							<div class="large-12 columns headerformcaptionstyle">Subscriber Details</div>
							<div class="large-12 columns static-field">
								<label>Type Name<span class="mandatoryfildclass">*</span></label>
								<select class="chzn-select validate[required]" data-placeholder="Select" data-prompt-position="topLeft" id="fromapptempaltetypeid" name="fromapptemplatetypeid" tabindex="103">
									<option value="">Select</option>
									<?php foreach($typename as $key):?>
										<option value="<?php echo $key->templatetypeid; ?>" data-modname="<?php echo $key->templatetypename;?>"><?php echo $key->templatetypename; ?> </option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="large-12 columns static-field">
								<label>Group Name<span class="mandatoryfildclass">*</span></label>
								<select class="chzn-select validate[required]" data-placeholder="Select" data-prompt-position="topLeft" id="smsgroupsid" name="smsgroupsid" tabindex="103">
									<option value="">Select</option>
									<?php foreach($groupname as $key):?>
										<option value="<?php echo $key->campaigngroupsid; ?>" data-modname="<?php echo $key->campaigngroupsname;?>"><?php echo $key->campaigngroupsname; ?> </option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="large-12 columns static-field">
								<label>Module Name<span class="mandatoryfildclass">*</span></label>
								<select class="chzn-select" data-placeholder="Select" data-prompt-position="topLeft" id="moduleid" name="moduleid" tabindex="101">
									<option value="">select</option>
									<?php foreach($modname as $key):?>
										<option value="<?php echo $key->moduleid; ?>" data-modname="<?php echo $key->menuname;?>"><?php echo $key->menuname; ?> </option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="large-12 columns static-field">
								<label>View Name</label>
								<select class="chzn-select" data-placeholder="Select" data-prompt-position="topLeft" id="viewid" name="viewid" tabindex="101">
									<option value="">select</option>
								</select>
							</div>
							<div class="large-12 columns addformaction" style="text-align: right">
								<label> </label>
								<input id="smsappsubmit" class="btn  addbtnclass" type="button" value="Submit" name="smsappsubmit" tabindex="106">
								<input id="smsappupdate" class="btn updatebtnclass hidedisplay" type="button" value="Submit" name="smsappupdate" tabindex="107" style="display: none;">
							</div>
							<!-- hidden fields-->
							<input id="primaryid" type="hidden" value="" name="primaryid">
							<input id="leadid" type="hidden" value="" name="leadid">
							<input id="contactid" type="hidden" value="" name="contactid">
							<input id="userid" type="hidden" value="" name="userid">
							<input id="accountid" type="hidden" value="" name="accountid">
							<input id="rolesid" type="hidden" value="" name="rolesid">
							<input id="randsid" type="hidden" value="" name="randsid">
						</div>
					</div>
				</span>
			</span>
			<div class="large-8 columns paddingbtm">
				<div class="large-12 columns paddingzero">
					<div class="large-12 columns borderstyle paddingzero">
						<div class="large-12 columns headerformcaptionstyle" style=" height:auto; padding: 0.2rem 0 0rem;">
							<span class="large-6 medium-6 small-12 columns lefttext small-only-text-center" >Subscriber List<span class="paging-box smsappgridfootercontainer"></span></span>
							<span class="large-6 medium-6 small-12 columns innergridicon righttext small-only-text-center">
							</span>
						</div>
						<?php
							$device = $this->Basefunctions->deviceinfo();
							if($device=='phone') {
								echo '<div class="large-12 columns forgetinggridname" id="smsfromapplicationaddgridwidth" style="padding-left:0;padding-right:0;"><div id="smsfromapplicationaddgrid" class=" inner-row-content inner-gridcontent" style="max-width:2000px; height:420px;top:0px;">
									<!-- Table header content , data rows & pagination for mobile-->
								</div>
								<footer class="inner-gridfooter footercontainer" id="smsfromapplicationaddgridfooter">
									<!-- Footer & Pagination content -->
								</footer></div>';
							} else {
								echo '<div class="large-12 columns forgetinggridname" id="smsfromapplicationaddgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="smsfromapplicationaddgrid" style="max-width:2000px; height:460px;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
								<div class="footercontainer footer-content" id="smsfromapplicationaddgridfooter">
									<!-- Footer & Pagination content -->
								</div></div>';
							}
						?>
					</div>
				</div>
			</div>
		</div>
		<?php
			$val = implode(',',$filedmodids);
			echo hidden('viewfieldids',$val);
		?>
	</form>
</div>