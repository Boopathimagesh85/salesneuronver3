<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Smssubscribers extends MX_Controller {
	function __construct() {
		parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Base/Basefunctions');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Smssubscribers/Smssubscribersmodel');
	}
	public function index() {
		$moduleid= array(271);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp']=$this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp']=$this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle']=$this->Basefunctions->gridtitleinformationfetch($moduleid);
		$viewfieldsmoduleids = array(271);
		$viewmoduleid = array(271);
		$data['moduleids']=$viewmoduleid;
		$data['filedmodids']=$viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['groupname'] = $this->Basefunctions->simpledropdownwithcond('campaigngroupsid','campaigngroupsname','campaigngroups','smsgrouptypeid',3);
		$data['typename'] = $this->Basefunctions->simpledropdownwithcond('templatetypeid','templatetypename','templatetype','moduleid',271);
		$data['modname'] = $this->Basefunctions->simpledropdownwithcond('moduleid','menuname','module','moduleprivilegeid',271);
		$this->load->view('Smssubscribers/smssubscribersview',$data);
	}
	public function newdatacreate() {
		$this->Smssubscribersmodel->newdatacreatemodel();
	}
	public function fetchformdataeditdetails() {
		$moduleid=271;
		$this->Smssubscribersmodel->informationfetchmodel($moduleid);
	}
	public function datainformationupdate() {
		$this->Smssubscribersmodel->datainformationupdatemodel();
	}
	public function deleteinformationdata() {
		$moduleid=271;
		$this->Smssubscribersmodel->deleteoldinformation($moduleid);
	}
	//module drop down load function
	public function moduledropdownloadfun() {
		$this->Smssubscribersmodel->moduledropdownloadfunmodel();
	}
	//View drop down load function
	public function viewdropdownloadfun() {
		$this->Smssubscribersmodel->viewdropdownloadfunmodel();
	}
	//parent table get
	public function parenttableget() {
		$this->Smssubscribersmodel->parenttablegetmodel();
	}
	//invite user grid data fetch
	public function fromapplicationgrigdatafetch() {
		$creationid = $_GET['viewid'];
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$viewcolmoduleids = $_GET['moduleid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$filter = '';
		if(isset($_GET['aptypeid'])){
			$apptypeid = $_GET['aptypeid'];
		}else{
			$apptypeid = NULL;
		}
		$chkbox = $_GET['checkbox'];
		$sortcol = isset($_GET['sortcol']) ? $_GET['sortcol'] : '';
		$sortord = isset($_GET['sortord']) ? $_GET['sortord'] : '';
		$footer = isset($_GET['footername']) ? $_GET['footername'] : '';
		$result = $this->Smssubscribersmodel->smssubsciberviewdynamicdatainfofetch($creationid,$primaryid,$viewcolmoduleids,$pagenum,$rowscount,$sortcol,$sortord,$filter,$apptypeid);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobileviewgriddatagenerate($result,$primarytable,$primaryid,$width,$height,$chkbox);
		} else {
			$datas = viewgriddatagenerate($result,$primarytable,$primaryid,$width,$height,$footer,$chkbox);
		}
		echo json_encode($datas);
	}
	//from application subscriber add
	public function fromappsubscriberadd() {
		$this->Smssubscribersmodel->fromappsubscriberaddmodel();
	}
	//sms group dd data fetch
	public function smsgroupdddatafetch() {
		$this->Smssubscribersmodel->smsgroupdddatafetchmodel();
	}
	//group name dd data fetch
	public function groupnameddfetch() {
		$this->Smssubscribersmodel->groupnameddfetchmodel();
	}
	//sms group type id get
	public function smsgroupidget() {
		$this->Smssubscribersmodel->smsgroupidgetmodel();
	}
	//Unique email id restriction while edit
	public function uniquedynamicviewemailidcheck() {
		$this->Smssubscribersmodel->uniquedynamicviewemailidcheckmodel();
	}
	//Unique Mobile Number restriction while edit
	public function uniquedynamicviewmobnocheck() {
		$this->Smssubscribersmodel->uniquedynamicviewmobnocheckmodel();
	}
}