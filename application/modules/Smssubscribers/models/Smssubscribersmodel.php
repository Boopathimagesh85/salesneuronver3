<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Smssubscribersmodel extends MX_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Base/Crudmodel');
	}
	public function newdatacreatemodel() {
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		$partablename = $this->Crudmodel->filtervalue($elementpartable);
		$fieldstable = $this->Crudmodel->filtervalue($formfieldstable);
		$gridfieldpartabname = explode(',',$_POST['griddatapartabnameinfo']);
		$gridrows = explode(',',$_POST['numofrows']);
		$girddata = $_POST['griddatas'];
		$girddatainfo = json_decode($girddata, true);
		$gridpartablename =  $this->Crudmodel->filtervalue($gridfieldpartabname);
		$tableinfo = explode(',',$fieldstable);
		$restricttable = explode(',',$_POST['resctable']);
		$templatetypeid = $_POST['templatetypeid'];
		$mobilenumber = $_POST['mobilenumber'];
		$emailid = $_POST['emailid'];
		if($templatetypeid == 5) {
			$checkemail = $this->emailidcheck($emailid);
			if($checkemail == 'False') {
					$primaryid = $this->Crudmodel->datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable);
					$primaryname = $this->Crudmodel->primaryinfo($partablename);
					if($girddatainfo && count($girddatainfo)>0 && $girddatainfo!= '') {
						$gresult = $this->Crudmodel->griddatainsert($primaryname,$gridpartablename,$girddatainfo,$gridrows,$primaryid);
				}
			} else {
				$this->Crudmodel->dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$checkemail,$restricttable);
			}
		} else {
			$checkmob = $this->mobilenumbercheck($mobilenumber,$templatetypeid);
			if($checkmob == 'False') {
				$primaryid = $this->Crudmodel->datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable);
				$primaryname = $this->Crudmodel->primaryinfo($partablename);
				if($girddatainfo && count($girddatainfo)>0 && $girddatainfo!= '') {
					$gresult = $this->Crudmodel->griddatainsert($primaryname,$gridpartablename,$girddatainfo,$gridrows,$primaryid);
				}
			} else {
				$this->Crudmodel->dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$checkmob,$restricttable);
			}
		}		
		echo 'TRUE';
	}
	//Email Id check if already available or not
	public function emailidcheck($emailid){
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('subscribersid');
		$this->db->from('subscribers');
		$this->db->where('subscribers.emailid',$emailid);
		$this->db->where("FIND_IN_SET('$industryid',subscribers.industryid) >", 0);
		$this->db->where('subscribers.status',1);
		$this->db->group_by('subscribers.emailid');
		$this->db->order_by('subscribers.subscribersid','asc');
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = $row->subscribersid;
			}
			return $data;
		} else {
			return "False";
		}
	}
	//mobile number check if already available or not
	public function mobilenumbercheck($mobilenumber,$templatetypeid){
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('subscribersid');
		$this->db->from('subscribers');
		$this->db->where('subscribers.mobilenumber',$mobilenumber);
		$this->db->where("FIND_IN_SET('$industryid',subscribers.industryid) >", 0);
		$this->db->where('subscribers.status',1);
		$this->db->where('subscribers.templatetypeid',$templatetypeid);
		$this->db->group_by('subscribers.mobilenumber');
		$this->db->order_by('subscribers.subscribersid','asc');
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = $row->subscribersid;
			}
			return $data;
		} else {
			return "False";
		}
	}
	public function informationfetchmodel($moduleid) {
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementspartabname']);
		$partablename = $this->Crudmodel->filtervalue($elementpartable);
		$fieldstable = $this->Crudmodel->filtervalue($formfieldstable);
		$primaryid = $_GET['dataprimaryid'];
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$fieldstable,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	public function datainformationupdatemodel() {
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		$partablename = $this->Crudmodel->filtervalue($elementpartable);
		$fieldstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fieldstable);
		$primaryid = $_POST['primarydataid'];
		$restricttable = explode(',',$_POST['resctable']);
		$mobilenumber = $_POST['mobilenumber'];
		$emailid = $_POST['emailid'];
		$templatetypeid = $_POST['templatetypeid'];
		$this->Crudmodel->dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable);
		echo 'TRUE';
	}
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primarydataid'];
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				echo 'TRUE';
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			echo 'TRUE';
		}
	}
	//module drop down load function 
	public function moduledropdownloadfunmodel() {
		$moduleid = array('4','201','202','203',);
		$modulename = array('User','Leads','Accounts','Contacts');
		$count = count($moduleid);
		if($count != 0){
			for($i=0;$i<$count;$i++){
				$data[$i] = array('datasid'=>$moduleid[$i],'dataname'=>$modulename[$i]);
			}
			echo json_encode($data);
		} else {
			 echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//view drop down load function 
	public function viewdropdownloadfunmodel() {
		$moduleid = $_GET['moduleid'];
		$userid = $this->Basefunctions->userid;
		$i = 0;
		$this->db->select('viewcreationid,viewcreationname');
		$this->db->from('viewcreation');
		$this->db->where('viewcreation.viewcreationmoduleid',$moduleid);
		$this->db->where_in('viewcreation.createuserid',array(1,$userid));
		$this->db->where('viewcreation.status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->viewcreationid,'dataname'=>$row->viewcreationname);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//parent table name fetch
	public function parenttablegetmodel(){
		$mid = $_GET['moduleid'];
		if($mid == '1'){$mid = '247';}
		$this->db->select('modulemastertable');
		$this->db->from('module');
		$this->db->where('module.moduleid',$mid);
		$result = $this->db->get();
		if($result->num_rows() >0){
			foreach($result->result() as $row){
				$data = $row->modulemastertable;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//from application form data add
	public function fromappsubscriberaddmodel() {
		$groupid = $_POST['smsgroupsid'];
		$leadid = $_POST['leadid'];
		$contactid = $_POST['contactid'];
		$userid = $_POST['userid'];
		$accountid = $_POST['accountid'];
		$fromtypeid = $_POST['fromapptemplatetypeid'];
		if($fromtypeid == 5){
			$getdata = 'emailid';
		} else {
			$getdata = 'mobilenumber';
		}
		$mobilenum = array();
		$j=0;
		$firstname = array();
		$lastname = array();
		$lid = array();
		$leadexp = array();
		if($leadid != '') {
			$leadexp = explode('|',$leadid);
			$leadinvite = explode(',',$leadexp[1]);
			$lcount = count($leadinvite);
			for($i=0;$i<$lcount;$i++) {
				$leademailid = $this->Basefunctions->generalinformaion('lead',$getdata,'leadid',$leadinvite[$i]);
				$leadfname = $this->Basefunctions->generalinformaion('lead','leadname','leadid',$leadinvite[$i]);
				$leadlname = $this->Basefunctions->generalinformaion('lead','lastname','leadid',$leadinvite[$i]);
				if($leademailid != '') {
					$emailid[$j] = $leademailid;
					$firstname[$j] = $leadfname;
					$lastname[$j] = $leadlname;
					$lid[$j] = $leadinvite[$i];
					$j++;
				}
			}
			//data insert
			if(!empty($emailid)){
				$this->fromapplicationinsertion($fromtypeid, $emailid,$firstname,$lastname,$groupid,'201',$lid);
			}
		}
		$contexp =array();
		if($contactid != '') {
			$cemailid =array();
			$cfirstname =array();
			$clastname =array();
			$contexp = explode('|',$contactid);
			$continvite = explode(',',$contexp[1]);
			$ccount = count($continvite);
			$cid = array();
			$k=0;
			for($i=0;$i<$ccount;$i++) {
				$contemailid = $this->Basefunctions->generalinformaion('contact',$getdata,'contactid',$continvite[$i]);
				$contfname = $this->Basefunctions->generalinformaion('contact','contactname','contactid',$continvite[$i]);
				$contlname = $this->Basefunctions->generalinformaion('contact','lastname','contactid',$continvite[$i]);
				if($contemailid != '') {
					$cemailid[$k] = $contemailid;
					$cfirstname[$k] = $contfname;
					$clastname[$k] = $contlname;
					$cid[$k] = $continvite[$i];
					$k++;
				}
			}
			//data insert
			if(!empty($cemailid)){
				$this->fromapplicationinsertion($fromtypeid, $cemailid,$cfirstname,$clastname,$groupid,'203',$cid);
			}
		}
		$userexp =array();
		if($userid != '') {
			$uemailid =array();
			$ufirstname =array();
			$ulastname =array();
			$userexp = explode('|',$userid);
			$userinvite = explode(',',$userexp[1]);
			$m=0;
			$uid = array();
			$ucount = count($userinvite);
			for($i=0;$i<$ucount;$i++) {
				$useremailid = $this->Basefunctions->generalinformaion('employee',$getdata,'employeeid',$userinvite[$i]);
				$userfname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$userinvite[$i]);
				$userlname = $this->Basefunctions->generalinformaion('employee','lastname','employeeid',$userinvite[$i]);
				if($useremailid != '') {
					$uemailid[$m] = $useremailid;
					$ufirstname[$m] = $userfname;
					$ulastname[$m] = $userlname;
					$uid[$m] = $userinvite[$i];
					$m++;
				}
			}
			//data insert
			if(!empty($uemailid)){
				$this->fromapplicationinsertion($fromtypeid, $uemailid,$ufirstname,$ulastname,$groupid,'4',$uid);
			}
		}
		$accoutexp =array();
		if($accountid != '') {
			$aemailid =array();
			$afirstname =array();
			$alastname =array();
			$accoutexp = explode('|',$accountid);
			$accinvite = explode(',',$accoutexp[1]);
			$m=0;
			$aid = array();
			$acount = count($accinvite);
			for($i=0;$i<$acount;$i++) {
				$accemailid = $this->Basefunctions->generalinformaion('account',$getdata,'accountid',$accinvite[$i]);
				$accfname = $this->Basefunctions->generalinformaion('account','accountname','accountid',$accinvite[$i]);
				$acclname = '';
				if($accemailid != '') {
					$aemailid[$m] = $accemailid;
					$afirstname[$m] = $accfname;
					$alastname[$m] = $acclname;
					$aid[$m] =$accinvite[$i];
					$m++;
				}
			}
			//data insert
			if(!empty($aemailid)){
				$this->fromapplicationinsertion($fromtypeid, $aemailid,$afirstname,$alastname,$groupid,'202',$aid);
			}
		}
		$moduleid =array();
		if(!empty($leadexp)){
			$leadexpo = $leadexp[0].',';
		} else {
			$leadexpo = '';
		}
		if(!empty($contexp)){
			$contexpo = $contexp[0].',';
		} else {
			$contexpo = '';
		}
		if(!empty($userexp)){
			$userexpo = $userexp[0].',';
		} else {
			$userexpo = '';
		}
		if(!empty($accoutexp)){
			$accexpo = $accoutexp[0];
		} else {
			$accexpo = '';
		}				
		$moduleid = $leadexpo.''.$contexpo.''.$userexpo.''.$accexpo;
		$mid = trim($moduleid,',');
		{//sms group module update 
			$groupmodule = array('moduleid'=>$mid, 'templatetypeid' => $fromtypeid);
			$this->db->where('campaigngroups.campaigngroupsid',$groupid);	
			$this->db->update('campaigngroups',$groupmodule);	
		} 
		echo "TRUE";
	}
	//insertion
	public function fromapplicationinsertion($typeid, $emailid,$firstname,$lastname,$groupid,$moduleid,$recordid) {
		$defvalue = $this->Crudmodel->defaultvalueget();
		$industryid = $this->Basefunctions->industryid;
		$count = count($emailid);
		for($i=0;$i<$count;$i++) {
			if($typeid != '5') {
				$mobile = 'mobilenumber';
			} else {
				$mobile = 'emailid';
			}
			$subscriber = array(
				'campaigngroupsid'=>$groupid,
				'templatetypeid'=>$typeid,
				'permissiongranted'=>'Yes',
				'lastname'=>$lastname[$i],
				'firstname'=>$firstname[$i],
				$mobile=>$emailid[$i],
				'smsgrouptypeid'=>'3',
				'moduleid'=>$moduleid,
				'recordid'=>$recordid[$i],
				'industryid'=>$industryid,
				'branchid'=>$this->Basefunctions->branchid,
				);
			$application = array_merge($subscriber,$defvalue);
			$this->db->insert('subscribers',$application);
		}
	}
	//sms group dd data fetch on add
	public function smsgroupdddatafetchmodel() {
		$process = $_GET['process'];
		if($process == 'add') {
			$grptype = array(2,4);
		} else if($process == 'edit') {
			$grptype = array(2,3,4);
		}
		$i=0;
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('campaigngroupsid,campaigngroupsname');
		$this->db->from('campaigngroups');
		$this->db->where_in('campaigngroups.smsgrouptypeid',$grptype);
		$this->db->where("FIND_IN_SET('$industryid',campaigngroups.industryid) >", 0);
		$this->db->where('campaigngroups.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row ) {
				$data[$i] = array('datasid'=>$row->campaigngroupsid,'dataname'=>$row->campaigngroupsname);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//sms group type id get
	public function smsgroupidgetmodel() {
		$groupid = $_GET['groupid'];
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('smsgrouptypeid');
		$this->db->from('campaigngroups');
		$this->db->where_in('campaigngroups.campaigngroupsid',array($groupid));
		$this->db->where("FIND_IN_SET('$industryid',campaigngroups.industryid) >", 0);
		$this->db->where('campaigngroups.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row ) {
				$data = $row->smsgrouptypeid;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	
	//group name dd data fetch
	public function groupnameddfetchmodel() {
		$typeid = $_GET['typeid'];
		$industryid = $this->Basefunctions->industryid;
		if($typeid) {
			$i=0;
			$this->db->select('campaigngroupsid,campaigngroupsname');
			$this->db->from('campaigngroups');
			$this->db->where_in('campaigngroups.templatetypeid',$typeid);
			if($typeid != 3){
				if(isset($_GET['app'])){
					$this->db->where('campaigngroups.smsgrouptypeid',3);
				}else{
					$this->db->where_in('campaigngroups.smsgrouptypeid',array('2', '4'));
				}
			}
			$this->db->where("FIND_IN_SET('$industryid',campaigngroups.industryid) >", 0);
			$this->db->where('campaigngroups.status',1);
			$result = $this->db->get();
			if($result->num_rows() > 0){
				foreach($result->result() as $row ) {
					$data[$i] = array('datasid'=>$row->campaigngroupsid,'dataname'=>$row->campaigngroupsname);
					$i++;
				}
				echo json_encode($data);
			} else {
				echo json_encode(array("fail"=>'FAILED'));
			}
		}else{
			echo json_encode(array("fail"=>'NODATA'));
		}
	}
	/* fetch grid data information dynamic view //for view */
	public function smssubsciberviewdynamicdatainfofetch($creationid,$primaryid,$viewcolmoduleids,$pagenum,$rowscount,$sortcol,$sortord,$filter,$type = NULL) {
		$whereString="";
		$groupid="";
		$filtercondvalue = "";
		//grid column title information fetch
		$excolinfo = $this->Basefunctions->gridinformationfetchmodel($creationid,$viewcolmoduleids);
		//filter condition
		if($filter == 'undefined') {$filter='';}
		if(!empty($filter)) {
			$filterdata = explode(",",$filter);
			foreach($filterdata as $conditiondata) {
				$fdate = explode('|',$conditiondata);
				$conditonid[] =  $fdate[0];
				$filtercondition[] =  $fdate[1];
				if($viewcolmoduleids == 214 && $fdate[2] == 'COMMA ( # )') {
					$filtervalue[] =  str_replace("COMMA ( , )","COMMA ( # )",$fdate[2]);
				} else {
					$filtervalue[] =  $fdate[2];
				}
			}
		} else {
			$conditonid = array();
			$filtercondition = array();
			$filtervalue = array();
		}
		$colinfo = $this->Basefunctions->filtergridinformationfetchmodel($creationid,$conditonid);
		//header colids
		$headcolids = $this->Basefunctions->mobiledatarowheaderidfetch($creationid);
		//view moduleid
		$viewmodid = $this->Basefunctions->mobiledataviewmoduleidfetch($creationid);
		//main table
		$maintable = $_GET['maintabinfo'];
		$selvalue = $maintable.'.'.$primaryid;
		$in = 0;
		//generate joins
		$counttable=0;
		if(count($colinfo)>0) {
			$counttable=count($colinfo['coltablename']);
		}
		$jtab = array($maintable);
		$ptab = array($maintable);
		$joinq = "";
		//attribute where condition
		$attrcond = "";
		$attrcondarr = array();
		$x = 0;
		$con = "";
		for($k=0;$k<$counttable;$k++) {
			if($x != 0) { $con = "AND"; }
			//parent join check
			$ptabjoin = "";
			$ptabjoin = $colinfo['colmodelparenttable'][$k];
			if(in_array($ptabjoin,$ptab)) {
				if(!in_array($colinfo['coltablename'][$k],$jtab) && $colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
					array_push($jtab,trim($colinfo['coltablename'][$k]));
					//employee
					$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid LIKE "%1%"';
					//group
					$joinq .= ' LEFT OUTER JOIN employeegroup ON FIND_IN_SET(employeegroup.employeegroupid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid  LIKE "%2%"';
					//user role and sub ordinate
					$joinq .= ' LEFT OUTER JOIN userrole ON FIND_IN_SET(userrole.userroleid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND ('.$maintable.'.employeetypeid LIKE "%3%" OR '.$maintable.'.employeetypeid LIKE "%4%")';
				} else {
					if($colinfo['colmodeldatatype'][$k] == 1) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
							}
						}
					} else if($colinfo['colmodeldatatype'][$k] == 2) {
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else if($colinfo['colmodeldatatype'][$k] == 3) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
							}
						}
					}
				}
			} else {
				if($colinfo['colmodeldatatype'][$k] == 1) {
					$result = $this->Basefunctions->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status NOT IN (0,3)';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status NOT IN (0,3)';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
							}
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.','.$maintable.'.'.$primaryid.') > 0 AND '.$ptabjoin.'.status NOT IN (0,3)';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 1) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
									}
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 2) {
					$joinresult = $this->Basefunctions->parentjointableinfo($ptabjoin,$viewcolmoduleids);
					if($joinresult->num_rows() >0) {
						foreach($joinresult->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
							//condition name
							$condcolname = $show->viewcreationcolmodelcondname;
							//condition value
							$condcolvalue = $show->viewcreationcolmodelcondvalue;
						}
						//restrict repeated parent join
						if(in_array($destab,$ptab)) {
							if($destab != $soucrcetab) {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status NOT IN (0,3)';
								}
							} else {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status NOT IN (0,3)';
								}
							}
						}
						//parent table condition
						if($condcolname != "" && $condcolvalue != "") {
							$attrcond .= " ".$con." ".$srcjointab.'.'.$condcolname."=".$condcolvalue;
							array_push($attrcondarr,$srcjointab.'.'.$joincolname);
							$x = 1;
						}
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] = $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.'='.$maintable.'.'.$primaryid.') > 0 AND '.$ptabjoin.'.status NOT IN (0,3)';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 2) {
								//attribute join
								if(!in_array($colinfo['coltablename'][$k],$jtab)) {
									array_push($jtab,trim($colinfo['coltablename'][$k]));
									$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
								}
								//condition
								if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
									$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
									array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
									$x = 1;
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 3) {
					$result = $this->Basefunctions->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status NOT IN (0,3)';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status NOT IN (0,3)';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
							}
						}
					}  else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.','.$maintable.'.'.$primaryid.') > 0 AND '.$ptabjoin.'.status NOT IN (0,3)';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 3) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
									}
								}
							}
						}
					}
				}
			}
			$dduitypeids = array('17','18','19','23','25','26','27','28','29');
			//fields fetch
			if($colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->Basefunctions->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].', GROUP_CONCAT(DISTINCT userrole.userrolename) AS rolename, GROUP_CONCAT(DISTINCT employeegroup.employeegroupname) AS empgrpname';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k].',userrole.userrolename AS rolename,employeegroup.employeegroupname AS empgrpname';
				}
			} else if( in_array($colinfo['colmodeluitype'][$k],$dduitypeids) ) {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->Basefunctions->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].'';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
				}
				if($conditonid) {
					if( in_array($colinfo['colid'][$k],$conditonid) ) {
						foreach($conditonid as $nfkey => $nfvalue) {
							if($nfvalue == $colinfo['colid'][$k]) {
								$filterconditionid[] = $filtercondition[$nfkey];
								$filtervalueid[] = $filtervalue[$nfkey];
							}
						}
						if($colinfo['colmodeluitype'][$k] == 25) {
							$fcond = explode(' as ',$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k]);
							$fkey = array_search($colinfo['colid'][$k], $conditonid);
							$filtercondvalue .= $this->Basefunctions->filterconditiongenerate($filterconditionid,$filtervalueid,$fcond[0]);
						} else{
							$fkey = array_search($colinfo['colid'][$k], $conditonid);
							$filtercondvalue .= $this->Basefunctions->filterconditiongenerate($filterconditionid,$filtervalueid,$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k]);
						}
					}
				}
			} else {
				$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
				if($conditonid) {
					if( in_array($colinfo['colid'][$k],$conditonid, true) ) {
						foreach($conditonid as $nfkey => $nfvalue) {
							if($nfvalue == $colinfo['colid'][$k]) {
								$filterconditionid[] = $filtercondition[$nfkey];
								$filtervalueid[] = $filtervalue[$nfkey];
							}
						}
						$fkey = array_search($colinfo['colid'][$k], $conditonid);
						$adddata = explode(' as',$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k]);
						if(count($adddata)>0){
							$filtercondvalue .= $this->Basefunctions->filterconditiongenerate($filterconditionid,$filtervalueid,$adddata[0]);
						} else{
							$filtercondvalue .= $this->Basefunctions->filterconditiongenerate($filterconditionid,$filtervalueid,$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k]);
						}
					}
				}
			}
		}
		//attr condition generate
		for($xi=0; $xi < ( count($attrcondarr)-1 ); $xi++ ) {
			$attrcond .= " AND ".$attrcondarr[0].'='.$attrcondarr[$xi+1];
		}
		if( $attrcond == "" ) { $attrcond = "1=1";}
		//custom condition generate
		$cuscondition = " AND 1=1";
		if( isset($_GET['cuscondfieldsname']) ) {
			$fieldsname = explode(',',$_GET['cuscondfieldsname']);
			if (strpos($_GET['cuscondvalues'], '|') !== false) {
				$condvalues = str_replace("|",",",explode(',',$_GET['cuscondvalues']));
			}
			else
			{
				$condvalues = explode(',',$_GET['cuscondvalues']);
			}
			if($_GET['cuscondtablenames']!=''){$condtables = explode(',',$_GET['cuscondtablenames']);}
			else{$condtables='';}
			if($_GET['cusjointableid']!=''){$condjoinid = explode(',',$_GET['cusjointableid']);}
			else{$condjoinid ='';}
			if(isset($_GET['conditionoperator'])){$conditionoperator = explode(',',$_GET['conditionoperator']);}
			$m=0;
			foreach($fieldsname AS $values) {
				if($condvalues[$m] != "") {
					if($condtables!="" && in_array($condtables[$m],$ptab)) {
						$tabname = explode('.',$values);
						//restrict repeated join
						if(!in_array($condtables[$m],$jtab)) {
							array_push($jtab,$condtables[$m]);
							$joinq=$joinq.' LEFT OUTER JOIN '.$condtables[$m].' ON '.$tabname[0].'.'.$condjoinid[$m].'='.$maintable.'.'.$condjoinid[$m];
						}
					}
					if($conditionoperator=='')
					{$cuscondition .= " AND ".$values."=".$condvalues[$m];
					}
					else
					{$cuscondition .= $this->Basefunctions->filterconditiongenerate($conditionoperator[$m],$condvalues[$m],$values);
					}
					$m++;
				}
			}
		}
		$conditionvalarray = $this->Basefunctions->conditioninfofetch($creationid);
		$c = count($conditionvalarray);
		if($c > 0) {
			$whereString=$this->Basefunctions->conditionalviewwhereclausegeneration($conditionvalarray);
		} else {
			$whereString="";
		}
		$status = $maintable.'.status NOT IN (0,3)';
		$actsts = $this->Basefunctions->activestatus;
		/* fetch module rules based employee list[apply rule] */
		$moduleid = explode(',',$viewcolmoduleids);
		$ruleid =  $this->Basefunctions->moduleruleidfetch($moduleid[0]);
		$cusruledata = $this->Basefunctions->customrulefetch($moduleid[0]);
		$roleid = $this->Basefunctions->userroleid;
		$userid = $this->Basefunctions->userid;
		$custrulecond = " ";
		$rulecond="";
		if($ruleid == 0) {
			$assignemp = $this->Basefunctions->checkassigntofield($maintable,'employeetypeid');
			$empids = $this->Basefunctions->subrolesempidfetch($roleid);
			$subempid = implode(',',$empids);
			$empid = ( ($subempid!="")? $subempid.','.$userid : $userid);
			if($assignemp == 'true') {
				/* custom rule */
				if( sizeof($cusruledata) > 0 ) {
					$custrulecond = " AND ( ".$maintable.".createuserid IN (".$empid.") OR ( ";
					foreach($cusruledata as $key => $customrule) {
						if($customrule['totypeid'] == '2') { //group
							$custrulecond .= "(".$maintable.".employeetypeid=".$customrule['totypeid']." AND ".$maintable.".employeeid=".$customrule['sharedto'].")";
							$custrulecond .= ' OR ';
						} else if( ($customrule['totypeid'] == '3' && $customrule['sharedto'] == $roleid) ) { //roles
							$custrulecond .= "(".$maintable.".employeetypeid=".$customrule['totypeid']." AND ".$maintable.".employeeid=".$customrule['sharedto'].")";
							$custrulecond .= ' OR ';
						} else if( $customrule['totypeid'] == '4' ) { //role and subordinate
							$roleids = $this->Basefunctions->roleididfetchfromrolesandsubroles($customrule['sharedto']);
							$usrroleids = implode(',',$roleids);
							$usrroleids = ( ($usrroleids!='')? $usrroleids:1);
							$custrulecond .= "(".$maintable.".employeetypeid=".$customrule['totypeid']." AND ".$maintable.".employeeid IN (".$usrroleids.") )";
							$custrulecond .= ' OR ';
						}
					}
					$custrulecond .= "(".$maintable.".employeetypeid=1 AND ".$maintable.".employeeid=".$this->Basefunctions->userid.")";
					$custrulecond .= " ) )";
				} else {
					$roleids = $this->Basefunctions->roleididfetchfromrolesandsubroles($roleid);
					$usrroleids = implode(',',$roleids);
					$usrroleids = ( ($usrroleids!='')? $usrroleids:1);
					$usrgrpid =  $this->Basefunctions->groupididfetchuserid($userid);
					$usergroupids = implode(',',$usrgrpid);
					$usergroupids = ( ($usergroupids!='')? $usergroupids:1);
					$rulecond = ( ($empid != '')? ' AND ( '.$maintable.'.createuserid IN ('.$empid.') OR ('.$maintable.'.employeetypeid=1 AND '.$maintable.'.employeeid='.$this->Basefunctions->userid.') OR ('.$maintable.'.employeetypeid=3 AND '.$maintable.'.employeeid='.$roleid.') OR ('.$maintable.'.employeetypeid=4 AND '.$maintable.'.employeeid IN ('.$usrroleids.')) OR ('.$maintable.'.employeetypeid=2 AND '.$maintable.'.employeeid IN ('.$usergroupids.')) )' : ' AND 1=1' );
				}
			} else {
				$rulecond = ( ($empid != '')? ' AND '.$maintable.'.createuserid IN ('.$empid.')' : ' AND 1=1' );
			}
		}
		/* column sorting */
		$order = '';
		if($sortcol!='' && $sortord!='') {
			$order = 'ORDER BY'.' '.$sortcol.' '.$sortord;
		} else {
			$order = 'ORDER BY'.' '.$maintable.'.'.$maintable.'id DESC';
		}
		/* Pagination */
		$page = $pagenum-1;
		$start = $page * $rowscount;
		$li = ' LIMIT '.$start.','.$rowscount;
		if($type == "3"){
			if($maintable == "account" || $maintable == "contact"){
				$dncall = 'AND '.$maintable.'.donotcall <> "Yes"';
			}else{
				$dncall = 'AND 1=1';
			}
		}else{
			$dncall = 'AND 1=1';
		}
		if($type == "5"){
			if($maintable == "account" || $maintable == "contact"){
				$dnemail = 'AND '.$maintable.'.donotemail <> "Yes"';
			}else{
				$dnemail = 'AND 1=1';
			}
		}else{
			$dnemail = 'AND 1=1';
		}
		/* query statements */
		$query = 'select SQL_CALC_FOUND_ROWS '.$selvalue.' from '.$maintable.' '.$joinq.' WHERE '.$status.' AND '.$attrcond.' '.$whereString.''.$cuscondition.''.$rulecond.''.$custrulecond.''.$filtercondvalue.' GROUP BY '.$maintable.'.'.$maintable.'id '.$order.$li;
		$data = $this->db->query($query);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		/* footer generate */
		$footerdata = array();
		$finalresult=array($excolinfo,$data,$page,$footerdata,$headcolids,$viewmodid,$creationid);
		return $finalresult;
	}
	// Unique Email id while edit
	public function uniquedynamicviewemailidcheckmodel(){
		$industryid = $this->Basefunctions->industryid;
		$emailid = $_POST['emailid'];
		if($emailid != "") {
			$result = $this->db->select('subscribers.subscribersid')->from('subscribers')->where('subscribers.emailid',$emailid)->where("FIND_IN_SET('$industryid',subscribers.industryid) >", 0)->where_not_in('subscribers.status',0)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->subscribersid;
				}
			} else {
				echo "False";
			}
		} else {
			echo "False";
		}
	}
	// Unique Mobile Number while edit
	public function uniquedynamicviewmobnocheckmodel(){
		$industryid = $this->Basefunctions->industryid;
		$templatetypeid = $_POST['templatetypeid'];
		$mobilenumber = $_POST['mobilenumber'];
		if($mobilenumber != "") {
			$result = $this->db->select('subscribers.subscribersid')->from('subscribers')->where('subscribers.templatetypeid',$templatetypeid)->where('subscribers.mobilenumber',$mobilenumber)->where("FIND_IN_SET('$industryid',subscribers.industryid) >", 0)->where_not_in('subscribers.status',0)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->subscribersid;
				}
			} else {
				echo "False";
			}
		} else {
			echo "False";
		}
	}
}