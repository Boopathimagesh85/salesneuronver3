<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Campaign extends MX_Controller{
    //Constructor Function To Load Models
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Campaign/Campaignmodel');
	}
	//Default View 
	public function index() {
		$moduleid = array(213);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(213);
		$viewmoduleid = array(213);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['dashboradviewdata'] = $this->Basefunctions->dashboarddataviewbydropdown($moduleid);
        $this->load->view('Campaign/campaignview',$data);
	}
	//create Campaign
	public function newdatacreate() {
    	$this->Campaignmodel->newdatacreatemodel();
    }
	//information fetch
	public function fetchformdataeditdetails() {
		$moduleid = array(213);
		$this->Campaignmodel->informationfetchmodel($moduleid);
	}
	//update Campaign
    public function datainformationupdate() {
        $this->Campaignmodel->datainformationupdatemodel();
    }
	//delete Campaign
    public function deleteinformationdata() {
    	$moduleid = array(213);
		$this->Campaignmodel->deleteoldinformation($moduleid);
    }
    //currency conversion dd load
    public function currencyddload() {
    	$this->Campaignmodel->currencyddloadmodel();
    }
}