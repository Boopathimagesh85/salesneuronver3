<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Receivesms extends MX_Controller{
	// constructor function 
    public function __construct() {
        parent::__construct();
        $this->load->helper('formbuild');
		$this->load->model('Receivesms/Receivesmsmodel');
		$this->load->view('Base/formfieldgeneration');
		$this->load->library('convert');
    }
	public function index() {
		$moduleid = array(274);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(274);
		$viewmoduleid = array(274);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Receivesms/receivesmsview',$data);
	}
}