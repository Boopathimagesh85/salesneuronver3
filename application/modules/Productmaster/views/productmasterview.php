<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
</head>
<body>
<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<?php
				if($industryid == 3) {
					$modid = '53,54,8';
				} else {
					$modid = '11,12,15';
				}
				$dataset['gridtitle'] = $gridtitle['title'];
				$dataset['titleicon'] = $gridtitle['titleicon'];
				$dataset['formtype'] = 'multipleaddform';
				$dataset['multimoduleid'] = $modid;
				$this->load->view('Base/mainviewaddformheader',$dataset);
			?>
			<div class="large-12 columns scrollbarclass paddingzero productmastertouch ">
				<div class="mastermodules">
				<?php
					
					//function call for dash board form fields generation
					dashboardformfieldsgeneratorwithgrid($moddbfrmfieldsgen,1,$modid);
				?>			
				</div>
			</div>			
		</div>
	</div>
</div>
<?php
	$device = $this->Basefunctions->deviceinfo();
	if($industryid == 3){
		echo hidden('meltinground',$melting_round);
	}
	if($device=='phone') {
		$this->load->view('Base/overlaymobile');
	} else {
		$this->load->view('Base/overlay');
	}
	$this->load->view('Base/modulelist');
?>	
	<div class="large-6 columns" style="position:absolute;">
		<div class="overlay" id="metalpurityoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">	
			<div class="row" style="padding-top:180px;">&nbsp;</div>
			<form id="itemrateoverlay" name="itemrateoverlay" class="">
			<div class="alert-panel">
				<div class="alertmessagearea">
					<div class="alert-title">Purity Set</div>
					<div class="alert-message">
						<span class="firsttab" tabindex="1000"></span>
						<div class="static-field overlayfield">             
							<label>Purity</label>
							<select id="metal_purity" name="metal_purity" class="chzn-select ffieldd" data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="1001">
							</select>									       
						</div>
					</div>
				</div>
				<div class="alertbuttonarea">
					<input type="button" id="metalpuirty" name="" value="Submit" class="alertbtn" tabindex="1002">	
					<input type="button" id="metalpuirtyclose" name="" value="Cancel" tabindex="1003" class="alertbtn flloop  alertsoverlaybtn" >
					<span class="lasttab" tabindex="1004"></span>
				</div>
			</div>
			</form>
		</div>
	</div>
</body>
	<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<?php $this->load->view('Base/bottomscript'); ?>
	<script src="<?php echo base_url();?>js/plugins/treemenu/modernizr.custom.min.js" type="text/javascript"></script>  
	<script>
		//Tabgroup Dropdown More
		// fwgautocollapse();
		$(function() {
			$('#dl-menu').dlmenu({
				animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
			});
		});		
		if(softwareindustryid != 3){
			$("#tab1").click(function()	{
				$('#branddataupdatesubbtn').hide();
				$('#brandreloadicon,#brandsavebutton,#branddeleteicon').show();
				$(".tabclass").addClass('hidedisplay');
				$("#tab1class").removeClass('hidedisplay');
				setTimeout(function(){ brandaddgrid(); },10);
				resetFields(); mastertabid=1; masterfortouch = 0;
			});
		}		
		if(softwareindustryid != 3){
			$("#tab2").click(function()	{
				$('#attributedataupdatesubbtn').hide();
				$('#attributereloadicon,#attributesavebutton,#attributedeleteicon').show();
				$(".tabclass").addClass('hidedisplay');
				$("#tab2class").removeClass('hidedisplay');
				setTimeout(function(){ attributeaddgrid(); },10);
				resetFields(); mastertabid=3; masterfortouch = 0;
			});	
			$("#tab3").click(function()	{
				$('#attributesetassigndataupdatesubbtn').hide();
				$('#attributesetassignreloadicon,#attributesetassignsavebutton,#attributesetassigndeleteicon').show();	
				$(".tabclass").addClass('hidedisplay');
				$("#tab3class").removeClass('hidedisplay');		
				dynamicloaddropdown('newattributeid','newattributeid','attributename','attributeid','attribute','','');
				setTimeout(function(){ attributesetassignaddgrid(); },10);
				resetFields(); mastertabid=6; masterfortouch = 0;
			});	
		}else{
			$("#tab1").click(function()	{
				$('#metaldataupdatesubbtn').hide();
				$('#metalreloadicon,#metalsavebutton,#metaldeleteicon').show();
				$(".tabclass").addClass('hidedisplay');
				$("#tab1class").removeClass('hidedisplay');
				resetFields(); mastertabid=2; masterfortouch = 0;	
			});	
			$("#tab2").click(function()	{
				$('#puritydataupdatesubbtn').hide();
				$('#purityreloadicon,#puritysavebutton,#puritydeleteicon').show();
				$(".tabclass").addClass('hidedisplay');
				$("#tab2class").removeClass('hidedisplay');
				setTimeout(function(){ purityaddgrid(); },10);
				resetFields(); mastertabid=3; masterfortouch = 0;
			});
		}			
		$(document).ready(function(){
			/* $("#tabgropdropdown").change(function(){
				var tabgpid = $("#tabgropdropdown").val();
				$('.dbsidebaricons[data-dbsubform="'+tabgpid+'"]').trigger('click');
			}); */
			 {//for keyboard global variable
				 viewgridview = 0;
				 innergridview = 1;
			 }
		});
		$('#closeproductmasterform').click(function(){
			window.location =base_url+'Product';
		});		
	</script>
</html>
<?php
if($industryid == 3){
	echo hidden('maxcategorylevel',$categorylevel);
}
?>