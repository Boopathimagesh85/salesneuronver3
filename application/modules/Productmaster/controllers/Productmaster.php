<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Productmaster extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Productmaster/Productmastermodel');
    }
    //first basic hitting view
    public function index() {
    	$moduleid = array(231,97);
    	sessionchecker($moduleid);
    	//action
		$data['moddashboardtabgrp'] = $this->Basefunctions->moddashboardtabgrpgeneration($moduleid); 
		$data['moddbfrmfieldsgen'] = $this->Basefunctions->dashboardmodsecfieldsgeneration($moduleid); 
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['industryid'] = $this->Basefunctions->industryid;
		if($data['industryid'] == 3){
		   $data['melting_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',4); //melting round-off
		   $data['categorylevel'] = $this->Basefunctions->get_company_settings('category_level'); //category_level
		}
		$this->load->view('Productmaster/productmasterview',$data);
	}
}