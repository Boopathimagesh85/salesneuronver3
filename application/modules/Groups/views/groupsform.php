<style type="text/css">
.footer-contentnew .paging-box {
		left:-35% !important;
		position:relative;
}
</style>
<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<div id="groupsview" class="gridviewdivforsh">
				<?php
					$dataset['gridtitle'] = $gridtitle['title'];
					$dataset['titleicon'] = $gridtitle['titleicon'];
					$dataset['gridid'] = 'groupsaddgrid';
					$dataset['gridwidth'] = 'groupsaddgridwidth';
					$dataset['gridfooter'] = 'groupsaddgridfooter';
					$dataset['viewtype'] = 'disable';
					$dataset['moduleid'] = '246';
					$this->load->view('Base/singlemainviewformwithgrid.php',$dataset);
				?>
			</div>
			<div id="groupsaddformdiv" class="singlesectionaddform">
				<div class="large-12 columns addformunderheader">&nbsp;</div>
				<div class="large-12 columns scrollbarclass addformcontainer groupstouch">
				<div class="row mblhidedisplay">&nbsp;</div>
				<div id="subformspan1" class="hiddensubform transitionhiddenform">
				<div class="closed effectbox-overlay effectbox-default" id="groupsectionoverlay">
					<div class="large-12 columns mblnopadding">
					<form method="POST" name="usergroupcreateform" class="" action ="" id="usergroupcreateform"  style="height: 100% !important;">
						<span id="usergropaddform" class="validationEngineContainer" >
							<span id="usergropupdateform" class="validationEngineContainer">
								<div class="large-4 large-offset-8 columns" style="padding-right: 0em !important;height:100% !important;">
									<div id="effect" class="large-12 columns cleardataform z-depth-5" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff">
										<div class="large-12 columns sectionheaderformcaptionstyle">Basic Details</div>
										<div class="large-12 columns sectionpanel">
										<div class="input-field large-12 columns">
											<input type="text" class="validate[required,funcCall[groupnamecheck],maxSize[100]]" name="usergroupname" id="usergroupname" value="" tabindex="101"/>
											<label for="usergroupname">Group Name<span class="mandatoryfildclass">*</span></label>
										</div>
										<div class="input-field large-12 columns">
											<textarea id="userdescription" class="validate[maxSize[200]] materialize-textarea " data-prompt-position="topLeft" tabindex="102" name="userdescription"></textarea>
											<label for="userdescription">Description</label>
										</div>
										<div id="groupmembersdivhid" class="static-field large-12 columns">
											<label>Group Members<span class="mandatoryfildclass">*</span></label>
											<select class="validate[required] chzn-select dropdownchange" data-prompt-position="topLeft:14,36" name="ddgroupmembers" id="ddgroupmembers" multiple="multiple" data-placeholder="Select"  tabindex="103">
											<?php
												$ddowndata = $this->Basefunctions->userspecificgroupdropdownvalfetch();
												$prev = "";
												for ($i=0;$i<count($ddowndata);$i++) {
													$cur = $ddowndata[$i]['PId'];
													$a ="";
													if($prev != $cur) {
														if($prev != " ") {
															echo '</optgroup>';
														}
														echo '<optgroup label="'. $ddowndata[$i]['PName'].'" class="'.str_replace(' ','', $ddowndata[$i]['PName']).'">';
														$prev = $ddowndata[$i]['PId'];
														$a = "pclass";
													}
													echo '<option data-groupusersid ="'.$ddowndata[$i]['CId'].'" data-grouusertypeid="'.$ddowndata[$i]['PId'].'" value="'.$ddowndata[$i]['DDval'].'">'.$ddowndata[$i]['CName'].'</option>';
												}
											?>
											</select>
											<input name="groupmembers" type="hidden" id="groupmembers" value="U:1" />
										</div>
										</div>
										<div class="divider large-12 columns"></div>
										<div class="large-12 columns submitkeyboard sectionalertbuttonarea" style="text-align: right">
											<input id="usersubmitbtn" class="alertbtnyes addkeyboard" type="button" value="Save" name="usersubmitbtn" tabindex="103">
											<input id="userupdatebtn" class="alertbtnyes hidedisplay updatekeyboard" type="button" value="Save" name="userupdatebtn" tabindex="103" style="display: none;">
											<input class="alertbtnno addsectionclose cancelkeyboard" type="button" value="Close" name="cancel" tabindex="104">
										</div>
									</div>
								</div>
								<input type="hidden" name="primaryid" id="primaryid" value="" />
							</span>
						</span>
					</form>
					</div>
				</div>
				</div>
			</div>	
			</div>
		</div>
	</div>
</div>