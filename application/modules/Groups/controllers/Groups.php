<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Groups extends MX_Controller {
    //Constructor Function To Load Models
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Groups/Groupsmodel');
		$this->load->view('Base/formfieldgeneration');
	}
	//Default View 
	public function index() {
		$moduleid = array(246);
		sessionchecker($moduleid);
		//action
		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(246);
		$viewmoduleid = array(246);
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$this->load->view('Groups/groupsview',$data);	
	}
	//add form data
	public function groupsubmitdata() {
		$this->Groupsmodel->groupsubmitdatamodel();
	}
	//update form data
	public function groupupdatedata() {
		$this->Groupsmodel->groupupdatedatamodel();
	}
	//roles main grid view
	public function groupdatafetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'employeegroup.employeegroupid') : 'employeegroup.employeegroupid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('employeegroupname','employeename','employeegroupname','userrolename','userrolename','description','statusname'),'colmodelindex'=>array('employeegroup.employeegroupname','employee.employeename','employeegroup.employeegroupname','userrole.userrolename','userrole.userrolename','employeegroup.description','status.statusname'),'coltablename'=>array('employeegroup','employee','employeegroup','userrole','userrole','employeegroup','status'),'uitype'=>array('2','2','2','2','2','2','2'),'colname'=>array('Group Name','Users','Group','Roles','Role and Subordinates','Description','Status'),'colsize'=>array('150','200','200','200','200','200','100'));
		$result=$this->Groupsmodel->groupdatafetchgriddatafetch($primarytable,$sortcol,$sortord,$pagenum,$rowscount);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Groups',$width,$height);
		} else {
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Groups',$width,$height);
		}
		echo json_encode($datas);
	}
	//grop data delete
	public function gropdatadelete() {
		$this->Groupsmodel->gropdatadeletemodel();
	}
	//edit data information fetch
	public function groupeditdatainfofetch() {
		$this->Groupsmodel->groupeditdatainfofetchmodel();
	}
	//employee group drop down val fetch
	public function employeegropdropdownvalfetch() {
		$datas = $this->Basefunctions->userspecificgroupdropdownvalfetch();
		if(count($datas)>0) {
			echo json_encode($datas);
		} else {
			echo json_encode(array('status'=>'fail'));
		}
	}
	//Group unique name check
	public function groupuniquename() {
		$this->Groupsmodel->groupuniquenamemodel();
	}
}