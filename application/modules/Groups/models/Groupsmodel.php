<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Groupsmodel extends CI_Model
{
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//add new group
	public function groupsubmitdatamodel() {
		$usergroupname = $_POST['usergroupname'];
		$userdescription = $_POST['userdescription'];
		$groupmember = $_POST['groupmembers'];
		$grpmembers = explode(',',$groupmember);
		$empids = array();
		$groupids = array();
		$roleids = array();
		$rolesubroleids = array();
		$memberkey = array('$empids'=>'U','$groupids'=>'G','$roleids'=>'R','$rolesubroleids'=>'SR');
		foreach($grpmembers as $userval) {
			$dataset = explode('-',$userval);
			if($dataset[0] == 'U') {
				$empids[] = $dataset[1];
			} else if($dataset[0] == 'G') {
				$groupids[] = $dataset[1];
			} else if($dataset[0] == 'R') {
				$roleids[] = $dataset[1];
			} else if($dataset[0] == 'SR') {
				$rolesubroleids[] = $dataset[1];
			}
		}
		$cdate = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$status	= $this->Basefunctions->activestatus;
		$group = array(
			'employeegroupname'=>ucwords($usergroupname),
			'description'=>$userdescription,
			'employeeid'=>implode(',',$empids),
			'usergroupid'=>implode(',',$groupids),
			'userroleid'=>implode(',',$roleids),
			'userroleandsubordinateid'=>implode(',',$rolesubroleids),
			'industryid'=>$this->Basefunctions->industryid,
			'branchid'=>$this->Basefunctions->branchid,
			'createdate'=>$cdate,
			'lastupdatedate'=>$cdate,
			'createuserid'=>$userid,
			'lastupdateuserid'=>$userid,
			'status'=>$status
		);
		$this->db->insert('employeegroup',$group);
		echo "TRUE";
	}
	//update new group
	public function groupupdatedatamodel() {
		$id = $_POST['primaryid'];
		$usergroupname = $_POST['usergroupname'];
		$userdescription = $_POST['userdescription'];
		$groupmember = $_POST['groupmembers'];
		$grpmembers = explode(',',$groupmember);
		$empids = array();
		$groupids = array();
		$roleids = array();
		$rolesubroleids = array();
		$memberkey = array('$empids'=>'U','$groupids'=>'G','$roleids'=>'R','$rolesubroleids'=>'SR');
		foreach($grpmembers as $userval) {
			$dataset = explode('-',$userval);
			if($dataset[0] == 'U') {
				$empids[] = $dataset[1];
			} else if($dataset[0] == 'G') {
				$groupids[] = $dataset[1];
			} else if($dataset[0] == 'R') {
				$roleids[] = $dataset[1];
			} else if($dataset[0] == 'SR') {
				$rolesubroleids[] = $dataset[1];
			}
		}
		$cdate = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$status	= $this->Basefunctions->activestatus;
		$group = array(
			'employeegroupname'=>ucwords($usergroupname),
			'description'=>$userdescription,
			'employeeid'=>implode(',',$empids),
			'usergroupid'=>implode(',',$groupids),
			'userroleid'=>implode(',',$roleids),
			'userroleandsubordinateid'=>implode(',',$rolesubroleids),
			'createdate'=>$cdate,
			'lastupdatedate'=>$cdate,
			'createuserid'=>$userid,
			'lastupdateuserid'=>$userid,
			'status'=>$status
		);
		$this->db->where('employeegroupid',$id);
		$this->db->update('employeegroup',$group);
		echo "TRUE";
	}
	//group main grid view
	public function groupdatafetchgriddatafetch($tablename,$sortcol,$sortord,$pagenum,$rowscount) {
		$dataset = 'employeegroup.employeegroupid,employeegroup.employeegroupname,employeegroup.description,employeegroup.employeeid,employeegroup.usergroupid,employeegroup.userroleid,employeegroup.userroleandsubordinateid,status.statusname';
		$join=' LEFT OUTER JOIN employee ON employee.employeeid<>employeegroup.employeeid';
		$join.=' LEFT OUTER JOIN userrole ON userrole.userroleid<>employeegroup.userroleid';
		$join.=' LEFT OUTER JOIN status ON status.status=employeegroup.status';
		$status = $tablename.'.status IN (1,2)';
		$status .= ' AND '.$tablename.'.industryid IN ('.$this->Basefunctions->industryid.')';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		/* query */
		$result = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$status.' GROUP BY employeegroup.employeegroupid ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$data=array();
		$i=0;
		foreach($result->result() as $row) {
			$employeeid = $row->employeeid;
			$employeename = $this->Groupsmodel->employeenameget($employeeid);
			//user group id
			$groupid = $row->usergroupid;
			$roleid = $row->userroleid;
			$groupname = $this->Groupsmodel->groupnameget($groupid);
			$rolename = $this->Groupsmodel->rolenameget($roleid);
			$subrolename = $this->Groupsmodel->rolenameget($row->userroleandsubordinateid);
			$data[$i]=array('id'=>$row->employeegroupid,$row->employeegroupname,$employeename,$groupname,$rolename,$subrolename,$row->description,$row->statusname);
			$i++;
		}
		$finalresult=array('0'=>$data,'1'=>$page,'2'=>'json');
		return $finalresult;
	}
	//group data delete
	public function gropdatadeletemodel() {
		$id = $_GET['datarowid'];
		$check = $this->checkindatasharerule($id);
		if($check == 'FALSE') {
			$status = array('status'=>0);
			$this->db->where('employeegroup.employeegroupid',$id);
			$this->db->update('employeegroup',$status);
			echo "TRUE";
		} else {
			echo "Denied";
		}
		
	}
	public function checkindatasharerule($groupid) {
		$sfdata ='';
		$stdata = '';
		$this->db->select('recordsharedfrom');
		$this->db->from('datasharecustomrule');
		$this->db->where('datasharecustomrule.recordsharedfromtypeid',2);
		$this->db->where('datasharecustomrule.recordsharedfrom',$groupid);
		$sharedfro= $this->db->get();
		foreach($sharedfro->result() as $row) {
			$sfdata = $row->recordsharedfrom;
		}
		$this->db->select('recordsharedto');
		$this->db->from('datasharecustomrule');
		$this->db->where('datasharecustomrule.recordsharedtotypeid',2);
		$this->db->where('datasharecustomrule.recordsharedto',$groupid);
		$sharedto= $this->db->get();
		foreach($sharedto->result() as $row) {
			$stdata = $row->recordsharedto;
		}
		if($sfdata != '' || $stdata !=''){
			return 'TRUE';
		} else {
			return 'FALSE';
		}
	}
	//edit information fetch model
	public function groupeditdatainfofetchmodel() {
		$datas = "";
		$id = $_GET['datarowid'];
		$this->db->select('SQL_CALC_FOUND_ROWS employeegroup.employeegroupid,employeegroup.employeegroupname,employeegroup.description,employeegroup.employeeid,employeegroup.usergroupid,employeegroup.userroleid,employeegroup.userroleandsubordinateid,status.statusname',false);
		$this->db->from('employeegroup');
		$this->db->join('employee','employee.employeeid<>employeegroup.employeeid');
		$this->db->join('userrole','userrole.userroleid<>employeegroup.userroleid');
		$this->db->join('status','status.status=employeegroup.status');
		$this->db->where('employeegroup.employeegroupid',$id);
		$this->db->where_not_in('employeegroup.status',array(3,0));
		$this->db->group_by('employeegroup.employeegroupid');
		$result=$this->db->get();
		foreach($result->result() as $row) {
			$datas .= $this->groupvalformater('U',$row->employeeid,1);
			$datas .= $this->groupvalformater('G',$row->usergroupid,0);
			$datas .= $this->groupvalformater('R',$row->userroleid,0);
			$datas .= $this->groupvalformater('SR',$row->userroleandsubordinateid,0);
			$groupname = $row->employeegroupname;
			$groupdesc = $row->description;
		}
		$datasets = array('id'=>$datas,'name'=>$groupname,'desc'=>$groupdesc);
		echo json_encode($datasets);
	}
	//group values format generation
	public function groupvalformater($prefix,$ids,$check) {
		$datasets = "";
		$dataids = explode(',',$ids);
		$m=0;
		foreach($dataids as $value) {
			if($m == 0) {
				$datasets.=$prefix.'-'.$value;
				$m++;
			} else {
				$datasets.=','.$prefix.'-'.$value;
			}
		}
		if($datasets != "" && $check != '1') {
			$datasets =','.$datasets;
		}
		return $datasets;
	}
	//employee name get
	public function employeenameget($employeeid) {
		$i =0;
		$data = array();
		$empid = explode(',',$employeeid);
		$count = count($empid);
		for($j=0;$j<$count;$j++) {
			$this->db->select('employeename');
			$this->db->from('employee');
			$this->db->where_in('employee.employeeid',$empid[$j]);
			$this->db->where('employee.status',1);
			$result = $this->db->get();
			if($result->num_rows() > 0) {
				foreach($result->result() as $row) {
					$data[$i] = $row->employeename;
					$i++;
				}
			}
		}
		$ename= implode(',',$data);
		return $ename;
	}
	//employee group name get
	public function groupnameget($groupid) {
		$i =0;
		$data = array();
		$grpid = explode(',',$groupid);
		$count = count($grpid);
		for($j=0;$j<$count;$j++) {
			$this->db->select('employeegroupname');
			$this->db->from('employeegroup');
			$this->db->where_in('employeegroup.employeegroupid',$grpid[$j]);
			$this->db->where('employeegroup.status',1);
			$result = $this->db->get();
			if($result->num_rows() > 0) {
				foreach($result->result() as $row) {
					$data[$i] = $row->employeegroupname;
					$i++;
				}
			}
		}
		$grpname= implode(',',$data);
		return $grpname;
	}
	//role name get
	public function rolenameget($roleid) {
		$data = array();
		$i =0;
		$grpid = explode(',',$roleid);
		$count = count($grpid);
		for($j=0;$j<$count;$j++) {
			$this->db->select('userrolename');
			$this->db->from('userrole');
			$this->db->where_in('userrole.userroleid',$grpid[$j]);
			$this->db->where('userrole.status',1);
			$result = $this->db->get();
			if($result->num_rows() > 0) {
				foreach($result->result() as $row) {
					$data[$i] = $row->userrolename;
					$i++;
				}
			}
		}
		$grpname= implode(',',$data);
		return $grpname;
	}
	//group unique name check
	public function groupuniquenamemodel() {
		$grpname = $_POST['groupname'];
		if($grpname != "") {
			$result = $this->db->select('employeegroup.employeegroupid')->from('employeegroup')->where('employeegroupname',$grpname)->where('employeegroup.status',1)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->employeegroupid;
				}
			} else { echo "False"; }
		} else { echo "False"; }
	}
}