<div id="serialnumberview" class="gridviewdivforsh">
<?php
	$dataset['gridtitle'] = $gridtitle['title'];
	$dataset['titleicon'] = $gridtitle['titleicon'];
	$dataset['gridid'] = 'serialnumbergrid';
	$dataset['gridwidth'] = 'serialnumbergridwidth';
	$dataset['gridfooter'] = 'serialnumbergridfooter';
	$dataset['viewtype'] = 'disable';
	$dataset['moduleid'] = '45';
	$this->load->view('Base/singlemainviewformwithgrid.php',$dataset);
?>
</div>
<div id="serialnumberaddformdiv" class="singlesectionaddform">
<div class="large-12 columns addformunderheader">&nbsp;</div>
<div class="large-12 columns salesaddformcontainer">
	<div class="row mblhidedisplay">&nbsp;</div>
	<div id="subformspan1" class="hiddensubform">
		  <div class="closed effectbox-overlay effectbox-default" id="serialnumbersectionoverlay"> 
		  	<form method="POST" name="serialnumberform" class="serialnumberform validationEngineContainer"  id="serialnumberform" style="height: 100% !important;">
				<div id="" class="" style="height: 100% !important;">
				  <div id="editvalidate" class="validationEngineContainer" style="height: 100% !important;">
					<div class="large-4 large-offset-8 columns" style="padding-right:0px !important;height: 100% !important;padding-left:0px !important;">
						<div class="large-12 columns cleardataform border-modal-8px" style="padding-left: 0 !important; padding-right: 0 !important;">
							<div class="large-12 columns sectionheaderformcaptionstyle"> Serial Number Setting </div>					
							<div class="large-12 columns sectionpanel">
								<div class="static-field large-6 columns ">
									<label>Module<span class="mandatoryfildclass">*</span></label>						
									<select class="chzn-select validate[required]" id="moduleid" name="moduleid" data-placeholder="Select" data-prompt-position="topLeft:14,36" tabindex="101">
										<option value=""></option>
									<?php foreach($serialmodule as $key):?>
									  <?php $lookup=explode(',',$key->lookup);  ?>
										<option value="<?php echo $key->moduleid;?>" data-table='<?php echo $key->modulemastertable;?>' data-serialfield='<?php echo $lookup[0];?>' data-field='<?php echo $lookup[1];?>'>
											<?php echo $key->modulename;?></option>
									<?php endforeach;?>		
									</select>
								
								</div>
								
								<div class="static-field large-6 columns ">
									<label>Method Type<span class="mandatoryfildclass">*</span></label>						
									<select class="chzn-select validate[required]" id="methodtype" name="methodtype" data-placeholder="Select" data-prompt-position="topLeft:14,36" tabindex="102">
									</select>
								</div>
								<div class="static-field large-6 columns hidedisplay" id="calculationtypeid">
								<label>Calc Type<span class="mandatoryfildclass">*</span></label>					
								<select id="transactionmodeid" name="transactionmodeid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="1007">
									<option value=""></option>
									<?php foreach($transactionmode as $key): ?>
									<option value="<?php echo $key->transactionmodeid;?>">
									<?php echo $key->transactionmodename;?></option>
									<?php endforeach;?>
								</select>
							  </div>
								<!--<div class="static-field large-12 columns hidedisplay" id="estimatenumbermodeiddivid">
								<label>Estimate Number Mode<span class="mandatoryfildclass">*</span></label>					
								<select id="estimatenumbermodeid" name="estimatenumbermodeid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="1007">
									<option value="1">Mode 1</option>
									<option value="2">Mode 2</option>
								</select>
							  </div>-->
								<div class="static-field large-6 columns ">
									<label>Number Type<span class="mandatoryfildclass">*</span></label>						
									<select class="chzn-select validate[required]" id="numbertype" name="numbertype" data-placeholder="Select" data-prompt-position="topLeft:14,36" tabindex="103">
									<option value=""></option>
									<?php foreach($numbertype as $key):?>
										<option value="<?php echo $key->serialnumbertypeid;?>">
											<?php echo $key->serialnumbertypename;?></option>
									<?php endforeach;?>		
									</select>
								</div>
								<div class="static-field large-6 columns end hidedisplay" id="serialmasternamediv">
									<input type="text"  id="serialmastername" name="serialmastername" value="" tabindex="102" class="chzn-select" data-prompt-position="bottomLeft:14,36" >
									</select>
									<label for="serialmastername">Unique Name</label>
								</div>
								<div class="input-field large-6 columns randomhide">
									<input type="text" class="validate[maxSize[15]]" id="startingnumber" name="startingnumber" value="" tabindex="105">
									<label for="startingnumber">Starting Number<span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="input-field large-6 columns">
									<input type="text" class="validate[custom[blockspchar],maxSize[10]]" id="prefix" name="prefix" value="" tabindex="106">
									<label for="prefix">Prefix</label>	
								</div>
								<div class="input-field large-6 columns end">
									<input type="text" class="validate[custom[blockspchar],maxSize[10]]" id="suffix" name="suffix" value="" tabindex="107">
									<label for="suffix">Suffix</label>
								</div>
								<div class="input-field large-6 columns end hidedisplay" id="startdatediv">
									<input type="text" class="fordatepicicon" id="startdate" name="startdate" data-dateformater="yy-mm-dd" value="" tabindex="107" data-prompt-position="topLeft" >
									<label for="startdate">Start Date</label>
								</div>
								<div class="input-field large-6 columns end hidedisplay" id="enddatediv">
									<input type="text" class="fordatepicicon" id="enddate" name="enddate" data-dateformater="yy-mm-dd" value="" tabindex="107" data-prompt-position="topLeft">
									<label for="enddate">End Date</label>
								</div>
							</div>
							<input type="hidden" id="viewfieldids" name="viewfieldids" value="45" />
							<div class="divider large-12 columns"></div>
							<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">
								<input type="button" class="alertbtnyes addkeyboard" id="addsave" name="addsave" value="Save" tabindex="108">
								<input type="button" class="alertbtnyes updatekeyboard" style="display:none" id="editsave" name="editsave" value="Save" tabindex="109">
								<input type="button" class="alertbtnno addsectionclose"  value="Close" name="cancel" tabindex="110">
							</div>
						</div>
					</div>					
			 	 </div>	
				</div>
			</form>
		 </div>	
	</div>	
</div>
</div>