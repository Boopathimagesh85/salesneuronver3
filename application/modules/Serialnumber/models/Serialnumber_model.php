<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Serialnumber_model extends CI_Model
{
    private $serialnumbermodule = 45;
	public function __construct()
    {
        parent::__construct();
		$this->load->model( 'Base/Basefunctions');
		$this->load->model( 'Base/Crudmodel');
    }		
	/**	* serialnumber create	*/
	public function create() 
	{
		//check previous entry of same settings
		//if($_POST['moduleid'] == '52' && $_POST['methodtype'] == '9' || $_POST['methodtype'] == '11' || $_POST['methodtype'] == '13' || $_POST['methodtype'] == '16' || $_POST['methodtype'] =='18' || $_POST['methodtype'] == '20' || $_POST['methodtype'] == '21'){
		/* if($_POST['moduleid'] == '52' && in_array($_POST['methodtype'],array(9,11,13,16,18,20,21,22,23))) {
			$this->deletepreviousentrywithcalc($_POST['moduleid'],$_POST['methodtype'],$_POST['transactionmodeid']);
		}else{
			$this->deletepreviousentry($_POST['moduleid'],$_POST['methodtype']);
		}  if(empty($_POST['transactionmodeid'])) {
			if($_POST['methodtype'] == '9') {
				$_POST['transactionmodeid']='2';
			}else{
				$_POST['transactionmodeid']='2';
			}
		} else {
			$_POST['transactionmodeid']=$_POST['transactionmodeid'];
		}
		if($_POST['moduleid'] == 52) {
			if($_POST['methodtype'] == 2) {
				$_POST['serialfield'] = 'urdpurchaseno';
			}else if($_POST['methodtype'] == 3) {
				$_POST['serialfield'] = 'creditno';
			}else {
				$_POST['serialfield'] = $_POST['serialfield'];
			}
		}*/
		if($_POST['numbertype'] == 2){
			$_POST['startingnumber']= 0;
			$digitslength = 0;
		}else{
			$_POST['startingnumber'] = $_POST['startingnumber'];
			$digitslength = strlen($_POST['startingnumber']);
		}
		
		$_POST['transactionmodeid']='1';
		$_POST['serialfield'] = $_POST['serialfield'];
		$insert = array(
				'moduleid' => $_POST['moduleid'],
				'serialnumbermastername' => strtolower($_POST['serialmastername']), 
				'methodtype' => $_POST['methodtype'],
				'methodtypename' => $_POST['typename'],	
				'serialnumbertypeid' => $_POST['numbertype'],						
				'table' => $_POST['table'],						
				'fieldname' => $_POST['serialfield'],						
				'methodtypefield' => $_POST['fieldname'],						
				'startingnumber' => $_POST['startingnumber'],
				'currentnumber' => 0,
				'intialnumber' => $_POST['startingnumber'],
				'digitslength' => $digitslength,
				'prefix' => $_POST['prefix'],						
				'suffix' => $_POST['suffix'],
				'startdate' => $_POST['startdate'],		
				'enddate' => $_POST['enddate'],		
				'industryid'=>$this->Basefunctions->industryid,
				'transactionmodeid'=>$_POST['transactionmodeid'],
				'status' => $this->Basefunctions->activestatus
			  );
		$insert = array_merge($insert,$this->Crudmodel->defaultvalueget());
		$this->db->insert('varserialnumbermaster',array_filter($insert));
		$primaryid = $this->db->insert_id();
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity = ''.$user.' created serial number ';
		$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,$this->serialnumbermodule);
		echo 'SUCCESS';
	}
	/**	*	delete previous entries	*/
	public function deletepreviousentry($moduleid,$methodtype)
	{
		$wherearray = array(
							'moduleid'=>$moduleid,
							'methodtype'=>$methodtype,
							'status'=>$this->Basefunctions->activestatus
							);
		$this->db->where($wherearray);					
		$this->db->update('varserialnumbermaster',$this->Basefunctions->delete_log());					
	}
	public function deletepreviousentrywithcalc($moduleid,$methodtype,$transactionmodeid)
	{
		$wherearray = array(
				'moduleid'=>$moduleid,
				'methodtype'=>$methodtype,
				'transactionmodeid'=>$transactionmodeid,
				'status'=>$this->Basefunctions->activestatus
		);
		$this->db->where($wherearray);
		$this->db->update('varserialnumbermaster',$this->Basefunctions->delete_log());
	}
	/**	*serialnumber retrive	*/
	public function retrive()
	{
		$id = $_GET['primaryid'];
		$this->db->select('varserialnumbermaster.moduleid,varserialnumbermaster.methodtype,varserialnumbermaster.table,varserialnumbermaster.fieldname,varserialnumbermaster.serialnumbertypeid,varserialnumbermaster.suffix,varserialnumbermaster.prefix,varserialnumbermaster.startingnumber,varserialnumbermaster.digitslength,varserialnumbermaster.incrementby,varserialnumbermaster.nextnumber,varserialnumbermaster.varserialnumbermasterid,varserialnumbermaster.transactionmodeid,module.moduleid,module.modulename,varserialnumbermaster.startdate,varserialnumbermaster.enddate,varserialnumbermaster.serialnumbermastername');
		$this->db->from('varserialnumbermaster');
		$this->db->join('module','module.moduleid=varserialnumbermaster.moduleid');	
		$this->db->where('varserialnumbermaster.varserialnumbermasterid',$id);
		$this->db->limit(1);
		$info=$this->db->get()->row();
		
		$jsonarray=array(
							'moduleid'=>$info->moduleid,
							'methodtype'=>$info->methodtype,
							'numbertype'=>$info->serialnumbertypeid,
							'startingnumber'=>$info->startingnumber,
				   			'digitslength'=>$info->digitslength,
							'prefix'=>$info->prefix,
							'suffix'=>$info->suffix,
							'transactionmodeid'=>$info->transactionmodeid,
							'table'=>$info->table,
							'modulename'=>$info->modulename,
							'startdate'=>$info->startdate,
							'enddate'=>$info->enddate,
							'serialnumbermastername'=>$info->serialnumbermastername
						);
		echo json_encode($jsonarray);
	}
	/**	* serialnumber update	*/
	public function update()
	{
		$id = $_POST['primaryid'];
		if(empty($_POST['transactionmodeid'])) {
			if($_POST['methodtype'] == '9') {
				$_POST['transactionmodeid']='2';
			}else{
				$_POST['transactionmodeid']='1';
			}
		} else {
			$_POST['transactionmodeid']=$_POST['transactionmodeid'];
		}
		if($_POST['moduleid'] == 52) {
			if($_POST['methodtype'] == 2) {
				$_POST['serialfield'] = 'urdpurchaseno';
			}else if($_POST['methodtype'] == 3) {
				$_POST['serialfield'] = 'creditno';
			}else {
				$_POST['serialfield'] = $_POST['serialfield'];
			}
		}
		if($_POST['numbertype'] == 2){
		  $update = array(
					'methodtype' => $_POST['methodtype'],
					'methodtypename' => $_POST['typename'],
					'methodtypefield' => $_POST['fieldname'],
					'table' => $_POST['table'],
					'fieldname' => $_POST['serialfield'],
					'serialnumbertypeid' => $_POST['numbertype'],
					'startingnumber' => 0,
					'intialnumber' => 0,
					'digitslength' => 0,
					'prefix' => $_POST['prefix'],
					'suffix' => $_POST['suffix'],
					'startdate' => $_POST['startdate'],		
					'enddate' => $_POST['enddate'],
					'serialnumbermastername' => $_POST['serialmastername'],
					'transactionmodeid'=>$_POST['transactionmodeid'],
		  			'industryid'=>$this->Basefunctions->industryid,
			);
			$update = array_merge($update,$this->Crudmodel->updatedefaultvalueget());
			$this->db->where('varserialnumbermasterid',$id);
			$this->db->update('varserialnumbermaster',$update);
			//audit-log
			$user = $this->Basefunctions->username;
			$userid = $this->Basefunctions->logemployeeid;
			$activity = ''.$user.' created serial number ';
			$this->Basefunctions->notificationcontentadd($id,'Updated',$activity ,$userid,$this->serialnumbermodule);
			echo 'SUCCESS';
		}else{
			$startnumberlength=strlen($_POST['startingnumber']);
			$update = array(
					'methodtype' => $_POST['methodtype'],
					'methodtypename' => $_POST['typename'],
					'methodtypefield' => $_POST['fieldname'],
					'table' => $_POST['table'],
					'fieldname' => $_POST['serialfield'],
					'serialnumbertypeid' => $_POST['numbertype'],
					'startingnumber' => $_POST['startingnumber'],
					'currentnumber' => $_POST['startingnumber'],
					'intialnumber' => $_POST['startingnumber'],
					'digitslength' => $startnumberlength,
					'prefix' => $_POST['prefix'],
					'suffix' => $_POST['suffix'],
					'serialnumbermastername' => $_POST['serialmastername'],
					'transactionmodeid'=>$_POST['transactionmodeid'],
					'startdate' => $_POST['startdate'],		
					'enddate' => $_POST['enddate']
			);
			$update = array_merge($update,$this->Crudmodel->updatedefaultvalueget());
			$this->db->where('varserialnumbermasterid',$id);
			$this->db->update('varserialnumbermaster',$update);
			//audit-log
			$user = $this->Basefunctions->username;
			$userid = $this->Basefunctions->logemployeeid;
			$activity = ''.$user.' updated serial number ';
			$this->Basefunctions->notificationcontentadd($id,'Updated',$activity ,$userid,$this->serialnumbermodule);
			echo 'SUCCESS';
		}
	}
	/**	*inactivate-delete serialnumber grid		*/
	public function delete()
	{
		$id=$_GET['primaryid'];
		//inactivate serialnumber
		$inactive = $this->Basefunctions->delete_log();
		$this->db->where('varserialnumbermasterid',$id);
		$this->db->update('varserialnumbermaster',$inactive);
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity = ''.$user.' deleted serial number ';
		$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,$this->serialnumbermodule);
		echo 'SUCCESS';
	}
	public function getserialmodule() {
		$this->db->select('moduleid,modulename,modulemastertable,lookup');
		$this->db->from('module');
		$this->db->where("FIND_IN_SET('SN',showin) >", 0);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$result=$this->db->get()->result();
		return $result;
	}
	public function getmethodtype() {
		$result=array();
		$moduleid=$_GET['primaryid'];
		$userplanid=$this->session->userdata('planids')['plandataid']; 
		$moduledetails=$this->db->where('moduleid',$moduleid)->where('status',$this->Basefunctions->activestatus)->get('module')->row();
		$modulefield=$moduledetails->snfield;
		$moduledata=explode('-',$modulefield); 
		$module_val=explode(',',$moduledata[1]);
		for($i=0; $i < count($module_val); $i++) {
			if($moduledata[0] == 'tagtype')
			{
				$moduletype_array=$this->db->where('tagtypeid',$module_val[$i])->where('FIND_IN_SET('.$userplanid.',planid) > 0')->where('status',$this->Basefunctions->activestatus)->get($moduledata[0])->row();
				if(isset($moduletype_array->tagtypeid)){
				$result[]=array(
				'tagtypeid' => $moduletype_array->tagtypeid,
				'tagtypename' => $moduletype_array->tagtypename
				);
				}
			} else if($moduledata[0] == 'salestransactiontype') {
				$moduletype_array=$this->db->where('salestransactiontypeid',$module_val[$i])->where('FIND_IN_SET('.$userplanid.',planid) > 0')->where('status',$this->Basefunctions->activestatus)->get($moduledata[0])->row();
				if(isset($moduletype_array->salestransactiontypeid)){
					$result[]=array(
					'tagtypeid' => $moduletype_array->salestransactiontypeid,
					'tagtypename' => $moduletype_array->salestransactiontypename
					);
				}
			}
		}
		
		echo json_encode($result);
	}
	public function checkstartnumber() {
		$moduleid=$_POST['moduleid'];
		$check_data=$this->db->select_max('currentnumber')->select('intialnumber')->where('moduleid',$moduleid)->get('varserialnumbermaster');
		if($check_data->num_rows() > 0){
			if(empty($check_data->row()->currentnumber)){
				$currentnumber = 0;
			}else{
				$currentnumber=$check_data->row()->currentnumber;
			}
			$intialnumber=$check_data->row()->intialnumber;
		}else{
			$currentnumber = 0;
			$intialnumber=0;
		}
		$result['currentnumber']=$currentnumber;
		$result['intialnumber']=$intialnumber;
		echo json_encode($result);
	}
	public function checkserialnumber() {
		$moduleid = $_POST['moduleid'];
		$table = $_POST['modtable'];
		$methodtype = $_POST['methodtype'];
		if($methodtype == '9') {
			$modeid = 2;
		}else{
			$modeid = $_POST['modeid'];
		}
		$select =  $table.'number';
		$status = 'false';
		if($moduleid == '52'){
			if($methodtype == 1){
					$serial_rows = $this->db->select($select)->from($table)->where_not_in($select,'')->where_in('status',array(1,11,12))->get()->num_rows();
			}else if($methodtype == '9' || $methodtype == '11' || $methodtype == '13' || $methodtype == '16' || $methodtype == '18' || $methodtype == '20' || $methodtype == '21'){
					$serial_rows = $this->db->select($select)->from($table)->where_not_in($select,'')->where('salestransactiontypeid',$methodtype)->where('transactionmodeid',$modeid)->where_in('status',array(1,11,12))->get()->num_rows();
			}else if($methodtype == '2' || $methodtype == '3'){
				    if($methodtype == '2') {
						$select = 'urdpurchaseno';
					}else if($methodtype == '3') {
						$select = 'creditno';
					}
					$serial_rows = $this->db->select($select)->from($table)->where_not_in($select,'')->where('salestransactiontypeid',11)->where('transactionmodeid',$modeid)->where_in('status',array(1,11,12))->get()->num_rows();
			}else{
				$serial_rows = $this->db->select($select)->from($table)->where_not_in($select,'')->where('salestransactiontypeid',$methodtype)->where_in('status',array(1,11,12))->get()->num_rows();
			}
		}else if($moduleid == '50'){
			if($methodtype == 1){
				$serial_rows = $this->db->select($select)->from($table)->where_not_in($select,'')->where_in('status',array(1,11,12))->get()->num_rows();
			}else{
				$serial_rows = $this->db->select($select)->from($table)->where_not_in($select,'')->where('tagtypeid',$methodtype)->where_in('status',array(1,11,12))->get()->num_rows();
			}
		}else{
				$serial_rows = $this->db->select($select)->from($table)->where_not_in($select,'')->where_in('status',array(1,11,12))->get()->num_rows();
		}
		if($serial_rows > 0){
			$status = 'true';
		}
		echo json_encode($status);
	}
	public function checkexistrecord() {
		$moduleid = $_POST['moduleid'];
		$methodtype = $_POST['methodtype'];
		/* if($methodtype == '9') {
			$modeid = 2;
		}else{ */
			$modeid = $_POST['modeid'];
		//}
		$this->db->select('varserialnumbermasterid');
		$this->db->from('varserialnumbermaster');
		$this->db->where('varserialnumbermaster.moduleid',$moduleid);
		$this->db->where('varserialnumbermaster.methodtype',$methodtype);
		$this->db->where('varserialnumbermaster.transactionmodeid',$modeid);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$res=$this->db->get();
		if($res->num_rows() > 0) {
			$status = 'true';
		}else{
			$status = 'false';
		}
		echo json_encode($status);
	}
	public function serialmasternameload() {
		$moduleid = $_GET['moduleid'];
		$prizeheadernamenames = array();
		$prizeheadername = $this->db->distinct()->select('serialnumbermastername')->from('varserialnumbermaster')->where('serialnumbermastername !=','')->where('moduleid',$moduleid)->where('status',$this->Basefunctions->activestatus)->get();
		$i=0;
		foreach($prizeheadername->result() as $prizeheadernamedata) {
			$prize_headername=explode(',',$prizeheadernamedata->serialnumbermastername);
			 $prize_headername_count=count($prize_headername); 
			for($j=0; $j < $prize_headername_count; $j++){ 
				$prizeheadernamenames[$i] =$prize_headername[$j];
				$i++;
			}
		}
		$datas_prizeheadernamenames = array_map("unserialize", array_unique(array_map("serialize", $prizeheadernamenames)));
		echo json_encode($datas_prizeheadernamenames);
	}
	//filter values
	public function filtervalue($fields) {
		$fieldsinfo = array_unique( $fields ) ;
		$fieldname = implode(',',$fieldsinfo);
		return $fieldname;
	}
	//unique Serial Master Name check
	public function serialnumbermasternamecheck(){
		$industryid = $this->Basefunctions->industryid;
		$primaryid=$_POST['primaryid'];
		$methodtype=$_POST['methodtype'];
		$accname=$_POST['accname'];
		$elementpartable = explode(',',$_POST['elementspartabname']);
		$partable =  $this->filtervalue($elementpartable);
		$ptid = $partable.'id';
		$ptname = $_POST['fieldname'];
		if($accname != "") {
			$result = $this->db->select($ptname.','.$ptid)->from($partable)->where($partable.'.'.$ptname,$accname)->where($partable.'.methodtype',$methodtype)->where($partable.'.status',1)->where("FIND_IN_SET('".$industryid."',industryid) >", 0)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->$ptid;
				}
			} else { echo "False"; }
		}else { echo "False"; }
	}
}