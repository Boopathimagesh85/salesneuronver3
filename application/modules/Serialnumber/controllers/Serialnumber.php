<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Serialnumber extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Serialnumber_model');	
    }
    private $serialnumbermoduleid = 45;
	//first basic hitting view
    public function index()
    {	
    	$moduleid = array(45);
		sessionchecker($moduleid);
		//action		
		$data['actionassign'] = $this->Basefunctions->actionassign($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['filedmodids'] = $moduleid;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($moduleid);
		$data['serialmodule']=$this->Serialnumber_model->getserialmodule(); 
        $data['numbertype'] = $this->Basefunctions->simpledropdown('serialnumbertype','serialnumbertypeid,serialnumbertypename','serialnumbertypeid');
        $data['transactionmode'] = $this->Basefunctions->simpledropdown('transactionmode','transactionmodeid,transactionmodename,setdefault','transactionmodeid',array('moduleid'=>'52'));
        $this->load->view('serialnumberview',$data);
	  }
	/**	*serialnumber creation	*/
    public function create() 
    {  
    	$this->Serialnumber_model->create();
    }
	/**	*serialnumber retrieve-for edit/view 	*/
    public function retrive() 
    {  
    	$this->Serialnumber_model->retrive();
    }
	/**	*serialnumber update	*/
    public function update()
    {
		$this->Serialnumber_model->update();
    }
	/**	*serialnumber delete	*/
    public function delete()
    {
		$this->Serialnumber_model->delete();	
    }
	/**	*return the tagtypename and salestransactionname based on moduels	*/
	public function getmethodtypename($methodtype,$moduleid)
	{
		$name ='';
		if($moduleid == 12)
		{
			$name = $this->Basefunctions->singlefieldfetch('tagtypename','tagtypeid','tagtype',$methodtype);
		}
		if($moduleid == 20)
		{
			$name = $this->Basefunctions->singlefieldfetch('salestransactiontypename','salestransactiontypeid','salestransactiontype',$methodtype);
		}
		return $name;
	}
	/**	*returns the options name based on Random/Sequences.	*/
	public function getrandom($random)
	{
		if($random == 'Yes')
		{
			$out = 'Random';
		}
		else
		{
			$out = 'Sequence';
		}
		return $out;
	}
	public function getmethodtype() {
		$this->Serialnumber_model->getmethodtype();
	}
	public function checkstartnumber() {
		$this->Serialnumber_model->checkstartnumber();
	}
	public function checkserialnumber() {
		$this->Serialnumber_model->checkserialnumber();
	}
	public function checkexistrecord() {
		$this->Serialnumber_model->checkexistrecord();
	}
	public function serialmasternameload(){
			$this->Serialnumber_model->serialmasternameload();
	}
	public function serialnumbermasternamecheck(){
			$this->Serialnumber_model->serialnumbermasternamecheck();
	}
}