<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Solutions extends MX_Controller{
    //Constructor Function To Load Models
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Solutions/Solutionsmodel');	
	}
	//Default View 
	public function index() {
		$moduleid = array(229,79);
		sessionchecker($moduleid);
		//action
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(229,79);
		$viewmoduleid = array(229,79);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['dashboradviewdata'] = $this->Basefunctions->dashboarddataviewbydropdown($moduleid);
		$this->load->view('Solutions/solutionsview',$data);	
	}
	//create Campaign
	public function newdatacreate() {  
    	$this->Solutionsmodel->newdatacreatemodel();
    }
	//information fetch
	public function fetchformdataeditdetails() {
		$moduleid = '229,79';
		$this->Solutionsmodel->informationfetchmodel($moduleid);
	}
	//update Campaign
    public function datainformationupdate() {
        $this->Solutionsmodel->datainformationupdatemodel();
    }
	//delete Campaign
    public function deleteinformationdata() {
        $moduleid = '229,79';
		$this->Solutionsmodel->deleteoldinformation($moduleid);
    }   
	//editor value fetch 	
	public function editervaluefetch() {
		$filename = $_GET['filename']; 
		$newfilename = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1])){
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]);
			echo json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			echo json_encode($tccontent);
		}
	}
	//document details fetch
	public function solutionsproductdetailfetch() {
		$this->Solutionsmodel->solutionsproductdetailfetchmodel();
	}
	/* Local non-live grid heder information fetch */
	public function localgirdheaderinformationfetch() {
		$moduleid = $_GET['moduleid'];
		$tabgrpid = $_GET['tabgroupid'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modname = $_GET['modulename'];
		$colinfo = $this->Basefunctions->localgridheaderinformationfetchmodel($moduleid,$tabgrpid);
		$device = $this->Basefunctions->deviceinfo();
	//	if($device=='phone') {
			//$datas = $this->Solutionsmodel->mobilelocalviewgridheadergenerate($colinfo,$modname,$width,$height);
		//} else {
			$datas = $this->Solutionsmodel->localviewgridheadergenerate($colinfo,$modname,$width,$height);
		//}
		echo json_encode($datas);
	}
	//Folder insertion
	public function solutionsfolderinsert() {
		$this->Solutionsmodel->solutionsfolderinsertmodel();
	}
	//Folder Grid View
	public function solutionsfoldernamegridfetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'foldername.foldernameid') : 'foldername.foldernameid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('foldernamename','description','setdefault','public'),'colmodelindex'=>array('foldername.foldernamename','foldername.description','foldername.setdefault','foldername.public'),'coltablename'=>array('foldername','foldername','foldername','foldername'),'uitype'=>array('2','2','2','2'),'colname'=>array('Folder Name','Description','Default','Public'),'colsize'=>array('200','200','200','200'));
		$result=$this->Solutionsmodel->solutionsfoldernamegridxmlview($primarytable,$sortcol,$sortord,$pagenum,$rowscount);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($result,$page,'');
		$device = $this->Basefunctions->deviceinfo();
	//	if($device=='phone') {
		//	$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Folder List',$width,$height);
		//} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Folder List',$width,$height);
		//}
		echo json_encode($datas);
	}
	//Folder data fetch
	public function solutionsfolderdatafetch() {
		$this->Solutionsmodel->solutionsfolderdatafetchmodel();
	}
	//Folder Update
	public function solutionsfolderupdate() {
		$this->Solutionsmodel->solutionsfolderupdatemodel();
	}
	//Folder Delete
	public function folderdeleteoperation() {
		$this->Solutionsmodel->folderdeleteoperationmodel();
	}
	//Folder drop down Fetch Data
	public function fetchdddataviewddval() {
		$this->Solutionsmodel->fetchdddataviewddvalmodel();
	}
	//Folder Unique Name
	public function foldernameunique() {
		$this->Solutionsmodel->foldernameuniquemodel();
	}
}