<style type="text/css">
.padding-space-open-for-form {
	padding-left:13px !important;
}
#solutionsfoladdgrid1footer {
	display:none;
}
</style>
<div class="large-12 columns paddingzero formheader">
	<?php
		$this->load->view('Base/mainviewaddformheader');
	?>
</div>
<div class="large-12 columns addformunderheader">&nbsp;</div>
<div class="large-12 columns scrollbarclass addformcontainer tabtouchaddcontainer">
	<div class="row mblhidedisplay">&nbsp;</div>
	<form method="POST" name="dataaddform" class="" action ="" id="dataaddform">
		<span id="formaddwizard" class="validationEngineContainer" >
			<span id="formeditwizard" class="validationEngineContainer">
				<?php
                    //function call for form fields generation
                     formfieldstemplategenerator($modtabgrp,'');
                ?>	
			</span>
		</span>
		<!--hidden values-->
		<?php
			echo hidden('primarydataid','');
			echo hidden('sortorder','');
			echo hidden('sortcolumn','');
			echo hidden('conrowcolids','');
			$value = 'crmfileinfo';
			echo hidden('resctable',$value);
			//hidden text box view columns field module ids
			$modid = $this->Basefunctions->getmoduleid($filedmodids);
			echo hidden('viewfieldids',$modid);
			$defaultrecordview = $this->Basefunctions->get_company_settings('mainviewdefaultview');
			echo hidden('mainviewdefaultview',$defaultrecordview);
		?>
	</form>
</div>