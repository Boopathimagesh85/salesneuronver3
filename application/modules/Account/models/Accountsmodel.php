<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Model File
class Accountsmodel extends CI_Model{    
    public function __construct() {		
		parent::__construct(); 
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
		$this->load->model('Sales/Salesmodel');
		$this->load->model('Chitbookentry/Chitbookentrymodel');
    }  
	//To Create account Details
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$restricttable = explode(',',$_POST['resctable']);
		//industry based moduleid
		$moduleid = $_POST['viewfieldids'];
		//Retrieve the Account autoNumber
		$anfieldnameinfo = $_POST['anfieldnameinfo'];
		$anfieldnameidinfo = $_POST['anfieldnameidinfo'];
		$anfieldtabinfo = $_POST['anfieldtabinfo'];
		$anfieldnamemodinfo = $_POST['anfieldnamemodinfo'];
		$randomnum = $this->Basefunctions->randomnumbergenerator($anfieldnamemodinfo,$anfieldtabinfo,$anfieldnameinfo,$anfieldnameidinfo);
		$_POST['accountnumber']=trim($randomnum);
		//dynamic Insertion
		$primaryid = $this->Crudmodel->datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable);
		//Signature image insert
		if($_POST['signatureimgpath'] != '') {
			$update=array(
				'signaturepad_path'=>$_POST['signatureimgpath']
			);
			$update = array_merge($update);
			$this->db->where('accountid',$primaryid);
			$this->db->update('account',$update);
		}
		//address insertion
		$arrname=array('billing','shipping');
		$this->Crudmodel->addressdatainsert($arrname,$partablename,$primaryid);
		//notification entry (Audit log)
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		if(isset($_POST['employeeid'])) {
			$assignid = $_POST['employeeid'];
			//$emptypes = $_POST['employeetypeid'];
			$emptypes = 1;
			$empdataids = array();
			$assignempids = array();
			if($assignid != '') {
				$i = 0;
				$m=0;
				$empiddatas = explode(',',$assignid);
				$emptypeids = explode(',',$emptypes);
				foreach($empiddatas as $empids) {
					$emptype = $emptypeids[$m];
					if($emptype == 1) {
						$empdataids[$i] = $empids;
						$i++;
					} else if($emptype == 2) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromgroup($empids);
						$i++;
					} else if($emptype == 3) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromroles($empids);
						$i++;
					} else if($emptype == 4) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromrolesandsubroles($empids);
						$i++;
					}
					$m++;
				}
			} else {
				$assignid = 1;
			}
		} else {
			$assignid = 1;
		}
		if(isset($_POST['accountname'])) {
			$accountname = $_POST['accountname'];
		} else {
			$accountname = $_POST['name'];
		}
		if($assignid == '1') {
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,2);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,$moduleid);
			}
		} else {
			foreach($empdataids as $empidinfo) {
				if(is_array($empidinfo)) { //group of employees
					foreach($empidinfo as $empid) {
						if( !in_array($empid,$assignempids) ) {
							array_push($assignempids,$empid);
							$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,2);
							if($notimsg != '') {
								$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,$moduleid);
							}
						}
					}
				} else { //individual employees
					if( !in_array($empidinfo,$assignempids) ) {
						array_push($assignempids,$empidinfo);
						$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,2);
						if($notimsg != '') {
							$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,$moduleid);
						}
					}
				}
			}
		}
		{//workflow management -- for data create
			$this->Basefunctions->workflowmanageindatacreatemode($moduleid,$primaryid,$partablename);
		}
		if($this->Basefunctions->industryid == 3 && in_array($_POST['accounttypeid'],array(6,16))) {
			$issuereceiptid = 1;
			$wtissuereceiptid = 1;
			$paymentirtypeid = 5;
			$amtissue = 0;
			$amtreceipt = 0;
			$wtissue = 0;
			$summarywt = 0;
			$salesdate = date("Y-m-d");
			if(!isset($_POST['openingbalance'])) {
				$_POST['openingbalance'] = 0;
			}
			if(!isset($_POST['openinggram'])) {
				$_POST['openinggram'] = 0;
			}
			if(($_POST['openingbalance'] != '' && $_POST['openingbalance'] != 0) || ($_POST['openinggram'] != '' && $_POST['openinggram'] != 0)) {
				if($_POST['openingbalance'] > 0) 
				{
					$primprefix = 'ACBAL';
					$issuereceiptid = 2;
					$amtissue = $_POST['openingbalance'];
					$amtreceipt = 0;
				}else if($_POST['openingbalance'] < 0)
				{
					$primprefix = 'ACADV';
					$issuereceiptid = 3;
					$amtissue = 0;
					$amtreceipt = $_POST['openingbalance'];
				}
				if($_POST['openinggram'] > 0) 
				{
					$primprefix = 'ACBAL';
					$wtissuereceiptid = 2;
					$wtissue = $_POST['openinggram'];
					$summarywt = 0;
				}else if($_POST['openinggram'] < 0)
				{
					$primprefix = 'ACADV';
					$wtissuereceiptid = 3;
					$wtissue = 0;
					$summarywt = $_POST['openinggram'];
				}
				$creditno = $primprefix.$primaryid;
				$insertaccountledgerdata = array(
									'salesid'=>1,
									'accountid'=>$primaryid,
									'entrymode'=>2,
									'creditno'=>$creditno,
									'referencenumber'=>$accountname,
									'salesdate'=>$salesdate,
									'issuereceiptid'=>$issuereceiptid,
									'weightissuereceiptid'=>$wtissuereceiptid,
									'paymentirtypeid'=>$paymentirtypeid,
									'amtissue'=>abs($amtissue),
									'amtreceipt'=>abs($amtreceipt),
									'weightreceipt'=>abs($summarywt),
									'weightissue'=>abs($wtissue)
							);
				$insertaccountledgerdata = array_merge($insertaccountledgerdata,$this->Crudmodel->defaultvalueget());
				$this->db->insert('accountledger',array_filter($insertaccountledgerdata));
					// credit no based balance entry
					if($creditno != '')
					{
						$this->Salesmodel->creditnobalancesummary(1,1,$accountname,$primaryid,$creditno,$paymentirtypeid);
					}
					// account no based balance summary
					$this->Salesmodel->accountbalancesummary(1,1,$accountname,$primaryid,$salesdate,$paymentirtypeid);
			}
		}
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' created Account - '.$_POST['name'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,91);
		$result_create = ','.$primaryid.'';
		echo 'TRUE'.$result_create.'';
	}
	//information fetch
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementpartable']);
		$restricttable = explode(',',$_GET['resctable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['dataprimaryid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$sndefault = $this->Basefunctions->singlefieldfetch('sndefault','accountid','account',$primaryid); //Check default value
		if($sndefault == 1) {
			$result = $this->Crudmodel->dbdatafetchwithrestrict($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$restricttable,$moduleid);
			echo $result;
		} else {
			echo json_encode(array("fail"=>'sndefault'));
		}
	}
	//update data information
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['primarydataid'];
		$restricttable = explode(',',$_POST['resctable']);
		//industry based moduleid
		$moduleid = $_POST['viewfieldids'];
		{//fetch old values -- work flow
			$condstatvals=$this->Basefunctions->workflowmanageolddatainfofetch($moduleid,$primaryid);
		}
		$result = $this->Crudmodel->dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable);
		//Signature image update	
		$update=array(
			'signaturepad_path'=>$_POST['signatureimgpath']
		);
		$update = array_merge($update);
		$this->db->where('accountid',$primaryid);
		$this->db->update('account',$update);
		$arrname=array('billing','shipping');
		$this->Crudmodel->addressdataupdate($arrname,$partablename,$primaryid);	
		//notification update
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		if(isset($_POST['employeeid'])) {
			$assignid = $_POST['employeeid'];
			//$emptypes = $_POST['employeetypeid'];
			$emptypes = 1;
			$empdataids = array();
			$assignempids = array();
			if($assignid != '') {
				$i = 0;
				$m=0;
				$empiddatas = explode(',',$assignid);
				foreach($empiddatas as $empids) {
					$emptype = $emptypes[$m];
					if($emptype == 1) {
						$empdataids[$i] = $empids;
						$i++;
					} else if($emptype == 2) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromgroup($empids);
						$i++;
					} else if($emptype == 3) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromroles($empids);
						$i++;
					} else if($emptype == 4) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromrolesandsubroles($empids);
						$i++;
					}
					$m++;
				}
			} else {
				$assignid = 1;
			}
		} else {
			$assignid = 1;
		}
		if(isset($_POST['accountname'])) {
			$accountname = $_POST['accountname'];
		} else {
			$accountname = $_POST['name'];
		}
		if($assignid == '1'){
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,3);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,$moduleid);
			}
		} else {
			foreach($empdataids as $empidinfo) {
				if(is_array($empidinfo)) { //group of employees
					foreach($empidinfo as $empid) {
						if( !in_array($empid,$assignempids) ) {
							array_push($assignempids,$empid);
							$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,3);
							if($notimsg != '') {
								$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,$moduleid);
							}
						}
					}
				} else { //individual employees
					if( !in_array($empidinfo,$assignempids) ) {
						array_push($assignempids,$empidinfo);
						$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,3);
						if($notimsg != '') {
							$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,$moduleid);
						}
					}
				}
			}
		}
		{//workflow management for data update
			$this->Basefunctions->workflowmanageindataupdatemode($moduleid,$primaryid,$partablename,$condstatvals);
		}
		$accounttypeid = $this->Basefunctions->singlefieldfetch('accounttypeid','accountid','account',$primaryid);
		if($this->Basefunctions->industryid == 3 && in_array($accounttypeid,array(6,16))) {
			// delete previous entry
			$delete = $this->Basefunctions->delete_log();
			$salesdata = $this->db->select('accountid,referencenumber,creditno,salesdate,paymentirtypeid,accountledgerid')
						->from('accountledger')
						->where('accountledger.accountid',$primaryid)
						->where('accountledger.paymentirtypeid',5)
						->where_in('accountledger.entrymode',array(2))
						->where('accountledger.status',$this->Basefunctions->activestatus)
						->get();
			if($salesdata->num_rows()>0){
				foreach($salesdata->result() as $salesinfo){
					$ledgeraccountid = $salesinfo->accountid;
					$referencenumber = $salesinfo->referencenumber;
					$creditno = $salesinfo->creditno;
					$salesdate = $salesinfo->salesdate;
					$ledgerpaymentirtypeid = $salesinfo->paymentirtypeid;
					$accountledgerid = $salesinfo->accountledgerid;
				}
				// delete accountledger entry
				$this->db->where('accountledgerid',$accountledgerid);
				$this->db->update('accountledger',$delete);
				// credit no based balance entry
				if($creditno != '')
				{
					$this->Salesmodel->creditnobalancesummary(1,1,$referencenumber,$ledgeraccountid,$creditno,$ledgerpaymentirtypeid);
				}
				// account no based balance summary
				$this->Salesmodel->accountbalancesummary(1,1,$referencenumber,$ledgeraccountid,$salesdate,$ledgerpaymentirtypeid);
			}
			$issuereceiptid = 1;
			$wtissuereceiptid = 1;
			$paymentirtypeid = 5;
			$amtissue = 0;
			$amtreceipt = 0;
			$wtissue = 0;
			$summarywt = 0;
			$salesdate = date("Y-m-d");
			if(!isset($_POST['openingbalance'])) {
				$_POST['openingbalance'] = 0;
			}
			if(!isset($_POST['openinggram'])) {
				$_POST['openinggram'] = 0;
			}
			if(($_POST['openingbalance'] != '' && $_POST['openingbalance'] != 0) || ($_POST['openinggram'] != '' && $_POST['openinggram'] != 0)) {
				if($_POST['openingbalance'] > 0) 
				{
					$primprefix = 'ACBAL';
					$issuereceiptid = 2;
					$amtissue = $_POST['openingbalance'];
					$amtreceipt = 0;
				}else if($_POST['openingbalance'] < 0)
				{
					$primprefix = 'ACADV';
					$issuereceiptid = 3;
					$amtissue = 0;
					$amtreceipt = $_POST['openingbalance'];
				}
				if($_POST['openinggram'] > 0) 
				{
					$primprefix = 'ACBAL';
					$wtissuereceiptid = 2;
					$wtissue = $_POST['openinggram'];
					$summarywt = 0;
				}else if($_POST['openinggram'] < 0)
				{
					$primprefix = 'ACADV';
					$wtissuereceiptid = 3;
					$wtissue = 0;
					$summarywt = $_POST['openinggram'];
				}
				$creditno = $primprefix.$primaryid;
				$insertaccountledgerdata = array(
									'salesid'=>1,
									'accountid'=>$primaryid,
									'entrymode'=>2,
									'creditno'=>$creditno,
									'referencenumber'=>$accountname,
									'salesdate'=>$salesdate,
									'issuereceiptid'=>$issuereceiptid,
									'weightissuereceiptid'=>$wtissuereceiptid,
									'paymentirtypeid'=>$paymentirtypeid,
									'amtissue'=>abs($amtissue),
									'amtreceipt'=>abs($amtreceipt),
									'weightreceipt'=>abs($summarywt),
									'weightissue'=>abs($wtissue)
							);
				$insertaccountledgerdata = array_merge($insertaccountledgerdata,$this->Crudmodel->defaultvalueget());
				$this->db->insert('accountledger',array_filter($insertaccountledgerdata));
					// credit no based balance entry
					if($creditno != '')
					{
						$this->Salesmodel->creditnobalancesummary(1,1,$accountname,$primaryid,$creditno,$paymentirtypeid);
					}
					// account no based balance summary
					$this->Salesmodel->accountbalancesummary(1,1,$accountname,$primaryid,$salesdate,$paymentirtypeid);
			}
		}
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' updated Account - '.$_POST['name'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$activity ,$userid,91);
		echo "TRUE";
	}
	//delete old information
	public function deleteoldinformation($moduleid)	{
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primarydataid'];
		$msg='False';
		$filename = $this->Basefunctions->generalinformaion('account','accountname','accountid',$id);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//industrybased moduleid fetch
		$modid = $this->Basefunctions->getmoduleid($moduleid);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);	
		{//workflow management -- for data delete
			$this->Basefunctions->workflowmanageindatadeletemode($modid,$id,$primaryname,$partabname);
		}
		$sndefault = $this->Basefunctions->singlefieldfetch('sndefault','accountid','account',$id); //Check default value
		if($sndefault == 2) {
			$msg='sndefault';
		} else {
			if($ruleid == 1 || $ruleid == 2) {
				$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$modid);
				if($chek == 0) {
					$msg='Denied';
				} else {
					//notification log
					$empid = $this->Basefunctions->logemployeeid;
					$notimsg = $this->Basefunctions->notificationtemplatedatafetch($modid,$id,$partabname,3);
					if($notimsg != '') {
						$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,$modid);
					}
					$check = $this->checkaccountincashcounter($id);
					if($check != "TRUE") {
						$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
						if($this->Basefunctions->industryid != 3) {
							$this->accountbasedcontactdelete($id);
						}
						if($this->Basefunctions->industryid == 3) {
							// delete account ledger entry
								$delete = $this->Basefunctions->delete_log();
								$salesdata = $this->db->select('accountid,referencenumber,creditno,salesdate,paymentirtypeid,accountledgerid')
											->from('accountledger')
											->where('accountledger.accountid',$id)
											->where('accountledger.paymentirtypeid',5)
											->where_in('accountledger.entrymode',array(2))
											->where('accountledger.status',$this->Basefunctions->activestatus)
											->get();
								if($salesdata->num_rows()>0){
									foreach($salesdata->result() as $salesinfo){
										$ledgeraccountid = $salesinfo->accountid;
										$referencenumber = $salesinfo->referencenumber;
										$creditno = $salesinfo->creditno;
										$salesdate = $salesinfo->salesdate;
										$ledgerpaymentirtypeid = $salesinfo->paymentirtypeid;
										$accountledgerid = $salesinfo->accountledgerid;
									}
									// delete accountledger entry
									$this->db->where('accountledgerid',$accountledgerid);
									$this->db->update('accountledger',$delete);
									// credit no based balance entry
									if($creditno != '')
									{
										$this->creditnobalancesummary(1,1,$referencenumber,$ledgeraccountid,$creditno,$ledgerpaymentirtypeid);
									}
									// account no based balance summary
									$this->accountbalancesummary(1,1,$referencenumber,$ledgeraccountid,$salesdate,$ledgerpaymentirtypeid);
								}
						}
						$msg='TRUE';
					} else {
						$msg= "CASHCOUNTER";
					}			
				}
			} else {
				$notimsg = $this->Basefunctions->notificationtemplatedatafetch($modid,$id,$partabname,3);
				if($notimsg != '') {
					$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,$modid);
				}
				$check = $this->checkaccountincashcounter($id);
				if($check != "TRUE") {
					$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
					if($this->Basefunctions->industryid != 3) {
						$this->accountbasedcontactdelete($id);
					}
					if($this->Basefunctions->industryid == 3) {
							// delete account ledger entry
								$delete = $this->Basefunctions->delete_log();
								$salesdata = $this->db->select('accountid,referencenumber,creditno,salesdate,paymentirtypeid,accountledgerid')
											->from('accountledger')
											->where('accountledger.accountid',$id)
											->where('accountledger.paymentirtypeid',5)
											->where_in('accountledger.entrymode',array(2))
											->where('accountledger.status',$this->Basefunctions->activestatus)
											->get();
								if($salesdata->num_rows()>0){
									foreach($salesdata->result() as $salesinfo){
										$ledgeraccountid = $salesinfo->accountid;
										$referencenumber = $salesinfo->referencenumber;
										$creditno = $salesinfo->creditno;
										$salesdate = $salesinfo->salesdate;
										$ledgerpaymentirtypeid = $salesinfo->paymentirtypeid;
										$accountledgerid = $salesinfo->accountledgerid;
									}
									// delete accountledger entry
									$this->db->where('accountledgerid',$accountledgerid);
									$this->db->update('accountledger',$delete);
									// credit no based balance entry
									if($creditno != '')
									{
										$this->creditnobalancesummary(1,1,$referencenumber,$ledgeraccountid,$creditno,$ledgerpaymentirtypeid);
									}
									// account no based balance summary
									$this->accountbalancesummary(1,1,$referencenumber,$ledgeraccountid,$salesdate,$ledgerpaymentirtypeid);
								}
						}
					$msg='TRUE';
				} else {
					$msg= "CASHCOUNTER";
				}
			}
		}
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$accountname = $this->Basefunctions->singlefieldfetch('accountname','accountid','account',$id);
		$activity = ''.$user.' deleted Account - '.$filename.'';
		$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,91);
		echo $msg;
	}
	//account based contact removed
	public function checkaccountincashcounter($id) {
		$this->db->select('cardcommissionid');
		$this->db->from('cardcommission');
		$this->db->where('accountid',$id);
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			return "TRUE";
		} else {
			return "FALSE";
		}
	}
	//account based contact removed
	public function accountbasedcontactdelete($id) {
		$defaultupdatearray = $this->Crudmodel->deletedefaultvalueget();
		$array = array('accountid'=>$id);
		$wherearray = $array;
		$this->db->where_in('accountid',$id);
		$this->db->update('contact',$defaultupdatearray);
	}
	//tree create model
	public function treecreatemodel() {
		$data = array();
		$this->db->select('accountid,accountname,parentaccountid');
		$this->db->from('account');
		$this->db->where('account.status',1);
	    $this->db->order_by('account.accountid','ASC');
		$result = $this->db->get();		
		foreach($result->result() as $row) {
			$data[$row->accountid] = array('id'=>$row->accountid,'name'=>$row->accountname,'pid'=>$row->parentaccountid);
		}
		$itemsByParent = array();
		foreach ($data as $item) {
			if (!isset($itemsByParent[$item['pid']])) {
				$itemsByParent[$item['pid']] = array();
			}
			$itemsByParent[$item['pid']][] = $item;
		}	
		return $itemsByParent;		
	}
	//primary and secondary address value fetch
	public function primaryaddressvalfetchmodel() {
		$accid = $_GET['dataprimaryid'];
        $accname = $this->Basefunctions->singlefieldfetch('accountname','accountid','account',$accid); //Accountname
		$parent_accountid = $this->Basefunctions->singlefieldfetch('parentaccountid','accountid','account',$accid); //Parent Accountid
		$parent_accountname = $this->Basefunctions->singlefieldfetch('accountname','accountid','account',$parent_accountid); //Accountname
		$this->db->select('accountaddress.addresssourceid,accountaddress.address,accountaddress.pincode,accountaddress.city,accountaddress.state,accountaddress.country');
		$this->db->from('accountaddress');
		$this->db->where('accountaddress.accountid',$accid);
		$this->db->where('accountaddress.status',1);
		$this->db->order_by('accountaddressid','asc');
		$result = $this->db->get();
		$arrname=array('billing','shipping');
		if($result->num_rows() >0) {
			$m=0;
			foreach($result->result() as $row) {
				$data[] = array($arrname[$m].'addresstype'=>$row->addresssourceid,
								$arrname[$m].'address'=>$row->address,
								$arrname[$m].'pincode'=>$row->pincode,
								$arrname[$m].'city'=>$row->city,
								$arrname[$m].'state'=>$row->state,
								$arrname[$m].'country'=>$row->country,
								'accid'=>$accid,
								'accname'=>$accname,
								'parent_accountid'=>$parent_accountid,
								'parent_accountname'=>$parent_accountname
								);
			
				$m++;
			}
			if(count($data) != 1 ) { 
				$finalarray = array_merge($data[0],$data[1]);
			} else {
				$finalarray = $data[0];
			}
			echo json_encode($finalarray);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//parent account name fetch
	public function parentaccountnamefetchmodel() {
		$id=$_GET['id'];
		$data=$this->db->select('a.parentaccountid as parentaccountids,b.accountname as accountnames')
				->from('account as a')
				->join('account as b','a.parentaccountid=b.accountid')
				->where('a.accountid',$id)
				->where('b.status',1)
				->get();
		if($data->num_rows() >0) {
			foreach($data->result() as $in)	{
				$parentcategoryid=$in->parentaccountids;
				$categoryname=$in->accountnames;
			}
			$a = array('parentcategoryid'=>$parentcategoryid,'categoryname'=>$categoryname);
		} else {
			$a = array('parentcategoryid'=>'0','categoryname'=>'');
		}
		echo json_encode($a);
	}
	public function checkaccounttype( ){
		$id=$_REQUEST['primaryid'];
		$this->db->select('accounttypeid');
		$this->db->from('account');
		$this->db->where('accountid',$id);
		$this->db->where('accounttypeid',17);
		$info=$this->db->get();
		if($info->num_rows() > 0){
			echo $info->row()->accounttypeid;
		}else{
			echo '0';
		}
	}
	public function accountname() {
		$id=$_GET['primaryid'];
		$this->db->select('accountname');
		$this->db->from('account');
		$this->db->where('accountid',$id);
		$info=$this->db->get()->row();
		$jsonarray=array(
				'accountname' => $info->accountname
		);
		echo json_encode($jsonarray);
	}
	//chit book create
	public function chitbookcreate() {
		$scheme_id=$_POST['schemename'];
		$scheme_name=$_POST['chitschemename'];
		$scheme_prefix=$_POST['chitschemeprefix'];
		$scheme_amount=$_POST['schemeamount'];
		$scheme_amt_suffix=$_POST['schemeamtprefix'];
		if(isset($_POST['chitcashamount'])) {
			$chitcashamount=$_POST['chitcashamount'];
		} else {
			$chitcashamount=0;
		}
		if(isset($_POST['chitcardamount'])) {
			$chitcardamount=$_POST['chitcardamount'];
		} else {
			$chitcardamount=0;
		}
		if(isset($_POST['chitchequeamount'])) {
			$chitchequeamount=$_POST['chitchequeamount'];
		} else {
			$chitchequeamount=0;
		}
		if(isset($_POST['chitneftamount'])) {
			$chitneftamount=$_POST['chitneftamount'];
		} else {
			$chitneftamount=0;
		}
		if(isset($_POST['chitinterestamount'])) {
			$chitinterestamount=$_POST['chitinterestamount'];
		} else {
			$chitinterestamount=0;
		}
		if(empty($_POST['chitbookname'])) {
			$chit_bookname=$_POST['chit_account_name'];
		} else {
			$chit_bookname=$_POST['chitbookname'];
		}
		$chitlookup = $this->Basefunctions->generalinformaion('chitscheme','chitlookupsid','chitschemeid',$scheme_id);
		if($chitlookup == 6) { //for both
		    //$scheme_amount = $_POST['bothschemeamount'];
			if($_POST['variablemodeid'] == 1) {
				$scheme_amount = $_POST['bothschemeamount'];
			} else {
				$scheme_amount = $scheme_amount;
			}
		} else {
			if($_POST['variablemodeid'] == 1) {
				$scheme_amount = $_POST['bothschemeamount'];
			} else {
				$scheme_amount = $scheme_amount;
			}
		}
		$count_scheme=$this->db->where('chitschemeid',$scheme_id)->get('chitbook')->num_rows();
		$scheme_count=$count_scheme+1;
		$count_schemeamount=$this->db->where('chitschemeid',$scheme_id)->where('amount',$scheme_amount)->get('chitbook')->num_rows();
		$scheme_amount_count=$count_schemeamount+1;
		$chitbook_pattern=$this->db->select('chitbooknopattern')->where('chitschemeid',$scheme_id)->where('status',$this->Basefunctions->activestatus)->get('chitscheme')->row()->chitbooknopattern;
		$chitbook_pattern_explode=explode(',',$chitbook_pattern);
		$chitbook_pattern_count=count($chitbook_pattern_explode);
		$pattern_array=array();
		for($i=0; $i <$chitbook_pattern_count; $i++) {
			if($chitbook_pattern_explode[$i] == 'Scheme Prefix') {
				$pattern_array[$i]=$scheme_prefix;
			} else if($chitbook_pattern_explode[$i] == 'Scheme Name') {
				$pattern_array[$i]=$scheme_name;
			} else if($chitbook_pattern_explode[$i] == 'Amount Suffix') {
				$pattern_array[$i]=$scheme_amt_suffix;
			} else if($chitbook_pattern_explode[$i] == 'Amount') {
				$pattern_array[$i]=$scheme_amount;
			} else if($chitbook_pattern_explode[$i] == 'Scheme Count' ) {
				if(isset($chitbook_pattern_explode[$i+1])) {
					$pattern_array_digits = explode('-',$chitbook_pattern_explode[$i+1]);
					if(count($pattern_array_digits) == 1) {
						$pattern_array[$i]=$scheme_count;
					} else {
						if($pattern_array_digits[1] == 'DIGITS') {
							$pattern_array[$i]= str_pad($scheme_count,$pattern_array_digits[0],0,STR_PAD_LEFT); } else {$pattern_array[$i]=$scheme_count;}
					}
					$i++;
				} else {$pattern_array[$i]=$scheme_count;}
			} else if($chitbook_pattern_explode[$i] == 'Amount Count') {
				if(isset($chitbook_pattern_explode[$i+1])) {
					$pattern_array_digits = explode('-',$chitbook_pattern_explode[$i+1]);
					if(count($pattern_array_digits) == 1) {
						$pattern_array[$i]=$scheme_amount_count;
					} else {
						if($pattern_array_digits[1] == 'DIGITS') {
							$pattern_array[$i]= str_pad($scheme_amount_count,$pattern_array_digits[0],0,STR_PAD_LEFT); } else {$pattern_array[$i]=$scheme_amount_count;}
					}
					$i++;
				} else {
					$pattern_array[$i]=$scheme_amount_count;
				}
			} else{
				if (strpos($chitbook_pattern_explode[$i], '-') !== false) {
					$pattern_array_digits = explode('-',$chitbook_pattern_explode[$i]);
					if($pattern_array_digits[1] == 'DIGITS') {
						$pattern_array[$i]= str_pad($scheme_count,$pattern_array_digits[0],0,STR_PAD_LEFT); } else {$pattern_array[$i]=$scheme_count;}
				} else { $pattern_array[$i] = $chitbook_pattern_explode[$i];}
								
			}
		}
		$chitbookno=implode("",$pattern_array);
		$chitbooktokennopattern=$this->db->select('chitbooktokennopattern')->where('chitschemeid',$scheme_id)->where('status',$this->Basefunctions->activestatus)->get('chitscheme')->row()->chitbooktokennopattern;
		$chitluckydraw=$this->db->select('luckydraw')->where('chitschemeid',$scheme_id)->where('status',$this->Basefunctions->activestatus)->get('chitscheme')->row()->luckydraw;
		
		$loclingtime = $this->Basefunctions->get_company_settings('lockingtime');
		$updatingtime = $this->Basefunctions->get_company_settings('updatingtime');
		$currenttime = time("H:i:s");
		$ctime = new DateTime();
		$ctime->setTimestamp($currenttime);
		$ltime  = new DateTime();
		if($loclingtime == '00:00:00') {
			$ltime->setTimestamp($currenttime);
		} else {
			$ltime->setTimestamp(strtotime($loclingtime));
		}
		if($ctime > $ltime) {
			$chitbookdate = date("Y-m-d",strtotime("tomorrow"));
			$chitbookdate = $this->Basefunctions->checkgivendateisholidayornot($chitbookdate);
		} else {
			$chitbookdate=$this->Basefunctions->ymd_format($_POST['chitbookdate']);
		}
		if($chitluckydraw == 'YES') {
			$this->db->select_max('chitbookid');
			$this->db->where('chitschemeid',$scheme_id);
			$this->db->where('status',$this->Basefunctions->activestatus);
			$info=$this->db->get('chitbook');
			$last_chitbookid=$info->row()->chitbookid;
			if($last_chitbookid == '') {
				$last_tokenno=0;
			} else {
				$last_tokenno=$this->db->where('chitbookid',$last_chitbookid)->where('chitschemeid',$scheme_id)->where('status',$this->Basefunctions->activestatus)->get('chitbook')->row()->lasttokenno;
			}
			if($chitbooktokennopattern == 'scheme token') {
				$schme_explode = explode(',',$last_tokenno);
				$schme_explode_count = count($schme_explode);
				$explode_scheme=$schme_explode_count-1;
				$last_tokenno=$schme_explode[$explode_scheme];
				$tokenno_array=array();
				$credits_token=$_POST['giftcredits'];
				for($j=1; $j <= $credits_token; $j++) {
					$tokenno_array[$j]=++$last_tokenno;
				}
				$last_token_no=implode(",",$tokenno_array);
			} else if($chitbooktokennopattern == 'scheme amount token') {
				$last_token_no=$last_tokenno+1;
			} else { 
				$last_token_no=1; 
			}
			$tokencount=count(explode(',',$last_token_no));
		} else {
			$last_token_no=0;
			$tokencount=0;
		}
		$insert=array(
				'chitbookdate' => $chitbookdate,
				'chitbookname'=>$chit_bookname,
				'chitschemeid'=>$_POST['schemename'],
				'accountid'=>$_POST['chitbookaccountid'],
				'noofmonths'=>$_POST['noofmonths'],
				'amount'=>$scheme_amount,
				'firstmonthentry'=>$_POST['firstmonthentry'],
				'printtemplateid'=>$_POST['printtemplate'],
				'chitbookno'=>$chitbookno,
				'giftcredits'=>$_POST['giftcredits'],
				'lasttokenno'=>$last_token_no,
				'tokencount'=>$tokencount,
				'chitschemelevelcount'=>$scheme_count,
				'chitschemeamountlevelcount'=>$scheme_amount_count,
				'industryid'=>$this->Basefunctions->industryid
		);
		if($ctime > $ltime) {
			$insert = array_merge($insert,$this->Crudmodel->nextdaydefaultvalueget());
		} else {
			$insert = array_merge($insert,$this->Crudmodel->defaultvalueget());
		}
		$this->db->insert('chitbook',array_filter($insert));
		$primaryid = $this->db->insert_id();
		if($_POST['printtemplate'] == 'YES') {
			$this->db->select('chitbookid,regtemplateid,regtemplatetype',false);
			$this->db->from('chitbook');
			$this->db->join('chitscheme','chitscheme.chitschemeid = chitbook.chitschemeid');
			$this->db->where('chitbookno',$chitbookno);
			$this->db->where('chitbook.status',$this->Basefunctions->activestatus);
			$allchitregid=$this->db->get();
			foreach($allchitregid->result() as $allchitregid) {
				if($allchitregid->regtemplatetype == '2') {
					$print_data=array('templateid'=> $allchitregid->regtemplateid, 'Templatetype'=>'Chit Entry temp','primaryset'=>'chitbook.chitbookid','primaryid'=>$allchitregid->chitbookid,'parentid'=>$allchitregid->chitbookid,'where'=>'chitbook.chitbookid','join'=>'LEFT OUTER JOIN account ON account.accountid=chitbook.accountid  LEFT OUTER JOIN accountcf ON accountcf.accountid=account.accountid LEFT OUTER JOIN accountaddress as accountbilling ON accountbilling.accountid=account.accountid AND accountbilling.addressmethod=4');
				$data = $this->Printtemplatesmodel->generatlabelprint($print_data);
				}
			}
		}
		// Add chitbook entry while create chitbook
		if($_POST['firstmonthentry']>0 || !empty($_POST['firstmonthentry'])) {
			$this->Chitbookentrymodel->firstchitbookentry($this->Basefunctions->ymd_format($_POST['chitbookdate']),$chitbookno,$_POST['schemename'],$_POST['chitbookaccountid'],$scheme_amount,$_POST['firstmonthentry'],$this->Basefunctions->userid,$_POST['printtemplate'],$chitcashamount,$chitcardamount,$chitchequeamount,$chitinterestamount,$chitneftamount);
		}
		{//workflow management -- for data create
			$this->Basefunctions->workflowmanageindatacreatemode('153',$primaryid,'chitbook');
		}
		// notification log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' created chitbook - '.$chit_bookname.'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,153);
		echo 'SUCCESS';
	}
	// chit book no genearate
	public function chitbooknoget() {
		$id=$_GET['primaryid'];
		$this->db->select_max('chitbookid');
		$this->db->where('chitschemeid',$id);
		$this->db->where('status',1);
		$info=$this->db->get('chitbook');
		$last_chitbookid=$info->row()->chitbookid;
		$this->db->select('chitbook.chitbookno,chitbook.lasttokenno,chitbook.chitschemelevelcount');
		$this->db->from('chitbook');
		$this->db->where('chitbookid',$last_chitbookid);
		$info_last=$this->db->get();
		$info_last_row=$info_last->row();
		$info_numrows=$info_last->num_rows();
		$this->db->select('chitscheme.chitschemename,chitscheme.schemeprefix,chitscheme.chitlookupsid,chitscheme.purityid');
		$this->db->from('chitscheme');
		$this->db->where('chitschemeid',$id);
		$sheme_info=$this->db->get()->row();
		if($info_numrows == 0) {
			$jsonarray=array(
					'chitbookno'=>'',
					'lasttokenno'=>'',
					'chitschemelevelcount'=>'',
					'schemename'=>$sheme_info->chitschemename,
					'schemeprefix'=>$sheme_info->schemeprefix,
					'chitlookupsid'=>$sheme_info->chitlookupsid,
					'purityid'=>$sheme_info->purityid
				);
		} else {
			$schemetoken_pattern=$this->db->where('chitschemeid',$id)->get('chitscheme')->row()->chitbooktokennopattern;
			$scheme_last_token=$this->db->where('chitbookid',$last_chitbookid)->where('chitschemeid',$id)->get('chitbook')->row()->lasttokenno;
			if($schemetoken_pattern == 'scheme amount token') {
				$scheme_token=$scheme_last_token;
			} else {
				$schme_explode=explode(',',$scheme_last_token);
				$schme_explode_count=count($schme_explode);
				$explode_scheme=$schme_explode_count-1;
				$scheme_token=$schme_explode[$explode_scheme];
			}
			$jsonarray=array(
					'chitbookno'=>$info_last_row->chitbookno,
					'lasttokenno'=>$scheme_token,
					'chitschemelevelcount'=>$info_last_row->chitschemelevelcount,
					'schemename'=>$sheme_info->chitschemename,
					'schemeprefix'=>$sheme_info->schemeprefix,
					'chitlookupsid'=>$sheme_info->chitlookupsid,
					'purityid'=>$sheme_info->purityid
			);
		}
		echo json_encode($jsonarray);
	}
	//Scheme level Active chitbook details
	public function schemeactivecount() {
		$id = $_GET['primaryid'];
		$memberslimit = $this->Basefunctions->singlefieldfetch('memberslimit','chitschemeid','chitscheme',$id);
		$this->db->select('COUNT(chitbook.chitbookid) as schemeactivecount');
		$this->db->from('chitbook');
		$this->db->where('chitschemeid',$id);
		$this->db->where_in('status',array(1,11));
		$sheme_info = $this->db->get()->row();
		if($sheme_info->schemeactivecount != '0') {
			$schemependgcount = round($memberslimit - $sheme_info->schemeactivecount);
			$jsonarray=array(
				'memberslimit'=>$memberslimit,
				'schemelvlactivecount'=>$sheme_info->schemeactivecount,
				'schemependgcount'=>abs($schemependgcount),
				'recordsfound'=>'yes'
			);
		} else {
			$jsonarray=array(
				'memberslimit'=>$memberslimit,
				'schemelvlactivecount'=>0,
				'schemependgcount'=>0,
				'recordsfound'=>'no'
			);
		}
		echo json_encode($jsonarray);
	}
	//chit book retrieve
	public function chitbookretrive() {
		$id=$_GET['primaryid'];
		$this->db->select('chitbook.chitbookid,chitbook.chitschemeid,chitbook.accountid,chitbook.chitbookname,chitbook.noofmonths,chitbook.amount,chitbook.firstmonthentry,chitbook.chitbookno,chitbook.giftcredits,chitbook.lasttokenno');
		$this->db->from('chitbook');
		$this->db->where('chitbookid',$id);
		$info=$this->db->get()->row();
		$jsonarray=array(
				'schemeid'=>$info->chitschemeid,
				'chitbookname'=>$info->chitbookname,
				'noofmonths'=>$info->noofmonths,
				'amount'=>$info->amount,
				'firstmonthentry'=>$info->firstmonthentry,
				'chitbookno'=>$info->chitbookno,
				'giftcredits'=>$info->giftcredits,
				'chittoken'=>$info->lasttokenno,
				'accountid'=>$info->accountid
		);
		echo json_encode($jsonarray);
	}
	public function chitbookupdate() {
		$id=$_POST['primaryid'];
		$update=array(
				'chitbookname'=>$_POST['chitbookname'],
				'industryid'=>$this->Basefunctions->industryid
		);
		$update = array_merge($update , $this->Crudmodel->updatedefaultvalueget());
		$this->db->where('chitbookid',$id);
		$this->db->update('chitbook',$update);
		// notification log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' Updated chitbook - '.$_POST['chitbookname'].'';
		$this->Basefunctions->notificationcontentadd($id,'Updated',$activity ,$userid,152);
		echo 'SUCCESS';
	}
	public function chitbookdelete() {
		$id=$_GET['primaryid'];
		//inactivate chitbook
		$inactive = $this->Crudmodel->deletedefaultvalueget();
		$this->db->where('chitbookid',$id);
		$this->db->update('chitbook',$inactive);
		// inactive chitbook entry
		$chitbookdata=$this->db->where('chitbookid',$id)->get('chitbook');
		if($chitbookdata->num_rows()>0) {
			$chitbookno=$chitbookdata->row()->chitbookno;
			$this->db->where('chitbookno',$chitbookno);
			$this->db->update('chitentry',$inactive);
			$chitentrygroup=$this->db->where('chitbookno',$chitbookno)->get('chitentry');
			$i=0;
			foreach($chitentrygroup->result() as $chit_group) {
				$this->db->where('chitentrygroupid',$chit_group->chitentrygroupid);
				$this->db->update('chitentrygroup',$inactive);
				$i++;
			}
		}
		// notification log
		$chitbook = $this->Basefunctions->singlefieldfetch('chitbookname','chitbookid','chitbook',$id);
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' deleted chitbook - '.$chitbook.'';
		$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,152);
		echo 'SUCCESS';
	}
	public function stonsettingcreate() {
		$purity_val=ltrim($_POST['purity'],',');
		$purity=explode(',',$purity_val);
		$purityid=count($purity);
		for($i = 0; $i < $purityid; $i++) {
			$checkpurity=$this->db->where('purityid',$purity[$i])->where('accountid',$_POST['stoneaccountid'])->where('status',$this->Basefunctions->activestatus)->get('stonesetting');
			if($checkpurity->num_rows() >0) {
				$update=array(
						'netwtcalctypeid'=>$_POST['netwtcalctype'],
						'mccalctypeid'=>$_POST['mccalctype']
				);
				$update = array_merge($update , $this->Crudmodel->updatedefaultvalueget());
				$this->db->where('purityid',$purity[$i]);
				$this->db->where('status',$this->Basefunctions->activestatus);
				$this->db->update('stonesetting',$update);
			} else {
				$insert=array(
						'accountid'=>$_POST['stoneaccountid'],
						'purityid'=>$purity[$i],
						'netwtcalctypeid'=>$_POST['netwtcalctype'],
						'mccalctypeid'=>$_POST['mccalctype'],
						'status'=>$this->Basefunctions->activestatus
				);
				$insert = array_merge($insert,$this->Crudmodel->defaultvalueget());
				$this->db->insert('stonesetting',array_filter($insert));
				$primaryid = $this->db->insert_id();
				// notification log
				$user = $this->Basefunctions->username;
				$userid = $this->Basefunctions->logemployeeid;
				$activity = ''.$user.' created stone setting';
				$this->Basefunctions->notificationcontentadd($primaryid,'Created',$activity ,$userid,202);
			}
		}
		echo 'SUCCESS';
	}
	public function stonesettingretrive() {
		$id=$_GET['primaryid'];
		$this->db->select('purityid,netwtcalctypeid,mccalctypeid');
		$this->db->from('stonesetting');
		$this->db->where('stonesettingid',$id);
		$info=$this->db->get()->row();
		$jsonarray=array(
				'purity'=>$info->purityid,
				'netwtcalctype'=>$info->netwtcalctypeid,
				'mccalctype'=>$info->mccalctypeid
		);
		echo json_encode($jsonarray);
	}
	public function stonesettingupdate() {
		$id=$_POST['primaryid'];
		$update=array(
				'purityid'=>ltrim($_POST['purity'],','),
				'netwtcalctypeid'=>$_POST['netwtcalctype'],
				'mccalctypeid'=>$_POST['mccalctype']
		);
		$update = array_merge($update , $this->Crudmodel->updatedefaultvalueget());
		$this->db->where('stonesettingid',$id);
		$this->db->update('stonesetting',$update);
		// notification log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity = ''.$user.' updated stone setting';
		$this->Basefunctions->notificationcontentadd($id,'Updated',$activity ,$userid,202);
		echo 'SUCCESS';
	}
	public function stonesettingdelete() {
		$id=$_GET['primaryid'];
		//-inactivate stone setting
		$delete = $this->Crudmodel->deletedefaultvalueget();
		$this->db->where('stonesettingid',$id);
		$this->db->update('stonesetting',$delete);
		// notification log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity = ''.$user.' deleted stone setting';
		$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,202);
		echo 'SUCCESS';
	}
	// chit book amount get
	public function chitbookamountget() {
		$id=$_GET['primaryid'];
		$amount=$_GET['schemeamount'];
		$this->db->select_max('chitbookid');
		$this->db->from('chitbook');
		$this->db->where('chitschemeid',$id);
		$this->db->where('amount',$amount);
		$this->db->where('status',1);
		$info=$this->db->get()->row();
		$last_chitbookid=$info->chitbookid;
		$this->db->select('chitbook.chitschemeamountlevelcount');
		$this->db->from('chitbook');
		$this->db->where('chitschemeid',$id);
		$this->db->where('amount',$amount);
		$this->db->where('chitbookid',$last_chitbookid);
		$info_last=$this->db->get();
		$info_last_row=$info_last->row();
		$info_numrows=$info_last->num_rows();
		if( $info_numrows == 0)	{
			$jsonarray=array(
				'chitschemeamountlevelcount'=>''
			);
		} else {
			$jsonarray=array(
				'chitschemeamountlevelcount'=>$info_last_row->chitschemeamountlevelcount
			);
		}
		echo json_encode($jsonarray);
	}
	public function viewdynamicdatainfofetch($tablename,$sortcol,$sortord,$pagenum,$rowscount,$accountid) {
		$dataset ='stonesetting.stonesettingid,stonesetting.purityid,purity.purityname,
		stonecalctype.netwtcalctype,sct.mccalctype';			
		$join=' LEFT OUTER JOIN purity ON purity.purityid=stonesetting.purityid';		
		$join.=' LEFT OUTER JOIN stonecalctype ON stonecalctype.stonecalctypeid=stonesetting.netwtcalctypeid';
		$join.=' LEFT OUTER JOIN stonecalctype AS sct ON sct.stonecalctypeid=stonesetting.mccalctypeid';	
		$cuscondition=$tablename.'.accountid='.$accountid.' AND ';
		$status=$tablename.'.status='.$this->Basefunctions->activestatus.'';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		/* query */
		$result = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$cuscondition.' '. $status.' ORDER BY '.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$data=array();
		$i=0;
		foreach($result->result() as $row) {
			$stonesettingid = $row->stonesettingid;
			$purityname =$row->purityname;
			$netwtcalctype =$row->netwtcalctype;
			$mccalctype =$row->mccalctype;		
			
			$data[$i]=array('id'=>$stonesettingid,$purityname,$netwtcalctype,$mccalctype);
			$i++;
		}
		$finalresult=array('0'=>$data,'1'=>$page,'2'=>'json');
		return $finalresult;
	}
	public function chitbookprint() {
		$chitbookid = trim($_GET['primaryid']);
		$chitbookno = trim($_GET['chitbookno']);
		$accountid = $this->Basefunctions->singlefieldfetch('accountid','chitbookno','chitbook',$chitbookno);
		$this->db->select('chitbookid,regtemplateid,regtemplatetype',false);
		$this->db->from('chitbook');
		$this->db->join('chitscheme','chitscheme.chitschemeid = chitbook.chitschemeid');
		$this->db->where('chitbookno',$chitbookno);
		$this->db->where('chitbook.status',$this->Basefunctions->activestatus);
		$allchitregid=$this->db->get();
		foreach($allchitregid->result() as $allchitregid) {
			if($allchitregid->regtemplatetype == '2') {
				$print_data=array('templateid'=> $allchitregid->regtemplateid, 'Templatetype'=>'Chit Book temp','primaryset'=>'chitbook.chitbookid','primaryid'=>$allchitregid->chitbookid,'parentid'=>$allchitregid->chitbookid,'where'=>'chitbook.chitbookid','join'=>'LEFT OUTER JOIN account ON account.accountid=chitbook.accountid  LEFT OUTER JOIN accountcf ON accountcf.accountid=account.accountid LEFT OUTER JOIN accountaddress as accountbilling ON accountbilling.accountid=account.accountid AND accountbilling.addressmethod=4');
				$data = $this->Printtemplatesmodel->generatlabelprint($print_data);
			}
		}
		echo 'SUCCESS';
	}
	public function chitbookhtmlprint() {
		$chitbookid = trim($_GET['primaryid']);
		$chitbookno = trim($_GET['chitbookno']);
		$accountid = $this->Basefunctions->singlefieldfetch('accountid','chitbookno','chitbook',$chitbookno);
		$this->db->select('chitbookid,regtemplateid,regtemplatetype',false);
		$this->db->from('chitbook');
		$this->db->join('chitscheme','chitscheme.chitschemeid = chitbook.chitschemeid');
		$this->db->where('chitbookno',$chitbookno);
		$this->db->where('chitbook.status',$this->Basefunctions->activestatus);
		$allchitregid=$this->db->get();
		foreach($allchitregid->result() as $allchitregid) {
			if($allchitregid->regtemplatetype == '2') {
				$print_data=array('templateid'=> $allchitregid->regtemplateid, 'Templatetype'=>'Chit Book temp','primaryset'=>'chitbook.chitbookid','primaryid'=>$allchitregid->chitbookid,'parentid'=>$allchitregid->chitbookid,'where'=>'chitbook.chitbookid','join'=>'LEFT OUTER JOIN account ON account.accountid=chitbook.accountid  LEFT OUTER JOIN accountcf ON accountcf.accountid=account.accountid LEFT OUTER JOIN accountaddress as accountbilling ON accountbilling.accountid=account.accountid AND accountbilling.addressmethod=4');
				$data = $this->Printtemplatesmodel->generatlabelprint($print_data);
			}
		}
		echo 'SUCCESS';
	}
	public function checkrateforpurity(){
		$purityid ='';
		$purityid = trim($_POST['purityid']);
		$rate_row = $this->db->where('purityid',$purityid)->where('status',$this->Basefunctions->activestatus)->get('rate')->num_rows();
		echo $rate_row;
	}
	//account unique name verification in jewel module
	public function newaccountuniquedynamicviewnamecheckmodel() {
		$industryid = $this->Basefunctions->industryid;
		$primaryid=$_POST['primaryid'];
		$accname=$_POST['accname'];
		$accountypeid=$_POST['accountypeid'];
		$elementpartable = explode(',',$_POST['elementspartabname']);
		$partable =  $this->Basefunctions->filtervalue($elementpartable);
		$ptid = $partable.'id';
		$ptname = $partable.'name';
		if($partable == 'currency'){
			$ptname = 'currencycode';
		}
		if($accname != "") {
			$result = $this->db->select($ptname.','.$ptid)->from($partable)->where($partable.'.'.$ptname,$accname)->where($partable.'.status',1)->where($partable.'.accounttypeid',$accountypeid)->where("FIND_IN_SET('".$industryid."',industryid) >", 0)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->$ptid;
				} 
			} else { echo "False"; }
		}else { echo "False"; }
	}
	//parent account name fetch
	public function catalognamefetchmodel() {
		$id=$_GET['id'];
		$data=$this->db->select('accountcatalog.accountcatalogid,accountcatalog.accountcatalogname')
		->from('account')
		->join('accountcatalog','accountcatalog.accountcatalogid=account.accountcatalogid')
		->where('account.accountid',$id)
		->where('account.status',1)
		->get();
		if($data->num_rows() >0) {
			foreach($data->result() as $in)	{
				$accountcatalogid=$in->accountcatalogid;
				$accountcatalogname=$in->accountcatalogname;
			}
			$a = array('accountcatalogid'=>$accountcatalogid,'accountcatalogname'=>$accountcatalogname);
		} else {
			$a = array('accountcatalogid'=>'0','accountcatalogname'=>'');
		}
		echo json_encode($a);
	}
	// load referral name
	public function account_dd(){
		$i=0;
		$data = array();
		if(isset($_POST['q'])){
			if(is_numeric($_POST['q'])){
				$check = 'mobilenumber';
			} else{
				$check = 'accountname';
			}
			$filter =$_POST['q'];
		if(isset($_POST['acid'])){
			$acid =$_POST['acid'];
		} else{
			$acid ='1';
		}
		if(empty($_POST['type'])) {
			$type ='16,6,18,19';
		}else {
			$type =$_POST['type'];
		}
		if($acid != 1){
			$query = $this->db->query(
			"SELECT account.accountname,account.accountid,accounttype.accounttypename,account.accounttypeid,account.accounttaxstatus,account.mobilenumber
				FROM account
				JOIN accounttype ON accounttype.accounttypeid = account.accounttypeid
				WHERE account.status =1 and account.accountid in (".$acid.") and account.accounttypeid in (".$type.")
			");
		} else {
			if($check == 'accountname'){
				$query = $this->db->query(
						"SELECT account.accountname,account.accountid,accounttype.accounttypename,account.accounttypeid,account.accounttaxstatus,account.mobilenumber,accountcf.primaryarea
				FROM account
				JOIN accounttype ON accounttype.accounttypeid = account.accounttypeid
				LEFT OUTER JOIN accountcf ON accountcf.accountid = account.accountid
				WHERE account.status =1 and account.accounttypeid in (".$type.") and account.accountname like '%".$filter."%'
				");
			} else{
				$query = $this->db->query(
						"SELECT account.accountname,account.accountid,accounttype.accounttypename,account.accounttypeid,account.accounttaxstatus,account.mobilenumber,accountcf.primaryarea
				FROM account
				JOIN accounttype ON accounttype.accounttypeid = account.accounttypeid
				LEFT OUTER JOIN accountcf ON accountcf.accountid = account.accountid
				WHERE account.status =1 and account.accounttypeid in (".$type.") and account.mobilenumber like '%".$filter."%'
				");
			}
		}
		foreach($query->result() as $row){
			 if($row->primaryarea != null){
				$primary = $row->primaryarea; 
			 } else {
				 $primary ='empty'; 
			 }
			$data[$i] = array('id'=>$row->accountid,'text'=>$row->accountname,'accountname'=>$row->accountname,'accountid'=>$row->accountid,'accounttypename'=>$row->accounttypename,'accounttaxstatus'=>$row->accounttaxstatus,'mobilenumber'=>$row->mobilenumber,'primaryarea'=>$primary);
			$i++;
		} 
		} 
		echo json_encode($data);
	}
	//Chitbook - registrationform print via printtemplate
	function printtemplatedetails()	{
	    $templateid = $_POST['templateid'];
		$this->db->select('printtypeid,printername');
		$this->db->from('printtemplates');
		$this->db->where('printtemplates.printtemplatesid',$templateid);
		$result = $this->db->get();
		foreach($result->result() as $data) {
			$info = array('printtypeid'=>$data->printtypeid,'printername'=>$data->printername);
		}
		echo json_encode($info);
	}
	// load employeename
	public function loademployeename() {
		$result = array();
		$tablename = $_POST['table'];
		$data = $this->db->query("SELECT employee.employeeid, employee.employeename FROM `employee` LEFT JOIN ".$tablename." ON ".$tablename.".employeeid = employee.employeeid and ".$tablename.".status = 1 WHERE ".$tablename.".`employeeid` is null and employee.status = 1 and employee.userchittypeid = 3 GROUP BY employee.employeeid");
		$i=0;
		if($data->num_rows() > 0){
			foreach($data->result() as $user_name)
			{
				$result[]=array(
						'employeeid' => $user_name->employeeid,
						'employeename' => $user_name->employeename,
				);
				$i++;
			}
		}else{
			$result='';
		}
		echo json_encode($result);
	}
}