<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Account extends MX_Controller
{
	//To load the Register View
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		//$this->load->view('Base/formfieldgeneration');
		$this->load->model('Account/Accountsmodel');
	}
	public function index() {
		$moduleid = array(202,74,81,91,129);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid); 
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$data['branchid'] = $this->Basefunctions->branchid;
		$industryid = $this->Basefunctions->industryid;
		if($industryid == 3) {
			$data['purity']=$this->Basefunctions->purity_groupdropdown();
			$data['stonecalctype']=$this->Basefunctions->simpledropdown('stonecalctype','stonecalctypeid,netwtcalctype,mccalctype','stonecalctypeid');
			$data['stonestatus']= $this->Basefunctions->get_company_settings('stone');
			$data['schemestatus']= $this->Basefunctions->get_company_settings('schemestatus');
			$data['companyid'] = $this->Basefunctions->singlefieldfetch('companyid','branchid','branch',$this->Basefunctions->branchid);
			$data['weight_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
			$data['amount_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
			$data['accountmobilevalidation']= $this->Basefunctions->get_company_settings('accountmobilevalidation'); //Mobile Number validation (YES/NO)
			$data['creditinfohideshow']= $this->Basefunctions->get_company_settings('creditinfohideshow'); //Credit Info HideShow
			$data['accountmobileunique']= $this->Basefunctions->get_company_settings('accountmobileunique'); //Account - Mobile Number unique
			$data['chitauthrole']= $this->Basefunctions->get_company_settings('chituserroleauth'); //Authorization users who can use via company settings
			$data['chitemployeetypeid']= $this->Basefunctions->singlefieldfetch('employeetypeid','employeeid','employee',$this->Basefunctions->userid); //retrieve emp type
			$data['accountfieldcaps']= $this->Basefunctions->get_company_settings('accountfieldcaps'); //Credit Info HideShow
		}  
		$data['gstapplicable']=$this->Basefunctions->get_company_settings('gstapplicable');
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$this->load->view('Account/accountsview',$data);
	}
	//Create New data
	public function newdatacreate() {
    	$this->Accountsmodel->newdatacreatemodel();
    }
	//information fetch
	public function fetchformdataeditdetails() {
		$moduleid = array(202,74,81,91,129);
		$this->Accountsmodel->informationfetchmodel($moduleid);
	}
	//Update old data
    public function datainformationupdate() {
        $this->Accountsmodel->datainformationupdatemodel();
    }
	//delete old information
    public function deleteinformationdata() {
		$moduleid = array(202,74,81,91,129);
        $this->Accountsmodel->deleteoldinformation($moduleid);
    }
	//primary and secondary address value fetch
	public function primaryaddressvalfetch() {
		$this->Accountsmodel->primaryaddressvalfetchmodel();
	}
	//parent account name fetch
	public function parentaccountnamefetch() {
		$this->Accountsmodel->parentaccountnamefetchmodel();
	}
	//clone data
	public function viewdataclone() {
		$empid = $this->Basefunctions->userid;
		$vmaxid = $_POST['rowid'];
		$parenttable = 'account';
		$childtabinfo = 'accountaddress';
		$fieldinfo = 'accountid';
		$fieldvalue = $vmaxid;
		$updatecloumn = 'accountname';
		$viewid = $this->Basefunctions->viewdataclonemodel($parenttable,$childtabinfo,$fieldinfo,$fieldvalue);
		if($updatecloumn!='') {
			$this->db->query(' UPDATE '.$parenttable.' SET '.$updatecloumn.' = concat('.$updatecloumn.'," clone") where '.$fieldinfo.' = '.$viewid.'');
		}
		echo 'TRUE';
	}
	public function checkaccounttype() {
		$this->Accountsmodel->checkaccounttype();
	}
	// get account name
	public function accountname() {
		$this->Accountsmodel->accountname();
	}
	public function chitbookcreate() {
		$this->Accountsmodel->chitbookcreate();
	}
	// chit book no generate
	public function chitbooknoget() {
		$this->Accountsmodel->chitbooknoget();
	}
	// Scheme level Active chitbook details
	public function schemeactivecount() {
		$this->Accountsmodel->schemeactivecount();
	}
	// chitbook retrieve
	public function chitbookretrive() {
		$this->Accountsmodel->chitbookretrive();
	}
	public function chitbookupdate()
	{
		$this->Accountsmodel->chitbookupdate();
	}
	// chit book delete
	public function chitbookdelete() {
		$this->Accountsmodel->chitbookdelete();
	}
	// retrieve stone setting
	public function stonesettingview() {
		$grid = $this->Basefunctions->getpagination();
		$result = $this->Accountsmodel->stonesettingview($grid['sidx'],$grid['sord'],$grid['start'],$grid['limit'],$grid['wh']);
		$pageinfo = $this->Basefunctions->page();
		header("Content-type: text/xml;charset=utf-8");
		$s = "<?xml version='1.0' encoding='utf-8'?>";
		$s .=  "<rows>";
		$s .= "<page>".$_GET['page']."</page>";
		$s .= "<total>".$pageinfo['totalpages']."</total>";
		$s .= "<records>".$pageinfo['count']."</records>";
		foreach($result->result()as $row) {
			$s .= "<row id='". $row->stonesettingid."'>";
			$s .= "<cell><![CDATA[". $row->stonesettingid."]]></cell>";
			$s .= "<cell><![CDATA[". $row->purityname."]]></cell>";
			$s .= "<cell><![CDATA[". $row->netwtcalctype."]]></cell>";
			$s .= "<cell><![CDATA[". $row->mccalctype."]]></cell>";
			$s .= "</row>";
		} //exit;
		$s .= "</rows>";
		echo $s;
	}
	public function stonsettingcreate() {
		$this->Accountsmodel->stonsettingcreate();
	}
	public function stonesettingretrive() {
		$this->Accountsmodel->stonesettingretrive();
	}
	public function stonesettingupdate() {
		$this->Accountsmodel->stonesettingupdate();
	}
	public function stonesettingdelete() {
		$this->Accountsmodel->stonesettingdelete();
	}
	// chit book amount get
	public function chitbookamountget() {
		$this->Accountsmodel->chitbookamountget();
	}
	/* Fetch Main grid header,data,footer information */
	public function gridinformationfetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$account_id=$_GET['filter'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'stonesetting.stonesettingid') : 'stonesetting.stonesettingid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('purityname','netwtcalctype','mccalctype'),'colmodelindex'=>array('purity.purityname','stonecalctype.netwtcalctype','sct.mccalctype'),'coltablename'=>array('purity','stonecalctype','stonecalctype'),'uitype'=>array('2','2','2'),'colname'=>array('Purity Name','Net Weight Calc Type','MC Calc Type'),'colsize'=>array('200','200','200'));
		$result=$this->Accountsmodel->viewdynamicdatainfofetch($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$account_id);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Stonesetting',$width,$height);
		} else {
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Stonesetting',$width,$height);
		}
		echo json_encode($datas);
	}
	public function chitbookprint() {
		$this->Accountsmodel->chitbookprint();
	}
	public function chitbookhtmlprint() {
		$this->Accountsmodel->chitbookhtmlprint();
	}
	public function checkrateforpurity() {
		$this->Accountsmodel->checkrateforpurity();
	}
	//Account name unique check in jewel
	public function newaccountuniquedynamicviewnamecheck() {
		$this->Accountsmodel->newaccountuniquedynamicviewnamecheckmodel();
	}
	//catalog name fetch
	public function catalognamefetch() {
		$this->Accountsmodel->catalognamefetchmodel();
	}
	//load referralname
	public function account_dd(){
		$this->Accountsmodel->account_dd();
	}
	//Chitbook - registrationform print via printtemplate
	public function printtemplatedetails() {
		$this->Accountsmodel->printtemplatedetails();
	}
	//load employeename
	public function loademployeename() {
		$this->Accountsmodel->loademployeename();
	}
}