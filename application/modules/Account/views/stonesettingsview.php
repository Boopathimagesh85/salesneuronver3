<style type="text/css">
	#stonesettinggrid .gridcontent{
		height: 63vh !important;
	}
	#stonesettingoverlayclose {
		left:-20px !important;
		position:relative;
		font-size: 0.9rem;
    text-transform: uppercase;
    display: inline-block;
    font-family: 'Nunito Semibold', sans-serif;
    background-color: #E57276;
    color: #fff;
    padding-left: 10px;
    padding-right: 10px;
    height: 30px;
    box-sizing: border-box;
    position: relative;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    cursor: pointer;
    outline: 0;
    border: none;
    -webkit-tap-highlight-color: transparent;
    display: inline-block;
    white-space: nowrap;
    text-decoration: none;
    vertical-align: baseline;
    text-align: center;
    margin: 0;
    min-width: 64px;
    line-height: 19px;
    padding: 0 16px;
    border-radius: 4px;
    overflow: visible;
    transform: translate3d(0,0,0);
    transition: background .4s cubic-bezier(.25,.8,.25,1),box-shadow 280ms cubic-bezier(.4,0,.2,1);
	}
	#stonesettinggridfooter {
		display:none;
	}
</style>
<?php
	$device = $this->Basefunctions->deviceinfo();
	$dataset['gridtitle'] = 'Stone Setting';
	$dataset['titleicon'] = 'add_circle';
	$modtabgroup[0] = array("tabpgrpid"=>"1","tabgrpname"=>"Stone Setting","moduleid"=>"91","templateid"=>"2");
	$dataset['modtabgrp'] = $modtabgroup;
	$dataset['moduleid'] = '91';
	$dataset['otype'] = 'overlay';
	$dataset['action'][0] = array('actionid'=>'stonesettingoverlayclose','actiontitle'=>'Close','actionmore'=>'No','ulname'=>'chitbook');
	$this->load->view('Base/formwithgridmenuheader.php',$dataset);
?>	
<div class="" id="stonesettingoverlay">
		<div class="large-12 columns addformunderheader">&nbsp;</div>
		<div class="large-12 columns scrollbarclass addformcontainer">
		<div class="large-12 medium-12 small-12 large-centered medium-centered small-centered columns paddingzero">
			<div class="large-12 column paddingzero">
				<?php if($device=='phone') {?>
				<div class="closed effectbox-overlay effectbox-default" id="stonesettingsectionoverlay">
				<?php }?> 
				<div class="row mblhidedisplay">&nbsp;</div>
				<form id="stonesettingform" name="stonesettingform" class="cleardataform">
				<div id="addstonesettingvalidate" class="validationEngineContainer">
				<div id="editstonesettingvalidate" class="validationEngineContainer">
					<div class="large-4 columns end paddingbtm" style="top:-5px;">
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #fff">
							<?php if($device=='phone') {?>
							<div class="large-12 columns sectionheaderformcaptionstyle">Stone Setting</div>
							<div class="large-12 columns sectionpanel">
								<?php  } else { ?>
									<div class="large-12 columns headerformcaptionstyle">Stone Setting</div>
							<?php  	} ?> 
							<div class="static-field large-12 columns ">
							<label>Purity<span class="mandatoryfildclass">*</span></label>						
							<select class="chzn-select validate[required]" id="purity" name="purity" value="" tabindex="" multiple data-prompt-position="topLeft:14,36">
								<option value=""></option>
								<?php $prev = ' ';
								foreach($purity->result() as $key):
								$cur = $key->metalid;
								$a="";
								if($prev != $cur )
								{
									if($prev != " ")
									{
										 echo '</optgroup>';
									}
									echo '<optgroup  label="'.$key->metalname.'">';
									$prev = $key->metalid;
								}
								?>
								<option value="<?php echo $key->purityid;?>"><?php echo $key->purityname;?></option>
								<?php endforeach;?>
							</select>
						</div>
						<div class="static-field large-12 columns ">
							<label>Net Wt-Calculation Type<span class="mandatoryfildclass">*</span></label>
							<select id="netwtcalctype" name="netwtcalctype" class="chzn-select validate[required]" data-prompt-position="topLeft:14,36">
							<option value="">Select</option>
							<?php foreach($stonecalctype as $key):?>
								<option value="<?php echo $key->stonecalctypeid;?>">
									<?php echo $key->netwtcalctype;?></option>
							<?php endforeach;?>	
							</select>
						</div>
						<div class="static-field large-12 columns ">
							<label>MC-Calculation Type<span class="mandatoryfildclass">*</span></label>
							<select id="mccalctype" name="mccalctype" class="chzn-select validate[required]" data-prompt-position="topLeft:14,36">
							<option value="">Select</option>
							<?php foreach($stonecalctype as $key):
							if($key->mccalctype!=''){
							?>
								<option value="<?php echo $key->stonecalctypeid;?>">
									<?php echo $key->mccalctype;?></option>
							<?php } endforeach;?>	
							</select>
						</div>	
						<?php if($device=='phone') { ?>
							</div>
							<div class="divider large-12 columns"></div>
							<div class="large-12 columns submitkeyboard sectionalertbuttonarea" style="text-align: right">
								<input id="addstonesettingbtn" class="alertbtn addkeyboard" type="button" value="Submit" name="addstonesettingbtn" tabindex="103">
								<input id="updatestonesettingbtn" class="alertbtn hidedisplay updatekeyboard" type="button" value="Submit" name="updatestonesettingbtn" tabindex="103" style="display: none;">
								<input id="stonesettingsectionclose"class="alertbtn addsectionclose cancelkeyboard" type="button" value="Cancel" name="cancel" tabindex="104">
								<input type="hidden" value="" name="updatestonesettingid" id="updatestonesettingid">
								<input type="hidden" value="" name="stonesettingsortorder" id="stonesettingsortorder">
								<input type="hidden" value="" name="stonesettingsortcolumn" id="stonesettingsortcolumn">
								<input type="hidden" value="" name="stoneaccountid" id="stoneaccountid">
							</div>
								
							<?php } else { ?> 	
							<div class="large-12 columns" style="text-align: right">
								<label> </label>
								<input type="hidden" value="" name="updatestonesettingid" id="updatestonesettingid">
								<input type="hidden" value="" name="stonesettingsortorder" id="stonesettingsortorder">
								<input type="hidden" value="" name="stonesettingsortcolumn" id="stonesettingsortcolumn">
								<input type="hidden" value="" name="stoneaccountid" id="stoneaccountid">
								<input id="addstonesettingbtn" class="btn" type="button" value="Submit" name="addstonesettingbtn">
								<input id="updatestonesettingbtn" class="btn" type="button" value="Submit" name="updatestonesettingbtn" style="display:none;">
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
				</div>
				</form>
				<?php if($device=='phone') {?>
				</div>
				<?php }?>
				<div class="large-8 columns paddingbtm"  style="top:5px;">
					<div class="large-12 columns paddingzero">
						<div class="large-12 columns viewgridcolorstyle paddingzero  borderstyle">
							<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0 0rem;">
								<span class="large-6 medium-6 small-6 columns text-left" >Stone Setting List</span>
									<span class="large-6 medium-6 small-6 columns innergridicon righttext">
										<?php if($device=='phone') {?>
										  <span id="stonesettingaddicon" class="icon-box" title="Add" ><i class="material-icons">add</i></span>
										  <?php }?> 
										<span class="icon-box" title="EDIT" id="stoneedit"><i class="material-icons">edit</i></span>
										<span class="icon-box" title="Delete"id="stonedeleteicon"><i class="material-icons">delete</i></span>
									</span>
							</div>
							<?php
									$device = $this->Basefunctions->deviceinfo();
									if($device=='phone') {
										echo '<div class="large-12 columns paddingzero forgetinggridname" id="stonesettinggridwidth"><div class=" inner-row-content inner-gridcontent" id="stonesettinggrid" style="height:68vh !important; top:0px;">
											<!-- Table header content , data rows & pagination for mobile-->
										</div>
										<footer class="inner-gridfooter footercontainer" id="stonesettinggridfooter">
											<!-- Footer & Pagination content -->
										</footer></div>';
									} else {
										echo '<div class="large-12 columns forgetinggridname" id="stonesettinggridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="stonesettinggrid" style="max-width:2000px; height:68vh !important; top:0px;">
										<!-- Table content[Header & Record Rows] for desktop-->
										</div>
										<div class="inner-gridfooter footer-content footercontainer" id="stonesettinggridfooter">
											<!-- Footer & Pagination content -->
										</div></div>';
									}
								?>	
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>