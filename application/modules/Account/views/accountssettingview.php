<style type="text/css">
	#chitbookgrid .gridcontent{
		height: 63vh !important;
	}
	#chitbookoverlayclose {
		left:-20px !important;
		position:relative;
		font-size: 0.9rem;
    text-transform: uppercase;
    display: inline-block;
    font-family: 'Nunito Semibold', sans-serif;
    background-color: #E57276;
    color: #fff;
    padding-left: 10px;
    padding-right: 10px;
    height: 30px;
    box-sizing: border-box;
    position: relative;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    cursor: pointer;
    outline: 0;
    border: none;
    -webkit-tap-highlight-color: transparent;
    display: inline-block;
    white-space: nowrap;
    text-decoration: none;
    vertical-align: baseline;
    text-align: center;
    margin: 0;
    min-width: 64px;
    line-height: 19px;
    padding: 0 16px;
    border-radius: 4px;
    overflow: visible;
    transform: translate3d(0,0,0);
    transition: background .4s cubic-bezier(.25,.8,.25,1),box-shadow 280ms cubic-bezier(.4,0,.2,1);
	}
	#chitbookgridfooter .rppdropdown {
	left:62% !important;	
	}
</style>

<?php
	$device = $this->Basefunctions->deviceinfo();
	$dataset['gridtitle'] = 'Chit Book Creation';
	$dataset['titleicon'] = 'material-icons library_books';
	$modtabgroup[0] = array("tabpgrpid"=>"1","tabgrpname"=>"Chit Book Creation","moduleid"=>"91","templateid"=>"2");
	$dataset['modtabgrp'] = $modtabgroup;
	$dataset['moduleid'] = '91';
	$dataset['otype'] = 'overlay';
	$dataset['action'][0] = array('actionid'=>'chitbookoverlayclose','actiontitle'=>'Close','actionmore'=>'No','ulname'=>'chitbook');
	$this->load->view('Base/formwithgridmenuheader.php',$dataset);
?>
<div class="" id="chitbookoverlay">
		<div class="large-12 columns addformunderheader">&nbsp;</div>
		<div class="large-12 columns scrollbarclass addformcontainer">
		<div class="large-12 medium-12 small-12 large-centered medium-centered small-centered columns paddingzero">
			<div class="large-12 column paddingzero">
				<?php if($device=='phone') {?>
				<div class="closed effectbox-overlay effectbox-default" id="chitbooksectionoverlay">
				<?php }?> 
				<div class="row mblhidedisplay">&nbsp;</div>
				<form id="chitbookform" name="chitbookform" class="cleardataform">
				<div id="addchitbookvalidate" class="validationEngineContainer">
				<div id="editchitbookvalidate" class="validationEngineContainer">
					<div class="large-4 columns end paddingbtm">
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff;top:-4px;">
							<?php if($device=='phone') {?>
							<div class="large-12 columns sectionheaderformcaptionstyle">Chitbook Details</div>
							<div class="large-12 columns sectionpanel">
								<?php  } else { ?>
									<div class="large-12 columns headerformcaptionstyle">Chitbook Details </div>
							<?php  	} ?> 
							<div class="input-field large-6 columns ">
								<input id="chitbookdate" class="fordatepicicon validate[required]"  type="text" data-dateformater="dd-mm-yy" data-prompt-position="topLeft" readonly="readonly" name="chitbookdate" >
								<label for="chitbookdate">Date<span class="mandatoryfildclass">*</span></label>
						    </div>
							<div class="input-field large-6 columns">
								<input type="text" id="chitbookaccountname" name="chitbookaccountname" class=""  disabled>
								<label for="chitbookaccountname">Account Name<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="static-field large-6 columns">
								<label>Scheme Name<span class="mandatoryfildclass">*</span></label>
								<select id="schemename" name="schemename" class="chzn-select validate[required]" data-prompt-position="topLeft:14,36">
								<option value="">Select</option> 
								</select>
							</div>
							<div class="static-field large-6 columns" id="schemeamountdivhid">
								<label>Amount<span class="mandatoryfildclass">*</span></label>
								<select id="schemeamount" name="schemeamount" data-amountprefix="" class="chzn-select validate[required]" data-prompt-position="topLeft:14,36" data-placeholder="select" >
								<option value="">Select</option>
								</select>
							</div>
							<div class="static-field large-6 columns hidedisplay" id="bothschemeamountdivhid">
								<input type="text" id="bothschemeamount" name="bothschemeamount" class="validate[required,custom[number]]" value="" >
								<label for="bothschemeamount">Amount<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="input-field large-6 columns">
								<input type="text" id="chitbookname" name="chitbookname" value="" class="" />
								<label for="chitbookname">Chit Book Name</label>
							</div>
							<div class="static-field large-6 columns hidedisplay">
							    <label>Print Template</label>			
								<select id="printtemplate" data-tags="true" name="printtemplate"  class="chzn-select" aria-hidden="true" data-prompt-position="topLeft:14,36" >
										<option value="YES">YES</option>
										<option value="NO">NO</option>
									</select> 
						        </div>
							<div class="input-field large-6 columns">
								<input type="text" id="firstmonthentry" name="firstmonthentry" value="" class="validate[required,max[1],custom[onlyWholeNumber]]" />
								<label for="firstmonthentry">Months Entry<span class="mandatoryfildclass">*</span></label>
							</div>
							
							<div class="input-field large-6 columns">
								<input type="text" id="chitbookno" name="chitbookno" value="" class="" readonly>
								<label for="chitbookno">Last Chitbook No</label>
							</div>
							<div class="input-field large-6 columns hidedisplay">
								<input type="text" id="schemecount" name="schemecount" value="" class="" readonly>
								<label for="schemecount">Scheme Count</label>
							</div>
							<div class="input-field large-6 columns hidedisplay">
								<input type="text" id="noofmonths" name="noofmonths" value="" class="" readonly />
								<label for="noofmonths">Months</label>
							</div>
							<div class="input-field large-6 columns chit_token_no">
								<input type="text" id="chittoken" name="chittoken" value="" class="" readonly />
								<label for="chittoken">Last Token No</label>
							</div>
							<div class="input-field large-6 columns hidedisplay">
								<input type="text" id="schemeamtcount" name="schemeamtcount" value="" class="" readonly />
								<label for="schemeamtcount">Scheme Chitbook Count</label>
							</div>
							<div class="input-field large-6 columns hidedisplay">
								<input type="text" id="giftcredits" name="giftcredits" value="" class="" readonly />
								<label for="giftcredits">Gift Credits</label>
							</div>
							<div class="input-field large-6 columns">
								<input type="text" id="chitcashamount" name="chitcashamount" value="0" class="validate[custom[number]]" />
								<label for="chitcashamount">Cash</label>
							</div>
							<div class="input-field large-6 columns">
								<input type="text" id="chitcardamount" name="chitcardamount" value="0" class="validate[custom[number]]" />
								<label for="chitcardamount">Card</label>
							</div>
							<div class="input-field large-6 columns">
								<input type="text" id="chitchequeamount" name="chitchequeamount" value="0" class="validate[custom[number]]" />
								<label for="chitchequeamount">Cheque</label>
							</div>
							<div class="input-field large-6 columns">
							<input type="text" id="chitneftamount" name="chitneftamount" value="0" class="validate[custom[number]]" />
								<label for="chitneftamount">NEFT</label>
							</div>
							<div class="input-field large-6 columns hidedisplay">			
								<input type="text" id="chitinterestamount" name="chitinterestamount" value="0" class="" readonly />
								<label for="chitinterestamount">Interest</label>
							</div>
							<?php if($device=='phone') { ?>
							</div>
							<div class="divider large-12 columns"></div>
							<div class="large-12 columns submitkeyboard sectionalertbuttonarea" style="text-align: right">
								<input id="addchitbookbtn" class="alertbtn addkeyboard" type="button" value="Submit" name="addchitbookbtn" tabindex="103">
								<input id="editchitbookbtn" class="alertbtn hidedisplay updatekeyboard" type="button" value="Submit" name="editchitbookbtn" tabindex="103" style="display: none;">
								<input id="chitbooksectionclose"class="alertbtn addsectionclose cancelkeyboard" type="button" value="Cancel" name="cancel" tabindex="104">
								<input type="hidden" name="chitbookaccountid" id="chitbookaccountid">
								<input type="hidden" name="chitbooksortorder" id="chitbooksortorder">
								<input type="hidden" name="chitbooksortcolumn" id="chitbooksortcolumn">
								<input type="hidden" name="updatechitbookid" id="updatechitbookid">
								<input type="hidden" name="chitbookschemebaseamount" id="chitbookschemebaseamount">
								<input type="hidden" name="chitschemename" id="chitschemename">
								<input type="hidden" name="chitschemeprefix" id="chitschemeprefix">
								<input type="hidden" name="schemeamtprefix" id="schemeamtprefix">
								<input type="hidden" name="chitbookprimaryname" id="chitbookprimaryname">
								<input type="hidden" name="chitbkhidid" id="chitbkhidid">
								<input type="hidden" name="chit_account_name" id="chit_account_name">
								<input type="hidden" name="chitlookupsid" id="chitlookupsid">
								<input type="hidden" name="purityid" id="purityid">
								<input type="hidden" name="memberslimit" id="memberslimit">
								<input type="hidden" name="schemelvlactivecount" id="schemelvlactivecount">
								<input type="hidden" name="schemependgcount" id="schemependgcount">
								<input type="hidden" name="recordsfound" id="recordsfound">
							</div>
								
							<?php } else { ?> 
							<div class="large-12 columns" style="text-align: right">
								<label> </label>
								<input type="hidden" name="chitbookaccountid" id="chitbookaccountid">
								<input type="hidden" name="chitbooksortorder" id="chitbooksortorder">
								<input type="hidden" name="chitbooksortcolumn" id="chitbooksortcolumn">
								<input type="hidden" name="updatechitbookid" id="updatechitbookid">
								<input type="hidden" name="chitbookschemebaseamount" id="chitbookschemebaseamount">
								<input type="hidden" name="chitschemename" id="chitschemename">
								<input type="hidden" name="chitschemeprefix" id="chitschemeprefix">
								<input type="hidden" name="schemeamtprefix" id="schemeamtprefix">
								<input type="hidden" name="chitbookprimaryname" id="chitbookprimaryname">
								<input id="addchitbookbtn" class="btn" type="button" value="Submit" name="">
								<input id="editchitbookbtn" class="btn hidedisplay" type="button" value="Submit" name="">
								<input type="hidden" name="chitbkhidid" id="chitbkhidid">
								<input type="hidden" name="chit_account_name" id="chit_account_name">
								<input type="hidden" name="chitlookupsid" id="chitlookupsid">
								<input type="hidden" name="purityid" id="purityid">
								<input type="hidden" name="memberslimit" id="memberslimit">
								<input type="hidden" name="schemelvlactivecount" id="schemelvlactivecount">
								<input type="hidden" name="schemependgcount" id="schemependgcount">
								<input type="hidden" name="recordsfound" id="recordsfound">
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
				</div>
				</form>
				<?php if($device=='phone') {?>
				</div>
				<?php }?> 
				<div class="large-8 columns paddingbtm">
					<div class="large-12 columns paddingzero">
						<div class="large-12 columns viewgridcolorstyle paddingzero borderstyle" style="top: 4px;">
							<div class="large-12 columns headerformcaptionstyle" style="height:auto;padding: 0.2rem 0 0rem;">
								<span class="large-6 medium-6 small-6 columns text-left small-only-text-center" >Chit Book List</span>
								<span class="large-6 medium-6 small-6 columns innergridicon righttext small-only-text-center">
										<?php if($device=='phone') {?>
										  <span id="chitbookaddicon" class="icon-box" title="Add" ><i class="material-icons">add</i></span>
										  <?php }?> 
										 <span id="chitbookediticon" class="icon-box" title="Edit" ><i class="material-icons">edit</i></span>
					<span id="chitbookdeleteicon" class="icon-box" title="Delete"><i class="material-icons">delete</i> </span>
					<span id="chitbookprinticon" title="Print"><i class="material-icons printiconclass icon-box">print</i> </span>
					<span id="chitbookhtmlprinticon" title="Print"><i class="material-icons detailedviewiconclass icon-box">chrome_reader_mode</i> </span> 
					</div>
					<?php
							$device = $this->Basefunctions->deviceinfo();
							if($device=='phone') {
								echo '<div class="large-12 columns paddingzero forgetinggridname" id="chitbookgridwidth"><div class=" inner-row-content inner-gridcontent" id="chitbookgrid" style="height:68vh !important;top:0px;">
									<!-- Table header content , data rows & pagination for mobile-->
								</div>
								<footer class="inner-gridfooter footercontainer" id="chitbookgridfooter">
									<!-- Footer & Pagination content -->
								</footer></div>';
							} else {
								echo '<div class="large-12 columns forgetinggridname" id="chitbookgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="chitbookgrid" style="max-width:2000px; height:68vh !important;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
								<div class="inner-gridfooter footer-content footercontainer" id="chitbookgridfooter">
									<!-- Footer & Pagination content -->
								</div></div>';
							}
				?>
				    	</div>
					</div>
				</div>
			</div>
		</div>
		</div>		
	</div>
	
	