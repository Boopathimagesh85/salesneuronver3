<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Header Files -->
	<?php $this->load->view('Base/headerfiles'); ?>
	<?php 
		$industryid = $this->Basefunctions->industryid;
		if($industryid == 3) {		
	?>
	<link href="<?php echo base_url();?>css/jqgrid.min.css" media="screen" rel="stylesheet" type="text/css" />
	<script src="<?php echo base_url();?>js/plugins/autocomplete/jquery-ui.js" type="text/javascript"></script>
	<link rel="stylesheet" href="<?php echo base_url();?>css/jquery-ui.css">
	<?php 
	 }
	?>
</head>
<body>
	<?php
		$device = $this->Basefunctions->deviceinfo();
		$dataset['gridenable'] = "yes"; //no
		$dataset['griddisplayid'] = "acccreationview";
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['spanattr'] = array();
		$dataset['gridtableid'] = "leadaccountsgrid";
		$dataset['griddivid'] = "leadaccountsgriddiv";
		$dataset['moduleid'] = $moduleids;
	    if($industryid == 3) {
			$dataset['forminfo'] = array(array('id'=>'acccreationformadd','class'=>'hidedisplay','formname'=>'accountscreationform'),array('id'=>'accountssettingview','class'=>'hidedisplay','formname'=>'accountssettingview'),array('id'=>'stonesettingsview','class'=>'hidedisplay','formname'=>'stonesettingsview'));
		}else{
			$dataset['forminfo'] = array(array('id'=>'acccreationformadd','class'=>'hidedisplay','formname'=>'accountscreationform'));
		}
		 if($device=='phone') {
			$this->load->view('Base/gridmenuheadermobile',$dataset);
			$this->load->view('Base/overlaymobile');
			$this->load->view('Base/viewselectionoverlay');
		} else {
			$this->load->view('Base/gridmenuheader',$dataset);
			$this->load->view('Base/overlay');
		
		}
		$this->load->view('Base/basedeleteform');
		$this->load->view('Base/modulelist');
	?>
	</body>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js File -->
	<script src="<?php echo base_url();?>js/Account/account.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/treemenu/modernizr.custom.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/signature_pad.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/say-cheese.js"></script>
	<!--<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>-->
	<script>
		$(function() {
			$('#dl-menu').dlmenu({
				animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
			});
		});
	</script>
	<!--For Tablet and Mobile view Dropdown Script-->
	<script>
		$(document).ready(function() {			
			$("#tabgropdropdown").change(function() {
				var tabgpid = $("#tabgropdropdown").val();
				$('.sidebaricons[data-subform="'+tabgpid+'"]').trigger('click');
			});
		});
	</script>
	
</html>