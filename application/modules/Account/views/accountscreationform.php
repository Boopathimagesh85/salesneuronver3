<style type="text/css">
.large-12 .columns .scrollbarclass .addformcontainer .tabtouchaddcontainer {
	height:100% !important;
}
.wrapper {
	position: relative;
	width: 400px;
	height: 200px;
	-moz-user-select: none;
	-webkit-user-select: none;
	-ms-user-select: none;
	user-select: none;
}
</style>
<div class="large-12 columns paddingzero formheader">
	<?php
		$this->load->view('Base/mainviewaddformheader');
		$loggedinuserroleid = $this->Basefunctions->userroleid;
	?>
</div>
<div class="large-12 columns addformunderheader">&nbsp;</div>
<?php
$device = $this->Basefunctions->deviceinfo();
if($device=='phone') {
	echo '<div class="large-12 columns scrollbarclass addformcontainer tabtouchaddcontainer" style="background: #f2f3fa !important;">';			
} else {
	echo '<div class="large-12 columns scrollbarclass addformcontainer tabtouchaddcontainer">';
}
?>
	<div class="row mblhidedisplay">&nbsp;</div>
	<form method="POST" name="dataaddform" class="" action ="" id="dataaddform">
		<span id="formaddwizard" class="validationEngineContainer" >
			<span id="formeditwizard" class="validationEngineContainer">
				<?php
					$this->load->view('Base/formfieldgeneration');
					//function call for form fields generation
					formfieldstemplategenerator($modtabgrp);
				?>
			</span>
		</span>
		<!--hidden values-->
		<input type="hidden" value="<?php echo $gstapplicable?>" name="gstapplicableid" id="gstapplicableid" />
		<?php
			echo hidden('primarydataid','');
			echo hidden('chitlastaccountid','');
			echo hidden('sortorder','');
			echo hidden('sortcolumn','');
			echo hidden('signatureimgpath','');
			$value = 'accountaddress';
			echo hidden('resctable',$value);
			//hidden text box view columns field module ids
			$modid = $this->Basefunctions->getmoduleid($filedmodids);
			echo hidden('viewfieldids',$modid);
			echo hidden('branchidval',$branchid);
			$industryid = $this->Basefunctions->industryid;
			$defaultrecordview = $this->Basefunctions->get_company_settings('mainviewdefaultview');
			if($industryid == 3) {
				echo hidden('stonestatus',$stonestatus);
				echo hidden('schemestatus',$schemestatus);
				echo hidden('companyidval',$companyid);
				echo hidden('weight_round',$weight_round);
				echo hidden('amount_round',$amount_round);
				echo hidden('accountmobilevalidation',$accountmobilevalidation);
				echo hidden('creditinfohideshow',$creditinfohideshow);
				echo hidden('accountmobileunique',$accountmobileunique);
				echo hidden('chitauthrole',$chitauthrole);
				echo hidden('userroleid',$loggedinuserroleid);
				echo hidden('chitemployeetypeid',$chitemployeetypeid);
				echo hidden('mainviewdefaultview',$defaultrecordview);
				echo hidden('accountfieldcaps',$accountfieldcaps);
			}		
		?>
	</form>
</div>
