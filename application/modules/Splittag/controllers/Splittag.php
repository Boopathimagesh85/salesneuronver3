<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Splittag extends MX_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Splittag/Splittagmodel');
		$this->load->model('Base/Basefunctions');
    }
	//first basic hitting view
    public function index() {	
		$moduleid = array(140);
		sessionchecker($moduleid);
		$userid = $this->Basefunctions->userid;
		$data['account'] = $this->Basefunctions->simpledropdown_var('account','accountid,accountname','accountid');
		//action		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['rate_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //rate round-off
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['product'] = $this->Basefunctions->product_dd();
		$data['purity'] = $this->Basefunctions->purity_groupdropdown();
		$data['counter'] = $this->Basefunctions->counter_groupdropdown();
		$data['notdefaultcounter'] = $this->Basefunctions->counter_groupdropdown_notdefault();
		$data['stonename'] = $this->Basefunctions->stone_groupdropdown();
		$data['stoneentrycalctype'] = $this->Basefunctions->simpledropdown('stoneentrycalctype','stoneentrycalctypeid,stoneentrycalctypename','stoneentrycalctypeid');
		$data['sizename'] = $this->Splittagmodel->size_groupdropdown();
		$data['account'] = $this->Basefunctions->accountgroup_dd('16');
		$data['weight_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$data['dia_weight_round'] =$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',8); //Diamond weight round off
		$data['amount_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$data['melting_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',4); //melting round-off
		$data['rate_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //rate round-off
		$data['counter_val'] = $this->Basefunctions->get_company_settings('counter'); // deacitivate to counter dropdown validation
		$data['netwtcalctypeid'] = $this->Basefunctions->get_company_settings('netwtcalctypeid'); // Stone Net Wet Cal
		$data['stonepiecescalc'] = $this->Basefunctions->get_company_settings('stonepiecescalc');
		$data['tag_fieldrule'] = $this->Basefunctions->retrivemodulerule($moduleid[0],'itemtagform');
		//stone,image,addon-license.
		$data['mainlicense'] = array(
			'stone'=>$this->Basefunctions->get_company_settings('stone'),
			'image'=>$this->Basefunctions->get_company_settings('image'),
			'autoweight'=>$this->Basefunctions->get_company_settings('weightscale'),
			'weightscale'=>$this->Basefunctions->get_company_settings('weightscale'),
			'counter'=>$this->Basefunctions->get_company_settings('counter'),
			'lot'=>$this->Basefunctions->get_company_settings('lotdetail'),
			'tagtype'=>$this->Basefunctions->get_company_settings('tagtype'),
			'purchasedata'=>$this->Basefunctions->get_company_settings('purchasedata'),
			'rfidoption'=>$this->Basefunctions->get_company_settings('barcodeoption'),
			'collection'=>$this->Basefunctions->get_company_settings('collection'),
			'stocknotify'=>$this->Basefunctions->get_company_settings('stocknotify'),
			'vacchargeshow'=>$this->Basefunctions->get_company_settings('vacchargeshow'),
			'chargeaccountgroup'=>$this->Basefunctions->get_company_settings('chargeaccountgroup'),
			'chargecalcoption'=>$this->Basefunctions->get_company_settings('chargecalcoption'),
			'chargecalculation'=>$this->Basefunctions->get_company_settings('chargecalculation'),
			'productidshow'=>$this->Basefunctions->get_company_settings('productidshow'),
			'lotpieces'=>$this->Basefunctions->get_company_settings('lotpieces'),
			'storagequantity'=>$this->Basefunctions->get_company_settings('storagequantity'),
			'storagedisplay'=>$this->Basefunctions->get_company_settings('storagedisplay'),
			'grossweighttrigger'=>$this->Basefunctions->get_company_settings('grossweighttrigger'),
			'vendordisable'=>$this->Basefunctions->get_company_settings('vendordisable'),
			'storagedisplay'=>$this->Basefunctions->get_company_settings('storagedisplay'),
			'storagequantity'=>$this->Basefunctions->get_company_settings('storagequantity'),
			'counter'=>$this->Basefunctions->get_company_settings('counter')
		);
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$this->load->view('splittagview',$data);
    }
	//splittag create
	function splittagcreate(){
		$this->Splittagmodel->splittagcreatemodel(); 
	}
	//tag number data retrive
	function retrievetagdata(){
		$this->Splittagmodel->retrievetagdata(); 
	}
	//work flow condition header information fetch
	public function splittaginnergridheaderfetch() {
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$modname = $_GET['modulename'];
		$colinfo = $this->Splittagmodel->splittaginnergridheaderfetch($modid);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height);
		} else {
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height);
		}
		echo json_encode($datas);
	}
	// Retrieve Live Stone details
	public function autostonegridheaderinformationfetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$gridstoneids = $_GET['gridstoneids'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$account_id=$_GET['filter'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'stoneentry.stoneentryid') : 'stoneentry.stoneentryid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'ASC') : 'ASC';
		$colinfo = array('colmodelname'=>array('stoneid','stonename','stonetypeid','stonetypename','stoneentrycalctypeid','stoneentrycalctypename','purchaserate','basicrate','stonepieces','stonegram','caratweight','totalcaratweight','totalweight','stonetotalamount'),'colmodelindex'=>array('stone.stoneid','stone.stonename','stonetype.stonetypeid','stonetype.stonetypename','stoneentrycalctype.stoneentrycalctypeid','stoneentrycalctype.stoneentrycalctypename','stoneentry.purchaserate','stoneentry.basicrate','stoneentry.stonepieces','stoneentry.stonegram','stoneentry.caratweight','stoneentry.totalcaratweight','stoneentry.totalweight','stoneentry.stonetotalamount'),'coltablename'=>array('stone','stone','stonetype','stonetype','stoneentrycalctype','stoneentrycalctype','stoneentry','stoneentry','stoneentry','stoneentry','stoneentry','stoneentry','stoneentry'),'uitype'=>array('2','2','2','2','2','2','2','2','2','2','2','2','2','2'),'colname'=>array('Stone Id','Stone Name','Stone Typeid','Stone Type Name','Stone Calc. ID','Stone Calc. Type','Purchase Rate','Sales Rate','Pieces','Gram Weight','Carat Weight','Total Carat Weight','Total Net Weight','Total Amount'),'colsize'=>array('200','200','200','200','200','200','200','200','200','200','200','200','200','200'));
		$result=$this->Splittagmodel->autostonegriddynamicdatainfofetch($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$primaryid,$gridstoneids);
		$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Autostoneitem',$width,$height,'true');
		echo json_encode($datas);
	}
	// Check Stone Complete
	public function checkallstonedatacomplete() {
		$this->Splittagmodel->checkallstonedatacomplete();
	}
	//stone json convert data
	public function stonejsonconvertdata() {
		$this->Splittagmodel->stonejsonconvertdata();	
	}
}