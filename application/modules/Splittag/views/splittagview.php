<!DOCTYPE html>
<html lang="en">
<head>
	<style>
		input[type=text].rowtext{height: 1.2rem; margin: 0;color: #222;}
	</style>
	<?php $this->load->view('Base/headerfiles'); ?>
	<?php $this->load->view('Base/basedeleteform'); ?>
	<?php 
		$industryid = $this->Basefunctions->industryid;
		if($industryid == 3) {		
	?>
	<link href="<?php echo base_url();?>css/jqgrid.min.css" media="screen" rel="stylesheet" type="text/css" />
	<?php 
	 }
	?>
    </head>
<body>
	<?php
		$moduleid = implode(',',$moduleids);
		$device = $this->Basefunctions->deviceinfo();
		$dataset['gridenable'] = "yes"; //no
		$dataset['griddisplayid'] = "splittagcreationview";
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['spanattr'] = array();
		$dataset['gridtableid'] = "splittaggrid";
		$dataset['moduleid'] = $moduleids;
		$dataset['griddivid'] = "splittaggriddiv";
		$dataset['forminfo'] = array(array('id'=>'splittagformadd','class'=>'hidedisplay','formname'=>'splittagform'),array('id'=>'stonedetailsform','class'=>'hidedisplay','formname'=>'stonedetailsform'));
		if($device=='phone') {
			$this->load->view('Base/gridmenuheadermobile',$dataset);
			$this->load->view('Base/overlaymobile');
			$this->load->view('Base/viewselectionoverlay');
		} else {
			$this->load->view('Base/gridmenuheader',$dataset);
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
		$this->load->view('Base/basedeleteform');
	?>
	<!--View Creation Overlay-->
				<div class="hidedisplay" id="viewcreationformdiv">
				</div>
	<?php
		$this->load->view('Base/basedeleteformforcombainedmodules');
	?>

<!---- Automatic Lot - Stone Overlay Grid with Details ----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="automaticstonedetailoverlay" style="overflow-y: auto;overflow-x: hidden;">		
		<div class="large-10 medium-10 small-10 large-centered medium-centered small-centered columns borderstyle" style="top:5% !important;">
			<form method="POST" name="autostonedetailform" style="" id="autostonedetailform" action=""  class="clearformbordergridoverlay">
				<span id="autostonedetailformvalidation" class="validationEngineContainer">
					<div class="row" style="background:#fff">&nbsp;</div>
					<!-- <div class="row" style="background:#fff">&nbsp;</div> -->
					<div class="row" style="background:#fff">
						<div class="large-12 columns sectionheaderformcaptionstyle" style="top: -10px;">
							Stone Detail
						</div>
						<?php
							echo '<div class="large-12 columns forgetinggridname" id="autostonegridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="autostonegrid" style="max-width:2000px; height:250px;top:0px;top: -40px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
							<!--<div class="inner-gridfooter footer-content footercontainer" id="autostonegridfooter">
									 Footer & Pagination content 
								</div>--></div>';
						?>
					</div>
					<div class="row" style="background:#fff">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right;top:15px;">
							<input type="button" id="autostonegridsubmit" name="" value="Submit" class="alertbtnyes" tabindex="1001">	
							<input type="button" id="autostonegridclose" name="" value="Close" tabindex="1002" class="alertbtnno  alertsoverlaybtn" >
						</div>
					</div>
					<div class="row" style="background:#fff">&nbsp;</div>
				</span>
				<input type="hidden" name="" id="" />
			</form>
		</div>
	</div>
</div>

</body>
<?php $this->load->view('Base/bottomscript'); ?>
<script src="<?php echo base_url();?>js/Splittag/splittag.js" type="text/javascript"></script>
</html>
