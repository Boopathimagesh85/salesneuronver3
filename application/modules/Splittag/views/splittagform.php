<?php
	$device = $this->Basefunctions->deviceinfo();
	$dataset['gridtitle'] = $gridtitle;
	$dataset['titleicon'] = $titleicon;
	$this->load->view('Base/mainformwithgridmenuheader.php',$dataset);
	$defaultrecordview = $this->Basefunctions->get_company_settings('mainviewdefaultview');
	echo hidden('mainviewdefaultview',$defaultrecordview);
?>
<input type ="hidden" id="counteryesno" value="<?php echo strtolower($mainlicense['counter']);?>">
<input type ="hidden" id="weight_round" value="<?php echo $weight_round;?>">
<input type ="hidden" id="dia_weight_round" value="<?php echo $dia_weight_round;?>">
<input type ="hidden" id="amount_round" value="<?php echo $amount_round;?>">
<input type ="hidden" id="melting_round" value="<?php echo $melting_round;?>">
<input type="hidden" id="storagedisplay" name="storagedisplay" value="<?php echo $mainlicense['storagedisplay']; ?>">
<input type="hidden" id="storagequantity" name="storagequantity" value="<?php echo $mainlicense['storagequantity']; ?>">
<?php if(strtolower($mainlicense['stone']) == 'yes') { $stoneyesno = 1; } else { $stoneyesno = 0;  } ?>
<input type="hidden" id="hiddenstoneyesno" name="hiddenstoneyesno" value="<?php echo $stoneyesno; ?>">
<input type="hidden" id="netwtcalid" name="netwtcalid" value="<?php echo $netwtcalctypeid; ?>">
<input type="hidden" id="stonepiecescalc" name="stonepiecescalc" value="<?php echo $stonepiecescalc; ?>">
<input type="hidden" id="gridcolnames" name="gridcolnames" value="stonegroupid,stonegroupname,stoneid,stonename,purchaserate,salesrate,stoneentrycalctypeid,stoneentrycalctypename,stonepieces,caratweight,stonegram,totalcaratweight,totalweight,stonetotalamount">
<input type="hidden" id="gridcoluitype" name="gridcoluitype" value="31,32,2,33,2,2,2,34,2,2,2,2,2,2">
<div class="large-12 columns addformunderheader">&nbsp; </div>
<div class="large-12 columns addformcontainer">
	<div class="row mblhidedisplay">&nbsp;</div>
	<?php
	if($device=='phone') 
	{
		?>
	<div id="subformspan1" class="large-12 columns end itemdetailsopacity"  style="opacity:1;position: relative; background: #f2f3fa;width: 98%;padding:0px !important;">
		<form method="POST" name="splittagform" class="validationEngineContainer splittagform"  id="splittagform">
			<div class="large-12 columns tagadditionalchargeclear override-large-2" style="padding-left: 0 !important; padding-right: 0 !important;background: #f2f3fa;" id="additionalchargeaddvalidate2">
			
	<?php } else { ?>
	<div id="subformspan1" class="large-12 columns end itemdetailsopacity borderstyle"  style="opacity:1;left: 14px;position: relative; background: #fff;width: 98%;">
		<form method="POST" name="splittagform" class="validationEngineContainer splittagform"  id="splittagform">
			<div class="large-12 columns tagadditionalchargeclear override-large-2" style="padding-left: 0 !important; padding-right: 0 !important;background: #fff;" id="additionalchargeaddvalidate2">
	
	
	<?php } ?>
				<!--  <div class="toggle-headercontent">-->
				<div class="static-field large-2 columns" id="">
					<input id="splittagdate" class="fordatepicicon" type="text" data-dateformater="dd-mm-yy" data-prompt-position="bottomLeft" readonly="readonly" tabindex="1001" name="splittagdate" data-validation-engine="validate[required]">
					<label for="splittagdate">Date<span class="mandatoryfildclass">*</span></label>
				</div>
				<div class="static-field large-2 columns clearonstocktypechange " id="itemtagnumber-div">
					<input type="text" class="" id="itemtagnumber" name="itemtagnumber" value=""  data-validation-engine="" tabindex="110">
					<input type="hidden" class="" id="itemtagid" name="itemtagid" value=""  data-validation-engine="" tabindex="110">
					<label for="itemtagnumber">Tag Number<span class="mandatoryfildclass" id="itemtagnumber_req"></span></label>
				</div>
				<div class="static-field large-3 columns clearonstocktypechange " id="product-div">
					<label>Product<span class="mandatoryfildclass" id="product_req">*</span></label>	
					<select id="product" name="product" readonly class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="111">
					<option class="groupall ddhidedisplay" data-purchasechargeid="1" data-chargeid ="1" value="1" data-stoneapplicable="" data-counterid="1" data-purityid="1" data-label="" data-bullionapplicable="" data-taxid="" data-taxdataid="" data-categorytaxid ="" data-size="">1 - ALL</option>
					<?php 
					 foreach($product->result() as $key):
					?>
					<option value="<?php echo $key->productid;?>" data-purchasechargeid="<?php echo $key->purchasechargeid;?>" data-chargeid="<?php echo $key->chargeid;?>" data-stoneapplicable="<?php echo $key->stoneapplicable;?>" data-counterid="<?php echo $key->counterid;?>" data-purityid="<?php echo $key->purityid;?>" data-label="<?php echo $key->productname;?>" data-bullionapplicable="<?php echo $key->bullionapplicable;?>" data-taxid="<?php echo $key->taxid;?>" data-taxdataid="<?php echo $key->taxdataid;?>" data-taxmasterid="<?php echo $key->taxmasterid;?>" data-categoryid="<?php echo $key->categoryid;?>" data-calctypeid="<?php echo $key->weightcalculationtypeid;?>" data-categorytaxid ="<?php echo $key->categorytaxid;?>" data-size="<?php echo $key->size;?>"> 
					<?php if($mainlicense['productidshow'] == 'YES') { echo $key->productid.' - '.$key->productname; } else { 
					echo $key->productname; } ?></option>
					<?php endforeach;?>
					</select>
				</div>
				<div class="input-field large-2 columns " id="grossweight-div">
					<input type="text" class="" id="grossweight" data-calc="GWT" name="grossweight" value=""  data-validation-engine="" readonly tabindex="115">
					<label for="grossweight">G.WT<span class="mandatoryfildclass" id="grossweight_req">*</span></label>
				</div>
				<div class="input-field large-1 columns " id="stoneweight-div">
					<input type="text" class=" " data-calc="SWT" data-validategreat="grossweight" id="stoneweight" name="stoneweight" readonly value=""  data-validation-engine="" tabindex="116"/>
					<label for="stoneweight">S.WT<span class="mandatoryfildclass" id="stoneweight_req">*</span></label>
					<input type ="hidden" id="hidstonecaratweight" value="">
					<input type ="hidden" id="hidstonegram" value="">
					<input type ="hidden" id="hidstonepieces" value="">
					<input type ="hidden" id="allentryids" value="">
					<input type ="hidden" id="checktagstone" value="">
				</div>
				<div class="input-field large-2 columns  " id="netweight-div">
					<input type="text" class="" data-calc="NWT" data-validategreat="grossweight" id="netweight" readonly name="netweight" value="" data-validation-engine="" tabindex="117"/>
					<label for="netweight">N.WT<span class="mandatoryfildclass" id="netweight_req">*</span></label>
				</div>
		</div>	
		</form>
	</div>
	<div class="large-12 columns">&nbsp; </div>
	<?php
	if($device=='phone') 
	{
		?>
	<div id="" class="large-12 columns end itemdetailsopacity" style="opacity:1; position: relative; background: #f2f3fa;width: 98%;padding:0px !important;">
		<div class="large-12 columns end paddingzero ">	
		<form method="POST" name="splitdetailaddform" id="splitdetailaddform" class="rulecriteriacondclear splitdetailaddform validationEngineContainer">
					<span class="foramounttype">
						<div class="large-12 columns paddingzero" id="tabindexhideshow">	
							<div class="large-12 columns cleardataform override-large-2" style="background:#f2f3fa !important;padding-left: 0 !important; padding-right: 0 !important;box-shadow: none;">
	<?php } else { ?>
	<div id="" class="large-12 columns end itemdetailsopacity borderstyle" style="opacity:1; left: 14px;position: relative; background: #fff;width: 98%;">
		<div class="large-12 columns end paddingzero ">
		<form method="POST" name="splitdetailaddform" id="splitdetailaddform" class="rulecriteriacondclear splitdetailaddform validationEngineContainer">
					<span class="foramounttype">
						<div class="large-12 columns paddingzero" id="tabindexhideshow">	
							<div class="large-12 columns cleardataform override-large-2" style="padding-left: 0 !important; padding-right: 0 !important;box-shadow: none;">
	
	<?php } ?>
	
			
									<div class="static-field large-2 columns clearonstocktypechange" id="purityid-div">
										<label>Purity<span class="mandatoryfildclass" id="purityid_req">*</span></label>				
										<select id="purityid" name="purityid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="106">
											<option value=""></option>
											<?php $prev = ' ';
											foreach($purity->result() as $key):
											$cur = $key->metalid;
											$a="";
											if($prev != $cur )
											{
												if($prev != " ")
												{
													 echo '</optgroup>';
												}
												echo '<optgroup  label="'.$key->metalname.'">';
												$prev = $key->metalid;
											}
											?>
											<option data-melting="<?php echo $key->melting;?>" data-purityname="<?php echo $key->purityname;?>" data-metalid="<?php echo $key->metalid;?>" value="<?php echo $key->purityid;?>"><?php echo $key->purityname;?></option>
											<?php endforeach;?>
										</select>
									</div>
									<div class="static-field large-2 columns clearonstocktypechange " id="splitproduct-div">
										<label>Product<span class="mandatoryfildclass" id="product_req">*</span></label>	
										<select id="splitproduct"  name="splitproduct" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="111">
										<option class="groupall ddhidedisplay" data-purchasechargeid="1" data-chargeid ="1" value="1" data-stoneapplicable="" data-counterid="1" data-purityid="1" data-label="" data-bullionapplicable="" data-taxid="" data-taxdataid="" data-categorytaxid ="" data-size="">1 - ALL</option>
										<?php 
										 foreach($product->result() as $key):
										?>
										<option value="<?php echo $key->productid;?>" data-purchasechargeid="<?php echo $key->purchasechargeid;?>" data-chargeid="<?php echo $key->chargeid;?>" data-stoneapplicable="<?php echo $key->stoneapplicable;?>" data-counterid="<?php echo $key->counterid;?>" data-purityid="<?php echo $key->purityid;?>" data-label="<?php echo $key->productname;?>" data-bullionapplicable="<?php echo $key->bullionapplicable;?>" data-taxid="<?php echo $key->taxid;?>" data-taxdataid="<?php echo $key->taxdataid;?>" data-taxmasterid="<?php echo $key->taxmasterid;?>" data-categoryid="<?php echo $key->categoryid;?>" data-calctypeid="<?php echo $key->weightcalculationtypeid;?>" data-categorytaxid ="<?php echo $key->categorytaxid;?>" data-size="<?php echo $key->size;?>"> 
										<?php if($mainlicense['productidshow'] == 'YES') { echo $key->productid.' - '.$key->productname; } else { 
										echo $key->productname; } ?></option>
										<?php endforeach;?>
										</select>
									</div>
									<?php if((strtolower($mainlicense['counter']) == 'yes')) { 
										?>
										<div class="static-field large-2 columns clearonstocktypechange" id="splitcounterid-div">
											<label>Counter <span class="mandatoryfildclass" id="splitcounterid_req">*</span></label>			
											<select id="splitcounterid" name="splitcounterid" class="chzn-select " data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="117">
												<option value=""></option>
												<?php $prev = ' ';
												foreach($notdefaultcounter->result() as $key):
												?>
												<option value="<?php echo $key->counterid;?>" data-countertypeid="<?php echo $key->countertypeid;?>" data-countername="<?php echo $key->countername;?>" data-maxquantity="<?php echo $key->maxquantity;?>"> <?php echo $key->counterid.' - '.$key->countername;?></option>
												<?php endforeach;?>
											</select>
										</div>
									<?php } ?>
									<div class="static-field large-2 columns clearonstocktypechange" id="splitsize-div">
										<label>Size</label>
										<select id="splitsize" name="splitsize" class="chzn-select" style="" data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="110">
											<option value="" data-purityid="0"></option>
											<?php $prev = ' ';
											foreach($sizename->result() as $key):
											?>
										<option value="<?php echo $key->sizemasterid;?>" data-productname="<?php echo $key->productname;?>" data-sizemastername="<?php echo $key->sizemastername;?>" data-productid="<?php echo $key->productid;?>"> <?php echo $key->sizemastername; ?></option>
											<?php endforeach;?>
										</select>
									</div>
									<div class="input-field large-1 columns " id="splitgrossweight-div">
										<input type="text" class="weightcalculate" id="splitgrossweight" data-calc="GWT" name="splitgrossweight" value=""  data-validation-engine="validate[required,min[0.001],custom[number],maxSize[15],decval[<?php echo $weight_round; ?>]]"  tabindex="115">
										<label for="splitgrossweight">G.WT<span class="mandatoryfildclass" id="splitgrossweight_req">*</span></label>
									</div>
									<div class="input-field large-1 columns " id="splitstoneweight-div">
										<input type="text" class="weightcalculate " data-calc="SWT" data-validategreat="" id="splitstoneweight" name="splitstoneweight"  value=""  data-validation-engine="" tabindex="116"/>
										<label for="splitstoneweight">S.WT</label>
										<input type="hidden" id="hiddenstonedata" name="hiddenstonedata"/>
										<input type="hidden" id="hiddenstonedetail" value=""/>
										<input type="hidden" id="hiddenstoneentryid" value=""/>
									</div>
									<div class="input-field large-1 columns  " id="splitnetweight-div">
										<input type="text" class="weightcalculate" data-calc="NWT" data-validategreat="" id="splitnetweight" readonly name="secondnetweight" value="" data-validation-engine="" tabindex="117"/>
										<label for="splitnetweight">N.WT<span class="mandatoryfildclass" id="splitnetweight_req">*</span></label>
									</div>
									<div class="input-field large-1 columns clearonstocktypechange" id="splitpieces-div"> 
										<input type="text" class="tagaddclear" id="splitpieces" name="splitpieces" value="1" tabindex="116" data-validation-engine="" readonly>
										<label for="splitpieces">Pieces <span class="mandatoryfildclass" id="pieces_req">*</span></label>
									</div>
									<input type="hidden" name="purityidname" id="purityidname" value="" >
									<input type="hidden" name="splitproductname" id="splitproductname" value="" >
									<input type="hidden" name="splitcountername" id="splitcountername" value="" >
									<input type="hidden" name="splitsizename" id="splitsizename" value="" >
									<?php
	if($device=='phone') 
	{
		?>
									
									<div style="text-align:center;" class="large-12 columns">
									
	<?php } else { ?>
	<div style="text-align:right;" class="large-12 columns">
	<?php } ?>
										<label></label>
										<input id="splittagdetailsubmit" class="btn" type="button" name="splittagdetailsubmit" value="Submit" tabindex="123" />
									</div>
								<div class="large-12 columns "> &nbsp; </div>
							</div>
						</div>
					</span>
			</form>
		</div>
	</div>
	<div class="large-12 columns">&nbsp; </div>
	<div id="splittaginnergriddiv" class="large-12 columns">	
		<div class="large-12 columns viewgridcolorstyle borderstyle" style="padding-left:0;padding-right:0;">
			<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0rem 0rem;line-height:1.8rem;height:auto;">	
				<div class="large-12 columns paddingzero">
					<div class="large-6  medium-6 small-6 columns lefttext">Split List</div>
					<div class="large-6 medium-6 small-6 columns innergridicons righttext" >
					<span class="" title="Delete" id="splitdetaildelete"><i class="material-icons">delete</i></span>
					</div>
				</div>
			</div>
			<div class="large-12 columns paddingzero">
			<?php
				$device = $this->Basefunctions->deviceinfo();
				if($device=='phone') {
					echo '<div class="large-12 columns paddingzero forgetinggridname" id="splittaginnergridwidth"><div class=" inner-row-content inner-gridcontent" id="splittaginnergrid" style="height:420px;top:0px;">
						<!-- Table header content , data rows & pagination for mobile-->
					</div>
					<footer class="inner-gridfooter footercontainer" style="height:0px !important;" id="splittaginnergridfooter">
						<!-- Footer & Pagination content -->
					</footer></div>';
				} else {
					echo '<div class="large-12 columns forgetinggridname" id="splittaginnergridwidth" style="padding-left:0;padding-right:0;">
					<div class="desktop row-content inner-gridcontent" id="splittaginnergrid" style="max-width:2000px; height:240px;top:0px;">
					<!-- Table content[Header & Record Rows] for desktop-->
					</div>
					<div class="footer-content footercontainer"  style="height:0px !important;" id="splittaginnergridfooter">
							<!-- Footer Previous / Next page -->
						</div>
					</div>';
				}
			?>	
			</div>
		</div>
		<div class="large-12 columns ">&nbsp;</div>
	</div>
</div>
