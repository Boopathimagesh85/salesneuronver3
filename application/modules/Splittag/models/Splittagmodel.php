<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Splittagmodel extends CI_Model {
   	private $splittagmoduleid = 140;	
	public function __construct() {
        parent::__construct();
		$this->load->model( 'Base/Basefunctions' );
		$this->load->model('Base/Crudmodel');
    }
	//itemtag based data fetch
	public function retrievetagdata() {
		$tagnumber = $_GET['tagnumber'];
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$dia_weight_round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',8);
		$this->db->select(
						'itemtag.itemtagid,DATE_FORMAT(tagdate,"%d-%m-%Y") AS tagdate,itemtag.productid,itemtag.purityid,itemtag.size,itemtag.counterid,
						ROUND(itemtag.grossweight,'.$round.') as grossweight,
						ROUND(itemtag.stoneweight,'.$round.') as stoneweight,
						ROUND(SUM(stoneentry.caratweight),'.$dia_weight_round.') as stonecaratweight,
						ROUND(SUM(stoneentry.gram),'.$round.') as stonegram,
						SUM(stoneentry.pieces) as stonepieces,
						ROUND(itemtag.netweight,'.$round.') as netweight,itemtag.status,group_concat(stoneentry.stoneentryid) as stoneentryid',false);
		$this->db->from('itemtag');
		$this->db->join('stoneentry','stoneentry.itemtagid=itemtag.itemtagid AND stoneentry.status IN (1)','left');
		if(!empty($_GET['tagnumber'])) {
			$this->db->where('itemtag.itemtagnumber',$tagnumber);
		}
		$this->db->limit(1);
		$info = $this->db->get();
		if($info->num_rows() == 1) {
			$infos = $info->row();			
			if($infos->status == 1) {
				$checktagstone = 0;
				if($infos->stoneentryid != '') {
					$checktagstone = 1;
				}
				$tagjsonarray = array(
					'itemtagid' => $infos->itemtagid,
					'product'=> $infos->productid,
					'purityid'=> $infos->purityid,
					'size'=> $infos->size,
					'counterid'=> $infos->counterid,
					'grossweight' => $infos->grossweight + 0,
					'stoneweight' => $infos->stoneweight + 0,
					'netweight' => $infos->netweight + 0,
					'stonecaratweight' => $infos->stonecaratweight + 0,
					'stonegram' => $infos->stonegram + 0,
					'stonepieces' => $infos->stonepieces,
					'checktagstone' => $checktagstone,
					'allentryids' => $infos->stoneentryid
				);	
			} else if($infos->status == 12) {
				$tagjsonarray = array('output'=>'SOLD');
			} else {
				$tagjsonarray = array('output'=>'NO');
			}
		} else {
			$tagjsonarray = array('output'=>'NO');
		}
		echo json_encode($tagjsonarray);
	}
	//size dropdown data fetch
	public function size_groupdropdown() {
		$this->db->select('sizemaster.sizemasterid,sizemaster.sizemastername,sizemaster.productid,product.productname');
		$this->db->from('sizemaster');
		$this->db->join('product','product.productid=sizemaster.productid');
		$this->db->where('sizemaster.status',1);
		$result = $this->db->get();
		return $result;
	}
	//split tag inner grid header fetch
	public function splittaginnergridheaderfetch($moduleid) {
		$fnames = array('splitagid','purityid','purityidname','splitproduct','splitproductname','splitcounterid','splitcountername','splitsize','splitsizename','splitgrossweight','splitstoneweight','splitnetweight','splitpieces','hiddenstonedata','hiddenstonedetail','hiddenstoneentryid');
		$flabels = array('Splitag Id','Purity','Purity','Product','Product','Counter','Counter','Size','Size','Gross Wt','Stone Wt','Net Wt','Pieces','Hide Stone Data','Hide Stone Details','Hidden Stone ID');
		$colmodnames = array('splitagid','purityid','purityidname','splitproduct','splitproductname','splitcounterid','splitcountername','splitsize','splitsizename','splitgrossweight','splitstoneweight','splitnetweight','splitpieces','hiddenstonedata','hiddenstonedetail','hiddenstoneentryid');
		$colindexnames = array('splitag','splitag','splitag','splitag','splitag','splitag','splitag','splitag','splitag','splitag','splitag','splitag','splitag','splitag','splitag','splitag');
		$uitypes = array('2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2');
		$viewtypes = array('0','0','1','0','1','0','1','0','1','1','1','1','1','0','0','0');
		$dduitypeids = array('17','18','19','20','23','25','26','27','28','29');
		$data = array();
		$i=0;
		$j=0;
		foreach($fnames as $fname) {
			if(in_array($uitypes[$j],$dduitypeids)) {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j].'name';
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]='0';
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]='2';
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			} else {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			}
			$j++;
		}
		return $data;
	}
	//split tag insert
	public function splittagcreatemodel() {
		$loclingtime = $this->Basefunctions->get_company_settings('lockingtime');
		$updatingtime = $this->Basefunctions->get_company_settings('updatingtime');
		$currenttime = time("H:i:s");
		$ctime = new DateTime();
		$ctime->setTimestamp($currenttime);
		$ltime  = new DateTime();
		$splittagdate = $_POST['splittagdate'];
		if($loclingtime == '00:00:00') {
			$ltime->setTimestamp($currenttime);
		} else {
			$ltime->setTimestamp(strtotime($loclingtime));
		}
		if($ctime > $ltime) {
			$tagdate = date("Y-m-d",strtotime("tomorrow"));
			$tagdate = $this->Basefunctions->checkgivendateisholidayornot($tagdate);
		} else {
			$tagdate = $this->Basefunctions->ymd_format($splittagdate);
		}
		$itemtagid = $_POST['itemtagid'];
		$olditemtagnumber = $_POST['itemtagnumber'];
		$girddata = $_POST['splittaggriddata'];
		$cricount = $_POST['count'];
		$girddatainfo = json_decode($girddata,true);
		$userid = $this->Basefunctions->userid;
		$cdate = date($this->Basefunctions->datef);
		for($i=0;$i<=($cricount-1);$i++) {
			//item tag create
			$serialnumber = $this->Basefunctions->varrandomnumbergenerator(50,'itemtagnumber',2,''); //serialnumber
			$tagnumber = $this->Basefunctions->singlefieldfetch('itemtagnumber','itemtagnumber','itemtag',$serialnumber);
			if($tagnumber == 1){
				$tagno = $serialnumber;
			} else {
				$tagno = $this->Basefunctions->varrandomnumbergenerator(50,'itemtagnumber',2,''); //serialnumber
			}
			$datesett = $this->Basefunctions->get_company_settings('partialtagdate');
			if($datesett == '1') {
				$stockdate = $this->Basefunctions->generalinformaion('itemtag','tagdate','itemtagid',$itemtagid);
			} else {
				$stockdate = $tagdate;
			}
			$insert = array(
				'itemtagnumber' => $tagno,
				'tagdate' => $stockdate,
				'tagtypeid' =>$this->Basefunctions->generalinformaion('itemtag','tagtypeid','itemtagid',$itemtagid),
				'lotid' =>$this->Basefunctions->generalinformaion('itemtag','lotid','itemtagid',$itemtagid),	
				'purityid' => $girddatainfo[$i]['purityid'] ,						
				'accountid' =>$this->Basefunctions->generalinformaion('itemtag','accountid','itemtagid',$itemtagid),
				'productid' =>$girddatainfo[$i]['splitproduct'],
				'grossweight' => $girddatainfo[$i]['splitgrossweight'],		
				'stoneweight' => $girddatainfo[$i]['splitstoneweight'],		
				'netweight' => $girddatainfo[$i]['splitnetweight'],				
				'pieces' => $girddatainfo[$i]['splitpieces'],					
				'size' => $girddatainfo[$i]['splitsize'],
				'counterid' =>$girddatainfo[$i]['splitcounterid'] ,	
				'stockincounterid' =>$this->Basefunctions->generalinformaion('itemtag','stockincounterid','itemtagid',$itemtagid),									
				'fromcounterid' => $this->Basefunctions->generalinformaion('itemtag','fromcounterid','itemtagid',$itemtagid),
				'description' => $this->Basefunctions->generalinformaion('itemtag','description','itemtagid',$itemtagid),
				'tagimage' => $this->Basefunctions->generalinformaion('itemtag','tagimage','itemtagid',$itemtagid),	
				'tagtemplateid' => $this->Basefunctions->generalinformaion('itemtag','tagtemplateid','itemtagid',$itemtagid),
				'status'=>$this->Basefunctions->activestatus,
				'melting' => $this->Basefunctions->generalinformaion('itemtag','melting','itemtagid',$itemtagid),
				'touch' => $this->Basefunctions->generalinformaion('itemtag','touch','itemtagid',$itemtagid),
				'rate' => $this->Basefunctions->generalinformaion('itemtag','rate','itemtagid',$itemtagid),
				'transfertagno'=>$this->Basefunctions->generalinformaion('itemtag','transfertagno','itemtagid',$itemtagid),
				'tagentrytypeid' =>$this->Basefunctions->generalinformaion('itemtag','tagentrytypeid','itemtagid',$itemtagid),
	            'rfidtagno'=>'',
				'itemrate'=>$this->Basefunctions->generalinformaion('itemtag','itemrate','itemtagid',$itemtagid),
				'itemratewithgst'=>$this->Basefunctions->generalinformaion('itemtag','itemratewithgst','itemtagid',$itemtagid),
				'industryid'=>$this->Basefunctions->industryid,
				'branchid'=>$this->Basefunctions->branchid,
				'hallmarkno'=>$this->Basefunctions->generalinformaion('itemtag','hallmarkno','itemtagid',$itemtagid),
				'hallmarkcenter'=>$this->Basefunctions->generalinformaion('itemtag','hallmarkcenter','itemtagid',$itemtagid),
				'guaranteecard'=>$this->Basefunctions->generalinformaion('itemtag','guaranteecard','itemtagid',$itemtagid),
				'scaleoption'=>$this->Basefunctions->generalinformaion('itemtag','scaleoption','itemtagid',$itemtagid),
				'chargedetails'=>$this->Basefunctions->generalinformaion('itemtag','chargedetails','itemtagid',$itemtagid),
				'chargeid'=>$this->Basefunctions->generalinformaion('itemtag','chargeid','itemtagid',$itemtagid),
				'chargecategoryid'=>$this->Basefunctions->generalinformaion('itemtag','chargecategoryid','itemtagid',$itemtagid),
				'chargeamount'=>$this->Basefunctions->generalinformaion('itemtag','chargeamount','itemtagid',$itemtagid),
				'chargespandetails'=>$this->Basefunctions->generalinformaion('itemtag','chargespandetails','itemtagid',$itemtagid),
				'chargespanamount'=>$this->Basefunctions->generalinformaion('itemtag','chargespanamount','itemtagid',$itemtagid),
				'processproductid'=>$this->Basefunctions->generalinformaion('itemtag','processproductid','itemtagid',$itemtagid),
				'splititemtagid'=>$itemtagid,
				'splititemtagnumber'=>$olditemtagnumber,
				'productaddoninfoid'=>$this->Basefunctions->generalinformaion('itemtag','productaddoninfoid','itemtagid',$itemtagid),
				'certificationno'=>$this->Basefunctions->generalinformaion('itemtag','certificationno','itemtagid',$itemtagid)
			);
			if($ctime > $ltime) {
				$insert = array_merge($insert,$this->Crudmodel->nextdaydefaultvalueget());
			} else {
				$insert = array_merge($insert,$this->Crudmodel->defaultvalueget());
			}
			$this->db->insert('itemtag',array_filter($insert));
			$primaryid = $this->db->insert_id();
			// Stone Entry => Insert
			$stoneentry = json_decode($girddatainfo[$i]['hiddenstonedata']);
			if(!empty($stoneentry)) {
				usort($stoneentry,function($a,$b) {
					return $a->stonegroupid - $b->stonegroupid;
				});
				$stoneorder = 0;	
				foreach($stoneentry as $row) {
					$stoneinsert = array(
						'stoneentryorder' => $stoneorder,
						'stonetypeid' => $row->stonegroupid,
						'itemtagid' => $primaryid,
						'stoneid' => $row->stoneid,
						'purchaserate' =>$row->purchaserate,
						'stonerate' =>$row->basicrate,
						'pieces' => $row->stonepieces,	
						'caratweight' => $row->caratweight,
						'gram' => $row->stonegram,
						'totalcaratweight' => $row->totalcaratweight,						
						'totalnetweight' =>$row->totalweight,
						'totalamount' => $row->stonetotalamount,
						'stoneentrycalctypeid'=>$row->stoneentrycalctypeid,
						'status'=>$this->Basefunctions->activestatus
					);
					if($ctime > $ltime) {
						$stoneinsert = array_merge($stoneinsert,$this->Crudmodel->nextdaydefaultvalueget());
					} else {
						$stoneinsert = array_merge($stoneinsert,$this->Crudmodel->defaultvalueget());
					}
					$this->db->insert('stoneentry',$stoneinsert);
					$stoneorder++;		
				}
				// Update Caratweight and Pieces separately for Diamond and Other Stones by Kumaresan
				$diamondcaratweight = 0.00;
				$diamondpieces = 0;
				$stonecaratweight = 0.00;
				$stone_pieces = 0;
				foreach($stoneentry as $row) {
					if($row->stonegroupid == 2) {
						$diamondcaratweight = number_format((float)$row->caratweight + $diamondcaratweight, 2, '.', '');
						$diamondpieces = number_format((int)$row->stonepieces + $diamondpieces, 0, '.', '');
					} else if($row->stonegroupid != 2) {
						$stonecaratweight = number_format((float)$row->caratweight + $stonecaratweight, 2, '.', '');
						$stone_pieces = number_format((int)$row->stonepieces + $stone_pieces, 0, '.', '');
					}
				}
				$itemtagdiamonddetailsupdate = array(
					'diacaratweight' => $diamondcaratweight,
					'diapieces' => $diamondpieces,
					'stonecaratweight' => $stonecaratweight,
					'stonepieces' => $stone_pieces
				);
				$itemtagdiamonddetailsupdate = array_merge($itemtagdiamonddetailsupdate,$this->Crudmodel->updatedefaultvalueget());
				$this->db->where('itemtagid',$primaryid);
				$this->db->update('itemtag',$itemtagdiamonddetailsupdate);
			}
			$conddatas = array(
				'splititemtagid'=>$itemtagid,
				'splititemtagnumber'=>$olditemtagnumber,
				'itemtagid'=>$primaryid,
				'itemtagnumber'=>$tagno,
				'splittagdate'=>$tagdate,
				'productid'=>$girddatainfo[$i]['splitproduct'],
				'purityid'=>$girddatainfo[$i]['purityid'],
				'counterid'=>$girddatainfo[$i]['splitcounterid'],
				'sizemasterid'=>$girddatainfo[$i]['splitsize'],
				'grossweight'=>$girddatainfo[$i]['splitgrossweight'],
				'stoneweight'=>$girddatainfo[$i]['splitstoneweight'],
				'netweight'=>$girddatainfo[$i]['splitnetweight'],
				'pieces'=>$girddatainfo[$i]['splitpieces'],
				'industryid'=>$this->Basefunctions->industryid,
				'branchid'=>$this->Basefunctions->branchid
			);
			if($ctime > $ltime) {
				$conddatas = array_merge($conddatas,$this->Crudmodel->nextdaydefaultvalueget());
			} else {
				$conddatas = array_merge($conddatas,$this->Crudmodel->defaultvalueget());
			}
			$this->db->insert('splittag',$conddatas);
			$splittagid = $this->db->insert_id();
			//audit-log
			$userid = $this->Basefunctions->logemployeeid;
			$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
			$activity = ''.$user.' created Itemtag via Splittag of Tag '.$serialnumber.'';
			$this->Basefunctions->notificationcontentadd($splittagid,'Added',$activity ,$userid,50);
			//update split tag id in item tag
			$update = array('splittagid'=>$splittagid);
			$this->db->where('itemtagid',$primaryid);
			$this->db->update('itemtag',$update);
			//stock table update
			$this->Basefunctions->stocktableentryfunction($primaryid,'50');
		}
		//disable state of old item tag
		$updateitemtag = array(
			'lastupdateuserid'=>$userid,
			'lastupdatedate'=>$cdate,
			'status'=>0			
		);
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$deltagnumber = $this->Basefunctions->singlefieldfetch('itemtagnumber','itemtagid','itemtag',$itemtagid);
		$activity = ''.$user.' deleted Itemtag via Splittag of Tag '.$deltagnumber.'';
		$this->Basefunctions->notificationcontentadd($splittagid,'Added',$activity ,$userid,50);
		$this->db->where('itemtagid',$itemtagid);
		$this->db->update('itemtag',$updateitemtag);
		// Update Stone Entry
		$this->db->where('itemtagid',$itemtagid);
		$this->db->update('stoneentry',$updateitemtag);
		echo 'SUCCESS';
	}
	// Retrieve Live Stone details
	public function autostonegriddynamicdatainfofetch($tablename,$sortcol,$sortord,$pagenum,$rowscount,$primaryid,$gridstoneids) {
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$rateround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //Rate round-off
		$dia_weight_round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',8);
		$taxround = 2;
		$newlist_stonegrp = array();
		$arr_stoneids = array();
		$checktagstone = $_GET['checktagstone'];
		if($checktagstone == 1) {
			$stoneentry = json_decode($_GET['stonelist_arr'], true);
			$stoneid_list = $_GET['stoneid_list'];
			if(!empty($stoneentry)) {
				foreach($stoneentry as $row) {
					$stoneid = $row['stoneid'];
					if(!in_array($row['stoneid'],$arr_stoneids)) {
						$arr_stoneids[] = $stoneid;
						$newlist_stonegrp[$stoneid] = array(
							'stoneid' => $row['stoneid'],
							'pieces' => $row['stonepieces'],	
							'caratweight' => $row['caratweight'],
							'gram' => $row['stonegram'],
							'totalcaratweight' => $row['totalcaratweight'],
							'totalnetweight' =>$row['totalweight'],
							'totalamount' => $row['stonetotalamount']
						);
					} else if(in_array($stoneid,$arr_stoneids)) {
						foreach($newlist_stonegrp as $value) {
							if($stoneid == $value['stoneid']) {
								$newlist_stonegrp[$stoneid]['pieces'] +=  $row['stonepieces'];
								$newlist_stonegrp[$stoneid]['caratweight'] +=  $row['caratweight'];
								$newlist_stonegrp[$stoneid]['gram'] +=  $row['stonegram'];
								$newlist_stonegrp[$stoneid]['totalcaratweight'] +=  $row['totalcaratweight'];
								$newlist_stonegrp[$stoneid]['totalnetweight'] +=  $row['totalweight'];
								$newlist_stonegrp[$stoneid]['totalamount'] +=  $row['stonetotalamount'];
							}
						}
					}
				}
			}
		}
		if(!empty($gridstoneids)) {
			$stoneidcond = "stoneid NOT IN (".$gridstoneids.")";
		} else {
			$stoneidcond = "stoneid NOT IN (1)";
		}
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		/* query */
		$result = $this->db->query('SELECT SQL_CALC_FOUND_ROWS stoneentry.stoneentryid, stoneentry.stoneid, stoneentry.stonetypeid, ROUND(COALESCE(stoneentry.purchaserate,0), '.$amountround.') as purchaserate, ROUND(COALESCE(stoneentry.stonerate,0), '.$amountround.') as stonerate, ROUND(COALESCE(stoneentry.pieces,0),0) as pieces, ROUND(COALESCE(stoneentry.caratweight,0), '.$dia_weight_round.') as caratweight, ROUND(COALESCE(stoneentry.gram,0), '.$dia_weight_round.') as gram, ROUND(COALESCE(stoneentry.totalcaratweight,0), '.$dia_weight_round.') as totalcaratweight, ROUND(COALESCE(stoneentry.totalamount,0), '.$amountround.') as totalamount, ROUND(COALESCE(stoneentry.totalnetweight,0), '.$round.') as totalnetweight, stoneentry.stoneentrycalctypeid, stone.stonename, stonetype.stonetypename, stoneentrycalctype.stoneentrycalctypename FROM itemtag LEFT OUTER JOIN stoneentry on stoneentry.itemtagid = itemtag.itemtagid LEFT OUTER JOIN `stone` ON `stone`.`stoneid`=`stoneentry`.`stoneid` AND stone.status = 1 LEFT OUTER JOIN `stonetype` ON `stonetype`.`stonetypeid`=`stoneentry`.`stonetypeid` AND stonetype.status = 1 LEFT OUTER JOIN `stoneentrycalctype` ON `stoneentrycalctype`.`stoneentrycalctypeid`=`stoneentry`.`stoneentrycalctypeid` AND stoneentrycalctype.status = 1 WHERE `itemtag`.`itemtagid` = '.$primaryid.' AND stoneentry.'.$stoneidcond.' AND stoneentry.status = 1 GROUP BY stoneentry.stoneentryid ORDER BY '.' stoneid '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$data=array();
		$i=0;
		foreach($result->result() as $row) {
			$insideforeach = 0;
			foreach($newlist_stonegrp as $value) {
				if($row->stoneid == $value['stoneid']) {
					$stonepieces = ROUND($row->pieces - $value['pieces'],0);
					$stonegram = ROUND($row->gram - $value['gram'],$round);
					$totalcaratweight = ROUND($row->totalcaratweight - $value['totalcaratweight'],$dia_weight_round);
					$caratweight = ROUND($row->caratweight - $value['caratweight'],$dia_weight_round);
					$totalweight = ROUND($row->totalnetweight - $value['totalnetweight'],$round);
					$stonetotalamount = ROUND($row->totalamount - $value['totalamount'],$round);
					$insideforeach = 1;
				}
			}
			if($insideforeach == 0) {
				$stonepieces = $row->pieces;
				$stonegram = $row->gram;
				$totalcaratweight = $row->totalcaratweight;
				$caratweight = $row->caratweight;
				$totalweight = $row->totalnetweight;
				$stonetotalamount = $row->totalamount;
			}
			$stoneentryid = $row->stoneentryid;
			$stoneid = $row->stoneid;
			$stonename =$row->stonename;
			$stonetypeid = $row->stonetypeid;
			$stonetypename =$row->stonetypename;
			$stoneentrycalctypeid =$row->stoneentrycalctypeid;
			$stoneentrycalctypename =$row->stoneentrycalctypename;
			$purchaserate =$row->purchaserate;
			$basicrate =$row->stonerate;
			if($stonepieces > 0 || $stonegram > 0 || $caratweight > 0) {
				$data[$i] = array('id'=>$stoneentryid,$stoneid,$stonename,$stonetypeid,$stonetypename,$stoneentrycalctypeid,$stoneentrycalctypename,$purchaserate,$basicrate,$stonepieces,$stonegram,$caratweight,$totalcaratweight,$totalweight,$stonetotalamount);
				$i++;
			}
		}
		$finalresult = array('0'=>$data,'1'=>$page,'2'=>'json');
		return $finalresult;
	}
	//stone json convert data
	public function stonejsonconvertdata() {
		$getgriddata = json_decode($_POST['datas']);
		$stonedetail='';
		$j=1;
		foreach($getgriddata as $value) {
			$stonedetail->rows[$j]['id'] = $j;
			$stonedetail->rows[$j]['cell']=array(
					$value->stonegroupid,
					$value->stonegroupname,
					$value->stoneid,
					$value->stonename,
					$value->purchaserate,
					$value->basicrate,
					$value->stoneentrycalctypeid,
					$value->stoneentrycalctypename,
					$value->stonepieces,
					$value->caratweight,
					$value->stonegram,
					$value->totalcaratweight,
					$value->totalweight,
					$value->stonetotalamount
				);
			$j++;
		}
		echo  json_encode($stonedetail);	
	}
	// Stone Data Complete check
	public function checkallstonedatacomplete() {
		$primaryid = $_GET['primaryid'];
		$gridstoneids = $_GET['gridstoneids'];
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$rateround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //Rate round-off
		$dia_weight_round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',8);
		$taxround = 2;
		$newlist_stonegrp = array();
		$arr_stoneids = array();
		$checktagstone = $_GET['checktagstone'];
		if($checktagstone == 1) {
			$stoneentry = json_decode($_GET['stonelist_arr'], true);
			$stoneid_list = $_GET['stoneid_list'];
			if(!empty($stoneentry)) {
				foreach($stoneentry as $row) {
					$stoneid = $row['stoneid'];
					if(!in_array($row['stoneid'],$arr_stoneids)) {
						$arr_stoneids[] = $stoneid;
						$newlist_stonegrp[$stoneid] = array(
							'stoneid' => $row['stoneid'],
							'pieces' => $row['stonepieces'],	
							'caratweight' => $row['caratweight'],
							'gram' => $row['stonegram'],
							'totalcaratweight' => $row['totalcaratweight'],
							'totalnetweight' =>$row['totalweight'],
							'totalamount' => $row['stonetotalamount']
						);
					} else if(in_array($stoneid,$arr_stoneids)) {
						foreach($newlist_stonegrp as $value) {
							if($stoneid == $value['stoneid']) {
								$newlist_stonegrp[$stoneid]['pieces'] +=  $row['stonepieces'];
								$newlist_stonegrp[$stoneid]['caratweight'] +=  $row['caratweight'];
								$newlist_stonegrp[$stoneid]['gram'] +=  $row['stonegram'];
								$newlist_stonegrp[$stoneid]['totalcaratweight'] +=  $row['totalcaratweight'];
								$newlist_stonegrp[$stoneid]['totalnetweight'] +=  $row['totalweight'];
								$newlist_stonegrp[$stoneid]['totalamount'] +=  $row['stonetotalamount'];
							}
						}
					}
				}
			}
		}
		if(!empty($gridstoneids)) {
			$stoneidcond = "stoneid NOT IN (".$gridstoneids.")";
		} else {
			$stoneidcond = "stoneid NOT IN (1)";
		}
		/* query */
		$result = $this->db->query('SELECT SQL_CALC_FOUND_ROWS stoneentry.stoneentryid, stoneentry.stoneid, stoneentry.stonetypeid, ROUND(COALESCE(stoneentry.purchaserate,0), '.$amountround.') as purchaserate, ROUND(COALESCE(stoneentry.stonerate,0), '.$amountround.') as stonerate, ROUND(COALESCE(stoneentry.pieces,0),0) as pieces, ROUND(COALESCE(stoneentry.caratweight,0), '.$dia_weight_round.') as caratweight, ROUND(COALESCE(stoneentry.gram,0), '.$dia_weight_round.') as gram, ROUND(COALESCE(stoneentry.totalcaratweight,0), '.$dia_weight_round.') as totalcaratweight, ROUND(COALESCE(stoneentry.totalamount,0), '.$amountround.') as totalamount, ROUND(COALESCE(stoneentry.totalnetweight,0), '.$round.') as totalnetweight, stoneentry.stoneentrycalctypeid, stone.stonename, stonetype.stonetypename, stoneentrycalctype.stoneentrycalctypename FROM itemtag LEFT OUTER JOIN stoneentry on stoneentry.itemtagid = itemtag.itemtagid LEFT OUTER JOIN `stone` ON `stone`.`stoneid`=`stoneentry`.`stoneid` AND stone.status = 1 LEFT OUTER JOIN `stonetype` ON `stonetype`.`stonetypeid`=`stoneentry`.`stonetypeid` AND stonetype.status = 1 LEFT OUTER JOIN `stoneentrycalctype` ON `stoneentrycalctype`.`stoneentrycalctypeid`=`stoneentry`.`stoneentrycalctypeid` AND stoneentrycalctype.status = 1 WHERE `itemtag`.`itemtagid` = '.$primaryid.' AND stoneentry.'.$stoneidcond.' AND stoneentry.status = 1 GROUP BY stoneentry.stoneentryid ORDER BY '.' stoneid ASC ');
		$i = 0;
		foreach($result->result() as $row) {
			$insideforeach = 0;
			foreach($newlist_stonegrp as $value) {
				if($row->stoneid == $value['stoneid']) {
					$stonepieces = ROUND($row->pieces - $value['pieces'],0);
					$stonegram = ROUND($row->gram - $value['gram'],$round);
					$totalcaratweight = ROUND($row->totalcaratweight - $value['totalcaratweight'],$dia_weight_round);
					$caratweight = ROUND($row->caratweight - $value['caratweight'],$dia_weight_round);
					$totalweight = ROUND($row->totalnetweight - $value['totalnetweight'],$round);
					$stonetotalamount = ROUND($row->totalamount - $value['totalamount'],$round);
					$insideforeach = 1;
				}
			}
			if($insideforeach == 0) {
				$stonepieces = $row->pieces;
				$stonegram = $row->gram;
				$totalcaratweight = $row->totalcaratweight;
				$caratweight = $row->caratweight;
				$totalweight = $row->totalnetweight;
				$stonetotalamount = $row->totalamount;
			}
			$stoneentryid = $row->stoneentryid;
			$stoneid = $row->stoneid;
			$stonename =$row->stonename;
			$stonetypeid = $row->stonetypeid;
			$stonetypename =$row->stonetypename;
			$stoneentrycalctypeid =$row->stoneentrycalctypeid;
			$stoneentrycalctypename =$row->stoneentrycalctypename;
			$purchaserate =$row->purchaserate;
			$basicrate =$row->stonerate;
			if($stonepieces > 0 || $stonegram > 0 || $caratweight > 0) {
				$i++;
			}
		}
		if($i == 0) {
			$finalresult = 'SUCCESS';
		} else {
			$finalresult = 'FAIL';
		}
		echo $finalresult;
	}
}