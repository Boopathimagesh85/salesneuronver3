<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Metalmodel extends CI_Model{
    public function __construct(){
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//itemcounter create
	public function newdatacreatemodel(){
		//table and fields information
		$formfieldsname = explode(',',$_POST['metalelementsname']);
		$formfieldstable = explode(',',$_POST['metalelementstable']);
		$formfieldscolmname = explode(',',$_POST['metalelementscolmn']);
		$elementpartable = explode(',',$_POST['metalelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
		$primaryid = $this->db->insert_id();
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' created Metal - '.$_POST['metalname'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,53);
		echo 'TRUE';
	}
	//Retrive itemcounter data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['metalelementsname']);
		$formfieldstable = explode(',',$_GET['metalelementstable']);
		$formfieldscolmname = explode(',',$_GET['metalelementscolmn']);
		$elementpartable = explode(',',$_GET['metalelementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['metalprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$metalname = $this->Basefunctions->singlefieldfetch('metalname','metalid','metal',$primaryid);
		$activity = ''.$user.' updated Metal - '.$metalname.'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$activity ,$userid,53);
		echo $result;
	}
	//itemcounter update
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['metalelementsname']);
		$formfieldstable = explode(',',$_POST['metalelementstable']);
		$formfieldscolmname = explode(',',$_POST['metalelementscolmn']);
		$elementpartable = explode(',',$_POST['metalelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['metalprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$metalname = $this->Basefunctions->singlefieldfetch('metalname','metalid','metal',$primaryid);
		$activity = ''.$user.' updated Metal - '.$metalname.'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$activity ,$userid,53);
		echo $result;
	}
	//itemcounter delete
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['metalelementstable']);
		$parenttable = explode(',',$_GET['metalparenttable']);
		$id = $_GET['metalprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$metalname = $this->Basefunctions->singlefieldfetch('metalname','metalid','metal',$id);
		$activity = ''.$user.' deleted Metal - '.$metalname.'';
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,53);
				echo "TRUE";
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,53);
			echo "TRUE";
		}
	} 
}