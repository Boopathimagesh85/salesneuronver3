<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Metal extends MX_Controller{
    public function __construct(){
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Metal/Metalmodel');	
    }
    //first basic hitting view
    public function index() {
    	$moduleid = array(53);
    	sessionchecker($moduleid);
		//action
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Metal/metalview',$data);
	}
	//create metal
	public function newdatacreate() {  
    	$this->Metalmodel->newdatacreatemodel();
    }
	//information fetchitemcounter
	public function fetchformdataeditdetails() {
		$moduleid = 53;
		$this->Metalmodel->informationfetchmodel($moduleid);
	}
	//update metal
    public function datainformationupdate() {
        $this->Metalmodel->datainformationupdatemodel();
    }
	//delete metal
    public function deleteinformationdata() {
        $moduleid = 53;
		$this->Metalmodel->deleteoldinformation($moduleid);
    }
    public function loadmetalbasedpurity()
    {
    	$metalid=$_POST['primaryid'];
    	$metalarr = array();
    	$metal = $this->Basefunctions->simpledropdownwithcondmultiple('purityid','purityname','purity',$metalid,'metalid');
    	foreach($metal as $info){
    		$metalarr[]=array(
    				'purityid' => $info->purityid,
    				'purityname' => $info->purityname
    		);
    	}
    	echo json_encode($metalarr);
    }
    public function setmetalbasedpurity()
    {
    	$metalid=$_POST['primaryid'];
    	$metal =$this->Basefunctions->singlefieldfetch('purity_id','metalid','metal',$metalid);
    	echo json_encode($metal);
    }
    public function updatemetalbasedpurity()
    {
    	$purityid=$_POST['primaryid'];
    	$metalid=$_POST['metalid'];
    	$update=array(
    			'purity_id'=>$purityid
    	);
    	$update=array_merge($update,$this->Crudmodel->updatedefaultvalueget());
    	$this->db->where('metalid',$metalid);
    	$this->db->update('metal',$update);
    	$update_purity=array(
    			'purityrateid'=>$purityid
    	);
    	$update_purity=array_merge($update_purity,$this->Crudmodel->updatedefaultvalueget());
    	$this->db->where('metalid',$metalid);
    	$this->db->update('purity',$update_purity);
    	echo 'SUCCESS';
    }
}