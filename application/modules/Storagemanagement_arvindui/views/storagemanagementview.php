<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('Base/headerfiles'); ?>
	<?php $this->load->view('Base/overlay'); ?>
	<?php  $this->load->view('Base/basedeleteform');
	$this->load->view('Base/modulelist'); ?>
</head>
<body>
	<div class="row" style="max-width:100%">
		<div class="off-canvas-wrap" data-offcanvas>
			<div class="inner-wrap">
				<!-- Add Form -->
				<?php $this->load->view('storagemanagementform'); ?>
			</div>
		</div>
	</div>
</body>
<div class="large-12 columns" style="position:absolute;"><div class="overlay" id="viewdeleteoverlayconform" style="z-index:120;"></div></div>
<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
<?php $this->load->view('Base/bottomscript'); ?>
<?php include 'js/Storagemanagement/storagemanagement.php' ?>
</html>
