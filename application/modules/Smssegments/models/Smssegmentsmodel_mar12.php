<?php
Class Smssegmentsmodel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->model('Base/Crudmodel');
    }
	//folder value fetch
	public function segmentddloadmodel() {
		$i=0;
		$industryid = $this->Basefunctions->industryid;
		$listid = $_GET['listid'];
		$this->db->select('segmentsid,segmentsname');
		$this->db->from('segments');
		$this->db->where('segments.campaigngroupsid',$listid);
		$this->db->where("FIND_IN_SET('$industryid',segments.industryid) >", 0);
		$this->db->where('segments.status','1');
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->segmentsid,'dataname'=>$row->segmentsname);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//cond field name load model
	public function viewdropdownloadmodel() {
		$i=0;
		$ids = $_GET['ids'];
		$this->db->select('viewcreationcolumnid,viewcreationcolmodeljointable,viewcreationcolmodelindexname,viewcreationcolumnname,uitypeid,moduletabsectionid,viewcreationparenttable');
		$this->db->from('viewcreationcolumns');
		$this->db->where('viewcreationcolumns.viewcreationmoduleid',$ids);
		$this->db->where('status',1);
		$result= $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('jointable'=>$row->viewcreationcolmodeljointable,'indexname'=>$row->viewcreationcolmodelindexname,'datasid'=>$row->viewcreationcolumnid,'dataname'=>$row->viewcreationcolumnname,'uitype'=>$row->uitypeid,'modtabsec'=>$row->moduletabsectionid,'parenttable'=>$row->viewcreationparenttable);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//check box value get
	public function checkboxvaluegetmodel() {
		$checkdata = 'Yes,No';
		$checkid = '1,2';
		$cdata = explode(',',$checkdata);
		$cid = explode(',',$checkid);
		for($i=0;$i<count($cdata);$i++){
			$data[$i] = array('datasid'=>$cid[$i],'dataname'=>$cdata[$i]);
		}
		echo json_encode($data);
	}
	//default view fetch based on module
	public function defaultviewfetchmodel($mid){
		$data = "";
		$this->db->select('viewcreationid');
		$this->db->from('viewcreation');
		$this->db->where('viewcreation.viewcreationmoduleid',$mid);
		$this->db->where('viewcreation.createuserid',1);
		$this->db->where('viewcreation.lastupdateuserid',1);
		$this->db->where('viewcreation.status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row){
				$data = $row->viewcreationid;
			}
		}
		return $data;
	}
	//parent table name fetch
	public function parenttablegetmodel(){
		$mid = $_GET['moduleid'];
		$this->db->select('parenttable');
		$this->db->from('modulefield');
		$this->db->where('modulefield.moduletabid',$mid);
		$result = $this->db->get();
		if($result->num_rows() >0){
			foreach($result->result() as $row){
				$data = $row->parenttable;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//From application - subscriber list view
	public function viewdynamicdatainfofetch($tablename,$sortcol,$sortord,$pagenum,$rowscount,$colinfo,$viewcolmoduleids,$creationid,$rowid,$listid) {
		$count = count($colinfo['colmodelindex']);
		$industryid = $this->Basefunctions->industryid;
		$dataset ='';
		$x = 0;
		$joinq = '';
		$extracolinfo = array();
		$multiarray = array();
		$j=0;
		$ccount = $_GET['cricount'];
		$griddata = $_GET['griddata'];
		$formdata=json_decode( $griddata, true );
		for ( $i=( $ccount-1 ); $i >=0 ; $i-- ) {
			$extracolinfo[$j] = $formdata[$i]['fieldid'];
			$multiarray[$j]=array(
					'0'=>$formdata[$i]['jointable'],
					'1'=>$formdata[$i]['indexname'],
					'2'=>$formdata[$i]['condition'],
					'3'=>$formdata[$i]['critreiavalue'],
					'4'=>$formdata[$i]['massandorcond'],
			);
			$j++;
		}
		//grid column title information fetch
		if($extracolinfo == "") {
			$colinfo = $this->Basefunctions->gridinformationfetchmodel($creationid);
		} else {
			$colinfo = $this->extragridinformationfetchmodel($creationid,$extracolinfo);
		}
		$ptab = array($tablename);
		$jtab = array($tablename);
		$maintable = $tablename;
		$selvalue = $tablename.'.'.$tablename.'id';
		for($k=0;$k<$count;$k++) {
			if($k==0) {
				$dataset = $colinfo['colmodelindex'][$k];
			} else{
				$dataset .= ','.$colinfo['colmodelindex'][$k];
			}
			if($x != 0) { $con = "AND"; }
			//parent join check
			$ptabjoin = "";
			$ptabjoin = $colinfo['colmodelparenttable'][$k];
			if(in_array($ptabjoin,$ptab)) {
				if(!in_array($colinfo['coltablename'][$k],$jtab) && $colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
					array_push($jtab,trim($colinfo['coltablename'][$k]));
					//employee
					$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid LIKE "%1%"';
					//group
					$joinq .= ' LEFT OUTER JOIN employeegroup ON FIND_IN_SET(employeegroup.employeegroupid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid  LIKE "%2%"';
					//user role and sub ordinate
					$joinq .= ' LEFT OUTER JOIN userrole ON FIND_IN_SET(userrole.userroleid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND ('.$maintable.'.employeetypeid LIKE "%3%" OR '.$maintable.'.employeetypeid LIKE "%4%")';
				} else {
					if($colinfo['colmodeldatatype'][$k] == 1) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						}
					} else if($colinfo['colmodeldatatype'][$k] == 2) {
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else if($colinfo['colmodeldatatype'][$k] == 3) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						}
					}
				}
			} else {
				if($colinfo['colmodeldatatype'][$k] == 1) {
					$result = $this->Basefunctions->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.','.$maintable.'.'.$primaryid.') > 0 AND '.$ptabjoin.'.status=1';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 1) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
									}
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 2) {
					$joinresult = $this->parentjointableinfo($ptabjoin,$viewcolmoduleids);
					if($joinresult->num_rows() >0) {
						foreach($joinresult->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
							//condition name
							$condcolname = $show->viewcreationcolmodelcondname;
							//condition value
							$condcolvalue = $show->viewcreationcolmodelcondvalue;
						}
						//restrict repeated parent join
						if(in_array($destab,$ptab)) {
							if($destab != $soucrcetab) {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							} else {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							}
						}
						//parent table condition
						if($condcolname != "" && $condcolvalue != "") {
							$attrcond .= " ".$con." ".$srcjointab.'.'.$condcolname."=".$condcolvalue;
							array_push($attrcondarr,$srcjointab.'.'.$joincolname);
							$x = 1;
						}
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] = $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.'='.$maintable.'.'.$primaryid.') > 0 AND '.$ptabjoin.'.status=1';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 2) {
								//attribute join
								if(!in_array($colinfo['coltablename'][$k],$jtab)) {
									array_push($jtab,trim($colinfo['coltablename'][$k]));
									$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
								}
								//condition
								if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
									$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
									array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
									$x = 1;
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 3) {
					$result = $this->Basefunctions->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						}
					}  else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.','.$maintable.'.'.$primaryid.') > 0 AND '.$ptabjoin.'.status=1';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 3) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
									}
								}
							}
						}
					}
				}
			}
			$dduitypeids = array('17','18','19','23','25','26','27','28','29');
			//fields fetch
			if($colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->Basefunctions->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].', GROUP_CONCAT(DISTINCT userrole.userrolename) AS rolename, GROUP_CONCAT(DISTINCT employeegroup.employeegroupname) AS empgrpname';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k].',userrole.userrolename AS rolename,employeegroup.employeegroupname AS empgrpname';
				}
			} else if( in_array($colinfo['colmodeluitype'][$k],$dduitypeids) ) {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->Basefunctions->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].'';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
				}
			} else {
				$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
			}
		}
		if($maintable == 'employee') {
			$status = $maintable.'.status NOT IN (3)';
		} else {
			$status = $maintable.'.status IN (1)';
		}
		if($listid != '') {
			$listid = $listid;
		}else {
			$listid = 1;
		}
		$maillistid = 'subscribers.campaigngroupsid='.$listid.' AND ';
		$where = '1=1';
		$ewh = 'AND 1=1';
		$conditionvalarray = $this->Basefunctions->conditioninfofetch($creationid);
		$c = count($conditionvalarray);
		$d = count($multiarray);
		if($c>0) {
			$whereString=$this->whereclausegeneration($conditionvalarray);
		} else {
			$whereString=$ewh;
		}
		if($d>0) {
			$extrawhereString=$this->whereclausegeneration($multiarray);
		} else {
			$extrawhereString=$ewh;
		}
		$industry = $maintable.'.industryid IN ('.$industryid.')';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		//query
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$selvalue.' from '.$tablename.' '.$joinq.' WHERE '.$where.' '.$whereString.' '.$extrawhereString.' AND '.$maillistid.' '.$status.' AND '.$industry.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		return $data;
	}
	//where condition generation
    public function whereclausegeneration($conditionvalarray) {
		$whereString="";
		$m=0;
		$count = count($conditionvalarray);
		$braces = '';
		foreach ($conditionvalarray as $key) {
			if($m == 0) {
				$key[4] = "";
				$whereString .= " AND ".str_repeat("(",$count-1)."";
			} else {
				if($key[4] != 'AND' and $key[4] != 'OR') {
					$key[4] = 'AND';
				}
			}
			if($m >= 1){
				$braces = ')';
			}
			$whereString.=$this->conditiongenerate($key,$braces);
			$m++;
		}
		if($whereString !="" ) {
			$whereString .= "";
		}
		return $whereString;
    }
	//condition generation
	public function conditiongenerate($key,$braces) {
		switch($key[2]) {
			case "Equalto":
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." = '".$key[3]."'".$braces;
				break;
			case "NotEqual": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." != '".$key[3]."'".$braces;
				break;
			case "Startwith":
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." LIKE '".$key[3].'%'."'".$braces;
				break;
			case "Endwith": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." LIKE '".'%'.$key[3]."'".$braces;
				break;
			case "Middle": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." LIKE '".'%'.$key[3].'%'."'".$braces;
				break;
			case "IN": 
				$whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." IN '".$key[3]."'".$braces;
				break;
			case "NOT IN": 
				return $whereString.=" ".$key[4]." ".$key[0].'.'.$key[1]." NOT IN '".$key[3]."'".$braces;
				break;
			case "GreaterThan": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." > '" .$key[3]."'".$braces;
				break;
			case "LessThan": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." < '" .$key[3]."'".$braces;
				break;
			case "GreaterThanEqual": 
				return $whereString= " ".$key[4]." ".$key[0].'.'.$key[1]." >= '" .$key[3]."'".$braces;
				break;
			case "LessThanEqual": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." <= '" .$key[3]."'".$braces;
				break;
			default:
				return $whereString="";
				break;
		}
	}
	//fetch colname & colmodel information fetch model
    public function extragridinformationfetchmodel($creationid,$extracolinfo) {
		//fetch show col ids
		$this->db->select('viewcreation.viewcreationcolumnid,viewcreation.viewcreationid,viewcreationmodule.viewcreationmoduleid,viewcreationmodule.viewcreationmodulename,viewcreation.viewcolumnsize',false);
		$this->db->from('viewcreation');
		$this->db->join('viewcreationmodule','viewcreationmodule.viewcreationmoduleid=viewcreation.viewcreationmoduleid');
		$this->db->where_in('viewcreation.viewcreationid',$creationid);
		$this->db->where('viewcreation.status',1);
		$result=$this->db->get()->result();
		foreach($result as $row) {
			$colids=$row->viewcreationcolumnid;
			$modname = $row->viewcreationmodulename;
			$modid = $row->viewcreationmoduleid;
			$colsize = $row->viewcolumnsize;
		}
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$headcolids = $this->Basefunctions->mobiledatarowheaderidfetch($creationid);
			$vwcolids = explode(',',$colids);
			$vwmgcolids = array_merge($vwcolids,$headcolids);
			$viewcolids = array_unique($vwmgcolids);
		} else {
			$viewcolids = explode(',',$colids);
		}
		/* for fetch colname & colmodel fetch */
		$i=0;
		$m=0;
		$data = array();
		$colsizes = explode(',',$colsize);
		$this->db->select("viewcreationcolumns.viewcreationcolumnid,
					viewcreationcolumns.viewcreationcolumnname,
					viewcreationcolumns.viewcreationcolmodelname,
					viewcreationcolumns.viewcreationcolmodelindexname,
					viewcreationcolumns.viewcreationcolmodeljointable,
					viewcreationcolumns.viewcreationcolumnid,
					viewcreationcolumns.viewcreationcolmodelaliasname,
					viewcreationcolumns.viewcreationcolmodeltable,
					viewcreationcolumns.viewcreationparenttable,
					viewcreationcolumns.viewcreationmoduleid,
					viewcreationcolumns.viewcreationjoincolmodelname,
					viewcreationcolumns.viewcreationcolumnviewtype,
					viewcreationcolumns.viewcreationtype,
					viewcreationcolumns.viewcreationcolmodelcondname,
					viewcreationcolumns.uitypeid,
					viewcreationcolumns.moduletabsectionid,
					viewcreationcolumns.viewcreationcolmodelcondvalue,
					viewcreationcolumns.viewcreationcolumnicon,
					viewcreationcolumns.viewcreationcolumnsize");
		$this->db->from('viewcreationcolumns');
		$this->db->where_in('viewcreationcolumns.viewcreationcolumnid',$viewcolids);
		$this->db->where('viewcreationcolumns.status',1);
		$showfield = $this->db->get();
		if($showfield->num_rows() >0) {
			foreach($showfield->result() as $show) {
				$data['colid'][$i]=$show->viewcreationcolumnid;
					$data['colname'][$i]=$show->viewcreationcolumnname;
					$data['colicon'][$i]=$show->viewcreationcolumnicon;
					$data['colsize'][$i]=$colsizes[$m];
					$data['colmodelname'][$i]=$show->viewcreationcolmodelname;
					$data['colmodelaliasname'][$i]=$show->viewcreationcolmodelaliasname;
					$data['coltablename'][$i]=$show->viewcreationcolmodeltable;
					$data['coljointablename'][$i]=$show->viewcreationcolmodeljointable;
					$data['coljoinmodelname'][$i]=$show->viewcreationjoincolmodelname;
					$data['colmodelindex'][$i]=$show->viewcreationcolmodeljointable .'.'.$show->viewcreationcolmodelindexname;
					$data['colmodelparenttable'][$i]=$show->viewcreationparenttable;
					$data['colmodelviewtype'][$i]=$show->viewcreationcolumnviewtype;
					$data['colmodeldatatype'][$i]=$show->viewcreationtype;
					$data['colmodelcondname'][$i]=$show->viewcreationcolmodelcondname;
					$data['colmodelcondvalue'][$i]=$show->viewcreationcolmodelcondvalue;
					$data['colmodelmoduleid'][$i]=$show->viewcreationmoduleid;
					$data['colmodeluitype'][$i]=$show->uitypeid;
					$data['colmodelsectid'][$i]=$show->moduletabsectionid;
					$data['colmodeltype'][$i]='text';
					$data['modname'][$i] = $modname;
					$i++;
			}
		}
		return $data;
    }
	 //fetch colname & colmodel information fetch based viewcolumnids 
    public function viewcolumndatainformationfetch($viewcolids) {
		$i=0;
		$data = array();
		$this->db->select("viewcreationcolumns.viewcreationcolumnname,
		viewcreationcolumns.viewcreationcolmodelname,
		viewcreationcolumns.viewcreationcolmodelindexname,
		viewcreationcolumns.viewcreationcolmodeljointable,
		viewcreationcolumns.viewcreationcolumnid,
		viewcreationcolumns.viewcreationcolmodelaliasname,
		viewcreationcolumns.viewcreationcolmodeltable,
		viewcreationcolumns.viewcreationparenttable,
		viewcreationcolumns.viewcreationmoduleid,
		viewcreationcolumns.viewcreationjoincolmodelname,
		viewcreationcolumns.viewcreationcolumnviewtype,
		viewcreationcolumns.viewcreationtype,
		viewcreationcolumns.viewcreationcolmodelcondname,
		viewcreationcolumns.viewcreationcolmodelcondvalue");
		$this->db->from('viewcreationcolumns');
		$this->db->where_in('viewcreationcolumns.viewcreationcolumnid',$viewcolids);
		$this->db->where('viewcreationcolumns.status',1);
		$showfield = $this->db->get();
		if($showfield->num_rows() >0) {
			foreach($showfield->result() as $show) {
				$data['colname'][$i]=$show->viewcreationcolumnname;
				$data['colmodelname'][$i]=$show->viewcreationcolmodelname;
				$data['colmodelaliasname'][$i]=$show->viewcreationcolmodelaliasname;
				$data['coltablename'][$i]=$show->viewcreationcolmodeltable;
				$data['coljointablename'][$i]=$show->viewcreationcolmodeljointable;
				$data['coljoinmodelname'][$i]=$show->viewcreationjoincolmodelname;
				$data['colmodelindex'][$i]=$show->viewcreationcolmodeljointable .'.'.$show->viewcreationcolmodelindexname;
				$data['colmodelparenttable'][$i]=$show->viewcreationparenttable;
				$data['colmodelviewtype'][$i]=$show->viewcreationcolumnviewtype;
				$data['colmodeldatatype'][$i]=$show->viewcreationtype;
				$data['colmodelcondname'][$i]=$show->viewcreationcolmodelcondname;
				$data['colmodelcondvalue'][$i]=$show->viewcreationcolmodelcondvalue;
				$data['colmodelmoduleid'][$i]=$show->viewcreationmoduleid;
				$data['colmodeltype'][$i]='text';
				$i++;
			}
		}
		return $data;
    }
	//sms segment insertion
	public function newsegmentdatacreatemodel() {
		$segmentname = $_POST['segment'];
		$decription = $_POST['decription'];
		$autoupdate = $_POST['autoupdate'];
		$groupid = $_POST['groupid'];
		$subscriberids = $_POST['subscriberids'];
		$typeid = $_POST['typeid'];
		$date = date($this->Basefunctions->datef);
		$segment= array(
				'templatetypeid'=>$typeid,
				'segmentsname'=>$segmentname,
				'campaigngroupsid'=>$groupid,
				'subscribersid'=>$subscriberids,
				'description'=>$decription,
				'autoupdate'=>$autoupdate,
				'industryid'=>$this->Basefunctions->industryid,
				'branchid'=>$this->Basefunctions->branchid,
				'createdate'=>$date,
				'lastupdatedate'=>$date,
				'createuserid'=>$this->Basefunctions->userid,
				'lastupdateuserid'=>$this->Basefunctions->userid,
				'status'=>$this->Basefunctions->activestatus
			);
		$this->db->insert('segments',$segment);
		$segmentsid=$this->Basefunctions->maximumid('segments','segmentsid');
		$count= $_POST['numofrows'];
		$griddata= $_POST['griddatas'];
		$formdata=json_decode( $griddata, true );
		//criteria condition create
		if($formdata != "") {
			for ( $i=0;$i<$count;$i++ ) {
				$condcolumn=array(
							'segmentsid'=>$segmentsid,
							'viewcreationcolumnid'=>$formdata[$i]['fieldid'],
							'viewcreationconditionname'=>$formdata[$i]['condition'],
							'viewcreationconditionvalue'=>$formdata[$i]['critreiavalue'],
							'viewcreationandorvalue'=>$formdata[$i]['massandorcond'],
							'createdate'=>$date,
							'lastupdatedate'=>$date,
							'createuserid'=>$this->Basefunctions->userid,
							'lastupdateuserid'=>$this->Basefunctions->userid,
							'status'=>$this->Basefunctions->activestatus
							);
				$datacon=array_filter($condcolumn);	
				$this->db->insert('segmentscriteria',$datacon);
			}
		}
		echo "TRUE";
	}
	//solutions document details fetch
	public function smscreteriagridfetchmodel() {
		$productdetail = '';
		$smssegmentsid=trim($_GET['primarydataid']);
		$this->db->select('segmentscriteria.segmentscriteriaid,segments.campaigngroupsid,segments.templatetypeid,templatetype.templatetypename,campaigngroups.campaigngroupsname,segmentscriteria.viewcreationcolumnid,viewcreationcolumns.viewcreationcolumnname,segmentscriteria.viewcreationconditionname,segmentscriteria.viewcreationconditionvalue,segmentscriteria.viewcreationandorvalue,viewcreationcolumns.viewcreationcolmodeljointable,viewcreationcolumns.viewcreationcolmodelindexname');
		$this->db->from('segmentscriteria');
		$this->db->join('viewcreationcolumns','viewcreationcolumns.viewcreationcolumnid=segmentscriteria.viewcreationcolumnid');
		$this->db->join('segments','segments.segmentsid=segmentscriteria.segmentsid');
		$this->db->join('campaigngroups','campaigngroups.campaigngroupsid=segments.campaigngroupsid');
		$this->db->join('templatetype','segments.templatetypeid=templatetype.templatetypeid');
		$this->db->where_not_in('segmentscriteria.status',$this->Basefunctions->statusinfo);
		$this->db->where('segmentscriteria.segmentsid',$smssegmentsid);
		$data=$this->db->get()->result();
		$j=0;
		foreach($data as $value) {
			$productdetail->rows[$j]['id']=$value->segmentscriteriaid;
			$productdetail->rows[$j]['cell']=array(
					$value->templatetypename,
					$value->templatetypeid,
					$value->campaigngroupsname,
					$value->campaigngroupsid,
					$value->viewcreationcolumnname,
					$value->viewcreationcolumnid,
					$value->viewcreationconditionname, 
					$value->viewcreationconditionname,
					$value->viewcreationconditionvalue,
					$value->viewcreationconditionvalue,
					$value->viewcreationconditionvalue,
					$value->viewcreationandorvalue,
					$value->viewcreationandorvalue,
					$value->viewcreationconditionname,
					$value->viewcreationandorvalue,
					$value->viewcreationconditionvalue,
					$value->viewcreationcolmodeljointable,
					$value->viewcreationcolmodelindexname,
					$value->viewcreationcolumnid,
					$value->segmentscriteriaid,
			);
			$j++;
		}
		if($productdetail == '') {
			$productdetail = array('fail'=>'FAILED');
		}
		//print_r($productdetail); die;
		echo  json_encode($productdetail);
	}
	//sms segment data fetch
	public function smssegmentsdatafetchmodel() {
		$id  = $_GET['ids'];	
		$data ='';
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('templatetypeid,segmentsname,campaigngroupsid,subscribersid,description,autoupdate');
		$this->db->from('segments');
		$this->db->where('segments.segmentsid',$id);
		$this->db->where("FIND_IN_SET('$industryid',segments.industryid) >", 0);
		$this->db->where('segments.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('segnmae'=>$row->segmentsname,'typeid'=>$row->templatetypeid,'groupid'=>$row->campaigngroupsid,'subsid'=>$row->subscribersid,'description'=>$row->description,'autoupdate'=>$row->autoupdate);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//sms segments update
	public function smssegmentsupdatemodel() {
		$typeid = $_POST['typeid'];
		$deleterowid = $_POST['deleterowid'];
		$deletecondid = explode(',',$deleterowid);
		$segmentid = $_POST['segmentid'];
		$segmentname = $_POST['segment'];
		$decription = $_POST['decription'];
		$autoupdate = $_POST['autoupdate'];
		$groupid = $_POST['groupid'];
		$subscriberids = $_POST['subscriberids'];
		$date = date($this->Basefunctions->datef);
		$segment= array(
				'templatetypeid' => $typeid,
				'segmentsname'=>$segmentname,
				'campaigngroupsid'=>$groupid,
				'subscribersid'=>$subscriberids,
				'description'=>$decription,
				'autoupdate'=>$autoupdate,
				'lastupdatedate'=>$date,
				'lastupdateuserid'=>$this->Basefunctions->userid,
				'status'=>$this->Basefunctions->activestatus
			);
		$this->db->where('segments.segmentsid',$segmentid);
		$this->db->update('segments',$segment);
		$count=$_POST['count'];
		$griddata=$_POST['viewdata'];
		$formdata=json_decode( $griddata,true);
		//view condition create
		if($formdata != "") {
			for ( $i=0;$i<$count;$i++ ) {
				if($formdata[$i]['criteriaid'] == "") {
					$condcolumn=array(
									'segmentsid'=>$segmentid,
									'viewcreationcolumnid'=>$formdata[$i]['fieldid'],
									'viewcreationconditionname'=>$formdata[$i]['condition'],
									'viewcreationconditionvalue'=>$formdata[$i]['critreiavalue'],
									'viewcreationandorvalue'=>$formdata[$i]['massandorcond'],
									'createdate'=>$date,
									'lastupdatedate'=>$date,
									'createuserid'=>$this->Basefunctions->userid,
									'lastupdateuserid'=>$this->Basefunctions->userid,
									'status'=>$this->Basefunctions->activestatus
									);
					$datacon=array_filter($condcolumn);	
					$this->db->insert('segmentscriteria',$datacon);
				}
			}
		}
		//delete condition
		foreach($deletecondid as $conrowid) {
			$updatedcondata=array('lastupdatedate'=>$date,'lastupdateuserid'=>$this->Basefunctions->userid,'status'=>0);
			$this->db->update('segmentscriteria',$updatedcondata,array('createuserid'=>$this->Basefunctions->userid,'segmentscriteriaid'=>$conrowid));
		}
		echo "TRUE";
	}
	//sms segments delete
	public function smssegmentsdeletemodel() {
		$segmentid = $_GET['ids'];
		$date = date($this->Basefunctions->datef);
		$deletearray = array('lastupdatedate'=>$date,'lastupdateuserid'=>$this->Basefunctions->userid,'status'=>0);
		$this->db->where('segments.segmentsid',$segmentid);
		$this->db->update('segments',$deletearray);
		echo 'TRUE';
	}
	//field name based drop down value get - picklist
	public function fieldnamebesdpicklistddvaluemodel() {
		$i=0;
		$moduleid = 'moduleid';
		$fieldname = $_GET['fieldid'];
		$mid = $_GET['moduleid'];
		$table = substr($fieldname, 0, -4);
		$fieldid = $table.'id';
		$this->db->select($fieldid.','.$fieldname);
		$this->db->from($table);
		$this->db->where("FIND_IN_SET($mid,$table."."$moduleid) >", 0);
		$this->db->where('status',1);
		$reult =$this->db->get();
		if($reult->num_rows() > 0){
			foreach($reult->result() as $row){
				$data[$i] = array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//field name based drop down value get - main drop down
	public function fieldnamebesdddvaluemodel() {
		$i=0;
		$fieldname = $_GET['fieldid'];
		$table = substr($fieldname, 0, -4);
		$fieldid = $table.'id';
		$this->db->select($fieldid.','.$fieldname);
		$this->db->from($table);
		$this->db->where('status',1);
		$reult =$this->db->get();
		if($reult->num_rows() > 0){
			foreach($reult->result() as $row){
				$data[$i] = array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	function localviewgridheadergenerate($colinfo,$modulename,$width,$height) {
		$CI =& get_instance();
		/* width & height set */
		$columnswidth = 1200;
		$addcolsize = 0;
		$reccount = count($colinfo['colmodelname']);
		$columnswidth = 5;
		foreach ($colinfo['colmodelviewtype'] as $type) {
			$columnswidth += $type=='1'? 150 : 0;
		}
		$extrasize = ($columnswidth>$width)?0 : ($width-$columnswidth);
		$addcolsize = $extrasize!=0 ? round($extrasize/$reccount) : 0;
		$columnswidth = ($columnswidth>$width)?$columnswidth : $width;
		/* height set */
		if($height<=420) {
			$datarowheight = $height-35;
		} else {
			$datarowheight = $height-35;
		}
		/* header generation */
		$mdivopen ='<div class="grid-view">';
		$header = '<div class="header-caption" style="width:'.$columnswidth.'px;"><ul class="inline-list">';
		$i=0;
		$modname = strtolower(preg_replace('/[^A-Za-z0-9]/', '', $modulename));
		foreach ($colinfo['fieldlabel'] as $headname) {
			$viewtype = ($colinfo['colmodelviewtype'][$i]=='0') ? ' display:none':'';
			$header .='<li id="'.$modname.'headcol'.$i.'" class="'.$colinfo['colmodelname'][$i].'-class '.$modname.'headercolsort" data-class="'.$colinfo['colmodelname'][$i].'-class" data-sortcolname="'.$colinfo['colmodelindex'][$i].'" data-sortorder="ASC" data-uitype="'.$colinfo['colmodeluitype'][$i].'" data-fieldname="'.$colinfo['fieldname'][$i].'" style="width:'.(150+$addcolsize).'px; '.$viewtype.'" data-width="'.(150+$addcolsize).'" data-viewtype="'.$colinfo['colmodelviewtype'][$i].'" data-position="'.$i.'">'.$headname.'</li>';
			$i++;
		}
		$header .='</ul></div>';
		$content = '<div class="gridcontent" style="height:'.$datarowheight.'px; overflow:auto;"><div class="data-content" style="width:'.$columnswidth.'px;">&nbsp;</div></div';
		$mdivclose = '</div>';
		$datas = $mdivopen.$header.$content.$mdivclose;
		$footer = '';
		/* Footer generation */
		$footer = '<div class="summary-blue">';
		$footer .= '</div>';
		return array('content'=>$datas,'footer'=>$footer);
	}
	
	//group name dd data fetch
	public function segmentgroupddfetchmodel() {
		$typeid = $_GET['typeid'];
		$industryid = $this->Basefunctions->industryid;
		if($typeid) {
			$i=0;
			$this->db->select('campaigngroupsid,campaigngroupsname');
			$this->db->from('campaigngroups');
			$this->db->where_in('campaigngroups.templatetypeid',$typeid);
			$this->db->where("FIND_IN_SET('$industryid',campaigngroups.industryid) >", 0);
			$this->db->where('campaigngroups.status',1);
			$result = $this->db->get();
			if($result->num_rows() > 0){
				foreach($result->result() as $row ) {
					$data[$i] = array('datasid'=>$row->campaigngroupsid,'dataname'=>$row->campaigngroupsname);
					$i++;
				}
				echo json_encode($data);
			} else {
				echo json_encode(array("fail"=>'FAILED'));
			}
		}else{
			echo json_encode(array("fail"=>'NODATA'));
		}
	}
}	
?>