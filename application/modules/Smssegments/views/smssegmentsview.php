<!DOCTYPE HTML>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
<style type="text/css">
.innergridpaddingbtm {
	top:20px !important;
}
</style>
</head>
<body>
	<?php
		$moduleid = implode(',',$moduleids);
		$device = $this->Basefunctions->deviceinfo();
		$dataset['gridenable'] = "yes"; //no
		$dataset['griddisplayid'] = "smssegmentsview";
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['spanattr'] = array();
		$dataset['gridtableid'] = "smssegmentsviewgrid";
		$dataset['griddivid'] = "smssegmentsviewgridnav";
		$dataset['moduleid'] = $moduleids;
		$dataset['forminfo'] = array(array('id'=>'smssegmentsformdiv','class'=>'hidedisplay','formname'=>'smssegmentsform'));
		$this->load->view('Base/basedeleteform');
		if($device=='phone') {
			$this->load->view('Base/gridmenuheadermobile',$dataset);
			$this->load->view('Base/overlaymobile');
			$this->load->view('Base/viewselectionoverlay');
		} else {
			$this->load->view('Base/gridmenuheader',$dataset);
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
	?>
</body>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js File -->
	<script src="<?php echo base_url();?>js/Smssegments/smssegments.js" type="text/javascript"></script>
	<script>
		$(document).ready(function(){
			$("#tabgropdropdown").change(function(){
				var tabgpid = $("#tabgropdropdown").val();
				if(($("#smsgroupsid").val() != '') || ($("#smsgroupsid").val() != undefined ) ) {
					$('.sidebaricons[data-subform="'+tabgpid+'"]').trigger('click');
				}
			});
		});
	</script>	
</html>