<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Smssegments extends CI_Controller {
	function __construct() {
    	parent::__construct();	
		$this->load->model('Smssegments/Smssegmentsmodel');
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Base/Basefunctions');
    }
	public function index() {
		$moduleid = array(25);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(25);
		$viewmoduleid = array(25);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Smssegments/smssegmentsview',$data);
	}
	//cond field name load
	public function viewdropdownload() {
		$this->Smssegmentsmodel->viewdropdownloadmodel();
	}
	//check box value get
	public function checkboxvalueget() {
		$this->Smssegmentsmodel->checkboxvaluegetmodel();
	}
	//mass update default view fetch
	public function defaultviewfetch() {
		$mid = $_GET['modid'];
		$viewid = $this->Smssegmentsmodel->defaultviewfetchmodel($mid);
		echo $viewid;
	}
	//parent table get
	public function parenttableget() {
		$this->Smssegmentsmodel->parenttablegetmodel();
	}
	//fetch json data information
	public function gridvalinformationfetch() {
		$creationid = $_GET['viewid'];
		$groupid = $_GET['smsgroupsid'];
		$moduleid = $_GET['moduleid'];
		$information = $this->Basefunctions->gridinformationfetchmodel($creationid);
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$chkbox = $_GET['checkbox'];
		$size = array();
		for($i=0;$i<count($information['colmodelname']);$i++) {
			$size[$i] = '200';
		}
		$colmodelname = $information['colmodelname'];
		$colmodelaliasname = $information['colmodelaliasname'];
		$coltablename = $information['coltablename'];
		$colmodeluitype = $information['colmodeluitype'];
		$colname = $information['colname'];
		$colsize = $size;
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : $primarytable.'.'.$primaryid) : $primarytable.'.'.$primaryid;
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>$colmodelname,'colmodelindex'=>$colmodelaliasname,'coltablename'=>$coltablename,'uitype'=>$colmodeluitype,'colname'=>$colname,'colsize'=>$colsize);
		$result=$this->Smssegmentsmodel->viewdynamicdatainfofetch($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$information,$moduleid,$creationid,$primaryid,$groupid);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($result,$page,'');
		$device = $this->Basefunctions->deviceinfo();
		//if($device=='phone') {
			//$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Subscriber List',$width,$height,$chkbox);
		//} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Subscriber List',$width,$height,$chkbox);
		//}
		echo json_encode($datas);
	}
	//new segment data create_function
	public function newsegmentdatacreate() {
		$this->Smssegmentsmodel->newsegmentdatacreatemodel();
	}
	//pagination set page
	public function generatepage($p) {
		if($p=='NaN')  {
			$p=1; //all
		} else {
			$p=$p;//harcoded value
		}
		return $p;
	}//document details fetch
	public function  smscreteriagriddatafetch() {
		$this->Smssegmentsmodel->smscreteriagridfetchmodel();
	}
	//sms segment data fetch
	public function smssegmentsdatafetch() {
		$this->Smssegmentsmodel->smssegmentsdatafetchmodel();
	}
	//sms segment update
	public function updatesegmentdatacreate() {
		$this->Smssegmentsmodel->smssegmentsupdatemodel();
	}
	//sms segments delete
	public function smssegmentsdelete() {
		$this->Smssegmentsmodel->smssegmentsdeletemodel();
	}
	//group name dd data fetch
	public function segmentgroupddfetch() {
		$this->Smssegmentsmodel->segmentgroupddfetchmodel();
	}
	//field name based drop down value - picklist
	public function fieldnamebesdpicklistddvalue() {
		$this->Smssegmentsmodel->fieldnamebesdpicklistddvaluemodel();
	}
	//field name based drop down value - main drop down
	public function fieldnamebesdddvalue() {
		$this->Smssegmentsmodel->fieldnamebesdddvaluemodel();
	}
}