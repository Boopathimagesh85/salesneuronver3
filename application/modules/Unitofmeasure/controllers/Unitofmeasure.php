<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Unitofmeasure extends MX_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Unitofmeasure/Uommodel');
		$this->load->view('Base/formfieldgeneration');
    }
    //first basic hitting view
    public function index() {        
    	$moduleid = array(10);
    	sessionchecker($moduleid);
		/*action*/
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(10);
		$viewmoduleid = array(10);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
      	$this->load->view('Unitofmeasure/uomview',$data);
	}
	//create itemcounter
	public function newdatacreate() {  
    	$this->Uommodel->newdatacreatemodel();
    }
	//information fetchitemcounter
	public function fetchformdataeditdetails() {
		$moduleid = 10;
		$this->Uommodel->informationfetchmodel($moduleid);
	}
	//update itemcounter
    public function datainformationupdate() {
        $this->Uommodel->datainformationupdatemodel();
    }
	//delete itemcounter
    public function deleteinformationdata() {
        $moduleid = 10;
		$this->Uommodel->deleteoldinformation($moduleid);
    } 
}