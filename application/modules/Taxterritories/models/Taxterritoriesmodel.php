<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Taxterritoriesmodel extends CI_Model{
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//taxterritories create
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['taxterritorieselementsname']);
		$formfieldstable = explode(',',$_POST['taxterritorieselementstable']);
		$formfieldscolmname = explode(',',$_POST['taxterritorieselementscolmn']);
		$elementpartable = explode(',',$_POST['taxterritorieselementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		//check the rate exists already.
		$id=$this->db->select('taxterritoriesid')
					 ->from('taxterritories')
					 ->where(array('taxid'=>$_POST['taxid'],'territoryid'=>$_POST['territoryid'],'status'=>$this->Basefunctions->activestatus))
					 ->limit(1)
					 ->get();
		if($id->num_rows() > 0){
			//if records exits previously update the records
			foreach($id->result() as $info){
				$taxid= $info->taxterritoriesid;
			}
			$value_array=array('taxrate'=>trim($_POST['regiontaxrate']),'lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid);
			$this->db->where('taxterritoriesid',$taxid);
			$this->db->update('taxterritories',$value_array);
		}
		else{
			$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);		
		}		
		echo 'TRUE';
	}
	//Retrive taxterritories data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['taxterritorieselementsname']);
		$formfieldstable = explode(',',$_GET['taxterritorieselementstable']);
		$formfieldscolmname = explode(',',$_GET['taxterritorieselementscolmn']);
		$elementpartable = explode(',',$_GET['taxterritorieselementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['taxterritoriesprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	//taxterritories update
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['taxterritorieselementsname']);
		$formfieldstable = explode(',',$_POST['taxterritorieselementstable']);
		$formfieldscolmname = explode(',',$_POST['taxterritorieselementscolmn']);
		$elementpartable = explode(',',$_POST['taxterritorieselementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['taxterritoriesprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		$taxprimaryid = $_POST['taxterritoriesprimarydataid'];
		$taxid = $_POST['taxid'];
		$territoryid = $_POST['territoryid'];
		$ludate = date($this->Basefunctions->datef);
		$luuserid= $this->Basefunctions->userid;
		$delete=array('status'=>0,'lastupdatedate'=>$ludate,'lastupdateuserid'=>$luuserid);
		$this->db->where_in('taxid',$taxid);
		$this->db->where_in('territoryid',$territoryid);
		$this->db->where_not_in('taxterritoriesid',$taxprimaryid);
		$this->db->update('taxterritories',$delete);
		echo $result;
	}
	//taxterritories delete
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['taxterritorieselementstable']);
		$parenttable = explode(',',$_GET['taxterritoriesparenttable']);
		$id = $_GET['taxterritoriesprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				echo "TRUE";
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			echo "TRUE";
		}
	} 
	/**
	*Retrieves the Tax of Variant Types
	*@returns the variant taxes
	*/
	public function gettaxname() {
		$tax=array();
		$data=$this->db->query("SELECT taxid,taxname
							   FROM tax
							   WHERE taxmodeid=3 AND status = 1");
		foreach($data->result() as $inf){
			$tax[]=array('id'=>$inf->taxid,'name'=>$inf->taxname);
		}
		echo json_encode($tax);
	}
}