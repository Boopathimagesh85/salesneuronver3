<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Taxterritories extends MX_Controller{
    private $taxterritories = 20;
	public function __construct() {
        parent::__construct();
        $this->load->view('Base/formfieldgeneration');
        $this->load->helper('formbuild');
		$this->load->model('Taxterritories/Taxterritoriesmodel');	
    }
    //first basic hitting view
    public function index() {        
    	$moduleid = array($this->taxterritories);
    	sessionchecker($moduleid);
		//action
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array($moduleid);
		$viewmoduleid = array($moduleid);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Taxterritories/taxterritoriesview',$data);
	}
	//create taxterritories
	public function newdatacreate() {  
    	$this->Taxterritoriesmodel->newdatacreatemodel();
    }
	//information fetchtaxterritories
	public function fetchformdataeditdetails() {		
		$this->Taxterritoriesmodel->informationfetchmodel($this->taxterritories);
	}
	//update taxterritories
    public function datainformationupdate() {
        $this->Taxterritoriesmodel->datainformationupdatemodel();
    }
	//delete taxterritories
    public function deleteinformationdata() {       
		$this->Taxterritoriesmodel->deleteoldinformation($this->taxterritories);
    } 	
	/**
	*Retrieves the Tax of Variant Types
	*@returns the variant taxes
	*/
	public function gettaxname() {
		$this->Taxterritoriesmodel->gettaxname();
	}
}