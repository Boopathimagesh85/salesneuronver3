<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Puritymodel extends CI_Model{
    public function __construct(){
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//itemcounter create
	public function newdatacreatemodel(){
		//table and fields information
		$formfieldsname = explode(',',$_POST['purityelementsname']);
		$formfieldstable = explode(',',$_POST['purityelementstable']);
		$formfieldscolmname = explode(',',$_POST['purityelementscolmn']);
		$elementpartable = explode(',',$_POST['purityelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
		$primaryid = $this->db->insert_id();
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity = ''.$user.' created Purity - '.$_POST['purityname'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Created',$activity ,$userid,54);
		echo 'TRUE';
	}
	//Retrive itemcounter data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['purityelementsname']);
		$formfieldstable = explode(',',$_GET['purityelementstable']);
		$formfieldscolmname = explode(',',$_GET['purityelementscolmn']);
		$elementpartable = explode(',',$_GET['purityelementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['purityprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$sndefault = $this->Basefunctions->singlefieldfetch('sndefault','purityid','purity',$primaryid); //Check default value
		if($sndefault == 1) {
			$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
			echo $result;
		} else {
			echo json_encode(array("fail"=>'sndefault'));
		}
	}
	//itemcounter update
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['purityelementsname']);
		$formfieldstable = explode(',',$_POST['purityelementstable']);
		$formfieldscolmname = explode(',',$_POST['purityelementscolmn']);
		$elementpartable = explode(',',$_POST['purityelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['purityprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity = ''.$user.' updated Purity - '.$_POST['purityname'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$activity ,$userid,54);
		echo $result;
	}
	//itemcounter delete
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['purityelementstable']);
		$parenttable = explode(',',$_GET['purityparenttable']);
		$id = $_GET['purityprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$purityname = $this->Basefunctions->singlefieldfetch('purityname','purityid','purity',$id); //Purity name
		$activity = ''.$user.' deleted Purity - '.$purityname.'';
		$sndefault = $this->Basefunctions->singlefieldfetch('sndefault','purityid','purity',$id); //Check default value
		if($sndefault == 2) {
			echo 'sndefault';
		} else {
			if($ruleid == 1 || $ruleid == 2) {
				$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
				if($chek == 0) {
					echo 'Denied';
				} else {
					$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
					$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,54);
					echo "TRUE";
				}
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,54);
				echo "TRUE";
			}
		}
	}
	public function puritymapcheckproduct() {
		$purityid = $_POST['purityid'];
		$this->db->select('productid');
		$this->db->from('product');
		$this->db->where('status',$this->Basefunctions->activestatus);
		$this->db->where("FIND_IN_SET('$purityid',purityid) >", 0);
		$this->db->where("productid !=",3);
		$data=$this->db->get()->num_rows();
		if($data > 0) {
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
	}
}