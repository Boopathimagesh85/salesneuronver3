<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Purity extends MX_Controller{
    public function __construct(){
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Purity/Puritymodel');	
    }
    //first basic hitting view
    public function index() {
    	$moduleid = array(54);
    	sessionchecker($moduleid);
		//action
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['meltingvalue']=$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',4); //melting round-off
		$this->load->view('Purity/purityview',$data);
	}
	//create metal
	public function newdatacreate() {  
    	$this->Puritymodel->newdatacreatemodel();
    }
	//information fetchitemcounter
	public function fetchformdataeditdetails() {
		$moduleid = 54;
		$this->Puritymodel->informationfetchmodel($moduleid);
	}
	//update metal
    public function datainformationupdate() {
        $this->Puritymodel->datainformationupdatemodel();
    }
	//delete metal
    public function deleteinformationdata() {
        $moduleid = 54;
		$this->Puritymodel->deleteoldinformation($moduleid);
    }
    public function loadmetal()
    {
    	$metalarr = array();
    	$metal = $this->Basefunctions->simpledropdownwithcondorder('metalid','metalname','metal','','','metalid');
    	foreach($metal as $info){
    		$metalarr[]=array(
    				'metalid' => $info->metalid,
    				'metalname' => $info->metalname,
    		);
    	}
    	echo json_encode($metalarr);
    }
	//check purity in product
    public function puritymapcheckproduct() {
        $this->Puritymodel->puritymapcheckproduct();
    }
}