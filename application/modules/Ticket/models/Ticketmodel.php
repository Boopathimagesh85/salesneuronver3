<?php
class Ticketmodel extends CI_Model{
	function __construct() {
		parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
	}
	//Ticket create
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$restricttable = explode(',',$_POST['resctable']);
		//industry based moduleid
		$moduleid = $_POST['viewfieldids'];
		//Retrive the Ticket autoNumber
		$anfieldnameinfo = $_POST['anfieldnameinfo'];
		$anfieldnameidinfo = $_POST['anfieldnameidinfo'];
		$anfieldtabinfo = $_POST['anfieldtabinfo'];
		$anfieldnamemodinfo = $_POST['anfieldnamemodinfo'];
		$randomnum = $this->Basefunctions->randomnumbergenerator($anfieldnamemodinfo,$anfieldtabinfo,$anfieldnameinfo,$anfieldnameidinfo);
		$_POST['ticketnumber'] = trim($randomnum);
		//dynamic Insertion
		$primaryid = $this->Crudmodel->datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable);
		//notification entry
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		if(isset($_POST['employeeid'])) {
			$assignid = $_POST['employeeid'];
			$emptypes = $_POST['employeetypeid'];
			$empdataids = array();
			$assignempids = array();
			if($assignid != '') {
				$i = 0;
				$m=0;
				$empiddatas = explode(',',$assignid);
				$emptypeids = explode(',',$emptypes);
				foreach($empiddatas as $empids) {
					$emptype = $emptypeids[$m];
					if($emptype == 1) {
						$empdataids[$i] = $empids;
						$i++;
					} else if($emptype == 2) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromgroup($empids);
						$i++;
					} else if($emptype == 3) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromroles($empids);
						$i++;
					} else if($emptype == 4) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromrolesandsubroles($empids);
						$i++;
					}
					$m++;
				}
			} else {
				$assignid = 1;
			}
		} else {
			$assignid = 1;
		}
		$tickname = $_POST['ticketnumber'];
		if($assignid == '1'){
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,2);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,$moduleid);
			}
		} else {
			foreach($empdataids as $empidinfo) {
				if(is_array($empidinfo)) { //group of employees
					foreach($empidinfo as $empid) {
						if( !in_array($empid,$assignempids) ) {
							array_push($assignempids,$empid);
							$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,2);
							if($notimsg != '') {
								$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,$moduleid);
							}
						}
					}
				} else { //individual employees
					if( !in_array($empidinfo,$assignempids) ) {
						array_push($assignempids,$empidinfo);
						$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,2);
						if($notimsg != '') {
							$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,$moduleid);
						}
					}
				}
			}
		}
		{//workflow management -- for data create
			$this->Basefunctions->workflowmanageindatacreatemode($moduleid,$primaryid,$partablename);
		}
		echo "TRUE";
	}
	//Retrieve Ticket data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['dataprimaryid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	//Ticket update
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['primarydataid'];
		$restricttable = explode(',',$_POST['resctable']);
		//industry based moduleid
		$moduleid = $_POST['viewfieldids'];
		{//fetch old values -- work flow
			$condstatvals=$this->Basefunctions->workflowmanageolddatainfofetch($moduleid,$primaryid);
		}
		$this->Crudmodel->dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable);
		//notification entry
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
			if(isset($_POST['employeeid'])) {
			$assignid = $_POST['employeeid'];
			$emptypes = $_POST['employeetypeid'];
			$empdataids = array();
			$assignempids = array();
			if($assignid != '') {
				$i = 0;
				$m=0;
				$empiddatas = explode(',',$assignid);
				foreach($empiddatas as $empids) {
					$emptype = $emptypes[$m];
					if($emptype == 1) {
						$empdataids[$i] = $empids;
						$i++;
					} else if($emptype == 2) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromgroup($empids);
						$i++;
					} else if($emptype == 3) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromroles($empids);
						$i++;
					} else if($emptype == 4) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromrolesandsubroles($empids);
						$i++;
					}
					$m++;
				}
			} else {
				$assignid = 1;
			}
		} else {
			$assignid = 1;
		}
		$tickname = $_POST['ticketnumber'];
		if($assignid == '1'){
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,3);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,$moduleid);
			}
		} else {
			foreach($empdataids as $empidinfo) {
				if(is_array($empidinfo)) { //group of employees
					foreach($empidinfo as $empid) {
						if( !in_array($empid,$assignempids) ) {
							array_push($assignempids,$empid);
							$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,3);
							if($notimsg != '') {
								$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,$moduleid);
							}
						}
					}
				} else { //individual employees
					if( !in_array($empidinfo,$assignempids) ) {
						array_push($assignempids,$empidinfo);
						$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,3);
						if($notimsg != '') {
							$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,$moduleid);
						}
					}
				}
			}
		}
		{//workflow management for data update
			$this->Basefunctions->workflowmanageindataupdatemode($moduleid,$primaryid,$partablename,$condstatvals);
		}
		echo 'TRUE';
	}
	//Ticket delete
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primarydataid'];
		$msg='False';
		$filename = $this->Basefunctions->generalinformaion('ticket','ticketnumber','ticketid',$id);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		//industrybased moduleid fetch
		$modid = $this->Basefunctions->getmoduleid($moduleid);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				$msg='Denied';
			} else {
				$notimsg = $this->Basefunctions->notificationtemplatedatafetch($modid,$id,$partabname,4);
				if($notimsg != '') {
					$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,$modid);
				}
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				$msg='TRUE';
			}
		} else {
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch($modid,$id,$partabname,4);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,1,$modid);
			}
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			$msg='TRUE';
		}
		{//workflow management -- for data delete
			$this->Basefunctions->workflowmanageindatadeletemode($modid,$id,$primaryname,$partabname);
		}
		echo $msg;
	}
	//Contact drop down value fetch
	public function contactnamefetchvalmodel() {
		$id =$_GET['accountid'];
		$k=0;
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('contactname,contactid',false);
		$this->db->from('contact');
		if($id != 1){
			$this->db->where('contact.accountid',$id);
		}
		$this->db->where("FIND_IN_SET('$industryid',contact.industryid) >", 0);
		$this->db->where('contact.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$k] = array('id'=>$row->contactid,'name'=>$row->contactname);
				$k++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//account details fetch
	public function accountdetailfetchvalmodel() {
		$id =$_GET['accountid'];
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('mobilenumber,emailid',false);
		$this->db->from('account');
		$this->db->where('account.accountid',$id);
		$this->db->where("FIND_IN_SET('$industryid',account.industryid) >", 0);
		$this->db->where('account.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('mobile'=>$row->mobilenumber,'email'=>$row->emailid);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//account name based contact name
	public function fetchcontatdatawithmultiplecondmodel() {
		$i = 0;
		$accid = $_GET['accid'];
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('contactname,contactid',false);
		$this->db->from('contact');
		$this->db->where('contact.accountid',$accid);
		$this->db->where("FIND_IN_SET('$industryid',contact.industryid) >", 0);
		$this->db->where('contact.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				$data[$i] = array('datasid'=>$row->contactid,'dataname'=>$row->contactname); 
			}
			echo json_encode($data);
		}
		else{
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
}