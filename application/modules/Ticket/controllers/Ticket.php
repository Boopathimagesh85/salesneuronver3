<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ticket extends MX_Controller{
    //Constructor Function To Load Models
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Ticket/Ticketmodel');
	}
	//Default View
	public function index() {
		$moduleid = array(227,78);
		sessionchecker($moduleid);
		//action
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(227,78);
		$viewmoduleid = array(227,78);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['dashboradviewdata'] = $this->Basefunctions->dashboarddataviewbydropdown($moduleid);
        $this->load->view('Ticket/ticketview',$data);	
	}
	//create Campaign
	public function newdatacreate() {
    	$this->Ticketmodel->newdatacreatemodel();
    }
	//information fetch
	public function fetchformdataeditdetails() {
		$moduleid = '227,78';
		$this->Ticketmodel->informationfetchmodel($moduleid);
	}
	//update Campaign
    public function datainformationupdate() {
        $this->Ticketmodel->datainformationupdatemodel();
    }
	//delete Campaign
    public function deleteinformationdata() {
        $moduleid = '227,78';
		$this->Ticketmodel->deleteoldinformation($moduleid);
    }
	//contact name fetch model
	public function contactnamefetchval() {
		$this->Ticketmodel->contactnamefetchvalmodel();
	}
	public function accountdetailfetchval() {
		$this->Ticketmodel->accountdetailfetchvalmodel();
	}
	//editer valuefetch editervaluefetch
	public function editervaluefetch() {
		$filename = $_GET['filename'];
		$newfilename = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]);
			echo json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			echo json_encode($tccontent);
		}
	}
	//account name based contact name
	public function fetchcontatdatawithmultiplecond() {
		$this->Ticketmodel->fetchcontatdatawithmultiplecondmodel();
	}
}