<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Itemtag extends MX_Controller 
{	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
		$this->load->model('Itemtag/Itemtagmodel');
		$this->load->model('Printtemplates/Printtemplatesmodel');
	}
	public function index()
	{		
		/* $this->Basefunctions->generatecleandb();
		die();
		$cldb = $this->db->query("SELECT dbtablename from cleandbgenerate where type = 2");
		$finaltext = '';
		foreach($cldb->result() as $cldbinfo) {
			$data = $this->db->query("SELECT * from ".$cldbinfo->dbtablename." where createdate > '2014-09-23 15:01:00' or lastupdatedate > '2018-09-23 15:01:00'");
			$rows = array();
			$i = 0;
			$qytext = '';
			foreach($data->result() as $info) {
				$rows = (array)$info;
				if($i == 0){
					$tbclname = implode(", ",array_keys($rows)); 
					$qytext .= " REPLACE INTO ".$cldbinfo->dbtablename." (".$tbclname.") VALUES";
				}
				$tddata = implode("','",array_values($rows)); 
				$tddata = "'".$tddata."'";
				$qytext .= " (".$tddata."), "; 
				$i++;
				if($i>5000)
				{
					$i =0;
					$qytext = rtrim($qytext,", ");
					$qytext .= '; </br>'; 
				}
			}
			$qytext = rtrim($qytext,", ");
			$qytext .= '; </br>'; 
			$finaltext .= $qytext ;
		}
		//$finaltext = rtrim($finaltext,", ");
		$fh = fopen('dt.sql','w') or die('fileopenerror');
		fwrite($fh,$finaltext);
		fclose($fh);
		print_r($finaltext);
		die(); */
		
		/* $this->Itemtagmodel->updatediamondcaratweightdetails();
		$this->Itemtagmodel->updatestonecaratweightdetails();
		$this->Itemtagmodel->updatetagimagedetails();
		print_r('Success');
		die(); */
		
		$moduleid = array(50);
		sessionchecker($moduleid);
		//action		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		//view
		$viewfieldsmoduleids =$moduleid;
		$viewmoduleid = $moduleid;	
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd'] = $this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol'] = $this->Basefunctions->vardataviewdropdowncolumns($viewfieldsmoduleids);
		//form
		$data['tagtype'] = $this->Basefunctions->simpledropdown('tagtype','tagtypeid,tagtypename','tagtypeid',array('moduleid'=>$moduleid,'sortbasedfetch'=>'0-49'));
		$data['stoneentrycalctype'] = $this->Basefunctions->simpledropdown('stoneentrycalctype','stoneentrycalctypeid,stoneentrycalctypename','stoneentrycalctypeid');
		$data['branch'] = $this->Basefunctions->simpledropdown('branch','branchid,branchname','branchid');
		$data['tagtemplate'] = $this->Basefunctions->simpledropdownwithcond('tagtemplateid','tagtemplatename','tagtemplate','moduleid',50);
		$data['tagentrytype'] = $this->Basefunctions->simpledropdownwithcond_var('tagtypeid','tagtypename','tagtype','sortorder','50');
		$data['tagentrytype_nontag'] = $this->Basefunctions->simpledropdownwithcond_var('tagtypeid','tagtypename','tagtype','sortorder','50');
		$data['nontagentrytype'] = $this->Basefunctions->simpledropdown('tagentrytype','tagentrytypeid,tagentrytypename','tagentrytypeid',array('moduleid'=>$moduleid,'sortbasedfetch'=>'2-2'));
		$data['entrytypeid'] = $this->Basefunctions->simpledropdownwithcond('tagentrytypeid,tagtypeshow','tagentrytypename','tagentrytype','sortorder','1,3,4,5,6');
		$data['purity'] = $this->Basefunctions->purity_groupdropdown();
		$data['counter'] = $this->Basefunctions->counter_groupdropdown();
		$data['notdefaultcounter'] = $this->Basefunctions->counter_groupdropdown_notdefault();
		$data['defaultcounterwithjewel'] = $this->Itemtagmodel->defaultcounterwithjewel();
		$data['product'] = $this->Itemtagmodel->product_dd();
		$data['processproduct'] = $this->Itemtagmodel->processproduct_dd(); 
		$data['transferprocessproduct'] = $this->Itemtagmodel->transferprocessproduct_dd();
		$data['nontagproduct'] = $this->Itemtagmodel->nontagproduct_dd(); //Non tag product list in dd
		$data['stonename'] = $this->Basefunctions->stone_groupdropdown();
		$data['sizename'] = $this->Itemtagmodel->size_groupdropdown();
		$data['account'] = $this->Basefunctions->accountgroup_dd('16');
		$data['weight_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$data['dia_weight_round'] =$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',8); //Diamond weight round off
		$data['amount_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$data['melting_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',4); //melting round-off
		$data['rate_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //rate round-off
		$data['counter_val'] = $this->Basefunctions->get_company_settings('counter'); // deacitivate to counter dropdown validation
		$data['netwtcalctypeid'] = $this->Basefunctions->get_company_settings('netwtcalctypeid'); // Stone Net Wet Cal
		$data['stonewtcheck'] = $this->Basefunctions->get_company_settings('stoneweightcheck'); // Stone Net Wet Cal
		$data['tagautoimage'] = $this->Basefunctions->get_company_settings('tagautoimage'); // Stone Net Wet Cal
		$data['itemtagauthrole'] = $this->Basefunctions->get_company_settings('itemtagauthrole'); // Stock entry -field changes by roles level
		$data['chargeapply'] = $this->Basefunctions->get_company_settings('tagchargesfrom'); // Chareges from (stock entry / product addon) set
		$data['rfidnoofdigits'] = $this->Basefunctions->get_company_settings('rfidnoofdigits'); // No of digits controlled on company settings
		$data['stonepiecescalc'] = $this->Basefunctions->get_company_settings('stonepiecescalc'); // No of digits controlled on company settings
		$data['itemtagvendorvalidate'] = $this->Basefunctions->get_company_settings('itemtagvendorvalidate');
		$data['tag_fieldrule'] = $this->Basefunctions->retrivemodulerule($moduleid[0],'itemtagform');
		$data['productaddoninfo_dd'] = $this->Itemtagmodel->productaddoninfo_dd();
		//stone,image,addon-license.
		$data['mainlicense'] = array(
			'stone'=>$this->Basefunctions->get_company_settings('stone'),
			'image'=>$this->Basefunctions->get_company_settings('image'),
			'autoweight'=>$this->Basefunctions->get_company_settings('weightscale'),
			'weightscale'=>$this->Basefunctions->get_company_settings('weightscale'),
			'counter'=>$this->Basefunctions->get_company_settings('counter'),
			'lot'=>$this->Basefunctions->get_company_settings('lotdetail'),
			'tagtype'=>$this->Basefunctions->get_company_settings('tagtype'),
			'purchasedata'=>$this->Basefunctions->get_company_settings('purchasedata'),
			'rfidoption'=>$this->Basefunctions->get_company_settings('barcodeoption'),
			'collection'=>$this->Basefunctions->get_company_settings('collection'),
			'stocknotify'=>$this->Basefunctions->get_company_settings('stocknotify'),
			'vacchargeshow'=>$this->Basefunctions->get_company_settings('vacchargeshow'),
			'chargeaccountgroup'=>$this->Basefunctions->get_company_settings('chargeaccountgroup'),
			'chargecalcoption'=>$this->Basefunctions->get_company_settings('chargecalcoption'),
			'chargecalculation'=>$this->Basefunctions->get_company_settings('chargecalculation'),
			'productidshow'=>$this->Basefunctions->get_company_settings('productidshow'),
			'lotpieces'=>$this->Basefunctions->get_company_settings('lotpieces'),
			'storagequantity'=>$this->Basefunctions->get_company_settings('storagequantity'),
			'storagedisplay'=>$this->Basefunctions->get_company_settings('storagedisplay'),
			'grossweighttrigger'=>$this->Basefunctions->get_company_settings('grossweighttrigger'),
			'vendordisable'=>$this->Basefunctions->get_company_settings('vendordisable'),
			'lotweightlimit'=>$this->Basefunctions->get_company_settings('lotweightlimit'),
			'chargerequired'=>$this->Basefunctions->get_company_settings('chargerequired'),
			'prizetaggst'=>$this->Basefunctions->get_company_settings('prizetaggst'),
			'untaguniqueno'=>$this->Basefunctions->get_company_settings('untaguniqueno'),
			'lottolerancepercent'=>$this->Basefunctions->get_company_settings('lottolerancepercent'),
			'lottolerancevalue'=>$this->Basefunctions->get_company_settings('lottolerancevalue'),
			'productaddoninfo'=>$this->Basefunctions->get_company_settings('productaddoninfo'),
			'itemtagsavefocus'=>$this->Basefunctions->get_company_settings('itemtagsavefocus'),
			'salesreturnautomatic'=>$this->Basefunctions->get_company_settings('salesreturnautomatic'),
			'lottolerancediactvalue'=>$this->Basefunctions->get_company_settings('lottolerancediactvalue'),
			'lotdiaweightlimit'=>$this->Basefunctions->get_company_settings('lotdiaweightlimit')
		);
		if($data['mainlicense']['counter'] == 'YES'){
		  $data['fromnotdefaultcounter'] = $this->Basefunctions->counter_groupdropdown_notdefault();
		}else{
		  $data['fromnotdefaultcounter'] = $this->Itemtagmodel->counter_groupdropdown_notdefault();
		}
		$data['planstatus'] = $this->Basefunctions->planid;
		$data['defaultprinttemplate'] = $this->Itemtagmodel->defaultprinttemplate();
		$this->load->view('Itemtag/itemtagview',$data);
	}
	/**	*retrieve getcategorylevel	*/
	public function getcategorylevel()
	{
		$category_level = $this->Basefunctions->get_company_settings('category_level');
		echo json_encode(array('category_level'=>$category_level));
	}
	/**	*retrieve category - multi search	*/
	public function productsearchview()
	{
		$grid=$this->Basefunctions->getpagination();
		$result=$this->Itemtagmodel->productsearchview($grid['sidx'],$grid['sord'],$grid['start'],$grid['limit'],$grid['wh']);
		$pageinfo=$this->Basefunctions->page();
		header("Content-type: text/xml;charset=utf-8"); 
		$s = "<?xml version='1.0' encoding='utf-8'?>";
		$s .=  "<rows>";
		$s .= "<page>".$_GET['page']."</page>";
		$s .= "<total>".$pageinfo['totalpages']."</total>";
		$s .= "<records>".$pageinfo['count']."</records>";
		$category_level = $this->Basefunctions->get_company_settings('category_level'); //lot license
		foreach($result->result()as $row)
		{
			$s .= "<row id='". $row->productid."'>"; 
			$s .= "<cell><![CDATA[". $row->productname ."]]></cell>";			
			$s .= "<cell><![CDATA[". $row->shortname ."]]></cell>";
			for($mk =1;$mk <= $category_level;$mk++)
			{
				$categoryname = 'category_'.$mk;
				if($mk == $row->categorylevel){
				$s .= "<cell><![CDATA[".$row->categoryname."]]></cell>";
				}else{
					$s .= "<cell><![CDATA[ - ]]></cell>";
				}
			}
			$s .= "</row>";
		}                                  
		$s .= "</rows>";  
		echo $s;       
	}
	/**	 *retrieve category - multi search => Product Addon	 */
	public function productaddonsearchview()
	{
		$grid=$this->Basefunctions->getpagination();
		$result=$this->Itemtagmodel->productaddonsearchview($grid['sidx'],$grid['sord'],$grid['start'],$grid['limit'],$grid['wh']);
		$pageinfo=$this->Basefunctions->page();
		header("Content-type: text/xml;charset=utf-8");
		$s = "<?xml version='1.0' encoding='utf-8'?>";
		$s .=  "<rows>";
		$s .= "<page>".$_GET['page']."</page>";
		$s .= "<total>".$pageinfo['totalpages']."</total>";
		$s .= "<records>".$pageinfo['count']."</records>";
		$category_level = $this->Basefunctions->get_company_settings('category_level'); //lot license
		foreach($result->result()as $row)
		{
			$s .= "<row id='". $row->productid."'>";
			$s .= "<cell><![CDATA[". $row->productname ."]]></cell>";
			$s .= "<cell><![CDATA[". $row->shortname ."]]></cell>";
			for($mk =1;$mk <= $category_level;$mk++)
			{
				$categoryname = 'category_'.$mk;
				if($mk == $row->categorylevel){
					$s .= "<cell><![CDATA[".$row->categoryname."]]></cell>";
				}else{
					$s .= "<cell><![CDATA[ - ]]></cell>";
				}
			}
			$s .= "</row>";
		}
		$s .= "</rows>";
		echo $s;
	}
	/**	* validation class itemtag	*/
	function itemtag_validation_property()
	{
		$data = $this->Itemtagmodel->itemtag_validation_property(); 
		echo $data;
	}
	/**	*create tag	*/
	public function itemtagcreate() {  
    	$this->Itemtagmodel->itemtagcreate();
    }
	/**	*retrieve tag	*/
	public function itemtagretrive() 
	{  
    	$this->Itemtagmodel->itemtagretrive();
    }
	/**	*update itemtag	*/
	public function itemtagupdate()
	{
		$itemtagid = $_POST['primaryid'];
		$this->Itemtagmodel->itemtagupdate($itemtagid);
	}
	/**	*delete itemtagdelete	*/
	public function itemtagdelete()
	{	
		$this->Itemtagmodel->itemtagdelete();
	}
	/**	*	load lot -return JSON with pending weights	*/
	public function current_lot() { 
		$lotid = $this->input->post('lotid');
		$lot = array();
		$lot_license = $this->Basefunctions->get_company_settings('lotdetail');	//lot license
		if($lot_license == 'YES') {
			if($lotid != 0) {
				$lotid = $lotid;
				$data = $this->Itemtagmodel->lotdropdown_close($lotid); //@param empty to get all lot.
			} else {
				$data = $this->Itemtagmodel->lotdropdown(''); //@param empty to get all lot.
			}
			
			foreach($data->result() as $info) {
				$loosestoneinfo = $this->Basefunctions->singlefieldfetch('loosestone','productid','product',$info->productid);
				$lot[] = array (
						'lotid' => $info->lotid,
						'lotnumber' => $info->lotnumber,
						'lotpurity' => $info->purityid,
						'productid' => $info->productid,
						'accountid' => $info->accountid,
						'pendinggrossweight' => $info->pendinggrossweight,
						'pendingnetweight' => $info->pendingnetweight,
						'pendingcaratweight' => $info->pendingcaratweight,
						'pendingpieces' => $info->pendingpieces,
						'grossweight' => $info->totalgrossweight,
						'netweight' => $info->totalnetweight,
						'caratweight' => $info->totalcaratweight,
						'pieces' => $info->totalpieces,
						'purityname' => $info->purityname,
						'categoryid' => $info->categoryid,
						'categoryname' => $info->categoryname,
						'salesdetailid' => $info->salesdetailid,
						'billrefno' => $info->billrefno,
						'receiveorder' => $info->receiveorder,
						'stoneapplicable' => $info->stoneapplicable,
						'lottypeid' => $info->lottypeid,
						'loosestone' => $loosestoneinfo,
						'productcount'=> $this->puritycountbasedonproduct($info->purityid)
				);
			}
		}
		echo json_encode($lot);
	}
	//get stone count based on the itemtag
	function puritycountbasedonproduct($purityid) {
		$count = 0;
		$this->db->select('purityid');
		$this->db->from('product');
		$this->db->where("FIND_IN_SET('$purityid',product.purityid) >", 0);
		$this->db->where('product.productid >',10);
		$this->db->where('product.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			$count = 1;
		} else {
			$count = 0;
		}
		return $count;
	}
	/*	*load the pending current weights of the lot */
	public function retrivecurrentlotpending()
	{
		$lotid = $_GET['lotid'];
		$data = $this->Itemtagmodel->lotdropdown($lotid); //@param lotid
		if($data->num_rows() > 0)
		{
			foreach($data->result() as $info)
			{
				$lot = array (								
									'totalgrossweight' => $info->totalgrossweight,
									'totalnetweight' => $info->totalnetweight,
									'totalcaratweight' => $info->totalcaratweight,
									'totalpieces' => $info->totalpieces,
									'completedgrossweight' => $info->completedgrossweight,
									'completednetweight' => $info->completednetweight,
									'completedcaratweight' => $info->completedcaratweight,
									'completedpieces' => $info->completedpieces,
									'pendinggrossweight' => $info->pendinggrossweight,
									'pendingnetweight' => $info->pendingnetweight,
									'pendingcaratweight' => $info->pendingcaratweight,
									'pendingstoneweight' => $info->pendingstoneweight,
									'pendingpieces' => $info->pendingpieces
								);
			}
		}
		else
		{
			$lot = array (								
							'totalgrossweight' => 0,
							'totalnetweight' =>0,
							'totalcaratweight' =>0,
							'totalpieces' => 0,
							'completedgrossweight' => 0,
							'completednetweight' => 0,
							'completedcaratweight' => 0,
							'completedpieces' => 0,
							'pendinggrossweight' => 0,
							'pendingnetweight' => 0,
							'pendingcaratweight' => 0,
							'pendingstoneweight' => 0,
							'pendingpieces' => 0
						);
		}
		echo json_encode($lot);
	}
	/**	*loadproductaddon - for tag	*/
	public function loadproductaddon()
	{
		$product = $_GET['productid'];
		$purity = $_GET['purityid'];
		$weight = $_GET['weight'];
		$resultin = $this->Itemtagmodel->loadproductaddon($product,$purity,$weight);
		if($resultin->num_rows() > 0)
		{			
			foreach($resultin->result() as $info)
			{
				$charge_array[] = array(
										'additionalchargeid'=>$info->additionalchargeid,
										'additionalchargeidname'=>$info->additionalchargename,
										'calculationbyid'=>$info->calculationbyid,
										'calculationbyidname'=>$info->calculationbyname,
										'additionalchargevalue'=>$info->value,
										'tagadditionalchargeid'=>$info->additionalchargeid							
									);
			}					
		}
		else
		{
			$charge_array = array();
		}
		echo json_encode($charge_array);
	}
	/*	*itemtag reprinting	*/
	public function itemtagreprint()
	{
		$itemtagid = trim($_GET['primaryid']);		
		$tagtemplateid = $this->Basefunctions->singlefieldfetch('tagtemplateid','itemtagid','itemtag',$itemtagid);
		$tagtypeid = $this->Basefunctions->singlefieldfetch('tagtypeid','itemtagid','itemtag',$itemtagid);
		//printing
		$taggentagtype = array('2','4','5','6');
		if (in_array($tagtypeid,$taggentagtype ))
		{	
			if($tagtemplateid > 1)
			{
				$print_data=array('templateid'=> $tagtemplateid, 'Templatetype'=>'Tagtemp','primaryset'=>'itemtag.itemtagid','primaryid'=>$itemtagid);
				$data = $this->Printtemplatesmodel->generatlabelprint($print_data);
				echo $data;
			}
			else 
			{
				echo 'Kindly Edit the Print template of the tag in edit Screen.Since u have selected any print option earlier.';
				exit();
			}
		}
		else 
		{
			echo 'Cannot Print This. Its not a tag item.';
			exit();
		}
	}
	/**	* size auto suggestion methods	*/
	public function sizeautosuggest()
	{
		$size = trim($_GET['size']);
		$data = $this->Itemtagmodel->sizeautosuggest($size);
		if($data->num_rows() > 0)
		{			
			foreach($data->result() as $info)
			{
				echo '<div class="display_box" align="left" tabindex="148">';
				echo $info->size;
				echo '<br/>';
				echo '</div>';
			}			
		}
		else
		{
		}
	}
	/*	*itemtag ordering	*/
	public function orderitem()
	{
		$this->Itemtagmodel->orderitem();
	}	
	// item tag retrieve
	public function retrievetagdata(){
		$this->Itemtagmodel->retrievetagdata();
	}
	public function itemtagtransfercreate(){
		$this->Itemtagmodel->itemtagtransfercreate();
	}
	public function itemtagget(){
		$this->Itemtagmodel->itemtagget();
	}
	//non tag transfer submit button 
	public function nontagtransfercreate(){
		$this->Itemtagmodel->nontagtransfercreate();
	}
	public function autoitemsize(){
		$this->Itemtagmodel->autoitemsize();
	}
	public function checklotstatus(){
		$this->Itemtagmodel->checklotstatus();
	}
	public function tagtestprint()
	{
		$tagtemplateid=$_POST['tagtemplateid'];
		$data='';
		echo $data;
	}
	public function getratefromstonename()
	{
		$this->Itemtagmodel->getratefromstonename();
	}
	public function getstonedetails()
	{
		$this->Itemtagmodel->getstonedetails();
	}
	public function loadsizedata()
	{
		$this->Itemtagmodel->loadsizedata();
	}
	public function retrive_stoneentry_div()
	{
		$this->Itemtagmodel->retrive_stoneentry_div();
	}
	public function rfidretrive()
	{
		$result=$this->Itemtagmodel->insert_rfiddata();
		if($result == 'success'){
			$code =200;
		}else{
			$code =400;
		}
		$data = '{"code":'.$code.'}';
		echo $data;
	}
	public function rfiddevicestore()
	{
		$result=$this->Itemtagmodel->rfiddevicestore();
		if($result == 'success'){
			$code =200;
		}else{
			$code =400;
		}
		$data = '{"code":'.$code.'}';
		echo $data;
	}
	public function getrfidtagnumber()
	{
		$this->Itemtagmodel->getrfidtagnumber();
	}
	public function checkrfiddevice()
	{
		$this->Itemtagmodel->checkrfiddevice();
	}
	public function getfromstorage()
	{
		$this->Itemtagmodel->getfromstorage();
	}
	public function getpurity()
	{
		$this->Itemtagmodel->getpurity();
	}
	public function loadproductfromstock()
	{
		$this->Itemtagmodel->loadproductfromstock();
	}
	public function getweight()
	{
		$this->Itemtagmodel->getweight();
	}
	public function getpurityforuntag()
	{
		$this->Itemtagmodel->getpurityforuntag();
	}
	//unique name restriction
	public function uniquedynamicviewnamecheck(){
		$this->Itemtagmodel->uniquedynamicviewnamecheckmodel();
	}
	// load product charge
	public function loadproductcharge(){
		$this->Itemtagmodel->loadproductcharge();
	}
	// chech max counter quantity in stockentry 
	public function checkmaxquantity(){
		$this->Itemtagmodel->checkmaxquantity();
	}
	//get last itemtag number fetch
	public function getlastitemtagno() {
		$action = $_POST['action'];
		if($action != 'add') {
			$primaryid = $action -1;
		} else {
			$primaryid = $this->Itemtagmodel->maximumid('itemtag','itemtagid','tagtypeid','3');
		}
		if($primaryid !='' || $primaryid > 1) {
			$itemtagnumber = $this->Itemtagmodel->modulelinkfetch('itemtag','itemtagnumber','itemtagid',$primaryid,'tagtypeid','3');
			$rfidnumber = $this->Itemtagmodel->modulelinkfetch('itemtag','rfidtagno','itemtagid',$primaryid,'tagtypeid','3');
			$tagno['itemtagno'] =$itemtagnumber;
			$tagno['rfidno'] =$rfidnumber;
		} else {
			$tagno['itemtagno'] =0;
			$tagno['rfidno'] =0;
		}
		echo json_encode($tagno);
	}
	// check order tag 
	public function checkordertag(){
		$this->Itemtagmodel->checkordertag();
	}
	// Remove image from folder
	public function deleteimagefromfolder(){
		$this->Itemtagmodel->deleteimagefromfolder();
	}
	// Missing items add form
	public function missingitemaddform(){
		$this->Itemtagmodel->missingitemaddform();
	}
	// Missing items available status
	public function checkitemtagstatus(){
		$this->Itemtagmodel->checkitemtagstatus();
	}
	// Retrieve tag image of double click from main view grid.
	public function retrievetagimage(){
		$this->Itemtagmodel->retrievetagimage();
	}
	// Retrieve return item details from Sales & salesdetail table.
	public function returnitemsgridheaderinformationfetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$account_id=$_GET['filter'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'salesdetail.salesdetailid') : 'salesdetail.salesdetailid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('salesdate','salesnumber','accountname','stocktypename','productname','purityname','grossweight','stoneweight','netweight','pieces'),'colmodelindex'=>array('sales.salesdate','sales.salesnumber','account.accountname','stocktype.stocktypename','product.productname','purity.purityname','salesdetail.grossweight','salesdetail.stoneweight','salesdetail.netweight','salesdetail.pieces'),'coltablename'=>array('sales','sales','account','stocktype','product','purity','salesdetail','salesdetail','salesdetail','salesdetail'),'uitype'=>array('2','2','2','2','2','2','2','2','2','2'),'colname'=>array('Date','Sales Number','Account Name','Type','Product Name','Purity','Gross WT','Stone WT','Net WT','Pieces'),'colsize'=>array('150','200','200','200','200','200','200','200','200','200'));
		$result=$this->Itemtagmodel->returnitemsgriddynamicdatainfofetch($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$primaryid);
		$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Orderitem',$width,$height,'true');
		echo json_encode($datas);
	}
	// Retrieve Item detail to form set
	public function retrievegriddetailstofrom() {
		$this->Itemtagmodel->retrievegriddetailstofrom();
	}
	// Retrieve Live Stone details
	public function autostonegridheaderinformationfetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$salesdetailid = $_GET['salesdetailid'];
		$gridstoneids = $_GET['gridstoneids'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$account_id=$_GET['filter'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'salesstoneentry.salesstoneentryid') : 'salesstoneentry.salesstoneentryid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('stoneid','stonename','stonetypeid','stonetypename','stoneentrycalctypeid','stoneentrycalctypename','purchaserate','basicrate','stonepieces','stonegram','caratweight','totalweight','stonetotalamount'),'colmodelindex'=>array('stone.stoneid','stone.stonename','stonetype.stonetypeid','stonetype.stonetypename','stoneentrycalctype.stoneentrycalctypeid','stoneentrycalctype.stoneentrycalctypename','salesstoneentry.purchaserate','salesstoneentry.basicrate','salesstoneentry.stonepieces','salesstoneentry.stonegram','salesstoneentry.caratweight','salesstoneentry.totalweight','salesstoneentry.stonetotalamount'),'coltablename'=>array('stone','stone','stonetype','stonetype','stoneentrycalctype','stoneentrycalctype','salesstoneentry','salesstoneentry','salesstoneentry','salesstoneentry','salesstoneentry','salesstoneentry','salesstoneentry'),'uitype'=>array('2','2','2','2','2','2','2','2','2','2','2','2','2'),'colname'=>array('Stone Id','Stone Name','Stone Typeid','Stone Type Name','Stone Calc. ID','Stone Calc. Type','Purchase Rate','Sales Rate','Pieces','Gram Weight','Carat Weight','Total Net Weight','Total Amount'),'colsize'=>array('200','200','200','200','200','200','200','200','200','200','200','200','200'));
		$result=$this->Itemtagmodel->autostonegriddynamicdatainfofetch($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$primaryid,$salesdetailid,$gridstoneids);
		$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Autostoneitem',$width,$height,'true');
		echo json_encode($datas);
	}
}