<!DOCTYPE html>
<html lang="en">
<head>
    <style>
		input[type=text]:disabled.green{color:#03BCA9;}
		input[type=text]:disabled.red{color: #d95c5c;}
		input[type=text].green{color:#03BCA9;}
		input[type=text].red{color: #d95c5c;}
	</style>
	<?php $this->load->view('Base/headerfiles'); ?>	
	<?php $this->load->view('Base/overlay'); ?>
	<?php $this->load->view('Base/basedeleteform'); ?>
	<?php 
		$industryid = $this->Basefunctions->industryid;
		if($industryid == 3) {		
	?>
	<link href="<?php echo base_url();?>css/jqgrid.min.css" media="screen" rel="stylesheet" type="text/css" />
	<?php 
	 }
	?>
</head>
<body class="accountclass">
	<?php
		$device = $this->Basefunctions->deviceinfo();
		$dataset['gridenable'] = "yes"; //no
		$dataset['griddisplayid'] = "itemtaggriddisplay";
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['spanattr'] = array();
		$dataset['gridtableid'] = "itemtaggrid";
		$dataset['griddivid'] = "itemtaggriddiv";
		$dataset['moduleid'] = $moduleids;
		$dataset['forminfo'] = array(array('id'=>'itemtagaddformdiv','class'=>'hidedisplay','formname'=>'itemtagform'),array('id'=>'stonedetailsform','class'=>'hidedisplay','formname'=>'stonedetailsform'));
		if($device=='phone') {
			$this->load->view('Base/gridmenuheadermobile',$dataset);
			$this->load->view('Base/overlaymobile');
			$this->load->view('Base/viewselectionoverlay');
		} else {
			$this->load->view('Base/gridmenuheader',$dataset);
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/basedeleteform');
		$this->load->view('Base/modulelist');
	?>

<!----Order Overlay----->
<div class="large-12 small-12 medium-12 columns" style="position:absolute;">
	<div class="overlay" id="tagorderoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="row sectiontoppadding">&nbsp;</div>
		<div class="large-4 medium-4 small-12 large-centered medium-centered small-centered columns ">
			<div class="large-12 column paddingzero">
				<div class="row">&nbsp;</div>
				<form id="orderoverlayform" name="orderoverlayform" class="cleardataform">
					<div id="ordervalidate" class="validationEngineContainer">
						<div class="large-12 columns end paddingbtm ">
							<div class="large-12 columns cleardataform z-depth-5 overlayborder" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff">
								<div class="large-12 columns sectionheaderformcaptionstyle">Order Item</div>
								<div class="large-12 large-offset-right-2 columns sectionpanel" style="padding-bottom:0px;">						
									<div class="large-12 columns">						
										<div class="static-field overlayfield">
											<label>Order Status<span class="mandatoryfildclass" id="delivered_req"></span></label>	
											<select id="orderstatus" name="orderstatus" class="chzn-select ffieldd" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="1001">
												<option value="Yes" selected="selected">Yes</option>
												<option value="No">No</option>
											</select>
										</div>
									</div>	
								</div>									
								<div class="large-12 large-offset-right-2 columns sectionpanel">
									<div class="large-12 columns">
										<div class="static-field overlayfield">
											<label>Order Number<span class="mandatoryfildclass" id="delivered_req"></span></label>	
											<select id="ordernumber" name="ordernumber" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex=	"1002">
												<option value=""></option>
											</select>
										</div>
									</div>
									<input type="hidden" id="ordersalesnumber" name="ordersalesnumber"/>
									<input type="hidden" id="ordersalesdetailid" name="ordersalesdetailid"/>
								
									<div class="large-12 large-offset-right-2 columns sectionalertbuttonarea" style="text-align:right">
										<input type="button" id="tagordersubmit" name="" tabindex="1003" value="Submit" class="alertbtn" >
										<input type="button" id="tagorderclose" name="" value="Cancel" tabindex="1004" class="flloop alertbtn alertsoverlaybtn" >
										<span class="lasttab" tabindex="1005"></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>	
			</div>
		</div>		
	</div>
</div>
<!----item tag transfer overlay----->
<div class="large-12 small-12 medium-12 columns" style="position:absolute;">
	<div class="overlay" id="tagtransferoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="row sectiontoppadding">&nbsp;</div>
		<div class="large-4 medium-6 small-12 large-centered medium-centered small-centered columns ">
			<div class="large-12 column paddingzero">
				<div class="row">&nbsp;</div>
				<form id="tagtransferform" name="tagtransferform" class="cleardataform">
					<div id="tagtransfervalidate" class="validationEngineContainer">
						<div class="large-12 columns end paddingbtm ">
							<div class="large-12 columns cleardataform z-depth-5 border-modal-8px" style="background: #ffffff;border-radius: 8px !important;">
							<div class="large-12 columns sectionheaderformcaptionstyle">Tag Transfer Details</div>
							<div class="large-12 columns sectionpanel" style="padding: 15px 15px;">
								<div class="static-field  overlayfield">
									<label>Date<span class="mandatoryfildclass">*</span></label>
									<input id="transferdate" class="fordatepicicon validate[required]"  type="text" data-dateformater="dd-mm-yy" data-prompt-position="bottomLeft" readonly="readonly" tabindex="101" name="transferdate" >
								</div>
								<div class="tagtransferclear">
									<div class="input-field overlayfield">
										<input type="text" id="tagnumber" name="tagnumber" value="" class="validate[required]" tabindex="103"/>
										<label for="tagnumber">Tag Number</label>
									</div>
									<div class="static-field overlayfield tocounterhide">
										<label>To Storage</label>
										<select id="tocounter" name="tocounter" class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="104">
										  <option value=""></option>
											<?php $prev = ' ';
											foreach($defaultcounterwithjewel->result() as $key):
											?>
											<option value="<?php echo $key->counterid;?>" data-counterdefault="<?php echo $key->counterdefault;?>" data-counterdefaultid="<?php echo $key->counterdefaultid;?>"> <?php echo $key->counterid.' - '.$key->countername;?></option>
											<?php endforeach;?>		
										</select>
									</div>
									<div class="static-field overlayfield tagtranprocessproductdiv" style="display:none;">
										<label>Process Product</label>
										<select id="tagtransprocessproductid" name="tagtransprocessproductid" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="104">
											<option value=""></option>
											<?php $prev = ' ';
											foreach($transferprocessproduct->result() as $key):
											?>
											<option value="<?php echo $key->productid;?>" data-tagtypeid="<?php echo $key->tagtypeid;?>" data-categoryid="<?php echo $key->categoryid;?>" data-counterid="<?php echo $key->counterid;?>" data-purityid="<?php echo $key->purityid;?>"> <?php if($mainlicense['productidshow'] == 'YES') { echo $key->productid.' - '.$key->productname; } else { echo $key->productname;} ?></option>
											<?php endforeach;?>	
										</select>
									</div>
									<div class="static-field overlayfield oldtagdatadiv hidedisplay">
										<label>Old Item Details<span class="mandatoryfildclass" id="">*</span></label>
										<select id="tagolditemdetails" name="tagolditemdetails" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="105">
											<option value=""></option>
										</select>
									</div>
								</div>
								<div class="divider large-12 columns"></div>
								<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">
									<input type="hidden" name="itemtagid" id="itemtagid">
									<input type="hidden" name="tagtypetransferid" id="tagtypetransferid">
									<input type="hidden" name="purity" id="purity">
									<input type="hidden" name="product" id="product">
									<input type="hidden" name="grossweighttransfer" id="grossweighttransfer">
									<input type="hidden" name="stoneweighttransfer" id="stoneweighttransfer">
									<input type="hidden" name="netweighttransfer" id="netweighttransfer">
									<input type="hidden" name="transferpieces" id="transferpieces">
									<input type="hidden" name="transfermelting" id="transfermelting">
									<input type="hidden" name="transferfromcounterid" id="transferfromcounterid">
									<input type="button" id="addtranfertagbtn" name="" tabindex="106" value="Save" class="alertbtnyes" >
									<input type="button" id="closetagtransferoverlay" name="" value="Close" tabindex="107" class="flloop alertbtnno alertsoverlaybtn" >
								</div>
							</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>		
	</div>
</div>
<!----Non item tag transfer overlay----->
<div class="large-12 small-12 medium-12 columns" style="position:absolute;">
	<div class="overlay" id="nontagtransferoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="row sectiontoppadding">&nbsp;</div>
		<div class="large-4 medium-6 small-12 large-centered medium-centered small-centered columns">
			<div class="large-12 column paddingzero">
				<div class="row">&nbsp;</div>
				<form id="nontagtransferform" name="nontagtransferform" class="cleardataform">
				<div id="nontagtransfervalidate" class="validationEngineContainer">
				<div class="large-12 columns end paddingbtm nontagtransfer">
					<div class="large-12 columns cleardataform z-depth-5 border-modal-8px" style="background: #ffffff;border-radius: 8px !important;">
						<div class="large-12 columns sectionheaderformcaptionstyle">Non Tag Transfer Details</div>
						<div class="large-12 columns sectionpanel" style="padding: 15px 15px;">
							<div class="static-field large-6 columns hidedisplay" style="padding-bottom: 5px;">
								<label>Date<span class="mandatoryfildclass">*</span></label>								
								<input id="nontagtransferdate" class="fordatepicicon validate[required]"  type="text" data-dateformater="dd-mm-yy" data-prompt-position="bottomLeft" readonly="readonly" tabindex="101" name="nontagtransferdate" >
							</div>
							<div class="static-field large-6 columns hidedisplay">
								<label>Tag Entry Type</label>
								<select id="tagentrytype" name="tagentrytype" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="102" data-placeholder="select" >
								<option value="">Select</option>
								<?php foreach($nontagentrytype as $key):?>
								<option value="<?php echo $key->tagentrytypeid;?>" data-name="<?php echo $key->tagentrytypename;?>">
									<?php echo $key->tagentrytypename;?></option>
								<?php endforeach;?>	 
								</select>
							</div>
							<div class="untagtransferclearone" >
								<!-- <div class="static-field large-6 columns">
									<label>Transfer To</label>
									<select id="nontagtransferto" name="nontagtransferto"  class="chzn-select" data-validation-engine="validate[required]" tabindex="103" data-prompt-position="bottomLeft:14,36" data-placeholder="select" >
									<option value="">Select</option>
									<?php //foreach($tagentrytype_nontag as $key):?>
									<option value="<?php //echo $key->tagtypeid;?>" label="<?php //echo $key->tagtypename;?>">
										<?php //echo $key->tagtypename;?></option>
									<?php //endforeach;?>	 
									</select>
								</div>-->
								<div class="untagtransferclear" >
									<div class="static-field large-12 columns nontagfromcounterhide">
									<label>From Storage <span class="mandatoryfildclass" id="fromcounterid_req">*</span></label>				
									<select id="nontagfromcounterid" name="nontagfromcounterid" class="chzn-select"  data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="111">
										<option value=""></option>
										<?php $prev = ' ';
										foreach($fromnotdefaultcounter->result() as $key):
										?>
										<option value="<?php echo $key->counterid;?>"> <?php echo $key->counterid.' - '.$key->countername;?></option>
										<?php endforeach;?>
									</select>
									</div>
									<div class="static-field large-6 columns">
											<label>Purity<span class="mandatoryfildclass">*</span></label>
											<select id="nontagpurity" name="nontagpurity" class="chzn-select" data-validation-engine="validate[required]"  data-prompt-position="bottomLeft:14,36" tabindex="105">
											<option value=""></option>
											<?php $prev = ' ';
											foreach($purity->result() as $key):
											$cur = $key->metalid;
											$a="";
											if($prev != $cur )
											{
												if($prev != " ")
												{
													 echo '</optgroup>';
												}
												echo '<optgroup  label="'.$key->metalname.'">';
												$prev = $key->metalid;
											}
											?>
											<option data-melting="<?php echo $key->melting;?>" value="<?php echo $key->purityid;?>"><?php echo $key->purityname;?></option>
											<?php endforeach;?>
										</select>
										</div>
									<div class="static-field large-6 columns " >
										<label>Product Name<span class="mandatoryfildclass" id="nontagproductid_req">*</span></label>								
										<select id="nontagproductid" name="nontagproductid" class="chzn-select"  data-validation-engine="validate[required]"  data-prompt-position="bottomLeft:14,36" tabindex="104">
											<option value=""></option>
											<?php $prev = ' ';
											foreach($nontagproduct->result() as $key):
											?>
											<option value="<?php echo $key->productid;?>" data-tagtypeid="<?php echo $key->tagtypeid;?>" data-purityid="<?php echo $key->purityid;?>"  data-counterid="<?php echo $key->counterid;?>"> <?php echo $key->productid.' - '.$key->productname;?></option>
											<?php endforeach;?>
										</select>
									</div>
									<div class="static-field large-12 columns nontagtocounterhide">
										<label>To Storage<span class="mandatoryfildclass">*</span></label>
										<select id="nontagtocounter" name="nontagtocounter" data-validation-engine="" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="112">
										<option value=""></option>
										<?php $prev = ' ';
										foreach($defaultcounterwithjewel->result() as $key):
										?>
										<option value="<?php echo $key->counterid;?>" data-counterdefault="<?php echo $key->counterdefault;?>" data-counterdefaultid="<?php echo $key->counterdefaultid;?>"> <?php echo $key->counterid.' - '.$key->countername;?></option>
										<?php endforeach;?>					
										</select>
									</div>
									<div class="static-field large-6 columns hidedisplay" id="nontagprocessproductid-div">
										<label>Process Product<span class="mandatoryfildclass" id="nontagproductid_req">*</span></label>
										<select id="nontagprocessproductid" name="nontagprocessproductid" class="chzn-select" style="" data-prompt-position="topLeft:14,36" tabindex="110">
											<option value="" data-purityid="0"></option>
											<?php $prev = ' ';
											foreach($transferprocessproduct->result() as $key):
											?>
											<option value="<?php echo $key->productid;?>" data-tagtypeid="<?php echo $key->tagtypeid;?>" data-categoryid="<?php echo $key->categoryid;?>" data-counterid="<?php echo $key->counterid;?>" data-purityid="<?php echo $key->purityid;?>"> <?php if($mainlicense['productidshow'] == 'YES') { echo $key->productid.' - '.$key->productname; } else { echo $key->productname;} ?></option>
											<?php endforeach;?>
										</select>
									</div>
									<div class="input-field large-6 columns ">
										<input type="text" class="nontagweightcalculate tagaddclear" id="nontaggrossweight" data-calc="GWT" name="nontaggrossweight" value="" tabindex="106" data-validation-engine="validate[required,custom[number],decval[<?php echo $weight_round?>],min[0.1]]">
										<label for="nontaggrossweight">Gross Weight<span class="mandatoryfildclass" >*</span></label>
									</div>
									<div class="input-field large-6 columns ">
										<input type="text" class="nontagweightcalculate tagaddclear" data-calc="NWT" data-validategreat="nontaggrossweight" id="nontagnetweight" name="nontagnetweight" value="" tabindex="108" data-validation-engine="validate[required,custom[number],decval[<?php echo $weight_round?>],min[0.1]]" readonly>
										<label for="nontagnetweight">Net Weight<span class="mandatoryfildclass">*</span></label>
									</div>
									
									<div class="input-field large-6 columns">
										<input type="text" class="tagaddclear" id="nontagpieces" name="nontagpieces" value="" tabindex="109" data-validation-engine="validate[required,custom[integer],min[1]]">
										<label for="nontagpieces">Pieces<span class="mandatoryfildclass">*</span></label>
									</div>
									<div class="input-field large-6 columns hidedisplay">
																
										<input type="text" class="tagaddclear" id="uttmelting" name="uttmelting" value="" tabindex="110" data-validation-engine="">
										<label for="uttmelting">Melting<span class="mandatoryfildclass">*</span></label>
									</div>
									<div class="static-field large-6 columns oldnontagdatadiv hidedisplay">
										<label>Old Item Details<span class="mandatoryfildclass" id="">*</span></label>
									   <select id="nontagolditemdetails" name="nontagolditemdetails" data-validation-engine="" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="113">
										<option value=""></option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="divider large-12 columns"></div>
						<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">
						<input type="button" class="alertbtnyes leftformsbtn" id="addtranfernontagbtn" name="" value="Save" tabindex="114">
						<input type="button" class="alertbtnno addsectionclose" id="closenontagtransferoverlay" value="Close" name="cancel" tabindex="115">
						</div>
					</div>
					</div>
					</div>
				</form>
				
			</div>
		</div>		
	</div>
</div>
<!----Missing items overlay----->
<div class="large-12 small-12 medium-12 columns" style="position:absolute;">
	<div class="overlay" id="missingitemoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="row sectiontoppadding">&nbsp;</div>
		<div class="large-4 medium-6 small-12 large-centered medium-centered small-centered columns ">
			<div class="large-12 column paddingzero">
				<div class="row">&nbsp;</div>
				<form id="missingitemform" name="missingitemform" class="cleardataform">
				<div id="missingitemvalidate" class="validationEngineContainer">
				<div class="large-12 columns end paddingbtm ">
					<div class="large-12 columns cleardataform z-depth-5 border-modal-8px" style="background: #ffffff;border-radius: 6px !important;">
						<div class="large-12 columns sectionheaderformcaptionstyle">Missing Item Details</div>
						<div class="large-12 columns sectionpanel" style="padding: 15px 15px;">
							<div class="static-field large-6 columns" style="margin-top: 19px;">
								<label>Missing Date<span class="mandatoryfildclass">*</span></label>								
								<input id="missingitemdate" class="fordatepicicon validate[required]" type="text" data-dateformater="dd-mm-yy" data-prompt-position="topLeft" readonly="readonly" tabindex="101" name="missingitemdate">
							</div>
							<div class="static-field large-6 columns">
								<label>Status</label>			
								<select id="missingitemstatus" name="missingitemstatus" class="chzn-select" data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="124">
									<option value="26" data-missingitemstatus="Missing/Theft">Missing/Theft</option>
								</select>
							</div>
							<!--<div class="tagtransferclear">-->
							<div class="input-field large-6 columns" id=""> 
								<input type="text" class="missingtagnumber" id="missingtagnumber" name="missingtagnumber" value="" tabindex="116" data-validation-engine="" maxlength="10">
								<label for="missingtagnumber">Tag Number<span class="mandatoryfildclass" id="">*</span></label>
							</div>
							<div class="input-field large-6 columns" id=""> 
								<input type="text" class="missingrfidno" id="missingrfidno" name="missingrfidno" value="" tabindex="116" data-validation-engine="" maxlength="10">
								<label for="missingrfidno">RFID Number<span class="mandatoryfildclass" id="">*</span></label>
							</div>
							<div class="input-field large-12 columns">		
								<textarea name="missingdescription" class="materialize-textarea" id="missingdescription" rows="3" cols="40" tabindex="125" data-validation-engine="validate[required,maxSize[200]]"></textarea>
								<label for="missingdescription">Status/Description</label>
							</div>
							<!--</div>-->
							<div class="divider large-12 columns"></div>
							<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">
								<input type="button" class="alertbtnyes leftformsbtn" id="addmissingitemform" name="" value="Save" tabindex="114">
								<input type="button" class="alertbtnno addsectionclose" id="closemissingitemoverlay" value="Close" name="cancel" tabindex="115">
							</div>
						</div>
					</div>
				</div>
				</div>
				</form>
			</div>
		</div>		
	</div>
</div>
<!-- Image Preview in Main view grid. -->
<div class="large-12 small-12 medium-12 columns" style="position:absolute;">
	<div class="overlay" id="tagimagepreview" style="overflow-y: scroll;overflow-x: hidden;z-index:40">
		<div class="large-12 columns end paddingbtm ">
				<div class="large-6 columns cleardataform z-depth-5 border-modal-8px" style="background: #ffffff;border-radius: 6px !important;">
					<div class="large-12 columns sectionheaderformcaptionstyle">Tag Image Preview</div>
					<div class="large-12 columns">
						<div id="itemtagimagepreview" class="large-12 columns uploadcontainerstyle" style="height:27rem; padding:0px; border:1px solid #CCCCCC"></div>
						<div class="row">&nbsp;</div>
						<div class="row">&nbsp;</div>
						<div class="large-12" style="">
						<div class="tagimagepreviewcls">
							<input type="button" value="Close" id="tagimagepreviewclose" class="btn leftformsbtn" tabindex="124">
						</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>

<!-- Return Items Overlay grid ---->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="returnitemsdetailoverlay" style="overflow-y: auto;overflow-x: hidden;">		
		<div class="large-10 medium-10 small-10 large-centered medium-centered small-centered columns borderstyle" style="top:5% !important;">
			<form method="POST" name="returnitemsdetailform" style="" id="returnitemsdetailform" action=""  class="clearformbordergridoverlay">
				<span id="returnitemsdetailformvalidation" class="validationEngineContainer">
					<div class="row" style="background:#fff">&nbsp;</div>
					<!-- <div class="row" style="background:#fff">&nbsp;</div> -->
					<div class="row" style="background:#fff">
						<div class="large-12 columns sectionheaderformcaptionstyle" style="top: -10px;">
							Return Items Detail
						</div>
						<?php
							echo '<div class="large-12 columns forgetinggridname" id="returnitemsdetailgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="returnitemsdetailgrid" style="max-width:2000px; height:250px;top:0px;top: -40px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
							<!--<div class="inner-gridfooter footer-content footercontainer" id="returnitemsdetailgridfooter">
									 Footer & Pagination content 
								</div>--></div>';
						?>
					</div>
					<div class="row" style="background:#fff">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right;top:15px;">
							<input type="button" id="returnitemsgridsubmit" name="" value="Submit" class="alertbtnyes" tabindex="1001">	
							<input type="button" id="returnitemsgridclose" name="" value="Close" tabindex="1002" class="alertbtnno  alertsoverlaybtn" >
						</div>
					</div>
					<div class="row" style="background:#fff">&nbsp;</div>
				</span>
				<input type="hidden" name="returnsalesdetailid" id="returnsalesdetailid" />
			</form>
		</div>
	</div>
</div>

<!---- Automatic Lot - Stone Overlay Grid with Details ----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="automaticstonedetailoverlay" style="overflow-y: auto;overflow-x: hidden;">		
		<div class="large-10 medium-10 small-10 large-centered medium-centered small-centered columns borderstyle" style="top:5% !important;">
			<form method="POST" name="autostonedetailform" style="" id="autostonedetailform" action=""  class="clearformbordergridoverlay">
				<span id="autostonedetailformvalidation" class="validationEngineContainer">
					<div class="row" style="background:#fff">&nbsp;</div>
					<!-- <div class="row" style="background:#fff">&nbsp;</div> -->
					<div class="row" style="background:#fff">
						<div class="large-12 columns sectionheaderformcaptionstyle" style="top: -10px;">
							Stone Detail
						</div>
						<?php
							echo '<div class="large-12 columns forgetinggridname" id="autostonegridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="autostonegrid" style="max-width:2000px; height:250px;top:0px;top: -40px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
							<!--<div class="inner-gridfooter footer-content footercontainer" id="autostonegridfooter">
									 Footer & Pagination content 
								</div>--></div>';
						?>
					</div>
					<div class="row" style="background:#fff">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right;top:15px;">
							<input type="button" id="autostonegridsubmit" name="" value="Submit" class="alertbtnyes" tabindex="1001">	
							<input type="button" id="autostonegridclose" name="" value="Close" tabindex="1002" class="alertbtnno  alertsoverlaybtn" >
						</div>
					</div>
					<div class="row" style="background:#fff">&nbsp;</div>
				</span>
				<input type="hidden" name="" id="" />
			</form>
		</div>
	</div>
</div>

<!--RFID TAG alert overlay-->
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay alertsoverlay overlayalerts" id="rfidalerts">	
			<div class="row">&nbsp;</div>
			<div class="row sectiontoppadding"></div>
			<div class="overlaybackground">
				<div class="alert-panel">
					<div class="alertmessagearea">
					<div class="row">&nbsp;</div>
					<div class="alert-message rfidalertinputstyle">Please select a row</div>
					</div>
					<div class="alertbuttonarea">
						<span class="firsttab" tabindex="1000"></span>
						<input type="button" id="rfidalertsclose" name="" value="Ok" class="alertbtnyes  alertsoverlaybtn" >
						<span class="lasttab" tabindex="1003"></span>
					</div>
					<div class="row">&nbsp;</div>
					<div class="row">&nbsp;</div>
				</div>
			</div>
		</div>
	</div>
</body>

<?php $this->load->view('Base/bottomscript'); ?>
<?php include 'js/Itemtag/itemtag.php'  ?>
<script src="<?php echo base_url();?>js/plugins/autocomplete/jquery-ui.js"></script>
<script src="<?php echo base_url();?>js/plugins/jqgrid/jquery.jqGrid.src.min.js"></script>
<script src="<?php echo base_url();?>js/plugins/filecomputer/fileupload.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/plugins/say-cheese.js"></script>
</html>