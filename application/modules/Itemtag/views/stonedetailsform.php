<?php
				$device = $this->Basefunctions->deviceinfo();
				$dataset['gridtitle'] = 'Stone Entry';
				$dataset['titleicon'] = 'material-icons group_work';
				$modtabgroup[0] = array("tabpgrpid"=>"1","tabgrpname"=>"Stone Entry","moduleid"=>"stone","templateid"=>"2");
				$dataset['modtabgrp'] = $modtabgroup;
				$this->load->view('Base/mainformwithgridmenuheader.php',$dataset);
				echo hidden('rate_round',$rate_round);
?>
<div class="large-12 columns paddingzero">
	<div class="" id="stoneoverlay">		
		<div class="large-12 columns addformunderheader">&nbsp;</div>
		<div class="large-12 columns scrollbarclass addformcontainer">
		<form id="" name="" class="" style="background-color:#f2f3fa !important;">		
		<div class="large-12 medium-12 small-12 large-centered medium-centered small-centered columns">
			<div class="large-12 column paddingzero">
				<?php if($device=='phone') {?>
				<div class="closed effectbox-overlay effectbox-default" id="stoneentrysectionoverlay">
				<?php }?> 
				<div class="row mblhidedisplay">&nbsp;</div>
				<div class="large-4 columns end paddingbtm ">
					<div class="large-12 columns validationEngineContainer tagadditionalchargeclear cleardataform borderstyle" id="stoneentryaddvalidate" style="padding-left: 0 !important; padding-right: 0 !important;background: #fff;">
						<?php if($device=='phone') {?>
						<div class="large-12 columns sectionheaderformcaptionstyle">Stone Entry</div>
						<div class="large-12 columns sectionpanel">
							<?php  } else { ?>
								<div class="large-12 columns headerformcaptionstyle">Stone Entry</div>
						<?php  	} ?> 
						<div class="tagadditionalchargeclear_stone">
						<div class="static-field large-12 columns ">
							<label>Stone Name<span class="mandatoryfildclass">*</span></label>
							<select id="stoneid" name="stoneid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="176">
								<option value=""></option>
								<?php $prev = ' ';
									foreach($stonename->result() as $key){
									$cur = $key->stonetypeid;
									$a="";
									if($prev != $cur )
									{
										if($prev != " ")
										{
											 echo '</optgroup>';
										}
										echo '<optgroup  label="'.$key->stonetypename.'" id="'.$key->stonetypename.'">';
										$prev = $key->stonetypeid;
									}
									?>
								<option value="<?php echo $key->stoneid;?>" data-name="<?php echo $key->stonename;?>" data-stoneidgroupid="<?php echo $key->stonetypeid;?>" data-groupid="<?php echo $key->stonetypeid;?>" data-stoneidhidden="<?php echo $key->stonename;?>" data-stoneidgroupname="<?php echo $key->stonetypename;?>" data-groupname="<?php echo $key->stonetypename;?>"><?php echo $key->stonename;?></option>
								<?php } ?>
							</select>
						</div>
						<div class="static-field large-6 columns">
							<label>Stone Calculation Type<span class="mandatoryfildclass">*</span></label>								
							<select id="stoneentrycalctypeid" name="stoneentrycalctypeid" class="chzn-select " data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="102">
								<option value=""></option>
								<?php foreach($stoneentrycalctype as $key):?>
								<option value="<?php echo $key->stoneentrycalctypeid;?>" data-name="<?php echo $key->stoneentrycalctypename;?>" data-stonecalcidhidden="<?php echo $key->stoneentrycalctypename;?>">
								<?php echo $key->stoneentrycalctypename;?></option>
								<?php endforeach;?>	 
							</select>
						</div>
						<div class="input-field large-6 columns ">
							<input type="text" class="" id="purchaserate" name="purchaserate" value="" data-validation-engine="validate[required,custom[number],decval[<?php echo $rate_round;?>],maxSize[15],min[0]]" tabindex="177">
							<label for="purchaserate">Purchase Rate<span class="mandatoryfildclass">*</span></label>
						</div>
						<div class="input-field large-6 columns ">
							<input type="text" class="" id="basicrate" name="basicrate" value="" data-validation-engine="validate[required,custom[number],decval[<?php echo $rate_round;?>],maxSize[15],min[0]]" tabindex="177">
							<label for="basicrate">Sales Rate<span class="mandatoryfildclass">*</span></label>
						</div>
						<div class="input-field large-6 columns " id="stonegramdiv">
							<input type="text" class="" id="stonegram" name="stonegram" value="" data-validation-engine="" maxlength="15" tabindex="177">
							<label for="stonegram">Gram Weight</label>
						</div>
						<div class="input-field large-6 columns " id="stonepiecesdiv">
							<input type="text" class="" id="stonepieces" name="stonepieces" value="" data-validation-engine=""  maxlength="15" tabindex="178">
							<label for="stonepieces">Pieces</label>
						</div>
						<div class="input-field large-6 columns " id="caratweightdiv">
							<input type="text" class="" id="caratweight" name="caratweight" value="" data-validation-engine="" maxlength="15" tabindex="179">
							<label for="caratweight">Carat Weight</label>
						</div>
						<div class="input-field large-6 columns totacarat hidedisplay">
							
							<input type="text" class="" id="totalcaratweight" name="totalcaratweight" value="" data-validation-engine="" tabindex="">
							<label for="totalcaratweight">Total Carat Weight<span class="mandatoryfildclass">*</span></label>
						</div>
						<div class="input-field large-6 columns">
							<input type="text" class="" id="totalweight" name="totalweight" value="" data-validation-engine="" tabindex="">
							<label for="totalweight">Total Net Weight<span class="mandatoryfildclass">*</span></label>
						</div>
						<div class="input-field large-6 columns end">
							<input type="text" class="" id="stonetotalamount" name="stonetotalamount" value="" data-validation-engine="validate[required,custom[number]]" tabindex="">
							<label for="totalamount">Total Amount<span class="mandatoryfildclass">*</span></label>
						</div>
						<input type = "hidden" id="autolotpieces" name="autolotpieces" value="" />
						<input type = "hidden" id="autolotgramwt" name="autolotgramwt" value="" />
						<input type = "hidden" id="autolotcaratwt" name="autolotcaratwt" value="" />
					</div>
						<div class="input-field large-12 columns taghide hidedisplay">
								<input type="text" id="stonetagnumber" name="stonetagnumber" value="" class="validate[required]"readonly/>
								<label for="stonetagnumber">Tag Number</label>
						</div>
						<div class="large-12 columns taghide hidedisplay">
								 &nbsp;
						</div>
						<?php if($device=='phone') { ?>
							</div>
							<div class="divider large-12 columns"></div>
							<div class="large-12 columns submitkeyboard sectionalertbuttonarea" style="text-align: right">
								<input id="tagaddstone" class="alertbtn addkeyboard" type="button" value="Submit" name="tagaddstone" tabindex="180">
								<input id="tageditstone" class="alertbtn hidedisplay updatekeyboard" type="button" value="Submit" name="tageditstone" tabindex="180" style="display: none;">
								<input id="stoneentrysectionclose"class="alertbtn addsectionclose cancelkeyboard" type="button" value="Cancel" name="cancel" tabindex="181">
								<input type = "hidden" id="tagstoneentryid" name="tagstoneentryid"/>
							</div>
								
							<?php } else { ?> 	
						<div class="large-12 columns stonedetailsform" style="text-align: right">
							<label> </label>
							<input type = "hidden" id="tagstoneentryid" name="tagstoneentryid"/>
							<input type="button" class="btn leftformsbtn stoneaddon" id="tagaddstone" name="tagaddstone" data-id="ADD" value="SUBMIT" tabindex="180">
							<input type="button" class="btn leftformsbtn stoneaddon" style="display:none" id="tageditstone" data-id="UPDATE" name="tageditstone" value="SUBMIT" tabindex="181">
						</div>
						<?php } ?>
					</div>
				</div>
				<?php if($device=='phone') {?>
				</div>
				<?php }?>
				<div class="large-8 columns paddingbtm" style="top:10px;">
					<div class="large-12 columns paddingzero">
						<div class="large-12 columns viewgridcolorstyle paddingzero cleardataform borderstyle">
							<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0 0rem;">
							<span class="large-6 medium-6 small-6 columns text-left" >Stone Entry</span>
							<div class="large-6 medium-6 small-6 columns stoneiconlisthide" style="text-align:right;"> 
								<?php if($device=='phone') {?>
								  <span id="stoneentryaddicon" class="icon-box" title="Add" ><i class="material-icons">add</i></span>
								  <?php }?> 
								  <span id="tagstoneentryedit" class=" editiconclass" title="Edit"> <i class="material-icons">edit</i></span>
								  <span id="tagstoneentrydelete" class="deleteiconclass"  title="Delete"><i class="material-icons">delete</i></span>
							</div>
							</div>
							<?php
									$device = $this->Basefunctions->deviceinfo();
									if($device=='phone') {
										echo '<div class="large-12 columns paddingzero forgetinggridname" id="stoneentrygridwidth"><div class=" inner-row-content inner-gridcontent" id="stoneentrygrid" style="height:420px;top:0px;">
											<!-- Table header content , data rows & pagination for mobile-->
										</div>
										<footer class="inner-gridfooter footercontainer" id="stoneentrygridfooter">
											<!-- Footer & Pagination content -->
										</footer></div>';
									} else {
										echo '<div class="large-12 columns forgetinggridname" id="stoneentrygridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="stoneentrygrid" style="max-width:2000px; height:390px;top:0px;">
										<!-- Table content[Header & Record Rows] for desktop-->
										</div>
										<!--<div class="inner-gridfooter footer-content footercontainer" id="stoneentrygridfooter">
											 Footer & Pagination content
										</div> --></div>';
									}
								?>
						</div>
					</div>
				</div>
				<div style="top: 30px;position: relative;">
				<div class="large-12 columns" style="top:10px;left: 12px;padding-right: 37px;padding-bottom:20px;">
							<div class="large-12 columns viewgridcolorstyle cleardataform borderstyle" style="background: #fff none repeat scroll 0 0;padding-left: 0 !important;padding-right: 0 !important;">	
							<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0rem 0rem; height:auto; line-height:1.8rem;">	
							<div class="large-12  medium-12 small-12 columns small-only-text-center" style="">Stone Entry Summary</div>
							<div class="large-6 medium-6 small-12 columns innergridicons small-only-text-center" style="text-align:right">						
							</div>
							</div>
							<div class="large-12 columns" style="padding-left:0;padding-right:0; background-color:#fff">
							<div class="large-2 columns ">
							<label>Dia Pieces<span class=""> : </span> <span id="totdiapieces"></span></label>	
							</div>
							<div class="large-2 columns ">
							<label>Cs Pieces<span class=""> : </span> <span id="totcspieces"></span></label>	
							</div>	
							<div class="large-2 columns ">
							<label>Pearl Pieces<span class=""> : </span> <span id="totpearlpieces"></span></label>	
							</div>	
							<div class="large-2 columns ">
							<label>Pieces<span class=""> : </span> <span id="totpieces"></span></label>	
							</div>	
							<div class="large-2 columns ">
							<label>Dia Carat Wt<span class=""> : </span><span id="totdiacaratwt"></span></label>									
							</div>	
							<div class="large-2 columns ">
							<label>Cs Carat Wt<span class=""> : </span><span id="totcscaratwt"></span></label>									
							</div>	
							<div class="large-2 columns ">
							<label>Pearl Carat Wt<span class=""> : </span><span id="totpearlcaratwt"></span></label>									
							</div>
							<div class="large-2 columns ">
							<label>Carat Wt<span class=""> : </span><span id="caratwt"></span></label>									
							</div>		
							<div class="large-2 columns hidedisplay">
							<label>Dia Wt<span class=""> : </span><span id="totdiawt"></span></label>	
							</div>	
							<div class="large-2 columns hidedisplay">
							<label>CS Wt<span class=""> : </span><span id="totcswt"></span></label>	
							</div>
							<div class="large-2 columns hidedisplay">
							<label>Pearl Wt<span class=""> : </span><span id="totpearlwt"></span></label>	
							</div>
							<div class="large-2 columns hidedisplay">
							<label>Wt<span class=""> : </span><span id="totwt"></span></label>	
							</div>		
							<div class="large-2 columns">
							<label>Dia Amt<span class=""> : </span><span id="totdiaamt"></span></label>	
							</div>	
							<div class="large-2 columns ">
							<label>CS Amt<span class=""> : </span><span id="totcsamt"></span></label>	
							</div>	
							<div class="large-2 columns">
							<label>Pearl Amt<span class=""> : </span><span id="totpearlamt"></span></label>	
							</div>	
							<div class="large-2 columns">
							<label>Amt<span class=""> : </span><span id="totamt"></span></label>	
							</div>	
						</div>				
					</div>
				</div>	
				</div>
							
		</div>
		</form>
		</div>
	</div>
	<div class="large-12 columns" style="position:absolute;"><div class="overlay" id="viewdeleteoverlayconform" style="z-index:120;"></div></div>
</div>