<?php
	$loggedinuserroleid = $this->Basefunctions->userroleid;
	$device = $this->Basefunctions->deviceinfo();
	$dataset['gridtitle'] = $gridtitle;
	$dataset['titleicon'] = $titleicon;
	$this->load->view('Base/mainformwithgridmenuheader.php',$dataset);
	$defaultrecordview = $this->Basefunctions->get_company_settings('mainviewdefaultview');
	echo hidden('mainviewdefaultview',$defaultrecordview);
?>
	<input type ="hidden" id="validation_property" value='<?php echo json_encode($tag_fieldrule);?>'/>
	<input type ="hidden" id="operationmode" value="">
	<input type ="hidden" id="defaultprinttemplate" value="<?php echo $defaultprinttemplate;?>">
	<input type ="hidden" id="counteryesno" value="<?php echo strtolower($mainlicense['counter']);?>">
	<input type ="hidden" id="weight_round" value="<?php echo $weight_round;?>">
	<input type ="hidden" id="dia_weight_round" value="<?php echo $dia_weight_round;?>">
	<input type ="hidden" id="amount_round" value="<?php echo $amount_round;?>">
	<input type ="hidden" id="melting_round" value="<?php echo $melting_round;?>">
	<input type ="hidden" id="autoweight" value="<?php echo $mainlicense['autoweight']?>">
	<input type="hidden" id="counterval" name="counterval" value="<?php echo $counter_val; ?>">
	<input type="hidden" id="netwtcalid" name="netwtcalid" value="<?php echo $netwtcalctypeid; ?>">
	<?php if(strtolower($mainlicense['stone']) == 'yes' && $planstatus >= 5) {$stoneyesno=1; }else{ $stoneyesno=1;  } ?>
	<input type="hidden" id="hiddenstoneyesno" name="hiddenstoneyesno" value="<?php echo $stoneyesno; ?>">
	<input type="hidden" id="tagtype" name="tagtype" value="<?php echo $mainlicense['tagtype']?>">
	<input type="hidden" id="changelotid" name="changelotid" value="">
	<input type="hidden" id="changegrosswt" name="changegrosswt" value="">
	<input type="hidden" id="changenetwt" name="changenetwt" value="">
	<input type="hidden" id="gridcolnames" name="gridcolnames" value="stonegroupid,stonegroupname,stoneid,stonename,purchaserate,salesrate,stoneentrycalctypeid,stoneentrycalctypename,stonepieces,caratweight,stonegram,totalcaratweight,totalweight,stonetotalamount">
	<input type="hidden" id="gridcoluitype" name="gridcoluitype" value="31,32,2,33,2,2,2,34,2,2,2,2,2,2">
	<input type="hidden" id="stonestatus" name="stonestatus" value="<?php echo $mainlicense['stone']; ?>"/>
	<input type="hidden" id="rfidstatus" name="rfidstatus" value="<?php echo $mainlicense['rfidoption']; ?>"/>
	
	<input type="hidden" id="hiddentagtypeid" name="hiddentagtypeid">
	<input type="hidden" id="stocknetweight" name="stocknetweight">
	<input type="hidden" id="stockgrossweight" name="stockgrossweight">
	<input type="hidden" id="stockpieces" name="stockpieces">
	<input type="hidden" id="lotstatus" name="lotstatus" value="<?php echo $mainlicense['lot']; ?>">
	<input type="hidden" id="stocknotify" name="stocknotify" value="<?php echo $mainlicense['stocknotify']; ?>">
	<input type="hidden" id="vacchargeshow" name="vacchargeshow" value="<?php echo $mainlicense['vacchargeshow']; ?>">
	<input type="hidden" id="chargeaccountgroup" name="chargeaccountgroup" value="<?php echo $mainlicense['chargeaccountgroup']; ?>">
	<input type="hidden" id="chargecalculation" name="chargecalculation" value="<?php echo $mainlicense['chargecalculation']; ?>">
	<input type="hidden" id="lotpieces" name="lotpieces" value="<?php echo $mainlicense['lotpieces']; ?>">
	<input type="hidden" id="imagestatus" name="imagestatus" value="<?php echo $mainlicense['image']; ?>">
	<input type="hidden" id="storagequantity" name="storagequantity" value="<?php echo $mainlicense['storagequantity']; ?>">
	<input type="hidden" id="storagedisplay" name="storagedisplay" value="<?php echo $mainlicense['storagedisplay']; ?>">
	<input type="hidden" id="countermaxcount" name="countermaxcount">
	<input type="hidden" id="editcounterid" name="editcounterid">
	<input type="hidden" id="primarydataid" name="primarydataid">
	<input type="hidden" id="previouscharge" name="previouscharge">
	<input type="hidden" id="grossweighttrigger" name="grossweighttrigger" value="<?php echo $mainlicense['grossweighttrigger']; ?>">
	<input type="hidden" id="vendordisable" name="vendordisable" value="<?php echo $mainlicense['vendordisable']; ?>">
	<input type="hidden" id="lotweightlimit" name="lotweightlimit" value="<?php echo $mainlicense['lotweightlimit']; ?>">
	<input type="hidden" id="chargerequired" name="chargerequired" value="<?php echo $mainlicense['chargerequired']; ?>">
	<input type="hidden" id="weightscale" name="weightscale" value="<?php echo $mainlicense['weightscale']; ?>">
	<input type="hidden" id="rate_round" name="rate_round" value="<?php echo $rate_round; ?>">
	<input type="hidden" id="pendinglotstonewt" name="pendinglotstonewt" value="">
	<input type="hidden" id="prizetaggst" name="prizetaggst" value="<?php echo $mainlicense['prizetaggst']; ?>">
	<input type="hidden" id="untaguniqueno" name="untaguniqueno" value="<?php echo $mainlicense['untaguniqueno']; ?>">
	<input type="hidden" id="stonewtcheck" name="stonewtcheck" value="<?php echo $stonewtcheck; ?>">
	<input type="hidden" id="tagautoimage" name="tagautoimage" value="<?php echo $tagautoimage; ?>">
	<input type="hidden" id="itemtagauthrole" name="itemtagauthrole" value="<?php echo $itemtagauthrole; ?>">
	<input type="hidden" id="chargeapply" name="chargeapply" value="<?php echo $chargeapply; ?>">
	<input type="hidden" id="rfidnoofdigits" name="rfidnoofdigits" value="<?php echo $rfidnoofdigits; ?>">
	<input type="hidden" id="userroleid" name="userroleid" value="<?php echo $loggedinuserroleid; ?>">
	<input type="hidden" id="lottolerancepercent" name="lottolerancepercent" value="<?php echo $mainlicense['lottolerancepercent']; ?>">
	<input type="hidden" id="lottolerancevalue" name="lottolerancevalue" value="<?php echo $mainlicense['lottolerancevalue']; ?>">
	<input type="hidden" id="hidproductaddoninfo" name="hidproductaddoninfo" value="<?php echo $mainlicense['productaddoninfo']; ?>">
	<input type="hidden" id="itemtagsavefocus" name="itemtagsavefocus" value="<?php echo $mainlicense['itemtagsavefocus']; ?>">
	<input type="hidden" id="salesreturnautomatic" name="salesreturnautomatic" value="<?php echo $mainlicense['salesreturnautomatic']; ?>">
	<input type="hidden" id="stonepiecescalc" name="stonepiecescalc" value="<?php echo $stonepiecescalc; ?>">
	<input type="hidden" id="itemtagvendorvalidate" name="itemtagvendorvalidate" value="<?php echo $itemtagvendorvalidate; ?>">
	<input type="hidden" id="lottolerancediactvalue" name="lottolerancediactvalue" value="<?php echo $mainlicense['lottolerancediactvalue']; ?>">
	<input type="hidden" id="lotdiaweightlimit" name="lotdiaweightlimit" value="<?php echo $mainlicense['lotdiaweightlimit']; ?>">
	<input type="hidden" id="packwtlessgrosswt" name="packwtlessgrosswt" value="">
<div class="large-12 columns addformunderheader">&nbsp; </div>
<div class="large-12 columns addformcontainer">
	<div class="row mblhidedisplay">&nbsp;</div>
	<form method="POST" name="itemtagform" class="itemtagform"  id="itemtagform">
		<div id="addtagvalidate" class="validationEngineContainer" >
		 <div id="edittagvalidate" class="validationEngineContainer" >
			<div id="subformspan1" class="hiddensubform transitionhiddenform"> 
			<?php $device = $this->Basefunctions->deviceinfo();
							if($device=='phone') {
								?>
				<div class="large-4 columns paddingbtm tagfirstsection">	
					<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
						<div class="large-12 columns headerformcaptionstyle">Basic  Details</div>
						<div class="static-field large-6 columns stockentrydatehide " style="margin-top: 19px;">
							<label>Date<span class="mandatoryfildclass">*</span></label>								
							<input id="date" class="fordatepicicon validate[required]" type="text" data-dateformater="dd-mm-yy" data-prompt-position="topLeft" readonly="readonly" tabindex="101" name="date">
						</div>
						<div class="static-field large-6 columns branchiddiv">
							<label>Branch<span class="mandatoryfildclass">*</span></label>								
							<select id="branchid" name="branchid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="102">
								<option value=""></option>
								<?php foreach($branch as $key):?>
								<option value="<?php echo $key->branchid;?>">
								<?php echo $key->branchname;?></option>
								<?php endforeach;?>	 
							</select>
						</div>
						<div class="static-field large-6 columns ">
							<label>From Type<span class="mandatoryfildclass">*</span></label>								
							<select id="tagentrytypeid" name="tagentrytypeid" class="chzn-select " data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="102">
								<option value=""></option>
								<?php foreach($entrytypeid as $key):?>
								<option value="<?php echo $key->tagentrytypeid;?>" data-tagtypeshow="<?php echo $key->tagtypeshow;?>">
								<?php echo $key->tagentrytypename;?></option>
								<?php endforeach;?>	 
							</select>
						</div>
						<div class="static-field large-6 columns ">
							<label>To Type<span class="mandatoryfildclass">*</span></label>								
							<select id="tagtypeid" name="tagtypeid" class="chzn-select " data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="102">
								<option value=""></option>
								<?php foreach($tagtype as $key):?>
								<option value="<?php echo $key->tagtypeid;?>">
								<?php echo $key->tagtypename;?></option>
								<?php endforeach;?>	 
							</select>
						</div>
						<div class="static-field large-6 columns fromcounterhide hidedisplay" id="">
							<label>From Storage <span class="mandatoryfildclass" id="fromcounterid_req">*</span></label>				
							<select id="fromcounterid" name="fromcounterid" class="chzn-select" data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="115">
								<option value=""></option>
								<?php $prev = ' ';
								foreach($counter->result() as $key):
								?>
								<option value="<?php echo $key->counterid;?>" data-parentcounterid="<?php echo $key->parentcounterid;?>"  data-counterdefault="<?php echo $key->counterdefault;?>" data-counterdefaultid="<?php echo $key->counterdefaultid;?>"> <?php echo $key->counterid.' - '.$key->countername;?></option>
								<?php endforeach;?>
							</select>
						</div>
						<?php //} ?>
						<div class="static-field large-6 columns ">
							<label>Tag Template</label>	
							<select id="tagtemplateid" name="tagtemplateid" class="chzn-select " data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="103">
								<option value="1">No Print</option>
								<?php foreach($tagtemplate as $key):?>
								<option value="<?php echo $key->tagtemplateid;?>">
								<?php echo $key->tagtemplatename;?></option>
								<?php endforeach;?>	 
							</select>
						</div>
						<?php if(strtolower($mainlicense['lot']) == 'yes'){?>
						<div class="static-field large-12 columns clearonstocktypechange" id="lotid-div">
							<label>Lot Number</label>						
							<select id="lotid" name="lotid" class="chzn-select"  data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="104">	
								<option value="">Select</option>								
							</select>
						</div>
						<div class="input-field large-4 columns lotsummary lotweighthide">
							<input type="text" class="" id="totallotgrosswt" name="totallotgrosswt" value="" tabindex="104"  readonly>
							<label for="totallotgrosswt">Total G.Wt</label>	
						</div>
						<div class="input-field large-4 columns lotsummary lotweighthide">
							<input type="text" class="green" id="completedlotgrosswt" name="completedlotgrosswt" value="" tabindex="104"  readonly>
							<label for="completedlotgrosswt">Compltd G.Wt</label>	
						</div>
						<div class="input-field large-4 columns lotsummary lotweighthide">
							<input type="text" class="red" id="pendinglotgrosswt" name="pendinglotgrosswt" value="" tabindex="104"  readonly>
							<label for="pendinglotgrosswt">Pending G.Wt</label>	
						</div>
						<div class="input-field large-4 columns lotsummary lotweighthide">
							<input type="text" class="" id="totallotnetwt" name="totallotnetwt" value="" tabindex="105"   readonly>
							<label for="totallotnetwt">Total N.Wt</label>	
						</div>
						<div class="input-field large-4 columns lotsummary lotweighthide">
							<input type="text" class="green" id="completedlotnetwt" name="completedlotnetwt" value="" tabindex="105"   readonly>
							<label for="completedlotnetwt">Compltd N.Wt</label>	
						</div>
						<div class="input-field large-4 columns lotsummary lotweighthide">
							<input type="text" class="red" id="pendinglotnetwt" name="pendinglotnetwt" value="" tabindex="105"   readonly>
							<label for="pendinglotnetwt">Pending N.Wt</label>	
						</div>
						<div class="input-field large-4 columns lotsummary lotctweighthide">
							<input type="text" class="" id="totallotcaratwt" name="totallotcaratwt" value="" tabindex="105"   readonly>
							<label for="totallotcaratwt">Total Ct.Wt</label>	
						</div>
						<div class="input-field large-4 columns lotsummary lotctweighthide">
							<input type="text" class="green" id="completedlotcaratwt" name="completedlotcaratwt" value="" tabindex="105"   readonly>
							<label for="completedlotcaratwt">Compltd Ct.Wt</label>	
						</div>
						<div class="input-field large-4 columns lotsummary lotctweighthide">
							<input type="text" class="red" id="pendinglotcaratwt" name="pendinglotcaratwt" value="" tabindex="105"   readonly>
							<label for="pendinglotcaratwt">Pending Ct.Wt</label>	
						</div>
						<?php if($mainlicense['lotpieces'] == 'Yes'){?>
						<div class="input-field large-4 columns lotsummary">
							<input type="text" class="" id="totallotpieces" name="totallotpieces" value="" tabindex="105"   readonly>
							<label for="totallotpieces">Total Pcs</label>	
						</div>
						<div class="input-field large-4 columns lotsummary">
							<input type="text" class="green" id="completedlotpieces" name="completedlotpieces" value="" tabindex="105"   readonly>
							<label for="completedlotpieces">Completed Pcs</label>	
						</div>
						<div class="input-field large-4 columns lotsummary">
							<input type="text" class="red" id="pendinglotpieces" name="pendinglotpieces" value="" tabindex="105"   readonly>
							<label for="pendinglotpieces">Pending Pcs</label>	
						</div>
						<?php } ?>
						<?php } ?>
						<div class="static-field large-6 columns clearonstocktypechange" id="purityid-div">
							<label>Purity<span class="mandatoryfildclass" id="purityid_req">*</span></label>				
							<select id="purityid" name="purityid" class="chzn-select" data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="106">
								<option value=""></option>
								<?php $prev = ' ';
								foreach($purity->result() as $key):
								$cur = $key->metalid;
								$a="";
								if($prev != $cur )
								{
									if($prev != " ")
									{
										 echo '</optgroup>';
									}
									echo '<optgroup  label="'.$key->metalname.'">';
									$prev = $key->metalid;
								}
								?>
								<option data-melting="<?php echo $key->melting;?>" data-metalid="<?php echo $key->metalid;?>" value="<?php echo $key->purityid;?>"><?php echo $key->purityname;?></option>
								<?php endforeach;?>
							</select>
						</div>
						<div class="static-field large-6 columns hidedisplay" id="processproductid-div">
							<label>Process Product<span class="mandatoryfildclass" id="productid_req">*</span></label>
							<select id="processproductid" name="processproductid" class="chzn-select" style="" data-prompt-position="topLeft:14,36" tabindex="110">
								<option value="" data-purityid="0"></option>
								<?php $prev = ' ';
								foreach($processproduct->result() as $key):
								?>
								<option value="<?php echo $key->productid;?>" data-tagtypeid="<?php echo $key->tagtypeid;?>" data-categoryid="<?php echo $key->categoryid;?>" data-counterid="<?php echo $key->counterid;?>" data-purityid="<?php echo $key->purityid;?>" data-stone="<?php echo $key->stoneapplicable;?>"> <?php if($mainlicense['productidshow'] == 'YES') { echo $key->productid.' - '.$key->productname; } else { echo $key->productname;} ?></option>
								<?php endforeach;?>
							</select>
						</div>
						<div class="static-field large-6 columns clearonstocktypechange" id="accountid-div">
							<label>Vendor<?php if($itemtagvendorvalidate == 1) { echo '<span class="mandatoryfildclass" id="purityid_req">*</span>'; } ?></label>
							<select id="accountid" name="accountid" class="chzn-select" data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="107">
								<option value="">Select</option>
								<?php $prev = ' ';
								foreach($account->result() as $key):
								$cur = $key->accounttypeid;
								$a="";
								if($prev != $cur )
								{
									if($prev != " ")
									{
										 echo '</optgroup>';
									}
									echo '<optgroup  label="'.$key->accounttypename.'">';
									$prev = $key->accounttypeid;
								}
								?>
								<option value="<?php echo $key->accountid;?>"><?php echo $key->accountname;?></option>
							<?php endforeach;?>
							</select>
						</div>
						<?php if($mainlicense['weightscale'] == 'Yes'){?>
							<!--<div class="static-field large-6 columns">
										<label>Auto Weight</label>							
										<select data-validation-engine="" class="chzn-select" id="scaleoption" name="scaleoption" data-prompt-position="topLeft:14,36" tabindex="108">
											<option value="Yes">Yes</option>
											<option value="No">No</option>
										</select>
							</div>-->
						<?php } ?>
						<div class="static-field large-6 columns hidedisplay">
									<label>Guarantee Card</label>							
									<select data-validation-engine="" class="chzn-select" id="guaranteecard" name="guaranteecard" data-prompt-position="topLeft:14,36" tabindex="109">
										<option value="No">No</option>
										<option value="Yes">Yes</option>
									</select>
						</div>
					<div class="large-12 columns "> &nbsp; </div>
					</div>
				</div>
							<?php	
							} else {
								?>
				<div class="large-4 columns paddingbtm tagfirstsection">	
					<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
						<div class="large-12 columns headerformcaptionstyle">Basic Details</div>
						<div class="static-field large-6 columns stockentrydatehide " style="margin-top: 19px;">
							<label>Date<span class="mandatoryfildclass">*</span></label>								
							<input id="date" class="fordatepicicon validate[required]" type="text" data-dateformater="dd-mm-yy" data-prompt-position="topLeft" readonly="readonly" tabindex="101" name="date">
						</div>
						<div class="static-field large-6 columns branchiddiv">
							<label>Branch<span class="mandatoryfildclass">*</span></label>								
							<select id="branchid" name="branchid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="102">
								<option value=""></option>
								<?php foreach($branch as $key):?>
								<option value="<?php echo $key->branchid;?>">
								<?php echo $key->branchname;?></option>
								<?php endforeach;?>	 
							</select>
						</div>
						<div class="static-field large-6 columns ">
							<label>From Type<span class="mandatoryfildclass">*</span></label>								
							<select id="tagentrytypeid" name="tagentrytypeid" class="chzn-select " data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="102">
								<option value=""></option>
								<?php foreach($entrytypeid as $key):?>
								<option value="<?php echo $key->tagentrytypeid;?>" data-tagtypeshow="<?php echo $key->tagtypeshow;?>">
								<?php echo $key->tagentrytypename;?></option>
								<?php endforeach;?>	 
							</select>
						</div>
						<div class="static-field large-6 columns ">
							<label>To Type<span class="mandatoryfildclass">*</span></label>								
							<select id="tagtypeid" name="tagtypeid" class="chzn-select " data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="102">
								<option value=""></option>
								<?php foreach($tagtype as $key):?>
								<option value="<?php echo $key->tagtypeid;?>">
								<?php echo $key->tagtypename;?></option>
								<?php endforeach;?>	 
							</select>
						</div>
						<div class="static-field large-6 columns fromcounterhide hidedisplay" id="">
							<label>From Storage <span class="mandatoryfildclass" id="fromcounterid_req">*</span></label>				
							<select id="fromcounterid" name="fromcounterid" class="chzn-select" data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="115">
								<option value=""></option>
								<?php $prev = ' ';
								foreach($counter->result() as $key):
								?>
								<option value="<?php echo $key->counterid;?>" data-parentcounterid="<?php echo $key->parentcounterid;?>"  data-counterdefault="<?php echo $key->counterdefault;?>" data-counterdefaultid="<?php echo $key->counterdefaultid;?>"> <?php echo $key->counterid.' - '.$key->countername;?></option>
								<?php endforeach;?>
							</select>
						</div>
						<?php //} ?>
						<div class="static-field large-6 columns ">
							<label>Tag Template</label>	
							<select id="tagtemplateid" name="tagtemplateid" class="chzn-select " data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="103">
								<option value="1">No Print</option>
								<?php foreach($tagtemplate as $key):?>
								<option value="<?php echo $key->tagtemplateid;?>">
								<?php echo $key->tagtemplatename;?></option>
								<?php endforeach;?>	 
							</select>
						</div>
						<?php if(strtolower($mainlicense['lot']) == 'yes'){?>
						<div class="static-field large-12 columns clearonstocktypechange" id="lotid-div">
							<label>Lot Number</label>						
							<select id="lotid" name="lotid" class="chzn-select"  data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="104">	
								<option value="">Select</option>								
							</select>
						</div>
						<div class="input-field large-4 columns lotsummary lotweighthide">
							<input type="text" class="" id="totallotgrosswt" name="totallotgrosswt" value="" tabindex="104"  readonly>
							<label for="totallotgrosswt">Total G.Wt</label>	
						</div>
						<div class="input-field large-4 columns lotsummary lotweighthide">
							<input type="text" class="green" id="completedlotgrosswt" name="completedlotgrosswt" value="" tabindex="104"  readonly>
							<label for="completedlotgrosswt">Compltd G.Wt</label>	
						</div>
						<div class="input-field large-4 columns lotsummary lotweighthide">
							<input type="text" class="red" id="pendinglotgrosswt" name="pendinglotgrosswt" value="" tabindex="104"  readonly>
							<label for="pendinglotgrosswt">Pending G.Wt</label>	
						</div>
						<div class="input-field large-4 columns lotsummary lotweighthide">
							<input type="text" class="" id="totallotnetwt" name="totallotnetwt" value="" tabindex="105"   readonly>
							<label for="totallotnetwt">Total N.Wt</label>	
						</div>
						<div class="input-field large-4 columns lotsummary lotweighthide">
							<input type="text" class="green" id="completedlotnetwt" name="completedlotnetwt" value="" tabindex="105"   readonly>
							<label for="completedlotnetwt">Compltd N.Wt</label>	
						</div>
						<div class="input-field large-4 columns lotsummary lotweighthide">
							<input type="text" class="red" id="pendinglotnetwt" name="pendinglotnetwt" value="" tabindex="105"   readonly>
							<label for="pendinglotnetwt">Pending N.Wt</label>	
						</div>
						<div class="input-field large-4 columns lotsummary lotctweighthide">
							<input type="text" class="" id="totallotcaratwt" name="totallotcaratwt" value="" tabindex="105"   readonly>
							<label for="totallotcaratwt">Total Ct.Wt</label>	
						</div>
						<div class="input-field large-4 columns lotsummary lotctweighthide">
							<input type="text" class="green" id="completedlotcaratwt" name="completedlotcaratwt" value="" tabindex="105"   readonly>
							<label for="completedlotcaratwt">Compltd Ct.Wt</label>	
						</div>
						<div class="input-field large-4 columns lotsummary lotctweighthide">
							<input type="text" class="red" id="pendinglotcaratwt" name="pendinglotcaratwt" value="" tabindex="105"   readonly>
							<label for="pendinglotcaratwt">Pending Ct.Wt</label>	
						</div>
						<?php if($mainlicense['lotpieces'] == 'Yes'){?>
						<div class="input-field large-4 columns lotsummary">
							<input type="text" class="" id="totallotpieces" name="totallotpieces" value="" tabindex="105"   readonly>
							<label for="totallotpieces">Total Pcs</label>	
						</div>
						<div class="input-field large-4 columns lotsummary">
							<input type="text" class="green" id="completedlotpieces" name="completedlotpieces" value="" tabindex="105"   readonly>
							<label for="completedlotpieces">Completed Pcs</label>	
						</div>
						<div class="input-field large-4 columns lotsummary">
							<input type="text" class="red" id="pendinglotpieces" name="pendinglotpieces" value="" tabindex="105"   readonly>
							<label for="pendinglotpieces">Pending Pcs</label>	
						</div>
						<?php } ?>
						<?php } ?>
						<div class="static-field large-6 columns clearonstocktypechange" id="purityid-div">
							<label>Purity<span class="mandatoryfildclass" id="purityid_req">*</span></label>				
							<select id="purityid" name="purityid" class="chzn-select" data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="106">
								<option value=""></option>
								<?php $prev = ' ';
								foreach($purity->result() as $key):
								$cur = $key->metalid;
								$a="";
								if($prev != $cur )
								{
									if($prev != " ")
									{
										 echo '</optgroup>';
									}
									echo '<optgroup  label="'.$key->metalname.'">';
									$prev = $key->metalid;
								}
								?>
								<option data-melting="<?php echo $key->melting;?>" data-metalid="<?php echo $key->metalid;?>" value="<?php echo $key->purityid;?>"><?php echo $key->purityname;?></option>
								<?php endforeach;?>
							</select>
						</div>
						<div class="static-field large-6 columns hidedisplay" id="processproductid-div">
							<label>Process Product<span class="mandatoryfildclass" id="productid_req">*</span></label>
							<select id="processproductid" name="processproductid" class="chzn-select" style="" data-prompt-position="topLeft:14,36" tabindex="110">
								<option value="" data-purityid="0"></option>
								<?php $prev = ' ';
								foreach($processproduct->result() as $key):
								?>
								<option value="<?php echo $key->productid;?>" data-tagtypeid="<?php echo $key->tagtypeid;?>" data-categoryid="<?php echo $key->categoryid;?>" data-counterid="<?php echo $key->counterid;?>" data-purityid="<?php echo $key->purityid;?>" data-stone="<?php echo $key->stoneapplicable;?>"> <?php if($mainlicense['productidshow'] == 'YES') { echo $key->productid.' - '.$key->productname; } else { echo $key->productname;} ?></option>
								<?php endforeach;?>
							</select>
						</div>
						<div class="static-field large-6 columns clearonstocktypechange" id="accountid-div">
							<label>Vendor<?php if($itemtagvendorvalidate == 1) { echo '<span class="mandatoryfildclass" id="purityid_req">*</span>'; } ?></label>
							<select id="accountid" name="accountid" class="chzn-select" data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="107">
								<option value="">Select</option>
								<?php $prev = ' ';
								foreach($account->result() as $key):
								$cur = $key->accounttypeid;
								$a="";
								if($prev != $cur )
								{
									if($prev != " ")
									{
										 echo '</optgroup>';
									}
									echo '<optgroup  label="'.$key->accounttypename.'">';
									$prev = $key->accounttypeid;
								}
								?>
								<option value="<?php echo $key->accountid;?>"><?php echo $key->accountname;?></option>
								<?php endforeach;?>
							</select>
						</div>
						<?php if($mainlicense['weightscale'] == 'Yes'){?>
							<!--<div class="static-field large-6 columns">
										<label>Auto Weight</label>							
										<select data-validation-engine="" class="chzn-select" id="scaleoption" name="scaleoption" data-prompt-position="topLeft:14,36" tabindex="108">
											<option value="Yes">Yes</option>
											<option value="No">No</option>
										</select>
							</div>-->
						<?php } ?>
						<div class="static-field large-6 columns hidedisplay">
									<label>Guarantee Card</label>							
									<select data-validation-engine="" class="chzn-select" id="guaranteecard" name="guaranteecard" data-prompt-position="topLeft:14,36" tabindex="109">
										<option value="No">No</option>
										<option value="Yes">Yes</option>
									</select>
						</div>
					<div class="large-12 columns "> &nbsp; </div>
					</div>
				</div>
				<?php }
				?>
				<div class="large-4 columns paddingbtm tagsecondsection">	
					<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
						<div class="large-12 columns headerformcaptionstyle">Product Details</div>
						<div class="static-field large-12 columns clearonstocktypechange" id="productid-div">
							<label>Product Name<span class="mandatoryfildclass" id="productid_req">*</span></label>
							<select id="productid" name="productid" class="chzn-select" style="" data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="110">
								<option value="" data-purityid="0"></option>
								<?php $prev = ' ';
								foreach($product->result() as $key):
								?>
								<option value="<?php echo $key->productid;?>" data-tagtypeid="<?php echo $key->tagtypeid;?>" data-categoryid="<?php echo $key->categoryid;?>" data-counterid="<?php echo $key->counterid;?>" data-purityid="<?php echo $key->purityid;?>"  data-purityidhidden="<?php echo $key->purityidhidden;?>" data-stone="<?php echo $key->stoneapplicable;?>" data-rfid="<?php echo $key->rfidapplicable;?>" data-stonevalcetype="<?php echo $key->productstonecalctypeid;?>" data-taxratesum="<?php echo $key->taxratesum;?>" data-tagweight="<?php echo $key->tagweight;?>" data-packingweight="<?php echo $key->packingweight;?>" data-packwtless="<?php echo $key->packwtless;?>" data-size="<?php echo $key->size;?>" data-productpieces="<?php echo $key->productpieces;?>" data-tagtemplateid="<?php echo $key->tagtemplateid;?>"> <?php if($mainlicense['productidshow'] == 'YES') { echo $key->productid.' - '.$key->productname; } else { echo $key->productname;} ?></option>
								<?php endforeach;?>
							</select>
						</div>
						<?php if($mainlicense['productaddoninfo'] == '1') { ?>
							<div class="static-field large-6 columns" id="productaddoninfo-div">
								<label>Product Addon Info </label>			
								<select id="productaddoninfoid" name="productaddoninfoid" class="chzn-select " data-validation-engine="" data-prompt-position="topLeft:14,36" multiple tabindex="116">
									<?php 
									foreach($productaddoninfo_dd->result() as $key):
									?>
										<option value="<?php echo $key->productaddoninfoid; ?>" data-productaddoninfoname="<?php echo $key->productaddoninfoname; ?>" ><?php echo $key->productaddoninfoname; ?></option>
									<?php endforeach;?>
								</select>
							</div>
						<?php } ?>
						<?php if((strtolower($mainlicense['counter']) == 'yes')) { 
							//$showtocounter = 'ddhidedisplay';}else {$showtocounter = '';}?>
							<div class="static-field large-6 columns clearonstocktypechange <?php //echo $showtocounter; ?>" id="counterid-div">
								<label>To Storage <span class="mandatoryfildclass" id="counterid_req">*</span></label>			
								<select id="counterid" name="counterid" class="chzn-select " data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="117">
									<option value=""></option>
									<?php $prev = ' ';
									foreach($notdefaultcounter->result() as $key):
									?>
									<option value="<?php echo $key->counterid;?>" data-countertypeid="<?php echo $key->countertypeid;?>" data-maxquantity="<?php echo $key->maxquantity;?>"> <?php echo $key->counterid.' - '.$key->countername;?></option>
									<?php endforeach;?>
								</select>
							</div>
						<?php } ?>
						<div class="static-field large-6 columns clearonstocktypechange" id="size-div">
							<label>Size<span class="mandatoryfildclass">*</span></label>
							<select id="size" name="size" class="chzn-select" style="" data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="110">
								<!--<option value="1" data-purityid="0"></option>-->
								<?php $prev = ' ';
								foreach($sizename->result() as $key):
								?>
							<option value="<?php echo $key->sizemasterid;?>" data-productname="<?php echo $key->productname;?>" data-productid="<?php echo $key->productid;?>"> <?php echo $key->sizemastername; ?></option>
								<?php endforeach;?>
							</select>
						</div>
						<div class="input-field large-6 columns clearonstocktypechange" id="grossweight-div">
							<input type="text" class="weightcalculate tagaddclear" id="grossweight" data-calc="GWT" name="grossweight" value="" tabindex="112" data-validation-engine="" maxlength="15">
							<label for="grossweight">Gross Weight<span class="mandatoryfildclass" >*</span></label>
						</div>
						<div class="input-field large-6 columns clearonstocktypechange" id="stoneweight-div">
							<input type="text" class="weightcalculate tagaddclear" data-calc="SWT" data-validategreat="grossweight" id="stoneweight" name="stoneweight" value="" tabindex="113" data-validation-engine="" readonly>
							<label for="stoneweight">Stone Weight<span class="mandatoryfildclass">*</span></label>
						</div>
						<input type="hidden" id="hiddenstonedata" name="hiddenstonedata"/>
						<div class="input-field large-6 columns clearonstocktypechange" id="netweight-div">
							<input type="text" class="weightcalculate tagaddclear" data-calc="NWT" data-validategreat="grossweight" id="netweight" name="netweight" value="" tabindex="114" data-validation-engine="" readonly>
							<label for="netweight">Net Weight<span class="mandatoryfildclass">*</span></label>
						</div>
						<div class="input-field large-6 columns clearonstocktypechange" id="itemcaratweight-div">
							<input type="text" class="weightcalculate tagaddclear" data-calc="CTWT" data-validategreat="" id="itemcaratweight" name="itemcaratweight" value="" tabindex="114" data-validation-engine="">
							<label for="itemcaratweight">Carat Weight<span class="mandatoryfildclass">*</span></label>
						</div>
						<?php if($mainlicense['prizetaggst'] == '0') { ?>
						<div class="input-field large-6 columns clearonstocktypechange" id="itemrate-div">
							<input type="text" class="" id="itemrate" name="itemrate" value="" tabindex="115" data-validation-engine="" maxlength="10">
							<label for="itemrate">Item Rate<span class="mandatoryfildclass" >*</span></label>
						</div>
						<?php }?>
						<?php if($mainlicense['prizetaggst'] == '1') { ?>
						<div class="input-field large-6 columns clearonstocktypechange" id="itemratewithgst-div">
							<input type="text"  class="" id="itemratewithgst" name="itemratewithgst" value="" tabindex="115" data-validation-engine="">
							<label for="itemratewithgst">Item Rate(With GST)<span class="mandatoryfildclass" >*</span></label>
						</div>
						<div class="input-field large-6 columns clearonstocktypechange" id="itemrate-div">
							<input type="text" readonly class="" id="itemrate" name="itemrate" value="" tabindex="115" data-validation-engine="">
							<label for="itemrate">Item Rate(Without GST)<span class="mandatoryfildclass" >*</span></label>
						</div>
						<?php } ?>
						<?php if($mainlicense['rfidoption'] == '2' || $mainlicense['rfidoption'] == '3') { ?>
						<div class="input-field large-6 columns clearonstocktypechange" id="rfidtagno-div">
							<input type="text" class="tagaddclear" id="rfidtagno" name="rfidtagno" value="" tabindex="111">
							<label for="rfidtagno">RFID Tag No<span class="mandatoryfildclass" id="rfidtagno_req">*</span></label>	
						</div>
						<?php } ?>
						<?php if($mainlicense['untaguniqueno'] == '1') { ?>
						<div class="input-field large-6 columns clearonstocktypechange" id="untagno-div">
							<input type="text" class="tagaddclear" id="untagno" name="untagno" value="" tabindex="111">
							<label for="untagno">Untag Unique No<span class="mandatoryfildclass" id="untagno_req"></span></label>	
						</div>
						<?php } ?>
						<div class="input-field large-6 columns clearonstocktypechange" id="pieces-div"> 
							<input type="text" class="tagaddclear" id="pieces" name="pieces" value="" tabindex="116" data-validation-engine="" maxlength="10">
							<label for="pieces">Pieces <span class="mandatoryfildclass" id="pieces_req">*</span></label>
						</div>
						<div class="input-field large-12 columns chargedetailsdiv charge3 hidedisplay">
							<input type="text"  chargeid="" keywordcalc="" class="chargedata" keywordrange="" keyword="" id="wastage" name="wastage" value="" tabindex="112" readonly>
							<label id="charge3" for="wastage" title="Wastage"><span id="wastagespan" spankeyword="">Wastage</span>
							<?php 
							$itemtagauthrole = explode(",",$itemtagauthrole);
							if(in_array($loggedinuserroleid, $itemtagauthrole)) {
							?>
								<i id="wastageiconchange" class="material-icons waves-effect waves-circle addonchargeiconchange" keywordarray="" keywordcalc="">details</i> 
								<span keyname="wastagespankey" class="orginalvalclass inlinespanedit" id=""></span>
							<?php } ?>
							</label>
							</div>
						<div class="input-field large-12 columns chargedetailsdiv charge2 hidedisplay">
							<input type="text"  chargeid="" keywordcalc="" class="chargedata" keyword="" keywordrange="" id="making" name="making" value="" tabindex="112" readonly>
							<label id="charge2" for="Making" title="Making"><span id="makingspan" spankeyword="">Making</span>
							<?php if(in_array($loggedinuserroleid, $itemtagauthrole)) { ?>			
								<i id="makingiconchange" class="material-icons waves-effect waves-circle addonchargeiconchange" keywordarray="">details</i> 
								<span keyname="makingspankey" class="orginalvalclass inlinespanedit" id=""></span>
							<?php } ?>
							</label>
							</div>
						<div class="input-field large-12 columns chargedetailsdiv charge6 hidedisplay">
							<input type="text"  chargeid="" keywordcalc="" class="chargedata" keyword="" keywordrange="" id="flat" name="flat" value="" tabindex="112" readonly>
							<label id="charge6" for="flat" title="flat"><span id="flatspan" spankeyword="">flat</span>
							<?php if(in_array($loggedinuserroleid, $itemtagauthrole)) { ?>			
								<i id="flaticonchange" class="material-icons waves-effect waves-circle addonchargeiconchange" keywordarray="">details</i> 
								<span keyname="flatspankey" class="orginalvalclass inlinespanedit" id=""></span>
							<?php } ?>
							</label>
							</div>
						<div class="input-field large-6 columns chargedetailsdiv charge4 hidedisplay">
							<input type="text"  chargeid=""  keywordcalc="" class="chargedata" keyword="" keywordrange="" id="hallmark" name="hallmark" value="" tabindex="112" readonly>
							<label id="charge4" for="hallmark" title="Hallmark"><span id="hallmarkspan" spankeyword="">Hallmark</span>
							<?php if(in_array($loggedinuserroleid, $itemtagauthrole)) { ?>	
								<i id="hallmarkiconchange" class="material-icons waves-effect waves-circle addonchargeiconchange" keywordarray="">details</i> 
								<span keyname="hallmarkspankey" class="orginalvalclass inlinespanedit" id=""></span>
						<?php } ?>
						</label>
							</div>
						<?php if($mainlicense['chargecalcoption'] == 1 || $mainlicense['chargecalcoption'] ==  3 ) { ?>
						<div class="input-field large-6 columns chargedetailsdiv charge5 hidedisplay">
							<input type="text" chargeid="11" minid="cflatmin" maxid="cflatmax" keywordrange="CC-FT.MIN" keywordcalc="CC-FT-MIN" class="chargedata" keyword="CC-FT" id="cflatmin" name="cflatmin" value="" tabindex="117" readonly>
							<label for="cflatmin" title="Certification Flat Min">Cert Flat Min 
							<?php if(in_array($loggedinuserroleid, $itemtagauthrole)) { ?>	
							<span class="orginalvalclass inlinespanedit" id="CC-FT-MIN"></span>
							<?php } ?>
							</label>
						</div>
						<?php } ?>
						<?php if($mainlicense['chargecalcoption'] == 2 || $mainlicense['chargecalcoption'] ==  3 ) { ?>
						<div class="input-field large-6 columns chargedetailsdiv charge5 hidedisplay">
							<input type="text" chargeid="11" minid="cflatmin" maxid="cflatmax" keywordrange="CC-FT.MAX" keywordcalc="CC-FT-MAX" class="chargedata max" keyword="CC-FT" id="cflatmax" name="cflatmax" value="" tabindex="117" readonly>
							<label for="cflatmax" title="Certification Flat Max">Cert Flat Max 
							<?php if(in_array($loggedinuserroleid, $itemtagauthrole)) { ?>	
							<span class="orginalvalclass inlinespanedit" id="CC-FT-MAX"></span>
							<?php } ?>
							</label>
						</div>
						<?php } ?>
						
						<?php if($mainlicense['chargecalcoption'] == 1 || $mainlicense['chargecalcoption'] ==  3 ) { ?>
						<div class="input-field large-6 columns chargedetailsdiv charge13 hidedisplay">
							<input type="text" minid="ompermin" maxid="ompermax" keywordrange="OM-PT.MIN" keywordcalc="OM-PT-MIN" class="chargedata" keyword="OM-PT" id="ompermin" name="ompermin" value="" tabindex="119" readonly>
							<label for="ompermin" title="Old Multiple Percent Min">Old Multiple Percent Min <span class="orginalvalclass inlinespanedit" id="OM-PT-MIN"></span></label>
						</div>
						<?php } ?>
						<?php if($mainlicense['chargecalcoption'] == 2 || $mainlicense['chargecalcoption'] ==  3 ) { ?>
						<div class="input-field large-6 columns chargedetailsdiv charge13 hidedisplay">
							<input type="text"  minid="ompermin" maxid="ompermax" keywordrange="OM-PT.MAX" keywordcalc="OM-PT-MAX" class="chargedata" keyword="OM-PT" id="ompermax" name="ompermax" value="" tabindex="119" readonly>
							<label for="ompermax" title="Old Multiple Percent Max">Old Multiple Percent Max <span class="orginalvalclass inlinespanedit" id="OM-PT-MAX"></span></label>
						</div>
						<?php } ?>
						<div class="large-12 columns "> &nbsp; </div>
					</div>
				</div>
				<div class="large-4 columns paddingbtm tagthirdsection">	
					<div class="large-12 columns cleardataform borderstyle " style="padding-left: 0 !important; padding-right: 0 !important;">					
						<div class="large-12 columns headerformcaptionstyle">Additional Details</div>
						<?php if($mainlicense['purchasedata']  == 'YES') { $data=''; } else { $data='hidedisplay';}?>
							
						<div class="input-field large-6 columns lasttagnodiv">
							<input type="text" class="" id="lasttagno" name="lasttagno" value="" tabindex="118" readonly data-validation-engine="">
							<label for="lasttagno">Last Tag No</label>
						</div>
						<?php if($mainlicense['rfidoption'] == '2' || $mainlicense['rfidoption'] == '3') { ?>
						<div class="input-field large-6 columns lastrfidnodiv">
							<input type="text" class="" id="lastrfidno" name="lastrfidno" value="" tabindex="118" readonly data-validation-engine="">
							<label for="lasttagno">Last RFID No</label>
						</div>
						<?php } ?>
						<div class="input-field large-6 columns">
							<input type="text" class="" id="tagweight" name="tagweight" value="" tabindex="118" data-validation-engine="validate[custom[number],decval[<?php echo $weight_round;?>]]" maxlength="15">
							<label for="tagweight">Tag Weight</label>
						</div>
						<div class="input-field large-6 columns">
							<input type="text" class="" id="packingweight" name="packingweight" value="" tabindex="118" data-validation-engine="validate[custom[number],decval[<?php echo $weight_round;?>]]" maxlength="15">
							<label for="packingweight">Packing Weight</label>
						</div>
						<div class="static-field large-6 columns">
							<label>Packing Wt Less</label>			
							<select id="packwtless" name="packwtless" class="chzn-select" data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="124">
								<option value=""></option>
								<option value="No">No</option>
								<option value="Yes">Yes</option>
							</select>
						</div>
						<div class="static-field large-6 columns">
							<label>Charges From</label>			
							<select id="chargefrom" name="chargefrom" class="chzn-select" data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="124">
								<option value="0">Product Addon</option>
								<option value="1">Stock Entry</option>
							</select>
						</div>
						<div class="input-field large-6 columns">
							<input type="text" class="" id="certificationno" name="certificationno" value="" tabindex="122" maxlength="20" >
							<label for="certificationno">Certification No</label>
						</div>
						<div class="static-field large-6 columns">
							<label>Tag Auto Image</label>			
							<select id="mtagautoimage" name="mtagautoimage" class="chzn-select" data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="125">
								<option value="0">No</option>
								<option value="1">Yes</option>
							</select>
						</div>
						<div class="input-field large-6 columns <?php echo $data; ?>">
							<input type="text" class="" id="rate" name="rate" value="" tabindex="119" data-validation-engine="validate[custom[number],decval[<?php echo $rate_round;?>],maxSize[15]]">
							<label for="rate">Purchase Rate / Gm</label>
						</div>
						<div class="input-field large-6 columns <?php echo $data; ?>">
							<input type="text" class="" id="touch" name="touch" value="" tabindex="120" data-validation-engine="validate[custom[number],decval[<?php echo $melting_round;?>],maxSize[15],max[150]]">
							<label for="touch">Purchase Touch</label>
						</div>
						<div class="input-field large-6 columns <?php echo $data; ?>">
							<input type="text" class="" id="melting" name="melting" value="" tabindex="121" data-validation-engine="validate[custom[number],decval[<?php echo $melting_round;?>],maxSize[15],max[100]]">
							<label for="melting">Purchase Melting</label>
						</div>
						<div class="input-field large-6 columns <?php echo $data; ?>">
							<input type="text" class="" id="hallmarkno" name="hallmarkno" value="" tabindex="122" data-validation-engine="validate[maxSize[20]]">
							<label for="hallmarkno">HallMark No</label>
						</div>
						<div class="input-field large-6 columns <?php echo $data; ?>">
							<input type="text" class="" id="hallmarkcenter" name="hallmarkcenter" value="" tabindex="123" data-validation-engine="validate[maxSize[20]]">
							<label for="hallmarkcenter">HallMark Center</label>
						</div>
						<?php if((strtolower($mainlicense['collection']) == 'yes')) {?>
						<div class="static-field large-6 columns">
							<label>Collection</label>			
							<select id="collectionid" name="collectionid" class="chzn-select" data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="124">
								<?php $prev = ' ';
								foreach($collection as $key):
								?>
								<option value="<?php echo $key->collectionid;?>"> <?php echo $key->collectionid.' - '.$key->collectionname;?></option>
								<?php endforeach;?>
							</select>
						</div> <?php } ?>
						<div class="input-field large-12 columns  <?php echo $data; ?>">		
							<textarea name="description" class="materialize-textarea" id="description" rows="3" cols="40" tabindex="125" data-validation-engine="validate[maxSize[200]]"></textarea>
							<label for="description">Description</label>
						</div>					
						<div class="large-12 columns "> &nbsp; </div>
					</div>
				</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>
				
			</div>
			<div id="subformspan2" class="hiddensubform hidedisplay transitionhiddenform"> 
				<div class="large-6 columns paddingbtm">	
					<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
						<div class="large-12 columns headerformcaptionstyle">Photo</div>
						<div class="large-12 medium-12 small-12 columns" id="imagediv">
							<div class="large-3 medium-3 small-12 columns" style="padding-left: 0px;">
								<label>Upload Image</label>
								<div id="tagfileuploadfromid" style="text-align:center; border:1px solid #ffffff;">
								<span  class="" title="Click to upload" style="padding:2px; color:#2574a9; font-size:26px;"><i class="material-icons">file_upload</i></span></div>
							</div>
							<input id="tagimage" type="hidden" name="tagimage" value="" tabindex="122">
							<div class="large-3 medium-3 small-12 columns">
								<label>Take Picture</label>
								<div id="" style="text-align:center; border:1px solid #ffffff;">
								<span id="imgcapture" class="" title="Click To Take Photo" style="padding:2px; color:#2574a9; font-size:26px;"><i class="material-icons">add_a_photo</i></span></div>
							</div>	
							<span id="taglogomulitplefileuploader" style="display:none;">upload</span>						
						</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">
							<div id="taglivedisplay" class="large-12 medium-12 small-12 columns" style="border:1px solid #ffffff; height:20rem; padding:0px;"></div>						
						</div>			
						<div class="large-12 columns">&nbsp;</div>		
						<div class="large-4 columns" class="snapshotcapturebtn">
						<input id="take-snapshot" class="btn leftformsbtn" type="button" value="Capture" tabindex="123">
						</div>
					</div>
				</div>
				<div class="large-6 columns end paddingbtm">
					<div class="large-12 columns cleardataform paddingzero borderstyle" style="">
						<div class="large-12 columns headerformcaptionstyle">Preview</div>
						<div class="large-12 columns">
							<input id="" type="hidden" name="" value="">
							<div id="tagimagedisplay" class="large-12 columns uploadcontainerstyle" style="overflow:scroll;height:27rem; padding:0px; border:1px solid #CCCCCC"></div>
							
							<div class="row">&nbsp;</div>
							<div class="row">&nbsp;</div>
							<div class="large-12" style="">
								<div class="snapshotsavebtn">											
									<input type="button" value="Save" id="savecapturedimg" class="btn leftformsbtn" tabindex="124">
									<input type="button" value="Remove" id="resetcapturedimg" class="btn leftformsbtn" tabindex="125">
								</div>
							</div>
						</div>
					</div>					
					<textarea class="hidedisplay" id="imgurldata"></textarea>
					<textarea class="hidedisplay" id="imgurldatadelete"></textarea>
				</div>
			</div>
			</div>
		</div>
		<?php
			//hidden text box view columns field module ids
			$modid = $this->Basefunctions->getmoduleid($filedmodids);
			echo hidden('viewfieldids',$modid);
			echo hidden('moduleviewid',$modid);
		?>
	</form>	 
</div>


<!--Product Search Grid---->
<div class="large-6 columns" style="position:absolute;">
	<div class="overlay" id="productsearchoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="large-10 medium-10 large-centered medium-centered columns">
			<div class="large-12 columns headercaptionstyle headerradius">
				<div class="large-6 medium-6 small-6 columns headercaptionleft">
					Product Search
				</div>
				<div class="large-5 medium-6 small-6 columns addformheadericonstyle" style="text-align:right;padding-right:0 !important">
					<span id="productsearchicon" class="fa fa-search searchiconclass" title="Search"> </span>
					<span id="productsearchsubmit" class="fa fa-floppy-o" title="Save"> </span>
					<span id="productsearchclose" class="fa fa-times" title="Close"> </span>
				</div>
			</div>
			<div class="large-12 column paddingzero" id="productsearchgridwidth" style="background:#ffffff">
				<table id="productsearchgrid"></table> 
				<div id="productsearchgriddiv" class="w100"></div> 
			</div>
		</div>
	</div>
</div>
<!--charge icon overlay-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="chargeiconoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-10 medium-10 small-12 large-centered medium-centered small-centered columns">
			<div class="large-12 column paddingzero"  style="background:transparent">
				<div class="row">&nbsp;</div>
				<div class="large-4 large-offset-4 columns end paddingbtm" style="padding-top: 150px;">
					<div class="large-12 columns cleardataform validationEngineContainer borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;" id="additionalchargeaddvalidate">
					<div class="large-12 columns sectionheaderformcaptionstyle"> Charge Details</div>
						<div class="input-field large-6 columns">
						<input type="text" class="chargecloneclass" data-keyword="" id="chargeclone" name="chargeclone" value="" tabindex="">
								<label for="chargeclone" title="" id="chargeclonelabel">Value</label>
							</div>
							<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
							<input type="button" class="alertbtnno" id="chargeiconclose" name="" value="Close" tabindex="">
						</div>
							</div>
							</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>				
			</div>
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
	</div>
</div>