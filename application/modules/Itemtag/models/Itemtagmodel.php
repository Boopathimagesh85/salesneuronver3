<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Itemtagmodel extends CI_Model
{    
    private $itemtagmodule = 50;
    public function __construct()
    {		
		parent::__construct();
		$this->load->model('Lot/Lot_model');
		$this->load->model('Printtemplates/Printtemplatesmodel');
    }				
	/*	*lotdropdown*/
	public function lotdropdown($lotid) //?issue
	{
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$wh1 = ' lot.status = 1 ';
		$limit = '';
		if($lotid != '')
		{	
			$wh1 = ' lot.lotid = '.$lotid. ' ';
		}
		if($lotid != '')
		{	
			$limit = ' limit  1 ';
		}
		$defaultstatus = 3;
		$activestatus = $this->Basefunctions->activestatus;
		$info = $this->db->query("
							SELECT lot.lotid,lotnumber,lotdate,lot.productid,lot.purityid,purity.purityname,lot.accountid,lot.categoryid,lot.salesdetailid,lot.billrefno,lot.receiveorder,
							ROUND((lot.grossweight - coalesce(SUM(itemtag.grossweight),0)),$round) as pendinggrossweight,
							ROUND((lot.netweight - coalesce(SUM(itemtag.netweight),0)),$round) as pendingnetweight,
							ROUND((lot.stoneweight - coalesce(SUM(itemtag.stoneweight),0)),$round) as pendingstoneweight,
							ROUND((lot.caratweight - coalesce(SUM(itemtag.caratweight),0)),2) as pendingcaratweight,
							ROUND((lot.pieces - coalesce(SUM(itemtag.pieces),0)),0) as pendingpieces,
							ROUND(lot.grossweight,$round) as totalgrossweight,
							ROUND(lot.netweight,$round) as totalnetweight,
							ROUND(lot.caratweight,2) as totalcaratweight,
							ROUND(lot.pieces,0) as totalpieces,
							ROUND(coalesce(SUM(itemtag.grossweight),0),$round) as completedgrossweight,
							ROUND(coalesce(SUM(itemtag.netweight),0),$round) as completednetweight,
							ROUND(coalesce(SUM(itemtag.caratweight),0),2) as completedcaratweight,
							ROUND(coalesce(SUM(itemtag.pieces),0),0) as completedpieces,category.categoryname,lot.stoneapplicable,lot.lottypeid
							FROM lot
							LEFT JOIN purity ON purity.purityid = lot.purityid
							LEFT JOIN category ON category.categoryid = lot.categoryid
							LEFT JOIN itemtag ON itemtag.lotid = lot.lotid AND (itemtag.status not in (0,3) or itemtag.status is null)
							WHERE $wh1 
							GROUP BY lot.lotid $limit
						");
		return $info;
	}	
	public function lotdropdown_close($lotid) //?issue
	{
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$wh1 = ' lot.status = 1 ';
		$limit = '';
		$wh2 = ' lot.lotid = '.$lotid. ' ';
		$defaultstatus = 3;
		$activestatus = $this->Basefunctions->activestatus;
		$info = $this->db->query("
				SELECT lot.lotid,lotnumber,lotdate,lot.productid,lot.purityid,purity.purityname,lot.accountid,lot.categoryid,lot.salesdetailid,lot.billrefno,lot.receiveorder,
				ROUND((lot.grossweight - coalesce(SUM(itemtag.grossweight),0)),$round) as pendinggrossweight,
				ROUND((lot.netweight - coalesce(SUM(itemtag.netweight),0)),$round) as pendingnetweight,
				ROUND((lot.caratweight - coalesce(SUM(itemtag.caratweight),0)),2) as pendingcaratweight,
				ROUND((lot.pieces - coalesce(SUM(itemtag.netweight),0)),0) as pendingpieces,
				ROUND(lot.grossweight,$round) as totalgrossweight,
				ROUND(lot.netweight,$round) as totalnetweight,
				ROUND(lot.caratweight,2) as totalcaratweight,
				ROUND(lot.pieces,0) as totalpieces,
				ROUND(SUM(itemtag.grossweight),$round) as completedgrossweight,
				ROUND(SUM(itemtag.netweight),$round) as completednetweight,
				ROUND(SUM(itemtag.caratweight),2) as completedcaratweight,
				ROUND(SUM(itemtag.pieces),0) as completedpieces,category.categoryname,lot.stoneapplicable,lot.lottypeid
				FROM lot
				LEFT JOIN purity ON purity.purityid = lot.purityid
				LEFT JOIN category ON category.categoryid = lot.categoryid
				LEFT JOIN itemtag ON itemtag.lotid = lot.lotid AND (itemtag.status not in (0,3) or itemtag.status is null)
				WHERE $wh1 or $wh2
				GROUP BY lot.lotid $limit
				");
		return $info;
	}
	/*	*create itemtag	*/
	public function itemtagcreate() {
		$loclingtime = $this->Basefunctions->get_company_settings('lockingtime');
		$updatingtime = $this->Basefunctions->get_company_settings('updatingtime');
		$currenttime = time("H:i:s");
		$ctime = new DateTime();
		$ctime->setTimestamp($currenttime);
		$ltime  = new DateTime();
		if($loclingtime == '00:00:00') {
			$ltime->setTimestamp($currenttime);
		} else {
			$ltime->setTimestamp(strtotime($loclingtime));
		}
		if($ctime > $ltime) {
			$tagdate = date("Y-m-d",strtotime("tomorrow"));
			$tagdate = $this->Basefunctions->checkgivendateisholidayornot($tagdate);
		} else {
			$tagdate = $this->Basefunctions->ymd_format($_POST['date']);
		}
		$counter_license = $this->Basefunctions->get_company_settings('counter');	//counter license
		$lot_license = $this->Basefunctions->get_company_settings('lotdetail');	//lot license	
		$stock_notify = $this->Basefunctions->get_company_settings('stocknotify');	//stock notification
		$storagequantity = $this->Basefunctions->get_company_settings('storagequantity');
		$planid=$this->Basefunctions->planid;
		$tagtypeid = $_POST['tagtypeid'];
		$taggentagtype = array('2','4','5','6','12');
		$serialnumber = '';
		$chargecategoryid = array();
		$rfidtagno = '';
		$finalrfidtagno = '';
		$data = array();
		if($_POST['pieces'] == '' or $_POST['pieces'] == null) {
			$_POST['pieces'] = 1;			
		}
		if(!isset($_POST['tagtemplateid']) || empty($_POST['tagtemplateid']) || $_POST['tagtemplateid']=='null') {
			$_POST['tagtemplateid'] = 1;			
		}
		if(empty($_POST['melting'])) {
			$_POST['melting']=0.00;
		} else {
			$_POST['melting']=$_POST['melting'];
		}
		if(empty($_POST['touch'])) {
			$_POST['touch']=0;
		} else {
			$_POST['touch']=$_POST['touch'];
		}
		if(empty($_POST['rate'])) {
			$_POST['rate']=0;
		} else {
			$_POST['rate']=$_POST['rate'];
		}
		if(isset($_POST['rfidtagno'])) {
			$_POST['rfidtagno']=$_POST['rfidtagno'];
		} else {
			$_POST['rfidtagno']='';
		}
		if(isset($_POST['itemratewithgst'])) {
			$_POST['itemratewithgst']=$_POST['itemratewithgst'];
		} else {
			$_POST['itemratewithgst']='';
		}
		$pieces = 1;
		$tagpiece = $_POST['pieces'];
		if($tagtypeid == 4 || $tagtypeid == 5) {//for pricetag(4) and bulktag(5) pieces equivalent to no of tags.
			$pieces = $_POST['pieces'];
			$tagpiece = 1;
		}
		if(isset($_POST['itemrate'])) {
			$_POST['itemrate']=$_POST['itemrate'];
		} else {
			$_POST['itemrate']='';
		}
		if(isset($_POST['guaranteecard'])) {
			$_POST['guaranteecard']=$_POST['guaranteecard'];
		} else {
			$_POST['guaranteecard']='No';
		}
		if(isset($_POST['scaleoption'])) {
			$_POST['scaleoption']=$_POST['scaleoption'];
		} else {
			$_POST['scaleoption']='No';
		}
		if(empty($_POST['sizeid'])) {
			$_POST['sizeid']='1';
		} else {
			$_POST['sizeid']=$_POST['sizeid'];
		}
		if(empty($_POST['accountid'])) {
			$_POST['accountid']='1';
		} else {
			$_POST['accountid']=$_POST['accountid'];
		}
		if(empty($_POST['categoryid'])) {
			$_POST['categoryid']='1';
		} else {
			$_POST['categoryid']=$_POST['categoryid'];
		}
		if($_POST['categoryid'] == 5) {
			$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
			$_POST['grossweight'] = $_POST['itemcaratweight'];
			$_POST['netweight'] = ROUND(($_POST['itemcaratweight'] * 5),$round);
		}
		$tagentrytype = $_POST['tagentrytypeid'];
		if($tagentrytype == 4){        // tagentrytype -new entry  purchase counter
			$_POST['fromcounterid'] = 11;
			$processproductid = 3; // stock purchase productid
		} else if(in_array($tagentrytype,array(10,11,12,13))) { // sales return,old jewel,Partial tag & purchase return
			$processproductid = $_POST['processproductid'];
		}else
		{
			$processproductid = 1; 
		}
		$fromcounterid=$this->Basefunctions->checkintegervalue($_POST['fromcounterid'],1);
		// if counter is disable
		if((strtolower($counter_license) == 'yes')) {
			$counterid=$this->Basefunctions->checkintegervalue($_POST['counterid'],1);
			$stockincounterid=$this->Basefunctions->checkintegervalue($_POST['counterid'],1);
		} else {
			$counterid=11;
			$stockincounterid=11;
		}
		if($lot_license=='YES') {
			if(empty($_POST['lotid']) || $_POST['lotid'] == '1' || $_POST['lotid'] == 'null') {
				$lotid=0;
				$ordernumber = '';
				$salesdetailid = '1';
				$orderstatus = 'No';
			} else {
				if($_POST['receiveorderstatus'] == 'Yes') {
					$lotid=$_POST['lotid'];
					$ordernumber = $_POST['salesdetailordernumberv'];
					$salesdetailid = $_POST['salesdetailidv'];
					$orderstatus = $_POST['receiveorderstatus'];
				} else {
					$lotid=$_POST['lotid'];
					$ordernumber = '';
					$salesdetailid = '1';
					$orderstatus = 'No';
				}
			}
		} else {
			$lotid=0;
			$ordernumber = '';
			$salesdetailid = '1';
			$orderstatus = 'No';
		}
		if(!empty($_POST['chargeiddata'])){
				$charegiddata = explode(',',$_POST['chargeiddata']);
				for($k=0;$k<count($charegiddata);$k++) {
					$chargedetails = '';
					$chargedetails = $this->db->select('chargecategoryid')->from('charge')->where_in('chargeid',$charegiddata[$k])->get();
					foreach($chargedetails->result() as $info){
						$chargecategoryid[$k]= $info->chargecategoryid;
					}
				}
				$chargecategoryid = implode(',',$chargecategoryid);
		}else{
			$chargecategoryid = '';
		}
		$barcodeoption= $this->Basefunctions->get_company_settings('barcodeoption');
		if($barcodeoption == '2' || $barcodeoption == '3'){
			if($_POST['rfidtagno'] == ''){
				$splitrfidtagno = '';
			}else{
				$splitrfidtagno = explode(",",$_POST['rfidtagno']);
			}
		}else{
			$splitrfidtagno = '';
		}
		$tagnumber = '';
		$tagno = '';
		for($ab = 0;$ab < $pieces;$ab++) {
			if(in_array($tagtypeid,$taggentagtype )) {
				$serialnumber = $this->Basefunctions->varrandomnumbergenerator(50,'itemtagnumber',$tagtypeid,''); //serialnumber
			}
			$tagnumber = $this->Basefunctions->singlefieldfetch('itemtagnumber','itemtagnumber','itemtag',$serialnumber);
			if($tagnumber == 1){
				$tagno = $serialnumber;
			}else {
				if (in_array($tagtypeid,$taggentagtype )) {
					$tagno = $this->Basefunctions->varrandomnumbergenerator(50,'itemtagnumber',$tagtypeid,''); //serialnumber
				}
			}
			if($barcodeoption == '2' || $barcodeoption == '3'){
				if($splitrfidtagno != '') {
					$finalrfidtagno = $splitrfidtagno[$ab];
					$finalrfidtag = (string)$finalrfidtagno;
					$rfidlength = strlen($finalrfidtag);
					$finalrfidtagno = str_pad($finalrfidtag,$rfidlength, '0', STR_PAD_LEFT);
				}
			}else{
				$finalrfidtagno = '';
			}
			if($_POST['tagautoimage'] == 1) {
				$_POST['tagimage'] = 'uploads/tag/'.$tagno.'.png';
			}
			if($_POST['itemcaratweight'] > 0) {
				$_POST['grossweight'] = 0;
				$_POST['netweight'] = 0;
			} else {
				$_POST['itemcaratweight'] = 0;
			}
			$insert = array(
				'itemtagnumber' => $tagno,
				'tagdate' => $tagdate,
				'tagtypeid' => $tagtypeid,
				'lotid' =>$lotid,
				'purityid' => $this->Basefunctions->checkintegervalue($_POST['purityid'],1) ,
				'accountid' => $this->Basefunctions->checkintegervalue($_POST['accountid'],1),
				'productid' =>$this->Basefunctions->checkintegervalue($_POST['productid'],1),
				'grossweight' => $_POST['grossweight'],
				'stoneweight' => $_POST['stoneweight'],
				'netweight' => $_POST['netweight'],
				'caratweight' => $_POST['itemcaratweight'],
				'pieces' => $tagpiece,
				'size' => $_POST['sizeid'],
				'counterid' =>$counterid ,
				'stockincounterid' =>$stockincounterid,
				'fromcounterid' => $fromcounterid,
				'description' => $_POST['description'],
				'tagimage' => $_POST['tagimage'],
				'tagtemplateid' => $_POST['tagtemplateid'],
				'status'=>$this->Basefunctions->activestatus,
				'melting' => $_POST['melting'],
				'touch' => $_POST['touch'],
				'rate' => $_POST['rate'],
				'ordernumber' =>$ordernumber,
				'salesdetailid' =>$salesdetailid,
				'orderstatus' =>$orderstatus,
				'transfertagno'=>1,
				'tagentrytypeid' =>$_POST['tagentrytypeid'],
	            'rfidtagno'=>$finalrfidtagno,
				'itemrate'=>$_POST['itemrate'],
				'itemratewithgst'=>$_POST['itemratewithgst'],
				'industryid'=>$this->Basefunctions->industryid,
				'branchid'=>$_POST['branchid'],
				'certificationno'=>$_POST['certificationno'],
				'hallmarkno'=>$_POST['hallmarkno'],
				'hallmarkcenter'=>$_POST['hallmarkcenter'],
				'guaranteecard'=>$_POST['guaranteecard'],
				'scaleoption'=>$_POST['scaleoption'],
				'chargedetails'=>$_POST['chargedata'],
				'chargeid'=>$_POST['chargeiddata'],
				'chargecategoryid'=>$chargecategoryid,
				'chargeamount'=>$_POST['chargevalues'],
				'chargespandetails'=>$_POST['chargespan'],
				'chargespanamount'=>$_POST['chargespanvalue'],	
				'chargeapply'=>$_POST['chargeapply'],	
				'processproductid'=>$processproductid,
				'tagweight'=>$_POST['tagweight'],
				'packingweight'=>$_POST['packingweight'],
				'packwtless'=>$_POST['packwtless'],
				'productaddoninfoid'=>$_POST['productaddoninfoid'],
				'returnsalesdetailid'=>$_POST['returnsalesdetailid']
			);
			if($ctime > $ltime) {
				$insert = array_merge($insert,$this->Crudmodel->nextdaydefaultvalueget());
			} else {
				$insert = array_merge($insert,$this->Crudmodel->defaultvalueget());
			}
			$this->db->insert('itemtag',array_filter($insert));
			$primaryid = $this->db->insert_id();
			// Loose Stone Update
			if($_POST['itemcaratweight'] > 0) {
				$loosestonectwtupdate = array(
					'diacaratweight' => $_POST['itemcaratweight'],
					'diapieces' => $tagpiece
				);
			} else {
				$loosestonectwtupdate = array(
					'diacaratweight' => 0,
					'diapieces' => 0
				);
			}
			$this->db->where('itemtagid',$primaryid);
			$this->db->update('itemtag',$loosestonectwtupdate);
			//stock report data entry
			$purityid = $this->Basefunctions->checkintegervalue($_POST['purityid'],1);
			$productid = $this->Basefunctions->checkintegervalue($_POST['productid'],1);
			$tagentrytype = $_POST['tagentrytypeid'];
			$branchid = $this->Basefunctions->branchid;
			//stock table update
			//$this->Basefunctions->stocktableentryfunction($primaryid,'50');
			//audit-log
			$userid = $this->Basefunctions->logemployeeid;
			$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
			$tagtypename = $this->Basefunctions->singlefieldfetch('tagtypename','tagtypeid','tagtype',$tagtypeid);
			$activity = ''.$user.' created Itemtag of '.$tagtypename.' - '.$serialnumber.'';
			$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,$this->itemtagmodule);
			// Stone Entry => Insert
			$stoneentry=json_decode($_REQUEST['hiddenstonedata']);
			if(!empty($stoneentry)){
				usort($stoneentry,function($a,$b){
						return $a->stonegroupid - $b->stonegroupid;
				});
				$stoneorder = 0;	
				foreach($stoneentry as $row) {
					$stoneinsert = array(
						'stoneentryorder' => $stoneorder,
						'stonetypeid' => $row->stonegroupid,
						'itemtagid' => $primaryid,
						'stoneid' => $row->stoneid,
						'purchaserate' =>$row->purchaserate,
						'stonerate' =>$row->basicrate,
						'pieces' => $row->stonepieces,	
						'caratweight' => $row->caratweight,
						'gram' => $row->stonegram,
						'totalcaratweight' => $row->totalcaratweight,						
						'totalnetweight' =>$row->totalweight,
						'totalamount' => $row->stonetotalamount,
						'stoneentrycalctypeid'=>$row->stoneentrycalctypeid,
						'status'=>$this->Basefunctions->activestatus
					);
					if($ctime > $ltime) {
						$stoneinsert = array_merge($stoneinsert,$this->Crudmodel->nextdaydefaultvalueget());
					} else {
						$stoneinsert = array_merge($stoneinsert,$this->Crudmodel->defaultvalueget());
					}
					$this->db->insert('stoneentry',$stoneinsert);
					$stoneorder++;		
				}
				// Update Caratweight and Pieces separately for Diamond and Other Stones by Kumaresan
				$diamondcaratweight = 0.00;
				$diamondpieces = 0;
				$stonecaratweight = 0.00;
				$stone_pieces = 0;
				foreach($stoneentry as $row) {
					if($row->stonegroupid == 2) {
						$diamondcaratweight = number_format((float)$row->caratweight + $diamondcaratweight, 2, '.', '');
						$diamondpieces = number_format((int)$row->stonepieces + $diamondpieces, 0, '.', '');
					} else if($row->stonegroupid != 2) {
						$stonecaratweight = number_format((float)$row->caratweight + $stonecaratweight, 2, '.', '');
						$stone_pieces = number_format((int)$row->stonepieces + $stone_pieces, 0, '.', '');
					}
				}
				$itemtagdiamonddetailsupdate = array(
					'diacaratweight' => $diamondcaratweight,
					'diapieces' => $diamondpieces,
					'stonecaratweight' => $stonecaratweight,
					'stonepieces' => $stone_pieces
				);
				$itemtagdiamonddetailsupdate = array_merge($itemtagdiamonddetailsupdate,$this->Crudmodel->updatedefaultvalueget());
				$this->db->where('itemtagid',$primaryid);
				$this->db->update('itemtag',$itemtagdiamonddetailsupdate);
			}
			//**auto lot closing**//
			//verify lot license
			if($lot_license == "YES" && $tagtypeid !=6 && $lotid != '') {
				$this->Lot_model->autolotopenclose($lotid);
			} 
			//printing
			if(isset($_POST['tagtemplateid']) && $tagtypeid !=3) {		
				$printtemplate = trim($_POST['tagtemplateid']);
				if($tagtypeid != 3 AND $printtemplate > 1) {	
					$print_data=array('templateid'=> $printtemplate, 'Templatetype'=>'Tagtemp','primaryset'=>'itemtag.itemtagid','primaryid'=>$primaryid);
					$data = $this->Printtemplatesmodel->generatlabelprint($print_data);
				}			
			} else {
			}
		}
		if($stock_notify == 'YES') {
			$result['msg'] = 'SUCCESS';
			$result['itemtagno'] = $tagno;
			$result['rfidno'] = $finalrfidtagno;
			$result['tagnumber'] = $this->Basefunctions->singlefieldfetch('itemtagnumber','itemtagid','itemtag',$primaryid);
			$result['dataurlimg'] = $_POST['tagimage'];
		} else {
			$result['msg'] = 'SUCCESS';
			$result['dataurlimg'] = $_POST['tagimage'];
			$result['itemtagno'] = $tagno;
			$result['rfidno'] = $finalrfidtagno;
		}
		if($lot_license=='YES') {
			if(!empty($_POST['lotid']) || $_POST['lotid'] != '1' || $_POST['lotid'] != 'null') {
				if($_POST['receiveorderstatus'] == 'Yes') {
					//itemtag update in salesdetail table
					$itemtagupdate = array(
											'itemtagid' => $primaryid
										);
					$itemtagupdate = array_merge($itemtagupdate,$this->Crudmodel->updatedefaultvalueget());
					$this->db->where('salesdetailid',$_POST['salesdetailidv']);
					$this->db->update('salesdetail',$itemtagupdate);
				}
			}
		}
		if($_POST['returnsalesdetailid'] != 1) {
			$returnitemtagidupdate = array(
				'taggeditemtagid' => $primaryid
			);
			$returnitemtagidupdate = array_merge($returnitemtagidupdate,$this->Crudmodel->updatedefaultvalueget());
			$this->db->where('salesdetailid',$_POST['returnsalesdetailid']);
			$this->db->update('salesdetail',$returnitemtagidupdate);
		}
		echo json_encode($result);
	}
	/**	*retrieve itemtag data with additionalcharge data**/
	public function itemtagretrive() {
		$itemtagid = $_GET['primaryid'];
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //weight round-off
		$meltround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',4); //melting round-off
		$this->db->select(
						'itemtag.itemtagid,itemtag.itemtagnumber,DATE_FORMAT(tagdate,"%d-%m-%Y") AS tagdate,itemtag.lotid,	 itemtag.purityid,itemtag.accountid,itemtag.fromcounterid,itemtag.counterid,itemtag.tagtypeid,itemtag.productid,
						ROUND(itemtag.grossweight,'.$round.') as grossweight,
						ROUND(itemtag.stoneweight,'.$round.') as stoneweight,
						ROUND(itemtag.netweight,'.$round.') as netweight,
						ROUND(itemtag.caratweight,'.$round.') as itemcaratweight,
						ROUND(itemtag.melting,'.$meltround.') as melting,						itemtag.pieces,itemtag.branchid,itemtag.size,itemtag.description,itemtag.tagimage,itemtag.status,status.statusname,itemtag.tagtemplateid,ROUND(itemtag.touch,'.$meltround.') as touch,ROUND(itemtag.rate,'.$amountround.') as rate,itemtag.tagentrytypeid,itemtag.rfidtagno,ROUND(itemtag.itemrate,'.$amountround.') as itemrate,itemtag.branchid,itemtag.certificationno,itemtag.hallmarkno,itemtag.hallmarkcenter,itemtag.guaranteecard,itemtag.scaleoption,itemtag.chargeid,itemtag.chargedetails,itemtag.chargespandetails,itemtag.processproductid,ROUND(itemtag.itemratewithgst,'.$amountround.') as itemratewithgst,ROUND(itemtag.tagweight,'.$round.') as tagweight,ROUND(itemtag.packingweight,'.$round.') as packingweight,itemtag.packwtless,itemtag.chargeapply,itemtag.productaddoninfoid',false);
		$this->db->from('itemtag');	
		$this->db->join('status','status.status=itemtag.status');	
		$this->db->where('itemtag.itemtagid',$itemtagid);
		$this->db->limit(1);
		$info = $this->db->get()->row();
		$edittagcharge = $this->Basefunctions->get_company_settings('edittagcharge');
		if($edittagcharge == 0) { // load charge from productaddon
			$chargeaccountgroup = $this->Basefunctions->get_company_settings('chargeaccountgroup');
			$previouschargearray = $this->loadunchangeproductcharge($info->purityid,$info->productid,$info->netweight,$chargeaccountgroup);
			$chargedetails = $previouschargearray['chargedetails'];
			$chargeid = $previouschargearray['chargeid'];
			$chargespandetails = $previouschargearray['chargespandetails'];
			$previousdata = '1';
		}else{
			$chargeid = $info->chargeid;
			$chargedetails = $info->chargedetails;
			$chargespandetails = $info->chargespandetails;
			$previousdata = '0';
		}
		$tagjsonarray = array(
				'itemtagid' => $info->itemtagid,
				'tagtypeid' => $info->tagtypeid,
				'itemtagnumber' => $info->itemtagnumber,							
				'date'=> $info->tagdate,
				'purityid'=> $info->purityid,
				'accountid'=> $info->accountid,
				'productid'=> $info->productid,
				'grossweight' => $info->grossweight,
				'stoneweight' => $info->stoneweight,
				'netweight' => $info->netweight,
				'itemcaratweight' => $info->itemcaratweight,
				'pieces' => $info->pieces,
				'size' => $info->size,
				'counterid' => $info->counterid,
				'fromcounterid' => $info->fromcounterid,
				'lotid' => $info->lotid,
				'description' => $info->description,
				'tagimage' => $info->tagimage,
				'tagtemplateid' => $info->tagtemplateid,
				'tagstatusid' => $info->status,
				'tagstatusname' => $info->statusname,
				'melting' => $info->melting,
				'touch' => $info->touch,
				'rate' => $info->rate,
				'tagentrytypeid'=>$info->tagentrytypeid,
                'rfidtagno'=>$info->rfidtagno,
				'itemrate'=>$info->itemrate,
				'itemratewithgst'=>$info->itemratewithgst,
				'branchid'=>$info->branchid,
				'certificationno'=>$info->certificationno,
				'hallmarkno'=>$info->hallmarkno,
				'hallmarkcenter'=>$info->hallmarkcenter,
				'guaranteecard'=>$info->guaranteecard,
				'scaleoption'=>$info->scaleoption,
				'chargeid'=>$chargeid,
				'chargedetails'=>$chargedetails,
				'chargespandetails'=>$chargespandetails,
				'previousdata'=>$previousdata,
				'processproductid'=>$info->processproductid,
				'tagweight'=>$info->tagweight,
				'packingweight'=>$info->packingweight,
				'packwtless'=>$info->packwtless,
				'chargeapply'=>$info->chargeapply,
				'productaddoninfoid'=>$info->productaddoninfoid
		    );
		$tagjsonarray['stoneentry'] = $this->retrive_itemtag_stoneentry($itemtagid);
		$chargeid =  $this->Basefunctions->generalinformaion('product','chargeid','productid',$info->productid);
		$data=$this->db->query("select GROUP_CONCAT(chargecategoryid,':',chargename,':',chargekeyword,':',chargeid) as chargekeyword
				FROM charge
				WHERE chargeid IN ($chargeid)"
				);
		foreach($data->result() as $info) {
			$tagjsonarray['keyword']= array(
					'chargekeyword'=>$info->chargekeyword
			);
		}
		echo json_encode($tagjsonarray);
	}
	/**
	*update itemtag data
	*/
	public function itemtagupdate($itemtagid) {	
		$loclingtime = $this->Basefunctions->get_company_settings('lockingtime');
		$updatingtime = $this->Basefunctions->get_company_settings('updatingtime');
		$currenttime = time("H:i:s");
		$ctime = new DateTime();
		$ctime->setTimestamp($currenttime);
		$ltime  = new DateTime();
		if($loclingtime == '00:00:00') {
			$ltime->setTimestamp($currenttime);
		} else {
			$ltime->setTimestamp(strtotime($loclingtime));
		}
		/* if($ctime > $ltime) {
			$tagdate = date("Y-m-d",strtotime("tomorrow"));
			$tagdate = $this->Basefunctions->checkgivendateisholidayornot($tagdate);
		} else {
			$tagdate = $this->Basefunctions->ymd_format($_POST['date']);
		} */
		$tagtypeid = $_POST['tagtypeid'];
		$counter_license = $this->Basefunctions->get_company_settings('counter');	//counter license
		$lot_license = $this->Basefunctions->get_company_settings('lotdetail');	//lot license
		$planid=$this->Basefunctions->planid;
		$chargecategoryid = array();
		//stock entry update
		//$this->Basefunctions->stockupdateondelete($itemtagid,'50');
		// lot id change 
		$changelotid=$_POST['changelotid'];
		$changegrosswt=$_POST['changegrosswt'];
		$changenetwt=$_POST['changenetwt'];
		if($lot_license=='YES') {
			if(empty($_POST['lotid']) || $_POST['lotid'] == 'null') {
				$lotid=0;
			} else {
				$lotid=$_POST['lotid'];
			}
		} else {
			$lotid=0;
		} 
		if(!isset($_POST['fromcounterid'])){
			$_POST['fromcounterid']='';
		}
		if(!isset($_POST['accountid'])){
			$_POST['accountid']='1';
		}
		if($_POST['pieces'] == '' or $_POST['pieces'] == null) {
			$_POST['pieces'] = 1;			
		}
		if($tagtypeid == 4 || $tagtypeid == 5) {// for price tag and bulk tag pieces entry is always 1
			$_POST['pieces'] = '1';
		}
		if(empty($_POST['melting'])) {
			$_POST['melting']=0.00;
		} else {
			$_POST['melting']=$_POST['melting'];
		}
		if(empty($_POST['touch'])) {
			$_POST['touch']=0;
		} else {
			$_POST['touch']=$_POST['touch'];
		}
		if(empty($_POST['rate'])) {
			$_POST['rate']=0;
		} else {
			$_POST['rate']=$_POST['rate'];
		}
		if(isset($_POST['rfidtagno'])) {
			$_POST['rfidtagno']=$_POST['rfidtagno'];
		} else {
			$_POST['rfidtagno']='';
		}
		if(isset($_POST['itemrate'])) {
			$_POST['itemrate']=$_POST['itemrate'];
		} else {
			$_POST['itemrate']='';
		}
		if(isset($_POST['itemratewithgst'])) {
			$_POST['itemratewithgst']=$_POST['itemratewithgst'];
		} else {
			$_POST['itemratewithgst']='';
		}
		if(isset($_POST['guaranteecard'])) {
			$_POST['guaranteecard']=$_POST['guaranteecard'];
		} else {
			$_POST['guaranteecard']='No';
		}
		if(isset($_POST['scaleoption'])) {
			$_POST['scaleoption']=$_POST['scaleoption'];
		} else {
			$_POST['scaleoption']='No';
		}
		if(isset($_POST['tagtemplateid'])) {
			$_POST['tagtemplateid']=$_POST['tagtemplateid'];
		} else {
			$_POST['tagtemplateid']='1';
		}
		if(empty($_POST['sizeid'])) {
			$_POST['sizeid']='1';
		} else {
			$_POST['sizeid']=$_POST['sizeid'];
		}
		if(empty($_POST['accountid'])) {
			$_POST['accountid']='1';
		} else {
			$_POST['accountid']=$_POST['accountid'];
		}
		if(empty($_POST['categoryid'])) {
			$_POST['categoryid']='1';
		} else {
			$_POST['categoryid']=$_POST['categoryid'];
		}
		if($_POST['categoryid'] == 5) {
			$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
			$_POST['grossweight'] = $_POST['itemcaratweight'];
			$_POST['netweight'] = ROUND(($_POST['itemcaratweight'] * 5),$round);
		}
		$fromcounterid=$this->Basefunctions->checkintegervalue($_POST['fromcounterid'],1);
		// if counter is disable
		if($counter_license == 'YES') {
			$counterid=$this->Basefunctions->checkintegervalue($_POST['counterid'],1);
			$stockincounterid=$this->Basefunctions->checkintegervalue($_POST['counterid'],1);
		} else {
			$counterid=11;
			$stockincounterid=11;
		}
		if(!empty($_POST['chargeiddata'])){
				$charegiddata = explode(',',$_POST['chargeiddata']);
				for($k=0;$k<count($charegiddata);$k++) {
					$chargedetails = '';
					$chargedetails = $this->db->select('chargecategoryid')->from('charge')->where_in('chargeid',$charegiddata[$k])->get();
					foreach($chargedetails->result() as $info){
						$chargecategoryid[$k]= $info->chargecategoryid;
					}
				}
				$chargecategoryid = implode(',',$chargecategoryid);
			} else {
				$chargecategoryid = '';
			}
		$branchid = $this->Basefunctions->branchid;
		if($_POST['itemcaratweight'] > 0) {
			$_POST['grossweight'] = 0;
			$_POST['netweight'] = 0;
		} else {
			$_POST['itemcaratweight'] = 0;
		}
		$update=array(					
			'purityid' => $this->Basefunctions->checkintegervalue($_POST['purityid'],1) ,						
			'accountid' => $this->Basefunctions->checkintegervalue($_POST['accountid'],1),						
			'productid' =>$this->Basefunctions->checkintegervalue($_POST['productid'],1),
			'grossweight' => $_POST['grossweight'],					
			'stoneweight' => $_POST['stoneweight'],					
			'netweight' => $_POST['netweight'],
			'caratweight' => $_POST['itemcaratweight'],
			'pieces' => $_POST['pieces'],	
			'size' => $_POST['sizeid'],					
			'counterid' =>$counterid,	
			'stockincounterid' =>$stockincounterid,			
			'fromcounterid' => $fromcounterid,					
			'description' => $_POST['description'],
			'tagimage' => $_POST['tagimage'],
			'melting' => $_POST['melting'],
			'touch' => $_POST['touch'],
			'rate' => $_POST['rate'],
			'tagtemplateid' => $_POST['tagtemplateid'],	
			'tagentrytypeid' =>$_POST['tagentrytypeid'],
	        'lotid'=>$lotid,
	        'rfidtagno'=>$_POST['rfidtagno'],
			'itemrate'=>$_POST['itemrate'],
			'itemratewithgst'=>$_POST['itemratewithgst'],
			'industryid'=>$this->Basefunctions->industryid,
			'branchid'=>$_POST['branchid'],
			'certificationno'=>$_POST['certificationno'],
			'hallmarkno'=>$_POST['hallmarkno'],
			'hallmarkcenter'=>$_POST['hallmarkcenter'],
			'guaranteecard'=>$_POST['guaranteecard'],
			'scaleoption'=>$_POST['scaleoption'],
			'chargedetails'=>$_POST['chargedata'],
			'chargeid'=>$_POST['chargeiddata'],
			'chargeamount'=>$_POST['chargevalues'],
			'chargespandetails'=>$_POST['chargespan'],
			'chargespanamount'=>$_POST['chargespanvalue'],
			'chargeapply'=>$_POST['chargeapply'],
			'productaddoninfoid'=>$_POST['productaddoninfoid'],
			'chargecategoryid'=>$chargecategoryid
		);
		if($ctime > $ltime) {
			$update=array_merge($update,$this->Crudmodel->nextdayupdatedefaultvalueget());
		} else {
			$update=array_merge($update,$this->Crudmodel->updatedefaultvalueget());
		}
	    $this->db->where('itemtagid',$itemtagid);
		$this->db->update('itemtag',$update);
		// Loose Stone Update
		if($_POST['itemcaratweight'] > 0) {
			$loosestonectwtupdate = array(
				'diacaratweight' => $_POST['itemcaratweight'],
				'diapieces' => $_POST['pieces']
			);
		} else {
			$loosestonectwtupdate = array(
				'diacaratweight' => 0,
				'diapieces' => 0
			);
		}
		$this->db->where('itemtagid',$itemtagid);
		$this->db->update('itemtag',$loosestonectwtupdate);
		//stock report data entry
		$purityid = $this->Basefunctions->checkintegervalue($_POST['purityid'],1);
		$productid = $this->Basefunctions->checkintegervalue($_POST['productid'],1);
		$tagentrytype = $_POST['tagentrytypeid'];
		//stock table update
		//$this->Basefunctions->stocktableentryfunction($itemtagid,'50');
		$delete = $this->Basefunctions->delete_log();
		$this->db->where('itemtagid',$itemtagid);
		$this->db->update('stoneentry',$delete);
		$stoneentry=json_decode($_REQUEST['hiddenstonedata']);
		if(!empty($stoneentry)){
			usort($stoneentry,function($a,$b){
				return $a->stonegroupid - $b->stonegroupid;
			});
			$stoneorder = 0;	
			foreach($stoneentry as $row) {
				$stoneinsert = array(
					'stoneentryorder' => $stoneorder,
					'stonetypeid' => $row->stonegroupid,
					'itemtagid' => $itemtagid,
					'stoneid' => $row->stoneid,
					'purchaserate' =>$row->purchaserate,
					'stonerate' =>$row->basicrate,
					'pieces' => $row->stonepieces,	
					'caratweight' => $row->caratweight,
					'gram' => $row->stonegram,
					'totalcaratweight' => $row->totalcaratweight,						
					'totalnetweight' =>$row->totalweight,
					'totalamount' => $row->stonetotalamount,
					'stoneentrycalctypeid'=>$row->stoneentrycalctypeid,
					'status'=>$this->Basefunctions->activestatus
				);
				if($ctime > $ltime) {
					$stoneinsert = array_merge($stoneinsert,$this->Crudmodel->nextdaydefaultvalueget());
				} else {
					$stoneinsert = array_merge($stoneinsert,$this->Crudmodel->defaultvalueget());
				}
				$this->db->insert('stoneentry',$stoneinsert);
				$stoneorder++;				
			}
			// Update Caratweight and Pieces separately for Diamond and Other Stones by Kumaresan
			$diamondcaratweight = 0.00;
			$diamondpieces = 0;
			$stonecaratweight = 0.00;
			$stone_pieces = 0;
			foreach($stoneentry as $row) {
				if($row->stonegroupid == 2) {
					$diamondcaratweight = number_format((float)$row->caratweight + $diamondcaratweight, 2, '.', '');
					$diamondpieces = number_format((int)$row->stonepieces + $diamondpieces, 0, '.', '');
				} else if($row->stonegroupid != 2) {
					$stonecaratweight = number_format((float)$row->caratweight + $stonecaratweight, 2, '.', '');
					$stone_pieces = number_format((int)$row->stonepieces + $stone_pieces, 0, '.', '');
				}
			}
			$itemtagdiamonddetailsupdate = array(
				'diacaratweight' => $diamondcaratweight,
				'diapieces' => $diamondpieces,
				'stonecaratweight' => $stonecaratweight,
				'stonepieces' => $stone_pieces
			);
			$itemtagdiamonddetailsupdate = array_merge($itemtagdiamonddetailsupdate,$this->Crudmodel->updatedefaultvalueget());
			$this->db->where('itemtagid',$itemtagid);
			$this->db->update('itemtag',$itemtagdiamonddetailsupdate);
		}
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$tagtypeid = $this->Basefunctions->singlefieldfetch('tagtypeid','itemtagid','itemtag',$itemtagid);
		$tagtypename = $this->Basefunctions->singlefieldfetch('tagtypename','tagtypeid','tagtype',$tagtypeid);
		$itemtagnumber = $this->Basefunctions->singlefieldfetch('itemtagnumber','itemtagid','itemtag',$itemtagid);
		$activity = ''.$user.' created Itemtag of '.$tagtypename.' - '.$itemtagnumber.'';
		$this->Basefunctions->notificationcontentadd($itemtagid,'Added',$activity ,$userid,$this->itemtagmodule);
		//lot functionality
		$this->lottag($itemtagid);
		//**auto lot closing**//
		//verify lot license
		if($lot_license == "YES" && $tagtypeid !=5 && $tagtypeid !=6 && $lotid != '') {
			$this->Lot_model->autolotopenclose($lotid);
		}
		// lot id change for tag 
		if($changelotid == $lotid) {
		} else {
			$lot_status = $this->Basefunctions->singlefieldfetch('status','lotid','lot',$changelotid);
			if($lot_status == 4){
				$update_lotstatus = array('status' =>1);
				$this->db->where('lotid',$changelotid);
				$this->db->update('lot',$update_lotstatus);
			} else {
			}
		}
		echo 'SUCCESS';
	}	
	/**
	*inactivate-delete itemtag grid	
	*/
	public function itemtagdelete() {
		$id=$_GET['primaryid'];
		$tagtype = $this->db->where('itemtagid',$id)->get('itemtag')->row()->tagtypeid;
		$tagentrytypeid = $this->db->where('itemtagid',$id)->get('itemtag')->row()->tagentrytypeid;
		$itemtagnumber = $this->db->where('itemtagid',$id)->get('itemtag')->row()->itemtagnumber; 
		$delete = $this->Basefunctions->delete_log();
		if($id != '' && $id != '1' ){	
		/*
		code commented by vishal - by default no transfer entry can be deleted. so commented below code
		$deletestatus = 0;
		$this->db->select('itemtagid');
		$this->db->from('itemtag');
		$this->db->where_in('tagtypeid',array(11,13,14));
		$this->db->where('transfertagno',$itemtagnumber);
		$this->db->where('transfertagno !=','');
		$this->db->where('status',$this->Basefunctions->activestatus);
		$data = $this->db->get();
		if($data->num_rows() > 0) {
			$deletestatus = 1;
		} 			
		
		//stock entry update
		//$this->Basefunctions->stockupdateondelete($id,'50');
		if($deletestatus == 1) {// other counter and tag type
			echo 'Tag transfer entry to "other counter" cannot be deleted.Kindly do new transfer entry.';
		}else if(($tagtype == 11 || $tagtype == 13) && $tagentrytypeid == 2) {// tag transfer old jewel & purcase return
		//if($tagtype != 4) {// other counter and tag type
			echo 'Tag transfer entry to either "old jewel or purchase return counter" cannot be deleted.Kindly do new transfer entry.';
		} 
		
		
		if (in_array($tagentrytypeid,array('2','3','5','6','7'))){
			echo 'Transfer entry cannot be deleted.Kindly do new transfer entry. If Its other type transfer then do Re-tagging of the item from stock entry add using appropriate from type.';
		}
		else {
		*/
			// Return tag id check and update
			$returnsalesdetailid = $this->Basefunctions->singlefieldfetch('returnsalesdetailid','itemtagid','itemtag',$id);
			if($returnsalesdetailid > 1) {
				$returnitemtagidupdate = array(
					'taggeditemtagid' => '1'
				);
				$returnitemtagidupdate = array_merge($returnitemtagidupdate,$this->Crudmodel->updatedefaultvalueget());
				$this->db->where('salesdetailid',$returnsalesdetailid);
				$this->db->update('salesdetail',$returnitemtagidupdate);
			}
			//Stone Entry => delete itemtag-main 
			$this->db->where('itemtagid',$id);
			$this->db->update('stoneentry',$delete);
			//split tag entry delete - if available
			$splitid = $this->Basefunctions->generalinformaion('itemtag','splittagid','itemtagid',$id);
			if($splitid > 1) {
				$this->db->where('splittagid',$splitid);
				$this->db->update('splittag',$delete);
			}
			//delete itemtag-main
			$this->db->where('itemtagid',$id);
			$this->db->update('itemtag',$delete);
			//delete all transfer entry for this tag itemtag-main
			
			$this->db->where('transfertagno',$itemtagnumber);
			$this->db->where('status',$this->Basefunctions->activestatus);
			$this->db->update('itemtag',$delete);
			
		    //audit-log
			$tagtypename = $this->Basefunctions->singlefieldfetch('tagtypename','tagtypeid','tagtype',$tagtype);
			$userid = $this->Basefunctions->logemployeeid;
			$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
			$activity = ''.$user.' deleted Itemtag of '.$tagtypename.' - '.$itemtagnumber.'';
			$this->Basefunctions->notificationcontentadd($id,'Updated',$activity ,$userid,$this->itemtagmodule);			
			//lot functionality
			$this->lottag($id);	
			echo 'SUCCESS';
		/* }	 */	
		}
		else{
			echo 'Id passed is empty';
		}
	}
	/**	*lot tag - delete(open/close)lot only for lot users	*/
	public function lottag($itemtagid)
	{
		$lot_license = $this->Basefunctions->get_company_settings('lotdetail'); //lot license
		if($lot_license == "YES")
		{
			//retrieve the tagtypeid
			$tagtype = $this->db->select('lotid')
								->from('itemtag')
								->where('itemtagid',$itemtagid)
								->limit(1)
								->get()
								->row();
			$lotid = $tagtype->lotid;
			if($lotid > 1)
			{
				$this->Lot_model->autolotopenclose($lotid);						
			}
		}
	}
	/**	*retrieve the Stone Entery details of an itemtag*/
	public function retrive_itemtag_stoneentry($itemtag)
	{
		//tag based charges
		$infotag = array();
		$infotagcharge = array();
		$this->db->select('se.stoneentryid,se.stoneid,stone.stonename,se.purchaserate,se.stonerate,se.pieces,se.caratweight,se.gram,se.totalcaratweight,se.totalnetweight,se.totalamount,stonetype.stonetypeid,stonetype.stonetypename,stoneentrycalctype.stoneentrycalctypeid,stoneentrycalctype.stoneentrycalctypename');
		$this->db->from('stoneentry as se');
		$this->db->join('stone','stone.stoneid = se.stoneid');
		$this->db->join('stonecategory','stonecategory.stonecategoryid = stone.stonecategoryid');
		$this->db->join('stonetype','stonetype.stonetypeid = stonecategory.stonetypeid');
		$this->db->join('stoneentrycalctype','stoneentrycalctype.stoneentrycalctypeid = se.stoneentrycalctypeid');
		$this->db->where('se.itemtagid',$itemtag);
		$this->db->where_in('se.status',array($this->Basefunctions->activestatus));
		$data = $this->db->get();
		if($data->num_rows()>0){
			foreach($data->result() as $row)
			{	
				$infotag[] = array(
									'itemtagid' =>$row->stoneid,
									'stonegroupid'=>$row->stonetypeid,
									'stoneidgroupname'=>$row->stonetypename,
									'stoneid' => $row->stoneid,
									'stoneidname'=>$row->stonename,
									'purchaserate' =>$row->purchaserate,
									'basicrate' =>$row->stonerate,
									'stoneentrycalctypeid' => $row->stoneentrycalctypeid,
									'stoneentrycalctypename'=>$row->stoneentrycalctypename,
									'stonepieces' => $row->pieces,	
									'caratweight' => $row->caratweight,
									'stonegram' => $row->gram,
									'totalcaratweight' => $row->totalcaratweight,						
									'totalweight' =>$row->totalnetweight,
									'stonetotalamount' => $row->totalamount
								);
						  
			}
			return json_encode($infotag);
		}else{
			return '';
		}
	}
	public function retrive_stoneentry_div()
	{
		//tag based charges
		$itemtag=$_POST['primaryid'];
		$infotag = array();
		$infotagcharge = array();
		$this->db->select('se.stoneentryid,se.stoneid,stone.stonename,se.purchaserate,se.stonerate,se.pieces,se.caratweight,se.gram,se.totalcaratweight,se.totalnetweight,se.totalamount,stonetype.stonetypeid,stonetype.stonetypename,stoneentrycalctype.stoneentrycalctypeid,stoneentrycalctype.stoneentrycalctypename');
		$this->db->from('stoneentry as se');
		$this->db->join('stone','stone.stoneid = se.stoneid');
		$this->db->join('stonecategory','stonecategory.stonecategoryid = stone.stonecategoryid');
		$this->db->join('stonetype','stonetype.stonetypeid = stonecategory.stonetypeid');
		$this->db->join('stoneentrycalctype','stoneentrycalctype.stoneentrycalctypeid = se.stoneentrycalctypeid');
		$this->db->where('se.itemtagid',$itemtag);
		$this->db->where_in('se.status',array($this->Basefunctions->activestatus));
		$data = $this->db->get();
		if($data->num_rows() > 0) {
			$j=1;
		foreach($data->result() as $value)
		{
			$stonedetail->rows[$j]['id'] = $j;
			$stonedetail->rows[$j]['cell']=array(
					$value->stonetypeid,
					$value->stonetypename,
					$value->stoneid,
					$value->stonename,
					$value->purchaserate,
					$value->stonerate,
					$value->stoneentrycalctypeid,
					$value->stoneentrycalctypename,
					$value->pieces,
					$value->caratweight,
					$value->gram,
					$value->totalcaratweight,
					$value->totalnetweight,
					$value->totalamount
			);
			$j++;	
		}
		}else{
			$stonedetail='';
		}
		echo json_encode($stonedetail);
	}
	/*	*retrieve the productsearch	*/
	public function productsearchview($sidx,$sord,$start,$limit,$wh)
	{   
		if(isset($_GET['purityid'])){
		$purityid=$_GET['purityid'];
		}
		$wh_search = ' 1=1 ';
		if($wh != "1")
		{
			$wh_search = $wh;
		}
		$m = 1;
		$category_level = $this->Basefunctions->get_company_settings('category_level'); //lot license
		$this->db->select('productid ,productname,product.shortname,product.purityid,product.categoryid,category.categorylevel,category.categoryname');
		$this->db->from('product');
		$this->db->join('category','category.categoryid =  product.categoryid and category.status=1');
		if(!empty($purityid)) {
			$this->db->where("FIND_IN_SET('$purityid',product.purityid) >", 0);
		}
		$this->db->where('product.bullionapplicable','No');
		$this->db->where_in('product.status',array($this->Basefunctions->activestatus));
		$this->db->order_by('product.productid','desc');
		$data = $this->db->get();
		return $data;							
	}
	/*	 *retrieve the Product based productsearch	 */
	public function productaddonsearchview($sidx,$sord,$start,$limit,$wh)
	{
		$wh_search = ' 1=1 ';
		if($wh != "1")
		{
			$wh_search = $wh;
		}
		$m = 1;
		$category_level = $this->Basefunctions->get_company_settings('category_level'); //lot license
		
		$this->db->select('productid ,productname,product.shortname,product.purityid,product.categoryid,category.categorylevel,category.categoryname');
		$this->db->from('product');
		$this->db->join('category','category.categoryid =  product.categoryid and category.status=1');
		$this->db->where('product.bullionapplicable','No');
		$this->db->where_in('product.status',array($this->Basefunctions->activestatus));
		$this->db->order_by('product.productid','desc');
		$data = $this->db->get();
		return $data;
	}
	/**	*	loadproductaddon-other	*/
	public function loadproductaddon($product,$purity,$weight)
	{	
		$this->db->select('productcharge.additionalchargeid,productcharge.accountgroupid,calculationby.calculationbyname,additionalcharge.additionalchargename,productcharge.calculationbyid,ROUND(productcharge.chargeamount,2) as value
		',false);
        $this->db->from('productcharge');
		$this->db->join('additionalcharge','additionalcharge.additionalchargeid=productcharge.additionalchargeid');
		$this->db->join('calculationby','calculationby.calculationbyid=productcharge.calculationbyid');
        $this->db->where('productcharge.productid',$product);
		$this->db->where("FIND_IN_SET('$purity',productcharge.purityid) >", 0);
		$this->db->where('startweight <=',$weight);
		$this->db->where('endweight >=',$weight);
		$this->db->where('productcharge.status',$this->Basefunctions->activestatus);
		$this->db->order_by('productcharge.productchargeid','desc');
		$this->db->group_by('productcharge.additionalchargeid');
        $productcharge = $this->db->get();
		return $productcharge;        
	}
	public function defaultprinttemplate()
	{
		$tagtemplateid = '';
		$data = $this->db->select('tagtemplateid')
					->from('tagtemplate')
					->where('default','Yes')
					->where('status',1)
					->where('moduleid',50)
					->order_by('lastupdatedate','desc')
					->get()
					->result();
		foreach($data as $info)
		{
			$tagtemplateid = $info->tagtemplateid;
		}
		return $tagtemplateid;
	}	
	public function sizeautosuggest($size)
	{		
		$data = $this->db->query("SELECT size FROM itemtag WHERE size LIKE '%".$size."%'
		GROUP BY size");		
		return $data;
	}
	/*	*itemtag ordering	*/
	public function orderitem()
	{
		$id = $_GET['id'];
		$taggentagtype = array('2','4','5','6');
		$type = $this->Basefunctions->singlefieldfetch('tagtypeid','itemtagid','itemtag',$id);
		if (in_array($type,$taggentagtype ))
		{
			$status = $this->Basefunctions->singlefieldfetch('status','itemtagid','itemtag',$id);
			if($status == 1){
				echo 'Yes';	
			} else {
				echo 'No';	
			}			
		}
		else
		{
			echo 'No';						
		}		
	}	
	// tag data retrieve
	public function retrievetagdata(){
		$tagnumber = $_GET['tagnumber'];
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$this->db->select('itemtag.itemtagid,itemtag.itemtagnumber,itemtag.purityid,itemtag.accountid,itemtag.fromcounterid,itemtag.counterid,itemtag.tagtypeid,itemtag.productid,ROUND(itemtag.grossweight,'.$round.') as grossweight,
		ROUND(itemtag.stoneweight,'.$round.') as stoneweight,
		ROUND(itemtag.netweight,'.$round.') as netweight,
		,itemtag.pieces,itemtag.description,itemtag.status,itemtag.melting',false);
		$this->db->from('itemtag');		
		$this->db->where('itemtag.itemtagnumber',$tagnumber);
		$this->db->limit(1);
		$info = $this->db->get();
		if($info->num_rows() == 1)
		{
			$infos = $info->row();			
			if($infos->status == 1)
			{
				$tagjsonarray = array(
										'itemtagid' => $infos->itemtagid,
										'tagtypetransferid' => $infos->tagtypeid,
										'purity'=> $infos->purityid,								
										'product'=> $infos->productid,
										'grossweighttransfer' => $infos->grossweight + 0,
										'stoneweighttransfer' => $infos->stoneweight + 0,
										'netweighttransfer' => $infos->netweight + 0,
										'transferpieces' => $infos->pieces,	
									    'transfermelting' => $infos->melting,	
										'transferfromcounterid' => $infos->counterid,	
										'transfertocounterid' => $this->Basefunctions->singlefieldfetch('counterid','productid','product',$infos->productid),	
										'output'=>'UNSOLD'
									);
			}
			else if($infos->status == 5)
			{
				$tagjsonarray = array('output'=>'SOLD');
			}
			else
			{
				$tagjsonarray = array('output'=>'NO');
			}
		}
		else
		{
			$tagjsonarray = array('output'=>'NO');
		}
		echo json_encode($tagjsonarray);
	}
	//item tag - transfer create
	public function itemtagtransfercreate() {
		$tagtypeid = '';
		$tagentrytypeid = '';
		$processproductid=1;
		if(isset($_POST['tocounter'])) {
			$counter = $_POST['tocounter'];
		} else {
			$counter=1;
		}
		if($_POST['tagtypetransferid'] == 2){  // Tag
			$tagentrytypeid = 2;
		}else if($_POST['tagtypetransferid'] == 4){  // Bulk Tag
			$tagentrytypeid = 6;
		}else if($_POST['tagtypetransferid'] == 5){  // Price Tag
			$tagentrytypeid = 5;
		}
		$tagstatus = '1';
		if($_POST['counterdefault'] == 0){   // Tag transfer
			$tagtypeid = 14;
			$tagstatus = $this->Basefunctions->activestatus;
		}else if($_POST['counterdefault'] == 1){
			if($_POST['counterdefaultid'] == 2){
				$tagstatus = $this->Basefunctions->transferstatus;
				$tagtypeid = 11;             // Old Jewels
				if(isset($_POST['tagtransprocessproductid'])) {
					$processproductid = $_POST['tagtransprocessproductid'];
				} else {
					$processproductid=1;
				}
			}else if($_POST['counterdefaultid'] == 4){
				$tagstatus = $this->Basefunctions->transferstatus;
				$tagtypeid = 13;			 // Purchase Return
				$processproductid = $_POST['product'];
			}else if($_POST['counterdefaultid'] == 12){
				$tagstatus = $this->Basefunctions->activestatus;
				$tagtypeid = 14;			 //  Counter loaded for all products
				$processproductid = $_POST['product'];
			}
		}
		
		$tagweight = $this->Basefunctions->generalinformaion('itemtag','tagweight','itemtagnumber',$_POST['tagnumber']);
		$insert = array(
				'itemtagnumber' => '',
				'tagdate' => $this->Basefunctions->ymd_format($_POST['transferdate']),
				'tagtypeid' =>$tagtypeid,
				'purityid' => $_POST['purity'],						
				'productid' => $_POST['product'],
				'grossweight' => $_POST['grossweighttransfer'],		
				'stoneweight' => $_POST['stoneweighttransfer'],		
				'netweight' => $_POST['netweighttransfer'],				
				'pieces' => $_POST['transferpieces'],					
				'counterid' => $this->Basefunctions->checkintegervalue($counter,1),	
				'fromcounterid' => $this->Basefunctions->checkintegervalue($_POST['transferfromcounterid'],1),				
				'status'=>$this->Basefunctions->activestatus,
				'tagentrytypeid'=>$tagentrytypeid,
				'processproductid'=>$processproductid,
				'transfertagno' =>$_POST['tagnumber'],
				'melting' =>$_POST['transfermelting'],
				'tagweight' =>$tagweight,
		        'industryid'=>$this->Basefunctions->industryid,
				'branchid'=>$this->Basefunctions->branchid
			);
		$insert = array_merge($insert,$this->Crudmodel->defaultvalueget());
		$this->db->insert('itemtag',array_filter($insert));
		$primaryid = $this->db->insert_id();
		$oldcounterid = $this->Basefunctions->generalinformaion('itemtag','counterid','itemtagnumber',$_POST['tagnumber']);
		$uptag=array(
					'counterid' => $this->Basefunctions->checkintegervalue($counter,1),
					'lastupdateuserid'=>$this->Basefunctions->userid,					 
			       	'lastupdatedate'=>date($this->Basefunctions->datef),
				  	'status'=>$tagstatus);
		$this->db->where('itemtagnumber',$_POST['tagnumber']);
		$this->db->update('itemtag',$uptag);
		
		//new counter update or create
		//$this->Basefunctions->stocktableentryfunction($primaryid,'50');
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' transferred Tag - '.$_POST['tagnumber'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,$this->itemtagmodule);
		echo 'SUCCESS';
	}
	public function itemtagget() {
		$tagid = $_POST['itemtagid'];
		$tagentrytypeid = array('2','3');
		
		$this->db->select('itemtag.tagtypeid,itemtag.tagentrytypeid,itemtag.status,status.statusname,itemtag.itemtagnumber');
		$this->db->from('itemtag');	
		$this->db->join('status','status.status=itemtag.status');	
		$this->db->where('itemtag.itemtagid',$tagid);
		$this->db->limit(1);
		$data = $this->db->get(); 
		foreach($data->result() as $info)
		{
		if (in_array($info->tagentrytypeid,$tagentrytypeid ))
		{
			echo 'Transfer Cannot Be edited and deleted also. Do new transfer only.';
		}
		else if (in_array($info->status,array('17','18','19')))
		{
			echo 'Item is Delivery type tag cannot edit it ';
		}
		else if ($info->status == '15')
		{
			echo 'Item is Given on Approval Cannot  Edit It';
		}
		else if ($info->status == '12')
		{
			echo 'The '.$info->itemtagnumber.' is Sold';
		}
		else if ($info->status == '13')
		{
			echo 'The '.$info->itemtagnumber.' is Transfered to other type.Cannot Edit or delete.';
		}
		else if ($info->status == '1')
		{
			echo 'SUCCESS';
		}
		else
		{
			echo 'FAIl';
		}
		}
	}
	//non tag transfer create
	public function nontagtransfercreate() {
		$tagtypeid = '';
		if(isset($_POST['nontagtocounter'])) {
			$counter=$_POST['nontagtocounter'];
		} else {
			$counter=1;
		}
		if(isset($_POST['nontagfromcounterid'])) {
			$fcounter=$_POST['nontagfromcounterid'];
		} else {
			$fcounter=1;
		}
		if(isset($_POST['purityid'])) {
			$nontagpurity=$_POST['purityid'];
		} else {
			$nontagpurity=1;
		}
		$processproductid = 1;
		if($_POST['counterdefault'] == 0){
			$tagtypeid = 16;			     // Non Tag transfer
		}else if($_POST['counterdefault'] == 1){
			if($_POST['counterdefaultid'] == 2){
				$tagtypeid = 11;             // Old Jewels
				$processproductid = $_POST['nontagprocessproductid'];
			}else if($_POST['counterdefaultid'] == 4){
				$tagtypeid = 13;			 // Purchase Return
				$processproductid = $_POST['nontagproductid'];
			}else if($_POST['counterdefaultid'] == 12){
				$tagtypeid = 16;			 // common counter
			}
		}
		$insert = array(
				'tagdate' => $this->Basefunctions->ymd_format($_POST['nontagtransferdate']),
				'tagtypeid' => $tagtypeid,
				'purityid' => $this->Basefunctions->checkintegervalue($nontagpurity,1),
				'productid' => $this->Basefunctions->checkintegervalue($_POST['nontagproductid'],1),
				'grossweight' => $_POST['nontaggrossweight'],		
				'netweight' => $_POST['nontagnetweight'],				
				'pieces' => $_POST['nontagpieces'],					
				'counterid' => $this->Basefunctions->checkintegervalue($counter,1),				
				'fromcounterid' => $this->Basefunctions->checkintegervalue($fcounter,1),				
				'tagentrytypeid' => $_POST['tagentrytype'],
				'melting' =>$_POST['uttmelting'],
				'industryid'=>$this->Basefunctions->industryid,
				'branchid'=>$this->Basefunctions->branchid,
				'processproductid'=>$processproductid,
				'status'=>$this->Basefunctions->activestatus
		);
		$insert = array_merge($insert,$this->Crudmodel->defaultvalueget());
		$this->db->insert('itemtag',array_filter($insert));
		$primaryid = $this->db->insert_id();
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' transferred NonTag';
		$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,$this->itemtagmodule);
		//stock report data entry
		/* $purityid = $this->Basefunctions->checkintegervalue($nontagpurity,1);
		$productid = $this->Basefunctions->checkintegervalue($_POST['nontagproductid'],1);
		$tagentrytype = 3;
		$counterid = $this->Basefunctions->checkintegervalue($counter,1);
		$oldcounterid = $this->Basefunctions->checkintegervalue($fcounter,1);
		$branchid=$this->Basefunctions->branchid;
		$tagtypeid = '16'; */
		//new counter update or create
		//$this->Basefunctions->stocktableentryfunction($primaryid,'50');
		echo 'SUCCESS';
	}
	public function autoitemsize() {
		$item_size=array();
		$item_sizes = $this->db->select('size')->from('itemtag')->where('status',$this->Basefunctions->activestatus)->where('size !=','')->get();
		if($item_sizes->num_rows() > 0) {
		$i=0;
		foreach($item_sizes->result() as $itemsizesdata) {
			$item_size[]=$itemsizesdata->size;
			 	$i++;
					}
		}
		else {
			$item_size[]='';
		}
		$datas = array_map("unserialize", array_unique(array_map("serialize", $item_size)));
		echo json_encode($datas);
	}
	// Check Lot Status
	public function checklotstatus() {
		$lotid = $_GET['lotid'];
		$islotclose=$this->db->where('lotid',$lotid)->where('status',$this->Basefunctions->closestatus)->get('lot');
		if($islotclose->num_rows() > 0) {
			$lotstatus='closed';
		} else {
			$lotstatus='';
		}
		echo $lotstatus;
	}
	public function getratefromstonename()
	{	$stoneid=$_POST['stoneid'];
	    $roundamount=$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); // amount round
	    $selectquery = 'stoneentrycalctypeid,round(COALESCE(sum(stone.stonerate),0),roundamount) as stonerate  ,round(COALESCE(sum(stone.purchaserate),0),roundamount) as purchaserate';
		$selectquery = str_replace("roundamount",$roundamount,$selectquery);
		$result = $this->db->select($selectquery)->from('stone')->where('stoneid',$stoneid)->where('status',1)->get();
		foreach($result->result() as $result_data){
		   $data = array('stonerate'=>$result_data->stonerate,'purchaserate'=>$result_data->purchaserate,'stoneentrycalctypeid'=>$result_data->stoneentrycalctypeid);
		}
		echo json_encode($data);
	}
	//simple drop down with data grouping
	public function simpledropdownwithcond($did,$dname,$table,$whfield,$whdata) {
		$i=0;
		$mvalu=explode(",",$whdata);
		$this->db->select("$dname,$did");
		$this->db->from($table);
		if($whdata != '') {
			if(count($mvalu)>=2) {
				$this->db->where_in($whfield,$mvalu);
			} else {
				$this->db->where("FIND_IN_SET('$whdata',$whfield) >", 0);
			}
		}
		$this->db->order_by('sortorder','desc');
		$result = $this->db->get();
		return $result->result();
	}

	public function getstonedetails()
	{
		$itemtno=$_GET['tagnumber'];
		$itemtag=$tagtypeid = $this->Basefunctions->singlefieldfetch('itemtagid','itemtagnumber','itemtag',$itemtno);
		//tag based charges
		$infotag = array();
		$infotagcharge = array();
		$this->db->select('se.stoneentryid,se.stoneid,stone.stonename,se.purchaserate,se.stonerate,se.pieces,se.caratweight,se.gram,se.totalcaratweight,se.totalnetweight,se.totalamount,stonetype.stonetypeid,stonetype.stonetypename,stoneentrycalctype.stoneentrycalctypeid,stoneentrycalctype.stoneentrycalctypename');
		$this->db->from('stoneentry as se');
		$this->db->join('stone','stone.stoneid = se.stoneid');
		$this->db->join('stonecategory','stonecategory.stonecategoryid = stone.stonecategoryid');
		$this->db->join('stonetype','stonetype.stonetypeid = stonecategory.stonetypeid');
		$this->db->join('stoneentrycalctype','stoneentrycalctype.stoneentrycalctypeid = se.stoneentrycalctypeid');
		$this->db->where('se.itemtagid',$itemtag);
		$this->db->where_in('se.status',array($this->Basefunctions->activestatus));
		$data = $this->db->get();
		if($data->num_rows() > 0) {
			$j=1;
			foreach($data->result() as $value)
			{
				$stonedetail->rows[$j]['id'] = $j;
				$stonedetail->rows[$j]['cell']=array(
						$value->stonetypeid,
						$value->stonetypename,
						$value->stoneid,
						$value->stonename,
						$value->purchaserate,
						$value->stonerate,
						$value->stoneentrycalctypeid,
						$value->stoneentrycalctypename,
						$value->pieces,
						$value->caratweight,
						$value->gram,
						$value->totalcaratweight,
						$value->totalnetweight,
						$value->totalamount
				);
				$j++;
			}
		}else{
			$stonedetail='';
			
		}
		echo json_encode($stonedetail);
	}
	public function loadsizedata(){
		$productid=$_GET['productid'];
		$result_array=[];
		$this->db->select('itemtag.size');
		$this->db->from('itemtag');
		$this->db->where('status',$this->Basefunctions->activestatus);
		$this->db->where('productid',$productid);
		$this->db->where('size !=','');
		$viewcreationarray=$this->db->get();
		if($viewcreationarray->num_rows() > 0) {
			foreach($viewcreationarray->result() as $data){
				$result_array[]=$data->size;
			}
		}else{
			$result_array='';
		}
		echo json_encode($result_array);
	}
	public function insert_rfiddata(){
		$noofdigits = '1';
			$update_lotstatus = array('branchname' =>$this->db->database);
			$this->db->where('branchid','1');
			$this->db->update('branch',$update_lotstatus);
			$masterdb =  $this->Basefunctions->inlineuserdatabaseactive($this->db->database);
			$json = file_get_contents('php://input');
			$obj = json_decode($json);
			$tid = substr($obj->tagID, '20');
			$tid = '1'.$tid ;
			if(!empty($tid)) {
				$insert = array(
								'rfidserialno'=>$obj->deviceInfo->serial,
								'tagserialno'=>$tid.'-'.$obj->deviceInfo->serial,
								'rfidtagno'=>$tid

								);
				$masterdb->insert('rfidstock',array_filter($insert));
			}
			return 'success'; 
	}
	public function getrfidtagnumber(){
		$sessionid = $_POST['sessionid'];
		$result='';
		$masterdb = $this->Basefunctions->inlineuserdatabaseactive($this->db->masterdb); //master data base select
		$masterdb->select('rfidstock.tagid');
		$masterdb->from('rfidstock');
		$masterdb->where('sessionid',$sessionid);
		$tagarray=$masterdb->get();
		if($tagarray->num_rows() > 0) {
			foreach($tagarray->result() as $data){
				$result=$data->tagid;
			}
		}else{
				$result='';
		}
		echo json_encode($result);
	}
	public function rfiddevicestore(){
		$jsonval = file_get_contents('php://input');
		$objval = json_decode($jsonval); 
		if(isset($objval->deviceName)){
			$devicename = $objval->deviceName;
		}else{
			$devicename = '';
		}
		$insert = array(
				'rfiddevicename'=>$devicename,
				'sessionid'=>$objval->sessionId,
				'status'=>$objval->status
		);
		$this->db->insert('rfiddevice',array_filter($insert));
		return 'success';
	}
	public function checkrfiddevice(){
		$sessionid = $_POST['sessionid'];
		$result='';
		$masterdb = $this->Basefunctions->inlineuserdatabaseactive($this->db->masterdb); //master data base select
		$masterdb->select('rfiddevice.status');
		$masterdb->from('rfiddevice');
		$masterdb->where('sessionid',$sessionid);
		$tagarray=$masterdb->get();
		if($tagarray->num_rows() > 0) {
			foreach($tagarray->result() as $data){
				$result=$data->status;
			}
		}else{
			$result=0;
		}
		echo json_encode($result);
	}
	//stock report table data insertion
	public function stockreportdataentry($tagentrytype,$tagtypeid,$purityid,$counterid,$productid,$branchid) {
		$data = array();
		$query = 'select itemtag.purityid,itemtag.productid,itemtag.branchid,(CASE WHEN itemtag.tagtypeid in (2,4,5) THEN 2 WHEN itemtag.tagtypeid in (3)  THEN 3 WHEN itemtag.tagtypeid = 14 and itemtag.tagentrytypeid = 2 THEN 2 WHEN itemtag.tagtypeid = 16  and itemtag.tagentrytypeid = 3  THEN 3 WHEN itemtag.tagtypeid = 13 and  itemtag.tagentrytypeid = 2 THEN 14 WHEN itemtag.tagtypeid = 13 and  itemtag.tagentrytypeid = 3 THEN 14 WHEN itemtag.tagtypeid = 11 and  itemtag.tagentrytypeid = 2 THEN 4 WHEN itemtag.tagtypeid = 11 and  itemtag.tagentrytypeid = 3 THEN 4 ELSE 1 END) as  stockreporttypeid ,(CASE WHEN itemtag.tagtypeid in (2,4,5) THEN stockincounterid WHEN itemtag.tagtypeid in (3) THEN counterid WHEN itemtag.tagtypeid in (13,11) THEN counterid WHEN itemtag.tagtypeid in (14) THEN counterid ELSE 1 END) as  counterid,status,COALESCE(sum(itemtag.grossweight),0) as grossweight,COALESCE(sum(itemtag.stoneweight),0) as stoneweight,COALESCE(sum(itemtag.netweight),0) as netweight,COALESCE(sum(itemtag.pieces),0) as pieces,COALESCE(sum(itemtag.melting),0) as melting FROM itemtag WHERE itemtag.status not in (0,3) group by stockreporttypeid,purityid,productid,counterid,branchid';
		$result = $this->db->query($query);
		foreach($result->result() as $row) {
			$data = array('purityid'=>$row->purityid,
				'counterid'=>$row->counterid,
				'productid'=>$row->productid,
				'branchid'=>$row->branchid,
				'stockreporttypeid'=>$row->stockreporttypeid,
				'grossweight'=>$row->grossweight,
				'netweight'=>$row->netweight,
				'stoneweight'=>$row->stoneweight,
				'pieces'=>$row->pieces,
				'melting'=>$row->melting
			);
			$check = $this->checkdataset($row->stockreporttypeid,$row->purityid,$row->counterid,$row->productid,$row->branchid);
			if($check == 'FALSE'){ // if not exist create record
				$defdataarr = $this->defaultvalueget();
				$industryarray = array('industryid'=>$this->Basefunctions->industryid);
				$newinddata = array_merge($data,$defdataarr);
				$newdata = array_merge($newinddata,$industryarray);
				$this->db->insert( 'stock', array_filter($newdata) );
			} else { // if exist update record
				$defdataarr = $this->updatedefaultvalueget();
				$newdata = array_merge($data,$defdataarr);
				$this->db->where('stock.stockid',$check);
				$this->db->update( 'stock', array_filter($newdata) );
			}
		}
	}
	//check if the row exist or not
	public function checkdataset($tagtypeid,$purityid,$counterid,$productid,$branchid) {
		$data = '';
		$this->db->select('stockid');
		$this->db->from('stock');
		$this->db->where_in('stock.purityid',$purityid);
		$this->db->where_in('stock.productid',$productid);
		$this->db->where_in('stock.counterid',$counterid);
		$this->db->where_in('stock.branchid',$branchid);
		$this->db->where_in('stock.stockreporttypeid',$tagtypeid);
		$this->db->where('stock.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = $row->stockid;
			}
		} else {
			$data = 'FALSE';
		}
		return $data;
	}
	//default value set array for add
	public function defaultvalueget() {
		$defdata['createdate'] = date($this->Basefunctions->datef);
		$defdata['lastupdatedate'] = date($this->Basefunctions->datef);
		$defdata['createuserid'] = $this->Basefunctions->userid;
		$defdata['lastupdateuserid'] = $this->Basefunctions->userid;
		$defdata['status'] = 1;
		return $defdata;
	}
	//default value set array for update
	public function updatedefaultvalueget() {
		$defdata['lastupdatedate'] = date($this->Basefunctions->datef);
		$defdata['lastupdateuserid'] = $this->Basefunctions->userid;
		return $defdata;
	}
	// get storge from stock
	public function getfromstorage() {
		$tagentrytype = $_POST['tag_entrytype'];
		$branchid = $_POST['branchid'];
		$stocktype = '';
		if($tagentrytype == 10){        // tagentrytype -Sales return
			$stocktype = 7;
		}else if($tagentrytype == 11){ // tagentrytype -Old Jewels
			$stocktype = 6;
		}else if($tagentrytype == 12){ // tagentrytype -Partial Tag
			$stocktype = 8;
		}else if($tagentrytype == 3){ // tagentrytype -un Tag
			$stocktype = 3;
		}
		$data = array();
		$this->db->select('GROUP_CONCAT(DISTINCT(counterid) SEPARATOR ",") as counterid');
		$this->db->from('currentstock');
		$this->db->where('grossweight >',0);
		$this->db->where('pieces >',0);
		$this->db->where('stockreporttypeid',$stocktype);
		$this->db->where('branchid',$branchid);
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('counterid'=>$row->counterid);
			}
		}else {
			$data = '';
		}
		echo json_encode($data);
	}
	public function getpurity() {
		$tagentrytype = $_POST['tag_entrytype'];
		$storage = $_POST['storage'];
		$branchid = $_POST['branchid'];
		$stocktype = '';
		if($tagentrytype == 10){        // tagentrytype -Sales return
			$stocktype = 7;
		}else if($tagentrytype == 11){ // tagentrytype -Old Jewels
			$stocktype = 6;
		}else if($tagentrytype == 12){ // tagentrytype -Partial Tag
			$stocktype = 8;
		}else if($tagentrytype == 3){ // tagentrytype -un Tag
			$stocktype = 3;
		}
		$data = array();
		$this->db->select('GROUP_CONCAT(DISTINCT(purityid) SEPARATOR ",") as purityid');
		$this->db->from('currentstock');
		$this->db->where('grossweight >',0);
		$this->db->where('pieces >',0);
		$this->db->where('stockreporttypeid',$stocktype);
		$this->db->where('counterid',$storage);
		$this->db->where('branchid',$branchid);
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('purityid'=>$row->purityid);
			}
		}else {
			$data = '';
		}
		echo json_encode($data);
	}
	public function loadproductfromstock() {
		$tagentrytype = $_POST['tag_entrytype'];
		$storage = $_POST['storage'];
		$purity = $_POST['purity'];
		$branchid = $_POST['branchid'];
		$stocktype = '';
		if($tagentrytype == 10){        // tagentrytype -Sales return
			$stocktype = 7;
		}else if($tagentrytype == 11){ // tagentrytype -Old Jewels
			$stocktype = 6;
		}else if($tagentrytype == 12){ // tagentrytype -Partial Tag
			$stocktype = 8;
		}else if($tagentrytype == 3){ // tagentrytype -un Tag
			$stocktype = 3;
		}
		$data = array();
		$this->db->select('GROUP_CONCAT(DISTINCT(productid) SEPARATOR ",") as productid');
		$this->db->from('currentstock');
		$this->db->where('grossweight >',0);
		$this->db->where('pieces >',0);
		$this->db->where('stockreporttypeid',$stocktype);
		$this->db->where('counterid',$storage);
		$this->db->where('purityid',$purity);
		$this->db->where('branchid',$branchid);
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('productid'=>$row->productid);
			}
		}else {
			$data = '';
		}
		echo json_encode($data);
	}
	public function getweight() {
		$tagentrytype = $_POST['tag_entrytype'];
		$storage = $_POST['storage'];
		$purity = $_POST['purity'];
		$branchid = $_POST['branchid'];
		$productid = $_POST['productid'];
		$stocktype = '';
		if($tagentrytype == 10){        // tagentrytype -Sales return
			$stocktype = 7;
		}else if($tagentrytype == 11){ // tagentrytype -Old Jewels
			$stocktype = 6;
		}else if($tagentrytype == 12){ // tagentrytype -Partial Tag
			$stocktype = 8;
		}
		else if($tagentrytype == 3){ // tagentrytype -un Tag
			$stocktype = 3;
		}
		$data = array();
		$this->db->select('netweight,grossweight,pieces');
		$this->db->from('currentstock');
		$this->db->where('grossweight >',0);
		$this->db->where('pieces >',0);
		$this->db->where('stockreporttypeid',$stocktype);
		$this->db->where('counterid',$storage);
		$this->db->where('purityid',$purity);
		$this->db->where('branchid',$branchid);
		$this->db->where('productid',$productid);

		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('netweight'=>$row->netweight,'grossweight'=>$row->grossweight,'pieces'=>number_format((float)$row->pieces, 0, '.', ''));
			}
		}else {
			$data = '';
		}
		echo json_encode($data);
	}
	
	public function getpurityforuntag() {
		$storageid = $_POST['storageid'];
		$data = array();
		$this->db->select('purityid');
		$this->db->from('product');
		$this->db->where("FIND_IN_SET('".$storageid."',counterid) >", 0);
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('purityid'=>$row->purityid);
			}
		}else {
			$data = '';
		}
		echo json_encode($data);
	}
	public function defaultcounterwithjewel()
	{
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('countername,counterid,parentcounterid,counterdefault,counterdefaultid');
		$this->db->from('counter');
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->where('storagetypeid',2);
		$this->db->where_not_in('counterdefaultid',array(3,5,10,11));
		$this->db->where('status',$this->Basefunctions->activestatus);
		$this->db->order_by('counterid','asc');
		$info = $this->db->get();
		return $info;
	}
	//unique name check
	public function uniquedynamicviewnamecheckmodel(){
		$industryid = $this->Basefunctions->industryid;
		$primaryid = $_POST['primaryid'];
		$accname = $_POST['accname'];
		//$accname = explode(',',$accname);
		$partable =  'itemtag';
		$ptid = $partable.'id';
		$ptname = $partable.'rfidtagno';
		if($accname != "") {
			$result = $this->db->query('SELECT tagno from rfidref WHERE tagserialno IN("'.$accname.'") LIMIT 1');
			if($result->num_rows() > 0) {
				foreach($result->result() as $row) {
					$this->db->select($ptid);
					$this->db->from($partable);
					$this->db->where_in('itemtag.rfidtagno',$row->tagno);
					$this->db->where_in($partable.'.status',array(1));
					$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
					if($primaryid != '') {
						$this->db->where_not_in($ptid,array($primaryid));
					}
					$result = $this->db->get();
					if($result->num_rows() > 0) {
						foreach($result->result()as $row) {
							echo 'DUPLICATE'.$row->$ptid;
						}
					} else {
						echo $row->tagno;
					}
				}
			} else {
				echo 'NOTAGNO';
			}
		} else { 
			echo "BLANKFIELD";
		}
	}
	// load product addon for stock module
	public function loadproductcharge() {
		$purity=$_POST['purityid'];
		$product=$_POST['productid'];
		$weight=$_POST['netwt'];
		$accountgroupid=$_POST['chargeaccountgroup'];
		// specific account group
		$this->db->select('chargedetails,chargeid',false);
		$this->db->from('productcharge');
		$this->db->where('productcharge.productid',$product);
		$this->db->where_in('productcharge.accountgroupid',$accountgroupid);
		$this->db->where('productcharge.accounttypeid',6);
		$this->db->where('productcharge.purityid',$purity);
		$this->db->where('startweight <=',$weight);
		$this->db->where('endweight >=',$weight);
		$this->db->where('productcharge.status',$this->Basefunctions->activestatus);
		$this->db->order_by('productcharge.productchargeid','desc');
		$this->db->limit(1);
		$productchargearray=$this->db->get();
		// all account group
		$this->db->select('chargedetails,chargeid',false);
		$this->db->from('productcharge');
		$this->db->where('productcharge.productid',$product);
		$this->db->where_in('productcharge.accountgroupid',1);
		$this->db->where('productcharge.accounttypeid',6);
		$this->db->where('productcharge.purityid',$purity);
		$this->db->where('startweight <=',$weight);
		$this->db->where('endweight >=',$weight);
		$this->db->where('productcharge.status',$this->Basefunctions->activestatus);
		$this->db->order_by('productcharge.productchargeid','desc');
		$this->db->limit(1);
		$productchargeallarray=$this->db->get();
		if($productchargearray->num_rows() > 0) {
			foreach($productchargearray->result() as $info) {
				$charge_array= array(
						'chargedetails'=>$info->chargedetails,
						'chargeid'=>$info->chargeid
				);
			}
		}else if($productchargeallarray->num_rows() > 0) {
			foreach($productchargeallarray->result() as $allinfo) {
				$charge_array= array(
						'chargedetails'=>$allinfo->chargedetails,
						'chargeid'=>$allinfo->chargeid
				);
			}
		}else{
			$chargeid =  $this->Basefunctions->generalinformaion('product','chargeid','productid',$product);
			$chargearrayid = explode(",",$chargeid);
			$field = 'chargekeyword';
			$this->db->select('GROUP_CONCAT('.$field.') as chargekeyword');
			$this->db->from('charge');
			$this->db->where_in('charge.chargeid',$chargearrayid);
			$chargedetails=$this->db->get();
			foreach($chargedetails->result() as $chargedetailsinfo){
				$chargekeyworddata = $chargedetailsinfo->chargekeyword;
			}
				
			$charge_array= array(
					'chargedetails'=>$chargekeyworddata,
					'chargeid'=>$chargeid
			);
		}
		$chargeid =  $this->Basefunctions->generalinformaion('product','chargeid','productid',$product);
		$data=$this->db->query("select GROUP_CONCAT(chargecategoryid,':',chargename,':',chargekeyword,':',chargeid) as chargekeyword
				FROM charge
				WHERE chargeid IN ($chargeid)"
				);
		foreach($data->result() as $info) {
			$charge_array['keyword']= array(
					'chargekeyword'=>$info->chargekeyword
			);
		}
		echo json_encode($charge_array);
	}
	public function product_dd()
	{
		$industryid = $this->Basefunctions->industryid;
		$info =$this->db->query("SELECT `product`.`productname`, `product`.`productid`, `product`.`categoryid`, `category`.`categoryid`, `category`.`categoryname`, `category`.`categorylevel`, `product`.`counterid` as counterid, `product`.`bullionapplicable`,  `product`.`rfidapplicable`,`product`.`taxable`,`product`.`tagtemplateid`,`product`.`productpieces`, group_concat(`tax`.`taxname`, '-',`tax`.`taxid`) as taxid,group_concat(distinct(`tax`.`taxid`)) as taxdataid, `product`.`purityid`,`product`.`chargeid`,`product`.`purchasechargeid`,`product`.`stoneapplicable`,`product`.`size`,`product`.`tagtypeid`,`product`.`productstonecalctypeid`,CONCAT('a', REPLACE(product.purityid, ',', 'a,a'), 'a') as purityidhidden, sum(`tax`.`taxrate`) as taxratesum,`product`.`tagweight`,`product`.`packingweight`,`product`.`packwtless` FROM `product` JOIN `category` ON `category`.`categoryid` = `product`.`categoryid` JOIN `taxmaster` ON `taxmaster`.`taxmasterid` = `product`.`taxmasterid` LEFT JOIN `tax` ON `tax`.`taxmasterid` = `taxmaster`.`taxmasterid` JOIN `charge` ON `charge`.`chargeid` = `product`.`chargeid` JOIN `purchasecharge` ON `purchasecharge`.`purchasechargeid` = `product`.`purchasechargeid` WHERE `product`.`status` = 1 and `product`.`productid` not in (2,3) and `category`.`categoryid` not in(2,3,4) and `product`.`bullionapplicable`= 'No' and `product`.`industryid` = ".$industryid." GROUP BY `product`.`productid` ORDER BY `product`.`productid` DESC");
		return $info;
	}
	// Except UNtag all the products
	public function productrfid_dd()
	{
		$industryid = $this->Basefunctions->industryid;
		$info =$this->db->query("SELECT `product`.`productname`, `product`.`productid`, `product`.`categoryid`, `category`.`categoryid`, `category`.`categoryname`, `category`.`categorylevel`, `product`.`counterid` as counterid, `product`.`bullionapplicable`,  `product`.`rfidapplicable`,`product`.`taxable`,`product`.`tagtemplateid`,`product`.`productpieces`, group_concat(`tax`.`taxname`, '-',`tax`.`taxid`) as taxid,group_concat(distinct(`tax`.`taxid`)) as taxdataid, `product`.`purityid`,`product`.`chargeid`,`product`.`purchasechargeid`,`product`.`stoneapplicable`,`product`.`size`,`product`.`tagtypeid`,`product`.`productstonecalctypeid`,CONCAT('a', REPLACE(product.purityid, ',', 'a,a'), 'a') as purityidhidden, sum(`tax`.`taxrate`) as taxratesum,`product`.`tagweight`,`product`.`packingweight`,`product`.`packwtless` FROM `product` JOIN `category` ON `category`.`categoryid` = `product`.`categoryid` JOIN `taxmaster` ON `taxmaster`.`taxmasterid` = `product`.`taxmasterid` LEFT JOIN `tax` ON `tax`.`taxmasterid` = `taxmaster`.`taxmasterid` JOIN `charge` ON `charge`.`chargeid` = `product`.`chargeid` JOIN `purchasecharge` ON `purchasecharge`.`purchasechargeid` = `product`.`purchasechargeid` JOIN `tagtype` ON `tagtype`.`tagtypeid` = `product`.`tagtypeid` WHERE `product`.`status` = 1 and `product`.`productid` not in (2,3) and `category`.`categoryid` not in(2,3,4) and `product`.`bullionapplicable`= 'No' and `product`.`industryid` = ".$industryid." AND product.tagtypeid IN (2,4,5,17) GROUP BY `product`.`productid` ORDER BY `product`.`productid` DESC");
		return $info;
	}
	public function processproduct_dd()
	{
		$industryid = $this->Basefunctions->industryid;
		$info =$this->db->query("SELECT `product`.`productname`, `product`.`productid`, `product`.`categoryid`, `category`.`categoryid`, `category`.`categoryname`, `category`.`categorylevel`, `product`.`counterid` as counterid, `product`.`bullionapplicable`, `product`.`taxable`, group_concat(`tax`.`taxname`, '-',`tax`.`taxid`) as taxid,group_concat(distinct(`tax`.`taxid`)) as taxdataid, `product`.`purityid`,`product`.`chargeid`,`product`.`purchasechargeid`,`product`.`stoneapplicable`,`product`.`size`,`product`.`tagtypeid` FROM `product` JOIN `category` ON `category`.`categoryid` = `product`.`categoryid` JOIN `taxmaster` ON `taxmaster`.`taxmasterid` = `product`.`taxmasterid` LEFT JOIN `tax` ON `tax`.`taxmasterid` = `taxmaster`.`taxmasterid` JOIN `charge` ON `charge`.`chargeid` = `product`.`chargeid` JOIN `purchasecharge` ON `purchasecharge`.`purchasechargeid` = `product`.`purchasechargeid` WHERE `product`.`status` = 1 and `product`.`productid` != 3 and `category`.`categoryid` != 2 and `product`.`bullionapplicable`= 'No' and `product`.`industryid` = ".$industryid." GROUP BY `product`.`productid` ORDER BY `product`.`productid` DESC");
		return $info;
	}
	// Tag & Nontag Transfer process productid
	public function transferprocessproduct_dd()
	{
		$industryid = $this->Basefunctions->industryid;
		$info =$this->db->query("SELECT `product`.`productname`, `product`.`productid`, `product`.`categoryid`, `category`.`categoryid`, `category`.`categoryname`, `category`.`categorylevel`, `product`.`counterid` as counterid, `product`.`bullionapplicable`, `product`.`purityid`,`product`.`tagtypeid` FROM `product` JOIN `category` ON `category`.`categoryid` = `product`.`categoryid` WHERE `product`.`status` = 1 and `product`.`productid` != 3 and `category`.`categoryid` in(3,4) and `product`.`bullionapplicable`= 'No' and `product`.`industryid` = ".$industryid." GROUP BY `product`.`productid` ORDER BY `product`.`productid` DESC");
		return $info;
	}
	public function nontagproduct_dd()
	{
		$industryid = $this->Basefunctions->industryid;
		$info =$this->db->query("SELECT `product`.`productname`, `product`.`productid`, `product`.`categoryid`, `category`.`categoryid`, `category`.`categoryname`, `category`.`categorylevel`, `product`.`counterid` as counterid, `product`.`bullionapplicable`,  `product`.`rfidapplicable`,`product`.`taxable`,`product`.`tagtemplateid`, group_concat(`tax`.`taxname`, '-',`tax`.`taxid`) as taxid,group_concat(distinct(`tax`.`taxid`)) as taxdataid, `product`.`purityid`,`product`.`chargeid`,`product`.`purchasechargeid`,`product`.`stoneapplicable`,`product`.`size`,`product`.`tagtypeid`,`product`.`productstonecalctypeid`,CONCAT('a', REPLACE(product.purityid, ',', 'a,a'), 'a') as purityidhidden, sum(`tax`.`taxrate`) as taxratesum,`product`.`tagweight`,`product`.`packingweight`,`product`.`packwtless` FROM `product` JOIN `category` ON `category`.`categoryid` = `product`.`categoryid` JOIN `taxmaster` ON `taxmaster`.`taxmasterid` = `product`.`taxmasterid` LEFT JOIN `tax` ON `tax`.`taxmasterid` = `taxmaster`.`taxmasterid` JOIN `charge` ON `charge`.`chargeid` = `product`.`chargeid` JOIN `purchasecharge` ON `purchasecharge`.`purchasechargeid` = `product`.`purchasechargeid` WHERE `product`.`status` = 1 and `product`.`productid` not in (2,3) and `product`.`tagtypeid` in (3,17) and `category`.`categoryid` not in(2,3,4) and `product`.`bullionapplicable`= 'No' and `product`.`industryid` = ".$industryid." GROUP BY `product`.`productid` ORDER BY `product`.`productid` DESC");
		return $info;
	}
	public function loadunchangeproductcharge($purity,$product,$weight,$accountgroupid){
		// specific account group
		$this->db->select('chargedetails,chargeid',false);
		$this->db->from('productcharge');
		$this->db->where('productcharge.productid',$product);
		$this->db->where_in('productcharge.accountgroupid',$accountgroupid);
		$this->db->where('productcharge.accounttypeid',6);
		$this->db->where('productcharge.purityid',$purity);
		$this->db->where('startweight <=',$weight);
		$this->db->where('endweight >=',$weight);
		$this->db->where('productcharge.status',$this->Basefunctions->activestatus);
		$this->db->order_by('productcharge.productchargeid','desc');
		$this->db->limit(1);
		$productchargearray=$this->db->get();
		// all account group
		$this->db->select('chargedetails,chargeid',false);
		$this->db->from('productcharge');
		$this->db->where('productcharge.productid',$product);
		$this->db->where_in('productcharge.accountgroupid',1);
		$this->db->where('productcharge.accounttypeid',6);
		$this->db->where('productcharge.purityid',$purity);
		$this->db->where('startweight <=',$weight);
		$this->db->where('endweight >=',$weight);
		$this->db->where('productcharge.status',$this->Basefunctions->activestatus);
		$this->db->order_by('productcharge.productchargeid','desc');
		$this->db->limit(1);
		$productchargeallarray=$this->db->get();
		if($productchargearray->num_rows() > 0) {
			foreach($productchargearray->result() as $info) {
				$charge_array= array(
						'chargedetails'=>$info->chargedetails,
						'chargespandetails'=>$info->chargedetails,
						'chargeid'=>$info->chargeid
				);
			}
		}else if($productchargeallarray->num_rows() > 0) {
			foreach($productchargeallarray->result() as $allinfo) {
				$charge_array= array(
						'chargedetails'=>$allinfo->chargedetails,
						'chargespandetails'=>$allinfo->chargedetails,
						'chargeid'=>$allinfo->chargeid
				);
			}
		}else{
			$chargeid =  $this->Basefunctions->generalinformaion('product','chargeid','productid',$product);
			$chargearrayid = explode(",",$chargeid);
			$field = 'chargekeyword';
			$this->db->select('GROUP_CONCAT('.$field.') as chargekeyword');
			$this->db->from('charge');
			$this->db->where_in('charge.chargeid',$chargearrayid);
			$chargedetails=$this->db->get();
			foreach($chargedetails->result() as $chargedetailsinfo){
				$chargekeyworddata = $chargedetailsinfo->chargekeyword;
			}
				
			$charge_array= array(
					'chargedetails'=>$chargekeyworddata,
					'chargeid'=>$chargeid,
					'chargespandetails'=>''
			);
		}
		return $charge_array;
	}
	public function counter_groupdropdown_notdefault()
	{
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('countername,counterid,parentcounterid,parentcounterid,counterdefault,counterdefaultid');
		$this->db->from('counter');
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->where('counterdefaultid',11);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$this->db->order_by('counterid','asc');
		$info = $this->db->get();
		return $info;
	}
	// chech max counter quantity in stockentry
	public function checkmaxquantity(){
		$counterid = $_POST['counterid'];
		$maxcount = $this->db->select('counterid')->from('itemtag')->where('counterid',$counterid)->where('status',$this->Basefunctions->activestatus)->get()->num_rows();
		echo json_encode($maxcount);
	}
	//size dropdown data fetch
	public function size_groupdropdown() {
		$this->db->select('sizemaster.sizemasterid,sizemaster.sizemastername,sizemaster.productid,product.productname');
		$this->db->from('sizemaster');
		$this->db->join('product','product.productid=sizemaster.productid');
		$this->db->where('sizemaster.status',1);
		$result = $this->db->get();
		return $result;
	}
	//fetch max id of table
	public function maximumid($tablename,$fieldname,$tagtype,$tagvalue) {
		$data=$this->db->select_max($fieldname)->where_not_in($tagtype,$tagvalue)->where('status',1)->from($tablename)->get();
		foreach($data->result() as $datas) { return $id = $datas->$fieldname; }
	}
	//fetch field name
	public function modulelinkfetch($tblename,$fieldname,$cond,$cvalue,$tagtype,$tagvalue) {
		$data=0;
		$this->db->select($fieldname,false);
		$this->db->from($tblename);
		$this->db->where($cond,$cvalue);
		if($tagvalue != '') {
			$this->db->where_not_in($tagtype,$tagvalue);
		}
		$this->db->limit(1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result()as $row) {
				$data=$row->$fieldname;
			}
		}
		return $data;
	}
	// check order tag 
	public function checkordertag(){
		$ordcheck = 0;
		$itemtagid = $_POST['itemtagid'];
		$this->db->select('itemtagnumber,ordernumber');
		$this->db->from('itemtag');
		$this->db->where('orderstatus','Yes');
		$this->db->where('itemtagid',$itemtagid);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$this->db->limit(1);
		$data = $this->db->get();
		$ordcheck = $data->num_rows();
		if($ordcheck > 0) {
			foreach($data->result() as $info)
			{
				echo  'Order Item tag no : '.$info->itemtagnumber.'Cannot Be edited and deleted.Kindly remove it from recieve order no : ,'.$info->ordernumber.' then do edit or delete.';
			}
		} else {
			$tagentrytypeid = array('2','3','5','6');
			$this->db->select('itemtag.tagtypeid,itemtag.tagentrytypeid,itemtag.status,status.statusname,itemtag.itemtagnumber');
			$this->db->from('itemtag');	
			$this->db->join('status','status.status=itemtag.status');	
			$this->db->where('itemtag.itemtagid',$itemtagid);
			$this->db->limit(1);
			$data = $this->db->get(); 
			foreach($data->result() as $info) {
				if (in_array($info->tagentrytypeid,$tagentrytypeid )) {
					echo 'Transfer Cannot Be edited and deleted. Do new transfer only.';
				} else if (in_array($info->status,array('17','18','19'))) {
					echo 'Item is Delivery type tag cannot edit/Delete it ';
				} else if ($info->status == '15') {
					echo 'Item is Given on Approval Cannot edit/Delete it';
				} else if ($info->status == '12') {
					echo 'The '.$info->itemtagnumber.' is Sold';
				} else if ($info->status == '13') {
					echo 'The '.$info->itemtagnumber.' is Transfered to other type.Cannot Edit or delete.';
				} else if ($info->status == '26') {
					echo 'The '.$info->itemtagnumber.' is Missed/Theft. Cannot Edit or delete.';
				} else if ($info->status == '1') {
					echo 'SUCCESS';
				} else {
					echo 'FAIl';
				}
			}
		}
	}
	// Remove image from folder
	public function deleteimagefromfolder() {
		$filenamwithfolder = explode(',',$_POST['imagewithloc']);
		foreach($filenamwithfolder as $newfilename) {
			unlink($newfilename);
		}
		echo 'SUCCESS';
	}
	// Missing items add form
	public function missingitemaddform(){
		$missingitemstatus = $_POST['missingitemstatus'];
		$datarowid = $_POST['datarowid'];
		if($datarowid > 1) {
			$missingsarray = array(
							'missingdate' => date("Y-m-d", strtotime($_POST['missingitemdate'])),
							'missingdescription' => $_POST['missingdescription'],
							'lastupdatedate' => date($this->Basefunctions->datef),
							'lastupdateuserid' => $this->Basefunctions->userid,
							'status' => $missingitemstatus
							);
			$this->db->where('itemtagid',$datarowid);
			$this->db->update('itemtag',$missingsarray);
			echo 'SUCCESS';
		} else {
			echo 'FAILURE';
		}
	}
	// Missing items available status
	public function checkitemtagstatus(){
		$datarowid = $_POST['datarowid'];
		$this->db->select('itemtagid,itemtagnumber,rfidtagno,missingdate,missingdescription,status');
		$this->db->from('itemtag');
		$this->db->where('itemtagid',$datarowid);
		$this->db->where_not_in('status',array(0,2,5,3,12));
		$this->db->limit(1);
		$data = $this->db->get();
		if($data->num_rows() > 0) {
			$status = array(
				'missingdate' => date("Y-m-d", strtotime($data->row()->missingdate)),
				'rfidtagno' => $data->row()->rfidtagno,
				'itemtagnumber' => $data->row()->itemtagnumber,
				'missingdescription' => $data->row()->missingdescription,
				'status' => $data->row()->status,
				);
		} else {
			$status = '';
		}
		echo json_encode($status);
	}
	// Retrieve tag image of double click from main view grid.
	public function retrievetagimage() {
		$datarowid = $_POST['itemtagid'];
		$this->db->select('tagimage');
		$this->db->from('itemtag');
		$this->db->where('itemtagid',$datarowid);
		$this->db->where_not_in('status',array(0,3));
		$this->db->limit(1);
		$data = $this->db->get();
		if($data->num_rows() > 0) {
			foreach($data->result()as $row) {
				$tagimage = $row->tagimage;;
			}
		} else {
			$tagimage = '';
		}
		echo $tagimage;
	}
	// Product Additional Information
	public function productaddoninfo_dd() {
		$industryid = $this->Basefunctions->industryid;
		$info =$this->db->query("SELECT `productaddoninfo`.`productaddoninfoid`, `productaddoninfo`.`productaddoninfoname` FROM `productaddoninfo` WHERE `productaddoninfo`.`status` = 1 and `productaddoninfo`.`industryid` = ".$industryid." ORDER BY `productaddoninfo`.`productaddoninfoid` ASC");
		return $info;
	}
	// Retrieve Grid Details to Form Data Set
	public function retrievegriddetailstofrom() {
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$rateround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //Rate round-off
		$taxround = 2;
		$datarowid = $_POST['salesdetailid'];
		$stocktype = $_POST['stocktype'];
		if($stocktype == 'Tag') {
			$previtemtagid = $this->Basefunctions->singlefieldfetch('itemtagid','salesdetailid','salesdetail',$datarowid);
			$this->db->select("itemtagid, tagtemplateid, tagtypeid, purityid, accountid, productid, ROUND(grossweight, '".$round."') as grossweight, ROUND(stoneweight, '".$round."') as stoneweight, ROUND(netweight, '".$round."') as netweight, caratweight, pieces, size, productaddoninfoid, diacaratweight, diapieces, counterid, fromcounterid, stockincounterid, processproductid, chargeamount, chargespanamount, chargeid, chargecategoryid, chargedetails, chargespandetails, certificationno");
			$this->db->from('itemtag');
			$this->db->where('itemtagid',$previtemtagid);
			$this->db->where('status',12);
			$this->db->limit(1);
			$data = $this->db->get();
			if($data->num_rows() > 0) {
				$info = array(
					'itemtagid' => $data->row()->itemtagid,
					'tagtemplateid' => $data->row()->tagtemplateid,
					'tagtypeid' => $data->row()->tagtypeid,
					'purityid' => $data->row()->purityid,
					'accountid' => $data->row()->accountid,
					'productid' => $data->row()->productid,
					'grossweight' => $data->row()->grossweight,
					'stoneweight' => $data->row()->stoneweight,
					'netweight' => $data->row()->netweight,
					'pieces' => $data->row()->pieces,
					'size' => $data->row()->size,
					'productaddoninfoid' => $data->row()->productaddoninfoid,
					'counterid' => $data->row()->counterid,
					'fromcounterid' => $data->row()->fromcounterid,
					'stockincounterid' => $data->row()->stockincounterid,
					'processproductid' => $data->row()->processproductid,
					'chargeamount' => $data->row()->chargeamount,
					'chargespanamount' => $data->row()->chargespanamount,
					'chargeid' => $data->row()->chargeid,
					'chargecategoryid' => $data->row()->chargecategoryid,
					'chargedetails' => $data->row()->chargedetails,
					'chargespandetails' => $data->row()->chargespandetails,
					'certificationno' => $data->row()->certificationno
				);
				$info['stoneentry'] = $this->retrive_itemtag_stoneentry($previtemtagid);
				$chargeid =  $this->Basefunctions->generalinformaion('product','chargeid','productid',$data->row()->productid);
				$data=$this->db->query("select GROUP_CONCAT(chargecategoryid,':',chargename,':',chargekeyword,':',chargeid) as chargekeyword
						FROM charge
						WHERE chargeid IN ($chargeid)"
						);
				foreach($data->result() as $infores) {
					$info['keyword']= array(
							'chargekeyword'=>$infores->chargekeyword
					);
				}
			}
		} else {
			$info =$this->db->query("SELECT * FROM `salesdetail` WHERE `salesdetailid` = $datarowid ");
		}
		echo json_encode($info);
	}
	// Retrieve Return Items details from sales & sales detail table.
	public function returnitemsgriddynamicdatainfofetch($tablename,$sortcol,$sortord,$pagenum,$rowscount,$primaryid) {
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$rateround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //Rate round-off
		$taxround = 2;
		$dataset = "DATE_FORMAT(sales.salesdate,'%d-%m-%Y') AS salesdate, account.accountname,`sales`.`salesid`, sales.salesnumber, `salesdetail`.`salesdetailid`, `itemtag`.`itemtagnumber`, `product`.`productname`, `purity`.`purityname`, `counter`.`countername`, ROUND(salesdetail.grossweight, '".$round."') as grossweight, ROUND(salesdetail.stoneweight, '".$round."') as stoneweight, ROUND(salesdetail.netweight, '".$round."') as netweight, `salesdetail`.`pieces`, `salesdetail`.`productid`, `salesdetail`.`purityid`, `salesdetail`.`counterid`, `salesdetail`.`employeeid`, `employee`.`employeename`, `salesdetail`.`itemtagid`, ROUND(salesdetail.ratepergram, '".$rateround."') as ratepergram, `salesdetail`.`stocktypeid`, `stocktype`.`stocktypename`, `sizemaster`.`sizemastername`, `salesdetail`.`comment`";
		$join = 'LEFT OUTER JOIN `itemtag` ON `itemtag`.`itemtagid`=`salesdetail`.`itemtagid`';
		$join.= 'LEFT OUTER JOIN `stocktype` ON `stocktype`.`stocktypeid`=`salesdetail`.`stocktypeid`';
		$join.= 'LEFT OUTER JOIN `sizemaster` ON `sizemaster`.`sizemasterid`=`salesdetail`.`orderitemsize`';
		$join.= 'LEFT OUTER JOIN `product` ON `product`.`productid`=`salesdetail`.`productid`';
		$join.= 'LEFT OUTER JOIN `purity` ON `purity`.`purityid`=`salesdetail`.`purityid`';
		$join.= 'LEFT OUTER JOIN `counter` ON `counter`.`counterid`=`salesdetail`.`counterid`';
		$join.= 'LEFT OUTER JOIN `employee` ON `employee`.`employeeid`=`salesdetail`.`employeeid`';
		$join.= 'LEFT OUTER JOIN `sales` ON `sales`.`salesid`=`salesdetail`.`salesid`';
		$join.= 'LEFT OUTER JOIN `account` ON `account`.`accountid`=`sales`.`accountid`';
		$cuscondition = " `salesdetail`.`stocktypeid` = '20' AND salesdetail.taggeditemtagid = 1 AND `salesdetail`.`salesdetailid` NOT IN(0,3) AND ";
		$status = $tablename.'.status='.$this->Basefunctions->activestatus.'';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		/* query */
		$result = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from salesdetail '.$join.' WHERE '.$cuscondition.' '. $status.' ORDER BY '.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$data=array();
		$i=0;
		foreach($result->result() as $row) {
			$salesdetailid = $row->salesdetailid;
			$salesdate = $row->salesdate;
			$salesnumber =$row->salesnumber;
			$accountname =$row->accountname;
			if($row->itemtagid == 1) {
				$stocktypename = 'Untag';
			} else {
				$stocktypename = 'Tag';
			}
			$productname =$row->productname;
			$purityname =$row->purityname;
			$grossweight =$row->grossweight;
			$stoneweight =$row->stoneweight;
			$netweight =$row->netweight;
			$pieces =$row->pieces;
			$data[$i] = array('id'=>$salesdetailid,$salesdate,$salesnumber,$accountname,$stocktypename,$productname,$purityname,$grossweight,$stoneweight,$netweight,$pieces);
			$i++;
		}
		$finalresult = array('0'=>$data,'1'=>$page,'2'=>'json');
		return $finalresult;
	}
	// Retrieve Live Stone details
	public function autostonegriddynamicdatainfofetch($tablename,$sortcol,$sortord,$pagenum,$rowscount,$primaryid,$salesdetailid,$gridstoneids) {
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$rateround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //Rate round-off
		$taxround = 2;
		if(!empty($gridstoneids)) {
			$stoneidcond = "stoneid NOT IN (".$gridstoneids.")";
		} else {
			$stoneidcond = "stoneid NOT IN (1)";
		}
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		/* query */
		$result = $this->db->query('SELECT SQL_CALC_FOUND_ROWS salesstoneentryid, stoneid, stonetypeid, ROUND(COALESCE(SUM(sdpurchaserate),0), '.$amountround.') as purchaserate, ROUND(COALESCE(SUM(sdstonerate),0), '.$amountround.') as stonerate, ROUND(COALESCE(SUM(sdpieces-tagpieces),0),0) as pieces, ROUND(COALESCE(SUM(sdcaratweight-tagcaratweight),0),2) as caratweight, ROUND(COALESCE(SUM(sdgram-taggram),0), '.$round.') as gram, ROUND(COALESCE(SUM(sdtotalnetweight-tagtotalnetweight),0), '.$round.') as totalnetweight, ROUND(COALESCE(sdtotalamount,0), '.$amountround.') as totalamount, stoneentrycalctypeid, stonename, stonetypename, stoneentrycalctypename FROM 
		(SELECT "" as stoneentryid, salesstoneentry.salesstoneentryid, salesstoneentry.stoneid, salesstoneentry.stonetypeid, ROUND(salesstoneentry.purchaserate, '.$amountround.') as sdpurchaserate, ROUND(salesstoneentry.stonerate, '.$amountround.') as sdstonerate, 0 as tagpieces, ROUND(COALESCE(SUM(salesstoneentry.pieces),0),0) as sdpieces, 0 as tagcaratweight, ROUND(COALESCE(SUM(salesstoneentry.caratweight),0),2) as sdcaratweight, 0 as taggram, ROUND(COALESCE(SUM(salesstoneentry.gram),0), '.$round.') as sdgram, 0 as tagtotalnetweight, ROUND(COALESCE(SUM(salesstoneentry.totalnetweight),0), '.$round.') as sdtotalnetweight, 0 as tagtotalamount, ROUND(COALESCE(SUM(salesstoneentry.totalamount),0), '.$amountround.') as sdtotalamount, salesstoneentry.stoneentrycalctypeid, stone.stonename, stonetype.stonetypename, stoneentrycalctype.stoneentrycalctypename FROM salesstoneentry LEFT OUTER JOIN `stone` ON `stone`.`stoneid`=`salesstoneentry`.`stoneid` AND stone.status = 1 LEFT OUTER JOIN `stonetype` ON `stonetype`.`stonetypeid`=`salesstoneentry`.`stonetypeid` AND stonetype.status = 1 LEFT OUTER JOIN `salesdetail` ON `salesdetail`.`salesdetailid`=`salesstoneentry`.`salesdetailid` LEFT OUTER JOIN `stoneentrycalctype` ON `stoneentrycalctype`.`stoneentrycalctypeid`=`salesstoneentry`.`stoneentrycalctypeid` AND stoneentrycalctype.status = 1 WHERE `salesstoneentry`.`salesdetailid` = '.$salesdetailid.' AND `salesdetail`.`salesdetailid` NOT IN(0,3) AND salesstoneentry.'.$stoneidcond.' AND salesstoneentry.status = 1 GROUP BY salesstoneentry.stoneid
		UNION ALL
		SELECT stoneentry.stoneentryid, "" as salesstoneentryid, stoneentry.stoneid, stoneentry.stonetypeid, 0 as sdpurchaserate, 0 as sdstonerate, ROUND(COALESCE(SUM(stoneentry.pieces),0),0) as tagpieces, 0 as sdpieces, ROUND(COALESCE(SUM(stoneentry.caratweight),0),2) as tagcaratweight, 0 as sdcaratweight, ROUND(COALESCE(SUM(stoneentry.gram),0), '.$round.') as taggram, 0 as sdgram, ROUND(COALESCE(SUM(stoneentry.totalnetweight),0), '.$round.') as tagtotalnetweight, 0 as sdtotalnetweight, ROUND(COALESCE(SUM(stoneentry.totalamount),0), '.$amountround.') as tagtotalamount, 0 as  sdtotalamount, stoneentry.stoneentrycalctypeid, stone.stonename, stonetype.stonetypename, stoneentrycalctype.stoneentrycalctypename FROM itemtag LEFT OUTER JOIN stoneentry on stoneentry.itemtagid = itemtag.itemtagid LEFT OUTER JOIN `stone` ON `stone`.`stoneid`=`stoneentry`.`stoneid` AND stone.status = 1 LEFT OUTER JOIN `stonetype` ON `stonetype`.`stonetypeid`=`stoneentry`.`stonetypeid` AND stonetype.status = 1 LEFT OUTER JOIN `stoneentrycalctype` ON `stoneentrycalctype`.`stoneentrycalctypeid`=`stoneentry`.`stoneentrycalctypeid` AND stoneentrycalctype.status = 1 WHERE `itemtag`.`lotid` = '.$primaryid.' AND stoneentry.'.$stoneidcond.' AND stoneentry.status = 1 GROUP BY stoneentry.stoneid) 
		as t GROUP BY stoneid ORDER BY '.' stoneid '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$data=array();
		$i=0;
		foreach($result->result() as $row) {
			$salesstoneentryid = $row->salesstoneentryid;
			$stoneid = $row->stoneid;
			$stonename =$row->stonename;
			$stonetypeid = $row->stonetypeid;
			$stonetypename =$row->stonetypename;
			$stoneentrycalctypeid =$row->stoneentrycalctypeid;
			$stoneentrycalctypename =$row->stoneentrycalctypename;
			$purchaserate =$row->purchaserate;
			$basicrate =$row->stonerate;
			$stonepieces =$row->pieces;
			$stonegram =$row->gram;
			$caratweight =$row->caratweight;
			$totalweight =$row->totalnetweight;
			$stonetotalamount =$row->totalamount;
			$data[$i] = array('id'=>$salesstoneentryid,$stoneid,$stonename,$stonetypeid,$stonetypename,$stoneentrycalctypeid,$stoneentrycalctypename,$purchaserate,$basicrate,$stonepieces,$stonegram,$caratweight,$totalweight,$stonetotalamount);
			$i++;
		}
		$finalresult = array('0'=>$data,'1'=>$page,'2'=>'json');
		return $finalresult;
	}
	// Update Scenario for old customers
	public function updatediamondcaratweightdetails() {
		$info =$this->db->query("SELECT itemtag.itemtagid, COALESCE(ROUND(SUM(stoneentry.caratweight),2),0) as diacaratweight, COALESCE(ROUND(SUM(stoneentry.pieces),0),0) as diapieces from itemtag LEFT OUTER JOIN stoneentry ON stoneentry.itemtagid = itemtag.itemtagid AND stoneentry.status = 1 AND stoneentry.stonetypeid = 2 WHERE itemtag.itemtagid > 1 Group by itemtag.itemtagid");
		if($info->num_rows() > 0) {
			foreach($info->result() as $infodata) {
				$itemtagdiamonddetailsupdate = array(
					'diacaratweight' => $infodata->diacaratweight,
					'diapieces' => $infodata->diapieces
				);
				$itemtagdiamonddetailsupdate = array_merge($itemtagdiamonddetailsupdate);
				$this->db->where('itemtagid',$infodata->itemtagid);
				$this->db->update('itemtag',$itemtagdiamonddetailsupdate);
			}
		}
	}
	public function updatestonecaratweightdetails() {
		$info =$this->db->query("SELECT itemtag.itemtagid, COALESCE(ROUND(SUM(stoneentry.caratweight),2),0) as stonecaratweight, COALESCE(ROUND(SUM(stoneentry.pieces),0),0) as stonepieces from itemtag LEFT OUTER JOIN stoneentry ON stoneentry.itemtagid = itemtag.itemtagid AND stoneentry.status = 1 AND stoneentry.stonetypeid NOT IN (2) WHERE itemtag.itemtagid > 1 Group by itemtag.itemtagid");
		if($info->num_rows() > 0) {
			foreach($info->result() as $infodata) {
				$itemtagdiamonddetailsupdate = array(
					'stonecaratweight' => $infodata->stonecaratweight,
					'stonepieces' => $infodata->stonepieces
				);
				$itemtagdiamonddetailsupdate = array_merge($itemtagdiamonddetailsupdate);
				$this->db->where('itemtagid',$infodata->itemtagid);
				$this->db->update('itemtag',$itemtagdiamonddetailsupdate);
			}
		}
	}
	// Update Tag Image details in salesdetail table
	public function updatetagimagedetails() {
		$info =$this->db->query("SELECT itemtag.itemtagid,itemtag.tagimage from itemtag WHERE itemtag.itemtagid > 1 AND tagtypeid IN (2,4,5) Group by itemtag.itemtagid");
		if($info->num_rows() > 0) {
			foreach($info->result() as $infodata) {
				$tagimagedetails = array(
					'tagimage' => $infodata->tagimage
				);
				$tagimagedetails = array_merge($tagimagedetails);
				$this->db->where('itemtagid',$infodata->itemtagid);
				$this->db->update('salesdetail',$tagimagedetails);
			}
		}
	}
}