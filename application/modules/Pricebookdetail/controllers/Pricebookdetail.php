<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pricebookdetail extends MX_Controller{
    public function __construct() {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Pricebookdetail/Pricebookdetailmodel');	
    }
    //first basic hitting view
    public function index() {        
    	$moduleid = array(219);
    	sessionchecker($moduleid);
		/*action*/
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(219);
		$viewmoduleid = array(219);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
      	$this->load->view('Pricebookdetail/pricebookdetailview',$data);
	}
	//create price book detail
	public function newdatacreate() {  
    	$this->Pricebookdetailmodel->newdatacreatemodel();
    }
	//information fetch price book detail
	public function fetchformdataeditdetails() {
		$moduleid = 219;
		$this->Pricebookdetailmodel->informationfetchmodel($moduleid);
	}
	//update price book detail
    public function datainformationupdate() {
        $this->Pricebookdetailmodel->datainformationupdatemodel();
    }
	//delete price book detail
    public function deleteinformationdata() {
		$moduleid = 219;       
	   $this->Pricebookdetailmodel->deleteoldinformation($moduleid);
    }
	//price book detail get_browser
	public function productdetialget() {
		 $this->Pricebookdetailmodel->productdetialgetmodel();	
	}
}