<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<div class="large-12 columns headercaptionstyle headerradius paddingzero">
				<div class="large-6 medium-6 small-12 columns headercaptionleft">
					<span data-activates="slide-out" class="headermainmenuicon button-collapse" title="Menu"><i class="material-icons">menu</i></span>
					<span class="gridcaptionpos">
						<?php echo span($maingridtitle,$spanattr); ?> <span>-</span> <span id="viewgridheaderid"></span>
					</span>
				</div>
				<div class="large-5 medium-5 small-12 columns foriconcolor righttext small-only-text-center"> 
					<!-- Action menu generation -->
					<?php
						$size = 10;
						$value = array();
						$des = array();
						$title = array();
						foreach($actionassign as $key):
							$value[$key->toolbarnameid] = $key->toolbarname;
							$des[] = $key->description;
							$title[] = $key->toolbartitle;
						endforeach;
						echo actionmenu($value,$size,$des,$title,'pricebookdetail','icon-w ');
						//user menu
						$this->load->view('Base/usermenugenerate');
					?>
				</div>
			</div>
			<!-- tab group creation -->
			<div class="large-12 columns tabgroupstyle">
				 <?php
					$ulattr = array("class"=>"tabs");
					$uladdinfo = "data-tab";
					$tabgrpattr = array("class"=>"tab-title sidebaricons");
					$tabstatus = "active";
					$dataname = "subform";
					echo tabgroupgenerate($ulattr,$uladdinfo,$tabgrpattr,$tabstatus,$dataname,$modtabgrp);
				?>
			</div>
			
			<?php
				//function call for form fields generation
				formfieldsgeneratorwithgrid($modtabsecfrmfkdsgrp,$filedmodids);
			?>
		</div>
	</div>
</div>
<!--Overlay-->			
<div>
	<?php $this->load->view('Base/viewselectionoverlay'); ?> 
</div>