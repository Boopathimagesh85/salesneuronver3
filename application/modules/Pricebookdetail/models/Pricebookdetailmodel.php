<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pricebookdetailmodel extends CI_Model{
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//price book create
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['pricebookdetailelementsname']);
		$formfieldstable = explode(',',$_POST['pricebookdetailelementstable']);
		$formfieldscolmname = explode(',',$_POST['pricebookdetailelementscolmn']);
		$elementpartable = explode(',',$_POST['pricebookdetailelementspartabname']);
		 //filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
		echo 'TRUE'; 
	}
	//Retrieve price book data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['pricebookdetailelementsname']);
		$formfieldstable = explode(',',$_GET['pricebookdetailelementstable']);
		$formfieldscolmname = explode(',',$_GET['pricebookdetailelementscolmn']);
		$elementpartable = explode(',',$_GET['pricebookdetailelementspartabname']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['pricebookdetailprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	//price book update
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['pricebookdetailelementsname']);
		$formfieldstable = explode(',',$_POST['pricebookdetailelementstable']);
		$formfieldscolmname = explode(',',$_POST['pricebookdetailelementscolmn']);
		$elementpartable = explode(',',$_POST['pricebookdetailelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['pricebookdetailprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		echo $result;
	}
	//price book delete
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['pricebookdetailelementstable']);
		$parenttable = explode(',',$_GET['pricebookdetailelementspartabname']);
		$id = $_GET['pricebookdetailprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				echo "TRUE";
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			echo "TRUE";
		}
	} 
	//price book detail  -- product value get_browser
	public function productdetialgetmodel() {
		$id = $_GET['pid'];
		$this->db->select('description,unitprice');
		$this->db->from('product');
		$this->db->where('product.productid',$id);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$data = array('des'=>$row->description,'unit'=>$row->unitprice);
			}
			echo json_encode($data);
		}
	}
}