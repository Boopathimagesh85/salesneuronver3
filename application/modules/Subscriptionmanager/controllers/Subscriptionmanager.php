<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Subscriptionmanager extends MX_Controller 
{
    //Constructor Function To Load Models
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Subscriptionmanager/Subscriptionmanagermodel');
		$this->load->view('Base/formfieldgeneration');
	}
	//Default View 
	public function index() {
		$this->Subscriptionmanagermodel->checksubscriptionstatus();
		$moduleid = array(5);
		sessionchecker($moduleid);
		//action
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(5);
		$viewmoduleid = array(5);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['userdata']=$this->Subscriptionmanagermodel->subcribeuserinfo();
		$data['currency']=$this->Subscriptionmanagermodel->subscribecurrency();
		$data['billinfo']=$this->Subscriptionmanagermodel->billingdetails();
		$data['subscripinfo']=$this->Subscriptionmanagermodel->subscribedetails();
		$data['addonamtinfo']=$this->Subscriptionmanagermodel->addonsubscribedetails();
		$this->Subscriptionmanagermodel->inlinedefaultdbenable();
		$data['demodata']=$this->Subscriptionmanagermodel->checkdemodatadetails();
		$this->load->view('Subscriptionmanager/subscriptionmanagerview',$data);	
	}
	//invoice deatails
	public function paymentinvoicedetails() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'salesneuronpaymenthistory.salesneuronpaymenthistoryid') : 'salesneuronpaymenthistory.salesneuronpaymenthistoryid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('invoicenumber','transactiondate','planname','planduration','paymenttype','currencyname','transactionamount'),'colmodelindex'=>array('salesneuronpaymenthistory.invoicenumber','salesneuronpaymenthistory.transactiondate','plan.planname','salesneuronpaymenthistory.planduration','salesneuronpaymenthistory.paymenttype','currency.currencyname','salesneuronpaymenthistory.transactionamount'),'coltablename'=>array('salesneuronpaymenthistory','salesneuronpaymenthistory','plan','salesneuronpaymenthistory','salesneuronpaymenthistory','currency','salesneuronpaymenthistory'),'uitype'=>array('2','2','2','2','2','2','2'),'colname'=>array('Invoice Number','Invoice Date','Plan Name','Plan Duration','Payment Mode','Currency Name','Amount'),'colsize'=>array('200','200','150','200','200','200','100'));
		$result=$this->Subscriptionmanagermodel->paymentinvoicefetchgridxmlview($primarytable,$sortcol,$sortord,$pagenum,$rowscount);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Invoice',$width,$height);
		} else {
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Invoice',$width,$height);
		}
		echo json_encode($datas);
	}
	//Add-ons deatails
	public function addonsinvoicedetails() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'salesneuronpaymentdetails.salesneuronpaymentdetailsid') : 'salesneuronpaymentdetails.salesneuronpaymentdetailsid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array(
				'colmodelname'=>array('transactionid','transactiondate','prodcutdescription','productquantity','creditavailable','currencyname','productamount'),
				'colmodelindex'=>array('salesneuronpaymenthistory.transactionid','salesneuronpaymenthistory.transactiondate','salesneuronpaymentdetails.prodcutdescription','salesneuronpaymentdetails.productquantity','salesneuronaddonscredit.addonavailablecredit','currency.currencyname','salesneuronpaymentdetails.productamount'),
				'coltablename'=>array('salesneuronpaymenthistory','salesneuronpaymenthistory','plan','salesneuronpaymenthistory','salesneuronpaymenthistory','currency','salesneuronpaymentdetails'),
				'uitype'=>array('2','2','2','2','2','2','2'),
				'colname'=>array('Invoice Number','Invoice Date','Add-On Name','Last Purchased Credits','Available Credits','Currency Name','Amount'),
				'colsize'=>array('180','150','150','230','200','200','150')
		);
		$result=$this->Subscriptionmanagermodel->addonsinvoicefetchgridxmlview($primarytable,$sortcol,$sortord,$pagenum,$rowscount);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Addons List',$width,$height);
		} else {
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Addons List',$width,$height);
		}
		echo json_encode($datas);
	}
	//get billing details
	public function getbilldetails() {
		$result = $this->Subscriptionmanagermodel->billingdetails();
		echo json_encode($result);
	}
	//update billing information
	public function billdetailsupdate() {
		$this->Subscriptionmanagermodel->billdetailsupdatemodel();
	}
	//cancel crm account
	public function cancelaccount() {
		$this->Subscriptionmanagermodel->cancelaccountmodel();
	}
	//clear demo data
	public function cleardemodata() {
		$this->Subscriptionmanagermodel->cleardemodatamodel();
	}
}