<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Subscriptionmanagermodel extends CI_Model
{
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//get emp information
	public function subcribeuserinfo() {
		$tzone = $this->Basefunctions->employeetimezoneset($this->Basefunctions->logemployeeid);
		$datasets = array('name'=>'','email'=>'','userid'=>'','zone'=>'');
		$dataset=$this->db->select('employeeid,employeename,lastname,emailid,salutation.salutationname')->from('employee')->join('salutation','salutation.salutationid=employee.salutationid')->where('employee.employeeid',$this->Basefunctions->logemployeeid)->get();
		foreach($dataset->result() as $row) {
			$name=$row->salutationname.$row->employeename.$row->lastname;
			$email=$row->emailid;
			$empid = $row->employeeid;
			$datasets = array('name'=>$name,'email'=>$email,'userid'=>$empid,'zone'=>$tzone['name']);
		}
		return $datasets;
	}
	//subscriber currency
	public function subscribecurrency()	{
		$cid = 120;
		$curinfo = array('currid'=>$cid,'cursymbol'=>'$','code'=>'USD');
		$empid = $this->Basefunctions->userid;
		$compquery = $this->db->query('select company.companyid,company.currencyid,currency.symbol,currency.currencycode from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid join currency ON currency.currencyid=company.currencyid where employeeid='.$empid.'')->result();
		foreach($compquery as $key) {
			$curinfo = array('currid'=>$key->currencyid,'cursymbol'=>$key->symbol,'code'=>$key->currencycode);
		}
		return $curinfo;
	}
	//subscriber master company
	public function usermastercompanyid()	{
		$mcid = 1;
		$empid = $this->Basefunctions->userid;
		$compquery = $this->db->query('select company.companyid,company.mastercompanyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.'')->result();
		foreach($compquery as $key) {
			$mcid = $key->mastercompanyid;
		}
		return $mcid;
	}
	//Invoice information
	public function paymentinvoicefetchgridxmlview($tablename,$sortcol,$sortord,$pagenum,$rowscount) {
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		$mcid = $this->usermastercompanyid();
		$mdbname = $this->db->masterdb;
		$masdb=$this->inlineuserdatabaseactive($mdbname);
		$dataset = 'salesneuronpaymenthistory.salesneuronpaymenthistoryid,salesneuronpaymenthistory.planusers,salesneuronpaymenthistory.planduration,salesneuronpaymenthistory.paymenttype,salesneuronpaymenthistory.transactionid,salesneuronpaymenthistory.invoicenumber,DATE_FORMAT(salesneuronpaymenthistory.transactiondate,"%d-%m-%Y %h:%i:%s %p") AS transactiondate,salesneuronpaymenthistory.transactionamount,plan.planname,currency.currencyname';
		$join=' LEFT OUTER JOIN plan ON plan.planid=salesneuronpaymenthistory.planid';
		$join.=' LEFT OUTER JOIN currency ON currency.currencyid=salesneuronpaymenthistory.transactioncurrencyid';
		$where = $tablename.'.mastercompanyid='.$mcid;
		$status = $tablename.'.status NOT IN (0,3)';
		/* query */
		$result = $masdb->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$status.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		$page = $this->paginationgenerate($pagenum,$rowscount,$masdb);
		$data=array();
		$i=0;
		foreach($result->result() as $row) {
			$data[$i]=array('id'=>$row->salesneuronpaymenthistoryid,$row->invoicenumber,$row->transactiondate,$row->planname,($row->planduration==1)?'Monthly':'Annually',$row->paymenttype,$row->currencyname,$row->transactionamount);
			$i++;
		}
		$finalresult=array('0'=>$data,'1'=>$page,'2'=>'json');
		return $finalresult;
	}
	//generate page
	public function paginationgenerate($curpage,$limit,$masdb) {
		$count= $masdb->query('SELECT FOUND_ROWS() count;')->row()->count;
		if($limit == 'ALL') {
			$limit=$count;
		} else {
			$limit=$limit;
		}
		if($count >0) {
			$total_pages = ceil($count/$limit);
		} else {
			$total_pages = 0;
		}
		$arr=array('records'=>$limit,'curpage'=>$curpage,'totalpage'=>$total_pages,'recordcount'=>$count);
		return $arr;
	}
	//add-ons billing information
	public function addonsinvoicefetchgridxmlview($tablename,$sortcol,$sortord,$pagenum,$rowscount) {
		$mcid = $this->usermastercompanyid();
		$mdbname = $this->db->masterdb;
		$masdb=$this->inlineuserdatabaseactive($mdbname);
		$datainfo = array();
		$page = $this->paginationgenerate($pagenum,$rowscount,$masdb);
		//get all the addons id
		$i=0;
		$addoninfo = $masdb->query('select addonspaymentinfo.addonspaymentinfoid,addonspaymentinfo.addonstypeid from addonspaymentinfo group by addonstypeid order by addonstypeid');
		foreach($addoninfo->result() as $datas) {
			$this->db->select('SQL_CALC_FOUND_ROWS salesneuronpaymentdetails.salesneuronpaymentdetailsid,salesneuronpaymentdetails.productamount,salesneuronpaymentdetails.productquantity,salesneuronpaymentdetails.prodcutdescription,currency.currencyname,DATE_FORMAT(salesneuronpaymenthistory.transactiondate,"%d-%m-%Y %h:%i:%s %p") AS transactiondate,salesneuronaddonscredit.addonavailablecredit,salesneuronpaymenthistory.transactionid,salesneuronpaymenthistory.invoicenumber',false);
			$this->db->from('salesneuronpaymentdetails');
			$this->db->join('salesneuronpaymenthistory','salesneuronpaymenthistory.salesneuronpaymenthistoryid=salesneuronpaymentdetails.salesneuronpaymenthistoryid');
			$this->db->join('currency','currency.currencyid=salesneuronpaymenthistory.transactioncurrencyid');
			$this->db->join('salesneuronaddonscredit','salesneuronaddonscredit.addonstypeid=salesneuronpaymentdetails.productid','left outer');
			$this->db->where('salesneuronpaymentdetails.mastercompanyid',$mcid);
			$this->db->where('salesneuronpaymentdetails.producttypeid','2');
			$this->db->where('salesneuronpaymentdetails.productid',$datas->addonstypeid);
			$this->db->where_not_in('salesneuronpaymenthistory.status',array(3,0));
			$this->db->where_not_in('salesneuronpaymentdetails.status',array(3,0));
			$this->db->order_by($sortcol,$sortord);
			$this->db->limit(1);
			$result=$this->db->get();
			foreach($result->result() as $dataset) {
				$amt = round($row['productquantity']*$row['productamount'],2);
				$datainfo[$i]['id']=$dataset->salesneuronpaymentdetailsid;
				$datainfo[$i][]=$dataset->invoicenumber;
				$datainfo[$i][]=$dataset->transactiondate;	
				$datainfo[$i][]=$dataset->prodcutdescription;
				$datainfo[$i][]=$dataset->productquantity;
				$datainfo[$i][]=$dataset->addonavailablecredit;				
				$datainfo[$i][]=$dataset->currencyname;
				$datainfo[$i][]=$amt;
				$i++;
			}
		}
		$finalresult=array($datainfo,$page,'json');
		return $finalresult;
	}
	//billing details
	public function billingdetails() {
		$cname="";
		$mcid = 1;
		$empid = $this->Basefunctions->userid;
		$compquery = $this->db->query('select company.companyid,company.companyname,company.mastercompanyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.'')->result();
		foreach($compquery as $key) {
			$cname = $key->companyname;
			$mcid = $key->mastercompanyid;
		}
		$billinfo=array('cname'=>$cname,'billpersonname'=>'','email'=>'','billmobilenum'=>'','billaddr'=>'','billcity'=>'','billstate'=>'','billcountry'=>'','billzip'=>'','billusers'=>'1','billcost'=>'0');
		$this->db->select('individualname,emailid,phone,billingaddress,billingcity,billingstate,billingcountry,billingpostalcode,billingusers,billingcost');
		$this->db->from('billinginformation');
		$this->db->where('billinginformation.mastercompanyid',$mcid);
		$billdatainfo=$this->db->get();
		foreach($billdatainfo->result() as $billdata) {
			$billinfo['billpersonname'] = $billdata->individualname;
			$billinfo['email'] = $billdata->emailid;
			$billinfo['billmobilenum'] = $billdata->phone;
			$billinfo['billaddr'] = $billdata->billingaddress;
			$billinfo['billcity'] = $billdata->billingcity;
			$billinfo['billstate'] = $billdata->billingstate;
			$billinfo['billcountry'] = $billdata->billingcountry;
			$billinfo['billzip'] = $billdata->billingpostalcode;
			$billinfo['billusers'] = $billdata->billingusers;
			$billinfo['billcost'] = $billdata->billingcost;
		}
		return $billinfo;
	}
	//subscription information
	public function subscribedetails() {
		$tzone = $this->Basefunctions->employeetimezoneset($this->Basefunctions->userid);
		date_default_timezone_set($tzone['name']);
		$mcid = $this->usermastercompanyid();
		//user emailid information
		$emailid=$this->Basefunctions->generalinformaion('employee','emailid','employeeid',$this->Basefunctions->userid);
		$mdbname = $this->db->masterdb;
		$masdb=$this->inlineuserdatabaseactive($mdbname);
		//user created date and activation date
		$masdb->select('userplaninfo.userplaninfoid,DATE_FORMAT(userplaninfo.accountcreationdate,"%a, %d %b,%Y") AS accountcreationdate,DATE_FORMAT(userplaninfo.expiredate,"%a, %d %b,%Y") AS expiredate,userplaninfo.expiredate AS accexpiredate,DATE_FORMAT(userplaninfo.activationdate,"%a, %d %b,%Y") AS activationdate,userplaninfo.activationdate AS accactivedate,plan.planname,plan.planid',false);
		$masdb->from('userplaninfo');
		$masdb->join('plan','plan.planid=userplaninfo.planid');
		$masdb->where('userplaninfo.registeremailid',$emailid);
		$userdataset = $masdb->get();
		foreach($userdataset->result() as $dataset) {
			$acstartdate = $dataset->accountcreationdate;
			$pname = $dataset->planname;
			$pnameid = $dataset->planid;
			$expdate = $dataset->expiredate;
			$actdate = $dataset->activationdate;
			$accexpdate = $dataset->accexpiredate;
			$acccredate = $dataset->accactivedate;
		}
		//remaining days left
		$edate = strtotime($accexpdate);
		$diff=$edate-time();
		$dayleft = floor($diff/(60*60*24));
		$sbuscrpdatasets = array('accstartdate'=>$acstartdate,'cplan'=>$pname,'cplanid'=>$pnameid,'payduration'=>'15 Days','plancost'=>'0','userscount'=>'1','planamt'=>'0','addonamount'=>'0','totalplanamt'=>'0','lapidamont'=>'0','lapaiddate'=>'','lapaymode'=>'','rendate'=>$expdate,'actdate'=>$actdate,'daysleft'=>$dayleft,'paydurid'=>'15','cursymbol'=>'','acccreatedate'=>$acccredate,'accexpdate'=>$accexpdate);
		//payment history information
		$masdb->select('SQL_CALC_FOUND_ROWS  salesneuronpaymenthistory.salesneuronpaymenthistoryid,salesneuronpaymenthistory.planusers,salesneuronpaymenthistory.planduration,salesneuronpaymenthistory.paymenttype,salesneuronpaymenthistory.transactionid,salesneuronpaymenthistory.invoicenumber,DATE_FORMAT(salesneuronpaymenthistory.transactiondate,"%a, %d %b,%Y %h:%i %p") AS transactiondate,salesneuronpaymenthistory.transactionamount,salesneuronpaymenthistory.planamount,salesneuronpaymenthistory.addonamount,plan.planname,salesneuronpaymenthistory.planid,currency.currencyname,currency.symbol',false);
		$masdb->from('salesneuronpaymenthistory');
		$masdb->join('plan','plan.planid=salesneuronpaymenthistory.planid');
		$masdb->join('currency','currency.currencyid=salesneuronpaymenthistory.transactioncurrencyid','left outer');
		$masdb->where('salesneuronpaymenthistory.mastercompanyid',$mcid);
		$masdb->where_not_in('salesneuronpaymenthistory.planamount',array('0.00'));
		$masdb->where_not_in('salesneuronpaymenthistory.status',array(3,0));
		$masdb->order_by('salesneuronpaymenthistory.salesneuronpaymenthistoryid','DESC');
		$masdb->limit('1','0');
		$result=$masdb->get();
		foreach($result->result() as $phdatasets) {
			$sbuscrpdatasets['payduration'] = (($phdatasets->planduration)==1?'Monthly':'Annually');
			$pcost = ($phdatasets->planamount/$phdatasets->planduration);
			$sbuscrpdatasets['plancost'] = $pcost;
			$sbuscrpdatasets['userscount'] = $phdatasets->planusers;
			$pamount = ($phdatasets->planamount*$phdatasets->planusers);
			$sbuscrpdatasets['planamt'] = $pamount;
			$sbuscrpdatasets['addonamount'] = $phdatasets->addonamount;
			$sbuscrpdatasets['lapidamont'] = $phdatasets->transactionamount;
			$sbuscrpdatasets['totalplanamt'] = ($pamount+$phdatasets->addonamount);
			$sbuscrpdatasets['lapaiddate'] = $phdatasets->transactiondate;
			$sbuscrpdatasets['lapaymode'] = $phdatasets->paymenttype;
			$sbuscrpdatasets['cplan'] = $phdatasets->planname;
			$sbuscrpdatasets['cplanid'] = $phdatasets->planid;
			$sbuscrpdatasets['paydurid'] = $phdatasets->planduration;
			$sbuscrpdatasets['cursymbol'] = $phdatasets->symbol;
		}
		return $sbuscrpdatasets;
	}
	//add on subscription information
	public function addonsubscribedetails() {
		$this->inlinedefaultdbenable();
		$mcid = $this->usermastercompanyid();
		$mdbname = $this->db->masterdb;
		$masdb=$this->inlineuserdatabaseactive($mdbname);
		$datainfo = array();
		//get all the addons id
		$i=0;
		$amt=0;
		$addoninfo = $masdb->query('select addonspaymentinfo.addonspaymentinfoid,addonspaymentinfo.addonstypeid from addonspaymentinfo group by addonstypeid order by addonstypeid');
		foreach($addoninfo->result() as $datas) {
			$this->db->select('SQL_CALC_FOUND_ROWS salesneuronpaymentdetails.salesneuronpaymentdetailsid,salesneuronpaymentdetails.productamount,salesneuronpaymentdetails.productquantity,salesneuronpaymentdetails.prodcutdescription,currency.currencyname,DATE_FORMAT(salesneuronpaymenthistory.transactiondate,"%d-%m-%Y %h:%i:%s") AS transactiondate',false);
			$this->db->from('salesneuronpaymentdetails');
			$this->db->join('salesneuronpaymenthistory','salesneuronpaymenthistory.salesneuronpaymenthistoryid=salesneuronpaymentdetails.salesneuronpaymenthistoryid');
			$this->db->join('currency','currency.currencyid=salesneuronpaymenthistory.transactioncurrencyid');
			$this->db->where('salesneuronpaymentdetails.mastercompanyid',$mcid);
			$this->db->where('salesneuronpaymentdetails.producttypeid','2');
			$this->db->where('salesneuronpaymentdetails.productid',$datas->addonstypeid);
			$this->db->where_not_in('salesneuronpaymenthistory.status',array(3,0));
			$this->db->where_not_in('salesneuronpaymentdetails.status',array(3,0));
			$this->db->order_by('salesneuronpaymentdetails.salesneuronpaymentdetailsid','DESC');
			$this->db->limit(1);
			$result=$this->db->get();
			foreach($result->result() as $dataset) {
				$amt=$amt+($dataset->productamount*$dataset->productquantity);
			}
		}
		$addonamtinfo = array('addonamount'=>round($amt,2));
		return $addonamtinfo;
	}
	//check demo data clear details
	public function checkdemodatadetails() {
		$compid = 1;
		$empid = $this->Basefunctions->userid;
		$compquery = $this->db->query('select company.companyid,company.mastercompanyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.'')->result();
		foreach($compquery as $key) {
			$compid = $key->companyid;
		}
		//get demo data details
		$demodatas = 'Yes';
		$demodataset = $this->db->select('generalsettingid,demodata')->from('generalsetting')->where('companyid',$compid)->get();
		foreach($demodataset->result() as $datas) {
			$demodatas=$datas->demodata;
		}
		return $demodatas;
	}
	//update billing details
	public function billdetailsupdatemodel() {
		$mcid = $this->usermastercompanyid();
		$date = date($this->Basefunctions->datef);
		//update information in client db
		$billinfo = array(
			'individualname'=>$_POST['billpersonname'],
			'phone'=>$_POST['billmobilenum'],
			'billingaddress'=>$_POST['billaddr'],
			'billingcity'=>$_POST['billcity'],
			'billingstate'=>$_POST['billstate'],
			'billingcountry'=>$_POST['billcountry'],
			'billingpostalcode'=>$_POST['billzip'],
			'lastupdatedate'=>$date,
			'lastupdateuserid'=>$this->Basefunctions->userid
		);
		$this->db->where('billinginformation.mastercompanyid',$mcid);
		$this->db->update('billinginformation',$billinfo);
		//update information in master db
		$mdbname = $this->db->masterdb;
		$serverdb=$this->inlineuserdatabaseactive($mdbname);
		$serverdb->where('billinginformation.mastercompanyid',$mcid);
		$serverdb->update('billinginformation',$billinfo);
		sleep(1);
		$this->inlinedefaultdbenable();
		echo 'TRUE';
	}
	//cancel account
	public function cancelaccountmodel() {
		$password = $_POST['canaccountpasswd'];
		$reason = $_POST['cancelaccreason'];
		$comts = $_POST['cancelacccomments'];
		//user information
		$empid=$this->Basefunctions->userid;
		$mascompid = $this->usermastercompanyid();
		$this->inlinedefaultdbenable();
		$this->db->select('employee.employeeid,employee.password,employee.emailid,employee.employeename');
		$this->db->from('employee');
		$this->db->where('employeeid',$empid);
	    $this->db->limit(1);
		$query = $this->db->get();
		if($query->num_rows() >= 1) {
			foreach($query->result() as $datas) {
				$email = $datas->emailid;
				$pwd = $datas->password;
				$name = $datas->employeename;
				if ( password_verify($password, $pwd)) {
					//deactive account
					$date = date($this->Basefunctions->datef);
					$cdate = date('jS M,Y');
					$cdbname = $this->db->masterdb;
					$masdb=$this->inlineuserdatabaseactive($cdbname);
					$masdb->where('userplaninfo.companyid',$mascompid);
					$masdb->update('userplaninfo',array('accountclosedate'=>$date,'status'=>5));
					//update cancel account information
					$canaccountinfo = array(
										'companyid'=>$mascompid,
										'employeeid'=>$empid,
										'emailid'=>$email,
										'cancelreason'=>$reason,
										'cancelcomments'=>$comts,
										'canceldate'=>$date,
										'status'=>1
									);
					$masdb->insert('cancelaccountinfo',$canaccountinfo);
					$mstatus = $this->sendaccountcancelmail($email,$cdate,$name);
					sleep(1);
					$this->inlinedefaultdbenable();
					echo 'TRUE';
				} else {
					echo 'Invalid Password.';
				}
			}
		} else {
			echo 'Authentication failed.';
		}
	}
	//send account cancellation mail
	public function sendaccountcancelmail($email,$date,$cname) {
		$this->load->helper('phpmailer');
		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->SMTPAuth   = true;
		$mail->SMTPSecure = "tls";
		$mail->Username   = "arvind@salesneuron.com";
		$mail->Password   = "X9WnMXpxhC8YEOUPuQ3oZw";
		$mail->Host       = "smtp.mandrillapp.com";
		$mail->Port       = 587;
		$mail->SetFrom('noreply@salesneuron.com','Sales Neuron');
		$mail->Subject   = "Sales Neuron: Account Cancellation Notification";
		$secure = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "");
		$root = (($_SERVER['HTTP_HOST']!='localhost')?"http".$secure."://".$_SERVER['HTTP_HOST'] : "http".$secure."://".$_SERVER['HTTP_HOST'].'/salesneuron');
		//new
		$body='<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
<title>Destiny</title>';
		$body.="
<link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
<style type='text/css'>
div, p, a, li, td { -webkit-text-size-adjust:none; }
*{
-webkit-font-smoothing: antialiased;
-moz-osx-font-smoothing: grayscale;
}
.ReadMsgBody
{width: 100%; background-color: #ffffff;}
.ExternalClass
{width: 100%; background-color: #ffffff;}
body{width: 100%; height: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;}
html{width: 100%; background-color: #ffffff;}
@font-face {font-family: 'proxima_novalight';src: url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_nova_rgregular'; src: url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_novasemibold';src: url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_nova_rgbold';src: url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_novathin';src: url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_novaextrabold';src: url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.woff2') format('woff2'),url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
p {padding: 0!important; margin-top: 0!important; margin-right: 0!important; margin-bottom: 0!important; margin-left: 0!important; }
.hover:hover {opacity:0.85;filter:alpha(opacity=85); }
.jump:hover {
	opacity:0.75;
	filter:alpha(opacity=75);
	padding-top: 10px!important;
}
a#rotator img {
-webkit-transition: all 1s ease-in-out;
-moz-transition: all 1s ease-in-out; 
-o-transition: all 1s ease-in-out; 
-ms-transition: all 1s ease-in-out; 
}
a#rotator img:hover { 
-webkit-transform: rotate(360deg); 
-moz-transform: rotate(360deg); 
-o-transform: rotate(360deg);
-ms-transform: rotate(360deg); 
}
@-webkit-keyframes spaceboots {
	0% { -webkit-transform: translate(2px, 1px) rotate(0deg); }
	10% { -webkit-transform: translate(-1px, -2px) rotate(-1deg); }
	20% { -webkit-transform: translate(-3px, 0px) rotate(1deg); }
	30% { -webkit-transform: translate(0px, 2px) rotate(0deg); }
	40% { -webkit-transform: translate(1px, -1px) rotate(1deg); }
	50% { -webkit-transform: translate(-1px, 2px) rotate(-1deg); }
	60% { -webkit-transform: translate(-3px, 1px) rotate(0deg); }
	70% { -webkit-transform: translate(2px, 1px) rotate(-1deg); }
	80% { -webkit-transform: translate(-1px, -1px) rotate(1deg); }
	90% { -webkit-transform: translate(2px, 2px) rotate(0deg); }
	100% { -webkit-transform: translate(1px, -2px) rotate(-1deg); }
}
#shake:hover,
#shake:focus {
	-webkit-animation-name: spaceboots;
	-webkit-animation-duration: 0.8s;
	-webkit-transform-origin:50% 50%;
	-webkit-animation-iteration-count: infinite;
	-webkit-animation-timing-function: linear;
}
.image600 img {width: 600px; height: auto;}
.image595 img {width: 595px; height: auto;}
.icon27 img {width: 27px; height: auto;}
.icon24 img {width: 24px; height: auto;}
.image254 img {width: 254px; height: auto;}
.image176 img {width: 176px; height: auto;}
.image260 img {width: 260px; height: auto;}
.avatar96 img {width: 96px; height: auto;}
.icons46 img {width: 46px; height: auto;}
.image248 img {width: 246px; height: auto;}
#logo img {width: 120px; height: auto;}
#icon18 img {width: 18px; height: auto;}
</style>
<!-- @media only screen and (max-width: 640px) 
{*/
-->
<style type='text/css'> @media only screen and (max-width: 640px){
		body{width:auto!important;}
		table[class=full] {width: 100%!important; clear: both; }
		table[class=mobile] {width: 100%!important; padding-left: 30px; padding-right: 30px; clear: both; }
		table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
		td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
		*[class=erase] {display: none;}
		*[class=buttonScale] {float: none!important; text-align: center!important; display: inline-block!important; clear: both;}
		.h60 {height: 60px!important;}
		.h30 {height: 30px!important;}
		.h10 {height: 10px!important;}
		.h15 {height: 15px!important;}
		td[class=image600] img {width: 100%!important; text-align: center!important; clear: both; }
		td[class=image595] img {width: 100%!important; text-align: center!important; clear: both; }
		table[class=sponsor] {text-align:center; float:none; width:70%!important;}
} </style>
<!--
@media only screen and (max-width: 479px) 
{
-->
<style type='text/css'>
@media only screen and (max-width: 479px){
	body{width:auto!important;}
	table[class=full] {width: 100%!important; clear: both; }
	table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
	table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
	td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
	*[class=erase] {display: none;}
	*[class=buttonScale] {float: none!important; text-align: center!important; display: inline-block!important; clear: both;}
	.h60 {height: 60px!important;}
	.h30 {height: 30px!important;}
	.h10 {height: 10px!important;}
	.h15 {height: 15px!important;}
	td[class=image600] img {width: 100%!important; text-align: center!important; clear: both; }
	td[class=image595] img {width: 100%!important; text-align: center!important; clear: both; }
	table[class=sponsor] {text-align:center; float:none; width:100%!important;}
	td[class=font10] {font-size: 10px!important;}
	span[class=font38] {font-size: 38px!important; line-height: 42px!important;}
}
}</style>
</head>";
		$body.='
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix">
<div class="ui-sortable" id="sort_them">
<!-- Seperator 2 + Social -->
<table class="full" align="center" bgcolor="#1dbbff" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td  style="padding:20px;" id="sep2"align="center">
			<!-- Wrapper -->
			<table class="mobile" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td align="center">
						<!-- Wrapper -->
						<table class="full" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
							<tr>
								<td align="center" width="100%">
									<!-- SORTABLE -->
									<div class="sortable_inner ui-sortable">
									<!-- Space -->
									<table class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="40" width="100%"></td>
										</tr>
									</table><!-- End Space -->
									<!-- Text -->
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td style="font-size: 26px; color: rgb(255, 255, 255); text-align: center; font-family: Helvetica,Arial,sans-serif; line-height: 32px; font-weight: bold; vertical-align: top;" class="fullCenter" align="center" width="100%">
												<!--[if !mso]><!--><span style="font-family: '; $body.="'Titillium Web'"; $body.=', sans-serif; font-weight:normal;"><!--<![endif]--><p cu-identify="element_06079989344851364" >Account Cancellation Notification<br><!--[if !mso]><!--></span><!--<![endif]--><p>
											</td>
										</tr>
										<tr>
											<td height="05" width="100%"></td>
										</tr>
										<!-- Space -->
										<table class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
											<tr>
												<td height="10" width="100%"></td>
											</tr>
										</table><!-- End Space -->
										<tr>
											<td style="font-size: 26px; color: rgb(255, 255, 255); text-align: center; font-family: Helvetica,Arial,sans-serif; line-height: 32px; font-weight: bold; vertical-align: top;" class="fullCenter" align="center" width="100%">
												<!--[if !mso]><!--><span style="font-family:'; $body.="'proxima_novasemibold'";$body.=', Helvetica; font-weight:normal;"><!--<![endif]--><p cu-identify="element_06079989344851364" >
												<img width="170" height="100" style="padding-right:10px;padding-bottom:10px;" src="'.$root.'/images/homepagelogo.png"><br><!--[if !mso]><!--></span><!--<![endif]--><p>
											</td>
										</tr>
										<tr>
											<td height="05" width="100%"></td>
										</tr>
										<tr>
											<td style="font-size: 26px; color: rgb(255, 255, 255); text-align: center; font-family: Helvetica,Arial,sans-serif; line-height: 32px; font-weight: bold; vertical-align: top;" class="fullCenter" align="center" width="100%">
												<!--[if !mso]><!--><span style="font-family:'; $body.="'Titillium Web'";$body.=', sans-serif; font-weight:normal;"><!--<![endif]--><p cu-identify="element_06079989344851364" >Hi '.$cname.',<br><!--[if !mso]><!--></span><!--<![endif]--><p>
											</td>
										</tr>
									</table>
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="15" width="100%">
											</td>
										</tr>
									</table>
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td style="font-size: 14px; color: rgb(255, 255, 255); text-align: center; font-family: Helvetica,Arial,sans-serif; line-height: 22px; font-weight: bold; vertical-align: top;" class="fullCenter" align="center" width="100%">												
												<!--[if !mso]><!--><span style="font-family:'; $body.="'proxima_nova_rgregular'"; $body.=', Helvetica; font-weight:normal;"><!--<![endif]--><p cu-identify="element_05107549716118674" ><div style="text-align: center;"><font size="3"><span style="font-family: Verdana;">Account has been cancelled on '.$date.'. All the data stored in sales neuron has been permanently deleted. Thank you for using our services. Please get in touch with our customer success team for any clarifications or sharing your suggestions.<br /><br />
												Regards,<br />
												Customer Success Team
												</span><br><span style="font-family: Verdana;"></span></font></div><!--[if !mso]><!--></span><!--<![endif]--><p>
											</td>
										</tr>
									</table>									
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="15" width="100%">
											</td>
										</tr>
									</table>									
									
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="80" width="100%">
											</td>
										</tr>
									</table>									
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td style="font-size: 14px; color: rgb(255, 255, 255); text-align: center; font-family: Helvetica,Arial,sans-serif; line-height: 22px; font-weight: bold; vertical-align: top;" class="fullCenter" align="center" width="100%">
												<p cu-identify="element_03207356464405948" >Did not Sign Up? Please ignore this mail.<br>
											</td>
										</tr>
									</table>									
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="0" width="100%">
											</td>
										</tr>
									</table>									
									<!-- Social Icons -->
									<div style="display: none" id="element_09872123869190068"></div>
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="0" width="100%">
											</td>
										</tr>
									</table>
									</div>
									<!-- END SORTABLE -->
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table><!-- End Wrapper --> 
		</div>
		</td>
	</tr>
</table>
<!-- End Wrapper -->
<!-- Wrapper 3 -->
<div style="display: none" id="element_03663119396258956"></div>
<!-- End Wrapper 3 -->
<!-- Wrapper 28 -->
<div style="display: none" id="element_08147460135100794"></div><!-- Wrapper 28 -->
<!-- Wrapper 28 -->
<div style="display: none;" id="element_046737467804971156"></div><!-- Wrapper 28 -->
<!-- Wrapper 3 -->
<div style="display: none" id="element_010546669432505962"></div>
<!-- End Wrapper 3 -->
<!-- Wrapper Footer -->
<table style="background-color:#e1e1e1;" class="full" align="center" bgcolor="#f1f4f6" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td align="center" valign="top" width="100%">
			<!-- Wrapper -->
			<table class="mobile" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td align="center">
						<!-- Space -->
						<table class="full" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td height="20" width="100%"></td>
							</tr>
						</table><!-- End Space -->
						<!-- Wrapper -->
						<table class="full" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
							<tr>
								<td align="center" width="100%">
									<!-- Icon 1 -->
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: center;" class="fullCenter" align="left" border="0" cellpadding="0" cellspacing="0" width="400">
										<tr>
											<td style="text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 13px; color: #aaacaf; line-height: 22px;" class="fullCenter" valign="middle" width="100%">
												<span style="font-family:'; $body.="'proxima_nova_rgregular'";$body.=', Helvetica; font-weight: normal;">
													<span style="color: #787b80;">© 2015 </span>,<a href="http://www.aucventures.com" style="text-decoration: none; color: #787b80;">AUC Ventures Pvt Ltd</a> , All Rights Reserved
													<span style="font-family:'; $body.="'proxima_nova_rgregular'";$body.=', Helvetica;"></span>
												</span>
											</td>
										</tr>
									</table>
									<!-- Space -->
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" align="left" border="0" cellpadding="0" cellspacing="0" width="1">
										<tr>
											<td height="12" width="100%"></td>
										</tr>
									</table>
									<!-- Social Icons Footer -->
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: right;" class="fullCenter" align="right" border="0" cellpadding="0" cellspacing="0" width="120">
										<tr>
											<td id="icon18" width="100%">
												&nbsp;
												<a href="http://facebook.com/salesneuron" style="text-decoration: none;" >
													<img src="http://rocketway.net/themebuilder/products/templates/destiny/images/footer_icon2.png" style="width: 18px; height:auto;" alt=""  class="hover" border="0" height="auto" width="18">
												</a>
												&nbsp;
												<a href="https://twitter.com/salesneuron" style="text-decoration: none;" >
													<img src="http://rocketway.net/themebuilder/products/templates/destiny/images/footer_icon3.png" style="width: 18px; height:auto;" alt=""  class="hover" border="0" height="auto" width="18">
												</a>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table><!-- End Wrapper -->
						<!-- Space -->
						<table class="full" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td height="20" width="100%"></td>
							</tr>
						</table><!-- End Space -->
					</td>
				</tr>
			</table><!-- End Wrapper -->
		</div>
		</td>
	</tr>
</table><!-- Wrapper Footer -->
</div>
</body></html>	<style>body{ background: none !important; } </style>';
		$newbody = $body;
		$mail->IsHTML(true);
		$mail->MsgHTML ($newbody);
		$mail->AddAddress(trim($email));
		$mail->AddBCC('arvind@aucventures.com');
		$mail->AltBody="This is text only alternative body.";
		if($mail->send()) {
			return 'success';
		} else {
			return 'fail';
		}
	}
	//clear demo data model
	public function cleardemodatamodel() {
		$mastercompanyid = $this->usermastercompanyid();
		$mdbname = $this->db->masterdb;//master db
		$databasename = $this->db->database;//user db
		{//fetch users old informations
			//company details
			$companydatas=array();
			$compdata = $this->db->select('companyname,contactperson,emailid,timezoneid,currencyid,dateformatid,mastercompanyid,industryid,createuserid,lastupdateuserid,createdate,lastupdatedate,status')->from('company')->where_not_in('companyid',array('1'))->order_by('companyid','asc')->get();
			foreach($compdata->result() as $compdatasets) {
				$companydatas[]=array('companyname'=>$compdatasets->companyname,'contactperson'=>$compdatasets->contactperson,'emailid'=>$compdatasets->emailid,'timezoneid'=>$compdatasets->timezoneid,'createdate'=>$compdatasets->createdate,'lastupdatedate'=>$compdatasets->lastupdatedate,'currencyid'=>$compdatasets->currencyid,'dateformatid'=>$compdatasets->dateformatid,'industryid'=>$compdatasets->industryid,'createuserid'=>$compdatasets->createuserid,'lastupdateuserid'=>$compdatasets->lastupdateuserid,'status'=>$compdatasets->status,'mastercompanyid'=>$compdatasets->mastercompanyid);
			}
			//branch details
			$branchdatas=array();
			$branchdata = $this->db->select('branchname,companyid,branchtypeid,emailid,industryid,createdate,lastupdatedate,createuserid,lastupdateuserid,status')->from('branch')->where_not_in('branchid',array('1'))->order_by('branchid','asc')->get();
			foreach($branchdata->result() as $branchdatasets) {
				$branchdatas[]=array('branchname'=>$branchdatasets->branchname,'companyid'=>$branchdatasets->companyid,'branchtypeid'=>$branchdatasets->branchtypeid,'emailid'=>$branchdatasets->emailid,'industryid'=>$compdatasets->industryid,'createdate'=>$branchdatasets->createdate,'lastupdatedate'=>$branchdatasets->lastupdatedate,'createuserid'=>$branchdatasets->createuserid,'lastupdateuserid'=>$branchdatasets->lastupdateuserid,'status'=>$branchdatasets->status);
			}
			//employee details
			$employeedatas=array();
			$employeedata = $this->db->select('employeename,branchid,userroleid,emailid,timezoneid,password,employeeimage_fromid,employeeimage,employeeimage_size,employeeimage_type,employeeimage_path,employeeimagestatus,industryid,createdate,lastupdatedate,createuserid,lastupdateuserid,status')->from('employee')->where_not_in('employeeid',array('1'))->order_by('employeeid','asc')->get();
			foreach($employeedata->result() as $empdatasets) {
				$employeedatas[]=array('employeename'=>$empdatasets->employeename,'branchid'=>$empdatasets->branchid,'userroleid'=>$empdatasets->userroleid,'emailid'=>$empdatasets->emailid,'timezoneid'=>$empdatasets->timezoneid,'password'=>$empdatasets->password,'employeeimage_fromid'=>$empdatasets->employeeimage_fromid,'employeeimage'=>$empdatasets->employeeimage,'employeeimage_size'=>$empdatasets->employeeimage_size,'employeeimage_type'=>$empdatasets->employeeimage_type,'employeeimage_path'=>$empdatasets->employeeimage_path,'employeeimagestatus'=>$empdatasets->employeeimagestatus,'industryid'=>$compdatasets->industryid,'createdate'=>$empdatasets->createdate,'lastupdatedate'=>$empdatasets->lastupdatedate,'createuserid'=>$empdatasets->createuserid,'lastupdateuserid'=>$empdatasets->lastupdateuserid,'status'=>$empdatasets->status);
			}
			//employee address details
			$empaddrdatas=array();
			$empaddrdata = $this->db->select('employeeid,addresstypeid,addressmethod,createdate,lastupdatedate,createuserid,lastupdateuserid,status')->from('employeeaddress')->where_not_in('employeeaddressid',array('1'))->order_by('employeeaddressid','asc')->get();
			foreach($empaddrdata->result() as $empaddrdatasets) {
				$empaddrdatas[]=array('employeeid'=>$empaddrdatasets->employeeid,'addresstypeid'=>$empaddrdatasets->addresstypeid,'addressmethod'=>$empaddrdatasets->addressmethod,'createdate'=>$empaddrdatasets->createdate,'lastupdatedate'=>$empaddrdatasets->lastupdatedate,'createuserid'=>$empaddrdatasets->createuserid,'lastupdateuserid'=>$empaddrdatasets->lastupdateuserid,'status'=>$empaddrdatasets->status);
			}
			//general settings
			$gensetdatas=array();
			$gensetdata = $this->db->select('companyid,mandrillaccountnumber,status')->from('generalsetting')->order_by('generalsettingid','asc')->get();
			foreach($gensetdata->result() as $gensetdatasets) {
				$gensetdatas[]=array('companyid'=>$gensetdatasets->companyid,'mandrillaccountnumber'=>$gensetdatasets->mandrillaccountnumber,'status'=>$gensetdatasets->status);
			}
			//bill information
			$billinfos=array();
			$billdata = $this->db->select('mastercompanyid,billingusers,billingcost,individualname,emailid,phone,billingaddress,billingcity,billingstate,billingcountry,billingpostalcode,shippingaddress,shippingcity,shippingstate,shippingcountry,shippingpostalcode,createdate,lastupdatedate,createuserid,lastupdateuserid,status')->from('billinginformation')->order_by('billinginformationid','asc')->get();
			foreach($billdata->result() as $billdatas) {
				$billinfos[] = array('mastercompanyid'=>$billdatas->mastercompanyid,'billingusers'=>$billdatas->billingusers,'billingcost'=>$billdatas->billingcost,'individualname'=>$billdatas->individualname,'emailid'=>$billdatas->emailid,'phone'=>$billdatas->phone,'billingaddress'=>$billdatas->billingaddress,'billingcity'=>$billdatas->billingcity,'billingstate'=>$billdatas->billingstate,'billingcountry'=>$billdatas->billingcountry,'billingpostalcode'=>$billdatas->billingpostalcode,'shippingaddress'=>$billdatas->shippingaddress,'shippingcity'=>$billdatas->shippingcity,'shippingstate'=>$billdatas->shippingstate,'shippingcountry'=>$billdatas->shippingcountry,'shippingpostalcode'=>$billdatas->shippingpostalcode,'createdate'=>$billdatas->createdate,'lastupdatedate'=>$billdatas->lastupdatedate,'createuserid'=>$billdatas->createuserid,'lastupdateuserid'=>$billdatas->lastupdateuserid,'status'=>$billdatas->status);
			}
			//add-ons credit information
			$addonsdatas=array();
			$addondata = $this->db->select('mastercompanyid,addonstypeid,addonavailablecredit,status')->from('salesneuronaddonscredit')->where_not_in('salesneuronaddonscreditid',array(1))->where('mastercompanyid',$mastercompanyid)->order_by('salesneuronaddonscreditid','asc')->get();
			foreach($addondata->result() as $addondatas) {
				$addonsdatas[]=array('mastercompanyid'=>$addondatas->mastercompanyid,'addonstypeid'=>$addondatas->addonstypeid,'addonavailablecredit'=>$addondatas->addonavailablecredit,'status'=>$addondatas->status);
			}
		}
		//create database
		$demodata='No';
		if($demodata == 'Yes') {
			$datafile = "database/datadb.sql";
		} else {
			$datafile = "database/cleandb.sql";
		}
		$this->load->dbforge();
		$this->load->dbutil();
		//backup data base
		$backup = $this->dbutil->backup();
		$this->load->helper('file');
		write_file('clientdatabase/'.$databasename.'.gz', $backup);
		//create new data base
		$dbcreate = 'No';
		$dbcheck = $this->dbutil->database_exists($databasename);
		if($dbcheck == '') {
			$this->dbforge->create_database($databasename);
			$dbcreate = 'Yes';
		} else {
			if($this->dbforge->drop_database($databasename)) {
				$this->dbforge->create_database($databasename);
				$dbcreate = 'Yes';
			}
		}
		if ($dbcreate == 'Yes') {
			//activate user data base
			$import_db = $this->inlineuserdatabaseactive($databasename);
			//create content to user data base
			$importres = $this->doImport($databasename,$datafile,$compress=false,$import_db);
			if($importres=='true') {
				{//update user database
					//create company
					$compids=array();
					foreach($companydatas as $companydata) {
						$import_db->insert('company',$companydata);
						$compids[] = $import_db->insert_id();
					}
					//create new branch
					$import_db->insert_batch('branch',$branchdatas);
					//create new employee
					$import_db->insert_batch('employee',$employeedatas);
					//employee address creattion
					$import_db->insert_batch('employeeaddress',$empaddrdatas);
					//insert mandrill account info
					$import_db->insert_batch('generalsetting',$gensetdatas);
					//update general settings
					foreach($compids as $compid) {
						$import_db->update('generalsetting',array('demodata'=>'No'),array('companyid'=>$compid));
					}
					//bill information
					if(count($billinfos)!=0) {
						$import_db->insert_batch('billinginformation',$billinfos);
					}
					//update default add-ons credit
					if(count($addonsdatas)!=0) {
						$import_db->insert_batch('salesneuronaddonscredit',$addonsdatas);
					}
				}
				echo 'success';
			} else {
				echo 'fail';
			}
		} else {
			echo 'fail';
		}
	}
	//Import SQL file into selected database
	public function doImport($database,$filename,$compress,$import_db) {
		if($filename != "") {
			$import_db->query("SET FOREIGN_KEY_CHECKS = 0");
			$import = $this->importSql($filename,$compress,$import_db);
			$import_db->query("SET FOREIGN_KEY_CHECKS = 1");
			if(!$import)
				return 'false';
			else 
				return 'true';
		}
	}
	//execute query
	public function importSql($file,$compress,$import_db) {
		//reading file content
		if ($compress) {
			$lines = gzfile($file);
		} else {
			$lines = file($file);
		}
		$x = 0;
		$importSql = "";
		$procent = 0;
		foreach ($lines as $line) {
			// Importing SQL
			$importSql .= $line;
			if ( substr(trim($line), strlen(trim($line))-1) == ";" ) {
				$query = $import_db->query($importSql);
				if (!$query) return false;
				$importSql = "";
			}
		}
		return true;
	}
	//inline default data base set
	public function inlinedefaultdbenable() {
		$active_group = 'default';
		$query_builder = TRUE;
		$this->load->database('default',TRUE);
	}
	//inline user data base set
	public function inlineuserdatabaseactive($databasename) {
		$host = $this->db->hostname;
		$user = $this->db->username;
		$passwd = $this->db->password;
		$dbconfig['hostname'] = $host;
		$dbconfig['username'] = $user;
		$dbconfig['password'] = $passwd;
		$dbconfig['database'] = $databasename;
		$dbconfig['dbdriver'] = 'mysqli';
		$dbconfig['dbprefix'] = '';
		$dbconfig['pconnect'] = TRUE;
		$dbconfig['db_debug'] = TRUE;
		$dbconfig['cache_on'] = FALSE;
		$dbconfig['cachedir'] = '';
		$dbconfig['char_set'] = 'utf8';
		$dbconfig['dbcollat'] = 'utf8_general_ci';
		$dbconfig['swap_pre'] = '';
		$dbconfig['autoinit'] = TRUE;
		$dbconfig['stricton'] = FALSE;
		return $import_db = $this->load->database($dbconfig, TRUE);
	}
	//update subscription session status
	public function checksubscriptionstatus() {
		$branchid = $this->Basefunctions->branchid;
		$datas = $this->db->select('generalsetting.accountexpire')->from('generalsetting')->join('company','company.mastercompanyid=generalsetting.companyid')->join('branch','branch.companyid=company.companyid')->where('branch.branchid',$branchid)->get();
		foreach($datas->result() as $row) {
			$accstatus = (($row->accountexpire=='Yes')?'inactive':'active');
			$this->session->set_userdata('UserAccount',$accstatus);
		}
	}
}