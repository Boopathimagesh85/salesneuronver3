<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('Base/headerfiles'); ?>
</head>
<body>
<?php
	$this->load->view('subscriptionmanagerform'); 
	$this->load->view('subscriptionoverlay');
	$device = $this->Basefunctions->deviceinfo();
	if($device=='phone') {
		$this->load->view('Base/overlaymobile');
	} else {
		$this->load->view('Base/overlay');
	}
	$this->load->view('Base/modulelist');
?>
</body>
	<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js Files -->	
	<script src="<?php echo base_url();?>js/Subscriptionmanager/subscriptionmanager.js" type="text/javascript"></script>
	<!--For Tablet and Mobile view Dropdown Script-->
</html>
 
