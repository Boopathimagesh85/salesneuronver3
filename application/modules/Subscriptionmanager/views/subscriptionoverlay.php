<!-- Subscription cancel  Overlay-->

<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="subscriptioncanceloverlay" style="overflow-y: scroll;overflow-x: hidden">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-4 medium-10 large-centered medium-centered columns">
			<form method="POST" name="accountcanceldatasetsform" id="accountcanceldatasetsform" action="" enctype="">
				<span id="accountcanceldatasetsformvalidate" class="accountcancelclearform validationEngineContainer">
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-10 columns" style="text-align:center;"> Cancel Account </div>
							<div class="small-1 columns" id="subscripoverlayclose" style="text-align:right;cursor:pointer;background:none">X</div>
						</div>
					</div>
					<div class="row" style="background:#f5f5f5" >
						<div class="large-12 small-12 medium-4 columns large-centered medium-centered">
							<div class="large-12 columns">
								<label>Current password<span class="mandatoryfildclass">*</span></label>
								<input id="canaccountpasswd" class="validate[required]" type="password" tabindex="201" value="" name="canaccountpasswd">
							</div>
							<div class="large-12 columns">
								<label>Reason<span class="mandatoryfildclass">*</span></label>
								<select id="cancelaccreason" class="chzn-select validate[required]" name="cancelaccreason" data-placeholder="Select" data-prompt-position="topLeft:14,36" tabindex="202">
									<option></option>
									<option value="We are not happy with software features">We are not happy with software features</option>
									<option value="Software does not meet our requirements">Software does not meet our requirements</option>
									<option value="Software pricing is very high">Software pricing is very high</option>
								</select>
							</div>
							<div class="large-12 columns">
								<label>Comments</label>
								<textarea id="cancelacccomments" tabindex="203" name="cancelacccomments" rows="3"></textarea>
							</div>
							<div class="row" style="background:#f5f5f5">&nbsp;</div>
							<div class="large-12 columns">
								<div class="row" style="background:#f5f5f5">&nbsp;</div>
								<div class="small-6 medium-6 large-6 columns">
									<input id="cancelaccbtn" class="btn formbuttonsalert" type="button" value="Submit" tabindex="204" name="cancelaccbtn">
								</div>
								<div class="small-6 medium-6 large-6 columns">
									<input id="cancelaccnobtn" class="btn formbuttonsalert" type="button" value="Reset" tabindex="205" name="cancelaccnobtn">
								</div>
							</div>
							<div class="row" style="background:#f5f5f5">&nbsp;</div>
							<div class="row" style="background:#f5f5f5">&nbsp;</div>
							<div class="row" style="background:#f5f5f5">&nbsp;</div>
						</div>
					</div>
					<div class="row">&nbsp;</div>
				</span>
			</form>
		</div>
	</div>
</div>

<!-- account close confirmation Overlay Yes / No-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="cancelaccconfirmoverlay">	
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-4 medium-6 small-10 large-centered medium-centered small-centered columns">
			<div class="row">&nbsp;</div>
			<div class="row">
				<div class="large-12 columns alertsheadercaptionstyle" style="text-align:left">
					<div class="small-12 columns"><span style="color:#ffffff;padding-right:0.5rem"><i class="material-icons">check_box</i>  </span> Confirmation</div>
				</div>
				<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
				<div class="large-12 large-centered columns" style="background:#f5f5f5;text-align:center">
					<span class="" style="	color: #5e6d82;font-family: segoe ui;font-size: 0.9rem;line-height:1.5rem;">Cancelling your account will delete permanently all the data and account information stored in sales neuron services which cannot be restored in future. Do you wish to cancel your account?</span>
				</div>
			</div>
			<span class="firsttab" tabindex="1000"></span>
			<div class="row" style="background:#f5f5f5"><br><br>
				<div class="medium-2 large-2 columns"> &nbsp;</div>
				<div class="small-6  medium-4 large-4 columns">
					<input type="button" id="cancelaccyes" name="cancelaccyes" value="Yes" tabindex="1001" class="btn ffield formbuttonsalert">	
				</div>
				<div class="small-6 medium-4 large-4 columns">
					<input type="button" id="cancelaccno" name="cancelaccno" value="No" tabindex="1002" class="btn flloop formbuttonsalert alertsoverlaybtn" style="background: #e57276;">	
				</div>
				<span class="lasttab" tabindex="1003"></span>
				<div class="medium-2 large-2 columns"> &nbsp;</div>
			</div>
			<div class="row" style="background:#f5f5f5">&nbsp;</div>
			<div class="row" style="background:#f5f5f5">&nbsp;</div>
		</div>
	</div>
</div>

<!-- Edit plan Overlay-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="planeditoverlay" style="overflow-y: scroll;overflow-x: hidden">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-4 medium-10 large-centered medium-centered columns" >
			<form method="POST" name="editplanoverlayform" style="" id="editplanoverlayform" action="" enctype="" class="">
				<span id="editplanoverlayvalidate" class="validationEngineContainer"> 
					<div class="row">&nbsp;</div>
					<div class="row">&nbsp;</div>
					
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-10 columns" style="text-align:left;"></div>
							<div class="small-1 columns" id="planeditoverlayclose" style="text-align:right;cursor:pointer;background:none"><span class="cross-subs pull-left">X</span>
							    <span class="plan-overlay-subs pull-right"><span>Plan :</span> <span><?php echo $subscripinfo['cplan']; ?></span></span>
							</div>

						</div>
					</div>
					<div class="row" style="background:#394b53" >
						<div class="large-12 small-12 medium-12 columns large-centered medium-centered">
							
							<div class="large-12 columns">
								<label>Billing</label>
								<?php $duration = $subscripinfo['paydurid'];?>
								<select id="editplanduration" class="chzn-select" name="editplanduration" tabindex="301">
									<option value=""></option>
									<option value="1"<?php echo (($duration==1)?' selected="selected"' : '')?>>Monthly</option>
									<option value="12"<?php echo (($duration==12)?' selected="selected"' : '')?>>Annually</option>
								</select>
							</div>
							<div class="large-12 columns">
								<div class="small-4 medium-4 large-4 columns">
									<label>Users<span class="mandatoryfildclass">*</span></label>
									<input id="editplanusers" class="validate[required,funcCall[userplanvalidate]]" type="text" tabindex="302" value="<?php echo $billinfo['billusers']; ?>" name="editplanusers">
								</div>
								<div class="small-4 medium-4 large-4 columns">
									<label>Cost<span class="mandatoryfildclass">*</span></label>
									<input id="plancosts" class="validate[required]" type="text" tabindex="302" value="<?php echo $billinfo['billcost']; ?>" name="plancosts" readonly="readonly">
								</div>
								<div class="small-4 medium-4 large-4 columns">
									<label>Duration<span class="mandatoryfildclass">*</span></label>
									<input id="plandurations" class="validate[required]" type="text" tabindex="302" value="<?php echo $subscripinfo['paydurid']; ?>" name="plandurations" readonly="readonly">
								</div>
							</div>
							<div class="large-12 columns">
								<div class="small-12 medium-12 large-12 columns center">
									<label><span>Total : </span><span><?php echo $currency['cursymbol']; ?></span><span id="totaleditplancost">0</span></label>
								</div>
							</div>
							<div class="row">&nbsp;</div>
							<div class="large-12 columns">
								<div class="row">&nbsp;</div>
								<div class="small-6 medium-6 large-6 columns large-centered medium-centered small-centered">
									<?php
										$curid = (isset($currency['currid'])?$currency['currid']:'120');
										if($curid=='2') {
											echo '<input id="instplanupdatebtn" class="btn formbuttonsalert" type="button" value="Update Plan" tabindex="304" name="instplanupdatebtn">';
											echo '<span id="instspan"></span>';
										} else {
											echo '<input id="planupdatebtn" class="btn formbuttonsalert" type="button" value="Update Plan" tabindex="304" name="planupdatebtn">';
										}
									?>
								</div>
							</div>
							<div class="row">&nbsp;</div>
							<div class="row">&nbsp;</div>
							<div class="row">&nbsp;</div>
						</div>
					</div>
					<div class="row">&nbsp;</div>
				</span>
				<!--hidden fields-->
				<input type="hidden" id="planusercount" value="<?php echo $billinfo['billusers']; ?>" name="planusercount">
				<input type="hidden" name="editplanid" id="editplanid" value="<?php echo $subscripinfo['cplanid']; ?>"/>
				<input type="hidden" name="editplandur" id="editplandur" value="<?php echo $subscripinfo['paydurid']; ?>"/>
				<input type="hidden" name="editplancost" id="editplancost" value="<?php echo $billinfo['billcost']; ?>"/>
				<input type="hidden" name="editplanactdate" id="editplanactdate" value="<?php echo $subscripinfo['acccreatedate']; ?>"/>
				<input type="hidden" name="editplanexpdate" id="editplanexpdate" value="<?php echo $subscripinfo['accexpdate']; ?>"/>
				<input type="hidden" name="editplandaysleft" id="editplandaysleft" value="<?php echo $subscripinfo['daysleft']; ?>" />
			</form>
		</div>
	</div>
</div>
<!--instamojo url-->
<input type="hidden" id="instamojourl" name="instamojourl" value="https://www.instamojo.com/aucventures/pay-here-60a4a/">
<!-- instamojo data form -->
<form class="form-horizontal" action="https://www.instamojo.com/aucventures/pay-here-60a4a/" method="GET" name="instamojodataform" id="instamojodataform">
	<!-- instamojo mandatory information -->
	<input type='hidden' name='data_amount' id='instsubscripetotalamount' value='' />
	<input type='hidden' name='data_name' id='instcard_holder_name' class='cname' value='<?php echo (isset($billinfo['indname'])?$billinfo['indname']:'');?>' />
	<input type='hidden' name='data_phone' id='instphone' class='cphone' value='<?php echo (isset($billinfo['phone'])?$billinfo['phone']:'');?>' />
	<input type='hidden' name='data_email' id='instemail' class='cemail' value='<?php echo (isset($billinfo['email'])?$billinfo['email']:'');?>' />

	<!--- hidden fields user information--->
	<input type="hidden" name="data_Field_15804" id="instpaymentduration" value="1" />
	<input type="hidden" name="data_Field_27018" id="instsubscripeplan" value="<?php echo $subscripinfo['cplanid']; ?>" />
	<input type="hidden" name="data_Field_27019" id="instsubplancost" value="<?php echo $billinfo['billcost']; ?>" />

	<!--customer master information-->
	<input type='hidden' name='data_Field_32933' id="instcurrencyid" value='<?php echo (isset($currency['currid'])?$currency['currid']:'120');?>' />
	<input type='hidden' name='data_Field_85438' id="instuseremail" value='<?php echo $userdata['email'];?>' />
	<input type='hidden' name='data_Field_85437' id="instuserid" value='<?php echo $userdata['userid'];?>' />
	<input type='hidden' name='data_Field_85436' id="instuserzone" value='<?php echo $userdata['zone'];?>' />
	<input type='hidden' name='data_Field_85435' id="instchkoutopt" value='product' />
	<input type='hidden' name='data_Field_36438' id="insplanmode" value='Update' />

	<!-- user information -->
	<input type='hidden' name='data_Field_85434' id='inststreet_address' class='caddress' value='<?php echo (isset($billinfo['billaddr'])?$billinfo['billaddr']:'');?>' />
	<input type='hidden' name='data_Field_85433' id='instcity' class='ccity' value='<?php echo (isset($billinfo['billcity'])?$billinfo['billcity']:'');?>' />
	<input type='hidden' name='data_Field_85440' id='inststate' class='cstate' value='<?php echo (isset($billinfo['billstate'])?$billinfo['billstate']:'');?>' />
	<input type='hidden' name='data_Field_85439' id='instzip' class='czip' value='<?php echo (isset($billinfo['billzip'])?$billinfo['billzip']:'');?>' />
	<input type='hidden' name='data_Field_85442' id='instcountry' class='ccountry' value='<?php echo (isset($billinfo['billcountry'])?$billinfo['billcountry']:'');?>' />

	<!--product information -->
	<input type='hidden' name='data_Field_77012' id='instli_0_typeid' value='1' />
	<input type='hidden' name='data_Field_12182' id='instli_0_nameid' value='<?php echo $subscripinfo["cplanid"]; ?>' />
	<input type='hidden' name='data_Field_52189' id='instli_0_name' value='<?php echo $subscripinfo["cplan"]; ?>' />
	<input type='hidden' name='data_Field_52188' id='instli_0_quantity' value='' />
	<input type='hidden' name='data_Field_52187' id='instli_0_price' value='' />
	
	<!-- data visibility properties-->
	<input type='hidden' name='data_readonly' value='data_email' />
	<input type='hidden' name='data_readonly' value='data_phone' />
	<input type='hidden' name='data_readonly' value='data_name' />
	<input type='hidden' name='data_readonly' value='data_amount' />

	<input type="hidden" name="data_hidden" value="data_Field_27019" />
	<input type="hidden" name="data_hidden" value="data_Field_27018" />
	<input type="hidden" name="data_hidden" value="data_Field_15804" />

	<input type="hidden" name="data_hidden" value="data_Field_36438" />
	<input type="hidden" name="data_hidden" value="data_Field_85435" />
	<input type="hidden" name="data_hidden" value="data_Field_85436" />
	<input type="hidden" name="data_hidden" value="data_Field_85437" />
	<input type="hidden" name="data_hidden" value="data_Field_85438" />
	<input type="hidden" name="data_hidden" value="data_Field_32933" />

	<input type="hidden" name="data_hidden" value="data_Field_85442" />
	<input type="hidden" name="data_hidden" value="data_Field_85439" />
	<input type="hidden" name="data_hidden" value="data_Field_85440" />
	<input type="hidden" name="data_hidden" value="data_Field_85433" />
	<input type="hidden" name="data_hidden" value="data_Field_85434" />
	
	<input type="hidden" name="data_hidden" value="data_Field_52187" />
	<input type="hidden" name="data_hidden" value="data_Field_52188" />
	<input type="hidden" name="data_hidden" value="data_Field_52189" />
	<input type="hidden" name="data_hidden" value="data_Field_12182" />
	<input type="hidden" name="data_hidden" value="data_Field_77012" />
	<input type="hidden" name="data_hidden" value="data_Field_8167" />
	
	<input type="hidden" name="data_hidden" value="data_Field_8166" />
	<input type="hidden" name="data_hidden" value="data_Field_8165" />
	<input type="hidden" name="data_hidden" value="data_Field_8164" />
	<input type="hidden" name="data_hidden" value="data_Field_8163" />
	
	<input type="hidden" name="data_hidden" value="data_Field_8172" />
	<input type="hidden" name="data_hidden" value="data_Field_8171" />
	<input type="hidden" name="data_hidden" value="data_Field_8170" />
	<input type="hidden" name="data_hidden" value="data_Field_8169" />
	<input type="hidden" name="data_hidden" value="data_Field_8168" />
	
	<input type="hidden" name="data_hidden" value="data_Field_8177" />
	<input type="hidden" name="data_hidden" value="data_Field_8176" />
	<input type="hidden" name="data_hidden" value="data_Field_8175" />
	<input type="hidden" name="data_hidden" value="data_Field_8174" />
	<input type="hidden" name="data_hidden" value="data_Field_8173" />
	<!--instamojo form properties-->
	<input type="hidden" name="embed" value="form">
	<input type="hidden" name="intent" value="buy">
	<input name='instsubmit' id="instsubmit" type='submit' value='Checkout' class="hidedisplay" />
</form>
<!--payment checkout form -->
<form  class="form-horizontal"  method="POST" action="https://www.2checkout.com/checkout/purchase" name="orderconfirmationform" id="orderconfirmationform">
	<!-- hidden fields for show information to span and for calculation -->
	<input type="hidden" id="paymentduration" name="paymentduration" value="1" />
	<input type="hidden" id="subscripeplan" name="subscripeplan" value="<?php echo $subscripinfo['cplanid']; ?>" />
	<input type="hidden" id="subplancost" name="subplancost" value="<?php echo $billinfo['billcost']; ?>" />
	<input type='hidden' name='currencyid' id="currencyid" value='<?php echo (isset($currency['currid'])?$currency['currid']:'120');?>' />
	<input type='hidden' name='currencysymbol' id="currencysymbol" value='<?php echo $currency['cursymbol'];?>' />
	<input type='hidden' name='useremail' id="useremail" value='<?php echo $userdata['email'];?>' />
	<input type='hidden' name='userid' id="userid" value='<?php echo $userdata['userid'];?>' />
	<input type='hidden' name='userzone' id="userzone" value='<?php echo $userdata['zone'];?>' />
	<input type='hidden' name='chkoutopt' id="chkoutopt" value='product' />
	<input type='hidden' name='planmode' id="planmode" value='Update' />
	<!-- Payment information for to proceed for amout pay -->
	<input type='hidden' name='sid' value='202490408' />
	<input type='hidden' name='mode' value='2CO' />
	<input type='hidden' name='currency_code' id='currency_code' value='<?php echo (isset($currency['code'])?$currency['code']:'USD');?>' />
	<input type='hidden' name='merchant_order_id' value='AUCINV12345' />
	<!-- product information-->
	<input type='hidden' name='li_0_type' id='li_0_type' value='Product' />
	<input type='hidden' name='li_0_typeid' id='li_0_typeid' value='1' />
	<input type='hidden' name='li_0_name' id='li_0_name' value='<?php echo $subscripinfo["cplan"]; ?>' />
	<input type='hidden' name='li_0_nameid' id='li_0_nameid' value='<?php echo $subscripinfo["cplanid"]; ?>' />
	<input type='hidden' name='li_0_quantity' id='li_0_quantity' value='' />
	<input type='hidden' name='li_0_price' id='li_0_price' value='' />
	<!-- user information -->
	<input type='hidden' name='card_holder_name' id='card_holder_name' value='<?php echo (isset($billinfo['indname'])?$billinfo['indname']:'');?>' />
	<input type='hidden' name='street_address' id='street_address' value='<?php echo (isset($billinfo['billaddr'])?$billinfo['billaddr']:'');?>' />
	<input type='hidden' name='city' id='city' value='<?php echo (isset($billinfo['billcity'])?$billinfo['billcity']:'');?>' />
	<input type='hidden' name='state' id='state' value='<?php echo (isset($billinfo['billstate'])?$billinfo['billstate']:'');?>' />
	<input type='hidden' name='zip' id='zip' value='<?php echo (isset($billinfo['billzip'])?$billinfo['billzip']:'');?>' />
	<input type='hidden' name='country' id='country' value='<?php echo (isset($billinfo['billcountry'])?$billinfo['billcountry']:'');?>' />
	<input type='hidden' name='phone' id='phone' value='<?php echo (isset($billinfo['phone'])?$billinfo['phone']:'');?>' />
	<input type='hidden' name='email' id='email' value='<?php echo (isset($billinfo['email'])?$billinfo['email']:'');?>' />
	<input name='submit' id="submit" type='submit' value='Checkout' class="hidedisplay" /> 
</form>
<script src="https://www.2checkout.com/static/checkout/javascript/direct.min.js"></script>