<style type="text/css">
	#submanageradvanceddrop{
		left: -82.1666px !important;
	}
	#invoiceadvanceddrop{
		left: -43.1666px !important;
	}
	#addonadvanceddrop{
		left: -49.1666px !important;
	}
	.gridcontent {
		height:475px !important;
	}
</style>
<div class="row subscriptionmanagerform" style="max-width:100%;opacity:0;">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<?php
				$dataset['gridtitle'] = $gridtitle['title'];
				$dataset['titleicon'] = $gridtitle['titleicon'];
				$dataset['moduleid'] = '5';
				$dataset['action'][0] = array('actionid'=>'billedit','actiontitle'=>'Edit','actionmore'=>'Yes','ulname'=>'submanager');
				$dataset['action'][1] = array('actionid'=>'invoicedownicon','actiontitle'=>'Download','actionmore'=>'Yes','ulname'=>'invoice');
				$dataset['action'][2] = array('actionid'=>'addonbuyicon','actiontitle'=>'Purchase','actionmore'=>'Yes','ulname'=>'addon');
				$this->load->view('Base/formwithgridmenuheader.php',$dataset);
			?>
			<div class="large-12 columns addformunderheader">&nbsp;</div>
			<div class="large-12 columns scrollbarclass addformcontainer groupstouch">
			<!-- More Action DD start -- Gowtham -->
				<ul id="submanageradvanceddrop" class="multinavaction-drop arrow_box" style="white-space: nowrap; position: absolute; top: 34.8px; left: -82.1666px; opacity: 1; display: none;">
					<li id="planupgrade"><span class="icon-box"><i class="material-icons" title="Upgrade Plan">system_update_alt</i>Upgrade Plan</span></li>
					<li id="planediticon"><span class="icon-box"><i class="material-icons edoiticonclass " title="Plan Edit">edit</i>Plan Edit</span></li>
					<li id="cancelaccount"><span class="icon-box"><i class="material-icons canceliconclass " title="Cancel Account">cancel</i>Cancel Account</span></li>
				</ul>
				<ul id="invoiceadvanceddrop" class="multinavaction-drop arrow_box" style="white-space: nowrap; position: absolute; top: 34.8px; left: -62.1666px; opacity: 1; display: none;">
					<li id="reloadicon"><span class="icon-box"><i class="material-icons reloadiconclass " title="Reload">refresh</i>Reload</span></li>
				</ul>
				<ul id="addonadvanceddrop" class="multinavaction-drop arrow_box" style="white-space: nowrap; position: absolute; top: 34.8px; left: -62.1666px; opacity: 1; display: none;">
					<li id="addonreloadicon"><span class="icon-box"><i class="material-icons reloadiconclass" title="Reload">refresh</i>Reload</span></li>
				</ul>
		<!-- More Action DD start -- Gowtham -->
				<div id="subformspan1" class="hiddensubform transitionhiddenform">
					<div class="row">&nbsp;</div>
					<form method="POST" name="subscriptioninformationform" class="" action ="" id="subscriptioninformationform">
						<span id="" class="validationEngineContainer">
							<span id="" class="validationEngineContainer">
								<div class="large-4 columns paddingbtm">
									<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff">
										<div class="large-12 columns headerformcaptionstyle">Account Details</div>
										<div class="input-field large-12 columns">
											<input type="text" class="" name="compname" id="compname" value="<?php echo $billinfo['cname']; ?>" tabindex="101"/>
											<label for="compname">Company Name</label>
										</div>
										<div class="input-field large-12 columns">
											<input type="text" class="" name="accountcreatedate" id="accountcreatedate" value="<?php echo $subscripinfo['accstartdate']; ?>" tabindex="102"/>
											<label for="accountcreatedate">Created Date</label>
										</div>
										<div class="input-field large-12 columns">
											<input type="text" class="" name="billemailid" id="billemailid" value="<?php echo $billinfo['email']; ?>" tabindex="116"/>
											<label for="billemailid">E-Mail</label>
										</div>
										<div id="" class="input-field large-6 medium-6 small-12 columns">
											<input type="text" class="" name="accountplan" id="accountplan" value="<?php echo $subscripinfo['cplan']; ?>" tabindex="103"/>
											<label for="accountplan">Current Plan</label>
											<input type="hidden" class="" name="accountplanid" id="accountplanid" value="<?php echo $subscripinfo['cplanid']; ?>" tabindex="105"/>
										</div>
										<div id="" class="input-field large-6 medium-6 small-12 columns">
											<input type="text" class="" name="planduration" id="planduration" value="<?php echo $subscripinfo['payduration']; ?>" tabindex="104"/>
											<label for="planduration">Payment Duration</label>
										</div>
										<div class="input-field large-6 medium-6 small-12 columns">
											<input type="text" class="" name="plancost" id="plancost" value="<?php echo $subscripinfo['cursymbol'].$billinfo['billcost']; ?>" tabindex="105"/>
											<label for="plancost">Plan Cost</label>
										</div>
										<div class="input-field large-6 medium-6 small-12 columns">
											<input type="text" class="" name="planusers" id="planusers" value="<?php echo $billinfo['billusers']; ?>" tabindex="106"/>
											<label for="planusers">No Of Users</label>
										</div>
										<div class="input-field large-12 columns">
											<input type="text" class="" name="planamount" id="planamount" value="<?php echo $subscripinfo['cursymbol'].$billinfo['billcost']*$billinfo['billusers']*$subscripinfo['paydurid']; ?>" tabindex="107"/>
											<label for="planamount">Plan Amount</label>
										</div>
										<div class="large-12 columns">&nbsp;</div>
									</div>
								</div>
								<div class="large-4 columns end paddingbtm">
									<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff">
										<div class="large-12 columns headerformcaptionstyle">Billing Summary</div>
										<div class="input-field large-12 columns">
											<input type="text" class="" name="lastpaidamt" id="lastpaidamt" value="<?php echo $subscripinfo['cursymbol'].$subscripinfo['lapidamont']; ?>" tabindex="110"/>
											<label for="lastpaidamt">Last Paid Amount</label>
										</div>
										<div class="input-field large-12 columns">
											<input type="text" class="" name="lastpaiddate" id="lastpaiddate" value="<?php echo $subscripinfo['lapaiddate']; ?>" tabindex="111"/>
											<label for="lastpaiddate">Last Payment Date/Time</label>
										</div>
										<div class="input-field large-12 columns">
											<input type="text" class="" name="lastpaidmode" id="lastpaidmode" value="<?php echo $subscripinfo['lapaymode']; ?>" tabindex="112"/>
											<label for="lastpaidmode">Last Payment Mode</label>
										</div>
										<div class="input-field large-12 columns">
											<input type="text" class="" name="daysleft" id="daysleft" value="<?php echo $subscripinfo['daysleft']; ?>" tabindex="113"/>
											<label for="daysleft">No Of Days Left For Renewal</label>
										</div>
										<div class="input-field large-12 columns">
											<input type="text" class="" name="nextduedate" id="nextduedate" value="<?php  echo $subscripinfo['rendate'];?>" tabindex="114"/>
											<label for="nextduedate">Renewal Date</label>
										</div>
										<div class="input-field large-12 columns">
											<input type="text" class="" name="nextdueamount" id="nextdueamount" value="<?php echo $subscripinfo['cursymbol'].$billinfo['billcost']*$billinfo['billusers']*$subscripinfo['paydurid']; ?>" tabindex="114"/>
											<label for="nextdueamount">Renewal Amount</label>
										</div>
										<div class="large-12 columns">&nbsp;</div>
									</div>
								</div>
							</span>
						</span>
					</form>
					<form method="POST" name="subscripbillinfo" class="" action ="" id="subscripbillinfo">
						<span id="subscripbillinfovalidate" class="validationEngineContainer">
							<div class="large-4 columns paddingbtm">
								<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff">
									<div class="large-12 columns headerformcaptionstyle">Billing Details</div>
									<div class="input-field large-12 columns">
										<input type="text" class="validate[required]" name="billpersonname" id="billpersonname" value="<?php echo $billinfo['billpersonname']; ?>" tabindex="115"/>
										<label for="billpersonname">Name<span class="mandatoryfildclass billdetailmand hidedisplay">*</span></label>
									</div>
									<div class="input-field large-12 columns">
										<input type="text" class="validate[required,custom[phone]]" name="billmobilenum" id="billmobilenum" value="<?php echo $billinfo['billmobilenum']; ?>" tabindex="116"/>
										<label for="billmobilenum">Mobile Number<span class="mandatoryfildclass billdetailmand hidedisplay">*</span></label>
									</div>
									<div class="input-field large-12 columns">
										<input type="text" class="validate[required]" name="billaddr" id="billaddr" value="<?php echo $billinfo['billaddr']; ?>" tabindex="117"/>
										<label for="billaddr">Address<span class="mandatoryfildclass billdetailmand hidedisplay">*</span></label>
									</div>
									<div class="input-field large-12 columns">
										<input type="text" class="validate[required]" name="billcity" id="billcity" value="<?php echo $billinfo['billcity']; ?>" tabindex="118"/>
										<label for="billcity">City<span class="mandatoryfildclass billdetailmand hidedisplay">*</span></label>
									</div>
									<div class="input-field large-12 columns">
										<input type="text" class="validate[required]" name="billzip" id="billzip" value="<?php echo $billinfo['billzip']; ?>" tabindex="119"/>
										<label for="billzip">Pincode<span class="mandatoryfildclass billdetailmand hidedisplay">*</span></label>
									</div>
									<div class="input-field large-12 columns">
										<input type="text" class="validate[required]" name="billstate" id="billstate" value="<?php echo $billinfo['billstate']; ?>" tabindex="120"/>
										<label for="billstate">State<span class="mandatoryfildclass billdetailmand hidedisplay">*</span></label>
									</div>
									<div class="input-field large-12 columns">
										<input type="text" class="validate[required]" name="billcountry" id="billcountry" value="<?php echo $billinfo['billcountry']; ?>" tabindex="121"/>
										<label for="billcountry">Country<span class="mandatoryfildclass billdetailmand hidedisplay">*</span></label>
									</div>
									<div class="large-12 columns submitkeyboard hidedisplay" style="text-align:right" id="billdetupdatediv">
										<div class="large-6 columns">
											<label></label>
											<input id="billdetailsupdatecancel" class="btn frmtogridbutton addbtnclass addkeyboard" type="button" value="Cancel" tabindex="104" name="billdetailsupdatecancel">
										</div>
										<div class="large-6 columns">
											<label></label>
											<input id="billdetailsupdate" class="btn frmtogridbutton addbtnclass addkeyboard" type="button" value="Submit" tabindex="104" name="billdetailsupdate">
											</div>
									</div>
									<div class="large-12 columns">&nbsp;</div>
								</div>
							</div>
						</span>
					</form>
					<!-- hidden fields-->
					<?php
						$CI=& get_instance();
						$empid = $CI->Basefunctions->logemployeeid;
						$datasets = $userdata;
						$curid = $currency['currid'];
						$baseurl =  base_url();
						$infos = "uesn9s".base64_encode($datasets['email'])."a1o8cuidsn9s".base64_encode($curid)."a1o8eidsn9s".base64_encode($datasets['userid'])."a1o8tzsn9s".base64_encode($datasets['zone'])."a1o8bsurlsn9s".base64_encode($baseurl);
						$datas = strrev($infos."a1o8optsn9s".base64_encode('product'));
						$urldatasets = base64_encode($datas);
						$secure = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "");
						if($_SERVER['HTTP_HOST']=='localhost') {
							$root = "http".$secure."://".$_SERVER['HTTP_HOST']."/salesneuronsite/checkout.php";
						} else {
							$hname = explode('.',$_SERVER['HTTP_HOST']);
							$count = count($hname);
							$root = "http".$secure."://subscribe.salesneuron.".$hname[($count-1)]."/checkout.php";
						}
						$editinfos=$infos."a1o8optsn9s".base64_encode('addon');
						$editdatas = strrev($editinfos);
						$editurldatasets = base64_encode($editdatas);
					?>
					<input type="hidden" name="url" id="url" value="<?php echo $root; ?>" />
					<input type="hidden" name="customerinfo" id="customerinfo" value="<?php echo $urldatasets; ?>" />
					<input type="hidden" name="editcustomerinfo" id="editcustomerinfo" value="<?php echo $editurldatasets; ?>" />
					<input type="hidden" name="check" id="check" value="<?php echo $datasets['email']; ?>" />
				</div>
				<!-- Invoices -->
				<div id="subformspan2" class="hiddensubform hidedisplay transitionhiddenform">
					<div class="row">&nbsp;</div>
						<div class="large-12 columns paddingbtm">
						<div class="large-12 columns paddingzero">
							<div class="large-12 columns viewgridcolorstyle" style="padding-left:0;padding-right:0;">
								<?php
									$device = $this->Basefunctions->deviceinfo();
									if($device=='phone') {
										echo '<div class="large-12 columns paddingzero forgetinggridname" id="invoicelistgridwidth"><div class="desktop row-content inner-gridcontent" id="invoicelistgrid" style="height:480px;top:0px;">
											<!-- Table header content , data rows & pagination for mobile-->
										</div>
									<!--<footer class="inner-gridfooter footercontainer" id="invoicelistgridfooter">
											 Footer & Pagination content
										</footer> --></div>';
									} else {
										echo '<div class="large-12 columns forgetinggridname" id="invoicelistgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent  viewgridcolorstyle borderstyle" id="invoicelistgrid" style="max-width:2000px; height:530px;top:0px;">
										<!-- Table content[Header & Record Rows] for desktop-->
										</div>
										<!-- <div class="inner-gridfooter footer-content footercontainer" id="invoicelistgridfooter">
											Footer & Pagination content 
										</div>--></div>';
									}
								?>
							</div>
						</div>
					</div>
				</div>
				<!-- Add-ons-->
				<div id="subformspan3" class="hiddensubform hidedisplay transitionhiddenform">
				<div class="row">&nbsp;</div>
					<div class="large-12 columns paddingbtm">
						<div class="large-12 columns paddingzero">
							<div class="large-12 columns viewgridcolorstyle" style="padding-left:0;padding-right:0;">
									<?php
									$device = $this->Basefunctions->deviceinfo();
									if($device=='phone') {
										echo '<div class="large-12 columns paddingzero forgetinggridname" id="addonslistgridwidth"><div class="desktop row-content inner-gridcontent" id="addonslistgrid" style="height:480px;top:0px;">
											<!-- Table header content , data rows & pagination for mobile-->
										</div>
										<!--<footer class="inner-gridfooter footercontainer" id="addonslistgridfooter">
											 Footer & Pagination content 
										</footer> --></div>';
									} else {
										echo '<div class="large-12 columns forgetinggridname " id="addonslistgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent  viewgridcolorstyle borderstyle" id="addonslistgrid" style="max-width:2000px; height:530px;top:0px;">
										<!-- Table content[Header & Record Rows] for desktop-->
										</div>
										<!--<div class="inner-gridfooter footer-content footercontainer" id="addonslistgridfooter">
											 Footer & Pagination content
										</div> --></div>';
									}
								?>
							</div>
						</div>
					</div>
				</div>
				<!--hidden fields-->
				<input type="hidden" name="demodata" id="demodata" value="<?php echo $demodata;?>" />
			</div>			
		</div>
	</div>
</div>
