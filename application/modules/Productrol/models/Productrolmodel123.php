<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Productrolmodel extends CI_Model{
    public function __construct(){
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//sizemaster dd load function
	public function sizemasterdd() {
		$industryid = $this->Basefunctions->industryid;
		$info =$this->db->query("SELECT `sizemaster`.`sizemastername`, `sizemaster`.`sizemasterid`, `sizemaster`.`productid` FROM `sizemaster`  WHERE `sizemaster`.`status` = 1 ORDER BY `sizemaster`.`sizemasterid` DESC");
		return $info;
	}
	//account dd load function
	public function accountdd() {
		$industryid = $this->Basefunctions->industryid;
		$info =$this->db->query("SELECT `account`.`accountname`, `account`.`accountid` FROM `account`  WHERE `account`.`status` = 1 and `account`.`accountid` not in (2,3) ORDER BY `account`.`accountid` DESC");
		return $info;
	}
	//create producr rol
	public function createproductrolmodel() {
		if(isset($_POST['productid'])) {
			$_POST['productid']=$_POST['productid'];
		} else {
			$_POST['productid']=1;
		}
		if(isset($_POST['reordertypeid'])) {
			$_POST['reordertypeid']=$_POST['reordertypeid'];
		} else {
			$_POST['reordertypeid']=1;
		}
		if(isset($_POST['sizemasterid'])) {
			$_POST['sizemasterid']=$_POST['sizemasterid'];
		} else {
			$_POST['sizemasterid']=1;
		}
		if(isset($_POST['reorderquantity'])) {
			$_POST['reorderquantity']=$_POST['reorderquantity'];
		} else {
			$_POST['reorderquantity']='';
		}
		if(isset($_POST['maximumquantity'])) {
			$_POST['maximumquantity']=$_POST['maximumquantity'];
		} else {
			$_POST['maximumquantity']='';
		}
		if(isset($_POST['purityid'])) {
			$_POST['purityid']=$_POST['purityid'];
		} else {
			$_POST['purityid']='';
		}
		if(isset($_POST['accountid'])) {
			$_POST['accountid']=$_POST['accountid'];
		} else {
			$_POST['accountid']='';
		}
		if(isset($_POST['rangemasterid'])) {
			$_POST['rangemasterid']=$_POST['rangemasterid'];
		} else {
			$_POST['rangemasterid']='';
		}
		$check = $this->checkalreadyexistornot($_POST['productid'],$_POST['purityid'],$_POST['rangemasterid'],$_POST['sizemasterid'],$_POST['fromweight'],$_POST['toweight']);
		if($check == 'Notexist') {
			$productrol = array(
			'productid'=>$_POST['productid'],
			'reordertypeid'=>$_POST['reordertypeid'],
			'reorderquantity'=>$_POST['reorderquantity'],
			'minimumquantity'=>$_POST['minimumquantity'],
			'maximumquantity'=>$_POST['maximumquantity'],
			'sizemasterid'=>$_POST['sizemasterid'],
			'fromweight'=>$_POST['fromweight'],
			'toweight'=>$_POST['toweight'],
			'purityid'=>$_POST['purityid'],
			'accountid'=>$_POST['accountid'],
			'rangemasterid'=>$_POST['rangemasterid'],
			'branchid'=>$this->Basefunctions->branchid,
			'industryid'=>$this->Basefunctions->industryid
			);
			$productrol = array_merge($productrol,$this->Crudmodel->defaultvalueget());
			$this->db->insert('productrol',array_filter($productrol));
			$primaryid = $this->db->insert_id();
			echo 'SUCCESS';
		} else {
			echo 'Exist';
		}
	}
	//check the combo exist or not
	public function checkalreadyexistornot($productid,$purityid,$rangemasterid,$sizemasterid,$fromweight,$toweight) {
		$data = 'Notexist';
		$this->db->select('productrolid');
		$this->db->from('productrol');
		$this->db->where('productrol.productid',$productid);
		$this->db->where('productrol.purityid',$purityid);
		$this->db->where('productrol.rangemasterid',$rangemasterid);
		$this->db->where('productrol.sizemasterid',$sizemasterid);
		$this->db->where('productrol.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			$data = 'Exist'; 
		} else {
			$data = 'Notexist'; 
		}
		return $data;
	}
	/**	* product rol retrive	*/
	public function productrolretrieve() {
		$primary_id = $_GET['primaryid'];
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2);
		$this->db->select('productid,reordertypeid,reorderquantity,minimumquantity,maximumquantity,sizemasterid,purityid,rangemasterid,ROUND(productrol.fromweight,'.$round.') as fromweight,ROUND(productrol.toweight,'.$round.') as toweight,accountid',false);
		$this->db->from('productrol');
		$this->db->where('productrol.productrolid',$primary_id);				
		$this->db->limit(1);				
		$info=$this->db->get()->row();	
		$jsonarray=array(
			'productid' => $info->productid,
			'reordertypeid' => $info->reordertypeid,
			'sizemasterid'=> $info->sizemasterid,
			'fromweight'=> $info->fromweight,
			'toweight' => $info->toweight,
			'minimumquantity' =>$info->minimumquantity,							
			'reorderquantity' =>$info->reorderquantity,
			'maximumquantity' => $info->maximumquantity,
			'purityid' => $info->purityid,
			'rangemasterid' => $info->rangemasterid,
			'accountid' => $info->accountid
		);
		echo json_encode($jsonarray);
	}
	/**	*update product rol	*/
	public function productrolupdatemodel() {
		$primaryid = $_POST['primaryid'];
		if(isset($_POST['productid'])) {
			$_POST['productid']=$_POST['productid'];
		} else {
			$_POST['productid']=1;
		}
		if(isset($_POST['reordertypeid'])) {
			$_POST['reordertypeid']=$_POST['reordertypeid'];
		} else {
			$_POST['reordertypeid']=1;
		}
		if(isset($_POST['sizemasterid'])) {
			$_POST['sizemasterid']=$_POST['sizemasterid'];
		} else {
			$_POST['sizemasterid']=1;
		}
		if(isset($_POST['reorderquantity'])) {
			$_POST['reorderquantity']=$_POST['reorderquantity'];
		} else {
			$_POST['reorderquantity']='';
		}
		if(isset($_POST['maximumquantity'])) {
			$_POST['maximumquantity']=$_POST['maximumquantity'];
		} else {
			$_POST['maximumquantity']='';
		}
		if(isset($_POST['purityid'])) {
			$_POST['purityid']=$_POST['purityid'];
		} else {
			$_POST['purityid']='';
		}
		if(isset($_POST['accountid'])) {
			$_POST['accountid']=$_POST['accountid'];
		} else {
			$_POST['accountid']='';
		}
		if(isset($_POST['rangemasterid'])) {
			$_POST['rangemasterid']=$_POST['rangemasterid'];
		} else {
			$_POST['rangemasterid']='';
		}
		$productrol = array(
			'productid'=>$_POST['productid'],
			'reordertypeid'=>$_POST['reordertypeid'],
			'reorderquantity'=>$_POST['reorderquantity'],
			'minimumquantity'=>$_POST['minimumquantity'],
			'maximumquantity'=>$_POST['maximumquantity'],
			'sizemasterid'=>$_POST['sizemasterid'],
			'fromweight'=>$_POST['fromweight'],
			'toweight'=>$_POST['toweight'],
			'purityid'=>$_POST['purityid'],
			'accountid'=>$_POST['accountid'],
			'rangemasterid'=>$_POST['rangemasterid']
		);
		$productrol = array_merge($productrol,$this->Crudmodel->defaultvalueget());
		$this->db->where_in('productrolid',$primaryid);
		$this->db->update('productrol',array_filter($productrol));
        echo 'SUCCESS';
	}
	/** *product rol delete */
	public function productroldeletemodel() {
		$primaryid = array_filter(explode(',',$_GET['primaryid']));
		if(count($primaryid) > 0) {
			$this->db->where_in('productrolid',$primaryid);
			$this->db->update('productrol',$this->Basefunctions->delete_log());
		}
		echo 'SUCCESS';
	}
	//check weight already exist or not
	public function checkweightexistmodel() {
		$productid = $_POST['productid'];
		$purityid = $_POST['purityid'];
		$rangemasterid = $_POST['rangemasterid'];
		$primaryid = $_POST['primaryid'];
		$sizemasterid=$_POST['sizemasterid'];
		if($primaryid == ''){
			$primaryid = 1;
		}
		$table = 'productrol';
		$tableprimaryid = $table.'id';
      	$this->db->select($tableprimaryid);
        $this->db->from($table);
		$this->db->where('productrol.rangemasterid',$rangemasterid);
		$this->db->where('productrol.productid',$productid);
		$this->db->where('productrol.purityid',$purityid);
		$this->db->where('productrol.industryid',$this->Basefunctions->industryid);
		$this->db->where('productrol.sizemasterid',$sizemasterid);
		if($primaryid != 1) {
			$this->db->where_not_in('productrol.productrolid',$primaryid);
		}
		$this->db->where('status',$this->Basefunctions->activestatus);
		$res=$this->db->get();
		if($res->num_rows() > 0) {
			$data['count'] =$res->num_rows();
			$data['productrolid']= $res->row()->$tableprimaryid;
		} else {
			$data['count'] = 0 ;
			$data['productrolid']= '';
		}
		echo json_encode($data);
	}
}