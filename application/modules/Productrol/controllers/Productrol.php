<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Productrol extends MX_Controller{
    public function __construct(){
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Productrol/Productrolmodel');	
    }
    //first basic hitting view
    public function index() {
    	$moduleid = array(137);
    	sessionchecker($moduleid);
		//action
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		/* Product ROL*/
		$data['mainlicense'] = array(
			'productidshow'=>$this->Basefunctions->get_company_settings('productidshow'),
			'reorderqty'=>$this->Basefunctions->get_company_settings('reorderqty')
		);
		$data['weight_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2);
		$data['product'] = $this->Basefunctions->product_dd();
		$data['purity'] = $this->Basefunctions->purity_groupdropdown();
		$data['reordertype'] = $this->Basefunctions->simpledropdownwithcond('reordertypeid','reordertypename','reordertype','moduleid','137');
		$data['rangemaster'] = $this->Productrolmodel->rangemasterddfetch();
		$data['account'] = $this->Basefunctions->accountgroup_dd('16');
		$data['sizemaster'] = $this->Productrolmodel->sizemasterdd();
		$this->load->view('Productrol/productrolview',$data);
	}
	/***create product rol   */
	public function createproductrol() {  
    	$this->Productrolmodel->createproductrolmodel();
    }
	/***retrieve product rol   */
	public function productrolretrieve() {  
    	$this->Productrolmodel->productrolretrieve();
    }
	/** *update product addon  */
	public function productrolupdate() {  
    	$this->Productrolmodel->productrolupdatemodel();
    }
	/** *delete product rol */
	public function productroldelete() {  
    	$this->Productrolmodel->productroldeletemodel();
    }
	//check weight already exist or not
	public function checkweightexist() {
		$this->Productrolmodel->checkweightexistmodel();
	}
}