<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<div id="productrolview" class="gridviewdivforsh">
				<?php
					$loggedinuserroleid = $this->Basefunctions->userroleid;
					$dataset['gridtitle'] = $gridtitle;
					$dataset['titleicon'] = $titleicon;
					$dataset['modtabgrp'] = $modtabgrp;
					$dataset['gridid'] = 'productrolgrid';
					$dataset['gridwidth'] = 'productrolgridwidth';
					$dataset['gridfooter'] = 'productrolgridfooter';
					$dataset['viewtype'] = 'disable';
					$dataset['moduleid'] = '137';
					$this->load->view('Base/singlemainviewformwithgrid.php',$dataset);
					$defaultrecordview = $this->Basefunctions->get_company_settings('mainviewdefaultview');
					echo hidden('mainviewdefaultview',$defaultrecordview);
				?>
			</div>
			<div id="productrolformdiv" class="singlesectionaddform">
				<div class="large-12 columns addformunderheader">&nbsp;</div>
				<div class="large-12 columns scrollbarclass addformcontainer">
					<div class="row mblhidedisplay">&nbsp;</div>
					<div id="productrolcharge">
						<div class="large-12 columns mblnopadding" style="padding-left:0px !important;">
							<div class="closed effectbox-overlay effectbox-default " id="groupsectionoverlay">							
							<form method="POST" name="productaddonform" class="productaddonform"  id="productaddonform" style="height: 100% !important;">
								<div id="productroladdvalidate" class="validationEngineContainer" style="height: 100% !important;">		
								<div id="productrolupdatevalidate" class="validationEngineContainer" style="height: 100% !important;">		
									<div class="large-4 large-offset-8 columns" style="padding-right:0px !important;height: 100% !important;padding-left:0px !important;">	
										<div class="large-12 columns cleardataform border-modal-8px" style="padding-left: 0 !important; padding-right: 0 !important;">
										<div class="large-12 columns sectionheaderformcaptionstyle">Product ROL</div>
											<div class="large-12 columns sectionpanel">
												<div class="static-field large-6 columns ">
													<label>Reorder Type<span class="mandatoryfildclass" id="reordertypeid_req">*</span></label>
													<select id="reordertypeid" name="reordertypeid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="102" disabled>
														<option value=""></option>
														<?php foreach($reordertype as $key):
														?>
														<option value="<?php echo $key->reordertypeid;?>" data-reordertypename="<?php echo $key->reordertypename;?>"> <?php echo $key->reordertypename;?></option>
														<?php endforeach;?>
													</select>
												</div>
												<div class="static-field large-6 columns ">
													<label>Product Name<span class="mandatoryfildclass" id="productid_req">*</span></label>			
													<select id="productid" name="productid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="100">
														<option value=""></option>
														<?php foreach($product->result() as $key):
														?>
														<option value="<?php echo $key->productid;?>" data-productname="<?php echo $key->productname;?>" data-purityid="<?php echo $key->purityid;?>" data-size="<?php echo $key->size;?>"> <?php if($mainlicense['productidshow'] == 'YES') { echo $key->productid.' - '.$key->productname; } else { echo $key->productname;} ?></option>
														<?php endforeach;?>
													</select>
												</div>
												<div class="static-field large-6 columns ">
													<label>Purity Name<span class="mandatoryfildclass" id="purityid_req">*</span></label>			
													<select id="purityid" name="purityid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="106">
														<option value=""></option>
														<?php $prev = ' ';
														foreach($purity->result() as $key):
														$cur = $key->metalid;
														$a="";
														if($prev != $cur )
														{
															if($prev != " ")
															{
																 echo '</optgroup>';
															}
															echo '<optgroup  label="'.$key->metalname.'">';
															$prev = $key->metalid;
														}
														?>
														<option data-melting="<?php echo $key->melting;?>" data-metalid="<?php echo $key->metalid;?>" value="<?php echo $key->purityid;?>"><?php echo $key->purityname;?></option>
														<?php endforeach;?>
													</select>
												</div>
												<div class="static-field large-6 columns" id="rangemasteriddivhid">
													<label>Wt Range<span class="mandatoryfildclass" id="rangemaster_req">*</span></label>						
													<select id="rangemasterid" name="rangemasterid" class="chzn-select" data-validation-engine="validate[required]" tabindex="103" >
														<?php foreach($rangemaster as $key):?>
														<option value="<?php echo $key->rangemasterid;?>" data-metalid="<?php echo $key->metalid;?>" data-fromrange="<?php echo $key->fromrange;?>" data-torange="<?php echo $key->torange;?>" data-rangemastername="<?php echo $key->rangemastername;?>">
														<?php echo $key->rangemastername;?></option>
														<?php endforeach;?>		
													</select>
												</div>
												<div class="static-field small-6 columns" id="sizemasteriddivhid">
													<label>Size<span class="mandatoryfildclass" id="sizemasterid_req">*</span></label>						
													<select id="sizemasterid" name="sizemasterid" class="chzn-select" data-validation-engine="" tabindex="103" >
														<?php foreach($sizemaster->result() as $key):?>
														<option value="<?php echo $key->sizemasterid;?>" data-productid="<?php echo $key->productid;?>" data-sizemastername="<?php echo $key->sizemastername;?>">
														<?php echo $key->sizemastername;?></option>
														<?php endforeach;?>		
													</select>
												</div>
												<div class="static-field small-6 columns clearonstocktypechange" id="accountid-div">
													<label>Vendor</label>						
													<select id="accountid" name="accountid" class="chzn-select" data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="107">
														<option value=""></option>
														<?php $prev = ' ';
															foreach($account->result() as $key):
															$cur = $key->accounttypeid;
															$a="";
															if($prev != $cur )
															{
																if($prev != " ")
																{
																	 echo '</optgroup>';
																}
																echo '<optgroup  label="'.$key->accounttypename.'">';
																$prev = $key->accounttypeid;
															}
														?>
														<option value="<?php echo $key->accountid;?>"><?php echo $key->accountname;?></option>
														<?php endforeach;?>
													</select>
												</div>
												<div class="input-field small-6 columns ">
													<input type="text" class="" id="fromweight" name="fromweight" value="" tabindex="106" data-validation-engine="validate[required,decval[<?php echo $weight_round?>],custom[number]]" readonly>
													<label for="fromweight">From Weight<span class="mandatoryfildclass">*</span></label>
												</div>
												<div class="input-field small-6 columns ">
													<input type="text" class="" data-validategreat="" id="toweight" name="toweight" value="" tabindex="107" data-validation-engine="validate[required,decval[<?php echo $weight_round?>],custom[number]]" readonly>
													<label for="toweight">To Weight<span class="mandatoryfildclass">*</span></label>
												</div>
												<div class="input-field small-6 columns ">
													<input type="text" class="" id="minimumquantity" name="minimumquantity" value="" tabindex="108" data-validation-engine="validate[required,maxSize[15],custom[onlyWholeNumber],min[1]]">
													<label for="minimumquantity">Min Qty<span class="mandatoryfildclass">*</span></label>
												</div>
												<?php if($mainlicense['reorderqty'] == '1') { ?>
													<div class="input-field small-6 columns reorderquantitydiv">
														<input type="text" id="reorderquantity" name="reorderquantity" value="" tabindex="112" data-validation-engine="validate[maxSize[15],custom[onlyWholeNumber]]">
														<label for="reorderquantity">Reorder Qty</label>
													</div>
													<div class="input-field small-6 columns maximumquantitydiv">
														<input type="text" id="maximumquantity" name="maximumquantity" value="" tabindex="112" data-validation-engine="validate[maxSize[15],custom[onlyWholeNumber]]">
														<label for="maximumquantity">Max Qty </label>
													</div>
												<?php } ?>
												<div class="row">&nbsp;</div>
											</div>
											<div class="divider large-12 columns"></div>
											<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">
												<input type ="hidden" id="primaryid" name="primaryid"/>
												<input type ="hidden" id="producrolsortcolumn" name="producrolsortcolumn"/>
												<input type ="hidden" id="productrolsortorder" name="productrolsortorder"/>
												<input type="button" class="alertbtnyes addkeyboard" id="productroladd" name="productroladd" value="Save" tabindex="">
												<input type="button" class="alertbtnyes updatekeyboard" id="productrolupdate" name="productrolupdate" value="Save" tabindex="" style="display: none;">	
												<input type="button" class="alertbtnno addsectionclose"  value="Close" name="cancel" tabindex="">
											</div>
										</div>
									</div>
								</div>
								</div>
							</form>
							</div>
						</div>
				   </div>
				</div>
			</div>
		</div>			
	</div>
</div>
<!--Productaddon Double Alert Overlay-->
<div class="large-12 columns" style="position:absolute;">
		<div class="overlay alertsoverlay overlayalerts" id="productrolalertsdouble">	
			<div class="row">&nbsp;</div>
			<div class="row sectiontoppadding"></div>
			<div class="">
				<div class="row">&nbsp;</div>
				<div class="alert-panel">
					<div class="alertmessagearea">
					<div class="alert-title">Product ROL Details</div>
					<div class="alert-message">
					<span class="" style="display:block;padding-bottom:5px;font-size:0.9rem">Product : <span id="alertproductname"></span> </span>
						<span class="" style="display:block;padding-bottom:5px;font-size:0.9rem">Reorder Type : <span id="alerttypename"></span> </span>
						<span class="" style="display:block;padding-bottom:5px;font-size:0.9rem">Size Master : <span id="alertsizemastername"></span> </span>
						<span class="" style="display:block;padding-bottom:5px;font-size:0.9rem">From weight : <span id="alertfromweight"></span> </span>
						<span class="" style="display:block;padding-bottom:5px;font-size:0.9rem">Ro weight : <span id="alerttoweight"></span> </span>
						<div class="large-12 column">&nbsp;</div>
						<span class="" style="display:block;padding-bottom:5px;font-size:0.9rem"> Already The above combination of rol created.Do you want to edit the rol?</span>
						<div class="large-12 column">&nbsp;</div>
					</div>
					</div>
					<div class="alertbuttonarea">
						<span class="firsttab" tabindex="1000"></span>
						<input type="button" id="alertsdoubleeditproductrol" name="" tabindex="1001" value="Yes" class="alertbtn  ffield" >
						<input type="button" id="alertsdoublecloseproductrol" name="" value="No" tabindex="1002" class="flloop alertbtn alertsoverlaybtn" >
						<input type="hidden" id="editchargeid" name="editchargeid">
						<span class="lasttab" tabindex="1003"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!----Product charge copy overlay----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="productaddoncopyalertsdouble" style="overflow-y: auto;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="large-4 medium-6 small-11 large-centered medium-centered small-centered columns" >
			<form method="POST" name="billnumberoverlayform" style="" id="billnumberoverlayform" action="" enctype="" class="overlayborder clearformbillnumberoverlay">
				<span id="billnumberoverlayvalidation" class="validationEngineContainer"> 
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 columns sectionheaderformcaptionstyle">
							Product Details
						</div>
						<div class="large-12 columns" style="background-color: #617d8a;">
						<div class="static-field large-12 columns">
							<label>Copy From<span class="mandatoryfildclass"></span></label>								
							<select id="chargedproduct" name="chargedproduct" class="chzn-select" data-prompt-position="topLeft:14,36" tabindex="100">
								<option value=""></option>
								
							</select>
						</div>
						</div>
					<?php
							$device = $this->Basefunctions->deviceinfo();
							if($device=='phone') {
								echo '<div class="large-12 columns paddingzero forgetinggridname" id="productchargecopygridwidth"><div id="productchargecopygrid" class="inner-row-content inner-gridcontent" style="height:300px;">
									<!-- Table header content , data rows & pagination for mobile-->
								</div>
								<!--<footer class="inner-gridfooter footercontainer" id="productchargecopygridfooter">
									 Footer & Pagination content 
								</footer>--></div>';
							} else {
								echo '<div class="large-12 columns forgetinggridname" id="productchargecopygridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="productchargecopygrid" style="max-width:2000px; height:300px;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
							<!--<div class="inner-gridfooter footer-content footercontainer" id="productchargecopygridfooter">
									 Footer & Pagination content 
								</div>--></div>';
							}
						?></div>
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
							<input type="button" id="productchargecopygridsubmit" name="productchargecopygridsubmit" value="Submit" class="alertbtn">
							<input type="button" id="productchargeclose" name="productchargeclose" value="Cancel" class="alertbtn">
						</div>
					</div>
					<!-- hidden fields -->
					<input type="hidden" name="chargebasedproductid" id="chargebasedproductid" />
				</span>
			</form>
		</div>
	</div>
</div>
	