<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Termsandcondition extends MX_Controller{
	//To load the Register View
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Termsandcondition/Termsandconditionmodel');
    }
	public function index() {
		$moduleid = array(224);
		sessionchecker($moduleid);
		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(224);
		$viewmoduleid = array(224);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Termsandcondition/termsandconditionview',$data);
	}
	//Create New data
	public function newdatacreate() {  
    	$this->Termsandconditionmodel->newdatacreatemodel();
    }
	//Update old data
    public function datainformationupdate() {
        $this->Termsandconditionmodel->datainformationupdatemodel();
    }
	//delete old informtion
    public function deleteinformationdata() {
		$moduleid = 224;
        $this->Termsandconditionmodel->deleteoldinformation($moduleid);
    } 
	//drop down value set with multiple condition
    public function fetchmaildddatawithmultiplecond() {
        $this->Termsandconditionmodel->fetchmaildddatawithmultiplecondmodel();
    } 
	//terms and condition data fetch
	public function termsandcontdatafetch() {
		$this->Termsandconditionmodel->termsandcontdatafetchmodel();
	}
	//editer valuefetch editervaluefetch	
	public function editervaluefetch() {
		$filename = $_GET['filename']; 
		$newfilename = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]);
			echo json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			echo json_encode($tccontent);
		}
	}
}