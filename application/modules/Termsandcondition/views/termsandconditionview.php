<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
</head>
<body>
	<?php
		$moduleid = implode(',',$moduleids);
		$device = $this->Basefunctions->deviceinfo();
		$dataset['gridenable'] = "yes"; //no
		$dataset['griddisplayid'] = "termsandconditioncreationview";
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['spanattr'] = array();
		$dataset['gridtableid'] = "termsandconditionviewgrid";
		$dataset['griddivid'] = "termsandconditionviewgridnav";
		$dataset['moduleid'] = $moduleids;
		$dataset['forminfo'] = array(array('id'=>'termsandconditioncreationformadd','class'=>'hidedisplay','formname'=>'termsandconditioncreationform'));
		$this->load->view('Base/basedeleteform');
		if($device=='phone') {
			$this->load->view('Base/gridmenuheadermobile',$dataset);
			$this->load->view('Base/overlaymobile');
			$this->load->view('Base/viewselectionoverlay');
		} else {
			$this->load->view('Base/gridmenuheader',$dataset);
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
	?>
</body>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js Files -->
	<script src="<?php echo base_url();?>js/Termsandcondition/termsandcondition.js" type="text/javascript"></script>
	<!--For Tablet and Mobile view Dropdown Script-->
	<script>
		$(document).ready(function(){
			$("#tabgropdropdown").change(function(){
				var tabgpid = $("#tabgropdropdown").val();
				$('.sidebaricons[data-subform="'+tabgpid+'"]').trigger('click');
			});
		});
	</script>
</html>