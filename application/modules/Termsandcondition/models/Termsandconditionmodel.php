<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Model File
class Termsandconditionmodel extends CI_Model{    
    public function __construct() {		
		parent::__construct(); 
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    } 
	//To Create New data
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$restricttable = explode(',',$_POST['resctable']);
		$default = $_POST['setdefault'];
		if($default == "Yes"){
			$lastupdatedate = date($this->Basefunctions->datef);
			$lastupdateuserid = $this->Basefunctions->userid;
			$data = array('setdefault' => 'No','lastupdatedate'=>$lastupdatedate,'lastupdateuserid'=>$lastupdateuserid);
			$priviledgeid = $_POST['moduleprivilegeid'];
			$this->db->where_in('moduleprivilegeid', $priviledgeid);
			$this->db->update('termsandcondition', $data);
		}
		$primaryid = $this->Crudmodel->datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable);
		echo "TRUE";
	}
	//update data information
	public function datainformationupdatemodel() {
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		$partablename = $this->Crudmodel->filtervalue($elementpartable);
		$fieldstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fieldstable);
		$primaryid = $_POST['primarydataid'];
		$restricttable = explode(',',$_POST['resctable']);
		$default = $_POST['setdefault'];
		if($default == "Yes"){
			$lastupdatedate = date($this->Basefunctions->datef);
			$lastupdateuserid = $this->Basefunctions->userid;
			$data = array('setdefault' => 'No','lastupdatedate'=>$lastupdatedate,'lastupdateuserid'=>$lastupdateuserid);
			$priviledgeid = $_POST['moduleprivilegeid'];
			$this->db->where_in('moduleprivilegeid', $priviledgeid);
			$this->db->update('termsandcondition', $data);
		}
		$this->Crudmodel->dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable);
		echo 'TRUE';
	}
	//delete old information
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				echo 'TRUE';
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			echo 'TRUE';
		}
	}
	//dropdown value set with multiple condition
	public function fetchmaildddatawithmultiplecondmodel() {
		$i=0;
		$dname=$_GET['dataname'];
		$did=$_GET['dataid'];
		$dataattrname1=$_GET['othername1'];
		$dataattrname2=$_GET['othername2'];
		$table=$_GET['datatab'];
		$whfield=$_GET['whdatafield'];
		$whdata=$_GET['whval'];
		$mulcond=explode(",",$whfield);
		$mulcondval=explode(",",$whdata);
		$i=0;
		$this->db->select("$dname,$did,$dataattrname1,$dataattrname2");
		$this->db->from($table);
		foreach($mulcond AS $condname) {
			if($mulcondval[$i] != "0" && $mulcondval[$i] != "") {
				$this->db->where($condname,$mulcondval[$i]);
			}
			$i++;
		}
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {		
			foreach($result->result()as $row) {
				$data[$i]=array('datasid'=>$row->$did,'dataname'=>$row->$dname,'otherdataattrname1'=>$row->$dataattrname1,'otherdataattrname2'=>$row->$dataattrname2);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//terms and condition data fetch
	public function termsandcontdatafetchmodel() {
		$id = $_GET['id'];
		$this->db->select('termsandconditionstermsandconditions_editorfilename,termsandconditionname,moduleid,moduleprivilegeid,setdefault');
		$this->db->from('termsandcondition');
		$this->db->where('termsandcondition.termsandconditionid',$id);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$data = array('filename'=>$row->termsandconditionstermsandconditions_editorfilename,'moduleid'=>$row->moduleid,'moduleprivilegeid'=>$row->moduleprivilegeid,'setdefault'=>$row->setdefault,'name'=>$row->termsandconditionname);
			}
			echo json_encode($data);
		}
	}
}      