<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Findduplicates extends CI_Controller {
	function __construct() {
    	parent::__construct();	
		$this->load->model('Findduplicates/Findduplicatesmodel');
		$this->load->helper('formbuild');
		$this->load->model('Base/Basefunctions');
    }
	public function index() {
		$moduleid = array(256);
		sessionchecker($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['module'] = $this->Basefunctions->simpledropdownwithcond('moduleid','modulename,menuname','module','moduleprivilegeid','256');
		$data['andor'] = $this->Basefunctions->simpledropdown('massandorcond','massandorcondid,massandorcondname','massandorcondname');
		$data['condition'] = $this->Basefunctions->simpledropdown('masscondition','massconditionid,massconditionname','massconditionname');
		$this->load->view('Findduplicates/findduplicatesview',$data);
	}
	//cond field name load
	public function viewdropdownload() {
		$this->Findduplicatesmodel->viewdropdownloadmodel();
	}
	//mass update default view fetch
	public function defaultviewfetch() {
		$mid = $_GET['modid'];
		$viewid = $this->Findduplicatesmodel->defaultviewfetchmodel($mid);
		echo $viewid;
	}
	//parent table get
	public function parenttableget() {
		$this->Findduplicatesmodel->parenttablegetmodel();
	}
	//fetch json data information
	public function gridvalinformationfetch() {
		$creationid = $_GET['viewid'];
		$rowid = $_GET['parentid'];
		$viewcolmoduleids = $_GET['viewfieldids'];
		$grid=$this->Basefunctions->getpagination();
		$result=$this->Findduplicatesmodel->viewdynamicdatainfofetch($grid['sidx'],$grid['sord'],$grid['start'],$grid['limit'],$grid['wh'],$creationid,$rowid,$viewcolmoduleids);
		$pageinfo=$this->Basefunctions->page();
		header("Content-type: text/xml;charset=utf-8");
		$s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .= "<rows>";
        $s .= "<page>".$_GET['page']."</page>";
        $s .= "<total>".$pageinfo['totalpages']."</total>";
        $s .= "<records>".$pageinfo['count']."</records>";
		$count = 0;
		if(count($result[0]) > 0) {
			$count = count($result[0]['colmodelname']);
		}
		foreach($result[1]->result() as $row) {
			$s .= "<row id='".$row->$rowid."'>";
			for($i=0;$i<$count;$i++) {
				$column_data = $result[0]['colmodelname'][$i];
				$data = preg_replace('~&~','&amp;',$row->$column_data);
				$s .= "<cell><![CDATA[".$data."]]></cell>";
			}
			$s .= "</row>";
		}
		$s .= "</rows>";  
		echo $s;
	}
	//field name based drop down value
	public function fieldviewnamebespicklistdddvalue() {
		$this->Findduplicatesmodel->fieldviewnamebespicklistdddvaluemodel();
	}
	//field name based drop down value
	public function viewfieldnamebesdddvalue() {
		$this->Findduplicatesmodel->viewfieldnamebesdddvaluemodel();
	}
	//check box value get
	public function checkboxvalueget() {
		$this->Findduplicatesmodel->checkboxvaluegetmodel();
	}
	//duplicate record delete
	public function duplicaterecdelete() {
		$moduleid = $_GET['moduleid'];
		$ids = $_GET['ids'];
		$parenttable = $_GET['parenttable'];
		$result = $this->Findduplicatesmodel->duplicaterecdeletemodel($moduleid,$parenttable,$ids);
		echo $result;
	}
	//merge data information fetch
	public function mergedatainformationfetch() {
		$m=1;
		$ids=explode(',',$_GET['rowids']);
		$resultdataset = $this->Findduplicatesmodel->mergedatainformationfetchmodel();
		header("Content-type: text/xml;charset=utf-8");
		$s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .=  "<rows>";
        $s .= "<page>1</page>";
        $s .= "<total>1</total>";
        $s .= "<records>1</records>";
		for($i=0;$i<count($resultdataset[0]);$i++) {
			$intcolname = $resultdataset[0][$i]['internalcolumnname'];
			$s .= "<row id='".$m."'>";
			$s .= "<cell><![CDATA[".$resultdataset[0][$i]['internalcolumnname']."]]></cell>";
			$s .= "<cell><![CDATA[".$resultdataset[0][$i]['columnname']."]]></cell>";
			$s .= "<cell><![CDATA[".$resultdataset[0][$i]['tablename']."]]></cell>";
			$s .= "<cell><![CDATA[".$resultdataset[0][$i]['uitypeid']."]]></cell>";
			$s .= "<cell><![CDATA[".$resultdataset[0][$i]['parenttable']."]]></cell>";
			for($h=0;$h<count($ids);$h++) {
				if( isset($resultdataset[1][$h][$intcolname]) ) {
					$data = preg_replace('~&~','&amp;', $resultdataset[1][$h][$intcolname]);
					$s .= "<cell><![CDATA[".$data."]]></cell>";
				} else {
					$s .= "<cell></cell>";
				}
			}
			$s .= "</row>";
			$m++;
		}
		$s .= "</rows>";  
		echo $s;
	}
	//merge duplicate records
	public function mergeduplicaterecords() {
		$this->Findduplicatesmodel->mergeduplicaterecordsmodel();
	}
}