<!-- For Date Formatter -->
<style type="text/css">
	.stepperformcontainer {
      margin-top: 25px;
}
</style>
<?php 
$CI =& get_instance();
$appdateformat = $CI->Basefunctions->appdateformatfetch(); ?>
<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<?php
				$device = $this->Basefunctions->deviceinfo();
				$dataset['gridtitle']=$gridtitle['title'];
				$dataset['titleicon']=$gridtitle['titleicon'];
				$dataset['moduelid']=256;
				$dataset['formtype']= 'stepper';
				$this->load->view('Base/mainviewaddformheader',$dataset);
			?>
			<div class="large-12 columns addformunderheader">&nbsp;</div>
			<div class="large-12 columns scrollbarclass  stepperformcontainer" style="background:#f2f3fa;">
				<div id="subformspan1" class="hiddensubform">
				<div class="" id="">
					<div class="large-4 columns">
					<form method="POST" name="findduplicateaddform" class="" action ="" id="findduplicateaddform">
						<span id="findduplicatecriteriaaddgridvalidation" class="validationEngineContainer" >
							<span id="findduplicatecriteriaaddgridvalidation" class="validationEngineContainer">
								<div class="large-12 columns paddingbtm ">
									<div class="large-12 columns cleardataform borderstyle z-depth-5" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
										<div class="large-12 columns headerformcaptionstyle">Criteria</div>
										<div class="large-12 columns">
											<label>Module Name<span class="mandatoryfildclass">*</span></label>
											<select class="chzn-select validate[required]" data-placeholder="Select" data-prompt-position="bottomLeft:14,36" id="moduleid" name="moduleid" tabindex="101">
												<option value="">Select</option>
												<?php foreach($module as $key):?>
													<option value="<?php echo $key->moduleid; ?>" data-modname="<?php echo $key->modulename;?>"><?php echo $key->modulename; ?> </option>
												<?php endforeach; ?>
											</select>
										</div>
										<div class="large-12 columns">
											<label>Field Name<span class="mandatoryfildclass">*</span></label>
											<select class="select2-container chzn-select dropdownchange validate[required] " data-placeholder="Select"data-prompt-position="bottomLeft:14,36" id="formfields" name="formfields" tabindex="102">
												<option value=""></option>
											</select>
										</div>
										<div class="large-12 columns">
											<label>Condition<span class="mandatoryfildclass">*</span></label>
											<select class="select2-container chzn-select dropdownchange validate[required]" data-placeholder="Select" data-prompt-position="bottomLeft:14,36" id="massconditionid" name="massconditionid" tabindex="103">
												<option value=""></option>
												<?php foreach($condition as $key):?>
													<option value="<?php echo $key->massconditionname;?>" data-condid="<?php echo $key->massconditionid;?>"><?php echo $key->massconditionname;?></option>
												<?php endforeach;?>
											</select>
										</div>
										<div class="large-12 columns viewcondclear" id="valuedivhid">
											<label>Value <span class="mandatoryfildclass">*</span></label>
											<input type="text" data-dateformater="<?php echo $appdateformat; ?>" class="validate[required]" data-prompt-position="bottomLeft" id="value" name="value"  tabindex="104" >
										</div>
										<div class="large-12 columns viewcondclear" style="display:none;" id="timevaluedivhid">
											<label>Value <span class="mandatoryfildclass">*</span></label>
											<input type="text" class="validate[required]" data-prompt-position="bottomLeft" id="timevalue" name="timevalue"  tabindex="105" >
										</div>
										<div class="large-12 columns viewcondclear" style="display:none;" id="criteriaddfieldvaluedivhid">
											<label>Value<span class="mandatoryfildclass">*</span></label>
											<select data-placeholder="Select"  class="validate[required] chzn-select chosenwidth dropdownchange" tabindex="106" data-prompt-position="bottomLeft:14,36" name="criteriaddfieldvalue" id="criteriaddfieldvalue">
												<option></option>
											</select>  
										</div>
										<div class="large-12 columns" id="massandorcondiddivhid">
											<label>And/Or<span class="mandatoryfildclass">*</span></label>
											<select class="select2-container chzn-select dropdownchange  validate[required]" data-placeholder="Select" data-prompt-position="bottomLeft:14,36" id="massandorcondid" name="massandorcondid" tabindex="107">
												<option value=""></option>
												<?php foreach($andor as $key):?>
													<option value="<?php echo $key->massandorcondname;?>" data-andorid="<?php echo $key->massandorcondid;?>"><?php echo $key->massandorcondname;?></option>
												<?php endforeach;?>
											</select>
										</div>
										<div class="large-12 columns submitkeyboard sectionalertbuttonarea" style="text-align: right">
										<input id="findduplicatecriaddbutton" class="alertbtn addkeyboard" type="button" value="Submit" name="findduplicatecriaddbutton" tabindex="108">
										<input id="categorydataupdatesubbtn" class="alertbtn updatekeyboard hidedisplay" type="button" value="Submit" name="categorydataupdatesubbtn" tabindex="109" style="display: none;">
										</div>
									</div>
									<!-- hidden values -->
									<input type="hidden" name="moduleidname" id="moduleidname" value="" />
									<input type="hidden" name="formfieldsname" id="formfieldsname" value="" />
									<input type="hidden" name="massconditionidname" id="massconditionidname" value="" />
									<input type="hidden" name="massandorcondidname" id="massandorcondidname" value="" />
									<input type="hidden" name="critreiavalue" id="critreiavalue" value="" />
									<input type="hidden" name="jointable" id="jointable" value="" />
									<input type="hidden" name="indexname" id="indexname" value="" />
									<input type="hidden" name="fieldid" id="fieldid" value="" />
								</div>
							</span>
						</span>
					</form>
					</div>
					</div>
				    <div class="large-8 columns paddingbtm">
						<div class="large-12 columns paddingzero ">
							<div class="large-12 columns viewgridcolorstyle borderstyle" style="padding-left:0;padding-right:0;">
								<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0 0.4rem;">
									<span class="large-6 medium-6 small-6 columns" style="text-align:left">Criteria List</span>
									<span class="large-6 medium-6 small-6 columns innergridicon" style="text-align:right">
										<span class="deleteiconclass" id="findduplicateeletebtn" title="Delete"><i class="material-icons">delete</i></span>
									</span>
								</div>
								<div class="large-12 columns frmtogridname forgetinggridname" id="findduplicatecriteriaaddgridwidth" style="padding-left:0;padding-right:0;">	
									<table id="findduplicatecriteriaaddgrid"></table>
									<div id="findduplicatecriteriaaddgridnav"> </div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="subformspan2" class="hidedisplay hiddensubform">
				<!--hidden values for delete operation -->
				<input type="hidden" name="allrecorddelete" id="allrecorddelete" value="No" />
				<input type="hidden" name="defaultviewcreationid" id="defaultviewcreationid" value="No" />
				<input type="hidden" name="parenttable" id="parenttable" value="" />
				<input type="hidden" name="parenttableid" id="parenttableid" value="" />
				<div class="large-12 columns paddingbtm">
						<div class="large-12 columns paddingzero">
							<div class="large-12 columns viewgridcolorstyle borderstyle" style="padding-left:0;padding-right:0;">
								<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0 0.4rem;">
									<span class="large-6 medium-6 small-6 columns" style="text-align:left">Record List</span>
									<span class="large-6 medium-6 small-6 columns innergridicon" style="text-align:right">
										<span id="finddupdeleteicon" title="Delete"><i class="material-icons deleteiconclass">delete</i></span>
										<span id="finddupmergeicon"  title="Merge"> <i class="material-icons mergeiconclass">merge_type</i></span>
									</span>
								</div>
								<div class="large-12 columns forgetinggridname " id="findduplicaterecordaddgridwidth" style="padding-left:0;padding-right:0;">	
									<table id="findduplicaterecordaddgrid"></table>
									<div id="findduplicaterecordaddgridnav"> </div>												
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="subformspan3" class="hidedisplay hiddensubform">
					<!--hidden text box-->
					<input type="hidden" name="mergedatarowid" id="mergedatarowid" value="" />
					<input type="hidden" name="mergedatarow" id="mergedatarow" value="" />
					<div class="large-12 columns paddingbtm">
						<div class="large-12 columns paddingzero">
							<div class="large-12 columns viewgridcolorstyle" style="padding-left:0;padding-right:0;">
								<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0 0.4rem;">
									<span class="large-6 medium-6 small-6 columns" style="text-align:left">Record List</span>
									<span class="large-6 medium-6 small-6 columns innergridicon" style="text-align:right">
										<span id="savemergerecordicon" class="saveiconclass" title="Save"><i class="material-icons">save</i> </span>
									</span>
								</div>
								<div class="large-12 columns forgetinggridname borderstyle" id="findduplicatemergeaddgridwidth" style="padding-left:0;padding-right:0;">
									<table id="findduplicatemergeaddgrid"></table>
									<div id="findduplicatemergeaddgridnav"> </div>			
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</div>
</div>