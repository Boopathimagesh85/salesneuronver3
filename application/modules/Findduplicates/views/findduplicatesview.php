<!DOCTYPE HTML>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
	<link href="<?php echo base_url();?>css/jqgrid.min.css" media="screen" rel="stylesheet" type="text/css" />
	<style>
		#gbox_findduplicatemergeaddgrid .ui-jqgrid-sortable{left:16px}		
	</style>
</head>
<body class="hidedisplay">
	<?php
		$this->load->view('findduplicatesform'); 
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$this->load->view('Base/overlaymobile');
		} else {
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
	?>
	<script src="<?php echo base_url();?>js/plugins/jqgrid/jquery.jqGrid.src.min.js"></script>
	<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js File -->
	<script src="<?php echo base_url();?>js/Findduplicates/findduplicates.js" type="text/javascript"></script>
	<!--For Tablet and Mobile view Dropdown Script-->
	<script>
		$(document).ready(function(){
			$("#tabgropdropdown").change(function(){
				var tabgpid = $("#tabgropdropdown").val(); 
				$('.sidebariconstab[data-subform="'+tabgpid+'"]').trigger('click');
			});			
		});
	</script>
</body>
</html>