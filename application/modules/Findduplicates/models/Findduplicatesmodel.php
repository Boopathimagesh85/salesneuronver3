<?php
Class Findduplicatesmodel extends CI_Model {
	public function __construct() {
		parent::__construct();
    }
	//cond field name load model
	public function viewdropdownloadmodel() {
		$i=0;
		$ids = $_GET['ids'];
		$this->db->select('viewcreationcolumnid,viewcreationcolmodeljointable,viewcreationcolmodelindexname,viewcreationcolumnname,uitypeid,moduletabsectionid,viewcreationparenttable');
		$this->db->from('viewcreationcolumns');
		$this->db->where('viewcreationcolumns.viewcreationmoduleid',$ids);
		$this->db->where('status',1);
		$result= $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('jointable'=>$row->viewcreationcolmodeljointable,'indexname'=>$row->viewcreationcolmodelindexname,'datasid'=>$row->viewcreationcolumnid,'dataname'=>$row->viewcreationcolumnname,'uitype'=>$row->uitypeid,'modtabsec'=>$row->moduletabsectionid,'parenttable'=>$row->viewcreationparenttable);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//default view fetch based on module
	public function defaultviewfetchmodel($mid){
		$data = "";
		$this->db->select('viewcreationid');
		$this->db->from('viewcreation');
		$this->db->where('viewcreation.viewcreationmoduleid',$mid);
		$this->db->where('viewcreation.createuserid',2);
		$this->db->where('viewcreation.lastupdateuserid',2);
		$this->db->where('viewcreation.viewdefault','Yes');
		$this->db->where('viewcreation.status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row){
				$data = $row->viewcreationid;
			}
		} else {
			$this->db->select('viewcreationid');
			$this->db->from('viewcreation');
			$this->db->where('viewcreation.viewcreationmoduleid',$mid);
			$this->db->where('viewcreation.status',1);
			$result = $this->db->get();
			if($result->num_rows() >0) {
				foreach($result->result() as $row){
					$data = $row->viewcreationid;
				}
			}
		}
		return $data;
	}
	//parent table name fetch
	public function parenttablegetmodel(){
		$mid = $_GET['moduleid'];
		$this->db->select('parenttable');
		$this->db->from('modulefield');
		$this->db->where('modulefield.moduletabid',$mid);
		$result = $this->db->get();
		if($result->num_rows() >0){
			foreach($result->result() as $row){
				$data = $row->parenttable;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//fetch grid data information
    public function viewdynamicdatainfofetch($sidx,$sord,$start,$limit,$wh,$creationid,$rowid,$viewcolmoduleids) {
		$whereString="";
		$groupid="";
		$extracolinfo = array();
		$multiarray = array();
		$j=0;
		$count = $_GET['cricount'];
		$griddata = $_GET['griddata'];
		$formdata=json_decode($griddata,true);
		for( $i=0; $i<$count;$i++ ) {
			$extracolinfo[$j] = $formdata[$i]['fieldid'];
			$multiarray[$j]=array(
				'0'=>$formdata[$i]['jointable'],
				'1'=>$formdata[$i]['indexname'],
				'2'=>$formdata[$i]['massconditionid'],
				'3'=>$formdata[$i]['critreiavalue'],
				'4'=>$formdata[$i]['massandorcondid']
			);
			$j++;
		}
		/* grid column title information fetch */
		if($extracolinfo == "") {
			$colinfo = $this->Basefunctions->gridinformationfetchmodel($creationid);
		} else {
			$colinfo = $this->extragridinformationfetchmodel($creationid,$extracolinfo);
		}
		 //main table
		$maintable = $_GET['maintabinfo'];
		$selvalue = $maintable.'.'.$rowid;
		$in = 0;
		//generate joins
		$counttable=0;
		if(count($colinfo)>0) {
			$counttable=count($colinfo['coltablename']);
		}
		$jtab = array($maintable);
		$ptab = array($maintable);
		$joinq = "";
		//attribute where condition
		$attrcond = "";
		$attrcondarr = array();
		$x = 0;
		$con = "";
		for($k=0;$k<$counttable;$k++) {
			if($x != 0) { $con = "AND"; }
			//parent join check
			$ptabjoin = "";
			$ptabjoin = $colinfo['colmodelparenttable'][$k];
			if(in_array($ptabjoin,$ptab)) {
				if(!in_array($colinfo['coltablename'][$k],$jtab) && $colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
					array_push($jtab,trim($colinfo['coltablename'][$k]));
					//employee
					$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid LIKE "%1%"';
					//group
					$joinq .= ' LEFT OUTER JOIN employeegroup ON FIND_IN_SET(employeegroup.employeegroupid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid  LIKE "%2%"';
					//user role and sub ordinate
					$joinq .= ' LEFT OUTER JOIN userrole ON FIND_IN_SET(userrole.userroleid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND ('.$maintable.'.employeetypeid LIKE "%3%" OR '.$maintable.'.employeetypeid LIKE "%4%")';
				} else {
					if($colinfo['colmodeldatatype'][$k] == 1) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
							}
						}
					} else if($colinfo['colmodeldatatype'][$k] == 2) {
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else if($colinfo['colmodeldatatype'][$k] == 3) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0';
							}
						}
					}
				}
			} else {
				if($colinfo['colmodeldatatype'][$k] == 1) {
					$result = $this->Basefunctions->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
							}
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$rowid.','.$maintable.'.'.$rowid.') > 0';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 1) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
									}
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 2) {
					$joinresult = $this->Basefunctions->parentjointableinfo($ptabjoin,$viewcolmoduleids);
					if($joinresult->num_rows() >0) {
						foreach($joinresult->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
							//condition name
							$condcolname = $show->viewcreationcolmodelcondname;
							//condition value
							$condcolvalue = $show->viewcreationcolmodelcondvalue;
						}
						//restrict repeated parent join
						if(in_array($destab,$ptab)) {
							if($destab != $soucrcetab) {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0';
								}
							} else {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0';
								}
							}
						}
						//parent table condition
						if($condcolname != "" && $condcolvalue != "") {
							$attrcond .= " ".$con." ".$srcjointab.'.'.$condcolname ."=". $condcolvalue;
							array_push($attrcondarr,$srcjointab.'.'.$joincolname);
							$x = 1;
						}
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] = $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$rowid.'='.$maintable.'.'.$rowid.') > 0';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 2) {
								//attribute join
								if(!in_array($colinfo['coltablename'][$k],$jtab)) {
									array_push($jtab,trim($colinfo['coltablename'][$k]));
									$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0';
								}
								//condition
								if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
									$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
									array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
									$x = 1;
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 3) {
					$result = $this->Basefunctions->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0';
							}
						}
					}  else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$rowid.','.$maintable.'.'.$rowid.') > 0';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 3) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0';
									}
								}
							}
						}
					}
				}
			}
			$dduitypeids = array('17','18','19','23','25','26','27','28','29');
			//fields fetch
			if($colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->Basefunctions->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].', GROUP_CONCAT(DISTINCT userrole.userrolename) AS rolename, GROUP_CONCAT(DISTINCT employeegroup.employeegroupname) AS empgrpname';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k].',userrole.userrolename AS rolename,employeegroup.employeegroupname AS empgrpname';
				}
			} else if( in_array($colinfo['colmodeluitype'][$k],$dduitypeids) ) {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->Basefunctions->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].'';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
				}
			} else {
				$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
			}
		}
		//attr condition generate
		for($xi=0; $xi < ( count($attrcondarr)-1 ); $xi++ ) {
			$attrcond .= " AND ".$attrcondarr[0].'='.$attrcondarr[$xi+1];
		}
		if( $attrcond == "" ) { $attrcond = "1=1";}
		
		//custom condition generate
		$cuscondition = " AND 1=1";
		if( isset($_GET['cuscondfieldsname']) ) {
			$fieldsname = explode(',',$_GET['cuscondfieldsname']);
			$condvalues = explode(',',$_GET['cuscondvalues']);
			$condtables = explode(',',$_GET['cuscondtablenames']);
			$condjoinid = explode(',',$_GET['cusjointableid']);
			$m=0;
			foreach($fieldsname AS $values) {
				if($condvalues[$m] != "") {
					if(in_array($condtables[$m],$ptab)) {
						$tabname = explode('.',$values);
						//restrict repeated join
						if(!in_array($condtables[$m],$jtab)) {
							array_push($jtab,$condtables[$m]);
							$joinq=$joinq.' LEFT OUTER JOIN '.$condtables[$m].' ON '.$tabname[0].'.'.$condjoinid[$m].'='.$maintable.'.'.$condjoinid[$m];
						}
					}
					$cuscondition .= " AND ".$values."=".$condvalues[$m];
					$m++;
				}
			}
		}
		//generate condition
		$w = '1=1';
		if($wh == "1") {
			$wh = $w;
		}
		$whereString="";
		$d = count($multiarray);
		if($d>0) {
			$extrawhereString=$this->whereclausegeneration($multiarray,$extracolinfo);
		} else {
			$extrawhereString="";
		}
		if($maintable == 'employee') {
			$status = $maintable.'.status NOT IN (3)';
		} else {
			$status = $maintable.'.status IN (1)';
		}
		$status .= ' AND '.$maintable.'.industryid IN ('.$this->Basefunctions->industryid.')';
		$li= 'LIMIT '.$start.','.$limit;
		$actsts = $this->Basefunctions->activestatus;
		/* query statements */
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$selvalue.' from '.$maintable.' '.$joinq.' WHERE '.$attrcond.' AND '.$wh.''.$whereString.''.$extrawhereString.' '.$cuscondition.' AND '.$status.' GROUP By '.$maintable.'.'.$maintable.'id ORDER BY'.' '.$sidx.' '.$sord.' '.$li);
		$finalresult=array($colinfo,$data);
		return $finalresult;
    }
    //where condition generation
    public function whereclausegeneration($conditionvalarray,$extracolinfo) {
    	$whereString="";
    	$m=0;
    	$count = count($conditionvalarray);
    	$braces = '';
    	foreach ($conditionvalarray as $key) {
    		$uitype = $this->Basefunctions->generalinformaion('viewcreationcolumns','uitypeid','viewcreationcolumnid',$extracolinfo[$m]);
    		if($uitype == 8) {
    			$key[3] =  $this->Basefunctions->ymddateconversion($key[3]);
    		} else {
    			$key[3] = $key[3];
    		}
    		if($m == 0) {
    			$key[4] = "";
    			$whereString .= " AND ".str_repeat("(",$count-1)."";
    		} else {
    			if($key[4] != 'AND' and $key[4] != 'OR') {
    				$key[4] = 'AND';
    			}
    		}
    		if($m >= 1){
    			$braces = ')';
    		}
    		$whereString.=$this->conditiongenerate($key,$braces);
    		$m++;
    	}
    	if($whereString !="" ) {
    		$whereString .= "";
    	}
    	return $whereString;
    }
    //condition generation
    public function conditiongenerate($key,$braces) {
    	switch($key[2]) {
    		case "Equalto":
    			return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." = '".$key[3]."'".$braces;
    			break;
    		case "NotEqual":
    			return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." != '".$key[3]."'".$braces;
    			break;
    		case "Startwith":
    			return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." LIKE '".$key[3].'%'."'".$braces;
    			break;
    		case "Endwith":
    			return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." LIKE '".'%'.$key[3]."'".$braces;
    			break;
    		case "Middle":
    			return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." LIKE '".'%'.$key[3].'%'."'".$braces;
    			break;
    		case "IN":
    			$whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." IN '".$key[3]."'".$braces;
    			break;
    		case "NOT IN":
    			return $whereString.=" ".$key[4]." ".$key[0].'.'.$key[1]." NOT IN '".$key[3]."'".$braces;
    			break;
    		case "GreaterThan":
    			return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." > '" .$key[3]."'".$braces;
    			break;
    		case "LessThan":
    			return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." < '" .$key[3]."'".$braces;
    			break;
    		case "GreaterThanEqual":
    			return $whereString= " ".$key[4]." ".$key[0].'.'.$key[1]." >= '" .$key[3]."'".$braces;
    			break;
    		case "LessThanEqual":
    			return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." <= '" .$key[3]."'".$braces;
    			break;
    		default:
    			return $whereString="";
    			break;
    	}
    }
	//fetch colname & colmodel information fetch model
	public function extragridinformationfetchmodel($creationid,$extracolinfo) {
		//fetch show col ids
		$this->db->select('viewcreation.viewcreationcolumnid,viewcreation.viewcreationid,viewcreationmodule.viewcreationmoduleid,viewcreationmodule.viewcreationmodulename,viewcreation.viewcolumnsize',false);
		$this->db->from('viewcreation');
		$this->db->join('viewcreationmodule','viewcreationmodule.viewcreationmoduleid=viewcreation.viewcreationmoduleid');
		$this->db->where_in('viewcreation.viewcreationid',$creationid);
		$this->db->where('viewcreation.status',1);
		$result=$this->db->get()->result();
		foreach($result as $row) {
			$colids=$row->viewcreationcolumnid;
			$modname = $row->viewcreationmodulename;
			$modid = $row->viewcreationmoduleid;
			$colsize = $row->viewcolumnsize;
		}
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$headcolids = $this->Basefunctions->mobiledatarowheaderidfetch($creationid);
			$vwcolids = explode(',',$colids);
			$vwmgcolids = array_merge($vwcolids,$headcolids);
			$viewcolids = array_unique($vwmgcolids);
		} else {
			$viewcolids = explode(',',$colids);
		}
		/* for fetch colname & colmodel fetch */
		$i=0;
		$m=0;
		$data = array();
		$colsizes = explode(',',$colsize);
		foreach($viewcolids as $colid) {
			$this->db->select("viewcreationcolumns.viewcreationcolumnid,
								viewcreationcolumns.viewcreationcolumnname,
								viewcreationcolumns.viewcreationcolmodelname,
								viewcreationcolumns.viewcreationcolmodelindexname,
								viewcreationcolumns.viewcreationcolmodeljointable,
								viewcreationcolumns.viewcreationcolumnid,
								viewcreationcolumns.viewcreationcolmodelaliasname,
								viewcreationcolumns.viewcreationcolmodeltable,
								viewcreationcolumns.viewcreationparenttable,
								viewcreationcolumns.viewcreationmoduleid,
								viewcreationcolumns.viewcreationjoincolmodelname,
								viewcreationcolumns.viewcreationcolumnviewtype,
								viewcreationcolumns.viewcreationtype,
								viewcreationcolumns.viewcreationcolmodelcondname,
								viewcreationcolumns.uitypeid,
								viewcreationcolumns.moduletabsectionid,
								viewcreationcolumns.viewcreationcolmodelcondvalue,
								viewcreationcolumns.viewcreationcolumnicon");
			$this->db->from('viewcreationcolumns');
			$this->db->where('viewcreationcolumns.viewcreationcolumnid',$colid);
			$this->db->where('viewcreationcolumns.status',1);
			$showfield = $this->db->get();
			if($showfield->num_rows() >0) {
				foreach($showfield->result() as $show) {
					$data['colid'][$i]=$show->viewcreationcolumnid;
					$data['colname'][$i]=$show->viewcreationcolumnname;
					$data['colicon'][$i]=$show->viewcreationcolumnicon;
					$data['colsize'][$i]=$colsizes[$m];
					$data['colmodelname'][$i]=$show->viewcreationcolmodelname;
					$data['colmodelaliasname'][$i]=$show->viewcreationcolmodelaliasname;
					$data['coltablename'][$i]=$show->viewcreationcolmodeltable;
					$data['coljointablename'][$i]=$show->viewcreationcolmodeljointable;
					$data['coljoinmodelname'][$i]=$show->viewcreationjoincolmodelname;
					$data['colmodelindex'][$i]=$show->viewcreationcolmodeljointable .'.'.$show->viewcreationcolmodelindexname;
					$data['colmodelparenttable'][$i]=$show->viewcreationparenttable;
					$data['colmodelviewtype'][$i]=$show->viewcreationcolumnviewtype;
					$data['colmodeldatatype'][$i]=$show->viewcreationtype;
					$data['colmodelcondname'][$i]=$show->viewcreationcolmodelcondname;
					$data['colmodelcondvalue'][$i]=$show->viewcreationcolmodelcondvalue;
					$data['colmodelmoduleid'][$i]=$show->viewcreationmoduleid;
					$data['colmodeluitype'][$i]=$show->uitypeid;
					$data['colmodelsectid'][$i]=$show->moduletabsectionid;
					$data['colmodeltype'][$i]='text';
					$data['modname'][$i] = $modname;
					$i++;
				}
			}
			$m++;
		}
		return $data;
    }
	//field name based drop down value get
	public function fieldviewnamebespicklistdddvaluemodel(){
		$i=0;
		$moduleid = 'moduleid';
		$mid = $_GET['moduleid'];
		$industryid = $this->Basefunctions->industryid;
		$fieldname = $_GET['fieldname'];
		$table = substr($fieldname, 0, -4);
		$fieldid = $table.'id';
		$this->db->select($fieldid.','.$fieldname);
		$this->db->from($table);
		$this->db->where("FIND_IN_SET($mid,$table."."$moduleid) >", 0);
		$this->db->where("FIND_IN_SET($industryid,industryid) >", 0);
		$this->db->where('status',1);
		$reult =$this->db->get();
		if($reult->num_rows() > 0){
			foreach($reult->result() as $row){
				$data[$i] = array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//field name based drop down value get
	public function viewfieldnamebesdddvaluemodel(){
		$i=0;
		$fieldname = $_GET['fieldname'];
		$industryid = $this->Basefunctions->industryid;
		$table = substr($fieldname, 0, -4);
		$fieldid = $table.'id';
		$this->db->select($fieldid.','.$fieldname);
		$this->db->from($table);
		$this->db->where('industryid',$industryid);
		$this->db->where('status',1);
		$reult =$this->db->get();
		if($reult->num_rows() > 0){
			foreach($reult->result() as $row){
				$data[$i] = array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//check box value get
	public function checkboxvaluegetmodel() {
		$checkdata = 'Yes,No';
		$checkid = '1,2';
		$cdata = explode(',',$checkdata);
		$cid = explode(',',$checkid);
		for($i=0;$i<count($cdata);$i++){
			$data[$i] = array('datasid'=>$cid[$i],'dataname'=>$cdata[$i]);
		}
		echo json_encode($data);
	}
	//filter values
	public function filtervalue($fields) {
		$fieldsinfo = array_unique( $fields ) ;
		$fieldname = implode(',',$fieldsinfo);
		return $fieldname;
	}
	//default value set array for add
	public function defaultvalueget() {
		$defdata['createdate'] = date($this->Basefunctions->datef);
		$defdata['lastupdatedate'] = date($this->Basefunctions->datef);
		$defdata['createuserid'] = $this->Basefunctions->userid;
		$defdata['lastupdateuserid'] = $this->Basefunctions->userid;
		$defdata['status'] = 1;
		return $defdata;
	}
	//default value set array for add
	public function defaultupdatevalueget() {
		$defdata['createuserid'] = $this->Basefunctions->userid;
		$defdata['lastupdateuserid'] = $this->Basefunctions->userid;
		$defdata['status'] = 1;
		return $defdata;
	}
	//primary key infromation fetch
	public function primaryinfo($table) {
		$primarydata = $this->db->query("SHOW KEYS FROM ".$table." WHERE Key_name = 'PRIMARY'")->result();
		foreach($primarydata as $key) {
			$primarykey = $key->Column_name;
		}
		return $primarykey;
	}
	//fetch table name based on moduleid
	public function tableinfofetch($moduleid) {
		$tabname = array();
		$this->db->select('tablename');
		$this->db->from('modulefield');
		$this->db->where('modulefield.moduletabid',$moduleid);
		$result = $this->db->get();
		foreach($result->result() as $rowdata) {
			if($rowdata->tablename != "") {
				$tabname[] = $rowdata->tablename;
			}
		}
		return $tabname;
	}
	//table field name check
	public function tablefieldnamecheck($tblname,$fieldname) {
		$result = 'false';
		$fields = $this->db->field_data($tblname);
		foreach ($fields as $field) {
			$name =  $field->name;
			if($fieldname == $name) {
				$result = 'true';
			}
		}
		return $result;
	}
	//duplicate record delete
	public function duplicaterecdeletemodel($moduleid,$parenttable,$ids) {
		$parentid = $parenttable.'id';
		$userid = $this->Basefunctions->userid;
		$cdate = date($this->Basefunctions->datef);
		$tablename = $this->tableinfofetch($moduleid);
		foreach($tablename as $table) {
			$fieldnamecheck = $this->tablefieldnamecheck($table,$parentid);
			if($fieldnamecheck == 'true') {
				$this->db->query("UPDATE"." ".$table." SET "."status = 0,lastupdatedate='".$cdate."',lastupdateuserid=".$userid." WHERE ".$table.".".$parentid." "."IN "."(".$ids.")" );
			}
		}
		return "TRUE";
	}
	//internal filed label fetch
	public function datacolumninformationfetchmodel($moduleid) {
		$i=0;
		$data = array();
		$this->db->select("moduletabid,columnname,tablename,uitypeid,fieldlabel,parenttable");
		$this->db->from('modulefield');
		$this->db->join('module','module.moduleid=modulefield.moduletabid');
		$this->db->where_in('moduletabid',$moduleid);
		$this->db->where('tablename != ""');
		$this->db->where('active = "Yes"');
		$this->db->where_not_in('modulefield.status',array(0,3,4));
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result()as $row) {
				$data[$i]=array('moduletabid'=>$row->moduletabid,'columnname'=>$row->columnname,'tablename'=>$row->tablename,'uitypeid'=>$row->uitypeid,'internalcolumnname'=>$row->fieldlabel,'parenttable'=>$row->parenttable);
				$i++;
			}
		}
		return $data;
	}
	//drop down value fetch
	public function dropdownvaluefetch($tablefieldid,$value,$partable,$moduleid) {
		$name=array();
		$tablename = substr($tablefieldid,0,-2);
		$elname = array('parentcategoryid','parentstoragecategoryid','parentaccountid');
		$fname = array('categoryname','storagecategoryname','accountname');
		$fnameid = array('categoryid','storagecategoryid','accountid');
		$tname = array('category','storagecategory','account');
		if(in_array($tablefieldid,$elname)) {
			$key = array_search($tablefieldid,$elname);
			$tablename = $tname[$key];
			$tabfieldname = $fname[$key];
			$result = $this->db->select("$tname[$key].$fname[$key],$tname[$key].$fnameid[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$value)->where($tname[$key].'.status',1)->get();
		} else {
			$val = explode(',',$value);
			$fieldinfo = $this->fetchfieldparenttable($partable,$tablefieldid);
			//fetch original field of picklist data in view creation 
			$viewfieldinfo = $this->viewfieldsinformation($partable,$tablefieldid,$fieldinfo['modid']);
			$tabfieldname = ( (count($viewfieldinfo)>0)? $viewfieldinfo['joinfieldname'] : $tablefieldid );
			//fetch picklist datas
			$tablename = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
			$tabfieldname = $tablename.'name';
			$tablefieldid = $tablename.'id';
			$result = $this->db->select("$tablefieldid,$tabfieldname")->from($tablename)->where_in($tablename.'.'.$tablefieldid,$val)->where($tablename.'.status',1)->get();
		}
		if($result->num_rows() > 0) {
			foreach($result->result()as $row) {
				$name[] = $row->$tabfieldname;
			}
		}
		$datasets = implode(',',$name);
		return $datasets;
	}
	//fetch parent table information
	public function fetchfieldparenttable($tablname,$fieldname) {
		$pardata = array();
		$datas = $this->db->query('SELECT modulefieldid,uitypeid,parenttable,moduletabid  FROM modulefield WHERE columnname LIKE "%'.$fieldname.'%" AND tablename LIKE "%'.$tablname.'%" AND status=1',false);
		foreach($datas->result() as $data) {
			$pardata = array('uitype'=>$data->uitypeid,'partab'=>$data->parenttable,'modid'=>$data->moduletabid);
		}
		return $pardata;
	}
	//fetch relation and view type 3 field information
	public function viewfieldsinformation($tablename,$tabfield,$modid) {
		$datasets = array();
		$viewfieldinfo = $this->db->query('SELECT viewcreationcolumnid,viewcreationparenttable,viewcreationjoincolmodelname,viewcreationcolmodelcondname FROM viewcreationcolumns WHERE viewcreationparenttable LIKE "%'.$tablename.'%" AND viewcreationcolmodelcondname LIKE "%'.$tabfield.'%" AND viewcreationtype=3 AND viewcreationmoduleid='.$modid.' AND status=1');
		foreach($viewfieldinfo->result() as $viewdata) {
			$datasets = array('viewcolid'=>$viewdata->viewcreationcolumnid,'joinfieldname'=>$viewdata->viewcreationjoincolmodelname,'condfiledname'=>$viewdata->viewcreationcolmodelcondname);
		}
		return $datasets;
	}
	//look up table value check
	public function checklookuptabdata($columnname,$colvalue,$uitypeid,$moduleid) {
		$id = 1;
		if( !is_numeric($colvalue) ) {
			//parameters
			$tablename = substr($columnname,0,-2);
			$tabid = $tablename.'id';
			$tabfieldname = $tablename.'name';
			//check datas availability otherwise insert data.
			if($uitypeid != 21 && $uitypeid != 26 && $uitypeid != 27) {
				$filedchk = $this->tablefieldnamecheck($tablename,'moduleid');
				if($filedchk == 'false') {
					$result = $this->db->select("$tabfieldname,$tabid")->from($tablename)->where($tablename.'.'.$tabfieldname,$colvalue)->where($tablename.'.status',1)->get();
				} else {
					$result = $this->db->select("$tabfieldname,$tabid")->from($tablename)->where($tablename.'.'.$tabfieldname,$colvalue)->where($tablename.'.status',1)->where($tablename.'.moduleid',$moduleid)->get();
				}
				if($result->num_rows() > 0) {
					foreach($result->result()as $row) {
						$id=$row->$tabid;
						return $id;
					}
				} else {
					if($uitypeid == 17 || $uitypeid == 25 || $uitypeid == 18) {
						$data = array($tabfieldname=>$colvalue,'userroleid'=>$this->Basefunctions->userroleid);
					} else {
						$data = array($tabfieldname=>$colvalue);
					}
					//default value get
					$defdataarr = $this->defaultvalueget();
					$newdata = array_merge($data,$defdataarr);
					//data insertion
					$this->db->insert( $tablename, array_filter($newdata) );
					$id = $this->db->insert_id();
					return $id;
				}
			} else if($uitypeid == 26) {
				//fetch relational field conditions
				$conddata = $this->fetchmodulefiledrelationinfo($columnname,$moduleid);
				$id = (($conddata!='')?$this->fetchrelationfieldvalueid($conddata,$moduleid,$colvalue):1);
				return $id;
			} else if($uitypeid == 27) {
				$qy=$this->db->select('moduleid')->from('module')->where('modulename',$colvalue)->or_where('menuname',$colvalue)->get();
				if($qy->num_rows()>=1) {
					foreach($qy->result() as $data) {
						$id=$data->moduleid;
					}
				} else {
					$id=1;
				}
				return $id;
			} else {
				$id = 0;
				$elname = array('parentcategoryid','parentstoragecategoryid','parentaccountid');
				$fname = array('categoryname','storagecategoryname','accountname');
				$fnameid = array('categoryid','storagecategoryid','accountid');
				$tname = array('category','storagecategory','account');
				$key = array_search($columnname,$elname);
				if($key != NULL) {
					$result = $this->db->select("$fname[$key],$fnameid[$key]")->from($tname[$key])->where($tname[$key].'.'.$fname[$key],$colvalue)->where($tname[$key].'.status',1)->get();
					if($result->num_rows() > 0) {
						foreach($result->result()as $row) {
							$id=$row->$fnameid[$key];
						}
					}
				}
				return $id;
			}
		} else {
			if($uitypeid == 21) {
				$id = 0;
				return $id;
			} else {
				return $id;
			}
		}
	}
	//fetch relational field information
	public function fetchmodulefiledrelationinfo($columnname,$moduleid) {
		$datasets = "";
		$datas = $this->db->select('modulefieldid,ddparenttable')->from('modulefield')->where('moduletabid',$moduleid)->where('columnname',$columnname)->get();
		if($datas->num_rows()>=1) {
			foreach($datas->result() as $data) {
				$datasets = $data->ddparenttable;
			}
		}
		return $datasets;
	}
	//fetch relational field values
	public function fetchrelationfieldvalueid($conddata,$modid,$colvalue) {
		$condinfo = explode('|',$conddata);
		$join="";
		$partable =  $condinfo[0];
		$tabfieldname = $condinfo[1];
		$tabname = $condinfo[2];
		$uitypeid = $condinfo[3];
		$moduleid = ( ($modid!='')? $modid : 1 );
		$id=1;
		//check the filed values
		if( $partable != '' && $tabfieldname != '' && $tabname != '' && $uitypeid != '' ) {
			if($uitypeid == 17 || $uitypeid == 18 || $uitypeid == 19 || $uitypeid == 20 || $uitypeid == 25 || $uitypeid == 26 || $uitypeid == 28 || $uitypeid == 29) {
				$ftablename = substr($tabfieldname,0,-2);
				$ftabfildname = $ftablename.'name';
				$ftabid = $ftablename.'id';
				$tabname = $ftablename;
				if($ftablename!=$tabname) {
					$join = $join.' LEFT OUTER JOIN '.$tabname.' ON '.$tabname.'.'.$ftabid.'='.$ftablename.'.'.$ftabid;
				}
				$mnamechk = $this->Basefunctions->tablefieldnamecheck($ftablename,'moduleid');
				$modcond = ( ($mnamechk == "true") ? 'AND '.$ftablename.'.moduleid='.$moduleid : '');
				$query = $this->db->query('select '.$ftablename.'.'.$ftabid.' AS Id'.','.$ftablename.'.'.$ftabfildname.' AS Name from '.$ftablename.''.$join.' where '.$ftablename.'.'.$ftabfildname.'="'.$colvalue.'" '.$modcond.' AND '.$ftablename.'.status NOT IN (0,3) ORDER BY '.$ftablename.'.'.$ftabid.'');
				$count = $query->num_rows();
				if($count>=1) {
					foreach($query->result() as $data) {
						$id = $data->Id;
					}
				}
			} else {
				$mnamechk = $this->Basefunctions->tablefieldnamecheck($ftablename,'moduleid');
				$modcond = ( ($mnamechk == "true") ? 'AND '.$ftablename.'.moduleid='.$moduleid : '');
				$tabid = $tabname.'id';
				$query = $this->db->query( 'select '.$tabname.'.'.$tabid.' AS Id'.','.$tabname.'.'.$tabfieldname.' AS Name from '.$tabname.' where '.$ftablename.'.'.$ftabfildname.'="'.$colvalue.'" '.$modcond.' AND '.$tabname.'.status NOT IN (0,3) ORDER BY '.$tabname.'.'.$tabid.'');
				$count = $query->num_rows();
				if($count>=1) {
					foreach($query->result() as $data) {
						$id = $data->Id;
					}
				}
			}
		}
		return $id;
	}
	//merge data information fetch model
	public function mergedatainformationfetchmodel() {
		$moduleid = $_GET['moduleid'];
		$parenttable = $_GET['ptabinfo'];
		$parenttabid = $_GET['ptabpriminfo'];
		$rowid = $_GET['rowids'];
		$columninfo = $this->datacolumninformationfetchmodel($moduleid);
		$tablenames = array();
		$columnnames = array();
		$uitypeids = array();
		$labelnames = array();
		$parenttabnames = array();
		foreach($columninfo as $data) {
			$tablenames[] = $data['tablename'];
			$columnnames[] = $data['columnname'];
			$uitypeids[] = $data['uitypeid'];
			$labelnames[] = $data['internalcolumnname'];
			$parenttabnames[] = $data['parenttable'];
		}
		$tabname = $this->filtervalue($tablenames);
		$tabnameinfo = explode(',',$tabname);
		$pdatacolname = array();
		$addrmodid = array(4,201,202,203,216,217,226,227,230);
		$priaddrmodid = array(4,201,203);
		$billaddrmodid = array(202,216,217,226,227,230);
		$dduitypeid = array(17,18,19,20,21,23,25,26,27,28,29);
		//$m=0;
		$colsdatainfo = array();
		foreach($tabnameinfo as $table) {
			if( !in_array($table,array('termsandcondition','documents')) ) {
				$dataset = $this->db->query('select * from '.$table.' where '.$parenttabid.' IN  ('.$rowid.') AND status=1 ORDER BY '.$parenttabid.' DESC')->result();
				$m=0;
				foreach($columnnames as $colname) {
					//parent table
					if( strcmp(trim($tablenames[$m]),trim($table)) == 0 && strcmp(trim($tablenames[$m]),trim($parenttable)) == 0) {
						$i=0;
						foreach($dataset as $row) {
							if( in_array($uitypeids[$m],$dduitypeid) ) {
								$dddata = "";
								$dddata = $this->dropdownvaluefetch($colname,$row->$colname,$table,$moduleid);
								$dataresultset[$i][$labelnames[$m]] = $dddata;
							} else {
								$dataresultset[$i][$labelnames[$m]] = $row->$colname;
							}
							$i++;
						}
					} else if( strcmp(trim($tablenames[$m]),trim($table)) == 0 && strcmp(trim($tablenames[$m]),trim($parenttable)) != 0) { //child table
						if( in_array($moduleid,$addrmodid) ) {//child address table
							if( in_array($moduleid,$priaddrmodid) ) {
								$addtype = array('Primary','Secondary');
								$addtypeid = array(2,3);
							} else if( in_array($moduleid,$billaddrmodid) ) {
								$addtype = array('Billing','Shipping');
								$addtypeid = array(4,5);
							}
							//check if address table or not
							$addname = explode(' ',$labelnames[$m]);
							$addtypefiledchk = $this->tablefieldnamecheck($table,'addresstypeid');
							if($addtypefiledchk == 'true') {
								$hh=0;
								foreach($addtypeid as $type) {
										$childdataset = $this->db->query('select * from '.$table.' where '.$parenttabid.' IN ('.$rowid.') AND addresstypeid ='.$type.' AND status=1 ORDER BY '.$parenttabid.' DESC' )->result();
										$i=0;
										foreach($childdataset as $childrow) {
											if( in_array($uitypeids[$m],$dduitypeid) ) {
												$dddata = "";
												$dddata = $this->dropdownvaluefetch($colname,$childrow->$colname,$table,$moduleid);
												$dataresultset[$i][$labelnames[$m]] = $dddata;
											} else {
												$dataresultset[$i][$labelnames[$m]] = $childrow->$colname;
											}
											$i++;
										}
									$hh++;
								}
							} else {
								$i=0;
								foreach($dataset as $row) {
									if( in_array($uitypeids[$m],$dduitypeid) ) {
										$dddata = "";
										$dddata = $this->dropdownvaluefetch($colname,$row->$colname,$table,$moduleid);
										$dataresultset[$i][$labelnames[$m]] = $dddata;
									} else {
										$dataresultset[$i][$labelnames[$m]] = $row->$colname;
									}
									$i++;
								}
							}
						} else {
							$i=0;
							foreach($dataset as $row) {
								if( in_array($uitypeids[$m],$dduitypeid) ) {
									$dddata = "";
									$dddata = $this->dropdownvaluefetch($colname,$row->$colname,$table,$moduleid);
									$dataresultset[$i][$labelnames[$m]] = $dddata;
								} else {
									$dataresultset[$i][$labelnames[$m]] = $row->$colname;
								}
								$i++;
							}
						}
					}
					$m++;
				}
				$colsdatainfo = array($columninfo,$dataresultset);
			}
		}
		return $colsdatainfo;
	}
	//drop down value check
	public function filedvaluecheck($tablename,$filedname,$filedid,$filedvalue) {
		$dataset = 0;
		$result=$this->db->query(' select '.$filedid.' from '.$tablename.' WHERE '.$filedname.'="'.$filedvalue.'" ')->result();
		foreach($result as $data) {
			$dataset=$data->$filedid;
		}
		return $dataset;
	}
	//merge duplicate records
	public function mergeduplicaterecordsmodel() {
		$moduleid = $_POST['moduleid'];
		$rowids = explode(',',$_POST['ids']);
		$primaryid = $rowids[0];
		//grid datas
		$dataset = $_POST['sets'];
		$dduitypeid = array(17,18,19,20,21,23,26,27,28);
		//address module ids
		$addrmodid = array(4,201,202,203,216,217,226,227,230);
		$priaddrmodid = array(4,201,203);
		$billaddrmodid = array(202,216,217,226,227,230);
		//insert data assign
		$columninfo = $this->datacolumninformationfetchmodel($moduleid);
		$fieldname = array();
		$fieldtable = array();
		$columnname = array();
		$fieldpartable = array();
		$fielduitype = array();
		$fieldlabel = array();
		foreach($columninfo as $data) {
			$fieldtable[] = $data['tablename'];
			$columnname[] = $data['columnname'];
			$fielduitype[] = $data['uitypeid'];
			$fieldlabel[] = $data['internalcolumnname'];
			$fieldname[] = $data['internalcolumnname'];
			$fieldpartable[] = $data['parenttable'];
		}
		//grid data manipulation
		$datainformation = json_decode( $dataset, true );
		$information = array();
		$m=0;
		foreach($datainformation as $dataval) {
			$dataval = preg_replace('~A110S~','&', $dataval);
			$information[$fieldname[$m]] = $dataval;
			$m++;
		}
		//data arrange for insertion
		$parenttable = $this->filtervalue($fieldpartable);
		$tablenames = $this->filtervalue($fieldtable);
		$tableinfo = explode(',',$tablenames);
		//data merge operation
		$pdata = array();
		$pmergedata = array();
		$m=0;
		foreach($tableinfo as $tblname) {
			${'$cdata'.$m} = array();
			${'$cmerge'.$m} = array();
			$i = 0;
			foreach($fieldname as $fname) {
				if( strcmp( trim($parenttable),trim($fieldtable[$i]) ) == 0  && strcmp( trim($parenttable),trim($tblname) == 0) ) { //ptable
					if( isset( $information[$fname] ) ) {
						$name = explode('_',$fname);
						if( in_array($fielduitype[$i],$dduitypeid) ) {
							if( $information[$fname] == '' ) {
								$ddid = 1;
							} else {
								$ddid = $this->checklookuptabdata($columnname[$i],$information[$fname],$fielduitype[$i],$moduleid);
							}
							$pdata[$columnname[$i]] = $ddid;
						} else {
							if( in_array('password',$name) ) {
								$hash = password_hash($information[$fname], PASSWORD_DEFAULT);								
								$pdata[$columnname[$i]] = $hash ;
							} else {
								if( ctype_alpha($information[$fname]) ) {
									$pdata[$columnname[$i]] = ucwords($information[$fname]);
								} else {
									$pdata[$columnname[$i]] = $information[$fname];
								}
							}
						}
						
						$i++;
					}
				} else if( in_array($moduleid,$addrmodid) ) { //child address table
					if( in_array($moduleid,$priaddrmodid) ) {
						$addtype = array('Primary','Secondary');
					} else if( in_array($moduleid,$billaddrmodid) ) {
						$addtype = array('Billing','Shipping');
					}
					$h=0;
					$addtypefiledchk = $this->tablefieldnamecheck($tblname,'addresstypeid');
					if($addtypefiledchk == 'true') { //address child table
						foreach($addtype as $type) {
							$addname = explode(' ',$fieldlabel[$i]);
							if( isset($information[$fname]) && in_array($type,$addname) ) {
								$name = explode('_',$fname);
								if( in_array($fielduitype[$i],$dduitypeid) ) {
									if( $information[$fname] == '' ) {
										$ddid = 1;
									} else {
										$ddid = $this->checklookuptabdata($columnname[$i],$information[$fname],$fielduitype[$i],$moduleid);
									}
									${'$cdata'.$h}[$columnname[$i]] = $ddid;
								} else {
									if( in_array('password',$name) ) {
										$hash = password_hash($information[$fname], PASSWORD_DEFAULT);	
										${'$cdata'.$h}[$columnname[$i]] = $hash ;
									} else {
										if( ctype_alpha($information[$fname]) ) {
											${'$cdata'.$h}[$columnname[$i]] = ucwords($information[$fname]);
										} else {
											${'$cdata'.$h}[$columnname[$i]] = $information[$fname];
										}
									}
								}
							}
							$h++;
						}
					} else { //other child tables
						$addtypefiledchk = $this->tablefieldnamecheck($tblname,$columnname[$i]);
						if($addtypefiledchk == 'true') {
							if( isset( $information[$fname] ) ) {
								$name = explode('_',$fname);
								if( in_array($fielduitype[$i],$dduitypeid) ) {
									if( $information[$fname] == '' ) {
										$ddid = 1;
									} else {
										$ddid = $this->checklookuptabdata($columnname[$i],$information[$fname],$fielduitype[$i],$moduleid);
									}
									${'$cmerge'.$m}[$columnname[$i]] = $ddid;
								} else {
									if( in_array('password',$name) ) {
										$hash = password_hash($information[$fname], PASSWORD_DEFAULT);	
										${'$cmerge'.$m}[$columnname[$i]] = $hash ;
									} else {
										if( ctype_alpha($information[$fname]) ) {
											${'$cmerge'.$m}[$columnname[$i]] = ucwords($information[$fname]);
										} else {
											${'$cmerge'.$m}[$columnname[$i]] = $information[$fname];
										}
									}
								}
							}
						}
					}
					$i++;
				} else {
					if( strcmp( trim($fieldtable[$i]),trim($tblname) ) == 0 ) { //ctable
						if( isset( $information[$fname] ) ) {
							$name = explode('_',$fname);
							if( in_array($fielduitype[$i],$dduitypeid) ) {
								if( $information[$fname] == '' ) {
									$ddid = 1;
								} else {
									$ddid = $this->checklookuptabdata($columnname[$i],$information[$fname],$fielduitype[$i],$moduleid);
								}
								${'$cdata'.$m}[$columnname[$i]] = $ddid;
							} else {
								if( in_array('password',$name) ) {
									$hash = password_hash($information[$fname], PASSWORD_DEFAULT);	
									${'$cdata'.$m}[$columnname[$i]] = $hash ;
								} else {
									if( ctype_alpha($information[$fname]) ) {
										${'$cdata'.$m}[$columnname[$i]] = ucwords($information[$fname]);
									} else {
										${'$cdata'.$m}[$columnname[$i]] = $information[$fname];
									}
								}
							}
						}
						$i++;
					}
				}
			}
			$m++;
		}
		//default value get
		$defdataarr = $this->defaultvalueget();
		$defupdatedataarr = $this->defaultupdatevalueget();
		//parent table primary name
		$primaryname = $this->primaryinfo($parenttable);
		{
			//parent table update
			$newpardata = array_merge($pdata,$defupdatedataarr);
			$this->db->where($primaryname,$primaryid);
			$this->db->update($parenttable,$newpardata);
			//child table insertion
			if( in_array($moduleid,$addrmodid) ) {
				if( in_array($moduleid,$priaddrmodid) ) {
					$addtype = array('Primary','Secondary');
					$addtypeid = array(2,3);
				} else if( in_array($moduleid,$billaddrmodid) ) {
					$addtype = array('Billing','Shipping');
					$addtypeid = array(4,5);
				}
				foreach( $tableinfo as $tblname ) { //address tables
					$h=0;
					foreach($addtype as $type) {
						$cnewdata = array();
						if(count(${'$cdata'.$h}) > 0 ) {
							if( strcmp( trim($parenttable),trim($tblname) ) != 0 ) {
								$addtypefiledchk = $this->tablefieldnamecheck($tblname,'addresstypeid');
								if($addtypefiledchk == 'true') {
									$chk = $this->filedvaluecheck($tblname,$primaryname,$primaryname,$primaryid);
									if($chk!=0) {
										${'$cdata'.$h}['addresstypeid']=$addtypeid[$h];
										${'$cdata'.$h}[$primaryname]=$primaryid;
										$cnewdata = array_merge(${'$cdata'.$h},$defdataarr);
										//data insertion
										$this->db->insert( $tblname, array_filter($cnewdata) );
									} else {
										${'$cdata'.$h}['addresstypeid']=$addtypeid[$h];
										$cnewdata = array_merge(${'$cdata'.$h},$defupdatedataarr);
										//update information
										$this->db->where($primaryname,$primaryid);
										$this->db->update($tblname,$cnewdata);
									}
									unset(${'$cdata'.$h}['addresstypeid']);
								}
							}
						}
						$h++;
					}
				}
				$m=0;
				foreach( $tableinfo as $tblname ) { //other child tables
					$cnewdata = array();
					if(count(${'$cmerge'.$m}) > 0 ) {
						if( strcmp( trim($parenttable),trim($tblname) ) != 0 ) {
							$chk = $this->filedvaluecheck($tblname,$primaryname,$primaryname,$primaryid);
							if($chk!=0) {
								${'$cmerge'.$m}[$primaryname]=$primaryid;
								$cnewdata = array_merge(${'$cmerge'.$m},$defdataarr);
								//data insertion
								$this->db->insert( $tblname, array_filter($cnewdata) );
							} else {
								$cnewdata = array_merge(${'$cmerge'.$m},$defupdatedataarr);
								//update information
								$this->db->where($primaryname,$primaryid);
								$this->db->update($tblname,$cnewdata);
							}
						}
					}
					$m++;
				}
			} else {
				$m=0;
				foreach( $tableinfo as $tblname ) {
					$cnewdata = array();
					if(count(${'$cdata'.$m}) > 0 ) {
						if( strcmp( trim($parenttable),trim($tblname) ) != 0 ) {
							$chk = $this->filedvaluecheck($tblname,$primaryname,$primaryname,$primaryid);
							if($chk!=0) {
								${'$cdata'.$m}[$primaryname]=$primaryid;
								$cnewdata = array_merge(${'$cdata'.$m},$defdataarr);
								//data insertion
								$this->db->insert( $tblname, array_filter($cnewdata) );
							} else {
								$cnewdata = array_merge(${'$cdata'.$m},$defupdatedataarr);
								//update information
								$this->db->where($primaryname,$primaryid);
								$this->db->update($tblname,$cnewdata);
							}
						}
					}
					$m++;
				}
			}
		}
		//delete remaining value
		$m=0;
		$datarowid = array();
		foreach($rowids as $colids) {
			if($m!=0) {
				$datarowid[]=$colids;
			}
			$m++;
		}
		$ids = implode(',',$datarowid);
		$this->duplicaterecdeletemodel($moduleid,$parenttable,$ids);
		//notification trigger in Find and Merge
		$empid = $this->Basefunctions->logemployeeid;
		$modulename = $this->Basefunctions->generalinformaion('module','modulename','moduleid',$moduleid);
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		$notimsg = $empname." Merged Duplicates for a".$modulename." Records using Find and Merge";
		$this->Basefunctions->notificationcontentadd(1,'Duplicate',$notimsg,1,$moduleid);
		echo "TRUE";
	}
}
?>