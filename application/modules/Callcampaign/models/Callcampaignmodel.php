<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Model File
class CallCampaignmodel extends CI_Model{    
    public function __construct() {
		parent::__construct(); 
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//default view fetch based on module
	public function defaultviewfetchmodel($mid){
		$data = "";
		$this->db->select('viewcreationid');
		$this->db->from('viewcreation');
		$this->db->where('viewcreation.viewcreationmoduleid',$mid);
		$this->db->where('viewcreation.createuserid',1);
		$this->db->where('viewcreation.lastupdateuserid',1);
		$this->db->where('viewcreation.status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row){
				$data = $row->viewcreationid;
			}
		}
		return $data;
	}
	//parent table name fetch
	public function parenttablegetmodel() {
		$mid = $_GET['moduleid'];
		$this->db->select('parenttable');
		$this->db->from('modulefield');
		$this->db->where('modulefield.moduletabid',$mid);
		$result = $this->db->get();
		if($result->num_rows() >0){
			foreach($result->result() as $row){
				$data = $row->parenttable;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	public function callsubscribervaluegetgridxmlview($tablename,$sortcol,$sortord,$pagenum,$rowscount,$listid,$segmentid,$segsubscriberid,$autoupdate) {
		$industryid = $this->Basefunctions->industryid;
		if($tablename == 'employee') {
			$status = $tablename.'.status NOT IN (3)';
		} else {
			$status = $tablename.'.status IN (1)';
		}
		if($listid != '') {
			$maillistid = 'subscribers.campaigngroupsid='.$listid.' AND ';
		}else {
			$maillistid = 'subscribers.campaigngroupsid=0 AND ';
		}
		if($autoupdate != 'Yes') {
			if($segsubscriberid != '') {
				$segsubscriberid = $segsubscriberid;
				$segmentsubscribercondition = 'AND subscribers.subscribersid IN ('.$segsubscriberid.')';
			} else {
				$segmentsubscribercondition = 'AND 1=1';
			}
		} else {
			$segmentsubscribercondition = 'AND 1=1';
		}
		$dataset = 'subscribers.subscribersid,campaigngroups.campaigngroupsname,subscribers.firstname,subscribers.lastname,subscribers.mobilenumber,subscribers.permissiongranted';
		$join =' LEFT OUTER JOIN campaigngroups ON campaigngroups.campaigngroupsid=subscribers.campaigngroupsid';
		$where = '1=1';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		$industry = $tablename.'.industryid IN ('.$industryid.')';
		//query
		$data = $this->db->query('select '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$where.' '.$segmentsubscribercondition.' AND '.$maillistid.' '.$status.' AND '.$industry.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		return $data;
	}
	//segment based subscriber id get
	public function relatedsubscriberidgetmodel() {
		$segmentid = $_POST['segmentid'];
		$data= '';
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('subscribersid,autoupdate');
		$this->db->from('segments');
		$this->db->where('segments.segmentsid',$segmentid);
		$this->db->where("FIND_IN_SET('$industryid',segments.industryid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data = array('subscriberid'=>$row->subscribersid,'autoupdate'=>$row->autoupdate);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//mobile number get
	public function mobilenumbergetmodel() {
		$subscriberid = $_POST['subscriberid'];
		$exp = explode(',',$subscriberid);
		$data= '';
		$industryid = $this->Basefunctions->industryid;
		for($i=0;$i<count($exp);$i++) {
			$this->db->select('subscribersid,mobilenumber');
			$this->db->from('subscribers');
			$this->db->where_in('subscribers.subscribersid',array($exp[$i]));
			$this->db->where("FIND_IN_SET('$industryid',subscribers.industryid) >", 0);
			$this->db->where_in('subscribers.status',1);
			$result = $this->db->get();
			if($result->num_rows() > 0){
				foreach($result->result() as $row) {
					$data[$i] = $row->mobilenumber;
				}
			}
		}
		if($data != '') {
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//sender id drop down value fetch function
	public function callertnumberfetchmodel() {
		$i=0;
		$numtype = $_GET['numtype'];
		$userid = $this->Basefunctions->userid;
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('callsettingsid,mobilenumber,apikey');
		$this->db->from('callsettings');
		$this->db->where('callsettings.callnumbertypeid',$numtype);
		$this->db->where('callsettings.callservicetypeid',3);
		$this->db->where('callsettings.callservicemodeid',3);
		$this->db->where("FIND_IN_SET('$industryid',callsettings.industryid) >", 0);
		$this->db->where('callsettings.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data[$i] = array('datasid'=>$row->callsettingsid,'dataname'=>$row->mobilenumber,'apikey'=>$row->apikey);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//account credit details fetch function 
	public function accountcresitdetailsfetchmodel($apikey,$addontypeid) { 
		$this->db->select('addonavailablecredit');
		$this->db->from('salesneuronaddonscredit');
		$this->db->where('salesneuronaddonscredit.addonstypeid',$addontypeid);
		$this->db->where('salesneuronaddonscredit.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$credit = $row->addonavailablecredit;
			}
		} else {
			$credit = '0';
		}
		return $credit;
	}
	//folder value fetch
	public function segmentddloadmodel() {
		$i=0;
		$listid = $_GET['listid'];
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('segmentsid,segmentsname');
		$this->db->from('segments');
		$this->db->where('segments.campaigngroupsid',$listid);
		$this->db->where("FIND_IN_SET('$industryid',segments.industryid) >", 0);
		$this->db->where('segments.status','1');
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->segmentsid,'dataname'=>$row->segmentsname);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//segment criteria fetch
	public function segmentcreteriadatafetch($segmentid) {
		$i = 0;
		$this->db->select('viewcreationcolumns.viewcreationcolmodelindexname,viewcreationcolumns.viewcreationcolmodeljointable,segmentscriteria.viewcreationconditionname,segmentscriteria.viewcreationconditionvalue,segmentscriteria.viewcreationandorvalue');
		$this->db->from('segmentscriteria');
		$this->db->join('viewcreationcolumns','viewcreationcolumns.viewcreationcolumnid=segmentscriteria.viewcreationcolumnid');
		$this->db->where('segmentscriteria.segmentsid',$segmentid);
		$this->db->where('segmentscriteria.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('indexname'=>$row->viewcreationcolmodelindexname,'jointable'=>$row->viewcreationcolmodeljointable,'critreiavalue'=>$row->viewcreationconditionvalue,'conditionidname'=>$row->viewcreationconditionname,'massandorcondidname'=>$row->viewcreationandorvalue);
				$i++;
			}
		} else {
			$data = array();
		}
		return $data;
	}
	//send sms model
	public function voicecallCampaignmodel() {
		$schdate="";
		$msgsenddate = "";
		$subscriberid = $_GET['selectedsegmentsubscriberid'];
		$caller = $_GET['caller'];
		$apikey = $_GET['apikey'];
		$subid = explode(',',$subscriberid);
		$campname =  $_GET['callcampaignname'];
		$groupid = $_GET['callgroupsid'];
		$totalsub =  $_GET['totalsubscribercount'];
		$validsub =  $_GET['validsubscribercount'];
		$mobilenumber =  $_GET['mobilenumber'];
		$soundsid =  $_GET['soundsid'];
		$retriesvalue =  $_GET['retriesvalue'];
		//Send type
		if(isset($_GET['smssendtypeid'])){
			$smssendtypeid = $_GET['smssendtypeid']; 
			if($smssendtypeid == '') { 
				$smssendtypeid = '1';
			}
		} else { 
			$smssendtypeid = '1';
		}
		//segment value
		if(isset($_GET['callsegmentsid'])){
			$segmentid = $_GET['callsegmentsid']; 
			if($segmentid == ''){ 
				$segmentid = '1';
			}
		} else { 
			$segmentid = '1';
		}
		//segment value
		if(isset($_GET['callnumbertypeid'])){
			$numbertype = $_GET['callnumbertypeid']; 
			if($numbertype == ''){ 
				$numbertype = '1';
			}
		} else { 
			$numbertype = '1';
		}
		//segment value
		if(isset($_GET['callintervalid'])){
			$interval = $_GET['callintervalid']; 
			if($interval == ''){ 
				$interval = '1';
			}
		} else { 
			$interval = '1';
		}
		$splay = $this->Basefunctions->generalinformaion('sounds','soundfileid','soundsid',$soundsid);
		$play = $splay.'.sound';
		$xmlurl = "http://obd.solutionsinfini.com/api/v1/?api_key=".$apikey."&method=voice.call&format=xml&play=".$play."&numbers=".$mobilenumber;
		$ch=curl_init();
		curl_setopt($ch, CURLOPT_URL, $xmlurl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$output=curl_exec($ch);
		curl_close($ch);
		$dataset = $this->parse_response($output);
		for($i=0;$i<count($dataset);$i++) {
			$date= date($this->Basefunctions->datef);
			$userid= $this->Basefunctions->userid;
			$activestatus = $this->Basefunctions->activestatus;
			$campaign = array(
				'callcampaignname'=>$campname,
				'campaigngroupsid'=>$groupid,
				'segmentsid'=>$segmentid,
				'totalsubscribercount'=>$totalsub,
				'validsubscribercount'=>$validsub,
				'smssendtypeid'=>$smssendtypeid,
				'callnumbertypeid'=>$numbertype,
				'callernumber'=>$caller,
				'mobilenumber'=>$mobilenumber,
				'soundsid'=>$soundsid,
				'retriesvalue'=>$retriesvalue,
				'callintervalid'=>$interval,
				'campaignuniqueid'=>$dataset[$i]['id'],
				'industryid'=>$this->Basefunctions->industryid,
				'branchid'=>$this->Basefunctions->branchid,
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
				'createdate'=>$date,
				'lastupdatedate'=>$date,
				'status'=>$activestatus
			);
			$this->db->insert('callcampaign',$campaign);
		}
		echo json_encode('Success');
	}
	//fetch xml content
	public function parse_response($response) {
		$xml = new SimpleXMLElement($response);
		$result = array();
		$h=0;
		foreach ($xml->data as $element) {
		     foreach($element as $key => $val) {
			 $result[$h][(string)$key] = (string)$val;
		     }
		     $h++;
		}
		return $result;
	}
}      