<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Callcampaign extends MX_Controller{
	//To load the Register View
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Callcampaign/Callcampaignmodel');
    }
	public function index() {
		$moduleid = array(34);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);		
		//view
		$viewfieldsmoduleids = array(34);
		$viewmoduleid = array(34);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Callcampaign/callcampaignview',$data);
	}
	//mass update default view fetch
	public function defaultviewfetch() {
		$mid = $_GET['modid'];
		$viewid = $this->Callcampaignmodel->defaultviewfetchmodel($mid);
		echo $viewid;
	}
	//parent table get
	public function parenttableget() {
		$this->Callcampaignmodel->parenttablegetmodel();
	}
	//fetch json data information
	public function callsubscribervalueget() {
		$groupid = $_GET['callgroupsid'];
		$segmentid = $_GET['segmentid'];
		$autoupdate = $_GET['autoupdate'];
		$segsubscriberid = $_GET['smssegmentsubscriber'];
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$chkbox = $_GET['checkbox'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'subscribers.subscribersid') : 'subscribers.subscribersid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('campaigngroupsname','firstname','lastname','mobilenumber','permissiongranted'),'colmodelindex'=>array('campaigngroups.campaigngroupsname','subscribers.firstname','subscribers.lastname','subscribers.mobilenumber','subscribers.permissiongranted'),'coltablename'=>array('campaigngroups','subscribers','subscribers','subscribers','subscribers'),'uitype'=>array('17','2','2','11','13'),'colname'=>array('Group Name','First Name','Last Name','Mobile Number','Permission Granted'),'colsize'=>array('200','200','200','200','200'));
		$result=$this->Callcampaignmodel->callsubscribervaluegetgridxmlview($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$groupid,$segmentid,$segsubscriberid,$autoupdate);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($result,$page,'');
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Call Subscriber List',$width,$height,$chkbox);
		} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Call Subscriber List',$width,$height,$chkbox);
		}
		echo json_encode($datas);
	}
	//segment based subscriber id get
	public function relatedsubscriberidget() {  
    	$this->Callcampaignmodel->relatedsubscriberidgetmodel();
    }
	//mobile number fetch function
	public function mobilenumberget() {  
    	$this->Callcampaignmodel->mobilenumbergetmodel();
    }
	//sms sender id value fetch
	public function callertnumberfetch() {
		$this->Callcampaignmodel->callertnumberfetchmodel();
	}
	//segment drop down load
	public function segmentddload() {
		$this->Callcampaignmodel->segmentddloadmodel();		
	} 
	// voice call process...
	public function voicecallcampaign() {
		$this->Callcampaignmodel->voicecallCampaignmodel();
	}
}