<div class="large-12 columns paddingzero formheader">
	<?php
		$this->load->view('Base/mainviewaddformheader');
	?>
</div>
 <div class="large-12 columns addformunderheader">&nbsp;</div>
<div class="large-12 columns scrollbarclass addformcontainer tabtouchaddcontainer">
	<div class="row">&nbsp;</div>
	<form method="POST" name="dataaddform" class="" action ="" id="dataaddform">
		<span id="formaddwizard" class="validationEngineContainer" >
			<span id="formeditwizard" class="validationEngineContainer">
				<?php
					//function call for form fields generation
					formfieldstemplategenerator($modtabgrp);
					$defaultrecordview = $this->Basefunctions->get_company_settings('mainviewdefaultview');
					echo hidden('mainviewdefaultview',$defaultrecordview);
				?>
			</span>
		</span>
		<!--hidden values-->
		<?php
			echo hidden('primarydataid','');
			echo hidden('sortorder','');
			echo hidden('sortcolumn','');
			echo hidden('resctable','stonecategory');
		?>
		<?php
		//hidden text box view columns field module ids
			$val = implode(',',$filedmodids);
			$modid = $this->Basefunctions->getmoduleid($filedmodids);
			echo hidden('viewfieldids',$modid);
			echo hidden('rate_round',$rate_round);
			echo hidden('counterstatus',$counterstatus);
		?>
	</form>
</div>