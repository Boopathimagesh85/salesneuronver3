<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('Base/headerfiles'); ?>
 </head>
<body class="">
	<?php
		$moduleid = implode(',',$moduleids);
		$device = $this->Basefunctions->deviceinfo();
		$dataset['gridenable'] = "yes"; //no
		$dataset['griddisplayid'] = "stonecreationview";
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['spanattr'] = array();
		$dataset['gridtableid'] = "stonegrid";
		$dataset['griddivid'] = "stonegriddiv";
		$dataset['moduleid'] = $moduleids;
		$dataset['forminfo'] = array(array('id'=>'stonecreationformadd','class'=>'hidedisplay','formname'=>'stoneform'));
		if($device=='phone') {
			$this->load->view('Base/gridmenuheadermobile',$dataset);
			$this->load->view('Base/overlaymobile');
			$this->load->view('Base/viewselectionoverlay');
		} else {
			$this->load->view('Base/gridmenuheader',$dataset);
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
		$this->load->view('Base/basedeleteform');
	?>
</body>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js File -->
	<script src="<?php echo base_url();?>js/Stone/stone.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/plugins/jqgrid/jquery.jqGrid.src.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/plugins/treemenu/modernizr.custom.min.js" type="text/javascript"></script>  
<script src="<?php echo base_url();?>js/plugins/treemenu/jquery.dlmenu.js" type="text/javascript"></script> 
</html>
