<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Stonemodel extends CI_Model
{
   	private $stonemoduleid = 42;	
	public function __construct()
    {
        parent::__construct();
		$this->load->model( 'Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
    public function stonecreate(){
    	//table and fields information
    	$formfieldsname = explode(',',$_POST['elementsname']);
    	$formfieldstable = explode(',',$_POST['elementstable']);
    	$formfieldscolmname = explode(',',$_POST['elementscolmn']);
    	$elementpartable = explode(',',$_POST['elementspartabname']);
		$_POST['vendoraccountid'] = ltrim($_POST['vendoraccountid'],",");
    	//filter unique parent table
    	$partablename =  $this->Crudmodel->filtervalue($elementpartable);
    	//filter unique fields table
    	$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
    	$tableinfo = explode(',',$fildstable);
    	$restricttable = explode(',',$_POST['resctable']);
    	//dynamic Insertion
    	$primaryid = $this->Crudmodel->datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable);
    	// update stone category
    	$stonecategoryid=$_POST['parentstonecategoryid'];
		$counterid = ltrim($_POST['loosecounter'],",");
    	$update=array('stonecategoryid'=>$stonecategoryid,'counterid'=>$counterid);
    	$this->db->where('stoneid',$primaryid);
    	$this->db->update('stone',$update);
    	// notification log
    	$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
    	$activity = ''.$user.' created stone - '.$_POST['stonename'].'';
    	$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,$this->stonemoduleid);
		// Loose stone mode - create product
		if($_POST['stonemodeid'] == 2) {
			$this->loosestoneproductcreate(1,$primaryid,$counterid);
		}
    	echo 'TRUE';
     }
	 // Loose stone mode - create product
	 public function loosestoneproductcreate($mode,$primaryid,$counterid) {
		$chargeid = $this->Basefunctions->singlefieldfetch('chargeid','stoneentrycalctypeid','stoneentrycalctype',$_POST['stoneentrycalctype']);
		$loosesalescharge = $this->Basefunctions->get_company_settings('loosesalescharge');
		$loosepurchasecharge = $this->Basefunctions->get_company_settings('loosepurchasecharge');
		//$_POST['vendoraccountid'] = ltrim($_POST['vendoraccountid'],",");
		if(empty($loosesalescharge)) {
			$loosesalescharge = $chargeid;
		}else {
			$loosesalescharge = $chargeid.','.$loosesalescharge;
		}
		if(empty($loosepurchasecharge)) {
			$loosepurchasecharge = $chargeid;
		}else {
			$loosepurchasecharge = $chargeid.','.$loosepurchasecharge;
		}
		$categoryid = $this->Basefunctions->singlefieldfetch('categoryid','stonecategoryid','stonecategory',$_POST['parentstonecategoryid']);
		if($mode == 1) { // add
			$purityid = $this->Basefunctions->singlefieldfetch('purityid','shortname','purity','LST');
			if(isset($_POST['vendoraccountid'])) {
				$_POST['vendoraccountid'] = $_POST['vendoraccountid'];
			} else{
				$_POST['vendoraccountid'] = '';
			}
			if(isset($_POST['supplytypeid'])) {
				$_POST['supplytypeid'] = $_POST['supplytypeid'];
			} else{
				$_POST['supplytypeid'] = '1';
			}
			if(isset($_POST['gstcalculationtypeid'])) {
				$_POST['gstcalculationtypeid'] = $_POST['gstcalculationtypeid'];
			} else{
				$_POST['gstcalculationtypeid'] = '';
			}
			$_POST['taxmasterid'] = '3';
			$productarray = array(
				'categoryid'=>$categoryid,	
				'productname'=>$_POST['stonename'],	
				'shortname'=>$_POST['shortname'],	
				'counterid'=>$counterid,	
				'purityid'=>$purityid,	
				'chargeid'=>$loosesalescharge,	
				'purchasechargeid'=>$loosepurchasecharge,	
				'tagtypeid'=>3,	
				'weightcalculationtypeid'=>3,
				'taxable'=>$_POST['stonegstapplicable'],
				'gstapplicable'=>$_POST['stonegstapplicable'],
				'supplytypeid'=>$_POST['supplytypeid'],
				'gstcalculationtypeid'=>$_POST['gstcalculationtypeid'],
				'gsthsncode'=>$_POST['stonegsthsncode'],
				'gsthsndescription'=>$_POST['stonegsthsndescription'],
				'taxmasterid'=>$_POST['taxmasterid'],
				'loosestone'=>1,
				'stoneid'=>$primaryid,
				'employeeid'=>$this->Basefunctions->logemployeeid,
				'accountid'=>$_POST['vendoraccountid'], 
				'branchid'=>$this->Basefunctions->branchid,
				'industryid'=>$this->Basefunctions->industryid
			);
			$header_insert = array_merge($productarray,$this->Crudmodel->defaultvalueget());
			$this->db->insert('product',array_filter($header_insert));
			$productid = $this->db->insert_id();
			// notification log
			$userid = $this->Basefunctions->logemployeeid;
			$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
			$activity = ''.$user.' created Product - '.$_POST['stonename'].'';
			$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,90);
			// product update for loose stone
			$stoneproduct = array('productid'=>$productid);
			$this->db->where('stoneid',$primaryid);
			$this->db->update('stone',array_filter($stoneproduct));
		}else if($mode == 0) {  // update
			$looseproductid = $this->Basefunctions->singlefieldfetch('productid','stoneid','stone',$primaryid);
			if (!isset($_POST['supplytypeid'])) {
				$_POST['supplytypeid'] = '1';
			}
			if (!isset($_POST['gstcalculationtypeid'])) {
				$_POST['gstcalculationtypeid'] = '1';
			}
			if (!isset($_POST['stonegsthsncode'])) {
				$_POST['stonegsthsncode'] = '';
			}
			if (!isset($_POST['stonegsthsndescription'])) {
				$_POST['stonegsthsndescription'] = '';
			}
			$_POST['taxmasterid'] = '3';
			if (!isset($_POST['vendoraccountid'])) {
				$_POST['vendoraccountid'] = '1';
			}
			$productarray = array(
				'categoryid'=>$categoryid,
				'productname'=>$_POST['stonename'],	
				'shortname'=>$_POST['shortname'],	
				'counterid'=>$counterid,	
				'chargeid'=>$loosesalescharge,	
				'purchasechargeid'=>$loosepurchasecharge,	
				'taxable'=>$_POST['stonegstapplicable'],
				'gstapplicable'=>$_POST['stonegstapplicable'],
				'supplytypeid'=>$_POST['supplytypeid'],
				'gstcalculationtypeid'=>$_POST['gstcalculationtypeid'],
				'gsthsncode'=>$_POST['stonegsthsncode'],
				'gsthsndescription'=>$_POST['stonegsthsndescription'],
				'taxmasterid'=>$_POST['taxmasterid'],
				'branchid'=>$this->Basefunctions->branchid,
				'accountid'=>$_POST['vendoraccountid']
			);
			$header_update = array_merge($productarray,$this->Crudmodel->updatedefaultvalueget());
			$this->db->where('productid',$looseproductid);
			$this->db->update('product',array_filter($header_update));
			// notification log
			$userid = $this->Basefunctions->logemployeeid;
			$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
			$activity = ''.$user.' updated Product - '.$_POST['stonename'].'';
			$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$activity ,$userid,90);
		}				
	}
    //chit book retrieve
    public function stoneretrieve($moduleid)
    {
    	//table and fields information
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementpartable']);
		$restricttable = explode(',',$_GET['resctable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['primaryid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetchwithrestrict($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$restricttable,$moduleid);
		echo $result;
   }
    public function stoneupdate(){
    	//table and fields information
    	$formfieldsname = explode(',',$_POST['elementsname']);
    	$formfieldstable = explode(',',$_POST['elementstable']);
    	$formfieldscolmname = explode(',',$_POST['elementscolmn']);
    	$elementpartable = explode(',',$_POST['elementspartabname']);
    	//filter unique parent table
    	$partablename =  $this->Crudmodel->filtervalue($elementpartable);
    	//filter unique fields table
    	$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
    	$tableinfo = explode(',',$fildstable);
    	$primaryid = $_POST['primaryid'];
    	$restricttable = explode(',',$_POST['resctable']);
    	$result = $this->Crudmodel->dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable);
    	// update stone category
    	$stonecategoryid=$_POST['parentstonecategoryid'];
    	$update=array('stonecategoryid'=>$stonecategoryid);
    	$this->db->where('stoneid',$primaryid);
    	$this->db->update('stone',$update);
    	// notification log
    	$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
    	$activity = ''.$user.' updated stone - '.$_POST['stonename'].'';
    	$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$activity ,$userid,$this->stonemoduleid);
		// Loose stone mode - create product
		if(isset($_POST['stonemodeid'])) {
			$_POST['stonemodeid'] = $_POST['stonemodeid'];
		} else{
			$_POST['stonemodeid'] = '';
		}
		if($_POST['stonemodeid'] == 2) {
			$counterid = ltrim($_POST['loosecounter'],",");
			$looseproductid = $this->Basefunctions->singlefieldfetch('productid','stoneid','stone',$primaryid);
			if($looseproductid != 1) {
				$this->loosestoneproductcreate(0,$primaryid,$counterid);
			} else {
				$this->loosestoneproductcreate(1,$primaryid,$counterid);
			}
		}
    	echo 'TRUE';
    }
    public function stonedelete($moduleid){
    	$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primaryid'];
		//$msg='False';
		$filename = $this->Basefunctions->generalinformaion('stone','stonename','stoneid',$id);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		$this->Crudmodel->outerdeletefunctionwithoutrestrict($partabname,$primaryname,$ctable,$id);
    	$stone = $this->Basefunctions->singlefieldfetch('stonename','stoneid','stone',$id);
    	$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
    	$activity = ''.$user.' deleted stone - '.$stone.'';
    	$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,$this->stonemoduleid);
		// loose stone product
		$stonemodeid = $this->Basefunctions->singlefieldfetch('stonemodeid','stoneid','stone',$id);
		if($stonemodeid == 2) {
			$productid = $this->Basefunctions->singlefieldfetch('productid','stoneid','stone',$primaryid);
			$stoneproduct = array('status'=>0);
			$this->db->where('productid',$productid);
			$this->db->update('product',array_filter($stoneproduct));
			// notification log
			$userid = $this->Basefunctions->logemployeeid;
			$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
			$productname = $this->Basefunctions->singlefieldfetch('productname','productid','product',$productid);
			$activity = ''.$user.' deleted Product - '.$productname.'';
			$this->Basefunctions->notificationcontentadd($productid,'Deleted',$activity ,$userid,90);
		}
    	echo 'TRUE';
    }
    //stone type based tree create model
    public function stonecategorytreecreatemodel($tablename,$stonetype,$stonemode)
    {
    	$id = $tablename.'id';
    	$name = $tablename.'name';
    	$pdataid = 'parent'.$tablename.'id';
    	$level = $tablename.'level';
    	$data = array();
		if($stonemode == '2') {
			$loosestonewhere = 'Yes';
		} else {
			$loosestonewhere = 'No';
		}
    	$this->db->select( ''.$id.' AS Id'.','.''.$name.' AS Name'.','.$pdataid.' AS ParentId'.','.$level.' AS level' );
    	$this->db->from($tablename);
    	$this->db->where(''.$tablename.'.status',1);
    	$this->db->where('stonecategory.stonetypeid',$stonetype);
		$this->db->where('stonecategory.loosestone',$loosestonewhere);
    	$this->db->order_by(''.$tablename.'.'.$id.'','ASC');
    	$result = $this->db->get();
    	foreach($result->result() as $row) {
    		$data[$row->Id] = array('id'=>$row->Id,'name'=>$row->Name,'pid'=>$row->ParentId,'level'=>$row->level);
    	}
    	$itemsByParent = array();
    	foreach ($data as $item){
    		if (!isset($itemsByParent[$item['pid']])) {
    			$itemsByParent[$item['pid']] = array();
    		}
    		$itemsByParent[$item['pid']][] = $item;
    	}//
    	return $itemsByParent;
    }
    //parent account name fetch
    public function stonecategorynamefetchmodel() {
    	$id=$_GET['id'];
    	$stonecategoryid = $this->Basefunctions->singlefieldfetch('stonecategoryid','stoneid','stone',$id);
    	$data=$this->db->select('a.stonecategoryid as stonecategoryids,a.stonecategoryname as stonecategorynames')
    	->from('stonecategory as a')
    	->where('a.stonecategoryid',$stonecategoryid)
    	->get();
    	if($data->num_rows() >0) {
    		foreach($data->result() as $in)	{
    			$parentcategoryid=$in->stonecategoryids;
    			$categoryname=$in->stonecategorynames;
    		}
    		$a = array('parentcategoryid'=>$parentcategoryid,'categoryname'=>$categoryname);
    	} else {
    		$a = array('parentcategoryid'=>'0','categoryname'=>'');
    	}
    	echo json_encode($a);
    }
	//check stone entry
    public function checkstoneentry() {
    	$stoneid=$_POST['stoneid'];
		$status = $_POST['status']; // Loose or Studded
		if($status == 1) {
			$status = $this->Basefunctions->singlefieldfetch('stonemodeid','stoneid','stone',$stoneid);
		}
		if($status == 2) {
			$productid = $this->Basefunctions->singlefieldfetch('productid','stoneid','stone',$stoneid);
			$this->db->select('itemtagid');
			$this->db->from('itemtag');
			$this->db->where('status',$this->Basefunctions->activestatus);
			$this->db->where('productid',$productid);
			$data=$this->db->get()->num_rows();
		} else {
			$this->db->select('stoneid');
			$this->db->from('stoneentry');
			$this->db->where('status',$this->Basefunctions->activestatus);
			$this->db->where('stoneid',$stoneid);
			$data=$this->db->get()->num_rows();
		}
		if($data > 0) {
			echo 'FAIL';
		}else{
			echo 'SUCCESS';
		}
    }
 }