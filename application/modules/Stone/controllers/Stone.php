<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Stone extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Stone/Stonemodel');
		$this->load->view('Base/formfieldgeneration');
    }
	//first basic hitting view
    public function index()
    {	
		$moduleid = array(42);
		sessionchecker($moduleid);
		$userid = $this->Basefunctions->userid;
		//action		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['rate_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //rate round-off
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		// counter status based on company settings
		$data['counterstatus'] = $this->Basefunctions->get_company_settings('counter');
		$this->load->view('Stone/stoneview',$data);
	}
    public function stonecreate(){
    	$this->Stonemodel->stonecreate();
    }
    public function stoneretrieve(){
    	$moduleid = 42;
    	$this->Stonemodel->stoneretrieve($moduleid);
    }
    public function stoneupdate(){
    	$this->Stonemodel->stoneupdate();
    }
    public function stonedelete(){
    	$moduleid = 42;
    	$this->Stonemodel->stonedelete($moduleid);
    }
    //new tree create
    public function treedatafetchfun() {
    	$tablename = $_POST['tabname'];
    	$manopt = $_POST['mandval'];
    	$lablname = $_POST['fieldlabl'];
    	$stonetype = $_POST['stonetype'];
		$stonemode = $_POST['stonemode'];
    	$type = $_POST['type'];
    	$mandlab = "";
    	if($manopt!='') {
    		$mandlab = "<span class='mandatoryfildclass'>*</span>";
    	}
    	echo '<label>'.$lablname.$mandlab.'</label>';
    	if($type == 'Detail') {
    		$buttonclass = "disabled=disabled ";
    	} else{
    		$buttonclass = " ";
    	}
    	echo divopen( array("id"=>"dl-menu","class"=>"dl-menuwrapper","style"=>"z-index:2;margin-bottom: 1rem") );
    	$type="button";
    	echo '<button name="treebutton" type="'.$type.'" id="treebutton" class="btn dl-trigger treemenubtnclick" tabindex="102"'.$buttonclass.'style="height:2.2rem;color:#ffffff;font-size: 1.45rem;padding: 0.1rem;padding-top:0.3rem;background-color:#546E7A;"><i class="material-icons">format_indent_increase</i></button>';
    	$txtoptions = array("style"=>"width:83%;display:inline;position:absolute;height:2.12rem;padding-left: 5px;color:#333333","disabled"=>"disabled","tabindex"=>"101","class"=>"".$manopt."");
    	echo text('parent'.$tablename.'','',$txtoptions);
    	echo hidden('parent'.$tablename.'id','');
    	echo '<ul class="dl-menu maintreegenerate large-12 medium-6 small-12 column" id="stonemasterlistuldata">';
    	$dataset = $this->Stonemodel->stonecategorytreecreatemodel($tablename,$stonetype,$stonemode);
    	$this->createTree($dataset,0);
    	echo close('ul');
    	echo close('div');
    	echo '<input type="hidden" value="'.$manopt.'" id="treevalidationcheck" name="treevalidationcheck">';
    	echo '<input type="hidden" value="'.$lablname.'" id="treefiledlabelcheck" name="treefiledlabelcheck">';
    }
	//tree creation
	public function createTree($items = array(), $parentId = 0,$removeid = 0) {
		echo " <ul class='dl-submenu'> ";
		if($removeid == 0){
			echo '<li><a href="#" style="font-weight:bold;">Remove<i class="material-icons parentclear">close</i></a></li>';
		}
		if(count($items) > 0 ) {
			foreach ($items[$parentId] as $item) {
				echo '<li data-listname="'.$item['name'].'" data-listid="'.$item['id'].'" ><a href="#">'.$item['name'].'</a>';//level title
				$curId = $item['id'];
				//if there are children
				if (!empty($items[$curId])) {
					$this->createTree($items, $curId,1);
				}
				echo '</li>';
			}
		}
		echo '</ul>';
	}    
    //parent account name fetch
    public function stonecategorynamefetch() {
    	$this->Stonemodel->stonecategorynamefetchmodel();
    }
	 //check stone entry
    public function checkstoneentry() {
    	$this->Stonemodel->checkstoneentry();
    }
}