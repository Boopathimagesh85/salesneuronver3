<?php
class Contractmodel extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
	}
	//contract create
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		$griddata = $_POST['griddatas'];
		$modid = $_POST['viewfieldids'];
		$griddatainfo = json_decode($griddata, true);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$restricttable = explode(',',$_POST['resctable']);
		//Retrive the contract autoNumber
		$anfieldnameinfo = $_POST['anfieldnameinfo'];
		$anfieldnameidinfo = $_POST['anfieldnameidinfo'];
		$anfieldtabinfo = $_POST['anfieldtabinfo'];
		$anfieldnamemodinfo = $_POST['anfieldnamemodinfo'];
		$randomnum = $this->Basefunctions->randomnumbergenerator($anfieldnamemodinfo,$anfieldtabinfo,$anfieldnameinfo,$anfieldnameidinfo);
		$_POST['contractnumber'] = trim($randomnum);
		//dynamic Insertion
		$primaryid = $this->Crudmodel->datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable);
		//address insertion
		$arrname=array('billing','shipping');
		$this->Crudmodel->addressdatainsert($arrname,$partablename,$primaryid);	
		//grid insert
		$griddata = $_POST['griddatas'];
		$griddatainfo = json_decode($griddata, true);
		//grid data insertion
		//primary key
		$defdataarr = $this->Crudmodel->defaultvalueget();
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$m=0;
		$h=1;
		$attachmentgrid = 0;
		$gridfieldpartabname = explode(',',$_POST['griddatapartabnameinfo']);
		$gridrows = explode(',',$_POST['numofrows']);
		$attachment_totrows = $gridrows[1] + 1;
		$griddata = $_POST['griddatas'];
		$griddatainfo = json_decode($griddata, true);
		foreach($gridrows as $rowcount) {
			$gridfieldsname = explode(',',$_POST['gridcolnames'.$h]);
			//filter grid unique fields table
			$gridfieldtabname = explode(',',$_POST['griddatatabnameinfo'.$h]);
			$gridfieldstable = $this->Crudmodel->filtervalue($gridfieldtabname);
			$gridtableinfo = explode(',',$gridfieldstable);
			$i = $attachmentgrid;
			for($i;$i<$attachment_totrows;$i++) {
				foreach( $gridtableinfo as $gdtblname ) {
					${'$gcdata'.$m} = array();
					$t=0;
					foreach($gridfieldsname AS $gdfldsname) {
						if(trim($gdtblname) != '') {
							if(strcmp( trim($gridfieldtabname[$t]),trim($gdtblname) ) == 0 ) {
								if( isset( $griddatainfo[$i][$gdfldsname] ) ) {
									$name = explode('_',$gdfldsname);
									if( ctype_alpha($griddatainfo[$i][$gdfldsname]) && $griddatainfo[$i][$gdfldsname] != "" ) {
										${'$gcdata'.$m}[$gdfldsname] = ucwords( $griddatainfo[$i][$gdfldsname] );
									} else if( $griddatainfo[$i][$gdfldsname] != "" ) {
										${'$gcdata'.$m}[$gdfldsname] = $griddatainfo[$i][$gdfldsname];
									}
								}
							}
						}
					}
					if(trim($gdtblname) != '') {
						if(count(${'$gcdata'.$m})>0) {
							${'$gcdata'.$m}[$primaryname]=$primaryid;
							$gnewdata = array_merge(${'$gcdata'.$m},$defdataarr);
							$gnewdata['commonid']=$primaryid;
							$gnewdata['moduleid']=$modid;
							if(!isset($gnewdata['employeeid'])){
								$gnewdata['employeeid']='1';}
							//unset the hidden variables
							$removehidden=array('crmfileinfoid','contractid');
							for($mk=0;$mk<count($removehidden);$mk++){
								unset($gnewdata[$removehidden[$mk]]);
							}
							$inarr = array_filter($gnewdata);
							$cnt = substr_count($inarr['filename'], ',');
							if($cnt >= 1){											
							$uname = explode(',' , $inarr['filename']);
							$usize = explode(',' , $inarr['filename_size']);
							$upath = explode(',' , $inarr['filename_path']);
							$utype = explode(',' , $inarr['filename_type']);
							$ffid = explode(',' , $inarr['filename_fromid']);
							foreach($uname as $key => $value){
								$inarr['filename'] = $value;
								$inarr['filename_size'] = $usize[$key];
								$inarr['filename_path'] = $upath[$key];
								$inarr['filename_type'] = $utype[$key];
								$inarr['filename_fromid'] = $ffid[$key];
								$inarr['industryid'] = $this->Basefunctions->industryid;
								$inarr['branchid'] = $this->Basefunctions->branchid;
								$this->db->insert($gdtblname,$inarr);
							}
							}else{
								$this->db->insert($gdtblname,$inarr);
							}
						}
						$m++;
					}
				}
			}
			$attachmentgrid++;
			$h++;
		}
		//notification entry
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		if(isset($_POST['contractemployeeid'])) {
			$assignid = $_POST['contractemployeeid'];
			$emptypes = $_POST['employeetypeid'];
			$empdataids = array();
			$assignempids = array();
			if($assignid != '') {
				$i = 0;
				$m=0;
				$empiddatas = explode(',',$assignid);
				foreach($empiddatas as $empids) {
					$emptype = $emptypes[$m];
					if($emptype == 1) {
						$empdataids[$i] = $empids;
						$i++;
					} else if($emptype == 2) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromgroup($empids);
						$i++;
					} else if($emptype == 3) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromroles($empids);
						$i++;
					} else if($emptype == 4) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromrolesandsubroles($empids);
						$i++;
					}
					$m++;
				}
			} else {
				$assignid = 1;
			}
		} else {
			$assignid = 1;
		}
		if(isset($_POST['contractname'])){
			$contname = $_POST['contractname'];
		}else{
			$contname = '';
		}		
		if($assignid == '1'){
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch($modid,$primaryid,$partablename,2);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,$modid);
			} 
		} else {
			foreach($empdataids as $empidinfo) {
				if(is_array($empidinfo)) { //group of employees
					foreach($empidinfo as $empid) {
						if( !in_array($empid,$assignempids) ) {
							array_push($assignempids,$empid);
							$notimsg = $this->Basefunctions->notificationtemplatedatafetch($modid,$primaryid,$partablename,2);
							if($notimsg != '') {
								$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,$modid);
							} 
						}
					}
				} else { //individual employees
					if( !in_array($empidinfo,$assignempids) ) {
						array_push($assignempids,$empidinfo);
						$notimsg = $this->Basefunctions->notificationtemplatedatafetch($modid,$primaryid,$partablename,2);
						if($notimsg != '') {
							$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,$modid);
						}
					}
				}
			}
		}
		{//workflow management -- for data create
			$this->Basefunctions->workflowmanageindatacreatemode($modid,$primaryid,$partablename);
		}
		$softwareindustryid = $this->Basefunctions->industryid;
		if($softwareindustryid == 4) {
			echo $primaryid;
		} else {
			echo "TRUE";
		}
	}
	//Retrieve Campaign data for edit
	public function informationfetchmodel($moduleid) {
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementpartable']);
		$restricttable = explode(',',$_GET['resctable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['dataprimaryid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetchwithrestrict($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$restricttable,$moduleid);
		echo $result;
	}
	//contract update
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['primarydataid'];
		//industry based moduleid
		$moduleid = $_POST['viewfieldids'];
		{//fetch old values -- work flow
			$condstatvals=$this->Basefunctions->workflowmanageolddatainfofetch($moduleid,$primaryid);
		}
		$restricttable = explode(',',$_POST['resctable']);
		$result = $this->Crudmodel->dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable);
		$arrname=array('billing','shipping');
		$this->Crudmodel->addressdataupdate($arrname,$partablename,$primaryid);
		//retrieve all the documents
		$crmfileinfoiddata = $this->retrievealldocuments($primaryid);
		//primary key
		$defdataarr = $this->Crudmodel->defaultvalueget();
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$m=0;
		$h=1;
		$attachmentgrid = 0;
		$gridfieldpartabname = explode(',',$_POST['griddatapartabnameinfo']);
		$gridrows = explode(',',$_POST['numofrows']);
		$attachment_totrows = $gridrows[1] + 1;
		$griddata = $_POST['griddatas'];
		$griddatainfo = json_decode($griddata, true);
		foreach($gridrows as $rowcount) {
			$gridfieldsname = explode(',',$_POST['gridcolnames'.$h]);
			//filter grid unique fields table
			$gridfieldtabname = explode(',',$_POST['griddatatabnameinfo'.$h]);
			$gridfieldstable = $this->Crudmodel->filtervalue($gridfieldtabname);
			$gridtableinfo = explode(',',$gridfieldstable);
			$i = $attachmentgrid;
			for($i;$i<$attachment_totrows;$i++) {
				foreach( $gridtableinfo as $gdtblname ) {
					${'$gcdata'.$m} = array();
					$t=0;
					foreach($gridfieldsname AS $gdfldsname) {
						if(trim($gdtblname) != '') {
							if(strcmp( trim($gridfieldtabname[$t]),trim($gdtblname) ) == 0 ) {
								if( isset( $griddatainfo[$i][$gdfldsname] ) ) {
									$name = explode('_',$gdfldsname);
									if( ctype_alpha($griddatainfo[$i][$gdfldsname]) && $griddatainfo[$i][$gdfldsname] != "" ) {
										${'$gcdata'.$m}[$gdfldsname] = ucwords( $griddatainfo[$i][$gdfldsname] );
									} else if( $griddatainfo[$i][$gdfldsname] != "" ) {
										${'$gcdata'.$m}[$gdfldsname] = $griddatainfo[$i][$gdfldsname];
									}
								}
							}
						}
					}
					if(trim($gdtblname) != '') {
						if(count(${'$gcdata'.$m})>0) {
							${'$gcdata'.$m}['commonid']=$primaryid;
							${'$gcdata'.$m}['moduleid']=$moduleid;
							$gnewdata = array_merge(${'$gcdata'.$m},$defdataarr);
							//unset the hidden variables
							$removehidden=array('contractid');
							for($mk=0;$mk<count($removehidden);$mk++){
								unset($gnewdata[$removehidden[$mk]]);
							}
							if(isset($griddatainfo[$i]['crmfileinfoid'])) {
								$griddatainfo[$i]['crmfileinfoid'] = $griddatainfo[$i]['crmfileinfoid'];
								if (($key = array_search($griddatainfo[$i]['crmfileinfoid'], $crmfileinfoiddata)) !== false) {
									unset($crmfileinfoiddata[$key]);
								}
							} else {
								$griddatainfo[$i]['crmfileinfoid'] = 0;
							}
							if($griddatainfo[$i]['crmfileinfoid'] == 0) { //new-data
								$this->db->insert($gdtblname,array_filter($gnewdata));
							}
							/* if($griddatainfo[$i]['crmfileinfoid'] > 0){ //update-existing data
								$delete_sub_data=array('lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid,'status'=>$this->Basefunctions->deletestatus);
								$moduledetailtable=array('crmfileinfo');
								//delete the sub tables(crmfileinfo)
								foreach($moduledetailtable as $table) {
									$this->db->where_in('crmfileinfoid',$griddatainfo[$i]['crmfileinfoid']);
									$this->db->update($table,$delete_sub_data);
								}
							} */
						}
						$m++;
					}
				}
			}
			$attachmentgrid++;
			$h++;
		}
		$deleteddocuments = count($crmfileinfoiddata);
		if($deleteddocuments > 0) {
			$delete_sub_data=array('lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid,'status'=>$this->Basefunctions->deletestatus);
			//delete the sub tables(crmfileinfo)
			$this->db->where_in('moduleid','230');
			$this->db->where_in('crmfileinfoid',$crmfileinfoiddata);
			$this->db->update('crmfileinfo',$delete_sub_data);
		}
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		if(isset($_POST['contractemployeeid'])) {
			$assignid = $_POST['contractemployeeid'];
			$emptypes = $_POST['employeetypeid'];
			$empdataids = array();
			$assignempids = array();
			if($assignid != '') {
				$i = 0;
				$m=0;
				$empiddatas = explode(',',$assignid);
				foreach($empiddatas as $empids) {
					$emptype = $emptypes[$m];
					if($emptype == 1) {
						$empdataids[$i] = $empids;
						$i++;
					} else if($emptype == 2) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromgroup($empids);
						$i++;
					} else if($emptype == 3) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromroles($empids);
						$i++;
					} else if($emptype == 4) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromrolesandsubroles($empids);
						$i++;
					}
					$m++;
				}
			} else {
				$assignid = 1;
			}
		} else {
			$assignid = 1;
		}
		if(isset($_POST['contractname'])){
			$contname = $_POST['contractname'];
		}else{
			$contname = '';
		}
		if($assignid == '1'){
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,3);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,$moduleid);
			}
		} else {
			foreach($empdataids as $empidinfo) {
				if(is_array($empidinfo)) { //group of employees
					foreach($empidinfo as $empid) {
						if( !in_array($empid,$assignempids) ) {
							array_push($assignempids,$empid);
							$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,3);
							if($notimsg != '') {
								$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,$moduleid);
							}
						}
					}
				} else { //individual employees
					if( !in_array($empidinfo,$assignempids) ) {
						array_push($assignempids,$empidinfo);
						$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,3);
						if($notimsg != '') {
							$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,$moduleid);
						}
					}
				}
			}
		}
		{//workflow management for data update
			$this->Basefunctions->workflowmanageindataupdatemode($moduleid,$primaryid,$partablename,$condstatvals);
		}
		$softwareindustryid = $this->Basefunctions->industryid;
		if($softwareindustryid == 4){
			echo $primaryid;
		}else{
			echo "TRUE";
		}
	}
	// Retrieve documents for specific primaryid
	public function retrievealldocuments($primaryid) {
		$resultdata = array();
		$this->db->select('crmfileinfo.crmfileinfoid');
		$this->db->from('crmfileinfo');
		$this->db->where('crmfileinfo.moduleid','230');
		$this->db->where('crmfileinfo.commonid',$primaryid);
		$result = $this->db->get()->result();
		$countableresult = count($result);
		if($countableresult > 0) {
			foreach($result as $rowdata) {
				$resultdata[] = $rowdata->crmfileinfoid;
			}
		}
		return $resultdata;
	}
	//Campaign delete
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primarydataid'];
		$msg='False';
		$filename = $this->Basefunctions->generalinformaion('contract','contractnumber','contractid',$id);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//industrybased moduleid fetch
		$modid = $this->Basefunctions->getmoduleid($moduleid);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		{//workflow management -- for data delete
			$this->Basefunctions->workflowmanageindatadeletemode($modid,$id,$primaryname,$partabname);
		}
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				$msg='Denied';
			} else {
				//deleted file name
				$notimsg = $this->Basefunctions->notificationtemplatedatafetch($modid,$id,$partabname,4);
				if($notimsg != '') {
					$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,$modid);
				}
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				$this->unlinkdocumentsfromlocaldrive($_GET['primarydataid']);
				$msg='TRUE';
			}
		} else {
			//deleted file name
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch($modid,$id,$partabname,4);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,1,$modid);
			}
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			$this->unlinkdocumentsfromlocaldrive($_GET['primarydataid']);
			$msg='TRUE';
		}
		echo $msg;
	} 
	// Remove documents from local drive
	public function unlinkdocumentsfromlocaldrive($primaryid) {
		$this->db->select('crmfileinfo.filename_path');
		$this->db->from('crmfileinfo');
		$this->db->where('crmfileinfo.moduleid','230');
		$this->db->where('crmfileinfo.commonid',$primaryid);
		$result = $this->db->get()->result();
		$countableresult = count($result);
		if($countableresult > 0) {
			foreach($result as $rowdata) {
				unlink($rowdata->filename_path);
			}
		}	
	}
	//address insert
	public function addressdatainsert($partablename,$primaryid) {
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$arrname=array('billing','shipping');
		$arrvalue=array('4','5');
		$table=$partablename.'address';
		for($i=0;$i < count($arrname);$i++) {
			$address = array(
								$primaryname=>$primaryid,
								'addresstypeid'=>$_POST[$arrname[$i].'addresstype'],
								'addressmethod'=>$arrvalue[$i],
								'address'=>$_POST[$arrname[$i].'address'],
								'pincode'=>$_POST[$arrname[$i].'pincode'],
								'city'=>$_POST[$arrname[$i].'city'],
								'state'=>$_POST[$arrname[$i].'state'],
								'country'=>$_POST[$arrname[$i].'country']);
			$defdataarr = $this->Crudmodel->defaultvalueget();
			$newdata = array_merge($address,$defdataarr);
			$finaladd=array_filter($newdata);
			$this->db->insert($table,$finaladd);
		}
	}
	//solutions document details fetch
	public function documentsdetailfetchmodel() {
		$productdetail ='';
		$contractid=trim($_GET['primarydataid']);
		$industryid = $this->Basefunctions->industryid;
		if($industryid == 4){
			$moduleid = '80';
		} else {
			$moduleid = '230';
		}
		$this->db->select('crmfileinfoid,commonid,filename,crmfileinfo.filename_size,crmfileinfo.filename_type,crmfileinfo.filename_path,foldername.foldernamename,crmfileinfo.keywords,crmfileinfo.foldernameid,crmfileinfo.employeeid,crmfileinfo.filename_fromid');
		$this->db->from('crmfileinfo');
		$this->db->join('foldername','foldername.foldernameid=crmfileinfo.foldernameid');
		$this->db->where('crmfileinfo.moduleid',$moduleid);
		$this->db->where_in('crmfileinfo.commonid',array($contractid));
		$this->db->where('crmfileinfo.status',$this->Basefunctions->activestatus);
		$data=$this->db->get()->result();	
		$j=0;
		foreach($data as $value) {			
			$productdetail->rows[$j]['id']=$value->crmfileinfoid;
			$productdetail->rows[$j]['cell']=array(
													$value->crmfileinfoid,
													$value->foldernamename,
													$value->foldernameid,
													$value->keywords,
													$value->filename_fromid,
													$value->filename,
													$value->filename_size,
													$value->filename_type,
													$value->filename_path,
												);						
			$j++;				
		}
		if($productdetail == ''){
			$productdetail = array('fail'=>'FAILED');
		}
		echo  json_encode($productdetail);
	}
	//primary and seconadaty address value fetch
	public function primaryaddressvalfetchmodel() {
		$accid = $_GET['dataprimaryid'];	
		$addtype = $_GET['addtype'];	
		$this->db->select('contractaddress.addresstypeid,,contractaddress.address,contractaddress.pincode,contractaddress.city,contractaddress.state,contractaddress.country',false);
		$this->db->from('contractaddress');
		$this->db->where('contractaddress.addressmethod',$addtype);
		$this->db->where('contractaddress.contractid',$accid);
		$this->db->where('contractaddress.Status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$data = array('addresstypeid'=>$row->addresstypeid,'address'=>$row->address,'pincode'=>$row->pincode,'city'=>$row->city,'state'=>$row->state,'country'=>$row->country);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//account name based contact name
	public function fetchcontatdatawithmultiplecondmodel() {
		$i = 0;
		$accid = $_GET['accid'];
		$this->db->select('contactname,contactid',false);
		$this->db->from('contact');
		if($accid != 1){
			$this->db->where('contact.accountid',$accid);
		}
		$this->db->where('contact.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data[$i] = array('datasid'=>$row->contactid,'dataname'=>$row->contactname);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	function fetchmembersdetailsbasedondatemodel(){
		$i = 0;
		$contactid = $_GET['contactid'];
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('product.productname,product.productid',false);
		$this->db->from('contactdetail');
		$this->db->join('product','product.productid=contactdetail.membershipplans');
		$this->db->where('contactdetail.contactid',$contactid);
		$this->db->where('contactdetail.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data[$i] = array('productname'=>$row->productname,'productid'=>$row->productid);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	function membershipplansdatafectchoncontactmodel(){
		$planid = $_GET['planid'];
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('duration.durationid,duration.durationname,contactdetail.joiningdate,contactdetail.amount,contactdetail.expirydate',false);
		$this->db->from('contactdetail');
		$this->db->join('duration','duration.durationid=contactdetail.durationid');
		$this->db->where('contactdetail.membershipplans',$planid);
		$this->db->where('contactdetail.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data = array('durationid'=>$row->durationid,'durationname'=>$row->durationname,'joiningdate'=>$row->joiningdate,'amount'=>$row->amount,'expirydate'=>$row->expirydate);
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	function localviewgridheadergenerate($colinfo,$modulename,$width,$height,$checkbox = 'false') {
		$CI =& get_instance();
		/* width & height set */
		$columnswidth = 1200;
		$addcolsize = 0;
		$reccount = count($colinfo['colmodelname']);
		$columnswidth = 5;
		$reccount = 0;
		foreach ($colinfo['colmodelviewtype'] as $type) {
			$columnswidth += $type=='1'? 150 : 0;
			$reccount++;
		}
		$extrasize = ($columnswidth>$width)?0 : ($width-$columnswidth);
		$addcolsize = $extrasize!=0 ? round($extrasize/$reccount) : 0;
		$columnswidth = ($columnswidth>$width)?$columnswidth : $width;
		/* height set */
		if($height<=420) {
			$datarowheight = $height-35;
		} else {
			$datarowheight = $height-35;
		}
		/* header generation */
		if($checkbox == 'true') {
			$columnswidth = $columnswidth+35;
		}
		$mdivopen ='<div class="grid-view">';
		$header = '<div class="header-caption" style="width:'.$columnswidth.'px;"><ul class="inline-list">';
		$i=0;
		$m=0;
		$modname = strtolower(preg_replace('/[^A-Za-z0-9]/', '', $modulename));
		foreach ($colinfo['fieldlabel'] as $headname) {
			if($checkbox == 'true' && $m==0) {
				$header .='<li data-uitype="13" data-fieldname="gridcheckboxfield" data-width="35" data-class="gridcheckboxclass" data-viewtype="1" style="width:35px;" id="'.$modname.'headcheckbox"><input type="checkbox" id="'.$modname.'_headchkbox" name="'.$modname.'_headchkbox" class="'.$modname.'_headchkboxclass headercheckbox" /></li>';
				$m=1;
			}
			$cmodname = strtolower($colinfo['colmodelname'][$i]);
			$cmodindex = strtolower($colinfo['colmodelindex'][$i]);
			$cfield = strtolower($colinfo['fieldname'][$i]);
			$viewtype = ($colinfo['colmodelviewtype'][$i]=='0') ? ' display:none':'';
			$header .='<li id="'.$modname.'headcol'.$i.'" class="'.$cmodname.'-class '.$modname.'headercolsort" data-class="'.$cmodname.'-class" data-sortcolname="'.$cmodindex.'" data-sortorder="ASC" data-uitype="'.$colinfo['colmodeluitype'][$i].'" data-fieldname="'.$cfield.'" style="width:'.(150+$addcolsize).'px;'.$viewtype.'" data-width="'.(150+$addcolsize).'" data-viewtype="'.$colinfo['colmodelviewtype'][$i].'" data-position="'.$i.'">'.$headname.'</li>';
			$i++;
		}
		$header .='</ul></div>';
		$content = '<div class="gridcontent" style="height:'.$datarowheight.'px; overflow:auto;"><div class="data-content wrappercontent" style="width:'.$columnswidth.'px;">&nbsp;</div></div';
		$mdivclose = '</div>';
		$datas = $mdivopen.$header.$content.$mdivclose;
		$footer = '';
		/* Footer generation */
		$footer = '<div class="summary-blue">';
		$footer .= '</div>';
		return array('content'=>$datas,'footer'=>$footer);
	}
	function mobilelocalviewgridheadergenerate($colinfo,$modulename,$width,$height,$checkbox = 'false') {
		$CI =& get_instance();
		/* Height set */
		if($height > '460') {
			$height = $height-105;
		}else {
			$height = $height-128;
		}
		$mdivopen ='<div class="grid-view">';
		$content = '<div class="header-caption" style="display:none">';
		$i=0;
		$m=0;
		$modname = strtolower(preg_replace('/[^A-Za-z0-9]/', '', $modulename));
		foreach ($colinfo['fieldlabel'] as $headname) {
			if($checkbox == 'true' && $m==0) {
				$content .='<span data-uitype="13" data-fieldname="gridcheckboxfield" data-class="gridcheckboxclass" style="width:35px;" data-viewtype="1" id="'.$modname.'headcheckbox"<input type="checkbox" id="'.$modname.'_headchkbox" name="'.$modname.'_headchkbox" class="content"/></span>';
				$m=1;
			}
			$cmodname = strtolower($colinfo['colmodelname'][$i]);
			$cmodindex = strtolower($colinfo['colmodelindex'][$i]);
			$cfield = strtolower($colinfo['fieldname'][$i]);
			$viewtype = ($colinfo['colmodelviewtype'][$i]=='0') ? ' display:none':'';
			$content .= '<span id="'.$modname.'headcol'.$i.'"  data-class="'.$cmodname.'-class" data-sortcolname="'.$cmodindex.'" data-sortorder="ASC" data-uitype="'.$colinfo['colmodeluitype'][$i].'" data-fieldname="'.$cfield.'" class="'.$cmodname.'-class '.$modname.'headercolsort content" style="width:150px;'.$viewtype.'"  data-viewtype="'.$colinfo['colmodelviewtype'][$i].'" data-position="'.$i.'" data-label="'.$headname.' ">'.$headname.':</span>';
			$i++;
		}
		$content .='</div><div class="row-content gridcontent" style="height:'.$height.'px;"><div class="wrappercontent"></div></div>';
		$mdivclose = '</div>';
		$datas = $content;
		/* Footer generation */
		$footer = '<div class="summary-blue">';
		$footer .= '</div>';
		return array('content'=>$datas,'footer'=>$footer);
	}
	//terms and condition data fetch
	public function termsandcontdatafetchmodel() {
		$id = $_GET['id'];
		$this->db->select('termsandconditionstermsandconditions_editorfilename');
		$this->db->from('termsandcondition');
		$this->db->where('termsandcondition.termsandconditionid',$id);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$data = $row->termsandconditionstermsandconditions_editorfilename;
			}
			echo json_encode($data);
		}
	}
	//Folder main grid view
	public function contractfoldernamegridxmlview($tablename,$sortcol,$sortord,$pagenum,$rowscount) {
		$dataset = 'foldername.foldernamename,foldername.foldernameid,foldername.description,foldername.setdefault,foldername.public';
		$join ='';
		$status = $tablename.'.status IN (1)';
		$where = $tablename.'.moduleid IN (230)';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		//query
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$where.' AND '.$status.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		return $data;
	}
	//Folder Insertion
	public function contractfolderinsertmodel() {
		$foldername = $_POST['foldername'];
		$description = $_POST['description'];
		$default = $_POST['setdefault'];
		$ppublic = $_POST['ppublic'];
		$date = date($this->Basefunctions->datef);
		$folderarray = array(
				'foldernamename'=>$foldername,
				'description'=>$description,
				'moduleid'=>'230',
				'setdefault'=>$default,
				'public'=>$ppublic,
				'userroleid'=>$this->Basefunctions->userroleid,
				'industryid'=>$this->Basefunctions->industryid,
				'createdate'=>$date,
				'lastupdatedate'=>$date,
				'createuserid'=>$this->Basefunctions->userid,
				'lastupdateuserid'=>$this->Basefunctions->userid,
				'status'=>$this->Basefunctions->activestatus
		);
		$this->db->insert('foldername',$folderarray);
		$folderid = $this->db->insert_id();
		$this->setdefaultupdate(230,$default,$folderid);
		echo "TRUE";
	}
	//set default update
	public function setdefaultupdate($moduleid,$default,$folderid) {
		if($default != 'No') {
			$updatearray = array('setdefault'=>'No');
			$this->db->where_not_in('foldername.foldernameid',$folderid);
			$this->db->where('foldername.moduleid',$moduleid);
			$this->db->update('foldername',$updatearray);
		}
	}
	//Folder Edit Data Fetch
	public function contractfolderdatafetchmodel() {
		$folderid = $_GET['datarowid'];
		$this->db->select('SQL_CALC_FOUND_ROWS  foldername.foldernamename,foldername.foldernameid,foldername.description,foldername.setdefault,foldername.public',false);
		$this->db->from('foldername');
		$this->db->where('foldername.foldernameid',$folderid);
		$this->db->where('foldername.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data=array('id'=>$row->foldernameid,'foldername'=>$row->foldernamename,'description'=>$row->description,'setdefault'=>$row->setdefault,'ppublic'=>$row->public);
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//Folder Update
	public function contractfolderupdatemodel() {
		$foldernameid = $_POST['foldernameid'];
		$foldername = $_POST['foldername'];
		$description = $_POST['description'];
		$default = $_POST['setdefault'];
		$ppublic = $_POST['ppublic'];
		$date = date($this->Basefunctions->datef);
		$folderarray = array(
				'foldernamename'=>$foldername,
				'description'=>$description,
				'setdefault'=>$default,
				'public'=>$ppublic,
				'userroleid'=>$this->Basefunctions->userroleid,
				'industryid'=>$this->Basefunctions->industryid,
				'lastupdatedate'=>$date,
				'lastupdateuserid'=>$this->Basefunctions->userid,
				'status'=>$this->Basefunctions->activestatus
		);
		$this->db->where('foldername.foldernameid',$foldernameid);
		$this->db->update('foldername',$folderarray);
		$this->setdefaultupdate(230,$default,$foldernameid);
		echo "TRUE";
	}
	//Folder Delete
	public function folderdeleteoperationmodel() {
		$folderid = $_GET['id'];
		$sndefault = $this->Basefunctions->singlefieldfetch('sndefault','foldernameid','foldername',$folderid); //default record
		if($sndefault != '2') {
			$date = date($this->Basefunctions->datef);
			$folderarray = array(
							'lastupdatedate'=>$date,
							'lastupdateuserid'=>$this->Basefunctions->userid,
							'status'=>0
						);
			$this->db->where('foldername.foldernameid',$folderid);
			$this->db->update('foldername',$folderarray);
			echo "TRUE";
		} else {
			echo "DEFAULT";
		}
	}
	//Folder Drop Down Reload function
	public function fetchdddataviewddvalmodel() {
		$loguserid = $this->Basefunctions->userid;
		$i = 0;
		$fieldname = $_GET['dataname'];
		$fieldid = $_GET['dataid'];
		$tablename = $_GET['datatab'];
		$moduleid = $_GET['moduleid'];
		$this->db->select("$fieldid,$fieldname,setdefault");
		$this->db->from("$tablename");
		$this->db->where("FIND_IN_SET('$moduleid',".$tablename.".moduleid) >", 0);
		if($loguserid != 2){
			$this->db->where_not_in("$tablename".'.public','No');
		}
		$this->db->where("$tablename".'.status',1);
		$result=$this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data[$i]=array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname,'setdefault'=>$row->setdefault);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//Folder name unique model
	public function foldernameuniquemodel(){
		$industryid = $this->Basefunctions->industryid;
		$foldernamename = $_POST['foldernamename'];
		if($foldernamename != "") {
			$result = $this->db->select('foldername.foldernameid')->from('foldername')->where('foldername.foldernamename',$foldernamename)->where('foldername.moduleid','230')->where("FIND_IN_SET('$industryid',foldername.industryid) >", 0)->where('foldername.status','1')->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->foldernameid;
				}
			} else {
				echo "False";
			}
		} else {
			echo "False";
		}
	}
}