<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contract extends MX_Controller{
    //Constructor Function To Load Models
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Contract/Contractmodel');
	}
	//Default View 
	public function index()	{
		$moduleid = array(230,80);
		sessionchecker($moduleid);
		//action
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(230,80);
		$viewmoduleid = array(230,80);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['dashboradviewdata'] = $this->Basefunctions->dashboarddataviewbydropdown($moduleid);
        $this->load->view('Contract/contractview',$data);
	}
	//create Campaign
	public function newdatacreate() {
    	$this->Contractmodel->newdatacreatemodel();
    }
	//information fetch
	public function fetchformdataeditdetails() {
		$moduleid = array(230,80);
		$this->Contractmodel->informationfetchmodel($moduleid);
	}
	//update Campaign
    public function datainformationupdate() {
        $this->Contractmodel->datainformationupdatemodel();
    }
	//delete Campaign
    public function deleteinformationdata() {
    	$moduleid = array(230,80);
		$this->Contractmodel->deleteoldinformation($moduleid);
    }  
	//document details fetch
	public function documentsdetailfetch() {
		$this->Contractmodel->documentsdetailfetchmodel();
	}
	//primary address value fetch
	public function primaryaddressvalfetch() {
		$this->Contractmodel->primaryaddressvalfetchmodel();
	}
	//account name based contact name
	public function fetchcontatdatawithmultiplecond(){
		$this->Contractmodel->fetchcontatdatawithmultiplecondmodel();
	}
	//editor value fetch
	public function editervaluefetch() {
		$filename = $_GET['filename'];
		$newfilename = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]);
			echo json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			echo json_encode($tccontent);
		}
	}
	/* Local non-live grid heder information fetch */
	public function localgirdheaderinformationfetch() {
		$moduleid = $_GET['moduleid'];
		$tabgrpid = $_GET['tabgroupid'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modname = $_GET['modulename'];
		$colinfo = $this->Basefunctions->localgridheaderinformationfetchmodel($moduleid,$tabgrpid);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = $this->Contractmodel->mobilelocalviewgridheadergenerate($colinfo,$modname,$width,$height);
		} else {
			$datas = $this->Contractmodel->localviewgridheadergenerate($colinfo,$modname,$width,$height);
		}
		echo json_encode($datas);
	}
	//terms and condition data fetch
	public function termsandcontdatafetch() {
		$this->Contractmodel->termsandcontdatafetchmodel();
	}
	
	public function fetchmembersdetailsbasedondate(){
		$this->Contractmodel->fetchmembersdetailsbasedondatemodel();
	}
	public function membershipplansdatafectchoncontact(){
		$this->Contractmodel->membershipplansdatafectchoncontactmodel();
	}
	//Folder insertion
	public function contractfolderinsert() {
		$this->Contractmodel->contractfolderinsertmodel();
	}
	//Folder Grid View
	public function contractfoldernamegridfetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'foldername.foldernameid') : 'foldername.foldernameid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('foldernamename','description','setdefault','public'),'colmodelindex'=>array('foldername.foldernamename','foldername.description','foldername.setdefault','foldername.public'),'coltablename'=>array('foldername','foldername','foldername','foldername'),'uitype'=>array('2','2','2','2'),'colname'=>array('Folder Name','Description','Default','Public'),'colsize'=>array('200','200','200','200'));
		$result=$this->Contractmodel->contractfoldernamegridxmlview($primarytable,$sortcol,$sortord,$pagenum,$rowscount);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($result,$page,'');
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Folder List',$width,$height);
		} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Folder List',$width,$height);
		}
		echo json_encode($datas);
	}
	//Folder data fetch
	public function contractfolderdatafetch() {
		$this->Contractmodel->contractfolderdatafetchmodel();
	}
	//Folder Update
	public function contractfolderupdate() {
		$this->Contractmodel->contractfolderupdatemodel();
	}
	//Folder Delete
	public function folderdeleteoperation() {
		$this->Contractmodel->folderdeleteoperationmodel();
	}
	//Folder drop down Fetch Data
	public function fetchdddataviewddval() {
		$this->Contractmodel->fetchdddataviewddvalmodel();
	}
	//Folder Unique Name
	public function foldernameunique() {
		$this->Contractmodel->foldernameuniquemodel();
	}
}