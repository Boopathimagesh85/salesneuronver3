<style type="text/css">
	#chitgenerationinnergrid .gridcontent{
		height: 75vh !important;
	}
</style>
<div id="chitgenerationcreationview" class="gridviewdivforsh">
	<?php
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['gridid'] = 'chitgenerationgrid';
		$dataset['gridwidth'] = 'chitgenerationgridwidth';
		$dataset['gridfooter'] = 'chitgenerationgridfooter';
		$dataset['viewtype'] = 'disable';
		$dataset['moduleid'] = '273';
		$this->load->view('Base/singlemainviewformwithgrid.php',$dataset);
		$defaultrecordview = $this->Basefunctions->get_company_settings('mainviewdefaultview');
		$chitinteresttype = $this->Basefunctions->get_company_settings('chitinteresttype');
		echo hidden('mainviewdefaultview',$defaultrecordview);
		echo hidden('chitinteresttype',$chitinteresttype);
	?>
</div>
</div>
</div>
</div>
<input type="hidden" name="hiddeninterestrate" id="hiddeninterestrate" />
<input type="hidden" name="hiddenrefundrate" id="hiddenrefundrate" />
<input type="hidden" name="hiddenchitlookupsid" id="hiddenchitlookupsid" />
<input type="hidden" name="hiddenintrestmodeid" id="hiddenintrestmodeid" />
<input type="hidden" name="hiddenpurityrate" id="hiddenpurityrate" />
<div id="chitgenerationformadd" class="hidedisplay" style="">
	<div class="large-12 columns paddingzero formheader">
		<?php
		$device = $this->Basefunctions->deviceinfo();
			if($device=='phone') {
				echo '<div class="large-12 columns headercaptionstyle headerradius addformreloadtablet paddingzero">
					<div class="large-6 medium-6 small-6 columns headercaptionleft">
						<span class="gridcaptionpos" style="width: 300px !important;position: relative;"><span>'.$gridtitle['title'].'</span></span>
					</div>
					<div class="large-5 medium-6 small-6 columns addformheadericonstyle addformaction righttext">
					<span class="icon-box" id="chitgenerationclose" ><i class="material-icons" title="Close">close</i></span>
					</div>';
			echo '</div>';
		} else {
			echo '<div class="large-12 columns headercaptionstyle headerradius addformreloadtablet paddingzero">
					<div class="large-6 medium-6 small-6 columns headercaptionleft">
						<span class="gridcaptionpos"><span>'.$gridtitle['title'].'</span></span>
					</div>
					<div class="large-6 medium-6 small-6 columns addformheadericonstyle addformaction righttext">
					<span id="chitgenerationsubmit" class="addbtnclass icon-box"><input id="" name="" tabindex="105" value="Save" class="alertbtnyes  ffield" type="button"></span>	 
						<span id="chitgenerationclose" class="icon-box"><input id="" name="" tabindex="186" value="Close" class="alertbtnno  ffield" type="button"></span>
					</div>';
			echo '</div>';
		}
		?>
		<div class='large-12 columns addformunderheader'>&nbsp;</div>
		<!-- tab group creation -->
		<?php  if($device!='phone') {?>
			<div class="large-12 columns tabgroupstyle desktoptabgroup">
				<ul class="tabs" data-tab="">
					<li id="tab1" class="tab-title sidebaricons active ftabnew" data-subform="1">
						<span class="waves-effect waves-ripple waves-light">Chit Generation</span>
					</li>
				</ul>
			</div>
		<?php } else {?>
			<div class="large-12 columns tabgroupstyle mobiletabgroup">
				<ul class="tabs" data-tab="">
					<li id="tab1"  class="tab-title sidebaricons active ftabnew" data-subform="1">
						<span  class="waves-effect waves-ripple waves-light">Chit Generation</span>
					</li>
				</ul>
			</div>
		<?php }?>
	</div>
	<div class="large-12 columns addformunderheader">&nbsp; </div>
	<div class="large-12 columns addformcontainer padding-space-open-for-form" style="">
		<div class="row mblhidedisplay">&nbsp;</div>
		<form method="POST" name="chitgenerationform" class=""  id="chitgenerationform">
			<div id="addchitgenerationvalidate" class="validationEngineContainer" >
			<?php
				if($device=='phone') {
			?>
				<div id="subformspan1" class="hiddensubform"> 
					<div class="large-4 columns paddingbtm" style="padding:0px !important; padding-left: 10px !important;">	
						<div class="large-12 columns cleardataform" style="padding:0px !important;background:#f2f3fa !important;box-shadow:none;">
							<div class="large-12 columns headerformcaptionstyle" style="background:#f2f3fa !important;">Chit Generation Slip Details</div>
							
			<?php } else { ?>
				<div id="subformspan1" class="hiddensubform"> 
					<div class="large-4 columns paddingbtm" style="padding-left:0px;padding-right: 1.8rem;">	
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
							<div class="large-12 columns headerformcaptionstyle" >Chit Generation Slip Details</div>
			<?php } ?>
							<div class="static-field large-6 columns hidedisplay">
								<label>Date<span class="mandatoryfildclass">*</span></label>								
								<input id="chitgenerationdate" class="fordatepicicon validate[required]"  type="text" data-dateformater="dd-mm-yy" data-prompt-position="bottomLeft:14,36" readonly="readonly" tabindex="101" name="chitgenerationdate" >
							</div>
							<div class="static-field large-6  medium-4 small-4 columns hidedisplay">
								<label>Sales Person<span class="mandatoryfildclass">*</span></label>						
								<select id="salespersonid" name="salespersonid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="1004">
									<?php
										foreach($employeedata as $key):
									?>
									<option data-name="<?php echo $key->employeename;?>" value="<?php echo $key->employeeid;?>"><?php echo $key->employeename;?></option>
									<?php endforeach;?>						
								</select>
							</div>
							<div class="input-field large-6 columns ">
								<input type="text" class="validate[required,maxSize[100]]" id="chitbookno" name="chitbookno" value="" tabindex="103">
								<label for="chitbookno">Scan Chitbook<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="large-6 columns sectionalertbuttonarea" style="text-align:center">
								<input id="searchchitbookno" type="button" class="alertbtnyes" name="" value="Search" tabindex="" style="margin-right: 7px;">	
							</div>
							<div class="static-field large-12 columns ">
								<label>Account Name</label>						
								<select id="accountid" name="accountid" class="chzn-select dropdownchange" data-prompt-position="topLeft:14,36" tabindex="102">
									<option value=""></option>
								</select>
							</div>
							<div class="input-field large-12 columns ">
								<input type="text" class="validate[maxSize[100]]" id="accountaddress" name="accountaddress" value="" tabindex="103">
								<label for="accountaddress">Address<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="input-field large-6 columns ">
								<input type="text" class="validate[maxSize[100]]" id="accountarea" name="accountarea" value="" tabindex="103">
								<label for="accountarea">Area<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="input-field large-6 columns ">
								<input type="text" class="validate[maxSize[100]]" id="accountpostalcode" name="accountpostalcode" value="" tabindex="103">
								<label for="accountpostalcode">Postal Code<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="input-field large-6 columns ">
								<input type="text" class="validate[maxSize[100]]" id="accountcity" name="accountcity" value="" tabindex="103">
								<label for="accountcity">City<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="static-field large-6 columns ">
								<label>State</label>
								<select id="accountstate" name="accountstate" class="chzn-select dropdownchange" data-prompt-position="topLeft:14,36" tabindex="102">
									<option value=""></option>
								</select>
							</div>
							<div class="static-field large-12 columns ">
								<label>Comments</label>
								<textarea name="accountcomments" class="materialize-textarea validate[maxSize[200]]" id="accountcomments" rows="3" cols="40"></textarea>
							</div>
							<div class="input-field large-6 columns ">
								<input type="text" class="validate[required,custom[number],decval[<?php echo $amount_round;?>],maxSize[100]]" id="totalpaidamt" name="totalpaidamt" value="" tabindex="103">
								<label for="totalpaidamt">Total Paid Amt<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="input-field large-6 columns totalpaidgrms-div">
								<input type="text" class="validate[required,custom[number],decval[<?php echo $weight_round;?>],maxSize[100]]" id="totalpaidgrms" name="totalpaidgrms" value="" tabindex="103">
								<label for="totalpaidgrms">Total Paid Grms<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="static-field large-6  medium-6 small-6 columns" id="chitclosureid_div">
								<label>Chit Closure<span class="mandatoryfildclass">*</span></label>
								<select id="chitclosureid" name="chitclosureid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="1004">
									<?php
										foreach($chitclosure as $key):
									?>
									<option data-name="<?php echo $key->chitclosurename;?>" value="<?php echo $key->chitclosureid;?>"><?php echo $key->chitclosurename;?></option>
									<?php endforeach;?>						
								</select>
							</div>
							<div class="input-field large-6 columns schemeinterestamt-div amountcalculate">
								<input type="text" class="validate[required,custom[number],decval[<?php echo $amount_round;?>],maxSize[100]]" id="schemeinterestamt" name="schemeinterestamt" value="" tabindex="103">
								<label for="schemeinterestamt">Interest Amt<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="input-field large-6 columns refundrateamt-div amountcalculate">
								<input type="text" class="validate[required,custom[number],decval[<?php echo $amount_round;?>],maxSize[100]]" id="refundrateamt" name="refundrateamt" value="" tabindex="103">
								<label for="refundrateamt">Refund Rate<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="input-field large-6 columns schemegiftamt-div amountcalculate">
								<input type="text" class="validate[required,custom[number],decval[<?php echo $amount_round;?>],maxSize[100]]" id="schemegiftamt" name="schemegiftamt" value="" tabindex="103">
								<label for="schemegiftamt">Gift Amount<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="input-field large-6 columns amountcalculate">
								<input type="text" class="validate[required,custom[number],decval[<?php echo $amount_round;?>],maxSize[100]]" id="bonusamount" name="bonusamount" value="" tabindex="103">
								<label for="bonusamount">Bonus Amount<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="input-field large-6 columns amountcalculate" id="adjustmentamount-div">
								<input type="text" class="validate[required,custom[number],decval[<?php echo $amount_round;?>],maxSize[100]]" id="adjustmentamount" name="adjustmentamount" value="" tabindex="103">
								<label for="adjustmentamount">Adjustment Amount<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="input-field large-6 columns ">
								<input type="text" class="validate[required,custom[number],decval[<?php echo $amount_round;?>],maxSize[100]]" id="totalamtclosure" name="totalamtclosure" value="" tabindex="103">
								<label for="totalamtclosure">Amount Closure<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="input-field large-6 columns amountcalculate" id="adjustmentwt-div">
								<input type="text" class="validate[required,custom[number],decval[<?php echo $weight_round;?>],maxSize[100]]" id="adjustmentweight" name="adjustmentweight" value="" tabindex="103">
								<label for="adjustmentweight">Adjustment Weight<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="input-field large-6 columns totalgrmclosure-div">
								<input type="text" class="validate[required,custom[number],decval[<?php echo $weight_round;?>],maxSize[100]]" id="totalgrmclosure" name="totalgrmclosure" value="" tabindex="103">
								<label for="totalgrmclosure">Gram Closure<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="divider large-12 columns"></div>
							<div class="large-6 columns sectionalertbuttonarea" style="text-align:center">
								<input id="submitchitgenerationadd" type="button" class="alertbtnyes" name="" value="Save" tabindex="" style="margin-right: 7px;">	
								<input type="hidden" name="scanchitbookno" id="scanchitbookno" />
								<input type="hidden" name="scan_chitbookid" id="scan_chitbookid" />
								<input type="hidden" name="chitschemeid" id="chitschemeid" />
								<input type ="hidden" id="mainprintemplateid" value="<?php echo $mainprintemplateid; ?>">
								<input type ="hidden" id="hiddenemployeeid" value="<?php echo $employeeid; ?>" >
							</div>
							<!--<div class="large-6 columns sectionalertbuttonarea" style="text-align:center">
								<input id="resetfilter" type="button" class="alertbtnyes" name="" value="Reset" tabindex="" style="margin-right: 7px;">	
							</div>-->
						</div>
					</div>
					<div class="large-8 columns viewgridcolorstyle borderstyle" style="padding-left:0;padding-right:0;top:20px;">	
						<?php
							$device = $this->Basefunctions->deviceinfo();
							if($device=='phone') {
								echo '<div class="large-12 columns paddingzero forgetinggridname" id="chitgenerationinnergridwidth"><div id="chitgenerationinnergrid" class="inner-row-content inner-gridcontent" style="height:81vh !important;">
									<!-- Table header content , data rows & pagination for mobile-->
								</div>
								<!--<footer class="inner-gridfooter footercontainer" id="chitgenerationinnergridfooter">
									 Footer & Pagination content 
								</footer>--></div>';
							} else {
								echo '<div class="large-12 columns forgetinggridname" id="chitgenerationinnergridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="chitgenerationinnergrid" style="max-width:2000px; height:81vh !important;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
								<!--<div class="inner-gridfooter footer-content footercontainer" id="chitgenerationinnergridfooter">
									 Footer & Pagination content 
								</div>--></div>';
							}
							$industryid = $this->Basefunctions->industryid;
							if($industryid == 3) {
								echo hidden('weight_round',$weight_round);
								echo hidden('amount_round',$amount_round);
							}
						?>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<!-- Base Cancel overlay for combined modules -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="basecanceloverlayforcommodule">
		<div class="row sectiontoppadding">&nbsp;</div>
		<div class="overlaybackground">
			<div class="alert-panel">
				<div class="alertmessagearea cancelmessagearea">
				<div class="row">&nbsp;</div>
				<div class="alert-message basedelalerttxt">Are you sure, you want to cancel the record?</div>
				</div>
				<div class="alertbuttonarea">
					<span class="firsttab" tabindex="1000"></span>
					<input type="button" id="basecancelyes" name="" value="Cancel" tabindex="1001" class="alertbtnyes ffield commodyescls">
					<input type="button" id="basecancelno" name="" tabindex="1002" value="Cancel" class="alertbtnno flloop  alertsoverlaybtn" >
					<span class="lasttab" tabindex="1003"></span>
				</div>
				<div class="row">&nbsp;</div>
			</div>
		</div>
	</div>
</div>