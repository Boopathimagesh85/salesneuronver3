<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Chitgenerationmodel extends CI_Model {
    private $chitgenmodule = 273;
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//retrieve printtemplateid
	public function mainprintemplate() {
		$this->db->select('printtemplates.printtemplatesid');
		$this->db->from('printtemplates');
		$this->db->where('printtemplates.moduleid','273');
		$this->db->where('printtemplates.setasdefault','Yes');
		$this->db->where('status',$this->Basefunctions->activestatus);
		$this->db->limit(1);
		$qryinfo = $this->db->get();		
		if($qryinfo->num_rows()>0) {
			$info = $qryinfo->row();
			return $info->printtemplatesid;
		} else {
			return 1;
		}
	}
    //size master inner grid work
    public function viewdynamicdatainfofetch($tablename,$sortcol,$sortord,$pagenum,$rowscount,$chitbookno) {
    	$dataset ='chitentry.chitentryid,chitentry.chitentrydate,chitentry.chitentryno,chitentry.chitbookno,chitscheme.chitschemeid,chitscheme.chitschemename,chitentry.entryamount, chitentry.entryweight,chitentry.totalweight,chitentry.monthentry';
    	$join = ' LEFT JOIN chitscheme ON chitscheme.chitschemeid = chitentry.chitschemeid';
		$where = " WHERE chitentry.chitbookno = '$chitbookno' ";
    	$status = 'AND '.$tablename.'.status='.$this->Basefunctions->activestatus.'';
    	/* pagination */
    	$pageno = $pagenum-1;
    	$start = $pageno * $rowscount;
    	/* query */
    	$result = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.$where.$status .' ORDER BY '.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
    	$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
    	$data = array();
    	$i = 1;
    	foreach($result->result() as $row) {
    		$chitentryid =$row->chitentryid;
    		$chitentrydate =$row->chitentrydate;
    		$chitentryno =$row->chitentryno;
    		$chitbookno =$row->chitbookno;
    		$chitschemeid =$row->chitschemeid;
    		$chitschemename =$row->chitschemename;
    		$entryamount =$row->entryamount;
    		$entryweight =$row->entryweight;
    		$totalweight =$row->totalweight;
    		$monthentry =$row->monthentry;
    		$data[$i]=array('id'=>$chitentryid,$chitentrydate,$chitentryno,$chitbookno,$chitschemeid,$chitschemename,$entryamount,$entryweight,$totalweight,$monthentry);
    		$i++;
    	}
    	$finalresult = array('0'=>$data,'1'=>$page,'2'=>'json');
    	return $finalresult;
    }
	//account information data retrive
    public function accountinformationretrieve() {
    	$chitbookno = $_POST['chitbookno'];
		if($chitbookno != '') {
			$chitbookid = $this->Chitgenerationmodel->retrievechitbookid($chitbookno);
			$accountid = $this->Basefunctions->singlefieldfetch('accountid','chitbookid','chitbook',$chitbookid);
		}
		$this->db->select('account.accountid,account.accountname,account.stateid,accountaddress.address,accountaddress.pincode,accountaddress.city,accountcf.primaryarea,state.statename,account.description');
    	$this->db->from('account');
		$this->db->join('accountaddress','accountaddress.accountid = account.accountid');
		$this->db->join('accountcf','accountcf.accountid = account.accountid');
		$this->db->join('state','account.stateid = state.stateid');
    	$this->db->where('account.accountid',$accountid);
    	$this->db->where('accountaddress.addresstypeid','4');
    	$this->db->where('account.status',$this->Basefunctions->activestatus);
    	$result = $this->db->get();
    	if($result->num_rows() > 0) {
    		foreach($result->result() as $row) {
    			$data = array('accountid'=>$row->accountid,'accountname'=>$row->accountname,'stateid'=>$row->stateid,'address'=>$row->address,'pincode'=>$row->pincode,'city'=>$row->city,'primaryarea'=>$row->primaryarea,'statename'=>$row->statename,'description'=>$row->description);
    		}
    	} else {
    		$data = array('accountid'=>'','accountname'=>'','stateid'=>'','address'=>'','pincode'=>'','city'=>'','primaryarea'=>'','statename'=>'','description'=>'');
    	}
    	echo json_encode($data);
    }
	//retrieve chitbookid
    public function retrievechitbookid($chitbookno) {
		$this->db->select('chitbookid');
		$this->db->from('chitbook');
		$this->db->where('chitbook.chitbookno',$chitbookno);
		$this->db->where('chitbook.chitgeneration','0');
		$this->db->where('chitbook.status',$this->Basefunctions->activestatus);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = $row->chitbookid;
			}
		} else {
			$data = '';
		}
		return $data;
	}
	//chitbook status
	public function chitbookdetails() {
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$interestrate_amount = 0;
		$refundrate_amount = 0;
		$chitbookno = $_POST['chitbookno'];
		if($chitbookno != '') {
			$chitbookid = $this->Chitgenerationmodel->retrievechitbookid($chitbookno);
			if($chitbookid != '') {
				$chitschemeid = $this->Basefunctions->singlefieldfetch('chitschemeid','chitbookid','chitbook',$chitbookid);
				$chitclosestatus = $this->Basefunctions->singlefieldfetch('chitclosed','chitbookid','chitbook',$chitbookid);
				$interestratemode = $this->Basefunctions->get_company_settings('chitinteresttype');
				$this->db->select('ROUND(SUM(chitentry.entryamount),'.$amountround.') as totentryamount, ROUND(SUM(chitentry.entryweight),'.$round.') as totentryweight, ROUND(chitscheme.interestrate,'.$round.') as interestrate,ROUND(chitscheme.refundrate,'.$round.') as refundrate, chitscheme.chitlookupsid, chitscheme.intrestmodeid, chitscheme.gift, chitscheme.chitschemeid, chitscheme.purityid');
				$this->db->from('chitentry');
				$this->db->join('chitscheme','chitscheme.chitschemeid = chitentry.chitschemeid');
				$this->db->where('chitentry.chitbookno',$chitbookno);
				$this->db->where('chitentry.status',$this->Basefunctions->activestatus);
				$result = $this->db->get();
				if($result->num_rows() > 0) {
					foreach($result->result() as $row) {
						if($interestratemode == 0) {
							$interestrate_amount = $row->totentryamount * ($row->interestrate/100);
							$refundrate_amount = $row->totentryamount * ($row->refundrate/100);
						} else {
							$interestrate_amount = $row->interestrate;
							$refundrate_amount = $row->refundrate;
						}
						if($row->gift == 'YES') {
							$schemegiftamt = $this->retrieveschemegiftamount($row->chitschemeid,$chitbookno);
						} else {
							$schemegiftamt = 0;
						}
						$todaypurityrate = $this->todaypurityrate($row->purityid);
						$data = array('chitbookid'=>$chitbookid,'chitclosestatus'=>$chitclosestatus,'totentryamount'=>$row->totentryamount,'totentryweight'=>$row->totentryweight,'interestrate'=>$row->interestrate,'interestratemode'=>$interestratemode,'interestrate_amount'=>$interestrate_amount,'chitlookupsid'=>$row->chitlookupsid,'intrestmodeid'=>$row->intrestmodeid,'schemegift'=>$row->gift,'schemegiftamt'=>$schemegiftamt,'chitschemeid'=>$row->chitschemeid,'refundrateamt'=>$row->refundrate,'purityid'=>$row->purityid,'purityrate'=>$todaypurityrate);
					}
				}
			} else {
				$data = array('chitbookid'=>'','chitclosestatus'=>'','totentryamount'=>'','totentryweight'=>'','interestrate'=>'','interestratemode'=>'','interestrate_amount'=>'','chitlookupsid'=>'','intrestmodeid'=>'','schemegift'=>'','schemegiftamt'=>'','chitschemeid'=>'','refundrateamt'=>'','purityid'=>'','purityrate'=>'');
			}
		} else {
			$data = array('chitbookid'=>'','chitclosestatus'=>'','totentryamount'=>'','totentryweight'=>'','interestrate'=>'','interestratemode'=>'','interestrate_amount'=>'','chitlookupsid'=>'','intrestmodeid'=>'','schemegift'=>'','schemegiftamt'=>'','chitschemeid'=>'','refundrateamt'=>'','purityid'=>'','purityrate'=>'');
		}
		echo json_encode($data);
	}
	// Scheme Gift Amount if the scheme is yes
    public function retrieveschemegiftamount($chitschemeid,$chitbookno) {
		$totamount = 0;
		$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$giftissued = $this->Basefunctions->singlefieldfetch('giftamount','chitbookno','prizeissue',$chitbookno);
		$schemeamount = $this->Basefunctions->singlefieldfetch('amount','chitbookno','chitbook',$chitbookno);
		if($giftissued == 1) {
			$giftissued = 0;
		} else {
			$giftissued = $giftissued;
		}
		$this->db->select('chitgift.chitschemeid,chitgift.chitschemeid,ROUND(SUM(chitgift.schemeamount),'.$amountround.') as totschemeamount, ROUND(SUM(chitgift.giftamount),'.$amountround.') as totgiftamount, chitgift.giftdescription');
		$this->db->from('chitgift');
		$this->db->where_in('chitgift.chitschemeid',$chitschemeid);
		$this->db->where('chitgift.chitlookupsid',4);
		$this->db->where('chitgift.schemeamount',$schemeamount);
		$this->db->where('chitgift.status',$this->Basefunctions->activestatus);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$totamount = $totamount + $row->totgiftamount;
			}
			if($giftissued > 0) {
				$totamount = ROUND(($totamount - $giftissued),$amountround);
			} else {
				$totamount = ROUND($totamount,$amountround);
			}
		}
		return $totamount;
	}
    // Chit Generation insert
    public function chitgenerationcreatemodel() {
		$chitgenserialrow = $this->Basefunctions->singlefieldfetch('moduleid','moduleid','varserialnumbermaster',$this->chitgenmodule);
		if($chitgenserialrow == 1){
			$array = array(
					'salesid'=>'',
					'status'=>'FAIL',
					'salesnumber'=>''
				);
		} else {
			if(empty($_POST['accountid']) || empty($_POST['scan_chitbookid'])) {
				$array = array(
					'salesid'=>'',
					'status'=>'FAIL',
					'salesnumber'=>''
				);
			} else {
				$serialnumber = $this->Basefunctions->varrandomnumbergenerator($this->chitgenmodule,'chitgenerationnumber',1,'');
				if(empty($_POST['accountid'])){
					$_POST['accountid'] = 1;
				}else{
					$_POST['accountid'] = $_POST['accountid'];
				}
				if(empty($_POST['chitclosureid'])){
					$_POST['chitclosureid'] = 1;
				}else{
					$_POST['chitclosureid'] = $_POST['chitclosureid'];
				}
				$chitgeneration_create = array(
					'chitgenerationnumber'=>$serialnumber,
					'chitgenerationdate'=>$this->Basefunctions->ymd_format($_POST['chitgenerationdate']),
					'accountid'=>$_POST['accountid'],
					'employeeid'=>$_POST['salespersonid'],
					'chitbookid'=>$_POST['scan_chitbookid'],
					'chitbookno'=>$_POST['scanchitbookno'],
					'chitschemeid'=>$_POST['chitschemeid'],
					'chitclosureid'=>$_POST['chitclosureid'],
					'totalpaidamt'=>$_POST['totalpaidamt'],
					'totalpaidgrms'=>$_POST['totalpaidgrms'],
					'schemeinterestamt'=>$_POST['schemeinterestamt'],
					'schemerefundrateamt'=>$_POST['refundrateamt'],
					'schemegiftamt'=>$_POST['schemegiftamt'],
					'totalbonusamt'=>$_POST['bonusamount'],
					'adjustmentamt'=>$_POST['adjustmentamount'],
					'adjustmentwt'=>$_POST['adjustmentweight'],
					'purityrate'=>$_POST['purityrate'],
					'totalamtclosure'=>$_POST['totalamtclosure'],
					'totalgrmclosure'=>$_POST['totalgrmclosure'],
					'industryid'=>$this->Basefunctions->industryid,
					'branchid'=>$this->Basefunctions->branchid
				);
				$insertarray = array_merge($chitgeneration_create,$this->Crudmodel->defaultvalueget());
				$this->db->insert('chitgeneration',array_filter($insertarray));
				$salesid = $this->db->insert_id();
				$update = array(
					'chitgeneration'=>'1',
				);
				$update = array_merge($update,$this->Crudmodel->updatedefaultvalueget());
				$this->db->where('chitbookid',$_POST['scan_chitbookid']);
				$this->db->update('chitbook',$update);
				$array = array(
					'salesid'=>$salesid,
					'status'=>'SUCCESS',
					'salesnumber'=>$serialnumber	
				);
				{//workflow management -- for data create
					$this->Basefunctions->workflowmanageindatacreatemode('273',$salesid,'chitgeneration');
				}
			}
		}
		echo json_encode($array);
    }
	// Chit Generation delete
    public function chitgenerationdeletemodel() {
    	$primaryid = $_POST['primaryid'];
		$scan_chitbookid = $this->Basefunctions->singlefieldfetch('chitbookid','chitgenerationid','chitgeneration',$primaryid);
    	$delete = $this->Crudmodel->deletedefaultvalueget();
    	$this->db->where('chitgeneration.chitgenerationid',$primaryid);
    	$this->db->update('chitgeneration',$delete);
		$update = array(
			'chitgeneration'=>'0',
		);
		$update = array_merge($update,$this->Crudmodel->updatedefaultvalueget());
		$this->db->where('chitbookid',$scan_chitbookid);
		$this->db->update('chitbook',$update);
    	echo 'Success';
    }
	// Chit Generation cancel
    public function chitgenerationcancelmodel() {
    	$primaryid = $_POST['primaryid'];
		$scan_chitbookid = $this->Basefunctions->singlefieldfetch('chitbookid','chitgenerationid','chitgeneration',$primaryid);
    	$delete = $this->Crudmodel->deletedefaultvalueget();
    	$this->db->where('chitgeneration.chitgenerationid',$primaryid);
    	$this->db->update('chitgeneration',$delete);
		$update = array(
			'chitgeneration'=>'0',
		);
		$update = array_merge($update,$this->Crudmodel->updatedefaultvalueget());
		$this->db->where('chitbookid',$scan_chitbookid);
		$this->db->update('chitbook',$update);
    	echo 'Success';
    }
	// To display image in overlay after searched chitbook information
    public function retrieveaccountimagedetails() {
    	$accountid = $_POST['accountid'];
		$this->db->select('account.accountlogo_path,account.signaturepad_path');
    	$this->db->from('account');
    	$this->db->where('account.accountid',$accountid);
    	$this->db->where('account.status',$this->Basefunctions->activestatus);
    	$result = $this->db->get();
    	if($result->num_rows() > 0) {
    		foreach($result->result() as $row) {
				$imagedetails = $row->accountlogo_path.','.$row->signaturepad_path;
				$imagedetails = trim($imagedetails,",");
    			$data = array('imgdetails'=>$imagedetails);
    		}
    	} else {
    		$data = array('fail'=>'FAILED');
    	}
    	echo json_encode($data);
    }
	// Get today purity rate
    public function todaypurityrate($purityid) {
		$data = 0;
		$this->db->select('currentrate');
		$this->db->from('rate');
		$this->db->where('rate.purityid',$purityid);
		$this->db->where('rate.status',1);
		$this->db->order_by('rate.rateid','desc');
		$this->db->limit(1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = $row->currentrate;
			}
		} 
		return $data;
	}
}