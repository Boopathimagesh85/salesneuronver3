<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Chitgeneration extends MX_Controller {
    public function __construct(){
        parent::__construct();
		$this->load->helper('formbuild'); 
		$this->load->model('Chitgenerationmodel');
		$this->load->model('Base/Basefunctions');
    }  
    public function index() {
		$moduleid = array(273);
		sessionchecker($moduleid);
		//action		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['account'] = $this->Basefunctions->accountgroup_dd('16');
		$data['weight_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$data['amount_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$data['employeedata'] = $this->Basefunctions->simpledropdownwithcond('employeeid','employeename,employeenumber','employee','branchid',$this->Basefunctions->branchid);
		$data['chitclosure'] = $this->Basefunctions->simpledropdownwithcond('chitclosureid','chitclosurename','chitclosure','moduleid',273);
		$userid = $this->Basefunctions->userid;
		$data['employeeid'] = $userid;
		$data['mainprintemplateid'] = $this->Chitgenerationmodel->mainprintemplate();
		$this->load->view('chitgenerationcreationview',$data);
	}
	/* Fetch Main grid header,data,footer information */
	public function gridinformationfetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$chitbookno = $_GET['chitbookno'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'chitentry.chitentryid') : 'chitentry.chitentryid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colid'=>array('1','1'),'colname'=>array('ChitEntry Date','Chitentry No','Chitbook No','Chit Scheme','Chit Scheme','Entry Amount','Entry Weight','Total Weight','Month Entry'),'colicon'=>array('','','','','','','','',''),'colsize'=>array('200','200','200','200','200','200','200','200','200'),'colmodelname'=>array('chitentrydate','chitentryno','chitbookno','chitschemeid','chitschemename','entryamount','entryweight','totalweight','monthentry'),'colmodelindex'=>array('chitentry.chitentrydate','chitentry.chitentryno','chitentry.chitbookno','chitscheme.chitschemeid','chitscheme.chitschemename','chitentry.entryamount','chitentry.entryweight','chitentry.totalweight','chitentry.monthentry'),'coltablename'=>array('chitentry','chitentry','chitentry','chitscheme','chitscheme','chitentry','chitentry','chitentry','chitentry'),'uitype'=>array('2','2','2','4','2','2','2','2','2'),'modname'=>array('chitentry','chitentry','chitentry','chitscheme','chitscheme','chitentry','chitentry','chitentry','chitentry'));
		$result=$this->Chitgenerationmodel->viewdynamicdatainfofetch($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$chitbookno);
		$device = $this->Basefunctions->deviceinfo();
	//	if($device=='phone') {
		//	$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'sizemasterinner',$width,$height);
		//} else {
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'sizemasterinner',$width,$height,'');
	//	}
		echo json_encode($datas);
	}
	//account information retrieve
	public function accountinformationretrieve() {
		$this->Chitgenerationmodel->accountinformationretrieve();
	}
	//chitbook details
	public function chitbookdetails() {
		$this->Chitgenerationmodel->chitbookdetails();
	}
	//chitgeneration insert
	public function chitgenerationcreate() {
		$this->Chitgenerationmodel->chitgenerationcreatemodel();
	}
	//chitgeneration delete
	public function chitgenerationdelete() {
		$this->Chitgenerationmodel->chitgenerationdeletemodel();
	}
	// Retrieve Account image / logo and signature images
	public function retrieveaccountimagedetails() {
		$this->Chitgenerationmodel->retrieveaccountimagedetails();
	}
	// chitgeneration cancel
	public function chitgenerationcancel() {
		$this->Chitgenerationmodel->chitgenerationcancelmodel();
	}
}