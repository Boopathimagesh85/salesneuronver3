<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class sms extends MX_Controller{
	// constructor function 
    public function __construct() {
        parent::__construct();
        $this->load->helper('formbuild');
		$this->load->model('Sms/Smsmodel');
		$this->load->view('Base/formfieldgeneration');
		$this->load->library('convert');
    }
	public function index() {
		$moduleid = array(210);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(210);
		$viewmoduleid = array(210);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Sms/smsview',$data);
	}
	//fetch sms template msg
	public function fetchtemplatedetails() {
		$this->Smsmodel->fetchtemplatedetailsmodel();
	}
	//sms merge template content information
	public function smsmergcontinformationfetch() {
		$this->Smsmodel->smsprintfileinfofetch();
	}
	// sms send
	public function leadsmssend() {
		$this->Smsmodel->leadsmssendmodel();
	}
	//editor value fetch 	
	public function editervaluefetch() {
		$filename = $_GET['filename']; 
		$newfilename = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]);
			echo json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			echo json_encode($tccontent);
		}
	}
	//sms signature value fetch
	public function smssignatureddvalfetch() {
		$this->Smsmodel->smssignatureddvalfetchmodel();
	}
	//sms record value fetch
	public function smsrecordddvalfetch() {
		$this->Smsmodel->smsrecordddvalfetchmodel();
	}
	//sms sender id value fetch
	public function smssenderddvalfetch() {
		$this->Smsmodel->smssenderddvalfetchmodel();
	}
	//schedule date time validation
	public function scheduledatetimevalidation() {
		$this->Smsmodel->scheduledatetimevalidationmodel();
	}
	//Default sender id value fetch
	public function defaultsenderidget() {
		$this->Smsmodel->defaultsenderidgetmodel();
	}
	// dnd mobile number check
	public function dndmobilenumbercheck() {
		$this->Smsmodel->dndmobilenumbercheckmodel();
	}
	//mobile number based on record id
	public function mobilenumberbasedonrecordid() {
		$this->Smsmodel->mobilenumberbasedonrecordidmodel();
	}
	//For template value fetch
	public function smstemplateddvalfetch() {
		$this->Smsmodel->smstemplateddvalfetchmodel();
	}
	//schedule delete
	public function scheduledelete() {
		$this->Smsmodel->scheduledeletemodel();
	}
	//schedule edit
	public function scheduleedit() {
		$this->Smsmodel->scheduleeditmodel();
	}
	//resend sms data fetch
	public function resendsmsdatavalget() {
		$this->Smsmodel->resendsmsdatavalgetmodel();
	}
	//resend sms data fetch
	public function smsrecordpull() {
		$this->Smsmodel->smsrecordpullvalgetmodel();
	}
	//schedule chec
	public function scheduleeditvaluecheck() {
		$this->Smsmodel->scheduleeditvaluecheckmodel();
	}
	public function smstemplateddget() {
		$this->Smsmodel->smstemplateddgetmodel();
	}
}