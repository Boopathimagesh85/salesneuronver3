<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//To Perform Insert Update Read and Delete Operations
class Smsmodel extends CI_Model{    
    function __construct() {
    	parent::__construct(); 
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
	}
	//template msg details
	public function fetchtemplatedetailsmodel() {
		$templateid = $_GET['templid'];
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('templates.leadtemplatecontent_editorfilename,templates.moduleid',false);
		$this->db->from('templates');
		$this->db->where('templates.Status',1);
		$this->db->where("FIND_IN_SET('$industryid',templates.industryid) >", 0);
		if($templateid != '') {
			$this->db->where('templates.templatesid',$templateid);
		}
		$result = $this->db->get();
		if($result->num_rows() >0) {		
			foreach($result->result()as $row) {
				$data=array('msg'=>$row->leadtemplatecontent_editorfilename,'moduleid'=>$row->moduleid);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	public function leadsmssendmodel_test() { // Not using this function
		/* $output = '{"status":"OK","data":[{"id":"fc37c8d9-40f1-4fc5-808d-24d873afe3db:1","mobile":"9790831180","status":"DELIVRD","senttime":"2019-06-26 15:13:35","dlrtime":"2019-06-26 15:13:38","custom":"SNJEWE","custom1":null,"custom2":null}],"message":"Processed Successfully"}';
		$someArray = json_decode($output, true);
		print_r($someArray['data'][0]['mobile']); die();
		print_r($smsstatus); die();
		$apikey = 'Abcf421aeae99c696593c886ef495572b';
		$crediturl = "https://api-alerts.kaleyra.com/v4/?api_key=$apikey&method=account.credits";
		$crdurl=curl_init();
		curl_setopt($crdurl, CURLOPT_URL, $crediturl);
		curl_setopt($crdurl, CURLOPT_RETURNTRANSFER, true);
		$creditoutput = curl_exec($crdurl);
		curl_close($crdurl);
		$jsoncreditresult = json_decode($creditoutput, true);
		foreach($jsoncreditresult['data'] as $key => $value) {
			$creditremaining = $value;
			print_r($creditremaining); die();
			$credit = $this->db->query("UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = $creditremaining WHERE `addonstypeid` = $addontypeid");
		} */
	}
	public function smsprintfileinfofetch() {
		$tempid = $_POST['tempid'];
		$moduleid = $_POST['moduleid'];
		$parenttable = $_POST['parenttablename'];
		$recordid = $_POST['recordid'];
		$mergerid = explode(',',$recordid);
		$mergetemp = $_POST['content'];
		$mergegrp = explode(',',$mergetemp);
		$primaryset = $parenttable.'.'.$parenttable.'id';
		for($i=0;$i<count($mergerid);$i++) {
			$print_data=array('templateid'=>$tempid,'Templatetype'=>'Printtemp','primaryset'=>$primaryset,'primaryid'=>$mergerid[$i]);
			$resultset[$i] = $this->smsgenerateprinthtmlfile($print_data,$mergetemp,$moduleid);
		}
		echo json_encode($resultset);
	}
	//preview and print pdf
	public function smsgenerateprinthtmlfile($print_data,$mergetemp,$moduleid) {
		$this->load->model('Printtemplates/Printtemplatesmodel');
		//defaults
		$printdetails['moduleid'] = '1';
		$htmldatacontents='';
		$industryid = $this->Basefunctions->industryid;
		$templateid = ( (isset($print_data['templateid']))? $print_data['templateid'] : 1);
		$id = ( (isset($print_data['primaryid']))? $print_data['primaryid'] : 1);
		$this->db->select('templates.leadtemplatecontent_editorfilename,templates.templatesid,templates.moduleid,templates.templatesname');
		$this->db->from('templates');
		$this->db->where('templates.templatesid',$templateid);
		$this->db->where("FIND_IN_SET('$industryid',templates.industryid) >", 0);
		$result = $this->db->get();
		foreach($result->result() as $rowdata) {
			$printdetails['contentfile'] =  $rowdata->leadtemplatecontent_editorfilename;
			$printdetails['moduleid'] =  $rowdata->moduleid;
			$printdetails['templatename'] = $rowdata->templatesname;
		}
		$parenttable = $this->Basefunctions->generalinformaion('module','modulemastertable','moduleid',$printdetails['moduleid']);
		//body data
		$bodycontent = $this->Printtemplatesmodel->filecontentfetch($printdetails['contentfile']);
		$bodycontent = preg_replace('~a10s~','&nbsp;', $bodycontent);
		$bodycontent = preg_replace('~<br>~','<br />', $bodycontent);
		$bodycontent = $this->Printtemplatesmodel->removespecialchars($bodycontent);
		$bodyhtml = $this->Printtemplatesmodel->generateprintingdata($bodycontent,$parenttable,$print_data,$printdetails['moduleid']);
		$bodyhtml = $this->Printtemplatesmodel->addspecialchars($bodyhtml);
		return $bodyhtml;
	}
	//send sms model - KALERA
	public function leadsmssendmodel() {
		$schdate="";
		$msgsenddate = "";
		$custmobile=$_POST['communicationto'];
		$message = $_POST['defaultsubseditorval'];
		$smscount=$_POST['smscount'];
		$datacontent = $_POST['datacontent'];
		$apikey = $_POST['apikey'];
		if($datacontent != 'null') {
			$mobnum = explode(',',$custmobile);
			$mobmessage = explode('|||',$message);
			$mobcount = count($mobnum);
			$smscount = $smscount * $mobcount;
		} else {
			//$mobnum = array($custmobile);
			$mobmessage = $message;
			//$mobcount = '1';
			$mobnum = explode(',',$custmobile);
			$mobcount = count($mobnum);
			$smscount = $smscount * $mobcount;
		}
		//Sender Name
		if(isset($_POST['senderid'])){
			$senderid = $_POST['senderid']; 
			if($senderid == '') { 
				$senderid = '1';
			}
		} else { 
			$senderid = '1';
		}
		if($senderid != 1) {
			$provid = $this->Basefunctions->generalinformaion('smssettings','smsprovidersettingsid','smssettingsid',$senderid);
			$apikey = $this->Basefunctions->generalinformaion('smsprovidersettings','apikey','smsprovidersettingsid',$provid);
		}
		//Sender Settings id
		if(isset($_POST['sendername'])) { 
			$sendername = $_POST['sendername']; 
		} else { 
			$sendername = '';
		}
		//sms send type 
		$smssendtype = $_POST['smssendtypeid'];
		if($smssendtype == '2') {
			$xmlurl = "https://api-alerts.kaleyra.com/v4/?api_key=";
			$newstatusurl = "https://api-alerts.kaleyra.com/v4/?api_key=";
			$addontypeid = '2';
		} else if($smssendtype == '3') {
			$xmlurl = "https://api-promo.kaleyra.com/v4/?api_key=";
			$newstatusurl = "https://api-promo.kaleyra.com/v4/?api_key=";
			//$apikey = 'Acb930e4fa7792f62b7b510d5245ce266'; // Definitely need to get API key for Promotional SMS
			$apikey = 'Acb930e4fa7792f62b7b510d8945ce288';
			$sendername = 'BULKSMS';
			$addontypeid = '4';
		} else if($smssendtype == '4') {
			$xmlurl = "https://api-alerts.kaleyra.com/v4/?api_key=$apikey&method=sms.xml&xml=";
			$newstatusurl = "https://api-alerts.kaleyra.com/v4/?api_key=$apikey&method=sms.status&id=";
			$apikey = 'A1363f16118616942397968b9a9f736da';
			$sendername = 'AUCGLO';
			$addontypeid = '5';
		}
		$credits = $this->accountcresitdetailsfetchmodel($apikey,$addontypeid);
		if( $credits >= $smscount ) {
			$msg=$_POST['description'];
			//sms type id
			if(isset($_POST['smstypeid'])) {
				$type = $_POST['smstypeid']; 
			} else { 
				$type = 2; 
			}
			if($type == 2 ) { 
				$unicode = 'N'; 
				$flash = 'N';
			} else if($type == 3 ) { 
				$unicode = 'N'; 
				$flash = 'Y';
			} else if($type == 4 ) { 
				$unicode = 'Y'; 
				$flash = 'N';
			}
			//Record id
			if(isset($_POST['recordsid'])) {
				$rid = $_POST['recordsid']; 
				if($rid == '') { 
					$recordid = 1;
				} else { 
					$recordid = 1;
				}
			} else { 
				$recordid = 1;
			}
			//templateid
			if(isset($_POST['leadtemplateid'])){
				$leadtemplateid = $_POST['leadtemplateid']; 
				if($leadtemplateid == '') { 
					$leadtemplateid = 1;
				}
			} else { 
				$leadtemplateid = 1;
			}
			//Signature id
			if(isset($_POST['signatureid'])){
				$signatureid = $_POST['signatureid']; 
				if($signatureid == '') { 
					$signatureid = 1;
				}
			} else { 
				$signatureid = 1;
			}
			$date = $_POST['communicationdate'];
			$time = $_POST['communicationtime'];
			if($date == '' && $time == '') { 
				$schedule = 'No';
			} else {
				$schedule = 'Yes';
			}
			if($date!='') {
				$schtimestamp = strtotime($date);
				$schdate = date("Y-m-d", $schtimestamp);
				$tosplitmins = explode(':',$time);
				$roundofhr = $tosplitmins[0];
				if ($tosplitmins[1] <= 15) {
					$roundofmin = '15';
				}
				if ($tosplitmins[1] >= 16 && $tosplitmins[1] <=30) {
					$roundofmin = '30';
				}
				if ($tosplitmins[1] >= 31 && $tosplitmins[1] <=45) {
					$roundofmin = '45';
				}
				if ($tosplitmins[1] >= 46 && $tosplitmins[1] <=60) {
					$roundofmin = '00'; 
					$roundofhr = $tosplitmins[0] + 1;
				}
				if($roundofhr > '12') { 
					$roundofhr = $roundofhr - 12; $ttt = 'pm';
				} else { 
					$roundofhr = $roundofhr; 
					$ttt = 'am';
				}
				if($roundofhr < '9' ) { 
					$roundofhr = '0'.$roundofhr;
				} else {
					$roundofhr = $roundofhr;
				}
				$commdate = $schdate.' '.$roundofhr.':'.$roundofmin.''.$ttt;
			} else {
				$commdate="";
			}
			if($commdate != '') {
				$communicationmode = 'SCHEDULE SMS';
			} else {
				$communicationmode = 'SENT SMS';
			}
			/**********  KALERA  **************/	
			//sms sending code
			for($i=0;$i<$mobcount;$i++) {
				if(is_array($mobmessage)) {
					$mobmessage = $mobmessage[$i];
				} else {
					$mobmessage = $mobmessage;
				}
				$data='<?xml version="1.0" encoding="UTF-8"?>
				<api>
				<sms>
				<to>'.$mobnum[$i].'</to>
				<message>'.$mobmessage.'</message>
				<msgid>'.$sendername.'</msgid>
				<sender>'.$sendername.'</sender>
				</sms>
				<response>Y</response>
				<unicode>'.$unicode.'</unicode>
				<flash>'.$flash.'</flash>
				<time>'.$commdate.'</time>
				</api>';
				$url = $xmlurl."$apikey&method=sms.xml&xml=".urlencode($data)."&type=json";
				$ch=curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$output=curl_exec($ch);
				curl_close($ch);
									//fetch & update delivery status (Xml way)
									/* $datasets = $this->parse_response($output); Commented by kumaresan */
				//fetch & update delivery status (json way)
				if(!isset($output[0]['errcode'])) {
					sleep(15);
					$json_result = json_decode($output, true);
					foreach($json_result['data'] as $key => $value) {
						$statusurl = $newstatusurl."$apikey&method=sms.status&id=".$value['id']."";
						$chk=curl_init();
						curl_setopt($chk, CURLOPT_URL, $statusurl);
						curl_setopt($chk, CURLOPT_RETURNTRANSFER, true);
						$statusoutput=curl_exec($chk);
						curl_close($chk);
						$status_update = json_decode($statusoutput, true);
						//schedule date & time code
						if(empty($status_update['data'])) {
							$communicationto_update = $mobnum[$i];
							$groupid_update = $value['id'];
							$communicationstatus_update = $value['status'];
						} else {
							$communicationto_update = $status_update['data'][0]['mobile'];
							$groupid_update = $status_update['data'][0]['id'];
							$communicationstatus_update = $status_update['data'][0]['status'];
						}
						//print_r($someArray['data'][0]['mobile']); die();
						//$smsstatus = explode(' ',trim($statusoutput));
						//$salesstatus = $this->statusvalueget($smsstatus[2]);
						//comm log
						$smslog = array( 
							'commonid'=>$recordid,
							'moduleid'=>1,
							'employeeid'=>$this->Basefunctions->userid,
							'smstypeid'=>$type,
							'smssettingsid'=>$senderid,
							'smssendtypeid'=>$smssendtype,
							'templatesid'=>$leadtemplateid,
							'signatureid'=>$signatureid,
							'crmcommunicationmode'=>$communicationmode,
							'communicationfrom'=>0,
							'communicationto'=>$communicationto_update,
							'smscount'=>$smscount,
							'message'=>$mobmessage,
							'groupid'=>$groupid_update,
							'communicationstatus'=>$communicationstatus_update,
							'communicationdate'=>date($this->Basefunctions->datef),
							'industryid'=>$this->Basefunctions->industryid,
							'branchid'=>$this->Basefunctions->branchid,
							'createdate'=>date($this->Basefunctions->datef),
							'lastupdatedate'=>date($this->Basefunctions->datef),
							'createuserid'=>$this->Basefunctions->userid,
							'lastupdateuserid'=>$this->Basefunctions->userid,
							'status'=>$this->Basefunctions->activestatus
						);
						$this->db->insert('crmcommunicationlog',$smslog);
						$primaryid = $this->Basefunctions->maximumid('crmcommunicationlog','crmcommunicationlogid');
						//notification log entry
						$empid = $this->Basefunctions->logemployeeid;
						$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
						$notimsg = $empname." "."Sent a SMS to ".$communicationto_update;
						$this->Basefunctions->notificationcontentadd($primaryid,'SMS',$notimsg,1,210);
						if($smssendtype != 4) {
							if($smssendtype == 2) {
								$crediturl = "https://api-alerts.kaleyra.com/v4/?api_key=$apikey&method=account.credits";
							} else if($smssendtype == 3) {
								$crediturl = "https://api-promo.kaleyra.com/v4/?api_key=$apikey&method=account.credits";
							}
							$crdurl=curl_init();
							curl_setopt($crdurl, CURLOPT_URL, $crediturl);
							curl_setopt($crdurl, CURLOPT_RETURNTRANSFER, true);
							$creditoutput = curl_exec($crdurl);
							curl_close($crdurl);
							$jsoncreditresult = json_decode($creditoutput, true);
							foreach($jsoncreditresult['data'] as $key => $value) {
								if($value == '') {
									$value = '100';
								}
								$credit = $this->db->query("UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = $value WHERE `addonstypeid` = $addontypeid");
							}
							/* *** Locally used but now it s gettting from online *** Kumaresan
							$credit = $this->db->query("UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = `addonavailablecredit` - $smscount WHERE `addonstypeid` = $addontypeid"); */
						}
						//master company id get - master db update
						$mastercompanyid = $this->Basefunctions->generalinformaion('company','mastercompanyid','companyid',2);
						//Master DB update
						$mdbname = $this->db->masterdb;		
					}
				} 
			}
			if(!isset($datasets[0]['errcode'])) {
				echo json_encode('success');
			} else {
				echo json_encode($datasets[0]['desc']);
			}	
		} else {
			echo json_encode('Credits');
		}
	}
	//fetch xml content
	public function parse_response($response) {
		//$xml = new SimpleXMLElement($response,true);
		$xml = array($response);
		$result = array();
		$h=0;
		foreach($xml as $key => $value) {
			if( $key == 'to' || $key == 'id' || $key == 'status' ) {
				$result[$h][(string)$key] = (string)$value;
				if( count($result[$h]) == '3' ) {
					$h++;
				}
			} else if($key == 'errcode' || $key == 'desc') {
				$result[$h][(string)$key] = (string)$value;
				if( count($result[$h]) == '2' ) {
					$h++;
				}
			}
		}
		return $result;
	}
	public function leadsmssendmodel_infinisolution() { // Not using this function
		$schdate="";
		$msgsenddate = "";
		$custmobile=$_GET['communicationto'];
		$message = $_GET['defaultsubseditorval'];
		$smscount=$_GET['smscount'];
		$datacontent = $_GET['datacontent'];
		$apikey = $_GET['apikey'];
		if($datacontent != 'null') {
			$mobnum = explode(',',$custmobile);
			$mobmessage = explode('|||',$message);
			$mobcount = count($mobnum);
			$smscount = $smscount * $mobcount;
		} else {
			$mobnum = array($custmobile);
			$mobmessage = array($message);
			$mobcount = '1';
			$newmobnum = explode(',',$custmobile);
			$newmobcount = count($newmobnum);
			$smscount = $smscount * $newmobcount;
		}
		//Sender Name
		if(isset($_GET['senderid'])){
			$senderid = $_GET['senderid']; 
			if($senderid == '') { 
				$senderid = '1';
			}
		} else { 
			$senderid = '1';
		}
		if($senderid != 1) {
			$provid = $this->Basefunctions->generalinformaion('smssettings','smsprovidersettingsid','smssettingsid',$senderid);
			$apikey = $this->Basefunctions->generalinformaion('smsprovidersettings','apikey','smsprovidersettingsid',$provid);
		}
		//Sender Settings id
		if(isset($_GET['sendername'])) { 
			$sendername = $_GET['sendername']; 
		} else { 
			$sendername = '';
		}
		//sms send type 
		$smssendtype = $_GET['smssendtypeid'];
		if($smssendtype == '2') {
			$xmlurl = "http://alerts.sinfini.com/api/xmlapi.php?data=";
			$newstatusurl = "http://alerts.sinfini.com/api/status.php?workingkey=";
			$addontypeid = '2';
		} else if($smssendtype == '3') {
			$xmlurl = "http://promo.sinfini.com/api/xmlapi.php?data=";
			$newstatusurl = "http://promo.sinfini.com/api/status.php?workingkey=";
			$apikey = 'Ac9bb7caf562bf6bfa7f9aab4c83ffde9';
			$sendername = 'BULKSMS';
			$addontypeid = '4';
		} else if($smssendtype == '4') {
			$xmlurl = "http://global.sinfini.com/api/xmlapi.php?data=";
			$newstatusurl = "http://global.sinfini.com/api/status.php?workingkey=";
			$apikey = 'A1363f16118616942397968b9a9f736da';
			$sendername = 'AUCGLO';
			$addontypeid = '5';
		}
		$credits = $this->accountcresitdetailsfetchmodel($apikey,$addontypeid);
		if( $credits >= $smscount ) {
			$msg=$_GET['description'];
			//sms type id
			if(isset($_GET['smstypeid'])) {
				$type = $_GET['smstypeid']; 
			} else { 
				$type = 2; 
			}
			if($type == 2 ) { 
				$unicode = 'N'; 
				$flash = 'N';
			} else if($type == 3 ) { 
				$unicode = 'N'; 
				$flash = 'Y';
			} else if($type == 4 ) { 
				$unicode = 'Y'; 
				$flash = 'N';
			}
			//Record id
			if(isset($_GET['recordsid'])) {
				$rid = $_GET['recordsid']; 
				if($rid == '') { 
					$recordid = 1;
				} else { 
					$recordid = 1;
				}
			} else { 
				$recordid = 1;
			}
			//templateid
			if(isset($_GET['leadtemplateid'])){
				$leadtemplateid = $_GET['leadtemplateid']; 
				if($leadtemplateid == '') { 
					$leadtemplateid = 1;
				}
			} else { 
				$leadtemplateid = 1;
			}
			//Signature id
			if(isset($_GET['signatureid'])){
				$signatureid = $_GET['signatureid']; 
				if($signatureid == '') { 
					$signatureid = 1;
				}
			} else { 
				$signatureid = 1;
			}
			$date = $_GET['communicationdate'];
			$time = $_GET['communicationtime'];
			if($date == '' && $time == '') { 
				$schedule = 'No';
			} else {
				$schedule = 'Yes';
			}
			if($date!='') {
				$schtimestamp = strtotime($date);
				$schdate = date("Y-m-d", $schtimestamp);
				$tosplitmins = explode(':',$time);
				$roundofhr = $tosplitmins[0];
				if ($tosplitmins[1] <= 15) {
					$roundofmin = '15';
				}
				if ($tosplitmins[1] >= 16 && $tosplitmins[1] <=30) {
					$roundofmin = '30';
				}
				if ($tosplitmins[1] >= 31 && $tosplitmins[1] <=45) {
					$roundofmin = '45';
				}
				if ($tosplitmins[1] >= 46 && $tosplitmins[1] <=60) {
					$roundofmin = '00'; 
					$roundofhr = $tosplitmins[0] + 1;
				}
				if($roundofhr > '12') { 
					$roundofhr = $roundofhr - 12; $ttt = 'pm';
				} else { 
					$roundofhr = $roundofhr; 
					$ttt = 'am';
				}
				if($roundofhr < '9' ) { 
					$roundofhr = '0'.$roundofhr;
				} else {
					$roundofhr = $roundofhr;
				}
				$commdate = $schdate.' '.$roundofhr.':'.$roundofmin.''.$ttt;
			} else {
				$commdate="";
			}
			if($commdate != '') {
				$communicationmode = 'SCHEDULE SMS';
			} else {
				$communicationmode = 'SENT SMS';
			}
			/**********  solution infini  **************/	
			//sms sending code
			for($i=0;$i<$mobcount;$i++) {
				$data='<?xml version="1.0" encoding="UTF-8"?>
				<xmlapi>
				<auth>
				<apikey>'.$apikey.'</apikey>
				</auth>
				<sendSMS>
				<to>'.$mobnum[$i].'</to>
				<text>'.$mobmessage[$i].'</text>
				<msgid>'.$sendername.'</msgid>
				<sender>'.$sendername.'</sender>
				</sendSMS>
				<response>Y</response>
				<unicode>'.$unicode.'</unicode>
				<flash>'.$flash.'</flash>
				<time>'.$commdate.'</time>
				</xmlapi>';
				$url = $xmlurl.urlencode($data)."&type=json";
				$ch=curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$output=curl_exec($ch);
				curl_close($ch);
				//fetch & update delivery status
				$datasets = $this->parse_response($output);
				if(!isset($datasets[0]['errcode'])) {
					sleep(15);
					foreach($datasets as $key => $value) {
						$statusurl = $newstatusurl.$apikey."&messageid=".$value['msgid']."";
						$chk=curl_init();
						curl_setopt($chk, CURLOPT_URL, $statusurl);
						curl_setopt($chk, CURLOPT_RETURNTRANSFER, true);
						$statusoutput=curl_exec($chk);
						curl_close($chk);
						$smsstatus = explode(' ',trim($statusoutput));
						$salesstatus = $this->statusvalueget($smsstatus[2]);
						//comm log
						$smslog = array( 
							'commonid'=>$recordid,
							'moduleid'=>1,
							'employeeid'=>$this->Basefunctions->userid,
							'smstypeid'=>$type,
							'smssettingsid'=>$senderid,
							'smssendtypeid'=>$smssendtype,
							'templatesid'=>$leadtemplateid,
							'signatureid'=>$signatureid,
							'crmcommunicationmode'=>$communicationmode,
							'communicationfrom'=>0,
							'communicationto'=>$smsstatus[1],
							'smscount'=>$smscount,
							'message'=>$mobmessage[$i],
							'groupid'=>$smsstatus[0],
							'communicationstatus'=>$salesstatus,
							'communicationdate'=>date($this->Basefunctions->datef),
							'industryid'=>$this->Basefunctions->industryid,
							'branchid'=>$this->Basefunctions->branchid,
							'createdate'=>date($this->Basefunctions->datef),
							'lastupdatedate'=>date($this->Basefunctions->datef),
							'createuserid'=>$this->Basefunctions->userid,
							'lastupdateuserid'=>$this->Basefunctions->userid,
							'status'=>$this->Basefunctions->activestatus
						);
						$this->db->insert('crmcommunicationlog',$smslog);
						$primaryid = $this->Basefunctions->maximumid('crmcommunicationlog','crmcommunicationlogid');
						//notification log entry
						$empid = $this->Basefunctions->logemployeeid;
						$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
						$notimsg = $empname." "."Sent a SMS to ".$smsstatus[1];
						$this->Basefunctions->notificationcontentadd($primaryid,'SMS',$notimsg,1,210);
						if($smssendtype != 4) {
							$credit = $this->db->query("UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = `addonavailablecredit` - $smscount WHERE `addonstypeid` = $addontypeid");	
						}
						//master company id get - master db update
						$mastercompanyid = $this->Basefunctions->generalinformaion('company','mastercompanyid','companyid',2);
						//Master DB update
						$mdbname = $this->db->masterdb;		
					}
				} 
			}
			if(!isset($datasets[0]['errcode'])) {
				echo json_encode('success');
			} else {
				echo json_encode($datasets[0]['desc']);
			}	
		} else {
			echo json_encode('Credits');
		}
	}
	public function leadsmssendmodel_api() { // Not using this function
		$schdate="";
		$msgsenddate = "";
		$custmobile=$_GET['communicationto'];
		$message = $_GET['defaultsubseditorval'];
		$smscount=$_GET['smscount'];
		$datacontent = $_GET['datacontent'];
		$apikey = $_GET['apikey'];
		if($datacontent != 'null') {
			$mobnum = explode(',',$custmobile);
			$mobmessage = explode('|||',$message);
			$mobcount = count($mobnum);
			$smscount = $smscount * $mobcount;
		} else {
			$mobnum = array($custmobile);
			$mobmessage = array($message);
			$mobcount = '1';
			$newmobnum = explode(',',$custmobile);
			$newmobcount = count($newmobnum);
			$smscount = $smscount * $newmobcount;
		}
		//Sender Name
		if(isset($_GET['senderid'])){
			$senderid = $_GET['senderid']; 
			if($senderid == '') { 
				$senderid = '1';
			}
		} else { 
			$senderid = '1';
		}
		if($senderid != 1) {
			$provid = $this->Basefunctions->generalinformaion('smssettings','smsprovidersettingsid','smssettingsid',$senderid);
			$apikey = $this->Basefunctions->generalinformaion('smsprovidersettings','apikey','smsprovidersettingsid',$provid);
		}
		//Sender Settings id
		if(isset($_GET['sendername'])) { 
			$sendername = $_GET['sendername']; 
		} else { 
			$sendername = '';
		}
		//sms send type 
		$smssendtype = $_GET['smssendtypeid'];
		if($smssendtype == '2') {
			$xmlurl = "http://alerts.sinfini.com/api/xmlapi.php?data=";
			$newstatusurl = "http://alerts.sinfini.com/api/status.php?workingkey=";
			$addontypeid = '2';
		} else if($smssendtype == '3') {
			$xmlurl = "http://promo.sinfini.com/api/xmlapi.php?data=";
			$newstatusurl = "http://promo.sinfini.com/api/status.php?workingkey=";
			$apikey = 'Ac9bb7caf562bf6bfa7f9aab4c83ffde9';
			$sendername = 'BULKSMS';
			$addontypeid = '4';
		} else if($smssendtype == '4') {
			$xmlurl = "http://global.sinfini.com/api/xmlapi.php?data=";
			$newstatusurl = "http://global.sinfini.com/api/status.php?workingkey=";
			$apikey = 'A1363f16118616942397968b9a9f736da';
			$sendername = 'AUCGLO';
			$addontypeid = '5';
		}
		$credits = $this->accountcresitdetailsfetchmodel($apikey,$addontypeid);
		if( $credits >= $smscount ) {
			$msg=$_GET['description'];
			//sms type id
			if(isset($_GET['smstypeid'])) {
				$type = $_GET['smstypeid']; 
			} else { 
				$type = 2; 
			}
			if($type == 2 ) { 
				$unicode = 'N'; 
				$flash = 'N';
			} else if($type == 3 ) { 
				$unicode = 'N'; 
				$flash = 'Y';
			} else if($type == 4 ) { 
				$unicode = 'Y'; 
				$flash = 'N';
			}
			//Record id
			if(isset($_GET['recordsid'])) {
				$rid = $_GET['recordsid']; 
				if($rid == '') { 
					$recordid = 1;
				} else { 
					$recordid = 1;
				}
			} else { 
				$recordid = 1;
			}
			//templateid
			if(isset($_GET['leadtemplateid'])){
				$leadtemplateid = $_GET['leadtemplateid']; 
				if($leadtemplateid == '') { 
					$leadtemplateid = 1;
				}
			} else { 
				$leadtemplateid = 1;
			}
			//Signature id
			if(isset($_GET['signatureid'])){
				$signatureid = $_GET['signatureid']; 
				if($signatureid == '') { 
					$signatureid = 1;
				}
			} else { 
				$signatureid = 1;
			}
			$date = $_GET['communicationdate'];
			$time = $_GET['communicationtime'];
			if($date == '' && $time == '') { 
				$schedule = 'No';
			} else {
				$schedule = 'Yes';
			}
			if($date!='') {
				$schtimestamp = strtotime($date);
				$schdate = date("Y-m-d", $schtimestamp);
				$tosplitmins = explode(':',$time);
				$roundofhr = $tosplitmins[0];
				if ($tosplitmins[1] <= 15) {
					$roundofmin = '15';
				}
				if ($tosplitmins[1] >= 16 && $tosplitmins[1] <=30) {
					$roundofmin = '30';
				}
				if ($tosplitmins[1] >= 31 && $tosplitmins[1] <=45) {
					$roundofmin = '45';
				}
				if ($tosplitmins[1] >= 46 && $tosplitmins[1] <=60) {
					$roundofmin = '00'; 
					$roundofhr = $tosplitmins[0] + 1;
				}
				if($roundofhr > '12') { 
					$roundofhr = $roundofhr - 12; $ttt = 'pm';
				} else { 
					$roundofhr = $roundofhr; 
					$ttt = 'am';
				}
				if($roundofhr < '9' ) { 
					$roundofhr = '0'.$roundofhr;
				} else {
					$roundofhr = $roundofhr;
				}
				$commdate = $schdate.' '.$roundofhr.':'.$roundofmin.''.$ttt;
			} else {
				$commdate="";
			}
			if($commdate != '') {
				$communicationmode = 'SCHEDULE SMS';
			} else {
				$communicationmode = 'SENT SMS';
			}
			/**********  solution infini  **************/	
			//sms sending code
			for($i=0;$i<$mobcount;$i++) {
				$data='<?xml version="1.0" encoding="UTF-8"?>
				<xmlapi>
				<auth>
				<apikey>'.$apikey.'</apikey>
				</auth>
				<sendSMS>
				<to>'.$mobnum[$i].'</to>
				<text>'.$mobmessage[$i].'</text>
				<msgid>'.$sendername.'</msgid>
				<sender>'.$sendername.'</sender>
				</sendSMS>
				<response>Y</response>
				<unicode>'.$unicode.'</unicode>
				<flash>'.$flash.'</flash>
				<time>'.$commdate.'</time>
				</xmlapi>';
				$url = $xmlurl.urlencode($data)."&type=json";
				$ch=curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$output=curl_exec($ch);
				curl_close($ch);
				//fetch & update delivery status
				$datasets = $this->parse_response($output);
				if(!isset($datasets[0]['errcode'])) {
					sleep(15);
					foreach($datasets as $key => $value) {
						$statusurl = $newstatusurl.$apikey."&messageid=".$value['msgid']."";
						$chk=curl_init();
						curl_setopt($chk, CURLOPT_URL, $statusurl);
						curl_setopt($chk, CURLOPT_RETURNTRANSFER, true);
						$statusoutput=curl_exec($chk);
						curl_close($chk);
						$smsstatus = explode(' ',trim($statusoutput));
						$salesstatus = $this->statusvalueget($smsstatus[2]);
						//comm log
						$smslog = array( 
							'commonid'=>$recordid,
							'moduleid'=>1,
							'employeeid'=>$this->Basefunctions->userid,
							'smstypeid'=>$type,
							'smssettingsid'=>$senderid,
							'smssendtypeid'=>$smssendtype,
							'templatesid'=>$leadtemplateid,
							'signatureid'=>$signatureid,
							'crmcommunicationmode'=>$communicationmode,
							'communicationfrom'=>0,
							'communicationto'=>$smsstatus[1],
							'smscount'=>$smscount,
							'message'=>$mobmessage[$i],
							'groupid'=>$smsstatus[0],
							'communicationstatus'=>$salesstatus,
							'communicationdate'=>date($this->Basefunctions->datef),
							'industryid'=>$this->Basefunctions->industryid,
							'branchid'=>$this->Basefunctions->branchid,
							'createdate'=>date($this->Basefunctions->datef),
							'lastupdatedate'=>date($this->Basefunctions->datef),
							'createuserid'=>$this->Basefunctions->userid,
							'lastupdateuserid'=>$this->Basefunctions->userid,
							'status'=>$this->Basefunctions->activestatus
						);
						$this->db->insert('crmcommunicationlog',$smslog);
						$primaryid = $this->Basefunctions->maximumid('crmcommunicationlog','crmcommunicationlogid');
						//notification log entry
						$empid = $this->Basefunctions->logemployeeid;
						$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
						$notimsg = $empname." "."Sent a SMS to ".$smsstatus[1];
						$this->Basefunctions->notificationcontentadd($primaryid,'SMS',$notimsg,1,210);
						if($smssendtype != 4) {
							$credit = $this->db->query("UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = `addonavailablecredit` - $smscount WHERE `addonstypeid` = $addontypeid");	
						}
						//master company id get - master db update
						$mastercompanyid = $this->Basefunctions->generalinformaion('company','mastercompanyid','companyid',2);
						//Master DB update
						$mdbname = $this->db->masterdb;		
					}
				} 
			}
			if(!isset($datasets[0]['errcode'])) {
				return 'success';
			} else {
				return $datasets[0]['desc'];
			}	
		} else {
			return 'Credits';
		}
	}
	//inline user data base set
	public function inlineuserdatabaseactive($databasename) {
		$host = $this->db->hostname;
		$user = $this->db->username;
		$passwd = $this->db->password;
		$dbconfig['hostname'] = $host;
		$dbconfig['username'] = $user;
		$dbconfig['password'] = $passwd;
		$dbconfig['database'] = $databasename;
		$dbconfig['dbdriver'] = 'mysql';
		$dbconfig['dbprefix'] = '';
		$dbconfig['pconnect'] = TRUE;
		$dbconfig['db_debug'] = TRUE;
		$dbconfig['cache_on'] = FALSE;
		$dbconfig['cachedir'] = '';
		$dbconfig['char_set'] = 'utf8';
		$dbconfig['dbcollat'] = 'utf8_general_ci';
		$dbconfig['swap_pre'] = '';
		$dbconfig['autoinit'] = TRUE;
		$dbconfig['stricton'] = FALSE;
		return $import_db = $this->load->database($dbconfig, TRUE);
	}
	//sms signature drop down value fetch
	public function smssignatureddvalfetchmodel() {
		$i=0;
		$industryid = $this->Basefunctions->industryid;
		$userid = $this->Basefunctions->userid;
		$this->db->select('signatureid,signaturename,smssignature');
		$this->db->from('signature');
		$this->db->where('signature.employeeid',$userid);
		$this->db->where("FIND_IN_SET('$industryid',signature.industryid) >", 0);
		$this->db->where('signature.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data[$i] = array('datasid'=>$row->signatureid,'dataname'=>$row->signaturename,'smssig'=>$row->smssignature);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//sms Record Value fetch
	public function smsrecordddvalfetchmodel() {
		$i=0;
		$moduleid = $_GET['mid'];
		$tabname = $this->moduletablenamefetch($moduleid);
		$fieldid = $tabname.'id';
		if($moduleid == '216' || $moduleid == '217' || $moduleid == '226' || $moduleid == '225' || $moduleid == '227' || $moduleid == '229' || $moduleid == '249') {
			$fieldname = $tabname.'number';
		}else{
			$fieldname = $tabname.'name';
		}
		$userid = $this->Basefunctions->userid;
		if($moduleid == '201' || $moduleid == '203' || $moduleid == '4') {
			$this->db->select($fieldid.','.$fieldname.',CONCAT(salutation.salutationname, '.$fieldname.', lastname) AS name', FALSE);
			$this->db->from($tabname);
			$this->db->join('salutation','salutation.salutationid='.$tabname.'.salutationid','left outer');
		} else {
			$this->db->select($fieldname.' AS name ,'.$fieldid.','.$fieldname);
			$this->db->from($tabname);
		}
		$this->db->where("$tabname".'.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data[$i] = array('datasid'=>$row->$fieldid,'dataname'=>$row->name,'modulename'=>$tabname);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//module table name get
	public function moduletablenamefetch($moduleid) {
		$i=0;
		$this->db->select('modulemastertable');
		$this->db->from('module');
		$this->db->where('module.moduleid',$moduleid);
		$this->db->where('module.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data= $row->modulemastertable;
			}
		}else{ $data = ''; }
		return $data;
	}
	//sender id drop down value fetch function
	public function smssenderddvalfetchmodel() {
		$typeid = $_GET['typeid'];
		$i=0;
		$userid = $this->Basefunctions->userid;
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('smssettings.smssettingsid,smssettings.senderid,smssettings.apikey as settingapikey,smsprovidersettings.smssendtypeid');
		$this->db->from('smsprovidersettings');
		$this->db->join('smssettings','smssettings.smsprovidersettingsid=smsprovidersettings.smsprovidersettingsid');
		$this->db->where_in('smsprovidersettings.smssendtypeid',$typeid);
		$this->db->where("FIND_IN_SET('$industryid',smssettings.industryid) >", 0);
		$this->db->where('smssettings.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data[$i] = array('datasid'=>$row->smssettingsid,'dataname'=>$row->senderid,'apikey'=>$row->settingapikey);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//schedule date time validation
	public function scheduledatetimevalidationmodel() {
		$date = $_GET['sdate'];
		$time = $_GET['stime'];
		//current time 
		$curdate = date("Y-m-d h:i:s");
		$splitdate = explode(' ',$curdate);
		$splittime = explode(':',$splitdate[1]);
		$selectedTime = $splitdate[1];
		$endTime = strtotime("-15 minutes", strtotime($selectedTime));
		echo date('h:i:s', $endTime);
	}
	//default sender id value fetch
	public function defaultsenderidgetmodel() {
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('smssettingsid');
		$this->db->from('smssettings');
		$this->db->where('smssettings.setdefault','Yes');
		$this->db->where("FIND_IN_SET('$industryid',smssettings.industryid) >", 0);
		$this->db->where('smssettings.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data = array('senderid'=>$row->smssettingsid);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//dnd mobile number check
	public function dndmobilenumbercheckmodel() {
		$mobilenum = $_GET['mobilenum'];
		$apikeyandsenderid = $this->defapikeyandsenderidget();
		$apikey = $apikeyandsenderid['apikey'];
		$statusurl = "http://qfilter.sinfini.com/api/v1/api.php?api_key=".$apikey."&method=dnd&number=".$mobilenum."&format=json";
		$chk=curl_init();
		curl_setopt($chk, CURLOPT_URL, $statusurl);
		curl_setopt($chk, CURLOPT_RETURNTRANSFER, true);
		$statusoutput=curl_exec($chk);
		curl_close($chk);
		$smsstatus = explode(' ',trim($statusoutput) );
		echo $statusoutput;
	}
	//record id based on mobile number
	public function mobilenumberbasedonrecordidmodel() {
		$tablename = $_GET['module'];
		$recordid = $_GET['recordid'];
		$expid = explode(',',$recordid);
		$i=0;
		if($tablename != '') {
			$check = $this->tablefieldnamecheck($tablename,'mobilenumber');
			if($check == 'Yes') {
				for($j=0;$j<count($expid);$j++) {
					$id = $tablename.'id';
					$this->db->select('mobilenumber');
					$this->db->from("$tablename");
					$this->db->where_in("$tablename".'.'."$id",array($expid[$j]));
					$this->db->where("$tablename".'.status',1);
					$result = $this->db->get();
					if($result->num_rows() > 0) {
						foreach($result->result() as $row) {
							$moblenum[$i] = $row->mobilenumber;
							$i++;
						}
					}
				}
			} else { $moblenum = ''; }
		} else { $moblenum = ''; }
		if($moblenum != '') {
			$explomobile = implode(',',$moblenum);
		} else {
			$explomobile = '';
		}
		$mnumber = trim($explomobile,',');
		echo json_encode($mnumber);
	}
	//Filed check
	public function tablefieldnamecheck($tblname,$fieldname) {
		$fields = $this->db->field_exists($fieldname,$tblname);
		if($fields == '1') {
			return 'Yes';
		} else {
			return 'No';
		}
	}
	//For SMS Send using SMS Widget 
	public function smstemplateddvalfetchmodel() {
		$sendsmstype = $_GET['smssendtype'];
		$senderid = $_GET['senderid'];
		$i=0;
		$userid = $this->Basefunctions->userid;
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('templatesid,templatesname,smsgrouptypeid,moduleid');
		$this->db->from('templates');
		if($sendsmstype == '2'){
			$this->db->where_in('templates.smssettingsid',array($senderid));
		}
		if($sendsmstype != '4') {
			$this->db->where_in('templates.smssendtypeid',array($sendsmstype));
		}
		$this->db->where_in('templates.smsgrouptypeid',array(3,4));
		$this->db->where('templates.templatetypeid',2);
		$this->db->where("FIND_IN_SET('$industryid',templates.industryid) >", 0);
		$this->db->where('templates.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->templatesid,'dataname'=>$row->templatesname,'typeid'=>$row->smsgrouptypeid,'moduleid'=>$row->moduleid);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}	
	}
	//schedule delete
	public function scheduledeletemodel() {
		$id = $_GET['rid'];
		$smscredit = $this->creditvalueget($id);
		$smscount = $smscredit['count'];
		$groupid = $smscredit['groupid'];
		if($smscredit['smsmode'] == 'SCHEDULE SMS') {
			$sendtype = $smscredit['senttype'];
			if($sendtype == '2') { // for transactional
				$xmlurl = "http://alerts.sinfini.com/api/v3/api.php?method=sms.schedule&api_key=";
				$apikey = 'A2edf707fbf1a13aed9e47d7011a1e93d';
				$addontypeid = '2';
			} else if($sendtype == '3') { //for promotional
				$xmlurl = "http://promo.sinfini.com/api/v3/api.php?method=sms.schedule&api_key=";
				$apikey = 'Ac9bb7caf562bf6bfa7f9aab4c83ffde9';
				$addontypeid = '4';
			} else if($sendtype == '4') { // for global
				$xmlurl = "http://global.sinfini.com/api/v3/api.php?method=sms.schedule&api_key=";
				$apikey = 'A1363f16118616942397968b9a9f736da';
				$addontypeid = '5';
			}
			$statusurl = $xmlurl.$apikey."&groupid=".$groupid."&format=json";
			$chk=curl_init();
			curl_setopt($chk, CURLOPT_URL, $statusurl);
			curl_setopt($chk, CURLOPT_RETURNTRANSFER, true);
			$statusoutput=curl_exec($chk);
			curl_close($chk);
			$smsstatus = explode(',',trim($statusoutput) ); 
			$status = explode(':',$smsstatus[0]);
			if($status[1] == "OK") {
				if($sendtype != 4) {
					$credit = $this->db->query("UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = `addonavailablecredit` + $smscount WHERE `addonstypeid` = $addontypeid");
				}
				//master company id get - master db update
				$mastercompanyid = $this->Basefunctions->generalinformaion('company','mastercompanyid','companyid',2);
				$CI =& get_instance();
				$CI->load->database();
				$hostname = $CI->db->hostname; 
				$username = $CI->db->username; 
				$password = $CI->db->password; 
				$masterdb = $CI->db->masterdb; 
				$con = mysqli_connect($hostname,$username,$password,$masterdb);
				// Check connection
				if (mysqli_connect_errno())	{
					echo "Failed to connect to MySQL: " . mysqli_connect_error();
				}
				if($sendtype != 4) {
					mysqli_query($con,"UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = `addonavailablecredit` + $smscount WHERE `addonstypeid` = $addontypeid and `mastercompanyid` ='".$mastercompanyid."'");
				}
				mysqli_close($con);	
				echo $statusoutput;
			} else {
				echo $statusoutput;
			} 
		} else {
			echo json_encode("SENT SMS");
		}
	}
	//Default api key and sender id get
	public function defapikeyandsenderidget() {
		$data = '';
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('senderid,apikey');
		$this->db->from('smssettings');
		$this->db->where('smssettings.setdefault','Yes');
		$this->db->where('smssettings.status',1);
		$this->db->where("FIND_IN_SET('$industryid',smssettings.industryid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data = array('senderid'=>$row->senderid,'apikey'=>$row->apikey);
			}
			return $data;
		} else {
		   return $data;
		}
	}
	//schedule edit operation
	public function scheduleeditmodel() {
		$id = $_GET['primaryid'];
		$smscredit = $this->creditvalueget($id);
		$groupid = $smscredit['groupid'];
		$sendtype = $smscredit['senttype'];
		if($sendtype == '2') { // for transactional
			$xmlurl = "http://alerts.sinfini.com/api/v3/index.php?method=sms.schedule&api_key=";
			$apikey = 'A2edf707fbf1a13aed9e47d7011a1e93d';
		} else if($sendtype == '3') { //for promotional
			$xmlurl = "http://promo.sinfini.com/api/v3/index.php?method=sms.schedule&api_key=";
			$apikey = 'A2edf707fbf1a13aed9e47d7011a1e93d';
		} else if($sendtype == '4') { // for global
			$xmlurl = "http://global.sinfini.com/api/v3/index.php?method=sms.schedule&api_key=";
			$apikey = 'A2edf707fbf1a13aed9e47d7011a1e93d';
		}
		$date = $_GET['scheduleeditdate'];
		$time = $_GET['scheduleedittime'];
		if($time == '') {
			$time = '00:00:00';
		}
		if($date!='') {
			$schtimestamp = strtotime($date);
			$schdate = date("Y-m-d", $schtimestamp);
			$tosplitmins = explode(':',$time);
			$roundofhr = $tosplitmins[0];
			if ($tosplitmins[1] <= 15) {
				$roundofmin = '15';
			}
			if ($tosplitmins[1] >= 16 && $tosplitmins[1] <=30) {
				$roundofmin = '30';
			}
			if ($tosplitmins[1] >= 31 && $tosplitmins[1] <=45) {
				$roundofmin = '45';
			}
			if ($tosplitmins[1] >= 46 && $tosplitmins[1] <=60) {
				$roundofmin = '00';
				$roundofhr = $tosplitmins[0] + 1;
			}
			if($roundofhr > '12') { 
				$roundofhr = $roundofhr - 12; $ttt = 'pm';
			} else { 
				$roundofhr = $roundofhr;
				$ttt = 'am';
			}
			if($roundofhr < '9' ) { 
				$roundofhr = '0'.$roundofhr;
			} else {
				$roundofhr = $roundofhr;
			}
			$commdate = $schdate.' '.$roundofhr.':'.$roundofmin.''.$ttt;
		} else {
			$commdate="";
		}
		$sstatusurl = $xmlurl.$apikey."&groupid=".$groupid."&format=json&task=modify"."&time=".$commdate;
		$cchk=curl_init();
		curl_setopt($cchk, CURLOPT_URL, $sstatusurl);
		curl_setopt($cchk, CURLOPT_RETURNTRANSFER, true);
		$sstatusoutput=curl_exec($cchk);
		curl_close($cchk);
		$ssmsstatus = explode(' ',trim($sstatusoutput) ); 
		$sstatus = explode(':',$ssmsstatus[0]);
		if($sstatus[0] != '') {
			if($sstatus[1] == "OK") {
				echo $sstatusoutput;
			} else {
				echo $sstatusoutput;
			} 
		} else {
			echo json_encode('empty');
		}
	}
	//resend sms data fetch
	public function resendsmsdatavalgetmodel() {
		$recordid = $_GET['recordid'];
		$this->db->select('smssendtypeid,smstypeid,smssettingsid,templatesid,signatureid,communicationto,commonid');
		$this->db->from('crmcommunicationlog');
		$this->db->where('crmcommunicationlog.crmcommunicationlogid',$recordid);
		$result = $this->db->get();
		if($result->num_rows() > 0 ) {
			foreach($result->result() as $row) {
				$data = array('smssendtypeid'=>$row->smssendtypeid,'type'=>$row->smstypeid,'senderid'=>$row->smssettingsid,'templatesid'=>$row->templatesid,'signatureid'=>$row->signatureid,'mobnum'=>$row->communicationto,'recordid'=>$row->commonid);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}	
	}
	//sales neuron status value get
	public function statusvalueget($smsstatus) {
		$this->db->select('smssalesneuronname');
		$this->db->from('smsstatus');
		$this->db->where('smsstatus.smsstatusid',$smsstatus);
		$result = $this->db->get();
		if($result->num_rows() > 0 ) {
			foreach($result->result() as $row) {
				$data = $row->smssalesneuronname;
			}
			return $data;
		} else {
			return $smsstatus;
		}
	}
	//account credit details fetch function 
	public function accountcresitdetailsfetchmodel($apikey,$addontypeid) {
		$this->db->select('addonavailablecredit');
		$this->db->from('salesneuronaddonscredit');
		$this->db->where('salesneuronaddonscredit.addonstypeid',$addontypeid);
		$this->db->where('salesneuronaddonscredit.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$credit = $row->addonavailablecredit;
			}
		} else {
			$credit = '0';
		}
		return $credit;
	}
	//credit value get
	public function creditvalueget($id) {
		$data ='';
		$this->db->select('smscount,crmcommunicationmode,smssendtypeid,groupid');
		$this->db->from('crmcommunicationlog');
		$this->db->where('crmcommunicationlog.crmcommunicationlogid',$id);
		$this->db->where('crmcommunicationlog.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('count'=>$row->smscount,'smsmode'=>$row->crmcommunicationmode,'senttype'=>$row->smssendtypeid,'groupid'=>$row->groupid);
			}
			return $data;
		} else {
			return $data;
		}
	}
	//sms record data pull based on the send sms
	public function smsrecordpullvalgetmodel() {
		$recordid = $_GET['recordid'];
		$smsdata = $this->groupidgetbasedonthesms($recordid);
		$groupid =  $smsdata['groupid'];
		$senderid =  $smsdata['senderid'];
		$apikey =  $smsdata['apikey'];
		$statusurl = "http://alerts.sinfini.com/api/v3/index.php?method=sms.status&api_key=".$apikey."&groupid=".$groupid."&format=json";
		$chk=curl_init();
		curl_setopt($chk, CURLOPT_URL, $statusurl);
		curl_setopt($chk, CURLOPT_RETURNTRANSFER, true);
		$statusoutput=curl_exec($chk);
		curl_close($chk);
		$smsstatus = explode(' ',trim($statusoutput) ); 
		echo json_encode($statusoutput);
	}
	//group id and sender-id value fetch
	public function groupidgetbasedonthesms($recordid) {
		$data = '';
		$this->db->select('crmcommunicationlog.groupid,smssettings.senderid,smssettings.apikey');
		$this->db->from('crmcommunicationlog');
		$this->db->join('smssettings','smssettings.smssettingsid=crmcommunicationlog.smssettingsid');
		$this->db->where('crmcommunicationlog.crmcommunicationlogid',$recordid);
		$this->db->where('crmcommunicationlog.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data = array('groupid'=>$row->groupid,'senderid'=>$row->senderid,'apikey'=>$row->apikey);
			}
		}
		return $data;
	}
	//schedule edit check
	public function scheduleeditvaluecheckmodel() {
		$id = $_GET['rid'];
		$data ='';
		$this->db->select('crmcommunicationmode');
		$this->db->from('crmcommunicationlog');
		$this->db->where('crmcommunicationlog.crmcommunicationlogid',$id);
		$this->db->where('crmcommunicationlog.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = $row->crmcommunicationmode;
			}
		}	
		echo json_encode($data);
	}
	public function smstemplateddgetmodel() {
		$moduleid = $_GET['moduleid'];
		$sendsmstype = $_GET['smssendtype'];
		$senderid = $_GET['senderid'];
		$i=0;
		$userid = $this->Basefunctions->userid;
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('templatesid,templatesname,smsgrouptypeid,moduleid');
		$this->db->from('templates');
		if($sendsmstype == '2'){
			$this->db->where_in('templates.smssettingsid',array($senderid));
			$this->db->where_in('templates.moduleid',array($moduleid));
		}
		$this->db->where("FIND_IN_SET('$industryid',templates.industryid) >", 0);
		$this->db->where_in('templates.smsgrouptypeid',array(3,4));
		$this->db->where('templates.templatetypeid',2);
		$this->db->where('templates.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data[$i] = array('datasid'=>$row->templatesid,'dataname'=>$row->templatesname,'typeid'=>$row->smsgrouptypeid,'moduleid'=>$row->moduleid);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}	
	}
}