<!-- Do Not Disturb Mobile overlay -->
<!-- For Date Formatter -->
<?php 
$CI =& get_instance();
$appdateformat = $CI->Basefunctions->appdateformatfetch(); ?>
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="mobiledndovrelay" style="overflow-y: scroll;overflow-x: hidden">
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-4 medium-6 large-centered medium-centered columns" >
			<form method="POST" name="dndmobileform" style="" id="dndmobileform" action="" enctype="" class="cleardndoverlayform">
				<span id="dndoverlayvalidation" class="validationEngineContainer">
					<div class="row">&nbsp;</div>
					<div class="row cleardataform" style="background:#f5f5f5">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-10 columns" style="background:none">DND Number Conformation</div>
							<div class="small-1 columns" id="dndmobileoverlayclose" style="text-align:right;cursor:pointer;background:none">X</div>
						</div>
						<div class="large-12 medium-12 columns small-12 ">
							<div class="large-12 columns">&nbsp;</div>
							<div class="large-6 medium-6 columns small-6 centertext">          
								<label>Total Mobile Number</label>
							</div>
							<div class="large-6 medium-6 columns small-6">      
								<input type="text" id="totalmobilenubers" class="validate[required]" readonly name="totalmobilenubers" value="" />
							</div>
						</div>
						<div class="large-12 medium-12 columns small-12 ">
							<div class="large-12 columns">&nbsp;</div>
							<div class="large-6 medium-6 columns small-6 centertext">         
								<label>DND Number</label>
							</div>
							<div class="large-6 medium-6 columns small-6 ">       
								<input type="text" id="totaldndnumbers" class="validate[required]" readonly name="totaldndnumbers" value="" />
							</div>
						</div>
						<div class="large-12 medium-12 columns small-12 ">
							<div class="large-12 columns">&nbsp;</div>
							<div class="large-6 medium-6 columns small-6 centertext">           
								<label>Non DND Numbers</label>
							</div>
							<div class="large-6 medium-6 columns small-6 ">
								<input type="text" id="totalnondndnumbers" class="validate[required]" readonly name="totalnondndnumbers" value="" />
							</div>
						</div>
						<input type="hidden" name="nondndnumbers" id="nondndnumbers" />
						<input type="hidden" name="dndnumbers" id="dndnumbers" />
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 medium-12 columns small-12 centertext">
							<label>Do you want to sent message to DND numbers</label>
						</div>
						<div class="medium-4 large-4 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 medium-12 columns small-12 ">
							<div class="large-6 medium-6 columns small-6">      
								<input type="button" id="dndyesbtn" name="dndyesbtn" value="Yes" class="btn formbuttonsalert" >
							</div>
							<div class="large-6 medium-6 columns small-6 ">
								<input type="button" id="dndnobtn" name="dndnobtn" value="No" class="btn formbuttonsalert" >
							</div>
						</div>
						<div class="medium-4 large-4 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div class="row">&nbsp;</div>
				</span>
			</form>
		</div>
	</div>
</div>