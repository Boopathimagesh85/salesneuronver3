<!-- Calendar Overlay -->
<?php 
$CI =& get_instance();
$appdateformat = $CI->Basefunctions->appdateformatfetch(); ?>
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay overlayalerts resetoverlay" id="scheduleeditoverlay" style="overflow-y: auto;overflow-x: hidden;z-index:40;">
		<div class="row sectiontoppadding">&nbsp;</div>
		<div class="large-3 medium-6 large-centered medium-centered columns" >
			<div class="row">
				<span id="scheduleditspan" class="">
					<form method="POST" name="scheduleeditoverlayform" style="" id="scheduleeditoverlayform" action="" enctype="" class="clearscheduleditform">
						<span id="validatescheduleeditoverlay" class="validationEngineContainer">
							<span id="validateschedultoverlay" class="validationEngineContainer">
								<div class="alert-panel cleartaskform">
									<div class="alertmessagearea">
										<div class="alert-title">Schedule Edit Operation</div>
										<div class="alert-message" style="height:100%">
											<span class="firsttab" tabindex="1000"></span>
												<div class="input-field overlayfield">
													<input type="text" class="ffield" data-dateformater="<?php echo $appdateformat; ?>" id="scheduleeditdate" name="scheduleeditdate" value="" class="validate[required] fordatepicicon " tabindex="1001">
													<label for="scheduleeditdate">Schedule Date <span class="mandatoryfildclass">*</span></label>
												</div>
												<div class="input-field overlayfield">
													<input type="text" id="scheduleedittime" name="scheduleedittime" value="" class="validate[required] fortimepicicon" tabindex="1002">
													<label for="scheduleedittime">Schedule Time <span class="mandatoryfildclass">*</span></label>
												</div>
												<!-- hidden fields -->
												<input type="hidden" id="primaryid" name="primaryid" value="" class="" >
										</div>
									</div>
									<div class="alertbuttonarea">
										<input type="button" id="scheduleeditsubmit" name="scheduleeditsubmit" value="Submit" tabindex="1003" class="alertbtn" >
										<input type="button" id="closescheduleoverlay" name="" value="Cancel" tabindex="1004" class="flloop alertbtn alertsoverlaybtn" >
										<span class="lasttab" tabindex="1005"></span>
									</div>
								</div>
							</span>
						</span>
					</form>
				</span>
			</div>
		</div>
	</div>
</div>