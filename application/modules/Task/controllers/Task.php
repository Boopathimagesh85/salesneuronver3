<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Task extends MX_Controller{
	//To load the Register View
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Task/Taskmodel');
    }
	public function index() {
		$moduleid = array(206,77);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);		
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['dashboradviewdata'] = $this->Basefunctions->dashboarddataviewbydropdown($moduleid);
		$this->load->view('Task/taskview',$data);
	}
	//Create New data
	public function newdatacreate() {  
    	$this->Taskmodel->newdatacreatemodel();
    }
	//information fetch
	public function fetchformdataeditdetails() {
		$moduleid = array(206,77);
		$this->Taskmodel->informationfetchmodel($moduleid);
	}
	//Update old data
    public function datainformationupdate() {
        $this->Taskmodel->datainformationupdatemodel();
    }
	//delete old information
    public function deleteinformationdata() {
		$moduleid = array(206,77);
        $this->Taskmodel->deleteoldinformation($moduleid);
    } 
	//used for fetch the data bases on module
	public function dropdownvaluefecth() {
        $this->Taskmodel->dropdownvaluefecthmodel();
    }
	//editor valuefetch editervaluefetch	
	public function editervaluefetch() {
		$filename = $_GET['filename'];
		$newfilename = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]);
			echo json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			echo json_encode($tccontent);
		}
	}
	//Editor value fetch - mail templates	
	public function maileditervaluefetch() {
		$filename = $_GET['filename']; 
		$newfilename = explode('/',$filename);
		if(read_file('templates'.DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = read_file('templates'.DIRECTORY_SEPARATOR.$newfilename[1]);
			echo json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			echo json_encode($tccontent);
		}
	}
	//task validation
	public function taskvalidation() {
		$this->Taskmodel->taskvalidationmodel();
	}
	//invite module drop down load function
	public function invitemoduledropdownloadfun() {
		$this->Taskmodel->invitemoduledropdownloadfunmodel();
	}
	//View drop down load function
	public function viewdropdownloadfun() {
		$this->Taskmodel->viewdropdownloadfunmodel();
	}
	//parent table get
	public function parenttableget() {
		$this->Taskmodel->parenttablegetmodel();
	}
	//invite user grid data fetch
	public function inviteusergrigdatafetch() {
		$creationid = $_GET['viewid'];
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$viewcolmoduleids = $_GET['moduleid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$filter = '';
		$chkbox = $_GET['checkbox'];
		$sortcol = isset($_GET['sortcol']) ? $_GET['sortcol'] : '';
		$sortord = isset($_GET['sortord']) ? $_GET['sortord'] : '';
		$footer = isset($_GET['footername']) ? $_GET['footername'] : '';
		$result = $this->Basefunctions->viewdynamicdatainfofetch($creationid,$primaryid,$viewcolmoduleids,$pagenum,$rowscount,$sortcol,$sortord,$filter);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobileviewgriddatagenerate($result,$primarytable,$primaryid,$width,$height,$chkbox);
		} else {
			$datas = viewgriddatagenerate($result,$primarytable,$primaryid,$width,$height,$footer,$chkbox);
		}
		echo json_encode($datas);
	}
	//mobile number and email id check  - validation
	public function mobileandmailvalidation() {
		$this->Taskmodel->mobileandmailvalidationmodel();
	}
	//reminder validation
	public function reminderintervalcheck() {
		$this->Taskmodel->reminderintervalchecknmodel();
	}
	//recurrence onday data fetch
	public function recurrecneonthe() {
		$this->Taskmodel->recurrecneondaynmodel();
	}
	//record name fetch
	public function recordnamefetch(){
		$this->Taskmodel->recordnamefetchmodel();
	}
	//templatenamefetch
	public function templateddvalfetch() {
		$this->Taskmodel->templateddvalfetchmodel();
	}
	//template val fetch on update
	public function templatenamefetch() {
		$this->Taskmodel->templatenamefetchmodel();
	}
	//sms template file name fetch
	public function smsfilenamefetch() {
		$this->Taskmodel->smsfilenamefetchmodel();
	}
	//sms merge template content information
	public function smsmergcontinformationfetch() {
		$this->Taskmodel->smsmergcontinformationfetchmodel();
	}
	//mail template file name fetch
	public function mailfilenamefetch() {
		$this->Taskmodel->mailfilenamefetchmodel();
	}
	// 	to set template dropdown on edit
	public function gettemplatesid() {
		$this->Taskmodel->gettemplatesidmodel();
	}
	//retrieve dropdown options
	public function getdropdownoptions() {
		$table=$_GET['table'];
		$this->Taskmodel->getdropdownoptions($table);
	}
	//fetch leadcontact
	public function leadcontacttypefunction() {
		$this->Taskmodel->leadcontacttypefunction();
	}
}