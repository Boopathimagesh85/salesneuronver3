<!-- Validate Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="taskvalidateovrelay">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="alert-panel">
			<div class="alertmessagearea">
				<div class="alert-title">Confirmation</div>
				<div class="alert-message">
					<span >You have another task which overlaps with this task.</span> <br>
					<span>Are you sure you want to create this task anyway?</span>
				</div>
			</div>
			<div class="alertbuttonarea">
				<span class="firsttab" tabindex="1000"></span>
				<input type="button" id="taskcreatebtn" name="taskcreatebtn" tabindex="1001" value="Agree" class="alertbtn ffield" >	
				<input type="button" id="taskcancelbtn" name="taskcancelbtn" value="Disagree" tabindex="1002" class="alertbtn flloop  alertsoverlaybtn" >
				<span class="lasttab" tabindex="1003"></span>
			</div>
		</div>
	</div>
</div>

