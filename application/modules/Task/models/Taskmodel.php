<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Model File
class Taskmodel extends CI_Model{    
    public function __construct() {		
		parent::__construct(); 
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//To Create New task values
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$empid = $this->Basefunctions->logemployeeid;
		//activity data set
		$startdate = $_POST['recurrencestartdate'];
		$count = 0;
		$crmtaskname = $_POST['crmtaskname'];
		if(isset($_POST['taskpriorityid'])){
			$priorityid = $_POST['taskpriorityid'];
			if($priorityid == '') {
				$priorityid = 1;
			}
		} else { 
			$priorityid = 1; 
		}
		$emptypes = $_POST['employeetypeid'];
		if(isset($_POST['employeeid'])){
			$employeeid = $_POST['employeeid'];
			if($employeeid == '') {
				$employeeid = 1;
			}
		} else { 
			$employeeid = 1; 
		}
		if(isset($_POST['crmstatusid'])) {
			$leadstatusid = $_POST['crmstatusid'];
			if($leadstatusid == '') {
				$leadstatusid = 1;
			}
		} else { 
			$leadstatusid =1; 
		}
		if(isset($_POST['eventlocation'])){
			$location = $_POST['eventlocation'];
		} else { 
			$location =''; 
		}
		if(isset($_POST['notes'])){
			$description = $_POST['notes'];
		} else { 
			$description =''; 
		}
		$taskstartdate = $startdate;
		if(isset($_POST['remindermodeid'])){
			$remmode = $_POST['remindermodeid'];
			$remindermode = implode(',',$remmode);
			if($remindermode == '') {
				$remindermode = 1;
			}
		} else { 
			$remindermode = 1; 
		}
		//invite mode ids
		if(isset($_POST['taskremindermodeid'])){
			$inviteremmode = $_POST['taskremindermodeid'];
			$inviteremindermode = implode(',',$inviteremmode);
			if($inviteremindermode == '') {
				$inviteremindermode = 1;
			}
		} else {
			$inviteremindermode = 1;
		}
		//reminder template update
		if(isset($_POST['reminderleadtemplateid'])){
			$remindersmstempid = $_POST['reminderleadtemplateid'];
			if($remindersmstempid != '') {
				$remsmstempid = $remindersmstempid;
			} else { 
				$remsmstempid = 1; 
			}
		} else { 
			$remsmstempid = 1;
		}
		if(isset($_POST['reminderemailtemplatesid'])){
			$reminderemailtempid = $_POST['reminderemailtemplatesid'];
			if($reminderemailtempid != '') {
				$rememailtempid = $reminderemailtempid;
			} else { 
				$rememailtempid = 1;
			}
		} else {
			$rememailtempid = 1;
		}
		$cronnotification = 0;
		$cronstatus = 1;
		$assignid = $_POST['employeeid'];
		$emptypes = $_POST['employeetypeid'];
		$userid = $this->Basefunctions->userid;
		$cdate = date($this->Basefunctions->datef);
		//recurrence check
		$restricttable = explode(',',$_POST['resctable']);
		if(isset($_POST['recurrence'])){
			$recurrence = $_POST['recurrence'];
		} else {
			$recurrence = 'No';
		}
		if($recurrence != 'No') {
			$recurrencefreqid = $_POST['taskrecurrencefreqid'];
			if(isset($_POST['taskrecurrencefreqid'])) {
				$crmrecurrenceontheday = $_POST['taskrecurrencefreqid'];
				if($crmrecurrenceontheday != "") {
					$crmrecurrenceontheday  = $crmrecurrenceontheday;
				} else {
					$crmrecurrenceontheday = 2;
				}
			} else { 
				$crmrecurrenceontheday = 2; 
			}
			if(isset($_POST['repeatonmultipleid'])) {
				$crmrecurrencedayid = $_POST['repeatonmultipleid'];
				if($crmrecurrencedayid != "") {
					$crmrecurrencedayid  = $crmrecurrencedayid;
				} else {
					$crmrecurrencedayid = 1;
				}
			} else { 
				$crmrecurrencedayid = 1; 
			}
			if(isset($_POST['taskrecurrenceendsid'])) {
				$taskrecurrenceendsid = $_POST['taskrecurrenceendsid'];
				if($taskrecurrenceendsid != "") {
					$taskrecurrenceendsid  = $taskrecurrenceendsid;
				} else {
					$taskrecurrenceendsid = 1;
				}
			} else { 
				$taskrecurrenceendsid = 1; 
			}
			if(isset($_POST['afteroccurences'])){
				$afteroccurences = $_POST['afteroccurences'];
			} else { 
				$afteroccurences = 1; 
			}
			$activityrecurrencestartdate = $_POST['recurrencestartdate'];
			if(isset($_POST['taskrecurrenceuntil'])) {
				$crmrecurrenceuntil = $_POST['taskrecurrenceuntil'];
			} else {
				$crmrecurrenceuntil = '';
			}
			$crmrecurrenceeveryday = $_POST['taskrecurrenceeveryday'];
			//for recurrence activity
			$taskrecurrenceontheid = $_POST['taskrecurrenceontheid'];
			$recurrenceendsid = $_POST['taskrecurrenceendsid'];
			$startdate = $_POST['recurrencestartdate'];
			$enddate = $_POST['taskrecurrenceuntil'];
			if($recurrenceendsid == '6') {
				$enddate = $_POST['recurrenceenddate'];
			} else if($recurrenceendsid == '7') {
				$enddate = $_POST['taskrecurrenceuntil'];
			}
			$datetime1 = date_create($startdate);
			$datetime2 = date_create($enddate);
			if($recurrencefreqid == '8') { // For daily
				if($recurrenceendsid == '7') {
					$count = $datetime1->diff($datetime2)->days;
				} else if($recurrenceendsid == '6') {
					$datecount = $datetime1->diff($datetime2)->days;
					$count = $afteroccurences;
				} else if($recurrenceendsid == '2') {
					$day = 730;
					$sdate = strtotime("+".$day." day", strtotime($startdate));
					$enddate = date("d-m-Y", $sdate);
					$datetime2 = date_create($enddate);
					$count = $datetime1->diff($datetime2)->days;
				}
			} else if($recurrencefreqid == '9') { //for weekly
				if($recurrenceendsid == '2') {
					$day = 730;
					$sdate = strtotime("+".$day." day", strtotime($startdate));
					$enddate = date("d-m-Y", $sdate);
					$count = 1;
				} else if($recurrenceendsid == '6') {
					$ccc = $datetime1->diff($datetime2)->days;
					$count = 1;
				} else if($recurrenceendsid == '7') {
					$ccc = $datetime1->diff($datetime2)->days;
					$count = 1;
				}
			} else if($recurrencefreqid == '10') { //for monthly
				if($recurrenceendsid == '7') {
					$ts1 = strtotime($startdate);
					$ts2 = strtotime($enddate);
					$year1 = date('Y', $ts1);
					$year2 = date('Y', $ts2);
					$month1 = date('m', $ts1);
					$month2 = date('m', $ts2);
					$diff = (($year2 - $year1) * 12) + ($month2 - $month1);
					$count = $diff/$crmrecurrenceeveryday;
				} else if($recurrenceendsid == '6') { 
					$count = $afteroccurences;
				} else if($recurrenceendsid == '2') {
					$day = 730;
					$sdate = strtotime("+".$day." day", strtotime($startdate));
					$enddate = date("d-m-Y", $sdate);
					$ts1 = strtotime($startdate);
					$ts2 = strtotime($enddate);
					$year1 = date('Y', $ts1);
					$year2 = date('Y', $ts2);
					$month1 = date('m', $ts1);
					$month2 = date('m', $ts2);
					$diff = (($year2 - $year1) * 12) + ($month2 - $month1);
					$count = $diff;
				}
			} else if($recurrencefreqid == '11') { //for yearly
				if($recurrenceendsid == '7') {
					$ccc = $datetime1->diff($datetime2)->y;
					$count = $ccc/$crmrecurrenceeveryday; 
				} else if($recurrenceendsid == '6') { 
					$count = $afteroccurences;
				} else if($recurrenceendsid == '2') { 
					$day = 730;
					$sdate = strtotime("+".$day." day", strtotime($startdate));
					$enddate = date("d-m-Y", $sdate);
					$datetime2 = date_create($enddate);
					$count = $datetime1->diff($datetime2)->y; 
				}
			}
			$tempeventid = date('YmdHis');
			for($i=0;$i<$count;$i++) {
				$sdate = $this->getstartdate($crmrecurrenceeveryday,$startdate,$enddate,$recurrencefreqid,$crmrecurrenceontheday,$crmrecurrencedayid,$i);
				$rcount = count($sdate);
				for($j=0;$j<$rcount;$j++) {
					$rdate = explode(',',$sdate[$j]);
					$startdate = $rdate[0];
					$enddate = $rdate[1];
					$_POST['taskstartdate']=$startdate;
					$_POST['taskenddate']=$enddate;
					//invite section
					if(isset($_POST['leadtemplateid'])){
						$invitesmstempid = $_POST['leadtemplateid'];
						if($invitesmstempid != '') {
							$invsmstempid = $invitesmstempid;
						} else { $invsmstempid = 1; }
					} else { 
						$invsmstempid = 1; 
					}
					if(isset($_POST['emailtemplatesid'])){
						$inviteemailtempid = $_POST['emailtemplatesid'];
						if($inviteemailtempid != '') {
							$invemailtempid = $inviteemailtempid;
						} else { $invemailtempid = 1; }
					} else { 
						$invemailtempid = 1; 
					}
					$primaryid = $this->Crudmodel->datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable);
					//invite section
					if(isset($_POST['leadtemplateid'])){
						$invitesmstempid = $_POST['leadtemplateid'];
						if($invitesmstempid != '') {
							$invsmstempid = $invitesmstempid;
						} else { $invsmstempid = 1; }
					} else { $invsmstempid = 1; }
					if(isset($_POST['emailtemplatesid'])){
						$inviteemailtempid = $_POST['emailtemplatesid'];
						if($inviteemailtempid != '') {
							$invemailtempid = $inviteemailtempid;
						} else { $invemailtempid = 1; }
					} else { $invemailtempid = 1; }
					if(isset($_POST['taskinvitemoduleid'])){
						$invitemodid = $_POST['taskinvitemoduleid'];
						if($invitemodid != '') {
							$invmodeid = $invitemodid;
						} else { $invmodeid = 1; }
					} else { $invmodeid = 1; }
					if(isset($_POST['inviteviewname'])){
						$inviteviewname = $_POST['inviteviewname'];
						if($inviteviewname != '') {
							$invviewname = $inviteviewname;
						} else { $invviewname = 1; }
					} else { $invviewname = 1; }
					// lead/contact's name insert
					if(isset($_POST['leadcontacttype'])){
						$leadcontacttypeid = $_POST['leadcontacttype'];
						if($leadcontacttypeid == 2) {
							if(isset($_POST['contacttaskid'])){
								$contacttaskid = $_POST['contacttaskid'];
								if($contacttaskid !=  '') {
									$contacttaskid = $contacttaskid;
								} else {
										$contacttaskid = 1;
								}
								$leadtaskid = 1;
							} else {
								$contacttaskid = 1;
								$leadtaskid = 1;
							}
						} else {
							 if(isset($_POST['leadtaskid'])){
								$leadtaskid = $_POST['leadtaskid'];
								if($leadtaskid !=  ''){
									$leadtaskid = $leadtaskid;
								} else {
									$leadtaskid = 1;
								}
								$contacttaskid = 1;
							}
						}
					}
					$inviteremindermode = trim($inviteremindermode,",");
					$template= array('leadtemplateid'=>$remsmstempid,'emailtemplatesid'=>$rememailtempid, 'invitemode' => $inviteremindermode, 'invitesmstemplate' => $invsmstempid, 'invitemailtemplate' => $invemailtempid, 'inviteviewname' => $invviewname, 'leadid' => $invmodeid, 'contacttaskid' => $contacttaskid, 'leadtaskid' => $leadtaskid);
					$this->db->where_in('crmtask.crmtaskid',array($primaryid));
					$this->db->update('crmtask',$template);
					$recurrence = array(
							'recurrencefreqid'=>$recurrencefreqid,
							'recurrencedayid'=>$crmrecurrencedayid,
							'crmtaskid'=>$primaryid,
							'moduleid'=>1,
							'crmrecurrenceuntil'=>$this->Basefunctions->ymddateconversion($crmrecurrenceuntil),
							'crmrecurrenceeveryday'=>$crmrecurrenceeveryday,
							'recurrenceendsid'=>$recurrenceendsid,
							'afteroccurences'=>$afteroccurences,
							'createdate'=>$cdate,
							'lastupdatedate'=>$cdate,
							'createuserid'=>$userid,
							'lastupdateuserid'=>$userid,
							'status'=>$this->Basefunctions->activestatus
						);
					$this->db->insert('crmrecurrence',$recurrence);
					$apikeyandsenderid = $this->defapikeyandsenderidget();
					$subaccountid = $this->subaccountdetailsfetchmodel();
					//reminder notification
					$industry = $this->Basefunctions->industryid;
					{ //notification entry
						$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
						if(isset($_POST['employeeid'])) {
							$assignid = $_POST['employeeid'];
							$emptypes = $_POST['employeetypeid'];
							$empdataids = array();
							$assignempids = array();
							if($assignid != '') {
								$k = 0;
								$m=0;
								$empiddatas = explode(',',$assignid);
								$emptypeids = explode(',',$emptypes);
								foreach($empiddatas as $empids) {
									$emptype = $emptypeids[$m];
									if($emptype == 1) {
										$empdataids[$k] = $empids;
										$k++;
									} else if($emptype == 2) {
										$empdataids[$k] = $this->Basefunctions->empidfetchfromgroup($empids);
										$k++;
									} else if($emptype == 3) {
										$empdataids[$k] = $this->Basefunctions->empidfetchfromroles($empids);
										$k++;
									} else if($emptype == 4) {
										$empdataids[$k] = $this->Basefunctions->empidfetchfromrolesandsubroles($empids);
										$k++;
									}
									$m++;
								}
							} else {
								$assignid = 1;
							}
						} else {
							$assignid = 1;
						}
						$activityname = $_POST['crmtaskname'];
						if($assignid == '1'){
							$notimsg = $this->Basefunctions->notificationtemplatedatafetch(205,$primaryid,$partablename,2);
							if($notimsg != '') {
								$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,205);
							}
						} else {
							foreach($empdataids as $empidinfo) {
								if(is_array($empidinfo)) { //group of employees
									foreach($empidinfo as $empid) {
										if( !in_array($empid,$assignempids) ) {
											array_push($assignempids,$empid);
											$notimsg = $this->Basefunctions->notificationtemplatedatafetch(205,$primaryid,$partablename,2);
											if($notimsg != '') {
												$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,205);
											}
										}
									}
								} else { //individual employees
									if( !in_array($empidinfo,$assignempids) ) {
										array_push($assignempids,$empidinfo);
										$notimsg = $this->Basefunctions->notificationtemplatedatafetch(205,$primaryid,$partablename,22);
										if($notimsg != '') {
											$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,205);
										}
									}
								}
							}
						}						
					}
					$notifypatient = (isset($_POST['notifypatient'])) ? $_POST['notifypatient'] : 'No';
					if($notifypatient != 'No') {
						$remindermodeid = $_POST['remindermodeid'];
						for($pat=0;$pat<count($remindermodeid);$pat++){
							if($remindermodeid[$pat] == 2) {
								$remindermobilenum = '';
								$remindersmstempid = $_POST['reminderleadtemplateid'];
								if($remindersmstempid != ''){
									$print_data=array('templateid'=>$remindersmstempid,'Templatetype'=>'Printtemp','primaryset'=>'crmtask.crmtaskid','primaryid'=>$primaryid);
									$datacontent = $this->smsgenerateprinthtmlfile($print_data);
								}
					} else if($remindermodeid[$pat] == '3') {
								$remindermail = '';
								$remindermailid = $_POST['reminderemailtemplatesid'];
								if($remindermailid != ''){
									$print_data=array('templateid'=>$remindermailid,'Templatetype'=>'Printtemp','primaryset'=>'crmtask.crmtaskid','primaryid'=>$primaryid);
									$datacontent = $this->generateprinthtmlfile($print_data);
									$this->mailnotificationsent($remindermail,$primaryid,$activityname,$this->Basefunctions->ymddateconversion($startdate),$datacontent,$subaccountid);
								}
								
							}
						}
					}
					//invite user notification
					$date = $_POST['taskstartdate'];
					$gcaldate = $date; //For G cal
					$leadinviteid = $_POST['leadinviteusetid'];
					$contactinviteid = $_POST['contactinviteuserid'];
					$userinviteid = $_POST['userinviteuserid'];
					$groupinviteid = $_POST['groupinviteuserid'];
					$rolesinviteid = $_POST['rolesinviteuserid'];
					$randsinviteid = $_POST['randsinviteuserid'];
					//dashboard notification sent
					$invitetype = $_POST['invitetypeid'];
					if($invitetype != '') {
						$typeid = explode(',',$invitetype);
						for($l=0;$l < count($typeid);$l++) {
							if($typeid[$l] == '6') {
								$this->inviteusernotification($leadinviteid,$contactinviteid,$userinviteid,$groupinviteid,$rolesinviteid,$randsinviteid,$primaryid,$crmtaskname,$date);
							} else if($typeid[$l] == '2') {
								$invitemobilenum = $_POST['invitemobilenumber'];
								$invitealnotes = $_POST['leadtemplateid'];
								if($invitealnotes !=''){
									$print_data=array('templateid'=>$invitealnotes,'Templatetype'=>'Printtemp','primaryset'=>'crmtask.crmtaskid','primaryid'=>$primaryid);
									$datacontent = $this->smsgenerateprinthtmlfile($print_data);
								}
							} else if($typeid[$l] == '3') {
								$invitemail = $_POST['inviteemailid'];
								$invitemailnotes = $_POST['emailtemplatesid'];
								if($invitemailnotes !=''){
									$print_data=array('templateid'=>$invitemailnotes,'Templatetype'=>'Printtemp','primaryset'=>'crmtask.crmtaskid','primaryid'=>$primaryid);
									$datacontent = $this->generateprinthtmlfile($print_data);
									$this->mailnotificationsent($invitemail,$primaryid,$activityname,$date,$datacontent,$subaccountid);
								}
								
							}
							
						}
					}
				}
			}
		} else {
			$activityname = $_POST['crmtaskname'];
			$primaryid = $this->Crudmodel->datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable);
			//reminder template update
			if(isset($_POST['reminderleadtemplateid'])){
				$remindersmstempid = $_POST['reminderleadtemplateid'];
				if($remindersmstempid != '') {
					$remsmstempid = $remindersmstempid;
				} else { $remsmstempid = 1; }
			} else { $remsmstempid = 1; }
			if(isset($_POST['reminderemailtemplatesid'])){
				$reminderemailtempid = $_POST['reminderemailtemplatesid'];
				if($reminderemailtempid != '') {
					$rememailtempid = $reminderemailtempid;
				} else { $rememailtempid = 1; }
			} else { $rememailtempid = 1; }
			//invite section
			if(isset($_POST['leadtemplateid'])){
				$invitesmstempid = $_POST['leadtemplateid'];
				if($invitesmstempid != '') {
					$invsmstempid = $invitesmstempid;
				} else { $invsmstempid = 1; }
			} else { $invsmstempid = 1; }
			if(isset($_POST['emailtemplatesid'])){
				$inviteemailtempid = $_POST['emailtemplatesid'];
				if($inviteemailtempid != '') {
					$invemailtempid = $inviteemailtempid;
				} else { $invemailtempid = 1; }
			} else { $invemailtempid = 1; }
			if(isset($_POST['taskinvitemoduleid'])){
				$invitemodid = $_POST['taskinvitemoduleid'];
				if($invitemodid != '') {
					$invmodeid = $invitemodid;
				} else { $invmodeid = 1; }
			} else { $invmodeid = 1; }
			if(isset($_POST['inviteviewname'])){
				$inviteviewname = $_POST['inviteviewname'];
				if($inviteviewname != '') {
					$invviewname = $inviteviewname;
				} else { $invviewname = 1; }
			} else { $invviewname = 1; }
			// lead/contact's name insert
			if(isset($_POST['leadcontacttype'])){
				$leadcontacttypeid = $_POST['leadcontacttype'];
				if($leadcontacttypeid == '2') {
					if(isset($_POST['contacttaskid'])){
						$contacttaskid = $_POST['contacttaskid'];
						if($contacttaskid != '') {
							$contacttaskid = $contacttaskid;
						} else {
							$contacttaskid = 1;
						}
						$leadtaskid = 1;
					} else {
						$contacttaskid = 1;
						$leadtaskid = 1;
					}
				} else {
					 if(isset($_POST['leadtaskid'])){
						$leadtaskid = $_POST['leadtaskid'];
						if($leadtaskid !=  ''){
							$leadtaskid = $leadtaskid;
						} else {
							$leadtaskid = 1;
						}
						$contacttaskid = 1;
					}
				}
			}
			$inviteremindermode = trim($inviteremindermode,",");
			$template= array('leadtemplateid'=>$remsmstempid,'emailtemplatesid'=>$rememailtempid, 'invitemode' => $inviteremindermode, 'invitesmstemplate' => $invsmstempid, 'invitemailtemplate' => $invemailtempid, 'inviteviewname' => $invviewname, 'leadid' => $invmodeid, 'contacttaskid' => $contacttaskid, 'leadtaskid' => $leadtaskid);
			$this->db->where_in('crmtask.crmtaskid',array($primaryid));
			$this->db->update('crmtask',$template);
			//reminder notification
			$apikeyandsenderid = $this->defapikeyandsenderidget();
			$subaccountid = $this->subaccountdetailsfetchmodel();
			$industry = $this->Basefunctions->industryid;
			$notifypatient = (isset($_POST['notifypatient'])) ? $_POST['notifypatient'] : 'No';
			if($notifypatient != 'No') {
				$remindermodeid = $_POST['remindermodeid'];
				for($pat=0;$pat<count($remindermodeid);$pat++){
					if($remindermodeid[$pat] == 2) {
						$invitemobilenum = $_POST['mobilenumber'];
						$remindersmstempid = $_POST['reminderleadtemplateid'];
						if($remindersmstempid != ''){
							$print_data=array('templateid'=>$remindersmstempid,'Templatetype'=>'Printtemp','primaryset'=>'crmtask.crmtaskid','primaryid'=>$primaryid);
							$datacontent = $this->smsgenerateprinthtmlfile($print_data);
						}
					} else if($remindermodeid[$pat] == '3') {
						$invitemail = $_POST['emailaddress'];
						$reminderemailtempid = $_POST['reminderemailtemplatesid'];
						if($reminderemailtempid != ''){
							$print_data=array('templateid'=>$reminderemailtempid,'Templatetype'=>'Printtemp','primaryset'=>'crmtask.crmtaskid','primaryid'=>$primaryid);
							$datacontent = $this->generateprinthtmlfile($print_data);
							$this->mailnotificationsent($invitemail,$primaryid,$activityname,$this->Basefunctions->ymddateconversion($startdate),$datacontent,$subaccountid);
						}
					}
				}
			}
			//invite user notification
			$date = $_POST['taskstartdate'];
			$gcaldate = $date; //For G cal
			$leadinviteid = $_POST['leadinviteusetid'];
			$contactinviteid = $_POST['contactinviteuserid'];
			$userinviteid = $_POST['userinviteuserid'];
			$groupinviteid = $_POST['groupinviteuserid'];
			$rolesinviteid = $_POST['rolesinviteuserid'];
			$randsinviteid = $_POST['randsinviteuserid'];
			//dashboard notification sent
			$invitetype = $_POST['invitetypeid'];
			if($invitetype != '') {
				$typeid = explode(',',$invitetype);
				for($l=0;$l < count($typeid);$l++) {
					if($typeid[$l] == '6') {
						$this->inviteusernotification($leadinviteid,$contactinviteid,$userinviteid,$groupinviteid,$rolesinviteid,$randsinviteid,$primaryid,$crmtaskname,$date);
					} else if($typeid[$l] == '2') {
						$invitemobilenum = $_POST['invitemobilenumber'];
						$invitesmstempid = $_POST['leadtemplateid'];
						if($invitesmstempid != ''){
							$print_data=array('templateid'=>$invitesmstempid,'Templatetype'=>'Printtemp','primaryset'=>'crmtask.crmtaskid','primaryid'=>$primaryid);
							$datacontent = $this->smsgenerateprinthtmlfile($print_data);
						}
					} else if($typeid[$l] == '3') {
						$invitemail = $_POST['inviteemailid'];
						$invitemailid = $_POST['emailtemplatesid'];
						if($invitemailid != '') {
							$print_data=array('templateid'=>$invitemailid,'Templatetype'=>'Printtemp','primaryset'=>'crmtask.crmtaskid','primaryid'=>$primaryid);
							$datacontent = $this->generateprinthtmlfile($print_data);
							$this->mailnotificationsent($invitemail,$primaryid,$activityname,$date,$datacontent,$subaccountid);
						}
						
					}
				}
			}
		}
		echo 'TRUE';
	 }	
	//used to get the date based on the recurrence selection -- daily/weekly/monthly/yearly
	public function getstartdate($crmrecurrenceeveryday,$sdate,$enddate,$crmrecurrencefreqid,$taskrecurrenceonthe,$crmrecurrencedayid,$count) {
		if($crmrecurrencefreqid == 8) {
			//for daily operation
			if($count == 0) {
				$daydate =$sdate.','.$enddate;
				$daydate = array($daydate);
			} else {
				$sdate = strtotime("+".$crmrecurrenceeveryday." day", strtotime($sdate));
				$edate = strtotime("+".$crmrecurrenceeveryday." day", strtotime($enddate));
				$daydate =date("d-m-Y", $sdate).','.date('d-m-Y',$edate);
				$daydate = array($daydate);
			}
			return $daydate;
		} else if($crmrecurrencefreqid == 9) {
			$ssdate = array();
			$weekdate = '';
			$endweekdate = '';
			$datetime1 = date_create($sdate);
			$datetime2 = date_create($enddate);
			$ccc = $datetime1->diff($datetime2)->days;
			$newcount = round($ccc/7);//week number
			$dayvalue = explode(',',$crmrecurrencedayid);
			$weekday = $this->getweekday($dayvalue);
			$j=0;
			for($i=0;$i<$newcount;$i++) {
				$value = 7 * $crmrecurrenceeveryday;
				$num = $i * $value;
				$newdate = strtotime("+".$num." day", strtotime($sdate));
				$date = date("d-m-Y", $newdate);
				$dt = new DateTime($date);
				$dayname = $dt->format('l');
				$startdate[$j] = $date;
				$day[$j] = $dayname;
				$edate = strtotime("+".$num." day", strtotime($enddate));
				$endate = date("d-m-Y", $edate);
				$edt = new DateTime($endate);
				$edayname = $edt->format('l');
				$eenddate[$j] = $endate;
				$eday[$j] = $edayname; 
				$j++;
				$weekdate = $this->getweekdate($startdate,$weekday);
				$endweekdate = $this->getweekdate($eenddate,$weekday);
			} 
			$ccount = count($weekdate);
			$n=0;
			for($m=0;$m<$ccount;$m++) {
				$ssdate[$n] = $weekdate[$m].','.$endweekdate[$m];
					$n++;
			}
			return $ssdate;	
		} else if($crmrecurrencefreqid == 10) {
			//for monthly operation
			if($count == 0) {
				$daydate =$sdate.','.$enddate;
				$mdate = array($daydate);
			} else {
					//for start date
					$month = explode('-',$sdate);
					if($month[1] == '01' || $month[1] == '03' || $month[1] == '05' || $month[1] == '07'|| $month[1] == '08' || $month[1] == '10' || $month[1] == '12'){
						$day = $crmrecurrenceeveryday * 31;
					} else if($month[1] == '04' || $month[1] == '06' || $month[1] == '09' || $month[1] == '11') {
						$day = $crmrecurrenceeveryday * 30;
					} else {
						if(($month[2] % 4) == 0) {
							$day = $crmrecurrenceeveryday * 29;
						} else {
							$day = $crmrecurrenceeveryday * 28;
						}
					}
					//for end date
					$endmonth = explode('-',$enddate);
					if($endmonth[1] == '01' || $endmonth[1] == '03' || $endmonth[1] == '05' || $endmonth[1] == '07'|| $endmonth[1] == '08' || $endmonth[1] == '10' || $endmonth[1] == '12'){
						$eday = $crmrecurrenceeveryday * 31;
					} else if($endmonth[1] == '04' || $endmonth[1] == '06' || $endmonth[1] == '09' || $endmonth[1] == '11') {
						$eday = $crmrecurrenceeveryday * 30;
					} else {
						if(($month[2] % 4) == 0) {
							$eday = $crmrecurrenceeveryday * 29;
						} else {
							$eday = $crmrecurrenceeveryday * 28;
						}
					}
					$reccuurday = $taskrecurrenceonthe - 1;
					//for start date
					$sdate = strtotime("+".$day." day", strtotime($sdate));
					$date = date("d-m-Y", $sdate);
					$date1 =  explode('-',$date);
					$sdata = $reccuurday.'-'.$date1[1].'-'.$date1[2];
					//for end date
					$enddate = strtotime("+".$eday." day", strtotime($enddate));
					$edate = date("d-m-Y", $enddate);
					$edate1 =  explode('-',$edate);
					$edata = $reccuurday.'-'.$edate1[1].'-'.$edate1[2];
					//final start and end date
					$daydate =$sdata.','.$edata;
					$mdate = array($daydate);
				}
				return $mdate;
			/* } */
		} else if($crmrecurrencefreqid == 11) {
			if($count == 0) {
				$daydate =$sdate.','.$enddate;
				$daydate = array($daydate);
				return $daydate;
			} else {
				//for start date
				$year = explode('-',$sdate);
				$eyear = explode('-',$enddate);
				if(($year[2] % 4) == 0) {
					$day = $crmrecurrenceeveryday * 366;
				} else {
					$day = $crmrecurrenceeveryday * 365;
				}
				$sdate = strtotime("+".$day." day", strtotime($sdate));
				//for end date
				
				if(($eyear[2] % 4) == 0) {
					$eday = $crmrecurrenceeveryday * 366;
				} else {
					$eday = $crmrecurrenceeveryday * 365;
				}
				$enddate = strtotime("+".$eday." day", strtotime($enddate));
				$daydate =date("d-m-Y", $sdate).','.date("d-m-Y", $enddate);
				$ydate = array($daydate);
				return $ydate;
			}
		}
	}
	//get week day
	public function getweekday($dayvalue) { 
		$count = count($dayvalue);
		for($i=0;$i<$count;$i++) {
			if($dayvalue[$i] == 15){
				$weekday[$i] = 'Sunday';
			}else if($dayvalue[$i] == 9){
				$weekday[$i] = 'Monday';
			}else if($dayvalue[$i] == 11){
				$weekday[$i] = 'Wednesday';
			}else if($dayvalue[$i] == 10){
				$weekday[$i] = 'Tuesday';
			}else if($dayvalue[$i] == 12){
				$weekday[$i] = 'Thursday';
			}else if($dayvalue[$i] == 13){
				$weekday[$i] = 'Friday';
			}else if($dayvalue[$i] == 14){
				$weekday[$i] = 'Saturday';
			}
		}
		return $weekday;
	}
	//week date get based on the user data
	public function getweekdate($startdate,$weekday) {
		$count = count($startdate);
		$k=0;
		for($i=0;$i<$count;$i++) {
			for($j=0;$j<7;$j++) {
				$newdate = strtotime("+".$j." day", strtotime($startdate[$i]));
				$date = date("d-m-Y", $newdate);
				$dt = new DateTime($date);
				$dayname = $dt->format('l');
				if(in_array($dayname,$weekday)) {
					$wdate[$k] = $date;
					$k++;
				}
			}
		}
		return $wdate;
	}
	//information fetch
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['dataprimaryid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	//update data information
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['primarydataid'];
		{//fetch old values -- work flow
			$condstatvals=$this->Basefunctions->workflowmanageolddatainfofetch(206,$primaryid);
		}
		$restricttable = explode(',',$_POST['resctable']);
		$result = $this->Crudmodel->dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable);
		//notification entry
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		if(isset($_POST['employeeid'])) {
			$assignid = $_POST['employeeid'];
			$emptypes = $_POST['employeetypeid'];
			$empdataids = array();
			$assignempids = array();
			if($assignid != '') {
				$i = 0;
				$m=0;
				$empiddatas = explode(',',$assignid);
				foreach($empiddatas as $empids) {
					$emptype = $emptypes[$m];
					if($emptype == 1) {
						$empdataids[$i] = $empids;
						$i++;
					} else if($emptype == 2) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromgroup($empids);
						$i++;
					} else if($emptype == 3) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromroles($empids);
						$i++;
					} else if($emptype == 4) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromrolesandsubroles($empids);
						$i++;
					}
					$m++;
				}
			} else {
				$assignid = 1;
			}
		} else {
			$assignid = 1;
		}
		$taskname = $_POST['crmtaskname'];
		if($assignid == '1'){
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch(206,$primaryid,$partablename,3);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,206);
			}
		} else {
			foreach($empdataids as $empidinfo) {
				if(is_array($empidinfo)) { //group of employees
					foreach($empidinfo as $empid) {
						if( !in_array($empid,$assignempids) ) {
							array_push($assignempids,$empid);
							$notimsg = $this->Basefunctions->notificationtemplatedatafetch(206,$primaryid,$partablename,3);
							if($notimsg != '') {
								$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,206);
							}
						}
					}
				} else { //individual employees
					if( !in_array($empidinfo,$assignempids) ) {
						array_push($assignempids,$empidinfo);
						$notimsg = $this->Basefunctions->notificationtemplatedatafetch(206,$primaryid,$partablename,3);
						if($notimsg != '') {
							$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,206);
						}
					}
				}
			}
		}
		//reminder template update
		if(isset($_POST['reminderleadtemplateid'])){
			$remindersmstempid = $_POST['reminderleadtemplateid'];
			if($remindersmstempid != '') {
				$remsmstempid = $remindersmstempid;
			} else { $remsmstempid = 1; }
		} else { $remsmstempid = 1; }
		if(isset($_POST['reminderemailtemplatesid'])){
			$reminderemailtempid = $_POST['reminderemailtemplatesid'];
			if($reminderemailtempid != '') {
				$rememailtempid = $reminderemailtempid;
			} else { $rememailtempid = 1; }
		} else { $rememailtempid = 1; }
		if(isset($_POST['leadcontacttype'])){
			$leadcontacttypeid = $_POST['leadcontacttype'];
			if($leadcontacttypeid == 2) {
				if(isset($_POST['contacttaskid'])){
					$contacttaskid = $_POST['contacttaskid'];
					if($contacttaskid !=  '') {
						$contacttaskid = $contacttaskid;
					} else {
						$contacttaskid = 1;
					}
					$leadtaskid = 1;
				} else {
					$contacttaskid = 1;
					$leadtaskid = 1;
				}
			} else {
				if(isset($_POST['leadtaskid'])){
					$leadtaskid = $_POST['leadtaskid'];
					if($leadtaskid !=  ''){
						$leadtaskid = $leadtaskid;
					} else {
						$leadtaskid = 1;
					}
					$contacttaskid = 1;
				}
			}
		} else {
			$contacttaskid = 1;
			$leadtaskid = 1;
		}
		$template= array('leadtemplateid'=>$remsmstempid,'emailtemplatesid'=>$rememailtempid, 'contacttaskid'=>$contacttaskid, 'leadtaskid'=>$leadtaskid);
		$this->db->where('crmtask.crmtaskid',$primaryid);
		$this->db->update('crmtask',$template);
		{//invite user notification
			$startdate = $_POST['taskstartdate'];
			$leadinviteid = $_POST['leadinviteusetid'];
			$contactinviteid = $_POST['contactinviteuserid'];
			$userinviteid = $_POST['userinviteuserid'];
			$groupinviteid = $_POST['groupinviteuserid'];
			$rolesinviteid = $_POST['rolesinviteuserid'];
			$randsinviteid = $_POST['randsinviteuserid'];
			//notification sent
			$invitetype = $_POST['invitetypeid'];
			$apikeyandsenderid = $this->defapikeyandsenderidget();
			$subaccountid = $this->subaccountdetailsfetchmodel();
			if($invitetype != '') {
				$typeid = explode(',',$invitetype);
				for($l=0;$l < count($typeid);$l++) {
					if($typeid[$l] == '7') {
						$this->inviteusernotification($leadinviteid,$contactinviteid,$userinviteid,$groupinviteid,$rolesinviteid,$randsinviteid,$primaryid,$taskname,$startdate);
					} else if($typeid[$l] == '2') {
						$invitemobilenum = $_POST['invitemobilenumber'];	
						$alnotes = $_POST['invitesmstemplatevalue'];
						if($alnotes != ''){
							$datacontent = $this->smsmergcontinformationfetchmodel($alnotes,$primaryid);
							$this->Basefunctions->sendsmsinallmodules($apikeyandsenderid['apikey'],$apikeyandsenderid['senderid'],$invitemobilenum,$datacontent,'',$primaryid,'206');
						}
						
					} else if($typeid[$l] == '3') {
						$invitemail = $_POST['inviteemailid'];
						$mailnotes = $_POST['invitemailtemplatevalue'];
						if($mailnotes != ''){
							$datacontent = $this->smsmergcontinformationfetchmodel($mailnotes,$primaryid);
							$this->mailnotificationsent($invitemail,$primaryid,$activityname,$date,$datacontent,$subaccountid);
						}
					}
				}
			}
		}
		{//workflow management for data update
			$this->Basefunctions->workflowmanageindataupdatemode(206,$primaryid,$partablename,$condstatvals);
		}
		echo "TRUE";
	}
	public function leadcontacttypefunction(){
		$detail = array();
		$id = $_GET['primarydataid'];
		$result = $this->db->select('contacttaskid,leadtaskid')
					->from('crmtask')
					->where('crmtaskid',$id)
					->limit(1)
					->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $value){
				$detail = array('id'=>$id,'contacttaskid'=>$value->contacttaskid,'leadtaskid'=>$value->leadtaskid);
			}
			$result = json_encode($detail);
		} else {
			$detail = json_encode(array('failed'=>'Failure'));
		}
		echo $result;
	}
	//delete old information
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primarydataid'];
		$msg='False';
		$filename = $this->Basefunctions->generalinformaion('crmtask','crmtaskname','crmtaskid',$id);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		{//workflow management -- for data delete
			$this->Basefunctions->workflowmanageindatadeletemode(206,$id,$primaryname,$partabname);
		}
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				$msg=array('result' => 'Denied');
			} else {
				//notification log
				$empid = $this->Basefunctions->logemployeeid;
				$notimsg = $this->Basefunctions->notificationtemplatedatafetch(206,$id,$partabname,4);
				//deleted file name
				if($notimsg != '') {
					$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,1,206);
				}
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				$msg = 'TRUE';
			}
		} else {
			//notification log
			$empid = $this->Basefunctions->logemployeeid;
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch(206,$id,$partabname,4);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,1,206);
			}
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			$msg = 'TRUE';
		}
		
		echo $msg;
	}
	//drop down value fetch model
	public function dropdownvaluefecthmodel() {
		$id = $_GET['moduleid'];
		$this->db->select('modulefieldid',false);
		$this->db->from('primaryfieldmapping');
		$this->db->where('primaryfieldmapping.moduleid',$id);
		$this->db->where('primaryfieldmapping.status',1);
		$result = $this->db->get();
		if($result ->num_rows() > 0) {
			foreach($result->result() as $row) {
				$mfid = $row->modulefieldid;
			}
		}
		$this->db->select('columnname,tablename',false);
		$this->db->from('modulefield');
		$this->db->where('modulefield.modulefieldid',$mfid);
		$this->db->where('modulefield.status',1);
		$result = $this->db->get();
		if($result ->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('filedname'=>$row->columnname,'tablename'=>$row->tablename);
			}
			echo json_encode($data);
		} else {
			echo json_encode(array('failed'=>'Failure'));
		}			
	}
	//record name fetch
	public function recordnamefetchmodel(){
		$i =0;
		$tablename = $_GET['tabname'];
		$fieldname = $_GET['fieldname'];
		$fieldid = $tablename.'id';
		$fieldid = $tablename.'id';
		$industryid = $this->Basefunctions->industryid;
		if($tablename == 'lead' || $tablename == 'contact' || $tablename == 'employee') {
			$this->db->select($fieldid.','.$fieldname.',CONCAT(salutation.salutationname, '.$fieldname.', lastname) AS name', FALSE);
			$this->db->from($tablename);
			$this->db->join('salutation','salutation.salutationid='.$tablename.'.salutationid','left outer');
		} else {
			$this->db->select($fieldname.' AS name ,'.$fieldid.','.$fieldname);
			$this->db->from($tablename);
		}
		$this->db->where($tablename.'.'.'Status',1);
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$data[$i] = array('id'=>$row->$fieldid,'name'=>$row->name);
				$i++;
			}
			echo json_encode($data); 			
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}
	}
	//task name  validation
	public function taskvalidationmodel() {
		$name = $_POST['name'];
		$date = $_POST['date'];
		$time = $_POST['time'];
		$this->db->select('crmtaskname');
		$this->db->from('crmtask');
		$this->db->where('crmtask.crmtaskname',$name);
		$this->db->where('crmtask.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data = 'TRUE';
			}
			echo json_encode($data);
		} else {
			 echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//Default api key and sender id get
	public function defapikeyandsenderidget() {
		$data = '';
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('senderid,smsprovidersettings.apikey');
		$this->db->from('smssettings');
		$this->db->join('smsprovidersettings','smsprovidersettings.smsprovidersettingsid=smssettings.smsprovidersettingsid');
		$this->db->where('smssettings.smsprovidersettingsid',2);
		$this->db->where('smssettings.status',1);
		$this->db->where("FIND_IN_SET('".$industryid."',smssettings.industryid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data = array('senderid'=>$row->senderid,'apikey'=>$row->apikey);
			}
			return $data;
		} else {
		   return $data;
		}
	}
	//Login user mobile number get
	public function usermobilenuberget() {
		$userid = $this->Basefunctions->userid;
		$data = '';
		$this->db->select('mobilenumber');
		$this->db->from('employee');
		$this->db->where('employee.employeeid',$userid);
		$result = $this->db->get();
		if($result->num_rows()  > 0 ) {
			foreach($result->result() as $row) {
				$data = $row->mobilenumber;
			}
			return $data;
		} else {
		   return $data;
		}
	}
	//invite module drop down load function 
	public function invitemoduledropdownloadfunmodel() {
	$industryid = $this->Basefunctions->industryid;
		if($industryid == 2){
			$moduleid = array('4');
			$modulename = array('User');
		} else {
			$moduleid = array('203','201','4');
			$modulename = array('Contacts','Leads','User');
		}
		$count = count($moduleid);
		if($count != 0){
			for($i=0;$i<$count;$i++){
				$data[$i] = array('datasid'=>$moduleid[$i],'dataname'=>$modulename[$i]);
			}
			echo json_encode($data);
		} else {
			 echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//view drop down load function 
	public function viewdropdownloadfunmodel() {
		$invitemoduleid = $_GET['invitemoduleid'];
		$userid = $this->Basefunctions->userid;
		$i = 0;
		$this->db->select('viewcreationid,viewcreationname');
		$this->db->from('viewcreation');
		$this->db->where('viewcreation.viewcreationmoduleid',$invitemoduleid);
		$this->db->where_in('viewcreation.createuserid',array(1,$userid));
		$this->db->where('viewcreation.status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row){
				$data[$i] = array('datasid'=>$row->viewcreationid,'dataname'=>$row->viewcreationname);
				$i++;
			}
			echo json_encode($data);
		} else {
			 echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//parent table name fetch
	public function parenttablegetmodel(){
		$mid = $_GET['moduleid'];
		if($mid == '1'){$mid = '247';}
		$this->db->select('modulemastertable');
		$this->db->from('module');
		$this->db->where('module.moduleid',$mid);
		$result = $this->db->get();
		if($result->num_rows() >0){
			foreach($result->result() as $row){
				$data = $row->modulemastertable;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
//From application - subscriber list view
	public function inviteusergrigdatafetchgridxmlview($tablename,$sortcol,$sortord,$pagenum,$rowscount,$colinfo,$viewcolmoduleids) {
		$count = count($colinfo['colmodelindex']);
		$dataset ='';
		$x = 0;
		$joinq = '';
		$ptab = array($tablename);
		$jtab = array($tablename);
		$maintable = $tablename;
		$selvalue = $tablename.'.'.$tablename.'id';
		for($k=0;$k<$count;$k++) {
			if($k==0) {
				$dataset = $colinfo['colmodelindex'][$k];
			} else{
				$dataset .= ','.$colinfo['colmodelindex'][$k];
			}
			if($x != 0) { $con = "AND"; }
			//parent join check
			$ptabjoin = "";
			$ptabjoin = $colinfo['colmodelparenttable'][$k];
			if(in_array($ptabjoin,$ptab)) {
				if(!in_array($colinfo['coltablename'][$k],$jtab) && $colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
					array_push($jtab,trim($colinfo['coltablename'][$k]));
					//employee
					$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid LIKE "%1%"';
					//group
					$joinq .= ' LEFT OUTER JOIN employeegroup ON FIND_IN_SET(employeegroup.employeegroupid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid  LIKE "%2%"';
					//user role and sub ordinate
					$joinq .= ' LEFT OUTER JOIN userrole ON FIND_IN_SET(userrole.userroleid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND ('.$maintable.'.employeetypeid LIKE "%3%" OR '.$maintable.'.employeetypeid LIKE "%4%")';
				} else {
					if($colinfo['colmodeldatatype'][$k] == 1) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						}
					} else if($colinfo['colmodeldatatype'][$k] == 2) {
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else if($colinfo['colmodeldatatype'][$k] == 3) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						}
					}
				}
			} else {
				if($colinfo['colmodeldatatype'][$k] == 1) {
					$result = $this->Basefunctions->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.','.$maintable.'.'.$primaryid.') > 0 AND '.$ptabjoin.'.status=1';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 1) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
									}
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 2) {
					$joinresult = $this->parentjointableinfo($ptabjoin,$viewcolmoduleids);
					if($joinresult->num_rows() >0) {
						foreach($joinresult->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
							//condition name
							$condcolname = $show->viewcreationcolmodelcondname;
							//condition value
							$condcolvalue = $show->viewcreationcolmodelcondvalue;
						}
						//restrict repeated parent join
						if(in_array($destab,$ptab)) {
							if($destab != $soucrcetab) {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							} else {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							}
						}
						//parent table condition
						if($condcolname != "" && $condcolvalue != "") {
							$attrcond .= " ".$con." ".$srcjointab.'.'.$condcolname."=".$condcolvalue;
							array_push($attrcondarr,$srcjointab.'.'.$joincolname);
							$x = 1;
						}
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] = $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.'='.$maintable.'.'.$primaryid.') > 0 AND '.$ptabjoin.'.status=1';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 2) {
								//attribute join
								if(!in_array($colinfo['coltablename'][$k],$jtab)) {
									array_push($jtab,trim($colinfo['coltablename'][$k]));
									$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
								}
								//condition
								if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
									$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
									array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
									$x = 1;
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 3) {
					$result = $this->Basefunctions->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						}
					}  else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.','.$maintable.'.'.$primaryid.') > 0 AND '.$ptabjoin.'.status=1';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 3) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
									}
								}
							}
						}
					}
				}
			}
			$dduitypeids = array('17','18','19','23','25','26','27','28','29');
			//fields fetch
			if($colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->Basefunctions->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].', GROUP_CONCAT(DISTINCT userrole.userrolename) AS rolename, GROUP_CONCAT(DISTINCT employeegroup.employeegroupname) AS empgrpname';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k].',userrole.userrolename AS rolename,employeegroup.employeegroupname AS empgrpname';
				}
			} else if( in_array($colinfo['colmodeluitype'][$k],$dduitypeids) ) {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->Basefunctions->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].'';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
				}
			} else {
				$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
			}
		}
		$status = $tablename.'.status IN (1)';
		$where = '1=1';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		//query
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$selvalue.' from '.$tablename.' '.$joinq.' WHERE '.$where.' AND '.$status.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		return $data;
	}
	//invite user desktop notification sent
	public function inviteusernotification($leadinviteid,$contactinviteid,$userinviteid,$groupinviteid,$rolesinviteid,$randsinviteid,$primaryid,$taskname,$startdate) {
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		if($leadinviteid != '') {
			$leadexp = explode('|',$leadinviteid);
			$leadinvite = explode(',',$leadexp[1]);
			$lcount = count($leadinvite);
			for($i=0;$i<$lcount;$i++) {
				$invitemsg = $empname." "."Invited You On"." - ".$taskname.' Starts on '.$startdate;
				$this->Basefunctions->notificationcontentadd($primaryid,'Invite',$invitemsg,$leadinvite[$i],$leadexp[0]);
			}
		}
		if($contactinviteid != '') {
			$contexp = explode('|',$contactinviteid);
			$continvite = explode(',',$contexp[1]);
			$ccount = count($continvite);
			for($i=0;$i<$ccount;$i++) {
				$invitemsg = $empname." "."Invited You On"." - ".$taskname.' Starts on '.$startdate;
				$this->Basefunctions->notificationcontentadd($primaryid,'Invite',$invitemsg,$continvite[$i],$contexp[0]);
			}
		}
		if($userinviteid != '') {
			$userexp = explode('|',$userinviteid);
			$userinvite = explode(',',$userexp[1]);
			$ucount = count($userinvite);
			for($i=0;$i<$ucount;$i++) {
				$invitemsg = $empname." "."Invited You On"." - ".$taskname.' Starts on '.$startdate;
				$this->Basefunctions->notificationcontentadd($primaryid,'Invite',$invitemsg,$userinvite[$i],$userexp[0]);
			}
		} 
		if($groupinviteid != '') {
			$j=0;
			$grpexp = explode('|',$groupinviteid);
			$grpinvite = explode(',',$grpexp[1]);
			$gcount = count($grpinvite);
			for($i=0;$i<$gcount;$i++) {
				$empdataids[$j] = $this->Basefunctions->empidfetchfromgroup($grpinvite[$i]);
				$dcount = count($empdataids[$j]);
				for($k=0;$k<$dcount;$k++) {
					$invitemsg = $empname." "."Invited You On"." - ".$taskname.' Starts on '.$startdate;
					$this->Basefunctions->notificationcontentadd($primaryid,'Invite',$invitemsg,$empdataids[$j][$k],4);
				}
			}
		}
		if($rolesinviteid != '') {
			$j=0;
			$roleexp = explode('|',$rolesinviteid);
			$roleinvite = explode(',',$roleexp[1]);
			$rcount = count($roleinvite);
			for($i=0;$i<$rcount;$i++) {
				$emproledataids[$j] = $this->Basefunctions->empidfetchfromroles($roleinvite[$i]);
				$dcount = count($emproledataids[$j]);
				for($k=0;$k<$dcount;$k++) {
					$invitemsg = $empname." "."Invited You On"." - ".$taskname.' Starts on '.$startdate;
					$this->Basefunctions->notificationcontentadd($primaryid,'Invite',$invitemsg,$emproledataids[$j][$k],4);
				}
			}
		}
	}
	//mail sent
	public function mailnotificationsent($invitemail,$primaryid,$taskname,$date,$datacontent,$subaccountid) {
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		$empmailid = $this->Basefunctions->generalinformaion('employee','emailid','employeeid',$empid);
		$mid=array();
		$mailid = explode(',',$invitemail);
		$lcount = count($mailid);
		for($i=0;$i<$lcount;$i++) {
			$mid[$i] = array( 'email' =>$mailid[$i] );
		}
		if($mid != '') {
			$message = array(
				'html' => $datacontent,
				'text' => '',
				'subject' => 'Task Invitation Mail',
				'from_email' => $empmailid,
				'from_name' => $empname,
				'to' => $mid,
				'track_opens' => true,
				'track_clicks' => true,
				'inline_css' => true,
				'url_strip_qs' => true,
				'important' => true,
				'auto_text' => true,
				'auto_html' => true,
			);
			$send_at = '';
			$this->mailsentfunction($message,$send_at);
			$ddate = date($this->Basefunctions->datef);
			$currentuserid = $this->Basefunctions->userid;
			$sql = $this->db->query("INSERT INTO crmmaillog (communicationfrom,communicationto,subject,employeeid,moduleid,commonid,emailtemplatesid,signatureid,communicationdate,createdate,lastupdatedate,createuserid,lastupdateuserid,status) VALUES ('".$empmailid."','".$invitemail."','Task Invitation Mail','".$currentuserid."','206','".$primaryid."','1','1','".$ddate."','".$ddate."','".$ddate."','".$currentuserid."','".$currentuserid."','1')");
			//notification log entry
			$pprimaryid = $this->Basefunctions->maximumid('crmmaillog','crmmaillogid');
			$notimsg = $empname." "."Sent a Test Mail to"." - ".$invitemail;
			$this->Basefunctions->notificationcontentadd($pprimaryid,'Mail',$notimsg,1,242);
			$credits = $this->db->query("UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = `addonavailablecredit` - $lcount WHERE `addonstypeid` = '3'");
			//master company id get - master db update
			$mastercompanyid = $this->Basefunctions->generalinformaion('company','mastercompanyid','companyid',2);
			$CI =& get_instance();
			$CI->load->database();
			$hostname = $CI->db->hostname; 
			$username = $CI->db->username; 
			$password = $CI->db->password; 
			$masterdb = $CI->db->masterdb; 
			$con = mysqli_connect($hostname,$username,$password,$masterdb);
			// Check connection
			if (mysqli_connect_errno())	{
				echo "Failed to connect to MySQL: " . mysqli_connect_error();
			}
			mysqli_query($con,"UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = `addonavailablecredit` - $lcount WHERE `addonstypeid` = 3 and `mastercompanyid` ='".$mastercompanyid."'");
			mysqli_close($con);
		}
	}
	//mail notification sent
	public function mailsentfunction($message,$send_at) {
		require_once 'application/third_party/mandrill-api-php/src/Mandrill.php';
		try {
			$mandrill = new Mandrill('X9WnMXpxhC8YEOUPuQ3oZw');
			$async = true; //for bulk message
			$ip_pool = '';
			$result = $mandrill->messages->send($message, $async, $ip_pool, $send_at);
		} catch(Mandrill_Error $e) {
			// Mandrill errors are thrown as exceptions
			echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			throw $e;
		}
	}
	//mobile number and email check - validate
	public function mobileandmailvalidationmodel() {
		$leadinviteid = $_POST['leadid'];
		$contactinviteid = $_POST['contactid'];
		$userinviteid = $_POST['userid'];
		$groupinviteid = $_POST['groupid'];
		$rolesinviteid = $_POST['rolesid'];
		$randsinviteid = $_POST['randsid'];
		$emailid = array();$j=0;
		$mobilenumber = array();$l =0;
		$unmobleadname = array();
		$unmailleadname = array();
		if($leadinviteid != '') {
			$leadexp = explode('|',$leadinviteid);
			$leadinvite = explode(',',$leadexp[1]);
			$lcount = count($leadinvite);
			$k =0;
			$m=0;
			for($i=0;$i<$lcount;$i++) {
				$leademailid = $this->Basefunctions->generalinformaion('lead','emailid','leadid',$leadinvite[$i]);
				if($leademailid != '') {
					$emailid[$j] = $leademailid;
					$j++;
				} else {
					$leadfname = $this->Basefunctions->generalinformaion('lead','leadname','leadid',$leadinvite[$i]);
					$leadname = $this->Basefunctions->generalinformaion('lead','lastname','leadid',$leadinvite[$i]);
					$unmailleadname[$k] = $leadfname.' '.$leadname;;
					$k++;
				}
				$leadmobilenum = $this->Basefunctions->generalinformaion('lead','mobilenumber','leadid',$leadinvite[$i]);
				if($leadmobilenum != '') {
					$mobilenumber[$l] = $leadmobilenum;
					$l++;
				} else {
					$leadfname = $this->Basefunctions->generalinformaion('lead','leadname','leadid',$leadinvite[$i]);
					$leadname = $this->Basefunctions->generalinformaion('lead','lastname','leadid',$leadinvite[$i]);
					$unmobleadname[$m] = $leadfname.' '.$leadname;;
					$m++;
				}
			}
		}
		$unmobilecontact = array();
		$unemailcontact = array();
		if($contactinviteid != '') {
			$contexp = explode('|',$contactinviteid);
			$continvite = explode(',',$contexp[1]);
			$ccount = count($continvite);
			$a = 0;
			$b = 0;
			for($i=0;$i<$ccount;$i++) {
				$contactemailid = $this->Basefunctions->generalinformaion('contact','emailid','contactid',$continvite[$i]);
				if($contactemailid != '') {
					$emailid[$j] = $contactemailid;
					$j++;
				} else {
					$contfname = $this->Basefunctions->generalinformaion('contact','contactname','contactid',$continvite[$i]);
					$contname = $this->Basefunctions->generalinformaion('contact','lastname','contactid',$continvite[$i]);
					$unemailcontact[$a] = $contfname.' '.$contname;
					$a++;
				}
				$contmobilenum = $this->Basefunctions->generalinformaion('contact','mobilenumber','contactid',$continvite[$i]);
				if($contmobilenum != '') {
					$mobilenumber[$l] = $contmobilenum;
					$l++;
				} else {
					$contfname = $this->Basefunctions->generalinformaion('contact','contactname','contactid',$continvite[$i]);
					$contname = $this->Basefunctions->generalinformaion('contact','lastname','contactid',$continvite[$i]);
					$unmobilecontact[$b] = $contfname.' '.$contname;
					$b++;
				}
			}
		}
		$unmobileuser = array();
		$unemailuser = array();
		if($userinviteid != '') {
			$userexp = explode('|',$userinviteid);
			$userinvite = explode(',',$userexp[1]);
			$ucount = count($userinvite);
			$d = 0;
			$c = 0;
			for($i=0;$i<$ucount;$i++) {
				$useremailid = $this->Basefunctions->generalinformaion('employee','emailid','employeeid',$userinvite[$i]);
				if($useremailid != '') {
					$emailid[$j] = $useremailid;
					$j++;
				} else {
					$userfname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$userinvite[$i]);
					$username = $this->Basefunctions->generalinformaion('employee','lastname','employeeid',$userinvite[$i]);
					$unemailuser[$d] = $userfname.' '.$username;;
					$d++;
				}
				$usermobilenum = $this->Basefunctions->generalinformaion('employee','mobilenumber','employeeid',$userinvite[$i]);
				if($usermobilenum != '') {
					$mobilenumber[$l] = $usermobilenum;
					$l++;
				} else {
					$userfname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$userinvite[$i]);
					$username = $this->Basefunctions->generalinformaion('employee','lastname','employeeid',$userinvite[$i]);
					$unmobileuser[$c] = $userfname.' '.$username;;
					$c++;
				}
			}
		}
		$unmobile = "";
		if(!empty($unmobleadname) || !empty($unmobilecontact) || !empty($unmobileuser)){
				
			$impunmobleadname = implode(',',$unmobleadname);
			$impunmobilecontact = implode(',',$unmobilecontact);
			$impununmobileuser = implode(',',$unmobileuser);
			$str = "";
			if($impunmobleadname){
				$str .= $impunmobleadname;
			}
			if($impunmobilecontact){
				$str .= $impunmobilecontact;
			}
			if($impununmobileuser){
				$str .= $impununmobileuser;
			}
			$unmobile = $str;
		}
		$umemailid = "";
		if(!empty($unmailleadname) || !empty($unemailcontact) || !empty($unemailuser)){
			$impunmailleadname = implode(',',$unmailleadname);
			$impunemailcontact = implode(',',$unemailcontact);
			$impunemailuser = implode(',',$unemailuser);
			$mstr = "";
			if($impunmailleadname){
				$mstr .= $impunmailleadname;
			}
			if($impunemailcontact){
				$mstr .= $impunemailcontact;
			}
			if($impunemailuser){
				$mstr .= $impunemailuser;
			}
			$umemailid = $mstr;
		}
		$data = array('mobilenum'=>$mobilenumber,'emailid'=>$emailid,'withoutmoble'=>$unmobile,'withoutmail'=>$umemailid);
		echo json_encode($data);
	}
	//reminder validation
	public function reminderintervalchecknmodel() {
		//reminder details
		$reminder = $_GET['reminderval'];
		$remindervalue = $this->remindervalueget($reminder);
		//data details
		$startdate = $_GET['sdate'];
		$currentdate = date('d-m-Y', time());
		$starttime = $_GET['stime'];
		date_default_timezone_set("Asia/Kolkata");
		$currentdate = date('d-m-Y H:i:s', time());
		$selectdate = $startdate.' '.$starttime;
		if(strtotime($startdate) > strtotime($currentdate)) {
			$data = 'True';
			echo json_encode($data);
		} else {
			$currenttime = date('H:i:s', time());
			if(strtotime($starttime) >= strtotime($currenttime)) {
				$diff = $starttime - $currenttime;
				if($diff = '1') {
					$time = strtotime($starttime);
					$time = $time - ($remindervalue * 60);
					$date = date("H:i:s", $time);
					if(strtotime($date) >= strtotime($currenttime)) {
						$datetime2 = new DateTime($starttime);
						$datetime1 = new DateTime($currenttime);
						$interval = $datetime2->diff($datetime1);
						$elapsed = $interval->format('%i');
						if($elapsed >= $remindervalue) {
							$data = 'True';
						} else {
							$data = 'False';
						}
					} else {
						$data = 'False';
					} 
				} else if($diff = '0') {
					$time = strtotime($starttime);
					$time = $time - ($remindervalue * 60);
					$date = date("H:i:s", $time);
					if(strtotime($date) >= strtotime($currenttime)) {
						$datetime2 = new DateTime($starttime);
						$datetime1 = new DateTime($currenttime);
						$interval = $datetime2->diff($datetime1);
						$elapsed = $interval->format('%i');
						if($elapsed >= $remindervalue) {
							$data = 'True';
						} else {
							$data = 'False';
						}
					} else {
						$data = 'False';
					} 
				} else {
					$data = 'True';
				}
			} else {
				$data = 'totalFalse';
			}
			echo json_encode($data);
		}
	}
	//reminder value get
	public function remindervalueget($reminder) {
		$reminid = explode(',',$reminder);
		$industryid = $this->Basefunctions->industryid;
		for($i=0;$i<count($reminid);$i++) {
			$this->db->select('reminderintervalname');
			$this->db->from('reminderinterval');
			$this->db->where_in('reminderinterval.reminderintervalid',$reminid[$i]);
			$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
			$result = $this->db->get();
			if($result->num_rows() > 0) {
				foreach($result->result() as $row) {
					$data = $row->reminderintervalname;
				}
			}
		}
		return $data;
	}
	//recurrence data fetch
	public function recurrecneondaynmodel() {
		$i=0;
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('recurrenceontheid,recurrenceonthename');
		$this->db->from('recurrenceonthe');
		$this->db->where("FIND_IN_SET('206',recurrenceonthe.moduleid) >", 0);
		$this->db->where('recurrenceonthe.status',1);
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->recurrenceontheid,'dataname'=>$row->recurrenceonthename);
				$i++;
			}
			echo json_encode($data);
		} else {
			 echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//fetch grid data information dynamic view //for view
    public function viewdynamicdatainfofetch($sidx,$sord,$start,$limit,$wh,$creationid,$rowid,$viewcolmoduleids) {
		$whereString="";
		$groupid="";
		//grid column title information fetch
		$colinfo = $this->Basefunctions->gridinformationfetchmodel($creationid);
		//main table
		$maintable = $_GET['maintabinfo'];
		$selvalue = $maintable.'.'.$rowid;
		$in = 0;
		//generate joins
		$counttable=0;
		if(count($colinfo)>0) {
			$counttable=count($colinfo['coltablename']);
		}
		$jtab = array($maintable);
		$ptab = array($maintable);
		$joinq = "";
		//attribute where condition
		$attrcond = "";
		$attrcondarr = array();
		$x = 0;
		$con = "";
		for($k=0;$k<$counttable;$k++) {
			if($x != 0) { $con = "AND"; }
			//parent join check
			$ptabjoin = "";
			$ptabjoin = $colinfo['colmodelparenttable'][$k];
			if(in_array($ptabjoin,$ptab)) {
				if(!in_array($colinfo['coltablename'][$k],$jtab) && $colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
					array_push($jtab,trim($colinfo['coltablename'][$k]));
					//employee
					$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid LIKE "%1%"';
					//group
					$joinq .= ' LEFT OUTER JOIN employeegroup ON FIND_IN_SET(employeegroup.employeegroupid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid  LIKE "%2%"';
					//user role and sub ordinate
					$joinq .= ' LEFT OUTER JOIN userrole ON FIND_IN_SET(userrole.userroleid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND ('.$maintable.'.employeetypeid LIKE "%3%" OR '.$maintable.'.employeetypeid LIKE "%4%")';
				} else {
					if($colinfo['colmodeldatatype'][$k] == 1) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
							}
						}
					} else if($colinfo['colmodeldatatype'][$k] == 2) {
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else if($colinfo['colmodeldatatype'][$k] == 3) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0';
							}
						}
					}
				}
			} else {
				if($colinfo['colmodeldatatype'][$k] == 1) {
					$result = $this->Basefunctions->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
							}
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$rowid.','.$maintable.'.'.$rowid.') > 0';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 1) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
									}
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 2) {
					$joinresult = $this->Basefunctions->parentjointableinfo($ptabjoin,$viewcolmoduleids);
					if($joinresult->num_rows() >0) {
						foreach($joinresult->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
							//condition name
							$condcolname = $show->viewcreationcolmodelcondname;
							//condition value
							$condcolvalue = $show->viewcreationcolmodelcondvalue;
						}
						//restrict repeated parent join
						if(in_array($destab,$ptab)) {
							if($destab != $soucrcetab) {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0';
								}
							} else {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0';
								}
							}
						}
						//parent table condition
						if($condcolname != "" && $condcolvalue != "") {
							$attrcond .= " ".$con." ".$srcjointab.'.'.$condcolname ."=". $condcolvalue;
							array_push($attrcondarr,$srcjointab.'.'.$joincolname);
							$x = 1;
						}
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] = $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$rowid.'='.$maintable.'.'.$rowid.') > 0';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 2) {
								//attribute join
								if(!in_array($colinfo['coltablename'][$k],$jtab)) {
									array_push($jtab,trim($colinfo['coltablename'][$k]));
									$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0';
								}
								//condition
								if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
									$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
									array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
									$x = 1;
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 3) {
					$result = $this->Basefunctions->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0';
							}
						}
					}  else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$rowid.','.$maintable.'.'.$rowid.') > 0';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 3) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0';
									}
								}
							}
						}
					}
				}
			}
			$dduitypeids = array('17','18','19','23','25','26','27','28','29');
			//fields fetch
			if($colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->Basefunctions->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].', GROUP_CONCAT(DISTINCT userrole.userrolename) AS rolename, GROUP_CONCAT(DISTINCT employeegroup.employeegroupname) AS empgrpname';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k].',userrole.userrolename AS rolename,employeegroup.employeegroupname AS empgrpname';
				}
			} else if( in_array($colinfo['colmodeluitype'][$k],$dduitypeids) ) {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->Basefunctions->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].'';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
				}
			} else {
				$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
			}
		}
		//attr condition generate
		for($xi=0; $xi < ( count($attrcondarr)-1 ); $xi++ ) {
			$attrcond .= " AND ".$attrcondarr[0].'='.$attrcondarr[$xi+1];
		}
		if( $attrcond == "" ) { $attrcond = "1=1";}
		//custom condition generate
		$cuscondition = " AND 1=1";
		if( isset($_GET['cuscondfieldsname']) ) {
			$fieldsname = explode(',',$_GET['cuscondfieldsname']);
			$condvalues = explode(',',$_GET['cuscondvalues']);
			$condtables = explode(',',$_GET['cuscondtablenames']);
			$condjoinid = explode(',',$_GET['cusjointableid']);
			$m=0;
			foreach($fieldsname AS $values) {
				if($condvalues[$m] != "") {
					if(in_array($condtables[$m],$ptab)) {
						$tabname = explode('.',$values);
						//restrict repeated join
						if(!in_array($condtables[$m],$jtab)) {
							array_push($jtab,$condtables[$m]);
							$joinq=$joinq.' LEFT OUTER JOIN '.$condtables[$m].' ON '.$tabname[0].'.'.$condjoinid[$m].'='.$maintable.'.'.$condjoinid[$m];
						}
					}
					$cuscondition .= " AND ".$values."=".$condvalues[$m];
					$m++;
				}
			}
		}
		//generate condition
		$w = '1=1';
		if($wh == "1") {
			$wh = $w;
		}
		$conditionvalarray = $this->Basefunctions->conditioninfofetch($creationid);
		$c = count($conditionvalarray);
		if($c>0) {
			$whereString=$this->Basefunctions->viewwhereclausegeneration($conditionvalarray);
		} else {
			$whereString="";
		}
		if($maintable == 'employee') {
			$status = $maintable.'.status NOT IN (3,8)';
		} else {
			$status = $maintable.'.status NOT IN (0,3,8)';
		}
		$li= 'LIMIT '.$start.','.$limit;
		$actsts = $this->Basefunctions->activestatus;
		//fetch module rules based employee list[apply rule]
		$moduleid = explode(',',$viewcolmoduleids);
		$ruleid =  $this->Basefunctions->moduleruleidfetch($moduleid[0]);
		$cusruledata = $this->Basefunctions->customrulefetch($moduleid[0]);
		$roleid = $this->Basefunctions->userroleid;
		$userid = $this->Basefunctions->userid;
		$custrulecond = " ";
		$rulecond="";
		if($ruleid == 0) {
			$assignemp = $this->Basefunctions->checkassigntofield($maintable,'employeetypeid');
			$empids = $this->Basefunctions->subrolesempidfetch($roleid);
			$subempid = implode(',',$empids);
			$empid = ( ($subempid!="")? $subempid.','.$userid : $userid);
			if($assignemp == 'true') {
				//custom rule
				if( sizeof($cusruledata) > 0 ) {
					$custrulecond = " AND ( ".$maintable.".createuserid IN (".$empid.") OR ( ";
					foreach($cusruledata as $key => $customrule) {
						if($customrule['totypeid'] == '2') { //group
							$custrulecond .= "(".$maintable.".employeetypeid=".$customrule['totypeid']." AND ".$maintable.".employeeid=".$customrule['sharedto'].")";
							$custrulecond .= ' OR ';
						} else if( ($customrule['totypeid'] == '3' && $customrule['sharedto'] == $roleid) ) { //roles
							$custrulecond .= "(".$maintable.".employeetypeid=".$customrule['totypeid']." AND ".$maintable.".employeeid=".$customrule['sharedto'].")";
							$custrulecond .= ' OR ';
						} else if( $customrule['totypeid'] == '4' ) { //role and subordinate
							$roleids = $this->Basefunctions->roleididfetchfromrolesandsubroles($customrule['sharedto']);
							$roleid = implode(',',$roleids);
							$custrulecond .= "(".$maintable.".employeetypeid=".$customrule['totypeid']." AND ".$maintable.".employeeid IN (".$roleid.") )";
							$custrulecond .= ' OR ';
						}
					}
					$custrulecond .= "(".$maintable.".employeetypeid=1 AND ".$maintable.".employeeid=".$this->userid.")";
					$custrulecond .= " ) )";
				} else {
					$rulecond = ( ($empid != '')? ' AND ('.$maintable.'.createuserid IN ('.$empid.') OR '.$maintable.'.employeetypeid=1 AND '.$maintable.'.employeeid='.$this->userid.')' : ' AND 1=1' );
				}
			} else {
				$rulecond = ( ($empid != '')? ' AND '.$maintable.'.createuserid IN ('.$empid.')' : ' AND 1=1' );
			}
		}
		//query statements
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$selvalue.' from '.$maintable.' '.$joinq.' WHERE '.$status.' AND '.$attrcond.' AND '.$wh.''.$whereString.' GROUP BY '.$maintable.'.'.$maintable.'id ORDER BY'.' '.$sidx.' '.$sord.' '.$li);
		$finalresult=array($colinfo,$data);
		return $finalresult;
    }
	//template name fetch
	public function templateddvalfetchmodel() {
		$i=0;
		$industryid = $this->Basefunctions->industryid;
		$tablename = $_GET['tablename'];
		$fieldid = $_GET['fieldid'];
		$fieldname = $_GET['fieldname'];
		$moduleid = $_GET['moduleid'];
		$type = $_GET['type'];
		$this->db->select("$fieldid,$fieldname");
		$this->db->from("$tablename");
		$this->db->where("$tablename".'.moduleid',$moduleid);
		if($type == 'sms') {
			$this->db->where("$tablename".'.smssendtypeid',2);
			$this->db->where("$tablename".'.templatetypeid',2);
		}
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->where("$tablename".'.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}	
	}
	//template value fetch on update
	public function templatenamefetchmodel() {
		$id = $_GET['datarowid'];
		$this->db->select('emailtemplatesid,leadtemplateid');
		$this->db->from('crmtask');
		$this->db->where('crmtask.crmtaskid',$id);
		$result = $this->db->get();
		if($result->num_rows() >0 ) {
			foreach($result->result() as $row){
				$data = array('emailtemplatesid'=>$row->emailtemplatesid,'leadtemplateid'=>$row->leadtemplateid);
			}
			echo json_encode($data);
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}	
	}
	//sms tempalte file name fetch
	public function smsfilenamefetchmodel() {
		$smstempid = $_GET['smstempid'];
		$this->db->select('leadtemplatecontent_editorfilename');
		$this->db->from('templates');
		$this->db->where('templates.templatesid',$smstempid);
		$result = $this->db->get();
		if($result->num_rows() >0 ) {
			foreach($result->result() as $row){
				$data = array('templatename'=>$row->leadtemplatecontent_editorfilename);
			}
			echo json_encode($data);
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}	
	}
	//mail template details fetch
	public function mailfilenamefetchmodel() {
		$mailtempid = $_GET['mailtempid'];
		$this->db->select('emailtemplatesemailtemplate_editorfilename');
		$this->db->from('emailtemplates');
		$this->db->where('emailtemplates.emailtemplatesid',$mailtempid);
		$result = $this->db->get();
		if($result->num_rows() >0 ) {
			foreach($result->result() as $row){
				$data = array('templatename'=>$row->emailtemplatesemailtemplate_editorfilename);
			}
			echo json_encode($data);
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}	
	}
	//sms merge template content information
	public function smsmergcontinformationfetchmodel($mergetemp,$recordid) {
		$resultset = array();
		$moduleid = '205';
		$parenttable = 'crmtask';
		$result = $this->gettablerowmappedcontent($mergetemp,$parenttable,$recordid,$moduleid);
		return $result;
	} 
	//table data fetch
	function gettablerowmappedcontent($datacontent,$parenttable,$id,$moduleid) {
		$htmlcontent = $datacontent;
		$finalcont = $datacontent;
		$tabpattern = "/<table ?.*>.*<\/table>/U";
		preg_match_all($tabpattern,$datacontent,$tabmatches);
		foreach($tabmatches as $tabmatch) { //contain more than one table(s)
			$tabcontent="";
			foreach($tabmatch as $tabcontentmatach) {//contain single table
				$tabcontent = $tabcontentmatach;
				$trpattern = "/<tr ?.*>.*<\/tr>/U";
				preg_match_all($trpattern,$tabcontentmatach, $trmatches);
				foreach($trmatches as $trmatch) {
					$prevtrcontent="";
					foreach($trmatch as $trdata) {
						$prevtrcontent = $trdata;
						$tdpattern = "/<td ?.*>.*<\/td>/U";
						preg_match_all($tdpattern,$trdata, $tdmatches);
						foreach($tdmatches as $tdmatch) {//contain multiple td in single row
							$matchelement = array();
							$matcheledata = array();
							$prevmatchelem = array();
							$tabrowcellcount = 0;
							$tdmatchcountcheck = "true";
							//check row contain multiple td cells
							foreach($tdmatch as $tddata) { //check cell td contents
								preg_match_all ("/{.*}/U", $tddata,$contentmatch); //content match
								foreach($contentmatch as $mccontent) { //check table cell contain multiple elements 
									if(count($mccontent) >= 1) {
										$tabrowcellcount++;
									}
								}
							}
							$tdmatchcountcheck = ( (count($tdmatch)>1 && $tabrowcellcount>1)? "true":"false" );
							if($tdmatchcountcheck == "false") { //if table cell contain multiple value and single row td
								foreach($tdmatch as $tddata) {
									$pretdcontent = $tddata;
									preg_match_all ("/{.*}/U", $tddata, $contentmatch); //content match
									foreach($contentmatch as $mcontent) { //individual td data
										foreach($mcontent as $cellmatachdata) { //fetch data for td matches
											if( $cellmatachdata != '{S.Number}' ) {
												$displaydata = $this->mergcontinformationfetchmodel($cellmatachdata,$parenttable,$id,$moduleid);
												if(count($displaydata)>1) {
													$data = $displaydata[0];
												} else {
													$data = $displaydata[0];
												}
											} else {
												$data = " ";
											}
											$tddata = preg_replace('~'.$cellmatachdata.'~',$data, $tddata); //cell data replace
										}
									}
									$trdata = preg_replace('~'.$pretdcontent.'~',$tddata,$trdata); //td data replace to row
								}
								$tabcontentmatach = preg_replace('~'.$prevtrcontent.'~',$trdata,$tabcontentmatach);//row data replace to table
							} else {
								foreach($tdmatch as $tddata) { //if table cell contain single value
									$pretdcontent = $tddata;
									preg_match_all ("/{.*}/U", $tddata, $contentmatch); //content match
									foreach($contentmatch as $mcontent) { //individual td data
										foreach($mcontent as $cellmatachdata) { //fetch data for td matches
											if( $cellmatachdata == '{S.Number}' ) {
												$displaydata = 'ss890ss';
											} else {
												$displaydata = $this->mergcontinformationfetchmodel($cellmatachdata,$parenttable,$id,$moduleid);
											}
											$matchelement[] = $cellmatachdata;
											$matcheledata[] = $displaydata;
										}
									}
								}
								//multiple content mapping
								$datas = array();
								$slnumkey = array_search('ss890ss',$matcheledata,false);
								$sno = 1;
								$sm = 0;
								//convert to row data based on column
								foreach($matcheledata as $key => $matchvals) {
									$m=0;
									$sno = 1;
									if($slnumkey !== false) { //sno available
										if(is_array($matchvals)) {
											foreach($matchvals as $val) {
												$datas[$m][$slnumkey] = $sno;
												$datas[$m][$key] = $val;
												$m++;
												$sno++;
											}
										} else if($matchvals != 'ss890ss') {
											$datas[$m][$key] = $matchvals;
											$m++;
										}
									} else { //sno is not available
										if(is_array($matchvals)) {
											foreach($matchvals as $val) {
												$datas[$m][$key] = $val;
												$m++;
											}
										} else {
											$datas[$m][$key] = $matchvals;
											$m++;
										}
									}
								}
								$trdatasets = "";
								$trdataset = "";
								if(count($datas) > 0) {
									foreach($datas as $matchdata) { //each row
										$trdataset = $trdata;
										$h=0;
										for($i=0;$i<count($matchdata);$i++) {
											if(isset($matchdata[$i])) {
												if(is_array($matchdata[$i])) {
													if(count($matchdata)>1) {
														$data = implode('<br/>',$matchdata[$i]);
													} else {
														$data = implode(',',$matchdata[$i]);
													}
												} else {
													$data = $matchdata[$i];
												}
												$trdataset = preg_replace('~'.$matchelement[$h].'~',$data,$trdataset); //td data replace to row
												$h++;
											}
										}
										$trdatasets .= $trdataset;
									}
									$trdata =  $trdatasets;
								}
								$tabcontentmatach = preg_replace('~'.$prevtrcontent.'~',$trdata,$tabcontentmatach);//row data replace to table
							}
							$tabcontentmatach = preg_replace('~'.$prevtrcontent.'~',$trdata,$tabcontentmatach);//row data replace to table
						}
						$tabcontentmatach = preg_replace('~'.$prevtrcontent.'~',$trdata,$tabcontentmatach);//row data replace to table
					}
					$htmlcontent = preg_replace('~'.$tabcontent.'~',$tabcontentmatach,$htmlcontent); //table data replace to content
					$finalcont = $htmlcontent;
				}
 			}
		}
		preg_match_all ("/{.*}/U", $finalcont, $contmatch);
		foreach($contmatch as $bocontent) {
			foreach($bocontent as $bomatch) {
				if($bomatch != "{S.Number}") {
					$htmldata = $this->mergcontinformationfetchmodel($bomatch,$parenttable,$id,$moduleid);
					$finalcont = preg_replace('~'.$bomatch.'~',implode(',',$htmldata),$finalcont);
				} else {
					$finalcont = preg_replace('~'.$bomatch.'~','',$finalcont);
				}
			}
		}
		return $finalcont;
	}
	//remove special chars
	function removespecialchars($content) {
		$symbols = array('+','-','/','%','~','!','@','#','$','^','&','*','_','=','|','(',')');
		foreach($symbols as $symbol) {
			switch($symbol) {
				case '(':
					$content = preg_replace('~\(~','890sss', $content);
					break;
				case ')':
					$content = preg_replace('~\)~','sss890', $content);
					break;
				case '+':
					$content = preg_replace('~\+~','ssplus890', $content);
					break;
				case '~':
					$content = preg_replace('~\~~','ssstilt890', $content);
					break;
				case '!':
					$content = preg_replace('~\!~','sssastr890', $content);
					break;
				case '@':
					$content = preg_replace('~\@~','sssat890', $content);
					break;
				case '#':
					$content = preg_replace('~\#~','ssshash890', $content);
					break;
				case '^':
					$content = preg_replace('~\^~','ssscap890', $content);
					break;
				case '*':
					$content = preg_replace('~\*~','sssstar890', $content);
					break;
				case '=':
					$content = preg_replace('~\=~','ssseqw890', $content);
					break;
				case '|':
					$content = preg_replace('~\|~','ssspipe890', $content);
					break;
			}
		}
		return $content;
	}
	//remove special chars
	function addspecialchars($content) {
		$symbols = array('+','-','/','%','~','!','@','#','$','^','&','*','_','=','|','(',')');
		foreach($symbols as $symbol) {
			switch($symbol) {
				case '(':
					$content = preg_replace('~890sss~','(', $content);
					break;
				case ')':
					$content = preg_replace('~sss890~',')', $content);
					break;
				case '+':
					$content = preg_replace('~ssplus890~','+', $content);
					break;
				case '~':
					$content = preg_replace('~ssstilt890~','~', $content);
					break;
				case '!':
					$content = preg_replace('~sssastr890~','!', $content);
					break;
				case '@':
					$content = preg_replace('~sssat890~','@', $content);
					break;
				case '#':
					$content = preg_replace('~ssshash890~','#', $content);
					break;
				case '^':
					$content = preg_replace('~ssscap890~','^', $content);
					break;
				case '*':
					$content = preg_replace('~sssstar890~','*', $content);
					break;
				case '=':
					$content = preg_replace('~ssseqw890~','=', $content);
					break;
				case '|':
					$content = preg_replace('~ssspipe890~','|', $content);
					break;
			}
		}
		return $content;
	}
	//Filed check
	public function tablefieldnamecheck($tblname,$fieldname) {
		$fields = $this->db->field_exists($fieldname,$tblname);
		if($fields == '1') {
			return 'Yes';
		} else {
			return 'No';
		}
	}
	//merge content data fetch
	public function mergcontinformationfetchmodel($mergegrp,$parenttable,$id,$moduleid) {
		$relmodids = $this->fetchrelatedmodulelist($moduleid);
		$primaryid = $id;
		$database = $this->db->database;
		$resultset = array();
		$mergeindval = $mergegrp;
		$formatortype = "1";
		$id = $primaryid;
		//parent child field concept
		$elname = array('parentcategoryid','parentstoragecategoryid','parentaccountid');
		$fname = array('categoryname','storagecategoryname','accountname');
		$fnameid = array('categoryid','storagecategoryid','accountid');
		$tname = array('category','storagecategory','account');
		
		$empid = $this->Basefunctions->userid;
			$newdata = array();
			$tempval = substr($mergeindval,1,(strlen($mergeindval)-2));
			$newval = explode('.',$tempval);
			$reltabname = explode(':',$newval[0]);
			$table = $reltabname[0];
			$uitypeid = 2;
			//chk editor field name
			$chkeditfname = explode('_',$newval[1]);
			$editorfname = ( (count($chkeditfname) >= 2)? $chkeditfname[1] : '');
			if($table!='REL' && $table!='GEN') { //parent table fields information
				$tablename = trim($reltabname[0]);
				$tabfield = $newval[1];
				//field fetch parent table fetch
				$fieldnamechk = explode('-',$tabfield);
				$fldname = ( (count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
				$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
				$uitypeid = ( (count($fieldinfo) > 0)? $fieldinfo['uitype'] : 1);
				if($fldname != 'attributesetid') {
					//get user company & branch id
					//check its parent child field concept
					if(in_array($fldname,$elname)) {
						$key = array_search($fldname,$elname);
						$pid = $id;
						if($key != NULL) {
							//parent id get
							$result = $this->db->select("$tname[$key].$elname[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$id)->where($tname[$key].'.status',1)->get();
							if($result->num_rows() > 0) {
								foreach($result->result()as $row) {
									$pid=$row->$elname[$key];
								}
								if($pid!=0) {
									//parent name
									$result = $this->db->select("$tname[$key].$fname[$key],$tname[$key].$fnameid[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$pid)->where($tname[$key].'.status',1)->get();
									if($result->num_rows() > 0) {
										foreach($result->result()as $row) {
											$resultset[]=$row->$fname[$key];
										}
									}
								}
							}
						}
					}
					else {
						//field fetch parent table fetch
						$fieldnamechk = explode('-',$tabfield);
						$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
						$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
						if(count($fieldinfo)>0) {
							$uitypeid = $fieldinfo['uitype'];
							//fetch parent table join id
							$relpartable = $fieldinfo['partab'];
							$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
							$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
							$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
							//fetch original field of picklist data in view creation 
							$viewfieldinfo = $this->viewfieldsinformation($tablename,$tabfield,$fieldinfo['modid']);
							$tabfieldname = ( (count($viewfieldinfo)>0)? $viewfieldinfo['joinfieldname'] : $tabfield );
							$jointabfieldid = ( (count($viewfieldinfo)>0)? $viewfieldinfo['condfiledname'] : $tabfield );
							//join relation table and main table
							$joinq="";
							if($relpartable!=$tablename) {
								$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id AND '.$relpartable.'.status=1';
							}
							//main table join
							if($mainjointable!=$parenttable) {
								$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id AND '.$relpartable.'.status=1';
							}
							//if its picklist data
							$newfield = substr( $tabfieldname,( strlen($tabfieldname)-2 ));
							$tabname = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
							$tbachk = $this->tableexitcheck($database,$tabname);
							if( $newfield == "id" && $tbachk != 0 ) {
								$newtable = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
								$newjfield = $newtable."name";
								$whtable = (($tablename=="company" || $tablename=="branch")? $tablename : $parenttable);
								$newjdata = $this->db->query("SELECT ".$newtable.".".$newtable."id AS columdataid,".$newtable.".".$newjfield." AS columdata FROM ".$newtable." LEFT OUTER JOIN ".$tablename." ON ".$tablename.".".$tabfieldname." = ".$newtable.".".$tabfieldname."".$joinq." WHERE ".$tablename.".".$whtable."id = ".$id." AND ".$newtable.".status=1 AND ".$tablename.".status=1",false);
								if($newjdata->num_rows() >0) {
									foreach($newjdata->result() as $jkey) {
										$resultset[] = $jkey->columdata;
									}
								} else {
									$resultset[] = " ";
								}
							}
							else {
								//main table data sets information fetching
								$fldcountchk = explode('-',$tabfield);
								$tablcolumncheck = $this->tablefieldnamecheck($tablename,$parenttable."id");
								if( ( $parenttable == $tablename && (count($fldcountchk)) < 2 ) || ( $tablcolumncheck == 'false' && (count($fldcountchk)) < 2 ) ) {
									$newdata = $this->db->select("".$tabfield." AS columdata")->from($tablename)->where($tablename.".".$tablename."id",$id)->where($tablename.".status",1)->get()->result();
								}
								else {
									$cond = "";
									$addfield = explode('-',$tabfield);
									if( (count($addfield)) >= 2 ) {
										$datafiledname=$addfield[1];
										if(in_array('PR',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=2';
										} else if(in_array('SE',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=3'; 
										} else if(in_array('BI',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=4';
										} else if(in_array('SH',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=5';
										} else if(in_array('CW',$addfield)) {
											$formatortype = "CW";
										} else if(in_array('CF',$addfield)) {
											$formatortype = "CF";
										} else if(in_array('CS',$addfield)) {
											$formatortype = "CS";
										}
										if($tablename!=$parenttable) {
											$tablcolumncheck = $this->tablefieldnamecheck($tablename,"addresstypeid");
											$cond = ( ($tablcolumncheck == 'true')? $cond : ' ');
											$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename." JOIN ".$parenttable." ON ".$parenttable.".".$parenttable."id = ".$tablename.".".$parenttable."id WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1".$cond."")->result();
										} else {
											$newdata = $this->db->select(''.$datafiledname.' AS columdata',false)->from($tablename)->where($tablename.'id',$id)->where($tablename.'.status',1)->get()->result();
										}
									}
									else {
										$newdata = $this->db->query("SELECT ".$tabfield." AS columdata FROM ".$tablename." JOIN ".$parenttable." ON ".$parenttable.".".$parenttable."id = ".$tablename.".".$parenttable."id WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1")->result();
									}
								}
								foreach($newdata as $key) {
									if($tabfield == 'companylogo') {
										$imgname = $key->columdata;
										if(filter_var($imgname,FILTER_VALIDATE_URL)) {
											$resultset[] = '<img src="'.$imgname.'" />';
										} else {
											if( file_exists($imgname) ) {
												$resultset[] = '<img src="'.$imgname.'" style="width:11em; height:3em" />';
											} else {
												$resultset[] = " ";
											}
										}
									}
									else {
										$currencyinfo = $this->currencyinformation($parenttable,$id,$moduleid);
										if($formatortype=="CW") { //convert to word
											if(count($currencyinfo)>0) {
												$resultset[] = $this->convert->numtowordconvert($key->columdata,$currencyinfo['name'],$currencyinfo['subcode']);
											} else {
												$resultset[] = $this->convert->numtowordconvert($key->columdata);
											}
										} else if($formatortype=="CF") { //currenct format
											if(count($currencyinfo)>0) {
												$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
											} else {
												$resultset[] = $this->convert->formatcurrency($key->columdata);
											}
										} else if($formatortype=="CS") { //currency with symbol
											if(count($currencyinfo)>0) {
												$amt = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
												$resultset[] = $currencyinfo['symbol'].' '.$amt;
											} else {
												$resultset[] = $currencyinfo['symbol'].' '.$this->convert->formatcurrency($key->columdata);
											}
										} else {
											if(is_numeric($key->columdata)) {
												if($uitypeid == '4' || $uitypeid == '5' || $uitypeid == '7') {
													if(count($currencyinfo)>0) {
														$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
													} else {
														$resultset[] = $this->convert->formatcurrency($key->columdata);
													}
												} else {
													$resultset[] = $key->columdata;
												}
											} else {
												if($editorfname=='editorfilename') {
													if( file_exists($key->columdata) ) {
														$datas = $this->geteditorfilecontent($key->columdata);
														$datas = preg_replace('~a10s~','&nbsp;', $datas);
														$datas = preg_replace('~<br>~','<br />', $datas);
														$resultset[] = $datas;
													} else {
														$resultset[] = '';
													}
												} else {
													$resultset[] = $key->columdata;
												}
											}
										}
									}
								}
							}
						}
					}
				} 
				else {
					//attribute value fetch for non relational modules
					$fieldnamechk = explode('-',$tabfield);
					$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
					$fieldinfo = $this->fetchfieldparenttable($table,$fldname);
					if(count($fieldinfo)>0) {
						$uitypeid = $fieldinfo['uitype'];
						//fetch parent table join id
						$relpartable = $fieldinfo['partab'];
						$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
						$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
						$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
						//join relation table and main table
						$joinq="";
						if($parenttable!=$table) {
							$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$table.'.'.$parenttable.'id';
						}
						//fetch attribute name and values
						$i=0;
						$datasets = array();
						$attrsetids = $this->db->query(" SELECT ".$table.".".$table."id as Id,".$table.".attributesetid as attrsetid,".$table.".attributes as attributeid,".$table.".attributevalues as attributevalueid FROM ".$table.$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$mainjointable.".status=1 AND ".$table.".status=1",false)->result();
						foreach($attrsetids as $attrset) {
							$datasets[$i] = array('id'=>$attrset->Id,'attrsetid'=>$attrset->attrsetid,'attrnames'=>$attrset->attributeid,'attrvals'=>$attrset->attributevalueid);
							$i++;
						}
						$resultset = array();
						foreach($datasets as $set) {
							$valid = explode('_',$set['attrvals']);
							$nameid = explode('_',$set['attrnames']);
							$n=1;
							$vi = 0;
							$attrdatasets = array();
							for($h=0;$h<count($valid);$h++) {
								$attrval = '';
								$attrname = '';
								if($valid[$h]!='') {
									//attribute value fetch
									$attrvaldata = $this->db->query(' SELECT attributevalueid,attributevaluename FROM attributevalue WHERE attributevalueid='.$valid[$h].' AND status=1',false)->result();
									foreach($attrvaldata as $valsets) {
										$attrval = $valsets->attributevaluename;
									}
									//attribute name fetch
									$attrnamedata = $this->db->query(' SELECT attributeid,attributename FROM attribute WHERE attributeid='.$nameid[$h].' AND status=1',false)->result();
									foreach($attrnamedata as $namesets) {
										$attrname = $namesets->attributename;
									}
								}
								$attrset = "";
								if( $attrval!='' ) {
									$attrset = $n.'.'.$attrname.': '.$attrval;
									++$n;
								}
								$attrdatasets[$vi] = $attrset;
								$vi++;
							}
							$resultset[] = implode('<br />',$attrdatasets);
						}
					}
				}
			} 
			else if($table=='REL') { //related module fields information fetch
				$tablename = trim($reltabname[1]);
				$tabfield = trim($newval[1]);
				$fieldnamechk = explode('-',$tabfield);
				$fldname = ( (count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
				if($fldname != 'attributesetid') {
					//get user company & branch id
					//check its parent child field concept
					$fldnamechk = explode('-',$tabfield);
					$fldname = ( (count($fldnamechk) >= 2)? $fldnamechk[1] : $tabfield);
					if(in_array($fldname,$elname)) {
						$key = array_search($fldname,$elname);
						$pid = $id;
						if($key != NULL) {
							//parent id get
							$result = $this->db->select("$tname[$key].$elname[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$id)->where($tname[$key].'.status',1)->get();
							if($result->num_rows() > 0) {
								foreach($result->result()as $row) {
									$pid=$row->$elname[$key];
								}
								if($pid!=0) {
									//parent name
									$result = $this->db->select("$tname[$key].$fname[$key],$tname[$key].$fnameid[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$pid)->where($tname[$key].'.status',1)->get();
									if($result->num_rows() > 0) {
										foreach($result->result()as $row) {
											$resultset[]=$row->$fname[$key];
										}
									}
								}
							}
						}
					}
					else {
						//field fetch parent table fetch
						$fieldnamechk = explode('-',$tabfield);
						$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
						$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
						if(count($fieldinfo)>0) {
							$uitypeid = $fieldinfo['uitype'];
							//fetch parent table join id
							$relpartable = $fieldinfo['partab'];
							$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
							$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
							$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
							//fetch original field of picklist data in view creation 
							$viewfieldinfo = $this->viewfieldsinformation($tablename,$tabfield,$fieldinfo['modid']);
							$tabfieldname = ( (count($viewfieldinfo)>0)? $viewfieldinfo['joinfieldname'] : $tabfield );
							$jointabfieldid = ( (count($viewfieldinfo)>0)? $viewfieldinfo['condfiledname'] : $tabfield );
							//join relation table and main table
							$joinq="";
							if($relpartable!=$tablename) {
								$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id AND '.$relpartable.'.status=1';
							}
							//main table join
							if($mainjointable==$parenttable) {
								$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id AND '.$mainjointable.'.status=1';
							} else {
								$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id AND '.$mainjointable.'.status=1';
								$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$mainjointable.'.'.$parenttable.'id AND '.$parenttable.'.status=1';
							}
							//if its picklist data
							$newfield = substr( $tabfieldname,( strlen($tabfieldname)-2 ));
							$tabname = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
							$tblchk = $this->tableexitcheck($database,$tabname);
							if( $newfield == "id" && $tblchk != 0 ) {
								$newtable = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
								$newjfield = $newtable."name";
								$whtable = (($tablename=="company" || $tablename=="branch")? $tablename : $parenttable);
								$newjdata = $this->db->query("SELECT ".$newtable.".".$newtable."id AS columdataid,".$newtable.".".$newjfield." AS columdata FROM ".$newtable." LEFT OUTER JOIN ".$tablename." ON ".$tablename.".".$jointabfieldid." = ".$newtable.".".$tabfieldname."".$joinq." WHERE ".$parenttable.".".$whtable."id = ".$id." AND ".$mainjointable.".status=1 AND ".$newtable.".status=1 AND ".$tablename.".status=1");
								if($newjdata->num_rows() >0) {
									foreach($newjdata->result() as $jkey) {
										$resultset[] = $jkey->columdata;
									}
								} else {
									$resultset[] = " ";
								}
							}
							else {
								//main table data sets information fetching
								$fldcountchk = explode('-',$tabfield);
								$tablcolumncheck = $this->tablefieldnamecheck($tablename,$parenttable."id");
								if( ( $parenttable == $tablename && (count($fldcountchk)) < 2 ) || ( $tablcolumncheck == 'false' && (count($fldcountchk)) < 2 ) ) {
									$newdata = $this->db->query("SELECT ".$tablename.".".$tabfield." AS columdata FROM ".$tablename."".$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1")->result();
								}
								else {
									$cond = "";
									$addfield = explode('-',$tabfield);
									if( (count($addfield)) >= 2 ) {
										$datafiledname=$addfield[1];
										if(in_array('PR',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=2';
										} else if(in_array('SE',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=3'; 
										} else if(in_array('BI',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=4';
										} else if(in_array('SH',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=5';
										} else if(in_array('CW',$addfield)) {
											$formatortype = "CW";
										} else if(in_array('CF',$addfield)) {
											$formatortype = "CF";
										} else if(in_array('CS',$addfield)) {
											$formatortype = "CS";
										}
										$tablcolumncheck = $this->tablefieldnamecheck($tablename,"addresstypeid");
										$cond = ( ($tablcolumncheck == 'true')? $cond : ' ');
										if($tablename!=$parenttable) {
											$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename."".$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1".$cond."")->result();
										} else {
											$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename." WHERE ".$tablename."id = ".$id." AND ".$tablename.".status=1")->result();
										}
									}
									else {
										$newdata = $this->db->query("SELECT ".$tabfield." AS columdata FROM ".$tablename."".$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1".$cond."")->result();
									}
								}
								foreach($newdata as $key) {
									if($tabfield == 'companylogo') {
										$imgname = $key->columdata;
										if(filter_var($imgname, FILTER_VALIDATE_URL)) {
											$resultset[] = '<img src="'.$imgname.'" />';
										} else {
											if( file_exists($imgname) ) {
												$resultset[] = '<img src="'.$imgname.'" style="width:11em; height:3em" />';
											} else {
												$resultset[] = " ";
											}
										}
									}
									else {
										$currencyinfo = $this->currencyinformation($parenttable,$id,$moduleid);
										if($formatortype=="CW") { //convert to word
											if(count($currencyinfo)>0) {
												$resultset[] = $this->convert->numtowordconvert($key->columdata,$currencyinfo['name'],$currencyinfo['subcode']);
											} else {
												$resultset[] = $this->convert->numtowordconvert($key->columdata);
											}
										} else if($formatortype=="CF") { //currenct format
											if(count($currencyinfo)>0) {
												$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
											} else {
												$resultset[] = $this->convert->formatcurrency($key->columdata);
											}
										} else if($formatortype=="CS") { //currency with symbol
											if(count($currencyinfo)>0) {
												$amt = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
												$resultset[] = $currencyinfo['symbol'].' '.$amt;
											} else {
												$resultset[] = $currencyinfo['symbol'].' '.$this->convert->formatcurrency($key->columdata);
											}
										} else {
											if(is_numeric($key->columdata)) {
												if( $uitypeid == '4' || $uitypeid == '5' || $uitypeid == '7' ) {
													if(count($currencyinfo)>0) {
														$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
													} else {
														$resultset[] = $this->convert->formatcurrency($key->columdata);
													}
												} else {
													$resultset[] = $key->columdata;
												}
											} else {
												if($editorfname=='editorfilename') {
													if( file_exists($key->columdata) ) {
														$datas = $this->geteditorfilecontent($key->columdata);
														$datas = preg_replace('~a10s~','&nbsp;', $datas);
														$datas = preg_replace('~<br>~','<br />', $datas);
														$resultset[] = $datas;
													} else {
														$resultset[] = '';
													}
												} else {
													$resultset[] = $key->columdata;
												}
											}
										}
									}
								}
							}
						}
					}
				} 
				else {
					//attribute value fetch
					$fieldnamechk = explode('-',$tabfield);
					$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
					$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
					if(count($fieldinfo)>0) {
						$uitypeid = $fieldinfo['uitype'];
						//fetch parent table join id
						$relpartable = $fieldinfo['partab'];
						$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
						$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
						$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
						//join relation table and main table
						$joinq="";
						if($relpartable!=$tablename) {
							$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id';
						}
						//main table join
						if($mainjointable==$parenttable) {
							$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id';
						} else {
							$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id';
							$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$mainjointable.'.'.$parenttable.'id';
						}
						//fetch attribute name and values
						$i=0;
						$datasets = array();
						$attrsetids = $this->db->query(" SELECT ".$tablename.".".$tablename."id as Id,".$tablename.".attributesetid as attrsetid,".$tablename.".attributes as attributeid,".$tablename.".attributevalues as attributevalueid FROM ".$tablename.$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$mainjointable.".status=1 AND ".$tablename.".status=1",false)->result();
						foreach($attrsetids as $attrset) {
							$datasets[$i] = array('id'=>$attrset->Id,'attrsetid'=>$attrset->attrsetid,'attrnames'=>$attrset->attributeid,'attrvals'=>$attrset->attributevalueid);
							$i++;
						}
						$resultset = array();
						foreach($datasets as $set) {
							$valid = explode('_',$set['attrvals']);
							$nameid = explode('_',$set['attrnames']);
							$n=1;
							$vi = 0;
							$attrdatasets = array();
							for($h=0;$h<count($valid);$h++) {
								$attrval = '';
								$attrname = '';
								if($valid[$h]!='') {
									//attribute value fetch
									$attrvaldata = $this->db->query(' SELECT attributevalueid,attributevaluename FROM attributevalue WHERE attributevalueid='.$valid[$h].' AND status=1',false)->result();
									foreach($attrvaldata as $valsets) {
										$attrval = $valsets->attributevaluename;
									}
									//attribute name fetch
									$attrnamedata = $this->db->query(' SELECT attributeid,attributename FROM attribute WHERE attributeid='.$nameid[$h].' AND status=1',false)->result();
									foreach($attrnamedata as $namesets) {
										$attrname = $namesets->attributename;
									}
								}
								$attrset = "";
								if( $attrval!='' ) {
									$attrset = $n.'.'.$attrname.': '.$attrval;
									++$n;
								}
								$attrdatasets[$vi] = $attrset;
								$vi++;
							}
							$resultset[] = implode('<br />',$attrdatasets);
						}
					}
				}
			}
			else if($table=='GEN') {
				$tablename = trim($reltabname[1]);
				$tabfield = $newval[1];
				//field fetch parent table fetch
				$fieldnamechk = explode('-',$tabfield);
				$fldname = ( (count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
				$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
				$uitypeid = ( (count($fieldinfo) > 0)? $fieldinfo['uitype'] : 1);
				if($fldname != 'attributesetid') {
					//get user company & branch id
					$tabinfo = array('company','companycf','branch','branchcf');
					if( in_array($tablename,$tabinfo) ) {
						if($tablename == 'company' || $tablename == 'companycf') {
							$compquery = $this->db->query('select company.companyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND company.status=1')->result();
							foreach($compquery as $key) {
								$id = $key->companyid;
							}
						} else if($tablename == 'branch' || $tablename == 'branchcf') {
							$brquery = $this->db->query('select branch.branchid from branch join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND branch.status=1')->result();
							foreach($brquery as $key) {
								$id = $key->branchid;
							}
						} else { //employee
							$id = $this->Basefunctions->userid;
						}
					} else { //employee
						$id = $this->Basefunctions->userid;
					}
					//check its parent child field concept
					if(in_array($fldname,$elname)) {
						$key = array_search($fldname,$elname);
						$pid = $id;
						if($key != NULL) {
							//parent id get
							$result = $this->db->select("$tname[$key].$elname[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$id)->where($tname[$key].'.status',1)->get();
							if($result->num_rows() > 0) {
								foreach($result->result()as $row) {
									$pid=$row->$elname[$key];
								}
								if($pid!=0) {
									//parent name
									$result = $this->db->select("$tname[$key].$fname[$key],$tname[$key].$fnameid[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$pid)->where($tname[$key].'.status',1)->get();
									if($result->num_rows() > 0) {
										foreach($result->result()as $row) {
											$resultset[]=$row->$fname[$key];
										}
									}
								}
							}
						}
					}
					else {
						//field fetch parent table fetch
						$fieldnamechk = explode('-',$tabfield);
						$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
						$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
						$moduleid = $fieldinfo['modid'];
						if(count($fieldinfo)>0) {
							$uitypeid = $fieldinfo['uitype'];
							//fetch parent table join id
							$relpartable = $fieldinfo['partab'];
							$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
							$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $relpartable );
							$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
							//fetch original field of picklist data in view creation 
							$viewfieldinfo = $this->viewfieldsinformation($tablename,$tabfield,$fieldinfo['modid']);
							$tabfieldname = ( (count($viewfieldinfo)>0)? $viewfieldinfo['joinfieldname'] : $tabfield );
							$jointabfieldid = ( (count($viewfieldinfo)>0)? $viewfieldinfo['condfiledname'] : $tabfield );
							//join relation table and main table
							$joinq="";
							if($relpartable!=$tablename) {
								$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id';
							}
							//if its picklist data
							$newfield = substr( $tabfieldname,( strlen($tabfieldname)-2 ));
							$tabname = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
							$tbachk = $this->tableexitcheck($database,$tabname);
							if( $newfield == "id" && $tbachk != 0 ) {
								$newtable = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
								$newjfield = $newtable."name";
								$whtable = (($tablename=="company" || $tablename=="branch")? $tablename : $relpartable);
								$newjdata = $this->db->query("SELECT ".$newtable.".".$newtable."id AS columdataid,".$newtable.".".$newjfield." AS columdata FROM ".$newtable." LEFT OUTER JOIN ".$tablename." ON ".$tablename.".".$tabfieldname." = ".$newtable.".".$tabfieldname."".$joinq." WHERE ".$tablename.".".$whtable."id = ".$id." AND ".$newtable.".status=1 AND ".$tablename.".status=1",false);
								if($newjdata->num_rows() >0) {
									foreach($newjdata->result() as $jkey) {
										$resultset[] = $jkey->columdata;
									}
								} else {
									$resultset[] = " ";
								}
							}
							else {
								//main table data sets information fetching
								$fldcountchk = explode('-',$tabfield);
								$tablcolumncheck = $this->tablefieldnamecheck($tablename,$relpartable."id");
								if( ( $relpartable == $tablename && (count($fldcountchk)) < 2 ) || ( $tablcolumncheck == 'false' && (count($fldcountchk)) < 2 ) ) {
									$newdata = $this->db->select("".$tabfield." AS columdata")->from($tablename)->where($tablename.".".$tablename."id",$id)->where($tablename.".status",1)->get()->result();
								}
								else {
									$cond = "";
									$addfield = explode('-',$tabfield);
									if( (count($addfield)) >= 2 ) {
										$datafiledname=$addfield[1];
										if(in_array('PR',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=2';
										} else if(in_array('SE',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=3'; 
										} else if(in_array('BI',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=4';
										} else if(in_array('SH',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=5';
										} else if(in_array('CW',$addfield)) {
											$formatortype = "CW";
										} else if(in_array('CF',$addfield)) {
											$formatortype = "CF";
										} else if(in_array('CS',$addfield)) {
											$formatortype = "CS";
										}
										if($tablename!=$relpartable) {
											$tablcolumncheck = $this->tablefieldnamecheck($tablename,"addresstypeid");
											$cond = ( ($tablcolumncheck == 'true')? $cond : ' ');
											$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename." JOIN ".$relpartable." ON ".$relpartable.".".$relpartable."id = ".$tablename.".".$relpartable."id WHERE ".$relpartable.".".$relpartable."id = ".$id." AND ".$tablename.".status=1 AND ".$relpartable.".status=1".$cond."")->result();
										} else {
											$newdata = $this->db->select(''.$datafiledname.' AS columdata',false)->from($tablename)->where($tablename.'id',$id)->where($tablename.'.status',1)->get()->result();
										}
									}
									else {
										$newdata = $this->db->query("SELECT ".$tabfield." AS columdata FROM ".$tablename." JOIN ".$relpartable." ON ".$relpartable.".".$relpartable."id = ".$tablename.".".$relpartable."id WHERE ".$relpartable.".".$relpartable."id = ".$id." AND ".$tablename.".status=1 AND ".$relpartable.".status=1")->result();
									}
								}
								foreach($newdata as $key) {
									if($tabfield == 'companylogo') {
										$imgname = $key->columdata;
										if(filter_var($imgname,FILTER_VALIDATE_URL)) {
											$resultset[] = '<img src="'.$imgname.'" />';
										} else {
											if( file_exists($imgname) ) {
												$resultset[] = '<img src="'.$imgname.'" style="width:11em; height:3em" />';
											} else {
												$resultset[] = " ";
											}
										}
									}
									else {
										if($formatortype=="CW") { //convert to word
											$currencyinfo = $this->currencyinformation($tablename,$id,$moduleid);
											if(count($currencyinfo)>0) {
												$resultset[] = $this->convert->numtowordconvert($key->columdata,$currencyinfo['name'],$currencyinfo['subcode']);
											} else {
												$resultset[] = $this->convert->numtowordconvert($key->columdata);
											}
										} else if($formatortype=="CF") { //currenct format
											$currencyinfo = $this->currencyinformation($tablename,$id,$moduleid);
											if(count($currencyinfo)>0) {
												$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
											} else {
												$resultset[] = $this->convert->formatcurrency($key->columdata);
											}
										} else if($formatortype=="CS") { //currency with symbol
											$currencyinfo = $this->currencyinformation($tablename,$id,$moduleid);
											if(count($currencyinfo)>0) {
												$amt = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
												$resultset[] = $currencyinfo['symbol'].' '.$amt;
											} else {
												$resultset[] = $currencyinfo['symbol'].' '.$this->convert->formatcurrency($key->columdata);
											}
										} else {
											$currencyinfo = $this->currencyinformation($tablename,$id,$moduleid);
											if(is_numeric($key->columdata)) {
												if($uitypeid == '4' || $uitypeid == '5' || $uitypeid == '7') {
													if(count($currencyinfo)>0) {
														$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
													} else {
														$resultset[] = $this->convert->formatcurrency($key->columdata);
													}
												} else {
													$resultset[] = $key->columdata;
												}
											} else {
												if($editorfname=='editorfilename') {
													if( file_exists($key->columdata) ) {
														$datas = $this->geteditorfilecontent($key->columdata);
														$datas = preg_replace('~a10s~','&nbsp;', $datas);
														$datas = preg_replace('~<br>~','<br />', $datas);
														$resultset[] = $datas;
													} else {
														$resultset[] = '';
													}
												} else {
													$resultset[] = $key->columdata;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		return $resultset;
	}
	//table name check
	public function tableexitcheck($database,$newtable) {
		$counttab=0;
		$tabexits = $this->db->query("SELECT COUNT(*) AS tabcount FROM information_schema.tables WHERE table_schema = '".$database."'AND table_name = '".$newtable."'");
		foreach($tabexits->result() as $tkey) { 
			$counttab = $tkey->tabcount;
		};
		return $counttab;
	}
	//fetch parent table information
	public function fetchfieldparenttable($tablname,$fieldname) {
		$pardata = array();
		$datas = $this->db->query('SELECT modulefieldid,uitypeid,parenttable,moduletabid  FROM modulefield WHERE columnname LIKE "%'.$fieldname.'%" AND tablename LIKE "%'.$tablname.'%" AND status IN (1,10)',false);
		foreach($datas->result() as $data) {
			$pardata = array('uitype'=>$data->uitypeid,'partab'=>$data->parenttable,'modid'=>$data->moduletabid);
		}
		return $pardata;
	}
	//fetch related module information
	public function fetchrelatedmodulelist($moduleid) {
		$relmodids = array();
		$relmoduleids = $this->db->select('modulerelationid,relatedmoduleid')->from('modulerelation')->where('moduleid',$moduleid)->get();
		foreach($relmoduleids->result() as $reldata) {
			$relmodids[] = $reldata->relatedmoduleid;
		}
		return $relmodids;
	}
	//fetch main table join information
	public function fetchjoincolmodelname($relpartable,$moduleid) {
		$joindata = array();
		$datas = $this->db->query('SELECT modulerelationid,parentfieldname,parenttable FROM modulerelation WHERE moduleid = '.$moduleid.' AND relatedtable LIKE "%'.$relpartable.'%" AND status=1',false);
		foreach($datas->result() as $data) {
			$joindata = array('relid'=>$data->modulerelationid,'joinfieldname'=>$data->parentfieldname,'jointabl'=>$data->parenttable);
		}
		return $joindata;
	}
	//fetch relation and view type 3 field information
	public function viewfieldsinformation($tablename,$tabfield,$modid) {
		$datasets = array();
		$viewfieldinfo = $this->db->query('SELECT viewcreationcolumnid,viewcreationparenttable,viewcreationjoincolmodelname,viewcreationcolmodelcondname FROM viewcreationcolumns WHERE viewcreationparenttable LIKE "%'.$tablename.'%" AND viewcreationcolmodelcondname LIKE "%'.$tabfield.'%" AND viewcreationtype=3 AND viewcreationmoduleid='.$modid.' AND status=1');
		foreach($viewfieldinfo->result() as $viewdata) {
			$datasets = array('viewcolid'=>$viewdata->viewcreationcolumnid,'joinfieldname'=>$viewdata->viewcreationjoincolmodelname,'condfiledname'=>$viewdata->viewcreationcolmodelcondname);
		}
		return $datasets;
	}
	//fetch currency information
	public function currencyinformation($parenttable,$id,$moduleid) {
		$datasets = array();
		$colmodeltable = "";
		$cid = 1;
		//modulerelation
		$datasets = $this->db->select('viewcreationcolumnid,viewcreationcolmodeltable,viewcreationjoincolmodelname,viewcreationparenttable,viewcreationtype,viewcreationcolmodelcondname',false)->from('viewcreationcolumns')->where('viewcreationmoduleid',$moduleid)->where('viewcreationjoincolmodelname','currencyid')->where('status',1)->get();
		if($datasets->num_rows()>0) {
			foreach($datasets->result() as $data) {
				$colname =  $data->viewcreationcolmodeltable;
				$colmodeltable = $data->viewcreationcolmodeltable;
				$joincolmodelname= $data->viewcreationjoincolmodelname;
				$colmodelpartable = $data->viewcreationparenttable;
				$viewtype = $data->viewcreationtype;
				$viewcolcondname = $data->viewcreationcolmodelcondname;
			}
		}
		if($colmodeltable!= "") {
			$joinq="";
			if($viewtype==3) {
				if($colmodelpartable!="currency") {
					if($colmodelpartable == $parenttable) {
						$joinq = 'LEFT OUTER JOIN '.$colmodelpartable.' ON '.$colmodelpartable.'.'.$viewcolcondname.'='.$colmodeltable.'.'.$joincolmodelname;
					} else {
						$joinq = 'LEFT OUTER JOIN '.$colmodelpartable.' ON '.$colmodelpartable.'.'.$viewcolcondname.'='.$colmodeltable.'.'.$joincolmodelname;
						$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$colmodelpartable.'.'.$parenttable.'id';
					}
				}
			} else {
				if($colmodelpartable!="currency") {
					if($colmodelpartable == $parenttable) {
						$joinq = 'LEFT OUTER JOIN '.$colmodelpartable.' ON '.$colmodelpartable.'.'.$joincolmodelname.'='.$colmodeltable.'.'.$joincolmodelname;
					} else {
						$joinq = 'LEFT OUTER JOIN '.$colmodelpartable.' ON '.$colmodelpartable.'.'.$joincolmodelname.'='.$colmodeltable.'.'.$joincolmodelname;
						$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$colmodelpartable.'.'.$parenttable.'id';
					}
				}
			}
			//echo $joinq;
			$curdatasets = $this->db->query('select currency.currencyid from currency '.$joinq.' where '.$parenttable.'id='.$id.'')->result();
			foreach($curdatasets as $data) {
				$cid = $data->currencyid;
			}
		} else {
			$empid = $this->Basefunctions->userid;
			$compquery = $this->db->query('select company.companyid,company.currencyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.'')->result();
			foreach($compquery as $key) {
				$cid = $key->currencyid;
			}
		}
		$curdatasets = $this->db->select('currencyid,currencyname,currencycode,symbol,currencycountry,subunit',false)->from('currency')->where('currencyid',$cid)->get();
		foreach($curdatasets->result() as $datas) {
			$datasets = array('id'=>$datas->currencyid,'name'=>$datas->currencyname,'code'=>$datas->currencycode,'symbol'=>$datas->symbol,'country'=>$datas->currencycountry,'subcode'=>$datas->subunit);
		}
		return $datasets;
	}
	//check the mail add ons added or not 
	public function subaccountdetailsfetchmodel() {
		$data = '';
		$this->db->select('generalsettingid,mandrillaccountnumber');
		$this->db->from('generalsetting');
		$this->db->where('generalsetting.companyid',2);
		$this->db->where('generalsetting.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = $row->mandrillaccountnumber;
			}
		} else {
			$data = '';
		}
		return $data;
	}
	/* ---- For General ---- */
	public function gettemplatesidmodel(){
		$id = $_GET['taskid'];
		if($id) {
			$this->db->select('leadtemplateid,emailtemplatesid,invitemode,invitesmstemplate,invitemailtemplate,leadid,inviteviewname');
			$this->db->from('crmtask');
			$this->db->where('crmtask.crmtaskid',$id);
			$result = $this->db->get()->row_array();
			$smsid = $result['leadtemplateid'];
			$emailid = $result['emailtemplatesid'];
			$invmodeid = $result['invitemode'];
			$invsmsid = $result['invitesmstemplate'];
			$invemailid = $result['invitemailtemplate'];
			$moduleid = $result['leadid'];
			$viewid = $result['inviteviewname'];
			$data = array(
				'status'=>'success',
				'smsid' => $smsid,
				'emailid' => $emailid,
				'invmodeid' => $invmodeid,
				'invsmsid' => $invsmsid,
				'invemailid' => $invemailid,
				'viewid' => $viewid,
				'moduleid' => $moduleid,
			);
		} else {
			$data = array('status'=>'FAILED');
		}
		echo json_encode($data);
	}
	//preview and print pdf
	public function generateprinthtmlfile($print_data) {
		$this->load->model('Printtemplates/Printtemplatesmodel');
		//defaults
		$printdetails['moduleid'] = '1';
		$htmldatacontents='';
			
		$templateid = ( (isset($print_data['templateid']))? $print_data['templateid'] : 1);
		$id = ( (isset($print_data['primaryid']))? $print_data['primaryid'] : 1);
		$this->db->select('emailtemplates.emailtemplatesemailtemplate_editorfilename,emailtemplates.emailtemplatesid,emailtemplates.moduleid,emailtemplates.emailtemplatesname');
		$this->db->from('emailtemplates');
		$this->db->where('emailtemplates.emailtemplatesid',$templateid);
		$result = $this->db->get();
		foreach($result->result() as $rowdata) {
			$printdetails['contentfile'] =  $rowdata->emailtemplatesemailtemplate_editorfilename;
			$printdetails['moduleid'] =  $rowdata->moduleid;
			$printdetails['templatename'] = $rowdata->emailtemplatesname;
		}
		$parenttable = $this->Basefunctions->generalinformaion('module','modulemastertable','moduleid',$printdetails['moduleid']);
	
		//body data
		$bodycontent = $this->Printtemplatesmodel->filecontentfetch($printdetails['contentfile']);
		$bodycontent = preg_replace('~a10s~','&nbsp;', $bodycontent);
		$bodycontent = preg_replace('~<br>~','<br />', $bodycontent);
		$bodycontent = $this->Printtemplatesmodel->removespecialchars($bodycontent);
		$bodyhtml = $this->Printtemplatesmodel->generateprintingdata($bodycontent,$parenttable,$print_data,$printdetails['moduleid']);
		$bodyhtml = $this->Printtemplatesmodel->addspecialchars($bodyhtml);
		return $bodyhtml;
	}
	//preview and print pdf
	public function smsgenerateprinthtmlfile($print_data) {
		$this->load->model('Printtemplates/Printtemplatesmodel');
		//defaults
		$printdetails['moduleid'] = '1';
		$htmldatacontents='';
			
		$templateid = ( (isset($print_data['templateid']))? $print_data['templateid'] : 1);
		$id = ( (isset($print_data['primaryid']))? $print_data['primaryid'] : 1);
		$this->db->select('templates.leadtemplatecontent_editorfilename,templates.templatesid,templates.moduleid,templates.templatesname');
		$this->db->from('templates');
		$this->db->where('templates.templatesid',$templateid);
		$result = $this->db->get();
		foreach($result->result() as $rowdata) {
			$printdetails['contentfile'] =  $rowdata->leadtemplatecontent_editorfilename;
			$printdetails['moduleid'] =  $rowdata->moduleid;
			$printdetails['templatename'] = $rowdata->templatesname;
		}
		$parenttable = $this->Basefunctions->generalinformaion('module','modulemastertable','moduleid',$printdetails['moduleid']);
	
		//body data
		$bodycontent = $this->Printtemplatesmodel->filecontentfetch($printdetails['contentfile']);
		$bodycontent = preg_replace('~a10s~','&nbsp;', $bodycontent);
		$bodycontent = preg_replace('~<br>~','<br />', $bodycontent);
		$bodycontent = $this->Printtemplatesmodel->removespecialchars($bodycontent);
		$bodyhtml = $this->Printtemplatesmodel->generateprintingdata($bodycontent,$parenttable,$print_data,$printdetails['moduleid']);
		$bodyhtml = $this->Printtemplatesmodel->addspecialchars($bodyhtml);
		return $bodyhtml;
	}
	//retrieve lead/contact's
	public function getdropdownoptions($table) {
		$i=0;
		$id=$table.'id';
		$name=$table.'name';
		$this->db->select(''.$id.' AS Id'.','.''.$name.' AS Name,salutation.salutationname AS SName,lastname AS LName');
		$this->db->from($table);
		$this->db->join('salutation','salutation.salutationid='.$table.'.salutationid','left outer');
		$this->db->where($table.'.industryid',$this->Basefunctions->industryid);
		$this->db->where_in(''.$table.'.status',array(1,8));
		$data=$this->db->get();
		if($data->num_rows() > 0) {
			foreach($data->result() as $info) {
				$sname = (($info->SName!='')? $info->SName.' ':'');
				$lname = (($info->LName!='')? ' '.$info->LName:'');
				$datam[$i] = array('id'=>$info->Id,'name'=>$sname.$info->Name.$lname);
				$i++;
			}
			echo json_encode($datam);
		}
		else{
			echo json_encode(array('fail'=>'FAILED'));
		}
	}
}     