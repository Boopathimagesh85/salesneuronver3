<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Storage extends MX_Controller{
    public function __construct() {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Storage/Storagemodel');
		$this->load->model('Printtemplates/Printtemplatesmodel');
    }
    //first basic hitting view
    public function index() {        
    	$moduleid = array(7);
    	sessionchecker($moduleid);
		/*action*/
		$data['actionassign'] = $this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$industryid = $this->Basefunctions->industryid;
		if($industryid == 3) {
			$data['storagequantity'] = $this->Basefunctions->get_company_settings('storagequantity');
			$data['storagedisplay'] = $this->Basefunctions->get_company_settings('storagedisplay');
			$data['weight_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		}
		//view
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
      	$this->load->view('Storage/storageview',$data);
	}
	//create storage
	public function newdatacreate() {  
    	$this->Storagemodel->newdatacreatemodel();
    }
	//information fetchstorage
	public function fetchformdataeditdetails() {
		$moduleid = 7;
		$this->Storagemodel->informationfetchmodel($moduleid);
	}
	//update storage
    public function datainformationupdate() {
        $this->Storagemodel->datainformationupdatemodel();
    }
	//delete storage
    public function deleteinformationdata() {
        $moduleid = 7;
		$this->Storagemodel->deleteoldinformation($moduleid);
    }
    //reload the storage list element
    public function getcounterparentid() {
    	$this->Storagemodel->getcounterparent();
    }    
    //new tree create
    public function treedatafetchfun() {
		$tname=explode("-",$_POST['tabname']);
		$tablename = $tname[0];
    	$manopt = $_POST['mandval'];
    	$lablname = $_POST['fieldlabl'];
    	$mandlab = "";
    	if($manopt!='') {
    		$mandlab = "<span class='mandatoryfildclass'>*</span>";
    	}
    	echo '<label>'.$lablname.$mandlab.'</label>';
    	echo divopen( array("id"=>"dl-menu","class"=>"dl-menuwrapper","style"=>"z-index:2;margin-bottom: 1rem") );
    	$type="button";
    	echo '<button name="treebutton" type="'.$type.'" id="treebutton" class="dl-trigger treemenubtnclick" tabindex="102" style="height:2.2rem;color:#ffffff;font-size: 1.45rem;padding: 0.1rem;padding-top:0.3rem;background-color:#546E7A;"><i class="material-icons">format_indent_increase</i></button>';
    	$txtoptions = array("style"=>"width:83%;display:inline;position:absolute;height:2.12rem;padding-left: 5px;","readonly"=>"readonly","tabindex"=>"101","class"=>"".$manopt."");
    	echo text('parent'.$tablename.'','',$txtoptions);
    	echo hidden('parent'.$tablename.'id','');
    	echo '<ul class="dl-menu maintreegenerate large-12 medium-6 small-12 column" id="storagelistuldata">';
    	$dataset = $this->Basefunctions->treecreatemodel($tablename);
    	$this->createTree($dataset,0);
    	echo close('ul');
    	echo close('div');
    	echo '<input type="hidden" value="'.$manopt.'" id="treevalidationcheck" name="treevalidationcheck">';
    	echo '<input type="hidden" value="'.$lablname.'" id="treefiledlabelcheck" name="treefiledlabelcheck">';
    }
	//tree creation
	public function createTree($items = array(), $parentId = 0,$removeid = 0) {
		echo " <ul class='dl-submenu'> ";
		if($removeid == 0){
			echo '<li><a href="#" style="font-weight:bold;">Remove<i class="material-icons parentclear">close</i></a></li>';
		}
		if(count($items) > 0 ) {
			foreach ($items[$parentId] as $item) {
				echo '<li data-listname="'.$item['name'].'" data-level="'.$item['level'].'" data-listid="'.$item['id'].'" ><a href="#">'.$item['name'].'</a>';//level title
				$curId = $item['id'];
				//if there are children
				if (!empty($items[$curId])) {
					$this->createTree($items, $curId,1);
				}
				echo '</li>';
			}
		}
		echo '</ul>';
	}
	//rfid tagno unique check
	public function uniquedynamicviewnamecheck(){
		$this->Storagemodel->uniquedynamicviewnamecheckmodel();
	}
	// Storage print icon action*/
	public function storagereprint() {
		$storageid = trim($_GET['primaryid']);	
		$templateid = trim($_GET['templateid']);
		if($templateid > 1) {
			$print_data=array('templateid'=> $templateid, 'Templatetype'=>'Tagtemp','primaryset'=>'counter.counterid','primaryid'=>$storageid);
			$data = $this->Printtemplatesmodel->generatlabelprint($print_data);
			echo $data;
		} else {
			echo 'Kindly Edit the Print template of the tag in edit Screen.Since u have selected any print option earlier.';
			exit();
		}
	}
}