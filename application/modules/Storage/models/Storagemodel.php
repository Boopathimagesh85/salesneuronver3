<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Storagemodel extends CI_Model{
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//create counter
	public function newdatacreatemodel() {
		$industryid = $this->Basefunctions->industryid;
		if($industryid == 3){
			$storage_level = $this->Basefunctions->get_company_settings('counter_level'); //category_level
			if($_POST['parentcounterid'] > 0){
				$level = $this->Basefunctions->gettreelevel($_POST['parentcounterid'],'counter');
			}else{
				$level = 1;
			}
		}
		//table and fields information	
		$formfieldsname = explode(',',$_POST['storageelementsname']);
		$formfieldstable = explode(',',$_POST['storageelementstable']);
		$formfieldscolmname = explode(',',$_POST['storageelementscolmn']);
		$elementpartable = explode(',',$_POST['storageelementspartabname']);
		if($industryid == 3){
			if($level <= $storage_level)
			{
				//filter unique parent table
				$partablename =  $this->Crudmodel->filtervalue($elementpartable);
				//filter unique fields table
				$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
				$tableinfo = explode(',',$fildstable);
				$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
				$primaryid = $this->db->insert_id();
			}else{
				echo 'LEVEL'; exit;
			}
	   }else{
		   	//filter unique parent table
		   	$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		   	//filter unique fields table
		   	$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		   	$tableinfo = explode(',',$fildstable);
		   	$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
		   	$primaryid = $this->db->insert_id();
		}
		if($industryid == 3){
			$level  =0;
			$counterdefault = 0;
			$counterdefaultid = 0;
			if($_POST['parentcounterid'] > 0){
				$level = $this->Basefunctions->gettreelevel($_POST['parentcounterid'],'counter');
			} else {
				$level = 1;
			}
			if(empty($_POST['parentcounterid'])){
				
			}else{
				$counterdefault = $this->Basefunctions->singlefieldfetch('counterdefault','counterid','counter',$_POST['parentcounterid']);
				$counterdefaultid = $this->Basefunctions->singlefieldfetch('counterdefaultid','counterid','counter',$_POST['parentcounterid']);
			}
			$update_last=array(
					'counterlevel'=>$level,
					'counterdefault' =>$counterdefault,
					'counterdefaultid' =>$counterdefaultid
			);
			$this->db->where('counterid',$primaryid);
			$this->db->update('counter',$update_last);
		}
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity = ''.$user.' created Storage - '.$_POST['storagename'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Created',$activity ,$userid,7);
		echo 'TRUE';
	}
	//retrive counter data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['storageelementsname']);
		$formfieldstable = explode(',',$_GET['storageelementstable']);
		$formfieldscolmname = explode(',',$_GET['storageelementscolmn']);
		$elementpartable = explode(',',$_GET['storageelementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['storageprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$sndefault = $this->Basefunctions->singlefieldfetch('sndefault','counterid','counter',$primaryid); //Check default value
		if($sndefault == 1) {
			$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
			echo $result;
		} else {
			echo json_encode(array("fail"=>'sndefault'));
		}
	}
	//update counter
	public function datainformationupdatemodel() {
		$industryid = $this->Basefunctions->industryid;
		if($industryid == 3){
			$storage_level = $this->Basefunctions->get_company_settings('counter_level'); //storage_level
			if($_POST['parentcounterid'] > 0){
				$level = $this->Basefunctions->gettreelevel($_POST['parentcounterid'],'counter');
			}else{
				$level = 1;
			}
		}
		//table and fields information
		$formfieldsname = explode(',',$_POST['storageelementsname']);
		$formfieldstable = explode(',',$_POST['storageelementstable']);
		$formfieldscolmname = explode(',',$_POST['storageelementscolmn']);
		$elementpartable = explode(',',$_POST['storageelementspartabname']);
		if($industryid == 3){
			if($level <= $storage_level)
			{
				if($_POST['parentcounterid'] == 0){ // parent category only
					$info=0;
				}else{
					if($_POST['storageprimarydataid'] > $_POST['parentcounterid']){ // child - parent category
						$info=0;
					}else{ // parent - child category
						$info=$this->Basefunctions->singlefieldfetch('COUNT(counterid)','parentcounterid','counter',$_POST['parentcounterid']);
					}
				}
				if($info<=0){
					//filter unique parent table
					$partablename =  $this->Crudmodel->filtervalue($elementpartable);
					//filter unique fields table
					$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
					$tableinfo = explode(',',$fildstable);
					$primaryid = $_POST['storageprimarydataid'];
					$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
				}else{
					echo "FAILURE"; exit;
				}
			}else{
				echo 'LEVEL'; exit;
			}
		}else{
			//filter unique parent table
			$partablename =  $this->Crudmodel->filtervalue($elementpartable);
			//filter unique fields table
			$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
			$tableinfo = explode(',',$fildstable);
			$primaryid = $_POST['storageprimarydataid'];
			$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		}
		if($industryid == 3){
			$level  =0;
			$counterdefault = 0;
			$counterdefaultid = 0;
			if($_POST['parentcounterid'] > 0){
				$level = $this->Basefunctions->gettreelevel($_POST['parentcounterid'],'counter');
			} else {
				$level = 1;
			}
			if(empty($_POST['parentcounterid'])){
			
			}else{
				$counterdefault = $this->Basefunctions->singlefieldfetch('counterdefault','counterid','counter',$_POST['parentcounterid']);
				$counterdefaultid = $this->Basefunctions->singlefieldfetch('counterdefaultid','counterid','counter',$_POST['parentcounterid']);
			}
			$update_last=array(
					'counterlevel'=>$level,
					'counterdefault' =>$counterdefault,
					'counterdefaultid' =>$counterdefaultid
			);
			$this->db->where('counterid',$primaryid);
			$this->db->update('counter',$update_last);
		}
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity = ''.$user.' updated Storage - '.$_POST['storagename'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$activity ,$userid,7);
		echo $result;
	}
	//delete counter
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['storageelementstable']);
		$parenttable = explode(',',$_GET['storageparenttable']);
		$id = $_GET['storageprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$countername = $this->Basefunctions->singlefieldfetch('countername','counterid','counter',$id); //Storage name
		$activity = ''.$user.' deleted Storage - '.$countername.'';
		$sndefault = $this->Basefunctions->singlefieldfetch('sndefault','counterid','counter',$id); //Check default value
		if($sndefault == 2) {
			echo 'sndefault';
		} else {
			if($ruleid == 1 || $ruleid == 2) {
				$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
				if($chek == 0) {
					echo 'Denied';
				} else {
					$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
					$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,7);
					echo "TRUE";
				}
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,7);
				echo "TRUE";
			}
		}
	}
	public function getcounterparent() {
		$parentcounterid='';
		$countername='';
		$counterdefaultid='';
		$id=$_GET['id'];
		$data=$this->db->select('a.parentcounterid as parentcounterids,b.countername as counternames,a.counterdefaultid as counterdefaultid')
		->from('counter as a')
		->join('counter as b','a.parentcounterid=b.counterid')
		->where('a.counterid',$id)
		->get()->result();
		foreach($data as $in) {
			$parentcounterid=$in->parentcounterids;
			$countername=$in->counternames;
			$counterdefaultid=$in->counterdefaultid;
		}
		$a=array('parentcounterid'=>$parentcounterid,'countername'=>$countername,'counterdefaultid'=>$counterdefaultid);
		echo json_encode($a);
	}
	//unique name check
	public function uniquedynamicviewnamecheckmodel(){
		$industryid = $this->Basefunctions->industryid;
		$primaryid=$_POST['primaryid'];
		$accname=$_POST['accname'];
		$accname=explode(',',$accname);
		$partable =  'counter';
		$ptid = $partable.'id';
		$ptname = $partable.'rfidtagno';
		if($accname != "") {
			$this->db->select($ptid);
			$this->db->from($partable);
			$this->db->where_in('counter.rfidtagno',$accname);
			$this->db->where_in($partable.'.status',array(1));
			$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
			$result = $this->db->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->$ptid;
				}
			} else { echo "False"; }
		}else { echo "False"; }
	}
}