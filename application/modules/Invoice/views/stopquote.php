<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="lostoverlay" style="display:block">	
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-3 medium-6 small-10 large-centered medium-centered small-centered columns">
			<div class="row">&nbsp;</div>
			<div class="row">
				<div class="large-12 columns alertsheadercaptionstyle" style="text-align:left">
					<div class="small-12 columns"><span style="color:#ffffff;padding-right:0.5rem"><i class="material-icons">check_box</i>  </span> Confirmation</div>
				</div>				
			</div>
			<span class="firsttab" tabindex="1000"></span>
			<div class="row" style="background:#f5f5f5" >
				<div id="descriptiondivhid" class="input-field large-12 columns" id="stopnow">
					<textarea id="lostreason" class="materialize-textarea" data-prompt-position="topLeft" tabindex="1001" name="description"></textarea>
					<label for="lostreason">Reason</label>
				</div>
				<div class="large-12 large-centered columns" style="background:#f5f5f5;text-align:center" id="stoprevert">
					<span class="" style="	color: #5e6d82;font-family: segoe ui;font-size: 0.9rem;" id="stopmessage">Quotation Already Lost.Do u want revert to Draft?</span>
					<div class="large-12 column">&nbsp;</div>
				</div>
				<div class="large-12 column" style="background:#f5f5f5">&nbsp;</div>
				<div class="medium-2 large-2 columns"> &nbsp;</div>
				<div class="small-6  medium-4 large-4 columns">
					<input type="button" id="lostyes" name="" value="Yes" tabindex="1002" class="btn ffield formbuttonsalert" >	
				</div>
				<div class="small-6 medium-4 large-4 columns">
					<input type="button" id="lostno" name="" value="No" tabindex="1003" class="btn flloop formbuttonsalert alertsoverlaybtn" >	
				</div>
				<span class="lasttab" tabindex="1003"></span>
				<div class="medium-2 large-2 columns"> &nbsp;</div>
			</div>
			
			<div class="row" style="background:#f5f5f5">&nbsp;</div>
			<div class="row" style="background:#f5f5f5">&nbsp;</div>
		</div>
	</div>
	<div class="overlay alertsoverlay overlayalerts" id="bookedoverlay">	
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-3 medium-6 small-10 large-centered medium-centered small-centered columns">
			<div class="row">&nbsp;</div>
			<div class="row">
				<div class="large-12 columns alertsheadercaptionstyle" style="text-align:left">
					<div class="small-12 columns"><span style="color:#ffffff;padding-right:0.5rem"><i class="material-icons">check_box</i>  </span> Confirmation</div>
				</div>
				<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
				<div class="large-12 large-centered columns" style="background:#f5f5f5;text-align:center">
					<span class="" style="	color: #5e6d82;font-family: segoe ui;font-size: 0.9rem;" id="bookedmessage">Do You Want To Cancel This Quotation?</span>
					<div class="large-12 column">&nbsp;</div>
				</div>
			</div>
			<span class="firsttab" tabindex="1000"></span>
			<div class="row" style="background:#f5f5f5">
				<div class="medium-2 large-2 columns"> &nbsp;</div>
				<div class="small-6  medium-4 large-4 columns">
					<input type="button" id="bookedyes" name="" value="Yes" tabindex="1001" class="btn ffield formbuttonsalert" >
				</div>
				<div class="small-6 medium-4 large-4 columns">
					<input type="button" id="bookedno" name="" value="No" tabindex="1002" class="btn flloop formbuttonsalert alertsoverlaybtn" >	
				</div>
				<span class="lasttab" tabindex="1003"></span>
				<div class="medium-2 large-2 columns"> &nbsp;</div>
			</div>
			<div class="row" style="background:#f5f5f5">&nbsp;</div>
			<div class="row" style="background:#f5f5f5">&nbsp;</div>
		</div>
	</div>
</div>