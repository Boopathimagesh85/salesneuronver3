<!-- Discount Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="discountoverlay" style="overflow-y: scroll;overflow-x: hidden">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="large-3 medium-6 large-centered medium-centered columns" >
			<form method="POST" name="discountform" style="" id="discountform" action="" enctype="" class="">
				<span id="individualdiscountvalidation" class="validationEngineContainer"> 
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-10 columns" style="background:none">Discount</div>
							<div class="small-1 columns" id="discountclose" style="text-align:right;cursor:pointer;background:none">X</div>
						</div>
						<div class="large-12 columns">             
							<label>Calculation Type</label>
							<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="validate[required] chzn-select dropdownchange ffieldd" tabindex="2" name="discountcalculationtype" id="discountcalculationtype">
							<option value=""></option>
							<?php foreach($calculationtype as $key):?>
									<option value="<?php echo $key->calculationtypeid;?>">
									<?php echo $key->calculationtypeid;?>-<?php echo $key->calculationtypename;?></option>
							<?php endforeach;?>	
							</select>                  
						</div>
						<div class="large-12 columns"> 
							<label>Value</label>
							<input type="text" id="discountvalue" name="discountvalue" value="" class="validate[custom[number],funcCall[validatepercentage]]" >
						</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div class="row" style="background:#f5f5f5">
						<div class="medium-4 large-4 columns">&nbsp;</div>
						<div class="small-4 small-centered medium-4 large-4 columns">
							<input type="button" id="indiscountadd" name="" value="Submit" class="btn formbuttonsalert" >	
						</div>
						<div class="medium-4 large-4 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div class="row">&nbsp;</div>
					<!-- hidden fields -->
				</span>
			</form>
		</div>
	</div>
</div>
<!----Tax overlay----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="taxoverlay" style="overflow-y: scroll;overflow-x: hidden">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="large-3 medium-6 large-centered medium-centered columns" >
			<form method="POST" name="taxoverlayform" style="" id="taxoverlayform" action="" enctype="" class="clearformtaxoverlay">
				<span id="taxoverlayvalidation" class="validationEngineContainer"> 
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-10 columns" style="background:none">Tax Charges</div>
							<div class="small-1 columns" id="taxclose" style="text-align:right;cursor:pointer;background:none">X</div>
						</div>
						<div class="large-12 columns">             
							<label>Tax Category Name</label>
							<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd" tabindex="2" name="taxcategory" id="taxcategory">
							<option value=""></option>
							<?php foreach($category as $key):?>
									<option value="<?php echo $key->taxmasterid;?>">
									<?php echo $key->taxmasterid;?>-<?php echo $key->taxmastername;?></option>
							<?php endforeach;?>	
							</select>                  
						</div>
						<div class="taxdiv0 hidedisplay">
							<div class="large-6 columns">             
								<label id="taxlabel0"></label>
								<input type="hidden" name="tmdid_0" id="tmdid_0" />
								<input type="text" id="taxpercent_0" name="taxpercent_0" value="" class="taxpercent validate[custom[number],min[0],max[100]]" >              
								<input type="hidden" id="taxrule_0" name="taxrule_0" value="" class="" >              
							</div>
							<div class="large-6 columns">             
								<label id="taxlabel01">Amount</label>
								<input type="text" id="taxamount_0" readonly name="taxamount_0" value="" class="individualtaxamount" >
							</div>
						</div>
						<div class="taxdiv1 hidedisplay">
							<div class="large-6 columns"> 
								<label id="taxlabel1"></label>
								<input type="hidden" name="tmdid_1" id="tmdid_1" />
								<input type="text" id="taxpercent_1"  name="taxpercent_1" value="" class="taxpercent validate[custom[number],min[0],max[100]]" >
								<input type="hidden" id="taxrule_1" name="taxrule_1" value="" class="" >
							</div>
							<div class="large-6 columns">             
								<label id="taxlabel01">Amount</label>
								<input type="text" id="taxamount_1" readonly name="taxamount_1" value="" class="individualtaxamount" >
							</div>
						</div>
						<div class="taxdiv2 hidedisplay">
							<div class="large-6 columns"> 
								<label id="taxlabel2"></label>
								<input type="hidden" name="tmdid_2" id="tmdid_2" />
								<input type="text" id="taxpercent_2" name="taxpercent_2" value="" class="taxpercent validate[custom[number],min[0],max[100]]" >
								<input type="hidden" id="taxrule_2" name="taxrule_2" value="" class="" >
							</div>
							<div class="large-6 columns">             
								<label id="taxlabel01">Amount</label>
								<input type="text" id="taxamount_2" readonly name="taxamount_2" value="" class="individualtaxamount" >             
							</div>
						</div>
						<div class="taxdiv3 hidedisplay">
							<div class="large-6 columns"> 
								<label id="taxlabel3"></label>
								<input type="hidden" name="tmdid_3" id="tmdid_3" />
								<input type="text" id="taxpercent_3" name="taxpercent_3" value="" class="taxpercent validate[custom[number],min[0],max[100]]" >
								<input type="hidden" id="taxrule_3" name="taxrule_3" value="" class="" >
							</div>
							<div class="large-6 columns">             
								<label id="taxlabel01">Amount</label>
								<input type="text" id="taxamount_3" readonly name="taxamount_3" value="" class="individualtaxamount" >             
							</div>
						</div>
						<div class=''>
							<div class="large-12 columns">             
								<label id="taxlabel">Total Tax Amount</label>
								<input type="text" id="totalselling" readonly name="totalselling" value="" class="totalindividualtax" >
							</div>
						</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div class="row" style="background:#f5f5f5">
						<div class="medium-4 large-4 columns">&nbsp;</div>
						<div class="small-4 small-centered medium-4 large-4 columns">
							<input type="button" id="taxsubmit" name="taxsubmit" value="Submit" class="btn formbuttonsalert" >	
						</div>
						<div class="medium-4 large-4 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div class="row">&nbsp;</div>
					<!-- hidden fields -->
					<input type="hidden" name="taxcatid" id="taxcatid" />
				</span>
			</form>
		</div>
	</div>
</div>
<!----Additional Charge Details overlay----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="additionaloverlay" style="overflow-y: scroll;overflow-x: hidden">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="large-6 medium-6 large-centered medium-centered columns" >
			<form method="POST" name="additionaloverlayform" style="" id="additionaloverlayform" action="" enctype="" class="clearformadditionaloverlay">
				<span id="additionaloverlayvalidation" class="validationEngineContainer"> 
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-10 columns" style="background:none">Additional Charges</div>
							<div class="small-1 columns" id="additionalclose" style="text-align:right;cursor:pointer;background:none">X</div>
						</div>
						<div class="large-12 columns">             
							<label>Additional Category Name</label>
							<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd" tabindex="2" name="additionalcategory" id="additionalcategory">
							<option value=""></option>
							<?php foreach($additionalcategory as $key):?>
									<option value="<?php echo $key->additionalchargecategoryid;?>">
									<?php echo $key->additionalchargecategoryid;?>-<?php echo $key->additionalchargecategoryname;?></option>
							<?php endforeach;?>	
							</select>                  
						</div>
						<div class="adddiv0 hidedisplay">
							<div class="large-3 columns">             
								<label id="">Name</label>
								<input type="text" readonly="readonly" id="addchargename_0" name="addchargename_0" value="" class="" >              
								<input type="hidden" id="addchargeid_0" name="addchargeid_0" value="" class="" >              
							</div>
							<div class="large-3 columns">             
								<label>Type</label>
								<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd caltypedd" tabindex="2" name="additionalcaltype_0" data-val="0" id="additionalcaltype_0">
									<option value=""></option>
									<?php foreach($calculationtype as $key):?>
											<option value="<?php echo $key->calculationtypeid;?>">
											<?php echo $key->calculationtypeid;?>-<?php echo $key->calculationtypename;?></option>
									<?php endforeach;?>	
								</select>                  
							</div>
							<div class="large-3 columns">             
								<label id="">Value</label>
						<input type="text" id="addval_0" name="addval_0" value="" data-val="0" class="individualaddchargevalue validate[custom[number],funcCall[validateaddchargepercent]]" >              
							</div>
							<div class="large-3 columns">             
								<label id="">Amount</label>
								<input type="text" id="addamount_0" readonly name="addamount_0" value="" class="individualaddchargeamount" >              
							</div>
						</div>
						<div class="adddiv1 hidedisplay">
							<div class="large-3 columns">             
								<label id="">Name</label>
								<input type="text" readonly="readonly" id="addchargename_1" name="addchargename_1" value="" class="" >  
								<input type="hidden" id="addchargeid_1" name="addchargeid_1" value="" class="" >
							</div>
							<div class="large-3 columns">             
								<label>Type</label>
								<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd caltypedd" tabindex="2" data-val="1" name="additionalcaltype_1" id="additionalcaltype_1">
									<option value=""></option>
									<?php foreach($calculationtype as $key):?>
											<option value="<?php echo $key->calculationtypeid;?>">
											<?php echo $key->calculationtypeid;?>-<?php echo $key->calculationtypename;?></option>
									<?php endforeach;?>	
								</select>                  
							</div>
							<div class="large-3 columns">             
								<label id="">Value</label>
								<input type="text" id="addval_1" name="addval_1" value="" data-val="1" class="individualaddchargevalue validate[custom[number],funcCall[validateaddchargepercent]]" >              
							</div>
							<div class="large-3 columns">             
								<label id="">Amount</label>
								<input type="text" id="addamount_1" readonly name="addamount_1" value="" class="individualaddchargeamount" >              
							</div>
						</div>
						<div class="adddiv2 hidedisplay">
							<div class="large-3 columns">             
								<label id="">Name</label>
								<input type="text" readonly="readonly" id="addchargename_2" name="addchargename_2" value="" class="" >
								<input type="hidden" id="addchargeid_2" name="addchargeid_2" value="" class="" >
							</div>
							<div class="large-3 columns">             
								<label>Type</label>
								<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd caltypedd" tabindex="2" data-val="2" name="additionalcaltype_2" id="additionalcaltype_2">
									<option value=""></option>
									<?php foreach($calculationtype as $key):?>
											<option value="<?php echo $key->calculationtypeid;?>">
											<?php echo $key->calculationtypeid;?>-<?php echo $key->calculationtypename;?></option>
									<?php endforeach;?>	
								</select>                  
							</div>
							<div class="large-3 columns">             
								<label id="">Value</label>
								<input type="text" id="addval_2" name="addval_2" value="" data-val="2" class="individualaddchargevalue validate[custom[number],funcCall[validateaddchargepercent]]" >              
							</div>
							<div class="large-3 columns">             
								<label id="">Amount</label>
								<input type="text" id="addamount_2" readonly name="addamount_2" value="" class="individualaddchargeamount" >              
							</div>
						</div>
						<div class="adddiv3 hidedisplay">
							<div class="large-3 columns">             
								<label id="">Name</label>
								<input type="text" readonly="readonly" id="addchargename_3" name="addchargename_3" value="" class="" >
								<input type="hidden" id="addchargeid_3" name="addchargeid_3" value="" class="" >
							</div>
							<div class="large-3 columns">             
								<label>Type</label>
								<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd caltypedd" tabindex="2" data-val="3" name="additionalcaltype_3" id="additionalcaltype_3">
									<option value=""></option>
									<?php foreach($calculationtype as $key):?>
											<option value="<?php echo $key->calculationtypeid;?>">
											<?php echo $key->calculationtypeid;?>-<?php echo $key->calculationtypename;?></option>
									<?php endforeach;?>	
								</select>                  
							</div>
							<div class="large-3 columns">             
								<label id="">Value</label>
								<input type="text" id="addval_3" name="addval_3" value="" data-val="3" class="individualaddchargevalue validate[custom[number],funcCall[validateaddchargepercent]]" >              
							</div>
							<div class="large-3 columns">             
								<label id="">Amount</label>
								<input type="text" id="addamount_3" readonly name="addamount_3" value="" class="individualaddchargeamount" >              
							</div>
						</div>
						<div class="adddiv4 hidedisplay">
							<div class="large-3 columns">             
								<label id="">Name</label>
								<input type="text" readonly="readonly" id="addchargename_4" name="addchargename_4" value="" class="" > 
								<input type="hidden" id="addchargeid_4" name="addchargeid_4" value="" class="" >
							</div>
							<div class="large-3 columns">             
								<label>Type</label>
								<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd caltypedd" tabindex="2" data-val="4" name="additionalcaltype_4" id="additionalcaltype_4">
									<option value=""></option>
									<?php foreach($calculationtype as $key):?>
											<option value="<?php echo $key->calculationtypeid;?>">
											<?php echo $key->calculationtypeid;?>-<?php echo $key->calculationtypename;?></option>
									<?php endforeach;?>	
								</select>                  
							</div>
							<div class="large-3 columns">             
								<label id="">Value</label>
								<input type="text" id="addval_4" name="addval_4" value="" data-val="4" class="individualaddchargevalue validate[custom[number],funcCall[validateaddchargepercent]]" >              
							</div>
							<div class="large-3 columns">             
								<label id="">Amount</label>
								<input type="text" id="addamount_4" readonly name="addamount_4" value="" class="individualaddchargeamount" >              
							</div>
						</div>
						<div class="adddiv5 hidedisplay">
							<div class="large-3 columns">             
								<label id="">Name</label>
								<input type="text" readonly="readonly" id="addchargename_5" name="addchargename_5" value="" class="" >
								<input type="hidden" id="addchargeid_5" name="addchargeid_5" value="" class="" >
							</div>
							<div class="large-3 columns">             
								<label>Type</label>
								<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd caltypedd" tabindex="2" data-val="5" name="additionalcaltype_5" id="additionalcaltype_5">
									<option value=""></option>
									<?php foreach($calculationtype as $key):?>
											<option value="<?php echo $key->calculationtypeid;?>">
											<?php echo $key->calculationtypeid;?>-<?php echo $key->calculationtypename;?></option>
									<?php endforeach;?>	
								</select>                  
							</div>
							<div class="large-3 columns">             
								<label id="">Value</label>
								<input type="text" id="addval_5" name="addval_5" value=""  data-val="5" class="individualaddchargevalue validate[custom[number],funcCall[validateaddchargepercent]]" >              
							</div>
							<div class="large-3 columns">             
								<label id="">Amount</label>
								<input type="text" id="addamount_5" readonly name="addamount_5" value="" class="individualaddchargeamount" >              
							</div>
						</div>						
						<div class=''>
							<div class="large-12 columns">             
								<label id="">Total Additional Amount</label>
								<input type="text" id="totaladdselling" readonly name="totaladdselling" value="" class="totaladditionalamount" >
							</div>
						</div>
					</div>	
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 columns">&nbsp;</div>
						<div class="medium-4 large-4 columns">&nbsp;</div>
						<div class="small-4 small-centered medium-4 large-4 columns">
							<input type="button" id="addamtsubmit" name="addamtsubmit" value="Submit" class="btn formbuttonsalert" >	
						</div>
						<div class="medium-4 large-4 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div class="row">&nbsp;</div>
					<!-- hidden fields -->
					<input type="hidden" name="addcatid" id="addcatid" />
				</span>
			</form>
		</div>
	</div>
</div>
<!----Price book Details overlay----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="pricebookoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="large-10 medium-10 large-centered medium-centered columns">
			<div class="large-12 columns headercaptionstyle headerradius">
				<div class="large-6 medium-6 small-6 columns headercaptionleft">
					Price Book List
				</div>
				<div class="large-5 medium-6 small-6 columns addformheadericonstyle" style="text-align:right;padding-right:0 !important">
					<span id="pricebooksubmit" class="fa fa-floppy-o" title="Save"> </span>
					<span id="pricebookclose" class="fa fa-times" title="Close"> </span>
				</div>
			</div>
			<div class="large-12 column paddingzero" id="pricebookgridwidth" style="background:#f5f5f5">
				<table id="pricebookgrid"></table> 
				<div id="pricebookgriddiv" class="w100"></div> 
			</div>
		</div>
	</div>
</div>


<!--- Summary footer overlay concept --->
<!--- Tax overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay" id="summarytaxoverlay" style="overflow-y: scroll;overflow-x: hidden">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="large-3 medium-6 large-centered medium-centered columns" >
			<form method="POST" name="summarytaxoverlayform" class="summaryclearformtaxoverlay" style="" id="summarytaxoverlayform" action="" enctype="" >
				<span id="summarytaxoverlayvalidation" class="validationEngineContainer"> 
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#f5f5f5">
						
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-10 columns" style="background:none">Tax Charges</div>
							<div class="small-1 columns" id="summarytaxclose" style="text-align:right;cursor:pointer;background:none">X</div>
						</div>
						<div class="large-12 columns">             
							<label>Tax Category Name</label>
							<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd" tabindex="2" name="summarytaxcategory" id="summarytaxcategory" disabled>
							<option value=""></option>
							<?php foreach($category as $key):?>
									<option value="<?php echo $key->taxmasterid;?>">
									<?php echo $key->taxmasterid;?>-<?php echo $key->taxmastername;?></option>
							<?php endforeach;?>	
							</select>                  
						</div>
						<div class="summarytaxdiv0 hidedisplay">
							<div class="large-6 columns">             
								<label id="summarytaxlabel0"></label>
								<input type="hidden" name="summarytmdid_0" id="summarytmdid_0" />
								<input type="text" id="summarytaxpercent_0" data-tid="0" name="summarytaxpercent_0" value="" class="summarytaxpercent validate[custom[number],min[0],max[100]]" >
								<input type="hidden" id="summarytaxrule_0" name="summarytaxrule_0" value="" class="" >								
							</div>
							<div class="large-6 columns">             
								<label id="summarytaxlabel01">Amount</label>
								<input type="text" id="summarytaxamount_0" readonly name="summarytaxamount_0" value="" class="summarytaxamount" >              
							</div>
						</div>
						<div class="summarytaxdiv1 hidedisplay">
							<div class="large-6 columns"> 
								<label id="summarytaxlabel1"></label>
								<input type="hidden" name="summarytmdid_1" id="summarytmdid_1" />
								<input type="text" id="summarytaxpercent_1" data-tid="1" class="summarytaxpercent validate[custom[number],min[0],max[100]]" name="summarytaxpercent_1" value="" >
								<input type="hidden" id="summarytaxrule_1" name="summarytaxrule_1" value="" class="" >	
							</div>
							<div class="large-6 columns">             
								<label id="summarytaxlabel01">Amount</label>
								<input type="text" id="summarytaxamount_1" readonly name="summarytaxamount_1" value="" class="summarytaxamount" >             
							</div>
						</div>
						<div class="summarytaxdiv2 hidedisplay">
							<div class="large-6 columns"> 
								<label id="summarytaxlabel2"></label>
								<input type="hidden" name="summarytmdid_2" id="summarytmdid_2" />
								<input type="text" id="summarytaxpercent_2" data-tid="2" name="summarytaxpercent_2" value="" class="summarytaxpercent validate[custom[number],min[0],max[100]]" >
								<input type="hidden" id="summarytaxrule_2" name="summarytaxrule_2" value="" class="" >	
							</div>
							<div class="large-6 columns">             
								<label id="summarytaxlabel01">Amount</label>
								<input type="text" id="summarytaxamount_2" readonly name="summarytaxamount_2" value="" class="summarytaxamount" >             
							</div>
						</div>
						<div class="summarytaxdiv3 hidedisplay">
							<div class="large-6 columns"> 
								<label id="summarytaxlabel3"></label>
								<input type="hidden" name="summarytmdid_3" id="summarytmdid_3" />
								<input type="text" id="summarytaxpercent_3" data-tid="3" name="summarytaxpercent_3" value="" class="summarytaxpercent validate[custom[number],min[0],max[100]]" >
								<input type="hidden" id="summarytaxrule_3" name="summarytaxrule_3" value="" class="" >	
							</div>
							<div class="large-6 columns">             
								<label id="summarytaxlabel01">Amount</label>
								<input type="text" id="summarytaxamount_3" readonly name="summarytaxamount_3" value="" class="summarytaxamount" >             
							</div>
						</div>
						<div class=''>
							<div class="large-12 columns">             
								<label id="summarytaxlabel">Total Tax Amount</label>
								<input type="text" id="summarytaxtotal" readonly name="summarytaxtotal" value="" class="" >
							</div>
						</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div class="row" style="background:#f5f5f5">
						<div class="medium-4 large-4 columns">&nbsp;</div>
						<div class="small-4 small-centered medium-4 large-4 columns">
							<input type="button" id="summarytaxsubmit" name="summarytaxsubmit" value="Submit" class="btn formbuttonsalert" >	
						</div>
						<div class="medium-4 large-4 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div class="row">&nbsp;</div>
					<!-- hidden fields -->
					<input type="hidden" name="" id="" />
				</span>
			</form>
		</div>
	</div>
</div>
<!-- Discount overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay" id="summarydiscountoverlay" style="overflow-y: scroll;overflow-x: hidden">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="large-3 medium-6 large-centered medium-centered columns" >
			<form method="POST" name="summarydiscountform" style="summarydiscountclearform" id="summarydiscountform" action="" enctype="" class="">
				<span id="summarydiscountvalidation" class="validationEngineContainer"> 
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-10 columns" style="background:none">Discount</div>
							<div class="small-1 columns" id="summarydiscountclose" style="text-align:right;cursor:pointer;background:none">X</div>
						</div>
						<div class="large-12 columns">             
							<label>Calculation Type</label>
							<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="validate[required] chzn-select dropdownchange ffieldd" tabindex="2" name="summarydiscountcalculationtype" id="summarydiscountcalculationtype">
							<option value=""></option>
							<?php foreach($calculationtype as $key):?>
									<option value="<?php echo $key->calculationtypeid;?>">
									<?php echo $key->calculationtypeid;?>-<?php echo $key->calculationtypename;?></option>
							<?php endforeach;?>	
							</select>                  
						</div>
						<div class="large-12 columns"> 
							<label>Value</label>
							<input type="text" id="summarydiscountvalue" name="summarydiscountvalue" value="" class="validate[custom[number],funcCall[summaryvalidatepercentage]]" >
						</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div class="row" style="background:#f5f5f5">
						<div class="medium-4 large-4 columns">&nbsp;</div>
						<div class="small-4 small-centered medium-4 large-4 columns">
							<input type="button" id="summaryindiscountadd" name="summaryindiscountadd" value="Submit" class="btn formbuttonsalert" >	
						</div>
						<div class="medium-4 large-4 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div class="row">&nbsp;</div>
					<!-- hidden fields -->
				</span>
			</form>
		</div>
	</div>
</div>
<!----Additional Charge Details overlay----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay" id="summaryadditionaloverlay" style="overflow-y: scroll;overflow-x: hidden">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="large-5 medium-6 large-centered medium-centered columns" >
			<form method="POST" name="summaryadditionaloverlayform" style="" id="summaryadditionaloverlayform" action="" enctype="" class="summaryclearformadditionaloverlay">
				<span id="summaryadditionaloverlayvalidation" class="validationEngineContainer"> 
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-10 columns" style="background:none">Additional Charges</div>
							<div class="small-1 columns" id="summaryadditionalclose" style="text-align:right;cursor:pointer;background:none">X</div>
						</div>
						<div class="large-12 columns">             
							<label>Additional Category Name</label>
							<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd" tabindex="2" name="summaryadditionalcategory" id="summaryadditionalcategory" disabled>
							<option value=""></option>
							<?php foreach($additionalcategory as $key):?>
									<option value="<?php echo $key->additionalchargecategoryid;?>">
									<?php echo $key->additionalchargecategoryid;?>-<?php echo $key->additionalchargecategoryname;?></option>
							<?php endforeach;?>	
							</select>                  
						</div>
						<div class="summaryadddiv0 hidedisplay">
							<div class="large-3 columns">             
								<label id="">Name</label>
								<input type="text" readonly="readonly" id="summaryaddchargename_0" name="summaryaddchargename_0" value="" class="" > 
								<input type="hidden" id="summaryaddchargeid_0" name="summaryaddchargeid_0" value="" class="" > 
							</div>
							<div class="large-3 columns">             
								<label>Type</label>
								<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd summarycaltypedd" tabindex="2" name="summaryadditionalcaltype_0" data-val="0" id="summaryadditionalcaltype_0">
									<option value=""></option>
									<?php foreach($calculationtype as $key):?>
											<option value="<?php echo $key->calculationtypeid;?>">
											<?php echo $key->calculationtypeid;?>-<?php echo $key->calculationtypename;?></option>
									<?php endforeach;?>	
								</select>                  
							</div>
							<div class="large-3 columns">             
								<label id="">Value</label>
								<input type="text" id="summaryaddval_0" name="summaryaddval_0" data-val="0" value="" class="summaryaddchargevalue validate[custom[number],funcCall[validatesummaryaddchargepercent]]" >              
							</div>
							<div class="large-3 columns">             
								<label id="">Amount</label>
								<input type="text" id="summaryaddamount_0" readonly name="summaryaddamount_0" value="" class="summaryaddchargeamount" >          
							</div>
						</div>
						<div class="summaryadddiv1 hidedisplay">
							<div class="large-3 columns">             
								<label id="">Name</label>
								<input type="text"  readonly="readonly" id="summaryaddchargename_1" name="summaryaddchargename_1" value="" class="" >
								<input type="hidden" id="summaryaddchargeid_1" name="summaryaddchargeid_1" value="" class="" > 								
							</div>
							<div class="large-3 columns">             
								<label>Type</label>
								<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd summarycaltypedd" tabindex="2" data-val="1" name="summaryadditionalcaltype_1" id="summaryadditionalcaltype_1">
									<option value=""></option>
									<?php foreach($calculationtype as $key):?>
											<option value="<?php echo $key->calculationtypeid;?>">
											<?php echo $key->calculationtypeid;?>-<?php echo $key->calculationtypename;?></option>
									<?php endforeach;?>	
								</select>                  
							</div>
							<div class="large-3 columns">             
								<label id="">Value</label>
								<input type="text" id="summaryaddval_1" name="summaryaddval_1" value="" data-val="1" class="summaryaddchargevalue validate[custom[number],funcCall[validatesummaryaddchargepercent]]" >              
							</div>
							<div class="large-3 columns">             
								<label id="">Amount</label>
								<input type="text" id="summaryaddamount_1" readonly name="summaryaddamount_1" value="" class="summaryaddchargeamount" >              
							</div>
						</div>
						<div class="summaryadddiv2 hidedisplay">
							<div class="large-3 columns">             
								<label id="">Name</label>
								<input type="text" readonly="readonly" id="summaryaddchargename_2" name="summaryaddchargename_2" value="" class="" > 
								<input type="hidden" id="summaryaddchargeid_2" name="summaryaddchargeid_2" value="" class="" > 
							</div>
							<div class="large-3 columns">             
								<label>Type</label>
								<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd summarycaltypedd" tabindex="2" data-val="2" name="summaryadditionalcaltype_2" id="summaryadditionalcaltype_2">
									<option value=""></option>
									<?php foreach($calculationtype as $key):?>
											<option value="<?php echo $key->calculationtypeid;?>">
											<?php echo $key->calculationtypeid;?>-<?php echo $key->calculationtypename;?></option>
									<?php endforeach;?>	
								</select>                  
							</div>
							<div class="large-3 columns">             
								<label id="">Value</label>
								<input type="text" id="summaryaddval_2" name="summaryaddval_2" value="" data-val="2" class="summaryaddchargevalue validate[custom[number],funcCall[validatesummaryaddchargepercent]]" >              
							</div>
							<div class="large-3 columns">             
								<label id="">Amount</label>
								<input type="text" id="summaryaddamount_2" readonly name="summaryaddamount_2" value="" class="summaryaddchargeamount" >              
							</div>
						</div>
						<div class="summaryadddiv3 hidedisplay">
							<div class="large-3 columns">             
								<label id="">Name</label>
								<input type="text" readonly="readonly" id="summaryaddchargename_3" name="summaryaddchargename_3" value="" class="" >
								<input type="hidden" id="summaryaddchargeid_3" name="summaryaddchargeid_3" value="" class="" > 
							</div>
							<div class="large-3 columns">             
								<label>Type</label>
								<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd summarycaltypedd" tabindex="2" data-val="3" name="summaryadditionalcaltype_3" id="summaryadditionalcaltype_3">
									<option value=""></option>
									<?php foreach($calculationtype as $key):?>
											<option value="<?php echo $key->calculationtypeid;?>">
											<?php echo $key->calculationtypeid;?>-<?php echo $key->calculationtypename;?></option>
									<?php endforeach;?>	
								</select>                  
							</div>
							<div class="large-3 columns">             
								<label id="">Value</label>
								<input type="text" id="summaryaddval_3" name="summaryaddval_3" value="" data-val="3" class="summaryaddchargevalue validate[custom[number],funcCall[validatesummaryaddchargepercent]]" >              
							</div>
							<div class="large-3 columns">             
								<label id="">Amount</label>
								<input type="text" id="summaryaddamount_3" readonly name="summaryaddamount_3" value="" class="summaryaddchargeamount" >              
							</div>
						</div>
						<div class="summaryadddiv4 hidedisplay">
							<div class="large-3 columns">             
								<label id="">Name</label>
								<input type="text" readonly="readonly" id="summaryaddchargename_4" name="summaryaddchargename_4" value="" class="" >  
								<input type="hidden" id="summaryaddchargeid_4" name="summaryaddchargeid_4" value="" class="" > 								
							</div>
							<div class="large-3 columns">             
								<label>Type</label>
								<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd summarycaltypedd" tabindex="2" data-val="4" name="summaryadditionalcaltype_4" id="summaryadditionalcaltype_4">
									<option value=""></option>
									<?php foreach($calculationtype as $key):?>
											<option value="<?php echo $key->calculationtypeid;?>">
											<?php echo $key->calculationtypeid;?>-<?php echo $key->calculationtypename;?></option>
									<?php endforeach;?>	
								</select>                  
							</div>
							<div class="large-3 columns">             
								<label id="">Value</label>
								<input type="text" id="summaryaddval_4" name="summaryaddval_4" value="" data-val="4" class="summaryaddchargevalue validate[custom[number],funcCall[validatesummaryaddchargepercent]]" >              
							</div>
							<div class="large-3 columns">             
								<label id="">Amount</label>
								<input type="text" id="summaryaddamount_4" readonly name="summaryaddamount_4" value="" class="summaryaddchargeamount" >              
							</div>
						</div>
						<div class="summaryadddiv5 hidedisplay">
							<div class="large-3 columns">             
								<label id="">Name</label>
								<input type="text" readonly="readonly" id="summaryaddchargename_5" name="summaryaddchargename_5" value="" class="" > 
								<input type="hidden" id="summaryaddchargeid_5" name="summaryaddchargeid_5" value="" class="" > 								
							</div>
							<div class="large-3 columns">             
								<label>Type</label>
								<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd summarycaltypedd" tabindex="2" data-val="5" name="summaryadditionalcaltype_5" id="summaryadditionalcaltype_5">
									<option value=""></option>
									<?php foreach($calculationtype as $key):?>
											<option value="<?php echo $key->calculationtypeid;?>">
											<?php echo $key->calculationtypeid;?>-<?php echo $key->calculationtypename;?></option>
									<?php endforeach;?>	
								</select>                  
							</div>
							<div class="large-3 columns">             
								<label id="">Value</label>
								<input type="text" id="summaryaddval_5" name="summaryaddval_5" value="" data-val="5" class="summaryaddchargevalue validate[custom[number],funcCall[validatesummaryaddchargepercent]]" >              
							</div>
							<div class="large-3 columns">             
								<label id="">Amount</label>
								<input type="text" id="summaryaddamount_5" readonly name="summaryaddamount_5" value="" class="summaryaddchargeamount" >              
							</div>
						</div>						
						<div class=''>
							<div class="large-12 columns">             
								<label id="">Total Additional Amount</label>
								<input type="text" id="summarytotaladdamount" readonly name="summarytotaladdamount" value="" class="" >
							</div>
						</div>
					</div>	
					<div class="row" style="background:#f5f5f5">
						<div class="medium-4 large-4 columns">&nbsp;</div>
						<div class="small-4 small-centered medium-4 large-4 columns">
							<input type="button" id="summaryaddamtsubmit" name="summaryaddamtsubmit" value="Submit" class="btn formbuttonsalert" >	
						</div>
						<div class="medium-4 large-4 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div class="row">&nbsp;</div>
					<!-- hidden fields -->
					<input type="hidden" name="" id="" />
				</span>
			</form>
		</div>
	</div>
</div>
<!-- Adjustment Amount Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay" id="adjustmentoverlay" style="overflow-y: scroll;overflow-x: hidden">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="large-3 medium-6 large-centered medium-centered columns" >
			<form method="POST" name="adjustmentform" style="" id="adjustmentform" action="" enctype="" class="clearadjustmentform">
				<span id="adjustmentformvalidation" class="validationEngineContainer"> 
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-10 columns" style="background:none">Adjustment</div>
							<div class="small-1 columns" id="adjustmentclose" style="text-align:right;cursor:pointer;background:none">X</div>
						</div>
						<div class="large-12 columns">             
							<label>Adjustment Type</label>
							<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd" tabindex="2" name="adjustmenttype" id="adjustmenttype">
							<option value=""></option>
							<?php foreach($adjusttype as $key):?>
									<option value="<?php echo $key->adjustmenttypeid;?>">
									<?php echo $key->adjustmenttypeid;?>-<?php echo $key->adjustmenttypename;?></option>
							<?php endforeach;?>	
							</select>                  
						</div>
						<div class="large-12 columns"> 
							<label>Value</label>
							<input type="text" id="adjustmentvalue" name="adjustmentvalue" value="" class="" >
						</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>				
					<div class="row" style="background:#f5f5f5">
						<div class="medium-4 large-4 columns">&nbsp;</div>
						<div class="small-4 small-centered medium-4 large-4 columns">
							<input type="button" id="adjustmentsbtadd" name="adjustmentsbtadd" value="Submit" class="btn formbuttonsalert" >	
						</div>
						<div class="medium-4 large-4 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
				
					<div class="row">&nbsp;</div>
					<!-- hidden fields -->
				</span>
			</form>
		</div>
	</div>
</div>
