<!-- CurrencyConversion Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="pricebookcurrencyconvoverlay" style="overflow-y: scroll;overflow-x: hidden;">		
		<div class="row sectiontoppadding" >
			<div class= >
			<div class="row">&nbsp;</div>
			<form method="POST" name="" style="" id="" action="" enctype="" class="">
				<span id="pricebookcurrencyconv_validate" class="validationEngineContainer"> 
					<div class="alert-panel" style="background-color:#465a63">
						<div class="alertmessagearea">
							<div class="alert-title" style="color:#ffffff">Pricebook Currencies</div>
							<div class="alert-message" style="height:100%">
								<span class="firsttab" tabindex="1000"></span>
								<div class="static-field overlayfield"> 
									<label>PriceBook Currency<span class="mandatoryfildclass">*</span></label>
									<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="validate[required] chzn-select dropdownchange ffieldd" tabindex="1001" name="pricebookcurrency" id="pricebookcurrency">
										<option value=""></option>
										<?php foreach($conv_currency as $key):?>
											<option value="<?php echo $key->currencyid;?>">
											<?php echo $key->currencycountry.'-'.$key->currencyname;?></option>
										<?php endforeach;?>	
									</select>
								</div>
								<div class="static-field overlayfield"> 
									<label>Current Currency<span class="mandatoryfildclass">*</span></label>
									<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="validate[required] chzn-select dropdownchange" tabindex="1002" name="currentcurrency" id="currentcurrency">
										<option value=""></option>
										<?php foreach($conv_currency as $key):?>
											<option value="<?php echo $key->currencyid;?>">
											<?php echo $key->currencycountry.'-'.$key->currencyname;?></option>
										<?php endforeach;?>	
									</select>
								</div>
								<div class="input-field overlayfield"> 
									<input type="text" id="pricebook_currencyconv" name="pricebook_currencyconv" value="" tabindex="1003" class="validate[custom[number],required]" >
									<label for="pricebook_currencyconv">Currency ConversionRate<span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="">
									<input type="hidden" id="pricebook_sellingprice" name="pricebook_sellingprice" >
								</div>
							</div>
						</div>
						<div class="alertbuttonarea">
							<input type="button" id="pricebook_currencyconv_submit" name="" value="Submit" tabindex="1004" class="alertbtn flloop" >
							<input type="button" id="pricebook_currencyconvclose" name="" value="Cancel" tabindex="1005" class="flloop alertbtn alertsoverlaybtn" >
							<span class="lasttab" tabindex="1006"></span>
						</div>
					</div>
				</span>
			</form>
			<div class="row">&nbsp;</div>
			</div>
		</div>
	</div>
</div>
<!-- Overall Currency Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="currencyconvoverlay" style="overflow-y: auto;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="" >
		<div class="row">&nbsp;</div>
			<form method="POST" name="" style="" id="" action="" enctype="" class="">
				<span id="currencyconv_validate" class="validationEngineContainer"> 
					<div class="alert-panel" style="background-color:#465a63">
						<div class="alertmessagearea">
							<div class="alert-title" style="color:#ffffff">Currency Conversion</div>
							<div class="alert-message">
								<span class="firsttab" tabindex="1000"></span>
								<div class="static-field overlayfield"> 
										<label>Default Currency<span class="mandatoryfildclass">*</span></label>
										<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="validate[required] chzn-select dropdownchange ffieldd" tabindex="1001" name="defaultcurrency" id="defaultcurrency">
											<option value=""></option>
											<?php foreach($conv_currency as $key):?>
												<option value="<?php echo $key->currencyid;?>">
												<?php echo $key->currencycountry.'-'.$key->currencyname;?></option>
											<?php endforeach;?>	
										</select>
								</div>
								<div class="static-field overlayfield"> 
									<label>Conversion Currency<span class="mandatoryfildclass">*</span></label>
									<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="validate[required] chzn-select dropdownchange" tabindex="1002" name="convcurrency" id="convcurrency" >
										<option value=""></option>
										<?php foreach($conv_currency as $key):?>
											<option value="<?php echo $key->currencyid;?>">
											<?php echo $key->currencycountry.'-'.$key->currencyname;?></option>
										<?php endforeach;?>	
									</select>
								</div>
								<div class="input-field overlayfield"> 
									<input type="text" id="currencyconv" name="currencyconv" value="" tabindex="1003" class="validate[custom[number],required]" >
									<label for="currencyconv">Currency ConversionRate<span class="mandatoryfildclass">*</span></label>
								</div>	
							</div>
						</div>
						<div class="alertbuttonarea">
							<input type="button" id="currencyconv_submit" name="" value="Submit" tabindex="1004" class="alertbtn" >	
							<input type="button" id="currencyconvclose" name="" value="Cancel" tabindex="1005" class="alertbtn flloop  alertsoverlaybtn" >
							<span class="lasttab" tabindex="1006"></span>
						</div>
					</div>
				</span>
			</form>
			<div class="row">&nbsp;</div>
		</div>
	</div>
</div>
<!-- Discount Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="discountoverlay" style="overflow-y: auto;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>				
		<div class="" >
		<div class="row">&nbsp;</div>
			<form method="POST" name="discountform" style="" id="discountform" action="" enctype="" class="">
				<span id="individualdiscountvalidation" class="validationEngineContainer">
					 <div class="alert-panel" style="background-color:#465a63">
						<div class="alertmessagearea">
							<div class="alert-title">Discount</div>
							<div class="alert-message">
								<span class="firsttab" tabindex="1000"></span>
								<div class="static-field overlayfield">             
									<label>Discount Type<span class="mandatoryfildclass">*</span></label>
									<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="validate[required] chzn-select dropdownchange ffieldd" tabindex="1001" name="discounttypeid" id="discounttypeid">
									<option value=""></option>
									<?php foreach($calculationtype as $key):?>
											<option value="<?php echo $key->calculationtypeid;?>">
											<?php echo $key->calculationtypename;?></option>
									<?php endforeach;?>	
									</select>                  
								</div>
								<div class="input-field overlayfield"> 
									<input type="text" id="discountpercent" name="discountpercent" value="" class="validate[custom[number],funcCall[validatepercentage]]" tabindex="1002">
									<label for="discountpercent">Value<span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="input-field overlayfield"> 
									<input type="text" id="singlediscounttotal" name="" value="" class="" tabindex="1003" readonly >
									<label for="singlediscounttotal">Total Discount</label>
								</div>
							</div>
						</div>
						<div class="alertbuttonarea">
							<input type="button" id="discountsubmit" name="" value="Submit" class="alertbtn" tabindex="1004">	
							<input type="button" id="discountclose" name="" value="Cancel" tabindex="1005" class="alertbtn flloop  alertsoverlaybtn" >
							<span class="lasttab" tabindex="1006"></span>
						</div>
					</div>
				</span>
			</form>
			<div class="row">&nbsp;</div>
		</div>
	</div>
</div>
<!----Tax overlay----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="taxoverlay" style="overflow-y: auto;overflow-x: hidden">		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="large-4 medium-6 small-11 large-centered medium-centered small-centered columns" >
			<form method="POST" name="taxoverlayform" style="" id="taxoverlayform" action="" enctype="" class="clearformtaxoverlay">
				<span id="taxoverlayvalidation" class="validationEngineContainer"> 
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#465a63">
						<div class="large-12 columns sectionheaderformcaptionstyle">
							Tax Charges
						</div>
						<div class="large-12 columns headerformcaptionstyleforcharge" style="height:auto;padding: 0.1rem 0 0;background:#465a63"">
									<span class="large-6 medium-6 small-10 columns end" style="line-height:1.8">
										<span class="large-4 medium-4 small-4 columns" style="line-height:1.5;color:#fff">
											Category<span class="mandatoryfildclass">*</span>
										</span>
										<span class="large-6 large-8 small-8 end columns">
											<select class="chzn-select" id="taxcategory" name="taxcategory" data-placeholder="Select" data-prompt-position="topLeft:14,36">
												<option value=""></option>
												<?php foreach($category as $key):?>
														<option value="<?php echo $key->taxmasterid;?>">
														<?php echo $key->taxmastername;?></option>
												<?php endforeach;?>	
											</select>										
										</span>								
									</span>
									<span class="large-6 medium-6 small-2 columns innergridicon righttext small-only-text-center" style="line-height:1.8">
										<span id="taxdeleteicon" class="deleteiconclass" title="Delete"> <i class="material-icons">delete</i></span>
									</span>
						</div>	
						<?php
							$device = $this->Basefunctions->deviceinfo();
							if($device=='phone') {
								echo '<div class="large-12 columns paddingzero forgetinggridname" id="taxgridwidth"><div id="taxgrid" class="inner-row-content inner-gridcontent"  style="max-width:2000px; height:300px;top:0px;">
									<!-- Table header content , data rows & pagination for mobile-->
								</div>
								<!--<footer class="inner-gridfooter footercontainer" id="taxgridfooter">
									 Footer & Pagination content 
								</footer>--></div>';
							} else {
								echo '<div class="large-12 columns forgetinggridname" id="taxgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="taxgrid" style="max-width:2000px; height:300px;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
								<!-- <div class="inner-gridfooter footer-content footercontainer" id="taxgridfooter">
									Footer & Pagination content 
								</div>--></div>';
							}
						?>
					</div>
					<div class="row" style="background:#465a63">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
							Total : <input type="text" id="tax_data_total" name="tax_data_total" value="0.00" class="" readonly style="width:80px;">
							<input type="button" id="taxgridsubmit" name="taxgridsubmit" value="Submit" class="alertbtn" onclick="submittaxgrid();">
							<input type="button" id="taxclose" name="" value="Cancel" class="alertbtn">
						</div>
					</div>
					<div class="row">&nbsp;</div>
					<!-- hidden fields -->
					<input type="hidden" name="taxcatid" id="taxcatid" />
				</span>
			</form>
		</div>
	</div>
</div>
<!----Additional Charge Details overlay----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="additionaloverlay" style="overflow-y: scroll;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>			
		<div class="large-4 medium-6 small-11 large-centered medium-centered small-centered columns" >
			<form method="POST" name="additionaloverlayform" style="" id="additionaloverlayform" action="" enctype="" class="clearformadditionaloverlay">
				<span id="additionaloverlayvalidation" class="validationEngineContainer"> 
					<div class="row" style="background:#465a63">
						<div class="large-12 columns sectionheaderformcaptionstyle" style="line-height:1rem;">
							Additional Charges
						</div>
						<div class="large-12 columns headerformcaptionstyleforcharge" style="height:auto;padding: 0.1rem 0 0;background:#465a63"">
									<span class="large-8 medium-6 small-10 columns end" style="line-height:1.8">
										<span class="large-4 medium-4 small-4 columns" style="line-height:1.5;color:#fff">
											Category<span class="mandatoryfildclass">*</span>
										</span>
										<span class="large-6 large-8 small-8 end columns">
											<span class="firsttab" tabindex="1000"></span>
											<select class="chzn-select ffieldd" id="chargecategory" name="chargecategory" data-placeholder="Select" data-prompt-position="bottomLeft:14,36" tabindex="1001">
												<option value=""></option>
												<?php foreach($additionalcategory as $key):?>
														<option value="<?php echo $key->additionalchargecategoryid;?>">
														<?php echo $key->additionalchargecategoryname;?></option>
												<?php endforeach;?>	
											</select>										
										</span>
									</span>
									<span class="large-4 medium-6 small-2 columns innergridicon righttext small-only-text-center" style="line-height:1.8">
										<span id="chargedeleteicon" class="deleteiconclass" title="Delete"><i class="material-icons">delete</i></span>
									</span>
						</div>
						<?php
							$device = $this->Basefunctions->deviceinfo();
							if($device=='phone') {
								echo '<div class="large-12 columns paddingzero forgetinggridname" id="chargesgridwidth"><div id="chargesgrid" class=" inner-row-content inner-gridcontent" style="max-width:2000px; height:300px;top:0px;">
									<!-- Table header content , data rows & pagination for mobile-->
								</div>
								<!--<footer class="inner-gridfooter footercontainer" id="chargesgridfooter">
									 Footer & Pagination content
								</footer> --></div>';
							} else {
								echo '<div class="large-12 columns forgetinggridname" id="chargesgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="chargesgrid" style="max-width:2000px; height:300px;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
								<!-- <div class="inner-gridfooter footer-content footercontainer" id="chargesgridfooter">
									Footer & Pagination content 
								</div>--></div>';
							}
						?>	
					</div>
					<div class="row" style="background:#465a63">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
							Total : <input type="text" id="charge_data_total" name="charge_data_total" value="0.00" class="" readonly style="width:80px;" tabindex="1002">
							<input type="button" id="chargegridsubmit" name="chargegridsubmit" value="Submit" class="alertbtn" onclick="submitchargegrid();" tabindex="1003">
							<input type="button" id="additionalclose" name="" value="Cancel" class="alertbtn" tabindex="1004">
							<span class="lasttab" tabindex="1005"></span>
						</div>
					</div>					
					<!-- hidden fields -->
					<input type="hidden" name="addcatid" id="addcatid" />
				</span>
			</form>
		</div>
	</div>
</div>
<!----Price book Details overlay----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="pricebookoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40;">		
		<div class="row sectiontoppadding">&nbsp;</div>			
		<div class="large-6 medium-6 small-11 large-centered medium-centered small-centered columns">
			<div class="row">
			<div class="large-12 columns sectionheaderformcaptionstyle" style="line-height:1rem;">
				Price Book List
			</div>
			<?php
				$device = $this->Basefunctions->deviceinfo();
				if($device=='phone') {
					echo '<div class="large-12 columns paddingzero forgetinggridname" id="pricebookgridwidth"><div id="pricebookgrid" class=" inner-row-content inner-gridcontent" style="max-width:2000px; height:300px;top:0px;">
						<!-- Table header content , data rows & pagination for mobile-->
					</div>
					<footer class="inner-gridfooter footercontainer" id="pricebookgridfooter">
						<!-- Footer & Pagination content -->
					</footer></div>';
				} else {
					echo '<div class="large-12 columns forgetinggridname" id="pricebookgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="pricebookgrid" style="max-width:2000px; height:300px;top:0px;">
					<!-- Table content[Header & Record Rows] for desktop-->
					</div>
					<!-- <div class="inner-gridfooter footer-content footercontainer" id="pricebookgridfooter">
						Footer & Pagination content 
					</div>--></div>';
				}
			?>
			</div>
			<div class="row" style="background:#465a63">
				<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
					<span class="firsttab" tabindex="1000"></span>
					<input type="button" id="pricebooksubmit" name="" value="Submit" class="alertbtn ffield" tabindex="1001">	
					<input type="button" id="pricebookclose" name="" value="Cancel" tabindex="1002" class="alertbtn  alertsoverlaybtn flloop" >
					<span class="lasttab" tabindex="1003"></span>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Product Search Grid---->
<div class="large-6 columns" style="position:absolute;">
	<div class="overlay" id="productsearchoverlay" style="overflow-y: auto;overflow-x: hidden;z-index:40;">		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="large-6 medium-6 small-11 large-centered medium-centered small-centered columns">
			<div class="row">
			<div class="large-12 columns sectionheaderformcaptionstyle" style="line-height:1rem;">
				Product Search
			</div>
			<?php
				$device = $this->Basefunctions->deviceinfo();
				if($device=='phone') {
					echo '<div class="large-12 columns paddingzero forgetinggridname" id="productsearchgridwidth"><div id="productsearchgrid" class=" inner-row-content inner-gridcontent" style="max-width:2000px; height:300px;top:0px;">
						<!-- Table header content , data rows & pagination for mobile-->
					</div>
					<!--<footer class="inner-gridfooter footercontainer" id="productsearchgridfooter">
						 Footer & Pagination content
					</footer> --></div>';
				} else {
					echo '<div class="large-12 columns forgetinggridname" id="productsearchgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="productsearchgrid" style="max-width:2000px; height:300px;top:0px;">
					<!-- Table content[Header & Record Rows] for desktop-->
					</div>
					<!-- <div class="inner-gridfooter footer-content footercontainer" id="productsearchgridfooter">
						Footer & Pagination content 
					</div>--></div>';
				}
			?>
			</div>
			<div class="row" style="background:#465a63">
				<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
					<span class="firsttab" tabindex="1000"></span>
					<input type="button" id="productsearchsubmit" name="" value="Submit" class="alertbtn ffield" tabindex="1001">	
					<input type="button" id="productsearchclose" name="" value="Cancel" tabindex="1002" class="alertbtn  alertsoverlaybtn flloop" >
					<span class="lasttab" tabindex="1003"></span>
				</div>
			</div>
		</div>
	</div>
</div>
<!----Charges on taxes----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="taxoverlay" style="overflow-y: scroll;overflow-x: hidden">		
		<div class="row sectiontoppadding">&nbsp;</div>	
		<div class="large-4 medium-6 small-11 large-centered medium-centered small-centered  columns" >
			<form method="POST" name="chargetaxoverlayform" style="" id="chargetaxoverlayform" action="" enctype="" class="clearformtaxoverlay">
				<span id="" class="validationEngineContainer"> 
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#465a63">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-10 columns" style="background:none">Tax On Charges</div>
							<div class="small-1 columns" id="" style="text-align:right;cursor:pointer;background:none">X</div>
						</div>
						<div class="row">&nbsp;</div><div class="row">&nbsp;</div>						
						<div class="large-12 columns headerformcaptionstyleforcharge" style="height:auto;padding: 0.1rem 0 0;">									
								<span class="large-6 medium-6 small-12 columns innergridicon righttext small-only-text-center" style="line-height:1.8">
									<span id="chargetaxdeleteicon" class="icon icon-bin deleteiconclass" title="Delete"> </span>
									<span id="chargetaxediticon" class="icon icon-pencil editiconclass " title="Edit"> </span>										
									<span id="chargetaxsaveicon" class="icon icon-floppy-disk saveiconclass" title="Save"> </span>
								</span>
						</div>						
						<div class="large-12 column paddingzero" id="taxgridwidth" style="background:#f5f5f5">
							<table id="chargetaxtaxgrid"></table> 
							<div id="chargetaxtaxgriddiv" class="w100"></div> 
						</div>
						<div class="row">&nbsp;</div>
						<div class="row">&nbsp;</div>						
					</div>
					<div class="row" style="background:#465a63">
						<div class="medium-4 large-4 columns">&nbsp;</div>
						<div class="small-4 small-centered medium-4 large-4 columns">
							<input type="button" id="chargetaxtaxgridsubmit" name="chargetaxtaxgridsubmit" value="Submit" class="btn formbuttonsalert" onclick="submitchargetaxgrid();">	
						</div>
						<div class="medium-4 large-4 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div class="row">&nbsp;</div>
				</span>
			</form>
		</div>
	</div>
</div>
<!--- Summary footer overlay concept --->
	<div class="overlay alertsoverlay overlayalerts resetoverlay" id="summarytaxoverlay" style="overflow-y: scroll;overflow-x: hidden;">	
		<div class="row sectiontoppadding">&nbsp;</div>			
		<div class="large-3 medium-6 small-11 large-centered medium-centered small-centered columns" >
			<form method="POST" name="summarytaxoverlayform" class="summaryclearformtaxoverlay" style="" id="summarytaxoverlayform" action="" enctype="" >
				<span id="summarytaxoverlayvalidation" class="validationEngineContainer"> 
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#465a63">
						<div class="large-12 columns sectionheaderformcaptionstyle">
							Tax Charges
						</div>
						<div class="static-field large-12 columns">             
							<label>Tax Category Name</label>
							<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd" tabindex="2" name="summarytaxcategory" id="summarytaxcategory" disabled>
							<option value=""></option>
							<?php foreach($category as $key):?>
									<option value="<?php echo $key->taxmasterid;?>">
									<?php echo $key->taxmastername;?></option>
							<?php endforeach;?>	
							</select>                  
						</div>
						<div class="summarytaxdiv0 hidedisplay">
							<div class="input-field large-6 columns">
								<input type="hidden" name="summarytmdid_0" id="summarytmdid_0" />
								<input type="text" id="summarytaxpercent_0" data-tid="0" name="summarytaxpercent_0" value="" class="summarytaxpercent validate[custom[number],min[0],max[100]]" >
								<label for="summarytaxpercent_0" id="summarytaxlabel0"></label>
								<input type="hidden" id="summarytaxrule_0" name="summarytaxrule_0" value="" class="" >								
							</div>
							<div class="input-field large-6 columns">
								<input type="text" id="summarytaxamount_0" readonly name="summarytaxamount_0" value="" class="summarytaxamount" >    
								<label for="summarytaxamount_0" id="summarytaxlabel01">Amount</label>
							</div>
						</div>
						<div class="summarytaxdiv1 hidedisplay">
							<div class="input-field large-6 columns">
								<input type="hidden" name="summarytmdid_1" id="summarytmdid_1" />
								<input type="text" id="summarytaxpercent_1" data-tid="1" class="summarytaxpercent validate[custom[number],min[0],max[100]]" name="summarytaxpercent_1" value="" >
								<label for="summarytaxpercent_1" id="summarytaxlabel1"></label>
								<input type="hidden" id="summarytaxrule_1" name="summarytaxrule_1" value="" class="" >	
							</div>
							<div class="input-field large-6 columns">
								<input type="text" id="summarytaxamount_1" readonly name="summarytaxamount_1" value="" class="summarytaxamount" >       
								<label for="summarytaxamount_1" id="summarytaxlabel01">Amount</label>
							</div>
						</div>
						<div class="summarytaxdiv2 hidedisplay">
							<div class="input-field large-6 columns">
								<input type="hidden" name="summarytmdid_2" id="summarytmdid_2" />
								<input type="text" id="summarytaxpercent_2" data-tid="2" name="summarytaxpercent_2" value="" class="summarytaxpercent validate[custom[number],min[0],max[100]]" >
								<label for="summarytaxpercent_2" id="summarytaxlabel2"></label>
								<input type="hidden" id="summarytaxrule_2" name="summarytaxrule_2" value="" class="" >	
							</div>
							<div class="input-field large-6 columns">
								<input type="text" id="summarytaxamount_2" readonly name="summarytaxamount_2" value="" class="summarytaxamount" >  
								<label for="summarytaxamount_2" id="summarytaxlabel01">Amount</label>								
							</div>
						</div>
						<div class="summarytaxdiv3 hidedisplay">
							<div class="input-field large-6 columns">
								<input type="hidden" name="summarytmdid_3" id="summarytmdid_3" />
								<input type="text" id="summarytaxpercent_3" data-tid="3" name="summarytaxpercent_3" value="" class="summarytaxpercent validate[custom[number],min[0],max[100]]" >
								<label for="summarytaxpercent_3" id="summarytaxlabel3"></label>
								<input type="hidden" id="summarytaxrule_3" name="summarytaxrule_3" value="" class="" >	
							</div>
							<div class="input-field large-6 columns"> 
								<input type="text" id="summarytaxamount_3" readonly name="summarytaxamount_3" value="" class="summarytaxamount" >
								<label for="summarytaxamount_3" id="summarytaxlabel01">Amount</label>
							</div>
						</div>
						<div class=''>
							<div class="input-field large-12 columns">
								<input type="text" id="summarytaxtotal" readonly name="summarytaxtotal" value="" class="" >
								<label for="summarytaxtotal" id="summarytaxlabel">Total Tax Amount</label>
							</div>
						</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div class="row" style="background:#465a63">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
							<input type="button" id="summarytaxsubmit" name="summarytaxsubmit" value="Submit" class="alertbtn" >
							<input type="button" id="summarytaxclose" name="" value="Cancel" class="alertbtn">
						</div>
					</div>
					<div class="row">&nbsp;</div>
					<!-- hidden fields -->
					<input type="hidden" name="" id="" />
				</span>
			</form>
		</div>
	</div>
<!-- Discount overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay" id="summarydiscountoverlay" style="overflow-y: scroll;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>			
		<div class="" >
			<div class="row">&nbsp;</div>
			<form method="POST" name="summarydiscountform" style="summarydiscountclearform" id="summarydiscountform" action="" enctype="" class="">
				<span id="summarydiscountvalidation" class="validationEngineContainer"> 
					<div class="alert-panel" style="background-color:#465a63">
						<div class="alertmessagearea">
							<div class="alert-title">Discount</div>
							<div class="alert-message">
								<span class="firsttab" tabindex="1000"></span>
								<div class="static-field overlayfield">             
									<label>Discount Type<span class="mandatoryfildclass">*</span></label>
									<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="validate[required] chzn-select dropdownchange ffieldd" tabindex="1001" name="groupdiscounttypeid" id="groupdiscounttypeid">
									<option value=""></option>
									<?php foreach($calculationtype as $key):?>
											<option value="<?php echo $key->calculationtypeid;?>">
											<?php echo $key->calculationtypename;?></option>
									<?php endforeach;?>	
									</select>                  
								</div>
								<div class="input-field overlayfield"> 
									<input type="text" id="groupdiscountpercent" name="groupdiscountpercent" value="" tabindex="1002" class="validate[custom[number],funcCall[summaryvalidatepercentage]]" >
									<label for="groupdiscountpercent">Value<span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="input-field overlayfield"> 
									<input type="text" id="groupdiscounttotal" name="" value="" tabindex="1003" class="" readonly >
									<label for="groupdiscounttotal">Total Discount</label>
								</div>
							</div>
						</div>
						<div class="alertbuttonarea">
							<input type="button" id="summarydiscountadd" name="summarydiscountadd" value="Submit" tabindex="1004" class="alertbtn" >	
							<input type="button" id="summarydiscountclose" name="" value="Cancel" tabindex="1005" class="alertbtn flloop  alertsoverlaybtn" >
							<span class="lasttab" tabindex="1006"></span>
						</div>
					</div>
				</span>
			</form>
			<div class="row">&nbsp;</div>
		</div>
	</div>
</div>
	<div class="overlay alertsoverlay overlayalerts resetoverlay" id="summaryadditionaloverlay" style="overflow-y: scroll;overflow-x: hidden">		
		<div class="row sectiontoppadding">&nbsp;</div>			
		<div class="large-5 medium-6 small-11 large-centered medium-centered small-centered columns" >
			<form method="POST" name="summaryadditionaloverlayform" style="" id="summaryadditionaloverlayform" action="" enctype="" class="summaryclearformadditionaloverlay">
				<span id="summaryadditionaloverlayvalidation" class="validationEngineContainer"> 
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#465a63">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-10 columns" style="background:none">Additional Charges</div>
							<div class="small-1 columns" id="summaryadditionalclose" style="text-align:right;cursor:pointer;background:none"><span class=""><i class="material-icons">cancel</i></span></div>
						</div>
						<div class="static-field large-12 columns">             
							<label> Additional Category Name </label>
							<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd" tabindex="2" name="summaryadditionalcategory" id="summaryadditionalcategory" disabled>
							<option value=""></option>
							<?php foreach($additionalcategory as $key):?>
									<option value="<?php echo $key->additionalchargecategoryid;?>">
									<?php echo $key->additionalchargecategoryname;?></option>
							<?php endforeach;?>	
							</select>                  
						</div>
						
					</div>	
					<div class="row" style="background:#465a63">
						<div class="medium-4 large-4 columns">&nbsp;</div>
						<div class="small-4 small-centered medium-4 large-4 columns">
							<input type="button" id="summaryaddamtsubmit" name="summaryaddamtsubmit" value="Submit" class="btn formbuttonsalert" >	
						</div>
						<div class="medium-4 large-4 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div class="row">&nbsp;</div>
					<!-- hidden fields -->
					<input type="hidden" name="" id="" />
				</span>
			</form>
		</div>
	</div>
<!-- Adjustment Amount Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay" id="adjustmentoverlay" style="overflow-y: scroll;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>			
		<div class="" >
			<div class="row">&nbsp;</div>
			<form method="POST" name="adjustmentform" style="" id="adjustmentform" action="" enctype="" class="clearadjustmentform">
				<span id="adjustmentformvalidation" class="validationEngineContainer"> 
					<div class="alert-panel" style="background-color:#465a63">
						<div class="alertmessagearea">
							<div class="alert-title">Adjustment</div>
							<div class="alert-message">
								<span class="firsttab" tabindex="1000"></span>
								<div class="static-field overlayfield">             
									<label>Adjustment Type</label>
									<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd" tabindex="1001" name="adjustmenttypeid" id="adjustmenttypeid">
									<?php foreach($adjusttype as $key):?>
											<option value="<?php echo $key->adjustmenttypeid;?>">
											<?php echo $key->adjustmenttypename;?></option>
									<?php endforeach;?>	
									</select>                  
								</div>
								<div class="input-field overlayfield"> 
									<input type="text" id="adjustmentvalue" name="adjustmentvalue" value="" tabindex="1002" class="" >
									<label for="adjustmentvalue">Value</label>
								</div>
							</div>
						</div>
						<div class="alertbuttonarea">
							<input type="button" id="adjustmentadd" name="adjustmentadd" value="Submit" tabindex="1003" class="alertbtn " >
							<input type="button" id="adjustmentclose" name="" value="Cancel" tabindex="1004" class="alertbtn flloop  alertsoverlaybtn" >
							<span class="lasttab" tabindex="1005"></span>
						</div>
					</div>
				</span>
			</form>
			<div class="row">&nbsp;</div>
		</div>
	</div>
</div>
<!--Modulenumber Search Grid---->
<div class="large-6 columns" style="position:absolute;">
	<div class="overlay" id="mnsearchoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="row sectiontoppadding">&nbsp;</div>			
		<div class="large-6 medium-6 small-11 large-centered medium-centered small-centered columns">
			<div class="row">
			<div class="large-12 columns sectionheaderformcaptionstyle" style="line-height:1.9rem;">
				Module Number Search
			</div>
			<?php
				$device = $this->Basefunctions->deviceinfo();
				if($device=='phone') {
					echo '<div class="large-12 columns paddingzero forgetinggridname" id="modulenumbersearchgridwidth"><div id="modulenumbersearchgrid" class="inner-row-content inner-gridcontent" style="max-width:2000px; height:360px;top:0px;">
						<!-- Table header content , data rows & pagination for mobile-->
					</div>
					<!--<footer class="inner-gridfooter footercontainer" id="modulenumbersearchgridfooter">
						 Footer & Pagination content 
					</footer>--></div>';
				} else {
					echo '<div class="large-12 columns forgetinggridname" id="modulenumbersearchgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="modulenumbersearchgrid" style="max-width:2000px; height:360px;top:0px;">
					<!-- Table content[Header & Record Rows] for desktop-->
					</div>
					<!--<div class="inner-gridfooter footer-content footercontainer" id="modulenumbersearchgridfooter">
						 Footer & Pagination content 
					</div>--></div>';
				}
			?>
			</div>
			<div class="row" style="background:#465a63">
				<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
					<span class="firsttab" tabindex="1000"></span>
					<input type="button" id="mnsearchsubmit" name="" value="Submit" class="alertbtn ffield" tabindex="1001">	
					<input type="button" id="mnsearchclose" name="" value="Cancel" tabindex="1002" class="alertbtn  alertsoverlaybtn flloop" >
					<span class="lasttab" tabindex="1003"></span>
				</div>
			</div>
		</div>
	</div>
</div>