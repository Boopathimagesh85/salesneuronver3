<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Invoice extends MX_Controller{
	//To load the Register View
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Invoice/Invoicemodel');
		$this->load->view('Base/formfieldgeneration');
    }
	public function index() {
		$moduleid = array(86,95,226);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['calculationtype']=$this->Basefunctions->simpledropdown('calculationtype','calculationtypeid,calculationtypename','calculationtypename');
		$data['additionalcategory']=$this->Basefunctions->simpledropdown('additionalchargecategory','additionalchargecategoryid,additionalchargecategoryname','additionalchargecategoryname');
		$data['category']=$this->Invoicemodel->taxcategory();
		$data['adjusttype']=$this->Basefunctions->simpledropdown('adjustmenttype','adjustmenttypeid,adjustmenttypename','adjustmenttypename');
		$data['conv_currency']=$this->Basefunctions->simpledropdown('currency','currencyid,currencyname,currencycountry','currencyname');
		$data['dashboradviewdata'] = $this->Basefunctions->dashboarddataviewbydropdown($moduleid);
		$this->load->view('Invoice/invoiceview',$data);	   
	}
	/*	*Create New Invoice	*/
	public function invoicecreate() {  
    	$this->Invoicemodel->invoicecreatemodel();
    }
	//information fetch
	public function fetchformdataeditdetails() {	
		$moduleid = array(86,95,226);
		$this->Invoicemodel->informationfetchmodel($moduleid);
	}
	//Update old data
    public function datainformationupdate() {
        $this->Invoicemodel->datainformationupdatemodel();
    }
	//delete old information
    public function invoicedelete() {	
    	$moduleid = array(86,95,226);
        $this->Invoicemodel->invoicedeletemodel($moduleid);
    }
	//primary and secondary address value fetch
	public function primaryaddressvalfetch() {
		$this->Invoicemodel->primaryaddressvalfetchmodel();
	}		
	public function getquotenumber() {
		$this->Invoicemodel->getquotenumber();
	}
	public function invoicedetailfetch() {
		$this->Invoicemodel->summarydetail();
	}
	public function invoiceproductdetailfetch() {
		$this->Invoicemodel->retrievesoproductdetail();
	}
	public function retrievepaymentdetail() {
		$this->Invoicemodel->retrievepaymentdetail();
	}
	//drop down value set with multiple condition
	public function fetchmaildddatawithmultiplecond() {
		$this->Invoicemodel->fetchmaildddatawithmultiplecondmodel();
	} 
	//terms and condition data fetch
	public function termsandcontdatafetch() {
		$this->Invoicemodel->termsandcontdatafetchmodel();
	}
	//editor value fetch
	public function getsalesordernumber() {
		$this->Invoicemodel->getsonumber();
	} 	
	//editor value fetch 	
	public function editervaluefetch() {
		$filename = $_GET['filename']; 
		$newfilename = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]);
			echo json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			echo json_encode($tccontent);
		}
	}	
	//retrieves basic salesorder data
	public function getmoduledata() {
		$this->Invoicemodel->getmoduledata();
	}
	//retrieves salesorder address data	
	public function  getsoaddressdetail() {
		$parentid=$_GET['dataprimaryid'];
		$addressdetail=$this->Basefunctions->crmaddressfetch($parentid,'salesorderid','salesorderaddress');
	}
	/*	* Invoice stage-data retrival for Lost-Cancel-Draft-Convert	*/
	public function invoicecurrentstage(){
		$invoiceid = trim($_GET['invoiceid']);
		$data=$this->Invoicemodel->invoicecurrentstage($invoiceid);
		echo json_encode($data);
	}
	//retrieves salesorder product data
	public function getsoproductdetail() {
		$id=$_GET['primarydataid'];
		$this->Invoicemodel->getsoproductdetail($id);
	}
	//retrieve salesorder summary data
	public function getsosummarydata() {
		$id=$_GET['dataprimaryid'];
		$this->Invoicemodel->getsosummarydetail($id);
	}
	//cancel Invoicemodel
	public function canceldata() {
		$this->Invoicemodel->canceldata();		
	}
	//book invocie
	public function bookinvoice() {
		$this->Invoicemodel->bookinvoice();
	}
	public function getmoduleid(){
		$modulearray = array(86,95,226);
		$data=$this->db->select("moduleid")->from("module")->where_in('moduleid',$modulearray)->where('status',1)->limit(1)->get();
		foreach($data->result() as $info){
			$moduleid = $info->moduleid;
		}
		echo $moduleid;
	}
	//product storage fetch - instock
	public function productstoragefetchfun() {
		$productid = $_GET['productid'];
		$totalstock = $this->Invoicemodel->productstoragefetchfunmodel($productid);
		echo json_encode($totalstock);
	}
}