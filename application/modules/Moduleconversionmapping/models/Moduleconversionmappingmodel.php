<?php
Class Moduleconversionmappingmodel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->model('Base/Crudmodel');
    }
	public function constantmodulemapfields($sourcemoduleid,$destmoduleid) {
		$destinate_data=$source_data=array();
		$this->db->select('tomodulefieldid,frommodulefieldid');
		$this->db->from('moduleconversionmapping');				
		$this->db->where('moduleid',$sourcemoduleid);
		$this->db->where('mappingmoduleid',$destmoduleid);
		$this->db->where('createuserid',1);
		$this->db->where('status',1);
		$results = $this->db->get();
		foreach($results->result() as $inf){
			$destinate_data[] = $inf->tomodulefieldid;
			$source_data[] = $inf->frommodulefieldid;
		}
		$f_data=array('destinate_data'=>$destinate_data,'source_data'=>$source_data);
		return $f_data;
	}
	public function simplegroupdropdown($did,$dname,$table,$whfield,$sourcemoduleid,$destmoduleid,$uitype,$columnname,$constantmodulemap) {
		$i=0;
		$this->db->select("$dname,$did,$uitype,moduletabid");
		$this->db->from($table);	
		$this->db->where_in('moduletabid',array($sourcemoduleid,$destmoduleid));
		if(count($constantmodulemap['destinate_data']) > 0){	
		$this->db->where_not_in('modulefieldid',$constantmodulemap['destinate_data']);}
		if(count($constantmodulemap['source_data']) > 0){	
		$this->db->where_not_in('modulefieldid',$constantmodulemap['source_data']);}
		$this->db->where_not_in('uitypeid',array(15,16,12,13,27,21,22,23,24));
		$this->db->where_not_in('active','No');
		$this->db->where('status',1);
		$result = $this->db->get();
		$source = $destination =array();
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				if($row->moduletabid == $sourcemoduleid){
					$source[]=array('datasid'=>$row->$did,'dataname'=>$row->$dname,'uitype'=>$row->$uitype);
				} 
				else if ($row->moduletabid == $destmoduleid){
					$destination[]=array('datasid'=>$row->$did,'dataname'=>$row->$dname,'uitype'=>$row->$uitype);
				}
			}
			echo json_encode(array('source'=>$source,'destination'=>$destination));
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
    }
	//new data add form
	public function newdatacreatemodel() {
		$cdate = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$status	= $this->Basefunctions->activestatus;
		$moduleid = $_POST['moduleid'];
		$mappingmoduleid = $_POST['mappingmoduleid'];
		$frommodulefieldid = $_POST['frommodulefieldid'];
		$tomodulefieldid = $_POST['tomodulefieldid'];
		//check previous data existance
		$where_array=array('moduleid'=>$moduleid,'mappingmoduleid'=>$mappingmoduleid,'frommodulefieldid'=>$frommodulefieldid,'status'=>1);
		$data_chk = $this->db->select('moduleconversionmappingid')->from('moduleconversionmapping')->where($where_array)->limit(1)->get();
		if($data_chk->num_rows() == 0){
			$leadmap = array(
					'moduleid'=>$moduleid,
					'mappingmoduleid'=>$mappingmoduleid,
					'frommodulefieldid'=>$frommodulefieldid,
					'tomodulefieldid'=>$tomodulefieldid,
					'industryid'=>$this->Basefunctions->industryid,
					'branchid'=>$this->Basefunctions->branchid,
					'createdate'=>$cdate,
					'lastupdatedate'=>$cdate,
					'createuserid'=>$userid,
					'lastupdateuserid'=>$userid,
					'status'=>$status
				);
			$this->db->insert('moduleconversionmapping',$leadmap);
		} else {
			foreach($data_chk->result() as $info){
				$moduleconversionmappingid = $info->moduleconversionmappingid;
			}
			$widgetmap = array(
				'frommodulefieldid'=>$frommodulefieldid,
				'tomodulefieldid'=>$tomodulefieldid,
				'lastupdatedate'=>$cdate,
				'lastupdateuserid'=>$userid,
				'status'=>$status
			);
			$this->db->where('moduleconversionmapping.moduleconversionmappingid',$moduleconversionmappingid);
			$this->db->update('moduleconversionmapping',$widgetmap);
		}
		echo "TRUE";
	}
	public function checkleadfieldid($leadid) {
		$data ="False";
		$this->db->select('leadid,leadconversionmappingid');
		$this->db->from('leadconversionmapping');
		$this->db->where_in('leadconversionmapping.leadid',$leadid);
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				$data = $row->leadconversionmappingid;
			}
			return $data;
		} else { 
			return $data;
		}
	}
	//edit operation
	public function editdatacreatemodel() {
		$cdate = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$status	= $this->Basefunctions->activestatus;
		$leadid = $_POST['leadfieldid'];
		$accountid = $_POST['accountfieldid'];
		if($accountid != ""){
			$accid = $accountid;
		} else {
			$accid = 1;
		}
		$contactid = $_POST['contactfieldid'];
		if($contactid != ""){
			$contid = $contactid;
		} else {
			$contid = 1;
		}
		$opportunityid = $_POST['opprtunityfieldid'];
		if($opportunityid != "") {
			$oppid = $opportunityid;
		} else {
			$oppid = 1;
		}
		$editid = $_POST['editid'];
		$leadmap = array(
				'leadid'=>$leadid,
				'accountid'=>$accid,
				'contactid'=>$contid,
				'opportunityid'=>$oppid,
				'lastupdatedate'=>$cdate,
				'lastupdateuserid'=>$userid,
				'status'=>$status
			);
			$this->db->where('leadconversionmapping.leadconversionmappingid',$editid);
			$this->db->update('leadconversionmapping',$leadmap);
			echo "TRUE";
	}
	//delete information
	public function deleteinformationdatamodel() {
		$partabname = 'moduleconversionmapping';
		$primaryname = 'moduleconversionmappingid';
		$id = $_GET['primarydataid'];
		$ctable= '';
		$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
		echo "TRUE";
	}
	//drop down value fetch 
	public function fetchdddatawithcondmodel() {
		$id = $_GET['fieldid'];
		$name = $_GET['fieldname'];
		$tablename = $_GET['tablename'];
		$moduleid = $_GET['moduleid'];
		$value = $_GET['value'];
		$whfield = $_GET['moduletabid'];
		$i=0;
		$this->db->select("$name,$id");
		$this->db->from($tablename);
		if($moduleid != '') {
			$this->db->where_in($whfield,$moduleid);
		}
		$this->db->where_not_in('active','No');
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->$id,'dataname'=>$row->$name);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//data fetch
	public function datafetchineditformmodel() {
		$id = $_GET['primarydataid'];
		$this->db->select('leadid,accountid,contactid,opportunityid');
		$this->db->from('leadconversionmapping');
		$this->db->where('leadconversionmapping.leadconversionmappingid',$id);
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('leadid'=>$row->leadid,'accountid'=>$row->accountid,'contactid'=>$row->contactid,'opportunityid'=>$row->opportunityid);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//account-contact-opportunity field name check
	public function accountfieldnamecheckmodel() {
		$fieldid = $_POST['fieldid'];
		$primaryid = $_POST['primaryid'];
		$tablename = $_POST['tablename'];
		$tableid = $_POST['tableid'];
		if($fieldid != "") {
			$result = $this->db->select($tableid)->from($tablename)->where($tablename.'.'.$tableid,$fieldid)->where($tablename.'.status',1)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->$tableid;
				} 
			} else {
				echo "False";
			}
		} else {
			echo "False";
		}
	}
}
?>