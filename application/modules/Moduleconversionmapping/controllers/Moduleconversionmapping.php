<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Moduleconversionmapping extends CI_Controller {
	function __construct() {
    	parent::__construct();
		$this->load->model('Moduleconversionmapping/Moduleconversionmappingmodel');
		$this->load->helper('formbuild');
		$this->load->model('Base/Basefunctions');
    }
	public function index() {
		$moduleid = array(27);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Moduleconversionmapping/moduleconversionmappingview',$data);
	}
	//Create New data
	public function modulefieldname() {
		$sourcemoduleid=trim($_GET['sourcemoduleid']);
		$destmoduleid=trim($_GET['destmoduleid']);
		//get system constanct fields map
		$constantmodulemap=$this->Moduleconversionmappingmodel->constantmodulemapfields($sourcemoduleid,$destmoduleid);
		//restricted
    	$this->Moduleconversionmappingmodel->simplegroupdropdown('modulefieldid','fieldlabel','modulefield','moduletabid',$sourcemoduleid,$destmoduleid,'uitypeid','columnname',$constantmodulemap);
    }
	//Create New data
	public function newdatacreate() {
    	$this->Moduleconversionmappingmodel->newdatacreatemodel();
    }//Edit data
	public function editdatacreate() {
    	$this->Moduleconversionmappingmodel->editdatacreatemodel();
    }
	//Delete New data
	public function deleteinformationdata() {
    	$this->Moduleconversionmappingmodel->deleteinformationdatamodel();
    }
	//dropdown datafetch
	public function fetchdddatawithcond() {
    	$this->Moduleconversionmappingmodel->fetchdddatawithcondmodel();
    }
	//data fetch
	public function datafetchineditform() {
    	$this->Moduleconversionmappingmodel->datafetchineditformmodel();
    }
	public function accountfieldnamecheck() {
    	$this->Moduleconversionmappingmodel->accountfieldnamecheckmodel();
    }
}