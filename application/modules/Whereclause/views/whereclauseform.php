<!-- Form Header -->
<?php 
$CI =& get_instance();
$appdateformat = $CI->Basefunctions->appdateformatfetch(); 
?>
		<?php
				$device = $this->Basefunctions->deviceinfo();
				if($device=='phone') {
					echo '<div class="large-12 columns headercaptionstyle headerradius rolesgridformreload paddingzero">
							<div class="large-6 medium-6 small-8 columns headercaptionleft">
								<span  data-activates="slide-out" class="headermainmenuicon button-collapse" title="Menu"><i class="material-icons">menu</i></span>
								<span class="gridcaptionpos"><span>'.$gridtitle['title'].'</span></span>
							</div>
							<div class="large-6 medium-6 small-4 columns righttext mainaction foriconcolor">';
								$this->load->view('Base/usermenugenerate');
					echo 	'</div>';
					echo '</div>';
				} else {
					echo '<div class="large-12 columns headercaptionstyle headerradius rolesgridformreload  paddingzero">
							<div class="large-6 medium-6 small-6 columns headercaptionleft">
								<span  data-activates="slide-out" class="headermainmenuicon button-collapse" title="Menu"><i class="material-icons">menu</i></span>
								<span class="gridcaptionpos"><span>'.$gridtitle['title'].'</span></span>
							</div>
							<div class="large-6 medium-6 small-6 columns righttext mainaction foriconcolor">';
								$this->load->view('Base/usermenugenerate');
					echo 	'</div>';
					echo '</div>';
				}
			?>
<!--Tab Group-->
<div class="large-12 columns tabgroupstyle show-for-large-up ">
	<ul class="tabs" data-tab="">
		<li class="tab-title sidebaricons ftab active" data-subform="1">
			<span><?php echo $gridtitle['title'];  ?></span>
		</li>
	</ul>
</div>
<div class="large-12 columns addformunderheader">&nbsp; </div>
<div class="large-12 columns addformcontainer">
	<div class="row">&nbsp;</div>
	<form method="POST" name="lotform" class="whereclausform"  id="whereclausform">
		<div id="addwhereclausevalidate" class="validationEngineContainer" >
			<div id="editwhereclausevalidate" class="validationEngineContainer" >
			<div id="subformspan1" class="hiddensubform"> 
				<div class="large-4 columns paddingbtm">	
				<div class="large-12 columns cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;">
						<div class="large-12 columns headerformcaptionstyle">Where Clause Creation</div>
						<input type="hidden" value="" name="editid" id="editid" />
						<div class="static-field large-6 columns">
								<label>Module Name<span class="mandatoryfildclass">*</span></label>
								<select class="chzn-select validate[required]" data-prompt-position="topLeft:14,36" id="whereclausemodule" name="whereclausemodule" data-prompt-position="topLeft">
									<option value=""></option> 	
									<?php foreach($module as $key):?>
									<option  value="<?php echo $key->moduleid;?>" ><?php echo strtoupper($key->modulename);?></option>
									<?php endforeach;?>	
								</select>
						</div>
						<div class="static-field large-6 columns">
								<label>Where clause type<span class="mandatoryfildclass">*</span></label>
								<select class="chzn-select validate[required]" id="whereclausetype" name="whereclausetype" data-prompt-position="topLeft:14,36">
									<option value=""></option> 
									<?php foreach($whereclausetype as $key):?>
									<option  value="<?php echo $key->whereclausetypeid;?>" ><?php echo strtoupper($key->whereclausetypename);?></option>
									<?php endforeach;?>	
						</select>
						</div>	
						<div id="querybased">
						<div class="static-field large-6 columns orderbydiv">
								<label>Field Name<span class="mandatoryfildclass">*</span></label>
									<select data-placeholder="Select" data-prompt-position="topLeft:14,36" class="validate[required] chzn-select chosenwidth viewdropdownchange" tabindex="10" name="reportcondcolumn" id="reportcondcolumn">
								<option></option>
							</select> 
								<input type="hidden" name="reportcondcolumnid" id="reportcondcolumnid" value="" />
						</div>
						<div class="static-field large-6 columns orderbyhidediv">
								<label>Condition<span class="mandatoryfildclass">*</span></label>
								<select id="reportcondition" class="validate[required] chzn-select chosenwidth dropdownchange select2-offscreen whitecss forsucesscls" name="reportcondition" tabindex="-1" data-placeholder="Select" data-prompt-position="topLeft:14,36" title="">
								<option></option>
								<option value="Equalto">Equalto</option>
								<option value="NotEqual">NotEqual</option>
								<option value="Startwith">Startwith</option>
								<option value="Endwith">Endwith</option>
								<option value="Middle">Middle</option>
								</select>
						</div>
						<div class="input-field large-12 columns viewcondclear orderbyhidediv" id="reportcondvaluedivhid">
							<input type="text" class="" id="reportcondvalue" name="reportcondvalue"  tabindex="12" >
							<label for="reportcondvalue">Value<span class="mandatoryfildclass">*</span></label>
						</div>
						<div class="static-field large-12 columns viewcondclear orderbyhidediv" id="reportddcondvaluedivhid">
							<label>Value<span class="mandatoryfildclass">*</span></label>
							<select data-placeholder="Select"  data-prompt-position="topLeft:14,36" class="validate[required] chzn-select chosenwidth dropdownchange" tabindex="13" name="reportddcondvalue" id="reportddcondvalue" multiple>
								<option></option>
							</select>  
							<input type="hidden" id="ddcondvalue" name="ddcondvalue" value="" />
						</div>
						<div class="input-field large-12 columns viewcondclear orderbyhidediv" id="reportdatecondvaluedivhid">
						<input type="text" class="" data-dateformater="<?php echo $appdateformat; ?>" id="reportdatecondvalue" name="reportdatecondvalue"  tabindex="12" >
						<label for="reportdatecondvalue">Value<span class="mandatoryfildclass">*</span></label>
						</div>
						</div>
						<div class="input-field large-12 columns">
							<input type="text" name="description" class="" id="description">
							<label for="description">Description</label>
						</div>
						<div class="input-field large-12 columns" id='whereclausequerydiv'>
							<textarea name="whereclausequery" class="materialize-textarea" id="whereclausequery" rows="3" cols="40"></textarea>
							<label for="whereclausequery">Where clause query</label>
						</div>	
						<div class="large-12 columns " style="text-align:right">
							<label>&nbsp;</label>						
							<input type="button" class="btn" id="whereclausecreate" name="" value="SUBMIT" tabindex="">
							<input type="button" class="btn" id="whereclauseupdate" name="" value="SUBMIT" tabindex="" style="display: none;">	
							<input type ="hidden" id="tagweight"/>
						</div>
					</div>	
					<div class="large-12 columns "> &nbsp; </div>
					</div>
				</div>				
			</div>
			</div>	
			
	</form>
	<div class="large-8 columns paddingbtm">	
		<div class="large-12 columns viewgridcolorstyle" style="padding-left:0;padding-right:0;">	
			
				<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0rem 0rem; height:auto; line-height:1.8rem;">	
					<span class="large-6  medium-6 small-12 columns small-only-text-center" style="text-align:left">Where Clause Entries<span class="fwgpaging-box rategridfootercontainer"></span></span>
							<span class="large-6 medium-6 small-12 columns innergridicon righttext small-only-text-center" style="text-align:right">
										<span id="editicon"  class="editiconclass" title="Edit"><i class="material-icons">edit</i></span>
										<span id="deleteicon" class="deleteiconclass"  title="Delete"><i class="material-icons">delete</i></span>
										<span id="reloadicon" class="reloadiconclass" title="Reload"><i class="material-icons">refresh</i></span>
										<span id="searchicon" class="searchiconclass" title="Search"><i class="material-icons">search</i></span>
							</span>		
				</div>
				<?php
									$device = $this->Basefunctions->deviceinfo();
									if($device=='phone') {
										echo '<div class="large-12 columns paddingzero forgetinggridname" id="whereclausegridwidth"><div class=" inner-row-content inner-gridcontent" id="whereclausegrid" style="height:420px;top:0px;">
											<!-- Table header content , data rows & pagination for mobile-->
										</div>
										<footer class="inner-gridfooter footercontainer" id="whereclausegridfooter">
											<!-- Footer & Pagination content -->
										</footer></div>';
									} else {
										echo '<div class="large-12 columns forgetinggridname" id="whereclausegridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="whereclausegrid" style="max-width:2000px; height:420px;top:0px;">
										<!-- Table content[Header & Record Rows] for desktop-->
										</div>
										<div class="inner-gridfooter footer-content footercontainer" id="whereclausegridfooter">
											<!-- Footer & Pagination content -->
										</div></div>';
									}
								?>						
			</div>
	</div>
</div>