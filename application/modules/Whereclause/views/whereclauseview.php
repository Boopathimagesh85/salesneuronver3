<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('Base/headerfiles'); ?>
	<?php $this->load->view('Base/overlay'); ?>
	<?php 
		$this->load->view('Base/Basedeleteform'); 
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$this->load->view('Base/overlaymobile');
		} else {
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
	?>
</head>
<body>
	<div class="row" style="max-width:100%">
		<div class="off-canvas-wrap" data-offcanvas>
			<div class="inner-wrap">
				<!-- Add Form -->
				<div id="lotcreationaddformdiv" class="">
					<?php $this->load->view('whereclauseform'); ?>
				</div>				
			</div>
		</div>
	</div>
	<?php
		$this->load->view('Base/basedeleteformforcombainedmodules');
	?>
</body>
<div class="large-12 columns" style="position:absolute;"><div class="overlay" id="viewdeleteoverlayconform" style="z-index:120;"></div></div>
<?php $this->load->view('Base/bottomscript'); ?>
<?php include 'js/Whereclause/whereclause.js' ?>	
</html>
