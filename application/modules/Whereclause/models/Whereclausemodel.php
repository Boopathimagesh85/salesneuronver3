<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Whereclausemodel extends CI_Model
{    
    private $whereclausemoduleid = 55;
    public function __construct()
    {		
		parent::__construct();
		$this->load->model('Reports/Reportsmodel');
    }
	//first basic hitting view
    public function index()
    {
		
	}
	/**
	*create whereclause	
	*/
	public function whereclausecreate()  
	{		
		$whereclauseid = $this->Basefunctions->varrandomnumbergenerator($this->whereclausemoduleid,'whereclauseid',1,''); //serialnumber
		if($_POST['whereclausetype']==2)
		{
			if(isset($_POST['reportcondvalue']) && $_POST['reportcondvalue']!=''){$value=$_POST['reportcondvalue'];}
			else if(isset($_POST['ddcondvalue']) && $_POST['ddcondvalue']!=''){$value=$_POST['ddcondvalue'];}
			else if(isset($_POST['reportdatecondvalue']) && $_POST['reportdatecondvalue']!=''){$value=$_POST['reportdatecondvalue'];}
			else{ $value='';}
			//Generating the where condition
			$contionalarray=$this->generateconditioninfo($_POST['reportcondcolumn'],$_POST['reportcondition'],$value);
			$wherequery=$this->generatewhereclause($_POST['reportcondcolumn'],$contionalarray);
		}
		else
		{	
			$_POST['reportcondcolumn']='';
			$_POST['reportcondition']='';
			$value='';
			$wherequery=$_POST['whereclausequery'];
		}
		
		$insert=array(
						'whereclauseid'=>$whereclauseid,
						'moduleid'=>$_POST['whereclausemodule'],
						'description'=>$_POST['description'],						
						'whereclausetypeid'=>$_POST['whereclausetype'],						
						'viewcreationcolumnid'=>$_POST['reportcondcolumn'],						
						'conditionname'=>$_POST['reportcondition'],						
						'conditionvalue'=>$value,						
						'wherequery'=>$wherequery,
						'industryid'=>$this->Basefunctions->industryid,
						'status'=>$this->Basefunctions->activestatus
					);
		$insert = array_merge($insert,$this->Crudmodel->defaultvalueget());
	    $this->db->insert('whereclausecreation',array_filter($insert));
		$primaryid = $this->db->insert_id();
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity =$user.' created whereclausemodule';
		$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,$this->whereclausemoduleid);
		echo 'SUCCESS';
	}
	/**
	*retrieve whereclauseretrive	
	*return JSON lot-data
	*/
	public function whereclauseretrive()
	{	
		$id=$_GET['primaryid'];
		$this->db->select('SQL_CALC_FOUND_ROWS wh.whereclausecreationid,wh.whereclauseid,wh.moduleid,wh.description,wh.whereclausetypeid,wh.viewcreationcolumnid,wh.conditionname,wh.conditionvalue,wh.wherequery,view.viewcreationcolumnname,module.modulename',false);
		$this->db->from('whereclausecreation as wh');
		$this->db->join('module','module.moduleid = wh.moduleid');
		$this->db->join('viewcreationcolumns as view','view.viewcreationcolumnid = wh.viewcreationcolumnid','LEFt OUTER');
		$this->db->where('wh.whereclausecreationid',$id);
		$this->db->where_in('wh.status',$this->Basefunctions->activestatus);
		$info=$this->db->get()->row();
		$jsonarray=array(
						'editid'=>$info->whereclausecreationid,
						'whereclausemodule'=>$info->moduleid,
						'description'=>$info->description,						
						'whereclausetype'=>$info->whereclausetypeid,						
						'reportcondcolumn'=>$info->viewcreationcolumnid,						
						'reportcondition'=>$info->conditionname,						
						'reportcondvalue'=>$info->conditionvalue,
						'reportddcondvalue'=>$info->conditionvalue,
						'reportdatecondvalue'=>$info->conditionvalue,
						'whereclausequery'=>$info->wherequery
						);
		echo json_encode($jsonarray);
	}
	/**	*update whereclause		*/
	public function whereclauseupdate()
	{
		$id=$_POST['primaryid'];		
		if($_POST['whereclausetype']==2)
		{	
			if(isset($_POST['reportcondvalue']) && $_POST['reportcondvalue']!=''){$value=$_POST['reportcondvalue'];}
			else if(isset($_POST['ddcondvalue']) && $_POST['ddcondvalue']!=''){$value=$_POST['ddcondvalue'];}
			else if(isset($_POST['reportdatecondvalue']) && $_POST['reportdatecondvalue']!=''){$value=$_POST['reportdatecondvalue'];}
			else{ $value='';}
			$contionalarray=$this->generateconditioninfo($_POST['reportcondcolumn'],$_POST['reportcondition'],$value);
			$wherequery=$this->generatewhereclause($_POST['reportcondcolumn'],$contionalarray);
		} 
		else
		{	$_POST['reportcondcolumn']=0;
			$_POST['reportcondition']='';
			$value='';
			$wherequery=$_POST['whereclausequery'];
		}	
		$update=array(
						'moduleid'=>$_POST['whereclausemodule'],
						'description'=>$_POST['description'],						
						'whereclausetypeid'=>$_POST['whereclausetype'],						
						'viewcreationcolumnid'=>$_POST['reportcondcolumn'],						
						'conditionname'=>$_POST['reportcondition'],						
						'conditionvalue'=>$value,						
						'wherequery'=>$wherequery
					 );
		$update=array_merge($update,$this->Crudmodel->updatedefaultvalueget());
	    $this->db->where('whereclausecreationid',$id);
		$this->db->update('whereclausecreation',$update);
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity =$user.' updated whereclause';
		$this->Basefunctions->notificationcontentadd($id,'Updated',$activity ,$userid,$this->whereclausemoduleid);
		echo 'SUCCESS';
	}
	/***inactivate-delete lot grid	*/
	public function whereclausedelete()
	{
		$id=$_GET['primaryid'];
		//inactivate lot
		$delete = $this->Basefunctions->delete_log();
		 $this->db->where('whereclausecreationid',$id);
		$this->db->update('whereclausecreation',$delete);		
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity = ''.$user.' deleted whereclause - '.$id.'';
		$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,$this->whereclausemoduleid);
		echo 'SUCCESS';
	}	
	//condition information fetch	
    public function generateconditioninfo($viewid,$condition,$value) 
	{
		$i=0;
		$multiarray = array();
		$viewcolids = [];
		//user for get the condition values;
		$this->db->select('viewcreationcolumns.viewcreationcolumnid,viewcreationcolumns.viewcreationcolmodeljointable,viewcreationcolumns.viewcreationcolmodelname,viewcreationcolumns.viewcreationcolmodelindexname,viewcreationjoincolmodelname,uitypeid',false);
		$this->db->from('viewcreationcolumns');
		$this->db->where_in('viewcreationcolumns.viewcreationcolumnid',$viewid);
		$this->db->where_in('viewcreationcolumns.status',$this->Basefunctions->activestatus);
		$condtionval=$this->db->get();
		foreach($condtionval->result() as $row) {
			$multiarray[$i] = array(
										'0'=>$row->viewcreationcolmodeljointable,
										'1'=>$row->viewcreationcolmodelindexname,
										'2'=>$condition,
										'3'=>$value,
										'4'=>'',
										'5'=>$row->viewcreationjoincolmodelname,
										'6'=>$row->uitypeid
									);
		}
		
		return $multiarray;
    }
	//where condition generation-new
    public function generatewhereclause($viewcreationcolumnid,$conditionvalarray) 
	{
		$whereString="";
		foreach ($conditionvalarray as $key) 
		{
		   if($key[4] == '' or $key[4] == null){
				$key[4]='AND';
			}
			$dduitypes = array(17,18,19,20,25,26);
			if(in_array($key[6],$dduitypes)){
				$field = $key[5];
			} else {
				$field = $key[1];
			}
			
			switch($key[2]) {
				case "Equalto": 
					$whereString .= $key[0].'.'.$field. " = '".$key[3]."'"." ". $key[4]." ";
					break;
				case "NotEqual": 
					$whereString .= $key[0].'.'.$field. " != '" .$key[3]."'". " ". $key[4]." ";
					break;
				case "Like":
				//	$whereString .= $key[0].'.'.$key[1]. " LIKE '".$key[3]"'"." ".  $key[4]." ";
				//	break;
				case "Startwith":
					$whereString .= $key[0].'.'.$field. " LIKE '".$key[3].'%'."'"." ".  $key[4]." ";
					break;
				case "Endwith": 
					$whereString .= $key[0].'.'.$field. " LIKE '".'%'.$key[3]."'". " ". $key[4]." ";
					break;
				case "Contains": 
					$whereString .= $key[0].'.'.$field. " LIKE '".'%'.$key[3].'%'."'". " ". $key[4]." ";
					break;
				case "IN": //specials
					$whereString .= $key[0].'.'.$field. " IN (".$key[3].")". " ". $key[4]." ";
					break;
				case "NOT IN": 
					$whereString .= $key[0].'.'.$field. " NOT IN (".$key[3].")". " ". $key[4]." ";
					break;
				case "Greater than": 
					$whereString .= $key[0].'.'.$field. " > '" .$key[3]."'"." ".  $key[4]." ";
					break;
				case "Less than": 
					$whereString .= $key[0].'.'.$field. " < '" .$key[3]."'". " ". $key[4]." ";
					break;
				case "Greater than equal": 
					$whereString .= $key[0].'.'.$field." >= '" .$key[3]."'"." ".  $key[4]." ";
					break;
				case "Less than equal": 
					$whereString .= $key[0].'.'.$field. " <= '" .$key[3]."'". " ". $key[4]." ";
					break;
				case "IS NULL": 
					$whereString .= $key[0].'.'.$field. " IS NULL '" .$key[3]."'". " ". $key[4]." ";
					break;
				case "NOT LIKE": 
					$whereString .= $key[0].'.'.$field. " NOT LIKE '" .$key[3]."'". " ". $key[4]." ";
					break;
				case "IS NOT NULL": 
					$whereString .= $key[0].'.'.$field. " IS NOT NULL '" .$key[3]."'". " ". $key[4]." ";
					break;
				default:
					$whereString .= "";
					break;
			}
		  }
		$whereString= substr($whereString, 0, -4);
		$whereString=$viewcreationcolumnid.'|'.$whereString;  // append view creation columnid
		return $whereString;
    }
	
}
