<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Whereclause extends MX_Controller 
{	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
		$this->load->model('Whereclause/Whereclausemodel');	
	}
	private $whereclausemoduleid = 55;
	public function index()
	{			
		$moduleid = $this->whereclausemoduleid;
		sessionchecker($moduleid);
		//action		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array($moduleid);
		$viewmoduleid = array($moduleid);	
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd'] = $this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol'] = $this->Basefunctions->vardataviewdropdowncolumns($viewfieldsmoduleids);
		//form
		$data['module']=$this->Basefunctions->simpledropdown('module','moduleid,modulename','moduleid');
		$data['whereclausetype'] = $this->Basefunctions->simpledropdown('whereclausetype','whereclausetypeid,whereclausetypename','whereclausetypeid');
		$this->load->view('Whereclause/Whereclauseview',$data);
	}
	/**	*create whereclause	*/
	public function whereclausecreate()
	{
		$this->Whereclausemodel->whereclausecreate();
	}
	/**	*retrieve whereclauseretrive	*/
	public function whereclauseretrive() 
	{  
    	$this->Whereclausemodel->whereclauseretrive();
    }
	/**	*update whereclause	*/
	public function whereclauseupdate()
	{
		$this->Whereclausemodel->whereclauseupdate();
	}
	/**	*delete whereclause	*/
	public function whereclausedelete()
	{	
		$this->Whereclausemodel->whereclausedelete();
	}
}