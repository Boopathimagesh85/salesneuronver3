<?php
class Productmodel extends CI_Model{
	function __construct() {
		parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
	}
	/**	*Product Create	*/
	public function newdatacreatemodel() {
		$rw_count=1;
		$industryid = $this->Basefunctions->industryid;
		if(!isset($_POST['attributevalueid'])) { //Incase there is no attributevalue set empty
			$_POST['attributevalueid']='';
		}
		if(!isset($_POST['attributesetassignid'])) { //Incase there is no attributeset set 1
			$_POST['attributesetassignid']='';
			$attribute_ids = '';
		}
		if($industryid != 3) {
			if(trim($_POST['attributesetassignid']) > 0) {
				//retrieve attribute names of selected attributes
				$attri_column_name = $this->productattributecolumn($_POST['attributesetassignid'],9);
				$attribute_ids = '';
				if($attri_column_name != '') {
					//attribute_ids
					$attribute_ids=implode('_',$attri_column_name['attribute_ids']);
				}
			}
		}
		$attributevaluearray = explode(',',$_POST['attributevalueid']);
		$rw_count=count($attributevaluearray);
		if($rw_count == 0){
			$rw_count=1;
		}
		for($rw=0;$rw < $rw_count;$rw++){ //product variant insertion		
			//table and fields information
			$formfieldsname = explode(',',$_POST['elementsname']);
			$formfieldstable = explode(',',$_POST['elementstable']);
			$formfieldscolmname = explode(',',$_POST['elementscolmn']);
			$elementpartable = explode(',',$_POST['elementspartabname']);
			$restricttable = explode(',',$_POST['resctable']);		
			//industry based moduleid
			$moduleid = $_POST['viewfieldids'];
			//filter unique parent table
			$partablename =  $this->Crudmodel->filtervalue($elementpartable);
			//filter unique fields table
			$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
			$tableinfo = explode(',',$fildstable);
			$primaryid = $this->Crudmodel->datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable);
			//Now Update the attributevalueid
			if($_POST['attributevalueid'] != '') {
				$variant_productname=trim($_POST['productname']).'_'.$attributevaluearray[$rw];
				$att_array=array('attributevalues'=>$attributevaluearray[$rw],'productname'=>$variant_productname,'attributes'=>$attribute_ids);
				$this->db->where('productid',$primaryid);
				$this->db->update('product',$att_array);
			}
			$this->Basefunctions->updatechargecloneforproducttable($_POST['categoryid'],$primaryid); 
			//notification entry
			$empid = $this->Basefunctions->logemployeeid;
			$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
			$assignid = 1;
			if(isset($_POST['employeeid'])){
				$assignid = $_POST['employeeid'];
				if($assignid != '') {
					$assignid = $assignid;
				}
			} 
			//grid data insertion
			//primary key
			$defdataarr = $this->Crudmodel->defaultvalueget();
			$primaryname = $this->Crudmodel->primaryinfo($partablename);
			//grid information
			$gridfieldpartabname = explode(',',$_POST['griddatapartabnameinfo']);
			$girddata = $_POST['griddatas'];
			$griddatainfo = json_decode($girddata,true);
			$noofrows = $_POST['numofrows'];
			$gridpartablename =  $this->Crudmodel->filtervalue($gridfieldpartabname);
			if(!empty($griddatainfo)) {
				//productdetail insert
				$industryid = $this->Basefunctions->industryid;
				if($industryid == 3) {				
					$productrolgriddata = $griddatainfo[0];//rolgrid
				}else{
				   $productrolgriddata = $griddatainfo[1];//rolgrid
				   $productstoragegriddata = $griddatainfo[2];//storagegrid
				}
				if($industryid != 3) {
					$productuomgriddata = $griddatainfo[0];//uomgrid
					if(count($productuomgriddata) > 0) {
						for($im=0;$im<count($productuomgriddata);$im++) {
							$array=array('uomfromid'=>$productuomgriddata[$im]['uomfromid'],'uomtoid'=>$productuomgriddata[$im]['uomtoid'],'conversionrate'=>$productuomgriddata[$im]['conversionrate'],'productid'=>$primaryid);
							$this->db->insert('productuomconversion',array_merge($array,$defdataarr));
						}
					}
				}
				if($industryid == 3) {
				if(count($productrolgriddata) > 0) {
						for($im=0;$im<count($productrolgriddata);$im++) {
							$arraym=array('reordertypeid'=>$productrolgriddata[$im]['reordertypeid'],'reorderquantity'=>$productrolgriddata[$im]['reorderquantity'],'minimumquantity'=>$productrolgriddata[$im]['minimumquantity'],'maximumquantity'=>$productrolgriddata[$im]['maximumquantity'],'productid'=>$primaryid,'sizemasterid'=>$productrolgriddata[$im]['sizemasterid'],'fromweight'=>$productrolgriddata[$im]['fromweight'],'toweight'=>$productrolgriddata[$im]['toweight'],'accountid'=>$productrolgriddata[$im]['vendorid']);
							$this->db->insert('productrol',array_merge($arraym,$defdataarr));
						}
					}
				}
				if($industryid != 3) {
						if(count($productstoragegriddata) > 0) {
							for($im=0;$im<count($productstoragegriddata);$im++) {
								$arrayy=array('branchid'=>$productstoragegriddata[$im]['storagebranchid'],'counterid'=>$productstoragegriddata[$im]['storageid'],'productid'=>$primaryid);
								$this->db->insert('productstorage',array_merge($arrayy,$defdataarr));
							}
						}
				}
			}
			$prodname = $_POST['productname'];
			if($assignid == '1'){
				$notimsg = $this->Basefunctions->notificationtemplatedatafetch(9,$primaryid,$partablename,2);
				if($notimsg != '') {
					$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,9);
				}	
			} else {
				$notimsg = $this->Basefunctions->notificationtemplatedatafetch(9,$primaryid,$partablename,2);
				if($notimsg != '') {
					$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,9);
				}
			}
			{//workflow management -- for data create
				$this->Basefunctions->workflowmanageindatacreatemode($moduleid,$primaryid,$partablename);
			}
		}
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity = ''.$user.' created Product - '.$_POST['productname'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Created',$activity ,$userid,90);
		echo 'TRUE';
	}
	//Checks whether the FROM UOM-TO UOM-Product combination exists
	public function productuomcombination($uomfromid,$uomtoid,$productid) {
		$where=array('uomfromid'=>$uomfromid,'uomtoid'=>$uomtoid,'productid'=>$productid,'status'=>$this->Basefunctions->activestatus);
		$datarows=$this->db->select('productuomconversionid')
							->from('productuomconversion')
							->where($where)
							->limit(1)
							->get();
		if($datarows->num_rows() == 0){
			return FALSE;
		} else {
			foreach($datarows->result() as $inf){
				return $inf->productuomconversionid;
			}
		}		
	}
	/**	* Retrieves the Data For Edit	* @param $moduleid product-moduleid is passed to get product data.
	*/
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementpartable']);
		$restricttable = explode(',',$_GET['resctable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['dataprimaryid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$sndefault = $this->Basefunctions->singlefieldfetch('sndefault','productid','product',$primaryid); //Check default value
		if($sndefault == 1) {
			echo $result = $this->Crudmodel->dbdatafetchwithrestrict($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$restricttable,$moduleid);
		} else {
			echo json_encode(array("fail"=>'sndefault'));
		}
	}
	/**	*Product Update	*/
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		$restricttable = explode(',',$_POST['resctable']);
		$industryid = $this->Basefunctions->industryid;
		if(!isset($_POST['attributesetassignid']) ){ //check attributesetassignid empty
			$_POST['attributesetassignid']='';
		}
		if($_POST['attributesetassignid'] == ''){ //check attributesetassignid empty
			$_POST['attributesetassignid']=1;
		}
		if($industryid != 3) {
			if(trim($_POST['attributesetassignid']) > 0){
				//retrieve attribute names of selected attributes
				$attri_column_name = $this->productattributecolumn($_POST['attributesetassignid'],9);
				$attribute_ids = '';
				if($attri_column_name != ''){
					//attribute_ids
					$attribute_ids=implode('_',$attri_column_name['attribute_ids']);
				}
			}
		}
		//
		if(!isset($_POST['taxmasterid'])){
			$_POST['taxmasterid'] = 1;		
		}
		if($_POST['taxmasterid'] == ''){
			$_POST['taxmasterid'] = 1;
		}
		//
		$griddata = $_POST['griddatas'];
		$griddatainfo = json_decode($griddata, true);
		$noofrows = $_POST['numofrows'];
		$industryid = $this->Basefunctions->industryid;
			if($industryid == 3) {
				if(!empty($griddatainfo[0])) {
					$productrolgriddata = $griddatainfo[0];//rolgrid
				} else {
					$productrolgriddata = array();//rolgrid
				}
			}else{
				$productuomgriddata = $griddatainfo[0];//uomgrid
				$productrolgriddata = $griddatainfo[1];//rolgrid
				$productstoragegriddata = $griddatainfo[2];//storagegrid
			}
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['primarydataid'];
		$attr_variant = $this->db->select('variant,attributes')
								->from('product')
								->where('productid',$primaryid)
								->limit(1)
								->get()->result();
		foreach($attr_variant as $attr_val){
			$editvariant = $attr_val->variant;
			$editattributes = $attr_val->attributes;
		}
		//
		if($_POST['attributevalueid'] == ''){
			$pex=explode('_',$_POST['productname']);
			$_POST['productname'] = $pex[0];
			$att_array=array('attributevalues'=>'','attributes'=>'','variant'=>'No');
			$this->db->where('productid',$primaryid);
			$this->db->update('product',$att_array);
		}
		//industry based moduleid
		$moduleid = $_POST['viewfieldids'];
		{//fetch old values -- work flow
			$condstatvals=$this->Basefunctions->workflowmanageolddatainfofetch($moduleid,$primaryid);
		}
		$this->Crudmodel->dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable);
		//Now Update the attributevalueid
		if($editvariant != 'Yes' AND $editattributes !=''){ //restricts previous variant records
			if($_POST['attributevalueid'] != ''){
				$pex=explode('_',$_POST['productname']);
				$productname = $pex[0].'_'.trim($_POST['attributevalueid']);			
				$att_array=array('attributevalues'=>trim($_POST['attributevalueid']),'productname'=>$productname,'attributes'=>$attribute_ids);
				$this->db->where('productid',$primaryid);
				$this->db->update('product',$att_array);
			}
		}
		$defdataarr = $this->Crudmodel->defaultvalueget();
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$this->Basefunctions->updatechargecloneforproducttable($_POST['categoryid'],$primaryid); 
		if($industryid == 3) {
		}else{
			//productUOMdetail update		
			$olduomdetailid =array();
			$p_uom_detail=$this->db->select('productuomconversionid')
									->from('productuomconversion')
									->where('productid',$primaryid)
									->where('status',$this->Basefunctions->activestatus)
									->get();
			foreach($p_uom_detail->result() as $info) {
				$olduomdetailid[]=$info->productuomconversionid;
			}
			$new_puom_etailid=array();
			for($lm=0; $lm < count($productuomgriddata); $lm++) {
				$new_puom_etailid[]=$productuomgriddata[$lm]['productuomconversionid'];
			}
			//find deleted productuom
			$p_uom_deleteddetailid=ARRAY();
			for($m=0;$m < count ($olduomdetailid);$m++) {
				if(!in_array($olduomdetailid[$m],$new_puom_etailid)) {	
					$p_uom_deleteddetailid[]=$olduomdetailid[$m];
				}			
			}
			if(count($p_uom_deleteddetailid) > 0) {
				$deleteproductuom=array('status'=>0,'lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid);
				$this->db->where_in('productuomconversionid',$p_uom_deleteddetailid);
				$this->db->update('productuomconversion',$deleteproductuom);
			}
			//grid data insertion		
			//verify whether its new records
			if(count($productuomgriddata) > 0){
				for($im=0;$im<count($productuomgriddata);$im++) {
					if($productuomgriddata[$im]['productuomconversionid'] == 0){
						$puommatch=$this->productuomcombination($productuomgriddata[$im]['uomfromid'],$productuomgriddata[$im]['uomtoid'],$primaryid);
						if($puommatch == false){
							$array=array('uomfromid'=>$productuomgriddata[$im]['uomfromid'],'uomtoid'=>$productuomgriddata[$im]['uomtoid'],'conversionrate'=>$productuomgriddata[$im]['conversionrate'],'productid'=>$primaryid);
							$this->db->insert('productuomconversion',array_merge($array,$defdataarr));
						} else{
							$product_uom_reset=array('conversionrate'=>$productuomgriddata[$im]['conversionrate'],'lastupdateuserid'=>$this->Basefunctions->userid,'lastupdatedate'=>date($this->Basefunctions->datef));
							$this->db->where('productuomconversionid',$puommatch);
							$this->db->update('productuomconversion',$product_uom_reset);
						}
					}
				}
			}
		}
		//productROLdetail update	
			$oldroldetailid =array();
			$p_rol_detail=$this->db->select('productrolid')
									->from('productrol')
									->where('productid',$primaryid)
									->where('status',$this->Basefunctions->activestatus)
									->get();
			foreach($p_rol_detail->result() as $info) {
				$oldroldetailid[]=$info->productrolid;
			}
			$new_prol_etailid=array();
			for($lm=0; $lm < count($productrolgriddata); $lm++) {
				$new_prol_etailid[]=$productrolgriddata[$lm]['productrolid'];
			}
		//find deleted productuom
		$p_rol_deleteddetailid=ARRAY();
		for($m=0;$m < count ($oldroldetailid);$m++) {
			if(!in_array($oldroldetailid[$m],$new_prol_etailid)) {	
				$p_rol_deleteddetailid[]=$oldroldetailid[$m];
			}			
		}
		
		if(count($p_rol_deleteddetailid) > 0) {
			$deleteproductrol=array('status'=>0,'lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid);
			$this->db->where_in('productrolid',$p_rol_deleteddetailid);
			$this->db->update('productrol',$deleteproductrol);
		}
		//grid data insertion		
		//verify whether its new records
		if(count($productrolgriddata) > 0){
			for($im=0;$im<count($productrolgriddata);$im++) {
				if($productrolgriddata[$im]['productrolid'] == 0 and $productrolgriddata[$im]['productrolid'] == ""){
					if($industryid != 3) {
						$arraym=array('branchid'=>$productrolgriddata[$im]['branchid'],'reordertypeid'=>$productrolgriddata[$im]['reordertypeid'],'reorderquantity'=>$productrolgriddata[$im]['reorderquantity'],'minimumquantity'=>$productrolgriddata[$im]['minimumquantity'],'maximumquantity'=>$productrolgriddata[$im]['maximumquantity'],'productid'=>$primaryid);
						$this->db->insert('productrol',array_merge($arraym,$defdataarr));
					} else {
						$arraym=array('reordertypeid'=>$productrolgriddata[$im]['reordertypeid'],'reorderquantity'=>$productrolgriddata[$im]['reorderquantity'],'minimumquantity'=>$productrolgriddata[$im]['minimumquantity'],'maximumquantity'=>$productrolgriddata[$im]['maximumquantity'],'productid'=>$primaryid,'sizemasterid'=>$productrolgriddata[$im]['sizemasterid'],'fromweight'=>$productrolgriddata[$im]['fromweight'],'toweight'=>$productrolgriddata[$im]['toweight'],'accountid'=>$productrolgriddata[$im]['vendorid']);
						$this->db->insert('productrol',array_merge($arraym,$defdataarr));
					}
				} else {
					if($industryid != 3) {
						$arraym=array('branchid'=>$productrolgriddata[$im]['branchid'],'reordertypeid'=>$productrolgriddata[$im]['reordertypeid'],'reorderquantity'=>$productrolgriddata[$im]['reorderquantity'],'minimumquantity'=>$productrolgriddata[$im]['minimumquantity'],'maximumquantity'=>$productrolgriddata[$im]['maximumquantity'],'productid'=>$primaryid);
						$this->db->where('productrolid',$productrolgriddata[$im]['productrolid']);
						$this->db->update('productrol',$arraym);
					} else {
						$arraym=array('reordertypeid'=>$productrolgriddata[$im]['reordertypeid'],'reorderquantity'=>$productrolgriddata[$im]['reorderquantity'],'minimumquantity'=>$productrolgriddata[$im]['minimumquantity'],'maximumquantity'=>$productrolgriddata[$im]['maximumquantity'],'productid'=>$primaryid,'sizemasterid'=>$productrolgriddata[$im]['sizemasterid'],'fromweight'=>$productrolgriddata[$im]['fromweight'],'toweight'=>$productrolgriddata[$im]['toweight'],'accountid'=>$productrolgriddata[$im]['vendorid']);
						$this->db->where('productrolid',$productrolgriddata[$im]['productrolid']);
						$this->db->update('productrol',$arraym);
					}
					
				}
			}
		}
		//productstoragedetail update
		if($industryid != 3) {
			$oldstrdetailid =array();
			$p_str_detail=$this->db->select('productstorageid')
									->from('productstorage')
									->where('productid',$primaryid)
									->where('status',$this->Basefunctions->activestatus)
									->get();
			foreach($p_str_detail->result() as $info) {
				$oldstrdetailid[]=$info->productstorageid;
			}
			$new_pstr_etailid=array();
			for($lm=0; $lm < count($productstoragegriddata); $lm++) {
				$new_pstr_etailid[]=$productstoragegriddata[$lm]['productstorageid'];
			}
			//find deleted productuom
			$p_str_deleteddetailid=ARRAY();
			for($m=0;$m < count ($oldstrdetailid);$m++) {
				if(!in_array($oldstrdetailid[$m],$new_pstr_etailid)) {	
					$p_str_deleteddetailid[]=$oldstrdetailid[$m];
				}			
			}
			if(count($p_str_deleteddetailid) > 0) {
				$deleteproductrol=array('status'=>0,'lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid);
				$this->db->where_in('productstorageid',$p_str_deleteddetailid);
				$this->db->update('productstorage',$deleteproductrol);
			}
			//grid data insertion		
			//verify whether its new records
			if(count($productstoragegriddata) > 0){
				for($im=0;$im<count($productstoragegriddata);$im++) {
					if($productstoragegriddata[$im]['productstorageid'] == 0){
						$puommatch=$this->productstoragecombination($productstoragegriddata[$im]['storagebranchid'],$productstoragegriddata[$im]['storageid'],$primaryid);
						if($puommatch == false){
							$arraym=array('branchid'=>$productstoragegriddata[$im]['storagebranchid'],'counterid'=>$productstoragegriddata[$im]['storageid'],'productid'=>$primaryid);
							$this->db->insert('productstorage',array_merge($arraym,$defdataarr));
						}				
					}
				}
			}
		}
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		$assignid = 1;
		if(isset($_POST['employeeid'])){
			$assignid = $_POST['employeeid'];
			if($assignid != '') {
				$assignid = $assignid;
			}
		} 
		$prodname = $_POST['productname'];
		if($assignid == '1'){
			$assignto = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$assignid);
			$notimsg = $empname." "."Updated a Product"." - ".$prodname;
		}else {
			$assignto = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$assignid);
			$notimsg = $empname." "."Updated a Product"." - ".$prodname." - Assigned To ".$assignto;
		}
		$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,9);
		$mainproductid=$primaryid; #assign the product
		#Special Case
		#Variant Creation On Edit Mode
		#Bit Long Process MaN
		if($editvariant != 'Yes' AND $editattributes !=''){ //restricts previous variant records
		$rw_count=1;
		if(!isset($_POST['attributevalueid'])){ //Incase there is no attributevalue set empty-
			$_POST['attributevalueid']='';
		}	
		$attributevaluearray = array_filter(explode(',',$_POST['attributevalueid']));
		$rw_count=count($attributevaluearray);
			if($rw_count > 1){	#When the Product has Variant On edit	
				for($rw=0;$rw < $rw_count;$rw++){ //product variant insertion		
					$griddata = $_POST['griddatas'];
					$griddatainfo = json_decode($griddata, true);
					$noofrows = $_POST['numofrows'];
					//table and fields information
					$formfieldsname = explode(',',$_POST['elementsname']);
					$formfieldstable = explode(',',$_POST['elementstable']);
					$formfieldscolmname = explode(',',$_POST['elementscolmn']);
					$elementpartable = explode(',',$_POST['elementspartabname']);
					$restricttable = explode(',',$_POST['resctable']);		
					$gridfieldpartabname = explode(',',$_POST['griddatapartabnameinfo']);
					$gridpartablename =  $this->Crudmodel->filtervalue($gridfieldpartabname);
					//filter unique parent table
					$partablename =  $this->Crudmodel->filtervalue($elementpartable);
					//filter unique fields table
					$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
					$tableinfo = explode(',',$fildstable);
					$primaryid = $this->Crudmodel->datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable);
					//Now Update the attributevalueid
					if($_POST['attributevalueid'] != ''){
						$pex=explode('_',$_POST['productname']);
						$productname = $pex[0].'_'.$attributevaluearray[$rw];
						$att_array=array('attributevalues'=>$attributevaluearray[$rw],'productname'=>$productname);
						$this->db->where('productid',$primaryid);
						$this->db->update('product',$att_array);
					}
					//notification entry
					$empid = $this->Basefunctions->logemployeeid;
					$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
					$assignid = 1;
					if(isset($_POST['employeeid'])){
						$assignid = $_POST['employeeid'];
						if($assignid != '') {
							$assignid = $assignid;
						}
					} 
					//grid data insertion
					//primary key
					$defdataarr = $this->Crudmodel->defaultvalueget();
					$primaryname = $this->Crudmodel->primaryinfo($partablename);
					//productdetail insert
					$productuomgriddata = $griddatainfo[0];//uomgrid
					$productrolgriddata = $griddatainfo[1];//rolgrid
					$productstoragegriddata = $griddatainfo[2];//storagegrid
					if(count($productuomgriddata) > 0) {
						for($im=0;$im<count($productuomgriddata);$im++) {
							$array=array('uomfromid'=>$productuomgriddata[$im]['uomfromid'],'uomtoid'=>$productuomgriddata[$im]['uomtoid'],'conversionrate'=>$productuomgriddata[$im]['conversionrate'],'productid'=>$primaryid);
							$this->db->insert('productuomconversion',array_merge($array,$defdataarr));
						}
					}
					if(count($productrolgriddata) > 0) {
						for($im=0;$im<count($productrolgriddata);$im++) {
							$arraym=array('branchid'=>$productrolgriddata[$im]['branchid'],'reordertypeid'=>$productrolgriddata[$im]['reordertypeid'],'reorderquantity'=>$productrolgriddata[$im]['reorderquantity'],'minimumquantity'=>$productrolgriddata[$im]['minimumquantity'],'maximumquantity'=>$productrolgriddata[$im]['maximumquantity'],'productid'=>$primaryid);
							$this->db->insert('productrol',array_merge($arraym,$defdataarr));
						}
					}
					if(count($productstoragegriddata) > 0) {
						for($im=0;$im<count($productstoragegriddata);$im++) {
							$arrayy=array('branchid'=>$productstoragegriddata[$im]['storagebranchid'],'counterid'=>$productstoragegriddata[$im]['storageid'],'productid'=>$primaryid);
							$this->db->insert('productstorage',array_merge($arrayy,$defdataarr));
						}
					}
					$prodname = $_POST['productname'];
					if($assignid == '1'){
						$notimsg = $this->Basefunctions->notificationtemplatedatafetch(201,$primaryid,$partablename,3);
						if($notimsg != '') {
							$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,201);
						}
					} else {
						$notimsg = $this->Basefunctions->notificationtemplatedatafetch(201,$primaryid,$partablename,3);
						if($notimsg != '') {
							$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,201);
						}
					}
				}
				//Convert the product to variant convert
				//product status change to variant change
				//insert as deleted record-notify as varianted			
				$where_product=array('status'=>$this->Basefunctions->deletestatus,'lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid);
				//product
				$this->db->where('productid',$mainproductid);
				$this->db->update('product',$where_product);
				//productrol
				$this->db->where('productid',$mainproductid);
				$this->db->update('productrol',$where_product);
				//productuom
				$this->db->where('productid',$mainproductid);
				$this->db->update('productuomconversion',$where_product);
				//productstorage
				$this->db->where('productid',$mainproductid);
				$this->db->update('productstorage',$where_product);
				//
				$notimsg = $empname." "."Convert To Variant Product"." - ";
				$this->Basefunctions->notificationcontentadd($mainproductid,'Variant',$notimsg,1,9);
			}
		}
		{//workflow management for data update
			$this->Basefunctions->workflowmanageindataupdatemode($moduleid,$primaryid,$partablename,$condstatvals);
		}
		echo 'TRUE';
	}
	/**
	*Product Delete
	*/
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primarydataid'];
		$mainproductid = $id;
		$filename = $this->Basefunctions->generalinformaion('product','productname','productid',$id);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		//industrybased moduleid fetch
		$modid = $this->Basefunctions->getmoduleid($moduleid);
		{//workflow management -- for data delete
			$this->Basefunctions->workflowmanageindatadeletemode($modid,$id,$primaryname,$partabname);
		}
		$industryid = $this->Basefunctions->industryid;
		if(!isset($_POST['taxmasterid'])){
			$_POST['taxmasterid'] = 1;
			if($_POST['taxmasterid'] == ''){
			$_POST['taxmasterid'] = 1;
			}
		}
		$where_product=array('status'=>$this->Basefunctions->deletestatus,'lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid);
		$sndefault = $this->Basefunctions->singlefieldfetch('sndefault','productid','product',$id); //Check default value
		if($sndefault == 2) {
			echo 'sndefault';
		} else {
			if($ruleid == 1 || $ruleid == 2) {
				$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
				if($chek == 0) {
					echo 'Denied';
				} else {
					//productrol
					$this->db->where('productid',$mainproductid);
					$this->db->update('productrol',$where_product);
					
				if($industryid != 3) {
					//productuom
					$this->db->where('productid',$mainproductid);
					$this->db->update('productuomconversion',$where_product);
					//productstorage
					$this->db->where('productid',$mainproductid);
					$this->db->update('productstorage',$where_product);
				}	
					//notification log
					$notimsg = $this->Basefunctions->notificationtemplatedatafetch(9,$id,$partabname,4);
					if($notimsg != '') {
						$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,1,9);
					}
					$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
					echo 'TRUE';
				}
			} else {
				//productrol
				$this->db->where('productid',$mainproductid);
				$this->db->update('productrol',$where_product);
				if($industryid != 3) {
					//productuom
					$this->db->where('productid',$mainproductid);
					$this->db->update('productuomconversion',$where_product);
					//productstorage
					$this->db->where('productid',$mainproductid);
					$this->db->update('productstorage',$where_product);
				}
				//notification log
				$notimsg = $this->Basefunctions->notificationtemplatedatafetch(9,$id,$partabname,4);
				if($notimsg != '') {
					$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,1,9);
				}
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				echo 'TRUE';
			}
			//audit-log
			$user = $this->Basefunctions->username;
			$userid = $this->Basefunctions->logemployeeid;
			$productname = $this->Basefunctions->singlefieldfetch('productname','productid','product',$id); //Cash counter name
			$activity = ''.$user.' deleted Product - '.$productname.'';
			$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,90);
		}
	}
	//default setting data
	public function defaultsettings() {
		$defaultuom=0;
		$defaultbrand='';
		//primary uom
		$data=$this->db->select('uomid')
						->from('uom')
						->like('setdefault','Yes')
						->where('status',$this->Basefunctions->activestatus)
						->order_by('lastupdatedate','desc')
						->limit(1)
						->get();
		foreach($data->result() as $info){
			$defaultuom=$info->uomid;
		}
		//default brand
		$data=$this->db->select('brandid')
						->from('brand')
						->like('setdefault','Yes')
						->where('status',$this->Basefunctions->activestatus)
						->order_by('lastupdatedate','desc')
						->limit(1)
						->get();
		foreach($data->result() as $info){
			$defaultbrand=$info->brandid;
		}
		echo json_encode (array('defaultuom'=>$defaultuom,'defaultbrand'=>$defaultbrand));
	}
	//product uom json grid data on edit/detail view
	public function productuomlist($productid){
		$productuomdetail='';
		//primary uom
		$data=$this->db->select('productuomconversionid,uomfromid,uomtoid,conversionrate,a.uomname as fromuomname,b.uomname as touomname')
						->from('productuomconversion')
						->join('uom as a','a.uomid=productuomconversion.uomfromid')
						->join('uom as b','b.uomid=productuomconversion.uomtoid')
						->where('productuomconversion.status',$this->Basefunctions->activestatus)
						->where('productuomconversion.productid',$productid)
						->get();
			$j=0;		
			foreach($data->result() as $value)
			{	
				$productuomdetail->rows[$j]['id']=$value->productuomconversionid;
				$productuomdetail->rows[$j]['cell']=array(														
														$value->fromuomname,
														$value->uomfromid,
														$value->touomname,
														$value->uomtoid,
														$value->conversionrate,
														$value->productuomconversionid							
													);						
				$j++;		
			}
		echo  json_encode($productuomdetail);
	}
	//product ROL json grid data on edit/detail view
	public function productrollist($productid){
		$productroldetail='';
		//primary uom
		$data=$this->db->select('productrolid,productrol.branchid,productrol.productid,reordertype.reordertypename,branch.branchname,reorderquantity,minimumquantity,maximumquantity,productrol.reordertypeid,branchname,productrol.sizemasterid,sizemaster.sizemastername,productrol.fromweight,productrol.toweight,productrol.accountid,account.accountname')
						->from('productrol')
						->join('branch','branch.branchid=productrol.branchid')
						->join('sizemaster','sizemaster.sizemasterid=productrol.sizemasterid')
						->join('account','account.accountid=productrol.accountid')
						->join('reordertype','reordertype.reordertypeid=productrol.reordertypeid')
						->where('productrol.status',$this->Basefunctions->activestatus)
						->where('productrol.productid',$productid)
						->get();
			$j=0;		
		foreach($data->result() as $value) {	
			$productroldetail->rows[$j]['id']=$value->productrolid;
			$productroldetail->rows[$j]['cell']=array(										    $value->reordertypename,
													$value->reordertypeid,
													$value->minimumquantity,
													$value->reorderquantity,
													$value->maximumquantity,	
													$value->productrolid,
													$value->sizemastername,
													$value->sizemasterid,
													$value->fromweight,
													$value->toweight,
													$value->accountname,
													$value->accountid
													
												);						
			$j++;		
		}
		echo  json_encode($productroldetail);
	}
	//product uom json grid data on edit/detail view
	public function productstoragelist($productid){
		$productstoragedetail='';
		//primary uom
		$data=$this->db->select('productstorage.productstorageid,branch.branchname,productstorage.branchid,counter.counterid,counter.countername')
						->from('productstorage')
						->join('branch','branch.branchid=productstorage.branchid')
						->join('counter','counter.counterid=productstorage.counterid')
						->where('productstorage.status',$this->Basefunctions->activestatus)
						->where('productstorage.productid',$productid)
						->get();
			$j=1;		
			foreach($data->result() as $value) {	
				$productstoragedetail->rows[$j]['id']=$j;
				$productstoragedetail->rows[$j]['cell']=array(														
														$value->branchname,
														$value->branchid,
														$value->countername,
														$value->counterid,
														$value->productstorageid
													);						
				$j++;		
			}
		echo  json_encode($productstoragedetail);
	}
	public function productstoragecombination($storagebranchid,$storageid,$productid){
		$where=array('branchid'=>$storagebranchid,'storageid'=>$storageid,'productid'=>$productid,'status'=>$this->Basefunctions->activestatus);
		$datarows=$this->db->select('productstorageid')->from('productstorage')->where($where)->limit(1)->get();
		if($datarows->num_rows() == 0){
			return false;
		} else {
			foreach($datarows->result() as $inf){
				return $inf->productstorageid;
			}
		}	
	}
	/**
	* @desc  Return the Attribute Grid Column Based On Attribute Set
	* @param $attributesetassignid the sttributesetit
	*/
	public function productattributecolumn($attributesetassignid,$moduleid) {
		$datas = array('fieldname'=>array('id'),'fieldlabel'=>array('ID'),'colmodelname'=>array('id'),'colmodelindex'=>array('id'),'colmodelviewtype'=>array(1),'colmodeltype'=>array('text'),'colmodeluitype'=>array(2),'colmoduleid'=>array($moduleid),'attribute_ids'=>array('1'));
		$i=0;
		$j=0;
		$data=$this->db->select('attributesetassign.attributeid')
					->from('attributesetassign')
					->where('attributesetassignid',$attributesetassignid)
					->where('attributesetassign.status',$this->Basefunctions->activestatus)
					->order_by('attributesetassign.attributeid','asc')
					->get();
		if($data->num_rows() > 0){
			foreach($data->result() as $info){
				$array = str_split($info->attributeid);
				$att_count=$this->db->select('attributeid,attributename')
									->from('attribute')
									->where_in('attributeid',$array)
									->where('status',$this->Basefunctions->activestatus)
									->get();
				foreach($att_count->result() as $attrval){	
					$attribute[]=$attrval->attributename;
					$attribute_ids[]=$attrval->attributeid;
				}					
			}
			$count=count($attribute);
			$i = 0;
			for($m=0;$m<$count;$m++) {
				$datas['fieldname'][$i]=strtolower($attribute[$m]);
				$datas['fieldlabel'][$i]=$attribute[$m];
				$datas['colmodelname'][$i]=strtolower($attribute[$m]);
				$datas['colmodelindex'][$i]=strtolower($attribute[$m]);
				$datas['colmodelviewtype'][$i]=1;
				$datas['colmodeltype'][$i]='text';
				$datas['colmodeluitype'][$i]=2;
				$datas['colmoduleid'][$i]=$moduleid;
				$datas['attribute_ids'][$i]=$attribute_ids[$m];
				$i++;
			}
		}
		return $datas;
	}
	/**
	*@desc To Generate the combiantion of the Attribute value sets
	*/
	public function attributevaluecombination($attributesetassignid) {
		$data=$this->db->select('attributesetassign.attributeid')
		->from('attributesetassign')
		->where('attributesetassignid',$attributesetassignid)
		->where('attributesetassign.status',$this->Basefunctions->activestatus)
		->order_by('attributesetassign.attributeid','asc')
		->get();
		if($data->num_rows() > 0){
			foreach($data->result() as $attrinfo){
				$array = str_split($attrinfo->attributeid);
				$attrdata = $this->db->select('attribute.attributevaluename as groupvalue')
				->from('attribute')
				->where_in('attributeid',$array)
				->where('attribute.status',$this->Basefunctions->activestatus)
				->order_by('attribute.attributeid','asc')
				->get();
				if($attrdata->num_rows() > 0){
					foreach($attrdata->result() as $info){
						$combination[] = explode(',',$info->groupvalue);
						$arrayval[] = explode(',',$info->groupvalue);
						$arrayval= count($arrayval, COUNT_RECURSIVE) - count($arrayval);						
						for($i=1;$i <=$arrayval;$i++){
							$combinations[] = $i;
						}
						unset($arrayval);
						$combinationid[]=$combinations;
						unset($combinations);
					}
					$op=$this->combinations($combination, $i = 0);
					$opid=$this->combinations($combinationid, $i = 0);
					$opcount=count($op);
					for($j=0;$j < $opcount ;$j++){	
						$attribute_value_combination->rows[$j]['id'] = implode('_',$opid[$j]);
						$attribute_value_combination->rows[$j]['cell']=$op[$j];
					}	
					ob_start('ob_gzhandler');
					echo json_encode($attribute_value_combination);
					ob_end_flush();
				} else {
					echo json_encode(array('fail'=>"FAILED"));
				}
			}
		}
	}
	/**
	*@desc This function creates the possible combination of attribute value (EX:3 x 3 x 3 =27 combination)
	*@param $arrays The comibation array
	*/
	public function combinations($arrays, $i = 0) {	
		if (!isset($arrays[$i])) {
			return array();
		}	
		if (($i == count($arrays) - 1) and (count($arrays) != 1)) { //to terminate
			return $arrays[$i];	
		}	
		if(count($arrays) == 1) {	//only for single attribute for product	
			$singlecombination = array();	
			for($kp=0 ;$kp < count($arrays[0]);$kp++) {
				$singlecombination []= array ($arrays[0][$kp]);
			}	
			return $singlecombination;
		}	
		$tmp = $this->combinations($arrays, $i + 1);
		$result = array();
		// concat each array from tmp with each element from $arrays[$i]
		foreach ($arrays[$i] as $v) {	
			foreach ($tmp as $t) {	
				$result[] = is_array($t) ? array_merge(array($v), $t) : array($v, $t);
			}
		}
		return $result;
	}
	/**
	*-Get the product attribtue values
	*
	*/
	public function getproductattributevalue($productid){
		$data=$this->db->select('attributevalues')
						->from('product')
						->where('productid',$productid)
						->limit(1)
						->get()
						->result();
		foreach($data as $info){
			$ret=array('attributevalues'=>$info->attributevalues);
		}
		echo json_encode($ret);
	}
	//product owner detail fetch
	public function productownersetmodel() {
		$userid = $this->Basefunctions->userid;
		echo json_encode($userid);
	}
	//product storage - in stock fetch
	public function productstoragefetchfunmodel($productid){
		//stock management stock
		$smstock = $this->stockmangementstockget($productid);
		//invoice delivered total stock 
		$invoicestock = $this->invoicedeliverdstockget($productid);
		//product instock 
		$productinstock = $this->Basefunctions->generalinformaion('product','instock','productid',$productid);
		//total stock
		$stock = $smstock - $invoicestock;
		$totalstock = $stock + $productinstock;
		return $totalstock;
	}
	//fetch total stock of stock management
	public function stockmangementstockget($productid) {
		$this->db->select_sum('quantity');
		$this->db->from('stockmanagement');
		$this->db->where('stockmanagement.productid',$productid);
		$this->db->group_by('stockmanagement.productid');
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$instock = $row->quantity;
			}
		} else {
			$instock = 0;
		}
		return $instock;
	}
	//invoice delivered stock information fetch
	public function invoicedeliverdstockget($productid) {
		$this->db->select_sum('quantity');
		$this->db->from('invoicedetail');
		$this->db->join('invoice','invoice.invoiceid=invoicedetail.invoiceid');
		$this->db->where('invoicedetail.productid',$productid);
		$this->db->where('invoice.status',1);
		$this->db->where('invoice.crmstatusid',38);
		$this->db->where('invoicedetail.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$invoicestock = $row->quantity;
			}
		} else {
			$invoicestock = 0;
		}
		return $invoicestock;
	}
	//product order and demand fetch
	public function productorderanddemandfetchfunmodel() {
		$productid = $_GET['productid'];
		//product based order quantity
		$stockorder = $this->productbasedstockorderfetch($productid);
		if($stockorder == null) {
			$stockorder = 0;
		}
		//product based order in demand
		$stockindemand = $this->productbasedstockdemand($productid);
		if($stockindemand == null) {
			$stockindemand = 0;
		}
		$productstock = array('quaninorder'=>$stockorder,'quanindemand'=>$stockindemand);
		echo json_encode($productstock);
	}
	//product - stock order (get from purchase order)
	public function productbasedstockorderfetch($productid) {
		$this->db->select_sum('quantity');
		$this->db->from('purchaseorderdetail');
		$this->db->join('purchaseorder','purchaseorder.purchaseorderid=purchaseorderdetail.purchaseorderid');
		$this->db->where('purchaseorderdetail.productid',$productid);
		$this->db->where('purchaseorder.status',1);
		$this->db->where('purchaseorder.crmstatusid',48);
		$this->db->where('purchaseorderdetail.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$stockorder = $row->quantity;
			}
		} else {
			$stockorder = 0;
		}
		return $stockorder;
	}
	//product based order in demand (get from invoice)
	public function productbasedstockdemand($productid) {
		$this->db->select_sum('quantity');
		$this->db->from('salesorderdetail');
		$this->db->join('salesorder','salesorder.salesorderid=salesorderdetail.salesorderid');
		$this->db->where('salesorderdetail.productid',$productid);
		$this->db->where('salesorder.status',1);
		$this->db->where('salesorder.crmstatusid',42);
		$this->db->where('salesorderdetail.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$stockdemand = $row->quantity;
			}
		} else {
			$stockdemand = 0;
		}
		return $stockdemand;
	}
	
	public function getproductbasedpurity($productid)
	{
		$this->db->select('product.purityid as purityid');
		$this->db->from('product');
		$this->db->where_in('product.productid',$productid);
		$this->db->where('product.industryid',$this->Basefunctions->industryid);
		$this->db->where('product.status',$this->Basefunctions->activestatus);
		$result=$this->db->get()->row();
		return $result->purityid;
	}	
	//parent account name fetch
	public function parentproductnamefetchmodel() {
		$id=$_GET['id'];
		$data=$this->db->select('category.categoryid,category.categoryname')
		->from('product')
		->join('category','category.categoryid=product.categoryid')
		->where('product.productid',$id)
		->where('category.status',1)
		->get();
		if($data->num_rows() >0) {
			foreach($data->result() as $in)	{
				$parentcategoryid=$in->categoryid;
				$categoryname=$in->categoryname;
			}
			$a = array('parentcategoryid'=>$parentcategoryid,'categoryname'=>$categoryname);
		} else {
			$a = array('parentcategoryid'=>'0','categoryname'=>'');
		}
		echo json_encode($a);
	}
	// check loose product
	public function checkeditlooseproduct() {
		    $estimatearray['rows'] = 0;
			$productid = $_POST['productid'];
			$this->db->select('productid');
			$this->db->from('product');
			$this->db->where('loosestone',1);
			$this->db->where('productid',$productid);
			$this->db->where('status',$this->Basefunctions->activestatus);
			$data = $this->db->get();
			$estimatearray['rows'] = $data->num_rows();
			echo json_encode($estimatearray); 			
	}
}