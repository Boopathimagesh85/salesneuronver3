<div class="large-12 columns paddingzero formheader">
	<?php
		$this->load->view('Base/mainviewaddformheader');
	?>
</div>
<div class="large-12 columns addformunderheader">&nbsp;</div>
<div class="large-12 columns scrollbarclass addformcontainer tabtouchaddcontainer">
	<div class="row mblhidedisplay">&nbsp;</div>
	<form method="POST" name="dataaddform" class="" action ="" id="dataaddform">
		<span id="formaddwizard" class="validationEngineContainer" >
			<span id="formeditwizard" class="validationEngineContainer">
				<?php
                    $this->load->view('Base/formfieldgeneration');
                    //function call for form fields generation
					formfieldstemplategenerator($modtabgrp,'');
					$defaultrecordview = $this->Basefunctions->get_company_settings('mainviewdefaultview');
					echo hidden('mainviewdefaultview',$defaultrecordview);
                ?>
			</span>
		</span>
		<!--hidden values-->
		<?php
			echo hidden('primarydataid','');
			echo hidden('sortorder','');
			echo hidden('sortcolumn','');
			$value = 'productuomconversion,productrol,productstorage';
			echo hidden('resctable',$value);
			$industryid = $this->Basefunctions->industryid;
			echo hidden('gstapplicableid',$gstapplicable);
			if($industryid == 3) {
				echo hidden('chargedataid',$mainlicense['chargedataid']);
				echo hidden('purchasechargedataid',$mainlicense['purchasechargedataid']);
				echo hidden('counterdataid',$mainlicense['counter']);
				echo hidden('defaultcategoryid',$categoryid);
			    echo hidden('defaultcategoryname',$categoryname);
			    echo hidden('taxmasterhideshow',$mainlicense['taxapplicable']);
				echo hidden('businesschargeid',$mainlicense['businesschargeid']);
				echo hidden('businesspurchasechargeid',$mainlicense['businesspurchasechargeid']);
				echo hidden('stonestatus',$mainlicense['stone']);
				echo hidden('productroltypeid',$mainlicense['productroltypeid']);
				echo hidden('sizestatus',$mainlicense['sizestatus']);
				echo hidden('barcodeoption',$mainlicense['barcodeoption']);
			} else {
				
			}
			//hidden text box view columns field module ids
			$modid = $this->Basefunctions->getmoduleid($filedmodids);
			echo hidden('viewfieldids',$modid);
			echo hidden('branchidval',$branchid);
		?>
	</form>
</div>