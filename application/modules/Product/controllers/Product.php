<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Product extends MX_Controller{
    //Constructor Function To Load Models
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Product/Productmodel');	
	}
	//Default View 
	public function index()	{
		$moduleid = array(9,89,90,98);
		sessionchecker($moduleid);
		//action
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid); 
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$industryid = $this->Basefunctions->industryid;
		$data['branchid'] = $this->Basefunctions->branchid;
		$data['gstapplicable']=$this->Basefunctions->get_company_settings('gstapplicable');
		if($industryid == 3) {
			$data['mainlicense'] = array(
				'counter'=>$this->Basefunctions->get_company_settings('counter'),
				'chargedataid'=>$this->Basefunctions->get_company_settings('chargeid'),
				'purchasechargedataid'=>$this->Basefunctions->get_company_settings('purchasechargeid'),
				'taxapplicable'=>$this->Basefunctions->get_company_settings('taxapplicable'),
				'businesschargeid'=>$this->Basefunctions->get_company_settings('businesschargeid'),
				'businesspurchasechargeid'=>$this->Basefunctions->get_company_settings('businesspurchasechargeid'),
				'stone'=>$this->Basefunctions->get_company_settings('stone'),
				'productroltypeid'=>$this->Basefunctions->get_company_settings('productroltypeid'),
				'sizestatus'=>$this->Basefunctions->get_company_settings('sizestatus'),
				'barcodeoption'=>$this->Basefunctions->get_company_settings('barcodeoption')
			); 
			$data['categoryid'] = $this->Basefunctions->get_company_settings('categoryid');
			$data['categoryname'] = $this->Basefunctions->singlefieldfetch('categoryname','categoryid','category',$data['categoryid']);
		}
		$this->load->view('Product/productview',$data); 
	}
	//create company
	public function newdatacreate() {  
    	$this->Productmodel->newdatacreatemodel();
    }
	//information fetch
	public function fetchformdataeditdetails() {
		$moduleid = array(9,89,90,98);
		$this->Productmodel->informationfetchmodel($moduleid);
	}
	//update company
    public function datainformationupdate() {
        $this->Productmodel->datainformationupdatemodel();
    }
	//delete company
    public function deleteinformationdata() {
        $moduleid = array(9,89,90,98);
		$this->Productmodel->deleteoldinformation($moduleid);
    }
	//default setting data
	public function defaultsettings() {
		$this->Productmodel->defaultsettings();
	}
	//load product uom data
	public function productuom() {
		$pid=$_GET['productid'];
		$this->Productmodel->productuomlist($pid);
	}
	//load product uom data
	public function productrol() {
		$pid=$_GET['productid'];
		$this->Productmodel->productrollist($pid);
	}
	//load product uom data
	public function productstorage() {
		$pid=$_GET['productid'];
		$this->Productmodel->productstoragelist($pid);
	}
	//Load the attribute column based on attribute set
	public function productattributecolumn() {	
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modname = $_GET['modulename'];
		$moduleid = $_GET['moduleid'];
		$checkbox = $_GET['checkbox'];
		$attributesetassignid = trim($_GET['attributesetassignid']);
		$colinfo = $this->Productmodel->productattributecolumn($attributesetassignid,$moduleid);	
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height,$checkbox);
		} else {
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height,$checkbox);
		}
		echo json_encode($datas);
	}
	/**	*@desc To Generate the combiantion of the Attribute value sets	*/
	public function attributevaluecombination() {
		if($_GET['attributesetassignid'] > 0){
			$this->Productmodel->attributevaluecombination(trim($_GET['attributesetassignid']));
		} else {
			echo json_encode(array('fail'=>"FAILED"));
		}
	}
	/**	*Get the multiselect attribute value	*/
	public function getproductattributevalue(){		
		$this->Productmodel->getproductattributevalue(trim($_GET['productid']));
	}
	//default product owner set
	public function productownerset() {
		$this->Productmodel->productownersetmodel();	
	}
	//product storage fetch - instock
	public function productstoragefetchfun() {
		$productid = $_GET['productid'];
		$totalstock = $this->Productmodel->productstoragefetchfunmodel($productid);
		echo json_encode($totalstock);
	}
	//product demand and order fetch
	public function productorderanddemandfetchfun() {
		$this->Productmodel->productorderanddemandfetchfunmodel();
	}
	public function getmoduleid(){
    	$modulearray = array(9,89,90,98);
    	$data=$this->db->select("moduleid")->from("module")->where_in('moduleid',$modulearray)->where('status',1)->limit(1)->get();
    	foreach($data->result() as $info){
    		$moduleid = $info->moduleid;
    	}
    	echo $moduleid;
    }
	//parent account name fetch
    public function parentproductnamefetch() {
    	$this->Productmodel->parentproductnamefetchmodel();
    }
	public function checkitem() {
    	$productid = $_GET['productid'];
    	$moduleid = $_GET['moduleid'];
		if($moduleid == 50){ // stock entry
			$table = 'itemtag';
		}else if($moduleid == 134){ // sizemaster
			$table = 'sizemaster';
		}
		$checkproduct = $this->Basefunctions->generalinformaion($table,'productid','productid',$productid);
		echo $checkproduct;
    }
	// check loose stone product
	public function checkeditlooseproduct() {
		$this->Productmodel->checkeditlooseproduct();
	}
}