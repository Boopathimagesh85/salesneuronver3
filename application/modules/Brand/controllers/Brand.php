<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Brand extends MX_Controller{
    public function __construct(){
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Brand/Brandmodel');	
    }
    //first basic hitting view
    public function index() {
    	$moduleid = array(11);
    	sessionchecker($moduleid);
		//action
		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(11);
		$viewmoduleid = array(11);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Brand/brandview',$data);
	}
	//create itemcounter
	public function newdatacreate() {  
    	$this->Brandmodel->newdatacreatemodel();
    }
	//information fetchitemcounter
	public function fetchformdataeditdetails() {
		$moduleid = 11;
		$this->Brandmodel->informationfetchmodel($moduleid);
	}
	//update itemcounter
    public function datainformationupdate() {
        $this->Brandmodel->datainformationupdatemodel();
    }   
	//delete itemcounter
    public function deleteinformationdata() {
        $moduleid = 11;
		$this->Brandmodel->deleteoldinformation($moduleid);
    } 
}