<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Brandmodel extends CI_Model{
    public function __construct(){
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//itemcounter create
	public function newdatacreatemodel(){
		//table and fields information
		$formfieldsname = explode(',',$_POST['brandelementsname']);
		$formfieldstable = explode(',',$_POST['brandelementstable']);
		$formfieldscolmname = explode(',',$_POST['brandelementscolmn']);
		$elementpartable = explode(',',$_POST['brandelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
		$setdefault = $_POST['setdefault'];
		$primaryid = $this->db->insert_id();
		$this->setdefaultupdate($setdefault,$primaryid);
		echo 'TRUE';
	}	
	//Retrive itemcounter data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['brandelementsname']);
		$formfieldstable = explode(',',$_GET['brandelementstable']);
		$formfieldscolmname = explode(',',$_GET['brandelementscolmn']);
		$elementpartable = explode(',',$_GET['brandelementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['brandprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	//itemcounter update
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['brandelementsname']);
		$formfieldstable = explode(',',$_POST['brandelementstable']);
		$formfieldscolmname = explode(',',$_POST['brandelementscolmn']);
		$elementpartable = explode(',',$_POST['brandelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['brandprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		$setdefault = $_POST['setdefault'];
		$this->setdefaultupdate($setdefault,$primaryid);
		echo $result;
	}
	//set default update
	public function setdefaultupdate($setdefault,$primaryid) {
		if($setdefault != 'No') {
			$updatearray = array('setdefault'=>'No');
			$this->db->where_not_in('brand.brandid',$primaryid);
			$this->db->update('brand',$updatearray);
		}
	}
	//itemcounter delete
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['brandelementstable']);
		$parenttable = explode(',',$_GET['brandparenttable']);
		$id = $_GET['brandprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				echo "TRUE";
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			echo "TRUE";
		}
	} 	
}