<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('Base/headerfiles'); ?>
	<!-- js Files -->	
	<script src="<?php echo base_url();?>js/Brand/brand.js" type="text/javascript"></script>
</head>
<body>
<?php
	$dataset['gridenable'] = "1"; //0-no  1-yes
	$dataset['griddisplayid'] = "brandaddformview"; //add form-div id
	$dataset['maingridtitle'] = $gridtitle['title'];   // form header
	$dataset['gridtitle'] = $gridtitle['title'];
	$dataset['titleicon'] = $gridtitle['titleicon'];  //grid header
	$dataset['spanattr'] = array();
	$dataset['gridtableid'] = "brandaddgrid"; //grid id
	$dataset['griddivid'] = "brandaddgridnav"; //grid pagination
	$this->load->view('Brand/brandform',$dataset);
	$device = $this->Basefunctions->deviceinfo();
	if($device=='phone') {
		$this->load->view('Base/overlaymobile');
	} else {
		$this->load->view('Base/overlay');
	};
?>	
</body>
<?php $this->load->view('Base/bottomscript'); ?>
</html>