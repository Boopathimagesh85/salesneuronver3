<!DOCTYPE html>
<html lang="en">
<head>
	<?php 
		$this->load->view('Base/headerfiles'); 
		$CI =& get_instance();
		$appdateformat = $CI->Basefunctions->appdateformatfetch();
	?>
	<!-- //For Left Padding Grid data -->
<style type="text/css">
#auditloggridwidth .ui-jqgrid tr.ui-row-ltr td {
	text-align: left;
	padding-left:1rem !important;
}
.desktop .actionmenucontainer .headeraction-menu .toggle-view .tabaction {
position:relative;
top: -5px !important;
}
.forgetinggridname2 {
padding-right:0px !important;
}
.filterbodyborderstyle {
	height: 77vh !important;
}
.paging-box {
    left: -40% !important;
}
</style>
</head>
<body>
	<div class="row" style="max-width:100%">
		<div class="off-canvas-wrap" data-offcanvas>
			<div class="inner-wrap">
				<div class="gridviewdivforsh" id="auditlogheaderview">
					<div class="large-12 columns paddingzero viewheader" style="top: 10px !important;">
						<?php
							$dataset['gridtitle'] = $gridtitle['title'];
							$dataset['titleicon'] = $gridtitle['titleicon'];
							$dataset['formtype'] = 'logaudmodform';
							$dataset['gridid'] = 'auditloggrid';
							$dataset['gridwidth'] = 'auditloggridwidth';
							$dataset['gridfooter'] = 'auditloggridfooter';
							$this->load->view('Base/mainviewheader',$dataset);
							$this->load->view('Base/singlemainviewformwithgrid',$dataset);
						?>
						<div class="large-3 medium-3 columns paddingzero borderstyle" style="padding-top:6px !important;">
							<div class="searchfilteroverlay filterbox ui-draggable borderstylefilter " style=" touch-action: none; -moz-user-select: none;">
							<form method="POST" name="auditlogmaintainform" class="" id="auditlogmaintainform">
								<div id="auditlogvalidation" class="validationEngineContainer">
									<div class="noticombowidget-box filterheaderborderstyle">
									<div class="large-12 medium-12 columns end noticomboheader activeheader filertabbgcolor" id="filterview" style="padding-top:0.4rem!important;padding-left:0.7rem !important;" data-griddataid="accountingcategoryaddgrid">
									<span id="fiteractive" class="tab-titlemini sidebariconsmini active filtermini"><i title="" class="material-icons" style="top:5px;position:relative;">search</i><span style="left:5px;position:relative;font-size:0.9rem;">Filter Panel</span></span>
									</div>
									<div class="large-12 medium-12 small-12 columns end filterheight filterbodyborderstyle" style="left: -4px;width: 103%;">
										<div class="static-field large-12 columns viewcondclear" id="">
											<label>From Date</label>
											<input type="text" class="fordatepicicon" data-dateformater="<?php echo $appdateformat; ?>" id="fromdate" name="fromdate" tabindex="101" readonly>
										</div>
										<div class="static-field large-12 columns viewcondclear" id="">
											<label>To Date</label>
											<input type="text" class="fordatepicicon" data-dateformater="<?php echo $appdateformat; ?>" id="todate" name="todate" tabindex="102" readonly>
										</div>
										<div class="static-field large-12 columns">
											<label>Type</label>
												<select class="chzn-select" id="auditlogaction" name="auditlogaction" data-placeholder="ALL" tabindex="103" style="color: #394b53 !important;">
												<option value="1">All Types</option>
												<option value="Added">Added</option>
												<option value="Updated">Updated</option>
												<option value="Deleted">Deleted</option>
											</select>
										</div>
										<div class="static-field large-12 columns">
											<label>User</label>
											<select class="chzn-select" id="auditloguser" name="auditloguser" data-placeholder="ALL" tabindex="104">
												<option value="">All</option>
												<?php foreach($userid as $key):?>
													<option value="<?php echo $key->employeeid;?>">
													<?php echo $key->employeename;?></option>
												<?php endforeach;?>
											</select>
										</div>
										<div class="static-field large-12 columns">
											<label>Module</label>
											<select class="chzn-select" id="auditmodule" name="auditmodule" class="" data-placeholder="ALL" tabindex="105">
												<option value="">All</option>
												<?php foreach($moduleid as $key):?>
													<option value="<?php echo $key->moduleid;?>"><?php echo $key->modulename;?></option>
												<?php endforeach;?>
											</select>
										</div>
										<div class="large-12 columns submitkeyboard sectionalertbuttonarea" style="text-align:center;">
										<input id="auditlogfiltersubbtn" class="alertbtn addkeyboard" type="button" value="Submit" name="auditlogfiltersubbtn" tabindex="106">
									</div>
									</div>							
										</div>
								</div>
							</form>
						
							</div>
						</div>
					</div>						
				</div>
			</div>
		</div>
	<?php
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$this->load->view('Base/overlaymobile');
		} else {
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
	?>
</body>
	<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js Files -->	
	<script src="<?php echo base_url();?>js/Auditlog/auditlog.js" type="text/javascript"></script>	
</html>