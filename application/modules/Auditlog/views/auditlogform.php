<!-- Form Header -->
<?php
	$CI =& get_instance();
	$appdateformat = $CI->Basefunctions->appdateformatfetch();
?>
<div class="large-12 columns addformunderheader">&nbsp;</div>
<div class="large-12 columns addformcontainer actionbarformcontainer">
	<div class="closed effectbox-overlay effectbox-default" id="auditlogsectionoverlay">
	<div class="large-12 columns mblnopadding">
	<div class="row sectiontoppadding">&nbsp;</div>
	<form method="POST" name="auditlogmaintainform" class="" id="auditlogmaintainform">
		<div id="auditlogvalidation" class="validationEngineContainer">
			<div class="large-4 large-offset-4 columns paddingbtm" id="leftpanearea">	
				<div class="large-12 columns" style="padding-left: 0 !important; padding-right: 0 !important;background: #617d8a ">
					<div class="large-12 column paddingzero" id="filterlistarea" style="">
						<div class="large-12 columns sectionheaderformcaptionstyle" style="">
							<div class="large-12 column paddingzero"> Filters </div>
						</div>
						<div class="large-12 columns sectionpanel">
							<div class="static-field large-12 columns viewcondclear" id="">
								<label>From Date</label>
								<input type="text" class="fordatepicicon" data-dateformater="<?php echo $appdateformat; ?>" id="fromdate" name="fromdate" tabindex="101" readonly>
							</div>
							<div class="static-field large-12 columns viewcondclear" id="">
								<label>To Date</label>
								<input type="text" class="fordatepicicon" data-dateformater="<?php echo $appdateformat; ?>" id="todate" name="todate" tabindex="102" readonly>
							</div>
							<div class="static-field large-12 columns">
								<label>Type</label>
									<select class="chzn-select" id="auditlogaction" name="auditlogaction" data-placeholder="ALL" tabindex="103">
									<option value="1">All Types</option>
									<option value="Added">Added</option>
									<option value="Updated">Updated</option>
									<option value="Deleted">Deleted</option>
								</select>
							</div>
							<div class="static-field large-12 columns">
								<label>User</label>
								<select class="chzn-select" id="auditloguser" name="auditloguser" data-placeholder="ALL" tabindex="104">
									<option value="">All</option>
									<?php foreach($userid as $key):?>
										<option value="<?php echo $key->employeeid;?>">
										<?php echo $key->employeename;?></option>
									<?php endforeach;?>
								</select>
							</div>
							<div class="static-field large-12 columns">
								<label>Module</label>
								<select class="chzn-select" id="auditmodule" name="auditmodule" class="" data-placeholder="ALL" tabindex="105">
									<option value="">All</option>
									<?php foreach($moduleid as $key):?>
										<option value="<?php echo $key->moduleid;?>"><?php echo $key->modulename;?></option>
									<?php endforeach;?>
								</select>
							</div>
						</div>
						<div class="divider large-12 columns"></div>
						<div class="large-12 columns submitkeyboard sectionalertbuttonarea" style="text-align:right">
							<input id="auditlogfiltersubbtn" class="alertbtn addkeyboard" type="button" value="Submit" name="auditlogfiltersubbtn" tabindex="106">
							<input id="auditlogfilterclose" class="alertbtn addsectionclose" type="button" value="Cancel" name="auditlogfilterclose" tabindex="107">
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
	</div>
	</div>
</div>

<style type="text/css">
.large-12 .columns .paddingzero .gridview-container {
	top:-1px !important;
}
</style>