<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Auditlog extends MX_Controller{
    public function __construct() {
        parent::__construct();
		$this->load->model('Auditlog/Auditlogmodel');
		$this->load->model('Base/Basefunctions');
		$this->load->helper('formbuild');
    }
    public  $notificationaction=array('added','updated','deleted','active','cancelled','lost','convert','stopped','import','duplicate','export','massdelete','massupdate','conversation','invite');
    public	$notificationicons=array('fa fa-plus','fa fa-pencil-square-o','fa fa-trash-o','fa fa-asterisk','fa fa-times-circle','fa fa-thumbs-o-down','fa fa-hand-o-right','fa fa-power-off','fa fa-upload','fa fa-search-plus','fa fa-download','fa fa-trash','fa fa-database','fa fa-comment-o','fa fa-star');
    public	$notificationlink=array('1','1','2','1','5','5','1','5','0','0','0','0','0','3','4');
    public $employeeurl='Employee';
    public $employeemodule=4;
    //first basic hitting view
    public function index() {
    	$moduleid = array(260);
    	sessionchecker($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['userid'] = $this->Basefunctions->simpledropdown('employee','employeeid,employeename','employeename');
		$data['moduleid'] = $this->Auditlogmodel->simpledropdownwithcond('module','moduleid,modulename','moduleid');
		$this->load->view('Auditlog/auditlogview',$data);
	}
	public function auditloginformationshow() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$action = $_GET['action'];
		$userid = $_GET['userid'];
		$todate= $_GET['todate'];
		$fromdate= $_GET['fromdate'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'notificationlog.notificationlogid') : 'notificationlog.notificationlogid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('time','modulename','notificationmessage','createdate'),'colmodelindex'=>array('time','module.modulename','notificationlog.notificationmessage','createdate'),'coltablename'=>array('notificationlog','module','notificationlog','notificationlog'),'uitype'=>array('2','2','2','2'),'colname'=>array('Time','Module Name','Messages','createdate'),'colsize'=>array('10','30','200','20'));
		$result=$this->Auditlogmodel->auditlogmaingriddatafetch($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$modid,$action,$userid,$fromdate,$todate);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$data=array();
		$i=0;
		$message='';
		$identity="";
		$position=0;
		$space=' ';
		$baseurl=$this->config->item('base_url');
		$employeeurl=$baseurl.$this->employeeurl;
		foreach($result->result() as $row) {
			//identity
			$identity=explode('-',$row->notificationmessage);
			if(isset($identity[1])) {
				$identity=trim($identity[1]);
			} else {
				$identity='';
			}
			//operation
			$locate=array_search(strtolower($row->notificationlogtypeid),$this->notificationaction);
			if($locate >= 0) {
				$position= $this->notificationlink[$locate];
			}
			//
			$date=array();
			$date=explode(' ',$row->createdate);
			$date = $this->Auditlogmodel->customerdateset($date[0]);
			//generate hyper link for messages
			$messages="<a class='auditlink' data-rowid='".$row->allempid."' data-moduleid='4' href='".$employeeurl."'>".$row->allemp."</a>";
			if($position == 1) {
				$message = $row->notificationmessage;
				$messages = $this->Auditlogmodel->smsprintfileinfofetch($message,$baseurl,$row->modulelink,$row->modulemastertable,$row->modulename,$row->moduleid,$row->commonid);
			} else if($position == 5) {
				$messages.="".$space."".$row->notificationlogtypeid." a ".$row->modulename." named <span class='auditlink' data-rowid='".$row->commonid."' data-moduleid='".$row->moduleid."' data-mastertable=".$row->modulemastertable." data-link='".$baseurl."".$row->modulelink."' style='color:#686900'>".$identity."".$space."</span>";
			} else if($position == 3) {
				$messages.="".$space." Mentioned U in a Conversation";
			} else if($position == 4) {
				$messages.="".$space."".$row->notificationlogtypeid." on ".$row->modulename." named <span class='auditlink' data-rowid='".$row->commonid."' data-moduleid='".$row->moduleid."' data-mastertable=".$row->modulemastertable." data-link='".$baseurl."".$row->modulelink."' style='color:#686900'>".$identity."".$space."</span>";
			} else {
				$message = $row->notificationmessage;
				$messages = $this->Auditlogmodel->smsprintfileinfofetch($message,$baseurl,$row->modulelink,$row->modulemastertable,$row->modulename,$row->moduleid,$row->commonid);
			}
			$data[$i]=array('id'=>$row->notificationlogid,'createdate'=>$date,$row->time,$row->modulename,$messages,$row->statusname);
			$i++;
		}
		$finalresult=array($data,$page,'jsongroup','createdate');
		$device = $this->Basefunctions->deviceinfo();
		//if($device=='phone') {
			//$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Audit Log',$width,$height);
	//	} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Audit Log',$width,$height);
		//}
		echo json_encode($datas);
	}
	//excel report
	public function exceldataexport() {
		$this->Auditlogmodel->exceldataexportmodel();
	}
}