<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Auditlogmodel extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
	public function simpledropdownwithcond($table,$dname,$did) {
		$i=0;
		$whdata = '260';
		$whfield = 'moduleprivilegeid';
		$this->db->select("$dname".',menucategory.menucategoryname');
		$this->db->from($table);
		$this->db->join('menucategory','menucategory.menucategoryid=module.menucategoryid');
		$this->db->where("FIND_IN_SET('$whdata',$whfield) >", 0);
		$this->db->where($table.'.status',1);
		$this->db->order_by('menucategory.menucategoryid',"ASC");
		$this->db->order_by('module.moduleid',"ASC");
		$result = $this->db->get();
		return $result->result();
    }
	//audit main grid view
	public function auditlogmaingriddatafetch($tablename,$sortcol,$sortord,$pagenum,$rowscount,$moduleid,$action,$userid,$fromdate,$todate) {
		$format = $this->Basefunctions->phpmysqlappdateformatfetch();
		$fromdate = $fromdate!='' ? date('Y-m-d',strtotime($fromdate)) : '';
		$todate = $todate!='' ? date('Y-m-d',strtotime($todate)) : '';
		$dataset = 'SQL_CALC_FOUND_ROWS notificationlog.notificationlogid,notificationlog.notificationmessage,notificationlog.notificationlogtypeid,module.modulename,module.modulemastertable,employee.employeename as allemp,status.statusname,DATE_FORMAT(notificationlog.createdate,"'.$format['mysqlformat'].'") as createdate,TIME_FORMAT(notificationlog.createdate,"%H:%i") as time,module.modulelink,b.employeename as inemp,notificationlog.commonid,employee.employeeid as allempid,b.employeeid as inempid,notificationlog.moduleid';
		$join=' LEFT OUTER JOIN employee ON employee.employeeid = notificationlog.createuserid';
		$join.=' LEFT OUTER JOIN employee as b ON b.employeeid = notificationlog.employeeid';
		$join.=' LEFT OUTER JOIN module ON module.moduleid = notificationlog.moduleid';
		$join.=' LEFT OUTER JOIN status ON status.status=notificationlog.status';
		$status = $tablename.'.status IN (1,2)';
		$where = 'notificationlog.moduleid NOT IN (40)';
		if($action != 1) {
			$where .= ' AND notificationlog.notificationlogtypeid="'.$action.'"';
		}
		if($userid != 1 && $userid != "") {
			$where .= ' AND notificationlog.createuserid='.$userid;
		}
		if($moduleid != 1 && $moduleid != "") {
			$where .= ' AND notificationlog.moduleid='.$moduleid;
		}
		if($fromdate != "" && $todate != "") {
			if($fromdate == $todate) {
				$where .= ' AND notificationlog.createdate LIKE "%'.$fromdate.'%"';
			} else {
				$where .= ' AND DATE(notificationlog.createdate) BETWEEN "'.$fromdate.'" AND "'.$todate.'"';
			}
		}
		/* query */
		$pagenum -= 1;
    	$start = $pagenum * $rowscount;
		$result = $this->db->query('select '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$where.' AND '.$status.' ORDER BY notificationlog.createdate DESC,'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		return $result;
	}
	//audit log excel data export 
	public function auditlogexportdata($action,$userid,$moduleid,$fromdate,$todate) {
		$fromdate = $fromdate!='' ? date('Y-m-d',strtotime($fromdate)) : '';
		$todate = $todate!='' ? date('Y-m-d',strtotime($todate)) : '';
		$format = $this->Basefunctions->phpmysqlappdateformatfetch();
		$this->db->select('SQL_CALC_FOUND_ROWS  notificationlog.notificationmessage,notificationlog.notificationlogtypeid,notificationlog.notificationlogid,module.modulename,module.modulemastertable,employee.employeename as allemp,status.statusname,DATE_FORMAT(notificationlog.createdate,"'.$format['mysqlformat'].' %H:%i:%s") as createdate,b.employeename as inemp,notificationlog.commonid,employee.employeeid as allempid,b.employeeid as inempid,notificationlog.moduleid,module.modulelink',false);
		$this->db->from('notificationlog');
		$this->db->join('employee','employee.employeeid = notificationlog.createuserid');#from employee
		$this->db->join('employee as b','b.employeeid = notificationlog.employeeid');#to employee
		$this->db->join('module','module.moduleid = notificationlog.moduleid');
		$this->db->join('status','status.status = notificationlog.status');
		$this->db->where_not_in('notificationlog.moduleid',array(40));
		if($action != 1) {
			$this->db->where_in('notificationlog.notificationlogtypeid',$action);		
		}
		if($userid != 1 && $userid != '') {
			$this->db->where_in('notificationlog.employeeid',$userid);		
			$this->db->where_in('notificationlog.createuserid',$userid);		
		}
		if($moduleid != 1 && $moduleid != '') {
			$this->db->where_in('notificationlog.moduleid',$moduleid);		
		}
		if($fromdate != "" && $todate != "") {
			$where = 'DATE(notificationlog.createdate) BETWEEN "'.$fromdate.'" AND "'.$todate.'"';
			$this->db->where($where);
		}
		$this->db->where_in('notificationlog.status',array(1,2));
		$this->db->order_by('notificationlog.notificationlogid','desc');
		$this->db->order_by('notificationlog.createdate','desc');
		$result=$this->db->get();
		return $result;  
	}
	//Export data here
	public function exceldataexportmodel() {
		ini_set('max_execution_time', 300);
		$this->load->library('excel');
		$coldata['colname']=array('DoneBy','Action','Module','Data','DateTime');
		$coldata['columndbname']=array('allemp','notificationlogtypeid','modulename','notificationmessage','createdate');
		$title = 'AuditLog';
		$name = preg_replace('/[^A-Za-z0-9]/', '', $title);
		$fname = strtolower($name);
		$action = $_POST['type'];
		$userid = $_POST['userid'];
		$moduleid = $_POST['moduleid'];
		$fromdate = $_POST['fromdate'];
		$todate = $_POST['todate'];
		$result=$this->auditlogexportdata($action,$userid,$moduleid,$fromdate,$todate);//main part
		$this->excel->setActiveSheetIndex(0);		
		$sheet = $this->excel->getActiveSheet();
		$sheet->setTitle($title);
		$count = 0;
		$celtitle = $coldata['colname'];
		$titlecount = count($celtitle);
		$dataname = $coldata['columndbname'];
		$baseurl=$this->config->item('base_url');
		//header
		//sl.NO
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(13);
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);	
		$this->excel->getActiveSheet()->setCellValue('A1','Sl.No');
		$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(50);
		$x=1;
		$h='B';
		for($i=0;$i<$titlecount;$i++) {
			$this->excel->getActiveSheet()->getStyle($h.$x)->getFont()->setSize(13);
			$this->excel->getActiveSheet()->getStyle($h.$x)->getFont()->setBold(true);	
			$this->excel->getActiveSheet()->setCellValue($h.$x,$celtitle[$i]);
			$this->excel->getActiveSheet()->getColumnDimension($h)->setAutoSize(50);
			$h++;
		}
		$m=1;
		$tot_rows = 1;
		$rows=2;
		foreach($result->result() as $row) {
			$c='B';
			$this->excel->getActiveSheet()->getStyle('A'.$rows)->getFont()->setBold(false);
			$this->excel->getActiveSheet()->setCellValue('A'.$rows,$m);
			$this->excel->getActiveSheet()->getStyle('A'.$rows)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			if ($rows % 2 == 0) {
				$this->excel->getActiveSheet()->getStyle('A'.$rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$this->excel->getActiveSheet()->getStyle('A'.$rows)->getFill()->getStartColor()->setARGB('e4f3fd');
			}
			$m++;
			for($i=0;$i<$titlecount;$i++) {
				$dname = $dataname[$i];
				if($dname == 'createdate') {
					$date=explode(' ',$row->$dname);
					$ndate = $this->Auditlogmodel->customerdateset($date[0]);
					$datas =$ndate.' '.$date[1];
				} else if($dname == 'notificationmessage') {
					$message = $row->$dname;
					$datas = $this->Auditlogmodel->excelmergecontentfetch($message,$baseurl,$row->modulelink,$row->modulemastertable,$row->modulename,$row->moduleid,$row->commonid);
				} else {
					$datas = $row->$dname;
				}
				$this->excel->getActiveSheet()->getStyle($c.$rows)->getFont()->setBold(false);
				$this->excel->getActiveSheet()->getStyle($c.$rows)->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->setCellValue($c.$rows,trim($datas));
				$this->excel->getActiveSheet()->getStyle($c.$rows)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				if( is_numeric( trim( $datas ) ) ) {
					$this->excel->getActiveSheet()->getStyle($c.$rows)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_GENERAL);
					$this->excel->getActiveSheet()->getStyle($c.$rows)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				}
				if ($rows % 2 == 0) {
					$this->excel->getActiveSheet()->getStyle($c.$rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
					$this->excel->getActiveSheet()->getStyle($c.$rows)->getFill()->getStartColor()->setARGB('e4f3fd');
				}
				$c++;
			}
			$rows++;
			$tot_rows++;
		}
		ob_clean();
		$time = date("d-m-Y-g-i-s-a");
		$today = date("d-m-Y");
		$today_time=date('g:i:s a');		
		//xlsx format
		$filename=$fname.'_'.$time.'.xlsx';
		header('Content-Type: application/vnd.ms-excel');
		header("Content-type:application/octetstream");
		header("Content-Disposition:attachment;filename=".$filename."");
		header("Content-Transfer-Encoding:binary");
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');		
		$objWriter->save('php://output');		
	}
	//customer based date set
	public function customerdateset($date) {
		$dateformat = $this->Basefunctions->phpmysqlappdateformatfetch();
		$convertdate = date($dateformat['phpformat'], strtotime($date));
		return $convertdate; 
	}
	public function smsprintfileinfofetch($message,$baseurl,$modulelink,$parenttable,$modulename,$moduleid,$primaryid) {
		$tempid = 1;
		$moduleid = $moduleid;
		$parenttable = $parenttable;
		$recordid = $primaryid;
		$primaryset = $parenttable.'.'.$parenttable.'id';
		$print_data=array('templateid'=>$tempid,'Templatetype'=>'Printtemp','primaryset'=>$primaryset,'primaryid'=>$recordid);
		$resultset = $this->smsgenerateprinthtmlfile($print_data,$message,$moduleid);
		return $resultset;
	}
	//preview and print pdf
	public function smsgenerateprinthtmlfile($print_data,$mergetemp,$moduleid) {
		$this->load->model('Printtemplates/Printtemplatesmodel');
		//defaults
		$printdetails['moduleid'] = '1';
		$htmldatacontents='';
		$industryid = $this->Basefunctions->industryid;
		$templateid = ( (isset($print_data['templateid']))? $print_data['templateid'] : 1);
		$id = ( (isset($print_data['primaryid']))? $print_data['primaryid'] : 1);
		$printdetails['contentfile'] =  '';
		$printdetails['moduleid'] =  $moduleid;
		$printdetails['templatename'] = '';
		$parenttable = $this->Basefunctions->generalinformaion('module','modulemastertable','moduleid',$moduleid);
		//body data
		$bodycontent = $mergetemp;
		$bodycontent = preg_replace('~a10s~','&nbsp;', $bodycontent);
		$bodycontent = preg_replace('~<br>~','<br />', $bodycontent);
		$bodycontent = $this->Printtemplatesmodel->removespecialchars($bodycontent);
		$bodyhtml = $this->Printtemplatesmodel->generateprintingdata($bodycontent,$parenttable,$print_data,$printdetails['moduleid'],$type="notification");
		$bodyhtml = $this->Printtemplatesmodel->addspecialchars($bodyhtml);
		return $bodyhtml;
	}
	//merge content for excel report
	public function excelmergecontentfetch($message,$baseurl,$modulelink,$parenttable,$modulename,$moduleid,$primaryid) {
		$this->load->model('Printtemplates/Printtemplatesmodel');
		$finalcont = $message;
		preg_match_all ("/{.*}/U", $message, $contmatch);
		foreach($contmatch as $bocontent) {
			foreach($bocontent as $bomatch) {
				if($bomatch != "{S.Number}") {
					$htmldata = $this->Printtemplatesmodel->snmergcontinformationfetchmodel($bomatch,$parenttable,$primaryid,$moduleid);
					$exp = explode(':',$bomatch);
					if($exp[0] == '{REL') {
						$explode = explode('.',$exp[1]);
						$reltabname = $explode[0];
						$relfieldname = explode('}',$explode[1]);
						$moddatafetch = $this->moduledatafetch($reltabname);
						$relrecordid = $this->Basefunctions->generalinformaion($parenttable,$reltabname.'id',$parenttable.'id',$primaryid);
						$field = $this->Basefunctions->generalinformaion($reltabname,$relfieldname[0],$reltabname.'id',$relrecordid);
						$data = $field;
					} else if($exp[0] == '{GEN') {
						$explode = explode('.',$exp[1]);
						$gentabname = $explode[0];
						$genfieldname = explode('}',$explode[1]);
						$moddatafetch = $this->moduledatafetch($gentabname);
						$genrecordid = $this->Basefunctions->generalinformaion($parenttable,'createuserid',$parenttable.'id',$primaryid);
						$field = $this->Basefunctions->generalinformaion($gentabname,$genfieldname[0],$gentabname.'id',$genrecordid);
						$data = $field;
					} else {
						$data = implode(',',$htmldata);
					}
					$finalcont = preg_replace('~'.$bomatch.'~',$data,$finalcont);
				} else {
					$finalcont = preg_replace('~'.$bomatch.'~','',$finalcont);
				}
			}
		}
		$content = preg_replace('~<p>~','', $finalcont);
		$content = preg_replace('~a10s~',' ', $content);
		$content = preg_replace('~<br>~','', $content);
		$content = preg_replace('~</p>~','', $content);
		return $content;
	}
	//module informationfetch
	public function moduledatafetch($reltabname) {
		$this->db->select("modulelink,moduleid");
		$this->db->from("module");
		$this->db->like("module.modulemastertable",$reltabname);
		$this->db->where("module.status",1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('mid'=>$row->moduleid,'link'=>$row->modulelink);
			}
		} else{
			$data = '';
		}
		return $data;
	}
}