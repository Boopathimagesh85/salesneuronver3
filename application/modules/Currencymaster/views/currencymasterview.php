<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
	<style>
	/* For Tablet */
	#currencyiddivhid .select2-container .select2-choice{*height:1.69rem !important}
	</style>
	<style>
		#currencyconversionadvanceddrop{
			left: -82.109px !important;
		}
		#currencyadvanceddrop{
		    left: -133.109px !important;
		}
	</style>
</head>
<body>
<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<?php
				$modid = '214,215';
				$dataset['gridtitle'] = $gridtitle['title'];
				$dataset['titleicon'] = $gridtitle['titleicon'];
				$dataset['formtype'] = 'multipleaddform';
				$dataset['multimoduleid'] = $modid;
				$this->load->view('Base/mainviewaddformheader',$dataset);
			?>
			<div class="large-12 columns paddingzero scrollbarclass currencymastercontainer">
				<div class="mastermodules">
				<?php
					//function call for dash board form fields generation
					dashboardformfieldsgeneratorwithgrid($moddbfrmfieldsgen,1,$modid);
				?>			
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	$device = $this->Basefunctions->deviceinfo();
	if($device=='phone') {
		$this->load->view('Base/overlaymobile');
	} else {
		$this->load->view('Base/overlay');
	}
	$this->load->view('Base/modulelist');
?>	
</body>
	<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<?php $this->load->view('Base/bottomscript'); ?>
	<script>
		//Tabgroup Dropdown More
		$("#tab1").click(function(){
			$('#currencydataupdatesubbtn').hide();
			$('#currencyreloadicon,#currencysavebutton,#currencydeleteicon').show();
			resetFields(); mastertabid=1; masterfortouch = 0;
			$(".tabclass").addClass('hidedisplay');
			$("#tab1class").removeClass('hidedisplay');
		});
		$("#tab2").click(function() {
			currencyconversionaddgrid();
			$('#currencyconversiondataupdatesubbtn').hide();
			$('#currencyconversionreloadicon,#currencyconversionsavebutton,#currencyconversiondeleteicon').show();
			$(".tabclass").addClass('hidedisplay');
			$("#tab2class").removeClass('hidedisplay');
			resetFields();
			mastertabid=2; 
			masterfortouch = 0;
		});
		$(document).ready(function(){
			{//for keyboard global variable
				 viewgridview = 0;
				 innergridview = 1;
			 }
			{//enable support icon
				$(".supportoverlay").css("display","inline-block")
			}	
		});	
			
	</script>
</html>