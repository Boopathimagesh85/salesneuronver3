<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Solutionscategory extends MX_Controller {
    //Constructor Function To Load Models
	function __construct() {
    	parent::__construct();
    	$this->load->helper('formbuild');
		$this->load->model('Solutionscategory/Solutionscategorymodel');	
	}
	//Default View 
	public function index() {
		$moduleid = array(228);
		sessionchecker($moduleid);
		//action
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(228);
		$viewmoduleid = array(228);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
        $this->load->view('Solutionscategory/solutionscategoryview',$data);	
	}
	//create Campaign
	public function newdatacreate() {  
    	$this->Solutionscategorymodel->newdatacreatemodel();
    }
	//information fetch
	public function fetchformdataeditdetails() {
		$moduleid = 228;
		$this->Solutionscategorymodel->informationfetchmodel($moduleid);
	}
	//update Campaign
    public function datainformationupdate() {
        $this->Solutionscategorymodel->datainformationupdatemodel();
    }
	//delete Campaign
    public function deleteinformationdata() {
        $moduleid = 228;
		$this->Solutionscategorymodel->deleteoldinformation($moduleid);
    } 
	//new tree create
	public function treedatafetchfun() {
		$tablename = $_POST['tabname'];
		$manopt = $_POST['mandval'];
		$lablname = $_POST['fieldlabl'];
		$primaryid = $_POST['primaryid'];
		$mandlab = "";
		if($manopt!='') {
			$mandlab = "<span class='mandatoryfildclass'>*</span>";
		}
		echo '<label>'.$lablname.$mandlab.'</label>';
		echo divopen( array("id"=>"dl-menu","class"=>"dl-menuwrapper","style"=>"z-index:2;margin-bottom: 1rem") );
		$type="button";
		echo '<button name="treebutton" type="button" id="'.$type.'" class="btn dl-trigger treemenubtnclick" tabindex="102" style="height:2.2rem;color:#ffffff;font-size: 1.45rem;padding: 0.1rem;padding-top:0.3rem;background-color:#546E7A;"><i class="material-icons">format_indent_increase</i></button>';
		$txtoptions = array("style"=>"width:83%;display:inline;position:absolute;height:2.12rem;padding-left: 5px;","readonly"=>"readonly","tabindex"=>"101","class"=>"".$manopt."");
		echo text('parent'.$tablename.'','',$txtoptions);
		echo hidden('parent'.$tablename.'id','');
		echo '<ul class="dl-menu maintreegenerate large-12 medium-6 small-12 column" id="solutionscategorylistuldata">';
		$dataset = $this->Basefunctions->treecreatemodel($tablename);
		$this->createTree($dataset,$primaryid,0);
		echo close('ul');
		echo close('div');
		echo '<input type="hidden" value="'.$manopt.'" id="treevalidationcheck" name="treevalidationcheck">';
		echo '<input type="hidden" value="'.$lablname.'" id="treefiledlabelcheck" name="treefiledlabelcheck">';
	}	
	//reload the category list element
	public function getcounterparentid() {
		$this->Solutionscategorymodel->getcategoryparent();
	}
	//tree data delete
	 public function deletetreeinformationdata() {
        $this->Solutionscategorymodel->deleteoldtreeinformation();
    }
	//tree generation code
	public function createTree($items = array(),$primaryid,$parentId= 0) {
		echo "<ul class='dl-submenu'> ";
		if( count($items) > 0 ) {
			foreach ($items[$parentId] as $item) {
				if($primaryid != $item['id']) {
					echo '<li data-listname="'.$item['name'].'" data-listid="'.$item['id'].'" ><a href="#">'.$item['name'].'</a>';//level title
					$curId = $item['id'];
					//if there are children
					if (!empty($items[$curId])) {
						$this->createTree($items, $curId);
					}
					echo '</li>';
				}
			}
		}
		echo '</ul>';
	}
}