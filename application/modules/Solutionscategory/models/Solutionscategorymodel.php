<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Solutionscategorymodel extends CI_Model{
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//create counter
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['solutionscategoryelementsname']);
		$formfieldstable = explode(',',$_POST['solutionscategoryelementstable']);
		$formfieldscolmname = explode(',',$_POST['solutionscategoryelementscolmn']);
		$categoryelementpartable = explode(',',$_POST['solutionscategoryelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($categoryelementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
		echo 'TRUE';
	}
	//retrive counter data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['solutionscategoryelementsname']);
		$formfieldstable = explode(',',$_GET['solutionscategoryelementstable']);
		$formfieldscolmname = explode(',',$_GET['solutionscategoryelementscolmn']);
		$categoryelementpartable = explode(',',$_GET['solutionscategoryelementspartabname']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($categoryelementpartable);
		$primaryid = $_GET['solutionscategoryprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	//update counter
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['solutionscategoryelementsname']);
		$formfieldstable = explode(',',$_POST['solutionscategoryelementstable']);
		$formfieldscolmname = explode(',',$_POST['solutionscategoryelementscolmn']);
		$categoryelementpartable = explode(',',$_POST['solutionscategoryelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($categoryelementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['solutionscategoryprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		echo $result;
	}
	//delete solutions category
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['solutionscategoryelementstable']);
		$parenttable = explode(',',$_GET['solutionscategoryelementspartabname']);
		$id = $_GET['solutionscategoryprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($categoryparenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				echo "TRUE";
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			echo "TRUE";
		}
	} 
	//delete tree counter
	public function deleteoldtreeinformation() {
		$formfieldstable = explode(',',$_GET['solutionscategoryelementstable']);
		$parenttable = explode(',',$_GET['solutionscategoryelementspartabname']);
		$id = $_GET['solutionscategoryprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//delete function
		$id = $_GET['solutionscategoryprimarydataid'];
		$parent = $id;
		$this->Crudmodel->treedatadeletefunction($partabname,$id,$parent);
		echo "TRUE";
	}
	//parent solution category
	public function getcategoryparent() {
		$id=$_GET['id'];
		$industryid = $this->Basefunctions->industryid;
		$data=$this->db->select('a.parentknowledgebasecategoryid as parentcategoryids,b.knowledgebasecategoryname as categorynames')
				->from('knowledgebasecategory as a')
				->join('knowledgebasecategory as b','a.parentknowledgebasecategoryid=b.knowledgebasecategoryid')
				->where('a.knowledgebasecategoryid',$id)
				->where('a.industryid',$industryid)
				->get()->result();
		foreach($data as $in)
		{
			$parentcategoryid=$in->parentcategoryids;
			$categoryname=$in->categorynames;
		}
		$a=array('parentcategoryid'=>$parentcategoryid,'categoryname'=>$categoryname);
		echo json_encode($a);
	}
}