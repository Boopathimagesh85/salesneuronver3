<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Model File
class Contactsmodel extends CI_Model{
    public function __construct() {
		parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//To Create New contact
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$restricttable = explode(',',$_POST['resctable']);
		//industry based moduleid
		$moduleid = $_POST['viewfieldids'];
		//Retrieve the Contact autoNumber
		$anfieldnameinfo = $_POST['anfieldnameinfo'];
		$anfieldnameidinfo = $_POST['anfieldnameidinfo'];
		$anfieldtabinfo = $_POST['anfieldtabinfo'];
		$anfieldnamemodinfo = $_POST['anfieldnamemodinfo'];
		$randomnum = $this->Basefunctions->randomnumbergenerator($anfieldnamemodinfo,$anfieldtabinfo,$anfieldnameinfo,$anfieldnameidinfo);
		$_POST['contactnumber'] = trim($randomnum);
		//dynamic Insertion
		$primaryid = $this->Crudmodel->datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable);
		///address insertion
		$arrname=array('primary','secondary');
		$this->Crudmodel->addressdatainsert($arrname,$partablename,$primaryid);
		//notification entry
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		if(isset($_POST['employeeid'])) {
			$assignid = $_POST['employeeid'];
			$emptypes = $_POST['employeetypeid'];
			$empdataids = array();
			$assignempids = array();
			if($assignid != '') {
				$i = 0;
				$m=0;
				$empiddatas = explode(',',$assignid);
				$emptypeids = explode(',',$emptypes);
				foreach($empiddatas as $empids) {
					$emptype = $emptypeids[$m];
					if($emptype == 1) {
						$empdataids[$i] = $empids;
						$i++;
					} else if($emptype == 2) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromgroup($empids);
						$i++;
					} else if($emptype == 3) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromroles($empids);
						$i++;
					} else if($emptype == 4) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromrolesandsubroles($empids);
						$i++;
					}
					$m++;
				}
			} else {
				$assignid = 1;
			}
		} else {
			$assignid = 1;
		}
		$contactname = $_POST['lastname'];
		if($assignid == '1') {
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,2);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,$moduleid);
			}
		} else {
			foreach($empdataids as $empidinfo) {
				if(is_array($empidinfo)) {//group of employees
					foreach($empidinfo as $empid) {
						if( !in_array($empid,$assignempids) ) {
							array_push($assignempids,$empid);
							$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,2);
							if($notimsg != '') {
								$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,$moduleid);
							}
						}
					}
				} else { //individual employees
					if( !in_array($empidinfo,$assignempids) ) {
						array_push($assignempids,$empidinfo);
						$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,2);
						if($notimsg != '') {
							$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,$moduleid);
						}
					}
				}
			}
		}
		$softwareindustryid = $this->Basefunctions->industryid;
		if($softwareindustryid == 4){
			//grid data insertion
			//primary key
			$defdataarr = $this->Crudmodel->defaultvalueget();
			$primaryname = $this->Crudmodel->primaryinfo($partablename);
			$m=0;
			$h=1;
			$gridfieldpartabname = explode(',',$_POST['griddatapartabnameinfo']);
			$gridrows = explode(',',$_POST['numofrows']);
			$griddata = $_POST['griddatas'];
			$griddatainfo = json_decode($griddata, true);
			foreach($gridrows as $rowcount) {
				$gridfieldsname = explode(',',$_POST['gridcolnames'.$h]);
				//filter grid unique fields table
				$gridfieldtabname = explode(',',$_POST['griddatatabnameinfo'.$h]);
				$gridfieldstable = $this->Crudmodel->filtervalue($gridfieldtabname);
				$gridtableinfo = explode(',',$gridfieldstable);
				for($i=0;$i<$rowcount;$i++) {
					foreach( $gridtableinfo as $gdtblname ) {
						${'$gcdata'.$m} = array();
						$t=0;
						foreach($gridfieldsname AS $gdfldsname) {
							if(strcmp( trim($gridfieldtabname[$t]),trim($gdtblname) ) == 0 ) {
								if( isset( $griddatainfo[$i][$gdfldsname] ) ) {
									$name = explode('_',$gdfldsname);
									if( ctype_alpha($griddatainfo[$i][$gdfldsname]) && $griddatainfo[$i][$gdfldsname] != "" ) {
										${'$gcdata'.$m}[$gdfldsname] = ucwords( $griddatainfo[$i][$gdfldsname] );
									} else if( $griddatainfo[$i][$gdfldsname] != "" ) {
										${'$gcdata'.$m}[$gdfldsname] = $griddatainfo[$i][$gdfldsname];
									}
								}
							}
						}
						if(count(${'$gcdata'.$m})>0) {
							${'$gcdata'.$m}[$primaryname]=$primaryid;
							$gnewdata = array_merge(${'$gcdata'.$m},$defdataarr);
							//unset the hidden variables
							$removehidden=array('contactdetailid');
							for($mk=0;$mk<count($removehidden);$mk++){
								unset($gnewdata[$removehidden[$mk]]);
							}
							$inarr = array_filter($gnewdata);
							$this->db->insert($gdtblname,$inarr);
						}
						$m++;
					}
				}
				$h++;
			}
		}
		{//workflow management -- for data create
			$this->Basefunctions->workflowmanageindatacreatemode($moduleid,$primaryid,$partablename);
		}		
		if($softwareindustryid == 4){
			echo $primaryid;
		}else{
			echo "TRUE";
		}
		
	}
	//information fetch
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementpartable']);
		$restricttable = array('contactdetail');
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['dataprimaryid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetchwithrestrict($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$restricttable,$moduleid);
		echo $result;
	}
	//update data information
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['primarydataid'];
		$restricttable = explode(',',$_POST['resctable']);
		//industry based moduleid
		$moduleid = $_POST['viewfieldids'];
		{//fetch old values -- work flow
			$condstatvals=$this->Basefunctions->workflowmanageolddatainfofetch($moduleid,$primaryid);
		}
		$result = $this->Crudmodel->dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable);
		$arrname=array('primary','secondary');
		$this->Crudmodel->addressdataupdate($arrname,$partablename,$primaryid);
		//notification update
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		if(isset($_POST['employeeid'])) {
			$assignid = $_POST['employeeid'];
			$emptypes = $_POST['employeetypeid'];
			$empdataids = array();
			$assignempids = array();
			if($assignid != '') {
				$i = 0;
				$m=0;
				$empiddatas = explode(',',$assignid);
				$emptypeids = explode(',',$emptypes);
				foreach($empiddatas as $empids) {
					$emptype = $emptypeids[$m];
					if($emptype == 1) {
						$empdataids[$i] = $empids;
						$i++;
					} else if($emptype == 2) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromgroup($empids);
						$i++;
					} else if($emptype == 3) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromroles($empids);
						$i++;
					} else if($emptype == 4) {
						$empdataids[$i] = $this->Basefunctions->empidfetchfromrolesandsubroles($empids);
						$i++;
					}
					$m++;
				}
			} else {
				$assignid = 1;
			}
		} else {
			$assignid = 1;
		}
		$Contactname = $_POST['lastname'];
		if($assignid == '1') {
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,3);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,$moduleid);
			}
		} else {
			foreach($empdataids as $empidinfo) {
				if(is_array($empidinfo)) { //group of employees
					foreach($empidinfo as $empid) {
						if( !in_array($empid,$assignempids) ) {
							array_push($assignempids,$empid);
							$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,3);
							if($notimsg != '') {
								$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,$moduleid);
							}
						}
					}
				} else { //individual employees
					if( !in_array($empidinfo,$assignempids) ) {
						array_push($assignempids,$empidinfo);
						$notimsg = $this->Basefunctions->notificationtemplatedatafetch($moduleid,$primaryid,$partablename,3);
						if($notimsg != '') {
							$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,$moduleid);
						}
					}
				}
			}
		}
		$softwareindustryid = $this->Basefunctions->industryid;
		if($softwareindustryid == 4){
			//get the old product detail
			$oldcontactdetailid =array();
			$contactdetail=$this->db->select('contactdetailid')
			->from('contactdetail')
			->where('contactid',$primaryid)
			->where('status',$this->Basefunctions->activestatus)
			->get();
			foreach($contactdetail->result() as $info) {
				$oldcontactdetailid[]=$info->contactdetailid;
			}
			$gridfieldpartabname = explode(',',$_POST['griddatapartabnameinfo']);
			$gridrows = explode(',',$_POST['numofrows']);
			$griddata = $_POST['griddatas'];
			$griddatainfo = json_decode($griddata, true);
			//filter unique grid parent table
			$gridpartablename =  $this->Crudmodel->filtervalue($gridfieldpartabname);
			for($lm=0;$lm < count($griddatainfo);$lm++) {
				$newcontactdetailid[]=$griddatainfo[$lm]['contactdetailid'];
			}
			$deletedcontactdetailid=ARRAY();
			//find deleted records
			for($m=0;$m < count ($oldcontactdetailid);$m++) {
				if(in_array($oldcontactdetailid[$m],$newcontactdetailid)) {
				} else {
					$deletedcontactdetailid[]=$oldcontactdetailid[$m];
				}
			}
			if(count($deletedcontactdetailid) > 0) {
				//delete productdetail and further tables
				for($k=0;$k<count($deletedcontactdetailid);$k++) {
					$this->Crudmodel->outerdeletefunction('contactdetail','contactdetailid','',$deletedcontactdetailid[$k]);
				}
			}
			//Insert the New quote records
			$defdataarr = $this->Crudmodel->defaultvalueget();
			$primaryname = $this->Crudmodel->primaryinfo($partablename);
			$m=0;
			$h=1;
			foreach($gridrows as $rowcount) {
				$gridfieldsname = explode(',',$_POST['gridcolnames'.$h]);
				//filter grid unique fields table
				$gridfieldtabname = explode(',',$_POST['griddatatabnameinfo'.$h]);
				$gridfieldstable = $this->Crudmodel->filtervalue($gridfieldtabname);
				$gridtableinfo = explode(',',$gridfieldstable);
				for($i=0;$i<$rowcount;$i++) {
					foreach( $gridtableinfo as $gdtblname ) {
						${'$gcdata'.$m} = array();
						$t=0;
						foreach($gridfieldsname AS $gdfldsname) {
							if(strcmp( trim($gridfieldtabname[$t]),trim($gdtblname) ) == 0 ) {
								if( isset( $griddatainfo[$i][$gdfldsname] ) ) {
									$name = explode('_',$gdfldsname);
									if( ctype_alpha($griddatainfo[$i][$gdfldsname]) && $griddatainfo[$i][$gdfldsname] != "" ) {
										${'$gcdata'.$m}[$gdfldsname] = ucwords( $griddatainfo[$i][$gdfldsname] );
									} else if( $griddatainfo[$i][$gdfldsname] != "" ) {
										${'$gcdata'.$m}[$gdfldsname] = $griddatainfo[$i][$gdfldsname];
									}
								}
							}
						}
						if(count(${'$gcdata'.$m})>0) {
							${'$gcdata'.$m}[$primaryname]=$primaryid;
							$gnewdata = array_merge(${'$gcdata'.$m},$defdataarr);
							//unset the hidden variables
							$removehidden=array('contactdetailid');
							for($mk=0;$mk<count($removehidden);$mk++){
								unset($gnewdata[$removehidden[$mk]]);
							}
							if($griddatainfo[$i]['contactdetailid'] == 0){ //new-data
								$this->db->insert($gdtblname,array_filter($gnewdata));
								$contactdetailid=$this->db->insert_id();
							}
							if($griddatainfo[$i]['contactdetailid'] > 0){ //update-existing data
								$this->db->where('contactid',$griddatainfo[$i]['contactdetailid']);
								$this->db->update($gdtblname,array_filter($gnewdata));
								$contactdetailid=$griddatainfo[$i]['contactdetailid'];								
							}							
						}
						$m++;
					}
				}
				$h++;
			}
		}
		{//workflow management for data update
			$this->Basefunctions->workflowmanageindataupdatemode($moduleid,$primaryid,$partablename,$condstatvals);
		}
		if($softwareindustryid == 4){
			echo $primaryid;
		}else{
			echo "TRUE";
		}
	}
	//delete old information
	public function deleteoldinformation($moduleid)	{
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$msg='False';
		$id = $_GET['primarydataid'];
		$filename = $this->Basefunctions->generalinformaion('contact','lastname','contactid',$id);	
		//google contactid
		$gcontactid = $this->Basefunctions->generalinformaion('contact','gcontactid','contactid',$id);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//industrybased moduleid fetch
		$modid = $this->Basefunctions->getmoduleid($moduleid);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		{//workflow management -- for data delete
			$this->Basefunctions->workflowmanageindatadeletemode($modid,$id,$primaryname,$partabname);
		}
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				$msg='Denied';
			} else {
				//notification log
				$empid = $this->Basefunctions->logemployeeid;
				//deleted file name
				$notimsg = $this->Basefunctions->notificationtemplatedatafetch($modid,$id,$partabname,4);
				if($notimsg != '') {
					$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,1,$modid);
				}
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				
				$msg='TRUE';
			}
		} else {
			//notification log
			$empid = $this->Basefunctions->logemployeeid;
			//deleted file name
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch($modid,$id,$partabname,4);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,1,$modid);
			}
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);		
			
			$msg='TRUE';
		}
		echo $msg;
	}
	public function membershipplansdatafectchmodel(){
		$planid = $_GET['planid'];
		$this->db->select('product.unitprice,productcf.durationid,productcf.batchnameid');
		$this->db->from('product');
		$this->db->join('productcf','productcf.productid=product.productid');
		$this->db->where('product.productid',$planid);
		$this->db->where('product.status',1);
		$result = $this->db->get();
		foreach($result->result() as $row) {
			$data= array('unitprice'=>$row->unitprice,'durationid'=>$row->durationid,'batchnameid'=>$row->batchnameid);			
		}
		echo json_encode($data);
	}
	public function memberdetailfetchmodel(){
		$memberid = $_GET['memberid'];
		$this->db->select('product.productname,contactdetail.membershipplans,contactdetail.joiningdate,contactdetail.expirydate');
		$this->db->from('contactdetail');
		$this->db->join('product','product.productid=contactdetail.membershipplans');
		$this->db->where('contactdetail.contactid',$memberid);
		$this->db->where('contactdetail.status',1);
		$detail = $this->db->get();
		if($detail->num_rows() > 0){
			foreach($detail->result() as $detailval) {
				$contactdetail[] = array('productname'=>$detailval->productname,'membershipplans'=>$detailval->membershipplans,'joiningdate'=>$detailval->joiningdate,'expirydate'=>$detailval->expirydate);
			}
		}
		$this->db->select('contact.contactnumber,contactcf.genderid,contactcf.age,contactcf.bloodgroupid,contactcf.height,contactcf.weight');
		$this->db->from('contact');
		$this->db->join('contactcf','contactcf.contactid=contact.contactid');
		$this->db->where('contact.contactid',$memberid);
		$this->db->where('contact.status',1);
		$result = $this->db->get();
		if(isset($contactdetail)){
			$contactdataset = $contactdetail;
		}else{
			$contactdataset = '';
		}
		foreach($result->result() as $row) {
			$data= array('contactnumber'=>$row->contactnumber,'genderid'=>$row->genderid,'age'=>$row->age,'bloodgroupid'=>$row->bloodgroupid,'height'=>$row->height,'weight'=>$row->weight,'data'=>$contactdataset);
		}
		echo json_encode($data);
	}
	public function contactgriddetailmodel() {
		$batchname = '';
		$contactid=trim($_GET['primarydataid']);
		$this->db->select('contactdetailid,product.productname,product.productid,duration.durationid,duration.durationname,contactdetail.joiningdate,contactdetail.amount,contactdetail.expirydate,planstatus.planstatusid,planstatus.planstatusname,contactdetail.holdduration,contactdetail.batchnameid');
		$this->db->from('contactdetail');
		$this->db->join('contact','contact.contactid=contactdetail.contactid');
		$this->db->join('product','product.productid=contactdetail.membershipplans');
		$this->db->join('duration','duration.durationid=contactdetail.durationid');
		$this->db->join('planstatus','planstatus.planstatusid=contactdetail.planstatusid');
		$this->db->where('contactdetail.contactid',$contactid);
		$this->db->where('contactdetail.status',$this->Basefunctions->activestatus);
		$data=$this->db->get();
		if($data->num_rows() > 0) {
			$j=0;			
			foreach($data->result() as $value) {	
				$mvalu=explode(",",$value->batchnameid);
				$this->db->select('batchnamename');
				$this->db->from('batchname');
				$this->db->where_in('batchnameid',$mvalu);
				$this->db->where('status',$this->Basefunctions->activestatus);
				$batchdata=$this->db->get();				
				if($batchdata->num_rows() > 0) {
					unset($batchname);
					foreach($batchdata->result() as $batchdataval) {						
						$batchname[] = $batchdataval->batchnamename;
					}					
				}
				if($value->holdduration){
					$holdduration = $value->holdduration;
				}else{
					$holdduration = '';
				}
				$productdetail->rows[$j]['id'] = $value->contactdetailid;			
				$productdetail->rows[$j]['cell']=array(
							$value->productname,
							$value->productid,
							$value->durationname,
							$value->durationid,
							$batchname,
							$value->batchnameid,
							$value->joiningdate,
							$value->amount,
							$value->expirydate,
							$value->planstatusname,
							$value->planstatusid,
							$holdduration,
							$value->contactdetailid
					);
				$j++;
			}
		}
		echo  json_encode($productdetail);
	}
	//primary and secondary address value fetch
	public function primaryaddressvalfetchmodel() {
		$accid = $_GET['dataprimaryid'];
		$this->db->select('contactaddress.addresssourceid,contactaddress.address,contactaddress.pincode,contactaddress.city,contactaddress.state,contactaddress.country');
		$this->db->from('contactaddress');
		$this->db->where('contactaddress.contactid',$accid);
		$this->db->where('contactaddress.status',1);
		$this->db->order_by('contactaddressid','asc');
		$result = $this->db->get();
		$arrname=array('primary','secondary');
		if($result->num_rows() >0) {
			$m=0;
			foreach($result->result() as $row) {
				$data[] = array($arrname[$m].'addresstype'=>$row->addresssourceid,
								$arrname[$m].'address'=>$row->address,
								$arrname[$m].'pincode'=>$row->pincode,
								$arrname[$m].'city'=>$row->city,
								$arrname[$m].'state'=>$row->state,
								$arrname[$m].'country'=>$row->country);
				$m++;
			}
			if(count($data) != 1 ) {
				$finalarray = array_merge($data[0],$data[1]);
			} else {
				$finalarray = $data[0];
			}
			echo json_encode($finalarray);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}	
}