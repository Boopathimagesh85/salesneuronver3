<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Contact extends MX_Controller{
	//To load the Register View
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Contact/Contactsmodel');
		$this->load->view('Base/formfieldgeneration');
    }
	public function index() {
		$moduleid = array(203,75,82,130);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['dashboradviewdata'] = $this->Basefunctions->dashboarddataviewbydropdown($moduleid);
		$this->load->view('Contact/contactsview',$data);
		
	}
	//Create New data
	public function newdatacreate() {
    	$this->Contactsmodel->newdatacreatemodel();
    }
	//information fetch
	public function fetchformdataeditdetails() {
		$moduleid = array(203,75,82,130);
		$this->Contactsmodel->informationfetchmodel($moduleid);
	}
	//Update old data
    public function datainformationupdate() {
        $this->Contactsmodel->datainformationupdatemodel();
    }
	//delete old information
    public function deleteinformationdata() {
    	$moduleid = array(203,75,82,130);
    	$this->Contactsmodel->deleteoldinformation($moduleid);
    }
	//primary address value fetch
	public function primaryaddressvalfetch() {
		$this->Contactsmodel->primaryaddressvalfetchmodel();
	}
	//clone data
	public function viewdataclone() {
		$empid = $this->Basefunctions->userid;
		$vmaxid = $_POST['rowid'];
		$parenttable = 'contact';
		$childtabinfo = 'contactaddress';
		$fieldinfo = 'contactid';
		$fieldvalue = $vmaxid;
		$updatecloumn = 'contactname';
		$viewid = $this->Basefunctions->viewdataclonemodel($parenttable,$childtabinfo,$fieldinfo,$fieldvalue);
		if($updatecloumn!='') {
			$this->db->query(' UPDATE '.$parenttable.' SET '.$updatecloumn.' = concat('.$updatecloumn.'," clone") where '.$fieldinfo.' = '.$viewid.'');
		}
		echo 'TRUE';
	}	
	public function setcontactid() {
		if(isset($_POST['datas'])) {
			$patientid = $_POST['datas'];
			$this->load->library('session');
			$this->session->set_userdata('patientid', $patientid);
			$pat = $this->session->userdata('patientid');
			echo $pat;
		}
	}
	
	public function membershipplansdatafectch(){
		$this->Contactsmodel->membershipplansdatafectchmodel();
	}
	public function memberdetailfetch(){
		$this->Contactsmodel->memberdetailfetchmodel();
	}
	public function contactgriddetail(){
		$this->Contactsmodel->contactgriddetailmodel();
	}
}