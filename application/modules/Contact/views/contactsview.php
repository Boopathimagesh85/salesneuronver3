<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
</head>
<body>
	<?php
		$moduleid = implode(',',$moduleids);
		$device = $this->Basefunctions->deviceinfo();
		$dataset['gridenable'] = "yes"; //no
		$dataset['griddisplayid'] = "contactcreationview";
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['spanattr'] = array();
		$dataset['gridtableid'] = "contactcreategrid";
		$dataset['griddivid'] = "contactcreategridnav";
		$dataset['moduleid'] = $moduleids;
		$dataset['forminfo'] = array(array('id'=>'contactcreationformadd','class'=>'hidedisplay','formname'=>'contactcreationform')); 
		if($device=='phone') {
			$this->load->view('Base/gridmenuheadermobile',$dataset);
			$this->load->view('Base/overlaymobile');
			$this->load->view('Base/viewselectionoverlay');
		} else {
			$this->load->view('Base/gridmenuheader',$dataset);
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
		$this->load->view('Contact/contactoverlayset.php');
		$this->load->view('Base/basedeleteform');
	?>
</body>
	<!-- To Load Date picker form fields placed above (DOB Format)-->
	<script src="<?php echo base_url();?>js/Contact/contact.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/uploadfile/jquery.uploadfile.min.js"></script>
	<!--For Tablet and Mobile view Dropdown Script-->
	<script>
		$(document).ready(function() {
			$("#tabgropdropdown").change(function() {
				var tabgpid = $("#tabgropdropdown").val();
				$('.sidebaricons[data-subform="'+tabgpid+'"]').trigger('click');
			});
		});
	</script>
	<?php $this->load->view('Base/bottomscript'); ?>
</html>