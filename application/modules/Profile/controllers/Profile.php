<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Profile extends MX_Controller 
{
    //Constructor Function To Load Models
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Profile/Profilemodel');
	}
	//Default View 
	public function index() {
		$moduleid = array(248);
		sessionchecker($moduleid);
		//action
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(248);
		$viewmoduleid = array(248);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['profile'] = $this->Basefunctions->simpledropdown('profile','profileid,profilename','profilename');
		$data['module'] = $this->Basefunctions->simpledropdownwithmulticond('moduleid','menuname,modulename','module','menutype,moduleprivilegeid','Internal,248');
        $this->load->view('Profile/profileview',$data);
	}
	//profile based module list
	public function fetchprofilebasedmodulefetch() {
		$this->Profilemodel->fetchprofilebasedmodulefetchmodel();
	}
	//profile information create
	public function profileinformationcreate() {
		$this->Profilemodel->profileinformationcreatemodel();
	}
	//data information fetch
	public function profiledatafetch() {
		$this->Profilemodel->profiledatafetchmodel();
	}
	//profile information update
	public function profileinformationupdate() {
		$this->Profilemodel->profileinformationupdatemodel();
	}
	//delete profile
    public function profiledeletefunction() {
        $this->Profilemodel->profiledeletefunctionmodel();
    }
	//profile data view
	public function profilegriddatafetch() {
		$grid=$this->Basefunctions->getpagination();
		$result=$this->Profilemodel->profilegriddatafetchxmlview($grid['sidx'],$grid['sord'],$grid['start'],$grid['limit'],$grid['wh']);
        $pageinfo=$this->Basefunctions->page();
		header("Content-type: text/xml;charset=utf-8");
		$s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .=  "<rows>";
        $s .= "<page>".$_GET['page']."</page>";
        $s .= "<total>".$pageinfo['totalpages']."</total>";
        $s .= "<records>".$pageinfo['count']."</records>";
		ob_clean();
		foreach($result->result() as $row) {
			$s .= "<row id='". $row->profileid."'>";
			$s .= "<cell><![CDATA[".$row->profileid."]]></cell>";
			$s .= "<cell><![CDATA[".$row->profilename."]]></cell>";
			$s .= "<cell><![CDATA[".$row->description."]]></cell>";
			$s .= "<cell><![CDATA[".$row->statusname."]]></cell>";
			$s .= "</row>";
		}
		$s .= "</rows>";
		echo $s;
	}
	//Module data view
	public function profilebasedmodulelist() {
		$profileid = $_GET['profileid'];
		$grid=$this->Basefunctions->getpagination();
		$result=$this->Profilemodel->profilebasedmodulelistxmlview($profileid,$grid['wh']);
		$pageinfo=$this->Basefunctions->page();
		ob_clean();
		header("Content-type: text/xml;charset=utf-8");
		$s = "<?xml version='1.0' encoding='utf-8'?>";
		$s .=  "<rows>";
		$s .= "<page>".$_GET['page']."</page>";
		$s .= "<total>".$pageinfo['totalpages']."</total>";
		$s .= "<records>".$pageinfo['count']."</records>";
		if($profileid!=1) {
			$check = $this->Profilemodel->profileidcheckinmoduleinfo('profileid','profilemoduleinfo',$profileid);
			if($check == "TRUE") { //module id is not available
				foreach($result->result() as $row) {
					$moduleid = $row->moduleid;
					$exmod = explode(',',$moduleid);
					$defmodid = $this->Profilemodel->defaultmoduleidget($grid['wh']);
					$count = count($defmodid);
					for($i=0;$i<$count;$i++) {
						$toolbarid = $this->Profilemodel->tollbaridget($defmodid[$i],$profileid);//profile based tool bar id
						$moduletoolbarid = $this->Profilemodel->moduletollbaridget($defmodid[$i]);//tool bar group id
						$modulename =  $this->Profilemodel->modulenameget($defmodid[$i]);//module name get
						$dashmod = $this->Profilemodel->checkdashboardid($defmodid[$i]);
						$profileaction = $this->Profilemodel->actionnamefetch($toolbarid,$exmod);//profile toolbarid separate
						$moduleaction = $this->Profilemodel->actionnamefetch($moduletoolbarid,$exmod);//module based toolbarid separate
						$actionvalue = $this->Profilemodel->modulebasedactionvalue($profileaction,$moduleaction);
						//check module status
						$modstatus = 'false';
						if(in_array($defmodid[$i],$exmod)) {
							$modstatus = 'true';
							$status = 'Profile Assigned';
						} else {
							$modstatus = 'false';
							$status = 'Profile Un Assigned';
						}
						$s .= "<row id='".$defmodid[$i]."'>";
						$s .= "<cell><![CDATA[".$defmodid[$i]."]]></cell>";
						$s .= "<cell><![CDATA[".$modulename['catname']."]]></cell>";
						$s .= "<cell><![CDATA[".$modulename['modname']." ".$dashmod."]]></cell>";
						$s .= "<cell><![CDATA[".$modstatus."]]></cell>";
						$s .= "<cell><![CDATA[".$actionvalue[0]."]]></cell>";
						$s .= "<cell><![CDATA[".$actionvalue[1]."]]></cell>";
						$s .= "<cell><![CDATA[".$actionvalue[2]."]]></cell>";
						$s .= "<cell><![CDATA[".$actionvalue[3]."]]></cell>";
						if($moduleaction[4] != '') {
							$cusstatus = ( ($profileaction[4]!="") ? 'true':'false');
							$s .= "<cell><![CDATA[".$cusstatus."]]></cell>";
						} else {
							$s .= "<cell><![CDATA[empty]]></cell>";
						}
						$s .= "<cell><![CDATA[".$status."]]></cell>";
						$s .= ( ($moduleaction[4]!="" ) ? "<cell><![CDATA[".$profileaction[4]."||".$moduleaction[4]."]]></cell>" : "<cell><![CDATA[ ]]></cell>" );
						$s .= "<cell><![CDATA[".$dashmod."]]></cell>";
						$s .= "<cell><![CDATA[]]></cell>";
						$s .= "</row>"; 
					}
				}
			} else if($check == "FALSE") { //module id available
				$defmodid = $this->Profilemodel->defaultmoduleidget($grid['wh']);
				foreach($result->result() as $row) {
					$toolbarid = $this->Profilemodel->moduletollbaridget($row->moduleid);//tool bar group id
					$value = $this->Profilemodel->actionnamefetch($toolbarid,$defmodid);
					$dashmod = $this->Profilemodel->checkdashboardid($row->moduleid);
					$moduletoolbarid = $this->Profilemodel->moduletollbaridget($row->moduleid);//tool bar group id
					$profileaction = $this->Profilemodel->actionnamefetch($toolbarid,$defmodid);//profile toolbarid separate
					$moduleaction = $this->Profilemodel->actionnamefetch($moduletoolbarid,$defmodid);//module based 
					$count = count($value);
					for($i=0;$i<$count;$i++){
						if($value[$i] != ''){
							$actionvalue[$i]='false';
						} else {
							$actionvalue[$i]='empty';
						}
					}
					$status = 'Profile Un Assigned';
					$s .= "<row id='". $row->moduleid."'>";
					$s .= "<cell><![CDATA[".$row->moduleid."]]></cell>";
					$s .= "<cell><![CDATA[".$row->menucategoryname."]]></cell>";
					$s .= "<cell><![CDATA[".$row->modulename." ".$dashmod."]]></cell>";
					$s .= "<cell><![CDATA[".'false'."]]></cell>";
					$s .= "<cell><![CDATA[".$actionvalue[0]."]]></cell>";
					$s .= "<cell><![CDATA[".$actionvalue[1]."]]></cell>";
					$s .= "<cell><![CDATA[".$actionvalue[2]."]]></cell>";
					$s .= "<cell><![CDATA[".$actionvalue[3]."]]></cell>";
					$s .= "<cell><![CDATA[".$actionvalue[4]."]]></cell>";
					$s .= "<cell><![CDATA[".$status."]]></cell>";
					$s .= ( ($moduleaction[4]!="" ) ? "<cell><![CDATA[".$profileaction[4]."||".$moduleaction[4]."]]></cell>" : "<cell><![CDATA[ ]]></cell>" );
					$s .= "<cell><![CDATA[".$dashmod."]]></cell>";
					$s .= "<cell><![CDATA[]]></cell>";
					$s .= "</row>";
				}
			}
		}
		$s .= "</rows>";
		echo $s;
	}
	//profile based module insertion and action
	public function profilemodulesubmit() {
		$this->Profilemodel->profilemodulesubmitmodel();
	}
	//Module based Filed data view
	public function modulebasedfieldslist() {
		$profileid = $_GET['profileid'];
		$moduleid = $_GET['moduleid'];
		$dashboardmodid = $this->Profilemodel->fetchdashboardmoduleid($moduleid);
		$moduleids = ( ($dashboardmodid!=1) ? $dashboardmodid : $_GET['moduleid'] );
		$modid = explode(',',$moduleids);
		$grid=$this->Basefunctions->getpagination();
		$result=$this->Profilemodel->modulebasedfieldslistxmlview($grid['sidx'],$grid['sord'],$modid);
		$pageinfo=$this->Basefunctions->page();
		header("Content-type: text/xml;charset=utf-8");
		$s = "<?xml version='1.0' encoding='utf-8'?>";
		$s .=  "<rows>";
		$s .= "<page>".$_GET['page']."</page>";
		$s .= "<total>".$pageinfo['totalpages']."</total>";
		$s .= "<records>".$pageinfo['count']."</records>";
		ob_clean();
		foreach($result->result() as $row) {
			$modulefieldid = $row->modulefieldid;
			$check = $this->Profilemodel->checkprofilemodulefield($modid,$profileid,$modulefieldid);
			$fstatus = ($check == 'TRUE' ? 'Assigned':'Un Assigned');
			$factive = ($check == 'TRUE' ? 'true':'false');
			$restrict = $row->fieldrestrict;
			$mread = "empty";
			$mvisible = "empty";
			if($restrict != 'Yes') {
				$mread = $row->readonly;
				$mvisible = $row->active;
				$datasets = $this->Profilemodel->fetchmodulefieldids($modulefieldid,$profileid,$modid);
				if(count($datasets) > 0 ) {
					$visibility = $datasets['fieldvisible'];
					$mode = explode(',',$visibility);
					$visible=$mode[0];
					$read=$mode[1];
					$mvisible = ($visible == 'Yes' ? 'true' : 'false');
					$mread = ($read == 'Yes' ? 'true' : 'false');
				} else {
					$read = $row->readonly;
					$visible = $row->active;
					$mvisible = ($visible == 'Yes' ? 'true' : 'false');
					$mread = ($read == 'Yes' ? 'true' : 'false');
				}
			}
			$s .= "<row id='".$row->modulefieldid."'>";
			$s .= "<cell><![CDATA[".$row->modulefieldid."]]></cell>";
			$s .= "<cell><![CDATA[".$row->moduletabid."]]></cell>";
			$s .= "<cell><![CDATA[".$row->fieldlabel."]]></cell>";
			$s .= "<cell><![CDATA[".$factive."]]></cell>";
			$s .= "<cell><![CDATA[".$mvisible."]]></cell>";
			$s .= "<cell><![CDATA[".$mread."]]></cell>";
			$s .= "<cell><![CDATA[".$fstatus."]]></cell>";
			$s .= "</row>";
		}
		$s .= "</rows>";
		echo $s;
	}
	//module base Field  update- active
	public function modulefieldsmodeupdate() {
		$this->Profilemodel->modulefieldsmodeupdatemodel();
	}
	//module base Field  update- read only
	public function modulefieldreadonly() {
		$this->Profilemodel->modulefieldreadonlymodel();
	}
	//value drop down load function model
	public function valuedropdownloadfun() {
		$this->Profilemodel->valuedropdownloadfunmodel();
	}
	//Module based Filed data view
	public function profilefieldvalueget() {
		$profid = $_GET['profid'];
		$modid = $_GET['modid'];
		$tablefieldid = $_GET['tablefieldid'];
		$modulefieldid = $_GET['modulefieldid'];
		$grid=$this->Basefunctions->getpagination();
		$result=$this->Profilemodel->profilefieldvaluegetxmlview($modid,$tablefieldid,$modulefieldid);
        $pageinfo=$this->Basefunctions->page();
		header("Content-type: text/xml;charset=utf-8");
		$s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .=  "<rows>";
        $s .= "<page>".$_GET['page']."</page>";
        $s .= "<total>".$pageinfo['totalpages']."</total>";
        $s .= "<records>".$pageinfo['count']."</records>";
		ob_clean();
		if($tablefieldid != '' && $tablefieldid != 'undefined') {
			$check = $this->Profilemodel->modulefieldvaluecheck($profid,$modid,$modulefieldid);
			if($check == 'TRUE') {
				foreach($result->result() as $row) {
					$newvalue = $this->Profilemodel->fetchfieldvalueidsbasedonprofile($profid,$modid,$modulefieldid);
					$value = explode(',',$newvalue);
					$rowid = $row->fieldid;
					$count = count($value);
					$status = ( (in_array($rowid,$value))? 'true' : 'false');
					$fstatus = ( (in_array($rowid,$value))? 'Assigned' : 'Un Assigned');
					$s .= "<row id='".$row->fieldid."'>";
					$s .= "<cell><![CDATA[".$row->fieldid."]]></cell>";
					$s .= "<cell><![CDATA[".$row->fieldname."]]></cell>";
					$s .= "<cell><![CDATA[".$status."]]></cell>";
					$s .= "<cell><![CDATA[".$fstatus."]]></cell>";
					$s .= "</row>";
				}
			} else {
				foreach($result->result() as $row) {
					$s .= "<row id='".$row->fieldid."'>";
					$s .= "<cell><![CDATA[".$row->fieldid."]]></cell>";
					$s .= "<cell><![CDATA[".$row->fieldname."]]></cell>";
					$s .= "<cell><![CDATA[false]]></cell>";
					$s .= "<cell><![CDATA[Un Assigned]]></cell>";
					$s .= "</row>";
				}
			}
		}
		$s .= "</rows>";
		echo $s;
	}
	//module filed values id insertion
	public function modulevaluesubmit() {
		$this->Profilemodel->modulevaluesubmitmodel();
	}
	//profile unique name check
	public function profileuniquenamecheck() {
		$this->Profilemodel->profileuniquenamecheckmodel();
	}
	//custom toolbar information get
	public function customtoolbarinfoget() {
		$datas = $this->Profilemodel->customtoolbarinfogetmodel();
		echo $datas;
	}
	//sub module custom toolbar information get
	public function subcustomtoolbarinfoget() {
		$datas = $this->Profilemodel->subcustomtoolbarinfogetmodel();
		echo $datas;
	}
	//Module data view
	public function profilebasedsubmodulelist() {
		$profileid = $_GET['pid'];
		$parmoduleid = $_GET['mid'];
		$grid=$this->Basefunctions->getpagination();
		$result=$this->Profilemodel->profilebasedmodulelistxmlview($profileid,$grid['wh']);
		$pageinfo=$this->Basefunctions->page();
		ob_clean();
		header("Content-type: text/xml;charset=utf-8");
		$s = "<?xml version='1.0' encoding='utf-8'?>";
		$s .=  "<rows>";
		$s .= "<page>".$_GET['page']."</page>";
		$s .= "<total>".$pageinfo['totalpages']."</total>";
		$s .= "<records>".$pageinfo['count']."</records>";
		if($profileid!=1) {
			$defmodid = $this->Profilemodel->defaultmoduleidget($grid['wh']);
			$check = $this->Profilemodel->profileidcheckinmoduleinfo('profileid','profilemoduleinfo',$profileid);
			if($check == "TRUE") { //module id is not available
				foreach($result->result() as $row) {
					$moduleid = $row->moduleid;
					$exprofilemodid = explode(',',$moduleid);
					$defdashmodids = $this->Profilemodel->defaultdashmoduleidget($parmoduleid);
					$defdashmodid = explode(',',$defdashmodids);
					$count = count($defdashmodid);
					for($i=0;$i<$count;$i++) {
						$toolbarid = $this->Profilemodel->tollbaridget($defdashmodid[$i],$profileid);//profile based toolbarid
						$moduletoolbarid = $this->Profilemodel->moduletollbaridget($defdashmodid[$i]);//toolbar groupid
						$modulename =  $this->Profilemodel->modulenameget($defdashmodid[$i]);//module name get
						$profileaction = $this->Profilemodel->actionnamefetch($toolbarid,$defmodid);//profile toolbarid separate
						$moduleaction = $this->Profilemodel->actionnamefetch($moduletoolbarid,$defmodid);//module based toolbarid separate
						$actionvalue = $this->Profilemodel->modulebasedactionvalue($profileaction,$moduleaction);
						//check module status
						$modstatus = 'false';
						if(in_array($defdashmodid[$i],$exprofilemodid)) {
							$modstatus = 'true';
							$status = 'Profile Assigned';
						} else {
							$modstatus = 'false';
							$status = 'Profile Un Assigned';
						}
						$s .= "<row id='".$defdashmodid[$i]."'>";
						$s .= "<cell><![CDATA[".$defdashmodid[$i]."]]></cell>";
						$s .= "<cell><![CDATA[".$modulename['catname']."]]></cell>";
						$s .= "<cell><![CDATA[".$modulename['modname']."]]></cell>";
						$s .= "<cell><![CDATA[".$modstatus."]]></cell>";
						$s .= "<cell><![CDATA[".$actionvalue[0]."]]></cell>";
						$s .= "<cell><![CDATA[".$actionvalue[1]."]]></cell>";
						$s .= "<cell><![CDATA[".$actionvalue[2]."]]></cell>";
						$s .= "<cell><![CDATA[".$actionvalue[3]."]]></cell>";
						if($moduleaction[4] != '') {
							$cusstatus = ( ($profileaction[4]!="") ? 'true':'false');
							$s .= "<cell><![CDATA[".$cusstatus."]]></cell>";
						} else {
							$s .= "<cell><![CDATA[empty]]></cell>";
						}
						$s .= "<cell><![CDATA[".$status."]]></cell>";
						$s .= ( ($moduleaction[4]!="" ) ? "<cell><![CDATA[".$profileaction[4]."||".$moduleaction[4]."]]></cell>" : "<cell><![CDATA[ ]]></cell>" );
						$s .= "</row>"; 
					}
				}
			} else if($check == "FALSE") { //module id available
				foreach($result->result() as $row) {
					$toolbarid = $this->Profilemodel->moduletollbaridget($row->moduleid);//tool bar group id
					$value = $this->Profilemodel->actionnamefetch($toolbarid,$defmodid);
					$count = count($value);
					for($i=0;$i<$count;$i++){
						if($value[$i] != ''){
							$actionvalue[$i]='false';
						} else {
							$actionvalue[$i]='empty';
						}
					}
					$status = 'Profile Un Assigned';
					$s .= "<row id='". $row->moduleid."'>";
					$s .= "<cell><![CDATA[".$row->moduleid."]]></cell>";
					$s .= "<cell><![CDATA[".$row->menucategoryname."]]></cell>";
					$s .= "<cell><![CDATA[".$row->modulename."]]></cell>";
					$s .= "<cell><![CDATA[".'false'."]]></cell>";
					$s .= "<cell><![CDATA[".$actionvalue[0]."]]></cell>";
					$s .= "<cell><![CDATA[".$actionvalue[1]."]]></cell>";
					$s .= "<cell><![CDATA[".$actionvalue[2]."]]></cell>";
					$s .= "<cell><![CDATA[".$actionvalue[3]."]]></cell>";
					$s .= "<cell><![CDATA[".$actionvalue[4]."]]></cell>";
					$s .= "<cell><![CDATA[".$status."]]></cell>";
					$s .= "</row>";
				}
			}
		}
		$s .= "</rows>";
		echo $s;
	}
}