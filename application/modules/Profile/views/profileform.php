<style type="text/css">
	.multinavaction-drop.arrow_box{
		left: -61.3594px !important;
	}
	#createprofileadvanceddrop {
	    left: -82.3594px !important;
	}
	.ui-jqgrid-view [type="checkbox"]:not(:checked), .ui-jqgrid-view [type="checkbox"]:checked {
	    position: relative;
	    left: auto;
	    opacity: 1 !important;
	    vertical-align: middle;
	}
	.headerformcaptionstyleforprofile, .headerformcaptionstyle {
	    font-size: 0.87rem;
	    font-weight: 500;
	    padding: 0.2em 0px !important;
	    background: #465a63;
	}
	.ui-jqgrid .ui-jqgrid-hdiv {
	    position: relative;
	    margin: 0;
	    padding: 22px 0px;
	}
	.ui-jqgrid .ui-jqgrid-htable th div {
	    height: 26px;
	    margin-top: -6px;
	}
	.ui-jqgrid .ui-jqgrid-resize-ltr {
	    margin: -10px -2px -2px 0;
	}
	.ui-jqgrid tr:hover {
	    background-color: #fff;
	}
	.ui-widget-content .ui-state-default {
	    font-size: 15px;
	}
</style>
<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<?php
			$device = $this->Basefunctions->deviceinfo();
				$dataset['gridtitle'] = $gridtitle['title'];
				$dataset['titleicon'] = $gridtitle['titleicon'];
				$dataset['moduleid'] = '248';
				$dataset['action'][0] = array('actionid'=>'profileaddicon','actiontitle'=>'Add','actionmore'=>'Yes','ulname'=>'createprofile');
				$dataset['action'][1] = array('actionid'=>'profilefieldsbtbtn','actiontitle'=>'Submit','actionmore'=>'Yes','ulname'=>'module');
				$dataset['action'][2] = array('actionid'=>'profilemodulefieldsbtbtn','actiontitle'=>'Submit','actionmore'=>'Yes','ulname'=>'fields');
				$dataset['action'][3] = array('actionid'=>'profilevaluesubmit','actiontitle'=>'Submit','actionmore'=>'Yes','ulname'=>'fieldsvalue');
				$this->load->view('Base/formwithgridmenuheader.php',$dataset);	
			?>	
			<div class="large-12 columns addformunderheader">&nbsp;</div>
			<div class="large-12 columns scrollbarclass addformcontainer profiletouch">
				<!-- More Action DD start -- Gowtham -->
			<ul id="createprofileadvanceddrop" class="multinavaction-drop arrow_box" style="white-space: nowrap; position: absolute; top: 34.8px; left: -62.1666px; opacity: 1; display: none;">
				<li id="profileedit"><span class="icon-box"><i class="material-icons editiconclass" title="Edit">edit</i>Edit</span></li>
				<li id="profiledelete"><span class="icon-box"><i class="material-icons deleteiconclass" title="Delete">delete</i>Delete</span></li>
				<li id="profilesearch"><span class="icon-box"><i class="material-icons searchiconclass" title="Search">search</i>Search</span></li>
				<li id="groupcloseaddform"><span class="icon-box"><i class="material-icons" title="Close">close</i>Close</span></li>
			</ul>
			<ul id="moduleadvanceddrop" class="multinavaction-drop arrow_box" style="white-space: nowrap; position: absolute; top: 34.8px; left: -62.1666px; opacity: 1; display: none;">
				<li id="profilemodreload"><span class="icon-box"><i class="material-icons reloadiconclass" title="Reload">refresh</i>refresh</span></li>
				<li id="profilemodsearchicon"><span class="icon-box"><i class="material-icons searchiconclass" title="Search">search</i>Search</span></li>
			</ul>
			<ul id="fieldsadvanceddrop" class="multinavaction-drop arrow_box" style="white-space: nowrap; position: absolute; top: 34.8px; left: -62.1666px; opacity: 1; display: none;">
				<li id="profilefieldreload"><span class="icon-box"><i class="material-icons reloadiconclass" title="Reload">refresh</i>Reload</span></li>
			</ul>
			<ul id="fieldsvalueadvanceddrop" class="multinavaction-drop arrow_box" style="white-space: nowrap; position: absolute; top: 34.8px; left: -62.1666px; opacity: 1; display: none;">
				<li id="profilevaluereload"><span class="icon-box"><i class="material-icons reloadiconclass" title="Reload">refresh</i>Reload</span></li>
			</ul>
	<!-- More Action DD End -- Gowtham -->
				<div class="row mblhidedisplay">&nbsp;</div>
				<div id="subformspan1" class="hiddensubform add-update-border padding-space-open-for-form">
				<div class="closed" id="profilesectionoverlay">
				<div class="row sectiontoppadding">&nbsp;</div>
					<div class="large-12 columns">
					<form method="POST" name="profilecreationform" class="" action ="" id="profilecreationform">
						<span id="profileaddvalidation" class="validationEngineContainer" >
							<span id="profileupdatevalidation" class="validationEngineContainer">
								<div class="large-4 columns  paddingbtm large-offset-4 ">
									<div class="large-12 columns cleardataform border-modal-8px z-depth-5" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
										<div class="large-12 columns sectionheaderformcaptionstyle">Basic Details</div>
										<div class="input-field large-12 columns">
											<input id="profilename" class="validate[required,funcCall[profileuniquenamechk],,maxSize[100]]" type="text" data-prompt-position="bottomLeft" tabindex="101" name="profilename">
											<label for="profilename">
												Profile Name<span class="mandatoryfildclass">*</span>
											</label>
										</div>
										<div class="input-field large-12 columns">
											<textarea id="profiledescription" class="materialize-textarea validate[maxSize[200]]" data-prompt-position="topLeft" tabindex="102" name="profiledescription"></textarea>
											<label for="profiledescription">Description</label>
										</div>
										<input type="hidden" name="profileprimaryid" id="profileprimaryid" value=""/>
										<div class="divider large-12 columns"></div>
										<div class="large-12 columns submitkeyboard sectionalertbuttonarea" style="text-align: right">
											<input id="profileaddbtn" class="alertbtn addkeyboard" type="button" value="Submit" name="profileaddbtn" tabindex="103">
											<input id="profileupdatebtn" class="alertbtn updatekeyboard hidedisplay" type="button" value="Submit" name="profileupdatebtn" tabindex="104" style="display: none;">
											<input id="profilecancelbutton" class="alertbtn cancelkeyboard" type="button" value="Cancel" name="profilecancelbutton" tabindex="106">
										</div>
									</div>
								</div>
							</span>
						</span>
					</form>
					</div>
					</div>
					<div class="large-12 columns paddingbtm paddingzero">
						<div class="large-12 columns paddingzero">
							<div class="large-12 columns viewgridcolorstyle borderstyle" style="padding-left:0;padding-right:0;">
								<div class="large-12 columns headerformcaptionstyle" style="height:auto;padding: 0.4rem !important;">
									<span class="large-6 medium-6 small-12 columns lefttext small-only-text-center" style="top:1px !important;">Profile List</span>
								</div>
								<div class="large-12 forgetinggridname columns paddingzero  viewgridcolorstyle " id="profileaddgridwidth">	
									<table id="profileaddgrid"></table>
									<div id="profileaddgridnav"> </div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="subformspan2" class="hidedisplay hiddensubform padding-space-open-for-form">
					<div class="large-12 columns paddingbtm paddingzero">
						<div class="large-12 columns paddingzero">
							<div class="large-12 columns viewgridcolorstyle borderstyle" style="padding-left:0;padding-right:0;">
								<div class="large-12 columns headerformcaptionstyleforprofile" style="height:auto;padding: 0.1rem 0 0;">
									<span class="large-6 medium-6 small-12 columns" style="line-height:1.8;">
										<span class="large-4 medium-5 small-5 columns" style="line-height:2.5">
											Profile :
										</span>
										<span class="large-6 medium-7 small-7 end columns">
											<select class="chzn-select validate[required]" id="moduleprofileid" name="moduleprofileid" data-placeholder="Select" >
												<option value=""></option>
												<?php foreach($profile as $key):?>
													<option value="<?php echo $key->profileid;?>" data-profileid="<?php echo $key->profileid;?>"><?php echo $key->profilename;?></option>
												<?php endforeach;?>
											</select>
										</span>
									</span>
									<span class="large-6 medium-6 small-12 columns innergridicon submitkeyboard righttext small-only-text-center" style="line-height:1.8;position: relative;top: 4px;">
										<span id="moreactionbtn" title="Tools"><i class="material-icons">query_builder</i></span>
										<span id="moremoduleicon"  title="Sub Module"><i class="material-icons">settings</i></span>
									</span>
								</div>
								<div class="large-12 forgetinggridname columns paddingzero  viewgridcolorstyle" id="moduleaddgridwidth">	
									<table id="moduleaddgrid"></table>
									<div id="moduleaddgridnav"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="subformspan3" class="hidedisplay hiddensubform padding-space-open-for-form">
					<div class="large-12 columns paddingbtm paddingzero">
						<div class="large-12 columns paddingzero">
							<div class="large-12 columns viewgridcolorstyle borderstyle" style="padding-left:0;padding-right:0;">
								<div class="large-12 columns headerformcaptionstyleforprofile" style="height:auto;padding: 0.1rem 0 0;">
									<span class="large-5 medium-6 small-12 columns" style="line-height:1.8">
										<span class="large-3 medium-5 small-5 columns" style="line-height:2.5">
											Profile :
										</span>
										<span class="large-6 medium-7 small-7 end columns">
											<select class="chzn-select validate[required]" id="fieldprofileid" name="fieldprofileid" data-placeholder="Select" >
												<option value=""></option>
												<?php foreach($profile as $key):?>
													<option value="<?php echo $key->profileid;?>" data-profileid="<?php echo $key->profileid;?>"><?php echo $key->profilename;?></option>
												<?php endforeach;?>
											</select>
										</span>
									</span>
									<span class="large-5 medium-6 small-12 columns end" style="line-height:1.8;">
										<span class="large-3 medium-5 small-5 columns" style="line-height:2.5">
											Module :
										</span>
										<span class="large-6 medium-7 small-7 end columns">
											<select class="chzn-select validate[required]" id="fieldmoduleid" name="fieldmoduleid" data-placeholder="Select" >
												<option value=""></option>
												<?php foreach($module as $key):
													$name=(($key->menuname!="")?$key->menuname:$key->modulename);
												?>
													<option value="<?php echo $key->moduleid;?>" data-moduleid="<?php echo $key->moduleid;?>"><?php echo $name;?></option>
												<?php endforeach;?>
											</select>
										</span>
									</span>
								</div>
								<div class="large-12 columns forgetinggridname paddingzero  viewgridcolorstyle " id="fieldsaddgridwidth">	
									<table id="fieldsaddgrid"></table>
									<div id="fieldsaddgridnav"> </div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="subformspan4" class="hidedisplay hiddensubform padding-space-open-for-form">
					<div class="large-12 columns paddingbtm paddingzero ">
						<div class="large-12 columns paddingzero ">
							<div class="large-12 columns viewgridcolorstyle paddingzero borderstyle">
								<div class="large-12 columns headerformcaptionstyleforprofile" style="height:auto;padding: 0.1rem 0 0;">
									<span class="large-3 medium-6 small-12 columns" style="line-height:1.8;">
										<span class="large-5 medium-5 small-5 columns" style="line-height:2.5">
											Profile :
										</span>
										<span class="large-6 medium-7 small-7 end columns">
											<select class="chzn-select validate[required]" id="valueprofileid" name="valueprofileid" data-placeholder="Select" >
												<option value=""></option>
												<?php foreach($profile as $key):?>
													<option value="<?php echo $key->profileid;?>" data-profileid="<?php echo $key->profileid;?>"><?php echo $key->profilename;?></option>
												<?php endforeach;?>
											</select>
										</span>
									</span>
									<span class="large-4 medium-6 small-12 columns end" style="line-height:1.8;">
										<span class="large-4 medium-5 small-5 columns" style="line-height:2.5">
											Module :
										</span>
										<span class="large-6 medium-7 small-7 end columns">
											<select class="chzn-select validate[required]" id="valuemoduleid" name="valuemoduleid" data-placeholder="Select" >
												<option value=""></option>
												<?php foreach($module as $key):
													$name=(($key->menuname!="")?$key->menuname:$key->modulename);
												?>
													<option value="<?php echo $key->moduleid;?>" data-moduleid="<?php echo $key->moduleid;?>"><?php echo $name;?></option>
												<?php endforeach;?>
											</select>
										</span>
									</span>
									<span class="large-3 medium-6 small-12 columns end" style="line-height:1.8;">
										<span class="large-5 medium-5 small-5 columns" style="line-height:2.5">
											Fields :
										</span>
										<span class="large-6 medium-7 small-7 end columns">
											<select class="chzn-select validate[required]" id="valuefieldid" name="valuefieldid" data-placeholder="Select" >
												<option value=""></option>
											</select>											
										</span>	
										<input type="hidden" name="selvalueid" id="selvalueid" value="" />
									</span>
								</div>
								<div class="large-12 columns forgetinggridname paddingzero  viewgridcolorstyle" id="fieldvaluesaddgridwidth">	
									<table id="fieldvaluesaddgrid"></table>
									<div id="fieldvaluesaddgridnav"> </div>	
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</div>
</div>
<!--Overlay For More Action Custom Tools-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="customtoolsoverlay" style="overflow-y: scroll;overflow-x: hidden">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-4 medium-10 large-centered medium-centered columns border-modal-8px">
			<form method="POST" name="customactionform" style="" id="customactionform" action="" enctype="" class="">
				<span id="" class="validationEngineContainer">
					<div class="large-12 columns headercaptionstyle headerradius paddingzero">
						<div class="large-6 medium-6 small-6 columns headercaptionleft">
							More Actions
						</div>
						<div class="large-5 medium-6 small-6 columns addformheadericonstyle righttext small-only-text-center">
							<span title="Close" id="cusoverlayclose"><i class="material-icons" title="Close">close</i></span>
						</div>
					</div>
					<div class="large-12 columns paddingzero" style="background:#e0e0e0">
						<div class="row">&nbsp;</div>
						<div class="large-12 columns end paddingbtm">
							<div class="large-12 columns cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;">
								<div class="large-12 columns headerformcaptionstyle">More Action choice</div>
								<div class="large-12 medium-12 small-12 columns end" style="">
									<div class="large-12 columns">
										<input id="checkuncheck"  class="filled-in" type="checkbox" tabindex="" name="checkuncheck" value=""><label for="checkuncheck" style="color:546E7A;"> Check / UnCheck All</label>
									</div>
								</div>
								<div class="large-12 columns customactiondata" style="">
									<!-- custom toolbar icons -->
								</div>
								<div class="large-12 columns" style="">&nbsp;</div>
								<div class="large-12 columns" style="">
									<div class="small-4 medium-4 large-4 columns large-centered medium-centered small-centered">
										<input type="button" class="btn formbuttonsalert" value="Save" name="custactionverlaybtn" id="custactionverlaybtn">	
									</div>
									<div class="medium-4 large-4 columns">&nbsp;</div>
									<div class="large-12 columns">&nbsp;</div>
								</div>
								<div class="row">&nbsp;</div>
								<!-- hidden fields-->
								<input type="hidden" name="modulegriddatarowid" id="modulegriddatarowid" value="" />
								<input type="hidden" name="modulegriddataactionid" id="modulegriddataactionid" value="" />
							</div>
						</div>
				    </div>
				</span>
			</form>
		</div>
	</div>
</div>

<!--Overlay For Sub Modules Grid-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="submoduleoverlay" style="overflow-y: scroll;overflow-x: hidden">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-10 medium-10 large-centered medium-centered columns">
			<div class="row">&nbsp;</div>
			<div class="row border-modal-8px" style="background:#f5f5f5">
				<div class="large-12 columns headercaptionstyle">
					<span class="large-6 medium-6 small-12 columns end">Sub Module
					</span>
					<span class="large-6 medium-6 small-12 columns addformheadericonstyle righttext small-only-text-center">
						<span id="submodmoreactionbtn" class=" editiconclass" title="More Tools"><i class="material-icons">settings</i></span>
						<span id="rolesubmodeulesbt" class="addkeyboard" title="Submit"><i class="material-icons">save</i></span>
						<span id="sbumodoverlayclose" class="" title="Close"><i class="material-icons">close</i></span>
					</span>
				</div>
				<div class="large-12 forgetinggridname columns paddingzero viewgridcolorstyle borderstyle" id="submoduleaddgridwidth">
					<table id="submoduleaddgrid"></table>
					<div id="submoduleaddgridnav"> </div>
				</div>
			</div>
		</div>
		<!-- Hidden fields -->
		<input name="mastermodgridrowid" type="hidden" id="mastermodgridrowid" value="1" />
	</div>
</div>
<!--Overlay For More Action Custom Tools-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="subcustomtoolsoverlay" style="overflow-y: scroll;overflow-x: hidden">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-4 medium-6 large-centered medium-centered columns">
			<form method="POST" name="subcustomactionform" style="" id="subcustomactionform" action="" enctype="" class="">
				<span id="" class="validationEngineContainer">
					<div class="large-12 columns headercaptionstyle headerradius paddingzero">
						<div class="large-6 medium-6 small-6 columns headercaptionleft">
							More Actions
						</div>
						<div class="large-5 medium-6 small-6 columns addformheadericonstyle righttext small-only-text-center">
							<span title="Close" id="subcusoverlayclose"><i class="material-icons" title="Close">close</i></span>
						</div>
					</div>
					<div class="large-12 columns paddingzero" style="background:#e0e0e0">
						<div class="row">&nbsp;</div>
						<div class="large-12 columns end paddingbtm">
							<div class="large-12 columns headerformcaptionstyle">More Action choice</div>
							<div class="large-12 medium-12 small-12 columns end" style="">
								<div class="large-12 columns">
									<input id="subcheckuncheck" class="filled-in" type="checkbox" tabindex="" name="subcheckuncheck" value=""><label for="subcheckuncheck"> Check/UnCheck All</label>
								</div>
							</div>
							<div class="large-12 columns subcustomactiondata" style="">
								<!-- custom toolbar icons -->
							</div>
							<div class="large-12 columns" style="">&nbsp;</div>
							<div class="large-12 columns" style="">&nbsp;</div>
							<div class="large-12 columns" style="">
								<div class="small-4 medium-4 large-4 columns large-centered medium-centered small-centered">
									<input type="button" class="btn formbuttonsalert" value="Save" name="subcustactionverlaybtn" id="subcustactionverlaybtn">	
								</div>
							</div>
							<div class="large-12 columns" style="">&nbsp;</div>
							<div class="large-12 columns" style="">&nbsp;</div>
							<div class="row">&nbsp;</div>
							<!-- hidden fields-->
							<input type="hidden" name="submodulegriddatarowid" id="submodulegriddatarowid" value="" />
							<input type="hidden" name="submodulegriddataactionid" id="submodulegriddataactionid" value="" />
						</div>
					</div>
				</span>
			</form>
		</div>
	</div>
</div>