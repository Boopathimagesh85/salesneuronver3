<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Profilemodel extends CI_Model
{
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//profile based model list
	public function fetchprofilebasedmodulefetchmodel() {
		$profileid = $_GET['pid'];
		$datasets = array();
		$i=0;
		$promodids = $this->db->select('profilemoduleinfo.moduleid',false)->from('profilemoduleinfo')->join('profile','profile.profileid=profilemoduleinfo.profileid')->where('profile.profileid',$profileid)->where('profile.status',1)->where('profilemoduleinfo.status',1)->get();
		foreach($promodids->result() as $prodata) {
			$modids = explode(',',$prodata->moduleid);
			$this->db->select('module.modulename,module.menuname,module.moduleid,menucategory.menucategoryname,menucategory.menucategoryid');
			$this->db->from('module');
			$this->db->join('moduleinfo','moduleinfo.moduleid=module.moduleid');
			$this->db->join('menucategory','menucategory.menucategoryid=module.menucategoryid');
			$this->db->where_in('moduleinfo.moduleid',$modids);
			$this->db->where("FIND_IN_SET('248',module.moduleprivilegeid) >", 0);
			$this->db->where('moduleinfo.status',1);
			$this->db->group_by('moduleinfo.moduleid');
			$this->db->order_by('menucategory.sortorder','ASC');
			$this->db->order_by('module.sortorder','ASC');
			$modresult = $this->db->get();
			foreach($modresult->result() as $data) {
				$name=(($data->menuname!="")?$data->menuname:$data->modulename);
				$datasets[$i] = array('datasid'=>$data->moduleid,'dataname'=>$name,'catename'=>$data->menucategoryname,'cateid'=>$data->menucategoryid);
				$i++;
			}
		}
		echo json_encode($datasets);
	}
	//profile data create model
	public function profileinformationcreatemodel() {
		$profilename = $_POST['profilename'];
		$profiledescription = $_POST['profiledescription'];
		$cdate = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$status	= $this->Basefunctions->activestatus;
		$profile = array(
			'profilename'=>ucwords($profilename),
			'description'=>$profiledescription,
			'industryid'=>$this->Basefunctions->industryid,
			'branchid'=>$this->Basefunctions->branchid,
			'createdate'=>$cdate,
			'lastupdatedate'=>$cdate,
			'createuserid'=>$userid,
			'lastupdateuserid'=>$userid,
			'status'=>$status
		);
		$this->db->insert('profile',$profile);
		$profileid = $this->db->insert_id();
		//update modules and actions to new profile
		{
			//fetch default mod ids
			$dashmodids = array();
			$defmoduleids = $this->defmoduleidgetfornewprofile();
			foreach($defmoduleids as $moduleid) {
				//check dashboard id
				$dashboardmodid = $this->fetchdashboardmoduleid($moduleid);
				$moduleids = ( ($dashboardmodid!=1) ? $dashboardmodid.','.$moduleid : $moduleid );
				$dashmodids[] = $moduleids;
				$modids = explode(',',$moduleids);
				foreach($modids as $modid) {
					//action insertion
					$toolbarids = $this->moduletollbaridget($modid);
					$profactdata = array(
						'profileid'=>$profileid,
						'moduleid'=>$modid,
						'toolbarnameid'=>$toolbarids,
						'createdate'=>$cdate,
						'createuserid'=>$userid,
						'lastupdatedate'=>$cdate,
						'lastupdateuserid'=>$userid,
						'status'=>$status
					);
					$this->db->insert('profilemoduleaction',$profactdata);
					//module fields insertion
					$modfields = $this->modulebasedfieldset($modid);
					foreach($modfields->result() as $fieldset) {
						$fieldvisible = $fieldset->active.','.$fieldset->readonly;
						$modulefieldid = $fieldset->modulefieldid;
						$fielddatasets = array(
							'modulefieldid'=>$modulefieldid,
							'moduleid'=>$modid,
							'profileid'=>$profileid,
							'fieldvisibility'=>$fieldvisible,
							'createdate'=>$cdate,
							'lastupdatedate'=>$cdate,
							'createuserid'=>$userid,
							'lastupdateuserid'=>$userid,
							'status'=>$status
						);
						$this->db->insert('profilemodulefields',$fielddatasets);
						//module field values insertion
						$fieldvalids = $this->modulefieldvalfetch($modid,$modulefieldid);
						$fieldvalues = array(
							'fieldvaluesid'=>$fieldvalids,
							'moduleid'=>$modid,
							'profileid'=>$profileid,
							'modulefieldid'=>$modulefieldid,
							'createdate'=>$cdate,
							'lastupdatedate'=>$cdate,
							'createuserid'=>$userid,
							'lastupdateuserid'=>$userid,
							'status'=>$status
						);
						$this->db->insert('profilemodulevalues',$fieldvalues);
					}
				}
			}
			//profile moduleinfo insertion
			$allmodids = array_merge($dashmodids,$defmoduleids);
			$moduledataids = array_unique($allmodids);
			$moduleidss = implode(',',$moduledataids);
			$profiledata = array(
				'profileid'=>$profileid,
				'moduleid'=>$moduleidss,
				'createdate'=>$cdate,
				'createuserid'=>$userid,
				'lastupdatedate'=>$cdate,
				'lastupdateuserid'=>$userid,
				'status'=>$status
			);
			$this->db->insert('profilemoduleinfo',$profiledata);
		}
		echo "TRUE";
	}
	//profile data update model
	public function profileinformationupdatemodel() {
		$primaryid = $_POST['profileprimaryid'];
		$profilename = $_POST['profilename'];
		$profiledescription = $_POST['profiledescription'];
		$cdate = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$status	= $this->Basefunctions->activestatus;
		$profile = array(
			'profilename'=>ucwords($profilename),
			'description'=>$profiledescription,
			'lastupdatedate'=>$cdate,
			'lastupdateuserid'=>$userid,
			'status'=>$status
		);
		$this->db->where('profile.profileid',$primaryid);
		$this->db->update('profile',$profile);
		echo "TRUE";
	}
	//profile data fetch
	public function profilegriddatafetchxmlview($sidx,$sord,$start,$limit,$wh) {
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('SQL_CALC_FOUND_ROWS  profile.profileid,profile.profilename,profile.description,status.statusname',false);
		$this->db->from('profile');
		$this->db->join('status','status.status=profile.status');
		$this->db->where("FIND_IN_SET('$industryid',profile.industryid) >", 0);
		$this->db->where_not_in('profile.status',array(3,0));
		if($wh != "1") {
			$this->db->where($wh);
		}
		$this->db->order_by($sidx,$sord);
        $this->db->limit($limit,$start);
		$result=$this->db->get();
		return $result;
	}
	//delete profile data
	public function profiledeletefunctionmodel() {
		$primaryid = $_GET['primaryid'];
		$primayname = 'profileid';
		$tablname = 'profile';
		$deldataarr = array('status'=>0);
		$userid = $this->Basefunctions->userid;
		$defdataarr = $this->Crudmodel->updatedefaultvalueget();
		$newdata = array_merge($deldataarr,$defdataarr);
		$this->db->where('createuserid',$userid);
		$this->db->where($primayname,$primaryid);
		$this->db->update($tablname,$newdata);
		$result = $this->db->affected_rows();
		if( $result==0 ) {
			echo "Fail";
		} else if( $result>=0 ){
			echo "True";
		}
	}
	//profile data fetch
	public function profiledatafetchmodel() {
		$primaryid = $_GET['primaryid'];
		$this->db->select('profile.profileid,profile.profilename,profile.description',false);
		$this->db->from('profile');
		$this->db->where('profile.profileid',$primaryid);
		$this->db->where('profile.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('primaryid'=>$row->profileid,'pname'=>$row->profilename,'description'=>$row->description);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//Module data fetch
	public function profilebasedmodulelistxmlview($profileid,$wh) {
		$industryid = $this->Basefunctions->industryid;
		$check = $this->profileidcheckinmoduleinfo('profileid','profilemoduleinfo',$profileid);
		if($check == "FALSE") {
			$this->db->select('module.moduleid,module.modulename,menucategory.menucategoryname');
			$this->db->from('module');
			$this->db->join('moduleinfo','moduleinfo.moduleid=module.moduleid');
			$this->db->join('menucategory','menucategory.menucategoryid=module.menucategoryid');
			$this->db->where_not_in('module.status',array(3,2,0,10));
			$this->db->where_not_in('moduleinfo.status',array(3,2,0,10));
			if($wh != "1") {
				$this->db->where($wh);
			}
			$this->db->order_by('menucategory.sortorder','ASC');
			$this->db->order_by('module.sortorder','ASC');
			$result=$this->db->get();
			return $result;
		} else if($check == "TRUE") {
			$this->db->select('SQL_CALC_FOUND_ROWS profilemoduleinfo.profilemoduleinfoid,profilemoduleinfo.moduleid',false);
			$this->db->from('profilemoduleinfo');
			$this->db->join('module','module.moduleid=profilemoduleinfo.moduleid');
			$this->db->join('menucategory','menucategory.menucategoryid=module.menucategoryid');
			$this->db->where_not_in('profilemoduleinfo.status',array(3,2,0,10));
			$this->db->where_in('profilemoduleinfo.profileid',array($profileid));
			$result=$this->db->get();
			return $result;	 
		}
	}
	//profile id check
	public function profileidcheckinmoduleinfo($tableid,$tablename,$profileid) {
		if($profileid != "") {
			$this->db->select($tableid);
			$this->db->from($tablename);
			$this->db->where($tablename.'.'.$tableid,$profileid);
			$this->db->where($tablename.'.status',1);
			$result = $this->db->get();
			if($result->num_rows() > 0) {
				$data = 'TRUE';
			} else {
				$data = 'FALSE';
			}
		} else {
			$data = 'FALSE'; 
		}
		return $data;
	}
	//tool bar id get
	public function moduletollbaridget($moduleid) {
		$data="";
		$this->db->select('toolbarnameid');
		$this->db->from('toolbargroup');
		$this->db->where_in("toolbargroup.moduleid",$moduleid);
		$this->db->where('toolbargroup.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data =$row->toolbarnameid;
			}
		}
		return $data;
	}
	//default module id get
	public function defaultmoduleidget($wh) {
		$i=0;
		$data = array();
		$industryid=$this->Basefunctions->industryid;
		$this->db->select('moduleid');
		$this->db->from('module');
		$this->db->join('menucategory','menucategory.menucategoryid=module.menucategoryid');
		$this->db->where_not_in('module.status',array(0,2,3));
		$this->db->where("FIND_IN_SET('$industryid',module.industryid) >", 0);
		if($wh != "1") {
			$this->db->where($wh);
		}
		$this->db->order_by('menucategory.sortorder','ASC');
		$this->db->order_by('module.sortorder','ASC');
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = $row->moduleid;
				$i++;
			}
		}
		return $data;
	}
	//default moduleid get for new profile insertion
	public function defmoduleidgetfornewprofile() {
		$i=0;
		$industryid = $this->Basefunctions->industryid;
		$data = array();
		$this->db->select('moduleid');
		$this->db->from('module');
		$this->db->where_in('module.status',array(1,10));
		$this->db->where("FIND_IN_SET('$industryid',industryid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = $row->moduleid;
				$i++;
			}
		}
		return $data;
	}
	//default dahsboard moduleid get
	public function defaultdashmoduleidget($moduleid) {
		$modid = 1;
		if($moduleid!='') {
			$result = $this->db->query('select moduleid,moduledashboardid from module where module.moduleid='.$moduleid.' AND module.moduledashboardid != "" AND module.status NOT IN (10)',false);
			if($result->num_rows() > 0) {
				foreach($result->result() as $row) {
					$modid = $row->moduledashboardid;
				}
			}
		}
		return $modid;
	}
	//module based action value fetch
	public function modulebasedactionvalue($profileaction,$moduleaction) {
		$count = count($profileaction);
		$value = array();
		for($i=0;$i<$count;$i++) {
			if($moduleaction[$i] != '') {
				if($profileaction[$i] == $moduleaction[$i]) {
					$value[$i] = 'true';
				} else{ 
					$value[$i] = 'false';
				}
			} else {
				$value[$i] = 'empty';
			}
		}
		return $value;
	}
	//action name fetch
	public function actionnamefetch($toolbarids,$moduleids) {
		$action = array();
		$toolbarid = explode(',',$toolbarids);
		$count = count($toolbarid);
		$j=0;
		$add='';
		$edit='';
		$delete='';
		$view='';
		$custom=array();
		for($i=0;$i<$count;$i++) {
			$chk = $this->checkmoduleidintoolbar($toolbarid[$i],$moduleids);
			if($chk == 'true') {
				if($toolbarid[$i] == 2) {
					$add = $toolbarid[$i];
				} else if($toolbarid[$i] == 3) {
					$edit = $toolbarid[$i];
				} else if($toolbarid[$i] == 4) {
					$delete = $toolbarid[$i];
				} else if($toolbarid[$i] == 62) {
					$view = $toolbarid[$i];
				} else {
					$custom[$j] = $toolbarid[$i];
					$j++;
				}
			}
		}
		$cusimp = implode(',',$custom);
		$action = array($add,$view,$edit,$delete,$cusimp);
		return $action;
	}
	//check moduleid exit in toolbar
	public function checkmoduleidintoolbar($toolbarid,$moduleids) {
		$chk = "false";
		$modids=array_map('intval',array(1));
		if(is_array($moduleids)) {
			$moduleids[]='1';
			$modids = implode(',',$moduleids);
			$modids = array_map('intval',$moduleids);
		} else {
			$modids = array_map('intval',explode(',',$moduleids));
		}
		$this->db->select("toolbarname.toolbarnameid",false);
		$this->db->from("toolbarname");
		$this->db->join("module","module.moduleid=toolbarname.moduleid");
		$this->db->where("toolbarname.toolbarnameid",$toolbarid);
		$this->db->where_in("toolbarname.moduleid",($modids));
		$this->db->where("toolbarname.status",1);
		$this->db->where_in("module.status",array(1,3));
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			$chk = "true";
		}
		return $chk;
	}
	//Module based Field data fetch
	public function modulebasedfieldslistxmlview($sidx,$sord,$moduleid) {
		$this->db->select('SQL_CALC_FOUND_ROWS  modulefield.modulefieldid,modulefield.fieldlabel,modulefield.readonly,modulefield.active,modulefield.fieldrestrict,modulefield.moduletabid',false);
		$this->db->from('modulefield');
		$this->db->where_in('modulefield.moduletabid',$moduleid);
		$this->db->where_not_in('modulefield.status',array(3,0,10));
		$this->db->order_by($sidx,$sord);
		$result=$this->db->get();
		return $result;
	}
	//Module based Field data fetch for insertion
	public function modulebasedfieldset($moduleid) {
		$this->db->select('modulefield.modulefieldid,modulefield.readonly,modulefield.active',false);
		$this->db->from('modulefield');
		$this->db->where_in('modulefield.moduletabid',$moduleid);
		$this->db->where_not_in('modulefield.status',array(3,0));
		$this->db->order_by('modulefield.modulefieldid','asc');
		$result=$this->db->get();
		return $result;
	}
	//module field value fetch
	public function modulefieldvalfetch($moduleid,$modulefieldid) {
		//fetch table information
		$fieldvalids = '1';
		$this->db->select('modulefield.modulefieldid,modulefield.columnname');
		$this->db->from('modulefield');
		$this->db->where_in('modulefield.uitypeid',array(17,18));
		$this->db->where_in('modulefield.moduletabid',$moduleid);
		$this->db->where('modulefield.modulefieldid',$modulefieldid);
		$this->db->where("FIND_IN_SET('2',modulefield.userroleid) >", 0);
		$this->db->where('modulefield.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$filedval = array('1');
				$i=0;
				$tablefieldid = $row->columnname;
				$table = substr($tablefieldid, 0, -2);
				$tablename = $table.'name';
				$this->db->select("$tablefieldid as fieldid,$tablename as fieldname");
				$this->db->from("$table");
				$this->db->where("FIND_IN_SET('$moduleid',".$table.".moduleid) >", 0);
				$this->db->where($table.".status",1);
				$valresult=$this->db->get();
				foreach($valresult->result() as $valrow) {
					$filedval[$i] = $valrow->fieldid;
					$i++;
				}
				$fieldvalids = implode(',',$filedval);
			}
		}
		return $fieldvalids;
	}
	//fetch role based dashboard moduleid
	public function fetchdashboardmoduleid($moduleid) {
		$modid = 1;
		if($moduleid!='') {
			$result = $this->db->query('select moduleid,moduledashboardid from module where module.moduleid='.$moduleid.' AND module.moduledashboardid != "" AND module.status NOT IN (0,10)',false);
			if($result->num_rows() > 0) {
				foreach($result->result() as $row) {
					$modid = $row->moduledashboardid;
				}
			}
		}
		return $modid;
	}
	//check module field available
	public function checkprofilemodulefield($moduleid,$profileid,$modulefieldid) {
		$this->db->select('profilemodulefieldsid');
		$this->db->from('profilemodulefields');
		$this->db->where_in('profilemodulefields.moduleid',$moduleid);
		$this->db->where('profilemodulefields.profileid',$profileid);
		$this->db->where('profilemodulefields.modulefieldid',$modulefieldid);
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			return 'TRUE';
		} else {
			return 'FALSE';
		}
	}
	//fetch profile module filed info
	public function fetchmodulefieldids($modulefieldid,$profileid,$moduleid) {
		$dataset = array();
		$this->db->select('profilemodulefields.profilemodulefieldsid,profilemodulefields.modulefieldid,profilemodulefields.fieldvisibility',false);
		$this->db->from('profilemodulefields');
		$this->db->join('modulefield','modulefield.modulefieldid=profilemodulefields.modulefieldid');
		$this->db->where_in('profilemodulefields.modulefieldid',$modulefieldid);
		$this->db->where_in('profilemodulefields.moduleid',$moduleid);
		$this->db->where_in('profilemodulefields.profileid',$profileid);
		$this->db->where_not_in('profilemodulefields.status',array(3,0));
		$result=$this->db->get();
		foreach($result->result() as $data) {
			$dataset = array('filedid'=>$data->modulefieldid,'fieldvisible'=>$data->fieldvisibility);
		}
		return $dataset;
	}
	//profile based module check
	public function profilebasedmodulecheck($profileid,$moduleid){
		$this->db->select('moduleid');
		$this->db->from('profilemodulefields');
		$this->db->where_in('profilemodulefields.moduleid',array($moduleid));
		$this->db->where_in('profilemodulefields.profileid',array($profileid));
		$this->db->where('profilemodulefields.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			return "TRUE";
		} else {
			return "False";
		}
	}
	//profile id check
	public function profileidcheck($profileid) {
		$this->db->select('profileid');
		$this->db->from('profilemoduleinfo');
		$this->db->where_in('profilemoduleinfo.profileid',$profileid);
		$this->db->where('profilemoduleinfo.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			return "TRUE";
		} else {
			return "False";
		}
	}
	//profile module action insertion
	public function profilemoduleactioninsertion($moduleid,$profileid,$value){
		$cdate = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$status	= $this->Basefunctions->activestatus;
		$exmod = explode(',',$moduleid);
		$count = count($exmod);
		if($value == 'Yes') {
			for($i=0;$i<$count;$i++) {
				$tolbarid = $this->toolbaridget($exmod[$i],$profileid);
				$actions = array (
					'profileid'=>$profileid,
					'moduleid'=>$exmod[$i],
					'toolbarnameid'=>$tolbarid,
					'createdate'=>$cdate,
					'lastupdatedate'=>$cdate,
					'createuserid'=>$userid,
					'lastupdateuserid'=>$userid,
					'status'=>$status
				);
				$this->db->insert('profilemoduleaction',$actions);
			}
		} else {
			for($i=0;$i<$count;$i++) {
				$actions = array (
					'profileid'=>$profileid,
					'moduleid'=>$exmod[$i],
					'toolbarnameid'=>'',
					'createdate'=>$cdate,
					'lastupdatedate'=>$cdate,
					'createuserid'=>$userid,
					'lastupdateuserid'=>$userid,
					'status'=>$status
				);
				$this->db->insert('profilemoduleaction',$actions);
			}
		}	
	}	
	//profile module action insertion
	public function profilemoduleactionupdate($moduleid,$profileid,$value) {
		$cdate = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$status	= $this->Basefunctions->activestatus;
		$exmod = explode(',',$moduleid);
		$count = count($exmod);
		//module name check
		$modcheck = $this->profilemodulecheck($moduleid,$profileid);
		if($modcheck == 'TRUE') {
			if($value == 'Yes') {
				for($i=0;$i<$count;$i++) {
					$tolbarid = $this->toolbaridget($exmod[$i],$profileid);
					$actions = array (
						'toolbarnameid'=>$tolbarid
					);
					$this->db->where('profilemoduleaction.status',1);
					$this->db->where('profilemoduleaction.moduleid',$exmod[$i]);
					$this->db->where('profilemoduleaction.profileid',$profileid);
					$this->db->update('profilemoduleaction',$actions);
				}
			} else {
				for($i=0;$i<$count;$i++) {
					$tolbarid = $this->toolbaridget($exmod[$i],$profileid);
					$actions = array (
						'toolbarnameid'=>''
					);
					$this->db->where('profilemoduleaction.status',1);
					$this->db->where('profilemoduleaction.moduleid',$exmod[$i]);
					$this->db->where('profilemoduleaction.profileid',$profileid);
					$this->db->update('profilemoduleaction',$actions);
				}
			}
		} else {
			if($value == 'Yes') {
				for($i=0;$i<$count;$i++) {
					$tolbarid = $this->toolbaridget($exmod[$i],$profileid);
					$actions = array (
						'profileid'=>$profileid,
						'moduleid'=>$exmod[$i],
						'toolbarnameid'=>$tolbarid,
						'createdate'=>$cdate,
						'lastupdatedate'=>$cdate,
						'createuserid'=>$userid,
						'lastupdateuserid'=>$userid,
						'status'=>$status
					);
					$this->db->insert('profilemoduleaction',$actions);
				}
			} else {
				for($i=0;$i<$count;$i++) {
					$tolbarid = '';
					$actions = array (
						'profileid'=>$profileid,
						'moduleid'=>$exmod[$i],
						'toolbarnameid'=>'',
						'createdate'=>$cdate,
						'lastupdatedate'=>$cdate,
						'createuserid'=>$userid,
						'lastupdateuserid'=>$userid,
						'status'=>$status
					);
					$this->db->insert('profilemoduleaction',$actions);
				}
			}
		}
	}
	//profile module check
	public function profilemodulecheck($moduleid,$profileid) {
		$this->db->select('moduleid');
		$this->db->from('profilemoduleaction');
		$this->db->where('profilemoduleaction.moduleid',$moduleid);
		$this->db->where('profilemoduleaction.profileid',$profileid);
		$this->db->where('profilemoduleaction.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			return "TRUE";
		} else {
			return "False";
		}
	}
	//tool-bar id get
	public function toolbaridget($moduleid,$profileid) {
		$data="";
		$this->db->select('toolbarnameid');
		$this->db->from('toolbargroup');
		$this->db->where('toolbargroup.moduleid',$moduleid);
		$this->db->where('toolbargroup.status',1);
		$result = $this->db->get();
		if($result->num_rows() >0 ) {
			foreach($result->result() as $row){
				$data = $row->toolbarnameid;
			}
			return $data;
		} else {
			return $data;
		}
	}
	//module name check
	public function  profilebasesinglemodulecheck($moduleid,$profileid) {
		$this->db->select('moduleid');
		$this->db->from('profilemoduleinfo');
		$this->db->where("FIND_IN_SET('$moduleid',profilemoduleinfo.moduleid) >", 0);
		$this->db->where_in('profilemoduleinfo.profileid',$profileid);
		$this->db->where('profilemoduleinfo.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			return "TRUE";
		} else {
			return "False";
		}
	}
	//old moduleid get
	public function oldmoduleidget($profileid) {
		$data = '';
		$this->db->select('moduleid');
		$this->db->from('profilemoduleinfo');
		$this->db->where_in('profilemoduleinfo.profileid',$profileid);
		$this->db->where('profilemoduleinfo.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				$data = $row->moduleid;
			}
			return $data;
		} else {
			return $data;
		}
	}
	//field active/readonly update
	public function modulefieldsmodeupdatemodel() {
		$profile =$_POST['profileid'];
		$modid =$_POST['moduleid'];
		$griddata=$_POST['fieldgriddata'];
		$formdata=json_decode( $griddata, true );
		$count = count($formdata);
		$status = $this->Basefunctions->activestatus;
		$date = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		for( $i=0; $i <$count ; $i++ ) {
			$mid = $formdata[$i]['moduleid'];
			$modulefieldid = $formdata[$i]['modulefieldid'];
			$activemode = "false";
			$activemode = $formdata[$i]['active'];
			$datasets = $this->checkrestrictfieldvisiblestatus($modulefieldid);
			$read = 'No';
			$visible = 'No';
			if( count($datasets)>0 ) {
				$read = $datasets['fread'];
				$visible = $datasets['factive'];
			} else {
				$mvisible = $formdata[$i]['visible'];
				$readonly = $formdata[$i]['readonly'];
				$visible = ($mvisible == 'true'? 'Yes':'No');
				$read = ($readonly == 'true'? 'Yes':'No');
			}
			$fildvisiblemodes = $visible.','.$read;
			if($activemode == "true") {
				//profile module field create / update
				$this->modulefiledmodecreateupdate($profile,$mid,$modulefieldid,$fildvisiblemodes,$date,$userid,$status);
			} else if($activemode == "false") {
				$fielddisable = array('lastupdatedate'=>$date,'lastupdateuserid'=>$userid,'status'=>0);
				$this->db->update('profilemodulefields',$fielddisable,array('modulefieldid'=>$modulefieldid,'moduleid'=>$modid,'profileid'=>$profile));
			}
		}
		//hidden module field create/update
		$datasets = $this->hiddenmodulebasedfieldslist($mid);
		foreach($datasets->result() as $row) {
			$modulefieldid = $row->modulefieldid;
			$fildvisiblemodes = $row->active.','.$row->readonly;
			$this->modulefiledmodecreateupdate($profile,$mid,$modulefieldid,$fildvisiblemodes,$date,$userid,$status);
		}
		echo "TRUE"; 
	}
	//profile module field create / update
	public function modulefiledmodecreateupdate($profile,$mid,$modulefieldid,$fildvisiblemodes,$date,$userid,$status) {
		$check = $this->profileidcheckinmodulefield($profile,$mid,$modulefieldid);
		if($check == 'TRUE') {
			$visiblevalues = array('fieldvisibility'=>$fildvisiblemodes,'lastupdatedate'=>$date,'lastupdateuserid'=>$userid,'status'=>$status);
			$this->db->where('profilemodulefields.modulefieldid',$modulefieldid);
			$this->db->where('profilemodulefields.moduleid',$mid);
			$this->db->where('profilemodulefields.profileid',$profile);
			$this->db->update('profilemodulefields',$visiblevalues);
		} else {
			$visiblevalues = array('modulefieldid'=>$modulefieldid,'moduleid'=>$mid,'profileid'=>$profile,'fieldvisibility'=>$fildvisiblemodes,'createdate'=>$date,'lastupdatedate'=>$date,'createuserid'=>$userid,'lastupdateuserid'=>$userid,'status'=>$status);
			$this->db->insert('profilemodulefields',$visiblevalues);
		}
	}
	//Hidden module fields list
	public function hiddenmodulebasedfieldslist($moduleid) {
		$this->db->select('modulefieldid,readonly,active,fieldrestrict,moduletabid',false);
		$this->db->from('modulefield');
		$this->db->where_in('modulefield.moduletabid',$moduleid);
		$this->db->where_in('modulefield.status',array(10));
		$result=$this->db->get();
		return $result;
	}
	//check restricted field value
	public function checkrestrictfieldvisiblestatus($modfieldid) {
		$data = array();
		$resultset = $this->db->select('active,readonly')->from('modulefield')->where('fieldrestrict','Yes')->where('modulefieldid',$modfieldid)->get();
		if($resultset->num_rows() > 0) {
			foreach($resultset->result() as $row) {
				$data = array('factive'=>$row->active,'fread'=>$row->readonly);
			}
		}
		return $data;
	}
	//module field id check
	public function profileidcheckinmodulefield($profile,$mid,$modfieldid) {
		$this->db->select('modulefieldid');
		$this->db->from('profilemodulefields');
		$this->db->where('profilemodulefields.modulefieldid',$modfieldid);
		$this->db->where('profilemodulefields.moduleid',$mid);
		$this->db->where('profilemodulefields.profileid',$profile);
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			return 'TRUE';
		}else {
			return 'False';
		}
	}
	//module drop down field name fetch
	public function valuedropdownloadfunmodel() {
		$profileid = $_GET['profid'];
		$moduleid = $_GET['modid'];
		$dashboardmodid = $this->fetchdashboardmoduleid($moduleid);
		$moduleids = ( ($dashboardmodid!=1) ? $dashboardmodid : $_GET['modid'] );
		$modid = explode(',',$moduleids);
		$i=0;
		$this->db->select('modulefieldid,columnname,fieldlabel,moduletabid');
		$this->db->from('modulefield');
		$this->db->where_in('modulefield.uitypeid',array(17,18));
		$this->db->where_in('modulefield.moduletabid',$modid);
		$this->db->where('modulefield.userroleid',2);
		$this->db->where('modulefield.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				$data[$i] = array('columnname'=>$row->columnname,'datasid'=>$row->modulefieldid,'dataname'=>$row->fieldlabel,'modtabid'=>$row->moduletabid);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//profile value grid fetch
	public function profilefieldvaluegetxmlview($modid,$tablefiledid,$modulefieldid) {
		$industryid = $this->Basefunctions->industryid;
		if($tablefiledid != '' && $tablefiledid != 'undefined') {
			$table = substr($tablefiledid, 0, -2);
			$tablename = $table.'name';
			$this->db->select("$tablefiledid as fieldid,$tablename as fieldname");
			$this->db->from("$table");
			$this->db->where("FIND_IN_SET('$modid',".$table.".moduleid) >", 0);
			$this->db->where("FIND_IN_SET('$industryid',industryid) >", 0);
			$this->db->where($table.".status",1);
			$result=$this->db->get();
			return $result;
		}
	}
	//check module field values
	public function modulefieldvaluecheck($profid,$modid,$modulefieldid){
		$this->db->select('modulefieldid');
		$this->db->from('profilemodulevalues');
		$this->db->where('profilemodulevalues.modulefieldid',$modulefieldid);
		$this->db->where('profilemodulevalues.moduleid',$modid);
		$this->db->where('profilemodulevalues.profileid',$profid);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				return 'TRUE';
			}
		} else {
			return 'FALSE';
		}
	}
	//module filed values id insertion
	public function modulevaluesubmitmodel() {
		$profleid = $_POST['profid'];
		$moduleid = $_POST['modid'];
		$modulefieldid = $_POST['modulefield'];
		$griddata = $_POST['fieldgriddata'];
		$filedval = array();
		$cdate = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$status	= $this->Basefunctions->activestatus;
		$formdata=json_decode( $griddata, true );
		$count = count($formdata);
		for( $i=0; $i <$count ; $i++ ) {
			$fstatus = $formdata[$i]['status'];
			if($fstatus == 'true') {
				$filedval[$i] = $formdata[$i]['fieldid'];
			}
		}
		$implodefiledval = implode(',',$filedval);
		$check = $this->modulefieldvaluecheck($profleid,$moduleid,$modulefieldid);
		if($check == 'TRUE') {
			$value = array(
				'fieldvaluesid'=>$implodefiledval,
				'lastupdatedate'=>$cdate,
				'lastupdateuserid'=>$userid,
				'status'=>$status
			);
			$this->db->where('profilemodulevalues.status',1);
			$this->db->where('profilemodulevalues.modulefieldid',$modulefieldid);
			$this->db->where('profilemodulevalues.moduleid',$moduleid);
			$this->db->where('profilemodulevalues.profileid',$profleid);
			$this->db->update('profilemodulevalues',$value);
		} else {
			$value = array(
					'fieldvaluesid'=>$implodefiledval,
					'moduleid'=>$moduleid,
					'profileid'=>$profleid,
					'modulefieldid'=>$modulefieldid,
					'createdate'=>$cdate,
					'lastupdatedate'=>$cdate,
					'createuserid'=>$userid,
					'lastupdateuserid'=>$userid,
					'status'=>$status
				);
			$this->db->insert('profilemodulevalues',$value);
		}
		echo 'TRUE';
	}
	//tool bar id get
	public function tollbaridget($modid,$profileid) {
		$data="";
		$this->db->select('toolbarnameid');
		$this->db->from('profilemoduleaction');
		$this->db->join('profilemoduleinfo','profilemoduleinfo.profileid=profilemoduleaction.profileid');
		$this->db->where_in("profilemoduleaction.moduleid",$modid);
		$this->db->where("FIND_IN_SET('$modid',profilemoduleinfo.moduleid) >", 0);
		$this->db->where_in('profilemoduleaction.profileid',$profileid);
		$this->db->where('profilemoduleaction.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				$data =$row->toolbarnameid;
			}
		}
		return $data;
	}
	//module name get
	public function modulenameget($moduleid) {
		$data=array('modname'=>'','catname'=>'');
		$this->db->select('modulename,menuname,menucategory.menucategoryname');
		$this->db->from('module');
		$this->db->join('menucategory','menucategory.menucategoryid=module.menucategoryid');
		$this->db->where_in("module.moduleid",$moduleid);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('modname'=>(($row->menuname == "")? $row->modulename : $row->menuname),'catname'=>$row->menucategoryname);
			}
		}
		return $data;
	}
	//check child module
	public function checkdashboardid($moduleid) {
		$modid = '';
		if($moduleid!='') {
			$result = $this->db->query('select moduledashboardid from module where module.moduleid='.$moduleid.' AND module.moduledashboardid != "" AND module.status NOT IN (0,2,3)',false);
			if($result->num_rows() > 0) {
				foreach($result->result() as $row) {
					$modid = (($row->moduledashboardid != 1)? '*' : '');
				}
			}
		}
		return $modid;
	}
	//fetch fields value ids based on profile
	public function fetchfieldvalueidsbasedonprofile($profid,$modid,$modulefieldid) {
		$data = '';
		$this->db->select('fieldvaluesid');
		$this->db->from('profilemodulevalues');
		$this->db->where('profilemodulevalues.modulefieldid',$modulefieldid);
		$this->db->where('profilemodulevalues.moduleid',$modid);
		$this->db->where('profilemodulevalues.profileid',$profid);
		$this->db->where('profilemodulevalues.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				$data = $row->fieldvaluesid;
			}
		}
		return $data;
	}
	//Profile based module create / update
	public function profilemodulesubmitmodel() {
		$profileid=$_POST['profileid'];
		$rowmoduleid=$_POST['allmoduleid'];
		$griddata=$_POST['modulegriddata'];
		$moduleid = explode(',',$rowmoduleid);
		$selectedmodid = array();
		$formdata=json_decode( $griddata, true );
		$status = $this->Basefunctions->activestatus;
		$date = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		//check module is checked or not
		$modcount = count($moduleid);
		for($k=0;$k<$modcount;$k++) {
			if($formdata[$k]['moduleid'] == 'true'){
				$selectedmodid[$k] = $moduleid[$k];
			}
		}
		$selmodsepid = implode(',',$selectedmodid);
		if($selmodsepid!='') {
			//profile id check
			$check = $this->profileidcheckinmoduleinfo('profileid','profilemoduleaction',$profileid);
			if($check == 'TRUE') {
				//profilemoduleaction update
				for( $i=0; $i <$modcount ; $i++ ) {
					$cmoduleid = $moduleid[$i];
					$cmodulestatus = $formdata[$i]['moduleid'];
					$create = $formdata[$i]['create'];
					$read = $formdata[$i]['read'];
					$update = $formdata[$i]['update'];
					$delete = $formdata[$i]['delete'];
					$custom = $formdata[$i]['custom'];
					$submod = $formdata[$i]['submodules'];
					$submoddata = $formdata[$i]['submodulesdata'];
					$customtoolbar = $formdata[$i]['customactions'];
					$custoolbarid = "";
					if($customtoolbar != "") {
						$toolbaridsets = explode('||',$customtoolbar);
						$custoolbarid = $toolbaridsets[0];
					}
					$moduletoolbarid = $this->moduletollbaridget($cmoduleid);
					$moduleaction = $this->actionnamefetch($moduletoolbarid,$moduleid);
					$usertoolbarid = $this->userrolesetedtollbar($moduleaction,$create,$read,$update,$delete,$custom,$custoolbarid,$cmodulestatus);
					if($cmodulestatus == 'true') {
						$actioncheck = $this->checkfieldvalue('profilemoduleaction','profilemoduleactionid','profileid,moduleid',"".$profileid.",".$cmoduleid."");
						if($actioncheck == 'True') {
							// update action
							$userpri = array('toolbarnameid'=>$usertoolbarid,'status'=>1,'lastupdatedate'=>$date,'lastupdateuserid'=>$userid);
							$this->db->where('profilemoduleaction.profileid',$profileid);
							$this->db->where('profilemoduleaction.moduleid',$cmoduleid);
							$this->db->update('profilemoduleaction',$userpri);
						} else {
							if($usertoolbarid != "") {
								//add new action
								$useraction = array('toolbarnameid'=>$usertoolbarid,'status'=>1,'profileid'=>$profileid,'moduleid'=>$cmoduleid,'createdate'=>$date,'createuserid'=>$userid,'lastupdatedate'=>$date,'lastupdateuserid'=>$userid);
								$this->db->insert('profilemoduleaction',$useraction);
							}
						}
						//insert/update sub modules information
						if($submod == '*' && $submoddata != "") {
							$subformdata=json_decode($submoddata,true);
							$subcount = count($subformdata);
							for( $j=0; $j <$subcount ; $j++ ) {
								$smoduleid = $subformdata[$j]['modid'];
								$scheckmodule = $subformdata[$j]['moduleid'];
								$screate = $subformdata[$j]['create'];
								$sread = $subformdata[$j]['read'];
								$supdate = $subformdata[$j]['update'];
								$sdelete = $subformdata[$j]['delete'];
								$scustom = $subformdata[$j]['custom'];
								$scustomtoolbar = $subformdata[$j]['customactions'];
								$scustoolbarid = "";
								if($scustomtoolbar != "") {
									$stoolbaridsets = explode('||',$scustomtoolbar);
									$scustoolbarid = $stoolbaridsets[0];
								}
								//create / update sub module and toolbar based on profile
								$smoduletoolbarid = $this->moduletollbaridget($smoduleid);
								$smoduleaction = $this->actionnamefetch($smoduletoolbarid,$moduleid);
								$susertoolbarid = $this->userrolesetedtollbar($smoduleaction,$screate,$sread,$supdate,$sdelete,$scustom,$scustoolbarid,$scheckmodule);
								if($scheckmodule == 'true') {
									$selectedmodid[] = $smoduleid;
									$actioncheck = $this->checkfieldvalue('profilemoduleaction','profilemoduleactionid','profileid,moduleid',"".$profileid.",".$smoduleid."");
									if($actioncheck == 'True') {
										// update action
										$protoolbar = array('toolbarnameid'=>$susertoolbarid,'status'=>1,'lastupdatedate'=>$date,'lastupdateuserid'=>$userid);
										$this->db->where('profilemoduleaction.profileid',$profileid);
										$this->db->where('profilemoduleaction.moduleid',$smoduleid);
										$this->db->update('profilemoduleaction',$protoolbar);
									} else {
										if($susertoolbarid != "") {
											//add new action
											$subuseraction = array('toolbarnameid'=>$susertoolbarid,'status'=>1,'profileid'=>$profileid,'moduleid'=>$smoduleid,'createdate'=>$date,'createuserid'=>$userid,'lastupdatedate'=>$date,'lastupdateuserid'=>$userid);
											$this->db->insert('profilemoduleaction',$subuseraction);
										}
									}
								} else {
									//delete action
									$protoolbardel = array('status'=>0,'lastupdatedate'=>$date,'lastupdateuserid'=>$userid);
									$this->db->where('profilemoduleaction.profileid',$profileid);
									$this->db->where('profilemoduleaction.moduleid',$smoduleid);
									$this->db->update('profilemoduleaction',$protoolbardel);
								}
							}
						}
					} else {
						//delete action
						$protooldel = array('status'=>0,'lastupdatedate'=>$date,'lastupdateuserid'=>$userid);
						$this->db->where('profilemoduleaction.profileid',$profileid);
						$this->db->where('profilemoduleaction.moduleid',$cmoduleid);
						$this->db->update('profilemoduleaction',$protooldel);
					}
				}
				//profilemoduleinfo update
				$selmodsepid = implode(',',$selectedmodid);
				$profiledata = array('moduleid'=>$selmodsepid,'lastupdatedate'=>$date,'lastupdateuserid'=>$userid);
				$this->db->where('profilemoduleinfo.profileid',$profileid);
				$this->db->update('profilemoduleinfo',$profiledata);
			} else if($check == 'FALSE') {
				//profilemoduleaction insertion
				for( $i=0; $i <$modcount ; $i++ ) {
					$cmoduleid = $moduleid[$i];
					$cmodulestatus = $formdata[$i]['moduleid'];
					$create = $formdata[$i]['create'];
					$read = $formdata[$i]['read'];
					$update = $formdata[$i]['update'];
					$delete = $formdata[$i]['delete'];
					$custom = $formdata[$i]['custom'];
					$submod = $formdata[$i]['submodules'];
					$submoddata = $formdata[$i]['submodulesdata'];
					$customtoolbar = $formdata[$i]['customactions'];
					$custoolbarid = "";
					if($customtoolbar != "") {
						$toolbaridsets = explode('||',$customtoolbar);
						$custoolbarid = $toolbaridsets[0];
					}
					if($submod!='*') {
						$moduletoolbarid = $this->moduletollbaridget($cmoduleid);
						$moduleaction = $this->actionnamefetch($moduleid);
						$usertoolbarid = $this->userrolesetedtollbar($moduleaction,$create,$read,$update,$delete,$custom,$custoolbarid,$cmodulestatus);
						if($cmodulestatus == 'true' && $usertoolbarid != "") {
							//insert
							$useraction = array('moduleid'=>$cmoduleid,'profileid'=>$profileid,'toolbarnameid'=>$usertoolbarid,'createdate'=>$date,'createuserid'=>$userid,'lastupdatedate'=>$date,'lastupdateuserid'=>$userid,'status'=>$status);
							$this->db->insert('profilemoduleaction',$useraction);
						}
					} else if($submod == '*') {
						if($submod == '*' && $submoddata != "") {
							$subformdata=json_decode($submoddata,true);
							$subcount = count($subformdata);
							for( $j=0; $j <$subcount ; $j++ ) {
								$smoduleid = $subformdata[$j]['modid'];
								$scheckmodule = $subformdata[$j]['moduleid'];
								$screate = $subformdata[$j]['create'];
								$sread = $subformdata[$j]['read'];
								$supdate = $subformdata[$j]['update'];
								$sdelete = $subformdata[$j]['delete'];
								$scustom = $subformdata[$j]['custom'];
								$scustomtoolbar = $subformdata[$j]['customactions'];
								$scustoolbarid = "";
								if($scustomtoolbar != "") {
									$stoolbaridsets = explode('||',$scustomtoolbar);
									$scustoolbarid = $stoolbaridsets[0];
								}
								$smoduletoolbarid = $this->moduletollbaridget($smoduleid);
								$smoduleaction = $this->actionnamefetch($smoduletoolbarid,$moduleid);
								$susertoolbarid = $this->userrolesetedtollbar($smoduleaction,$screate,$sread,$supdate,$sdelete,$scustom,$scustoolbarid,$sscheckmodule);
								if($sscheckmodule == 'true' && $susertoolbarid != "") {
									//insert
									$selectedmodid[] = $smoduleid;
									$useraction = array('moduleid'=>$smoduleid,'profileid'=>$profileid,'toolbarnameid'=>$susertoolbarid,'createdate'=>$date,'createuserid'=>$userid,'lastupdatedate'=>$date,'lastupdateuserid'=>$userid,'status'=>$status);
									$this->db->insert('profilemoduleaction',$useraction);
								}
							}
						}
					}
				}
				//profilemoduleinfo insertion
				$selmodsepid = implode(',',$selectedmodid);
				$profiledata = array('profileid'=>$profileid,'moduleid'=>$selmodsepid,'createdate'=>$date,'createuserid'=>$userid,'lastupdatedate'=>$date,'lastupdateuserid'=>$userid,'status'=>$status);
				$this->db->insert('profilemoduleinfo',$profiledata);
			}
		} else {
				$profiledata = array('moduleid'=>1,'lastupdatedate'=>$date,'lastupdateuserid'=>$userid);
				$this->db->where('profilemoduleinfo.profileid',$profileid);
				$this->db->update('profilemoduleinfo',$profiledata);
		}
		echo 'TRUE';
	}
	//check field value available
	public function checkfieldvalue($tablename,$fields,$whfieldid,$whfiledval) {
		$i=0;
		$whfields = explode(',',$whfieldid);
		$whfieldvals = explode(',',$whfiledval);
		$this->db->select($fields);
		$this->db->from($tablename);
		foreach($whfields as $fileds) {
			$this->db->where("FIND_IN_SET('$whfieldvals[$i]',$fileds) >", 0);
			$i++;
		}
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			return "True";
		} else {
			return "False";
		}
	}
	//user selected action get
	public function userrolesetedtollbar($moduleaction,$create,$read,$update,$delete,$custom,$custoolbarid,$checkmodule) {
		$datasets = "";
		if($checkmodule == 'true') {
			//create
			$caction = ($create == 'true'? $moduleaction[0] : '');
			//read
			$raction = ($read == 'true'? $moduleaction[1] : '');
			//update
			$uaction = ($update == 'true'? $moduleaction[2] : '');
			//delete
			$daction = ($delete == 'true'? $moduleaction[3] : '');
			//custom
			$cusaction = ($custom == 'true'? $custoolbarid : '');
			//total action
			$action = array($caction,$raction,$uaction,$daction,$cusaction); 
			$filter = array_filter($action);
			$datasets = implode(',',$filter);
		}
		return $datasets;
	}
	//unique profile name check
	public function profileuniquenamecheckmodel() {
		$proname = $_POST['name'];
		if($proname != "") {
			$result = $this->db->select('profile.profileid')->from('profile')->where('profilename',$proname)->where('profile.status',1)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->profileid;
				}
			} else { echo "False"; }
		} else { echo "False"; }
	}
	//custom toolbar info generate
	public function customtoolbarinfogetmodel() {
		$moduleids = json_decode($_POST['modids'],true);
		$modids=array_map('intval',array(1));
		if(is_array($moduleids)) {
			$moduleids[]='1';
			$modids = implode(',',$moduleids);
			$modids = array_map('intval',$moduleids);
		} else {
			$modids = array_map('intval',explode(',',$moduleids));
		}
		$toolbars = $_POST['toolbarids'];
		$toolbarids = explode('||',$toolbars);
		$usercustomaction = explode(',',$toolbarids[0]);
		$modcustomaction = array_map('intval', explode(',',$toolbarids[1]));
		$data = "";
		//get toolbar names
		$this->db->select('toolbarname.toolbarnameid,toolbarname.toolbarname,toolbarname.toolbartitle',false);
		$this->db->from('toolbarname');
		$this->db->join('module','module.moduleid=toolbarname.moduleid');
		$this->db->where_in('toolbarname.toolbarnameid',$modcustomaction);
		$this->db->where_in('toolbarname.moduleid',$modids);
		$this->db->where('toolbarname.status',1);
		$this->db->where_in('module.status',array(1,3));
		$toolbarset = $this->db->get();
		foreach($toolbarset->result() as $row) {
			$check = ( (in_array($row->toolbarnameid,$usercustomaction)) ? " checked='checked'" : ""  );
			$data .= '<div class="large-6 medium-6 small-12 columns end">
				<div class="large-12 columns">
					<input id="cusactchkbox'.$row->toolbarnameid.'"'.$check.' type="checkbox" class="checkboxcusact filled-in" tabindex="230" name="cusactchkbox'.$row->toolbarnameid.'" value="'.$row->toolbarnameid.'"><label for="cusactchkbox'.$row->toolbarnameid.'"'.$check.'>'.$row->toolbartitle.'</label>
				</div>
			</div>';
		}
		return json_encode( array('datasets'=>$data,'moduleaction'=>$toolbarids[1]) );
	}
	//sub module custom toolbar info generate
	public function subcustomtoolbarinfogetmodel() {
		$submoduleids = json_decode($_POST['submodids'],true);
		$moduleids = json_decode($_POST['modids'],true);
		$modid = array_merge($moduleids,$submoduleids);
		$modids=array_map('intval',array(1));
		if(is_array($moduleids)) {
			$moduleids[]='1';
			$modids = implode(',',$moduleids);
			$modids = array_map('intval',$moduleids);
		} else {
			$modids = array_map('intval',explode(',',$moduleids));
		}
		$toolbars = $_POST['toolbarids'];
		$toolbarids = explode('||',$toolbars);
		$usercustomaction = explode(',',$toolbarids[0]);
		$modcustomaction = array_map('intval', explode(',',$toolbarids[1]));
		$data = "";
		//get toolbar names
		$this->db->select('toolbarname.toolbarnameid,toolbarname.toolbarname,toolbarname.toolbartitle',false);
		$this->db->from('toolbarname');
		$this->db->join('module','module.moduleid=toolbarname.moduleid');
		$this->db->where_in('toolbarname.toolbarnameid',$modcustomaction);
		$this->db->where_in('toolbarname.moduleid',$modids);
		$this->db->where('toolbarname.status',1);
		$this->db->where_in('module.status',array(1,3));
		$toolbarset = $this->db->get();
		foreach($toolbarset->result() as $row) {
			$check = ( (in_array($row->toolbarnameid,$usercustomaction)) ? " checked='checked'" : ""  );
			$data .= '<div class="large-6 medium-6 small-4 columns end">
				<div class="large-12 columns">
					<input id="cusactchkbox'.$row->toolbarnameid.'"'.$check.' type="checkbox" class="subcheckboxcusact filled-in" tabindex="230" name="cusactchkbox'.$row->toolbarnameid.'" value="'.$row->toolbarnameid.'"><label for="cusactchkbox'.$row->toolbarnameid.'"'.$check.'>'.$row->toolbartitle.'</label>
				</div>
			</div>';
		}
		return json_encode( array('datasets'=>$data,'moduleaction'=>$toolbarids[1]) );
	}
}