<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Sales extends MX_Controller 
{	
	function __construct() {
		parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Base/Basefunctions');
		$this->load->model('Sales/Salesmodel');
		$this->load->model('Itemtag/Itemtagmodel');
		$this->load->model('Printtemplates/Printtemplatesmodel');
	}	
	public function index() {
		
		/* For Old Customer Updation */
		/* $this->Salesmodel->updatediamondcaratweightdetails();
		$this->Salesmodel->updatestonecaratweightdetails();
		$this->Salesmodel->updatesalesdiasummaryinfo();
		print_r('SUCCESS'); die(); */
		
		$moduleid = array(52);
		sessionchecker($moduleid);
		$data['moduleids'] = $moduleid;
		//action
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		//form
		$data['weight_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$data['melting_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',4); //melting round-off
		$data['amount_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$data['rate_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //rate round-off
		$data['dia_weight_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',8);
		$data['salestransactiontype'] = $this->Salesmodel->salestransactiontypedropdown_set();
		$data['salesmode'] = $this->Basefunctions->simpledropdownwithcondorder('salesmodeid','salesmodename','salesmode','','','salesmodeid');
		$data['stocktype'] = $this->Salesmodel->stocktypedropdown();
		$data['paymenttype'] = $this->Salesmodel->paymenttypedropdown();
		$data['purity']=$this->Basefunctions->purity_groupdropdown();
		$data['counter']=$this->Basefunctions->counter_groupdropdown();
		$data['deliverycounter']=$this->Salesmodel->counter_groupdropdown_delivery();
		$data['categoryname']=$this->Salesmodel->category_dropdown();
		$data['deliverystatus'] = $this->Salesmodel->deliverystatus();
		$data['tagtemplate'] = $this->Basefunctions->simpledropdownwithcond('tagtemplateid','tagtemplatename','tagtemplate','moduleid',50);
		$data['paymentcounter']=$this->Basefunctions->counter_groupdropdown_notdefault();
		$data['cashcounter']=$this->Salesmodel->cashcounterdropdownload();
		$data['product'] = $this->Basefunctions->product_dd();
		$data['bullionproduct'] = $this->Salesmodel->bullionproduct_dd();
        $data['product_all'] = $this->Basefunctions->product_dd();
		$data['transactionmode'] = $this->Basefunctions->simpledropdown('transactionmode','transactionmodeid,transactionmodename,setdefault','transactionmodeid',array('moduleid'=>$moduleid));
		$data['ordermode'] = $this->Basefunctions->simpledropdown('ordermode','ordermodeid,ordermodename','ordermodename');
		$data['vendororderitemstatus'] = $this->Basefunctions->simpledropdown('vendororderitemstatus','vendororderitemstatusid,vendororderitemstatusname','vendororderitemstatusname');
		$data['deliverypomode'] = $this->Basefunctions->simpledropdown('deliverypomode','deliverypomodeid,deliverypomodename','deliverypomodeid',array('moduleid'=>$moduleid));
		$data['accounttype'] = $this->Basefunctions->simpledropdownwithcond('accounttypeid','accounttypename','accounttype','accounttypeid','16,6');
		$data['ordervendorid'] = $this->Basefunctions->simpledropdownwithcond('accountid','accountname','account','accounttypeid','16');
		$data['salutation'] = $this->Basefunctions->simpledropdownwithcond('salutationid','salutationname','salutation','moduleid','91');
		$data['paymentaccount'] = $this->Salesmodel->paymentaccountnamefetch('accountid','accountname,accountcatalogid,accounttypeid,mobilenumber','account','accounttypeid','18,19');
		$data['journalaccount'] = $this->Salesmodel->paymentaccountnamefetch('accountid','accountname,accountcatalogid,accounttypeid,mobilenumber','account','accounttypeid','6,16,10,18,19,20');
		$data['tax'] = $this->Basefunctions->simpledropdown('tax','taxid,taxname','taxid');
		$data['stoneentrycalctype'] = $this->Basefunctions->simpledropdown('stoneentrycalctype','stoneentrycalctypeid,stoneentrycalctypename','stoneentrycalctypeid');
		$data['cardtype'] = $this->Basefunctions->simpledropdownwithcond('cardtypeid','cardtypename,cardgroupid','cardtype','moduleid','52');
		$data['delvemployee'] = $this->Basefunctions->simpledropdownwithcond('employeeid','employeename,employeeid','employee','employeetypeid','2');
		$data['account'] = $this->Basefunctions->accountgroup_dd('16,6');
		$userid = $this->Basefunctions->userid;
		$data['employeeid'] = $userid;
		$data['branchid'] =$this->Basefunctions->branchid;
		$data['employeename'] = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$data['calculationby'] = $this->Basefunctions->simpledropdown('calculationby','calculationbyid,calculationbyname','calculationbyid',array('moduleid'=>$moduleid));
		$data['accountgroup']= $this->Basefunctions->simpledropdown('accountgroup','accountgroupid,accountgroupname','accountgroupname');
		$data['printtemplate']= $this->Salesmodel->simpledropdown('printtemplates','printtemplatesid,printtemplatesname,printtypeid,salestransactiontypeid,setasdefault','moduleid','52');
		$data['issuereceipttype']= $this->Salesmodel->simpledropdown('issuereceipttype','issuereceipttypeid,issuereceipttypename,baladvname','moduleid','52');
		$data['stonename'] = $this->Basefunctions->stone_groupdropdown();
		$data['stonestatus'] = $this->Basefunctions->generalinformaion('module','status','moduleid',52); 
		$data['stonecalctype']=$this->Basefunctions->simpledropdown('stonecalctype','stonecalctypeid,netwtcalctype,mccalctype','stonecalctypeid');
		$data['calculationtype']=$this->Basefunctions->simpledropdown('calculationtype','calculationtypeid,calculationtypename','calculationtypename');
		$data['billsalesneumber']=$this->Basefunctions->simpledropdownwithcond('salesid','salesnumber,accountid','sales','salestransactiontypeid','11');
		//$data['paymentbillsalesneumber']=$this->Basefunctions->simpledropdownwithcond('salesid','salesnumber,accountid','sales','salestransactiontypeid','11,9');
		//$data['orderadvancenumber']=$this->Salesmodel->getadvanceordernumber();
		$data['paymentirtype']=$this->Basefunctions->simpledropdownwithcond('paymentirtypeid','paymentirtypeid,paymentirtypename','paymentirtype','moduleid','52');
		$data['creditreference']=$this->Basefunctions->simpledropdownwithcond('creditreferenceid','creditreferenceid,creditreferencename','creditreference','moduleid','52');
		$data['olditemdetails']=$this->Basefunctions->simpledropdown('olditemdetails','olditemdetailsid,olditemdetailsname','olditemdetailsid');
		$data['sizename'] = $this->Itemtagmodel->size_groupdropdown();
		$data['transactionmanageid']=$this->Salesmodel->transactionmanagedd();
		$data['vendoraccount'] = $this->Salesmodel->paymentaccountnamefetch('accountid','accountname','account','accounttypeid','16');
		$data['all_metaldetails'] = $this->Salesmodel->all_metaldetails();
		$data['all_puritydetails'] = $this->Salesmodel->all_puritydetails();
		$data['ordertrackaccdetails'] = $this->Salesmodel->ordertrackingaccountdetailsfetch();
		$data['ordertrackcatgdetails'] = $this->Salesmodel->ordertrackingcategorydetailsfetch();
		//view
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;	
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->vardataviewdropdowncolumns($viewfieldsmoduleids);
		$data['stocksales_license'] = $this->Basefunctions->retrivemodulerule($moduleid[0],'salesform');
		$data['deviceno'] = $this->Basefunctions->get_company_settings('deviceno');
		$data['estimatedefaultaccid'] = $this->Basefunctions->get_company_settings('estimatedefaultaccid');
		$data['estimatedefaultaccname'] = $this->Basefunctions->singlefieldfetch('accountname','accountid','account',$data['estimatedefaultaccid']);
		$data['estimatedefaultacctype'] = $this->Basefunctions->singlefieldfetch('accounttypeid','accountid','account',$data['estimatedefaultaccid']);
		$data['estimatedefaultmobnum'] = $this->Basefunctions->singlefieldfetch('mobilenumber','accountid','account',$data['estimatedefaultaccid']);
		$companysetttingarray = $this->Salesmodel->get_company_settings_sales();
        foreach($companysetttingarray as $companyarray) {		
			//counter-license.
			$data['mainlicense'] = array(
				'counter'=>$companyarray->counter,
				'stone'=>$companyarray->stone,
				'accounttax'=>$companyarray->accounttax,
				'rate_purityid'=>$companyarray->purityid,
				'pureweight'=>$companyarray->pureweight,
				'barcodeoption'=>$companyarray->barcodeoption,
				'bullionproduct'=>$companyarray->bullionproduct,
				'salesreturntype'=>$companyarray->salesreturntypeid,
				'billingtype'=>$companyarray->billingtypeid,
				'chargecalculation'=>$companyarray->chargecalculation,
				'defaultbank'=>$companyarray->defaultbankid,
				'oldjewelproducttype'=>$companyarray->oldjewelproducttypeid,
				'approvaloutcalculation'=>$companyarray->approvaloutcalculation,
				'schememethodid'=>$companyarray->schememethodid,
				'schemetypeid'=>$companyarray->schemetypeid,
				'productidshow'=>$companyarray->productidshow,
				'taxtypeid'=>$companyarray->taxtypeid,
				'discounttypeid'=>$companyarray->discounttypeid,
				'businesschargeid'=>$companyarray->businesschargeid,
				'businesspurchasechargeid'=>$companyarray->businesspurchasechargeid,
				'rateadditiondefault'=>$companyarray->rateadditiondefault,
				'estimateconcept'=>$companyarray->estimateconcept,
				'purchasewastage'=>$companyarray->purchasewastage,
				'salesrate'=>$companyarray->salesrate,
				'purchaserate'=>$companyarray->purchaserate,
				'paymentmodetype'=>$companyarray->paymentmodetype,
				'oldjewelvoucher'=>$companyarray->oldjewelvoucher,
				'autocalculatetax'=>$companyarray->autocalculatetax,
				'discountpasswordstatus'=>$companyarray->discountpassword,
				'discountcalc'=>$companyarray->discountcalc,
				'taxcalc'=>$companyarray->taxcalc,
				'defaultcashinhandid'=>$companyarray->defaultcashinhandid,
				'taxchargeinclude'=>$companyarray->taxchargeinclude,
				'discountauthorization'=>$companyarray->discountauthorization,
				'purewtcalctype'=>$companyarray->purewtcalctype,
				'oldjeweldetails'=>$companyarray->oldjeweldetails,
				'wastagecalctype'=>$companyarray->wastagecalctype,
				'gstapplicable'=>$companyarray->gstapplicable,
				'lottype'=>$companyarray->lot,
				'salesdatechangeablefor'=>$companyarray->salesdatechangeablefor,
				'cashcountershowhide'=>$companyarray->cashcountershowhide,
				'purchasedisplay'=>$companyarray->purchasedisplay,
				'chargerequired'=>$companyarray->chargerequired,
				'taxapplicable'=>$companyarray->taxapplicable,
				'inlinepdfpreview'=>$companyarray->inlinepdfpreview,
				'category_level'=>$companyarray->category_level,
				'netwtcalctypeid'=>$companyarray->netwtcalctypeid,
				'needezetap'=>$companyarray->needezetap,
				'askemailsmsfields'=>$companyarray->askemailsmsfields,
				'needcashcheque'=>$companyarray->needcashcheque,
				'ezetapnotification'=>$companyarray->ezetapnotification,
				'olditemdetails'=>$companyarray->olditemdetails,
				'metalpurity'=>$companyarray->metalpurity,
				'oldjewelvoucheredit'=>$companyarray->oldjewelvoucheredit,
				'roundvaluelimit'=>$companyarray->roundvaluelimit,
				'advancecalc'=>$companyarray->advancecalc,
				'customerwise'=>$companyarray->customerwise,
				'vendorwise'=>$companyarray->vendorwise,
				'cashtenthousand'=>$companyarray->cashtenthousand,
				'lotdetail'=>$companyarray->lotdetail,
				'weightvalidate'=>$companyarray->weightvalidate,
				'oldjewelsumtaxapplyid'=>$companyarray->oldjewelsumtaxapplyid, 
				'orderimageoption'=>$companyarray->orderimageoption, 
				'transactionimageoption'=>$companyarray->transactionimageoption, 
				'weightcheck'=>$companyarray->weightcheck, 
				'loosestonecharge'=>$companyarray->loosestonecharge,
				'approvaloutrfid'=>$companyarray->approvaloutrfid,
				'multiscanmode'=>$companyarray->multiscanmode,
				'allowedaccounttypeset'=>$companyarray->allowedaccounttypeset, 
				'placeorderdefaultaccid'=>$companyarray->placeorderdefaultaccid,
				'purchaseerrorwt'=>$companyarray->purchaseerrorwt,
				'flatweightwastagespan'=>$companyarray->flatweightwastagespan,
				'pancardrestrictamt'=>$companyarray->pancardrestrictamt,
				'cashreceiptlimit'=>$companyarray->cashreceiptlimit,
				'cashissuelimit'=>$companyarray->cashissuelimit,
				'touchchargesedit'=>$companyarray->touchchargesedit,
				'discountbilllevelpercent'=>$companyarray->discountbilllevelpercent,
				'salesauthuserrole'=>$companyarray->salesauthuserrole,
				'untagstoneenable'=>$companyarray->untagstoneenable,
				'multitagbarcode'=>$companyarray->multitagbarcode,
				'untagwtvalidate'=>$companyarray->untagwtvalidate,
				'untagwtshow'=>$companyarray->untagwtshow,
				'estimatequicktotal'=>$companyarray->estimatequicktotal,
				'estimatedefaultset'=>$companyarray->estimatedefaultset,
				'caratwtcalcvalue'=>$companyarray->caratwtcalcvalue,
				'salespaymenthideshow'=>$companyarray->salespaymenthideshow,
				'povendormanagement'=>$companyarray->povendormanagement,
				'takeordercategory'=>$companyarray->takeordercategory,
				'accountypehideshow'=>$companyarray->accountypehideshow,
				'takeorderduedate'=>$companyarray->takeorderduedate,
				'transactioncategoryfield'=>$companyarray->transactioncategoryfield,
				'transactionemployeeperson'=>$companyarray->transactionemployeeperson,
				'poautomaticvendor'=>$companyarray->poautomaticvendor,
				'receiveorderconcept'=>$companyarray->receiveorderconcept,
				'receiveordertemplateid'=>$companyarray->receiveordertemplateid,
				'rejectreviewordertemplateid'=>$companyarray->rejectreviewordertemplateid,
				'accountmobilevalidation'=>$companyarray->accountmobilevalidation,
				'accountmobileunique'=>$companyarray->accountmobileunique,
				'oldjewelsstonedetails'=>$companyarray->oldjewelsstonedetails,
				'chargeconversionfield'=>$companyarray->chargeconversionfield,
				'billamountcalculationtrigger'=>$companyarray->billamountcalculationtrigger,
				'oldjewelsstonecalc'=>$companyarray->oldjewelsstonecalc,
				'purchasemodstonedetails'=>$companyarray->purchasemodstonedetails,
				'discountauthmoduleview'=>$companyarray->discountauthmoduleview,
				'ratedisplaymainview'=>$companyarray->ratedisplaymainview,
				'ratedisplaypurityvalue'=>$companyarray->ratedisplaypurityvalue,
				'salesreturnbillcustomers'=>$companyarray->salesreturnbillcustomers,
				'questionaireapplicable'=>$companyarray->questionaireapplicable,
				'accountfieldcaps'=>$companyarray->accountfieldcaps,
				'stonepiecescalc'=>$companyarray->stonepiecescalc,
				'accountsummaryinfo'=>$companyarray->accountsummaryinfo,
				'paymentsavebutton'=>$companyarray->paymentsavebutton,
				'printtemplatehideshow'=>$companyarray->printtemplatehideshow,
				'oldpurchasecredittype'=>$companyarray->oldpurchasecredittype
			);
		}
		$data['taxapplicable'] = $data['mainlicense']['taxapplicable']; // tax yes no by default
		$data['pdfpreview'] = $data['mainlicense']['inlinepdfpreview'];
		$data['category_level']= $data['mainlicense']['category_level'];
		$gstapplicablestatus = $data['mainlicense']['gstapplicable'];
		if($gstapplicablestatus == 1) { // enable
		  $data['category'] = $this->Basefunctions->simpledropdownwithcond('taxmasterid','taxmastername,gsttaxmodeid','taxmaster','gsttax','Yes');
		} else {
		  $data['category'] = $this->Salesmodel->simpledropdownwithcond('taxmasterid','taxmastername,gsttaxmodeid','taxmaster');
		}
		$data['netwtcalctypeid'] = $data['mainlicense']['netwtcalctypeid']; // Stone Net Wet Cal
		$data['companygsttax'] = $this->Basefunctions->singlefieldfetch('taxmasterid','companyid','company',2);
		$data['transactiontype'] = $this->Basefunctions->singlefieldfetch('salestransactiontypeid','employeeid','employee',$this->Basefunctions->userid);
		$data['planstatus'] = $this->Basefunctions->planid;
		$data['needezetap'] = $data['mainlicense']['needezetap']; // Need EzeTap For transaction 
		$data['askemailsmsfields'] = $data['mainlicense']['askemailsmsfields']; // If we not selected Account, Need to ask Email & Mobile
		$data['needcashcheque'] = $data['mainlicense']['needcashcheque']; // Need Ezetap for Cash & Cheque also.
		$data['ezetapnotification'] = $data['mainlicense']['ezetapnotification']; // Need to use Ezetap notification or we will send by our self.
		$data['purityid']=$this->Basefunctions->purity_groupdropdown();
		$data['branch'] = $this->Basefunctions->simpledropdown('branch','branchid,branchname','branchid');
		$data['country'] = $this->Basefunctions->simpledropdownwithcond('countryid','countryname,countryid','country','moduleid',52);
		$data['state'] = $this->Basefunctions->simpledropdownwithcond('stateid','statename,stateid,countryid','state','moduleid',52);
		$data['employeedata'] = $this->Basefunctions->simpledropdownwithcond('employeeid','employeename,employeenumber','employee','branchid',$this->Basefunctions->branchid);
		$data['salestranscalctype'] = $this->Salesmodel->getcalculationtype('11','2');
		$data['salespaymentcalctype'] = $this->Salesmodel->getcalculationtype('11','3');
		$data['purchasetranscalctype'] = $this->Salesmodel->getcalculationtype('9','2');
		$data['purchasepaymentcalctype'] = $this->Salesmodel->getcalculationtype('9','3');
		$data['purchaseproduct'] = $this->Salesmodel->getpurchaseproduct(); // get purchase product
		$olditemdetails =$data['mainlicense']['olditemdetails'];
		$data['lottype']=$this->Basefunctions->simpledropdownwithcond('lottypeid','lottypename','lottype','moduleid','52');
		$data['purchasemode']=$this->Basefunctions->simpledropdownwithcond('purchasemodeid','purchasemodename','purchasemode','moduleid','52');
		$data['orderitemratefix']=$this->Basefunctions->simpledropdownwithcond('orderitemratefixid','orderitemratefixname','orderitemratefix','moduleid','52');
		$selected = array();
		$values = explode(',',$olditemdetails);
		foreach($values as $value) {
			array_push($selected, $value);
		}
		$data['olditemdata']=$selected;
		$data['olditemdatafirst']=$data['olditemdata'][0];
		$data['loginusercounter'] = $this->Basefunctions->singlefieldfetch('cashcounterid','employeeid','cashcounter',$this->Basefunctions->userid);
		$metalpurity =$data['mainlicense']['metalpurity'];
		$data['metalpuritydata'] = $this->Salesmodel->getmetaldata($metalpurity);
		$this->load->view('Sales/salesview',$data);		
	}	
	//current account balance
	public function currentaccountbalance() {
		$accountid = $_GET['accountid'];
		$salesid = $_GET['salesid'];
		$status = $_GET['status'];
		$array = $this->Salesmodel->currentaccountbalance($accountid,$salesid,$status);
		echo json_encode($array);
	}
	//billing type fetch
	public function billingtypefetch($table,$wherefield,$wherevalue,$retunvalue) {
		$data = '1';
		$industryid = $this->Basefunctions->industryid;
		$this->db->select("$retunvalue",false);
		$this->db->from($table);
		$this->db->where($table.'.'.$wherefield,$wherevalue);
		$this->db->where($table.'.stocktypeid',20);
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {		
			foreach($result->result()as $row) {
				$data=$row->$retunvalue;
			}		
		}
		return $data;
	}
	/**	*sales-create		*/
	public function salescreate() {
		$this->Salesmodel->salescreate(); 
	}
	
	/**	*sales-detailed view	*/
	public function salesdetailedview() {
		$this->Salesmodel->salesdetailedview(); 
	}
	/**	*sales-update	*/
	public function salesupdate() {
		$this->Salesmodel->salesupdate(); 
	}
	/**	*retrieve tag details	*/
	function retrievetagdata() {
		$this->Salesmodel->retrievetagdata(); 
	}
	/**	*returns the stock summary grid pattern	*/
	function getstocksummary_pattern() {
		$this->Salesmodel->getstocksummary_pattern(); 
	}
	/**
	*verify the estimate checkestimatetype is estimate
	*return true on estimate,otherwise false.
	*/	
	public function checkestimatetype() {
		$id = $_GET['salesid'];
		$saledata = $this->db->select('salestransactiontypeid,esttosales')
								->from('sales')
								->where('sales.salesid',$id)
								->get();
		foreach($saledata->result() as $info){
			$salestransactiontypeid = $info->salestransactiontypeid;
			$ets = $info->esttosales;
			if($ets == 'No' and $salestransactiontypeid == '16')	
			{
				echo true;
			}
			else if($ets == 'Yes'  and $salestransactiontypeid == '16')
			{
				echo 'Its already converted to sales';
			}
			else 
			{
				echo false;
			}
		}
	}
	/**	*loadproductaddon - for untag	*/
	public function loadproductaddon() {
		$product = $_GET['productid'];
		$purity = $_GET['purityid'];
		$weight = $_GET['weight'];
		$category = $_GET['categoryid'];
		$purchasedisplay = $_GET['purchasedisplay'];
		$salestransactiontypeid = $_GET['salestransactiontypeid'];
		$stocktypeid = $_GET['stocktypeid'];
		$accountid = $_GET['accountid'];
		$accountypeid = $_GET['accounttype_id'];
		$tagnumber = '';
		$rfidtagnumber = '';
		if(isset($_GET['itemtagnumber'])) {
			$tagnumber = $_GET['itemtagnumber'];
			$rfidtagnumber = '';
		}
		if(isset($_GET['rfidtagnumber'])) {
			$rfidtagnumber = $_GET['rfidtagnumber'];
		} else {
			$rfidtagnumber = '';
		}
		if(isset($_GET['accountid'])) {
			if($_GET['accountid'] == '') {
				$accountgroupid=1;
			} else {
				$accountgroupid=$this->db->select('accountgroupid')->where('accountid',$_GET['accountid'])->get('account')->row()->accountgroupid;
			}
		}
		$untagstockcharges = 0;
		$stockcharges = '';
		if($stocktypeid == 12 || $stocktypeid == 75 || $stocktypeid == 82 || $stocktypeid == 20) { // untag & takeorder
			//$stockcharges = $this->Salesmodel->loadproductaddonfromstockuntag($product,$purity,$weight);
			$untagstockcharges = 1;
			if($stocktypeid == 75) {
				$category = 1;
			}
		}else { 
			if($tagnumber != '' && $rfidtagnumber != ''){
				$stockcharges = $this->Salesmodel->loadproductaddonfromstock($tagnumber,$rfidtagnumber);
				if($stockcharges == 'no') {	
				   $untagstockcharges = 0;
				 }else{
				   $untagstockcharges = 2;
				    $charge_array = $stockcharges;
				 }
			}
		}
		if(($purchasedisplay == '0' || $purchasedisplay == '1') && ($stocktypeid == 17 || $stocktypeid == 80 || $stocktypeid == 81)) {
			$category = 1;
		}
		if($untagstockcharges != 2)  {
			$charge_array = $this->Salesmodel->loadproductcharges($category,$product,$purity,$weight,$accountgroupid,$accountypeid,$salestransactiontypeid);
		}
		echo json_encode($charge_array);
	}
	/**	*account-create		*/
	public function accountcreate()
	{
		$this->Salesmodel->accountcreate(); 
	}
	/**	*account-update		*/
	public function accountupdate()
	{
		$this->Salesmodel->accountupdatemodel(); 
	}
	/**	*loadaccount-json	*/
	public function loadaccount()
	{
		$account = array();
		$data = $this->Basefunctions->accountgroup_dd('16,6,10');
		if($data->num_rows() > 0)
		{
			foreach($data->result() as $info)
			{
				$account[]= array(
									'accounttypeid' => $info->accounttypeid,
									'accountid' => $info->accountid,
									'accountname' => $info->accountname,
									'accounttypename' => $info->accounttypename,
									'mobilenumber' => $info->mobilenumber,
								);
			}
		}
		echo json_encode($account);
	}
	/**	*template file pdf preview	*/
	public function templatefilepdfpreview() 
	{
		$id = trim($_GET['id']);
		$templateid = trim($_GET['templateid']);
		$print_data=array('templateid'=> $templateid, 			'Templatetype'=>'Printtemp','primaryset'=>'sales.salesid','primaryid'=>$id);
		$this->Printtemplatesmodel->generateprinthtmlfile($print_data);
		echo 'SUCCESS';
	}
	/**	*select and return the sales-printtemplateid	*/	
	public function salesprinttemplate() {
		$othertemplateidsarray = array();
		$salesid = trim($_GET['salesid']);
		$salestransactiontypeid = $this->Basefunctions->singlefieldfetch('salestransactiontypeid','salesid','sales',$salesid);;
		$data= $this->db->select('printtemplatesid')
						->from('sales')
						->where('salesid',$salesid)
						->limit(1)
						->get();
		foreach($data->result()  as $info) {
			if($info->printtemplatesid == 1 ||$info->printtemplatesid == '') {
				$template = '';
			} else {
				$template = $info->printtemplatesid;
			}
		}
		echo json_encode(array('id'=>$template));
	}
	/*	* sales printing-template check on pdf-html printing	*/
	public function getprinttemplatemode()
	{
		$templateid = $_GET['templateid'];
		$printtypeid = $this->Basefunctions->singlefieldfetch('printtypeid','printtemplatesid','printtemplates',$templateid);
		$html = 'No';
		if($printtypeid == 3)
		{
			$html = 'Yes';
		}
		else if($printtypeid == 4)
		{
			$html = 'Excel';
		}
		echo $html;
	}
	// Madasamy get sales details
	function retrievesalesdetailsdata()
	{
		$this->Salesmodel->retrievesalesdetailsdata(); 
	}
	// get take orders- of salesdetail
	public function gettakeorder()
	{
		$this->Salesmodel->gettakeorder(); 
	}
	// updates the tag order to manufactured/placeorder on No. -- check it 
	public function updatetagorder()
	{
		$this->Salesmodel->updatetagorder(); 
	}
   	// Madasamy get chtibook details
	function retrievechitbookdetails()
	{
		$this->Salesmodel->retrievechitbookdetails(); 
	}
	function banknameload()
	{
		$this->Salesmodel->banknameload(); 
	}
	public function getbullionpurity()
	{
		$this->Salesmodel->getbullionpurity();
	}
	/* public function stoneentryretrive()
	{
		$this->Salesmodel->stoneentryretrive();
	} */
	public function stoneentryretrive_stone()
	{
		$this->Salesmodel->stoneentryretrive_stone();
	}
	public function accpuritybasednetmccalc()
	{
		$this->Salesmodel->accpuritybasednetmccalc();
	}
	public function getratefromstonename()
	{
		$this->Salesmodel->getratefromstonename();
	}
	public function getaccountnotificationdetails()
	{
		$this->Salesmodel->getaccountnotificationdetails();
	}
	public function checkezetaportstatus()
	{
		$this->Salesmodel->checkezetaportstatus();
	}
	public function createledger()
	{
		$accountname=$this->input->post('accountname');
		$ledgerfromdate=$this->input->post('ledgerfromdate');
		$ledgertodate=$this->input->post('ledgertodate'); 
		$id=$accountname.'.'.$ledgertodate.'.'.$ledgertodate.'?accountledger';
		$this->load->model('studio/printtemplate/printtemplatemodel');
		echo $this->printtemplatemodel->generateprinthtmlfile($id,24);
	}
	
	public function gridinformationfetchbill() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$billstatus=$_GET['filter'];
		$salesid=$_GET['salesid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'sales.salesid') : 'sales.salesid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
	
		$colinfo = array('colmodelname'=>array('salesdate','employeeid','salestransactiontypeid','transactionmodeid','accountid'),'colmodelindex'=>array('sales.salesdate','employee.employeename','salestransactiontype.salestransactiontypename','transactionmode.transactionmodename','account.accountname'),'coltablename'=>array('sales','employee','salestransactiontype','transactionmode','account'),'uitype'=>array('8','19','19','19','19'),'colname'=>array('Sales Date','Employee','Transaction Type','Calculation Type','Account'),'colsize'=>array('120','120','100','100','100'));
	
		$result=$this->Salesmodel->viewdynamicdatainfofetchbill($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$billstatus,$salesid);
		$device = $this->Basefunctions->deviceinfo();
		/* if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Bill',$width,$height);
		} else { */
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Bill',$width,$height);
		//}
		echo json_encode($datas);
	}
	public function retrive()
	{
		$salesdetailid = $_POST['primaryid'];
		$salesid = $_POST['salesid'];
		$data = $this->Salesmodel->retrive($salesid,$salesdetailid);
		echo json_encode($data);
	}
	public function mainsalesdelete()
	{
		$this->Salesmodel->mainsalesdelete();
	}
	public function billretrive()
	{
		$this->Salesmodel->billretrive();
	}
	public function salenumbergenerate()
	{
		$this->Salesmodel->salenumbergenerate();
	}
	public function getsalesdetails()
	{
		$this->Salesmodel->getsalesdetails();
	}
	public function discountadd()
	{
		$this->Salesmodel->discountadd();
	}
	public function localgirdheaderinformationfetch() {
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modname = $_GET['modulename'];
		$data = array();
		$data['fieldname'][0]='stonegroupid';
		$data['fieldlabel'][0]='stonegroupid';
		$data['colmodelname'][0]='stonegroupid';
		$data['colmodelindex'][0]='stonegroupid';
		$data['colmodelviewtype'][0]='1';
		$data['colmodeltype'][0]='text';
		$data['colmodeluitype'][0]='31';
		$data['colmoduleid'][0]='52';
		
		$data['fieldname'][1]='stonegroupname';
		$data['fieldlabel'][1]='stonegroup name';
		$data['colmodelname'][1]='stonegroupname';
		$data['colmodelindex'][1]='stonegroupname';
		$data['colmodelviewtype'][1]='1';
		$data['colmodeltype'][1]='text';
		$data['colmodeluitype'][1]='32';
		$data['colmoduleid'][1]='52';
		
		$data['fieldname'][2]='stoneid';
		$data['fieldlabel'][2]='stoneid';
		$data['colmodelname'][2]='stoneid';
		$data['colmodelindex'][2]='stoneid';
		$data['colmodelviewtype'][2]='1';
		$data['colmodeltype'][2]='text';
		$data['colmodeluitype'][2]='2';
		$data['colmoduleid'][2]='52';
		
		$data['fieldname'][3]='stonename';
		$data['fieldlabel'][3]='stone name';
		$data['colmodelname'][3]='stonename';
		$data['colmodelindex'][3]='stonename';
		$data['colmodelviewtype'][3]='1';
		$data['colmodeltype'][3]='text';
		$data['colmodeluitype'][3]='33';
		$data['colmoduleid'][3]='52';
		
		$data['fieldname'][4]='purchaserate';
		$data['fieldlabel'][4]='purchase rate';
		$data['colmodelname'][4]='purchaserate';
		$data['colmodelindex'][4]='purchaserate';
		$data['colmodelviewtype'][4]='1';
		$data['colmodeltype'][4]='text';
		$data['colmodeluitype'][4]='2';
		$data['colmoduleid'][4]='52';
		
		$data['fieldname'][5]='basicrate';
		$data['fieldlabel'][5]='sales rate';
		$data['colmodelname'][5]='basicrate';
		$data['colmodelindex'][5]='basicrate';
		$data['colmodelviewtype'][5]='1';
		$data['colmodeltype'][5]='text';
		$data['colmodeluitype'][5]='2';
		$data['colmoduleid'][5]='52';
		
		$data['fieldname'][6]='stoneentrycalctypeid';
		$data['fieldlabel'][6]='stoneentrycalctypeid';
		$data['colmodelname'][6]='stoneentrycalctypeid';
		$data['colmodelindex'][6]='stoneentrycalctypeid';
		$data['colmodelviewtype'][6]='1';
		$data['colmodeltype'][6]='text';
		$data['colmodeluitype'][6]='2';
		$data['colmoduleid'][6]='52';
		
		$data['fieldname'][7]='stoneentrycalctypename';
		$data['fieldlabel'][7]='Stone Calc Type';
		$data['colmodelname'][7]='stoneentrycalctypename';
		$data['colmodelindex'][7]='stoneentrycalctypename';
		$data['colmodelviewtype'][7]='1';
		$data['colmodeltype'][7]='text';
		$data['colmodeluitype'][7]='34';
		$data['colmoduleid'][7]='52';
		
		$data['fieldname'][8]='stonepieces';
		$data['fieldlabel'][8]='pieces';
		$data['colmodelname'][8]='stonepieces';
		$data['colmodelindex'][8]='stonepieces';
		$data['colmodelviewtype'][8]='1';
		$data['colmodeltype'][8]='text';
		$data['colmodeluitype'][8]='2';
		$data['colmoduleid'][8]='52';
		
		$data['fieldname'][9]='caratweight';
		$data['fieldlabel'][9]='carat wt';
		$data['colmodelname'][9]='caratweight';
		$data['colmodelindex'][9]='caratweight';
		$data['colmodelviewtype'][9]='1';
		$data['colmodeltype'][9]='text';
		$data['colmodeluitype'][9]='2';
		$data['colmoduleid'][9]='52';
		
		$data['fieldname'][10]='stonegram';
		$data['fieldlabel'][10]='Gram';
		$data['colmodelname'][10]='stonegram';
		$data['colmodelindex'][10]='stonegram';
		$data['colmodelviewtype'][10]='1';
		$data['colmodeltype'][10]='text';
		$data['colmodeluitype'][10]='2';
		$data['colmoduleid'][10]='52';
		
		$data['fieldname'][11]='totalcaratweight';
		$data['fieldlabel'][11]='totalcaratweight';
		$data['colmodelname'][11]='totalcaratweight';
		$data['colmodelindex'][11]='totalcaratweight';
		$data['colmodelviewtype'][11]='1';
		$data['colmodeltype'][11]='text';
		$data['colmodeluitype'][11]='2';
		$data['colmoduleid'][11]='52';
		
		$data['fieldname'][12]='totalweight';
		$data['fieldlabel'][12]='totalweight';
		$data['colmodelname'][12]='totalweight';
		$data['colmodelindex'][12]='totalweight';
		$data['colmodelviewtype'][12]='1';
		$data['colmodeltype'][12]='text';
		$data['colmodeluitype'][12]='2';
		$data['colmoduleid'][12]='52';
		
		$data['fieldname'][13]='stonetotalamount';
		$data['fieldlabel'][13]='total amt';
		$data['colmodelname'][13]='stonetotalamount';
		$data['colmodelindex'][13]='stonetotalamount';
		$data['colmodelviewtype'][13]='1';
		$data['colmodeltype'][13]='text';
		$data['colmodeluitype'][13]='2';
		$data['colmoduleid'][13]='52';
		
		$colinfo=$data;
		$device = $this->Basefunctions->deviceinfo();
		/* if($device=='phone') {
			$datas = mobileviewformtogriddatagenerate($colinfo,$modname,$width,$height);
		} else { */
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height);
		//}
		echo json_encode($datas);
	}
	public function retrive_sales_stoneentry_div()
	{
		$this->Salesmodel->retrive_sales_stoneentry_div();
	}
	public function retrive_sales_taxentry_div()
	{
		$this->Salesmodel->retrive_sales_taxentry_div();
	}
	public function loadapprovalnumber()
	{
		$this->Salesmodel->loadapprovalnumber();
	}
	public function setsalesdetails()
	{
		$this->Salesmodel->setsalesdetails();
	}
	public function loadordernumber()
	{
		$this->Salesmodel->loadordernumber();
	}
	/* tax gird header load */
	public function taxgirdheaderinformationfetch() {
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modname = $_GET['modulename'];
		$moduleid = $_GET['moduleid'];
		$colinfo = $this->Salesmodel->taxgirdheaderinformationfetchmodel($moduleid);
		$device = $this->Basefunctions->deviceinfo();
		/* if($device=='phone') {
			$datas = mobileviewformtogriddatagenerate($colinfo,$modname,$width,$height);
		} else { */
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height);
		//}
		echo json_encode($datas);
	}
	//bill number grid data fetch
	public function billnumbergridgirdheaderinformationfetch() {
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modname = $_GET['modulename'];
		$moduleid = $_GET['moduleid'];
		$checkmultiple = $_GET['checkmultiple'];
		if($checkmultiple == 3) {
			$checkbox = 'true';
		} else {
			$checkbox = 'false';
		}
		$colinfo = $this->Salesmodel->billnumberheaderinformationfetchmodel($moduleid);
		$device = $this->Basefunctions->deviceinfo();
		/* if($device=='phone') {
			$datas = mobileviewformtogriddatagenerate($colinfo,$modname,$width,$height);
		} else { */
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height,$checkbox);
		//}
		echo json_encode($datas);
	}
	//old jewel voucher grid data fetch
	public function oldjewelvouchergridgridgirdheaderinformationfetch() {
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modname = $_GET['modulename'];
		$moduleid = $_GET['moduleid'];
		$checkbox = 'true';
		$colinfo = $this->Salesmodel->oldjewelvoucherheaderinformationfetchmodel($moduleid);
		$device = $this->Basefunctions->deviceinfo();
		/* if($device=='phone') {
			$datas = mobileviewformtogriddatagenerate($colinfo,$modname,$width,$height);
		} else { */
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height,$checkbox);
		//}
		echo json_encode($datas);
	}
	//approval return grid data fetch
	public function approvalreturngridgirdheaderinformationfetch() {
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modname = $_GET['modulename'];
		$moduleid = $_GET['moduleid'];
		$checkbox = $_GET['checkmultiple'];
		$colinfo = $this->Salesmodel->approvalreturnheaderinformationfetchmodel($moduleid);
		$device = $this->Basefunctions->deviceinfo();
		/* if($device=='phone') {
			$datas = mobileviewformtogriddatagenerate($colinfo,$modname,$width,$height);
		} else { */
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height,$checkbox);
		//}
		echo json_encode($datas);
	}
	//issue receipt grid data fetch
	public function issuereceiptgridgirdheaderinformationfetch() {
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modname = $_GET['modulename'];
		$moduleid = $_GET['moduleid'];
		$checkbox = $_GET['checkmultiple'];
		$colinfo = $this->Salesmodel->issuereceiptheaderinformationfetchmodel($moduleid);
		$device = $this->Basefunctions->deviceinfo();
		/* if($device=='phone') {
			$datas = mobileviewformtogriddatagenerate($colinfo,$modname,$width,$height);
		} else { */
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height,$checkbox);
		//}
		echo json_encode($datas);
	}
	//issue receipt grid data fetch
	public function issuereceiptlocalgridgirdheaderinformationfetch() {
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modname = $_GET['modulename'];
		$moduleid = $_GET['moduleid'];
		$colinfo = $this->Salesmodel->issuereceiptheaderinformationfetchmodel($moduleid);
		$device = $this->Basefunctions->deviceinfo();
		/* if($device=='phone') {
			$datas = mobileviewformtogriddatagenerate($colinfo,$modname,$width,$height);
		} else { */
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height);
		//}
		echo json_encode($datas);
	}
	//order grid data fetch
	public function ordergridgirdheaderinformationfetch() {
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modname = $_GET['modulename'];
		$moduleid = $_GET['moduleid'];
		$checkbox = $_GET['checkmultiple'];
		$colinfo = $this->Salesmodel->approvalreturnheaderinformationfetchmodel($moduleid);
		$device = $this->Basefunctions->deviceinfo();
		/* if($device=='phone') {
			$datas = mobileviewformtogriddatagenerate($colinfo,$modname,$width,$height);
		} else { */
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height,$checkbox);
		//}
		echo json_encode($datas);
	}
	//bulk sales grid data fetch
	public function bulksalesgridgirdheaderinformationfetch() {
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modname = $_GET['modulename'];
		$moduleid = $_GET['moduleid'];
		$checkbox = $_GET['checkmultiple'];
		$colinfo = $this->Salesmodel->approvalreturnheaderinformationfetchmodel($moduleid);
		$device = $this->Basefunctions->deviceinfo();
		/* if($device=='phone') {
			$datas = mobileviewformtogriddatagenerate($colinfo,$modname,$width,$height);
		} else { */
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height,$checkbox);
		//}
		echo json_encode($datas);
	}
	/* Fetch Main grid header,data,footer information */
	public function ordergridinformationfetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$account_id=$_GET['filter'];
		$ordertransactionpage=$_GET['ordertransactionpage'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'salesdetail.salesdetailid') : 'salesdetail.salesdetailid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('ordernumber','productname','purityname','grossweight','stoneweight','netweight','pieces','tagimage','orderduedate','poordernumber','secondtouch'),'colmodelindex'=>array('sales.salesnumber','product.productname','purity.purityname','salesdetail.grossweight','salesdetail.stoneweight','salesdetail.netweight','salesdetail.pieces','salesdetail.tagimage','salesdetail.orderduedate','posales.posalesnumber','salesdetail.secondtouch'),'coltablename'=>array('sales','product','purity','salesdetail','salesdetail','salesdetail','salesdetail','salesdetail','salesdetail','posales','salesdetail'),'uitype'=>array('2','2','2','2','2','2','2','2','2','2','2'),'colname'=>array('Order Number','Product Name','Purity','Gross WT','Stone WT','Net WT','Pieces','Image','Order Due date','Place Order Number','Touch'),'colsize'=>array('150','150','110','110','110','110','110','150','150','150','150'));
		$result=$this->Salesmodel->orderviewdynamicdatainfofetch($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$primaryid,$ordertransactionpage);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Orderitem',$width,$height);
		} else {
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Orderitem',$width,$height,'true');
		}
		echo json_encode($datas);
	}
	//tax master load
	public function taxmasterload() {
		$this->Salesmodel->taxmasterload();
	}
	//Bill Tax Master details load
	public function billtaxmasterload() {
		$this->Salesmodel->billtaxmasterload();
	}
	// Manual Tax Calculation to load in Inner Grid
	public function manualtaxmasterload() {
		$this->Salesmodel->manualtaxmasterload();
	}
	//bill bumber grid overlay fetch
	public function billnumbergriddatafetch() {
		$this->Salesmodel->billnumbergriddatafetchmodel();
	}
	//old jewel voucher grid overlay fetch
	public function oldjewelvouchergriddatafetch() {
		$this->Salesmodel->oldjewelvouchergriddatafetchmodel();
	}
	//approval out return grid overlay fetch
	public function approvalreturngriddatafetch() {
		$this->Salesmodel->approvalreturngriddatafetchmodel();
	}
	//approval out return grid overlay fetch
	public function ordergriddatafetch() {
		$this->Salesmodel->ordergriddatafetchmodel();
	}
	//issue/receipt return grid overlay fetch
	public function issuereceiptgriddatafetch() {
		$this->Salesmodel->issuereceiptgriddatafetchmodel();
	}
	//details of old jewel
	public function detailsofoldjewel() {
		$this->Salesmodel->detailsofoldjewelmodel();
	}
	//load bill number based on account
	public function loadbillnumber() {
		$this->Salesmodel->loadbillnumbermodel();
	}
	
	public function stocktypeset() {
		$this->Salesmodel->stocktypeset();
	}
	//pending product fetch based on the purity while loading partial tag
	public function pendingproductnamefetch(){
		$this->Salesmodel->pendingproductnamefetchmodel();
	}
	//pending product fetch based on the purity while loading partial tag
	public function accountgroup_dd(){
		$this->Salesmodel->accountgroup_dd();
	}
	//load-columnname
	public function loadcolumnname(){
		$viewfieldsmoduleids =array_filter(explode(',',$_GET['moduleid']));
		$data=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		echo json_encode($data);
	}
	//show hide fields based on the comapny setting
	public function showhidecommonfields() {
		$this->Salesmodel->showhidecommonfieldsmodel();
	}
	//get cash counter based on the user
	/* public function getcashcounterdata() {
		$this->Salesmodel->getcashcounterdatamodel();
	} */
	//get estimation number data
	public function getestimatedata() {
		$this->Salesmodel->getestimatedatamodel();
	}
	public function loadproductfromstock()
	{
		$this->Salesmodel->loadproductfromstock();
	}
	public function getpurity()
	{
		$this->Salesmodel->getpurity();
	}
	public function getcounter()
	{
		$this->Salesmodel->getcounter();
	}
	public function getweight()
	{
		$this->Salesmodel->getweight();
	}
	public function checkdiscountpassword()
	{
		$this->Salesmodel->checkdiscountpassword();
	}
	public function getdiscountvalue()
	{
		$this->Salesmodel->getdiscountvalue();
	}
	public function generatebillnumber()
	{
		$type = $_POST['type'];
		$transactionmode = $_POST['transactionmode'];
		$num = $this->Salesmodel->generatebillnumber($type,$transactionmode);
		echo json_encode($num);
	}
	//unique name restriction
	public function uniquedynamicviewnamecheck(){
		$this->Salesmodel->uniquedynamicviewnamecheckmodel();
	}
	// sales bill cancel
	public function Salesbillcancel()
	{
		$this->Salesmodel->Salesbillcancel();
	}
	//discount limit fetch
	public function discountlimitfetch() {
		$this->Salesmodel->discountlimitfetchmodel();
	}
	//load take order number
	public function loadtakeordernumber() {
		$this->Salesmodel->loadtakeordernumbermodel();
	}
	/* Fetch Main grid header,data,footer information */
	public function takeordergridinformationfetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$orderid = $_GET['ordernumber'];
		$statusid = $_GET['status'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'sales.salesid') : 'sales.salesid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		if($statusid == '5'){
			$colinfo = array('colid'=>array('1','1','1','1','1','1','1','1','1'),'colname'=>array('Tag Number','Product','Gr Wt','St Wt','Net Wt','Size','Order Status','Statusid','orderno'),'colicon'=>array('','','','','','','','',''),'colsize'=>array('120','250','100','100','100','100','150','50','150'),'colmodelname'=>array('itemtagnumber','innerproductid','innergrosswt','innerstonewt','innernetwt','innersize','innerstatus','innerstatusid','orderno'),'colmodelindex'=>array('itemtag.itemtagnumber','product.productname','salesdetail.grossweight','salesdetail.stoneweight','salesdetail.netweight','sizemaster.sizemastername','salesdetail.orderstatusname','salesdetail.orderstatusid','salesdetail.ordernumber'),'coltablename'=>array('itemtag','product','salesdetail','salesdetail','salesdetail','sizemaster','salesdetail','salesdetail','salesdetail'),'uitype'=>array('2','2','2','2','2','2','2','2','2'),'modname'=>array('Tag Number','Product','Gross Wt','Stone Wt','Net Wt','Size','Order Status','Statusid','orderno'));
		}else {
			$colinfo = array('colid'=>array('1','1','1','1','1','1','1','1','1','1'),'colname'=>array('Order ID','Vendor','Product','Gr Wt','St Wt','Net Wt','Size','Order Status','Statusid','comment'),'colicon'=>array('','','','','','','','','','',''),'colsize'=>array('100','120','200','100','100','100','120','140','100','1000'),'colmodelname'=>array('innersalesdetailid','inneraccountid','innerproductid','innergrosswt','innerstonewt','innernetwt','innersize','innerstatus','innerstatusid','comment'),'colmodelindex'=>array('salesdetail.salesdetailid','account.accountname','product.productname','salesdetail.grossweight','salesdetail.stoneweight','salesdetail.netweight','sizemaster.sizemastername','salesdetail.orderstatusname','salesdetail.orderstatusid','salesdetail.comment'),'coltablename'=>array('salesdetail','account','product','salesdetail','salesdetail','salesdetail','sizemaster','salesdetail','salesdetail','salesdetail'),'uitype'=>array('2','2','2','2','2','2','2','2','2','2'),'modname'=>array('Order ID','Vendor','Product','Gross Wt','Stone Wt','Net Wt','Size','Order Status','Statusid','comment'));
		}
		$result=$this->Salesmodel->takeorderviewdynamicdatainfofetch($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$orderid,$statusid);
		$device = $this->Basefunctions->deviceinfo();
		/* if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'takeorderdetailgrid',$width,$height);
		} else { */
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'takeorderdetailgrid',$width,$height,'true');
		//}
		echo json_encode($datas);
	}
	
	
	//discount auth overlay work
	public function discountauthpasswordget() {
		$this->Salesmodel->discountauthpasswordgetmodel();
	}
	//get balance amt  & details based on credit no
	public function getcreditamt() {
		$this->Salesmodel->getcreditamt();
	}
	
	
	//fetch account data set
	public function fetchaccountdataset() {
		$this->Salesmodel->fetchaccountdatasetmodel();	
	}
	//get pan number
	public function getpannumberbasedonaccount() {
		$accountid = $_POST['accountid'];
		$pannumber = $this->Basefunctions->generalinformaion('account','pannumber','accountid',$accountid);
		echo json_encode($pannumber);
	}
	//get current rate from rate master
	public function getrate() {
		$this->Salesmodel->getrate();
	}
	public function salesdetaillocalgirdheaderinformationfetch() {
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modname = $_GET['modulename'];
		$data = array();
		$this->db->select('localmodulefieldname,fieldlabel,colmodelname,colmodelindex,colmodelviewtype,colmodeluitype,modeid,defaultdata,moduleid',false);
		$this->db->from('localmodulefield');
		$this->db->where('moduleid',52);
		$this->db->where_not_in('status',array(0,3));
		$info = $this->db->get();
		$i = 0;
		if($info->num_rows()>0){
			foreach($info->result() as $salesdata)
			{
				$data['fieldname'][$i]=$salesdata->localmodulefieldname;
				$data['fieldlabel'][$i]=$salesdata->fieldlabel;
				$data['colmodelname'][$i]=$salesdata->colmodelname;
				$data['colmodelindex'][$i]=$salesdata->colmodelindex;
				$data['colmodelviewtype'][$i]=$salesdata->colmodelviewtype;
				$data['colmodeluitype'][$i]=$salesdata->colmodeluitype;
				$data['colmoduleid'][$i]=$salesdata->moduleid;
				$data['modeid'][$i]=$salesdata->modeid;
				$data['defaultdata'][$i]=$salesdata->defaultdata;
				$i++;
			}
		}
		$colinfo=$data;
		$device = $this->Basefunctions->deviceinfo();
		/* if($device=='phone') {
			$datas = mobileviewformtogriddatagenerate($colinfo,$modname,$width,$height);
		} else { */
			$datas = $this->saleslocalviewgridheadergenerate($colinfo,$modname,$width,$height);
		//}
		echo json_encode($datas);
	}
	/* Grid view data generate for local gird [non base modules] */
	function saleslocalviewgridheadergenerate($colinfo,$modulename,$width,$height,$checkbox = 'false') {
		$CI =& get_instance();
		/* width & height set */
		$addcolsize = 0;
		$reccount = count($colinfo['colmodelname']);
		$columnswidth = 5;
		$reccount = 0;
		$active =0;
		foreach ($colinfo['colmodelviewtype'] as $type) {
			
			$columnswidth += $type=='1'? 150 : 0;
			$reccount++;
			if($type == '1') {
				$active++;
			}
		}
		$extrasize = ($columnswidth>$width)?0 : ($width-$columnswidth);
		$addcolsize = $extrasize!=0 ? round($extrasize/$active) : 0;
		$columnswidth = ($columnswidth>$width)?$columnswidth : $width;
		/* height set */
		if($height<=420) {
			$datarowheight = $height-40;
		} else {
			$datarowheight = $height-40;
		}
		/* header generation */
		if($checkbox == 'true') {
			$columnswidth = $columnswidth+35;
		}
		$mdivopen ='<div class="grid-view">';
		$header = '<div class="header-caption" style="width:'.$columnswidth.'px;"><ul class="inline-list">';
		$i=0;
		$m=0;
		$modname = strtolower(preg_replace('/[^A-Za-z0-9]/', '', $modulename));
		
		foreach ($colinfo['fieldlabel'] as $headname) {
			
			if($checkbox == 'true' && $m==0) {
				$header .='<li data-uitype="13" data-fieldname="gridcheckboxfield" data-width="35" data-class="gridcheckboxclass" data-viewtype="1" style="width:35px;" id="'.$modname.'headcheckbox"><input type="checkbox" id="'.$modname.'_headchkbox" name="'.$modname.'_headchkbox" class="'.$modname.'_headchkboxclass headercheckbox" /></li>';
				$m=1;
			}
			$cmodname = strtolower(preg_replace('/[^A-Za-z0-9]/', '',$colinfo['colmodelname'][$i]));
			$cmodindex = strtolower(preg_replace('/[^A-Za-z0-9]/', '',$colinfo['colmodelindex'][$i]));
			$cfield = strtolower(preg_replace('/[^A-Za-z0-9]/', '',$colinfo['fieldname'][$i]));
			if($cfield == 'innergridrowid'){
				$defaultwidth = 70;
			}else{
				$defaultwidth = 150;
			}
			$viewtype = ($colinfo['colmodelviewtype'][$i]=='0') ? ' display:none':'';
			$header .='<li id="'.$modname.'headcol'.$i.'" class="'.$cmodname.'-class '.$modname.'headercolsort" data-class="'.$cmodname.'-class" data-sortcolname="'.$cmodindex.'" data-sortorder="ASC" data-uitype="'.$colinfo['colmodeluitype'][$i].'" data-fieldname="'.$cfield.'" style="width:'.($defaultwidth+$addcolsize).'px;'.$viewtype.'" data-width="'.($defaultwidth+$addcolsize).'" data-viewtype="'.$colinfo['colmodelviewtype'][$i].'" data-modeid="'.$colinfo['modeid'][$i].'" data-defaultdata="'.$colinfo['defaultdata'][$i].'" data-position="'.$i.'">'.$headname.'</li>';
			$i++;
			
		}
		$columnswidth = $columnswidth - 100;
		$header .='</ul></div>';
		$content = '<div class="gridcontent" style="height:'.$datarowheight.'px; overflow:auto;"><div class="data-content wrappercontent" style="width:'.$columnswidth.'px;"></div></div>';
		$mdivclose = '</div>';
		$datas = $mdivopen.$header.$content.$mdivclose;
		$footer = '';
		/* Footer generation */
		$footer = '<div class="summary-blue">';
		$footer .= '</div>';
		return array('content'=>$datas,'footer'=>$footer);
	}
	//fetch salesdetails data set
	public function billeditsalesdetails() {
		$this->Salesmodel->billeditsalesdetails();	
	}
	//check edit data
	public function checkeditsales() {
		$this->Salesmodel->checkeditsales();	
	}
	//get order advance
	public function getorderadvanceamt() {
		$this->Salesmodel->getorderadvanceamt();	
	}
	//get open close amt & weight
	public function loadaccountopenclose() {
		$this->Salesmodel->loadaccountopenclose();	
	}
	//check delete data
	public function checkdeletedata() {
		$this->Salesmodel->checkdeletedata();	
	}
	//check transaction type data
	public function checktransactiontypedata() {
		$this->Salesmodel->checktransactiontypedata();	
	}
	//load item details
	public function loaditemdetail() {
		$this->Salesmodel->loaditemdetail();	
	}
	//push to pending data into itemtag table
	public function pushtopendingdeliverycreate() {
		$this->Salesmodel->pushtopendingdeliverycreate();	
	}
	//load item data
	public function loaditemtag() {
		$this->Salesmodel->loaditemtag();	
	}
	//delivery status update
	public function pushtodeliveryupdate() {
		$this->Salesmodel->pushtodeliveryupdate();	
	}
	//cancel order status update
	public function cancelorderupdate() {
		$this->Salesmodel->cancelorderupdate();	
	}
	//json convert data
	public function jsonconvertdata() {
		$this->Salesmodel->jsonconvertdata();	
	}
	//stone json convert data
	public function stonejsonconvertdata() {
		$this->Salesmodel->stonejsonconvertdata();	
	}
	// Tax grid json convert data
	public function taxgridatajsonconvertdata() {
		$this->Salesmodel->taxgridatajsonconvertdata();	
	}
	// get rfid bulk data
	public function getbulktagnumberdata() {
		$this->Salesmodel->getbulktagnumberdatamodel();	
	}
	// get bulk sales grid data
	public function bulksalesgriddatafetch() {
		$this->Salesmodel->bulksalesgriddatafetchmodel();
	}
	public function getvalidatethetagdata() {
		$this->Salesmodel->getvalidatethetagdata();
	}
	public function clearrfidtable() {
		$this->Salesmodel->clearrfidtable();
	}
	public function appoutuntagupdate() {
		$this->Salesmodel->appoutuntagupdate(array($_POST['salesdetailid']),'2',$_POST['salestransactiontypeid']);
	} 
	public function postatusoverlaysubmitform() {
		$this->Salesmodel->postatusoverlaysubmitform();
	}
	public function deliverypooverlaysave() {
		$this->Salesmodel->deliverypooverlaysave();
	}
	public function checkdeliverypostatus() {
		$this->Salesmodel->checkdeliverypostatus();
	}
	public function getandsetaccountidpo() {
		$this->Salesmodel->getandsetaccountidpo();
	}
	public function loadplaceordernumber() {
		$this->Salesmodel->loadplaceordernumber();
	}
	// get purity based on category
	public function getpuritybasedoncategory() {
		$this->Salesmodel->getpuritybasedoncategory();
	}
	//Update rate based purity
	public function updateratebasedpurity() {
		$this->Salesmodel->updateratebasedpurity();
	}
	//vendor item status grid
	public function vendoritemstatusgridheaderinformationfetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$account_id=$_GET['filter'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'salesdetail.salesdetailid') : 'salesdetail.salesdetailid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('vendororderitemstatusname','lastupdatedate','ordernumber','orderduedate','tagimage','productname','purityname','grossweight','stoneweight','netweight','pieces','poordernumber','vendororderitemstatusid','salesdetailid'),'colmodelindex'=>array('vendororderitemstatus.vendororderitemstatusname','salesdetail.lastupdatedate','sales.salesnumber','salesdetail.orderduedate','salesdetail.tagimage','product.productname','purity.purityname','salesdetail.grossweight','salesdetail.stoneweight','salesdetail.netweight','salesdetail.pieces','posales.posalesnumber','salesdetail.vendororderitemstatusid','salesdetail.salesdetailid'),'coltablename'=>array('vendororderitemstatus','salesdetail','sales','salesdetail','salesdetail','product','purity','salesdetail','salesdetail','salesdetail','salesdetail','posales','salesdetail','salesdetail'),'uitype'=>array('2','2','2','2','2','2','2','2','2','2','2','2','2','2'),'colname'=>array('Vendor Item Status','Item Date Time','Order Number','Order Due date','Image','Product Name','Purity','Gross WT','Stone WT','Net WT','Pieces','Place Order Number','Vendor Item Statusid','Salesdetailid'),'colsize'=>array('150','200','150','150','150','150','110','110','110','110','110','150','110','110'));
		$result=$this->Salesmodel->vendoritemstatusdynamicdatainfofetch($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$primaryid);
		$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Orderitem',$width,$height,'false');
		echo json_encode($datas);
	}
	//vendor product detail information
	public function retrievevendorproductdetailinformation() {
		$this->Salesmodel->retrievevendorproductdetailinformation();
	}
	//Vendor Item Status Submit Call
	public function vendoritemstatusgridsubmitdetails() {
		$this->Salesmodel->vendoritemstatusgridsubmitdetails();
	}
	// Image display in main view grid.
	public function retrievesalesdetailimage() {
		$this->Salesmodel->retrievesalesdetailimage();
	}
	// Check Place Order is rejected by vendor to Reassign new vendor
	public function checkplaceorderisrejected() {
		$this->Salesmodel->checkplaceorderisrejected();
	}
	// Retrieve Rejected Product details
	public function retrieverejectedvendorproductdetailinformation() {
		$this->Salesmodel->retrieverejectedvendorproductdetailinformation();
	}
	// Retrieve Rejected Product Image details
	public function retrieverejectedproductimagedetails() {
		$this->Salesmodel->retrieverejectedproductimagedetails();
	}
	// Update Rejected product details
	public function updaterejectedproductdetails() {
		$this->Salesmodel->updaterejectedproductdetails(); 
	}
	// Retrieve Purity value in form view
	public function retrievelastupdatepurityvalue() {
		$this->Salesmodel->retrievelastupdatepurityvalue();
	}
	// Order Tracking Overlay grid.
	public function ordertrackinggridheaderinformationfetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$accountid = $_GET['accountid'];
		$categoryid = $_GET['categoryid'];
		$accountshortnameid = $_GET['accountshortnameid'];
		$mobilenumber = $_GET['mobilenumber'];
		$ordernumber = $_GET['ordernumber'];
		$ordertrackmainviewclick = $_GET['ordertrackmainviewclick'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$account_id=$_GET['filter'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'salesdetail.salesdetailid') : 'salesdetail.salesdetailid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('salesdate','ordernumber','accountname','orderstatusname','vendororderitemstatusname','generateponame','productname','purityname','grossweight','stoneweight','netweight','pieces','tagimage','vendorname','orderduedate','vendororderduedate'),'colmodelindex'=>array('sales.salesdate','sales.salesnumber','account.accountname','orderstaus.orderstausname','salesdetail.vendororderitemstatusname','salesdetail.generatepo','product.productname','purity.purityname','salesdetail.grossweight','salesdetail.stoneweight','salesdetail.netweight','salesdetail.pieces','salesdetail.tagimage','account.accountname','salesdetail.orderduedate','salesdetail.vendororderduedate'),'coltablename'=>array('sales','sales','sales','salesdetail','salesdetail','salesdetail','product','purity','salesdetail','salesdetail','salesdetail','salesdetail','salesdetail','salesdetail','salesdetail','salesdetail'),'uitype'=>array('2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2'),'colname'=>array('Date','Order Number','Account Name','Order Status','PO Item Status','Generate PO','Product Name','Purity','Gross WT','Stone WT','Net WT','Pieces','Image','Vendor Name','Order Due Date','Vendor Due Date'),'colsize'=>array('200','200','200','250','400','200','200','200','200','200','200','200','200','200','200','200'));
		$result=$this->Salesmodel->ordertrackinggriddynamicdatainfofetch($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$primaryid,$categoryid,$accountid,$accountshortnameid,$mobilenumber,$ordernumber,$ordertrackmainviewclick);
		$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Orderitem',$width,$height,'false');
		echo json_encode($datas);
	}
	// Retrieve all Bill numbers to load
	public function retrieveallbillnumberdetails() {
		$this->Salesmodel->retrieveallbillnumberdetails();
	}
	// Account search - Retrieve state id
	public function accountsearchsubmitretrievestateid() {
		$statename = $_POST['statename'];
		$stateid =  $this->Basefunctions->singlefieldfetch('stateid','statename','state',$statename);
		echo json_encode($stateid);
	}
	//unique name restriction
	public function rfitagnochecksales(){
		$this->Salesmodel->rfitagnochecksales();
	}
	// Display Account Information
	public function getselaccountinformation() {
		$this->Salesmodel->getselaccountinformation();
	}
}