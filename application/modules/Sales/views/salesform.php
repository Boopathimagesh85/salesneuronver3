<?php
	$device = $this->Basefunctions->deviceinfo();
	$loggedinuserroleid = $this->Basefunctions->userroleid;
	$dataset['gridtitle'] = $gridtitle;
	$dataset['titleicon'] = $titleicon;
	$this->load->view('Base/mainformwithgridmenuheader.php',$dataset);
	$defaultrecordview = $this->Basefunctions->get_company_settings('mainviewdefaultview');
	echo hidden('mainviewdefaultview',$defaultrecordview);
	$transactioncategoryfield = $this->Basefunctions->get_company_settings('transactioncategoryfield');
	$chitbooktypeid = $this->Basefunctions->get_company_settings('chitbooktype');
?>
<style type="text/css">
	.headerformcaptionstyle span {
	    line-height: 1.5rem;
	    margin-top: 2px;
	    margin-right: 0px !important;
	}
	.testingsh {
	    color: blue;
	}
	.stoneweighticoncls {
	margin-top:-5px !important;
	}
	#itemdiscountautnicon {
		margin-top:-5px !important;
	}
	#cdicon {
			margin-top:-5px !important;
	}
	.validatewtfontize{
		 font-size: 0.65rem;
	}
	.transactiontypehead {
		font-weight:900 !important;
		text-transform: uppercase;
		color:red !important;
	}
	<?php if($device=='phone') {?>
	.twelevewidth {
	
	}
	.thirtytwowidth {
	
	}
	.fourteenwidth {
	
	}
	.eightenwidth {
	
	}
	.seventywidth {
	width:70% !important;
	}
	<?php } else {?>
	.twelevewidth {
	width:12% !important;
	}
	.thirtytwowidth {
	width:32% !important;
	}
	.fourteenwidth {
	width:14% !important;
	}
	.eightenwidth {
	width:18% !important;
	}
	.seventywidth {
	width:70% !important;
	}
	.paddingbtm {
	padding-top: 0.4em !important;
	}
	<?php }?>
</style>
<div class="large-12 columns addformunderheader">&nbsp;</div>
<div class="large-12 columns addformcontainer" style="overflow-y:auto;height: 600px !important;">
	<div class="row mblhidedisplay">&nbsp;</div>
	<!-- -->	
	<input type ="hidden" id="pdfviewtype" value='<?php echo $pdfpreview;?>'/>
	<input type ="hidden" id="stocktypeproperty" value='<?php echo json_encode($stocksales_license);?>'/>
	
	<input type ="hidden" id="category_level" value="<?php echo $category_level;?>"/>	
	<input type ="hidden" id="currentaccountbalance"/>	
	<input type ="hidden" id="getstocksummary_pattern"/>	
	<input type ="hidden" id="getpaymentsummary_pattern"/>	
	<input type ="hidden" id="hiddenemployeename" value="<?php echo $employeename?>" />
	<input type ="hidden" id="hiddenemployeeid" value="<?php echo $employeeid?>"/>
	<input type ="hidden" id="hiddenbranchid" value="<?php echo $branchid?>"/>
	<input type ="hidden" id="hiddenplanstatus" value="<?php echo $planstatus?>"/>
	<!--billbalance from stocksummarygrid-->
	<input type ="hidden" id="billbalanceamount"/>	
	<input type ="hidden" id="billbalancepureweight"/>	
	<input type ="hidden" id="billbalancesalesnetweight"/>
	<!--livebalance with sign-->
	<input type ="hidden" id="currentbalanceamounthide"/>	
	<input type ="hidden" id="currentbalancepureweighthide"/>	
	<input type ="hidden" id="currentbalancesalesnetweighthide"/>
	<!--billbalance-on stock after payment-->
	<input type ="hidden" id="finalbillbalanceamount"/>	
	<input type ="hidden" id="finalbillbalancepureweight"/>	
	<input type ="hidden" id="finalbillbalancesalesnetweight"/>	
	<input type ="hidden" id="accountbalancestatus"/>	
	<!--   -->	
	<input type ="hidden" id="roundweight" value="<?php echo $weight_round?>"/>	
	<input type ="hidden" id="roundmelting" value="<?php echo $melting_round?>"/>	
	<input type ="hidden" id="roundamount" value="<?php echo $amount_round?>"/>
	<input type ="hidden" id="dia_weight_round" value="<?php echo $dia_weight_round?>"/>
	<input type="hidden" id="netwtcalid" name="netwtcalid" value="<?php echo $netwtcalctypeid; ?>">
	<?php if(strtolower($mainlicense['stone']) == 'yes' && $planstatus >= 4) {$stoneyesno=1; }else{ $stoneyesno=0;  } ?>
	<input type="hidden" id="hiddenstoneyesno" name="hiddenstoneyesno" value="<?php echo $stoneyesno; ?>">
	<input type ="hidden" id="needezetap" value='<?php echo $needezetap;?>'/>	
	<input type ="hidden" id="askemailsmsfields" value='<?php echo $askemailsmsfields;?>'/>		
	<input type ="hidden" id="needcashcheque" value='<?php echo $needcashcheque;?>'/>	
	<input type ="hidden" id="ezetapnotification" value='<?php echo $ezetapnotification;?>'/>
	<input type ="hidden" id="ezeaccmobileno" value=''/>
	<input type ="hidden" id="ezeaccemail" value=''/>	
	<input type="hidden" id="transactiontype" name="transactiontype" value="<?php echo $transactiontype; ?>">	
	<input type ="hidden" id="portstatus" value=''/>	
	<input type="hidden" id="accounttaxstatus" name="accounttaxstatus" value="<?php echo strtolower($mainlicense['accounttax']) ?>">
	<input type="hidden" id="rate_purityid" name="rate_purityid" value="<?php echo $mainlicense['rate_purityid']; ?>">
	<input type="hidden" id="rate_mode_purewt" name="rate_mode_purewt" value="">
	<input type="hidden" id="companytaxapplicable" name="companytaxapplicable" value="<?php echo  $taxapplicable; ?>">
	<input type="hidden" id="generatesalesid" name="generatesalesid" value="">
	<input type="hidden" id="salesbasedproductid" name="salesbasedproductid" value="">
	<input type="hidden" id="generatesalesnumber" name="generatesalesnumber" value="">
	<input type="hidden" id="generateitemtagid" name="generateitemtagid" value="">
	<input type="hidden" id="savedbillstatus" name="savedbillstatus" value="">
	<input type="hidden" id="salesreturntype" name="salesreturntype" value="<?php echo $mainlicense['salesreturntype']; ?>">
	<input type="hidden" id="billingtype" name="billingtype" value="<?php echo $mainlicense['billingtype']; ?>">
	<input type="hidden" id="gridcolnames" name="gridcolnames" value="stonegroupid,stonegroupname,stoneid,stonename,purchaserate,basicrate,stonepieces,caratweight,stonegram,totalcaratweight,totalweight,stonetotalamount">
	<input type="hidden" id="gridcoluitype" name="gridcoluitype" value="31,32,2,33,2,2,2,2,2,2,2,2">
	<input type="hidden" id="olditemfirst" name="olditemfirst" value="<?php echo $olditemdatafirst; ?>">
	<input type="hidden" id="melting" name="melting">
	<input type="hidden" id="bullionproduct" name="bullionproduct" value="<?php echo $mainlicense['bullionproduct']; ?>">
	<input type="hidden" id="purchaseproduct" name="purchaseproduct" value="<?php echo $purchaseproduct; ?>">
	<input type="hidden" id="tagnumber" name="tagnumber" value="">
	<input type="hidden" id="chargecalculation" name="chargecalculation" value="<?php echo $mainlicense['chargecalculation']; ?>">
	<input type="hidden" id="defaultbank" name="defaultbank" value="<?php echo $mainlicense['defaultbank']; ?>">
	<input type="hidden" id="oldjewelproducttype" name="oldjewelproducttype" value="<?php echo $mainlicense['oldjewelproducttype']; ?>">
	<input type="hidden" id="defultcounter" name="defultcounter" value="<?php echo $mainlicense['counter']; ?>">
	<input type="hidden" id="approvaloutcalculation" name="approvaloutcalculation" value="<?php echo $mainlicense['approvaloutcalculation']; ?>">
	<input type="hidden" id="grosspureweight" name="grosspureweight" value="">
	<input type="hidden" id="pregrossamount" name="pregrossamount" value="">
	<!--Sales/Purchase caluclation type-->
	<input type ="hidden" id="salestranscalctype" name="salestranscalctype" value="<?php echo $salestranscalctype; ?>"/>	
	<input type ="hidden" id="salespaymentcalctype" name="salespaymentcalctype" value="<?php echo $salespaymentcalctype; ?>"/>	
	<input type ="hidden" id="purchasetranscalctype" name="purchasetranscalctype" value="<?php echo $purchasetranscalctype; ?>"/>
	<input type ="hidden" id="purchasepaymentcalctype" name="purchasepaymentcalctype" value="<?php echo $purchasepaymentcalctype; ?>"/>
	<input type ="hidden" id="schememethodid" name="schememethodid" value="<?php echo $mainlicense['schememethodid']; ?>"/>
	<input type ="hidden" id="schemetypeid" name="schemetypeid" value="<?php echo $mainlicense['schemetypeid']; ?>"/>
	<input type ="hidden" id="ratearray" name="ratearray" value=''/>
	<input type ="hidden" id="productidshow" name="productidshow" value='<?php echo $mainlicense['productidshow']; ?>'/>
	<input type ="hidden" id="taxtypeid" name="taxtypeid" value='<?php echo $mainlicense['taxtypeid']; ?>'/>
	<input type ="hidden" id="sdiscounttypeid" name="sdiscounttypeid" value=''/>
	<input type ="hidden" id="discountdisplayid" name="discountdisplayid" value=''/>
	<input type ="hidden" id="businesschargeid" name="businesschargeid" value='<?php echo $mainlicense['businesschargeid']; ?>'/>
	<input type ="hidden" id="businesspurchasechargeid" name="businesspurchasechargeid" value='<?php echo $mainlicense['businesspurchasechargeid']; ?>'/>
	<input type ="hidden" id="modeidtype" name="modeidtype" value=''/>
	<input type ="hidden" id="rateadditiondefault" name="rateadditiondefault" value='<?php echo $mainlicense['rateadditiondefault']; ?>'/>
	<input type ="hidden" id="purchasewastage" name="purchasewastage" value='<?php echo $mainlicense['purchasewastage']; ?>'/>
	<input type ="hidden" id="logincashcounterid" name="logincashcounterid" value='<?php echo $loginusercounter; ?>'/>
	<input type ="hidden" id="salesrate" name="salesrate" value='<?php echo $mainlicense['salesrate']; ?>'/>
	<input type ="hidden" id="comppurchaserate" name="comppurchaserate" value='<?php echo $mainlicense['purchaserate']; ?>'/>
	<input type ="hidden" id="metalpurityarray" name="metalpurityarray" value='<?php echo json_encode($metalpuritydata); ?>'/>
	<input type="hidden" id="stocknetweight" name="stocknetweight">
	<input type="hidden" id="stockgrossweight" name="stockgrossweight">
	<input type="hidden" id="stockpieces" name="stockpieces">
	<input type="hidden" id="paymentstocknetweight" name="stocknetweight">
	<input type="hidden" id="paymentstockgrossweight" name="stockgrossweight">
	<input type="hidden" id="paymentstockpieces" name="stockpieces">
	<input type="hidden" id="issuereceiptid" name="issuereceiptid">
	<input type="hidden" id="wtissuereceiptid" name="wtissuereceiptid">
	<input type ="hidden" id="paymentmodestatus" name="paymentmodestatus" value='<?php echo $mainlicense['paymentmodetype']; ?>'/>
	<input type ="hidden" id="oldjewelvoucherstatus" name="oldjewelvoucherstatus" value='<?php echo $mainlicense['oldjewelvoucher']; ?>'/>
	<input type ="hidden" id="autocalculatetax" name="autocalculatetax" value='<?php echo $mainlicense['autocalculatetax']; ?>'/>
	<input type="hidden" id="taxgriddata" name="taxgriddata">
	<input type="hidden" id="summarytaxgriddata" name="summarytaxgriddata">
	<input type="hidden" id="hiddentaxcategoryid" name="hiddentaxcategoryid">
	<input type="hidden" id="discountpasswordstatus" name="discountpasswordstatus" value='<?php echo $mainlicense['discountpasswordstatus']; ?>'>
	<input type="hidden" id="discountvaluehidden" name="discountvaluehidden" value=''>
	<input type="hidden" id="discountcalc" name="discountcalc" value='<?php echo $mainlicense['discountcalc']; ?>'>
	<input type="hidden" id="taxcalc" name="taxcalc" value='<?php echo $mainlicense['taxcalc']; ?>'>
	<input type="hidden" id="defaultcashinhandid" name="defaultcashinhandid" value='<?php echo $mainlicense['defaultcashinhandid']; ?>'>
	<input type="hidden" id="taxchargeinclude" name="taxchargeinclude" value='<?php echo $mainlicense['taxchargeinclude']; ?>'>
	<input type="hidden" id="discountauthorization" name="discountauthorization" value='<?php echo $mainlicense['discountauthorization']; ?>'>
	<input type="hidden" id="purewtcalctype" name="purewtcalctype" value='<?php echo $mainlicense['purewtcalctype']; ?>'>
	<input type="hidden" id="oldjeweldetails" name="oldjeweldetails" value='<?php echo $mainlicense['oldjeweldetails']; ?>'>
	<input type="hidden" id="wastagecalctype" name="wastagecalctype" value='<?php echo $mainlicense['wastagecalctype']; ?>'>
	<input type="hidden" id="gstapplicable" name="gstapplicable" value='<?php echo $mainlicense['gstapplicable']; ?>'>
	<input type="hidden" id="companygsttax" name="companygsttax" value='<?php echo $companygsttax; ?>'>
	<input type="hidden" id="salesdatechangeablefor" name="salesdatechangeablefor" value='<?php echo $mainlicense['salesdatechangeablefor']; ?>'>
	<input type="hidden" id="openingamount" name="openingamount" value=''>
	<input type="hidden" id="roundwholevalue" name="roundwholevalue" value=''>
	<input type="hidden" id="primarydataid" name="primarydataid">
	<input type="hidden" id="discaltype" name="discaltype" value="">
	<input type="hidden" id="discamt" name="discamt" value="">
	<input type="hidden" id="lottype" name="lottype" value='<?php echo $mainlicense['lottype']; ?>'>
	<input type="hidden" id="purchasedisplay" name="purchasedisplay" value='<?php echo $mainlicense['purchasedisplay']; ?>'><input type="hidden" id="oldjewelsumtaxapplyid" name="oldjewelsumtaxapplyid" value='<?php echo $mainlicense['oldjewelsumtaxapplyid']; ?>'> 
	<input type="hidden" id="partialgrossweight" name="partialgrossweight">
	<input type="hidden" id="estimatenoarray" name="estimatenoarray">
	<input type="hidden" id="srbillid" name="srbillid">
	<input type="hidden" id="userroleid" name="userroleid" value="<?php echo $loggedinuserroleid; ?>">
	<input type ="hidden" id="hiddensalesmanid" value=""/>
	<input type="hidden" id="takeordersalesdetailid" value=""/>
	<input type="hidden" id="takeordernumber" value=""/>	
	<input type="hidden" id="mainordernumber" value=""/>	
	<input type="hidden" id="mainordernumberid" value=""/>
	<input type="hidden" id="orderadvanceno" value=""/>
	<input type="hidden" id="prepaymenttotalamount" value=""/>
	<input type="hidden" id="prepaymenttotalwt" value="0"/>
	<input type="hidden" id="chargerequired" value="<?php echo $mainlicense['chargerequired']; ?>"/>
	<input type="hidden" id="prediscountamount" value="0"/>
	<input type="hidden" id="estimatearray" value=""/>
	<input type="hidden" id="discountarray" value=""/>
	<input type="hidden" id="presumarydiscountamount" value=""/>
	<input type="hidden" id="hiddentaxdata" value=""/>
	<input type="hidden" id="editpaymenttotalamount" value="0"/>
	<input type="hidden" id="editinnerrowid" value=""/>
	<input type="hidden" id="hiddenstonedetail" value=""/>
	<input type="hidden" id="creditfinalamt" value=""/>
	<input type="hidden" id="billsumarydiscountlimit" value=""/>
	<input type="hidden" id="billdiscamt" value=""/>
	<input type="hidden" id="oldjewelvoucheredit" value="<?php echo $mainlicense['oldjewelvoucheredit']; ?>"/>
	<input type="hidden" id="roundvaluelimit" value="<?php echo $mainlicense['roundvaluelimit']; ?>"/>
	<input type="hidden" id="advancecalc" value="<?php echo $mainlicense['advancecalc']; ?>"/>
	<input type="hidden" id="customerwise" value="<?php echo $mainlicense['customerwise']; ?>"/>
	<input type="hidden" id="vendorwise" value="<?php echo $mainlicense['vendorwise']; ?>"/>
	<input type="hidden" id="cashtenthousand" value="<?php echo $mainlicense['cashtenthousand']; ?>"/>
	<input type="hidden" id="approvalouttagnumberarray" value=""/>
	<input type="hidden" id="approvaloutrfidnumberarray" value=""/>
	<input type="hidden" id="prepaymentpureweight" value="0"/>
	<input type="hidden" id="editpaymentpureweight" value="0"/>
	<input type="hidden" id="orderadvamtsummaryhidden" value="0"/>
	<input type="hidden" id="paymenttotalamounthidden" value="0"/>
	<input type="hidden" id="paymenttotalwthidden" value="0"/>
	<input type="hidden" id="orderadvcreditno" value=""/>
	<input type="hidden" id="salesdetailcount" value="0"/>
	<input type="hidden" id="vendoraccountid" value="1"/>
	<input type="hidden" id="liveadvanceclose" value=""/>
	<input type="hidden" id="orderadvadjustamthidden" value="0"/>
	<input type="hidden" id="prebillnetamount" value="0"/>
	<input type="hidden" id="prebillnetwt" value="0"/>
	<input type="hidden" id="weightvalidate" value="<?php echo $mainlicense['weightvalidate']; ?>"/>
	<input type="hidden" id="barcodeoption" value="<?php echo $mainlicense['barcodeoption']; ?>"/>
	<input type="hidden" id="issuereceiptmergetext" value=""/>
	<input type="hidden" id="untagvalidatemergetext" value=""/>
	<input type="hidden" id="untagvalidatecountermergetext" value=""/>
	<input type="hidden" id="billnetamountwithsign" value="0"/>
	<input type="hidden" id="billnetwtwithsign" value="0"/>
	<input type="hidden" id="editfinalamtreceipt" value="0"/>
	<input type="hidden" id="editfinalamtissue" value="0"/>
	<input type="hidden" id="editfinalweightreceipt" value="0"/>
	<input type="hidden" id="editfinalweightissue" value="0"/>
	<input type="hidden" id="editweightissuereceiptid" value="0"/>
	<input type="hidden" id="editaccledgerissuereceiptid" value="0"/>
	<input type="hidden" id="creditoverlayhidden" value=""/>
	<input type="hidden" id="creditadjusthidden" value=""/>
	<input type="hidden" id="creditoverlayvaluehidden" value=""/> 
	<input type="hidden" id="creditadjustvaluehidden" value=""/>
	<input type="hidden" id="creditoverlaystatus" value=""/>
	<input type="hidden" id="editoverlaystatus" value="zyx-0"/>
	<input type="hidden" id="editissuereceiptlocalgridid" value=""/>
	<input type="hidden" id="advanceorbalance" value=""/>
	<input type="hidden" id="summarysalesreturnroundvalue" value=""/>
	<input type="hidden" id="orderimageoption" value="<?php echo $mainlicense['orderimageoption']; ?>"/>
	<input type="hidden" id="transactionimageoption" value="<?php echo $mainlicense['transactionimageoption']; ?>"/>
	<input type="hidden" id="weightcheck" value="<?php echo $mainlicense['weightcheck']; ?>"/>
	<input type="hidden" id="loosestonecharge" value="<?php echo $mainlicense['loosestonecharge']; ?>"/>
	<input type="hidden" id="approvaloutrfid" value="<?php echo $mainlicense['approvaloutrfid']; ?>"/>
	<input type="hidden" id="multiplescanmode" value="<?php echo $mainlicense['multiscanmode']; ?>"/>
	<input type="hidden" id="allowedaccounttypeset" value="<?php echo $mainlicense['allowedaccounttypeset']; ?>"/>
	<input type="hidden" id="estimatedefaultaccid" value="<?php echo $estimatedefaultaccid; ?>"/> 
	<input type="hidden" id="placeorderdefaultaccid" value="<?php echo $mainlicense['placeorderdefaultaccid']; ?>"/> 
	<input type="hidden" id="estimatedefaultaccname" value="<?php echo $estimatedefaultaccname; ?>"/> 
	<input type="hidden" id="estimatedefaultacctype" value="<?php echo $estimatedefaultacctype; ?>"/> 
	<input type="hidden" id="estimatedefaultmobnum" value="<?php echo $estimatedefaultmobnum; ?>"/> 
	<input type="hidden" id="flatweightwastagespan" value="<?php echo $mainlicense['flatweightwastagespan']; ?>"/> 
	<input type="hidden" id="pancardrestrictamt" value="<?php echo $mainlicense['pancardrestrictamt']; ?>"/> 
	<input type="hidden" id="cashreceiptlimit" value="<?php echo $mainlicense['cashreceiptlimit']; ?>"/> 
	<input type="hidden" id="cashissuelimit" value="<?php echo $mainlicense['cashissuelimit']; ?>"/> 
	<input type="hidden" id="touchchargesedit" value="<?php echo $mainlicense['touchchargesedit']; ?>"/> 
	<input type="hidden" id="discountbilllevelpercent" value="<?php echo $mainlicense['discountbilllevelpercent']; ?>"/> 
	<input type="hidden" id="salesauthuserrole" value="<?php echo $mainlicense['salesauthuserrole']; ?>"/> 
	<input type="hidden" id="untagstoneenable" value="<?php echo $mainlicense['untagstoneenable']; ?>"/> 
	<input type="hidden" id="multitagbarcode" value="<?php echo $mainlicense['multitagbarcode']; ?>"/> 
	<input type="hidden" id="untagwtvalidate" value="<?php echo $mainlicense['untagwtvalidate']; ?>"/> 
	<input type="hidden" id="untagwtshow" value="<?php echo $mainlicense['untagwtshow']; ?>"/> 
	<input type="hidden" id="estimatequicktotal" value="<?php echo $mainlicense['estimatequicktotal']; ?>"/> 
	<input type="hidden" id="estimatedefaultset" value="<?php echo $mainlicense['estimatedefaultset']; ?>"/> 
	<input type="hidden" id="caratwtcalcvalue" value="<?php echo $mainlicense['caratwtcalcvalue']; ?>"/> 
	<input type="hidden" id="salespaymenthideshow" value="<?php echo $mainlicense['salespaymenthideshow']; ?>"/>
	<input type="hidden" id="povendormanagement" value="<?php echo $mainlicense['povendormanagement']; ?>"/>
	<input type="hidden" id="takeordercategory" value="<?php echo $mainlicense['takeordercategory']; ?>"/>
	<input type="hidden" id="accountypehideshow" value="<?php echo $mainlicense['accountypehideshow']; ?>"/>
	<input type="hidden" id="takeorderduedate" value="<?php echo $mainlicense['takeorderduedate']; ?>"/>
	<input type="hidden" id="transactioncategoryfield" value="<?php echo $mainlicense['transactioncategoryfield']; ?>"/>
	<input type="hidden" id="transactionemployeeperson" value="<?php echo $mainlicense['transactionemployeeperson']; ?>"/>
	<input type="hidden" id="poautomaticvendor" value="<?php echo $mainlicense['poautomaticvendor']; ?>"/>
	<input type="hidden" id="receiveorderconcept" value="<?php echo $mainlicense['receiveorderconcept']; ?>"/>
	<input type="hidden" id="receiveordertemplateid" value="<?php echo $mainlicense['receiveordertemplateid']; ?>"/>
	<input type="hidden" id="rejectreviewordertemplateid" value="<?php echo $mainlicense['rejectreviewordertemplateid']; ?>"/>
	<input type="hidden" id="accountmobilevalidation" value="<?php echo $mainlicense['accountmobilevalidation']; ?>"/>
	<input type="hidden" id="accountmobileunique" value="<?php echo $mainlicense['accountmobileunique']; ?>"/>
	<input type="hidden" id="oldjewelsstonedetails" value="<?php echo $mainlicense['oldjewelsstonedetails']; ?>"/>
	<input type="hidden" id="billamountcalculationtrigger" value="<?php echo $mainlicense['billamountcalculationtrigger']; ?>"/>
	<input type="hidden" id="oldjewelsstonecalc" value="<?php echo $mainlicense['oldjewelsstonecalc']; ?>"/>
	<input type="hidden" id="purchasemodstonedetails" value="<?php echo $mainlicense['purchasemodstonedetails']; ?>"/>
	<input type="hidden" id="discountauthmoduleview" value="<?php echo $mainlicense['discountauthmoduleview']; ?>"/>
	<input type="hidden" id="ratedisplaymainview" value="<?php echo $mainlicense['ratedisplaymainview']; ?>"/>
	<input type="hidden" id="ratedisplaypurityvalue" value="<?php echo $mainlicense['ratedisplaypurityvalue']; ?>"/>
	<input type="hidden" id="salesreturnbillcustomers" value="<?php echo $mainlicense['salesreturnbillcustomers']; ?>"/>
	<input type="hidden" id="questionaireapplicable" value="<?php echo $mainlicense['questionaireapplicable']; ?>"/>
	<input type="hidden" id="accountfieldcaps" value="<?php echo $mainlicense['accountfieldcaps']; ?>"/>
	<input type="hidden" id="stonepiecescalc" value="<?php echo $mainlicense['stonepiecescalc']; ?>"/>
	<input type="hidden" id="accountsummaryinfo" value="<?php echo $mainlicense['accountsummaryinfo']; ?>"/>
	<input type="hidden" id="paymentsavebutton" value="<?php echo $mainlicense['paymentsavebutton']; ?>"/>
	<input type="hidden" id="printtemplatehideshow" value="<?php echo $mainlicense['printtemplatehideshow']; ?>"/>
	<input type="hidden" id="oldpurchasecredittype" value="<?php echo $mainlicense['oldpurchasecredittype']; ?>"/>
	<input type="hidden" id="chitbooktypeid" value="<?php echo $chitbooktypeid; ?>"/>
	<input type="hidden" id="all_metalarraydata" value="<?php echo $all_metaldetails; ?>"/>
	<input type="hidden" id="all_purityarraydata" value="<?php echo $all_puritydetails; ?>"/>
	<div id="subformspan1" class="hiddensubform" style="margin-top: -11px;">
	<?php
	if($device=='phone') 
	{
		?>
		<div class="large-12 small-12 columns end itemdetailsopacity paddingbtm tabletinfo" style="opacity:0;padding:0px !important;" id="salesdetailssubform">
<?php 
	} 
	else 
	{					
	?>
	<div class="large-12 small-12 columns end itemdetailsopacity paddingbtm tabletinfo" style="opacity:0;" id="salesdetailssubform">
	<?php 
	}
	?>
			<form method="POST" name="salesaddform" id="salesaddform" class="salesaddform validationEngineContainer">
				<div id="editvalidate" class="validationEngineContainer">
					<input type="hidden" id="itemtagid" name="itemtagid" value="1"/>
					<input type="hidden" id="ckeyword" name="ckeyword" value="">
					<input type="hidden" id="ckeywordvalue" name="ckeywordvalue" value="">
					<input type="hidden" id="ckeywordfinalkey" name="ckeywordfinalkey" value="">
					<input type="hidden" id="ckeywordfinalvalue" name="ckeywordfinalvalue" value="">
					<input type="hidden" id="ckeywordoriginalvalue" name="ckeywordoriginalvalue" value="">
					<input type="hidden" id="ckeywordcalvalue" name="ckeywordcalvalue" value="">
					<input type="hidden" id="ckeywordfcalvalue" name="ckeywordfcalvalue" value="">
					<input type="hidden" id="otherdetails" name="otherdetails" value="">
					<input type="hidden" id="otherdetailsvalue" name="otherdetailsvalue" value="">
					<input type="hidden" id="taxapplicable" name="taxapplicable" value="">
					<input type="hidden" id="salesdetailid" name="salesdetailid"/>
					<input type="hidden" id="mainsalesdetailid" name="mainsalesdetailid"/>
					<input type="hidden" id="presalesdetailid" name="presalesdetailid"/>
					<input type="hidden" id="referencenumber" name="referencenumber"/>
					<input type="hidden" id="prereferencenumber" name="prereferencenumber"/>
					<input type="hidden" id="prechitbooknumber" name="prechitbooknumber"/>
					<input type="hidden" id="prewastage" value=""/>
					<input type="hidden" id="itemwastagevalue" value=""/>
					<input type="hidden" id="premakingcharge" value=""/>
					<input type="hidden" id="itemmakingvalue" value=""/>
					<input type="hidden" id="preflatcharge" value=""/>
					<input type="hidden" id="itemflatvalue" value=""/>
					<input type="hidden" id="prelstcharge" value=""/>
					<input type="hidden" id="itemlstvalue" value=""/>
					<input type="hidden" id="prerepaircharge" value=""/>
					<input type="hidden" id="itemrepairvalue" value=""/>
					<input type="hidden" id="itemchargesamtvalue" value=""/>
					<input type="hidden" id="discountdata" value=""/>	
					<?php
					if($device=='phone') { ?>
						<div class="large-4 small-12 columns end paddingzero" style="position:relative;height: 100vh !important;background: #f2f3fa;" id="salesprodformdetails">
						<span class="foramounttype">
						<div class="large-12 small-12 columns paddingzero" id="tabindexhideshow">				
							<div class="large-12 small-12 columns cleardataform override-large-2" style="padding-left: 0 !important; padding-right: 0 !important;box-shadow: none;background: #f2f3fa!important;" >
					<?php } else {?>					
					<div class="large-4 small-12 columns end z-depth-1 paddingzero borderstyle" style="position:relative;height: 750px !important;background: #fff;" id="salesprodformdetails">
					<span class="foramounttype">
					<div class="large-12 small-12 columns paddingzero" id="tabindexhideshow">			
						<div class="large-12 small-12 columns cleardataform override-large-2" style="padding-left: 0 !important; padding-right: 0 !important;box-shadow: none;" >
					<?php }
					?>
						
							
							
									<div style="background: #fff9c4!important; border: 4px solid #fff9c4 !important;height: 65px;">	
										<div class="static-field large-4 small-4 columns "  id="stocktypeid_div">
											<label>Type<span class="mandatoryfildclass" id="stocktypeid_req">*</span></label>		
											<select id="stocktypeid" name="stocktypeid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="102">									
												<option value=""></option>
												<?php 
													foreach($stocktype->result() as $key):
													?>
												<option value="<?php echo $key->stocktypeid;?>" data-label="<?php echo $key->stocktypelabel;?>" data-stocktypeidhidden="<?php echo $key->stocktypename;?>"  ><?php echo $key->stocktypename;?></option>
													<?php  endforeach;?>	
											</select>
											<input type="hidden" id="stocktypelabel" name="stocktypelabel"/>
										</div>
										<div class="static-field large-12 small-12 columns clearitemdetailstab clearonstocktypechange" id="ordernumber-div">
											<label>Order Number</label>						
											<select id="ordernumber" name="ordernumber" class="chzn-select" data-prompt-position="bottomLeft:14,36" data-validation-engine="" tabindex="105">
											</select>
										</div>
										<?php if($mainlicense['barcodeoption'] == '1' || $mainlicense['barcodeoption'] == '3') {?>
										<div class="static-field large-4 small-4 columns clearitemdetailstab clearonstocktypechange"  id="itemtagnumber-div">
											<input type="text" class="" id="itemtagnumber" name="itemtagnumber" value=""  data-validation-engine="" tabindex="110">
											<label for="itemtagnumber">Tag No<span class="mandatoryfildclass" id="itemtagnumber_req"></span></label>
										</div>
										<?php } ?>
										<?php if($mainlicense['barcodeoption'] == '2' || $mainlicense['barcodeoption'] == '3') {?>
										<div class="static-field large-4 small-4 columns clearitemdetailstab clearonstocktypechange" id="rfidtagnumber-div">
											<input type="text" class="" id="rfidtagnumber" name="rfidtagnumber" value=""  data-validation-engine="" tabindex="112">
											<label for="rfidtagnumber">RFID No<span class="mandatoryfildclass" id="rfidtagnumber_req"></span></label>
										</div>
										<?php } ?>
										<?php if($mainlicense['lotdetail'] == 'YES') { ?>
										<div class="static-field large-4 small-4  columns clearonstocktypechange" id="lottypeid-div">
											<label>Lot Type<span class="mandatoryfildclass">*</span></label>
											<select data-validation-engine="" class="chzn-select" id="lottypeid" name="lottypeid" data-prompt-position="topLeft:14,36">
												<?php foreach($lottype as $key):?>
													<option value="<?php echo $key->lottypeid;?>" data-lottypeidhidden="<?php echo $key->lottypename;?>">
													<?php echo $key->lottypename;?></option>
												<?php endforeach;?>	
											</select>
										</div>
										<?php } ?>
										<div class="static-field large-4 small-4 columns" id="ordermodeid-div">
											<label>Order Mode<span class="mandatoryfildclass" id="ordermodeid_req">*</span></label>	
											<select id="ordermodeid" name="ordermodeid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="111">
												<option value=""></option>
												<?php 
												 foreach($ordermode as $key):
												?>
												<option value="<?php echo $key->ordermodeid;?>" data-name ="<?php echo $key->ordermodename;?>"> 
												<?php echo $key->ordermodename; ?></option>
												<?php endforeach;?>
											</select>
										</div>
										<div class="static-field large-4 small-4  columns" id="orderduedate-div">
											<input id="orderduedate" class="fordatepicicon" type="text" data-dateformater="dd-mm-yy" data-prompt-position="bottomLeft" readonly="readonly" tabindex="1001" name="orderduedate" data-validation-engine="validate[required]">
											<label for="orderduedate">Due Date<span class="mandatoryfildclass">*</span></label>
										</div>
										<div class="static-field large-4 small-4  columns hidedisplay">
											<label>Mode<span class="mandatoryfildclass" id="modeid_req">*</span></label>	
											<select id="modeid" name="modeid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="101">							
												<?php	foreach($salesmode as $key):
												if($key->salesmodeid == 2){
												?>
													<option value="<?php echo $key->salesmodeid;?>" ><?php echo $key->salesmodename;?></option>
												<?php } endforeach;?>	
											</select>
										</div>
										<div class="static-field large-4 small-4 columns clearonstocktypechange" id="purchasemode-div">
											<label>Mode<span class="mandatoryfildclass">*</span></label>
											<select data-validation-engine="" class="chzn-select" id="purchasemode" name="purchasemode" data-prompt-position="topLeft:14,36">
												<?php foreach($purchasemode as $key):?>
													<option value="<?php echo $key->purchasemodeid;?>">
													<?php echo $key->purchasemodename;?></option>
												<?php endforeach;?>	
											</select>
										</div>
										<div class="static-field large-4 small-4 columns clearonstocktypechange" id="orderitemratefix-div">
											<label>Rate Fix</label>	
											<select id="orderitemratefix" name="orderitemratefix" class="chzn-select" data-prompt-position="bottomLeft:14,36" data-validation-engine="" tabindex="105">
												<?php foreach($orderitemratefix as $key):?>
													<option value="<?php echo $key->orderitemratefixid;?>" data-orderitemratefixhidden="<?php echo $key->orderitemratefixname;?>" >
													<?php echo $key->orderitemratefixname;?></option>
												<?php endforeach;?>
											</select>
										</div>
										<div class="static-field large-6 small-4 columns hidedisplay" id="billnumberid-div">
											<label>Bill Number<span class="mandatoryfildclass" id="billnumberid_req">*</span></label>		
											<select id="billnumberid" name="billnumberid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="102">									
												<option value=""></option>
													<?php 
													foreach($billsalesneumber as $key):
													if($key->salesnumber != 'pending') {
													?>
												<option value="<?php echo $key->salesid;?>" data-accountid="<?php echo $key->accountid;?>" data-label="<?php echo $key->salesnumber;?>"  ><?php echo $key->salesnumber;?></option>
													<?php } endforeach;?>	
											</select>
											<input type="hidden" id="billnumberidlabel" name="billnumberidlabel"/>
										</div>
										<div class="static-field large-4 small-4  columns clearonstocktypechange" id="approvalrfid-div">
											<label>Approval Rfid</label>						
											<select id="approvalrfid" name="approvalrfid" class="chzn-select" data-prompt-position="bottomLeft:14,36" data-validation-engine="" tabindex="104"> 
												<option value="1">YES</option>
												<option value="0">NO </option>
											</select>
										</div>
										<div class="static-field large-4 small-4 columns clearonstocktypechange" id="approvalnumber-div">
											<label>Approval No</label>						
											<select id="approvalnumber" name="approvalnumber" class="chzn-select" data-prompt-position="bottomLeft:14,36" data-validation-engine="" tabindex="104"> 
												<option value=""> </option>
											</select>
										</div>
										<div class="input-field large-3 small-3  columns hidedisplay" id="olditemvoucher-div">
											<span style="color:black;">Bill<span class="innerfrmicon" id="olditemvouchericon" style="position:relative;top:3px;left:5px;cursor:pointer;margin-top:-5px;" tabindex="383"><i class="material-icons">radio_button_checked</i></span></span>
										</div>
									</div>
								<div class="clearitemdetailstab">
									<?php if($transactioncategoryfield == '0') { ?>
										<div class="static-12  medium-12 small-12 columns clearonstocktypechange" id="category-div">
											<label>Category<span class="mandatoryfildclass">*</span></label>
											<?php 
												$mandlab = "";
												$tablename = 'category' ;
												$mandatoryopt = ""; //mandatory option set
												echo divopen( array("id"=>"dl-promenucategory","class"=>"dl-menuwrapper","style"=>"z-index:2;margin-bottom: 1rem") );
												$type="button";
												echo '<button name="treebutton" type="button" id="prodcategorylistbutton" class="btn dl-trigger treemenubtnclick" tabindex="" style="height:2.2rem;color:#ffffff;font-size: 1.45rem;padding: 0.1rem;padding-top:0.3rem;background-color:#546e7a;"><i class="material-icons">format_indent_increase</i></button>';
												$txtoptions = array("style"=>"width:83%;display:inline;position:absolute;height:2.12rem;padding-left: 5px;","readonly"=>"readonly","tabindex"=>"","class"=>"");
												echo text('categorynamehidden','',$txtoptions);
												echo '<ul class="dl-menu maintreegenerate large-12 medium-6 small-12 column" id="prodcategorylistuldata">';
												$dataset = $this->Salesmodel->jewel_treecreatemodel($tablename);
												$this->Basefunctions->createTree($dataset,0);
												echo close('ul');
												echo close('div');
											?>							
											<input type="hidden" id="category" name="category" value="" />
											<input type="hidden" id="treecategoryname" name="treecategoryname" value="" />
										</div>
									<?php } else {?>
										<div class="static-field large-4 small-4  columns clearonstocktypechange" id="category-div">
											<label>Category<span class="mandatoryfildclass">*</span></label>
											<select data-validation-engine="" class="chzn-select" id="category" name="category" data-prompt-position="topLeft:14,36">
												<?php foreach($categoryname->result() as $key):?>
													<option value="<?php echo $key->categoryid;?>" data-categoryhidden="<?php echo $key->categoryname; ?>">
													<?php echo $key->categoryname;?></option>
												<?php endforeach;?>	
											</select>
										</div>
									<?php }?>
									<div class="static-field large-4 small-4 columns clearonstocktypechange"  id="fasttotalfortagamt-div">
										<input type="text" class="" id="fasttotalfortagamt" name="fasttotalfortagamt" disabled value=""  data-validation-engine="" tabindex="110">
										<label for="fasttotalfortagamt">Quick Amt</label>
									</div>
									<div class="static-field large-4 small-4 columns clearonstocktypechange"  id="fasttotalfortagwt-div">
										<input type="text" class="" id="fasttotalfortagwt" name="fasttotalfortagwt" disabled value=""  data-validation-engine="" tabindex="110">
										<label for="fasttotalfortagwt">Quick WT</label>
									</div>
									
									<div class="static-field large-8 small-8 columns clearonstocktypechange"  id="product-div">
										<label>Product<span class="mandatoryfildclass" id="product_req">*</span></label>	
										<select id="product" name="product" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="111">
										<!--<option class="groupall ddhidedisplay" data-purchasechargeid="1" data-chargeid ="1" value="1" data-stoneapplicable="" data-counterid="1" data-purityid="1" data-label="" data-bullionapplicable="" data-taxid="" data-taxdataid="" data-categorytaxid ="" data-size="" data-productstonecalctypeid="1" data-tagtypeid="1" data-producthidden="ALL">1 - ALL</option>-->
										<?php 
										 foreach($product->result() as $key):
										?>
										<option value="<?php echo $key->productid;?>" data-purchasechargeid="<?php echo $key->purchasechargeid;?>" data-chargeid="<?php echo $key->chargeid;?>" data-stoneapplicable="<?php echo $key->stoneapplicable;?>" data-counterid="<?php echo $key->counterid;?>" data-purityid="<?php echo $key->purityid;?>" data-label="<?php echo $key->productname;?>" data-bullionapplicable="<?php echo $key->bullionapplicable;?>" data-taxid="<?php echo $key->taxid;?>" data-taxdataid="<?php echo $key->taxdataid;?>" data-taxmasterid="<?php echo $key->taxmasterid;?>" data-categoryid="<?php echo $key->categoryid;?>" data-calctypeid="<?php echo $key->weightcalculationtypeid;?>" data-categorytaxid ="<?php echo $key->categorytaxid;?>" data-size="<?php echo $key->size;?>" data-productstonecalctypeid="<?php echo $key->productstonecalctypeid;?>" data-tagtypeid="<?php echo $key->tagtypeid;?>" data-producthidden="<?php echo $key->productname; ?>" data-stoneid="<?php echo $key->stoneid; ?>" data-loosestone="<?php echo $key->loosestone; ?>"> 
										<?php if($mainlicense['productidshow'] == 'YES') { echo $key->productid.' - '.$key->productname; } else { 
										echo $key->productname; } ?></option>
										<?php endforeach;?>
										</select>
									</div>
									<div class="static-field large-4 small-4  columns clearonstocktypechange" id="olditemdetails-div">
										<label>Old Item Details<span class="mandatoryfildclass" id="">*</span></label>
										<select id="olditemdetails" name="olditemdetails" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="112" data-validation-engine="">
											<?php 
												foreach($olditemdetails as $key):
												if(in_array($key->olditemdetailsid,$olditemdata)){
											?>
												<option value="<?php echo $key->olditemdetailsid;?>" data-label="<?php echo $key->olditemdetailsname;?>" data-olditemdetailshidden="<?php echo $key->olditemdetailsname;?>" ><?php echo $key->olditemdetailsname;?></option>
											<?php } endforeach;?>
										</select>
									</div>	
									<div class="static-field large-4 small-4 columns clearonstocktypechange"    id="purity-div">
									<label>Purity<span class="mandatoryfildclass" id="purity_req">*</span></label>				
									<select id="purity" name="purity" class="chzn-select " data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="113">
										<option value=""></option>
										<?php $prev = ' ';
											foreach($purity->result() as $key):
											$cur = $key->metalid;
											$a="";
											if($prev != $cur )
											{
												if($prev != " ")
												{
													 echo '</optgroup>';
												}
												echo '<optgroup  label="'.$key->metalname.'">';
												$prev = $key->metalid;
											}
										?>
										<option data-metalpurity_id="<?php echo $key->purityrateid;?>"  data-metalset-purity_id="<?php echo $key->purity_id;?>" data-metalid="<?php echo $key->metalid;?>" data-melting="<?php echo $key->melting;?>" value="<?php echo $key->purityid;?>"  data-purityhidden="<?php echo $key->purityname;?>"><?php echo $key->purityname;?></option>
										<?php endforeach;?>
									</select>
									</div>
									<?php if(strtolower($mainlicense['counter']) == 'yes') {?>	
									<div class="static-field large-8 small-8 columns clearonstocktypechange" id="counter-div" >
										<label>Storage<span class="mandatoryfildclass " id="counter_req">*</span></label>				
										<select id="counter" name="counter" class="chzn-select " data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="114">
										<option value=""> </option>
										<?php  foreach($counter->result() as $key):?>
										<option value="<?php echo $key->counterid;?>" data-defcounterids="<?php echo $key->counterdefault;?>"  data-defparentcounterids="<?php echo $key->counterdefaultid;?>" data-label="<?php echo $key->countername;?>" data-counterhidden="<?php echo $key->countername;?>"> <?php echo $key->counterid.' - '.$key->countername;?></option>
										<?php endforeach;?>
										</select>
										<input type="hidden" name="processcounterid" id="processcounterid" value="1" />
									</div>
									<?php } ?>
									<!--  <div class="clearbelowpurity columns paddingzero"> -->
									<div class="input-field large-4 small-4 columns clearonstocktypechange" id="grossweight-div" >
										<input type="text" class="weightcalculate purewtpayment" id="grossweight" data-calc="GWT" name="grossweight" value=""  data-validation-engine="" tabindex="115">
										<label for="grossweight">G.WT<span class="mandatoryfildclass" id="grossweight_req">*</span><span class="validatewtfontize" id="grossweight_validatewt"></span></label>
									</div>
									<div class="input-field large-4 small-4  columns clearonstocktypechange"  id="stoneweight-div">
										<input type="text" class="weightcalculate " data-calc="SWT" data-validategreat="grossweight" id="stoneweight" name="stoneweight" value=""  data-validation-engine="" tabindex="116"/>
										<?php if(strtolower($mainlicense['stone']) == 'yes') {?>	
										<span class="innerfrmicon stoneweighticoncls" id="stoneoverlayicon" style="position: relative; top: -2rem; left: 6rem; cursor: pointer; margin-top:-5px;display: none;" tabindex="426"><i class="material-icons">group_work</i></span>
										<?php } ?>
										<label class="" for="stoneweight">S.WT<span class="mandatoryfildclass" id="stoneweight_req">*</span>(<span class="orginalvalclass backsapceclass"  id="stonetotdiaweight"></span>)</label>
										<input type="hidden" id="hiddenstonedata" name="hiddenstonedata"/>
									</div>
									<div class="input-field large-4 small-4  columns  clearonstocktypechange hidedisplay"   id="wastageweight-div">
										<input type="text" class="weightcalculate" data-calc="WWT" data-validategreat="wastageweight" id="wastageweight" name="wastageweight" value="" data-validation-engine="" tabindex="117"/>
										<label for="wastageweight">W.WT<span class="mandatoryfildclass" id="wastageweight_req">*</span></label>
									</div>
									<div class="input-field large-4 small-4  columns  clearonstocktypechange" id="dustweight-div">
										<input type="text" class="weightcalculate purewtpayment" data-calc="DWT"  data-validategreat="dustweight" id="dustweight" name="dustweight" value="" data-validation-engine="" tabindex="117"/>
										<label for="dustweight">D.WT<span class="mandatoryfildclass" id="dustweight_req">*</span></label>
									</div>
									<div class="input-field large-4 small-4 columns  clearonstocktypechange" id="netweight-div">
										<input type="text" class="weightcalculate purewtpayment" data-calc="NWT" data-validategreat="grossweight" id="netweight" name="netweight" value="" data-validation-engine="" tabindex="117"/>
										<label for="netweight">N.WT<span class="mandatoryfildclass" id="netweight_req">*</span><span class="validatewtfontize" id="netweight_validatewt"></span></label>
									</div>
									<div class="input-field large-4 small-4 columns  clearonstocktypechange" id="itemcaratweight-div">
										<input type="text" class="weightcalculate purewtpayment" data-calc="CWT" data-validategreat="" id="itemcaratweight" name="itemcaratweight" value="" data-validation-engine="" tabindex="117"/>
										<label for="netweight">C.WT<span class="mandatoryfildclass" id="">*</span></label>
									</div>
									<div class="input-field large-4 small-4  columns  clearonstocktypechange " id="interestcalc-div">
										<input type="text"  id="interestcalc" name="interestcalc" value="" data-validation-engine="" tabindex="118"/>
										<label for="interestcalc">Interest Calculation</label>
									</div>
									<div class="input-field large-4 small-4  columns  clearonstocktypechange " id="interestamount-div">
										<input type="text"  id="interestamount" name="interestamount" value="" data-validation-engine="" tabindex="119"/>
										<label for="interestamount">Interest Amount</label>
									</div>
									<div class="input-field large-4 small-4  columns clearonstocktypechange " id="pieces-div" >
										<input type="text" class="" id="pieces" name="pieces" maxlength="5" data-validation-engine="" tabindex="120"/>
										<label for="pieces">Pieces<span class="mandatoryfildclass" id="pieces_req"></span><span class="validatewtfontize" id="pieces_validatewt"></span></label>
									</div>
									<div class="static-field large-4 small-4  columns clearonstocktypechange " id="orderitemsize-div">
										<label>Size</label>
										<select id="orderitemsize" name="orderitemsize" class="chzn-select" data-validation-engine="" tabindex="120">
											<?php $prev = ' ';
												foreach($sizename->result() as $key):
												?>
											<option value="<?php echo $key->sizemasterid;?>" data-productname="<?php echo $key->productname;?>" data-productid="<?php echo $key->productid;?>" data-orderitemsizehidden="<?php echo $key->sizemastername;?>"> <?php echo $key->sizemastername; ?></option>
											<?php endforeach;?>
										</select>
									</div>
									<div class="input-field large-4 small-4  columns clearonstocktypechange " id="ratepergram-div">
										<input type="text" class="" id="ratepergram" name="ratepergram" data-validation-engine="" tabindex="121" maxlength="10">
										<label for="ratepergram">Rt/Gm<span class="mandatoryfildclass" id="ratepergram_req">*</span><span id="oldjewelnewrate"></span></label>
									</div>
									<div class="input-field large-4 small-4 columns clearonstocktypechange" id="proditemrate-div">
										<input type="text" class="" id="proditemrate" name="proditemrate" value="" tabindex="115" data-validation-engine="" maxlength="10">
										<label for="proditemrate">Item Rate<span class="mandatoryfildclass" >*</span></label>
									</div>
									<div class="input-field large-4 small-4  columns clearonstocktypechange" id="paymenttouch-div">
										<input type="text" class="purewtpayment" id="paymenttouch" name="paymenttouch" value="" tabindex="130" data-validation-engine="">
										<label for="paymenttouch">Touch<span class="mandatoryfildclass" id="paymenttouch_req"></span> </label>
									</div>
								
									<div class="input-field large-4 small-4 columns chargedetailsdiv charge12 clearonstocktypechange" id="osper-div">
										<input type="text" originalvalue="" origcalfinalvalue="0" category="OLD" calfinalvalue="0" formula="netweight-(netweight*osper/100)" discountvalue="" class="amtvalue" keyword="OS-PT" id="osper" name="osper" value="" tabindex="122" >
										<label for="osper" title="Old Subtract Percent">Old Subtract Percent<span class="mandatoryfildclass">*</span><span class="orginalvalclass" id="OS-PT" style="color:#d95c5c;"></span></label>
									</div>
									<div class="input-field large-4 small-4  columns chargedetailsdiv charge13 clearonstocktypechange" id="omper-div">
										<input type="text" originalvalue="" origcalfinalvalue="0" category="OLD" calfinalvalue="0" formula="(netweight*omper)/100" discountvalue="" class="amtvalue" keyword="OM-PT" id="omper" name="omper" value="" tabindex="123">
										<label for="omper" title="Old Multiple Percent">Old Multiple Percent<span class="mandatoryfildclass">*</span><span class="orginalvalclass" id="OM-PT" style="color:#d95c5c;"></span></label>
									</div>
									<div class="input-field large-4 small-4  columns clearonstocktypechange" id="wastageless-div">
										<input type="text" class="" keyword="" data-calc="" data-validategreat="" id="wastageless" name="wastageless" value="" data-validation-engine="" tabindex="117"/>
										<input type="hidden" name="wastagelesspercent" id="wastagelesspercent" value="0" />
										<label for="wastageless">Wst % less<span class="mandatoryfildclass" id="wastageless_req">*</span></label>
									</div>
									<?php if($mainlicense['wastagecalctype'] == 1) { ?>
									<div class="input-field large-4 small-4  columns clearonstocktypechange" id="wastage-div" >
										<input type="text"  keyword="" class="" originalvalue="" charge="additionalcharge3" data-calc="" data-validategreat="" id="wastage" name="wastage" value="" data-validation-engine="" tabindex="117" readonly>
										<label for="wastage">W.C<span id="wastagekeylabel"></span> (<span class="orginalvalclass backsapceclass" id="wastagespan" ></span>)</label>
									</div>
									<?php } else { ?>
									<div class="input-field large-4 small-4 columns clearonstocktypechange" id="wastage-div"  >
										<input type="text"  keyword="" class="" originalvalue="" charge="additionalcharge3" data-calc="" data-validategreat="" id="wastage" name="wastage" value="" data-validation-engine="" tabindex="117" readonly>
										<label for="wastage">W.C<span id="wastagekeylabel"></span> (<span class="orginalvalclass backsapceclass" id="wastagespan" ></span>)</label>
									</div>
									<?php } ?>
									<div class="input-field large-4 small-4 columns clearonstocktypechange" id="makingcharge-div" >
										<input type="text" keyword="" data-calc="" originalvalue="" charge="additionalcharge2" data-validategreat="" id="makingcharge" name="makingcharge" value="" data-validation-engine="" tabindex="117" readonly>
										<label for="makingcharge">M.C<span id="makingchargekeylabel"></span> (<span class="orginalvalclass backsapceclass"  id="makingchargespan" > </span>)</label>
									</div>
									<div class="input-field large-4 small-4  columns clearonstocktypechange" id="lstcharge-div" >
										<input type="text" keyword="" data-calc="" originalvalue="" charge="additionalcharge8" data-validategreat="" id="lstcharge" name="lstcharge" value="" data-validation-engine="" tabindex="117" readonly>
										<label for="lstcharge">LST<span id="lstchargekeylabel"></span> (<span class="orginalvalclass backsapceclass"  id="lstchargespan" > </span>)</label>
									</div>
									<div class="input-field large-4 small-4 columns clearonstocktypechange" id="flatcharge-div" >
										<input type="text" data-calc="" originalvalue="" charge="additionalcharge6" data-validategreat="" id="flatcharge" name="flatcharge" value="" data-validation-engine="" tabindex="117"/ readonly>
										<label for="flatcharge">F.C <span id="flatchargekeylabel"></span>(<span class="orginalvalclass backsapceclass" id="flatchargespan"></span>)</label>
									</div>
									<div class="input-field large-4 small-4 columns clearonstocktypechange" id="cdicon-div" >
										<input type="text" class="extracalc" keyword="additionalchargeamt" data-calc="" data-validategreat="" id="additionalchargeamt" name="additionalchargeamt" value="" readonly data-validation-engine="" tabindex="117"/>
										<label for="additionalchargeamt">Charges<span class="mandatoryfildclass" id="additionalchargeamt_req">*</span></label>
									</div>
									<div class="input-field large-4 small-4 columns clearonstocktypechange" id="repaircharge-div" >
										<input type="text" data-calc="" originalvalue="" charge="additionalcharge10" data-validategreat="" id="repaircharge" name="repaircharge" value="" data-validation-engine="" tabindex="117"/ readonly>
										<label for="repaircharge">R.C <span id="repairchargekeylabel"></span>(<span class="orginalvalclass backsapceclass" id="repairchargespan"></span>)</label>
									</div>
									<div class="input-field large-4 small-4  columns clearonstocktypechange" id="stonecharge-div">
										<input type="text" charge="additionalcharge7" class="" keyword="calcstoneamt" data-calc="" data-validategreat="" id="stonecharge" name="stonecharge" value="" data-validation-engine="" readonly tabindex="117"/>
										<label for="stonecharge">Stone<span class="mandatoryfildclass" id="stonecharge_req">*</span></label>
									</div>
									<div class="input-field large-4 small-4 columns clearonstocktypechange" id="grossamount-div" >
										<input type="text" charge="additionalcharge6" class="" keyword="calcgrossamt" data-calc="" data-validategreat="" id="grossamount" name="grossamount" value="" data-validation-engine="" readonly tabindex="117"/>
										<label for="grossamount">Gross Amt<span class="mandatoryfildclass" id="grossamount_req">*</span></label>
									</div>
									<div class="input-field large-4 small-4  columns clearonstocktypechange" id="discount-div">
										<input type="text" class="" charge="additionalcharge9" keyword="" data-calc="" data-validategreat="" id="discount" name="discount" value="" data-validation-engine="" tabindex="117"/>
										<label for="discount">D<span id="itemdiscountcalname"></span></label>
									</div>
									<div class="input-field large-4 small-4  columns clearonstocktypechange" id="taxamount-div">
										<input type="text" charge="additionalcharge8" data-calc="" data-validategreat="" id="taxamount" name="taxamount" value="" data-validation-engine="" readonly tabindex="117"/>
										<label for="taxamount">Tax<span class="mandatoryfildclass" id="taxamount_req">*</span></label>
									</div>
									<div class="input-field large-4 small-4 columns clearonstocktypechange" id="cardcommission-div">
										<input type="text" class="" id="cardcommission" name="cardcommission" value="" tabindex="131" data-validation-engine="">
										<label for="cardcommission">Card Commission<span class="mandatoryfildclass" id="paymenttouch_req"></span> </label>
									</div>
									<div class="input-field large-4 small-4  columns clearonstocktypechange" id="expiredate-div">
										<input id="expiredate" class="fordatepicicon" type="text" name="expiredate" tabindex="132" readonly="readonly" data-prompt-position="bottomLeft" data-dateformater="dd-mm-yy">
										<label for="expiredate">Expire Date</label>	
									</div>
									<div class="input-field large-4 small-4  columns clearonstocktypechange"  id="totalamount-div" style="background: #fff9c4!important;">
										<input type="text" class="" keyword="totalnetamount" id="totalamount" name="totalamount" data-validation-engine="" tabindex="133" value="">
										<label for="totalamount">Net Amt<span class="mandatoryfildclass" id="totalamount_req"></span></label>
									</div>
									<div class="input-field large-4 small-4  columns" id="pureweight-div">
										<input type="text" class="" id="pureweight" name="pureweight" data-validation-engine="" tabindex="134" readonly>
										<label for="pureweight">Pure Wt<span class="mandatoryfildclass" id="pureweight_req">*</span></label>
									</div>
									<div id="balaceproductid-divid" class="static-field large-6 small-8  columns hidedisplay">
										<label>Bal. Product<span class="mandatoryfildclass " id="balaceproductid_req">*</span></label>						
										<select id="balaceproductid" name="balaceproductid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="1004">
										<option value=""> </option>
										</select>
									</div>
									<?php if(strtolower($mainlicense['counter']) == 'yes') {?>
									<div class="static-field large-6 small-8 columns hidedisplay" id="balancecounter-div">
										<label>Bal. Storage<span class="mandatoryfildclass " id="counter_req">*</span></label>			
										<select id="balancecounter" name="balancecounter" class="chzn-select " data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="114">
										<option value=""> </option>
										<?php  foreach($counter->result() as $key):?>
										<option value="<?php echo $key->counterid;?>" data-defcounterids="<?php echo $key->counterdefault;?>"  data-defparentcounterids="<?php echo $key->counterdefaultid;?>" data-label="<?php echo $key->countername;?>"> <?php echo $key->counterid.' - '.$key->countername;?></option>
										<?php endforeach;?>
										</select>
									</div>
									<?php } ?>
									<div class="input-field large-4 small-4  columns hidedisplay" id="imageitem-div">
											<span style="color:black;">Image<span class="innerfrmicon" id="imageitemicon" style="position:relative;top:3px;left:5px;cursor:pointer;margin-top:-5px;" tabindex="383"><i class="material-icons">radio_button_checked</i></span></span>
									</div>
									<?php 
									if($loggedinuserroleid == '2') {
									?>
									<div id="ordervendorid-div" class="static-field large-4 medium-4 small-4  columns">
										<label>Vendor</label>						
										<select id="ordervendorid" name="ordervendorid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="1004">
											<option value=""> </option>
											<?php foreach($ordervendorid as $key):?>
											<option data-ordervendorname="<?php echo $key->accountname;?>" value="<?php echo $key->accountid;?>" data-ordervendoridhidden="<?php echo $key->accountname;?>"><?php echo $key->accountname;?></option>
											<?php endforeach;?>
										</select>
									</div>
									<div class="static-field large-4 medium-4 small-4  columns" id="vendororderduedate-div">
										<input id="vendororderduedate" class="fordatepicicon" type="text" data-dateformater="dd-mm-yy" data-prompt-position="bottomLeft" readonly="readonly" tabindex="1001" name="vendororderduedate" data-validation-engine="validate[required]">
										<label for="vendororderduedate">Vendor Due Date<span class="mandatoryfildclass">*</span></label>
									</div>
									<?php 
										}
									?>
								<?php 
									$transactionemployeeperson = $this->Basefunctions->get_company_settings('transactionemployeeperson');
									if($transactionemployeeperson == '0') {
								?>
									<div class="static-field large-10 medium-10 small-12 columns hidedisplay" id="employeepersonid-div" >
										<label>Employee<span class="mandatoryfildclass " id="counter_req">*</span></label>						
										<select id="employeepersonid" name="employeepersonid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="1004" multiple>
										<?php $prev = ' ';
											foreach($employeedata as $key):
										?>
											<option data-name="<?php echo $key->employeename;?>" value="<?php echo $key->employeeid;?>" data-employeepersonidhidden="<?php echo $key->employeename;?>"><?php echo $key->employeenumber;?></option>
											<?php endforeach;?>
										</select>
										<input type="hidden" class="" id="multipleemployeeid" name="multipleemployeeid" value="" tabindex="" />
										<input type="hidden" class="" id="multipleemployeename" name="multipleemployeename" value="" tabindex="" />
									</div>
								<?php } else { ?>
										<div class="static-field large-8 small-12 columns hidedisplay" id="employeepersonid-div" >
										<label>Employee<span class="mandatoryfildclass " id="counter_req">*</span></label>						
										<select id="employeepersonid" name="employeepersonid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="1004">
										<?php $prev = ' ';
											foreach($employeedata as $key):
										?>
											<option data-name="<?php echo $key->employeename;?>" value="<?php echo $key->employeeid;?>" data-employeepersonidhidden="<?php echo $key->employeename;?>"><?php echo $key->employeenumber."-".$key->employeename;?></option>
											<?php endforeach;?>
										</select>
									</div>
								<?php }?>
								
									<input type="hidden" class="" id="gwcaculation" name="gwcaculation" value="" tabindex="" data-validation-engine="">
									<input type="hidden" class="" id="ratelesscalc" name="ratelesscalc" value="" tabindex="" data-validation-engine="">
									<input type="hidden" class="" id="weightlesscalc" name="weightlesscalc" value="" tabindex="" data-validation-engine="">
									<!--hidden for sales details-->
									<input type="hidden" id="hiddentaxgridetail" name="hiddentaxgridetail"/>
									<input type="hidden" id="hiddenaddongriddetail" name="hiddenaddongriddetail"/>
									<!--Extension form elements---->
									<div class="large-6 small-4 columns" style="position:absolute;">
										<div class="overlay" id="salescommentoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">	
											<div class="row" style="padding-top:180px;">&nbsp;</div>
											<div class="large-3 medium-10 small-4 large-centered medium-centered columns">
												<div class="large-12 columns headercaptionstyle headerradius">
													<div class="large-6 medium-6 small-6 columns headercaptionleft">
													</div>
													<div class="large-6 medium-6 small-12 columns righttext small-only-text-center" style="text-align:right;">					
													<span class="fa fa-times" title="Close" id="salesddcommentclose"> </span>
													</div>
												</div>
												<div class="large-12 columns" style="">
													<input type="hidden" id="ordersalesdetailid" name="ordersalesdetailid"/>
													<div class="large-12 columns "> &nbsp; </div>
												</div>
											</div>
										</div>
									</div>

									<!-- </div>-->
									
								</div>
								</div>
														
								<?php if($device=='phone') { // Mobile settings?> 
		
									<div class="large-3 medium-3 small-3 columns" style="top: 0px !important;left: 30% !important;top:20px !important;"> 
										
										<div class=""> 
											<span id="salesdetailsubmit" data-id="ADD"  title="Save" class="innerheader-title">
											<input id="" name="" tabindex="110" value="Save" class="alertbtnyes  ffield" type="button">
											<span class="actiontitle" style="color:#ffffff; top: -6px;">Save</span>
											</span>
											<span id="salesdetailupdate" data-id="UPDATE" title="Save" class="innerheader-title" style="display:none;" >
											<input id="" name="" tabindex="110" value="Save" class="alertbtnyes  ffield" type="button">
											<span class="actiontitle" style="color:#ffffff; top: -6px;">Save</span>
											</span>
										</div>
									</div>
							<?php } else { ?>
									<div class="large-12 columns" style="position:relative;left: 8% !important;top: 20px !important;"> 
										<div class="large-6 columns"> </div>
										<div class="large-6 medium-6 small-6 columns" >
											<span id="salesdetailsubmit" data-id="ADD"  title="Save" class="innerheader-title">
											<input id="" name="" tabindex="110" value="Save" class="alertbtnyes  ffield" type="button">
											<span class="actiontitle" style="color:black;">Save</span>
											</span>
											<span id="salesdetailupdate" data-id="UPDATE" title="Save" class="innerheader-title" style="display:none;" >
											<input id="" name="" tabindex="110" value="Save" class="alertbtnyes  ffield" type="button">
											<span class="actiontitle" style="color:#ffffff; top: -6px;">Save</span></span>
										</div>
									</div>
		<?php
	}?>
							</div>
	
						</span>
					</div>
				 </div>
			</form>
	<?php if($device == 'phone') {	?>
		<div id="summaryheaderform" class="large-8 medium-12 small-12 columns end itemdetailsopacity tabletinfo" style="opacity:0;    top: 0px !important;padding: 0px !important;">
			<div class="large-12 small-12  columns end paddingzero">
			<input type="hidden" id="finalbillamount" name="finalbillamount" value="" />
						<input type="hidden" id="finalbillwt" name="finalbillwt" value="" />
						<form class="summarypaymentaddform">
							<span class="foramounttype">
								<div class="large-12 small-12 columns cleardataform paddingzero padding-bottom" id="amountsummary" style="background: #fff9c4 !important;box-shadow:none;">
	<?php } else { ?>
		<div id="summaryheaderform" class="large-8 medium-12 small-8 columns end itemdetailsopacity paddingbtm tabletinfo" style="opacity:0;<?php if($device == 'tablet') echo 'top: 5px;padding-right: 0px !important; padding-left: 0px !important;'; else echo 'top: -5px'; ?> !important;padding-bottom:0px !important;">
			<div class="large-12 small-12  columns end z-depth-1 paddingzero borderstyle">
				<input type="hidden" id="finalbillamount" name="finalbillamount" value="" />
							<input type="hidden" id="finalbillwt" name="finalbillwt" value="" />
							<form class="summarypaymentaddform">
								<span class="foramounttype">
									<div class="large-12 small-12 columns cleardataform paddingzero padding-bottom" id="amountsummary" style="box-shadow:none;">
	<?php } ?>
						
							
									<div class="large-2 small-4  columns summarycontainer journalhideshow">
										<ul>
											<li> <span class="title" style="font-weight:bold;">A/c Opening<span id="accopenamtspan"></span></span><span id="spanaccopenamt" class="value"></span><span id="spanaccopenamtsymbol"></span></li>
										</ul>
										<input type="hidden" name="accountopeningamount" id="accountopeningamount" value="" />
										<input type="hidden" name="accountopeningamountirid" id="accountopeningamountirid" value="" />
									</div>
									<div class="large-2 small-4  columns summarycontainer billamountdiv contrahide journalhideshow">
										<ul>
											<li> <span class="title" style="font-weight:bold;">Bill Amt<span class="billnetamountspan"></span></span><span id="spanbillamt" class="value"></span></li>
										</ul>
									</div>
									<div class="large-2 small-4  columns summarycontainer estimatehide contrahide ">
										<ul>
											<li class=""> <span class="title" style="font-weight:bold;">Paid Amt</span><span id="sumpaidamt" class="value"></span></li>
											<input type="hidden" name="sumarypaidamount" id="sumarypaidamount" value="0" />
										</ul>
									</div>
									<div class="large-2 small-4  columns summarycontainer estimatehide">
										<ul>
											<li class=""> <span class="title" style="font-weight:bold;">Issue Amt</span><span id="sumissueamt" class="value"></span></li>
											<input type="hidden" name="sumaryissueamount" id="sumaryissueamount" value="0" />
										</ul>
									</div>
									<div class="large-2 small-4 columns summarycontainer estimatehide contrahide journalhideshow">
										<ul>
											<li> <span class="title" style="font-weight:bold;">Bill Balance<span class="billbalamountspan"></span></span><span id="sumpendingamt" class="value"></span></li>
											<input type="hidden" name="sumarypendingamount" id="sumarypendingamount" value="0" />
										</ul>
									</div>
									<div class="large-2 small-4  columns summarycontainer estimatehide journalhideshow repairhideshow">
										<ul>
											<li> <span class="title" style="font-weight:bold;">A/c Closing<span id="acccloseamtspan"></span></span><span id="spanacccloseamt" class="value"></span><span id="spanacccloseamtsymbol"></span></li>
											<input type="hidden" id="accountclosingamount" name="accountclosingamount" value="" />
										</ul>
									</div>		
									</div> 
								</span>

					<?php if($device=='phone') { ?>
						<div class="large-12 columns cleardataform paddingzero padding-bottom" style="box-shadow:none;" id="pureweightsummary" style="background: #fff9c4 !important;">
					<?php } else { ?>
						<div class="large-12 columns cleardataform paddingzero padding-bottom" style="box-shadow:none;" id="pureweightsummary">
					<?php } ?>
								
									<div class="large-2 medium-2 small-4 columns summarycontainer journalhideshow">
										<ul>
											<li> <span class="title" style="font-weight:bold;">A/c Open Wt<span id="spanaccopenwtsymbol"></span></span><span id="spanaccopenwt" class="value"></span></li>
										<ul>
											<input type="hidden" name="accountopeningweight" id="accountopeningweight" value="0" />
											<input type="hidden" name="accountopeningweightirid" id="accountopeningweightirid" value="0" />
									</div>
									<div class="large-2 medium-2 small-4 columns summarycontainer journalhideshow">
										<ul>
											<li> <span class="title" style="font-weight:bold;">Bill Wt<span class="billwtspan sumbillnetwtspan"></span></span><span id="spanbillwt" class="value"></span></li>
										<ul>
										<input type="hidden" name="billweighthidden" id="billweighthidden" value="0" />
									</div>
									<div class="large-2 medium-2 small-4 columns summarycontainer estimatehide ">
										<ul>
											<li> <span class="title" style="font-weight:bold;">Paid Wt</span><span id="spanpaidpurewt" class="value"></span></li>
										</ul>
										<input type="hidden" name="spanpaidpurewthidden" id="spanpaidpurewthidden" value="0" />
									</div>
									<div class="large-2 medium-2 small-4 columns summarycontainer estimatehide ">
										<ul>
											<li> <span class="title" style="font-weight:bold;">Issue Wt</span><span id="spanissuepurewt" class="value"></span></li>

										</ul>
										<input type="hidden" name="spanissuepurewthidden" id="spanissuepurewthidden" value="0" />
									</div>
									<div class="large-2 medium-2 small-4 columns summarycontainer estimatehide">
										<ul>
											<li> <span class="title" style="font-weight:bold;">Paid Grs Wt</span><span id="spanpaidgrswt" class="value"></span></li>
										</ul>
										<input type="hidden" name="spanpaidgrswthidden" id="spanpaidgrswthidden" value="0" />
									</div>
									<div class="large-2 medium-2 small-4 columns summarycontainer estimatehide ">
										<ul>
											<li> <span class="title">Issue Grs Wt</span><span id="spanissuegrswt" class="value"></span></li>

										</ul>
										<input type="hidden" name="spanissuegrswthidden" id="spanissuegrswthidden" value="0" />
									</div>
									<div class="large-2 medium-2 small-4 columns summarycontainer estimatehide journalhideshow">
										<ul>
											<li> <span class="title">Bill Bal Wt<span class="billbalwtspan sumbillnetwtspan"></span></span><span id="spanbillclosewt" class="value"></span></li>
										<ul>
										<input type="hidden" name="balpurewthidden" id="balpurewthidden" value="0" />
									</div>
									<div class="large-2 medium-2 small-4 columns summarycontainer estimatehide journalhideshow">
										<ul>
											<li> <span class="title">A/c Close Wt<span id="spanaccclosewtsymbol"></span></span><span id="spanaccclosewt" class="value"></span></li>
											<input type="hidden" id="accountclosingwt" name="accountclosingwt" value="" />
										<ul>
									</div>	
								</div>
							</form>
						</div>
					</div>
														<?php
	if($device=='phone') 
	{?>
					<div id="maincommonotherwtsummary" class="large-8 small-12 columns end itemdetailsopacity tabletinfo" style="opacity:0; padding: 0px !important;">
					<div class="large-12 small-12  columns end paddingzero">
					<span class="commonotherwtsummary">
							<div class="large-12 small-12 columns cleardataform paddingzero padding-bottom" id="commonotherwtsummary" style="background: #fff9c4 !important;">
	<?php } else { ?>
		<div id="maincommonotherwtsummary" class="large-8 small-8 columns end itemdetailsopacity paddingbtm tabletinfo" style="opacity:0; ">
					<div class="large-12 small-12  columns end z-depth-1 paddingzero borderstyle">
					<span class="commonotherwtsummary">
							<div class="large-12 small-12 columns cleardataform paddingzero padding-bottom" style="" id="commonotherwtsummary">
	<?php } ?>
						
							<div class="large-2 small-4 columns summarycontainer ">
								<ul>
									<li> <span class="title">G.Wt</span><span id="commonotherwtsummarygwt" class="value commonotherwtsummaryspan"></span></li>
								</ul>
							</div>
							<div class="large-2 small-4 columns summarycontainer ">
								<ul>
									<li> <span class="title">N.Wt</span><span id="commonotherwtsummarynwt" class="value commonotherwtsummaryspan"></span></li>
								</ul>
							</div>
							<div class="large-2 small-4 columns summarycontainer ">
								<ul>
									<li> <span class="title">Pieces</span><span id="commonotherwtsummarypieces" class="value commonotherwtsummaryspan"></span></li>
								</ul>
							</div>
							<div class="large-2 small-4 columns summarycontainer ">
								<ul>
									<li> <span class="title">Tags</span><span id="commonotherwtsummarytags" class="value commonotherwtsummaryspan"></span></li>
								</ul>
							</div>
							</div> 
						</span>
					</div>
				</div>
				
		<?php if($device=='phone') {	?>
			<div id="salesdetailleftgrid" class="large-8 small-12 columns paddingbtm tabletinfo" style="top: 35px; position: relative;padding:0px !important;">
				<div class="large-12 small-12 columns paddingzero">
					<div class="large-12 small-12 columns viewgridcolorstyle paddingzero">
		<?php }  else { ?>
						<div id="salesdetailleftgrid" class="large-8 small-8 columns paddingbtm tabletinfo" style="top: 15px; position: relative;<?php if($device == 'tablet') echo 'padding-left: 0px !important;padding-right: 0px !important;width:100%;'; ?>">
						<div class="large-12 small-12 columns paddingzero">
						<div class="large-12 small-12 columns viewgridcolorstyle paddingzero borderstyle">
		<?php } ?>		
							<div class="large-12 small-12 columns headerformcaptionstyle" style="padding: 0.2rem 0 0rem;">
								<span class="large-6 medium-6 small-6 columns text-left small-only-text-center" >Detail List</span>
								<div class="large-6 small-6 columns" style="text-align:right;">
									<span id="">
										<span id="salesdetailentryedit" class="editiconclass" style="padding-right: 0rem;" title="Edit"><i class="material-icons">edit</i></span>
									</span>
									<span id="" style="">
										<span id="salesdetailentrydelete" class="deleteiconclass" style="padding-right: 0rem;" title="Delete"><i class="material-icons">delete</i></span>
									</span>
								</div><br>
							</div>
							<?php
									echo '<div class="large-12 small-12 columns forgetinggridname" id="saledetailsgridwidth" style="padding-left:0;padding-right:0;height:410px;"><div class="desktop row-content inner-gridcontent" id="saledetailsgrid" style="max-width:2000px; height:370px;top:0px;">
										<!-- Table content[Header & Record Rows] for desktop-->
										</div>
										<!--<div class="inner-gridfooter footer-content footercontainer" id="saledetailsgridfooter">-->
											<!-- Footer & Pagination content -->
										<!--</div>--></div>';
								?>
						</div>
					</div>
				</div>		
				
				
				<?php
				if($device=='phone') 
	{
		?>
				<div id="paymentformhideid" class="large-8 small-12 columns end  itemdetailsopacity tabletinfo" style="opacity:0;top: 15px !important;padding: 0px !important;">	
				<div class="large-12 small-12 columns end paddingzero">
	<?php }
	else {
	?>	
	<div id="paymentformhideid" class="large-8 small-12 columns end  itemdetailsopacity  paddingbtm tabletinfo" style="opacity:0;top: 15px !important;<?php if($device == 'tablet') echo 'padding-right: 0px !important; padding-left: 0px !important;'; ?>">	
				<div class="large-12 small-12 columns end z-depth-1 paddingzero borderstyle">
	<?php } ?>					
					
					<form method="POST" name="paymentaddform" id="paymentaddform" class="paymentaddform validationEngineContainer">
						<div id="paymenteditvalidate" class="validationEngineContainer">
							<?php
				if($device=='phone') 
	{
		?>
						<div class="large-12 small-12 columns end paddingzero" style="background: #f2f3fa!important;padding:0px !important;">
	<?php } else {?>
		<div class="large-12 small-12 columns end paddingzero" style="background: #fff!important;transition: box-shadow .25s;border-radius: 0;">
	<?php } ?>
						<div id="paymenthide">
										<div class="static-field large-2 small-6 columns hidedisplay">
											<label>Mode<span class="mandatoryfildclass" id="modeid_req">*</span></label>
											<select id="paymentmodeid" name="paymentmodeid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="101">									
												<?php	foreach($salesmode as $key):
												if($key->salesmodeid == 3){
												?>
													<option value="<?php echo $key->salesmodeid;?>" ><?php echo $key->salesmodename;?></option>
												<?php } endforeach;?>	
											</select>
										</div>
										<div id="paymentstocktypeid-div" class="static-field large-4 medium-3 small-6 columns <?php if($device != 'tablet') echo 'fourteenwidth'; ?>" >
											<label>Type<span class="mandatoryfildclass" id="paymentstocktypeid_req">*</span></label>
											<select id="paymentstocktypeid" name="paymentstocktypeid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="102">									
												<option value=""></option>
												<?php 
													foreach($stocktype->result() as $key):
													?>
												<option value="<?php echo $key->stocktypeid;?>" data-label="<?php echo $key->stocktypelabel;?>" data-paymentstocktypeidhidden="<?php echo $key->stocktypename;?>" ><?php echo $key->stocktypename;?></option>
													<?php  endforeach;?>	
											</select>
											<input type="hidden" id="paymentstocktypelabel" name="paymentstocktypelabel"/>
										</div>
										<div class="static-field large-2 medium-3 small-6 columns">
											<label>I.R Type<span class="mandatoryfildclass" id="modeid_req">*</span></label>
											<select id="issuereceipttypeid" name="issuereceipttypeid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="101" disabled="disabled">									
												<?php	foreach($issuereceipttype as $key):
												?>
													<option value="<?php echo $key->issuereceipttypeid;?>" data-issuereceipttypeidhidden="<?php echo $key->issuereceipttypename;?>"><?php echo $key->issuereceipttypename;?></option>
												<?php endforeach;?>	
											</select>
										</div>
										
										<div class="static-field large-6 medium-3 small-6 columns <?php if($device != 'tablet') echo 'eightenwidth'; ?>" id="paymentaccountdiv" >
											<label>Account</label>		
											<select id="paymentaccountid" name="paymentaccountid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="102">									
												<option value=""></option>
												<?php 
													foreach($paymentaccount as $key):
														$paymentaccountvalue = $key->accountname;
												?>												
												<option value="<?php echo $key->accountid;?>" data-accountcatalogid="<?php echo $key->accountcatalogid;?>" data-paymentaccountidhidden="<?php echo $paymentaccountvalue;?>"  data-accounttypeid="<?php echo $key->accounttypeid;?>"><?php echo $paymentaccountvalue;?></option>
												<?php  endforeach;?>	
											</select>
										</div>
										<div class="static-field large-2 columns text-left acnt-dropdown end" id="journalaccountiddiv">
											<label>Journal Account</label>
											<input type="hidden" name="journalaccountid" id="journalaccountid"/>
										</div>
										<div class="static-field large-2 small-6 columns clearonpaymentstocktypechange" id="advanceavailable-div">	
											<input type="text" class="" id="advanceavailable" name="advanceavailable" value="" tabindex="127" data-prompt-position="bottomLeft:14,36" readonly>
											<label for="advanceavailable">Adv Available<span class="mandatoryfildclass" id="advanceavailable_req"></span> </label>
										</div>
										<div class="static-field large-3 small-12 columns clearonpaymentstocktypechange " id="paymentproduct-div">
											<label>Product<span class="mandatoryfildclass" id="paymentproduct_req">*</span></label>	
											<select id="paymentproduct" name="paymentproduct" class="chzn-select "  data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="111">
											<option value=""></option>
											<?php 
											foreach($bullionproduct->result() as $key):
											?>
											<option value="<?php echo $key->productid;?>" data-purchasechargeid="<?php echo $key->purchasechargeid;?>" data-chargeid="<?php echo $key->chargeid;?>" data-counterid="<?php echo $key->counterid;?>" data-purityid="<?php echo $key->purityid;?>" data-label="<?php echo $key->productname;?>" data-bullionapplicable="<?php echo $key->bullionapplicable;?>" data-taxid="<?php echo $key->taxid;?>" data-taxdataid="<?php echo $key->taxdataid;?>" data-categoryid="<?php echo $key->categoryid;?>" data-kacha="<?php echo $key->kacha;?>" data-paymentproducthidden="<?php echo $key->productname;?>" >
											<?php if($mainlicense['productidshow'] == 'YES') { echo $key->productid.' - '.$key->productname; } else { 
											echo $key->productname; } ?></option>
											<?php endforeach;?>
											</select>
										</div>
										<div class="static-field large-2 small-6 columns clearonpaymentstocktypechange " id="paymentpurity-div">
											<label>Purity<span class="mandatoryfildclass" id="paymentpurity_req">*</span></label>
											<select id="paymentpurity" name="paymentpurity" class="chzn-select " data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="113">
												<option value=""></option>
												<?php $prev = ' ';
													foreach($purity->result() as $key):
													$cur = $key->metalid;
													$a="";
													if($prev != $cur )
													{
														if($prev != " ")
														{
															 echo '</optgroup>';
														}
														echo '<optgroup  label="'.$key->metalname.'">';
														$prev = $key->metalid;
													}
												?>
												<option data-metalpurity_id="<?php echo $key->purityrateid;?>"  data-metalset-purity_id="<?php echo $key->purity_id;?>" data-metalid="<?php echo $key->metalid;?>" data-melting="<?php echo $key->melting;?>" value="<?php echo $key->purityid;?>" data-label="<?php echo $key->purityname;?>" data-paymentpurityhidden="<?php echo $key->purityname;?>"><?php echo $key->purityname;?></option>
												<?php endforeach;?>
											</select>
										</div>
										<?php if(strtolower($mainlicense['counter']) == 'yes') {?>	
										<div class="static-field large-2 small-6 columns clearonpaymentstocktypechange " id="paymentcounter-div">
											<label>Storage<span class="mandatoryfildclass " id="paymentcounter_req">*</span></label>
											<select id="paymentcounter" name="paymentcounter" class="chzn-select " data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="114">
											<option value=""> </option>
											<?php  foreach($paymentcounter->result() as $key):?>
											<option value="<?php echo $key->counterid;?>" data-defcounterids="<?php echo $key->counterdefault;?>"  data-defparentcounterids="<?php echo $key->counterdefaultid;?>" data-label="<?php echo $key->countername;?>" data-paymentcounterhidden="<?php echo $key->countername;?>"> <?php echo $key->counterid.' - '.$key->countername;?></option>
											<?php endforeach;?>
											</select>
											<input type="hidden" name="paymentprocesscounterid" id="paymentprocesscounterid" value="1" />
										</div>
										<?php } ?>
										<div class="input-field large-2 small-6 columns clearonpaymentstocktypechange " id="paymentgrossweight-div">
											<input type="text" class="payemntpurewtpayment" id="paymentgrossweight" data-calc="GWT" name="paymentgrossweight" value=""  data-validation-engine="" tabindex="115" maxlength="15">
											<label for="paymentgrossweight">G.WT<span class="mandatoryfildclass" id="paymentgrossweight_req">*</span><span class="validatewtfontize" id="paymentgrossweight_validatewt"></label>
										</div>
										<div class="input-field large-2 small-6 columns  clearonpaymentstocktypechange " id="paymentnetweight-div">
											<input type="text" class="payemntpurewtpayment" data-calc="NWT" data-validategreat="paymentgrossweight" id="paymentnetweight" name="paymentnetweight" value="" data-validation-engine="" tabindex="117"/>
											<label for="paymentnetweight">N.WT<span class="mandatoryfildclass" id="paymentnetweight_req">*</span><span class="validatewtfontize" id="paymentnetweight_validatewt"></label>
										</div>
										<div class="input-field large-2 small-6 columns  clearonpaymentstocktypechange " id="paymentlessweight-div">
											<input type="text" class="payemntpurewtpayment" data-calc="NWT" data-validategreat="paymentgrossweight" id="paymentlessweight" name="paymentlessweight" value="" data-validation-engine="" tabindex="117"/>
											<label for="paymentlessweight">Less.WT<span class="mandatoryfildclass" id="paymentlessweight_req">*</span><span class="validatewtfontize" id="paymentlessweight_validatewt"></label>
										</div>
										<div class="input-field large-1 small-6 columns  clearonpaymentstocktypechange " id="paymentpieces-div">
											<input type="text" class="payemntpurewtpayment" data-calc="Pieces" data-validategreat="" id="paymentpieces" name="paymentpieces" value="" data-validation-engine="" tabindex="117"/>
											<label for="paymentpieces">Pieces<span class="mandatoryfildclass" id="paymentpieces_req">*</span><span class="validatewtfontize" id="paymentpieces_validatewt"></label>
										</div>
										<div class="static-field large-2 medium-3 small-6 columns clearonpaymentstocktypechange <?php if($device != 'tablet') echo 'twelevewidth'; ?>" id="cardtype-div" >
											<label>Type<span class="mandatoryfildclass" id="cardtype_req"></span> </label><select id="cardtype" name="cardtype" class="chzn-select" data-prompt-position="bottomLeft:14,36" data-validation-engine="" tabindex="128"> 
												<option value=""></option>
												<?php foreach($cardtype as $key):?>
												<option value="<?php echo $key->cardtypeid;?>" data-cardgroupid="<?php echo $key->cardgroupid;?>" data-cardtypehidden="<?php echo $key->cardtypename;?>" data-label="<?php echo $key->cardtypename;?>">
													<?php echo $key->cardtypename;?></option>
												<?php endforeach;?>	 
											</select>
										</div>
										<div class="static-field large-2 medium-3 small-6 columns clearonpaymentstocktypechange <?php if($device != 'tablet') echo 'twelevewidth'; ?>" id="bankname-div">	
											<input type="text" class="" id="bankname" name="bankname" value="" tabindex="127" data-prompt-position="bottomLeft:14,36" data-validation-engine="" maxlength="50">
											<label for="bankname">Bank<span class="mandatoryfildclass" id="bankname_req"></span> </label>
										</div>
										 <div class="input-field large-2 medium-3 small-6 columns clearonpaymentstocktypechange <?php if($device != 'tablet') echo 'twelevewidth'; ?>" id="paymentrefnumber-div" >	
											<input type="text" class="" id="paymentrefnumber" name="paymentrefnumber" value="" tabindex="124" data-validation-engine="" maxlength="50">
											<label for="paymentrefnumber">Number<span class="mandatoryfildclass" id="paymentrefnumber_req"></span> </label>
										</div>
										<div class="input-field large-2 medium-3 small-6 columns clearonpaymentstocktypechange <?php if($device != 'tablet') echo 'twelevewidth'; ?>" id="paymentreferencedate-div" >
											<input id="paymentreferencedate" class="fordatepicicon" type="text" name="paymentreferencedate" tabindex="125" readonly="readonly" data-prompt-position="bottomLeft" data-dateformater="dd-mm-yy">
											<label for="paymentreferencedate">Date<span class="mandatoryfildclass" id="paymentreferencedate_req"></span> </label>
									   </div>
									   <div class="input-field large-2 medium-3 small-6 columns clearonpaymentstocktypechange <?php if($device != 'tablet') echo 'twelevewidth'; ?>" id="approvalcode-div" >	
											<input type="text" class="" id="approvalcode" name="approvalcode" value="" tabindex="126" data-validation-engine="" maxlength="50">
											<label for="approvalcode">Appr Code<span class="mandatoryfildclass" id="approvalcode_req"></span> </label>
										</div>
										<div id="chitschemetypeid-div" class="static-field large-2 small-6 columns clearonpaymentstocktypechange">
											<label for="chitschemetypeid">Type</label>							
											<select data-validation-engine="" class="chzn-select" id="chitschemetypeid" name="chitschemetypeid" data-prompt-position="topLeft:14,36">
												<option value=""></option>
												<option value="2">Amount</option>
												<option value="3">Gram</option>
											</select>
										</div> 
										<div class="input-field large-2 small-6 columns clearonpaymentstocktypechange twelevewidth" id="chitbookno-div" >
											<input type="text" class="" id="chitbookno" name="chitbookno" value=""  tabindex="106">
											<label for="chitbookno">Number<span class="mandatoryfildclass">*</span></label>	
										</div>
										<div class="input-field large-2 small-6 columns clearonpaymentstocktypechange twelevewidth" id="chitbookname-div">
										<input type="text" class="" id="chitbookname" name="chitbookname" value=""  tabindex="107">
											<label for="chitbookname">Name</label>
										</div>
										<div class="input-field large-2 small-6 columns clearonpaymentstocktypechange twelevewidth" id="chitamount-div" >
											<input type="text" class="" data-validation-engine="" id="chitamount" name="chitamount" value="0"  tabindex="108" >
											<label for="chitamount">Amount</label>
										</div>
										<div class="input-field large-2 small-6 columns clearonpaymentstocktypechange twelevewidth" id="totalpaidamount-div" >
											<input type="text" class="" data-validation-engine="" id="totalpaidamount" name="totalpaidamount" value="0"  tabindex="109">
											<label for="totalpaidamount">Bonus</label>								
										</div>
										<div class="input-field large-2 small-6 columns clearonpaymentstocktypechange twelevewidth" id="chitgram-div" >
											<input type="text" class="" id="chitgram" name="chitgram" value=""  tabindex="108" >
											<label for="chitgram">Chit G/m</label>								
										</div>
										<div class="input-field large-2 small-6 columns clearonpaymentstocktypechange twelevewidth" id="grambonus-div" >
											<input type="text" class="" id="grambonus" name="grambonus" value=""  tabindex="109">
											<label for="grambonus">Bonus Gram</label>								
										</div>
										<div class="input-field large-2 small-6 columns clearonpaymentstocktypechange" id="paymentratepergram-div" >
											<input type="text" class="" id="paymentratepergram" name="paymentratepergram" data-validation-engine="" tabindex="121" maxlength="15">
											<label for="paymentratepergram">Rate/gm<span class="mandatoryfildclass" id="ratepergram_req">*</span></label>
										</div>
										<div class="input-field large-2 medium-3 small-6 columns clearonpaymentstocktypechange <?php if($device != 'tablet') echo 'twelevewidth'; ?>"  id="paymenttotalamount-div" >
											<input type="text" class="" keyword="paymenttotalnetamount" id="paymenttotalamount" name="paymenttotalamount" data-validation-engine="" tabindex="133" value="0">
											<label for="paymenttotalamount">Nt Amt<span class="mandatoryfildclass" id="paymenttotalamount_req"></span></label>
										</div>
										<div class="input-field large-2 medium-3 small-6 columns clearonpaymentstocktypechange twelevewidth"  id="bhavcuttotalamount-div" >
											<input type="text" class="" keyword="bhavcuttotalamount" id="bhavcuttotalamount" name="bhavcuttotalamount" data-validation-engine="" tabindex="133" maxlength="15">
											<label for="bhavcuttotalamount">Nt Amt<span class="mandatoryfildclass" id="bhavcuttotalamount_req"></span></label>
										</div>
										<div class="static-field large-2 medium-3 small-6 columns clearonpaymentstocktypechange twelevewidth" id="advancebalance-div" >	
											<input type="text" class="" id="advancebalance" name="advancebalance" value="" tabindex="127" data-prompt-position="bottomLeft:14,36" readonly>
											<label for="advancebalance">Adv Bal</label>
										</div>
										<div class="input-field large-2 medium-3 small-6 columns clearonpaymentstocktypechange twelevewidth" id="paymentmelting-div" >
											<input type="text" class="payemntpurewtpayment" id="paymentmelting" name="paymentmelting" value="" tabindex="129" data-validation-engine="">
											<label for="paymentmelting">Melting<span class="mandatoryfildclass" id="paymentmelting_req"></span> </label>
										</div>
										<div class="input-field large-2 medium-3 small-6 columns clearonpaymentstocktypechange twelevewidth" id="newpaymenttouch-div" >
											<input type="text" class="payemntpurewtpayment" id="newpaymenttouch" name="newpaymenttouch" value="" tabindex="130" data-validation-engine="" maxlength="15">
											<label for="newpaymenttouch">Touch<span class="mandatoryfildclass" id="newpaymenttouch_req"></span> </label>
										</div>
										<div class="input-field large-3 medium-3 small-6 columns clearonpaymentstocktypechange twelevewidth" id="paymentpureweight-div" >
											<input type="text" class="" id="paymentpureweight" name="paymentpureweight" data-validation-engine="" tabindex="134" readonly maxlength="15">
											<label for="paymentpureweight">Pure Weight<span class="mandatoryfildclass" id="paymentpureweight_req">*</span></label>
										</div>
										<div class="static-field large-3 medium-4 small-5 columns hidedisplay" id="paymentemployeepersonid-div">
											<label>Employee</label>						
											<select id="paymentemployeepersonid" name="paymentemployeepersonid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="1004">
											<option value=""></option>
											<?php $prev = ' ';
												foreach($employeedata as $key):
											?>
												<option data-name="<?php echo $key->employeename;?>" value="<?php echo $key->employeeid;?>" data-paymentemployeepersonidhidden="<?php echo $key->employeename;?>"><?php echo $key->employeeid."-".$key->employeename;?></option>
												<?php endforeach;?>						
											</select>
										</div>
										<!--<div class="static-field large-4 columns" id="balancedescription-div">
											<textarea name="balancedescription" class="materialize-textarea" id="balancedescription" tabindex="128" rows="1" style="min-height:3rem;padding:0px !important;" cols="5" maxlength="200"></textarea>
											<label for="balancedescription">Bill Comment</label>
										</div>--> 
											
									</div>
									</div>
								</div>
								<?php
				if($device=='phone') 
	{
		?>
								<div class="large-6 small-6  columns righttext" style="position:relative;left: 10% !important;top:10px !important;">
	<?php }  else { ?>
								<div class="large-6 small-6  columns righttext" style="position:relative;">
	<?php } ?>
							
							<span id="paymentdetailsubmit"  title="Save" class="innerheader-title" data-id="ADD">
								<input id="" name="" tabindex="110" value="Enter" class="alertbtnyes  ffield" type="button">
								<span class="actiontitle" style="color:#ffffff;line-height:20.2px;top: -6px;">Save</span>
							</span>
								<span id="paymentdetailupdate"  title="Save" class="innerheader-title" style="display:none;" data-id="UPDATE">
								<input id="" name="" tabindex="110" value="Enter" class="alertbtnyes  ffield" type="button">
								<span class="actiontitle" style="color:#ffffff;top: -6px;">Save</span>
								</span>
							</div>	
								</div>
								</form>
								
					
					
					
						<?php
				if($device=='phone') 
	{
		?>
		<span class="foramounttype">
						<div class="large-12 small-12 columns paddingzero" id="tabindexhideshow">
						<div class="large-12 small-12 columns cleardataform" style="box-shadow: none;padding: 0px !important;    background: #fff9c4 !important;">
	<?php } else { ?>
	<span class="foramounttype">
						<div class="large-12 small-12 columns paddingzero borderstyle" id="tabindexhideshow" style="background:#fff;">	
	<div class="large-12 small-12 columns cleardataform" style="box-shadow: none;padding: 0px !important;padding-bottom:2rem !important;">
	<?php } ?>
						<div id="discounthide">
						<div class="static-field large-2 medium-3 small-4 columns" id="billgrossamount-div">
						<input type="text" class="" id="billgrossamount" name="billgrossamount" value="0" tabindex="127" data-prompt-position="bottomLeft:14,36" data-validation-engine="" readonly="readonly">
							<label for="billgrossamount">Gr Amt </label>
						</div>
						<div class="static-field large-1 medium-3 small-4 columns" id="sumarydiscountlimit-div" style="display:none;">	
							<input type="text" class="" id="sumarydiscountlimit" name="sumarydiscountlimit" value="" tabindex="127" data-prompt-position="bottomLeft:14,36" data-validation-engine="" readonly>
							<input type="hidden" class="" id="discountempid" name="discountempid" value="">
							<label for="sumarydiscountlimit">Disc Limit </label>
						</div>
						<div class="static-field large-1 medium-3 small-4 columns" id="sumarydiscountamount-div" style="width:12% !important;">	
							<input type="text" class="" id="sumarydiscountamount" name="sumarydiscountamount" value="" tabindex="127" data-prompt-position="bottomLeft:14,36" data-validation-engine="" style="position: relative;">
							<label for="sumarydiscountamount">D<span id="discountpercentvalue"></span><span id="discountcalname" style="font-size: 0.8rem;"></span></label>
						</div>
						<div class="static-field large-1 medium-3 small-4 columns" id="sumarytaxamount-div" style="<?php if($device != 'tablet') echo 'width:12% !important;'; ?>">	
							<input type="text" class="" id="sumarytaxamount" name="sumarytaxamount" value="0" tabindex="127" data-prompt-position="bottomLeft:14,36" data-validation-engine="" readonly="readonly">
							<label for="sumarytaxamount">Tax</label>
						</div>
						<div class="static-field large-2 medium-3 small-4 columns" id="oldjewelamtsummary-div" style="<?php if($device != 'tablet') echo 'width:13% !important;'; ?>">	
						<input type="text" class="" id="oldjewelamtsummary" name="oldjewelamtsummary" value="0" tabindex="127" data-prompt-position="bottomLeft:14,36" data-validation-engine="" readonly>
							<label for="oldjewelamtsummary">O.P Amt</label>
						</div>
						<div class="static-field large-2 medium-3 small-4 columns" id="salesreturnsummary-div" style="<?php if($device != 'tablet') echo 'width:13% !important;'; ?>">	
						<input type="text" class="" id="salesreturnsummary" name="salesreturnsummary" value="0" tabindex="127" data-prompt-position="bottomLeft:14,36" data-validation-engine="" readonly>
							<label for="salesreturnsummary">S.R Amt</label>
						</div>
						<div class="static-field large-2 medium-3 small-4 columns" id="purchasereturnsummary-div" style="<?php if($device != 'tablet') echo 'width:13% !important;'; ?>">	
						<input type="text" class="" id="purchasereturnsummary" name="purchasereturnsummary" value="0" tabindex="127" data-prompt-position="bottomLeft:14,36" data-validation-engine="" readonly>
							<label for="purchasereturnsummary">P.R Amt</label>
						</div>
						<div class="static-field large-2 medium-3 small-4 columns" id="orderadvamtsummary-div" style="<?php if($device != 'tablet') echo 'width:13% !important;'; ?>">	
						<input type="text" class="" id="orderadvamtsummary" name="orderadvamtsummary" value="0" tabindex="127" data-prompt-position="bottomLeft:14,36" data-validation-engine="" maxlength="15">
							<label for="orderadvamtsummary">A.Amt<span class="oradvamtspan"></span></label>
						</div>
						<div class="static-field large-1 medium-3 small-4 columns" id="roundvalue-div" style="<?php if($device != 'tablet') echo 'width:12% !important;'; ?>">
						<input type="text" class="" id="roundvalue" name="roundvalue" value="0" tabindex="127" data-prompt-position="bottomLeft:14,36" data-validation-engine="">
							<label for="roundvalue">Adjustment</label>
						</div>
						<div class="static-field large-2 medium-3 small-4 columns" id="billnetamount-div" style="background: #fff9c4!important;">
						<input type="text" class="" id="billnetamount" name="billnetamount" value="0" tabindex="127" data-prompt-position="bottomLeft:14,36" data-validation-engine="" readonly="readonly">
							<label for="billnetamount">Net Amt<span class="billnetamountspan"></span> </label>
						</div>
						</div>
					</div>
					<?php
				if($device=='phone') 
	{
		?>
					<div class="large-12 small-12 columns cleardataform" style="box-shadow: none;padding: 0px !important;background: #fff9c4 !important;">
					<?php } else { ?>
					<div class="large-12 small-12 columns cleardataform" style="box-shadow: none;padding: 0px !important;">
					<?php } ?>
						<div id="weightsummaryhide" style="display:none;">
							<div class="static-field large-2 small-4 columns" id="billforgrosswt-div">
							<input type="text" class="" id="billforgrosswt" name="billforgrosswt" value="0" tabindex="127" data-prompt-position="bottomLeft:14,36" data-validation-engine="" readonly="readonly">
								<label for="billforgrosswt">Gross Wt </label>
							</div>
							<div class="static-field large-2 small-4 columns" id="billgrosswt-div">
							<input type="text" class="" id="billgrosswt" name="billgrosswt" value="0" tabindex="127" data-prompt-position="bottomLeft:14,36" data-validation-engine="" readonly="readonly">
								<label for="billgrosswt">Pure Gr.Wt </label>
							</div>
							<div class="static-field large-2 small-4 columns" id="oldjewelwtsummary-div">
							<input type="text" class="" id="oldjewelwtsummary" name="oldjewelwtsummary" value="0" tabindex="127" data-prompt-position="bottomLeft:14,36" data-validation-engine="" readonly>
								<label for="oldjewelwtsummary">O.P Wt</label>
							</div>
							<div class="static-field large-2 small-4 columns" id="salesreturnwtsummary-div">
							<input type="text" class="" id="salesreturnwtsummary" name="salesreturnwtsummary" value="0" tabindex="127" data-prompt-position="bottomLeft:14,36" data-validation-engine="" readonly>
								<label for="salesreturnwtsummary">S.R Wt</label>
							</div>
							<div class="static-field large-2 small-4 columns" id="purchasereturnwtsummary-div">
							<input type="text" class="" id="purchasereturnwtsummary" name="purchasereturnwtsummary" value="0" tabindex="127" data-prompt-position="bottomLeft:14,36" data-validation-engine="" readonly>
								<label for="purchasereturnwtsummary">P.R Wt</label>
							</div>
							<div class="static-field large-2 small-4 columns" id="billnetwt-div">
							<input type="text" class="" id="billnetwt" name="billnetwt" value="0" tabindex="127" data-prompt-position="bottomLeft:14,36" data-validation-engine="" readonly="readonly">
								<label for="billnetwt">Pure Nt.Wt<span class="billwtspan sumbillnetwtspan"></span> </label>
							</div>
							<div class="static-field large-2 small-4 columns" id="billfornetwt-div">
							<input type="text" class="" id="billfornetwt" name="billfornetwt" value="0" tabindex="127" data-prompt-position="bottomLeft:14,36" data-validation-engine="" readonly="readonly">
								<label for="billfornetwt">Net Wt (Pcs: <span class="billforpieces" id="billforpieces" value='0'></span>)</label>
							</div>
						</div>
					</div>
		</span>
				</div>
			</div> 
			
			<span class="hidedisplay" id="stonedetails">
				<div class="large-4 columns paddingbtm">	
					<div class="large-12 columns cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;">
						<div class="large-12 columns headerformcaptionstyle">Stone Details</div>
						<div class="large-6 columns ">
							<label>Stone Name</label>								
							<input type="text" class="" id="" name="" value="" tabindex="131">
						</div>
						<div class="large-6 columns ">
							<label>Type</label>								
							<select id="" name="" class="chzn-select" tabindex="132">
								<option></option>
							</select>
						</div>
						<div class="large-6 columns">
							<label>Carat</label>						
							<input type="text" class="" id="" name="" value="" tabindex="133">
						</div>
						<div class="large-6 columns">
							<label>Pieces</label>						
							<input type="text" class="" id="" name="" value="" tabindex="134">
						</div>
						<div class="large-6 columns">
							<label>Unit Rate</label>						
							<input type="text" class="" id="" name="" value="" tabindex="135">
						</div>
						<div class="large-6 columns">
							<label>Amount</label>						
							<input type="text" class="" id="" name="" value="" tabindex="136">
						</div>						
						<div class="large-12 columns " style="text-align:center">
							<label>&nbsp;</label>						
							<input type="button" class="btn leftformsbtn" id="" name="" value="Submit" tabindex="137">
						</div>
					</div>
				</div>
				<div class="large-8 columns paddingbtm">	
					<div class="large-12 columns viewgridcolorstyle" style="padding-left:0;padding-right:0;">	
						<div class="large-12 columns headerformcaptionstyle" style="padding: 0.5rem 0 0.4rem;">	
							<span>Stone Details</span>
						</div>
						<div class="large-12 columns" style="padding-left:0;padding-right:0;">
							<table id="stonegrid"> </table> 
							<div id="stonegriddiv" class="w100"></div>  		
						</div>
					</div>
				</div>
			</span>
			<div class="row">&nbsp;</div>
			</div>
	<div id="subformspan2" class="hidedisplay hiddensubform "> 
		
		<!-- ---------- Ezetap Account Notification Overlay  ------- -->					
		<div class="large-6 columns" style="position:absolute;">
		<div class="overlay" id="ezetapnotificationoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">	
			<div class="row" style="padding-top:180px;">&nbsp;</div>
			<div class="large-3 medium-10 large-centered medium-centered columns">
			<div class="large-12 columns headercaptionstyle headerradius">
			<div class="large-8 medium-8 small-8 columns headercaptionleft"> Account Details
			</div>
			<div class="large-4 medium-4 small-8 columns righttext small-only-text-center" style="text-align:right;">					
			<span class="" title="Close" id="ezetapcommentclose">
				<i title="Close" class="material-icons">close </i>
				<span class="actiontitle">Close</span>
			</span>
			</div>
	        </div>
			<div class="large-12 columns validationEngineContainer" style="" id="ezetapform">
			<div class="input-field large-12 columns" >
				<input type="text" class="" id="accmobileno" name="accmobileno" data-validation-engine="validate[required,custom[number]]"  tabindex=""/>
				<label for="accmobileno">Mobile No<span class="mandatoryfildclass" id=""></span></label>
			</div>
			<div class="input-field large-12 columns">
				<input type="text" class="" id="accemail" name="accemail" data-validation-engine="validate[required,custom[email]]" tabindex=""/>
				<label for="accemail">Email<span class="mandatoryfildclass" id="delivered_req"></span></label>	
			</div>
			<div class="large-12 columns">
			<input type="button" class="btn leftformsbtn" id="accnotify" name="accnotify" value="Save" tabindex="180">
			</div>
			</div>
			</div>
			</div>
			</div>
		<!-- ---------- Ezetap Account Notification Overlay  ------- -->
		<!-- ---------- Ezetap Response Notification Overlay  ------- -->					
		<div class="large-6 columns" style="position:absolute;">
		<div class="overlay" id="ezetapresponseoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">	
			<div class="row" style="padding-top:180px;">&nbsp;</div>
			<div class="large-6 medium-10 large-centered medium-centered columns">
			<div class="large-12 columns headercaptionstyle headerradius">
			<div class="large-8 medium-8 small-8 columns headercaptionleft">EzeTap Response</div>
			<div class="large-4 medium-4 small-8 columns righttext small-only-text-center" style="text-align:right;">					
			<span class="fa fa-times" title="Close" id="ezetapresponseclose"> </span>
			</div>
	        </div>
			<div class="large-12 columns validationEngineContainer" style="" id="ezetapform">
			<div id="ezemsg"></div>
			</div>
			</div>
			</div>
			</div>
		<!-- ---------- Ezetap Response Notification Overlay  ------- -->
		
	</div>
</div>
<!--Unsaved Bill ---->
<div class="large-6 columns" style="position:absolute;">
	<div class="overlay" id="unsavedbilloverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="large-10 medium-10 large-centered medium-centered columns">
			<div class="large-12 columns headercaptionstyle headerradius">
				<div class="large-6 medium-6 small-6 columns headercaptionleft paddingzero">
				<span id="billnameset">Unsaved Bill</span><span class="fwgpaging-box billgridfootercontainer"></span>
				</div>
				<div class="large-5 medium-6 small-6 columns addformheadericonstyle paddingzero" style="text-align:right;padding-right:0 !important">
					<span id="billediticon" class="icon-box" title="Edit"><i class="material-icons" >edit</i> </span>
					<span id="billdeleteicon" class="icon-box" title="Delete"><i class="material-icons">delete</i> </span>
					<span id="billprinticon"  class="icon-box"  title="Print"> <i class="material-icons">print</i></span>
					<span id="billcloseicon"  class="icon-box"  title="Close"><i class="material-icons">close</i></span>
				</div>
			</div>
			<?php
				echo '<div class="large-12 columns forgetinggridname" id="billgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="billgrid" style="max-width:2000px; height:420px;top:0px;">
					<!-- Table content[Header & Record Rows] for desktop-->
					</div>
					<div class=" footer-content footercontainer" id="billgridfooter">
						<!-- Footer & Pagination content -->
					</div></div>';
			?>	
		</div>
	</div>
</div>

<!--Sales Overlays Tax Chit and Discount-->
<?php $this->load->view('salesoverlays'); ?>
<!--billref no overlay-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="billrefnocreate" style="overflow-y: scroll;overflow-x: hidden;z-index:45;">	
		<div class="large-10 medium-10 small-12 large-centered medium-centered small-centered columns">
			<div class="large-12 column paddingzero"  style="background:transparent">
				<div class="row sectiontoppadding">&nbsp;</div>
				<div class="large-4 columns paddingbtm end large-centered" id="billrefnosection">
				<form id="billrefnoform" name="billrefnoform" class="validationEngineContainer billrefnoform">
					<div class="large-12 columns paddingzero border-modal-8px borderstyle" style="background-color:#fff">
					<div class="large-12 columns sectionheaderformcaptionstyle">Supplier Bill / Ref / Own Credit No</div>
					<span class="firsttab" tabindex="1000"></span>
					<div class="input-field large-12 columns">
						<input id="billrefnooverlaytbox" class="validate[required]" type="text" data-validatefield="" data-table=""  value="" name="billrefnooverlaytbox" data-primaryid="" maxlength="100">
						<label for="billrefnooverlaytbox">Supplier Bill / Ref / Own Credit No<span class="mandatoryfildclass">*</span></label>
					</div>
							
					<div class="large-12 columns sectionalertbuttonarea" style="float: right;margin-right: 6px;">
							<input type="button" class="alertbtnno  formtogridtagcharge close" id="closebillrefnooverlay" name="" value="Close" tabindex="1013">
							<span class="lasttab" tabindex="1014"></span>
					</div>
					</div>
				</form>
				</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>				
			</div>
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
	</div>
</div>
<!--ttypesettings no overlay-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="ttypesettingscreate" style="overflow-y: scroll;overflow-x: hidden;z-index:45;">	
		<div class="large-10 medium-10 small-12 large-centered medium-centered small-centered columns">
			<div class="large-12 column paddingzero"  style="background:transparent">
				<div class="row sectiontoppadding">&nbsp;</div>
				<div class="large-4 columns paddingbtm end large-centered" id="ttypesettingssection">
				<form id="ttypesettingsform" name="ttypesettingsform" class="validationEngineContainer ttypesettingsform">
					<div class="large-12 columns paddingzero borderstyle" style="background-color:#fff">
					<div class="large-12 columns sectionheaderformcaptionstyle">Settings</div>
					<span class="firsttab" tabindex="1000"></span>
					<div class="static-field large-6 small-6 columns ">
						<label>Class Name</label>						
						<select id="transactionmanageid" name="transactionmanageid" class="chzn-select" tabindex="1011">
							<?php foreach($transactionmanageid->result()  as $key):?>
							<option value="<?php echo $key->transactionmanageid;?>" data-additionalchargeid="<?php echo $key->additionalchargeid; ?>"
							data-pureadditionalchargeid="<?php echo $key->pureadditionalchargeid; ?>"
							data-transactionmode="<?php echo $key->transactionmodeid;?>"
							data-stocktypeid="<?php echo $key->stocktypeid;?>"
							data-salesmodeid="<?php echo $key->salesmodeid;?>"
							data-rateaddition="" data-roundingtype="<?php echo $key->roundingtype;?>"
							data-discounttype="<?php echo $key->discountmodeid;?>"
							data-discountcalc="<?php echo $key->discountcalctypeid;?>"
							data-discountdisplayid="<?php echo $key->discountdisplayid;?>"
							data-roundoffremove="<?php echo $key->roundoffremove;?>"
							data-allowedaccounttypeid="<?php echo $key->allowedaccounttypeid;?>"
							data-chargesedit="<?php echo $key->chargesedit;?>"
							data-autotax="<?php echo $key->autotax;?>"
							data-allowedaccounttypeid="<?php echo $key->allowedaccounttypeid;?>"
							data-allstocktypeid="<?php echo $key->allstocktypeid;?>"
							data-allpaymentstocktypeid="<?php echo $key->allpaymentstocktypeid;?>"
							data-allprinttemplateid="<?php echo $key->allprinttemplateid;?>"
							data-serialnumbermastername="<?php echo $key->serialnumbermastername;?>"
							data-defoldpurserialmaster="<?php echo $key->defoldpurserialmaster;?>"
							data-defsalesreturnserialmaster="<?php echo $key->defsalesreturnserialmaster;?>"
							data-metalarraydetails="<?php echo $key->metalarraydetails;?>"
							data-metalserialnumberdetails="<?php echo $key->metalserialnumberdetails;?>"
							data-metalprinttempids="<?php echo $key->metalprinttempids;?>"
							data-metaloldarraydetails="<?php echo $key->metaloldarraydetails;?>"
							data-metaloldserialnumberdetails="<?php echo $key->metaloldserialnumberdetails;?>"
							data-metalreturnarraydetails="<?php echo $key->metalreturnarraydetails;?>"
							data-metalreturnserialnumberdetails="<?php echo $key->metalreturnserialnumberdetails;?>"
							data-repairserialnumberdetails="<?php echo $key->repairserialnumberdetails;?>"
							data-repairmetalprintid="<?php echo $key->repairmetalprintid;?>"
							data-taxtypeid="<?php echo $key->taxtypeid;?>"
							data-salestransactiontypeid="<?php echo $key->salestransactiontypeid;?>"
							data-transactionmanageid="<?php echo $key->transactionmanageid;?>"
							data-roundoffamount="<?php echo $key->roundoffamount;?>"
							data-adjustmentconcept="<?php echo $key->adjustmentconcept;?>"
							data-stoneadjustment="<?php echo $key->stoneadjustment;?>"
							data-oldpurchasetouch="<?php echo $key->oldpurchasetouch;?>"
							data-advanceserialno="<?php echo $key->advanceserialno;?>"
							data-advanceprintid="<?php echo $key->advanceprintid;?>"
							data-receiptserialno="<?php echo $key->receiptserialno;?>"
							data-receiptprintid="<?php echo $key->receiptprintid;?>"
							data-issueserialno="<?php echo $key->issueserialno;?>"
							data-issueprintid="<?php echo $key->issueprintid;?>"
							data-balanceserialno="<?php echo $key->balanceserialno;?>"
							data-balanceprintid="<?php echo $key->balanceprintid;?>"
							data-wtpaymenttouch="<?php echo $key->wtpaymenttouch;?>"
							data-label="<?php echo $key->transactionmanagename;?>">
							<?php echo $key->transactionmanagename;?></option>
							<?php endforeach;?>
						</select>
					</div>
					
					<div class="large-12 columns sectionalertbuttonarea" style="float: right;margin-right: 6px;">
							<input type="button" class="alertbtnno  close" id="closettypesettingsoverlay" name="" value="Close" tabindex="1013">
							<span class="lasttab" tabindex="1014"></span>
					</div>
					</div>
				</form>
				</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>				
			</div>
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
	</div>
</div>
<?php 
							if($device=='phone') {
								?>
	<div class="overlay" id="accountselectoverlay" style="overflow:unset;z-index:45;background: #f2f3fa !important;">			
		<div class="large-11 medium-11 small-12 large-centered medium-centered small-centered columns" style="padding: 0px !important;">
					<span id="accountformconditionvalidation" class="validationEngineContainer">
						<div class="large-12 columns paddingzero" style="background-color:#f2f3fa !important;">
						    <div class="large-12 columns sectionheaderformcaptionstyle" style="background-color:#f2f3fa !important;">Account Search</div>
							<div class="static-field large-3 columns acccondclear">
								<label>Field Name <span class="mandatoryfildclass">*</span></label>
								<select data-placeholder="Select" data-prompt-position="topLeft:14,36" class="validate[required] chzn-select chosenwidth viewdropdownchange" tabindex="10" name="accountcondcolumn" id="accountcondcolumn">
									<option></option>
								</select>  
								<input type="hidden" name="acccondcolumnid" id="acccondcolumnid" value="" />
							</div>
							<div class="static-field large-3 columns acccondclear">
								<label>Condition <span class="mandatoryfildclass">*</span></label>
								<select data-placeholder="Select" data-prompt-position="topLeft:14,36" class="validate[required] chzn-select chosenwidth " tabindex="11" name="acccondition" id="acccondition">
									<option></option>
									</select> 
							</div>
							<div class="input-field large-3 columns acccondclear" id="acccondvaluedivhid">
								<input type="text" class="validate[required,maxSize[100]]" id="acccondvalue" name="acccondvalue"  tabindex="12" >
								<label for="acccondvalue">Value <span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="static-field large-3 columns acccondclear" id="accddcondvaluedivhid">
								<label>Value<span class="mandatoryfildclass">*</span></label>
								<select data-placeholder="Select" data-prompt-position="topLeft:14,36" class="validate[required] chzn-select chosenwidth valdropdownchange" tabindex="13" name="accddcondvalue" id="accddcondvalue">
									<option></option>
								</select>  
							</div>
							<div class="input-field large-3 columns acccondclear" id="accdatecondvaluedivhid">
								<input type="text" class="validate[required]" data-dateformater="" id="accdatecondvalue" name="accdatecondvalue" tabindex="12" >
								<label for="accdatecondvalue">Value <span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="large-3 columns centertext">
								<input type="button" style="" class="alertbtnyes" id="accaddcondsubbtn" Value="Submit" name="accaddcondsubbtn"  tabindex="14" style="margin-top:15px;">
							</div>
							<div class="large-6 columns acccondclear">
								<input type="hidden" class="" id="finalacccondvalue" name="finalacccondvalue"  tabindex="12" >
								<input type="hidden" class="" id="finalacccondvaluename" name="finalacccondvaluename"  tabindex="12" >
							</div>
							<div class="large-6 columns acccondclear">
								<input id="accconditionname" value="" name="accconditionname" type="hidden">
								<input id="accfiltervalue" value="" name="accfiltervalue" type="hidden">
								<input id="accfilterid" value="" name="accfilterid" type="hidden">
							</div>
							<div class="large-12 columns">
								<div id="accfilterconddisplay" class="large-12 medium-12 small-12 columns uploadcontainerstyle" style="height:5rem; border:1px solid #CCCCCC;overflow-y:auto;overflow-x:hidden;margin-top:5px;"></div>
							</div>
							<div class="large-12 columns">&nbsp;</div>
							<?php
							echo '<div class="large-12 columns forgetinggridname" id="accountsearchgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="accountsearchgrid" style="max-width:2000px; height:240px;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
								<div class="inner-gridfooter footer-content footercontainer" id="accountsearchgridfooter">
									Footer & Pagination content 
								</div></div>';
							?>
						<div class="large-12 columns" style="background:#f2f3fa;left: -10% !important;position: relative;" >
							<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
								<span class="firsttab" tabindex="1000"></span>
								<input type="button" id="accountsearchsubmit" name="" value="Submit" class="alertbtnyes" tabindex="1001">	
								<input type="button" id="accoutsearchclose" name="" value="Cancel" tabindex="1002" class="alertbtnno  alertsoverlaybtn" >
								<span class="lasttab" tabindex="1003"></span>
							</div>
						</div>
						</div>

					</span>
			
		</div>
	</div>
							<?php } else { ?>
							
							<div class="overlay" id="accountselectoverlay" style="overflow:unset;z-index:45;top:100px!important;">			
		<div class="large-10 medium-10 small-12 large-centered medium-centered small-centered columns">
					<span id="accountformconditionvalidation" class="validationEngineContainer">
						<div class="large-12 columns paddingzero borderstyle" style="background-color:#fff">
						    <div class="large-12 columns sectionheaderformcaptionstyle">Account Search</div>
							<div class="static-field large-3 columns acccondclear">
								<label>Field Name <span class="mandatoryfildclass">*</span></label>
								<select data-placeholder="Select" data-prompt-position="topLeft:14,36" class="validate[required] chzn-select chosenwidth viewdropdownchange" tabindex="10" name="accountcondcolumn" id="accountcondcolumn">
									<option></option>
								</select>  
								<input type="hidden" name="acccondcolumnid" id="acccondcolumnid" value="" />
							</div>
							<div class="static-field large-3 columns acccondclear">
								<label>Condition <span class="mandatoryfildclass">*</span></label>
								<select data-placeholder="Select" data-prompt-position="topLeft:14,36" class="validate[required] chzn-select chosenwidth " tabindex="11" name="acccondition" id="acccondition">
									<option></option>
									</select> 
							</div>
							<div class="input-field large-3 columns acccondclear" id="acccondvaluedivhid">
								<input type="text" class="validate[required,maxSize[100]]" id="acccondvalue" name="acccondvalue"  tabindex="12" >
								<label for="acccondvalue">Value <span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="static-field large-3 columns acccondclear" id="accddcondvaluedivhid">
								<label>Value<span class="mandatoryfildclass">*</span></label>
								<select data-placeholder="Select" data-prompt-position="topLeft:14,36" class="validate[required] chzn-select chosenwidth valdropdownchange" tabindex="13" name="accddcondvalue" id="accddcondvalue">
									<option></option>
								</select>  
							</div>
							<div class="input-field large-3 columns acccondclear" id="accdatecondvaluedivhid">
								<input type="text" class="validate[required]" data-dateformater="" id="accdatecondvalue" name="accdatecondvalue" tabindex="12" >
								<label for="accdatecondvalue">Value <span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="large-3 columns centertext">
								<input type="button" style="" class="alertbtnyes" id="accaddcondsubbtn" Value="Submit" name="accaddcondsubbtn"  tabindex="14" style="margin-top:15px;">
							</div>
							<div class="large-6 columns acccondclear">
								<input type="hidden" class="" id="finalacccondvalue" name="finalacccondvalue"  tabindex="12" >
								<input type="hidden" class="" id="finalacccondvaluename" name="finalacccondvaluename"  tabindex="12" >
							</div>
							<div class="large-6 columns acccondclear">
								<input id="accconditionname" value="" name="accconditionname" type="hidden">
								<input id="accfiltervalue" value="" name="accfiltervalue" type="hidden">
								<input id="accfilterid" value="" name="accfilterid" type="hidden">
							</div>
							<div class="large-12 columns">
								<div id="accfilterconddisplay" class="large-12 medium-12 small-12 columns uploadcontainerstyle" style="height:5rem; border:1px solid #CCCCCC;overflow-y:auto;overflow-x:hidden;margin-top:5px;"></div>
							</div>
							<div class="large-12 columns">&nbsp;</div>
							<?php
							echo '<div class="large-12 columns forgetinggridname" id="accountsearchgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="accountsearchgrid" style="max-width:2000px; height:240px;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
								<div class="inner-gridfooter footer-content footercontainer" id="accountsearchgridfooter">
									Footer & Pagination content 
								</div></div>';
							?>
						<div class="large-12 columns" style="background:#fff">
							<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
								<span class="firsttab" tabindex="1000"></span>
								<input type="button" id="accountsearchsubmit" name="" value="Submit" class="alertbtnyes" tabindex="1001">	
								<input type="button" id="accoutsearchclose" name="" value="Cancel" tabindex="1002" class="alertbtnno  alertsoverlaybtn" >
								<span class="lasttab" tabindex="1003"></span>
							</div>
						</div>
						</div>

					</span>
			
		</div>
	</div>
							<?php } ?>
<!--account create overlay-->
<?php 
							if($device=='phone') {
								?>
	<div class="overlay" id="accountcreate" style="overflow:unset;z-index:45;height: 100% !important;">		
		<div class="large-12 medium-12 small-12 large-centered medium-centered small-centered columns" style="padding: 0px !important;height: 100% !important;">
		<span id="accountupdateform" class="validationEngineContainer" style="height: 100% !important;">
			<div class="large-12 columns paddingzero" style="background-color:transparent;height: 100% !important;">
				<div class="large-4 small-12 columns end large-centered" id="addaccountsection" style="padding: 0px !important;height: 100% !important;">
				<form id="accountcreateform" name="accountcreateform" class="validationEngineContainer accountcreateform" style="height: 100% !important;">
				
					<div class="large-12 small-12 columns" style="background-color:#fff;height: 100% !important;">
					<div class="large-12 small-12  columns sectionheaderformcaptionstyle">Add Account</div>
					<span class="firsttab" tabindex="1000"></span>
					<div class="static-field large-6 small-6 columns">
							<label>Account Type<span class="mandatoryfildclass">*</span></label>						
							<select id="accounttypeid" name="accounttypeid" class="validate[required] chzn-select" tabindex="1001">
								<?php foreach($accounttype as $key):?>
								<option value="<?php echo $key->accounttypeid;?>" data-accounttype="<?php echo $key->accounttypeid;?>" data-label="<?php echo $key->accounttypename;?>">
								<?php echo $key->accounttypename;?></option>
								<?php endforeach;?>		
							</select>
					</div>
					<div class="static-field large-6 small-6 columns">
							<label>Salutation<span class="mandatoryfildclass">*</span></label>						
							<select id="salutationid" name="salutationid" class="validate[required] chzn-select" tabindex="1001">
								<?php foreach($salutation as $key):?>
								<option value="<?php echo $key->salutationid;?>">
								<?php echo $key->salutationname;?></option>
								<?php endforeach;?>		
							</select>
					</div>
					<div class="input-field large-6 small-12 columns">
						<input id="newaccountname" class="validate[required]" type="text" data-validatefield="accountname" data-table="account" tabindex="1003" value="" name="newaccountname" data-primaryid="" maxlength="100">
						<label for="newaccountname">Account Name<span class="mandatoryfildclass">*</span></label>
					</div>
					<div class="input-field large-6 small-12 columns">
						<input id="newaccountshortname" class="validate[funcCall[accountshortnamecheck]]" type="text" data-validatefield="newaccountshortname" data-table="account" tabindex="1003" value="" name="newaccountshortname" data-primaryid="" maxlength="50">
						<label for="newaccountshortname">Short Name</label>
					</div>
					<?php if($mainlicense['accountmobilevalidation'] == 'YES') { ?>
						<div class="input-field large-6 small-6 columns">
							<input type="text" class="validate[required,custom[phone],funcCall[mobilenocheck]]" id="accountmobilenumber" name="accountmobilenumber" value="" tabindex="1012">
							<label for="accountmobilenumber">Mobile Number<span class="mandatoryfildclass">*</span></label>
						</div>
						
					<?php } else { ?>
						<div class="input-field large-6 small-6 columns">
							<input type="text" class="validate[custom[phone],funcCall[mobilenocheck]]" id="accountmobilenumber" name="accountmobilenumber" value="" tabindex="1012" data-validation-engine="">
							<label for="accountmobilenumber">Mobile Number</label>
						</div>
					<?php } ?>
					<div class="input-field large-6 small-6 columns">
						<input type="text" class="validate[custom[phone]]" id="accountaddtmobilenumber" name="accountaddtmobilenumber" value="" tabindex="1012" data-validation-engine="">
						<label for="accountaddtmobilenumber">Addt. Mobile No</label>
					</div>
					<input id="newaccountid" class="hidedisplay" type="text" value="" name="newaccountid">
					<div class="input-field large-6 small-6 columns">
						<input type="text" class="" id="accountpannumber" name="accountpannumber" value="" tabindex="1007" data-validation-engine="" maxlength="30">
						<label for="accountpannumber">PAN Number</label>
					</div>
					<div class="input-field large-12  small-6 columns">
						<input type="text" class="" id="accountprimaryaddress" name="accountprimaryaddress" value="" tabindex="1004" data-validation-engine="" maxlength="100">
						<label for="accountprimaryaddress">Address</label>
					</div>
					<div class="input-field large-6 small-6 columns">
						<input type="text" class="" id="primaryarea" name="primaryarea" value="" tabindex="1005" data-validation-engine="" maxlength="30">
						<label for="primaryarea">Area</label>
					</div>
					<div class="input-field large-6 small-6 columns">
						<input type="text" class="validate[custom[number]]" id="accountprimarypincode" name="accountprimarypincode" value="" tabindex="1007" data-validation-engine="" maxlength="30">
						<label for="accountprimarypincode">Pincode</label>
					</div>
					<div class="input-field large-6 small-6 columns">
						<input type="text" class="" id="accountprimarycity" name="accountprimarycity" value="" tabindex="1008" data-validation-engine="" maxlength="50">
						<label for="accountprimarycity">City</label>
					</div>
					<div class="static-field large-6 small-6 columns">
						<label>State</label>						
						<select id="accountprimarystate" name="accountprimarystate" class="chzn-select" tabindex="1010">
							<?php foreach($state as $key):?>
							<option value="<?php echo $key->stateid;?>" data-stateid="<?php echo $key->stateid;?>" data-countryid="<?php echo $key->countryid;?>" data-label="<?php echo $key->statename;?>">
							<?php echo $key->statename;?></option>
							<?php endforeach;?>		
						</select>
					</div>
					<div class="static-field large-6 small-6 columns hidedisplay">
						<label>Country</label>						
						<select id="accountprimarycountry" name="accountprimarycountry" class="chzn-select" tabindex="1009">
							<?php foreach($country as $key):?>
							<option value="<?php echo $key->countryid;?>" data-countryid="<?php echo $key->countryid;?>" data-label="<?php echo $key->countryname;?>">
							<?php echo $key->countryname;?></option>
							<?php endforeach;?>		
						</select>
					</div>
					<div class="input-field large-12 small-12 columns">
						<input type="text" class="" id="gstnnumber" name="gstnnumber" value="" tabindex="1011" data-validation-engine="" maxlength="20">
						<label for="gstnnumber">GST Number</label>
					</div>
					<div class="input-field large-6 small-6 columns hidedisplay">
						<input type="text" class="validate[custom[number]]" id="accountlandlinenumber" name="accountlandlinenumber" value="" tabindex="1005" data-validation-engine="">
						<label for="accountlandlinenumber">Landline Number</label>
					</div>
					<div class="static-field large-6 small-6 columns hidedisplay">
						<label>Account Group</label>						
						<select id="accountgroupid" name="accountgroupid" class="chzn-select" tabindex="1011">
							<?php foreach($accountgroup as $key):?>
							<option value="<?php echo $key->accountgroupid;?>" data-accountgroupid="<?php echo $key->accountgroupid;?>" data-label="<?php echo $key->accountgroupname;?>">
							<?php echo $key->accountgroupname;?></option>
							<?php endforeach;?>
						</select>
					</div>		
					<div class="large-12 small-12 columns sectionalertbuttonarea" style="text-align: center;top: 10px;">
						<input type="button" class="alertbtnyes formtogridtagcharge save" id="accountsavebtn" name="accountsavebtn" value="Submit" tabindex="1011">
						<input type="button" class="alertbtnyes formtogridtagcharge save hidedisplay" id="accountupdatebtn" name="accountupdatebtn" value="Update" tabindex="1012">
						<input type="button" class="alertbtnno  formtogridtagcharge close" id="closeaccount" name="" value="Cancel" tabindex="1013">
						<span class="lasttab" tabindex="1014"></span>
					</div>
					</div>
					
					</form>
				</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>				
			</div>
		</span>
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
	</div>
							<?php }
							else { ?>
							<div class="overlay" id="accountcreate" style="overflow:unset;z-index:45;">		
		<div class="large-10 medium-10 small-12 large-centered medium-centered small-centered columns">
		<span id="accountupdateform" class="validationEngineContainer">
			<div class="large-12 columns paddingzero" style="background-color:transparent">
				<div class="large-4 small-12 columns paddingbtm end large-centered" id="addaccountsection">
				<form id="accountcreateform" name="accountcreateform" class="validationEngineContainer accountcreateform">
				
					<div class="large-12 small-12 columns paddingzero borderstyle" style="background-color:#fff;top:100px !important;">
					<div class="large-12 small-12  columns sectionheaderformcaptionstyle">Add Account</div>
					<span class="firsttab" tabindex="1000"></span>
					<div class="static-field large-6 small-6 columns">
							<label>Account Type<span class="mandatoryfildclass">*</span></label>						
							<select id="accounttypeid" name="accounttypeid" class="validate[required] chzn-select" tabindex="1001">
								<?php foreach($accounttype as $key):?>
								<option value="<?php echo $key->accounttypeid;?>" data-accounttype="<?php echo $key->accounttypeid;?>" data-label="<?php echo $key->accounttypename;?>">
								<?php echo $key->accounttypename;?></option>
								<?php endforeach;?>		
							</select>
					</div>
					<div class="static-field large-6 small-6 columns">
							<label>Salutation<span class="mandatoryfildclass">*</span></label>						
							<select id="salutationid" name="salutationid" class="validate[required] chzn-select" tabindex="1001">
								<?php foreach($salutation as $key):?>
								<option value="<?php echo $key->salutationid;?>">
								<?php echo $key->salutationname;?></option>
								<?php endforeach;?>		
							</select>
					</div>
					<div class="input-field large-6 small-12 columns">
						<input id="newaccountname" class="validate[required]" type="text" data-validatefield="accountname" data-table="account" tabindex="1003" value="" name="newaccountname" data-primaryid="" maxlength="100">
						<label for="newaccountname">Account Name<span class="mandatoryfildclass">*</span></label>
					</div>
					<div class="input-field large-6 small-12 columns">
						<input id="newaccountshortname" class="validate[funcCall[accountshortnamecheck]]" type="text" data-validatefield="newaccountshortname" data-table="account" tabindex="1003" value="" name="newaccountshortname" data-primaryid="" maxlength="50">
						<label for="newaccountshortname">Short Name</label>
					</div>
					<?php if($mainlicense['accountmobilevalidation'] == 'YES') { ?>
						<div class="input-field large-6 small-6 columns">
							<input type="text" class="validate[required,custom[phone],funcCall[mobilenocheck]]" id="accountmobilenumber" name="accountmobilenumber" value="" tabindex="1012">
							<label for="accountmobilenumber">Mobile Number<span class="mandatoryfildclass">*</span></label>
						</div>
					<?php } else { ?>
						<div class="input-field large-6 small-6 columns">
							<input type="text" class="validate[custom[phone],funcCall[mobilenocheck]]" id="accountmobilenumber" name="accountmobilenumber" value="" tabindex="1012" data-validation-engine="">
							<label for="accountmobilenumber">Mobile Number</label>
						</div>
					<?php } ?>
					<div class="input-field large-6 small-6 columns">
						<input type="text" class="validate[custom[phone]]" id="accountaddtmobilenumber" name="accountaddtmobilenumber" value="" tabindex="1012" data-validation-engine="">
						<label for="accountaddtmobilenumber">Addt. Mobile No</label>
					</div>
					<input id="newaccountid" class="hidedisplay" type="text" value="" name="newaccountid">
					<div class="input-field large-6 small-6 columns">
						<input type="text" class="" id="accountpannumber" name="accountpannumber" value="" tabindex="1007" data-validation-engine="" maxlength="30">
						<label for="accountpannumber">PAN Number</label>
					</div>
					<div class="input-field large-12  small-6 columns">
						<input type="text" class="" id="accountprimaryaddress" name="accountprimaryaddress" value="" tabindex="1004" data-validation-engine="" maxlength="100">
						<label for="accountprimaryaddress">Address</label>
					</div>
					<div class="input-field large-6 small-6 columns">
						<input type="text" class="" id="primaryarea" name="primaryarea" value="" tabindex="1005" data-validation-engine="" maxlength="30">
						<label for="primaryarea">Area</label>
					</div>
					<div class="input-field large-6 small-6 columns">
						<input type="text" class="validate[custom[number]]" id="accountprimarypincode" name="accountprimarypincode" value="" tabindex="1007" data-validation-engine="" maxlength="30">
						<label for="accountprimarypincode">Pincode</label>
					</div>
					<div class="input-field large-6 small-6 columns">
						<input type="text" class="" id="accountprimarycity" name="accountprimarycity" value="" tabindex="1008" data-validation-engine="" maxlength="50">
						<label for="accountprimarycity">City</label>
					</div>
					<div class="static-field large-6 small-6 columns">
						<label>State</label>						
						<select id="accountprimarystate" name="accountprimarystate" class="chzn-select" tabindex="1010">
							<?php foreach($state as $key):?>
							<option value="<?php echo $key->stateid;?>" data-stateid="<?php echo $key->stateid;?>" data-countryid="<?php echo $key->countryid;?>" data-label="<?php echo $key->statename;?>">
							<?php echo $key->statename;?></option>
							<?php endforeach;?>		
						</select>
					</div>
					<div class="input-field large-12 small-12 columns">
						<input type="text" class="" id="gstnnumber" name="gstnnumber" value="" tabindex="1011" data-validation-engine="" maxlength="20">
						<label for="gstnnumber">GST Number</label>
					</div>
					<div class="static-field large-6 small-6 columns hidedisplay">
						<label>Country</label>						
						<select id="accountprimarycountry" name="accountprimarycountry" class="chzn-select" tabindex="1009">
							<?php foreach($country as $key):?>
							<option value="<?php echo $key->countryid;?>" data-countryid="<?php echo $key->countryid;?>" data-label="<?php echo $key->countryname;?>">
							<?php echo $key->countryname;?></option>
							<?php endforeach;?>		
						</select>
					</div>
					<div class="input-field large-6 small-6 columns hidedisplay">
						<input type="text" class="validate[custom[number]]" id="accountlandlinenumber" name="accountlandlinenumber" value="" tabindex="1005" data-validation-engine="">
						<label for="accountlandlinenumber">Landline Number</label>
					</div>
					<div class="static-field large-6 small-6 columns hidedisplay">
						<label>Account Group</label>						
						<select id="accountgroupid" name="accountgroupid" class="chzn-select" tabindex="1011">
							<?php foreach($accountgroup as $key):?>
							<option value="<?php echo $key->accountgroupid;?>" data-accountgroupid="<?php echo $key->accountgroupid;?>" data-label="<?php echo $key->accountgroupname;?>">
							<?php echo $key->accountgroupname;?></option>
							<?php endforeach;?>
						</select>
					</div>		
					<div class="large-12 small-12 columns sectionalertbuttonarea" style="float: right;margin-right: 6px;">
						<input type="button" class="alertbtnyes formtogridtagcharge save" id="accountsavebtn" name="accountsavebtn" value="Submit" tabindex="1011">
						<input type="button" class="alertbtnyes formtogridtagcharge save hidedisplay" id="accountupdatebtn" name="accountupdatebtn" value="Update" tabindex="1012">
						<input type="button" class="alertbtnno  formtogridtagcharge close" id="closeaccount" name="" value="Cancel" tabindex="1013">
						<span class="lasttab" tabindex="1014"></span>
					</div>
					</div>
					
					</form>
				</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>				
			</div>
		</span>
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
	</div>
	<?php 
							} ?>
							
<!--chitbook no overlay-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="chitbooknocreate" style="overflow-y: scroll;overflow-x: hidden;z-index:45;">	
		<div class="large-10 medium-10 small-12 large-centered medium-centered small-centered columns">
			<div class="large-12 column paddingzero"  style="background:transparent">
				<div class="row sectiontoppadding">&nbsp;</div>
				<div class="large-6 columns paddingbtm end large-centered" id="chitbooknosection">
				<form id="chitbooknoform" name="chitbooknoform" class="validationEngineContainer chitbooknoform">
				<?php 
							if($device=='phone') {
								?>
					<div class="large-12 columns paddingzero border-modal-8px borderstyle" style="background-color:#f2f3fa">
					<div class="large-12 columns sectionheaderformcaptionstyle" style="background: #f2f3fa !important;">Scan Mutiple Chit Book No To Cancel</div>
							<?php } else { ?>
							<div class="large-12 columns paddingzero border-modal-8px borderstyle" style="background-color:#fff">
					<div class="large-12 columns sectionheaderformcaptionstyle" style="background: #fff !important;">Scan Mutiple Chit Book No To Cancel</div>
							
							<?php } ?>
					<span class="firsttab" tabindex="1000"></span>
					<div class="input-field large-12 columns">
						<input id="chitbooknooverlaytbox" class="validate[required]" type="text" data-validatefield="" data-table=""  value="" name="chitbooknooverlaytbox" data-primaryid="" maxlength="100">
						<label for="chitbooknooverlaytbox">Scan Mutiple Chit Book No<span class="mandatoryfildclass">*</span></label>
					</div>		
					<div class="large-12 columns sectionalertbuttonarea" style="float: right;margin-right: 6px;">
						<input type="button" class="alertbtnyes  formtogridtagcharge close" id="submitchitbooknooverlay" name="" value="Submit" tabindex="1013">
						<input type="button" class="alertbtnno  formtogridtagcharge close" id="closechitbooknooverlay" name="" value="Close" tabindex="1014">
						<span class="lasttab" tabindex="1014"></span>
					</div>
					</div>
				</form>
				</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>				
			</div>
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
	</div>
</div>

<!--  Weight check overlay - purchase-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="wtcheckcreate" style="overflow-y: scroll;overflow-x: hidden;z-index:45;">	
		<div class="large-10 medium-10 small-12 large-centered medium-centered small-centered columns">
			<div class="large-12 column paddingzero"  style="background:transparent">
				<div class="row sectiontoppadding">&nbsp;</div>
				<div class="large-4 columns paddingbtm end large-centered" id="billrefnosection">
				<form id="wtcheckform" name="wtcheckform" class="validationEngineContainer wtcheckform">
					<div class="large-12 columns paddingzero border-modal-8px borderstyle" style="background-color:#fff">
					<div class="large-12 columns sectionheaderformcaptionstyle">Purchase Weight Check</div>
					<span class="firsttab" tabindex="1000"></span>
					<div class="input-field large-12 columns">
						<input id="purchasecheckgrwt" class="" type="text" data-validatefield="" data-table=""  data-validation-engine="validate[required,custom[number],min[0.001],decval[5]]" value="0" name="purchasecheckgrwt" data-primaryid="" maxlength="100">
						<label for="purchasecheckgrwt">Gross Wt</label>
					</div>
					<div class="input-field large-12 columns hidedisplay">
						<input id="purchasecheckerrorwt" class="" type="text" data-validatefield=""  data-validation-engine="validate[required,custom[number],min[0],decval[5]]" data-table=""  value="<?php echo $mainlicense['purchaseerrorwt']; ?>" name="purchasecheckerrorwt" data-primaryid="" maxlength="100">
						<label for="purchasecheckerrorwt">Error Wt</label>
					</div>	
					<div class="large-12 columns sectionalertbuttonarea" style="float: right;margin-right: 6px;">
							<input type="button" class="alertbtnyes" id="wtchecksave" name="" value="Submit" tabindex="1013">
							<span class="lasttab" tabindex="1014"></span>
					</div>
					</div>
				</form>
				</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>				
			</div>
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
	</div>
</div>
<!--balance entry overlay-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="salesbalanceoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40;">		
		<div class="row sectiontoppadding">&nbsp;</div>
		<form id="salesbalanceform" name="salesbalanceform" class="validationEngineContainer salesbalanceform">		
		<div class="">
			<div class="alert-panel">
				<div class="alertmessagearea">
				<div class="alert-title">Balance/Advance</div>
				<input type="hidden" id="balanceamt" value="0"name="balanceamt">
				<input type="hidden" id="advanceamt" value="0"  name="advanceamt">
				<input type="hidden" id="balancepurewt" value="0"  name="balancepurewt">
				<input type="hidden" id="advancepurewt" value="0"  name="advancepurewt">
				<div class="alert-message">
					<span class="firsttab" tabindex="1000"></span>
					
					<div class="input-field overlayfield ">
						<input id="balanceduedate" class="fordatepicicon" tabindex="1002" type="text" data-dateformater="dd-mm-yy" data-prompt-position="bottomLeft" readonly="readonly" tabindex="" name="balanceduedate" data-validation-engine="">
						<label for="balanceduedate">Due Date<span class="mandatoryfildclass">*</span></label>
					</div>
				</div>
				</div>
				<div class="row">&nbsp;</div>
				<div class="alertbuttonarea">
					<input type="button" id="balaccountsave" name="" tabindex="1004" value="Submit" class="alertbtnyes" >
					<input type="button" id="balaccountclose" name="" value="Cancel" tabindex="1005" class="flloop alertbtnno alertsoverlaybtn" >
					<span class="lasttab" tabindex="1006"></span>
				</div>
			</div>
		</div>
		</form>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
	</div>
</div>
<!--discount overlay-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="salesdiscountoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="row sectiontoppadding">&nbsp;</div>
		<form id="salesdiscountform" name="salesdiscountform" class="validationEngineContainer salesdiscountform">		
		<div class="large-4 medium-10 small-12 large-centered medium-centered small-centered columns">
			<div class="alert-panel">
				<div class="alertmessagearea">
				<div class="alert-title">Discount</div>
				<div class="alert-message">
					<span class="firsttab" tabindex="1000"></span>
					<div class="input-field overlayfield">
						<input type="text" class="ffield" id="discamount" name="discount" prediscamount="0" value="0" tabindex="1001" data-validation-engine="validate[custom[number],required]">
						<label for="discamount" id="">Discount Amount</label>
					</div>
				</div>
				</div>
				<div class="alertbuttonarea">
					
					<input type="button" id="discountsave" name="" tabindex="1002" value="Submit" class="alertbtnyes" >
					<input type="button" id="discountclose" name="" value="Cancel" tabindex="1003" class="flloop alertbtnno alertsoverlaybtn" >
					<span class="lasttab" tabindex="1004"></span>
				</div>
			</div>
		</div>
		</form>
	</div>
</div>
<!----Stone Entry => CRUD overlay----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay overflowhidden" id="stoneoverlay" style="z-index:41">		
		<?php
			echo '<div class="large-12 columns headercaptionstyle headerradius  paddingzero">
						<div class="large-6 medium-6 small-5 columns headercaptionleft">
							<span class="gridcaptionpos"><span>Stone Entry</span></span>
						</div>
						<div class="large-6 medium-6 small-7 columns righttext addformheadericonstyle addformaction">
						<span class="icon-box" id="savestoneentrydata">
							<input id="" name="" tabindex="352" value="Save" class="alertbtnyes  ffield" type="button">
							<span class="actiontitle">Save</span>
						</span>
						<span class="icon-box closestoneentryform" id="closestoneentryform">
							<input id="" name="" tabindex="352" value="Close" class="alertbtnno  ffield" type="button">
							<span class="actiontitle">Close</span>
						</span>';
				echo 	'</div>';
				echo '</div>';
		?>
		<!-- tab group creation -->
		<div class="large-12 columns tabgroupstyle">
			<ul class="tabs" data-tab="">
				<li id="tab1" class="sidebariconstab tab-title active ftab asas" data-subform="1">
					<span>Stone Entry</span>
				</li>
			</ul>
		</div>
		<div class="large-12 columns addformunderheader">&nbsp;</div>
		<div class="large-12 columns scrollbarclass addformcontainer">
			<?php
				if($device=='phone') 
	{
		?>
		<form id="" name="" class="cleardataform" style="background:#f2f3fa !important;box-shadow:none;">		
		<div class="large-12 medium-12 small-12 large-centered medium-centered small-centered columns" style="padding: 0px !important;">
		
	<?php } else { ?>
		<form id="" name="" class="cleardataform" style="background:#f2f3fa !important;box-shadow:none;">	
		<div class="large-12 medium-12 small-12 large-centered medium-centered small-centered columns">
	<?php } ?>
		
			<div class="large-12 column paddingzero">
				<div class="row">&nbsp;</div>
				 <div id="stoneleftform" class="large-4 columns end paddingbtm hidedisplay">
					<div class="large-12 columns validationEngineContainer cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff" id="stoneentryaddvalidate">
						<div class="large-12 columns headerformcaptionstyle">Stone Entry</div>
					
						<?php //if($stonestatus == 0) { ?>
							<div class="static-field large-12 columns hidedisplay" >
									<label>Net Wt-Calc Type<span class=""></span></label>
									<select id="netwtcalctype" name="netwtcalctype" class="chzn-select" data-prompt-position="bottomLeft:14,36" disabled="true">
									<option value="">Select</option>
									<?php foreach($stonecalctype as $key):?>
										<option value="<?php echo $key->stonecalctypeid;?>">
											<?php echo $key->netwtcalctype;?></option>
									<?php endforeach;?>	
									</select>
							</div>
						<?php //} ?>
					<div class="tagadditionalchargeclear_stone">	
						<div class="static-field large-6 small-6 columns ">
							<label>Stone Name<span class="mandatoryfildclass">*</span></label>
							<select id="stoneid" name="stoneid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="176">
								<option value=""></option>
								<?php  $prev = ' ';
									foreach($stonename->result() as $key){
									$cur = $key->stonetypeid;
									$a="";
									if($prev != $cur )
									{
										if($prev != " ")
										{
											 echo '</optgroup>';
										}
										echo '<optgroup  label="'.$key->stonetypename.'" id="'.$key->stonetypename.'">';
										$prev = $key->stonetypeid;
									} 
									?>
								<option value="<?php echo $key->stoneid;?>" data-name="<?php echo $key->stonename;?>" data-stoneidgroupid="<?php echo $key->stonetypeid;?>" data-groupid="<?php echo $key->stonetypeid;?>" data-stoneidhidden="<?php echo $key->stonename;?>" data-stoneidgroupname="<?php echo $key->stonetypename;?>" data-groupname="<?php echo $key->stonetypename;?>"><?php echo $key->stonename;?></option>
								<?php } ?>
							</select>
						</div>
						<div class="static-field large-6 small-6 columns">
							<label>Stone Calculation Type<span class="mandatoryfildclass">*</span></label>								
							<select id="stoneentrycalctypeid" name="stoneentrycalctypeid" class="chzn-select " data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="102">
								<option value=""></option>
								<?php foreach($stoneentrycalctype as $key):?>
								<option value="<?php echo $key->stoneentrycalctypeid;?>" data-name="<?php echo $key->stoneentrycalctypename;?>" data-stonecalcidhidden="<?php echo $key->stoneentrycalctypename;?>">
								<?php echo $key->stoneentrycalctypename;?></option>
								<?php endforeach;?>	 
							</select>
						</div>
						<div class="input-field large-6 small-6 columns " id="purchaserate-div" >
							<input type="text" class="" id="purchaserate" name="purchaserate" value="" data-validation-engine="validate[required,custom[number],decval[<?php echo $rate_round;?>]]" tabindex="177">
							<label for="purchaserate">Purchase Rate<span class="mandatoryfildclass">*</span></label>
						</div>
						<div class="input-field large-6 small-6 columns " id="basicrate-div" >
							<input type="text" class="" id="basicrate" name="basicrate" value="" data-validation-engine="validate[required,custom[number],decval[<?php echo $rate_round;?>]]" tabindex="177">
							<label for="basicrate">Sales Rate<span class="mandatoryfildclass">*</span></label>
						</div>
						<div class="input-field large-6 small-6 columns " id="stonegramdiv">
							<input type="text" class="" id="stonegram" name="stonegram" value="" data-validation-engine="" tabindex="177">
							<label for="stonegram">Gram Weight</label>
						</div>
						<div class="input-field large-6 small-6 columns " id="stonepiecesdiv" >
							<input type="text" class="" id="stonepieces" name="stonepieces" value="" data-validation-engine="validate[required,custom[number]]" tabindex="178">
							<label for="stonepieces">Pieces<span class="mandatoryfildclass">*</span></label>
						</div>
						<div class="input-field large-6 small-6 columns" id="stonecaratdiv">
							<input type="text" class="" id="caratweight" name="caratweight" value="" data-validation-engine="validate[required,custom[number]]" tabindex="179">
							<label for="caratweight">Carat Weight<span class="mandatoryfildclass">*</span></label>
						</div>
						<div class="input-field large-6 small-6 columns hidedisplay">
							<input type="text" class="" id="totalcaratweight" name="totalcaratweight" value="" data-validation-engine="" tabindex="">
							<label for="totalcaratweight">Total Carat Weight<span class="mandatoryfildclass">*</span></label>
						</div>
						<div class="input-field large-6 small-6 columns hidedisplay">
							<input type="text" class="" id="totalweight" name="totalweight" value="" data-validation-engine="" tabindex="">
							<label for="totalweight">Total Net Weight<span class="mandatoryfildclass">*</span></label>
						</div>
						<div class="input-field large-6 small-6 columns ">
							<input type="text" class="" id="stonetotalamount" name="stonetotalamount" value="" data-validation-engine="validate[required,custom[number]]" tabindex="">
							<label for="stonetotalamount">Total Amount<span class="mandatoryfildclass">*</span></label>
						</div>
						<div class="large-12 small-12 columns" style="text-align: right">
							<label> </label>
							<input type = "hidden" id="tagstoneentryid" name="tagstoneentryid"/>
							<input type="button" class="btn leftformsbtn stoneaddon" id="tagaddstone" data-id="ADD" name="tagaddstone" value="Submit" tabindex="180">
							<input type="button" class="btn leftformsbtn stoneaddon" style="display:none" data-id="UPDATE" id="tageditstone" name="tageditstone" value="Submit" tabindex="181">
						</div>
					</div>
					</div>
				</div>
				
				
								<div id="stoneleftgrid" class="large-12 small-12 columns paddingbtm">
					<div class="large-12 small-12 columns paddingzero">
						<div class="large-12 small-12 columns viewgridcolorstyle paddingzero borderstyle">
							<div class="large-12 small-12 columns headerformcaptionstyle" style="padding: 0.2rem 0 0rem;">
						
								<span class="large-6 medium-6 small-12 columns text-left small-only-text-center" >Stone Entry</span>
								<div class="large-6 medium-6 small-12 columns" style="text-align:right;">
									<span id="">
										<span id="tagstoneentryedit" class="editiconclass hidedisplay" style="padding-right: 0rem;" title="Edit"><i class="material-icons">edit</i></span>
									</span>
									<span id="" style="">
										<span id="salestagstoneentrydelete" class="deleteiconclass hidedisplay" style="padding-right: 0rem;" title="Delete"><i class="material-icons">delete</i></span>
									</span>
								</div><br>
							</div>
							<?php
									echo '<div class="large-12 small-12  columns forgetinggridname" id="stoneentrygridwidth" style="padding-left:0;padding-right:0;height:300px;"><div class="desktop row-content inner-gridcontent" id="stoneentrygrid" style="max-width:2000px; height:300px;top:0px;">
										<!-- Table content[Header & Record Rows] for desktop-->
										</div>
										<!-- <div class="inner-gridfooter footer-content footercontainer" id="stoneentrygridfooter"></div> -->
											<!-- Footer & Pagination content -->
										
										</div>';
								?>
						</div>
					</div>
				</div>
				<?php 
							if($device=='phone') {
								?>
				<div class="large-12 small-12 columns" style="padding-top:1rem !important;background:#f2f3fa !important;">	
							<div class="large-12 small-12 columns viewgridcolorstyle" style="padding-left: 0 !important;padding-right: 0 !important;box-shadow: none;">	
							<div class="large-12 small-12 columns headerformcaptionstyle" style="padding: 0.2rem 0rem 0rem; height:auto; line-height:1.8rem;background: #f2f3fa !important;">	
							<div class="large-12 small-12  medium-12 small-12 columns small-only-text-center" style="">Stone Entry Summary</div>
							<?php } else { ?>
				<div class="large-12 small-12 columns paddingbtm" >	
							<div class="large-12 small-12 columns viewgridcolorstyle borderstyle" style="padding-left: 0 !important;padding-right: 0 !important;">	
							<div class="large-12 small-12 columns headerformcaptionstyle" style="padding: 0.2rem 0rem 0rem; height:auto; line-height:1.8rem;">	
							<div class="large-12 small-12  medium-12 small-12 columns small-only-text-center" style="">Stone Entry Summary</div>
										
							<?php } ?>
							<div class="large-6 medium-6 small-12 columns innergridicons small-only-text-center" style="text-align:right">						
							</div>
							</div>
							<?php 
							if($device=='phone') {
								?>
							<div class="large-12 small-12 columns" style="background:#f2f3fa;padding-left:0;padding-right:0;">
							<?php } else { ?>
							<div class="large-12 small-12 columns" style="background:#fff;padding-left:0;padding-right:0;">
							<?php }?>
							<div class="large-3 small-6 columns">
							<label>Dia Pieces<span class=""> : </span> <span id="totdiapieces"></span></label>	
							</div>
							<div class="large-3 small-6 columns ">
							<label>Cs Pieces<span class=""> : </span> <span id="totcspieces"></span></label>	
							</div>	
							<div class="large-3 small-6 columns ">
							<label>Pearl Pieces<span class=""> : </span> <span id="totpearlpieces"></span></label>	
							</div>	
							<div class="large-3 small-6 columns ">
							<label>Pieces<span class=""> : </span> <span id="totpieces"></span></label>	
							</div>	
							<div class="large-3 small-6 columns ">
							<label>Dia Carat Wt<span class=""> : </span><span id="totdiacaratwt"></span></label>									
							</div>	
							<div class="large-3 small-6 columns ">
							<label>Cs Carat Wt<span class=""> : </span><span id="totcscaratwt"></span></label>									
							</div>	
							<div class="large-3 small-6 columns ">
							<label>Pearl Carat Wt<span class=""> : </span><span id="totpearlcaratwt"></span></label>									
							</div>
							<div class="large-3 small-6 columns ">
							<label>Carat Wt<span class=""> : </span><span id="caratwt"></span></label>									
							</div>
							<div class="large-3 small-6 columns hidedisplay">
							<label>Dia Wt<span class=""> : </span><span id="totdiawt"></span></label>	
							</div>	
							<div class="large-3 small-6 columns hidedisplay">
							<label>CS Wt<span class=""> : </span><span id="totcswt"></span></label>	
							</div>
							<div class="large-3 small-6 columns hidedisplay">
							<label>Pearl Wt<span class=""> : </span><span id="totpearlwt"></span></label>	
							</div>
							<div class="large-3 small-6 columns hidedisplay">
							<label>Wt<span class=""> : </span><span id="totwt"></span></label>	
							</div>		
							<div class="large-3 small-6 columns ">
							<label>Dia Amt<span class=""> : </span><span id="totdiaamt"></span></label>	
							</div>	
							<div class="large-3 small-6 columns ">
							<label>CS Amt<span class=""> : </span><span id="totcsamt"></span></label>	
							</div>	
							<div class="large-3 small-6 columns ">
							<label>Pearl Amt<span class=""> : </span><span id="totpearlamt"></span></label>	
							</div>	
							<div class="large-3 small-6 columns ">
							<label>Total Amt<span class=""> : </span><span id="totamt"></span></label>	
							</div>	
						</div>				
					</div>
				</div>	
			</div>
		</div>
		</form>
		</div>
	</div>
</div>
<!-- Rate Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="salesrateoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">	
		<div class="large-3 medium-10 large-centered medium-centered columns" style=="top:20% !important;">
			<div class="alert-panel" style="background-color:#465a63">
					<div class="alertmessagearea">
						<div class="alert-title">Rate</div>
						<form id="itemrateoverlay" name="itemrateoverlay" class="">
							<div style="" id="purityratevalidation" class="validationEngineContainer">
								<span class="firsttab" tabindex="1000"></span>
								<div class="static-field overlayfield" id="" >
									<label>Purity</label>
									<select id="purityrate" name="purityrate" class="chzn-select ffieldd" data-prompt-position="bottomLeft:14,36" tabindex="1001">
										<option value=""></option>
										<?php $prev = ' ';
											foreach($purityid->result() as $key):
											$cur = $key->metalid;
											$a="";
											if($prev != $cur )
											{
												if($prev != " ")
												{
													 echo '</optgroup>';
												}
												echo '<optgroup  label="'.$key->metalname.'">';
												$prev = $key->metalid;
											}
										?>
										<option data-purity="<?php echo $key->purityid;?>" data-metal="<?php echo $key->metalid;?>" data-melting="<?php echo $key->melting;?>" value="<?php echo $key->purityid;?>" data-label="<?php echo $key->purityname;?>"><?php echo $key->purityname;?></option>
										<?php endforeach;?>
								  </select>	
								</div>
								<div class="input-field overlayfield purchaseorderduehide">
									<input id="itemrate" class="validate[required,custom[number],decval[<?php echo $amount_round;?>]]" type="text" data-prompt-position="bottomLeft" tabindex="1002" name="itemrate">
									<label for="itemrate">Rate</label>
								</div>
								<input type="hidden" id="" name=""/>
							</div>
						</form>
					</div>
					<div class="alertbuttonarea">
						<input type="button" id="itemrateaddsbtn" name="" value="Submit" class="alertbtnyes" tabindex="1003">
						<input type="button" id="salesrateclose" name="" value="Cancel" class="alertbtnno flloop alertsoverlaybtn" tabindex="1004" >
						<span class="lasttab" tabindex="1005"></span>
					</div>
				</div>
			
		</div>
	</div>
</div>
<!-- Total Sales Overlay -->
<?php 
if($device=='phone') {
								?>
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="totalsalesoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">	
		<div class="large-3 medium-10 large-centered medium-centered columns"  style="padding: 0px !important;height: 100% !important;">
			<div class="alert-panel" style="background-color:#465a63;height: 100% !important;margin: 0px !important;border-radius: 0px !important;border: 0px !important;">
					<div class="alertmessagearea">
						<div class="alert-title">Rfid Scanning</div>
						<form id="totalsalesoverlay" name="totalsalesoverlay" class="">
							
							<div style="" id="totalsalesvalidation" class="validationEngineContainer">
								<span class="firsttab" tabindex="1000"></span>
								<div class="static-field overlayfield" id="rfiddeviceno-div" >
								<label>Device No<span class="mandatoryfildclass">*</span></label>						
								<select id="rfiddeviceno" name="rfiddeviceno" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="1008">
								<?php 
									$values = explode(',',$deviceno);
									if(!empty($deviceno)) {
									foreach($values as $value) :?>
										<option value="<?php echo $value;?>">
										<?php echo $value;?></option>
									<?php endforeach; } ?>
								</select>
								</div>
									
								<div class="input-field overlayfield">
									<input id="totaltags" class="" type="text" data-prompt-position="bottomLeft" tabindex="1002" name="totaltags" readonly>
									<label for="totaltags">Total Tags</label>
								</div>
								<div class="input-field overlayfield">
									<input id="loadtags" class="" type="text" data-prompt-position="bottomLeft" tabindex="1002" name="loadtags" readonly>
									<label for="loadtags">Loaded Tags</label>
								</div>
								<input type="hidden" id="totaltagshidden">
								<input type="hidden" id="totaltagsnohidden">
								<input type="hidden" id="allrfideviceno" value="<?php echo $deviceno;?>">
							</div>
						</form>
					</div>
					<div class="alertbuttonarea" style="left: -10px !important;position: relative;" >
					<span style="position:relative;top:0px !important;left: 59px !important;">
						<input type="button" id="totalsalesaddsbtn" name="" value="Submit" class="alertbtnyes" tabindex="1003">
						<input type="button" id="totalsalesclose" name="" value="Cancel" class="alertbtnno flloop alertsoverlaybtn" tabindex="1004" >
				    </span>
						<span style="position:relative;top:40px !important;left:-115px !important;">
						<input type="button" id="totalsalescleartbl" name="" value="Clear" class="alertbtnno flloop alertsoverlaybtn" tabindex="1005" >
						<input type="button" id="totalsalesfetchtbl" name="" value="Fetch" class="alertbtnno flloop alertsoverlaybtn" tabindex="1006" >
						</span>
						<span class="lasttab" tabindex="1007"></span>
					</div>
				</div>
			
		</div>
	</div>
</div>
<?php } else { ?>
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="totalsalesoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">	
		<div class="row sectiontoppadding">&nbsp;</div>
		<div class="large-3 medium-10 large-centered medium-centered columns" style="position:relative;top:20% !important;">
			<div class="alert-panel" style="background-color:#465a63">
					<div class="alertmessagearea">
						<div class="alert-title">Rfid Scanning</div>
						<form id="totalsalesoverlay" name="totalsalesoverlay" class="">
							
							<div style="" id="totalsalesvalidation" class="validationEngineContainer">
								<span class="firsttab" tabindex="1000"></span>
								<div class="static-field overlayfield" id="rfiddeviceno-div" >
								<label>Device No<span class="mandatoryfildclass">*</span></label>						
								<select id="rfiddeviceno" name="rfiddeviceno" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="1008">
								
								<?php 
									$values = explode(',',$deviceno);
									if(!empty($deviceno)) {
									foreach($values as $value) :?>
										<option value="<?php echo $value;?>">
										<?php echo $value;?></option>
									<?php endforeach; } ?>
								</select>
							</div>
								<div class="input-field overlayfield">
									<input id="totaltags" class="" type="text" data-prompt-position="bottomLeft" tabindex="1002" name="totaltags" readonly>
									<label for="totaltags">Total Tags</label>
								</div>
								<div class="input-field overlayfield">
									<input id="loadtags" class="" type="text" data-prompt-position="bottomLeft" tabindex="1002" name="loadtags" readonly>
									<label for="loadtags">Loaded Tags</label>
								</div>
								<input type="hidden" id="totaltagshidden">
								<input type="hidden" id="totaltagsnohidden">
								<input type="hidden" id="allrfideviceno" value="<?php echo $deviceno;?>">
							</div>
						</form>
					</div>
					<div class="alertbuttonarea">
						<input type="button" id="totalsalesaddsbtn" name="" value="Submit" class="alertbtnyes" tabindex="1003">
						<input type="button" id="totalsalesclose" name="" value="Cancel" class="alertbtnno flloop alertsoverlaybtn" tabindex="1004" >
						<input type="button" id="totalsalescleartbl" name="" value="Clear" class="alertbtnno flloop alertsoverlaybtn" tabindex="1005" >
						<input type="button" id="totalsalesfetchtbl" name="" value="Fetch" class="alertbtnno flloop alertsoverlaybtn" tabindex="1006" >
						<span class="lasttab" tabindex="1007"></span>
					</div>
				</div>
			
		</div>
	</div>
</div>
<?php
}
?>
<!-- Total Sales Overlay -->
<?php 
							if($device=='phone') {
								?>
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="multitagscan" style="overflow-y: scroll;overflow-x: hidden;z-index:40">	
		<div class="large-3 medium-10 large-centered medium-centered columns" style="padding: 0px !important;height: 100% !important;">
			<div class="alert-panel" style="background-color:#465a63;margin: 0px !important;border-radius: 0px !important;border: 0px !important;height: 100% !important;">
					<div class="alertmessagearea" style="left: -8px;position: relative;">
						<div class="alert-title">Barcode Tag Scan</div>
						<form id="multitagscan" name="multitagscan" class="">
							<div style="" id="multitagscanvalidation" class="validationEngineContainer"  style="left: -12px !important; position: relative;">
								<span class="firsttab" tabindex="1000"></span>
								<div class="static-field  large-6 small-4 columns" id="multiscanmode-div" >
									<label>Scan Mode<span class="mandatoryfildclass">*</span></label>						
									<select id="multiscanmode" name="multiscanmode" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="1008">
									<option value="1">Tag No</option>
									<option value="2">Rfid No</option>		
									</select>
								</div>
								<input type="hidden" id="multiscantotaltagshidden">
								<input type="hidden" id="multiscantotaltagsnohidden">
								<div class="input-field overlayfield large-12 small-12 columns" id="" style="max-height:10em !important;">
									<textarea name="multitagscantagstextarea" class="materialize-textarea" tabindex="135" id="multitagscantagstextarea" data-validation-engine="" style="padding:0px !important;min-height: 10em!important;"></textarea>
								</div>
							</div>
						</form>
					</div>
					<div class="alertbuttonarea" style=" margin-top: 15em;left: -30px !important;position: relative;" >
						<input type="button" id="multitagaddsbtn" name="" value="Submit" class="alertbtnyes" tabindex="1003">
						<input type="button" id="multitagclose" name="" value="Cancel" class="alertbtnno flloop alertsoverlaybtn" tabindex="1004" >
						<input type="button" id="mainmultitagaddsbtn" name="" value="Main Save" class="alertbtnyes" tabindex="1005">
						<span class="lasttab" tabindex="1006"></span>
					</div>
				</div>
			
		</div>
	</div>
</div>
							<?php } else { ?>
							<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="multitagscan" style="overflow-y: scroll;overflow-x: hidden;z-index:40">	
		<div class="large-3 medium-10 large-centered medium-centered columns" style="top:20% !important;">
			<div class="alert-panel" style="background-color:#465a63;height: 25em">
					<div class="alertmessagearea">
						<div class="alert-title">Barcode Tag Scan</div>
						<form id="multitagscan" name="multitagscan" class="">
							<div style="" id="multitagscanvalidation" class="validationEngineContainer">
								<span class="firsttab" tabindex="1000"></span>
								<div class="static-field  large-6 small-4 columns" id="multiscanmode-div" >
									<label>Scan Mode<span class="mandatoryfildclass">*</span></label>						
									<select id="multiscanmode" name="multiscanmode" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="1008">
									<option value="1">Tag No</option>
									<option value="2">Rfid No</option>		
									</select>
								</div>
								<input type="hidden" id="multiscantotaltagshidden">
								<input type="hidden" id="multiscantotaltagsnohidden">
								<div class="input-field overlayfield large-12 small-12 columns" id="" style="max-height:10em !important;">
									<textarea name="multitagscantagstextarea" class="materialize-textarea" tabindex="135" id="multitagscantagstextarea" data-validation-engine="" style="padding:0px !important;min-height: 10em!important;"></textarea>
								</div>
							</div>
						</form>
					</div>
					<div class="alertbuttonarea" style=" margin-top: 15em;" >
						<input type="button" id="multitagaddsbtn" name="" value="Submit" class="alertbtnyes" tabindex="1003">
						<input type="button" id="multitagclose" name="" value="Cancel" class="alertbtnno flloop alertsoverlaybtn" tabindex="1004" >
						<input type="button" id="mainmultitagaddsbtn" name="" value="Main Save" class="alertbtnyes" tabindex="1005">
						<span class="lasttab" tabindex="1006"></span>
					</div>
				</div>
			
		</div>
	</div>
</div>

							<?php } ?>
							
							
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="multiscanconfirmoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:41">	
		<div class="row sectiontoppadding">&nbsp;</div>
		<div class="large-3 medium-10 large-centered medium-centered columns">
			<div class="alert-panel" style="background-color:#465a63">
					<div class="alertmessagearea">
						<div class="alert-title">Multi Scan Confirm</div>
						<form id="multiscanconfirmoverlay" name="multiscanconfirmoverlay" class="">
							<div style="" id="multiscanconfirmvalidation" class="validationEngineContainer">
								<span class="firsttab" tabindex="1000"></span>
								<div class="input-field overlayfield">
									<input id="multiscantotaltags" class="" type="text" data-prompt-position="bottomLeft" tabindex="1002" name="multiscantotaltags" readonly>
									<label for="multiscantotaltags">Total Tags</label>
								</div>
								<div class="input-field overlayfield">
									<input id="multiscanloadtags" class="" type="text" data-prompt-position="bottomLeft" tabindex="1002" name="multiscanloadtags" readonly>
									<label for="multiscanloadtags">Loaded Tags</label>
								</div>
							</div>
						</form>
					</div>
					<div class="alertbuttonarea">
						<input type="button" id="multiscanaddsbtn" name="" value="Submit" class="alertbtnyes" tabindex="1003">
						<input type="button" id="multiscanclose" name="" value="Cancel" class="alertbtnno flloop alertsoverlaybtn" tabindex="1004" >
						<span class="lasttab" tabindex="1005"></span>
					</div>
				</div>
			
		</div>
	</div>
</div>
<!--CDICON overlay-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="cdiconoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<form id="productaddonform" name="productaddonform" class="productaddonform validationEngineContainer">		
		<div class="large-10 medium-10 small-12 large-centered medium-centered small-centered columns chargedataform">
			<div class="large-12 column paddingzero"  style="background:transparent">
				<div class="row">&nbsp;</div>
				<div class="large-4 columns end paddingbtm" style="padding-top: 100px;">
					<div class="large-12 columns cleardataform validationEngineContainer borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;" id="additionalchargeaddvalidate">
					<div class="large-12 columns sectionheaderformcaptionstyle"> Charge Details</div>
						<div class="input-field large-6 small-6 columns chargedetailsdiv charge8 hidedisplay">
							<input type="text" originalvalue="" charge="additionalcharge4" origcalfinalvalue="0" category="HALLMARK" calfinalvalue="0" formula="hflat" discountvalue="" class="chargedata amtvalue charge" keyword="HM-FT" id="hflat" name="hflat" value="" tabindex="" maxlength="10">
							<label for="hflat" title="Hallmark Flat">Hallmark Flat<span class="mandatoryfildclass">*</span><span class="orginalvalclass" id="HM-FT" style="color:#d95c5c;"></span></label>
						</div>
						<div class="input-field large-6 small-6 columns hidedisplay hflatfinaldiv" style="display:none;">
							<input type="text" value="" id="hflatfinal" maxlength="10">
							<label for="hflatfinal" title="Hallmark Flat">Hallmark/flat Final</label>
						</div>
						<div class="input-field large-6 small-6 columns chargedetailsdiv charge9 hidedisplay">
							<input type="text" originalvalue="" charge="additionalcharge4" origcalfinalvalue="0" category="HALLMARK" calfinalvalue="0" formula="pieces*hpiece" discountvalue="" class="chargedata amtvalue charge"  keyword="HM-PI" id="hpiece" name="hpiece" value="" tabindex="" maxlength="10">
							<label for="hpiece" title="Hallmark Per Piece">Hallmark/Piece<span class="mandatoryfildclass">*</span><span class="orginalvalclass" id="HM-PI" style="color:#d95c5c;"></span></label>
						</div>
						<div class="input-field large-6 small-6 columns hidedisplay hpiecefinaldiv" style="display:none;">
							<input type="text" value="" id="hpiecefinal" maxlength="10">
							<label for="hpiecefinal" title="Hallmark Per Piece">H.M/piece Final</label>
						</div>
						<div class="input-field large-6 small-6 columns chargedetailsdiv charge11 hidedisplay">
							<input type="text" originalvalue="" charge="additionalcharge5" origcalfinalvalue="0" category="CERTIFICATION" calfinalvalue="0" formula="cflat" discountvalue="" class="chargedata amtvalue charge" keyword="CC-FT" id="cflat" name="cflat" value="" tabindex="" maxlength="10">
							<label for="cflat" title="Certification Flat">Cert Flat<span class="mandatoryfildclass">*</span><span class="orginalvalclass" id="CC-FT" style="color:#d95c5c;"></span></label>
						</div>
						<div class="input-field large-6 small-6 columns hidedisplay certfinaldiv" style="display:none;">
							<input type="text" value="" id="certfinal" maxlength="10">
							<label for="certfinal" title="Certification Flat">Cert/flat Final</label>
						</div>
						<div class="input-field large-12 small-12 columns">
							<input type="text" class="extracalc" keyword="totalchargeamount" id="totalchargeamount" name="totalchargeamount" value="" tabindex="" readonly>
							<label for="tpercent" title="Touch Percent Purewt">Charge Amount<span class="mandatoryfildclass">*</span><span class="orginalvalclass" id="totalchargeamount" style="color:#d95c5c;"></span></label>
						</div>
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
							<input type="button" class="alertbtnyes" id="additionalcharge" name="additionalcharge" value="Submit" tabindex="179">
							
						</div>
						</div>
					</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>				
			</div>
		</div>
		</form>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
	</div>
</div>
<! -- Rate icon overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="ratedefaulticonoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40;">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-10 medium-10 small-12 large-centered medium-centered small-centered columns">
			<div class="large-12 column paddingzero"  style="background:transparent">
				<div class="row">&nbsp;</div>
				<div class="large-4 columns end paddingbtm" style="padding-top: 100px;">
					<div class="large-12 columns cleardataform validationEngineContainer borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;" id="additionalchargeaddvalidate">
					<div class="large-12 columns sectionheaderformcaptionstyle"> Rate Details</div>
						<div class="input-field large-12 columns">
						<input type="text" class="ratedefaultcalc" keyword="" id="rateclone" name="rateclone" value="" tabindex="">
								<label for="rateclone" title="">Today's Rate</label>
						</div>
						<div class="input-field large-12 columns">
						<input type="text" class="ratedefaultcalc" keyword="" id="rateaddition" name="rateaddition" value="" tabindex="">
								<label for="rateaddition" title="">Rate Addition</label>
						</div>
						<div class="input-field large-12 columns">
						<input type="text" class="" keyword="" id="finalrateclone" name="finalrateclone" value="" tabindex="" readonly>
								<label for="finalrateclone" title="">Final Rate</label>
						</div>
							<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
							<input type="button" class="alertbtnno" id="ratedefaulticonclose" name="" value="Close" tabindex="">
						</div>
							</div>
							</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>				
			</div>
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
	</div>
</div>

<!--Wastage icon overlay-->
<div class="large-12 small-12 columns" style="position:absolute;">
	<div class="overlay" id="wastageiconoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="large-10  small-12 large-centered medium-centered small-centered columns" style="position:relative;top:20% !important;">
			<div class="large-12 small-12  column paddingzero modal-center"  style="background:transparent">
				<div class="row">&nbsp;</div>
				<div class="large-4 medium-6 small-12 columns end paddingbtm" style="padding-top: 100px;">
					<div class="large-12 small-12 columns cleardataform validationEngineContainer borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;" id="additionalchargeaddvalidate">
						<div class="large-12 small-12 columns sectionheaderformcaptionstyle"> Wastage Details</div>
						<div class="input-field large-6 small-12 columns">
							<input type="text" class="" keyword="" id="wastageclone" name="wastageclone" value="" tabindex="" maxlength="10">
								<label for="wastageclone" title="" id="wastageclonelabel">Wastage</label>
						</div>
						<?php if($mainlicense['chargeconversionfield'] == 1) { ?> <!-- To display charge conversion details in an overlay -->
							<div class="input-field large-6 small-12 columns wastagecloneconversion">
								<input type="text" class="" keyword="" id="wastagecloneconversion" name="wastagecloneconversion" value="" tabindex="" maxlength="10">
									<label for="wastagecloneconversion" title="" id="wastagecloneconversion">Amount Convert</label>
							</div>
						<?php } ?>
						<div class="large-12 small-12 columns sectionalertbuttonarea" style="text-align: right">
							<input type="button" class="alertbtnno" id="wastagechargeiconclose" name="" value="Close" tabindex="">
						</div>
					</div>
				</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>				
			</div>
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
	</div>
</div>
<!--Making Charge icon overlay-->
<div class="large-12 small-12 columns" style="position:absolute;">
	<div class="overlay" id="makingchargeiconoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="large-10 medium-10 small-12 large-centered medium-centered small-centered columns" style="position:relative;top:20% !important;">
			<div class="large-12 small-12 column paddingzero modal-center"  style="background:transparent">
				<div class="row">&nbsp;</div>
				<div class="large-4 medium-6 small-12 columns end paddingbtm"  style="padding-top: 100px;">
					<div class="large-12 small-12 columns cleardataform validationEngineContainer borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;" id="additionalchargeaddvalidate">
					<div class="large-12 columns sectionheaderformcaptionstyle"> Making Charge Details</div>
						<div class="input-field large-6 small-6 columns">
							<input type="text" class="" keyword="" id="makingchargeclone" name="makingchargeclone" value="" tabindex="" maxlength="10">
								<label for="makingchargeclone" title="" id="makingchargeclonelabel">Making Charge</label>
						</div>
						<?php if($mainlicense['chargeconversionfield'] == 1) { ?> <!-- To display charge conversion details in an overlay -->
							<div class="input-field large-6 small-12 columns makingcloneconversion">
								<input type="text" class="" keyword="" id="makingcloneconversion" name="makingcloneconversion" value="" tabindex="" maxlength="10">
									<label for="makingcloneconversion" title="" id="makingcloneconversion">Amount Convert</label>
							</div>
						<?php } ?>
						<div class="large-12 small-12 columns sectionalertbuttonarea" style="text-align: right">
							<input type="button" class="alertbtnno" id="makingchargeiconclose" name="" value="Close" tabindex="">
						</div>
					</div>
				</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>				
			</div>
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
	</div>
</div>
<!--lst Charge icon overlay-->
<div class="large-12 small-12 columns" style="position:absolute;">
	<div class="overlay" id="lstchargeiconoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-10 medium-10 small-12 large-centered medium-centered small-centered columns">
			<div class="large-12 small-12 column paddingzero modal-center"  style="background:transparent">
				<div class="row">&nbsp;</div>
				<div class="large-4 medium-6 small-12 columns end paddingbtm" style="padding-top: 100px;">
					<div class="large-12 small-12 columns cleardataform validationEngineContainer borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;" id="additionalchargeaddvalidate">
					<div class="large-12 columns sectionheaderformcaptionstyle"> LST Charge Details</div>
						<div class="input-field large-6 small-6 columns">
						<input type="text" class="" keyword="" id="lstchargeclone" name="lstchargeclone" value="" tabindex="" maxlength="10">
								<label for="lstchargeclone" title="" id="lstchargeclonelabel">LST Charge</label>
							</div>
							<div class="large-12 small-12 columns sectionalertbuttonarea" style="text-align: right">
							<input type="button" class="alertbtnno" id="lstchargeiconclose" name="" value="Close" tabindex="">
						</div>
							</div>
							</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>				
			</div>
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
	</div>
</div>
<!--Flat Charge icon overlay-->
<div class="large-12  small-12 columns" style="position:absolute;">
	<div class="overlay" id="flatchargeiconoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-10 medium-10 small-12 large-centered medium-centered small-centered columns">
			<div class="large-12  small-12  column paddingzero modal-center"  style="background:transparent">
				<div class="row">&nbsp;</div>
				<div class="large-4  medium-6 small-12 columns end paddingbtm" style="padding-top: 100px;">
					<div class="large-12  small-12  columns cleardataform validationEngineContainer borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;" id="additionalchargeaddvalidate">
						<div class="large-12  small-12  columns sectionheaderformcaptionstyle"> Flat Charge Details</div>
						<div class="input-field large-6  small-12  columns">
							<input type="text" class="" keyword="" id="flatchargeclone" name="flatchargeclone" value="" tabindex="" maxlength="10">
								<label for="flatchargeclone" id="flatchargeclonelabel" title="">Flat Charge</label>
						</div>
						<?php if($mainlicense['chargeconversionfield'] == 1) { ?> <!-- To display charge conversion details in an overlay -->
							<div class="input-field large-6 small-12 columns flatchargecloneconversion">
								<input type="text" class="" keyword="" id="flatchargecloneconversion" name="flatchargecloneconversion" value="" tabindex="" maxlength="10">
									<label for="flatchargecloneconversion" title="" id="flatchargecloneconversion">Amount Convert</label>
							</div>
						<?php } ?>
						<div class="large-12  small-12  columns sectionalertbuttonarea" style="text-align: right">
							<input type="button" class="alertbtnno" id="flatchargeiconclose" name="" value="Close" tabindex="">
						</div>
					</div>
				</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>				
			</div>
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
	</div>
</div>
<!--Repair Charge icon overlay-->
<div class="large-12  small-12 columns" style="position:absolute;">
	<div class="overlay" id="repairchargeiconoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-10 medium-10 small-12 large-centered medium-centered small-centered columns">
			<div class="large-12  small-12  column paddingzero modal-center"  style="background:transparent">
				<div class="row">&nbsp;</div>
				<div class="large-4  medium-6 small-12 columns end paddingbtm" style="padding-top: 100px;">
					<div class="large-12  small-12  columns cleardataform validationEngineContainer borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;" id="additionalchargeaddvalidate">
						<div class="large-12  small-12  columns sectionheaderformcaptionstyle"> Repair Charge Details</div>
						<div class="input-field large-6  small-12  columns">
							<input type="text" class="" keyword="" id="repairchargeclone" name="repairchargeclone" value="" tabindex="" maxlength="10">
							<label for="repairchargeclone" id="repairchargeclonelabel" title="">Repair Charge</label>
						</div>
						<?php if($mainlicense['chargeconversionfield'] == 1) { ?> <!-- To display charge conversion details in an overlay -->
							<!--<div class="input-field large-6 small-12 columns repairchargecloneconversion">
								<input type="text" class="" keyword="" id="repairchargecloneconversion" name="repairchargecloneconversion" value="" tabindex="" maxlength="10">
								<label for="repairchargecloneconversion" title="" id="repairchargecloneconversion">Amount Convert</label>
							</div>-->
						<?php } ?>
						<div class="large-12  small-12  columns sectionalertbuttonarea" style="text-align: right">
							<input type="button" class="alertbtnno" id="repairchargeiconclose" name="" value="Close" tabindex="">
						</div>
					</div>
				</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>				
			</div>
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
	</div>
</div>
<!--Net Amount - Round off Overlay-->
<div class="large-12 small-12 columns" style="position:absolute;">
	<div class="overlay" id="billamountcalculationoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="large-10 medium-10 small-12 large-centered medium-centered small-centered columns" style="position:relative;top:20% !important;">
			<div class="large-12 small-12 column paddingzero modal-center"  style="background:transparent">
				<div class="row">&nbsp;</div>
				<div class="large-4 medium-6 small-12 columns end paddingbtm"  style="padding-top: 100px;">
					<div class="large-12 small-12 columns cleardataform validationEngineContainer borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;" id="additionalchargeaddvalidate">
					<div class="large-12 columns sectionheaderformcaptionstyle"> Bill Amount Details</div>
						<div class="input-field large-6 small-6 columns">
							<input type="text" class="" keyword="" id="finalbillamountcalculationtrigger" name="finalbillamountcalculationtrigger" value="" tabindex="" maxlength="10">
								<label for="finalbillamountcalculationtrigger" title="" id="finalbillamountcalculationtrigger">Enter Bill Amount</label>
						</div>
						<div class="large-12 small-12 columns sectionalertbuttonarea" style="text-align: right">
							<input type="button" class="alertbtnno" id="billamountcalculationtriggersubmit" name="" value="Submit" tabindex="">
							<input type="button" class="alertbtnno" id="billamountcalculationtriggerclose" name="" value="Close" tabindex="">
						</div>
					</div>
				</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>				
			</div>
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
	</div>
</div>
<!--Old Purchase - Round off Overlay-->
<div class="large-12 small-12 columns" style="position:absolute;">
	<div class="overlay" id="oldjewelscalculationoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="large-10 medium-10 small-12 large-centered medium-centered small-centered columns" style="position:relative;top:20% !important;">
			<div class="large-12 small-12 column paddingzero modal-center"  style="background:transparent">
				<div class="row">&nbsp;</div>
				<div class="large-4 medium-6 small-12 columns end paddingbtm"  style="padding-top: 100px;">
					<div class="large-12 small-12 columns cleardataform validationEngineContainer borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;" id="additionalchargeaddvalidate">
					<div class="large-12 columns sectionheaderformcaptionstyle"> Old Jewels Amount Details</div>
						<div class="input-field large-6 small-6 columns">
							<input type="text" class="" keyword="" id="oldjewelscalculationchange" name="finaloldjewelscalculationchange" value="" tabindex="" maxlength="10">
								<label for="finaloldjewelscalculationchange" title="" id="finaloldjewelscalculationchange">Enter Old Amount</label>
						</div>
						<div class="large-12 small-12 columns sectionalertbuttonarea" style="text-align: right">
							<input type="button" class="alertbtnno" id="oldjewelscalculationchangesubmit" name="" value="Submit" tabindex="">
							<input type="button" class="alertbtnno" id="oldjewelscalculationchangeclose" name="" value="Close" tabindex="">
						</div>
					</div>
				</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>				
			</div>
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
	</div>
</div>
<!--sales comment box overlay-->
<div class="large-12 small-12 columns" style="position:absolute;">
	<div class="overlay" id="salescomementboxoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="large-10 medium-10 small-12 large-centered medium-centered small-centered columns" style="top:20% !important;">
			<div class="large-12 column paddingzero modal-center"  style="background:transparent">
				<div class="large-4 medium-6 small-12 columns end paddingbtm" style="padding-top: 100px;">
					<div class="large-12 small-12 columns cleardataform validationEngineContainer borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;" id="additionalchargeaddvalidate">
					<div class="large-12 small-12 columns sectionheaderformcaptionstyle">Comments</div>
						 <div class="input-field large-12 small-12 columns" id="" style="max-height:300px !important;">
										<textarea name="salescomment" class="materialize-textarea" tabindex="135" id="salescomment" data-validation-engine="" maxlength="200" style="padding:0px !important;min-height:8rem;"></textarea>
						  </div>
							<div class="large-12 small-12 columns sectionalertbuttonarea" style="text-align: right">
							<input type="button" class="alertbtnno" id="salescommenticonclose" name="" value="Close" tabindex="">
						</div>
							</div>
							</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>				
			</div>
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
	</div>
</div>
<!--payment comment box overlay-->
<div class="large-12 small-12 columns" style="position:absolute;">
	<div class="overlay" id="paymentcomementboxoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="large-10 medium-10 small-12 large-centered medium-centered small-centered columns"  style="top:20% !important;">
			<div class="large-12 column paddingzero modal-center"  style="background:transparent">
				<div class="large-4 medium-6 small-12 columns end paddingbtm" style="padding-top: 100px;">
					<div class="large-12 columns cleardataform validationEngineContainer borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;" id="additionalchargeaddvalidate">
					<div class="large-12 small-12 columns sectionheaderformcaptionstyle">Comments</div>
						 <div class="input-field large-12 columns" id="" style="max-height:300px !important;">
										<textarea name="paymentcomment" class="materialize-textarea" tabindex="135" id="paymentcomment" data-validation-engine="" maxlength="200" style="padding:0px !important;min-height:8rem;"></textarea>
						  </div>
							<div class="large-12 small-12 columns sectionalertbuttonarea" style="text-align: right">
							<input type="button" class="alertbtnno" id="paymentcommenticonclose" name="" value="Close" tabindex="">
						</div>
							</div>
							</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>				
			</div>
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
	</div>
</div>
<!--bill comment box overlay-->
<div class="large-12 small-12 columns" style="position:absolute;">
	<div class="overlay" id="billcomementboxoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="large-10 medium-10 small-12 large-centered medium-centered small-centered columns"  style="top:20% !important;">
			<div class="large-12 column paddingzero modal-center"  style="background:transparent">
				<div class="large-4 medium-6 small-12 columns end paddingbtm" style="padding-top: 100px;">
					<div class="large-12 columns cleardataform validationEngineContainer borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;" id="additionalchargeaddvalidate">
					<div class="large-12 small-12 columns sectionheaderformcaptionstyle">Comments</div>
						 <div class="input-field large-12 small-12 columns" id="" style="max-height:300px !important;">
										<textarea name="balancedescription" class="materialize-textarea" tabindex="135" id="balancedescription" data-validation-engine="" maxlength="200" style="padding:0px !important;min-height:8rem;"></textarea>
										<!--<label for="salescomment">Comments<span class="mandatoryfildclass" id="salescomment_req"></span></label>-->
						 </div>
							<div class="large-12 small-12 columns sectionalertbuttonarea" style="text-align: right">
							<input type="button" class="alertbtnno" id="billcommenticonclose" name="" value="Close" tabindex="">
						</div>
							</div>
							</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>				
			</div>
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
	</div>
</div>
<!---- Item Tax overlay----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="taxoverlay" style="overflow-y: auto;overflow-x: hidden;">		
		<div class="large-5 medium-6 small-12 large-centered medium-centered small-centered columns borderstyle" style="position:relative;top:20% !important;">
			<form method="POST" name="taxoverlayform" id="taxoverlayform" action="" enctype="" class="clearformtaxoverlay">
				<span id="taxoverlayvalidation" class="validationEngineContainer"> 
					<div class="row" style="background:#fff;height:100% !important;">
						<div class="large-12 columns sectionheaderformcaptionstyle">
							Tax Charges
						</div>
						<div class="large-12 columns" style="background:#fff !important;height:auto;padding: 0.1rem 0 0;">
									<span class="large-6 medium-6 small-10 columns end hidedisplay" style="line-height:1.8">
										<span class="large-4 medium-4 large-6 small-4 columns" style="line-height:1.5">
											Category<span class="mandatoryfildclass">*</span>
										</span>
										<span class="large-6 large-8 small-8 end columns">
											<select class="chzn-select" id="taxcategory" name="taxcategory"  data-prompt-position="topLeft:14,36">
												<option data-attr="" data-gsttaxmode="" value="">Select</option>
												<?php foreach($category as $key):?>
														<option data-attr="<?php echo $key->taxmasterid;?>" data-gsttaxmode="<?php echo $key->gsttaxmodeid;?>" value="<?php echo $key->taxmasterid;?>">
														<?php echo $key->taxmastername;?></option>
												<?php endforeach;?>	
											</select>										
										</span>								
									</span>
									<span class="large-6 medium-6 small-2 columns innergridicon righttext small-only-text-center hidedisplay" style="line-height:1.8">
										<span id="taxdeleteicon" class="deleteiconclass" title="Delete"><i class="material-icons">delete</i></span>
									</span>
						</div>
						<?php
							echo '<div class="large-12 columns forgetinggridname" id="taxgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="taxgrid" style="max-width:2000px; height:200px;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
							<!--<div class="inner-gridfooter footer-content footercontainer" id="taxgridfooter">
									 Footer & Pagination content 
								</div>--></div>';
						?>
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: center">
							Total : <input type="text" id="tax_data_total" name="tax_data_total" value="0.00" class="" readonly style="width:80px;">
							</div>
								<div class="large-12 columns sectionalertbuttonarea" style="text-align: center">
							<input type="button" id="taxgridsubmit" name="taxgridsubmit" value="Submit" class="alertbtnyes">
							<input type="button" id="taxclose" name="" value="Cancel" class="alertbtnno">
						</div>
					</div>
					<!-- hidden fields -->
					<input type="hidden" name="taxnetamount" id="taxnetamount" />
					<input type="hidden" name="taxcatid" id="taxcatid" />
				</span>
			</form>
		</div>
	</div>
</div>
<!----Bill Tax overlay----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="billtaxoverlay" style="overflow-y: auto;overflow-x: hidden;">		
		<div class="large-5 medium-6 small-12 large-centered medium-centered small-centered columns borderstyle" style="position:relative;top:20% !important;">
			<form method="POST" name="billtaxoverlayform" id="billtaxoverlayform" action="" enctype="" class="billclearformtaxoverlay">
				<span id="billtaxoverlayvalidation" class="validationEngineContainer"> 
					<div class="row" style="background:#fff;height:100% !important;">
						<div class="large-12 columns sectionheaderformcaptionstyle">
							Tax Charges
						</div>
						<?php
							echo '<div class="large-12 columns forgetinggridname" id="billtaxgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="billtaxgrid" style="max-width:2000px; height:200px;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
							<!--<div class="inner-gridfooter footer-content footercontainer" id="taxgridfooter">
									 Footer & Pagination content 
								</div>--></div>';
						?>
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: center">
							Total : <input type="text" id="billtax_data_total" name="billtax_data_total" value="0.00" class="" readonly style="width:80px;">
						</div>
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: center">
							<input type="button" id="billtaxclose" name="" value="Close" class="alertbtnno">
						</div>
					</div>
					<!-- hidden fields -->
					<input type="hidden" name="taxnetamount" id="taxnetamount" />
					<input type="hidden" name="taxcatid" id="taxcatid" />
				</span>
			</form>
		</div>
	</div>
</div>
<!-- Discount Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="discountoverlay" style="overflow-y: scroll;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="" >
			<form method="POST" name="discountform" style="" id="discountform" action="" enctype="" class="">
				<span id="individualdiscountvalidation" class="validationEngineContainer">
					 <div class="alert-panel" style="background-color:#465a63">
						<div class="alertmessagearea">
							<div class="alert-title">Discount</div>
							<div class="alert-message">
								<span class="firsttab" tabindex="1000"></span>
								<div class="static-field overlayfield">             
									<label>Discount Type<span class="mandatoryfildclass">*</span></label>
									<select data-placeholder="Select" data-prompt-position="topRight:35,35" class="validate[required] chzn-select dropdownchange ffieldd" tabindex="1001" name="discounttypeid" id="discounttypeid">
									<option value=""></option>
									<?php foreach($calculationtype as $key):?>
											<option value="<?php echo $key->calculationtypeid;?>">
											<?php echo $key->calculationtypename;?></option>
									<?php endforeach;?>	
									</select>                  
								</div>
								<div class="discountcommon" >
									<div class="input-field overlayfield"> 
										<input type="text" id="itemdiscountlimit" name="itemdiscountlimit" value="" class="validate[custom[number]]" tabindex="1002">
										<label for="itemdiscountlimit">Discount Limit<span class="mandatoryfildclass">*</span></label>
									</div>
								</div>
								<div class="discountcommon" >
									<div class="input-field overlayfield"> 
										<input type="text" id="discountpercent" name="discountpercent" value="" class="validate[custom[number],funcCall[validatepercentage]]" tabindex="1002">
										<label for="discountpercent">Value<span class="mandatoryfildclass">*</span></label>
									</div>
								</div>
								<div class="discountvoucher">
									<div class="input-field overlayfield"> 
										<input type="text" id="giftnumber" name="giftnumber" value="" class="" tabindex="1002">
										<label for="giftnumber">Gift Number<span class="mandatoryfildclass">*</span></label>
									</div>
									<div class="input-field overlayfield"> 
										<input type="text" id="giftremark" name="giftremark" value="" class="]" tabindex="1002">
										<label for="giftremark">Remark</label>
									</div>
									<div class="input-field overlayfield"> 
										<input type="text" id="giftdenomination" name="giftdenomination" value="" class="" tabindex="1002">
										<label for="giftdenomination">Denomination<span class="mandatoryfildclass">*</span></label>
									</div>
									<div class="input-field overlayfield"> 
										<input type="text" id="giftunit" name="giftunit" value="" class="" tabindex="1002">
										<label for="giftunit">Unit<span class="mandatoryfildclass">*</span></label>
									</div>
								</div>
								<div class="input-field overlayfield"> 
									<input type="text" id="singlediscounttotal" name="singlediscounttotal" value="" class="" tabindex="1003" readonly >
									<label for="singlediscounttotal">Total Discount</label>
								</div>
							</div>
						</div>
						<div class="alertbuttonarea">
							<input type="button" id="discountsubmit" name="" value="Submit" class="alertbtnyes" tabindex="1004">	
							<input type="button" id="olddiscountclose" name="" value="Cancel" tabindex="1005" class="alertbtnno flloop  alertsoverlaybtn" >
							<span class="lasttab" tabindex="1006"></span>
						</div>
					</div>
				</span>
			</form>
		</div>
	</div>
</div>
<!----Bill number overlay----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="billnumberoverlay" style="overflow-y: auto;overflow-x: hidden;">		
		<div class="large-10 medium-10 small-10 large-centered medium-centered small-centered columns borderstyle" style="top:20% !important;" >
			<form method="POST" name="oldjewelbillnumberoverlayform" style="" id="oldjewelbillnumberoverlayform" action="" enctype="" class="clearformbillnumberoverlay">
				<span id="billnumberoverlayvalidation" class="validationEngineContainer"> 
					<div class="row" style="background:#fff">
						<div class="large-12 columns sectionheaderformcaptionstyle">
							Product Details
						</div>
						<?php
							echo '<div class="large-12 columns forgetinggridname" id="billnumbergridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="billnumbergrid" style="max-width:2000px; height:300px;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
							<!--<div class="inner-gridfooter footer-content footercontainer" id="billnumbergridfooter">
									 Footer & Pagination content 
								</div>--></div>';
						?>
					</div>
					<div class="row" style="background:#fff;">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right; top:15px;">
							<input type="button" id="billnumbergridsubmit" name="billnumbergridsubmit" value="Submit" class="alertbtn">
							<input type="button" id="billnumberclose" name="billnumberclose" value="Cancel" class="alertbtnno">
						</div>
					</div>
					<div class="row" style="background:#fff">&nbsp;</div>
					<!-- hidden fields -->
					<input type="hidden" name="billbasedproductid" id="billbasedproductid" />
				</span>
			</form>
		</div>
	</div>
</div>
<!----Old jewel voucher number overlay----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="oldjewelvoucheroverlay" style="overflow-y: auto;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="large-10 medium-10 small-10 large-centered medium-centered small-centered columns" >
			<form method="POST" name="billnumberoverlayform" style="" id="billnumberoverlayform" action="" enctype="" class="clearformoldjewelvoucheroverlay">
				<span id="billnumberoverlayvalidation" class="validationEngineContainer"> 
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#fff">
						<div class="large-12 columns sectionheaderformcaptionstyle">
							Product Details
						</div>
						<?php
							echo '<div class="large-12 columns forgetinggridname" id="oldjewelvouchergridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="oldjewelvouchergrid" style="max-width:2000px; height:300px;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
							<!--<div class="inner-gridfooter footer-content footercontainer" id="oldjewelvouchergridfooter">
									 Footer & Pagination content 
								</div>--></div>';
						?>
					</div>
					<div class="row" style="background:#fff">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
							<input type="button" id="oldjewelvouchergridsubmit" name="oldjewelvouchergridsubmit" value="Submit" class="alertbtnyes">
							<input type="button" id="oldjewelvoucherclose" name="oldjewelvoucherclose" value="Cancel" class="alertbtnno">
						</div>
					</div>
					<div class="row">&nbsp;</div>
					<!-- hidden fields -->
					   <input type="hidden" name="oldjewelvoucherproductid" id="oldjewelvoucherproductid" />
					   <input type="hidden" name="oldjewelvoucherbillno" id="oldjewelvoucherbillno" />
				</span>
			</form>
		</div>
	</div>
</div>
<!----Approval Return overlay----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="approvalreturnoverlay" style="overflow-y: auto;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="large-9 medium-9 small-9 large-centered medium-centered small-centered columns borderstyle" style="position:relative;top:20% !important;">
			<form method="POST" name="approvalreturnoverlayform" style="" id="approvalreturnoverlayform" action=""  class="clearformbapprovalreturnoverlay">
				<span id="approvalreturnoverlayvalidation" class="validationEngineContainer"> 
				
					<div class="row" style="background:#fff">
						<div class="large-12 columns sectionheaderformcaptionstyle">
							Product Details
						</div>
						<?php
							echo '<div class="large-12 columns forgetinggridname" id="approvalreturngridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="approvalreturngrid" style="max-width:2000px; height:300px;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
							<!--<div class="inner-gridfooter footer-content footercontainer" id="approvalreturngridfooter">
									 Footer & Pagination content 
								</div>--></div>';
						?>
					</div>
					<div class="row" style="background:#fff">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
							<input type="button" id="approvalreturngridsubmit" name="approvalreturngridsubmit" value="Submit" class="alertbtnyes">
							<input type="button" id="approvalreturnclose" name="approvalreturnclose" value="Cancel" class="alertbtnno">
						</div>
					</div>
				
					</span>
					 <input type="hidden" name="approvaloutreturnproductid" id="approvaloutreturnproductid" />
					 <input type="hidden" name="approvaloutreturntagno" id="approvaloutreturntagno" />
			</form>
		</div>
	</div>
</div>
<!----Issue/Receipt overlay----->
<div class="large-12 columns" style="position:absolute;">
	<?php
		if($device=='phone') { ?>
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="issuereceiptoverlay" style="overflow-y: auto;overflow-x: hidden;">		
		<div class="large-10 medium-10 small-12 large-centered medium-centered small-centered columns">
			<form method="POST" name="issuereceiptform" style="" id="issuereceiptform" action=""  class="clearformbissuereceiptoverlay">
	<?php	} else { ?>
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="issuereceiptoverlay" style="overflow-y: auto;overflow-x: hidden;">		
		<div class="large-10 medium-10 small-10 large-centered medium-centered small-centered columns borderstyle" style="top: 80px !important;">
			<form method="POST" name="issuereceiptform" style="" id="issuereceiptform" action=""  class="clearformbissuereceiptoverlay">
				
	<?php } ?>
					
				<span id="issuereceiptvalidation" class="validationEngineContainer"> 					
					<div class="row" style="background:#fff">
					<div class="large-12 columns sectionheaderformcaptionstyle">
						Credit Details
					</div>
					<div class="large-12 columns" style="background-color: #fff;">
						<div class="input-field large-2 columns creditamtdiv" >
							<input type="text" class="" id="credittotalamount" name="credittotalamount" value="" tabindex="108" readonly>
							<label for="credittotalamount">Total Amount</label>
						</div>
						<div class="input-field large-2 columns creditwtdiv" >
							<input type="text" class="" id="credittotalpurewt" name="credittotalpurewt" value="" tabindex="108" readonly>
							<label for="credittotalpurewt">Total Purewt</label>
						</div>
						<div class="static-field large-2 columns">
							<label>Reference<span class="mandatoryfildclass"></span></label>								
							<select id="creditreference" name="creditreference" class="chzn-select" data-prompt-position="topLeft:14,36" tabindex="100" data-validation-engine="validate[required]">
								<option value=""></option>
								<?php foreach($creditreference as $key):?>
											<option value="<?php echo $key->creditreferenceid;?>" data-refername="<?php echo $key->creditreferencename;?>">
											<?php echo $key->creditreferencename;?></option>
									<?php endforeach;?>
							</select>
						</div>
						<div class="static-field large-2 columns">
							<label>Paid Type<span class="mandatoryfildclass">*</span></label>						
							<select id="creditpaymentirtypeid" name="creditpaymentirtypeid" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="1002">
							<option value=""></option>
							<?php $prev = ' ';
								foreach($paymentirtype as $key):
							?>
								<option value="<?php echo $key->paymentirtypeid;?>" data-refername="<?php echo $key->paymentirtypename;?>"><?php echo $key->paymentirtypename;?></option>
								<?php endforeach;?>						
							</select>
						</div>
						<div class="static-field large-1 columns hidedisplay">
							<label>I.R Type<span class="mandatoryfildclass">*</span></label>						
							<select id="creditissuereceiptid" name="creditissuereceiptid" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="1002">
							<option value=""></option>
							<?php $prev = ' ';
								foreach($issuereceipttype as $key):
							?>
								<option value="<?php echo $key->issuereceipttypeid;?>" data-refername="<?php echo $key->issuereceipttypename;?>"><?php echo $key->issuereceipttypename;?></option>
								<?php endforeach;?>						
							</select>
						</div>
						<div class="static-field large-1 columns hidedisplay">
							<label>WT I.R TYPE<span class="mandatoryfildclass">*</span></label>						
							<select id="creditweightissuereceiptid" name="creditweightissuereceiptid" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="1002">
							<option value=""></option>
							<?php $prev = ' ';
								foreach($issuereceipttype as $key):
							?>
								<option value="<?php echo $key->issuereceipttypeid;?>" data-refername="<?php echo $key->issuereceipttypename;?>"><?php echo $key->issuereceipttypename;?></option>
								<?php endforeach;?>						
							</select>
						</div>
						<div class="static-field large-1 columns hidedisplay">
							<label>Process<span class="mandatoryfildclass"></span></label>								
							<select id="processid" name="processid" class="chzn-select" data-prompt-position="topLeft:14,36" tabindex="100">
								<option value=""></option>
								<option value="0" data-refername="Manual">Manual</option>
								<option value="1" data-refername="Auto">Auto</option>
							</select>
						</div>
						<div class="input-field large-2 columns" id="overlaycreditno-div">
							<input type="text" class="" id="overlaycreditno" name="overlaycreditno" value="" tabindex="108" data-validation-engine="" maxlength="25">
							<label for="overlaycreditno">Credit No</label>
						</div>
						<div class="input-field large-2 columns creditamtdiv mainfinalamthiddendiv" >
							<input type="text" class="" data-validation-engine="validate[required,custom[number],decval[<?php echo $amount_round;?>]]"          id="mainfinalamthidden" name="mainfinalamthidden" value="0" tabindex="108" data-validation-engine="" maxlength="10">
							<label for="mainfinalamthidden">Amount</label>
						</div>
						<input type="hidden" name="finalamthidden" id="finalamthidden">
						<input type="hidden" name="accountledgerid" id="accountledgerid">
						<input type="hidden" name="creditsalesdate" id="creditsalesdate">
						<div class="input-field large-2 columns creditwtdiv mainfinalwthiddendiv" >
							<input type="text" class="" data-validation-engine="validate[required,custom[number],decval[<?php echo $weight_round;?>]]"          id="mainfinalwthidden" name="mainfinalwthidden" value="0" tabindex="108" data-validation-engine="" maxlength="10">
							<label for="mainfinalwthidden">Purewt</label>
						</div>
						
						<input type="hidden" name="finalwthidden" id="finalwthidden">
						<input type="hidden" name="amtissue" id="amtissue">
						<input type="hidden" name="amtreceipt" id="amtreceipt">
						<input type="hidden" name="weightissue" id="weightissue">
						<input type="hidden" name="weightreceipt" id="weightreceipt">
						
						<div class="row">
							<div class="large-2 columns sectionalertbuttonarea" style="text-align: center;top: 17px !important;">
								<input type="button" id="creditentryadd" name="creditentryadd" value="Add" data-id="ADD" class="creditcrud alertbtnyes">
								<input type="button" id="creditentryupdate" name="creditentryupdate" data-id="UPDATE" value="Update" class="creditcrud alertbtn" style="display:none;">
							</div>
						</div>
					</div>
						
				<?php
				echo '<div class="large-6 columns paddingbtm">
					  <div class="large-12 columns paddingzero">
					  <div class="large-12 columns viewgridcolorstyle paddingzero cleardataform borderstyle" style="top: 10px !important;">
					  <div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0 0rem;">
					  <span class="large-6 medium-6 small-6 columns text-left" >Adjustment Entry</span>
					  <div class="large-6 medium-6 small-6 columns stoneiconlisthide" style="text-align:right;"> 
						<span id="creditentryedit" class=" editiconclass" title="Edit"><i class="material-icons">edit</i></span>
						<span id="creditentrydelete" class="deleteiconclass"  title="Delete"><i class="material-icons">delete</i></span>
					  </div>
					  </div><div class="large-12 columns forgetinggridname" id="issuereceiptlocalgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="issuereceiptlocalgrid" style="max-width:2000px; height:300px;top:0px;">
						<!-- Table content[Header & Record Rows] for desktop-->
					  </div>
					  <!--<div class="inner-gridfooter footer-content footercontainer" id="issuereceiptlocalgridfooter">
						 Footer & Pagination content 
					  </div>--></div></div>
					  </div>
					  </div>';
				?>
						
				<?php
				echo '<div class="large-6 columns paddingbtm">
					  <div class="large-12 columns paddingzero">
					  <div class="large-12 columns viewgridcolorstyle paddingzero cleardataform borderstyle" style="top: 10px !important;">
					  <div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0 0rem;">
					  <span class="large-6 medium-6 small-6 columns text-left" >Pending Credit / Debit Entry</span>
					  <div class="large-6 medium-6 small-6 columns stoneiconlisthide" style="text-align:right;"> 
						<span id="issuereceiptpush" class="" title="Push"> <i class="material-icons">save</i></span>
					  </div>
					  </div><div class="large-12 columns forgetinggridname" id="issuereceiptgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="issuereceiptgrid" style="max-width:2000px; height:300px;top:0px;">
						<!-- Table content[Header & Record Rows] for desktop-->
						</div>
					  <!--<div class="inner-gridfooter footer-content footercontainer" id="issuereceiptgridfooter">
							 Footer & Pagination content 
					  </div>--></div></div>
					  </div>
					  </div>';
				?>
						</div>
				<?php
					if($device=='phone') { ?> 
						<div class="row" style="background:#fff">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: center;top: 8px !important;">
				<?php } else { ?>
						<div class="row" style="background:#fff">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right;top: 0px !important;">
						
				<?php } ?>
					<input type="button" id="issuereceiptsubmit" name="issuereceiptsubmit" value="Submit" class="alertbtnyes">
					<input type="button" id="issuereceiptclose" name="issuereceiptclose" value="Cancel" class="alertbtnno">
					</div>
					</div>
					</span>
					<input type="hidden" name="issuereceiptproductid" id="issuereceiptproductid" />
			</form>
		</div>
	</div>
</div>

<!----Order Grid overlay----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="ordergridoverlay" style="overflow-y: auto;overflow-x: hidden;">			
		<div class="large-10 medium-10 small-10 large-centered medium-centered small-centered columns borderstyle" style="top:20% !important;">
			<form method="POST" name="ordergridform" style="" id="ordergridform" action=""  class="clearformbordergridoverlay">
				<span id="ordergridvalidation" class="validationEngineContainer"> 
					<div class="row" style="background:#fff">
						<div class="large-12 columns sectionheaderformcaptionstyle">
							Product Details
						</div>
						<?php
							echo '<div class="large-12 columns forgetinggridname" id="ordergridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="ordergrid" style="max-width:2000px; height:300px;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
							<!--<div class="inner-gridfooter footer-content footercontainer" id="ordergridfooter">
									 Footer & Pagination content 
								</div>--></div>';
						?>
					</div>
					<div class="row" style="background:#fff">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right;top:15px;">
							<input type="button" id="ordergridsubmit" name="ordergridsubmit" value="Submit" class="alertbtnyes">
							<input type="button" id="ordergridclose" name="ordergridclose" value="Cancel" class="alertbtnno">
						</div>
					</div>
					<div class="row" style="background:#fff">&nbsp;</div>
				</span>
				<input type="hidden" name="orderproductid" id="orderproductid" />
			</form>
		</div>
	</div>
</div>

<!----Bulk sales Grid overlay----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="bulksalesgridoverlay" style="overflow-y: auto;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="large-10 medium-10 small-10 large-centered medium-centered small-centered columns" >
			<form method="POST" name="bulksalesgridform" style="" id="bulksalesgridform" action=""  class="clearformbordergridoverlay">
				<span id="bulksalesgridvalidation" class="validationEngineContainer"> 
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 columns sectionheaderformcaptionstyle">
							Product Details
						</div>
						<div class="large-12 columns" style="background-color: #617d8a;">
							<div class="input-field large-2 small-4 columns">
								<input type="text" class="" id="activecount" name="activecount" value="" tabindex="115" readonly>
								<label for="activecount">Active</label>
							</div>
							<div class="input-field large-2 small-4 columns">
								<input type="text" class="" id="conflictcount" name="conflictcount" value="" tabindex="115" readonly>
								<label for="conflictcount">Conflict</label>
							</div>
							<div class="large-8 columns sectionalertbuttonarea" style="text-align: right">
								<input type="button" id="bulksalesgridclear" name="bulksalesgridclear" value="Clear" class="alertbtnyes">
								<input type="button" id="bulksalesrgridfetch" name="bulksalesrgridfetch" value="Fetch" class="alertbtnno">
							</div>
						</div>
						<?php
							echo '<div class="large-12 columns forgetinggridname" id="bulksalesgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="bulksalesgrid" style="max-width:2000px; height:300px;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
							<!--<div class="inner-gridfooter footer-content footercontainer" id="bulksalesgridfooter">
									 Footer & Pagination content 
								</div>--></div>';
						?>
					</div>
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
							<input type="button" id="bulksalesgridsubmit" name="bulksalesgridsubmit" value="Submit" class="alertbtnyes">
							<input type="button" id="bulksalesrgridclose" name="bulksalesrgridclose" value="Cancel" class="alertbtnno">
						</div>
					</div>
					<div class="row">&nbsp;</div>
				</span>
				<input type="hidden" name="bulksalesrgridcloseproductid" id="bulksalesrgridcloseproductid" />
			</form>
		</div>
	</div>
</div>

<!--Take Order Detial overlay---->
<div class="large-6 columns" style="position:absolute;">
	<div class="overlay" id="takeorderdetailoverlay" style="overflow-y: auto;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="large-10 medium-10 small-11 large-centered medium-centered small-centered columns">
			<span id="takeorderformconditionvalidation" class="validationEngineContainer">
				<div class="large-12 columns paddingzero" style="background-color:#fff;border: 8px solid #b0bfc9;">
					<div class="large-12 columns sectionheaderformcaptionstyle">Take Order Detail</div>
					<div class="large-12 columns">&nbsp;</div>
					<?php
						echo '<div class="large-12 columns forgetinggridname" id="takeorderdetailgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="takeorderdetailgrid" style="max-width:2000px; height:240px;top:0px;">
							<!-- Table content[Header & Record Rows] for desktop-->
							</div>
							<!-- <div class="inner-gridfooter footer-content footercontainer" id="takeorderdetailgridfooter">
								Footer & Pagination content 
							</div>--></div>';
					?>
					<div class="large-12 columns" style="background:#f5f5f5">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
							<span class="firsttab" tabindex="1000"></span>
							<input type="button" id="takeorderdetailsubmit" name="" value="Submit" class="alertbtnyes" tabindex="1001">	
							<input type="button" id="takeorderdetailclose" name="" value="Cancel" tabindex="1002" class="alertbtnno  alertsoverlaybtn" >
							<span class="lasttab" tabindex="1003"></span>
						</div>
					</div>
				</div>
			</span>
		</div>
	</div>
</div>

<!-- Discountpassword  Overlay-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="discountpwdoverlay" style="overflow-y: scroll;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>
		<div class="">
			<div class="alert-panel" style="background-color:#465a63">
				<form method="POST" name="discountpwdoverlayform" style="" id="discountpwdoverlayform" action="" enctype="" class="clearformtaxoverlay">
					<span id="discountpwdoverlayformvalidation" class="validationEngineContainer"> 
						<span id="pdfsaveoverlayformvalidation" class="validationEngineContainer"> 
						<div class="alertmessagearea">
						<div class="alert-title">Discount Password Auth</div>
						<span class="firsttab" tabindex="999"></span>
						<div class="large-12 input-field">
							<input type="password" class="validate[required]" id="discountpassword" name="discountpassword"  tabindex="12" >
							<label for="discountpassword">Password <span class="mandatoryfildclass">*</span></label>
						</div>
						</div>
						<div class="alertbuttonarea">
							<input type="button" class="alertbtn" value="Save" name="discountpasswordtab" tabindex="1001" id="discountpasswordtab">
							<input type="button" class="flloop alertbtn" value="Cancel" name="discountpasswordcancel" tabindex="1002" id="discountpasswordcancel">
							<span class="lasttab" tabindex="1003"></span>
						</div>
						
						</span>
					</span>
				</form>
			</div>
		</div>
	</div>
</div>
	
<!-- Pancard  Overlay for government regulation-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="pancardoverlay" style="overflow-y: scroll;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>
		<div class="">
		<?php if($device=='phone') { ?>
			<div class="alert-panel" style="background-color:#465a63;left:0% !important; position:relative;     width: 90% !important;left: 5% !important;">
			<?php } else { ?>
			<div class="alert-panel" style="background-color:#465a63;left:40% !important; position:relative;">
			<?php } ?>
				<form method="POST" name="pancardoverlayform" style="" id="pancardoverlayform" action="" enctype="" class="clearformtaxoverlay">
					<span id="pancardoverlayformvalidation" class="validationEngineContainer"> 
						<span id="" class=""> 
							<div class="alertmessagearea">
							<div class="alert-title">Pancard Details</div>
							<span class="firsttab" tabindex="999"></span>
							<div class="large-12 input-field">
								<input type="text" class="validate[required]" id="pancardnumber" name="pancardnumber"  tabindex="12" >
								<label for="pancard">Pancard <span class="mandatoryfildclass">*</span></label>
							</div>
							</div>
							<div class="alertbuttonarea">
								<input type="button" class="alertbtn" value="Save" name="pancardsubmit" tabindex="1001" id="pancardsubmit">
								<input type="button" class="alertbtn" value="Cancal" name="pancardcancel" tabindex="1002" id="pancardcancel">
							</div>
						</span>
					</span>
				</form>
			</div>
		</div>
	</div>
</div>	

<!-- Main Header overlay  -->
<?php if($device=='phone') { ?>
<div class="large-10 columns" style="position:absolute;">
<div class="overlay" id="mainheaderoverlay" style="overflow-y: auto;overflow-x: hidden;z-index:40;">
	<div class="effectbox-overlay effectbox-default effectbox" id="groupsectionoverlay">
		<div class="large-12 columns" style="padding: 0px !important;height: 100% !important;">
			<form id="salescreateform" name="salescreateform" class="validationEngineContainer salescreateform" style="height:100% !important;">
				<div class="large-4 large-offset-4 columns paddingbtm" style="padding: 0px !important;height: 100% !important;">
					<div class="large-12 columns cleardataform border-modal-8px" style="padding-left: 0 !important; padding-right: 0 !important;">
						<div class="large-12 columns sectionheaderformcaptionstyle">Transactions</div>
						<div class="large-12 columns sectionpanel itemdetailsopacity paddingbtm tabletinfo" style="opacity:0;padding: 0px !important;">					
							<div class="large-12 small-12  columns tagadditionalchargeclear override-large-2" style="padding-left: 0 !important; padding-right: 0 !important;background: #fff;" id="additionalchargeaddvalidate2">
									<!--  <div class="toggle-headercontent">-->
										<div class="static-field large-6 medium-2 small-4 columns hidedisplay" id="">
											<input id="salesdate" type="text" data-prompt-position="bottomLeft" readonly="readonly" tabindex="1001" name="salesdate" data-validation-engine="validate[required]" />
											<label for="salesdate">Date<span class="mandatoryfildclass">*</span></label>
										</div>
										<div class="static-field large-6  medium-4 small-4 columns hidedisplay">
											<label>Sales Person<span class="mandatoryfildclass">*</span></label>						
											<select id="salespersonid" name="salespersonid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="1004">
											<option value=""></option>
											<?php $prev = ' ';
												foreach($employeedata as $key):
											?>
												<option data-name="<?php echo $key->employeename;?>" value="<?php echo $key->employeeid;?>"><?php echo $key->employeename;?></option>
												<?php endforeach;?>						
											</select>
										</div>
										<?php if(strtolower($mainlicense['cashcountershowhide']) == '1') {?>
										<div class="static-field large-6  medium-2 small-4 columns end" id="">
										<?php } else {?>
										<div class="static-field large-12  medium-12 small-12  columns hidedisplay">
										<?php } ?>
											<label>Cash Counter</label>
											<select id="cashcounter" name="cashcounter" class="chzn-select " data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="114">
											<option value="1">Select</option>
											<?php  foreach($cashcounter->result() as $key):?>
											<option value="<?php echo $key->cashcounterid;?>" data-label="<?php echo $key->cashcountername;?>"> <?php echo $key->cashcountername;?></option>
											<?php endforeach;?>
											</select>
											<input type="hidden" name="oldprocesscounterid" id="oldprocesscounterid" value="1" />
										</div>
										<div class="static-field large-12 medium-12 small-12 columns end" id="">
											<label>Transaction Type</label>
											<select id="salestransactiontypeid" name="salestransactiontypeid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="1006">
											<option value="1">Select</option>
											<?php  $prev = ' ';
											foreach($salestransactiontype->result() as $key): ?>
											<option value="<?php echo $key->salestransactiontypeid; ?>" data-additionalchargeid="<?php echo $key->additionalchargeid; ?>" data-pureadditionalchargeid="<?php echo $key->pureadditionalchargeid; ?>" data-transactionmode="<?php echo $key->transactionmodeid;?>" data-label="<?php echo $key->salestransactiontypelabel; ?>" 	data-summary="<?php echo $key->summarylabel;?>" data-stocktyperelation="<?php echo $key->stocktyperelationid;?>"
											data-serialnumbermastername="<?php echo $key->serialnumbermastername;?>"data-stocktypeid="<?php echo $key->stocktypeid;?>" data-name="<?php echo $key->salestransactiontypename;?>" data-salesmodeid="<?php echo $key->salesmodeid;?>"
											data-rateaddition="" data-roundingtype="<?php echo $key->roundingtype;?>" 
											data-discounttype="<?php echo $key->discountmodeid;?>" data-discountcalc="<?php echo $key->discountcalctypeid;?>" data-discountdisplayid="<?php echo $key->discountdisplayid;?>"
											data-roundoffremove="<?php echo $key->roundoffremove;?>" data-allowedaccounttypeid="<?php echo $key->allowedaccounttypeid;?>" data-chargesedit="<?php echo $key->chargesedit;?>" data-autotax="<?php echo $key->autotax;?>" data-headername="<?php echo $key->headerdetail;?>" data-printtemplatesid="<?php echo $key->printtemplatesid;?>" data-transactionmanageid="<?php echo $key->transactionmanageid;?>" data-taxtypeid="<?php echo $key->taxtypeid;?>"> <?php echo $key->salestransactiontypename; ?>
											</option>
											<?php endforeach; ?>
											</select>
										</div>				
										<div id="printtempaltedivhid" class="static-field large-12 medium-12 small-12 columns end">
											<label>Print Template</label>						
											<select id="printtemplateid" name="printtemplateid" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="1008">	
												<option value="1" data-transactiontype="0" data-printtype="1">Select Template</option>
												<?php foreach($printtemplate as $key):?>
												<option value="<?php echo $key->printtemplatesid;?>" data-transactiontype="<?php echo $key->salestransactiontypeid;?>" data-printtype="<?php echo $key->printtypeid;?>" data-setasdefault="<?php echo $key->setasdefault;?>">
												<?php echo $key->printtemplatesname;?></option>
												<?php endforeach;?>	
											</select>
										</div>
										<div class="static-field large-6 small-6 columns" id="ratecuttypeid-div" >
											<label>Rate Cut<span class="mandatoryfildclass">*</span></label>						
											<select id="ratecuttypeid" name="ratecuttypeid" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="1008">
											<option value="1">No</option>
											<option value="2">Yes</option>		
											</select>
										 </div>
										<div class="static-field large-6 small-6 columns" id="calculationtypeid">
											<label>Calc Type<span class="mandatoryfildclass">*</span></label>					
											<select id="transactionmodeid" name="transactionmodeid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36"  tabindex="1007">
												<?php foreach($transactionmode as $key): ?>
												<option value="<?php echo $key->transactionmodeid;?>">
												<?php echo $key->transactionmodename;?></option>
												<?php if($key->setdefault == 1){$tsetd = $key->transactionmodeid;}?>
												<?php endforeach;?>
											</select>
											<input type ="hidden" id="tranmodedefault" value='<?php echo $tsetd;?>' />
										</div>
									<?php if($mainlicense['estimateconcept'] == 'Yes') { ?>
									
										<div class="input-field large-6 small-6 columns" id="estimationnumber-div">
										
											<input type="text" class="" id="estimationnumber" name="estimationnumber" value=""  data-validation-engine="" tabindex="110">
											<label for="estimationnumber">Estimation No<span class="mandatoryfildclass" id="estimationnumber_req"></span></label>
										</div>
										<?php } ?>
										<div class="input-field large-6 small-6 columns"  id="billrefno-div">
											<input type="text" id="billrefno" name="billrefno" data-validation-engine="" tabindex="110" >
											<label for="billrefno">Supplier Bill No<span class="mandatoryfildclass" id="billrefno_req"></span></label>
										</div>
										<div class="static-field large-12 columns text-left acnt-dropdown end" id="accountiddiv">
											<label>Account</label>
											<input type="hidden" name="accountid" id="accountid"/>
											<input type="hidden" name="selaccounttypeid" id="selaccounttypeid"/>
											<input type="hidden" name="accountstateid" id="accountstateid"/>
										</div>
										<div class="static-field large-6 small-6 columns" id="loadbilltypediv">
											<label>Load Bill</label>
											<select id="loadbilltype" name="loadbilltype" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="101">									
												<?php	foreach($issuereceipttype as $key):
												?>
												<option value="<?php echo $key->issuereceipttypeid;?>" data-issuereceipttypeidhidden="<?php echo $key->issuereceipttypename;?>"><?php echo $key->baladvname;?></option>
												<?php endforeach;?>	
											</select>
										</div>
										<div class="input-field large-6 small-6 columns"  id="creditnodiv">
											<input type="text" id="creditno" name="creditno" data-validation-engine="" tabindex="133" >
											<label for="creditno">Credit No<span class="mandatoryfildclass" id="creditno_req"></span></label>
										</div>
										<div class="static-field large-6 small-6 columns" id="paymentirtypeiddiv" style="display:none;">
											<label>Paid Type<span class="mandatoryfildclass">*</span></label>						
											<select id="paymentirtypeid" name="paymentirtypeid" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="1002">
											<option value=""></option>
											<?php $prev = ' ';
												foreach($paymentirtype as $key):
											?>
												<option value="<?php echo $key->paymentirtypeid;?>"><?php echo $key->paymentirtypename;?></option>
												<?php endforeach;?>						
											</select>
										</div>
										<div class="input-field large-6 small-6 columns hidedisplay"  id="journalbilldatehdivid">
										<input type="text" id="journalbilldate" name="journalbilldate" data-validation-engine="" tabindex="136" >
											<label for="journalbilldate">Bill Date<span class="mandatoryfildclass" id="journalbilldate_req"></span></label>
										</div>
										<div class="input-field large-6 small-6 columns hidedisplay"  id="journalbillnohdivid">
											<input type="text" id="journalbillno" name="journalbillno" data-validation-engine="" tabindex="137" >
											<label for="journalbillno">Bill No<span class="mandatoryfildclass" id="journalbillno_req"></span></label>
										</div>
									</div>
								</div>
								<div style="top: 15px;position: relative;">
									<div class="large-12 columns sectionalertbuttonarea" style="text-align: center;background-color:white;">
										<span class="firsttab" tabindex="1000"></span>
										<input type="button" id="mainheadersubmit" name="" value="Submit" class="addkeyboard alertbtnyes" tabindex="1001" style="margin-top: 2px;">	
										<input type="button" id="mainheaderclose" name="" value="Cancel" tabindex="1002" class="alertbtnno  alertsoverlaybtn" style="margin-top: 2px;">
										<span class="lasttab" tabindex="1003"></span>
									</div>
								</div>
						</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>		
</div>
<?php } else  { ?>
<div class="large-10 columns" style="position:absolute;">
<div class="overlay" id="mainheaderoverlay" style="overflow-y: auto;overflow-x: hidden;z-index:40;">
	<div class="effectbox-overlay effectbox-default effectbox" id="groupsectionoverlay">
		<div class="large-12 columns" style="position: relative;top: 24% !important;">
			<form id="salescreateform" name="salescreateform" class="validationEngineContainer salescreateform" style="height:100% !important;">
				<div class="large-4 large-offset-4 columns paddingbtm" style="padding-right: 0em !important;">
					<div class="large-12 columns cleardataform border-modal-8px borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
						<div class="large-12 columns sectionheaderformcaptionstyle">Transactions</div>
						<div class="large-12 columns sectionpanel itemdetailsopacity paddingbtm tabletinfo" style="opacity:0;">					
							<div class="large-12 small-12  columns tagadditionalchargeclear override-large-2" style="padding-left: 0 !important; padding-right: 0 !important;background: #fff;min-height: 255px !important;" id="additionalchargeaddvalidate2">
									<!--  <div class="toggle-headercontent">-->
										<div class="static-field large-6 medium-2 small-4 columns hidedisplay" id="">
											<input id="salesdate" type="text" data-prompt-position="bottomLeft" readonly="readonly" tabindex="1001" name="salesdate" data-validation-engine="validate[required]" />
											<label for="salesdate">Date<span class="mandatoryfildclass">*</span></label>
										</div>
										<div class="static-field large-6  medium-4 small-4 columns hidedisplay">
											<label>Sales Person<span class="mandatoryfildclass">*</span></label>						
											<select id="salespersonid" name="salespersonid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="1004">
											<option value=""></option>
											<?php $prev = ' ';
												foreach($employeedata as $key):
											?>
												<option data-name="<?php echo $key->employeename;?>" value="<?php echo $key->employeeid;?>"><?php echo $key->employeename;?></option>
												<?php endforeach;?>						
											</select>
											</div>
											<?php if(strtolower($mainlicense['cashcountershowhide']) == '1') {?>
										<div class="static-field large-6  medium-2 small-4 columns end" id="">
										<?php } else {?>
										<div class="static-field large-6  medium-2 small-4  columns hidedisplay">
										<?php } ?>
											<label>Cash Counter</label>
											<select id="cashcounter" name="cashcounter" class="chzn-select " data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="114">
											<option value="1">Select</option>
											<?php  foreach($cashcounter->result() as $key):?>
											<option value="<?php echo $key->cashcounterid;?>" data-label="<?php echo $key->cashcountername;?>"> <?php echo $key->cashcountername;?></option>
											<?php endforeach;?>
											</select>
											<input type="hidden" name="oldprocesscounterid" id="oldprocesscounterid" value="1" />
										</div>
										<div class="static-field large-6 medium-4 small-4 columns end" id="">
											<label>Transaction Type</label>
											<select id="salestransactiontypeid" name="salestransactiontypeid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="1006">
											<option value="1">Select</option>
											<?php  $prev = ' ';
											foreach($salestransactiontype->result() as $key): ?>
											<option value="<?php echo $key->salestransactiontypeid; ?>" data-additionalchargeid="<?php echo $key->additionalchargeid; ?>" data-pureadditionalchargeid="<?php echo $key->pureadditionalchargeid; ?>" data-transactionmode="<?php echo $key->transactionmodeid;?>" data-label="<?php echo $key->salestransactiontypelabel; ?>" 	data-summary="<?php echo $key->summarylabel;?>" data-stocktyperelation="<?php echo $key->stocktyperelationid;?>"
											data-serialnumbermastername="<?php echo $key->serialnumbermastername;?>"data-stocktypeid="<?php echo $key->stocktypeid;?>" data-name="<?php echo $key->salestransactiontypename;?>" data-salesmodeid="<?php echo $key->salesmodeid;?>"
											data-rateaddition="" data-roundingtype="<?php echo $key->roundingtype;?>" 
											data-discounttype="<?php echo $key->discountmodeid;?>" data-discountcalc="<?php echo $key->discountcalctypeid;?>" data-discountdisplayid="<?php echo $key->discountdisplayid;?>"
											data-roundoffremove="<?php echo $key->roundoffremove;?>" data-allowedaccounttypeid="<?php echo $key->allowedaccounttypeid;?>" data-chargesedit="<?php echo $key->chargesedit;?>" data-autotax="<?php echo $key->autotax;?>" data-headername="<?php echo $key->headerdetail;?>" data-printtemplatesid="<?php echo $key->printtemplatesid;?>" data-transactionmanageid="<?php echo $key->transactionmanageid;?>" data-taxtypeid="<?php echo $key->taxtypeid;?>"> <?php echo $key->salestransactiontypename; ?>
											</option>
											<?php endforeach; ?>
											</select>
										</div>				
										<div id="printtempaltedivhid" class="static-field large-6 medium-4 small-4 columns end">
											<label>Print Template</label>						
											<select id="printtemplateid" name="printtemplateid" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="1008">	
												<option value="1" data-transactiontype="0" data-printtype="1">Select Template</option>
												<?php foreach($printtemplate as $key):?>
												<option value="<?php echo $key->printtemplatesid;?>" data-transactiontype="<?php echo $key->salestransactiontypeid;?>" data-printtype="<?php echo $key->printtypeid;?>" data-setasdefault="<?php echo $key->setasdefault;?>">
												<?php echo $key->printtemplatesname;?></option>
												<?php endforeach;?>	
											</select>
										</div>
										<div class="static-field large-6 small-4 columns" id="ratecuttypeid-div" >
											<label>Rate Cut<span class="mandatoryfildclass">*</span></label>						
											<select id="ratecuttypeid" name="ratecuttypeid" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="1008">
											<option value="1">No</option>
											<option value="2">Yes</option>		
											</select>
										 </div>
										<div class="static-field large-6 medium-3 small-4 columns" id="calculationtypeid">
											<label>Calc Type<span class="mandatoryfildclass">*</span></label>					
											<select id="transactionmodeid" name="transactionmodeid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36"  tabindex="1007">
												<?php foreach($transactionmode as $key): ?>
												<option value="<?php echo $key->transactionmodeid;?>">
												<?php echo $key->transactionmodename;?></option>
												<?php if($key->setdefault == 1){$tsetd = $key->transactionmodeid;}?>
												<?php endforeach;?>
											</select>
											<input type ="hidden" id="tranmodedefault" value='<?php echo $tsetd;?>' />
										</div>
										<?php if($mainlicense['estimateconcept'] == 'Yes') { ?>
										<div class="input-field large-6 small-6 columns" id="estimationnumber-div">
											<input type="text" class="" id="estimationnumber" name="estimationnumber" value=""  data-validation-engine="" tabindex="110">
											<label for="estimationnumber">Estimation No<span class="mandatoryfildclass" id="estimationnumber_req"></span></label>
										</div>
										<?php } ?>
										<div class="input-field large-6 small-4 columns"  id="billrefno-div">
											<input type="text" id="billrefno" name="billrefno" data-validation-engine="" tabindex="110" >
											<label for="billrefno">Supplier Bill No<span class="mandatoryfildclass" id="billrefno_req"></span></label>
										</div>
										<div class="static-field large-12 columns text-left acnt-dropdown end" id="accountiddiv">
											<label>Account</label>
											<input type="hidden" name="accountid" id="accountid"/>
											<input type="hidden" name="selaccounttypeid" id="selaccounttypeid"/>
											<input type="hidden" name="accountstateid" id="accountstateid"/>
										</div>
										<div class="static-field large-6 small-4 columns" id="loadbilltypediv">
											<label>Load Bill</label>
											<select id="loadbilltype" name="loadbilltype" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="101">									
												<?php	foreach($issuereceipttype as $key):
												?>
												<option value="<?php echo $key->issuereceipttypeid;?>" data-issuereceipttypeidhidden="<?php echo $key->issuereceipttypename;?>"><?php echo $key->baladvname;?></option>
												<?php endforeach;?>	
											</select>
										</div>
										<div class="input-field large-6 small-4 columns"  id="creditnodiv">
											<input type="text" id="creditno" name="creditno" data-validation-engine="" tabindex="133" >
											<label for="creditno">Credit No<span class="mandatoryfildclass" id="creditno_req"></span></label>
										</div>
										<div class="static-field large-6 small-4 columns" id="paymentirtypeiddiv" style="display:none;">
											<label>Paid Type<span class="mandatoryfildclass">*</span></label>						
											<select id="paymentirtypeid" name="paymentirtypeid" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="1002">
											<option value=""></option>
											<?php $prev = ' ';
												foreach($paymentirtype as $key):
											?>
												<option value="<?php echo $key->paymentirtypeid;?>"><?php echo $key->paymentirtypename;?></option>
												<?php endforeach;?>						
											</select>
										</div>
										<div class="input-field large-6 small-4 columns hidedisplay"  id="journalbilldatehdivid">
										<input type="text" id="journalbilldate" name="journalbilldate" data-validation-engine="" tabindex="136" >
											<label for="journalbilldate">Bill Date<span class="mandatoryfildclass" id="journalbilldate_req"></span></label>
										</div>
										<div class="input-field large-6 small-4 columns hidedisplay"  id="journalbillnohdivid">
											<input type="text" id="journalbillno" name="journalbillno" data-validation-engine="" tabindex="137" >
											<label for="journalbillno">Bill No<span class="mandatoryfildclass" id="journalbillno_req"></span></label>
										</div>
									</div>
								</div>
								<div style="top: -15px;position: relative;">
									<div class="large-12 columns sectionalertbuttonarea" style="text-align: center;background-color:white;">
										<span class="firsttab" tabindex="1000"></span>
										<input type="button" id="mainheadersubmit" name="" value="Submit" class="addkeyboard alertbtnyes" tabindex="1001" style="margin-top: 2px;">	
										<input type="button" id="mainheaderclose" name="" value="Cancel" tabindex="1002" class="alertbtnno  alertsoverlaybtn" style="margin-top: 2px;">
										<span class="lasttab" tabindex="1003"></span>
									</div>
								</div>
						</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>		
</div>
<?php
}
?>

<!-- image overlay  -->
<?php if($device == 'phone' || $device == 'tablet') { ?>
<div class="large-12 columns" style="position:absolute;padding:0px !important;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="imageformspan" style="overflow-y: auto;overflow-x: hidden;padding:0px !important;">	
		<div class="large-10 medium-10 small-12 large-centered medium-centered small-centered columns" style="padding:0px !important;">
			<form method="POST" name="itemimageoverlayform" style="" id="itemimageoverlayform" action="" enctype="" class="clearformitemimageoverlay">
				<span id="itemimageoverlayvalidation" class="validationEngineContainer"> 
					<div class="row">
						<div class="large-12 columns sectionheaderformcaptionstyle" style="padding-left: 32%!important;background:#f2f3fa !important;">
							Image Details
						</div>
						<div class="large-6 columns paddingbtm" style="padding:0px !important;">	
						<div class="large-12 columns cleardataform" style="box-shadow: none;background:#f2f3fa !important;">
							<div class="large-12 columns headerformcaptionstyle" style="text-align:center;background:#f2f3fa !important;">Photo</div>
							<div class="large-12 medium-12 small-12 columns" id="imagediv">
								<div class="large-4 medium-4 small-12 columns" style="text-align:left;">
									<label>Upload Image</label>
									<div id="tagfileuploadfromid" style="text-align:center; border:1px solid #CCCCCC;">
									<span  class="" title="Click to upload" style="padding:2px; color:#2574a9; font-size:26px;"><i class="material-icons">file_upload</i></span></div>
								</div>
								<input id="tagimage" type="hidden" name="tagimage" value="" tabindex="122">
								<span id="taglogomulitplefileuploader" style="display:none;">upload</span>						
							</div>
							<div class="large-12 columns">&nbsp;</div>
							<div class="large-12 columns">
								<div id="taglivedisplay" class="large-12 medium-12 small-12 columns" style="border:1px solid ##CCCCCC; height:20rem; padding:0px;"></div>						
							</div>			
							<div class="large-12 columns">&nbsp;</div>		
							<div class="large-4 columns" class="snapshotcapturebtn" style="text-align: center;">
								<input id="take-snapshot" class="btn leftformsbtn" type="button" value="Capture" tabindex="123">
							</div>
						</div>
						</div>
						<div class="large-6 columns end" style="background:#f2f3fa !important;">
							<div class="large-12 columns cleardataform paddingzero" style="box-shadow:none;">
								<div class="large-12 columns headerformcaptionstyle" style="text-align: center;background:#f2f3fa !important;">Preview</div>
								<div class="large-12 columns" style="background:#f2f3fa !important;">
									<input id="" type="hidden" name="" value="">
									<div id="tagimagedisplay" class="large-12 columns uploadcontainerstyle" style="height:27rem; padding:0px; border:1px solid #CCCCCC; overflow:auto;"></div>
									<div class="row">&nbsp;</div>
									<div class="row">&nbsp;</div>
									<div class="large-12" style="">
										<div class="snapshotsavebtn" style="text-align: center;">											
											<input type="button" value="Save" id="savecapturedimg" class="btn leftformsbtn" tabindex="124">
											<input type="button" value="Remove" id="resetcapturedimg" class="btn leftformsbtn" tabindex="125">
										</div>
									</div>
								</div>
							</div>					
							<textarea class="hidedisplay" id="imgurldata"></textarea>
							<textarea class="hidedisplay" id="imgurldatadelete"></textarea>
						</div>
					</div>
					<div class="row" style="background:#f2f3fa !important;">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: center">
							<input type="button" id="itemimageclose" name="itemimageclose" value="Cancel" class="alertbtnno">
						</div>
					</div>
				</span>
			</form>
		</div>
	</div>
</div>
<?php } else { ?>
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="imageformspan" style="overflow-y: auto;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="large-10 medium-10 small-10 large-centered medium-centered small-centered columns borderstyle" >
			<form method="POST" name="itemimageoverlayform" style="" id="itemimageoverlayform" action="" enctype="" class="clearformitemimageoverlay">
				<span id="itemimageoverlayvalidation" class="validationEngineContainer"> 
					<div class="row" style="background:#fff">
						<div class="large-12 columns sectionheaderformcaptionstyle">
							Image Details
						</div>
						<div class="large-6 columns paddingbtm">	
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
							<div class="large-12 columns headerformcaptionstyle">Photo</div>
							<div class="large-12 medium-12 small-12 columns" id="imagediv">
								<div class="large-4 medium-4 small-12 columns" style="padding-left: 0px;">
									<label>Upload Image</label>
									<div id="tagfileuploadfromid" style="text-align:center; border:1px solid #ffffff;">
									<span  class="" title="Click to upload" style="padding:2px; color:#2574a9; font-size:26px;"><i class="material-icons">file_upload</i></span></div>
								</div>
								<input id="tagimage" type="hidden" name="tagimage" value="" tabindex="122">
								<div class="large-4 medium-4 small-12 columns">
									<label>Take Picture</label>
									<div id="" style="text-align:center; border:1px solid #ffffff;">
									<span id="imgcapture" class="" title="Click To Take Photo" style="padding:2px; color:#2574a9; font-size:26px;"><i class="material-icons">add_a_photo</i></span></div>
								</div>	
								<span id="taglogomulitplefileuploader" style="display:none;">upload</span>						
							</div>
							<div class="large-12 columns">&nbsp;</div>
							<div class="large-12 columns">
								<div id="taglivedisplay" class="large-12 medium-12 small-12 columns" style="border:1px solid #ffffff; height:20rem; padding:0px;"></div>						
							</div>			
							<div class="large-12 columns">&nbsp;</div>		
							<div class="large-4 columns" class="snapshotcapturebtn">
							<input id="take-snapshot" class="btn leftformsbtn" type="button" value="Capture" tabindex="123">
							</div>
						</div>
						</div>
						<div class="large-6 columns end paddingbtm">
							<div class="large-12 columns cleardataform paddingzero borderstyle" style="">
								<div class="large-12 columns headerformcaptionstyle">Preview</div>
								<div class="large-12 columns">
									<input id="" type="hidden" name="" value="">
									<div id="tagimagedisplay" class="large-12 columns uploadcontainerstyle" style="height:27rem; padding:0px; border:1px solid #CCCCCC; overflow:auto;"></div>
									<div class="row">&nbsp;</div>
									<div class="row">&nbsp;</div>
									<div class="large-12" style="">
										<div class="snapshotsavebtn">											
											<input type="button" value="Save" id="savecapturedimg" class="btn leftformsbtn" tabindex="124">
											<input type="button" value="Remove" id="resetcapturedimg" class="btn leftformsbtn" tabindex="125">
										</div>
									</div>
								</div>
							</div>					
							<textarea class="hidedisplay" id="imgurldata"></textarea>
							<textarea class="hidedisplay" id="imgurldatadelete"></textarea>
						</div>
					</div>
					<div class="row" style="background:#fff">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
							<input type="button" id="itemimageclose" name="itemimageclose" value="Cancel" class="alertbtnno">
						</div>
					</div>
				</span>
			</form>
		</div>
	</div>
</div>
<?php } ?>

<!-- Tag Image Display in overlay. 
<div class="large-12 small-12 medium-12 columns" style="position:absolute;">
	<div class="overlay" id="salesdetailimageoverlaypreview" style="overflow-y: scroll;overflow-x: hidden;">
		<div class="large-12 columns end paddingbtm ">
				<div class="large-6 columns cleardataform z-depth-5 border-modal-8px" style="background: #ffffff;border-radius: 6px !important;">
					<div class="large-12 columns sectionheaderformcaptionstyle">Tag Image Preview</div>
					<div class="large-12 columns">
						<div id="salesdetailtagimagepreview" class="large-12 columns uploadcontainerstyle" style="overflow: scroll;height:27rem; padding:0px; border:1px solid #CCCCCC"></div>
						<div class="row">&nbsp;</div>
						<div class="row">&nbsp;</div>
						<div class="large-12" style="">
						<div class="tagimagepreviewcls">
							<input type="button" value="Close" id="salesdetailimagepreviewclose" class="btn leftformsbtn" tabindex="124">
						</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div> -->

<!----Vendor Items Status Grid overlay----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="vendoritemstatusgridoverlay" style="overflow-y: auto;overflow-x: hidden;">		
		<div class="large-10 medium-10 small-10 large-centered medium-centered small-centered columns borderstyle" style="top:20% !important;">
			<form method="POST" name="vendoritemstatusgridform" style="" id="ordergridform" action=""  class="clearformbordergridoverlay">
				<span id="vendoritemstatusgridvalidation" class="validationEngineContainer">
					<div class="row" style="background:#fff">&nbsp;</div>
					<div class="static-field large-6 small-4 columns" id="povendoritemlistiddiv">
						<label>Product List<span class="mandatoryfildclass">*</span></label>						
						<select id="povendoritemlistid" name="povendoritemlistid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="1002">
							<option value=""></option>					
						</select>
					</div>
					<div class="static-field large-3 small-4 columns" id="vendororderitemstatusiddiv">
						<label>Status Update<span class="mandatoryfildclass">*</span></label>						
						<select id="vendororderitemstatusid" name="vendororderitemstatusid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="1002">
							<option value=""></option>
							<?php	foreach($vendororderitemstatus as $key):
							?>
							<option value="<?php echo $key->vendororderitemstatusid;?>" data-vendororderitemstatusidhidden="<?php echo $key->vendororderitemstatusname;?>"><?php echo $key->vendororderitemstatusname;?></option>
							<?php endforeach;?>	
						</select>
					</div>
					<div class="large-12 columns" style="text-align: right">
						<input type="button" id="vendoritemstatusformtogrid" name="vendoritemstatusformtogrid" value="Update" class="alertbtnyes">
					</div>
					<div class="row" style="background:#fff">&nbsp;</div>
					<div class="row" style="background:#fff">
						<div class="large-12 columns sectionheaderformcaptionstyle">
							Product Details
						</div>
						<?php
							echo '<div class="large-12 columns forgetinggridname" id="vendoritemstatusgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="vendoritemstatusgrid" style="max-width:2000px; height:300px;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
							<!--<div class="inner-gridfooter footer-content footercontainer" id="vendoritemstatusgridfooter">
									 Footer & Pagination content 
								</div>--></div>';
						?>
					</div>
					<div class="row" style="background:#fff">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right;top:15px;">
							<input type="button" id="vendoritemstatusgridsubmit" name="vendoritemstatusgridsubmit" value="Submit" class="alertbtnyes">
							<input type="button" id="vendoritemstatusgridclose" name="vendoritemstatusgridclose" value="Cancel" class="alertbtnno">
						</div>
					</div>
					<div class="row" style="background:#fff">&nbsp;</div>
				</span>
				<input type="hidden" name="orderproductid" id="orderproductid" />
			</form>
		</div>
	</div>
</div>

<!---- Order Tracking Grid Overlay Details ----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="ordertrackinggridoverlay" style="overflow-y: auto;overflow-x: hidden;">		
		<div class="large-10 medium-10 small-10 large-centered medium-centered small-centered columns borderstyle" style="top:5% !important;">
			<form method="POST" name="ordertrackinggridform" style="" id="ordertrackinggridform" action=""  class="clearformbordergridoverlay">
				<span id="ordertrackinggridformvalidation" class="validationEngineContainer">
					<div class="row" style="background:#fff">&nbsp;</div>
					<div class="static-field large-3 small-3 columns">
						<label>Product Category</label>						
						<select id="ordtrackproductcatgid" name="ordtrackproductcatgid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="1002">
							<option value="1">Select</option>
							<?php foreach($ordertrackcatgdetails->result() as $key): ?>
							<option value="<?php echo $key->categoryid;?>" data-categoryid="<?php echo $key->categoryid;?>" data-label="<?php echo $key->categoryname;?>">
							<?php echo $key->categoryname;?></option>
								<?php endforeach;?>
						</select>
					</div>
					<div class="static-field large-3 small-3 columns">
						<label>Account Name</label>						
						<select id="ordtrackaccountid" name="ordtrackaccountid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="1002">
							<option value="1">Select</option>
							<?php foreach($ordertrackaccdetails->result() as $key):
								if($key->accounttypeid == 6) {?>
							<option value="<?php echo $key->accountid;?>" data-accountid="<?php echo $key->accountid;?>" data-label="<?php echo $key->accountname;?>">
							<?php echo $key->accountname;?></option>
								<?php } endforeach;?>
						</select>
					</div>
					<div class="static-field large-2 small-2 columns">
						<label>Short Name</label>						
						<select id="ordtrackaccountshrtid" name="ordtrackaccountshrtid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="1002">
							<option value="1">Select</option>
							<?php foreach($ordertrackaccdetails->result() as $key):
								if($key->accountshortname != '' || $key->accountshortname == 'null') {?>
							<option value="<?php echo $key->accountid;?>" data-accountid="<?php echo $key->accountid;?>" data-label="<?php echo $key->accountshortname;?>">
							<?php echo $key->accountshortname;?></option>
								<?php } endforeach;?>
						</select>
					</div>
					<div class="static-field large-2 small-2 columns">
						<input type="text" class="" id="ordtrackaccmobilenumber" name="ordtrackaccmobilenumber" value="" tabindex="1012">
						<label for="ordtrackaccmobilenumber">Mobile Number</label>
					</div>
					<div class="static-field large-2 small-2 columns">
						<input type="text" class="" id="ordtrackaccordernumber" name="ordtrackaccordernumber" value="" tabindex="1012">
						<label for="ordtrackaccordernumber">Order Number</label>
					</div>
					<div class="row" style="background:#fff">&nbsp;</div>
					<div class="row" style="background:#fff">&nbsp;</div>
					<div class="row" style="background:#fff">&nbsp;</div>
					<div class="row" style="background:#fff">&nbsp;</div>
					<div class="row" style="background:#fff">&nbsp;</div>
					<div class="large-12 columns" style="text-align: right;top: -45px;">
						<input type="button" id="ordertrackinggridsubmit" name="ordertrackinggridsubmit" value="Submit" class="alertbtnyes">&nbsp;&nbsp;&nbsp;
						<input type="button" id="ordertrackinggridclear" name="ordertrackinggridclear" value="Clear" class="alertbtnyes">
					</div>
					<!-- <div class="row" style="background:#fff">&nbsp;</div> -->
					<div class="row" style="background:#fff">
						<div class="large-12 columns sectionheaderformcaptionstyle" style="top: -30px;">
							Product Details
						</div>
						<?php
							echo '<div class="large-12 columns forgetinggridname" id="ordertrackinggridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="ordertrackinggrid" style="max-width:2000px; height:250px;top:0px;top: -40px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
							<!--<div class="inner-gridfooter footer-content footercontainer" id="ordertrackinggridfooter">
									 Footer & Pagination content 
								</div>--></div>';
						?>
					</div>
					<div class="row" style="background:#fff">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right;top:15px;">
							<input type="button" id="ordertrackinggridclose" name="ordertrackinggridclose" value="Cancel" class="alertbtnno">
						</div>
					</div>
					<div class="row" style="background:#fff">&nbsp;</div>
				</span>
				<input type="hidden" name="orderproductid" id="orderproductid" />
			</form>
		</div>
	</div>
</div>

<!-- Base delete overlay for combined modules -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="basecanceloverlayforcommodule">
		<div class="row sectiontoppadding">&nbsp;</div>
		<div class="overlaybackground">
			<div class="alert-panel">
				<div class="alertmessagearea cancelmessagearea" style="height:200px;">
				<div class="row">&nbsp;</div>
				<div class="alert-message basedelalerttxt">Are you sure,you want to cancel this bill?</div>
				<div class="static-field large-12 columns" id="cancelledcomment-div" style="text-align: left;"><textarea name="cancelledcomment" class="materialize-textarea" id="cancelledcomment" tabindex="128" rows="1" style="min-height:3rem;padding-top:10px !important;" cols="5" maxlength="100"></textarea><label for="cancelledcomment">Cancel Comment</label></div>
				</div>
				<div class="alertbuttonarea">
					<span class="firsttab" tabindex="1000"></span>
					<input type="button" id="" name="" value="Delete" tabindex="1001" class="alertbtnyes ffield commodyescls">
					<input type="button" id="basedeleteno" name="" tabindex="1002" value="Cancel" class="alertbtnno flloop  alertsoverlaybtn" >
					<span class="lasttab" tabindex="1003"></span>
				</div>
				<div class="row">&nbsp;</div>
			</div>
		</div>
	</div>
</div>

<!---- Questionaire Overlay for Generate Place Order (Vendor Overlay) ----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="questionairegenpovendoroverlay" style="overflow-y: auto;overflow-x: hidden;">		
		<div class="large-10 medium-10 small-10 large-centered medium-centered small-centered columns borderstyle" style="top:5% !important;">
			<form method="POST" name="questionairegenpovendorform" style="" id="questionairegenpovendorform" action=""  class="clearformbordergridoverlay">
				<span id="questionairegenpovendorformvalidation" class="validationEngineContainer">
					<div class="row" style="background:#fff">&nbsp;</div>
					<div class="large-12 columns sectionheaderformcaptionstyle">
						Questionaire Details
					</div>
					<div class="static-field large-12 small-4 columns" id="question1-div" >
						<label>1) Has finished product matching order design/finishing?<span class="mandatoryfildclass">*</span></label>						
						<select id="question1" name="question1" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="1008">
							<option value="NO">No</option>
							<option value="YES">Yes</option>
						</select>
					</div>
					<div class="static-field large-12 small-4 columns" id="question2-div" >
						<label>2) Has Finished product matching order weight?<span class="mandatoryfildclass">*</span></label>						
						<select id="question2" name="question2" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="1008">
							<option value="NO">No</option>
							<option value="YES">Yes</option>
						</select>
					</div>
					<div class="static-field large-12 small-4 columns" id="question3-div" >
						<label>3) Has finished product matching given size?<span class="mandatoryfildclass">*</span></label>						
						<select id="question3" name="question3" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="1008">
							<option value="NO">No</option>
							<option value="YES">Yes</option>
						</select>
					</div>
					<div class="static-field large-12 small-4 columns" id="question4-div" >
						<label>4) Has Finished product has" SJR" seal, Vendor seal?<span class="mandatoryfildclass">*</span></label>						
						<select id="question4" name="question4" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="1008">
							<option value="NO">No</option>
							<option value="YES">Yes</option>		
						</select>
					</div>
					<div class="static-field large-12 small-4 columns" id="question5-div" >
						<label>5) Has Finished product Hallmarked @ Krishna Hallmark-coimbatore?<span class="mandatoryfildclass">*</span></label>						
						<select id="question5" name="question5" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="1008">
							<option value="NO">No</option>
							<option value="YES">Yes</option>
						</select>
					</div>
					<div class="row" style="background:#fff">&nbsp;</div>
					<div class="row" style="background:#fff">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right;top:15px;">
							<input type="button" id="questionairegenpovendorformsubmit" name="questionairegenpovendorformsubmit" value="Submit" class="alertbtnyes">
							<input type="button" id="questionairegenpovendorformclose" name="questionairegenpovendorformclose" value="Cancel" class="alertbtnno">
						</div>
					</div>
					<div class="row" style="background:#fff">&nbsp;</div>
				</span>
				<input type="hidden" name="vendorquestionaireprintid" id="vendorquestionaireprintid" />
			</form>
		</div>
	</div>
</div>

<!---- Questionaire Overlay for Receive Order (Admin Overlay) ----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="questionairerecordadminoverlay" style="overflow-y: auto;overflow-x: hidden;">		
		<div class="large-10 medium-10 small-10 large-centered medium-centered small-centered columns borderstyle" style="top:5% !important;">
			<form method="POST" name="questionairerecordadminform" style="" id="questionairerecordadminform" action=""  class="clearformbordergridoverlay">
				<span id="questionairerecordadminformvalidation" class="validationEngineContainer">
					<div class="row" style="background:#fff">&nbsp;</div>
					<div class="large-12 columns sectionheaderformcaptionstyle">
						Questionaire Details
					</div>
					<div class="static-field large-12 small-4 columns" id="questionrecord1-div" >
						<label>1) Has Received order matching given design/ finishing?<span class="mandatoryfildclass">*</span></label>						
						<select id="questionrecord1" name="questionrecord1" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="1008">
							<option value="NO">No</option>
							<option value="YES">Yes</option>
						</select>
					</div>
					<div class="static-field large-12 small-4 columns" id="questionrecord2-div" >
						<label>2) Has Received order matching given order weight?<span class="mandatoryfildclass">*</span></label>						
						<select id="questionrecord2" name="questionrecord2" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="1008">
							<option value="NO">No</option>
							<option value="YES">Yes</option>
						</select>
					</div>
					<div class="static-field large-12 small-4 columns" id="questionrecord3-div" >
						<label>3) Has Received Order matching Given size?<span class="mandatoryfildclass">*</span></label>						
						<select id="questionrecord3" name="questionrecord3" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="1008">
							<option value="NO">No</option>
							<option value="YES">Yes</option>
						</select>
					</div>
					<div class="static-field large-12 small-4 columns" id="questionrecord4-div" >
						<label>4) Has Received Product has"SJR" seal, Vendor Seal?<span class="mandatoryfildclass">*</span></label>						
						<select id="questionrecord4" name="questionrecord4" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="1008">
							<option value="NO">No</option>
							<option value="YES">Yes</option>		
						</select>
					</div>
					<div class="static-field large-12 small-4 columns" id="questionrecord5-div" >
						<label>5) Has Received Product Hallmarked @ Krishna Hallmark-coimbatore?<span class="mandatoryfildclass">*</span></label>						
						<select id="questionrecord5" name="questionrecord5" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="1008">
							<option value="NO">No</option>
							<option value="YES">Yes</option>
						</select>
					</div>
					<div class="static-field large-12 small-4 columns" id="questionrecord6-div" >
						<label>6) Has Given sample on approval received back?<span class="mandatoryfildclass">*</span></label>						
						<select id="questionrecord6" name="questionrecord6" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="1008">
							<option value="NO">No</option>
							<option value="YES">Yes</option>
						</select>
					</div>
					<div class="row" style="background:#fff">&nbsp;</div>
					<div class="row" style="background:#fff">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right;top:15px;">
							<input type="button" id="questionairerecordadminformsubmit" name="questionairerecordadminformsubmit" value="Submit" class="alertbtnyes">
							<input type="button" id="questionairerecordadminformclose" name="questionairerecordadminformclose" value="Cancel" class="alertbtnno">
						</div>
					</div>
					<div class="row" style="background:#fff">&nbsp;</div>
				</span>
				<input type="hidden" name="adminquestionaireprintid" id="adminquestionaireprintid" />
			</form>
		</div>
	</div>
</div>

<div id="printingdiv" class="printinghidedisplay" >
</div>

<?php
	//hidden text box view columns field module ids
	$modid = $this->Basefunctions->getmoduleid($filedmodids);
	echo hidden('viewfieldids',$modid);
	echo hidden('moduleviewid',$modid);
?>
