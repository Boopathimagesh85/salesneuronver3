<style type="text/css">
	#orderitemsgrid .gridcontent{
		height: 63vh !important;
	}
</style>
<?php
	$device = $this->Basefunctions->deviceinfo();
	$dataset['gridtitle'] = 'Order Items Status';
	$dataset['titleicon'] = 'material-icons add_circle';
	$modtabgroup[0] = array("tabpgrpid"=>"1","tabgrpname"=>"Order Items Status","moduleid"=>"52","templateid"=>"2");
	$dataset['modtabgrp'] = $modtabgroup;
	$dataset['moduleid'] = '52';
	$dataset['otype'] = 'overlay';
	$dataset['action'][0] = array('actionid'=>'orderitemsoverlayclose','actiontitle'=>'Close','actionmore'=>'No','ulname'=>'orderitems');
	$this->load->view('Base/formwithgridmenuheader.php',$dataset);
?>	
<div class="" id="orderitemsoverlay">
	<div class="large-12 columns addformunderheader">&nbsp;</div>
	<div class="large-12 columns scrollbarclass addformcontainer">
	<div class="large-12 medium-12 small-12 large-centered medium-centered small-centered columns paddingzero">
		<div class="large-12 column paddingzero">
			<?php if($device=='phone') {?>
			<div class="closed effectbox-overlay effectbox-default" id="orderitemssectionoverlay">
			<?php }?> 
			<div class="row mblhidedisplay">&nbsp;</div>
			<form id="orderitemsform" name="orderitemsform" class="cleardataform">
			<div id="orderitemsvalidate" class="validationEngineContainer">
			<div id="editorderitemsvalidate" class="validationEngineContainer">
				<div class="large-4 columns end paddingbtm">
					<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #fff">
						<?php if($device=='phone') {?>
						<div class="large-12 columns sectionheaderformcaptionstyle">Order Items</div>
						<div class="large-12 columns sectionpanel">
							<?php  } else { ?>
								<div class="large-12 columns headerformcaptionstyle">Order Items</div>
						<?php  	} ?> 
						<div class="static-field large-12 columns">
							<label>Vendor Name<span class="mandatoryfildclass">*</span></label>
							<select data-validation-engine="" class="chzn-select" id="orderitemaccountid" name="orderitemaccountid" data-prompt-position="topLeft:14,36">
								<option value=""></option>
							</select>
						</div>
						<div class="static-field large-6 columns">
							<label>Status Date<span class="mandatoryfildclass">*</span></label>					
							<input id="orderitemdate" class="fordatepicicon" data-validation-engine="validate[required]" type="text" data-dateformater="dd-mm-yy" data-prompt-position="topLeft" readonly="readonly" tabindex="" name="orderitemdate">
						</div>
						<div class="static-field large-6 columns">
							<label>Order Number<span class="mandatoryfildclass">*</span></label>
							<select id="poordernumberid" name="poordernumberid" data-validation-engine="" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="104">
								<option value="" data-ordernumber=""></option>
							</select>
						</div>
						<div class="static-field large-12 columns">
							<label>Place Order Status<span class="mandatoryfildclass">*</span></label>
							<select data-validation-engine="validate[required]" class="chzn-select" id="orderitemsstatusacc" name="orderitemsstatusacc" data-prompt-position="topLeft:14,36">
								<option value="0" data-orderitemsstatusacc="Accepted">Accepted</option>
								<option value="1" data-orderitemsstatusacc="Rejected">Rejected</option>
								<option value="2" data-orderitemsstatusacc="Customer Review">Review</option>
							</select>
						</div>
						<div class="input-field large-12 columns">
							<textarea name="orderitemsstatuscomment" class="materialize-textarea" data-validation-engine="validate[required,maxSize[200]]" id="orderitemsstatuscomment" rows="3" cols="40"></textarea>
							<label for="orderitemsstatuscomment">Comments<span class="mandatoryfildclass">*</span></label>
						</div>
						<div class="input-field large-12 columns"></div>
						<div class="input-field large-12 columns"></div>
						<div class="input-field large-12 columns" id="adminorderitemstatuscomment-div">
							<textarea name="adminorderitemsstatuscomment" class="materialize-textarea" data-validation-engine="" id="adminorderitemsstatuscomment" rows="3" cols="40"></textarea>
							<label for="adminorderitemsstatuscomment">Admin Comments<span class="mandatoryfildclass">*</span></label>
						</div>
						<div class="input-field large-12 columns"></div>
						<div class="input-field large-12 columns"></div>
					<?php if($device=='phone') { ?>
						</div>
						<div class="divider large-12 columns"></div>
						<div class="large-12 columns submitkeyboard sectionalertbuttonarea" style="text-align: right">
							<input id="updateorderitemsbtn" class="btn" type="button" value="Submit" name="updateorderitemsbtn">
							<input type="hidden" value="" name="orderitemssortorder" id="orderitemssortorder">
							<input type="hidden" value="" name="orderitemssortcolumn" id="orderitemssortcolumn">
							<input type="hidden" value="" name="posalesid" id="posalesid">
							<input type="hidden" value="" name="poaccountid" id="poaccountid">
							<input type="hidden" value="" name="poaccountname" id="poaccountname">
						</div>
							
						<?php } else { ?> 	
						<div class="large-12 columns" style="text-align: right">
							<label> </label>
							<input type="hidden" value="" name="orderitemssortorder" id="orderitemssortorder">
							<input type="hidden" value="" name="orderitemssortcolumn" id="orderitemssortcolumn">
							<input type="hidden" value="" name="posalesid" id="posalesid">
							<input type="hidden" value="" name="poaccountid" id="poaccountid">
							<input type="hidden" value="" name="poaccountname" id="poaccountname">
							<input type="hidden" value="" name="ordertransactionpage" id="ordertransactionpage">
							<input id="updateorderitemsbtn" class="btn" type="button" value="Submit" name="updateorderitemsbtn">
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
			</div>
			</form>
			<?php if($device=='phone') {?>
			</div>
			<?php }?>
			<div class="large-8 columns paddingbtm">
				<div class="large-12 columns paddingzero">
					<div class="large-12 columns viewgridcolorstyle paddingzero  borderstyle">
						<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0 0rem;">
							<span class="large-6 medium-6 small-6 columns text-left" >Order Items List</span>
						</div>
						<?php
								$device = $this->Basefunctions->deviceinfo();
								if($device=='phone') {
									echo '<div class="large-12 columns paddingzero forgetinggridname" id="orderitemsgridwidth"><div class=" inner-row-content inner-gridcontent" id="orderitemsgrid" style="height:70vh !important; top:0px;">
										<!-- Table header content , data rows & pagination for mobile-->
									</div>
									<footer class="inner-gridfooter footercontainer" id="orderitemsgridfooter">
										<!-- Footer & Pagination content -->
									</footer></div>';
								} else {
									echo '<div class="large-12 columns forgetinggridname" id="orderitemsgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="orderitemsgrid" style="max-width:2000px; height:70vh !important; top:0px;">
									<!-- Table content[Header & Record Rows] for desktop-->
									</div>
									<div class="inner-gridfooter footer-content footercontainer" id="orderitemsgridfooter">
										<!-- Footer & Pagination content -->
									</div></div>';
								}
							?>	
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>