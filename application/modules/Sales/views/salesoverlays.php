<?php
	$device = $this->Basefunctions->deviceinfo();
	?>
<!--Tax Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="salestaxovrelay" style="overflow-y: scroll;overflow-x: hidden">
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="large-3 medium-6 large-centered medium-centered columns overlayborder" >
			<form method="POST" name="" style="" id="" action="" enctype="" class="">
				<span id="" class="validationEngineContainer"> 
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#617d8a">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-10 columns " style="background:none">Tax Details</div>
							<div class="small-1 columns " id="salestaxoverlayclose" style="text-align:right;cursor:pointer;background:none">X</div>
						</div>
						<div class="large-6 columns">             
							<label>VAT</label>
							<input type="text" class="" id="" name="" value="5%">     
						</div>
						<div class="large-6 columns">             
							<label>&nbsp;</label>
							<input type="text" class="" id="" disabled value="5323">     
						</div>
						<div class="large-6 columns">             
							<label>CST</label>
							<input type="text" class="" id="" name="" value="3%">     
						</div>
						<div class="large-6 columns">             
							<label>&nbsp;</label>
							<input type="text" class="" id="" disabled value="4533">     
						</div>
						<div class="large-6 columns" style="text-align:right">   
							<label>&nbsp;</label>						
							<label style="font-weight:bold">TAX Total = </label>
						</div>
						<div class="large-6 columns">             
							<label>&nbsp;</label>
							<input type="text" class="" id="" disabled value="10221">     
						</div>
						<div class="large-12 columns"> &nbsp; </div>
						<div class="large-12 columns"> &nbsp; </div>
					</div>
					<div class="row" style="background:#617d8a">
						<div class="medium-4 large-4 columns"> &nbsp; </div>
						<div class="small-4 small-centered medium-4 large-4 columns">
							<input type="button" id="" name="" value="Submit" class="btn formbuttonsalert" >	
						</div>
						<div class="medium-4 large-4 columns"> &nbsp; </div>
						<div class="large-12 columns"> &nbsp; </div>
					</div>
					<div class="row">&nbsp;</div>
					<!-- hidden fields -->
				</span>
			</form>
		</div>
	</div>
</div>

<!--Chit Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="saleschitovrelay" style="overflow-y: scroll;overflow-x: hidden">
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="large-3 medium-6 large-centered medium-centered columns overlayborder" >
			<form method="POST" name="" style="" id="" action="" enctype="" class="">
				<span id="" class="validationEngineContainer"> 
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#617d8a">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-10 columns " style="background:none">Chit Details</div>
							<div class="small-1 columns " id="saleschitoverlayclose" style="text-align:right;cursor:pointer;background:none">X</div>
						</div>
						<div class="large-12 columns"> &nbsp; </div>
						<div class="large-12 columns"> &nbsp; </div>
						<div class="large-12 column">
							<div class="large-6 columns" style="text-align:right">    	
								<label>Principal Amount = </label>							   
							</div>
							<div class="large-6 columns">             
								<input type="text" class="" id="" value="678">     
							</div>
						</div>
						<div class="large-12 column">
							<div class="large-6 columns" style="text-align:right">    	
								<label>Internal Amount = </label>							   
							</div>
							<div class="large-6 columns">             
								<input type="text" class="" id="" value="6545">     
							</div>
						</div>
						<div class="large-12 column">
							<div class="large-6 columns" style="text-align:right">    	
								<label>Other Amount = </label>							   
							</div>
							<div class="large-6 columns">             
								<input type="text" class="" id="" value="4325">     
							</div>
						</div>
						<div class="large-12 column">
							<div class="large-6 columns" style="text-align:right">    	
								<label>&nbsp;</label>
								<label>Final Amount = </label>							   
							</div>
							<div class="large-6 columns">             
								<label>&nbsp;</label>
								<input type="text" class="" id="" value="23443">     
							</div>
						</div>
						<div class="large-12 columns"> &nbsp; </div>
						<div class="large-12 columns"> &nbsp; </div>
					</div>
					<div class="row" style="background:#617d8a">
						<div class="medium-4 large-4 columns"> &nbsp; </div>
						<div class="small-4 small-centered medium-4 large-4 columns">
							<input type="button" id="" name="" value="Submit" class="btn formbuttonsalert" >	
						</div>
						<div class="medium-4 large-4 columns"> &nbsp; </div>
						<div class="large-12 columns"> &nbsp; </div>
					</div>
					<div class="row">&nbsp;</div>
					<!-- hidden fields -->
				</span>
			</form>
		</div>
	</div>
</div>

<!--Discount Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="salesdiscountovrelay" style="overflow-y: scroll;overflow-x: hidden">
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="large-3 medium-6 large-centered medium-centered columns overlayborder" >
			<form method="POST" name="" style="" id="" action="" enctype="" class="">
				<span id="" class="validationEngineContainer"> 
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#617d8a">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-10 columns " style="background:none">Discount Details</div>
							<div class="small-1 columns " id="salesdiscountoverlayclose" style="text-align:right;cursor:pointer;background:none">X</div>
						</div>
						<div class="large-12 columns"> &nbsp; </div>
						<div class="large-12 columns"> &nbsp; </div>
						
							<div class="large-6 columns">    	
								<label>Type</label>	
								<select id="" name="" class="chzn-select">
									<option value=""></option>							
								</select>					
							</div>
							<div class="large-6 columns">             
								<label>Value</label>	
								<input type="text" class="" id="" value="">     
							</div>
							<div class="large-12 columns">             
								<label>Discount Amount</label>	
								<input type="text" class="" id="" value="">     
							</div>
						<div class="large-12 columns"> &nbsp; </div>
						<div class="large-12 columns"> &nbsp; </div>
					</div>
					<div class="row" style="background:#617d8a">
						<div class="medium-4 large-4 columns"> &nbsp; </div>
						<div class="small-4 small-centered medium-4 large-4 columns">
							<input type="button" id="" name="" value="Submit" class="btn formbuttonsalert" >	
						</div>
						<div class="medium-4 large-4 columns"> &nbsp; </div>
						<div class="large-12 columns"> &nbsp; </div>
					</div>
					<div class="row">&nbsp;</div>
					<!-- hidden fields -->
				</span>
			</form>
		</div>
	</div>
</div>
<!--Bhavcut Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="salesbhavcutovrelay" style="overflow-y: scroll;overflow-x: hidden">
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="large-3 medium-6 large-centered medium-centered columns overlayborder" >
			<form method="POST" name="" style="" id="" action="" enctype="" class="">
				<span id="" class="validationEngineContainer"> 
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#617d8a">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-10 columns " style="background:none">Bhavcut Details</div>
							<div class="small-1 columns " id="salesbhavcutoverlayclose" style="text-align:right;cursor:pointer;background:none">X</div>
						</div>
						<div class="large-12 columns"> &nbsp; </div>
						<div class="large-12 columns"> &nbsp; </div>
						
							<div class="large-6 columns">    	
								<label>Rate</label>	
								<input type="text" class="" id="" value="">     				
							</div>
							<div class="large-6 columns">             
								<label>Metal</label>	
								<input type="text" class="" id="" value="">     
							</div>
							<div class="large-12 columns">             
								<label>Amount</label>	
								<input type="text" class="" id="" value="">     
							</div>
						<div class="large-12 columns"> &nbsp; </div>
						<div class="large-12 columns"> &nbsp; </div>
					</div>
					<div class="row" style="background:#617d8a">
						<div class="medium-4 large-4 columns"> &nbsp; </div>
						<div class="small-4 small-centered medium-4 large-4 columns">
							<input type="button" id="" name="" value="Submit" class="btn formbuttonsalert" >	
						</div>
						<div class="medium-4 large-4 columns"> &nbsp; </div>
						<div class="large-12 columns"> &nbsp; </div>
					</div>
					<div class="row">&nbsp;</div>
					<!-- hidden fields -->
				</span>
			</form>
		</div>
	</div>
</div>
<!-- Discount Auth Overlay -->
	<?php
				if($device=='phone') 
	{
		?>
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="discountauthovrelay" style="overflow-y: scroll;overflow-x: hidden">	
		<div class="large-3 medium-6 large-centered medium-centered columns" style="height: 100% !important;">
			<form method="POST" name="discountauthovrelayform" style="height: 100% !important;" id="discountauthovrelayform" class="">
				<span id="discountautnvalidation" class="validationEngineContainer"> 
					<div class="row" style="background:#fff;height: 100% !important;">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-10 columns " style="background:none;top:10px;">Discount Authorization</div>
							<span class="icon-box" id="discountauthovrelayclose" ><i class="material-icons" title="Close">close</i></span>
							
						</div>
						<div class="static-field large-12 columns">
							<label>User Name<span class="mandatoryfildclass">*</span></label>						
							<select id="discountauthemployeeid" name="discountauthemployeeid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="1004">
								<option value=""></option>
								<?php $prev = ' ';
									foreach($employeedata as $key):
								?>
								<option data-name="<?php echo $key->employeename;?>" value="<?php echo $key->employeeid;?>"><?php echo $key->employeename;?></option>
								<?php endforeach;?>						
							</select>
						</div>
						<div class="large-12 columns">             
							<label>Disc AuthPassword<span class="mandatoryfildclass">*</span></label>
							<input type="password" data-validation-engine="validate[required]" class="" id="discountauthpassword" name="discountauthpassword" value="" maxlength="20">
							<input type="hidden" data-validation-engine="" class="" id="olddiscountauthpassword" name="olddiscountauthpassword" value="">							
						</div>
						<div class="large-12 columns">             
							<label>Discount Limit<span class="mandatoryfildclass">*</span></label>
							<input type="text" data-validation-engine="validate[required,custom[number],decval[2]]" class="" id="discountauthlimit" name="discountauthlimit" readonly="readonly" value="" maxlength="10">
						</div>
						<div class="large-12 columns"> &nbsp; </div>
						<div class="large-12 columns"> &nbsp; </div>
						<div class="small-4 small-centered medium-4 large-4 columns">
							<input type="button" id="discountauthsubmit" name="discountauthsubmit" value="Submit" class="alertbtnyes formbuttonsalert" >	
						</div>
					</div>
				</span>
			</form>
		</div>
	</div>
</div>
	<?php } else { ?>
	
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="discountauthovrelay" style="overflow-y: scroll;overflow-x: hidden">		
		<div class="large-3 medium-6 large-centered medium-centered columns borderstyle" style="top:20% !important;" >
			<form method="POST" name="discountauthovrelayform" style="" id="discountauthovrelayform" class="">
				<span id="discountautnvalidation" class="validationEngineContainer"> 
					<div class="row" style="background:#fff">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-10 columns " style="background:none">Discount Authorization</div>
							<div class="small-2 columns " id="discountauthovrelayclose" style="text-align:right;cursor:pointer;background:none">X</div>
						</div>
						<div class="static-field large-12 columns">
							<label>User Name<span class="mandatoryfildclass">*</span></label>						
							<select id="discountauthemployeeid" name="discountauthemployeeid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="1004">
								<option value=""></option>
								<?php $prev = ' ';
									foreach($employeedata as $key):
								?>
								<option data-name="<?php echo $key->employeename;?>" value="<?php echo $key->employeeid;?>"><?php echo $key->employeename;?></option>
								<?php endforeach;?>						
							</select>
						</div>
						<div class="large-12 columns">             
							<label>Disc AuthPassword<span class="mandatoryfildclass">*</span></label>
							<input type="password" data-validation-engine="validate[required]" class="" id="discountauthpassword" name="discountauthpassword" value="" maxlength="20">
							<input type="hidden" data-validation-engine="" class="" id="olddiscountauthpassword" name="olddiscountauthpassword" value="">							
						</div>
						<div class="large-12 columns">             
							<label>Discount Limit<span class="mandatoryfildclass">*</span></label>
							<input type="text" data-validation-engine="validate[required,custom[number],decval[2]]" class="" id="discountauthlimit" name="discountauthlimit" readonly="readonly" value="" maxlength="10">
						</div>
						<div class="large-12 columns"> &nbsp; </div>
						<div class="large-12 columns"> &nbsp; </div>
					</div>
					<div class="row" style="background:#fff">
						<div class="medium-4 large-4 columns"> &nbsp; </div>
						<div class="small-4 small-centered medium-4 large-4 columns">
							<input type="button" id="discountauthsubmit" name="discountauthsubmit" value="Submit" class="alertbtnyes formbuttonsalert" >	
						</div>
					</div>
				</span>
			</form>
		</div>
	</div>
</div>
	<?php } ?>