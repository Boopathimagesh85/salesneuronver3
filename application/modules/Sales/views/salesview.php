<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
	<style>
		.summarycontainer ul li{
			list-style:none;
			text-align:left;
			font-size:1rem;
			padding-bottom:5px;
		}
		.summarycontainer ul{
			margin-left:0;
			margin-bottom:0.4rem;
		}
		.summarycontainer ul .header{
			color:#546e7a;
			font-size:1rem;
		}
		.summarycontainer ul span{
			color:black;
			display:inline-block;
			line-height:auto;
			vertical-align:middle;
			*width:40%;
			text-overflow:ellipsis;
			overflow:hidden;
			font-size:0.8rem;
			line-height:1.2rem;
			height:1.2rem;
		}
		.summarycontainer ul span.title{
			color:black;
			width:8rem;
			font-size:0.7rem;
			display:block;
			text-transform:uppercase;
		}
		.summarycontainer ul span.value{
			display:inline;
		}
		.ratepadding{
			padding-right:10px;
			width:80px;
			text-overflow:ellipsis;
			overflow:hidden;
		}
		@media screen and (min-width: 64em){
		.override-large-2 .large-2{width:10%;}
		.override-large-2 .large-3{width:20%;}
		.override-large-2 .large-1{width:8%;}
		.override-large-2 .large-3#employeepersonid-div{width:16%;}
		.toggle-headercontent .large-2{width:20%;}
		
		}
		.headerformcaptionstyleforprofile span{
			display: inline-block;
			line-height: 1.7rem;
			overflow: hidden !important;
			text-overflow: ellipsis;
			white-space: nowrap;
		}
		.innerheader-title{margin-right:10px;cursor:pointer;}
		.innerheader-title i{top:-1px}
		
		.input-field , .static-field{max-height:47px;}
		.acnt-dropdown a{margin:0 !important;}
	</style>
	<?php 
		$industryid = $this->Basefunctions->industryid;
	?>
</head>
<body class="accountclass">
	<?php
		$device = $this->Basefunctions->deviceinfo();
		$dataset['gridenable'] = "yes"; //no
		$dataset['griddisplayid'] = "salesgriddisplay";
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['spanattr'] = array();
		$dataset['gridtableid'] = "salesgrid";
		$dataset['griddivid'] = "salesgriddiv";
		$dataset['moduleid'] = $moduleids;
		$dataset['forminfo'] = array(array('id'=>'salesaddformdiv','class'=>'hidedisplay','formname'=>'salesform'),array('id'=>'salessettingsview','class'=>'hidedisplay','formname'=>'salessettingsview'));
		if($device=='phone') {
			$this->load->view('Base/gridmenuheadermobile',$dataset);
			$this->load->view('Base/overlaymobile');
			$this->load->view('Base/viewselectionoverlay');
		} else {
			$this->load->view('Base/gridmenuheader',$dataset);
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/basedeleteform');
		$this->load->view('Base/modulelist');
	?>
	<!----push to pending delivery overlay----->
<div class="large-12 small-12 medium-12 columns" style="position:absolute;">
		<div class="overlay" id="p2pdeliveryoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
			<div class="row sectiontoppadding">&nbsp;</div>
			<div class="large-4 medium-4 small-12 large-centered medium-centered small-centered columns ">
				<div class="large-12 column paddingzero">
					<div class="row">&nbsp;</div>
					<form id="p2pdeliveryform" name="p2pdeliveryform" class="cleardataform">
					<div id="p2pdeliveryformvalidate" class="validationEngineContainer">
					<div class="large-12 columns end paddingbtm ">
						<div class="large-12 columns cleardataform z-depth-5 border-modal-8px borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff">
							<div class="large-12 columns sectionheaderformcaptionstyle">Push To Pending Delivery</div>
							<div class="large-12 columns sectionpanel" style="padding: 15px 15px;">
							<div class="static-field large-12 columns">
									<label>Itemdetail<span class="mandatoryfildclass">*</span></label>
									<select id="itemdetail" name="itemdetail" class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="104">
									  <option value=""></option>
									</select>
							</div>
							<div class="input-field large-6 columns">
									<input type="text" id="currentstatus" name="currentstatus" value="" class="" tabindex="103" readonly />
									<input type="hidden" id="currentstatusid" name="currentstatusid" value=""/>
									<label for="currentstatus">Current Status</label>
							</div>
							<div class="input-field large-6 columns">
									<input type="text" id="newstatus" name="newstatus" value="" class="" tabindex="103" readonly />
									<input type="hidden" id="newstatusid" name="newstatusid" value=""/>
									<label for="newstatus">New Status</label>
							</div>
							<div class="static-field large-12 columns deliveryhidediv">
									<label>To Storage<span class="mandatoryfildclass">*</span></label>
									<select id="tocounter" name="tocounter" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="104">
									  <option value=""></option>
									  <?php  foreach($deliverycounter->result() as $key):?>
											<option value="<?php echo $key->counterid;?>"> <?php echo $key->counterid.' - '.$key->countername;?></option>
											<?php endforeach;?>		
									</select>
							</div>
							<?php if($mainlicense['barcodeoption'] == '2' || $mainlicense['barcodeoption'] == '3') {?>
							<div class="input-field large-6 columns deliveryhidediv">
									<input type="text" id="deliveryrfidtagno" name="deliveryrfidtagno" value="" class="validate[required,funcCall[rfitagnocheck]]" tabindex="103" />
									<label for="deliveryrfidtagno">RFID Tag No<span class="mandatoryfildclass">*</span></label>
							</div>
							<?php } ?>
							<div class="static-field large-6 columns  deliveryhidediv">
									<label>Tag Template</label>
									<select id="tagtemplateid" name="tagtemplateid" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="104">
									  <option value="1">No Print</option>
										<?php foreach($tagtemplate as $key):?>
										<option value="<?php echo $key->tagtemplateid;?>">
										<?php echo $key->tagtemplatename;?></option>
										<?php endforeach;?>		
									</select>
							</div>
							<div class="input-field large-12 columns deliveryhidediv" style="max-height:200px !important;">
									<textarea name="description" class="materialize-textarea validate[maxSize[200]]" id="description" rows="3" cols="40"></textarea>
									<label for="description">Description</label>
							</div>
							<div class="divider large-12 columns"></div>
							<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">
								<input type="button" id="addp2pbtn" name="" tabindex="106" value="Submit" class="alertbtnyes" >
								<input type="button" id="closep2pdeliveryoverlay" name="" value="Cancel" tabindex="107" class="flloop alertbtnno alertsoverlaybtn" ></div>
						
							</div>
							</div>
						</div>
						</div>
					</form>
					
				</div>
			</div>		
		</div>
	</div>
	<!----push to delivery overlay----->
<div class="large-12 small-12 medium-12 columns" style="position:absolute;">
		<div class="overlay" id="p2ddeliveryoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
			<div class="row sectiontoppadding">&nbsp;</div>
			<div class="large-4 medium-4 small-12 large-centered medium-centered small-centered columns ">
				<div class="large-12 column paddingzero">
					<div class="row">&nbsp;</div>
					<form id="p2ddeliveryform" name="p2ddeliveryform" class="cleardataform">
					<div id="p2ddeliveryformvalidate" class="validationEngineContainer">
					<div class="large-12 columns end paddingbtm ">
						<div class="large-12 columns cleardataform z-depth-5 border-modal-8px borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff">
							<div class="large-12 columns sectionheaderformcaptionstyle">Push To Delivery</div>
							<div class="large-12 columns sectionpanel" style="padding: 15px 15px;">
							<div class="static-field large-12 columns">
									<label>Process<span class="mandatoryfildclass">*</span></label>
									<select id="process" name="process" class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="104">
									  <option value=""></option>
									  <option value="1">Push To Delivery</option>
									  <option value="2">Push To Pending</option>
									</select>
							</div>
							<div class="static-field large-12 columns">
									<label>Pending Delivery Items<span class="mandatoryfildclass">*</span></label>
									<select id="pendingitemdetail" name="pendingitemdetail" class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="104">
									  <option value=""></option>
									  </select>
							</div>
							<div class="static-field large-6 columns deliverystatushidediv">
									<label>Delivery Status<span class="mandatoryfildclass">*</span></label>
									<select id="deliverystatusid" name="deliverystatusid" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="104">
									  <option value=""></option>
										<?php foreach($deliverystatus->result() as $key):?>
										<option value="<?php echo $key->status;?>">
										<?php echo $key->statusname;?></option>
										<?php endforeach;?>		
									</select>
							</div>
							<div class="input-field large-12 columns deliverystatushidediv" style="max-height: 200px !important;">
									<textarea name="deliverycomment" class="materialize-textarea" id="deliverycomment" rows="3" cols="40"></textarea>
									<label for="deliverycomment">Delivery Comments<span class="mandatoryfildclass">*</span></label>
							</div>
							</div>
							<div class="divider large-12 columns"></div>
							<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">
								<input type="button" id="addp2deliverybtn" name="" tabindex="106" value="Submit" class="alertbtnyes" >
								<input type="button" id="closep2ddeliveryoverlay" name="" value="Cancel" tabindex="107" class="flloop alertbtnno alertsoverlaybtn" >
							</div>
							</div>
							</div>
						</div>
						</div>
					</form>
					
				</div>
			</div>		
		</div>
	</div>
	<!----cancel order----->
	<div class="large-12 small-12 medium-12 columns" style="position:absolute;">
		<div class="overlay" id="cancelorderoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
			<div class="row sectiontoppadding">&nbsp;</div>
			<div class="large-4 medium-4 small-12 large-centered medium-centered small-centered columns ">
				<div class="large-12 column paddingzero">
					<div class="row">&nbsp;</div>
					<form id="cancelorderform" name="cancelorderform" class="cleardataform">
					<div id="cancelorderformvalidate" class="validationEngineContainer">
					<div class="large-12 columns end paddingbtm ">
						<div class="large-12 columns cleardataform z-depth-5 border-modal-8px borderstyle" style="padding-left: 0 !important; padding-right: 0 !important; border-radius: 6px !important; background: #ffffff">
							<div class="large-12 columns sectionheaderformcaptionstyle">Cancel Order</div>
							<div class="large-12 columns sectionpanel" style="padding: 15px 15px;">
								<div class="input-field large-12 columns">
									<input type="text" id="cancelordernumber" name="cancelordernumber" value="" class="" tabindex="103" readonly />
									<label for="cancelordernumber">Order Number</label>
								</div>
								<div class="static-field large-12 columns">
									<label>Cancel Order Items<span class="mandatoryfildclass">*</span></label>
									<select id="cancelitemdetail" name="cancelitemdetail" class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="104">
										<option value=""></option>
									</select>
								</div>
								<div class="input-field large-12 columns">
									<input type="text" id="currentorderstatus" name="currentorderstatus" value="" class="" tabindex="103" readonly />
									<label for="currentorderstatus">Current Status</label>
								</div>
								<div class="input-field large-12 columns" style="max-height: 200px !important;">
									<textarea name="cancelcomment" class="materialize-textarea validate[maxSize[200]] forsucesscls" id="cancelcomment" type="text"></textarea>
									<label for="cancelcomment">Comments</label>
								</div>
							</div>
							<div class="divider large-12 columns"></div>
								<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">
									<input type="button" id="cancelorderbtn" name="" tabindex="106" value="Submit" class="alertbtnyes" >
									<input type="button" id="closecancelorderoverlay" name="" value="Cancel" tabindex="107" class="flloop alertbtnno alertsoverlaybtn" >
								</div>
						</div>
					</div>
					</div>
					</form>
				</div>
			</div>		
		</div>
	</div>
	<!--Place Order Status icon overlay -->
	<div class="large-12 small-12 medium-12 columns" style="position:absolute;">
		<div class="overlay" id="postatusoverlayform" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
			<div class="row sectiontoppadding">&nbsp;</div>
			<div class="large-4 medium-4 small-12 large-centered medium-centered small-centered columns ">
				<div class="large-12 column paddingzero">
					<div class="row">&nbsp;</div>
					<form id="postatusoverlayform" name="postatusoverlayform" class="postatuscleardataform">
					<div id="postatusformvalidate" class="validationEngineContainer">
					<div class="large-12 columns end paddingbtm ">
						<div class="large-12 columns cleardataform z-depth-5 border-modal-8px" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff">
							<div class="large-12 columns sectionheaderformcaptionstyle">Place Order Status Overlay</div>
							<div class="large-12 columns sectionpanel" style="padding: 15px 15px;">
								<div class="static-field large-12 columns">
									<label>Place Order Status<span class="mandatoryfildclass">*</span></label>
									<select data-validation-engine="" class="chzn-select" id="placeorderstatusacc" name="placeorderstatusacc" data-prompt-position="topLeft:14,36">
										<option value="0" data-placeorderstatusacc="Accepted">Accepted</option>
										<option value="1" data-placeorderstatusacc="Rejected">Rejected</option>
										<option value="2" data-placeorderstatusacc="Customer Review">Customer Review</option>
									</select>
								</div>
								<div class="input-field large-12 columns">
									<textarea name="postatuscomment" class="materialize-textarea validate[maxSize[200]]" id="postatuscomment" rows="3" cols="40"></textarea>
									<label for="postatuscomment">Comments</label>
								</div>
							</div>
							<div class="divider large-12 columns"></div>
								<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">
									<input type="button" id="postatusoverlaysave" name="" tabindex="106" value="Submit" class="alertbtn" >
									<input type="button" id="postatusoverlaycancel" name="" value="Cancel" tabindex="107" class="flloop alertbtn alertsoverlaybtn" >
								</div>
							</div>
						</div>
						</div>
					</form>
					
				</div>
			</div>		
		</div>
	</div>
	<!--Delivery Place Order items via 3 Modes-->
	<div class="large-12 small-12 medium-12 columns" style="position:absolute;">
		<div class="overlay" id="deliveryplaceordoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
			<div class="row sectiontoppadding">&nbsp;</div>
			<div class="large-4 medium-4 small-12 large-centered medium-centered small-centered columns">
				<div class="large-12 column paddingzero">
					<div class="row">&nbsp;</div>
					<form id="deliveryplaceordform" name="deliveryplaceordform" class="cleardatadeliveryplaceordform">
					<div id="deliveryplaceordvalidate" class="validationEngineContainer">
					<div class="large-12 columns end paddingbtm ">
						<div class="large-12 columns cleardataform z-depth-5 border-modal-8px borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff;    height: 550px !important;">
							<div class="large-12 columns sectionheaderformcaptionstyle">Deliver Place Order Items</div>
							<div class="large-12 columns sectionpanel" style="padding: 15px 15px;">
								<div class="static-field large-6 columns">
									<label>Delivery Date<span class="mandatoryfildclass">*</span></label>					
									<input id="deliverypodate" class="fordatepicicon validate[required]" type="text" data-dateformater="dd-mm-yy" data-prompt-position="topLeft" readonly="readonly" tabindex="" name="deliverypodate">
								</div>
								<div class="static-field large-6 columns">
									<label>Delivery Option Mode<span class="mandatoryfildclass">*</span></label>
									<select data-validation-engine="" class="chzn-select" id="deliverypomodeid" name="deliverypomodeid" data-prompt-position="topLeft:14,36">
										<option value=""></option>
										<?php foreach($deliverypomode as $key):?>
										<option value="<?php echo $key->deliverypomodeid;?>" data-deliverypomodename="<?php echo $key->deliverypomodename;?>"><?php echo $key->deliverypomodename;?></option>
										<?php endforeach;?>	
									</select>
								</div>
								<div class="input-field large-12 columns delvendornamediv">
									<input type="text" class="validate[maxSize[100]]" id="delvendorname" name="delvendorname" value="" tabindex="" />
									<label for="delvendorname">Vendor Employee Name<span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="static-field large-12 columns delemployeediv">
									<label>Delivery Employee<span class="mandatoryfildclass">*</span></label>
									<select data-validation-engine="" class="chzn-select" id="delemployeeid" name="delemployeeid" data-prompt-position="topLeft:14,36">
										<option value=""></option>
										<?php foreach($delvemployee as $key):?>
										<option value="<?php echo $key->employeeid;?>" data-delemployeename="<?php echo $key->employeename;?>"><?php echo $key->employeename;?></option>
										<?php endforeach;?>	
									</select>
								</div>
								<div class="input-field large-12 columns delcouriernamediv">
									<input type="text" class="validate[maxSize[100]]" id="delcouriername" name="delcouriername" value="" tabindex="" />
									<label for="delcouriername">Courier Name/Docket Number<span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="input-field large-12 columns delotpdiv">
									<input type="text" class="validate[maxSize[200]]" id="delotp" name="delotp" value="" tabindex="" />
									<label for="delotp">OTP<span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="input-field large-12 columns">
									<textarea name="deliverypocomment" class="materialize-textarea validate[required,maxSize[200]]" id="deliverypocomment" rows="3" cols="40"></textarea>
									<label for="deliverypocomment">Delivery Comments</label>
								</div>
								<div class="static-field large-12 columns delvyorderstatusaccdiv">
									<label>Delivery Order Status<span class="mandatoryfildclass">*</span></label>
									<select data-validation-engine="" class="chzn-select" id="delvyorderstatusacc" name="delvyorderstatusacc" data-prompt-position="topLeft:14,36">
										<option value="0" data-delvyorderstatusacc="Accepted">Accepted</option>
										<option value="1" data-delvyorderstatusacc="Rejected">Rejected</option>
									</select>
								</div>
								
								<div class="large-12 columns sectionalertbuttonarea" style="position:relative;top:60px;text-align:right;">
									<input type="button" id="deliveryplaceordsave" name="" tabindex="106" value="Submit" class="alertbtnyes" >
									<input type="button" id="deliveryplaceordcancel" name="" value="Cancel" tabindex="107" class="flloop alertbtnno alertsoverlaybtn" >
								</div>
							</div>
							
							</div>
						</div>
						</div>
					</form>
				</div>
			</div>		
		</div>
	</div>
	<!-- Re-Assigning new vendor with new due date of vendor (already rejected by vendor) -->
	<div class="large-12 small-12 medium-12 columns" style="position:absolute;">
		<div class="overlay" id="reassigningvendoroverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
			<div class="row sectiontoppadding">&nbsp;</div>
			<div class="large-4 medium-4 small-12 large-centered medium-centered small-centered columns">
				<div class="large-12 column paddingzero">
					<div class="row">&nbsp;</div>
					<form id="reassigningvendorform" name="reassigningvendorform" class="">
					<div id="reassigningvendorformvalidate" class="validationEngineContainer">
					<div class="large-12 columns end paddingbtm ">
						<div class="large-12 columns cleardataform z-depth-5 border-modal-8px borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff;    height: 550px !important;">
							<div class="large-12 columns sectionheaderformcaptionstyle">Assign New Vendor</div>
							<div class="large-12 columns sectionpanel" style="padding: 15px 15px;">
								<div class="static-field large-12 columns">
									<label>Product Information<span class="mandatoryfildclass">*</span></label>
									<select data-validation-engine="" class="chzn-select validate[required]" id="salesdetailproductid" name="salesdetailproductid" data-prompt-position="topLeft:14,36">
										<option value=""></option>
									</select>
								</div>
								<div class="static-field large-12 columns">
									<label>Vendor<span class="mandatoryfildclass">*</span></label>
									<select data-validation-engine="" class="chzn-select validate[required]" id="newvendoraccountid" name="newvendoraccountid" data-prompt-position="topLeft:14,36">
										<?php 
											foreach($vendoraccount as $key):
										?>												
										<option value="<?php echo $key->accountid;?>" data-vendoraccount="<?php echo $key->accountname;?>" ><?php echo $key->accountname;?></option>
										<?php  endforeach;?>	
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label>Delivery Date<span class="mandatoryfildclass">*</span></label>					
									<input id="newvendorduedate" class="fordatepicicon validate[required]" type="text" data-dateformater="dd-mm-yy" data-prompt-position="topLeft" readonly="readonly" tabindex="" name="newvendorduedate">
								</div>
								<div class="input-field large-4 small-4  columns" id="imageitem-div">
										<span style="color:black;">Image<span class="innerfrmicon" id="imageitemicon" style="position:relative;top:3px;left:5px;cursor:pointer;margin-top:-5px;" tabindex="383"><i class="material-icons">radio_button_checked</i></span></span>
								</div>
								<input type="hidden" id="transactiontypepage" name="transactiontypepage" value="">
								<div class="large-12 columns sectionalertbuttonarea" style="position:relative;top:60px;text-align:right;">
									<input type="button" id="reassigningvendorsave" name="" tabindex="106" value="Submit" class="alertbtnyes" >
									<input type="button" id="reassigningvendorcancel" name="" value="Cancel" tabindex="107" class="flloop alertbtnno alertsoverlaybtn" >
								</div>
							</div>
							</div>
						</div>
						</div>
					</form>
				</div>
			</div>		
		</div>
	</div>
	<!-- Credit Number Generation Overlay -->
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay alertsoverlay overlayalerts" id="creditnogenerationoverlay">
			<div class="row sectiontoppadding">&nbsp;</div>
			<div class="overlaybackground">
				<div class="row">&nbsp;</div>
				<div class="alert-panel">
					<?php if($mainlicense['oldpurchasecredittype'] == 1) { ?>
						<div class="static-field large-6 columns oldpurchasecredittypecls">
							<label>Old Purchase Type<span class="mandatoryfildclass">*</span></label>
							<select data-validation-engine="" class="chzn-select" id="oldpurchasecredittypeid" name="oldpurchasecredittypeid" data-prompt-position="topLeft:14,36">
								<option value="1" data-oldpurchasecredittype="Old">Old</option>
								<option value="2" data-oldpurchasecredittype="Credit">Credit</option>
							</select>
						</div>
						<div class="static-field large-6 columns oldpurchasecredittypecls">
							<label>Balance Amount<span class="mandatoryfildclass">*</span></label>					
							<input id="balanceamountcredit" type="text" data-prompt-position="topLeft" readonly="readonly" tabindex="" name="balanceamountcredit">
						</div>
					<?php } ?>
					<div class="row">&nbsp;</div>
					<div class="alertmessagearea">
						<div class="row">&nbsp;</div>
						<div class="alert-message basedelalerttxt">Delete this data?</div>
					</div>
					<div class="alertbuttonarea">
						<span class="firsttab" tabindex="1000"></span>
						<input type="button" id="" name="" value="Delete" tabindex="1001" class="alertbtnyes ffield commodyescls">
						<input type="button" id="basedeleteno" name="" tabindex="1002" value="Cancel" class="alertbtnno flloop  alertsoverlaybtn" >
						<span class="lasttab" tabindex="1003"></span>
					</div>
					<div class="row">&nbsp;</div>
				</div>
			</div>
		</div>
	</div>
</body>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js File -->
	<?php include 'js/Sales/sales.php' ?>
	<script src="<?php echo base_url();?>js/plugins/filecomputer/fileupload.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/say-cheese.js"></script>
	<script src="<?php echo base_url();?>js/plugins/treemenu/modernizr.custom.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/treemenu/jquery.dlmenu.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/autocomplete/jquery-ui.js" type="text/javascript"></script>
	<link rel="stylesheet" href="<?php echo base_url();?>css/jquery-ui.css">
</html>