<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Model File
class Salesmodel extends CI_Model
{    
   	public function __construct()
    {		
		parent::__construct(); 
		$this->load->model( 'Base/Crudmodel');
		$this->load->model('Printtemplates/Printtemplatesmodel');
    }
	public $salesmoduleid = '52';
  	//account dd load function
	public function accountdd() {
		$industryid = $this->Basefunctions->industryid;
		$info =$this->db->query("SELECT `account`.`accountname`, `account`.`accountid`,`account`.`accountcatalogid`,`account`.`accounttypeid` FROM `account`  WHERE `account`.`status` = 1 and `account`.`accountid` not in (2,3) ORDER BY `account`.`accountid` DESC");
		return $info;
	}
	//simple drop down value fetch with condition
	public function paymentaccountnamefetch($did,$dname,$table,$whfield,$whdata) {
		$i=0;
		$industryid = $this->Basefunctions->industryid;
		$mvalu=explode(",",$whdata);
		$this->db->select("$dname,$did");
		$this->db->from($table);
		if($whdata != '') {
			if(count($mvalu)>=2) {
				$this->db->where_in($whfield,$mvalu);
			} else {
				$this->db->where("FIND_IN_SET('$whdata',$whfield) >", 0);
			}
		}
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->where('status',1);
		$this->db->where_not_in('accountname','');
		$this->db->order_by($dname,"asc");
		$result = $this->db->get();
		return $result->result();
    }
	/* Sales/Purchase calculation type fetch */
	public function getcalculationtype($transactiontype,$salesmode) {
		$data = '2';
		$this->db->select('transactionmodeid');
		$this->db->from('transactionmanage');
		$this->db->where('transactionmanage.salestransactiontypeid',$transactiontype);
		$this->db->where('transactionmanage.salesmodeid',$salesmode);
		$this->db->where('transactionmanage.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = $row->transactionmodeid;
			}
		} 
		return $data;
	}
	/**
	* salestransactiontypedropdown
	*/
	public function salestransactiontypedropdown()
	{
		$planid = $this->Basefunctions->planid;
		$userroleid = $this->Basefunctions->userroleid;
		$industryid = $this->Basefunctions->industryid;
		$data = $this->db->select('salestransactiontype.salestransactiontypeid,salestransactiontype.salestransactiontypename,salestransactiontype.salestransactiontypelabel,salestransactiontype.summarylabel,salestransactiontype.stocktyperelationid')
					->from('salestransactiontype')
					->where("FIND_IN_SET('$this->salesmoduleid',salestransactiontype.moduleid) >", 0)
					->where("FIND_IN_SET('$userroleid',salestransactiontype.userroleid) >", 0)
					->where("FIND_IN_SET('$planid',salestransactiontype.planid) >", 0)
					->where("FIND_IN_SET('$industryid',salestransactiontype.industryid) >", 0)
					->where('salestransactiontype.status',$this->Basefunctions->activestatus)
					->where('a.status',$this->Basefunctions->activestatus)
					->get();
		return $data;
	}
	// set transaction based on company setting
	public function salestransactiontypedropdown_set()
	{
		$planid = $this->Basefunctions->planid;
		$userroleid = $this->Basefunctions->userroleid;
		$allowedaccounttypeset = $this->Basefunctions->get_company_settings('allowedaccounttypeset');
		if($allowedaccounttypeset == 0) {
			$data=$this->db->query('SELECT `salestransactiontype`.`salestransactiontypeid`, `salestransactiontype`.`salestransactiontypename`, `salestransactiontype`.`salestransactiontypelabel`, `salestransactiontype`.`summarylabel`, `salestransactiontype`.`stocktyperelationid`, `salestransactiontype`.`headerdetail`, group_concat(`transactionmanage`.`stocktypeid`) as stocktypeid, `transactionmanage`.`status`,transactionmanage.transactionmodeid,transactionmanage.additionalchargeid,transactionmanage.pureadditionalchargeid,group_concat(transactionmanage.salesmodeid) as salesmodeid,transactionmanage.roundingtype,transactionmanage.roundoffremove,transactionmanage.autotax,transactionmanage.discountmodeid,transactionmanage.discountcalctypeid,transactionmanage.discountdisplayid,transactionmanage.taxtypeid,`printtemplates`.`printtemplatesid`,transactionmanage.chargesedit,transactionmanage.allowedaccounttypeid,transactionmanage.serialnumbermastername,transactionmanage.transactionmanageid,transactionmanage.metalarraydetails,transactionmanage.metalserialnumberdetails,transactionmanage.metalprinttempids,transactionmanage.metaloldarraydetails,transactionmanage.metaloldserialnumberdetails,transactionmanage.metalreturnarraydetails,transactionmanage.metalreturnserialnumberdetails
			FROM (`salestransactiontype`) 
			LEFT JOIN `printtemplates` ON `printtemplates`.`salestransactiontypeid` = `salestransactiontype`.`salestransactiontypeid` and `printtemplates`.`status` =1 
			LEFT JOIN `transactionmanage` ON `transactionmanage`.`salestransactiontypeid` = `salestransactiontype`.`salestransactiontypeid` and `transactionmanage`.`status` = 1 WHERE FIND_IN_SET(52,salestransactiontype.moduleid) > 0 AND FIND_IN_SET('.$userroleid.',salestransactiontype.userroleid) > 0 AND FIND_IN_SET(3,salestransactiontype.industryid) > 0 AND FIND_IN_SET(4,salestransactiontype.planid) > 0 AND `salestransactiontype`.`status` = 1 GROUP BY `salestransactiontype`.`salestransactiontypeid`');
		} else {
			$data=$this->db->query('SELECT `salestransactiontype`.`salestransactiontypeid`, `salestransactiontype`.`salestransactiontypename`, `salestransactiontype`.`salestransactiontypelabel`, `salestransactiontype`.`summarylabel`,`salestransactiontype`.`headerdetail`, group_concat(`transactionmanage`.`stocktypeid`) as stocktypeid, `transactionmanage`.`status`,transactionmanage.transactionmodeid,transactionmanage.additionalchargeid,transactionmanage.pureadditionalchargeid,group_concat(transactionmanage.salesmodeid) as salesmodeid,transactionmanage.roundingtype,transactionmanage.roundoffremove,transactionmanage.autotax,transactionmanage.discountmodeid,transactionmanage.discountcalctypeid,transactionmanage.discountdisplayid,transactionmanage.taxtypeid,transactionmanage.chargesedit,transactionmanage.allowedaccounttypeid,transactionmanage.allprinttemplateid as printtemplatesid,transactionmanage.allstocktypeid as stocktyperelationid,transactionmanage.allpaymentstocktypeid,transactionmanage.serialnumbermastername,transactionmanage.transactionmanageid,transactionmanage.metalarraydetails,transactionmanage.metalserialnumberdetails,transactionmanage.metalprinttempids,transactionmanage.metaloldarraydetails,transactionmanage.metaloldserialnumberdetails,transactionmanage.metalreturnarraydetails,transactionmanage.metalreturnserialnumberdetails
			FROM (`salestransactiontype`) 

			LEFT JOIN `transactionmanage` ON `transactionmanage`.`salestransactiontypeid` = `salestransactiontype`.`salestransactiontypeid` and `transactionmanage`.`status` = 1 and transactionmanage.defaultforttype = 1
			WHERE FIND_IN_SET(52,salestransactiontype.moduleid) > 0 AND FIND_IN_SET('.$userroleid.',salestransactiontype.userroleid) > 0 AND FIND_IN_SET(3,salestransactiontype.industryid) > 0 AND FIND_IN_SET(4,salestransactiontype.planid) > 0 AND `salestransactiontype`.`status` = 1 GROUP BY `salestransactiontype`.`salestransactiontypeid`');
		}
		return $data;
	}
	public function transactionmanagedd() {
		$data=$this->db->query('SELECT * FROM (`transactionmanage`)  WHERE  `transactionmanage`.`status` = 1');
		return $data;
	}
	/** *stocktypedropdown */
	public function stocktypedropdown() {
		$planid = $this->Basefunctions->planid;
		$userroleid = $this->Basefunctions->userroleid;
		$industryid = $this->Basefunctions->industryid;
		$data = $this->db->select('stocktype.stocktypeid,stocktype.stocktypename,stocktype.stocktypelabel,stocktype.shortname')
					->from('stocktype')
					->where("FIND_IN_SET('$this->salesmoduleid',stocktype.moduleid) >", 0)
					->where("FIND_IN_SET('$userroleid',stocktype.userroleid) >", 0)
					->where("FIND_IN_SET('$planid',stocktype.planid) >", 0)
					->where("FIND_IN_SET('$industryid',stocktype.industryid) >", 0)
					->where('stocktype.status',$this->Basefunctions->activestatus)
					->order_by('stocktype.stocktypeid','asc')
					->get();
		return $data;
	}
	/** *payment -type dropdown */
	public function paymenttypedropdown() {
		$planid = $this->Basefunctions->planid;
		$userroleid = $this->Basefunctions->userroleid;	
		$data = $this->db->select('stocktype.stocktypeid,stocktype.stocktypename,stocktype.stocktypelabel,stocktype.shortname')
					->from('stocktype')
					->where("FIND_IN_SET('$this->salesmoduleid',stocktype.moduleid) >", 0)
					->where("FIND_IN_SET('$userroleid',stocktype.userroleid) >", 0)
					->where("FIND_IN_SET('$planid',stocktype.planid) >", 0)
					->where('stocktype.status',$this->Basefunctions->activestatus)
					->order_by('stocktype.stocktypeid','asc')

					->get();
		return $data;
	}
	/** *retrieve tag details vishal */
	public function retrievetagdata() {
		$tagnumber = $_GET['tagnumber'];
		$stocktypeid = $_GET['stocktypeid'];
		$accountypeid = $_GET['accounttype_id'];
		$transactiontype = $_GET['transactiontype'];
		$product = 1;
		$purity = 1;
		$weight = 0;
		if(isset($_GET['accountid'])) {
			if($_GET['accountid'] == '') {
				$accountgroupid=1;
			} else {
				$accountgroupid=$this->db->select('accountgroupid')->where('accountid',$_GET['accountid'])->get('account')->row()->accountgroupid;
			}
		}
		$amountround =  $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$this->db->select(
						'itemtag.itemtagid,itemtag.itemtagnumber,DATE_FORMAT(tagdate,"%d-%m-%Y") AS tagdate,itemtag.lotid,	 itemtag.purityid,itemtag.accountid,itemtag.fromcounterid,itemtag.counterid,itemtag.tagtypeid,itemtag.productid,
						ROUND(itemtag.grossweight,'.$round.') as grossweight,
						ROUND(itemtag.stoneweight,'.$round.') as stoneweight,
						ROUND(itemtag.netweight,'.$round.') as netweight,
						itemtag.pieces,itemtag.size,itemtag.tagimage						,itemtag.status,itemtag.salesdetailid,itemtag.salesdetailid,itemtag.transfertagno,itemtag.tagentrytypeid,itemtag.rfidtagno,ROUND(itemtag.itemrate,'.$amountround.') as itemrate,itemtag.orderstatus',false);
		$this->db->from('itemtag');
		if(!empty($_GET['tagnumber'])) {
			$this->db->where('itemtag.itemtagnumber',$tagnumber);
			$this->db->where_not_in('itemtag.status',array(0,3));
		} else if(!empty($_GET['rfidtagnumber'])) {
			$this->db->where('itemtag.rfidtagno',$_GET['rfidtagnumber']);
			$this->db->where_not_in('itemtag.status',array(0,3));
		}
		
		$this->db->limit(1);
		$info = $this->db->get();
		if($info->num_rows() == 1) {
			$infos = $info->row();
			if($stocktypeid == 62 && $infos->status == 15) {
				$tagjsonarray['stock'] = array('output'=>'APPROVAL');
			} else if($infos->status == 1 || $infos->status == 15) {
				$countofstone = $this->stonecountbasedonitemtag($infos->itemtagid);
				$product = $infos->productid;
				$purity = $infos->purityid;
				$weight = $infos->grossweight;
				if($infos->tagimage != '' || $infos->tagimage != NULL) {
					$tagimage = $infos->tagimage;
				} else {
					$tagimage = '';
				}
				$tagjsonarray['stock'] = array(
					'itemtagid' => $infos->itemtagid,
					'tagtypeid' => $infos->tagtypeid,
					'itemtagnumber' => $infos->itemtagnumber,
					'purity'=> $infos->purityid,								
					'product'=> $infos->productid,
					'grossweight' => $infos->grossweight,
					'stoneweight' => $infos->stoneweight,
					'netweight' => $infos->netweight,
					'pieces' => $infos->pieces,								
					'counter' => $infos->counterid,
					'size' => $infos->size,
					'salesdetailid'=>$infos->salesdetailid,
					'transfertagno'=>$infos->transfertagno,
					'tagentrytypeid'=>$infos->tagentrytypeid,
					'rfidtagnumber'=>$infos->rfidtagno,
					'output'=>'UNSOLD',
					'itemrate'=>$infos->itemrate,
					'stonecount'=>$countofstone,
					'orderstatus'=>$infos->orderstatus,
					'tagimage'=>$tagimage,
					'status'=>$infos->status
				);	
			}
			else if($infos->status == 12) {
				$tagjsonarray['stock'] = array('output'=>'SOLD');
			} else if($infos->status == 17 || $infos->status == 18 || $infos->status == 19) {
				$tagjsonarray['stock'] = array('output'=>'DELIVERY');
			} else {
				$tagjsonarray['stock'] = array('output'=>'NO');
			}
		} else {
			$tagjsonarray['stock'] = array('output'=>'NO');
		}
		{// transactiontype based settings
			$salestransactiontypeid = $transactiontype;
			if($salestransactiontypeid == 9){
				$chargedataid = 'purchasechargeid';
				$salestransactiontypeid = 9;
				$accountypeid = 16;
			}else if($salestransactiontypeid == 18){
				if($accountypeid == 6){
					$chargedataid = 'chargeid';
					$salestransactiontypeid = 1;
				}else if($accountypeid == 16){
					$chargedataid = 'purchasechargeid';
					$salestransactiontypeid = 11;
				}
			}else{
				$chargedataid = 'chargeid';
				$salestransactiontypeid =1;
			}
		}
		$stockcharges = $this->Salesmodel->loadproductaddonfromstock($_GET['tagnumber'],$_GET['rfidtagnumber']);
		if($stockcharges != 'no') {	
		  $charge_array = $stockcharges;	
		}else{
			$category = '';
			$charge_array = $this->Salesmodel->loadproductcharges($category,$product,$purity,$weight,$accountgroupid,$accountypeid,$transactiontype);					
		}
		$tagjsonarray['charge'] = $charge_array;
		echo json_encode($tagjsonarray);
	}
	// get stone count based on the itemtag
	public function stonecountbasedonitemtag($itemtagid) {
		$count = 0;
		$this->db->select('stoneentryid');
		$this->db->from('stoneentry');
		$this->db->join('itemtag','itemtag.itemtagid=stoneentry.itemtagid');
		$this->db->where('stoneentry.itemtagid',$itemtagid);
		$this->db->where('stoneentry.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			$count = 1;
		} else {
			$count = 0;
		}
		return $count;
	}
	/**
	*loadproductaddon-other
	*/
	public function loadproductaddon($product,$purity,$weight)
	{	
		$this->db->select('productcharge.additionalchargeid,productcharge.accountgroupid,calculationby.calculationbyname,additionalcharge.additionalchargename,productcharge.calculationbyid,ROUND(productcharge.chargeamount,2) as value
		',false);
        $this->db->from('productcharge');
		$this->db->join('additionalcharge','additionalcharge.additionalchargeid=productcharge.additionalchargeid');
		$this->db->join('calculationby','calculationby.calculationbyid=productcharge.calculationbyid');
		$this->db->where('productcharge.productid',$product);
		$this->db->where('productcharge.purityid',$purity);
		$this->db->where('startweight <=',$weight);
		$this->db->where('endweight >=',$weight);
		$this->db->where('productcharge.status',$this->Basefunctions->activestatus);
		$this->db->order_by('productcharge.productchargeid','desc');
		$this->db->group_by('productcharge.additionalchargeid');
        $productcharge=$this->db->get();
		return $productcharge;
	}
	// load product addon for sales module
	public function loadproductaddonaccount($product,$purity,$weight,$accountgroupid,$accountypeid,$salestransactiontypeid) {
		$this->db->select('chargedetails,chargeid,chargewastageweight',false);
		$this->db->from('productcharge');
		$this->db->where('productcharge.productid',$product);
		$this->db->where_in('productcharge.accountgroupid',$accountgroupid);
		$this->db->where('productcharge.accounttypeid',$accountypeid);
		$this->db->where('productcharge.salestransactiontypeid',$salestransactiontypeid);
		$this->db->where('productcharge.purityid',$purity);
		$this->db->where('startweight <=',$weight);
		$this->db->where('endweight >=',$weight);
		$this->db->where('productcharge.status',$this->Basefunctions->activestatus);
		$this->db->order_by('productcharge.productchargeid','desc');
		$this->db->limit(1);
		$productchargearray=$this->db->get();
		return $productchargearray;
	}
	// load product addon for sales module
	public function loadproductaddonfromstock($tagnumber,$rfidnumber) {
		$this->db->select('chargespandetails,chargeid',false);
		$this->db->from('itemtag');
		if($tagnumber != ''){
			$this->db->where("itemtag.itemtagnumber",$tagnumber);
		}else{
			$this->db->where("itemtag.rfidtagno",$rfidnumber);
			$this->db->where("itemtag.rfidtagno !=",'');
		}
		$this->db->where("itemtag.chargeapply",1);
		$this->db->where("itemtag.status",1);
		$this->db->limit(1);
		$stockcharges=$this->db->get();
		if(($stockcharges->num_rows() > 0 && $stockcharges != '') ) {
		foreach($stockcharges->result() as $infostock) { 
				  // check account group is all or not
				  $charge_array= array(
										'chargedetails'=>$infostock->chargespandetails,
										'chargeid'=>$infostock->chargeid,
										'chargewastageweight'=>0
									);
				}	
		   
		 }else{
			 $charge_array = 'no';
		 }
		return $charge_array;
	}
	// load product addon for sales module
	public function loadproductaddonfromstockuntag($product,$purity,$weight) {
		$this->db->select('chargespandetails,chargeid',false);
		$this->db->from('itemtag');
		$this->db->where('itemtag.purityid',$purity);
		$this->db->where('itemtag.productid',$product);
		$this->db->where('itemtag.netweight <=',$weight);
		$this->db->where('itemtag.netweight >=',$weight);
		$this->db->where('itemtag.tagtypeid',3);
		$this->db->where("itemtag.chargedetails !=",'');
		$this->db->where("itemtag.chargeid !=",'');
		$this->db->where("itemtag.status",1);
		$this->db->order_by('itemtag.itemtagid',"DESC");
		$this->db->limit(1);
		$productchargearray=$this->db->get();
		return $productchargearray;
	}
	public function loadproductcharges($category,$product,$purity,$weight,$accountgroupid,$accountypeid,$salestransactiontypeid) {
		{// transactiontype based settings
			if($salestransactiontypeid == 9 || $salestransactiontypeid == 27){
				$chargedataid = 'purchasechargeid';
				$salestransactiontypeidtwo = 9;
				$accountypeid = 16;
			}else if($salestransactiontypeid == 18){
				if($accountypeid == 6){
					$chargedataid = 'chargeid';
					$salestransactiontypeidtwo = 1;
				}else if($accountypeid == 16){
					$chargedataid = 'purchasechargeid';
					$salestransactiontypeidtwo = 11;
				}
			}else{
				$chargedataid = 'chargeid';
				$salestransactiontypeidtwo = 1;
			}
		}
		if($category > 1){
			$cloneproductid = $this->Basefunctions->singlefieldfetch('parentproductid','categoryid','category',$category);
			$product = $cloneproductid;
		}else {
			$cloneproductid = $this->Basefunctions->singlefieldfetch('parentproductid','productid','product',$product);
			if($cloneproductid != 1){
			   $product = $cloneproductid;
			}
		}
		$resultin = $this->Salesmodel->loadproductaddonaccount($product,$purity,$weight,$accountgroupid,$accountypeid,$salestransactiontypeidtwo);
			if($resultin->num_rows() > 0) { 
				foreach($resultin->result() as $info) { 
				  // check account group is all or not
				  $charge_array= array(
										'chargedetails'=>$info->chargedetails,
										'chargeid'=>$info->chargeid,
										'chargewastageweight'=>$info->chargewastageweight
									);
				}					
			}else {
				$resultall = $this->Salesmodel->loadproductaddonaccount($product,$purity,$weight,1,$accountypeid,$salestransactiontypeidtwo);
				if($resultall->num_rows() > 0) { 	
					foreach($resultall->result() as $allinfo) { 
					  // check account group is all 
					  $charge_array= array(
											'chargedetails'=>$allinfo->chargedetails,
											'chargeid'=>$allinfo->chargeid,
											'chargewastageweight'=>$allinfo->chargewastageweight
										);
					 }	
				} else {
					if($salestransactiontypeid == 9 || $salestransactiontypeid == 27){
						$result_charge = $this->Salesmodel->loadpurchaseproductaddoncharge($product);
					}else if($salestransactiontypeid == 18){
						if($accountypeid == 6){
							$result_charge = $this->Salesmodel->loadproductaddoncharge($product);
						}else if($accountypeid == 16){
							$result_charge = $this->Salesmodel->loadpurchaseproductaddoncharge($product);
						}
					}			
					else{
						$result_charge = $this->Salesmodel->loadproductaddoncharge($product);
					}
					foreach($result_charge->result() as $info_charge) {
							if($info_charge->chargekeyword == ''){
								$charge_keyword = '';
							}else{
								$charge_keyword = $info_charge->chargekeyword;
							}
							$charge_array= array(
									'chargedetails'=>$charge_keyword,
									'chargeid'=>'',
									'chargewastageweight'=>''
								);
						}
			}
		 }
		 return $charge_array;
	}
	/*create account*/
	function accountcreate() {
		$accountnumber = $this->Basefunctions->randomnumbergenerator('91','account','accountnumber','2875');
		$accountcatalogid = $this->Basefunctions->singlefieldfetch('accountcatalogid','accounttypeid','accounttype',$_POST['accountype']); //Account catelog using accounttype
		if(isset($_POST['accountprimarycountry'])) {
			$_POST['accountprimarycountry'] = $_POST['accountprimarycountry'];
		} else {
			$_POST['accountprimarycountry'] = '2';
		}
		if(isset($_POST['accountgroupid'])) {
			$_POST['accountgroupid'] = $_POST['accountgroupid'];
		} else {
			$_POST['accountgroupid'] = '1';
		}
		$insert=array(
			'branchid'=>$this->Basefunctions->branchid,
			'salutationid'=>$_POST['salutationid'],
			'accountname'=>$_POST['newaccountname'],
			'accountshortname'=>$_POST['newaccountshortname'],
			'accountnumber'=>trim($accountnumber),
			'accounttypeid'=>$_POST['accountype'],
			'mobilenumber'=>$_POST['accountmobilenumber'],
			'additionalmobilenumber'=>$_POST['accountaddtmobilenumber'],
			'pannumber'=>$_POST['accountpannumber'],
			'landlinenumber'=>$_POST['accountlandlinenumber'],
			'accountgroupid'=>$_POST['accountgroupid'],
			'gstnnumber'=>$_POST['gstnnumber'],
			'industryid'=>$this->Basefunctions->industryid,
			'stateid'=>$_POST['accountprimarystate'],
			'countryid'=>$_POST['accountprimarycountry'],
			'accountcatalogid'=>$accountcatalogid,
			'status'=>$this->Basefunctions->activestatus
		);
		$insert = array_merge($insert,$this->Crudmodel->defaultvalueget());
	    $this->db->insert('account',array_filter($insert));
		$primaryid = $this->db->insert_id();
		$insert_area = array(
			'accountid'=>$primaryid,
			'primaryarea'=>$_POST['primaryarea'],
			'status'=>$this->Basefunctions->activestatus
		);
		$insert_area = array_merge($insert_area,$this->Crudmodel->defaultvalueget());
		$this->db->insert('accountcf',array_filter($insert_area));
		$insert_address=array(
				'addresstypeid'=>4,
				'addressmethod'=>4,
				'accountid'=>$primaryid,
				'address'=>$_POST['accountprimaryaddress'],
				'pincode'=>$_POST['accountprimarypincode'],
				'city'=>$_POST['accountprimarycity'],
				'status'=>$this->Basefunctions->activestatus
		);
		$insert_address = array_merge($insert_address,$this->Crudmodel->defaultvalueget());
		$this->db->insert('accountaddress',array_filter($insert_address));
		//audit-log
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity = ''.$user.' created Account - '.$_POST['newaccountname'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity,$userid,202);
		echo $primaryid;
	}
	/*update account*/
	function accountupdatemodel() {
		if(isset($_POST['accountprimarycountry'])) {
			$_POST['accountprimarycountry'] = $_POST['accountprimarycountry'];
		} else {
			$_POST['accountprimarycountry'] = '2';
		}
		if(isset($_POST['accountgroupid'])) {
			$_POST['accountgroupid'] = $_POST['accountgroupid'];
		} else {
			$_POST['accountgroupid'] = '1';
		}
		$update=array(
			'accountname'=>$_POST['newaccountname'],
			'accountshortname'=>$_POST['newaccountshortname'],
			'salutationid'=>$_POST['salutationid'],
			'accounttypeid'=>$_POST['accountype'],
			'accountgroupid'=>$_POST['accountgroupid'],
			'gstnnumber'=>$_POST['gstnnumber'],			
			'mobilenumber'=>$_POST['accountmobilenumber'],
			'additionalmobilenumber'=>$_POST['accountaddtmobilenumber'],
			'pannumber'=>$_POST['accountpannumber'],
			'landlinenumber'=>$_POST['accountlandlinenumber'],
			'stateid'=>$_POST['accountprimarystate'],
			'countryid'=>$_POST['accountprimarycountry'],
		);
		$update = array_merge($update,$this->Crudmodel->updatedefaultvalueget());
		$this->db->where('accountid',$_POST['newaccountid']);
	    $this->db->update('account',$update);
		$update_area=array(
			'primaryarea'=>$_POST['primaryarea']
		);
		$update_area = array_merge($update_area,$this->Crudmodel->updatedefaultvalueget());
		$this->db->where('accountid',$_POST['newaccountid']);
	    $this->db->update('accountcf',$update_area);
		$update_address=array(
			'address'=>$_POST['accountprimaryaddress'],
			'pincode'=>$_POST['accountprimarypincode'],
			'city'=>$_POST['accountprimarycity'],
		);
		$update_address = array_merge($update_address,$this->Crudmodel->updatedefaultvalueget());
		$this->db->where('addresstypeid',4);
		$this->db->where('accountid',$_POST['newaccountid']);
		$this->db->update('accountaddress',array_filter($update_address));
		echo $_POST['newaccountid'];
	}
	/*
	*account
	*/
	public function currentaccountbalance($accountid,$salesid=0,$status=0)
	{
		if($salesid != ''){
			if($status == 0) {
			  $salescond = 'AND sales.salesid < '.$salesid.'';
			}else {
			  $salescond = '';
			}
		} else {
			$salescond = '';
		}
		//$salesdate = $_GET['salesdate'];
		$weightround =  $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$amountround =  $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$openingbalance = $this->Basefunctions->singlefieldfetch('openingbalance','accountid','account',$accountid);
		$openinggram = $this->Basefunctions->singlefieldfetch('openinggram','accountid','account',$accountid);
		if($openingbalance == '') {
		 $openingbalance = 0.00;
		}
		if($openinggram == '') {
		 $openinggram = 0.00;
		}
		$array = array(
						'amount'=>0,
						'pureweight'=>0,
				     );
		$array_data['presumvalues'] = array(
				'presumtax'=>0,
				'presumdiscount'=>0
		);
		$array_data_round['presumvaluesround'] = array(
				'summaryroundwholevalue'=>0
		);
		
		$predata =	$this->db->query(' SELECT  sum(summarytaxamount) as summarytaxamount,sum(summarydiscountamount) as summarydiscountamount  FROM sales where sales.status=1 and sales.accountid = '.$accountid.' AND sales.salestransactiontypeid != 16 and sales.taxtype = 2 GROUP BY `sales`.`accountid`');
		if($predata->num_rows() > 0) {
			foreach($predata->result() as $preinfo) {
				$array_data['presumvalues'] = array(
						'presumtax'=>$preinfo->summarytaxamount,
						'presumdiscount'=>$preinfo->summarydiscountamount,
				);
			}
		}
		$predataround =	$this->db->query(' SELECT  sum(summaryroundwholevalue) as summaryroundwholevalue  FROM sales where sales.status=1 and sales.accountid = '.$accountid.' AND sales.salestransactiontypeid != 16 GROUP BY `sales`.`accountid`');
		if($predataround->num_rows() > 0) {
			foreach($predataround->result() as $preinfo) {
				$array_data_round['presumvaluesround'] = array(
						'summaryroundwholevalue'=>$preinfo->summaryroundwholevalue,
					);
			}
		}
		$sdata=$this->db->select('selectstatement')
		->from('viewcreationcolumns')
		->where_in('viewcreationcolumns.viewcreationcolumnid',array(1991))
		->where('viewcreationcolumns.status',$this->Basefunctions->activestatus)
		->get();
		foreach($sdata->result() as $sinfo)
		{ 
		$select = $sinfo->selectstatement;
		}
		$select = str_replace("roundamount",$amountround,$select);
		$select = str_replace("roundweight",$weightround,$select);
		$data =	$this->db->query(' SELECT  '.$select.' FROM sales JOIN salesdetail on salesdetail.salesid=sales.salesid WHERE sales.status=1 AND sales.salesnumber !="pending" '.$salescond.' AND sales.salestransactiontypeid != 16 AND salesdetail.status = 1 AND sales.accountid = '.$accountid.' GROUP BY `sales`.`accountid`');
		if($data->num_rows() > 0)
		{
			foreach($data->result() as $info)
				{
					$array = array(
							
							'amount'=>number_format((float)$info->accountbalanceamount+$array_data['presumvalues']['presumtax']-$array_data['presumvalues']['presumdiscount']-$array_data_round['presumvaluesround']['summaryroundwholevalue']+$openingbalance, $amountround, '.', ''),
							'pureweight'=>number_format((float)$info->accountpureweight + $openinggram, $weightround, '.', '')
						);
				}
		}else{
				$array = array(
						'amount'=>number_format((float)$openingbalance, $amountround, '.', ''),
						'pureweight'=>number_format((float)$openinggram, $weightround, '.', '')
			);
		}
		return $array;
	}

	public function approvalnodropdown()
	{
		$data = $this->db->select('sales.salesnumber,account.accountname,sales.salesdate,salestransactiontype.salestransactiontypename')
							->from('sales')
							->join('account','account.accountid = sales.accountid')
							->join('salestransactiontype','salestransactiontype.salestransactiontypeid = sales.salestransactiontypeid')
							->where('sales.salesnumber','sales.approvalno')
							->where('sales.approvalstatus',$this->Basefunctions->activestatus)
							->where_in('salestransactiontype.salestransactiontypeid',array(18,19))// approval in/out
							->where('sales.status',$this->Basefunctions->activestatus)
							->get();
		return $data;
	}
	public function gettakeorder()
	{
		$tagid=$_GET['tagid'];
		$res=$this->db->select('itemtag.productid,itemtag.purityid')
		->from('itemtag')
		->where('itemtag.itemtagid',$tagid)
		->where('itemtag.status',1)
		->get()->row();
		$productid=$res->productid;
		$purityid=$res->purityid;
		
		$data = $this->db->select('sales.salesid,sales.salesnumber,salesdetail.salesdetailid')
					 ->from('sales')
					 ->join('salesdetail','salesdetail.salesid=sales.salesid')
					 ->where_in('orderstatusid',array(1,2))//takeorder
					 ->where('salesdetail.stocktypeid',75)//takeorder
					 ->where('salesdetail.productid',$productid)//product
					 ->where('salesdetail.purityid',$purityid)//purity
					 ->where('sales.status',1)//takeorder
					 ->where('salesdetail.status',1)//takeorder
					 ->get();
		if($data->num_rows() > 0)
		{
			foreach($data->result() as $info)
			{
				$finaltakeorders[] = array(
										   'salesnumber' => $info->salesnumber,
										   'salesid' => $info->salesid,
										   'salesdetailid' => $info->salesdetailid
										  );
			}
		}
		else
		{
			$finaltakeorders = array();
		}
		echo json_encode ($finaltakeorders);
	}
	//update the tag/sales details to manufactures/placeorder/takeorder based on orderstatus
	public function updatetagorder()
	{
		$tagorderstatus  = trim($_GET['orderstatus']);
		//pre check - revertion on old records exitances
		$this->precheckordertag($_GET['tagid']);		
		if($tagorderstatus == 'Yes')
		{
			//itemtag update
			$itemtagupdate = array(
									'orderstatus' => trim($_GET['orderstatus']),
									'ordernumber' => trim($_GET['ordernumber']).'-'.trim($_GET['salesdetailid']),
									'salesdetailid' => trim($_GET['salesdetailid'])
								);
			$itemtagupdate = array_merge($itemtagupdate,$this->Crudmodel->updatedefaultvalueget());
			$this->db->where('itemtagid',trim($_GET['tagid']));
			$this->db->update('itemtag',$itemtagupdate);
			//salesdetail update
			$salesupdate = array(
									'orderstatusid' => 3, //manufactured
									'ordernumber' => trim($_GET['ordernumber']).'-'.trim($_GET['salesdetailid']),
									'salesdetailid' => trim($_GET['salesdetailid'])
								);
			$salesupdate = array_merge($salesupdate,$this->Crudmodel->updatedefaultvalueget());
			$this->db->where('salesdetailid',trim($_GET['salesdetailid']));
			$this->db->update('salesdetail',$salesupdate);
		}
		else if($tagorderstatus == 'No')
		{
			$itemtagupdate = array(
									'orderstatus' => trim($_GET['orderstatus']),
									'ordernumber' => '',
									'salesdetailid' => 1
								);
			$itemtagupdate = array_merge($itemtagupdate,$this->Crudmodel->updatedefaultvalueget());
			$this->db->where('itemtagid',trim($_GET['tagid']));
			$this->db->update('itemtag',$itemtagupdate);
		}
	}
	//revertion of order 
	public function precheckordertag($tagid)
	{
		$data = $this->db->select('ordernumber,salesdetailid')
						->from('itemtag')
						->where('itemtagid',$tagid)
						->get();
		foreach($data->result() as $info)
		{
			$ordernumber = $info->ordernumber;
			$salesdetailid = $info->salesdetailid;
		}
		if($salesdetailid > 1 && $ordernumber != '')
		{
			$data = $this->db->select('salesdetailid')
							->from('salesdetail')
							->where('salesdetail.status',$this->Basefunctions->activestatus)
							->where('salesdetail.orderstatusid',2)//placeorder							
							->where('salesdetail.ordernumber',$ordernumber)
							->get();
			if($data->num_rows() > 0)
			{
				$orderstatus = 2; //placeorder
			}
			else
			{
				$orderstatus = 1; //takeorder
			}
			$updatesalesdetail = array('orderstatusid'=>$orderstatus);
			$this->db->where('salesdetailid',$salesdetailid);
			$this->db->update('salesdetail',$updatesalesdetail);
		}
	}
	/**
	*retrieve the order_addcharge on order details
	*/
	public function order_addcharge($salesdetailid) {
		//tag based charges
		$infotag = array();
		$infotagcharge = array();
		$this->db->select('additionalchargename,calculationbyname,a.value,a.additionalchargeid,a.calculationbyid,a.salesdetailadditionalchargeid,a.status as status');
		$this->db->from('salesdetailadditionalcharge as a');
		$this->db->join('additionalcharge','additionalcharge.additionalchargeid = a.additionalchargeid');
		$this->db->join('calculationby','calculationby.calculationbyid = a.calculationbyid');
		$this->db->where('a.salesdetailid',$salesdetailid);
		$data = $this->db->get();
		foreach($data->result() as $info)
		{
			$infotagcharge[] = $info->additionalchargeid;
			if($info->status == 1){
				$infotag[] = array(
									'additionalchargeid'=>$info->additionalchargeid,
									'additionalchargeidname'=>$info->additionalchargename,
									'calculationbyid'=>$info->calculationbyid,
									'calculationbyidname'=>$info->calculationbyname,
									'additionalchargevalue'=>$info->value,
									'tagadditionalchargeid'=>$info->additionalchargeid,
									'primarychargeid'=>$info->salesdetailadditionalchargeid
								);
			}			  
		}		
		return json_encode($infotag);
	}
	// Madasamy chitbook details
	public function retrievechitbookdetails() {
		$chitbookno = $_GET['chitbookno'];
		$chitbookno = explode("|",$chitbookno);
		$validchitbookno = array();
		$allchitbookno = $_GET['allchitbookno'];
		$allchitbookno = str_replace('|', ',', $allchitbookno);
		$allchitbookno = explode(",",$allchitbookno);
		$alleditchitbookno = $_GET['alleditchitbookno'];
		$alleditchitbookno = explode("|",$alleditchitbookno);
		$salesdetailentryeditstatus = $_GET['salesdetailentryeditstatus'];
		if($salesdetailentryeditstatus == 1) {
			$finalallchitbookno = array_values(array_diff($allchitbookno,$alleditchitbookno));
			if(count($finalallchitbookno) == 0) {
				$finalallchitbookno = array('1');
			}
		} else {
			$finalallchitbookno = $allchitbookno;
		}
		$invalidchit = '';
		$chitbookname = '';
		$chittotalamount = 0;
		$chitentryweight = 0;
		$chitentryamount = 0;
		$validchitbookno = array();
		$bonus = 0;
		$m = 0;
		$chitenrtydetails = array();
		$chitenrtydetails['invalidchit'] = '';
		$weightround =  $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$amountround =  $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$chitbooktypeid = $_GET['chitbooktypeid'];
		if($chitbooktypeid == 0) {
			$data = $this->db->select('chitbook.chitbookno,chitbook.status,chitbook.chitbookname,chitbook.amount,chitscheme.chitlookupsid,chitscheme.interestrate')
					->from('chitbook')
					->join('chitscheme','chitscheme.chitschemeid = chitbook.chitschemeid')
					->where_not_in('chitbook.status',array(0,3))
					->where_in('chitbookno',$chitbookno)
					->where_not_in('chitbookno',$finalallchitbookno)
					->get();
		} else if($chitbooktypeid == 1) {
				$data = $this->db->select('chitgeneration.chitbookno,chitgeneration.status,chitbook.chitbookname,chitbook.amount,chitscheme.chitlookupsid,chitscheme.interestrate, chitgeneration.totalpaidamt,chitgeneration.totalpaidgrms,chitgeneration.totalbonusamt,chitgeneration.totalamtclosure,chitgeneration.adjustmentamt')
					->from('chitgeneration')
					->join('chitbook','chitbook.chitbookno = chitgeneration.chitbookno')
					->join('chitscheme','chitscheme.chitschemeid = chitbook.chitschemeid')
					->where_not_in('chitgeneration.status',array(0,3))
					->where_in('chitgenerationnumber',$chitbookno)
					->where_not_in('chitgenerationnumber',$finalallchitbookno)
					->get();
		}
		if($data->num_rows() > 0) {
			foreach($data->result() as $info) {
				$chitstatus = $info->status;
				if($chitstatus == '1') {
					array_push($validchitbookno,$info->chitbookno);
					$m++;
					if($chitbooktypeid == 0) {
						$datachit =	$this->db->query('SELECT ROUND(COALESCE(SUM(chitentry.chitamount),0),'.$amountround.') as chitamount ,  ROUND(COALESCE(SUM(chitentry.entryamount),0),'.$amountround.') as entryamount ,ROUND(COALESCE(SUM(chitentry.entryweight),0),'.$weightround.') as entryweight from chitentry where chitentry.chitbookno = "'.$info->chitbookno.'" and chitentry.status = 1 ');
						if($datachit->num_rows() > 0) {
							foreach($datachit->result() as $infochit) {
								$chitentryamount = $chitentryamount + $infochit->entryamount;
								$chitentryweight = $chitentryweight + $infochit->entryweight;
								$chittotalamount = $chittotalamount + $infochit->chitamount;
								$bonus = $bonus + $chittotalamount*($info->interestrate/100);
								$chitbookname = $chitbookname.$info->chitbookname.'|';
							}
							if($info->interestrate > 0) {
								$bonus = $chittotalamount*($info->interestrate/100);
							}
						}
					} else if($chitbooktypeid == 1) {
						$chitentryamount = $info->totalpaidamt;
						$chitentryweight = $info->totalpaidgrms;
						$chittotalamount = $info->totalpaidamt + $info->adjustmentamt;
						$bonus = $info->totalbonusamt;
						$chitbookname = $info->chitbookname;
					}
				} else if($chitstatus == '11') {
					$invalidchit = $invalidchit.$info->chitbookno.',';
				}
			}
			$vcb = implode ("|", $validchitbookno);
			$chitbookname = rtrim($chitbookname,'|');
			$chitenrtydetails = array(
				'chitbookname' =>$chitbookname,
				'chitamount' =>number_format((float)$chittotalamount, $amountround, '.', ''),
				'totalpaidamount'=>number_format((float)$bonus, $amountround, '.', ''), // bonus field in form
				'amountentered'=>number_format((float)$chitentryamount, $amountround, '.', ''),
				'weightentered'=>number_format((float)$chitentryweight, $weightround, '.', ''),
				'chitgram'=>number_format((float)$chitentryweight, $weightround, '.', ''),
				'netweight'=>number_format((float)$chitentryweight, $weightround, '.', ''),
				'conversionrate'=>'',
				'chittypeid'=>'',
				'chitbookno'=>$vcb,
				'validbooks'=>'YES',
				'totalbooks'=>$m
				);	
		} else {
			$chitenrtydetails['output']='NO';
		}
		if($invalidchit != '') {
			$invalidchit = ' these chitbook are Closed ';
			$chitenrtydetails['invalidchit'] = $invalidchit;
		}
		echo json_encode($chitenrtydetails);	
	}
	public function banknameload() {
		$bank_name=array();
		$banknames = $this->db->distinct()->select('bankname')->from('salesdetail')->where('status',$this->Basefunctions->activestatus)->where('bankname !=','')->get(); 
		if($banknames->num_rows() > 0) {
		$i=0;
		foreach($banknames->result() as $banknamesdata) {
			$bank_name[$i]=$banknamesdata->bankname;
			 	$i++;
					}
		}
		else {
			$bank_name[]='';
		} 
		$datas = array_map("unserialize", array_unique(array_map("serialize", $bank_name))); 
			echo json_encode($datas);
		}
		
		public function stoneentryretrive_stone()
		{
			$tagnumber=$_REQUEST['tagnumber'];
			$accid=$_REQUEST['accid'];
			$this->db->select('itemtagid');
			$this->db->from('itemtag');
			$this->db->where('itemtagnumber',$tagnumber);
			$this->db->where_in('status',array($this->Basefunctions->activestatus));
			$stoneentry = $this->db->get()->row();
			//tag based charges
			$infotag = array();
			$this->db->select('se.stoneentryid,se.itemtagid,se.stoneid,stone.stonename,se.purchaserate,se.stonerate,se.pieces,se.caratweight,se.gram,se.totalcaratweight,se.totalnetweight,se.totalamount,stonetype.stonetypeid,stonetype.stonetypename,stoneentrycalctype.stoneentrycalctypeid,stoneentrycalctype.stoneentrycalctypename,
		(SELECT (SELECT applyrate from stonechargedetail
		where stonechargeid IN (select acc.stonechargeid from account as acc where acc.accountid='.$accid.') AND
		stoneid=se.stoneid AND status=1)) AS applyrate');
			$this->db->from('stoneentry as se');
			$this->db->join('stone','stone.stoneid = se.stoneid');
			$this->db->join('stonecategory','stonecategory.stonecategoryid = stone.stonecategoryid');
			$this->db->join('stonetype','stonetype.stonetypeid = stonecategory.stonetypeid');
			$this->db->join('stoneentrycalctype','stoneentrycalctype.stoneentrycalctypeid = se.stoneentrycalctypeid');
			$this->db->where('se.itemtagid',$stoneentry->itemtagid);
			$this->db->where_in('se.status',array($this->Basefunctions->activestatus));
			$data = $this->db->get();
			$data_stonediv = $data;
			if($data->num_rows() > 0)
			{
				foreach($data->result() as $row)
				{	if($row->applyrate!='')
				{
					$stonerate=$row->applyrate;
					$totalamt=round($stonerate*$row->caratweight);
				}
				else
				{
					$stonerate=$row->stonerate;
					$totalamt=$row->totalamount;
				}
		
				$infotag[] = array(
						'itemtagid' =>$row->stoneid,
						'stonegroupid'=>$row->stonetypeid,
						'stonegroupname'=>$row->stonetypename,
						'stoneid' => $row->stoneid,
						'stonename'=>$row->stonename,
						'purchaserate' =>$row->purchaserate,
						'basicrate' =>$stonerate,
						'stoneentrycalctypeid' => $row->stoneentrycalctypeid,
						'stoneentrycalctypename'=>$row->stoneentrycalctypename,
						'stonepieces' => $row->pieces,
						'caratweight' => $row->caratweight,
						'stonegram' => $row->gram,
						'totalcaratweight' => $row->totalcaratweight,
						'totalweight' =>$row->totalnetweight,
						'stonetotalamount' =>$totalamt
				);
					
				}
					$j=1;
				foreach($data_stonediv->result() as $value) {
					if($value->applyrate!='')
					{
						$stonerate_div=$value->applyrate;
						$totalamt_div=round($stonerate_div*$value->caratweight);
					}
					else
					{
						$stonerate_div=$value->stonerate;
						$totalamt_div=$value->totalamount;
					}
					$stonedetail->rows[$j]['id'] = $j;
						$stonedetail->rows[$j]['cell']=array(
								$value->stonetypeid,
								$value->stonetypename,
								$value->stoneid,
								$value->stonename,
								$value->purchaserate,
								$stonerate_div,
								$value->stoneentrycalctypeid,
								$value->stoneentrycalctypename,
								$value->pieces,
								$value->caratweight,
								$value->gram,
								$value->totalcaratweight,
								$value->totalnetweight,
								$totalamt_div
						);
					
					$j++;
				}
			}else{
				$infotag = 0;
			}
			$tagjsonarray['stoneentry']=json_encode($infotag);
			$tagjsonarray['stonedetail']=$stonedetail;
			echo json_encode($tagjsonarray);
		}
	/* public function stoneentryretrive() {
		$tagnumber=$_REQUEST['tagnumber'];
		$accid=$_REQUEST['accid'];
		$this->db->select('itemtagid');
		$this->db->from('itemtag');
		$this->db->where('itemtagnumber',$tagnumber);
		$this->db->where_in('status',array($this->Basefunctions->activestatus));
		$stoneentry = $this->db->get()->row();
		//tag based charges
		$infotag = array();
		$this->db->select('se.stoneentryid,se.itemtagid,se.stoneid,stone.stonename,se.purchaserate,se.stonerate,se.pieces,se.caratweight,se.gram,se.totalcaratweight,se.totalnetweight,se.totalamount,stonetype.stonetypeid,stonetype.stonetypename,stoneentrycalctype.stoneentrycalctypeid,stoneentrycalctype.stoneentrycalctypename,
		(SELECT (SELECT applyrate from stonechargedetail 
		where stonechargeid IN (select acc.stonechargeid from account as acc where acc.accountid='.$accid.') AND 
		stoneid=se.stoneid AND status=1)) AS applyrate');
		$this->db->from('stoneentry as se');
		$this->db->join('stone','stone.stoneid = se.stoneid');
		$this->db->join('stonecategory','stonecategory.stonecategoryid = stone.stonecategoryid');
		$this->db->join('stonetype','stonetype.stonetypeid = stonecategory.stonetypeid');
		$this->db->join('stoneentrycalctype','stoneentrycalctype.stoneentrycalctypeid = se.stoneentrycalctypeid');
		$this->db->where('se.itemtagid',$stoneentry->itemtagid);
		$this->db->where_in('se.status',array($this->Basefunctions->activestatus));
		$data_stonediv = $this->db->get();
			
		if($data_stonediv->num_rows() > 0) {
			$j=1;
			foreach($data_stonediv->result() as $value) {
				if($value->applyrate!='')
				{
					$stonerate_div=$value->applyrate;
					$totalamt_div=round($stonerate_div*$value->caratweight);
				}
				else
				{
					$stonerate_div=$value->stonerate;
					$totalamt_div=$value->totalamount;
				}
				$stonedetail->rows[$j]['id'] = $j;
					$stonedetail->rows[$j]['cell']=array(
							$value->stonetypeid,
							$value->stonetypename,
							$value->stoneid,
							$value->stonename,
							$value->purchaserate,
							$stonerate_div,
							$value->stoneentrycalctypeid,
							$value->stoneentrycalctypename,
							$value->pieces,
							$value->caratweight,
							$value->gram,
							$value->totalcaratweight,
							$value->totalnetweight,
							$totalamt_div
					);
				
				$j++;
			}
		}else{
			$stonedetail='';
		}
		echo json_encode($stonedetail);
	} */
	
	/**
	*retrieve the Stone Entery details of an itemtag
	*/
	public function retrive_sales_stoneentry($salesid,$salesdetailid)
	{
		//tag based charges
		$infotag = array();
		$infotagcharge = array();
		$this->db->select('se.salesstoneentryid,se.stoneid,se.stonetypeid as orgstonetypeid,stone.stonename,se.purchaserate,se.stonerate,se.pieces,se.caratweight,se.totalcaratweight,se.totalnetweight,se.totalamount,stonetype.stonetypeid,stonetype.stonetypename,stoneentrycalctype.stoneentrycalctypeid,stoneentrycalctype.stoneentrycalctypename,se.gram');
		$this->db->from('salesstoneentry as se');
		$this->db->join('stone','stone.stoneid = se.stoneid');
		$this->db->join('stonecategory','stonecategory.stonecategoryid = stone.stonecategoryid');
		$this->db->join('stonetype','stonetype.stonetypeid = stonecategory.stonetypeid');
		$this->db->join('stoneentrycalctype','stoneentrycalctype.stoneentrycalctypeid = se.stoneentrycalctypeid');
		$this->db->where('se.salesdetailid',$salesdetailid);
		$this->db->where('se.salesid',$salesid);
		$this->db->where_in('se.status',array($this->Basefunctions->activestatus));
		$data = $this->db->get();
		$i=1;
		foreach($data->result() as $row)
		{	
			$infotag[] = array(
								'tagid'=>$i,
								'salesid' =>$salesid,
								'salesdetailid'=>$salesdetailid,
								'stonegroupid'=>$row->orgstonetypeid,
								'stoneidgroupname'=>$row->stonetypename,
								'stoneid' => $row->stoneid,
								'stoneidname'=>$row->stonename,
								'purchaserate' =>$row->purchaserate,
								'basicrate' =>$row->stonerate,
								'stoneentrycalctypeid' => $row->stoneentrycalctypeid,
								'stoneentrycalctypename'=>$row->stoneentrycalctypename,
								'stonepieces' => $row->pieces,	
								'caratweight' => $row->caratweight,						
								'stonegram' => $row->gram,					
								'totalcaratweight' => $row->totalcaratweight,						
								'totalweight' =>$row->totalnetweight,
								'stonetotalamount' => $row->totalamount
							);
			$i++;
					  
		}
		
		return json_encode($infotag);
	}
	public function retrive_sales_stoneentry_div()
	{
		$salesid=$_POST['salesid'];
		$salesdetailid=$_POST['primaryid'];
		//tag based charges
		$infotag = array();
		$infotagcharge = array();
		$this->db->select('se.salesstoneentryid,se.stoneid,se.stonetypeid as orgstonetypeid,stone.stonename,se.purchaserate,se.stonerate,se.pieces,se.caratweight,se.totalcaratweight,se.totalnetweight,se.totalamount,stonetype.stonetypeid,stonetype.stonetypename,stoneentrycalctype.stoneentrycalctypeid,stoneentrycalctype.stoneentrycalctypename,se.gram');
		$this->db->from('salesstoneentry as se');
		$this->db->join('stone','stone.stoneid = se.stoneid');
		$this->db->join('stonecategory','stonecategory.stonecategoryid = stone.stonecategoryid');
		$this->db->join('stonetype','stonetype.stonetypeid = stonecategory.stonetypeid');
		$this->db->join('stoneentrycalctype','stoneentrycalctype.stoneentrycalctypeid = se.stoneentrycalctypeid');
		$this->db->where('se.salesdetailid',$salesdetailid);
		$this->db->where('se.salesid',$salesid);
		$this->db->where_in('se.status',array($this->Basefunctions->activestatus));
		$data = $this->db->get();
		if($data->num_rows()>0){
			$i=1;
			foreach($data->result() as $value)
			{
				$stonedetail->rows[$i]['id'] = $i;
				$stonedetail->rows[$i]['cell']=array(
						$value->orgstonetypeid,
						$value->stonetypename,
						$value->stoneid,
						$value->stonename,
						$value->purchaserate,
						$value->stonerate,
						$value->stoneentrycalctypeid,
						$value->stoneentrycalctypename,
						$value->pieces,
						$value->caratweight,
						$value->gram,
						$value->totalcaratweight,
						$value->totalnetweight,
						$value->totalamount
				);
			$i++;
			}	
		}else{
			$stonedetail='FAIL';
		}
	
		echo json_encode($stonedetail);
	}
	public function retrive_sales_taxentry_div() {
		$salesid=$_POST['salesid'];
		$salesdetailid=$_POST['primaryid'];
		//tag based charges
		$infotag = array();
		$infotagcharge = array();
		$this->db->select('se.taxname,se.taxid,se.taxrate,se.value');
		$this->db->from('salesdetailtax as se');
		$this->db->where('se.salesdetailid',$salesdetailid);
		$this->db->where('se.salesid',$salesid);
		$this->db->where_in('se.status',array($this->Basefunctions->activestatus));
		$data = $this->db->get();
		if($data->num_rows()>0){
			$i=1;
			foreach($data->result() as $value)
			{
				$taxdetail->rows[$i]['id'] = $i;
				$taxdetail->rows[$i]['cell']=array(
						$value->taxid,
						$value->taxname,
						$value->taxrate,
						$value->value
			   );
				$i++;
			}
		}else{
			$taxdetail='FAIL';
		}
	
		echo json_encode($taxdetail);
	}
	/**
	*retrieve the EzeTap Payment details
	*/
	public function retrive_ezetap_payment($salesid,$salesdetailid)
	{
		//tag based charges
		$infotag = array();
		$infotagcharge = array();
		$this->db->select('salesid, txnId, txnDate, amount, currencyCode, paymentMode, authCode, deviceSerial, mid, tid, paymentstatus, email, mobileNo, receiptUrl, receiptDate, primaryRef, maskedCardNo, cardBrand, merchantCode, merchantName, response,status');
		$this->db->from('salesezetappayment');
		$this->db->where('salesid',$salesid);
		$this->db->where('salesdetailid',$salesdetailid);
		$this->db->where_in('status',array($this->Basefunctions->activestatus));
		$this->db->order_by('salesezetappayment.salesdetailid','asc');
		$data = $this->db->get();
		$i=1;
		foreach($data->result() as $row)
		{	
			$infotag = array(
							'salesid'=>$salesid,
							'salesdetailid'=>$salesdetailid,
							'result'=>array(
								'txn' => array(
									'txnId' => $row->txnId,
									'txnDate' => $row->txnDate,
									'amount' =>  $row->amount,
									'currencyCode'=> $row->currencyCode,
									'paymentMode' => $row->paymentMode,								
									'authCode' =>$row->authCode,
									'deviceSerial' =>$row->deviceSerial ,
									'mid' => $row->mid,
									'tid' => $row->tid,								
									'status' =>$row->paymentstatus,		
									),
								'customer' => array(
									'email' =>  $row->email,
									'mobileNo' => $row->mobileNo	
									),
								'receipt' => array(
									'receiptUrl' =>  $row->receiptUrl,					
									'receiptDate' =>  $row->receiptDate
									),
								'references' => array(
									'primaryRef' => $row->primaryRef,
									),
								'card' => array(
									'maskedCardNo' => $row->maskedCardNo,
									'cardBrand' =>  $row->cardBrand	
									),
								'merchant' => array(
									'merchantCode' => $row->merchantCode,
									'merchantName' => $row->merchantName
									)
								)
							);
			$i++;
		}
		return json_encode($infotag);
	}
	public function accpuritybasednetmccalc()
	{
		$accid=$_GET['accid'];
		$purityid=$_GET['purityid'];
		$this->db->select('COUNT(stonesettingid) as countofid,netwtcalctypeid,mccalctypeid');
		$this->db->from('stonesetting');
		$this->db->where('stonesetting.accountid',$accid);	
		$this->db->where('stonesetting.purityid',$purityid);
		$this->db->where('stonesetting.status',$this->Basefunctions->activestatus);
		$result=$this->db->get()->row();
		$valinfo= array();
		$valinfo=array(		
						'countofid'=>$result->countofid,
						'netwtcalcid'=>$result->netwtcalctypeid,
						'mccalcid' =>$result->mccalctypeid,
			);
		echo json_encode($valinfo);
	}
	public function getratefromstonename() {
		$stoneid=$_POST['stoneid'];
		$accid=$_POST['accid'];
		$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3);
		$this->db->select('COUNT(scd.stonechargedetailid) as countofid,scd.applyrate,stone.purchaserate,stone.stoneentrycalctypeid');
		$this->db->from('stonechargedetail as scd');
		$this->db->join('account as acc','acc.stonechargeid=scd.stonechargeid');
		$this->db->join('stone','stone.stoneid =scd.stoneid');
		$this->db->where('acc.accountid',$accid);	
		$this->db->where('scd.stoneid',$stoneid);
		$this->db->where('scd.status',$this->Basefunctions->activestatus);
		$result=$this->db->get()->row();
		
		if($result->countofid!=0){
			$stoneentrycalctypeid = $this->Basefunctions->singlefieldfetch('stoneentrycalctypeid','stoneid','stone',$stoneid);
		  	$data = array('stonerate'=>number_format((float)$result->applyrate, $amountround, '.', ''),'purchaserate'=>number_format((float)$result->purchaserate, $amountround, '.', ''),'stoneentrycalctypeid'=>$result->stoneentrycalctypeid);
		} else {
			$result = $this->db->select('stonerate,purchaserate,stoneentrycalctypeid')->from('stone')->where('stoneid',$stoneid)->where('status',1)->get();
			foreach($result->result() as $result_data){
				$data = array('stonerate'=>number_format((float)$result_data->stonerate, $amountround, '.', ''),'purchaserate'=>number_format((float)$result_data->purchaserate, $amountround, '.', ''),'stoneentrycalctypeid'=>$result_data->stoneentrycalctypeid);
			}
		}
		echo json_encode($data);
	}
	function getaccountnotificationdetails()
	{
		$accountid=$_GET['accountid'];
		$this->db->select('mobilenumber,emailid');
		$this->db->from('account');
		$this->db->where('accountid',$accountid);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$data=$this->db->get();		
		if($data->num_rows() > 0){
		$result=$data->row();
		$valinfo=array(
				'mobilenumber'=>$result->mobilenumber,
				'email'=>$result->emailid,
		);
		}else{
		$valinfo=array(
				'mobilenumber'=>1,
				'email'=>'',
		);
		}
		echo json_encode($valinfo);
	}
	function checkezetaportstatus()
	{	
		$connection = @fsockopen("localhost",8081);
		if ($connection) {
			$portstatus=1;
			fclose($connection);
		} else {
			$portstatus=0;
		}
		echo $portstatus;exit;
	}
	public function viewdynamicdatainfofetch($tablename,$sortcol,$sortord,$pagenum,$rowscount,$salesid,$salesdetailsid) {
	
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2);//weight round-off
		$rateround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5);//rate round-off
		$meltinground = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',4); //melting round-off
		$dataset ='sales.salesid,salesdetail.salesdetailid,purity.purityname,stocktype.stocktypename,salesdetail.totalamount,salesdetail.pureweight,itemtag.itemtagnumber,itemtag.rfidtagno,ROUND(salesdetail.ratepergram,'.$rateround.') as ratepergram,product.productname,ROUND(salesdetail.grossweight,'.$round.') as grossweight,ROUND(salesdetail.stoneweight,'.$round.') as stoneweight,ROUND(salesdetail.netweight,'.$round.') as netweight,salesdetail.pieces,salesdetail.bankname,salesdetail.ordernumber,salesdetail.comment,salesdetail.modeid,counter.countername,
		ROUND(salesdetail.secondtouch,'.$meltinground.') as secondtouch,salesdetail.grossamount,salesdetail.stoneamount,salesdetail.chargesamount,salesdetail.taxamount,salesdetail.discountamount,defbank.accountname as defaultbankname,employee.employeename,salesdetail.chitbookno,salesdetail.chitamount,salesdetail.chittotalpaid,chitbook.chitbookname,ordermode.ordermodename,salesdetail.stocktypeid,journalmode.journalmodename,paymentaccount.accountname as paymentaccountname,salesdetail.paymentaccountid,salesdetail.detailordernumberid,salesdetail.lottypeid,salesdetail.purchasemodeid';
		$join=' LEFT OUTER JOIN salesdetail ON salesdetail.salesid=sales.salesid';
		$join.=' LEFT OUTER JOIN ordermode ON ordermode.ordermodeid=salesdetail.ordermodeid';
		$join.=' LEFT OUTER JOIN purity ON purity.purityid=salesdetail.purityid';
		$join.=' LEFT OUTER JOIN stocktype ON stocktype.stocktypeid=salesdetail.stocktypeid';
		$join.=' LEFT OUTER JOIN product ON product.productid=salesdetail.productid';
		$join.=' LEFT OUTER JOIN itemtag ON itemtag.itemtagid=salesdetail.itemtagid';
		$join.=' LEFT OUTER JOIN counter ON counter.counterid=salesdetail.counterid';
		$join.=' LEFT OUTER JOIN account as defbank ON defbank.accountid=salesdetail.defaultbankid';
		$join.=' LEFT OUTER JOIN employee ON employee.employeeid=salesdetail.employeeid';
		$join.=' LEFT OUTER JOIN chitbook ON chitbook.chitbookno=salesdetail.chitbookno';
		
		$join.=' LEFT OUTER JOIN account as paymentaccount ON paymentaccount.accountid=salesdetail.paymentaccountid';
		$cuscondition=$tablename.'.salesid='.$salesid.' AND ';
		if($salesdetailsid != 1) {
			$cuscondition .='salesdetail.salesdetailid in('.$salesdetailsid.') AND ';
		}
		$status='salesdetail.status='.$this->Basefunctions->activestatus.'';
	
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		/* query */
		$result = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$cuscondition.' '. $status.' ORDER BY '.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$data=array();
		$i=0;
		foreach($result->result() as $row) {
			$salesid = $row->salesid;
			$salesdetailid = $row->salesdetailid;
			$purityname =$row->purityname;
			$stocktypename =$row->stocktypename;
			$totalamount =$row->totalamount;
			$pureweight =$row->pureweight;
			$tagnumber =$row->itemtagnumber;
			$rfidnumber =$row->rfidtagno;
			$rate =$row->ratepergram;
			$productname =$row->productname;
			$grossweight =$row->grossweight;
			$stoneweight =$row->stoneweight;
			$netweight =$row->netweight;
			$pieces =$row->pieces;
			$bankname =$row->bankname;
			$ordernumber =$row->ordernumber;
			$comment =$row->comment;
			if($row->modeid == 2) {
				$modename = 'Transaction';
			} else {
				$modename = 'Payment';
			}
			$counter =$row->countername;
			$secondtouch = $row->secondtouch;
			$grossamount = $row->grossamount;
			$stoneamount = $row->stoneamount;
			$chargesamount = $row->chargesamount;
			$taxamount = $row->taxamount;
			$discountamount = $row->discountamount;
			$defaultbankname = $row->defaultbankname;
			$employeename = $row->employeename;
			$chitbookno = $row->chitbookno;
			$chitamount = $row->chitamount;
			$chittotalpaid = $row->chittotalpaid;
			$chitbookname = $row->chitbookname;
			$ordermodename = $row->ordermodename;
			$stocktypeid = $row->stocktypeid;
			$journalmodename = $row->journalmodename;
			$paymentaccountname = $row->paymentaccountname;
			$paymentaccountid = $row->paymentaccountid;
			$detailordernumberid = $row->detailordernumberid;
			$lottypeid = $row->lottypeid;
			$purchasemodeid = $row->purchasemodeid;
		$counterstatus = $this->Basefunctions->get_company_settings('counter');
		if($counterstatus == 'YES'){	$data[$i]=array('id'=>$salesdetailid,$modename,$stocktypename,$tagnumber,$rfidnumber,$productname,$purityname,$counter,$grossweight,$stoneweight,$netweight,$pieces,$totalamount,$pureweight,$rate,$secondtouch,$grossamount,$chargesamount,$stoneamount,$taxamount,$discountamount,$defaultbankname,$bankname,$ordernumber,$chitbookno,$chitbookname,$chitamount,$chittotalpaid,$employeename,$comment,$ordermodename,$stocktypeid,$journalmodename,$paymentaccountname,$paymentaccountid,$detailordernumberid,$lottypeid,$purchasemodeid );
								   }else{
			$data[$i]=array('id'=>$salesdetailid,$modename,$stocktypename,$tagnumber,$rfidnumber,$productname,$purityname,$grossweight,$stoneweight,$netweight,$pieces,$totalamount,$pureweight,$rate,$secondtouch,$grossamount,$chargesamount,$stoneamount,$taxamount,$discountamount,$defaultbankname,$bankname,$ordernumber,$chitbookno,$chitbookname,$chitamount,$chittotalpaid,$employeename,$comment,$ordermodename,$journalmodename,$paymentaccountname,$paymentaccountid,$detailordernumberid,$lottypeid,$purchasemodeid );
		}
			$i++;
		}
		$finalresult=array('0'=>$data,'1'=>$page,'2'=>'json');
		return $finalresult;
	
	}
	public function retrive($salesid,$salesdetailid)
	{
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$meltinground = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',4); //melting round-off
		$this->db->select( 	'salesdetail.salesdetailid,salesdetail.itemtagid,salesdetail.purityid,salesdetail.productid,salesdetail.counterid,salesdetail.processcounterid,ROUND(salesdetail.grossweight,'.$round.') as grossweight,salesdetail.comment,
		ROUND(salesdetail.stoneweight,'.$round.') as stoneweight,
		ROUND(salesdetail.dustweight,'.$round.') as dustweight,
		ROUND(salesdetail.netweight,'.$round.') as netweight,
		ROUND(itemtag.grossweight,'.$round.') as itemgrossweight,
		ROUND(salesdetail.stoneamount,'.$amountround.') as  stoneamount,
		ROUND(salesdetail.melting,'.$meltinground.') as melting, ROUND(salesdetail.secondtouch,'.$meltinground.') as secondtouch, salesdetail.pieces,salesdetail.stocktypeid,salesdetail.ratepergram,salesdetail.totalamount,salesdetail.pureweight,itemtag.itemtagnumber,itemtag.rfidtagno,salesdetail.salesdetailimage,salesdetail.ordernumber,DATE_FORMAT(salesdetail.orderduedate,"%d-%m-%Y") AS orderduedate,salesdetail.referencenumber,salesdetail.issuereceipttypeid,salesdetail.otherdetails,salesdetail.otherdetailsvalue,salesdetail.ckeyword,salesdetail.ckeywordvalue,salesdetail.ckeywordoriginalvalue,salesdetail.ckeywordcalvalue,salesdetail.ckeywordfcalvalue,salesdetail.approvalstatus,salesdetail.chitbookno,salesdetail.chittotalpaid,salesdetail.interestcalculate,salesdetail.interestamt,salesdetail.chitamount,salesdetail.bankname,DATE_FORMAT(salesdetail.expiredate,"%d-%m-%Y") AS expiredate,DATE_FORMAT(salesdetail.referencedate,"%d-%m-%Y") AS referencedate,salesdetail.approvalcode,salesdetail.cardtypeid,salesdetail.netwtcalctypeid,salesdetail.olditemdetails,itemtag.itemtagnumber,salesdetail.modeid,salesdetail.discountamount,salesdetail.taxamount,salesdetail.calculationtypeid,salesdetail.itemdiscountlimit,salesdetail.discountpercent,salesdetail.giftnumber,salesdetail.giftremark,salesdetail.giftdenomination,salesdetail.giftunit,salesdetail.defaultcardid,salesdetail.defaultbankid,salesdetail.grossamount,salesdetail.wastage,salesdetail.making,salesdetail.wastagespan,salesdetail.makingspan,salesdetail.wastagespanlabel,salesdetail.makingchargespanlabel,salesdetail.employeeid,salesdetail.pendingproductid,salesdetail.chitbookname,salesdetail.chitschemetypeid,salesdetail.chitgram,salesdetail.grambonus,salesdetail.chargesamount,salesdetail.flatcharge,salesdetail.flatchargespan,salesdetail.rateclone,salesdetail.rateaddition,salesdetail.estimationnumber,salesdetail.wastageweight,salesdetail.wastageless,salesdetail.taxcategoryid,salesdetail.paymentaccountid,salesdetail.orderitemsize,salesdetail.ordermodeid,salesdetail.lottypeid,salesdetail.purchasemodeid,salesdetail.netweightcalculationid,salesdetail.ratelesscalc,salesdetail.accounttypeid',false);
		$this->db->from('salesdetail');
		$this->db->where('salesdetail.salesdetailid',$salesdetailid);
		$this->db->join('itemtag','itemtag.itemtagid = salesdetail.itemtagid','left outer');
		$this->db->where_not_in('salesdetail.status',array(0,3));
		$this->db->limit(1);
		$info = $this->db->get();
		if($info->num_rows() == 1)
		{
			$infos = $info->row();
			$bgetrate = $this->Basefunctions->getrate($infos->purityid);
			$getrate = $bgetrate['rate'];
			$stoneentry = $this->retrive_sales_stoneentry($salesid,$salesdetailid);
			$ckeyword=array();
					$ckeywordvalue=array();
					$keyworddetails=array();
					$keyworddata=array();
					if($infos->ckeyword != '' && $infos->ckeywordvalue != ''){
						$ckeyword = explode(',',$infos->ckeyword);
						$ckeywordvalue = explode(',',$infos->ckeywordvalue);
					$keyworddetails = array_combine($ckeyword, $ckeywordvalue);
					$j = 0;
					foreach($keyworddetails as $j => $l){
						$keyworddata[] = $j. ":".$l;
						$j++;
					}
					$keyword_str = implode (",", $keyworddata);
					}else{
						$keyword_str ='';
					}
					$ckeyword1=array();
					$ckeywordoriginalvalue=array();
					$keyworddetails1=array();
					$keyworddata1=array();
					if($infos->ckeyword != ''){
						$ckeyword1 = explode(',',$infos->ckeyword);
						$ckeywordoriginalvalue = explode(',',$infos->ckeywordoriginalvalue); 
					$keyworddetails1 = array_combine($ckeyword1, $ckeywordoriginalvalue);
					$m = 0;
					foreach($keyworddetails1 as $m => $p){
						$keyworddata1[] = $m. ":".$p;
						$m++;
					}
					$keyword_str1 = implode (",", $keyworddata1);
					}else{
						$keyword_str1 ='';
					}
					if($infos->modeid == 3){ //for payment
						$tagjsonarray = array(
							'salesdetailid' => $infos->salesdetailid,
							'modeid' => $infos->modeid,
							'paymentstocktypeid' => $infos->stocktypeid,
							'itemtagid' => $infos->itemtagid,
							'itemtagnumber' => $infos->itemtagnumber,
							'paymentproduct' => $infos->productid,
							'paymentpurity'=> $infos->purityid,
							'paymentcounter'=> $infos->counterid,
							'balancecounter'=> $infos->processcounterid,
							'paymentgrossweight' => $infos->grossweight + 0,
							'stoneweight' => $infos->stoneweight + 0,
							'dustweight' => $infos->dustweight + 0,
							'paymentnetweight' => $infos->netweight + 0,
							'pieces' => $infos->pieces,
							'paymentratepergram'=>$infos->ratepergram,
							'salescomment' => $infos->comment,
							'paymentpureweight' => $infos->pureweight,
							'referencenumber' => $infos->referencenumber,
							'paymentrefnumber' => $infos->referencenumber,
							'issuereceiptid' =>$infos->issuereceipttypeid,
							'otherdetails' => $infos->otherdetails,
							'otherdetailsvalue'=>$infos->otherdetailsvalue,
							'ckeyword'=>$infos->ckeyword,
							'ckeywordvalue' => $infos->ckeywordvalue,
							'ckeywordvaluedetails' => $keyword_str,
							'ckeywordoriginalvaluedetails' => $keyword_str1,
							'ckeywordoriginalvalue' => $infos->ckeywordoriginalvalue,
							'ckeywordcalvalue' => $infos->ckeywordcalvalue,
							'ckeywordfcalvalue'=>$infos->ckeywordfcalvalue,
							'hiddenstonedata' => $stoneentry,
							'approvalstatus'=>$infos->approvalstatus,
							'chitbookno' => $infos->chitbookno,
							'prechitbooknumber' => $infos->chitbookno,
							'chitbookname' => $infos->chitbookname,
							'chitschemetypeid' => $infos->chitschemetypeid,
							'chitamount'=>$infos->chitamount,
							'totalpaidamount' => $infos->chittotalpaid,
							'chitgram'=>$infos->chitgram,
							'grambonus'=>$infos->grambonus,
							'interestcalc'=>$infos->interestcalculate,
							'interestamount' =>$infos->interestamt,
							'discount'=>$infos->discountamount,
							'grossamount'=>$infos->grossamount,
							'paymenttotalamount'=>$infos->totalamount,
							'prepaymenttotalamount'=>$infos->totalamount,
							'taxamount'=>$infos->taxamount,
							'stonecharge'=>$infos->stoneamount,
							'bankname'=>$infos->bankname,
							'expiredate' =>$infos->expiredate,
							'paymentreferencedate'=>$infos->referencedate,
							'cardtype' =>$infos->cardtypeid,
							'approvalcode'=>$infos->approvalcode,
							'paymentmelting' =>$infos->melting,
							'newpaymenttouch'=>$infos->secondtouch,
							'netwtcalctype' =>$infos->netwtcalctypeid,
							'olditemdetails' =>$infos->olditemdetails,
							'rfidtagnumber' => $infos->rfidtagno,
							'wastage'=>$infos->wastage,
							'makingcharge'=>$infos->making,
							'wastagespan'=>$infos->wastagespan,
							'makingchargespan'=>$infos->makingspan,
							'flatcharge'=>$infos->flatcharge,
							'flatchargespan'=>$infos->flatchargespan,
							'calculationtypeid' =>$infos->calculationtypeid,
							'itemdiscountlimit'=>$infos->itemdiscountlimit,
							'discountpercent' =>$infos->discountpercent,
							'giftnumber' =>$infos->giftnumber,
							'giftremark' =>$infos->giftremark,
							'giftdenomination' =>$infos->giftdenomination,
							'giftunit' =>$infos->giftunit,
							'defaultcardid' =>$infos->defaultcardid,
							'defaultbankid' =>$infos->defaultbankid,
							'paymentemployeepersonid'=>$infos->employeeid,
							'pendingproductid'=>$infos->pendingproductid,
							'additionalchargeamt'=>$infos->chargesamount,
							'estimationnumber'=>$infos->estimationnumber,
							'paymentaccountid'=>$infos->paymentaccountid,
							
						);
					} else {
						$tagjsonarray = array(
							'salesdetailid' => $infos->salesdetailid,
							'modeid' => $infos->modeid,
							'stocktypeid' => $infos->stocktypeid,
							'itemtagid' => $infos->itemtagid,
							'itemtagnumber' => $infos->itemtagnumber,
							'product' => $infos->productid,
							'purity'=> $infos->purityid,
							'counter'=> $infos->counterid,
							'balancecounter'=> $infos->processcounterid,
							'grossweight' => $infos->grossweight + 0,
							'stoneweight' => $infos->stoneweight + 0,
							'dustweight' => $infos->dustweight + 0,
							'netweight' => $infos->netweight + 0,
							'pieces' => $infos->pieces,
							'ratepergram'=>$infos->ratepergram,
							'salescomment' => $infos->comment,
							'pureweight' => $infos->pureweight,
							'referencenumber' => $infos->referencenumber,
							'paymentrefnumber' => $infos->referencenumber,
							//'paymentmethodid' =>$infos->issuereceipttypeid,
							'otherdetails' => $infos->otherdetails,
							'otherdetailsvalue'=>$infos->otherdetailsvalue,
							'ckeyword'=>$infos->ckeyword,
							'ckeywordvalue' => $infos->ckeywordvalue,
							'ckeywordvaluedetails' => $keyword_str,
							'ckeywordoriginalvaluedetails' => $keyword_str1,
							'ckeywordoriginalvalue' => $infos->ckeywordoriginalvalue,
							'ckeywordcalvalue' => $infos->ckeywordcalvalue,
							'ckeywordfcalvalue'=>$infos->ckeywordfcalvalue,
							'hiddenstonedata' => $stoneentry,
							'approvalstatus'=>$infos->approvalstatus,
							'chitbookno' => $infos->chitbookno,
							'prechitbooknumber' => $infos->chitbookno,
							'chitbookname' => $infos->chitbookname,
							'chitschemetypeid' => $infos->chitschemetypeid,
							'chitamount'=>$infos->chitamount,
							'totalpaidamount' => $infos->chittotalpaid,
							'chitgram'=>$infos->chitgram,
							'grambonus'=>$infos->grambonus,
							'interestcalc'=>$infos->interestcalculate,
							'interestamount' =>$infos->interestamt,
							'discount'=>$infos->discountamount,
							'grossamount'=>$infos->grossamount,
							'totalamount'=>$infos->totalamount,
							'taxamount'=>$infos->taxamount,
							'stonecharge'=>$infos->stoneamount,
							'bankname'=>$infos->bankname,
							'expiredate' =>$infos->expiredate,
							'paymentreferencedate'=>$infos->referencedate,
							'cardtype' =>$infos->cardtypeid,
							'approvalcode'=>$infos->approvalcode,
							'paymentmelting' =>$infos->melting,
							'paymenttouch'=>$infos->secondtouch,
							'netwtcalctype' =>$infos->netwtcalctypeid,
							'olditemdetails' =>$infos->olditemdetails,
							'rfidtagnumber' => $infos->rfidtagno,
							'wastage'=>$infos->wastage,
							'wastageless'=>$infos->wastageless,
							'makingcharge'=>$infos->making,
							'wastagespan'=>$infos->wastagespan,
							'makingchargespan'=>$infos->makingspan,
							'wastagespanlabel'=>$infos->wastagespanlabel,
							'makingchargespanlabel'=>$infos->makingchargespanlabel,
							'flatcharge'=>$infos->flatcharge,
							'flatchargespan'=>$infos->flatchargespan,
							'calculationtypeid' =>$infos->calculationtypeid,
							'itemdiscountlimit'=>$infos->itemdiscountlimit,
							'discountpercent' =>$infos->discountpercent,
							'giftnumber' =>$infos->giftnumber,
							'giftremark' =>$infos->giftremark,
							'giftdenomination' =>$infos->giftdenomination,
							'giftunit' =>$infos->giftunit,
							'defaultcardid' =>$infos->defaultcardid,
							'defaultbankid' =>$infos->defaultbankid,
							'employeepersonid'=>$infos->employeeid,
							'pendingproductid'=>$infos->pendingproductid,
							'additionalchargeamt'=>$infos->chargesamount,
							'rateaddition'=>$infos->rateaddition,
							'rateclone'=>$infos->rateclone,
							'estimationnumber'=>$infos->estimationnumber,
							'wastageweight'=>$infos->wastageweight,
							'taxcategoryid'=>$infos->taxcategoryid,
							'orderitemsize'=>$infos->orderitemsize,
							'ordermodeid'=>$infos->ordermodeid,
							'lottypeid'=>$infos->lottypeid,
							'purchasemode'=>$infos->purchasemodeid,
							'gwcaculation'=>$infos->netweightcalculationid,
							'ratelesscalc'=>$infos->ratelesscalc,
							'itemgrossweight'=>$infos->itemgrossweight
						);
					}
			
		}
		else
		{
			$tagjsonarray = array('output'=>'NO');
		}
		return $tagjsonarray;
	}
	// main maindelete
	public function mainsalesdelete() {
		$salesid = $_GET['primaryid'];
		//$gridlength = $_GET['gridlength'];
		//$status = $_GET['status'];
		$sales_detail = array();
		$delete = $this->Basefunctions->delete_log();
		$loclingtime = $this->Basefunctions->get_company_settings('lockingtime');
		$updatingtime = $this->Basefunctions->get_company_settings('updatingtime');
		$currenttime = time("H:i:s");
		$ctime = new DateTime();
		$ctime->setTimestamp($currenttime);
		$ltime  = new DateTime();
		$deletepurchasestatus = 0;
		$salestransactiontype = '';
		$ledgeraccountid = 1;
		$referencenumber = '';
		$creditno = '';
		$salesdate = '0000-00-00';
		$ledgerpaymentirtypeid = 1;
		if($loclingtime == '00:00:00') {
			$ltime->setTimestamp($currenttime);
		} else {
			$ltime->setTimestamp(strtotime($loclingtime));
		}
		$accountid = $this->Basefunctions->generalinformaion('sales','accountid','salesid',$salesid);
		//retrieve salesdetailid
		$salesdetaildata = $this->db->select('salesid,stocktypeid,salesdetailid,itemtagid,referencenumber,chitbookno,presalesdetailid')
		->from('salesdetail')
		->where('salesdetail.salesid',$salesid)
		->where('salesdetail.status',$this->Basefunctions->activestatus)
		->get();
		foreach($salesdetaildata->result() as $info) {
			$sales_detail[] = array(
					'salesid'=>$info->salesid,
					'stocktypeid'=>$info->stocktypeid,
					'salesdetailid'=>$info->salesdetailid,
					'itemtagid'=>$info->itemtagid,
					'referencenumber'=>$info->referencenumber,
					'chitbookno'=>$info->chitbookno,
					'presalesdetailid'=>$info->presalesdetailid
			);
		}
		$salesdata = $this->db->select('salestransactiontypeid')
			->from('sales')
			->where('sales.salesid',$salesid)
			->where('sales.status',$this->Basefunctions->activestatus)
			->get();
		if($salesdata->num_rows()>0) {
			foreach($salesdata->result() as $salesinfo){
				$salestransactiontype = $salesinfo->salestransactiontypeid;
			}
		}
		$cancelbill = $this->db->select('salesid')
					->from('sales')
					->where('sales.salesid',$salesid)
					->where('sales.salestransactiontypeid',11)
					->where('sales.status',5)
					->get();
		if($cancelbill->num_rows()>0) {
			echo 'CANCELBILL';
		} else {
			/* if($salestransactiontype == 11) {  // sales
				$this->orderadvanceamtrevert($salesid); // orderadvanceamtrevert
			} */
			$appunt = 0;
			$approvaloutuntagsdid = array();
			// delete salesdetail tax
			$this->db->where('salesid',$salesid);
			$this->db->update('salesdetailtax',$delete);
			// Delete creditnodetails table
			$this->db->where('salesid',$salesid);
			$this->db->update('creditnodetails',$delete);
			// Delete questionairedetails table
			$this->db->where('salesid',$salesid);
			$this->db->update('questionairedetails',$delete);
			// delete sales stone entry
			$this->db->where('salesid',$salesid);
			$this->db->update('salesstoneentry',$delete);
			//delete salesdetail id
			for($i=0; $i < count($sales_detail) ;$i++) {
				//check whether they are tag methods.
				//update status of itemtag , tag or taginc
				$salestransactiontypeid = $this->Basefunctions->singlefieldfetch('salestransactiontypeid','salesid','sales',$salesid);
				if(!in_array($salestransactiontypeid,array(16,20,21,18,9))) {
					if($sales_detail[$i]['stocktypeid'] == 11 OR $sales_detail[$i]['stocktypeid'] == 13 OR $sales_detail[$i]['stocktypeid'] == 74 OR $sales_detail[$i]['stocktypeid'] == 83){
						if($sales_detail[$i]['stocktypeid'] == 74) {
							//$presalesid = $this->Basefunctions->singlefieldfetch('salesid','salesdetailid','salesdetail',$sales_detail[$i]['presalesdetailid']);
							//$presdid = $sales_detail[$i]['presalesdetailid'];
							$prestatus = $this->Basefunctions->approvaloutstatus;
							$updatesalesdetail = array(
								'approvalstatus'=>1
							);
							if($ctime > $ltime) {
								$updatesalesdetail = array_merge($updatesalesdetail,$this->Crudmodel->nextdayupdatedefaultvalueget());
							} else {
								$updatesalesdetail = array_merge($updatesalesdetail,$this->Crudmodel->updatedefaultvalueget());
							}
							$this->db->where('salesdetailid',$sales_detail[$i]['presalesdetailid']);
							$this->db->update('salesdetail',$updatesalesdetail);
						} else {
							//$presalesid = '1';
							//$presdid = '1';
							$prestatus = $this->Basefunctions->activestatus;
						}
						$update = array(
							'status'=>$prestatus
						);	
						if($ctime > $ltime) {
							$update=array_merge($update,$this->Crudmodel->nextdayupdatedefaultvalueget());
						} else {
							$update=array_merge($update,$this->Crudmodel->updatedefaultvalueget());
						}
						$this->db->where('itemtagid',$sales_detail[$i]['itemtagid']);
						$this->db->update('itemtag',$update);
						
					}
					$this->Basefunctions->stockupdateondelete($sales_detail[$i]['salesdetailid'],'52');
					if($sales_detail[$i]['stocktypeid'] == 89 && $salestransactiontypeid == 11) {
						$this->revertrepairstatus(4,5,$sales_detail[$i]['presalesdetailid']);
						$presalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$sales_detail[$i]['presalesdetailid']);
						$prepresalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$presalesdetailid);
						$this->revertrepairstatus(4,5,$presalesdetailid);
						$this->revertrepairstatus(4,5,$prepresalesdetailid);
					}
				}
				//reverting order status
				if($salestransactiontype == '21') { // place order
					$orderstatusid = $this->Basefunctions->singlefieldfetch('orderstatusid','salesdetailid','salesdetail',$sales_detail[$i]['presalesdetailid']);
					if($orderstatusid != 6) { // except cancel order
						$this->revertorderstatus(2,3,$sales_detail[$i]['presalesdetailid']);
						$presalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$sales_detail[$i]['presalesdetailid']);
						$this->revertorderstatus(2,3,$presalesdetailid);
						$this->update_placeorderitemstatus($this->Basefunctions->singlefieldfetch('salesid','salesdetailid','salesdetail',$sales_detail[$i]['presalesdetailid']));
					}
				} else if($salestransactiontype == '29' || $salestransactiontype == '30') {
					if($salestransactiontype == '29') {
						$this->revertrepairstatus(2,3,$sales_detail[$i]['presalesdetailid']);
						$presalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$sales_detail[$i]['presalesdetailid']);
						$this->revertrepairstatus(2,3,$presalesdetailid);
					} else if($salestransactiontype == '30') {
						$this->revertrepairstatus(3,4,$sales_detail[$i]['presalesdetailid']);
						$presalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$sales_detail[$i]['presalesdetailid']);
						$this->revertrepairstatus(3,4,$presalesdetailid);
					}
				} else if($sales_detail[$i]['stocktypeid']== '82') { // Receive - take order
					$orderstatusid = $this->Basefunctions->singlefieldfetch('orderstatusid','salesdetailid','salesdetail',$sales_detail[$i]['presalesdetailid']);
					if($orderstatusid != 6) { // except cancel order
						$this->revertorderstatus(3,5,$sales_detail[$i]['presalesdetailid']);
						$presalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$sales_detail[$i]['presalesdetailid']);
						$this->revertorderstatus(3,5,$presalesdetailid);
					}
				}else if($sales_detail[$i]['stocktypeid']== '83' AND $salestransactiontype == '11') { // Order Tag
					$orderstatusid = $this->Basefunctions->singlefieldfetch('orderstatusid','salesdetailid','salesdetail',$sales_detail[$i]['presalesdetailid']);
					if($orderstatusid != 6) { // except cancel order
						$this->revertorderstatus(5,4,$sales_detail[$i]['presalesdetailid']);
						$presalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$sales_detail[$i]['presalesdetailid']);
						$this->revertorderstatus(5,4,$presalesdetailid);
						$ordersalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$presalesdetailid);
						$this->revertorderstatus(5,4,$ordersalesdetailid);
					}
				} else if($sales_detail[$i]['stocktypeid']== '85') { // Generate place order revert back
					// updating place order salesdetail table
					$update_takeorderpostatus = array('generatepo'=>'1');
					$this->db->where('salesdetailid',$sales_detail[$i]['presalesdetailid']);
					$this->db->update('salesdetail',array_filter($update_takeorderpostatus));
				}
				//update approval out.
				if($sales_detail[$i]['stocktypeid'] == '16' || $sales_detail[$i]['stocktypeid'] == '66' || $sales_detail[$i]['stocktypeid'] == '25') { 
					$approvaloutuntagsdid[$appunt] = $sales_detail[$i]['presalesdetailid'];
					$appunt++;
				}
				if($sales_detail[$i]['stocktypeid'] == 62){
					$update = array(
							'salesid'=>'1',
							'salesdetailid'=>'1',
							'status'=>$this->Basefunctions->activestatus
					);
					if($ctime > $ltime) {
						$update = array_merge($update,$this->Crudmodel->nextdayupdatedefaultvalueget());
					} else {
						$update = array_merge($update,$this->Crudmodel->updatedefaultvalueget());
					}
					$this->db->where('itemtagid',$sales_detail[$i]['itemtagid']);
					$this->db->update('itemtag',$update);
				}
				//update approval out return.
				if($sales_detail[$i]['stocktypeid'] == 65){
					$update = array(
							'status'=>$this->Basefunctions->approvaloutstatus
					);
					if($ctime > $ltime) {
						$update = array_merge($update,$this->Crudmodel->nextdayupdatedefaultvalueget());
					} else {
						$update = array_merge($update,$this->Crudmodel->updatedefaultvalueget());
					}
					$this->db->where('itemtagid',$sales_detail[$i]['itemtagid']);
					$this->db->update('itemtag',$update);
					$updatesalesdetail = array(
							'approvalstatus'=>1
					);
					if($ctime > $ltime) {
						$updatesalesdetail = array_merge($updatesalesdetail,$this->Crudmodel->nextdayupdatedefaultvalueget());
					} else {
						$updatesalesdetail = array_merge($updatesalesdetail,$this->Crudmodel->updatedefaultvalueget());
					}
					$this->db->where('salesdetailid',$sales_detail[$i]['presalesdetailid']);
					$this->db->update('salesdetail',$updatesalesdetail);
				}
				// lot number delete
				if($salestransactiontype == 9){
					$lotid = $this->Basefunctions->singlefieldfetch('lotid','salesdetailid','lot',$sales_detail[$i]['salesdetailid']);
					if($lotid == 1){
						$itemtagid = 1;
					}else{
						$itemtagid = $this->Basefunctions->singlefieldfetch('itemtagid','lotid','itemtag',$lotid);
					}
					if($itemtagid == 1){
						$delete_lot = $this->Basefunctions->delete_log();
						$this->db->where('salesdetailid',$sales_detail[$i]['salesdetailid']);
						$this->db->update('lot',$delete_lot);
					}else{
						$deletepurchasestatus = 1;
					}
				}
				if($sales_detail[$i]['stocktypeid'] == 82){ // Receive
					//$this->Basefunctions->stockupdateondelete($sales_detail[$i]['itemtagid'],'50');
					$receiveorderconcept = $this->Basefunctions->get_company_settings('receiveorderconcept');
					if($receiveorderconcept == '0') { // revert itemtag entry
						$updateitemtag=array(
						'ordernumber'=>'',
						'salesdetailid'=>1,
						'orderstatus'=>'No'
						);
						$this->db->where('itemtagid',$sales_detail[$i]['itemtagid']);
						$this->db->update('itemtag',$updateitemtag);
					} else {
						$presalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$sales_detail[$i]['presalesdetailid']);
						$lotupdatesalesdetail = array(
									'status'=>0
							);
						$updatesalesdetail = array_merge($lotupdatesalesdetail);
						$this->db->where('salesdetailid',$presalesdetailid);
						$this->db->update('lot',$lotupdatesalesdetail);
					}
				}
				
				$delete = $this->Basefunctions->delete_log();
				$this->db->where('salesid',$salesid);
				$this->db->update('salesstoneentry',$delete);
					
				if($sales_detail[$i]['stocktypeid'] == 39) {
					$chitbooktypeid = $this->Basefunctions->get_company_settings('chitbooktype');
					$chitbookupdate = array(
							'status' => $this->Basefunctions->activestatus
					);
					if($ctime > $ltime) {
						$chitbookupdate = array_merge($chitbookupdate,$this->Crudmodel->nextdayupdatedefaultvalueget());
					} else {
						$chitbookupdate = array_merge($chitbookupdate,$this->Crudmodel->updatedefaultvalueget());
					}
					$cbno = explode("|",$sales_detail[$i]['chitbookno']);
					$this->db->where_in('chitbookno',$cbno);
					if($chitbooktypeid == 0) {
						$this->db->update('chitbook',$chitbookupdate);
					} else if($chitbooktypeid == 1) {
						$this->db->update('chitgeneration',$chitbookupdate);
					}
				}
			}
			if($appunt > 0){
				$this->appoutuntagupdate($approvaloutuntagsdid,'0',$salestransactiontype);
			}
			if($salestransactiontype == 11) {  // sales
			   $estimateno = $this->Basefunctions->singlefieldfetch('estimatenoarray','salesid','sales',$salesid);
			   if($estimateno != '' && $estimateno != '1') {
				   $estimateno = explode(',',$estimateno);
				   $activeestimateno = array('status'=>1);
				   $this->db->where_in('salesnumber',$estimateno);
				   $this->db->update('sales',$activeestimateno);
			   }
			}
			
			//audit-log
			$userid = $this->Basefunctions->logemployeeid;
			$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
			$salestransactiontypeid = $this->Basefunctions->singlefieldfetch('salestransactiontypeid','salesid','sales',$salesid);
			$salestransactiontypename = $this->Basefunctions->singlefieldfetch('salestransactiontypename','salestransactiontypeid','salestransactiontype',$salestransactiontypeid);
			$salesbillno = $this->Basefunctions->singlefieldfetch('salesnumber','salesid','sales',$salesid);
			$activity = ''.$user.' deleted '.$salestransactiontypename.' '.$salesbillno.'';
			$this->Basefunctions->notificationcontentadd($salesid,'Deleted',$activity ,$userid,$this->salesmoduleid);
			
			{//workflow management -- for data delete
				$this->Basefunctions->workflowmanageindatadeletemode($this->salesmoduleid,$salesid,'salesid','sales');
			}
			
			if($deletepurchasestatus == 0) {
				$delete = $this->Basefunctions->delete_log();
				//delete sales detail.
				$this->db->where('salesid',$salesid);
				$this->db->update('salesdetail',$delete);
				if(in_array($salestransactiontype,array(9,11,20,22,23,25,24))) {
					$this->accountledgerdelete($salesid);
				}
				
				//delete sales-main
				$this->db->where('salesid',$salesid);
				$this->db->update('sales',$delete);
				echo 'SUCCESS';
			} else{
				echo 'PURCHASESTOCKEXIST';
			}
		}
	}
	public function viewdynamicdatainfofetchbill($tablename,$sortcol,$sortord,$pagenum,$rowscount,$billstatus,$salesid) {
		$dataset ='sales.salesid,sales.salesdate,employee.employeename,salestransactiontype.salestransactiontypename,transactionmode.transactionmodename,account.accountname';
		$join=' LEFT OUTER JOIN employee ON employee.employeeid=sales.employeeid';
		$join.=' LEFT OUTER JOIN salestransactiontype ON salestransactiontype.salestransactiontypeid=sales.salestransactiontypeid';
		$join.=' LEFT OUTER JOIN transactionmode ON transactionmode.transactionmodeid=sales.transactionmodeid';
		$join.=' LEFT OUTER JOIN account ON account.accountid=sales.accountid';
		if($billstatus == 'no') {
		    $cuscondition = $tablename.'.salesid !='.$salesid.' AND '.$tablename.'.salesnumber="pending" AND ';
		} else if($billstatus == 'yes') {
			$cuscondition = $tablename.'.salesid !='.$salesid.' AND '.$tablename.'.salesnumber !="pending" AND ';
		} else {
			$cuscondition = '';
		}
		$status = 'sales.status='.$this->Basefunctions->activestatus.'';
	
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		/* query */
		$result = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$cuscondition.' '. $status.' ORDER BY '.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$data=array();
		$i=0;
		foreach($result->result() as $row) {
			$salesid = $row->salesid;
			$salesdate = $row->salesdate;
			$employeename = $row->employeename;
			$salestransactiontypename = $row->salestransactiontypename;
			$transactionmodename = $row->transactionmodename;
			$accountname = $row->accountname;
			$data[$i]=array('id'=>$salesid,$salesdate,$employeename,$salestransactiontypename,$transactionmodename,$accountname);
			$i++;
		}
		$finalresult=array('0'=>$data,'1'=>$page,'2'=>'json');
		return $finalresult;
	}
	// main bill reload mainedit
	public function billretrive(){
		$salesid = $_POST['primaryid'];
		$salesdetailcount = 0;
		$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$weightround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight 
		$taxround = 2;
		$liveadvanceamt = 0;
		$liveadvanceclose = 0;
		$finalamtreceipt = 0;
		$finalamtissue = 0;
		$finalweightreceipt = 0;
		$finalweightissue = 0;
		$weightissuereceiptid = 0 ; 
		$accledgerissuereceiptid = 0;
		$creditno = '';
		$salesdb = $this->db->select('
		sales.salesnumber,DATE_FORMAT(salesdate,"%d-%m-%Y") AS salesdate,sales.employeeid,sales.salestransactiontypeid,sales.transactionmodeid,sales.accountid,sales.cashcounterid,sales.balancecomment,DATE_FORMAT(balanceduedate,"%d-%m-%Y") AS balanceduedate,sales.printtemplatesid,purityid,purityrate,account.accountname,sales.summarytaxcategoryid,sales.discountcalculationtypeid,sales.discountvalue,sales.summarydiscountamount,sales.summaryroundwholevalue,sales.mainordernumberid,sales.summaryprediscount,sales.summarydiscountlimit,sales.discountempid,sales.summarygrossamount,ROUND(summarytaxamount,'.$taxround.') as summarytaxamount,sales.summaryoldjewelamount,sales.summarysalesretunamount,sales.creditno,sales.advanceno,sales.estimatenoarray,sales.mainstocktypeid,sales.issuereceiptid,account.mobilenumber,ROUND(summarypurewt,'.$weightround.') as summarypurewt,ROUND(summarydiscountamount,'.$amountround.') as summarydiscountamount,ROUND(summaryroundvalue,'.$amountround.') as summaryroundvalue,ROUND(orderadvanceamt,'.$amountround.') as orderadvanceamt,ROUND(advanceadjustamt,'.$amountround.') as advanceadjustamt,sales.paymentirtypeid,account.accounttypeid,sales.creditoverlayhidden,sales.creditoverlayvaluehidden,sales.credittotalamount,sales.credittotalpurewt,sales.loadbilltype,sales.billleveltaxautoremoval,sales.adjustmentamountkeypressed,sales.billrefno,sales.ratecuttypeid,ROUND(sales.purchasecheckgrwt,'.$weightround.') as purchasecheckgrwt,sales.summarysalesreturnroundvalue,sales.salesmetalid,sales.oldpurchasemetalid,sales.salesreturnmetalid,ROUND(summaryolddiscountamount,'.$amountround.') as summaryolddiscountamount',false)
			->from('sales')
			->join('account','account.accountid=sales.accountid','left outer')
			->join('counter','counter.counterid=sales.cashcounterid','left outer')
			->where('salesid',$salesid)
			->where_not_in('sales.status',array(0,3))
			->limit(1)
			->get()->row();
			$employeename = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$salesdb->employeeid);
			{// purity rate fetch
				$ckeyword=array();
				$ckeywordvalue=array();
				$keyworddetails=array();
				$keyworddata=array();
				if($salesdb->purityid != '' && $salesdb->purityrate != ''){
				$ckeyword = explode(',',$salesdb->purityid);
				$ckeywordvalue = explode(',',$salesdb->purityrate);
				$keyworddetails = array_combine($ckeyword, $ckeywordvalue);
				$j = 0;
				foreach($keyworddetails as $j => $l){
					$keyworddata[$j] = $l;
					$j++;
				}
					$keyword_str = $keyworddata;
				}else{
					$keyword_str ='';
				}
			}
				
				/* if($salesdb->salestransactiontypeid == 11){
					$salesdetailcount=$this->db->where('salesdetail.stocktypeid',75)->where('salesid',$salesdb->mainordernumberid)->where('status',$this->Basefunctions->activestatus)->get('salesdetail')->num_rows();
					if($salesdetailcount > 0)
					{// get live order advance amt based on credit no
						$this->db->select('ROUND(finalamtreceipt,'.$amountround.') as finalamtreceipt');
						$this->db->from('accountledger');
						$this->db->where('creditno',$salesdb->advanceno);
						$this->db->where('entrymode',4);
						$this->db->where_in('status',array(1,11)); 
						$result = $this->db->get();
						if($result->num_rows() > 0) {
							foreach($result->result() as $row) {
								$finalamtreceipt = $row->finalamtreceipt;
							}
						}else {
							$finalamtreceipt = 0;
							$liveadvanceclose = 1;
						}
						$liveadvanceamt = $salesdb->advanceadjustamt + $finalamtreceipt;
					}
				} */
				$salesjson = array(
					'salesnumber'=>  $salesdb->salesnumber,
					'salesdate'=>  $salesdb->salesdate,
					'employeeid'=>  $salesdb->employeeid,
					'employeename'=>  $employeename,
					'salespersonid'=>  $salesdb->employeeid,
					'salestransactiontypeid'=>  $salesdb->salestransactiontypeid,
					'transactionmodeid'=>$salesdb->transactionmodeid,
					'accountid'=>$salesdb->accountid,
					'accounttypeid'=>$salesdb->accounttypeid,
					'mobilenumber'=>$salesdb->mobilenumber,
					'cashcounter'=>$salesdb->cashcounterid,
					'printtemplateid'=>$salesdb->printtemplatesid,
					'balancedescription'=>$salesdb->balancecomment,
					'balanceduedate'=>$salesdb->balanceduedate,
					'purityid'=>$keyword_str,
					'salesdetailid'=>$this->Basefunctions->singlefieldfetch('salesdetailid','salesid','salesdetail',$salesid),
					'accountname'=>$salesdb->accountname,
					'taxcategory'=>$salesdb->summarytaxcategoryid,
					'discounttypeid'=>$salesdb->discountcalculationtypeid,
					'discountpercent'=>$salesdb->discountvalue,
					'singlediscounttotal'=>$salesdb->summarydiscountamount,
					'salesid'=>$salesid,
					'roundvalue'=>$salesdb->summaryroundvalue,
					'summarysalesreturnroundvalue'=>$salesdb->summarysalesreturnroundvalue,
					'roundwholevalue'=>$salesdb->summaryroundwholevalue,
					'mainordernumberid'=>$salesdb->mainordernumberid,
					'presumarydiscountamount'=>$salesdb->summaryprediscount,
					'billsumarydiscountlimit'=>$salesdb->summarydiscountlimit,
					'billdiscamt'=>$salesdb->summarydiscountlimit,
					'discountempid'=>$salesdb->discountempid,
					'estimatearray'=>$salesdb->estimatenoarray,
					'mainstocktypeid'=>$salesdb->mainstocktypeid,
					'summarytaxgriddata'=>$this->get_sales_taxentry_div($salesid,1),
					'issuereceiptid'=>$salesdb->issuereceiptid,
					'summarypurewt'=>$salesdb->summarypurewt,
					'sumarydiscountamount'=>$salesdb->summarydiscountamount,
					'orderadvamtsummaryhidden'=>$liveadvanceamt,
					'orderadvamtsummary'=>$salesdb->advanceadjustamt,
					'orderadvadjustamthidden'=>$salesdb->advanceadjustamt,
					'sumarytaxamount'=>$salesdb->summarytaxamount,
					'salesdetailcount'=>$salesdetailcount,
					'orderadvcreditno'=>$salesdb->advanceno,
					'liveadvanceclose'=>$liveadvanceclose,
					'paymentirtypeid'=>$salesdb->paymentirtypeid,
					'finalamtreceipt'=>$finalamtreceipt,
					'finalamtissue'=>$finalamtissue,
					'finalweightreceipt'=>$finalweightreceipt,
					'finalweightissue'=>$finalweightissue,
					'weightissuereceiptid'=>$weightissuereceiptid,
					'accledgerissuereceiptid'=>$accledgerissuereceiptid,
					'creditoverlayhidden'=>str_replace('zyx-0', '1', $salesdb->creditoverlayhidden),
					'creditoverlayvaluehidden'=>$salesdb->creditoverlayvaluehidden,
					'loadbilltype'=>$salesdb->loadbilltype,
					'credittotalamount'=>$salesdb->credittotalamount,
					'credittotalpurewt'=>$salesdb->credittotalpurewt,
					'billleveltaxautoremoval'=>$salesdb->billleveltaxautoremoval,
					'adjustmentamountkeypressed'=>$salesdb->adjustmentamountkeypressed,
					'billrefno'=>$salesdb->billrefno,		
					'salesmetalid'=>$salesdb->salesmetalid,
					'oldpurchasemetalid'=>$salesdb->oldpurchasemetalid,
					'salesreturnmetalid'=>$salesdb->salesreturnmetalid,
					'ratecuttypeid'=>$salesdb->ratecuttypeid,
					'creditno'=>$salesdb->creditno,
					'purchasecheckgrwt'=>$salesdb->purchasecheckgrwt,
					'summaryolddiscountamount'=>$salesdb->summaryolddiscountamount
				);
			echo json_encode($salesjson);
	}
	// main sales mainsave save
	public function salenumbergenerate() {
		$amountround =  $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount 
		$weightround =  $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-o
		$loclingtime = $this->Basefunctions->get_company_settings('lockingtime');
		$updatingtime = $this->Basefunctions->get_company_settings('updatingtime');
		$stateid = 2;
		if($_POST['accountstateid'] > 1){
			$stateid = $_POST['accountstateid'];
		}
		$cgst = 0.00;
		$salesdetailcgst = 0.00;
		$sgst = 0.00;
		$salesdetailsgst = 0.00;
		$igst = 0.00;
		$salesdetailigst = 0.00;
		$urdpurchasestatus = 0;
		$salesnumberarray = array();
		$salesserialnumber = 0;
		$oldpurchaseserialnumber = 0;
		$salesreturnserialnumber = 0;
		$urdpurchasenoarray = array();
		$urdpurchaseno = '';
		$urdsalesreturnnoarray = array();
		$urdsalesreturnno = '';
		$creditno = '';
		$modeid = '';
		$stocktypeid = '';
		$totalamount = '';
		$ratepergram = 0;
		$issuereceiptid = 1;
		$lotcreatestaus = 0;
		$currenttime = time("H:i:s");
		$ctime = new DateTime();
		$ctime->setTimestamp($currenttime);
		$ltime  = new DateTime();
		$stockgrossweight = '';
		$stockpieces = 0;
		$paymentaccountid = 1;
		$paymentreferencedate = '';
		$bonusamt = 0;
		$chitamount =0;
		$chitgram =0;	   
		$balproductid = 1;
		$processproductid = 1;
		$processcounter = 1;
		$precreditno = '';
		$preurdpurchaseno = '';
		$sales_detaildata = array();
		$paymentirtypeid = 1;
		$amtissue = 0;
		$amtreceipt = 0;
		$orderstatus = 0;
		$ordermodeid = 1;
		$poorderstatus = 1;
		$generatepo = 1;
		$manualstatus = $_POST['manualstatus'];
		$billrefno = $_POST['billrefno'];
		$ratecuttypeid = $_POST['ratecuttypeid']; 
		$ordernumbername = '';
		$oldpurchasegridcount = $_POST['gridcountdetailsofoldpurchase'];
		$salesreturngridcount = $_POST['gridcountdetailsofsalesreturn'];
		$delete = $this->Basefunctions->delete_log();
		
		$placeorderitemstatus = 1;
		// Take Order Status Update
		if($_POST['salestransactiontypeid'] == 20 && $_POST['mainstocktypeid'] == 75) {
			$placeorderitemstatus = 2;
		}
		
		// Default Old Purchase & Return Serial Masters
		$defoldpurserialmaster = $_POST['defoldpurserialmaster'];
		$defsalesreturnserialmaster = $_POST['defsalesreturnserialmaster'];
		
		// Metal Based restriction details variable datas
		/*** Sales Item Details ***/
		$billlevelrestrictmetalid = $_POST['billlevelrestrictmetalid'];
		$repairitemscount = $_POST['repairitemscount'];
		if(!empty($_POST['metalarraydetails'])) {
			$metalarraydetails = explode(',',$_POST['metalarraydetails']);
			if($repairitemscount > 0) {
				$metalserialnumberdetails = explode(',',$_POST['repairserialnumberdetails']);
			} else {
				$metalserialnumberdetails = explode(',',$_POST['metalserialnumberdetails']);
			}
		} else {
			$metalarraydetails = '';
			$metalserialnumberdetails = '';
		}
		// Common Template id for sales, old and return
		if(!empty($_POST['metalarraydetails']) || !empty($_POST['metaloldarraydetails']) || !empty($_POST['metalreturnarraydetails'])) {
			if($repairitemscount > 0) {
				$metalprinttempids = explode(',',$_POST['repairmetalprintid']);
			} else {
				$metalprinttempids = explode(',',$_POST['metalprinttempids']);
			}
		} else {
			$metalprinttempids = '';
		}
		/*** Old Item Details ***/
		$billlevelrestrictoldmetalid = $_POST['billlevelrestrictoldmetalid'];
		if(!empty($_POST['metaloldarraydetails'])) {
			$metaloldarraydetails = explode(',',$_POST['metaloldarraydetails']);
			$metaloldserialnumberdetails = explode(',',$_POST['metaloldserialnumberdetails']);
		} else {
			$metaloldarraydetails = '';
			$metaloldserialnumberdetails = '';
		}
		/*** Return Item Details ***/
		$billlevelrestrictreturnmetalid = $_POST['billlevelrestrictreturnmetalid'];
		if(!empty($_POST['metalreturnarraydetails'])) {
			$metalreturnarraydetails = explode(',',$_POST['metalreturnarraydetails']);
			$metalreturnserialnumberdetails = explode(',',$_POST['metalreturnserialnumberdetails']);
		} else {
			$metalreturnarraydetails = '';
			$metalreturnserialnumberdetails = '';
		}
		
		$summarysalesreturnroundvalue = 0;
		if(!empty($_POST['summarysalesreturnroundvalue'])) {
			$summarysalesreturnroundvalue = $_POST['summarysalesreturnroundvalue'];
		} else {
			$summarysalesreturnroundvalue = 0;
		}
		if($loclingtime == '00:00:00') {
			$ltime->setTimestamp($currenttime);
		} else {
			$ltime->setTimestamp(strtotime($loclingtime));
		}
		if($ctime > $ltime) {
			$salesdate = date("Y-m-d",strtotime("tomorrow"));
			$salesdate = $this->Basefunctions->checkgivendateisholidayornot($salesdate);
			$billdate = $salesdate.' '.$updatingtime;
			$defdatedataset = $this->Crudmodel->nextdaydefaultvalueget();
			$defupdatedataset = $this->Crudmodel->nextdayupdatedefaultvalueget();
			$updatedefaultvalueget = $defupdatedataset;
			$defaultvalueget = $defdatedataset;
		} else {
			$salesdate = $this->Basefunctions->ymd_format($_POST['salesdate']);
			$billdate = $this->Basefunctions->ymd_format($_POST['salesdate']).' '.date('H:i:s');
			$defdatedataset = $this->Crudmodel->defaultvalueget();
			$defupdatedataset = $this->Crudmodel->updatedefaultvalueget();
			$updatedefaultvalueget = $defupdatedataset;
			$defaultvalueget = $defdatedataset;
		}
		if(empty($_POST['loadbilltype'])) {
			$_POST['loadbilltype'] = '';
		} else {
			$_POST['loadbilltype'] = $_POST['loadbilltype'];
		}
		if(!isset($_POST['accountid']) || $_POST['accountid'] == 'null' || $_POST['accountid'] == '' || $_POST['accountid'] == '0') {
				$accountid=1;
		} else {
				$accountid=$_POST['accountid'];
		}
		if(!empty($_POST['discountformdata'])) {
			$discountdata = json_decode($_POST['discountformdata'], true);
			$discounttype = $discountdata['typeid'];
			$discountpercent = $discountdata['value'];
		} else {
			$discounttype = '1';
			$discountpercent = '';
		}
		
		{ //billlevel tax
			if(!in_array($_POST['salestransactiontypeid'],array(13,21,18,22,23))) {
				if($_POST['taxtypeid'] == 2 OR $_POST['taxtypeid'] == 3) {
					//$stateid = $this->Basefunctions->singlefieldfetch('stateid','accountid','account',$accountid); // state id
					$branchstateid = $this->Basefunctions->branchstateid; 
					if($stateid == $branchstateid) {  // branch state
						$_POST['gsttaxmodeid'] = 2;
					} else { // other state
						$_POST['gsttaxmodeid'] = 3;
					}
					if($_POST['gsttaxmodeid'] != '1' && $_POST['gsttaxmodeid'] != '') {
						if($_POST['gsttaxmodeid'] == '2') { // CGST/SGST
							if(!empty($_POST['summarytaxgriddata'])) {
								$salestaxdata = json_decode($_POST['summarytaxgriddata'], true);
								foreach($salestaxdata['data'] as $row) {
									if(substr($row['taxname'],0,4) == 'CGST') {
										$cgst = $cgst + $row['amount'];
									}
									if(substr($row['taxname'],0,4) == 'SGST') {
										$sgst = $sgst + $row['amount'];
									}
								}
							}
							$igst = '0';
						} else if($_POST['gsttaxmodeid'] == '3') { // IGST
							$cgst = '0';
							$sgst = '0';
							$igst = number_format((float)$_POST['sumarytaxamount'], 2, '.', '');
						}
					}
				}
			}
		}
		if(empty($_POST['prepaymenttotalamount'])) {
			$_POST['prepaymenttotalamount'] = 0;
		}
		if(empty($_POST['generatesalesid'])) {
			// Separate Sales Number for sales, Old Purchase, Sales Return
			if($_POST['salestransactiontypeid'] == 11) {
				if($_POST['gridcountdetailsofsalestrantype'] > 0) {
					if($billlevelrestrictmetalid != 1) {
						$metalspecificserialnumbermaster = $metalserialnumberdetails[array_search($billlevelrestrictmetalid,$metalarraydetails)];
						if($repairitemscount > 0) {
							$salesnumberarray = $this->Basefunctions->salesvarrandomnumbergenerator(52,'salesnumber','2',$metalspecificserialnumbermaster,$salesdate);
						} else {
							$salesnumberarray = $this->Basefunctions->salesvarrandomnumbergenerator(52,'salesnumber',$_POST['salestransactiontypeid'],$metalspecificserialnumbermaster,$salesdate);
						}
						$_POST['printtemplateid'] = $metalprinttempids[array_search($billlevelrestrictmetalid,$metalarraydetails)];
					} else {
						$salesnumberarray = $this->Basefunctions->salesvarrandomnumbergenerator(52,'salesnumber',$_POST['salestransactiontypeid'],$_POST['serialnumbermastername'],$salesdate);
						
					}
					$salesnumber = $salesnumberarray[0];
					$salesserialnumber = $salesnumberarray[1];
					
					if($oldpurchasegridcount > 0) {
						if($billlevelrestrictoldmetalid != 1) {
							$metaloldspeificserialnomaster = $metaloldserialnumberdetails[array_search($billlevelrestrictoldmetalid,$metaloldarraydetails)];
							$urdpurchasenoarray = $this->Basefunctions->salesvarrandomnumbergenerator(52,$metaloldspeificserialnomaster,'2',$metaloldspeificserialnomaster,$salesdate);
						} else {
							$urdpurchasenoarray = $this->Basefunctions->salesvarrandomnumbergenerator(52,$defoldpurserialmaster,'2',$defoldpurserialmaster,$salesdate);
						}
						$urdpurchaseno = $urdpurchasenoarray[0];
						$oldpurchaseserialnumber = $urdpurchasenoarray[1];
					}
					
					if($salesreturngridcount > 0) {
						if($billlevelrestrictreturnmetalid != 1) {
							$metalreturnspecificserialnomaster = $metalreturnserialnumberdetails[array_search($billlevelrestrictreturnmetalid,$metalreturnarraydetails)];
							$urdsalesreturnnoarray = $this->Basefunctions->salesvarrandomnumbergenerator(52,$metalreturnspecificserialnomaster,'2',$metalreturnspecificserialnomaster,$salesdate);
						} else {
							$urdsalesreturnnoarray = $this->Basefunctions->salesvarrandomnumbergenerator(52,$defsalesreturnserialmaster,'2',$defsalesreturnserialmaster,$salesdate);
						}
						$urdsalesreturnno = $urdsalesreturnnoarray[0];
						$salesreturnserialnumber = $urdsalesreturnnoarray[1];
					}
					
				} else if($_POST['gridcountdetailsofsalestrantype'] == 0) {
					if($oldpurchasegridcount > 0) {
						if($billlevelrestrictoldmetalid != 1) {
							$metaloldspeificserialnomaster = $metaloldserialnumberdetails[array_search($billlevelrestrictoldmetalid,$metaloldarraydetails)];
							$urdpurchasenoarray = $this->Basefunctions->salesvarrandomnumbergenerator(52,$metaloldspeificserialnomaster,'2',$metaloldspeificserialnomaster,$salesdate);
							$_POST['printtemplateid'] = $metalprinttempids[array_search($billlevelrestrictoldmetalid,$metaloldarraydetails)];
						} else {
							$urdpurchasenoarray = $this->Basefunctions->salesvarrandomnumbergenerator(52,$defoldpurserialmaster,'2',$defoldpurserialmaster,$salesdate);
							
						}
						$urdpurchaseno = $urdpurchasenoarray[0];
						$oldpurchaseserialnumber = $urdpurchasenoarray[1];
					}
					if($salesreturngridcount > 0) {
						if($billlevelrestrictreturnmetalid != 1) {
							$metalreturnspecificserialnomaster = $metalreturnserialnumberdetails[array_search($billlevelrestrictreturnmetalid,$metalreturnarraydetails)];
							$urdsalesreturnnoarray = $this->Basefunctions->salesvarrandomnumbergenerator(52,$metalreturnspecificserialnomaster,'2',$metalreturnspecificserialnomaster,$salesdate);
							$_POST['printtemplateid'] = $metalprinttempids[array_search($billlevelrestrictreturnmetalid,$metalreturnarraydetails)];
						} else {
							$urdsalesreturnnoarray = $this->Basefunctions->salesvarrandomnumbergenerator(52,$defsalesreturnserialmaster,'2',$defsalesreturnserialmaster,$salesdate);
						}
						$urdsalesreturnno = $urdsalesreturnnoarray[0];
						$salesreturnserialnumber = $urdsalesreturnnoarray[1];
					}
					if($oldpurchasegridcount > 0 && $salesreturngridcount > 0) {
						$salesnumber = $urdpurchaseno;
					} else if($oldpurchasegridcount > 0 && $salesreturngridcount == 0) {
						$salesnumber = $urdpurchaseno;
					} else if($oldpurchasegridcount == 0 && $salesreturngridcount > 0) {
						$salesnumber = $urdsalesreturnno;
					} else {
						$salesnumberarray = $this->Basefunctions->salesvarrandomnumbergenerator(52,'salesnumber',$_POST['salestransactiontypeid'],$_POST['serialnumbermastername'],$salesdate);
						$salesnumber = $salesnumberarray[0];
						$salesserialnumber = $salesnumberarray[1];
					}
				}
			} else if($_POST['salestransactiontypeid'] == 22 || $_POST['salestransactiontypeid'] == 23) {
				$accounttypecheck = $this->Basefunctions->singlefieldfetch('accounttypeid','accountid','account',$accountid);
				$chkcustomerwise = $this->Basefunctions->get_company_settings('customerwise');
				$chkvendorwise = $this->Basefunctions->get_company_settings('vendorwise');
				$accstatus = 0;
				if($accounttypecheck == 16 && $chkvendorwise == 1 || $accounttypecheck == 6 && $chkcustomerwise == 1) {															
					$accstatus = 1;
				} else {
					$accstatus = 0;
				}
				if($_POST['salestransactiontypeid'] == 23) {
					if($_POST['advanceserialno'] != '' && $_POST['advanceorbalance'] == '2' && $_POST['creditno'] == '') {
						$salesnumberarray = $this->Basefunctions->serialnumbergeneratevardaan(52,'salesnumber','',$_POST['advanceserialno'],$salesdate);
						$salesnumber = $salesnumberarray[0];
						$salesserialnumber = $salesnumberarray[1];
						$_POST['printtemplateid'] = $_POST['advanceprintid'];
					} else if($_POST['receiptserialno'] != '' && $_POST['advanceorbalance'] == '3' && $_POST['creditno'] != '') {
						$salesnumberarray = $this->Basefunctions->serialnumbergeneratevardaan(52,'salesnumber','',$_POST['receiptserialno'],$salesdate);
						$salesnumber = $salesnumberarray[0];
						$salesserialnumber = $salesnumberarray[1];
						$_POST['printtemplateid'] = $_POST['receiptprintid'];
					} else {
						$salesnumberarray = $this->Basefunctions->salesvarrandomnumbergenerator(52,'salesnumber',$_POST['salestransactiontypeid'],$_POST['serialnumbermastername'],$salesdate);
						$salesnumber = $salesnumberarray[0];
						$salesserialnumber = $salesnumberarray[1];
					}
				} else if($_POST['salestransactiontypeid'] == 22) {
					if($_POST['receiptserialno'] != '' && $_POST['advanceorbalance'] == '3' && $_POST['creditno'] == '') {
						$salesnumberarray = $this->Basefunctions->serialnumbergeneratevardaan(52,'salesnumber','',$_POST['receiptserialno'],$salesdate);
						$salesnumber = $salesnumberarray[0];
						$salesserialnumber = $salesnumberarray[1];
						$_POST['printtemplateid'] = $_POST['receiptprintid'];
					} else if($_POST['issueserialno'] != '' && $_POST['advanceorbalance'] == '2' && $_POST['creditno'] != '') {
						$salesnumberarray = $this->Basefunctions->serialnumbergeneratevardaan(52,'salesnumber','',$_POST['issueserialno'],$salesdate);
						$salesnumber = $salesnumberarray[0];
						$salesserialnumber = $salesnumberarray[1];
						$_POST['printtemplateid'] = $_POST['issueprintid'];
					} else {
						$salesnumberarray = $this->Basefunctions->salesvarrandomnumbergenerator(52,'salesnumber',$_POST['salestransactiontypeid'],$_POST['serialnumbermastername'],$salesdate);
						$salesnumber = $salesnumberarray[0];
						$salesserialnumber = $salesnumberarray[1];
					}
				}
				if(empty($_POST['creditno']) && $accstatus == 1) {
					$_POST['creditno'] = $salesnumber;
					$_POST['creditoverlayhidden'] = $this->crosscheckcreditnoisblank($_POST['creditoverlayvaluehidden'],$salesnumber);
					$_POST['creditoverlayvaluehidden'] = $this->crosscheckcreditnoisblankjson($_POST['creditoverlayvaluehidden'],$salesnumber);
				}
			} else {
				$salesnumberarray = $this->Basefunctions->salesvarrandomnumbergenerator(52,'salesnumber',$_POST['salestransactiontypeid'],$_POST['serialnumbermastername'],$salesdate);
				$salesnumber = $salesnumberarray[0];
				$salesserialnumber = $salesnumberarray[1];
			}
			$header = array(
				'salesdate' => $salesdate,
				'billdate' => $billdate,
				'employeeid' => $_POST['salespersonid'],
				'printtemplatesid'=>$_POST['printtemplateid'],
				'salesnumber' =>$salesnumber,
				'salestransactiontypeid'=>$_POST['salestransactiontypeid'],
				'transactionmodeid'=>$_POST['transactionmodeid'],
				'accountid'=>$accountid,
				'cashcounterid'=>$_POST['cashcounter'],
				'purityid'=>$_POST['purityid'],
				'purityrate'=>$_POST['purityrate'],
				'summarydiscountlimit'=>$_POST['sumarydiscountlimit'],
				'summaryprediscount'=>$_POST['presumarydiscountamount'],
				'presumtaxamount'=>$_POST['sumarytaxamount'],
				'presumdiscountamount'=>$_POST['sumarydiscountamount'],
				'presumnetamount'=>$_POST['sumarynetamount'],
				'summarytaxcategoryid'=>$_POST['taxcategoryid'],
				'discountcalculationtypeid'=>$_POST['sdiscounttypeid'],
				'discountvalue'=>$discountpercent,
				'summaryitemscount'=>$_POST['summaryitemscount'],
				'summarynetamount'=>abs($_POST['billnetamount']),
				'summarynetpurewt'=>abs($_POST['billnetwt']),
				'summarynetamountwithsign'=>$_POST['billnetamountwithsign'],
				'summarynetpurewtwithsign'=>$_POST['billnetwtwithsign'],
				'summarydiscountamount'=>$_POST['sumarydiscountamount'],
				'summaryolddiscountamount'=>$_POST['summaryolddiscountamount'],
				'summarytaxamount'=>$_POST['sumarytaxamount'],
				'summaryproditemrate'=>$_POST['summaryproditemrate'],
				'summarycaratweight'=>$_POST['summarylstchargecaratwt'],
				'taxtype'=>$_POST['taxtypeid'],
				'discounttype'=>$_POST['sdiscounttypeid'],
				'salescgst'=>$cgst,
				'salessgst'=>$sgst,
				'salesigst'=>$igst,
				'estimatenoarray'=>$_POST['estimatearray'],
				'industryid'=>$this->Basefunctions->industryid,
				'branchid'=>$this->Basefunctions->branchid,
				'balancecomment'=>$_POST['balancedescription'],
				'mainstocktypeid'=>$_POST['mainstocktypeid'],
				'summaryroundvalue'=>$_POST['roundvalue'],
				'summarysalesreturnroundvalue'=>$summarysalesreturnroundvalue,
				'issuereceiptid'=>$_POST['issuereceiptid'],
				'wtissuereceiptid'=>$_POST['wtissuereceiptid'],
				'mainordernumberid'=>$_POST['mainordernumberid'],
				'orderadvanceamt'=>$_POST['orderadvanceamt'],
				'advanceadjustamt'=>$_POST['orderadvanceadjustamt'],
				'advanceno'=>$_POST['orderadvanceno'],
				'paymentirtypeid'=>$_POST['paymentirtypeid'],
				'creditno'=>$_POST['creditno'],
				'creditoverlayhidden'=>$_POST['creditoverlayhidden'],
				'creditoverlayvaluehidden'=>$_POST['creditoverlayvaluehidden'],
				'loadbilltype'=>$_POST['loadbilltype'],
				'credittotalamount'=>$_POST['credittotalamount'],
				'credittotalpurewt'=>$_POST['credittotalpurewt'],
				'billleveltaxautoremoval'=>$_POST['billleveltaxautoremoval'],
				'adjustmentamountkeypressed'=>$_POST['adjustmentamountkeypressed'],
				'billrefno'=>$_POST['billrefno'],
				'urdpurchaseno'=>$urdpurchaseno,
				'urdsalesreturnno'=>$urdsalesreturnno,
				'salesserialnumber'=>$salesserialnumber,
				'oldpurchaseserialnumber'=>$oldpurchaseserialnumber,
				'salesreturnserialnumber'=>$salesreturnserialnumber,
				'salesmetalid'=>$billlevelrestrictmetalid,
				'oldpurchasemetalid'=>$billlevelrestrictoldmetalid,
				'salesreturnmetalid'=>$billlevelrestrictreturnmetalid,
				'ratecuttypeid'=>$_POST['ratecuttypeid'],
				'purchasecheckgrwt'=>$_POST['purchasewtcheckgrwt'],
				'purchasecheckerrorwt'=>$_POST['purchasewtcheckerrorwt'],
				'advanceorbalance'=>$_POST['advanceorbalance'],
				'placeorderitemstatus'=>$placeorderitemstatus,
				'oldpurchasecredittype'=>$_POST['oldpurchasecredittype']
			);
			$header_insert = array_merge($header,$defdatedataset);
			$this->db->insert('sales',array_filter($header_insert));
			$salesid = $this->db->insert_id();
			//workflow management -- for data create
			{
				$this->Basefunctions->workflowmanageindatacreatemode($this->salesmoduleid,$salesid,'sales');
			}
		} else {
			$salesid = $_POST['generatesalesid'];
			//fetch old values -- work flow
			{
				$condstatvals=$this->Basefunctions->workflowmanageolddatainfofetch($this->salesmoduleid,$salesid);
			}
			$salesnumber = $this->singlefieldfetch('salesnumber','salesid','sales',$salesid);
			$urdpurchaseno = $this->singlefieldfetch('urdpurchaseno','salesid','sales',$salesid);
			$urdsalesreturnno = $this->singlefieldfetch('urdsalesreturnno','salesid','sales',$salesid);
			$header = array(
				'accountid'=>$accountid,
				'salesdate' => $salesdate,
				'billdate' => $billdate,
				'employeeid' => $_POST['salespersonid'],
				'printtemplatesid'=>$_POST['printtemplateid'],
				'cashcounterid'=>$_POST['cashcounter'],
				'summarydiscountlimit'=>$_POST['sumarydiscountlimit'],
				'summaryprediscount'=>$_POST['presumarydiscountamount'],
				'presumtaxamount'=>$_POST['sumarytaxamount'],
				'presumdiscountamount'=>$_POST['sumarydiscountamount'],
				'presumnetamount'=>$_POST['sumarynetamount'],
				'summarytaxcategoryid'=>$_POST['taxcategoryid'],
				//'discountcalculationtypeid'=>$discounttype,
				'discountvalue'=>$discountpercent,
				'summaryitemscount'=>$_POST['summaryitemscount'],
				'summarynetamount'=>abs($_POST['billnetamount']),
				'summarynetpurewt'=>abs($_POST['billnetwt']),
				'summarynetamountwithsign'=>$_POST['billnetamountwithsign'],
				'summarynetpurewtwithsign'=>$_POST['billnetwtwithsign'],
				'summarydiscountamount'=>$_POST['sumarydiscountamount'],
				'summaryolddiscountamount'=>$_POST['summaryolddiscountamount'],
				'summarytaxamount'=>$_POST['sumarytaxamount'],
				'summaryproditemrate'=>$_POST['summaryproditemrate'],
				'summarycaratweight'=>$_POST['summarylstchargecaratwt'],
				//'taxtype'=>$_POST['taxtypeid'],
				'creditno'=>$_POST['creditno'],
				'salescgst'=>$cgst,
				'salessgst'=>$sgst,
				'salesigst'=>$igst,
				'estimatenoarray'=>$_POST['estimatearray'],
				'industryid'=>$this->Basefunctions->industryid,
				'balancecomment'=>$_POST['balancedescription'],
				'mainstocktypeid'=>$_POST['mainstocktypeid'],
				'issuereceiptid'=>$_POST['issuereceiptid'],
				'wtissuereceiptid'=>$_POST['wtissuereceiptid'],
				'summaryroundvalue'=>$_POST['roundvalue'],
				'summarysalesreturnroundvalue'=>$summarysalesreturnroundvalue,
				'orderadvanceamt'=>$_POST['orderadvanceamt'],
				'advanceadjustamt'=>$_POST['orderadvanceadjustamt'],
				'advanceno'=>$_POST['orderadvanceno'],
				'creditoverlayhidden'=>$_POST['creditoverlayhidden'],
				'creditoverlayvaluehidden'=>$_POST['creditoverlayvaluehidden'],
				'credittotalamount'=>$_POST['credittotalamount'],
				'credittotalpurewt'=>$_POST['credittotalpurewt'],
				'billleveltaxautoremoval'=>$_POST['billleveltaxautoremoval'],
				'adjustmentamountkeypressed'=>$_POST['adjustmentamountkeypressed'],
				'billrefno'=>$_POST['billrefno'],
				'urdpurchaseno'=>$urdpurchaseno,
				'urdsalesreturnno'=>$urdsalesreturnno,
				'salesmetalid'=>$billlevelrestrictmetalid,
				'oldpurchasemetalid'=>$billlevelrestrictoldmetalid,
				'salesreturnmetalid'=>$billlevelrestrictreturnmetalid,
				'ratecuttypeid'=>$_POST['ratecuttypeid'],
				'oldpurchasecredittype'=>$_POST['oldpurchasecredittype']
			);
			$header_insert = array_merge($header,$defupdatedataset);
			$this->db->where('salesid',$salesid);
			$this->db->update('sales',$header_insert);
			
			//workflow management for data update
			{
				$this->Basefunctions->workflowmanageindataupdatemode($this->salesmoduleid,$salesid,'sales',$condstatvals);
			}
			
			$salesdetaildata = $this->db->select('salesdetailid,stocktypeid,itemtagid,presalesdetailid,chitbookno')
				->from('salesdetail')
				->where('salesdetail.salesid',$salesid)
				->where('salesdetail.status',$this->Basefunctions->activestatus)
				->get();
			foreach($salesdetaildata->result() as $info){
				$sales_detaildata[] = array(
					'stocktypeid'=>$info->stocktypeid,
					'salesdetailid'=>$info->salesdetailid,
					'itemtagid'=>$info->itemtagid,
					'presalesdetailid'=>$info->presalesdetailid,
					'chitbookno'=>$info->chitbookno
				);
			}
			$appunt = 0;
			$approvaloutuntagsdid = array();
			
			//delete salesdetail id
			for($i=0; $i < count($sales_detaildata) ;$i++) {
				if($_POST['salestransactiontypeid'] == 11) {
					if($sales_detaildata[$i]['stocktypeid'] == 11 OR $sales_detaildata[$i]['stocktypeid'] == 13 OR $sales_detaildata[$i]['stocktypeid'] == 74 OR $sales_detaildata[$i]['stocktypeid'] == 83) {
						if($sales_detaildata[$i]['stocktypeid'] == 74) {
							$prestatus = $this->Basefunctions->approvaloutstatus;
							$updatesalesdetail = array(
							 'approvalstatus'=>1
							);
							$updatesalesdetail = array_merge($updatesalesdetail,$defupdatedataset);
							$this->db->where('salesdetailid',$sales_detaildata[$i]['presalesdetailid']);
							$this->db->update('salesdetail',$updatesalesdetail);
						} else {
							$prestatus = $this->Basefunctions->activestatus;
						}
						$update = array(
							'status'=>$prestatus,
						);
						$update=array_merge($update,$defupdatedataset);
						$this->db->where('itemtagid',$sales_detaildata[$i]['itemtagid']);
						$this->db->update('itemtag',$update);
					}
					$this->Basefunctions->stockupdateondelete($sales_detaildata[$i]['salesdetailid'],'52');
					if($sales_detaildata[$i]['stocktypeid']== '85') {
						$this->revertrepairstatus(4,5,$sales_detaildata[$i]['presalesdetailid']);
						$presalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$sales_detaildata[$i]['presalesdetailid']);
						$this->revertrepairstatus(4,5,$presalesdetailid);
						$prepresalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$presalesdetailid);
						$this->revertrepairstatus(4,5,$prepresalesdetailid);
					}
				}
				if($_POST['salestransactiontypeid'] == 18) {
					if($sales_detaildata[$i]['stocktypeid'] == 62) { //approval out tag
						if(!empty($sales_detaildata[$i]['itemtagid'])) {
							$update = array(
								'status'=>$this->Basefunctions->activestatus,
								'salesid'=>'1',
								'salesdetailid'=>'1'
							);
							
							$update=array_merge($update,$defupdatedataset);
							$this->db->where('itemtagid',$sales_detaildata[$i]['itemtagid']);
							$this->db->update('itemtag',$update);
						}
					}
					if($sales_detaildata[$i]['stocktypeid'] == 65) //approval out return tag
					{
						if(!empty($sales_detaildata[$i]['itemtagid'])) {
							$update = array(
									'status'=>$this->Basefunctions->approvaloutstatus
							);
							$update = array_merge($update,$defupdatedataset);
							$this->db->where('itemtagid',$sales_detaildata[$i]['itemtagid']);
							$this->db->update('itemtag',$update);
						}
						$updatesalesdetail = array(
								'approvalstatus'=>1
						);
						$updatesalesdetail = array_merge($updatesalesdetail,$defupdatedataset);
						$this->db->where('salesdetailid',$sales_detaildata[$i]['presalesdetailid']);
						$this->db->update('salesdetail',$updatesalesdetail);
					}
				}
				// delete lot for purchase
				if($_POST['salestransactiontypeid'] == 9) {
					if(in_array($sales_detaildata[$i]['stocktypeid'],array(17,80,81)))
					{
						$lotupdatesalesdetail = array(
								'status'=>0
						);
						$updatesalesdetail = array_merge($lotupdatesalesdetail,$defupdatedataset);
						$this->db->where('salesdetailid',$sales_detaildata[$i]['salesdetailid']);
						$this->db->update('lot',$lotupdatesalesdetail);
					}
				}
				//reverting order status
				if($_POST['salestransactiontypeid'] == '21') { // place order
					$this->revertorderstatus(2,3,$sales_detaildata[$i]['presalesdetailid']);
					$presalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$sales_detaildata[$i]['presalesdetailid']);
					$this->revertorderstatus(2,3,$presalesdetailid);
				} else if($_POST['salestransactiontypeid'] == '29') { // Place Repair
					$this->revertrepairstatus(2,3,$sales_detaildata[$i]['presalesdetailid']);
					$presalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$sales_detaildata[$i]['presalesdetailid']);
					$this->revertrepairstatus(2,3,$presalesdetailid);
				} else if($sales_detaildata[$i]['stocktypeid']== '82') { // Receive - take order
					if($_POST['receiveorderconcept'] == '0') { // revert itemtag entry
						$this->revertorderstatus(3,5,$sales_detaildata[$i]['presalesdetailid']);
						$presalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$sales_detaildata[$i]['presalesdetailid']);
						$this->revertorderstatus(3,5,$presalesdetailid);
						$povendormanagementconcept = $this->Basefunctions->get_company_settings('povendormanagement');
						if($povendormanagementconcept == 1) {
							$topresalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$presalesdetailid);
							$this->revertorderstatus(3,5,$topresalesdetailid);
						}
						//$this->Basefunctions->stockupdateondelete($sales_detaildata[$i]['itemtagid'],'50');
						$updatestock=array(
							'ordernumber'=>'',
							'salesdetailid'=>1,
							'orderstatus'=>'No'
							);
						$updatestock=array_merge($updatestock,$this->Crudmodel->updatedefaultvalueget());
						$this->db->where('itemtagid',$sales_detaildata[$i]['itemtagid']);
						$this->db->update('itemtag',$updatestock);
					} else { //Lot based creation revert
						if(in_array($sales_detaildata[$i]['stocktypeid'],array(82))) {
							$this->revertorderstatus(3,5,$sales_detaildata[$i]['presalesdetailid']);
							$presalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$sales_detaildata[$i]['presalesdetailid']);
							$this->revertorderstatus(3,5,$presalesdetailid);
							$povendormanagementconcept = $this->Basefunctions->get_company_settings('povendormanagement');
							if($povendormanagementconcept == 1) {
								$topresalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$presalesdetailid);
								$this->revertorderstatus(3,5,$topresalesdetailid);
							}
							$lotupdatesalesdetail = array(
									'status'=>0
							);
							$updatesalesdetail = array_merge($lotupdatesalesdetail,$defupdatedataset);
							$this->db->where('salesdetailid',$presalesdetailid);
							$this->db->update('lot',$lotupdatesalesdetail);
						}
					}
				} else if($_POST['salestransactiontypeid'] == '30') { // Receive Repair
					$this->revertrepairstatus(3,4,$sales_detaildata[$i]['presalesdetailid']);
					$presalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$sales_detaildata[$i]['presalesdetailid']);
					$this->revertrepairstatus(3,4,$presalesdetailid);
				} else if($sales_detaildata[$i]['stocktypeid']== '83' AND $_POST['salestransactiontypeid'] == '11') { // Order Tag
					$this->revertorderstatus(5,4,$sales_detaildata[$i]['presalesdetailid']);
					$presalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$sales_detaildata[$i]['presalesdetailid']);
					$this->revertorderstatus(5,4,$presalesdetailid);
					$ordersalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$presalesdetailid);
					$this->revertorderstatus(5,4,$ordersalesdetailid);
				} else if($sales_detaildata[$i]['stocktypeid']== '85') { // Generate PO order
					// updating place order salesdetail table
					$update_takeorderpostatus = array('generatepo'=>'1');
					$this->db->where('salesdetailid',$sales_detaildata[$i]['presalesdetailid']);
					$this->db->update('salesdetail',array_filter($update_takeorderpostatus));
				}
				{ // approval out untag
					if($sales_detaildata[$i]['stocktypeid'] == '16' || $sales_detaildata[$i]['stocktypeid'] == '66' || $sales_detaildata[$i]['stocktypeid'] == '25') { 
						$approvaloutuntagsdid[$appunt] = $sales_detaildata[$i]['presalesdetailid'];
						$appunt++;
					}
				}
				{//chitbook delete
					if($sales_detaildata[$i]['stocktypeid'] == 39) {
						$chitbooktypeid = $this->Basefunctions->get_company_settings('chitbooktype');
						$chitbookupdate = array(
								'status' => $this->Basefunctions->activestatus
						);
						$chitbookupdate = array_merge($chitbookupdate,$defupdatedataset);
						$cbno = explode("|",$sales_detaildata[$i]['chitbookno']);
						$this->db->where_in('chitbookno',$cbno);
						if($chitbooktypeid == 0) {
							$this->db->update('chitbook',$chitbookupdate);
						} else if($chitbooktypeid == 1) {
							$this->db->update('chitgeneration',$chitbookupdate);
						}
					}
				}		
			}
			if($appunt > 0) {
				$this->appoutuntagupdate($approvaloutuntagsdid,'0',$_POST['salestransactiontypeid']);
			}
			// delete sales detail
			$this->db->where('salesid',$salesid);
			$this->db->update('salesdetail',$delete);
			// delete salesdetail tax
			$this->db->where('salesid',$salesid);
			$this->db->update('salesdetailtax',$delete);
			// Delete questionairedetails table
			$this->db->where('salesid',$salesid);
			$this->db->update('questionairedetails',$delete);
			// delete sales stone entry
			$this->db->where('salesid',$salesid);
			$this->db->update('salesstoneentry',$delete);
			if(in_array($_POST['salestransactiontypeid'],array(9,11,20,22,23,25,24))) {
				$this->accountledgerdelete($salesid);
			}
		}
		{// Credit No Details Delete for Sales Details and also for Salesdetail Table
			$this->db->where('salesid',$salesid);
			$this->db->update('creditnodetails',$delete);
			// Insert Sales Table Credit No Overlay Details in creditnodetails table
			if(!empty($_POST['creditoverlayvaluehidden'])) {
				$salescreditnodetails = json_decode($_POST['creditoverlayvaluehidden'], true);
				foreach($salescreditnodetails as $row) {
					$salescreditnodetailsinsertdata = array(
						'salesid'=>$salesid,
						'salesdetailid'=>'1',
						'overlaycreditno'=>$row['overlaycreditno'],
						'creditsalesdate'=>$this->Basefunctions->ymd_format($row['creditsalesdate']),
						'paymentirtypeid'=>$row['paymentirtypeid'],
						'creditissuereceiptname'=>$row['creditissuereceiptname'],
						'amtissue'=>$row['amtissue'],
						'amtreceipt'=>$row['amtreceipt'],
						'finalamtissue'=>$row['finalamtissue'],
						'finalamtreceipt'=>$row['finalamtreceipt'],
						'creditissuereceiptid'=>$row['creditissuereceiptid'],
						'creditpaymentirtypeid'=>$row['creditpaymentirtypeid'],
						'creditweightissuereceiptname'=>$row['creditweightissuereceiptname'],
						'weightissue'=>$row['weightissue'],
						'weightreceipt'=>$row['weightreceipt'],
						'finalweightissue'=>$row['finalweightissue'],
						'finalweightreceipt'=>$row['finalweightreceipt'],
						'creditweightissuereceiptid'=>$row['creditweightissuereceiptid'],
						'accountledgerid'=>$row['accountledgerid'],
						'processid'=>$row['processid'],
						'process'=>$row['process'],
						'creditreference'=>$row['creditreference'],
						'creditreferencename'=>$row['creditreferencename'],
						'finalamthidden'=>$row['finalamthidden'],
						'finalwthidden'=>$row['finalwthidden'],
						'mainfinalamthidden'=>$row['mainfinalamthidden'],
						'mainfinalwthidden'=>$row['mainfinalwthidden'],
						'editoverlaystatus'=>$row['editoverlaystatus']
					);
					$salescreditnodetailsinsertdata = array_merge($salescreditnodetailsinsertdata,$defdatedataset);
					$this->db->insert('creditnodetails',array_filter($salescreditnodetailsinsertdata));
				}
			}
		}
		
		{// updating bill no with customer bill no, for acc ledger insert and lot automatic billnumber col
			$newsalesbillnumber = $salesnumber;
			if(!empty($billrefno)) { $newsalesbillnumber = $billrefno; }
		}
		$girddata = $_POST['salesdetailgriddata'];
		$cricount = $_POST['count'];
		$girddatainfo = json_decode($girddata,true);
		$checkforrecvorder = '0'; // for restricting acc ledger insertion
		$appunt = 0;
		$approvaloutuntagsdid = array();
		for($i=0;$i<=($cricount-1);$i++) {
			$salesdetailcgst = 0;
			$salesdetailsgst = 0;
			$salesdetailigst = 0;
			if($girddatainfo[$i]['paymentmodeid'] == 3 ) { // for payment
			    $stocktypeid = $girddatainfo[$i]['paymentstocktypeid'];
				$totalamount = $girddatainfo[$i]['paymenttotalamount'];
				$modeid = $girddatainfo[$i]['paymentmodeid'];
				$issuereceiptid = $girddatainfo[$i]['issuereceipttypeid'];
				$paymentaccountid = $girddatainfo[$i]['paymentaccountid'];
				$paymentreferencedate = $this->Basefunctions->ymd_format($girddatainfo[$i]['paymentreferencedate']);
				$wastagespanlabel ='';
				$makingchargespanlabel ='';
				$wastagekeylabel ='';
				$makingchargekeylabel ='';
				$paymenttouch = $girddatainfo[$i]['newpaymenttouch'];
				$pureweight = $girddatainfo[$i]['paymentpureweight'];
				$product = $girddatainfo[$i]['paymentproduct'];
				$purity = $girddatainfo[$i]['paymentpurity'];
				$counter = $girddatainfo[$i]['paymentcounter'];
				$grossweight = $girddatainfo[$i]['paymentgrossweight'];
				$netweight = $girddatainfo[$i]['paymentnetweight'];
				$pieces = $girddatainfo[$i]['paymentpieces'];
				$dustweight = $girddatainfo[$i]['paymentlessweight'];
				$ratepergram = $girddatainfo[$i]['paymentratepergram'];
				$employeepersonid = $girddatainfo[$i]['paymentemployeepersonid'];
				$comment = $girddatainfo[$i]['paymentcomment'];
				$orderstatus = 1;
				$ordermodeid = 1;
				$repairstatus = 1;
				
			} else if($girddatainfo[$i]['modeid'] == 2 ) { // for transaction
				$stocktypeid = $girddatainfo[$i]['stocktypeid'];
				$tagsalesdetailid = '';
				$tagstatus = '';
				if($stocktypeid == 11) {
					$this->db->select('itemtag.status,itemtag.salesdetailid');
					$this->db->from('itemtag');
					$this->db->where('itemtag.itemtagid',$girddatainfo[$i]['itemtagid']);
					$this->db->limit(1);
					$qryinfo = $this->db->get();		
					if($qryinfo->num_rows()>0){
						$info = $qryinfo->row();
						if($info->status == 15){
							$stocktypeid = 74;
							$girddatainfo[$i]['stocktypeid'] = 74;
							$tagsalesdetailid = $info->salesdetailid;
							$girddatainfo[$i]['salesdetailid'] = $info->salesdetailid;
						}
					}
				}
				$modeid = $girddatainfo[$i]['modeid'];
				$totalamount = $girddatainfo[$i]['totalamount'];
				$ratepergram = $girddatainfo[$i]['ratepergram'];
				$paymentreferencedate = '0000-00-00';
				if($stocktypeid == 82 ) {
					$paymentaccountid = $girddatainfo[$i]['vendoraccountid'];
					$checkforrecvorder = '1';
				} else {
					$paymentaccountid = 1;
				}
				$issuereceiptid = 1;
				$employeepersonid = $girddatainfo[$i]['employeepersonid'];
				{// tax amount settings
					if($_POST['taxtypeid'] == 2 || $stocktypeid == 20) {
						$branchstateid = $this->Basefunctions->branchstateid;
						if($stateid == $branchstateid) {  // branch state
							$_POST['gsttaxmodeid'] = 2;
						} else { // other state
							$_POST['gsttaxmodeid'] = 3;
						}
						if($_POST['gsttaxmodeid'] != '1' && $_POST['gsttaxmodeid'] != '') {
							if(!empty($girddatainfo[$i]['taxgriddata'])) {
								$salesdetailtaxdata = json_decode($girddatainfo[$i]['taxgriddata'], true);
								foreach($salesdetailtaxdata['data'] as $row) {
									if(substr($row['taxname'],0,4) == 'CGST') {
										$salesdetailcgst = $row['amount'];
									}
									if(substr($row['taxname'],0,4) == 'SGST') {
										$salesdetailsgst = $row['amount'];
									}
								}
								if($_POST['gsttaxmodeid'] == '2') { // CGST/SGST
									$salesdetailcgst = number_format((float)$salesdetailcgst, 2, '.', '');
									$salesdetailsgst = number_format((float)$salesdetailsgst, 2, '.', '');
									$salesdetailigst = 0;
								} else if($_POST['gsttaxmodeid'] == '3') { // IGST
									$salesdetailigst = number_format((float)$salesdetailcgst + $salesdetailsgst, 2, '.', '');
									$salesdetailcgst = 0;
									$salesdetailsgst = 0;
								}
							}
						}
					}
				}
				$stockgrossweight = $girddatainfo[$i]['stockgrossweight'];
				$stockpieces = $girddatainfo[$i]['stockpieces'];
				$wastagespanlabel = $girddatainfo[$i]['wastagespanlabel'];
				$makingchargespanlabel = $girddatainfo[$i]['makingchargespanlabel'];
				$wastagekeylabel =$girddatainfo[$i]['wastagekeylabel'];
				$makingchargekeylabel =$girddatainfo[$i]['makingchargekeylabel'];
				$paymenttouch = $girddatainfo[$i]['paymenttouch'];
				$pureweight = $girddatainfo[$i]['pureweight'];
				$product = $girddatainfo[$i]['product'];
				$purity = $girddatainfo[$i]['purity'];
				$counter = $girddatainfo[$i]['counter'];
				$grossweight = $girddatainfo[$i]['grossweight'];
				$netweight = $girddatainfo[$i]['netweight'];
				$comment = $girddatainfo[$i]['salescomment'];
				$pieces = $girddatainfo[$i]['pieces'];
				$dustweight = $girddatainfo[$i]['dustweight'];
				$itemcaratweight = $girddatainfo[$i]['itemcaratweight'];
				//specific order status
				if($_POST['salestransactiontypeid'] == 20) {//takeorder
					if($stocktypeid == '75') {
					  $orderstatus = 2;
					} else if($stocktypeid == '82') {
						$orderstatus = 5;
						$counter = $girddatainfo[$i]['counter'];
						if($girddatainfo[$i]['placeordernumber'] == 'undefined') {
							$ordernumbername = $girddatainfo[$i]['ordernumbername'];
						} else {
							$ordernumbername = $girddatainfo[$i]['placeordernumber'];
						}
					}else{
						$orderstatus = 1;
					}
					$ordermodeid = 1;
				} else if($_POST['salestransactiontypeid'] == 21) {//place order
					$orderstatus = 3;
					$ordermodeid = $_POST['ordermodeid'];
				} else if($_POST['salestransactiontypeid'] == 11 && $stocktypeid == '83') { //Order tag
					$orderstatus = 4;
					$ordernumbername = $girddatainfo[$i]['ordernumbername'];
				} else if($stocktypeid == '85') { //Generate Place order
					$orderstatus = 3;
					$poorderstatus = 21;
					$generatepo = 2;
				} else {
					$orderstatus = 1;
					$ordermodeid = 1;
				}
				// Repair Order Status
				if($_POST['salestransactiontypeid'] == 28 && $stocktypeid == '86') { // Take Repair
					$repairstatus = 2;
				} else if($_POST['salestransactiontypeid'] == 29) { // Place Repair
					$repairstatus = 3;
					$ordermodeid = $_POST['ordermodeid'];
				} else if($_POST['salestransactiontypeid'] == 30) {
					$repairstatus = 4;
				} else if($_POST['salestransactiontypeid'] == 11 && $stocktypeid == '89') {
					$repairstatus = 5;
				} else {
					$repairstatus = 1;
				}
			}
			if($stocktypeid == 19) {//old jewel
				if($girddatainfo[$i]['counter'] == '') { 
					$counter = '6';
				} else{
					$counter = $girddatainfo[$i]['counter'];
				}
			} else if($stocktypeid == 20) { //sales return
				if($girddatainfo[$i]['counter'] == '') {
					$counter = '7';
					$processcounter = '7';
				} else{
					$counter = $girddatainfo[$i]['counter'];
					$processcounter = $girddatainfo[$i]['balancecounter'];
				}
			}else if($stocktypeid == 13){//for partial tag
				$balproductid = $girddatainfo[$i]['balaceproductid'];
				$processproductid = $girddatainfo[$i]['balaceproductid'];
				if($girddatainfo[$i]['balancecounter'] == '') {
					$processcounter = 9;
				}else{
					$processcounter = $girddatainfo[$i]['balancecounter'];
				}
			}else{
				$balproductid = 1;
				$processproductid = 1;
				$processcounter = 1;
			}
			$approvalstatus = 0;
			if($stocktypeid == 62 || $stocktypeid == 63 || $stocktypeid == 24) //approval out tag,untag
			{
				$approvalstatus = 1;
			}
			$checkemployeepersonid = explode(',',$girddatainfo[$i]['employeepersonid']);
			if(is_array($checkemployeepersonid) && count($checkemployeepersonid) > 1) {
				$memployeepersonid = array();
				$memployeepersonid = $checkemployeepersonid;
				$employeepersonid = $checkemployeepersonid[0];
				if (($key = array_search($employeepersonid, $memployeepersonid)) !== false) {
					unset($memployeepersonid[$key]);
				}
				$memployeepersonid = implode(',',$memployeepersonid);
			} else {
				$employeepersonid = $employeepersonid;
				$memployeepersonid = 1;
			}
			if($_POST['salestransactiontypeid'] == '20' && $stocktypeid == '82' && $_POST['receiveorderconcept'] == '1') {
				$lottypeidv = 2;
				$purchasemodev = 3;
			} else {
				$lottypeidv = $girddatainfo[$i]['lottypeid'];
				$purchasemodev = $girddatainfo[$i]['purchasemode'];
			}
			$sales_detail = array(
				'salesid'=>$salesid,
				'modeid'=>$modeid,
				'stocktypeid'=>$stocktypeid,
				'itemtagid'=>$girddatainfo[$i]['itemtagid'],
				'purityid'=>$purity,
				'productid'=>$product,
				'categoryid'=>$girddatainfo[$i]['category'],
				'counterid'=>$counter,
				'grossweight'=>$grossweight,
				'stoneweight'=>$girddatainfo[$i]['stoneweight'],
				'dustweight'=>$dustweight,
				'netweight'=>$netweight,
				'caratweight'=>$girddatainfo[$i]['itemcaratweight'],
				'pieces'=>$pieces,
				'ratepergram'=>$ratepergram,
				'proditemrate'=>$girddatainfo[$i]['proditemrate'],
				'wastageless'=>$girddatainfo[$i]['wastageless'],
				'netweightcalculationid'=>$girddatainfo[$i]['gwcaculation'],
				'ratelesscalc'=>$girddatainfo[$i]['ratelesscalc'],
				'itemdiscountlimit'=>$girddatainfo[$i]['discountdata'],
				'discountpercent'=>$girddatainfo[$i]['sumarydiscountlimit'],
				'pendingproductid'=>$balproductid,
				'ckeyword'=>$girddatainfo[$i]['ckeyword'],
				'ckeywordvalue'=>$girddatainfo[$i]['ckeywordvalue'],
				'ckeywordoriginalvalue'=>$girddatainfo[$i]['ckeywordoriginalvalue'],
				'oldjewelvoucherstatus'=>$girddatainfo[$i]['statusoldjewelvoucher'],
				'comment'=>$comment,
				'grossamount'=>$girddatainfo[$i]['grossamount'],
				'chargesamount'=>$girddatainfo[$i]['additionalchargeamt'],
				'stoneamount'=>$girddatainfo[$i]['stonecharge'],
				'discountamount'=>$girddatainfo[$i]['discount'],
				'taxcategoryid'=>$girddatainfo[$i]['taxcategory'],
				'taxamount'=>$girddatainfo[$i]['taxamount'],
				'totalamount'=>$totalamount,
				'pureweight'=>$pureweight,
				'orderstatusid' => $orderstatus,
				'poorderstatusid' => $poorderstatus,
				'generatepo' => $generatepo,
				'orderduedate' => $this->Basefunctions->ymd_format($girddatainfo[$i]['orderduedate']),
				'vendororderduedate' => $this->Basefunctions->ymd_format($girddatainfo[$i]['vendororderduedate']),
				'ordervendorid' =>$girddatainfo[$i]['ordervendorid'],
				'orderitemratefix' =>$girddatainfo[$i]['orderitemratefix'],
				'chitbookno'=>$girddatainfo[$i]['chitbookno'],
				'chitbookname'=>$girddatainfo[$i]['chitbookname'],
				'chitschemetypeid'=>$girddatainfo[$i]['chitschemetypeid'],
				'interestamt'=>$girddatainfo[$i]['totalpaidamount'],
				'chitamount'=>$girddatainfo[$i]['chitamount'],
				'chitgram'=>$girddatainfo[$i]['chitgram'],
				'melting'=>$girddatainfo[$i]['paymentmelting'],
				'secondtouch'=>$paymenttouch,
				'olditemdetails'=>$girddatainfo[$i]['olditemdetails'],
				'wastage'=>$girddatainfo[$i]['wastage'],
				'making'=>$girddatainfo[$i]['makingcharge'],
				'prewastage'=>$girddatainfo[$i]['prewastage'],
				'premakingcharge'=>$girddatainfo[$i]['premakingcharge'],
				'wastagespan'=>$girddatainfo[$i]['wastagespan'],
				'makingspan'=>$girddatainfo[$i]['makingchargespan'],
				'referencedate'=>$paymentreferencedate,
				'wastagespankey'=>$wastagespanlabel,
				'makingchargespankey'=>$makingchargespanlabel,
				'wastagespanlabel'=>$wastagekeylabel,
				'makingchargespanlabel'=>$makingchargekeylabel,
				'lstcharge'=>$girddatainfo[$i]['lstcharge'],
				'lstchargespan'=>$girddatainfo[$i]['lstchargespan'],
				'lstchargespanlabel'=>$girddatainfo[$i]['lstchargespanlabel'],
				'lstchargekeylabel'=>$girddatainfo[$i]['lstchargekeylabel'],
				'prelstcharge'=>$girddatainfo[$i]['prelstcharge'],
				'flatcharge'=>$girddatainfo[$i]['flatcharge'],
				'flatchargespan'=>$girddatainfo[$i]['flatchargespan'],
				'preflatcharge'=>$girddatainfo[$i]['preflatcharge'],
				'flatchargespankey'=>$girddatainfo[$i]['flatchargespanlabel'],
				'flatchargespanlabel'=>$girddatainfo[$i]['flatchargekeylabel'],
				'bankname'=>$girddatainfo[$i]['bankname'],
				'referencenumber'=>$girddatainfo[$i]['paymentrefnumber'],
				'employeeid'=>$employeepersonid,
				'memployeeid'=>$memployeepersonid,
				'presalesdetailid'=>$girddatainfo[$i]['salesdetailid'],
				'estimationnumber'=>$girddatainfo[$i]['estimationnumber'],
				'partialgrossweight'=>$girddatainfo[$i]['partialgrossweight'],
				'processproductid'=>$processproductid,
				'processcounterid'=>$processcounter,
				'orderitemsize'=>$girddatainfo[$i]['orderitemsize'],
				'paymentaccountid'=>$paymentaccountid,
				'cgst'=>$salesdetailcgst,
				'sgst'=>$salesdetailsgst,
				'igst'=>$salesdetailigst,
				'stockgrossweight'=>$stockgrossweight,
				'stockpieces'=>$stockpieces,
				'issuereceipttypeid'=>$issuereceiptid,
				'cardtypeid'=>$girddatainfo[$i]['cardtype'],
				'approvalcode'=>$girddatainfo[$i]['approvalcode'],
				'lottypeid'=>$lottypeidv,
				'purchasemodeid'=>$purchasemodev,
				'ordermodeid'=>$ordermodeid,
				'detailordernumberid'=>$girddatainfo[$i]['ordernumber'],
				'creditadjusthidden'=>$girddatainfo[$i]['creditadjusthidden'],
				'creditadjustvaluehidden'=>$girddatainfo[$i]['creditadjustvaluehidden'],
				'ordernumber'=>$ordernumbername,
				'deliverystatusid'=>4,
				'oldsalesreturntax'=>$girddatainfo[$i]['oldsalesreturntax'],
				'tagimage'=>str_replace('radio_button_checked','',$girddatainfo[$i]['tagimage']),
				'wastageweight'=>$girddatainfo[$i]['wastageweight'],
				'approvalstatus'=>$approvalstatus,
				'repairstatusid'=>$repairstatus,
				'repaircharge'=>$girddatainfo[$i]['repaircharge'],
				'repairchargespan'=>$girddatainfo[$i]['repairchargespan'],
				'prerepaircharge'=>$girddatainfo[$i]['prerepaircharge'],
				'repairchargekeylabel'=>$girddatainfo[$i]['repairchargekeylabel'],
				'repairchargespanlabel'=>$girddatainfo[$i]['repairchargespanlabel'],
				'itemwastagevalue'=>$girddatainfo[$i]['itemwastagevalue'],
				'itemmakingvalue'=>$girddatainfo[$i]['itemmakingvalue'],
				'itemflatvalue'=>$girddatainfo[$i]['itemflatvalue'],
				'itemlstvalue'=>$girddatainfo[$i]['itemlstvalue'],
				'itemrepairvalue'=>$girddatainfo[$i]['itemrepairvalue'],
				'itemchargesamtvalue'=>$girddatainfo[$i]['itemchargesamtvalue'],
				'status'=>$this->Basefunctions->activestatus
			);
			$sales_detail = array_merge($sales_detail,$defdatedataset);
			$this->db->insert('salesdetail',array_filter($sales_detail));
			$salesdetailid = $this->db->insert_id();

			// Loose Stone - Update Diamond carat weight & Dia Pieces
			if($girddatainfo[$i]['itemcaratweight'] > 0) {
				$loosestonediadetails = array(
					'diacaratweight' => $girddatainfo[$i]['itemcaratweight'],
					'diapieces' => $pieces
				);
				$loosestonediadetails = array_merge($loosestonediadetails,$defupdatedataset);
				$this->db->where('salesdetailid',$salesdetailid);
				$this->db->update('salesdetail',$loosestonediadetails);
			}
			if($girddatainfo[$i]['modeid'] == 2 ) {
				// stone details insert
				if(!empty($girddatainfo[$i]['hiddenstonedata'])) {
					$salesstonedata = json_decode($girddatainfo[$i]['hiddenstonedata'], true);
					usort($salesstonedata,function($a,$b){
						return $a['stonegroupid'] - $b['stonegroupid'];
					});
					$salesstoneorder = 0;
					$diamondcaratweight = 0.00;
					$diamondpieces = 0;
					$stonecaratweight = 0.00;
					$stone_pieces = 0;
					foreach($salesstonedata as $row)
					{
						$stoneinsert = array(
								'salesid' =>$salesid,
								'salesdetailid' => $salesdetailid,
								'stoneid' => $row['stoneid'],
								'salesstoneentryorder'=> $salesstoneorder,
								'stonetypeid' => $row['stonegroupid'],
								'purchaserate' =>$row['purchaserate'],
								'stonerate' =>$row['basicrate'],
								'pieces' => $row['stonepieces'],
								'caratweight' => $row['caratweight'],
								'gram' => $row['stonegram'],
								'totalcaratweight' => $row['totalcaratweight'],
								'totalnetweight' =>$row['totalweight'],
								'totalamount' => $row['stonetotalamount'],
								'stoneentrycalctypeid'=>$row['stoneentrycalctypeid'],
								'status'=>$this->Basefunctions->activestatus
						);
						
						$stoneinsert = array_merge($stoneinsert,$defdatedataset);
						$this->db->insert('salesstoneentry',$stoneinsert);
						$salesstoneorder++;
						
						if($row['stonegroupid'] == 2) {
							$diamondcaratweight = number_format((float)$row['caratweight'] + $diamondcaratweight, 2, '.', '');
							$diamondpieces = number_format((int)$row['stonepieces'] + $diamondpieces, 0, '.', '');
						} else if($row['stonegroupid'] != 2) {
							$stonecaratweight = number_format((float)$row['caratweight'] + $stonecaratweight, 2, '.', '');
							$stone_pieces = number_format((int)$row['stonepieces'] + $stone_pieces, 0, '.', '');
						}
					}
					$sddiastonedetails = array(
						'diacaratweight' => $diamondcaratweight,
						'diapieces' => $diamondpieces,
						'stonecaratweight' => $stonecaratweight,
						'stonepieces' => $stone_pieces
					);
					$sd_updatesalesdetail = array_merge($sddiastonedetails,$defupdatedataset);
					$this->db->where('salesdetailid',$salesdetailid);
					$this->db->update('salesdetail',$sd_updatesalesdetail);
				}
				// salesdetail tax store
				if(!empty($girddatainfo[$i]['taxgriddata'])) {
					$taxdata = json_decode($girddatainfo[$i]['taxgriddata'], true);
					$taxorder = 0;
					foreach($taxdata['data'] as $row) {
						$taxinsert = array(
							'salesid' =>$salesid,
							'salesdetailid' =>$salesdetailid,
							'taxcategoryid'=>$taxdata['id'],
							'taxid'=>$row['taxid'],
							'taxname'=>$row['taxname'],
							'taxrate'=>$row['rate'],
							'value'=>$row['amount']
						);
						$taxinsert = array_merge($taxinsert,$defdatedataset);
						$this->db->insert('salesdetailtax',$taxinsert);
						$taxorder++;
					}
				}
				// lot create based on lot type
				if($_POST['salestransactiontypeid'] == 9 && $girddatainfo[$i]['lottypeid'] == '2') { // both & automatic
					$lotcreatestaus = 1;
				}else{
					$lotcreatestaus = 0;
				}
				if($lotcreatestaus == 1 & $stocktypeid != 73) {
					$serialnumber = $this->Basefunctions->varrandomnumbergenerator(44,'lotnumber',1,''); 
					if($girddatainfo[$i]['product'] == 3 || $girddatainfo[$i]['product'] == '' ) {
						$lotproduct = 1;
					}else {
						$lotproduct = $girddatainfo[$i]['product'];
					}
					$loosestone_val = 'No';
					if($girddatainfo[$i]['itemcaratweight'] > 0) {
						$loosestone_val = 'Yes';
					}
					$insert=array(
						'lotnumber'=>$serialnumber,
						'billnumber'=>$salesnumber,
						'billrefno'=>$billrefno, 
						'lotdate'=>$salesdate,
						'purityid'=>$girddatainfo[$i]['purity'],
						'productid'=>$lotproduct,
						'categoryid'=>$girddatainfo[$i]['category'],
						'grossweight'=>$girddatainfo[$i]['grossweight'],
						'stoneweight'=>$girddatainfo[$i]['stoneweight'],
						'netweight'=>$girddatainfo[$i]['netweight'],
						'caratweight'=>$girddatainfo[$i]['itemcaratweight'],
						'pieces'=>$girddatainfo[$i]['pieces'],
						'accountid'=>$accountid,
						'salesdetailid'=>$salesdetailid,
						'purchase'=>'Yes',
						'receiveorder'=>'No',
						'stoneapplicable'=>'No',
						'loosestone'=>$loosestone_val,
						'lottypeid'=>2,
						'branchid'=>$this->Basefunctions->branchid,
						'industryid'=>$this->Basefunctions->industryid,
						'status'=>$this->Basefunctions->activestatus
					);
					$insert = array_merge($insert,$defdatedataset);
					$this->db->insert('lot',array_filter($insert));
					$lotid = $this->db->insert_id();
					$lotidupdate = array('lotid'=>$lotid);
					$lotidupdate = array_merge($lotidupdate,$updatedefaultvalueget);
					$this->db->where('salesdetailid',$salesdetailid);
					$this->db->update('salesdetail',$lotidupdate);
					if($girddatainfo[$i]['itemcaratweight'] > 0) {
						$loosestonectwtupdate = array(
							'diacaratweight' => $girddatainfo[$i]['itemcaratweight'],
							'diapieces' => $girddatainfo[$i]['pieces']
						);
						$loosestonectwtupdate = array_merge($loosestonectwtupdate,$defupdatedataset);
						$this->db->where('salesdetailid',$salesdetailid);
						$this->db->update('lot',$loosestonectwtupdate);
					}
					if($_POST['salestransactiontypeid'] == 9) {
						if(!empty($girddatainfo[$i]['hiddenstonedata'])) {
							$salesstonedata = json_decode($girddatainfo[$i]['hiddenstonedata'], true);
							// Update Caratweight and Pieces separately for Diamond and Other Stones by Kumaresan
							$diamondcaratweight = 0.00;
							$diamondpieces = 0;
							$stonecaratweight = 0.00;
							$stone_pieces = 0;
							foreach($salesstonedata as $row) {
								if($row['stonegroupid'] == 2) {
									$diamondcaratweight = number_format((float)$row['caratweight'] + $diamondcaratweight, 2, '.', '');
									$diamondpieces = number_format((int)$row['stonepieces'] + $diamondpieces, 0, '.', '');
								} else if($row['stonegroupid'] != 2) {
									$stonecaratweight = number_format((float)$row['caratweight'] + $stonecaratweight, 2, '.', '');
									$stone_pieces = number_format((int)$row['stonepieces'] + $stone_pieces, 0, '.', '');
								}
							}
							$lotstoneapplicable = array(
								'stoneapplicable'=>'Yes',
								'diacaratweight' => $diamondcaratweight,
								'diapieces' => $diamondpieces,
								'stonecaratweight' => $stonecaratweight,
								'stonepieces' => $stone_pieces
							);
							$lot_updatesalesdetail = array_merge($lotstoneapplicable,$defupdatedataset);
							$this->db->where('salesdetailid',$salesdetailid);
							$this->db->update('lot',$lot_updatesalesdetail);
						}
					}
				}
			}
			// update the tag table to sold and do stock tables entries
			if(in_array($_POST['salestransactiontypeid'],array(11,13))) // sales,weightsales type means update the tag table
			{
				if($stocktypeid == 11 OR $stocktypeid == 13 OR $stocktypeid == 74 OR $stocktypeid == 83) //tag
				{
					if($stocktypeid == 74){
						
						$updatesalesdetail = array(
								'approvalstatus'=>2
						);
						
						$updatesalesdetail = array_merge($updatesalesdetail,$updatedefaultvalueget);
						$this->db->where('salesdetailid',$girddatainfo[$i]['salesdetailid']);
						$this->db->update('salesdetail',$updatesalesdetail);
					}
					$update = array(
							'status'=>$this->Basefunctions->soldstatus
					);
					
					$update = array_merge($update,$updatedefaultvalueget);
					$this->db->where('itemtagid',$girddatainfo[$i]['itemtagid']);
					$this->db->update('itemtag',$update);
				}
				//stock table update
				$this->Basefunctions->stocktableentryfunction($salesdetailid,'52');
				$this->removerfid();
				// Update Repair Status
				if($_POST['salestransactiontypeid'] == 11 && $stocktypeid == '89') {
					$presalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$girddatainfo[$i]['salesdetailid']);
					$prepresalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$presalesdetailid);
					$this->updatetakerepairstaus(5,$girddatainfo[$i]['salesdetailid']);
					$this->updatetakerepairstaus(5,$presalesdetailid);
					$this->updatetakerepairstaus(5,$prepresalesdetailid);
				}
			}
			// urd purchase check
			if($_POST['salestransactiontypeid'] == 11 && $stocktypeid == 19) { // sales & old jewel
				$urdpurchasestatus = 1;
			}
			//?approval out tag type
			if(in_array($_POST['salestransactiontypeid'],array(18)))
			{
				if($stocktypeid == 62) //approval out tag
				{
					if(!empty($girddatainfo[$i]['itemtagid'])) {
						$update = array(
							'status'=>$this->Basefunctions->approvaloutstatus,
							'salesid'=>$salesid,
							'salesdetailid'=>$salesdetailid 
						);
						
						$update = array_merge($update,$updatedefaultvalueget);
						$this->db->where('itemtagid',$girddatainfo[$i]['itemtagid']);
						$this->db->update('itemtag',$update);
					}
				}
				if($stocktypeid == 65) //approval out return tag
				{
					if(!empty($girddatainfo[$i]['itemtagid'])) {
					$update = array(
							'status'=>$this->Basefunctions->activestatus
					);
					$update = array_merge($update,$updatedefaultvalueget);
					$this->db->where('itemtagid',$girddatainfo[$i]['itemtagid']);
					$this->db->update('itemtag',$update);
					}
					$updatesalesdetail = array(
							'approvalstatus'=>0
					);
					$updatesalesdetail = array_merge($updatesalesdetail,$updatedefaultvalueget);
					$this->db->where('salesdetailid',$girddatainfo[$i]['salesdetailid']);
					$this->db->update('salesdetail',$updatesalesdetail);
				}
				$this->removerfid();							
			}
			{// approval out untag
					
				if($stocktypeid == '16' || $stocktypeid == '66' || $stocktypeid == '25') { 
					$approvaloutuntagsdid[$appunt] = $girddatainfo[$i]['salesdetailid'];
					$appunt++;
				}
			}
			// order status update
			if($stocktypeid == 76) { // place order
				$this->updatetakeorderstaus(3,$girddatainfo[$i]['salesdetailid']);
				$takeorder_salesid = $this->Basefunctions->singlefieldfetch('salesid','salesdetailid','salesdetail',$girddatainfo[$i]['salesdetailid']);
				$this->update_placeorderitemstatus($takeorder_salesid);
			} else if($stocktypeid == 87) { // Place Repair
				$this->updatetakerepairstaus(3,$girddatainfo[$i]['salesdetailid']);
			} else if($stocktypeid == 82) { // receive order
				$presalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$girddatainfo[$i]['salesdetailid']);
				$this->updatetakeorderstaus(5,$girddatainfo[$i]['salesdetailid']);
				$this->updatetakeorderstaus(5,$presalesdetailid);
				$povendormanagementconcept = $this->Basefunctions->get_company_settings('povendormanagement');
				if($povendormanagementconcept == 1) {
					$tosalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$presalesdetailid);
					$this->updatetakeorderstaus(5,$tosalesdetailid);
				}
				$presalesid = $this->Basefunctions->singlefieldfetch('salesid','salesdetailid','salesdetail',$presalesdetailid);
				$presalesnumber = $this->Basefunctions->singlefieldfetch('salesnumber','salesid','sales',$presalesid);
				if($_POST['receiveorderconcept'] == '0') { //Update itemtag entry
					$updateitemtag=array(
						'ordernumber'=>$presalesnumber,
						'salesdetailid'=>$presalesdetailid,
						'orderstatus'=>'Yes'
					);
					$updateitemtag = array_merge($updateitemtag,$updatedefaultvalueget);
					$this->db->where('itemtagid',$girddatainfo[$i]['itemtagid']);
					$this->db->update('itemtag',$updateitemtag);
				} else if($_POST['receiveorderconcept'] == '1') { //Lot creation based entry
					$serialnumber = $this->Basefunctions->varrandomnumbergenerator(44,'lotnumber',1,'');
					if($girddatainfo[$i]['product'] == 3 || $girddatainfo[$i]['product'] == '' ) {
						$lotproduct = 1;
					}else {
						$lotproduct = $girddatainfo[$i]['product'];
					}
					$loosestone_val = 'No';
					if($girddatainfo[$i]['itemcaratweight'] > 0) {
						$loosestone_val = 'Yes';
					}
					$insert=array(
						'lotnumber'=>$serialnumber,
						'billnumber'=>$salesnumber,
						'billrefno'=>$presalesnumber,
						'lotdate'=>$salesdate,
						'purityid'=>$girddatainfo[$i]['purity'],
						'productid'=>$lotproduct,
						'categoryid'=>$girddatainfo[$i]['category'],
						'grossweight'=>$girddatainfo[$i]['grossweight'],
						'stoneweight'=>$girddatainfo[$i]['stoneweight'],
						'netweight'=>$girddatainfo[$i]['netweight'],
						'caratweight'=>$girddatainfo[$i]['itemcaratweight'],
						'pieces'=>$girddatainfo[$i]['pieces'],
						'accountid'=>$accountid,
						'salesdetailid'=>$presalesdetailid,
						'purchase'=>'No',
						'receiveorder'=>'Yes',
						'loosestone'=>$loosestone_val,
						'lottypeid'=>2,
						'branchid'=>$this->Basefunctions->branchid,
						'industryid'=>$this->Basefunctions->industryid,
						'status'=>$this->Basefunctions->activestatus
					);
					$insert = array_merge($insert,$defdatedataset);
					$this->db->insert('lot',array_filter($insert));
					$lotid = $this->db->insert_id();
					$lotidupdate = array('lotid'=>$lotid);
					$lotidupdate = array_merge($lotidupdate,$updatedefaultvalueget);
					$this->db->where('salesdetailid',$salesdetailid);
					$this->db->update('salesdetail',$lotidupdate);
					if($girddatainfo[$i]['itemcaratweight'] > 0) {
						$loosestonectwtupdate = array(
							'diacaratweight' => $girddatainfo[$i]['itemcaratweight'],
							'diapieces' => $girddatainfo[$i]['pieces']
						);
						$loosestonectwtupdate = array_merge($loosestonectwtupdate,$defupdatedataset);
						$this->db->where('salesdetailid',$presalesdetailid);
						$this->db->update('lot',$loosestonectwtupdate);
					}
					if(!empty($girddatainfo[$i]['hiddenstonedata'])) {
						$salesstonedata = json_decode($girddatainfo[$i]['hiddenstonedata'], true);
						// Update Caratweight and Pieces separately for Diamond and Other Stones by Kumaresan
						$diamondcaratweight = 0.00;
						$diamondpieces = 0;
						$stonecaratweight = 0.00;
						$stone_pieces = 0;
						foreach($salesstonedata as $row) {
							if($row['stonegroupid'] == 2) {
								$diamondcaratweight = number_format((float)$row['caratweight'] + $diamondcaratweight, 2, '.', '');
								$diamondpieces = number_format((int)$row['stonepieces'] + $diamondpieces, 0, '.', '');
							} else if($row['stonegroupid'] != 2) {
								$stonecaratweight = number_format((float)$row['caratweight'] + $stonecaratweight, 2, '.', '');
								$stone_pieces = number_format((int)$row['stonepieces'] + $stone_pieces, 0, '.', '');
							}
						}
						$lotstoneapplicable = array(
							'stoneapplicable'=>'Yes',
							'diacaratweight' => $diamondcaratweight,
							'diapieces' => $diamondpieces,
							'stonecaratweight' => $stonecaratweight,
							'stonepieces' => $stone_pieces
						);
						$lot_updatesalesdetail = array_merge($lotstoneapplicable,$defupdatedataset);
						$this->db->where('lotid',$lotid);
						$this->db->update('lot',$lot_updatesalesdetail);
					}
				}
				if($_POST['questionaireapplicable'] == 1 && !empty($_POST['questionairerecorderadminarray'])) {
					$questionarrayadmindetails = explode(',',$_POST['questionairerecorderadminarray']);
					$insertquesforadmin =array(
						'salesid'=>$salesid,
						'salesdetailid'=>$salesdetailid,
						'question1'=>$questionarrayadmindetails[0],
						'question2'=>$questionarrayadmindetails[1],
						'question3'=>$questionarrayadmindetails[2],
						'question4'=>$questionarrayadmindetails[3],
						'question5'=>$questionarrayadmindetails[4],
						'question6'=>$questionarrayadmindetails[5]
					);
					$insertquesforadmin = array_merge($insertquesforadmin,$defdatedataset);
					$this->db->insert('questionairedetails',array_filter($insertquesforadmin));
				}
			} else if($stocktypeid == 88) { // Receive Repair
				$presalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$girddatainfo[$i]['salesdetailid']);
				$this->updatetakerepairstaus(4,$girddatainfo[$i]['salesdetailid']);
				$this->updatetakerepairstaus(4,$presalesdetailid);
			} else if($_POST['salestransactiontypeid'] == 11 && $stocktypeid == 83) { // order tag
				$this->updatetakeorderstaus(4,$girddatainfo[$i]['salesdetailid']);
				$orpresalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$girddatainfo[$i]['salesdetailid']);
				$this->updatetakeorderstaus(4,$orpresalesdetailid);
				$finalsalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$orpresalesdetailid);
				$this->updatetakeorderstaus(4,$finalsalesdetailid);
			} else if($stocktypeid == 85) { // generate place order - only status update
				$update_placeordergeneratepo = array('generatepo'=>'2');
				$this->db->where('salesdetailid',$girddatainfo[$i]['salesdetailid']);
				$this->db->update('salesdetail',array_filter($update_placeordergeneratepo));
				if($_POST['questionaireapplicable'] == 1 && !empty($_POST['questionairegenpovendorarray'])) {
					$questionarraydetails = explode(',',$_POST['questionairegenpovendorarray']);
					$insertquesforvendor =array(
						'salesid'=>$salesid,
						'salesdetailid'=>$salesdetailid,
						'question1'=>$questionarraydetails[0],
						'question2'=>$questionarraydetails[1],
						'question3'=>$questionarraydetails[2],
						'question4'=>$questionarraydetails[3],
						'question5'=>$questionarraydetails[4]
					);
					$insertquesforvendor = array_merge($insertquesforvendor,$defdatedataset);
					$this->db->insert('questionairedetails',array_filter($insertquesforvendor));
				}
			}
			// account ledger concept - advance
			{// insert - acc ledger creditno adj overlay inner overlay
				if($stocktypeid == 31) {
					if($_POST['salestransactiontypeid'] == 24) {
						$creidtnoadjaccid = $paymentaccountid;
					} else {
						$creidtnoadjaccid = $accountid;
					}
					$advreceiptpaidamount = 0;
					$advissuepaidamount = 0;
					$advreceiptpaidwt = 0;
					$advissuepaidwt = 0;
					$advissuereceiptid = 1;
					$advwtissuereceiptid = 1;
					$entrymode = 1;
					$advancegirddata = $girddatainfo[$i]['creditadjustvaluehidden'];
					$advancedatainfo = json_decode($advancegirddata,true);
					if(!empty($girddatainfo[$i]['creditadjustvaluehidden'])) {
						foreach($advancedatainfo as $row){
							if($girddatainfo[$i]['issuereceipttypeid'] == 3)
							{
								$advissuereceiptid = 2;
								$advwtissuereceiptid = 2;
								$advissuepaidamount = $row['mainfinalamthidden'];
								$advissuepaidwt = $row['mainfinalwthidden'];
							}else if($girddatainfo[$i]['issuereceipttypeid'] == 2) 
							{
								$advissuereceiptid = 3;
								$advwtissuereceiptid = 3;
								$advreceiptpaidamount = $row['mainfinalamthidden'];
								$advreceiptpaidwt = $row['mainfinalwthidden'];
							}
							if($row['creditreference'] == 2) { // Adjust
								$entrymode = 3;
							}else if($row['creditreference'] == 3) { // New Ref
								$entrymode = 6;
							}
							$advinsertaccountledgerdata = array(
												'salesid'=>$salesid,
												'salesdetailid'=>$salesdetailid,
												'accountid'=>$creidtnoadjaccid,
												'creditno'=>$row['overlaycreditno'],
												'entrymode'=>$entrymode,
												'referencenumber'=>$salesnumber,
												'salesdate'=>$salesdate,
												'issuereceiptid'=>$advissuereceiptid,
												'weightissuereceiptid'=>$advwtissuereceiptid,
												'paymentirtypeid'=>$row['creditpaymentirtypeid'],
												'amtissue'=>$advissuepaidamount,
												'amtreceipt'=>$advreceiptpaidamount,
												'weightissue'=>$advissuepaidwt,
												'weightreceipt'=>$advreceiptpaidwt,
							);
							$advinsertaccountledgerdata = array_merge($advinsertaccountledgerdata,$defdatedataset);
							$this->db->insert('accountledger',array_filter($advinsertaccountledgerdata));
							// credit no based balance entry
							if($row['overlaycreditno'] != '')
							{
								$this->creditnobalancesummary($salesid,$salesdetailid,$salesnumber,$creidtnoadjaccid,$row['overlaycreditno'],$row['creditpaymentirtypeid']);
							}
							// account no based balance summary
							$this->accountbalancesummary($salesid,$salesdetailid,$salesnumber,$creidtnoadjaccid,$salesdate,$row['creditpaymentirtypeid']);
						}
					}
					if(!empty($girddatainfo[$i]['creditadjustvaluehidden'])) {
						$sdcreditnodetails = json_decode($girddatainfo[$i]['creditadjustvaluehidden'], true);
						foreach($sdcreditnodetails as $row) {
							$sdcreditnodetailsinsertdata = array(
								'salesid'=>$salesid,
								'salesdetailid'=>$salesdetailid,
								'overlaycreditno'=>$row['overlaycreditno'],
								'creditsalesdate'=>$this->Basefunctions->ymd_format($row['creditsalesdate']),
								'paymentirtypeid'=>$row['paymentirtypeid'],
								'creditissuereceiptname'=>$row['creditissuereceiptname'],
								'amtissue'=>$row['amtissue'],
								'amtreceipt'=>$row['amtreceipt'],
								'finalamtissue'=>$row['finalamtissue'],
								'finalamtreceipt'=>$row['finalamtreceipt'],
								'creditissuereceiptid'=>$row['creditissuereceiptid'],
								'creditpaymentirtypeid'=>$row['creditpaymentirtypeid'],
								'creditweightissuereceiptname'=>$row['creditweightissuereceiptname'],
								'weightissue'=>$row['weightissue'],
								'weightreceipt'=>$row['weightreceipt'],
								'finalweightissue'=>$row['finalweightissue'],
								'finalweightreceipt'=>$row['finalweightreceipt'],
								'creditweightissuereceiptid'=>$row['creditweightissuereceiptid'],
								'accountledgerid'=>$row['accountledgerid'],
								'processid'=>$row['processid'],
								'process'=>$row['process'],
								'creditreference'=>$row['creditreference'],
								'creditreferencename'=>$row['creditreferencename'],
								'finalamthidden'=>$row['finalamthidden'],
								'finalwthidden'=>$row['finalwthidden'],
								'mainfinalamthidden'=>$row['mainfinalamthidden'],
								'mainfinalwthidden'=>$row['mainfinalwthidden'],
								'editoverlaystatus'=>$row['editoverlaystatus']
							);
							$sdcreditnodetailsinsertdata = array_merge($sdcreditnodetailsinsertdata,$defdatedataset);
							$this->db->insert('creditnodetails',array_filter($sdcreditnodetailsinsertdata));
						}
					}
				}
			}
			// account ledger concept - payment form - cash,card,cheque,dd,neft
			{// insert - acc ledger payment stocktype acc ledger insert only for amount
				if(in_array($stocktypeid,array(26,27,28,29,79,49))) {
					if($paymentaccountid > 1) {
						$payissuereceiptid = 1;
						$payamtissue = 0;
						$payamtreceipt = 0;
						$paywtissue = 0;
						$paywtreceipt = 0;
						$paypaymentirtypeid = 1;
						// payment ir type
						if($_POST['salestransactiontypeid'] == 11) {
							$paypaymentirtypeid = 2;
						}else if($_POST['salestransactiontypeid'] == 20) {
							$paypaymentirtypeid = 3;
						}else if($_POST['salestransactiontypeid'] == 9) {
							$paypaymentirtypeid = 4;
						}else if($_POST['salestransactiontypeid'] == 22) {
							$paypaymentirtypeid = 8;
						}else if($_POST['salestransactiontypeid'] == 23) {
							$paypaymentirtypeid = 7; 
						}else if($_POST['salestransactiontypeid'] == 25) {
							$paypaymentirtypeid = 6;
						}
						// i.r type with amt
						if($girddatainfo[$i]['issuereceipttypeid'] == 2)
						{
							$payissuereceiptid = 2;
							$payamtissue = $girddatainfo[$i]['paymenttotalamount'];
							$payamtreceipt = 0;
							$paywtissue = $girddatainfo[$i]['paymentpureweight'];
							$paywtreceipt = 0;
							
						}else if($girddatainfo[$i]['issuereceipttypeid'] == 3)
						{
							$payissuereceiptid = 3;
							$payamtissue = 0;
							$payamtreceipt = $girddatainfo[$i]['paymenttotalamount'];
							$paywtreceipt  = $girddatainfo[$i]['paymentpureweight'];
							$paywtissue = 0;
						}
						$payinsertaccountledgerdata = array(
							'salesid'=>$salesid,
							'accountid'=>$paymentaccountid,
							'entrymode'=>2,
							'referencenumber'=>$salesnumber,
							'salesdate'=>$salesdate,
							'issuereceiptid'=>$payissuereceiptid,
							'weightissuereceiptid'=>$payissuereceiptid,
							'paymentirtypeid'=>$paypaymentirtypeid,
							'amtissue'=>$payamtissue,
							'amtreceipt'=>$payamtreceipt,
							'weightissue'=>$paywtissue,
							'weightreceipt'=>$paywtreceipt
						);
						$payinsertaccountledgerdata = array_merge($payinsertaccountledgerdata,$defupdatedataset);
						$this->db->insert('accountledger',array_filter($payinsertaccountledgerdata));
						// account no based balance summary
						$this->accountbalancesummary($salesid,1,$salesnumber,$paymentaccountid,$salesdate,$paypaymentirtypeid);	
					}
				}
			}
			{//chitbook delete
				if($stocktypeid == 39) {
					$chitbooktypeid = $this->Basefunctions->get_company_settings('chitbooktype');
					$chitbookupdate = array(
							'status' => $this->Basefunctions->closestatus
					);
					$chitschemeid =  $this->Basefunctions->singlefieldfetch('chitschemeid','chitbookno','chitbook',trim($girddatainfo[$i]['chitbookno']));
					$cbno = explode("|",$girddatainfo[$i]['chitbookno']);
					$chitbookupdate = array_merge($chitbookupdate,$defupdatedataset);
					$this->db->where_in('chitbookno',$cbno);
					if($chitbooktypeid == 0) {
						$this->db->update('chitbook',$chitbookupdate);
					} else if($chitbooktypeid == 1) {
						$this->db->update('chitgeneration',$chitbookupdate);
					}
					$salesdetail_schemeid = array(
						'chitschemeid' => $chitschemeid
					);
					$salesdetail_schemeid = array_merge($salesdetail_schemeid,$defupdatedataset);
					$this->db->where_in('salesdetailid',$salesdetailid);
					$this->db->update('salesdetail',$salesdetail_schemeid);
				}
			}
		}
		
		if($appunt > 0){
			$this->appoutuntagupdate($approvaloutuntagsdid,'0',$_POST['salestransactiontypeid']);
		}
		// billwise tax store
		if(!empty($_POST['summarytaxgriddata'])) {
			$taxdata = json_decode($_POST['summarytaxgriddata'], true);
			$taxorder = 0;
			foreach($taxdata['data'] as $row){
				if($row['amount'] > 0) {
					$taxinsert = array(
						'salesid' =>$salesid,
						'salesdetailid' =>1,
						'taxcategoryid'=>$row['taxmasterid'],
						'taxid'=>$row['taxid'],
						'taxname'=>$row['taxname'],
						'taxrate'=>$row['rate'],
						'value'=>$row['amount']
					);
					if($ctime > $ltime) {
						$taxinsert = array_merge($taxinsert,$this->Crudmodel->nextdaydefaultvalueget());
					} else {
						$taxinsert = array_merge($taxinsert,$this->Crudmodel->defaultvalueget());
					}
					$taxinsert = array_merge($taxinsert,$defaultvalueget);
					$this->db->insert('salesdetailtax',$taxinsert);
				}
				$taxorder++;
			}
		}
		if($_POST['salestransactiontypeid'] == 11) {
			if(!empty($_POST['estimatearray'])) {
			   $estimateno = explode(',',$_POST['estimatearray']);
			   $inactiveestimateno = array('status'=>4);
			   $this->db->where_in('salesnumber',$estimateno);
			   $this->db->update('sales',$inactiveestimateno);
			}
		}
		// account ledger table insert main acc ledger
		if($checkforrecvorder == 0)  // no insertion for receive order
		{
			if(in_array($_POST['salestransactiontypeid'],array(9,11,20,25))) {
				$this->accountledgerinsert($_POST['salestransactiontypeid'],$_POST['oldjewelamtsummary'],$_POST['sumarypaidamount'],$_POST['sumaryissueamount'],$_POST['sumarypendingamount'],$_POST['balpurewthidden'],$_POST['issuereceiptid'],$_POST['wtissuereceiptid'],$salesid,$accountid,$salesnumber,$salesdate,$ctime,$ltime,$amountround,$weightround,$newsalesbillnumber,$_POST['advanceserialno'],$_POST['balanceserialno']);
			}
			
		}
		
		//sales summary update start
		$this->salessummaryupdate($salesid,$_POST['salestransactiontypeid'],$stateid);
		
		// account ledger concept - payment issue,receipt
		{ // insert - acc ledger main credit no adj overlay main overlay
			if(in_array($_POST['salestransactiontypeid'],array(22,23))) {
				$receiptpaidamount = 0;
				$issuepaidamount = 0;
				$receiptpaidwt = 0;
				$issuepaidwt = 0;
				$issuereceiptid = 1;
				$wtissuereceiptid = 1;
				$entrymode = 1;
				if($manualstatus == 1) {
					$creditgirddata = $_POST['creditoverlayvaluehidden'];
					$girddatainfo = json_decode($creditgirddata,true);
					if(!empty($_POST['creditoverlayvaluehidden'])) {
						foreach($girddatainfo as $row) {
						
							if($_POST['loadbilltype'] == 3) // advance bills
							{
								$issuereceiptid = 2;
								$wtissuereceiptid = 2;
								$issuepaidamount = $row['mainfinalamthidden'];
								$issuepaidwt = $row['mainfinalwthidden'];
								if($ratecuttypeid == 2 && $stocktypeid == 37)// amt to pure
								{
									$wtissuereceiptid = 3;
									$issuepaidwt = 0;
									$rwt = $row['mainfinalamthidden']/$ratepergram;
									$receiptpaidwt = number_format((float)$rwt, $weightround, '.', '');
								}else if($ratecuttypeid == 2 && $stocktypeid == 38)// pure to amt
								{
									$issuepaidamount = 0;
									$issuereceiptid = 3;
									$rwt = $row['mainfinalwthidden']*$ratepergram;
									$receiptpaidamount = number_format((float)$rwt, $amountround, '.', '');
								}
							}else if($_POST['loadbilltype'] == 2) 
							{
								$wtissuereceiptid = 3;
								$issuereceiptid = 3;
								$receiptpaidamount = $row['mainfinalamthidden'];
								$receiptpaidwt = $row['mainfinalwthidden'];
								if($ratecuttypeid == 2 && $stocktypeid == 37)// amt to pure
								{
									$receiptpaidwt = 0;
									$wtissuereceiptid = 2;
									$rwt = $row['mainfinalamthidden']/$ratepergram;
									$issuepaidwt = number_format((float)$rwt, $weightround, '.', '');
								}else if($ratecuttypeid == 2 && $stocktypeid == 38)// pure to amt
								{
									$receiptpaidamount = 0;
									$issuereceiptid = 2;
									$rwt = $row['mainfinalwthidden']*$ratepergram;
									$issuepaidamount = number_format((float)$rwt, $amountround, '.', '');
								}
							}
							if($row['creditreference'] == 2) { // Adjust
								$entrymode = 3;
							} else if($row['creditreference'] == 3) { // New Ref
								$entrymode = 6;
							}
							$insertaccountledgerdata = array(
												'salesid'=>$salesid,
												'accountid'=>$accountid,
												'creditno'=>$row['overlaycreditno'],
												'entrymode'=>$entrymode,
												'referencenumber'=>$salesnumber,
												'salesdate'=>$salesdate,
												'issuereceiptid'=>$issuereceiptid,
												'weightissuereceiptid'=>$wtissuereceiptid,
												'paymentirtypeid'=>$row['creditpaymentirtypeid'],
												'amtissue'=>$issuepaidamount,
												'amtreceipt'=>$receiptpaidamount,
												'weightissue'=>$issuepaidwt,
												'weightreceipt'=>$receiptpaidwt,
												
										);
							if($ctime > $ltime) {
								$insertaccountledgerdata = array_merge($insertaccountledgerdata,$this->Crudmodel->nextdaydefaultvalueget());
							} else {
								$insertaccountledgerdata = array_merge($insertaccountledgerdata,$this->Crudmodel->defaultvalueget());
							}
							$insertaccountledgerdata = array_merge($insertaccountledgerdata,$defaultvalueget);
							$this->db->insert('accountledger',array_filter($insertaccountledgerdata));
							// credit no based balance entry
							if($row['overlaycreditno'] != '') {
								$this->creditnobalancesummary($salesid,1,$salesnumber,$accountid,$row['overlaycreditno'],$row['creditpaymentirtypeid']);
							}
							// account no based balance summary
							$this->accountbalancesummary($salesid,1,$salesnumber,$accountid,$salesdate,$row['creditpaymentirtypeid']);
						}
				    }
				} else if($manualstatus == 0) {
					if($_POST['salestransactiontypeid'] == 22) { 
						$row['mainfinalamthidden'] = $_POST['sumaryissueamount'];
						$row['mainfinalwthidden'] = $_POST['spanissuepurewthidden'];
					}else if($_POST['salestransactiontypeid'] == 23) {
						$row['mainfinalamthidden'] = $_POST['sumarypaidamount'];
						$row['mainfinalwthidden'] = $_POST['spanpaidpurewthidden'];
					}
					if($_POST['loadbilltype'] == 3) // advance bills
					{
						$issuereceiptid = 2;
						$wtissuereceiptid = 2;
						$issuepaidamount = $row['mainfinalamthidden'];
						$issuepaidwt = $row['mainfinalwthidden'];
						if($ratecuttypeid == 2 && $stocktypeid == 37)// amt to pure
						{
							$wtissuereceiptid = 3;
							$issuepaidwt = 0;
							$rwt = $row['mainfinalamthidden']/$ratepergram;
							$receiptpaidwt = number_format((float)$rwt, $weightround, '.', '');
						}else if($ratecuttypeid == 2 && $stocktypeid == 38)// pure to amt
						{
							$issuepaidamount = 0;
							$issuereceiptid = 3;
							$rwt = $row['mainfinalwthidden']*$ratepergram;
							$receiptpaidamount = number_format((float)$rwt, $amountround, '.', '');
						}
					} else if($_POST['loadbilltype'] == 2) {
						$wtissuereceiptid = 3;
						$issuereceiptid = 3;
						$receiptpaidamount = $row['mainfinalamthidden'];
						$receiptpaidwt = $row['mainfinalwthidden'];
						if($ratecuttypeid == 2 && $stocktypeid == 37)// amt to pure
						{
							$receiptpaidwt = 0;
							$wtissuereceiptid = 2;
							$rwt = $row['mainfinalamthidden']/$ratepergram;
							$issuepaidwt = number_format((float)$rwt, $weightround, '.', '');
						}else if($ratecuttypeid == 2 && $stocktypeid == 38)// pure to amt
						{
							$receiptpaidamount = 0;
							$issuereceiptid = 2;
							$rwt = $row['mainfinalwthidden']*$ratepergram;
							$issuepaidamount = number_format((float)$rwt, $amountround, '.', '');
						}
					}
					$alentrymode = 6;
					$alpaymentirtypeid = 13;
					$insertaccountledgerdata = array(
										'salesid'=>$salesid,
										'accountid'=>$accountid,
										'creditno'=>'',
										'entrymode'=>$alentrymode,
										'referencenumber'=>$salesnumber,
										'salesdate'=>$salesdate,
										'issuereceiptid'=>$issuereceiptid,
										'weightissuereceiptid'=>$wtissuereceiptid,
										'paymentirtypeid'=>$alpaymentirtypeid,
										'amtissue'=>$issuepaidamount,
										'amtreceipt'=>$receiptpaidamount,
										'weightissue'=>$issuepaidwt,
										'weightreceipt'=>$receiptpaidwt,
										'status'=>$this->Basefunctions->activestatus
								);
					$insertaccountledgerdata = array_merge($insertaccountledgerdata,$defaultvalueget);
					$this->db->insert('accountledger',array_filter($insertaccountledgerdata));
					// account no based balance summary
					$this->accountbalancesummary($salesid,1,$salesnumber,$accountid,$salesdate,$alpaymentirtypeid);
				}
		     }
		}
		$array = array(
				'salesid'=>$salesid,
				'status'=>'SUCCESS',
				'salesnumber'=>$salesnumber	
				);
		echo json_encode($array);	
	}
	public function getsalesdetails()
	{
		$salesdetailid = $_POST['primaryid'];
		$salesid = $this->Basefunctions->singlefieldfetch('salesid','salesdetailid','salesdetail',$salesdetailid);
		$this->db->select('salestransactiontypeid');
		$this->db->from('sales');
		$this->db->where('salesid',$salesid);
		$this->db->where('salestransactiontypeid',16);
		$this->db->where_not_in('sales.status',array(0,3));
		$estimatedata=$this->db->get();
		if($estimatedata->num_rows() == 1){
				$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
				$rateround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //rate round-off
				$this->db->select(
						'salesdetail.itemtagid,salesdetail.purityid,salesdetail.productid,salesdetail.counterid,
								ROUND(salesdetail.grossweight,'.$round.') as grossweight,salesdetail.comment,
								ROUND(salesdetail.stoneweight,'.$round.') as stoneweight,
								ROUND(salesdetail.netweight,'.$round.') as netweight,
								salesdetail.pieces,salesdetail.stocktypeid,ROUND(salesdetail.ratepergram,'.$rateround.') as ratepergram,salesdetail.totalamount,itemtag.itemtagnumber,salesdetail.salesdetailimage,salesdetail.approvalnumber,salesdetail.ordernumber,DATE_FORMAT(salesdetail.orderduedate,"%d-%m-%Y") AS orderduedate,salesdetail.referencenumber,salesdetail.issuereceipttypeid,salesdetail.otherdetails,salesdetail.otherdetailsvalue,salesdetail.ckeyword,salesdetail.ckeywordvalue,salesdetail.ckeywordoriginalvalue,salesdetail.ckeywordcalvalue,salesdetail.ckeywordfcalvalue,itemtag.itemtagnumber',false);
				$this->db->from('salesdetail');
				$this->db->where('salesdetail.salesdetailid',$salesdetailid);
				$this->db->join('itemtag','itemtag.itemtagid = salesdetail.itemtagid','left outer');
				$this->db->where_not_in('salesdetail.status',array(0,3));
				$this->db->limit(1);
				$info = $this->db->get();
				if($info->num_rows() == 1)
				{
					$infos = $info->row();
					$bgetrate = $this->Basefunctions->getrate($infos->purityid);
					$getrate = $bgetrate['rate'];
					$stoneentry = $this->retrive_sales_stoneentry($salesid,$salesdetailid);
					$ckeyword=array();
					$ckeywordvalue=array();
					$keyworddetails=array();
					$keyworddata=array();
					$ckeyword = explode(',',$infos->ckeyword);
					$ckeywordvalue = explode(',',$infos->ckeywordvalue);
					$keyworddetails = array_combine($ckeyword, $ckeywordvalue);
					$j = 0;
					foreach($keyworddetails as $j => $l){
						$keyworddata[] = $j. ":".$l;
						$j++;
					}
					$keyword_str = implode (",", $keyworddata);
					$ckeyword1=array();
					$ckeywordoriginalvalue=array();
					$keyworddetails1=array();
					$keyworddata1=array();
					$ckeyword1 = explode(',',$infos->ckeyword);
					$ckeywordoriginalvalue = explode(',',$infos->ckeywordoriginalvalue);
					$keyworddetails1 = array_combine($ckeyword1, $ckeywordoriginalvalue);
					$m = 0;
					foreach($keyworddetails1 as $m => $p){
						$keyworddata1[] = $m. ":".$p;
						$m++;
					}
					$keyword_str1 = implode (",", $keyworddata1);
					$tagjsonarray = array(
							'stocktypeid' => $infos->stocktypeid,
							'itemtagid' => $infos->itemtagid,
							'itemtagnumber' => $infos->itemtagnumber,
							'product' => $infos->productid,
							'purity'=> $infos->purityid,
							'counter'=> $infos->counterid,
							'grossweight' => $infos->grossweight + 0,
							'stoneweight' => $infos->stoneweight + 0,
							'netweight' => $infos->netweight + 0,
							'pieces' => $infos->pieces,
							'ratepergram'=>$getrate,
							'referencenumber' => $infos->referencenumber,
							'otherdetails' => $infos->otherdetails,
							'otherdetailsvalue'=>$infos->otherdetailsvalue,
							'ckeyword'=>$infos->ckeyword,
							'ckeywordvalue' => $infos->ckeywordvalue,
							'ckeywordvaluedetails' => $keyword_str,
							'ckeywordoriginalvalue' => $infos->ckeywordoriginalvalue,
							'ckeywordoriginalvaluedetails' => $keyword_str1,
							'ckeywordcalvalue' => $infos->ckeywordcalvalue,
							'ckeywordfcalvalue'=>$infos->ckeywordfcalvalue,
							'hiddenstonedata' => $stoneentry
					);
				}
				else
				{
					$tagjsonarray = array('output'=>'NO');
				}
	}else
		{
			$tagjsonarray = array('output'=>'FAIL');
		}
	echo json_encode($tagjsonarray);
	}
	public function discountadd()
	{
		$salesid=$_POST['primaryid'];
		$discount=$_POST['discountvalue'];
		$this->db->select('stocktypeid');
		$this->db->from('salesdetail');
		$this->db->where('salesid',$salesid);
		$this->db->where('stocktypeid',57);
		$this->db->where_not_in('salesdetail.status',array(0,3));
		$discountdata=$this->db->get();
		if($discountdata->num_rows() == 1){
			 $update = array('totalamount'=>$discount);
			$this->db->where('salesid',$salesid);
			$this->db->where('stocktypeid',57);
			$this->db->update('salesdetail',$update);
		}else{
			$insert = array('totalamount'=>$discount,'salesid'=>$salesid,'stocktypeid'=>57);
			$header_insert = array_merge($insert,$this->Crudmodel->defaultvalueget());
			$this->db->insert('salesdetail',array_filter($header_insert));
		}
		echo 'SUCCESS';
	}
	public function loadapprovalnumber()
	{
		$stocktype=$_POST['stocktype'];
		$accountid=$_POST['accountid'];
		$stocktypeid='';
		if($stocktype == 25 || $stocktype == 77) // approval in return,approval in weight wise purchase
		{
			$stocktypeid=24;
		}else if($stocktype == 65 || $stocktype == 74){ // approval out tag return,approvalout tag sales
			$stocktypeid=62;
		}
		else if($stocktype == 66 || $stocktype == 16){ // approval out untag return,approvalout  untag sales
			$stocktypeid=63;
		}
		$this->db->select('sales.salesnumber');
		$this->db->from('sales');
		$this->db->join('salesdetail','salesdetail.salesid = sales.salesid');
		$this->db->where('sales.accountid',$accountid);
		$this->db->where('sales.salesnumber !=','pending');
		$this->db->where('salesdetail.stocktypeid',$stocktypeid);
		$this->db->where_in('salesdetail.approvalstatus',array(1));
		$this->db->where_not_in('salesdetail.status',array(0,3));
		$this->db->group_by("sales.salesnumber");
		$salesdata=$this->db->get();
		
		if($salesdata->num_rows()>0){
			foreach($salesdata->result() as $info)
			{
				if($info->salesnumber != ''){
				
					$salesarray[] = array(
						'data'=>$info->salesnumber
					);
				}	
			}	
		}else{
			$salesarray ='';
		}
		echo json_encode($salesarray);
	}
	public function setsalesdetails()
	{
		$salesdetailid = $_POST['primaryid'];
		$salesid = $this->Basefunctions->singlefieldfetch('salesid','salesdetailid','salesdetail',$salesdetailid);
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$rateround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //Rate round-off
		$this->db->select(
				'salesdetail.salesdetailid,salesdetail.itemtagid,salesdetail.purityid,salesdetail.productid,salesdetail.counterid,
						ROUND(salesdetail.grossweight,'.$round.') as grossweight,salesdetail.comment,
						ROUND(salesdetail.stoneweight,'.$round.') as stoneweight,
						ROUND(salesdetail.netweight,'.$round.') as netweight,
						salesdetail.pieces,ROUND(salesdetail.ratepergram,'.$rateround.') as ratepergram,salesdetail.totalamount,itemtag.itemtagnumber,salesdetail.salesdetailimage,salesdetail.ordernumber,DATE_FORMAT(salesdetail.orderduedate,"%d-%m-%Y") AS orderduedate,salesdetail.referencenumber,salesdetail.issuereceipttypeid,salesdetail.otherdetails,salesdetail.otherdetailsvalue,salesdetail.ckeyword,salesdetail.ckeywordvalue,salesdetail.ckeywordoriginalvalue,salesdetail.ckeywordcalvalue,salesdetail.ckeywordfcalvalue,itemtag.itemtagnumber,salesdetail.wastage,salesdetail.making,salesdetail.wastagespan,salesdetail.makingspan,salesdetail.presalesdetailid',false);
		$this->db->from('salesdetail');
		$this->db->where('salesdetail.salesdetailid',$salesdetailid);
		$this->db->join('itemtag','itemtag.itemtagid = salesdetail.itemtagid','left outer');
		$this->db->where_not_in('salesdetail.status',array(0,3));
		$this->db->limit(1);
		$info = $this->db->get();
		if($info->num_rows() == 1)
		{
			$infos = $info->row();
			$stoneentry = $this->retrive_sales_stoneentry($salesid,$salesdetailid);
			$ckeyword=array();
			$ckeywordvalue=array();
			$keyworddetails=array();
			$keyworddata=array();
			if($infos->ckeyword != ''){
				$ckeyword = explode(',',$infos->ckeyword);
				$ckeywordvalue = explode(',',$infos->ckeywordvalue);
				$keyworddetails = array_combine($ckeyword, $ckeywordvalue);
				$j = 0;
				foreach($keyworddetails as $j => $l){
					$keyworddata[] = $j. ":".$l;
					$j++;
				}
				$keyword_str = implode (",", $keyworddata);
			}else{
				$keyword_str ='';
			}
			$ckeyword1=array();
			$ckeywordoriginalvalue=array();
			$keyworddetails1=array();
			$keyworddata1=array();
			if($infos->ckeyword != ''){
				$ckeyword1 = explode(',',$infos->ckeyword);
				$ckeywordoriginalvalue = explode(',',$infos->ckeywordoriginalvalue);
				$keyworddetails1 = array_combine($ckeyword1, $ckeywordoriginalvalue);
				$m = 0;
				foreach($keyworddetails1 as $m => $p){
					$keyworddata1[] = $m. ":".$p;
					$m++;
				}
				$keyword_str1 = implode (",", $keyworddata1);
			}else{
				$keyword_str1 ='';
			}
			$tagjsonarray = array(
					'salesdetailid' => $infos->salesdetailid,
					'referencenumber' => $infos->salesdetailid,
					'salesid'=>$salesid,
					'itemtagid' => $infos->itemtagid,
					'itemtagnumber' => $infos->itemtagnumber,
					'product' => $infos->productid,
					'purity'=> $infos->purityid,
					'counter'=> $infos->counterid,
					'grossweight' => $infos->grossweight + 0,
					'stoneweight' => $infos->stoneweight + 0,
					'netweight' => $infos->netweight + 0,
					'pieces' => $infos->pieces,
					'ratepergram'=>$infos->ratepergram,
					'referencenumber' => $infos->referencenumber,
					'otherdetails' => $infos->otherdetails,
					'otherdetailsvalue'=>$infos->otherdetailsvalue,
					'ckeyword'=>$infos->ckeyword,
					'ckeywordvalue' => $infos->ckeywordvalue,
					'ckeywordvaluedetails' => $keyword_str,
					'ckeywordoriginalvaluedetails' => $keyword_str1,
					'ckeywordoriginalvalue' => $infos->ckeywordoriginalvalue,
					'ckeywordcalvalue' => $infos->ckeywordcalvalue,
					'ckeywordfcalvalue'=>$infos->ckeywordfcalvalue,
					'hiddenstonedata' => $stoneentry,
					'wastage'=>$infos->wastage,
					'makingcharge'=>$infos->making,
					'wastagespan'=>$infos->wastagespan,
					'makingchargespan'=>$infos->makingspan,
					'takeordernumber'=>$infos->ordernumber,
					'presalesdetailid'=>$infos->presalesdetailid
			);
		}
		else
		{
			$tagjsonarray = array('output'=>'NO');
		}
		echo json_encode($tagjsonarray);
	}
	//load take order number
	public function loadtakeordernumbermodel() {
		$status = $_POST['status'];
		$accountid = $_POST['accountid'];
		$poautomaticvendor = $_POST['poautomaticvendor'];
		$povendormanagement = $_POST['povendormanagement'];
		if($status == 3 && $povendormanagement == 1 && $_POST['stocktype'] != '88') {
			$mergedetails = ",tosales.salesnumber as nsalesnumber";
		} else {
			$mergedetails = "";
		}
		$this->db->select('sales.salesnumber,sales.salesid,sales.accountid,account.accountname,salesdetail.ordernumber'.$mergedetails.'');
		$this->db->from('sales');
		$this->db->join('salesdetail','salesdetail.salesid = sales.salesid','left');
		$this->db->join('account','account.accountid = sales.accountid','left');
		$this->db->where_not_in('sales.status',array(0,3));
		$this->db->where('sales.salesnumber !=','pending');
		$this->db->where_not_in('salesdetail.status',array(0,3));
		if($status == 3) {  // On receive Order Page
			if($_POST['stocktype'] != '88') {
				$this->db->where_in('salesdetail.orderstatusid',array(3));
				$this->db->where_in('sales.salestransactiontypeid',array(21));
				$this->db->where('sales.accountid',$accountid);
				$this->db->where_in('salesdetail.stocktypeid',array(76));
				if($povendormanagement == 1) {
					$this->db->join('salesdetail as posalesdetail','posalesdetail.presalesdetailid = salesdetail.salesdetailid AND posalesdetail.status NOT IN (0,3)','left');
					$this->db->join('salesdetail as tosalesdetail','tosalesdetail.salesdetailid = salesdetail.presalesdetailid AND tosalesdetail.status NOT IN (0,3)','left');
					$this->db->join('sales as posales','posales.salesid = posalesdetail.salesid AND posales.status NOT IN (0,3)','left');
					$this->db->join('sales as tosales','tosales.salesid = tosalesdetail.salesid AND tosales.status NOT IN (0,3)','left');
					$this->db->where_in('salesdetail.generatepo',array(2));
					$this->db->where_in('posalesdetail.delorderstatusid',array(24));
					$this->db->where_in('posales.deliverypomodeid',array(2,3,4));
				} else {
					$this->db->where_in('salesdetail.generatepo',array(1));
				}
			} else if($_POST['stocktype'] == '88') {
				$this->db->where_in('salesdetail.repairstatusid',array(3));
				$this->db->where_in('sales.salestransactiontypeid',array(29));
				$this->db->where('sales.accountid',$accountid);
				$this->db->where_in('salesdetail.stocktypeid',array(87));
			}
		}else if($status == 2) { // On Place Order Page
			if($_POST['stocktype'] == 87) {
				$this->db->where_in('salesdetail.repairstatusid',array(2));
				$this->db->where_in('sales.salestransactiontypeid',array(28));
				$this->db->where_in('salesdetail.stocktypeid',array(86));
			} else {
				if($poautomaticvendor == '1') {
					$this->db->where('salesdetail.ordervendorid',$accountid);
				}
				$this->db->where_in('salesdetail.orderstatusid',array(2));
				$this->db->where_in('sales.salestransactiontypeid',array(20));
				$this->db->where_in('salesdetail.stocktypeid',array(75));
			}
		}else if($status == 5) { // Sales Page - Order Tag Details - To show order received + tag done
			$this->db->where_in('sales.salestransactiontypeid',array(20));
			$this->db->where('sales.accountid',$accountid);
			$this->db->where_in('salesdetail.stocktypeid',array(75));
			if($_POST['receiveorderconcept'] == 1 && $povendormanagement == 1) {
				$this->db->join('salesdetail as posalesdetail','posalesdetail.presalesdetailid = salesdetail.salesdetailid AND posalesdetail.status NOT IN (0,3)','left');
				$this->db->join('salesdetail as gposalesdetail','gposalesdetail.presalesdetailid = posalesdetail.salesdetailid AND gposalesdetail.status NOT IN (0,3)','left');
				$this->db->join('salesdetail as rosalesdetail','rosalesdetail.presalesdetailid = gposalesdetail.salesdetailid AND rosalesdetail.status NOT IN (0,3)','left');
				$this->db->join('sales as posales','posales.salesid = posalesdetail.salesid AND posales.status NOT IN (0,3)','left');
				$this->db->join('sales as gposales','gposales.salesid = gposalesdetail.salesid AND gposales.status NOT IN (0,3)','left');
				$this->db->join('sales as rosales','rosales.salesid = rosalesdetail.salesid AND rosales.status NOT IN (0,3)','left');
				$this->db->where_not_in('rosalesdetail.lotid',array(1));
				$this->db->where_in('rosalesdetail.orderstatusid',array(5));
				$this->db->where_in('rosalesdetail.stocktypeid',array(82));
				$this->db->where_in('rosales.salestransactiontypeid',array(20));
			} else if($povendormanagement == 0) {
				$this->db->join('salesdetail as posalesdetail','posalesdetail.presalesdetailid = salesdetail.salesdetailid AND posalesdetail.status NOT IN (0,3)','left');
				$this->db->join('salesdetail as rosalesdetail','rosalesdetail.presalesdetailid = posalesdetail.salesdetailid AND rosalesdetail.status NOT IN (0,3)','left');
				$this->db->join('sales as posales','posales.salesid = posalesdetail.salesid AND posales.status NOT IN (0,3)','left');
				$this->db->join('sales as rosales','rosales.salesid = rosalesdetail.salesid AND rosales.status NOT IN (0,3)','left');
				$this->db->where_not_in('rosalesdetail.itemtagid',array(1));
				$this->db->where_in('salesdetail.orderstatusid',array(5));
			}
		} else if($status == 4) {  // Generate Place Order Page - Data retrieve from Place Order
			if($_POST['stocktype'] == '89') {
				$this->db->where_in('sales.salestransactiontypeid',array(28));
				$this->db->where('sales.accountid',$accountid);
				$this->db->where_in('salesdetail.stocktypeid',array(86));
				$this->db->where_in('salesdetail.repairstatusid',array(4));
			} else {
				$this->db->where_in('salesdetail.orderstatusid',array(3));
				$this->db->where_in('sales.salestransactiontypeid',array(21));
				$this->db->where('sales.accountid',$accountid);
				$this->db->where_in('salesdetail.stocktypeid',array(76));
				$this->db->where_in('salesdetail.poorderstatusid',array(21));
				$this->db->where_in('salesdetail.generatepo',array(1));
				$this->db->where('sales.salesid',$_POST['placeorderid']);
			}
		}
		$this->db->group_by('sales.salesid');
		$this->db->order_by('sales.salesid', 'DESC');
		$salesdata=$this->db->get();
		if($salesdata->num_rows()>0) {
			foreach($salesdata->result() as $info) {
				if($status == 5) {
					$accountid = $this->Basefunctions->singlefieldfetch('accountid','salesnumber','sales',$info->ordernumber);
					$accountname = $this->Basefunctions->singlefieldfetch('accountname','accountid','account',$accountid);
					$this->db->select('salesdetailid');
					$this->db->from('salesdetail');
					$this->db->where('salesid',$info->salesid);
					$this->db->where('salesdetail.stocktypeid',75);
					//$this->db->where('salesdetail.orderstatusid',5);
					$this->db->where('status',$this->Basefunctions->activestatus);
					$salesdetailcount = $this->db->get()->num_rows();
					$salesarray[] = array(
						'id'=>$info->salesid,
						'data'=>$info->salesnumber,
						'accountid'=>$accountid,
						'accountname'=>$accountname,
						'salesdetailcount'=>$salesdetailcount
					);
				} else {
					if($status == 3 && $povendormanagement == 1) {
						$info->salesnumber = $info->nsalesnumber;
					} else {
						$info->salesnumber = $info->salesnumber;
					}
					$salesarray[] = array(
						'id'=>$info->salesid,
						'data'=>$info->salesnumber,
						'accountid'=>$info->accountid,
						'accountname'=>$info->accountname,
						'salesdetailcount'=>0
					);
				}
			}
		} else {
			$salesarray = '';
		}
		echo json_encode($salesarray);
	}
	public function loadordernumber()
	{
		$stocktype=$_POST['stocktype'];
		$status=$_POST['status'];
		$refno=$_POST['refno'];
		$stocktypeid='';
		if($stocktype ==  76) // place order
		{
			$stocktypeid=75; // take order
		}
	
		$this->db->select('sales.salesnumber,DATE_FORMAT(sales.salesdate,"%d-%m-%Y") AS salesdate,salesdetail.salesdetailid,product.productname');
		$this->db->from('sales');
		$this->db->join('salesdetail','salesdetail.salesid = sales.salesid');
		$this->db->join('product','product.productid = salesdetail.productid');
		$this->db->where('salesdetail.stocktypeid',$stocktypeid);
		$this->db->where_in('salesdetail.orderstatusid',array(1));
		if($status == 0){
		    
		}else{
			$this->db->or_where('salesdetail.salesdetailid',$refno);
		}
		$this->db->where_not_in('salesdetail.status',array(0,3));
		$salesdata=$this->db->get();
	
		if($salesdata->num_rows()>0){
			foreach($salesdata->result() as $info)
			{
				$data='';
				if($info->salesnumber != ''){
					$data .=  $info->salesnumber.'-';
				}
				if($info->salesdetailid != ''){
					$data .=  $info->salesdetailid.'|';
				}
				if($info->salesdate != ''){
					$data .=  $info->salesdate.'|';
				}
				if($info->productname != ''){
					$data .=  $info->productname.'|';
				}
				$salesarray[] = array(
						'id'=>$info->salesdetailid,
						'data'=>$data
				);
			}
				
		}else{
			$salesarray ='';
		}
		echo json_encode($salesarray);
	}
	public function loadproductaddoncharge($product)
	{
		$chargeid=$this->Basefunctions->singlefieldfetch('chargeid','productid','product',$product);
		$chargeid=explode(',',$chargeid);
		$this->db->select('GROUP_CONCAT(chargekeyword) as chargekeyword',false);
		$this->db->from('charge');
		$this->db->where_in('chargeid',$chargeid);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$productchargearray=$this->db->get();
		return $productchargearray;
	}
	public function loadpurchaseproductaddoncharge($product)
	{
		$chargeid=$this->Basefunctions->singlefieldfetch('purchasechargeid','productid','product',$product);
		$chargeid=explode(',',$chargeid);
		$this->db->select('GROUP_CONCAT(chargekeyword) as chargekeyword',false);
		$this->db->from('purchasecharge');
		$this->db->where_in('purchasechargeid',$chargeid);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$productchargearray=$this->db->get();
		return $productchargearray;
	}
	public function simpledropdown($tablename,$fieldname,$whfield,$whdata) {
		$industryid = $this->Basefunctions->industryid;
		$this->db->select($fieldname);
		$this->db->from($tablename);
		$mvalu=explode(",",$whdata);
		if($whdata != '') {
			if(count($mvalu)>=2) {
				$this->db->where_in($whfield,$mvalu);
			} else {
				$this->db->where("FIND_IN_SET('$whdata',$whfield) >", 0);
			}
		}
		$this->db->where_not_in('status',array(0,3));
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$query= $this->db->get();
		return $query->result();
	}
	/* tax gird header load */
	public function taxgirdheaderinformationfetchmodel($moduleid) {
		$fnames = array('taxmasterid','taxmastername','taxid','taxname','rate','amount');
		$flabels = array('MasId','Tax Master','Id','Tax Name','Rate','Amount');
		$colmodnames = array('taxmasterid','taxmastername','taxid','taxname','taxrate','amount');
		$colindexnames = array('taxmaster','taxmaster','tax','tax','tax','tax');
		$uitypes = array('2','2','2','2','2','2');
		$viewtypes = array('0','1','0','1','1','1');
		$dduitypeids = array('17','18','20','19','23','25','26','27','28','29');
		$data = array();
		$i=0;
		$j=0;
		foreach($fnames as $fname) {
			if( in_array($uitypes[$j],$dduitypeids) ) {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j].'name';
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]='0';
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]='2';
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			} else {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			}
			$j++;
		}
		return $data;
	}
	//bill bumber grid data load
	public function billnumberheaderinformationfetchmodel($moduleid) {
		$data = array();
		$this->db->select('localmodulefieldname,fieldlabel,colmodelname,colmodelindex,colmodelviewtype,colmodeluitype,modeid,defaultdata,moduleid',false);
		$this->db->from('localmodulefield');
		$this->db->where('moduleid',52);
		$this->db->where_not_in('status',array(0,3));
		$info = $this->db->get();
		$i = 0;
		if($info->num_rows()>0){
			foreach($info->result() as $salesdata)
			{
				$data['fieldname'][$i]=$salesdata->localmodulefieldname;
				$data['fieldlabel'][$i]=$salesdata->fieldlabel;
				$data['colmodelname'][$i]=$salesdata->colmodelname;
				$data['colmodelindex'][$i]=$salesdata->colmodelindex;
				$data['colmodelviewtype'][$i]=$salesdata->colmodelviewtype;
				$data['colmodeluitype'][$i]=$salesdata->colmodeluitype;
				$data['colmoduleid'][$i]=$salesdata->moduleid;
				$data['modeid'][$i]=$salesdata->modeid;
				$data['defaultdata'][$i]=$salesdata->defaultdata;
				$i++;
			}
		}
		
		return $data;
	}
	//old jewel voucher grid data load
	public function oldjewelvoucherheaderinformationfetchmodel($moduleid) {
		$data = array();
		$this->db->select('localmodulefieldname,fieldlabel,colmodelname,colmodelindex,colmodelviewtype,colmodeluitype,modeid,defaultdata,moduleid',false);
		$this->db->from('localmodulefield');
		$this->db->where('moduleid',52);
		$this->db->where_not_in('status',array(0,3));
		$info = $this->db->get();
		$i = 0;
		if($info->num_rows()>0){
			foreach($info->result() as $salesdata)
			{
				$data['fieldname'][$i]=$salesdata->localmodulefieldname;
				$data['fieldlabel'][$i]=$salesdata->fieldlabel;
				$data['colmodelname'][$i]=$salesdata->colmodelname;
				$data['colmodelindex'][$i]=$salesdata->colmodelindex;
				$data['colmodelviewtype'][$i]=$salesdata->colmodelviewtype;
				$data['colmodeluitype'][$i]=$salesdata->colmodeluitype;
				$data['colmoduleid'][$i]=$salesdata->moduleid;
				$data['modeid'][$i]=$salesdata->modeid;
				$data['defaultdata'][$i]=$salesdata->defaultdata;
				$i++;
			}
		}
		
		return $data;
	}
	//approvakl out return grid data load
	public function approvalreturnheaderinformationfetchmodel($moduleid) {
		$data = array();
		$this->db->select('localmodulefieldname,fieldlabel,colmodelname,colmodelindex,colmodelviewtype,colmodeluitype,modeid,defaultdata,moduleid',false);
		$this->db->from('localmodulefield');
		$this->db->where('moduleid',52);
		$this->db->where_not_in('status',array(0,3));
		$info = $this->db->get();
		$i = 0;
		if($info->num_rows()>0){
			foreach($info->result() as $salesdata)
			{
				$data['fieldname'][$i]=$salesdata->localmodulefieldname;
				$data['fieldlabel'][$i]=$salesdata->fieldlabel;
				$data['colmodelname'][$i]=$salesdata->colmodelname;
				$data['colmodelindex'][$i]=$salesdata->colmodelindex;
				$data['colmodelviewtype'][$i]=$salesdata->colmodelviewtype;
				$data['colmodeluitype'][$i]=$salesdata->colmodeluitype;
				$data['colmoduleid'][$i]=$salesdata->moduleid;
				$data['modeid'][$i]=$salesdata->modeid;
				$data['defaultdata'][$i]=$salesdata->defaultdata;
				$i++;
			}
		}
		
		return $data;
	}
	// issue receipt grid data fetch
	public function issuereceiptheaderinformationfetchmodel($moduleid) {
		$fnames = array('overlaycreditno','creditsalesdate','paymentirtypeid','creditissuereceiptname','amtissue','amtreceipt','finalamtissue','finalamtreceipt','creditissuereceiptid','creditpaymentirtypeid','creditweightissuereceiptname','weightissue','weightreceipt','finalweightissue','finalweightreceipt','creditweightissuereceiptid','accountledgerid','processid','process','creditreference','creditreferencename','finalamthidden','finalwthidden','mainfinalamthidden','mainfinalwthidden','editoverlaystatus');
		$flabels = array('CreditNo','Date','Mode','Type','Amt Issue','Amt Recpt','Final Amt Issue','Final Amt Recpt','issuereceiptid','creditpaymentirtypeid','Wt Type','Wt Issue','Wt Recpt','Final Wt Issue','Final Wt Recpt','wtissuereceiptid','accountledgerid','processid','Process','creditreferenceid','Reference','Amount','Weight','Amount','Weight','editoverlaystatus');
		$colmodnames = array('overlaycreditno','creditsalesdate','paymentirtypeid','creditissuereceiptname','amtissue','amtreceipt','finalamtissue','finalamtreceipt','creditissuereceiptid','creditpaymentirtypeid','creditweightissuereceiptname','weightissue','weightreceipt','finalweightissue','finalweightreceipt','creditweightissuereceiptid','accountledgerid','processid','process','creditreference','creditreferencename','finalamthidden','finalwthidden','mainfinalamthidden','mainfinalwthidden','editoverlaystatus');
		$colindexnames = array('accountledger','accountledger','accountledger','accountledger','accountledger','accountledger','accountledger','accountledger','accountledger','accountledger','accountledger','accountledger','accountledger','accountledger','accountledger','accountledger','accountledger','accountledger','accountledger','accountledger','accountledger','accountledger','accountledger','accountledger','accountledger','accountledger');
		$uitypes = array('2','2','33','34','2','2','2','2','2','2','35','2','2','2','2','2','2','2','32','2','31','2','2','2','2','0');
		$viewtypes = array('1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','0');
		$dduitypeids = array('17','18','20','19','23','25','26','27','28','29');
		$data = array();
		$i=0;
		$j=0;
		foreach($fnames as $fname) {
			if( in_array($uitypes[$j],$dduitypeids) ) {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j].'name';
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]='0';
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]='2';
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			} else {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			}
			$j++;
		}
		return $data;
	}
	//tax category based tax load 
	public function taxmasterload() {
		$taxmasterid = $_GET['taxmasterid'];
		$type = $_GET['type'];
		$transmethod = $_GET['transmethod'];
		$finalamount = trim($_GET['netamount']);
			if($finalamount == '' AND !is_numeric($finalamount)){
				$finalamount = 0;
			}
		$taxdetail='';
		$j=0;
		if($type == 86 || $type == 88 || $type == 89) {
			$taxmasterid = '4';
		} else {
			$taxmasterid = $taxmasterid;
		}
		$data=$this->db->select('taxmaster.taxmasterid,taxmaster.taxmastername,tax.taxname,tax.taxrate,tax.taxid')
		->from('tax')
		->join('taxmaster','taxmaster.taxmasterid=tax.taxmasterid')
		->where('tax.taxmasterid',trim($taxmasterid))
		->where('tax.status',$this->Basefunctions->activestatus)
		->get();
		foreach($data->result() as $value){
			$amount=round((($value->taxrate/100)*$finalamount),2);
			$taxdetail->rows[$j]['id']=$j;
			$taxdetail->rows[$j]['cell']=array(
					$value->taxmasterid,
					$value->taxmastername,
					$value->taxid,
					$value->taxname,
					$value->taxrate,
					$amount
			);
			$j++;
		}
		echo json_encode($taxdetail);
	}
	// Bill tax load - Shows all details
	public function billtaxmasterload() {
		$finalamount = trim($_GET['netamount']);
		if($finalamount == '' AND !is_numeric($finalamount)){
			$finalamount = 0;
		}
		$taxdetail='';
		$j=0;
		$data=$this->db->select('taxmaster.taxmasterid,taxmaster.taxmastername,tax.taxname,tax.taxrate,tax.taxid')
		->from('tax')
		->join('taxmaster','taxmaster.taxmasterid=tax.taxmasterid')
		->where('tax.status',$this->Basefunctions->activestatus)
		->get();
		foreach($data->result() as $value){
			$amount=round((($value->taxrate/100)*$finalamount),2);
			$taxdetail->rows[$j]['id']=$j;
			$taxdetail->rows[$j]['cell']=array(
					$value->taxmasterid,
					$value->taxmastername,
					$value->taxid,
					$value->taxname,
					$value->taxrate,
					$amount
			);
			$j++;
		}
		echo json_encode($taxdetail);
	}
	// Manual Tax Calculation to load in Inner Grid
	public function manualtaxmasterload() {
		$finalamount = trim($_GET['grossamount']);
		$taxmasterid = trim($_GET['taxmasterid']);
		if($finalamount == '' AND !is_numeric($finalamount)){
			$finalamount = 0;
		}
		$taxdetail = [];
		$totaltaxamount = 0;
		$j=0;
		$data=$this->db->select('taxmaster.taxmasterid,taxmaster.taxmastername,tax.taxname,tax.taxrate,tax.taxid')
		->from('tax')
		->join('taxmaster','taxmaster.taxmasterid=tax.taxmasterid')
		->where('tax.status',$this->Basefunctions->activestatus)
		->where('tax.taxmasterid',$taxmasterid)
		->get();
		foreach($data->result() as $value) {
			$amount=round((($value->taxrate/100)*$finalamount),2);
			$totaltaxamount = number_format((float)$totaltaxamount + $amount, 2, '.', '');
			$taxdetail['id'] = $value->taxmasterid;
			$taxdetail['data'][] = array('taxmasterid'=>$value->taxmasterid,'taxmastername'=>$value->taxmastername,'taxid'=>$value->taxid,'taxname'=>$value->taxname,'rate'=>$value->taxrate,'amount'=>$amount);
			$j++;
		}
		echo json_encode(array('resultdata'=>$taxdetail,'totaltaxamount'=>$totaltaxamount));
	}
	// JSON COnvert for Tax Dat in inner Grid
	public function taxgridatajsonconvertdata() {
		$finalamount = trim($_GET['grossamount']);
		$taxmasterid = trim($_GET['taxmasterid']);
		if($finalamount == '' AND !is_numeric($finalamount)){
			$finalamount = 0;
		}
		$taxdetail='';
		$j=0;
		$data=$this->db->select('taxmaster.taxmasterid,taxmaster.taxmastername,tax.taxname,tax.taxrate,tax.taxid')
		->from('tax')
		->join('taxmaster','taxmaster.taxmasterid=tax.taxmasterid')
		->where('tax.status',$this->Basefunctions->activestatus)
		->where('tax.taxmasterid',$taxmasterid)
		->get();
		foreach($data->result() as $value){
			$amount=round((($value->taxrate/100)*$finalamount),2);
			$taxdetail->rows[$j]['id']=$j;
			$taxdetail->rows[$j]['cell']=array(
					$value->taxmasterid,
					$value->taxmastername,
					$value->taxid,
					$value->taxname,
					$value->taxrate,
					$amount
			);
			$j++;
		}
		echo json_encode($taxdetail);
	}
	//bill number grid data load loadoverlaygrid
	public function billnumbergriddatafetchmodel() {
		$billnumberid = $_GET['billnumberid'];
		$salesdetailsid = $_GET['billbasedproductid'];
		$salesdetailsid = explode(',',$salesdetailsid);
		$default = 0;
		$empty = '';
		$stoneentry = '';
		$stonedetails = '';
		$taxentry = '';
		$accountid = '';
		$accountname = '';
		$taxentrydata = '';
		$amountarray = '';
		$estimatedetail = '';
			$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
				$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
				$rateround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //Rate round-off
				$taxround = 2;
				$this->db->select(
				'sales.salesid,sales.accountid,ROUND(sales.summarydiscountamount,'.$amountround.') as summarydiscountamount,account.accountname,salesdetail.salesdetailid,salesdetail.itemtagid,salesdetail.purityid,purity.purityname,salesdetail.productid,product.productname,salesdetail.counterid,counter.countername,salesdetail.processcounterid,sizemaster.sizemastername,
				ROUND(salesdetail.grossweight,'.$round.') as grossweight,salesdetail.comment,salesdetail.tagimage,salesdetail.lstchargekeylabel,salesdetail.lstchargespanlabel,salesdetail.categoryid,
				ROUND(salesdetail.stoneweight,'.$round.') as stoneweight,
				ROUND(salesdetail.dustweight,'.$round.') as dustweight,
				ROUND(salesdetail.netweight,'.$round.') as netweight,
				ROUND(salesdetail.lstcharge,'.$round.') as lstcharge,
				ROUND(salesdetail.lstchargespan,'.$round.') as lstchargespan,
				ROUND(salesdetail.prelstcharge,'.$round.') as prelstcharge,
				ROUND(salesdetail.cgst,2) as cgst,
				ROUND(salesdetail.sgst,2) as sgst,
				ROUND(salesdetail.igst,2) as igst,
				salesdetail.pieces,salesdetail.stocktypeid,stocktype.stocktypename,ROUND(salesdetail.ratepergram,'.$rateround.') as ratepergram,ROUND(salesdetail.totalamount,'.$amountround.') as totalamount,salesdetail.pureweight,itemtag.itemtagnumber,itemtag.rfidtagno,salesdetail.salesdetailimage,salesdetail.ordernumber,DATE_FORMAT(salesdetail.orderduedate,"%d-%m-%Y") AS orderduedate,salesdetail.issuereceipttypeid,salesdetail.netwtcalctypeid,salesdetail.olditemdetails,salesdetail.modeid,ROUND(salesdetail.discountamount,'.$amountround.') as discountamount,ROUND(salesdetail.taxamount,'.$taxround.') as taxamount,ROUND(salesdetail.stoneamount,'.$amountround.') as stoneamount,salesdetail.calculationtypeid,ROUND(salesdetail.itemdiscountlimit,'.$amountround.') as itemdiscountlimit,salesdetail.discountpercent,ROUND(salesdetail.grossamount,'.$amountround.') as grossamount,salesdetail.wastage,salesdetail.making,salesdetail.wastagespan,salesdetail.makingspan,salesdetail.wastagespanlabel,salesdetail.makingchargespanlabel,salesdetail.wastagespankey,salesdetail.makingchargespankey,salesdetail.employeeid,employee.employeename,salesdetail.pendingproductid,ROUND(salesdetail.chargesamount,'.$amountround.') as chargesamount,salesdetail.flatcharge,salesdetail.flatchargespan,salesdetail.rateclone,salesdetail.rateaddition,salesdetail.estimationnumber,salesdetail.wastageweight,salesdetail.wastageless,salesdetail.taxcategoryid,salesdetail.orderitemsize,salesdetail.ordermodeid,salesdetail.lottypeid,salesdetail.purchasemodeid,salesdetail.netweightcalculationid,salesdetail.ratelesscalc,ROUND(itemtag.grossweight,'.$round.') as itemgrossweight,salesdetail.stockgrossweight,salesdetail.stockpieces,salesdetail.paymentaccountid,salesdetail.cardtypeid,cardtype.cardtypename,salesdetail.approvalcode,salesdetail.ckeyword,salesdetail.ckeywordvalue,salesdetail.ckeywordoriginalvalue,salesdetail.prewastage,salesdetail.premakingcharge,salesdetail.flatchargespanlabel,salesdetail.flatchargespankey,salesdetail.preflatcharge',false);
				$this->db->from('salesdetail');
				$this->db->join('sales','sales.salesid = salesdetail.salesid','left outer');
				$this->db->join('account','account.accountid = sales.accountid','left outer');
				$this->db->join('itemtag','itemtag.itemtagid = salesdetail.itemtagid','left outer');
				$this->db->join('stocktype','stocktype.stocktypeid = salesdetail.stocktypeid','left outer');
				$this->db->join('product','product.productid = salesdetail.productid','left outer');
				$this->db->join('purity','purity.purityid = salesdetail.purityid','left outer');
				$this->db->join('counter','counter.counterid = salesdetail.counterid','left outer');
				$this->db->join('employee','employee.employeeid = salesdetail.employeeid','left outer');
				$this->db->join('cardtype','cardtype.cardtypeid = salesdetail.cardtypeid','left outer');
				$this->db->join('sizemaster','sizemaster.sizemasterid=salesdetail.orderitemsize','left outer');
				$this->db->where('sales.salesid',$billnumberid);
				$this->db->where_not_in('salesdetail.status',array(0,3));
				$this->db->where_not_in('sales.status',array(0,3));
				$this->db->where_not_in('salesdetail.salesdetailid',$salesdetailsid);
				$this->db->where_in('salesdetail.stocktypeid',array(11,12,13,83));
				$this->db->where('salesdetail.itemtagid NOT IN (select itemtagid from salesdetail where stocktypeid = 20 and status = 1)',NULL,FALSE);
				$info = $this->db->get();
				if($info->num_rows() > 0) {
					$j=1;
					foreach($info->result() as $value) {
						$stoneentry = $this->get_sales_stoneentry_div($value->salesid,$value->salesdetailid);
						$stonedetails = $this->get_sales_stoneentry_details($value->salesid,$value->salesdetailid);
						$taxentry = $this->get_sales_taxentry_div($value->salesid,$value->salesdetailid);
						$taxentrydata = $this->get_sales_taxentry_data($value->salesid,$value->salesdetailid);
						$accountid = $value->accountid;
						$accountname = $value->accountname;
						// charges
						$ckeyword=array();
						$ckeywordvalue=array();
						$keyworddetails=array();
						$keyworddata=array();
						if($value->ckeyword != '' && $value->ckeywordvalue != '') {
							$ckeyword = explode(',',$value->ckeyword);
							$ckeywordvalue = explode(',',$value->ckeywordvalue);
						$keyworddetails = array_combine($ckeyword, $ckeywordvalue);
						$k = 0;
						foreach($keyworddetails as $k => $l){
							$keyworddata[] = $k. ":".$l;
							$k++;
						}
						$keyword_str = implode (",", $keyworddata);
						}else{
							$keyword_str ='';
						}
						$ckeyword1=array();
						$ckeywordoriginalvalue=array();
						$keyworddetails1=array();
						$keyworddata1=array();
						if($value->ckeyword != ''){
							$ckeyword1 = explode(',',$value->ckeyword);
							$ckeywordoriginalvalue = explode(',',$value->ckeywordoriginalvalue); 
						$keyworddetails1 = array_combine($ckeyword1, $ckeywordoriginalvalue);
						$m = 0;
						foreach($keyworddetails1 as $m => $p){
							$keyworddata1[] = $m. ":".$p;
							$m++;
						}
						$keyword_str1 = implode (",", $keyworddata1);
						}else{
							$keyword_str1 ='';
						}
						$branchstateid = $this->Basefunctions->branchstateid;
						$accountstateid = $this->Basefunctions->singlefieldfetch('stateid','accountid','account',$accountid);
						if($accountstateid == $branchstateid) {  // branch state
							$gstmodeids = 2;
						} else { // other state
							$gstmodeids = 3;
						}
						$itemtaxamount = $value->taxamount;
						$itemtotalamount = number_format((float) $value->totalamount, $amountround, '.', '');
						$estimatedetail->rows[$j]['id'] = $value->salesdetailid;
						$estimatedetail->rows[$j]['cell']=array(
								$j,
								20,
								'Return',
								$default,
								$empty,
								$itemtotalamount,
								$default,
								$value->itemtagnumber,
								$value->rfidtagno,
								$value->productid,
								$value->productname,
								$value->purityid,
								$value->purityname,
								$value->counterid,
								$value->countername,
								$value->grossweight,
								$value->stoneweight,
								$value->netweight,
								$value->pieces,
								$value->ratepergram,
								$value->wastage,
								$value->making,
								$value->stoneamount,
								$itemtaxamount,
								$default,
								$value->grossamount,
								$value->employeeid,
								$value->employeename,
								$value->wastagespan,
								$value->makingspan,
								$stoneentry,
								$value->flatcharge,
								$value->flatchargespan,
								$default,
								$default,
								$value->chargesamount,
								$value->wastagespan,
								$value->makingspan,
								$value->flatchargespan,
								$default,
								$default,
								$default,
								$default,
								$value->modeid,
								$default,
								$value->itemtagid,
								$default,
								$default,
								$default,
								$taxentry,
								$default,
								$default,
								$default,
								$default,
								$default,
								$empty,
								$empty,
								$value->salesdetailid,
								$default,
								$default,
								$taxentrydata,
								$default,
								$stonedetails,
								$empty,
								$value->wastagespanlabel,
								$value->makingchargespanlabel,
								$value->wastagespankey,
								$value->makingchargespankey,
								$value->ckeyword,
								$value->ckeywordvalue,
								$value->ckeywordoriginalvalue,
								$keyword_str,
								$keyword_str1,
								$value->chargesamount,
								$default,
								$default,
								$default,
								$default,
								$default,
								$empty,
								$empty,
								$empty,
								$empty,
								$empty,
								$value->processcounterid,
								$empty,
								$empty,
								$value->prewastage,
								$value->premakingcharge,
								$default,
								1,
								$empty,
								1,
								$empty,
								$default,
								$default,
								1,
								$empty,
								1,
								$empty,
								1,
								$empty,
								$default,
								$default,
								$default,
								$default,
								$default,
								$default,
								$value->sizemastername,
								$value->comment,
								$empty,
								$empty,
								$empty,
								$empty,
								$empty,
								$empty,
								$empty,
								$empty,
								$empty,
								$empty,
								1,
								$empty,
								$empty,
								$empty,
								$empty,
								$empty,
								$empty,
								$empty,
								$value->tagimage,
								$value->lstcharge,
								$value->lstchargespan,
								$value->lstchargespan,
								$value->lstchargekeylabel,
								$value->lstchargespanlabel,
								$value->prelstcharge,
								$value->flatchargespanlabel,
								$value->flatchargespankey,
								$value->preflatcharge,
								$empty,
								$empty,
								$empty,
								$default,
								$default,
								$default,
								$default,
								$default,
								$empty,
								$empty,
								$empty,
								$empty,
								$default
						);
						$j++;
						$amountarray = array('discount'=>$value->summarydiscountamount);
					}
				}else{
					$estimatedetail = ' ';
				}
				$estimatearray['accountid'] = $accountid; 
				$estimatearray['accountname'] = $accountname; 
				$estimatearray['estimatedata'] = $estimatedetail; 
				$estimatearray['amountarray'] = $amountarray; 
				echo json_encode($estimatearray);
	}
	//old jewel voucher grid data load loadoverlaygrid
	public function oldjewelvouchergriddatafetchmodel() {
		$stocktype = $_GET['stocktype'];
		$accountid = $_GET['accountid'];
		$transmethod = $_GET['transactionid'];
		$salesdetailsid = $_GET['billbasedproductid'];
		if(empty($salesdetailsid))
		{
			$salesdetailsid = 1;
		}
		$default = 0;
		$empty = '';
		$billdetail='';
		$stoneentry = '';
		$stonedetails = '';
		$taxentry = '';
		$accountname = '';
		$taxentrydata = '';
		$amountarray = '';
		$estimatedetail = '';
		$j=0;
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
				$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
				$rateround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //Rate round-off
		
			$info = $this->db->query("SELECT `salesdetail`.`salesdetailid`,`sales`.`salesnumber`, `stocktype`.`stocktypename`, `product`.`productname`, `counter`.`countername`,ROUND(salesdetail.grossweight,'".$round."') as grossweight,ROUND(salesdetail.stoneweight,'".$round."') as stoneweight,ROUND(salesdetail.netweight,'".$round."') as netweight, `salesdetail`.`pieces`,ROUND(salesdetail.ratepergram,'".$rateround."') as ratepergram, ROUND(salesdetail.grossamount,'".$amountround."') as grossamount,ROUND(salesdetail.chargesamount,'".$amountround."') as chargesamount,ROUND(salesdetail.stoneamount,'".$amountround."') as stoneamount,ROUND(salesdetail.taxamount,'".$amountround."') as taxamount, ROUND(salesdetail.discountamount,'".$amountround."') as discountamount,ROUND(salesdetail.totalamount,'".$amountround."') as totalamount,`salesdetail`.`productid`,`salesdetail`.`purityid`,`salesdetail`.`counterid`,`purity`.`purityname`,`salesdetail`.`employeeid`,`employee`.`employeename`,`salesdetail`.`itemtagid`,`salesdetail`.`ratelesscalc`,ROUND(salesdetail.dustweight,'".$round."') as dustweight,`salesdetail`.`wastageless`,`salesdetail`.`modeid`,`salesdetail`.`olditemdetails`,`olditemdetails`.`olditemdetailsname`,`salesdetail`.`netweightcalculationid` FROM `salesdetail`
			LEFT OUTER JOIN `stocktype` ON `stocktype`.`stocktypeid`=`salesdetail`.`stocktypeid`
			LEFT OUTER JOIN `product` ON `product`.`productid`=`salesdetail`.`productid`
			LEFT OUTER JOIN `purity` ON `purity`.`purityid`=`salesdetail`.`purityid`
			LEFT OUTER JOIN `counter` ON `counter`.`counterid`=`salesdetail`.`counterid`
			LEFT OUTER JOIN `employee` ON `employee`.`employeeid`=`salesdetail`.`employeeid`
			LEFT OUTER JOIN `sales` ON `sales`.`salesid`=`salesdetail`.`salesid`
			LEFT OUTER JOIN `olditemdetails` ON `olditemdetails`.`olditemdetailsid`=`salesdetail`.`olditemdetails`
			WHERE `sales`.`accountid` = '".$accountid."'
			AND `salesdetail`.`modeid` IN(2)
			AND `sales`.`salesnumber` NOT IN('pending')
			AND `salesdetail`.`salesdetailid` NOT IN(".$salesdetailsid.")
			AND `salesdetail`.`salesid` NOT IN(select salesid from salesdetail where stocktypeid in(20,31,16,13,12,74,11,28,78,70,49,26,27,29,79,37,38,39,62,63,65,66) group by salesid)
			AND `salesdetail`.`status` = 1");
		
		if($info->num_rows() > 0) {
					$j=1;
					foreach($info->result() as $value) {
					$estimatedetail->rows[$j]['id'] = $value->salesdetailid;
					$estimatedetail->rows[$j]['cell']=array(
							$j,
							$stocktype,
							$value->stocktypename,
							$default,
							$empty,
							$value->totalamount,
							$default,
							$empty,
							$empty,
							$value->productid,
							$value->productname,
							$value->purityid,
							$value->purityname,
							$value->counterid,
							$value->countername,
							$value->grossweight,
							$value->stoneweight,
							$value->netweight,
							$value->pieces,
							$value->ratepergram,
							$default,
							$default,
							$default,
							$default,
							$default,
							$value->grossamount,
							$value->employeeid,
							$value->employeename,
							$empty,
							$empty,
							$empty,
							$default,
							$empty,
							$default,
							$default,
							$default,
							$empty,
							$empty,
							$empty,
							$value->dustweight,
							$value->wastageless,
							$default,
							$default,
							$value->modeid,
							$default,
							$value->itemtagid,
							$default,
							$default,
							$default,
							$taxentry,
							$default,
							$default,
							$default,
							$default,
							$default,
							$empty,
							$empty,
							$value->salesdetailid,
							$value->netweightcalculationid,
							$value->ratelesscalc,
							$empty,
							$default,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$default,
							$default,
							$default,
							$default,
							$value->olditemdetails,
							1,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$value->olditemdetailsname,
							$empty,
							$empty,
							$default,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$default,
							$empty,
							$default,
							$empty,
							$default,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
					);
					$j++;
					}
				}else{
					$estimatedetail = ' ';
				}
				$estimatearray['estimatedata'] = $estimatedetail; 
				echo json_encode($estimatearray);
	}
	//approval out return grid data load loadoverlaygrid
	public function approvalreturngriddatafetchmodel() {
		$billnumber = $_GET['salesnumber'];
		$billwhere = 'sales.salesnumber';
		if($billnumber == 'all'){
			$billnumber = $_GET['accountid'];
			$_GET['salesnumber'] = 1;
			$billwhere = 'sales.accountid';
		}
		$salesdetailsid = $_GET['billbasedproductid'];
		$salesdetailsid = explode(',',$salesdetailsid);
		$billdetail='';
		$j=0;
		$default = 0;
		$empty = '';
		$stoneentry = '';
		$stonedetails = '';
		$taxentry = '';
		$taxentrydata = '';
		$stocktypeid = $_GET['stocktypeid'];
		if($stocktypeid == 66){
			$whereinstocktype = array(63);
		}else if($stocktypeid == 65){
			$whereinstocktype = array(62);
		}else if($stocktypeid == 25){
			$whereinstocktype = array(24);
		}
		$stocktypename = ''; 
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$rateround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //Rate round-off
		$taxround = 2;
		$info=$this->db->select('sales.salestransactiontypeid,sales.salesnumber,sales.salesid,salesdetail.salesdetailid,itemtag.itemtagnumber,itemtag.rfidtagno,product.productname,purity.purityname,counter.countername,ROUND(salesdetail.grossweight,'.$round.') as grossweight,ROUND(salesdetail.stoneweight,'.$round.') as stoneweight,ROUND(salesdetail.netweight,'.$round.') as netweight,salesdetail.pieces,salesdetail.productid,salesdetail.purityid,salesdetail.counterid,salesdetail.employeeid,employee.employeename,salesdetail.modeid,salesdetail.itemtagid,salesdetail.oldjewelvoucherstatus,ROUND(salesdetail.ratepergram,'.$rateround.') as ratepergram,ROUND(salesdetail.totalamount,'.$amountround.') as totalamount,salesdetail.netwtcalctypeid,ROUND(salesdetail.taxamount,'.$taxround.') as taxamount,salesdetail.calculationtypeid,ROUND(salesdetail.grossamount,'.$amountround.') as grossamount,ROUND(salesdetail.stoneamount,'.$amountround.') as stoneamount,salesdetail.wastage,salesdetail.making,salesdetail.wastagespan,salesdetail.makingspan,salesdetail.wastagespanlabel,salesdetail.makingchargespanlabel,salesdetail.wastagespankey,salesdetail.makingchargespankey,ROUND(salesdetail.chargesamount,'.$amountround.') as chargesamount,salesdetail.flatcharge,salesdetail.flatchargespan,salesdetail.taxcategoryid,salesdetail.ckeyword,salesdetail.ckeywordvalue,salesdetail.ckeywordoriginalvalue,salesdetail.processcounterid,,salesdetail.comment,salesdetail.tagimage,ROUND(salesdetail.lstcharge,'.$amountround.') as lstcharge,ROUND(salesdetail.lstchargespan,'.$amountround.') as lstchargespan,ROUND(salesdetail.prelstcharge,'.$amountround.') as prelstcharge,salesdetail.lstchargekeylabel,salesdetail.lstchargespanlabel,salesdetail.flatchargespanlabel,salesdetail.flatchargespankey,ROUND(salesdetail.preflatcharge,'.$amountround.') as preflatcharge')
		->from('salesdetail')
		->join('itemtag','itemtag.itemtagid=salesdetail.itemtagid','left outer')
		->join('product','product.productid=salesdetail.productid','left outer')
		->join('purity','purity.purityid=salesdetail.purityid','left outer')
		->join('counter','counter.counterid=salesdetail.counterid','left outer')
		->join('employee','employee.employeeid=salesdetail.employeeid','left outer')
		->join('sales','sales.salesid=salesdetail.salesid','left outer')
		->where($billwhere,trim($billnumber))
		->where_in('salesdetail.modeid',2)
		->where_in('salesdetail.stocktypeid',$whereinstocktype)
		->where_in('salesdetail.approvalstatus',array(1))
		->where_not_in('salesdetail.salesdetailid',$salesdetailsid)
		->where('salesdetail.status',$this->Basefunctions->activestatus)
		->get();
		if($info->num_rows() > 0) {
					$j=1;
					foreach($info->result() as $value) {
						// charges
						$ckeyword=array();
						$ckeywordvalue=array();
						$keyworddetails=array();
						$keyworddata=array();
						if($value->ckeyword != '' && $value->ckeywordvalue != ''){
							$ckeyword = explode(',',$value->ckeyword);
							$ckeywordvalue = explode(',',$value->ckeywordvalue);
						$keyworddetails = array_combine($ckeyword, $ckeywordvalue);
						$k = 0;
						foreach($keyworddetails as $k => $l){
							$keyworddata[] = $k. ":".$l;
							$k++;
						}
						$keyword_str = implode (",", $keyworddata);
						}else{
							$keyword_str ='';
						}
						$ckeyword1=array();
						$ckeywordoriginalvalue=array();
						$keyworddetails1=array();
						$keyworddata1=array();
						if($value->ckeyword != ''){
							$ckeyword1 = explode(',',$value->ckeyword);
							$ckeywordoriginalvalue = explode(',',$value->ckeywordoriginalvalue); 
							$keyworddetails1 = array_combine($ckeyword1, $ckeywordoriginalvalue);
						$m = 0;
						foreach($keyworddetails1 as $m => $p){
							$keyworddata1[] = $m. ":".$p;
							$m++;
						}
						$keyword_str1 = implode (",", $keyworddata1);
						}else{
							$keyword_str1 ='';
						}
						$stoneentry = $this->get_sales_stoneentry_div($value->salesid,$value->salesdetailid);
						$stonedetails = $this->get_sales_stoneentry_details($value->salesid,$value->salesdetailid);
						$taxentry = $this->get_sales_taxentry_div($value->salesid,$value->salesdetailid);
						$taxentrydata = $this->get_sales_taxentry_data($value->salesid,$value->salesdetailid);
						$estimatedetail->rows[$j]['id'] = $value->salesdetailid;
						if($stocktypeid == 65){
							$stocktypename = 'App Out Return';
						}else if($stocktypeid == 74){
							$stocktypename = 'A.O.Tag.Sales';
						}else if($stocktypeid == 66){
							$stocktypename = 'AO.UnTag Return';
						}else if($stocktypeid == 16){
							$stocktypename = 'AO.UnTag Sales';
						}else if($stocktypeid == 25){
							$stocktypename = 'App In Return';
						}
						if($stocktypeid == 66 || $stocktypeid == 16 || $stocktypeid == 25){
							$untagapprovalwt = $this->Salesmodel->appoutuntagupdate(array($value->salesdetailid),'3',$value->salestransactiontypeid);
							$value->grossweight = $untagapprovalwt['pendinggwt'];
							$value->netweight = $untagapprovalwt['pendingnwt'];
							$value->pieces = $untagapprovalwt['pendingpieces'];
						}
						$estimatedetail->rows[$j]['cell']=array(
							$stocktypeid,
							$stocktypename,
							$default,
							$empty,
							$default,
							$default,
							$value->itemtagnumber,
							$value->rfidtagno,
							$value->productid,
							$value->productname,
							$value->purityid,
							$value->purityname,
							$value->counterid,
							$value->countername,
							$value->grossweight,
							$value->stoneweight,
							$value->netweight,
							$value->pieces,
							$value->ratepergram,
							$value->wastage,
							$value->making,
							$value->stoneamount,
							$value->taxamount,
							$default,
							$value->grossamount,
							$value->employeeid,
							$value->employeename,
							$value->wastagespan,
							$value->makingspan,
							$stoneentry,
							$value->flatcharge,
							$value->flatchargespan,
							$default,
							$default,
							$value->chargesamount,
							$value->wastagespan,
							$empty,
							$empty,
							$default,
							$default,
							$default,
							$default,
							$value->modeid,
							$default,
							$value->itemtagid,
							$default,
							$default,
							$default,
							$default,
							$default,
							$default,
							$default,
							$default,
							$default,
							$empty,
							$empty,
							$value->salesdetailid,
							$default,
							$default,
							$empty,
							$default,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$default,
							$default,
							$default,
							$default,
							$default,
							$value->oldjewelvoucherstatus,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$value->processcounterid,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$value->comment,
							$empty,
							'0000-00-00',
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$value->tagimage,
							$value->lstcharge,
							$value->lstchargespan,
							$value->lstchargespan,
							$value->lstchargekeylabel,
							$value->lstchargespanlabel,
							$value->prelstcharge,
							$value->flatchargespanlabel,
							$value->flatchargespankey,
							$value->preflatcharge,
							$empty,
							$empty,
							$value->salesnumber,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty
					);
					$j++;
					}
				}else{
					$estimatedetail = ' ';
				}
				$estimatearray['estimatedata'] = $estimatedetail; 
				echo json_encode($estimatearray);
	}
	//order grid data load loadoverlaygrid
	public function ordergriddatafetchmodel() {
		$orderid = $_GET['orderid'];
		$status = $_GET['status'];
		$substocktypeid = $_GET['substocktypeid'];
		$vendoraccountid = $_GET['accountid'];
		$povendormanagement = 0;
		$selectedstocktypeid = 1;
		$povendormanagement = $_GET['povendormanagement'];
		$poautomaticvendor = $_GET['poautomaticvendor'];
		$selectedstocktypeid = $_GET['selectedstocktypeid'];
		$salesdetailsid = $_GET['billbasedproductid'];
		$ratearray = $_GET['ratearray'];
		$salesdetailvendorid = '1=1';
		$rate_apply = json_decode( json_encode($ratearray), true);
		$explodesalesdetailsid = explode(',',$salesdetailsid);
		if(empty($salesdetailsid)) {
			$salesdetailsid = 1;
		}
		if($orderid == 'all') {
			if($status == 2) {
				$orderid = 1;
				$billwhere = '1';
				if($selectedstocktypeid == 87 AND $_GET['stocktypeid'] == 86) {
					$salesdetailvendorid = "1=1";
				} else {
					$salesdetailvendorid = "salesdetail.ordervendorid = $vendoraccountid";
				}
			} else {
				$orderid = $_GET['accountid'];
				$billwhere = 'sales.accountid';
				if($substocktypeid != '1') {
					$genposalesstatus_where = 'salesdetail.poorderstatusid IN(21) AND salesdetail.generatepo IN(1)';
					$genpostatus_where = '1=1';
				} else {
					$genposalesstatus_where = '1=1';
					$genpostatus_where = '1=1';
				}
			}
			$_GET['orderid'] = 1;
		} else {
			if($status == 5) {
				$billwhere = 'salesdetail.salesid'; 
			} else if($status == 2) {
				$billwhere = 'sales.salesid';
				if($poautomaticvendor == 1) {
					$salesdetailvendorid = "salesdetail.ordervendorid = $vendoraccountid";
				}
			} else if($status == 3) {
				$billwhere = 'sales.salesid';
				if($substocktypeid != '1') {
					$genposalesstatus_where = 'salesdetail.poorderstatusid IN(21) AND salesdetail.generatepo IN(1)';
					$genpostatus_where = '1=1';
				} else {
					$genposalesstatus_where = '1=1';
					$genpostatus_where = '1=1';
				}
			} else if($status == 4 && $selectedstocktypeid == 89) {
				$billwhere = 'salesdetail.salesid'; 
			}
		}
		$billdetail='';
		$j=0;
		$default = 0;
		$empty = '';
		$stoneentry = '';
		$stonedetails = '';
		$taxentry = '';
		$taxentrydata = '';
		$stocktypeid = $_GET['stocktypeid'];
		$selectedaccountypeid = $_GET['accountypeid'];
		$stocktypename = '';
		$finalstocktypeid = 1;
		$vendorid = 1;
		$ordernumber = '';
		$placeordernumber = '';
		$receiveordernumber = '';
		$orderitemratefixname = '';
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$rateround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //Rate round-off
		$receiveorderconcept = $this->Basefunctions->get_company_settings('receiveorderconcept');
		$taxround = 2;
		if($status == 5) {
			if($receiveorderconcept == 0) {
				$extracondition = "AND 1=1";
				$info=$this->db->query("SELECT `rosales`.`salesnumber` as `rosalesnumber`,`sales`.`salesnumber` as `orsalesnumber`,`posales`.`salesnumber` as `posalesnumber`,`rosalesdetail`.`salesid`, `rosalesdetail`.`salesdetailid`, `itemtag`.`itemtagnumber`, `itemtag`.`rfidtagno`, `product`.`productname`,  `category`.`categoryname`, `purity`.`purityname`, `counter`.`countername`, ROUND(rosalesdetail.grossweight, '".$round."') as grossweight, ROUND(rosalesdetail.stoneweight,'".$round."') as stoneweight, ROUND(rosalesdetail.netweight, '".$round."') as netweight, ROUND(rosalesdetail.caratweight,'".$round."') as itemcaratweight, `rosalesdetail`.`pieces`, `rosalesdetail`.`productid`, `rosalesdetail`.`categoryid`, `rosalesdetail`.`purityid`, `rosalesdetail`.`counterid`, `rosalesdetail`.`employeeid`, `employee`.`employeename`, `rosalesdetail`.`modeid`, `rosalesdetail`.`itemtagid`, `rosalesdetail`.`oldjewelvoucherstatus`, ROUND(rosalesdetail.ratepergram, '".$rateround."') as ratepergram, ROUND(rosalesdetail.totalamount, '".$amountround."') as totalamount, `rosalesdetail`.`netwtcalctypeid`, ROUND(rosalesdetail.taxamount, '".$taxround."') as taxamount, `rosalesdetail`.`calculationtypeid`, ROUND(rosalesdetail.grossamount, '".$amountround."') as grossamount, ROUND(rosalesdetail.stoneamount, '".$amountround."') as stoneamount,  ROUND(rosalesdetail.wastage, '".$amountround."') as wastage,ROUND(rosalesdetail.making, '".$amountround."') as making , `rosalesdetail`.`wastagespan`, `rosalesdetail`.`makingspan`, `rosalesdetail`.`wastagespanlabel`, `rosalesdetail`.`makingchargespanlabel`, `rosalesdetail`.`wastagespankey`, `rosalesdetail`.`makingchargespankey`, ROUND(rosalesdetail.chargesamount, '".$amountround."') as chargesamount,ROUND(rosalesdetail.flatcharge, '".$amountround."') as flatcharge, `rosalesdetail`.`flatchargespan`, `rosalesdetail`.`taxcategoryid`, `rosalesdetail`.`ckeyword`, `rosalesdetail`.`ckeywordvalue`, `rosalesdetail`.`ckeywordoriginalvalue`, `rosalesdetail`.`processcounterid`, `rosalesdetail`.`stocktypeid`, `stocktype`.`stocktypename`, `sizemaster`.`sizemastername`, `rosalesdetail`.`orderitemsize`, `rosalesdetail`.`prewastage`, `rosalesdetail`.`premakingcharge`, `rosalesdetail`.`comment`,DATE_FORMAT(rosalesdetail.orderduedate,'%d-%m-%Y') AS orderduedate,`rosalesdetail`.`tagimage`,ROUND(rosalesdetail.lstcharge,'".$amountround."') as lstcharge,ROUND(rosalesdetail.lstchargespan,'".$amountround."') as lstchargespan,ROUND(rosalesdetail.prelstcharge,'".$amountround."') as prelstcharge,rosalesdetail.lstchargekeylabel,rosalesdetail.lstchargespanlabel,salesdetail.flatchargespanlabel,salesdetail.flatchargespankey,ROUND(salesdetail.preflatcharge,".$amountround.") as preflatcharge,rosalesdetail.orderitemratefix as orderitemratefix,DATE_FORMAT(rosalesdetail.vendororderduedate,'%d-%m-%Y') AS vendororderduedate,rosalesdetail.ordervendorid, ROUND(rosalesdetail.repaircharge,'".$amountround."') as repaircharge, rosalesdetail.repairchargekeylabel,rosalesdetail.repairchargespanlabel, ROUND(rosalesdetail.prerepaircharge,'".$amountround."') as prerepaircharge, ROUND(rosalesdetail.repairchargespan,'".$amountround."') as repairchargespan, ROUND(rosalesdetail.repairchargespan,'".$amountround."') as repairchargeclone,rosalesdetail.proditemrate, rosalesdetail.taxcategoryid FROM `salesdetail` LEFT OUTER JOIN salesdetail as posalesdetail ON posalesdetail.presalesdetailid=salesdetail.salesdetailid and posalesdetail.status NOT IN (0,3) LEFT OUTER JOIN salesdetail as rosalesdetail ON rosalesdetail.presalesdetailid=posalesdetail.salesdetailid and rosalesdetail.status NOT IN (0,3) LEFT OUTER JOIN `itemtag` ON `itemtag`.`itemtagid`=`rosalesdetail`.`itemtagid` LEFT OUTER JOIN `stocktype` ON `stocktype`.`stocktypeid`=`rosalesdetail`.`stocktypeid` LEFT OUTER JOIN `orderstatus` ON `orderstatus`.`orderstatusid`=`rosalesdetail`.`orderstatusid` LEFT OUTER JOIN repairstatus ON repairstatus.repairstatusid = rosalesdetail.repairstatusid LEFT OUTER JOIN `sizemaster` ON `sizemaster`.`sizemasterid`=`rosalesdetail`.`orderitemsize` LEFT OUTER JOIN `product` ON `product`.`productid`=`rosalesdetail`.`productid` LEFT OUTER JOIN category ON category.categoryid = rosalesdetail.categoryid LEFT OUTER JOIN `purity` ON `purity`.`purityid`=`rosalesdetail`.`purityid` LEFT OUTER JOIN `counter` ON `counter`.`counterid`=`rosalesdetail`.`counterid` LEFT OUTER JOIN `employee` ON `employee`.`employeeid`=`rosalesdetail`.`employeeid` LEFT OUTER JOIN `sales` as `posales` ON `posales`.`salesid`=`posalesdetail`.`salesid` LEFT OUTER JOIN `sales` as `rosales` ON `rosales`.`salesid`=`rosalesdetail`.`salesid` LEFT OUTER JOIN `sales`  ON `sales`.`salesid`=`salesdetail`.`salesid` WHERE `rosalesdetail`.`orderstatusid` = '5' AND `rosalesdetail`.`stocktypeid` = '82' AND `rosalesdetail`.`salesdetailid` NOT IN(".$salesdetailsid.") ".$extracondition." AND `rosalesdetail`.`status` = 1 and ".$billwhere." = '".$orderid."'");
			} else {
				$extracondition = "AND rosalesdetail.lotid !=1";
				$info=$this->db->query("SELECT `rosales`.`salesnumber` as `rosalesnumber`,`sales`.`salesnumber` as `orsalesnumber`,`posales`.`salesnumber` as `posalesnumber`,`rosalesdetail`.`salesid`, `rosalesdetail`.`salesdetailid`, `itemtag`.`itemtagnumber`, `itemtag`.`rfidtagno`, `product`.`productname`, `category`.`categoryname`, `purity`.`purityname`, `counter`.`countername`, ROUND(rosalesdetail.grossweight, '".$round."') as grossweight, ROUND(rosalesdetail.stoneweight,'".$round."') as stoneweight, ROUND(rosalesdetail.netweight, '".$round."') as netweight, ROUND(rosalesdetail.caratweight,'".$round."') as itemcaratweight, `rosalesdetail`.`pieces`, `rosalesdetail`.`productid`, `rosalesdetail`.`categoryid`, `rosalesdetail`.`purityid`, `rosalesdetail`.`counterid`, `rosalesdetail`.`employeeid`, `employee`.`employeename`, `rosalesdetail`.`modeid`, `rosalesdetail`.`itemtagid`, `rosalesdetail`.`oldjewelvoucherstatus`, ROUND(rosalesdetail.ratepergram, '".$rateround."') as ratepergram, ROUND(rosalesdetail.totalamount, '".$amountround."') as totalamount, `rosalesdetail`.`netwtcalctypeid`, ROUND(rosalesdetail.taxamount, '".$taxround."') as taxamount, `rosalesdetail`.`calculationtypeid`, ROUND(rosalesdetail.grossamount, '".$amountround."') as grossamount, ROUND(rosalesdetail.stoneamount, '".$amountround."') as stoneamount,  ROUND(rosalesdetail.wastage, '".$amountround."') as wastage,ROUND(rosalesdetail.making, '".$amountround."') as making , `rosalesdetail`.`wastagespan`, `rosalesdetail`.`makingspan`, `rosalesdetail`.`wastagespanlabel`, `rosalesdetail`.`makingchargespanlabel`, `rosalesdetail`.`wastagespankey`, `rosalesdetail`.`makingchargespankey`, ROUND(rosalesdetail.chargesamount, '".$amountround."') as chargesamount,ROUND(rosalesdetail.flatcharge, '".$amountround."') as flatcharge, `rosalesdetail`.`flatchargespan`, `rosalesdetail`.`taxcategoryid`, `rosalesdetail`.`ckeyword`, `rosalesdetail`.`ckeywordvalue`, `rosalesdetail`.`ckeywordoriginalvalue`, `rosalesdetail`.`processcounterid`, `rosalesdetail`.`stocktypeid`, `stocktype`.`stocktypename`, `sizemaster`.`sizemastername`, `rosalesdetail`.`orderitemsize`, `rosalesdetail`.`prewastage`, `rosalesdetail`.`premakingcharge`, `rosalesdetail`.`comment`,DATE_FORMAT(rosalesdetail.orderduedate,'%d-%m-%Y') AS orderduedate,`rosalesdetail`.`tagimage`,ROUND(rosalesdetail.lstcharge,'".$amountround."') as lstcharge,ROUND(rosalesdetail.lstchargespan,'".$amountround."') as lstchargespan,ROUND(rosalesdetail.prelstcharge,'".$amountround."') as prelstcharge,rosalesdetail.lstchargekeylabel,rosalesdetail.lstchargespanlabel,salesdetail.flatchargespanlabel,salesdetail.flatchargespankey,ROUND(salesdetail.preflatcharge,".$amountround.") as preflatcharge,rosalesdetail.orderitemratefix as orderitemratefix,DATE_FORMAT(rosalesdetail.vendororderduedate,'%d-%m-%Y') AS vendororderduedate,rosalesdetail.ordervendorid, ROUND(rosalesdetail.repaircharge,'".$amountround."') as repaircharge, rosalesdetail.repairchargekeylabel,rosalesdetail.repairchargespanlabel, ROUND(rosalesdetail.prerepaircharge,'".$amountround."') as prerepaircharge, ROUND(rosalesdetail.repairchargespan,'".$amountround."') as repairchargespan, ROUND(rosalesdetail.repairchargespan,'".$amountround."') as repairchargeclone,rosalesdetail.proditemrate, rosalesdetail.taxcategoryid FROM `salesdetail` LEFT OUTER JOIN salesdetail as po1salesdetail ON po1salesdetail.presalesdetailid=salesdetail.salesdetailid and po1salesdetail.status NOT IN (0,3) LEFT OUTER JOIN salesdetail as posalesdetail ON posalesdetail.presalesdetailid=po1salesdetail.salesdetailid and posalesdetail.status NOT IN (0,3) LEFT OUTER JOIN salesdetail as rosalesdetail ON rosalesdetail.presalesdetailid=posalesdetail.salesdetailid and rosalesdetail.status NOT IN (0,3) LEFT OUTER JOIN `itemtag` ON `itemtag`.`itemtagid`=`rosalesdetail`.`itemtagid` LEFT OUTER JOIN `stocktype` ON `stocktype`.`stocktypeid`=`rosalesdetail`.`stocktypeid` LEFT OUTER JOIN `orderstatus` ON `orderstatus`.`orderstatusid`=`rosalesdetail`.`orderstatusid` LEFT OUTER JOIN repairstatus ON repairstatus.repairstatusid = rosalesdetail.repairstatusid LEFT OUTER JOIN `sizemaster` ON `sizemaster`.`sizemasterid`=`rosalesdetail`.`orderitemsize` LEFT OUTER JOIN `product` ON `product`.`productid`=`rosalesdetail`.`productid` LEFT OUTER JOIN category ON category.categoryid = rosalesdetail.categoryid LEFT OUTER JOIN `purity` ON `purity`.`purityid`=`rosalesdetail`.`purityid` LEFT OUTER JOIN `counter` ON `counter`.`counterid`=`rosalesdetail`.`counterid` LEFT OUTER JOIN `employee` ON `employee`.`employeeid`=`rosalesdetail`.`employeeid` LEFT OUTER JOIN `sales` as `posales` ON `posales`.`salesid`=`posalesdetail`.`salesid` LEFT OUTER JOIN `sales` as `rosales` ON `rosales`.`salesid`=`rosalesdetail`.`salesid` LEFT OUTER JOIN `sales` as `po1sales` ON `po1sales`.`salesid`=`po1salesdetail`.`salesid` LEFT OUTER JOIN `sales`  ON `sales`.`salesid`=`salesdetail`.`salesid` WHERE `rosalesdetail`.`orderstatusid` = '5' AND `rosalesdetail`.`stocktypeid` = '82' AND `rosalesdetail`.`salesdetailid` NOT IN(".$salesdetailsid.") ".$extracondition." AND `rosalesdetail`.`status` = 1 and ".$billwhere." = '".$orderid."'");
			}
		} else if($status == 2) {
			if($stocktypeid == 75) {
				$orderrepair = 'salesdetail.orderstatusid = 2';
				$stocktypewhere = 'salesdetail.stocktypeid = 75';
			} else if($stocktypeid == 86) {
				$orderrepair = 'salesdetail.repairstatusid = 2 ';
				$stocktypewhere = 'salesdetail.stocktypeid = 86';
			}
			$info=$this->db->select('sales.salesid,sales.salesnumber as ordernumber,salesdetail.salesdetailid,itemtag.itemtagnumber,itemtag.rfidtagno,product.productname,category.categoryname,purity.purityname,counter.countername,ROUND(salesdetail.grossweight,'.$round.') as grossweight,ROUND(salesdetail.stoneweight,'.$round.') as stoneweight,ROUND(salesdetail.netweight,'.$round.') as netweight, ROUND(salesdetail.caratweight,'.$round.') as itemcaratweight, salesdetail.pieces,salesdetail.productid,`salesdetail`.`categoryid`,  salesdetail.purityid,salesdetail.counterid,salesdetail.employeeid,employee.employeename,salesdetail.modeid,salesdetail.itemtagid,salesdetail.oldjewelvoucherstatus,ROUND(salesdetail.ratepergram,'.$rateround.') as ratepergram,ROUND(salesdetail.totalamount,'.$amountround.') as totalamount,salesdetail.netwtcalctypeid,ROUND(salesdetail.taxamount,'.$taxround.') as taxamount,salesdetail.calculationtypeid,ROUND(salesdetail.grossamount,'.$amountround.') as grossamount,ROUND(salesdetail.stoneamount,'.$amountround.') as stoneamount,ROUND(salesdetail.wastage,'.$amountround.') as wastage ,ROUND(salesdetail.making,'.$amountround.') as making,salesdetail.wastagespan,salesdetail.makingspan,salesdetail.wastagespanlabel,salesdetail.makingchargespanlabel,salesdetail.wastagespankey,salesdetail.makingchargespankey,ROUND(salesdetail.chargesamount,'.$amountround.') as chargesamount,ROUND(salesdetail.flatcharge,'.$amountround.') as flatcharge ,salesdetail.flatchargespan,salesdetail.taxcategoryid,salesdetail.ckeyword,salesdetail.ckeywordvalue,salesdetail.ckeywordoriginalvalue,salesdetail.processcounterid,salesdetail.stocktypeid,stocktype.stocktypename,sizemaster.sizemastername,salesdetail.orderitemsize,salesdetail.prewastage,salesdetail.premakingcharge,salesdetail.comment,DATE_FORMAT(salesdetail.orderduedate,"%d-%m-%Y") AS orderduedate,salesdetail.tagimage,ROUND(salesdetail.lstcharge,'.$amountround.') as lstcharge,ROUND(salesdetail.lstchargespan,'.$amountround.') as lstchargespan,ROUND(salesdetail.prelstcharge,'.$amountround.') as prelstcharge,salesdetail.lstchargekeylabel,salesdetail.lstchargespanlabel,salesdetail.flatchargespanlabel,salesdetail.flatchargespankey,ROUND(salesdetail.preflatcharge,'.$amountround.') as preflatcharge,salesdetail.orderitemratefix,DATE_FORMAT(salesdetail.vendororderduedate,"%d-%m-%Y") AS vendororderduedate,salesdetail.ordervendorid, ROUND(salesdetail.repaircharge,'.$amountround.') as repaircharge, salesdetail.repairchargekeylabel,salesdetail.repairchargespanlabel, ROUND(salesdetail.prerepaircharge,'.$amountround.') as prerepaircharge, ROUND(salesdetail.repairchargespan,'.$amountround.') as repairchargespan, ROUND(salesdetail.repairchargespan,'.$amountround.') as repairchargeclone,salesdetail.proditemrate,salesdetail.taxcategoryid')
			->from('salesdetail')
			->join('itemtag','itemtag.itemtagid=salesdetail.itemtagid','left outer')
			->join('stocktype','stocktype.stocktypeid=salesdetail.stocktypeid','left outer')
			->join('orderstatus','orderstatus.orderstatusid=salesdetail.orderstatusid','left outer')
			->join('repairstatus','repairstatus.repairstatusid=salesdetail.repairstatusid','left outer')
			->join('sizemaster','sizemaster.sizemasterid=salesdetail.orderitemsize','left outer')
			->join('product','product.productid=salesdetail.productid','left outer')
			->join('category','category.categoryid=salesdetail.categoryid','left outer')
			->join('purity','purity.purityid=salesdetail.purityid','left outer')
			->join('counter','counter.counterid=salesdetail.counterid','left outer')
			->join('employee','employee.employeeid=salesdetail.employeeid','left outer')
			->join('sales','sales.salesid=salesdetail.salesid','left outer')
			->where($billwhere,trim($orderid))
			->where($salesdetailvendorid)
			->where($orderrepair)
			->where($stocktypewhere)
			->where_not_in('salesdetail.salesdetailid',$explodesalesdetailsid)
			->where('salesdetail.status',$this->Basefunctions->activestatus)
			->get();
		} else if($status == 3) {
			if($povendormanagement == 1 && $selectedstocktypeid == 82) {
				$info=$this->db->query("SELECT `sales`.`salesnumber` as `posalesnumber`,`posales`.`salesnumber` as `orsalesnumber`,`sales`.`salesid`, `salesdetail`.`salesdetailid`,`presales`.`salesid` as presalesid,`presalesdetail`.`salesdetailid` as presalesdetailid, `itemtag`.`itemtagnumber`, `itemtag`.`rfidtagno`, `product`.`productname`, `category`.`categoryname`, `purity`.`purityname`, `counter`.`countername`, ROUND(presalesdetail.grossweight, '".$round."') as grossweight, ROUND(presalesdetail.stoneweight, '".$round."') as stoneweight, ROUND(presalesdetail.netweight, '".$round."') as netweight, ROUND(presalesdetail.caratweight,'".$round."') as itemcaratweight, `presalesdetail`.`pieces`, `salesdetail`.`productid`, `salesdetail`.`categoryid`, `salesdetail`.`purityid`, `salesdetail`.`counterid`, `salesdetail`.`employeeid`, `employee`.`employeename`, `salesdetail`.`modeid`, `salesdetail`.`itemtagid`, `salesdetail`.`oldjewelvoucherstatus`, ROUND(salesdetail.ratepergram, '".$rateround."') as ratepergram, ROUND(salesdetail.totalamount, '".$amountround."') as totalamount, `salesdetail`.`netwtcalctypeid`, ROUND(salesdetail.taxamount, '".$taxround."') as taxamount, `salesdetail`.`calculationtypeid`, ROUND(salesdetail.grossamount, '".$amountround."') as grossamount, ROUND(salesdetail.stoneamount, '".$amountround."') as stoneamount, ROUND(salesdetail.wastage, '".$amountround."') as wastage, ROUND(salesdetail.making, '".$amountround."') as making, `salesdetail`.`wastagespan`, `salesdetail`.`makingspan`, `salesdetail`.`wastagespanlabel`, `salesdetail`.`makingchargespanlabel`, `salesdetail`.`wastagespankey`, `salesdetail`.`makingchargespankey`, ROUND(salesdetail.chargesamount, '".$amountround."') as chargesamount,ROUND(salesdetail.flatcharge, '".$amountround."') as flatcharge,`salesdetail`.`flatchargespan`, `salesdetail`.`taxcategoryid`, `salesdetail`.`ckeyword`, `salesdetail`.`ckeywordvalue`, `salesdetail`.`ckeywordoriginalvalue`, `salesdetail`.`processcounterid`, `salesdetail`.`stocktypeid`, `stocktype`.`stocktypename`, `sizemaster`.`sizemastername`, `salesdetail`.`orderitemsize`, `salesdetail`.`prewastage`, `salesdetail`.`premakingcharge`, `salesdetail`.`comment`, DATE_FORMAT(salesdetail.orderduedate, '%d-%m-%Y') AS orderduedate, `presalesdetail`.`tagimage`,ROUND(salesdetail.lstcharge,'".$amountround."') as lstcharge,ROUND(salesdetail.lstchargespan,'".$amountround."') as lstchargespan,ROUND(salesdetail.prelstcharge,'".$amountround."') as prelstcharge,salesdetail.lstchargekeylabel,salesdetail.lstchargespanlabel,salesdetail.flatchargespanlabel,salesdetail.flatchargespankey,ROUND(salesdetail.preflatcharge,".$amountround.") as preflatcharge,salesdetail.orderitemratefix,DATE_FORMAT(salesdetail.vendororderduedate,'%d-%m-%Y') AS vendororderduedate,salesdetail.ordervendorid, ROUND(salesdetail.repaircharge,'".$amountround."') as repaircharge, salesdetail.repairchargekeylabel,salesdetail.repairchargespanlabel, ROUND(salesdetail.prerepaircharge,'".$amountround."') as prerepaircharge, ROUND(salesdetail.repairchargespan,'".$amountround."') as repairchargespan, ROUND(salesdetail.repairchargespan,'".$amountround."') as repairchargeclone, salesdetail.proditemrate, salesdetail.taxcategoryid FROM `salesdetail` LEFT OUTER JOIN salesdetail as posalesdetail ON salesdetail.presalesdetailid=posalesdetail.salesdetailid and posalesdetail.status NOT IN (0,3) LEFT OUTER JOIN salesdetail as presalesdetail ON salesdetail.salesdetailid=presalesdetail.presalesdetailid and presalesdetail.status NOT IN (0,3) LEFT OUTER JOIN `itemtag` ON `itemtag`.`itemtagid`=`salesdetail`.`itemtagid` LEFT OUTER JOIN `stocktype` ON `stocktype`.`stocktypeid`=`salesdetail`.`stocktypeid` LEFT OUTER JOIN `orderstatus` ON `orderstatus`.`orderstatusid`=`salesdetail`.`orderstatusid` LEFT OUTER JOIN repairstatus ON repairstatus.repairstatusid = salesdetail.repairstatusid LEFT OUTER JOIN `sizemaster` ON `sizemaster`.`sizemasterid`=`salesdetail`.`orderitemsize` LEFT OUTER JOIN `product` ON `product`.`productid`=`salesdetail`.`productid` LEFT OUTER JOIN category ON category.categoryid = salesdetail.categoryid LEFT OUTER JOIN `purity` ON `purity`.`purityid`=`salesdetail`.`purityid` LEFT OUTER JOIN `counter` ON `counter`.`counterid`=`salesdetail`.`counterid` LEFT OUTER JOIN `employee` ON `employee`.`employeeid`=`salesdetail`.`employeeid` LEFT OUTER JOIN `sales` ON `sales`.`salesid`=`salesdetail`.`salesid` LEFT OUTER JOIN `sales` as `posales` ON `posales`.`salesid`=`posalesdetail`.`salesid` LEFT OUTER JOIN `sales` as `presales` ON `presales`.`salesid`=`presalesdetail`.`salesid` WHERE ".$genposalesstatus_where." AND ".$billwhere." = '".$orderid."' AND `salesdetail`.`orderstatusid` = '3' AND ".$genpostatus_where." AND `salesdetail`.`stocktypeid` = '76' AND `presalesdetail`.`salesdetailid` NOT IN(".$salesdetailsid.") AND salesdetail.generatepo IN(2) AND presales.deliverypomodeid in(2,3,4) AND presalesdetail.delorderstatusid IN(24) AND `salesdetail`.`status` = 1");
			} else {
				if($selectedstocktypeid == 88) {
					$orderrepairstatus = "`salesdetail`.`repairstatusid` = '3' AND `salesdetail`.`stocktypeid` = '87'";
				} else {
					$orderrepairstatus = "`salesdetail`.`orderstatusid` = '3' AND `salesdetail`.`stocktypeid` = '76'";
				}
				$info=$this->db->query("SELECT `sales`.`salesnumber` as `posalesnumber`,`posales`.`salesnumber` as `orsalesnumber`,`sales`.`salesid`, `salesdetail`.`salesdetailid`, `itemtag`.`itemtagnumber`, `itemtag`.`rfidtagno`, `product`.`productname`, `category`.`categoryname`, `purity`.`purityname`, `counter`.`countername`, ROUND(salesdetail.grossweight, '".$round."') as grossweight, ROUND(salesdetail.stoneweight, '".$round."') as stoneweight, ROUND(salesdetail.netweight, '".$round."') as netweight, ROUND(salesdetail.caratweight,'".$round."') as itemcaratweight, `salesdetail`.`pieces`, `salesdetail`.`productid`, `salesdetail`.`categoryid`, `salesdetail`.`purityid`, `salesdetail`.`counterid`, `salesdetail`.`employeeid`, `employee`.`employeename`, `salesdetail`.`modeid`, `salesdetail`.`itemtagid`, `salesdetail`.`oldjewelvoucherstatus`, ROUND(salesdetail.ratepergram, '".$rateround."') as ratepergram, ROUND(salesdetail.totalamount, '".$amountround."') as totalamount, `salesdetail`.`netwtcalctypeid`, ROUND(salesdetail.taxamount, '".$taxround."') as taxamount, `salesdetail`.`calculationtypeid`, ROUND(salesdetail.grossamount, '".$amountround."') as grossamount, ROUND(salesdetail.stoneamount, '".$amountround."') as stoneamount, ROUND(salesdetail.wastage, '".$amountround."') as wastage, ROUND(salesdetail.making, '".$amountround."') as making, `salesdetail`.`wastagespan`, `salesdetail`.`makingspan`, `salesdetail`.`wastagespanlabel`, `salesdetail`.`makingchargespanlabel`, `salesdetail`.`wastagespankey`, `salesdetail`.`makingchargespankey`, ROUND(salesdetail.chargesamount, '".$amountround."') as chargesamount,ROUND(salesdetail.flatcharge, '".$amountround."') as flatcharge,`salesdetail`.`flatchargespan`, `salesdetail`.`taxcategoryid`, `salesdetail`.`ckeyword`, `salesdetail`.`ckeywordvalue`, `salesdetail`.`ckeywordoriginalvalue`, `salesdetail`.`processcounterid`, `salesdetail`.`stocktypeid`, `stocktype`.`stocktypename`, `sizemaster`.`sizemastername`, `salesdetail`.`orderitemsize`, `salesdetail`.`prewastage`, `salesdetail`.`premakingcharge`, `salesdetail`.`comment`, DATE_FORMAT(salesdetail.orderduedate, '%d-%m-%Y') AS orderduedate, `salesdetail`.`tagimage`,ROUND(salesdetail.lstcharge,'".$amountround."') as lstcharge,ROUND(salesdetail.lstchargespan,'".$amountround."') as lstchargespan,ROUND(salesdetail.prelstcharge,'".$amountround."') as prelstcharge,salesdetail.lstchargekeylabel,salesdetail.lstchargespanlabel,salesdetail.flatchargespanlabel,salesdetail.flatchargespankey,ROUND(salesdetail.preflatcharge,".$amountround.") as preflatcharge,salesdetail.orderitemratefix,DATE_FORMAT(salesdetail.vendororderduedate,'%d-%m-%Y') AS vendororderduedate,salesdetail.ordervendorid, ROUND(salesdetail.repaircharge,'".$amountround."') as repaircharge, salesdetail.repairchargekeylabel,salesdetail.repairchargespanlabel, ROUND(salesdetail.prerepaircharge,'".$amountround."') as prerepaircharge, ROUND(salesdetail.repairchargespan,'".$amountround."') as repairchargespan, ROUND(salesdetail.repairchargespan,'".$amountround."') as repairchargeclone, salesdetail.proditemrate, salesdetail.taxcategoryid FROM `salesdetail` LEFT OUTER JOIN salesdetail as posalesdetail ON salesdetail.presalesdetailid=posalesdetail.salesdetailid and posalesdetail.status NOT IN (0,3) LEFT OUTER JOIN `itemtag` ON `itemtag`.`itemtagid`=`salesdetail`.`itemtagid` LEFT OUTER JOIN `stocktype` ON `stocktype`.`stocktypeid`=`salesdetail`.`stocktypeid` LEFT OUTER JOIN `orderstatus` ON `orderstatus`.`orderstatusid`=`salesdetail`.`orderstatusid` LEFT OUTER JOIN repairstatus ON repairstatus.repairstatusid = salesdetail.repairstatusid LEFT OUTER JOIN `sizemaster` ON `sizemaster`.`sizemasterid`=`salesdetail`.`orderitemsize` LEFT OUTER JOIN `product` ON `product`.`productid`=`salesdetail`.`productid` LEFT OUTER JOIN category ON category.categoryid = salesdetail.categoryid LEFT OUTER JOIN `purity` ON `purity`.`purityid`=`salesdetail`.`purityid` LEFT OUTER JOIN `counter` ON `counter`.`counterid`=`salesdetail`.`counterid` LEFT OUTER JOIN `employee` ON `employee`.`employeeid`=`salesdetail`.`employeeid` LEFT OUTER JOIN `sales` ON `sales`.`salesid`=`salesdetail`.`salesid` LEFT OUTER JOIN `sales` as `posales` ON `posales`.`salesid`=`posalesdetail`.`salesid` WHERE ".$genposalesstatus_where." AND ".$billwhere." = '".$orderid."' AND ".$orderrepairstatus." AND ".$genpostatus_where." AND `salesdetail`.`salesdetailid` NOT IN(".$salesdetailsid.") AND `salesdetail`.`status` = 1");
			}
		} else if($status == 4) {
			if($selectedstocktypeid == 89) {
				$extracondition = "AND 1=1";
				$info=$this->db->query("SELECT `rosales`.`salesnumber` as `rosalesnumber`,`sales`.`salesnumber` as `orsalesnumber`,`posales`.`salesnumber` as `posalesnumber`,`rosalesdetail`.`salesid`, `rosalesdetail`.`salesdetailid`, `itemtag`.`itemtagnumber`, `itemtag`.`rfidtagno`, `product`.`productname`,  `category`.`categoryname`, `purity`.`purityname`, `counter`.`countername`, ROUND(rosalesdetail.grossweight, '".$round."') as grossweight, ROUND(rosalesdetail.stoneweight,'".$round."') as stoneweight, ROUND(rosalesdetail.netweight, '".$round."') as netweight, ROUND(rosalesdetail.caratweight,'".$round."') as itemcaratweight, `rosalesdetail`.`pieces`, `rosalesdetail`.`productid`, `rosalesdetail`.`categoryid`, `rosalesdetail`.`purityid`, `rosalesdetail`.`counterid`, `rosalesdetail`.`employeeid`, `employee`.`employeename`, `rosalesdetail`.`modeid`, `rosalesdetail`.`itemtagid`, `rosalesdetail`.`oldjewelvoucherstatus`, ROUND(rosalesdetail.ratepergram, '".$rateround."') as ratepergram, ROUND(rosalesdetail.totalamount, '".$amountround."') as totalamount, `rosalesdetail`.`netwtcalctypeid`, ROUND(rosalesdetail.taxamount, '".$taxround."') as taxamount, `rosalesdetail`.`calculationtypeid`, ROUND(rosalesdetail.grossamount, '".$amountround."') as grossamount, ROUND(rosalesdetail.stoneamount, '".$amountround."') as stoneamount,  ROUND(rosalesdetail.wastage, '".$amountround."') as wastage,ROUND(rosalesdetail.making, '".$amountround."') as making , `rosalesdetail`.`wastagespan`, `rosalesdetail`.`makingspan`, `rosalesdetail`.`wastagespanlabel`, `rosalesdetail`.`makingchargespanlabel`, `rosalesdetail`.`wastagespankey`, `rosalesdetail`.`makingchargespankey`, ROUND(rosalesdetail.chargesamount, '".$amountround."') as chargesamount,ROUND(rosalesdetail.flatcharge, '".$amountround."') as flatcharge, `rosalesdetail`.`flatchargespan`, `rosalesdetail`.`taxcategoryid`, `rosalesdetail`.`ckeyword`, `rosalesdetail`.`ckeywordvalue`, `rosalesdetail`.`ckeywordoriginalvalue`, `rosalesdetail`.`processcounterid`, `rosalesdetail`.`stocktypeid`, `stocktype`.`stocktypename`, `sizemaster`.`sizemastername`, `rosalesdetail`.`orderitemsize`, `rosalesdetail`.`prewastage`, `rosalesdetail`.`premakingcharge`, `rosalesdetail`.`comment`,DATE_FORMAT(rosalesdetail.orderduedate,'%d-%m-%Y') AS orderduedate,`rosalesdetail`.`tagimage`,ROUND(rosalesdetail.lstcharge,'".$amountround."') as lstcharge,ROUND(rosalesdetail.lstchargespan,'".$amountround."') as lstchargespan,ROUND(rosalesdetail.prelstcharge,'".$amountround."') as prelstcharge,rosalesdetail.lstchargekeylabel,rosalesdetail.lstchargespanlabel,salesdetail.flatchargespanlabel,salesdetail.flatchargespankey,ROUND(salesdetail.preflatcharge,".$amountround.") as preflatcharge,rosalesdetail.orderitemratefix as orderitemratefix,DATE_FORMAT(rosalesdetail.vendororderduedate,'%d-%m-%Y') AS vendororderduedate,rosalesdetail.ordervendorid, ROUND(rosalesdetail.repaircharge,'".$amountround."') as repaircharge, rosalesdetail.repairchargekeylabel,rosalesdetail.repairchargespanlabel, ROUND(rosalesdetail.prerepaircharge,'".$amountround."') as prerepaircharge, ROUND(rosalesdetail.repairchargespan,'".$amountround."') as repairchargespan, ROUND(rosalesdetail.repairchargespan,'".$amountround."') as repairchargeclone,rosalesdetail.proditemrate, rosalesdetail.taxcategoryid FROM `salesdetail` LEFT OUTER JOIN salesdetail as posalesdetail ON posalesdetail.presalesdetailid=salesdetail.salesdetailid and posalesdetail.status NOT IN (0,3) LEFT OUTER JOIN salesdetail as rosalesdetail ON rosalesdetail.presalesdetailid=posalesdetail.salesdetailid and rosalesdetail.status NOT IN (0,3) LEFT OUTER JOIN `itemtag` ON `itemtag`.`itemtagid`=`rosalesdetail`.`itemtagid` LEFT OUTER JOIN `stocktype` ON `stocktype`.`stocktypeid`=`rosalesdetail`.`stocktypeid` LEFT OUTER JOIN `orderstatus` ON `orderstatus`.`orderstatusid`=`rosalesdetail`.`orderstatusid` LEFT OUTER JOIN repairstatus ON repairstatus.repairstatusid = rosalesdetail.repairstatusid LEFT OUTER JOIN `sizemaster` ON `sizemaster`.`sizemasterid`=`rosalesdetail`.`orderitemsize` LEFT OUTER JOIN `product` ON `product`.`productid`=`rosalesdetail`.`productid` LEFT OUTER JOIN category ON category.categoryid = rosalesdetail.categoryid LEFT OUTER JOIN `purity` ON `purity`.`purityid`=`rosalesdetail`.`purityid` LEFT OUTER JOIN `counter` ON `counter`.`counterid`=`rosalesdetail`.`counterid` LEFT OUTER JOIN `employee` ON `employee`.`employeeid`=`rosalesdetail`.`employeeid` LEFT OUTER JOIN `sales` as `posales` ON `posales`.`salesid`=`posalesdetail`.`salesid` LEFT OUTER JOIN `sales` as `rosales` ON `rosales`.`salesid`=`rosalesdetail`.`salesid` LEFT OUTER JOIN `sales`  ON `sales`.`salesid`=`salesdetail`.`salesid` WHERE `rosalesdetail`.`repairstatusid` = '4' AND `rosalesdetail`.`stocktypeid` = '88' AND `rosalesdetail`.`salesdetailid` NOT IN(".$salesdetailsid.") ".$extracondition." AND `rosalesdetail`.`status` = 1 and ".$billwhere." = '".$orderid."'");
			}
		}
		if($info->num_rows() > 0) {
			$j=1;
			foreach($info->result() as $value) {
				$orderid =  $value->salesid;
				// charges
				$ckeyword=array();
				$ckeywordvalue=array();
				$keyworddetails=array();
				$keyworddata=array();
				if($value->ckeyword != '' && $value->ckeywordvalue != '') {
					$ckeyword = explode(',',$value->ckeyword);
					$ckeywordvalue = explode(',',$value->ckeywordvalue);
					$keyworddetails = array_combine($ckeyword, $ckeywordvalue);
					$k = 0;
					foreach($keyworddetails as $k => $l){
						$keyworddata[] = $k. ":".$l;
						$k++;
					}
					$keyword_str = implode (",", $keyworddata);
				} else {
					$keyword_str ='';
				}
				$ckeyword1=array();
				$ckeywordoriginalvalue=array();
				$keyworddetails1=array();
				$keyworddata1=array();
				if($value->ckeyword != '') {
					$ckeyword1 = explode(',',$value->ckeyword);
					$ckeywordoriginalvalue = explode(',',$value->ckeywordoriginalvalue); 
					$keyworddetails1 = array_combine($ckeyword1, $ckeywordoriginalvalue);
					$m = 0;
					foreach($keyworddetails1 as $m => $p) {
						$keyworddata1[] = $m. ":".$p;
						$m++;
					}
					$keyword_str1 = implode (",", $keyworddata1);
				} else {
					$keyword_str1 ='';
				}
				if($value->orderitemratefix == 2) {
					$orderitemratefixname = 'Today';
				} else if($value->orderitemratefix == 3) {
					$orderitemratefixname = 'Delivery';
				} else {
					$orderitemratefixname = '';
				}
				$stoneentry = $this->get_sales_stoneentry_div($value->salesid,$value->salesdetailid);
				if($povendormanagement == 1 && $selectedstocktypeid == 82) {
					$stonedetails = $this->get_sales_stoneentry_details($value->presalesid,$value->presalesdetailid);
				} else {
					$stonedetails = $this->get_sales_stoneentry_details($value->salesid,$value->salesdetailid);
				}
				if($stocktypeid == 87 || $stocktypeid == 76 && $selectedstocktypeid == 82) {
					$taxsalesdetailid =  $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$value->salesdetailid);
					$taxsalesid =  $this->Basefunctions->singlefieldfetch('salesid','salesdetailid','salesdetail',$taxsalesdetailid);
					$taxentry = $this->get_sales_taxentry_div($taxsalesid,$taxsalesdetailid);
					$taxentrydata = $this->get_sales_taxentry_data($taxsalesid,$taxsalesdetailid);
					$taxcategoryid = $this->Basefunctions->singlefieldfetch('taxcategoryid','salesdetailid','salesdetail',$taxsalesdetailid);
				} else if($stocktypeid == 88 || $stocktypeid == 82 && $selectedstocktypeid == 83) {
					$taxentry = $this->get_sales_taxentry_div($value->salesid,$value->salesdetailid);
					$taxentrydata = $this->get_sales_taxentry_data($value->salesid,$value->salesdetailid);
					$taxcategoryid = $this->Basefunctions->singlefieldfetch('taxcategoryid','salesdetailid','salesdetail',$value->salesdetailid);
				} else {
					$taxentry = '';
					$taxentrydata = '';
					$taxcategoryid = 1;
				}
				if($value->ordervendorid > 1) {
					$ordervendorname = $this->Basefunctions->singlefieldfetch('accountname','accountid','account',$value->ordervendorid); //Vendor name
				} else {
					$ordervendorname = '';
				}
				if($stocktypeid == 75) {
					$stocktypename = 'Place Order';
					$finalstocktypeid = 76;
					if($_GET['ordernumber'] == 'all') {
						$ordernumber = $value->ordernumber;
					} else {
						$ordernumber = $_GET['ordernumber'];
					}
					$placeordernumber = '';
					$receiveordernumber = '';
				} else if($stocktypeid == 76) {
					if($substocktypeid != 1) {
						$stocktypename = 'Generate PO';
						$finalstocktypeid = 85;
					} else {
						$stocktypename = 'Receive';
						$finalstocktypeid = 82;
						if($povendormanagement == 1 && $selectedstocktypeid == 82) {
							$value->salesdetailid = $value->presalesdetailid;
						}
					}
					$vendorid = $this->Basefunctions->singlefieldfetch('accountid','salesid','sales',$value->salesid);
					$ordernumber = $value->orsalesnumber;
					$placeordernumber = $value->posalesnumber;
					$receiveordernumber = '';
				} else if($stocktypeid == 82) {
					$stocktypename = 'Order Tag';
					$finalstocktypeid = 83;
					$ordernumber = $value->orsalesnumber;
					$placeordernumber = $value->posalesnumber;
					$receiveordernumber = $value->rosalesnumber;
					if($value->orderitemratefix == 2) {
						$value->ratepergram;
					} else if($value->orderitemratefix == 3) {
						$purityid = $value->purityid;
						$rate_arr = json_decode($rate_apply, true);
						$final_rate = $rate_arr[$purityid];
						$value->ratepergram = $final_rate;
					} else {
						$value->ratepergram;
					}
				} else if($stocktypeid == 86 || $stocktypeid == 87) {
					if($stocktypeid == 86) {
						$stocktypename = 'Place Repair';
						$finalstocktypeid = 87;
					} else if($stocktypeid == 87) {
						$stocktypename = 'Receive Repair';
						$finalstocktypeid = 88;
					}
					if($_GET['ordernumber'] == 'all' && $stocktypeid == 86) {
						$ordernumber = $value->ordernumber;
					} else if($stocktypeid == 86) {
						$ordernumber = $value->ordernumber;
					} else {
						$ordernumber = $value->orsalesnumber;
					}
					$placeordernumber = '';
					$receiveordernumber = '';
				} else if($stocktypeid == 88) {
					$stocktypename = 'Repair Item';
					$finalstocktypeid = 89;
					$ordernumber = $value->orsalesnumber;
					$placeordernumber = $value->posalesnumber;
					$receiveordernumber = $value->rosalesnumber;
				}
				$estimatedetail->rows[$j]['id'] = $value->salesdetailid;
				$estimatedetail->rows[$j]['cell']=array(
						$finalstocktypeid,
						$stocktypename,
						$default,
						$empty,
						$value->totalamount,
						$default,
						$value->itemtagnumber,
						$value->rfidtagno,
						$value->productid,
						$value->productname,
						$value->purityid,
						$value->purityname,
						$value->counterid,
						$value->countername,
						$value->grossweight,
						$value->stoneweight,
						$value->netweight,
						$value->pieces,
						$value->ratepergram,
						$value->wastage,
						$value->making,
						$value->stoneamount,
						$value->taxamount,
						$default,
						$value->grossamount,
						$value->employeeid,
						$value->employeename,
						$value->wastagespan,
						$value->makingspan,
						$stoneentry,
						$value->flatcharge,
						$value->flatchargespan,
						$default,
						$default,
						$value->chargesamount,
						$value->wastagespan,
						$value->makingspan,
						$value->flatchargespan,
						$default,
						$default,
						$default,
						$default,
						$value->modeid,
						$default,
						$value->itemtagid,
						$default,
						$default,
						$default,
						$taxentry,
						$default,
						$default,
						$default,
						$default,
						$default,
						$empty,
						$empty,
						$value->salesdetailid,
						$default,
						$default,
						$taxentrydata,
						$default,
						$stonedetails,
						$empty,
						$value->wastagespanlabel,
						$value->makingchargespanlabel,
						$value->wastagespankey,
						$value->makingchargespankey,
						$value->ckeyword,
						$value->ckeywordvalue,
						$value->ckeywordoriginalvalue,
						$keyword_str,
						$keyword_str1,
						$value->chargesamount,
						$default,
						$default,
						$default,
						$default,
						$value->oldjewelvoucherstatus,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$value->processcounterid,
						$taxcategoryid,
						$empty,
						$value->prewastage,
						$value->premakingcharge,
						$default,
						1,
						$empty,
						1,
						$empty,
						$default,
						$default,
						1,
						$empty,
						1,
						$empty,
						1,
						$empty,
						$default,
						$default,
						$default,
						$default,
						$default,
						$value->orderitemsize,
						$value->sizemastername,
						$value->comment,
						$empty,
						$value->orderduedate,
						$orderid,
						$empty,
						$empty,
						$ordernumber,
						$vendorid,
						$placeordernumber,
						$receiveordernumber,
						$empty,
						1,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$value->categoryid,
						$value->categoryname,
						$value->tagimage,
						$value->lstcharge,
						$value->lstchargespan,
						$value->lstchargespan,
						$value->lstchargekeylabel,
						$value->lstchargespanlabel,
						$value->prelstcharge,
						$value->flatchargespanlabel,
						$value->flatchargespankey,
						$value->preflatcharge,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$value->orderitemratefix,
						$empty,
						$empty,
						$orderitemratefixname,
						$value->vendororderduedate,
						$value->ordervendorid,
						$ordervendorname,
						$value->proditemrate,
						$value->itemcaratweight,
						$value->repaircharge,
						$value->repairchargekeylabel,
						$value->repairchargespanlabel,
						$value->prerepaircharge,
						$value->repairchargespan,
						$value->repairchargespan
				);
				$j++;
			}
		} else {
			$estimatedetail = ' ';
		}
		$estimatearray['estimatedata'] = $estimatedetail;
		echo json_encode($estimatearray);
	}
	//Order Grid Status Overlay
	public function orderviewdynamicdatainfofetch($tablename,$sortcol,$sortord,$pagenum,$rowscount,$primaryid,$ordertransactionpage) {
		$billwhere = 'sales.salesid';
		$genposalesstatus_where = '1=1';
		if($ordertransactionpage == 'placeorder') {
			$genpostatus_where = 'salesdetail.poorderstatusid IN(1,23) AND `salesdetail`.`stocktypeid` IN(76) AND salesdetail.status NOT IN(0,3)';
		} else if($ordertransactionpage == 'generatepo') {
			$genpostatus_where = 'sales.deliverypomodeid IN(2,3,4) AND `salesdetail`.`stocktypeid` IN(85) AND `salesdetail`.`delorderstatusid` IN(1,23) AND `salesdetail`.`generatepo` IN(2) AND salesdetail.status NOT IN(0,3)';
		}
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$rateround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //Rate round-off
		$taxround = 2;
		$dataset = "`sales`.`salesnumber` as `posalesnumber`,`posales`.`salesnumber` as `orsalesnumber`,`sales`.`salesid`, `salesdetail`.`salesdetailid`, `itemtag`.`itemtagnumber`, `itemtag`.`rfidtagno`, `product`.`productname`, `purity`.`purityname`, `counter`.`countername`, ROUND(salesdetail.grossweight, '".$round."') as grossweight, ROUND(salesdetail.stoneweight, '".$round."') as stoneweight, ROUND(salesdetail.netweight, '".$round."') as netweight, `salesdetail`.`pieces`, `salesdetail`.`productid`, `salesdetail`.`purityid`, `salesdetail`.`counterid`, `salesdetail`.`employeeid`, `employee`.`employeename`, `salesdetail`.`modeid`, `salesdetail`.`itemtagid`, `salesdetail`.`oldjewelvoucherstatus`, ROUND(salesdetail.ratepergram, '".$rateround."') as ratepergram, ROUND(salesdetail.totalamount, '".$amountround."') as totalamount, `salesdetail`.`netwtcalctypeid`, ROUND(salesdetail.taxamount, '".$taxround."') as taxamount, `salesdetail`.`calculationtypeid`, ROUND(salesdetail.grossamount, '".$amountround."') as grossamount, ROUND(salesdetail.stoneamount, '".$amountround."') as stoneamount, ROUND(salesdetail.wastage, '".$amountround."') as wastage, ROUND(salesdetail.making, '".$amountround."') as making, `salesdetail`.`wastagespan`, `salesdetail`.`makingspan`, `salesdetail`.`wastagespanlabel`, `salesdetail`.`makingchargespanlabel`, `salesdetail`.`wastagespankey`, `salesdetail`.`makingchargespankey`, ROUND(salesdetail.chargesamount, '".$amountround."') as chargesamount,ROUND(salesdetail.flatcharge, '".$amountround."') as flatcharge,`salesdetail`.`flatchargespan`, `salesdetail`.`taxcategoryid`, `salesdetail`.`ckeyword`, `salesdetail`.`ckeywordvalue`, `salesdetail`.`ckeywordoriginalvalue`, `salesdetail`.`processcounterid`, `salesdetail`.`stocktypeid`, `stocktype`.`stocktypename`, `sizemaster`.`sizemastername`, `salesdetail`.`orderitemsize`, `salesdetail`.`prewastage`, `salesdetail`.`premakingcharge`, `salesdetail`.`comment`, DATE_FORMAT(salesdetail.orderduedate, '%d-%m-%Y') AS orderduedate, `salesdetail`.`tagimage`,ROUND(salesdetail.lstcharge,'".$amountround."') as lstcharge,ROUND(salesdetail.lstchargespan,'".$amountround."') as lstchargespan,ROUND(salesdetail.prelstcharge,'".$amountround."') as prelstcharge,salesdetail.lstchargekeylabel,salesdetail.lstchargespanlabel,salesdetail.flatchargespanlabel,salesdetail.flatchargespankey,ROUND(salesdetail.preflatcharge,".$amountround.") as preflatcharge,salesdetail.orderitemratefix,ROUND(salesdetail.secondtouch,2) as secondtouch";
		$join = 'LEFT OUTER JOIN salesdetail as posalesdetail ON salesdetail.presalesdetailid=posalesdetail.salesdetailid and posalesdetail.status NOT IN (0,3) ';
		$join.= 'LEFT OUTER JOIN `itemtag` ON `itemtag`.`itemtagid`=`salesdetail`.`itemtagid`';
		$join.= 'LEFT OUTER JOIN `stocktype` ON `stocktype`.`stocktypeid`=`salesdetail`.`stocktypeid`';
		$join.= 'LEFT OUTER JOIN `orderstatus` ON `orderstatus`.`orderstatusid`=`salesdetail`.`orderstatusid`';
		$join.= 'LEFT OUTER JOIN `sizemaster` ON `sizemaster`.`sizemasterid`=`salesdetail`.`orderitemsize`';
		$join.= 'LEFT OUTER JOIN `product` ON `product`.`productid`=`salesdetail`.`productid`';
		$join.= 'LEFT OUTER JOIN `purity` ON `purity`.`purityid`=`salesdetail`.`purityid`';
		$join.= 'LEFT OUTER JOIN `counter` ON `counter`.`counterid`=`salesdetail`.`counterid`';
		$join.= 'LEFT OUTER JOIN `employee` ON `employee`.`employeeid`=`salesdetail`.`employeeid`';
		$join.= 'LEFT OUTER JOIN `sales` ON `sales`.`salesid`=`salesdetail`.`salesid`';
		$join.= 'LEFT OUTER JOIN `sales` as `posales` ON `posales`.`salesid`=`posalesdetail`.`salesid`';
		$cuscondition = "".$genposalesstatus_where." AND ".$billwhere." = '".$primaryid."' AND `salesdetail`.`orderstatusid` = '3' AND ".$genpostatus_where." AND ";
		$status=$tablename.'.status='.$this->Basefunctions->activestatus.'';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		/* query */
		$result = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from salesdetail '.$join.' WHERE '.$cuscondition.' '. $status.' ORDER BY '.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$data=array();
		$i=0;
		foreach($result->result() as $row) {
			$salesdetailid = $row->salesdetailid;
			$productname =$row->productname;
			$purityname =$row->purityname;
			$grossweight =$row->grossweight;
			$stoneweight =$row->stoneweight;
			$netweight =$row->netweight;
			$pieces =$row->pieces;
			$orderduedate =$row->orderduedate;
			$tagimage =$row->tagimage;
			$ordernumber =$row->orsalesnumber;
			$poordernumber =$row->posalesnumber;
			$secondtouch =$row->secondtouch;
			$data[$i]=array('id'=>$salesdetailid,$ordernumber,$productname,$purityname,$grossweight,$stoneweight,$netweight,$pieces,$tagimage,$orderduedate,$poordernumber,$secondtouch);
			$i++;
		}
		$finalresult=array('0'=>$data,'1'=>$page,'2'=>'json');
		return $finalresult;
	}
	//Vendor Status grid information fetch
	public function vendoritemstatusdynamicdatainfofetch($tablename,$sortcol,$sortord,$pagenum,$rowscount,$primaryid) {
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$rateround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //Rate round-off
		$taxround = 2;
		$genposalesstatus_where = 'salesdetail.poorderstatusid IN(21) AND salesdetail.generatepo IN(1)';
		$billwhere = 'sales.salesid';
		$genpostatus_where = '1=1';
		$dataset = "`sales`.`salesnumber` as `posalesnumber`,`posales`.`salesnumber` as `orsalesnumber`,`sales`.`salesid`, `salesdetail`.`salesdetailid`, `itemtag`.`itemtagnumber`, `itemtag`.`rfidtagno`, `product`.`productname`, `purity`.`purityname`, `counter`.`countername`, ROUND(salesdetail.grossweight, '".$round."') as grossweight, ROUND(salesdetail.stoneweight, '".$round."') as stoneweight, ROUND(salesdetail.netweight, '".$round."') as netweight, `salesdetail`.`pieces`, `salesdetail`.`productid`, `salesdetail`.`purityid`, `salesdetail`.`counterid`, `salesdetail`.`employeeid`, `employee`.`employeename`, `salesdetail`.`modeid`, `salesdetail`.`itemtagid`, `salesdetail`.`oldjewelvoucherstatus`, ROUND(salesdetail.ratepergram, '".$rateround."') as ratepergram, ROUND(salesdetail.totalamount, '".$amountround."') as totalamount, `salesdetail`.`netwtcalctypeid`, ROUND(salesdetail.taxamount, '".$taxround."') as taxamount, `salesdetail`.`calculationtypeid`, ROUND(salesdetail.grossamount, '".$amountround."') as grossamount, ROUND(salesdetail.stoneamount, '".$amountround."') as stoneamount, ROUND(salesdetail.wastage, '".$amountround."') as wastage, ROUND(salesdetail.making, '".$amountround."') as making, `salesdetail`.`wastagespan`, `salesdetail`.`makingspan`, `salesdetail`.`wastagespanlabel`, `salesdetail`.`makingchargespanlabel`, `salesdetail`.`wastagespankey`, `salesdetail`.`makingchargespankey`, ROUND(salesdetail.chargesamount, '".$amountround."') as chargesamount,ROUND(salesdetail.flatcharge, '".$amountround."') as flatcharge,`salesdetail`.`flatchargespan`, `salesdetail`.`taxcategoryid`, `salesdetail`.`ckeyword`, `salesdetail`.`ckeywordvalue`, `salesdetail`.`ckeywordoriginalvalue`, `salesdetail`.`processcounterid`, `salesdetail`.`stocktypeid`, `stocktype`.`stocktypename`, `sizemaster`.`sizemastername`, `salesdetail`.`orderitemsize`, `salesdetail`.`prewastage`, `salesdetail`.`premakingcharge`, `salesdetail`.`comment`, DATE_FORMAT(salesdetail.orderduedate, '%d-%m-%Y') AS orderduedate, `salesdetail`.`tagimage`,ROUND(salesdetail.lstcharge,'".$amountround."') as lstcharge,ROUND(salesdetail.lstchargespan,'".$amountround."') as lstchargespan,ROUND(salesdetail.prelstcharge,'".$amountround."') as prelstcharge,salesdetail.lstchargekeylabel,salesdetail.lstchargespanlabel,salesdetail.flatchargespanlabel,salesdetail.flatchargespankey,ROUND(salesdetail.preflatcharge,".$amountround.") as preflatcharge,salesdetail.orderitemratefix,DATE_FORMAT(salesdetail.vendororderduedate,'%d-%m-%Y') AS vendororderduedate,salesdetail.ordervendorid,salesdetail.vendororderitemstatusid,vendororderitemstatus.vendororderitemstatusname,DATE_FORMAT(salesdetail.lastupdatedate,'%d-%m-%Y %H:%i:%s') as lastupdatedate ";
		$join = 'LEFT OUTER JOIN salesdetail as posalesdetail ON salesdetail.presalesdetailid=posalesdetail.salesdetailid and posalesdetail.status NOT IN (0,3) ';
		$join.= 'LEFT OUTER JOIN `itemtag` ON `itemtag`.`itemtagid`=`salesdetail`.`itemtagid`';
		$join.= 'LEFT OUTER JOIN `stocktype` ON `stocktype`.`stocktypeid`=`salesdetail`.`stocktypeid`';
		$join.= 'LEFT OUTER JOIN `orderstatus` ON `orderstatus`.`orderstatusid`=`salesdetail`.`orderstatusid`';
		$join.= 'LEFT OUTER JOIN `sizemaster` ON `sizemaster`.`sizemasterid`=`salesdetail`.`orderitemsize`';
		$join.= 'LEFT OUTER JOIN `product` ON `product`.`productid`=`salesdetail`.`productid`';
		$join.= 'LEFT OUTER JOIN `purity` ON `purity`.`purityid`=`salesdetail`.`purityid`';
		$join.= 'LEFT OUTER JOIN `counter` ON `counter`.`counterid`=`salesdetail`.`counterid`';
		$join.= 'LEFT OUTER JOIN `employee` ON `employee`.`employeeid`=`salesdetail`.`employeeid`';
		$join.= 'LEFT OUTER JOIN `sales` ON `sales`.`salesid`=`salesdetail`.`salesid`';
		$join.= 'LEFT OUTER JOIN `sales` as `posales` ON `posales`.`salesid`=`posalesdetail`.`salesid`';
		$join.= 'LEFT OUTER JOIN `vendororderitemstatus` ON `vendororderitemstatus`.`vendororderitemstatusid`=`salesdetail`.`vendororderitemstatusid`';
		$cuscondition = "".$genposalesstatus_where." AND ".$billwhere." = '".$primaryid."' AND `salesdetail`.`orderstatusid` = '3' AND ".$genpostatus_where." AND `salesdetail`.`stocktypeid` = '76' AND `salesdetail`.`salesdetailid` NOT IN(1) AND `salesdetail`.`status` = 1 AND";
		$status=$tablename.'.status='.$this->Basefunctions->activestatus.'';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		/* query */
		$result = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from salesdetail '.$join.' WHERE '.$cuscondition.' '. $status.' ORDER BY '.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$data=array();
		$i=0;
		foreach($result->result() as $row) {
			$salesdetailid = $row->salesdetailid;
			$vendororderitemstatusname = $row->vendororderitemstatusname;
			$lastupdatedate = $row->lastupdatedate;
			$productname =$row->productname;
			$purityname =$row->purityname;
			$grossweight =$row->grossweight;
			$stoneweight =$row->stoneweight;
			$netweight =$row->netweight;
			$pieces =$row->pieces;
			$orderduedate =$row->orderduedate;
			$tagimage =$row->tagimage;
			$ordernumber =$row->orsalesnumber;
			$poordernumber =$row->posalesnumber;
			$vendororderitemstatusid =$row->vendororderitemstatusid;
			$data[$i]=array('id'=>$salesdetailid,$vendororderitemstatusname,$lastupdatedate,$ordernumber,$orderduedate,$tagimage,$productname,$purityname,$grossweight,$stoneweight,$netweight,$pieces,$poordernumber,$vendororderitemstatusid,$salesdetailid);
			$i++;
		}
		$finalresult=array('0'=>$data,'1'=>$page,'2'=>'json');
		return $finalresult;
	}
	//Vendor Status grid information fetch - Product details in product field
	public function retrievevendorproductdetailinformation() {
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$salesid = $_POST['salesid'];
		$genposalesstatus_where = 'salesdetail.poorderstatusid IN(21) AND salesdetail.generatepo IN(1)';
		$billwhere = 'sales.salesid';
		$genpostatus_where = '1=1';
		$dataset = "`posales`.`salesnumber` as `orsalesnumber`,`sales`.`salesid`, `salesdetail`.`salesdetailid`, `product`.`productname`, `purity`.`purityname`, ROUND(salesdetail.grossweight, '".$round."') as grossweight,`salesdetail`.`pieces`, `salesdetail`.`productid`, `salesdetail`.`purityid`";
		$join = 'LEFT OUTER JOIN salesdetail as posalesdetail ON salesdetail.presalesdetailid=posalesdetail.salesdetailid and posalesdetail.status NOT IN (0,3) ';
		$join.= 'LEFT OUTER JOIN `sizemaster` ON `sizemaster`.`sizemasterid`=`salesdetail`.`orderitemsize`';
		$join.= 'LEFT OUTER JOIN `product` ON `product`.`productid`=`salesdetail`.`productid`';
		$join.= 'LEFT OUTER JOIN `purity` ON `purity`.`purityid`=`salesdetail`.`purityid`';
		$join.= 'LEFT OUTER JOIN `sales` ON `sales`.`salesid`=`salesdetail`.`salesid`';
		$join.= 'LEFT OUTER JOIN `sales` as `posales` ON `posales`.`salesid`=`posalesdetail`.`salesid`';
		$cuscondition = "".$genposalesstatus_where." AND ".$billwhere." = '".$salesid."' AND `salesdetail`.`orderstatusid` = '3'  AND `salesdetail`.`stocktypeid` = '76' AND `salesdetail`.`salesdetailid` NOT IN(1) AND `salesdetail`.`status` = 1 AND";
		$status='sales.status='.$this->Basefunctions->activestatus.'';
		$result = $this->db->query('select '.$dataset.' from salesdetail '.$join.' WHERE '.$cuscondition.' '. $status.'');
		$i=0;
		if($result->num_rows() > 0) {
			foreach($result->result() as $info) {
				$dataordernumber[$i] = array('salesdetailid'=>$info->salesdetailid,'ordernumber'=>$info->orsalesnumber,'productname'=>$info->productname,'purityname'=>$info->purityname,'grossweight'=>$info->grossweight);
				$i++;
			}
		}
		$datas = ( ($i==0)? json_encode(array('fail'=>'FAILED')) : json_encode($dataordernumber) );
		echo $datas;
	}
	//get estimation number data set mainestimate main estimatefetch loadinnergrid loadoverlaygrid
	public function getestimatedatamodel() {
		$estimationnumber = $_POST['estimationnumber'];
		$deliveryoderstatus = $_POST['deliveryoderstatus'];
		$default = 0;
		$empty = '';
		$stoneentry = '';
		$stonedetails = '';
		$taxentry = '';
		$accountid = '';
		$accountname = '';
		$taxentrydata = '';
		$transactionmodeid =1;
		$estimatearray['errormsg'] = '';
		$tagnoarray=array();
		$estimationid = $this->chequeestimationnumberstatus($estimationnumber);//check valid ot not
		if($estimationid != 0) {//if valid estimation number
			$estimationnewitemdetail = $this->estimationnewdetailscount($estimationid);
			if($estimationnewitemdetail > 0 && $deliveryoderstatus == 1){
				$estimationdata = array('status'=>'invalid');
				echo json_encode($estimationdata);
			}else {
				$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
				$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
				$rateround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //Rate round-off
				$meltinground = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',4); //Rate round-off
				$dia_weight_round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',8);
				$taxround = 2;
				$adjustmentamountkeypressed = 0;
				$billleveltaxautoremoval = 0;
				$this->db->select( 
				'sales.branchid,sales.adjustmentamountkeypressed,sales.billleveltaxautoremoval,itemtag.status as tagstatus,account.mobilenumber,sales.summarytaxamount,sales.summaryroundvalue,sales.transactionmodeid,account.accountname,
				sales.salesid,sales.accountid,ROUND(sales.summarydiscountamount,'.$amountround.') as summarydiscountamount,salesdetail.salesdetailid,salesdetail.itemtagid,salesdetail.purityid,purity.purityname,salesdetail.productid,product.productname,salesdetail.counterid,counter.countername,salesdetail.processcounterid,
				ROUND(salesdetail.grossweight,'.$round.') as grossweight,salesdetail.comment,
				ROUND(salesdetail.stoneweight,'.$round.') as stoneweight,
				ROUND(salesdetail.dustweight,'.$round.') as dustweight,
				ROUND(salesdetail.netweight,'.$round.') as netweight,
				ROUND(salesdetail.caratweight,'.$dia_weight_round.') as caratweight,
				ROUND(salesdetail.wastageweight,'.$round.') as wastageweight,ROUND(itemtag.grossweight,'.$round.') as itemgrossweight,
				ROUND(salesdetail.stockgrossweight,'.$round.') as stockgrossweight,
				ROUND(salesdetail.partialgrossweight,'.$round.') as partialgrossweight,
				salesdetail.pieces,salesdetail.stocktypeid,stocktype.stocktypename,ROUND(salesdetail.ratepergram,'.$rateround.') as ratepergram,ROUND(salesdetail.totalamount,'.$amountround.') as totalamount,ROUND(salesdetail.pureweight,'.$round.') as pureweight,itemtag.itemtagnumber,itemtag.rfidtagno,salesdetail.salesdetailimage,salesdetail.ordernumber,DATE_FORMAT(salesdetail.orderduedate,"%d-%m-%Y") AS orderduedate,salesdetail.issuereceipttypeid,salesdetail.netwtcalctypeid,salesdetail.olditemdetails,salesdetail.modeid,ROUND(salesdetail.discountamount,'.$amountround.') as discountamount,ROUND(salesdetail.taxamount,'.$amountround.') as taxamount,salesdetail.calculationtypeid,ROUND(salesdetail.itemdiscountlimit,'.$amountround.') as itemdiscountlimit,salesdetail.discountpercent,ROUND(salesdetail.grossamount,'.$amountround.') as grossamount,ROUND(salesdetail.stoneamount,'.$amountround.') as stoneamount,
				ROUND(salesdetail.melting,'.$meltinground.') as melting,
				ROUND(salesdetail.wastage,'.$amountround.') as wastage,ROUND(salesdetail.making,'.$amountround.') as making,
				ROUND(salesdetail.wastagespan,'.$amountround.') as wastagespan,ROUND(salesdetail.makingspan,'.$amountround.') as makingspan,
				ROUND(salesdetail.prewastage,'.$amountround.') as prewastage,ROUND(salesdetail.premakingcharge,'.$amountround.') as premakingcharge,
				ROUND(salesdetail.flatcharge,'.$amountround.') as flatcharge,ROUND(salesdetail.flatchargespan,'.$amountround.') as flatchargespan,
				ROUND(salesdetail.chargesamount,'.$amountround.') as chargesamount,ROUND(salesdetail.chitamount,'.$amountround.') as chitamount,ROUND(salesdetail.interestamt,'.$amountround.') as interestamt,ROUND(salesdetail.secondtouch,'.$meltinground.') as  secondtouch,salesdetail.wastagespanlabel,salesdetail.makingchargespanlabel,salesdetail.wastagespankey,salesdetail.makingchargespankey,salesdetail.employeeid,employee.employeename,salesdetail.pendingproductid,salesdetail.rateclone,salesdetail.rateaddition,salesdetail.estimationnumber,salesdetail.wastageless,salesdetail.taxcategoryid,salesdetail.orderitemsize,salesdetail.ordermodeid,salesdetail.lottypeid,salesdetail.purchasemodeid,salesdetail.netweightcalculationid,salesdetail.ratelesscalc,salesdetail.stockpieces,salesdetail.paymentaccountid,salesdetail.cardtypeid,cardtype.cardtypename,salesdetail.approvalcode,salesdetail.presalesdetailid,salesdetail.bankname,salesdetail.referencenumber,salesdetail.referencedate,salesdetail.ckeyword,salesdetail.ckeywordvalue,salesdetail.ckeywordoriginalvalue,salesdetail.oldjewelvoucherstatus,salesdetail.chitbookno,salesdetail.chitbookname,salesdetail.chitschemetypeid,olditemdetails.olditemdetailsname,lottype.lottypename,sizemaster.sizemastername,salesdetail.detailordernumberid,salesdetail.issuereceipttypeid,issuereceipttype.issuereceipttypename,salesdetail.creditadjusthidden,salesdetail.creditadjustvaluehidden,salesdetail.oldsalesreturntax,salesdetail.tagimage,ROUND(salesdetail.lstcharge,'.$amountround.') as lstcharge,ROUND(salesdetail.lstchargespan,'.$amountround.') as lstchargespan,ROUND(salesdetail.prelstcharge,'.$amountround.') as prelstcharge,salesdetail.lstchargekeylabel,salesdetail.lstchargespanlabel,salesdetail.flatchargespanlabel,salesdetail.flatchargespankey,ROUND(salesdetail.preflatcharge,'.$amountround.') as preflatcharge,salesdetail.orderitemratefix,ROUND(salesdetail.wastageweight,'.$round.') as wastageweight,ROUND(salesdetail.chitgram,'.$amountround.') as chitgram,salesdetail.vendororderduedate,salesdetail.ordervendorid,salesdetail.proditemrate,ROUND(salesdetail.repaircharge,'.$amountround.') as repaircharge,ROUND(salesdetail.repairchargespan,'.$amountround.') as repairchargespan,ROUND(salesdetail.prerepaircharge,'.$amountround.') as prerepaircharge,salesdetail.repairchargespanlabel,salesdetail.repairchargekeylabel,ROUND(salesdetail.itemwastagevalue,'.$amountround.') as itemwastagevalue,ROUND(salesdetail.itemmakingvalue,'.$amountround.') as itemmakingvalue,ROUND(salesdetail.itemflatvalue,'.$amountround.') as itemflatvalue,ROUND(salesdetail.itemlstvalue,'.$amountround.') as itemlstvalue,ROUND(salesdetail.itemrepairvalue,'.$amountround.') as itemrepairvalue,ROUND(salesdetail.itemchargesamtvalue,'.$amountround.') as itemchargesamtvalue',false);
				$this->db->from('salesdetail');
				$this->db->join('sales','sales.salesid = salesdetail.salesid','left outer');
				$this->db->join('account','account.accountid = sales.accountid','left outer');
				$this->db->join('itemtag','itemtag.itemtagid = salesdetail.itemtagid','left outer');
				$this->db->join('stocktype','stocktype.stocktypeid = salesdetail.stocktypeid','left outer');
				$this->db->join('product','product.productid = salesdetail.productid','left outer');
				$this->db->join('purity','purity.purityid = salesdetail.purityid','left outer');
				$this->db->join('counter','counter.counterid = salesdetail.counterid','left outer');
				$this->db->join('employee','employee.employeeid = salesdetail.employeeid','left outer');
				$this->db->join('cardtype','cardtype.cardtypeid = salesdetail.cardtypeid','left outer');
				$this->db->join('lottype','lottype.lottypeid = salesdetail.lottypeid','left outer');
				$this->db->join('olditemdetails','olditemdetails.olditemdetailsid = salesdetail.olditemdetails','left outer');
				$this->db->join('issuereceipttype','issuereceipttype.issuereceipttypeid = salesdetail.issuereceipttypeid','left outer');
				$this->db->join('sizemaster','sizemaster.sizemasterid = salesdetail.orderitemsize','left outer');
				$this->db->where('sales.salesnumber',$estimationnumber);
				$this->db->where_not_in('salesdetail.status',array(0,3));
				$this->db->where_not_in('sales.status',array(0,3,4));
				$info = $this->db->get();
				if($info->num_rows() > 0) {
					$j=1;
					foreach($info->result() as $value) {
					if((in_array($value->stocktypeid,array(11,13,74)) AND $value->tagstatus == 1) OR (!in_array($value->stocktypeid,array(11,13,74))))
					{
						if($value->stocktypeid == 74){
							$stocktypeid = 11;
							$value->stocktypeid = 11;
						}
						$stoneentry = $this->get_sales_stoneentry_div($value->salesid,$value->salesdetailid);
						$stonedetails = $this->get_sales_stoneentry_details($value->salesid,$value->salesdetailid);
						$taxentry = $this->get_sales_taxentry_div($value->salesid,$value->salesdetailid);
						$taxentrydata = $this->get_sales_taxentry_data($value->salesid,$value->salesdetailid);
						$accountid = $value->accountid;
						$accountname = $value->accountname;
						$adjustmentamountkeypressed = $value->billleveltaxautoremoval;
						$billleveltaxautoremoval = $value->billleveltaxautoremoval;
						$transactionmodeid = $value->transactionmodeid;
						// charges
						$ckeyword=array();
						$ckeywordvalue=array();
						$keyworddetails=array();
						$keyworddata=array();
						if($value->ckeyword != '' && $value->ckeywordvalue != ''){
							$ckeyword = explode(',',$value->ckeyword);
							$ckeywordvalue = explode(',',$value->ckeywordvalue);
						$keyworddetails = array_combine($ckeyword, $ckeywordvalue);
						$k = 0;
						foreach($keyworddetails as $k => $l){
							$keyworddata[] = $k. ":".$l;
							$k++;
						}
						$keyword_str = implode (",", $keyworddata);
						}else{
							$keyword_str ='';
						}
						$ckeyword1=array();
						$ckeywordoriginalvalue=array();
						$keyworddetails1=array();
						$keyworddata1=array();
						if($value->ckeyword != ''){
							$ckeyword1 = explode(',',$value->ckeyword);
							$ckeywordoriginalvalue = explode(',',$value->ckeywordoriginalvalue); 
						$keyworddetails1 = array_combine($ckeyword1, $ckeywordoriginalvalue);
						$m = 0;
						foreach($keyworddetails1 as $m => $p){
							$keyworddata1[] = $m. ":".$p;
							$m++;
						}
						$keyword_str1 = implode (",", $keyworddata1);
						}else{
							$keyword_str1 ='';
						}
						$branchprodpur = ''.$value->branchid.'-'.$value->productid.'-'.$value->purityid;
						$branchprodpurcount = ''.$value->branchid.'-'.$value->productid.'-'.$value->purityid.'-'.$value->counterid;
						$estimatedetail->rows[$j]['id'] = $j;
						$estimatedetail->rows[$j]['cell']=array(
							$value->stocktypeid,
							$value->stocktypename,
							$default,
							$empty,
							$value->totalamount,
							$default,
							$value->itemtagnumber,
							$value->rfidtagno,
							$value->productid,
							$value->productname,
							$value->purityid,
							$value->purityname,
							$value->counterid,
							$value->countername,
							$value->grossweight,
							$value->stoneweight,
							$value->netweight,
							$value->pieces,
							$value->ratepergram,
							$value->wastage,
							$value->making,
							$value->stoneamount,
							$value->taxamount,
							$value->discountamount,
							$value->grossamount,
							$value->employeeid,
							$value->employeename,
							$value->wastagespan,
							$value->makingspan,
							$stoneentry,
							$value->flatcharge,
							$value->flatchargespan,
							$default,
							$default,
							$value->chargesamount,
							$value->wastagespan,
							$value->makingspan,
							$value->flatchargespan,
							$value->dustweight,
							$value->wastageless,
							$value->stockgrossweight,
							$value->partialgrossweight,
							$value->modeid,
							$default,
							$value->itemtagid,
							$default,
							$default,
							$default,
							$taxentry,
							$value->itemdiscountlimit,
							$value->issuereceipttypeid,
							$value->paymentaccountid,
							$this->Basefunctions->singlefieldfetch('accountname','accountid','account',$value->paymentaccountid),
							$value->cardtypeid,
							$value->cardtypename,
							$value->approvalcode,
							$value->presalesdetailid,
							$value->netweightcalculationid,
							$value->ratelesscalc,
							$taxentrydata,
							$default,
							$stonedetails,
							$value->pendingproductid,
							$value->wastagespanlabel,
							$value->makingchargespanlabel,
							$value->wastagespankey,
							$value->makingchargespankey,
							$value->ckeyword,
							$value->ckeywordvalue,
							$value->ckeywordoriginalvalue,
							$keyword_str,
							$keyword_str1,
							$value->chargesamount,
							$value->discountpercent,
							$value->discountpercent,
							$estimationnumber,
							$value->olditemdetails,
							$value->oldjewelvoucherstatus,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$value->processcounterid,
							$value->taxcategoryid,
							$value->olditemdetailsname,
							$value->prewastage,
							$value->premakingcharge,
							$value->stockpieces,
							$empty,
							$empty,
							$empty,
							$empty,
							$value->secondtouch,
							$value->pureweight,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$value->comment,
							$empty,
							'0000-00-00',
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$empty,
							$value->tagimage,
							$value->lstcharge,
							$value->lstchargespan,
							$value->lstchargespan,
							$value->lstchargekeylabel,
							$value->lstchargespanlabel,
							$value->prelstcharge,
							$value->flatchargespanlabel,
							$value->flatchargespankey,
							$value->preflatcharge,
							$branchprodpur,
							$branchprodpurcount,
							$empty,
							$empty,
							$empty,
							$value->orderitemratefix,
							$value->wastageweight,
							$value->chitgram,
							$empty,
							'0000-00-00',
							$value->ordervendorid,
							$empty,
							$value->proditemrate,
							$value->caratweight,
							$value->repaircharge,
							$value->repairchargekeylabel,
							$value->repairchargespanlabel,
							$value->prerepaircharge,
							$value->repairchargespan,
							$value->repairchargespan,
							$value->itemwastagevalue,
							$value->itemmakingvalue,
							$value->itemflatvalue,
							$value->itemlstvalue,
							$value->itemrepairvalue,
							$value->itemchargesamtvalue
						);
					$j++;
					$amountarray = array('discount'=>$value->summarydiscountamount,'summarytaxamount'=>$value->summarytaxamount,'roundvalue'=>$value->summaryroundvalue);
					if(!empty($value->itemtagnumber)) {
						array_push($tagnoarray,$value->itemtagnumber);
					}
				 }else if((in_array($value->stocktypeid,array(11,13,74)) AND $value->tagstatus != 1)){
					 if($estimatearray['errormsg'] == ''){
						$estimatearray['errormsg'] .= 'Tag Nos: '.$value->itemtagnumber.'';
					 }else {$estimatearray['errormsg'] .= ','.$value->itemtagnumber.'';}
				 }
				}
				}else{
					$estimatedetail = ' ';
				}
				if($estimatearray['errormsg'] != ''){
					$estimatearray['errormsg'] .= ' are not active and not loaded in the inner grid. Rest are loaded.';
				}
				$accounttypeid = '';
				if($accountid != '' || $accountid != 1) {
					$accounttypeid = $this->Basefunctions->singlefieldfetch('accounttypeid','accountid','account',$accountid);
				}
				$estimatearray['accountid'] = $accountid;
				$estimatearray['accountname'] = $accountname;
				$estimatearray['accounttypeid'] = $accounttypeid;
				$estimatearray['transactionmodeid'] = $transactionmodeid; 
				$estimatearray['mobilenumber'] = $value->mobilenumber; 
				$estimatearray['estimatedata'] = $estimatedetail; 
				$estimatearray['amountarray'] = $amountarray; 
				$estimatearray['tagarray'] = $tagnoarray; 
				$estimatearray['adjustmentamountkeypressed'] = $adjustmentamountkeypressed; 
				$estimatearray['billleveltaxautoremoval'] = $billleveltaxautoremoval; 
				
				echo json_encode($estimatearray);
			}
		} else {//if not throw error message
			$estimationdata = array('status'=>'fail');
			echo json_encode($estimationdata);	
		}
	}
	// main salesdetail fetch mainsalesdetail loadinnegrid loadoverlaygrid
	public function billeditsalesdetails() {
		$salesid = $_POST['salesid'];
		$default = 0;
		$empty = '';
		$stoneentry = '';
		$stonedetails = '';
		$taxentry = '';
		$taxentrydata = '';
		$paymentamt = 0;
		$paymentstocktypeid = 0;
		$paymentstocktypename = '';
		$stocktypename = '';
		$stocktypeid = 0;
		$modeid = 0;
		$paymentmodeid = 0;
		$totalamount = 0;
		$paymenttouch = 0;
		$touch = 0;
		$pureweight = 0;
		$paymentpureweight = 0;
		$referencedate = '';
		$paymentaccountname = '';
		$productid = 1;
		$paymentproductid = 1;
		$productname = '';
		$paymentproductname = '';
		$counterid = 1;
		$paymentcounterid = 1;
		$countername = '';
		$paymentcountername = '';
		$purityid = 1;
		$paymentpurityid =1;
		$purityname = '';
		$paymentpurityname = '';
		$grosswt = 0;
		$paymentgrosswt = 0;
		$netwt = 0;
		$itemcaratweight = 0;
		$paymentnetwt = 0;
		$rate = 0;
		$paymentrate = 0;
		$paymentlesswt = 0;
		$paymentpieces = 0;
		$comment = '';
		$paymentcomment = '';
		$employeeid = 1;
		$paymentemployeeid = 1;
		$employeename = '';
		$paymentemployeename = '';
		$vendorid = 1;
		$categoryid = 1;
		$categoryname = '';
		$orderitemratefixname = '';
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$rateround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //Rate round-off
		$meltinground = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',4); //Rate round-off
		$dia_weight_round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',8);

		$this->db->select(
		'sales.branchid,sales.salesid,sales.accountid,ROUND(sales.summarydiscountamount,'.$amountround.') as summarydiscountamount,salesdetail.salesdetailid,salesdetail.itemtagid,salesdetail.purityid,purity.purityname,salesdetail.productid,product.productname,salesdetail.counterid,counter.countername,salesdetail.processcounterid,
		ROUND(salesdetail.grossweight,'.$round.') as grossweight,salesdetail.comment,
		ROUND(salesdetail.stoneweight,'.$round.') as stoneweight,
		ROUND(salesdetail.dustweight,'.$round.') as dustweight,
		ROUND(salesdetail.caratweight,'.$dia_weight_round.') as itemcaratweight,
		ROUND(salesdetail.netweight,'.$round.') as netweight,
		ROUND(salesdetail.wastageweight,'.$round.') as wastageweight,
		ROUND(itemtag.grossweight,'.$round.') as itemgrossweight,
		ROUND(salesdetail.stockgrossweight,'.$round.') as stockgrossweight,
		ROUND(salesdetail.partialgrossweight,'.$round.') as partialgrossweight,
		salesdetail.pieces,salesdetail.stocktypeid,stocktype.stocktypename,ROUND(salesdetail.ratepergram,'.$rateround.') as ratepergram,ROUND(salesdetail.totalamount,'.$amountround.') as totalamount,ROUND(salesdetail.pureweight,'.$round.') as pureweight,itemtag.itemtagnumber,itemtag.rfidtagno,salesdetail.salesdetailimage,salesdetail.proditemrate,salesdetail.ordernumber,salesdetail.orderitemratefix,DATE_FORMAT(salesdetail.orderduedate,"%d-%m-%Y") AS orderduedate,DATE_FORMAT(salesdetail.vendororderduedate,"%d-%m-%Y") AS vendororderduedate,salesdetail.ordervendorid,salesdetail.issuereceipttypeid,salesdetail.netwtcalctypeid,salesdetail.olditemdetails,salesdetail.modeid,ROUND(salesdetail.discountamount,'.$amountround.') as discountamount,ROUND(salesdetail.taxamount,'.$amountround.') as taxamount,salesdetail.calculationtypeid,ROUND(salesdetail.itemdiscountlimit,'.$amountround.') as itemdiscountlimit,salesdetail.discountpercent,ROUND(salesdetail.grossamount,'.$amountround.') as grossamount,ROUND(salesdetail.stoneamount,'.$amountround.') as stoneamount,ROUND(salesdetail.melting,'.$meltinground.') as melting,
		ROUND(salesdetail.wastage,'.$amountround.') as wastage,ROUND(salesdetail.making,'.$amountround.') as making,
		ROUND(salesdetail.wastagespan,'.$amountround.') as wastagespan,ROUND(salesdetail.makingspan,'.$amountround.') as makingspan,
		ROUND(salesdetail.prewastage,'.$amountround.') as prewastage,ROUND(salesdetail.premakingcharge,'.$amountround.') as premakingcharge,
		ROUND(salesdetail.flatcharge,'.$amountround.') as flatcharge,ROUND(salesdetail.flatchargespan,'.$amountround.') as flatchargespan,
		ROUND(salesdetail.chargesamount,'.$amountround.') as chargesamount,ROUND(salesdetail.chitamount,'.$amountround.') as chitamount,ROUND(salesdetail.interestamt,'.$amountround.') as interestamt,ROUND(salesdetail.secondtouch,'.$meltinground.') as secondtouch,salesdetail.wastagespanlabel,salesdetail.makingchargespanlabel,salesdetail.wastagespankey,salesdetail.makingchargespankey,salesdetail.employeeid,salesdetail.memployeeid,employee.employeename,salesdetail.pendingproductid,salesdetail.rateclone,salesdetail.rateaddition,salesdetail.estimationnumber,salesdetail.wastageless,salesdetail.taxcategoryid,salesdetail.orderitemsize,salesdetail.ordermodeid,salesdetail.lottypeid,salesdetail.purchasemodeid,salesdetail.netweightcalculationid,salesdetail.ratelesscalc,salesdetail.stockpieces,salesdetail.paymentaccountid,salesdetail.cardtypeid,cardtype.cardtypename,salesdetail.approvalcode,salesdetail.presalesdetailid,salesdetail.bankname,salesdetail.referencenumber,salesdetail.referencedate,salesdetail.ckeyword,salesdetail.ckeywordvalue,salesdetail.ckeywordoriginalvalue,salesdetail.oldjewelvoucherstatus,salesdetail.chitbookno,ROUND(salesdetail.chitgram,'.$round.') as chitgram,salesdetail.chitbookname,salesdetail.chitschemetypeid,olditemdetails.olditemdetailsname,lottype.lottypename,sizemaster.sizemastername,salesdetail.detailordernumberid,salesdetail.issuereceipttypeid,issuereceipttype.issuereceipttypename,salesdetail.creditadjusthidden,salesdetail.creditadjustvaluehidden,salesdetail.oldsalesreturntax,salesdetail.categoryid,category.categoryname,salesdetail.tagimage,ROUND(salesdetail.lstcharge,'.$amountround.') as lstcharge,ROUND(salesdetail.lstchargespan,'.$amountround.') as lstchargespan,ROUND(salesdetail.prelstcharge,'.$amountround.') as prelstcharge,salesdetail.lstchargekeylabel,salesdetail.lstchargespanlabel,salesdetail.flatchargespanlabel,salesdetail.flatchargespankey,ROUND(salesdetail.preflatcharge,'.$amountround.') as preflatcharge,ROUND(salesdetail.prerepaircharge,'.$amountround.') as prerepaircharge,salesdetail.repairchargekeylabel,salesdetail.repairchargespanlabel,ROUND(salesdetail.repairchargespan,'.$amountround.') as repairchargespan,ROUND(salesdetail.repaircharge,'.$amountround.') as repaircharge,ROUND(salesdetail.itemwastagevalue,'.$amountround.') as itemwastagevalue,ROUND(salesdetail.itemmakingvalue,'.$amountround.') as itemmakingvalue,ROUND(salesdetail.itemflatvalue,'.$amountround.') as itemflatvalue,ROUND(salesdetail.itemlstvalue,'.$amountround.') as itemlstvalue,ROUND(salesdetail.itemrepairvalue,'.$amountround.') as itemrepairvalue,ROUND(salesdetail.itemchargesamtvalue,'.$amountround.') as itemchargesamtvalue',false);
		$this->db->from('salesdetail');
		$this->db->join('sales','sales.salesid = salesdetail.salesid','left outer');
		$this->db->join('itemtag','itemtag.itemtagid = salesdetail.itemtagid','left outer');
		$this->db->join('stocktype','stocktype.stocktypeid = salesdetail.stocktypeid','left outer');
		$this->db->join('product','product.productid = salesdetail.productid','left outer');
		$this->db->join('purity','purity.purityid = salesdetail.purityid','left outer');
		$this->db->join('counter','counter.counterid = salesdetail.counterid','left outer');
		$this->db->join('employee','employee.employeeid = salesdetail.employeeid','left outer');
		$this->db->join('cardtype','cardtype.cardtypeid = salesdetail.cardtypeid','left outer');
		$this->db->join('lottype','lottype.lottypeid = salesdetail.lottypeid','left outer');
		$this->db->join('issuereceipttype','issuereceipttype.issuereceipttypeid = salesdetail.issuereceipttypeid','left outer');
		$this->db->join('sizemaster','sizemaster.sizemasterid = salesdetail.orderitemsize','left outer');
		$this->db->join('olditemdetails','olditemdetails.olditemdetailsid = salesdetail.olditemdetails','left outer');
		$this->db->join('category','category.categoryid = salesdetail.categoryid','left outer');
		$this->db->where('sales.salesid',$salesid);
		$this->db->where('salesdetail.advanceorderpay',0);
		$this->db->where('salesdetail.orderstatusid !=',6);
		$this->db->where_not_in('salesdetail.status',array(0,3));
		$this->db->where_not_in('sales.status',array(0,3));
		$info = $this->db->get();
		if($info->num_rows() > 0) {
			$j=1;
			foreach($info->result() as $value) {
				if($value->stocktypeid == 74)
				{
					$value->stocktypeid = 11;
					$value->stocktypename = 'Tag';
				}
				// charges
				$ckeyword=array();
				$ckeywordvalue=array();
				$keyworddetails=array();
				$keyworddata=array();
				if($value->ckeyword != '' && $value->ckeywordvalue != ''){
					$ckeyword = explode(',',$value->ckeyword);
					$ckeywordvalue = explode(',',$value->ckeywordvalue);
				$keyworddetails = array_combine($ckeyword, $ckeywordvalue);
				$k = 0;
				foreach($keyworddetails as $k => $l){
					$keyworddata[] = $k. ":".$l;
					$k++;
				}
				$keyword_str = implode (",", $keyworddata);
				}else{
					$keyword_str ='';
				}
				$ckeyword1=array();
				$ckeywordoriginalvalue=array();
				$keyworddetails1=array();
				$keyworddata1=array();
				if($value->ckeyword != ''){
					$ckeyword1 = explode(',',$value->ckeyword);
					$ckeywordoriginalvalue = explode(',',$value->ckeywordoriginalvalue); 
				$keyworddetails1 = array_combine($ckeyword1, $ckeywordoriginalvalue);
				$m = 0;
				foreach($keyworddetails1 as $m => $p){
					$keyworddata1[] = $m. ":".$p;
					$m++;
				}
				$keyword_str1 = implode (",", $keyworddata1);
				}else{
					$keyword_str1 ='';
				}
				if($value->orderitemratefix == 2) {
					$orderitemratefixname = 'Today';
				} else if($value->orderitemratefix == 3) {
					$orderitemratefixname = 'Delivery';
				} else {
					$orderitemratefixname = '';
				}
				$stoneentry = $this->get_sales_stoneentry_div($value->salesid,$value->salesdetailid);
				$stonedetails = $this->get_sales_stoneentry_details($value->salesid,$value->salesdetailid);
				$taxentry = $this->get_sales_taxentry_div($value->salesid,$value->salesdetailid);
				$taxentrydata = $this->get_sales_taxentry_data($value->salesid,$value->salesdetailid);
				$estimatedetail->rows[$j]['id'] = $j;
				if($value->ordervendorid > 1) {
					$ordervendoridname = $this->Basefunctions->singlefieldfetch('accountname','accountid','account',$value->ordervendorid); //vendor name
				} else {
					$ordervendoridname = '';
				}
				if($value->modeid == 3) {
					$paymentamt = $value->totalamount;
					$paymentstocktypeid = $value->stocktypeid;
					$stocktypeid = 0;
					$modeid = 0;
					$paymentmodeid = $value->modeid;
					$paymentstocktypename = $value->stocktypename;
					$stocktypename = '';
					$referencedate = $this->Basefunctions->ymd_format($value->referencedate);
					if($this->Basefunctions->singlefieldfetch('accountname','accountid','account',$value->paymentaccountid) == 1){
						$paymentaccountname = '';
					}else {
						$paymentaccountname = $this->Basefunctions->singlefieldfetch('accountname','accountid','account',$value->paymentaccountid);
					}
					$totalamount = 0;
					$paymenttouch = $value->secondtouch;
					$touch = 0;
					$pureweight = 0;
					$paymentpureweight = $value->pureweight;
					$productid = 1;
					$paymentproductid = $value->productid;
					$productname = '';
					$paymentproductname = $value->productname;
					$counterid = 1;
					$paymentcounterid = $value->counterid;
					$countername = '';
					$paymentcountername = $value->countername;
					$purityid = 1;
					$paymentpurityid =$value->purityid;
					$purityname = '';
					$paymentpurityname = $value->purityname;
					$grosswt = 0;
					$paymentgrosswt = $value->grossweight;
					$netwt = 0;
					$paymentnetwt = $value->netweight;
					$rate = 0;
					$paymentrate = $value->ratepergram;
					$comment = '';
					$paymentcomment = $value->comment;
					$employeeid = 1;
					$paymentemployeeid = $value->employeeid;
					$employeename = '';
					$paymentemployeename = $value->employeename;
					$categoryid = 1;
					$categoryname = '';
					$paymentlesswt = $value->dustweight;
					$paymentpieces = $value->pieces;
				}else{
					$paymentlesswt = 0;
					$paymentpieces = 0;
					$paymentamt = 0;
					$totalamount = $value->totalamount;
					$paymentstocktypeid = $default;
					$stocktypeid = $value->stocktypeid;
					if($stocktypeid == 82)
					{
						$vendorid = $value->paymentaccountid;
					}
					$modeid = $value->modeid;
					$paymentmodeid = 0;
					$paymentstocktypename = '';
					$stocktypename = $value->stocktypename;
					$referencedate = '';
					$paymentaccountname = '';
					$paymenttouch = 0;
					$touch = $value->secondtouch;
					$pureweight = $value->pureweight;
					$paymentpureweight = 0;
					$productid = $value->productid;
					$paymentproductid = 1;
					$productname = $value->productname;
					$paymentproductname = '';
					$counterid = $value->counterid;
					$paymentcounterid = 1;
					$countername = $value->countername;
					$paymentcountername = '';
					$purityid = $value->purityid;
					$paymentpurityid =1;
					$purityname = $value->purityname;
					$paymentpurityname = '';
					if($value->itemcaratweight > 0) {
						$grosswt = 0;
						$netwt = 0;
					} else {
						$grosswt = $value->grossweight;
						$netwt = $value->netweight;
					}
					$paymentgrosswt = 0;
					$paymentnetwt = 0;
					$rate = $value->ratepergram;
					$paymentrate = 0;
					$comment = $value->comment;
					$paymentcomment = '';
					if($value->memployeeid != '1') {
						$employeeid = $value->employeeid.','.$value->memployeeid;
					} else {
						$employeeid = $value->employeeid;
					}
					$paymentemployeeid = 1;
					if($value->memployeeid != '1') {
						$employeename = $this->retrievemultipleemployees($employeeid);
					} else {
						$employeename = $value->employeename;
					}
					$paymentemployeename = '';
					$categoryid = $value->categoryid;
					$categoryname = $value->categoryname;
				}
				$branchprodpur = ''.$value->branchid.'-'.$value->productid.'-'.$value->purityid;
				$branchprodpurcount = ''.$value->branchid.'-'.$value->productid.'-'.$value->purityid.'-'.$value->counterid;
				if($value->flatchargespankey == 'undefined') {
					$value->flatchargespankey = '';
				} else {
					$value->flatchargespankey = $value->flatchargespankey;
				}
				$estimatedetail->rows[$j]['cell']=array(
					$stocktypeid,
					$stocktypename,
					$paymentstocktypeid,
					$paymentstocktypename,
					$totalamount,
					$paymentamt,
					$value->itemtagnumber,
					$value->rfidtagno,
					$productid,
					$productname,
					$purityid,
					$purityname,
					$counterid,
					$countername,
					$grosswt,
					$value->stoneweight,
					$netwt,
					$value->pieces,
					$rate,
					$value->wastage,
					$value->making,
					$value->stoneamount,
					$value->taxamount,
					$value->discountamount,
					$value->grossamount,
					$employeeid,
					$employeename,
					$value->wastagespan,
					$value->makingspan,
					$stoneentry,
					$value->flatcharge,
					$value->flatchargespan,
					$default,
					$default,
					$value->chargesamount,
					$value->wastagespan,
					$value->makingspan,
					$value->flatchargespan,
					$value->dustweight,
					$value->wastageless,
					$value->stockgrossweight,
					$value->partialgrossweight,
					$modeid,
					$paymentmodeid,
					$value->itemtagid,
					$value->bankname,
					$value->referencenumber,
					$referencedate,
					$taxentry,
					$value->itemdiscountlimit,
					$value->issuereceipttypeid,
					$value->paymentaccountid,
					$paymentaccountname,
					$value->cardtypeid,
					$value->cardtypename,
					$value->approvalcode,
					$value->presalesdetailid,
					$value->netweightcalculationid,
					$value->ratelesscalc,
					$taxentrydata,
					$default,
					$stonedetails,
					$value->pendingproductid,
					$value->wastagespanlabel,
					$value->makingchargespanlabel,
					$value->wastagespankey,
					$value->makingchargespankey,
					$value->ckeyword,
					$value->ckeywordvalue,
					$value->ckeywordoriginalvalue,
					$keyword_str,
					$keyword_str1,
					$value->chargesamount,
					$value->discountpercent,
					$value->discountpercent,
					$value->estimationnumber,
					$value->olditemdetails,
					$value->oldjewelvoucherstatus,
					$value->chitbookno,
					$value->chitbookname,
					$value->chitamount,
					$value->interestamt,
					$value->chitschemetypeid,
					$value->processcounterid,
					$value->taxcategoryid,
					$value->olditemdetailsname,
					$value->prewastage,
					$value->premakingcharge,
					$value->stockpieces,
					$value->lottypeid,
					$value->lottypename,
					$value->purchasemodeid,
					$touch,
					$pureweight,
					$paymentproductid,
					$paymentproductname,
					$paymentpurityid,
					$paymentpurityname,
					$paymentcounterid,
					$paymentcountername,
					$paymentgrosswt,
					$paymentnetwt,
					$value->melting,
					$paymenttouch,
					$paymentpureweight,
					$paymentrate,
					$value->orderitemsize,
					$value->sizemastername,
					$comment,
					$paymentcomment,
					$value->orderduedate,
					$value->detailordernumberid,
					$paymentemployeeid,
					$paymentemployeename,
					$value->ordernumber,
					$vendorid,
					$value->ordernumber,
					$empty,
					$value->issuereceipttypename,
					$value->issuereceipttypeid,
					$paymentstocktypeid.'-'.$value->issuereceipttypeid,
					$value->creditadjustvaluehidden,
					str_replace('zyx-0', '1', $value->creditadjusthidden),
					$value->salesdetailid,
					$value->oldsalesreturntax,
					$categoryid,
					$categoryname,	
					$value->tagimage,
					$value->lstcharge,
					$value->lstchargespan,
					$value->lstchargespan,
					$value->lstchargekeylabel,
					$value->lstchargespanlabel,
					$value->prelstcharge,
					$value->flatchargespanlabel,
					$value->flatchargespankey,
					$value->preflatcharge,
					$branchprodpur,
					$branchprodpurcount,
					$empty,
					$paymentlesswt,
					$paymentpieces,
					$value->orderitemratefix,
					$value->wastageweight,
					$value->chitgram,
					$orderitemratefixname,
					$value->vendororderduedate,
					$value->ordervendorid,
					$ordervendoridname,
					$value->proditemrate,
					$value->itemcaratweight,
					$value->repaircharge,
					$value->repairchargekeylabel,
					$value->repairchargespanlabel,
					$value->prerepaircharge,
					$value->repairchargespan,
					$value->repairchargespan,
					$value->itemwastagevalue,
					$value->itemmakingvalue,
					$value->itemflatvalue,
					$value->itemlstvalue,
					$value->itemrepairvalue,
					$value->itemchargesamtvalue
				);
			$j++;
			$amountarray = array('discount'=>$value->summarydiscountamount);
			}
		}else{
			$estimatedetail = ' ';
			$amountarray = '';
		}
		$estimatearray['estimatedata'] = $estimatedetail; 
		$estimatearray['amountarray'] = $amountarray; 
		echo json_encode($estimatearray);
	}
	// Retrieve multiple Employees list
	public function retrievemultipleemployees($employeeid) {
		$employeeid = explode(',',$employeeid);
		$employeenamearray = array();
		$this->db->select('employee.employeename');
		$this->db->from('employee');
		$this->db->where_in('employee.employeeid',$employeeid);
		$this->db->where_not_in('employee.status',array(0,3));
		$info = $this->db->get();
		if($info->num_rows() > 0) {
			foreach($info->result() as $value) {
				$employeenamearray[] = $value->employeename;
			}
		}
		return $employeenamearray;
	}
	// get bulk rfid rows
	public function bulksalesgriddatafetchmodel() {
		$approvalnumber = $_GET['approvalnumber'];
		$stocktypeid = $_GET['stocktypeid'];
		$salesid = 1;
		$billwhere = '';
		if($stocktypeid == 65) {
			if($approvalnumber == 'all'){
				$billnumber = $_GET['accountid'];
				$billwhere = "`sales`.`accountid` = '".$_GET['accountid']."' ";
			}else{
				$salesid = $this->Basefunctions->singlefieldfetch('salesid','salesnumber','sales',$approvalnumber);
				$billwhere =   "`itemtag`.`salesid` = '".$salesid."' ";
			}
		}
		$innergridtagids = $_GET['innergridtagids'];
		$explodeinnergridtagids = explode(',',$innergridtagids);
		if(empty($innergridtagids))
		{
			$innergridtagids = 1;
		}
		$id = $_GET['id'];
		$j=0;
		$default = 0;
		$empty = '';
		$stoneentry = '';
		$stonedetails = '';
		$taxentry = '';
		$taxentrydata = '';
		$stocktypename = '';
		$finalstocktypeid = 1;
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		//$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		//$rateround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //Rate round-off
		//$taxround = 2;
		if($stocktypeid == 62) {
			$info=$this->db->query("SELECT 1 as salesnumber,1 as salesdetailid, `itemtag`.`itemtagnumber`, `itemtag`.`rfidtagno`, `product`.`productname`, `purity`.`purityname`, `counter`.`countername`, ROUND(itemtag.grossweight, '".$round."') as grossweight, ROUND(itemtag.stoneweight, '".$round."') as stoneweight, ROUND(itemtag.netweight, '".$round."') as netweight, `itemtag`.`pieces`, `itemtag`.`productid`, `itemtag`.`purityid`, `itemtag`.`counterid`, `itemtag`.`itemtagid` FROM `itemtag` LEFT OUTER JOIN `product` ON `product`.`productid`=`itemtag`.`productid` LEFT OUTER JOIN `purity` ON `purity`.`purityid`=`itemtag`.`purityid` LEFT OUTER JOIN `counter` ON `counter`.`counterid`=`itemtag`.`counterid` WHERE `itemtag`.`itemtagid` IN (".$id.") AND `itemtag`.`status` = '1' AND `itemtag`.`itemtagid` NOT IN(".$innergridtagids.")");
			$stocktypename = 'App Out Tag';
			$finalstocktypeid = 62;
		}else if($stocktypeid == 65) {
			$info=$this->db->query("SELECT sales.salesnumber,`itemtag`.`salesdetailid`,`itemtag`.`itemtagnumber`, `itemtag`.`rfidtagno`, `product`.`productname`, `purity`.`purityname`, `counter`.`countername`, ROUND(itemtag.grossweight, '".$round."') as grossweight, ROUND(itemtag.stoneweight, '".$round."') as stoneweight, ROUND(itemtag.netweight, '".$round."') as netweight, `itemtag`.`pieces`, `itemtag`.`productid`, `itemtag`.`purityid`, `itemtag`.`counterid`, `itemtag`.`itemtagid` FROM `itemtag` LEFT OUTER JOIN `product` ON `product`.`productid`=`itemtag`.`productid` LEFT OUTER JOIN `purity` ON `purity`.`purityid`=`itemtag`.`purityid` LEFT OUTER JOIN `counter` ON `counter`.`counterid`=`itemtag`.`counterid` LEFT OUTER JOIN `salesdetail` ON `salesdetail`.`salesdetailid`=`itemtag`.`salesdetailid`  LEFT OUTER JOIN `sales` ON `sales`.`salesid`=`itemtag`.`salesid` 
			WHERE `itemtag`.`itemtagid` IN (".$id.")  and `itemtag`.`status` = '15' AND `itemtag`.`itemtagid` NOT IN (".$innergridtagids.") AND ".$billwhere."");
			$stocktypename = 'App Out Return';
			$finalstocktypeid = 65;
		}
		//$info=$this->db->query("SELECT `itemtag`.`itemtagnumber`, `itemtag`.`rfidtagno`, `product`.`productname`, `purity`.`purityname`, `counter`.`countername`, ROUND(itemtag.grossweight, '".$round."') as grossweight, ROUND(itemtag.stoneweight, '".$round."') as stoneweight, ROUND(itemtag.netweight, '".$round."') as netweight, `itemtag`.`pieces`, `itemtag`.`productid`, `itemtag`.`purityid`, `itemtag`.`counterid`, `itemtag`.`itemtagid` FROM `itemtag` JOIN `product` ON `product`.`productid`=`itemtag`.`productid` JOIN `purity` ON `purity`.`purityid`=`itemtag`.`purityid` JOIN `counter` ON `counter`.`counterid`=`itemtag`.`counterid` WHERE `itemtag`.`status` = '1' limit 1,100");
		if($info->num_rows() > 0) {
				$j=1;
				foreach($info->result() as $value) {
					/* $stoneentry = $this->get_sales_stoneentry_div($value->salesid,$value->salesdetailid);
					$stonedetails = $this->get_sales_stoneentry_details($value->salesid,$value->salesdetailid);
					$taxentry = $this->get_sales_taxentry_div($value->salesid,$value->salesdetailid);
					$taxentrydata = $this->get_sales_taxentry_data($value->salesid,$value->salesdetailid); */
				$estimatedetail->rows[$j]['id'] = $value->itemtagid;
				
				$estimatedetail->rows[$j]['cell']=array(
						$finalstocktypeid,
						$stocktypename,
						$default,
						$empty,
						$default,
						$default,
						$value->itemtagnumber,
						$value->rfidtagno,
						$value->productid,
						$value->productname,
						$value->purityid,
						$value->purityname,
						$value->counterid,
						$value->countername,
						$value->grossweight,
						$value->stoneweight,
						$value->netweight,
						$value->pieces,
						$default,
						$default,
						$default,
						$default,
						$default,
						$default,
						$default,
						$default,
						$empty,
						$empty,
						$empty,
						$empty, // stone entry
						$default,
						$empty,
						$default,
						$default,
						$default,
						$default,
						$default,
						$default,
						$default,
						$default,
						$default,
						$default,
						2,
						$default,
						$value->itemtagid,
						$default,
						$default,
						$default,
						$default,
						$default,
						$default,
						$default,
						$default,
						$default,
						$empty,
						$empty,
						$value->salesdetailid,
						$default,
						$default,
						$empty,
						$default,
						$empty, // $stonedetails,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$default,
						$default,
						$default,
						$default,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$default,
						$default,
						$default,
						1,
						$empty,
						1,
						$empty,
						$default,
						$default,
						1,
						$empty,
						1,
						$empty,
						1,
						$empty,
						$default,
						$default,
						$default,
						$default,
						$default,
						$default,
						$empty,
						$empty,
						$empty,
						$empty,
						$default,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						1,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,  // LST
						$empty,
						$empty,
						$empty,
						$empty,
						$empty,
						$value->salesnumber,
						$empty,
						$empty
				);
				$j++;
				}
				}else{
					$estimatedetail = ' ';
				}
				$estimatearray['estimatedata'] = $estimatedetail;
				// remove rfid data
				echo json_encode($estimatearray);
	}
	// issue receipt data load creditoverlay data
	public function issuereceiptgriddatafetchmodel() {
		$accountid = $_GET['accountid'];
		$loadbilltype = $_GET['loadbilltype'];
		$issuereceipttypeid = $_GET['issuereceipttypeid'];
		$status = $_GET['status'];
		$billeditstatus = $_GET['billeditstatus'];
		$generatesalesid = $_GET['generatesalesid'];
		$salesdetailid = $_GET['salesdetailid'];
		$creditnodata = '';
		$creditdiffstring = '';
		$creditwhere = '';
		$newcreditnoarray = array();
		$oldcreditnoarray = array();
		$creditnodiffarray = array();
		$prevsalescreditnodata = '';
		$j=0;
		$issuestatus = 0;
		$transactionmodeid = $_GET['transactionmodeid'];
		$irtype = 0;
		if($status == 1) {
			if($loadbilltype == 2) {
				$issuestatus = 2;
			}else if($loadbilltype == 3) {
				$issuestatus = 3;
			}
		}else if($status == 0) {
			if($issuereceipttypeid == 2) {
				$issuestatus = 2;
			}else if($issuereceipttypeid == 3) {
				$issuestatus = 3;
			}
		}
		if($billeditstatus == 1 && $status == 1) {
			$this->db->select('sales.creditno',false);
			$this->db->from('sales');
			$this->db->where('salesid',$generatesalesid);
			$this->db->where('sales.status',$this->Basefunctions->activestatus);
			$this->db->where('creditno !=','');
			$result=$this->db->get();
			if($result->num_rows() > 0) {
				foreach($result->result() as $info){
					$prevsalescreditnodata .= $info->creditno;
				}
				$prevsalescreditnodata = explode(',',$prevsalescreditnodata);
				for($i=0;$i<count($prevsalescreditnodata); $i++) {
					$creditnodata .= "'".$prevsalescreditnodata[$i]."',";
					array_push($newcreditnoarray,$prevsalescreditnodata[$i]);
				}
				$creditnodata = substr($creditnodata, 0, -1);
				$creditwhere = "AND creditno NOT IN (".$creditnodata.")";
			}
			$where = "AND accountledger.salesid NOT IN (".$generatesalesid.")";
		}else if($billeditstatus == 1 && $status == 0) {
			$this->db->select('salesdetail.referencenumber',false);
			$this->db->from('salesdetail');
			$this->db->where('salesdetailid',$salesdetailid);
			$this->db->where('salesdetail.status',$this->Basefunctions->activestatus);
			$this->db->where('referencenumber !=','');
			$result=$this->db->get();
			if($result->num_rows() > 0) {
				foreach($result->result() as $info){
					$prevsalescreditnodata .= $info->referencenumber;
				}
				$prevsalescreditnodata = explode(',',$prevsalescreditnodata);
				for($i=0;$i<count($prevsalescreditnodata); $i++) {
					$creditnodata .= "'".$prevsalescreditnodata[$i]."',";
					array_push($newcreditnoarray,$prevsalescreditnodata[$i]);
				}
				$creditnodata = substr($creditnodata, 0, -1);
				$creditwhere = "AND creditno NOT IN (".$creditnodata.")";
			}
			$where = "AND accountledger.salesid NOT IN (".$generatesalesid.")";
		}
		$salesdate = $this->Basefunctions->ymd_format($_GET['salesdate']);
		$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$weightround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$creditno = $_GET['creditno'];
		$creditarray = array();
		$creditstring = '';
		$creditarray = explode(',',$_GET['creditno']);
		for($i=0;$i<count($creditarray); $i++) {
			$creditstring .= "'".$creditarray[$i]."',";
			array_push($oldcreditnoarray,$creditarray[$i]);
		}
		$creditstring = substr($creditstring, 0, -1);
		if(empty($creditno)) {
			$where = '';
		}else {
			$where = "AND creditno NOT IN (".$creditstring.")";
		}
		$creditnodiffarray = array_values(array_diff($newcreditnoarray,$oldcreditnoarray));
		
		{// credit bill fetch 
		if($issuestatus == 2)  // issue
		{
			if($transactionmodeid == 4){ // amt purewt
				$info= $this->db->query("SELECT `accountledger`.`accountledgerid`, `accountledger`.`creditno`, `accountledger`.`referencenumber`, `accountledger`.`salesdate`, `accountledger`.`paymentirtypeid`, `accountledger`.`issuereceiptid`,`accountledger`.`weightissuereceiptid`, ROUND(accountledger.amtissue, '".$amountround."') as amtissue, ROUND(accountledger.amtreceipt, '".$amountround."') as amtreceipt, ROUND(accountledger.finalamtissue, '".$amountround."') as finalamtissue, ROUND(accountledger.finalamtreceipt, '".$amountround."') as finalamtreceipt, ROUND(accountledger.weightissue, '".$weightround."') as weightissue, ROUND(accountledger.weightreceipt, '".$weightround."') as weightreceipt, ROUND(accountledger.finalweightissue, '".$weightround."') as finalweightissue, ROUND(accountledger.finalweightreceipt, '".$weightround."') as finalweightreceipt, `paymentirtype`.`paymentirtypename`, `issuereceipttype`.`issuereceipttypename` FROM `accountledger` LEFT OUTER JOIN `paymentirtype` ON `paymentirtype`.`paymentirtypeid`=`accountledger`.`paymentirtypeid` LEFT OUTER JOIN `issuereceipttype` ON `issuereceipttype`.`issuereceipttypeid`=`accountledger`.`issuereceiptid` WHERE `accountledger`.`entrymode` = 4 AND `accountledger`.`accountid` = '".$accountid."' AND ( `accountledger`.`finalamtissue` >0 OR `accountledger`.`finalweightissue` >0) AND `accountledger`.`creditno` != '' AND `accountledger`.`salesdate` <= '".$salesdate."' AND `accountledger`.`status` = 1 $where $creditwhere");
			}else if($transactionmodeid == 3){ // purewt
				$info= $this->db->query("SELECT `accountledger`.`accountledgerid`, `accountledger`.`creditno`, `accountledger`.`referencenumber`, `accountledger`.`salesdate`, `accountledger`.`paymentirtypeid`, `accountledger`.`issuereceiptid`,`accountledger`.`weightissuereceiptid`, ROUND(accountledger.amtissue, '".$amountround."') as amtissue, ROUND(accountledger.amtreceipt, '".$amountround."') as amtreceipt, ROUND(accountledger.finalamtissue, '".$amountround."') as finalamtissue, ROUND(accountledger.finalamtreceipt, '".$amountround."') as finalamtreceipt, ROUND(accountledger.weightissue, '".$weightround."') as weightissue, ROUND(accountledger.weightreceipt, '".$weightround."') as weightreceipt, ROUND(accountledger.finalweightissue, '".$weightround."') as finalweightissue, ROUND(accountledger.finalweightreceipt, '".$weightround."') as finalweightreceipt, `paymentirtype`.`paymentirtypename`, `issuereceipttype`.`issuereceipttypename` FROM `accountledger` LEFT OUTER JOIN `paymentirtype` ON `paymentirtype`.`paymentirtypeid`=`accountledger`.`paymentirtypeid` LEFT OUTER JOIN `issuereceipttype` ON `issuereceipttype`.`issuereceipttypeid`=`accountledger`.`issuereceiptid` WHERE `accountledger`.`entrymode` = 4 AND `accountledger`.`accountid` = '".$accountid."' AND `accountledger`.`finalweightissue` >0 AND `accountledger`.`creditno` != '' AND `accountledger`.`salesdate` <= '".$salesdate."' AND `accountledger`.`status` = 1 $where $creditwhere");
			}else if($transactionmodeid == 2){ // amount
				$info= $this->db->query("SELECT `accountledger`.`accountledgerid`, `accountledger`.`creditno`, `accountledger`.`referencenumber`, `accountledger`.`salesdate`, `accountledger`.`paymentirtypeid`, `accountledger`.`issuereceiptid`,`accountledger`.`weightissuereceiptid`, ROUND(accountledger.amtissue, '".$amountround."') as amtissue, ROUND(accountledger.amtreceipt, '".$amountround."') as amtreceipt, ROUND(accountledger.finalamtissue, '".$amountround."') as finalamtissue, ROUND(accountledger.finalamtreceipt, '".$amountround."') as finalamtreceipt, ROUND(accountledger.weightissue, '".$weightround."') as weightissue, ROUND(accountledger.weightreceipt, '".$weightround."') as weightreceipt, ROUND(accountledger.finalweightissue, '".$weightround."') as finalweightissue, ROUND(accountledger.finalweightreceipt, '".$weightround."') as finalweightreceipt, `paymentirtype`.`paymentirtypename`, `issuereceipttype`.`issuereceipttypename` FROM `accountledger` LEFT OUTER JOIN `paymentirtype` ON `paymentirtype`.`paymentirtypeid`=`accountledger`.`paymentirtypeid` LEFT OUTER JOIN `issuereceipttype` ON `issuereceipttype`.`issuereceipttypeid`=`accountledger`.`issuereceiptid` WHERE `accountledger`.`entrymode` = 4 AND `accountledger`.`accountid` = '".$accountid."' AND `accountledger`.`finalamtissue` >0 AND `accountledger`.`creditno` != '' AND `accountledger`.`salesdate` <= '".$salesdate."' AND `accountledger`.`status` = 1 $where $creditwhere");
			}
		}else if($issuestatus == 3) // receipt
		{
			if($transactionmodeid == 4){ // amt purewt
				$info= $this->db->query("SELECT `accountledger`.`accountledgerid`, `accountledger`.`creditno`, `accountledger`.`referencenumber`, `accountledger`.`salesdate`, `accountledger`.`paymentirtypeid`, `accountledger`.`issuereceiptid`,`accountledger`.`weightissuereceiptid`, ROUND(accountledger.amtissue, '".$amountround."') as amtissue, ROUND(accountledger.amtreceipt, '".$amountround."') as amtreceipt, ROUND(accountledger.finalamtissue, '".$amountround."') as finalamtissue, ROUND(accountledger.finalamtreceipt, '".$amountround."') as finalamtreceipt, ROUND(accountledger.weightissue, '".$weightround."') as weightissue, ROUND(accountledger.weightreceipt, '".$weightround."') as weightreceipt, ROUND(accountledger.finalweightissue, '".$weightround."') as finalweightissue, ROUND(accountledger.finalweightreceipt, '".$weightround."') as finalweightreceipt, `paymentirtype`.`paymentirtypename`, `issuereceipttype`.`issuereceipttypename` FROM `accountledger` LEFT OUTER JOIN `paymentirtype` ON `paymentirtype`.`paymentirtypeid`=`accountledger`.`paymentirtypeid` LEFT OUTER JOIN `issuereceipttype` ON `issuereceipttype`.`issuereceipttypeid`=`accountledger`.`issuereceiptid` WHERE `accountledger`.`entrymode` = 4 AND `accountledger`.`accountid` = '".$accountid."' AND ( `accountledger`.`finalamtreceipt` >0 OR `accountledger`.`finalweightreceipt` >0 ) AND `accountledger`.`creditno` != '' AND `accountledger`.`salesdate` <= '".$salesdate."' AND `accountledger`.`status` = 1 $where $creditwhere");
			}else if($transactionmodeid == 3){ // purewt
				$info= $this->db->query("SELECT `accountledger`.`accountledgerid`, `accountledger`.`creditno`, `accountledger`.`referencenumber`, `accountledger`.`salesdate`, `accountledger`.`paymentirtypeid`, `accountledger`.`issuereceiptid`,`accountledger`.`weightissuereceiptid`, ROUND(accountledger.amtissue, '".$amountround."') as amtissue, ROUND(accountledger.amtreceipt, '".$amountround."') as amtreceipt, ROUND(accountledger.finalamtissue, '".$amountround."') as finalamtissue, ROUND(accountledger.finalamtreceipt, '".$amountround."') as finalamtreceipt, ROUND(accountledger.weightissue, '".$weightround."') as weightissue, ROUND(accountledger.weightreceipt, '".$weightround."') as weightreceipt, ROUND(accountledger.finalweightissue, '".$weightround."') as finalweightissue, ROUND(accountledger.finalweightreceipt, '".$weightround."') as finalweightreceipt, `paymentirtype`.`paymentirtypename`, `issuereceipttype`.`issuereceipttypename` FROM `accountledger` LEFT OUTER JOIN `paymentirtype` ON `paymentirtype`.`paymentirtypeid`=`accountledger`.`paymentirtypeid` LEFT OUTER JOIN `issuereceipttype` ON `issuereceipttype`.`issuereceipttypeid`=`accountledger`.`issuereceiptid` WHERE `accountledger`.`entrymode` = 4 AND `accountledger`.`accountid` = '".$accountid."' AND  `accountledger`.`finalweightreceipt` >0  AND `accountledger`.`creditno` != '' AND `accountledger`.`salesdate` <= '".$salesdate."' AND `accountledger`.`status` = 1 $where $creditwhere");
			}else if($transactionmodeid == 2){
				$info= $this->db->query("SELECT `accountledger`.`accountledgerid`, `accountledger`.`creditno`, `accountledger`.`referencenumber`, `accountledger`.`salesdate`, `accountledger`.`paymentirtypeid`, `accountledger`.`issuereceiptid`,`accountledger`.`weightissuereceiptid`, ROUND(accountledger.amtissue, '".$amountround."') as amtissue, ROUND(accountledger.amtreceipt, '".$amountround."') as amtreceipt, ROUND(accountledger.finalamtissue, '".$amountround."') as finalamtissue, ROUND(accountledger.finalamtreceipt, '".$amountround."') as finalamtreceipt, ROUND(accountledger.weightissue, '".$weightround."') as weightissue, ROUND(accountledger.weightreceipt, '".$weightround."') as weightreceipt, ROUND(accountledger.finalweightissue, '".$weightround."') as finalweightissue, ROUND(accountledger.finalweightreceipt, '".$weightround."') as finalweightreceipt, `paymentirtype`.`paymentirtypename`, `issuereceipttype`.`issuereceipttypename` FROM `accountledger` LEFT OUTER JOIN `paymentirtype` ON `paymentirtype`.`paymentirtypeid`=`accountledger`.`paymentirtypeid` LEFT OUTER JOIN `issuereceipttype` ON `issuereceipttype`.`issuereceipttypeid`=`accountledger`.`issuereceiptid` WHERE `accountledger`.`entrymode` = 4 AND `accountledger`.`accountid` = '".$accountid."' AND  `accountledger`.`finalamtreceipt` >0 AND `accountledger`.`creditno` != '' AND `accountledger`.`salesdate` <= '".$salesdate."' AND `accountledger`.`status` = 1 $where $creditwhere");
			}
		}
		$processid = 1;
		$processname = 'Auto';
		$creditreference =2;
		$creditreferencename ='Adjust';
		$amthidden = 0;
		$wthidden = 0;
		$editstatus = 'zyx-0';
		if($info->num_rows() > 0) 
		{
			$j=1;
			foreach($info->result() as $value) {
				if($value->issuereceiptid == 2){
					$amthidden = $value->finalamtissue;
				}else if($value->issuereceiptid == 3){
					$amthidden = $value->finalamtreceipt;
				}
				if($value->weightissuereceiptid == 2){
					$wthidden = $value->finalweightissue;
				}else if($value->weightissuereceiptid == 3){
					$wthidden = $value->finalweightreceipt;
				}
				$estimatedetail->rows[$j]['id'] = $value->accountledgerid;
				$estimatedetail->rows[$j]['cell']=array(
					$value->creditno,
					$this->Basefunctions->ymd_format($value->salesdate),
					$value->paymentirtypename,
					$value->issuereceipttypename,
					$value->amtissue,
					$value->amtreceipt,
					$value->finalamtissue,
					$value->finalamtreceipt,
					$value->issuereceiptid,
					$value->paymentirtypeid,
					$value->issuereceipttypename,
					$value->weightissue,
					$value->weightreceipt,
					$value->finalweightissue,
					$value->finalweightreceipt,
					$value->weightissuereceiptid,
					$value->accountledgerid,
					$processid,
					$processname,
					$creditreference,
					$creditreferencename,
					$amthidden,
					$wthidden,
					$amthidden,
					$wthidden,
					$editstatus
				);
				$j++;
			}
		}
		}
		{// While main edit
		if(!empty($creditnodiffarray)) {
			if($j == 0){$j=1;}
			$finalamt = 0;
			$finalamtissue = 0;
			$finalamtreceipt = 0;
			$issuereceiptid = 1;
			$wtissuereceiptid = 1;
			$finalwt = 0;
			$finalwtissue = 0;
			$finalwtreceipt = 0;
			for($k=0;$k<count($creditnodiffarray); $k++) {
				$creditdiffstring .= "'".$creditnodiffarray[$k]."',";
			}
			$creditdiffstring = substr($creditdiffstring, 0, -1);
			// summary amt	
			$salessum = $this->db->query("SELECT  `accountledger`.`creditno`, `accountledger`.`referencenumber`, min(`accountledger`.`salesdate`) as salesdate, `accountledger`.`paymentirtypeid`, `accountledger`.`issuereceiptid`,`accountledger`.`weightissuereceiptid`,`paymentirtype`.`paymentirtypename`, `issuereceipttype`.`issuereceipttypename`,
			ROUND(COALESCE(SUM(accountledger.amtissue),0), '".$amountround."') as amtissue, 
			ROUND(COALESCE(SUM(accountledger.amtreceipt),0), '".$amountround."') as amtreceipt,
			ROUND(COALESCE(SUM(accountledger.weightissue),0), '".$weightround."') as weightissue,	
			ROUND(COALESCE(SUM(accountledger.weightreceipt),0), '".$weightround."') as weightreceipt 
			FROM accountledger LEFT OUTER JOIN `paymentirtype` ON `paymentirtype`.`paymentirtypeid`=`accountledger`.`paymentirtypeid` LEFT OUTER JOIN `issuereceipttype` ON `issuereceipttype`.`issuereceipttypeid`=`accountledger`.`issuereceiptid` WHERE accountledger.status=1 AND accountledger.accountid = '".$accountid."' AND accountledger.creditno IN (".$creditdiffstring.") AND accountledger.salesid != '".$generatesalesid."' AND accountledger.entrymode IN (2,3,6,7,8) GROUP BY `accountledger`.`creditno`");
			if($salessum->num_rows() > 0) 
			{
				foreach($salessum->result() as $value) {
				
					$finalamtissue = 0;
					$finalamtreceipt = 0;
					$finalweightissue = 0;
					$finalweightreceipt = 0;
					$finalamt = abs($value->amtissue - $value->amtreceipt);
					$finalwt = abs($value->weightissue - $value->weightreceipt);
					$amthidden = number_format((float)$finalamt, $amountround, '.', '');
					$wthidden = number_format((float)$finalwt, $weightround, '.', '');
					
					if($value->issuereceiptid == 2){
						$finalamtissue = $finalamt;
					}else if($value->issuereceiptid == 3){
						$finalamtreceipt = $finalamt;
					}
					if($value->weightissuereceiptid == 2){
						$finalweightissue = $finalwt;
					}else if($value->weightissuereceiptid == 3){
						$finalweightreceipt = $finalwt;
					}
					
					$estimatedetail->rows[$j]['id'] = 1;
					$estimatedetail->rows[$j]['cell']=array(
					$value->creditno,
					$this->Basefunctions->ymd_format($value->salesdate),
					$value->paymentirtypename,
					$value->issuereceipttypename,
					$value->amtissue,
					$value->amtreceipt,
					$finalamtissue,
					$finalamtreceipt,
					$value->issuereceiptid,
					$value->paymentirtypeid,
					$value->issuereceipttypename,
					$value->weightissue,
					$value->weightreceipt,
					$finalweightissue,
					$finalweightreceipt,
					$value->weightissuereceiptid,
					1,
					$processid,
					$processname,
					$creditreference,
					$creditreferencename,
					$amthidden,
					$wthidden,
					$amthidden,
					$wthidden,
					$editstatus
					);
					$j++;
				}
			}
		}
	   }
	  if(!isset($estimatedetail)) {
		$estimatedetail = '';
	   }
	   $estimatearray['estimatedata'] = $estimatedetail; 
	   echo json_encode($estimatearray);			
	}
	//old jewel details
	public function detailsofoldjewelmodel() {
		$data = array();
		$stocktype = $_GET['stocktype'];
		$transaction = $_GET['transactionid'];
		$itemdetail = $_GET['itemdetail'];
		$purity = $_GET['purity'];
		$this->db->select('netweightcalculationid,rateless,weightless');
		$this->db->from('oldjeweldetails');
		$this->db->where('oldjeweldetails.salestransactiontypeid',$transaction);
		$this->db->where('oldjeweldetails.olditemdetailsid',$itemdetail);
		$this->db->where('oldjeweldetails.purityid',$purity);
		$this->db->where('oldjeweldetails.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('grosswt'=>$row->netweightcalculationid,'rateless'=>$row->rateless,'weight'=>$row->weightless);
			}
		}else{
			$data = array('grosswt'=>0,'rateless'=>0,'weight'=>0);
		}
		echo json_encode($data);
	}
	//load bill number based on the account
	public function loadbillnumbermodel() {
		$accountid = $_POST['accountid'];
		if($_POST['salesid'] != '' ) {
			$salesid = $_POST['salesid'];
		}else{
			$salesid = 0;
		}
		$i = 0;
		$data = array();
		$this->db->select('salesid,salesnumber,sales.accountid,account.accountname,account.mobilenumber');
		$this->db->from('sales');
		$this->db->join('account','account.accountid=sales.accountid');
		$this->db->where('sales.accountid',$accountid);
		$this->db->where('sales.salestransactiontypeid',11);
		$this->db->where('sales.printtemplatesid !=',16);
		$this->db->where('sales.status',1);
		$this->db->where('sales.salesid !=',$salesid);
		$this->db->where_not_in('sales.salesnumber',array('pending'));
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('id'=>$row->salesid,'name'=>$row->salesnumber,'accountid'=>$row->accountid);
				$i++;
			}
		}
		echo json_encode($data);
	}
	//stone detail insert based on the operation
	public function stonedetailsinsert($salesid,$olddetailsid,$newdeialid) {
		$estimatestonedetail = $this->estimationstonedetailscount($olddetailsid);
		for($i=0;$i<count($estimatestonedetail);$i++) {
			$this->db->query('insert into salesstoneentry (`salesstoneentryorder`, `salesid`, `salesdetailid`, `stoneid`, `stonetypeid`, `purchaserate`, `stonerate`, `pieces`, `caratweight`, `totalcaratweight`, `totalnetweight`, `totalamount`, `createdate`, `lastupdatedate`, `createuserid`, `lastupdateuserid`, `status`) select `salesstoneentryorder`, `salesid`, `salesdetailid`, `stoneid`, `stonetypeid`, `purchaserate`, `stonerate`, `pieces`, `caratweight`, `totalcaratweight`, `totalnetweight`, `totalamount`, `createdate`, `lastupdatedate`, `createuserid`, `lastupdateuserid`, `status` from salesstoneentry where salesstoneentry.stoneid in ('.$estimatestonedetail[$i].') and salesstoneentry.salesdetailid in ('.$olddetailsid.')');
			$newstoneid = $this->db->insert_id();
			$uarray = array('salesid'=>$salesid,'salesdetailid'=>$newdeialid);
			$defaray = $this->Crudmodel->defaultvalueget();
			$updaatearray = array_merge($uarray,$defaray);
			$this->db->where('salesstoneentry.salesstoneentryid',$newstoneid);
			$this->db->update('salesstoneentry',$updaatearray);
		}
	}
	//sales summary update mainupdate
	public function salessummaryupdate($salesid,$salestransactiontypeid,$stateid) {
		$taxamount=0;
		$discountamount=0;
		$totalamount=0;
		$oldjewelamount = 0;
		$billamount= 0;
		$pendingamount=0;
		$netamount = 0;
		$ordertotaladvance = 0;
		$salescgst = 0.00;
		$salessgst = 0.00;
		$salesigst = 0.00;
		$weightround =  $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$amountround =  $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$salessummary = array(
				'summarygrossweight'=>0,
				'summarystoneweight'=>0,
				'summarynetweight'=>0,
				'summarypieces'=>0,
				'summarydiacaratweight'=>0,
				'summarydiapieces'=>0,
				'summarystonecaratweight'=>0,
				'summarystonepieces'=>0,
				'summarywastage'=>0,
				'summarymaking'=>0,
				'summaryflatcharge'=>0,
				'summaryrepaircharge'=>0,
				'summarychargesamount'=>0,
				'summarystoneamount'=>0,
				'summarygrossamount'=>0,
				'summarytaxamount'=>0,
				'summarydiscountamount'=>0,
				'summaryoldjewelamount'=>0,
				'summarysalesretunamount'=>0,
				'summarypurchaseretunamount'=>0,
				'summarypaidamount'=>0,
				'summarypendingamount'=>0,
				'summarypurewt'=>0,
				'summarypaidpurewt'=>0,
				'summarypendingpurewt'=>0,
				'summarypuretoamt'=>0,
				'summaryamttopure'=>0,
				'summarychitamount'=>0,
				'salescgst'=>0.00,
				'salessgst'=>0.00,
				'salesigst'=>0.00
		);
		
		{ // pre sales saved data
		$array['presumvalues'] = array(
				'presumtax'=>0,
				'presumdiscount'=>0,
				'prenetamount'=>0,
				'taxtypeid'=>1,
				'discounttypeid'=>1,
				'salescgst'=>0.00,
				'salessgst'=>0.00,
				'salesigst'=>0.00,
				'summaryroundwholevalue'=>0.00
		);	
		$predata =	$this->db->query(' SELECT  taxtype,discounttype,presumtaxamount,presumdiscountamount,presumnetamount,salescgst,salessgst,salesigst,summaryroundwholevalue FROM sales where sales.status=1 and sales.salesid = '.$salesid.' GROUP BY `sales`.`salesid`');
		if($predata->num_rows() > 0) {
			foreach($predata->result() as $preinfo) {
				$array['presumvalues'] = array(
						'presumtax'=>$preinfo->presumtaxamount,
						'presumdiscount'=>$preinfo->presumdiscountamount,
						'prenetamount'=>$preinfo->presumnetamount,
						'taxtypeid'=>$preinfo->taxtype,
						'discounttypeid'=>$preinfo->discounttype,
						'salescgst'=>$preinfo->salescgst,
						'salessgst'=>$preinfo->salessgst,
						'salesigst'=>$preinfo->salesigst,
						'summaryroundwholevalue'=>$preinfo->summaryroundwholevalue
				);
			}
		}	
		}
		
			$salesdetailwtamtnotin = "salesdetail.stocktypeid not in (19,20,26,27,28,29,31,37,38,39,49,79,84)";
			$salesselect = 'ROUND( +COALESCE(SUM(CASE WHEN '.$salesdetailwtamtnotin.' then salesdetail.grossweight end),0),roundweight) as grossweight,
			ROUND( +COALESCE(SUM(CASE WHEN '.$salesdetailwtamtnotin.' then salesdetail.stoneweight end),0),roundweight) as stoneweight,
			ROUND( +COALESCE(SUM(CASE WHEN '.$salesdetailwtamtnotin.' then salesdetail.netweight end),0),roundweight) as netweight,
			ROUND( +COALESCE(SUM(CASE WHEN '.$salesdetailwtamtnotin.' then salesdetail.pieces end),0),roundamount) as pieces,
			ROUND( +COALESCE(SUM(CASE WHEN '.$salesdetailwtamtnotin.' then salesdetail.diacaratweight end),0),2) as diacaratweight,
			ROUND( +COALESCE(SUM(CASE WHEN '.$salesdetailwtamtnotin.' then salesdetail.diapieces end),0),0) as diapieces,
			ROUND( +COALESCE(SUM(CASE WHEN '.$salesdetailwtamtnotin.' then salesdetail.stonecaratweight end),0),2) as stonecaratweight,
			ROUND( +COALESCE(SUM(CASE WHEN '.$salesdetailwtamtnotin.' then salesdetail.stonepieces end),0),0) as stonepieces,
			ROUND( +COALESCE(SUM(CASE WHEN '.$salesdetailwtamtnotin.' then salesdetail.wastage end),0),roundamount) as wastage,
			ROUND( +COALESCE(SUM(CASE WHEN '.$salesdetailwtamtnotin.' then salesdetail.making end),0),roundamount) as making,
			ROUND( +COALESCE(SUM(CASE WHEN '.$salesdetailwtamtnotin.' then salesdetail.flatcharge end),0),roundamount) as flatcharge,
			ROUND( +COALESCE(SUM(CASE WHEN '.$salesdetailwtamtnotin.' then salesdetail.chargesamount end),0),roundamount) as chargesamount,
			ROUND( +COALESCE(SUM(CASE WHEN '.$salesdetailwtamtnotin.' then salesdetail.stoneamount end),0),roundamount) as stoneamount,
			ROUND( +COALESCE(SUM(CASE WHEN '.$salesdetailwtamtnotin.' then salesdetail.grossamount end),0),roundamount) as grossamount,
			ROUND( +COALESCE(SUM(CASE WHEN '.$salesdetailwtamtnotin.' then salesdetail.taxamount end),0),roundamount) as taxamount,
			ROUND( +COALESCE(SUM(CASE WHEN '.$salesdetailwtamtnotin.' then salesdetail.discountamount end),0),roundamount) as discountamount,
			ROUND( +COALESCE(SUM(CASE WHEN '.$salesdetailwtamtnotin.' then salesdetail.totalamount end),0),roundamount) as totalamount,
			ROUND( +COALESCE(SUM(CASE WHEN '.$salesdetailwtamtnotin.' then salesdetail.repaircharge end),0),roundamount) as repaircharge,

			ROUND( +COALESCE(SUM(CASE WHEN '.$salesdetailwtamtnotin.' then salesdetail.cgst end),0),roundamount) as summarycgst,
			ROUND( +COALESCE(SUM(CASE WHEN '.$salesdetailwtamtnotin.' then salesdetail.sgst end),0),roundamount) as summarysgst,
			ROUND( +COALESCE(SUM(CASE WHEN '.$salesdetailwtamtnotin.' then salesdetail.igst end),0),roundamount) as summaryigst,
			ROUND( +COALESCE(SUM(CASE WHEN '.$salesdetailwtamtnotin.' then salesdetail.pureweight end),0),roundweight) as summarypurewt,
			ROUND( +COALESCE(SUM(CASE WHEN salesdetail.stocktypeid in (19,26,27,28,29,31,39,79) and issuereceipttypeid = 3 and sales.salestransactiontypeid = 20 then salesdetail.totalamount WHEN salesdetail.stocktypeid in (19) and sales.salestransactiontypeid = 20  THEN salesdetail.totalamount  end),0),roundamount) as ordertotaladvance,
			ROUND( +COALESCE(SUM(CASE WHEN salesdetail.stocktypeid in (19) then salesdetail.totalamount end),0),roundamount) as oldjewelamount,
			ROUND( +COALESCE(SUM(CASE WHEN salesdetail.stocktypeid in (20) then salesdetail.totalamount end),0),roundamount) as salesreturnamt,
			ROUND( +COALESCE(SUM(CASE WHEN salesdetail.stocktypeid in (73) then salesdetail.totalamount end),0),roundamount) as purchasereturnamt,
			ROUND( +COALESCE(SUM(CASE WHEN salesdetail.stocktypeid in (26,27,28,29,31,39,79) and issuereceipttypeid = 3 then salesdetail.totalamount end),0),roundamount) as paidamount,
			ROUND( +COALESCE(SUM(CASE WHEN salesdetail.stocktypeid in (26,27,28,29,31,39,79) and issuereceipttypeid = 2 then salesdetail.totalamount end),0),roundamount) as issueamount,
			ROUND( +COALESCE(SUM(CASE WHEN salesdetail.stocktypeid in (19) then salesdetail.pureweight end),0),roundweight) as summaryoldpurewt,
			ROUND( +COALESCE(SUM(CASE WHEN salesdetail.stocktypeid in (20) then salesdetail.pureweight end),0),roundweight) as summarysalesreturnpurewt,
			ROUND( +COALESCE(SUM(CASE WHEN salesdetail.stocktypeid in (20) then salesdetail.pureweight end),0),roundweight) as summarypurchasereturnpurewt,
			ROUND( +COALESCE(SUM(CASE WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid = 3 then salesdetail.pureweight end),0),roundweight) as summarypaidpurewt,
			ROUND( +COALESCE(SUM(CASE WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid = 2 then salesdetail.pureweight end),0),roundweight) as summaryissuepurewt,
			ROUND( +COALESCE(SUM(CASE WHEN salesdetail.stocktypeid in (38) then salesdetail.pureweight end),0),roundweight) as summarypurewtptoa,
			ROUND( +COALESCE(SUM(CASE WHEN salesdetail.stocktypeid in (37) then salesdetail.pureweight end),0),roundweight) as summarypurewtatop,
			ROUND( +COALESCE(SUM(CASE WHEN salesdetail.stocktypeid in (38) then salesdetail.totalamount end),0),roundamount) as summarypurewttoamt,
			ROUND( +COALESCE(SUM(CASE WHEN salesdetail.stocktypeid in (37) then salesdetail.totalamount end),0),roundamount) as summaryamttopure,
			ROUND( +COALESCE(SUM(CASE WHEN salesdetail.stocktypeid in (39) then salesdetail.totalamount end),0),roundamount) as summarychitamount,sales.summarynetamount,sales.summarynetpurewt,sales.summarynetamountwithsign,sales.summarynetpurewtwithsign,sales.accountid,sales.branchid';
			$salesselect = str_replace("roundamount",$amountround,$salesselect);
			$salesselect = str_replace("roundweight",$weightround,$salesselect);
			
			$salessum = $this->db->query(' SELECT  '.$salesselect.' FROM sales JOIN salesdetail on salesdetail.salesid=sales.salesid WHERE sales.status=1 AND salesdetail.status = 1 AND sales.salesid = '.$salesid.' GROUP BY `sales`.`salesid`');
			if($salessum->num_rows() > 0) {
			foreach($salessum->result() as $info) 
			{
				$branchstateid = $this->Basefunctions->branchstateid; 
				$pendingamount = number_format((float)$info->summarynetamountwithsign+$info->summarypurewttoamt-$info->paidamount+$info->issueamount-$info->summaryamttopure, $amountround, '.', ''); 
				$pendingpurewt = number_format((float)$info->summarynetpurewtwithsign+$info->summarypurewtatop-$info->summarypaidpurewt+$info->summaryissuepurewt-$info->summarypurewttoamt, $weightround, '.', ''); 
				$orderadvancesamount = number_format((float)$info->ordertotaladvance-$info->issueamount, $amountround, '.', ''); 
				
				{//tax and discount
					if($array['presumvalues']['taxtypeid'] == 1) { // item wise tax
						$taxamount = $info->taxamount;
						if($stateid == $branchstateid){
							$salescgst = $info->summarycgst;
							$salessgst = $info->summarysgst;
							$salesigst = 0;
						}else
						{
							$salescgst = 0;
							$salessgst = 0;
							$salesigst = $info->summaryigst;
						}
						
					} else { // bill wise tax
						$taxamount = $array['presumvalues']['presumtax'];
						$salescgst = $array['presumvalues']['salescgst'];
						$salessgst = $array['presumvalues']['salessgst'];
						$salesigst = $array['presumvalues']['salesigst'];
					}
				
					if($array['presumvalues']['discounttypeid'] == 2) {// item wise discount
						$discountamount = $info->discountamount;
					} else {// bill wise discount
						$discountamount = $array['presumvalues']['presumdiscount'];
					}
				}
					
				$salessummary = array(
					'summarygrossweight'=>$info->grossweight,
					'summarystoneweight'=>$info->stoneweight,
					'summarynetweight'=>$info->netweight,
					'summarypieces'=>$info->pieces,
					'summarydiacaratweight'=>$info->diacaratweight,
					'summarydiapieces'=>$info->diapieces,
					'summarystonecaratweight'=>$info->stonecaratweight,
					'summarystonepieces'=>$info->stonepieces,
					'summarywastage'=>$info->wastage,
					'summarymaking'=>$info->making,
					'summaryflatcharge'=>$info->flatcharge,
					'summaryrepaircharge'=>$info->repaircharge,
					'summarychargesamount'=>$info->chargesamount,
					'summarystoneamount'=>$info->stoneamount,
					'summarygrossamount'=>$info->grossamount,
					'summarytaxamount'=>$taxamount,
					'summarydiscountamount'=>$discountamount,
					'summaryoldjewelamount'=>$info->oldjewelamount,
					'summarysalesretunamount'=>$info->salesreturnamt,
					'summarypurchaseretunamount'=>$info->purchasereturnamt,
					'summarypaidamount'=>$info->paidamount,
					'summaryissueamount'=>$info->issueamount,
					'summarypendingamount'=>$pendingamount,
					'summarypaidpurewt'=>$info->summarypaidpurewt,
					'summaryissuepurewt'=>$info->summaryissuepurewt,
					'summarypendingpurewt'=>$pendingpurewt,
					'summarypurewt'=>$info->summarypurewt,
					'summarynetpurewt'=>$info->summarypurewt,
					'summarypuretoamt'=>$info->summarypurewttoamt,
					'summaryamttopure'=>$info->summaryamttopure, 
					'summaryoldpurewt'=>$info->summaryoldpurewt,
					'summarysalesreturnpurewt'=>$info->summarysalesreturnpurewt,
					'summarypurchasereturnpurewt'=>$info->summarypurchasereturnpurewt,
					'summarypurewtptoa'=>$info->summarypurewtptoa,
					'summarypurewtatop'=>$info->summarypurewtatop,
					'summarychitamount'=>$info->summarychitamount,
					'salescgst'=>$salescgst,
					'salessgst'=>$salessgst,
					'salesigst'=>$salesigst,
					'orderadvanceamt'=>$orderadvancesamount
				);
			
			}
		}
		$loclingtime = $this->Basefunctions->get_company_settings('lockingtime');
		$currenttime = time("H:i:s");
		$ctime = new DateTime();
		$ctime->setTimestamp($currenttime);
		$ltime  = new DateTime();
		if($loclingtime == '00:00:00') {
			$ltime->setTimestamp($currenttime);
		} else {
			$ltime->setTimestamp(strtotime($loclingtime));
		}
		if($ctime > $ltime) {
			$sales_summary = array_merge($salessummary,$this->Crudmodel->nextdayupdatedefaultvalueget()); 
		} else {
			$sales_summary = array_merge($salessummary,$this->Crudmodel->updatedefaultvalueget()); 
		}
		$this->db->where('salesid',$salesid);
		$this->db->update('sales',$sales_summary);
	}
	public function stocktypeset() {
		$modeid = $_POST['modeid'];
		$transactiontype = $_POST['transactiontype'];
		$this->db->select('stocktypeid');
		$this->db->from('transactionmanage');
		$this->db->where('salestransactiontypeid',$transactiontype);
		$this->db->where('salesmodeid',$modeid);
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			$stocktype = $result->row()->stocktypeid;
		}else{
			$stocktype = '';
		}
		echo json_encode($stocktype);
	}
	//pending product name fetch
	public function pendingproductnamefetchmodel() {
		$purity = $_POST['purity'];
		$data = array();
		$i=0;
		$industryid = $this->Basefunctions->industryid;
		$info =$this->db->query("SELECT `product`.`productname`, `product`.`productid`, `product`.`categoryid`, `category`.`categoryid`, `category`.`categoryname`, `category`.`categorylevel`, `product`.`counterid` as counterid, `product`.`bullionapplicable`, `product`.`taxable`, group_concat(`tax`.`taxname`, '-',`tax`.`taxid`) as taxid,group_concat(distinct(`tax`.`taxid`)) as taxdataid, `product`.`purityid`,`product`.`chargeid`,`product`.`purchasechargeid` FROM `product` JOIN `category` ON `category`.`categoryid` = `product`.`categoryid` JOIN `taxmaster` ON `taxmaster`.`taxmasterid` = `product`.`taxmasterid` LEFT JOIN `tax` ON `tax`.`taxmasterid` = `taxmaster`.`taxmasterid` JOIN `charge` ON `charge`.`chargeid` = `product`.`chargeid` JOIN `purchasecharge` ON `purchasecharge`.`purchasechargeid` = `product`.`purchasechargeid` WHERE `product`.`status` = 1 and find_in_set(".$purity.",`product`.`purityid`) > 0 and `product`.`industryid` = ".$industryid." and `product`.`productid` != 2 and `category`.`categoryid` != 2  GROUP BY `product`.`productid` ORDER BY `product`.`productid` DESC");
		foreach($info->result() as $row) {
			$data[$i] = array('productid'=>$row->productid,'productname'=>$row->productname);
			$i++;
		}
		echo json_encode($data);
	}
	//simple drop down value fetch
	public function getrate() {
		$rateround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //weight round-off
		$tablename = 'rate';
		$fieldname =  'rateid,ROUND(currentrate,'.$rateround.') as currentrate,purityid';
		$order = 'rateid';
		$i=0;
		$data = array();
		$industryid = $this->Basefunctions->industryid;
		$branchid = $this->Basefunctions->branchid;
		$this->db->select($fieldname);
		$this->db->from($tablename);
		$this->db->where_not_in('status',array(0,3));
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->where('rate.branchid',$branchid);
		$this->db->where('rate.currentstatus',1);
		$query= $this->db->get();
		foreach($query->result() as $row){
			$data[$row->purityid] = $row->currentrate;
			$i++;
		}
		echo json_encode($data);
	}
	//default account for bank and card
	public function defaccountdropdownwithcond($did,$dname,$table,$whfield,$whdata) {
		$i=0;
		$industryid = $this->Basefunctions->industryid;
		$mvalu=explode(",",$whdata);
		$this->db->select("$dname,$did,accounttypeid");
		$this->db->from($table);
		if($whdata != '') {
			if(count($mvalu)>=2) {
				$this->db->where_in($whfield,$mvalu);
			} else {
				$this->db->where("FIND_IN_SET('$whdata',$whfield) >", 0);
			}
		}
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->where('status',1);
		$this->db->order_by($dname,"asc");
		$result = $this->db->get();
		return $result->result();
	}
	public function accountgroup_dd(){
		$i=0;
		$allowedaccounttypeid = '6,16';
		$allowedaccounttypeset = '0';
		$data = array();
		$chitbookno = '';
		if(isset($_POST['allowedaccounttypeid'])){
			$allowedaccounttypeid = $_POST['allowedaccounttypeid'];
		}
		if(isset($_POST['allowedaccounttypeset'])){
			$allowedaccounttypeset = $_POST['allowedaccounttypeset'];
		}
		if(isset($_POST['q'])){
			$filter =$_POST['q'];
			if(substr($_POST['q'],0,1) == '.'){
				$check = 'chitbookno';
				$filter = substr($_POST['q'],1);
			}else if(is_numeric($_POST['q'])){
				$check = 'mobilenumber';
			} else{
				$check = 'accountname';
			}
			
			if(isset($_POST['acid'])){
				$acid =$_POST['acid'];
			} else{
				$acid ='1';
			}
			$transactiontype =$_POST['type'];
			if($allowedaccounttypeset == 0 || $allowedaccounttypeid == ''){
				if($transactiontype == '9'  || $transactiontype == '21' || $transactiontype == '19') {
					$type = '16';
				}else if($transactiontype == '22' || $transactiontype == '23') {
					$type = '16,6,18,19';
				}
				else if($transactiontype == '18' || $transactiontype == '20') {
					$type = '16,6';
				}// maddy
				else if($transactiontype == '25') {
					$type = '18,19';
				}else if($transactiontype == '24') {
					$type = '16,6';
				}
				else {
					$type = '6';
				}
			}else {
				$type = $allowedaccounttypeid;
			}
			if($acid != 1){
				$query = $this->db->query(
				"SELECT account.stateid,account.accountname,account.accountid,accounttype.accounttypename,account.accounttypeid,account.accounttaxstatus,account.mobilenumber,accounttype.shortname
					FROM account
					JOIN accounttype ON accounttype.accounttypeid = account.accounttypeid
					WHERE account.status =1 and account.accountid in (".$acid.") and account.accounttypeid in (".$type.")
				");
			} else {
				if($check == 'accountname'){
					$query = $this->db->query(
					"SELECT account.stateid,account.accountname,account.accountid,accounttype.accounttypename,account.accounttypeid,account.accounttaxstatus,account.mobilenumber,accountcf.primaryarea,accounttype.shortname
					FROM account
					JOIN accounttype ON accounttype.accounttypeid = account.accounttypeid
					LEFT OUTER JOIN accountcf ON accountcf.accountid = account.accountid
					WHERE account.status =1 and account.accounttypeid in (".$type.") and account.accountname like '%".$filter."%' 
					");
				} else if($check == 'chitbookno'){
					$query = $this->db->query(
					"SELECT account.stateid,account.accountname,account.accountid,accounttype.accounttypename,account.accounttypeid,account.accounttaxstatus,account.mobilenumber,accountcf.primaryarea,chitbook.chitbookno,accounttype.shortname
					FROM account
					JOIN accounttype ON accounttype.accounttypeid = account.accounttypeid
					JOIN chitbook ON chitbook.accountid = account.accountid
					LEFT OUTER JOIN accountcf ON accountcf.accountid = account.accountid
					WHERE account.status =1 and account.accounttypeid in (".$type.") and chitbook.chitbookno like '%".$filter."%' and chitbook.status in (1)
					");
				}else{
					$query = $this->db->query(
					"SELECT account.stateid,account.accountname,account.accountid,accounttype.accounttypename,account.accounttypeid,account.accounttaxstatus,account.mobilenumber,accountcf.primaryarea,accounttype.shortname
					FROM account
					JOIN accounttype ON accounttype.accounttypeid = account.accounttypeid
					LEFT OUTER JOIN accountcf ON accountcf.accountid = account.accountid
					WHERE account.status =1 and account.accounttypeid in (".$type.") and account.mobilenumber like '%".$filter."%'
					");
				}
			}
			/* if($query->num_rows() > 0) { */
				foreach($query->result() as $row){
					 if($row->primaryarea != null){
						$primary = $row->primaryarea; 
					 } else {
						 $primary ='empty'; 
					 }
					 if(isset($row->chitbookno)) {
						 $chitbookno = $row->chitbookno;
					 }
					$data[$i] = array('id'=>$row->accountid,'text'=>$row->accountname,'accountname'=>$row->accountname,'accountid'=>$row->accountid,'accounttypename'=>$row->accounttypename,'accounttaxstatus'=>$row->accounttaxstatus,'mobilenumber'=>$row->mobilenumber,'primaryarea'=>$primary,'chitbookno'=>$chitbookno,'stateid'=>$row->stateid,'accounttypeid'=>$row->accounttypeid,'acctypeshortname'=>$row->shortname,'ddcreate'=>'no');
					$i++;
				}
			/* } else {
				$data[$i] = array('id'=>'','text'=>$filter,'accountname'=>$filter,'accountid'=>'','accounttypename'=>'','accounttaxstatus'=>'','mobilenumber'=>'','primaryarea'=>'','chitbookno'=>'','stateid'=>'','accounttypeid'=>'','acctypeshortname'=>'','ddcreate'=>'yes');
			} */
		} 
		echo json_encode($data);
	}
	public function getpurchaseproduct(){
		$this->db->select('GROUP_CONCAT(product.productid) AS productid');
		$this->db->join('category','category.categoryid = product.categoryid');
		$this->db->where('category.categoryid',2);
		$this->db->where('category.status',$this->Basefunctions->activestatus);
		$this->db->where('product.status',$this->Basefunctions->activestatus);
		$result=$this->db->get('product')->result();
		foreach($result as $info){
			$data = $info->productid.',';
		}
		$data = rtrim($data,',');
		return $data;
	}
	//cash counter drop down load
	public function cashcounterdropdownload() {
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('cashcountername,cashcounterid,parentcashcounterid');
		$this->db->from('cashcounter');
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->where('status',$this->Basefunctions->activestatus);		
		$this->db->order_by('cashcounterid','asc');		
		$info = $this->db->get();
		return $info;
	}
	//show hide fields based on the comapny setting
	public function showhidecommonfieldsmodel() {
		$data = '';
		$fdata = array();
		$fexpdata = array();
		$transactiontype = $_GET['transactiontype'];
		$transactionmanageid = $_GET['transactionmanageid'];
		$allowedaccounttypeset = $_GET['allowedaccounttypeset'];
		$modeid = $_GET['modeid'];
		$this->db->select('salesfieldsid,stocktypeid');
		$this->db->from('transactionmanage');
		$this->db->where('salestransactiontypeid',$transactiontype);
		if($allowedaccounttypeset == 0){
			$this->db->where('salesmodeid',$modeid);
		} else if($allowedaccounttypeset == 1 && $transactionmanageid > 1 ) {
			$this->db->where('transactionmanageid',$transactionmanageid);
		}
		$this->db->where('status',1);
		$result = $this->db->get();
		foreach($result->result() as $info){
			$data = $info->salesfieldsid;
			$fexpdata['default'] = $info->stocktypeid;
		}
		if($data != "") {
			$expdata = explode(',',$data);
			for($i=0;$i<count($expdata);$i++){
				$this->db->select('salesfieldsdivid');
				$this->db->from('salesfields');
				$this->db->where('salesfieldsid',$expdata[$i]);
				$this->db->where('status',1);
				$fresult = $this->db->get();
				foreach($fresult->result() as $finfo){
					$fdata[$i] = $finfo->salesfieldsdivid;
				}
			}
			$fexpdata['hidden'] = implode(',',$fdata);
		}else{
			$fexpdata['hidden'] = '';
		}
		$fexpdata['cashcounter'] = '1';
		$salespersonid = $_GET['salespersonid'];
		$this->db->select('cashcounterid');
		$this->db->from('cashcounter');
		$this->db->where('cashcounter.employeeid',$salespersonid);
		$this->db->where('cashcounter.status',1);
		$this->db->limit(1);
		$result = $this->db->get();
		if($result->num_rows() > 0) { 
			foreach($result->result() as $row) {
				$fexpdata['cashcounter'] = $row->cashcounterid;
			}
		}
		$i=0;
		$employeeid = $_GET['employeeid'];
		$userroleid = $this->Basefunctions->generalinformaion('employee','userroleid','employeeid',$employeeid);
		$discountmode = $_GET['discountmode'];
		$discaltype = $_GET['discaltype'];
		if($discaltype == 5) {
			$discaltype = 4;
		}
		$this->db->select('discounttypeid,discountvalue,metalid,purityid,categoryid');
		$this->db->from('discountauthorization');
		$this->db->where_in('discountauthorization.discountmodeid',$discountmode);
		$this->db->where_in('discountauthorization.discounttypeid',$discaltype);
		$this->db->where_in('discountauthorization.status',array(1));
		$this->db->where("FIND_IN_SET('".$userroleid."',userroleid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result()as $row) {
				$fexpdata['discount'] = array('discounttypeid'=>$row->discounttypeid,'discountvalue'=>$row->discountvalue,'metalid'=>$row->metalid,'purityid'=>$row->purityid,'categoryid'=>$row->categoryid);
				$fexpdata['discountarray'][$i] = array('discounttypeid'=>$row->discounttypeid,'discountvalue'=>$row->discountvalue,'metalid'=>$row->metalid,'purityid'=>$row->purityid,'categoryid'=>$row->categoryid);
				$i++;
			}
			//$fexpdata['discount'] = $discountdata;
		} else {
			$fexpdata['discount'] = array('discounttypeid'=>1,'discountvalue'=>0,'metalid'=>1,'purityid'=>1,'categoryid'=>1);
		}
		// Sales Field comment Required
		$fexpdata['salesfieldcommentreq'] = $this->Basefunctions->singlefieldfetch('salesfieldcommentrequired','transactionmanageid','transactionmanage',$transactionmanageid);
		echo json_encode($fexpdata);
	}
	
	public function chequeestimationnumberstatus($estimationnumber) {
		$data = 0;
		$this->db->select('salesid');
		$this->db->from('sales');
		$this->db->where('sales.status',1);
		$this->db->where('sales.salestransactiontypeid',16);
		$this->db->where('sales.salesnumber',$estimationnumber);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data = $row->salesid;
			}
		}
		return $data;
	}
	//estimation details count fetch
	public function estimationdetailscount($estimationid) {
		$estimationdetailid = array();
		$estimatecount = 0;
		$estimateoldcount = 0;
		$i=0;
		$this->db->select('salesdetail.salesdetailid');
		$this->db->from('salesdetail');
		$this->db->join('itemtag','itemtag.itemtagid=salesdetail.itemtagid');
		$this->db->where('salesdetail.salesid',$estimationid);
		$this->db->where('salesdetail.status',1);
		$this->db->where('itemtag.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$estimationdetailid[$i] = $row->salesdetailid;
				$i++;
			}
			$estimatecount = 1;
		}else {
			$estimatecount = 0;
		}
		$estimationolddetailid = array();
		$j=0;
		$this->db->select('salesdetail.salesdetailid');
		$this->db->from('salesdetail');
		$this->db->where('salesdetail.itemtagid',1);
		$this->db->where('salesdetail.salesid',$estimationid);
		$this->db->where('salesdetail.status',1);
		$resultold = $this->db->get();
		if($resultold->num_rows() > 0){
			foreach($resultold->result() as $rowold) {
				$estimationolddetailid[$j] = $rowold->salesdetailid;
				$j++;
			}
			$estimateoldcount = 1;
		}else {
			$estimateoldcount = 0;
		}
		if($estimatecount == 0 && $estimateoldcount == 0) {
			return 0;
		}else {
			$finalsalesdetailid = array();
			$finalsalesdetailid = array_merge($estimationdetailid,$estimationolddetailid);
			return $finalsalesdetailid;
	  }
	}
	//estimation stone details count fetch
	public function estimationstonedetailscount($estimationid) {
		$estimationdetailid = array();
		$estimatecount = 0;
		$estimateoldcount = 0;
		$i=0;
		$this->db->select('salesstoneentry.stoneid');
		$this->db->from('salesstoneentry');
		$this->db->where('salesstoneentry.salesdetailid',$estimationid);
		$this->db->where('salesstoneentry.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$estimationdetailid[$i] = $row->stoneid;
				$i++;
			}
			$estimatecount = 1;
		}else {
			$estimatecount = 0;
		}
		return $estimationdetailid;
	 }
	//estimation details count fetch - order sales check
	public function estimationnewdetailscount($estimationid) {
		$estimationdetailid = array();
		$estimatecount = 0;
		$estimateoldcount = 0;
		$i=0;
		$this->db->select('salesdetail.salesdetailid');
		$this->db->from('salesdetail');
		$this->db->where('salesdetail.salesid',$estimationid);
		$this->db->where_in('salesdetail.stocktypeid',array(11,12,13,20));
		$this->db->where('salesdetail.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$estimationdetailid[$i] = $row->salesdetailid;
				$i++;
			}
			$estimatecount = $i;
		}else {
			$estimatecount = 0;
		}
		return $estimatecount;
	 }

	// pending bill itemtag available
	
	//sales detail insert operation for estimation number
	public function estimationsalesdetailsinsert($salesid,$salesdetailsid,$estimationnumber) {
		$firstdeailid = '1';
		$loclingtime = $this->Basefunctions->get_company_settings('lockingtime');
		$updatingtime = $this->Basefunctions->get_company_settings('updatingtime');
		$currenttime = time("H:i:s");
		$ctime = new DateTime();
		$ctime->setTimestamp($currenttime);
		$ltime  = new DateTime();
		if($loclingtime == '00:00:00') {
			$ltime->setTimestamp($currenttime);
		} else {
			$ltime->setTimestamp(strtotime($loclingtime));
		}
		for($i=0;$i<count($salesdetailsid);$i++) {
			$this->db->query('insert into salesdetail (`salesid`, `stocktypeid`, `itemtagid`, `purityid`, `productid`, `counterid`, `grossweight`, `stoneweight`, `dustweight`, `netweight`, `pureweight`, `pieces`, `grossamount`, `chargesamount`, `wastage`,`making`,`wastagespan`,`makingspan`,`wastagespanlabel`,`makingchargespanlabel`, `stoneamount`, `taxamount`, `discountamount`, `issuereceipttypeid`, `ratepergram`,`wastageless`,`melting`, `secondtouch`, `totalamount`, `comment`, `bankname`, `cardtypeid`, `expiredate`, `approvalcode`, `approvalstatus`, `commission`, `commissionamount`, `referencedate`, `referencenumber`, `orderstatusid`, `ordernumber`, `salesdetailimage`, `orderduedate`, `netwtcalctypeid`, `mccalctypeid`, `taxnames`, `taxvalue`, `individualtaxamt`, `otherdetails`, `otherdetailsvalue`, `ckeyword`, `ckeywordvalue`, `ckeywordoriginalvalue`, `ckeywordcalvalue`, `ckeywordfcalvalue`, `chitbookno`, `chittotalpaid`, `interestcalculate`, `interestamt`, `chitamount`, `olditemdetails`, `modeid`, `calculationtypeid`,`discountpercent`,`giftnumber`,`giftremark`,`giftdenomination`,`giftunit`,`defaultbankid`,`defaultcardid`,`employeeid`,`pendingproductid`,`approvalbillnumber`,`processproductid`,`processcounterid`,`flatcharge`,`flatchargespan`,`rateclone`,`rateaddition`, `createdate`, `lastupdatedate`, `createuserid`, `lastupdateuserid`, `status`) select `salesid`, `stocktypeid`, `itemtagid`, `purityid`, `productid`, `counterid`, `grossweight`, `stoneweight`, `dustweight`, `netweight`, `pureweight`, `pieces`, `grossamount`, `chargesamount`,`wastage`,`making`,`wastagespan`,`makingspan`,`wastagespanlabel`,`makingchargespanlabel`, `stoneamount`, `taxamount`, `discountamount`, `issuereceipttypeid`, `ratepergram`,`wastageless`, `melting`, `secondtouch`, `totalamount`, `comment`, `bankname`, `cardtypeid`, `expiredate`, `approvalcode`, `approvalstatus`, `commission`, `commissionamount`, `referencedate`, `referencenumber`, `orderstatusid`, `ordernumber`, `salesdetailimage`, `orderduedate`, `netwtcalctypeid`, `mccalctypeid`, `taxnames`, `taxvalue`, `individualtaxamt`, `otherdetails`, `otherdetailsvalue`, `ckeyword`, `ckeywordvalue`, `ckeywordoriginalvalue`, `ckeywordcalvalue`, `ckeywordfcalvalue`, `chitbookno`, `chittotalpaid`, `interestcalculate`, `interestamt`, `chitamount`, `olditemdetails`, `modeid`, `calculationtypeid`,`discountpercent`,`giftnumber`,`giftremark`,`giftdenomination`,`giftunit`,`defaultbankid`,`defaultcardid`,`employeeid`,`pendingproductid`,`approvalbillnumber`,`processproductid`,`processcounterid`,`flatcharge`,`flatchargespan`,`rateclone`,`rateaddition`, `createdate`, `lastupdatedate`, `createuserid`, `lastupdateuserid`, `status` from salesdetail where salesdetail.salesdetailid in ('.$salesdetailsid[$i].')');
			$newdeialid = $this->db->insert_id();
			$uarray = array('salesid'=>$salesid);
			if($ctime > $ltime) {
				$defaray = $this->Crudmodel->nextdaydefaultvalueget();
			} else {
				$defaray = $this->Crudmodel->defaultvalueget();
			}
			$updaatearray = array_merge($uarray,$defaray);
			$this->db->where('salesdetail.salesdetailid',$newdeialid);
			$this->db->update('salesdetail',$updaatearray);
			
			$this->db->select('salesdetail.salesdetailid,sales.salestransactiontypeid,salesdetail.stocktypeid,salesdetail.itemtagid,sales.salesid');
			$this->db->from('salesdetail');
			$this->db->join('sales','sales.salesid=salesdetail.salesid');
			$this->db->where_not_in('salesdetail.status',array(0,3));
			$this->db->where_in('salesdetail.salesdetailid',$newdeialid);
			$query= $this->db->get();
			foreach($query->result() as $row){
				 
				if(!in_array($row->salestransactiontypeid,array(16,20,21,18,9))) { // estimate,takeorder,purchase,placeorder type means donot update the tag table
				
					if($row->stocktypeid == 11 OR $row->stocktypeid == 13 OR $row->stocktypeid == 74) //tag
					{
						$update = array(
								'status'=>$this->Basefunctions->soldstatus,
								'salesdetailid'=>$row->salesdetailid,
								'salesid'=>$row->salesid
						);
						if($ctime > $ltime) {
							$update = array_merge($update,$this->Crudmodel->nextdayupdatedefaultvalueget());
						} else {
							$update = array_merge($update,$this->Crudmodel->updatedefaultvalueget());
						}
						$this->db->where('itemtagid',$row->itemtagid);
						$this->db->update('itemtag',$update);
					}
					//stock table update
					$this->Basefunctions->stocktableentryfunction($row->salesdetailid,'52');
				}
			}
			$stonedetails = $this->stonedetailsinsert($salesid,$salesdetailsid[$i],$newdeialid);
			if($i == 0) {
				$firstdeailid = $newdeialid;
			}	
		}
		$estimateinactive = array('status'=>'0');
		$this->db->where_in('salesnumber',$estimationnumber)->where('salestransactiontypeid',16)->update('sales',$estimateinactive);
		return $firstdeailid;
	}
	//simple drop down value fetch with condition
	public function simpledropdownwithcond($did,$dname,$table) {
		$i=0;
		$industryid = $this->Basefunctions->industryid;
		$this->db->select("$dname,$did");
		$this->db->from($table);
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->where('status',1);
		$this->db->where('taxapplytypeid !=',4);
		$this->db->order_by($dname,"asc");
		$result = $this->db->get();
		return $result->result();
	}
	public function bullionproduct_dd() {
		$industryid = $this->Basefunctions->industryid;
		$paymentproducts = $this->Basefunctions->get_company_settings('paymentproducts');
		if($paymentproducts == 0) {
			$bullionproducts = " AND 1=1 ";
		} else if($paymentproducts == 1) {
			$bullionproducts = " AND product.bullionapplicable = 'Yes' ";
		}
		$info =$this->db->query("SELECT `product`.`productname`, `product`.`productid`, `product`.`categoryid`, `category`.`categoryid`, `category`.`categoryname`, `category`.`categorylevel`, `product`.`counterid` as counterid, `product`.`bullionapplicable`, `product`.`taxable`, group_concat(`tax`.`taxname`, '-',`tax`.`taxid`) as taxid,group_concat(distinct(`tax`.`taxid`)) as taxdataid, `product`.`purityid`,`product`.`chargeid`,`product`.`purchasechargeid`,`product`.`stoneapplicable`, `product`.`kacha` FROM `product` JOIN `category` ON `category`.`categoryid` = `product`.`categoryid` JOIN `taxmaster` ON `taxmaster`.`taxmasterid` = `product`.`taxmasterid` LEFT JOIN `tax` ON `tax`.`taxmasterid` = `taxmaster`.`taxmasterid` JOIN `charge` ON `charge`.`chargeid` = `product`.`chargeid` JOIN `purchasecharge` ON `purchasecharge`.`purchasechargeid` = `product`.`purchasechargeid` WHERE `product`.`status` = 1  and `product`.`industryid` = ".$industryid." ".$bullionproducts." GROUP BY `product`.`productid` ORDER BY `product`.`productid` DESC");
		return $info;
	}
	public function getmetaldata($purity) {
		$purity = explode(',',$purity);
		$i=0;
		$data = array();
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('metalid,purityid');
		$this->db->from('purity');
		$this->db->where_not_in('status',array(0,3));
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->where_in('purityid',$purity);
		$query= $this->db->get();
		foreach($query->result() as $row){
			$data[$row->metalid] = $row->purityid;
			$i++;
		}
		return $data;
	}
	public function loadproductfromstock() {
		$type = $_POST['stocktype'];
		$branchid = $_POST['branchid'];
		$stocktype = '';
		$stockreporttype = '';
		if($type == '73'){ // purchase return
			$stocktype = array(13);
			$stockreporttype = array(14);
			$table = 'itemtag';
		}else if($type == '12' || $type == '63' ){        // untag
			$stocktype = array(3);
			$stockreporttype = array(3);
			$table = 'itemtag';
		}
		else if($type == '20' || $type == '24' || $type == '25'){   // sales return
			$stocktype = array(3);
			$table = 'product';
		}
		else if($type == '75'){        // takeorder
			$stocktype = array(3);
			$table = 'product';
		}
		else if($type == '86') { // Take Repair
			$stocktype = array(3);
			$table = 'product';
		}
 		$data = '';
		$this->db->select('GROUP_CONCAT(DISTINCT(productid) SEPARATOR ",") as productid');
		$this->db->from($table);
		if($table == 'itemtag'){
			$this->db->where_in('itemtag.tagtypeid',$stocktype);
		}
		else {
			$this->db->where('product.categoryid >',10);
			$this->db->or_where('product.categoryid',5);
		}
		$this->db->where('branchid',$branchid);
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				if (is_null($row->productid))
				{
					$data = '';
				}
				else{
					$data = $row->productid;
				}
			}
		}else {
			$data = '';
		}
		if($stockreporttype == ''){
			
		}else {
			$this->db->select('GROUP_CONCAT(DISTINCT(productid) SEPARATOR ",") as productid');
			$this->db->from('cummulativestock');
			$this->db->where_in('cummulativestock.stockreporttypeid',$stockreporttype);
			$this->db->where('branchid',$branchid);
			$this->db->where('status',1);
			$result = $this->db->get();
			if($result->num_rows() > 0) {
				foreach($result->result() as $row) {
					if (is_null($row->productid))
					{
						
					}
					else
					{
						$data = $data.','.$row->productid;
					}
				}
			}
		}
		$data = ltrim($data,',');
		$data = array('productid'=>$data);
		echo json_encode($data);
	}
	public function getpurity() {
		$type = $_POST['stocktype'];
		$product = $_POST['product'];
		$branchid = $_POST['branchid'];
		$stocktype = '';
		$stockreporttype = '';
		if($type == '73'){ // purchase return
			$stocktype = array(13);
			$stockreporttype = array(14);
		}else if($type == '12'){        // untag
			$stocktype = array(3);
			$stockreporttype = array(3);
		}
		if($type == '24'){        // approval in
			$purityid = $this->Basefunctions->singlefieldfetch('purityid','productid','product',$product);
			$data = array('purityid'=>$purityid);
		}else
		{
			$data = array();
			$this->db->select('GROUP_CONCAT(DISTINCT(purityid) SEPARATOR ",") as purityid');
			$this->db->from('itemtag');
			$this->db->where_in('itemtag.tagtypeid',$stocktype);
			$this->db->where('branchid',$branchid);
			if($product > 0){
			$this->db->where('itemtag.productid',$product);
			}
			$this->db->where('status',1);
			$result = $this->db->get();
			if($result->num_rows() > 0) {
				foreach($result->result() as $row) {
					if (is_null($row->purityid))
					{
						$data = '';
					}
					else{
						$data = $row->purityid;
					}
				}
			}else {
				$data = '';
			}
		}
		if($stockreporttype == ''){
			
		}else {
			$this->db->select('GROUP_CONCAT(DISTINCT(purityid) SEPARATOR ",") as purityid');
			$this->db->from('cummulativestock');
			$this->db->where_in('cummulativestock.stockreporttypeid',$stockreporttype);
			$this->db->where('branchid',$branchid);
			if($product > 0){
			$this->db->where('cummulativestock.productid',$product);
			}
			$this->db->where('status',1);
			$result = $this->db->get();
			if($result->num_rows() > 0) {
				foreach($result->result() as $row) {
					if (is_null($row->purityid))
					{
						
					}
					else{
						$data = $data.','.$row->purityid; 
					}
				}
			}
		}
		$data = ltrim($data,',');
		$data = array('purityid'=>$data);
		echo json_encode($data);
	}
	public function getcounter() {
		$type = $_POST['stocktypeid'];
		$product = $_POST['product'];
		$purity = $_POST['purity'];
		$branchid = $_POST['branchid'];
		$stocktype = '';
		$stockreporttype = '';
		if($type == '73'){ // purchase return
			$stocktype = 13;
			$stockreporttype = array(14);
		}else if($type == '12'){        // untag
			$stocktype = 3;
			$stockreporttype = array(3);
		}
		if($type == '24'){        // approval in
			$data = array();
			$this->db->select('GROUP_CONCAT(DISTINCT(counterid) SEPARATOR ",") as counterid');
			$this->db->from('counter');
			$this->db->where('counter.counterdefaultid',18);
			$this->db->where('counter.storagetypeid',2);
			$this->db->where('counter.status',1);
			$result = $this->db->get();
			if($result->num_rows() > 0) {
				foreach($result->result() as $row) {
					if(is_null($row->counterid)) {
						$data = '';
					} else{
						$data = $row->counterid;
					}
				}
			} else {
				$data = '';
			}
		} else {
			$data = array();
			$this->db->select('GROUP_CONCAT(DISTINCT(counterid) SEPARATOR ",") as counterid');
			$this->db->from('itemtag');
			$this->db->where_in('itemtag.tagtypeid',$stocktype);
			$this->db->where('branchid',$branchid);
			if($product > 0) {
				$this->db->where('itemtag.productid',$product);				
			}
			if($purity > 0) {
				$this->db->where('itemtag.purityid',$purity);				
			}
			$this->db->where('status',1);
			$result = $this->db->get();
			if($result->num_rows() > 0) {
				foreach($result->result() as $row) {						
					if (is_null($row->counterid)) {
						$data = '';
					} else {
						$data = $row->counterid;
					}
				}				
			} else {
				$data = '';				
			}
			if($stockreporttype == ''){
			
			}else {
				$this->db->select('GROUP_CONCAT(DISTINCT(counterid) SEPARATOR ",") as counterid');				
				$this->db->from('cummulativestock');				
				$this->db->where_in('cummulativestock.stockreporttypeid',$stockreporttype);				
				$this->db->where('branchid',$branchid);				
				if($product > 0){					
					$this->db->where('cummulativestock.productid',$product);				
				}				
				if($purity > 0){					
					$this->db->where('cummulativestock.purityid',$purity);				
				}	
				$this->db->where('status',1);				
				$result = $this->db->get();				
				if($result->num_rows() > 0) {					
					foreach($result->result() as $row) {						
						if (is_null($row->counterid))
						{
							
						}
						else{
							$data = $data.','.$row->counterid;
						}					
					}
				}
			}
		}
		$data = ltrim($data,',');
		$data = array('counterid'=>$data);
		echo json_encode($data);
	}
	public function getweight() {
		$type = $_POST['stocktypeid'];
		$storage = $_POST['counter'];
		$purity = $_POST['purity'];
		$branchid = $_POST['branchid'];
		$productid = $_POST['product'];
		$salesid = $_POST['datarowid'];
		if($type == '73'){        // purchase return
			$whereclauses['stockreporttypeid'] = 14;
		}else if($type == '12' && $type == '63'){  // appouuntag ,untag
			$whereclauses['stockreporttypeid'] = 3;
		} else {
			$whereclauses['stockreporttypeid'] = '';
		}
		
		/*
		$stocktype = '';
		if($type == '73'){        // purchase return
			$stocktype = 14;
		}else if($type == '12'){        // untag
			$stocktype = 3;
		} else {
			$stocktype = $type;
		}
		$data = array();
		$this->db->select('netweight,grossweight,pieces');
		$this->db->from('currentstock');
		//$this->db->where('grossweight >',0);
		//$this->db->where('pieces >',0);
		$this->db->where('stockreporttypeid',$stocktype);
		$this->db->where('counterid',$storage);
		$this->db->where('purityid',$purity);
		$this->db->where('branchid',$branchid);
		$this->db->where('productid',$productid);
		$this->db->where('status',1); */
		$partialtagselectgwt = " COALESCE(sum(itemtag.grossweight),0) ";
		$partialtagselectnwt = " COALESCE(sum(itemtag.netweight),0) ";
		$pagwt = "";
		$panwt = "";
		$supagwt = "";
		$supanwt = "";
		$transfertagwhere = "";
		$startdate='';
		$enddate='';
		$datewh='';
		$filterwherecondtion='';
		$whereString='';
		$grouporder='';
		$join_stmt='';
		$join_stmt='';
		$mode = "3";
		$whereclauses['counterid'] = $storage;
		$whereclauses['branchid'] = $branchid;
		$whereclauses['productid'] = $productid;
		$whereclauses['purityid'] = $purity;
		$whereclauses['salesid'] = $salesid;
		$salesidstr = '';
		$itemtagidstr = '';
		
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$mainquery = $this->Basefunctions->stockreportquerygeneratefunction($startdate,$enddate,$datewh,$filterwherecondtion,$whereString,$grouporder,$join_stmt,$transfertagwhere,$partialtagselectgwt,$partialtagselectnwt,$pagwt,$panwt,$supagwt,$supanwt,$salesidstr,$itemtagidstr,$mode,$whereclauses);
		$result = $this->db->query($mainquery);
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('netweight'=>number_format((float)$row->openingnetweight, $round, '.', ''),'grossweight'=>number_format((float)$row->openinggrossweight, $round, '.', ''),'pieces'=>number_format((float)$row->openingpieces, 0, '.', ''));
			}
		}else {
			$data = '';
		}
		echo json_encode($data);
	}
	public function checkdiscountpassword(){
		$password = $_POST['password'];
		$employeeid = $_POST['employeeid'];
		$database = $this->db->database;
		$user_db = $this->Basefunctions->inlineuserdatabaseactive($database);
		$user_db->select('discountauthorization.dapassword');
		$user_db->from('discountauthorization');
		$user_db->where('employeeid = '."'".$employeeid."'");
		$user_db->limit(1);
		$query = $user_db->get();
		if($query->num_rows() < 1) {
			$loginarray=array('status'=>'Password is invalid');
			echo json_encode($loginarray);
		} else {
			foreach($query->result() as $row) {
				$array1 = array(
						'password' => $row->dapassword
				);
			}
			// check to see whether password matches
			$passwdchk = '';
			$passwdchk = password_verify($password, $array1['password']);
			if($passwdchk!='') {
				if($query->num_rows() > 0) {
					$loginarray=array('status'=>'success');
					echo json_encode($loginarray);
				}
			}else{
				$loginarray=array('status'=>'fail');
				echo json_encode($loginarray);
			}
	 }
  }
  public function getdiscountvalue(){
    $discounttype = $_POST['discounttype'];
    $employeeid = $_POST['employee'];
	$userroleid = $this->Basefunctions->generalinformaion('employee','userroleid','employeeid',$employeeid);
    $salesdiscounttypeid = $_POST['salesdiscounttypeid'];
	if($salesdiscounttypeid == 5) {
		$salesdiscounttypeid = 4;
	}
    $data = array();
    $this->db->select('discountvalue');
    $this->db->from('discountauthorization');
	$this->db->where("FIND_IN_SET('".$userroleid."',userroleid) >", 0);
    $this->db->where('discounttypeid',$discounttype);
    $this->db->where('discountmodeid',$salesdiscounttypeid);
    $this->db->where('status',1);
    $result = $this->db->get();
    if($result->num_rows() > 0) {
    	foreach($result->result() as $row) {
    		$data = array('discountvalue'=>$row->discountvalue);
    	}
    }else {
    	$data = array('discountvalue'=>'');
    }
    echo json_encode($data);
  }
  public function generatebillnumber($type,$transactionmode)
  {
  	if($type == 11 | $type == 13 || $type == '16' || $type == '18' || $type == '20' || $type == '21' || $type == '22' || $type == '23'){
  	  $serialnumber =$this->Basefunctions->varrandomnumbergenerator(52,'salesnumber',$type,$transactionmode);
  	}else{
  	  $serialnumber =$this->Basefunctions->varrandomnumbergenerator(52,'salesnumber',$type,1);
  	  $transactionmode = 1;
  	}
  	$salestransactiontype = $type;
  	$this->db->select('varserialnumbermasterid,digitslength,suffix,prefix,currentnumber,serialnumbertypeid,estimationnumbermodeid');
  	$this->db->where('methodtype',$type);
  	$this->db->where('transactionmodeid',$transactionmode);
  	$this->db->where('moduleid',52);
  	$this->db->where('status',$this->Basefunctions->activestatus);
  	$inforow=$this->db->get('varserialnumbermaster');
  	$inforowdata = $inforow->num_rows();
  	if($inforowdata > 0){
  		$type = $type;
  		$digitslength = $inforow->row()->digitslength;
  		$suffix = $inforow->row()->suffix;
  		$prefix = $inforow->row()->prefix;
  		$serialtype = $inforow->row()->serialnumbertypeid;
		$estimationnumbermodeid = $inforow->row()->estimationnumbermodeid;
  	}else{
  		$type = 1;
  		$this->db->select('varserialnumbermasterid,digitslength,suffix,prefix,currentnumber,serialnumbertypeid,estimationnumbermodeid');
  		$this->db->where('methodtype',1);
  		$this->db->where('transactionmodeid',$transactionmode);
  		$this->db->where('moduleid',52);
  		$this->db->where('status',$this->Basefunctions->activestatus);
  		$infoall=$this->db->get('varserialnumbermaster');
  		if($infoall->num_rows() > 0){
  			$digitslength = $infoall->row()->digitslength;
  			$suffix = $infoall->row()->suffix;
  			$prefix = $infoall->row()->prefix;
  			$serialtype = $infoall->row()->serialnumbertypeid;
			$estimationnumbermodeid = $infoall->row()->estimationnumbermodeid;
  	    }else{
	  		$digitslength = 0;
	  		$suffix ='';
	  		$prefix ='';
	  		$serialtype = 0;
			$estimationnumbermodeid =1;
  		}
  	}
  	  if($serialtype == 3){
	    $this->db->select_max('salesid');
	    if($type == 11 | $type == 13 || $type == '16' || $type == '18' || $type == '20' || $type == '21' || $type == '22' || $type == '23'){
	    	$this->db->where('transactionmodeid',$transactionmode);
	    }	 
	  	$this->db->where('salestransactiontypeid',$salestransactiontype);
	  	$info=$this->db->get('sales');
	  	$last_billid=$info->row()->salesid;
	  	if($last_billid != '' && $last_billid != 'null'){
		  	$lastno = $this->Basefunctions->singlefieldfetchinactive('salesnumber','salesid','sales',$last_billid);
		  	if($prefix == '' && $suffix ==''){
		  		$prenumber = $lastno;
		  		$last_billno = $prenumber+1;
		  	}else if($prefix != '' && $suffix ==''){
		   	  $prenumber = explode($prefix,$lastno);
		   	  $last_billno = $prenumber[1]+1;
		  	}else if($prefix == '' && $suffix !=''){
		   	  $prenumber = explode($suffix,$lastno);
		   	  $last_billno = $prenumber[0]+1;
		  	}else{
		  		$number = explode($suffix,$lastno);
		  		$prenumber = explode($prefix,$number[0]);
		  		$last_billno = $prenumber[1]+1;
		  	}
	  		$current_number=str_pad($last_billno,$digitslength, '0', STR_PAD_LEFT);
	  		$num = $prefix.$current_number.$suffix;
	  		$number=array('currentnumber'=>$current_number);
		  	$this->db->where('moduleid',52)->where_in('methodtype',$type)->where('transactionmodeid',$transactionmode);
		  	$this->db->update('varserialnumbermaster',$number);
	  	}else{
	  		$num = $serialnumber;
	  	}
  	  }else if($serialtype == 2){
  	  	$num = $serialnumber;
  	  }
  	return $num;
  }
  //unique name check
  public function uniquedynamicviewnamecheckmodel(){
  	$industryid = $this->Basefunctions->industryid;
  	$accountmobilenumber=$_POST['accountmobilenumber'];
  	$partable = 'account';
  	$ptid = 'mobilenumber';
  	$ptname = 'mobilenumber';
  	if($accountmobilenumber != "") {
  		$this->db->select($ptid);
  		$this->db->from($partable);
  		$this->db->where_in('account.mobilenumber',$accountmobilenumber);
  		$this->db->where_in($partable.'.status',array(1));
  		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
  		$result = $this->db->get();
  		if($result->num_rows() > 0) {
  			echo "True";
  		} else { echo "False"; }
  	}else { echo "False"; }
  }
	public function Salesbillcancel() {
		$salesid = $_GET['primaryid'];
		$cancelledcomment = $_GET['cancelledcomment'];
		$salestransactiontype = $this->Basefunctions->singlefieldfetch('salestransactiontypeid','salesid','sales',$salesid);
		$sales_detail = array();
		$delete = $this->Basefunctions->delete_log();
		$ledgeraccountid = 1;
		$referencenumber = '';
		$creditno = '';
		$salesdate = '0000-00-00';
		$ledgerpaymentirtypeid = 1;
		$loclingtime = $this->Basefunctions->get_company_settings('lockingtime');
		$updatingtime = $this->Basefunctions->get_company_settings('updatingtime');
		$currenttime = time("H:i:s");
		$ctime = new DateTime();
		$ctime->setTimestamp($currenttime);
		$ltime  = new DateTime();
		$ltime  = new DateTime();
		if($loclingtime == '00:00:00') {
			$ltime->setTimestamp($currenttime);
		} else {
			$ltime->setTimestamp(strtotime($loclingtime));
		}
		//retrieve salesdetailid
		$salesdetaildata = $this->db->select('salesid,stocktypeid,salesdetailid,itemtagid,referencenumber,chitbookno,presalesdetailid')
		->from('salesdetail')
		->where('salesdetail.salesid',$salesid)
		->where('salesdetail.status',$this->Basefunctions->activestatus)
		->get();
		foreach($salesdetaildata->result() as $info) {
			$sales_detail[] = array(
					'salesid'=>$info->salesid,
					'stocktypeid'=>$info->stocktypeid,
					'salesdetailid'=>$info->salesdetailid,
					'itemtagid'=>$info->itemtagid,
					'referencenumber'=>$info->referencenumber,
					'chitbookno'=>$info->chitbookno,
					'presalesdetailid'=>$info->presalesdetailid
			);
		}
		$appunt = 0;
		$approvaloutuntagsdid = array();
		//delete salesdetail id
		for($i=0; $i < count($sales_detail) ;$i++) {
			//check whether they are tag methods.
			//update status of itemtag , tag or taginc
			$salestransactiontypeid = $this->Basefunctions->singlefieldfetch('salestransactiontypeid','salesid','sales',$salesid);
			if($salestransactiontypeid == 11 || $salestransactiontypeid == 13) { //sales & weight sales
				if($sales_detail[$i]['stocktypeid'] == 11 OR $sales_detail[$i]['stocktypeid'] == 13 OR $sales_detail[$i]['stocktypeid'] == 74 OR $sales_detail[$i]['stocktypeid'] == 83){
					if($sales_detail[$i]['stocktypeid'] == 74) {
						//$presalesid = $this->Basefunctions->singlefieldfetch('salesid','salesdetailid','salesdetail',$sales_detail[$i]['presalesdetailid']);
						//$presdid = $sales_detail[$i]['presalesdetailid'];
						$prestatus = $this->Basefunctions->approvaloutstatus;
						$updatesalesdetail = array(
						 'approvalstatus'=>1
						);
						if($ctime > $ltime) {
							$updatesalesdetail = array_merge($updatesalesdetail,$this->Crudmodel->nextdayupdatedefaultvalueget());
						} else {
							$updatesalesdetail = array_merge($updatesalesdetail,$this->Crudmodel->updatedefaultvalueget());
						}
						$this->db->where('salesdetailid',$sales_detail[$i]['presalesdetailid']);
						$this->db->update('salesdetail',$updatesalesdetail);
					} else {
						//$presalesid = '1';
						//$presdid = '1';
						$prestatus = $this->Basefunctions->activestatus;
					}
					$update = array(
						'status'=>$prestatus
					);
					if($ctime > $ltime) {
						$update=array_merge($update,$this->Crudmodel->nextdayupdatedefaultvalueget());
					} else {
						$update=array_merge($update,$this->Crudmodel->updatedefaultvalueget());
					}
					$this->db->where('itemtagid',$sales_detail[$i]['itemtagid']);
					$this->db->update('itemtag',$update);
				}
				$this->Basefunctions->stockupdateondelete($sales_detail[$i]['salesdetailid'],'52');
			}
			if($sales_detail[$i]['stocktypeid'] == '16' || $sales_detail[$i]['stocktypeid'] == '66' || $sales_detail[$i]['stocktypeid'] == '25') { 
				$approvaloutuntagsdid[$appunt] = $sales_detail[$i]['presalesdetailid'];
				$appunt++;
			}
			// update approval out.
			if($sales_detail[$i]['stocktypeid'] == 62) {
				$update = array(
						'salesid'=>'1',
						'salesdetailid'=>'1',
						'status'=>$this->Basefunctions->activestatus
				);
				if($ctime > $ltime) {
					$update = array_merge($update,$this->Crudmodel->nextdayupdatedefaultvalueget());
				} else {
					$update = array_merge($update,$this->Crudmodel->updatedefaultvalueget());
				}
				$this->db->where('itemtagid',$sales_detail[$i]['itemtagid']);
				$this->db->update('itemtag',$update);
			}
			//update approval out return.
			if($sales_detail[$i]['stocktypeid'] == 65) {
				$update = array(
						'status'=>$this->Basefunctions->approvaloutstatus
				);
				if($ctime > $ltime) {
					$update = array_merge($update,$this->Crudmodel->nextdayupdatedefaultvalueget());
				} else {
					$update = array_merge($update,$this->Crudmodel->updatedefaultvalueget());
				}
				$this->db->where('itemtagid',$sales_detail[$i]['itemtagid']);
				$this->db->update('itemtag',$update);
				$updatesalesdetail = array(
						'approvalstatus'=>1
				);
				if($ctime > $ltime) {
					$updatesalesdetail = array_merge($updatesalesdetail,$this->Crudmodel->nextdayupdatedefaultvalueget());
				} else {
					$updatesalesdetail = array_merge($updatesalesdetail,$this->Crudmodel->updatedefaultvalueget());
				}
				$this->db->where('salesdetailid',$sales_detail[$i]['presalesdetailid']);
				$this->db->update('salesdetail',$updatesalesdetail);
			}
			// Update Chitbook details
			if($sales_detail[$i]['stocktypeid'] == 39) {
				$chitbooktypeid = $this->Basefunctions->get_company_settings('chitbooktype');
				$chitbookupdate = array(
						'status' => $this->Basefunctions->activestatus
				);
				if($ctime > $ltime) {
					$chitbookupdate = array_merge($chitbookupdate,$this->Crudmodel->nextdayupdatedefaultvalueget());
				} else {
					$chitbookupdate = array_merge($chitbookupdate,$this->Crudmodel->updatedefaultvalueget());
				}
				$cbno = explode("|",$sales_detail[$i]['chitbookno']);
				$this->db->where_in('chitbookno',$cbno);
				if($chitbooktypeid == 0) {
					$this->db->update('chitbook',$chitbookupdate);
				} else if($chitbooktypeid == 1) {
					$this->db->update('chitgeneration',$chitbookupdate);
				}
			}
			//update approval out return.
			//delete sales detail.
			$this->db->where_in('salesdetailid',$sales_detail[$i]['salesdetailid']);
			$this->db->update('salesdetail',$delete);
			
			// Sales Stone Entry
			$this->db->where('salesid',$salesid);
			$this->db->update('salesstoneentry',$delete);
			
			// delete lot for purchase
			if($salestransactiontype == 9) {
				if(in_array($sales_detail[$i]['stocktypeid'],array(17,80,81))) {
					$lotupdatesalesdetail = array(
							'status'=>0
					);
					$lotupdatesalesdetail = array_merge($lotupdatesalesdetail,$this->Crudmodel->updatedefaultvalueget());
					$this->db->where('salesdetailid',$sales_detail[$i]['salesdetailid']);
					$this->db->update('lot',$lotupdatesalesdetail);
				}
			}
			//reverting order status
			if($salestransactiontype == '21'){ // place order
				$orderstatusid = $this->Basefunctions->singlefieldfetch('orderstatusid','salesdetailid','salesdetail',$sales_detail[$i]['presalesdetailid']);
				if($orderstatusid != 6) { // except cancel order
					$this->revertorderstatus(2,3,$sales_detail[$i]['presalesdetailid']);
					$presalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$sales_detail[$i]['presalesdetailid']);
					$this->revertorderstatus(2,3,$presalesdetailid);
					$this->update_placeorderitemstatus($this->Basefunctions->singlefieldfetch('salesid','salesdetailid','salesdetail',$sales_detail[$i]['presalesdetailid']));
				}
			}else if($sales_detail[$i]['stocktypeid']== '82') { // Receive - take order
				$orderstatusid = $this->Basefunctions->singlefieldfetch('orderstatusid','salesdetailid','salesdetail',$sales_detail[$i]['presalesdetailid']);
				if($orderstatusid != 6) { // except cancel order
					$this->revertorderstatus(3,5,$sales_detail[$i]['presalesdetailid']);
					$presalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$sales_detail[$i]['presalesdetailid']);
					$this->revertorderstatus(3,5,$presalesdetailid);
					//$this->Basefunctions->stockupdateondelete($sales_detail[$i]['itemtagid'],'50');
					$receiveorderconcept = $this->Basefunctions->get_company_settings('receiveorderconcept');
					if($receiveorderconcept == 0) {
						$updateitemtag=array(
								'ordernumber'=>'',
								'salesdetailid'=>1,
								'orderstatus'=>'No'
								);
						$this->db->where('itemtagid',$sales_detail[$i]['itemtagid']);
						$this->db->update('itemtag',$updateitemtag);
					} else {
						$presalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$sales_detail[$i]['presalesdetailid']);
						$lotupdatesalesdetail = array(
									'status'=>0
							);
						$updatesalesdetail = array_merge($lotupdatesalesdetail);
						$this->db->where('salesdetailid',$presalesdetailid);
						$this->db->update('lot',$lotupdatesalesdetail);
					}
				}
			}else if($sales_detail[$i]['stocktypeid']== '83' AND $salestransactiontype == '11') { // Order Tag
				$orderstatusid = $this->Basefunctions->singlefieldfetch('orderstatusid','salesdetailid','salesdetail',$sales_detail[$i]['presalesdetailid']);
				if($orderstatusid != 6) { // except cancel order
					$this->revertorderstatus(5,4,$sales_detail[$i]['presalesdetailid']);
					$presalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$sales_detail[$i]['presalesdetailid']);
					$this->revertorderstatus(5,4,$presalesdetailid);
					$ordersalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$presalesdetailid);
					$this->revertorderstatus(5,4,$ordersalesdetailid);
				}
			}  else if($sales_detail[$i]['stocktypeid']== '85') { // Generate place order revert back
				// updating place order salesdetail table
				$update_takeorderpostatus = array('generatepo'=>'1');
				$this->db->where('salesdetailid',$sales_detail[$i]['presalesdetailid']);
				$this->db->update('salesdetail',array_filter($update_takeorderpostatus));
			}
		}
		if($appunt > 0){
			$this->appoutuntagupdate($approvaloutuntagsdid,'0',$salestransactiontype);
		}	
		if(in_array($salestransactiontype,array(9,11,20,22,23,25,24))) {
			$this->accountledgerdelete($salesid);
		}
		// Delete salesdetail tax
		$this->db->where('salesid',$salesid);
		$this->db->update('salesdetailtax',$delete);
		
		// Delete creditnodetails table
		$this->db->where('salesid',$salesid);
		$this->db->update('creditnodetails',$delete);
		
		// Delete questionairedetails table
		$this->db->where('salesid',$salesid);
		$this->db->update('questionairedetails',$delete);
		
		// Revert back Esimation No to active status
		if($salestransactiontype == 11) {
		   $estimateno = $this->Basefunctions->singlefieldfetch('estimatenoarray','salesid','sales',$salesid);
		   if($estimateno != '' && $estimateno != '1') {
			   $estimateno = explode(',',$estimateno);
			   $activeestimateno = array('status'=>1);
			   $this->db->where_in('salesnumber',$estimateno);
			   $this->db->update('sales',$activeestimateno);
		   }
		}
		
		//cancel bill
		$cancelledcomment = array('cancelledcomment'=>$cancelledcomment);
		$cancel = array_merge($cancelledcomment,$this->Basefunctions->cancel_log());
		$this->db->where('salesid',$salesid);
		$this->db->update('sales',$cancel);
	  
		//audit-log
		$salesnumber = $this->Basefunctions->singlefieldfetch('salesnumber','salesid','sales',$salesid);
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->username;
		$activity = ''.$user.' Cancel Sales - '.$salesnumber.'';
		$this->Basefunctions->notificationcontentadd($salesid,'Cancelled',$activity ,$userid,$this->salesmoduleid);
		echo 'SUCCESS';
	}
	
	//place order overlay inner grid work
	public function takeorderviewdynamicdatainfofetch($tablename,$sortcol,$sortord,$pagenum,$rowscount,$orderid,$statusid) {
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$dataset='salesdetail.salesdetailid,account.accountname,product.productname,ROUND(salesdetail.grossweight,'.$round.') as grossweight,ROUND(salesdetail.stoneweight,'.$round.') as stoneweight,ROUND(salesdetail.netweight,'.$round.') as netweight,salesdetail.pieces,sizemaster.sizemastername,salesdetail.orderstatusid,orderstatus.orderstatusname,salesdetail.presalesdetailid,itemtag.itemtagnumber,salesdetail.ordernumber,salesdetail.vendorid,salesdetail.comment';
		$join =' LEFT JOIN account ON account.accountid=sales.accountid';
		$join .=' LEFT JOIN salesdetail ON salesdetail.salesid=sales.salesid';
		$join .=' LEFT JOIN product ON product.productid=salesdetail.productid';
		$join .=' LEFT JOIN orderstatus ON orderstatus.orderstatusid=salesdetail.orderstatusid';
		$join .=' LEFT JOIN sizemaster ON sizemaster.sizemasterid=salesdetail.orderitemsize';
		$join .=' LEFT JOIN itemtag ON itemtag.itemtagid=salesdetail.itemtagid';
		if($statusid == '5') { // receive order data
			$status=$tablename.'.status='.$this->Basefunctions->activestatus.' and salesdetail.stocktypeid in (82) and salesdetail.status = 1 and salesdetail.orderstatusid="'.$statusid.'" and sales.salesid="'.$orderid.'"';
		}if($statusid == '2') { // place order data
			$status=$tablename.'.status='.$this->Basefunctions->activestatus.' and salesdetail.stocktypeid in (75) and salesdetail.status = 1 and salesdetail.orderstatusid="'.$statusid.'" and sales.salesid="'.$orderid.'"';
		}else if($statusid == '3') { // take order data
			$status=$tablename.'.status='.$this->Basefunctions->activestatus.' and salesdetail.stocktypeid in (76) and salesdetail.status = 1 and salesdetail.orderstatusid="'.$statusid.'" and sales.salesid="'.$orderid.'"';
		}			
		$extracond = '';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		/* query */
		$result = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '. $status.$extracond.' ORDER BY '.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$data=array();
		$i=1;
		foreach($result->result() as $row) {
			$protid =$row->salesdetailid;
			$orderid =$row->salesdetailid;
			$productname =$row->productname;
			$grossweight =$row->grossweight;
			$stoneweight =$row->stoneweight;
			$netweight =$row->netweight;
			$pieces =$row->pieces;
			$orderitemsize =$row->sizemastername;
			$orderstatusid =$row->orderstatusid;
			$statusname =$row->orderstatusname;
			$itemtagnumber =$row->itemtagnumber;
			$ordernumber =$row->ordernumber;
			$comment =$row->comment;
			if($row->presalesdetailid != 1){
				$accountname = $this->Basefunctions->generalinformaion('account','accountname','accountid',$row->vendorid);
			} else {
				$accountname ='';
			}
			if($statusid == '5') { // receive order data
				$data[$i]=array('id'=>$protid,$itemtagnumber,$productname,$grossweight,$stoneweight,$netweight,$orderitemsize,$statusname,$orderstatusid,$ordernumber);
				$i++;
			}else{
				$data[$i]=array('id'=>$protid,$orderid,$accountname,$productname,$grossweight,$stoneweight,$netweight,$orderitemsize,$statusname,$orderstatusid,$comment);
				$i++;
			}
		}
		$finalresult=array('0'=>$data,'1'=>$page,'2'=>'json');
		return $finalresult;
	}
	
	//discount auth overlay work
	public function discountauthpasswordgetmodel() {
		$data = '';
		$industryid = $this->Basefunctions->industryid;
		$i=0;
		$employeeid = $_GET['employeeid'];
		$password = $_GET['newpassword'];
		$userroleid = $this->Basefunctions->generalinformaion('employee','userroleid','employeeid',$employeeid);
		$discountmode = $_GET['discountmode'];
		$discaltype = $_GET['discaltype'];
		$categoryid = $_GET['categoryid'];
		$purityid = $_GET['purityid'];
		$metalid = $_GET['metalid'];
		if($discaltype == 5) {
			$discaltype = 4;
		}
		$this->db->select('dapassword,discountvalue');
		$this->db->from('discountauthorization');
		$this->db->where_in('discountauthorization.discountmodeid',$discountmode);
		$this->db->where_in('discountauthorization.discounttypeid',$discaltype);
		$this->db->where('discountauthorization.purityid',$purityid);
		$this->db->where('discountauthorization.categoryid',$categoryid);
		$this->db->where('discountauthorization.metalid',$metalid);
		$this->db->where_in('discountauthorization.status',array(1));
		$this->db->where("FIND_IN_SET('".$userroleid."',userroleid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result()as $row) {
				$data = $row->dapassword;
				$limit = $row->discountvalue;
			}
			$passwdchk = password_verify($password, $data);
			if($passwdchk!='') {
				$array = array(
							'limit'=>$limit,
							'roleid'=>$userroleid,
							'status'=>'SUCCESS'
					);
			} else {
				$array = array(
							'status'=>'FAILURE'
					);
			}
		} else {
			$array = array(
							'status'=>'NOUSER'
					);
		}
		echo json_encode($array);
	}
	public function singlefieldfetch($field,$primaryfield,$table,$primaryid)
	{
		$this->db->select($field);
		$this->db->from($table);
		$this->db->where($primaryfield,$primaryid);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$this->db->limit(1);
		$qryinfo = $this->db->get();		
		if($qryinfo->num_rows()>0){
		$info = $qryinfo->row();
		return $info->$field;
		}else{
		return 0;
		}
	}
	public function revertorderstatus($revertstatus,$prestatus,$presalesdetailid) {
		$loclingtime = $this->Basefunctions->get_company_settings('lockingtime');
		$updatingtime = $this->Basefunctions->get_company_settings('updatingtime');
		$currenttime = time("H:i:s");
		$ctime = new DateTime();
		$ctime->setTimestamp($currenttime);
		$ltime  = new DateTime();
		if($loclingtime == '00:00:00') {
			$ltime->setTimestamp($currenttime);
		} else {
			$ltime->setTimestamp(strtotime($loclingtime));
		}
		$takeorderupdaterevert = array(
					'orderstatusid' =>$revertstatus,  //order status
			);
			if($ctime > $ltime) {
				$takeorderupdaterevert = array_merge($takeorderupdaterevert,$this->Crudmodel->nextdayupdatedefaultvalueget());
			} else {
				$takeorderupdaterevert = array_merge($takeorderupdaterevert,$this->Crudmodel->updatedefaultvalueget());
			}
			$this->db->where('salesdetailid',$presalesdetailid);
			$this->db->where('orderstatusid',$prestatus);
			$this->db->where('status',1);
			$this->db->update('salesdetail',$takeorderupdaterevert);
	}
    public function updatetakeorderstaus($status,$presalesdetailid)	{
		$update_order = array('orderstatusid'=>$status);
		$this->db->where('salesdetailid',$presalesdetailid);
		$this->db->update('salesdetail',array_filter($update_order));
	}
	// Revert back status for Repair Management
	public function revertrepairstatus($revertstatus,$prestatus,$presalesdetailid) {
		$loclingtime = $this->Basefunctions->get_company_settings('lockingtime');
		$updatingtime = $this->Basefunctions->get_company_settings('updatingtime');
		$currenttime = time("H:i:s");
		$ctime = new DateTime();
		$ctime->setTimestamp($currenttime);
		$ltime  = new DateTime();
		if($loclingtime == '00:00:00') {
			$ltime->setTimestamp($currenttime);
		} else {
			$ltime->setTimestamp(strtotime($loclingtime));
		}
		$takerepairupdaterevert = array(
					'repairstatusid' =>$revertstatus,  //order status
			);
			if($ctime > $ltime) {
				$takerepairupdaterevert = array_merge($takerepairupdaterevert,$this->Crudmodel->nextdayupdatedefaultvalueget());
			} else {
				$takerepairupdaterevert = array_merge($takerepairupdaterevert,$this->Crudmodel->updatedefaultvalueget());
			}
			$this->db->where('salesdetailid',$presalesdetailid);
			$this->db->where('repairstatusid',$prestatus);
			$this->db->where('status',1);
			$this->db->update('salesdetail',$takerepairupdaterevert);
	}
	// Update Take Repair Status
	public function updatetakerepairstaus($status,$presalesdetailid)	{
		$update_order = array('repairstatusid'=>$status);
		$this->db->where('salesdetailid',$presalesdetailid);
		$this->db->update('salesdetail',array_filter($update_order));
	}
	function orderadvanceamtrevert($salesid) {
		 $advanceno = $this->Basefunctions->singlefieldfetch('advanceno','salesid','sales',$salesid);
		  if($advanceno != '') {
			  $data_advanceadjustamt = 0;
			  $data_finaladvanceadjustamt = 0;
					$this->db->select('advanceadjustamt,orderadvanceamt');
					$this->db->from('sales');
					$this->db->where('salestransactiontypeid',20);
					$this->db->where('advanceno',$advanceno);
					$this->db->where('status',1);
					$result = $this->db->get();
					if($result->num_rows() > 0) {
						foreach($result->result() as $row) {
							$data_advanceadjustamt = $row->advanceadjustamt;
							$data_finaladvanceadjustamt = $row->orderadvanceamt;
						}
					} 
				$orderadvanceadjustamountrow = $this->db->query('select ROUND( +COALESCE(SUM(CASE WHEN salesdetail.stocktypeid in (31) then salesdetail.totalamount end),0),0) as totalamount from sales join salesdetail on salesdetail.salesid = sales.salesid where salesdetail.status = 1 and sales.salesid  = '.$salesid.'');
			  if($orderadvanceadjustamountrow->num_rows() > 0) {
					foreach($orderadvanceadjustamountrow->result() as $info) {
						$orderadvanceadjustamount = $info->totalamount;
					}
			  }else{
				  $orderadvanceadjustamount = 0;
			  }
			  $finaladvanceadjustamt = $data_advanceadjustamt - $orderadvanceadjustamount;
			  $finalamt = $data_finaladvanceadjustamt - $finaladvanceadjustamt;
			  $advancearray = array('advanceadjustamt'=>$finaladvanceadjustamt,'finaladvanceamt'=>$finalamt);
			  $this->db->where('advanceno',$advanceno);
			  $this->db->where('salestransactiontypeid',20);
			  $this->db->update('sales',$advancearray);
			}
	}
	public function getadvanceordernumber() {
		$this->db->distinct();
		$this->db->select("sales.salesid,sales.salesnumber,sales.accountid,account.accountname");
		$this->db->from('sales');
		$this->db->join('salesdetail','salesdetail.salesid=sales.salesid');
		$this->db->join('account','account.accountid=sales.accountid');
		$this->db->where('salesdetail.orderstatusid !=',4);
		$this->db->where('salesdetail.stocktypeid=',75);
		$this->db->where('sales.salestransactiontypeid',20);
		$this->db->where('salesdetail.status',1);
		$this->db->where('sales.status',1);
		$this->db->order_by('salesid',"asc");
		$result = $this->db->get();
		return $result->result();
    }
	//account data set

	public function fetchaccountdatasetmodel() {
		$data = array();
		$accountledgerdata = array();
		$accountledgerresult = array();
		$accountid = $_POST['accountid'];
		$this->db->select('salutationid,accounttypeid,parentaccountid,accountname,mobilenumber,additionalmobilenumber,stateid,countryid,accountaddress.address,accountaddress.pincode,accountaddress.city,accountcf.primaryarea,gstnnumber,account.accountcatalogid,account.accountid,account.accountshortname,account.pannumber');
		$this->db->from('account');
		$this->db->join('accountaddress','accountaddress.accountid=account.accountid and accountaddress.addressmethod=4','left outer');
		$this->db->join('accountcf','accountcf.accountid=account.accountid','left outer');
		$this->db->where('account.accountid',$accountid);
		$data = $this->db->get();
		if($data->num_rows() > 0) {
			foreach($data->result() as $row) {
				//$parentaccountname = $this->Basefunctions->generalinformaion('account','accountname','accountid',$row->parentaccountid);
				$data = array(
					'accounttypeid'=>$row->accounttypeid,
					'accountid'=>$row->accountid,
					'salutationid'=>$row->salutationid,
					'accountname'=>$row->accountname,
					'accountshortname'=>$row->accountshortname,
					'mobilenumber'=>$row->mobilenumber,
					'additionalmobilenumber'=>$row->additionalmobilenumber,
					'pannumber'=>$row->pannumber,
					'primaryarea'=>$row->primaryarea,
					'stateid'=>$row->stateid,
					'countryid'=>$row->countryid,
					'address'=>$row->address,
					'pincode'=>$row->pincode,
					'city'=>$row->city,
					'gstnnumber'=>$row->gstnnumber,
					'parentaccountid'=>$row->parentaccountid,
					'accountcatalogid'=>$row->accountcatalogid,
				);
			}
		} else {
			$data = array('accounttypeid'=>0);
		}
		$result['accountdata'] = $data;
		echo json_encode($result);
	}
	
	public function get_sales_stoneentry_div($salesid,$salesdetailid)
	{
		//tag based charges
		$infotag = array();
		$infotagcharge = array();
		$this->db->select('se.salesstoneentryid,se.stoneid,se.stonetypeid as orgstonetypeid,stone.stonename,se.purchaserate,se.stonerate,se.pieces,se.caratweight,se.totalcaratweight,se.totalnetweight,se.totalamount,stonetype.stonetypeid,stonetype.stonetypename,stoneentrycalctype.stoneentrycalctypeid,stoneentrycalctype.stoneentrycalctypename,se.gram');
		$this->db->from('salesstoneentry as se');
		$this->db->join('stone','stone.stoneid = se.stoneid');
		$this->db->join('stonecategory','stonecategory.stonecategoryid = stone.stonecategoryid');
		$this->db->join('stonetype','stonetype.stonetypeid = stonecategory.stonetypeid');
		$this->db->join('stoneentrycalctype','stoneentrycalctype.stoneentrycalctypeid = se.stoneentrycalctypeid');
		$this->db->where('se.salesdetailid',$salesdetailid);
		$this->db->where('se.salesid',$salesid);
		$this->db->where_in('se.status',array($this->Basefunctions->activestatus));
		$data = $this->db->get();
		if($data->num_rows()>0){
			$i=1;
			foreach($data->result() as $row)
			{
				$infotag[] = array(
						'stonegroupid'=>$row->orgstonetypeid,
						'stonegroupname'=>$row->stonetypename,
						'stoneid' => $row->stoneid,
						'stonename'=>$row->stonename,
						'purchaserate' =>$row->purchaserate,
						'basicrate' =>$row->stonerate,
						'stoneentrycalctypeid' => $row->stoneentrycalctypeid,
						'stoneentrycalctypename'=>$row->stoneentrycalctypename,
						'stonepieces' => $row->pieces,
						'caratweight' => $row->caratweight,
						'stonegram' => $row->gram,
						'totalcaratweight' => $row->totalcaratweight,
						'totalweight' =>$row->totalnetweight,
						'stonetotalamount' =>$row->totalamount
				);
			$i++;
			}
			return json_encode($infotag);
		}else{
			return '';
		}
		
	}
	public function get_sales_stoneentry_details($salesid,$salesdetailid)
	{
		//tag based charges
		$accountid = $this->Basefunctions->singlefieldfetch('accountid','salesid','sales',$salesid);
		$this->db->select('se.salesstoneentryid,se.stoneid,se.stonetypeid as orgstonetypeid,stone.stonename,se.purchaserate,se.stonerate,se.pieces,se.caratweight,se.totalcaratweight,se.totalnetweight,se.totalamount,stonetype.stonetypeid,stonetype.stonetypename,stoneentrycalctype.stoneentrycalctypeid,stoneentrycalctype.stoneentrycalctypename,se.gram,
		(SELECT (SELECT applyrate from stonechargedetail
		where stonechargeid IN (select acc.stonechargeid from account as acc where acc.accountid='.$accountid.') AND
		stoneid=se.stoneid AND status=1)) AS applyrate');
		$this->db->from('salesstoneentry as se');
		$this->db->join('stone','stone.stoneid = se.stoneid');
		$this->db->join('stonecategory','stonecategory.stonecategoryid = stone.stonecategoryid');
		$this->db->join('stonetype','stonetype.stonetypeid = stonecategory.stonetypeid');
		$this->db->join('stoneentrycalctype','stoneentrycalctype.stoneentrycalctypeid = se.stoneentrycalctypeid');
		$this->db->where('se.salesdetailid',$salesdetailid);
		$this->db->where('se.salesid',$salesid);
		$this->db->where_in('se.status',array($this->Basefunctions->activestatus));
		$data = $this->db->get();
		if($data->num_rows()>0){
			$j=1;
				foreach($data->result() as $value) {
					if($value->applyrate!='')
					{
						$stonerate_div=$value->applyrate;
						$totalamt_div=round($stonerate_div*$value->caratweight);
					}
					else
					{
						$stonerate_div=$value->stonerate;
						$totalamt_div=$value->totalamount;
					}
					$stonedetail->rows[$j]['id'] = $j;
						$stonedetail->rows[$j]['cell']=array(
								$value->stonetypeid,
								$value->stonetypename,
								$value->stoneid,
								$value->stonename,
								$value->purchaserate,
								$stonerate_div,
								$value->stoneentrycalctypeid,
								$value->stoneentrycalctypename,
								$value->pieces,
								$value->caratweight,
								$value->gram,
								$value->totalcaratweight,
								$value->totalnetweight,
								$totalamt_div
						);
					
					$j++;
				}
				return json_encode($stonedetail);
		}else{
			return '';
		}
		
	}
	public function get_sales_taxentry_div($salesid,$salesdetailid)
	{
		//tag based charges
		$infotag = array();
		$infotagcharge = array();
		$taxdetail = [];
		
		$this->db->select('se.taxname,se.taxid,se.taxrate,se.value,mid.taxmasterid,mid.taxmastername');
		$this->db->from('salesdetailtax as se');
		$this->db->join('taxmaster as mid','mid.taxmasterid = se.taxcategoryid');
		$this->db->where('se.salesdetailid',$salesdetailid);
		$this->db->where('se.salesid',$salesid);
		$this->db->where_in('se.status',array($this->Basefunctions->activestatus));
		$this->db->where_in('mid.status',array($this->Basefunctions->activestatus));
		$data = $this->db->get();
		if($data->num_rows()>0){
			$i=1;
			foreach($data->result() as $value)
			{
			   $taxdetail['id'] = $value->taxmasterid;
			   $taxdetail['data'][] = array('taxmasterid'=>$value->taxmasterid,'taxmastername'=>$value->taxmastername,'taxid'=>$value->taxid,'taxname'=>$value->taxname,'rate'=>$value->taxrate,'amount'=>$value->value);
				$i++;
			}
			return json_encode($taxdetail);
		}else{
			return '';
		}
		
	}
	public function get_sales_taxentry_data($salesid,$salesdetailid)
	{
		//tag based charges
		$infotag = array();
		$infotagcharge = array();
		$taxdetail = '';
		
		$this->db->select('se.taxname,se.taxid,se.taxrate,se.value,mid.taxmasterid,mid.taxmastername');
		$this->db->from('salesdetailtax as se');
		$this->db->join('taxmaster as mid','mid.taxmasterid = se.taxcategoryid');
		$this->db->where('se.salesdetailid',$salesdetailid);
		$this->db->where('se.salesid',$salesid);
		$this->db->where_in('se.status',array($this->Basefunctions->activestatus));
		$this->db->where_in('mid.status',array($this->Basefunctions->activestatus));
		$data = $this->db->get();
		if($data->num_rows()>0) {
			$j=1;
			foreach($data->result() as $value)
			{
			   $taxdetail->rows[$j]['id']=$j;
				$taxdetail->rows[$j]['cell']=array(
						$value->taxmasterid,
						$value->taxmastername,
						$value->taxid,
						$value->taxname,
						$value->taxrate,
						$value->value
				);
				$j++;
			}
			return json_encode($taxdetail);
		}else{
			return '';
		}
		
	}
	// main mainedit main editcheck
	public function checkeditsales() {
		$salesid = $_POST['salesid'];
		$itemtagid = '';
		$stocktypeid = 1;
		$takeoderstatus = 0;
		$povendormanagement = 0;
		$estimatearray['rows'] = 0;
		$estimatearray['cancelstatus'] = 0;	
		$salestransactiontypeid = $this->Basefunctions->singlefieldfetch('salestransactiontypeid','salesid','sales',$salesid);
		if($salestransactiontypeid == 13) {
			$this->db->select('salesid');
			$this->db->from('sales');
			$this->db->where('salestransactiontypeid',13);
			$this->db->where('salesid',$salesid);
			$this->db->where('status',$this->Basefunctions->activestatus);
			$data = $this->db->get();
			$estimatearray['rows'] = $data->num_rows(); 
			$estimatearray['id'] = $salestransactiontypeid;
			$estimatearray['cancelstatus'] = 0;
		} else if($salestransactiontypeid == 18) {
			$itemtagid = '';
			$salesdetailid = '';
			$this->db->select('GROUP_CONCAT(itemtagid) as itemtagid,GROUP_CONCAT(salesdetailid) as salesdetailid',false);
			$this->db->from('salesdetail');
			$this->db->where('stocktypeid',62);
			$this->db->where('salesid',$salesid);
			$this->db->where('status',$this->Basefunctions->activestatus);
			$data = $this->db->get();
			if($data->num_rows() > 0){
				$itemtagid = $data->row()->itemtagid;
				$itemtagid = explode(',',$itemtagid);
				$salesdetailid = $data->row()->salesdetailid;
				$salesdetailid = explode(',',$salesdetailid);
			}
			$this->db->select('salesdetailid');
			$this->db->from('salesdetail');
			$this->db->where_in('stocktypeid',array(65,74));
			$this->db->where_in('itemtagid',$itemtagid);
			$this->db->where_in('presalesdetailid',$salesdetailid);
			$this->db->where('status',$this->Basefunctions->activestatus);
			$final = $this->db->get();
			$estimatearray['rows'] = $final->num_rows(); 
			$estimatearray['id'] = $salestransactiontypeid;
			$estimatearray['cancelstatus'] = 0;
		} else if($salestransactiontypeid == 11) {
			$salesreturntagged = $this->checksalesreturntagged($salesid);
			$salesitemwithreturntagged = $this->checksalesitemwithreturntagged($salesid);
			if($salesreturntagged == '' && $salesitemwithreturntagged == '') {
				$creditno = $this->Basefunctions->singlefieldfetch('creditno','salesid','sales',$salesid);
				$estimatearray['rows'] = $this->checkinaccountledgerforentry($creditno); 
				$estimatearray['id'] = $salestransactiontypeid;
			} else {
				$estimatearray['rows'] = 1;
				$estimatearray['id'] = 'return'.$salestransactiontypeid;
			}
			$estimatearray['cancelstatus'] = 0;
		} else if($salestransactiontypeid == 9) {
			$datapurchase =$this->db->query('select itemtag.itemtagid from salesdetail left join lot on lot.salesdetailid = salesdetail.salesdetailid left join itemtag on itemtag.lotid = lot.lotid where stocktypeid in (17,80,81) and salesdetail.status = 1 and lot.status not in (0,3) and itemtag.status not in (0,3) and salesdetail.salesid = '.$salesid.'');
			if($datapurchase->num_rows() > 0) {
				$estimatearray['rows'] = $datapurchase->num_rows(); 
				$estimatearray['id'] = $salestransactiontypeid;
				$estimatearray['cancelstatus'] = 0;
			}else {
				$creditno = $this->Basefunctions->singlefieldfetch('creditno','salesid','sales',$salesid);
				$estimatearray['rows'] = $this->checkinaccountledgerforentry($creditno); 
				$estimatearray['id'] = $salestransactiontypeid;
				$estimatearray['cancelstatus'] = 0;
			}
		} else if($salestransactiontypeid == 20 || $salestransactiontypeid == 21) {
			if($salestransactiontypeid == 20) {
				$this->db->select('salesdetailid');
				$this->db->from('salesdetail');
				$this->db->where('stocktypeid',75);
				$this->db->where('salesid',$salesid);
				$this->db->where('status',$this->Basefunctions->activestatus);
				$datatake = $this->db->get();
				if($datatake->num_rows() > 0) {
					$stocktypeid = 75;
					$takeoderstatus = 1;
				}else {
					$stocktypeid = 82;
					$takeoderstatus = 0;
				}
			} else if($salestransactiontypeid == 21) {
				$stocktypeid = 76;
			}
			$this->db->select('GROUP_CONCAT(salesdetailid) as salesdetailid',false);
			$this->db->from('salesdetail');
			$this->db->where('stocktypeid',$stocktypeid);
			$this->db->where('salesid',$salesid);
			$this->db->where('status',$this->Basefunctions->activestatus);
			$data = $this->db->get();
			if($data->num_rows() > 0){
				$salesdetailid = $data->row()->salesdetailid;
				$salesdetailid = explode(',',$salesdetailid);
			}
			if($salestransactiontypeid == 20) {
				$receiveorderconcept = $this->Basefunctions->get_company_settings('receiveorderconcept');
				if($takeoderstatus == 1) {				
					$this->db->select('salesdetailid');
					$this->db->from('salesdetail');
					$this->db->where('stocktypeid',76);
					$this->db->where_in('presalesdetailid',$salesdetailid);
					$this->db->where('status',$this->Basefunctions->activestatus);
					$data = $this->db->get();
					$estimatearray['rows'] = $data->num_rows(); 
					$estimatearray['id'] = $salestransactiontypeid;
					$estimatearray['cancelstatus'] = 0;
				} else {
					$this->db->select('salesdetailid');
					$this->db->from('salesdetail');
					$this->db->where('stocktypeid',83);
					$this->db->where_in('presalesdetailid',$salesdetailid);
					$this->db->where('status',$this->Basefunctions->activestatus);
					$data = $this->db->get();
					if($data->num_rows() > 0) {
						$estimatearray['rows'] = $data->num_rows(); 
						$estimatearray['id'] = $salestransactiontypeid;
						$estimatearray['cancelstatus'] = 0;
					} else if($receiveorderconcept == 1) {
						$receiveorderdata =$this->db->query('select itemtag.itemtagid from salesdetail left join lot on lot.salesdetailid = salesdetail.salesdetailid left join itemtag on itemtag.lotid = lot.lotid where stocktypeid in (82) and salesdetail.status = 1 and lot.status not in (0,3) and itemtag.status not in (0,3) and salesdetail.salesid = '.$salesid.'');
						if($receiveorderdata->num_rows() > 0) {
							$estimatearray['rows'] = $receiveorderdata->num_rows(); 
							$estimatearray['id'] = $salestransactiontypeid;
							$estimatearray['cancelstatus'] = 0;
						}
					} else {
						$this->db->select('salesdetailid');
						$this->db->from('salesdetail');
						$this->db->where('orderstatusid !=',6);
						$this->db->where('salesid',$salesid);
						$this->db->where('stocktypeid',82);
						$this->db->where('status',$this->Basefunctions->activestatus);
						$canceldata = $this->db->get();
						if($canceldata->num_rows() == 0) {
							$estimatearray['rows'] = 1; 
							$estimatearray['id'] = $salestransactiontypeid;
							$estimatearray['cancelstatus'] = 1;
						}else {
							$estimatearray['rows'] = 0; 
							$estimatearray['id'] = $salestransactiontypeid;
							$estimatearray['cancelstatus'] = 0;
						}
					}
				}
			} else if($salestransactiontypeid == 21) {
				//$postatus = $this->Basefunctions->singlefieldfetch('postatus','salesid','sales',$salesid);
				//if($postatus != 1) {
					/* if(isset($_POST['vendorstatus'])) {
						if($_POST['vendorstatus'] == 1){
							$estimatearray['rows'] = 0; 
							$estimatearray['id'] = '1';
							$estimatearray['cancelstatus'] = 0;
						}
					} else {
						$estimatearray['rows'] = 1; 
						$estimatearray['id'] = 'acp21';
						$estimatearray['cancelstatus'] = 0;
					} */
				//}
				if(isset($_POST['povendormanagement'])) {
					$povendormanagement = $_POST['povendormanagement'];
				} else {
					$povendormanagement = 0;
				}
				if($povendormanagement == 1) {
					$this->db->select('salesdetailid');
					$this->db->from('salesdetail');
					$this->db->where('stocktypeid',76);
					$this->db->where_in('poorderstatusid',array(21,22));
					$this->db->where_in('salesdetailid',$salesdetailid);
					$this->db->where('status',$this->Basefunctions->activestatus);
					$data = $this->db->get();
					$estimatearray['rows'] = $data->num_rows(); 
					$estimatearray['id'] = $salestransactiontypeid;
					$estimatearray['cancelstatus'] = 0;
				} else {
					$this->db->select('salesdetailid');
					$this->db->from('salesdetail');
					$this->db->where('stocktypeid',82);
					$this->db->where_in('presalesdetailid',$salesdetailid);
					$this->db->where('status',$this->Basefunctions->activestatus);
					$data = $this->db->get();
					$estimatearray['rows'] = $data->num_rows(); 
					$estimatearray['id'] = $salestransactiontypeid;
					$estimatearray['cancelstatus'] = 0;
				}
			} else {
				$estimatearray['rows'] = 0;
				$estimatearray['cancelstatus'] = 0;
			}
		} else if($salestransactiontypeid == 28 || $salestransactiontypeid == 29 || $salestransactiontypeid == 30) { // Take Repair, Place Repair - Edit Check
			if($salestransactiontypeid == 28) {
				$prevstocktypeid = '86';
				$presentstocktypeid = '87';
			} else if($salestransactiontypeid == 29) {
				$prevstocktypeid = '87';
				$presentstocktypeid = '88';
			} else if($salestransactiontypeid == 30) {
				$prevstocktypeid = '88';
				$presentstocktypeid = '89';
			}
			$this->db->select('GROUP_CONCAT(salesdetailid) as salesdetailid',false);
			$this->db->from('salesdetail');
			$this->db->where('stocktypeid',$prevstocktypeid);
			$this->db->where('salesid',$salesid);
			$this->db->where('status',$this->Basefunctions->activestatus);
			$data = $this->db->get();
			if($data->num_rows() > 0){
				$salesdetailid = $data->row()->salesdetailid;
				$salesdetailid = explode(',',$salesdetailid);
			}
			$this->db->select('salesdetailid');
			$this->db->from('salesdetail');
			$this->db->where('stocktypeid',$presentstocktypeid);
			$this->db->where_in('presalesdetailid',$salesdetailid);
			$this->db->where('status',$this->Basefunctions->activestatus);
			$data = $this->db->get();
			$estimatearray['rows'] = $data->num_rows(); 
			$estimatearray['id'] = $salestransactiontypeid;
			$estimatearray['cancelstatus'] = 0;
		} else if($salestransactiontypeid == 27) {
			$deliverypomodeid = $this->Basefunctions->singlefieldfetch('deliverypomodeid','salesid','sales',$salesid);
			if($deliverypomodeid != 1) {
				$estimatearray['rows'] = 1;
				$estimatearray['id'] = 'delpo27';
				$estimatearray['cancelstatus'] = 0;
			} else {
				$estimatearray['rows'] = 0;
				$estimatearray['cancelstatus'] = 0;
			}
		} else{
			$estimatearray['rows'] = 0;
			$estimatearray['cancelstatus'] = 0;	
		}
		echo json_encode($estimatearray);
	}
	// Retrieve Delivery PlaceOrder Status
	public function checkdeliverypostatus() {
		$salesid = $_POST['salesid'];
		$this->db->select('deliverypomodeid,deliverypodate,delvendorname,delcouriername,delotp,deliverypocomment,delemployeeid,delvstatus');
		$this->db->from('sales');
		$this->db->where('status',$this->Basefunctions->activestatus);
		$this->db->where('salesid',$salesid);
		$this->db->limit(1);
		$data = $this->db->get();
		if($data->num_rows() > 0) {
			$status = array(
				'deliverypomodeid' => $data->row()->deliverypomodeid,
				'deliverypodate' => date("Y-m-d", strtotime($data->row()->deliverypodate)),
				'delvendorname' => $data->row()->delvendorname,
				'delemployeeid' => $data->row()->delemployeeid,
				'delcouriername' => $data->row()->delcouriername,
				'delotp' => $data->row()->delotp,
				'deliverypocomment' => $data->row()->deliverypocomment,
				'delvstatus' => $data->row()->delvstatus,
				);
		} else {
			$status = array(
				'deliverypomodeid' => 1,
				'deliverypodate' => '0000-00-00',
				'delvendorname' => '',
				'delcouriername' => '',
				'delotp' => '',
				'deliverypocomment' => '',
				'delvstatus' => '1',
				);
		}
		echo json_encode($status);
	}
	// credit no balance summary amount
	public function creditnobalancesummary($salesid,$salesdetailid,$salesnumber,$accountid,$creditno,$paymentirtypeid) 
	{
		$finalamt = 0;
		$finalamtissue = 0;
		$finalamtreceipt = 0;
		$issuereceiptid = 1;
		$wtissuereceiptid = 1;
		$finalwt = 0;
		$finalwtissue = 0;
		$finalwtreceipt = 0;
		$loclingtime = $this->Basefunctions->get_company_settings('lockingtime');
		$updatingtime = $this->Basefunctions->get_company_settings('updatingtime');
		$currenttime = time("H:i:s");
		$ctime = new DateTime();
		$ctime->setTimestamp($currenttime);
		$ltime  = new DateTime();
		$salesdate = '0000-00-00';
		// get main sales entry date
			$this->db->select('salesdate');
			$this->db->from('accountledger');
			$this->db->where('creditno',$creditno);
			$this->db->where('accountid',$accountid);
			$this->db->where_in('entrymode',array(2,3,6));
			$this->db->where('status',$this->Basefunctions->activestatus);
			$this->db->order_by('entrymode',"asc");
			$this->db->limit(1);
			$data = $this->db->get();
			if($data->num_rows() > 0)
			{
				$salesdate = $data->row()->salesdate;
			}else
			{
				$salesdate = '0000-00-00';
			}
		// delete pre summary entry
			$this->db->where('creditno',$creditno);
			$this->db->where('accountid',$accountid);
			$this->db->where('entrymode',4);
			$this->db->delete('accountledger');
		// summary amt	
		$salessum = $this->db->query('SELECT  sum(amtissue) as sumamtissue,sum(amtreceipt) as sumamtreceipt,sum(weightissue) as sumwtissue,sum(weightreceipt) as sumwtreceipt FROM accountledger WHERE accountledger.status=1 AND accountledger.accountid = '.$accountid.' AND accountledger.creditno = "'.$creditno.'" AND accountledger.entrymode IN (2,3,6,7,8) GROUP BY `accountledger`.`accountid`,`accountledger`.`creditno`');
		if($salessum->num_rows() > 0) 
		{
			foreach($salessum->result() as $info) 
			{
				
				// amt summary
				$finalamt = $info->sumamtissue - $info->sumamtreceipt;
				if($finalamt > 0) 
				{
					$issuereceiptid = 2;
					$finalamtissue = $finalamt;
					$finalamtreceipt = 0;
				}else if($finalamt < 0)
				{
					$issuereceiptid = 3;
					$finalamtissue = 0;
					$finalamtreceipt = $finalamt;
				}else if($finalamt == 0) 
				{
					$issuereceiptid = 1;
					$finalamtissue = 0;
					$finalamtreceipt = 0;
				}
				// wt  summary
				$finalwt = $info->sumwtissue - $info->sumwtreceipt;
				if($finalwt > 0) 
				{
					$wtissuereceiptid = 2;
					$finalwtissue = $finalwt;
					$finalwtreceipt = 0;
				}else if($finalwt < 0)
				{
					$wtissuereceiptid = 3;
					$finalwtissue = 0;
					$finalwtreceipt = $finalwt;
				}else if($finalwt == 0) 
				{
					$wtissuereceiptid = 1;
					$finalwtissue = 0;
					$finalwtreceipt = 0;
				}
				$accledgerstatus = 1;
				if($finalamt == 0 && $finalwt == 0) // credit no balance close
				{
					$accledgerstatus = 11;
				}
				$insertaccountledgerdata = array(
								'salesid'=>$salesid,
								'salesdetailid'=>$salesdetailid,
								'accountid'=>$accountid,
								'entrymode'=>4,
								'creditno'=>$creditno,
								'referencenumber'=>$salesnumber,
								'salesdate'=>$salesdate, // need to verify while delete main sales
								'issuereceiptid'=>$issuereceiptid,
								'weightissuereceiptid'=>$wtissuereceiptid,
								'paymentirtypeid'=>$paymentirtypeid,
								'amtissue'=>$info->sumamtissue,
								'finalamtissue'=>abs($finalamtissue),
								'amtreceipt'=>$info->sumamtreceipt,
								'finalamtreceipt'=>abs($finalamtreceipt),
								'weightissue'=>$info->sumwtissue,
								'finalweightissue'=>abs($finalwtissue),
								'weightreceipt'=>$info->sumwtreceipt,
								'finalweightreceipt'=>abs($finalwtreceipt),
								'status'=>$accledgerstatus
						);
			   if($ctime > $ltime) {
						$insertaccountledgerdata = array_merge($insertaccountledgerdata,$this->Crudmodel->nextdaydefaultvalueget());
				} else {
						$insertaccountledgerdata = array_merge($insertaccountledgerdata,$this->Crudmodel->defaultvalueget());
				}
				$this->db->insert('accountledger',array_filter($insertaccountledgerdata));
			}
		}
	}
	// account no balance summary
	public function accountbalancesummary($salesid,$salesdetailid,$salesnumber,$accountid,$salesdate,$paymentirtypeid) 
	{
		$finalamt = 0;
		$finalamtissue = 0;
		$finalamtreceipt = 0;
		$issuereceiptid = 1;
		$finalwt = 0;
		$finalwtissue = 0;
		$finalwtreceipt = 0;
		$wtissuereceiptid = 1;
		$loclingtime = $this->Basefunctions->get_company_settings('lockingtime');
		$updatingtime = $this->Basefunctions->get_company_settings('updatingtime');
		$currenttime = time("H:i:s");
		$ctime = new DateTime();
		$ctime->setTimestamp($currenttime);
		$ltime  = new DateTime();
		// delete pre summary entry
			$this->db->where('accountid',$accountid);
			$this->db->where('entrymode',5);
			$this->db->delete('accountledger');
		// summary amt	
		$salessum = $this->db->query('SELECT  sum(amtissue) as sumamtissue,sum(amtreceipt) as sumamtreceipt,sum(weightissue) as sumwtissue,sum(weightreceipt) as sumwtreceipt FROM accountledger WHERE accountledger.status=1 AND accountledger.accountid = '.$accountid.' AND accountledger.entrymode IN (2,3,6,7,8) GROUP BY `accountledger`.`accountid`');
		if($salessum->num_rows() > 0) 
		{
				foreach($salessum->result() as $info) 
				{
					// amt summary
					$finalamt = $info->sumamtissue - $info->sumamtreceipt;
					if($finalamt > 0) 
					{
						$issuereceiptid = 2;
						$finalamtissue = $finalamt;
						$finalamtreceipt = 0;
					}else if($finalamt < 0)
					{
						$issuereceiptid = 3;
						$finalamtissue = 0;
						$finalamtreceipt = $finalamt;
					}
					
					// wt summary
					
					$finalwt = $info->sumwtissue - $info->sumwtreceipt;
					if($finalwt > 0) 
					{
						$wtissuereceiptid = 2;
						$finalwtissue = $finalwt;
						$finalwtreceipt = 0;
					}else if($finalwt < 0)
					{
						$wtissuereceiptid = 3;
						$finalwtissue = 0;
						$finalwtreceipt = $finalwt;
					}
					$insertaccountledgerdata = array(
									'salesid'=>$salesid,
									'salesdetailid'=>$salesdetailid,
									'accountid'=>$accountid,
									'entrymode'=>5,
									'referencenumber'=>$salesnumber,
									'salesdate'=>$salesdate,
									'issuereceiptid'=>$issuereceiptid,
									'weightissuereceiptid'=>$wtissuereceiptid,
									'paymentirtypeid'=>$paymentirtypeid,
									'amtissue'=>$info->sumamtissue,
									'finalamtissue'=>abs($finalamtissue),
									'amtreceipt'=>$info->sumamtreceipt,
									'finalamtreceipt'=>abs($finalamtreceipt),
									'weightissue'=>$info->sumwtissue,
									'finalweightissue'=>abs($finalwtissue),
									'weightreceipt'=>$info->sumwtreceipt,
									'finalweightreceipt'=>abs($finalwtreceipt),
									'status'=>$this->Basefunctions->activestatus
							);
				   if($ctime > $ltime) {
							$insertaccountledgerdata = array_merge($insertaccountledgerdata,$this->Crudmodel->nextdaydefaultvalueget());
					} else {
							$insertaccountledgerdata = array_merge($insertaccountledgerdata,$this->Crudmodel->defaultvalueget());
					}
					$this->db->insert('accountledger',array_filter($insertaccountledgerdata));
				}
		}
	}
	
	// get open close amt & weight
	public function loadaccountopenclose()
	{
		$accountid = $_POST['accountid'];
		$finalamt = 0;
		$advancefinalamt = 0;
		$creditbalfinalamt = 0;
		$finalwt = 0;
		$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$weightround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
			$this->db->select('issuereceiptid,weightissuereceiptid,ROUND(finalamtissue,'.$amountround.') as finalamtissue,ROUND(finalamtreceipt,'.$amountround.') as finalamtreceipt,ROUND(finalweightissue,'.$weightround.') as finalweightissue,ROUND(finalweightreceipt,'.$weightround.') as finalweightreceipt');
			$this->db->from('accountledger');
			$this->db->where('accountledger.status',1);
			$this->db->where('accountledger.accountid',$accountid);
			$this->db->where('accountledger.entrymode',5); // account based summary
			$accountledgerdata = $this->db->get();
			if($accountledgerdata->num_rows() > 0) {
				foreach($accountledgerdata->result() as $info) {
					if($info->issuereceiptid == 2) // issue amt
					{
						$finalamt = $info->finalamtissue;
					}else if($info->issuereceiptid == 3) // receipt amt
					{
						$finalamt = $info->finalamtreceipt;
					}
					if($info->weightissuereceiptid == 2) // issue wt
					{
						$finalwt = $info->finalweightissue;
					}else if($info->weightissuereceiptid == 3) // receipt wt
					{
						$finalwt = $info->finalweightreceipt;
					}
					$creditbalfinalamt = $info->finalamtissue;
					$advancefinalamt = $info->finalamtreceipt;
					$accountledgerresult = array(
						'issuereceiptid'=>$info->issuereceiptid,
						'weightissuereceiptid'=>$info->weightissuereceiptid,
						'finalamt'=>$finalamt,
						'advanceamt'=>$advancefinalamt,
						'creditamt'=>$creditbalfinalamt,
						'finalwt'=>$finalwt,
						'accounttypeid'=>$this->Basefunctions->singlefieldfetch('accounttypeid','accountid','account',$accountid)
					);
				}
			}else{
				$accountledgerresult = array(
						'issuereceiptid'=>1,
						'weightissuereceiptid'=>1,
						'finalamt'=>0,
						'advanceamt'=>0,
						'creditamt'=>0,
						'finalwt'=>0,
						'accounttypeid'=>$this->Basefunctions->singlefieldfetch('accounttypeid','accountid','account',$accountid)
					);
			}
		   echo json_encode($accountledgerresult);
	}
	// main maindeletecheck deletecheck check delete data
	public function checkdeletedata() {
		$povendormanagement = 0;
		$povendormanagement = $_POST['povendormanagement'];
		$salesid = $_POST['salesid'];
		$stocktypeid = 1;
		$takeoderstatus = 0;
		$estimatearray['rows'] = 0; 
		$salestransactiontypeid = $this->Basefunctions->singlefieldfetch('salestransactiontypeid','salesid','sales',$salesid);
		if($salestransactiontypeid == 20) {
			$this->db->select('salesdetailid');
			$this->db->from('salesdetail');
			$this->db->where('stocktypeid',75);
			$this->db->where('salesid',$salesid);
			$this->db->where('status',$this->Basefunctions->activestatus);
			$datatake = $this->db->get();
			if($datatake->num_rows() > 0) {
				$stocktypeid = 75;
				$takeoderstatus = 1;
			}else {
				$stocktypeid = 82;
				$takeoderstatus = 0;
			}
		} else if($salestransactiontypeid == 28) {
			$stocktypeid = 86;
		} else if($salestransactiontypeid == 21) {
			$stocktypeid = 76;
		} else if($salestransactiontypeid == 29) {
			$stocktypeid = 87;
		} else if($salestransactiontypeid == 30) {
			$stocktypeid = 88;
		}
		$this->db->select('GROUP_CONCAT(salesdetailid) as salesdetailid',false);
		$this->db->from('salesdetail');
		$this->db->where('stocktypeid',$stocktypeid);
		$this->db->where('salesid',$salesid);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$data = $this->db->get();
		if($data->num_rows() > 0) {
			$salesdetailid = $data->row()->salesdetailid;
			$salesdetailid = explode(',',$salesdetailid);
		}
		if($salestransactiontypeid == 20) {
			if($takeoderstatus == 1) { //Check Take order items check
				$this->db->select('salesdetailid');
				$this->db->from('salesdetail');
				$this->db->where('stocktypeid',76);
				$this->db->where_in('presalesdetailid',$salesdetailid);
				$this->db->where('status',$this->Basefunctions->activestatus);
				$data = $this->db->get();
				$estimatearray['rows'] = $data->num_rows(); 
				$estimatearray['id'] = $salestransactiontypeid;
			} else { // Receiver Order Items check
				$this->db->select('salesdetailid');
				$this->db->from('salesdetail');
				$this->db->where('stocktypeid',83);
				$this->db->where_in('presalesdetailid',$salesdetailid);
				$this->db->where('status',$this->Basefunctions->activestatus);
				$data = $this->db->get();
				$estimatearray['rows'] = $data->num_rows(); 
				$estimatearray['id'] = $salestransactiontypeid;
				if($data->num_rows() == 0) {
					$receiveorderconcept = $this->Basefunctions->get_company_settings('receiveorderconcept');
					if($receiveorderconcept == 1) {
						for($i=0; $i < count($salesdetailid) ;$i++) {
							$presalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$salesdetailid[$i]);
							$presalesdetailid_order = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$presalesdetailid);
							$receiveorderdata =$this->db->query('select itemtag.itemtagid from salesdetail left join lot on lot.salesdetailid = salesdetail.salesdetailid left join itemtag on itemtag.lotid = lot.lotid where stocktypeid in (75) and salesdetail.status = 1 and lot.status not in (0,3) and itemtag.status not in (0,3) and salesdetail.salesdetailid = '.$presalesdetailid_order.'');
							if($receiveorderdata->num_rows() > 0) {
								$estimatearray['rows'] = $receiveorderdata->num_rows(); 
								$estimatearray['id'] = $salestransactiontypeid;
								$estimatearray['cancelstatus'] = 0;
							}
						}
					}
				}
			}
		} else if($salestransactiontypeid == 28 || $salestransactiontypeid == 29 || $salestransactiontypeid == 30) { // Take Repair-Place Repair / Receive Repair / Delivery Repair
			if($salestransactiontypeid == 28) {
				$presentstocktypeid = '87';
			} else if($salestransactiontypeid == 29) {
				$presentstocktypeid = '88';
			} else if($salestransactiontypeid == 30) {
				$presentstocktypeid = '89';
			}
			$this->db->select('salesdetailid');
			$this->db->from('salesdetail');
			$this->db->where('stocktypeid',$presentstocktypeid);
			$this->db->where_in('presalesdetailid',$salesdetailid);
			$this->db->where('status',$this->Basefunctions->activestatus);
			$data = $this->db->get();
			$estimatearray['rows'] = $data->num_rows();
			$estimatearray['id'] = $salestransactiontypeid;
		} else if($salestransactiontypeid == 21) {
			if($povendormanagement == '1') {
				$this->db->select('salesdetailid');
				$this->db->from('salesdetail');
				$this->db->where('stocktypeid',76);
				$this->db->where_in('poorderstatusid',array(21,22));
				$this->db->where_in('salesdetailid',$salesdetailid);
				$this->db->where('status',$this->Basefunctions->activestatus);
				$data = $this->db->get();
				$estimatearray['rows'] = $data->num_rows(); 
				$estimatearray['id'] = $salestransactiontypeid;
			} else {
				$this->db->select('salesdetailid');
				$this->db->from('salesdetail');
				$this->db->where('stocktypeid',82);
				$this->db->where_in('presalesdetailid',$salesdetailid);
				$this->db->where('status',$this->Basefunctions->activestatus);
				$data = $this->db->get();
				$estimatearray['rows'] = $data->num_rows(); 
				$estimatearray['id'] = $salestransactiontypeid;
			}
		} else if($salestransactiontypeid == 9) {
			$datapurchase =$this->db->query('select itemtag.itemtagid from salesdetail left join lot on lot.salesdetailid = salesdetail.salesdetailid left join itemtag on itemtag.lotid = lot.lotid where stocktypeid in (17,80,81) and salesdetail.status = 1 and lot.status not in (0,3) and itemtag.status not in (0,3) and  salesdetail.salesid = '.$salesid.'');
			if($datapurchase->num_rows() > 0) {
				$estimatearray['rows'] = $datapurchase->num_rows(); 
				$estimatearray['id'] = $salestransactiontypeid;
			}else {
				$creditno = $this->Basefunctions->singlefieldfetch('creditno','salesid','sales',$salesid);
				$estimatearray['rows'] = $this->checkinaccountledgerforentry($creditno); 
				$estimatearray['id'] = $salestransactiontypeid;
			}
		}else if($salestransactiontypeid == 11) {
			$salesreturntagged = $this->checksalesreturntagged($salesid);
			$salesitemwithreturntagged = $this->checksalesitemwithreturntagged($salesid);
			if($salesreturntagged == '' && $salesitemwithreturntagged == '') {
				$creditno = $this->Basefunctions->singlefieldfetch('creditno','salesid','sales',$salesid);
				$estimatearray['rows'] = $this->checkinaccountledgerforentry($creditno); 
				$estimatearray['id'] = $salestransactiontypeid;
			} else {
				$estimatearray['rows'] = 1;
				$estimatearray['id'] = $salestransactiontypeid;
			}
		}else if($salestransactiontypeid == 18) {
			$itemtagid = '';
			$salesdetailid = '';
			$this->db->select('GROUP_CONCAT(itemtagid) as itemtagid,GROUP_CONCAT(salesdetailid) as salesdetailid',false);
			$this->db->from('salesdetail');
			$this->db->where('stocktypeid',62);
			$this->db->where('salesid',$salesid);
			$this->db->where('status',$this->Basefunctions->activestatus);
			$data = $this->db->get();
			if($data->num_rows() > 0){
				$itemtagid = $data->row()->itemtagid;
				$itemtagid = explode(',',$itemtagid);
				$salesdetailid = $data->row()->salesdetailid;
				$salesdetailid = explode(',',$salesdetailid);
			}
			$this->db->select('salesdetailid');
			$this->db->from('salesdetail');
			$this->db->where_in('stocktypeid',array(65,74));
			$this->db->where_in('itemtagid',$itemtagid);
			$this->db->where_in('presalesdetailid',$salesdetailid);
			$this->db->where('status',$this->Basefunctions->activestatus);
			$final = $this->db->get();
			$estimatearray['rows'] = $final->num_rows(); 
			$estimatearray['id'] = $salestransactiontypeid;
			$estimatearray['cancelstatus'] = 0;
		} else if($salestransactiontypeid == 27) {
			$deliverypomodeid = $this->Basefunctions->singlefieldfetch('deliverypomodeid','salesid','sales',$salesid);
			if($deliverypomodeid != 1) {
				$estimatearray['rows'] = 1;
				$estimatearray['id'] = '27';
			} else {
				$estimatearray['rows'] = 0;
				$estimatearray['cancelstatus'] = 0;
			}
		} else{
			$estimatearray['rows'] = 0; 
		}
		echo json_encode($estimatearray);
	}
	public function accountledgerinsert($salestransactiontypeid,$oldjewelamtsummary,$sumarypaidamount,$sumaryissueamount,$sumarypendingamount,$balpurewthidden,$issuereceiptid,$wtissuereceiptid,$salesid,$accountid,$salesnumber,$salesdate,$ctime,$ltime,$amountround,$weightround,$newsalesbillnumber,$advanceserialno,$balanceserialno) {
			// account ledger concept -sales,purchse,order
		if(in_array($salestransactiontypeid,array(9,11,20,22,23))) {
			$accountledger = 0;
			$summaryamt = 0;
			$summarywt = 0;
			$wtissue = 0;
			$wtreceipt = 0;
			$amtissue = 0;
			$amtreceipt = 0;
			$primfirstprefix = '';
			$primprefix = '';
			$advanceorbalance = '1';
			$sissuereceiptid = 3;
			$swtissuereceiptid = 3;
			if($salestransactiontypeid == 20) { // take order
				$summaryamt = $oldjewelamtsummary + $sumarypaidamount - $sumaryissueamount;
				if($summaryamt > 0) {
					$issuereceiptid = 2;
					$accountledger = 1;
					$advanceorbalance = '2';
				} else if($summaryamt < 0) {
					$issuereceiptid = 3;
					$accountledger = 1;
					$advanceorbalance = '3';
				}
			} else {
				if($sumarypendingamount > 0 || $sumarypendingamount < 0) {
					$accountledger = 1;
					$summaryamt = abs($sumarypendingamount);
				} else if($balpurewthidden > 0 || $balpurewthidden < 0) {
					$accountledger = 1;
					$summarywt = abs($balpurewthidden);
				}
			}
			if($accountledger == 1) {
				if($issuereceiptid ==2) // issue - advance with us so we put it under receiptamount
				{
					if($advanceserialno != '') {
						$advancenumber = $this->Basefunctions->serialnumbergeneratevardaan(52,'salesnumber','',$advanceserialno,$salesdate);
						$primprefix = $advancenumber[0];
					} else {
						$primprefix = 'A-';
					}
					$issuereceiptid = 3;
					$amtissue = 0;
					$amtreceipt = $summaryamt;
					$sissuereceiptid = 2;
					$advanceorbalance = '2';
				} else if($issuereceiptid ==3) // receipt - balance with us so we put it under issueamount
				{
					if($balanceserialno != '') {
						$balancenumber = $this->Basefunctions->serialnumbergeneratevardaan(52,'salesnumber','',$balanceserialno,$salesdate);
						$primprefix = $balancenumber[0];
					} else {
						$primprefix = 'B-';
					}
					$issuereceiptid = 2;
					$amtissue = $summaryamt;
					$amtreceipt = 0;
					$sissuereceiptid = 3;
					$advanceorbalance = '3';
				}
				
				if($wtissuereceiptid ==2) // issue
				{
					if($advanceserialno != '') {
						$advancenumber = $this->Basefunctions->serialnumbergeneratevardaan(52,'salesnumber','',$advanceserialno,$salesdate);
						$primprefix = $advancenumber[0];
					} else {
						$primprefix = 'A-';
					}
					$wtissuereceiptid = 3;
					$wtissue = 0;
					$wtreceipt = $summarywt;
					$swtissuereceiptid = 2;
					$advanceorbalance = '2';
				} else if($wtissuereceiptid ==3)
				{
					if($balanceserialno != '') {
						$balancenumber = $this->Basefunctions->serialnumbergeneratevardaan(52,'salesnumber','',$balanceserialno,$salesdate);
						$primprefix = $balancenumber[0];
					} else {
						$primprefix = 'B-';
					}
					$wtissuereceiptid = 2;
					$wtissue = $summarywt;
					$wtreceipt = 0;
					$swtissuereceiptid = 3;
					$advanceorbalance = '3';
				}
				
				 if($salestransactiontypeid == 11) //sales
				{
					//$primfirstprefix = 'S' ;
					$entrymode = 2;
					$paymentirtypeid = 2;
					if($summaryamt == 0){$sissuereceiptid = 3;}
					if($summarywt == 0){$swtissuereceiptid = 3;}
				}else if($salestransactiontypeid == 9) // purchase
				{
					//$primfirstprefix = 'P';
					$entrymode = 2;
					$paymentirtypeid = 4;
					if($summaryamt == 0){$sissuereceiptid = 2;}
					if($summarywt == 0){$swtissuereceiptid = 2;}
				}else if($salestransactiontypeid == 20) // order
				{
					//$primfirstprefix = 'O';
					$entrymode = 2;
					$paymentirtypeid = 3;
					if($summaryamt == 0){$sissuereceiptid = 3;}
					if($summarywt == 0){$swtissuereceiptid = 3;}
				}
				$amtissue = number_format((float)$amtissue, $amountround, '.', '');
				$amtreceipt = number_format((float)$amtreceipt, $amountround, '.', '');
				$wtreceipt = number_format((float)$wtreceipt, $weightround, '.', '');
				$wtissue = number_format((float)$wtissue, $weightround, '.', '');
				if($advanceserialno != '' || $balanceserialno != '') {
					$creditno = $primfirstprefix.$primprefix;
				} else {
					$creditno = $primfirstprefix.$primprefix.$newsalesbillnumber;
				}
				$insertaccountledgerdata = array(
									'salesid'=>$salesid,
									'accountid'=>$accountid,
									'entrymode'=>$entrymode,
									'creditno'=>$creditno,
									'referencenumber'=>$salesnumber,
									'salesdate'=>$salesdate,
									'issuereceiptid'=>$issuereceiptid,
									'weightissuereceiptid'=>$wtissuereceiptid,
									'paymentirtypeid'=>$paymentirtypeid,
									'amtissue'=>$amtissue,
									'amtreceipt'=>$amtreceipt,
									'weightreceipt'=>$wtreceipt,
									'weightissue'=>$wtissue
									
							);
					if($ctime > $ltime) {
							$insertaccountledgerdata = array_merge($insertaccountledgerdata,$this->Crudmodel->nextdaydefaultvalueget());
					} else {
							$insertaccountledgerdata = array_merge($insertaccountledgerdata,$this->Crudmodel->defaultvalueget());
					}
					$this->db->insert('accountledger',array_filter($insertaccountledgerdata));
					$update = array(
					'creditno'=>$creditno,
					'issuereceiptid'=>$sissuereceiptid,
					'wtissuereceiptid'=>$swtissuereceiptid,
					'advanceorbalance'=>$advanceorbalance
					);	
					$this->db->where('salesid',$salesid);
					$this->db->update('sales',$update);
					// credit no based balance entry
					if($creditno != '')
					{
						$this->creditnobalancesummary($salesid,1,$salesnumber,$accountid,$creditno,$paymentirtypeid);
					}
					// account no based balance summary
					$this->accountbalancesummary($salesid,1,$salesnumber,$accountid,$salesdate,$paymentirtypeid);
			}
		} else if($salestransactiontypeid == 25) { // Contra
			$amtreceipt = 0;
			$amtreceipt = number_format((float)$sumaryissueamount, $amountround, '.', '');
			$insertaccountledgerdata = array(
									'salesid'=>$salesid,
									'accountid'=>$accountid,
									'entrymode'=>2,
									'referencenumber'=>$salesnumber,
									'salesdate'=>$salesdate,
									'issuereceiptid'=>3,
									'paymentirtypeid'=>6,
									'amtreceipt'=>$amtreceipt,
									'status'=>$this->Basefunctions->activestatus
							);
			if($ctime > $ltime) {
					$insertaccountledgerdata = array_merge($insertaccountledgerdata,$this->Crudmodel->nextdaydefaultvalueget());
			} else {
					$insertaccountledgerdata = array_merge($insertaccountledgerdata,$this->Crudmodel->defaultvalueget());
			}
			$this->db->insert('accountledger',array_filter($insertaccountledgerdata));
			// account no based balance summary
			$this->accountbalancesummary($salesid,1,$salesnumber,$accountid,$salesdate,6);
		}
	}
	// check transaction type data
	public function checktransactiontypedata() {
		$salesid = $_POST['salesid'];
		$type = $_POST['type'];
		$data['status'] = 0;
		$salestransactiontypeid = $this->Basefunctions->singlefieldfetch('salestransactiontypeid','salesid','sales',$salesid);
		if($salestransactiontypeid == 11 && $type == 11) {
			$data['status'] = 1;
		}else if($salestransactiontypeid == 21 && $type == 21) {
			$data['status'] = 2;
		}else if($salestransactiontypeid == 27 && $type == 27) {
			$data['status'] = 3;
		}else if($salestransactiontypeid == 20 && $type == 20) {
			$this->db->select('salesdetailid');
			$this->db->from('salesdetail');
			$this->db->where('stocktypeid',75);
			$this->db->where('salesid',$salesid);
			$this->db->where('status',$this->Basefunctions->activestatus);
			$datatake = $this->db->get();
			if($datatake->num_rows() > 0) {
				$data['status'] = 1;
				$data['ordernumber'] = $this->Basefunctions->singlefieldfetch('salesnumber','salesid','sales',$salesid);;
			}else {
				$data['status'] = 0;
			}
		}
		echo json_encode($data);
	}
	public function loaditemdetail() {
		$salesid = $_POST['salesid'];
		$status = $_POST['status'];
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		if($status == 1 ) { // load normal sales item
			$this->db->select('salesdetail.salesdetailid,salesdetail.stocktypeid,stocktype.stocktypename,salesdetail.purityid,purity.purityname,salesdetail.productid,product.productname,ROUND(salesdetail.grossweight,'.$round.') as grossweight,salesdetail.itemtagid,itemtag.itemtagnumber,salesdetail.deliverystatusid,deliverystatus.deliverystatusname');
				$this->db->from('sales');
				$this->db->join('salesdetail','salesdetail.salesid = sales.salesid','left');
				$this->db->join('stocktype','stocktype.stocktypeid = salesdetail.stocktypeid','left');
				$this->db->join('purity','purity.purityid = salesdetail.purityid','left');
				$this->db->join('product','product.productid = salesdetail.productid','left');
				$this->db->join('itemtag','itemtag.itemtagid = salesdetail.itemtagid','left');
				$this->db->join('deliverystatus','deliverystatus.deliverystatusid = salesdetail.deliverystatusid','left');
				$this->db->where_not_in('sales.status',array(0,3));
				$this->db->where_in('deliverystatus.deliverystatusid',array(2,4));
				$this->db->where_in('salesdetail.stocktypeid',array(11,12,13,74,83));
				$this->db->where('sales.salesid',$salesid);
				$this->db->where_not_in('salesdetail.status',array(0,3));
				$salesdata=$this->db->get();
				if($salesdata->num_rows()>0) {
				  foreach($salesdata->result() as $info) {
					  if(empty($info->itemtagnumber)) {
						  $itemtagnumber = '';
					  }else {
						  $itemtagnumber = $info->itemtagnumber;
					  }
					$result[] = array(
								'salesdetailid'=>$info->salesdetailid,
								'stocktypeid'=>$info->stocktypeid,
								'stocktypename'=>$info->stocktypename,
								'purityid'=>$info->purityid,
								'purityname'=>$info->purityname,
								'productid'=>$info->productid,
								'productname'=>$info->productname,
								'grossweight'=>$info->grossweight,
								'itemtagnumber'=>$itemtagnumber,
								'deliverystatusid'=>$info->deliverystatusid,
								'deliverystatusname'=>$info->deliverystatusname
								);
				  }
				}else{
					$result = '';
				}
		}else if($status == 2) { // load take order item
			$this->db->select('salesdetail.salesdetailid,salesdetail.purityid,purity.purityname,salesdetail.productid,product.productname,ROUND(salesdetail.grossweight,'.$round.') as grossweight,salesdetail.orderstatusid,orderstatus.orderstatusname');
				$this->db->from('sales');
				$this->db->join('salesdetail','salesdetail.salesid = sales.salesid','left');
				$this->db->join('purity','purity.purityid = salesdetail.purityid','left');
				$this->db->join('product','product.productid = salesdetail.productid','left');
				$this->db->join('orderstatus','orderstatus.orderstatusid = salesdetail.orderstatusid','left');
				$this->db->where('salesdetail.stocktypeid',75);
				$this->db->where_not_in('salesdetail.orderstatusid',array(4));
				$this->db->where('sales.salesid',$salesid);
				$this->db->where_not_in('salesdetail.status',array(0,3));
				$salesdata=$this->db->get();
				if($salesdata->num_rows()>0) {
				  foreach($salesdata->result() as $info) {
					  	$result[] = array(
								'salesdetailid'=>$info->salesdetailid,
								'purityid'=>$info->purityid,
								'purityname'=>$info->purityname,
								'productid'=>$info->productid,
								'productname'=>$info->productname,
								'grossweight'=>$info->grossweight,
								'orderstatusid'=>$info->orderstatusid,
								'orderstatusname'=>$info->orderstatusname
								);
				  }
				}else{
					$result = '';
				}
		}
			echo json_encode($result);
	}
	/*	counter_groupdropdown - delivery */
	public function counter_groupdropdown_delivery() {
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('countername,counterid,parentcounterid,parentcounterid,counterdefault,counterdefaultid,maxquantity,countertypeid');
		$this->db->from('counter');
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->where('storagetypeid',2);
		$this->db->where('counterdefaultid',14);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$this->db->order_by('counterid','asc');
		$info = $this->db->get();
		return $info;
	}
	//push to pending data into itemtag table
	public function pushtopendingdeliverycreate() {
		$salesdetailid = $_POST['itemdetail'];
		$newstatusid = $_POST['newstatusid'];
		$stoneitemtagid = '1';
		if($newstatusid == 2) { // Pending status
			$tocounter = $_POST['tocounter'];
			$tagtemplateid = $_POST['tagtemplateid'];
			$description = $_POST['description'];
			if(isset($_POST['deliveryrfidtagno'])){
				$_POST['deliveryrfidtagno'] = $_POST['deliveryrfidtagno'];
			}else {
				$_POST['deliveryrfidtagno'] = '';
			}
			$this->db->select('salesdetail.salesid,salesdetail.salesdetailid,salesdetail.stocktypeid,salesdetail.purityid,salesdetail.productid,salesdetail.grossweight,salesdetail.itemtagid,salesdetail.stoneweight,salesdetail.netweight,salesdetail.pieces,salesdetail.counterid,salesdetail.netweight,itemtag.itemtagnumber,salesdetail.orderitemsize');
			$this->db->from('salesdetail');
			$this->db->join('itemtag','itemtag.itemtagid = salesdetail.itemtagid','left');
			$this->db->where('salesdetail.salesdetailid',$salesdetailid);
			$salesdata=$this->db->get();
			$tagno = $this->Basefunctions->varrandomnumbergenerator(50,'itemtagnumber',2,''); //serialnumber
			if($salesdata->num_rows()>0) {
				foreach($salesdata->result() as $info) {
					$insert = array(
							'itemtagnumber' =>$tagno,
							'tagdate' => date("Y-m-d"),
							'tagtypeid' => 2,
							'branchid'=>$this->Basefunctions->branchid,
							'purityid' => $info->purityid,						
							'productid' =>$info->productid,
							'grossweight' => $info->grossweight,		
							'stoneweight' => $info->stoneweight,		
							'netweight' => $info->netweight,				
							'pieces' => $info->pieces,					
							'size' => $info->orderitemsize,
							'counterid' =>$tocounter ,	
							'stockincounterid' =>$tocounter,									
							'description' =>$description,					
							'tagtemplateid' =>$tagtemplateid,								
							'status'=>$this->Basefunctions->activestatus,
							'transfertagno'=>$info->itemtagnumber,
							'tagentrytypeid' =>7,
							'rfidtagno'=>$_POST['deliveryrfidtagno'],
							'salesid'=>$info->salesid,
							'salesdetailid'=>$info->salesdetailid,
							'industryid'=>$this->Basefunctions->industryid,
							'createdate' => date($this->Basefunctions->datef),
							'lastupdatedate' => date($this->Basefunctions->datef),
							'createuserid' => $this->Basefunctions->userid,
							'lastupdateuserid' => $this->Basefunctions->userid,
							'status'=>17
							);
					$this->db->insert('itemtag',array_filter($insert));
					$stoneitemtagid = $this->db->insert_id();
				}
			}
			/** Insert stoneentry table if the item is having stone details**/
			$this->db->select('salesdetail.salesid,salesdetail.salesdetailid,salesstoneentry.salesstoneentryid,salesstoneentry.salesstoneentryorder,salesstoneentry.stoneid,salesstoneentry.stonetypeid,salesstoneentry.purchaserate,salesstoneentry.stonerate,salesstoneentry.pieces,salesstoneentry.caratweight,salesstoneentry.gram,salesstoneentry.totalcaratweight,salesstoneentry.totalnetweight,salesstoneentry.totalamount,salesstoneentry.stoneentrycalctypeid');
			$this->db->from('salesstoneentry');
			$this->db->join('salesdetail','salesdetail.salesdetailid = salesstoneentry.salesdetailid','left');
			$this->db->where_in('salesstoneentry.salesdetailid',$salesdetailid);
			$salesstonedata=$this->db->get();
			if($salesstonedata->num_rows()>0) {
				foreach($salesstonedata->result() as $info) {
					$insert = array(
						'stoneentryorder' =>$info->salesstoneentryorder,
						'itemtagid' =>$stoneitemtagid,
						'stoneid' =>$info->stoneid,
						'stonetypeid' =>$info->stonetypeid,
						'purchaserate' =>$info->purchaserate,
						'stonerate' =>$info->stonerate,
						'pieces' =>$info->pieces,
						'caratweight' =>$info->caratweight,
						'gram' =>$info->totalnetweight,
						'totalcaratweight' =>$info->totalcaratweight,
						'totalnetweight' =>$info->totalnetweight,
						'totalamount' =>$info->totalamount,
						'stoneentrycalctypeid' =>$info->stoneentrycalctypeid,
						'createdate' => date($this->Basefunctions->datef),
						'lastupdatedate' => date($this->Basefunctions->datef),
						'createuserid' => $this->Basefunctions->userid,
						'lastupdateuserid' => $this->Basefunctions->userid,
						'status'=>1
					);
					$this->db->insert('stoneentry',array_filter($insert));
				}
			}
			//audit-log
			$userid = $this->Basefunctions->logemployeeid;
			$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
			$activity = ''.$user.' updated Sales item as Pending - '.$tagno.'';
			$this->Basefunctions->notificationcontentadd($salesdetailid,'Updated',$activity ,$userid,52);
		}else if($newstatusid == 4) { // Delivered status
			$this->db->select('itemtag.itemtagid');
			$this->db->from('itemtag');
			$this->db->where('itemtag.tagentrytypeid',7);
			$this->db->where('itemtag.salesdetailid',$salesdetailid);
			$this->db->where('itemtag.status',17);
			$itemtagdata=$this->db->get();
			$itemtagid = $itemtagdata->row()->itemtagid;
			if($itemtagdata->num_rows()>0) {
				$updateitemtag=array(
							'status' =>$this->Basefunctions->deletestatus,
							'lastupdatedate' => date($this->Basefunctions->datef),
							'lastupdateuserid' => $this->Basefunctions->userid
							);
				$this->db->where('itemtagid',$itemtagid);
				$this->db->update('itemtag',$updateitemtag);
				//audit-log
				$userid = $this->Basefunctions->logemployeeid;
				$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
				$itemtagnumber = $this->Basefunctions->singlefieldfetch('itemtagnumber','itemtagid','itemtag',$itemtagid);
				$activity = ''.$user.' updated Pending item to Delivered - '.$itemtagnumber.'';
				$this->Basefunctions->notificationcontentadd($itemtagid,'Updated',$activity ,$userid,52);
			}
		}
		$uptag=array(
					'deliverystatusid' =>$newstatusid,
					'lastupdatedate' => date($this->Basefunctions->datef),
					'lastupdateuserid' => $this->Basefunctions->userid
					);
		$this->db->where('salesdetailid',$salesdetailid);
		$this->db->update('salesdetail',$uptag);
		echo 'SUCCESS';		
	}
	/*	delivery status data*/
	public function deliverystatus() {
		$this->db->select('status,statusname');
		$this->db->from('status');
		$this->db->where_in('status',array(18,19));
		$this->db->order_by('status','asc');
		$info = $this->db->get();
		return $info;
	}
	public function checkinaccountledgerforentry($creditno) {
		$this->db->select('accountledgerid');
		$this->db->from('accountledger');
		$this->db->where_not_in('entrymode',array(5,4,2)); 
		$this->db->where('creditno',$creditno);
		$this->db->where('creditno !=','');
		$this->db->where('status',$this->Basefunctions->activestatus);
		$data = $this->db->get();
		return $data->num_rows(); 
	}
	public function loaditemtag() {
		$statusid = $_POST['statusid'];
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$this->db->select('itemtag.itemtagid,itemtag.itemtagnumber,itemtag.purityid,itemtag.productid,ROUND(itemtag.grossweight,'.$round.') as grossweight,purity.purityname,product.productname');
		$this->db->from('itemtag');
		$this->db->join('purity','purity.purityid = itemtag.purityid','left');
		$this->db->join('product','product.productid = itemtag.productid','left');
		if($statusid == 1) {
			$this->db->where('itemtag.status',17);
		}else if($statusid == 2) {
			$this->db->where_in('itemtag.status',array(18,19));
		}
		$itemtagdata=$this->db->get();
		if($itemtagdata->num_rows()>0) {
		  foreach($itemtagdata->result() as $info) {
			  $result[] = array(
						'itemtagid'=>$info->itemtagid,
						'purityid'=>$info->purityid,
						'purityname'=>$info->purityname,
						'productid'=>$info->productid,
						'productname'=>$info->productname,
						'grossweight'=>$info->grossweight,
						'itemtagnumber'=>$info->itemtagnumber
						);
		  }
		}else{
			$result = '';
		}
		echo json_encode($result);
	}
	// delivery status update
	public function pushtodeliveryupdate() {
		$process = $_POST['process'];
		$itemtagid = $_POST['pendingitemdetail'];
		$salesdeliverystatus = 2;
		if($process == 1) {
			$deliverystatusid = $_POST['deliverystatusid'];
			$deliverycomment = $_POST['deliverycomment'];
			if($deliverystatusid == 18){$salesdeliverystatus = 3;}else if($deliverystatusid == 19){$salesdeliverystatus = 4;}
			
		}else if($process == 2) {
			$deliverystatusid = 17;
			$deliverycomment = '';
			$salesdeliverystatus = 2;
		}
		$updateitemtag=array(
					'status' =>$deliverystatusid,
					'deliverycomment' =>$deliverycomment,
					'lastupdatedate' => date($this->Basefunctions->datef),
					'lastupdateuserid' => $this->Basefunctions->userid
					);
		$this->db->where('itemtagid',$itemtagid);
		$this->db->update('itemtag',$updateitemtag);
		$salesdetailid = $this->Basefunctions->singlefieldfetchinactive('salesdetailid','itemtagid','itemtag',$itemtagid);
		$uptag=array(
					'deliverystatusid' =>$salesdeliverystatus,
					'lastupdatedate' => date($this->Basefunctions->datef),
					'lastupdateuserid' => $this->Basefunctions->userid
					);
		$this->db->where('salesdetailid',$salesdetailid);
		$this->db->update('salesdetail',$uptag);
		echo 'SUCCESS';
	}
	// Place order overlay status update
	public function postatusoverlaysubmitform() {
		$salesid = $_POST['posalesid'];
		$postatus = $_POST['orderitemsstatusacc'];
		$statuscomment = $_POST['orderitemsstatuscomment'];
		$adminstatuscomment = $_POST['adminorderitemsstatuscomment'];
		$ordertransactionpage = $_POST['ordertransactionpage'];
		$salesdetailid = $_POST['salesdetailid'];
		$salesdetailid = explode(",",$salesdetailid);
		if($postatus == 0) { //Accepted Place Order
			if($ordertransactionpage == 'placeorder') {
				$salespostatus = 21;
				$status = $this->Basefunctions->activestatus;
			} else if($ordertransactionpage == 'generatepo') {
				$salespostatus = 24;
				$status = $this->Basefunctions->activestatus;
			}
		} else if ($postatus == 1) { // Rejected section
			if($ordertransactionpage == 'placeorder') { // cancel place order due to vendor rejected the place order
				$salespostatus = 22;
				$status = 1;
			} else if($ordertransactionpage == 'generatepo') {  // Rejected Place Order by Vendor
				$salespostatus = 25;
				$status = 1; // cancel by jewelry person due to rejected
			}
			//retrieve salesdetailid
			$salesdetaildata = $this->db->select('stocktypeid,salesdetailid,itemtagid,referencenumber,chitbookno,presalesdetailid')
								->from('salesdetail')
								->where('salesdetail.salesid',$salesid)
								->where_in('salesdetail.salesdetailid',$salesdetailid)
								->where('salesdetail.status',$this->Basefunctions->activestatus)
								->get();
			foreach($salesdetaildata->result() as $info) {
				$sales_detail[] = array(
						'stocktypeid'=>$info->stocktypeid,
						'salesdetailid'=>$info->salesdetailid,
						'itemtagid'=>$info->itemtagid,
						'referencenumber'=>$info->referencenumber,
						'chitbookno'=>$info->chitbookno,
						'presalesdetailid'=>$info->presalesdetailid
				);
			}
			$salestransactiontype = $this->Basefunctions->singlefieldfetch('salestransactiontypeid','salesid','sales',$salesid);
			for($i=0; $i < count($sales_detail) ;$i++) {
				if($salestransactiontype == '21') { // place order
					$orderstatusid = $this->Basefunctions->singlefieldfetch('orderstatusid','salesdetailid','salesdetail',$sales_detail[$i]['presalesdetailid']);
					if($orderstatusid != 6) { // except cancel order
						$this->revertorderstatus(2,3,$sales_detail[$i]['salesdetailid']);
						$presalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$sales_detail[$i]['salesdetailid']);
						$this->revertorderstatus(2,3,$presalesdetailid);
					}
				} else if($salestransactiontype == '27') {
					if($postatus == 1 || $postatus == 2) {  // generate po status as 1
						for($i=0; $i < count($sales_detail) ;$i++) {
							$generatepoupdate = array(
								'generatepo' => '1',
							);
							$generatepoupdate = array_merge($generatepoupdate,$this->Crudmodel->updatedefaultvalueget());
							$this->db->where_in('salesdetailid',$sales_detail[$i]['presalesdetailid']);
							$this->db->update('salesdetail',$generatepoupdate);
						}
					}
				}
			}
		} else if ($postatus == 2) { // It goes for Customer review
			$salespostatus = 23;
			$status = $this->Basefunctions->activestatus;
		}
		if($ordertransactionpage == 'placeorder') {
			$salesdet_array = array(
				'poorderitemdate' => date("Y-m-d", strtotime($_POST['orderitemdate'])),
				'poorderstatusid' => $salespostatus,
				'postatuscomment' => $statuscomment,
				'poadminstatuscomment' => $adminstatuscomment,
				'lastupdatedate' => date($this->Basefunctions->datef),
				'lastupdateuserid' => $this->Basefunctions->userid,
				'status' => $status
				);
		} else if($ordertransactionpage == 'generatepo') {
			if($postatus == 1) {
				$salesdata_array = array(
					'deliverypodate' => date("Y-m-d", strtotime(0000-00-00)),
					'deliverypomodeid' => '1',
					'delvendorname' => '',
					'delcouriername' => '',
					'delemployeeid' => '1',
					'delotp' => '',
					'deliverypocomment' => '',
					'delvstatus' => '1',
					'lastupdatedate' => date($this->Basefunctions->datef),
					'lastupdateuserid' => $this->Basefunctions->userid,
					'status' => '5'
					);
				$salesdata_array = array_merge($salesdata_array,$this->Crudmodel->updatedefaultvalueget());
				$this->db->where_in('salesid',$salesid);
				$this->db->update('sales',$salesdata_array);
			} else if($postatus == 0) {
				for($i=0 ; $i < count($salesdetailid); $i++) {
					$posalesdetailid = $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$salesdetailid[$i]);
					$posalesid = $this->Basefunctions->singlefieldfetch('salesid','salesdetailid','salesdetail',$posalesdetailid);
					$podelorderstatusid = array(
						'delorderstatusid' => $salespostatus
					);
					$podelorderstatusid = array_merge($podelorderstatusid,$this->Crudmodel->updatedefaultvalueget());
					$this->db->where_in('salesdetailid',$posalesdetailid);
					$this->db->where_in('salesid',$posalesid);
					$this->db->update('salesdetail',$podelorderstatusid);
				}
			}
			$salesdet_array = array(
				'delorderitemdate' => date("Y-m-d", strtotime($_POST['orderitemdate'])),
				'delorderstatusid' => $salespostatus,
				'delstatuscomment' => $statuscomment,
				'lastupdatedate' => date($this->Basefunctions->datef),
				'lastupdateuserid' => $this->Basefunctions->userid,
				'status' => $status
			);
		}
		$salesdet_array = array_merge($salesdet_array,$this->Crudmodel->updatedefaultvalueget());
		$this->db->where_in('salesdetailid',$salesdetailid);
		$this->db->where_in('salesid',$salesid);
		$this->db->update('salesdetail',$salesdet_array);
		echo 'SUCCESS';
	}
	//Delivery Place order has been updated in Gen.Place Order Transaction type.
	public function deliverypooverlaysave() {
		$salesid = $_POST['salesid'];
		if(isset($_POST['delvyorderstatusacc'])) {
			$_POST['delvyorderstatusacc'] = $_POST['delvyorderstatusacc'];
		} else {
			$_POST['delvyorderstatusacc'] = 1;
		}
		if($salesid != '1') {
			if($_POST['delvyorderstatusacc'] == 1){
				if(isset($_POST['delemployeeid'])) {
					if($_POST['delemployeeid'] == '') {
						$_POST['delemployeeid'] = 1;
					} else {
						$_POST['delemployeeid'] = $_POST['delemployeeid'];
					}
				} else {
					$_POST['delemployeeid'] = 1;
				}
				$deliverpodata = array(
									'deliverypodate' => date("Y-m-d", strtotime($_POST['deliverypodate'])),
									'deliverypomodeid' => $_POST['deliverypomodeid'],
									'delvendorname' => $_POST['delvendorname'],
									'delemployeeid' => $_POST['delemployeeid'],
									'delcouriername' => $_POST['delcouriername'],
									'delotp' => $_POST['delotp'],
									'deliverypocomment' => $_POST['deliverypocomment'],
									'delvstatus' => $_POST['delvyorderstatusacc'],
									);
				$deliverpodata = array_merge($deliverpodata,$this->Crudmodel->updatedefaultvalueget());
				$this->db->where('salesid',$salesid);
				$this->db->update('sales',$deliverpodata);
				echo 'SUCCESS';
			} else {
				if($_POST['delvyorderstatusacc'] == 0) {
					$delstatus = 24;
				} else if($_POST['delvyorderstatusacc'] == 1) {
					$delstatus = 25;
				}
				$deliverpodata = array(
									'delvstatus' => $delstatus,
									);
				$deliverpodata = array_merge($deliverpodata,$this->Crudmodel->updatedefaultvalueget());
				$this->db->where('salesid',$salesid);
				$this->db->update('sales',$deliverpodata);
				echo 'SUCCESS';
			}
		} else {
			echo 'FAIL';
		}
	}
	// cancel order status update
	public function cancelorderupdate() {
		$salesdetailid = $_POST['cancelitemdetail'];
		$ordernumber = $_POST['cancelordernumber'];
		$comment = $_POST['cancelcomment'];
		$newcomment = '';
		$orderstatusid = $this->Basefunctions->singlefieldfetch('orderstatusid','salesdetailid','salesdetail',$salesdetailid);
		if(!empty($comment)) {
			$precomment = $this->Basefunctions->singlefieldfetch('comment','salesdetailid','salesdetail',$salesdetailid);
			if(!empty($precomment)) {
				$precomment = preg_replace("(<co>)", " ", $precomment);
			}else {
				$precomment = '';
			}
			$newcomment .= $precomment;
			$newcomment .= '<co>';
			$newcomment .= $comment;
			$newcomment .= '<co>';
			$commentarray =array(
				'comment'=>$newcomment
			);
			$this->db->where('salesdetailid',$salesdetailid);
			$this->db->update('salesdetail',$commentarray);
		}
		// updating order status as cancel
		if($orderstatusid == 2) { // Take order
			$this->revertorderstatus(6,$orderstatusid,$salesdetailid);
		}else if($orderstatusid == 3) { // Place order
			$this->revertorderstatus(6,$orderstatusid,$salesdetailid);
			$presalesdetailid = $this->Basefunctions->singlefieldfetch('salesdetailid','presalesdetailid','salesdetail',$salesdetailid);
			$this->revertorderstatus(6,$orderstatusid,$presalesdetailid);
		}else if($orderstatusid == 5) { // Receive order
			$this->revertorderstatus(6,$orderstatusid,$salesdetailid);
			$presalesdetailid = $this->Basefunctions->singlefieldfetch('salesdetailid','presalesdetailid','salesdetail',$salesdetailid);
			$this->revertorderstatus(6,$orderstatusid,$presalesdetailid);
			$ordersalesdetailid = $this->Basefunctions->singlefieldfetch('salesdetailid','presalesdetailid','salesdetail',$presalesdetailid);
			$this->revertorderstatus(6,$orderstatusid,$ordersalesdetailid);
			$this->orderitemtagupdate($ordernumber,$ordersalesdetailid,0);
		}else if($orderstatusid == 6) { // Cancelled order - revert the order status
			$presalesdetailid = $this->Basefunctions->singlefieldfetch('salesdetailid','presalesdetailid','salesdetail',$salesdetailid);
			$ordersalesdetailid = $this->Basefunctions->singlefieldfetch('salesdetailid','presalesdetailid','salesdetail',$presalesdetailid);
			if($ordersalesdetailid != 1 && $presalesdetailid != 1) { // receive order
				$this->revertorderstatus(5,$orderstatusid,$salesdetailid);
				$this->revertorderstatus(5,$orderstatusid,$presalesdetailid);
				$this->revertorderstatus(5,$orderstatusid,$ordersalesdetailid);
				$this->orderitemtagupdate($ordernumber,$ordersalesdetailid,1);
			}else if($presalesdetailid != 1) { // place order
				$this->revertorderstatus(3,$orderstatusid,$salesdetailid);
				$this->revertorderstatus(3,$orderstatusid,$presalesdetailid);
			}else { // take order
				$this->revertorderstatus(2,$orderstatusid,$salesdetailid);
			}
		}
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$salesid = $this->Basefunctions->singlefieldfetch('salesid','salesdetailid','salesdetail',$salesdetailid);
		$salesbillno = $this->Basefunctions->singlefieldfetch('salesnumber','salesid','sales',$salesid);
		$activity = ''.$user.' Cancelled Order - '.$salesbillno.'';
		$this->Basefunctions->notificationcontentadd($salesdetailid,'Updated',$activity ,$userid,52);
		echo 'SUCCESS';
	}
	// order data update in itemtag table
	public function orderitemtagupdate($ordernumber,$ordersalesdetailid,$status) {
		$itemtagid = $this->Basefunctions->singlefieldfetch('itemtagid','salesdetailid','salesdetail',$ordersalesdetailid);
		if($status == 0) {
			$updateitemtag=array(
					'ordernumber'=>'',
					'salesdetailid'=>1,
					'orderstatus'=>'No'
					);
		}else if($status == 1) {
			$updateitemtag=array(
					'ordernumber'=>$ordernumber,
					'salesdetailid'=>$ordersalesdetailid,
					'orderstatus'=>'Yes'
					);
		}
		$this->db->where('itemtagid',$itemtagid);
		$this->db->update('itemtag',$updateitemtag);
	}
	//json convert data
	public function jsonconvertdata() {
		$getgriddata = json_decode($_POST['datas']);
		$creditdetail='';
		$j=1;
		foreach($getgriddata as $value){
			$creditdetail->rows[$j]['id']=$j;
			$creditdetail->rows[$j]['cell']=array(
					$value->overlaycreditno,
					$value->creditsalesdate,
					$value->paymentirtypeid,
					$value->creditissuereceiptname,
					$value->amtissue,
					$value->amtreceipt,
					$value->finalamtissue,
					$value->finalamtreceipt,
					$value->creditissuereceiptid,
					$value->creditpaymentirtypeid,
					$value->creditweightissuereceiptname,
					$value->weightissue,
					$value->weightreceipt,
					$value->finalweightissue,
					$value->finalweightreceipt,
					$value->creditweightissuereceiptid,
					$value->accountledgerid,
					$value->processid,
					$value->process,
					$value->creditreference,
					$value->creditreferencename,
					$value->finalamthidden,
                    $value->finalwthidden,
					$value->mainfinalamthidden,
                    $value->mainfinalwthidden,
                    $value->editoverlaystatus
			);
			$j++;
		}
		echo  json_encode($creditdetail);	
	}
	// account ledger delete data
	 public function accountledgerdelete($salesid) {
		 $delete = $this->Basefunctions->delete_log();
		 $salesdata = $this->db->select('accountledgerid,accountid,referencenumber,creditno,salesdate,paymentirtypeid')
					->from('accountledger')
					->where('accountledger.salesid',$salesid)
					->where_in('accountledger.entrymode',array(2,3,6,7,8))
					->where('accountledger.status',$this->Basefunctions->activestatus)
					->get();
					if($salesdata->num_rows()>0){
						foreach($salesdata->result() as $salesinfo){
							$ledgeraccountid = $salesinfo->accountid;
							$referencenumber = $salesinfo->referencenumber;
							$creditno = $salesinfo->creditno;
							$salesdate = $salesinfo->salesdate;
							$ledgerpaymentirtypeid = $salesinfo->paymentirtypeid;
							// delete accountledger entry
							$this->db->where('accountledgerid',$salesinfo->accountledgerid);
							$this->db->update('accountledger',$delete);
							// credit no based balance entry
							if($creditno != '')
							{
								$this->creditnobalancesummary($salesid,1,$referencenumber,$ledgeraccountid,$creditno,$ledgerpaymentirtypeid,1);
							}
							// account no based balance summary
							$this->accountbalancesummary($salesid,1,$referencenumber,$ledgeraccountid,$salesdate,$ledgerpaymentirtypeid);
						}
					}
	}
	/*	category  */
	public function category_dropdown() {
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('categoryname,categoryid');
		$this->db->from('category');
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->where('categoryid >',10);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$this->db->order_by('categoryid','asc');
		$info = $this->db->get();
		return $info;
	}
	//stone json convert data
	public function stonejsonconvertdata() {
		$getgriddata = json_decode($_POST['datas']);
		$stonedetail='';
		$j=1;
		foreach($getgriddata as $value) {
			$stonedetail->rows[$j]['id'] = $j;
			$stonedetail->rows[$j]['cell']=array(
					$value->stonegroupid,
					$value->stonegroupname,
					$value->stoneid,
					$value->stonename,
					$value->purchaserate,
					$value->basicrate,
					$value->stoneentrycalctypeid,
					$value->stoneentrycalctypename,
					$value->stonepieces,
					$value->caratweight,
					$value->stonegram,
					$value->totalcaratweight,
					$value->totalweight,
					$value->stonetotalamount
				);
			$j++;
		}
		echo  json_encode($stonedetail);	
	}
	// If the credit No is blank then use salesnumber as credit no
	public function crosscheckcreditnoisblank($creditoverlayhidden,$salesnumber) {
		$getgriddata = json_decode($creditoverlayhidden);
		$creditdetail='';
		$j=1;
		foreach($getgriddata as $value) {
			if($value->overlaycreditno != '') {
				$value->overlaycreditno = $value->overlaycreditno;
			} else {
				$value->overlaycreditno = $salesnumber;
			}
			$creditdetail->rows[$j]['id']=$j;
			$creditdetail->rows[$j]['cell']=array(
					$value->overlaycreditno,
					$value->creditsalesdate,
					$value->paymentirtypeid,
					$value->creditissuereceiptname,
					$value->amtissue,
					$value->amtreceipt,
					$value->finalamtissue,
					$value->finalamtreceipt,
					$value->creditissuereceiptid,
					$value->creditpaymentirtypeid,
					$value->creditweightissuereceiptname,
					$value->weightissue,
					$value->weightreceipt,
					$value->finalweightissue,
					$value->finalweightreceipt,
					$value->creditweightissuereceiptid,
					$value->accountledgerid,
					$value->processid,
					$value->process,
					$value->creditreference,
					$value->creditreferencename,
					$value->finalamthidden,
                    $value->finalwthidden,
					$value->mainfinalamthidden,
                    $value->mainfinalwthidden,
                    $value->editoverlaystatus
			);
			$j++;
		}
		return  json_encode($creditdetail);
	}
	// If the credit No is blank then use salesnumber as credit no
	public function crosscheckcreditnoisblankjson($creditoverlayhidden,$salesnumber) {
		$getgriddata = json_decode($creditoverlayhidden);
		$creditdetail = array();
		$j=1;
		foreach($getgriddata as $value){
			if($value->overlaycreditno != '') {
				$value->overlaycreditno = $value->overlaycreditno;
			} else {
				$value->overlaycreditno = $salesnumber;
			}
			$creditdetail[] = array(
					'overlaycreditno'=>$value->overlaycreditno,
					'creditsalesdate'=>$value->creditsalesdate,
					'paymentirtypeid'=>$value->paymentirtypeid,
					'creditissuereceiptname'=>$value->creditissuereceiptname,
					'amtissue'=>$value->amtissue,
					'amtreceipt'=>$value->amtreceipt,
					'finalamtissue'=>$value->finalamtissue,
					'finalamtreceipt'=>$value->finalamtreceipt,
					'creditissuereceiptid'=>$value->creditissuereceiptid,
					'creditpaymentirtypeid'=>$value->creditpaymentirtypeid,
					'creditweightissuereceiptname'=>$value->creditweightissuereceiptname,
					'weightissue'=>$value->weightissue,
					'weightreceipt'=>$value->weightreceipt,
					'finalweightissue'=>$value->finalweightissue,
					'finalweightreceipt'=>$value->finalweightreceipt,
					'creditweightissuereceiptid'=>$value->creditweightissuereceiptid,
					'accountledgerid'=>$value->accountledgerid,
					'processid'=>$value->processid,
					'process'=>$value->process,
					'creditreference'=>$value->creditreference,
					'creditreferencename'=>$value->creditreferencename,
					'finalamthidden'=>$value->finalamthidden,
                    'finalwthidden'=>$value->finalwthidden,
					'mainfinalamthidden'=>$value->mainfinalamthidden,
                    'mainfinalwthidden'=>$value->mainfinalwthidden,
                    'editoverlaystatus'=>$value->editoverlaystatus
			);
			$j++;
		}
		return  json_encode($creditdetail);
	}
	public function getbulktagnumberdatamodel() {
		$innergridtagids = $_GET['innergridtagids'];
		$explodeinnergridtagids = explode(',',$innergridtagids);
		$salestransactiontypeid = $_GET['salestransactiontypeid'];
		$stocktypeid = $_GET['stocktypeid'];
		$approvalnumber = $_GET['approvalnumber'];
		$rfiddeviceno = $_GET['rfiddeviceno'];
		$salesid = 1;
		if($approvalnumber != 'all' && $stocktypeid == 65){
			$salesid = $this->Basefunctions->singlefieldfetch('salesid','salesnumber','sales',$approvalnumber);
		}
		$this->db->select('tblrfid.rfid,itemtag.itemtagid,itemtag.itemtagnumber');
		$this->db->from('tblrfid');
		$this->db->join('rfidref','rfidref.tagserialno=tblrfid.rfid');
		$this->db->join('itemtag','itemtag.rfidtagno=rfidref.tagno');
		if($stocktypeid == 65)
		{
			if($approvalnumber != 'all'){
				$this->db->where('itemtag.salesid',$salesid);
			}else if($approvalnumber == 'all'){ 
				$this->db->join('sales','itemtag.salesid=sales.salesid');
				$this->db->where('sales.accountid', $_GET['accountid']);
			}
			$this->db->where('itemtag.status',15);
			$this->db->where_not_in('itemtag.itemtagid',$explodeinnergridtagids);
			
		}else if($stocktypeid == 62){
			$this->db->where('itemtag.status',1);
			$this->db->where_not_in('itemtag.itemtagid',$explodeinnergridtagids);
		}else if($stocktypeid == 11){
			$this->db->where_in('itemtag.status',array('1','15'));
			$this->db->where_not_in('itemtag.itemtagid',$explodeinnergridtagids);
		}if($rfiddeviceno != '')
		{
			$this->db->where('tblrfid.rfiddeviceno',$rfiddeviceno);
		}
		$this->db->order_by('tblrfid.tbl_rfidid','asc');
		$info = $this->db->get();
		$i=1;
		$resultid = array();
		$resulttagno = array();
		$activecount = 0;
		$totalcount = 0;
		foreach($info->result() as $res) {
			$resultid[] = $res->itemtagid;
			$resulttagno[] = $res->itemtagnumber;
			$activecount = $i;
			$i++;
		}
		$this->db->select('COUNT(tblrfid.RFID) as totalcount');
		$this->db->from('tblrfid');
		$infocount = $this->db->get();
		$totalcount = $infocount->row()->totalcount;
		$data['id'] = $resultid;
		$data['tagno'] = $resulttagno;
		$data['loadedcount'] = $activecount;
		$data['totalcount'] = $totalcount;
		echo json_encode($data);
	}
	// Remove RFID no
	public function removerfid() {
		$this->db->truncate('tblrfid');
	}public function clearrfidtable() {
		$this->db->truncate('tblrfid');
	} 	
	public function getvalidatethetagdata() {
		
		$multiscanmode = $_GET['multiscanmode'];
		$salestransactiontypeid = $_GET['salestransactiontypeid'];
		$stocktypeid = $_GET['stocktypeid'];
		$approvalnumber = $_GET['approvalnumber'];
		$multitagscantagstextarea = $_GET['multitagscantagstextarea'];
		$tagdata = explode(',', $multitagscantagstextarea);
		$tagdata =  array_filter($tagdata);
		$innergridtagids = $_GET['innergridtagids'];
		$explodeinnergridtagids = explode(',',$innergridtagids);
		/* if($multiscanmode == 2){
			$rfidnoofdigits = $this->Basefunctions->get_company_settings('rfidnoofdigits'); 
			for($i=0; $i < count($tagdata) ;$i++) {
				$tagdata[$i] = substr($tagdata[$i],$rfidnoofdigits);
			}
		} */
		if(count($tagdata) > 0){
			$salesid = 1;
			$salesid = $this->Basefunctions->singlefieldfetch('salesid','salesnumber','sales',$approvalnumber);
			$this->db->select('itemtag.itemtagid,itemtag.itemtagnumber');
			$this->db->from('itemtag');
			if($multiscanmode == 1)
			{
				$this->db->where_in('itemtag.itemtagnumber',$tagdata);
			}else if($multiscanmode == 2){
				$this->db->where_in('itemtag.rfidtagno',$tagdata);
			}
			if($stocktypeid == 65) {
				$this->db->where('itemtag.status',15);
				$this->db->where_not_in('itemtag.itemtagid',$explodeinnergridtagids);
				$this->db->where('itemtag.salesid',$salesid);
				
			}else if($stocktypeid == 62){
				$this->db->where('itemtag.status',1);
				$this->db->where_not_in('itemtag.itemtagid',$explodeinnergridtagids);
			}else if($stocktypeid == 11){
				$this->db->where_in('itemtag.status',array('1','15'));
				$this->db->where_not_in('itemtag.itemtagid',$explodeinnergridtagids);
			}
			$this->db->order_by('itemtag.itemtagid','asc');
			$info = $this->db->get();
			$i=1;
			$resultid = array();
			$resulttagno = array();
			$activecount = 0;
			$totalcount = 0;
			foreach($info->result() as $res) {
				$resultid[] = $res->itemtagid;
				$resulttagno[] = $res->itemtagnumber;
				$activecount = $i;
				$i++;
			}
			
			$totalcount =  count($tagdata);
			$data['id'] = $resultid;
			$data['tagno'] = $resulttagno;
			$data['loadedcount'] = $activecount;
			$data['totalcount'] = $totalcount;
		}else{
			$data['id'] = '';
			$data['tagno'] = '';
			$data['loadedcount'] = 0;
			$data['totalcount'] = 0;
		}
		echo json_encode($data);
	}
	
	public function appoutuntagupdate($sdid,$status,$salestransactiontypeid) {
		$roundweight=$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); // weight round
		$purchaseerrorwt = number_format((float) $this->get_company_settings_sales('purchaseerrorwt'), $roundweight, '.', '');
		$sdid = implode(',',$sdid);
		/* $data =	$this->db->query(' SELECT  COALESCE(sum(salesdetail.grossweight),0) as appuntaggwt,COALESCE(sum(salesdetail.netweight),0) as appuntagnwt,COALESCE(sum(salesdetail.pieces),0) as appuntagpieces,COALESCE(sum(untagreturn.grossweight),0) as retuntaggwt,COALESCE(sum(untagreturn.netweight),0) as retuntagnwt,COALESCE(sum(untagreturn.pieces),0) as retuntagpieces,COALESCE(sum(untagsales.grossweight),0) as saluntaggwt,COALESCE(sum(untagsales.netweight),0) as saluntagnwt,COALESCE(sum(untagsales.pieces),0) as saluntagpieces  FROM sales JOIN salesdetail on salesdetail.salesid=sales.salesid 
		left outer JOIN salesdetail as untagreturn on untagreturn.presalesdetailid = salesdetail.salesdetailid and untagreturn.stocktypeid = 66 and  untagreturn.status = 1
		left outer JOIN salesdetail as untagsales on untagsales.presalesdetailid = salesdetail.salesdetailid and untagsales.stocktypeid = 16 and  untagsales.status = 1
		 WHERE sales.status=1 AND sales.salestransactiontypeid != 16 AND salesdetail.status = 1 AND  salesdetail.salesdetailid in ('.$sdid.') and salesdetail.stocktypeid = 63 GROUP BY `salesdetail`.`salesdetailid`'); */
		if($salestransactiontypeid == 11 || $salestransactiontypeid == 18  ){
			$data =	$this->db->query('SELECT  salesdetail.salesdetailid,ROUND((COALESCE(sum(salesdetail.grossweight),0) -  COALESCE(sum(untagreturn.grossweight),0) -  COALESCE(sum(untagsales.grossweight),0)),'.$roundweight.') as pendinggwt,ROUND((COALESCE(sum(salesdetail.netweight),0) -  COALESCE(sum(untagreturn.netweight),0) -  COALESCE(sum(untagsales.netweight),0)),'.$roundweight.') as pendingnwt, ROUND((COALESCE(sum(salesdetail.pieces),0) -  COALESCE(sum(untagreturn.pieces),0) -  COALESCE(sum(untagsales.pieces),0)),0) as pendingpieces FROM sales JOIN salesdetail on salesdetail.salesid=sales.salesid 
			left outer JOIN salesdetail as untagreturn on untagreturn.presalesdetailid = salesdetail.salesdetailid and untagreturn.stocktypeid = 66 and  untagreturn.status = 1
			left outer JOIN salesdetail as untagsales on untagsales.presalesdetailid = salesdetail.salesdetailid and untagsales.stocktypeid = 16 and  untagsales.status = 1
			WHERE sales.status=1  AND salesdetail.status = 1 AND  salesdetail.salesdetailid in ('.$sdid.') and salesdetail.stocktypeid = 63 GROUP BY `salesdetail`.`salesdetailid`');
		}else if($salestransactiontypeid == 19 ){
			$data =	$this->db->query('SELECT  salesdetail.salesdetailid,ROUND((COALESCE(sum(salesdetail.grossweight),0) -  COALESCE(sum(approvalinreturn.grossweight),0)),'.$roundweight.') as pendinggwt,ROUND((COALESCE(sum(salesdetail.netweight),0) -  COALESCE(sum(approvalinreturn.netweight),0)),'.$roundweight.') as pendingnwt, ROUND((COALESCE(sum(salesdetail.pieces),0) -  COALESCE(sum(approvalinreturn.pieces),0)),0) as pendingpieces FROM sales JOIN salesdetail on salesdetail.salesid=sales.salesid left outer JOIN salesdetail as approvalinreturn on approvalinreturn.presalesdetailid = salesdetail.salesdetailid and approvalinreturn.stocktypeid = 25 and  approvalinreturn.status = 1 WHERE sales.status=1  AND salesdetail.status = 1 AND  salesdetail.salesdetailid in ('.$sdid.') and salesdetail.stocktypeid = 24 GROUP BY `salesdetail`.`salesdetailid`');
		}
		if($data->num_rows() > 0)
		{
			foreach($data->result() as $info)
			{
				$pendinggwt =  number_format((float)$info->pendinggwt, $roundweight, '.', '');
				$pendingnwt =  number_format((float)$info->pendingnwt, $roundweight, '.', '');
				$pendingpieces =  $info->pendingpieces;
				$salesdetailid =  $info->salesdetailid;
				if($status == 0){
					if($pendinggwt <= $purchaseerrorwt && $pendingnwt <= $purchaseerrorwt && $pendingpieces == 0){
						$updatesalesdetail = array(
								'approvalstatus'=>0
						);
					}else{
						$updatesalesdetail = array(
								'approvalstatus'=>1
						);
					}
					$updatesalesdetail = array_merge($updatesalesdetail,$this->Crudmodel->updatedefaultvalueget());
					$this->db->where('salesdetailid',$salesdetailid);
					$this->db->update('salesdetail',$updatesalesdetail);
				}else if($status == 2 || $status == 3){
						$updatesalesdetail = array(
								'pendinggwt'=>$pendinggwt,
								'pendingnwt'=>$pendingnwt,
								'pendingpieces'=>$pendingpieces
						);
				}
			}
			 if($status == 2){
				 echo json_encode($updatesalesdetail);
			 }else if($status == 3){
				 return ($updatesalesdetail);
			 }
		}
	}
	public function getandsetaccountidpo() {
		$datarowid = $_POST['salesid'];
		$accountid = $this->Basefunctions->singlefieldfetch('accountid','salesid','sales',$datarowid);
		$accountname = $this->Basefunctions->singlefieldfetch('accountname','accountid','account',$accountid);
		$acc_array = array(
				'accountid' => $accountid,
				'accountname' => $accountname,
					);
		echo json_encode($acc_array);
	}
	public function loadplaceordernumber() {
		$accountid = $_POST['accountid'];
		$ordertransactionpage = $_POST['ordertransactionpage'];
		if($ordertransactionpage == 'placeorder') {
			$data = $this->db->query("SELECT `sales`.`salesnumber`, `sales`.`salesid`, `sales`.`accountid`, `account`.`accountname`, `salesdetail`.`ordernumber`,`salesdetail`.`poorderstatusid`,`salesdetail`.`postatuscomment`,`salesdetail`.`poadminstatuscomment` FROM `sales` LEFT JOIN `salesdetail` ON `salesdetail`.`salesid` = `sales`.`salesid` LEFT JOIN `account` ON `account`.`accountid` = `sales`.`accountid` WHERE `sales`.`status` NOT IN(0, 3) AND `sales`.`salesnumber` != 'pending' AND `salesdetail`.`status` NOT IN(0, 3) AND `salesdetail`.`orderstatusid` IN(3) AND `salesdetail`.`poorderstatusid` IN(1,23) AND `sales`.`salestransactiontypeid` IN(21) AND `sales`.`accountid` = '$accountid' AND `salesdetail`.`stocktypeid` IN(76) GROUP BY `sales`.`salesid`");
		} else if($ordertransactionpage == 'generatepo') {
			$data = $this->db->query("SELECT `sales`.`salesnumber`, `sales`.`salesid`, `sales`.`accountid`, `account`.`accountname`, `salesdetail`.`ordernumber`,`salesdetail`.`poorderstatusid`,`salesdetail`.`postatuscomment`,`salesdetail`.`poadminstatuscomment` FROM `sales` LEFT JOIN `salesdetail` ON `salesdetail`.`salesid` = `sales`.`salesid` LEFT JOIN `account` ON `account`.`accountid` = `sales`.`accountid` WHERE `sales`.`status` NOT IN(0, 3) AND `sales`.`salesnumber` != 'pending' AND `salesdetail`.`status` NOT IN(0, 3) AND `salesdetail`.`orderstatusid` IN(3) AND sales.deliverypomodeid IN(2,3,4) AND `salesdetail`.`delorderstatusid` IN(1,23) AND `salesdetail`.`generatepo` IN(2) AND `sales`.`accountid` = '$accountid' AND `salesdetail`.`stocktypeid` IN(85) GROUP BY `sales`.`salesid`");
		}
		$i=0;
		foreach($data->result() as $info) {
			$dataordernumber[$i] = array('salesid'=>$info->salesid,'salesnumber'=>$info->salesnumber,'poorderstatusid'=>$info->poorderstatusid,'postatuscomment'=>$info->postatuscomment,'poadminstatuscomment'=>$info->poadminstatuscomment);
			$i++;
		}
		$datas = ( ($i==0)? json_encode(array('fail'=>'FAILED')) : json_encode($dataordernumber) );
		echo $datas;
	}
	// get purity based on category
	public function getpuritybasedoncategory() {
		$categoryid = $_POST['primaryid'];
		$this->db->select('GROUP_CONCAT(DISTINCT(purityid)) as purity',false);
		$this->db->from('product');
		$this->db->where('product.categoryid',$categoryid);
		$this->db->where('product.status',$this->Basefunctions->activestatus);
		$productdata=$this->db->get();
		if($productdata->num_rows()>0){
			foreach($productdata->result() as $info)
			{
				$productarray =$info->purity;
			}
	
		}else{
			$productarray =1;
		}
		$uniqueproductarray = array_unique(explode(",", $productarray)); 
		$implodearray = implode(",", $uniqueproductarray);
		echo json_encode($implodearray);
	}
	//Update rate based purity
	public function updateratebasedpurity() {
		$rate = $_POST['rate'];
		$purity = $_POST['purity'];
		$salesdate = $this->Basefunctions->ymd_format($_POST['salesdate']);
		$metalid = $this->Basefunctions->singlefieldfetch('metalid','purityid','purity',$purity); //get metal id
		$amountround =  $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		// check already rate combinations
		$this->db->where('metalid',$metalid);
		$this->db->where_in('purityid',$purity);
		$this->db->from('rate');
		$result = $this->db->get();
		if($result->num_rows() > 0){
			$update = array('currentstatus'=>0);
			$this->db->where_in('purityid',$purity);
			$this->db->where('metalid',$metalid);
			$this->db->update('rate',$update);
		}
		$insert=array(
			'metalid'=>$metalid,
			'purityid'=>$purity,
			'currentrate'=>number_format((float)$rate, $amountround, '.', ''),
			'displaydate'=>$salesdate,
			'currentstatus'=>'1',
			'industryid'=>$this->Basefunctions->industryid,
			'branchid'=>$this->Basefunctions->branchid,
			'status'=>$this->Basefunctions->activestatus
		);
		$insert = array_merge($insert,$this->Crudmodel->defaultvalueget());
	    $this->db->insert('rate',array_filter($insert));
		echo 'SUCCESS';
	}
	public function get_company_settings_sales() {
		$info = $this->db->select('taxapplicable,inlinepdfpreview,counter,stone,accounttax,purityid,pureweight,barcodeoption,bullionproduct,salesreturntypeid,billingtypeid,chargecalculation,defaultbankid,oldjewelproducttypeid,approvaloutcalculation,schememethodid,schemetypeid,productidshow,taxtypeid,discounttypeid,businesschargeid,businesspurchasechargeid,rateadditiondefault,estimateconcept,purchasewastage,salesrate,purchaserate,paymentmodetype,oldjewelvoucher,autocalculatetax,discountpassword,discountcalc,taxcalc,defaultcashinhandid,taxchargeinclude,discountauthorization,purewtcalctype,oldjeweldetails,wastagecalctype,gstapplicable,lot,salesdatechangeablefor,cashcountershowhide,purchasedisplay,chargerequired,category_level,netwtcalctypeid,needezetap,askemailsmsfields,needcashcheque,ezetapnotification,olditemdetails,metalpurity,oldjewelvoucheredit,roundvaluelimit,advancecalc,customerwise,vendorwise,cashtenthousand,lotdetail,weightvalidate,oldjewelsumtaxapplyid,orderimageoption,transactionimageoption,weightcheck,loosestonecharge,approvaloutrfid,multiscanmode,allowedaccounttypeset,placeorderdefaultaccid,purchaseerrorwt,flatweightwastagespan,pancardrestrictamt,cashreceiptlimit,cashissuelimit,touchchargesedit,discountbilllevelpercent,salesauthuserrole,untagstoneenable,multitagbarcode,untagwtvalidate,untagwtshow,estimatequicktotal,estimatedefaultset,caratwtcalcvalue,salespaymenthideshow,povendormanagement,takeordercategory,accountypehideshow,takeorderduedate,transactioncategoryfield,transactionemployeeperson,poautomaticvendor,receiveorderconcept,receiveordertemplateid,rejectreviewordertemplateid,accountmobileunique,accountmobilevalidation,oldjewelsstonedetails,chargeconversionfield,billamountcalculationtrigger,oldjewelsstonecalc,purchasemodstonedetails,discountauthmoduleview,ratedisplaymainview,ratedisplaypurityvalue,salesreturnbillcustomers,questionaireapplicable,accountfieldcaps,stonepiecescalc,accountsummaryinfo,paymentsavebutton,printtemplatehideshow,oldpurchasecredittype')
			->from('companysetting')
			->limit(1)
			->get()
			->result();
		return $info;
	}
	//tree create model - for jewel industry
	public function jewel_treecreatemodel($tablename) {
		$data = array();
		$this->db->select('category.categoryid  AS Id,category.categoryname AS Name,category.parentcategoryid AS ParentId,category.categorylevel AS level');
		$this->db->from('category');
		//$this->db->join('category','product.categoryid=category.categoryid');
		//$this->db->where('product.status',1);
		$this->db->where('category.status',1);
		$this->db->where('category.categoryid >',10);
		$this->db->order_by('category.categoryid','ASC');
		$result = $this->db->get();		
		foreach($result->result() as $row) {
			$data[$row->Id] = array('id'=>$row->Id,'name'=>$row->Name,'pid'=>$row->ParentId,'level'=>$row->level);
		}
		$itemsByParent = array();
		foreach ($data as $item){
			if (!isset($itemsByParent[$item['pid']])) {
				$itemsByParent[$item['pid']] = array();
			}
			$itemsByParent[$item['pid']][] = $item;
		}	
		
		return $itemsByParent;
	}
	//Vendor Item Status Submit Call
	public function vendoritemstatusgridsubmitdetails() {
		$girddata = $_POST['innergriddata'];
		$cricount = $_POST['criteriacnt'];
		$girddatainfo = json_decode($girddata,true);
		for($i=0;$i<=($cricount-1);$i++) {
			if($girddatainfo[$i]['salesdetailid'] > 0 ) {
				$update = array('vendororderitemstatusid'=>$girddatainfo[$i]['vendoritemstatusid']);
				$this->db->where('salesdetailid',$girddatainfo[$i]['salesdetailid']);
				$this->db->update('salesdetail',array_merge($update,$this->Crudmodel->updatedefaultvalueget()));
			}
		}
		echo 'SUCCESS';
	}
	// Image display in main view grid.
	public function retrievesalesdetailimage() {
		$tagimage = array();
		$datarowid = $_POST['salesid'];
		$transactiontype = $this->Basefunctions->singlefieldfetch('salestransactiontypeid','salesid','sales',$datarowid);
		$mainstocktypeid = $this->Basefunctions->singlefieldfetch('mainstocktypeid','salesid','sales',$datarowid);
		if($transactiontype == 20 && $mainstocktypeid == 75) {
			$stocktype = '75';
		} else if($transactiontype == 21) {
			$stocktype = '76';
		} else if($transactiontype == 20 && $mainstocktypeid == 82) {
			$stocktype = '82';
		} else if($transactiontype == 27) {
			$stocktype = '85';
		}
		$this->db->select('salesdetail.tagimage');
		$this->db->from('salesdetail');
		$this->db->where_in('salesdetail.stocktypeid',array($stocktype));
		$this->db->where('salesdetail.salesid',$datarowid);
		$this->db->where_not_in('salesdetail.status',array(0,3));
		$data = $this->db->get();
		if($data->num_rows() > 0) {
			foreach($data->result() as $row) {
				if($row->tagimage != '') {
					$tagimage[] = $row->tagimage;
				}
			}
		}
		if(empty($tagimage)) {
			$tagimage = 'FAILED';
		}
		echo json_encode($tagimage);
	}
	// Check Place Order is rejected by vendor to Reassign new vendor
	public function checkplaceorderisrejected() {
		$salesid = $_POST['salesid'];
		$type = $_POST['type'];
		$gporder = $_POST['gporder'];
		$salestransactiontypeid = $this->Basefunctions->singlefieldfetch('salestransactiontypeid','salesid','sales',$salesid);
		if($salestransactiontypeid == 21 && $type == 21) {
			$this->db->select('salesdetailid');
			$this->db->from('salesdetail');
			$this->db->where('stocktypeid',76);
			$this->db->where('salesid',$salesid);
			$this->db->where_in('poorderstatusid','22');
			$this->db->where('status','1');
			$datatake = $this->db->get();
			if($datatake->num_rows() > 0) {
				$data['status'] = 1;
			} else {
				$data['status'] = 0;
			}
		}else if($salestransactiontypeid == 27 && $gporder == 27) {
			$this->db->select('salesdetailid');
			$this->db->from('salesdetail');
			$this->db->where('stocktypeid',85);
			$this->db->where('salesid',$salesid);
			$this->db->where_in('delorderstatusid','25');
			$this->db->where_in('generatepo','2');
			$datatake = $this->db->get();
			if($datatake->num_rows() > 0) {
				$data['status'] = 2;
			} else {
				$data['status'] = 0;
			}
		} else {
			$data['status'] = 3;
		}
		echo json_encode($data);
	}
	// Retrieve Rejected Product details
	public function retrieverejectedvendorproductdetailinformation() {
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$salesid = $_POST['salesid'];
		$modulestatus = $_POST['status'];
		if($modulestatus == 1) {
			$dataset = "`sales`.`salesid`, `salesdetail`.`salesdetailid`,  `salesdetail`.`salesdetailimage`, `product`.`productname`, `purity`.`purityname`, ROUND(salesdetail.grossweight, '".$round."') as grossweight,`salesdetail`.`pieces`, `salesdetail`.`productid`, `salesdetail`.`purityid`,salesdetail.vendororderduedate,salesdetail.ordervendorid";
			$join = 'LEFT OUTER JOIN `product` ON `product`.`productid`=`salesdetail`.`productid`';
			$join.= 'LEFT OUTER JOIN `purity` ON `purity`.`purityid`=`salesdetail`.`purityid`';
			$join.= 'LEFT OUTER JOIN `sales` ON `sales`.`salesid`=`salesdetail`.`salesid`';
			$cuscondition = "salesdetail.poorderstatusid IN(22) AND salesdetail.generatepo IN(1) AND sales.salesid = '".$salesid."' AND `salesdetail`.`orderstatusid` = '2'  AND `salesdetail`.`stocktypeid` = '76' AND `salesdetail`.`salesdetailid` NOT IN(1) AND `salesdetail`.`status` = 1 AND";
			$status='sales.status='.$this->Basefunctions->activestatus.'';
		} else if($modulestatus == 2) {
			$dataset = "`sales`.`salesid`,`presalesdetail`.`salesdetailid` as presalesdetailid, `salesdetail`.`salesdetailid`,  `salesdetail`.`salesdetailimage`, `product`.`productname`, `purity`.`purityname`, ROUND(salesdetail.grossweight, '".$round."') as grossweight,`salesdetail`.`pieces`, `salesdetail`.`productid`, `salesdetail`.`purityid`,presalesdetail.vendororderduedate,presalesdetail.ordervendorid";
			$join = 'LEFT OUTER JOIN `product` ON `product`.`productid`=`salesdetail`.`productid`';
			$join.= 'LEFT OUTER JOIN `purity` ON `purity`.`purityid`=`salesdetail`.`purityid`';
			$join.= 'LEFT OUTER JOIN `sales` ON `sales`.`salesid`=`salesdetail`.`salesid`';
			$join.= 'LEFT OUTER JOIN `salesdetail` as presalesdetail ON `presalesdetail`.`salesdetailid`=`salesdetail`.`presalesdetailid`';
			$join.= 'LEFT OUTER JOIN `sales` as presales ON `presales`.`salesid`=`presalesdetail`.`salesid`';
			$cuscondition = "presalesdetail.orderstatusid IN(3) AND salesdetail.generatepo IN(2) AND sales.salesid = '".$salesid."' AND `salesdetail`.`stocktypeid` = '85' AND `salesdetail`.`salesdetailid` NOT IN(1) AND";
			$status='sales.status='.$this->Basefunctions->activestatus.'';
		}
		$result = $this->db->query('select '.$dataset.' from salesdetail '.$join.' WHERE '.$cuscondition.' '. $status.'');
		$i = 0;
		if($result->num_rows() > 0) {
			foreach($result->result() as $info) {
				if($modulestatus == 2) {
					$info->salesdetailid = $info->presalesdetailid;
				}
				$dataretrievedetails[$i] = array('salesdetailid'=>$info->salesdetailid,'productname'=>$info->productname,'purityname'=>$info->purityname,'grossweight'=>$info->grossweight,'salesdetailimage'=>$info->salesdetailimage,'vendororderduedate'=>$info->vendororderduedate,'ordervendorid'=>$info->ordervendorid,'status'=>$modulestatus);
				$i++;
			}
		}
		$datas = ( ($i==0)? json_encode(array('fail'=>'FAILED')) : json_encode($dataretrievedetails) );
		echo $datas;
	}
	// Retrieve Rejected Product Image details
	public function retrieverejectedproductimagedetails() {
		$salesdetailid = $_POST['salesdetailid'];
		$result = $this->db->query('select salesdetail.salesdetailimage from salesdetail WHERE salesdetail.salesdetailid = '.$salesdetailid.'');
		$i = 0;
		if($result->num_rows() > 0) {
			foreach($result->result() as $info) {
				$dataretrievedetails[$i] = array('salesdetailimage'=>$info->salesdetailimage);
				$i++;
			}
		}
		$datas = ( ($i==0)? json_encode(array('fail'=>'FAILED')) : json_encode($dataretrievedetails) );
		echo $datas;
	}
	// Update Rejected product details
	function updaterejectedproductdetails() {
		$transactionpage = $_POST['transactionpage'];
		if($transactionpage == 1) {
			// Update Place Order salesdetail id
			$update=array(
				'ordervendorid'=>$_POST['newvendoraccountid'],
				'poorderstatusid'=>'1',
				'status'=>'1',
				'orderstatusid'=>'3',
				'postatuscomment'=>'',
				'poadminstatuscomment'=>'',
				'vendororderduedate'=>$this->Basefunctions->ymd_format($_POST['newvendorduedate']),
				'salesdetailimage'=>$_POST['tagimage'],
			);
			$update = array_merge($update,$this->Crudmodel->updatedefaultvalueget());
			$this->db->where('salesdetailid',$_POST['salesdetailproductid']);
			$this->db->update('salesdetail',$update);
			// Update Take Order salesdetail id
			$presalesdetailid =  $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$_POST['salesdetailproductid']);
			$updatetakeorder = array(
				'ordervendorid'=>$_POST['newvendoraccountid'],
				'poorderstatusid'=>'1',
				'orderstatusid'=>'3',
				'vendororderduedate'=>$this->Basefunctions->ymd_format($_POST['newvendorduedate']),
				'salesdetailimage'=>$_POST['tagimage'],
			);
			$updatetakeorder = array_merge($updatetakeorder,$this->Crudmodel->updatedefaultvalueget());
			$this->db->where('salesdetailid',$presalesdetailid);
			$this->db->update('salesdetail',$updatetakeorder);
			// Update Sales Table
			$salesid =  $this->Basefunctions->singlefieldfetch('salesid','salesdetailid','salesdetail',$_POST['salesdetailproductid']);
			$updatesales = array(
				'accountid'=>$_POST['newvendoraccountid'],
			);
			$updatesales = array_merge($updatesales,$this->Crudmodel->updatedefaultvalueget());
			$this->db->where('salesid',$salesid);
			$this->db->update('sales',$updatesales);
		} else if($transactionpage == 2) {
			// Update Place Order salesdetail id
			$update=array(
				'ordervendorid'=>$_POST['newvendoraccountid'],
				'vendororderduedate'=>$this->Basefunctions->ymd_format($_POST['newvendorduedate']),
				'salesdetailimage'=>$_POST['tagimage'],
			);
			$update = array_merge($update,$this->Crudmodel->updatedefaultvalueget());
			$this->db->where('salesdetailid',$_POST['salesdetailproductid']);
			$this->db->update('salesdetail',$update);
			// Update Take Order salesdetail id
			$presalesdetailid =  $this->Basefunctions->singlefieldfetch('presalesdetailid','salesdetailid','salesdetail',$_POST['salesdetailproductid']);
			$updatetakeorder = array(
				'ordervendorid'=>$_POST['newvendoraccountid'],
				'vendororderduedate'=>$this->Basefunctions->ymd_format($_POST['newvendorduedate']),
				'salesdetailimage'=>$_POST['tagimage'],
			);
			$updatetakeorder = array_merge($updatetakeorder,$this->Crudmodel->updatedefaultvalueget());
			$this->db->where('salesdetailid',$presalesdetailid);
			$this->db->update('salesdetail',$updatetakeorder);
		}
		echo 'SUCCESS';
	}
	// Retrieve Purity value in form view
	public function retrievelastupdatepurityvalue() {
		$data=array();
		$amountround =  $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$ratedisplaypurityvalue = explode(',',$_POST['ratedisplaypurityvalue']);
		for($i = 0; $i < count($ratedisplaypurityvalue); $i++ ) {
			$currentrate = $this->Basefunctions->get_todaygoldratevalue('currentrate',$ratedisplaypurityvalue[$i]);
			$shortname =  $this->Basefunctions->singlefieldfetch('shortname','purityid','purity',$ratedisplaypurityvalue[$i]);
			$data[$i] = array('purityshortname'=>$shortname,'currentrate'=>ROUND($currentrate,$amountround));
		}
		echo json_encode($data);
	}
	// Order Tracking Overlay grid.
	public function ordertrackinggriddynamicdatainfofetch($tablename,$sortcol,$sortord,$pagenum,$rowscount,$primaryid,$categoryid,$accountid,$accountshortnameid,$mobilenumber,$ordernumber,$ordertrackmainviewclick) {
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
		$rateround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //Rate round-off
		$taxround = 2;
		$categoryconditionwhere = '';
		$accountconditionwhere = '';
		$accountshortnameconditionwhere = '';
		$mobilenumberconditionwhere = '';
		$ordernumberconditionwhere = '';
		if($ordertrackmainviewclick == 0) {
			$ordertrackmainconditionwhere = ' 1=2 ';
		} else {
			$ordertrackmainconditionwhere = ' 1=1 ';
		}
		if($categoryid != '1') {
			$categoryconditionwhere = " AND salesdetail.categoryid = '$categoryid' ";
		}
		if($accountid != '1') {
			$accountconditionwhere = " AND account.accountid = '$accountid' ";
		}
		if($accountshortnameid != '1') {
			$accountshortnameconditionwhere = " AND account.accountid = '$accountid' ";
		}
		if($mobilenumber != '') {
			$mobilenumberconditionwhere = " AND account.mobilenumber = '$mobilenumber' ";
		}
		if($ordernumber != '') {
			$ordernumberconditionwhere = " AND sales.salesnumber = '$ordernumber' ";
		}
		$dataset = "DATE_FORMAT(sales.salesdate,'%d-%m-%Y') AS salesdate,account.accountname,vendoraccount.accountname as vendorname,`sales`.`salesid`,sales.salesnumber, `salesdetail`.`salesdetailid`, `itemtag`.`itemtagnumber`,orderstatus.orderstatusname, `product`.`productname`, `purity`.`purityname`, `counter`.`countername`, ROUND(salesdetail.grossweight, '".$round."') as grossweight, ROUND(salesdetail.stoneweight, '".$round."') as stoneweight, ROUND(salesdetail.netweight, '".$round."') as netweight, `salesdetail`.`pieces`, `salesdetail`.`productid`, `salesdetail`.`purityid`, `salesdetail`.`counterid`, `salesdetail`.`employeeid`, `employee`.`employeename`, `salesdetail`.`itemtagid`, ROUND(salesdetail.ratepergram, '".$rateround."') as ratepergram, `salesdetail`.`stocktypeid`, `stocktype`.`stocktypename`, `sizemaster`.`sizemastername`, `salesdetail`.`orderitemsize`, `salesdetail`.`comment`, DATE_FORMAT(salesdetail.orderduedate, '%d-%m-%Y') AS orderduedate, `salesdetail`.`tagimage`,salesdetail.orderitemratefix,DATE_FORMAT(salesdetail.vendororderduedate,'%d-%m-%Y') AS vendororderduedate,salesdetail.ordervendorid,vendororderitemstatus.vendororderitemstatusname,DATE_FORMAT(salesdetail.lastupdatedate,'%d-%m-%Y %H:%i:%s') as lastupdatedate, posalesdetail.generatepo ";
		$join = 'LEFT OUTER JOIN `itemtag` ON `itemtag`.`itemtagid`=`salesdetail`.`itemtagid`';
		$join.= 'LEFT OUTER JOIN `stocktype` ON `stocktype`.`stocktypeid`=`salesdetail`.`stocktypeid`';
		$join.= 'LEFT OUTER JOIN `orderstatus` ON `orderstatus`.`orderstatusid`=`salesdetail`.`orderstatusid`';
		$join.= 'LEFT OUTER JOIN `sizemaster` ON `sizemaster`.`sizemasterid`=`salesdetail`.`orderitemsize`';
		$join.= 'LEFT OUTER JOIN `product` ON `product`.`productid`=`salesdetail`.`productid`';
		$join.= 'LEFT OUTER JOIN `purity` ON `purity`.`purityid`=`salesdetail`.`purityid`';
		$join.= 'LEFT OUTER JOIN `counter` ON `counter`.`counterid`=`salesdetail`.`counterid`';
		$join.= 'LEFT OUTER JOIN `employee` ON `employee`.`employeeid`=`salesdetail`.`employeeid`';
		$join.= 'LEFT OUTER JOIN `sales` ON `sales`.`salesid`=`salesdetail`.`salesid`';
		$join.= 'LEFT OUTER JOIN `account` ON `account`.`accountid`=`sales`.`accountid`';
		$join.= 'LEFT OUTER JOIN `account` as vendoraccount ON `vendoraccount`.`accountid`=`salesdetail`.`ordervendorid`';
		$join.= 'LEFT OUTER JOIN `salesdetail` as posalesdetail ON `posalesdetail`.`presalesdetailid`=`salesdetail`.`salesdetailid` AND posalesdetail.status NOT IN (0,3)';
		$join.= 'LEFT OUTER JOIN `vendororderitemstatus` ON `vendororderitemstatus`.`vendororderitemstatusid`=`posalesdetail`.`vendororderitemstatusid`';
		$cuscondition = " `salesdetail`.`stocktypeid` = '75' ".$categoryconditionwhere.$accountconditionwhere.$accountshortnameconditionwhere.$mobilenumberconditionwhere.$ordernumberconditionwhere." AND `salesdetail`.`salesdetailid` NOT IN(0,3) AND $ordertrackmainconditionwhere AND";
		$status = $tablename.'.status='.$this->Basefunctions->activestatus.'';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		/* query */
		$result = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from salesdetail '.$join.' WHERE '.$cuscondition.' '. $status.' ORDER BY '.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$data=array();
		$i=0;
		foreach($result->result() as $row) {
			if($row->generatepo == 2) {
				$generateponame = 'Yes';
			} else {
				$generateponame = 'No';
			}
			$salesdetailid = $row->salesdetailid;
			$salesdate = $row->salesdate;
			$ordernumber =$row->salesnumber;
			$accountname =$row->accountname;
			$orderstatusname =$row->orderstatusname;
			$vendororderitemstatusname =$row->vendororderitemstatusname;
			$productname =$row->productname;
			$purityname =$row->purityname;
			$grossweight =$row->grossweight;
			$stoneweight =$row->stoneweight;
			$netweight =$row->netweight;
			$pieces =$row->pieces;
			$tagimage = $row->tagimage;
			$vendorname = $row->vendorname;
			$orderduedate =$row->orderduedate;
			$vendororderduedate =$row->vendororderduedate;
			$data[$i]=array('id'=>$salesdetailid,$salesdate,$ordernumber,$accountname,$orderstatusname,$vendororderitemstatusname,$generateponame,$productname,$purityname,$grossweight,$stoneweight,$netweight,$pieces,$tagimage,$vendorname,$orderduedate,$vendororderduedate);
			$i++;
		}
		$finalresult = array('0'=>$data,'1'=>$page,'2'=>'json');
		return $finalresult;
	}
	// Retrieve all Bill numbers to load
	public function retrieveallbillnumberdetails() {
		$userroleid = $this->Basefunctions->userroleid;
		$salesid = $_POST['salesid'];
		$accountid = $_POST['accountid'];
		$salesreturnbillcustomers = $_POST['salesreturnbillcustomers'];
		if($salesreturnbillcustomers != 1) {
			$accountidwhere = "AND sales.accountid NOT IN(1) ";
		} else {
			$accountidwhere = "AND sales.accountid = '$accountid' ";
		}
		if($salesid != '') {
			$salesid = $salesid;
		} else {
			$salesid = '1';
		}
		$salesreturnuserroleauth = array($this->Basefunctions->get_company_settings('salesreturnuserroleauth'));
		if (in_array($userroleid, $salesreturnuserroleauth)) {
			$datecondition = ' AND 1=1';
		} else {
			$salesreturndays = $this->Basefunctions->get_company_settings('salesreturndays');
			$currentdate = date('Y-m-d');
			$restrictdate = date('Y-m-d', strtotime('-'.$salesreturndays.' days'));
			$datecondition = " AND sales.salesdate BETWEEN '$restrictdate' AND '$currentdate'";
		}
		$cuscondition = "salesdetail.stocktypeid IN(11,12,13,83) AND `salesdetail`.`status` = 1 AND `sales`.`salestransactiontypeid` = '11' AND sales.salesid NOT IN ($salesid) $accountidwhere AND sales.salesnumber != '' ";
		$status='sales.status='.$this->Basefunctions->activestatus.'';
		$result = $this->db->query('SELECT `sales`.`salesid`, `sales`.`salesnumber`, `sales`.`accountid`, `salesdetail`.`salesdetailid`, salesdetail.itemtagid from sales LEFT OUTER JOIN `salesdetail` ON `salesdetail`.`salesid`=`sales`.`salesid` WHERE '.$cuscondition.' AND salesdetail.itemtagid NOT IN (select itemtagid from salesdetail where stocktypeid = 20 and status = 1) AND '. $status.$datecondition.' group by sales.salesid order by sales.salesid desc');
		$i = 0;
		if($result->num_rows() > 0) {
			foreach($result->result() as $info) {
				$dataretrievedetails[$i] = array('salesid'=>$info->salesid,'salesnumber'=>$info->salesnumber,'accountid'=>$info->accountid);
				$i++;
			}
		}
		$datas = ( ($i==0)? json_encode(array('fail'=>'FAILED')) : json_encode($dataretrievedetails) );
		echo $datas;
	}
	// Retrieve Tax ids of taxmaster id
	public function retrievealltaxids($taxmasterid) {
		$taxdetail = [];
		$this->db->select('taxmaster.taxmasterid,taxmaster.taxmastername,tax.taxname,tax.taxid,tax.taxrate');
		$this->db->from('tax');
		$this->db->join('taxmaster','taxmaster.taxmasterid=tax.taxmasterid');
		$this->db->where('tax.taxmasterid',$taxmasterid);
		$this->db->where_in('tax.status',array($this->Basefunctions->activestatus));
		$data = $this->db->get();
		if($data->num_rows()>0){
			$i=1;
			foreach($data->result() as $value) {
				$taxdetail[$i] = array('taxmasterid'=>$value->taxmasterid,'taxmastername'=>$value->taxmastername,'taxid'=>$value->taxid,'taxname'=>$value->taxname,'rate'=>$value->taxrate);
				$i++;
			}
			return json_encode($taxdetail);
		} else {
			return '';
		}
	}
	// Purity data details
	public function all_puritydetails() {
		$puritydetails = $this->db->query("SELECT purityid from purity where status = 1 order by purityid asc");
		if($puritydetails->num_rows() > 0) {
			foreach($puritydetails->result() as $info) {
				$dataretrievedetails[] = $info->purityid;
			}
		}
		$dataretrievedetails = implode(',',$dataretrievedetails);
		return $dataretrievedetails;
	}
	// Metal data details
	public function all_metaldetails() {
		$metaldetails = $this->db->query("SELECT metalid from purity where status = 1 order by purityid asc");
		if($metaldetails->num_rows() > 0) {
			foreach($metaldetails->result() as $info) {
				$dataretrievedetails[] = $info->metalid;
			}
		}
		$dataretrievedetails = implode(',',$dataretrievedetails);
		return $dataretrievedetails;
	}
	// Product Based Tax Calculation Information
	public function productbasedtaxinformationfetch($totalamount,$productbasedtaxmasterid,$igstorcsgst) {
		$cgstvalue = 0.00;
		$sgstvalue = 0.00;
		$igstvalue = 0.00;
		$taxsplitup = $this->db->query("SELECT taxname,taxrate from tax where taxmasterid = $productbasedtaxmasterid");
		if($taxsplitup->num_rows() > 0) {
			foreach($taxsplitup->result() as $info) {
				if(substr($info->taxname, 0, 4) == 'CGST') {
					$cgstvalue = number_format((float)(($info->taxrate/100)*$totalamount), 2, '.', '');
				} else if(substr($info->taxname, 0, 4) == 'SGST') {
					$sgstvalue = number_format((float)(($info->taxrate/100)*$totalamount), 2, '.', '');
				}
			}
			if($igstorcsgst == 2) {
				$cgstvalue = $cgstvalue;
				$sgstvalue = $sgstvalue;
				$igstvalue = 0.00;
			} else if($igstorcsgst == 3) {
				$igstvalue = number_format((float)($cgstvalue + $sgstvalue), 2, '.', '');
				$cgstvalue = 0.00;
				$sgstvalue = 0.00;
			}
		}
		return array($cgstvalue,$sgstvalue,$igstvalue);
	}
	// Sales Return Tagged checking
	public function checksalesreturntagged($salesid) {
		$dataretrievedetails = [];
		$resultdata = $this->db->query("SELECT salesdetailid from salesdetail where status = 1 AND stocktypeid IN (20) AND taggeditemtagid NOT IN (1) AND salesid = $salesid ");
		if($resultdata->num_rows() > 0) {
			foreach($resultdata->result() as $info) {
				$dataretrievedetails[] = $info->salesdetailid;
			}
		}
		if(!empty($dataretrievedetails)) {
			$finalresult = implode(',',$dataretrievedetails);
		} else {
			$finalresult = '';
		}
		return $finalresult;
	}
	// Sales Item is Returned and Return item is Tagged checking
	public function checksalesitemwithreturntagged($salesid) {
		$dataretrievedetails = [];
		$resultdata = $this->db->query("SELECT salesdetailid from salesdetail where status = 1 AND taggeditemtagid IN (1) AND itemtagid IN (select itemtagid from salesdetail where stocktypeid IN (20) AND salesid != $salesid AND status = 1) AND salesid = $salesid ");
		if($resultdata->num_rows() > 0) {
			foreach($resultdata->result() as $info) {
				$dataretrievedetails[] = $info->salesdetailid;
			}
		}
		if(!empty($dataretrievedetails)) {
			$finalresult = implode(',',$dataretrievedetails);
		} else {
			$finalresult = '';
		}
		return $finalresult;
	}
	// Order Tracking - Account Name Fetch
	public function ordertrackingaccountdetailsfetch() {
		$query = $this->db->query(
			   "SELECT account.accountname, account.accountid, accounttype.accounttypename, account.accounttypeid, account.accounttaxstatus, account.mobilenumber,
				account.accountshortname
				FROM account
				JOIN sales ON sales.accountid = account.accountid
				JOIN salesdetail ON sales.salesid = salesdetail.salesid
				JOIN accounttype ON accounttype.accounttypeid = account.accounttypeid
				WHERE account.status =1 and account.accounttypeid in (6) AND salesdetail.stocktypeid IN (75) AND salesdetail.orderstatusid NOT IN (4)
				Group BY account.accountid ORDER BY account.accounttypeid asc
				");
		return $query;
	}
	// Order Tracking - Account Name Fetch
	public function ordertrackingcategorydetailsfetch() {
		$query = $this->db->query(
			   "SELECT category.categoryname, category.categoryid, category.categorysuffix
				FROM category
				WHERE category.status = 1 and category.categoryid > 10
				Group BY category.categoryid ORDER BY category.categoryid asc
				");
		return $query;
	}
	// Update Take ORder against Place Order Item Status
	public function update_placeorderitemstatus($salesid) {
		$takeorderitemscount = $this->Basefunctions->singlefieldfetch('summaryitemscount','salesid','sales',$salesid);
		$placeorder_statusmainview = $this->db->query(" SELECT salesdetailid from salesdetail where salesid = $salesid and stocktypeid in (75) and orderstatusid in (2) and status = 1 ");
		if($takeorderitemscount == $placeorder_statusmainview->num_rows()) {
			$placeorderitemstatus = array(
				'placeorderitemstatus'=>'2'
			);
		} else if($placeorder_statusmainview->num_rows() == 0) {
			$placeorderitemstatus = array(
				'placeorderitemstatus'=>'4'
			);
		} else {
			$placeorderitemstatus = array(
				'placeorderitemstatus'=>'3'
			);
		}
		$placeorderitemstatus = array_merge($placeorderitemstatus,$this->Crudmodel->updatedefaultvalueget());
		$this->db->where('salesid',$salesid);
		$this->db->update('sales',$placeorderitemstatus);
	}
	//unique name check
	public function rfitagnochecksales() {
		$industryid = $this->Basefunctions->industryid;
		$accname = $_POST['accname'];
		if($accname != "") {
			$result = $this->db->query('SELECT tagno from rfidref WHERE tagserialno IN("'.$accname.'") LIMIT 1');
			if($result->num_rows() > 0) {
				foreach($result->result() as $row) {
					echo $row->tagno;
				}
			} else {
				echo 'NOTAGNO';
			}
		} else {
			echo "BLANKFIELD";
		}
	}
	// Display Account Information
	public function getselaccountinformation() {
		$industryid = $this->Basefunctions->industryid;
		$accid = $_POST['accid'];
		$accname =  $this->Basefunctions->singlefieldfetch('accountname','accountid','account',$accid);
		$mobileno =  $this->Basefunctions->singlefieldfetch('mobilenumber','accountid','account',$accid);
		$accinfo['accname'] = $accname;
		$accinfo['mobileno'] = $mobileno;
		echo json_encode($accinfo);
	}
	// Update Scenario for old customers
	public function updatediamondcaratweightdetails() {
		$info =$this->db->query("SELECT salesdetail.salesdetailid, COALESCE(ROUND(SUM(salesstoneentry.caratweight),2),0) as diacaratweight, COALESCE(ROUND(SUM(salesstoneentry.pieces),0),0) as diapieces from salesdetail LEFT OUTER JOIN salesstoneentry ON salesstoneentry.salesdetailid = salesdetail.salesdetailid AND salesstoneentry.status = 1 AND salesstoneentry.stonetypeid = 2 WHERE salesdetail.salesdetailid > 1 Group by salesdetail.salesdetailid");
		if($info->num_rows() > 0) {
			foreach($info->result() as $infodata) {
				$sddiamonddetailsupdate = array(
					'diacaratweight' => $infodata->diacaratweight,
					'diapieces' => $infodata->diapieces
				);
				$sddiamonddetailsupdate = array_merge($sddiamonddetailsupdate);
				$this->db->where('salesdetailid',$infodata->salesdetailid);
				$this->db->update('salesdetail',$sddiamonddetailsupdate);
			}
		}
	}
	public function updatestonecaratweightdetails() {
		$info =$this->db->query("SELECT salesdetail.salesdetailid, COALESCE(ROUND(SUM(salesstoneentry.caratweight),2),0) as stonecaratweight, COALESCE(ROUND(SUM(salesstoneentry.pieces),0),0) as stonepieces from salesdetail LEFT OUTER JOIN salesstoneentry ON salesstoneentry.salesdetailid = salesdetail.salesdetailid AND salesstoneentry.status = 1 AND salesstoneentry.stonetypeid NOT IN (2) WHERE salesdetail.salesdetailid > 1 Group by salesdetail.salesdetailid");
		if($info->num_rows() > 0) {
			foreach($info->result() as $infodata) {
				$sddiamonddetailsupdate = array(
					'stonecaratweight' => $infodata->stonecaratweight,
					'stonepieces' => $infodata->stonepieces
				);
				$sddiamonddetailsupdate = array_merge($sddiamonddetailsupdate);
				$this->db->where('salesdetailid',$infodata->salesdetailid);
				$this->db->update('salesdetail',$sddiamonddetailsupdate);
			}
		}
	}
	public function updatesalesdiasummaryinfo() {
		$salesdetailwtamtnotin = "salesdetail.stocktypeid not in (19,20,26,27,28,29,31,37,38,39,49,79,84)";
		$info =$this->db->query("SELECT sales.salesid, ROUND( +COALESCE(SUM(CASE WHEN $salesdetailwtamtnotin then salesdetail.diacaratweight end),0),2) as diacaratweight, ROUND( +COALESCE(SUM(CASE WHEN $salesdetailwtamtnotin then salesdetail.diapieces end),0),0) as diapieces, ROUND( +COALESCE(SUM(CASE WHEN $salesdetailwtamtnotin then salesdetail.stonecaratweight end),0),2) as stonecaratweight, ROUND( +COALESCE(SUM(CASE WHEN $salesdetailwtamtnotin then salesdetail.stonepieces end),0),0) as stonepieces from sales JOIN salesdetail on salesdetail.salesid=sales.salesid AND salesdetail.status = 1 WHERE sales.salesid > 1 Group by sales.salesid");
		if($info->num_rows() > 0) {
			foreach($info->result() as $infodata) {
				$salesdiamonddetailsupdate = array(
					'summarydiacaratweight' => $infodata->diacaratweight,
					'summarydiapieces' => $infodata->diapieces,
					'summarystonecaratweight' => $infodata->stonecaratweight,
					'summarystonepieces' => $infodata->stonepieces
				);
				$salesdiamonddetailsupdate = array_merge($salesdiamonddetailsupdate);
				$this->db->where('salesid',$infodata->salesid);
				$this->db->update('sales',$salesdiamonddetailsupdate);
			}
		}
	}
}