<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Smscampaign extends MX_Controller{
	//To load the Register View
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Smscampaign/Smscampaignmodel');
    }
	public function index() {
		$moduleid = array(26);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(26);
		$viewmoduleid = array(26);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Smscampaign/smscampaignview',$data);
	}
	//mass update default view fetch
	public function defaultviewfetch() {
		$mid = $_GET['modid'];
		$viewid = $this->Smscampaignmodel->defaultviewfetchmodel($mid);
		echo $viewid;
	}
	//parent table get
	public function parenttableget() {
		$this->Smscampaignmodel->parenttablegetmodel();
	}
	//fetch json data information
	public function smssubscribervalueget() {
		$groupid = $_GET['smsgroupsid'];
		$segmentid = $_GET['segmentid'];
		$typeid = $_GET['typeid'];
		$autoupdate = $_GET['autoupdate'];
		$segsubscriberid = $_GET['smssegmentsubscriber'];
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$chkbox = $_GET['checkbox'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'subscribers.subscribersid') : 'subscribers.subscribersid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('campaigngroupsname','firstname','lastname','mobilenumber','permissiongranted'),'colmodelindex'=>array('campaigngroups.campaigngroupsname','subscribers.firstname','subscribers.lastname','subscribers.mobilenumber','subscribers.permissiongranted'),'coltablename'=>array('campaigngroups','subscribers','subscribers','subscribers','subscribers'),'uitype'=>array('17','2','2','11','13'),'colname'=>array('Group Name','First Name','Last Name','Mobile Number','Permission Granted'),'colsize'=>array('200','200','200','200','200'));
		$result=$this->Smscampaignmodel->smssubscribervaluegetgridxmlview($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$groupid,$segmentid,$typeid,$segsubscriberid,$autoupdate);
		$device = $this->Basefunctions->deviceinfo();
	//	if($device=='phone') {
			//$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'SMS Subscriber List',$width,$height,$chkbox);
		//} else {
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'SMS Subscriber List',$width,$height,$chkbox);
		//}
		echo json_encode($datas);
	}
	//segment based subscriber id get
	public function relatedsubscriberidget() {  
    	$this->Smscampaignmodel->relatedsubscriberidgetmodel();
    }
	//mobile number fetch function
	public function mobilenumberget() {  
    	$this->Smscampaignmodel->mobilenumbergetmodel();
    }
	//sms sender id value fetch
	public function smssenderddvalfetch() {
		$this->Smscampaignmodel->smssenderddvalfetchmodel();
	}
	//fetch sms template msg
	public function fetchtemplatedetails() {
		$this->Smscampaignmodel->fetchtemplatedetailsmodel();
	}
	//editor value fetch 	
	public function editervaluefetch() {
		$filename = $_GET['filename']; 
		$newfilename = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]);
			echo json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			echo json_encode($tccontent);
		}
	}
	//sms signature value fetch
	public function smssignatureddvalfetch() {
		$this->Smscampaignmodel->smssignatureddvalfetchmodel();
	}
	//sms merge template content information
	public function smsmergcontinformationfetch() {
		$this->Smscampaignmodel->smsmergcontinformationfetchmodel();
	}
	//account credit detail fetch
	public function accountcresitdetailsfetch() {
		$addontype = $_POST['addontype'];
		$apikey = '';
		$credit = $this->Smscampaignmodel->accountcresitdetailsfetchmodel($apikey,$addontype);
		echo $credit;
	}
	//default template get
	public function defeulttemplatevalueget() {
		$this->Smscampaignmodel->defeulttemplatevaluegetmodel();
	}
	// sms send
	public function leadsmssend() {
		$this->Smscampaignmodel->leadsmssendmodel();
	}
	// dnd mobile number check
	public function dndmobilenumbercheck() {
		$this->Smscampaignmodel->dndmobilenumbercheckmodel();
	}
	// sms schedule time check
	public function scheduletimecalculate() {
		$this->Smscampaignmodel->scheduletimecalculatemodel();
	}
	//group type get
	public function grouptypeidget() {
		$this->Smscampaignmodel->grouptypeidgetmodel();		
	}
	//segment drop down load
	public function segmentddload() {
		$this->Smscampaignmodel->segmentddloadmodel();		
	} 
	//mobile number based subscriber id get
	public function mobilenumbasedsubscriberidget() {
		$this->Smscampaignmodel->mobilenumbasedsubscriberidgetmodel();		
	}
	//sms template dd value fetch
	public function smstemplateddvalfetch() {
		$this->Smscampaignmodel->smstemplateddvalfetchtmodel();		
	}
}