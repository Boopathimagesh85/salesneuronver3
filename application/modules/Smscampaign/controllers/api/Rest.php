<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH.'third_party/rest/libraries/REST_Controller.php';
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Rest extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Smscampaign/Smscampaignmodel');
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        //  $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        //  $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        // $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }
	public function defaultviewfetch_get()
    {
        // Fetch the data from database
		$id = $_GET['modid'];
		$result_id =  $this->Smscampaignmodel->defaultviewfetchmodel_api($id);
		if(!empty($result_id)) {
			$result = ['data' => $result_id, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
    }
	public function parenttableget_get()
    {
        // Fetch the data from database
		$id = $_GET['moduleid'];
		$result_id =  $this->Smscampaignmodel->parenttablegetmodel_api();
		if(!empty($result_id)) {
			$result = ['mid' => $id, 'data' => $result_id, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['mid' => $id, 'data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
    }
	public function smssubscribervalueget_get() {
		$groupid = $_GET['smsgroupsid'];
		$segmentid = $_GET['segmentid'];
		$typeid = $_GET['typeid'];
		$autoupdate = $_GET['autoupdate'];
		$segsubscriberid = $_GET['smssegmentsubscriber'];
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$chkbox = $_GET['checkbox'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'subscribers.subscribersid') : 'subscribers.subscribersid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('campaigngroupsname','firstname','lastname','mobilenumber','permissiongranted'),'colmodelindex'=>array('campaigngroups.campaigngroupsname','subscribers.firstname','subscribers.lastname','subscribers.mobilenumber','subscribers.permissiongranted'),'coltablename'=>array('campaigngroups','subscribers','subscribers','subscribers','subscribers'),'uitype'=>array('17','2','2','11','13'),'colname'=>array('Group Name','First Name','Last Name','Mobile Number','Permission Granted'),'colsize'=>array('200','200','200','200','200'));
		$result=$this->Smscampaignmodel->smssubscribervaluegetgridxmlview($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$groupid,$segmentid,$typeid,$segsubscriberid,$autoupdate);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'SMS Subscriber List',$width,$height,$chkbox);
		} else {
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'SMS Subscriber List',$width,$height,$chkbox);
		}
		if($datas) {
			$result = ['data' => $datas, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['data' => $datas, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
	}
	public function smssenderddvalfetch_get()
    {
        // Fetch the data from database
		$id = $_GET['typeid'];
		$result_id =  $this->Smscampaignmodel->smssenderddvalfetchmodel_api();
		if(!empty($result_id)) {
			$result = ['mid' => $id, 'data' => $result_id, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['mid' => $id, 'data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
    }
	public function accountcresitdetailsfetch_post()
    {
        // Fetch the data from database
		$addontype = $_POST['addontype'];
		$apikey = '';
		$result_id =  $this->Smscampaignmodel->accountcresitdetailsfetchmodel_api($apikey,$addontype);
		if(!empty($result_id)) {
			$result = ['data' => $result_id, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
    }
	public function smssignatureddvalfetch_get()
    {
        // Fetch the data from database
		$result_id =  $this->Smscampaignmodel->smssignatureddvalfetchmodel_api();
		if(!empty($result_id)) {
			$result = ['data' => $result_id, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
    }
	public function mobilenumberget_post()
    {
        // Fetch the data from database
		$sid = $_POST['subscriberid'];
		$result_id =  $this->Smscampaignmodel->mobilenumbergetmodel_api();
		if(!empty($result_id)) {
			$result = ['sid' => $sid, 'data' => $result_id, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['sid' => $sid, 'data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
    }
	public function defeulttemplatevalueget_post()
    {
        // Fetch the data from database
		$gid = $_POST['groupid'];
		$result_id =  $this->Smscampaignmodel->defeulttemplatevaluegetmodel_api();
		if(!empty($result_id)) {
			$result = ['gid' => $gid, 'data' => $result_id, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['gid' => $gid, 'data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
    }
	public function smstemplateddvalfetch_get()
    {
        // Fetch the data from database
		$mid = $_GET['moduleid'];
		$result_id =  $this->Smscampaignmodel->smstemplateddvalfetchtmodel_api();
		if(!empty($result_id)) {
			$result = ['mid' => $mid, 'data' => $result_id, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['mid' => $mid, 'data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
    }
	public function fetchtemplatedetails_get()
    {
        // Fetch the data from database
		$mid = $_GET['templid'];
		$result_id =  $this->Smscampaignmodel->fetchtemplatedetailsmodel_api();
		if(!empty($result_id)) {
			$result = ['mid' => $mid, 'data' => $result_id, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['mid' => $mid, 'data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
    }
	public function editervaluefetch_get() {
		$filename = $_GET['filename']; 
		$newfilename = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]);
			$result = ['name' => $filename, 'data' => $tccontent, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$tccontent = "Fail";
			$result = ['name' => $filename, 'data' => $tccontent, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
	}
	public function scheduletimecalculate_get() {
		// Fetch the data from database
		$result_id =  $this->Smscampaignmodel->scheduletimecalculatemodel_api();
		if($result_id == 'True') {
			$result = ['data' => $result_id, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
	}
	public function smsmergcontinformationfetch_post()
    {
        // Fetch the data from database
		$mid = $_POST['moduleid'];
		$result_id =  $this->Smscampaignmodel->smsmergcontinformationfetchmodel_api();
		if($result_id) {
			$result = ['mid' => $mid, 'data' => $result_id, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['mid' => $mid, 'data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
    }
	public function relatedsubscriberidget_post()
    {
        // Fetch the data from database
		$sid = $_POST['segmentid'];
		$result_id =  $this->Smscampaignmodel->relatedsubscriberidgetmodel_api();
		if($result_id) {
			$result = ['sid' => $sid, 'data' => $result_id, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['sid' => $sid, 'data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
    }
	public function grouptypeidget_post()
    {
        // Fetch the data from database
		$gid = $_POST['groupid'];
		$result_id =  $this->Smscampaignmodel->grouptypeidgetmodel_api();
		if($result_id) {
			$result = ['gid' => $gid, 'data' => $result_id, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['gid' => $gid, 'data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
    }
	public function segmentddload_get() {
		// Fetch the data from database
		$lid = $_GET['listid'];
		$result_id =  $this->Smscampaignmodel->segmentddloadmodel_api();
		if(!empty($result_id)) {
			$result = ['lid' => $lid, 'data' => $result_id, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['lid' => $lid, 'data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
	}
	public function leadsmssend_get() {
		$result_id =$this->Smscampaignmodel->leadsmssendmodel_api();
		if($result_id == "success") {
			$result = ['data' => $result_id, 'message' => 'Success', 'desc' => 'Messages are sent.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
	}
	public function mobilenumbasedsubscriberid_post() {
		$result_id =$this->Smscampaignmodel->mobilenumbasedsubscriberidgetmodel_api();
		if(!empty($result_id)) {
			$result = ['data' => $result_id, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}		
	}
}