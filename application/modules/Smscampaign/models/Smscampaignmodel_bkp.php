<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Model File
class smsCampaignmodel extends CI_Model{    
    public function __construct() {
		parent::__construct(); 
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//default view fetch based on module
	public function defaultviewfetchmodel($mid) {
		$data = "";
		$this->db->select('viewcreationid');
		$this->db->from('viewcreation');
		$this->db->where('viewcreation.viewcreationmoduleid',$mid);
		$this->db->where('viewcreation.createuserid',1);
		$this->db->where('viewcreation.lastupdateuserid',1);
		$this->db->where('viewcreation.status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row){
				$data = $row->viewcreationid;
			}
		}
		return $data;
	}
	//parent table name fetch
	public function parenttablegetmodel() {
		$mid = $_GET['moduleid'];
		$this->db->select('parenttable');
		$this->db->from('modulefield');
		$this->db->where('modulefield.moduletabid',$mid);
		$result = $this->db->get();
		if($result->num_rows() >0){
			foreach($result->result() as $row){
				$data = $row->parenttable;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	public function smssubscribervaluegetgridxmlview($tablename,$sortcol,$sortord,$pagenum,$rowscount,$listid,$segmentid,$typeid,$segsubscriberid,$autoupdate) {
		$industryid = $this->Basefunctions->industryid;
		$industrycond = 'AND subscribers.industryid='.$industryid.'';
		if($tablename == 'employee') {
			$status = $tablename.'.status NOT IN (3)';
		} else {
			$status = $tablename.'.status IN (1)';
		}
		if($listid != '') {
			$maillistid = 'subscribers.campaigngroupsid='.$listid.' AND ';
		}else {
			$maillistid = 'subscribers.campaigngroupsid=0 AND ';
		}
		if($typeid != ''){
			$grouptype = 'subscribers.smsgrouptypeid='.$typeid.' AND ';
		}else {
			$grouptype = '1=1 AND';
		}
		if($autoupdate != 'Yes') {  
			if($segsubscriberid != '') {
				$segsubscriberid = $segsubscriberid;
				$segmentsubscribercondition = 'AND subscribers.subscribersid IN ('.$segsubscriberid.')';
			} else {
				$segmentsubscribercondition = 'AND 1=1';
			}
		} else {
			$segmentsubscribercondition = ' AND 1=1';
		}
		$dataset = 'subscribers.subscribersid,campaigngroups.campaigngroupsname,subscribers.firstname,subscribers.lastname,subscribers.mobilenumber,subscribers.permissiongranted';
		$join =' LEFT OUTER JOIN campaigngroups ON campaigngroups.campaigngroupsid=subscribers.campaigngroupsid';
		$where = '1=1';
		//pagination
		$query = 'select '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$where.' '.$segmentsubscribercondition.' AND '.$maillistid.' '.$grouptype.' '.$status.' '.$industrycond.' ORDER BY'.' '.$sortcol.' '.$sortord;
		$page = $this->Basefunctions->generatepage($query,$pagenum,$rowscount);
		//query
		$data = $this->db->query('select '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$where.' '.$segmentsubscribercondition.' AND '.$maillistid.' '.$grouptype.' '.$status.' '.$industrycond.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$page['start'].','.$page['records'].'');
		$finalresult=array($data,$page,'');
		return $finalresult;
	}
	//segment based subscriber id get
	public function relatedsubscriberidgetmodel() {
		$segmentid = $_POST['segmentid'];
		$data= '';
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('subscribersid,autoupdate');
		$this->db->from('segments');
		$this->db->where('segments.segmentsid',$segmentid);
		$this->db->where("FIND_IN_SET('$industryid',segments.industryid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data = array('subscriberid'=>$row->subscribersid,'autoupdate'=>$row->autoupdate);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//mobile number get
	public function mobilenumbergetmodel() {
		$subscriberid = $_POST['subscriberid'];
		$industryid = $this->Basefunctions->industryid;
		$exp = explode(',',$subscriberid);
		$data= array();
		for($i=0;$i<count($exp);$i++) {
			$this->db->select('subscribersid,mobilenumber');
			$this->db->from('subscribers');
			$this->db->where_in('subscribers.subscribersid',array($exp[$i]));
			$this->db->where('subscribers.status',1);
			$this->db->where("FIND_IN_SET('$industryid',subscribers.industryid) >", 0);
			$result = $this->db->get();
			if($result->num_rows() > 0){
				foreach($result->result() as $row) {
					$data[] = $row->mobilenumber;
				}
			}
		}
		if($data != '') {
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//template msg details
	public function fetchtemplatedetailsmodel() {
		$templateid = $_GET['templid'];
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('templates.leadtemplatecontent_editorfilename,templates.moduleid',false);
		$this->db->from('templates');
		$this->db->where('templates.Status',1);
		$this->db->where("FIND_IN_SET('$industryid',templates.industryid) >", 0);
		if($templateid != '') {
			$this->db->where('templates.templatesid',$templateid);
		}
		$result = $this->db->get();
		if($result->num_rows() >0) {		
			foreach($result->result()as $row) {
				$data=array('msg'=>$row->leadtemplatecontent_editorfilename,'moduleid'=>$row->moduleid);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//sms signature drop down value fetch
	public function smssignatureddvalfetchmodel() {
		$i=0;
		$industryid = $this->Basefunctions->industryid;
		$userid = $this->Basefunctions->userid;
		$this->db->select('signatureid,signaturename,smssignature');
		$this->db->from('signature');
		$this->db->where('signature.employeeid',$userid);
		$this->db->where('signature.status',1);
		$this->db->where("FIND_IN_SET('$industryid',signature.industryid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data[$i] = array('datasid'=>$row->signatureid,'dataname'=>$row->signaturename,'smssig'=>$row->smssignature);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	 //sms merge template content information
	public function smsmergcontinformationfetchmodel() {
		$tempid = $_POST['leadtemplateid'];
		$moduleid = $_POST['moduleid'];
		$parenttable = $this->Basefunctions->generalinformaion('module','modulemastertable','moduleid',$moduleid);
		$recordid = $_POST['recordid'];
		$mergerid = explode(',',$recordid);
		$mergetemp = $_POST['content'];
		$mergegrp = explode(',',$mergetemp);
		$primaryset = $parenttable.'.'.$parenttable.'id';
		for($i=0;$i<count($mergerid);$i++) {
			$print_data=array('templateid'=>$tempid,'Templatetype'=>'Printtemp','primaryset'=>$primaryset,'primaryid'=>$mergerid[$i]);
			$resultset[$i] = $this->smsgenerateprinthtmlfile($print_data,$mergetemp,$moduleid);
		}
		echo json_encode($resultset);
	}
	//preview and print pdf
	public function smsgenerateprinthtmlfile($print_data,$mergetemp,$moduleid) {
		$this->load->model('Printtemplates/Printtemplatesmodel');
		//defaults
		$printdetails['moduleid'] = '1';
		$htmldatacontents='';
		$industryid = $this->Basefunctions->industryid;
		$templateid = ( (isset($print_data['templateid']))? $print_data['templateid'] : 1);
		$id = ( (isset($print_data['primaryid']))? $print_data['primaryid'] : 1);
		$this->db->select('templates.leadtemplatecontent_editorfilename,templates.templatesid,templates.moduleid,templates.templatesname');
		$this->db->from('templates');
		$this->db->where('templates.templatesid',$templateid);
		$this->db->where("FIND_IN_SET('$industryid',templates.industryid) >", 0);
		$result = $this->db->get();
		foreach($result->result() as $rowdata) {
			$printdetails['contentfile'] =  $rowdata->leadtemplatecontent_editorfilename;
			$printdetails['moduleid'] =  $rowdata->moduleid;
			$printdetails['templatename'] = $rowdata->templatesname;
		}
		$parenttable = $this->Basefunctions->generalinformaion('module','modulemastertable','moduleid',$printdetails['moduleid']);
		//body data
		$bodycontent = $this->Printtemplatesmodel->filecontentfetch($printdetails['contentfile']);
		$bodycontent = preg_replace('~a10s~','&nbsp;', $bodycontent);
		$bodycontent = preg_replace('~<br>~','<br />', $bodycontent);
		$bodycontent = $this->Printtemplatesmodel->removespecialchars($bodycontent);
		$bodyhtml = $this->Printtemplatesmodel->generateprintingdata($bodycontent,$parenttable,$print_data,$printdetails['moduleid']);
		$bodyhtml = $this->Printtemplatesmodel->addspecialchars($bodyhtml);
		return $bodyhtml;
	}
	//Filed check
	public function tablefieldnamecheck($tblname,$fieldname) {
		$fields = $this->db->field_exists($fieldname,$tblname);
		if($fields == '1') {
			return 'Yes';
		} else {
			return 'No';
		}
	}
	//merge content data fetch
	public function mergcontinformationfetchmodel($mergegrp,$parenttable,$id,$moduleid) {
		$relmodids = $this->fetchrelatedmodulelist($moduleid);
		$primaryid = $id;
		$database = $this->db->database;
		$resultset = array();
		$mergeindval = $mergegrp;
		$formatortype = "1";
		$id = $primaryid;
		//parent child field concept
		$elname = array('parentcategoryid','parentstoragecategoryid','parentaccountid');
		$fname = array('categoryname','storagecategoryname','accountname');
		$fnameid = array('categoryid','storagecategoryid','accountid');
		$tname = array('category','storagecategory','account');
		$empid = $this->Basefunctions->userid;
			$newdata = array();
			$tempval = substr($mergeindval,1,(strlen($mergeindval)-2));
			$newval = explode('.',$tempval);
			$reltabname = explode(':',$newval[0]);
			$table = $reltabname[0];
			$uitypeid = 2;
			//chk editor field name
			$chkeditfname = explode('_',$newval[1]);
			$editorfname = ( (count($chkeditfname) >= 2)? $chkeditfname[1] : '');
			if($table!='REL' && $table!='GEN') { //parent table fields information
				$tablename = trim($reltabname[0]);
				$tabfield = $newval[1];
				//field fetch parent table fetch
				$fieldnamechk = explode('-',$tabfield);
				$fldname = ( (count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
				$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
				$uitypeid = ( (count($fieldinfo) > 0)? $fieldinfo['uitype'] : 1);
				if($fldname != 'attributesetid') {
					//check its parent child field concept
					if(in_array($fldname,$elname)) {
						$key = array_search($fldname,$elname);
						$pid = $id;
						if($key != NULL) {
							//parent id get
							$result = $this->db->select("$tname[$key].$elname[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$id)->where($tname[$key].'.status',1)->get();
							if($result->num_rows() > 0) {
								foreach($result->result()as $row) {
									$pid=$row->$elname[$key];
								}
								if($pid!=0) {
									//parent name
									$result = $this->db->select("$tname[$key].$fname[$key],$tname[$key].$fnameid[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$pid)->where($tname[$key].'.status',1)->get();
									if($result->num_rows() > 0) {
										foreach($result->result()as $row) {
											$resultset[]=$row->$fname[$key];
										}
									}
								}
							}
						}
					}
					else {
						//field fetch parent table fetch
						$fieldnamechk = explode('-',$tabfield);
						$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
						$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
						if(count($fieldinfo)>0) {
							$uitypeid = $fieldinfo['uitype'];
							//fetch parent table join id
							$relpartable = $fieldinfo['partab'];
							$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
							$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
							$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
							//fetch original field of picklist data in view creation 
							$viewfieldinfo = $this->viewfieldsinformation($tablename,$tabfield,$fieldinfo['modid']);
							$tabfieldname = ( (count($viewfieldinfo)>0)? $viewfieldinfo['joinfieldname'] : $tabfield );
							$jointabfieldid = ( (count($viewfieldinfo)>0)? $viewfieldinfo['condfiledname'] : $tabfield );
							//join relation table and main table
							$joinq="";
							if($relpartable!=$tablename) {
								$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id AND '.$relpartable.'.status=1';
							}
							//main table join
							if($mainjointable!=$parenttable) {
								$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id AND '.$relpartable.'.status=1';
							}
							//if its picklist data
							$newfield = substr( $tabfieldname,( strlen($tabfieldname)-2 ));
							$tabname = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
							$tbachk = $this->tableexitcheck($database,$tabname);
							if( $newfield == "id" && $tbachk != 0 ) {
								$newtable = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
								$newjfield = $newtable."name";
								$whtable = (($tablename=="company" || $tablename=="branch")? $tablename : $parenttable);
								$newjdata = $this->db->query("SELECT ".$newtable.".".$newtable."id AS columdataid,".$newtable.".".$newjfield." AS columdata FROM ".$newtable." LEFT OUTER JOIN ".$tablename." ON ".$tablename.".".$tabfieldname." = ".$newtable.".".$tabfieldname."".$joinq." WHERE ".$tablename.".".$whtable."id = ".$id." AND ".$newtable.".status=1 AND ".$tablename.".status=1",false);
								if($newjdata->num_rows() >0) {
									foreach($newjdata->result() as $jkey) {
										$resultset[] = $jkey->columdata;
									}
								} else {
									$resultset[] = " ";
								}
							}
							else {
								//main table data sets information fetching
								$fldcountchk = explode('-',$tabfield);
								$tablcolumncheck = $this->tablefieldnamecheck($tablename,$parenttable."id");
								if( ( $parenttable == $tablename && (count($fldcountchk)) < 2 ) || ( $tablcolumncheck == 'false' && (count($fldcountchk)) < 2 ) ) {
									$newdata = $this->db->select("".$tabfield." AS columdata")->from($tablename)->where($tablename.".".$tablename."id",$id)->where($tablename.".status",1)->get()->result();
								}
								else {
									$cond = "";
									$addfield = explode('-',$tabfield);
									if( (count($addfield)) >= 2 ) {
										$datafiledname=$addfield[1];
										if(in_array('PR',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=2';
										} else if(in_array('SE',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=3'; 
										} else if(in_array('BI',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=4';
										} else if(in_array('SH',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=5';
										} else if(in_array('CW',$addfield)) {
											$formatortype = "CW";
										} else if(in_array('CF',$addfield)) {
											$formatortype = "CF";
										} else if(in_array('CS',$addfield)) {
											$formatortype = "CS";
										}
										if($tablename!=$parenttable) {
											$tablcolumncheck = $this->tablefieldnamecheck($tablename,"addresstypeid");
											$cond = ( ($tablcolumncheck == 'true')? $cond : ' ');
											$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename." JOIN ".$parenttable." ON ".$parenttable.".".$parenttable."id = ".$tablename.".".$parenttable."id WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1".$cond."")->result();
										} else {
											$newdata = $this->db->select(''.$datafiledname.' AS columdata',false)->from($tablename)->where($tablename.'id',$id)->where($tablename.'.status',1)->get()->result();
										}
									}
									else {
										$newdata = $this->db->query("SELECT ".$tabfield." AS columdata FROM ".$tablename." JOIN ".$parenttable." ON ".$parenttable.".".$parenttable."id = ".$tablename.".".$parenttable."id WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1")->result();
									}
								}
								foreach($newdata as $key) {
									if($tabfield == 'companylogo') {
										$imgname = $key->columdata;
										if(filter_var($imgname,FILTER_VALIDATE_URL)) {
											$resultset[] = '<img src="'.$imgname.'" />';
										} else {
											if( file_exists($imgname) ) {
												$resultset[] = '<img src="'.$imgname.'" style="width:11em; height:3em" />';
											} else {
												$resultset[] = " ";
											}
										}
									}
									else {
										$currencyinfo = $this->currencyinformation($parenttable,$id,$moduleid);
										if($formatortype=="CW") { //convert to word
											if(count($currencyinfo)>0) {
												$resultset[] = $this->convert->numtowordconvert($key->columdata,$currencyinfo['name'],$currencyinfo['subcode']);
											} else {
												$resultset[] = $this->convert->numtowordconvert($key->columdata);
											}
										} else if($formatortype=="CF") { //currenct format
											if(count($currencyinfo)>0) {
												$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
											} else {
												$resultset[] = $this->convert->formatcurrency($key->columdata);
											}
										} else if($formatortype=="CS") { //currency with symbol
											if(count($currencyinfo)>0) {
												$amt = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
												$resultset[] = $currencyinfo['symbol'].' '.$amt;
											} else {
												$resultset[] = $currencyinfo['symbol'].' '.$this->convert->formatcurrency($key->columdata);
											}
										} else {
											if(is_numeric($key->columdata)) {
												if($uitypeid == '4' || $uitypeid == '5' || $uitypeid == '7') {
													if(count($currencyinfo)>0) {
														$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
													} else {
														$resultset[] = $this->convert->formatcurrency($key->columdata);
													}
												} else {
													$resultset[] = $key->columdata;
												}
											} else {
												if($editorfname=='editorfilename') {
													if( file_exists($key->columdata) ) {
														$datas = $this->geteditorfilecontent($key->columdata);
														$datas = preg_replace('~a10s~','&nbsp;', $datas);
														$datas = preg_replace('~<br>~','<br />', $datas);
														$resultset[] = $datas;
													} else {
														$resultset[] = '';
													}
												} else {
													$resultset[] = $key->columdata;
												}
											}
										}
									}
								}
							}
						}
					}
				} 
				else {
					//attribute value fetch for non relational modules
					$fieldnamechk = explode('-',$tabfield);
					$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
					$fieldinfo = $this->fetchfieldparenttable($table,$fldname);
					if(count($fieldinfo)>0) {
						$uitypeid = $fieldinfo['uitype'];
						//fetch parent table join id
						$relpartable = $fieldinfo['partab'];
						$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
						$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
						$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
						//join relation table and main table
						$joinq="";
						if($parenttable!=$table) {
							$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$table.'.'.$parenttable.'id';
						}
						//fetch attribute name and values
						$i=0;
						$datasets = array();
						$attrsetids = $this->db->query(" SELECT ".$table.".".$table."id as Id,".$table.".attributesetid as attrsetid,".$table.".attributes as attributeid,".$table.".attributevalues as attributevalueid FROM ".$table.$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$mainjointable.".status=1 AND ".$table.".status=1",false)->result();
						foreach($attrsetids as $attrset) {
							$datasets[$i] = array('id'=>$attrset->Id,'attrsetid'=>$attrset->attrsetid,'attrnames'=>$attrset->attributeid,'attrvals'=>$attrset->attributevalueid);
							$i++;
						}
						$resultset = array();
						foreach($datasets as $set) {
							$valid = explode('_',$set['attrvals']);
							$nameid = explode('_',$set['attrnames']);
							$n=1;
							$vi = 0;
							$attrdatasets = array();
							for($h=0;$h<count($valid);$h++) {
								$attrval = '';
								$attrname = '';
								if($valid[$h]!='') {
									//attribute value fetch
									$attrvaldata = $this->db->query(' SELECT attributevalueid,attributevaluename FROM attributevalue WHERE attributevalueid='.$valid[$h].' AND status=1',false)->result();
									foreach($attrvaldata as $valsets) {
										$attrval = $valsets->attributevaluename;
									}
									//attribute name fetch
									$attrnamedata = $this->db->query(' SELECT attributeid,attributename FROM attribute WHERE attributeid='.$nameid[$h].' AND status=1',false)->result();
									foreach($attrnamedata as $namesets) {
										$attrname = $namesets->attributename;
									}
								}
								$attrset = "";
								if( $attrval!='' ) {
									$attrset = $n.'.'.$attrname.': '.$attrval;
									++$n;
								}
								$attrdatasets[$vi] = $attrset;
								$vi++;
							}
							$resultset[] = implode('<br />',$attrdatasets);
						}
					}
				}
			} 
			else if($table=='REL') { //related module fields information fetch
				$tablename = trim($reltabname[1]);
				$tabfield = trim($newval[1]);
				$fieldnamechk = explode('-',$tabfield);
				$fldname = ( (count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
				if($fldname != 'attributesetid') {
					//check its parent child field concept
					$fldnamechk = explode('-',$tabfield);
					$fldname = ( (count($fldnamechk) >= 2)? $fldnamechk[1] : $tabfield);
					if(in_array($fldname,$elname)) {
						$key = array_search($fldname,$elname);
						$pid = $id;
						if($key != NULL) {
							//parent id get
							$result = $this->db->select("$tname[$key].$elname[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$id)->where($tname[$key].'.status',1)->get();
							if($result->num_rows() > 0) {
								foreach($result->result()as $row) {
									$pid=$row->$elname[$key];
								}
								if($pid!=0) {
									//parent name
									$result = $this->db->select("$tname[$key].$fname[$key],$tname[$key].$fnameid[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$pid)->where($tname[$key].'.status',1)->get();
									if($result->num_rows() > 0) {
										foreach($result->result()as $row) {
											$resultset[]=$row->$fname[$key];
										}
									}
								}
							}
						}
					}
					else {
						//field fetch parent table fetch
						$fieldnamechk = explode('-',$tabfield);
						$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
						$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
						if(count($fieldinfo)>0) {
							$uitypeid = $fieldinfo['uitype'];
							//fetch parent table join id
							$relpartable = $fieldinfo['partab'];
							$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
							$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
							$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
							//fetch original field of picklist data in view creation 
							$viewfieldinfo = $this->viewfieldsinformation($tablename,$tabfield,$fieldinfo['modid']);
							$tabfieldname = ( (count($viewfieldinfo)>0)? $viewfieldinfo['joinfieldname'] : $tabfield );
							$jointabfieldid = ( (count($viewfieldinfo)>0)? $viewfieldinfo['condfiledname'] : $tabfield );
							//join relation table and main table
							$joinq="";
							if($relpartable!=$tablename) {
								$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id AND '.$relpartable.'.status=1';
							}
							//main table join
							if($mainjointable==$parenttable) {
								$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id AND '.$mainjointable.'.status=1';
							} else {
								$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id AND '.$mainjointable.'.status=1';
								$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$mainjointable.'.'.$parenttable.'id AND '.$parenttable.'.status=1';
							}
							//if its picklist data
							$newfield = substr( $tabfieldname,( strlen($tabfieldname)-2 ));
							$tabname = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
							$tblchk = $this->tableexitcheck($database,$tabname);
							if( $newfield == "id" && $tblchk != 0 ) {
								$newtable = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
								$newjfield = $newtable."name";
								$whtable = (($tablename=="company" || $tablename=="branch")? $tablename : $parenttable);
								$newjdata = $this->db->query("SELECT ".$newtable.".".$newtable."id AS columdataid,".$newtable.".".$newjfield." AS columdata FROM ".$newtable." LEFT OUTER JOIN ".$tablename." ON ".$tablename.".".$jointabfieldid." = ".$newtable.".".$tabfieldname."".$joinq." WHERE ".$parenttable.".".$whtable."id = ".$id." AND ".$mainjointable.".status=1 AND ".$newtable.".status=1 AND ".$tablename.".status=1");
								if($newjdata->num_rows() >0) {
									foreach($newjdata->result() as $jkey) {
										$resultset[] = $jkey->columdata;
									}
								} else {
									$resultset[] = " ";
								}
							}
							else {
								//main table data sets information fetching
								$fldcountchk = explode('-',$tabfield);
								$tablcolumncheck = $this->tablefieldnamecheck($tablename,$parenttable."id");
								if( ( $parenttable == $tablename && (count($fldcountchk)) < 2 ) || ( $tablcolumncheck == 'false' && (count($fldcountchk)) < 2 ) ) {
									$newdata = $this->db->query("SELECT ".$tablename.".".$tabfield." AS columdata FROM ".$tablename."".$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1")->result();
								}
								else {
									$cond = "";
									$addfield = explode('-',$tabfield);
									if( (count($addfield)) >= 2 ) {
										$datafiledname=$addfield[1];
										if(in_array('PR',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=2';
										} else if(in_array('SE',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=3'; 
										} else if(in_array('BI',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=4';
										} else if(in_array('SH',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=5';
										} else if(in_array('CW',$addfield)) {
											$formatortype = "CW";
										} else if(in_array('CF',$addfield)) {
											$formatortype = "CF";
										} else if(in_array('CS',$addfield)) {
											$formatortype = "CS";
										}
										$tablcolumncheck = $this->tablefieldnamecheck($tablename,"addresstypeid");
										$cond = ( ($tablcolumncheck == 'true')? $cond : ' ');
										if($tablename!=$parenttable) {
											$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename."".$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1".$cond."")->result();
										} else {
											$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename." WHERE ".$tablename."id = ".$id." AND ".$tablename.".status=1")->result();
										}
									}
									else {
										$newdata = $this->db->query("SELECT ".$tabfield." AS columdata FROM ".$tablename."".$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$tablename.".status=1 AND ".$parenttable.".status=1".$cond."")->result();
									}
								}
								foreach($newdata as $key) {
									if($tabfield == 'companylogo') {
										$imgname = $key->columdata;
										if(filter_var($imgname, FILTER_VALIDATE_URL)) {
											$resultset[] = '<img src="'.$imgname.'" />';
										} else {
											if( file_exists($imgname) ) {
												$resultset[] = '<img src="'.$imgname.'" style="width:11em; height:3em" />';
											} else {
												$resultset[] = " ";
											}
										}
									}
									else {
										$currencyinfo = $this->currencyinformation($parenttable,$id,$moduleid);
										if($formatortype=="CW") { //convert to word
											if(count($currencyinfo)>0) {
												$resultset[] = $this->convert->numtowordconvert($key->columdata,$currencyinfo['name'],$currencyinfo['subcode']);
											} else {
												$resultset[] = $this->convert->numtowordconvert($key->columdata);
											}
										} else if($formatortype=="CF") { //currenct format
											if(count($currencyinfo)>0) {
												$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
											} else {
												$resultset[] = $this->convert->formatcurrency($key->columdata);
											}
										} else if($formatortype=="CS") { //currency with symbol
											if(count($currencyinfo)>0) {
												$amt = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
												$resultset[] = $currencyinfo['symbol'].' '.$amt;
											} else {
												$resultset[] = $currencyinfo['symbol'].' '.$this->convert->formatcurrency($key->columdata);
											}
										} else {
											if(is_numeric($key->columdata)) {
												if( $uitypeid == '4' || $uitypeid == '5' || $uitypeid == '7' ) {
													if(count($currencyinfo)>0) {
														$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
													} else {
														$resultset[] = $this->convert->formatcurrency($key->columdata);
													}
												} else {
													$resultset[] = $key->columdata;
												}
											} else {
												if($editorfname=='editorfilename') {
													if( file_exists($key->columdata) ) {
														$datas = $this->geteditorfilecontent($key->columdata);
														$datas = preg_replace('~a10s~','&nbsp;', $datas);
														$datas = preg_replace('~<br>~','<br />', $datas);
														$resultset[] = $datas;
													} else {
														$resultset[] = '';
													}
												} else {
													$resultset[] = $key->columdata;
												}
											}
										}
									}
								}
							}
						}
					}
				} 
				else {
					//attribute value fetch
					$fieldnamechk = explode('-',$tabfield);
					$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
					$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
					if(count($fieldinfo)>0) {
						$uitypeid = $fieldinfo['uitype'];
						//fetch parent table join id
						$relpartable = $fieldinfo['partab'];
						$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
						$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $parenttable );
						$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
						//join relation table and main table
						$joinq="";
						if($relpartable!=$tablename) {
							$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id';
						}
						//main table join
						if($mainjointable==$parenttable) {
							$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id';
						} else {
							$joinq .= ' LEFT OUTER JOIN '.$mainjointable.' ON '.$mainjointable.'.'.$maintabjoincolname.'='.$relpartable.'.'.$relpartable.'id';
							$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$mainjointable.'.'.$parenttable.'id';
						}
						//fetch attribute name and values
						$i=0;
						$datasets = array();
						$attrsetids = $this->db->query(" SELECT ".$tablename.".".$tablename."id as Id,".$tablename.".attributesetid as attrsetid,".$tablename.".attributes as attributeid,".$tablename.".attributevalues as attributevalueid FROM ".$tablename.$joinq." WHERE ".$parenttable.".".$parenttable."id = ".$id." AND ".$mainjointable.".status=1 AND ".$tablename.".status=1",false)->result();
						foreach($attrsetids as $attrset) {
							$datasets[$i] = array('id'=>$attrset->Id,'attrsetid'=>$attrset->attrsetid,'attrnames'=>$attrset->attributeid,'attrvals'=>$attrset->attributevalueid);
							$i++;
						}
						$resultset = array();
						foreach($datasets as $set) {
							$valid = explode('_',$set['attrvals']);
							$nameid = explode('_',$set['attrnames']);
							$n=1;
							$vi = 0;
							$attrdatasets = array();
							for($h=0;$h<count($valid);$h++) {
								$attrval = '';
								$attrname = '';
								if($valid[$h]!='') {
									//attribute value fetch
									$attrvaldata = $this->db->query(' SELECT attributevalueid,attributevaluename FROM attributevalue WHERE attributevalueid='.$valid[$h].' AND status=1',false)->result();
									foreach($attrvaldata as $valsets) {
										$attrval = $valsets->attributevaluename;
									}
									//attribute name fetch
									$attrnamedata = $this->db->query(' SELECT attributeid,attributename FROM attribute WHERE attributeid='.$nameid[$h].' AND status=1',false)->result();
									foreach($attrnamedata as $namesets) {
										$attrname = $namesets->attributename;
									}
								}
								$attrset = "";
								if( $attrval!='' ) {
									$attrset = $n.'.'.$attrname.': '.$attrval;
									++$n;
								}
								$attrdatasets[$vi] = $attrset;
								$vi++;
							}
							$resultset[] = implode('<br />',$attrdatasets);
						}
					}
				}
			}
			else if($table=='GEN') {
				$tablename = trim($reltabname[1]);
				$tabfield = $newval[1];
				//field fetch parent table fetch
				$fieldnamechk = explode('-',$tabfield);
				$fldname = ( (count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
				$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
				$uitypeid = ( (count($fieldinfo) > 0)? $fieldinfo['uitype'] : 1);
				if($fldname != 'attributesetid') {
					//get user company & branch id
					$tabinfo = array('company','companycf','branch','branchcf');
					if( in_array($tablename,$tabinfo) ) {
						if($tablename == 'company' || $tablename == 'companycf') {
							$compquery = $this->db->query('select company.companyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND company.status=1')->result();
							foreach($compquery as $key) {
								$id = $key->companyid;
							}
						} else if($tablename == 'branch' || $tablename == 'branchcf') {
							$brquery = $this->db->query('select branch.branchid from branch join employee ON employee.branchid=branch.branchid where employeeid='.$empid.' AND branch.status=1')->result();
							foreach($brquery as $key) {
								$id = $key->branchid;
							}
						} else { //employee
							$id = $this->Basefunctions->userid;
						}
					} else { //employee
						$id = $this->Basefunctions->userid;
					}
					//check its parent child field concept
					if(in_array($fldname,$elname)) {
						$key = array_search($fldname,$elname);
						$pid = $id;
						if($key != NULL) {
							//parent id get
							$result = $this->db->select("$tname[$key].$elname[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$id)->where($tname[$key].'.status',1)->get();
							if($result->num_rows() > 0) {
								foreach($result->result()as $row) {
									$pid=$row->$elname[$key];
								}
								if($pid!=0) {
									//parent name
									$result = $this->db->select("$tname[$key].$fname[$key],$tname[$key].$fnameid[$key]")->from($tname[$key])->where($tname[$key].'.'.$fnameid[$key],$pid)->where($tname[$key].'.status',1)->get();
									if($result->num_rows() > 0) {
										foreach($result->result()as $row) {
											$resultset[]=$row->$fname[$key];
										}
									}
								}
							}
						}
					}
					else {
						//field fetch parent table fetch
						$fieldnamechk = explode('-',$tabfield);
						$fldname = ((count($fieldnamechk) >= 2)? $fieldnamechk[1] : $tabfield);
						$fieldinfo = $this->fetchfieldparenttable($tablename,$fldname);
						$moduleid = $fieldinfo['modid'];
						if(count($fieldinfo)>0) {
							$uitypeid = $fieldinfo['uitype'];
							//fetch parent table join id
							$relpartable = $fieldinfo['partab'];
							$partabjoinname = $this->fetchjoincolmodelname($relpartable,$moduleid);
							$mainjointable = ( ( count($partabjoinname)>0 )? $partabjoinname['jointabl'] : $relpartable );
							$maintabjoincolname = ( ( count($partabjoinname)>0 )? $partabjoinname['joinfieldname'] : $relpartable.'id' );
							//fetch original field of picklist data in view creation 
							$viewfieldinfo = $this->viewfieldsinformation($tablename,$tabfield,$fieldinfo['modid']);
							$tabfieldname = ( (count($viewfieldinfo)>0)? $viewfieldinfo['joinfieldname'] : $tabfield );
							$jointabfieldid = ( (count($viewfieldinfo)>0)? $viewfieldinfo['condfiledname'] : $tabfield );
							//join relation table and main table
							$joinq="";
							if($relpartable!=$tablename) {
								$joinq .= ' LEFT OUTER JOIN '.$relpartable.' ON '.$relpartable.'.'.$relpartable.'id='.$tablename.'.'.$relpartable.'id';
							}
							//if its picklist data
							$newfield = substr( $tabfieldname,( strlen($tabfieldname)-2 ));
							$tabname = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
							$tbachk = $this->tableexitcheck($database,$tabname);
							if( $newfield == "id" && $tbachk != 0 ) {
								$newtable = substr( $tabfieldname,0,( strlen($tabfieldname)-2 ) );
								$newjfield = $newtable."name";
								$whtable = (($tablename=="company" || $tablename=="branch")? $tablename : $relpartable);
								$newjdata = $this->db->query("SELECT ".$newtable.".".$newtable."id AS columdataid,".$newtable.".".$newjfield." AS columdata FROM ".$newtable." LEFT OUTER JOIN ".$tablename." ON ".$tablename.".".$tabfieldname." = ".$newtable.".".$tabfieldname."".$joinq." WHERE ".$tablename.".".$whtable."id = ".$id." AND ".$newtable.".status=1 AND ".$tablename.".status=1",false);
								if($newjdata->num_rows() >0) {
									foreach($newjdata->result() as $jkey) {
										$resultset[] = $jkey->columdata;
									}
								} else {
									$resultset[] = " ";
								}
							}
							else {
								//main table data sets information fetching
								$fldcountchk = explode('-',$tabfield);
								$tablcolumncheck = $this->tablefieldnamecheck($tablename,$relpartable."id");
								if( ( $relpartable == $tablename && (count($fldcountchk)) < 2 ) || ( $tablcolumncheck == 'false' && (count($fldcountchk)) < 2 ) ) {
									$newdata = $this->db->select("".$tabfield." AS columdata")->from($tablename)->where($tablename.".".$tablename."id",$id)->where($tablename.".status",1)->get()->result();
								}
								else {
									$cond = "";
									$addfield = explode('-',$tabfield);
									if( (count($addfield)) >= 2 ) {
										$datafiledname=$addfield[1];
										if(in_array('PR',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=2';
										} else if(in_array('SE',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=3'; 
										} else if(in_array('BI',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=4';
										} else if(in_array('SH',$addfield)) {
											$cond = ' AND '.$tablename.'.addresstypeid=5';
										} else if(in_array('CW',$addfield)) {
											$formatortype = "CW";
										} else if(in_array('CF',$addfield)) {
											$formatortype = "CF";
										} else if(in_array('CS',$addfield)) {
											$formatortype = "CS";
										}
										if($tablename!=$relpartable) {
											$tablcolumncheck = $this->tablefieldnamecheck($tablename,"addresstypeid");
											$cond = ( ($tablcolumncheck == 'true')? $cond : ' ');
											$newdata = $this->db->query("SELECT ".$datafiledname." AS columdata FROM ".$tablename." JOIN ".$relpartable." ON ".$relpartable.".".$relpartable."id = ".$tablename.".".$relpartable."id WHERE ".$relpartable.".".$relpartable."id = ".$id." AND ".$tablename.".status=1 AND ".$relpartable.".status=1".$cond."")->result();
										} else {
											$newdata = $this->db->select(''.$datafiledname.' AS columdata',false)->from($tablename)->where($tablename.'id',$id)->where($tablename.'.status',1)->get()->result();
										}
									}
									else {
										$newdata = $this->db->query("SELECT ".$tabfield." AS columdata FROM ".$tablename." JOIN ".$relpartable." ON ".$relpartable.".".$relpartable."id = ".$tablename.".".$relpartable."id WHERE ".$relpartable.".".$relpartable."id = ".$id." AND ".$tablename.".status=1 AND ".$relpartable.".status=1")->result();
									}
								}
								foreach($newdata as $key) {
									if($tabfield == 'companylogo') {
										$imgname = $key->columdata;
										if(filter_var($imgname,FILTER_VALIDATE_URL)) {
											$resultset[] = '<img src="'.$imgname.'" />';
										} else {
											if( file_exists($imgname) ) {
												$resultset[] = '<img src="'.$imgname.'" style="width:11em; height:3em" />';
											} else {
												$resultset[] = " ";
											}
										}
									}
									else {
										if($formatortype=="CW") { //convert to word
											$currencyinfo = $this->currencyinformation($tablename,$id,$moduleid);
											if(count($currencyinfo)>0) {
												$resultset[] = $this->convert->numtowordconvert($key->columdata,$currencyinfo['name'],$currencyinfo['subcode']);
											} else {
												$resultset[] = $this->convert->numtowordconvert($key->columdata);
											}
										} else if($formatortype=="CF") { //currenct format
											$currencyinfo = $this->currencyinformation($tablename,$id,$moduleid);
											if(count($currencyinfo)>0) {
												$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
											} else {
												$resultset[] = $this->convert->formatcurrency($key->columdata);
											}
										} else if($formatortype=="CS") { //currency with symbol
											$currencyinfo = $this->currencyinformation($tablename,$id,$moduleid);
											if(count($currencyinfo)>0) {
												$amt = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
												$resultset[] = $currencyinfo['symbol'].' '.$amt;
											} else {
												$resultset[] = $currencyinfo['symbol'].' '.$this->convert->formatcurrency($key->columdata);
											}
										} else {
											$currencyinfo = $this->currencyinformation($tablename,$id,$moduleid);
											if(is_numeric($key->columdata)) {
												if($uitypeid == '4' || $uitypeid == '5' || $uitypeid == '7') {
													if(count($currencyinfo)>0) {
														$resultset[] = $this->convert->formatcurrency($key->columdata,$currencyinfo['code']);
													} else {
														$resultset[] = $this->convert->formatcurrency($key->columdata);
													}
												} else {
													$resultset[] = $key->columdata;
												}
											} else {
												if($editorfname=='editorfilename') {
													if( file_exists($key->columdata) ) {
														$datas = $this->geteditorfilecontent($key->columdata);
														$datas = preg_replace('~a10s~','&nbsp;', $datas);
														$datas = preg_replace('~<br>~','<br />', $datas);
														$resultset[] = $datas;
													} else {
														$resultset[] = '';
													}
												} else {
													$resultset[] = $key->columdata;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		return $resultset;
	}
	//table name check
	public function tableexitcheck($database,$newtable) {
		$counttab=0;
		$tabexits = $this->db->query("SELECT COUNT(*) AS tabcount FROM information_schema.tables WHERE table_schema = '".$database."'AND table_name = '".$newtable."'");
		foreach($tabexits->result() as $tkey) { 
			$counttab = $tkey->tabcount;
		};
		return $counttab;
	}
	//fetch parent table information
	public function fetchfieldparenttable($tablname,$fieldname) {
		$pardata = array();
		$datas = $this->db->query('SELECT modulefieldid,uitypeid,parenttable,moduletabid  FROM modulefield WHERE columnname LIKE "%'.$fieldname.'%" AND tablename LIKE "%'.$tablname.'%" AND status IN (1,10)',false);
		foreach($datas->result() as $data) {
			$pardata = array('uitype'=>$data->uitypeid,'partab'=>$data->parenttable,'modid'=>$data->moduletabid);
		}
		return $pardata;
	}
	//fetch related module information
	public function fetchrelatedmodulelist($moduleid) {
		$relmodids = array();
		$relmoduleids = $this->db->select('modulerelationid,relatedmoduleid')->from('modulerelation')->where('moduleid',$moduleid)->get();
		foreach($relmoduleids->result() as $reldata) {
			$relmodids[] = $reldata->relatedmoduleid;
		}
		return $relmodids;
	}
	//fetch main table join information
	public function fetchjoincolmodelname($relpartable,$moduleid) {
		$joindata = array();
		$datas = $this->db->query('SELECT modulerelationid,parentfieldname,parenttable FROM modulerelation WHERE moduleid = '.$moduleid.' AND relatedtable LIKE "%'.$relpartable.'%" AND status=1',false);
		foreach($datas->result() as $data) {
			$joindata = array('relid'=>$data->modulerelationid,'joinfieldname'=>$data->parentfieldname,'jointabl'=>$data->parenttable);
		}
		return $joindata;
	}
	//fetch relation and view type 3 field information
	public function viewfieldsinformation($tablename,$tabfield,$modid) {
		$datasets = array();
		$viewfieldinfo = $this->db->query('SELECT viewcreationcolumnid,viewcreationparenttable,viewcreationjoincolmodelname,viewcreationcolmodelcondname FROM viewcreationcolumns WHERE viewcreationparenttable LIKE "%'.$tablename.'%" AND viewcreationcolmodelcondname LIKE "%'.$tabfield.'%" AND viewcreationtype=3 AND viewcreationmoduleid='.$modid.' AND status=1');
		foreach($viewfieldinfo->result() as $viewdata) {
			$datasets = array('viewcolid'=>$viewdata->viewcreationcolumnid,'joinfieldname'=>$viewdata->viewcreationjoincolmodelname,'condfiledname'=>$viewdata->viewcreationcolmodelcondname);
		}
		return $datasets;
	}
	//fetch currency information
	public function currencyinformation($parenttable,$id,$moduleid) {
		$datasets = array();
		$colmodeltable = "";
		$cid = 1;
		//modulerelation
		$datasets = $this->db->select('viewcreationcolumnid,viewcreationcolmodeltable,viewcreationjoincolmodelname,viewcreationparenttable,viewcreationtype,viewcreationcolmodelcondname',false)->from('viewcreationcolumns')->where('viewcreationmoduleid',$moduleid)->where('viewcreationjoincolmodelname','currencyid')->where('status',1)->get();
		if($datasets->num_rows()>0) {
			foreach($datasets->result() as $data) {
				$colname =  $data->viewcreationcolmodeltable;
				$colmodeltable = $data->viewcreationcolmodeltable;
				$joincolmodelname= $data->viewcreationjoincolmodelname;
				$colmodelpartable = $data->viewcreationparenttable;
				$viewtype = $data->viewcreationtype;
				$viewcolcondname = $data->viewcreationcolmodelcondname;
			}
		}
		if($colmodeltable!= "") {
			$joinq="";
			if($viewtype==3) {
				if($colmodelpartable!="currency") {
					if($colmodelpartable == $parenttable) {
						$joinq = 'LEFT OUTER JOIN '.$colmodelpartable.' ON '.$colmodelpartable.'.'.$viewcolcondname.'='.$colmodeltable.'.'.$joincolmodelname;
					} else {
						$joinq = 'LEFT OUTER JOIN '.$colmodelpartable.' ON '.$colmodelpartable.'.'.$viewcolcondname.'='.$colmodeltable.'.'.$joincolmodelname;
						$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$colmodelpartable.'.'.$parenttable.'id';
					}
				}
			} else {
				if($colmodelpartable!="currency") {
					if($colmodelpartable == $parenttable) {
						$joinq = 'LEFT OUTER JOIN '.$colmodelpartable.' ON '.$colmodelpartable.'.'.$joincolmodelname.'='.$colmodeltable.'.'.$joincolmodelname;
					} else {
						$joinq = 'LEFT OUTER JOIN '.$colmodelpartable.' ON '.$colmodelpartable.'.'.$joincolmodelname.'='.$colmodeltable.'.'.$joincolmodelname;
						$joinq .= ' LEFT OUTER JOIN '.$parenttable.' ON '.$parenttable.'.'.$parenttable.'id='.$colmodelpartable.'.'.$parenttable.'id';
					}
				}
			}
			$curdatasets = $this->db->query('select currency.currencyid from currency '.$joinq.' where '.$parenttable.'id='.$id.'')->result();
			foreach($curdatasets as $data) {
				$cid = $data->currencyid;
			}
		} else {
			$empid = $this->Basefunctions->userid;
			$compquery = $this->db->query('select company.companyid,company.currencyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.'')->result();
			foreach($compquery as $key) {
				$cid = $key->currencyid;
			}
		}
		$curdatasets = $this->db->select('currencyid,currencyname,currencycode,symbol,currencycountry,subunit',false)->from('currency')->where('currencyid',$cid)->get();
		foreach($curdatasets->result() as $datas) {
			$datasets = array('id'=>$datas->currencyid,'name'=>$datas->currencyname,'code'=>$datas->currencycode,'symbol'=>$datas->symbol,'country'=>$datas->currencycountry,'subcode'=>$datas->subunit);
		}
		return $datasets;
	}
	//account credit details fetch function 
	public function accountcresitdetailsfetchmodel($apikey,$addontypeid) { 
		$this->db->select('addonavailablecredit');
		$this->db->from('salesneuronaddonscredit');
		$this->db->where('salesneuronaddonscredit.addonstypeid',$addontypeid);
		$this->db->where('salesneuronaddonscredit.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$credit = $row->addonavailablecredit;
			}
		} else {
			$credit = '0';
		}
		return $credit;
	}
	//dnd mobile number check
	public function dndmobilenumbercheckmodel() {
		$mobilenum = $_GET['mobilenum'];
		$apikey = $_GET['apikey'];
		$statusurl = "http://qfilter.sinfini.com/api/v1/api.php?api_key=".$apikey."&method=dnd&number=".$mobilenum."&format=json";
		$chk=curl_init();
		curl_setopt($chk, CURLOPT_URL, $statusurl);
		curl_setopt($chk, CURLOPT_RETURNTRANSFER, true);
		$statusoutput=curl_exec($chk);
		curl_close($chk);
		$smsstatus = explode(' ',trim($statusoutput) );
		echo $statusoutput;
	}
	//default template value fetch
	public function defeulttemplatevaluegetmodel() {
		$groupid = $_POST['groupid'];
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('templatesid,signatureid,smssettingsid,smstypeid,moduleid');
		$this->db->from('campaigngroups');
		$this->db->where('campaigngroups.campaigngroupsid',$groupid);
		$this->db->where('campaigngroups.status',1);
		$this->db->where("FIND_IN_SET('$industryid',campaigngroups.industryid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data = array('template'=>$row->templatesid,'signature'=>$row->signatureid,'senderid'=>$row->smssettingsid,'smstype'=>$row->smstypeid,'moduleid'=>$row->moduleid);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//send sms model - KALERA
	public function leadsmssendmodel() {
		$schdate="";
		$msgsenddate = "";
		$custmobile=$_GET['communicationto'];
		$message = $_GET['defaultsubseditorval'];
		$smscount=$_GET['smscount'];
		$datacontent = $_GET['datacontent'];
		$apikey = $_GET['apikey'];
		if($datacontent != 'null') {
			$mobnum = explode(',',$custmobile);
			$mobmessage = explode('|||',$message);
			$mobcount = count($mobnum);
			$smscount = $smscount * $mobcount;
		} else {
			//$mobnum = array($custmobile);
			$mobmessage = $message;
			//$mobcount = '1';
			$mobnum = explode(',',$custmobile);
			$mobcount = count($mobnum);
			$smscount = $smscount * $mobcount;
		}
		//Sender Name
		if(isset($_GET['senderid'])){
			$senderid = $_GET['senderid']; 
			if($senderid == '') { 
				$senderid = '1';
			}
		} else { 
			$senderid = '1';
		}
		if($senderid != 1) {
			$provid = $this->Basefunctions->generalinformaion('smssettings','smsprovidersettingsid','smssettingsid',$senderid);
			$apikey = $this->Basefunctions->generalinformaion('smsprovidersettings','apikey','smsprovidersettingsid',$provid);
		}
		//Sender Settings id
		if(isset($_GET['sendername'])) { 
			$sendername = $_GET['sendername']; 
		} else { 
			$sendername = '';
		}
		//sms send type 
		$smssendtype = $_GET['smssendtypeid'];
		if($smssendtype == '2') {
			$xmlurl = "https://api-alerts.kaleyra.com/v4/?api_key=";
			$newstatusurl = "https://api-alerts.kaleyra.com/v4/?api_key=";
			$addontypeid = '2';
		} else if($smssendtype == '3') {
			$xmlurl = "https://api-promo.kaleyra.com/v4/?api_key=";
			$newstatusurl = "https://api-promo.kaleyra.com/v4/?api_key=";
			//$apikey = 'Acb930e4fa7792f62b7b510d5245ce266'; // Definitely need to get API key for Promotional SMS
			$apikey = 'Acb930e4fa7792f62b7b510d8945ce288';
			$sendername = 'BULKSMS';
			$addontypeid = '4';
		} else if($smssendtype == '4') {
			$xmlurl = "https://api-alerts.kaleyra.com/v4/?api_key=$apikey&method=sms.xml&xml=";
			$newstatusurl = "https://api-alerts.kaleyra.com/v4/?api_key=$apikey&method=sms.status&id=";
			$apikey = 'A1363f16118616942397968b9a9f736da';
			$sendername = 'AUCGLO';
			$addontypeid = '5';
		}
		$credits = $this->accountcresitdetailsfetchmodel($apikey,$addontypeid);
		if( $credits >= $smscount ) {
			$msg=$_GET['description'];
			//sms type id
			if(isset($_GET['smstypeid'])) {
				$type = $_GET['smstypeid']; 
			} else { 
				$type = 2; 
			}
			if($type == 2 ) { 
				$unicode = 'N'; 
				$flash = 'N';
			} else if($type == 3 ) { 
				$unicode = 'N'; 
				$flash = 'Y';
			} else if($type == 4 ) { 
				$unicode = 'Y'; 
				$flash = 'N';
			}
			//Record id
			if(isset($_GET['recordsid'])) {
				$rid = $_GET['recordsid']; 
				if($rid == '') { 
					$recordid = 1;
				} else { 
					$recordid = 1;
				}
			} else { 
				$recordid = 1;
			}
			//templateid
			if(isset($_GET['leadtemplateid'])){
				$leadtemplateid = $_GET['leadtemplateid']; 
				if($leadtemplateid == '') { 
					$leadtemplateid = 1;
				}
			} else { 
				$leadtemplateid = 1;
			}
			//Signature id
			if(isset($_GET['signatureid'])){
				$signatureid = $_GET['signatureid']; 
				if($signatureid == '') { 
					$signatureid = 1;
				}
			} else { 
				$signatureid = 1;
			}
			$date = $_GET['communicationdate'];
			$time = $_GET['communicationtime'];
			if($date == '' && $time == '') { 
				$schedule = 'No';
			} else {
				$schedule = 'Yes';
			}
			if($date!='') {
				$schtimestamp = strtotime($date);
				$schdate = date("Y-m-d", $schtimestamp);
				$tosplitmins = explode(':',$time);
				$roundofhr = $tosplitmins[0];
				if ($tosplitmins[1] <= 15) {
					$roundofmin = '15';
				}
				if ($tosplitmins[1] >= 16 && $tosplitmins[1] <=30) {
					$roundofmin = '30';
				}
				if ($tosplitmins[1] >= 31 && $tosplitmins[1] <=45) {
					$roundofmin = '45';
				}
				if ($tosplitmins[1] >= 46 && $tosplitmins[1] <=60) {
					$roundofmin = '00'; 
					$roundofhr = $tosplitmins[0] + 1;
				}
				if($roundofhr > '12') { 
					$roundofhr = $roundofhr - 12; $ttt = 'pm';
				} else { 
					$roundofhr = $roundofhr; 
					$ttt = 'am';
				}
				if($roundofhr < '9' ) { 
					$roundofhr = '0'.$roundofhr;
				} else {
					$roundofhr = $roundofhr;
				}
				$commdate = $schdate.' '.$roundofhr.':'.$roundofmin.''.$ttt;
			} else {
				$commdate="";
			}
			if($commdate != '') {
				$communicationmode = 'SCHEDULE SMS';
			} else {
				$communicationmode = 'SENT SMS';
			}
			/**********  KALERA  **************/	
			//sms sending code
			for($i=0;$i<$mobcount;$i++) {
				if(is_array($mobmessage)) {
					$mobmessage = $mobmessage[$i];
				} else {
					$mobmessage = $mobmessage;
				}
				$data='<?xml version="1.0" encoding="UTF-8"?>
				<api>
				<sms>
				<to>'.$mobnum[$i].'</to>
				<message>'.$mobmessage.'</message>
				<msgid>'.$sendername.'</msgid>
				<sender>'.$sendername.'</sender>
				</sms>
				<response>Y</response>
				<unicode>'.$unicode.'</unicode>
				<flash>'.$flash.'</flash>
				<time>'.$commdate.'</time>
				</api>';
				$url = $xmlurl."$apikey&method=sms.xml&xml=".urlencode($data)."&type=json";
				$ch=curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$output=curl_exec($ch);
				curl_close($ch);
									//fetch & update delivery status (Xml way)
									/* $datasets = $this->parse_response($output); Commented by kumaresan */
				//fetch & update delivery status (json way)
				if(!isset($output[0]['errcode'])) {
					sleep(15);
					$json_result = json_decode($output, true);
					foreach($json_result['data'] as $key => $value) {
						$statusurl = $newstatusurl."$apikey&method=sms.status&id=".$value['id']."";
						$chk=curl_init();
						curl_setopt($chk, CURLOPT_URL, $statusurl);
						curl_setopt($chk, CURLOPT_RETURNTRANSFER, true);
						$statusoutput=curl_exec($chk);
						curl_close($chk);
						$status_update = json_decode($statusoutput, true);
						//schedule date & time code
						if(empty($status_update['data'])) {
							$communicationto_update = $mobnum[$i];
							$groupid_update = $value['id'];
							$communicationstatus_update = $value['status'];
						} else {
							$communicationto_update = $status_update['data'][0]['mobile'];
							$groupid_update = $status_update['data'][0]['id'];
							$communicationstatus_update = $status_update['data'][0]['status'];
						}
						//print_r($someArray['data'][0]['mobile']); die();
						//$smsstatus = explode(' ',trim($statusoutput));
						//$salesstatus = $this->statusvalueget($smsstatus[2]);
						//comm log
						$smslog = array( 
							'commonid'=>$recordid,
							'moduleid'=>1,
							'employeeid'=>$this->Basefunctions->userid,
							'smstypeid'=>$type,
							'smssettingsid'=>$senderid,
							'smssendtypeid'=>$smssendtype,
							'templatesid'=>$leadtemplateid,
							'signatureid'=>$signatureid,
							'crmcommunicationmode'=>$communicationmode,
							'communicationfrom'=>0,
							'communicationto'=>$communicationto_update,
							'smscount'=>$smscount,
							'message'=>$mobmessage[$i],
							'groupid'=>$groupid_update,
							'communicationstatus'=>$communicationstatus_update,
							'communicationdate'=>date($this->Basefunctions->datef),
							'industryid'=>$this->Basefunctions->industryid,
							'branchid'=>$this->Basefunctions->branchid,
							'createdate'=>date($this->Basefunctions->datef),
							'lastupdatedate'=>date($this->Basefunctions->datef),
							'createuserid'=>$this->Basefunctions->userid,
							'lastupdateuserid'=>$this->Basefunctions->userid,
							'status'=>$this->Basefunctions->activestatus
						);
						$this->db->insert('crmcommunicationlog',$smslog);
						$primaryid = $this->Basefunctions->maximumid('crmcommunicationlog','crmcommunicationlogid');
						//notification log entry
						$empid = $this->Basefunctions->logemployeeid;
						$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
						$notimsg = $empname." "."Sent a SMS to ".$communicationto_update;
						$this->Basefunctions->notificationcontentadd($primaryid,'SMS',$notimsg,1,210);
						if($smssendtype != 4) {
							if($smssendtype == 2) {
								$crediturl = "https://api-alerts.kaleyra.com/v4/?api_key=$apikey&method=account.credits";
							} else if($smssendtype == 3) {
								$crediturl = "https://api-promo.kaleyra.com/v4/?api_key=$apikey&method=account.credits";
							}
							$crdurl=curl_init();
							curl_setopt($crdurl, CURLOPT_URL, $crediturl);
							curl_setopt($crdurl, CURLOPT_RETURNTRANSFER, true);
							$creditoutput = curl_exec($crdurl);
							curl_close($crdurl);
							$jsoncreditresult = json_decode($creditoutput, true);
							foreach($jsoncreditresult['data'] as $key => $value) {
								if($value == '') {
									$value = '100';
								}
								$credit = $this->db->query("UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = $value WHERE `addonstypeid` = $addontypeid");
							}
							/* *** Locally used but now it s gettting from online *** Kumaresan
							$credit = $this->db->query("UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = `addonavailablecredit` - $smscount WHERE `addonstypeid` = $addontypeid"); */
						}
						//master company id get - master db update
						$mastercompanyid = $this->Basefunctions->generalinformaion('company','mastercompanyid','companyid',2);
						//Master DB update
						$mdbname = $this->db->masterdb;		
					}
				} 
			}
			if(!isset($datasets[0]['errcode'])) {
				$smscampaign = array(
						'smscampaignname'=>$_GET['smscampaignname'],
						'campaigngroupsid'=>$_GET['smsgroupsid'],
						'segmentsid'=>$_GET['smssegmentsid'],
						'totalsubscribercount'=>$_GET['totalsubscribercount'],
						'validsubscribercount'=>$_GET['validsubscribercount'],
						'campaigncredits'=>$_GET['totalsmscreditofcampaign'],
						'industryid'=>$this->Basefunctions->industryid,
						'branchid'=>$this->Basefunctions->branchid,
						'createdate'=>date($this->Basefunctions->datef),
						'lastupdatedate'=>date($this->Basefunctions->datef),
						'createuserid'=>$this->Basefunctions->userid,
						'lastupdateuserid'=>$this->Basefunctions->userid,
						'status'=>$this->Basefunctions->activestatus
					);
				$this->db->insert('smscampaign',$smscampaign);
				echo json_encode('success');
			} else {
				echo json_encode($datasets[0]['desc']);
			}	
		} else {
			echo json_encode('Credits');
		}
	}
	//send sms model
	public function leadsmssendmodel_infinisolution() {
		$schdate="";
		$msgsenddate = "";
		$subscriberid = $_GET['selectedsegmentsubscriberid'];
		$totalcredits = $_GET['totalsmscreditofcampaign'];
		$message = $_GET['defaultsubseditorval'];
		$smscount = $_GET['smscount'];
		$datacontent = $_GET['datacontent'];
		$subid = explode(',',$subscriberid);
		if($datacontent != 'null') {
			$custmobile=$_GET['communicationto'];
			$mobnum = explode(',',$custmobile);
			$mobmessage = explode('|||',$message);
			$mobcount = count($mobnum);
		} else {
			$custmobile=$_GET['communicationto'];
			$mobnum = array($custmobile);
			$mobmessage = array($message);
			$mobcount = '1';
		}
		//Sender Name
		if(isset($_GET['senderid'])){
			$senderid = $_GET['senderid']; 
			if($senderid == '') { 
				$senderid = '1';
			}
		} else { 
			$senderid = '1';
		}
		if($senderid != 1) {
			$provid = $this->Basefunctions->generalinformaion('smssettings','smsprovidersettingsid','smssettingsid',$senderid);
			$apikey = $this->Basefunctions->generalinformaion('smsprovidersettings','apikey','smsprovidersettingsid',$provid);
		}
		//Sender Settings id
		if(isset($_GET['sendername'])) { 
			$sendername = $_GET['sendername'];
		} else{ 
			$sendername = '';
		};
		//sms send type 
		$smssendtype = $_GET['smssendtypeid'];
		if($smssendtype == '2') {
			$xmlurl = "http://alerts.sinfini.com/api/xmlapi.php?data=";
			$newstatusurl = "http://alerts.sinfini.com/api/status.php?workingkey=";
			$addontypeid = '2';
		} else if($smssendtype == '3') {
			$xmlurl = "http://promo.sinfini.com/api/xmlapi.php?data=";
			$newstatusurl = "http://promo.sinfini.com/api/status.php?workingkey=";
			$apikey = 'Ac9bb7caf562bf6bfa7f9aab4c83ffde9';
			$sendername = 'BULKSMS';
			$addontypeid = '4';
		} else if($smssendtype == '4') {
			$xmlurl = "http://global.sinfini.com/api/xmlapi.php?data=";
			$newstatusurl = "http://global.sinfini.com/api/status.php?workingkey=";
			$apikey = 'A1363f16118616942397968b9a9f736da';
			$sendername = 'AUCGLO';
			$addontypeid = '5';
		}
		$credits = $this->accountcresitdetailsfetchmodel($apikey,$addontypeid);
		if($credits > $totalcredits) {
			$credits = 1;
			{ //sms campaign insertion
				$campname =  $_GET['smscampaignname'];
				if(isset($_GET['smsgroupsid'])){
					$grpid = $_GET['smsgroupsid']; 
					if($grpid == '') { 
						$grpid = '1';
					}
				} else { 
					$grpid = '1';
				}
				if(isset($_GET['smssegmentsid'])){
					$segmentid = $_GET['smssegmentsid']; 
					if($segmentid == ''){ 
						$segmentid = '1';
					}
				} else { 
					$segmentid = '1';
				}
				$totalsub =  $_GET['totalsubscribercount'];
				$validsub =  $_GET['validsubscribercount'];
			}
			//sms type id
			if(isset($_GET['smstypeid'])) {
				$type = $_GET['smstypeid']; 
			} else { 
				$type = 2; 
			}
			if($type == 2 ) { 
				$unicode = 'N'; 
				$flash = 'N';
			} else if($type == 3 ) { 
				$unicode = 'N'; 
				$flash = 'Y';
			} else if($type == 4 ) { 
				$unicode = 'Y'; 
				$flash = 'N';
			}
			//templateid
			if(isset($_GET['leadtemplateid'])){
				$leadtemplateid = $_GET['leadtemplateid'];  
				if($leadtemplateid == '') { 
					$leadtemplateid = 1;
				}
			} else { 
				$leadtemplateid = 1;
			}
			//Signature id
			if(isset($_GET['signatureid'])) {
				$signatureid = $_GET['signatureid']; 
				if($signatureid == '') { 
					$signatureid = 1;
				}
			} else { 
				$signatureid = 1;
			}
			$date = $_GET['communicationdate'];
			$time = $_GET['communicationtime'];
			if($time == ''){ $time = '00:00:00';}
			if($date!='') {
				$schtimestamp = strtotime($date);
				$schdate = date("Y-m-d", $schtimestamp);
				$tosplitmins = explode(':',$time);
				$roundofhr = $tosplitmins[0];
				if($tosplitmins[1] <= 15) {
					$roundofmin = '15';
				}
				if($tosplitmins[1] >= 16 && $tosplitmins[1] <=30) {
					$roundofmin = '30';
				}
				if($tosplitmins[1] >= 31 && $tosplitmins[1] <=45) {
					$roundofmin = '45';
				}
				if($tosplitmins[1] >= 46 && $tosplitmins[1] <=60) { 
					$roundofmin = '00'; $roundofhr = $tosplitmins[0] + 1;
				}
				if($roundofhr > '12') { 
					$roundofhr = $roundofhr - 12; 
					$ttt = 'PM';
				} else { 
					$roundofhr = $roundofhr; 
					$ttt = 'AM';
				}
				if($roundofhr < '9' ) { 
					$roundofhr = '0'.$roundofhr;
				} else {
					$roundofhr = $roundofhr;
				}
				$commdate = $schdate.' '.$roundofhr.':'.$roundofmin.' '.$ttt;
			} else {
				$commdate="";
			}
			if($commdate != '') {
				$communicationmode = 'SCHEDULE SMS';
			} else {
				$communicationmode = 'SENT SMS';
			}
			
			for($i=0;$i<$mobcount;$i++) {
				$data='<?xml version="1.0" encoding="UTF-8"?>
				<xmlapi>
				<auth>
				<apikey>'.$apikey.'</apikey>
				</auth>
				<sendSMS>
				<to>'.$mobnum[$i].'</to>
				<text>'.$mobmessage[$i].'</text>
				<msgid>'.$sendername.'</msgid>
				<sender>'.$sendername.'</sender>
				</sendSMS>
				<response>Y</response>
				<unicode>'.$unicode.'</unicode>
				<flash>'.$flash.'</flash>
				<time>'.$commdate.'</time>
				</xmlapi>';
				$url = $xmlurl.urlencode($data)."&type=json";
				$ch=curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$output=curl_exec($ch);
				curl_close($ch);
				//fetch & update delivery status
				$datasets = $this->parse_response($output);
				if(!isset($datasets[0]['errcode'])) {
					sleep(15);
					foreach($datasets as $key => $value) {
						$statusurl = $newstatusurl.$apikey."&messageid=".$value['msgid']."";
						$chk=curl_init();
						curl_setopt($chk, CURLOPT_URL, $statusurl);
						curl_setopt($chk, CURLOPT_RETURNTRANSFER, true);
						$statusoutput=curl_exec($chk);
						curl_close($chk);
						$smsstatus = explode(' ',trim($statusoutput));
						$salesstatus = $this->statusvalueget($smsstatus[2]);
						//comm log
						$smslog=array( 
							'commonid'=>$subid[$i],
							'moduleid'=>'26',
							'employeeid'=>$this->Basefunctions->userid,
							'smstypeid'=>$type,
							'smssettingsid'=>$senderid,
							'templatesid'=>$leadtemplateid,
							'signatureid'=>$signatureid,
							'crmcommunicationmode'=>$communicationmode,
							'communicationfrom'=>0,
							'communicationto'=>$smsstatus[1],
							'smscount'=>$smscount,
							'groupid'=>$smsstatus[0],
							'communicationstatus'=>$salesstatus,
							'communicationdate'=>date($this->Basefunctions->datef),
							'createdate'=>date($this->Basefunctions->datef),
							'lastupdatedate'=>date($this->Basefunctions->datef),
							'createuserid'=>$this->Basefunctions->userid,
							'lastupdateuserid'=>$this->Basefunctions->userid,
							'status'=>$this->Basefunctions->activestatus
						);
						$this->db->insert('crmcommunicationlog',$smslog);
						$primaryid = $this->Basefunctions->maximumid('crmcommunicationlog','crmcommunicationlogid');
						//notification log entry
						$empid = $this->Basefunctions->logemployeeid;
						$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
						$notimsg = $empname." "."Sent a SMS to"." ".$smsstatus[1];
						$this->Basefunctions->notificationcontentadd($primaryid,'SMS',$notimsg,1,210);	
						if($smssendtype != 4) {						
							$credit = $this->db->query("UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = `addonavailablecredit` - $smscount WHERE `addonstypeid` = $addontypeid");
						}
						//master company id get - master db update
						$mastercompanyid = $this->Basefunctions->generalinformaion('company','mastercompanyid','companyid',2);
						//Master DB update
					}
				} 
			}
			if(!isset($datasets[0]['errcode'])) {
				$smscampaign = array(
						'smscampaignname'=>$campname,
						'campaigngroupsid'=>$grpid,
						'segmentsid'=>$segmentid,
						'totalsubscribercount'=>$totalsub,
						'validsubscribercount'=>$validsub,
						'campaigncredits'=>$credits,
						'industryid'=>$this->Basefunctions->industryid,
						'branchid'=>$this->Basefunctions->branchid,
						'createdate'=>date($this->Basefunctions->datef),
						'lastupdatedate'=>date($this->Basefunctions->datef),
						'createuserid'=>$this->Basefunctions->userid,
						'lastupdateuserid'=>$this->Basefunctions->userid,
						'status'=>$this->Basefunctions->activestatus
					);
				$this->db->insert('smscampaign',$smscampaign); 
			 	echo json_encode('success');
			} else {
				echo json_encode($datasets[0]['desc']);
			}
		} else {
			echo json_encode('Credits');
		} 
	}
	public function leadsmssendmodel_api() {
		$schdate="";
		$msgsenddate = "";
		$subscriberid = $_GET['selectedsegmentsubscriberid'];
		$totalcredits = $_GET['totalsmscreditofcampaign'];
		$message = $_GET['defaultsubseditorval'];
		$smscount = $_GET['smscount'];
		$datacontent = $_GET['datacontent'];
		$subid = explode(',',$subscriberid);
		if($datacontent != 'null') {
			$custmobile=$_GET['communicationto'];
			$mobnum = explode(',',$custmobile);
			$mobmessage = explode('|||',$message);
			$mobcount = count($mobnum);
		} else {
			$custmobile=$_GET['communicationto'];
			$mobnum = array($custmobile);
			$mobmessage = array($message);
			$mobcount = '1';
		}
		//Sender Name
		if(isset($_GET['senderid'])){
			$senderid = $_GET['senderid']; 
			if($senderid == '') { 
				$senderid = '1';
			}
		} else { 
			$senderid = '1';
		}
		if($senderid != 1) {
			$provid = $this->Basefunctions->generalinformaion('smssettings','smsprovidersettingsid','smssettingsid',$senderid);
			$apikey = $this->Basefunctions->generalinformaion('smsprovidersettings','apikey','smsprovidersettingsid',$provid);
		}
		//Sender Settings id
		if(isset($_GET['sendername'])) { 
			$sendername = $_GET['sendername'];
		} else{ 
			$sendername = '';
		};
		//sms send type 
		$smssendtype = $_GET['smssendtypeid'];
		if($smssendtype == '2') {
			$xmlurl = "http://alerts.sinfini.com/api/xmlapi.php?data=";
			$newstatusurl = "http://alerts.sinfini.com/api/status.php?workingkey=";
			$addontypeid = '2';
		} else if($smssendtype == '3') {
			$xmlurl = "http://promo.sinfini.com/api/xmlapi.php?data=";
			$newstatusurl = "http://promo.sinfini.com/api/status.php?workingkey=";
			$apikey = 'Ac9bb7caf562bf6bfa7f9aab4c83ffde9';
			$sendername = 'BULKSMS';
			$addontypeid = '4';
		} else if($smssendtype == '4') {
			$xmlurl = "http://global.sinfini.com/api/xmlapi.php?data=";
			$newstatusurl = "http://global.sinfini.com/api/status.php?workingkey=";
			$apikey = 'A1363f16118616942397968b9a9f736da';
			$sendername = 'AUCGLO';
			$addontypeid = '5';
		}
		$credits = $this->accountcresitdetailsfetchmodel($apikey,$addontypeid);
		if($credits > $totalcredits) {
			$credits = 1;
			{ //sms campaign insertion
				$campname =  $_GET['smscampaignname'];
				if(isset($_GET['smsgroupsid'])){
					$grpid = $_GET['smsgroupsid']; 
					if($grpid == '') { 
						$grpid = '1';
					}
				} else { 
					$grpid = '1';
				}
				if(isset($_GET['smssegmentsid'])){
					$segmentid = $_GET['smssegmentsid']; 
					if($segmentid == ''){ 
						$segmentid = '1';
					}
				} else { 
					$segmentid = '1';
				}
				$totalsub =  $_GET['totalsubscribercount'];
				$validsub =  $_GET['validsubscribercount'];
			}
			//sms type id
			if(isset($_GET['smstypeid'])) {
				$type = $_GET['smstypeid']; 
			} else { 
				$type = 2; 
			}
			if($type == 2 ) { 
				$unicode = 'N'; 
				$flash = 'N';
			} else if($type == 3 ) { 
				$unicode = 'N'; 
				$flash = 'Y';
			} else if($type == 4 ) { 
				$unicode = 'Y'; 
				$flash = 'N';
			}
			//templateid
			if(isset($_GET['leadtemplateid'])){
				$leadtemplateid = $_GET['leadtemplateid'];  
				if($leadtemplateid == '') { 
					$leadtemplateid = 1;
				}
			} else { 
				$leadtemplateid = 1;
			}
			//Signature id
			if(isset($_GET['signatureid'])) {
				$signatureid = $_GET['signatureid']; 
				if($signatureid == '') { 
					$signatureid = 1;
				}
			} else { 
				$signatureid = 1;
			}
			$date = $_GET['communicationdate'];
			$time = $_GET['communicationtime'];
			if($time == ''){ $time = '00:00:00';}
			if($date!='') {
				$schtimestamp = strtotime($date);
				$schdate = date("Y-m-d", $schtimestamp);
				$tosplitmins = explode(':',$time);
				$roundofhr = $tosplitmins[0];
				if($tosplitmins[1] <= 15) {
					$roundofmin = '15';
				}
				if($tosplitmins[1] >= 16 && $tosplitmins[1] <=30) {
					$roundofmin = '30';
				}
				if($tosplitmins[1] >= 31 && $tosplitmins[1] <=45) {
					$roundofmin = '45';
				}
				if($tosplitmins[1] >= 46 && $tosplitmins[1] <=60) { 
					$roundofmin = '00'; $roundofhr = $tosplitmins[0] + 1;
				}
				if($roundofhr > '12') { 
					$roundofhr = $roundofhr - 12; 
					$ttt = 'PM';
				} else { 
					$roundofhr = $roundofhr; 
					$ttt = 'AM';
				}
				if($roundofhr < '9' ) { 
					$roundofhr = '0'.$roundofhr;
				} else {
					$roundofhr = $roundofhr;
				}
				$commdate = $schdate.' '.$roundofhr.':'.$roundofmin.' '.$ttt;
			} else {
				$commdate="";
			}
			if($commdate != '') {
				$communicationmode = 'SCHEDULE SMS';
			} else {
				$communicationmode = 'SENT SMS';
			}
			
			for($i=0;$i<$mobcount;$i++) {
				$data='<?xml version="1.0" encoding="UTF-8"?>
				<xmlapi>
				<auth>
				<apikey>'.$apikey.'</apikey>
				</auth>
				<sendSMS>
				<to>'.$mobnum[$i].'</to>
				<text>'.$mobmessage[$i].'</text>
				<msgid>'.$sendername.'</msgid>
				<sender>'.$sendername.'</sender>
				</sendSMS>
				<response>Y</response>
				<unicode>'.$unicode.'</unicode>
				<flash>'.$flash.'</flash>
				<time>'.$commdate.'</time>
				</xmlapi>';
				$url = $xmlurl.urlencode($data)."&type=json";
				$ch=curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$output=curl_exec($ch);
				curl_close($ch);
				//fetch & update delivery status
				$datasets = $this->parse_response($output);
				if(!isset($datasets[0]['errcode'])) {
					sleep(15);
					foreach($datasets as $key => $value) {
						$statusurl = $newstatusurl.$apikey."&messageid=".$value['msgid']."";
						$chk=curl_init();
						curl_setopt($chk, CURLOPT_URL, $statusurl);
						curl_setopt($chk, CURLOPT_RETURNTRANSFER, true);
						$statusoutput=curl_exec($chk);
						curl_close($chk);
						$smsstatus = explode(' ',trim($statusoutput));
						$salesstatus = $this->statusvalueget($smsstatus[2]);
						//comm log
						$smslog=array( 
							'commonid'=>$subid[$i],
							'moduleid'=>'26',
							'employeeid'=>$this->Basefunctions->userid,
							'smstypeid'=>$type,
							'smssettingsid'=>$senderid,
							'templatesid'=>$leadtemplateid,
							'signatureid'=>$signatureid,
							'crmcommunicationmode'=>$communicationmode,
							'communicationfrom'=>0,
							'communicationto'=>$smsstatus[1],
							'smscount'=>$smscount,
							'groupid'=>$smsstatus[0],
							'communicationstatus'=>$salesstatus,
							'communicationdate'=>date($this->Basefunctions->datef),
							'createdate'=>date($this->Basefunctions->datef),
							'lastupdatedate'=>date($this->Basefunctions->datef),
							'createuserid'=>$this->Basefunctions->userid,
							'lastupdateuserid'=>$this->Basefunctions->userid,
							'status'=>$this->Basefunctions->activestatus
						);
						$this->db->insert('crmcommunicationlog',$smslog);
						$primaryid = $this->Basefunctions->maximumid('crmcommunicationlog','crmcommunicationlogid');
						//notification log entry
						$empid = $this->Basefunctions->logemployeeid;
						$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
						$notimsg = $empname." "."Sent a SMS to"." ".$smsstatus[1];
						$this->Basefunctions->notificationcontentadd($primaryid,'SMS',$notimsg,1,210);	
						if($smssendtype != 4) {						
							$credit = $this->db->query("UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = `addonavailablecredit` - $smscount WHERE `addonstypeid` = $addontypeid");
						}
						//master company id get - master db update
						$mastercompanyid = $this->Basefunctions->generalinformaion('company','mastercompanyid','companyid',2);
						//Master DB update
					}
				} 
			}
			if(!isset($datasets[0]['errcode'])) {
				$smscampaign = array(
						'smscampaignname'=>$campname,
						'campaigngroupsid'=>$grpid,
						'segmentsid'=>$segmentid,
						'totalsubscribercount'=>$totalsub,
						'validsubscribercount'=>$validsub,
						'campaigncredits'=>$credits,
						'industryid'=>$this->Basefunctions->industryid,
						'branchid'=>$this->Basefunctions->branchid,
						'createdate'=>date($this->Basefunctions->datef),
						'lastupdatedate'=>date($this->Basefunctions->datef),
						'createuserid'=>$this->Basefunctions->userid,
						'lastupdateuserid'=>$this->Basefunctions->userid,
						'status'=>$this->Basefunctions->activestatus
					);
				$this->db->insert('smscampaign',$smscampaign); 
			 	return 'success';
			} else {
				return $datasets[0]['desc'];
			}
		} else {
			return 'Credits';
		} 
	}
	//fetch xml content
	public function parse_response($response) {
		$xml = new SimpleXMLElement($response);
		$result = array();
		$h=0;
		foreach($xml as $key => $value) {
			if( $key == 'to' || $key == 'msgid' || $key == 'status' ) {
				$result[$h][(string)$key] = (string)$value;
				if( count($result[$h]) == '3' ) {
					$h++;
				}
			} else if($key == 'errcode' || $key == 'desc'){
				$result[$h][(string)$key] = (string)$value;
				if( count($result[$h]) == '2' ) {
					$h++;
				}
			}
		}
		return $result;
	}
	//inline user data base set
	public function inlineuserdatabaseactive($databasename) {
		$host = $this->db->hostname;
		$user = $this->db->username;
		$passwd = $this->db->password;
		$dbconfig['hostname'] = $host;
		$dbconfig['username'] = $user;
		$dbconfig['password'] = $passwd;
		$dbconfig['database'] = $databasename;
		$dbconfig['dbdriver'] = 'mysql';
		$dbconfig['dbprefix'] = '';
		$dbconfig['pconnect'] = TRUE;
		$dbconfig['db_debug'] = TRUE;
		$dbconfig['cache_on'] = FALSE;
		$dbconfig['cachedir'] = '';
		$dbconfig['char_set'] = 'utf8';
		$dbconfig['dbcollat'] = 'utf8_general_ci';
		$dbconfig['swap_pre'] = '';
		$dbconfig['autoinit'] = TRUE;
		$dbconfig['stricton'] = FALSE;
		return $import_db = $this->load->database($dbconfig, TRUE);
	}
	//sms schedule time check
	public function scheduletimecalculatemodel() {
		$scheduledate = $_GET['date'];
		$scheduletime = $_GET['time'];
		$currentdate = date('d-m-Y', time());
		$currenttime = date('H:i:s', time());
		$sdate = $scheduledate.' '.$scheduletime;
		date_default_timezone_set("Asia/Kolkata");
		$currentdate = date('d-m-Y H:i:s', time());
		if(strtotime($sdate) > strtotime($currentdate)) {
			$data = 'True';
			echo json_encode($data);
		} else {
			$currenttime = date('H:i:s', time());
			if(strtotime($scheduletime) >= strtotime($currenttime)) {
				$diff = $scheduletime - $currenttime;
				if($diff = '1') {
					$data = 'True';
				} else if($diff = '0') {
					 $data = 'True';
				} else {
					$data = 'True';
				}
			} else {
				$data = 'False';
			}
			echo json_encode($data);
		}	
	}
	//sms group type get
	public function grouptypeidgetmodel() {
		$groupid = $_POST['groupid'];
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('smsgrouptypeid,moduleid');
		$this->db->from('campaigngroups');
		$this->db->where('campaigngroups.campaigngroupsid',$groupid);
		$this->db->where("FIND_IN_SET('$industryid',campaigngroups.industryid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data = array('typeid'=>$row->smsgrouptypeid,'moduleid'=>$row->moduleid);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//sms subscriber id get
	public function mobilenumbasedsubscriberidgetmodel() {
		$mobnum = $_POST['mobnum'];
		$grouptype = $_POST['grouptype'];
		$expmobnum = explode(',',$mobnum);
		$industryid = $this->Basefunctions->industryid;
		$data = array();
		$j=0;
		if($grouptype =='2') {
			for($i=0;$i<count($expmobnum);$i++) {
				$this->db->select('subscribersid');
				$this->db->from('subscribers');
				$this->db->like('subscribers.mobilenumber',trim($expmobnum[$i])); 
				$this->db->where('subscribers.templatetypeid','2');
				$this->db->where('subscribers.status','1');
				$this->db->where("FIND_IN_SET('$industryid',subscribers.industryid) >", 0);
				$this->db->limit(1,0);
				$result = $this->db->get();
				if($result->num_rows() > 0){
					foreach($result->result() as $row) {
						$data[$j] = array('recordid'=>$row->subscribersid,'moduleid'=>'271');
						$j++;
					}
				}
			}
			if($data != '') {
				echo json_encode($data);
			} else {
				echo json_encode(array("fail"=>'FAILED'));
			}
		} else if($grouptype =='3') {
			$j=0;
			$data = '';
			for($i=0;$i<count($expmobnum);$i++) {
				$moduleid = $this->smsgeneralinformaionfetch('subscribers','moduleid','mobilenumber',trim($expmobnum[$i]));
				$recordid = $this->smsgeneralinformaionfetch('subscribers','recordid','mobilenumber',trim($expmobnum[$i]));
				$data[$j] = array('moduleid'=>$moduleid,'recordid'=>$recordid);
				$j++;
			}
			if($data != '') {
				echo json_encode($data);
			} else {
				echo json_encode(array("fail"=>'FAILED'));
			}
		}
	}
	public function mobilenumbasedsubscriberidgetmodel_api() {
		$mobnum = $_POST['mobnum'];
		$grouptype = $_POST['grouptype'];
		$expmobnum = explode(',',$mobnum);
		$industryid = $this->Basefunctions->industryid;
		$data = array();
		$j=0;
		if($grouptype =='2') {
			for($i=0;$i<count($expmobnum);$i++) {
				$this->db->select('subscribersid');
				$this->db->from('subscribers');
				$this->db->like('subscribers.mobilenumber',trim($expmobnum[$i])); 
				$this->db->where('subscribers.templatetypeid','2');
				$this->db->where('subscribers.status','1');
				$this->db->where("FIND_IN_SET('$industryid',subscribers.industryid) >", 0);
				$this->db->limit(1,0);
				$result = $this->db->get();
				if($result->num_rows() > 0){
					foreach($result->result() as $row) {
						$data[$j] = array('recordid'=>$row->subscribersid,'moduleid'=>'271');
						$j++;
					}
				}
			}
			if($data != '') {
				return $data;
			} else {
				return array();
			}
		} else if($grouptype =='3') {
			$j=0;
			$data = '';
			for($i=0;$i<count($expmobnum);$i++) {
				$moduleid = $this->smsgeneralinformaionfetch('subscribers','moduleid','mobilenumber',trim($expmobnum[$i]));
				$recordid = $this->smsgeneralinformaionfetch('subscribers','recordid','mobilenumber',trim($expmobnum[$i]));
				$data[$j] = array('moduleid'=>$moduleid,'recordid'=>$recordid);
				$j++;
			}
			if($data != '') {
				return $data;
			} else {
				return array();
			}
		}
	}
	//general data fetch
	public function smsgeneralinformaionfetch($tabname,$field,$cond,$data) {
		$fdata = '';
		$this->db->select($field);
		$this->db->from($tabname);
		$this->db->like($cond,trim($data), 'both');
		$this->db->where('status',1);
		$this->db->limit(1,0);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$fdata = $row->$field;
			}
		}
		return $fdata;
	}	
	//sms template dd value fetch
	public function smstemplateddvalfetchtmodel() {
		$fromappmod = $_GET['moduleid'];
		$grouptype = $_GET['grouptype'];
		$senderid = $_GET['senderid'];
		$smsendtype = $_GET['smsendtype'];
		$fmodule = explode(',',$fromappmod);
		$industryid = $this->Basefunctions->industryid;
		if(count($fmodule) == '1') {
			$moduleid = $fmodule[0];
			$grouptype = $grouptype;
		} else {
			$moduleid = '271';
			$grouptype = '2';
		}
		if(!isset($moduleid)){
			$moduleid = '26';
		}
		$i = 0;
		$this->db->select('templatesid,templatesname');
		$this->db->from('templates');
		$this->db->where_in('templates.smssettingsid',array($senderid));
		$this->db->where_in('templates.smssendtypeid',array($smsendtype));
		$this->db->where("FIND_IN_SET('$industryid',templates.industryid) >", 0);
		$this->db->where('templates.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->templatesid,'dataname'=>$row->templatesname);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//sales neuron status value get
	public function statusvalueget($smsstatus) {
		$this->db->select('smssalesneuronname');
		$this->db->from('smsstatus');
		$this->db->where('smsstatus.smsstatusid',$smsstatus);
		$result = $this->db->get();
		if($result->num_rows() > 0 ) {
			foreach($result->result() as $row) {
				$data = $row->smssalesneuronname;
			}
			return $data;
		} else {
			return $smsstatus;
		}
	}
	//folder value fetch
	public function segmentddloadmodel() {
		$i=0;
		$listid = $_GET['listid'];
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('segmentsid,segmentsname');
		$this->db->from('segments');
		$this->db->where('segments.campaigngroupsid',$listid);
		$this->db->where('segments.status','1');
		$this->db->where("FIND_IN_SET('$industryid',segments.industryid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->segmentsid,'dataname'=>$row->segmentsname);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//segment criteria fetch
	public function segmentcreteriadatafetch($segmentid) {
		$i = 0;
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('viewcreationcolumns.viewcreationcolmodelindexname,viewcreationcolumns.viewcreationcolmodeljointable,segmentscriteria.viewcreationconditionname,segmentscriteria.viewcreationconditionvalue,segmentscriteria.viewcreationandorvalue');
		$this->db->from('segmentscriteria');
		$this->db->join('viewcreationcolumns','viewcreationcolumns.viewcreationcolumnid=segmentscriteria.viewcreationcolumnid');
		$this->db->where('segmentscriteria.segmentsid',$segmentid);
		$this->db->where('segmentscriteria.status',1);
		$this->db->where("FIND_IN_SET('$industryid',segmentscriteria.industryid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('indexname'=>$row->viewcreationcolmodelindexname,'jointable'=>$row->viewcreationcolmodeljointable,'critreiavalue'=>$row->viewcreationconditionvalue,'conditionidname'=>$row->viewcreationconditionname,'massandorcondidname'=>$row->viewcreationandorvalue);
				$i++;
			}
		} else {
			$data = array();
		}
		return $data;
	}
	//sender id drop down value fetch function
	public function smssenderddvalfetchmodel() {
		$typeid = $_GET['typeid'];
		$industryid = $this->Basefunctions->industryid;
		$i=0;
		$userid = $this->Basefunctions->userid;
		$this->db->select('smssettings.smssettingsid,smssettings.senderid,smsprovidersettings.apikey as settingapikey,smsprovidersettings.smssendtypeid');
		$this->db->from('smsprovidersettings');
		$this->db->join('smssettings','smssettings.smsprovidersettingsid=smsprovidersettings.smsprovidersettingsid');
		$this->db->where_in('smsprovidersettings.smssendtypeid',$typeid);
		$this->db->where('smssettings.status',1);
		$this->db->where("FIND_IN_SET('$industryid',smsprovidersettings.industryid) >", 0);
		$this->db->where("FIND_IN_SET('$industryid',smssettings.industryid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data[$i] = array('datasid'=>$row->smssettingsid,'dataname'=>$row->senderid,'apikey'=>$row->settingapikey);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
}      