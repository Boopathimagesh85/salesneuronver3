<div id="storagemanagementview" class="gridviewdivforsh">
	<?php
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['gridid'] = 'storagemanagementgrid';
		$dataset['gridwidth'] = 'storagemanagementgridwidth';
		$dataset['gridfooter'] = 'storagemanagementgridfooter';
		$dataset['viewtype'] = 'disable';
		$dataset['moduleid'] = '59';
		$this->load->view('Base/singlemainviewformwithgrid.php',$dataset);
	?>
</div>
</div>
</div>
</div>
<div id="storagemanagementformadd" class="hidedisplay" style="">
	<input type ="hidden" id="hiddenemployeeid" value="<?php echo $employeeid?>"/>
	<input type ="hidden" id="weight_round" value="<?php echo $weight_round?>"/>
	<input type ="hidden" id="printyesno" value="0"/>
	<div class="large-12 columns paddingzero formheader">
		<?php
		$device = $this->Basefunctions->deviceinfo();
			if($device=='phone') {
				echo '<div class="large-12 columns headercaptionstyle headerradius addformreloadtablet paddingzero">
					<div class="large-6 medium-6 small-6 columns headercaptionleft">
						<span class="gridcaptionpos"><span>'.$gridtitle['title'].'</span></span>
					</div>
					<div class="large-5 medium-6 small-6 columns addformheadericonstyle addformaction righttext">
						<span class="icon-box" id="smsfrmappcloseform" title="Close"><i class="material-icons">close</i> </span>
					</div>';
			echo '</div>';
		} else {
			echo '<div class="large-12 columns headercaptionstyle headerradius addformreloadtablet paddingzero">
					<div class="large-6 medium-6 small-6 columns headercaptionleft">
						<span class="gridcaptionpos"><span>'.$gridtitle['title'].'</span></span>
					</div>
					<div class="large-6 medium-6 small-6 columns addformheadericonstyle addformaction righttext">
						<span id="addstoragemanagement" class="addbtnclass icon-box"><input id="" name="" tabindex="186" value="Save" class="alertbtnyes  ffield" type="button"></span>
						<span id="addsectionclose" class="icon-box"><input id="" name="" tabindex="186" value="Close" class="alertbtnno  ffield" type="button"></span>
					</div>';
			echo '</div>';
		}
		?>
		<div class='large-12 columns addformunderheader'>&nbsp;</div>
		<!-- tab group creation -->
		<?php  if($device!='phone') {?>
			<div class="large-12 columns tabgroupstyle desktoptabgroup">
				<ul class="tabs" data-tab="">
					<li id="tab1" class="tab-title sidebaricons active ftabnew" data-subform="1">
						<span class="waves-effect waves-ripple waves-light">Storage Management</span>
					</li>
				</ul>
			</div>
		<?php } else {?>
			<div class="large-12 columns tabgroupstyle mobiletabgroup">
				<ul class="tabs" data-tab="">
					<li id="tab1"  class="tab-title sidebaricons active ftabnew" data-subform="1">
						<span  class="waves-effect waves-ripple waves-light">Storage Management</span>
					</li>
				</ul>
			</div>
		<?php }?>
	</div>
	<div class="large-12 columns addformunderheader">&nbsp; </div>
		<div class="large-12 columns addformcontainer padding-space-open-for-form" style="">
			<div class="row mblhidedisplay">&nbsp;</div>
			<form method="POST" name="storagemanagmentform" class="storagemanagmentform"  id="storagemanagmentform">
			<div id="addstoragemanagmentvalidate" class="validationEngineContainer" >
			<div id="subformspan1" class="hiddensubform"> 
					<div class="large-4 columns paddingbtm" style="padding-left:0px !important;padding-right:1.8rem !important;">	
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
							<div class="large-12 columns headerformcaptionstyle" >Basic Details</div>
							<div class="static-field large-6 columns">
									<label>Date</label>					
									<input id="storagemanagementdate" class="fordatepicicon validate[required]" type="text" data-dateformater="dd-mm-yy" data-prompt-position="topLeft" readonly="readonly" tabindex="" name="storagemanagementdate">
							</div>
							<div class="static-field large-6 columns hidedisplay">
									<label>Sales Person</label>						
									<select id="salespersonid" name="salespersonid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="1004" disabled="disabled">
									<option value=""></option>
									<?php $prev = ' ';
										foreach($employeedata as $key):
									?>
										<option data-name="<?php echo $key->employeename;?>" value="<?php echo $key->employeeid;?>"><?php echo $key->employeename;?></option>
										<?php endforeach;?>						
									</select>
							</div>
							<div class="static-field large-6 columns hidedisplay">
								<label>Session</label>
								<select id="session" name="session" class="chzn-select" data-prompt-position="topLeft:14,36" tabindex="102">
								<option value=""></option>
								<option value="No">No</option>	
								<option value="Yes">Yes</option> 
								</select>
							</div>
						    <div class="input-field large-6 columns sessioniddiv">
								<input type="text" class="" id="sessionid" name="sessionid" value="" tabindex="" readonly>
								<label for="sessionid">Session Id</label>
							</div>
							<div class="static-field large-6 columns hidedisplay">
								<label>Mode</label>
								<select id="mode" name="mode" class="chzn-select" data-prompt-position="topLeft:14,36" tabindex="102">
								<option value=""></option>
								<option value="One">One</option>	
								<option value="Many">Many</option> 
								</select>
							</div>
							<div class="input-field large-6 columns ">
								<input type="text" class="" id="counterid" name="counterid"  value="" tabindex="">
								<label for="counterid">Counter ID</label>
							</div>
							<div class="static-field large-6 columns">
								<label>Storage<span class="mandatoryfildclass">*</span></label>				
								<select id="counter" name="counter" class="chzn-select " data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="114">
								<option value=""></option>
								<?php  foreach($counter->result() as $key):?>
								<option value="<?php echo $key->counterid;?>" data-lastprintdatetime="<?php echo $key->lastprintdatetime;?>" data-boxweight="<?php echo $key->boxweight;?>" data-description="<?php echo $key->comment;?>"> <?php echo $key->counterid.' - '.$key->countername;?></option>
								<?php endforeach;?>
								</select>
							</div>
							<div class="input-field large-6 columns">
								<input type="text" class="" id="manualclosingweight" name="manualclosingweight" data-validation-engine="validate[required,custom[number],min[0.1],decval[<?php echo $weight_round;?>]]" value="" tabindex="" maxlength="15">
								<label for="manualclosingweight">Manual Closing Weight</label>
							</div>
							<div class="input-field large-6 columns">
								<input type="text" class="" id="manualclosingpieces" name="manualclosingpieces" data-validation-engine="validate[custom[integer]]" value="" tabindex="" maxlength="10">
								<label for="manualclosingpieces">Manual Closing Pieces</label>
							</div>
							<div class="input-field large-6 columns">
								<input type="text" class="" id="lastprintdatetime" name="lastprintdatetime" value="" tabindex="" disabled="disabled">
								<label for="lastprintdatetime">Last Print Date Time</label>
							</div>
							<div class="input-field large-6 columns ">
								<input type="text" class="" id="counterweight" name="counterweight" disabled value="" tabindex="">
								<label for="counterweight">Counter Weight</label>
							</div>
							<div class="input-field large-12 columns hidedisplay">
								<textarea name="description" class="materialize-textarea" id="description" rows="3" cols="40" tabindex="125" data-validation-engine="validate[maxSize[200]]"></textarea>
								<label for="description">Counter Weight Description</label>
							</div>
							<div class="large-12 columns">&nbsp;</div>
						</div>
					</div>
					<div class="large-4 columns paddingbtm" style="padding-left:0px !important;padding-right:1.8rem !important;">	
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
							<div class="large-12 columns headerformcaptionstyle" >Weight Summary Details</div>
							<div class="input-field large-6 columns">
								<input type="text" class="" id="openingweight" name="openingweight" value="" tabindex="" readonly>
								<label for="openingweight">Opening Weight</label>
							</div>
							<div class="input-field large-6 columns">
								<input type="text" class="" id="openingpieces" name="openingpieces" value="" tabindex="" readonly>
								<label for="openingpieces">Opening Pieces</label>
							</div>
							<div class="input-field large-6 columns">
								<input type="text" class="" id="inweight" name="inweight" value="" tabindex="" readonly>
								<label for="inweight">In Weight</label>
							</div>
							<div class="input-field large-6 columns">
								<input type="text" class="" id="inpieces" name="inpieces" value="" tabindex="" readonly>
								<label for="inpieces">In Pieces</label>
							</div>
							<div class="input-field large-6 columns">
								<input type="text" class="" id="outweight" name="outweight" value="" tabindex="" readonly>
								<label for="outweight">Out Weight</label>
							</div>
							<div class="input-field large-6 columns">
								<input type="text" class="" id="outpieces" name="outpieces" value="" tabindex="" readonly>
								<label for="outpieces">Out Pieces</label>
							</div>
							<div class="input-field large-6 columns">
								<input type="text" class="" id="closingweight" name="closingweight" value="" tabindex="" readonly>
								<label for="closingweight">Closing Weight</label>
							</div>
							<div class="input-field large-6 columns">
								<input type="text" class="" id="closingpieces" name="closingpieces" value="" tabindex="" readonly>
								<label for="closingpieces">Closing Pieces</label>
							</div>
							<input type ="text" id="openingtagweight" class="hidedisplay" value="0">
							<input type ="text" id="intagwt"  class="hidedisplay" value="0">
							<input type ="text" id="outtagwt"  class="hidedisplay" value="0">
							<input type ="text" id="closingtagweight" class="hidedisplay"  value="0">
							<div class="large-12 columns">&nbsp;</div>
						</div>
					</div>
					<div class="large-4 columns paddingbtm" style="padding-left:0px !important;padding-right:1.8rem !important;">	
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
							<div class="large-12 columns headerformcaptionstyle" >Additional Details</div>
							
							<div class="input-field large-6 columns">
								<input type="text" class="" id="errorweight" name="errorweight" value="" tabindex="" readonly>
								<label for="errorweight">Error Weight</label>
							</div>
							<div class="input-field large-6 columns">
								<input type="text" class="" id="errorpieces" name="errorpieces" value="" tabindex="" readonly>
								<label for="errorpieces">Error Pieces</label>
							</div>
							<?php if($boxwttemplate == 1 ) { ?>
							<div class="static-field large-6 columns ">
								<label>Tag Template</label>	
								<select id="tagtemplateid" name="tagtemplateid" class="chzn-select " data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="103">
									<option value="1">No Print</option>
									<?php foreach($tagtemplate as $key):?>
									<option value="<?php echo $key->tagtemplateid;?>">
									<?php echo $key->tagtemplatename;?></option>
									<?php endforeach;?>	 
								</select>
							</div>
							<?php } ?>
							<?php if($boxwttemplate == 2 ) { ?>
							<div class="static-field large-6 columns">
								<label>Print Template</label>	
								<select id="printtemplateid" name="printtemplateid" class="chzn-select " data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="103">
									<option value="1">No Print</option>
									<?php foreach($printtemplate as $key):?>
									<option value="<?php echo $key->printtemplatesid;?>">
									<?php echo $key->printtemplatesname;?></option>
									<?php endforeach;?>	 
								</select>
							</div>
							<?php } ?>
							<div class="large-12 columns">&nbsp;</div>
						</div>
					</div>
					<div class="large-12 columns paddingbtm">
					<div class="large-12 columns paddingzero">
						<div class="large-12 columns viewgridcolorstyle paddingzero borderstyle">
							<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0 0rem;">
								<span class="large-6 medium-6 small-12 columns text-left small-only-text-center" >Detail List</span>
							</div>
							<?php
									echo '<div class="large-12 columns forgetinggridname" id="innergridwidth" style="padding-left:0;padding-right:0;height:300px;"><div class="desktop row-content inner-gridcontent" id="innergrid" style="max-width:2000px; height:300px;top:0px;">
										<!-- Table content[Header & Record Rows] for desktop-->
										</div>
										<div class="inner-gridfooter footer-content footercontainer" id="innergridgridfooter">
											<!-- Footer & Pagination content -->
										</div></div>';
								?>
						</div>
					</div>
				</div>	
			</div>
		  </div>
		  </form>
		</div>
	</div>
</div>