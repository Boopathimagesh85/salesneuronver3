<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Smsgroups extends CI_Controller {
	function __construct() {
    	parent::__construct();
		$this->load->model('Smsgroups/Smsgroupsmodel');
		$this->load->helper('formbuild');
	}	
	public function index() {
		$moduleid = array(250);
		sessionchecker($moduleid);
		//action
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(250);
		$viewmoduleid = array(250);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Smsgroups/smsgroupsview',$data);
	}
	//new data create
	public function newdatacreate() {
		$this->Smsgroupsmodel->newdatacreatemodel();
	}
	//data fetch
	public function datafetchineditform(){
		$this->Smsgroupsmodel->datafetchineditformmodel();
	}
	//update
	public function smsgroupupdate(){
		$this->Smsgroupsmodel->smsgroupupdatemodel();
	}
	//delete
	public function smsgroupsdelete(){
		$this->Smsgroupsmodel->smsgroupsdeletemodel();
	}
	//sms sender dropdown
	public function sendergroupdd(){
		$this->Smsgroupsmodel->sendergroupddmodel();
	}
	//group name unique value
	public function smsgroupnameunique(){
		$this->Smsgroupsmodel->smsgroupnameuniquemodel();
	}
}