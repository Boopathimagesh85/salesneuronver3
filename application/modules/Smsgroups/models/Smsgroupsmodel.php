<?php
Class Smsgroupsmodel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->model('Base/Crudmodel');
		$this->load->model('Base/Basefunctions');
    }
	//new data add form
	public function newdatacreatemodel() {
		$formfieldsname = explode(',',$_POST['smsgroupselementsname']);
		$formfieldstable = explode(',',$_POST['smsgroupselementstable']);
		$formfieldscolmname = explode(',',$_POST['smsgroupselementscolmn']);
		$elementpartable = explode(',',$_POST['smsgroupselementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$restricttable = array();
		$result = $this->Crudmodel->datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable);
		if(isset($_POST['smsgrpsenderid'])) {
			if($_POST['smsgrpsenderid'] != ''){
				$smsgrpsender = $_POST['smsgrpsenderid'];
			} else {
				$smsgrpsender = 1;
			}
		} else {
			$smsgrpsender = 1;
		}
		if($_POST['templatetypeid'] == 5) {
			
		}
		$updatearray = array('smssettingsid'=>$smsgrpsender);
		$this->db->where('campaigngroups.campaigngroupsid',$result);
		$this->db->update('campaigngroups',$updatearray);
		echo "TRUE";
	}
	//edit data fetch
	public function datafetchineditformmodel() {
		$id = $_GET['primarydataid'];
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('campaigngroups.campaigngroupsname,campaigngroups.templatetypeid,campaigngroups.description,campaigngroups.personalmessage,campaigngroups.smssettingsid,campaigngroups.smstypeid,campaigngroups.templatesid,campaigngroups.signatureid,campaigngroups.smsgrouptypeid,campaigngroups.emailid,campaigngroups.fromname,campaigngroups.notificationtypeid,campaigngroups.subject,campaigngroups.emaillisttypeid');
		$this->db->from('campaigngroups');
		$this->db->where('campaigngroups.campaigngroupsid',$id);
		$this->db->where("FIND_IN_SET('$industryid',campaigngroups.industryid) >", 0);
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array(
					'smsgroupsname'=>$row->campaigngroupsname,
					'typeid'=>$row->templatetypeid,
					'description'=>$row->description,
					'personalmessage'=>$row->personalmessage,
					'senderid'=>$row->smssettingsid,
					'smstypeid'=>$row->smstypeid,
					'leadtemplateid'=>$row->templatesid,
					'signatureid'=>$row->signatureid,
					'emailid'=>$row->emailid,
					'fromname'=>$row->fromname,
					'smstypeid'=>$row->smstypeid,
					'notificationtypeid'=>$row->notificationtypeid,
					'subject'=>$row->subject,
					'smsgrptypeid'=>$row->smsgrouptypeid,
					'emaillisttypeid'=>$row->emaillisttypeid
				);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//update
	public function smsgroupupdatemodel() {
		$formfieldsname = explode(',',$_POST['smsgroupselementsname']);
		$formfieldstable = explode(',',$_POST['smsgroupselementstable']);
		$formfieldscolmname = explode(',',$_POST['smsgroupselementscolmn']);
		$elementpartable = explode(',',$_POST['smsgroupselementspartabname']);
		if(empty($_POST['emaillisttypeid'])){
			$_POST['emaillisttypeid'] = 1;
		}
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['id'];
		$restricttable = array('');
		$result = $this->Crudmodel->dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable);
		if(isset($_POST['smsgrpsenderid'])) {
			if($_POST['smsgrpsenderid'] != ''){
				$smsgrpsender = $_POST['smsgrpsenderid'];
			} else {
				$smsgrpsender = 1;
			}
		} else {
			$smsgrpsender = 1;
		}
		$updatearray = array('smssettingsid'=>$smsgrpsender);
		$this->db->where('campaigngroups.campaigngroupsid',$primaryid);
		$this->db->update('campaigngroups',$updatearray);
		echo "TRUE";
	}
	//delete
	public function smsgroupsdeletemodel() {
		$id = $_POST['datarowid'];
		$cdate = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$header = array(
			'lastupdatedate'=>$cdate,
			'lastupdateuserid'=>$userid,
			'status'=>0
		);
		$this->db->where('campaigngroups.campaigngroupsid',$id);
		$this->db->update('campaigngroups',$header);
		echo "TRUE";
	}
	//sender dropdown
	public function sendergroupddmodel() {
		$i=0;
		$industryid = $this->Basefunctions->industryid;
		$userid = $this->Basefunctions->userid;
		$this->db->select('smssettings.smssettingsid,smssettings.senderid,smssettings.apikey as settingapikey,smsprovidersettings.smssendtypeid');
		$this->db->from('smsprovidersettings');
		$this->db->join('smssettings','smssettings.smsprovidersettingsid=smsprovidersettings.smsprovidersettingsid');
		$this->db->where("FIND_IN_SET('$industryid',smssettings.industryid) >", 0);
		$this->db->where('smssettings.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data[$i] = array('datasid'=>$row->smssettingsid,'dataname'=>$row->senderid,'apikey'=>$row->settingapikey);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	// SMS Group Unique name Template type
	public function smsgroupnameuniquemodel(){
		$industryid = $this->Basefunctions->industryid;
		$templatetypeid = $_POST['templatetypeid'];
		$smsgroupname = $_POST['smsgroupname'];
		if($smsgroupname != "") {
			$result = $this->db->select('campaigngroups.campaigngroupsid')->from('campaigngroups')->where('campaigngroups.templatetypeid',$templatetypeid)->where('campaigngroups.campaigngroupsname',$smsgroupname)->where("FIND_IN_SET('$industryid',campaigngroups.industryid) >", 0)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->campaigngroupsid;
				}
			} else {
				echo "False";
			}
		} else {
			echo "False";
		}
	}
}
?>