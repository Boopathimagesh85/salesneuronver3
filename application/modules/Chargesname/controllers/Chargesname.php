<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Chargesname extends MX_Controller{
    private $chargenamemoduleid = 223;
	public function __construct()    {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Chargesname/Chargesnamemodel');	
    }
    //first basic hitting view
    public function index() {
    	$moduleid = array($this->chargenamemoduleid);
    	sessionchecker($moduleid);
		/*action*/
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
      	$this->load->view('Chargesname/chargesnameview',$data);
	}
	//create chargename
	public function newdatacreate() {  
    	$this->Chargesnamemodel->newdatacreatemodel();
    }
	//information fetchchargename
	public function fetchformdataeditdetails() {
		$this->Chargesnamemodel->informationfetchmodel($this->chargenamemoduleid);
	}
	//update chargename
    public function datainformationupdate() {
        $this->Chargesnamemodel->datainformationupdatemodel();
    }
	//delete chargename
    public function deleteinformationdata() {
    	$this->Chargesnamemodel->deleteoldinformation($this->chargenamemoduleid);
    }
	//unique name check
	public function namecheckfunction()	{
		$this->Chargesnamemodel->namecheckfunctionmodel();
	}
	/**	* Get the Charge Name of (additional charge category type).*/
	public function gettax() {
		$this->Chargesnamemodel->gettax();
	}
	//charges category reload in charges name
	public function fetchdddataviewddval(){
		$this->Chargesnamemodel->fetchdddataviewddvalmodel();
	}
}