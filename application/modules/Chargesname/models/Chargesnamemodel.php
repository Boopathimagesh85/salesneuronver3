<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Chargesnamemodel extends CI_Model{
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//chargename create
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['chargesnameelementsname']);
		$formfieldstable = explode(',',$_POST['chargesnameelementstable']);
		$formfieldscolmname = explode(',',$_POST['chargesnameelementscolmn']);
		$elementpartable = explode(',',$_POST['chargesnameelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
		$userroleid = $this->Basefunctions->userroleid;
		$primaryid = $this->db->insert_id(); //last inserted id
		$this->db->query(" UPDATE ".$fildstable." SET moduleid =223 ,  sortorder = sortorder+".$primaryid.", userroleid = ".$userroleid." WHERE additionalchargetypeid = ".$primaryid." ");
		//update the  tax applicable
		if($_POST['compoundon'] != '' AND $_POST['istaxable'] == 'Yes' ){
			$compoundon=array('taxid'=>trim($_POST['compoundon']));
			$this->db->where('additionalchargetypeid',$primaryid);
			$this->db->update('additionalchargetype',$compoundon);
		}
		echo 'TRUE';
	}
	//Retrive chargename data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['chargesnameelementsname']);
		$formfieldstable = explode(',',$_GET['chargesnameelementstable']);
		$formfieldscolmname = explode(',',$_GET['chargesnameelementscolmn']);
		$elementpartable = explode(',',$_GET['chargesnameelementspartabname']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['chargesnameprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		//retrieve taxes data
		$mdata=$this->db->query("SELECT taxid FROM additionalchargetype
						  WHERE additionalchargetypeid = $primaryid");
		foreach($mdata->result() as $info){
			$compounddata['compoundon']=$info->taxid;
		}
		$result = json_decode($result,true);
		$cresult= array_merge($result,$compounddata);
		$result=json_encode($cresult);	
		echo $result;
	}
	//chargename update
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['chargesnameelementsname']);
		$formfieldstable = explode(',',$_POST['chargesnameelementstable']);
		$formfieldscolmname = explode(',',$_POST['chargesnameelementscolmn']);
		$elementpartable = explode(',',$_POST['chargesnameelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['chargesnameprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		//update the  tax applicable
		if($_POST['compoundon'] != '' AND $_POST['istaxable'] == 'Yes' ){
			$ludate = date($this->Basefunctions->datef);
			$luuserid= $this->Basefunctions->userid;
			$compoundon=array('taxid'=>trim($_POST['compoundon']),'lastupdatedate'=>$ludate,'lastupdateuserid'=>$luuserid);
			$this->db->update('additionalchargetype',$compoundon);
		}
		echo $result;
	}
	//Additional charge detail delete
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['additionalchargeelementstable']);
		$parenttable = explode(',',$_GET['additionalchargeelementspartable']);
		$id = $_GET['additionalchargeprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		$ludate = date($this->Basefunctions->datef);
		$luuserid= $this->Basefunctions->userid;
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				$delete=array('status'=>0,'lastupdatedate'=>$ludate,'lastupdateuserid'=>$luuserid);
				$this->db->where_in('additionalchargetypeid',$id);
				$this->db->update('chargeterritories',$delete);
				echo "TRUE";
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			$delete=array('status'=>0,'lastupdatedate'=>$ludate,'lastupdateuserid'=>$luuserid);
			$this->db->where_in('additionalchargetypeid',$id);
			$this->db->update('chargeterritories',$delete);
			echo "TRUE";
		}
	} 	
	//unique name check
	public function namecheckfunctionmodel() {
		$name = $_POST['name'];
		$this->db->select('additionalchargetypename');
		$this->db->from('additionalchargetype');
		$this->db->where_in('additionalchargetype.additionalchargetypename',$name);
		$this->db->where('additionalchargetype.status',1);
		$result  = $this->db->get();
		if($result->num_rows() > 0) {
			echo "TRUE";
		} else {
			echo "False";
		}
	}
	/**	* Get the Charge Name of (additional charge category type).	**/
	public function gettax() {
		$array=array();
		$data=$this->db->select('taxname,taxid,taxrate')
						->from('tax')
						->join('taxmaster','taxmaster.taxmasterid=tax.taxmasterid')
						->where('taxapplytypeid',3) //additional charge type
						->where('tax.status',$this->Basefunctions->activestatus) //additional charge type
						->get()
						->result();
		foreach($data as $info){
			$array[]=array('id'=>$info->taxid,'name'=>$info->taxname,'rate'=>$info->taxrate);
		}
		echo json_encode($array);
	} 
	//charge category reload in charge name
	public function fetchdddataviewddvalmodel() {
		$i = 0;
		$industryid = $this->Basefunctions->industryid;
		$fieldname = $_GET['dataname'];
		$fieldid = $_GET['dataid'];
		$tablename = $_GET['datatab'];
		$this->db->select("$fieldid,$fieldname,setdefault");
		$this->db->from("$tablename");
		$this->db->where("$tablename".'.status',1);
		$this->db->where("FIND_IN_SET('$industryid',industryid) >", 0);
		$result=$this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data[$i]=array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname,'setdefault'=>$row->setdefault);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
}