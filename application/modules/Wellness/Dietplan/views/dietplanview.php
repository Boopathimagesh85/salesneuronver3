<!DOCTYPE html>
<html lang='en'>
	<head>
		<?php $this->load->view('Base/headerfiles'); ?>
	</head>
	<body class=''>
		<?php
			$device = $this->Basefunctions->deviceinfo();
			$dataset['gridenable'] = 'yes';
			$dataset['moduleid'] = $moduleids;
			$dataset['griddisplayid'] = 'dietplancreationview';
			$dataset['gridtitle'] = $gridtitle['title'];
			$dataset['titleicon'] = $gridtitle['titleicon'];
			$dataset['spanattr'] = array();
			$dataset['gridtableid'] = 'dietplanaddgrid';
			$dataset['griddivid'] = 'dietplanaddgridnav';
			$dataset['forminfo'] = array(array('id'=>'dietplancreationformadd','class'=>'hidedisplay','formname'=>'dietplancreationform'));
			if($device=='phone') {
				$this->load->view('Base/gridmenuheadermobile',$dataset);
				$this->load->view('Base/overlaymobile');
				$this->load->view('Base/viewselectionoverlay');
			} else {
				$this->load->view('Base/gridmenuheader',$dataset);
				$this->load->view('Base/overlay');
			}
			$this->load->view('Base/modulelist');
			$this->load->view('Base/basedeleteform');
		?>
	</body>
		<?php $this->load->view('Base/bottomscript'); ?>
		<script src='<?php echo base_url();?>js/Dietplan/dietplan.js' type='text/javascript'></script>
</html>
