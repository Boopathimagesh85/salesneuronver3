<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dietplan extends MX_Controller {
	function __construct() {
		parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Dietplan/Dietplanmodel');
		$this->load->view('Base/formfieldgeneration');
	}
	public function index() {
		$moduleid= array(72);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp']=$this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp']=$this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle']=$this->Basefunctions->gridtitleinformationfetch($moduleid);
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids']=$viewmoduleid;
		$data['filedmodids']=$viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Dietplan/dietplanview',$data);
	}
	public function newdatacreate() {
		$this->Dietplanmodel->newdatacreatemodel();
	}
	public function fetchformdataeditdetails() {
		$moduleid=array(72);
		$this->Dietplanmodel->informationfetchmodel($moduleid);
	}
	public function datainformationupdate() {
		$this->Dietplanmodel->datainformationupdatemodel();
	}
	public function dietplanproductdetailfetch() {
		$this->Dietplanmodel->dietplanproductdetailfetchmodel();
	}
	public function deleteinformationdata() {
		$moduleid=array(72);
		$this->Dietplanmodel->deleteoldinformation($moduleid);
	}
}