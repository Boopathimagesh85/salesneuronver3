<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Measurementcharts extends MX_Controller {
	function __construct() {
		parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Measurementcharts/Measurementchartsmodel');
		$this->load->view('Base/formfieldgeneration');
	}
	public function index() {
		$moduleid=array(73);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp']=$this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp']=$this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle']=$this->Basefunctions->gridtitleinformationfetch($moduleid);
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids']=$viewmoduleid;
		$data['filedmodids']=$viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Measurementcharts/measurementchartsview',$data);
	}
	public function newdatacreate() {
		$this->Measurementchartsmodel->newdatacreatemodel();
	}
	public function fetchformdataeditdetails() {
		$moduleid=array(73);
		$this->Measurementchartsmodel->informationfetchmodel($moduleid);
	}
	public function datainformationupdate() {
		$this->Measurementchartsmodel->datainformationupdatemodel();
	}
	public function deleteinformationdata() {
		$moduleid=array(73);
		$this->Measurementchartsmodel->deleteoldinformation($moduleid);
	}
}