<!DOCTYPE html>
<html lang='en'>
	<head>
		<?php $this->load->view('Base/headerfiles'); ?>
	</head>
	<body class=''>
		<?php
			$moduleid = implode(',',$moduleids);
			$device = $this->Basefunctions->deviceinfo();
			$dataset['gridenable'] = 'yes';
			$dataset['moduleid']= $moduleids;
			$dataset['griddisplayid'] = 'workoutplanscreationview';
			$dataset['gridtitle'] = $gridtitle['title'];
			$dataset['titleicon'] = $gridtitle['titleicon'];
			$dataset['spanattr'] = array();
			$dataset['gridtableid'] = 'workoutplansaddgrid';
			$dataset['griddivid'] = 'workoutplansaddgridnav';
			$dataset['forminfo'] = array(array('id'=>'workoutplanscreationformadd','class'=>'hidedisplay','formname'=>'workoutplanscreationform'));
			if($device=='phone') {
				$this->load->view('Base/gridmenuheadermobile',$dataset);
				$this->load->view('Base/overlaymobile');
			} else {
				$this->load->view('Base/gridmenuheader',$dataset);
				$this->load->view('Base/overlay');
			}
			$this->load->view('Base/modulelist');
			$this->load->view('Base/basedeleteform');
		?>
	</body>
		<?php $this->load->view('Base/bottomscript'); ?>
		<script src='<?php echo base_url();?>js/Workoutplans/Workoutplans.js' type='text/javascript'></script>
</html>
