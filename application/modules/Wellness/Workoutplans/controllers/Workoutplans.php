<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Workoutplans extends MX_Controller {
	function __construct() {
		parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Workoutplans/Workoutplansmodel');
		$this->load->view('Base/formfieldgeneration');
	}
	public function index() {
		$moduleid=  array(71);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp']=$this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp']=$this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle']=$this->Basefunctions->gridtitleinformationfetch($moduleid);
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids']=$viewmoduleid;
		$data['filedmodids']=$viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Workoutplans/Workoutplansview',$data);
	}
	public function newdatacreate() {
		$this->Workoutplansmodel->newdatacreatemodel();
	}
	public function fetchformdataeditdetails() {
		$moduleid=array(71);
		$this->Workoutplansmodel->informationfetchmodel($moduleid);
	}
	public function datainformationupdate() {
		$this->Workoutplansmodel->datainformationupdatemodel();
	}
	public function deleteinformationdata() {
		$moduleid=array(71);
		$this->Workoutplansmodel->deleteoldinformation($moduleid);
	}
	public function workoutplanproductdetailfetch(){
		$this->Workoutplansmodel->workoutplanproductdetailfetchmodel();
	}
}