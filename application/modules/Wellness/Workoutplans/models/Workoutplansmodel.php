<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Workoutplansmodel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
	}
	public function newdatacreatemodel() {
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		$partablename = $this->Crudmodel->filtervalue($elementpartable);
		$fieldstable = $this->Crudmodel->filtervalue($formfieldstable);
		$gridfieldpartabname = explode(',',$_POST['griddatapartabnameinfo']);
		$gridrows = explode(',',$_POST['numofrows']);
		$girddata = $_POST['griddatas'];
		$girddatainfo = json_decode($girddata, true);
		$gridpartablename =  $this->Crudmodel->filtervalue($gridfieldpartabname);
		$tableinfo = explode(',',$fieldstable);
		$restricttable = explode(',',$_POST['resctable']);
		$primaryid = $this->Crudmodel->datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable);
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		//grid data insertion
		//primary key
		$defdataarr = $this->Crudmodel->defaultvalueget();
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$m=0;
		$h=1;
		$gridfieldpartabname = explode(',',$_POST['griddatapartabnameinfo']);
		$gridrows = explode(',',$_POST['numofrows']);
		$griddata = $_POST['griddatas'];
		$griddatainfo = json_decode($griddata, true);
		foreach($gridrows as $rowcount) {
			$gridfieldsname = explode(',',$_POST['gridcolnames'.$h]);
			//filter grid unique fields table
			$gridfieldtabname = explode(',',$_POST['griddatatabnameinfo'.$h]);
			$gridfieldstable = $this->Crudmodel->filtervalue($gridfieldtabname);
			$gridtableinfo = explode(',',$gridfieldstable);
			for($i=0;$i<$rowcount;$i++) {
				foreach( $gridtableinfo as $gdtblname ) {
					${'$gcdata'.$m} = array();
					$t=0;
					foreach($gridfieldsname AS $gdfldsname) {
						if(strcmp( trim($gridfieldtabname[$t]),trim($gdtblname) ) == 0 ) {
							if( isset( $griddatainfo[$i][$gdfldsname] ) ) {
								$name = explode('_',$gdfldsname);
								if( ctype_alpha($griddatainfo[$i][$gdfldsname]) && $griddatainfo[$i][$gdfldsname] != "" ) {
									${'$gcdata'.$m}[$gdfldsname] = ucwords( $griddatainfo[$i][$gdfldsname] );
								} else if( $griddatainfo[$i][$gdfldsname] != "" ) {
									${'$gcdata'.$m}[$gdfldsname] = $griddatainfo[$i][$gdfldsname];
								}
							}
						}
					}
					if(count(${'$gcdata'.$m})>0) {
						${'$gcdata'.$m}[$primaryname]=$primaryid;
						$gnewdata = array_merge(${'$gcdata'.$m},$defdataarr);
						$removehidden=array('workoutplansworkoutdetailsid');
						for($mk=0;$mk<count($removehidden);$mk++){
							unset($gnewdata[$removehidden[$mk]]);
						}
						$inarr = array_filter($gnewdata);
						$this->db->insert($gdtblname,$inarr);
					}
					$m++;
				}
			}
			$h++;
		}
		echo 'TRUE';
	}
	public function informationfetchmodel($moduleid) {
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementspartabname']);
		$restricttable = array('workoutplansworkoutdetails');
		$partablename = $this->Crudmodel->filtervalue($elementpartable);
		$fieldstable = $this->Crudmodel->filtervalue($formfieldstable);
		$primaryid = $_GET['dataprimaryid'];
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetchwithrestrict($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$fieldstable,$partablename,$formfieldstable,$restricttable,$moduleid);
		echo $result;
	}
	public function datainformationupdatemodel() {
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		$partablename = $this->Crudmodel->filtervalue($elementpartable);
		$fieldstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fieldstable);
		$primaryid = $_POST['primarydataid'];
		$restricttable = explode(',',$_POST['resctable']);
		$this->Crudmodel->dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable);

		//get the old product detail
		$oldworkoutplansworkoutdetailsid =array();
		$workoutplansworkoutdetails=$this->db->select('workoutplansworkoutdetailsid')
		->from('workoutplansworkoutdetails')
		->where('workoutplansid',$primaryid)
		->where('status',$this->Basefunctions->activestatus)
		->get();
		foreach($workoutplansworkoutdetails->result() as $info) {
			$oldworkoutplansworkoutdetailsid[]=$info->workoutplansworkoutdetailsid;
		}
		$gridfieldpartabname = explode(',',$_POST['griddatapartabnameinfo']);
		$gridrows = explode(',',$_POST['numofrows']);
		$griddata = $_POST['griddatas'];
		$griddatainfo = json_decode($griddata, true);
		//filter unique grid parent table
		$gridpartablename =  $this->Crudmodel->filtervalue($gridfieldpartabname);
		for($lm=0;$lm < count($griddatainfo);$lm++) {
			$newworkoutplansworkoutdetailsid[]=$griddatainfo[$lm]['workoutplansworkoutdetailsid'];
		}
		$deletedworkoutplansworkoutdetailsid=ARRAY();
		//find deleted records
		for($m=0;$m < count ($oldworkoutplansworkoutdetailsid);$m++) {
			if(in_array($oldworkoutplansworkoutdetailsid[$m],$newworkoutplansworkoutdetailsid)) {
			} else {
				$deletedworkoutplansworkoutdetailsid[]=$oldworkoutplansworkoutdetailsid[$m];
			}
		}
		if(count($deletedworkoutplansworkoutdetailsid) > 0) {
			//delete productdetail and further tables
			for($k=0;$k<count($deletedworkoutplansworkoutdetailsid);$k++) {
				$this->Crudmodel->outerdeletefunction('workoutplansworkoutdetails','workoutplansworkoutdetailsid','',$deletedworkoutplansworkoutdetailsid[$k]);
			}
		}
		//Insert the New quote records
		$defdataarr = $this->Crudmodel->defaultvalueget();
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$m=0;
		$h=1;
		foreach($gridrows as $rowcount) {
			$gridfieldsname = explode(',',$_POST['gridcolnames'.$h]);
			//filter grid unique fields table
			$gridfieldtabname = explode(',',$_POST['griddatatabnameinfo'.$h]);
			$gridfieldstable = $this->Crudmodel->filtervalue($gridfieldtabname);
			$gridtableinfo = explode(',',$gridfieldstable);
			for($i=0;$i<$rowcount;$i++) {
				foreach( $gridtableinfo as $gdtblname ) {
					${'$gcdata'.$m} = array();
					$t=0;
					foreach($gridfieldsname AS $gdfldsname) {
						if(strcmp( trim($gridfieldtabname[$t]),trim($gdtblname) ) == 0 ) {
							if( isset( $griddatainfo[$i][$gdfldsname] ) ) {
								$name = explode('_',$gdfldsname);
								if( ctype_alpha($griddatainfo[$i][$gdfldsname]) && $griddatainfo[$i][$gdfldsname] != "" ) {
									${'$gcdata'.$m}[$gdfldsname] = ucwords( $griddatainfo[$i][$gdfldsname] );
								} else if( $griddatainfo[$i][$gdfldsname] != "" ) {
									${'$gcdata'.$m}[$gdfldsname] = $griddatainfo[$i][$gdfldsname];
								}
							}
						}
					}
					if(count(${'$gcdata'.$m})>0) {
						${'$gcdata'.$m}[$primaryname]=$primaryid;
						$gnewdata = array_merge(${'$gcdata'.$m},$defdataarr);
						//unset the hidden variables
						$removehidden=array('workoutplansworkoutdetailsid');
						for($mk=0;$mk<count($removehidden);$mk++){
							unset($gnewdata[$removehidden[$mk]]);
						}
						if($griddatainfo[$i]['workoutplansworkoutdetailsid'] == 0){ //new-data
							$this->db->insert($gdtblname,array_filter($gnewdata));
							$workoutplansworkoutdetailsid=$this->db->insert_id();
						}
						if($griddatainfo[$i]['workoutplansworkoutdetailsid'] > 0){ //update-existing data
							$this->db->where('workoutplansid',$griddatainfo[$i]['workoutplansworkoutdetailsid']);
							$this->db->update($gdtblname,array_filter($gnewdata));
							$workoutplansworkoutdetailsid=$griddatainfo[$i]['workoutplansworkoutdetailsid'];
						}
					}
					$m++;
				}
			}
			$h++;
		}		
		echo 'TRUE';
	}
	public function workoutplanproductdetailfetchmodel(){
		$workoutplanid=trim($_GET['primarydataid']);
		$this->db->select('trainingtype.trainingtypeid,trainingtype.trainingtypename,exercise.exerciseid,exercise.exercisename,categoryname.categorynameid,categoryname.categorynamename,workoutplansworkoutdetails.sets,workoutplansworkoutdetails.reps,workoutplansworkoutdetails.time,workoutplansworkoutdetails.distance,workoutplansworkoutdetails.weight,intensity.intensityid,intensity.intensityname,workoutplansworkoutdetails.notes');
		$this->db->from('workoutplansworkoutdetails');
		$this->db->join('trainingtype','trainingtype.trainingtypeid=workoutplansworkoutdetails.trainingtypeid');
		$this->db->join('exercise','exercise.exerciseid=workoutplansworkoutdetails.exerciseid');
		$this->db->join('categoryname','categoryname.categorynameid=workoutplansworkoutdetails.categorynameid');
		$this->db->join('intensity','intensity.intensityid=workoutplansworkoutdetails.intensityid');
		$this->db->where('workoutplansworkoutdetails.workoutplansid',$workoutplanid);
		$this->db->where('workoutplansworkoutdetails.status',$this->Basefunctions->activestatus);
		$data=$this->db->get();
		if($data->num_rows() > 0) {
			$j=0;
			foreach($data->result() as $value) {
				$productdetail->rows[$j]['id'] = $j;
				$productdetail->rows[$j]['cell']=array(
						$value->trainingtypename,
						$value->trainingtypeid,
						$value->exercisename,
						$value->exerciseid,
						$value->categorynamename,
						$value->categorynameid,
						$value->sets,
						$value->reps,
						$value->time,
						$value->distance,
						$value->weight,
						$value->intensityname,
						$value->intensityid,
						$value->notes
				);
				$j++;
			}
		}
		echo  json_encode($productdetail);
	}
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primarydataid'];
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				echo 'TRUE';
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			echo 'TRUE';
		}
	}
}