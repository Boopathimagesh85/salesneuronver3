<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Opportunity extends MX_Controller{
	//To load the Register View
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Opportunity/Opportunitymodel');
		$this->load->view('Base/formfieldgeneration');
    }
	public function index() {
		$moduleid = array(84,93,204,131);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['dashboradviewdata'] = $this->Basefunctions->dashboarddataviewbydropdown($moduleid);
		$this->load->view('Opportunity/opportunityview',$data);
	}
	//Create New data
	public function newdatacreate() {  
    	$this->Opportunitymodel->newdatacreatemodel();
    }
	//information fetch
	public function fetchformdataeditdetails() {
		$moduleid = array(84,93,204,131);
		$this->Opportunitymodel->informationfetchmodel($moduleid);
	}
	//Update old data
    public function datainformationupdate() {
        $this->Opportunitymodel->datainformationupdatemodel();
    }
	//delete old information
    public function deleteinformationdata() {
		$moduleid = array(84,93,204,131);
        $this->Opportunitymodel->deleteoldinformation($moduleid);
    }
	//account name based contact name
	public function fetchcontatdatawithmultiplecond() {
		$this->Opportunitymodel->fetchcontatdatawithmultiplecondmodel();
	}
	//clone data
	public function viewdataclone() {
		$empid = $this->Basefunctions->userid;
		$vmaxid = $_POST['rowid'];
		$parenttable = 'opportunity';
		$childtabinfo = '';
		$fieldinfo = 'opportunityid';
		$fieldvalue = $vmaxid;
		$updatecloumn = 'opportunityname';
		$viewid = $this->Basefunctions->viewdataclonemodel($parenttable,$childtabinfo,$fieldinfo,$fieldvalue);
		if($updatecloumn!='') {
			$this->db->query(' UPDATE '.$parenttable.' SET '.$updatecloumn.' = concat('.$updatecloumn.'," clone") where '.$fieldinfo.' = '.$viewid.'');
		}
		echo 'TRUE';
	}
}