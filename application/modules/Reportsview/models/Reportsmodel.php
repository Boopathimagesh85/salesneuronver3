<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Reportsmodel extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
	//get module name
	public function reportmodeldd() {
	   $industryid=$this->Basefunctions->industryid;
	   $data = $this->db->select('moduleid ,menuname')
							->from('module')
							->where_in('modulecategorytypeid',(2))
							->where_in('status',array(1,3))
							->where("FIND_IN_SET('".$industryid."',industryid) >", 0)
							->get()
							->result();
		return $data;
	}
	public function getreportdetailsmodel($id) {
		$this->db->select('reportfolder.reportfoldername,reportfolder.description,reportfolder.setaspublic,reportfolder.setdefault');
		$this->db->from('reportfolder');
		$this->db->where('reportfolder.reportfolderid',$id);
		$result=$this->db->get()->result();
		foreach ($result as $row){
			$getreportinfo=array('reportfoldername'=>$row->reportfoldername,'description'=>$row->description,'setaspublic'=>$row->setaspublic,'setdefault'=>$row->setdefault);
		}
		echo json_encode($getreportinfo);
	}
	
	//view report folder
	public function viewreportfolder($tablename,$sortcol,$sortord,$pagenum,$rowscount) {
		$dataset = 'reportfolderid,reportfoldername,description,setaspublic,setdefault';
		$status = 'status NOT IN (0,3)';
		/* pagination */
		$query = 'select '.$dataset.' from reportfolder WHERE '.$status.' ORDER BY'.' '.$sortcol.' '.$sortord;
		$page = $this->Basefunctions->generatepage($query,$pagenum,$rowscount);
		/* query */
		$data = $this->db->query('select '.$dataset.' from reportfolder WHERE '.$status.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$page['start'].','.$page['records'].'');
		$finalresult=array($data,$page,'');
		return $finalresult;
	}
	//view report list
	public function reportlist($tablename,$sortcol,$sortord,$pagenum,$rowscount) {
		$userroleid = $this->Basefunctions->userroleid;
		$industryid=$this->Basefunctions->industryid;
		$pagenum -= 1;
		$start = $pagenum * $rowscount;
		$this->db->select('SQL_CALC_FOUND_ROWS reportid,reportid,reportname,reportdescription,schedulereports,reportfolder.reportfolderid,reportfolder.reportfoldername,parentmodule.modulename as parentmodulename,module.menuname',false);
		$this->db->from('report');
		$this->db->join('reportfolder','reportfolder.reportfolderid=report.reportfolderid');
		$this->db->join('module','module.moduleid=report.moduleid');
		$this->db->join('module as parentmodule','parentmodule.moduleid=module.parentmoduleid');
		$this->db->where('report.status',$this->Basefunctions->activestatus);
		if(isset($_GET['reportfolder'])) {
			if($_GET['reportfolder'] != 'all') {
				$this->db->where('report.reportfolderid',$_GET['reportfolder']);
			}
		}
		$this->db->where("FIND_IN_SET('".$industryid."',".$tablename.".industryid) >", 0);
		//$this->db->order_by($sortcol,$sortord);
		if($userroleid == 3) {
			$this->db->where('report.userroleid',3);
		} else {
			$this->db->where_not_in('report.userroleid',3);
		}
		$this->db->order_by('reportfolder.reportfolderid',$sortord);
		$this->db->order_by($sortcol,$sortord);
        $this->db->limit($rowscount,$start);
		return $this->db->get();
	}
	//create report folder
	public function createreportfolder() {
		if(!isset($_POST['reportfoldersetpublic'])){
			$_POST['reportfoldersetpublic']='No';
		}
		if(!isset($_POST['reportfoldersetdefault'])){
			$_POST['reportfoldersetdefault']='No';
		} 
		if(isset($_POST['reportfoldersetpublic']) and $_POST['reportfoldersetpublic'] == ''){
			$_POST['reportfoldersetpublic']='No';
		}
		if(!isset($_POST['reportfoldersetdefault'])and $_POST['reportfoldersetdefault'] == ''){
			$_POST['reportfoldersetdefault']='No';
		}
		//insert script 
		$insertarray=array('reportfoldername'=>trim($_POST['reportsfoldername']),'setaspublic'=>$_POST['reportfoldersetpublic'],'setdefault'=>$_POST['reportfoldersetdefault'],'description'=>trim($_POST['reportfolderdescription']),'industryid'=>$this->Basefunctions->industryid,'branchid'=>$this->Basefunctions->branchid,'createdate'=>date($this->Basefunctions->datef),'lastupdatedate'=>date($this->Basefunctions->datef),'createuserid'=>$this->Basefunctions->userid,'lastupdateuserid'=>$this->Basefunctions->userid,'status'=>$this->Basefunctions->activestatus);
		$this->db->insert('reportfolder',$insertarray);
		
		$setdefault = $_POST['reportfoldersetdefault'];
		$primaryid = $this->db->insert_id();
		$this->setdefaultupdate($setdefault,$primaryid);
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' created Report Folder - '.$_POST['reportsfoldername'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,35);
		echo true;
	}
	//update report folder
	public function updatereportfolder() {
		$reportfolderid=$_POST['reportfolderid'];
		if(!isset($_POST['reportfoldersetpublic'])){
		$_POST['reportfoldersetpublic']='No';
		}
		if(!isset($_POST['reportfoldersetdefault'])){
		$_POST['reportfoldersetdefault']='No';
		}
		if(isset($_POST['reportfoldersetpublic']) and $_POST['reportfoldersetpublic'] == ''){
			$_POST['reportfoldersetpublic']='No';
		}
		if(!isset($_POST['reportfoldersetdefault'])and $_POST['reportfoldersetdefault'] == ''){
			$_POST['reportfoldersetdefault']='No';
		}
		//insert script
		$updatearray=array('reportfoldername'=>trim($_POST['reportsfoldername']),'setaspublic'=>$_POST['reportfoldersetpublic'],'setdefault'=>$_POST['reportfoldersetdefault'],'description'=>trim($_POST['reportfolderdescription']),'lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid,'status'=>$this->Basefunctions->activestatus);
		$this->db->where('reportfolderid',$reportfolderid);
		$this->db->update('reportfolder',$updatearray);
		$setdefault = $_POST['reportfoldersetdefault'];
		$primaryid = $reportfolderid;
		$this->setdefaultupdate($setdefault,$primaryid);
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' updated Report Folder - '.$_POST['reportsfoldername'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$activity ,$userid,35);
		echo true;
	}
	//delete report folder
	public function deletereportfolder() {
		$primaryid = $_GET['primaryid'];
		$updatefolderid = $_GET['updatefolderid'];
		$updatenewfolder=array('reportfolderid'=>$updatefolderid,'lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid);
		$this->db->where_in('report.reportfolderid',$primaryid);
		$this->db->update('report',$updatenewfolder);
		$updatearray=array('lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid,'status'=>$this->Basefunctions->deletestatus);
		$this->db->where('reportfolderid',$primaryid);
		$this->db->update('reportfolder',$updatearray);
		echo true;
	}
	//sort report folder
	//data sort order
	public function datarowsortingmodel() {
		$rowids = $_POST['rowids'];
		$sortfield = $_POST['sortfield'];
		$dataids = explode(',',$rowids);
		$i=1;
		foreach($dataids as $id) {
			$this->db->query("UPDATE reportfolder SET  reportfolder.sortorder=$i WHERE reportfolderid=$id" );
			$i++;
		}
		echo "TRUE";
	}
	public function loadreportfoldername() {
		$userroleid = $this->Basefunctions->userroleid;
		$userid = $this->Basefunctions->logemployeeid;
		$this->db->select('reportfolderid,reportfoldername,description,setaspublic,setdefault');
		$this->db->from('reportfolder');
		$this->db->where('(( reportfolder.createuserid = '.$userid.' AND reportfolder.status =1) OR ( reportfolder.setaspublic = "Yes" AND reportfolder.status =1 ))');
		if($userroleid == 3) {
			$this->db->where('reportfolder.userroleid',3);
		} else {
			$this->db->where_not_in('reportfolder.userroleid',3);
		}
		$this->db->order_by('sortorder','asc');
		$data=$this->db->get();
		if($data->num_rows() > 0){
			foreach($data->result() as $info){
				$jsondata[]=array('result'=>true,'reportfoldername'=>$info->reportfoldername,'reportfolderid'=>$info->reportfolderid);
			}
		} else {
			$jsondata=array();
		}
		echo json_encode($jsondata);
	}
	//reportdelete
	public function reportdelete() {
		$reportid=trim($_GET['primarydataid']);
		$deletearray=array('lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid,'status'=>$this->Basefunctions->deletestatus);
		$deletetable=array('report','reportcondition');
		$sndefault = $this->Basefunctions->singlefieldfetch('sndefault','reportid','report',$reportid); //Check default value
		if($sndefault == 2) {
			echo 'sndefault';
		} else {
			for($m=0;$m<count($deletetable);$m++)
			{
				$this->db->where('reportid',$reportid);
				$this->db->update($deletetable[$m],$deletearray);
			}
			//audit-log
			$userid = $this->Basefunctions->logemployeeid;
			$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
			$reportname = $this->Basefunctions->singlefieldfetch('reportname','reportid','report',$reportid);
			$activity = ''.$user.' deleted Report - '.$reportname.'';
			$this->Basefunctions->notificationcontentadd($reportid,'Report',$activity ,$userid,35);
			//audit-log
			echo true;
		}
	}	
	
	//fetch colname & colmodel information fetch model
    public function reportgridinformationfetchmodel($creationid) {
		//fetch show col ids
		$this->db->select('report.viewcreationcolumnid,report.reportid,report.reportname,report.groupbyviewcreationcolumnid',false);
		$this->db->from('report');
		$this->db->where_in('report.reportid',$creationid);
		$result = $this->db->get()->result();
		foreach($result as $row) {
			$colids = $row->viewcreationcolumnid;
			$reportname = $row->reportname;
			$summarygroup = array($row->groupbyviewcreationcolumnid);
		}
		$viewcolids = explode(',',$colids);
		$data = array();
		$data['reportname'] = $reportname;
		$i = 0;
		
		foreach($viewcolids as $viewcolid) {
			//for fetch colname & colmodel fetch
			$this->db->select("viewcreationcolumns.viewcreationmoduleid,viewcreationcolumns.viewcreationcolumnname,
			viewcreationcolumns.viewcreationcolmodelname,
			viewcreationcolumns.viewcreationcolmodelindexname,
			viewcreationcolumns.viewcreationcolmodeljointable,
			viewcreationcolumns.viewcreationcolumnid,
			viewcreationcolumns.viewcreationcolmodelaliasname,
			viewcreationcolumns.viewcreationcolmodeltable,
			viewcreationcolumns.viewcreationparenttable,
			viewcreationcolumns.viewcreationmoduleid,
			viewcreationcolumns.viewcreationjoincolmodelname,
			viewcreationcolumns.viewcreationcolumnviewtype,
			viewcreationcolumns.viewcreationtype,
			viewcreationcolumns.uitypeid,
			viewcreationcolumns.viewcreationcolmodelcondname,viewcreationcolumns.viewcreationcolmodelcondvalue,linkable,linkmodule.modulelink,viewcreationcolumns.linkmoduleid");
			$this->db->from('viewcreationcolumns');
			$this->db->join('module as linkmodule','linkmodule.moduleid=viewcreationcolumns.linkmoduleid');
			$this->db->where_in('viewcreationcolumns.viewcreationcolumnid',$viewcolid);
			$this->db->where('viewcreationcolumns.status',$this->Basefunctions->activestatus);
			$showfield = $this->db->get();		
			
			if($showfield->num_rows() >0) {
				// grouping
				if(count($summarygroup) > 0) {
					for($m=0;$m<count($summarygroup);$m++) {
						$data['groupcolumnname'][$m] = $summarygroup[$m];
					}
				} else {
					$data['groupcolumnname'] = 0;
				}
				//get summary field related data
				//grid data viewcreationcolmodelindexname
				foreach($showfield->result() as $show) {
					$data['colname'][$i]=$show->viewcreationcolumnname;
					$data['moduleid'][$i]=$show->viewcreationmoduleid;
					$data['colmodelname'][$i]=$show->viewcreationcolmodelname;
					$data['colmodelaliasname'][$i]=$show->viewcreationcolmodelaliasname;
					$data['coltablename'][$i]=$show->viewcreationcolmodeltable;
					$data['coljointablename'][$i]=$show->viewcreationcolmodeljointable;
					$data['coljoinmodelname'][$i]=$show->viewcreationjoincolmodelname;
					$data['columndbname'][$i]=$show->viewcreationcolmodelindexname;
					$data['colmodelindex'][$i]=$show->viewcreationcolmodeljointable .'.'.$show->viewcreationcolmodelindexname;
					$data['colmodelparenttable'][$i]=$show->viewcreationparenttable;
					$data['colmodelviewtype'][$i]=$show->viewcreationcolumnviewtype;
					$data['colmodeldatatype'][$i]=$show->viewcreationtype;
					$data['colmodelcondname'][$i]=$show->viewcreationcolmodelcondname;
					$data['colmodelcondvalue'][$i]=$show->viewcreationcolmodelcondvalue;
					$data['colmodelmoduleid'][$i]=$show->viewcreationmoduleid;
					$data['colmodeluitype'][$i]=$show->uitypeid;
					$data['colmodeltype'][$i]='text';
					$data['linkable'][$i]=$show->linkable;
					$data['linkpath'][$i]=$show->modulelink;
					$data['linkmoduleid'][$i]=$show->linkmoduleid;
					$i++;
				}
			}
		}
		return $data;
    }	
    public function conditioninfofetch($viewid) {
		$i=0;
		$multiarray = array();
		//user for get the condition values;
		$this->db->select('viewcreationcolumns.viewcreationcolmodeljointable,viewcreationcolumns.viewcreationcolmodelname,viewcreationcolumns.viewcreationcolmodelindexname,reportcondition.viewcreationconditionname,reportcondition.viewcreationconditionvalue,reportcondition.reportid,reportcondition.viewcreationandorvalue,uitypeid',false);
		$this->db->from('reportcondition');
		$this->db->join('viewcreationcolumns','viewcreationcolumns.viewcreationcolumnid=reportcondition.viewcreationcolumnid');
		$this->db->where_in('reportcondition.reportid',$viewid);
		$this->db->where_in('reportcondition.status',$this->Basefunctions->activestatus);
		$condtionval=$this->db->get();
		foreach($condtionval->result() as $row) {
			$multiarray[$i]=array(
					'0'=>$row->viewcreationcolmodeljointable,
					'1'=>$row->viewcreationcolmodelindexname,
					'2'=>$row->viewcreationconditionname,
					'3'=>$row->viewcreationconditionvalue,
					'4'=>$row->viewcreationandorvalue,
					'5'=>$row->uitypeid
					);
			$i++;
		}
		return $multiarray;
    }
    //fetch dynamic view parent table information (case 1 )
    public function parenttableinfo($leadfieldtab,$viewcolmoduleids) {
		$moduleids = explode(',',$viewcolmoduleids);
		$this->db->select("viewcreationcolumns.viewcreationcolmodeljointable,
		viewcreationcolumns.viewcreationjoincolmodelname,viewcreationcolumns.viewcreationcolmodeltable,
		viewcreationcolumns.viewcreationparenttable");
		$this->db->from('viewcreationcolumns');
		$this->db->where('viewcreationcolumns.status',1);
		$this->db->where('viewcreationcolumns.viewcreationcolmodeltable',$leadfieldtab);
		$this->db->where_in('viewcreationcolumns.viewcreationmoduleid',$moduleids);
		$this->db->limit(1,0);
		$result = $this->db->get();
		return $result;
    }
	//fetch dynamic view parent table information ( case 2 )
    public function parentjointableinfo($leadfieldtab,$viewcolmoduleids) {
		$moduleids = explode(',',$viewcolmoduleids);
		$this->db->select("viewcreationcolumns.viewcreationcolmodeljointable,viewcreationcolumns.viewcreationjoincolmodelname,viewcreationcolumns.viewcreationcolmodeltable,viewcreationcolumns.viewcreationparenttable,viewcreationcolumns.viewcreationcolmodelcondname,viewcreationcolumns.viewcreationcolmodelcondvalue");
		$this->db->from('viewcreationcolumns');
		$this->db->where('viewcreationcolumns.status',1);
		$this->db->where('viewcreationcolumns.viewcreationcolmodeljointable',$leadfieldtab);
		$this->db->where_in('viewcreationcolumns.viewcreationmoduleid',$moduleids);
		$this->db->limit(1,0);
		$result = $this->db->get();
		return $result;
    }
    //attribute join tables
	public function attributejoin($parenttable) {
		$djoinq = "";
		$djoinq = $djoinq.' LEFT OUTER JOIN module ON module.moduleid = '.$parenttable.'.moduleid';
		$djoinq = $djoinq.' LEFT OUTER JOIN moduleattributesetting ON moduleattributesetting.moduleid = module.moduleid';
		$djoinq = $djoinq.' LEFT OUTER JOIN attributeset ON attributeset.attributesetid = moduleattributesetting.attributesetid';
		$djoinq = $djoinq.' LEFT OUTER JOIN attributesetassign ON attributesetassign.attributesetid = attributeset.attributesetid';
		$djoinq = $djoinq.' LEFT OUTER JOIN attribute ON attribute.attributeid = attributesetassign.attributeid';
		return $djoinq;
	}
	//date view
	public function reportdatecolumn($reportdatecolumnid)
	{
		//for fetch colname & colmodel fetch
			$this->db->select("viewcreationcolumns.viewcreationcolumnname,
			viewcreationcolumns.viewcreationcolmodelname,
			viewcreationcolumns.viewcreationcolmodelindexname,
			viewcreationcolumns.viewcreationcolmodeljointable,
			viewcreationcolumns.viewcreationcolumnid,
			viewcreationcolumns.viewcreationcolmodelaliasname,
			viewcreationcolumns.viewcreationcolmodeltable,
			viewcreationcolumns.viewcreationparenttable,
			viewcreationcolumns.viewcreationmoduleid,
			viewcreationcolumns.viewcreationjoincolmodelname,
			viewcreationcolumns.viewcreationcolumnviewtype,
			viewcreationcolumns.viewcreationtype,
			viewcreationcolumns.viewcreationcolmodelcondname,viewcreationcolumns.viewcreationcolmodelcondvalue");
			$this->db->from('viewcreationcolumns');
			$this->db->join('module as linkmodule','linkmodule.moduleid=viewcreationcolumns.linkmoduleid');
			$this->db->where('viewcreationcolumns.viewcreationcolumnid',$reportdatecolumnid);
			$this->db->limit(1);
			$showfield = $this->db->get();
			foreach($showfield->result() as $inj){
				$datearr=array('parent'=>$inj->viewcreationparenttable,'columnname'=>$inj->viewcreationcolmodelindexname);
			}
			return $datearr;
	}
	//where condition generation
    public function viewwhereclausegeneration($conditionvalarray) {
		$whereString="";
		$count = count($conditionvalarray);
		$braces = '';
		$m=0;
		foreach ($conditionvalarray as $key) {
			if($m == 0) {
				$key[4] = "";
				$whereString .= " AND ".str_repeat("(",$count-1)."";
			} else {
				if($key[4] != 'AND' and $key[4] != 'OR') {
					$key[4] = 'AND';
				}
			}
			if($m >= 1){
				$braces = ')';
			}
			if( $key[5] == 20 ) {
				$braces = '';
				//employee
				$whereString.=$this->conditiongenerate($key,$braces);
				//group
				$gkey = array(0=>'employeegroup',1=>'employeegroupname',2=>$key[2],3=>$key[3],4=>'OR');
				$whereString.=$this->conditiongenerate($gkey,$braces);
				//user role
				$ukey = array(0=>'userrole',1=>'userrolename',2=>$key[2],3=>$key[3],4=>'OR');
				$whereString.=$this->conditiongenerate($ukey,$braces);
			} else {
				$whereString.=$this->conditiongenerate($key,$braces);
			}
			$m++;
		}
		if($whereString !="") {
			$whereString .= "";
		}
		return $whereString;
    }
	//condition generation
	public function conditiongenerate($key,$braces) {
		switch($key[2]) {
			case "Equalto":
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." = '".$key[3]."'".$braces;
				break;
			case "NotEqual": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." != '".$key[3]."'".$braces;
				break;
			case "Startwith":
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." LIKE '".$key[3].'%'."'".$braces;
				break;
			case "Endwith": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." LIKE '".'%'.$key[3]."'".$braces;
				break;
			case "Middle": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." LIKE '".'%'.$key[3].'%'."'".$braces;
				break;
			case "IN": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." IN ('".$key[3]."')".$braces;
				break;
			case "NOT IN": 
				return $whereString.=" ".$key[4]." ".$key[0].'.'.$key[1]." NOT IN ('".$key[3]."')".$braces;
				break;
			case "GreaterThan": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." > '" .$key[3]."'".$braces;
				break;
			case "LessThan": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." < '" .$key[3]."'".$braces;
				break;
			case "GreaterThanEqual": 
				return $whereString= " ".$key[4]." ".$key[0].'.'.$key[1]." >= '" .$key[3]."'".$braces;
				break;
			case "LessThanEqual": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." <= '" .$key[3]."'".$braces;
				break;
			default:
				return $whereString="";
				break;
		}
	}
	 //where condition generation
    public function whereclausegeneration($conditionvalarray) {
		$whereString="";
		foreach ($conditionvalarray as $key) {
			if($key[4] == '' or $key[4] == null){
				$key[4]='AND';
			}
			switch($key[2]) {
				case "Equalto": 
					$whereString .= $key[0].'.'.$key[1]. " = '".$key[3]."'"." ". $key[4]." ";
					break;
				case "NotEqual": 
					$whereString .= $key[0].'.'.$key[1]. " != '" .$key[3]."'". " ". $key[4]." ";
					break;
				case "Startwith":
					$whereString .= $key[0].'.'.$key[1]. " LIKE '".$key[3].'%'."'"." ".  $key[4]." ";
					break;
				case "Endwith": 
					$whereString .= $key[0].'.'.$key[1]. " LIKE '".'%'.$key[3]."'". " ". $key[4]." ";
					break;
				case "Middle": 
					$whereString .= $key[0].'.'.$key[1]. " LIKE '".'%'.$key[3].'%'."'". " ". $key[4]." ";
					break;
				case "IN": 
					$whereString .= $key[0].'.'.$key[1]. " IN '".$key[3]."'". " ". $key[4]." ";
					break;
				case "NOT IN": 
					$whereString .= $key[0].'.'.$key[1]. " NOT IN '".$key[3]."'". " ". $key[4]." ";
					break;
				case "GreaterThan": 
					$whereString .= $key[0].'.'.$key[1]. " > '" .$key[3]."'"." ".  $key[4]." ";
					break;
				case "LessThan": 
					$whereString .= $key[0].'.'.$key[1]. " < '" .$key[3]."'". " ". $key[4]." ";
					break;
				case "GreaterThanEqual": 
					$whereString .= $key[0].'.'.$key[1]." >= '" .$key[3]."'"." ".  $key[4]." ";
					break;
				case "LessThanEqual": 
					$whereString .= $key[0].'.'.$key[1]. " <= '" .$key[3]."'". " ". $key[4]." ";
					break;
				default:
					$whereString .= "";
					break;
			}
		}
		$whereString= substr($whereString, 0, -4);
		return $whereString;
    }
	//report groupby/sortby record 
	public function reportsortbygroupbyquery($reportid) {
		//fetch the clauses
		$data=$this->db->select('reportgroupcondition.viewcreationcolumnid,viewcreationcolumns.viewcreationcolmodelname,sortby,typeby,viewcreationcolumns.viewcreationcolmodeljointable,viewcreationcolumns.viewcreationtype,viewcreationcolumns.viewcreationcolmodelindexname')
			->from('reportgroupcondition')
			->join('viewcreationcolumns','viewcreationcolumns.viewcreationcolumnid=reportgroupcondition.viewcreationcolumnid')
			->where('reportgroupcondition.reportid',$reportid)
			->where('reportgroupcondition.status',1)
			->get();
		if($data->num_rows() > 0) {
			$viecolids=array();
			$groupby=array();
			$groupbyname=array();
			$groupquery=" group by ";
			$groupquerywotbl=" group by ";
			$orderbyquery=" order by ";
			foreach($data->result() as $info) {
				if($info->sortby =='asc' or $info->sortby == 'desc') {
					$sort=$info->sortby;
				} else {
					$sort=1;
				}
				if($info->typeby==1){ //Group By Only
					$groupby[]=$info->viewcreationcolumnid;
					$groupbyname[]=$info->viewcreationcolmodelname;
					$groupquery.=$info->viewcreationcolmodeljointable.'.'.$info->viewcreationcolmodelname.',';
				} else if($info->typeby==2) {	//Order By Only
					$orderbyquery.=$info->viewcreationcolmodelname.' '.$info->sortby.',';	
				} else if($info->typeby==3 || $info->typeby==0) { //Group By & Oderby 
					$groupby[]=$info->viewcreationcolumnid;
					$groupbyname[]=$info->viewcreationcolmodelname;
					if($info->viewcreationtype==3){
						$groupquery.=$info->viewcreationcolmodeljointable.'.'.$info->viewcreationcolmodelindexname.' '.$info->sortby.',';
					} else {
						$groupquery.=$info->viewcreationcolmodeljointable.'.'.$info->viewcreationcolmodelname.' '.$info->sortby.',';
					}
					$groupquerywotbl.=$info->viewcreationcolmodelname.' '.$info->sortby.',';	
					$orderbyquery.=$info->viewcreationcolmodelname.' '.$info->sortby.',';
				}
				$viecolids[]=$info->viewcreationcolumnid;
			}
			$groupquery=rtrim($groupquery, ",");//group by query with table name
			$groupquerywotbl=rtrim($groupquerywotbl, ",");//group by query WITHOUT table name
			$orderbyquery=rtrim($orderbyquery, ",");
			$final=$groupquery.' '.$orderbyquery;
		} else {
			$final = " group by 1 order by 1";
			$viecolids[]='';
			$groupby=array();
			$groupbyname[]='';
			$groupquery=" group by 1";
			$groupquerywotbl=" group by 1";
			$orderbyquery=" order by 1";
		}
		$info=array(
			'query'=>$final,
			'viewcolids'=>$viecolids,
			'groupbycolids'=>$groupby,
			'groupbyquery'=>$groupquery,
			'orderbyquery'=>$orderbyquery,
			'groupcol'=>$groupbyname,
			'groupbyquerywotbl'=>$groupquerywotbl,
		);
		return $info;
	}
	////this function retrieves the primary id of that selected values
	public function getdestinationrowid()
	{
	$tablename=$_GET['tablename'];
	$primarycolumn=$tablename.'id';
	$columnname=$_GET['columnname'];
	$primarid=$_GET['primarid'];
	$data=$this->db->select($columnname)
						->from($tablename)
						->where($primarycolumn,$primarid)
						->get()->result();
	foreach($data as $info)
	{
	$id=$info->$columnname;
	}
	echo json_encode(array('id'=>$id));
	}
	
	//summary group report colspan prerun $result[0]['colmodelname'][$i]
	public function groupsegregation($data,$groupcol)
	{		
		$i=0;
		$old = '';
		$k=0;$iterate=1;
		$groupcolcount=array();
		$numrows=$data->num_rows();
		foreach($data->result() as $row) 
		{
			$new=$row->$groupcol;			
			if($new != $old and $old != '')
			{
				$groupcolcount[$i-$k]=$k;
				$k=1;
				$groupcolcount[$i]=0;
			}
			elseif($iterate == $numrows)
			{				
				$groupcolcount[$i-($k)]=$k+1;
				if($new == $old)
				{
					$groupcolcount[$i]=0;
				}
				else
				{
					$groupcolcount[$i]=1;
				}				
			}			
			else
			{			
				$groupcolcount[$i]=0;
				$k++;
				if($iterate == $numrows)
				{
				echo'y'.$k;
				}
			}			
			//pending lastarray
			$old = $row->$groupcol;
			$i++;				
			$iterate++;				
		}
		return ($groupcolcount);
	}
	/**2nd grouping function works only if 3 level grouping is applied
	@data - query data
	@column - grouping column name
	@firstgroup - contains first grouping data-1D array
	*/
	public function secondgroupsegregation($data,$column,$firstgroup)
	{		
		#this foreach gathers second grouping column data in to array
		foreach($data->result() as $info)
		{
			if($info->$column == ''){
			$secondgroupdata[]="emptyemptyempty";	
			}
			else{
			$secondgroupdata[]=$info->$column;
			}
		}		
		$groupcolcount=array();
		#this foreach map the second array value to 1 on first array value
		foreach($firstgroup as $key=>$value)
		{
			if($value == 1)
			{
				$groupcolcount[$key] = 1;
			}
		}	
		for($a=0;$a<count($secondgroupdata);$a++)
		{		
			//iterates through group data
			if($firstgroup[$a] > 1)
			{
				$innercount=$firstgroup[$a];			
				$newarray=array();
				for($s=$a;$s < ($a+$innercount);$s++)
				{
					$newarray[$s]=$secondgroupdata[$s];
				}			
				//array-unique;
				$arrunique=array_unique($newarray);
				$arruniquekeys=array_flip($arrunique);
				//fills value 1 if all values are unique
				if(count($arrunique) == count ($newarray))
				{
					foreach($arrunique as $key=>$value)
					{
						$groupcolcount[$key]=1;
					}
				}
				//fills relavant data
				else
				{
					$arrcount= array_count_values($newarray);				
					foreach($newarray as $key=>$value)
					{						
						if($value == "emptyemptyempty"){
						$groupcolcount[$key]=1;
						}
						elseif(in_array($key,$arruniquekeys)){					
						$groupcolcount[$key]=$arrcount[$value];						
						}
						else{
						$groupcolcount[$key]=0;						
						}
					}				
				 } 		
			}			
		}		
		ksort($groupcolcount);	
		return $groupcolcount;
	}
	/**
	CHART DATA
	@dependency->chart (where,join is applied)
	**/
	public function chartreportsummary($creationid,$selectsummary) {	
		//needed report column data
		$data=$this->db->select('report.moduleid,modulemastertable,viewcreationcolumnid')
						->from('report')
						->join('module','module.moduleid=report.moduleid')
						->where('report.reportid',$creationid)
						->get();
		if($data->num_rows() > 0) {
			foreach($data->result() as $info) {
				$getreportinfo=array('moduleid'=>$info->moduleid,'modulemastertable'=>$info->modulemastertable,'viewcreationcolumnid'=>$info->viewcreationcolumnid);
			}
		} else {
			$getreportinfo=array('moduleid'=>1,'modulemastertable'=>1,'viewcreationcolumnid'=>1);
		}		
		$rowid = $getreportinfo['modulemastertable'].'id';
		$viewcolmoduleids = $getreportinfo['moduleid'];	
		$_GET['maintabinfo'] = $getreportinfo['modulemastertable'];
		$chartcolids = $getreportinfo['viewcreationcolumnid'];
		$wh = 1;
		$whereString = "";
		$groupid = "";
		//grid column title information fetch reportgridinformationfetchmodel
		$colinfo = $this->reportgridinformationfetchmodel($creationid);		
		//main table
		$maintable = $_GET['maintabinfo'];
		$selvalue = 1;
		$in = 0;
		//generate joins
		$counttable = 0;
		if(count($colinfo) > 0) {
			$counttable = count($colinfo['coltablename']);
		}
		$jtab = array($maintable);
		$ptab = array($maintable);
		$joinq = "";
		//attribute where condition
		$attrcond = "";
		$attrcondarr = array();
		$x = 0;
		$con = "";
		
		/* Generate Join & Group by & Order By condition */
		$chartcolids = explode(",",$chartcolids);
		$grouporder = $this->reportsortbygroupbyquery($creationid);
		$chartcolids = array_unique(array_merge($grouporder['viewcolids'],$chartcolids)); 
		/* Join Query Generation Start*/
		$join_stmt = $this->generatejoinquery($viewcolmoduleids,$chartcolids,$_GET['maintabinfo']);
		//attr condition generate
		for($xi=0; $xi < ( count($attrcondarr)-1 ); $xi++ ) {
			$attrcond .= " AND ".$attrcondarr[0].'='.$attrcondarr[$xi+1];
		}
		if( $attrcond == "" ) { $attrcond = "1=1";}
		
		//custom condition generate
		$cuscondition = " AND 1=1";
		//generate condition
		$w = '1=1';
		if($wh == "1" or $wh == '') {
			$wh = $w;
		}
		$conditionvalarray = $this->conditioninfofetch($creationid);
		$c = count($conditionvalarray);
		if($c>0) {
			$whereString=$this->viewwhereclausegeneration($conditionvalarray);
		} else {
			$whereString=$w;
			$wh = $w.' AND ';
		}
		if($maintable == 'employee') {
			$status = $maintable.'.status NOT IN (3)';
		} else {
			$status = $maintable.'.status not IN (0,3)';
		}
		//chart groupby element
		$groupby=$selectsummary['group'];
		$select=$selectsummary['select'];
		$data = $this->db->query('select '.$select.' from '.$maintable.' '.$join_stmt.' WHERE '.$attrcond.' AND '.$wh.' '.$whereString.' '.$cuscondition.' AND '.$status.' group by '.$groupby);
		$finalresult=$data;
		return $finalresult;
		//removed orderby
	}	
	public function tempgroupsegregation($data,$groupcol) {		
		$m=0;
		#this foreach gathers second grouping column data in to array
		$secondgroupdata = array();
		foreach($data->result() as $info)
		{
			$secondgroupdata[]=$info->$groupcol;
			$m++;
		}		
		$groupcolcount=array();			
		//array-unique;
		$arrunique=array_filter(array_unique($secondgroupdata));
		$arruniquekeys=array_flip($arrunique);
		//fills value 1 if all values are unique
		if(count($arrunique) == count ($secondgroupdata))
		{
			$groupcolcount = array_fill(0,(count ($secondgroupdata)), '1');			
		}
		//fills relavant data
		else
		{
			$arrcount= array_count_values(array_filter($secondgroupdata));			
			foreach($secondgroupdata as $key=>$value)
			{						
				if(in_array($key,$arruniquekeys)){					
					$groupcolcount[$key]=$arrcount[$value];						
				}
				else if($secondgroupdata[$key] == "" or $secondgroupdata[$key] == null){
					$groupcolcount[$key]=1;	
				}
				else{
					$groupcolcount[$key]=0;						
				}
			}						
		 }			
		ksort($groupcolcount);		
		return $groupcolcount;
	}
	//retrieve report information
	public function getreportinfo($creationid) {
		$data=$this->db->select('report.moduleid,modulemastertable,viewcreationcolumnid,reportdatecolumnid,reportdatemethod,reportmode,reportstartdate,reportenddate,relatedmoduleid',false)
						->from('report')
						->join('module','module.moduleid=report.moduleid')
						->where('report.reportid',$creationid)
						->limit(1,0)
						->get();
		if($data->num_rows() > 0) {
			foreach($data->result() as $info) {
				$related_mod ='';
				$tt_arr = array_filter(explode(',',$info->relatedmoduleid));
				$related_moduleid = implode(',',$tt_arr);
				if(count($tt_arr) > 0) {
					$related_mas = $this->db->select('GROUP_CONCAT(modulemastertable SEPARATOR ",") as relatedmodule',false)->from('module')->where_in('moduleid',$tt_arr)->get()->result();
					foreach($related_mas as $oom) {
						$related_mod = $oom->relatedmodule;
					}
				}
				$array=array('moduleid'=>$info->moduleid,'modulemastertable'=>$info->modulemastertable,'viewcreationcolumnid'=>$info->viewcreationcolumnid,'reportdatecolumnid'=>$info->reportdatecolumnid,'reportdatemethod'=>$info->reportdatemethod,'reportmode'=>$info->reportmode,'reportstartdate'=>$info->reportstartdate,'reportenddate'=>$info->reportenddate,'relatedmodule'=>$related_mod,'relatedmoduleid'=>$related_moduleid);
			}
		} else {
			$array=array('moduleid'=>1,'modulemastertable'=>1,'viewcreationcolumnid'=>1,'reportdatecolumnid'=>1,'reportdatemethod'=>'','reportmode'=>'','reportstartdate'=>'','reportenddate'=>'');
		}
		return $array;
	}
	//return the related join parameters
	public function getrelatedmodulejoin($viewcolmoduleids,$related_mod_id)
	{
		$related_mod_id = array_filter(explode(',',$related_mod_id));
		$data = $this->db->select('parenttable,parentfieldname,relatedtable,relatedfieldname,moduleid,relatedmoduleid')
						->from('modulerelation')
						->where_in('relatedmoduleid',$related_mod_id)
						->where('moduleid',$viewcolmoduleids)
						->get();
		foreach($data->result() as $info)
		{
			$relateddata[] = array(
									'parenttable'=>$info->parenttable,
									'parentfieldname'=>$info->parentfieldname,
									'relatedtable'=>$info->relatedtable,
									'relatedfieldname'=>$info->relatedfieldname,
									'moduleid'=>$info->moduleid,
									'relatedmoduleid'=>$info->relatedmoduleid
								);
		}
		return $relateddata;		
	}
	//3rd level join 
	public function getthirdleveljoin($fieldids,$related_mod_id,$maintable)
	{
		//viewcreationcolmodelname viewcreationcolmodeltable viewcreationjoincolmodelname		
		$fieldids = explode(',',$fieldids);
		$this->db->select("viewcreationcolumns.viewcreationcolmodeljointable,viewcreationcolumns.viewcreationjoincolmodelname,viewcreationcolumns.viewcreationcolmodeltable,		viewcreationcolumns.viewcreationparenttable,viewcreationcolumns.viewcreationcolmodelname,viewcreationcolumns.viewcreationcolmodelaliasname,viewcreationcolumns.viewcreationcolmodelindexname,viewcreationcolumns.viewcreationcolumnname",false);
		$this->db->from('viewcreationcolumns');
		$this->db->where('viewcreationcolumns.status',1);
		$this->db->where_in('viewcreationcolumns.viewcreationcolumnid',$fieldids);
		$this->db->where('viewcreationcolumns.viewcreationmoduleid',$related_mod_id);				
		$this->db->where('viewcreationcolumns.viewcreationcolmodeltable !=',$maintable);				
		$data = $this->db->get();
		if($data->num_rows() > 0)
		{
			foreach($data->result() as $info)
			{
				if($maintable != $info->viewcreationcolmodeltable)
				{
					$result[] = array(
										'tableone'=>$maintable,
										'tableonefield'=>$maintable.'id',
										'tabletwo'=>$info->viewcreationcolmodeltable,
										'tabletwofield'=>$info->viewcreationjoincolmodelname,
									);
				} 
			}
		}
		else
		{
			$result = array();
		}
		return $result;
	}
	//date format fetch based on user in report condition grid
	public function dateformatfetch($reportconditiongridid) {
		$data ='';
		$dateformat = $this->Basefunctions->phpmysqlappdateformatfetch();
		$mysqlformat = $dateformat['mysqlformat'];
		$this->db->select('SQL_CALC_FOUND_ROWS DATE_FORMAT(reportcondition.viewcreationconditionvalue,"'.$mysqlformat.'") AS conditionvalue',false);
		$this->db->from('reportcondition');
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row){
				$data = $row->conditionvalue;
			}
			return $data;
		} else {
			return $data;
		} 
	}
	//return the related join parameters
	public function generatejoinquery($mainmoduleid,$viewfieldids,$parenttable)
	{
		$joined_table= array();		
		$join_query_string='';
		$this->db->select("viewcreationcolumns.viewcreationmoduleid,viewcreationcolumns.viewcreationcolmodeltable",false);
		$this->db->from('viewcreationcolumns');
		$this->db->where('viewcreationcolumns.status',1);
		$this->db->where_in('viewcreationcolumns.viewcreationcolumnid',$viewfieldids);
		$this->db->group_by(array("viewcreationmoduleid", "viewcreationcolmodeltable"));
		$this->db->ORDER_BY("FIELD (viewcreationcolumns.viewcreationmoduleid,".$mainmoduleid.") DESC,viewcreationcolumns.viewcreationmoduleid ASC,viewcreationcolumns.viewcreationcolumnid ASC");		
		$data = $this->db->get();
		$result=$data->result();
		if($data->num_rows() > 0)
		{	
			foreach($data->result() as $info)
			{		
				$join_query= array();
				$join_a = '';
				$newparenttable = $info->viewcreationcolmodeltable;
				$newmoduleid = $info->viewcreationmoduleid;
				
				if($parenttable!=$newparenttable)
				{
					if(!in_array($newparenttable,$joined_table))
					{ 
						do
						{  $intbl=$newparenttable;
							$jinfo=$this->db->select('moduleid,parenttable,parentfieldname,relatedmoduleid,relatedtable,relatedfieldname,joinpriority')
								->from('modulerelation')
								->where_in('moduleid',array($mainmoduleid,$newmoduleid))
								->where('relatedtable',$newparenttable)
								->group_by("relatedtable")
								->ORDER_BY("FIELD (moduleid,".$mainmoduleid.")DESC,moduleid ASC")		
								->where('status',1)						
								->get();
							
							$joininfo=$jinfo->row();
							if($jinfo->num_rows() > 0)
							{	//Alias based split the related table 
								$relatedtblsting=explode(" as ",$joininfo->relatedtable);
								if(isset($relatedtblsting[1])){$relatedtblname=$relatedtblsting[1];}else{$relatedtblname=$relatedtblsting[0];}
								
								$join_query[]= ' LEFT OUTER JOIN '.$joininfo->relatedtable.' ON '.$joininfo->parenttable.'.'.$joininfo->parentfieldname.'='.$relatedtblname.'.'.$joininfo->relatedfieldname;
								
								$joined_table[]=$joininfo->relatedtable;	
								$newparenttable=$joininfo->parenttable;
								$newmoduleid=$joininfo->moduleid;
							}
							else
							{	
									break;
							}
							
							
						}while(($parenttable!=$newparenttable)&&(!in_array($newparenttable,$joined_table)));
						$join_a = implode(" ",array_reverse($join_query));
					}
				}
				$join_query_string = $join_query_string.' '.$join_a;

			}
			return $join_query_string;
        }
	}
	public function generateselectstmt($reportcolids) {	
		$softwareindustryid = $this->Basefunctions->industryid;
		if($softwareindustryid == 3) {
			$roundweight=$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); // weight round
			$amountround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
			$roundmelting = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',4); //melting round-off
			$rateround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); // rate round
		}
		$select=',';
		$selectwithouttbl=',';
		$fieldids = explode(',',$reportcolids);
		$data=$this->db->select('viewcreationcolumns.selectstatement,viewcreationcolumns.viewcreationcolmodelname,viewcreationcolmodeltable,uitypeid,viewcreationcolmodelaliasname as colmodelaliasname,viewcreationcolumns.viewcreationcolumnid,calcoperation,viewcreationcolmodeljointable as coljointablename,viewcreationcolumns.roundtype,roundtype.roundtypevalue,aggregatemethod.aggregatemethodkey')
			->from('viewcreationcolumns')
			->join('roundtype','roundtype.roundtypeid=viewcreationcolumns.roundtype','left outer' )
			->join('aggregatemethod','aggregatemethod.aggregatemethodid=viewcreationcolumns.calcoperation','left outer')
			->where_in('viewcreationcolumns.viewcreationcolumnid',$fieldids)
			->where('viewcreationcolumns.status',$this->Basefunctions->activestatus)
			->order_by('FIELD(viewcreationcolumns.viewcreationcolumnid,'.$reportcolids.')')
			->get();
			
		if($data->num_rows() > 0) {			
			$v = 0;
			foreach($data->result() as $info) {
				$viewcolids[$v] =  $info->viewcreationcolumnid;
				$v++;
				//Alias based split the related table 
				$colmodelaliasname=explode(" as ",$info->colmodelaliasname);
				if(isset($colmodelaliasname[1])) {
					$columnname = $colmodelaliasname[1];
				} else {
					$columnname = $colmodelaliasname[0];
				}
				if($info->aggregatemethodkey!= '' || $info->aggregatemethodkey != null) { //identity any precalc data
					if($info->aggregatemethodkey == 'COUNT' || ($info->aggregatemethodkey == 'MAX' || $info->aggregatemethodkey == 'MIN'))	{
						if($info->aggregatemethodkey == 'COUNT') {
							$select .= ' CONCAT('.$info->coljointablename.'.'.$info->colmodelaliasname.'," ",round('.$info->aggregatemethodkey.'('.$info->coljointablename.'.'.$info->colmodelaliasname.'),2),"  Nos.") as '.$info->colmodelaliasname.',';
							$selectwithouttbl .=' CONCAT('.$columnname.'," ",round('.$info->aggregatemethodkey.'('.$columnname.'),2),"  Nos.") as '.$columnname.',';
						}
						if($info->aggregatemethodkey == 'MAX' || $info->aggregatemethodkey == 'MIN') {
							$select .= ' '.$info->aggregatemethodkey.'('.$info->coljointablename.'.'.$info->colmodelaliasname.') as '.$info->colmodelaliasname.',';
							$selectwithouttbl .= ' '.$info->aggregatemethodkey.'('.$columnname.') as '.$columnname.',';
						}	
					}
					else {
						$select .= ' ROUND('.$info->aggregatemethodkey.'('.$info->coljointablename.'.'.$info->colmodelaliasname.'),'.$info->roundtypevalue.') as '.$info->colmodelaliasname.',';
						
						$selectwithouttbl .= ' ROUND('.$info->aggregatemethodkey.'('.$columnname.'),'.$info->roundtypevalue.') as '.$columnname.',';
					}				
				} else {
					if($info->uitypeid == 5) {//round the weight fields based on decimal licence
						if($softwareindustryid == 3) {
							if($info->roundtype == 'roundweight') {
								$select .= ', ROUND(('.$info->coljointablename.'.'.$info->colmodelaliasname.'),'.$roundweight.') as columdata';
							} else if($info->roundtype == 'roundmelting') {
								$select .= ', ROUND(('.$info->coljointablename.'.'.$info->colmodelaliasname.'),'.$roundmelting.') as columdata';
							} else if($info->roundtype == 'roundamount') {
								$select .= ', ROUND(('.$info->coljointablename.'.'.$info->colmodelaliasname.'),'.$amountround.') as columdata';
							} else if($info->roundtype == 'noround' || $info->roundtype == '') {
								$select .= ', ROUND(('.$info->coljointablename.'.'.$info->colmodelaliasname.'),'.'0'.') as columdata';
							} else if($info->roundtype == 'rateround') {
								$select .= ', ROUND(('.$info->coljointablename.'.'.$info->colmodelaliasname.'),'.$rateround.') as columdata';
							}
						} else {
							$select .= $info->coljointablename .'.'.$info->colmodelaliasname.',';
							$selectwithouttbl .= $columnname.',';
						}
					} else if($info->uitypeid == 8) {
						$format = $this->Basefunctions->phpmysqlappdateformatfetch();
						$select .= 'DATE_FORMAT('.$info->coljointablename .'.'.$info->colmodelaliasname.' ,"'.$format['mysqlformat'].'") as '.$info->colmodelaliasname.'';
					} else {
						$select .= $info->coljointablename .'.'.$info->colmodelaliasname.',';
						$selectwithouttbl .= $columnname.',';
					}
				}
			}
			$select = rtrim($select, ",");		
			$select = ltrim($select, ",");
			$selectwithouttbl = rtrim($selectwithouttbl, ",");		
			$selectwithouttbl = ltrim($selectwithouttbl, ",");
		}
		$info=array(
			'Selectstmt'=>$select,
			'Selectstmtwithouttbl'=>$selectwithouttbl
		);
	return $info;			
}
	//condition information fetch	
    public function generateconditioninfo($viewid) 
	{
		$i=0;
		$multiarray = array();
		$viewcolids = [];
		//user for get the condition values;
		$this->db->select('viewcreationcolumns.viewcreationcolumnid,viewcreationcolumns.viewcreationcolmodeljointable,viewcreationcolumns.viewcreationcolmodelname,viewcreationcolumns.viewcreationcolmodelindexname,reportcondition.viewcreationconditionname,reportcondition.viewcreationconditionvalue,reportcondition.reportid,reportcondition.viewcreationandorvalue,viewcreationjoincolmodelname,uitypeid',false);
		$this->db->from('reportcondition');
		$this->db->join('viewcreationcolumns','viewcreationcolumns.viewcreationcolumnid=reportcondition.viewcreationcolumnid');
		$this->db->where_in('reportcondition.reportid',$viewid);
		$this->db->where_in('reportcondition.status',$this->Basefunctions->activestatus);
		$condtionval=$this->db->get();
		foreach($condtionval->result() as $row) {
			$multiarray[$i] = array(
										'0'=>$row->viewcreationcolmodeljointable,
										'1'=>$row->viewcreationcolmodelindexname,
										'2'=>$row->viewcreationconditionname,
										'3'=>$row->viewcreationconditionvalue,
										'4'=>$row->viewcreationandorvalue,
										'5'=>$row->viewcreationjoincolmodelname,
										'6'=>$row->uitypeid
									);
			$viewcolids[$i] =$row->viewcreationcolumnid;
			$i++;
		}
		$info=array(
				'conditionalarry'=>$multiarray,
				'viewcolid'=>$viewcolids
		);
		return $info;
    }
	//where condition generation-new
    public function generatewhereclause($conditionvalarray) 
	{
		$whereString="";
		$havingString=""; 
		foreach ($conditionvalarray as $key) 
		{
		   if($key[4] == '' or $key[4] == null){
				$key[4]='AND';
			}
			$dduitypes = array(17,18,19,20,25,26);
			if(in_array($key[6],$dduitypes)){
				$field = $key[5];
			} else {
				$field = $key[1];
			}		
				switch($key[2]) {
					case "Equalto": 
						$whereString .= $key[0].'.'.$field. " = '".$key[3]."'"." ". $key[4]." ";
						break;
					case "NotEqual": 
						$whereString .= $key[0].'.'.$field. " != '" .$key[3]."'". " ". $key[4]." ";
						break;
					case "Like":
					//	$whereString .= $key[0].'.'.$key[1]. " LIKE '".$key[3]"'"." ".  $key[4]." ";
					//	break;
					case "Startwith":
						$whereString .= $key[0].'.'.$field. " LIKE '".$key[3].'%'."'"." ".  $key[4]." ";
						break;
					case "Endwith": 
						$whereString .= $key[0].'.'.$field. " LIKE '".'%'.$key[3]."'". " ". $key[4]." ";
						break;
					case "Contains": 
						$whereString .= $key[0].'.'.$field. " LIKE '".'%'.$key[3].'%'."'". " ". $key[4]." ";
						break;
					case "IN": //specials
						$whereString .= $key[0].'.'.$field. " IN (".$key[3].")". " ". $key[4]." ";
						break;
					case "NOT IN": 
						$whereString .= $key[0].'.'.$field. " NOT IN (".$key[3].")". " ". $key[4]." ";
						break;
					case "Greater than": 
						$whereString .= $key[0].'.'.$field. " > '" .$key[3]."'"." ".  $key[4]." ";
						break;
					case "Less than": 
						$whereString .= $key[0].'.'.$field. " < '" .$key[3]."'". " ". $key[4]." ";
						break;
					case "Greater than equal": 
						$whereString .= $key[0].'.'.$field." >= '" .$key[3]."'"." ".  $key[4]." ";
						break;
					case "Less than equal": 
						$whereString .= $key[0].'.'.$field. " <= '" .$key[3]."'". " ". $key[4]." ";
						break;
					case "IS NULL": 
						$whereString .= $key[0].'.'.$field. " IS NULL '" .$key[3]."'". " ". $key[4]." ";
						break;
					case "NOT LIKE": 
						$whereString .= $key[0].'.'.$field. " NOT LIKE '" .$key[3]."'". " ". $key[4]." ";
						break;
					case "IS NOT NULL": 
						$whereString .= $key[0].'.'.$field. " IS NOT NULL '" .$key[3]."'". " ". $key[4]." ";
						break;
					default:
						$whereString .= "";
						break;
				}
		  }
		$whereString= substr($whereString, 0, -4);
		return $whereString;
    }
	
  public function generatehavingclause($conditionvalarray) 
  {	
		$havingString="";
		foreach ($conditionvalarray as $key) 
		{ 
		 if($key[6]==4 || $key[6]==5)
			{
				if($key[4] == '' or $key[4] == null){
					$key[4]='AND';
				}
				$dduitypes = array(17,18,19,20,25,26);
				if(in_array($key[6],$dduitypes)){
					$field = $key[5];
				} else {
					$field = $key[1];
				}
			
				switch($key[2])
				{
					case "Equalto": 
						$havingString .= $field. " = '".$key[3]."'"." ". $key[4]." ";
						break;
					case "NotEqual": 
						$havingString .= $field. " != '" .$key[3]."'". " ". $key[4]." ";
						break;
					case "Like":
					//	$havingString .= $key[0].'.'.$key[1]. " LIKE '".$key[3]"'"." ".  $key[4]." ";
					//	break;
					case "Startwith":
						$havingString .= $field. " LIKE '".$key[3].'%'."'"." ".  $key[4]." ";
						break;
					case "Endwith": 
						$havingString .= $field. " LIKE '".'%'.$key[3]."'". " ". $key[4]." ";
						break;
					case "Contains": 
						$havingString .= $field. " LIKE '".'%'.$key[3].'%'."'". " ". $key[4]." ";
						break;
					case "IN": //specials
						$havingString .= $field. " IN (".$key[3].")". " ". $key[4]." ";
						break;
					case "NOT IN": 
						$havingString .= $field. " NOT IN (".$key[3].")". " ". $key[4]." ";
						break;
					case "Greater than": 
						$havingString .= $field. " > '" .$key[3]."'"." ".  $key[4]." ";
						break;
					case "Less than": 
						$havingString .= $field. " < '" .$key[3]."'". " ". $key[4]." ";
						break;
					case "Greater than equal": 
						$havingString .= $field." >= '" .$key[3]."'"." ".  $key[4]." ";
						break;
					case "Less than equal": 
						$havingString .= $field. " <= '" .$key[3]."'". " ". $key[4]." ";
						break;
					case "IS NULL": 
						$havingString .= $field. " IS NULL '" .$key[3]."'". " ". $key[4]." ";
						break;
					case "NOT LIKE": 
						$havingString .= $field. " NOT LIKE '" .$key[3]."'". " ". $key[4]." ";
						break;
					case "IS NOT NULL": 
						$havingString .= $field. " IS NOT NULL '" .$key[3]."'". " ". $key[4]." ";
						break;
					default:
						$havingString .= "";
						break;
				}
		    }
		}
		$havingString= substr($havingString, 0, -4);
		return $havingString;
    }
	
	 public function grpreportgridinformationfetchmodel($creationid,$groupids,$viewids)
	 {
		//fetch show col ids
		$this->db->select('report.viewcreationcolumnid,report.reportid,report.reportname',false);
		$this->db->from('report');
		$this->db->where_in('report.reportid',$creationid);
		$result=$this->db->get()->result();
		foreach($result as $row) {
			$colids=$row->viewcreationcolumnid;
			$reportname=$row->reportname;
		}
		$viewcolids = explode(',',$colids);
		$data = array();
		$data['reportname']=$reportname;
		$i=0;
		$colids= array_merge($groupids,$viewids);
		//get groupby condition
		$summarygroup=$this->Reportsmodel->summaryreportgroup($creationid);
		foreach($colids as $viewcolid) {
			//for fetch colname & colmodel fetch
			$this->db->select("viewcreationcolumns.viewcreationmoduleid,viewcreationcolumns.viewcreationcolumnname,
			viewcreationcolumns.viewcreationcolmodelname,
			viewcreationcolumns.viewcreationcolmodelindexname,
			viewcreationcolumns.viewcreationcolmodeljointable,
			viewcreationcolumns.viewcreationcolumnid,
			viewcreationcolumns.viewcreationcolmodelaliasname,
			viewcreationcolumns.viewcreationcolmodeltable,
			viewcreationcolumns.viewcreationparenttable,
			viewcreationcolumns.viewcreationmoduleid,
			viewcreationcolumns.viewcreationjoincolmodelname,
			viewcreationcolumns.viewcreationcolumnviewtype,
			viewcreationcolumns.viewcreationtype,
			viewcreationcolumns.uitypeid,
			viewcreationcolumns.viewcreationcolmodelcondname,viewcreationcolumns.viewcreationcolmodelcondvalue,linkable,linkmodule.modulelink,viewcreationcolumns.linkmoduleid");
			$this->db->from('viewcreationcolumns');
			$this->db->join('module as linkmodule','linkmodule.moduleid=viewcreationcolumns.linkmoduleid');
			$this->db->where_in('viewcreationcolumns.viewcreationcolumnid',$viewcolid);
			$this->db->where('viewcreationcolumns.status',$this->Basefunctions->activestatus);
			$showfield = $this->db->get();		
			
			if($showfield->num_rows() >0) {
				//grouping
				if(count($summarygroup) > 0)
				{
					for($m=0;$m<count($summarygroup);$m++)
					{
						$data['groupcolumnname'][$m]=$summarygroup[$m];
					}					
				}
				else{
				$data['groupcolumnname']=0;
				}
				//get summary field related data
				//grid data viewcreationcolmodelindexname
				foreach($showfield->result() as $show) 
				{
					$data['colname'][$i]=$show->viewcreationcolumnname;
					$data['moduleid'][$i]=$show->viewcreationmoduleid;
					$data['colmodelname'][$i]=$show->viewcreationcolmodelname;
					$data['colmodelaliasname'][$i]=$show->viewcreationcolmodelaliasname;
					$data['coltablename'][$i]=$show->viewcreationcolmodeltable;
					$data['coljointablename'][$i]=$show->viewcreationcolmodeljointable;
					$data['coljoinmodelname'][$i]=$show->viewcreationjoincolmodelname;
					$data['columndbname'][$i]=$show->viewcreationcolmodelindexname;
					$data['colmodelindex'][$i]=$show->viewcreationcolmodeljointable .'.'.$show->viewcreationcolmodelindexname;
					$data['colmodelparenttable'][$i]=$show->viewcreationparenttable;
					$data['colmodelviewtype'][$i]=$show->viewcreationcolumnviewtype;
					$data['colmodeldatatype'][$i]=$show->viewcreationtype;
					$data['colmodelcondname'][$i]=$show->viewcreationcolmodelcondname;
					$data['colmodelcondvalue'][$i]=$show->viewcreationcolmodelcondvalue;
					$data['colmodelmoduleid'][$i]=$show->viewcreationmoduleid;
					$data['colmodeluitype'][$i]=$show->uitypeid;
					$data['colmodeltype'][$i]='text';
					$data['linkable'][$i]=$show->linkable;
					$data['linkpath'][$i]=$show->modulelink;
					$data['linkmoduleid'][$i]=$show->linkmoduleid;
					$i++;
				}
			}
		}
		return $data;
    }	
	/* Grid data view generate for base modules */
	function viewgriddatagenerate($tabledata,$primarytable,$primaryid,$width,$height,$checkbox = 'false') {
		$CI =& get_instance();
		//width set
		$columnswidth = 1200;
		$addcolsize = 0;
		$reccount = count($tabledata[0]['colmodelname']);
		$columnswidth = array_sum($tabledata[0]['colsize'])+5;
		$extrasize = ($columnswidth>$width)?0 : ($width-$columnswidth);
		$addcolsize = $extrasize!=0 ? round($extrasize/$reccount) : 0;
		$columnswidth = ($columnswidth>$width)?$columnswidth : $width;
		//height set
		if($height<=420) {
			$datarowheight = $height-35;
		} else {
			if(empty($tabledata[3]['datainfo'])) {
				$datarowheight = $height-133;
			} else {
				$datarowheight = $height-133;
			}
		}
		//header generation
		if($checkbox == 'true') {
			$columnswidth = $columnswidth+35;
		}
		$mdivopen ='<div class="grid-view">';
		$header = '<div class="header-caption" style="width:'.$columnswidth.'px;"><ul class="inline-list">';
		$i=0;
		$modname='';
		$m=0;
		foreach ($tabledata[0]['colname'] as $headname) {
			$modname = strtolower(preg_replace('/[^A-Za-z0-9]/', '', $tabledata[0]['modname'][$i]));
			if($checkbox == 'true' && $m==0) {
				$header .='<li id="headcheckbox'.$m.'" data-uitype="13" data-fieldname="gridcheckboxfield" data-width="35" data-class="gridcheckboxclass" data-viewtype="1" style="width:35px;"><input type="checkbox" id="'.$modname.'_headchkbox" name="'.$modname.'_headchkbox" class="'.$modname.'_headchkboxclass headercheckbox" /></li>';
				$m=1;
			}
			$header .='<li id="headcol'.$i.'" class="headerresizeclass '.$modname.'headercolsort" data-class="'.$tabledata[0]['colmodelname'][$i].'-class" data-sortorder="ASC" data-sortcolname="'.$tabledata[0]['colmodelindex'][$i].'" style="width:'.($tabledata[0]['colsize'][$i]+$addcolsize).'px;" data-width="'.($tabledata[0]['colsize'][$i]+$addcolsize).'" data-viewcolumnid="'.$tabledata[0]['colid'][$i].'" data-viewid="'.$tabledata[6].'" data-position="'.$i.'">'.$headname.'<i class="icon-sort icon-sortsort"></i></li>';
			$i++;
		}
		$header .='</ul></div>';
		$content = '<div class="gridcontent" style="height:'.$datarowheight.'px; overflow:auto;"><div class="data-content wrappercontent" style="width:'.$columnswidth.'px;">';
		$data = $tabledata[1]->result();
		if( empty($data) ) {
			$content .= '<span class="nocontentspan">No Record(s) Available</span>';
		}
		//content generation
		$count = 0;
		if(count($tabledata[0]) > 0) {
			$count = count($tabledata[0]['colmodelname']);
		}
		foreach($tabledata[1]->result() as $row) {
			$content .= '<div class="data-rows" id="'.$row->$primaryid.'">
							<ul class="inline-list">';
			$rowid = $row->$primaryid;
			for($i=0;$i<$count;$i++) {
				if($checkbox == 'true' && $i==0) {
					$content .='<li style="width:35px;"><input type="checkbox" id="'.$modname.'_rowchkbox'.$rowid.'" name="'.$modname.'_rowchkbox'.$rowid.'" data-rowid="'.$rowid.'" class="'.$modname.'_rowchkboxclass rowcheckbox" value="'.$rowid.'" /></li>';
				}
				if($tabledata[0]['colmodeluitype'][$i] == 8) {
					$val = $CI->Basefunctions->userdateformatconvert($row->$tabledata[0]['colmodelname'][$i]);
				} else if($tabledata[0]['colmodeluitype'][$i] == 31) {
					$val = $CI->Basefunctions->userdatetimeformatconversion($row->$tabledata[0]['colmodelname'][$i]);
				} else if($tabledata[0]['colmodeluitype'][$i] == 20) {
					$val = $row->$tabledata[0]['colmodelname'][$i].$row->rolename.$row->empgrpname;
				} else {
					if($tabledata[0]['colmodelname'][$i]=='commonid') {
						$value = $row->$tabledata[0]['colmodelname'][$i];
						$table = $tabledata[0]['coltablename'][$i];
						$val = $CI->Basefunctions->getrelatedmodulefieldvalues($value,$primarytable,$table,$row->$primaryid);
						$val = (($val!='')?$val:'');
					} else {
						$val = $row->$tabledata[0]['colmodelname'][$i];
					}
				}
				$align = is_float($val)? 'textcenter' : 'textleft';
				$content .= '<li class="'.$align.' '.$tabledata[0]['colmodelname'][$i].'-class" style="width:'.($tabledata[0]['colsize'][$i]+$addcolsize).'px;">'.$val.'</li>';
			}
			$content .= '</ul></div>';
		}
		$content .='</div>';
		$mdivclose = '</div>';
		$datas = $mdivopen.$header.$content.$mdivclose;
		$footer="";
		
		return array('content'=>$datas,'footer'=>$footer);
	}
	//set default update
	public function setdefaultupdate($setdefault,$reportfolderid) {
		if($setdefault != 'No') {
			$updatearray = array('setdefault'=>'No');
			$this->db->where_not_in('reportfolder.reportfolderid',$reportfolderid);
			$this->db->update('reportfolder',$updatearray);
		}
	}
	public function deletefolderddvalfetch()
	{	$reportfolderid = $_GET['reportfolderid'];
		$userid = $this->Basefunctions->logemployeeid;
		$this->db->select('reportfolderid,reportfoldername,description,setaspublic,setdefault');
		$this->db->from('reportfolder');	
		$this->db->where_not_in('reportfolder.reportfolderid',$reportfolderid);
		$this->db->where('reportfolder.status',$this->Basefunctions->activestatus);
		$this->db->order_by('sortorder','asc');
		$data=$this->db->get();
		if($data->num_rows() > 0){
			foreach($data->result() as $info){
				$jsondata[]=array('result'=>true,'reportfoldername'=>$info->reportfoldername,'reportfolderid'=>$info->reportfolderid);
			}
		}
		else{
			$jsondata=array();
		}
		echo json_encode($jsondata);
	}
	public function checkfolderhavereports()
	{
		$reportfolderid = $_GET['reportfolderid'];
		$this->db->select('COUNT(reportfolderid) AS reportcount');
		$this->db->from('report');
		$this->db->where_in('report.reportfolderid',$reportfolderid);
		$this->db->where('report.status',$this->Basefunctions->activestatus);
		$data=$this->db->get()->row();
		echo $data->reportcount;
	}
	//delete report folder
	public function deletemptyreportfolder(){
		$reportfolderid=$_GET['reportfolderid'];
		$updatearray=array('lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid,'status'=>$this->Basefunctions->deletestatus);
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$repfoldername = $this->Basefunctions->singlefieldfetch('reportfoldername','reportfolderid','reportfolder',$reportfolderid);
		$activity = ''.$user.' deleted Report Folder - '.$repfoldername.'';
		$this->Basefunctions->notificationcontentadd($reportfolderid,'Deleted',$activity ,$userid,35);
		//audit-log
		$this->db->where('reportfolderid',$reportfolderid);
		$this->db->update('reportfolder',$updatearray);
		$this->db->where('reportfolderid',$reportfolderid);
		echo true;
	}
	//retrieve report information
	public function getreportmoduleinfo($moduleid) {
		$data=$this->db->select('module.moduleid,module.modulemastertable',false)
						->from('module')
						->where('module.moduleid',$moduleid)
						->limit(1,0)
						->get();
		if($data->num_rows() > 0) {
			foreach($data->result() as $info) {
				$array=array('moduleid'=>$info->moduleid,'modulemastertable'=>$info->modulemastertable);
			}
		} else {
			$array=array('moduleid'=>1,'modulemastertable'=>1);
		}
		return $array;
	}
/* New Report concept start*/
public function savereporttodb()
{
	$id = $_GET['hdnreportid'];
	$sndefault = $this->Basefunctions->singlefieldfetch('sndefault','reportid','report',$id); //Check default value
	if($sndefault == 2) {
		echo 'sndefault';
	} else {
		$datearray=array();
		if(!isset($_GET['schedulereport']))
		{
			$_GET['schedulereport']='No';
		}
		if($_GET['reportrelatedmodules'] == null || $_GET['reportrelatedmodules'] == 'null' || $_GET['reportrelatedmodules'] == '')
		{
			$reportrelatedmodules = '';
		}
		else
		{			
			$reportrelatedmodules = trim($_GET['reportrelatedmodules']);
		}	
		if(isset($_GET['date_columnid']) and isset($_GET['date_method']))
		{
			if($_GET['date_columnid'] < 1){
			$_GET['date_columnid']=1;
			}
			if($_GET['date_method'] == 'custom')
			{
				if(isset($_GET['date_start']) and isset($_GET['date_end']))
				{
						$_GET['date_start']=date("Y-m-d", strtotime($_GET['date_start']));
						$_GET['date_end']=date("Y-m-d", strtotime($_GET['date_end']));
						$datearray=array('reportdatecolumnid'=>$_GET['date_columnid'],'reportdatemethod'=>$_GET['date_method'],'reportmode'=>$_GET['date_mode'],'reportstartdate'=>$_GET['date_start'],'reportenddate'=>$_GET['date_end']);
				}
			}
			else
			{
				$datearray=array('reportdatecolumnid'=>$_GET['date_columnid'],'reportdatemethod'=>$_GET['date_method'],'reportmode'=>$_GET['date_mode'],'reportstartdate'=>' ','reportenddate'=>' ');
			}
		}else
		{
				$datearray=array('reportdatecolumnid'=>1,'reportdatemethod'=>0,'reportmode'=>0,'reportstartdate'=>' ','reportenddate'=>' ');
		}
		$hdnreportid=$_GET['hdnreportid'];
		$moduletabsectionid=isset($_GET['reportmoduletab']) ? $_GET['reportmoduletab'] : '1';
		if($moduletabsectionid != '' || $moduletabsectionid != 0 ) {
			$moduletabsectionid = $moduletabsectionid;
		} else {
			$moduletabsectionid = 1;
		}
		if(trim($_GET['groupcolids']) != '') {
			$groupbyviewcreationcolumnid = trim($_GET['groupcolids']);
		} else {
			$groupbyviewcreationcolumnid = "NULL";
		}
		if(trim($_GET['sortby']) != '') {
			$groupbysortbyvalue = trim($_GET['sortby']);
		} else {
			$groupbysortbyvalue = "NULL";
		}
		if(trim($_GET['tabularviewcolids']) != '') {
			$tabularviewcreationcolumnid = trim($_GET['tabularviewcolids']);
		} else {
			$tabularviewcreationcolumnid = "NULL";
		}
		if(trim($_GET['summarviewcolids']) != '') {
			$summaryviewcreationcolumnid = trim($_GET['summarviewcolids']);
		} else {
			$summaryviewcreationcolumnid = "NULL";
		}
		if(trim($_GET['summaramids']) != '') {
			$summaryaggregatemethodid = trim($_GET['summaramids']);
		} else {
			$summaryaggregatemethodid = "NULL";
		}
		if(trim($_GET['summaramname']) != '') {
			$summaryaggregatemethodname = trim($_GET['summaramname']);
		} else {
			$summaryaggregatemethodname = "NULL";
		}
		if(isset($_GET['htmlaccountid'])) {
			$_GET['htmlaccountid'] = $_GET['htmlaccountid'];
		} else {
			$_GET['htmlaccountid'] = 1;
		}
		/* $aggridarray = explode(',', $_GET['summaramids']); //Explode on comma
		$aggridarray = array_filter($aggridarray, function($var){
		return (trim($var) !== ''); //Return true if not empty string
		});
		$aggrnamearray = explode(',', $_GET['summaramname']); //Explode on comma
		$aggrnamearray = array_filter($aggrnamearray, function($var){
		return (trim($var) !== ''); //Return true if not empty string
		});
		 $_GET['summaramids'] = $aggridarray; */
		//report basicdetail entry		
		$basicdetail=array(
				'reporttypeid'=>trim($_GET['reporttypeid']),
				'reportfolderid'=>trim($_GET['reportfolderid']),
				'reportname'=>trim($_GET['reportname']),
				'reportdescription'=>trim($_GET['reportdescription']),
				'schedulereports'=>trim($_GET['schedulereport']),
				'moduleid'=>trim($_GET['reportmodule']),
				'relatedmoduleid'=>$reportrelatedmodules,
				'moduletabsectionid'=>$moduletabsectionid,
				'viewcreationcolumnid'=>trim($_GET['viewcolids']),				
				'viewcreationcolumnid'=>trim($_GET['viewcolids']),
				'tabularviewcreationcolumnid'=>$tabularviewcreationcolumnid,
				'summaryviewcreationcolumnid'=>$summaryviewcreationcolumnid,
				'summaryaggregatemethodid'=>$summaryaggregatemethodid,	
				'summaryaggregatemethodname'=>$summaryaggregatemethodname,					
				'groupbyviewcreationcolumnid'=>$groupbyviewcreationcolumnid,
				'sortby'=>$groupbysortbyvalue,
				'groupbycolrange'=>trim($_GET['groupbyrange']),
				'transfer'=>trim($_GET['transferyesno']),
				'reportingmode'=>trim($_GET['reportingmode']),				
				'summaryrollup'=>trim($_GET['summaryrollup']),
				'printtemplateid'=>trim($_GET['printtemplateid']),
				'htmlfieldmodeid'=>trim($_GET['htmlfieldmodeid']),
				'htmlfieldcondition'=>trim($_GET['htmlfieldcondition']),
				'htmlreportfromdate'=>$this->Basefunctions->ymd_format($_GET['htmlreportfromdate']),
				'htmlreporttodate'=>$this->Basefunctions->ymd_format($_GET['htmlreporttodate']),
				'htmlaccountid'=>trim($_GET['htmlaccountid']),
				'industryid'=>$this->Basefunctions->industryid,
				'branchid'=>$this->Basefunctions->branchid,				
				'createdate'=>date($this->Basefunctions->datef),			
				'lastupdatedate'=>date($this->Basefunctions->datef),			
				'createuserid'=>$this->Basefunctions->userid,				
				'lastupdateuserid'=>$this->Basefunctions->userid,				
				'status'=>$this->Basefunctions->activestatus			
		);
		$merged_array = array_filter(array_merge($basicdetail,$datearray));
		if($hdnreportid!='') {
			$this->db->where('reportid',$hdnreportid);
			$this->db->update('report',$merged_array);
			$reportid=$hdnreportid;	
		} else {
			$this->db->insert('report',$merged_array); 
			$reportid=$this->db->insert_id();			
		}
		//report condition entry
		$conditioncount=$_GET['reportconditioncount'];
		$conditiongriddata=$_GET['reportconditiondata'];
		$conditiondata=json_decode($conditiongriddata,true);
		if($hdnreportid!='')
		{
			$delecondids =trim($_GET['reportcreateconditionids']);
			$deletecondid = explode(',',$delecondids);
			//deleted report condition records
			$updatedcondata=array('lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid,'status'=>$this->Basefunctions->deletestatus);
			foreach($deletecondid as $conrowid) {		
				$this->db->where('reportid',$hdnreportid);
				$this->db->update('reportcondition',$updatedcondata);
			}
		}
		for($i=0;$i<$conditioncount;$i++ ) 
		{
			$fileduitype = $this->Basefunctions->generalinformaion('viewcreationcolumns ','uitypeid','viewcreationcolumnid',$conditiondata[$i]['reportcondcolumnid']);
			if($fileduitype == 8 ) {
				if($conditiondata[$i]['reportcondition'] == 'Custom') {
					$conditionvalue = $conditiondata[$i]['finalreportcondvalueid'];
				} else {
					$conditionvalue = $this->Basefunctions->ymddateconversion($conditiondata[$i]['finalreportcondvalueid']);
				}
			} else {
				$conditionvalue = $conditiondata[$i]['finalreportcondvalueid'];	
			}
			if($fileduitype == 8 ) {
				if($conditiondata[$i]['reportcondition'] == 'Custom') {
					$conditionvaluename = $conditiondata[$i]['finalreportcondvalue'];
				} else {
					$conditionvaluename = $this->Basefunctions->ymddateconversion($conditiondata[$i]['finalreportcondvalue']);
				}
			} else {
				$conditionvaluename = $conditiondata[$i]['finalreportcondvalue'];	
			}
			
			$condcolumn=array(
						'reportid'=>$reportid,
						'viewcreationcolumnid'=>$conditiondata[$i]['reportcondcolumnid'],
						'viewcreationconditionname'=>$conditiondata[$i]['reportcondition'],
						'viewcreationaggregatename'=>$conditiondata[$i]['reportaggregate'],
						'viewcreationconditionvalue'=>$conditionvalue,
						'viewcreationconditionvaluename'=>$conditionvaluename,
						'viewcreationandorvalue'=>$conditiondata[$i]['reportandorcond'],
						'createdate'=>date($this->Basefunctions->datef),			
						'lastupdatedate'=>date($this->Basefunctions->datef),			
						'createuserid'=>$this->Basefunctions->userid,				
						'lastupdateuserid'=>$this->Basefunctions->userid,				
						'status'=>$this->Basefunctions->activestatus
						);
			//$datacon=array_filter($condcolumn);	
			$this->db->insert('reportcondition',$condcolumn);
		}
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' created Report - '.$_GET['reportname'].'';
		$this->Basefunctions->notificationcontentadd($hdnreportid,'Added',$activity ,$userid,35);
		echo $reportid;
	}
}
//retrieve => Report Data
	public function newretrievereport()
	{
		$reportid=trim($_GET['dataprimaryid']);
		$dateformat = $this->Basefunctions->phpmysqlappdateformatfetch();
		$mysqlformat = $dateformat['mysqlformat'];
		$basicdata=$this->db->select('report.reporttypeid,report.reportfolderid,reportname,reportdescription,schedulereports,report.moduleid,relatedmoduleid,moduletabsectionid,recurrencefreqid,ondays,time,filetypeid,sendmailto,recipients,viewcreationcolumnid,reportdatecolumnid,reportdatemethod,DATE_FORMAT(reportstartdate,"'.$mysqlformat.'") AS startdate,DATE_FORMAT(reportenddate,"'.$mysqlformat.'") AS enddate,module.parentmoduleid as parentid,	tabularviewcreationcolumnid,IFNULL(summaryviewcreationcolumnid,0)AS summaryviewcreationcolumnid,IFNULL(summaryaggregatemethodid,0) AS summaryaggregatemethodid,IFNULL(summaryaggregatemethodname,0) AS summaryaggregatemethodname,IFNULL(groupbyviewcreationcolumnid,0) AS groupbyviewcreationcolumnid,IFNULL(sortby,0) AS sortby,IFNULL(groupbycolrange,0) AS groupbycolrange,report.transfer,reportingmode,summaryrollup,printtemplateid,htmlfieldmodeid,htmlfieldcondition,htmlreportfromdate,htmlreporttodate,htmlaccountid')
							->from('report')
							->join('reporttype','reporttype.reporttypeid=report.reporttypeid')
							->join('reportfolder','reportfolder.reportfolderid=report.reportfolderid')
							->join('module','module.moduleid=report.moduleid')
							->where('report.reportid',$reportid)
							->limit(1)
							->get()
							->result();							
		foreach($basicdata as $info) {
			if($info->startdate != null and $info->enddate != null) {
				$sdate = $info->startdate;
				$edate = $info->enddate;
			} else {
				$sdate='';
				$edate='';
			}
			$jsonbasic=array('reporttypeid'=>$info->reporttypeid,'reportfolderid'=>$info->reportfolderid,'reportname'=>$info->reportname,'reportdescription'=>$info->reportdescription,'schedulereport'=>$info->schedulereports,'reportmodule'=>$info->moduleid,'reportrelatedmoduleid'=>$info->relatedmoduleid,'reportfrequency'=>$info->recurrencefreqid,'ondays'=>$info->ondays,'recurrencytime'=>'','fileformat'=>$info->filetypeid,'recurrenceemail'=>$info->sendmailto,'recurrencerecipient'=>$info->recipients,'reportmaincolumnid'=>$info->viewcreationcolumnid,'reportdatecolumnid'=>$info->reportdatecolumnid,'reportdatemethod'=>trim($info->reportdatemethod),'reportstartdate'=>$sdate,'reportenddate'=>$edate,'reportcategory'=>$info->parentid,'tabularcolids'=>$info->tabularviewcreationcolumnid,'summarycolids'=>$info->summaryviewcreationcolumnid,'aggregatemethodids'=>$info->summaryaggregatemethodid,'aggregatemethodname'=>$info->summaryaggregatemethodname,'groupbycolids'=>$info->groupbyviewcreationcolumnid,'sortby'=>$info->sortby,'groupbycolrange'=>$info->groupbycolrange,'moduletabsectionid'=>$info->moduletabsectionid,'transfer'=>$info->transfer,'summaryrollup'=>$info->summaryrollup,'reportingmode'=>$info->reportingmode,'printtemplateid'=>$info->printtemplateid,'htmlfieldmodeid'=>$info->htmlfieldmodeid,'htmlfieldcondition'=>$info->htmlfieldcondition,'htmlreportfromdate'=>$info->htmlreportfromdate,'htmlreporttodate'=>$info->htmlreporttodate,'htmlaccountid'=>$info->htmlaccountid);
		}
		echo json_encode(array('basic'=>$jsonbasic));		
	}
	//export data
	public function newexceldataexportmodel() {
		$fieldhideshowids = $this->Basefunctions->get_company_settings('reportscolumnfieldhideshow');
		$salessalesdetailid = explode(',',$fieldhideshowids);
		$time = date("d-m-Y-g-i-s-a");
		$today = date("d-m-Y");
		$today_time=date('g:i:s a');
		$title = $this->input->post('reportname');
		$title =  str_replace('00',' ',$title);
		$title =  substr($title,0,30);
		$name = preg_replace('/[^A-Za-z0-9]/', '', $title);
		$fname = strtolower($name);  
		$filetype = $this->input->post('filetypename');	
		$viewcolids=$this->input->post('viewcolids');
		$exportall = $this->input->post('reportexportall');
		$pagenum = $this->input->post('page');
		$rowscount = $this->input->post('rowcount');
		$width = $this->input->post('wwidth');
		$height = $this->input->post('wheight');
		$getreportmoduleinfo=$this->Reportsmodel->getreportmoduleinfo($this->input->post('reportmodule'));
		
		$_GET['parentid']=$getreportmoduleinfo['modulemastertable'].'id';
		$_GET['viewfieldids']=$viewcolids;
		$_GET['maintabinfo']=$getreportmoduleinfo['modulemastertable'];
		if($exportall == 'Yes') {
			$lastid = $this->Basefunctions->maximumid($_GET['maintabinfo'],$_GET['parentid']);
			$pagenum = 1;
			//$rowscount = $lastid;
			$rowscount = 10000;
		} else {
			$pagenum = 1;
			$rowscount = $this->input->post('rowcount');
		}
		$_GET['tabularviewcolids']=$this->input->post('tabularviewcolids');
		$_GET['summarviewcolids']=$this->input->post('summarviewcolids');
		$_GET['summaramids']=$this->input->post('summaramids');
		$_GET['summaramname']=$this->input->post('summaramname');
		$_GET['groupcolids']=$this->input->post('groupcolids');
		$_GET['sortby']=$this->input->post('sortby');
		$_GET['groupbyrange']=$this->input->post('groupbyrange');
		$_GET['transferyesno']=$this->input->post('transferyesno');
		$_GET['date_columnid']=$this->input->post('date_columnid');
		$_GET['date_method']=$this->input->post('date_method');
		$_GET['date_mode']=$this->input->post('date_mode');
		$_GET['date_start']=$this->input->post('date_start');
		$_GET['date_end']=$this->input->post('date_end');
		$_GET['summaryrollup']=$this->input->post('summaryrollup');
		$_GET['reportingmode']=$this->input->post('reportingmode');
		
		$_GET['reportmodule']=$this->input->post('reportmodule');
		$_GET['reportcreateconditionids']=$this->input->post('reportcreateconditionids');
		$_GET['reportconditiondata']=$this->input->post('reportconditiondata');
		$_GET['filter'] = $this->input->post('filter');
		$_GET['conditionname']= $this->input->post('conditionname');
		$_GET['filtervalue']= $this->input->post('filtervalue');
		$rowid = $_GET['parentid'];
		$viewcolmoduleids = $getreportmoduleinfo['moduleid'];
	
		$reporttype=$this->input->post('rftype');
		$sortcol =  '';
		$sortord =  '';		
		$filterid = isset($_GET['filter']) ? $_GET['filter'] : '';
		$conditionname = isset($_GET['conditionname']) ? $_GET['conditionname'] : '';
		$filtervalue = isset($_GET['filtervalue']) ? $_GET['filtervalue'] : '';
		$filter = array('0'=>$filterid,'1'=>$conditionname,'2'=>$filtervalue);
		$result = $this->Basefunctions->generatereport($viewcolids,$pagenum,$rowscount,$sortcol,$sortord,$rowid,$viewcolmoduleids,$reporttype,$filter,$filetype);
		$resultcoldata=$result[0];
		$result=$result[1];
		$this->excel->setActiveSheetIndex(0);
		$sheet = $this->excel->getActiveSheet();
		$sheet->setTitle($title);
		$count = 0;
		$celtitle = $resultcoldata['colname'];
		$titlecount = count($celtitle);
		$dataname = $resultcoldata['colmodelname'];
		 $style = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			)
		);
		$reportdatecolumnid=$_GET['date_columnid'];
		$reportdatemethod=$_GET['date_method'];
		$reportmode=$_GET['date_mode'];
		$reportstartdate=$_GET['date_start'];
		$reportenddate=$_GET['date_end'];
		//check date column range
		$datewhere="1";
		$date = date("Y-m-d");
		$startdate = $date;
		$enddate = $date;
		if($reportdatecolumnid > 1 and $reportdatemethod != '') {
			//get column specific data
			$daterangecolumn= $this->Reportsmodel->reportdatecolumn($reportdatecolumnid);
			if($reportmode == 'custom'){
				$startdate=$reportstartdate;
				$startdate = date('Y-m-d', strtotime($startdate));
				$enddate = $reportenddate;
				$enddate = date('Y-m-d', strtotime($enddate));			
			} else if($reportmode == 'switch'){
				$r_date=$this->Basefunctions->datesystem($reportdatemethod);
				$startdate=$r_date['start'];
				$enddate=$r_date['end'];
			} else if($reportmode == 'instant'){
				$dateofinstant = $this->Basefunctions->getdatevalue($reportdatemethod,$date);
				$startdate = $dateofinstant['start'];
				$enddate = $dateofinstant['end'];
			} else if($reportmode == 'direct') {
				$f_date = strtotime(''.$reportdatemethod.'' ,strtotime($date));
				$f_date = date("Y-m-d",$f_date);
				if(strtotime($f_date) > strtotime($date)) {
					$startdate=$date;
					$enddate=$f_date;
				} else {
					$startdate=$f_date;
					$enddate=$date;
				}
			} else if($reportmode == 'before'){
				$f_date = strtotime(''.$reportdatemethod.' day' ,strtotime($date));
				$f_date = date("Y-m-d",$f_date);
				$startdate=$f_date;
				$enddate=$f_date;
			}
			$date_col=$daterangecolumn['parent'].'.'.$daterangecolumn['columnname'];
			if($reportmode == 'before') {
				$datewhere="DATE(".$date_col.") < '".$enddate."' ";
				$dateshow = 'Before '.$enddate;
			} else {
				if($startdate == $enddate) {
					$datewhere="DATE(".$date_col.") = '".$startdate."' ";
				} else {
					$datewhere="DATE(".$date_col.") >= '".$startdate."' AND DATE(".$date_col.") <= '".$enddate."'";
				}
				$dateshow = $startdate.' To '.$enddate;	
			}
		} else {
			$datewhere="1";
			$dateshow = 'ALL Data';
		}
		$userid = $this->Basefunctions->userid;
		$branchid = $this->Basefunctions->branchid;
		$employeename = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$branchname = $this->Basefunctions->singlefieldfetch('branchname','branchid','branch',$branchid);
		//Report name
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(13);
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);	
		$this->excel->getActiveSheet()->setCellValue('A1','Report Name : '.$title);
		$this->excel->getActiveSheet()->mergeCells('A1:E1');
		//Report Date and Time
		$detailsfrom = $dateshow ;
		$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(13);
		$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);	
		$this->excel->getActiveSheet()->setCellValue('A2','Date : '.$detailsfrom);
		$this->excel->getActiveSheet()->mergeCells('A2:E2');
		//Report generated Date and Time
		$gendetails = $today_time.'  by  '.$employeename.'  at Branch :  '. $branchname;
		$this->excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(13);
		$this->excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(true);	
		$this->excel->getActiveSheet()->setCellValue('A3','Report Generated on : '.$today.'  '.$gendetails);
		$this->excel->getActiveSheet()->mergeCells('A3:E3');
		//Filter data display
		if(!empty($filter)) {
			$fconditonid = explode('|',$filter[0]);
			$filtercondition = explode('|',$filter[1]);
			$filtervalue = explode('|',$filter[2]);
		} else {
			$fconditonid = array();
			$filtercondition = array();
			$filtervalue = array();
		}
		$x=4;

		//filter data display
		$filtercontent = '';
		$dduitypeids = array('17','18','19','23','25','26','27','28','29');
		if(!empty($fconditonid)) {
			for($i=0;$i<count($fconditonid);$i++) {
				if($fconditonid[$i] != 'undefined' && $fconditonid[$i] != '') {
					$uitype = $this->Basefunctions->generalinformaion('viewcreationcolumns','uitypeid','viewcreationcolumnid',$fconditonid[$i]);
					if(in_array($uitype,$dduitypeids)){
						$tablename = $this->Basefunctions->generalinformaion('viewcreationcolumns','viewcreationcolmodeltable','viewcreationcolumnid',$fconditonid[$i]);
						$table =  explode(' as ',$tablename);
						if(isset($table[1])) {
							$tablename = $table[0];
						}
						$label = $this->Basefunctions->generalinformaion('viewcreationcolumns','viewcreationcolumnname','viewcreationcolumnid',$fconditonid[$i]);
						$value = $this->Basefunctions->fetchvalue($tablename,$tablename.'name',$tablename.'id',$filtervalue[$i]);
						$this->excel->getActiveSheet()->getStyle('A'.$x)->getFont()->setSize(13);
						$this->excel->getActiveSheet()->setCellValue('A'.$x,$label.' : '.$value);
						$this->excel->getActiveSheet()->mergeCells('A'.$x.':E'.$x); 
					} else {
						$label = $this->Basefunctions->generalinformaion('viewcreationcolumns','viewcreationcolumnname','viewcreationcolumnid',$fconditonid[$i]);
						$this->excel->getActiveSheet()->getStyle('A'.$x)->getFont()->setSize(13);
						if($filtercondition[$i] == 'Between') {
							$value = $this->Basefunctions->fetchvalue('rangemaster','rangemastername','rangemasterid',$filtervalue[$i]);
						} else {
							$value = $filtervalue[$i];
						}
						$this->excel->getActiveSheet()->setCellValue('A'.$x,$label.'  '.$filtercondition[$i].' '.$value);
						$this->excel->getActiveSheet()->mergeCells('A'.$x.':E'.$x);
					}
					$x++;
				}
			}
		}
		//header
		//sl.NO
		$this->excel->getActiveSheet()->getStyle('A'.$x)->getFont()->setSize(13);
		$this->excel->getActiveSheet()->getStyle('A'.$x)->getFont()->setBold(true);	
		$this->excel->getActiveSheet()->setCellValue('A'.$x,'Sl.No');
		$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(50);
		$h='B';
		for($i=0;$i<$titlecount;$i++) {
			if(in_array($resultcoldata['colid'][$i],$salessalesdetailid)) { } else {
				$this->excel->getActiveSheet()->getStyle($h.$x)->getFont()->setSize(13);
				$this->excel->getActiveSheet()->getStyle($h.$x)->getFont()->setBold(true);	
				$this->excel->getActiveSheet()->setCellValue($h.$x,$celtitle[$i]);
				$this->excel->getActiveSheet()->getColumnDimension($h)->setAutoSize(50);
			}
			$h++;
		} 
		$m=1;
		$tot_rows = 1;
		$rows=$x+1;
		foreach($result->result() as $row) {
			$c='B';
			$this->excel->getActiveSheet()->getStyle('A'.$rows)->getFont()->setBold(false);
			$this->excel->getActiveSheet()->setCellValue('A'.$rows,$m);
			$this->excel->getActiveSheet()->getStyle('A'.$rows)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			if ($rows % 2 == 0) {
				$this->excel->getActiveSheet()->getStyle('A'.$rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$this->excel->getActiveSheet()->getStyle('A'.$rows)->getFill()->getStartColor()->setARGB('e4f3fd');
			}
			$m++;
			for($i=0;$i<$titlecount;$i++) {
				$rowproperty = get_object_vars($row);				
				$propertykey = array_keys($rowproperty);				
				$difference = array_diff($propertykey,$dataname);
				$value = array("billingaddress","shippingaddress","billingcity","shippingcity","billingstate","shippingstate","billingcountry","shippingcountry","billingpincode","shippingpincode","newaccountname");
				$rowpropertychange = array_intersect($value,$difference);
				$rowpropertychange = array_values($rowpropertychange);
				if(count($rowpropertychange)> 0){ 
					 for ($ai = 0; $ai < count($rowpropertychange); ++$ai) {
					 	if($rowpropertychange[$ai] == 'billingaddress' || $rowpropertychange[$ai] == 'shippingaddress'){
					 		$row->address = $row->$rowpropertychange[$ai];
					 		unset($row->$rowpropertychange[$ai]);
					 	}
					 	elseif($rowpropertychange[$ai] == 'billingcity' || $rowpropertychange[$ai] == 'shippingcity'){
					 		$row->city = $row->$rowpropertychange[$ai];
					 		unset($row->$rowpropertychange[$ai]);
					 	}
					 	elseif($rowpropertychange[$ai] == 'billingstate' || $rowpropertychange[$ai] == 'shippingstate'){
					 		$row->state = $row->$rowpropertychange[$ai];
					 		unset($row->$rowpropertychange[$ai]);
					 	}
					 	elseif($rowpropertychange[$ai] == 'billingcountry' || $rowpropertychange[$ai] == 'shippingcountry'){
					 		$row->country = $row->$rowpropertychange[$ai];
					 		unset($row->$rowpropertychange[$ai]);
					 	}
					 	elseif($rowpropertychange[$ai] == 'billingpincode' || $rowpropertychange[$ai] == 'shippingpincode'){
					 		$row->pincode = $row->$rowpropertychange[$ai];
					 		unset($row->$rowpropertychange[$ai]);
					 	}
					 	elseif($rowpropertychange[$ai] == 'newaccountname'){
					 		$row->accountname = $row->$rowpropertychange[$ai];
					 		unset($row->$rowpropertychange[$ai]);
					 	}
					}
				}	
				$dname = $dataname[$i];
				if(in_array($resultcoldata['colid'][$i],$salessalesdetailid)) { } else {
					$this->excel->getActiveSheet()->getStyle($c.$rows)->getFont()->setBold(false);
					$this->excel->getActiveSheet()->getStyle($c.$rows)->getAlignment()->setWrapText(true);		
					$this->excel->getActiveSheet()->setCellValue($c.$rows,trim($row->$dname));
					$this->excel->getActiveSheet()->getStyle($c.$rows)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					if( is_numeric( trim( $row->$dname ) ) ) {
						$this->excel->getActiveSheet()->getStyle($c.$rows)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_GENERAL);
						$this->excel->getActiveSheet()->getStyle($c.$rows)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					}
					if ($rows % 2 == 0) {
						$this->excel->getActiveSheet()->getStyle($c.$rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
						$this->excel->getActiveSheet()->getStyle($c.$rows)->getFill()->getStartColor()->setARGB('e4f3fd');
					}
				}
				$c++;
			}
			$rows++;
			$tot_rows++;
		}
			
		ob_clean();
		if($filetype == 'xlsx'){
			$filename=$fname.'_'.$time.'.xlsx';
			header("Content-type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8;");
			header("Content-type:application/octetstream");
			header("Content-Disposition:attachment;filename=".$filename."");
			header("Content-Transfer-Encoding:binary");
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
		}
		else{ //csv format
			$filename=$fname.'_'.$time.'.csv';
			header('Content-Type: application/vnd.ms-excel');
			header("Content-type:application/octetstream");
			header("Content-Disposition:attachment;filename=".$filename."");
			header("Content-Transfer-Encoding:binary");
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'CSV');
		}
		$objWriter->save('php://output');			
	}
	//pdf report
	public function newpdfdataexportmodel() {
		$fieldhideshowids = $this->Basefunctions->get_company_settings('reportscolumnfieldhideshow');
		$salessalesdetailid = explode(',',$fieldhideshowids);
		$time = date("d-m-Y-g-i-s-a");
		$today = date("d-m-Y");
		$today_time=date('g:i:s a');
		$this->load->library('pdf');
		$title = $this->input->post('reportname');
		$title =  str_replace('00',' ',$title);
		$name = preg_replace('/[^A-Za-z0-9]/', '', $title);
		$fname = strtolower($name);  
		$filetype = $this->input->post('filetypename');	
		$viewcolids=$this->input->post('viewcolids');
		$exportall = $this->input->post('reportexportall');
		$width = $this->input->post('wwidth');
		$height = $this->input->post('wheight');
		$getreportmoduleinfo=$this->Reportsmodel->getreportmoduleinfo($this->input->post('reportmodule'));
		$_GET['parentid']=$getreportmoduleinfo['modulemastertable'].'id';
		$_GET['viewfieldids']=$viewcolids;
		$_GET['maintabinfo']=$getreportmoduleinfo['modulemastertable'];
		if($exportall == 'Yes') {
			$lastid = $this->Basefunctions->maximumid($_GET['maintabinfo'],$_GET['parentid']);
			$pagenum = 1;
			$rowscount = $lastid;
		} else {
			$pagenum = 1;
			$rowscount = $this->input->post('rowcount');
		}
		$_GET['tabularviewcolids']=$this->input->post('tabularviewcolids');
		$_GET['summarviewcolids']=$this->input->post('summarviewcolids');
		$_GET['summaramids']=$this->input->post('summaramids');
		$_GET['summaramname']=$this->input->post('summaramname');
		$_GET['groupcolids']=$this->input->post('groupcolids');
		$_GET['sortby']=$this->input->post('sortby');
		$_GET['groupbyrange']=$this->input->post('groupbyrange');
		$_GET['summaryrollup']=$this->input->post('summaryrollup');
		$_GET['reportingmode']=$this->input->post('reportingmode');
		
		$_GET['date_columnid']=$this->input->post('date_columnid');
		$_GET['date_method']=$this->input->post('date_method');
		$_GET['date_mode']=$this->input->post('date_mode');
		$_GET['date_start']=$this->input->post('date_start');
		$_GET['date_end']=$this->input->post('date_end');
		
		$_GET['reportmodule']=$this->input->post('reportmodule');
		$_GET['reportcreateconditionids']=$this->input->post('reportcreateconditionids');
		$_GET['reportconditiondata']=$this->input->post('reportconditiondata');
		$_GET['filter'] = $this->input->post('filter');
		$_GET['conditionname']= $this->input->post('conditionname');
		$_GET['filtervalue']= $this->input->post('filtervalue');
		$rowid = $_GET['parentid'];
		$viewcolmoduleids = $getreportmoduleinfo['moduleid'];
		$reportdatecolumnid=$_GET['date_columnid'];
		$reportdatemethod=$_GET['date_method'];
		$reportmode=$_GET['date_mode'];
		$reportstartdate=$_GET['date_start'];
		$reportenddate=$_GET['date_end'];
		//check date column range
		$datewhere="1";
		$date = date("Y-m-d");
		$startdate = $date;
		$enddate = $date;
		if($reportdatecolumnid > 1 and $reportdatemethod != '') {
			//get column specific data
			$daterangecolumn= $this->Reportsmodel->reportdatecolumn($reportdatecolumnid);
			if($reportmode == 'custom'){
				$startdate=$reportstartdate;
				$startdate = date('Y-m-d', strtotime($startdate));
				$enddate = $reportenddate;
				$enddate = date('Y-m-d', strtotime($enddate));			
			} else if($reportmode == 'switch'){
				$r_date=$this->Basefunctions->datesystem($reportdatemethod);
				$startdate=$r_date['start'];
				$enddate=$r_date['end'];
			} else if($reportmode == 'instant'){
				$dateofinstant = $this->Basefunctions->getdatevalue($reportdatemethod,$date);
				$startdate = $dateofinstant['start'];
				$enddate = $dateofinstant['end'];
			} else if($reportmode == 'direct') {
				$f_date = strtotime(''.$reportdatemethod.'' ,strtotime($date));
				$f_date = date("Y-m-d",$f_date);
				if(strtotime($f_date) > strtotime($date)) {
					$startdate=$date;
					$enddate=$f_date;
				} else {
					$startdate=$f_date;
					$enddate=$date;
				}
			} else if($reportmode == 'before'){
				$f_date = strtotime(''.$reportdatemethod.' day' ,strtotime($date));
				$f_date = date("Y-m-d",$f_date);
				$startdate=$f_date;
				$enddate=$f_date;
			}
			$date_col=$daterangecolumn['parent'].'.'.$daterangecolumn['columnname'];
			if($reportmode == 'before') {
				$datewhere="DATE(".$date_col.") < '".$enddate."' ";
				$dateshow = 'Before '.$enddate;
			} else {
				if($startdate == $enddate) {
					$datewhere="DATE(".$date_col.") = '".$startdate."' ";
				} else {
					$datewhere="DATE(".$date_col.") >= '".$startdate."' AND DATE(".$date_col.") <= '".$enddate."'";
				}
				$dateshow = $startdate.' To '.$enddate;	
			}
		} else {
			$datewhere="1";
			$dateshow = 'ALL Data';
		}
		$reporttype=$this->input->post('rftype');
		$sortcol =  '';
		$sortord =  '';		
		$filterid = isset($_GET['filter']) ? $_GET['filter'] : '';
		$conditionname = isset($_GET['conditionname']) ? $_GET['conditionname'] : '';
		$filtervalue = isset($_GET['filtervalue']) ? $_GET['filtervalue'] : '';
		$filter = array('0'=>$filterid,'1'=>$conditionname,'2'=>$filtervalue);
		$result = $this->Basefunctions->generatereport($viewcolids,$pagenum,$rowscount,$sortcol,$sortord,$rowid,$viewcolmoduleids,$reporttype,$filter,$filetype);
		$resultcoldata=$result[0];
		$result=$result[1];				
		//titlec content
		$count = 0;
		$celtitle = $resultcoldata['colname'];
		$titlecount = count($celtitle);
		$dataname = $resultcoldata['colmodelname'];
		
		//pdf creation
		$this->pdf->SetCreator(PDF_CREATOR);
		$this->pdf->SetTitle($title);
		$this->pdf->SetMargins(10,5,5,false);// set margins
		//Report Date and Time
		$detailsfrom = $dateshow.' ON '.$today.' '.$today_time;
		if($titlecount<='4') {
			$this->pdf->AddPage('P', 'A4');
			$unitsize = round(210/$titlecount);		
			$colm_size = max(array_fill(0,($titlecount),$unitsize));
		} else {
			$this->pdf->AddPage('L', 'A3');
			$unitsize = round(260/$titlecount);		
			$colm_size = max(array_fill(0,($titlecount),$unitsize));
		} 
		$this->pdf->SetFont('helvetica', 'B',10);
		if($titlecount<='5') {
			$this->pdf->Cell(170,17,$title, 0, false, 'C', 0, 0, false, 'M', 'M');
		} else {
			$this->pdf->Cell(250,17,$title, 0, false, 'C', 0, 0, false, 'M', 'M');
		}
		$this->pdf->Ln(4.5);
		$this->pdf->SetFont('helvetica', 'B',8);
		if($titlecount<='5') {
			$this->pdf->Cell(180,17,$detailsfrom, 0, false, 'C', 0, 0, false, 'M', 'M');
			$x = 180;
		} else {
			$this->pdf->Cell(260,17,$detailsfrom, 0, false, 'C', 0, 0, false, 'M', 'M');
			$x = 260;
		}
		$this->pdf->Ln(4.5);
		//Filter data display
		if(!empty($filter)) {
			$fconditonid = explode('|',$filter[0]);
			$filtercondition = explode('|',$filter[1]);
			$filtervalue = explode('|',$filter[2]);
		} else {
			$fconditonid = array();
			$filtercondition = array();
			$filtervalue = array();
		}
		$filtercontent = '';
		$dduitypeids = array('17','18','19','23','25','26','27','28','29');
		$x = $x+10;
		if(!empty($fconditonid)) {
			for($i=0;$i<count($fconditonid);$i++) {
				if($fconditonid[$i] != 'undefined' && $fconditonid[$i] != '') {
					$uitype = $this->Basefunctions->generalinformaion('viewcreationcolumns','uitypeid','viewcreationcolumnid',$fconditonid[$i]);
					if(in_array($uitype,$dduitypeids)){
						$tablename = $this->Basefunctions->generalinformaion('viewcreationcolumns','viewcreationcolmodeltable','viewcreationcolumnid',$fconditonid[$i]);
						$table =  explode(' as ',$tablename);
						if(isset($table[1])) {
							$tablename = $table[0];
						}
						$label = $this->Basefunctions->generalinformaion('viewcreationcolumns','viewcreationcolumnname','viewcreationcolumnid',$fconditonid[$i]);
						$value = $this->Basefunctions->fetchvalue($tablename,$tablename.'name',$tablename.'id',$filtervalue[$i]);
						$fdata = $label.' : '.$value;
						$this->pdf->Cell($x,17,$fdata, 0, false, 'L', 0, 0, false, 'M', 'M');
						$this->pdf->Ln(4.5);
					} else {
						$label = $this->Basefunctions->generalinformaion('viewcreationcolumns','viewcreationcolumnname','viewcreationcolumnid',$fconditonid[$i]);
						if($filtercondition[$i] == 'Between') {
							$value = $this->Basefunctions->fetchvalue('rangemaster','rangemastername','rangemasterid',$filtervalue[$i]);
						} else {
							$value = $filtervalue[$i];
						}
						$fdata = $label.'  '.$filtercondition[$i].' '.$value;
						$this->pdf->Cell($x,17,$fdata, 0, false, 'L', 0, 0, false, 'M', 'M');
						$this->pdf->Ln(4.5);
					}
					$x+10;
				}
			}
		}
		$this->pdf->SetFillColor(241,250,255);//background color
        $this->pdf->SetTextColor(28,148,196);//font color
        $this->pdf->SetDrawColor(0,0,0);//border color
        $this->pdf->SetLineWidth(0.2);
        $this->pdf->SetFont('helvetica', 'B',8);
		$this->pdf->setCellPaddings(1,2,0,0);		
		//header
		$w =explode(',',$colm_size);
		$linebrkcount = max($w);
		$h='';
		for($i=0;$i<($titlecount);++$i) {
			if($i==0) {
				$h=$this->pdf->getNumLines($celtitle[$i],$linebrkcount);
			}
			$h=$h.','.$this->pdf->getNumLines($celtitle[$i],$linebrkcount);
		}
		$he=explode(',',$h);
		$linecount1 = max($he);
		$this->pdf->MultiCell(10,$linecount1*7,'S.No','LRBT','L',0,0);
		for($hi = 0; $hi < ($titlecount); ++$hi) {
			if(in_array($resultcoldata['colid'][$hi],$salessalesdetailid)) { } else {
				$this->pdf->MultiCell(($colm_size),$linecount1*7,$celtitle[$hi],'LRBT','L',0,0);
			}
		}
        $this->pdf->Ln();
		//content
		$fill=true;
		$m=1;
		$h=1;
		
		foreach($result->result() as $row) {
			//Content
			$this->pdf->SetFillColor(228,243,253);//background color		
			$this->pdf->SetTextColor(51,51,51);	//font color	
			$this->pdf->SetDrawColor(0, 0, 0);//border color
			$this->pdf->SetLineWidth(.1); 
			$this->pdf->SetFont('helvetica','',7);	
			$this->pdf->setCellPaddings(1,2,0,0);
			$a='';
			$getcount=count($dataname);
			for($i=0;$i<$getcount;$i++) {
				$dname = $dataname[$i];
				if($i==0) {
					if(in_array($resultcoldata['colid'][$i],$salessalesdetailid)) { } else {
						$a=$this->pdf->getNumLines($row->$dname,$linebrkcount);
					}
				}
				if(in_array($resultcoldata['colid'][$i],$salessalesdetailid)) { } else {
					$a=$a.','.$this->pdf->getNumLines($row->$dname,$linebrkcount);
				}
			}
			$b=explode(',',$a);
			$linecount = max($b);
			$this->pdf->setAutoPageBreak(false);
			$this->pdf->startTransaction();
			$this->pdf->MultiCell(10,$linecount*6,$m,'LRBT','L',0,0);
			for($j=0;$j<$getcount;$j++) {
				$dname = $dataname[$j];
				if(in_array($resultcoldata['colid'][$j],$salessalesdetailid)) { } else {
					$this->pdf->MultiCell(($colm_size),$linecount*6,$row->$dname,'LRBT','L',0,0);
				}
			}
			$this->pdf->Ln();
			$fill=!$fill;
			if ($this->pdf->getY() > $this->pdf->getPageHeight() - 20) {
				$this->pdf->rollbackTransaction(true);
				$this->pdf->AddPage();
				$this->pdf->MultiCell(10,$linecount*6,$m,'LRBT','L',0,0);
				for($j=0;$j<$getcount;$j++) {
					$dname = $dataname[$j];
					if(in_array($resultcoldata['colid'][$j],$salessalesdetailid)) { } else {
						$this->pdf->MultiCell(($colm_size),$linecount*6,$row->$dname,'LRBT','L',0,0);
					}
				}
				$this->pdf->ln();
				$fill=!$fill;
			}
			$m++;
			$this->pdf->commitTransaction();
			$this->pdf->setAutoPageBreak(true,30);
		}
		$today = date("d-M-Y");
		$today_time=date('g-i-s_a');
		$this->pdf->Output($title.'_'.$today.'_'.$today_time.'.pdf','I');
		header("Connection:close");
		//now summary data
	}
	
	
	//Html Generate report
	public function htmlformatreprotgeneratemodel() {
		$title = $this->input->post('reportname');
		$name = preg_replace('/[^A-Za-z0-9]/', '', $title);
		$fname = strtolower($name);  
		$filetype = $this->input->post('filetypename');	
		$viewcolids=$this->input->post('viewcolids');
		$exportall = $this->input->post('reportexportall');
		$width = $this->input->post('wwidth');
		$height = $this->input->post('wheight');
		$getreportmoduleinfo=$this->Reportsmodel->getreportmoduleinfo($this->input->post('reportmodule'));
		$_GET['parentid']=$getreportmoduleinfo['modulemastertable'].'id';
		$_GET['viewfieldids']=$viewcolids;
		$_GET['maintabinfo']=$getreportmoduleinfo['modulemastertable'];
		if($exportall == 'Yes') {
			$lastid = $this->Basefunctions->maximumid($_GET['maintabinfo'],$_GET['parentid']);
			$pagenum = 1;
			$rowscount = $lastid;
		} else {
			$pagenum = 1;
			$rowscount = $this->input->post('rowcount');
		}
		// FILE NAME FOR GENERATE HTML REPORT
		$html_file_name = 'printtemplate/htmlreportfile.html';
		@chmod($html_file_name,0766);
		//
		$_GET['tabularviewcolids']=$this->input->post('tabularviewcolids');
		$_GET['summarviewcolids']=$this->input->post('summarviewcolids');
		$_GET['summaramids']=$this->input->post('summaramids');
		$_GET['summaramname']=$this->input->post('summaramname');
		$_GET['groupcolids']=$this->input->post('groupcolids');
		$_GET['sortby']=$this->input->post('sortby');
		$_GET['groupbyrange']=$this->input->post('groupbyrange');
		
		$_GET['date_columnid']=$this->input->post('date_columnid');
		$_GET['date_method']=$this->input->post('date_method');
		$_GET['date_mode']=$this->input->post('date_mode');
		$_GET['date_start']=$this->input->post('date_start');
		$_GET['date_end']=$this->input->post('date_end');
		
		$_GET['reportmodule']=$this->input->post('reportmodule');
		$_GET['reportcreateconditionids']=$this->input->post('reportcreateconditionids');
		$_GET['reportconditiondata']=$this->input->post('reportconditiondata');
		$_GET['filter'] = $this->input->post('filter');
		$_GET['conditionname']= $this->input->post('conditionname');
		$_GET['filtervalue']= $this->input->post('filtervalue');
		$rowid = $_GET['parentid'];
		$viewcolmoduleids = $getreportmoduleinfo['moduleid'];
		$reporttype='SV';
		$sortcol =  '';
		$sortord =  '';		
		$filterid = isset($_GET['filter']) ? $_GET['filter'] : '';
		$conditionname = isset($_GET['conditionname']) ? $_GET['conditionname'] : '';
		$filtervalue = isset($_GET['filtervalue']) ? $_GET['filtervalue'] : '';
		$filter = array('0'=>$filterid,'1'=>$conditionname,'2'=>$filtervalue);
		$result = $this->Basefunctions->generatereport($viewcolids,$pagenum,$rowscount,$sortcol,$sortord,$rowid,$viewcolmoduleids,$reporttype,$filter,$filetype);
		$resultcoldata=$result[0];
		$groupbycolumns=$result[8];
		$result=$result[1];				
		//titlec content
		$count = 0;
		$celtitle = $resultcoldata['colname'];
		$titlecount = count($celtitle);
		$dataname = $resultcoldata['colmodelname'];
		
		//HEADER OF HTML REPORT
		$content = '';
		$content .= '<table style="width: 100%;"><tbody style=""><tr><td style="border:1px solid rgb(0,0,0);width: 100%; text-align: center;"><strong>'.$title.'</strong><br></td></tr></tbody></table>';
		//COLUMNS OF THE REPORT
		$content .= '<table style="width: 100%;"><tbody style=""><tr>';
		$width =  100/$titlecount;
		for($i=0;$i<($titlecount);++$i) {
			$content .= '<td style="border:1px solid rgb(0,0,0); text-align: center;"><strong>'.$celtitle[$i].'</strong><br></td>';
		}
		$content .= '</tr></tbody></table>';
		//BODY OF HTML REPORT
		$content .= '<table style="width: 100%;"><tbody style="">';
		$g=1;
		$repeativearr=array();
		$recordcount = $result->num_rows();
		foreach($result->result() as $row) {
			$getcount=count($dataname);
				$content .= '<tr>';
				for($i=0;$i<$getcount;$i++) {
					if (is_numeric($row->$dataname[$i])) {
						$allign = 'right';
					} else {
						$allign = 'left';
					}
					if($i<count($groupbycolumns)){
						if(in_array($dataname[$i],$groupbycolumns)) {
							if($row->$dataname[$i] == '' || $row->$dataname[$i] == null){
								$value = 'Total';
							} else {
								if(in_array($row->$dataname[$i],$repeativearr)) {
									$value = '';
								} else {
									$repeativearr[$i] = $row->$dataname[$i];
									$value = $row->$dataname[$i];
								}
							}
						}
					} else {
						$value = $row->$dataname[$i];
					$content .= '<td style="border:1px solid rgb(0,0,0); text-align: '.$allign.';">'.$value.'<br></td>';
					}
					
				}
			$g++;
			$content .= '</tr>';
		}
		$content .= '</tbody></table>';
		@write_file($html_file_name,$content);
		echo $content;
	}
	/* Code Cleaning
	*/
	/*
	Get Module name Based On Menu Category 
	*/
	public function loadmodulenamebasedcategory() {
		$industryid=$this->Basefunctions->industryid;
		$data = $this->db->select('GROUP_CONCAT(moduleid) as moduleid,GROUP_CONCAT(moduledashboardid) as moduledashboardid')
							->from('module')
							->where('parentmoduleid',$_GET['partentid'])
							->where('status','1')
							->where("FIND_IN_SET('".$industryid."',industryid) >", 0)
							->get()->result();
		$arr1=explode(',',$data[0]->moduleid);
		$val=explode(',',$data[0]->moduledashboardid);
		if(count(array_filter(array_diff($val,array(1))))>0) {
				$submodules = $this->db->select('GROUP_CONCAT(moduleid) as moduleid,GROUP_CONCAT(moduledashboardid) as moduledashboardid')
							->from('module')
							->where('parentmoduleid',$_GET['partentid'])
							->where('moduledashboardid!="" and moduledashboardid!=1')
							->where('status','1')
							->where("FIND_IN_SET('".$industryid."',industryid) >", 0)
							->get()->result();
			
			$arr2=explode(',',$submodules[0]->moduleid);
			$arr3=explode(',',$submodules[0]->moduledashboardid);
			$uniquemoduleid=array_diff($arr1,$arr2);	
			$finalmoduleid=array_merge($uniquemoduleid,$arr3);
			$status=array(0,1);
		} else {
			$finalmoduleid=$arr1;
			$status=array(1);
		}
		$arrinfo = $this->db->select('moduleid,menuname')
							->from('module')
							->where_in('moduleid',$finalmoduleid)
							->where_in('status',$status)
							->order_by('sortorder','ASC')
							->get();
		
		if($arrinfo->num_rows() > 0) {
			foreach($arrinfo->result() as $info){
				$jsondata[]=array('result'=>true,'modulename'=>$info->menuname,'moduleid'=>$info->moduleid);
			}
		} else {
			$jsondata=array();
		}
		echo   json_encode($jsondata);
	}
	// Load => Report builder Condition based on UI tpye.
	public function loadconditionbyuitype() {
		$result=$this->db->select('whereclauseid',false)->where('uitypeid',$_GET['uitypeid'])
		->where_in('status',array(1,3))->get('uitype')->row()->whereclauseid;
		$result=explode(',',$result);
		$this->db->select('whereclause.whereclauseid,whereclause.whereclausename',false);
		$this->db->from('whereclause');
		$this->db->where_in('whereclause.whereclauseid',$result);
		$this->db->where_in('whereclause.status',$this->Basefunctions->activestatus);
		$data=$this->db->get();
		if($data->num_rows() >0) {
			$i=0;
			foreach($data->result()as $row) {
				$ddata[$i]=array('whereclausename'=>$row->whereclausename,'whereclausename'=>$row->whereclausename);
				$i++;
			}
			echo json_encode($ddata);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}		
	}
	// Load => Report builder Condition based on UI tpye. 
	public function loadaggregatemethodbyuitype() {
		$result=$this->db->select('aggregatemethodid',false)->where('uitypeid',$_GET['uitypeid'])
		->where_in('status',array(1,3))->get('uitype')->row()->aggregatemethodid;
		$result=explode(',',$result);
		$this->db->select('aggregatemethodid,aggregatemethodname,aggregatemethodkey,whereclauseid',false);
		$this->db->from('aggregatemethod');
		$this->db->where_in('aggregatemethod.aggregatemethodid',$result);
		$this->db->where_in('aggregatemethod.status',$this->Basefunctions->activestatus);
		$data=$this->db->get();
		if($data->num_rows() >0) {
			$i=0;
			foreach($data->result()as $row) {
				$ddata[$i]=array('aggregatemethodname'=>$row->aggregatemethodname,'aggregatemethodkey'=>$row->aggregatemethodkey,'whereclauseid'=>$row->whereclauseid);
				$i++;
			}
			echo json_encode($ddata);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}		
	}
	//folder data fetch for drop down
	public function folderloadbydropdown() {
		$data = array();
		$i=0;
		$this->db->select('reportfoldername,reportfolderid,setdefault');
		$this->db->from('reportfolder');
		$this->db->where('reportfolder.status',1);
		$result = $this->db->get();
		return $result->result();
	}
	//public function get all dropdowns
	public function getalldropdownbasedonmodule($modid,$moduletabsecid) {
		$userroleid = $this->Basefunctions->userroleid;
		$colname = array();
		$moduleid= explode(',',$modid);
		$uitype = array(17,18,19,20,21,23,25,26,27,28,29,13);
		$industryid = $this->Basefunctions->industryid;
		$i=0;
		$data= array();
		for($k=0;$k<count($moduleid);$k++) {
			$this->db->select('viewcreationcolumnid,viewcreationcolmodelindexname,viewcreationcolumnname,viewcreationparenttable,multiple,uitypeid,viewcreationcolumns.viewcreationmoduleid,module.modulename');
			$this->db->from('viewcreationcolumns');
			$this->db->join('viewcreationmodule','viewcreationmodule.viewcreationmoduleid=viewcreationcolumns.viewcreationmoduleid','left outer');
			$this->db->join('moduletabsection','moduletabsection.moduletabsectionid=viewcreationcolumns.moduletabsectionid','left outer');
			$this->db->join('module','module.moduleid=moduletabsection.moduleid','left outer');
			$this->db->join('moduleinfo','moduleinfo.moduleid=module.moduleid','left outer');
			if($userroleid == 3 && $moduleid[$k] == 52) {
				$this->db->where_not_in('viewcreationcolumns.viewcreationcolumnid',$this->Basefunctions->restrictviewcreationcolids);
			}
			$this->db->where('viewcreationcolumns.status',1);
			$this->db->where('moduleinfo.status',1);
			if($moduletabsecid != 1) {
				$this->db->where('viewcreationcolumns.moduletabsectionid',$moduletabsecid);
			}
			$this->db->where_in('viewcreationcolumns.uitypeid',$uitype);
			$this->db->where_in('viewcreationcolumns.viewcreationmoduleid',$moduleid[$k]);
			$this->db->where("FIND_IN_SET('$industryid',module.industryid) >", 0);
			$this->db->group_by("module.moduleid");
			$this->db->group_by("viewcreationcolumns.viewcreationcolumnid");
			$result = $this->db->get();
			if($result->num_rows() > 0) {
				foreach($result->result() as $row) {
					if(!in_array($row->viewcreationcolmodelindexname,$colname)) {
						$data[$i] = array('columnaname'=>$row->viewcreationcolmodelindexname,'fieldlabel'=>$row->viewcreationcolumnname,'parenttable'=>$row->viewcreationparenttable,'multiple'=>$row->multiple,'uitypeid'=>$row->uitypeid,'moduletabid'=>$row->viewcreationmoduleid,'modulefieldid'=>$row->viewcreationcolumnid,'modulename'=>$row->modulename);
						$colname[$i] = $row->viewcreationcolmodelindexname;
						$i++;
					}
				}
			}
		}
		return $data;
	}
	//display all dropdowns
	public function showalldropdowns($moduledd) {
		$content = '';
		$multattr = '';
		$tabindex = 10000;
		$i=0;
		$data = array('content'=>$content,'length'=>$i);
		foreach($moduledd as $row) {
			$multiple = $row['multiple'];
			$moduleid = $row['moduletabid'];
			$modulefieldid = $row['modulefieldid'];
			$tablename = substr($row['columnaname'],0,-4);
			$fieldprimaryid = $tablename.'id';
			$tabfieldname = $tablename.'name';
			$multattr = 'multiple=multple';
			if($row['uitypeid'] == 20){
				$mandlab = "";
				$txtval="";
				$mandatoryopt = ""; //mandatory option set
				$divattr = array('class'=>'elementlist large-12 columns static-field','id'=>'');
				$labval = $row['fieldlabel'].$mandlab;
				$labattr = array();
				$name = 'reportddfilerchange'.$i;
				$value = array();
				$defval = array();
				$value = array();
				$dataval = array();
				$groupid = array();
				$ddowndata = $this->Basefunctions->userspecificgroupdropdownvalfetch();
				$prev = ' ';
				$grpname = '';
				for ($k=0;$k<count($ddowndata);$k++) {
					$cur = $ddowndata[$k]['PId'];
					$a ="";
					if($prev != $cur) {
						if( $prev != '' ) {
							$value[$grpname] = $dataval;
						}
						$dataval = array();
						$dataval[$ddowndata[$k]['CId']] = $ddowndata[$k]['CName'];
						$groupid[] = $ddowndata[$k]['PId'];
						$prev = $ddowndata[$k]['PId'];
						$grpname = $ddowndata[$k]['PName'];
					} else {
						$dataval[$ddowndata[$k]['CId']] = $ddowndata[$k]['CName'];
						$groupid[] = $ddowndata[$k]['PId'];
					}
				}
				$value[$grpname] = $dataval;
				$txtval = ( ($txtval!='')? $txtval : 1 );
				$valoption = "id";
				$dataattr = array();
				$multiple = '';
				$seldataattr = array("placeholder"=>"",'defvalattr'=>''.$txtval.'','modulefieldid'=>''.$row['modulefieldid'].'','uitype'=>''.$row['uitypeid'].'');
				$option = array("class"=>"chzn-select reportddfilerchange ".$mandatoryopt."","tabindex"=>"".$tabindex."","data"=>$seldataattr,'data-prompt-position'=>'bottomLeft:14,36');
				$hidname = '';
				$hidval = '';
				$content .= groupdropdownspanuserspecific($divattr,$labval,$labattr,$name,$value,$valoption,$dataattr,$multiple,$option,$groupid,$hidname);
			} else if($row['uitypeid'] == 13) {
				$content .= '<div class="input-field large-12 medium-12 small-12 columns" >';
				$content .= '<input type="checkbox" name="reportddfilerchange'.$i.'" id="reportddfilerchange'.$i.'" data-table="'.$tablename.'"  data-hidname="checkbox'.$i.'"  data-fieldname="'.$tabfieldname.'" data-fieldid="'.$fieldprimaryid.'" data-modulefieldid="'.$row['modulefieldid'].'" data-uitype="'.$row['uitypeid'].'" class="reportddfilerchange checkboxcls radiochecksize filled-in" tabindex="'.$tabindex.'" /><label for="ddfilerchange'.$i.'">'.$row['fieldlabel'].'('.$row['modulename'].')</label><input name="checkbox'.$i.'" id="checkbox'.$i.'" value="No" data-defvalattr="No" type="hidden">';
				$content .= '</div>';
			} else {
				$content .= '<div class="elementlist large-12 medium-12 small-12 columns static-field" ><div class="" id=""><label>'.$row['fieldlabel'].'('.$row['modulename'].')</label><select data-placeholder="" data-prompt-position="topLeft:14,36" id="reportddfilerchange'.$i.'" name="reportddfilerchange'.$i.'" data-table="'.$tablename.'"  data-fieldname="'.$tabfieldname.'" data-fieldid="'.$fieldprimaryid.'" data-modulefieldid="'.$row['modulefieldid'].'" data-uitype="'.$row['uitypeid'].'" class="chzn-select reportddfilerchange" tabindex="'.$tabindex.'" '.$multattr.'><option></option>';
				if($row['uitypeid'] == 17 || $row['uitypeid'] == 18 || $row['uitypeid'] == 28 || $row['uitypeid'] == 25) {
					$dddata = $this->Basefunctions->dynamicdropdownvalfetch($moduleid,$tablename,$tabfieldname,$fieldprimaryid);
					foreach($dddata as $key) {
						$content .= '<option value="'.$key->Id.'" data-id"'.$key->Id.'" data-name"'.$key->Name.'">'.$key->Name.'</option>';
					}
				} else if($row['uitypeid'] == 19 || $row['uitypeid'] == 21) {
					if($modulefieldid == 136) { //for size master dd
						$dddata = $this->Basefunctions->sizemasterdisplay($tablename,$tabfieldname,$fieldprimaryid,'');
						foreach($dddata as $key) {
							$content .= '<option value="'.$key->Id.'" data-id"'.$key->Id.'" data-name"'.$key->Name.'">'.$key->productname.' - '.$key->Name.'</option>';
						}
					} else if($modulefieldid == 148) {
						$dddata = $this->Basefunctions->vendornamedisplay($tablename,$tabfieldname,$fieldprimaryid,'');
						foreach($dddata as $key) {
							$content .= '<option value="'.$key->Id.'" data-id"'.$key->Id.'" >'.$key->Name.'</option>';
						}
					} else {
						$dddata = $this->Basefunctions->dynamicdropdownvalfetchparent($tablename,$tabfieldname,$fieldprimaryid,'');
						foreach($dddata as $key) {
							$content .= '<option value="'.$key->Id.'" data-id"'.$key->Id.'" data-name"'.$key->Name.'">'.$key->Name.'</option>';
						}
					}
				} else if($row['uitypeid'] == 27) {
					$dddata = $this->Basefunctions->dynamicmoduledropdownvalfetch($moduleid);
					foreach($dddata as $key) {
						$content .= '<option value="'.$key->Id.'" data-id"'.$key->Id.'" data-name"'.$key->Name.'">'.$key->Name.'</option>';
					}
				}
				$content .='</select></div></div>';
			}
			$tabindex++;
			$i++;
		}
		$data = array('content'=>$content,'length'=>$i);
		return $data;
	}
	public function filterdataviewdropdowncolumns($moduleids,$moduletabsecid) {
		/*Checking the given moduleid exits in moduletabsection table.
		 1. If available means join with moduletabsection and get viewcreationcolumnicon values with tabsection name.
		 2. If not available means, remove moduletabsection join and get only viewcreationcolumnicon values.
		 */
		$industryid = $this->Basefunctions->industryid;
		$query=$this->Basefunctions->checkthemoduleexistinmoduletabsec($moduleids);
		if($query->available) {
			$this->db->select('viewcreationcolumns.viewcreationcolumnid,viewcreationcolumns.viewcreationcolumnname,moduletabsection.moduletabsectionname,viewcreationcolumns.moduletabsectionid,viewcreationcolumns.uitypeid,viewcreationcolumns.fieldrestrict,viewcreationcolumns.fieldview,viewcreationcolumns.viewcreationcolmodelindexname,uitype.aggregatemethodid,module.modulename,viewcreationcolumns.viewcreationmoduleid',false);
			$this->db->from('viewcreationcolumns');
			$this->db->join('viewcreationmodule','viewcreationmodule.viewcreationmoduleid=viewcreationcolumns.viewcreationmoduleid','left outer');
			$this->db->join('moduletabsection','moduletabsection.moduletabsectionid=viewcreationcolumns.moduletabsectionid','left outer');
			$this->db->join('moduleinfo','moduleinfo.moduleid=moduletabsection.moduleid','left outer');
			$this->db->join('module','module.moduleid=moduleinfo.moduleid','left outer');
			$this->db->join('uitype','viewcreationcolumns.uitypeid=uitype.uitypeid','left outer');
			if($moduletabsecid != 1) {
				$this->db->where('viewcreationcolumns.moduletabsectionid',$moduletabsecid);
			}
			$this->db->where_in('viewcreationcolumns.viewcreationmoduleid',$moduleids);
			$this->db->where('viewcreationcolumns.status',$this->Basefunctions->activestatus);
			$this->db->where('moduleinfo.userroleid',$this->Basefunctions->userroleid);
			$this->db->where('moduleinfo.status',$this->Basefunctions->activestatus);
			$this->db->where("FIND_IN_SET('$industryid',module.industryid) >", 0);
			$this->db->where_not_in('viewcreationcolumns.fieldrestrict',array(1));
			$this->db->where_not_in('uitype.uitypeid',array(17,18,19,20,21,23,25,26,27,28,29,13));
			$this->db->where_not_in('viewcreationcolumns.viewcreationcolumnviewtype',array(0));
			$this->db->order_by('moduletabsection.moduletabsectionid',"ASC");
			$this->db->order_by('viewcreationcolumns.viewcreationcolumnid',"ASC");
			$this->db->order_by('module.moduleid',"DESC");
			$query=$this->db->get();
			return  $query->result();
		} else {
			$this->db->select('viewcreationcolumns.viewcreationcolumnid,viewcreationcolumns.viewcreationcolumnname,viewcreationcolumns.moduletabsectionid,viewcreationcolumns.uitypeid,viewcreationcolumns.fieldrestrict,viewcreationcolumns.fieldview,viewcreationcolumns.viewcreationcolmodelindexname,uitype.aggregatemethodid',false);
			$this->db->from('viewcreationcolumns');
			$this->db->join('viewcreation','viewcreation.viewcreationmoduleid=viewcreationcolumns.viewcreationmoduleid','left outer');
			$this->db->join('viewcreationmodule','viewcreationmodule.viewcreationmoduleid=viewcreation.viewcreationmoduleid','left outer');
			$this->db->join('uitype','viewcreationcolumns.uitypeid=uitype.uitypeid','left outer');
			$this->db->where_in('viewcreationcolumns.viewcreationmoduleid',$moduleids);
			$this->db->where('viewcreationcolumns.status',$this->Basefunctions->activestatus);
			$this->db->where_not_in('viewcreationcolumns.fieldrestrict',array(1));
			$this->db->where_not_in('uitype.uitypeid',array(17,18,19,20,21,23,25,26,27,28,29,13));
			$this->db->where_not_in('viewcreationcolumns.viewcreationcolumnviewtype',array(0));
			$this->db->order_by('viewcreationcolumns.viewcreationcolumnid',"ASC");
			//$this->db->order_by('module.moduleid',"DESC");
			$query=$this->db->get();
			return  $query->result();
		}
	}
	//modulebased tab section get 
	public function loadmoduletabsctionmodel() {
		$i=0;
		$moduleid = $_GET['moduleid'];
		$this->db->select('moduletabsectionid,moduletabsectionname');
		$this->db->from('moduletabsection');
		$this->db->where('moduletabsection.moduleid',$moduleid);
		$this->db->where('moduletabsection.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('id'=>$row->moduletabsectionid,'name'=>$row->moduletabsectionname);
				$i++;
			}
		}
		echo json_encode($data);
	}
	public function repordataviewdropdowncolumns($moduleids,$tabsction) {
		/*Checking the given moduleid exits in moduletabsection table.
		 1. If available means join with moduletabsection and get viewcreationcolumnicon values with tabsection name.
		 2. If not available means, remove moduletabsection join and get only viewcreationcolumnicon values.
			*/
		$industryid = $this->Basefunctions->industryid;
		$query=$this->Basefunctions->checkthemoduleexistinmoduletabsec($moduleids);
		if($query->available) {
			$this->db->select('viewcreationcolumns.viewcreationcolumnid,viewcreationcolumns.viewcreationcolumnname,moduletabsection.moduletabsectionname,viewcreationcolumns.moduletabsectionid,viewcreationcolumns.uitypeid,viewcreationcolumns.fieldrestrict,viewcreationcolumns.fieldview,viewcreationcolumns.viewcreationcolmodelindexname,uitype.aggregatemethodid,module.modulename,viewcreationcolumns.viewcreationmoduleid,viewcreationcolumns.viewcreationcolmodelname',false);
			$this->db->from('viewcreationcolumns');
			$this->db->join('viewcreationmodule','viewcreationmodule.viewcreationmoduleid=viewcreationcolumns.viewcreationmoduleid','left outer');
			$this->db->join('moduletabsection','moduletabsection.moduletabsectionid=viewcreationcolumns.moduletabsectionid','left outer');
			$this->db->join('moduleinfo','moduleinfo.moduleid=moduletabsection.moduleid','left outer');
			$this->db->join('module','module.moduleid=moduleinfo.moduleid','left outer');
			$this->db->join('uitype','viewcreationcolumns.uitypeid=uitype.uitypeid','left outer');
			$this->db->where_in('viewcreationcolumns.viewcreationmoduleid',$moduleids);
			if($tabsction != '1'){
				$this->db->where_in('viewcreationcolumns.moduletabsectionid',$tabsction);
			}
			$this->db->where('viewcreationcolumns.status',$this->Basefunctions->activestatus);
			$this->db->where('moduleinfo.status',$this->Basefunctions->activestatus);
			$this->db->where("FIND_IN_SET('$industryid',module.industryid) >", 0);
			$this->db->where_not_in('viewcreationcolumns.fieldrestrict',array(1));
			//$this->db->where_not_in('viewcreationcolumns.selecttype',array(5));
			$this->db->where_not_in('viewcreationcolumns.viewcreationcolumnviewtype',array(0));
			$this->db->order_by('moduletabsection.moduletabsectionid',"ASC");
			$this->db->group_by('viewcreationcolumns.viewcreationcolumnid',"ASC");
			$query=$this->db->get();
			return  $query->result();
		} else {
			$this->db->select('viewcreationcolumns.viewcreationcolumnid,viewcreationcolumns.viewcreationcolumnname,viewcreationcolumns.moduletabsectionid,viewcreationcolumns.uitypeid,viewcreationcolumns.fieldrestrict,viewcreationcolumns.fieldview,viewcreationcolumns.viewcreationcolmodelindexname,uitype.aggregatemethodid,viewcreationcolumns.viewcreationmoduleid,viewcreationcolumns.viewcreationcolmodelname',false);
			$this->db->from('viewcreationcolumns');
			$this->db->join('viewcreation','viewcreation.viewcreationmoduleid=viewcreationcolumns.viewcreationmoduleid','left outer');
			$this->db->join('viewcreationmodule','viewcreationmodule.viewcreationmoduleid=viewcreation.viewcreationmoduleid','left outer');
			$this->db->join('uitype','viewcreationcolumns.uitypeid=uitype.uitypeid','left outer');
			$this->db->where_in('viewcreationcolumns.viewcreationmoduleid',$moduleids);
			$this->db->where('viewcreationcolumns.status',$this->Basefunctions->activestatus);
			$this->db->where_not_in('viewcreationcolumns.fieldrestrict',array(1));
			$this->db->where_not_in('viewcreationcolumns.selecttype',array(5));
			$this->db->where_not_in('viewcreationcolumns.viewcreationcolumnviewtype',array(0));
			$this->db->order_by('viewcreationcolumns.viewcreationcolumnid',"ASC");
			$query=$this->db->get();
			return  $query->result();
		}
	}
	// Retrieve Date Value - from date and to date
	public function retrievedatevaluehtmlreport() {
		$current_date = date("Y-m-d");
		$startdate = "";
		$enddate = "";
		$htmlfieldcondition = $_GET['htmlfieldcondition'];
		switch($htmlfieldcondition) {
			case "Yesterday":
				$date_value = $this->Basefunctions->getdatevalue('-1 day',$current_date);
				$startdate = $date_value['start'];
				$enddate = $date_value['start'];
				break;
			case "Today":
				$date_value = $this->Basefunctions->getdatevalue('0 day',$current_date);
				$startdate = $date_value['start'];
				$enddate = $date_value['start'];
				break;
			case "Tomorrow":
				$date_value = $this->Basefunctions->getdatevalue('1 day',$current_date);
				$startdate = $date_value['start'];
				$enddate = $date_value['start'];
				break;
			case "Last 7 Days":
				$f_date = strtotime('-6 day' ,strtotime($current_date));
				$f_date = date("Y-m-d",$f_date);
				if(strtotime($f_date) > strtotime($current_date)) {
					$startdate = $current_date;
					$enddate = $f_date;
				} else {
					$startdate = $f_date;
					$enddate = $current_date;
				}
				break;
			case "Last 30 Days":
				$f_date = strtotime('-29 day' ,strtotime($current_date));
				$f_date = date("Y-m-d",$f_date);
				if(strtotime($f_date) > strtotime($current_date)) {
					$startdate = $current_date;
					$enddate = $f_date;
				} else {
					$startdate = $f_date;
					$enddate = $current_date;
				}
				break;
			case "Last 60 Days":
				$f_date = strtotime('-59 day' ,strtotime($current_date));
				$f_date = date("Y-m-d",$f_date);
				if(strtotime($f_date) > strtotime($current_date)) {
					$startdate = $current_date;
					$enddate = $f_date;
				} else {
					$startdate = $f_date;
					$enddate = $current_date;
				}
				break;
			case "Last 90 Days":
				$f_date = strtotime('-89 day' ,strtotime($current_date));
				$f_date = date("Y-m-d",$f_date);
				if(strtotime($f_date) > strtotime($current_date)) {
					$startdate = $current_date;
					$enddate = $f_date;
				} else {
					$startdate = $f_date;
					$enddate = $current_date;
				}
				break;
			case "Last 120 Days":
				$f_date = strtotime('-119 day' ,strtotime($current_date));
				$f_date = date("Y-m-d",$f_date);
				if(strtotime($f_date) > strtotime($current_date)) {
					$startdate=$current_date;
					$enddate=$f_date;
				} else {
					$startdate=$f_date;
					$enddate=$current_date;
				}
				break;
			case "Next 7 Days":
				$f_date = strtotime('6 day' ,strtotime($current_date));
				$f_date = date("Y-m-d",$f_date);
				if(strtotime($f_date) > strtotime($current_date)) {
					$startdate = $current_date;
					$enddate = $f_date;
				} else {
					$startdate = $f_date;
					$enddate = $current_date;
				}
				break;
			case "Next 30 Days":
				$f_date = strtotime('29 day' ,strtotime($current_date));
				$f_date = date("Y-m-d",$f_date);
				if(strtotime($f_date) > strtotime($current_date)) {
					$startdate = $current_date;
					$enddate = $f_date;
				} else {
					$startdate = $f_date;
					$enddate = $current_date;
				}
				break;
			case "Next 60 Days":
				$f_date = strtotime('59 day' ,strtotime($current_date));
				$f_date = date("Y-m-d",$f_date);
				if(strtotime($f_date) > strtotime($current_date)) {
					$startdate = $current_date;
					$enddate = $f_date;
				} else {
					$startdate = $f_date;
					$enddate = $current_date;
				}
				break;
			case "Next 90 Days":
				$f_date = strtotime('89 day' ,strtotime($current_date));
				$f_date = date("Y-m-d",$f_date);
				if(strtotime($f_date) > strtotime($current_date)) {
					$startdate = $current_date;
					$enddate = $f_date;
				} else {
					$startdate = $f_date;
					$enddate = $current_date;
				}
				break;
			case "Next 120 Days":
				$f_date = strtotime('119 day' ,strtotime($current_date));
				$f_date = date("Y-m-d",$f_date);
				if(strtotime($f_date) > strtotime($current_date)) {
					$startdate = $current_date;
					$enddate = $f_date;
				} else {
					$startdate = $f_date;
					$enddate = $current_date;
				}
				break;
			case "Last Week":
				$date_value = $this->Basefunctions->datesystem('lastweek',$current_date);
				$startdate = $date_value['start'];
				$enddate = $date_value['end'];
				break;
			case "Current Week":
				$date_value = $this->Basefunctions->datesystem('currentweek',$current_date);
				$startdate = $date_value['start'];
				$enddate = $date_value['end'];
				break;
			case "Next Week":
				$date_value = $this->Basefunctions->datesystem('nextweek',$current_date);
				$startdate = $date_value['start'];
				$enddate = $date_value['end'];
				break;
			case "Last Month":
				$date_value = $this->Basefunctions->datesystem('lastmonth',$current_date);
				$startdate = $date_value['start'];
				$enddate = $date_value['end'];
				break;
			case "Current Month":
				$date_value = $this->Basefunctions->datesystem('currentmonth',$current_date);
				$startdate = $date_value['start'];
				$enddate = $date_value['end'];
				break;
			case "Next Month":
				$date_value = $this->Basefunctions->datesystem('nextmonth',$current_date);
				$startdate = $date_value['start'];
				$enddate = $date_value['end'];
				break;
			case "Current and Previous Month":
				$date_value = $this->Basefunctions->datesystem('currentandpreviousmonth',$current_date);
				$startdate = $date_value['start'];
				$enddate = $date_value['end'];
				break;
			case "Current and Next Month":
				$date_value = $this->Basefunctions->datesystem('currentandnextmonth',$current_date);
				$startdate = $date_value['start'];
				$enddate = $date_value['end'];
				break;
			case "Before 7 Days":
				$f_date = strtotime('-6 day' ,strtotime($current_date));
				$f_date = date("Y-m-d",$f_date);
				if(strtotime($f_date) > strtotime($current_date)) {
					$startdate = $current_date;
					$enddate = $f_date;
				} else {
					$startdate = $f_date;
					$enddate = $current_date;
				}
				break;
			case "Before 30 Days":
				$f_date = strtotime('-29 day' ,strtotime($current_date));
				$f_date = date("Y-m-d",$f_date);
				if(strtotime($f_date) > strtotime($current_date)) {
					$startdate = $current_date;
					$enddate = $f_date;
				} else {
					$startdate = $f_date;
					$enddate = $current_date;
				}
				break;
			case "Before 60 Days":
				$f_date = strtotime('-59 day' ,strtotime($current_date));
				$f_date = date("Y-m-d",$f_date);
				if(strtotime($f_date) > strtotime($current_date)) {
					$startdate = $current_date;
					$enddate = $f_date;
				} else {
					$startdate = $f_date;
					$enddate = $current_date;
				}
				break;
			case "Before 90 Days":
				$f_date = strtotime('-89 day' ,strtotime($current_date));
				$f_date = date("Y-m-d",$f_date);
				if(strtotime($f_date) > strtotime($current_date)) {
					$startdate = $current_date;
					$enddate = $f_date;
				} else {
					$startdate = $f_date;
					$enddate = $current_date;
				}
				break;
			case "Before 120 Days":
				$f_date = strtotime('-119 day' ,strtotime($current_date));
				$f_date = date("Y-m-d",$f_date);
				if(strtotime($f_date) > strtotime($current_date)) {
					$startdate = $current_date;
					$enddate = $f_date;
				} else {
					$startdate = $f_date;
					$enddate = $current_date;
				}
				break;
			default:
				$startdate = "";
				$enddate = "";
				break;
		}
		echo json_encode(array($startdate,$enddate));
	}
	// Sales Detail - Image display
	public function retrievesalesdetailimage() {
		$tagimage = array();
		$salesdetailid = $_POST['salesdetailid'];
		$salesid = $_POST['salesid'];
		$this->db->select('salesdetail.tagimage');
		$this->db->from('salesdetail');
		if($salesdetailid != '1') {
			$this->db->where('salesdetail.salesdetailid',$salesdetailid);
		} else {
			$this->db->where('salesdetail.salesid',$salesid);
		}
		$this->db->where_not_in('salesdetail.status',array(0,3));
		$data = $this->db->get();
		if($data->num_rows() > 0) {
			foreach($data->result() as $row) {
				if($row->tagimage != '') {
					$tagimage[] = $row->tagimage;
				}
			}
		}
		if(empty($tagimage)) {
			$tagimage = 'FAILED';
		}
		echo json_encode($tagimage);
	}
	// Stock Tag Image display
	public function retrievestocktagimage() {
		$tagimage = array();
		$stocktagid = $_POST['stocktagid'];
		if(isset($_POST['typename'])) {
			if($_POST['typename'] == 'itemtag') {
				$stocktagid = $_POST['stocktagid'];
			} else if($_POST['typename'] == 'salesdetailid') {
				$stocktagid = $this->Basefunctions->singlefieldfetch('itemtagid','salesdetailid','salesdetail',$_POST['stocktagid']);
			}
		}
		$rettagimage = $this->Basefunctions->singlefieldfetch('tagimage','itemtagid','itemtag',$stocktagid);
		if($rettagimage != '') {
			$this->db->select('itemtag.tagimage');
			$this->db->from('itemtag');
			$this->db->where('itemtag.itemtagid',$stocktagid);
			$this->db->where_not_in('itemtag.status',array(0,3));
			$data = $this->db->get();
			if($data->num_rows() > 0) {
				foreach($data->result() as $row) {
					if($row->tagimage != '') {
						$tagimage[] = $row->tagimage;
					}
				}
			}
		} else {
			$tagimage = 'FAILED';
		}
		echo json_encode($tagimage);
	}
}