<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Reportsview extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('formbuild');
        $this->load->model('Reportsview/Reportsmodel'); //mainreports
		$this->load->model('Base/Basefunctions');
		$this->load->library('excel');
    }
    //first basic hitting view
    public function index() {
		$moduleid = array(35);
		sessionchecker($moduleid);
		$data['moduleids'] = $moduleid;
		$data['filedmodids'] = $moduleid;
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//load dropdown data related to reportform
		$data['reporttype']=$this->Basefunctions->simpledropdown('reporttype','reporttypeid,reporttypename','reporttypeid');
		$data['module']=$this->Reportsmodel->reportmodeldd();
		$data['frequency']=$this->Basefunctions->simpledropdown('recurrencefreq','recurrencefreqid,recurrencefreqname','recurrencefreqname');
		$data['filetype']=$this->Basefunctions->simpledropdown('filetype','filetypeid,filetypename','filetypename');
		$data['employee']=$this->Basefunctions->simpledropdown('employee','employeeid,employeename','employeename');
		$data['folderdata']=$this->Reportsmodel->folderloadbydropdown();
		$data['account'] = $this->Basefunctions->accountgroup_dd('16,6');
		$this->load->view('Reportsview/reportsview',$data); 
    }
	
	//viewreportfolder
	public function viewreportfolder() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'reportfolder.reportfolderid') : 'reportfolder.reportfolderid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'ASC') : 'ASC';
		$colinfo = array('colmodelname'=>array('reportfoldername','description','setaspublic','setdefault'),'colmodelindex'=>array('reportfoldername','description','setaspublic','setdefault'),'coltablename'=>array('reportfolder','reportfolder','reportfolder','reportfolder'),'uitype'=>array('2','2','2','2'),'colname'=>array('Folder Name','Description','Public','Default'),'colsize'=>array('200','200','200','200'));
		$result=$this->Reportsmodel->viewreportfolder($primarytable,$sortcol,$sortord,$pagenum,$rowscount);		
		$device = $this->Basefunctions->deviceinfo();
	//	if($device=='phone') {
		//	$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Report List',$width,$height);
		//} else {
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Report List',$width,$height);
		//}
		echo json_encode($datas);
	}
	//view report list
	public function viewreportlist() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'report.reportid') : 'report.reportid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('reportname','parentmodulename','menuname','reportdescription'),'colmodelindex'=>array('report.reportname','parentmodule.modulename','module.menuname','report.reportdescription'),'coltablename'=>array('report','parentmodule','module','report'),'uitype'=>array('2','2','2','2'),'colname'=>array('Report Name','Menu Category','Module Name','Description'),'colsize'=>array('100','100','100','100'));
		$result=$this->Reportsmodel->reportlist($primarytable,$sortcol,$sortord,$pagenum,$rowscount);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$data=array();
		$i=0;
		foreach($result->result() as $row) {
			$reportname = '<a href="" onClick="reportopenclick('.$row->reportid.'); return false;" >'.$row->reportname.'</a>';
			$data[$i]=array('id'=>$row->reportid,'reportfoldername'=>$row->reportfoldername,$reportname,$row->parentmodulename,$row->menuname,$row->reportdescription);
			$i++;
		}
		$finalresult=array($data,$page,'jsongroup','reportfoldername');
		$device = $this->Basefunctions->deviceinfo();
		//if($device=='phone') {
			//$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Report View',$width,$height);
		//} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Report View',$width,$height);
		//}
		echo json_encode($datas);
	}
	//createreportfolder
	public function createreportfolder() {
		$this->Reportsmodel->createreportfolder();
	}
	public function getreportdetails(){
		$value = $this->Reportsmodel->getreportdetailsmodel($_GET['reportid']);
	}
	//updatereportfolder
	public function updatereportfolder() {
		$this->Reportsmodel->updatereportfolder();
	}
	//data row sorting
	public function datarowsorting() {
		$this->Reportsmodel->datarowsortingmodel();
	}
	//load-reportfoldername
	public function loadreportfoldername() {
		$this->Reportsmodel->loadreportfoldername();
	}
	//delete report-
	public function reportdelete() {
		$this->Reportsmodel->reportdelete();
	}
	///report data clone
	public function reportdataclone() {
		$empid = $this->Basefunctions->userid;
		$vmaxid = $_POST['reportid'];
		$getreportinfo=$this->Reportsmodel->getreportinfo($_POST['reportid']);
		$moduleid = $getreportinfo['moduleid'];
		$parenttable = 'report';
		$childtabinfo = 'reportcondition,reportcondition';
		$fieldinfo = 'reportid';
		$fieldvalue = $vmaxid;
		$updatecloumn = 'reportname';
		$maxreportid = $this->Basefunctions->maximumid($parenttable,$fieldinfo);
		$maxreportid = $maxreportid + 1;
		$viewid = $this->Basefunctions->viewdataclonemodel($parenttable,$childtabinfo,$fieldinfo,$fieldvalue);
		if($updatecloumn!='') {
			$this->db->query(' UPDATE '.$parenttable.' SET '.$updatecloumn.' = concat('.$updatecloumn.'," '.$maxreportid.' ") where '.$fieldinfo.' = '.$viewid.'');
		}
		echo 'TRUE';
	}	
	///this function retrieves the primary id of that selected values
	public function getdestinationrowid() {
		$this->Reportsmodel->getdestinationrowid();
	}
	/* Fetch Main grid header,data,footer information */
	public function reportgridinformationfetch(){
		$creationid = $_GET['reportid'];
		$reporttype=$_GET['reporttype'];
		$getreportinfo=$this->Reportsmodel->getreportinfo($creationid);
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$footer = isset($_GET['footername']) ? $_GET['footername'] : '';
		$_GET['parentid']=$getreportinfo['modulemastertable'].'id';
		$_GET['viewfieldids']=$getreportinfo['viewcreationcolumnid'];
		$_GET['maintabinfo']=$getreportinfo['modulemastertable'];
		$rowid = $_GET['parentid'];
		$viewcolmoduleids = $getreportinfo['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? $_GET['sortcol'] : '';
		$sortord = isset($_GET['sortord']) ? $_GET['sortord'] : '';
		$result = $this->Basefunctions->reportdynamicdatainfofetch($creationid,$pagenum,$rowscount,$sortcol,$sortord,$rowid,$viewcolmoduleids,$reporttype);
		$device = $this->Basefunctions->deviceinfo();
	//	if($device=='phone') {
		//	$datas = mobileviewgriddatagenerate($result,$primarytable,$primaryid,$width,$height);
		//} else {
			$datas = reportgriddatagenerate($result,$_GET['parentid'],$rowid,$width,$height,$footer);
		//}
	    echo json_encode($datas);
	}
	//deletemptyreportfolder
	public function deletemptyreportfolder() {
		$this->Reportsmodel->deletemptyreportfolder();
	}
	//deletereportfolder
	public function deletereportfolder() {
		$this->Reportsmodel->deletereportfolder();
	}
	//fetch delete folder DD Value
	public function deletefolderddvalfetch() {
		$this->Reportsmodel->deletefolderddvalfetch();
	}
	//checkfolderhavereports
	public function checkfolderhavereports() {
		$this->Reportsmodel->checkfolderhavereports();
	}
	/* Fetch Main grid header,data,footer information */
	public function generatereport(){
		$viewcolids=$_GET['viewcolids'];
		$tabularviewcolids=$_GET['tabularviewcolids'];
		$summarviewcolids=$_GET['summarviewcolids'];
		$summaramids=$_GET['summaramids'];
		$groupcolids=$_GET['groupcolids'];
		$sortby=$_GET['sortby'];
		$groupbyrange=$_GET['groupbyrange'];
		$reportconditiondata=$_GET['reportconditiondata'];
		$reportbasicdetails=$_GET['reportbasicdetails'];
		$date_columnid=$_GET['date_columnid'];
		$date_method=$_GET['date_method'];
		$date_start=$_GET['date_start'];
		$date_end=$_GET['date_end'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['wwidth'];
		$height = $_GET['wheight'];
		$reportmodulesectionid = isset($_GET['reportmoduletab']) ? $_GET['reportmoduletab'] : '1';
		$getreportmoduleinfo=$this->Reportsmodel->getreportmoduleinfo($_GET['reportmodule']);
		$_GET['parentid']=$getreportmoduleinfo['modulemastertable'].'id';
		$_GET['viewfieldids']=$viewcolids;
		$_GET['maintabinfo']=$getreportmoduleinfo['modulemastertable'];
		$rowid = $_GET['parentid'];
		$viewcolmoduleids = $getreportmoduleinfo['moduleid'];
		$reporttype=$_GET['rftype'];
		$reportname=$_GET['reportname'];
		$formattype=$_GET['formattype'];
		$filetypename = $_GET['filetypename'];
		$sortcol = isset($_GET['sortcol']) ? $_GET['sortcol'] : '';
		$sortord = isset($_GET['sortord']) ? $_GET['sortord'] : '';
		$footer = isset($_GET['footername']) ? $_GET['footername'] : 'empty';
		$filterid = isset($_GET['filter']) ? $_GET['filter'] : '';
		$conditionname = isset($_GET['conditionname']) ? $_GET['conditionname'] : '';
		$filtervalue = isset($_GET['filtervalue']) ? $_GET['filtervalue'] : '';
		$filter = array('0'=>$filterid,'1'=>$conditionname,'2'=>$filtervalue);
		//non-base module id and table name set
		if($viewcolmoduleids == 49 || $viewcolmoduleids == 41) {
			$maintable = $this->Basefunctions->fetchmodueltabsectiotable($reportmodulesectionid);
			$rowid = $maintable.'id';
		} else {
			$maintable = $_GET['maintabinfo'];
			$rowid = $rowid;
		}
		$parentid = $rowid;
		$result = $this->Basefunctions->generatereport($viewcolids,$pagenum,$rowscount,$sortcol,$sortord,$rowid,$viewcolmoduleids,$reporttype,$filter,$filetypename,$reportmodulesectionid,$reportname);
		$device = $this->Basefunctions->deviceinfo();
		$reprotfilename = $result[9];
		/* if($device=='phone') {
			$datas = mobileviewgriddatagenerate($result,$primarytable,$primaryid,$width,$height);
		} else { */
			$datas = reportgriddatagenerate($result,$parentid,$rowid,$width,$height,$footer);
		//} 
	    echo json_encode(array('rdata'=>$datas,'filename'=>$reprotfilename));
	}
	//create report-
	public function savereporttodb() {
		$this->Reportsmodel->savereporttodb();
	}
	public function reportfetchdataset() {
		$this->Reportsmodel->reportfetchdataset();
	}
	//retrieve => Report Data
	public function newretrievereport() {
		$this->Reportsmodel->newretrievereport();
	}
	//excel Download report
	public function newexceldataexport() {			
		$this->Reportsmodel->newexceldataexportmodel();
	}
	//PDF Download report
	public function newpdfdataexport() {			
		$this->Reportsmodel->newpdfdataexportmodel();
	}
	/* Code Cleaning*/
	//load-Module name based on Menu Category.
	public function loadmodulenamebasedcategory() {
		$this->Reportsmodel->loadmodulenamebasedcategory();
	}	
	// Load - Condition based on UItype	
	public function loadconditionbyuitype() {
		$this->Reportsmodel->loadconditionbyuitype();
	}
	// Load - Aggregate Method based on UItype	
	public function loadaggregatemethodbyuitype() {
		$this->Reportsmodel->loadaggregatemethodbyuitype();
	}
	//set report column with grouping-
	public function setreportcolumndata() {
		$viewfieldsmoduleids=array_filter(explode(',',$_GET['moduleid']));
		$reportcolumndata = '';
		$tabsction=$_GET['tabsction'];
		$colname = array();
		$i=0;
		foreach($viewfieldsmoduleids as $viewfieldsmoduleid) {
			$data=$this->Reportsmodel->repordataviewdropdowncolumns($viewfieldsmoduleid,$tabsction);
			foreach($data as $key) {
				/*if(!in_array($key->viewcreationcolmodelname,$colname)) {
					 if($key->viewcreationcolumnid != 4561 || $key->viewcreationcolumnid != 4582) {
						$colname[$i] = $key->viewcreationcolmodelname;
					} */
					$amid=explode(",",$key->aggregatemethodid);
					$this->db->select('GROUP_CONCAT(aggregatemethodname) as aggregatemethodname,GROUP_CONCAT(aggregatemethodkey) as aggregatemethodkey',false);
					$this->db->from('aggregatemethod');
					$this->db->where_in('aggregatemethodid',$amid);
					$this->db->where('status',$this->Basefunctions->activestatus);
					$sumval=$this->db->get()->row();
					echo  '<option data-aggregatemethodkey='.$sumval->aggregatemethodkey.' data-aggregatemethodid='.$key->aggregatemethodid.' data-applysummary="" data-selecttype="" data-selecttypename="" data-uitype='.$key->uitypeid .'   value=' . $key->viewcreationcolumnid . '>	'.$key->viewcreationcolumnname .'('.$key->modulename.')</option>';
				//}
				//$i++;
			}
		}
	}
	//load-columnname
	public function loadcolumnname(){
		$viewfieldsmoduleids =array_filter(explode(',',$_GET['moduleid']));
		$tabsction = isset($_GET['tabsction'])? $_GET['tabsction'] : 1;
		$data=$this->Reportsmodel->repordataviewdropdowncolumns($viewfieldsmoduleids,$tabsction);
		echo json_encode($data);
	}
	//eprot filter column fetch
	public function reportfiltercolumnfetch() {
		$content = '';
		$appdateformat = $this->Basefunctions->appdateformatfetch();
		$moduleid = $_POST['moduleid'];
		$moduletabsecid = isset($_POST['moduletabsecid']) ? $_POST['moduletabsecid'] : '1';
		$relatedmoduleid =$_POST['relatedmoduleid'];
		if($relatedmoduleid == 'null' || $relatedmoduleid == '') {
			$moduleid = $moduleid;
		} else {
			$moduleid = $moduleid.','.$relatedmoduleid;
		}
		if($_POST['moduleid'] == 49 || $_POST['moduleid'] == 41) {
			$moduletabsecid = $moduletabsecid;
		} else {
			$moduletabsecid = 1;
		}
		$moduledd = $this->Reportsmodel->getalldropdownbasedonmodule($moduleid,$moduletabsecid);
		$data = $this->Reportsmodel->showalldropdowns($moduledd);
		$moduleid= explode(',',$moduleid);
		$filtertabsecfield = $this->Reportsmodel->filterdataviewdropdowncolumns($moduleid,$moduletabsecid);
		
		$content .= '<input type="hidden" value="'.$data['length'].'" id="reportfilterdropdownlenth" name="reportfilterdropdownlenth"/>';
		$content .= '<div class="elementlist large-12 columns static-field" >
					<div class="" id=""><label>Fields</label><select data-placeholder="" data-prompt-position="topLeft:14,36" id="reportfiltercolumn" class="validate[required] chzn-select dropdownchange reportcolumnfilter" name="viewcolname" tabindex="9"><option></option>';
		$prev = ' ';
		foreach($filtertabsecfield as $key):
		$cur = $key->moduletabsectionid;
		$a ="";
		if($prev != $cur ) {
			if($prev != " ") {
				$content.= '</optgroup>';
			}
			$content.= '<optgroup  label="'.$key->moduletabsectionname.'" class="'.str_replace(' ','',$key->moduletabsectionname).'">';
			$prev = $key->moduletabsectionid;
			$a = "pclass";
		}
		$selopt="";
		if($key->fieldview == '1') {
			$selopt="selected='selected'";
		}
		$content .='<option value="'.$key->viewcreationcolumnid.'" data-indexname="'.$key->viewcreationcolmodelindexname.'" data-uitype="'.$key->uitypeid.'" data-moduleid="'.$key->viewcreationmoduleid.'" data-label="'.$key->viewcreationcolumnname.'" data-fldview="'.$key->fieldview.'" data-fldrestrict="'.$key->fieldrestrict.'">'.$key->viewcreationcolumnname.'</option>';
		endforeach;
		$content .='</select></div></div>';
		$content .='</div></div>';
		$content .='<div class="static-field large-12 columns viewcondclear">
						<label>Condition <span class="mandatoryfildclass">*</span></label>
						<select data-placeholder="" data-prompt-position="topLeft:14,36" class="validate[required] chzn-select chosenwidth " tabindex="11" name="reportfiltercondition" id="reportfiltercondition">
							<option></option>
						</select>
					</div>
				 <div class="input-field large-12 columns viewcondclear" id="reportfiltercondvaluedivhid">
					<input type="text" class="validate[required,maxSize[100]]" id="reportfiltercondvalue" name="reportfiltercondvalue"  tabindex="12" >
					<label for="reportfiltercondvalue">Value <span class="mandatoryfildclass">*</span></label>
				</div>
				<div class="static-field large-12 columns viewcondclear hidedisplay" id="reportfilterddcondvaluedivhid">
					<label>Value<span class="mandatoryfildclass">*</span></label>
					<select data-placeholder="" data-prompt-position="topLeft:14,36" class="validate[required] chzn-select chosenwidth valdropdownchange" tabindex="13" name="reportfilterddcondvalue" id="reportfilterddcondvalue">
						<option></option>
					</select>
				</div>
				<div class="static-field large-12 columns viewcondclear hidedisplay" id="reportfilterassignddcondvaluedivhid">
					<label>Value<span class="mandatoryfildclass">*</span></label>
					<select data-placeholder="" data-prompt-position="topLeft:14,36" class="validate[required] chzn-select chosenwidth valdropdownchange" tabindex="13" name="reportfilterassignddcondvalue" id="reportfilterassignddcondvalue">
						<option></option>
					</select>
				</div>
				<div class="input-field large-6 columns viewcondclear hidedisplay" id="reportfilterdatecondvaluedivhid">
					<input type="text" class="validate[required]" data-dateformater="';
		$content .= $appdateformat.'" id="reportfilterdatecondvalue" name="reportfilterdatecondvalue" tabindex="12" >
					<label for="reportfilterdatecondvalue">From Date <span class="mandatoryfildclass">*</span></label>
				</div>
				<div class="input-field large-6 columns viewcondclear hidedisplay" id="reportfilterdatecondtovaluedivhid">
					<input type="text" class="validate[required]" data-dateformater="';
		$content .= $appdateformat.'" id="reportfilterdatecondtovalue" name="reportfilterdatecondtovalue" tabindex="12" >
					<label for="reportfilterdatecondtovalue">To Date <span class="mandatoryfildclass">*</span></label>
				</div>
				<div class="filterchecklistview hidedisplay" id="reportfilterchechboxcondvaluedivhid">
					
				</div>
				<div class="large-12 columns viewcondclear">
					<input type="hidden" class="" id="reportfilterfinalviewcondvalue" name="reportfilterfinalviewcondvalue"  tabindex="12" ><input type="hidden" class="" id="reportfilterfinalviewconid" name="reportfilterfinalviewconid"  tabindex="12" >
				</div>
				<div class="large-12 columns">&nbsp;</div>
				<div class="large-12 columns text-center">
					<input type="button" style="width:80px;" class="btn formbuttonsalert" id="reportfilteraddcondsubbtn" Value="Submit" name="reportfilteraddcondsubbtn"  tabindex="14" >
				</div></span>
				<div class="large-12 columns">
					<input type="hidden" id="reportfilterid" value="" name="reportfilterid">
					<input type="hidden" id="reportconditionname" value="" name="reportconditionname">
					<input type="hidden" id="reportfiltervalue" value="" name="reportfiltervalue">
					<input type="hidden" id="reportddfilterid" value="" name="reportddfilterid">
					<input type="hidden" id="reportddconditionname" value="" name="reportddconditionname">
					<input type="hidden" id="reportddfiltervalue" value="" name="reportddfiltervalue">
				</div>
				<div class="large-12 columns">
					<div id="reportfilterconddisplay" class="large-12 medium-12 small-12 columns uploadcontainerstyle" style="height:10rem; border:1px solid #CCCCCC;overflow-y:auto;overflow-x:hidden;"></div>
				</div>
				<div class="large-12 columns">&nbsp;</div>
				<div class="large-12 columns">&nbsp;</div>';
		$content .= $data['content'];	// All Dropdown fields showing
		$content .= '<div class="large-12 columns">&nbsp;</div></span></div></div></div>';
		echo json_encode($content);
	}
	public function loadmoduletabsction() {
		$this->Reportsmodel->loadmoduletabsctionmodel();
	}
	// Retrieve Date Value - from date and to date
	public function retrievedatevaluehtmlreport() {
		$this->Reportsmodel->retrievedatevaluehtmlreport();
	}
	// Sales Detail Image display
	public function retrievesalesdetailimage() {
		$this->Reportsmodel->retrievesalesdetailimage();
	}
	// Stock - Tag Image display
	public function retrievestocktagimage() {
		$this->Reportsmodel->retrievestocktagimage();
	}
}