<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('Base/headerfiles'); ?>
	<link href="<?php echo base_url();?>css/jqgrid.min.css" media="screen" rel="stylesheet" type="text/css" />
	<!-- //For Left Padding Grid data  -->	
	<style>
		#customreportgridwidth .ui-jqgrid tr.ui-row-ltr td {
			text-align: left;
			padding-left:1rem !important;
		}
	</style>
</head>
<body>
	<div class="row" style="max-width:100%" id="productsearchoverlay">
		<div class="off-canvas-wrap" data-offcanvas>
			<div class="inner-wrap">
				<!-- Add Form -->
				<div id="stockreportsformdiv" class="">
					<!-- Form Header -->
					<div class="large-12 columns headercaptionstyle mastergridformreload paddingzero">
						<div class="large-6 medium-6 small-12 columns headercaptionleft">
							<span  data-activates="slide-out" class="headermainmenuicon button-collapse" title="Menu"><i class="material-icons">menu</i></span>
							<span class="gridcaptionpos"><span><?php echo $reportname;?></span></span>
						</div>
						<!-- Form Header Icons -->
						<div class="large-5 medium-6 small-12 columns addformheadericonstyle righttext small-only-text-center">
							<span title="Excel" class="icon-box" id="reportdetailedexcel"><i class="icon-w icon-file-excel"></i> </span>
							<span title="Csv" class="icon-box" id="reportdetailedcsv"><i class="icon-w icon-libreoffice"></i> </span>
							<span title="Reload" class="icon-box" id="reportdetailreload"><i class="material-icons reloadiconclass" title="Reload">refresh</i></span>
							<span id="closeaddform" class="icon-box" title="Close"><i class="material-icons">close</i></span>
						</div>  
					</div>
					<div class="large-12 columns addformunderheader">&nbsp;</div>
					<div class="large-12 columns scrollbarclass addformcontainer formwithouttabgroup">
						<div class="row">&nbsp;</div>
						<input type="hidden" id="primaryreportid" value="<?php echo $primaryreportid?>"/>
						<div class="large-12 columns">	
							<div class="large-12 columns viewgridcolorstyle" style="padding-left:0;padding-right:0;">	
								<div class="large-12 columns reportsheaderfiltercontainer" style="padding: 0.25rem 0 0.4rem;float:left">
									<ul class="filterunorderlist" id="filtercontainer">
									</ul>
								</div>
								<div class="large-12 columns" id="customreportgridwidth" style="padding-left:0;padding-right:0;">
									<table id="customreportgrid"> </table> 
									<div id="customreportgriddiv" class="w100"></div>  		
								</div>								
							</div>
						</div>
						<div class="large-12 columns">&nbsp;</div>						
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
				</div>
				<!--hidden-->
				<input type="hidden" id="hiddenrptid" value="<?php echo $id;?>"/>				
				<input type="hidden" id="reportgridcoldetail" value=""/>				
			</div>
		</div>
	</div>
</body>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js Files -->	
	<script src="<?php echo base_url();?>js/Reportsview/customreports.js" type="text/javascript"></script> 
</html>
