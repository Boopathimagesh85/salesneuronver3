<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="folderdeleteformoverlay">	
		<div class="row toppadding">&nbsp;</div>
		<div class="large-3 medium-6 small-10 large-centered medium-centered small-centered columns">
			<span id="reportfolderdeletevalidation" class="validationEngineContainer" >
			<div class="alert-panel" style="background-color:#465a63">
				<div class="alertmessagearea">
					<div class="alert-title">Transfer & Delete</div>
					<div class="static-field overlayfield large-12 columns">
						<label>Transfer To Folder<span class="mandatoryfildclass">*</span></label>
						<select class="chzn-select validate[required]" id="conformfolderid" name="conformfolderid" data-prompt-position="topLeft" data-placeholder="Select">
						<option value=""></option>
						</select>
						<input type="hidden" name="delfolderid" id="delfolderid" value="" />
					</div>
					<div style="font-size: 1rem;color: #fff;line-height: 1.4rem;text-align:center">Delete this data?</div>
				</div>
				<div class="alertbuttonarea">
					<span class="firsttab" tabindex="1000"></span>
					<input type="button" id="basedeleteyes1" name="" value="Delete" tabindex="1001" class="alertbtnyes ffield " >	
					<input type="button" id="basedeleteno" name="" value="Cancel" tabindex="1002" class="alertbtnno flloop  alertsoverlaybtn" >
					<input type="hidden" id="geventid" name="gdeleventid" value="" class="" >
					<span class="lasttab" tabindex="1003"></span>
				</div>
			</div>
			</span>
		</div>
	</div>
</div>
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="folderdeleteformoverlay">	
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-3 medium-6 small-10 large-centered medium-centered small-centered columns">
		<span id="reportfolderdeletevalidation" class="validationEngineContainer" >
			<div class="row">&nbsp;</div>
			<div class="row">
				<div class="large-12 columns alertsheadercaptionstyle" style="text-align:left">
					<div class="small-12 columns"><span style="color:#f5f5f5;padding-right:0.5rem"><i class="material-icons">check_box</i>  </span> Transfer & Delete</div>
				</div>
				<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
				<div class="static-field large-12 columns cleardataform" style="background:#f5f5f5">
					<label>
						Transfer To Folder<span class="mandatoryfildclass">*</span>
					</label>
					<select class="chzn-select validate[required]" id="conformfolderid" name="conformfolderid" data-prompt-position="topLeft" data-placeholder="Select">
						<option value=""></option>
					</select>
					<input type="hidden" name="delfolderid" id="delfolderid" value="" />
				</div>
				<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
				<div class="large-12 large-centered columns" style="background:#f5f5f5;text-align:center">
					<span class="" style="	color: #5e6d82;font-family: segoe ui;font-size: 0.9rem;">Delete the data ?</span>
					<div class="large-12 column">&nbsp;</div>
				</div>
			</div>
			<span class="firsttab" tabindex="1000"></span>
			<div class="row" style="background:#f5f5f5">
				<div class="medium-2 large-2 columns"> &nbsp;</div>
				<div class="small-6  medium-4 large-4 columns">
					<input type="button" id="basedeleteyes1" name="" value="Yes" tabindex="1001" class="btn ffield formbuttonsalert" >	
				</div>
				<div class="small-6 medium-4 large-4 columns">
					<input type="button" id="basedeleteno" name="" value="No" tabindex="1002" class="btn flloop formbuttonsalert alertsoverlaybtn" >	
				</div>
				<span class="lasttab" tabindex="1003"></span>
				<div class="medium-2 large-2 columns"> &nbsp;</div>
			</div>
			<div class="row" style="background:#f5f5f5">&nbsp;</div>
			<div class="row" style="background:#f5f5f5">&nbsp;</div>
		</span>
		</div>
	</div>
</div>