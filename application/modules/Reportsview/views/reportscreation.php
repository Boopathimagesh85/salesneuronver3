<!-- For Date Formatter -->
<?php 
$CI =& get_instance();
$appdateformat = $CI->Basefunctions->appdateformatfetch(); ?>
<div class="large-12 columns headercaptionstyle headerradius addformreloadtablet paddingzero">
	<div class="large-6 medium-6 small-12 columns headercaptionleft">
		<span  data-activates="slide-out" class="headermainmenuicon button-collapse" title="Menu"><i class="material-icons">menu</i></span>
		<span class="gridcaptionpos"><span><?php echo $gridtitle; ?></span></span>
	</div>
	<div class="large-5 medium-6 small-12 columns addformheadericonstyle addformaction righttext small-only-text-center"> 
		<span class ="addbtnclass">
		<span id="mainreportlistaddsubmit" title="Save"><i class="material-icons">save</i><span class="actiontitle">Save</span></span>
		</span>
		<span class ="">
		<span id="mainreportrefreshsubmit" title="Refersh"><i class="material-icons" title="Reload">refresh</i><span class="actiontitle">Refresh</span></span>
		</span>
		<span class ="updatebtnclass">
		<span id="mainreportlisteditsubmit" title="Save"><i class="material-icons">save</i><span class="actiontitle">Save</span></span>
		</span>
		<span class ="">
		<span id="reportcloseaddform" title="Close"><i class="material-icons">close</i><span class="actiontitle">Close</span></span>
		</span>
	</div>
</div>
<!-- tab group creation -->
<div class="large-12 columns tabgroupstyle show-for-large-up">
	<ul class="tabs" data-tab="">
		<li id="tab1" class="tab-title sidebaricons active ftab" data-subform="1">
			<span>Basic</span>
		</li>
		<li id="tab2" class="tab-title sidebaricons ftab2" data-subform="2">
			<span>Criteria</span>
		</li>
		<li id="tab3" class="tab-title sidebaricons " data-subform="3">
			<span>Add On</span>
		</li>
	</ul>
</div>
<!--For Tablet and Mobile view Dropdown-->
<div class="large-12 columns tabgroupstyle centertext show-for-medium-down tabgrpddstyle">
	<span class="tabmoiconcontainer">
		<select id="tabgropdropdown" class="chzn-select" style="width:40%">
			<option value="1">Basic</option>
			<option value="2">Criteria</option>
			<option value="3">Add On</option>
		</select>
	</span>
</div>
<div class="large-12 columns addformunderheader">&nbsp;</div>
<div class="large-12 columns scrollbarclass addformcontainer reportscreationtouch" style="background-color: #f2f3fa !important;">
	<div class="row">&nbsp;</div>
	<div id="subformspan1" class="hiddensubform">
		<form method="POST" name="reportbasicdetails" class=""  id="reportbasicdetails">
			<span id="validatereportbasicdetails" class="validationEngineContainer clearbasicdetail" >
			<span id="validatereportbasiceditdetails" class="validationEngineContainer clearbasicdetail" >
				<span id="" class="validationEngineContainer">
					<div class="large-4 columns paddingbtm">
						<div class="large-12 columns cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
							<div class="large-12 columns headerformcaptionstyle">Basic Details</div>
							<div class="static-field large-12 columns">
								<label>Report Type<span class="mandatoryfildclass">*</span></label>
								<select class="chzn-select validate[required]" id="reporttype" name="reporttype" data-prompt-position="topLeft:14,36">
									<option value=""></option> 
									<?php foreach($reporttype as $key):?>
									<option  value="<?php echo $key->reporttypeid;?>" ><?php echo $key->reporttypename;?></option>
									<?php endforeach;?>	
								</select>
							</div>
							<div class="input-field large-12 columns">
								<input id="reportname" class="validate[required]" type="text" tabindex="" name="reportname">
								<label for="reportname">Report Name<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="input-field large-12 columns">
								<label>Description</label>
								<textarea id="reportdescription" class="validate[maxSize[100]] materialize-textarea" data-prompt-position="topLeft" tabindex="" name="reportdescription"></textarea>
							</div>
							<div class="static-field large-12 columns">
								<label>Report Folder<span class="mandatoryfildclass">*</span></label>
								<select class="chzn-select validate[required]" id="reportfolderid" name="reportfolderid" data-prompt-position="topLeft:14,36">
									<option value=""></option>	
								</select>
							</div>
							<span class="hidedisplay">
								<div class="large-12 columns">
									<input id="schedulereport" class="checkboxcls radiochecksize filled-in" type="checkbox"  tabindex="102"  name="schedulereport" data-prompt-position="topLeft:14,36" data-hidname="schedulereport" value="">
									<label for="schedulereport">Schedule Report</label>
								</div>
							</span>
							<div class="static-field large-12 columns">
								<label>Main Module Name<span class="mandatoryfildclass">*</span></label>
								<select class="chzn-select validate[required]" data-prompt-position="topLeft:14,36" id="reportmodule" name="reportmodule" data-prompt-position="topLeft">
									<option value=""></option> 
									<?php foreach($module as $key):?>
									<option  value="<?php echo $key->moduleid;?>" ><?php echo $key->moduleid;?>-<?php echo $key->modulename;?></option>
									<?php endforeach;?>		
								</select>
							</div>
							<div class="static-field large-12 columns">
								<label>Related Module<span class="mandatoryfildclass">*</span></label>
								<select class="chzn-select validate[required]" data-prompt-position="topLeft:14,36" multiple id="reportrelatedmodule" name="reportrelatedmodule[]" data-prompt-position="topLeft">
									<option value=""></option>									
								</select>
							</div>
							<div class="large-12 columns"> &nbsp </div>
						</div>
					</div>
					<div class="large-4 columns  paddingbtm end" id='recurrencedetaildiv'>
						<div class="large-12 columns cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
							<div class="large-12 columns headerformcaptionstyle">Recurrence Details</div>
							<div class="static-field large-12 columns">
								<label>Frequency</label>
								<select class="chzn-select" id="reportfrequency" name="reportfrequency" data-prompt-position="topLeft">
									<option value=""></option> 
									<?php foreach($frequency as $key):?>
									<option  value="<?php echo $key->recurrencefreqid;?>" ><?php echo $key->recurrencefreqname;?></option>
									<?php endforeach;?>		
								</select>
							</div>
							<div class="input-field large-6 columns">
								<input id="ondays" class="" type="text" data-prompt-position="topLeft" tabindex="" name="ondays">
								<label for="ondays">On Days</label>
							</div>
							<div class="input-field large-6 columns">
								<input id="recurrencytime" class="" type="text" data-prompt-position="topLeft" tabindex="" name="recurrencytime">
								<label for="recurrencytime">Time</label>
							</div>
							<div class="static-field large-12 columns">
								<label>Recipient To</label>
								<select class="chzn-select" id="recurrencerecipient" name="recurrencerecipient" data-prompt-position="topLeft">
									<option value=""></option> 
									<?php foreach($employee as $key):?>
									<option  value="<?php echo $key->employeeid;?>"><?php echo $key->employeename;?></option>
									<?php endforeach;?>		
								</select>
							</div>
							<div class="input-field large-12 columns">
								<input id="recurrenceemail" class="" type="text" data-prompt-position="topLeft" tabindex="" name="recurrenceemail">
								<label for="recurrenceemail">Send Email To</label>
							</div>							
							<div class="static-field large-12 columns">
								<label>File Format</label>
								<select class="chzn-select" id="fileformat" name="fileformat" data-prompt-position="topLeft">
									<option value=""></option> 
									<?php foreach($filetype as $key):?>
									<option  value="<?php echo $key->filetypeid;?>" ><?php echo $key->filetypename;?></option>
									<?php endforeach;?>	
								</select>
							</div>							
							<div class="large-12 columns"> &nbsp </div>
						</div>
					</div>
				</span>
				</span>
			</span>
			<!-- hidden fields-->
			<input type="hidden" name="reportconrowcolids" id="reportconrowcolids" value="0" />
			<input type="hidden" name="reportcalcrowcolids" id="reportcalcrowcolids" value="0" />
		</form>
	</div>
	<div id="subformspan2" class="hiddensubform hidedisplay">
				<div class="large-12 columns  paddingbtm end">
						<div class="large-12 columns cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
							<div class="large-12 columns headerformcaptionstyle"> Select Columns </div>
							<div class="static-field large-12 columns">
							<label>Select Columns</label>
							<select id="reportmaincolumnname" class="validate[required] chzn-select dropdownchange whitecss select2-offscreen reportmaincolumnname" tabindex="" name="reportmaincolumnname" multiple="multiple" data-prompt-position="topRight:-120,0" data-placeholder="Select Columns">
								<option></option>
								</select>
							</div>
							<div class="large-12 columns"> &nbsp </div>
						</div>
				</div>
				<div class="large-4 columns  paddingbtm end">
				<div class="large-12 columns cleardataform paddingzero" style="background-color:#f5f5f5">	
					<div class="large-12 columns headerformcaptionstyle" >Standard Filter</div>		
						<div class="static-field large-12 columns">
							<label>Date Column</label>
								<select data-placeholder="Select" data-prompt-position="topLeft:14,36" class="validate[required] chzn-select chosenwidth viewdropdownchange" tabindex="10" name="report_datefield" id="report_datefield">
								<option></option>
								</select> 
							<input type="hidden" name="" id="" value="" />
						</div>
						<div class="static-field large-12 columns">
							<label>Range</label>
								<select data-placeholder="Select" data-prompt-position="topLeft:14,36" class="validate[required] chzn-select chosenwidth viewdropdownchange" tabindex="10" name="report_range" id="report_range">
								<option></option>
								<option value="custom" data-mode="instant">Custom</option>
								 <optgroup label="Day">	
									<option value="-1 day" data-mode="instant">Yesterday</option>
									<option value="0 day" data-mode="instant">Today</option>
									<option value="1 day" data-mode="instant">Tomorrow</option>
									<option value="-6 day" data-mode="direct">Last 7 Days</option>
									<option value="-29 day" data-mode="direct">Last 30 Days</option>
									<option value="-59 day" data-mode="direct">Last 60 Days</option>
									<option value="-89 day" data-mode="direct">Last 90 Days</option>
									<option value="-119 day" data-mode="direct">Last 120 Days</option>
									<option value="6 day" data-mode="direct">Next 7 Days</option>
									<option value="29 day" data-mode="direct">Next 30 Days</option>
									<option value="59 day" data-mode="direct">Next 60 Days</option>
									<option value="89 day" data-mode="direct">Next 90 Days</option> 
									<option value="119 day" data-mode="direct">Next 120 Days</option>
								 </optgroup>
								<optgroup label="Calendar Week">
									<option value="lastweek" data-mode="switch">Last Week</option>
									<option value="currentweek" data-mode="switch">Current Week</option>
									<option value="nextweek" data-mode="switch">Next Week</option>
								</optgroup>
								<optgroup label="Calendar Month">
									<option value="lastmonth" data-mode="switch">Last Month</option>
									<option value="currentmonth" data-mode="switch">Current Month</option>
									<option value="nextmonth" data-mode="switch">Next Month</option>
									<option value="currentandpreviousmonth" data-mode="switch">Current and Previous Month</option>
									<option value="currentandnextmonth" data-mode="switch">Current and Next Month</option>		
								</optgroup>
								</select> 
							<input type="hidden" name="" id="" value="" />
						</div>
						<div class="input-field large-6 columns viewcondclear" id="">
							<input type="text" class="fordatepicicon" data-dateformater="<?php echo $appdateformat; ?>" id="report_startdate" name="report_startdate"  tabindex="" >
							<label for="report_startdate">Start Date</label>
						</div>
						<div class="input-field large-6 columns viewcondclear" id="">
							<input type="text" class="fordatepicicon" data-dateformater="<?php echo $appdateformat; ?>" id="report_enddate" name="report_enddate"  tabindex="" >
							<label for="report_enddate">End Date</label>
						</div>
					<div class="large-12 columns" >&nbsp </div>
				</div>
				<div class="large-12 columns" >&nbsp </div>
				<form id="reportcriteriaform" name="reportcriteriaform">
				<div id="reportformconditionvalidation" class='validationEngineContainer reportcondclear'>
						<div class="large-12 columns cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
							<div class="large-12 columns headerformcaptionstyle">Advanced Filter</div>
							<div class="static-field large-12 columns">
								<label>Field Name<span class="mandatoryfildclass">*</span></label>
									<select data-placeholder="Select" data-prompt-position="topLeft:14,36" class="validate[required] chzn-select chosenwidth viewdropdownchange" tabindex="10" name="reportcondcolumn" id="reportcondcolumn">
								<option></option>
							</select> 
								<input type="hidden" name="reportcondcolumnid" id="reportcondcolumnid" value="" />
							</div>
							<div class="static-field large-12 columns">
								<label>Condition<span class="mandatoryfildclass">*</span></label>
								<select id="reportcondition" class="validate[required] chzn-select chosenwidth dropdownchange select2-offscreen whitecss forsucesscls" name="reportcondition" tabindex="-1" data-placeholder="Select" data-prompt-position="topLeft:14,36" title="">
								<option></option>
								<option value="Equalto">Equalto</option>
								<option value="NotEqual">NotEqual</option>
								<option value="Startwith">Startwith</option>
								<option value="Endwith">Endwith</option>
								<option value="Middle">Middle</option>
								</select>
							</div>
							<div class="input-field large-12 columns viewcondclear" id="reportcondvaluedivhid">
							<input type="text" class="validate[required]" id="reportcondvalue" name="reportcondvalue"  tabindex="12" >
							<label for="reportcondvalue">Value<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="static-field large-12 columns viewcondclear" id="reportddcondvaluedivhid">
							<label>Value<span class="mandatoryfildclass">*</span></label>
							<select data-placeholder="Select"  data-prompt-position="topLeft:14,36" class="validate[required] chzn-select chosenwidth dropdownchange" tabindex="13" name="reportddcondvalue" id="reportddcondvalue">
								<option></option>
							</select>  
							</div>
						<div class="input-field large-12 columns viewcondclear" id="reportdatecondvaluedivhid">
							<input type="text" class="validate[required]" data-dateformater="<?php echo $appdateformat; ?>" id="reportdatecondvalue" name="reportdatecondvalue"  tabindex="12" >
							<label for="reportdatecondvalue">Value<span class="mandatoryfildclass">*</span> <span class="mandatoryfildclass">*</span></label>
						</div>
						<div class="static-field large-12 columns">
							<label>AND/OR Operation<span class="mandatoryfildclass">*</span></label>
							<select id="reportandorcond" data-prompt-position="topLeft:14,36" class="validate[required] chzn-select chosenwidth dropdownchange select2-offscreen whitecss forsucesscls" name="reportandorcond" tabindex="-1" data-placeholder="Select" title="">
							<option></option>
							<option value="AND">AND</option>
							<option value="OR">OR</option>
							</select>
						</div>
						<div class="large-12 columns" style="text-align: right">
							<label> </label>
							<input id="reportaddcondsubbtn" class="btn" type="button" value="Submit" name="" tabindex="">
							<input id="" class="btn hidedisplay" type="button" value="Submit" name="" tabindex="" style="display: none;">
						</div>								
						<div class="large-12 columns"> &nbsp </div>
						</div>
						<input type="hidden" class="" id="finalreportcondvalue" name="finalreportcondvalue"  tabindex="12" >
						<input type="hidden" class="" id="finalreportcondvalueid" name="finalreportcondvalueid"  tabindex="12" >
						<input type="hidden" class="" id="reportconditionoperate" name="reportconditionoperate"  tabindex="12" >
						<input type="hidden" name="reportcolstatus" id="reportcolstatus" value="2"/>
						</div>
				</form>
				</div>
				<div class="large-8 columns paddingbtm">
						<div class="large-12 columns paddingzero">
							<div class="large-12 columns viewgridcolorstyle paddingzero">
								<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0 0.4rem;">
									<span class="large-6 medium-6 small-6 columns" style="text-align:left">Criteria List</span>
									<span class="large-6 medium-6 small-6 columns innergridicon" style="text-align:right;margin-top: 0px !important;">
										<span id="reportconddeleteicon" class="fa fa-trash-o deleteiconclass " title="Delete"> </span>
									</span>
								</div>
								<div class="large-12 columns paddingzero" id="reportcreateconditiongridwidth">	
									<table id="reportcreateconditiongrid"></table>
									<div id="reportcreateconditiongridnav"> </div>
								</div>
							</div>
						</div>
				</div>
	</div>
	<div id="subformspan3" class="hiddensubform hidedisplay">
		<span id="groupingdetailsspan">
			<div class="large-12 columns  paddingbtm" >
				<form id='reportgroupbysortby'>
					<div class="large-4 columns paddingbtm cleardataform">
						<div class="large-12 columns paddingzero" style="background: #f5f5f5">
							<div class="large-12 columns headerformcaptionstyle">Summary Info By</div>
							<div class="static-field large-12 columns">
								<label>Field Name</label>
								<select class="chzn-select reportmaincolumnname" id="reportgroupby1" name="reportgroupby1" data-prompt-position="topLeft">
								<option value=""></option>
								</select>
							</div>
							<div class="large-6 columns">
								<input type="Radio" class="with-gap" id="reportsortby1" name="reportsortby1" value="asc" tabindex="" >
								<label for="reportsortby1">Asc</label>
							</div>
								<div class="large-6 columns">
								<input type="Radio" class="with-gap" id="reportsortby1" name="reportsortby1" value="desc" tabindex="" >
								<label for="reportsortby1">Dsc</label>
							</div>
							<div class="large-12 columns"> &nbsp </div>
						</div>	
					</div>	
					<div class="large-4 columns paddingbtm cleardataform">
						<div class="large-12 columns" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
							<div class="large-12 columns headerformcaptionstyle">& Then By</div>
							<div class="static-field large-12 columns">
								<label>Field Name</label>
								<select class="chzn-select reportmaincolumnname" id="reportgroupby2" name="reportgroupby2" data-prompt-position="topLeft">
								<option value=""></option>
								</select>
							</div>
							<div class="large-6 small-6 columns">
								<input type="Radio" class="with-gap" id="reportsortby2" name="reportsortby2" value="asc" tabindex="" >
								<label for="reportsortby2">Asc</label>
							</div>
							<div class="large-6 small-6 columns">
								<input type="Radio" class="with-gap" id="reportsortby2" name="reportsortby2" value="desc" tabindex="" >
								<label for="reportsortby2">Dsc</label>
							</div>
							<div class="large-12 columns"> &nbsp </div>
						</div>	
						<div class="small-12 columns show-for-small-only "> &nbsp </div>
					</div>	
					<div class="large-4 columns paddingbtm cleardataform">
						<div class="large-12 columns end" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
							<div class="large-12 columns headerformcaptionstyle">& Finally By</div>
							<div class="static-field large-12 columns">
								<label>Field Name</label>
								<select class="chzn-select reportmaincolumnname" id="reportgroupby3" name="reportgroupby3" data-prompt-position="topLeft">
								<option value=""></option>
								</select>
							</div>
							<div class="large-6 small-6 columns">
								<input type="Radio" class="with-gap" id="reportsortby3" name="reportsortby3" value="asc" tabindex="" >
								<label for="reportsortby3">Asc</label>
							</div>
							<div class="large-6 small-6 columns">
							<input type="Radio" class="with-gap" id="reportsortby3" name="reportsortby3" value="desc" tabindex="" >
							<label for="reportsortby3">Dsc</label>
							</div>
							<div class="large-12 columns"> &nbsp </div>
						</div>	
						<div class="small-12 columns show-for-small-only "> &nbsp </div>
					</div>			
				</form>
			</div>
		</span>
		<div class="large-12 columns  paddingzero end">
			<div id="reportcalculationvalidation" class='validationEngineContainer reportcalculationclear'>
				<form id="reportcalculationform" name="reportcalculationform">
					<div class="large-4 columns paddingbtm">
						<div id="" class="large-12 columns cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
							<div class="large-12 columns headerformcaptionstyle">Summary Total</div>
							<div class="static-field large-12 columns" id="">
								<label>Calculation Formula<span class="mandatoryfildclass">*</span></label>
									<select class="chzn-select validate[required] " id="reportcalculationformula" name="reportcalculationformula" data-prompt-position="topLeft:14,36">
									<option value=""></option>
									<option value="SUM">SUM</option>
									<option value="AVG">AVERAGE</option>
									<option value="MIN">MIN</option>
									<option value="MAX">MAX</option>						
									<option value="COUNT">COUNT</option>						
									</select>
							</div>
							<div class="static-field large-12 columns" id="">
								<label>Calculation On<span class="mandatoryfildclass">*</span></label>
									<select class="chzn-select validate[required]" id="reportcalculationon" name="reportcalculationon" data-prompt-position="topLeft:14,36">
									<option value=""></option>
									</select>
							</div>							
							<div class="large-12 columns" style="text-align: right">
								<label> </label>
								<input id="reportcalculationaddsubmit" class="btn" type="button" value="Submit" name="" tabindex="">
								<input id="" class="btn hidedisplay" type="button" value="Submit" name="" tabindex="" style="display: none;">
							</div>									
						</div>
						<div class="large-12 columns"> &nbsp </div>
					</div>
					<div class="large-8 columns paddingbtm">
						<div class="large-12 columns paddingzero">
							<div class="large-12 columns viewgridcolorstyle paddingzero">
								<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0 0.4rem;">
									<span class="large-6 medium-6 small-6 columns" style="text-align:left">Calculation List</span>
									<span class="large-6 medium-6 small-6 columns innergridicon" style="text-align:right">
										<span id="reportcalcdeleteicon" class="fa fa-trash-o deleteiconclass " title="Delete"> </span>
									</span>
								</div>
								<div class="large-12 columns paddingzero" id="reportcalculationgridwidth">	
									<table id="reportcalculationgrid"></table>
									<div id="reportcalculationgridnav"> </div>
								</div>
							</div>
						</div>
					</div>
					<input type="hidden" id="reportcalculationonid" name="reportcalculationonid"/>
					<input type="hidden" name="reportcalcstatus" id="reportcalcstatus" value="2"/>
				</form>
			</div>
		</div>
	</div>
	<input type="hidden" name="reportcolnameid" id="reportcolnameid" value=""/>	
	<input type="hidden" name="editreportid" id="editreportid" value="" />
</div>