<!DOCTYPE html>
<html lang="en">
<head>
 	<?php $this->load->view('Base/headerfiles'); ?>
	<link href="<?php echo base_url();?>css/jqgrid.min.css" media="screen" rel="stylesheet" type="text/css" />
<style>
	a:hover {
		font-weight:bold;
		font-size:18px;
		color: #512DA8;
	}
	.footer-contentnew .paging-box {
		left:-35% !important;
		position:relative;
	}
</style>
</head>
<body>
	<div class="row" style="max-width:100%">
		<div class="off-canvas-wrap" data-offcanvas>
			<div class="inner-wrap">
				<div class="gridviewdivforsh" id="reportsview">
					<?php
						$device = $this->Basefunctions->deviceinfo();
						$dataset['gridtitle'] = $gridtitle['title'];
						$dataset['titleicon'] = $gridtitle['titleicon'];
						$dataset['formtype'] = 'reportdashbaord';
						$dataset['gridid'] = 'reportsgrid';
						$dataset['gridwidth'] = 'reportsgridwidth';
						$dataset['gridfooter'] = 'reportsgridfooter';
						$this->load->view('Base/mainviewheader',$dataset);
						$dataset['moduleid'] = '35';
						$this->load->view('Base/singlemainviewformwithgrid',$dataset);
					?>
				</div></div>
				</div>
				<div class="hidedisplay" id="folderoverlaydiv">
					<?php $this->load->view('folderform'); ?>
				</div>
				<!-- Add Form -->
				<div id="reportsformdiv" class="">
				</div>
				<?php
					$this->load->view('Base/basedeleteform');
					$this->load->view('reportdeleteform');
					$this->load->view('folderdeleteform');
					if($device=='phone') {
						$this->load->view('Base/overlaymobile');
					} else {
						$this->load->view('Base/overlay');
					}
					$this->load->view('Base/modulelist');
				?>										
			</div>
		</div>
	</div>
<?php $this->load->view('Reportsview/overlayset'); ?>
</body>
<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js Files -->	
	<script src="<?php echo base_url();?>js/Reportsview/multiselect.js" type="text/javascript"></script> 
	<script src="<?php echo base_url();?>js/Reportsview/reports.js" type="text/javascript"></script> 
	<script src="<?php echo base_url();?>js/plugins/sortable.js" type="text/javascript"></script>
	<!--For Tablet and Mobile view Dropdown Script-->
	<script>
		$(document).ready(function(){
			$("#tabgropdropdown").change(function(){
				var tabgpid = $("#tabgropdropdown").val(); 
				$('.sidebaricons[data-subform="'+tabgpid+'"]').trigger('click');
			});
		});		
	</script>
</html>
<style type="text/css">
@media (min-width:320px) and (max-width:480px) {

}

.rppdropdown {
	left:66% !important;
}
</style>