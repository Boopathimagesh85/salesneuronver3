<!--Reports Grid---->
<?php 
$CI =& get_instance();
$appdateformat = $CI->Basefunctions->appdateformatfetch(); 
$userroleid = $CI->Basefunctions->userroleid;
?>
<style>
body {
      font-family: 'Roboto', sans-serif;
      -webkit-font-smoothing: antialiased;
    }

    .c-btn {
      font-size: 14px;
      text-transform: capitalize;
      font-weight: 600;
      display: inline-block;
      line-height: 36px;
      cursor: pointer;
      text-align: center;
      text-transform: uppercase;
      min-width: 88px;
      height: 36px;
      margin: 10px 8px;
      padding: 0 8px;
      text-align: center;
      letter-spacing: .5px;
      border-radius: 2px;
      background: #F1F1F1;
      color: #393939;
      transition: background 200ms ease-in-out;
      box-shadow: 0 3.08696px 5.82609px 0 rgba(0, 0, 0, 0.16174), 0 3.65217px 12.91304px 0 rgba(0, 0, 0, 0.12435);
    }

    .c-btn--flat {
      background: transparent;
      margin: 10px 8px;
      min-width: 52px;
    }

    .c-btn:hover {
      background: rgba(153, 153, 153, 0.2);
      color: #393939;
    }

    .c-btn:active {
      box-shadow: 0 9.6087px 10.78261px 0 rgba(0, 0, 0, 0.17217), 0 13.56522px 30.3913px 0 rgba(0, 0, 0, 0.15043);
    }

    .c-btn--flat, .c-btn--flat:hover, .c-btn--flat:active {
      box-shadow: none;
    }
#reportgroupby1,#reportgroupby1_to,#reportmaincolumnname,#reportmaincolumnname_to
{
    height: 28rem;
    display: block;
    padding: 15px;
    background-color: #ffffff;
    overflow: auto;
    border: 0xp solid #fff;
    outline: none;
}
#reportgroupby1 optgroup,#reportgroupby1_to optgroup,#reportmaincolumnname optgroup,#reportmaincolumnname_to optgroup
{
padding:5px;
}
#reportgroupby1  option{
padding-top:5px;
padding-bottom:5px;
}
#reportgroupby1_to  option{
padding-top:5px;
padding-bottom:5px;
}
#reportmaincolumnname_to option{
padding-top:5px;
padding-bottom:5px;
}
#reportmaincolumnname  option{
padding-top:5px;
padding-bottom:5px;
}
#reportmaincolumnname_to  option{
padding-top:5px;
padding-bottom:5px;
}
.form-control{
background-color:#f5f5f5 !important;
}
.searchbox{
    background-color: #fff !important;
    margin: 6px 15px !important;
    width: 85% !important;
}
.updownbutton .btn{
 margin-top: 0px;
    margin-left: 10px;
    float: right;
}
.multiselect-btns{padding-top:2rem;}
.multiselect-btns .btn{width:100%;}
.multiselect-btns .btn i{vertical-align: middle;}
.reportdatashowshide {
    color: transparent !important;
}
.reportdatashowshide:hover {
    color: #000000 !important;
}
#reportmaincolumnname option {
	font-size:1.0rem !important;
}
.multiselect-btns .btn {
    width: 75% !important;
}
#reportmaincolumnname_to option {
	font-size:0.9rem !important;	
}
#reportgroupby1_to option {
	font-size:0.9rem !important;	
}
#reportgroupby1 option {
	font-size:0.9rem !important;	
}
</style>

<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="reportviewoverlay" style="overflow: unset;z-index:40;">	
		<div class="desktop actionmenucontainer action-menu breadcrumb">
			<ul class="module-view">
				<ul class="module-view">
                    <li style="top: -10px;position: relative;">
                        <span class="module-name" style="font-size:1rem;">
                            <span id="reportnameval" style="border-bottom: 3px solid black;"></span>
                        </span>
                    </li>
					<!--
                    <li>
                    	<span id="reporttypename" style="color:#black;left: -6px !important;top: 8px;position: relative;font-size: 1.0rem;font-family: 'Nunito semibold', sans-serif;border-bottom: 3px solid black;"> </span>
						<span style="border-bottom: 3px solid black;">)</span>
                    </li>
					-->
				 <li style="top: -10px;position: relative;">
						<span class="icon-box" id="" style="position: relative;"><i title="help" id="viewtoggle" class="material-icons" style="font-size: 1.8rem;line-height: 1.0;color: #788db4;top:13px;position:relative;">filter_list</i><span class="actiontitle">Filter</span></span>
					</li>
                </ul>
			</ul>

			<ul class="toggle-view tabaction">
				<li class="action-icons" style="top: 5px;position: relative;">
					<span title="Print Report" class="icon-box" id="printhtmlreport" ><i class="material-icons" >print</i><span class="actiontitle">Print Report</span></span>
					<?php if($userroleid == 2) { ?>
					<span class="addbtnclass icon-box" id="savereporttodb" title="Save Report"><i title="Save" class="material-icons">save</i><span class="actiontitle">Save Report</span></span>
					<?php } ?>
					<span title="Edit Report" class="icon-box" id="newoverlay"><i class="material-icons" >edit</i><span class="actiontitle">Edit Report</span></span>
					<span title="Export" class="drop-container dropdown-button icon-box htmlreportexportdiv" id="" data-activates="helpdrop51" id="exportdiv"><i class="material-icons" >apps</i><span class="actiontitle">Export</span></span>
					<ul id="helpdrop51" class="action-drop arrow_box head-help-ul" style="width: 200px;">
					<li id="supportcenter" style="color:white;background-color:#2c2f48;height: 60px;top: -8px; position: relative;border-top-left-radius: 4px;border-top-right-radius: 4px;"><i title="supportcenter" id="supportcenter" class="material-icons" style="font-size: 2rem;line-height: 1.9;color: #fff;">apps</i><span style="position:relative;top:-12px;color:#fff !important;font-size:0.9rem;font-family:
					"Nunito, sans-serif;"">Export Report</span></li>
						<li class="drift-open-chat" id="reportdetailedexcel"><i title="Excel" id="excelicon" class="material-icons" style="font-size: 1.4rem;line-height: 2.0rem;color: #788db4;">view_comfy</i><span style="position:relative;top:-5px;">Excel</span>
						</li>
						<li id="reportdetailedcsv"><i title="upgradeplan" id="csvicon" class="material-icons" style="font-size: 1.4rem;line-height: 2.0rem;color: #788db4;">description</i><span style="position:relative;top:-5px;">Csv</span></li>
						<li id="pdf"><i title="upgradeplan" id="pdficon" class="material-icons" style="font-size: 1.4rem;line-height: 2.0rem;color: #788db4;">picture_as_pdf</i><span style="position:relative;top:-5px;">PDF</span></li>
					</ul>
					<span title="Export" class="drop-container dropdown-button icon-box htmlreportexportdiv" id="" data-activates="helpdrop52" id="exportdiv"><i class="material-icons" >grid_on</i><span class="actiontitle">Views</span></span>
					<ul id="helpdrop52" class="action-drop arrow_box head-help-ul" style="width: 200px;">
					<li id="supportcenter" style="color:white;background-color:#2c2f48;height: 60px;top: -8px; position: relative;border-top-left-radius: 4px;border-top-right-radius: 4px;"><i title="supportcenter" id="supportcenter" class="material-icons" style="font-size: 2rem;line-height: 1.9;color: #fff;">grid_on</i><span style="position:relative;top:-12px;color:#fff !important;font-size:0.9rem;font-family:
					"Nunito, sans-serif;"">Report Views</span></li>
						<li title="Tabular View" id="reporttv"><i title="Tabular" id="tabicon" class="material-icons" style="font-size: 1.4rem;line-height: 2.0rem;color: #788db4;">note</i><span style="position:relative;top:-5px;">Tabular View</span>
						</li>
						<li id="reportsv" title ="Summary View"><i title="Summary" id="summaryicon" class="material-icons" style="font-size: 1.4rem;line-height: 2.0rem;color: #788db4;">format_list_bulleted</i><span style="position:relative;top:-5px;">Summary View</span></li>
						<li id="reporthv" title="HTML View"><i title="htmlview" id="htmlicon" class="material-icons" style="font-size: 1.4rem;line-height: 2.0rem;color: #788db4;">drafts</i><span style="position:relative;top:-5px;">HTML View</span></li>
					</ul>
	                <span id="closeaddform" class="icon-box" title="Close"><i class="material-icons">close</i><span class="actiontitle">Close</span></span>
					<input type="hidden" name="reportcreationid" id="reportcreationid" value="" />
					<input type="hidden" name="reporttype" id="reporttype" value="TV" />
				</li>
			</ul>
		</div>
		<?php
			$device = $this->Basefunctions->deviceinfo();
			if($device=='phone') {
				echo '<div class="large-12 columns paddingzero forgetinggridname" id="customreportgrid"><div id="customreportgriddiv" class=" inner-row-content inner-gridcontent" style="">
						<!-- Table header content , data rows & pagination for mobile-->
					</div>
					<div class="footer-content footercontainer" id="customreportgriddivfooter">
						</div>
			   		 </div>';
			} else {
				echo '<div class="large-12" style="background-color:#f2f3fa">
      					<div class="large-12 columns forgetinggridname" id="customreportgrid" style="padding-left:0;padding-right:0;background-color:#f2f3fa;">
							<input type="hidden" name="hdnreportid" id="hdnreportid" value="" />
							<input type="hidden" name="actiontype" id="actiontype" value="" />
							<input type="hidden" name="hdntabularoverlay" id="hdntabularoverlay" value="" />
							<input type="hidden" name="hdntabularcondition" id="hdntabularcondition" value="" data-condition="" data-values="" />
							<input type="hidden" name="overlaystatus" id="overlaystatus" value="" />
      						<div class="touchgrid-container viewgridcolorstyle borderstyle" style="margin: 10px 10px 7px 15px;border:none;margin-top: -10px !important;">
								<div class="desktop row-content inner-gridcontent viewgridcolorstyle borderstyle" id="customreportgriddiv" style="max-width:2000px;">
									<!-- Table content[Header & Record Rows] for desktop-->
								</div>
								<div class="footer-content footercontainer" id="customreportgriddivfooter">
								</div>
      						</div>
			   			 </div>
						 <div class="large-12 small-12 medium-12 columns" style="position:absolute;">
							<div class="overlay large-12 columns mblnopadding" id="overlayreportsfilterdisplay" style="z-index:40;display: none;">	
								<div class="large-4 large-offset-8 columns" style="padding-right: 0em !important;height:100% !important;padding-left:0px !important;padding: 0px !important;">
									<div class="large-12 column paddingzero" style="position: relative;left: 4px;">
										<div id="filterdisplaymainviewformvalidate" class="validationEngineContainer">
										<div class="large-12 columns end" style="padding: 0px !important;">
											<div class="large-12 columns cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff">
								<div class="large-12 medium-12 columns paddingzero reportsearchfilteroverlay filterbox ui-draggable" style="margin: 10px 10px 10px 0px; touch-action: none; -moz-user-select: none;box-shadow: none;>
									<div class="large-12 columns noticombowidget-box filterheaderborderstyle">
										
										<div class="large-6 medium-4 columns end noticomboheader activeheader filertabbgcolor" id="filterview" style="padding-top:0.4rem!important;padding-left:0.7rem !important;padding-bottom:0.5rem!important" data-griddataid=""><span id="fiteractive" class="tab-titlemini sidebariconsmini active filtermini"><div class="large-6 columns sectionheaderformcaptionstyle" style="text-align: left;top: -10px !important;">Filter</div></span></div>
										
										<div class="large-3 medium-4 columns end noticomboheader activeheader filertabbgcolor" id="filtersearch" style="padding-top:0.4rem!important;padding-left:0.7rem !important;padding-bottom:0.5rem!important; left: 40px;" data-griddataid="">
											<input type="button" class="addkeyboard alertbtnyes" value="Search" tabindex="129">
										</div>
										
										<div class="large-3 medium-4 columns end noticomboheader activeheader filertabbgcolor" id="filterclose" style="padding-top:0.4rem!important;padding-left:0.7rem !important;padding-bottom:0.5rem!important" data-griddataid="">
											<input type="button" class="alertbtnno addsectionclose leftcloseposition" value="Close" tabindex="130">
										</div>
										
										<div class="large-12 medium-12 small-12 columns end filterheight filterbodyborderstyle sheepbar" data-x="false" data-y="true" id="fbuildercat1" style="box-shadow: none;padding:10px 10px;position:relative;overflow-y:auto;overflow-x:hidden;background-color:#eceff1;height: 565px;"><span id="reportfilterformconditionvalidation" class="validationEngineContainer">
											<div id="filtercolumndisplay">';?>
												<?php
												echo '</div>
										</div>
									</div>
								</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
      				</div>';
			}
		?>
		<div class="large-12" style="background-color:#f2f3fa">
      		<div class="large-12 columns forgetinggridname" id="htmlprintingdiv" style="margin: 0px 10px 10px 0px; max-width:2000px; background-color:#f2f3fa;overflow:auto;height: 650px;">
				
			</div>
		</div>
	</div>
</div>
	<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="tabularviewoverlay" style="overflow-y: auto;overflow-x: hidden;z-index:40;">	
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-10 columns large-centered ">
		<header class="header-content">
			<div class="large-6 medium-6 small-6 columns">
				<span class="grid-logo"><span >Reports Summary Detail</span></span>
			</div>
			<div class="large-6 medium-6 small-6 columns text-right mainaction">
				<span id="closetabularoverlay" class="icon-box" title="Close"><i class="material-icons">close</i></span>
			</div>		
		</header>		
			<?php
				$device = $this->Basefunctions->deviceinfo();
				if($device=='phone') {
					echo '<div class="large-12 columns paddingzero forgetinggridname" id="tabulargrid"><div id="tabulargriddiv" class=" inner-row-content inner-gridcontent" style="">
						<!-- Table header content , data rows & pagination for mobile-->
					</div>
					<div class="footer-content footercontainer" id="tabulargriddivfooter">
					</div>
				    </div>';
				} else {
					echo '<div class="large-12 columns forgetinggridname" id="tabulargrid" style="padding-left:0;padding-right:0;">
					<div class="desktop row-content inner-gridcontent" id="tabulargriddiv" style="">
					<!-- Table content[Header & Record Rows] for desktop-->
					</div>
					<div class="footer-content footercontainer" id="tabulargriddivfooter">
					</div>
				    </div>';
				}
			?>
			</div>
		</div>
	</div>
		<div class="large-12 columns" style="position:absolute;">
		<div class="overlay overflowhidden" id="newoverlayform" style="z-index:40;">	
			<?php
				$device = $this->Basefunctions->deviceinfo();
				$dataset['gridtitle'] = $gridtitle;
				$dataset['titleicon'] = $titleicon;
				$modtabgroup[0] = array("tabpgrpid"=>"1","tabgrpname"=>"Basic","usertabgrpname"=>"Basic","moduleid"=>"35","templateid"=>"2");
				$modtabgroup[1] = array("tabpgrpid"=>"2","tabgrpname"=>"Advanced","usertabgrpname"=>"Advanced","moduleid"=>"35","templateid"=>"2");
				$modtabgroup[2] = array("tabpgrpid"=>"3","tabgrpname"=>"HTML Details","usertabgrpname"=>"HTML Details","moduleid"=>"35","templateid"=>"2");
				//$modtabgroup[2] = array("tabpgrpid"=>"3","tabgrpname"=>"Group / Sort By","usertabgrpname"=>"Group / Sort By","moduleid"=>"35","templateid"=>"2");
				//$modtabgroup[3] = array("tabpgrpid"=>"4","tabgrpname"=>"Manual Data","usertabgrpname"=>"Manual Data","moduleid"=>"35","templateid"=>"2");
				$dataset['modtabgrp']=$modtabgroup;
				$dataset['formtype']= 'report';
				$this->load->view('Base/mainviewaddformheader',$dataset);
			?>
			<div class="large-12 columns addformunderheader">&nbsp;</div>
			<div class="large-12 columns addformcontainer scrollbarclass " style="background-color:#f2f3fa !important;min-height:520px;">
				<div class="row">&nbsp;</div>
				<div id="subformspan1" class="hiddensubform">
					<form method="POST" name="reportbasicdetails" class=""  id="reportbasicdetails">
						<span id="validatereportbasicdetails" class="validationEngineContainer clearbasicdetail" >
						<span id="validatereportbasiceditdetails" class="validationEngineContainer clearbasicdetail" >
							<span id="" class="validationEngineContainer">
								<div class="large-4 columns paddingbtm">
									<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
										<div class="large-12 columns headerformcaptionstyle">Basic Details</div>
										<div class="input-field large-12 columns">
											<input id="reportname" class="validate[required,funcCall[reportnamecheck]]" type="text" tabindex="" name="reportname">
											<label for="reportname">Report Name<span class="mandatoryfildclass">*</span></label>
										</div>
										<div class="static-field large-6 columns" style="">
											<label>Report Type<span class="mandatoryfildclass">*</span></label>
											<select class="chzn-select validate[required]" id="reporttypeid" name="reporttypeid" data-prompt-position="bottomLeft:14,36">
												<?php foreach($reporttype as $key):?>
												<option  value="<?php echo $key->reporttypeid;?>" ><?php echo $key->reporttypename;?></option>
												<?php endforeach;?>	
											</select>
										</div>
										<div class="static-field large-6 columns">
											<label>Report Folder<span class="mandatoryfildclass">*</span></label>
											<select class="chzn-select validate[required]" id="reportfolderid" name="reportfolderid" data-prompt-position="bottomLeft:14,36">
												<option value=""></option>	
											</select>
										</div>
										<span class="hidedisplay">
											<div class="input-field large-6 columns">
												<input id="schedulereport" class="checkboxcls radiochecksize " type="checkbox"  tabindex="102"  name="schedulereport" data-prompt-position="bottomLeft:14,36" data-hidname="schedulereport" value="">
												<label for="schedulereport">Schedule Report</label>
											</div>
										</span>
										<div class="static-field large-6 columns">
											<label>Menu Category<span class="mandatoryfildclass">*</span></label>
											<select class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" id="reportcategory" name="reportcategory" data-prompt-position="bottomLeft">
												<option value=""></option> 
												<?php foreach($module as $key):?>
												<option  value="<?php echo $key->moduleid;?>" ><?php echo strtoupper($key->menuname);?></option>
												<?php endforeach;?>		
											</select>
										</div>
										<div class="static-field large-6 columns">
											<label>Main Module Name<span class="mandatoryfildclass">*</span></label>
											<select class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" id="reportmodule" name="reportmodule" data-prompt-position="bottomLeft">
												<option value=""></option> 		
											</select>
										</div>
										<div id="reportmoduletabdiv" class="static-field large-12 columns hidedisplay">
											<label>Tab Group<span class="mandatoryfildclass">*</span></label>
											<select class="chzn-select" data-prompt-position="bottomLeft:14,36" id="reportmoduletab" name="reportmoduletab" data-prompt-position="bottomLeft">
												<option value=""></option> 		
											</select>
										</div>
										<div class="static-field large-12 columns">
											<label>Related Module</label>
											<select class="chzn-select"  multiple id="reportrelatedmodule" name="reportrelatedmodule" data-prompt-position="bottomLeft">
												<option value=""></option>									
											</select>
										</div>
										<div class="input-field large-12 columns">
											<textarea id="reportdescription" class="validate[maxSize[100]] materialize-textarea" data-prompt-position="bottomLeft" tabindex="" name="reportdescription"  style="min-height: 5.5rem !important;"></textarea>
											<label for="reportdescription">Description</label>
										</div>
										<div class="large-12 columns"> &nbsp </div>
									</div>
								</div>
								
							</span>
							</span>
						</span>
						<!-- hidden fields-->
						<input type="hidden" name="reportexportall" id=""reportexportall"" value="No" />
						<input type="hidden" name="reportconrowcolids" id="reportconrowcolids" value="0" />
						<input type="hidden" name="reportcalcrowcolids" id="reportcalcrowcolids" value="0" />
						<input type="hidden" name="hdnreportconditioncount" id="hdnreportconditioncount" value="" /><input type="hidden" name="hdnreportconditiondata" id="hdnreportconditiondata" value="" /><input type="hidden" name="hdngroupcolids" id="hdngroupcolids" value="" /><input type="hidden" name="hdnsortby" id="hdnsortby" value="" /><input type="hidden" name="hdngroupbyrange" id="hdngroupbyrange" value="" /><input type="hidden" name="hdnviewcolids" id="hdnviewcolids" value="" /><input type="hidden" name="hdntabularviewcolids" id="hdntabularviewcolids" value="" /><input type="hidden" name="hdnsummarviewcolids" id="hdnsummarviewcolids" value="" /><input type="hidden" name="hdnsummaramids" id="hdnsummaramids" value="" /><input type="hidden" name="hdnsummaramname" id="hdnsummaramname" value="" /><input type="hidden" name="hdndate_columnid" id="hdndate_columnid" value="" /><input type="hidden" name="hdndate_method" id="hdndate_method" value="" /><input type="hidden" name="hdndate_mode" id="hdndate_mode" value="" /><input type="hidden" name="hdndate_start" id="hdndate_start" value="" /><input type="hidden" name="hdndate_end" id="hdndate_end" value="" /><input type="hidden" name="hdnreportrelatedmodules" id="hdnreportrelatedmodules" value="" /><input type="hidden" name="hdnreportcreateconditionids" id="hdnreportcreateconditionids" value="" /><input type="hidden" name="hdnreportname" id="hdnreportname" value="" /><input type="hidden" name="hdnreportfolderid" id="hdnreportfolderid" value=""><input type="hidden" name="hdnreportmodule" id="hdnreportmodule" value=""><input type="hidden" name="hdnreportrelatedmodule" id="hdnreportrelatedmodule" value=""><input type="hidden" name="hdnreportdescription" id="hdnreportdescription" value="">
					</form>
					<div class="large-8 columns paddingbtm" id="columnviewlists">
						<div class="large-5 medium-5 columns paddingzero borderstyle" id="reportcalculationgridwidth" style="background-color:#fff;transition: box-shadow .25s;border-radius: 8px;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);">	
						<div class="large-12 columns headerformcaptionstyle">Select Columns</div>
						<select name="from[]" id="reportmaincolumnname" class="multiselectclass" size="8" multiple="multiple" name="reportmaincolumnname" style="border:none;">
						</select>
						</div>
						<div class="large-2 medium-2 columns multiselect-btns" style="top:55px !important;left: 16px !important;">
							<button type="button" id="reportmaincolumnname_rightSelected" class="btn btn-block"><i class="material-icons selectcolbtn">keyboard_arrow_right</i></button>
							<button type="button" id="reportmaincolumnname_leftSelected" class="btn btn-block"><i class="material-icons selectcolbtn">keyboard_arrow_left</i></button>
							<input type="button" class="btn multiselectupdown" data-selectddid = "reportmaincolumnname_to" value="Up">
							<input type="button" class="btn multiselectupdown" data-selectddid = "reportmaincolumnname_to" value="Down">
							<button type="button" id="reportmaincolumnname_rightAll" class="btn btn-block selectcolbtn"><i class="material-icons">fast_forward</i></button>
							<button type="button" id="reportmaincolumnname_leftAll" class="btn btn-block"><i class="material-icons selectcolbtn">fast_rewind</i></button>
							<input type="button" value="SUM" class="btn" id="btnsummary" name="brandsavebutton">
							
						</div>
						 <div class="large-5 medium-5 columns paddingzero ">
							<div class="large-12 columns paddingzero borderstyle" style="background-color:#fff;transition: box-shadow .25s;border-radius: 8px;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);">
								<div class="large-12 columns headerformcaptionstyle">Selected Columns</div>
								<select name="to[]" id="reportmaincolumnname_to" class="validate[required] multiselectclass" size="8" multiple="multiple" style="border:none;" ></select>
							</div>
								
						</div>
					</div>
				</div>
			
				<div id="subformspan2" class="hidedisplay hiddensubform">
					<div class="large-12 columns paddingzero hidedisplay">
						<div class="large-12 columns"> 
							<div class="large-12 columns cleardataform borderstyle paddingzero" style="background-color:#f5f5f5">
								<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0 0.4rem;">
									<span class="large-6 medium-6 small-6 columns" style="text-align:left">Standard Filter</span>
									<span class="large-6 medium-6 small-6 columns innergridicon" style="text-align:right;    margin-top: 5px;">
										<span id="standardreportconddeleteicon" title="Delete"><i class="icon24 icon24-delete"></i></span>
									</span>
								</div>
								<div class="static-field large-3 columns">
									<label>Date Column</label>
										<select data-placeholder="Select" data-prompt-position="bottomLeft:14,36" class="validate[required] chzn-select chosenwidth viewdropdownchange"  name="report_datefield" id="report_datefield">
										<option></option>
										</select> 
									<input type="hidden" name="" id="" value="" />
								</div>
								<div class="static-field large-3 columns">
									<label>Range</label>
										<select data-placeholder="Select" data-prompt-position="bottomLeft:14,36" class="validate[required] chzn-select chosenwidth viewdropdownchange"  name="report_range" id="report_range">
										<option value="" data-mode=""></option>
										<option value="custom" data-mode="custom">Custom</option>
										<optgroup label="Day">	
											<option value="-1 day" data-mode="instant">Yesterday</option>
											<option value="0 day" data-mode="instant">Today</option>
											<option value="1 day" data-mode="instant">Tomorrow</option>
											<option value="-6 day" data-mode="direct">Last 7 Days</option>
											<option value="-29 day" data-mode="direct">Last 30 Days</option>
											<option value="-59 day" data-mode="direct">Last 60 Days</option>
											<option value="-89 day" data-mode="direct">Last 90 Days</option>
											<option value="-119 day" data-mode="direct">Last 120 Days</option>
											<option value="6 day" data-mode="direct">Next 7 Days</option>
											<option value="29 day" data-mode="direct">Next 30 Days</option>
											<option value="59 day" data-mode="direct">Next 60 Days</option>
											<option value="89 day" data-mode="direct">Next 90 Days</option> 
											<option value="119 day" data-mode="direct">Next 120 Days</option>
										</optgroup>
										<optgroup label="Calendar Week">
											<option value="lastweek" data-mode="switch">Last Week</option>
											<option value="currentweek" data-mode="switch">Current Week</option>
											<option value="nextweek" data-mode="switch">Next Week</option>
										</optgroup>
										<optgroup label="Calendar Month">
											<option value="lastmonth" data-mode="switch">Last Month</option>
											<option value="currentmonth" data-mode="switch">Current Month</option>
											<option value="nextmonth" data-mode="switch">Next Month</option>
											<option value="currentandpreviousmonth" data-mode="switch">Current and Previous Month</option>
											<option value="currentandnextmonth" data-mode="switch">Current and Next Month</option>		
										</optgroup>
										<optgroup label="Before">
											<option value="-6" data-mode="before">Before 7 Days</option>
											<option value="-29" data-mode="before">Before 30 Days</option>
											<option value="-59" data-mode="before">Before 60 Days</option>
											<option value="-89" data-mode="before">Before 90 Days</option>
											<option value="-119" data-mode="before">Before 120 Days</option>
										</optgroup>
										</select> 
									<input type="hidden" name="" id="" value="" />
								</div>
								<div class="static-field large-3 columns viewcondclear reportdatepickerdiv" id="">
									<input type="text" class="datepicker" data-dateformater="<?php echo $appdateformat; ?>" id="report_startdate" name="report_startdate"  tabindex="" >
									<label for="report_startdate">Start Date</label>
								</div>
								<div class="static-field large-3 columns viewcondclear reportdatepickerdiv" id="">
									<input type="text" class="datepicker" data-dateformater="<?php echo $appdateformat; ?>" id="report_enddate" name="report_enddate"  tabindex="" >
									<label for="report_enddate">End Date</label>
								</div>
								<div class="static-field large-3 columns viewcondclear reportdatetimepickerdiv hidedisplay" id="">
									<input type="text" class="fordatepicicon"  id="report_startdatetime" name="report_startdatetime"  tabindex="" >
									<label for="report_startdatetime">Start Date</label>
								</div>
								<div class="static-field large-3 columns viewcondclear reportdatetimepickerdiv hidedisplay" id="">
									<input type="text" class="fordatepicicon" id="report_enddatetime" name="report_enddatetime"  tabindex="" >
									<label for="report_enddatetime">End Date</label>
								</div>
								<div class="large-12 columns" >&nbsp </div>
							</div>
						</div>
						<div class="large-12 columns" >&nbsp </div>
					</div>
					<div class="large-3" style="left: 15px !important;position: relative;">
						<div class="large-12 medium-5 columns paddingzero borderstyle" id="reportcalculationgridwidth" style="background-color:#fff;transition: box-shadow .25s;border-radius: 8px;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);">	
							<div class="large-12 columns headerformcaptionstyle">Select Columns</div>
							<select name="from[]" id="reportgroupby1" class="" size="10" multiple="multiple" name="reportmaincolumnname" style="border:none;height:18rem !important;">
							</select>
						</div>
					</div>
					<div class="large-1 medium-1 columns" style="top:-10px !important;position:relative;left: 10px !important;">
						<div class="large-12 columns input-field reportgroup" style="background:none;">
						<label>Range</label>
						<input id="groupbyrange" class="validate[required,custom[number],decval[0],max[15]] reportaddtextbox" tabindex="" name="groupbyrange" type="text" placeholder="Groupby Range" value="0">
						</div>
						<div class="large-12 columns " style="background:none;">
						<button type="button" id="reportgroupby1_rightSelected" class="btn btn-block" style="width: 100% !important;"><i class="material-icons selectcolbtn">keyboard_arrow_right</i></button>
						<button type="button" id="reportgroupby1_leftSelected" class="btn btn-block" style="width: 100% !important;"><i class="material-icons selectcolbtn">keyboard_arrow_left</i></button>
						<!--<button type="button" id="fieldsgroup" class="btn btn-block orderby" value="grp(asc)" style="width: 150% !important;">GRP</button>-->
						<button type="button" id="ascorder" class="btn btn-block orderby" value="asc" style="width: 100% !important;">ASC</button>
						<button type="button" id="descorder" class="btn btn-block orderby" value="desc" style="width: 100% !important;">DSC</button>
						<button type="button" id="reptransfericon" class="btn btn-block" value="trnf" style="width: 100% !important;">TRNF</button>
						<!--<button type="button" id="cleargrpfield" class="btn btn-block orderbyclear" value="clr" style="width: 150% !important;">Clear</button>-->
						</div>
						<div class="large-12 columns reportgroup hidedisplay" style="background:none;">
						<label>Transfer yes/no</label>
						<input id="transferyesno" class="validate[required]" style="color: #000000;" tabindex="" name="transferyesno" type="text" placeholder="Transfer yes/no" value="no">
						</div>
						<div class="large-12 columns" style="background:none;">
						<input type="button" class="btn multiselectupdown" data-selectddid = "reportgroupby1_to" value="Down" style="width: 100% !important;">
						<input type="button" class="btn multiselectupdown" data-selectddid = "reportgroupby1_to" value="Up" style="width: 100 !important;">
						</div>
						
						<div class="large-12 columns input-field reportgroup " style="background:none;">
						<label>Report</label>
						<input id="reportingmode" class="validate[required,custom[number],decval[0],max[15]] reportaddtextbox" tabindex="" name="reportingmode" type="text" placeholder="Report Mode" value="0">
						</div>	
						<div class="large-12 columns input-field reportgroup " style="background:none;">
						<label>Summary</label>
						<input id="summaryrollup" class="validate[required,custom[number],decval[0],max[15]] reportaddtextbox" tabindex="" name="summaryrollup" type="text" placeholder="Summary Mode" value="0">
						</div>		
					</div>
					<div class="large-8 columns end">
					<form id="reportcriteriaform" name="reportcriteriaform">
					<div id="reportformconditionvalidation" class='validationEngineContainer reportcondclear'>
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
							<div class="large-12 columns headerformcaptionstyle">Advanced Filter</div>
							<div class="static-field large-2 columns">
								<label>Field Name<span class="mandatoryfildclass">*</span></label>
								<select data-placeholder="Select" data-prompt-position="bottomLeft:14,36" class="validate[required] chzn-select chosenwidth viewdropdownchange"  name="reportcondcolumn" id="reportcondcolumn">
									<option></option>
								</select> 
								<input type="hidden" name="reportcondcolumnid" id="reportcondcolumnid" value="" />
								<input type="hidden" name="editconditionid" id="editconditionid" value="" />
							</div>
							<div class="static-field large-2 columns" id="reportaggregatecond">
								<label>Aggregate Method<span class="mandatoryfildclass">*</span></label>
								<select id="reportaggregate" class="validate[required] chzn-select chosenwidth dropdownchange select2-offscreen whitecss forsucesscls" name="reportaggregate"  data-placeholder="Select" data-prompt-position="bottomLeft:14,36" title="">
								<option value="0">Actual Values</option>
								</select>
							</div>
							<div class="static-field large-2 columns" id="reportconditioncond">
								<label>Condition<span class="mandatoryfildclass">*</span></label>
								<select id="reportcondition" class="validate[required] chzn-select chosenwidth dropdownchange select2-offscreen whitecss forsucesscls" name="reportcondition"  data-placeholder="Select" data-prompt-position="bottomLeft:14,36" title="">
								<option></option>
								<option value="Equalto">Equalto</option>
								<option value="NotEqual">NotEqual</option>
								<option value="Startwith">Startwith</option>
								<option value="Endwith">Endwith</option>
								<option value="Middle">Middle</option>
								</select>
							</div>
							<div class="input-field large-4 columns viewcondclear" id="reportcondvaluedivhid">
								<input type="text" class="" id="reportcondvalue" name="reportcondvalue"  >
								<label for="reportcondvalue">Value<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="static-field large-4 columns viewcondclear" id="reportddcondvaluedivhid">
								<label>Value<span class="mandatoryfildclass">*</span></label>
								<select data-placeholder="Select"  data-prompt-position="bottomLeft:14,36" class="validate[required] chzn-select chosenwidth dropdownchange"  name="reportddcondvalue" id="reportddcondvalue" multiple>
									<option></option>
								</select>  
							</div>
							<div class="input-field large-4 columns viewcondclear" id="reportdatecondvaluedivhid">
								<input type="text" class="validate[required]" data-dateformater="<?php echo $appdateformat; ?>" id="reportdatecondvalue" name="reportdatecondvalue">
								<label for="reportdatecondvalue">Value<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="static-field large-3 columns viewcondclear reportdatepickerdiv reportdatefield" id="startdatefieldcondvaluedivhid">
								<input type="text" class="fordatepicicon" data-dateformater="<?php echo $appdateformat; ?>" id="report_startdate" name="report_startdate"  tabindex="" >
								<label for="report_startdate">Start Date</label>
							</div>
							<div class="static-field large-3 columns viewcondclear reportdatepickerdiv reportdatefield" id="enddatefieldcondvaluedivhid">
								<input type="text" class="fordatepicicon" data-dateformater="<?php echo $appdateformat; ?>" id="report_enddate" name="report_enddate"  tabindex="" >
								<label for="report_enddate">End Date</label>
							</div>
							<div class="static-field large-2 columns" id="reportandorconddivhid" style="display:none;">
								<label>AND/OR Operation<span class="mandatoryfildclass condmandclass">*</span></label>
								<select id="reportandorcond" data-prompt-position="bottomLeft:14,36" class="validate[required] chzn-select chosenwidth dropdownchange select2-offscreen whitecss forsucesscls" name="reportandorcond" data-placeholder="Select" title="">
								<option></option>
								<option value="AND">AND</option>
								<option value="OR">OR</option>
								</select>
							</div>
							<div class="large-12 columns" style="text-align: center;top:10px !important;position:relative;">
									<input id="reportaddcondsubbtn" class="btn" type="button" value="Submit" name="" tabindex="">
									<input id="" class="btn hidedisplay" type="button" value="Submit" name="" tabindex="" style="display: none;">
							</div>								
						</div>
					</div>
				</form>
				<input type="hidden" class="" id="finalreportcondvalue" name="finalreportcondvalue">
				<input type="hidden" class="" id="finalreportcondvalueid" name="finalreportcondvalueid">
				<input type="hidden" class="" id="reportconditionoperate" name="reportconditionoperate">
				<input type="hidden" name="reportcolstatus" id="reportcolstatus" value="2"/>
				</div>
				<div class="large-8 columns paddingbtm" style="top:20px !important;">
					<div class="large-12 columns paddingzero">
						<div class="large-12 columns viewgridcolorstyle paddingzero borderstyle">
							<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0 0.4rem;">
								<span class="large-6 medium-6 small-6 columns" style="text-align:left">Criteria List</span>
								<span class="large-6 medium-6 small-6 columns innergridicon" style="text-align:right;    margin-top: 5px;">
									<span class="icon-box"><i id="reportcondediticon" class="material-icons editiconclass" title="Edit">edit</i></span>
									
									<span id="reportconddeleteicon" title="Delete"><i class="material-icons ">delete</i></span>
								</span>
							</div>
							<?php
						$device = $this->Basefunctions->deviceinfo();
						if($device=='phone') {
							echo '<div class="large-12 columns paddingzero forgetinggridname" id="reportcreateconditiongridwidth"><div class="desktop row-content inner-gridcontent" id="reportcreateconditiongrid" style="height:450px;top:0px;">
								<!-- Table header content , data rows & pagination for mobile-->
							</div>
							<footer class="inner-gridfooter footercontainer" id="viewcreateconditiongridfooter">
								<!-- Footer & Pagination content -->
							</footer></div>';
						} else {
							echo '<div class="large-12 columns forgetinggridname" id="reportcreateconditiongridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="reportcreateconditiongrid" style="max-width:2000px; height:450px;top:0px;">
							<!-- Table content[Header & Record Rows] for desktop-->
							</div>
							<!-- <div class="inner-gridfooter footer-content footercontainer" id="viewcreateconditiongridfooter">
								Footer & Pagination content
							</div> -->
							</div>';
						}
					?>
						</div>
					</div>
				</div>
				<div class="large-3 medium-3 columns paddingzero" style="top: -310px !important;left:15px !important;">
							<div class="large-12 columns paddingzero borderstyle" style="background-color:#fff;transition: box-shadow .25s;border-radius: 8px;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);">
							<div class="large-12 columns headerformcaptionstyle">Group/Filter By Columns</div>
							<select name="to[]" id="reportgroupby1_to" class="" size="8" multiple="multiple" style="height:18rem !important;"></select>
							</div>		
						</div>
				
				</div>
				
				<div id="subformspan3" class="hiddensubform hidedisplay">
					<form method="POST" name="" action ="" id="">
						<span id="" class="validationEngineContainer" >
							<span id="" class="validationEngineContainer">
								<div class="large-10 columns">
									<div class="large-2 columns static-field " style="background:none;">
										<label>Print Template</label>
										<input id="printtemplateid" class="reportaddtextbox" tabindex="" name="printtemplateid" type="text" placeholder="Template Id" value="0">
									</div>
									<div class="static-field large-2 columns" style="">
										<label>Report Field Mode<span class="mandatoryfildclass">*</span></label>
										<select class="chzn-select" id="htmlfieldmodeid" name="htmlfieldmodeid" data-prompt-position="bottomLeft:14,36">
											<option value="1" data-uitypeid="8" >Date Field</option>
										</select>
									</div>
									<div class="static-field large-2 columns" style="">
										<label>Field Condition<span class="mandatoryfildclass">*</span></label>
										<select class="chzn-select" id="htmlfieldcondition" name="htmlfieldcondition" data-prompt-position="bottomLeft:14,36">
											<option></option>
										</select>
									</div>
									<div class="input-field large-3 columns hidedisplay" id="htmlfieldconditionvaluediv">
										<input type="text" class="" id="htmlfieldconditionvalue" name="htmlfieldconditionvalue"  >
										<label for="htmlfieldconditionvalue">Value<span class="mandatoryfildclass">*</span></label>
									</div>
									<div class="static-field large-2 columns reportdatepickerdiv reportdatefield" id="htmlreportfromdatediv">
										<input type="text" class="fordatepicicon" data-dateformater="<?php echo $appdateformat; ?>" id="htmlreportfromdate" name="htmlreportfromdate"  tabindex="" >
										<label for="htmlreportfromdate">From Date</label>
									</div>
									<div class="static-field large-2 columns reportdatepickerdiv reportdatefield" id="htmlreporttodatediv">
										<input type="text" class="fordatepicicon" data-dateformater="<?php echo $appdateformat; ?>" id="htmlreporttodate" name="htmlreporttodate"  tabindex="" >
										<label for="htmlreporttodate">To Date</label>
									</div>
								</div>
								<div class="large-10 columns">
									<div class="static-field large-2 columns" style="">
										<label>Account Name<span class="mandatoryfildclass">*</span></label>
										<select class="chzn-select" id="htmlaccountid" name="htmlaccountid" data-prompt-position="bottomLeft:14,36">
											<option value="1" data-accounttypeid="" data-label="" >Select</option>
											<?php foreach($account->result() as $key):?>
												<option value="<?php echo $key->accountid;?>" data-accounttypeid="<?php echo $key->accounttypeid;?>" data-label="<?php echo $key->accountname;?>">
												<?php echo $key->accountname;?></option>
											<?php endforeach;?>
										</select>
									</div>
								</div>
							</span>
						</span>
					</form>
				</div>
				<!--
				<div id="subformspan3" class="hiddensubform hidedisplay">
					<form method="POST" name="" action ="" id="">
						<span id="" class="validationEngineContainer" >
							<span id="" class="validationEngineContainer">
							<div class="large-10 columns">
								<div class="large-5 medium-5 columns paddingzero borderstyle" id="reportcalculationgridwidth" style="background-color:#fff;transition: box-shadow .25s;border-radius: 8px;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);">	
									<div class="large-12 columns headerformcaptionstyle">Select Columns</div>
									<select name="from[]" id="reportgroupby1" class="" size="10" multiple="multiple" name="reportmaincolumnname" style="border:none;">
									</select>
								</div>
								<!--
								<div class="large-5 medium-5 columns paddingzero ">
									<div class="large-12 columns paddingzero borderstyle" style="background-color:#fff;transition: box-shadow .25s;border-radius: 8px;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);">
									<div class="large-12 columns headerformcaptionstyle">Select Columns</div>
									<select name="to[]" id="reportgroupby1_to" class="" size="8" multiple="multiple"></select>
									</div>
									
								</div>
								-->
								<!--</div>
							</span>
						</span>
					</form>
				</div>	-->
				<div id="subformspan4" class="hidedisplay hiddensubform hidedisplay">
				<div class="large-12 columns paddingzero">
					<div class="large-12 columns input-field reportgroup" style="background-color:#fff">
						<label>Manual Join</label>
						<input id="manualjoin" class="reportaddtextbox" tabindex="" name="manualjoin" type="text" placeholder="Manual Join" value="">
					</div>
				</div>
				<div class="large-12 columns paddingzero">
					<div class="large-12 columns input-field reportgroup" style="background-color:#fff">
						<label>Where Clause</label>
						<input id="whereclausestring" class="reportaddtextbox" tabindex="" name="whereclausestring" type="text" placeholder="Where Clause" value="">
					</div>
				</div>
				<div class="large-12 columns paddingzero">
					<div class="large-12 columns input-field reportgroup" style="background-color:#fff">
						<label>Having Clause</label>
						<input id="havingstring" class="reportaddtextbox" tabindex="" name="havingstring" type="text" placeholder="Having Clause" value="">
					</div>
				</div>
				<div class="large-12 columns paddingzero">
					<div class="large-12 columns input-field reportgroup" style="background-color:#fff">
						<label>Having Select</label>
						<input id="havingselect" class="reportaddtextbox" tabindex="" name="havingselect" type="text" placeholder="Having Select" value="">
					</div>
				</div>
				</div>
				<div id="subformspan5" class="hidedisplay hiddensubform">
				</div>
			</div>
		</div>
	</div>
</div>
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay alertsoverlay overlayalerts" id="newoverlay2">	
			<div class="row show-for-medium-up">&nbsp;</div>
			<div class="row show-for-medium-up">&nbsp;</div>
			<div class="row show-for-medium-up">&nbsp;</div>
			<div class="row show-for-medium-up">&nbsp;</div>		
			<div class="row show-for-medium-up">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="large-3 medium-6 small-10 large-centered medium-centered small-centered columns">
				<div class="row">&nbsp;</div>
				<div class="alert-panel" style="background-color:#617d8a">
					<div class="alertmessagearea" >
						<div class="alert-title">Reports summary</div>
						<div class="sectionpanel" ">
						<div class="alert-message alertinputstyle" id="aggregatemethod">
	
						</div>
						</div>
					</div>
					<div class="alertbuttonarea">
						<span class="firsttab" tabindex="1000"></span>
						<input type="button" id="btnsummaryfieldcheck" name="btnsummaryfieldcheck" tabindex="1001" value="submit" class="alertbtn  ffield" >
						<input type="button" id="alertsfcloseno" name="" value="Cancel" tabindex="1002" class="flloop alertbtn alertsoverlaybtn" >
						<span class="lasttab" tabindex="1003"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay alertsoverlay overlayalerts" id="exportovelay">	
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="large-3 medium-6 small-10 large-centered medium-centered small-centered columns">
				<div class="row">&nbsp;</div>
				<div class="alert-panel" style="background-color:#617d8a">
					<div class="alertmessagearea" >
						<div class="row">&nbsp;</div>
						<div class="alert-message alertinputstyle" style="text-align:center;">Do You want to export all data?</div>
					</div>
					<div class="alertbuttonarea">
						<span class="firsttab" tabindex="1000"></span>
						<input type="button" id="exportallyes" name="exportallyes" tabindex="1001" value="Yes" class="alertbtnyes  ffield" >
						<input type="button" id="exportallno" name="exportallno" value="No" tabindex="1002" class="flloop alertbtnno alertsoverlaybtn" >
						<span class="lasttab" tabindex="1003"></span>
					</div>
					<div class="row">&nbsp;</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document).ready(function() {
			var windheight = $( window ).height();
			var repgridheight = windheight -145 ;
		});
	</script>
	<!--<script src="<?php //echo base_url();?>js/plugins/datepicker.standalone.js"></script>
	<script src="<?php //echo base_url();?>js/plugins/moment.min.js"></script>-->
<script type="text/javascript">
$(document).ready(function($) {
    $('#reportmaincolumnname').multiselect({
        search: {
            left: '<input type="text" name="q" class="form-control large-12 columns searchbox" placeholder="Search..." />',
            right: '<input type="text" name="q" class="form-control large-12 columns searchbox" placeholder="Search..." />',
        },
		 keepRenderingSort: true
    });
	$('#reportgroupby1').multiselect({
        search: {
            left: '<input type="text" name="q1" class="form-control large-12 columns searchbox" placeholder="Search..." />',
            right: '<input type="text" name="q1" class="form-control large-12 columns searchbox" placeholder="Search..." />',
        },
		 keepRenderingSort: true
    });
	$('.multiselectupdown').click(function(){
        var selectddid = $(this).data("selectddid");
		var $op = $('#'+selectddid+' option:selected');
        $this = $(this);
        if($op.length){
            ($this.val() == 'Up') ? 
                $op.first().prev().before($op) : 
                $op.last().next().after($op);
        }
    });
});
</script>
