<style type="text/css">
	#folderoperationgrid .gridcontent{
		height: 63vh !important;
	}
</style>
<div class="large-12 columns paddingzero">
	<div class="" id="folderoverlaydivid">
		<?php
			$device = $this->Basefunctions->deviceinfo();
			if($device=='phone') {
				echo '<div class="large-12 columns headercaptionstyle headerradius paddingzero">
						<div class="large-6 medium-5 small-12 columns headercaptionleft">
							<span  data-activates="slide-out" class="headermainmenuicon button-collapse" title="Menu"><i class="material-icons">folder</i></span>
							<span class="gridcaptionpos"><span>Folder</span></span>
						</div>
						<div class="large-6 medium-7 small-12 columns centertext addformheadericonstyle addformaction">
						<span id="closefoldercrudoverlay" class="icon-box" title="Close">
						<input id name value="Close" class="alertbtnno summaryicon ffield" type="button">
						</span>';
				echo 	'</div>';
				echo '</div>';
			} else {
				echo '<div class="large-12 columns headercaptionstyle headerradius  paddingzero">
						<div class="large-6 medium-6 small-5 columns headercaptionleft">
							<span  data-activates="slide-out" class="headermainmenuicon button-collapse" title="Menu"><i class="material-icons">folder</i></span>
							<span class="gridcaptionpos"><span>Folder</span></span>
						</div>
						<div class="large-6 medium-6 small-7 columns righttext addformheadericonstyle addformaction">
							<span id="closefoldercrudoverlay" class="icon-box" title="Close">
						<input id name value="Close" class="alertbtnno summaryicon ffield" type="button">
						</span>';
							
				echo 	'</div>';
				echo '</div>';
			}
		?>		
		<!-- tab group creation -->
		<div class="large-12 columns tabgroupstyle">
			<ul class="tabs" data-tab="">
				<li id="tab1" class="sidebariconstab tab-title active ftab" data-subform="1">
					<span>Folder</span>
				</li>
			</ul>
		</div>
		<div class="large-12 columns addformunderheader">&nbsp;</div>
		<div class="large-12 columns scrollbarclass addformcontainer dataimporttouchcontainer">
		<form id="reportfolderform" name="reportfolderform" class="">
		<div id="addreportfoldervalidate" class="validationEngineContainer">
		<div id="editreportfoldervalidate" class="validationEngineContainer">
		<div class="large-12 medium-12 small-12 large-centered medium-centered small-centered columns cleardataform paddingzero">
			<div class="large-12 column paddingzero">
				<?php if($device=='phone') {?>
				<div class="closed effectbox-overlay effectbox-default" id="groupsectionoverlay">
				<?php }?>
				<div class="row mblhidedisplay">&nbsp;</div>
				<div class="large-4 columns end paddingbtm">
					<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
						<?php if($device=='phone') {?>
						<div class="large-12 columns sectionheaderformcaptionstyle">Folder Details</div>
						<div class="large-12 columns sectionpanel">
							<?php  } else { ?>
						<div class="large-12 columns headerformcaptionstyle">Folder Details </div>
						<?php  	} ?> 
						<div class="input-field large-12 columns">
							<input type="text" id="reportsfoldername" name="reportsfoldername" value="" class="validate[required,maxSize[100],funcCall[reportfoldernamecheck]]" tabindex="101" />
							<label for="reportsfoldername" >Folder Name<span class="mandatoryfildclass">*</span></label>
						</div>
						<div class="input-field large-12 columns">
							<textarea id="reportfolderdescription" class="materialize-textarea validate[maxSize[200]]" name="reportfolderdescription" value="" tabindex="102"> </textarea>
							<label for="reportfolderdescription">Description</label>
						</div>
						<div class="large-6 columns"> 
							<input type="checkbox" data-hidname='reportfoldersetpublic' id="reportfoldersetpublic" name="reportfoldersetpublic" class="checkboxcls filled-in" value="" tabindex="103"/>
							<label for="reportfoldersetpublic">Set As Public</label>
							<input type="hidden" id="editprimarydataid" name="editprimarydataid"/>
						</div>
						<div class="large-6 columns">
							<input type="checkbox" id="reportfoldersetdefault" name="reportfoldersetdefault"  data-hidname='reportfoldersetdefault' class="checkboxcls filled-in" value=""tabindex="104" />
							<label for="reportfoldersetdefault">Default</label>
						</div>
						<?php if($device=='phone') { ?>
						</div>
						<div class="divider large-12 columns"></div>
						<div class="large-12 columns submitkeyboard sectionalertbuttonarea" style="text-align: right">
								<input id="addreportfolderbtn" class="alertbtn addkeyboard" type="button" value="Submit" name="addreportfolderbtn" tabindex="105">
								<input id="editreportfolderbtn" class="alertbtn hidedisplay updatekeyboard" type="button" value="Submit" name="editreportfolderbtn" tabindex="105" style="display: none;">
								<input id="addsectionclose"class="alertbtn addsectionclose cancelkeyboard" type="button" value="Cancel" name="cancel" tabindex="106">
								
							</div>
								
						<?php } else { ?> 
						<div class="large-12 columns" style="text-align: right">
							<label> </label>
							<input id="addreportfolderbtn" class="btn" type="button" value="Submit" name="" tabindex="105">
							<input id="editreportfolderbtn" class="btn" type="button" value="Submit" name="" tabindex="106" >
						</div>
						<?php } ?>					
					</div>
				</div>
				<?php if($device=='phone') {?>
				</div>
				<?php }?> 
				<div class="large-8 columns paddingbtm">
					<div class="large-12 columns paddingzero">
						<div class="large-12 columns viewgridcolorstyle paddingzero borderstyle" style="top:5px !important;">
							<div class="large-12 columns headerformcaptionstyle" style="height:auto;padding: 0.2rem 0 0rem;">
								<span class="large-6 medium-6 small-6 columns lefttext" >Folder List</span>
								<span class="large-6 medium-6 small-6 columns righttext">
									<?php if($device=='phone') {?>
									  <span id="folderaddicon" class="icon-box" title="Add" ><i class="material-icons">add</i></span>
									  <?php }?> 
									<span title="Edit" id="reportfolderediticon" class="icon-box"><i class="material-icons">edit</i></span>
									<span title="Delete" id="reportfolderdeleteicon" class="icon-box deleteiconclass"><i class="material-icons">delete</i></span>
								</span>
							</div>
							<?php
							$device = $this->Basefunctions->deviceinfo();
							if($device=='phone') {
								echo '<div class="large-12 columns paddingzero forgetinggridname" id="folderoperationgridwidth"><div id="folderoperationgrid" class=" inner-row-content inner-gridcontent" style="max-width:2000px; height:70vh !important;top:0px;">
									<!-- Table header content , data rows & pagination for mobile-->
								</div>
								<footer class="inner-gridfooter footercontainer" id="folderoperationgridfooter">
									<!-- Footer & Pagination content -->
								</footer></div>';
							} else {
								echo '<div class="large-12 columns forgetinggridname" id="folderoperationgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="folderoperationgrid" style="max-width:2000px; height:70vh !important;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
								<div class="inner-gridfooter footer-content footercontainer" id="folderoperationgridfooter">
									<!-- Footer & Pagination content -->
								</div></div>';
							}
						?>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div></div>
		</form>
		</div>
	</div>
</div>

