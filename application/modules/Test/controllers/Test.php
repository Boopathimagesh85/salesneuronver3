<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Test extends MX_Controller {
	function __construct() {
		parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Test/Testmodel');
		$this->load->view('Base/formfieldgeneration');
	}
	public function index() {
		$moduleid= array(5001);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp']=$this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp']=$this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle']=$this->Basefunctions->gridtitleinformationfetch($moduleid);
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids']=$viewmoduleid;
		$data['filedmodids']=$viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Test/testview',$data);
	}
	public function newdatacreate() {
		$this->Testmodel->newdatacreatemodel();
	}
	public function fetchformdataeditdetails() {
		$moduleid= array(5001);
		$this->Testmodel->informationfetchmodel($moduleid);
	}
	public function datainformationupdate() {
		$this->Testmodel->datainformationupdatemodel();
	}
	public function deleteinformationdata() {
		$moduleid= array(5001);
		$this->Testmodel->deleteoldinformation($moduleid);
	}
}