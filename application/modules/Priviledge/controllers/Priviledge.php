<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Priviledge extends MX_Controller
{
	//To load the Register View
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Priviledge/Priviledgemodel');
		$this->load->view('Base/formfieldgeneration');
    }
	public function index() {
		$moduleid = array(109);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$data['branchid'] = $this->Basefunctions->branchid;
		$data['companyid'] = $this->Basefunctions->singlefieldfetch('companyid','branchid','branch',$this->Basefunctions->branchid);
		$industryid = $this->Basefunctions->industryid;
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$this->load->view('Priviledge/priviledgeview',$data);
	}
	//Create New data
	public function newdatacreate() {
    	$this->Priviledgemodel->newdatacreatemodel();
    }
	//information fetch
	public function fetchformdataeditdetails() {
		$moduleid = array(109);
		$this->Priviledgemodel->informationfetchmodel($moduleid);
	}
	//Update old data
    public function datainformationupdate() {
        $this->Priviledgemodel->datainformationupdatemodel();
    }
	//delete old information
    public function deleteinformationdata() {
		$moduleid = array(109);
        $this->Priviledgemodel->deleteoldinformation($moduleid);
    }
   //product charge grid data fetch
    public function productsearchgridheaderinformationfetch() {
    	$width = $_GET['width'];
    	$height = $_GET['height'];
    	$modname = $_GET['modulename'];
    	$moduleid = $_GET['moduleid'];
    	$checkbox = 'true';
    	$colinfo = $this->Priviledgemodel->productsearchgridheaderinformationfetchmodel($moduleid);
    	$device = $this->Basefunctions->deviceinfo();
    	if($device=='phone') {
    		$datas = mobileviewformtogriddatagenerate($colinfo,$modname,$width,$height);
    	} else {
    		$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height,$checkbox);
    	}
    	echo json_encode($datas);
    }
    //product charge grid overlay fetch
    public function productgriddatafetch() {
    	$this->Priviledgemodel->productgriddatafetchmodel();
    }
}