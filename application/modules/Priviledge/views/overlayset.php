<!--Product Search Grid---->
<div class="large-6 columns" style="position:absolute;">
	<div class="overlay" id="productsearchoverlay" style="overflow-y: auto;overflow-x: hidden;z-index:40;">		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="large-6 medium-6 small-11 large-centered medium-centered small-centered columns">
			<div class="row">
			<div class="large-12 columns sectionheaderformcaptionstyle" style="line-height:1rem;">
				Product Search
			</div>
			<?php
				$device = $this->Basefunctions->deviceinfo();
				if($device=='phone') {
					echo '<div class="large-12 columns paddingzero forgetinggridname" id="productsearchgridwidth"><div id="productsearchgrid" class=" inner-row-content inner-gridcontent" style="max-width:2000px; height:300px;top:0px;">
						<!-- Table header content , data rows & pagination for mobile-->
					</div>
					<!--<footer class="inner-gridfooter footercontainer" id="productsearchgridfooter">
						 Footer & Pagination content 
					</footer>--></div>';
				} else {
					echo '<div class="large-12 columns forgetinggridname" id="productsearchgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="productsearchgrid" style="max-width:2000px; height:300px;top:0px;">
					<!-- Table content[Header & Record Rows] for desktop-->
					</div>
					<!-- <div class="inner-gridfooter footer-content footercontainer" id="productsearchgridfooter">
						Footer & Pagination content 
					</div>--></div>';
				}
			?>
			</div>
			<div class="row" style="background:#f5f5f5">&nbsp;</div>
			<div class="row" style="background:#f5f5f5">
				<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
					<span class="firsttab" tabindex="1000"></span>
					<input type="button" id="productsearchsubmit" name="" value="Submit" class="alertbtn" tabindex="1001">	
					<input type="button" id="productsearchclose" name="" value="Cancel" tabindex="1002" class="alertbtn  alertsoverlaybtn" >
					<span class="lasttab" tabindex="1003"></span>
				</div>
			</div>
		</div>
	</div>
</div>
