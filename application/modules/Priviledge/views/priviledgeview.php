<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
</head>
<body class="previledgeclass">
	<?php
		$device = $this->Basefunctions->deviceinfo();
		$dataset['gridenable'] = "yes"; //no
		$dataset['griddisplayid'] = "previledgecreationview";
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['spanattr'] = array();
		$dataset['gridtableid'] = "priviledgegrid";
		$dataset['moduleid'] = $moduleids;
	   	$dataset['forminfo'] = array(array('id'=>'priviledgeformadd','class'=>'hidedisplay','formname'=>'priviledgeform'));
		if($device=='phone') {
			$this->load->view('Base/gridmenuheadermobile',$dataset);
			$this->load->view('Base/overlaymobile');
			$this->load->view('Base/viewselectionoverlay');
		} else {
			$this->load->view('Base/gridmenuheader',$dataset);
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/basedeleteform');
		$this->load->view('Base/modulelist');
	?>
	</body>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js File -->
	<script src="<?php echo base_url();?>js/Priviledge/priviledge.js" type="text/javascript"></script>
	<!--For Tablet and Mobile view Dropdown Script-->
	<script>
		$(document).ready(function() {			
			$("#tabgropdropdown").change(function() {
				var tabgpid = $("#tabgropdropdown").val();
				$('.sidebaricons[data-subform="'+tabgpid+'"]').trigger('click');
				$('.addformcontainer').animate({scrollTop:0},'slow');	
			});
		});
	</script>
</html>