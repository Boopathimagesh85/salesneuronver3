<div class="large-12 columns paddingzero formheader">
	<?php
		$this->load->view('Base/mainviewaddformheader');
	?>
</div>
<div class="large-12 columns addformunderheader">&nbsp;</div>
<div class="large-12 columns scrollbarclass addformcontainer tabtouchaddcontainer">
	<div class="row mblhidedisplay">&nbsp;</div>
	<form method="POST" name="dataaddform" class="" action ="" id="dataaddform">
		<span id="formaddwizard" class="validationEngineContainer" >
			<span id="formeditwizard" class="validationEngineContainer">
				<?php
					//function call for form fields generation
					formfieldstemplategenerator($modtabgrp);
				?>
			</span>
		</span>
		<!--hidden values-->
		<?php
			echo hidden('primarydataid','');
			echo hidden('sortorder','');
			echo hidden('sortcolumn','');
			$value = '';
			echo hidden('resctable',$value);
			//hidden text box view columns field module ids
			$modid = $this->Basefunctions->getmoduleid($filedmodids);
			echo hidden('viewfieldids',$modid);
			echo hidden('branchidval',$branchid);
			echo hidden('companyidval',$companyid);
			echo hidden('searchbasedproductid','');
		?>
	</form>
</div>
<!--Overlay-->
<div>
	<?php $this->load->view('Priviledge/overlayset');?>
</div>