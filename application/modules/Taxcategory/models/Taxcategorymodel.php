<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Taxcategorymodel extends CI_Model{
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//tax master create
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['taxcategoryelementsname']);
		$formfieldstable = explode(',',$_POST['taxcategoryelementstable']);
		$formfieldscolmname = explode(',',$_POST['taxcategoryelementscolmn']);
		$elementpartable = explode(',',$_POST['taxcategoryelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
		$setdefault = $_POST['setdefault'];
		$primaryid = $this->db->insert_id();
		$this->setdefaultupdate($setdefault,$primaryid);
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' created TaxCategory - '.$_POST['taxmastername'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,220);
		echo 'TRUE';
	}
	//Retrive tax master data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['taxmasterelementsname']);
		$formfieldstable = explode(',',$_GET['taxmasterelementstable']);
		$formfieldscolmname = explode(',',$_GET['taxmasterelementscolmn']);
		$elementpartable = explode(',',$_GET['taxmasterelementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['taxmasterprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	//tax master update
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['taxcategoryelementsname']);
		$formfieldstable = explode(',',$_POST['taxcategoryelementstable']);
		$formfieldscolmname = explode(',',$_POST['taxcategoryelementscolmn']);
		$elementpartable = explode(',',$_POST['taxcategoryelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['taxcategoryprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		$setdefault = $_POST['setdefault'];
		$this->setdefaultupdate($setdefault,$primaryid);
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' updated TaxCategory - '.$_POST['taxmastername'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$activity ,$userid,220);
		echo $result;
	}
	//tax master delete
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['taxmasterelementstable']);
		$parenttable = explode(',',$_GET['taxmasterelementspartable']);
		$id = $_GET['taxmasterprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=array('tax');
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		//delete other status
		$taxnamedelete = array('status'=>$this->Basefunctions->deletestatus,'lastupdateuserid'=>$this->Basefunctions->userid,'lastupdatedate'=>date($this->Basefunctions->datef));
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$taxcatgname = $this->Basefunctions->singlefieldfetch('taxmastername','taxmasterid','taxmaster',$id);
		$activity = ''.$user.' deleted TaxCategory - '.$taxcatgname.'';
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				//retrieve charge territory
				$taxids=array();
				$tax=$this->db->query("SELECT taxid from tax WHERE taxmasterid =$id AND status = 1");
				foreach($tax->result() as $info){
					$taxids[] = $info->taxid;
				}
				$this->Crudmodel->outerdeletefunctionwithoutrestrict($partabname,$primaryname,$ctable,$id);
				if(count($taxids) > 0){
					$this->db->where_in('taxid',$taxids);
					$this->db->update('taxterritories',$taxnamedelete);
				}
				$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,220);
				echo "TRUE";
			}
		} else {
			//retrieve charge territory
			$taxids=array();
			$tax=$this->db->query("SELECT taxid from taxs WHERE taxmasterid =$id AND status = 1");
			foreach($tax->result() as $info){
				$taxids[] = $info->taxid;
			}
			$this->Crudmodel->outerdeletefunctionwithoutrestrict($partabname,$primaryname,$ctable,$id);			
			if(count($taxids) > 0){
				$this->db->where_in('taxid',$taxids);
				$this->db->update('taxterritories',$taxnamedelete);
			}
			$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,220);
			echo "TRUE";
		}
	} 	
	//set default update
	public function setdefaultupdate($setdefault,$taxmasterid) {
		if($setdefault != 'No') {
			$updatearray = array('setdefault'=>'No');
			$this->db->where_not_in('taxmaster.taxmasterid',$taxmasterid);
			$this->db->update('taxmaster',$updatearray);
		}
	}
	//tax category reload in tax name
	public function fetchdddataviewddvalmodel() {
		$i = 0;
		$fieldname = $_GET['dataname'];
		$fieldid = $_GET['dataid'];
		$tablename = $_GET['datatab'];
		$moduleid = $_GET['moduleid'];
		$this->db->select("$fieldid,$fieldname,setdefault");
		$this->db->from("$tablename");
		$this->db->where("$tablename".'.status',1);
		$result=$this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data[$i]=array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname,'setdefault'=>$row->setdefault);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
}