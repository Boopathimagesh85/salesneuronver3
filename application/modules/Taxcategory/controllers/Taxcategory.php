<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Taxcategory extends MX_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Taxcategory/Taxcategorymodel');	
    }
    //first basic hitting view
    public function index() {        
    	$moduleid =  array(220);
    	sessionchecker($moduleid);
		//action
		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(220);
		$viewmoduleid = array(220);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['moddashboardtabgrp'] = $this->Basefunctions->moddashboardtabgrpgeneration($moduleid);
		$data['moddbfrmfieldsgen'] = $this->Basefunctions->dashboardmodsecfieldsgeneration($moduleid);
      	$this->load->view('Taxcategory/taxcategoryview',$data);
	}
	//create tax master
	public function newdatacreate() {  
    	$this->Taxcategorymodel->newdatacreatemodel();
    }
	//information fetch tax master
	public function fetchformdataeditdetails() {
		$moduleid = 220;
		$this->Taxcategorymodel->informationfetchmodel($moduleid);
	}
	//update tax master
    public function datainformationupdate() {
        $this->Taxcategorymodel->datainformationupdatemodel();
    }
	//delete tax master
    public function deleteinformationdata() {
		$moduleid = 220;
		$this->Taxcategorymodel->deleteoldinformation($moduleid);
    }
    //tax category reload in tax name
    public function fetchdddataviewddval(){
    	$this->Taxcategorymodel->fetchdddataviewddvalmodel();
    }
}