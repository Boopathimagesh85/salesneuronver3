<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="lostoverlay">	
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="overlaybackground">
			<div class="alert-panel">
				<div class="alertmessagearea">
					<div class="alert-message">
						<div class="row">&nbsp;</div>
						<span class="firsttab" tabindex="1000"></span>
						<div id="descriptiondivhid" class="input-field large-12 columns" id="stopnow">
							<textarea id="lostreason" class="materialize-textarea ffield"  data-prompt-position="topLeft" tabindex="1001" name="description"></textarea>
							<label for="lostreason">Reason</label>
						</div>
						<div class="" id="stoprevert">
							<span class="" style="	color: #5e6d82;font-family: segoe ui;font-size: 0.9rem;" id="stopmessage">Quotation Already Lost.Do u want revert to Draft?</span>
						</div>
					</div>
				</div>
				<div class="alertbuttonarea">
					<input type="button" id="lostyes" name="" value="Yes" tabindex="1002" class="alertbtnyes" >	
					<input type="button" id="lostno" name="" value="No" tabindex="1003" class="alertbtnno flloop alertsoverlaybtn" >	
					<span class="lasttab" tabindex="1004"></span>
				</div>
				<div class="row">&nbsp;</div>
			</div>
		</div>
	</div>
	<div class="overlay alertsoverlay overlayalerts" id="bookedoverlay">	
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="overlaybackground">
			<div class="alert-panel">
				<div class="alertmessagearea">
					<div class="row">&nbsp;</div>
					<div class="alert-message">
						<span id="bookedmessage">Do You Want To Cancel This Invoice?</span>
					</div>
				</div>
				<div class="alertbuttonarea">
					<span class="firsttab" tabindex="1000"></span>
					<input type="button" id="bookedyes" name="" value="Yes" tabindex="1001" class="alertbtnyes ffield" >
					<input type="button" id="bookedno" name="" value="No" tabindex="1002" class="alertbtnno flloop alertsoverlaybtn" >
					<span class="lasttab" tabindex="1003"></span>
				</div>
				<div class="row">&nbsp;</div>
			</div>
		</div>
	</div>
</div>