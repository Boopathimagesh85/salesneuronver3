<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Purchaseorder extends MX_Controller{
	public $purchaseordermodule = array(88,99,225);
	//To load the Register View
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Purchaseorder/Purchaseordermodel');
		$this->load->view('Base/formfieldgeneration');
    }
	public function index() {
		$moduleid = array(88,99,225);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['uom_conv']=$this->Basefunctions->simpledropdown('uom','uomid,uomname','uomname');
		$data['calculationtype']=$this->Basefunctions->simpledropdown('calculationtype','calculationtypeid,calculationtypename','calculationtypename');
		$data['additionalcategory']=$this->Basefunctions->simpledropdown('additionalchargecategory','additionalchargecategoryid,additionalchargecategoryname','additionalchargecategoryname');
		$data['category']=$this->Purchaseordermodel->taxcategory();
		$data['adjusttype']=$this->Basefunctions->simpledropdown('adjustmenttype','adjustmenttypeid,adjustmenttypename','adjustmenttypename');
		$data['conv_currency']=$this->Basefunctions->simpledropdown('currency','currencyid,currencyname,currencycountry','currencyname');
		$data['dashboradviewdata'] = $this->Basefunctions->dashboarddataviewbydropdown($moduleid);
		$this->load->view('Purchaseorder/purchaseorderview',$data);	   
	}
	/**Create New purchaseorder	*/
	public function purchaseordercreate() {  
    	$this->Purchaseordermodel->purchaseordercreatemodel();
    }
	//information fetch
	public function fetchformdataeditdetails() {	
		$moduleid = array(88,99,225);
		$this->Purchaseordermodel->informationfetchmodel($moduleid);
	}
	//Update old data
    public function datainformationupdate() {
        $this->Purchaseordermodel->datainformationupdatemodel();
    }
	//delete old information
    public function purchaseorderdelete() {		
    	$moduleid = array(88,99,225);
    	$this->Purchaseordermodel->purchaseorderdeletemodel($moduleid);
    }
	//primary and secondary address value fetch
	public function primaryaddressvalfetch() {
		$this->Purchaseordermodel->primaryaddressvalfetchmodel();
	}		
	//so
	public function getquotenumber() {
		$this->Purchaseordermodel->getquotenumber();
	}
	public function purchaseorderdetailfetch() {
		$this->Purchaseordermodel->summarydetail();
	}
	public function purchaseorderproductdetailfetch() {
		$this->Purchaseordermodel->retrievesoproductdetail();
	}
	public function retrievepaymentdetail() {
		$this->Purchaseordermodel->retrievepaymentdetail();
	}
	//drop down value set with multiple condition
	public function fetchmaildddatawithmultiplecond() {
		$this->Purchaseordermodel->fetchmaildddatawithmultiplecondmodel();
	} 
	//terms and condition data fetch
	public function termsandcontdatafetch() {
		$this->Purchaseordermodel->termsandcontdatafetchmodel();
	}
	//editor value fetch
	public function getsalesordernumber() {
		$this->Purchaseordermodel->getsonumber();
	} 	
	//editor value fetch 	
	public function editervaluefetch() {
		$filename = $_GET['filename']; 
		$newfilename = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]);
			echo json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			echo json_encode($tccontent);
		}
	}	
	//retrieves basic salesorder data
	public function getmoduledata() {
		$this->Purchaseordermodel->getmoduledata();
	}
	//retrieves salesorder address data	
	public function  getsoaddressdetail() {
		$parentid=$_GET['dataprimaryid'];
		$addressdetail=$this->Basefunctions->crmaddressfetch($parentid,'salesorderid','salesorderaddress');
	}
	/*	* purchaseorder stage-data retrival for Lost-Cancel-Draft-Convert	*/
	public function purchaseordercurrentstage(){
		$purchaseorderid = trim($_GET['purchaseorderid']);
		$data=$this->Purchaseordermodel->purchaseordercurrentstage($purchaseorderid);
		echo json_encode($data);
	}
	//retrieves salesorder product data
	public function getsoproductdetail() {
		$id=$_GET['primarydataid'];
		$this->Purchaseordermodel->getsoproductdetail($id);
	}
	//retrieve salesorder summary data
	public function getsosummarydata() {
		$id=$_GET['dataprimaryid'];
		$this->Purchaseordermodel->getsosummarydetail($id);
	}
	//cancel Purchaseordermodel
	public function canceldata() {
		$this->Purchaseordermodel->canceldata();		
	}
	public function getmoduleid(){
		$modulearray = array(88,99,225);
		$data=$this->db->select("moduleid")->from("module")->where_in('moduleid',$modulearray)->where('status',1)->limit(1)->get();
		foreach($data->result() as $info){
			$moduleid = $info->moduleid;
		}
		echo $moduleid;
	}
	//book invocie
	public function bookpurchaseorder() {
		$this->Purchaseordermodel->bookpurchaseorder();
	}
	//product storage fetch - instock
	public function productstoragefetchfun() {
		$productid = $_GET['productid'];
		$totalstock = $this->Purchaseordermodel->productstoragefetchfunmodel($productid);
		echo json_encode($totalstock);
	}
}