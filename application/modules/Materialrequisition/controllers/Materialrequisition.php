<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Materialrequisition extends MX_Controller {
	public  $mrmodule=249;
	function __construct() {
		parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Base/Basefunctions');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Materialrequisition/Materialrequisitionmodel');
	}
	public function index() {
		$moduleid= array($this->mrmodule);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp']=$this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp']=$this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$viewfieldsmoduleids = array($this->mrmodule);
		$viewmoduleid = array($this->mrmodule);
		$data['moduleids']=$viewmoduleid;
		$data['filedmodids']=$viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['conv_currency']=$this->Basefunctions->simpledropdown('currency','currencyid,currencyname,currencycountry','currencyname');
		$data['uom_conv']=$this->Basefunctions->simpledropdown('uom','uomid,uomname','uomname');
		$data['calculationtype']=$this->Basefunctions->simpledropdown('calculationtype','calculationtypeid,calculationtypename','calculationtypename');
		$data['adjusttype']=$this->Basefunctions->simpledropdown('adjustmenttype','adjustmenttypeid,adjustmenttypename','adjustmenttypename');
		$data['dashboradviewdata'] = $this->Basefunctions->dashboarddataviewbydropdown($moduleid);
		$this->load->view('Materialrequisition/materialrequisitionview',$data);
	}
	public function newdatacreate() {
		$this->Materialrequisitionmodel->newdatacreatemodel();
	}
	public function fetchformdataeditdetails() {		
		$this->Materialrequisitionmodel->informationfetchmodel($this->mrmodule);
	}
	public function groupdiscountandadjustdetailsfetch() {
		$this->Materialrequisitionmodel->groupdiscountandadjustdetailsfetchmodel();
	}
	public function datainformationupdate() {
		$this->Materialrequisitionmodel->datainformationupdatemodel();
	}
	public function deleteinformationdata() {		
		$this->Materialrequisitionmodel->deleteoldinformation($this->mrmodule);
	}
	//retrieves the salesorder numbers
	public function getsalesordernumber() {
		$this->Materialrequisitionmodel->getsonumber();
	} 
	public function materialrequisitionproductdetailfetch() {
		$this->Materialrequisitionmodel->retrieveproductdetail();
	}
	//terms and condition data fetch
	public function termsandcontdatafetch() {
		$this->Materialrequisitionmodel->termsandcontdatafetchmodel();
	}
	//editor value fetch 	
	public function editervaluefetch() {
		$filename = $_GET['filename']; 
		$newfilename = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]);
			echo json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			echo json_encode($tccontent);
		}
	}
	//drop down value set with multiple condition
	public function fetchmaildddatawithmultiplecond() {
		$this->Materialrequisitionmodel->fetchmaildddatawithmultiplecondmodel();
	} 
	//retrieve the salesorder details
	public function getsomoduledata() {
		$this->Materialrequisitionmodel->getsomoduledata();
	}
	//retrieve the product details
	public function getsoproductdetail() {
		$this->Materialrequisitionmodel->getsoproductdetail();
	}
	//cancel Materialrequisitionmodel
	public function canceldata() {
		$this->Materialrequisitionmodel->canceldata();
	}
	//stop the process
	public function stopdata() {
		$this->Materialrequisitionmodel->stopdata();
	}
	//convert to so check
	public function checkconvertpo() {
		$this->Materialrequisitionmodel->checkconvertpo();
	}
	//summmary
	public function getsosummarydata() {
		$this->Materialrequisitionmodel->getsosummarydetail();
	}
	//currency dd val fetch
	public function specialcurrency() {
		$this->Materialrequisitionmodel->specialCurrencymodel();
	}
	//get conversion rates
	public function getcurrencyconversionrate(){
		$this->Materialrequisitionmodel->getcurrencyconversionrate();
	}
	//product storage fetch - instock
	public function productstoragefetchfun() {
		$productid = $_GET['productid'];
		$totalstock = $this->Materialrequisitionmodel->productstoragefetchfunmodel($productid);
		echo json_encode($totalstock);
	}
}