<!DOCTYPE html>
<html lang='en'>
	<head>
		<?php $this->load->view('Base/headerfiles'); ?>
	</head>
	<body class='hidedisplay'>
		<?php
			$moduleid = implode(',',$moduleids);
			$device = $this->Basefunctions->deviceinfo();
			$dataset['gridenable'] = 'yes';
			$dataset['modulename']='Materialrequisition';
			$dataset['griddisplayid'] = 'materialrequisitioncreationview';
			$dataset['gridtitle'] = $gridtitle['title'];
			$dataset['titleicon'] = $gridtitle['titleicon'];
			$dataset['spanattr'] = array();
			$dataset['gridtableid'] = 'materialrequisitionaddgrid';
			$dataset['griddivid'] = 'materialrequisitionaddgridnav';
			$dataset['moduleid'] = $moduleids;
			$dataset['forminfo'] = array(array('id'=>'materialrequisitioncreationformadd','class'=>'hidedisplay','formname'=>'materialrequisitioncreationform'));
			if($device=='phone') {
				$this->load->view('Base/gridmenuheadermobile',$dataset);
				$this->load->view('Base/overlaymobile');
				$this->load->view('Base/viewselectionoverlay');
			} else {
				$this->load->view('Base/gridmenuheader',$dataset);
				$this->load->view('Base/overlay');
			}
			$this->load->view('Base/modulelist');
			$this->load->view('Base/basedeleteform');
			$this->load->view('Base/basestopform');
			$this->load->view('Base/basecancelform');
		?>
	</body>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js Files -->
	<script src='<?php echo base_url();?>js/Materialrequisition/materialrequisition.js' type='text/javascript'></script>
</html>