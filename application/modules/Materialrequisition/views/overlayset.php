<!-- CurrencyConversion Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="pricebookcurrencyconvoverlay" style="overflow-y: scroll;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="" >
		<div class="row">&nbsp;</div>
			<form method="POST" name="" style="" id="" action="" enctype="" class="">
				<span id="pricebookcurrencyconv_validate" class="validationEngineContainer"> 
					<div class="alert-panel"  style="background-color:#465a63">
						<div class="alertmessagearea">
							<div class="alert-title">Pricebook Currencies</div>
							<div class="alert-message" style="height:100%">
								<span class="firsttab" tabindex="1000"></span>
								<div class="static-field overlayfield"> 
									<label>PriceBook Currency<span class="mandatoryfildclass">*</span></label>
									<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="validate[required] chzn-select dropdownchange ffieldd" tabindex="1001" name="pricebookcurrency" id="pricebookcurrency">
										<option value=""></option>
										<?php foreach($conv_currency as $key):?>
											<option value="<?php echo $key->currencyid;?>">
											<?php echo $key->currencycountry.'-'.$key->currencyname;?></option>
										<?php endforeach;?>	
									</select>
								</div>
								<div class="static-field overlayfield"> 
									<label>Current Currency<span class="mandatoryfildclass">*</span></label>
									<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="validate[required] chzn-select dropdownchange" tabindex="1002" name="currentcurrency" id="currentcurrency">
										<option value=""></option>
										<?php foreach($conv_currency as $key):?>
											<option value="<?php echo $key->currencyid;?>">
											<?php echo $key->currencycountry.'-'.$key->currencyname;?></option>
										<?php endforeach;?>	
									</select>
								</div>
								<div class="input-field overlayfield"> 
									<input type="text" id="pricebook_currencyconv" name="pricebook_currencyconv" value="" tabindex="1003" class="validate[custom[number],required]" >
									<label for="pricebook_currencyconv">Currency ConversionRate<span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="">
									<input type="hidden" id="pricebook_sellingprice" name="pricebook_sellingprice" >
								</div>
							</div>
						</div>
						<div class="alertbuttonarea">
							<input type="button" id="pricebook_currencyconv_submit" name="" value="Submit" tabindex="1004" class="alertbtn flloop" >
							<input type="button" id="pricebook_currencyconvclose" name="" value="Cancel" tabindex="1005" class="flloop alertbtn alertsoverlaybtn" >
							<span class="lasttab" tabindex="1006"></span>
						</div>
					</div>
				</span>
			</form>
			<div class="row">&nbsp;</div>
		</div>
	</div>
</div>
<!-- Overall Currency Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="currencyconvoverlay" style="overflow-y: scroll;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="" >
		<div class="row">&nbsp;</div>
			<form method="POST" name="" style="" id="" action="" enctype="" class="">
				<span id="currencyconv_validate" class="validationEngineContainer"> 
					<div class="alert-panel"  style="background-color:#465a63">
						<div class="alertmessagearea">
							<div class="alert-title" style="color:#fff">Currency Conversion</div>
							<div class="alert-message">
								<span class="firsttab" tabindex="1000"></span>
								<div class="static-field overlayfield"> 
										<label>Default Currency<span class="mandatoryfildclass">*</span></label>
										<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="validate[required] chzn-select dropdownchange ffieldd" tabindex="1001" name="defaultcurrency" id="defaultcurrency">
											<option value=""></option>
											<?php foreach($conv_currency as $key):?>
												<option value="<?php echo $key->currencyid;?>">
												<?php echo $key->currencycountry.'-'.$key->currencyname;?></option>
											<?php endforeach;?>	
										</select>
								</div>
								<div class="static-field overlayfield"> 
									<label>Conversion Currency<span class="mandatoryfildclass">*</span></label>
									<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="validate[required] chzn-select dropdownchange" tabindex="1002" name="convcurrency" id="convcurrency" >
										<option value=""></option>
										<?php foreach($conv_currency as $key):?>
											<option value="<?php echo $key->currencyid;?>">
											<?php echo $key->currencycountry.'-'.$key->currencyname;?></option>
										<?php endforeach;?>	
									</select>
								</div>
								<div class="input-field overlayfield"> 
									<input type="text" id="currencyconv" name="currencyconv" value="" tabindex="1003" class="validate[custom[number],required]" >
									<label for="currencyconv">Currency ConversionRate<span class="mandatoryfildclass">*</span></label>
								</div>	
							</div>
						</div>
						<div class="alertbuttonarea">
							<input type="button" id="currencyconv_submit" name="" value="Submit" tabindex="1004" class="alertbtn" >	
							<input type="button" id="currencyconvclose" name="" value="Cancel" tabindex="1005" class="alertbtn flloop  alertsoverlaybtn" >
							<span class="lasttab" tabindex="1006"></span>
						</div>
					</div>
				</span>
			</form>
			<div class="row">&nbsp;</div>
		</div>
	</div>
</div>
<!-- UOM Conversion Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="uomconvoverlay" style="overflow-y: scroll;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>			
		<div class="" >
		<div class="row">&nbsp;</div>
			<form method="POST" name="" style="" id="" action="" enctype="" class="">
				<span id="uomconv_validate" class="validationEngineContainer"> 
					<div class="alert-panel"  style="background-color:#465a63">
						<div class="alertmessagearea">
							<div class="alert-title">UOM Conversion</div>
							<div class="alert-message" style="height:100%">
								<div class="static-field overlayfield"> 
									<label>Default UOM<span class="mandatoryfildclass">*</span></label>
									<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange" tabindex="1001" name="uomid" id="uomid">
										<option value=""></option>
										<?php foreach($uom_conv as $key):?>
											<option data-uomidhidden="<?php echo $key->uomname;?>" value="<?php echo $key->uomid;?>">
											<?php echo $key->uomname;?></option>
										<?php endforeach;?>	
									</select>
								</div>
								<span class="firsttab" tabindex="1000"></span>
								<div class="static-field overlayfield"> 
									<label>To UOM<span class="mandatoryfildclass">*</span></label>
									<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd" tabindex="1002" name="touomid" id="touomid">
										<option value=""></option>										
									</select>
								</div>
								<div class="input-field overlayfield">
									<input id="conversionrate" class="validate[maxSize[100]]" type="text" data-prompt-position="bottomLeft" name="conversionrate"  tabindex="1003">
									<label for="conversionrate">UOM Conversion Rate</label>
								</div>
								<div class="input-field overlayfield">
									<input id="conversionquantity" class="validate[maxSize[100]]" type="text" data-prompt-position="bottomLeft" name="conversionquantity"  tabindex="1004">
									<label for="conversionquantity">Conversion Quantity</label>
								</div>
							</div>
						</div>
						<div class="alertbuttonarea">
							<input type="button" id="uomconv_submit" name="" value="Submit" tabindex="1005" class="alertbtn" >
							<input type="button" id="uomconvclose" name="" value="Cancel" tabindex="1006" class="alertbtn alertsoverlaybtn flloop" >
							<span class="lasttab" tabindex="1007"></span>
						</div>
					</div>
				</span>
			</form>
			<div class="row">&nbsp;</div>
		</div>
	</div>
</div>
<!-- Discount Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="discountoverlay" style="overflow-y: scroll;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="" >
			<div class="row">&nbsp;</div>
			<form method="POST" name="discountform" style="" id="discountform" action="" enctype="" class="">
				<span id="individualdiscountvalidation" class="validationEngineContainer">
					 <div class="alert-panel"  style="background-color:#465a63">
						<div class="alertmessagearea">
							<div class="alert-title">Discount</div>
							<div class="alert-message">
								<span class="firsttab" tabindex="1000"></span>
								<div class="static-field overlayfield">             
									<label>Discount Type<span class="mandatoryfildclass">*</span></label>
									<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="validate[required] chzn-select dropdownchange ffieldd" tabindex="1001" name="discounttypeid" id="discounttypeid">
									<option value=""></option>
									<?php foreach($calculationtype as $key):?>
											<option value="<?php echo $key->calculationtypeid;?>">
											<?php echo $key->calculationtypename;?></option>
									<?php endforeach;?>	
									</select>                  
								</div>
								<div class="input-field overlayfield"> 
									<input type="text" id="discountpercent" name="discountpercent" value="" class="validate[custom[number],funcCall[validatepercentage]]" tabindex="1002">
									<label for="discountpercent">Value<span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="input-field overlayfield"> 
									<input type="text" id="singlediscounttotal" name="" value="" class="" tabindex="1003" readonly >
									<label for="singlediscounttotal">Total Discount</label>
								</div>
							</div>
						</div>
						<div class="alertbuttonarea">
							<input type="button" id="discountsubmit" name="" value="Submit" class="alertbtn" tabindex="1004">	
							<input type="button" id="discountclose" name="" value="Cancel" tabindex="1005" class="alertbtn flloop  alertsoverlaybtn" >
							<span class="lasttab" tabindex="1006"></span>
						</div>
					</div>
				</span>
			</form>
		</div>
	</div>
</div>
<!----Price book Details overlay----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="pricebookoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40;">		
		<div class="row sectiontoppadding">&nbsp;</div>			
		<div class="large-6 medium-6 small-11 large-centered medium-centered small-centered columns overlayborder">
			<div class="row">
			<div class="large-12 columns sectionheaderformcaptionstyle" style="line-height:1rem;">
				Price Book List
			</div>
			<?php
				$device = $this->Basefunctions->deviceinfo();
				if($device=='phone') {
					echo '<div class="large-12 columns paddingzero forgetinggridname" id="pricebookgridwidth"><div id="pricebookgrid" class=" inner-row-content inner-gridcontent" style="max-width:2000px; height:300px;top:0px;">
						<!-- Table header content , data rows & pagination for mobile-->
					</div>
					<!--<footer class="inner-gridfooter footercontainer" id="pricebookgridfooter">
						 Footer & Pagination content 
					</footer>--></div>';
				} else {
					echo '<div class="large-12 columns forgetinggridname" id="pricebookgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="pricebookgrid" style="max-width:2000px; height:300px;top:0px;">
					<!-- Table content[Header & Record Rows] for desktop-->
					</div>
					<!-- <div class="inner-gridfooter footer-content footercontainer" id="pricebookgridfooter">
						Footer & Pagination content 
					</div>--></div>';
				}
			?>
			</div>
			<div class="row" style="background:#465a63">
				<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
					<span class="firsttab" tabindex="1000"></span>
					<input type="button" id="pricebooksubmit" name="" value="Submit" class="alertbtn" tabindex="1001">	
					<input type="button" id="pricebookclose" name="" value="Cancel" tabindex="1002" class="alertbtn  alertsoverlaybtn" >
					<span class="lasttab" tabindex="1003"></span>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Discount overlay Group-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay" id="summarydiscountoverlay" style="overflow-y: scroll;overflow-x: hidden;">
		<div class="row sectiontoppadding">&nbsp;</div>	
		<div class="" >
			<div class="row">&nbsp;</div>
			<form method="POST" name="summarydiscountform" style="summarydiscountclearform" id="summarydiscountform" action="" enctype="" class="">
				<span id="summarydiscountvalidation" class="validationEngineContainer">
					<div class="alert-panel"  style="background-color:#465a63">
						<div class="alertmessagearea">
							<div class="alert-title">Discount</div>
							<div class="alert-message">
								<span class="firsttab" tabindex="1000"></span>
								<div class="static-field overlayfield">
									<label>Calculation Type</label>
									<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="validate[required] chzn-select dropdownchange ffieldd" tabindex="1001" name="groupdiscounttypeid" id="groupdiscounttypeid">
									<option value=""></option>
									<?php foreach($calculationtype as $key):?>
											<option value="<?php echo $key->calculationtypeid;?>">
											<?php echo $key->calculationtypeid;?>-<?php echo $key->calculationtypename;?></option>
									<?php endforeach;?>
									</select>
								</div>
								<div class="input-field overlayfield">
									<input type="text" id="groupdiscountpercent" name="groupdiscountpercent" value="" tabindex="1002" class="validate[custom[number],funcCall[summaryvalidatepercentage]]" >
									<label for="groupdiscountpercent">Value</label>
								</div>
								<div class="input-field overlayfield">
									<input type="text" id="groupdiscounttotal" name="" value="" tabindex="1003" class="" readonly >
									<label for="groupdiscounttotal">Total Discount</label>
								</div>
							</div>
						</div>
						<div class="alertbuttonarea">
							<input type="button" id="summaryindiscountadd" name="summaryindiscountadd" value="Submit"  tabindex="1004" class="alertbtn" >
							<input type="button" id="summarydiscountclose" name="" value="Cancel" tabindex="1005" class="alertbtn flloop  alertsoverlaybtn" >
							<span class="lasttab" tabindex="1006"></span>
						</div>
					</div>
				</span>
			</form>
			<div class="row">&nbsp;</div>
		</div>
	</div>
</div>
<!-- Adjustment Amount Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay" id="adjustmentoverlay" style="overflow-y: scroll;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>			
		<div class="" >
			<div class="row">&nbsp;</div>
			<form method="POST" name="adjustmentform" style="" id="adjustmentform" action="" enctype="" class="clearadjustmentform">
				<span id="adjustmentformvalidation" class="validationEngineContainer"> 
					<div class="alert-panel"  style="background-color:#465a63">
						<div class="alertmessagearea">
							<div class="alert-title">Adjustment</div>
							<div class="alert-message">
								<span class="firsttab" tabindex="1000"></span>
								<div class="static-field overlayfield">             
									<label>Adjustment Type</label>
									<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd" tabindex="1001" name="adjustmenttypeid" id="adjustmenttypeid">
									<?php foreach($adjusttype as $key):?>
											<option value="<?php echo $key->adjustmenttypeid;?>">
											<?php echo $key->adjustmenttypename;?></option>
									<?php endforeach;?>	
									</select>                  
								</div>
								<div class="input-field overlayfield"> 
									<input type="text" id="adjustmentvalue" name="adjustmentvalue" value="" tabindex="1002" class="" >
									<label for="adjustmentvalue">Value</label>
								</div>
							</div>
						</div>
						<div class="alertbuttonarea">
							<input type="button" id="adjustmentadd" name="adjustmentadd" value="Submit" tabindex="1003" class="alertbtn " >
							<input type="button" id="adjustmentclose" name="" value="Cancel" tabindex="1004" class="alertbtn flloop  alertsoverlaybtn" >
							<span class="lasttab" tabindex="1005"></span>
						</div>
					</div>
				</span>
			</form>
			<div class="row">&nbsp;</div>
		</div>
	</div>
</div>
<!--Modulenumber Search Grid---->
<div class="large-6 columns" style="position:absolute;">
	<div class="overlay" id="mnsearchoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="row sectiontoppadding">&nbsp;</div>			
		<div class="large-6 medium-6 small-11 large-centered medium-centered small-centered columns overlayborder">
			<div class="row">
			<div class="large-12 columns sectionheaderformcaptionstyle" style="line-height:1.9rem;">
				Module Number Search
			</div>
			<?php
				$device = $this->Basefunctions->deviceinfo();
				if($device=='phone') {
					echo '<div class="large-12 columns paddingzero forgetinggridname" id="modulenumbersearchgridwidth"><div id="modulenumbersearchgrid" class="inner-row-content inner-gridcontent" style="max-width:2000px; height:360px;top:0px;">
						<!-- Table header content , data rows & pagination for mobile-->
					</div>
					<!--<footer class="inner-gridfooter footercontainer" id="modulenumbersearchgridfooter">
						 Footer & Pagination content 
					</footer>--></div>';
				} else {
					echo '<div class="large-12 columns forgetinggridname" id="modulenumbersearchgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="modulenumbersearchgrid" style="max-width:2000px; height:360px;top:0px;">
					<!-- Table content[Header & Record Rows] for desktop-->
					</div>
					<!--<div class="inner-gridfooter footer-content footercontainer" id="modulenumbersearchgridfooter">
						 Footer & Pagination content 
					</div>--></div>';
				}
			?>
			</div>
			<div class="row" style="background:#465a63">
				<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
					<span class="firsttab" tabindex="1000"></span>
					<input type="button" id="mnsearchsubmit" name="" value="Submit" class="alertbtn" tabindex="1001">	
					<input type="button" id="mnsearchclose" name="" value="Cancel" tabindex="1002" class="alertbtn  alertsoverlaybtn" >
					<span class="lasttab" tabindex="1003"></span>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Product Search Grid---->
<div class="large-6 columns" style="position:absolute;">
	<div class="overlay" id="productsearchoverlay" style="overflow-y: auto;overflow-x: hidden;z-index:40;">		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="large-6 medium-6 small-11 large-centered medium-centered small-centered columns overlayborder">
			<div class="row">
			<div class="large-12 columns sectionheaderformcaptionstyle" style="line-height:1rem;">
				Product Search
			</div>
			<?php
				$device = $this->Basefunctions->deviceinfo();
				if($device=='phone') {
					echo '<div class="large-12 columns paddingzero forgetinggridname" id="productsearchgridwidth"><div id="productsearchgrid" class=" inner-row-content inner-gridcontent" style="max-width:2000px; height:300px;top:0px;">
						<!-- Table header content , data rows & pagination for mobile-->
					</div>
					<!--<footer class="inner-gridfooter footercontainer" id="productsearchgridfooter">
						 Footer & Pagination content 
					</footer>--></div>';
				} else {
					echo '<div class="large-12 columns forgetinggridname" id="productsearchgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="productsearchgrid" style="max-width:2000px; height:300px;top:0px;">
					<!-- Table content[Header & Record Rows] for desktop-->
					</div>
					<!-- <div class="inner-gridfooter footer-content footercontainer" id="productsearchgridfooter">
						Footer & Pagination content 
					</div>--></div>';
				}
			?>
			</div>
			<div class="row" style="background:#465a63">
				<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
					<span class="firsttab" tabindex="1000"></span>
					<input type="button" id="productsearchsubmit" name="" value="Submit" class="alertbtn" tabindex="1001">	
					<input type="button" id="productsearchclose" name="" value="Cancel" tabindex="1002" class="alertbtn  alertsoverlaybtn" >
					<span class="lasttab" tabindex="1003"></span>
				</div>
			</div>
		</div>
	</div>
</div>