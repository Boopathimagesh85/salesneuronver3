<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Materialrequisitionmodel extends MX_Controller {
	public $materialrequisition =249;
	function __construct() {
		parent::__construct();
		$this->load->model('Base/Crudmodel');
	}
	public function newdatacreatemodel() {
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		$partablename = $this->Crudmodel->filtervalue($elementpartable);
		$fieldstable = $this->Crudmodel->filtervalue($formfieldstable);
		$gridfieldpartabname = explode(',',$_POST['griddatapartabnameinfo']);
		$gridrows = explode(',',$_POST['numofrows']);
		$girddata = $_POST['griddatas'];
		$girddatainfo = json_decode($girddata, true);
		$gridpartablename =  $this->Crudmodel->filtervalue($gridfieldpartabname);
		$tableinfo = explode(',',$fieldstable);
		$restricttable = explode(',',$_POST['resctable']);
		$primaryid = $this->Crudmodel->datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable);
		$adjustmenttypeid = '1';
		$discounttypeid = '1';
		$discountvalue = '0';
		$mr_discount = json_decode($_POST['groupdiscountdata'], true);
		if(count($mr_discount) > 0){
			if(isset($mr_discount['typeid']) AND isset($mr_discount['value'])){
				$discounttypeid = $mr_discount['typeid'];
				$discountvalue = $mr_discount['value'];
			}
		}
		$mr_adjustment = json_decode($_POST['groupadjustmentdata'], true);	
		if(count($mr_adjustment) > 0){
			if(isset($mr_adjustment['typeid'])){
				$adjustmenttypeid=$mr_adjustment['typeid'];
			}
		}
		//update the invoice records with mode/overlay types
		$otherdetail=array('groupdiscounttypeid'=>$discounttypeid,'groupdiscountpercent'=>$discountvalue,'adjustmenttypeid'=>$adjustmenttypeid);
		$this->db->where('materialrequisition.materialrequisitionid',$primaryid);
		$this->db->update('materialrequisition',array_filter($otherdetail));
		//grid data insertion
		//primary key
		$defdataarr = $this->Crudmodel->defaultvalueget();
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		//productdetail insert
		$m=0;
		$h=1;
		$colcount = 0;
		foreach($gridrows as $rowcount) {
			$gridfieldsname = explode(',',$_POST['gridcolnames'.$h]);
			//filter grid unique fields table
			$gridfieldtabname = explode(',',$_POST['griddatatabnameinfo'.$h]);
			$gridfieldstable = $this->Crudmodel->filtervalue($gridfieldtabname);
			$gridtableinfo = explode(',',$gridfieldstable);
			for($i=0;$i<$rowcount;$i++) {
				foreach($gridtableinfo as $gdtblname) {
					${'$gcdata'.$m} = array();
					$t=0;
					foreach($gridfieldsname AS $gdfldsname) {
						if(strcmp( trim($gridfieldtabname[$t]),trim($gdtblname) ) == 0 ) {
							if( isset( $girddatainfo[$i][$gdfldsname] ) ) {
								$name = explode('_',$gdfldsname);
								if( ctype_alpha($girddatainfo[$i][$gdfldsname]) && $girddatainfo[$i][$gdfldsname] != "" ) {									
									${'$gcdata'.$m}[$gdfldsname] = ucwords( $girddatainfo[$i][$gdfldsname] );								
								} else if( $girddatainfo[$i][$gdfldsname] != "" ) {									
									${'$gcdata'.$m}[$gdfldsname] = $girddatainfo[$i][$gdfldsname];									
								}
							}
						}
					}
					if(count(${'$gcdata'.$m})>0) {
						${'$gcdata'.$m}[$primaryname]=$primaryid;
						$gnewdata = array_merge(${'$gcdata'.$m},$defdataarr);
						//individual						
						$individualdiscountdetail=json_decode($girddatainfo[$i]['discountdata'],true);
						//unset the hidden variables
						$removehidden=array('materialrequisitionproductdetailsid','discountdata','uomdata');
						for($mk=0;$mk<count($removehidden);$mk++){						
							unset($gnewdata[$removehidden[$mk]]);
						}
						$this->db->insert($gdtblname,array_filter($gnewdata) );
						$lastinsertid=$this->db->insert_id();	
						//product detail discount type						
						$discounttypeid = '';
						$discountvalue = '';
						$uomid = '';
						$touomid = '';
						$conversionrate = '';
						$conversionqty = '';
						$uomdata = json_decode($girddatainfo[$i]['uomdata'], true);	
						if(count($uomdata) > 0){
							if(isset($uomdata['uomid'])){
								$uomid = $uomdata['uomid'];
							}
							if(isset($uomdata['touomid'])){
								$touomid = $uomdata['touomid'];
							}								
							$conversionrate = $uomdata['conversionrate'];
							$conversionqty = $uomdata['conversionquantity'];
							//update the product detail records with mode/overlay types
							$uomdetail=array('uomid'=>$uomid,'touomid'=>$touomid,'conversionrate'=>$conversionrate,'conversionquantity'=>$conversionqty);
							$this->db->where('materialrequisitionproductdetailsid',$lastinsertid);
							$this->db->update('materialrequisitionproductdetails',array_filter($uomdetail));
						}
						$quote_discount = json_decode($girddatainfo[$i]['discountdata'], true);						
						if(count($quote_discount) > 0){
							if(isset($quote_discount['typeid']) AND isset($quote_discount['value'])){
								$discounttypeid = $quote_discount['typeid'];
								$discountvalue = $quote_discount['value'];
								//update the product detail records with mode/overlay types
								$productdetail=array('discounttypeid'=>$discounttypeid,'discountpercent'=>$discountvalue);
								$this->db->where('materialrequisitionproductdetailsid',$lastinsertid);
								$this->db->update('materialrequisitionproductdetails',array_filter($productdetail));
							}
						} 						
					}
					$m++;
				}
			}
			$h++;
		}  
		//notification entry
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		if(isset($_POST['employeeid'])){
			$assignid = $_POST['employeeid']; 
			if($assignid != '') {
				$assignid = $assignid;
			} else {
				$assignid = 1;
			}
		} else { 
			$assignid = 1; 
		}
		$sonum = $_POST['materialrequisitionnumber'];		
		if($assignid == '1'){
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch(249,$primaryid,$partablename,2);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,249);
			}
		} else {
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch(249,$primaryid,$partablename,2);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,249);
			}
		}
		{//workflow management -- for data create
			$this->Basefunctions->workflowmanageindatacreatemode(249,$primaryid,$partablename);
		}
		echo 'TRUE';
	}
	public function informationfetchmodel($moduleid) {
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementspartabname']);
		$restricttable = array('materialrequisitionproductdetails','termsandcondition');
		$partablename = $this->Crudmodel->filtervalue($elementpartable);
		$fieldstable = $this->Crudmodel->filtervalue($formfieldstable);
		$primaryid = $_GET['dataprimaryid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetchwithrestrict($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$restricttable,$moduleid);
		echo $result;
	}
	public function datainformationupdatemodel() {
		$softwareindustryid = $this->Basefunctions->industryid;
		if(isset($_POST['termsandconditionid'])){
			if($_POST['termsandconditionid'] == ''){
				$_POST['termsandconditionid'] = 1;
				$_POST['materialrequisitiontermsandconditions_editorfilename'] = '';
			}
		}
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		$partablename = $this->Crudmodel->filtervalue($elementpartable);
		$fieldstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fieldstable);
		$primaryid = $_POST['primarydataid'];
		{//fetch old values -- work flow
			$condstatvals=$this->Basefunctions->workflowmanageolddatainfofetch(249,$primaryid);
		}
		$restricttable = explode(',',$_POST['resctable']);
		$this->Crudmodel->dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable);
		//productdetail update
		//get the old product detail
		$oldmrdetailid =array();
		$mrdetail=$this->db->select('materialrequisitionproductdetailsid')
					->from('materialrequisitionproductdetails')
					->where('materialrequisitionid',$primaryid)
					->where('status',$this->Basefunctions->activestatus)
					->get();
		foreach($mrdetail->result() as $info) {
			$oldmrdetailid[]=$info->materialrequisitionproductdetailsid;
		}		
		$gridfieldpartabname = explode(',',$_POST['griddatapartabnameinfo']);
		$gridrows = explode(',',$_POST['numofrows']);
		$griddata = $_POST['griddatas'];
		$girddatainfo = json_decode($griddata, true);	
		//filter unique grid parent table
		$gridpartablename =  $this->Crudmodel->filtervalue($gridfieldpartabname);
		for($lm=0; $lm < count($girddatainfo); $lm++) {
			$newmretailid[]=$girddatainfo[$lm]['materialrequisitionproductdetailsid'];
		}		
		$deletedmrdetailid=ARRAY();
		//find deleted records
		for($m=0;$m < count ($oldmrdetailid);$m++) {
			if(in_array($oldmrdetailid[$m],$newmretailid)) {			
			} else {
				$deletedmrdetailid[]=$oldmrdetailid[$m];
			}
		}
		if(count($deletedmrdetailid) > 0) {
			//delete productdetail and further tables
			for($k=0;$k<count($deletedmrdetailid);$k++) {
				$this->Crudmodel->outerdeletefunction('materialrequisitionproductdetails','materialrequisitionproductdetailsid','',$deletedmrdetailid[$k]);
			}		
		}
		//Insert the New MR records
		$defdataarr = $this->Crudmodel->defaultvalueget();
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$m=0;
		$h=1;
		foreach($gridrows as $rowcount) {
			$gridfieldsname = explode(',',$_POST['gridcolnames'.$h]);
			//filter grid unique fields table
			$gridfieldtabname = explode(',',$_POST['griddatatabnameinfo'.$h]);
			$gridfieldstable = $this->Crudmodel->filtervalue($gridfieldtabname);
			$gridtableinfo = explode(',',$gridfieldstable);
			for($i=0;$i<$rowcount;$i++) {
				foreach( $gridtableinfo as $gdtblname ) {
					${'$gcdata'.$m} = array();
					$t=0;
					foreach($gridfieldsname AS $gdfldsname) {
						if(strcmp( trim($gridfieldtabname[$t]),trim($gdtblname) ) == 0 ) {
							if( isset( $girddatainfo[$i][$gdfldsname] ) ) {
								$name = explode('_',$gdfldsname);
								if( ctype_alpha($girddatainfo[$i][$gdfldsname]) && $girddatainfo[$i][$gdfldsname] != "" ) {									
									${'$gcdata'.$m}[$gdfldsname] = ucwords( $girddatainfo[$i][$gdfldsname] );								
								} else if( $girddatainfo[$i][$gdfldsname] != "" ) {									
									${'$gcdata'.$m}[$gdfldsname] = $girddatainfo[$i][$gdfldsname];									
								}
							}
						}
					}
					if(count(${'$gcdata'.$m})>0) {
						${'$gcdata'.$m}[$primaryname]=$primaryid;
						$gnewdata = array_merge(${'$gcdata'.$m},$defdataarr);
						//individual					
						$individualdiscountdetail=json_decode($girddatainfo[$i]['discountdata'],true);//unset the hidden variables
						$removehidden=array('materialrequisitionproductdetailsid','discountdata','uomdata');
						for($mk=0;$mk<count($removehidden);$mk++){						
							unset($gnewdata[$removehidden[$mk]]);
						}
						//verify whether its new records
						if($girddatainfo[$i]['materialrequisitionproductdetailsid'] == 0){
							$this->db->insert($gdtblname,array_filter($gnewdata));
							$lastinsertid=$this->db->insert_id();
							//product detail discount type						
							$discounttypeid = '';
							$discountvalue = '';
							$uomid = '';
							$touomid = '';
							$conversionrate = '';
							$conversionqty = '';
							$uomdata = json_decode($girddatainfo[$i]['uomdata'], true);
							if(count($uomdata) > 0){
								if(isset($uomdata['uomid'])){
									$uomid = $uomdata['uomid'];
								}
								if(isset($uomdata['touomid'])){
									$touomid = $uomdata['touomid'];
								}
								$conversionrate = $uomdata['conversionrate'];
								$conversionqty = $uomdata['conversionquantity'];
								//update the product detail records with mode/overlay types
								$uomdetail=array('uomid'=>$uomid,'touomid'=>$touomid,'conversionrate'=>$conversionrate,'conversionquantity'=>$conversionqty);
								$this->db->where('materialrequisitionproductdetailsid',$lastinsertid);
								$this->db->update('materialrequisitionproductdetails',array_filter($uomdetail));
							}
							$quote_discount = json_decode($girddatainfo[$i]['discountdata'], true);						
							if(count($quote_discount) > 0){
								if(isset($quote_discount['typeid']) AND isset($quote_discount['value'])){
									$discounttypeid = $quote_discount['typeid'];
									$discountvalue = $quote_discount['value'];
									//update the product detail records with mode/overlay types
									$productdetail=array('discounttypeid'=>$discounttypeid,'discountpercent'=>$discountvalue);
									$this->db->where('materialrequisitionproductdetailsid',$lastinsertid);
									$this->db->update('materialrequisitionproductdetails',array_filter($productdetail));
								}
							} 	
						}
						if($girddatainfo[$i]['materialrequisitionproductdetailsid'] > 0){ //update-existing data
							$this->db->where('materialrequisitionproductdetailsid',$girddatainfo[$i]['materialrequisitionproductdetailsid']);
							$this->db->update($gdtblname,array_filter($gnewdata));
							$detailid=$girddatainfo[$i]['materialrequisitionproductdetailsid'];
						}
					}
					$m++;
				}
			}
			$h++;
		}
		//notification entry
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		if(isset($_POST['employeeid'])){
			$assignid = $_POST['employeeid'];
			if($assignid != ''){
				$assignid = $assignid;
			} else {
				$assignid = 1;
			}
		} else {
			$assignid = 1; 
		}
		$sonum = $_POST['materialrequisitionnumber'];		
		if($assignid == '1'){
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch(249,$primaryid,$partablename,3);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,249);
			}
		}else {
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch(249,$primaryid,$partablename,3);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$notimsg,$assignid,249);
			}
		}
		{//workflow management for data update
			$this->Basefunctions->workflowmanageindataupdatemode(249,$primaryid,$partablename,$condstatvals);
		}
		echo true;	
	}
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primarydataid'];
		$msg='False';
		$filename = $this->Basefunctions->generalinformaion('materialrequisition','materialrequisitionnumber','materialrequisitionid',$id);
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		//
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		{//workflow management -- for data delete
			$this->Basefunctions->workflowmanageindatadeletemode(249,$id,$primaryname,$partabname);
		}
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				$msg='Denied';
			} else {
				//notification log entry
				$notimsg = $this->Basefunctions->notificationtemplatedatafetch(249,$id,$partabname,4);
				if($notimsg != '') {
					$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,1,249);
				}
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				$msg='TRUE';
			}
		} else {
			//notification log entry
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch(249,$id,$partabname,4);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,1,249);
			}
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			$msg='TRUE';
		}
		echo $msg;
	}
	//retrieves sales order numbers
	public function getsonumber() {
		$array=array();
		$i=0;
		$data=$this->db->select('salesordernumber,salesorderid')->from('salesorder')->where('status',$this->Basefunctions->activestatus)->get();
		foreach($data->result() as $value) {
			$array[$i] = array('id'=>$value->salesorderid,'name'=>$value->salesordernumber);
			$i++;
		}
		echo json_encode($array);
	}
	public function insertdiscountdetail($darray) {
		$defdataarr = $this->Crudmodel->defaultvalueget();
		$discountdata = array(
				'calculationtypeid'=>$darray['calculationtypeid'],
				'discountvalue'=>$darray['discountvalue'],
				'moduleid'=>$darray['moduleid'],
				'singlegrouptypeid'=>$darray['modeid'],
				'id'=>$darray['id']);				
		$newdata = array_filter(array_merge($discountdata,$defdataarr));			
		$this->db->insert('modulediscountdetail',array_filter($newdata));
	}
	public function retrieveproductdetail()	{
		$materialrequisitionid=trim($_GET['primarydataid']);
		$this->db->select('a.grossamount,a.netamount,a.discountamount,a.requireddate,a.sellingprice,a.unitprice,a.quantity,a.uomid,a.productid,product.productname,materialrequisitionproductdetailsid,a.discounttypeid,a.discountpercent,a.conversionrate,a.conversionquantity,a.instock,a.touomid');
		$this->db->from('materialrequisitionproductdetails as a');
		$this->db->join('product','product.productid=a.productid');
		$this->db->where('a.materialrequisitionid',$materialrequisitionid);
		$this->db->where('a.status',$this->Basefunctions->activestatus);
		$data=$this->db->get()->result();			
		$j=0;
		foreach($data as $value) {			
			$uom_json = array('uomid'=>$value->uomid,'touomid'=>$value->touomid,'conversionrate'=>$value->conversionrate,'conversionquantity'=>$value->conversionquantity);
			$discount_json = array('typeid'=>$value->discounttypeid,'value'=>$value->discountpercent);
			$productdetail->rows[$j]['id']=$value->materialrequisitionproductdetailsid;
			$productdetail->rows[$j]['cell']=array(
					$value->productname,
					$value->productid,
					$value->instock,
					$value->quantity,
					$value->unitprice,
					$value->sellingprice,
					$value->grossamount,
					$value->discountamount,
					$value->requireddate,
					$value->netamount,
					json_encode($discount_json),					
					$value->materialrequisitionproductdetailsid,
					json_encode($uom_json),
				);						
			$j++;		
		}
		echo  json_encode($productdetail);
	}
	//terms and condition data fetch
	public function termsandcontdatafetchmodel() {
		$id = $_GET['id'];
		$this->db->select('termsandconditionstermsandconditions_editorfilename');
		$this->db->from('termsandcondition');
		$this->db->where('termsandcondition.termsandconditionid',$id);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$data = $row->termsandconditionstermsandconditions_editorfilename;
			}
			echo json_encode($data);
		}
	}	
	//drop down value set with multiple condition
	public function fetchmaildddatawithmultiplecondmodel() {
		$i=0;
		$dname=$_GET['dataname'];
		$did=$_GET['dataid'];
		$dataattrname1=$_GET['othername1'];
		$dataattrname2=$_GET['othername2'];
		$table=$_GET['datatab'];
		$whfield=$_GET['whdatafield'];
		$whdata=$_GET['whval'];
		$mulcond=explode(",",$whfield);
		$mulcondval=explode(",",$whdata);
		$i=0;
		$this->db->select("$dname,$did,$dataattrname1,$dataattrname2");
		$this->db->from($table);
		foreach($mulcond AS $condname) {
			if($mulcondval[$i] != "0" && $mulcondval[$i] != "") {
				$this->db->where($condname,$mulcondval[$i]);
			}
			$i++;
		}
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {		
			foreach($result->result()as $row) {
				$data[$i]=array('datasid'=>$row->$did,'dataname'=>$row->$dname,'otherdataattrname1'=>$row->$dataattrname1,'otherdataattrname2'=>$row->$dataattrname2);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}	
	public function getsomoduledata() {
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementpartable']);
		$restricttable = array('');
		$partablename = $this->Crudmodel->filtervalue($elementpartable);
		$fieldstable = $this->Crudmodel->filtervalue($formfieldstable);
		$primaryid = $_GET['dataprimaryid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetchwithrestrict($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$restricttable);
		echo $result;
	}
	//retrieve product salesorder detail
	public function getsoproductdetail() {
		$salesorderid=$_GET['primarydataid'];
		$this->db->select('salesorderdetailid,salesorderdetail.salesorderid,productname,salesorderdetail.productid,salesorderdetail.instock,salesorderdetail.quantity,salesorderdetail.unitprice,salesorderdetail.sellingprice,salesorderdetail.grossamount,salesorderdetail.additionalamount,salesorderdetail.taxamount,salesorderdetail.discountamount,salesorderdetail.netprice,salesorderdetail.descriptiondetail,salesorder.taxmodeid,salesorder.discountmodeid,salesorder.additionalchargemodeid');
		$this->db->from('salesorderdetail');
		$this->db->join('salesorder','salesorder.salesorderid=salesorderdetail.salesorderid');
		$this->db->join('product','product.productid=salesorderdetail.productid');
		$this->db->where('salesorder.salesorderid',$salesorderid);
		$this->db->where('salesorderdetail.status',$this->Basefunctions->activestatus);
		$data=$this->db->get();	
		$j=0;
		if($data->num_rows() > 0) {
			$this->load->model('Salesorder/Salesordermodel');
			foreach($data->result() as $value) {	
				$detailarray=$this->Salesordermodel->soindividualdetail($salesorderid,$value->taxmodeid,$value->discountmodeid,$value->additionalchargemodeid);
				$productdetail->rows[$j]['id']=$j;
				$productdetail->rows[$j]['cell']=array(
														$value->productname,
														$value->productid,
														0,
														1,
														$value->quantity,
														$value->unitprice,
														$value->sellingprice,
														$value->grossamount,
														'',
														$value->discountamount,$value->netprice,
														$detailarray['discount'],
														$value->salesorderdetailid
													);						
				$j++;		
			}
		}
		echo  json_encode($productdetail);
	}
	//cancel
	public function canceldata() {
		$table='materialrequisition,materialrequisitionsummary,materialrequisitionproductdetails';
		$primaryid=$_GET['primarydataid'];
		$primaryidname='materialrequisitionid';
		//get existing status
		$dbdata=$this->db->select('status')->from('materialrequisition')->where('materialrequisitionid',$primaryid)->get()->result();
		foreach($dbdata as $info){
			$mrstatus=$info->status;
		}
		if($mrstatus == $this->Basefunctions->cancelstatus) {//5cac status
			$status=$this->Basefunctions->activestatus;
		} else {
			$status=$this->Basefunctions->cancelstatus;
		}	
		//
		$this->Basefunctions->canceldata($table,$primaryid,$primaryidname,$status);
		//notification trigger for quote Lost
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		$quotenum = $this->Basefunctions->generalinformaion('materialrequisition','materialrequisitionnumber','materialrequisitionid',$primaryid);
		if($status == $this->Basefunctions->cancelstatus) { 
			$notimsg = $empname." Changed The Material Requisition Named ".$quotenum." as Cancelled";$val = "Cancelled";
		} else { 
			$notimsg = $empname." Changed The Material Requisition Named ".$quotenum." as Active"; $val = "Active";
		}
		$this->Basefunctions->notificationcontentadd($primaryid,$val,$notimsg,1,249); 
	}
	//stop
	public function stopdata() {
		$primaryid=$_GET['primarydataid'];
		//get existing status
		$dbdata=$this->db->select('status')->from('materialrequisition')->where('materialrequisitionid',$primaryid)->get()->result();
		foreach($dbdata as $info){
			$mrstatus=$info->status;
		}
		if($mrstatus == 7) {//7 lost status
			$status=$this->Basefunctions->activestatus;
		} else {
			$status=7;
		}		
		$updatearray=array('status'=>$status,'lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid);
		$table='materialrequisition,materialrequisitionsummary,materialrequisitionproductdetails';
		$tablearray=explode(',',$table);
		for($i=0;$i<count($tablearray);$i++) {
			$this->db->where('materialrequisitionid',$primaryid);
			$this->db->update($tablearray[$i],$updatearray);
		}
		//notification trigger for PO Lost
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		$quotenum = $this->Basefunctions->generalinformaion('materialrequisition','materialrequisitionnumber','materialrequisitionid',$primaryid);
		if($status == 7) { 
			$notimsg = $empname." Changed The Material Requisition Named ".$quotenum." as Stopped";$val = "Stopped";
		} else { 
			$notimsg = $empname." Changed The Material Requisition Named ".$quotenum." as Active"; $val = "Active";
		}
		$this->Basefunctions->notificationcontentadd($primaryid,$val,$notimsg,1,249); 
		echo true;		
	}
	//convert to purchaseorder
	public function checkconvertpo() {
		$primaryid=$_GET['primarydataid'];
		$dbdata=$this->db->select('status,convertpo')->from('materialrequisition')->where('materialrequisitionid',$primaryid)->get()->result();
		foreach($dbdata as $info) {
			$mrstatus=$info->status;
			$convertpo=$info->convertpo;
		}
		if($mrstatus == 1 and $convertpo == 0) {
			echo true;
		} else {
			echo false;
		}
	}
	//
	public function getsosummarydetail() {
		$quoteid=$_GET['dataprimaryid'];
		$data=$this->db->select('netamount,adjustmentamount,grossamount,discountamount,adjustmenttypeid,salesorder.salesorderid')
						->from('salesorder')
						->where('salesorder.salesorderid',$quoteid)
						->join('salesordersummary','salesordersummary.salesorderid=salesorder.salesorderid')
						->get();
		$row=$data->row();		
		$modedetail=array('adjustmenttype'=>$row->adjustmenttypeid,'adjustmentvalue'=>$row->adjustmentamount,
						  'summarynetamount'=>$row->netamount,
						  'summarygrossamount'=>$row->grossamount,'summarydiscountamount'=>$row->discountamount);
		echo json_encode($modedetail);
	}
	//group adjustment and discount data fetch
	public function groupdiscountandadjustdetailsfetchmodel() {
		$mrid = $_GET['dataprimaryid'];
		$this->db->select('groupdiscounttypeid,groupdiscountpercent,groupdiscountamount,adjustmenttypeid,adjustmentamount');
		$this->db->from('materialrequisition');
		$this->db->where('materialrequisition.materialrequisitionid',$mrid);
		$this->db->where('materialrequisition.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$discount_json = array('typeid'=>$row->groupdiscounttypeid,'value'=>$row->groupdiscountpercent);
				$adjustment_json = array('typeid'=>$row->adjustmenttypeid,'value'=>$row->adjustmentamount);
				$data = array('discount'=>json_encode($discount_json),'addcharge'=>json_encode($adjustment_json));
			}
		}
		echo json_encode($data);
	}	
	//currency value fetch
	public function specialCurrencymodel() {
		$i=0;
		$this->db->select('currencyid,CONCAT(currencycountry, " - ",currencyname) as currency,decimalplaces',false);
		$this->db->from('currency');
		$this->db->where('currency.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('currencyid'=>$row->currencyid,'currencyname'=>$row->currency,'decimal'=>$row->decimalplaces);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		} 
	}
	/**
	* Retrieves the conversion rate of the currency
	*/
	public function getcurrencyconversionrate(){
		$currencyid=trim($_GET['currencyid']);
		$date=date($this->Basefunctions->datef);
		//first get the basecurrency
		$currency_data=$this->Basefunctions->getdefaultcurrency();
		$base_currency = json_decode($currency_data,true);
		
		$base_currency = $base_currency['currency'];
		if($base_currency == $currencyid){
			$data=array('status'=>TRUE,'rate'=>1.00);
		} else {
			$conversion_rate = $this->db->select('multiplyrate')
										->from('currencyconversion')
										->where('currencyid',$base_currency) //basecurrency
										->where('currencytoid',$currencyid) //given currency
										->where('status',$this->Basefunctions->activestatus)
										->limit(1)
										->get();
			if($conversion_rate->num_rows() > 0){
				foreach($conversion_rate->result() as $conv){
				$data=array('status'=>TRUE,'rate'=>$conv->multiplyrate);	
				}
			} else {
				$data=array('status'=>FALSE,'rate'=>1.00);
			}
		}
		echo json_encode($data);
	}
	//product instock fetch
	public function productstoragefetchfunmodel($productid) {
		$this->load->model('Product/Productmodel');
		$totalstock = $this->Productmodel->productstoragefetchfunmodel($productid);
		return $totalstock;
	}
}