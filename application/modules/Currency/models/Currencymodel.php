<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Currencymodel extends CI_Model{
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//currency create
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['currencyelementsname']);
		$formfieldstable = explode(',',$_POST['currencyelementstable']);
		$formfieldscolmname = explode(',',$_POST['currencyelementscolmn']);
		$elementpartable = explode(',',$_POST['currencyelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
		echo 'TRUE';
	}
	//Retrive currency data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['currencyelementsname']);
		$formfieldstable = explode(',',$_GET['currencyelementstable']);
		$formfieldscolmname = explode(',',$_GET['currencyelementscolmn']);
		$elementpartable = explode(',',$_GET['currencyelementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['currencyprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	//currency update
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['currencyelementsname']);
		$formfieldstable = explode(',',$_POST['currencyelementstable']);
		$formfieldscolmname = explode(',',$_POST['currencyelementscolmn']);
		$elementpartable = explode(',',$_POST['currencyelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['currencyprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		echo $result;
	}
	//currency delete
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['attributeelementstable']);
		$parenttable = explode(',',$_GET['attributeelementspartable']);
		$id = $_GET['attributeprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=array('currencyconversion');
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				$sctablename = 'currencyconversion';
				$fieldid = 'currencytoid';
				$this->currencyupdate($sctablename,$fieldid,$id);
				echo "TRUE";
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			$sctablename = 'currencyconversion';
			$fieldid = 'currencytoid';
			$this->currencyupdate($sctablename,$fieldid,$id);
			echo "TRUE";
		}
		
	} 
	public function currencyupdate($sctablename,$fieldid,$id){
		$data = array('status'=>0);
		$this->db->where($sctablename.'.'.$fieldid,$id);
		$this->db->update($sctablename,$data);
	}
}