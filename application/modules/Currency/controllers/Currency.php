<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Currency extends MX_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->view('Base/formfieldgeneration');
        $this->load->helper('formbuild');
		$this->load->model('Currency/Currencymodel');	
    }
    //first basic hitting view
    public function index() {        
    	$moduleid =  array(214);
    	sessionchecker($moduleid);
		//action
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(214);
		$viewmoduleid = array(214);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['moddashboardtabgrp'] = $this->Basefunctions->moddashboardtabgrpgeneration($moduleid);
		$data['moddbfrmfieldsgen'] = $this->Basefunctions->dashboardmodsecfieldsgeneration($moduleid);
		$this->load->view('Currency/currencyview',$data);
	}
	//create itemcounter
	public function newdatacreate() {  
    	$this->Currencymodel->newdatacreatemodel();
    }
	//information fetchitemcounter
	public function fetchformdataeditdetails() {
		$moduleid =  array(214);
		$this->Currencymodel->informationfetchmodel($moduleid);
	}
	//update itemcounter
    public function datainformationupdate() {
        $this->Currencymodel->datainformationupdatemodel();
    }
	//delete itemcounter
    public function deleteinformationdata() {
    	$moduleid =  array(214);
		$this->Currencymodel->deleteoldinformation($moduleid);
    } 
}