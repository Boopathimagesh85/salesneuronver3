<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Calendar extends MX_Controller {
	//To load the Register View
	function __construct() {
    	parent::__construct();
    	$this->load->helper('formbuild');
    	$this->load->model('Base/Basefunctions');
		$this->load->model('Calendar/Calendarmodel');
    }
	public function index() {
		$moduleid = array(241);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['dynamicviewdd']=$this->Calendarmodel->dataviewbydropdown();
		$data['tstatus']=$this->Calendarmodel->simpledropdown('crmstatus','crmstatusid,crmstatusname','crmstatusname',206); 
		$data['astatus']=$this->Calendarmodel->simpledropdown('crmstatus','crmstatusid,crmstatusname','crmstatusname',205); 
		$data['apriority']=$this->Calendarmodel->simpledropdown('priority','priorityid,priorityname','priorityname',205); 
		$data['tpriority']=$this->Calendarmodel->simpledropdown('priority','priorityid,priorityname','priorityname',206); 
		$data['tmoduleid']=$this->Basefunctions->simpledropdownwithcond('moduleid','modulename','module','moduleprivilegeid',206);
		$data['assignto']=$this->Basefunctions->simpledropdown('employee','employeeid,employeename','employeename');
		$data['product']=$this->Basefunctions->simpledropdown('product','productid,productname','productname');
		$this->load->view('Calendar/calendarview',$data);
	}
	//new task data create
	public function tasknewdatacreate()
	{
		$this->Calendarmodel->tasknewdatacreatemodel();
	}
	//new activity data create
	public function activitynewdatacreate()
	{
		$this->Calendarmodel->activitynewdatacreatemodel();
	}
	//task default id value get
	public function taskdefaultvalueget()
	{
		$this->Calendarmodel->taskdefaultvaluegetmodel();
	}
	//acctivity default id value get
	public function activitydefaultvalueget()
	{
		$this->Calendarmodel->activitydefaultvaluegetmodel();
	}
	//task fetch data
	public function fetchtaskvalue()
	{
		$this->Calendarmodel->fetchtaskvaluemodel();
	}
	//activity fetch data
	public function fetchactivityvalue()
	{
		$this->Calendarmodel->fetchactivityvaluemodel();
	}
	//task update function 
	public function taskupadatefunction()
	{
		$this->Calendarmodel->taskupadatefunctionmodel();
	}
	//activity update function 
	public function activityupadatefunction()
	{
		$this->Calendarmodel->activityupadatefunctionmodel();
	}
	//record delete for task
	public function recorddeltefortask()
	{
		$this->Calendarmodel->recorddeltefortaskmodel();
	}
	//record delete for activity
	public function recorddelteforactivity()
	{
		$this->Calendarmodel->recorddelteforactivitymodel();
	}
	//task view data fetch
	public function taskviewdatafetch()
	{
		$this->Calendarmodel->taskviewdatafetchmodel();
	}
	//task view data fetch
	public function activitiesviewdatafetch()
	{
		$this->Calendarmodel->activitiesviewdatafetchmodel();
	}
	//activities view data fetch for google
	public function activitiesviewforgoogle()
	{
		$this->Calendarmodel->activitiesviewforgooglemodel();
	}
	//used for fetch the module in data bases
	public function dropdownvaluefecth() {
		$this->Calendarmodel->dropdownvaluefecthmodel();
	}
	//record name fetch
	public function recordnamefetch(){
		$this->Calendarmodel->recordnamefetchmodel();
	}
	//google export grid data fetch
	public function activitylistfetch() {
		$creationid = $_GET['viewid'];
		$moduleid = $_GET['moduleid'];
		$information = $this->Basefunctions->gridinformationfetchmodel($creationid);
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$chkbox = $_GET['checkbox'];
		$size = array();
		for($i=0;$i<count($information['colmodelname']);$i++) {
			$size[$i] = '200';
		}
		$colmodelname = $information['colmodelname'];
		$colmodelaliasname = $information['colmodelaliasname'];
		$coltablename = $information['coltablename'];
		$colmodeluitype = $information['colmodeluitype'];
		$colname = $information['colname'];
		$colsize = $size;
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : $primarytable.'.'.$primaryid) : $primarytable.'.'.$primaryid;
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>$colmodelname,'colmodelindex'=>$colmodelaliasname,'coltablename'=>$coltablename,'uitype'=>$colmodeluitype,'colname'=>$colname,'colsize'=>$colsize);
		$result=$this->Calendarmodel->activitylistfetchgrid($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$information,$moduleid);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($result,$page,'');
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Activity List',$width,$height,$chkbox);
		} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Activity List',$width,$height,$chkbox);
		}
		echo json_encode($datas);
	}
	//google export grid data fetch
	public function tasklistfetch() {
		$creationid = $_GET['viewid'];
		$moduleid = $_GET['moduleid'];
		$information = $this->Basefunctions->gridinformationfetchmodel($creationid);
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$chkbox = $_GET['checkbox'];
		$size = array();
		for($i=0;$i<count($information['colmodelname']);$i++) {
			$size[$i] = '200';
		}
		$colmodelname = $information['colmodelname'];
		$colmodelaliasname = $information['colmodelaliasname'];
		$coltablename = $information['coltablename'];
		$colmodeluitype = $information['colmodeluitype'];
		$colname = $information['colname'];
		$colsize = $size;
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : $primarytable.'.'.$primaryid) : $primarytable.'.'.$primaryid;
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>$colmodelname,'colmodelindex'=>$colmodelaliasname,'coltablename'=>$coltablename,'uitype'=>$colmodeluitype,'colname'=>$colname,'colsize'=>$colsize);
		$result=$this->Calendarmodel->activitylistfetchgrid($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$information,$moduleid);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($result,$page,'');
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Task List',$width,$height,$chkbox);
		} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Task List',$width,$height,$chkbox);
		}
		echo json_encode($datas);
	}
	//get selected data for google cal export
	public function selectedeventsfetch() {
		$this->Calendarmodel->selectedeventsfetchmodel();
	}
	//get selected data for google cal export
	public function selectedtasksfetch() {
		$this->Calendarmodel->selectedtasksfetchmodel();
	}
	//view based data fetch
	public function dynamicdatafetchbasedonview() {
		$data = '';
    	$viewid = $_GET['viewid'];
    	$moduleid = $_GET['moduleid'];
		if($moduleid == 205) {
			$data = $this->Calendarmodel->activitiesviewdatafetchmodel();
		} else if($moduleid == 206) {
			$data = $this->Calendarmodel->taskviewdatafetchmodel();
		}
	}
	//status change
	public function statuschange() {
		$this->Calendarmodel->statuschangemodel();
	}
	//Task drag update function
	public function taskdragupdate()
	{
		$this->Calendarmodel->taskdragupdatemodel();
	}
	//Activity drag update function
	public function activitydragupdate()
	{
		$this->Calendarmodel->activitydragupdatemodel();
	}
	//Activity resize update function
	public function activityresizeupdate()
	{
		$this->Calendarmodel->activityresizeupdatemodel();
	}
	//google calendr data insert and update
	public function googlecalendardatainsert(){
		$this->Calendarmodel->googlecalendardatainsert();
	}
	//member data laod
	public function memberdataload_dd(){
		$this->Calendarmodel->memberdataload_dd();
	}
}