<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH.'third_party/rest/libraries/REST_Controller.php';
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Rest extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Calendar/Calendarmodel');
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        //  $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        //  $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        // $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }
	public function taskviewdatafetch_get()
    {
        // Fetch the data from database
		$result_id =  $this->Calendarmodel->taskviewdatafetchmodel_api();
		if(!empty($result_id)) {
			$result = ['data' => $result_id, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
    }
	public function activitiesviewdatafetch_get()
    {
        // Fetch the data from database
		$result_id =  $this->Calendarmodel->activitiesviewdatafetchmodel_api();
		if(!empty($result_id)) {
			$result = ['data' => $result_id, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
    }
	public function activitylistfetch_get() {
		$creationid = $_GET['viewid'];
		$moduleid = $_GET['moduleid'];
		$information = $this->Basefunctions->gridinformationfetchmodel($creationid);
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$chkbox = $_GET['checkbox'];
		$size = array();
		for($i=0;$i<count($information['colmodelname']);$i++) {
			$size[$i] = '200';
		}
		$colmodelname = $information['colmodelname'];
		$colmodelaliasname = $information['colmodelaliasname'];
		$coltablename = $information['coltablename'];
		$colmodeluitype = $information['colmodeluitype'];
		$colname = $information['colname'];
		$colsize = $size;
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : $primarytable.'.'.$primaryid) : $primarytable.'.'.$primaryid;
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>$colmodelname,'colmodelindex'=>$colmodelaliasname,'coltablename'=>$coltablename,'uitype'=>$colmodeluitype,'colname'=>$colname,'colsize'=>$colsize);
		$result=$this->Calendarmodel->activitylistfetchgrid($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$information,$moduleid);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($result,$page,'');
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Activity List',$width,$height,$chkbox);
		} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Activity List',$width,$height,$chkbox);
		}
		if($datas) {
			$result = ['data' => $datas, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['data' => $datas, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
	}
	public function tasklistfetch_get() {
		$creationid = $_GET['viewid'];
		$moduleid = $_GET['moduleid'];
		$information = $this->Basefunctions->gridinformationfetchmodel($creationid);
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$chkbox = $_GET['checkbox'];
		$size = array();
		for($i=0;$i<count($information['colmodelname']);$i++) {
			$size[$i] = '200';
		}
		$colmodelname = $information['colmodelname'];
		$colmodelaliasname = $information['colmodelaliasname'];
		$coltablename = $information['coltablename'];
		$colmodeluitype = $information['colmodeluitype'];
		$colname = $information['colname'];
		$colsize = $size;
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : $primarytable.'.'.$primaryid) : $primarytable.'.'.$primaryid;
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>$colmodelname,'colmodelindex'=>$colmodelaliasname,'coltablename'=>$coltablename,'uitype'=>$colmodeluitype,'colname'=>$colname,'colsize'=>$colsize);
		$result=$this->Calendarmodel->activitylistfetchgrid($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$information,$moduleid);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($result,$page,'');
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Task List',$width,$height,$chkbox);
		} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Task List',$width,$height,$chkbox);
		}
		if($datas) {
			$result = ['data' => $datas, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['data' => $datas, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
	}
	public function dropdownvaluefecth_get()
    {
        // Fetch the data from database
		$id = $_GET['moduleid'];
		$result_id =  $this->Calendarmodel->dropdownvaluefecthmodel_api();
		if(!empty($result_id)) {
			$result = ['mid' => $id, 'data' => $result_id, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['mid' => $id, 'data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
    }
	public function recordnamefetch_get()
    {
        // Fetch the data from database
		$id = $_GET['tabname'];
		$result_id =  $this->Calendarmodel->recordnamefetchmodel_api();
		if(!empty($result_id)) {
			$result = ['tname' => $id, 'data' => $result_id, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['tname' => $id, 'data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
    }
	public function taskdefaultvalueget_get()
    {
        // Fetch the data from database
		$result_id =  $this->Calendarmodel->taskdefaultvaluegetmodel_api();
		if(!empty($result_id)) {
			$result = ['data' => $result_id, 'message' => 'Success', 'desc' => 'Retrieved ID for last created task'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
    }
	public function activitynewdatacreate_post()
    {
		// Create
		$result_id =  $this->Calendarmodel->activitynewdatacreatemodel_api();
		if(!empty($result_id)){
			$result = ['id' => $result_id, 'message' => 'Success', 'desc' => 'New Record is created'];
			$this->set_response($result, REST_Controller::HTTP_CREATED);
		} else {
			$result = ['id' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result,404);
		}
    }
	public function fetchtaskvalue_get()
    {
        // Fetch the data from database
		$id = $_GET['datarowid'];
		$result_id =  $this->Calendarmodel->fetchtaskvaluemodel_api();
		if(!empty($result_id)) {
			$result = ['id' => $id, 'data' => $result_id, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['id' => $id, 'data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
    }
	public function taskupadatefunction_post()
    {
		$primaryid = $_POST['datarowid'];
		$result_id =  $this->Calendarmodel->taskupadatefunctionmodel_api();
		if($result_id == 'TRUE') {
			$result = ['id' => $primaryid, 'message' => 'Success', 'desc' => 'Record is updated.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['id' => $primaryid, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result,404);
		}
    }
	public function recorddeltefortask_get()
    {
		$primaryid = $_GET['datarowid'];
		$result_id =  $this->Calendarmodel->recorddeltefortaskmodel_api();
		if($result_id == 'TRUE') {
			$result = ['id' => $primaryid, 'message' => 'Success','desc' => 'Record is deleted'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['id' => $primaryid, 'message' => 'Error', 'desc' => 'Record was already delted and it is showing Error Message'];
			$this->set_response($result,REST_Controller::HTTP_BAD_REQUEST);
		}
    }
	public function fetchactivityvalue_get()
    {
        // Fetch the data from database
		$id = $_GET['datarowid'];
		$result_id =  $this->Calendarmodel->fetchactivityvaluemodel_api();
		if(!empty($result_id)) {
			$result = ['id' => $id, 'data' => $result_id, 'message' => 'Success', 'desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['id' => $id, 'data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
		}
    }
	public function activityupadatefunction_post()
    {
		$primaryid = $_POST['datarowid'];
		$result_id =  $this->Calendarmodel->activityupadatefunctionmodel_api();
		if($result_id == 'TRUE') {
			$result = ['id' => $primaryid, 'message' => 'Success', 'desc' => 'Record is updated.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['id' => $primaryid, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result,404);
		}
    }
	public function recorddelteforactivity_get()
    {
		$primaryid = $_GET['datarowid'];
		$result_id =  $this->Calendarmodel->recorddelteforactivitymodel_api();
		if($result_id == 'TRUE') {
			$result = ['id' => $primaryid, 'message' => 'Success','desc' => 'Record is deleted'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['id' => $primaryid, 'message' => 'Error', 'desc' => 'Record was already delted and it is showing Error Message'];
			$this->set_response($result,REST_Controller::HTTP_BAD_REQUEST);
		}
    }
	public function activityresizeupdate_get()
    {
		$primaryid = $_GET['datarowid'];
		$result_id =  $this->Calendarmodel->activityresizeupdatemodel_api();
		if($result_id == 'TRUE') {
			$result = ['id' => $primaryid, 'message' => 'Success','desc' => 'Record is updated'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['id' => $primaryid, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result,REST_Controller::HTTP_BAD_REQUEST);
		}
    }
	public function activitydefaultvalueget_get()
    {
		$result_id =  $this->Calendarmodel->activitydefaultvaluegetmodel_api();
		if(!empty($result_id)) {
			$result = ['data' => $result_id, 'message' => 'Success','desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result,REST_Controller::HTTP_BAD_REQUEST);
		}
    }
	public function dynamicdatafetchbasedonview_get() {
		$data = '';
    	$viewid = $_GET['viewid'];
    	$moduleid = $_GET['moduleid'];
		if($moduleid == 205) {
			$data = $this->Calendarmodel->activitiesviewdatafetchmodel_api();
		} else if($moduleid == 206) {
			$data = $this->Calendarmodel->taskviewdatafetchmodel_api();
		}
		if(!empty($data)) {
			$result = ['modid' => $moduleid, 'data' => $data, 'message' => 'Success','desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['modid' => $moduleid, 'data' => $data, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result,REST_Controller::HTTP_BAD_REQUEST);
		}
	}
	public function activitydragupdate_get()
	{
		$result_id = $this->Calendarmodel->activitydragupdatemodel_api();
		if($result_id == 'TRUE') {
			$result = ['data' => $result_id, 'message' => 'Success','desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result,REST_Controller::HTTP_BAD_REQUEST);
		}
	}
	//Task drag update function
	public function taskdragupdate_get()
	{
		$result_id = $this->Calendarmodel->taskdragupdatemodel_api();
		if($result_id == 'TRUE') {
			$result = ['data' => $result_id, 'message' => 'Success','desc' => 'Record is fetched.'];
			$this->set_response($result, REST_Controller::HTTP_OK);
		} else {
			$result = ['data' => $result_id, 'message' => 'Error', 'desc' => 'Something is problem, Please try again'];
			$this->set_response($result,REST_Controller::HTTP_BAD_REQUEST);
		}
	}
}