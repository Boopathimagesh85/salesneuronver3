<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); 
		$taskmoduleid = array(206,77);
		$taskmodid = $this->Basefunctions->getmoduleid($taskmoduleid);
		$taskstatus = $this->Calendarmodel->gettaskstatus('moduleinfo',$taskmodid);
		$activitymoduleid = array(205,76);
		$activitymodid = $this->Basefunctions->getmoduleid($activitymoduleid);
		$activitymodulename = $this->Basefunctions->generalinformaion('module','menuname','moduleid',$activitymodid);
		$taskmodulename = $this->Basefunctions->generalinformaion('module','menuname','moduleid',$taskmodid);
	?>
<style>
	#minicalendars {
	    margin: 0 auto;
	    font-size: 10px;
	}
	#minicalendars .fc-toolbar h2 {
	    font-size: .9rem;
	    white-space: normal !important;
	}
	#minicalendars table tr td, table tr th{
	*font-size:1rem;
	}
	#minicalendars .fc-event .fc-content{
		display:none;
	}
	#minicalendars table tfoot tr td,#minicalendars table tfoot tr th,#minicalendars table thead tr td,#minicalendars table thead tr th{
		font-size:0.8rem;
	}
	#minicalendars .fc-widget-header{
		height:1.3rem;
	}
	.tabs .tab-title, .multitabs .tab-title {
	    border-right: 0px solid #ebeff2;
	}
	.tabs .tab-title.active:after {
	    top: 39px;
	}
	.tabs li span, .multitabs li span {
	    padding: 0.59rem 1rem !important;
	}
	.tabs {
	    margin-left: 0px;
	}
	#taskstartdate{
		height: 2.0em;
	}
	@media only screen 
	and (min-device-width : 768px) 
	and (max-device-width : 1024px)  { 
		 #activitylistoverlay {
		    left: 684.109px !important;
		}
	}
	.tabs li span, .multitabs li span {
		    background: #fff !important;
	}
	.tabs .tab-title.active span, .multitabs .tab-title.active span {
		 background: #fff !important;
	}
</style>
<link href="<?php echo base_url();?>css/fullcalendar.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="row" style="max-width:100%">
		<div class="off-canvas-wrap" data-offcanvas>
			<div class="inner-wrap">
					<div class="large-12 columns paddingzero viewheader">
					<?php
						$device = $this->Basefunctions->deviceinfo();
						if($device=='phone') {
							echo '<ul class="actionicons-view">
								<!--<li class="mobfilterspan"><i class="icon icon-filter"></i></li>-->
								<li>
									<nav class="top-bar" data-topbar style="display: inline-block; line-height: 25px; background:none;" role="navigation">
										<section class="top-bar-section" style="top:-15px; ">
											<ul class="right" style="height: 20px !important; background-color:#dae1e4;">
												<li class="has-dropdown dropdown-button-above" data-activates="actionbaricons" style="padding:0;">
													<span class="action-click"><i class="material-icons">more</i></span>
													<ul class="actionoverlay dropdown-content" id="actionbaricons">
														<li><span class="action-category">Basic</span></li>														
														<li id="gotoactivityicon"><span><i class="material-icons gotoactivityiconclass" title="Goto Activity">event</i>Goto '.$activitymodulename.'</span></li>
														<li id="gototaskicon"><span><i class="material-icons gototaskiconclass" title="Goto Task">event_available</i>Goto '.$taskmodulename.'</span></li>
														<!--li id="converticon"><span><i class="material-icons converticonclass" title="Synchronization">sync</i>Synchronization</span></li-->
														<li id="reloadicon"><span><i class="material-icons reloadiconclass" title="Reload">refresh</i>Reload</span></li>
														<li id="printicon"><span><i class="material-icons printiconclass" title="Print">print</i>Print</span></li>
														<li id="authorize-button" style=""><span><i class="material-icons printiconclass" title="Print">print</i>Print</span></li>
														<li id="signout-button"  style="display: block;"><span><i class="material-icons printiconclass" title="Print">print</i>Print</span></li>
													</ul>
												</li>
												<script>
													$(document).ready(function() {
														setTimeout(function(){
															var height=$(window).height();
															height = height-105;';
														echo "$('.actionicons-view .actionoverlay').attr('style', 'height: '+height+'px !important');";
														echo '},10);
													});
												</script>
											</ul>
										</section>
									</nav>
								</li>
							</ul>';
						} else {
							$dataset['gridtitle'] = $gridtitle['title'];
							$this->load->view('mainviewheader',$dataset);
							echo '<div class="desktop actionmenucontainer headeraction-menu ">	
								<ul class="module-view">
									<li class="view-change-dropdown" style="padding: 5px 11px;">
										<select id="dynamicdddataview" class="chzn-select dropdownchange" data-placeholder="Select View"  tabindex="" name="dynamicdddataview" title="" style="height: 36px;border-bottom-style: solid;left: 17px;">
											<option></option>';
												$prev = ' ';
												$select ='';
												foreach($dynamicviewdd as $key):
												$cur = $key->viewcreationmoduleid;
												$a ="";
												if($prev != $cur ) {
													if($prev != " ") {
														echo '</optgroup>';
													}
													echo '<optgroup  label="'.$key->viewcreationmodulename.'" class="'.str_replace(' ','',$key->viewcreationmodulename).'">';
													$prev = $key->viewcreationmoduleid;
													$a = "pclass";
												}
											echo '<option value="'.$key->viewcreationid.'" data-moduleid="'.$key->viewcreationmoduleid.'" > '.$key->viewcreationname.'</option>';
											 endforeach;
									echo '</select>
									</li>
								</ul>';
							echo'<ul class="toggle-view">
								<li class="action-icons">
									<div class="drop-container activitylistoverlay dropdown-button" data-activates="activitylistoverlay" style="right:8px">
										
										<span id="addicon" class="icon-box addiconclass add-form-for-2nd-head">Add</span>
									</div>';
									echo '<ul class="action-drop arrow_box" style="width:200px;top: 65px !important;" id="activitylistoverlay">
											<li id="calendarsettingsicon" style="color:white;background-color:#2c2f48;height: 60px;    top: -8px; position: relative;border-top-left-radius: 4px;border-top-right-radius: 4px;"><i title="actionicons" id="actionicons" class="material-icons" style="font-size: 2rem;line-height: 1.9;color: #fff;">apps</i><span style="position:relative;top:-12px;color:#fff !important;font-size:0.9rem;font-family:
			"Nunito, sans-serif;"">Create Panel</span></li>';
											echo '<li id="gotoactivityicon"><span class="icon-box gotoactivityiconclass"><i title="" class="material-icons" style="font-size: 1.4rem;line-height: 1.9;color: #788db4;">event</i><span style="position:relative;top:2px;">'.$activitymodulename.'</span></span></li>';
											if($taskstatus == 1) {
												echo '<li id="gototaskicon"><span class="icon-box gototaskiconclass"><i title="'.$taskmodulename.'" class="material-icons" style="font-size: 1.4rem;line-height: 1.9;color: #788db4;">event_available</i><span style="position:relative;top:2px;">'.$taskmodulename.'</span></span></li>';
											
											}
									echo '</ul>';
	
									echo '<div class="drop-container activitylistoverlay advanceddropbox dropdown-button more-btn-for-2nd-head" data-activates="moreadvanceddrop"><span class="icon-box" style="position:relative;top:-1px;"><i title="help" id="viewcategory" class="material-icons" style="font-size: 1.6rem;line-height: 1.0;color: #fff;left:-8px;top:0px;position:relative;">more_horiz</i></span></div>';
									echo '<ul id="moreadvanceddrop" class="action-drop arrow_box" style="width:200px;top: 80px !important;"><li id="calendarsettingsicon" style="color:white;background-color:#2c2f48;height: 60px;    top: -8px; position: relative;border-top-left-radius: 4px;border-top-right-radius: 4px;"><i title="actionicons" id="actionicons" class="material-icons" style="font-size: 2rem;line-height: 1.9;color: #fff;">apps</i><span style="position:relative;top:-12px;color:#fff !important;font-size:0.9rem;font-family:
			"Nunito, sans-serif;"">Actions Panel</span></li>
											<li id="editicon"><span class="icon-box"><i class="material-icons editiconclass" title="Edit" style="font-size: 1.4rem;line-height: 1.9;color: #788db4;">edit</i><span style="position:relative;top:2px;">Edit</span></span></li>
											<li id="deleteicon"><span class="icon-box"><i class="material-icons deleteiconclass " title="Delete" style="font-size: 1.4rem;line-height: 1.9;color: #788db4;">delete</i><span style="position:relative;top:2px;">Delete</span></span></li>
											<li id="cloneicon"><span class="icon-box"><i class="material-icons cloneiconclass " title="Clone" style="font-size: 1.4rem;line-height: 1.9;color: #788db4;">library_add</i><span style="position:relative;top:2px;">Clone</span></span></li>
											<!--<li id="cancelicon"><span class="icon-box"><i class="material-icons" title="Cancel" style="font-size: 1.4rem;line-height: 1.9;color: #788db4;">cancel</i><span style="position:relative;top:2px;">Cancel</span></span></li>-->
											<!--<li id="converticon"><span class="icon-box"><i class="material-icons converticonclass" title="Convert">arrow_forward</i>Convert</span></li>
											<li id="reloadicon"><span class="icon-box"><i class="material-icons reloadiconclass" title="Reload">refresh</i>Reload</span></li>
											<li id="authorize-button"><span class="icon-box"><i class="material-icons reloadiconclass" title="Auth">dvr</i>Auth</span></li>
											<li id="signout-button"><span class="icon-box"><i class="material-icons reloadiconclass" title="SignOut">power_settings_new</i>Signout</span></li>-->';
									
									echo '</ul>';
								echo '</li>
							</ul>';
						}
					?>
				</div>	
				</div>
				<div class="calendarviewcontainer large-12 columns paddingzero" style="height:561px;display:none;">
					<div class="large-12 medium-12 columns paddingzero" style="background-color:#fff;">
						<div class="large-12 columns" style="background-color:#fff;">
							
							<div id='calendar' style="margin-bottom:0.5rem;"></div>
						</div>
					</div>
				</div>
				<?php $this->load->view('Base/basedeleteform'); ?>
				<?php 
				$this->load->view('overlayset'); ?>				
			</div>
		</div>
	</div>
	<?php
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$this->load->view('Base/overlaymobile');
		} else {
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
	?>
	<style type="text/css" media="print">
  @page { size: landscape; }
  #minicalendars {
    width: 10%;
  }
</style>
</body>
	<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<?php $this->load->view('Base/bottomscript'); ?>
	<script async defer src="https://apis.google.com/js/api.js"
      onload="this.onload=function(){};handleClientLoad()"
      onreadystatechange="if (this.readyState === 'complete') this.onload()">
    </script>
	<!-- js Files -->	
	<script src="<?php echo base_url();?>js/Calendar/calendar.js" type="text/javascript"></script> 
	<script src="<?php echo base_url();?>js/plugins/Developer/timepicker/jquery.timepicker.js" type="text/javascript"></script>
	<!--Calendar Code-->
	<script src="<?php echo base_url();?>js/plugins/fullcalendar/lib/moment.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/fullcalendar/fullcalendar.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/fullcalendar/gcal.min.js" type="text/javascript"></script></html>
	 