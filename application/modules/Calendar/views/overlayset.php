<!-- Calendar Overlay -->
<?php 
$CI =& get_instance();
$appdateformat = $CI->Basefunctions->appdateformatfetch();
$industryid = $CI->Basefunctions->industryid;
$taskmoduleid = array(206,77);
$taskmodid = $CI->Basefunctions->getmoduleid($taskmoduleid);
$taskstatus = $CI->Calendarmodel->gettaskstatus('moduleinfo',$taskmodid);
$activitymoduleid = array(205,76);
$activitymodid = $CI->Basefunctions->getmoduleid($activitymoduleid);
$activitymodulename = $CI->Basefunctions->generalinformaion('module','menuname','moduleid',$activitymodid);
?>
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay overlayalerts resetoverlay overflowhidden" id="creationoverlay" style="z-index:40;">		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="large-4 medium-6 small-10 small-centered large-centered medium-centered columns paddingzero" >
				<div class="large-12 columns paddingzero borderstyle" style="background:#fff">
					<ul class="tabs" data-tab="">
						<?php if($taskstatus == 1) { ?>
							<li class="tab-title sidebaricons active" id="viewtaskspan" >
								<span style="padding-top:1.1rem">Task</span>
							</li>
						<?php } ?>
						<?php if($taskstatus == 1) { ?>
							<li class="tab-title sidebaricons" id="viewactivityspan">
						<?php } else { ?>
							<li class="tab-title sidebaricons active" id="viewactivityspan">
						<?php } ?>
							<span style="padding-top:1.1rem"><?php echo $activitymodulename; ?></span>
						</li>
					</ul>
				</div>
				<div class="large-12 columns paddingzero borderstyle" style="background:#fff;">
				<?php if($taskstatus == 1) { ?>
				<span id="taskspan" class="">
					<form method="POST" name="taskoverlayform"  id="taskoverlayform" action="" enctype="" class="clearpassworddataform">				
						<span id="validatetaskoverlay" class="validationEngineContainer"> 
							<span id="editvalidatetaskoverlay" class="validationEngineContainer"> 
								<div class="large-12 columns paddingzero">
								<div class="large-12 columns cleartaskform sectionpanel" style="background:#fff">
									<div class="large-12 columns">&nbsp;</div>
									<div class="input-field large-12 columns"> 
										<input type="text" id="tasktitlename" name="tasktitlename" value="" class="validate[required]" tabindex="100">
										<label for="tasktitlename">Task Name <span class="mandatoryfildclass">*</span></label>
									</div>
									<div class="large-6 columns"> 
										<label>Due Date <span class="mandatoryfildclass">*</span></label>
										<input type="date" data-dateformater="<?php  echo $appdateformat; ?>" readonly='readonly' id="taskstartdate" name="taskstartdate" value="" class="validate[required] fordatepicicon " tabindex="101">
									</div>
									<div class="large-6 columns"> 
										<label>Assigned To <span class="mandatoryfildclass">*</span></label>
										<select data-placeholder="select" data-prompt-position="bottomLeft:14,36" class="validate[required] chzn-select" name="taskassignto" id="taskassignto" tabindex="105">
										<option value=""></option>
											<?php foreach($assignto as $key):?>
											<option value="<?php echo $key->employeeid;?>" ><?php echo $key->employeename;?></option>
											<?php endforeach;?>
										</select>
									</div>
									<div class="large-6 columns"> 
										<label>Task Status</label>
										<select data-placeholder="select" data-prompt-position="bottomLeft:14,36" class="chzn-select" name="taskstatus" id="taskstatus" tabindex="106">
										<option value=""></option>
											<?php foreach($tstatus as $key):?>
											<option value="<?php echo $key->crmstatusid;?>" ><?php echo $key->crmstatusname;?></option>
											<?php endforeach;?>
										</select>
									</div>
									<div class="large-6 columns"> 
										<label>Priority</label>
										<select data-placeholder="select" data-prompt-position="bottomLeft:14,36" class="chzn-select" name="taskpriority" id="taskpriority" tabindex="107">
										<option value=""></option>
											<?php foreach($tpriority as $key):?>
											<option value="<?php echo $key->priorityid;?>" ><?php echo $key->priorityname;?></option>
											<?php endforeach;?>
										</select> 
									</div>
									<div class="large-6 columns"> 
										<label>Related To</label>
										<select data-placeholder="select" data-prompt-position="bottomLeft:14,36" class="chzn-select" name="taskmodule" id="taskmodule" tabindex="108">
										<option value=""></option>
											<?php foreach($tmoduleid as $key):?>
											<option value="<?php echo $key->moduleid;?>" data-mname="<?php echo $key->modulename;?>"><?php echo $key->modulename;?></option>
											<?php endforeach;?>
										</select> 
									</div>
									<div class="large-6 columns"> 
										<label>Name</label>
										<select data-placeholder="select" data-prompt-position="bottomLeft:14,36" class="chzn-select" name="taskrecord" id="taskrecord" tabindex="109">
											<option value=""></option>
										</select> 
									</div>
									<input type="hidden" id="taskid" name="taskid" value="" class="" >
									
									<div class="large-12 columns">&nbsp;</div>
									<div class="large-12 columns">&nbsp;</div>
								</div>
								</div>
							</span>
						</span>
						<div class="divider large-12 columns"></div>
						<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">
							<span  id="forsubmitdata">
								<input type="button" id="createtask" name="createtask" value="Submit" tabindex="110" class="alertbtnyes" >	
							</span>
							<span  class="hidedisplay" id="forupdatedata">
								<input type="button" id="updatecreatetask" name="" tabindex="110" value="SUBMIT" class="alertbtnyes" >	
							</span>
							<input type="button"  name="" tabindex="111" value="Cancel" class="alertbtnno closecreator" >
						</div>
						<!-- hidden fields -->
					</form>
				</span>
				<?php } ?>
				<?php if($taskstatus == 1) { ?>
					<span class="hidedisplay" id="activityspan">
				<?php } else { ?>
					<span class="" id="activityspan">
				<?php } ?>
					<form method="POST" name="activityoverlayform"  id="activityoverlayform" action="" enctype="" class="clearpassworddataform">
						<span id="activitytaskaddvalidation" class="validationEngineContainer"> 
							<span id="activitytaskeditvalidation" class="validationEngineContainer"> 
								<div class="large-12 columns paddingzero">
								<div class="large-12 columns clearactivityform sectionpanel" style="background:#fff">
									<div class="large-12 columns">&nbsp;</div>
									<?php if($industryid == 4) { ?>
									<div class="input-field large-12 columns"> 
										<label>Member Name <span class="mandatoryfildclass">*</span></label>
									</div>
									<div class="large-12 columns">&nbsp;</div>
									<div class="input-field large-12 columns"> 
										<input type="hidden" name="contactid" id="contactid" class="validate[required]" tabindex="200"/>
									</div>
									<?php } ?>
									<div class="large-6 columns"> 
										<label>Start Date <span class="mandatoryfildclass">*</span></label>
										<input type="date" data-dateformater="<?php echo $appdateformat; ?>" id="activitystartdate" name="activitystartdate" value="" class="validate[required] fordatepicicon " readonly='readonly' tabindex="201">
									</div>
									<div class="large-6 columns"> 
										<label>Start Time <span class="mandatoryfildclass">*</span></label>
										<input type="text" id="activitystarttime" name="activitystarttime" value="" class="validate[required] fortimepicicon" tabindex="202">
									</div>
									<div class="large-6 columns"> 
										<label>End Date <span class="mandatoryfildclass">*</span></label>
										<input type="date" data-dateformater="" readonly='readonly' id="activityenddate" name="activityenddate" value="" class="validate[required] fordatepicicon " tabindex="203">
									</div>
									<div class="large-6 columns"> 
										<label>End Time <span class="mandatoryfildclass">*</span></label>
										<input type="text" id="activityendtime" name="activityendtime" value="" class="validate[required] fortimepicicon" tabindex="204">
									</div>
									<?php if($industryid == 4) { ?>
									<div class="large-12 columns"> 
										<label>Product/Service Name<span class="mandatoryfildclass">*</span></label>
										<select data-placeholder="select" data-prompt-position="bottomLeft:14,36" class="validate[required] chzn-select" name="productid[]" id="productid" multiple tabindex="205">
										<option value=""></option>
											<?php foreach($product as $key):?>
											<option value="<?php echo $key->productid;?>" ><?php echo $key->productname;?></option>
											<?php endforeach;?>
										</select>
									</div>
									<?php } ?>
									<div class="large-12 columns"> 
										<label>Assigned To <span class="mandatoryfildclass">*</span></label>
										<select data-placeholder="select" data-prompt-position="bottomLeft:14,36" class="validate[required] chzn-select" name="activityassignto" id="activityassignto" tabindex="206">
										<option value=""></option>
											<?php foreach($assignto as $key):?>
											<option value="<?php echo $key->employeeid;?>" ><?php echo $key->employeename;?></option>
											<?php endforeach;?>
										</select>
									</div>
									<div class="large-6 columns"> 
										<label>Task Status</label>
										<select data-placeholder="select" data-prompt-position="bottomLeft:14,36" class="chzn-select" name="activitystatus" id="activitystatus" tabindex="207">
										<option value=""></option>
											<?php foreach($astatus as $key):?>
											<option value="<?php echo $key->crmstatusid;?>" ><?php echo $key->crmstatusname;?></option>
											<?php endforeach;?>
										</select>
									</div>
									<div class="large-6 columns"> 
										<label>Priority</label>
										<select data-placeholder="select" data-prompt-position="bottomLeft:14,36" class="chzn-select" name="activitypriority" id="activitypriority" tabindex="208">
										<option value=""></option>
											<?php foreach($apriority as $key):?>
											<option value="<?php echo $key->priorityid;?>" ><?php echo $key->priorityname;?></option>
											<?php endforeach;?>
										</select> 
									</div>
									<?php if( $industryid !=4) {?>
									<div class="large-6 columns"> 
										<label>Related To</label>
										<select data-placeholder="select" data-prompt-position="bottomLeft:14,36" class="chzn-select" name="activitymodule" id="activitymodule" tabindex="209">
										<option value=""></option>
											<?php foreach($tmoduleid as $key):?>
											<option value="<?php echo $key->moduleid;?>" data-mname="<?php echo $key->modulename;?>"><?php echo $key->modulename;?></option>
											<?php endforeach;?>
										</select> 
									</div>
									<div class="large-6 columns"> 
										<label>Name</label>
										<select data-placeholder="select" data-prompt-position="bottomLeft:14,36" class="chzn-select" name="activityrecord" id="activityrecord" tabindex="210">
											<option value=""></option>
										</select> 
									</div>
									<?php } ?>
									<div class="input-field large-12 columns"> 
										<input type="text" id="activityname" name="activityname" value="" class="validate[required]" tabindex="211">
										<label for="activityname"><?php echo $activitymodulename; ?> Name <span class="mandatoryfildclass">*</span></label>
									</div>
									<!--
									<div class="large-12 columns">
										<label >Booking Note</label>
										<textarea name="description" class="materialize-textarea validate[maxSize[100]] forsucesscls" id="description" value="" tabindex="212"></textarea>
										
									</div>
									-->
									<input type="hidden" id="activityid" name="activityid" value="" class="" >
									<input type="hidden" id="geventid" name="geventid" value="" class="" >
								</div>
								</div>
								<div class="divider large-12 columns"></div>
								<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">
									<span class="" id="activityforsubmitdata">
										<input type="button" id="createactivity" tabindex="213" name="createactivity" value="Submit" class="alertbtnyes" >	
									</span>
									<span class="hidedisplay" id="activityforupdatedata">
										<input type="button" id="updatecreateactivity" tabindex="213" name="updatecreateactivity" value="SUBMIT" class="alertbtnyes" >	
									</span>
									<input type="button" name=""value="Cancel" tabindex="214" class="alertbtnno closecreator"  >
								</div>
							</span>
						</span>
					</form>
				</span>
			</div>
		</div>
	</div>
</div>
<!-- Print Overlay  Overlay-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="previewoverlayoverlay" style="overflow-y: scroll;overflow-x: hidden">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-10 medium-10 large-centered medium-centered columns overlayborder" >
			<form method="POST" name="" id="" action="" enctype="" class="clearformtaxoverlay">
				<span id="pdfviewoverlayformvalidation" class="validationEngineContainer"> 
					<div class="row">&nbsp;</div>
					<div class="row" style="background:#617d8a ">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-10 columns" style="background:none">Preview</div>
							<div class="small-1 columns" id="previewclose" style="text-align:right;cursor:pointer;background:none">X</div>
						</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div style="background:#617d8a" class="row">
						<div id="calpreview"></div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
					<div class="row">&nbsp;</div>
				</span>
			</form>
		</div>
	</div>
</div>
<!--Activity Export overlay-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="activitylistoverlay" style="overflow-y: scroll;overflow-x: hidden">
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-10 medium-10 large-centered medium-centered columns overlayborder" >
			<div class="row" style="background:#617d8a">
				<div class="large-12 columns">
					<ul class="tabs" data-tab="">
						<li class="tab-title sidebaricons active" id="activitylistspan" >
							<span>Activities</span>
						</li>
						<li class="tab-title sidebaricons" id="tasklistspan">
							<span>Task</span>
						</li>
						<span class="right">
							<span id="gridlistclose" style="margin-left:25px;float:right;text-align:right;cursor:pointer;background:none">X</span>
						</span>
					</ul>
				</div>
				<span class="" id="activityexportspan">
					<form method="POST" name="" id="gridlistexport" action="" enctype="" class="clearformtaxoverlay">
						<div class="large-12 columns paddingbtm">
							<div class="large-12 columns paddingzero">
								<div class="large-12 columns viewgridcolorstyle paddingzero">
									<div class="large-12 columns headerformcaptionstyle" style=" height:auto; padding: 0.2rem 0 0rem;">
										<span class="large-6 medium-6 small-12 columns lefttext small-only-text-center" >Activity List</span>
										<span class="large-6 medium-6 small-12 columns innergridicon righttext small-only-text-center">
											<span id="exporticon" class="icon icon-pencil editiconclass " title="Export to Google Calendar"> </span>
										</span>
									</div>
									<input type='hidden' id='activitiesid' name='activitiesid' value=''/>
									<?php
										$device = $this->Basefunctions->deviceinfo();
										if($device=='phone') {
											echo '<div class="large-12 columns forgetinggridname" id="activitygridwidth" style="padding-left:0;padding-right:0;"><div id="activitygrid">
												<!-- Table header content , data rows & pagination for mobile-->
											</div>
											<footer class="finner-gridfooter footercontainer" id="activitygridfooter">
												<!-- Footer & Pagination content -->
											</footer></div>';
										} else {
											echo '<div class="large-12 columns forgetinggridname" id="activitygridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="activitygrid" style="max-width:2000px; height:460px;top:0px;">
											<!-- Table content[Header & Record Rows] for desktop-->
											</div>
											<div class="inner-gridfooter footercontainer" id="activitygridfooter">
												<!-- Footer & Pagination content -->
											</div></div>';
										}
									?>
								</div>
							</div>
						</div>
					</form>
				</span>
				<span class="hidedisplay" id="taskexportspan">
					<form method="POST" name=""  id="taskgridlistexport" action="" enctype="" class="clearformoverlay">
						<div class="large-12 columns paddingbtm">
							<div class="large-12 columns paddingzero">
								<div class="large-12 columns viewgridcolorstyle paddingzero">
									<div class="large-12 columns headerformcaptionstyle" style=" height:auto; padding: 0.2rem 0 0rem;">
										<span class="large-6 medium-6 small-12 columns lefttext small-only-text-center" >Task List</span>
										<span class="large-6 medium-6 small-12 columns innergridicon righttext small-only-text-center">
											<span id="exporttaskicon" class="icon icon-pencil editiconclass " title="Export to Google Task"> </span>
										</span>
									</div>
									<input type='hidden' id='tasklistid' name='tasklistid' value=''/>
									<?php
										$device = $this->Basefunctions->deviceinfo();
										if($device=='phone') {
											echo '<div class="large-12 columns forgetinggridname" id="tasklistgridwidth" style="padding-left:0;padding-right:0;"><div id="tasklistgrid">
												<!-- Table header content , data rows & pagination for mobile-->
											</div>
											<footer class="finner-gridfooter footercontainer" id="tasklistgridfooter">
												<!-- Footer & Pagination content -->
											</footer></div>';
										} else {
											echo '<div class="large-12 columns forgetinggridname" id="tasklistgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="tasklistgrid" style="max-width:2000px; height:460px;top:0px;">
											<!-- Table content[Header & Record Rows] for desktop-->
											</div>
											<div class="inner-gridfooter footercontainer" id="tasklistgridfooter">
												<!-- Footer & Pagination content -->
											</div></div>';
										}
									?>
								</div>
							</div>
						</div>
					</form>
				</span>	
			</div>
		</div>
	</div>
</div>
<!--// Kumaresan - Calendar drag event confirmation overlay-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="calconfirmoverlay">	
		<div class="row toppadding">&nbsp;</div>
		<div class="overlaybackground">
			<div class="alert-panel">
				<div class="alertmessagearea">
					<div class="row">&nbsp;</div>
					<div class="alert-message">Are you sure about this change?</div>
				</div>
				<div class="row">&nbsp;</div>
				<div class="alertbuttonarea">
					<span class="firsttab" tabindex="1000"></span>
					<input type="button" id="calconfirmyes" name="" value="Agree" tabindex="1001" class="alertbtnyes ffield " >	
					<input type="button" id="calconfirmno" name="" value="Cancel" tabindex="1002" class="alertbtnno flloop  alertsoverlaybtn" >
					<input type="hidden" id="geventid" name="gdeleventid" value="" class="" >
					<span class="lasttab" tabindex="1003"></span>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Event detail -->
	<div  id="eventdetailcontent"></div>
<!-- invoice overlay redirect -->
<div class="overlay alertsoverlay overlayalerts" id="coninvoverlay">	
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>		
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="overlaybackground">
		<div class="alert-panel">
			<div class="alertmessagearea">
				<div class="row">&nbsp;</div>
				<div class="alert-message">
					<span id="bookedmessage">Do You Want To Generate Invoice?</span>
				</div>
			</div>
			<div class="row">&nbsp;</div>
			<div class="alertbuttonarea">
				<span class="firsttab" tabindex="1000"></span>
				<input type="button" id="convertyes" name="" value="Yes" tabindex="1001" class="alertbtnyes ffield" >
				<input type="button" id="convertno" name="" value="No" tabindex="1002" class="alertbtnno flloop alertsoverlaybtn" >
				<span class="lasttab" tabindex="1003"></span>
			</div>
		</div>
	</div>
</div>