<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Model File
class Calendarmodel extends CI_Model {    
    public function __construct() {		
		parent::__construct(); 
		$this->load->model('Base/Crudmodel');	
    } 
	//data insert function
	public function tasknewdatacreatemodel(){
		$name= $_POST['tasktitlename'];
		$start= $_POST['taskstartdate'];
		$employeeid= $_POST['taskassignto'];
		$statusid= $_POST['taskstatus'];
		if($statusid !=''){$statusid = $statusid;}else {$statusid = 1;}
		$priorityid= $_POST['taskpriority'];
		if($priorityid !=''){$priorityid = $priorityid;}else {$priorityid = 1;}
		$moduleid= $_POST['taskmodule'];
		if($moduleid !=''){$moduleid = $moduleid;}else {$moduleid = 1;}
		if(isset($_POST['taskrecord'])){
			$commonid = $_POST['taskrecord'];
		}else{
			$commonid = 1;
		}
		if($commonid !=''){$commonid = $commonid;}else {$commonid = 1;}
		$userid = $this->Basefunctions->userid;
		$cdate = date($this->Basefunctions->datef);
		$taskarray = array(
				'crmtaskname'=>$name,
				'employeeid'=>$employeeid,
				'taskstartdate'=> $this->Basefunctions->ymddateconversion($start),//date('Y-m-d', strtotime($start)),//$start,
				'priorityid'=>$priorityid,
				'crmstatusid'=>$statusid,
				'commonid'=>$commonid,
				'moduleid'=>$moduleid,
				'industryid'=>$this->Basefunctions->industryid,
				'branchid'=>$this->Basefunctions->branchid,
				'createdate'=>$cdate,
				'lastupdatedate'=>$cdate,
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
				'status'=>$this->Basefunctions->activestatus
			);
		$this->db->insert('crmtask',$taskarray);
		$primaryid = $this->Basefunctions->maximumid('crmtask','crmtaskid');	
		//notification entry
		$userid = $this->Basefunctions->userid;
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		if(isset($_POST['taskassignto'])){
			$assignid = $_POST['taskassignto'];if($assignid != ''){$assignid = $assignid;}else{$assignid = 1;}
		}else{$assignid = 1;}
		$taskname = $_POST['tasktitlename'];
		if($assignid == '1'){
			$assignto = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$assignid);
			$notimsg = $empname." "."Created a Task"." - ".$taskname;
		}else {
			$assignto = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$assignid);
			$notimsg = $empname." "."Created a Task"." - ".$taskname." - Assinged To ".$assignto;
		}
		$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,206);		
		echo "TRUE";
	}
	//data insert function
	public function activitynewdatacreatemodel(){
		$name= $_POST['activityname'];
		$start= $_POST['activitystartdate'];
		$starttime= $_POST['activitystarttime'];
		$end= $_POST['activityenddate'];
		$endtime= $_POST['activityendtime'];
		$employeeid= $_POST['activityassignto'];
		$statusid= $_POST['activitystatus'];
		if($statusid !=''){$statusid = $statusid;}else {$statusid = 1;}
		$priorityid= $_POST['activitypriority'];
		if($priorityid !=''){$priorityid = $priorityid;}else {$priorityid = 1;}
		if(isset($_POST['activitymodule'])){
			$moduleid= $_POST['activitymodule'];
		} else {
			$moduleid= 1;
		}
		if($moduleid !=''){$moduleid = $moduleid;}else {$moduleid = 1;}
		if(isset($_POST['activityrecord'])){
			$commonid= $_POST['activityrecord'];
		} else {
			$commonid= 1;
		}
		if($commonid !=''){$commonid = $commonid;}else {$commonid = 1;}
		if(isset($_POST['contactid'])){
			$contactid= $_POST['contactid'];
		} else {
			$contactid= 1;
		}
		if($contactid !=''){$contactid = $contactid;}else {$contactid = 1;}
		$productid = '';
		if(isset($_POST['productid'])) {
			$productid= $_POST['productid'];
		}
		if($productid !='' ){$productid = implode(',',$productid);}else {$productid = 1;}
		$description = '';
		if(isset($_POST['description'])) {
			$description= $_POST['description'];
		}
		$userid = $this->Basefunctions->userid;
		$cdate = date($this->Basefunctions->datef);
		$taskarray = array(
				'crmactivityname'=>$name,
				'employeeid'=>$employeeid,
				'activitystartdate'=>$this->Basefunctions->ymddateconversion($start),//$start,
				'activitystarttime'=>$starttime,
				'activityenddate'=>$this->Basefunctions->ymddateconversion($end),//$end,
				'activityendtime'=>$endtime,
				'priorityid'=>$priorityid,
				'crmstatusid'=>$statusid,
				'contactid'=>$contactid,
				'commonid'=>$commonid,
				'moduleid'=>$moduleid,
				'description'=>$description,
				'productid'=>$productid,
				'industryid'=>$this->Basefunctions->industryid,
				'branchid'=>$this->Basefunctions->branchid,
				'createdate'=>$cdate,
				'lastupdatedate'=>$cdate,
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
				'status'=>$this->Basefunctions->activestatus
			);
		$this->db->insert('crmactivity',$taskarray);
		$primaryid = $this->Basefunctions->maximumid('crmactivity','crmactivityid');
		//notification entry
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		if(isset($_POST['activityassignto'])){
			$assignid = $_POST['activityassignto'];if($assignid != ''){$assignid = $assignid;}else{$assignid = 1;}
		}else{$assignid = 1;}
		$activityname = $_POST['activityname'];
		if($assignid == '1'){
			$assignto = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$assignid);
			$notimsg = $empname." "."Created a Activity"." - ".$activityname;
		}else {
			$assignto = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$assignid);
			$notimsg = $empname." "."Created a Activity"." - ".$activityname." - Assinged To ".$assignto;
		}
		$this->Basefunctions->notificationcontentadd($primaryid,'Added',$notimsg,$assignid,205);
		echo $primaryid;
	}
	//task default value get
	public function taskdefaultvaluegetmodel(){
		$taxid=$this->Basefunctions->maximumid('crmtask','crmtaskid'); 
		if($taxid != ""){	
			echo json_encode($taxid);
		}else{			
			echo json_encode(array("fail"=>'FAILED'));
		}
	}		
	//activity default value get
	public function activitydefaultvaluegetmodel(){
		$taxid=$this->Basefunctions->maximumid('crmactivity','crmactivityid'); 
		if($taxid != ""){	
			echo json_encode($taxid);
		}else{			
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//fetch task data
	public function fetchtaskvaluemodel(){
		$id = $_GET['datarowid'];
		$dateformat = $this->Basefunctions->phpmysqlappdateformatfetch();
		$industryid = $this->Basefunctions->industryid;
		$mysqlformat = $dateformat['mysqlformat'];
		$result = $this->db->query('select crmtaskname,employeeid,DATE_FORMAT(taskstartdate,"'.$mysqlformat.'") AS startdate,DATE_FORMAT(taskenddate,"'.$mysqlformat.'") AS enddate,taskstarttime,taskendtime,priorityid,crmstatusid,moduleid,commonid from crmtask where crmtask.crmtaskid='.$id.' AND crmtask.status=1 AND crmtask.industryid in ('.$industryid.')');
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				$data = array('taskname'=>$row->crmtaskname,'empid'=>$row->employeeid,'startdate'=>$row->startdate,'enddate'=>$row->enddate,'starttime'=>$row->taskstarttime,'endtime'=>$row->taskendtime,'priority'=>$row->priorityid,'status'=>$row->crmstatusid,'datarowid'=>$id,'mid'=>$row->moduleid,'rid'=>$row->commonid);
				}
			echo json_encode($data);
		}else{			
			echo json_encode(array("fail"=>'FAILED'));
		} 
	}
	//fetch activity data
	public function fetchactivityvaluemodel(){
		$id = $_GET['datarowid'];
		$dateformat = $this->Basefunctions->phpmysqlappdateformatfetch();
		$industryid = $this->Basefunctions->industryid;
		$mysqlformat = $dateformat['mysqlformat'];
		$result = $this->db->query('select crmactivity.crmactivityname,crmactivity.employeeid,DATE_FORMAT(activitystartdate,"'.$mysqlformat.'") AS startdate,DATE_FORMAT(activityenddate,"'.$mysqlformat.'") AS enddate,crmactivity.activitystarttime,crmactivity.activityendtime,crmactivity.priorityid,crmactivity.crmstatusid,crmactivity.moduleid,crmactivity.commonid,crmactivity.productid,crmactivity.description,crmactivity.contactid,contact.contactname,contact.mobilenumber from crmactivity left outer join contact on contact.contactid=crmactivity.contactid where crmactivity.crmactivityid='.$id.' AND crmactivity.status=1 AND crmactivity.industryid in ('.$industryid.')');
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data = array('activityname'=>$row->crmactivityname,'empid'=>$row->employeeid,'startdate'=>$row->startdate,'enddate'=>$row->enddate,'starttime'=>$row->activitystarttime,'endtime'=>$row->activityendtime,'priority'=>$row->priorityid,'status'=>$row->crmstatusid,'datarowid'=>$id,'mid'=>$row->moduleid,'rid'=>$row->commonid,'productid'=>$row->productid,'description'=>$row->description,'contactname'=>$row->contactname,'mobilenumber'=>$row->mobilenumber,'contactid'=>$row->contactid);
				}
			echo json_encode($data);
		}else{			
			echo json_encode(array("fail"=>'FAILED'));
		} 
	}
	//calender task update
	public function taskupadatefunctionmodel()
	{
		$name= $_POST['tasktitlename'];
		$start= $_POST['taskstartdate'];
		$employeeid= $_POST['taskassignto'];
		if(isset($_POST['taskstatus'])){
			$statusid= $_POST['taskstatus'];
			if($statusid !='') {
				$statusid = $statusid;
			} else {
				$statusid = 1;
			}
		} else {
			$statusid = 1;
		}
		if(isset($_POST['taskpriority'])){
			$priorityid= $_POST['taskpriority'];
			if($priorityid !='') {
				$priorityid = $priorityid;
			}else {
				$priorityid = 1;
			}
		}else {
			$priorityid = 1;
		}
		if(isset($_POST['taskmodule'])){
			$moduleid= $_POST['taskmodule'];
			if($moduleid !=''){
				$moduleid = $moduleid;
			} else {
				$moduleid = 1;
			}
		}else {
			$moduleid = 1;
		}
		if(isset($_POST['taskrecord'])){
			$commonid= $_POST['taskrecord'];
			if($commonid !=''){
				$commonid = $commonid;
			}else {
				$commonid = 1;
			}
		} else {
			$commonid = 1;
		}
		$userid = $this->Basefunctions->userid;
		$cdate = date($this->Basefunctions->datef);
		$datarowid= $_GET['datarowid'];
		$taskarray = array(
				'crmtaskname'=>$name,
				'employeeid'=>$employeeid,
				'taskstartdate'=>$this->Basefunctions->ymddateconversion($start),//$start,
				'priorityid'=>$priorityid,
				'crmstatusid'=>$statusid,
				'commonid'=>$commonid,
				'moduleid'=>$moduleid,
				'createdate'=>$cdate,
				'lastupdatedate'=>$cdate,
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
				'status'=>$this->Basefunctions->activestatus
			);
			$this->db->where('crmtask.crmtaskid',$datarowid);
			$this->db->update('crmtask',$taskarray);
		//notification entry
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		$taskid = $this->Basefunctions->generalinformaion('crmtask','gtaskid','crmtaskid',$datarowid);
		if(isset($_POST['taskassignto'])){
			$assignid = $_POST['taskassignto'];if($assignid != ''){$assignid = $assignid;}else{$assignid = 1;}
		}else{$assignid = 1;}
		$taskname = $_POST['tasktitlename'];
		if($assignid == '1'){
			$assignto = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$assignid);
			$notimsg = $empname." "."Updated a Task"." - ".$taskname;
		}else {
			$assignto = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$assignid);
			$notimsg = $empname." "."Updated a Task"." - ".$taskname." - Assinged To ".$assignto;
		}
		$this->Basefunctions->notificationcontentadd($datarowid,'Updated',$notimsg,$assignid,206);
		echo "TRUE";
	}
	//Task drag update
	public function taskdragupdatemodel()
	{
		$taskstartdate = $_GET['taskstartdate'];
		$datarowid = $_GET['datarowid'];
		$userid = $this->Basefunctions->userid;
		$cdate = date($this->Basefunctions->datef);
		$taskarray = array(
				'taskstartdate'=>$this->Basefunctions->ymddateconversion($taskstartdate),//due date
				'taskstarttime'=>'00:00:00',
				'taskendtime'=>'01:00:00',
				'createdate'=>$cdate,
				'lastupdatedate'=>$cdate,
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
			);
		$this->db->where('crmtask.crmtaskid',$datarowid);
		$this->db->update('crmtask',$taskarray);
		echo "TRUE";
	}
	public function taskdragupdatemodel_api()
	{
		$taskstartdate = $_GET['taskstartdate'];
		$datarowid = $_GET['datarowid'];
		$userid = $this->Basefunctions->userid;
		$cdate = date($this->Basefunctions->datef);
		$taskarray = array(
				'taskstartdate'=>$this->Basefunctions->ymddateconversion($taskstartdate),//due date
				'taskstarttime'=>'00:00:00',
				'taskendtime'=>'01:00:00',
				'createdate'=>$cdate,
				'lastupdatedate'=>$cdate,
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
			);
		$this->db->where('crmtask.crmtaskid',$datarowid);
		$this->db->update('crmtask',$taskarray);
		return "TRUE";
	}
	//Activity drag update
	public function activitydragupdatemodel()
	{
		$activitystartdate = $_GET['activitystartdate'];
		$activityenddate = $_GET['activityenddate'];
		$activitystarttime = $_GET['activitystarttime'];
		$activityendtime = $_GET['activityendtime'];
		$datarowid = $_GET['datarowid'];
		$userid = $this->Basefunctions->userid;
		$cdate = date($this->Basefunctions->datef);
		$activityarray = array(
				'activitystartdate'=>$this->Basefunctions->ymddateconversion($activitystartdate),//start date
				'activityenddate'=>$this->Basefunctions->ymddateconversion($activityenddate),//end date
				'activitystarttime'=>$activitystarttime,
				'activityendtime'=>$activityendtime,
				'createdate'=>$cdate,
				'lastupdatedate'=>$cdate,
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
		);
		$this->db->where('crmactivity.crmactivityid',$datarowid);
		$this->db->update('crmactivity',$activityarray);
		echo "TRUE";
	}
	public function activitydragupdatemodel_api()
	{
		$activitystartdate = $_GET['activitystartdate'];
		$activityenddate = $_GET['activityenddate'];
		$activitystarttime = $_GET['activitystarttime'];
		$activityendtime = $_GET['activityendtime'];
		$datarowid = $_GET['datarowid'];
		$userid = $this->Basefunctions->userid;
		$cdate = date($this->Basefunctions->datef);
		$activityarray = array(
				'activitystartdate'=>$this->Basefunctions->ymddateconversion($activitystartdate),//start date
				'activityenddate'=>$this->Basefunctions->ymddateconversion($activityenddate),//end date
				'activitystarttime'=>$activitystarttime,
				'activityendtime'=>$activityendtime,
				'createdate'=>$cdate,
				'lastupdatedate'=>$cdate,
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
		);
		$this->db->where('crmactivity.crmactivityid',$datarowid);
		$this->db->update('crmactivity',$activityarray);
		return "TRUE";
	}
	//Activity resize update
	public function activityresizeupdatemodel()
	{
		$activitystarttime = $_GET['activitystarttime'];
		$activityendtime = $_GET['activityendtime'];
		$datarowid = $_GET['datarowid'];
		$userid = $this->Basefunctions->userid;
		$cdate = date($this->Basefunctions->datef);
		$activityarray = array(
				'activitystarttime'=>$activitystarttime,
				'activityendtime'=>$activityendtime,
				'createdate'=>$cdate,
				'lastupdatedate'=>$cdate,
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
		);
		$this->db->where('crmactivity.crmactivityid',$datarowid);
		$this->db->update('crmactivity',$activityarray);
		echo "TRUE";
	}
	//calender activity update
	public function activityupadatefunctionmodel() {
		$name= $_POST['activityname'];
		$start= $_POST['activitystartdate'];
		$starttime= $_POST['activitystarttime'];
		$end= $_POST['activityenddate'];
		$endtime= $_POST['activityendtime'];
		$employeeid= $_POST['activityassignto'];
		if(isset($_POST['activitystatus'])){
			$statusid= $_POST['activitystatus'];
			if($statusid !=''){$statusid = $statusid;}else {$statusid = 1;}
		}else {$statusid = 1;}
		if(isset($_POST['activitypriority'])){
		$priorityid= $_POST['activitypriority'];
		if($priorityid !=''){$priorityid = $priorityid;}else {$priorityid = 1;}
		}else {$priorityid = 1;}
		if(isset($_POST['activitymodule'])){
			$moduleid= $_POST['activitymodule'];
			if($moduleid !=''){
				$moduleid = $moduleid;
			} else {
				$moduleid = 1;
			}
		} else {
			$moduleid = 1;
		}
		if(isset($_POST['activityrecord'])){
			$commonid= $_POST['activityrecord'];
			if($commonid !=''){
				$commonid = $commonid;
			}else {
				$commonid = 1;
			}
		} else {
			$commonid = 1;
		}
		if(isset($_POST['productid'])){
			$productid= $_POST['productid'];
			if($productid !=''){
				$productid =implode(',',$productid);
			}else {
				$productid = 1;
			}
		} else {
			$productid = 1;
		}
		if(isset($_POST['contactid'])){
			$contactid= $_POST['contactid'];
		} else {
			$contactid= 1;
		}
		if($contactid !=''){$contactid = $contactid;}else {$contactid = 1;}
		$description= $_POST['description'];
		$userid = $this->Basefunctions->userid;
		$cdate = date($this->Basefunctions->datef);
		$datarowid= $_GET['datarowid'];
		$taskarray = array(
				'crmactivityname'=>$name,
				'employeeid'=>$employeeid,
				'activitystartdate'=>$this->Basefunctions->ymddateconversion($start),//$start,
				'activitystarttime'=>$starttime,
				'activityenddate'=>$this->Basefunctions->ymddateconversion($end),//$end,
				'activityendtime'=>$endtime,
				'priorityid'=>$priorityid,
				'crmstatusid'=>$statusid,
				'contactid'=>$contactid,
				'commonid'=>$commonid,
				'moduleid'=>$moduleid,
				'description'=>$description,
				'productid'=>$productid,
				'createdate'=>$cdate,
				'lastupdatedate'=>$cdate,
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
				'status'=>$this->Basefunctions->activestatus
			);
			$this->db->where('crmactivity.crmactivityid',$datarowid);
			$this->db->update('crmactivity',$taskarray);
			//notification entry
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		if(isset($_POST['activityassignto'])){
			$assignid = $_POST['activityassignto'];if($assignid != ''){$assignid = $assignid;}else{$assignid = 1;}
		}else{$assignid = 1;}
		$activityname = $_POST['activityname'];
		if($assignid == '1'){
			$assignto = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$assignid);
			$notimsg = $empname." "."Updated a Activity"." - ".$activityname;
		}else {
			$assignto = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$assignid);
			$notimsg = $empname." "."Updated a Activity"." - ".$activityname." - Assinged To ".$assignto;
		}
		$this->Basefunctions->notificationcontentadd($datarowid,'Updated',$notimsg,$assignid,205);
		echo "TRUE";
	}
	//record delete for task
	public function recorddeltefortaskmodel()
	{
		$id = $_GET['datarowid'];
		$task = array('status'=>0);
		$this->db->where('crmtask.crmtaskid',$id);
		$this->db->update('crmtask',$task);
		$taskid = $this->Basefunctions->generalinformaion('crmtask','gtaskid','crmtaskid',$id);
		echo 'TRUE';
	}
	//record delete for activity
	public function recorddelteforactivitymodel()
	{
		$id = $_GET['datarowid'];
		$task = array('status'=>0);
		$this->db->where('crmactivity.crmactivityid',$id);
		$this->db->update('crmactivity',$task);
		echo "TRUE";
	}
	//task data view fetch 
	public function taskviewdatafetchmodel()
	{
		$i=0;
		$title = '';
		$this->db->select('crmtaskid,crmtaskname,employeeid,taskstartdate,taskenddate,taskstarttime,taskendtime,crmtask.moduleid,module.modulename,module.modulemastertable,crmtask.commonid,crmtask.priorityid,priority.priorityname,crmstatus.crmstatusname');
		$this->db->from('crmtask');
		$this->db->join('module','module.moduleid=crmtask.moduleid');
		$this->db->join('priority','priority.priorityid=crmtask.priorityid');
		$this->db->join('crmstatus','crmstatus.crmstatusid=crmtask.crmstatusid');
		$this->db->where_in('crmtask.industryid',$this->Basefunctions->industryid);
		$this->db->where('crmtask.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				//for start time
				$start = $row->taskstartdate;
				$starttime = $row->taskstarttime;
				$timestamp = strtotime($start);
				$sdate = date("Y-m-d", $timestamp);
				$st = $sdate."T".$starttime;
				//for end time
				$end = $row->taskenddate;
				$endtime = $row->taskendtime;
				$etimestamp = strtotime($end);
				$edate = date("Y-m-d", $etimestamp);
				$et = $edate."T".$endtime;
				$title = $row->crmtaskname;
				$data[$i] = array('title'=>$title,'start'=>$st,'end'=>$et,'id'=>$row->crmtaskid,'color'=>'#546E7A','type'=>'1');
				$i++;
				}
			echo json_encode($data);
		} else {
			
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//Activities data view fetch 
	public function activitiesviewdatafetchmodel() {
		$i=0;
		$title = '';
		$this->db->select('crmactivityid,crmactivityname,employeeid,activitystartdate,activityenddate,	activitystarttime,activityendtime,crmactivity.moduleid,module.modulename,module.modulemastertable,crmactivity.commonid,activitytype.activitytypename,crmactivity.priorityid,priority.priorityname,crmstatus.crmstatusname');
		$this->db->from('crmactivity');
		$this->db->join('activitytype','activitytype.activitytypeid=crmactivity.activitytypeid');
		$this->db->join('priority','priority.priorityid=crmactivity.priorityid');
		$this->db->join('crmstatus','crmstatus.crmstatusid=crmactivity.crmstatusid');
		$this->db->join('module','module.moduleid=crmactivity.moduleid');
		$this->db->where_in('crmactivity.industryid',$this->Basefunctions->industryid);
		$this->db->where('crmactivity.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				//for start time
				$start = $row->activitystartdate;
				$starttime = $row->activitystarttime;
				$timestamp = strtotime($start);
				$sdate = date("Y-m-d", $timestamp);
				$st = $sdate."T".$starttime;
				//for end time
				$end = $row->activityenddate;
				$endtime = $row->activityendtime;
				$etimestamp = strtotime($end);
				$edate = date("Y-m-d", $etimestamp);
				$et = $edate."T".$endtime;
				
				if($row->activitytypename != '') {
					$title = $row->activitytypename.'-'.$row->crmactivityname;
				} else{
					$title = $row->crmactivityname;
				}
				$data[$i] = array('title'=>$title,'start'=>$st,'end'=>$et,'id'=>$row->crmactivityid,'color'=>'#44d4c7','type'=>'2');
				$i++;
				}
			echo json_encode($data);
		}else{
			
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//related record data fetch
	public function relatedrecordfetch($moduleid,$commonid,$modulemastertable) {
		$data ='test';
		return $data;
	}
	//simple group drop down
	public function simplegroupdropdown($tablename,$tblfieldid,$tblfieldname,$moduleid) {
		$industryid = $this->Basefunctions->industryid;
		$this->db->select($fieldname);
		$this->db->from($tablename);
		$this->db->where('moduleid',$moduleid);
		$this->db->where_not_in('Status',array(0,3));
		if($tablename == "employee" && !in_array($this->userroleid,$this->adminuserrole)) {	
			$this->db->where('employee.EmployeeId',$this->userid);
		}
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->order_by($order,"asc");
		$query= $this->db->get();
        return $query->result();
	}
	//simple drop down value fetch
	public function simpledropdown($tablename,$fieldname,$order,$moduleid) {
		$industryid = $this->Basefunctions->industryid;
		$this->db->select($fieldname);
		$this->db->from($tablename);
		$this->db->where_not_in('status',array(0,3));
		$this->db->where_in('moduleid',array($moduleid));
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->order_by($order,"asc");
		$query= $this->db->get();
        return $query->result();
	}
	//drop down value fetch based on the modules
	public function dropdownvaluefecthmodel() {
		$id = $_GET['moduleid'];
		$this->db->select('modulefieldid',false);
		$this->db->from('primaryfieldmapping');
		$this->db->where('primaryfieldmapping.moduleid',$id);
		$this->db->where('primaryfieldmapping.status',1);
		$result = $this->db->get();
		if($result ->num_rows() > 0) {
			foreach($result->result() as $row) {
				$mfid = $row->modulefieldid;
			}
		}
		$this->db->select('columnname,tablename',false);
		$this->db->from('modulefield');
		$this->db->where('modulefield.modulefieldid',$mfid);
		$this->db->where('modulefield.status',1);
		$result = $this->db->get();
		if($result ->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('filedname'=>$row->columnname,'tablename'=>$row->tablename);
			}
			echo json_encode($data);
		} else {
			echo json_encode(array('failed'=>'Failure'));
		}
	}
	//record name fetch
	public function recordnamefetchmodel() {
		$i =0;
		$tablename = $_GET['tabname'];
		$fieldname = $_GET['fieldname'];
		$fieldid = $tablename.'id';
		$industryid = $this->Basefunctions->industryid;
		if($tablename == 'lead' || $tablename == 'contact' || $tablename == 'employee') {
			$this->db->select($fieldid.','.$fieldname.',CONCAT(salutation.salutationname, '.$fieldname.', lastname) AS name', FALSE);
			$this->db->from($tablename);
			$this->db->join('salutation','salutation.salutationid='.$tablename.'.salutationid','left outer');
		} else {
			$this->db->select($fieldname.' AS name ,'.$fieldid.','.$fieldname);
			$this->db->from($tablename);
		}
		$this->db->where("FIND_IN_SET('".$industryid."',$tablename.industryid) >", 0);
		$this->db->where($tablename.'.'.'status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$data[$i] = array('id'=>$row->$fieldid,'name'=>$row->name);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}
	}
	//From application - subscriber list view
	public function activitylistfetchgrid($tablename,$sortcol,$sortord,$pagenum,$rowscount,$colinfo,$viewcolmoduleids) {
		$count = count($colinfo['colmodelindex']);
		$dataset ='';
		$x = 0;
		$joinq = '';
		$ptab = array($tablename);
		$jtab = array($tablename);
		$maintable = $tablename;
		$selvalue = $tablename.'.'.$tablename.'id';
		for($k=0;$k<$count;$k++) {
			if($k==0) {
				$dataset = $colinfo['colmodelindex'][$k];
			} else{
				$dataset .= ','.$colinfo['colmodelindex'][$k];
			}
			if($x != 0) { $con = "AND"; }
			//parent join check
			$ptabjoin = "";
			$ptabjoin = $colinfo['colmodelparenttable'][$k];
			if(in_array($ptabjoin,$ptab)) {
				if(!in_array($colinfo['coltablename'][$k],$jtab) && $colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
					array_push($jtab,trim($colinfo['coltablename'][$k]));
					//employee
					$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid LIKE "%1%"';
					//group
					$joinq .= ' LEFT OUTER JOIN employeegroup ON FIND_IN_SET(employeegroup.employeegroupid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid  LIKE "%2%"';
					//user role and sub ordinate
					$joinq .= ' LEFT OUTER JOIN userrole ON FIND_IN_SET(userrole.userroleid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND ('.$maintable.'.employeetypeid LIKE "%3%" OR '.$maintable.'.employeetypeid LIKE "%4%")';
				} else {
					if($colinfo['colmodeldatatype'][$k] == 1) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						}
					} else if($colinfo['colmodeldatatype'][$k] == 2) {
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else if($colinfo['colmodeldatatype'][$k] == 3) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						}
					}
				}
			} else {
				if($colinfo['colmodeldatatype'][$k] == 1) {
					$result = $this->Basefunctions->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.','.$maintable.'.'.$primaryid.') > 0 AND '.$ptabjoin.'.status=1';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 1) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
									}
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 2) {
					$joinresult = $this->parentjointableinfo($ptabjoin,$viewcolmoduleids);
					if($joinresult->num_rows() >0) {
						foreach($joinresult->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
							//condition name
							$condcolname = $show->viewcreationcolmodelcondname;
							//condition value
							$condcolvalue = $show->viewcreationcolmodelcondvalue;
						}
						//restrict repeated parent join
						if(in_array($destab,$ptab)) {
							if($destab != $soucrcetab) {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							} else {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							}
						}
						//parent table condition
						if($condcolname != "" && $condcolvalue != "") {
							$attrcond .= " ".$con." ".$srcjointab.'.'.$condcolname."=".$condcolvalue;
							array_push($attrcondarr,$srcjointab.'.'.$joincolname);
							$x = 1;
						}
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] = $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.'='.$maintable.'.'.$primaryid.') > 0 AND '.$ptabjoin.'.status=1';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 2) {
								//attribute join
								if(!in_array($colinfo['coltablename'][$k],$jtab)) {
									array_push($jtab,trim($colinfo['coltablename'][$k]));
									$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
								}
								//condition
								if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
									$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
									array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
									$x = 1;
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 3) {
					$result = $this->Basefunctions->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						}
					}  else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.','.$maintable.'.'.$primaryid.') > 0 AND '.$ptabjoin.'.status=1';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 3) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
									}
								}
							}
						}
					}
				}
			}
			$dduitypeids = array('17','18','19','23','25','26','27','28','29');
			//fields fetch
			if($colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->Basefunctions->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].', GROUP_CONCAT(DISTINCT userrole.userrolename) AS rolename, GROUP_CONCAT(DISTINCT employeegroup.employeegroupname) AS empgrpname';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k].',userrole.userrolename AS rolename,employeegroup.employeegroupname AS empgrpname';
				}
			} else if( in_array($colinfo['colmodeluitype'][$k],$dduitypeids) ) {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->Basefunctions->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].'';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
				}
			} else {
				$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
			}
		}
		$status = $tablename.'.status IN (1)';
		if($viewcolmoduleids == '205'){
			$where = 'crmactivity.calendartype=1 AND crmactivity.eventid IS NULL';
		} else {
			$where = 'crmtask.tasktype=1 AND crmtask.gtaskid IS NULL';
		}
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		//query
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$selvalue.' from '.$tablename.' '.$joinq.' WHERE '.$where.' AND '.$status.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		return $data;
	}
	//dynamic view drop down
    public function dataviewbydropdown() {
    	$vmid = '205,206';
    	$userid = $this->Basefunctions->logemployeeid;
 		$this->db->select('viewcreation.viewcreationid,viewcreation.viewcreationname,datapreference.employeeid,datapreference.viewcreationid AS viewid,viewcreation.viewcreationmoduleid,viewcreationmodule.viewcreationmodulename');
    	$this->db->from('viewcreation');
    	$this->db->join('datapreference','datapreference.viewcreationid=viewcreation.viewcreationid','left outer');
    	$this->db->join('viewcreationmodule','viewcreationmodule.viewcreationmoduleid=viewcreation.viewcreationmoduleid','left outer');
    	$this->db->where( '( viewcreation.createuserid = '.$userid.' AND viewcreation.viewcreationmoduleid IN ('.$vmid.') AND viewcreation.status = 1 ) OR ( viewcreation.viewaspublic = "Yes" AND viewcreation.viewcreationmoduleid IN ('.$vmid.') AND viewcreation.status = 1  )' );
    	$this->db->order_by('viewcreation.viewcreationname',"asc");
    	$this->db->group_by("viewcreation.viewcreationid");
    	$query= $this->db->get();
    	return $query->result();
    }
    // view based data fetch
    public function dynamicdatafetchbasedonviewmodel() {
    	$data = '';
    	$viewid = $_GET['viewid'];
    	$moduleid = $_GET['moduleid'];
    	if($moduleid == 205) {
    		$data = $this->activitiesviewdatafetchmodel();
    	} else if($moduleid == 206) {
    		$data = $this->taskviewdatafetchmodel();
    	}
    	echo json_encode($data);
    }
    //status chnage
    public function statuschangemodel(){
    	$viewid = $_GET['viewid'];
    	$moduleid = $_GET['moduleid'];
    	if($moduleid == 206){
    		$statusid = 69;
    	} else {
    		$statusid = 75;
    	}
    	$date= date($this->Basefunctions->datef);
    	$userid= $this->Basefunctions->userid;
    	$updateayyay = array('status'=>'0','lastupdateuserid'=>$userid,'lastupdatedate'=>$date);
    	$this->db->update('crmstatus',$updateayyay);
    	$this->db->where('crmstatus.crmstatusid',$statusid);
    	echo 'TRUE';
    }
    //google calendar data insert
    public function googlecalendardatainsert() {
    	$eventtype = '';
    	$type = $_POST['type'];
    	$status = $_POST['status'];
	    $method = explode('#',$type);
	    $startdatetime = $_POST['startdatetime'];
	    $enddatetime = $_POST['enddatetime'];
	    $start = $this->datetimesplitfunction($startdatetime);
	    $end = $this->datetimesplitfunction($enddatetime);
    	if($method[1] == 'event') {
    		$insertarray = array(
    			'crmactivityname'=>$_POST['name'],
    			'eventid'=>$_POST['id'],
    			'activitystartdate'=>$start['date'],
    			'activityenddate'=>$end['date'],
    			'activitystarttime'=>$start['time'],
    			'activityendtime'=> $end['time'],
    			'industryid'=>$this->Basefunctions->industryid,
    			'branchid'=>$this->Basefunctions->branchid
    		);
    		$check = $this->checkidexistornot($_POST['id'],'eventid','crmactivity');
    		if($check == 'false') {
    			$eventtype = 'insert';
    			$newdefdataarr = $this->Crudmodel->defaultvalueget();
    			$nnewdata = array_merge($insertarray,$newdefdataarr);
    			$this->db->insert('crmactivity',$nnewdata);
    		} else {
    			if($status == 'cancelled') {
    				$eventtype = 'Delete';
    				$newdefdataarr = $this->Crudmodel->deletedefaultvalueget();
    			} else {
    				$eventtype = 'Update';
    				$newdefdataarr = $this->Crudmodel->updatedefaultvalueget();
    			}
    			$nnewdata = array_merge($insertarray,$newdefdataarr);
    			$this->db->where('crmactivity.crmactivityid',$check);
    			$this->db->update('crmactivity',$nnewdata);
    		}
    	} else {
    		$insertarray = array(
    			'crmtaskname'=>$_POST['name'],
    			'gtaskid'=>$_POST['id'],
    			'taskstartdate'=>$start['date'],
    			'taskenddate'=>$end['date'],
    			'taskstarttime'=>$start['time'],
    			'taskenddate'=> $end['time'],
    			'industryid'=>$this->Basefunctions->industryid,
    			'branchid'=>$this->Basefunctions->branchid
    		);
    		$check = $this->checkidexistornot($_POST['id'],'gtaskid','crmtask');
    		if($check == 'false') {
    			$eventtype = 'insert';
    			$newdefdataarr = $this->Crudmodel->defaultvalueget();
    			$nnewdata = array_merge($insertarray,$newdefdataarr);
    			$this->db->insert('crmtask',$nnewdata);
    		} else {
    			if($status == 'cancelled') {
    				$eventtype = 'Delete';
    				$newdefdataarr = $this->Crudmodel->deletedefaultvalueget();
    			} else {
    				$eventtype = 'Update';
    				$newdefdataarr = $this->Crudmodel->updatedefaultvalueget();
    			}
    			$nnewdata = array_merge($insertarray,$newdefdataarr);
    			$this->db->where('crmtask.crmtaskid',$check);
    			$this->db->update('crmtask',$nnewdata);
    		}
    	}
    	echo json_encode($eventtype);
    }
    //split function
    public function datetimesplitfunction($datetime){
    	$ndatetime = explode('T',$datetime);
    	if(count($ndatetime) > 1){
    		$ntime = explode('.',$ndatetime[1]);
    		$time = $ntime[0];
    		$date = $ndatetime[0];
    	} else {
    		$time = '00.00.00';
    		$date = $datetime;
    	}
    	$datetimearray = array ('date'=>$date,'time'=>$time);
    	return $datetimearray;
    }
    public function checkidexistornot($id,$field,$table) {
    	$this->db->select($table.'id');
    	$this->db->from($table);
    	$this->db->where($field,$id);
    	$result = $this->db->get();
    	if($result->num_rows() > 0){
    		foreach($result->result() as $row){
    			$id = $table.'id';
    			return $row->$id;
    		}
    	} else {
    		return 'false';
    	}
    }
    //get task module status
    public function gettaskstatus($tablename,$taskmodid) {
    	$userroleid = $this->Basefunctions->userroleid;
    	$status = 0;
    	$this->db->select('status');
    	$this->db->from($tablename);
    	$this->db->where($tablename.'.userroleid',$userroleid);
    	$this->db->where($tablename.'.moduleid',$taskmodid);
    	$result = $this->db->get();
    	if($result->num_rows() > 0) {
    		foreach($result->result() as $row){
    			$status = $row->status;
    		}
    	}
    	return $status;
    }
    //member data load
    public function memberdataload_dd(){
    	$i=0;
    	$data = array();
    	if(isset($_POST['q'])){
    		if(is_numeric($_POST['q'])){
    			$check = 'mobilenumber';
    		} else{
    			$check = 'contactname';
    		}
    		$filter =$_POST['q'];
    	} else {
    		$filter ='';
    	}
    	if(isset($_POST['acid'])){
    		$acid =$_POST['acid'];
    	} else{
    		$acid ='1';
    	}
    	if($acid != 1){
    		$query = $this->db->query(
    				"SELECT contact.contactname,contact.contactid,contact.mobilenumber
				FROM contact
				WHERE contact.status =1 and contact.contactid in (".$acid.") 
				ORDER BY contact.contactid asc
			");
    	} else {
    		if($check == 'contactname'){
    			$query = $this->db->query(
    					"SELECT contact.contactname,contact.contactid,contact.mobilenumber
				FROM contact
				LEFT OUTER JOIN contactcf ON contactcf.contactid = contact.contactid
				WHERE contact.status =1 and contact.contactname like '%".$filter."%'
				ORDER BY contact.contactid asc
			");
    		} else{
    			$query = $this->db->query(
    					"SELECT contact.contactname,contact.contactid,contact.mobilenumber
				FROM contact
				LEFT OUTER JOIN contactcf ON contactcf.contactid = contact.contactid
				WHERE contact.status =1 and contact.mobilenumber like '%".$filter."%'
				ORDER BY contact.contactid asc
			");
    		}
    			
    	}
    
    	foreach($query->result() as $row){
    		$data[$i] = array('id'=>$row->contactid,'text'=>$row->contactname,'mobilenumber'=>$row->mobilenumber);
    		$i++;
    	}
    	echo json_encode($data);
    }
}     