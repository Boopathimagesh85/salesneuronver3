<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Conversation extends MX_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model('Conversation/Conversationmodel');
		$this->load->model('Base/Basefunctions');
		$this->load->helper('formbuild');
		$this->load->library('pdf');
		$this->load->library('excel');
    }
    //first basic hitting view
    public function index() {	
		$moduleid = array(267);
		sessionchecker($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['moduleid'] = $this->Basefunctions->simpledropdownwithcond('moduleid','menuname','module','moduleprivilegeid',267);
		$this->load->view('Conversation/conversationview',$data); 
	}
	//view report list
	public function autorulelist() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'autofollowrule.autofollowruleid') : 'autofollowrule.autofollowruleid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('autofollowrulename','modulename','public','createdate'),'colmodelindex'=>array('autofollowrule.autofollowrulename','module.modulename','loginhistory.public','autofollowrule.createdate'),'coltablename'=>array('autofollowrule','module','autofollowrule','autofollowrule'),'uitype'=>array('2','2','2','2'),'colname'=>array('Rule Name','Module Name','Public','Created Date'),'colsize'=>array('300','351','300','350'));
		$result=$this->Conversationmodel->autoruleconceptlist($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$modid);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Auto Follow Rule',$width,$height);
		} else {
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Auto Follow Rule',$width,$height);
		}
		echo json_encode($datas);
	}
	//view drop down load-
	public function filedsdropdownload() {
		$this->Conversationmodel->filedsdropdownloadmodel();
	}
	//field name based drop down value
	public function fieldviewnamebespicklistdddvalue() {
		$this->Conversationmodel->fieldviewnamebespicklistdddvaluemodel();
	}
	//field name based drop down value
	public function viewfieldnamebesdddvalue() {
		$this->Conversationmodel->viewfieldnamebesdddvaluemodel();
	}
	//auto rule insertion
	public function autoruleinsertion() {
		$this->Conversationmodel->autoruleinsertionmodel();	
	}
	//auto follow rule delete
	public function autoruledelete() {
		$this->Conversationmodel->autoruledeletemodel();	
	}
	//auto follow rule edit operation
	public function autoruleeditdatafetch() {
		$this->Conversationmodel->autoruleeditdatafetchmodel();	
	}
	//auto follow rule condition fetch
	public function autofollowruledconditionfetchgrid() {
		$this->Conversationmodel->autofollowruledconditionfetchgrid();
	}
	//auto follow rule update
	public function autoruleupdate() {
		$this->Conversationmodel->autoruleupdatemodel();	
	}
	//view fetch based on module 
	public function viewfetchbasedonmodule() {
		$this->Conversationmodel->viewfetchbasedonmodulemodel();	
	}
	//auto rule fetch based on module 
	public function autorulefetchbasedonmodule() {
		$this->Conversationmodel->autorulefetchbasedonmodulemodel();	
	}
	//parent table get
	public function parenttableget() {
		$this->Conversationmodel->parenttablegetmodel();
	}
	//fetch json data information
	public function gridvalinformationfetch() {
		$creationid = $_GET['viewid'];
		$moduleid = $_GET['moduleid'];
		$information = $this->Basefunctions->gridinformationfetchmodel($creationid);
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$viewcolmoduleids = $_GET['moduleid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$ruleid = $_GET['ruleid'];
		$size = array();
		for($i=0;$i<count($information['colmodelname']);$i++) {
			$size[$i] = '200';
		}
		$colmodelname = $information['colmodelname'];
		$colmodelaliasname = $information['colmodelaliasname'];
		$coltablename = $information['coltablename'];
		$colmodeluitype = $information['colmodeluitype'];
		$colname = $information['colname'];
		$colsize = $size;
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : $primarytable.'.'.$primaryid) : $primarytable.'.'.$primaryid;
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>$colmodelname,'colmodelindex'=>$colmodelaliasname,'coltablename'=>$coltablename,'uitype'=>$colmodeluitype,'colname'=>$colname,'colsize'=>$colsize);
		$result=$this->Conversationmodel->viewdynamicdatainfofetch($creationid,$primaryid,$viewcolmoduleids,$sortcol,$sortord,$pagenum,$rowscount,$ruleid);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($result,$page,'');
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$moduleid,'Unnoticed Record List',$width,$height,$chkbox='false');
		} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$moduleid,'Unnoticed Record List',$width,$height,$chkbox='false');
		}
		echo json_encode($datas);
	}
	//un follow records
	public function unfollowrecords() {
		$this->Conversationmodel->unfollowrecordsmodel();
	}
	//mass update default view fetch
	public function defaultviewfetch() {
		$mid = $_GET['modid'];
		$viewid = $this->Conversationmodel->defaultviewfetchmodel($mid);
		echo $viewid;
	}
	//fetch json data information
	public function unnoticedrecordfetch() {
		$creationid = $_GET['viewid'];
		$moduleid = $_GET['moduleid'];
		$information = $this->Basefunctions->gridinformationfetchmodel($creationid);
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$chkbox = $_GET['checkbox'];
		$daycount = $_GET['daycount'];
		$size = array();
		for($i=0;$i<count($information['colmodelname']);$i++) {
			$size[$i] = '200';
		}
		$colmodelname = $information['colmodelname'];
		$colmodelaliasname = $information['colmodelaliasname'];
		$coltablename = $information['coltablename'];
		$colmodeluitype = $information['colmodeluitype'];
		$colname = $information['colname'];
		$colsize = $size;
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : $primarytable.'.'.$primaryid) : $primarytable.'.'.$primaryid;
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>$colmodelname,'colmodelindex'=>$colmodelaliasname,'coltablename'=>$coltablename,'uitype'=>$colmodeluitype,'colname'=>$colname,'colsize'=>$colsize);
		$result=$this->Conversationmodel->unnoticedrecordfetchmodel($primarytable,$primaryid,$sortcol,$sortord,$pagenum,$rowscount,$information,$moduleid,$daycount);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($result,$page,'');
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Unnoticed Record List',$width,$height,$chkbox);
		} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Unnoticed Record List',$width,$height,$chkbox);
		}
		echo json_encode($datas);
	}
	//create new reply-
	public function createreply(){	
		$this->Conversationmodel->createreply();		
	}
	//auto follow rule condition header information fetch
	public function autofollowruleconditioninformationfetch() {
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$modname = $_GET['modulename'];
		$colinfo = $this->Conversationmodel->autofollowruleconditioninformationfetchmodel($modid);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobileviewformtogriddatagenerate($colinfo,$modname,$width,$height);
		} else {
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height);
		}
		echo json_encode($datas);
	}
}