<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('Base/headerfiles'); ?>
	<link href="<?php echo base_url();?>css/jqgrid.min.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<style type="text/css">
	.sendbtnstyle {
    height: 52px;
}
</style>
<body>
	<div class="row" style="max-width:100%">
		<div class="off-canvas-wrap" data-offcanvas>
			<div class="inner-wrap">
				<div class="gridviewdivforsh" id="conversationview">
					<div class="large-12 columns paddingzero viewheader">
						<?php
							$device = $this->Basefunctions->deviceinfo();
							if($device=='phone') {
								echo '<ul class="actionicons-view">
									<li class="mobfilterspan"><i class="icon icon-filter"></i></li>
									<li>
										<nav class="top-bar" data-topbar style="display: inline-block; line-height: 25px; background:none;" role="navigation">
											<section class="top-bar-section" style="top:-15px; ">
												<ul class="right" style="height: 20px !important; background-color:#dae1e4;">
													<li class="has-dropdown" style="padding:0;">
														<span class="action-click"><i class="icon icon-show8"></i></span>
														<ul class="actionoverlay">
															<li id="conversation"><i class="icon-w icon-chat75 addiconclass" title="Conversation"></i></li>
															<li id="autofollowrule"><i class="icon-w icon-man460 editiconclass" title="Auto Follow Rule"></i></li>
															<li id="followedbymeid"><i class="icon-w icon-circles23 deleteiconclass" title="Followed By Me"></i></li>
															<li id="unnoticedrecordsid"><i class="material-icons reloadiconclass" title="Un Noticed Record">warning</i></li>
														</ul>
													</li>
													<script>
														$(document).ready(function() {
															setTimeout(function(){
																var height=$(window).height();
																height = height-105;';
															echo "$('.actionicons-view .actionoverlay').attr('style', 'height: '+height+'px !important');";
															echo '},10);
														});
													</script>
												</ul>
											</section>
										</nav>
									</li>
								</ul>';
							} else {
								$dataset['gridtitle'] = $gridtitle['title'];
								$this->load->view('mainviewheader',$dataset);
								echo'
							<div class="desktop actionmenucontainer headeraction-menu ">	
								<ul class="module-view">
										<li class="view-change-dropdown">
											<select id="conversationmoduleid" class="chzn-select dropdownchange" data-placeholder="Select View"  tabindex="" name="conversationmoduleid" title="" style="background-color: #7e57c2; border-radius: 2px;"><option value="1">Select</option><option value="267">Conversation</option>';
											?>
											<?php
											foreach($moduleid as $key):
											?>
											<option value="<?php echo $key->moduleid;?>"><?php echo $key->menuname;?></option>;
											<?php endforeach;?>
											<?php
											echo ' </select>
										</li>
									</ul>';	
								echo '<ul class="toggle-view tabaction">
									<li class="action-icons">
										<!--<span class="icon-box addiconclass" id="conversation"><i  class="material-icons" title="Conversation">question_answer</i><span class="actiontitle">Conversation</span></span>
										<span class="icon-box editiconclass" id="autofollowrule"><i  class="material-icons" title="Auto Follow Rule">spellcheck
</i></span>
										<span class="icon-box deleteiconclass"  id="followedbymeid" ><i class="material-icons" title="Followed By Me">verified_user</i></span>-->
									</li>
								</ul>';
							}
						?>
					</div>
					</div>
				</div>
				<!-- Add Form -->
				<div id="reportsformdiv" class="">
				<?php
					$this->load->view('conversationform');
					$this->load->view('Base/basedeleteform');
				?>
				</div>						
			</div>
		</div>
	</div>
	<?php
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$this->load->view('Base/overlaymobile');
		} else {
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
	?>
	<script src="<?php echo base_url();?>js/plugins/jqgrid/jquery.jqGrid.src.min.js"></script>
	<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js Files -->
	<script src="<?php echo base_url();?>js/plugins/uploadfile/jquery.uploadfile.min.js"></script>
	<script src="<?php echo base_url();?>js/Conversation/conversation.js" type="text/javascript"></script> 
	<!--For Tablet and Mobile view Dropdown Script-->
	<script>
		$(document).ready(function(){
			$("#tabgropdropdown").change(function(){
				var tabgpid = $("#tabgropdropdown").val(); 
				$('.sidebaricons[data-subform="'+tabgpid+'"]').trigger('click');
			});			
		});
	</script>
</body>
</html>
