<?php 
$CI =& get_instance();
$appdateformat = $CI->Basefunctions->appdateformatfetch(); ?>
<div class="large-12 columns addformunderheader">&nbsp;</div>
<div class="large-12 columns addformcontainer actionbarformcontainer" style="overflow-y: auto;position: relative; background-color:#FFFFFF;padding-left: 0.78em;padding-right: 0.4em;">
	<div class="large-3 medium-3 small-12 columns mblnopadding">
		<div class="large-12 columns cleardataform paddingzero borderstyle" style="background: #eceff1 !important;">
			<div class="large-12 columns headerformcaptionstyle">Filter</div>
			<div class="large-12 columns conversationbox">
				<div class="large-12 columns">
					<select id="conversationfilter" class="chzn-select" data-placeholder="Now"  tabindex="" name="conversationfilter" title="">
						<option value="">Select</option>
						<option value="all">All</option>
						<option value="my">My</option>
						<option value="today">Today</option>
						<option value="yesterday">Yesterday</option>
						<option value="thisweek">This Week</option>
						<option value="thismonth">This Month</option>
				    </select>
			    </div>
			</div>
		</div>
	</div>
	<div class="large-9 medium-9  small-12 columns mblnopadding" id="conversationarea">	
		<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
			<div class="large-12 columns headerformcaptionstyle">Conversation</div>		
			<div class="large-12 columns  c_scroll conversationbox conversationboxmbl" id="conversation" style="overflow-y:hidden; background: #eceff1;">
				<span class="large-12 columns mblnopadding" id="textenterdiv">
					<div class="large-12 columns paddingzero">
						<div class="large-12 columns hidedisplay titleinputspan">&nbsp;</div> 
						<div class="large-12 columns paddingzero">
							<div class="border-top-for-chat"></div>
							<div style=" position: absolute;bottom: -452px;background: #e0e0e0;border-radius: 50px;box-shadow: 0 5px 10px 0 rgba(0, 0, 0, 0.16), 0 0px 0px 0 rgba(0, 0, 0, 0.12);" class="large-12 columns paddingzero">
								<div class="large-12 columns conversbar paddingzero">
									<span id="convmulitplefileuploader"></span>
									<span style="" class="uploadattachments attachbtnstyle" title="Attachment"><i class="material-icons">attachment</i></span>
									<textarea class="mention convmessagetoenterstyle" rows="3" placeholder="Enter Your Message Here..." id="homedashboardconvid" name="homedashboardconvid"></textarea>
									<input type="hidden" id="unique_rowid" name="unique_rowid" value=""/>
									<span class="sendbtnstyle" id="postsubmit" style=""><i class="material-icons">send</i></span>
									<span class="sendbtnstyle hidedisplay" id="editpostsubmit" style=""><i class="material-icons">send</i></span>
								</div>
							</div>
						</div>	
						<div class="large-12 columns mblnopadding" id="conversationdatablog" style="position: relative;z-index: 0;overflow-x: hidden;overflow-y: scroll;max-height: 361px;top: 0px;">
						</div>
					</div>
				</span>
			</div>
		</div>
		<div class="large-12 columns">&nbsp;</div>
	</div>
	<div class="large-12 medium-12 columns hidedisplay" style="padding-bottom:1rem" id="followedbrmearea">	
		<div class="large-12 columns viewgridcolorstyle" style="padding-left:0;padding-right:0;">	
			<div class="large-12 columns headerformcaptionstyleforprofile" style="height:auto;padding: 0.1rem 0 0;">	
				<span class="large-6 medium-6 small-12 columns end" style="line-height:2">
					<span class="large-4 medium-4 small-4 columns" style="line-height:1.5">
						Module :
					</span>
					<span class="large-6 medium-8 small-8 end columns">
						<select class="chzn-select" id="moduleid" name="moduleid" data-placeholder="Select" data-prompt-position="topLeft:14,36">
							<option value="">Select</option>
							<?php foreach($moduleid as $key):?>
								<option value="<?php echo $key->moduleid; ?>" data-modname="<?php echo $key->modulename;?>"><?php echo $key->modulename; ?> </option>
							<?php endforeach; ?>
						</select>										
					</span>
				</span>
				<span class="large-6 medium-6 small-12 columns innergridicon righttext small-only-text-center" style="line-height:1.8">
					<span id="autofollowaddicon" class="addiconclass " title="Add"><i class="material-icons">add</i> </span>
					<span id="autofollowediticon" class="editiconclass " title="Edit"><i class="material-icons">edit</i>  </span>
					<span id="autofoloweddeleteicon" class="deleteiconclass " title="Delete"><i class="material-icons">delete</i>  </span>
					<span id="autofolowedreloadicon" class="reloadiconclass " title="Reload"> <i class="material-icons">refresh</i> </span>
				</span>
			</div>
			<?php
				$device = $this->Basefunctions->deviceinfo();
				if($device=='phone') {
					echo '<div class="large-12 columns paddingzero forgetinggridname" id="autofollowgridwidth"><div class="desktop row-content inner-gridcontent" id="autofollowgrid" style="height:420px;top:0px;">
						<!-- Table header content , data rows & pagination for mobile-->
					</div>
					<footer class="inner-gridfooter footercontainer" id="autofollowgridfooter">
						<!-- Footer & Pagination content -->
					</footer></div>';
				} else {
					echo '<div class="large-12 columns forgetinggridname" id="autofollowgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="autofollowgrid" style="max-width:2000px; height:420px;top:0px;">
					<!-- Table content[Header & Record Rows] for desktop-->
					</div>
					<div class="inner-gridfooter footer-content footercontainer" id="autofollowgridfooter">
						<!-- Footer & Pagination content -->
					</div></div>';
				}
			?>
		</div>
		<div class="large-12 columns">&nbsp;</div>
		<div class="large-12 columns">&nbsp;</div>
	</div>
	<div class="large-12 medium-12 columns paddingbtm hidedisplay" id="autofollowaddform">	
		<div class="large-12 columns headerformcaptionstyle" style="height:auto; line-height:1.8rem;padding: 0.2rem 0 0rem;">
			<span class="large-6 medium-6 small-12 columns end lefttext"> Auto Rule Form</span>
			<span  class="large-6 medium-6 small-12 columns innergridicon righttext small-only-text-center">
				<span id="autoruleadd" class="" title="Save"><i class="material-icons">save</i></span>
				<span id="autoruleupdate" class="" style="display:none;"title="Update"><i class="material-icons">save</i></span>
				<span id="autoruleclose" class="" title="Close"><i class="material-icons">delete</i></span>
			</span>
		</div>
		<div class="large-12 columns" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
			<form method="POST" name="autorulecreateform" class="viewcreateformclearform" action ="" id="autorulecreateform">
				<span id="viewformconditionvalidation" class="validationEngineContainer">
					<div class="large-4 columns paddingbtm">
						<span id="autorulebasicvalidation" class="validationEngineContainer">
						<span id="autoruleeditbasicvalidation" class="validationEngineContainer">
							<div class="large-12 columns" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
								<div class="input-field large-12 columns">
									<input id="autorulename" class="validate[required]" type="text" data-prompt-position="bottomLeft" tabindex="101" name="autorulename">
									<label for="autorulename"> Rule Name<span class="mandatoryfildclass">*</span> </label>
								</div>
								<div class="static-field large-12 columns" id="">
									<label> Module<span class="mandatoryfildclass">*</span> </label>
									<select class="chzn-select validate[required] " id="autorulemoduleid" name="autorulemoduleid" data-prompt-position="topLeft:14,36" tabindex="100" data-placeholder="Select">
										<option value="">Select</option>
										<?php foreach($moduleid as $key):?>
											<option value="<?php echo $key->moduleid; ?>" data-modname="<?php echo $key->modulename;?>"><?php echo $key->modulename; ?> </option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="input-field large-6 columns end">
									<input id="autorulepublic" class="filled-in checkboxcls" type="checkbox" tabindex="" name="autorulepublic" value="No">
									<label for="autorulepublic">Public</label>
								</div>
								<div class="large-6 columns end">
									<input id="autoruleid" type="hidden" tabindex="" name="autoruleid" value="">
								</div>
							</div>
						</span>
						</span>
					</div>	
					<div class="large-4 columns paddingbtm">
						<div class="large-12 columns" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
							<div class="static-field large-12 columns viewcondclear" id="">
								<label> Field Name<span class="mandatoryfildclass">*</span> </label>
								<select class="chzn-select validate[required] " id="autocolumnname" name="autocolumnname" data-prompt-position="topLeft:14,36" tabindex="100" data-placeholder="Select">
									<option value=""></option>
								</select>
							</div>
							<div class="static-field large-12 columns viewcondclear" id="">
								<label> Condition<span class="mandatoryfildclass">*</span> </label>
								<select class="chzn-select validate[required]" id="autocondition" name="autocondition" data-prompt-position="topLeft:14,36" tabindex="100" data-placeholder="Select">
									<option value=""></option>
									</select>
							</div>
						</div>
					</div>
					<div class="large-4 columns paddingbtm">
						<div class="large-12 columns" style="padding-left: 0 !important; padding-right: 0 !important;background: #f5f5f5">
							<div class="input-field large-12 columns viewcondclear" id="autocondvaluedivhid">
								<input type="text" class="validate[required,maxSize[100]]" id="autocondvalue" name="autocondvalue"  tabindex="12" >
								<label for="autocondvalue"> Value <span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="static-field large-12 columns viewcondclear" id="autoddcondvaluedivhid">
								<label> Value<span class="mandatoryfildclass">*</span></label>
								<select data-placeholder="Select"  data-prompt-position="topLeft:14,36" class="validate[required] chzn-select chosenwidth valdropdownchange" tabindex="13" name="autoddcondvalue" id="autoddcondvalue">
									<option></option>
								</select>  
							</div>
							<div class="input-field large-12 columns viewcondclear" id="autodatecondvaluedivhid">
								<input type="text" class="validate[required]" data-dateformater="<?php echo $appdateformat; ?>" id="autodatecondvalue" name="autodatecondvalue"  tabindex="12" >
								<label for="autodatecondvalue"> Value <span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="large-12 columns viewcondclear">
								<input type="hidden" class="" id="finalautocondvalue" name="finalautocondvalue"  tabindex="" >
							</div>
							<div class="large-12 columns viewcondclear">
								<input type="hidden" class="" id="jointable" name="jointable"  tabindex="" >
							</div>
							<div class="large-12 columns viewcondclear">
								<input type="hidden" class="" id="indexname" name="indexname"  tabindex="" >
							</div>
							<div class="large-12 columns viewcondclear">
								<input type="hidden" class="" id="fieldid" name="fieldid"  tabindex="" >
							</div>
							<div class="large-12 columns viewcondclear">
								<input type="hidden" class="" id="comumnname" name="comumnname"  tabindex="" >
							</div>
							<div class="large-12 columns viewcondclear">
								<input type="hidden" class="" id="modulename" name="modulename"  tabindex="" >
							</div>
							<div class="large-12 columns viewcondclear">
								<input type="hidden" class="" id="autofollowrulecondtionid" name="autofollowrulecondtionid"  value ='0' tabindex="" >
							</div>
							<div class="large-12 columns viewcondclear">
								<input type="hidden" class="" id="conditiondeleteid" name="conditiondeleteid" value="" tabindex="" >
							</div>
							<div class="static-field large-12 columns viewcondclear" id="autoandordivhid">
								<label> And / OR<span class="mandatoryfildclass"></span> </label>
								<select class="chzn-select" id="autoandor" name="autoandor" data-prompt-position="topLeft:14,36" tabindex="100" data-placeholder="Select">
									<option value=""></option>
									<option value="AND">AND</option>
									<option value="OR">OR</option>
								</select>
							</div>
						</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns submitkeyboard" style="text-align: right">
							<input id="autoaddcondsubbtn" class="btn addkeyboard" type="button" value="Submit" name="autoaddcondsubbtn" tabindex="104">
						</div>
					</div>
				</span>
			</form>
			<div class="large-12 columns" style="background-color:#cecece;">&nbsp;</div>
			<div class="large-12 columns" style="background-color:#cecece;">&nbsp;</div>
			<div class="large-12 columns  paddingzero">
				<div class="large-12 columns headerformcaptionstyle" style="padding: 0.1rem 0 0;">
					<div class="large-6 medium-6 small-6 columns" style="text-align:left;display:inline-block;line-height: 1.5rem;white-space: nowrap;overflow:hidden !important;text-overflow: ellipsis;">
						Conditions to Apply
					</div>
					<div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height: 1.8;">
						<span id="conddeleteicon" title="Delete" class="icon icon-bin"> </span>
					</div>
				</div>
				<?php
					$device = $this->Basefunctions->deviceinfo();
					if($device=='phone') {
						echo '<div class="large-12 columns paddingzero forgetinggridname" id="autofollowconditiongridwidth"><div class="desktop row-content inner-gridcontent" id="autofollowconditiongrid" style="height:420px;top:0px;">
							<!-- Table header content , data rows & pagination for mobile-->
						</div>
						<footer class="inner-gridfooter footercontainer" id="autofollowconditiongridfooter">
							<!-- Footer & Pagination content -->
						</footer></div>';
					} else {
						echo '<div class="large-12 columns forgetinggridname" id="autofollowconditiongridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="autofollowconditiongrid" style="max-width:2000px; height:420px;top:0px;">
						<!-- Table content[Header & Record Rows] for desktop-->
						</div>
						<div class="inner-gridfooter footer-content footercontainer" id="autofollowconditiongridfooter">
							<!-- Footer & Pagination content -->
						</div></div>';
					}
				?>
			</div>
		</div>	
		<div class="large-12 columns">&nbsp;</div>
		<div class="large-12 columns">&nbsp;</div>
	</div>
	<div class="large-12 medium-12 columns paddingbtm hidedisplay" id="followedbyme">	
		<div class="large-12 columns viewgridcolorstyle" style="padding-left:0;padding-right:0;">	
			<div class="large-12 columns headerformcaptionstyleforprofile" style="height:auto;padding: 0.5rem 0 0;">	
				<span class="large-4 medium-3 small-12 columns" style="line-height:2">
					<span class="large-3 medium-4 small-4 columns  paddingzero" style="line-height:1.5"> Module:</span>
					<span class="large-9 medium-8 small-8 end columns">
						<select class="chzn-select" id="followmoduleid" name="followmoduleid" data-placeholder="Select" data-prompt-position="topLeft:14,36">
							<option value="">Select</option>
							<?php foreach($moduleid as $key):?>
								<option value="<?php echo $key->moduleid; ?>" data-modname="<?php echo $key->modulename;?>"><?php echo $key->modulename; ?> </option>
							<?php endforeach; ?>
						</select>										
					</span>
				</span>
				<span class="large-3 medium-3 small-12 columns end" style="line-height:2">
					<span class="large-3 medium-4 small-4 columns paddingzero" style="line-height:1.5">
						View:
					</span>
					<span class="large-9 medium-8 small-8 end columns">
						<select class="chzn-select" id="followviewid" name="followviewid" data-placeholder="Select" data-prompt-position="topLeft:14,36">
							<option value=""></option>
						</select>										
					</span>
				</span>
				<span class="large-3 medium-3 small-12 columns end" style="line-height:2">
					<span class="large-3 medium-4 small-4 columns paddingzero" style="line-height:1.5">
						Rule:
					</span>
					<span class="large-9 medium-8 small-8 end columns">
						<select class="chzn-select" id="followruleid" name="followruleid" data-placeholder="Select" data-prompt-position="topLeft:14,36">
							<option value=""></option>
						</select>										
					</span>
				</span>
				<span class="large-2 medium-3 small-12 columns innergridicon righttext small-only-text-center" style="line-height:1.8">
					<span id="unfollowrecordid" class="addiconclass " title="Unfollow"><i class="material-icons">block</i> </span>
				</span>
			</div>
			<?php
				$device = $this->Basefunctions->deviceinfo();
				if($device=='phone') {
					echo '<div class="large-12 columns paddingzero forgetinggridname" id="followedbymegridwidth"><div class="desktop row-content inner-gridcontent" id="followedbymegrid" style="height:420px;top:0px;">
						<!-- Table header content , data rows & pagination for mobile-->
					</div>
					<footer class="inner-gridfooter footercontainer" id="followedbymegridfooter">
						<!-- Footer & Pagination content -->
					</footer></div>';
				} else {
					echo '<div class="large-12 columns forgetinggridname" id="followedbymegridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="followedbymegrid" style="max-width:2000px; height:420px;top:0px;">
					<!-- Table content[Header & Record Rows] for desktop-->
					</div>
					<div class="inner-gridfooter footer-content footercontainer" id="followedbymegridfooter">
						<!-- Footer & Pagination content -->
					</div></div>';
				}
			?>
		</div>
		<div class="large-12 columns">&nbsp;</div>
		<div class="large-12 columns">&nbsp;</div>
	</div>
	
	<div class="large-12 medium-12 columns paddingbtm hidedisplay" id="unnoticedrecords">	
		<div class="large-12 columns viewgridcolorstyle" style="padding-left:0;padding-right:0;">	
			<div class="large-12 columns headerformcaptionstyleforprofile" style="height:auto;padding: 0.5rem 0 0;">	
				<span class="large-4 medium-3 small-12 columns" style="line-height:2">
					<span class="large-3 medium-4 small-4 columns  paddingzero" style="line-height:1.5">
						Module:</span>
					<span class="large-9 medium-8 small-8 end columns">
						<select class="chzn-select" id="unnoticedmoduleid" name="unnoticedmoduleid" data-placeholder="Select" data-prompt-position="topLeft:14,36">
							<option value="">Select</option>
							<?php foreach($moduleid as $key):?>
								<option value="<?php echo $key->moduleid; ?>" data-modname="<?php echo $key->modulename;?>"><?php echo $key->modulename; ?> </option>
							<?php endforeach; ?>
						</select>										
					</span>
				</span>
				<span class="large-3 medium-3 small-12 columns innergridicon righttext small-only-text-center" style="line-height:1.8">
					<p>
					  <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
					</p>
				</span>
				<span class="large-5 medium-5 small-12 columns innergridicon lefttext small-only-text-center" style="line-height:1.8 ; margin-top:12px">
				<div id="slider"></div>
				</span>
			</div>
			<?php
				$device = $this->Basefunctions->deviceinfo();
				if($device=='phone') {
					echo '<div class="large-12 columns paddingzero forgetinggridname" id="unnoticedgridwidth"><div class="desktop row-content inner-gridcontent" id="unnoticedgrid" style="height:420px;top:0px;">
						<!-- Table header content , data rows & pagination for mobile-->
					</div>
					<footer class="inner-gridfooter footercontainer" id="unnoticedgridfooter">
						<!-- Footer & Pagination content -->
					</footer></div>';
				} else {
					echo '<div class="large-12 columns forgetinggridname" id="unnoticedgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="unnoticedgrid" style="max-width:2000px; height:420px;top:0px;">
					<!-- Table content[Header & Record Rows] for desktop-->
					</div>
					<div class="inner-gridfooter footer-content footercontainer" id="unnoticedgridfooter">
						<!-- Footer & Pagination content -->
					</div></div>';
				}
			?>
		</div>
		<div class="large-12 columns">&nbsp;</div>
		<div class="large-12 columns">&nbsp;</div>
	</div>
</div>