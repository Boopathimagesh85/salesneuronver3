<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Conversationmodel extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
	//view report list
	public function autoruleconceptlist($tablename,$sortcol,$sortord,$pagenum,$rowscount,$moduleid) {
		$dataset='autofollowrule.autofollowruleid,autofollowrule.autofollowrulename,module.modulename,autofollowrule.public,autofollowrule.createdate';
		$join=' LEFT OUTER JOIN module ON module.moduleid=autofollowrule.moduleid';
		$status = $tablename.'.status IN (1,2)';
		$where = ' AND 1=1';
		if($moduleid != "") {
			$where = ' AND autofollowrule.moduleid IN ('.$moduleid.')';
		}
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		//query
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$status.$where.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($data,$page,'');
		return $finalresult;
	}
	//module drop down fetch
	public function filedsdropdownloadmodel() {
		$i=0;
		$ids = $_GET['ids'];
		$autonum = $_GET['autonum'];
		$this->db->select('viewcreationcolumnid,viewcreationcolmodeljointable,viewcreationcolmodelindexname,viewcreationcolumnname,uitypeid,moduletabsectionid,viewcreationparenttable');
		$this->db->from('viewcreationcolumns');
		$this->db->where('viewcreationcolumns.viewcreationmoduleid',$ids);
		if($autonum != 'yes') {
			$this->db->where_not_in('viewcreationcolumns.uitypeid',array(14));
		}
		$this->db->where_in('viewcreationcolumns.viewcreationcolumnviewtype',array(1));
		$this->db->where('status',1);
		$result= $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('jointable'=>$row->viewcreationcolmodeljointable,'indexname'=>$row->viewcreationcolmodelindexname,'datasid'=>$row->viewcreationcolumnid,'dataname'=>$row->viewcreationcolumnname,'uitype'=>$row->uitypeid,'modtabsec'=>$row->moduletabsectionid,'parenttable'=>$row->viewcreationparenttable);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//field name based drop down value get
	public function fieldviewnamebespicklistdddvaluemodel() {
		$i=0;
		$moduleid = 'moduleid';
		$mid = $_GET['moduleid'];
		$fieldname = $_GET['fieldname'];
		$table = substr($fieldname, 0, -4);
		$fieldid = $table.'id';
		$this->db->select($fieldid.','.$fieldname);
		$this->db->from($table);
		$this->db->where("FIND_IN_SET($mid,$table."."$moduleid) >", 0);
		$this->db->where('status',1);
		$reult =$this->db->get();
		if($reult->num_rows() > 0){
			foreach($reult->result() as $row){
				$data[$i] = array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//field name based drop down value get
	public function viewfieldnamebesdddvaluemodel() {
		$i=0;
		$moduleid = $_GET['moduleid'];
		$fieldname = $_GET['fieldname'];
		if($moduleid == 216 || $moduleid == 217 || $moduleid == 225 || $moduleid == 226){
			$table = substr($fieldname, 0, -6);
			$fieldid = $table.'id';
		}else {
			$table = substr($fieldname, 0, -4);
			$fieldid = $table.'id';
		}
		$this->db->select($fieldid.','.$fieldname);
		$this->db->from($table);
		$this->db->where('status',1);
		$reult =$this->db->get();
		if($reult->num_rows() > 0){
			foreach($reult->result() as $row){
				$data[$i] = array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//auto rule insertion
	public function autoruleinsertionmodel() {
		$rulename = $_POST['rulename'];
		$moduleid = $_POST['moduleid'];
		$public = $_POST['public'];
		$userid = $this->Basefunctions->userid;
		$cdate = date($this->Basefunctions->datef);
		$rulearray = array(
				'autofollowrulename'=>$rulename,
				'moduleid'=>$moduleid,
				'public'=>$public,
				'createdate'=>$cdate,
				'lastupdatedate'=>$cdate,
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
				'status'=>1
			);
		$this->db->insert('autofollowrule',$rulearray);
		$autoruleid = $this->db->insert_id();
		$girddata = $_POST['criteriagriddata'];
		$cricount = $_POST['criteriacnt'];
		$girddatainfo = json_decode($girddata,true);
		for($i=0;$i<=($cricount-1);$i++) {
			$conddatas = array(
				'autofollowruleid'=>$autoruleid,
				'viewcreationcolumnid'=>$girddatainfo[$i]['fieldid'],
				'viewcreationconditionname'=>$girddatainfo[$i]['autocondition'],
				'viewcreationconditionvalue'=>$girddatainfo[$i]['finalautocondvalue'],
				'viewcreationandorvalue'=>$girddatainfo[$i]['autoandor'],
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
				'createdate'=>$cdate,
				'lastupdatedate'=>$cdate,
				'status'=>1				
			);
			$this->db->insert('autofollowrulecondtion',$conddatas);
		}
		echo "TRUE";
	}
	//auto follow rule delete
	public function autoruledeletemodel() {
		$id = $_POST['datarowid'];
		$userid = $this->Basefunctions->userid;
		$cdate = date($this->Basefunctions->datef);
		$status = array('status'=>0	, 'lastupdateuserid'=>$userid , 'lastupdatedate'=>$cdate);
		$this->db->where('autofollowrule.autofollowruleid',$id);
		$this->db->update('autofollowrule',$status);
		echo "TRUE";
	}
	//auto follow rule edit
	public function autoruleeditdatafetchmodel() {
		$id = $_POST['datarowid'];
		$this->db->select('autofollowrulename,moduleid,public');
		$this->db->from('autofollowrule');
		$this->db->where('autofollowrule.autofollowruleid',$id);
		$this->db->where('autofollowrule.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('rulename'=>$row->autofollowrulename,'module'=>$row->moduleid,'publicc'=>$row->public);
			}
		} else {
			$data = array('');
		}
		echo json_encode($data);
	}
	public function autofollowruledconditionfetchgrid() {
		$id=trim($_GET['primarydataid']);
		$this->db->select('autofollowrule.autofollowrulename,autofollowrule.moduleid,module.modulename,autofollowrule.public,autofollowrulecondtionid,autofollowrulecondtionid,viewcreationcolumns.viewcreationcolumnid,viewcreationcolumns.viewcreationcolumnname,viewcreationconditionname,viewcreationconditionvalue,viewcreationandorvalue,viewcreationcolumns.viewcreationcolmodelindexname,viewcreationcolumns.viewcreationcolmodeljointable');
		$this->db->from('autofollowrule');
		$this->db->join('module','module.moduleid=autofollowrule.moduleid');
		$this->db->join('autofollowrulecondtion','autofollowrulecondtion.autofollowruleid=autofollowrule.autofollowruleid');
		$this->db->join('viewcreationcolumns','viewcreationcolumns.viewcreationcolumnid=autofollowrulecondtion.viewcreationcolumnid');
		$this->db->where('autofollowrulecondtion.autofollowruleid',$id);
		$this->db->where('autofollowrule.status',$this->Basefunctions->activestatus);
		$this->db->where('autofollowrulecondtion.status',$this->Basefunctions->activestatus);
		$data=$this->db->get();
		if($data->num_rows() > 0) {
			$j=0;
			foreach($data->result() as $value) {
				$productdetail->rows[$j]['id'] = $value->autofollowrulecondtionid;
				$productdetail->rows[$j]['cell']=array(
						$value->autofollowrulecondtionid,
						$value->autofollowrulename,
						$value->public,
						$value->moduleid,
						$value->modulename,
						$value->viewcreationandorvalue,
						$value->viewcreationcolumnid,
						$value->viewcreationcolumnname,
						$value->viewcreationconditionname,
						$value->viewcreationconditionvalue,
						$value->viewcreationconditionvalue,
						$value->viewcreationcolmodeljointable,
						$value->viewcreationcolmodelindexname
				);
				$j++;
			}
		} else {
			$productdetail = array('fail'=>'FAILED');
		}
		echo  json_encode($productdetail);
	}
	//auto follow rule update
	public function autoruleupdatemodel() {
		$ruleid = $_POST['ruleid'];
		$rulename = $_POST['rulename'];
		$moduleid = $_POST['moduleid'];
		$public = $_POST['public'];
		$delecondids = $_POST['conrowcolids'];
		$deletecondid = explode(',',$delecondids);
		$userid = $this->Basefunctions->userid;
		$cdate = date($this->Basefunctions->datef);
		$rulearray = array(
				'autofollowrulename'=>$rulename,
				'moduleid'=>$moduleid,
				'public'=>$public,
				'lastupdatedate'=>$cdate,
				'lastupdateuserid'=>$userid,
				'status'=>1
			);
		$this->db->where('autofollowrule.autofollowruleid',$ruleid);
		$this->db->update('autofollowrule',$rulearray);
		$girddata = $_POST['criteriagriddata'];
		$cricount = $_POST['criteriacnt'];
		$girddatainfo = json_decode($girddata,true);
		for($i=0;$i<=($cricount-1);$i++) {
			if($girddatainfo[$i]['autofollowrulecondtionid'] == 0) {
				$conddatas = array(
					'autofollowruleid'=>$ruleid,
					'viewcreationcolumnid'=>$girddatainfo[$i]['fieldid'],
					'viewcreationconditionname'=>$girddatainfo[$i]['autocondition'],
					'viewcreationconditionvalue'=>$girddatainfo[$i]['finalautocondvalue'],
					'viewcreationandorvalue'=>$girddatainfo[$i]['autoandor'],
					'createuserid'=>$userid,
					'lastupdateuserid'=>$userid,
					'createdate'=>$cdate,
					'lastupdatedate'=>$cdate,
					'status'=>1				
				);
				$this->db->insert('autofollowrulecondtion',$conddatas);
			}
		}
		//delete condition
		foreach($deletecondid as $conrowid) {
			$updatedcondata=array('lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$userid,'status'=>0);
			$this->db->where('autofollowrulecondtion.autofollowrulecondtionid',$conrowid);
			$this->db->update('autofollowrulecondtion',$updatedcondata);
		}
		echo "TRUE";
	}
	//view fetch based on module
	public function viewfetchbasedonmodulemodel() {
		$moduleid = $_GET['moduleid'];
		$i=0;
		$this->db->select('viewcreationname,viewcreationid,viewcreationcolumnid');
		$this->db->from('viewcreation');
		$this->db->where('viewcreation.viewcreationmoduleid',$moduleid);
		$this->db->where('viewcreation.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->viewcreationid,'dataname'=>$row->viewcreationname,'columnid'=>$row->viewcreationcolumnid);
				$i++;
			}
			echo json_encode($data);
		}  else {
			echo json_encode(array('fail'=>'FAILED'));	
		}
	}
	//auto rule fetch based on module
	public function autorulefetchbasedonmodulemodel() {
		$moduleid = $_GET['moduleid'];
		$i=0;
		$this->db->select('autofollowrulename,autofollowruleid');
		$this->db->from('autofollowrule');
		$this->db->where('autofollowrule.moduleid',$moduleid);
		$this->db->where('autofollowrule.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->autofollowruleid,'dataname'=>$row->autofollowrulename);
				$i++;
			}
			echo json_encode($data);
		}  else {
			echo json_encode(array('fail'=>'FAILED'));	
		}
	}
	//parent table name fetch
	public function parenttablegetmodel(){
		$mid = $_GET['moduleid'];
		$this->db->select('parenttable');
		$this->db->from('modulefield');
		$this->db->where('modulefield.moduletabid',$mid);
		$result = $this->db->get();
		if($result->num_rows() >0){
			foreach($result->result() as $row){
				$data = $row->parenttable;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//unnoticed record list view
	public function viewdynamicdatainfofetch($creationid,$primaryid,$viewcolmoduleids,$sortcol,$sortord,$pagenum,$rowscount,$ruleid) {
		$whereString="";
		$groupid="";
		$extracolinfo = array();
		$multiarray = array();
		$j=0;
		$rulecondition = $this->ruleconditiondatafetch($ruleid);
		$extracolinfo = $rulecondition[0];
		$multiarray  = $rulecondition[1];
		//grid column title information fetch
		if($extracolinfo == "") {
			$colinfo = $this->Basefunctions->gridinformationfetchmodel($creationid);
		} else {
			$colinfo = $this->extragridinformationfetchmodel($creationid,$extracolinfo);
		}
		//header colids
		$headcolids = $this->Basefunctions->mobiledatarowheaderidfetch($creationid);
		//view moduleid
		$viewmodid = $this->Basefunctions->mobiledataviewmoduleidfetch($creationid);
		 //main table
		$maintable = $_GET['maintabinfo'];
		$selvalue = $maintable.'.'.$primaryid;
		$in = 0;
		//generate joins
		$counttable=0;
		if(count($colinfo)>0) {
			$counttable=count($colinfo['coltablename']);
		}
		$jtab = array($maintable);
		$ptab = array($maintable);
		$joinq = "";
		//attribute where condition
		$attrcond = "";
		$attrcondarr = array();
		$x = 0;
		$con = "";
		for($k=0;$k<$counttable;$k++) {
			if($x != 0) { $con = "AND"; }
			//parent join check
			$ptabjoin = "";
			$ptabjoin = $colinfo['colmodelparenttable'][$k];
			if(in_array($ptabjoin,$ptab)) {
				if(!in_array($colinfo['coltablename'][$k],$jtab) && $colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
					array_push($jtab,trim($colinfo['coltablename'][$k]));
					//employee
					$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid=1';
					//group
					$joinq .= ' LEFT OUTER JOIN employeegroup ON FIND_IN_SET(employeegroup.employeegroupid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid=2';
					//user role and sub ordinate
					$joinq .= ' LEFT OUTER JOIN userrole ON FIND_IN_SET(userrole.userroleid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND ('.$maintable.'.employeetypeid=3 OR '.$maintable.'.employeetypeid=4)';
				} else {
					if($colinfo['colmodeldatatype'][$k] == 1) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
							}
						}
					} else if($colinfo['colmodeldatatype'][$k] == 2) {
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else if($colinfo['colmodeldatatype'][$k] == 3) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0';
							}
						}
					}
				}
			} else {
				if($colinfo['colmodeldatatype'][$k] == 1) {
					$result = $this->Basefunctions->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
							}
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.','.$maintable.'.'.$primaryid.') > 0';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 1) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0';
									}
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 2) {
					$joinresult = $this->Basefunctions->parentjointableinfo($ptabjoin,$viewcolmoduleids);
					if($joinresult->num_rows() >0) {
						foreach($joinresult->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
							//condition name
							$condcolname = $show->viewcreationcolmodelcondname;
							//condition value
							$condcolvalue = $show->viewcreationcolmodelcondvalue;
						}
						//restrict repeated parent join
						if(in_array($destab,$ptab)) {
							if($destab != $soucrcetab) {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0';
								}
							} else {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0';
								}
							}
						}
						//parent table condition
						if($condcolname != "" && $condcolvalue != "") {
							$attrcond .= " ".$con." ".$srcjointab.'.'.$condcolname ."=". $condcolvalue;
							array_push($attrcondarr,$srcjointab.'.'.$joincolname);
							$x = 1;
						}
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] = $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.'='.$maintable.'.'.$primaryid.') > 0';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 2) {
								//attribute join
								if(!in_array($colinfo['coltablename'][$k],$jtab)) {
									array_push($jtab,trim($colinfo['coltablename'][$k]));
									$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0';
								}
								//condition
								if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
									$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
									array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
									$x = 1;
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 3) {
					$result = $this->Basefunctions->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0';
							}
						}
					}  else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.','.$maintable.'.'.$primaryid.') > 0';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 3) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0';
									}
								}
							}
						}
					}
				}
			}
			$dduitypeids = array('17','18','19','23','25','26','27','28','29');
			//fields fetch
			if($colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->Basefunctions->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].', GROUP_CONCAT(DISTINCT userrole.userrolename) AS rolename, GROUP_CONCAT(DISTINCT employeegroup.employeegroupname) AS empgrpname';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k].',userrole.userrolename AS rolename,employeegroup.employeegroupname AS empgrpname';
				}
			} else if( in_array($colinfo['colmodeluitype'][$k],$dduitypeids) ) {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->Basefunctions->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].'';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
				}
			} else {
				$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
			}
		}
		//attr condition generate
		for($xi=0; $xi < ( count($attrcondarr)-1 ); $xi++ ) {
			$attrcond .= " AND ".$attrcondarr[0].'='.$attrcondarr[$xi+1];
		}
		if( $attrcond == "" ) { $attrcond = "1=1";}
		//custom condition generate
		$cuscondition = " AND 1=1";
		if( isset($_GET['cuscondfieldsname']) ) {
			$fieldsname = explode(',',$_GET['cuscondfieldsname']);
			$condvalues = explode(',',$_GET['cuscondvalues']);
			$condtables = explode(',',$_GET['cuscondtablenames']);
			$condjoinid = explode(',',$_GET['cusjointableid']);
			$m=0;
			foreach($fieldsname AS $values) {
				if($condvalues[$m] != "") {
					if(in_array($condtables[$m],$ptab)) {
						$tabname = explode('.',$values);
						//restrict repeated join
						if(!in_array($condtables[$m],$jtab)) {
							array_push($jtab,$condtables[$m]);
							$joinq=$joinq.' LEFT OUTER JOIN '.$condtables[$m].' ON '.$tabname[0].'.'.$condjoinid[$m].'='.$maintable.'.'.$condjoinid[$m];
						}
					}
					$cuscondition .= " AND ".$values."=".$condvalues[$m];
					$m++;
				}
			}
		}
		//generate condition
		$ewh = 'AND 1=1';
		$conditionvalarray = $this->Basefunctions->conditioninfofetch($creationid);
		$c = count($conditionvalarray);
		$d = count($multiarray);
		if($c>0) {
			$whereString=$this->whereclausegeneration($conditionvalarray);
		} else {
			$whereString=$ewh;
		}
		if($d>0) {
			$extrawhereString=$this->Basefunctions->whereclausegeneration($multiarray);
		} else {
			$extrawhereString=$ewh;
		}
		if($maintable == 'employee') {
			$status = $maintable.'.status NOT IN (3)';
		} else {
			$status = $maintable.'.status IN (1)';
		}
		/* column sorting */
		$order = '';
		if($sortcol!='' && $sortord!='') {
			$order = 'ORDER BY'.' '.$sortcol.' '.$sortord;
		} else {
			$order = 'ORDER BY'.' '.$maintable.'.'.$maintable.'id DESC';
		}
		//un follow records fetch
		$check = $this->unfollowedrecordidget($viewcolmoduleids);
		if($check['check'] != 'False'){
			$exp = $check['recordid'];
			$unfolloecondition = 'AND '.$maintable.'.'.$maintable.'id NOT IN ('.$exp.')';
		} else {
			$unfolloecondition=$ewh;
		}
		$actsts = $this->Basefunctions->activestatus;
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		$li= 'LIMIT '.$start.','.$rowscount;
		/* query statements */
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$selvalue.' from '.$maintable.' '.$joinq.' WHERE '.$attrcond.' '.$whereString.' '.$extrawhereString.' '.$cuscondition.' '.$unfolloecondition.' AND '.$status.' GROUP BY '.$maintable.'.'.$maintable.'id '.$order.' '.$li);
		return $data;
	}
	//where condition generation
    public function whereclausegeneration($conditionvalarray) {
		$whereString="";
		$m=0;
		$count = count($conditionvalarray);
		$braces = '';
		foreach ($conditionvalarray as $key) {
			if($m == 0) {
				$key[4] = "";
				$whereString .= " AND ".str_repeat("(",$count-1)."";
			} else {
				if($key[4] != 'AND' and $key[4] != 'OR') {
					$key[4] = 'AND';
				}
			}
			if($m >= 2){
				$braces = ')';
			}
			$whereString.=$this->conditiongenerate($key,$braces);
			$m++;
		}
		if($whereString !="" ) {
			$whereString .= " )";
		}
		return $whereString;
    }
	//condition generation
	public function conditiongenerate($key,$braces) {
		switch($key[2]) {
			case "Equalto":
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." = '".$key[3]."'"."'".$braces;
				break;
			case "NotEqual": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." != '".$key[3]."'"."'".$braces;
				break;
			case "Startwith":
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." LIKE '".$key[3].'%'."'"."'".$braces;
				break;
			case "Endwith": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." LIKE '".'%'.$key[3]."'"."'".$braces;
				break;
			case "Middle": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." LIKE '".'%'.$key[3].'%'."'"."'".$braces;
				break;
			case "IN": 
				$whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." IN '".$key[3]."'"."'".$braces;
				break;
			case "NOT IN": 
				return $whereString.=" ".$key[4]." ".$key[0].'.'.$key[1]." NOT IN '".$key[3]."'"."'".$braces;
				break;
			case "GreaterThan": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." > '" .$key[3]."'"."'".$braces;
				break;
			case "LessThan": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." < '" .$key[3]."'"."'".$braces;
				break;
			case "GreaterThanEqual": 
				return $whereString= " ".$key[4]." ".$key[0].'.'.$key[1]." >= '" .$key[3]."'"."'".$braces;
				break;
			case "LessThanEqual": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." <= '" .$key[3]."'"."'".$braces;
				break;
			default:
				return $whereString="";
				break;
		}
	}
	//fetch colname & colmodel information fetch model
    public function extragridinformationfetchmodel($creationid,$extracolinfo) {
		//fetch show col ids
		$this->db->select('viewcreation.viewcreationcolumnid,viewcreation.viewcreationid',false);
		$this->db->from('viewcreation');
		$this->db->where_in('viewcreation.viewcreationid',$creationid);
		$this->db->where('viewcreation.status',1);
		$result=$this->db->get()->result();
		foreach($result as $row) {
			$colids=$row->viewcreationcolumnid;
		}
		$viewcolids = explode(',',$colids);
		$ccolids = array_merge($viewcolids,$extracolinfo);
		//for fetch colname & colmodel fetch
		$i=0;
		$data = array();
		$this->db->select("viewcreationcolumns.viewcreationcolumnname,
		viewcreationcolumns.viewcreationcolmodelname,
		viewcreationcolumns.viewcreationcolmodelindexname,
		viewcreationcolumns.viewcreationcolmodeljointable,
		viewcreationcolumns.viewcreationcolumnid,
		viewcreationcolumns.viewcreationcolmodelaliasname,
		viewcreationcolumns.viewcreationcolmodeltable,
		viewcreationcolumns.viewcreationparenttable,
		viewcreationcolumns.viewcreationmoduleid,
		viewcreationcolumns.viewcreationjoincolmodelname,
		viewcreationcolumns.viewcreationcolumnviewtype,
		viewcreationcolumns.viewcreationtype,
		viewcreationcolumns.uitypeid,
		viewcreationcolumns.moduletabsectionid,
		viewcreationcolumns.viewcreationcolmodelcondname,
		viewcreationcolumns.viewcreationcolmodelcondvalue");
		$this->db->from('viewcreationcolumns');
		$this->db->where_in('viewcreationcolumns.viewcreationcolumnid',$ccolids);
		$this->db->where('viewcreationcolumns.status',1);
		$showfield = $this->db->get();
		if($showfield->num_rows() >0) {
			foreach($showfield->result() as $show) {
				$data['colname'][$i]=$show->viewcreationcolumnname;
				$data['colmodelname'][$i]=$show->viewcreationcolmodelname;
				$data['colmodelaliasname'][$i]=$show->viewcreationcolmodelaliasname;
				$data['coltablename'][$i]=$show->viewcreationcolmodeltable;
				$data['coljointablename'][$i]=$show->viewcreationcolmodeljointable;
				$data['coljoinmodelname'][$i]=$show->viewcreationjoincolmodelname;
				$data['colmodelindex'][$i]=$show->viewcreationcolmodeljointable .'.'.$show->viewcreationcolmodelindexname;
				$data['colmodelparenttable'][$i]=$show->viewcreationparenttable;
				$data['colmodelviewtype'][$i]=$show->viewcreationcolumnviewtype;
				$data['colmodeldatatype'][$i]=$show->viewcreationtype;
				$data['colmodelcondname'][$i]=$show->viewcreationcolmodelcondname;
				$data['colmodelcondvalue'][$i]=$show->viewcreationcolmodelcondvalue;
				$data['colmodelmoduleid'][$i]=$show->viewcreationmoduleid;
				$data['colmodeluitype'][$i]=$show->uitypeid;
				$data['colmodelsectid'][$i]=$show->moduletabsectionid;
				$data['colmodeltype'][$i]='text';
				$i++;
			}
		}
		return $data;
    }
	 //fetch colname & colmodel information fetch based viewcolumnids 
    public function viewcolumndatainformationfetch($viewcolids) {
		$i=0;
		$data = array();
		$this->db->select("viewcreationcolumns.viewcreationcolumnname,
		viewcreationcolumns.viewcreationcolmodelname,
		viewcreationcolumns.viewcreationcolmodelindexname,
		viewcreationcolumns.viewcreationcolmodeljointable,
		viewcreationcolumns.viewcreationcolumnid,
		viewcreationcolumns.viewcreationcolmodelaliasname,
		viewcreationcolumns.viewcreationcolmodeltable,
		viewcreationcolumns.viewcreationparenttable,
		viewcreationcolumns.viewcreationmoduleid,
		viewcreationcolumns.viewcreationjoincolmodelname,
		viewcreationcolumns.viewcreationcolumnviewtype,
		viewcreationcolumns.viewcreationtype,
		viewcreationcolumns.viewcreationcolmodelcondname,
		viewcreationcolumns.uitypeid,
		viewcreationcolumns.moduletabsectionid,
		viewcreationcolumns.viewcreationcolmodelcondvalue");
		$this->db->from('viewcreationcolumns');
		$this->db->where_in('viewcreationcolumns.viewcreationcolumnid',$viewcolids);
		$this->db->where('viewcreationcolumns.status',1);
		$showfield = $this->db->get();
		if($showfield->num_rows() >0) {
			foreach($showfield->result() as $show) {
				$data['colname'][$i]=$show->viewcreationcolumnname;
				$data['colmodelname'][$i]=$show->viewcreationcolmodelname;
				$data['colmodelaliasname'][$i]=$show->viewcreationcolmodelaliasname;
				$data['coltablename'][$i]=$show->viewcreationcolmodeltable;
				$data['coljointablename'][$i]=$show->viewcreationcolmodeljointable;
				$data['coljoinmodelname'][$i]=$show->viewcreationjoincolmodelname;
				$data['colmodelindex'][$i]=$show->viewcreationcolmodeljointable .'.'.$show->viewcreationcolmodelindexname;
				$data['colmodelparenttable'][$i]=$show->viewcreationparenttable;
				$data['colmodelviewtype'][$i]=$show->viewcreationcolumnviewtype;
				$data['colmodeldatatype'][$i]=$show->viewcreationtype;
				$data['colmodelcondname'][$i]=$show->viewcreationcolmodelcondname;
				$data['colmodelcondvalue'][$i]=$show->viewcreationcolmodelcondvalue;
				$data['colmodelmoduleid'][$i]=$show->viewcreationmoduleid;
				$data['colmodeluitype'][$i]=$show->uitypeid;
				$data['colmodelsectid'][$i]=$show->moduletabsectionid;
				$data['colmodeltype'][$i]='text';
				$i++;
			}
		}
		return $data;
    }
	//rule condition data fetch
	public function ruleconditiondatafetch($ruleid) {
		$j=0;
		$extracolinfo = array();
		$multiarray = array();
		$this->db->select('autofollowrulecondtion.viewcreationcolumnid,autofollowrulecondtion.viewcreationconditionname,autofollowrulecondtion.viewcreationconditionvalue,autofollowrulecondtion.viewcreationandorvalue,viewcreationcolumns.viewcreationcolmodelindexname,viewcreationcolumns.viewcreationcolmodeljointable');
		$this->db->from('autofollowrulecondtion');
		$this->db->join('viewcreationcolumns','viewcreationcolumns.viewcreationcolumnid=autofollowrulecondtion.viewcreationcolumnid');
		$this->db->where('autofollowrulecondtion.autofollowruleid',$ruleid);
		$this->db->where('autofollowrulecondtion.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$extracolinfo[$j] = $row->viewcreationcolumnid;
				$multiarray[$j]=array(
					'0'=>$row->viewcreationcolmodeljointable,
					'1'=>$row->viewcreationcolmodelindexname,
					'2'=>$row->viewcreationconditionname,
					'3'=>$row->viewcreationconditionvalue,
					'4'=>$row->viewcreationandorvalue
				);
				$j++;
			}
			$array = array('0'=>$extracolinfo,'1'=>$multiarray);
		} else {
			$array = array('0'=>$extracolinfo,'1'=>$multiarray);
		}
		return $array;
	}
	//un follow records
	public function unfollowrecordsmodel() {
		$moduleid = $_GET['moduleid'];
		$ids = $_GET['ids'];
		$userid = $this->Basefunctions->userid;
		$cdate = date($this->Basefunctions->datef);
		$check = $this->unfollowedrecordidget($moduleid);
		$count = count($check); 
		if($check['check'] == 'False'){
			$recordid = $ids;
			$array = array('moduleid'=>$moduleid,'recordid'=>$recordid,'createdate'=>$cdate,'lastupdatedate'=>$cdate,'lastupdateuserid'=>$userid,'createuserid'=>$userid,'status'=>1);
			$this->db->insert('unfollowrecord',$array);
		} else {
			$exp = $check['recordid'];
			$recordid = $exp.','.$ids;
			$array = array('recordid'=>$recordid,'lastupdatedate'=>$cdate,'lastupdateuserid'=>$userid);
			$this->db->where('unfollowrecord.moduleid',$moduleid);
			$this->db->update('unfollowrecord',$array);
		}
	}
	//un followed record id fetch
	public function unfollowedrecordidget($moduleid) {
		$this->db->select('recordid');
		$this->db->from('unfollowrecord');
		$this->db->where_in('unfollowrecord.moduleid',$moduleid);
		$this->db->where('unfollowrecord.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('check'=>'True','recordid'=>$row->recordid);
			}
		} else {
			$data = array('check'=>'False');
		}
		return $data;
	}
	//mass update default view fetch
	public function defaultviewfetchmodel($mid) {
		$data = "";
		$this->db->select('viewcreationid');
		$this->db->from('viewcreation');
		$this->db->where('viewcreation.viewcreationmoduleid',$mid);
		$this->db->where('viewcreation.createuserid',1);
		$this->db->where('viewcreation.lastupdateuserid',1);
		$this->db->where('viewcreation.status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row){
				$data = $row->viewcreationid;
			}
		}
		return $data;
	}
	//unnoticed record list view
	public function unnoticedrecordfetchmodel($tablename,$primaryid,$sortcol,$sortord,$pagenum,$rowscount,$colinfo,$viewcolmoduleids,$daycount) {
		$count = count($colinfo['colmodelindex']);
		$dataset ='';
		$x = 0;
		$joinq = '';
		$ptab = array($tablename);
		$jtab = array($tablename);
		$maintable = $tablename;
		$selvalue = $tablename.'.'.$tablename.'id';
		for($k=0;$k<$count;$k++) {
			if($k==0) {
				$dataset = $colinfo['colmodelindex'][$k];
			} else{
				$dataset .= ','.$colinfo['colmodelindex'][$k];
			}
			if($x != 0) { $con = "AND"; }
			//parent join check
			$ptabjoin = "";
			$ptabjoin = $colinfo['colmodelparenttable'][$k];
			if(in_array($ptabjoin,$ptab)) {
				if(!in_array($colinfo['coltablename'][$k],$jtab) && $colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
					array_push($jtab,trim($colinfo['coltablename'][$k]));
					//employee
					$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid LIKE "%1%"';
					//group
					$joinq .= ' LEFT OUTER JOIN employeegroup ON FIND_IN_SET(employeegroup.employeegroupid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid  LIKE "%2%"';
					//user role and sub ordinate
					$joinq .= ' LEFT OUTER JOIN userrole ON FIND_IN_SET(userrole.userroleid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND ('.$maintable.'.employeetypeid LIKE "%3%" OR '.$maintable.'.employeetypeid LIKE "%4%")';
				} else {
					if($colinfo['colmodeldatatype'][$k] == 1) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						}
					} else if($colinfo['colmodeldatatype'][$k] == 2) {
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else if($colinfo['colmodeldatatype'][$k] == 3) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						}
					}
				}
			} else {
				if($colinfo['colmodeldatatype'][$k] == 1) {
					$result = $this->Basefunctions->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.','.$maintable.'.'.$primaryid.') > 0 AND '.$ptabjoin.'.status=1';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 1) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
									}
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 2) {
					$joinresult = $this->parentjointableinfo($ptabjoin,$viewcolmoduleids);
					if($joinresult->num_rows() >0) {
						foreach($joinresult->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
							//condition name
							$condcolname = $show->viewcreationcolmodelcondname;
							//condition value
							$condcolvalue = $show->viewcreationcolmodelcondvalue;
						}
						//restrict repeated parent join
						if(in_array($destab,$ptab)) {
							if($destab != $soucrcetab) {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							} else {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							}
						}
						//parent table condition
						if($condcolname != "" && $condcolvalue != "") {
							$attrcond .= " ".$con." ".$srcjointab.'.'.$condcolname."=".$condcolvalue;
							array_push($attrcondarr,$srcjointab.'.'.$joincolname);
							$x = 1;
						}
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] = $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.'='.$maintable.'.'.$primaryid.') > 0 AND '.$ptabjoin.'.status=1';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 2) {
								//attribute join
								if(!in_array($colinfo['coltablename'][$k],$jtab)) {
									array_push($jtab,trim($colinfo['coltablename'][$k]));
									$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
								}
								//condition
								if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
									$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
									array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
									$x = 1;
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 3) {
					$result = $this->Basefunctions->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
							}
						}
					}  else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.','.$maintable.'.'.$primaryid.') > 0 AND '.$ptabjoin.'.status=1';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 3) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
									}
								}
							}
						}
					}
				}
			}
			$dduitypeids = array('17','18','19','23','25','26','27','28','29');
			//fields fetch
			if($colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->Basefunctions->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].', GROUP_CONCAT(DISTINCT userrole.userrolename) AS rolename, GROUP_CONCAT(DISTINCT employeegroup.employeegroupname) AS empgrpname';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k].',userrole.userrolename AS rolename,employeegroup.employeegroupname AS empgrpname';
				}
			} else if( in_array($colinfo['colmodeluitype'][$k],$dduitypeids) ) {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->Basefunctions->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].'';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
				}
			} else {
				$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
			}
		}
		$dayconversion = "AND ".$maintable.".lastupdatedate BETWEEN DATE_SUB(NOW(), INTERVAL ".$daycount." DAY) AND NOW()";
		$status = $tablename.'.status IN (1)';
		$where = '1=1';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		//query
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$selvalue.' from '.$tablename.' '.$joinq.' WHERE '.$where.' '.$dayconversion.' AND '.$status.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		return $data;
	}
	//get conversation edit data-
	public function getconversationeditdata($rowid) {
		$array=array();
		$this->db->select('notificationmessage,notificationlogid,moduleid,commonid,employeeid,filename,privacy');
		$this->db->from('notificationlog');					
		$this->db->where('notificationlog.notificationlogid',$rowid);
		$this->db->limit(1);
		$data=$this->db->get();
		foreach($data->result() as $inf) {			
			$array = array('c_message'=>$inf->notificationmessage,'c_rowid'=>$inf->notificationlogid,'c_moduleid'=>$inf->moduleid,'c_commonid'=>$inf->commonid,'c_employeeid'=>$inf->employeeid,'c_file'=>$inf->filename);
		}
		return $array;
	}
	//create reply
	public function createreply() {
		//retrieve 
		$rowid=$this->input->post('rowid');	
		$message=$this->input->post('message');
		$file=$this->input->post('file');
		$filename=$this->input->post('filename');
		$g_message = $message;
		$parentdata=$this->getconversationeditdata($rowid);	
	
		$numarray=array();
		$createemployee=$this->Basefunctions->logemployeeid;
		
		$moduleid = $parentdata['c_moduleid'];
	    $commonid = $parentdata['c_commonid'];
		
		$usertags='';
		preg_match_all ("/@(.*)\-/U", $message, $pat_array);
		$count=count($pat_array);
		if($count > 1) {
			$usertags = $pat_array[$count-1];
			foreach($usertags as $key=>$value ) {
				if(is_numeric($value)) {
					$numarray[]=$value;
				}
			}
			$usertags = implode(',',array_unique($numarray));
		}
		//group user array segregate
		preg_match_all ("/@[0-9]\:/U",$g_message,$pat_arraytwo); //identify the group patterns
		$count = count($pat_arraytwo);
		if($count == 1) {
			$gnumarray=array();
			$grouptags = $pat_arraytwo;			
			$stepone=str_replace('@','',$grouptags[0]); //remove @
			$steptwo=str_replace(':','',$stepone); //remove -
			foreach($steptwo as $key=>$value ) {
				if(is_numeric($value)){
					$gnumarray[]=$value;
				}
			}	
			$uniquegroup = array_unique($gnumarray);
			$grouptags = implode(',',$uniquegroup);
		}
		$convertinsert=array('feedstype'=>1,'moduleid'=>$moduleid,'notificationmessage'=>$message,'employeeid'=>$usertags,'employeegroupid'=>trim($grouptags),'createuserid'=>$this->Basefunctions->logemployeeid,'lastupdateuserid'=>$this->Basefunctions->logemployeeid,'createdate'=>date($this->Basefunctions->datef),'lastupdatedate'=>date($this->Basefunctions->datef),'status'=>$this->Basefunctions->activestatus,'commonid'=> $commonid,'parentconversationid'=>$rowid,'filename'=>$filename,'filepath'=>$file);
		$this->db->insert('notificationlog',array_filter($convertinsert));
		$conversationid=$this->db->insert_id();
		//notification log entry
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$this->Basefunctions->logemployeeid);
		if(count($numarray) > 0) {
			if(($key = array_search($this->Basefunctions->logemployeeid,$numarray)) !== false) {
				unset($numarray[$key]);
			}
			foreach($numarray as $key=>$value) {
				if($value != $this->Basefunctions->logemployeeid) {
					$notification_array=array('notificationlogtypeid'=>'replied','employeeid'=>$value,'notificationmessage'=>$empname.' replied to ur conversation','createdate'=>date($this->Basefunctions->datef),'lastupdatedate'=>date($this->Basefunctions->datef),'createuserid'=>$this->Basefunctions->logemployeeid,'lastupdateuserid'=>$this->Basefunctions->logemployeeid,'status'=>$this->Basefunctions->activestatus,'moduleid'=>267,'commonid'=>$conversationid,'flag'=>1);
					$this->db->insert('notificationlog',array_filter($notification_array));
				}
			}
		}	
		if(count($uniquegroup) > 0) {
			for($lk=0;$lk<count($uniquegroup);$lk++) {
				$emp_group_data=$this->db->select('employeeid,employeegroupname')
											->from('employeegroup')
											->where('employeegroupid',$uniquegroup[$lk])
											->limit(1)
											->get();
				if($emp_group_data->num_rows() > 0) {
					foreach($emp_group_data->result() as $info){
						$employeeids=$info->employeeid;						
						$employeegroupname=$info->employeegroupname;						
					}
					$groupnumarray=explode(',',$employeeids);
					foreach($groupnumarray as $key=>$value) {					
						if($value != $this->Basefunctions->logemployeeid) {
							$notification_array=array('notificationlogtypeid'=>'conversation','employeeid'=>$value,'notificationmessage'=>$empname.' mentioned u '.$employeegroupname.' in a conversation','createdate'=>date($this->Basefunctions->datef),'lastupdatedate'=>date($this->Basefunctions->datef),'createuserid'=>$this->Basefunctions->logemployeeid,'lastupdateuserid'=>$this->Basefunctions->logemployeeid,'status'=>$this->Basefunctions->activestatus,'moduleid'=>267,'commonid'=>$conversationid,'flag'=>1);
							$this->db->insert('notificationlog',array_filter($notification_array));		
						}
					}
				}
			}
		}
		//tagging a user who interact with this conversation
		$userid = $this->getuseridfromtheconversation($rowid);
		for($i=0;$i<count($userid);$i++) {
			if($userid[$i] != $this->Basefunctions->logemployeeid) {
				$username = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$userid[$i]);
				$notification_array=array('notificationlogtypeid'=>'conversation','employeeid'=>$userid[$i],'notificationmessage'=>$empname.' commented on your conversation','createdate'=>date($this->Basefunctions->datef),'lastupdatedate'=>date($this->Basefunctions->datef),'createuserid'=>$this->Basefunctions->logemployeeid,'lastupdateuserid'=>$this->Basefunctions->logemployeeid,'status'=>$this->Basefunctions->activestatus,'moduleid'=>267,'commonid'=>$conversationid,'flag'=>1);
				$this->db->insert('notificationlog',array_filter($notification_array));	
			}	
		}
		echo true;		
	}
	//get all user id who interact this conversation
	public function getuseridfromtheconversation($rowid) {
		$mainconversationid = $this->getparentconversationid($rowid);
		$userid = $this->getalluseridfromconversation($mainconversationid);
		return $userid;
	}
	//get the main parent conversation id
	public function getparentconversationid($conversationid) {
		$this->db->select('parentconversationid,conversationid');
		$this->db->from('conversation');
		$this->db->where('conversation.conversationid',$conversationid);
		$this->db->where('conversation.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$parentid = $row->parentconversationid;
				$converid = $row->conversationid;
				if($parentid != 0) {
					$this->getparentconversationid($parentid);
				} 
			}
			return $converid;
		}
	}
	//get all user id from the conversation
	public function getalluseridfromconversation($mainconversationid) {
		$parentid = [];
		$this->db->select('createuserid,conversationid');
		$this->db->from('conversation');
		$this->db->where_in('conversation.parentconversationid',$mainconversationid);
		$this->db->where('conversation.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$employeeid[] = $row->createuserid;
				$parentid[] = $row->conversationid;
				if(!empty($parentid)) {
					$this->getalluseridfromconversation($parentid);
				}
			}
			return $employeeid;
		}
	}
	//work flow condition header information fetch
	public function autofollowruleconditioninformationfetchmodel($moduleid) {
		$fnames = array('autofollowrulecondtionid','autorulename','autorulepublic','autorulemoduleid','modulename','autoandor','fieldid','autocolumnname','autocondition','finalautocondvalue','jointable','indexname');
		$flabels = array('Condtion Id','Rule Name','Public','Module Id','Module Name','AND/OR','Field id','Field Name','Condition','Value','Value','Join Table','Indexname');
		$colmodnames = array('autofollowrulecondtionid','autorulename','autorulepublic','autorulemoduleid','modulename','autoandor','fieldid','autocolumnname','autocondition','autoddcondvalue','finalautocondvalue','jointable','indexname');
		$colindexnames = array('autofollowrulecondtion','autofollowrulecondtion','autofollowrulecondtion','autofollowrulecondtion','autofollowrulecondtion','autofollowrulecondtion','autofollowrulecondtion','autofollowrulecondtion','autofollowrulecondtion','autofollowrulecondtion','autofollowrulecondtion','autofollowrulecondtion','autofollowrulecondtion');
		$uitypes = array('2','2','2','2','2','2','2','2','2','2','2','2','2');
		$viewtypes = array('0','0','0','0','0','1','0','1','1','0','1','0','0');
		$dduitypeids = array('17','18','19','20','23','25','26','27','28','29');
		$data = array();
		$i=0;
		$j=0;
		foreach($fnames as $fname) {
			if(in_array($uitypes[$j],$dduitypeids)) {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j].'name';
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]='0';
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]='2';
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			} else {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			}
			$j++;
		}
		return $data;
	}
}