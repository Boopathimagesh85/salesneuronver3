<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Employee extends MX_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Employee/Employeemodel');  		
	}
	//Default Function
	public function index() {
		$moduleid = array(4);
		sessionchecker($moduleid);
		//action
		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(4);
		$viewmoduleid = array(4);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['defaultstateid']=$this->Basefunctions->singlefieldfetch('stateid','branchid','branch',$this->Basefunctions->branchid);
		$data['defaultcountryid']=$this->Basefunctions->singlefieldfetch('countryid','branchid','branch',$this->Basefunctions->branchid);
      	$this->load->view('Employee/employeeview',$data);       
	}
	//Create New data
	public function newdatacreate() { 
    	$this->Employeemodel->newdatacreatemodel();
    }
	//information fetch
	public function fetchformdataeditdetails() {
		$moduleid = array(4);
		$this->Employeemodel->informationfetchmodel($moduleid);
	}
	//Update old data
    public function datainformationupdate() {
        $this->Employeemodel->datainformationupdatemodel();
    }
	//delete old informtion
    public function deleteinformationdata() {
    	$moduleid = array(4);
		$this->Employeemodel->deleteoldinformation($moduleid);
    } 
	//delete old informtion
    public function enableinformationdata() {
        $moduleid = 4;
		$this->Employeemodel->enableinformationdata($moduleid);
    }
    //disavle infrmation
    public function dsiableinformationdata() {
    	$moduleid = 4;
    	$this->Employeemodel->dsiableinformationdatamodels($moduleid);
    }
	//primary and secondary address value fetch
	public function primaryaddressvalfetch() {
		$this->Employeemodel->primaryaddressvalfetchmodel();
	}
	//editor value fetch 	
	public function editervaluefetch() {
		$filename = $_GET['filename']; 
		$newfilename = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1])){
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]);
			echo json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			echo json_encode($tccontent);
		}
	}
	//employee password update
	public function passwordupdate() {
		$this->Employeemodel->passwordupdatemodel();
	}
	//employee id check for employee
	public function employeeidcheck() {
		$this->Employeemodel->employeeidcheckmodel();
	}
	//employee unique id check
	public function employeeuniqueemailidcheck() {
		$this->Employeemodel->employeeuniqueemailidcheckmodel();
	}
	//employee sms-mail Signature data fetch
	public function signaturedatafetch() {
		$this->Employeemodel->signaturedatafetchmodel();
	}
	//default time zone get 
	public function defaulttimezoneget() {
		$this->Employeemodel->defaulttimezonegetmodel();
	}
	//load accountname
	public function loadvendorname() {
		$this->Employeemodel->loadaccountname();
	}
}