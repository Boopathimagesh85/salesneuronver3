<?php if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );
class Employeemodel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
	}
	//To Create New employee Details
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$restricttable = explode(',',$_POST['resctable']);
		//industry based moduleid
		$moduleid = $_POST['viewfieldids'];
		//Restrict user creation based on plan users
		$billusers='0';
		$mcid = $this->usercompanyid();
		$this->db->select('billinginformationid,billingusers');
		$this->db->from('billinginformation');
		$this->db->where('billinginformation.mastercompanyid',$mcid);
		$purusercount=$this->db->get();
		foreach($purusercount->result() as $userdata) {
			$billusers=$userdata->billingusers;
		}
		//Retrieve the Account autoNumber
		$anfieldnameinfo = $_POST['anfieldnameinfo'];
		$anfieldnameidinfo = $_POST['anfieldnameidinfo'];
		$anfieldtabinfo = $_POST['anfieldtabinfo'];
		$anfieldnamemodinfo = $_POST['anfieldnamemodinfo'];
		$randomnum = $this->Basefunctions->randomnumbergenerator($anfieldnamemodinfo,$anfieldtabinfo,$anfieldnameinfo,$anfieldnameidinfo);
		$_POST['employeenumber']=trim($randomnum);
		//check created user count
		$this->db->select('employeeid');
		$this->db->from('employee');
		$this->db->where('employee.status',1);
		$creduser=$this->db->get();
		$curusercount = $creduser->num_rows();
		if($billusers!=0 && $billusers>$curusercount) {
			$primaryid = $this->Crudmodel->datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable);
			//password insertion
			$employee_password = $_POST['password'];
			if(!empty($employee_password)) {
				/* Unwanted code - bcz it doesn't take special characters.
				$userpassword = preg_replace('/[^A-Za-z0-9]/', '',$employee_password);
				$userpassword = strtolower($userpassword); */
				$hash = password_hash($_POST['password'], PASSWORD_DEFAULT);
				$hasharray=array('password'=>$hash);
				$this->db->where('employeeid',$primaryid);
				$this->db->update('employee',$hasharray);
			}
			//address insertion
			$arrname=array('primary','secondary');
			$this->Crudmodel->addressdatainsert($arrname,$partablename,$primaryid);
			{//insert data into master db
				$empinfo = $this->db->query('select employee.emailid,company.mastercompanyid from employee JOIN branch ON branch.branchid=employee.branchid JOIN company ON company.companyid=branch.companyid where employee.employeeid = '.$primaryid.' AND employee.status=1')->result();
				foreach($empinfo as $result) {
					$mascompanyid = $result->mastercompanyid;
					$emailid = $result->emailid;
				}
				//userdatabase
				$database = $this->db->database;
				//activate user plan
				$this->userplanactivationinfolog($mascompanyid,$emailid,$this->db->masterdb,$primaryid);
				//activate user database
				$this->inlineuserdbenable($database);
				//send account activation mail
				//$zone = $this->Basefunctions->employeetimezoneset($this->Basefunctions->userid);
				//$this->load->helper('phpmailer');
				//$this->sendverificationmail($emailid,$zone['name'],$_POST['name'].$_POST['lastname'],$this->Basefunctions->userid);
			}
			{//workflow management -- for data create
				$this->Basefunctions->workflowmanageindatacreatemode($moduleid,$primaryid,$partablename);
			}
			//audit-log
			$userid = $this->Basefunctions->logemployeeid;
			$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
			$activity = ''.$user.' created Users - '.$_POST['name'].'';
			$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,4);
			echo 'TRUE';
		} else {
			echo 'User limit is exist.Please buy more users.';
		}
	}
	//user plan activation log
	public function userplanactivationinfolog($companyid,$email,$databasename,$primaryid) {
		$masterdb = $this->inlinemasterdatabaseactive($this->db->masterdb); //master data base select
		$userplaninfo = $masterdb->query('select userplaninfo.planid,userplaninfo.industryid,userplaninfo.companyid,userplaninfo.activationdate,userplaninfo.expiredate,userplaninfo.domainnameinfo,userplaninfo.databasenameinfo,userplaninfo.accountcreationdate,userplaninfo.accountclosedate from userplaninfo where userplaninfo.companyid = '.$companyid.' AND userplaninfo.status=1 ORDER BY userplaninfoid LIMIT 1')->result();
		foreach($userplaninfo as $upinfo) {
			$industryid = $upinfo->industryid;
			$planid = $upinfo->planid;
			$activationdate = $upinfo->activationdate;
			$expiredate = $upinfo->expiredate;
			$domainname = $upinfo->domainnameinfo;
			$databasename = $upinfo->databasenameinfo;
			$accntcredate = $upinfo->accountcreationdate;
			$accntcredate = $upinfo->accountcreationdate;
			$accntclosedate = $upinfo->accountclosedate;
		}
		$planloginfo = array(
			'industryid'=>$industryid,
			'planid'=>$planid,
			'companyid'=>$companyid,
			'registeremailid'=>$email,
			'accountcreationdate'=>$accntcredate,
			'activationdate'=>$activationdate,
			'expiredate'=>$expiredate,
			'domainnameinfo'=>$domainname,
			'databasenameinfo'=>$databasename,
			'activationstatus'=>'Yes',
			'accountclosedate'=>$accntclosedate,
			'status'=>1
		);
		$masterdb->insert('userplaninfo',$planloginfo);
	}
	//subscriber master company
	public function usercompanyid()	{
		$mcid = 1;
		$empid = $this->Basefunctions->userid;
		$compquery = $this->db->query('select company.companyid,company.mastercompanyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.'')->result();
		foreach($compquery as $key) {
			$mcid = $key->mastercompanyid;
		}
		return $mcid;
	}
	//information fetch
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementpartable']);
		$restricttable = explode(',',$_GET['resctable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['dataprimaryid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		echo $result = $this->Crudmodel->dbdatafetchwithrestrict($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$restricttable,$moduleid);
	}
	//update data information
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['primarydataid'];
		$restricttable = explode(',',$_POST['resctable']);
		//industry based moduleid
		$moduleid = $_POST['viewfieldids'];
		{//fetch old values -- work flow
			$condstatvals=$this->Basefunctions->workflowmanageolddatainfofetch($moduleid,$primaryid);
		}
		$result = $this->Crudmodel->dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable);
		//address insertion
		$arrname=array('primary','secondary');
		$this->Crudmodel->addressdataupdate($arrname,$partablename,$primaryid);
		//password insertion
		$employee_password = $_POST['password'];
		if(!empty($employee_password)) {
			/* Unwanted code - bcz it doesn't take special characters.
			$userpassword = preg_replace('/[^A-Za-z0-9]/', '',$employee_password);
			$userpassword = strtolower($userpassword); */
			$hash = password_hash($_POST['password'], PASSWORD_DEFAULT);
			$hasharray=array('password'=>$hash);
			$this->db->where('employeeid',$primaryid);
			$this->db->update('employee',$hasharray);
		}
		{//workflow management for data update
			$this->Basefunctions->workflowmanageindataupdatemode($moduleid,$primaryid,$partablename,$condstatvals);
		}
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' updated Users - '.$_POST['name'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$activity ,$userid,4);
		echo 'TRUE';
	}
	//delete old information
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//Check if it is primary user
		$primaryuser = $this->primaryuseridget($partabname);
		if($primaryuser != $id ) {
			//delete operation
			$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
			$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
			if($ruleid == 1 || $ruleid == 2) {
				$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
				if($chek == 0) {
					echo 'Denied';
				} else {
					$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$tablename,$id);
					echo 'TRUE';
				}
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$tablename,$id);
				echo 'TRUE';
			}
		} else {
			echo 'FALSE';
		}
	}
	//Enable old information
	public function enableinformationdata($moduleid) {
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//Check if it is primary user
		$primaryuser = $this->primaryuseridget($partabname);
		if($primaryuser != $id ) {
			$validusercheck = $this->checkvaliduser();
			if($validusercheck == 'TRUE'){
				$check = $this->checkrecordstatus($id,0);
				if($check == 'TRUE') {
					$this->enableuser($id);
					{//update data into master db
						$empinfo = $this->db->query('select employee.emailid,company.mastercompanyid from employee JOIN branch ON branch.branchid=employee.branchid JOIN company ON company.companyid=branch.companyid where employee.employeeid = '.$id.' AND employee.status=1')->result();
						foreach($empinfo as $result) {
							$mascompanyid = $result->mastercompanyid;
							$emailid = $result->emailid;
						}
						//userdatabase
						$database = $this->db->database;
						//enable user
						$this->enabledisabletheuserinmasterdb($mascompanyid,$emailid,$this->db->masterdb,$id,1);
						//activate user database
						$this->inlineuserdbenable($database);
						//send account activation mail
					}
					echo 'TRUE';
				} else{
					echo 'Denied';
				}
			} else {
				echo 'MAXUSER';
			}
		} else {
			echo 'FALSE';
		}
	}
	public function checkvaliduser() {
		$billusers='0';
		$mcid = $this->usercompanyid();
		$this->db->select('billinginformationid,billingusers');
		$this->db->from('billinginformation');
		$this->db->where('billinginformation.mastercompanyid',$mcid);
		$purusercount=$this->db->get();
		foreach($purusercount->result() as $userdata) {
			$billusers=$userdata->billingusers;
		}
		$this->db->select('employeeid');
		$this->db->from('employee');
		$this->db->where('employee.status',1);
		$creduser=$this->db->get();
		$curusercount = $creduser->num_rows();
		if($billusers!=0 && $billusers>$curusercount) {
			return 'TRUE';
		} else {
			return 'FALSE';
		}
	}
	//enable and disable the user in masterdb
	public function enabledisabletheuserinmasterdb($mascompanyid,$emailid,$masterdb,$primaryid,$status) {
		$masterdb = $this->inlinemasterdatabaseactive($this->db->masterdb); //master data base select
		$planloginfo = array(
				'status'=>$status
		);
		$masterdb->where('userplaninfo.companyid',$mascompanyid);
		$masterdb->where('userplaninfo.registeremailid',$emailid);
		$masterdb->update('userplaninfo',$planloginfo);
	}
	//disable the data
	public function dsiableinformationdatamodels($moduleid) {
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//Check if it is primary user
		$primaryuser = $this->primaryuseridget($partabname);
		if($primaryuser != $id ) {
			$check = $this->newcheckrecordstatus($id,2);
			if($check == 'TRUE') {
				{//update data into master db
					$empinfo = $this->db->query('select employee.emailid,company.mastercompanyid from employee JOIN branch ON branch.branchid=employee.branchid JOIN company ON company.companyid=branch.companyid where employee.employeeid = '.$id.' AND employee.status=1')->result();
					foreach($empinfo as $result) {
						$mascompanyid = $result->mastercompanyid;
						$emailid = $result->emailid;
					}
					//userdatabase
					$database = $this->db->database;
					//enable user
					$this->enabledisabletheuserinmasterdb($mascompanyid,$emailid,$this->db->masterdb,$id,2);
					//activate user database
					$this->inlineuserdbenable($database);
					//send account activation mail
				}
				$this->disbeluser($id);
				//audit-log
				$userid = $this->Basefunctions->logemployeeid;
				$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
				$disableuser = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$id);
				$activity = ''.$user.' disabled Users - '.$disableuser.'';
				$this->Basefunctions->notificationcontentadd($id,'Updated',$activity ,$userid,4);
				echo 'TRUE';
			} else{
				echo 'Denied';
			}
		} else {
			echo 'FALSE';
		}
	}
	public function enableuser($id) {
		$updatedate = date($this->Basefunctions->datef);
		$updateuser = $this->Basefunctions->userid;
		$enablearray = array('status'=>1,'lastupdatedate'=>$updatedate,'lastupdateuserid'=>$updateuser);
		$this->db->where('employeeid',$id);
		$this->db->update('employee',$enablearray);
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$enableuser = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$id);
		$activity = ''.$user.' enabled Users - '.$enableuser.'';
		$this->Basefunctions->notificationcontentadd($id,'Updated',$activity ,$userid,4);
		//audit-log
	}
	public function disbeluser($id) {
		$updatedate = date($this->Basefunctions->datef);
		$updateuser = $this->Basefunctions->userid;
		$enablearray = array('status'=>2,'lastupdatedate'=>$updatedate,'lastupdateuserid'=>$updateuser);
		$this->db->where('employeeid',$id);
		$this->db->update('employee',$enablearray);
	}
	//check record status
	public function checkrecordstatus($id,$status) {
		$data = 'False';
		$this->db->select('status');
		$this->db->from('employee');
		$this->db->where('employee.employeeid',$id);
		$this->db->where_not_in('employee.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = 'TRUE';
			} 
		}
		return $data;
	}
	//check record status
	public function newcheckrecordstatus($id,$status) {
		$data = 'False';
		$this->db->select('employeeid');
		$this->db->from('employee');
		$this->db->where('employee.employeeid',$id);
		$this->db->where_not_in('employee.status',2);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = 'TRUE';
			}
		}
		return $data;
	}
	//primary user id get
	public function primaryuseridget($partabname) {
		$partabnameid = $partabname.'id';
		$this->db->select("$partabnameid");
		$this->db->from("$partabname");
		$this->db->where("$partabname".'.branchid',2);
		$this->db->where("$partabname".'.status',1);
		$this->db->order_by("$partabname".'.'."$partabnameid", "asc");
		$this->db->limit(1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = $row->$partabnameid;
			}
			return $data;
		}
	}
	//primary and secondary address value fetch
	public function primaryaddressvalfetchmodel() {
		$accid = $_GET['dataprimaryid'];	
		$this->db->select('addresssourceid,address,pincode,city,state,country');
		$this->db->from('employeeaddress');
		$this->db->where('employeeaddress.employeeid',$accid);
		$this->db->where('employeeaddress.status',1);
		$this->db->order_by('employeeaddressid','asc');
		$result = $this->db->get();
		$arrname=array('primary','secondary');
		if($result->num_rows() >0) {
			$m=0;
			foreach($result->result() as $row) {
				$data[] = array($arrname[$m].'addresstype'=>$row->addresssourceid,
								$arrname[$m].'address'=>$row->address,
								$arrname[$m].'pincode'=>$row->pincode,
								$arrname[$m].'city'=>$row->city,
								$arrname[$m].'state'=>$row->state,
								$arrname[$m].'country'=>$row->country);
				$m++;
			}
			if(count($data) != 1 ) { 
				$finalarray = array_merge($data[0],$data[1]);
			} else {
				$finalarray = $data[0];
			}
			echo json_encode($finalarray);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//password update
	public function passwordupdatemodel() {
		$id = $_POST['empid'];
		$newpassword = $_POST['newpassword'];
		$hash = password_hash($newpassword, PASSWORD_DEFAULT);
		$password = array('password'=>$hash);
		$this->db->where('employee.employeeid',$id);
		$this->db->update('employee',$password);
		echo "TRUE";
	}
	//employee id check model
	public function employeeidcheckmodel() {
		$employeeid = $_GET['employeeid'];
		$this->db->select('status');
		$this->db->from('employee');
		$this->db->where('employee.employeeid',$employeeid);
		$this->db->where('employee.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				$data = $row->status;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//employee email id check
	public function employeeuniqueemailidcheckmodel() {
		$emailid = $_POST['email'];
		$masterdb = $this->inlinemasterdatabaseactive($this->db->masterdb);
		if($emailid != "") {
			$result = $masterdb->select('userplaninfo.userplaninfoid')->from('userplaninfo')->where('userplaninfo.registeremailid',$emailid)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->userplaninfoid;
				}
			} else {
				echo "False";
			}
		} else {
			echo "False";
		}
	}
	//inline user data base set
	public function inlinemasterdatabaseactive($databasename) {
		$host = $this->db->hostname;
		$user = $this->db->username;
		$passwd = $this->db->password;
		$dbconfig['hostname'] = $host;
		$dbconfig['username'] = $user;
		$dbconfig['password'] = $passwd;
		$dbconfig['database'] = $databasename;
		$dbconfig['dbdriver'] = 'mysqli';
		$dbconfig['dbprefix'] = '';
		$dbconfig['pconnect'] = TRUE;
		$dbconfig['db_debug'] = TRUE;
		$dbconfig['cache_on'] = FALSE;
		$dbconfig['cachedir'] = '';
		$dbconfig['char_set'] = 'utf8';
		$dbconfig['dbcollat'] = 'utf8_general_ci';
		$dbconfig['swap_pre'] = '';
		$dbconfig['autoinit'] = TRUE;
		$dbconfig['stricton'] = FALSE;
		return $import_db = $this->load->database($dbconfig, TRUE);
	}
	public function inlineuserdbenable($database) {
		$active_group = 'default';
		$query_builder = TRUE;
		$host = $this->db->hostname;
		$user = $this->db->username;
		$passwd = $this->db->password;
		$masdb=$this->db->masterdb;
		$db['default']['hostname'] = $host;
		$db['default']['username'] = $user;
		$db['default']['password'] = $passwd;
		$db['default']['database'] = $database;
		$db['default']['dbdriver'] = 'mysqli';
		$db['default']['dbprefix'] = '';
		$db['default']['pconnect'] = TRUE;
		$db['default']['db_debug'] = TRUE;
		$db['default']['cache_on'] = FALSE;
		$db['default']['cachedir'] = '';
		$db['default']['char_set'] = 'utf8';
		$db['default']['dbcollat'] = 'utf8_general_ci';
		$db['default']['swap_pre'] = '';
		$db['default']['autoinit'] = TRUE;
		$db['default']['stricton'] = FALSE;
		$db['default']['masterdb'] = $masdb;
		$this->load->database('default',TRUE);
	}
	//sms-mail signature insert/Update
	public function smsmailsignatureinsert($signaturetypeid,$signaturename,$primaryid,$editordata) {
		$value = $this->smsmailsignaturecheck($signaturetypeid,$primaryid);
		$date = date($this->Basefunctions->datef);
		$file_name = 'termscondition/template_'.trim($signaturename)."_".time().".html";
		$html_file_name = 'template_'.trim($signaturename)."_".time().".html";
		$fp = fopen('templates/'.$html_file_name, "w");
		fwrite($fp, $editordata);
		fclose($fp);
		if($value != "False") {
			$array = array('signaturename'=>$signaturename,'userssignature_editorfilename'=>$file_name,'lastupdatedate'=>$date,'lastupdateuserid'=>$this->Basefunctions->userid);
			$this->db->where_in('signature.signaturetypeid',array($signaturetypeid));
			$this->db->where_in('signature.employeeid',array($primaryid));
			$this->db->update('signature',$array);
		} else {
			$array = array('signaturetypeid'=>$signaturetypeid,'signaturename'=>$signaturename,'employeeid'=>$primaryid,'userssignature_editorfilename'=>$file_name,'createdate'=>$date,'createuserid'=>$this->Basefunctions->userid,'lastupdatedate'=>$date,'lastupdateuserid'=>$this->Basefunctions->userid,'status'=>$this->Basefunctions->activestatus);
			$this->db->insert('signature',$array);
		}
	}
	//sms-mail signature check - if its available or not
	public function smsmailsignaturecheck($signaturetypeid,$primaryid) {
		$this->db->select('signatureid');
		$this->db->from('signature');
		$this->db->where('signature.signaturetypeid',$signaturetypeid);
		$this->db->where('signature.employeeid',$primaryid);
		$this->db->where('signature.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = "TRUE";
			}
			return $data;
		} else {
			$data = "False";
			return $data;
		}	
	}
	//sms mail signature data fetch
	public function signaturedatafetchmodel() {
		$empid = $_GET['empid'];
		$typeid = $_GET['typeid'];
		$this->db->select('signaturename,userssignature_editorfilename');
		$this->db->from('signature');
		$this->db->where('signature.signaturetypeid',$typeid);
		$this->db->where('signature.employeeid',$empid);
		$this->db->where('signature.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('signame'=>$row->signaturename,'filename'=>$row->userssignature_editorfilename);
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}	
	}
	//default time zone get
	public function defaulttimezonegetmodel() {
		$timezonid = $this->Basefunctions->generalinformaion('company','timezoneid','companyid',2);
		echo json_encode($timezonid);
	}
	//verification mail send
	public function sendverificationmail($email,$zone,$cname,$logempid) {
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$logempid);
		$empemail = $this->Basefunctions->generalinformaion('employee','emailid','employeeid',$logempid);
		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->SMTPAuth   = true;
		$mail->SMTPSecure = "tls";
		$mail->Username   = "arvind@salesneuron.com";
		$mail->Password   = "X9WnMXpxhC8YEOUPuQ3oZw";
		$mail->Host       = "smtp.mandrillapp.com";
		$mail->Port       = 587;
		$mail->SetFrom($empemail,'[Sales Neuron]'.$empname.'');
		$mail->Subject   = "Sales Neuron: Account Created";
		$expdate = date("YmdHis", strtotime('+1 days'));
		$datasets = "uesns9s".base64_encode($email);
		$datas = strrev($datasets);
		$urldatasets = base64_encode($datas);
		$secure = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "");
		$root = (($_SERVER['HTTP_HOST']!='localhost')?"http".$secure."://".$_SERVER['HTTP_HOST'] : "http".$secure."://".$_SERVER['HTTP_HOST'].'/salesneuronsite');
		//new
		$body='<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
<title>Destiny</title>';
		$body.="
<link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
<style type='text/css'>
div, p, a, li, td { -webkit-text-size-adjust:none; }
*{
-webkit-font-smoothing: antialiased;
-moz-osx-font-smoothing: grayscale;
}
.ReadMsgBody
{width: 100%; background-color: #f5f5f5;}
.ExternalClass
{width: 100%; background-color: #f5f5f5;}
body{width: 100%; height: 100%; background-color: #f5f5f5; margin:0; padding:0; -webkit-font-smoothing: antialiased;}
html{width: 100%; background-color: #f5f5f5;}
@font-face {font-family: 'proxima_novalight';src: url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_nova_rgregular'; src: url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_novasemibold';src: url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_nova_rgbold';src: url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_novathin';src: url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
@font-face {font-family: 'proxima_novaextrabold';src: url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.woff2') format('woff2'),url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
p {padding: 0!important; margin-top: 0!important; margin-right: 0!important; margin-bottom: 0!important; margin-left: 0!important; }
.hover:hover {opacity:0.85;filter:alpha(opacity=85); }
.jump:hover {
	opacity:0.75;
	filter:alpha(opacity=75);
	padding-top: 10px!important;
}
a#rotator img {
-webkit-transition: all 1s ease-in-out;
-moz-transition: all 1s ease-in-out; 
-o-transition: all 1s ease-in-out; 
-ms-transition: all 1s ease-in-out; 
}
a#rotator img:hover { 
-webkit-transform: rotate(360deg); 
-moz-transform: rotate(360deg); 
-o-transform: rotate(360deg);
-ms-transform: rotate(360deg); 
}
@-webkit-keyframes spaceboots {
	0% { -webkit-transform: translate(2px, 1px) rotate(0deg); }
	10% { -webkit-transform: translate(-1px, -2px) rotate(-1deg); }
	20% { -webkit-transform: translate(-3px, 0px) rotate(1deg); }
	30% { -webkit-transform: translate(0px, 2px) rotate(0deg); }
	40% { -webkit-transform: translate(1px, -1px) rotate(1deg); }
	50% { -webkit-transform: translate(-1px, 2px) rotate(-1deg); }
	60% { -webkit-transform: translate(-3px, 1px) rotate(0deg); }
	70% { -webkit-transform: translate(2px, 1px) rotate(-1deg); }
	80% { -webkit-transform: translate(-1px, -1px) rotate(1deg); }
	90% { -webkit-transform: translate(2px, 2px) rotate(0deg); }
	100% { -webkit-transform: translate(1px, -2px) rotate(-1deg); }
}
#shake:hover,
#shake:focus {
	-webkit-animation-name: spaceboots;
	-webkit-animation-duration: 0.8s;
	-webkit-transform-origin:50% 50%;
	-webkit-animation-iteration-count: infinite;
	-webkit-animation-timing-function: linear;
}
.image600 img {width: 600px; height: auto;}
.image595 img {width: 595px; height: auto;}
.icon27 img {width: 27px; height: auto;}
.icon24 img {width: 24px; height: auto;}
.image254 img {width: 254px; height: auto;}
.image176 img {width: 176px; height: auto;}
.image260 img {width: 260px; height: auto;}
.avatar96 img {width: 96px; height: auto;}
.icons46 img {width: 46px; height: auto;}
.image248 img {width: 246px; height: auto;}
#logo img {width: 120px; height: auto;}
#icon18 img {width: 18px; height: auto;}
</style>
<!-- @media only screen and (max-width: 640px) 
{*/
-->
<style type='text/css'> @media only screen and (max-width: 640px){
		body{width:auto!important;}
		table[class=full] {width: 100%!important; clear: both; }
		table[class=mobile] {width: 100%!important; padding-left: 30px; padding-right: 30px; clear: both; }
		table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
		td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
		*[class=erase] {display: none;}
		*[class=buttonScale] {float: none!important; text-align: center!important; display: inline-block!important; clear: both;}
		.h60 {height: 60px!important;}
		.h30 {height: 30px!important;}
		.h10 {height: 10px!important;}
		.h15 {height: 15px!important;}
		td[class=image600] img {width: 100%!important; text-align: center!important; clear: both; }
		td[class=image595] img {width: 100%!important; text-align: center!important; clear: both; }
		table[class=sponsor] {text-align:center; float:none; width:70%!important;}
} </style>
<!--
@media only screen and (max-width: 479px) 
{
-->
<style type='text/css'>
@media only screen and (max-width: 479px){
	body{width:auto!important;}
	table[class=full] {width: 100%!important; clear: both; }
	table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
	table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
	td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
	*[class=erase] {display: none;}
	*[class=buttonScale] {float: none!important; text-align: center!important; display: inline-block!important; clear: both;}
	.h60 {height: 60px!important;}
	.h30 {height: 30px!important;}
	.h10 {height: 10px!important;}
	.h15 {height: 15px!important;}
	td[class=image600] img {width: 100%!important; text-align: center!important; clear: both; }
	td[class=image595] img {width: 100%!important; text-align: center!important; clear: both; }
	table[class=sponsor] {text-align:center; float:none; width:100%!important;}
	td[class=font10] {font-size: 10px!important;}
	span[class=font38] {font-size: 38px!important; line-height: 42px!important;}
}
}</style>
</head>";
		$body.='
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix">
<div class="ui-sortable" id="sort_them">
<!-- Seperator 2 + Social -->
<table class="full" align="center" bgcolor="#1dbbff" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td  style="padding:20px;" id="sep2"align="center">
			<!-- Wrapper -->
			<table class="mobile" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td align="center">
						<!-- Wrapper -->
						<table class="full" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
							<tr>
								<td align="center" width="100%">
									<!-- SORTABLE -->
									<div class="sortable_inner ui-sortable">
									<!-- Space -->
									<table class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="40" width="100%"></td>
										</tr>
									</table><!-- End Space -->
									<!-- Text -->
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td style="font-size: 26px; color: rgb(255, 255, 255); text-align: center; font-family: Helvetica,Arial,sans-serif; line-height: 32px; font-weight: bold; vertical-align: top;" class="fullCenter" align="center" width="100%">
												<!--[if !mso]><!--><span style="font-family: '; $body.="'Titillium Web'"; $body.=', sans-serif; font-weight:normal;"><!--<![endif]--><p cu-identify="element_06079989344851364" >Sign Up Confirmation<br><!--[if !mso]><!--></span><!--<![endif]--><p>
											</td>
										</tr>
										<tr>
											<td height="05" width="100%"></td>
										</tr>
										<!-- Space -->
										<table class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
											<tr>
												<td height="10" width="100%"></td>
											</tr>
										</table><!-- End Space -->
										<tr>
											<td style="font-size: 26px; color: rgb(255, 255, 255); text-align: center; font-family: Helvetica,Arial,sans-serif; line-height: 32px; font-weight: bold; vertical-align: top;" class="fullCenter" align="center" width="100%">
												<!--[if !mso]><!--><span style="font-family:'; $body.="'proxima_novasemibold'";$body.=', Helvetica; font-weight:normal;"><!--<![endif]--><p cu-identify="element_06079989344851364" >
												<img width="170" height="100" style="padding-right:10px;padding-bottom:10px;" src="'.$root.'/images/homepagelogo.png"><br><!--[if !mso]><!--></span><!--<![endif]--><p>
											</td>
										</tr>
										<tr>
											<td height="05" width="100%"></td>
										</tr>
										<tr>
											<td style="font-size: 26px; color: rgb(255, 255, 255); text-align: center; font-family: Helvetica,Arial,sans-serif; line-height: 32px; font-weight: bold; vertical-align: top;" class="fullCenter" align="center" width="100%">
												<!--[if !mso]><!--><span style="font-family:'; $body.="'Titillium Web'";$body.=', sans-serif; font-weight:normal;"><!--<![endif]--><p cu-identify="element_06079989344851364" >Hi '.$cname.',<br><!--[if !mso]><!--></span><!--<![endif]--><p>
											</td>
										</tr>
									</table>
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="15" width="100%">
											</td>
										</tr>
									</table>
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td style="font-size: 14px; color: rgb(255, 255, 255); text-align: center; font-family: Helvetica,Arial,sans-serif; line-height: 22px; font-weight: bold; vertical-align: top;" class="fullCenter" align="center" width="100%">												
												<!--[if !mso]><!--><span style="font-family:'; $body.="'proxima_nova_rgregular'"; $body.=', Helvetica; font-weight:normal;"><!--<![endif]--><p cu-identify="element_05107549716118674" ><div style="text-align: center;"><font size="3"><span style="font-family: Verdana;">you have been added to sales neuron crm by '.$empname.'<br> User Name : '.$email.' </span><br><span style="font-family: Verdana;"></span></font></div><!--[if !mso]><!--></span><!--<![endif]--><p>
											</td>
										</tr>
									</table>									
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="15" width="100%">
											</td>
										</tr>
									</table>									
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<!----------------- Button Center ----------------->
										<tr>
											<td class="buttonScale" align="center" width="auto">
												<table class="buttonScale"style="border-radius: 25px; padding-left: 35px; padding-right: 35px; background-color: rgb(0, 150, 136);" id="shake" align="center" bgcolor="#1dbbff" border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td style="font-weight: bold; font-family: Helvetica,Arial,sans-serif; color: rgb(255, 255, 255); background-color: rgb(0, 150, 136);" align="center" bgcolor="#ffffff" height="52" width="auto">
															<!--[if !mso]><!--><span style=" border: 1px solid #ffffff;border-radius: 8px; font-family:'; $body.="'proxima_nova_rgbold'";$body.=',Helvetica;font-weight: normal;padding: 8px; background-color:#ffffff;"><!--<![endif]-->
																<a cu-identify="element_0753161873513802" href="'.$root.'/resetpasswordform.php?SN='.$urldatasets.'" style="color: #1dbbff; font-size: 18px; text-decoration: none; line-height: 34px; width: 100%;">Set my password</a>
															<!--[if !mso]><!--></span><!--<![endif]-->
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<!----------------- End Button Center ----------------->
									</table>
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="80" width="100%">
											</td>
										</tr>
									</table>									
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td style="font-size: 14px; color: rgb(255, 255, 255); text-align: center; font-family: Helvetica,Arial,sans-serif; line-height: 22px; font-weight: bold; vertical-align: top;" class="fullCenter" align="center" width="100%">
												<p cu-identify="element_03207356464405948" >Did not Sign Up? Please ignore this mail.<br>
											</td>
										</tr>
									</table>									
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="0" width="100%">
											</td>
										</tr>
									</table>									
									<!-- Social Icons -->
									<div style="display: none" id="element_09872123869190068"></div>
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" object="drag-module-small" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
										<tr>
											<td height="0" width="100%">
											</td>
										</tr>
									</table>
									</div>
									<!-- END SORTABLE -->
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table><!-- End Wrapper --> 
		</div>
		</td>
	</tr>
</table>
<!-- End Wrapper -->
<!-- Wrapper 3 -->
<div style="display: none" id="element_03663119396258956"></div>
<!-- End Wrapper 3 -->
<!-- Wrapper 28 -->
<div style="display: none" id="element_08147460135100794"></div><!-- Wrapper 28 -->
<!-- Wrapper 28 -->
<div style="display: none;" id="element_046737467804971156"></div><!-- Wrapper 28 -->
<!-- Wrapper 3 -->
<div style="display: none" id="element_010546669432505962"></div>
<!-- End Wrapper 3 -->
<!-- Wrapper Footer -->
<table style="background-color:#e1e1e1;" class="full" align="center" bgcolor="#f1f4f6" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td align="center" valign="top" width="100%">
			<!-- Wrapper -->
			<table class="mobile" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td align="center">
						<!-- Space -->
						<table class="full" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td height="20" width="100%"></td>
							</tr>
						</table><!-- End Space -->
						<!-- Wrapper -->
						<table class="full" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
							<tr>
								<td align="center" width="100%">
									<!-- Icon 1 -->
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: center;" class="fullCenter" align="left" border="0" cellpadding="0" cellspacing="0" width="400">
										<tr>
											<td style="text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 13px; color: #aaacaf; line-height: 22px;" class="fullCenter" valign="middle" width="100%">
												<span style="font-family:'; $body.="'proxima_nova_rgregular'";$body.=', Helvetica; font-weight: normal;">
													<span style="color: #787b80;">© 2015 </span>,<a href="http://www.aucventures.com" style="text-decoration: none; color: #787b80;">AUC Ventures Pvt Ltd</a> , All Rights Reserved
													<span style="font-family:'; $body.="'proxima_nova_rgregular'";$body.=', Helvetica;"></span>
												</span>
											</td>
										</tr>
									</table>
									<!-- Space -->
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full" align="left" border="0" cellpadding="0" cellspacing="0" width="1">
										<tr>
											<td height="12" width="100%"></td>
										</tr>
									</table>
									<!-- Social Icons Footer -->
									<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: right;" class="fullCenter" align="right" border="0" cellpadding="0" cellspacing="0" width="120">
										<tr>
											<td id="icon18" width="100%">
												&nbsp;
												<a href="http://facebook.com/salesneuron" style="text-decoration: none;" >
													<img src="http://rocketway.net/themebuilder/products/templates/destiny/images/footer_icon2.png" style="width: 18px; height:auto;" alt=""  class="hover" border="0" height="auto" width="18">
												</a>
												&nbsp;
												<a href="https://twitter.com/salesneuron" style="text-decoration: none;" >
													<img src="http://rocketway.net/themebuilder/products/templates/destiny/images/footer_icon3.png" style="width: 18px; height:auto;" alt=""  class="hover" border="0" height="auto" width="18">
												</a>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table><!-- End Wrapper -->
						<!-- Space -->
						<table class="full" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td height="20" width="100%"></td>
							</tr>
						</table><!-- End Space -->
					</td>
				</tr>
			</table><!-- End Wrapper -->
		</div>
		</td>
	</tr>
</table><!-- Wrapper Footer -->
</div>
</body></html>	<style>body{ background: none !important; } </style>';
		$newbody = $body;
		$mail->IsHTML(true);
		$mail->MsgHTML($newbody);
		$mail->AddAddress(trim($email));
		$mail->AddBCC('arvind@aucventures.com');
		$mail->AltBody="This is text only alternative body.";
		if($mail->send()) {
			return 'success';
		} else {
			return 'fail';
		}
	}
	// load accountname without/with assigned
	public function loadaccountname() {
		$result=array();
		$data = $this->db->query("SELECT account.accountid, account.accountname FROM `account` WHERE account.status = 1 and account.accounttypeid = 16 GROUP BY account.accountid");
		$i=0;
		if($data->num_rows() > 0){
			foreach($data->result() as $user_name)
			{
				$result[]=array(
						'accountid' => $user_name->accountid,
						'accountname' => $user_name->accountname,
				);
				$i++;
			}
		}else{
			$result='';
		}
		echo json_encode($result);
	}
}
