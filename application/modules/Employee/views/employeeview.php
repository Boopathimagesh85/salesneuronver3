<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
	<?php 
		$industryid = $this->Basefunctions->industryid;
		if($industryid == 3) {		
	?>
	<link href="<?php echo base_url();?>css/jqgrid.min.css" media="screen" rel="stylesheet" type="text/css" />
	<?php 
	 }
	?>
	 <!-- To Load Date picker form fields placed above (DOB Format)-->
   <script src="<?php echo base_url();?>js/Employee/employee.js" type="text/javascript"></script> 
</head>
<body>
	<?php
		$moduleid = implode(',',$moduleids);
		$device = $this->Basefunctions->deviceinfo();
		$dataset['gridenable'] = "yes"; //no
		$dataset['griddisplayid'] = "employeegriddisplay";
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['spanattr'] = array();
		$dataset['gridtableid'] = "employeegrid";
		$dataset['griddivid'] = "employeegriddiv";
		$dataset['moduleid'] = $moduleids;
		$dataset['forminfo'] = array(array('id'=>'employeeform','class'=>'hidedisplay','formname'=>'employeeform'));
		if($device=='phone') {
			$this->load->view('Base/gridmenuheadermobile',$dataset);
			$this->load->view('Base/overlaymobile');
			$this->load->view('Base/viewselectionoverlay');
		} else {
			$this->load->view('Base/gridmenuheader',$dataset);
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
	?>
</body>
	<!-- js Files -->		
	<?php $this->load->view('Base/bottomscript'); ?>
	<!--For Tablet and Mobile view Dropdown Script-->
	<script>
		$(document).ready(function(){
			$("#tabgropdropdown").change(function(){
				var tabgpid = $("#tabgropdropdown").val(); 
				$('.sidebaricons[data-subform="'+tabgpid+'"]').trigger('click');
			});
		});
	</script>
</html>