<style type="text/css">
#s2id_salutationid {
position:relative;
top:-6px !important;
}
</style>

<div class="large-12 columns paddingzero formheader">
	<?php
		$this->load->view('Base/mainviewaddformheader');
	?>
</div>
<div class="large-12 columns addformunderheader">&nbsp;</div>
<div class="large-12 columns scrollbarclass addformcontainer tabtouchaddcontainer">
	<div class="row mblhidedisplay">&nbsp;</div>
	<form method="POST" name="dataaddform" class="" action ="" id="dataaddform">
		<span id="formaddwizard" class="validationEngineContainer" >
			<span id="formeditwizard" class="validationEngineContainer">
				<?php
					//function call for form fields generation
					formfieldstemplategenerator($modtabgrp);
                ?>
			</span>
		</span>
		<!--hidden values-->
		<?php
			echo hidden('primarydataid','');
			echo hidden('sortorder','');
			echo hidden('sortcolumn','');
			$value = 'employeeaddress';
			echo hidden('resctable',$value);
			$modid = $this->Basefunctions->getmoduleid($filedmodids);
			echo hidden('viewfieldids',$modid);
			echo hidden('defaultstateid',$defaultstateid);
			echo hidden('defaultcountryid',$defaultcountryid);
		?>
	</form>
</div>