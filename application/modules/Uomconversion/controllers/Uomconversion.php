<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Uomconversion extends MX_Controller{	
    private $uomconversionmodule = 261;
	public function __construct() {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Uomconversion/Uomconversionmodel');	
    }
    //first basic hitting view
    public function index() {        
    	$moduleid = array(261);
    	sessionchecker($moduleid);
		//action
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(261);
		$viewmoduleid = array(261);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
      	$this->load->view('Uomconversion/uomconversionview',$data);
	}
	//create uom conversion factor
	public function newdatacreate() {  
    	$this->Uomconversionmodel->newdatacreatemodel();
    }
	//retrieve uom conversion data fetch
	public function fetchformdataeditdetails() {
		$this->Uomconversionmodel->informationfetchmodel($this->uomconversionmodule);
	}
	//update uom conversion factor
    public function datainformationupdate() {
        $this->Uomconversionmodel->datainformationupdatemodel();
    }
	//delete uom conversion factor
    public function deleteinformationdata() {
        $this->Uomconversionmodel->deleteoldinformation($this->uomconversionmodule);
    } 
}