<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Uomconversionmodel extends CI_Model{
    private $uomconversionmodule = 216;
	public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//uomconversion data
	public function newdatacreatemodel() {
		//table and fields information	
		$formfieldsname = explode(',',$_POST['uomconversionelementsname']);
		$formfieldstable = explode(',',$_POST['uomconversionelementstable']);
		$formfieldscolmname = explode(',',$_POST['uomconversionelementscolmn']);
		$elementpartable = explode(',',$_POST['uomconversionelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		//check the rate exists already.
		$id=$this->db->select('uomconversionid')
		->from('uomconversion')
		->where(array('uomid'=>$_POST['uomid'],'uomtoid'=>$_POST['uomtoid'],'status'=>$this->Basefunctions->activestatus))
		->limit(1)
		->get();
		if($id->num_rows() > 0) {
			//if records exits previously update the records
			foreach($id->result() as $info){
				$conversionid= $info->uomconversionid;
			}
			$value_array=array('conversionrate'=>trim($_POST['conversionrate']),'lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid);
			$this->db->where('uomconversionid',$conversionid);
			$this->db->update('uomconversion',$value_array);
		}
		else{
			$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
		}
		echo 'TRUE';
	}
	//uomconversion data fetch
	public function informationfetchmodel()	{
		//table and fields information
		$formfieldsname = explode(',',$_GET['uomconversionelementsname']);
		$formfieldstable = explode(',',$_GET['uomconversionelementstable']);
		$formfieldscolmname = explode(',',$_GET['uomconversionelementscolmn']);
		$elementpartable = explode(',',$_GET['uomconversionelementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['uomconversionprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$this->uomconversionmodule);
		echo $result;
	}
	//uomconversion update
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['uomconversionelementsname']);
		$formfieldstable = explode(',',$_POST['uomconversionelementstable']);
		$formfieldscolmname = explode(',',$_POST['uomconversionelementscolmn']);
		$elementpartable = explode(',',$_POST['uomconversionelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['uomconversionprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		$uomconvprimaryid = $_POST['uomconversionprimarydataid'];
		$uomid = $_POST['uomid'];
		$uomtoid = $_POST['uomtoid'];
		$ludate = date($this->Basefunctions->datef);
		$luuserid= $this->Basefunctions->userid;
		$delete=array('status'=>0,'lastupdatedate'=>$ludate,'lastupdateuserid'=>$luuserid);
		$this->db->where_in('uomid',$uomid);
		$this->db->where_in('uomtoid',$uomtoid);
		$this->db->where_not_in('uomconversionid',$uomconvprimaryid);
		$this->db->update('uomconversion',$delete);
		echo $result;
	}
	//uomconversion delete
	public function deleteoldinformation($moduleid)	{
		$formfieldstable = explode(',',$_GET['uomconversionelementstable']);
		$parenttable = explode(',',$_GET['uomconversionparenttable']);
		$id = $_GET['uomconversionprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($this->uomconversionmodule);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				echo "TRUE";
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			echo "TRUE";
		}
	} 	
}