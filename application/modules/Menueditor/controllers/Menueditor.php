<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Menueditor extends CI_Controller {
	function __construct() {
    	parent::__construct();	
		$this->load->model('Menueditor/Menueditormodel');
		$this->load->model('Base/Basefunctions');
		$this->load->helper('formbuild');
    }
	public function index() {
		$moduleid = array(243);
		sessionchecker($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['menucate'] = $this->Basefunctions->simpledropdown('menucategory','menucategoryid,menucategoryname','menucategoryname');
		$data['modname'] = $this->Basefunctions->simpledropdownwithcond('moduleid','menuname','module','menutype','internal');
		$this->load->view('Menueditor/menueditorview',$data);
	}
	//data row sorting
	public function datarowsorting() {
		$this->Menueditormodel->datarowsortingmodel();
	}
	//unique name check
	public function uniquenamecheck() {
		$this->Menueditormodel->uniquenamecheckmodel();
	}
	//menu category creation
	public function newmenucategorycreate() {  
    	$this->Menueditormodel->newmenucategorycreatemodel();
    }
	//menu category data view
	public function newmenucategorygriddatafetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'b.sortorder') : 'b.sortorder';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'ASC') : 'ASC';
		$colinfo = array('colmodelname'=>array('parentmenucategoryname','menucategoryname','description','statusname','status'),'colmodelindex'=>array('menucategory.parentmenucategoryname','menucategory.menucategoryname','menucategory.description','status.statusname','menucategory.status'),'coltablename'=>array('menucategory','menucategory','menucategory','status','menucategory'),'uitype'=>array('2','2','2','2','2'),'colname'=>array('Parent Category','Category Name','Description','Status','Status Id'),'colsize'=>array('150','150','250','50','5'));
		$result=$this->Menueditormodel->newmenucategorydatafetchmodel($primarytable,$sortcol,$sortord,$pagenum,$rowscount);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($result,$page,'');
		$device = $this->Basefunctions->deviceinfo();
		//if($device=='phone') {
			//$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Menucategory List',$width,$height);
		//} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Menucategory List',$width,$height);
		//}
		echo json_encode($datas);
	}
	//menu category form data information fetch
	public function fetchmenucateformdatadetails() {
		$moduleid = 243;
		$this->Menueditormodel->fetchmenucateformdatadetailsmodel($moduleid);
	}
	//menu category update
    public function menucategorydataupdate() {
        $this->Menueditormodel->menucategorydataupdatemodel();
    }
	//parent menu category fetch
	public function parentaccountnamefetch() {
		$this->Menueditormodel->parentaccountnamefetchmodel();
	}
	//parent menu name fetch
	public function parentmenunamefetch() {
		$this->Menueditormodel->parentmenunamefetchmodel();
	}
	//delete menu category
    public function deletemenucategorydata() {
		$moduleid = 243;
        $this->Menueditormodel->deletemenucategorydatamodel($moduleid);
    }
	//enable menu category
    public function enablemenucategorydata() {
        $this->Menueditormodel->enablemenucategorydatamodel();
    }
	//menu list data view
	public function newmenulistgriddatafetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'module.sortorder') : 'module.sortorder';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'ASC') : 'ASC';
		$colinfo = array('colmodelname'=>array('parentmodulename','menutype','newmodulename','menuname','modulelink','statusname','status'),'colmodelindex'=>array('parentmodule.modulename','module.menutype','newmodule.modulename','module.menuname','module.modulelink','status.statusname','moduleinfo.status'),'coltablename'=>array('menucategory','module','module','module','module','status','status'),'uitype'=>array('19','17','19','2','2','2','2'),'colname'=>array('Category Name','Menu Type','Module Name','Menu Name','Hyper Link','Status','Status Id'),'colsize'=>array('180','150','200','200','200','100','10'));
		$finalresult=$this->Menueditormodel->newmenulistgriddatafetchmodel($primarytable,$sortcol,$sortord,$pagenum,$rowscount);
		$device = $this->Basefunctions->deviceinfo();
//	if($device=='phone') {
	//		$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Menu List',$width,$height);
	//	} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Menu List',$width,$height);
		//}
		echo json_encode($datas);
	}
	//menu list add function
	public function newmenulistcreate() {
		$this->Menueditormodel->newmenulistcreatemodel();
	}
	//menu list form data information fetch
	public function fetchmenulistformdatadetails() {
		$moduleid = 243;
		$this->Menueditormodel->fetchmenulistformdatadetailsmodel($moduleid);
	}
	//menu category update
    public function menulistdataupdate() {
        $this->Menueditormodel->menulistdataupdatemodel();
    }
	//delete menu list
    public function deletemenulistdata() {
		$moduleid = 243;
        $this->Menueditormodel->deletemenulistdatamodel($moduleid);
    }
    //enable the menu liat
    public function enablemenulistdata(){
    	$moduleid = 243;
    	$this->Menueditormodel->enablemenulistdatamodel($moduleid);
    }
}