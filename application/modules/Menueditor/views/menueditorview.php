<!DOCTYPE HTML>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
</head>
<body class="hidedisplay">
	<?php
		$this->load->view('menueditorform'); 
	/* $this->load->view('menueditorbasedeleteform'); */
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$this->load->view('Base/overlaymobile');
		} else {
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
	?>
</body>
	<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js File -->
	<script src="<?php echo base_url();?>js/Menueditor/menueditor.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/sortable.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/treemenu/modernizr.custom.min.js" type="text/javascript"></script>  
	<script src="<?php echo base_url();?>js/plugins/treemenu/jquery.dlmenu.js" type="text/javascript"></script> 
</html>