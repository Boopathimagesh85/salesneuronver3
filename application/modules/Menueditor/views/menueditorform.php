<style type="text/css">
	#menueditoradvanceddrop{
		left: -82.1666px !important;
	}
	.gridcontent {
	height: 77vh !important;
	}
	#menuaddgridfooter {
		position: relative;
    top: 340px !important;
		
	}
</style>
<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<span class="gridviewdivforsh">
			<?php
				$device = $this->Basefunctions->deviceinfo();
				$dataset['gridtitle'] = $gridtitle['title'];
				$dataset['titleicon'] = $gridtitle['titleicon'];
				$dataset['moduleid'] = '243';
				$dataset['action'][0] = array('actionid'=>'menuaddicon','actiontitle'=>'Add','actionmore'=>'Yes','ulname'=>'menueditor');
				$this->load->view('Base/formwithgridmenuheader.php',$dataset);
			?>
			<div class="large-12 columns addformunderheader">&nbsp;</div>
			<div class="large-12 columns scrollbarclass addformcontainer tabtouchmenuviewcontainer">
				<div class="row mblhidedisplay">&nbsp;</div>
				<!-- More Action DD start -- Gowtham -->
					<ul id="menueditoradvanceddrop" class="multinavaction-drop arrow_box" style="white-space: nowrap; position: absolute; top: 34.8px; left: -62.1666px; opacity: 1; display: none;">
						<li id="menuediticon"><span class="icon-box"><i class="material-icons editiconclass " title="Edit">edit</i>Edit</span></li>
						<li id="menudeleteicon"><span class="icon-box"><i class="material-icons" title="Disable">visibility_off</i>Disable</span></li>
						<li id="menuenableicon"><span class="icon-box"><i class="material-icons" title="Enable">visibility</i>Enable</span></li>
					</ul>
				<!-- More Action DD End -- Gowtham -->
				<div id="subformspan2" class="hiddensubform transitionhiddenform padding-space-open-for-form">
				<div class="closed effectbox-overlay effectbox-default" id="menusectionoverlay">
					<div class="large-12 columns mblnopadding" style="height:100% !important;">
					<form method="POST" name="menudataaddform" class="" action ="" id="menudataaddform" style="height:100% !important;">
						<span id="menuformaddwizard" class="validationEngineContainer" style="height:100% !important;">
							<span id="menuformeditwizard" class="validationEngineContainer" style="height:100% !important;">
								<div class="large-4 columns large-offset-8" style="padding-right:0px !important;height:100% !important;"> 
									<div class="large-12 columns cleardataform z-depth-5 border-modal-8px" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff">
										<div class="large-12 columns sectionheaderformcaptionstyle">Menu Details</div>
										<div class="large-12 columns sectionpanel">
										<div class="static-field large-12 columns">
											<label>Module Type <span class="mandatoryfildclass">*</span></label>
											<select class="chzn-select validate[required]" data-prompt-position="bottomLeft:14,36" id="moduletypeid" name="modulecategorytypeid" data-ddid="modulecategorytypeid">
												<option value="">Select</option>
												<option value="1">Module</option>
												<option value="2">Category</option>
											</select>
										</div>
										<div class="large-12  medium-12 small-12 columns categorytreediv" id="menucategorydivhid">
											<label>Parent Menu</label>
											<?php 
												$mandlab = "";
												$tablename = 'module' ;
												$mandatoryopt = ""; //mandatory option set
												echo divopen( array("id"=>"dl-menucategoryid","class"=>"dl-menuwrapper","style"=>"z-index:2;margin-bottom: 1rem") );
												$type="button";
												echo '<button name="treebutton" type="button" id="menucategorylistbutton" class="btn dl-trigger treemenubtnclick" tabindex="" style="height:2.2rem;color:#ffffff;font-size: 1.45rem;padding: 0.1rem;padding-top:0.3rem;background-color:#546E7A;"><i class="material-icons">format_indent_increase</i></button>';
												$txtoptions = array("style"=>"width:83%;display:inline;position:absolute;height:2.12rem;padding-left: 5px;","readonly"=>"readonly","tabindex"=>"","class"=>"");
												echo text('modulemenucategoryname','',$txtoptions);
												echo '<ul class="dl-menu maintreegenerate large-12 medium-6 small-12 column" id="menucategorylistuldata">';
												$dataset = $this->Menueditormodel->modulecategorytreecreatemodel($tablename);
												$this->Basefunctions->createTree($dataset,1);
												echo close('ul');
												echo close('div');
											?>							
											<input type="hidden" id="menucategoryid" name="menucategoryid" value="">	
										</div>
										<div class="static-field large-12 columns" id="menutypedivhid">
											<label>Menu Type</label>
											<select class="chzn-select" id="menutype" value="" name="menutype" data-prompt-position="bottomLeft" data-ddid="menutype">
												<option value="Internal">Internal</option>
												<option value="External">External</option>
											</select>
										</div>
										<div class="static-field large-12 columns" id="modulenamediv">
											<label>Module Name <span class="mandatoryfildclass">*</span></label>
											<select class="chzn-select" id="modulename" name="modulename" data-prompt-position="bottomLeft:14,36" data-ddid="modulename">
												<option value=""></option>
												<?php foreach($modname as $key):?>
													<option value="<?php echo $key->moduleid?>"><?php echo $key->menuname; ?> </option>
												<?php endforeach;?>
											</select>
										</div>
										<div class="input-field large-12 columns hidedisplay" id="hyperlinknamediv">
											<input id="modulelink" type="text" data-prompt-position="bottomLeft" tabindex="" class="validate[maxSize[150]]" name="modulelink" value="http://">
											<label for="modulelink">Hyper Link <span class="mandatoryfildclass">*</span></label>
										</div>
										<div class="input-field large-12 columns">
											<input id="menuname" type="text" data-prompt-position="bottomLeft" tabindex="" name="menuname" class="validate[required,funcCall[menunamecheck],maxSize[50]]">
											<label for="menuname">Menu Name <span class="mandatoryfildclass">*</span></label>
										</div>
										</div>
										<div class="divider large-12 columns"></div>
										<div class="large-12 columns submitkeyboard sectionalertbuttonarea" style="text-align: right">
										<input id="menusavebutton" class="alertbtnyes addkeyboard" type="button" value="Submit" name="menusavebutton">
										<input id="menudataupdatesubbtn" class="alertbtnyes updatekeyboard hidedisplay" type="button" value="Submit" name="menudataupdatesubbtn" style="display: none;">
										<input id="menucancelbutton" class="alertbtnno addsectionclose" type="button" value="Cancel" name="menucancelbutton">
										</div>
									</div>
								</div>
							</span>
						</span>
						<!-- hidden fields-->
						<input id="menulistelementsname" type="hidden" value="menucategoryid,menutype,modulename,modulelink,menuname,moduletypeid" name="menulistelementsname">
						<input id="menulistelementstable" type="hidden" value="module,module,module,module,module,module" name="menulistelementstable">
						<input id="menulistelementscolmn" type="hidden" value="parentmoduleid,menutype,moduleid,modulelink,menuname,moduletypeid" name="menulistelementscolmn">
						<input id="menulistelementspartabname" type="hidden" value="module" name="menulistelementspartabname">
						<input id="menulistprimarydataid" type="hidden" name="menulistprimarydataid">
						<input id="menulistacttype" type="hidden" name="menulistacttype" value="0">
					</form>
					</div>
					</div>
					<?php
						$device = $this->Basefunctions->deviceinfo();
						if($device=='phone') {
							echo '<div class="large-12 columns paddingbtm ">
									<div class="large-12 columns paddingzero ">
										<div class="large-12 columns viewgridcolorstyle" style="padding-left:0;padding-right:0;">
											<div class="large-12 columns headerformcaptionstyle" style=" height:auto; padding: 0.2rem 0 0rem;line-height:1.8rem;">
												<span class="large-6 medium-6 small-5 columns  lefttext">Menu List<span class="fwgpaging-box menulistgridfootercontainer"></span></span>
												<span class="large-6 medium-6 small-7 columns innergridicon righttext">
												<span id="menuaddicon" class="addiconclass" title="Add"  ><i class="material-icons">add</i></span>
													<span title="Edit" id="menuediticon" class="editiconclass"><i class="material-icons">edit</i></span>
													<span title="Disable" id="menudeleteicon" class=""><i class="material-icons">visibility_off
</i></span>
													<span id="menuenableicon" class="" title="Enable"><i class="material-icons">visibility
</i></span>
													<span title="Reload" id="menureloadicon"  class="reloadiconclass"><i class="material-icons">refresh
</i></span>
													<!-- <span title="Search" id="menusearchicon" class=""><i class="material-icons">search
</i></span> -->
												</span>
											</div>';
										echo '<div class="large-12 columns forgetinggridname" id="menuaddgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="menuaddgrid" style="max-width:2000px; height:1;top:0px;">
											<!-- Table header content , data rows & pagination for mobile-->
										</div>
										<footer class="inner-gridfooter  footercontainer" id="menuaddgridfooter">
											<!-- Footer & Pagination content -->
										</footer></div></div></div></div>';
						} else {
							echo '<div class="large-12 columns viewgridcolorstyle borderstyle" style="padding:0px;">
									<!--<div class="large-12 columns headerformcaptionstyle " style=" height:auto; padding: 0.2rem 0 0rem;line-height:1.8rem;">
										<span class="large-6 medium-6 small-12 columns  lefttext small-only-text-center">Menu List<span class="fwgpaging-box menulistgridfootercontainer"></span></span>
										<span class="large-6 medium-6 small-12 columns innergridicon righttext small-only-text-center">
										<span id="menuaddicon" class="addiconclass" title="Add"><i class="material-icons">add</i></span>
											<span title="Edit" id="menuediticon" class="editiconclass"><i class="material-icons">edit</i></span>
											<span title="Disable" id="menudeleteicon" class=""><i class="material-icons">visibility_off</i></span>
											<span id="menuenableicon" class="" title="Enable"><i class="material-icons">visibility</i></span>
											<span title="Reload" id="menureloadicon"  class="reloadiconclass"><i class="material-icons">refresh</i></span>
											<span title="Search" id="menusearchicon" class=""><i class="material-icons">search</i></span> 
										</span>
									</div>-->';
									echo '<div class="large-12 columns forgetinggridname" id="menuaddgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content  inner-gridcontent" id="menuaddgrid" style="max-width:2000px; height:450px;top:0px;">
									<!-- Table content[Header & Record Rows] for desktop-->
									</div>
									<div class="footer-content footercontainer" id="menuaddgridfooter">
										<!-- Footer & Pagination content -->
									</div>
								</div>
							</div>';
						}
					?>
				</div>
			</div>
			<span>
		</div>
	</div>
</div>
