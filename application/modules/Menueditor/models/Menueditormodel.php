<?php
Class Menueditormodel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->model('Base/Crudmodel');
    }
	//default values
	public function defaultvalueget() {
		$defdata['createdate'] = date($this->Basefunctions->datef);
		$defdata['lastupdatedate'] = date($this->Basefunctions->datef);
		$defdata['createuserid'] = $this->Basefunctions->userid;
		$defdata['lastupdateuserid'] = $this->Basefunctions->userid;
		$defdata['status'] = 1;
		return $defdata;
	}
	//default value set array for update
	public function updatedefaultvalueget() {
		$defdata['lastupdatedate'] = date($this->Basefunctions->datef);
		$defdata['lastupdateuserid'] = $this->Basefunctions->userid;
		return $defdata;
	}
	//unique name check model
	public function uniquenamecheckmodel() {
		$name = $_POST['fieldvalue'];
		$whfield = $_POST['whfield'];
		$table = $_POST['tablename'];
		$datafield = $_POST['fieldname'];
		$data = "False";
		if($name != "" ) {
			$result = $this->db->select($datafield)->from($table)->where($table.'.'.$whfield,$name)->where_not_in($table.'.status',array(0,3))->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					$data = $row->$datafield;
				}
			}
		}
		echo $data;
	}
	//data sort order
	public function datarowsortingmodel() {
		$rowids = $_POST['rowids'];
		$tablename = $_POST['tablename'];
		$tableid = $_POST['primaryid'];
		$sortfield = $_POST['sortfield'];
		$dataids = explode(',',$rowids);
		$i=1;
		foreach($dataids as $id) {
			$this->db->query("UPDATE"." ".$tablename." SET ".$tablename.".".$sortfield."=".$i." WHERE ".$tablename.".".$tableid."=".$id );
			$i++;
		}
		echo "TRUE";
	}
	//menu categor add
	public function newmenucategorycreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['menucategoryelementsname']);
		$formfieldstable = explode(',',$_POST['menucategoryelementstable']);
		$formfieldscolmname = explode(',',$_POST['menucategoryelementscolmn']);
		$categoryelementpartable = explode(',',$_POST['menucategoryelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($categoryelementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
		echo 'TRUE';
	}
	//Folder main grid view
	public function newmenucategorydatafetchmodel($tablename,$sortcol,$sortord,$pagenum,$rowscount) {
		$userroleid = $this->Basefunctions->userroleid;
		$industryid = $this->Basefunctions->industryid;
		$dataset = 'b.menucategoryid,a.menucategoryname as parentmenucategoryname,b.menucategoryname,b.description,status.statusname,b.status,b.sortorder';
		$join =' LEFT OUTER JOIN menucategory as a ON a.menucategoryid=b.parentmenucategoryid';
		$join .=' LEFT OUTER JOIN status ON status.status=b.status';
		$status = 'b.status NOT IN (0,3)';
		$groupby = '';
		$where = 'FIND_IN_SET("'.$industryid.'",b.industryid) >0';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		//query
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' as b '.$join.' WHERE '.$where.' AND '.$status.' '.$groupby.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		return $data;
	}
	//menu category form information fetch
	public function fetchmenucateformdatadetailsmodel($moduleid){
		//table and fields information
		$formfieldsname = explode(',',$_GET['menucategoryelementsname']);
		$formfieldstable = explode(',',$_GET['menucategoryelementstable']);
		$formfieldscolmname = explode(',',$_GET['menucategoryelementscolmn']);
		$categoryelementpartable = explode(',',$_GET['menucategoryelementspartabname']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($categoryelementpartable);
		$primaryid = $_GET['menucategoryprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	//menu category update model
	public function menucategorydataupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['menucategoryelementsname']);
		$formfieldstable = explode(',',$_POST['menucategoryelementstable']);
		$formfieldscolmname = explode(',',$_POST['menucategoryelementscolmn']);
		$categoryelementpartable = explode(',',$_POST['menucategoryelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($categoryelementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['menucategoryprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		echo $result;
	}
	//delete menu category
	public function deletemenucategorydatamodel($moduleid) {
		$primaryid = $_GET['primaryid'];
		$primayname = 'menucategoryid';
		$tablname = 'menucategory';
		$tablenames = explode(',',$tablname);
		$deldataarr = array(
			'status'=>2
		);
		$defdataarr = $this->updatedefaultvalueget();
		$newdata = array_merge($deldataarr,$defdataarr);
		foreach($tablenames as $table) {
			$this->db->where($primayname,$primaryid);
			$this->db->update($table,$newdata);
		}
		echo "TRUE";
	}
	//parent menu category fetch
	public function parentaccountnamefetchmodel() {
		$id=$_GET['id'];
		$data=$this->db->select('a.parentmenucategoryid as parentmenucategoryid,b.menucategoryname as menucategorynames')
				->from('menucategory as a')
				->join('menucategory as b','a.parentmenucategoryid=b.menucategoryid')
				->where('a.menucategoryid',$id)
				->where('b.status',1)
				->get();
		if($data->num_rows() >0) {
			foreach($data->result() as $in)	{
				$parentcategoryid=$in->parentmenucategoryid;
				$categoryname=$in->menucategorynames;
			}
			$a = array('parentcategoryid'=>$parentcategoryid,'categoryname'=>$categoryname);
		} else {
			$a = array('parentcategoryid'=>'0','categoryname'=>'');
		}
		echo json_encode($a);
	}
	//parent menu name fetch
	public function parentmenunamefetchmodel() {
		$id=$_GET['id'];
		$data=$this->db->select('a.parentmoduleid as pmenuname, b.menuname as menuname')
				->from('module as a')
				->join('module as b','b.moduleid=a.parentmoduleid','left outer')
				->where('a.parentmoduleid',$id)
				->where('a.status',1)
				->get();
		if($data->num_rows() >0) {
			foreach($data->result() as $in)	{
				$parentcategoryid=$in->pmenuname;
				$categoryname=$in->menuname;
			}
			$a = array('parentcategoryid'=>$parentcategoryid,'categoryname'=>$categoryname);
		} else {
			$a = array('parentcategoryid'=>'0','categoryname'=>'');
		}
		echo json_encode($a);
	}
	//enable menu category
	public function enablemenucategorydatamodel() {
		$primaryid = $_GET['primaryid'];
		$primayname = 'menucategoryid';
		$tablname = 'menucategory';
		$tablenames = explode(',',$tablname);
		$deldataarr = array(
			'status'=>1
		);
		$defdataarr = $this->updatedefaultvalueget();
		$newdata = array_merge($deldataarr,$defdataarr);
		foreach($tablenames as $table) {
			$this->db->where($primayname,$primaryid);
			$this->db->update($table,$newdata);
		}
		echo "TRUE";
	}
	//Folder main grid view
	public function newmenulistgriddatafetchmodel($tablename,$sortcol,$sortord,$pagenum,$rowscount) {
		$userrole = $this->Basefunctions->userroleid;
		$industryid = $this->Basefunctions->industryid;
		$result = array();
		$i = 0;
		$dataset = 'module.moduleid,parentmodule.modulename as parentmodulename,module.menutype,newmodule.modulename as newmodulename,module.menuname,module.modulelink,status.statusname,moduleinfo.status';
		$join=' LEFT OUTER JOIN module as newmodule ON newmodule.moduleid=module.moduleid ';
		$join.=' LEFT OUTER JOIN module as parentmodule ON parentmodule.moduleid=module.parentmoduleid ';
		$join.=' LEFT OUTER JOIN moduleinfo ON moduleinfo.moduleid=module.moduleid';
		$join.=' LEFT OUTER JOIN menucategory ON menucategory.menucategoryid=module.menucategoryid';
		$join.=' LEFT OUTER JOIN status ON status.status=moduleinfo.status';
		$join.=' LEFT OUTER JOIN userrole ON userrole.userroleid=moduleinfo.status';
		$status = $tablename.'.status IN (1,2) AND moduleinfo.status IN (1,2)';
		$wherecond = 'moduleinfo.userroleid = '.$userrole.' AND module.menuviewtype=1 AND FIND_IN_SET('.$industryid.',module.industryid) >0 ';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		/* query */ // .'module.sortorder ASC,' removed from query
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$wherecond.' AND '.$status.' ORDER BY '.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		foreach($data->result() as $row) {
			$result[$i]=array('id'=>$row->moduleid,$row->parentmodulename,$row->menutype,$row->newmodulename,$row->menuname,($row->menutype=='External')?$row->modulelink:'',$row->statusname,$row->status);
			$i++;
		}
		$finalresult=array($result,$page,'json');
		return $finalresult;
	}
	//menu list create model
	public function newmenulistcreatemodel() {
		if(isset($_POST['modulecategorytypeid'])){
			$modulecategorytypeid = $_POST['modulecategorytypeid'];
			if($modulecategorytypeid == '2') {
				$moduletype = 2;
			} else {
				$moduletype = 1;
			}
		} else {
			$modulecategorytypeid =1;
			$moduletype = 1;
		}
		if(isset($_POST['menucategoryid'])){
			$menucatname = $_POST['menucategoryid'];
			if($menucatname == ''){
				$menucatname = 0;
			}
		} else {
			$menucatname =0;
		}
		if(isset($_POST['menutype'])){
			$menutype = $_POST['menutype'];
			if($menutype == ''){
				$menutype = 'Internal';
			}
		} else {
			$menutype ='Internal';
		}
		if(isset($_POST['modulename'])){
			$moduleid = $_POST['modulename'];
			if($moduleid == ''){
				$moduleid = '1';
			}
		} else {
			$moduleid = '1';
		}
		$modulelink = $_POST['modulelink'];
		$menuname = $_POST['menuname'];
		if($menutype == 'External') {
			$this->modulegen($menuname,$modulelink,$menucatname,$menutype,$modulecategorytypeid,$moduletype);
			$this->viewmodulegen($menuname);
		} else if($menutype == 'Internal') {
			if($moduleid != 1) {
				$updataarr = array(
					'menuname'=>ucwords($menuname),
					'modulecategorytypeid'=>$modulecategorytypeid,
					'parentmoduleid'=>$menucatname,
					'menutype'=>$menutype
				);
				$defdataarr = $this->updatedefaultvalueget();
				$newdata = array_merge($updataarr,$defdataarr);
				//update information
				$this->db->where('moduleid',$moduleid);
				$this->db->update('module',$newdata);
			} else {
				$this->modulegen($menuname,$modulelink,$menucatname,$menutype,$modulecategorytypeid,$moduletype);
				$this->viewmodulegen($menuname);
			}
		}
		echo "TRUE";
	}
	//module generate
	public function modulegen($modulename,$link,$menucatname,$menutype,$modulecategorytypeid,$moduletype) {
		$moduleid=0;
		if($modulename!="") {
			$dataarr = array(
				'modulename'=>ucwords($modulename),
				'menuname'=>ucwords($modulename),
				'modulecategorytypeid'=>$modulecategorytypeid,
				'menutype'=>$menutype,
				'modulelink'=>$link,
				'moduledashboardid'=>'',
				'modulemastertable'=>'',
				'moduletypeid'=>$moduletype,
				'parentmoduleid'=>$menucatname,
				'industryid'=>$this->Basefunctions->industryid
			);
			$defdataarr = $this->defaultvalueget();
			$newdata = array_merge($dataarr,$defdataarr);
			$this->db->insert( 'module', $newdata);
			$moduleid = $this->db->insert_id();
			$datainfo = array(
				'moduleid'=>$moduleid,
				'userroleid'=>$this->Basefunctions->userroleid
			);
			$newdatainfo = array_merge($datainfo,$defdataarr);
			$this->db->insert( 'moduleinfo', $newdatainfo );
			return $moduleid;
		} else {
			return $moduleid;
		}
	}
	//dynamic view module creation
	public function viewmodulegen($modulename) {
		$viewmoduleid=0;
		if($modulename!="") {
			$dataarr = array(
				'viewcreationmodulename'=>ucwords($modulename),
				'status'=>1
			);
			$this->db->insert( 'viewcreationmodule', $dataarr );
			$viewmoduleid = $this->db->insert_id();
			return $viewmoduleid;
		}
	}
	//menu list form information fetch
	public function fetchmenulistformdatadetailsmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['menulistelementsname']);
		$formfieldstable = explode(',',$_GET['menulistelementstable']);
		$formfieldscolmname = explode(',',$_GET['menulistelementscolmn']);
		$categoryelementpartable = explode(',',$_GET['menulistelementspartabname']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($categoryelementpartable);
		$primaryid = $_GET['menulistprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	//menu list create model
	public function menulistdataupdatemodel() {
		if(isset($_POST['modulecategorytypeid'])){
			$modulecategorytypeid = $_POST['modulecategorytypeid'];
			if($modulecategorytypeid == '2') {
				$moduletype = 2;
			} else {
				$moduletype = 1;
			}
		} else {
			$modulecategorytypeid =1;
			$moduletype = 1;
		}
		if(isset($_POST['menucategoryid'])){
			$menucatname = $_POST['menucategoryid'];
			if($menucatname == ''){
				$menucatname = 0;
			}
		} else {
			$menucatname =0;
		}
		$menutype = $_POST['menutype'];
		$modulelink = $_POST['modulelink'];
		$menuname = $_POST['menuname'];
		$primaryid = $_POST['menulistprimarydataid'];
		if($menutype == 'External') {
			$updataarr = array(
				'modulename'=>ucwords($menuname),
				'menuname'=>ucwords($menuname),
				'modulecategorytypeid'=>$modulecategorytypeid,
				'parentmoduleid'=>$menucatname,
				'menutype'=>$menutype,
				'modulelink'=>$modulelink,
				'moduletypeid'=>$moduletype
			);
		} else if($menutype == 'Internal') {
			$updataarr = array(
				'menuname'=>ucwords($menuname),
				'modulecategorytypeid'=>$modulecategorytypeid,
				'menutype'=>$menutype,
				'parentmoduleid'=>$menucatname
			);
		}
		$defdataarr = $this->updatedefaultvalueget();
		$newdata = array_merge($updataarr,$defdataarr);
		//update information
		$this->db->where('moduleid',$primaryid);
		$this->db->update('module',$newdata);
		echo "TRUE";
	}
	//delete menu list
	public function deletemenulistdatamodel($moduleid) {
		$primaryid = $_GET['primaryid'];
		$userroleid = $this->Basefunctions->userroleid;
		$primaryname = 'moduleid';
		$tablname = 'moduleinfo';
		$tablenames = explode(',',$tablname);
		$deldataarr = array(
			'status'=>2
		);
		$defdataarr = $this->updatedefaultvalueget();
		$newdata = array_merge($deldataarr,$defdataarr);
		foreach($tablenames as $table) {
			if($table == 'moduleinfo') {
				$this->db->where('userroleid',$userroleid);
			}
			$this->db->where($primaryname,$primaryid);
			$this->db->update($table,$newdata);
		}
		echo "TRUE";
	}
	//enable menu list
	public function enablemenulistdatamodel($moduleid) {
		$primaryid = $_GET['primaryid'];
		$userroleid = $this->Basefunctions->userroleid;
		$primaryname = 'moduleid';
		$tablname = 'moduleinfo';
		$tablenames = explode(',',$tablname);
		$deldataarr = array(
				'status'=>1
		);
		$defdataarr = $this->updatedefaultvalueget();
		$newdata = array_merge($deldataarr,$defdataarr);
		foreach($tablenames as $table) {
			if($table == 'moduleinfo') {
				$this->db->where('userroleid',$userroleid);
			}
			$this->db->where($primaryname,$primaryid);
			$this->db->update($table,$newdata);
		}
		echo "TRUE";
	}
	public function modulecategorytreecreatemodel($tablename)
	{
		$id = $tablename.'id';
		$name = $tablename.'name';
		$pdataid = 'parent'.$tablename.'id';
		$level = $tablename.'level';
		$data = array();
		$this->db->select( ''.$id.' AS Id'.','.''.$name.' AS Name'.','.$pdataid.' AS ParentId'.','.$level.' AS level' );
		$this->db->from($tablename);
		$this->db->where(''.$tablename.'.status',1);
		$this->db->where(''.$tablename.'.modulecategorytypeid',2);
		$this->db->order_by(''.$tablename.'.'.$id.'','ASC');
		$result = $this->db->get();
		foreach($result->result() as $row) {
			$data[$row->Id] = array('id'=>$row->Id,'name'=>$row->Name,'pid'=>$row->ParentId,'level'=>$row->level);
		}
		$itemsByParent = array();
		foreach ($data as $item){
			if (!isset($itemsByParent[$item['pid']])) {
				$itemsByParent[$item['pid']] = array();
			}
			$itemsByParent[$item['pid']][] = $item;
		}//
		return $itemsByParent;
	}
}
?>