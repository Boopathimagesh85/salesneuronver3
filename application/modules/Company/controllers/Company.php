<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Company extends MX_Controller {
    //Constructor Function To Load Models
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Company/Companymodel');
		$this->load->model('Itemtag/Itemtagmodel');
		$this->load->view('Base/formfieldgeneration');
	}
	//Default View 
	public function index() {
		$moduleid = array(2);
		sessionchecker($moduleid);
		//action
		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$industryid = $this->Basefunctions->industryid;
		if($industryid == 3) { // company setting
			$data['tagtype'] = $this->Basefunctions->simpledropdownwithcondorder('tagtypeid','tagtypename','tagtype','sortorder','1,2,3,4','tagtypeid');
			$data['salestransactiontype'] = $this->Basefunctions->salestransactiontypedropdown('salestransactiontypeid','salestransactiontypename','salestransactiontype','','');
			$data['metaldata'] = $this->Basefunctions->simplegroupdropdown('metalid','metalname','metal','','');
			$data['calculationby'] = $this->Basefunctions->simplegroupdropdown('calculationbyid','calculationbyname','calculationby','','');
			$data['salesmode'] = $this->Basefunctions->simpledropdownwithcondorder('salesmodeid','salesmodename','salesmode','','','salesmodeid');
			$data['printtemplate'] = $this->Basefunctions->simplegroupdropdown('printtemplatesid','printtemplatesname','printtemplates','','');
			$data['stonecalctype']=$this->Basefunctions->simpledropdown_var('stonecalctype','stonecalctypeid,netwtcalctype,mccalctype','stonecalctypeid');
			$data['purity']=$this->Basefunctions->purity_groupdropdown();
			$data['primarypurity']=$this->Basefunctions->primarypurity_groupdropdown();
			$data['nonprimarypurity']=$this->Basefunctions->nonprimarypurity_groupdropdown();
			$data['transactionmode'] = $this->Basefunctions->simpledropdown('transactionmode','transactionmodeid,transactionmodename','transactionmodeid');
			$data['additionalcharge'] = $this->Basefunctions->simpledropdown('additionalcharge','additionalchargeid,additionalchargename','additionalchargeid');
			$data['accountgroup'] = $this->Basefunctions->simpledropdown('accountgroup','accountgroupid,accountgroupname','accountgroupid');
			$data['displayfields'] = $this->Basefunctions->simpledropdown('salesfields','salesfieldsid,salesfieldsname','salesfieldsid');
			$data['olditemdetails'] = $this->Basefunctions->simpledropdown('olditemdetails','olditemdetailsid,olditemdetailsname','olditemdetailsid');
			$data['netweightcalc'] = $this->Basefunctions->simpledropdown('netweightcalculation','netweightcalculationid,netweightcalculationname','netweightcalculationid');
			$data['ratecalculation'] = $this->Basefunctions->simpledropdown('ratecalculation','ratecalculationid,ratecalculationname','ratecalculationid');
			$data['calctype'] = $this->Basefunctions->simpledropdown('calculationtype','calculationtypeid,calculationtypename','calculationtypeid');
			$data['discountmode'] = $this->Basefunctions->simpledropdown('discountmode','discountmodeid,discountmodename','discountmodeid');
			$data['taxtype'] = $this->Basefunctions->simpledropdownwithcond('taxtypeid','taxtypename','taxtype','moduleid',2);
			$data['stocktype'] = $this->Basefunctions->stocktypedropdown();
			$data['charge']=$this->Basefunctions->charge_groupdropdown();
			$data['salesloosecharge']=$this->Companymodel->charge_groupdropdown();
			$data['purchaseloosecharge']=$this->Companymodel->purchasecharge_groupdropdown();
			$data['defaultchargeid']=$this->Basefunctions->default_charge_groupdropdown();
			$data['purchasecharge']=$this->Basefunctions->purchasecharge_groupdropdown();
			$data['chituserroleauth']=$this->Basefunctions->simpledropdown('userrole','userroleid,userrolename','userroleid');
			$data['itemtagauthrole']=$this->Basefunctions->simpledropdown('userrole','userroleid,userrolename','userroleid');
			$data['salesauthuserrole']=$this->Basefunctions->simpledropdown('userrole','userroleid,userrolename','userroleid');
			$data['tax']=$this->Basefunctions->simpledropdown('taxmaster','taxmasterid,taxmastername','taxmasterid');
			$data['oldjeweltax']=$this->Basefunctions->simpledropdownwithcond('taxid','taxname','tax','transactionmethodid','8,9');
			$data['billingtype']=$this->Basefunctions->simpledropdown('billingtype','billingtypeid,billingtypename','billingtypeid');
			$data['salesreturntype']=$this->Basefunctions->simpledropdown('salesreturntype','salesreturntypeid,salesreturntypename','salesreturntypeid');
			//$data['defaultbank']=$this->Basefunctions->simpledropdownwithcond('accountid','accountname,accountcatalogid','account','accountcatalogid','7,9,10');
			$data['defaultbank']=$this->Basefunctions->simpledropdownwithcond('accountid','accountname','account','accounttypeid','18');
			//$data['defaultcard']=$this->Basefunctions->simpledropdownwithcond('accountid','accountname','account','accounttypeid','19');
			$data['defaultcash']=$this->Basefunctions->simpledropdownwithcond('accountid','accountname','account','accounttypeid','19');
			$data['product'] = $this->Itemtagmodel->product_dd();
			$data['counter'] = $this->Basefunctions->counter_groupdropdown();
			$data['category'] = $this->Basefunctions->simpledropdown('category','categoryid,categoryname','categoryid');
			$data['chargeaccountgroup']=$this->Basefunctions->simpledropdownwithcond('accountgroupid','accountgroupname','accountgroup','accounttypeid','6');
			$data['productidshow']=$this->Basefunctions->get_company_settings('productidshow');
			$data['chargedataid']=$this->Basefunctions->get_company_settings('businesschargeid');
			$data['purchasechargedataid']=$this->Basefunctions->get_company_settings('businesspurchasechargeid');
			$data['gstapplicable']=$this->Basefunctions->get_company_settings('gstapplicable');
			$data['lottype'] = $this->Basefunctions->simpledropdown('lottype','lottypeid,lottypename','lottypeid');
			$data['recurrenceday'] = $this->Basefunctions->simpledropdownwithcond('recurrencedayid','recurrencedayname','recurrenceday','moduleid','2');
			$data['weight_round'] =$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2);//weight round-off
			$data['dia_weight_round'] =$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',8);
		}
		//view
		$viewfieldsmoduleids = array(2);
		$viewmoduleid = array(2);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Company/companyview',$data);	
	}
	//create company
	public function newdatacreate() {  
    	$this->Companymodel->newdatacreatemodel();
    }
	//information fetch
	public function fetchformdataeditdetails() {
		$moduleid = array(2);
		$this->Companymodel->informationfetchmodel($moduleid);
	}
	//update company
    public function datainformationupdate() {
        $this->Companymodel->datainformationupdatemodel();
    }
	//company count fetch
	public function companycountfetch() {
		$this->Companymodel->companycountfetchmodel();
	}
	public function companysettingupdate(){
		$this->Companymodel->companysettingupdate();
	}
	public function getcompanydata(){
		$this->Companymodel->getcompanydata();
	}
	public function loadtransactiontype(){
		$this->Companymodel->loadtransactiontype();
	}
	/**	*Company Setting => Transaction  creation	*/
    public function transactionsetting() 
    {  
    	$this->Companymodel->transactionsetting();
    }
    //old jewel settings create
    public function oldjewelsettingcreate() {
    	$this->Companymodel->oldjewelsettingcreatemodels();
    }
    //old jewel delete
    public function oldjeweldetailsdelete() {
    	$this->Companymodel->oldjeweldetailsdeletemodels();
    }
    //old jewel data retrive
    public function oldjewelsettingretrive(){
    	$this->Companymodel->oldjewelsettingretrivemodel();
    }
	/**	*Company Setting =>Transaction Delete	*/
    public function transactionsettingdelete() 
    {  
    	$this->Companymodel->transactionsettingdelete();
    }
	/**	*Company Setting =>Transaction retrieve	*/
    public function transactionsettingretrive() 
    {  
    	$this->Companymodel->transactionsettingretrive();
    }
	/**	*Company Setting =>Transaction type based load Other fields value	*/
    public function autoloadfieldvalues() 
    {  
    	$this->Companymodel->autoloadfieldvalues();
    }
	public function getproductchargerow() {  
    	$this->Companymodel->getproductchargerow();
    }
	// Check for updates
	public function checkforupdates() {  
    	$this->Companymodel->checkforupdates();
    }
	// Check for AMC details
	public function checkamcdetails() {  
    	$this->Companymodel->checkamcdetails();
    }
	// Check for Verification code and datetime details
	public function checkverificationcodeanddatetime() {  
    	$this->Companymodel->checkverificationcodeanddatetime();
    }
	// Rate Calculation settings create
    public function ratecalcsettingcreate() {
    	$this->Companymodel->ratecalcsettingcreatemodels();
    }
	// Rate Calculation settings retrieve
    public function ratecalcsettingretrive() {
    	$this->Companymodel->ratecalcsettingretrivemodel();
    }
	// Rate Calculation delete
    public function ratecalcdetailsdelete() {
    	$this->Companymodel->ratecalcdetailsdeletemodels();
    }
	// Success Email To Customer for software upgradation
    public function updatesuccessemail() {
    	$this->Companymodel->updatesuccessemail();
    }
}