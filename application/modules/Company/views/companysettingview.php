<style type="text/css">
#transactiongridfooter .rppdropdown {
	left:62% !important;
}
#oldjewelgridfooter .rppdropdown {
	left:62% !important;
}
#ratecalcgridfooter .rppdropdown {
	left:62% !important;
}
</style>

<?php
	$device = $this->Basefunctions->deviceinfo();
	$dataset['gridtitle'] = 'Company Settings';
	$dataset['titleicon'] = 'material-icons settings_applications';
	$modtabgroup[0] = array("tabpgrpid"=>"1","tabgrpname"=>"Customer","moduleid"=>"company","templateid"=>"2");
	$modtabgroup[1] = array("tabpgrpid"=>"2","tabgrpname"=>"General","moduleid"=>"company","templateid"=>"2");
	$modtabgroup[2] = array("tabpgrpid"=>"2","tabgrpname"=>"Masters","moduleid"=>"company","templateid"=>"2");
	$modtabgroup[3] = array("tabpgrpid"=>"2","tabgrpname"=>"Stock Entry","moduleid"=>"company","templateid"=>"2");
	$modtabgroup[4] = array("tabpgrpid"=>"3","tabgrpname"=>"Transaction Basic","moduleid"=>"company","templateid"=>"2");
	$modtabgroup[5] = array("tabpgrpid"=>"3","tabgrpname"=>"Transaction Concept","moduleid"=>"company","templateid"=>"2");
	$modtabgroup[6] = array("tabpgrpid"=>"4","tabgrpname"=>"Transaction","moduleid"=>"company","templateid"=>"2");
	$modtabgroup[7] = array("tabpgrpid"=>"5","tabgrpname"=>"Old jewel Calculations","moduleid"=>"company","templateid"=>"2");
	$modtabgroup[8] = array("tabpgrpid"=>"6","tabgrpname"=>"Rate Calculations","moduleid"=>"company","templateid"=>"2");
	$dataset['modtabgrp'] = $modtabgroup;
	$this->load->view('Base/mainformwithgridmenuheader.php',$dataset);
?>
<!----Company setting overlay----->
<div class="" id="companyoverlay" style="z-index:40">		
	<div class="large-12 medium-12 small-12 large-centered medium-centered small-centered columns paddingzero">
		<div class="large-12 columns addformunderheader">&nbsp;</div>
		<div class="large-12 column paddingzero addformcontainer"  style="">
			<div class="row mblhidedisplay">&nbsp;</div>
			<form id="companysettingform" name="companysettingform" class="">
				<div id="companysettingvalidate" class="validationEngineContainer ">
					<div id="overlaysubformspan1" class="hiddensubform transitionhiddenform">
						<div class="large-4 columns end paddingbtm">
							<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
								<div class="large-12 columns headerformcaptionstyle">Round And Level Details</div>
								<div class="static-field large-6 columns">
									<label data-description="Maximum level of decimal for Weight">Weight Round<span class="mandatoryfildclass">*</span></label>
									<select id="weightdecisize" name="weightdecisize" data-validation-engine="validate[required]" class="chzn-select"  data-prompt-position="topLeft:14,36" data-placeholder="select" >
									<option value="1">0.0</option>
									<option value="2">0.00</option>
									<option value="3">0.000</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Maximum level of decimal for Melting">Melting Round<span class="mandatoryfildclass">*</span></label>
										<select data-validation-engine="validate[required]" class="chzn-select" id="meltdecisize" name="meltdecisize" data-prompt-position="topLeft:14,36">
											<option value="1">0.0</option>
											<option value="2">0.00</option>
											<option value="3">0.000</option>
										</select>
								</div>
								<div class="static-field large-6 columns">
										<label data-description="Maximum level of decimal for Amount">Amount Round<span class="mandatoryfildclass">*</span></label>
										<select data-validation-engine="validate[required]" class="chzn-select" id="amountround" name="amountround" data-prompt-position="topLeft:14,36">
												<option value="0">0</option>
												<option value="1">0.0</option>
												<option value="2">0.00</option>
												<option value="3">0.000</option>
												<option value="4">0.0000</option>
												<option value="5">0.00000</option>
										</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Maximum level of decimal for Rate">Rate Round<span class="mandatoryfildclass">*</span></label>
									<select data-validation-engine="validate[required]" class="chzn-select" id="rateround" name="rateround" data-prompt-position="topLeft:14,36">
											<option value="0">0</option>
											<option value="1">0.0</option>
											<option value="2">0.00</option>
											<option value="3">0.000</option>
											<option value="4">0.0000</option>
											<option value="5">0.00000</option>
									</select>
								</div>
								<div class="input-field large-6 medium-6 small-6 columns" id="lockingtimedivhid">
									<label for="lockingtime" class="" data-description="At what time the sales transaction time close today">Locking Time</label>
									<input name="lockingtime" id="lockingtime" class="fortimepicicon ui-timepicker-input" tabindex="149" data-prompt-position="bottomLeft" autocomplete="off" type="text">
								</div>
								<div class="input-field large-6 medium-6 small-6 columns" id="updatingtimedivhid">
									<label for="updatingtime" class="" data-description="At what time should update after locking time">Update Time</label>
									<input name="updatingtime" id="updatingtime" class="fortimepicicon ui-timepicker-input" tabindex="149" data-prompt-position="bottomLeft" autocomplete="off" type="text">
								</div>
								<div class="static-field large-12 columns">
									<label data-description="Updation date & time will be taken after the holidays.">Holidays</label>
									<select class="chzn-select" id="recurrencedayid" name="recurrencedayid" data-prompt-position="topLeft:14,36" multiple>
										<option value=""></option>
										<?php $prev = ' ';
											foreach($recurrenceday as $key):
										?>
										<option value="<?php echo $key->recurrencedayid;?>"> <?php echo $key->recurrencedayid.' - '.$key->recurrencedayname;?></option>
										<?php endforeach;?>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Auto Hide Popup for all the overlay used in software">Popup Trigger</label>
									<select data-validation-engine="" class="chzn-select" id="popuphideshow" name="popuphideshow" data-prompt-position="topLeft:14,36">
										<option value="0">Manual</option>
										<option value="1">Automatic</option>
									</select>
								</div>
								<div class="large-12 columns">&nbsp;</div>
							</div>
						</div>
					</div>
					<div id="overlaysubformspan2" class="hiddensubform hidedisplay transitionhiddenform">
						<div class="large-4 columns end paddingbtm">
							<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
								<div class="large-12 columns headerformcaptionstyle">General Details</div>
								<div class="static-field large-6 columns">
									<label data-description="Max level of storage to be created in Storage module">Storage Level</label>
									<select  class="chzn-select" id="counterlevel" name="counterlevel" data-prompt-position="topLeft:14,36">
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Max level of category to be created in Category module">Category Level</label>
									<select  class="chzn-select" id="categorylevel" name="categorylevel" data-prompt-position="topLeft:14,36">
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Max level of stone category to be created in module">Stone Level</label>
									<select data-validation-engine="validate[required]" class="chzn-select" id="stonelevel" name="stonelevel" data-prompt-position="topLeft:14,36">
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Validation for reached the max weight in stock entry">Storage Quantity</label>
									<select data-validation-engine="" class="chzn-select" id="storagequantity" name="storagequantity" data-prompt-position="topLeft:14,36">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="To display storage field in itemtag, Product and transaction Module">Storage Mode<span class="mandatoryfildclass">*</span></label>
									<select data-validation-engine="validate[required]" class="chzn-select" id="counteroption" name="counteroption" data-prompt-position="topLeft:14,36">
										<option value="YES">Yes</option>
										<option value="NO">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="To display Size field in Product Module (Enable / Disable)">Size Enable</label>
									<select data-validation-engine="" class="chzn-select" id="sizestatus" name="sizestatus" data-prompt-position="topLeft:14,36">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="To display stone field in Product module">Stone<span class="mandatoryfildclass">*</span></label>
									<select data-validation-engine="validate[required]" class="chzn-select" id="stoneoption" name="stoneoption" data-prompt-position="topLeft:14,36">
										<option value="YES">Yes</option>
										<option value="NO">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Select default driver to be set on form">Drive Name</label>
									<select class="chzn-select" id="drivename" name="drivename" data-prompt-position="topLeft:14,36" tabindex="1001" data-placeholder="Select">
										<option value=""></option>
										<?php
											/* $fso = new COM('Scripting.FileSystemObject');
											foreach ($fso->Drives as $drive) { */
										?>
												<option value="<?php //echo $drive->DriveLetter;?>"><?php // echo  $drive->DriveLetter;?></option>
										<?php
											//}
										?>
									</select>
								</div>
								<div class="static-field large-12 columns">
									<label data-description="To display RFID device number on Stock Session form">RFID Device No</label>
									<input type="text" class="validate[required]" id="deviceno" name="deviceno" value="" data-prompt-position="bottomLeft:14,36" tabindex="116">
								</div>
								<div class="static-field large-6 columns">
									<label>Discount Authorization<span class="mandatoryfildclass">*</span></label>
									<select class="chzn-select" id="discountauthmoduleview" name="discountauthmoduleview" data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36">
										<option value="0">Enable</option>
										<option value="1">Disable</option>
									</select>
								</div>
								<div class="static-field large-12 columns hidedisplay">
									<label data-description="">Purchase Calculation Type</label>
									<select id="purchasetransactionmodeid" name="purchasetransactionmodeid" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="109" Multiple>
										<option value=""></option>
										<?php foreach($transactionmode as $key): ?>
										<option value="<?php echo $key->transactionmodeid;?>">
										<?php echo $key->transactionmodename;?></option>
										<?php endforeach;?>
									</select>
								</div>
								<div class="static-field large-6 columns hidedisplay">
									<label data-description="Don't know have to ask vishal">Account Group</label>
									<select class="chzn-select" id="chargeaccountgroup" name="chargeaccountgroup" value="" tabindex="104" data-prompt-position="topLeft:14,36">								
										<option class="groupall" value="1">ALL</option>
										<?php foreach($chargeaccountgroup as $key):?>
										<option value="<?php echo $key->accountgroupid;?>">
											<?php echo $key->accountgroupname;?></option>
									<?php endforeach;?>	
									</select>							
								</div>
								<div class="static-field large-12 columns hidedisplay">
									<label data-description="Didn't understand the code">Product ROL</label>
									<select data-validation-engine="" class="chzn-select" id="productroltype" name="productroltype" data-prompt-position="topLeft:14,36" multiple>
										<option value=""></option>
										<option value="1">Size</option>
										<option value="2">Weight Range</option>
										<option value="3">Vendor</option>
									</select>
								</div>
								<div class="static-field large-6 columns hidedisplay">
									<label data-description="">GST<span class="mandatoryfildclass">*</span></label>
									<select data-validation-engine="" class="chzn-select" id="gstapplicable" name="gstapplicable" data-prompt-position="topLeft:14,36">
										<option value="1">Enable</option>
										<option value="2">Disable</option>
									</select>
								</div>
								<div class="static-field large-6 columns hidedisplay">
									<label data-description="">Print Template</label>
									<select id="printtemplateid" name="printtemplateid" class="chzn-select" data-prompt-position="topLeft:14,36">	
										<option value="1">No Print</option>
										<?php foreach($printtemplate as $key):?>
										<option value="<?php echo $key->printtemplatesid;?>">
										<?php echo $key->printtemplatesname;?></option>
										<?php endforeach;?>	
									</select>
								</div>
								<div class="static-field large-6 columns hidedisplay">
									<label data-description="">Discount Password</label>
									<select data-validation-engine="" class="chzn-select" id="discountpassword" name="discountpassword" data-prompt-position="topLeft:14,36">
											<option value="0">No</option>
											<option value="1">Yes</option>
									 </select>
								</div>
								<div class="static-field large-6 columns hidedisplay">
									<label>Advance Calculation</label>
									<select data-validation-engine="" class="chzn-select" id="advancecalc" name="advancecalc" data-prompt-position="topLeft:14,36">
										<option value="1">Automatic</option>
										<option value="0">Manual</option>
									</select>
								</div>
								<div class="static-field large-6 columns hidedisplay">
									<label>Tax Type</label>
									<select data-validation-engine="" class="chzn-select" id="taxtypeid" name="taxtypeid" data-prompt-position="topLeft:14,36">
										<option value=""></option>
										<option value="1">Item Level</option>
										<option value="2">Bill Level</option>
										</select>
								</div>
								<div class="static-field large-6 columns hidedisplay">
									<label>Discount Type</label>
									<select data-validation-engine="" class="chzn-select" id="discounttypeid" name="discounttypeid" data-prompt-position="topLeft:14,36">
										<option value=""></option>
										<option value="1">Item Level</option>
										<option value="2">Bill Level</option>
										</select>
								</div>
								<div class="static-field large-6 columns hidedisplay">
									<label>Rate Addition Default %</label>
									<input type="text" name="rateadditiondefault" id="rateadditiondefault" value=""/>
								</div>
								<div class="static-field large-6 columns hidedisplay">
									<label>Weight Validate</label>
									<select data-validation-engine="" class="chzn-select" id="weightvalidate" name="weightvalidate" data-prompt-position="topLeft:14,36">
										<option value=""></option>
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns hidedisplay">
									<label>Tax</label>
									<select data-validation-engine="" class="chzn-select" id="oldjewelsumtaxid" name="oldjewelsumtaxid" data-prompt-position="topLeft:14,36">
										<option value=""></option>
											<?php foreach($oldjeweltax as $key): ?>
											<option value="<?php echo $key->taxid;?>">
											<?php echo $key->taxname;?></option>
											<?php endforeach;?>
									</select>
								</div>
								<div class="static-field large-6 columns hidedisplay">
									<label>Purity </label>
									<select class="chzn-select" id="purityid" name="purityid" value="" tabindex=""  data-prompt-position="topLeft:14,36">
										<option value=""></option>
										<?php $prev = ' ';
										foreach($purity->result() as $key):
										$cur = $key->metalid;
										$a="";
										if($prev != $cur )
										{
											if($prev != " ")
											{
												 echo '</optgroup>';
											}
											echo '<optgroup  label="'.$key->metalname.'" data-metalid="'.$key->metalid.'">';
											$prev = $key->metalid;
										}
										?>
										<option data-metal_id="" value="<?php echo $key->purityid; ?>"><?php echo $key->purityname;?></option>
										<?php endforeach;?>
									</select>
								</div>
								<div class="large-12 columns">&nbsp;</div>
								<input type="hidden" name="companysettingid" id="companysettingid">
						   </div>
						</div>
						<div class="large-4 columns end paddingbtm">
							<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
								<div class="large-12 columns headerformcaptionstyle">Transaction Customer Part</div>
								<div class="static-field large-6 columns">
									<label data-description="PAN card is mandatory doing sales greater than equal to value.">PAN Card Limit</label>
									<input type="text" data-validation-engine="validate[required,custom[onlyWholeNumber],maxSize[10]]" name="pancardrestrictamt" id="pancardrestrictamt" value=""/>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Restricted amount for receive payment by Cash">Cash Receipt Limit</label>
									<input type="text" data-validation-engine="validate[required,custom[onlyWholeNumber],maxSize[10]]" name="cashreceiptlimit" id="cashreceiptlimit" value=""/>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Restricted amount for issue payment by Cash">Cash Issue Limit</label>
									<input type="text" data-validation-engine="validate[required,custom[onlyWholeNumber],maxSize[10]]" name="cashissuelimit" id="cashissuelimit" value=""/>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Max value of adjustment to be given in transaction">Adjustment Limit</label>
									<input type="text" data-validation-engine="validate[custom[number]]"  name="roundvaluelimit" id="roundvaluelimit" value="0"/>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Autoset Bank Accounts in payment method for Card, Cheque">Default Bank Name</label>
									<select data-validation-engine="" class="chzn-select" id="defaultbankid" name="defaultbankid" data-prompt-position="topLeft:14,36">
									<option value=""></option>
										<?php foreach($defaultbank as $key):?>
											<option value="<?php echo $key->accountid;?>"><?php echo $key->accountname;?></option>
										<?php endforeach;?>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Autoset Cash Accounts in Payment method for Cash">Default Cash in hand</label>
									<select data-validation-engine="" class="chzn-select" id="defaultcashinhandid" name="defaultcashinhandid" data-prompt-position="topLeft:14,36">
									<option value=""></option>
										<?php foreach($defaultcash as $key):?>
											<option value="<?php echo $key->accountid;?>"><?php echo $key->accountname;?></option>
										<?php endforeach;?>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Records to display by default in Main view Grid.">Main View - D. Records</label>
									<select  class="chzn-select" id="mainviewdefaultview" name="mainviewdefaultview" data-prompt-position="topLeft:14,36">
											<option value="10">10</option>
											<option value="20">20</option>
											<option value="30">30</option>
											<option value="50">50</option>
											<option value="100">100</option>
											<option value="200">200</option>
											<option value="300">300</option>
											<option value="500">500</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Rate which display in main view for all the modules.">Rate Display Main View</label>
									<select data-validation-engine="" class="chzn-select" id="ratedisplaymainview" name="ratedisplaymainview" data-prompt-position="topLeft:14,36">
										<option value="0">No</option>
										<option value="1">Yes</option>
									</select>
								</div>
								<div class="static-field large-12 columns ">
									<label data-description="Rate Display in Main view and they can select any two purity to display in main view.">Rate Display - Purity</label>
									<select id="ratedisplaypurityvalue" name="ratedisplaypurityvalue" class="chzn-select" data-prompt-position="topLeft:14,36" multiple>
										<option value=""></option>
										<?php foreach($purity->result() as $key): ?>
										<option value="<?php echo $key->purityid;?>">
										<?php echo $key->purityname;?></option>
										<?php endforeach;?>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Sales Return - Display only the customer based bill level or shows all the customer list details.">Sales Return - Lists</label>
									<select data-validation-engine="" class="chzn-select" id="salesreturnbillcustomers" name="salesreturnbillcustomers" data-prompt-position="topLeft:14,36">
										<option value="0">All Customers</option>
										<option value="1">Specific Customers</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Discount Module - Yes / No. Discount Authorization will work based on discount module or free to give discount value.">Discount Authorization</label>
									<select data-validation-engine="" class="chzn-select" id="discountauthorization" name="discountauthorization" data-prompt-position="topLeft:14,36">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
								<div class="large-12 columns">&nbsp;</div>
							</div>
						</div>
						<div class="large-4 columns end paddingbtm">
							<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
								<div class="large-12 columns headerformcaptionstyle">Other Details</div>
								<div class="static-field large-6 columns">
									<label data-description="It will through an popup message to the user that Chitbook is closed. If giving No, it will not shown message to the user.">Chitbook Close Alert</label>
									<select data-validation-engine="" class="chzn-select" id="chitcompletealert" name="chitcompletealert" data-prompt-position="topLeft:14,36">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="In Scheme Master, Interest type to be in Percentage or Flat Amount.">Chit Interest Type</label>
									<select data-validation-engine="" class="chzn-select" id="chitinteresttype" name="chitinteresttype" data-prompt-position="topLeft:14,36">
										<option value="1">Flat Amount</option>
										<option value="0">Percentage</option>
									</select>
								</div>
								<div class="static-field large-12 columns ">
									<label data-description="Authorization Person can able to do changes for date">Chit Authorization Roles</label>
									<select id="chituserroleauth" name="chituserroleauth" class="chzn-select" data-prompt-position="topLeft:14,36" multiple>
												<option value=""></option>
												<?php foreach($chituserroleauth as $key): ?>
												<option value="<?php echo $key->userroleid;?>">
												<?php echo $key->userrolename;?></option>
												<?php endforeach;?>	
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="To show credit information in Account Module">Credit Info</label>
									<select data-validation-engine="" class="chzn-select" id="creditinfohideshow" name="creditinfohideshow" data-prompt-position="topLeft:14,36">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="In transaction module, account type short name will be displaying in the Dropdown for searching an account. Purpose to identify whether the account is customer/vendor.">Account Type in Account DD</label>
									<select data-validation-engine="" class="chzn-select" id="accountypehideshow" name="accountypehideshow" data-prompt-position="topLeft:14,36">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Validation for Mobile number field. If yes, it will changed as mandatory otherwise its an non mandatory">Account - Mobile Required</label>
									<select data-validation-engine="" class="chzn-select" id="accountmobilevalidation" name="accountmobilevalidation" data-prompt-position="topLeft:14,36">
										<option value="YES">Yes</option>
										<option value="NO">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Unique number validation for mobile number. If yes, you one give only one mobile number for one account. Else no, can give mobile number for too many accounts.">Account - Mobile Unique No</label>
									<select data-validation-engine="" class="chzn-select" id="accountmobileunique" name="accountmobileunique" data-prompt-position="topLeft:14,36">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="While typing in Account field should automatically convert to Capital Letters or Default Value.">Account field CAPS</label>
									<select data-validation-engine="" class="chzn-select" id="accountfieldcaps" name="accountfieldcaps" data-prompt-position="topLeft:14,36">
										<option value="0">No</option>
										<option value="1">Yes</option>
									</select>
								</div>
								<div class="large-12 columns">&nbsp;</div>
							</div>
						</div>
					</div>
					<div id="overlaysubformspan3" class="hiddensubform hidedisplay transitionhiddenform">
						<div class="large-4 columns end paddingbtm">
							<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
								<div class="large-12 columns headerformcaptionstyle">Charges</div>
								<div class="static-field large-12 columns ">
									<label data-description="By default sales charges should be set in product module">Default Sales Charges (Product)</label>
									<select id="chargeid" name="chargeid" class="chzn-select" data-prompt-position="topLeft:14,36" multiple>
										<option value=""></option> 
										<?php $prev = ' ';
										foreach($charge->result() as $key):
										$cur = $key->chargecategoryid;
										$a="";
										if($prev != $cur )
										{
											if($prev != " ")
											{
												 echo '</optgroup>';
											}
											echo '<optgroup  label="'.$key->chargecategoryname.'">';
											$prev = $key->chargecategoryid;
										}
										?>
										<option value="<?php echo $key->chargeid;?>"><?php echo $key->chargename;?></option>
										<?php endforeach;?>
									</select>
								</div>
								<div class="static-field large-12 columns ">
									<label data-description="Autoset Purchase Charges in Product Module">Default Purchase Charges (Product)</label>
									<select id="purchasechargeid" name="purchasechargeid" class="chzn-select" data-prompt-position="topLeft:14,36" multiple>
									<option value=""></option> 
									<?php $prev = ' ';
									foreach($purchasecharge->result() as $key):
									$cur = $key->chargecategoryid;
									$a="";
									if($prev != $cur )
									{
										if($prev != " ")
										{
											 echo '</optgroup>';
										}
										echo '<optgroup  label="'.$key->chargecategoryname.'">';
										$prev = $key->chargecategoryid;
									}
									?>
									<option value="<?php echo $key->purchasechargeid;?>"><?php echo $key->purchasechargename;?></option>
									<?php endforeach;?>
									</select>
								</div>
								<div class="static-field large-12 columns ">
									<label data-description="Select charges to be used in the jewellery for Sales">Sales Charge For Business</label>
									<select id="businesschargeid" name="businesschargeid" class="chzn-select" data-prompt-position="topLeft:14,36" multiple>
									<option value=""></option> 
									<?php $prev = ' ';
									foreach($charge->result() as $key):
									$cur = $key->chargecategoryid;
									$a="";
									if($prev != $cur )
									{
										if($prev != " ")
										{
											 echo '</optgroup>';
										}
										echo '<optgroup  label="'.$key->chargecategoryname.'">';
										$prev = $key->chargecategoryid;
									}
									?>
									<option value="<?php echo $key->chargeid;?>"><?php echo $key->chargename;?></option>
									<?php endforeach;?>
									</select>
								</div>
								<div class="static-field large-12 columns ">
									<label data-description="Select charges to be used in the jewellery for Purchase">Purchase Charge For Business</label>
									<select id="businesspurchasechargeid" name="businesspurchasechargeid" class="chzn-select" data-prompt-position="topLeft:14,36" multiple>
									<option value=""></option> 
									<?php $prev = ' ';
									foreach($purchasecharge->result() as $key):
									$cur = $key->chargecategoryid;
									$a="";
									if($prev != $cur )
									{
										if($prev != " ")
										{
											 echo '</optgroup>';
										}
										echo '<optgroup  label="'.$key->chargecategoryname.'">';
										$prev = $key->chargecategoryid;
									}
									?>
									<option value="<?php echo $key->purchasechargeid;?>"><?php echo $key->purchasechargename;?></option>
									<?php endforeach;?>
									</select>
								</div>
							</div>
						</div>
						<div class="large-4 columns end paddingbtm">
							<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
								<div class="large-12 columns headerformcaptionstyle">Masters</div>
								<div class="static-field large-6 columns">
									<label data-description="Autoset Product Category field in Product Module">Default Product Category</label>
									<select data-validation-engine="" class="chzn-select" id="categoryid" name="categoryid" data-prompt-position="topLeft:14,36">
										<option value="">Select</option>
										<?php $prev = ' '; foreach($category as $key): ?>
										<option value="<?php echo $key->categoryid;?>"><?php echo $key->categoryname;?></option>
										<?php endforeach;?>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="To validate the max quantity field in Product Addon Module and Product Module">Reorder & Max Qty</label>
									<select data-validation-engine="" class="chzn-select" id="reorderqty" name="reorderqty" data-prompt-position="topLeft:14,36">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Template load in Storage Management Module">Box Wt Template</label>
									<select data-validation-engine="" class="chzn-select" id="boxwttemplate" name="boxwttemplate" data-prompt-position="topLeft:14,36">
											<option value="0">No Print</option>
											<option value="1">Tag Template</option>
											<option value="2">Print Template</option>
									 </select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="To display Chitbook icon in More Option (Enable / Disable)">Scheme</label>
									<select data-validation-engine="" class="chzn-select" id="schemestatus" name="schemestatus" data-prompt-position="topLeft:14,36">
										<option value="YES">Yes</option>
										<option value="NO">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns hidedisplay">
								<label>Charge Mode<span class="mandatoryfildclass">*</span></label>
									<select data-validation-engine="" class="chzn-select" id="chargecalcoption" name="chargecalcoption" data-prompt-position="topLeft:14,36">
										<option value="1">Min</option>
										<option value="2">Max</option>
										<!--<option value="3">Both</option>-->
									</select>
								</div>
								<div class="static-field large-6 columns hidedisplay">
									<label>Default Charge Apply<span class="mandatoryfildclass">*</span></label>
									<select data-validation-engine="" class="chzn-select" id="chargecalculation" name="chargecalculation" data-prompt-position="topLeft:14,36">
										<option value="1">Max</option>
										<option value="2">Min</option>
									</select>
								</div>
								<div class="static-field large-12 columns">
									<label data-description="purity for rate based on metal">Autoset Purity For Rate</label>
									<select class="chzn-select" id="ratepurityid" name="ratepurityid" value="" tabindex=""  data-prompt-position="topLeft:14,36" multiple>
										<option value=""></option>
										<?php $prev = ' ';
										foreach($purity->result() as $key):
										$cur = $key->metalid;
										$a="";
										if($prev != $cur )
										{
											if($prev != " ")
											{
												 echo '</optgroup>';
											}
											echo '<optgroup  label="'.$key->metalname.'" data-metalid="'.$key->metalid.'">';
											$prev = $key->metalid;
										}
										?>
										<option data-metal_id="" value="<?php echo $key->purityid; ?>"><?php echo $key->purityname;?></option>
										<?php endforeach;?>
									</select>
								</div>
								<div class="large-12 columns">&nbsp;</div>
							</div>
						</div>
						<div class="large-4 columns end paddingbtm">
							<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
								<div class="large-12 columns headerformcaptionstyle">Loose Stone Details</div>
								<div class="static-field large-12 columns ">
									<label data-description="Select charges to be applied for loose stone items for sales transactiontype">Loose Stone Sales Charges</label>
									<select id="loosesalescharge" name="loosesalescharge" class="chzn-select" data-prompt-position="topLeft:14,36" multiple>
										<option value=""></option> 
										<?php $prev = ' ';
										foreach($salesloosecharge->result() as $key):
										$cur = $key->chargecategoryid;
										$a="";
										if($prev != $cur )
										{
											if($prev != " ")
											{
												 echo '</optgroup>';
											}
											echo '<optgroup  label="'.$key->chargecategoryname.'">';
											$prev = $key->chargecategoryid;
										}
										?>
										<option value="<?php echo $key->chargeid;?>"><?php echo $key->chargename;?></option>
										<?php endforeach;?>
									</select>
								</div>
								<div class="static-field large-12 columns ">
									<label data-description="Select charges to be applied for loose stone items for purchase transactiontype">Loose Stone Purchase Charges</label>
									<select id="loosepurchasecharge" name="loosepurchasecharge" class="chzn-select" data-prompt-position="topLeft:14,36" multiple>
										<option value=""></option> 
										<?php $prev = ' ';
										foreach($purchaseloosecharge->result() as $key):
										$cur = $key->chargecategoryid;
										$a="";
										if($prev != $cur )
										{
											if($prev != " ")
											{
												 echo '</optgroup>';
											}
											echo '<optgroup  label="'.$key->chargecategoryname.'">';
											$prev = $key->chargecategoryid;
										}
										?>
										<option value="<?php echo $key->purchasechargeid;?>"><?php echo $key->purchasechargename;?></option>
										<?php endforeach;?>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="From where the loose stone charge taken from.">Loose Stone Charge</label>
									<select data-validation-engine="" class="chzn-select" id="loosestonecharge" name="loosestonecharge" data-prompt-position="topLeft:14,36">
										<option value="1">Sales charge</option>
										<option value="2">Purchase Charge</option>
									</select>
								</div>
								<div class="large-12 columns">&nbsp;</div>
							</div>
						</div>
					</div>
					<div id="overlaysubformspan4" class="hiddensubform hidedisplay transitionhiddenform">
						<div class="large-4 columns end paddingbtm">
							<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
								<div class="large-12 columns headerformcaptionstyle">Stock Entry Modes & Trigger</div>
								<div class="static-field large-6 columns">
									<label data-description="By default what tag type should trigger change after click on add button">Tag Type</label>
									<select class="chzn-select " id="tagtype" name="tagtype" data-prompt-position="topLeft:14,36">
									   <?php foreach($tagtype as $key):?>
										<option value="<?php echo $key->tagtypeid;?>">
											<?php echo $key->tagtypename;?></option>
									<?php endforeach;?>	 
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="To display RFID field in itemtag Module">Tagging Mode<span class="mandatoryfildclass">*</span></label>
									<select data-validation-engine="validate[required]" class="chzn-select" id="barcodeoption" name="barcodeoption" data-prompt-position="topLeft:14,36">
										<option value="1">Item Tag</option>
										<!--<option value="2">RFID Tag</option>-->
										<option value="3">Both</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Enter key to trigger on save button">Grossweight Trigger<span class="mandatoryfildclass">*</span></label>
									<select data-validation-engine="" class="chzn-select" id="grossweighttrigger" name="grossweighttrigger" data-prompt-position="topLeft:14,36">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Show product id in the Drop Down in Itemtag Module">Product Id show</label>
									<select data-validation-engine="" class="chzn-select" id="productidshow" name="productidshow" data-prompt-position="topLeft:14,36">
										<option value=""></option>
										<option value="YES">Yes</option>
										<option value="NO">No</option>
										</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="To display image tab group in Stock entry module">Stock Image Concept<span class="mandatoryfildclass">*</span></label>
									<select data-validation-engine="validate[required]" class="chzn-select" id="imageoption" name="imageoption" data-prompt-position="topLeft:14,36">
										<option value="YES">Yes</option>
										<option value="NO">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Default image capture trigger on itemtag module">Tag Auto Image</label>
									<select data-validation-engine="" class="chzn-select" id="tagautoimage" name="tagautoimage" data-prompt-position="topLeft:14,36">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Charges display while edit, fetch and display either itemtag or productaddon">Edit ItemTag charge</label>
									<select data-validation-engine="" class="chzn-select" id="edittagcharge" name="edittagcharge" data-prompt-position="topLeft:14,36">
											<option value="0">Product Addon</option>
											<option value="1">Item Tag</option>
									 </select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="By doing sales by default where the charges should come.">Itemtag Charges From</label>
									<select data-validation-engine="" class="chzn-select" id="tagchargesfrom" name="tagchargesfrom" data-prompt-position="topLeft:14,36">
										<option value="1">Stock Entry</option>
										<option value="0">Product Addon</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Purchase Details will be displayed in the Itemtag Module">Purchase Details Section</label>
									<select class="chzn-select" id="purchasedata" name="purchasedata" data-prompt-position="topLeft:14,36">
										<option value="">Select</option>
										 <option value="YES">Yes</option>
										<option value="NO">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Stock Entry popup notification for creating a stock entry (Yes / No). Used in Splittag module but there s no code in the JS file">Stock Entry Notification</label>
									<select data-validation-engine="" class="chzn-select" id="stocknotify" name="stocknotify" data-prompt-position="topLeft:14,36">
										<option value=""></option>
										<option value="YES">Yes</option>
										<option value="NO">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="To display safe counter or display counter or both counters to be in Itemtag Module on To Storage DD.">Storage Display</label>
									<select data-validation-engine="" class="chzn-select" id="storagedisplay" name="storagedisplay" data-prompt-position="topLeft:14,36">
										<option value="1">Both</option>
										<option value="2">Safe</option>
										<option value="3">Display</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="To show all the charges fields in Itemtag Module">VAC Charge show</label>
									<select data-validation-engine="" class="chzn-select" id="vacchargeshow" name="vacchargeshow" data-prompt-position="topLeft:14,36">
										<option value=""></option>
										<option value="YES">Yes</option>
										<option value="NO">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="After item has been saved in the itemtag module. Then the next field focust to be in Gross weight field or product field.">Item Save Focus</label>
									<select data-validation-engine="" class="chzn-select" id="itemtagsavefocus" name="itemtagsavefocus" data-prompt-position="topLeft:14,36">
										<option value="0">Gross Weight</option>
										<option value="1">Product</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Sales Return - Returned detail will come in overlay and automatically set in form fields.">Sales Return Automatic</label>
									<select data-validation-engine="" class="chzn-select" id="salesreturnautomatic" name="salesreturnautomatic" data-prompt-position="topLeft:14,36">
										<option value="0">Manual</option>
										<option value="1">Automatic</option>
									</select>
								</div>
								<div class="large-12 columns">&nbsp;</div>
							</div>
						</div>
						<div class="large-4 columns end paddingbtm">
							<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
								<div class="large-12 columns headerformcaptionstyle">Stock Entry Authorization & Validation</div>
								<div class="static-field large-6 columns">
									<label data-description="Need lot based tagging or direct tagging">Lot Concept<span class="mandatoryfildclass">*</span></label>
									<select data-validation-engine="validate[required]" class="chzn-select" id="lotdetailsopt" name="lotdetailsopt" data-prompt-position="topLeft:14,36">
										<option value="YES">Yes</option>
										<option value="NO">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Stone products can validate stone weight or not">StoneWt Check</label>
									<select data-validation-engine="" class="chzn-select" id="stoneweightcheck" name="stoneweightcheck" data-prompt-position="topLeft:14,36">
										<option value="Yes">Yes</option>
										<option value="No">No</option>
									</select>
								</div>
								<div class="static-field large-12 columns ">
									<label data-description="Users Permission to do changes in the Itemtag Module">Itemtag Authorization Roles</label>
									<select id="itemtagauthuserroleid" name="itemtagauthuserroleid" class="chzn-select" data-prompt-position="topLeft:14,36" multiple>
												<option value=""></option>
												<?php foreach($itemtagauthrole as $key): ?>
												<option value="<?php echo $key->userroleid;?>">
												<?php echo $key->userrolename;?></option>
												<?php endforeach;?>	
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Autofill weight trigger from weighing machine">Weighing Scale Mode <span class="mandatoryfildclass">*</span></label>
									<select data-validation-engine="validate[required]" class="chzn-select" id="scaleoption" name="scaleoption" data-prompt-position="topLeft:14,36">
										<option value="Yes">Yes</option>
										<option value="No">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Validation for charges fields in Itemtag Modules.">Charge Required</label>
									<select data-validation-engine="" class="chzn-select" id="chargerequired" name="chargerequired" data-prompt-position="topLeft:14,36">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Display two fields in itemtag module like Itemrate with GST and Without GST">Prize Tag GST</label>
									<select data-validation-engine="" class="chzn-select" id="prizetaggst" name="prizetaggst" data-prompt-position="topLeft:14,36">
											<option value="0">No</option>
											<option value="1">Yes</option>
									 </select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Validation for RFID tag numeber in Itemtag Module">RFID No of Digits</label>
									<input type="text" name="rfidnoofdigits" id="rfidnoofdigits" value="" />
								</div>
								<div class="static-field large-6 columns">
									<label data-description="To calculate netwt for Stone items. For eg., will calculate G - D - CS - P = Netwt">Net Wt-Calc Type<span class="mandatoryfildclass">*</span></label>
									<select data-validation-engine="validate[required]" id="netwtcalctype" name="netwtcalctype" class="chzn-select validate[required]" data-prompt-position="topLeft:14,36">
									<?php foreach($stonecalctype as $key):?>
										<option value="<?php echo $key->stonecalctypeid;?>">
											<?php echo $key->netwtcalctype;?></option>
									<?php endforeach;?>	
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="To show the product additional information. Like for example, 1 Hook or 2 Hook or something like that.">Prod Additional Info</label>
									<select data-validation-engine="" class="chzn-select" id="productaddoninfo" name="productaddoninfo" data-prompt-position="topLeft:14,36">
											<option value="0">No</option>
											<option value="1">Yes</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="To calculate grossweight for stone pieces wise calculation or not.">Stone Pieces Calculation</label>
									<select data-validation-engine="" class="chzn-select" id="stonepiecescalc" name="stonepiecescalc" data-prompt-position="topLeft:14,36">
										<option value="0">Without Grossweight</option>
										<option value="1">With Grossweight</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="On Adding an itemtag Vendor field will be mandatory or not.">Vendor Validation</label>
									<select data-validation-engine="" class="chzn-select" id="itemtagvendorvalidate" name="itemtagvendorvalidate" data-prompt-position="topLeft:14,36">
										<option value="0">Not Mandatory</option>
										<option value="1">Mandatory</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="On doing partial tag date, should have taken date from stock date or current date.">Partial Tag Date</label>
									<select data-validation-engine="" class="chzn-select" id="partialtagdate" name="partialtagdate" data-prompt-position="topLeft:14,36">
										<option value="0">Current Date</option>
										<option value="1">Stock Date</option>
									</select>
								</div>
								<div class="large-12 columns">&nbsp;</div>
							</div>
						</div>
						<div class="large-4 columns end paddingbtm">
							<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
								<div class="large-12 columns headerformcaptionstyle">Lot Details</div>
								<div class="static-field large-12 columns">
									<label data-description="Validation to be at lot module for mandatory fields">Lot Creation Data</label>
									<select data-validation-engine="" class="chzn-select" id="lotdata" name="lotdata" data-prompt-position="topLeft:14,36" multiple>
										<option value="1">Product</option>
										<option value="2">Purity</option>
										<option value="3">Vendor</option>
										<option value="4">Category</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Creation of lot from purchase or manual creation">Lot Creation Mode<span class="mandatoryfildclass">*</span></label>
									<select data-validation-engine="validate[required]" class="chzn-select" id="lotcreationopt" name="lotcreationopt" data-prompt-position="topLeft:14,36">
										<?php foreach($lottype as $key):?>
											<option value="<?php echo $key->lottypeid;?>">
											<?php echo $key->lottypename;?></option>
										<?php endforeach;?>	
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="To display pieces and validate in Lot module">Lot Pieces</label>
									<select data-validation-engine="" class="chzn-select" id="lotpieces" name="lotpieces" data-prompt-position="topLeft:14,36">
										<option value=""></option>
										<option value="Yes">Yes</option>
										<option value="No">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Lot closing weight is from direct weight or using percentage concept. If the concept is yes, then direct weight concept is disabled.">Lot Closing Percentage</label>	
									<select data-validation-engine="" class="chzn-select" id="lottolerancepercent" name="lottolerancepercent" data-prompt-position="topLeft:14,36">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="If percentage concept is yes, we have to give percentage value. Based on the value, it will calculate and autoclose +/- of the percentage Lot grossweight value.">Lot Percentag Value</label>
									<input type="text" data-validation-engine="validate[required,custom[number],decval[2],min[0.01]]"  name="lottolerancevalue" id="lottolerancevalue" value=""/>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Tolerance weight will be added at the time of closing the lot">Lot Close Weight Limit</label>
									<input type="text" data-validation-engine="validate[custom[number],decval[<?php echo $weight_round?>],min[0.001]]"  name="lotweightlimit" id="lotweightlimit" value=""/>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="If percentage concept is yes, we have to give percentage value. Based on the value, it will calculate and autoclose +/- of the percentage Lot diamond carat weight value.">Percentag Diamond Value</label>
									<input type="text" data-validation-engine="validate[required,custom[number],decval[2],min[0.01]]"  name="lottolerancediactvalue" id="lottolerancediactvalue" value=""/>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Tolerance weight will be added at the time of closing the lot (Diamond Carat Weight)">Diamond Close Weight Limit</label>
									<input type="text" data-validation-engine="validate[custom[number],decval[<?php echo $dia_weight_round?>],min[0.001]]"  name="lotdiaweightlimit" id="lotdiaweightlimit" value=""/>
								</div>
								<div class="large-12 columns">&nbsp;</div>
							</div>
						</div>
					</div>
					<div id="overlaysubformspan5" class="hiddensubform hidedisplay transitionhiddenform">
						<div class="large-4 columns end paddingbtm">
							<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
								<div class="large-12 columns headerformcaptionstyle">Basic Details</div>
								<div class="static-field large-6 columns">
									<label data-description="To display image concept for All Transaction modules">Transaction Image Concept</label>
									<select data-validation-engine="" class="chzn-select" id="transactionimageoption" name="transactionimageoption" data-prompt-position="topLeft:14,36">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="To display image concept for Order Management">Order Image Concept</label>
									<select data-validation-engine="" class="chzn-select" id="orderimageoption" name="orderimageoption" data-prompt-position="topLeft:14,36">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Vendor management for Take order (Generate Place Order)">PO Vendor Management</label>
									<select data-validation-engine="" class="chzn-select" id="povendormanagement" name="povendormanagement" data-prompt-position="topLeft:14,36">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="On doing Take Order, want category field should display or only with product field.">Take Order Category</label>
									<select data-validation-engine="" class="chzn-select" id="takeordercategory" name="takeordercategory" data-prompt-position="topLeft:14,36">
										<option value="1">With Category</option>
										<option value="0">Without Category</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Take Order due date - To set due date 7days or 15days from today.">Take Order Category</label>
									<select data-validation-engine="" class="chzn-select" id="takeorderduedate" name="takeorderduedate" data-prompt-position="topLeft:14,36">
										<option value="7">7 Days</option>
										<option value="15">15 Days</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Convert Estimation to Sales or Direct Sales">Estimate on sales bill</label>
									<select class="chzn-select" id="estimateconcept" name="estimateconcept" data-prompt-position="topLeft:14,36">
										<option value="Yes">Yes</option>
										<option value="No">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="To display cashcounter field in transaction form">Cash Counter<span class="mandatoryfildclass">*</span></label>
									<select data-validation-engine="validate[required]" class="chzn-select" id="cashcountershowhide" name="cashcountershowhide" data-prompt-position="topLeft:14,36">
										<option value="1">Show</option>
										<option value="2">Hide</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="set default account and autofocus on stocktypeid for estimate">Estimate Account Set Mode</label>
									<select data-validation-engine="" class="chzn-select" id="estimatedefaultset" name="estimatedefaultset" data-prompt-position="topLeft:14,36">
										<option value="0">No</option>
										<option value="1">Yes</option>
									</select>
								</div>
								<div class="static-field large-6 columns hidedisplay">
									<label>Transaction Account Type Set</label>
									<select data-validation-engine="" class="chzn-select" id="allowedaccounttypeset" name="allowedaccounttypeset" data-prompt-position="topLeft:14,36">
										<option value="0">Yes</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Estimate default account id - By default which account should be set in estimate account">Estimate D.Account Set</label>
									<input type="text" data-validation-engine="validate[custom[number]]"  name="estimatedefaultaccid" id="estimatedefaultaccid" value="" />
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Placeorder - With accountid they can place Stockorder to be placed using Place Order">Takeorder D.Account Set</label>
									<input type="text" data-validation-engine="validate[custom[number]]"  name="placeorderdefaultaccid" id="placeorderdefaultaccid" value=""></input>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Transaction module does have Print Template field to change template by doing any kind of transaction or does not have this option to chosen by customer.">Print Template Hide/Show</label>
									<select data-validation-engine="" class="chzn-select" id="printtemplatehideshow" name="printtemplatehideshow" data-prompt-position="topLeft:14,36">
										<option value="0">No</option>
										<option value="1">Yes</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Transaction module will generate a credit payment for old purchase if the amount not fully issued to the customer. It will thrown an overlay for credit type (CREDIT / OLD) concept to show in print preview.">Old Purchase Credit Type</label>
									<select data-validation-engine="" class="chzn-select" id="oldpurchasecredittype" name="oldpurchasecredittype" data-prompt-position="topLeft:14,36">
										<option value="0">No</option>
										<option value="1">Yes</option>
									</select>
								</div>
								<div class="large-12 columns">&nbsp;</div>
							</div>
						</div>
						<div class="large-4 columns end paddingbtm">
							<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
								<div class="large-12 columns headerformcaptionstyle">Transaction Modes & Triggers</div>
								<div class="static-field large-6 columns">
									<label data-description="To calculate and display Total amount for Approval Out Transaction type">Approval Out Calculation</label>
									<select data-validation-engine="" class="chzn-select" id="approvaloutcalculation" name="approvaloutcalculation" data-prompt-position="topLeft:14,36">
										<option value="YES">Yes</option>
										<option value="NO">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="1- no rfid button 2- strictly only rfid based approval return 3- grid or rfid based approval return">RFID Approval Out</label>
									<select data-validation-engine="" class="chzn-select" id="approvaloutrfid" name="approvaloutrfid" data-prompt-position="topLeft:14,36">
										<option value="1">No Overlay</option>
										<!--<option value="2">RFID Appr.out</option>-->
										<option value="3">Overlay Appr.Out</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="If you select Yes, then only it will calculate all the charges in Gross Amt field. Or else it will calculate only grossweight * rate.">Tax Include Wastage</label>
									<select data-validation-engine="" class="chzn-select" id="taxchargeinclude" name="taxchargeinclude" data-prompt-position="topLeft:14,36">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Generate tax for old jewel items.">Apply Tax For Old Jewel</label>
									<select data-validation-engine="" class="chzn-select" id="oldjewelsumtaxapplyid" name="oldjewelsumtaxapplyid" data-prompt-position="topLeft:14,36">
										<option value=""></option>
										<option value="1">Yes</option>
										<option value="2">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Validate weight at the time of Untag Sales">Untag Weight Validation</label>
									<select data-validation-engine="" class="chzn-select" id="untagwtvalidate" name="untagwtvalidate" data-prompt-position="topLeft:14,36">
										<option value="0">No</option>
										<option value="1">Yes</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="To show the Available Untag total weight in the Transaction Module">Untag Weight Show</label>
									<select data-validation-engine="" class="chzn-select" id="untagwtshow" name="untagwtshow" data-prompt-position="topLeft:14,36">
										<option value="0">No</option>
										<option value="1">Yes</option>
									</select>
								</div>
								<div class="static-field large-6 columns hidedisplay">
									<label data-description="For untag items, displaying stone details. (Not developed fully, now implemented only on Sales)">Untag Stone</label>
									<select data-validation-engine="" class="chzn-select" id="untagstoneenable" name="untagstoneenable" data-prompt-position="topLeft:14,36">
										<option value="0">No</option>
										<option value="1">Yes</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="To find the caratweight values. Here is the formula caratwt = grosswt * caratwtcalcvalue;">Carat Wt Calc Value</label>
									<select data-validation-engine="" class="chzn-select" id="caratwtcalcvalue" name="caratwtcalcvalue" data-prompt-position="topLeft:14,36">
										<option value="5">5</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Multiple tags barcode scanning at same time">Multiple Tag Barcode</label>
									<select data-validation-engine="" class="chzn-select" id="multitagbarcode" name="multitagbarcode" data-prompt-position="topLeft:14,36">
										<option value="1">No</option>
										<option value="2">Yes</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Showing total amount field near to the itemtag number for scanning area">Estimate Quick Total</label>
									<select data-validation-engine="" class="chzn-select" id="estimatequicktotal" name="estimatequicktotal" data-prompt-position="topLeft:14,36">
										<option value="0">No</option>
										<option value="1">Yes</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="">Payment Hide/Show - LST</label>
									<select data-validation-engine="Hide/Show payment details for Purchase and Sales Transaction type. It was created for Loose Stone Items." class="chzn-select" id="salespaymenthideshow" name="salespaymenthideshow" data-prompt-position="topLeft:14,36">
										<option value="0">No</option>
										<option value="1">Yes</option>
									</select>
								</div>
								<div class="large-12 columns">&nbsp;</div>
							</div>
						</div>
						<div class="large-4 columns end paddingbtm">
							<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
								<div class="large-12 columns headerformcaptionstyle">Purchase Details Section</div>
								<div class="static-field large-6 columns">
									<label data-description="Don't know">Purchase Wastage Include</label>
									<select data-validation-engine="" class="chzn-select" id="purchasewastage" name="purchasewastage" data-prompt-position="topLeft:14,36">
										<option value="Yes">Yes</option>
										<option value="No">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Selected type to displayed at the time of Purchase creation">Product,Category & Storage</label>
									<select data-validation-engine="" class="chzn-select" id="purchasedisplay" name="purchasedisplay" data-prompt-position="topLeft:14,36">
										<option value="1">With Product & Storage</option>
										<option value="0">Without Product & Storage</option>
										<option value="2">With Category</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Purchase form Weight Validation check">Weight Check</label>
									<select data-validation-engine="" class="chzn-select" id="weightcheck" name="weightcheck" data-prompt-position="topLeft:14,36">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Error weight will be given in the purchase Weight Check overlay">Purchase Error Weight Limit</label>
									<input type="text" data-validation-engine="validate[custom[number],decval[<?php echo $weight_round?>],min[0.001]]"  name="purchaseerrorwt" id="purchaseerrorwt" value=""/>
								</div>
								<div class="static-field large-12 columns">
									<label data-description="Select the types of Old purchase that you want.">Old Item Type <span class="mandatoryfildclass">*</span></label>
									<select class="chzn-select" id="olditemdetails" name="olditemdetails" data-prompt-position="topLeft:14,36" multiple>
										<option value=""></option>
										<?php foreach($olditemdetails as $key):?>
											<option value="<?php echo $key->olditemdetailsid;?>" data-name="<?php echo $key->olditemdetailsname;?>">
											<?php echo $key->olditemdetailsname;?></option>
										<?php endforeach;?>	
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Hide Show Old Item Details field in Transaction form fields for Sales Transaction type">Old Jewel Details</label>
									<select class="chzn-select" id="oldjeweldetails" name="oldjeweldetails" data-prompt-position="topLeft:14,36">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Select how to take Old jewel items by Product wise or None">Old Jewel Product<span class="mandatoryfildclass">*</span></label>
									<select data-validation-engine="validate[required]" class="chzn-select" id="oldjewelproducttype" name="oldjewelproducttype" data-prompt-position="topLeft:14,36">
										<option value="1">Default Mode</option>
										<option value="2">Product Wise</option>
										<option value="3">All Products</option>
									</select>
								</div>
								<div class="static-field large-6 columns hidedisplay">
									<label data-description="To display Old Jewel Voucher field in transaction form fields.">Old Jewel Voucher</label>
									<select data-validation-engine="" class="chzn-select" id="oldjewelvoucher" name="oldjewelvoucher" data-prompt-position="topLeft:14,36">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns hidedisplay">
									<label data-description="To edit Old Jewel Voucher details in transaction module">Old Jewel Voucher Edit</label>
									<select data-validation-engine="" class="chzn-select" id="oldjewelvoucheredit" name="oldjewelvoucheredit" data-prompt-position="topLeft:14,36">
										<option value="1">No</option>
										<option value="0">Yes</option>
									</select>
								</div>
								<div class="static-field large-12 columns hidedisplay">
									<label data-description="To autoselect the Bullion product in Payment type of Sales Module">Bullion Product</label>
									<select class="chzn-select" id="bullionproduct" name="bullionproduct" data-prompt-position="topLeft:14,36" multiple>
									 <option value=""></option>
									<?php $prev = ' ';
									foreach($product->result() as $key):
									?>
									<option value="<?php echo $key->productid;?>" data-counterid="<?php echo $key->counterid;?>" data-purityid="<?php echo $key->purityid;?>"> <?php echo $key->productid.' - '.$key->productname;?></option>
									<?php endforeach;?>
									</select>
								</div>
								<div class="static-field large-6 columns hidedisplay">
									<label>Inline Pdf Preview <span class="mandatoryfildclass">*</span></label>
									<select data-validation-engine="" class="chzn-select" id="pdfoption" name="pdfoption" data-prompt-position="topLeft:14,36">
										<option value="Yes">Yes</option>
										<option value="No">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns hidedisplay">
									<label>Need EzeTap </label>
									<select class="chzn-select" id="needezetap" name="needezetap" data-prompt-position="topLeft:14,36">
									<option value="YES">Yes</option>
									 <option value="NO">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns hidedisplay">
									<label>Ask Email & Sms Fields</label>
									<select class="chzn-select" id="askemailsmsfields" name="askemailsmsfields" data-prompt-position="topLeft:14,36">
									<option value="YES">Yes</option>
									<option value="NO">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns hidedisplay">
									 <label>Need Cash & Cheque in Eze</label>
									 <select class="chzn-select" id="needcashcheque" name="needcashcheque" data-prompt-position="topLeft:14,36">
									 <option value="YES">Yes</option>
									 <option value="NO">No</option>
									 </select>
								</div>
								<div class="large-12 columns">&nbsp;</div>
							</div>
						</div>
					</div>
					<div id="overlaysubformspan6" class="hiddensubform hidedisplay transitionhiddenform">
						<div class="large-4 columns end paddingbtm">
							<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
								<div class="large-12 columns headerformcaptionstyle">Core Concept Level</div>
								<div class="static-field large-6 columns">
									<label data-description="How the weight should be calculate (by amount, melting,etc.,)">Wastage Calctype</label>
									<select data-validation-engine="" class="chzn-select" id="wastagecalctype" name="wastagecalctype" data-prompt-position="topLeft:14,36">
										<option value="1">Amount</option>
										<option value="2">Weight</option>
										<option value="3">Touch - Melting = Wst </option>
										<option value="4">Melting + Wst</option>
										<option value="5">Direct Touch</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="By giving discount value it should get calculated by amount or Wastage Percent">Discount Calculation Type</label>
									<select data-validation-engine="" class="chzn-select" id="discountbilllevelpercent" name="discountbilllevelpercent" data-prompt-position="topLeft:14,36">
										<option value="0">Direct Amount</option>
										<option value="1">Wastage Percentage</option>
										<option value="2">Rate * Nt.Wt Percent</option>
									</select>
								</div>
								<div class="static-field large-6 columns hidedisplay">
									<label data-description="Discount calculation to be done either after Tax or before Tax">Discount Calculation</label>
									<select data-validation-engine="" class="chzn-select" id="discountcalc" name="discountcalc" data-prompt-position="topLeft:14,36">
										<option value="1">Before Tax</option>
										<option value="2">After Tax</option>
									</select>
								</div>
								<div class="static-field large-6 columns hidedisplay">
									<label data-description="Tax calcuation to be done either after Discount or before Discount">Tax Calculation</label>
									<select data-validation-engine="" class="chzn-select" id="taxcalc" name="taxcalc" data-prompt-position="topLeft:14,36">
										<option value="1">After Discount</option>
										<option value="2">Before Discount</option>
									</select>
								</div>
								<div id="billingtypeiddivhid" class="static-field large-6 columns">
									<label data-description="Sales Return items either by Billing or Without Billing">Sales Return Mode</label>
								<select id="billingtypeid" name="billingtypeid" class="chzn-select" data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="108">									
										<option></option>
										<?php foreach($billingtype as $key): ?>
									<option value="<?php echo $key->billingtypeid;?>">
									<?php echo $key->billingtypename;?></option>
									<?php endforeach;?>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Select how to return items for without Billing concept.">Sales Return Type</label>
									<select data-validation-engine="" class="chzn-select" id="salesreturntypeid" name="salesreturntypeid" data-prompt-position="topLeft:14,36">
									<option></option>
										<?php foreach($salesreturntype as $key):?>
											<option value="<?php echo $key->salesreturntypeid;?>"><?php echo $key->salesreturntypename;?></option>
										<?php endforeach;?>
									</select>
								</div>
								<div class="static-field large-6 columns hidedisplay">
									<label data-description="Payment Issue/Receipt concept either by Bill or without Bill">Payment Issue/Receipt</label>
									<select data-validation-engine="" class="chzn-select" id="paymentmodetype" name="paymentmodetype" data-prompt-position="topLeft:14,36">
										<option value="1">Without Bill</option>
										<option value="2">With Bill</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Payment Issue/Receipt - At the time of payment for without Billing concept. Whether to display accordingly by Credit No or Direct Amount Payment Issue/Amount">Customer Wise</label>
									<select data-validation-engine="" class="chzn-select" id="customerwise" name="customerwise" data-prompt-position="topLeft:14,36">
										<option value="1">Auto</option>
										<option value="0">Manual</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Payment Issue/Receipt - At the time of payment for without Billing concept. Whether to display accordingly by Credit No or Direct Amount Payment Issue/Amount">Vendor Wise</label>
									<select data-validation-engine="" class="chzn-select" id="vendorwise" name="vendorwise" data-prompt-position="topLeft:14,36">
										<option value="1">Auto</option>
										<option value="0">Manual</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="To display Conversion details showing in the overlay form of any charge field.">Charge Conversion Field</label>
									<select data-validation-engine="" class="chzn-select" id="chargeconversionfield" name="chargeconversionfield" data-prompt-position="topLeft:14,36">
										<option value="0">No</option>
										<option value="1">Yes</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Bill Amount [ie., Net Amount] Calculation trigger will change automatically +/- value from BillAmountMaxValue field and it is autoadjusted with stone amount. The Entered amount - Excess value or lower value will change in stone rate and it will reflect in stone amount.">Bill Amount Calculation</label>
									<select data-validation-engine="" class="chzn-select" id="billamountcalculationtrigger" name="billamountcalculationtrigger" data-prompt-position="topLeft:14,36">
										<option value="0">Type A</option>
										<option value="1">Type B</option>
										<option value="2">Type C</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Chit Payment Type - Retrieve chitbook with full details by automatic or Manual concept">Scheme Method</label>
									<select data-validation-engine="" class="chzn-select" id="schememethodid" name="schememethodid" data-prompt-position="topLeft:14,36">
										<option value=""></option>
										<option value="2">Manual</option>
										<option value="3">Automatic</option>
									</select>
								</div>
								<div id="schemedivhid" class="static-field large-6 columns hidedisplay">
									<label>Scheme Type</label>
									<select data-validation-engine="" class="chzn-select" id="schemetypeid" name="schemetypeid" data-prompt-position="topLeft:14,36">
										<option value=""></option>
										<option value="2">Amount</option>
										<option value="3">Gram</option>
										<option value="4">Both</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="">Payment Product</label>
									<select data-validation-engine="" class="chzn-select" id="paymentproducts" name="paymentproducts" data-prompt-position="topLeft:14,36">
										<option value="0">All Products</option>
										<option value="1">Bullion Product</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="">Payment Chitbook Type</label>
									<select data-validation-engine="" class="chzn-select" id="chitbooktype" name="chitbooktype" data-prompt-position="topLeft:14,36">
										<option value="0">Chitbook</option>
										<option value="1">Chitbook Generation</option>
									</select>
								</div>
								<div class="large-12 columns">&nbsp;</div>
							</div>
						</div>
						<div class="large-4 columns end paddingbtm">
							<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
								<div class="large-12 columns headerformcaptionstyle">Authorization & Calculation Part</div>
								<div class="static-field large-12 columns ">
									<label data-description="Users Permission to do changes in the Transaction Module">Transaction Charges Edit</label>
									<select id="salesauthuserrole" name="salesauthuserrole" class="chzn-select" data-prompt-position="topLeft:14,36" multiple>
												<option value=""></option>
												<?php foreach($salesauthuserrole as $key): ?>
												<option value="<?php echo $key->userroleid;?>">
												<?php echo $key->userrolename;?></option>
												<?php endforeach;?>	
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="User Permission to change Date field in Sales Module">Sales Date Changeable For<span class="mandatoryfildclass">*</span></label>
									<select data-validation-engine="validate[required]" class="chzn-select" id="salesdatechangeablefor" name="salesdatechangeablefor" data-prompt-position="topLeft:14,36">
										<option value="1">All User</option>
										<option value="2">Admin</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Touch value by manual entry or default from Product Addon">Touch Edit Permission</label>
									<select data-validation-engine="" class="chzn-select" id="touchchargesedit" name="touchchargesedit" data-prompt-position="topLeft:14,36">
										<option value="0">Yes</option>
										<option value="1">No</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Taking tax from product, if not tax applied for Product then it would have taken from Product Category">Default Tax Category</label>
									<select class="chzn-select validate[required]" id="taxoption" name="taxoption" data-prompt-position="topLeft:14,36">
										<option value="">Select</option>
										<?php foreach($tax as $key):?>
											<option value="<?php echo $key->taxmasterid;?>">
											<?php echo $key->taxmastername;?></option>
										<?php endforeach;?>	
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="To calculate percent of Melting or Touch Percent. For eg. if Touch is 92, Purewt calculation is done accordingly to the Melting or Touch Percent">Purewt Calctype</label>
									<select data-validation-engine="" class="chzn-select" id="purewtcalctype" name="purewtcalctype" data-prompt-position="topLeft:14,36">
										<option value="100.00000">100 T</option>
										<option value="99.95000">99.95 T</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Which rate will apply for doing Sales. If you select purity rate it will taken rate from purity, or else whichever you have selected in Metal Purity multiple DD it will take rate for each category.">Sales Rate</label>
									<select class="chzn-select" id="salesrate" name="salesrate" data-prompt-position="topLeft:14,36">
										<option value="1">Purity Rate</option>
										<option value="2">Specific Purity Rate</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Which rate will apply for doing Purchase. If you selected purity rate it will taken rate from purity, or else whichever you have selected in Metal Purity multiple DD it will take rate for each category.">Purchase Rate</label>
									<select class="chzn-select" id="purchaserate" name="purchaserate" data-prompt-position="topLeft:14,36">
										<option value="1">Purity Rate</option>
										<option value="2">Specific Purity Rate</option>
									</select>
								</div>
								<div class="static-field large-12 columns">
										<label data-description="Rate which you apply for Specific Purity Rate on Sales Rate and Purchase Rate DD.">Metal Purity</label>
										<select class="chzn-select" id="metalpurityid" name="metalpurityid" value="" tabindex=""  data-prompt-position="topLeft:14,36" multiple>
											<option value=""></option>
											<?php $prev = ' ';
											foreach($purity->result() as $key):
											$cur = $key->metalid;
											$a="";
											if($prev != $cur )
											{
												if($prev != " ")
												{
													 echo '</optgroup>';
												}
												echo '<optgroup  label="'.$key->metalname.'" data-metalid="'.$key->metalid.'">';
												$prev = $key->metalid;
											}
											?>
											<option data-metal_id="" value="<?php echo $key->purityid; ?>"><?php echo $key->purityname;?></option>
											<?php endforeach;?>
										</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Other Users are resticted for returning the items 'N' number of days and here the days count in text box.">Sales Return Days</label>
									<input type="text" name="salesreturndays" id="salesreturndays" value=""/>
								</div>
								<div class="static-field large-12 columns">
									<label data-description="Users Permission can do return for any number of days.">Sales Return 'N' Days</label>
									<select id="salesreturnuserroleauth" name="salesreturnuserroleauth" class="chzn-select" data-prompt-position="topLeft:14,36" multiple>
										<option value=""></option>
										<?php foreach($salesauthuserrole as $key): ?>
										<option value="<?php echo $key->userroleid;?>">
										<?php echo $key->userrolename;?></option>
										<?php endforeach;?>
									</select>
								</div>
								<div class="large-12 columns">&nbsp;</div>
							</div>
						</div>
						<div class="large-4 columns end paddingbtm">
							<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
								<div class="large-12 columns headerformcaptionstyle">Customer Specification</div>
								<div class="static-field large-6 columns">
									<label data-description="Flat Weight will be added with Netweight and also calculate wastage percentage.">Flat Weight (Wastage %)</label>
									<select class="chzn-select" id="flatweightwastagespan" name="flatweightwastagespan" data-prompt-position="topLeft:14,36">
										<option value="0">No</option>
										<option value="1">Yes</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Category field should have in Tree Concept or Normal DD. If 1, Normal Drop down. If 0, Tree Concept.">Category Field</label>
									<select class="chzn-select" id="transactioncategoryfield" name="transactioncategoryfield" data-prompt-position="topLeft:14,36">
										<option value="0">Tree Concept</option>
										<option value="1">Drop Down</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Employee person field can select mulitple field or single Field. If Single field, the value is 1. Or else if Multiple Field, the value is 0 (Zero).">Employee Person Field</label>
									<select class="chzn-select" id="transactionemployeeperson" name="transactionemployeeperson" data-prompt-position="topLeft:14,36">
										<option value="0">Multi Select</option>
										<option value="1">Drop Down</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Place Order - Automatic Take Order Vendor has been selected and filtered in Drop down for showing take order number details.">PO Automatic Vendor</label>
									<select class="chzn-select" id="poautomaticvendor" name="poautomaticvendor" data-prompt-position="topLeft:14,36">
										<option value="0">No</option>
										<option value="1">Yes</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="On Receive items from vendor, There is an option called directly created Lot or Already created items has to link to the Receive Order">Receive Order Concept</label>
									<select class="chzn-select" id="receiveorderconcept" name="receiveorderconcept" data-prompt-position="topLeft:14,36">
										<option value="0">Without Lot</option>
										<option value="1">Create Lot</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="For Purchase Transaction type - To display and enter stone entry details.">Purchase Stone Details</label>
									<select class="chzn-select" id="purchasemodstonedetails" name="purchasemodstonedetails" data-prompt-position="topLeft:14,36">
										<option value="0">No</option>
										<option value="1">Yes</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="For Old purchase stock type - To display and enter stone entry details.">Old Pur. Stone Details</label>
									<select class="chzn-select" id="oldjewelsstonedetails" name="oldjewelsstonedetails" data-prompt-position="topLeft:14,36">
										<option value="0">No</option>
										<option value="1">Yes</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="For Old purchase stock type - Stone Calculation is done on Sales Rate or Purchase Rate.">Old Pur. Stone Calc</label>
									<select class="chzn-select" id="oldjewelsstonecalc" name="oldjewelsstonecalc" data-prompt-position="topLeft:14,36">
										<option value="0">Sales Rate</option>
										<option value="1">Purchase Rate</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Default template set for Receive Order. I.e., while select stock type receive order then template will set automatically. We have to pass the template id here.">Receive Order Template</label>
									<input type="text" name="receiveordertemplateid" id="receiveordertemplateid" value=""/>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Reject/ Review order from jewellery side then the template will set automatically and print after submit button.">Reject/Review Template</label>
									<input type="text" name="rejectreviewordertemplateid" id="rejectreviewordertemplateid" value=""/>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Feedback for Item generated by vendor. Questionaire applicable in Generate Place Order and Receive Order.">Questionaire</label>
									<select class="chzn-select" id="questionaireapplicable" name="questionaireapplicable" data-prompt-position="topLeft:14,36">
										<option value="0">No</option>
										<option value="1">Yes</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Account Summary Information in top of the Detail List Grid. Hide / Show those details according to the customers.">Account Summary Info</label>
									<select class="chzn-select" id="accountsummaryinfo" name="accountsummaryinfo" data-prompt-position="topLeft:14,36">
										<option value="0">No</option>
										<option value="1">Yes</option>
									</select>
								</div>
								<div class="static-field large-6 columns">
									<label data-description="Transaction Payment save button to be hide or not. Using enter key to follow up and trigger save button.">Payment Save Button</label>
									<select class="chzn-select" id="paymentsavebutton" name="paymentsavebutton" data-prompt-position="topLeft:14,36">
										<option value="0">No</option>
										<option value="1">Yes</option>
									</select>
								</div>
								<div class="large-12 columns">&nbsp;</div>
							</div>
						</div>
					</div>
				</div>
			</form>
			<div id="overlaysubformspan7" class="hiddensubform hidedisplay transitionhiddenform">
			
			<form name="frmtransactionsetting" id="frmtransactionsetting" class="transactionsettingform validationEngineContainer"  >
			<?php if($device=='phone') {?>
			<div class="closed effectbox-overlay effectbox-default" id="transactionsectionoverlay">
			<?php }?>
				<div class="large-4 columns end paddingbtm">
					<div class="large-12 columns validationEngineContainer transactiontypeclear borderstyle cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;" id="transactiontypeaddvalidate">
						<?php if($device=='phone') {?>
						<div class="large-12 columns sectionheaderformcaptionstyle">Transaction Details</div>
						<div class="large-12 columns sectionpanel">
						<?php  } else { ?>
								<div class="large-12 columns headerformcaptionstyle">Transaction Details</div>
						<?php  	} ?> 																		
							<div class="static-field large-12 columns">
								<label data-description="Select Multiple metal to create metal based Serial Number and Print Template">Multiple Metal<span class="mandatoryfildclass">*</span></label>
								<select class="chzn-select" id="metalarraydetails" name="metalarraydetails" data-prompt-position="topLeft:14,36" multiple>
									<?php foreach($metaldata as $key):?>
										<option value="<?php echo $key->metalid;?>" data-name="<?php echo $key->metalname;?>"><?php echo $key->metalname;?></option>
									<?php endforeach;?>	
								</select>
							</div>
							<div class="static-field large-6 columns ">
								<label>Transaction Type<span class="mandatoryfildclass">*</span></label>						
								<select id="transactiontype" name="transactiontype" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="107">
								<option value=""></option>
									<?php $prev = ' ';
								foreach($salestransactiontype->result() as $key):
							?>
							<option value="<?php echo $key->salestransactiontypeid;?>"   data-stocktyperelation="<?php echo $key->stocktyperelationid;?>" data-name="<?php echo $key->salestransactiontypename;?>"><?php echo $key->salestransactiontypename;?></option>
							<?php endforeach;?>				
								</select>
							 </div>
							<div class="static-field large-6 columns">
								<input type="text" class="" id="transactionmanagename" name="transactionmanagename" value="" tabindex="107">
								<label for="transactionmanagename">Unique Name</label>
							</div>
							<div class="static-field large-6 columns ">
								<label>Mode<span class="mandatoryfildclass" id="modeid_req">*</span></label>		
								<select id="salesmodeid" name="salesmodeid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="101">									
									<option value=""></option>
									<?php	foreach($salesmode as $key):
									?>
								<option value="<?php echo $key->salesmodeid;?>" ><?php echo $key->salesmodename;?></option>
								<?php endforeach;?>				
								</select>
							</div>	
							<div class="static-field large-6 columns">
								<label>Calculation Type<span class="mandatoryfildclass">*</span></label>						
								<select id="transactionmodeid" name="transactionmodeid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="109">
									<option value=""></option>
									<?php foreach($transactionmode as $key): ?>
							<option value="<?php echo $key->transactionmodeid;?>">
							<?php echo $key->transactionmodename;?></option>
							<?php endforeach;?>
								</select>
							 </div>
							<div class="static-field large-6 columns ">
								<label>Default Type<span class="mandatoryfildclass stockreq">*</span></label>		
								<select id="stocktypeid" name="stocktypeid" class="chzn-select" data-validation-engine="" data-prompt-position="topLeft:14,36" tabindex="108">									
									<?php $prev = ' ';
								foreach($stocktype->result() as $key):
							?>
							<option value="<?php echo $key->stocktypeid;?>" data-label="<?php echo $key->stocktypelabel;?>"  data-shortname='<?php echo $key->shortname;?>' data-name="<?php echo $key->stocktypename;?>"><?php echo $key->stocktypename;?></option>
								<?php  endforeach;?>	
								</select>
							 </div>
							<div class="static-field large-12 columns transactioncalc hidedisplay">
								<label>Pure Weight<span class="mandatoryfildclass">*</span></label>						
								<select id="transactionpureweight" name="transactionpureweight" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="110" multiple>
									<option value=""></option>
									<?php foreach($additionalcharge as $key): ?>
							<option value="<?php echo $key->additionalchargeid;?>">
							<?php echo $key->additionalchargename;?></option>
							<?php endforeach;?>
								</select>
							 </div>
							<div class="static-field large-12 columns transactioncalc hidedisplay">
								<label>Total Amount<span class="mandatoryfildclass">*</span></label>						
								<select id="transactiontotalamount" name="transactiontotalamount" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="111" multiple>
									<option value=""></option>
									<?php foreach($additionalcharge as $key): ?>
							<option value="<?php echo $key->additionalchargeid;?>">
							<?php echo $key->additionalchargename;?></option>
							<?php endforeach;?>
								</select>
							 </div>
							<div class="static-field large-6 columns">
								<label>Display Fields</label>					
								<select id="displayfieldid" name="displayfieldid" class="chzn-select" data-validation-engine="" data-prompt-position="bottomLeft:14,36" tabindex="112" multiple>
									<option value=""></option>
									<?php foreach($displayfields as $key): ?>
							<option value="<?php echo $key->salesfieldsid;?>">
							<?php echo $key->salesfieldsname;?></option>
							<?php endforeach;?>
								</select>
							 </div>
							 <div class="static-field large-6 columns">
								<label>Rounding Types<span class="mandatoryfildclass">*</span></label>						
								<select id="roundingtype" name="roundingtype" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="113">
									<option value="1">Normal Rounding</option>
									<option value="2">Upward Rounding</option>
									<option value="3">Downward Rounding</option>
								</select>
							 </div>
							 <div class="static-field large-6 columns">
								<label>Roundoff Remove Zero<span class="mandatoryfildclass">*</span></label>						
								<select id="roundoffremove" name="roundoffremove" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="bottomLeft:14,36" tabindex="114">
									<option value="1">Yes</option>
									<option value="0">No</option>
								</select>
							 </div>
							 <div class="static-field large-6 columns">
								<label>Auto Calculate Tax<span class="mandatoryfildclass">*</span></label>
								<select class="chzn-select" id="transactionautotax" name="transactionautotax" data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36">
									<option value="1">Yes</option>
									<option value="0">No</option>
								</select>
							</div>
							<div class="static-field large-6 columns">
								<label>Tax Type<span class="mandatoryfildclass">*</span></label>
								<select class="chzn-select" id="taxmodeid" name="taxmodeid" data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36">
									<option value="1">Not Applicable</option>
									<?php foreach($taxtype as $key): ?>
										<option value="<?php echo $key->taxtypeid;?>">
										<?php echo $key->taxtypename;?></option>
									<?php endforeach;?>
								</select>
							</div>
							<div class="static-field large-6 columns">
								<label>Discount Type<span class="mandatoryfildclass">*</span></label>
								<select class="chzn-select" id="discountmodeid" name="discountmodeid" data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36">
									<option value="1">Not Applicable</option>
									<?php foreach($discountmode as $key): ?>
										<option value="<?php echo $key->discountmodeid;?>">
										<?php echo $key->discountmodename;?></option>
									<?php endforeach;?>
								</select>
							</div>
							<div class="static-field large-6 columns">
								<label>Discount<span class="mandatoryfildclass">*</span></label>
								<select class="chzn-select" id="discountdisplayid" name="discountdisplayid" data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36">
									<option value="1">Enable</option>
									<option value="2">Disable</option>
								</select>
							</div>
							<div class="static-field large-6 columns">
								<label>Discount Calc<span class="mandatoryfildclass">*</span></label>
								<select class="chzn-select" id="discountcalctypeid" name="discountcalctypeid" data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36">
									<option value="1">Not Applicable</option>
									<?php foreach($calctype as $key): ?>
										<option value="<?php echo $key->calculationtypeid;?>">
										<?php echo $key->calculationtypename;?></option>
									<?php endforeach;?>
								</select>
							</div>
							 <!--<div class="static-field large-6 columns">
									<label data-description="Error weight will be given in the purchase Weight Check overlay">Purchase Error Weight Limit</label>
									<input type="text" data-validation-engine="validate[custom[number],decval[<?php //echo $weight_round?>],min[0.001]]"  name="purchaseerrorwt" id="purchaseerrorwt" value=""/>
							</div>-->
							<div class="static-field large-12 columns">
								<input type="text" class="" id="allstocktypeid" name="allstocktypeid" value="" tabindex="107">
								<label for="allstocktypeid" data-description="give all the Stocktypeid u want to show.first one you give will be set as default">Allowed Stocktypeid</label>
							</div>
							<div class="static-field large-12 columns end hidedisplay">
								<input type="text" class="" id="allpaymentstocktypeid" name="allpaymentstocktypeid" value="" tabindex="107">
								<label for="allpaymentstocktypeid" data-description="give all the Payment Stocktypeid u want to show.first one you give will be set as default">Allowed Payment Stocktypeid</label>
							</div>
							<div class="static-field large-12 columns">
								<input type="text" class="" id="allowedaccounttypeid" name="allowedaccounttypeid" value="" tabindex="107">
								<label for="allowedaccounttypeid" data-description="give all the accountype accounts u want to show.">Allowed AccountTypeid</label>
							</div>
							<div class="static-field large-6 columns">
								<input type="text" class="" id="allprinttemplateid" name="allprinttemplateid" value="" tabindex="107">
								<label data-description="default printtemplateid for this type" for="allprinttemplateid">Print Templateid</label>
							</div>
							<div class="static-field large-6 columns">
								<input type="text" class="" id="serialmastername" name="serialmastername" value="" tabindex="107">
								<label for="serialmastername" data-description="Unique name of serialmaster should be updated. So for this it will apply from this serial master" >Serial Master Name</label>
							</div>
							<div class="static-field large-6 columns">
								<input type="text" class="" id="defaultforttype" name="defaultforttype" value="" tabindex="107">
								<label data-description="Set it as 1. if you want this to be autoselected in transactiontype change in transaction. When you give 1 rest of the entries for thsi type with 1 will be updated as 0."for="defaultforttype">Default For Transaction Type</label>
							</div>
							<div class="static-field large-6 columns">
								<input type="text" class="" id="payrecadvanceconcept" name="payrecadvanceconcept" value="" tabindex="107">
								<label data-description="Payment Receipt - Advance + Balance Due Receipt concept should be split. If it is yes, then the Serial Number will be generated for Normal Advance and Balance Due Receipt." for="payrecadvanceconcept">Paymt Rec - Advance Concept</label>
							</div>
							<div class="static-field large-6 columns">
								<input type="text" class="" id="payrecadvanceprintid" name="payrecadvanceprintid" value="" tabindex="107">
								<label data-description="If the Payment receipt Advance Concept is Yes, then it will take print id from here." for="payrecadvanceprintid">Pay Rec - Advance PrintID</label>
							</div>
							<div class="static-field large-6 columns end hidedisplay">
								<input type="text" class="" id="chargesedit" name="chargesedit" value="" tabindex="107">
								<label data-description="Wither u want charges to be editable or not.need to test and cross verify and update here.value is 0 for editable and 1 for not editable" for="chargesedit">Charges Edit</label>
							</div>
							<input type="hidden" name="transactionmanageid" id="transactionmanageid">
							 <?php if($device=='phone') { ?>
							</div>
							<div class="divider large-12 columns"></div>
							<div class="large-12 columns submitkeyboard sectionalertbuttonarea" style="text-align: right">
									<input id="transactiontypeadd" class="alertbtn addkeyboard formtogridtransaction" type="button" value="Submit" name="transactiontypeadd" tabindex="103">
									<input id="transactiontypeedit" class="alertbtn hidedisplay updatekeyboard formtogridtransaction" type="button" value="Submit" name="transactiontypeedit" tabindex="103" style="display: none;">
									<input id="transactionaddsectionclose"class="alertbtn addsectionclose cancelkeyboard" type="button" value="Cancel" name="cancel" tabindex="104">
									<input type="hidden" name="transactiontypeid" id="transactiontypeid">
								</div>
							<?php } else { ?> 
							<div class="large-12 columns">&nbsp;</div>
							<input type="hidden" name="transactiontypeid" id="transactiontypeid">
							<div class="large-12 columns " style="text-align:right">
								<label>&nbsp;</label>						
								<input type="button" class="btn leftformsbtn formtogridtransaction" id="transactiontypeadd" name="transactiontypeadd" value="SUBMIT" tabindex="120">
								<input type="button" class="btn leftformsbtn formtogridtransaction" style="display:none" id="transactiontypeedit" name="transactiontypeedit" value="SUBMIT" tabindex="121">
							</div>
							<?php } ?>
					</div>
				</div>
				<?php if($device=='phone') {?>
				</div>
				<?php }?> 
				<div class="large-8 columns end paddingbtm">
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff">
						<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0 0.4rem;">	
							   <div class="large-6 medium-6 small-6 columns text-left"> 
									<span style="line-height: 1.9rem !important;">Transaction Details <span class="fwgpaging-box transactiongridfootercontainer"></span></span>
							   </div>		
								<div class="large-6 medium-6 small-6 columns innergridicons text-right" style="margin-top: 5px;"> 
									<?php if($device=='phone') {?>
								  <span id="transactionaddicon" class="icon-box" title="Add" ><i class="material-icons">add</i></span>
								  <?php }?>
									<span id="transactiontypeediticon" class=" editiconclass"  title="Edit"> <i class="material-icons">edit</i></span>
									<span id="transactiontypedelete" class="deleteiconclass "  title="Delete"><i class="material-icons">delete</i></span>
									
								</div>
							</div>
								<?php
										$device = $this->Basefunctions->deviceinfo();
										if($device=='phone') {
											echo '<div class="large-12 columns paddingzero forgetinggridname" id="transactiongridwidth"><div class=" inner-row-content inner-gridcontent" id="transactiongrid" style="height:420px;top:0px;">
												<!-- Table header content , data rows & pagination for mobile-->
											</div>
											<footer class="inner-gridfooter footercontainer" id="transactiongridfooter">
												<!-- Footer & Pagination content -->
											</footer></div>';
										} else {
											echo '<div class="large-12 columns forgetinggridname" id="transactiongridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="transactiongrid" style="max-width:2000px; height:420px;top:0px;">
											<!-- Table content[Header & Record Rows] for desktop-->
											</div>
											<div class="inner-gridfooter footer-content footercontainer" id="transactiongridfooter">
												<!-- Footer & Pagination content -->
											</div></div>';
										}
									?>	
						</div>
				</div>
			</form>
		</div>
		<div id="overlaysubformspan8" class="hiddensubform hidedisplay transitionhiddenform">
			<form name="oldjewelsettingform" id="oldjewelsettingform" class="oldjewelsettingform validationEngineContainer"  >
				<?php if($device=='phone') {?>
				<div class="closed effectbox-overlay effectbox-default" id="oldjewelsectionoverlay">
				<?php }?>
				<div class="large-4 columns end paddingbtm">
					<div class="large-12 columns validationEngineContainer transactiontypeclear borderstyle cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;" id="transactiontypeaddvalidate">
							<?php if($device=='phone') {?>
							<div class="large-12 columns sectionheaderformcaptionstyle">Old Jewel Details</div>
							<div class="large-12 columns sectionpanel">
								<?php  } else { ?>
									<div class="large-12 columns headerformcaptionstyle">Old Jewel Details</div>
							<?php  	} ?> 																			
							 <div class="static-field large-12 columns ">
								<label>Transaction Type<span class="mandatoryfildclass">*</span></label>						
								<select id="jeweltransactiontype" name="jeweltransactiontype" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="107">
								<option value=""></option>
									<?php $prev = ' ';
									foreach($salestransactiontype->result() as $key):
									if($key->salestransactiontypeid == 11 || $key->salestransactiontypeid == 13 || $key->salestransactiontypeid == 16 || $key->salestransactiontypeid == 20) {
								?>
									<option value="<?php echo $key->salestransactiontypeid;?>"   data-stocktyperelation="<?php echo $key->stocktyperelationid;?>" data-name="<?php echo $key->salestransactiontypename;?>"><?php echo $key->salestransactiontypename;?></option>
									<?php } endforeach;?>				
								</select>
							</div>	
							 <div class="static-field large-12 columns ">
								<label>Old Item details<span class="mandatoryfildclass stockreq">*</span></label>		
								<select id="jeweltypeid" name="jeweltypeid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="108">									
									<option value=""></option>
									<?php foreach($olditemdetails as $key):?>
										<option value="<?php echo $key->olditemdetailsid;?>" data-name="<?php echo $key->olditemdetailsname;?>">
										<?php echo $key->olditemdetailsname;?></option>
									<?php endforeach;?>		
								</select>
							</div>
							 <div class="static-field large-12 columns ">
								<label>Purity<span class="mandatoryfildclass">*</span></label>		
								<select id="oldjewelpurityid" name="oldjewelpurityid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="112">
									<option value=""></option>								
									<?php $prev = ' ';
										foreach($purity->result() as $key):
										$cur = $key->metalid;
										$a="";
										if($prev != $cur ) {
											if($prev != " ") {
												echo '</optgroup>';
											}
											echo '<optgroup  label="'.$key->metalname.'" data-metalid="'.$key->metalid.'">';
											$prev = $key->metalid;
										}
										?>
										<option data-metal_id="" value="<?php echo $key->purityid; ?>"><?php echo $key->purityname;?></option>
										<?php endforeach;?>
								</select>
								</div>
							 <div class="static-field large-12 columns ">
									<label>Gross Amount Calculation<span class="mandatoryfildclass">*</span></label>		
									<select id="netweightcalculationid" name="netweightcalculationid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="112">									
										<option value=""></option>
											<?php foreach($netweightcalc as $key):?>
											<option value="<?php echo $key->netweightcalculationid;?>" data-name="<?php echo $key->netweightcalculationname;?>">
											<?php echo $key->netweightcalculationname;?></option>
											<?php endforeach;?>		
									</select>
							   </div>
							 <div class="static-field large-12 columns ">
									<label>Rate(less)<span class="mandatoryfildclass">*</span></label>		
									<input type="text" name="rateamount" id="rateamount" value="" data-validation-engine="validate[required]"/>
							   </div>
							 <div class="static-field large-12 columns ">
									<label>Weight Less(%)<span class="mandatoryfildclass">*</span></label>		
									<input type="text" name="weightless" id="weightless" value="" data-validation-engine="validate[required]"/>
								</div>	
								<?php if($device=='phone') { ?>
								</div>
								<div class="divider large-12 columns"></div>
								<div class="large-12 columns submitkeyboard sectionalertbuttonarea" style="text-align: right">
									<input id="oldjeweladd" class="alertbtn addkeyboard formtogridoldjewel" type="button" value="Submit" name="oldjeweladd" tabindex="103">
									<input id="oldjeweledit" class="alertbtn hidedisplay updatekeyboard formtogridoldjewel" type="button" value="Submit" name="oldjeweledit" tabindex="103" style="display: none;">
									<input id="oldjeweladdsectionclose"class="alertbtn addsectionclose cancelkeyboard" type="button" value="Cancel" name="cancel" tabindex="104">
										 <input type="hidden" name="oldjewelid" id="oldjewelid">
									</div>
										
								<?php } else { ?> 	
							 <div class="large-12 columns">&nbsp;</div>
							 <input type="hidden" name="oldjewelid" id="oldjewelid">
							 <div class="large-12 columns " style="text-align:right">
								<label>&nbsp;</label>						
								<input type="button" class="btn leftformsbtn formtogridoldjewel" id="oldjeweladd" name="oldjeweladd" value="SUBMIT" tabindex="120">
								<input type="button" class="btn leftformsbtn formtogridoldjewel" style="display:none" id="oldjeweledit" name="oldjeweledit" value="SUBMIT" tabindex="121">
							</div>
							<?php } ?>
					</div>
				</div>
				<?php if($device=='phone') {?>
				</div>
				<?php }?> 
				<div class="large-8 columns end paddingbtm">
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff">
							<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0rem 0rem;height: auto;line-height: 0.1rem;">	
							   <div class="large-6 medium-6 small-6 columns text-left"> 
									<span style="line-height: 1.9rem !important;">Old Jewel Details <span class="fwgpaging-box oldjewelgridfootercontainer"></span></span>
							   </div>		
								<div class="large-6 medium-6 small-6 columns innergridicons text-right" style="margin-top: 4px;"> 
									<?php if($device=='phone') {?>
								  <span id="oldjeweladdicon" class="icon-box" title="Add" ><i class="material-icons">add</i></span>
								  <?php }?>
								  <span id="oldjewelediticon" class=" editiconclass"  title="Edit"> <i class="material-icons">edit</i></span>
								 <span id="oldjeweldelete" class="deleteiconclass "  title="Delete"><i class="material-icons">delete</i></span>
								</div>
							</div>
								<?php
										$device = $this->Basefunctions->deviceinfo();
										if($device=='phone') {
											echo '<div class="large-12 columns paddingzero forgetinggridname" id="oldjewelgridwidth"><div class=" inner-row-content inner-gridcontent" id="oldjewelgrid" style="height:420px;top:0px;">
												<!-- Table header content , data rows & pagination for mobile-->
											</div>
											<footer class="inner-gridfooter footercontainer" id="oldjewelgridfooter">
												<!-- Footer & Pagination content -->
											</footer></div>';
										} else {
											echo '<div class="large-12 columns forgetinggridname" id="oldjewelgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="oldjewelgrid" style="max-width:2000px; height:420px;top:0px;">
											<!-- Table content[Header & Record Rows] for desktop-->
											</div>
											<div class="inner-gridfooter footer-content footercontainer" id="oldjewelgridfooter">
												<!-- Footer & Pagination content -->
											</div></div>';
										}
									?>	
						</div>
				</div>
			</form>
		</div>
		<div id="overlaysubformspan9" class="hiddensubform hidedisplay transitionhiddenform">
			<form name="ratesettingform" id="ratesettingform" class="ratesettingform validationEngineContainer">
				<?php if($device=='phone') {?>
				<div class="closed effectbox-overlay effectbox-default" id="ratecalcsectionoverlay">
				<?php }?>
				<div class="large-4 columns end paddingbtm">
					<div class="large-12 columns validationEngineContainer transactiontypeclear borderstyle cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;" id="transactiontypeaddvalidate">
							<?php if($device=='phone') {?>
							<div class="large-12 columns sectionheaderformcaptionstyle">Rate Details</div>
							<div class="large-12 columns sectionpanel">
								<?php  } else { ?>
									<div class="large-12 columns headerformcaptionstyle">Rate Details</div>
							<?php  	} ?>
							<div class="static-field large-12 columns ">
								<label>Primary Purity<span class="mandatoryfildclass">*</span></label>		
								<select id="primarypurityid" name="primarypurityid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="112">
									<option value=""></option>
									<?php $prev = ' ';
										foreach($primarypurity->result() as $key):
										$cur = $key->metalid;
										$a="";
										if($prev != $cur ) {
											if($prev != " ") {
												echo '</optgroup>';
											}
											echo '<optgroup  label="'.$key->metalname.'" data-metalid="'.$key->metalid.'">';
											$prev = $key->metalid;
										}
										?>
										<option data-metal_id="<?php echo $key->metalid; ?>" value="<?php echo $key->purityid; ?>"><?php echo $key->purityname;?></option>
										<?php endforeach;?>
								</select>
							</div>
							<div class="static-field large-12 columns ">
								<label>Primary Purity<span class="mandatoryfildclass">*</span></label>		
								<select id="nonprimarypurityid" name="nonprimarypurityid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="112">
									<option value=""></option>
									<?php $prev = ' ';
										foreach($nonprimarypurity->result() as $key): ?>
											<option data-metal_id="<?php echo $key->metalid; ?>" value="<?php echo $key->purityid; ?>"><?php echo $key->purityname;?></option>
										<?php endforeach;?>
								</select>
							</div>
							<div class="static-field large-12 columns ">
								<label>Amount Calculation<span class="mandatoryfildclass">*</span></label>		
								<select id="ratecalculationid" name="ratecalculationid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="112">									
									<option value=""></option>
										<?php foreach($ratecalculation as $key):?>
										<option value="<?php echo $key->ratecalculationid;?>" data-name="<?php echo $key->ratecalculationname;?>">
										<?php echo $key->ratecalculationname;?></option>
										<?php endforeach;?>		
								</select>
							</div>
							<div class="static-field large-12 columns ">
								<label>Rate Percent<span class="mandatoryfildclass">*</span></label>		
								<input type="text" name="ratepercentvalue" id="ratepercentvalue" value="" data-validation-engine="validate[required]"/>
							</div>
							<?php if($device=='phone') { ?>
								</div>
								<div class="divider large-12 columns"></div>
								<div class="large-12 columns submitkeyboard sectionalertbuttonarea" style="text-align: right">
									<input id="ratecalcadd" class="alertbtn addkeyboard formtogridratecalc" type="button" value="Submit" name="ratecalcadd" tabindex="103">
									<input id="ratecalcedit" class="alertbtn hidedisplay updatekeyboard formtogridratecalc" type="button" value="Submit" name="ratecalcedit" tabindex="103" style="display: none;">
									<input id="rateaddsectionclose"class="alertbtn addsectionclose cancelkeyboard" type="button" value="Cancel" name="cancel" tabindex="104">
										 <input type="hidden" name="ratecalcid" id="ratecalcid">
									</div>
										
								<?php } else { ?> 	
							 <div class="large-12 columns">&nbsp;</div>
							 <input type="hidden" name="ratecalcid" id="ratecalcid">
							 <div class="large-12 columns " style="text-align:right">
								<label>&nbsp;</label>						
								<input type="button" class="btn leftformsbtn formtogridratecalc" id="ratecalcadd" name="ratecalcadd" value="SUBMIT" tabindex="120">
								<input type="button" class="btn leftformsbtn formtogridratecalc" style="display:none" id="ratecalcedit" name="ratecalcedit" value="SUBMIT" tabindex="121">
							</div>
							<?php } ?>
					</div>
				</div>
				<?php if($device=='phone') {?>
				</div>
				<?php }?> 
				<div class="large-8 columns end paddingbtm">
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff">
							<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0rem 0rem;height: auto;line-height: 0.1rem;">	
							   <div class="large-6 medium-6 small-6 columns text-left"> 
									<span style="line-height: 1.9rem !important;">Rate Details <span class="fwgpaging-box ratecalcgridfootercontainer"></span></span>
							   </div>		
								<div class="large-6 medium-6 small-6 columns innergridicons text-right" style="margin-top: 4px;"> 
									<?php if($device=='phone') {?>
								  <span id="ratecalcaddicon" class="icon-box" title="Add" ><i class="material-icons">add</i></span>
								  <?php }?>
								  <span id="ratecalcediticon" class=" editiconclass"  title="Edit"> <i class="material-icons">edit</i></span>
								 <span id="ratecalcdelete" class="deleteiconclass "  title="Delete"><i class="material-icons">delete</i></span>
								</div>
							</div>
								<?php
										$device = $this->Basefunctions->deviceinfo();
										if($device=='phone') {
											echo '<div class="large-12 columns paddingzero forgetinggridname" id="ratecalcgridwidth"><div class=" inner-row-content inner-gridcontent" id="ratecalcgrid" style="height:420px;top:0px;">
												<!-- Table header content , data rows & pagination for mobile-->
											</div>
											<footer class="inner-gridfooter footercontainer" id="ratecalcgridfooter">
												<!-- Footer & Pagination content -->
											</footer></div>';
										} else {
											echo '<div class="large-12 columns forgetinggridname" id="ratecalcgridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="ratecalcgrid" style="max-width:2000px; height:420px;top:0px;">
											<!-- Table content[Header & Record Rows] for desktop-->
											</div>
											<div class="inner-gridfooter footer-content footercontainer" id="ratecalcgridfooter">
												<!-- Footer & Pagination content -->
											</div></div>';
										}
									?>	
						</div>
				</div>
			</form>
		</div>
		</div>
	</div>		
</div>