<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Header Files -->	
	<?php $this->load->view('Base/headerfiles'); ?>
	<?php 
		$industryid = $this->Basefunctions->industryid;
		if($industryid == 3) {
	?>
			<link href="<?php echo base_url();?>css/jqgrid.min.css" media="screen" rel="stylesheet" type="text/css" />
	<?php 
		}
	?>
</head>
<body>
	<?php
		$moduleid = implode(',',$moduleids);
		$device = $this->Basefunctions->deviceinfo();
		$dataset['gridenable'] = "yes"; //no
		$dataset['griddisplayid'] = "companygriddisplay";
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['spanattr'] = array();
		$dataset['gridtableid'] = "companygrid";
		$dataset['griddivid'] = "companygriddiv";
		$dataset['moduleid'] = $moduleids;
		if($industryid == 3) {
			$dataset['forminfo'] = array(array('id'=>'companyaddformdiv','class'=>'hidedisplay','formname'=>'companyform'),array('id'=>'companysettingview','class'=>'hidedisplay','formname'=>'companysettingview'));
		} else{
			$dataset['forminfo'] = array(array('id'=>'companyaddformdiv','class'=>'hidedisplay','formname'=>'companyform'));
		}
		if($device=='phone') {
			$this->load->view('Base/gridmenuheadermobile',$dataset);
			$this->load->view('Base/overlaymobile');
			$this->load->view('Base/viewselectionoverlay');
		} else {
			$this->load->view('Base/gridmenuheader',$dataset);
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
		$this->load->view('Base/basedeleteform');
	?>
</body>
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="companyalerts">	
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="overlaybackground">
			<div class="alert-panel">
				<div class="alertmessagearea">
				<div class="row">&nbsp;</div>
				<div class="alert-message otheralertinputstyle">Only One Company Can be Created</div>
				</div>
				<div class="alertbuttonarea">
					<input type="button" id="alertsclose" tabindex="1001"name="" value="Ok" class="alertbtn flloop alertsoverlaybtn" >
				</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>
			</div>
		</div>
	</div>
</div>

<!-- Release Details Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts borderstyle" id="releasedetailsoverlay" style="overflow-y: scroll;overflow-x: hidden">
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="large-10 medium-10 small-10 large-centered medium-centered columns overlayborder" >
			<div class="modal ajaxviewModel" role="dialog" data-keyboard="false" data-backdrop="static"  tabindex="-1">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header modalheader" style="font-weight: bolder;">
							<h5 class="modal-title text-center modalheading" data-background-color="purple"></h5>
						<!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					   </button> -->
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-12 ajaxupdateData"></div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--CodeVerification Details OVerlay via Email -->
<div class="large-12 small-12 medium-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="verificationcodeoverlay" style="overflow-y: scroll;overflow-x: hidden">
		<div class="row sectiontoppadding">&nbsp;</div>
		<div class="large-4 medium-4 small-12 large-centered medium-centered small-centered columns ">
			<div class="large-12 column paddingzero">
				<div class="row">&nbsp;</div>
				<form method="POST" name="" style="" id="" action="" enctype="" class="">
					<div id="validateverificationcode" class="validationEngineContainer">
						<div class="large-12 columns end paddingbtm ">
							<div class="large-12 columns cleardataform z-depth-5 border-modal-8px" style="background: #ffffff;border-radius: 8px !important;">
								<div class="large-12 columns sectionheaderformcaptionstyle">Verification Code Details</div>
								<div class="large-12 columns sectionpanel" style="padding: 15px 15px;">
									<div class="input-field large-12 columns">
										<input type="text" class="" id="checkverificationcode" name="checkverificationcode" value="" tabindex="109" data-validation-engine="validate[required,custom[onlyWholeNumber],minSize[10],maxSize[10]]">
										<label for="checkverificationcode">Enter Verification Code</label>
									</div>
								</div>
								<div class="divider large-12 columns"></div>
								<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">
									<input type="button" class="alertbtnyes leftformsbtn verificationcodesubmit" id="verificationcodesubmit" name="" value="Submit" tabindex="114">
									<input type="button" class="alertbtnno addsectionclose overlayclosedetails" id="overlayclosedetails" value="Cancel" name="cancel" tabindex="115">
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Update Proceeding Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="updateproceedoverlay">	
		<div class="row">&nbsp;</div>
		<div class="overlaybackground">
			<div class="alert-panel">
				<div class="alertmessagearea">
				<div class="row">&nbsp;</div>
					<div class="alert-message">Are you sure want to update software?</div>
				</div>
				<div class="alertbuttonarea">
					<span class="firsttab" tabindex="1000"></span>
					<input type="button" id="updateproceedyes" name="" value="Yes" tabindex="1001" class="alertbtnyes ffield" >
					<input type="button" id="updateproceedno" name="" value="No" tabindex="1002" class="alertbtnno flloop  alertsoverlaybtn" >
					<span class="lasttab" tabindex="1003"></span>
				</div>
				<div class="row">&nbsp;</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('Base/bottomscript'); ?>
<!-- js Files -->	
<script src="<?php echo base_url();?>js/Company/company.js" type="text/javascript"></script>
<!--For Tablet and Mobile view Dropdown Script-->
<script>
	$(document).ready(function(){
		$("#tabgropdropdown").change(function(){
			var tabgpid = $("#tabgropdropdown").val(); 
			$('.sidebaricons[data-subform="'+tabgpid+'"]').trigger('click');
		});
		$("#tabgropdropdowncompanysetting").change(function(){
			var tabgpid = $("#tabgropdropdowncompanysetting").val();
			$('.sidebar[data-subforms="'+tabgpid+'"]').trigger('click');
		});
	});
</script>
</html>
