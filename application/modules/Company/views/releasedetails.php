<?php
if(isset($serverversionList)) {
	if((!empty($serverversionList))) {
   ?>
	<div class="row ">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
			<span style="color: black;"><b>Release Version List Available</b></span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<table class="table table-bordered">
				<thead>
					<tr style="text-align:center;background-color: purple;color: white;font-weight: bolder">
						<th style="text-align: center !important" width="30%">Version No.</th>
						<th style="text-align: center !important" width="50%">Release Notes</th>
						<th style="text-align: center !important" width="20%">Version Date</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($serverversionList as $row):?>
					<tr style="text-align:center;color: black;font-weight: bolder">
						<td><?php echo $row['versionno'] ?></td>
						<td><?php echo $row['releasenotes'] ?></td>
						<td><?php echo $row['versiondate'] ?></td>
					</tr>
				<?php endforeach;  ?>
				</tbody>
			</table>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
			<input type="hidden" id="hiddencompanyid" value="<?php echo $companyid ?>" />
			<button type="submit" name="update" class="btn btn-primary checkamcdetails" randomcodegeneration='no' cidAttr='<?php echo $companyid ?>'>Update</button>
			<button type="submit" name="close" class="btn btn-primary overlayclose" randomcodegeneration='no' cidAttr='<?php echo $companyid ?>'>Close</button>
		</div>
	</div>
<?php } else { ?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
			<span style="color: black;"><b>No Latest Versions available for your plan!</b></span>
		</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
		<button type="submit" name="close" class="btn btn-primary overlayclose" randomcodegeneration='no' cidAttr='<?php echo $companyid ?>'>Close</button>
	</div>
<?php }
} ?>