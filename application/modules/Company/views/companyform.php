<div class="large-12 columns paddingzero formheader">
	<?php
		$this->load->view('Base/mainviewaddformheader');
	?>
</div>
<div class="large-12 columns addformunderheader">&nbsp;</div>
<div class="large-12 columns scrollbarclass addformcontainer tabtouchaddcontainer">
	<div class="row">&nbsp;</div>
	<form method="POST" name="dataaddform" class="" action ="" id="dataaddform">
		<span id="formaddwizard" class="validationEngineContainer" >
			<span id="formeditwizard" class="validationEngineContainer">
				<?php
                    //function call for form fields generation
					formfieldstemplategenerator($modtabgrp);
                ?>
			</span>
		</span>
		<!--hidden values-->
		<input type="hidden" value="<?php echo $gstapplicable?>" name="gstapplicableid" id="gstapplicableid" />
		<?php
			echo hidden('primarydataid','');
			echo hidden('sortorder','');
			echo hidden('sortcolumn','');
			//hidden text box view columns field module ids
			echo hidden('companycount','');
			$modid = $this->Basefunctions->getmoduleid($filedmodids);
			echo hidden('viewfieldids',$modid);
			$industryid = $this->Basefunctions->industryid;
			if($industryid == 3) {
				echo hidden('defaultproductchargeid',$chargedataid);
				echo hidden('defaultproductpurchasechargeid',$purchasechargedataid);
			}
		?>
	</form>
</div>