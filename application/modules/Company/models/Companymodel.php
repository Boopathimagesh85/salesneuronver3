<?php
class Companymodel extends CI_Model {
 private $companymodule = 1;
	function __construct() {
		parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
	}
	//company create
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
		$primaryid = $this->db->insert_id();
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' created Company - '.$_POST['companyname'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,2);
		echo 'TRUE';
	}
	//Retrive data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['dataprimaryid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	//company update
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['primarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' updated Company - '.$_POST['companyname'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$activity ,$userid,2);
		echo $result;
	}
	//company count fetch
	public function companycountfetchmodel() {
		$this->db->select('count(companyid) as ccount');
		$this->db->from('company');
		$this->db->where('status',1);
		$result = $this->db->get();
		foreach($result->result() as $data) {
			$count = $data->ccount;
		}
		echo $count;
	}
	public function companysettingupdate() {
		$id=$_POST['companysettingid'];
		if(empty($_POST['lotcreationopt'])) {
			$_POST['lotcreationopt']='1';
		}else {
			$_POST['lotcreationopt']=$_POST['lotcreationopt'];
		}
		if(empty($_POST['lotdetailsopt'])) {
			$_POST['lotdetailsopt']='NO';
		}else {
			$_POST['lotdetailsopt']=$_POST['lotdetailsopt'];
		}
		if(empty($_POST['counteroption'])) {
			$_POST['counteroption']='NO';
		}else {
			$_POST['counteroption']=$_POST['counteroption'];
		}
		if(empty($_POST['stoneoption'])) {
			$_POST['stoneoption']='NO';
		}else {
			$_POST['stoneoption']=$_POST['stoneoption'];
		}
		if(empty($_POST['imageoption'])) {
			$_POST['imageoption']='NO';
		}else {
			$_POST['imageoption']=$_POST['imageoption'];
		}
		if(empty($_POST['scaleoption'])) {
			$_POST['scaleoption']='No';
		}else {
			$_POST['scaleoption']=$_POST['scaleoption'];
		}
		if(empty($_POST['netwtcalctype'])) {
			$_POST['netwtcalctype']=3;
		}else {
			$_POST['netwtcalctype']=$_POST['netwtcalctype'];
		}
		if(empty($_POST['taxoption'])) {
			$_POST['taxoption']=1;
		}else {
			$_POST['taxoption']=$_POST['taxoption'];
		}
		if(empty($_POST['pdfoption'])) {
			$_POST['pdfoption']='NO';
		}else {
			$_POST['pdfoption']=$_POST['pdfoption'];
		}
		if(empty($_POST['tagtype'])) {
			$_POST['tagtype']=0;
		}else {
			$_POST['tagtype']=$_POST['tagtype'];
		}
		if(empty($_POST['needezetap'])) {
			$_POST['needezetap']='NO';
		}else {
			$_POST['needezetap']=$_POST['needezetap'];
		}
		if(empty($_POST['needcashcheque'])) {
			$_POST['needcashcheque']='NO';
		}else {
			$_POST['needcashcheque']=$_POST['needcashcheque'];
		}
		if(empty($_POST['askemailsmsfields'])) {
			$_POST['askemailsmsfields']='NO';
		}else {
			$_POST['askemailsmsfields']=$_POST['askemailsmsfields'];
		}
		if(empty($_POST['ezetapnotification'])) {
			$_POST['ezetapnotification']='NO';
		}else {
			$_POST['ezetapnotification']=$_POST['ezetapnotification'];
		}
		if(empty($_POST['counterlevel'])) {
			$_POST['counterlevel']=0;
		}else {
			$_POST['counterlevel']=$_POST['counterlevel'];
		}
		if(empty($_POST['categorylevel'])) {
			$_POST['categorylevel']=0;
		}else {
			$_POST['categorylevel']=$_POST['categorylevel'];
		}
		if(empty($_POST['stonelevel'])) {
			$_POST['stonelevel']=0;
		}else {
			$_POST['stonelevel']=$_POST['stonelevel'];
		}
		if(empty($_POST['purchasedata'])) {
			$_POST['purchasedata']='NO';
		}else {
			$_POST['purchasedata']=$_POST['purchasedata'];
		}
		if(empty($_POST['olditemdata'])) {
			$_POST['olditemdata']='';
		}else {
			$_POST['olditemdata']=$_POST['olditemdata'];
		}
		if(empty($_POST['lotdata'])) {
			$_POST['lotdata']='2';
		}else {
			$_POST['lotdata']=$_POST['lotdata'];
		}
		if(empty($_POST['barcodeoption'])) {
			$_POST['barcodeoption']='1';
		}else {
			$_POST['barcodeoption']=$_POST['barcodeoption'];
		}
		if(empty($_POST['bullionproductdata'])) {
			$_POST['bullionproductdata']='';
		}else {
			$_POST['bullionproductdata']=$_POST['bullionproductdata'];
		}
		if(empty($_POST['billingtypeid'])) {
			$_POST['billingtypeid']='1';
		} else {
			$_POST['billingtypeid']=$_POST['billingtypeid'];
		}
		if(empty($_POST['defaultbankid'])) {
			$_POST['defaultbankid']='10';
		} else {
			$_POST['defaultbankid']=$_POST['defaultbankid'];
		}
		if(empty($_POST['defaultcashinhandid'])) {
			$_POST['defaultcashinhandid']='11';
		} else {
			$_POST['defaultcashinhandid']=$_POST['defaultcashinhandid'];
		}
		if(empty($_POST['salesreturntypeid'])) {
			$_POST['salesreturntypeid']='1';
		} else {
			$_POST['salesreturntypeid']=$_POST['salesreturntypeid'];
		}
		if(empty($_POST['oldjewelproducttype'])) {
			$_POST['oldjewelproducttype']='1';
		} else {
			$_POST['oldjewelproducttype']=$_POST['oldjewelproducttype'];
		}
		if(empty($_POST['schemestatus'])) {
			$_POST['schemestatus']='NO';
		}else {
			$_POST['schemestatus']=$_POST['schemestatus'];
		}
		if(empty($_POST['categoryid'])) {
			$_POST['categoryid']=0;
		}else {
			$_POST['categoryid']=$_POST['categoryid'];
		}
		if(empty($_POST['approvaloutcalculation'])) {
			$_POST['approvaloutcalculation']='NO';
		}else {
			$_POST['approvaloutcalculation']=$_POST['approvaloutcalculation'];
		}
		if(empty($_POST['schememethodid'])) {
			$_POST['schememethodid']=2;
		} else {
			$_POST['schememethodid']=$_POST['schememethodid'];
		}
		if(empty($_POST['schemetypeid'])) {
			if($_POST['schememethodid'] == 2){
				$_POST['schemetypeid']=2;
			} else{
				$_POST['schemetypeid']=0;
			}
		} else {
			$_POST['schemetypeid']=$_POST['schemetypeid'];
		}
		if(empty($_POST['stocknotify'])) {
			$_POST['stocknotify']='NO';
		} else {
			$_POST['stocknotify']=$_POST['stocknotify'];
		}
		if(empty($_POST['productidshow'])) {
			$_POST['productidshow']='YES';
		} else {
			$_POST['productidshow']=$_POST['productidshow'];
		}
		if(empty($_POST['vacchargeshow'])) {
			$_POST['vacchargeshow']='NO';
		} else {
			$_POST['vacchargeshow']=$_POST['vacchargeshow'];
		}
		if(empty($_POST['chargeaccountgroup'])) {
			$_POST['chargeaccountgroup']=0;
		} else {
			$_POST['chargeaccountgroup']=$_POST['chargeaccountgroup'];
		}
		if(empty($_POST['taxtypeid'])) {
			$_POST['taxtypeid']=1;
		} else {
			$_POST['taxtypeid']=$_POST['taxtypeid'];
		}
		if(empty($_POST['discounttypeid'])) {
			$_POST['discounttypeid']=1;
		} else {
			$_POST['discounttypeid']=$_POST['discounttypeid'];
		}
		if(empty($_POST['purchasetransactionmodedata'])) {
			$_POST['purchasetransactionmodedata']='';
		} else {
			$_POST['purchasetransactionmodedata']=$_POST['purchasetransactionmodedata'];
		}
		if(empty($_POST['oldjewelsumtaxapplyid'])) {
			$_POST['oldjewelsumtaxapplyid']='1';
		} else {
			$_POST['oldjewelsumtaxapplyid']=$_POST['oldjewelsumtaxapplyid'];
		}
		if(empty($_POST['oldjewelsumtaxid'])) {
			$_POST['oldjewelsumtaxid']='1';
		} else {
			$_POST['oldjewelsumtaxid']=$_POST['oldjewelsumtaxid'];
		}
		if(empty($_POST['estimateconcept'])) {
			$_POST['estimateconcept']='No';
		} else {
			$_POST['estimateconcept']=$_POST['estimateconcept'];
		}
		if(empty($_POST['lotpieces'])) {
			$_POST['lotpieces']='No';
		}else {
			$_POST['lotpieces']=$_POST['lotpieces'];
		}
		if(empty($_POST['purchasewastage'])) {
			$_POST['purchasewastage']='No';
		}else {
			$_POST['purchasewastage']=$_POST['purchasewastage'];
		}
		if(empty($_POST['salesrate'])) {
			$_POST['salesrate']='1';
		}else {
			$_POST['salesrate']=$_POST['salesrate'];
		}
		if(empty($_POST['purchaserate'])) {
			$_POST['purchaserate']='1';
		}else {
			$_POST['purchaserate']=$_POST['purchaserate'];
		}
		if(empty($_POST['paymentmodetype'])) {
			$_POST['paymentmodetype']='1';
		}else {
			$_POST['paymentmodetype']=$_POST['paymentmodetype'];
		}
		if(empty($_POST['oldjewelvoucher'])) {
			$_POST['oldjewelvoucher']='0';
		}else {
			$_POST['oldjewelvoucher']=$_POST['oldjewelvoucher'];
		}
		if(empty($_POST['discountauthorization'])) {
			$_POST['discountauthorization']='0';
		}else {
			$_POST['discountauthorization']=$_POST['discountauthorization'];
		}
		if(empty($_POST['taxchargeinclude'])) {
			$_POST['taxchargeinclude']='0';
		}else {
			$_POST['taxchargeinclude']=$_POST['taxchargeinclude'];
		}
		if(empty($_POST['discountpassword'])) {
			$_POST['discountpassword']='0';
		}else {
			$_POST['discountpassword']=$_POST['discountpassword'];
		}
		if(empty($_POST['discountcalc'])) {
			$_POST['discountcalc']='0';
		}else {
			$_POST['discountcalc']=$_POST['discountcalc'];
		}
		if(empty($_POST['taxcalc'])) {
			$_POST['taxcalc']='0';
		}else {
			$_POST['taxcalc']=$_POST['taxcalc'];
		}
		if(empty($_POST['storagequantity'])) {
			$_POST['storagequantity']='0';
		}else {
			$_POST['storagequantity']=$_POST['storagequantity'];
		}
		if(empty($_POST['purewtcalctype'])) {
			$_POST['purewtcalctype']='1';
		}else {
			$_POST['purewtcalctype']=$_POST['purewtcalctype'];
		}
		if(empty($_POST['oldjeweldetails'])) {
			$_POST['oldjeweldetails']='0';
		}else {
			$_POST['oldjeweldetails']=$_POST['oldjeweldetails'];
		}
		if(empty($_POST['storagedisplay'])) {
			$_POST['storagedisplay']='1';
		}else {
			$_POST['storagedisplay']=$_POST['storagedisplay'];
		}
		if(empty($_POST['wastagecalctype'])) {
			$_POST['wastagecalctype']='1';
		}else {
			$_POST['wastagecalctype']=$_POST['wastagecalctype'];
		}
		if(empty($_POST['gstapplicable'])) {
			$_POST['gstapplicable']='2';
		}else {
			$_POST['gstapplicable']=$_POST['gstapplicable'];
		}
		if(empty($_POST['grossweighttrigger'])) {
			$_POST['grossweighttrigger']='0';
		}else {
			$_POST['grossweighttrigger']=$_POST['grossweighttrigger'];
		}
		if(empty($_POST['productroltypeid'])) {
			$_POST['productroltypeid']='0';
		}else {
			$_POST['productroltypeid']=$_POST['productroltypeid'];
		}
		if(empty($_POST['reorderqty'])) {
			$_POST['reorderqty']=0;
		}else {
			$_POST['reorderqty']=$_POST['reorderqty'];
		}
		if(empty($_POST['salesdatechangeablefor'])) {
			$_POST['salesdatechangeablefor']=1;
		}else {
			$_POST['salesdatechangeablefor']=$_POST['salesdatechangeablefor'];
		}
		if(empty($_POST['purchasedisplay'])) {
			$_POST['purchasedisplay']=0;
		}else {
			$_POST['purchasedisplay']=$_POST['purchasedisplay'];
		}
		if(empty($_POST['edittagcharge'])) {
			$_POST['edittagcharge']=0;
		}else {
			$_POST['edittagcharge']=$_POST['edittagcharge'];
		}
		if(empty($_POST['lockingtime'])) {
			$_POST['lockingtime']='00:00:00';
		}else {
			$_POST['lockingtime']=$_POST['lockingtime'];
		}
		if(empty($_POST['updatingtime'])) {
			$_POST['updatingtime']='00:00:00';
		}else {
			$_POST['updatingtime']=$_POST['updatingtime'];
		}
		if(empty($_POST['chargerequired'])) {
			$_POST['chargerequired']=0;
		}else {
			$_POST['chargerequired']=$_POST['chargerequired'];
		}
		if(empty($_POST['deviceno'])) {
			$_POST['deviceno']='';
		}else {
			$_POST['deviceno']=$_POST['deviceno'];
		}
		if(empty($_POST['orderimageoption'])) {
			$_POST['orderimageoption']=0;
		}else {
			$_POST['orderimageoption']=$_POST['orderimageoption'];
		}
		if(empty($_POST['transactionimageoption'])) {
			$_POST['transactionimageoption']=0;
		}else {
			$_POST['transactionimageoption']=$_POST['transactionimageoption'];
		}
		if(empty($_POST['weightcheck'])) {
			$_POST['weightcheck']=0;
		}else {
			$_POST['weightcheck']=$_POST['weightcheck'];
		}if(empty($_POST['loosestonecharge'])) {
			$_POST['loosestonecharge']=1;
		}else {
			$_POST['loosestonecharge']=$_POST['loosestonecharge'];
		}
		if(empty($_POST['loosesaleschargedata'])) {
			$_POST['loosesaleschargedata']='';
		}else {
			$_POST['loosesaleschargedata']=$_POST['loosesaleschargedata'];
		}
		if(empty($_POST['loosepurchasechargedata'])) {
			$_POST['loosepurchasechargedata']='';
		}else {
			$_POST['loosepurchasechargedata']=$_POST['loosepurchasechargedata'];
		}
		if(empty($_POST['stoneweightcheck'])) {
			$_POST['stoneweightcheck']='Yes';
		}else {
			$_POST['stoneweightcheck']=$_POST['stoneweightcheck'];
		}
		if(empty($_POST['tagautoimage'])) {
			$_POST['tagautoimage']='0';
		}else {
			$_POST['tagautoimage']=$_POST['tagautoimage'];
		}
		if(empty($_POST['drivename'])) {
			$_POST['drivename']='';
		}else {
			$_POST['drivename']=$_POST['drivename'];
		}
		if(empty($_POST['itemtagauthuserroleid'])) {
			$_POST['itemtagauthuserroleid']= '2';
		}else {
			$_POST['itemtagauthuserroleid'] = $_POST['itemtagauthuserroleid'];
		}
		if(empty($_POST['tagchargesfrom'])) {
			$_POST['tagchargesfrom']= '';
		}else {
			$_POST['tagchargesfrom'] = $_POST['tagchargesfrom'];
		}
		if(empty($_POST['rfidnoofdigits'])) {
			$_POST['rfidnoofdigits']= '';
		}else {
			$_POST['rfidnoofdigits'] = $_POST['rfidnoofdigits'];
		}
		if(empty($_POST['pancardrestrictamt'])) {
			$_POST['pancardrestrictamt']= '';
		}else {
			$_POST['pancardrestrictamt'] = $_POST['pancardrestrictamt'];
		}
		if(empty($_POST['cashreceiptlimit'])) {
			$_POST['cashreceiptlimit']= '';
		}else {
			$_POST['cashreceiptlimit'] = $_POST['cashreceiptlimit'];
		}
		if(empty($_POST['cashissuelimit'])) {
			$_POST['cashissuelimit']= '';
		}else {
			$_POST['cashissuelimit'] = $_POST['cashissuelimit'];
		}
		if(empty($_POST['touchchargesedit'])) {
			$_POST['touchchargesedit']= '';
		}else {
			$_POST['touchchargesedit'] = $_POST['touchchargesedit'];
		}
		if(empty($_POST['discountbilllevelpercent'])) {
			$_POST['discountbilllevelpercent']= '';
		}else {
			$_POST['discountbilllevelpercent'] = $_POST['discountbilllevelpercent'];
		}
		if(empty($_POST['untagwtvalidate'])) {
			$_POST['untagwtvalidate']= '';
		}else {
			$_POST['untagwtvalidate'] = $_POST['untagwtvalidate'];
		}
		if(empty($_POST['untagwtshow'])) {
			$_POST['untagwtshow']= '';
		}else {
			$_POST['untagwtshow'] = $_POST['untagwtshow'];
		}
		if(empty($_POST['approvaloutrfid'])) {
			$_POST['approvaloutrfid']= '';
		}else {
			$_POST['approvaloutrfid'] = $_POST['approvaloutrfid'];
		}
		if(empty($_POST['salesauthuserrole'])) {
			$_POST['salesauthuserrole']= '';
		}else {
			$_POST['salesauthuserrole'] = $_POST['salesauthuserrole'];
		}
		if(empty($_POST['chituserroleauth'])) {
			$_POST['chituserroleauth']= '';
		}else {
			$_POST['chituserroleauth'] = $_POST['chituserroleauth'];
		}
		if(empty($_POST['multitagbarcode'])) {
			$_POST['multitagbarcode']= '';
		}else {
			$_POST['multitagbarcode'] = $_POST['multitagbarcode'];
		}
		if(empty($_POST['estimatequicktotal'])) {
			$_POST['estimatequicktotal']= '';
		}else {
			$_POST['estimatequicktotal'] = $_POST['estimatequicktotal'];
		}
		if(empty($_POST['estimatedefaultset'])) {
			$_POST['estimatedefaultset']= '';
		}else {
			$_POST['estimatedefaultset'] = $_POST['estimatedefaultset'];
		}
		if(empty($_POST['caratwtcalcvalue'])) {
			$_POST['caratwtcalcvalue']= '5';
		}else {
			$_POST['caratwtcalcvalue'] = $_POST['caratwtcalcvalue'];
		}
		if(empty($_POST['untagstoneenable'])) {
			$_POST['untagstoneenable']= '';
		}else {
			$_POST['untagstoneenable'] = $_POST['untagstoneenable'];
		}
		if(empty($_POST['purchaseerrorwt'])) {
			$_POST['purchaseerrorwt']= '';
		}else {
			$_POST['purchaseerrorwt'] = $_POST['purchaseerrorwt'];
		}
		if(empty($_POST['placeorderdefaultaccid'])) {
			$_POST['placeorderdefaultaccid']= '';
		}else {
			$_POST['placeorderdefaultaccid'] = $_POST['placeorderdefaultaccid'];
		}
		if(empty($_POST['estimatedefaultaccid'])) {
			$_POST['estimatedefaultaccid']= '';
		}else {
			$_POST['estimatedefaultaccid'] = $_POST['estimatedefaultaccid'];
		}
		if(empty($_POST['chitcompletealert'])) {
			$_POST['chitcompletealert']= '';
		}else {
			$_POST['chitcompletealert'] = $_POST['chitcompletealert'];
		}
		if(empty($_POST['accountmobilevalidation'])) {
			$_POST['accountmobilevalidation']= '';
		}else {
			$_POST['accountmobilevalidation'] = $_POST['accountmobilevalidation'];
		}
		if(empty($_POST['lottolerancepercent'])) {
			$_POST['lottolerancepercent']= '';
		}else {
			$_POST['lottolerancepercent'] = $_POST['lottolerancepercent'];
		}
		if(empty($_POST['lottolerancevalue'])) {
			$_POST['lottolerancevalue']= '';
		}else {
			$_POST['lottolerancevalue'] = $_POST['lottolerancevalue'];
		}
		if(empty($_POST['flatweightwastagespan'])) {
			$_POST['flatweightwastagespan']= '';
		}else {
			$_POST['flatweightwastagespan'] = $_POST['flatweightwastagespan'];
		}
		if(empty($_POST['salespaymenthideshow'])) {
			$_POST['salespaymenthideshow']= '';
		}else {
			$_POST['salespaymenthideshow'] = $_POST['salespaymenthideshow'];
		}
		if(empty($_POST['creditinfohideshow'])) {
			$_POST['creditinfohideshow']= '0';
		}else {
			$_POST['creditinfohideshow'] = $_POST['creditinfohideshow'];
		}
		if(empty($_POST['povendormanagement'])) {
			$_POST['povendormanagement']= '0';
		}else {
			$_POST['povendormanagement'] = $_POST['povendormanagement'];
		}
		if(empty($_POST['takeordercategory'])) {
			$_POST['takeordercategory']= '0';
		}else {
			$_POST['takeordercategory'] = $_POST['takeordercategory'];
		}
		if(empty($_POST['accountypehideshow'])) {
			$_POST['accountypehideshow']= '0';
		}else {
			$_POST['accountypehideshow'] = $_POST['accountypehideshow'];
		}
		if(empty($_POST['takeorderduedate'])) {
			$_POST['takeorderduedate']= '15';
		}else {
			$_POST['takeorderduedate'] = $_POST['takeorderduedate'];
		}
		if(empty($_POST['chitinteresttype'])) {
			$_POST['chitinteresttype']= '0';
		}else {
			$_POST['chitinteresttype'] = $_POST['chitinteresttype'];
		}
		if(empty($_POST['accountmobileunique'])) {
			$_POST['accountmobileunique']= '0';
		}else {
			$_POST['accountmobileunique'] = $_POST['accountmobileunique'];
		}
		if(empty($_POST['ratedisplaymainview'])) {
			$_POST['ratedisplaymainview']= '0';
		}else {
			$_POST['ratedisplaymainview'] = $_POST['ratedisplaymainview'];
		}
		if(empty($_POST['mainviewdefaultview'])) {
			$_POST['mainviewdefaultview']= '30';
		}else {
			$_POST['mainviewdefaultview'] = $_POST['mainviewdefaultview'];
		}
		if(empty($_POST['transactioncategoryfield'])) {
			$_POST['transactioncategoryfield']= '0';
		}else {
			$_POST['transactioncategoryfield'] = $_POST['transactioncategoryfield'];
		}
		if(empty($_POST['transactionemployeeperson'])) {
			$_POST['transactionemployeeperson']= '0';
		}else {
			$_POST['transactionemployeeperson'] = $_POST['transactionemployeeperson'];
		}
		if(empty($_POST['poautomaticvendor'])) {
			$_POST['poautomaticvendor']= '0';
		}else {
			$_POST['poautomaticvendor'] = $_POST['poautomaticvendor'];
		}
		if(empty($_POST['receiveorderconcept'])) {
			$_POST['receiveorderconcept']= '0';
		}else {
			$_POST['receiveorderconcept'] = $_POST['receiveorderconcept'];
		}
		if(empty($_POST['productaddoninfo'])) {
			$_POST['productaddoninfo']= '0';
		}else {
			$_POST['productaddoninfo'] = $_POST['productaddoninfo'];
		}
		if(empty($_POST['receiveordertemplateid'])) {
			$_POST['receiveordertemplateid']= '1';
		}else {
			$_POST['receiveordertemplateid'] = $_POST['receiveordertemplateid'];
		}
		if(empty($_POST['rejectreviewordertemplateid'])) {
			$_POST['rejectreviewordertemplateid']= '1';
		}else {
			$_POST['rejectreviewordertemplateid'] = $_POST['rejectreviewordertemplateid'];
		}
		if(empty($_POST['oldjewelsstonedetails'])) {
			$_POST['oldjewelsstonedetails']= '0';
		}else {
			$_POST['oldjewelsstonedetails'] = $_POST['oldjewelsstonedetails'];
		}
		if(empty($_POST['chargeconversionfield'])) {
			$_POST['chargeconversionfield']= '0';
		}else {
			$_POST['chargeconversionfield'] = $_POST['chargeconversionfield'];
		}
		if(empty($_POST['billamountcalculationtrigger'])) {
			$_POST['billamountcalculationtrigger']= '0';
		}else {
			$_POST['billamountcalculationtrigger'] = $_POST['billamountcalculationtrigger'];
		}
		if(empty($_POST['oldjewelsstonecalc'])) {
			$_POST['oldjewelsstonecalc']= '0';
		}else {
			$_POST['oldjewelsstonecalc'] = $_POST['oldjewelsstonecalc'];
		}
		if(empty($_POST['purchasemodstonedetails'])) {
			$_POST['purchasemodstonedetails']= '0';
		}else {
			$_POST['purchasemodstonedetails'] = $_POST['purchasemodstonedetails'];
		}
		if(empty($_POST['discountauthmoduleview'])) {
			$_POST['discountauthmoduleview']= '0';
		}else {
			$_POST['discountauthmoduleview'] = $_POST['discountauthmoduleview'];
		}
		if(empty($_POST['ratedisplaypurityvalue'])) {
			$_POST['ratedisplaypurityvalue']= '2,3';
		}else {
			$_POST['ratedisplaypurityvalue'] = $_POST['ratedisplaypurityvalue'];
		}
		if(empty($_POST['salesreturnbillcustomers'])) {
			$_POST['salesreturnbillcustomers']= '0';
		} else {
			$_POST['salesreturnbillcustomers'] = $_POST['salesreturnbillcustomers'];
		}
		if(empty($_POST['salesreturnautomatic'])) {
			$_POST['salesreturnautomatic']= '0';
		} else {
			$_POST['salesreturnautomatic'] = $_POST['salesreturnautomatic'];
		}
		if(empty($_POST['itemtagsavefocus'])) {
			$_POST['itemtagsavefocus']= '0';
		} else {
			$_POST['itemtagsavefocus'] = $_POST['itemtagsavefocus'];
		}
		if(empty($_POST['questionaireapplicable'])) {
			$_POST['questionaireapplicable']= '0';
		} else {
			$_POST['questionaireapplicable'] = $_POST['questionaireapplicable'];
		}
		if(empty($_POST['stonepiecescalc'])) {
			$_POST['stonepiecescalc']= '0';
		} else {
			$_POST['stonepiecescalc'] = $_POST['stonepiecescalc'];
		}
		if(empty($_POST['accountfieldcaps'])) {
			$_POST['accountfieldcaps']= '0';
		} else {
			$_POST['accountfieldcaps'] = $_POST['accountfieldcaps'];
		}
		if(empty($_POST['paymentproducts'])) {
			$_POST['paymentproducts']= '0';
		} else {
			$_POST['paymentproducts'] = $_POST['paymentproducts'];
		}
		if(empty($_POST['chitbooktype'])) {
			$_POST['chitbooktype']= '0';
		} else {
			$_POST['chitbooktype'] = $_POST['chitbooktype'];
		}
		if(empty($_POST['itemtagvendorvalidate'])) {
			$_POST['itemtagvendorvalidate']= '0';
		} else {
			$_POST['itemtagvendorvalidate'] = $_POST['itemtagvendorvalidate'];
		}
		if(empty($_POST['accountsummaryinfo'])) {
			$_POST['accountsummaryinfo']= '0';
		} else {
			$_POST['accountsummaryinfo'] = $_POST['accountsummaryinfo'];
		}
		if(empty($_POST['paymentsavebutton'])) {
			$_POST['paymentsavebutton']= '0';
		} else {
			$_POST['paymentsavebutton'] = $_POST['paymentsavebutton'];
		}
		if(empty($_POST['popuphideshow'])) {
			$_POST['popuphideshow']= '0';
		} else {
			$_POST['popuphideshow'] = $_POST['popuphideshow'];
		}
		if(empty($_POST['partialtagdate'])) {
			$_POST['partialtagdate']= '0';
		} else {
			$_POST['partialtagdate'] = $_POST['partialtagdate'];
		}
		if(empty($_POST['salesreturndays'])) {
			$_POST['salesreturndays']= '0';
		}else {
			$_POST['salesreturndays'] = $_POST['salesreturndays'];
		}
		if(empty($_POST['salesreturnuserroleauth'])) {
			$_POST['salesreturnuserroleauth']= '2';
		}else {
			$_POST['salesreturnuserroleauth'] = $_POST['salesreturnuserroleauth'];
		}
		if(empty($_POST['printtemplatehideshow'])) {
			$_POST['printtemplatehideshow']= '0';
		}else {
			$_POST['printtemplatehideshow'] = $_POST['printtemplatehideshow'];
		}
		if(empty($_POST['oldpurchasecredittype'])) {
			$_POST['oldpurchasecredittype']= '0';
		}else {
			$_POST['oldpurchasecredittype'] = $_POST['oldpurchasecredittype'];
		}
		// scheme master enable or disable
		   if($_POST['sizestatus'] == '0'){ 
				$sizestatus=array('status'=>$this->Basefunctions->deletestatus);
		   }else{
			   $sizestatus=array('status'=>$this->Basefunctions->activestatus);
		   }
			$this->db->where_in('moduleid',array('134'));
			$this->db->update('module',$sizestatus);
		// size master enable or disable
		   if($_POST['schemestatus'] == 'NO'){ 
				$schemestatus=array('status'=>$this->Basefunctions->deletestatus);
		   }else{
			   $schemestatus=array('status'=>$this->Basefunctions->activestatus);
		   }
			$this->db->where_in('moduleid',array('49','51'));
			$this->db->update('module',$schemestatus);	
		// PO Vendor Management
			if($_POST['povendormanagement'] == '0') {
				$povendormgmtstatus = array('status'=>$this->Basefunctions->deletestatus);
			} else {
				$povendormgmtstatus = array('status'=>$this->Basefunctions->activestatus);
			}
			$this->db->where_in('toolbarnameid',array('152','153','154','156','158','159'));
			$this->db->update('toolbarname',$povendormgmtstatus);
		{ // round type update
			$roundweight=array('roundtypevalue'=>$_POST['weightdecisize']);
			$this->db->where('roundtypeid',2);
			$this->db->update('roundtype',$roundweight);
			
			$roundamount=array('roundtypevalue'=>$_POST['amountround']);
			$this->db->where('roundtypeid',3);
			$this->db->update('roundtype',$roundamount);
			
			$meltinground=array('roundtypevalue'=>$_POST['meltdecisize']);
			$this->db->where('roundtypeid',4);
			$this->db->update('roundtype',$meltinground);
			
			$rateround=array('roundtypevalue'=>$_POST['rateround']);
			$this->db->where('roundtypeid',5);
			$this->db->update('roundtype',$rateround);
		}
		{  // update other counter
			if($_POST['counteroption'] == 'YES'){
				$tagtypestatus=1;
				$counterstatus=array('status'=>$this->Basefunctions->activestatus);
			}else{
				$tagtypestatus=0;
				$counterstatus=array('status'=>$this->Basefunctions->deletestatus);
			}
			
			$tagtypedata=array('status'=>$tagtypestatus);
			$this->db->where('tagtypeid',14);
			$this->db->update('tagtype',$tagtypedata);
			//Storage hideshow
			$this->db->where_in('moduleid',array('7','59'));
			$this->db->update('module',$counterstatus);
		}
		{ // Discount Authorization Module Enabled / Disabled
			if($_POST['discountauthorization'] == 0) {
				$discatuhupdate = array('status'=>$this->Basefunctions->deletestatus);
			} else if($_POST['discountauthorization'] == 1) {
				$discatuhupdate = array('status'=>$this->Basefunctions->activestatus);
			}
			$this->db->where_in('moduleid',array('110'));
			$this->db->update('module',$discatuhupdate);
		}
		$update_company=array(
				'lot'=>$_POST['lotcreationopt'],
				'lotdetail'=>$_POST['lotdetailsopt'],
				'counter'=>$_POST['counteroption'],
				'stone'=>$_POST['stoneoption'],
				'image'=>$_POST['imageoption'],
				'taxapplicable'=>$_POST['taxoption'],
				'inlinepdfpreview'=>$_POST['pdfoption'],
				'weightscale'=>$_POST['scaleoption'],
				'netwtcalctypeid'=>$_POST['netwtcalctype'],
				'tagtype'=>$_POST['tagtype'],
				'schemestatus'=>$_POST['schemestatus'],
				'sizestatus'=>$_POST['sizestatus'],
				'needezetap'=>$_POST['needezetap'],
				'needcashcheque'=>$_POST['needcashcheque'],
				'askemailsmsfields'=>$_POST['askemailsmsfields'],
				'ezetapnotification'=>$_POST['ezetapnotification'],
				'counter_level'=>$_POST['counterlevel'],
				'category_level'=>$_POST['categorylevel'],
				'stone_level'=>$_POST['stonelevel'],
				'purchasedata'=>$_POST['purchasedata'],
				'purityid'=>$_POST['purity'],
				'chargeid'=>$_POST['chargedata'],
				'purchasechargeid'=>$_POST['purchasechargedata'],
				'businesschargeid'=>$_POST['businesschargedata'],
				'businesspurchasechargeid'=>$_POST['businesspurchasechargedata'],
				'olditemdetails'=>$_POST['olditemdata'],
				'barcodeoption'=>$_POST['barcodeoption'],
				'bullionproduct'=>$_POST['bullionproductdata'],
				'chargecalcoption'=>$_POST['chargecalcoption'],
				'chargecalculation'=>$_POST['chargecalculation'],
				'salesreturntypeid'=>$_POST['salesreturntypeid'],
				'billingtypeid'=>$_POST['billingtypeid'],
				'defaultbankid'=>$_POST['defaultbankid'],
				'defaultcashinhandid'=>$_POST['defaultcashinhandid'],
				'oldjewelproducttypeid'=>$_POST['oldjewelproducttype'],
				'categoryid'=>$_POST['categoryid'],
				'approvaloutcalculation'=>$_POST['approvaloutcalculation'],
				'schememethodid'=>$_POST['schememethodid'],
				'schemetypeid'=>$_POST['schemetypeid'],
				'stocknotify'=>$_POST['stocknotify'],
				'productidshow'=>$_POST['productidshow'],
				'vacchargeshow'=>$_POST['vacchargeshow'],
				'chargeaccountgroup'=>$_POST['chargeaccountgroup'],
				'taxtypeid'=>$_POST['taxtypeid'],
				'discounttypeid'=>$_POST['discounttypeid'],
				'purchasetransactionmodeid'=>$_POST['purchasetransactionmodedata'],
				'oldjewelsumtaxapplyid'=>$_POST['oldjewelsumtaxapplyid'],
				'oldjewelsumtaxid'=>$_POST['oldjewelsumtaxid'],
				'rateadditiondefault'=>$_POST['rateadditiondefault'],
				'estimateconcept'=>$_POST['estimateconcept'],
				'purityrate'=>$_POST['ratepurity'],
				'lotpieces	'=>$_POST['lotpieces'],
				'purchasewastage'=>$_POST['purchasewastage'],
				'purchaserate'=>$_POST['purchaserate'],
				'salesrate'=>$_POST['salesrate'],
				'metalpurity'=>$_POST['metalpurity'],
				'paymentmodetype'=>$_POST['paymentmodetype'],
				'oldjewelvoucher'=>$_POST['oldjewelvoucher'],
				'discountauthorization'=>$_POST['discountauthorization'],
				'taxchargeinclude'=>$_POST['taxchargeinclude'],
				'discountpassword'=>$_POST['discountpassword'],
				'discountcalc'=>$_POST['discountcalc'],
				'taxcalc'=>$_POST['taxcalc'],
				'storagequantity'=>$_POST['storagequantity'],
				'purewtcalctype'=>$_POST['purewtcalctype'],
				'oldjeweldetails'=>$_POST['oldjeweldetails'],
				'lotdata'=>$_POST['lotdata'],
				'storagedisplay'=>$_POST['storagedisplay'],
				'wastagecalctype'=>$_POST['wastagecalctype'],
				'gstapplicable'=>$_POST['gstapplicable'],
				'grossweighttrigger'=>$_POST['grossweighttrigger'],
				'productroltypeid'=>$_POST['productroltypeid'],
				'reorderqty'=>$_POST['reorderqty'],
				'salesdatechangeablefor'=>$_POST['salesdatechangeablefor'],
				'cashcountershowhide'=>$_POST['cashcountershowhide'],
				'purchasedisplay'=>$_POST['purchasedisplay'],
				'edittagcharge'=>$_POST['edittagcharge'],
				'lockingtime'=>$_POST['lockingtime'],
				'updatingtime'=>$_POST['updatingtime'],
				'recurrencedayid'=>$_POST['recurrenceday'],
				'lotweightlimit'=>$_POST['lotweightlimit'],
				'chargerequired'=>$_POST['chargerequired'],
				'oldjewelvoucheredit'=>$_POST['oldjewelvoucheredit'],
				'roundvaluelimit'=>$_POST['roundvaluelimit'],
				'advancecalc'=>$_POST['advancecalc'],
				'customerwise'=>$_POST['customerwise'],
				'vendorwise'=>$_POST['vendorwise'],
				'weightvalidate'=>$_POST['weightvalidate'],
				'prizetaggst'=>$_POST['prizetaggst'],
				'boxwttemplate'=>$_POST['boxwttemplate'],
				'deviceno'=>$_POST['deviceno'],
				'orderimageoption'=>$_POST['orderimageoption'],
				'transactionimageoption'=>$_POST['transactionimageoption'],
				'weightcheck'=>$_POST['weightcheck'],
				'loosestonecharge'=>$_POST['loosestonecharge'],
				'loosesalescharge'=>$_POST['loosesaleschargedata'],
				'loosepurchasecharge'=>$_POST['loosepurchasechargedata'],
				'stoneweightcheck'=>$_POST['stoneweightcheck'],
				'tagautoimage'=>$_POST['tagautoimage'],
				'locationdrive'=>$_POST['drivename'],
				'itemtagauthrole'=>$_POST['itemtagauthuserroleid'],
				'tagchargesfrom'=>$_POST['tagchargesfrom'],
				'rfidnoofdigits'=>$_POST['rfidnoofdigits'],
				'pancardrestrictamt'=>$_POST['pancardrestrictamt'],
				'cashreceiptlimit'=>$_POST['cashreceiptlimit'],
				'cashissuelimit'=>$_POST['cashissuelimit'],
				'touchchargesedit'=>$_POST['touchchargesedit'],
				'discountbilllevelpercent'=>$_POST['discountbilllevelpercent'],
				'untagwtvalidate'=>$_POST['untagwtvalidate'],
				'untagwtshow'=>$_POST['untagwtshow'],
				'approvaloutrfid'=>$_POST['approvaloutrfid'],
				'salesauthuserrole'=>$_POST['salesauthuserrole'],
				'multitagbarcode'=>$_POST['multitagbarcode'],
				'estimatequicktotal'=>$_POST['estimatequicktotal'],
				'estimatedefaultset'=>$_POST['estimatedefaultset'],
				'caratwtcalcvalue'=>$_POST['caratwtcalcvalue'],
				'untagstoneenable'=>$_POST['untagstoneenable'],
				'purchaseerrorwt'=>$_POST['purchaseerrorwt'],
				'placeorderdefaultaccid'=>$_POST['placeorderdefaultaccid'],
				'estimatedefaultaccid'=>$_POST['estimatedefaultaccid'],
				'chitcompletealert'=>$_POST['chitcompletealert'],
				'chituserroleauth'=>$_POST['chituserroleauth'],
				'accountmobilevalidation'=>$_POST['accountmobilevalidation'],
				'lottolerancepercent'=>$_POST['lottolerancepercent'],
				'lottolerancevalue'=>$_POST['lottolerancevalue'],
				'flatweightwastagespan'=>$_POST['flatweightwastagespan'],
				'salespaymenthideshow'=>$_POST['salespaymenthideshow'],
				'creditinfohideshow'=>$_POST['creditinfohideshow'],
				'povendormanagement'=>$_POST['povendormanagement'],
				'takeordercategory'=>$_POST['takeordercategory'],
				'accountypehideshow'=>$_POST['accountypehideshow'],
				'takeorderduedate'=>$_POST['takeorderduedate'],
				'chitinteresttype'=>$_POST['chitinteresttype'],
				'accountmobileunique'=>$_POST['accountmobileunique'],
				'ratedisplaymainview'=>$_POST['ratedisplaymainview'],
				'mainviewdefaultview'=>$_POST['mainviewdefaultview'],
				'transactioncategoryfield'=>$_POST['transactioncategoryfield'],
				'transactionemployeeperson'=>$_POST['transactionemployeeperson'],
				'poautomaticvendor'=>$_POST['poautomaticvendor'],
				'receiveorderconcept'=>$_POST['receiveorderconcept'],
				'productaddoninfo'=>$_POST['productaddoninfo'],
				'receiveordertemplateid'=>$_POST['receiveordertemplateid'],
				'rejectreviewordertemplateid'=>$_POST['rejectreviewordertemplateid'],
				'oldjewelsstonedetails'=>$_POST['oldjewelsstonedetails'],
				'chargeconversionfield'=>$_POST['chargeconversionfield'],
				'billamountcalculationtrigger'=>$_POST['billamountcalculationtrigger'],
				'oldjewelsstonecalc'=>$_POST['oldjewelsstonecalc'],
				'purchasemodstonedetails'=>$_POST['purchasemodstonedetails'],
				'discountauthmoduleview'=>$_POST['discountauthmoduleview'],
				'ratedisplaypurityvalue'=>$_POST['ratedisplaypurityvalue'],
				'salesreturnbillcustomers'=>$_POST['salesreturnbillcustomers'],
				'salesreturnautomatic'=>$_POST['salesreturnautomatic'],
				'itemtagsavefocus'=>$_POST['itemtagsavefocus'],
				'questionaireapplicable'=>$_POST['questionaireapplicable'],
				'stonepiecescalc'=>$_POST['stonepiecescalc'],
				'accountfieldcaps'=>$_POST['accountfieldcaps'],
				'paymentproducts'=>$_POST['paymentproducts'],
				'chitbooktype'=>$_POST['chitbooktype'],
				'itemtagvendorvalidate'=>$_POST['itemtagvendorvalidate'],
				'accountsummaryinfo'=>$_POST['accountsummaryinfo'],
				'paymentsavebutton'=>$_POST['paymentsavebutton'],
				'popuphideshow'=>$_POST['popuphideshow'],
				'partialtagdate'=>$_POST['partialtagdate'],
				'salesreturndays'=>$_POST['salesreturndays'],
				'salesreturnuserroleauth'=>$_POST['salesreturnuserroleauth'],
				'printtemplatehideshow'=>$_POST['printtemplatehideshow'],
				'lottolerancediactvalue'=>$_POST['lottolerancediactvalue'],
				'lotdiaweightlimit'=>$_POST['lotdiaweightlimit'],
				'oldpurchasecredittype'=>$_POST['oldpurchasecredittype']
			);
		$update_company = array_merge($update_company,$this->Crudmodel->updatedefaultvalueget());
		$this->db->where('companysettingid',$id);
		$this->db->update('companysetting',$update_company);
		echo 'SUCCESS';
	}
	public function getcompanydata() {
		$this->db->from('companysetting');
		$this->db->where('companysettingid',1);
		$info=$this->db->get();
		$data=$info->row();
		$weightround=$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2);
		$amountround=$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3);
		$meltinground=$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',4);
		$rateround=$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5);
		$dia_weight_round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',8);
		if($info->num_rows() > 0){
			$company_setting=array(
					'companysettingid'=>$data->companysettingid,
					'weightdecisize'=>$weightround,
					'meltdecisize'=>$meltinground,
					'amountround'=>$amountround,
					'rateround'=>$rateround,
					'lotcreationopt'=>$data->lot,
					'lotdetailsopt'=>$data->lotdetail,
					'counteroption'=>$data->counter,
					'stoneoption'=>$data->stone,
					'imageoption'=>$data->image,
					'taxoption'=>$data->taxapplicable,
					'pdfoption'=>$data->inlinepdfpreview,
					'scaleoption'=>$data->weightscale,
					'netwtcalctype'=>$data->netwtcalctypeid,
					'tagtype'=>$data->tagtype,
					'schemestatus'=>$data->schemestatus,
					'sizestatus'=>$data->sizestatus,
					'needezetap'=>$data->needezetap,
					'askemailsmsfields'=>$data->askemailsmsfields,
					'needcashcheque'=>$data->needcashcheque,
					'ezetapnotification'=>$data->ezetapnotification,
					'counterlevel'=>$data->counter_level,
					'categorylevel'=>$data->category_level,
					'stonelevel'=>$data->stone_level,
					'purchasedata'=>$data->purchasedata,
					'purityid'=>$data->purityid,
					'chargeid'=>$data->chargeid,
					'purchasechargeid'=>$data->purchasechargeid,
					'businesschargeid'=>$data->businesschargeid,
					'businesspurchasechargeid'=>$data->businesspurchasechargeid,
					'olditemdetails'=>$data->olditemdetails,
					'barcodeoption'=>$data->barcodeoption,
					'bullionproduct'=>$data->bullionproduct,
					'chargecalcoption'=>$data->chargecalcoption,
					'chargecalculation'=>$data->chargecalculation,
					'salesreturntypeid'=>$data->salesreturntypeid,
					'billingtypeid'=>$data->billingtypeid,
					'defaultbankid'=>$data->defaultbankid,
					'defaultcashinhandid'=>$data->defaultcashinhandid,
					'oldjewelproducttype'=>$data->oldjewelproducttypeid,
					'categoryid'=>$data->categoryid,
					'approvaloutcalculation'=>$data->approvaloutcalculation,
					'schememethodid'=>$data->schememethodid,
					'schemetypeid'=>$data->schemetypeid,
					'stocknotify'=>$data->stocknotify,
					'productidshow'=>$data->productidshow,
					'vacchargeshow'=>$data->vacchargeshow,
					'chargeaccountgroup'=>$data->chargeaccountgroup,
					'taxtypeid'=>$data->taxtypeid,
					'discounttypeid'=>$data->discounttypeid,
					'purchasetransactionmodeid'=>$data->purchasetransactionmodeid,
					'oldjewelsumtaxapplyid'=>$data->oldjewelsumtaxapplyid,
					'oldjewelsumtaxid'=>$data->oldjewelsumtaxid,
					'rateadditiondefault'=>$data->rateadditiondefault,
					'estimateconcept'=>$data->estimateconcept,
					'ratepurityid'=>$data->purityrate,
					'lotpieces'=>$data->lotpieces,
					'purchasewastage'=>$data->purchasewastage,
					'salesrate'=>$data->salesrate,
					'purchaserate'=>$data->purchaserate,
					'metalpurityid'=>$data->metalpurity,
					'paymentmodetype'=>$data->paymentmodetype,
					'oldjewelvoucher'=>$data->oldjewelvoucher,
					'discountauthorization'=>$data->discountauthorization,
					'taxchargeinclude'=>$data->taxchargeinclude,
					'discountpassword'=>$data->discountpassword,
					'discountcalc'=>$data->discountcalc,
					'taxcalc'=>$data->taxcalc,
					'storagequantity'=>$data->storagequantity,
					'purewtcalctype'=>$data->purewtcalctype,
					'oldjeweldetails'=>$data->oldjeweldetails,
					'lotdata'=>$data->lotdata,
					'storagedisplay'=>$data->storagedisplay,
					'wastagecalctype'=>$data->wastagecalctype,
					'gstapplicable'=>$data->gstapplicable,
					'grossweighttrigger'=>$data->grossweighttrigger,
					'productroltypeid'=>$data->productroltypeid,
					'reorderqty'=>$data->reorderqty,
					'salesdatechangeablefor'=>$data->salesdatechangeablefor,
					'cashcountershowhide'=>$data->cashcountershowhide,
					'purchasedisplay'=>$data->purchasedisplay,
					'edittagcharge'=>$data->edittagcharge,
					'lockingtime'=>$data->lockingtime,
					'updatingtime'=>$data->updatingtime,
					'recurrencedayid'=>$data->recurrencedayid,
					'lotweightlimit'=>number_format((float)$data->lotweightlimit, $weightround, '.', ''),
					'chargerequired'=>$data->chargerequired,
					'oldjewelvoucheredit'=>$data->oldjewelvoucheredit,
					'roundvaluelimit'=>$data->roundvaluelimit,
					'advancecalc'=>$data->advancecalc,
					'customerwise'=>$data->customerwise,
					'vendorwise'=>$data->vendorwise,
					'weightvalidate'=>$data->weightvalidate,
					'prizetaggst'=>$data->prizetaggst,
					'boxwttemplate'=>$data->boxwttemplate,
					'deviceno'=>$data->deviceno,
					'orderimageoption'=>$data->orderimageoption,
					'transactionimageoption'=>$data->transactionimageoption,
					'weightcheck'=>$data->weightcheck,
					'loosestonecharge'=>$data->loosestonecharge,
					'loosesalescharge'=>$data->loosesalescharge,
					'loosepurchasecharge'=>$data->loosepurchasecharge,
					'stoneweightcheck'=>$data->stoneweightcheck,
					'tagautoimage'=>$data->tagautoimage,
					'drivename'=>$data->locationdrive,
					'itemtagauthrole'=>$data->itemtagauthrole,
					'tagchargesfrom'=>$data->tagchargesfrom,
					'rfidnoofdigits'=>$data->rfidnoofdigits,
					'pancardrestrictamt'=>$data->pancardrestrictamt,
					'cashreceiptlimit'=>$data->cashreceiptlimit,
					'cashissuelimit'=>$data->cashissuelimit,
					'touchchargesedit'=>$data->touchchargesedit,
					'discountbilllevelpercent'=>$data->discountbilllevelpercent,
					'untagwtvalidate'=>$data->untagwtvalidate,
					'untagwtshow'=>$data->untagwtshow,
					'approvaloutrfid'=>$data->approvaloutrfid,
					'salesauthuserrole'=>$data->salesauthuserrole,
					'multitagbarcode'=>$data->multitagbarcode,
					'estimatequicktotal'=>$data->estimatequicktotal,
					'estimatedefaultset'=>$data->estimatedefaultset,
					'caratwtcalcvalue'=>$data->caratwtcalcvalue,
					'untagstoneenable'=>$data->untagstoneenable,
					'purchaseerrorwt'=>number_format((float)$data->purchaseerrorwt, $weightround, '.', ''),
					'placeorderdefaultaccid'=>$data->placeorderdefaultaccid,
					'estimatedefaultaccid'=>$data->estimatedefaultaccid,
					'chitcompletealert'=>$data->chitcompletealert,
					'chituserroleauth'=>$data->chituserroleauth,
					'accountmobilevalidation'=>$data->accountmobilevalidation,
					'lottolerancepercent'=>$data->lottolerancepercent,
					'lottolerancevalue'=>number_format((float)$data->lottolerancevalue, 2, '.', ''),
					'flatweightwastagespan'=>$data->flatweightwastagespan,
					'salespaymenthideshow'=>$data->salespaymenthideshow,
					'creditinfohideshow'=>$data->creditinfohideshow,
					'povendormanagement'=>$data->povendormanagement,
					'takeordercategory'=>$data->takeordercategory,
					'accountypehideshow'=>$data->accountypehideshow,
					'takeorderduedate'=>$data->takeorderduedate,
					'chitinteresttype'=>$data->chitinteresttype,
					'accountmobileunique'=>$data->accountmobileunique,
					'ratedisplaymainview'=>$data->ratedisplaymainview,
					'mainviewdefaultview'=>$data->mainviewdefaultview,
					'transactioncategoryfield'=>$data->transactioncategoryfield,
					'transactionemployeeperson'=>$data->transactionemployeeperson,
					'poautomaticvendor'=>$data->poautomaticvendor,
					'receiveorderconcept'=>$data->receiveorderconcept,
					'productaddoninfo'=>$data->productaddoninfo,
					'receiveordertemplateid'=>$data->receiveordertemplateid,
					'rejectreviewordertemplateid'=>$data->rejectreviewordertemplateid,
					'oldjewelsstonedetails'=>$data->oldjewelsstonedetails,
					'chargeconversionfield'=>$data->chargeconversionfield,
					'billamountcalculationtrigger'=>$data->billamountcalculationtrigger,
					'oldjewelsstonecalc'=>$data->oldjewelsstonecalc,
					'purchasemodstonedetails'=>$data->purchasemodstonedetails,
					'discountauthmoduleview'=>$data->discountauthmoduleview,
					'ratedisplaypurityvalue'=>$data->ratedisplaypurityvalue,
					'salesreturnbillcustomers'=>$data->salesreturnbillcustomers,
					'salesreturnautomatic'=>$data->salesreturnautomatic,
					'itemtagsavefocus'=>$data->itemtagsavefocus,
					'questionaireapplicable'=>$data->questionaireapplicable,
					'stonepiecescalc'=>$data->stonepiecescalc,
					'accountfieldcaps'=>$data->accountfieldcaps,
					'paymentproducts'=>$data->paymentproducts,
					'chitbooktype'=>$data->chitbooktype,
					'itemtagvendorvalidate'=>$data->itemtagvendorvalidate,
					'accountsummaryinfo'=>$data->accountsummaryinfo,
					'paymentsavebutton'=>$data->paymentsavebutton,
					'popuphideshow'=>$data->popuphideshow,
					'partialtagdate'=>$data->partialtagdate,
					'salesreturndays'=>$data->salesreturndays,
					'salesreturnuserroleauth'=>$data->salesreturnuserroleauth,
					'printtemplatehideshow'=>$data->printtemplatehideshow,
					'lottolerancediactvalue'=>number_format((float)$data->lottolerancediactvalue, 2, '.', ''),
					'lotdiaweightlimit'=>number_format((float)$data->lotdiaweightlimit, $dia_weight_round, '.', ''),
					'oldpurchasecredittype'=>$data->oldpurchasecredittype
				);
		} else {
			$company_setting='';
		}
		echo json_encode($company_setting);
	}
	
	/**	*Company Setting => Transaction creation	*/
	public function transactionsetting() {
		if(empty($_POST['totalamountdata'])) {
			$_POST['totalamountdata']='';
		} else {
			$_POST['totalamountdata']=$_POST['totalamountdata'];
		}
		if(empty($_POST['pureweightdata'])) {
			$_POST['pureweightdata']='';
		} else {
			$_POST['pureweightdata']=$_POST['pureweightdata'];
		}
		if(empty($_POST['discountmodeid'])) {
			$_POST['discountmodeid']='1';
		} else {
			$_POST['discountmodeid']=$_POST['discountmodeid'];
		}
		if(empty($_POST['discountcalctypeid'])) {
			$_POST['discountcalctypeid']='1';
		} else {
			$_POST['discountcalctypeid']=$_POST['discountcalctypeid'];
		}
		if(empty($_POST['discountdisplayid'])) {
			$_POST['discountdisplayid']='1';
		} else {
			$_POST['discountdisplayid']=$_POST['discountdisplayid'];
		}
		//if($_POST['transactiontypeeditstatus'] == 1 && $_POST['transactionmanageid'] > 1)
		if($_POST['transactiontypeeditstatus'] == 1)
		{
		/* $this->db->select('COUNT(transactionmanageid) as recordavailability,',false);
		$this->db->from('transactionmanage');
		$this->db->where('transactionmanage.salestransactiontypeid',$_POST['transactiontype']);
		$this->db->where('transactionmanage.salesmodeid',$_POST['salesmodeid']);
		$this->db->where('transactionmanage.status',$this->Basefunctions->activestatus);
		$info=$this->db->get()->row();
		if($info->recordavailability!=0) { */
			$update=array(
				'stocktypeid'=>$_POST['stocktypeid'],
				'transactionmodeid'=>$_POST['transactionmodeid'],
				'additionalchargeid'=>ltrim($_POST['totalamountdata'],','),
				'pureadditionalchargeid'=>ltrim($_POST['pureweightdata'],','),
				'salesmodeid'=>$_POST['salesmodeid'],
				'salesfieldsid'=>ltrim($_POST['displayfields'],','),
				'roundingtype'=>$_POST['roundingtype'],
				'roundoffremove'=>$_POST['roundoffremove'],
				'autotax'=>$_POST['transactionautotax'],
				'taxtypeid'=>$_POST['taxmodeid'],
				'discountmodeid'=>$_POST['discountmodeid'],
				'discountcalctypeid'=>$_POST['discountcalctypeid'],
				'discountdisplayid'=>$_POST['discountdisplayid'],
				'allstocktypeid'=>$_POST['allstocktypeid'],
				'allpaymentstocktypeid'=>$_POST['allpaymentstocktypeid'],
				'serialnumbermastername'=>$_POST['serialmastername'],
				'allprinttemplateid'=>$_POST['allprinttemplateid'],
				'payrecadvanceconcept'=>$_POST['payrecadvanceconcept'],
				'payrecadvanceprintid'=>$_POST['payrecadvanceprintid'],
				'defaultforttype'=>$_POST['defaultforttype'],
				'chargesedit'=>$_POST['chargesedit'],
				'allowedaccounttypeid'=>$_POST['allowedaccounttypeid'],
				'transactionmanagename'=>$_POST['transactionmanagename'],
				'industryid'=>$this->Basefunctions->industryid 
			);
			$update=array_merge($update,$this->Crudmodel->updatedefaultvalueget());
			//update transaction manage data
			$this->db->where('transactionmanageid',$_POST['transactionmanageid']);
			$this->db->update('transactionmanage',$update);
			$primaryid = $_POST['transactionmanageid'];
		} else { 
			$insert_transactiondetail=array(
				'salestransactiontypeid'=>$_POST['transactiontype'],
				'stocktypeid'=>$_POST['stocktypeid'],
				'transactionmodeid'=>$_POST['transactionmodeid'],
				'additionalchargeid'=>ltrim($_POST['totalamountdata'],','),
				'pureadditionalchargeid'=>ltrim($_POST['pureweightdata'],','),
				'salesmodeid'=>$_POST['salesmodeid'],
				'salesfieldsid'=>ltrim($_POST['displayfields'],','),
				'roundingtype'=>$_POST['roundingtype'],
				'roundoffremove'=>$_POST['roundoffremove'],
				'autotax'=>$_POST['transactionautotax'],
				'taxtypeid'=>$_POST['taxmodeid'],
				'discountmodeid'=>$_POST['discountmodeid'],
				'discountcalctypeid'=>$_POST['discountcalctypeid'],
				'allstocktypeid'=>$_POST['allstocktypeid'],
				'allpaymentstocktypeid'=>$_POST['allpaymentstocktypeid'],
				'serialnumbermastername'=>$_POST['serialmastername'],
				'allprinttemplateid'=>$_POST['allprinttemplateid'],
				'payrecadvanceconcept'=>$_POST['payrecadvanceconcept'],
				'payrecadvanceprintid'=>$_POST['payrecadvanceprintid'],
				'defaultforttype'=>$_POST['defaultforttype'],
				'chargesedit'=>$_POST['chargesedit'],
				'allowedaccounttypeid'=>$_POST['allowedaccounttypeid'],
				'transactionmanagename'=>$_POST['transactionmanagename'],
				'industryid'=>$this->Basefunctions->industryid
			);
			$insert_transactiondetail = array_merge($insert_transactiondetail,$this->Crudmodel->defaultvalueget());
		    $this->db->insert('transactionmanage',array_filter($insert_transactiondetail));
		    $primaryid = $this->db->insert_id();
		}	
		if($_POST['defaultforttype'] == 1){
			$update=array(
				'defaultforttype'=>0
			);
			$update=array_merge($update,$this->Crudmodel->updatedefaultvalueget());
			//update transaction manage data
			$this->db->where('transactionmanageid !=',$primaryid);
			$this->db->where('salestransactiontypeid',$_POST['transactiontype']);
			$this->db->update('transactionmanage',$update);
		}
		echo 'SUCCESS';
	}
	//old jewel settings create and update
	public function oldjewelsettingcreatemodels() {
		$transactiontype = $_POST['jeweltransactiontype'];
		$olditemdetails = $_POST['jeweltypeid'];
		$oldjewelpurityid = $_POST['oldjewelpurityid'];
		$netweightcalculationid = $_POST['netweightcalculationid'];
		$rateamount = $_POST['rateamount'];
		$weightless = $_POST['weightless'];
		$check = $this->oldjeweldetailsexistcheck($transactiontype,$olditemdetails,$oldjewelpurityid);
		$olditemdetailsarrray = array(
			'salestransactiontypeid'=>$transactiontype,
			'olditemdetailsid'=>$olditemdetails,
			'purityid'=>$oldjewelpurityid,
			'netweightcalculationid'=>$netweightcalculationid,
			'rateless'=>$rateamount,
			'weightless'=>$weightless,
			'industryid'=>$this->Basefunctions->industryid,
			'branchid'=>$this->Basefunctions->branchid
		);
		if($check == 'FALSE') {
			$insertarray = array_merge($olditemdetailsarrray,$this->Crudmodel->defaultvalueget());
			$this->db->insert('oldjeweldetails',array_filter($insertarray));
		} else {
			$update=array_merge($olditemdetailsarrray,$this->Crudmodel->updatedefaultvalueget());
			//update transaction manage data
			$this->db->where('oldjeweldetailsid',$check);
			$this->db->where('status',$this->Basefunctions->activestatus);
			$this->db->update('oldjeweldetails',$update);
		}
		echo 'SUCCESS';
	}
	public function oldjeweldetailsexistcheck($transactiontype,$olditemdetails,$oldjewelpurityid) {
		$data = 'FALSE';
		$this->db->select('oldjeweldetailsid');
		$this->db->from('oldjeweldetails');
		$this->db->where('oldjeweldetails.salestransactiontypeid',$transactiontype);
		$this->db->where('oldjeweldetails.olditemdetailsid',$olditemdetails);
		$this->db->where('oldjeweldetails.purityid',$oldjewelpurityid);
		$this->db->where('oldjeweldetails.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data = $row->oldjeweldetailsid;
			}
		} else{
			$data = 'FALSE';
		}
		return $data;
	}
	//old jewel data retvie
	public function oldjewelsettingretrivemodel(){
		$primaryid = $_GET['primaryid'];
		$id=$_GET['primaryid'];
		$this->db->select('oldjeweldetails.salestransactiontypeid,oldjeweldetails.olditemdetailsid,oldjeweldetails.purityid,oldjeweldetails.netweightcalculationid,oldjeweldetails.rateless,oldjeweldetails.weightless,oldjeweldetailsid',false);
		$this->db->from('oldjeweldetails');
		$this->db->where('oldjeweldetails.oldjeweldetailsid',$primaryid);
		$this->db->where('oldjeweldetails.status',$this->Basefunctions->activestatus);
		$info=$this->db->get()->row();
		$infotag = array(
				'jeweltransactiontype'=>$info->salestransactiontypeid,
				'jeweltypeid'=>$info->olditemdetailsid,
				'oldjewelpurityid'=>$info->purityid,
				'netweightcalculationid'=>$info->netweightcalculationid,
				'rateamount'=>$info->rateless,
				'weightless'=>$info->weightless,
				'transactiontypeid'=>$info->oldjeweldetailsid
		);
		echo json_encode($infotag);
	}
	//old jeqwel delete
	public function oldjeweldetailsdeletemodels() {
		$id = $_GET['primaryid'];
		$delete = array('status'=>0);
		$deletearray =array_merge($delete,$this->Crudmodel->updatedefaultvalueget());
		$this->db->where('oldjeweldetailsid',$id);
		$this->db->update('oldjeweldetails',$deletearray);
		echo 'SUCCESS';
	}
	/**
	*inactivate-Transaction Setting 
	*/
	public function transactionsettingdelete()
	{
		$id=$_GET['primaryid'];
		$inactive = $this->Basefunctions->delete_log();
		$this->db->where('transactionmanageid',$id);
		$this->db->update('transactionmanage',$inactive);
		echo 'SUCCESS';
	}
	
	public function transactionsettingretrive() {
		$id=$_GET['primaryid'];
		$this->db->select('transactionmanage.transactionmanageid,transactionmanage.salestransactiontypeid,transactionmanage.stocktypeid,transactionmanage.transactionmodeid,transactionmanage.additionalchargeid,transactionmanage.pureadditionalchargeid,transactionmanage.salesmodeid,transactionmanage.salesfieldsid,transactionmanage.roundingtype,transactionmanage.roundoffremove,transactionmanage.autotax,discountmodeid,discountcalctypeid,discountdisplayid,taxtypeid,transactionmanage.allstocktypeid,transactionmanage.allpaymentstocktypeid,transactionmanage.serialnumbermastername,transactionmanage.allprinttemplateid,transactionmanage.defaultforttype,transactionmanage.chargesedit,transactionmanage.allowedaccounttypeid,transactionmanage.transactionmanagename,transactionmanage.payrecadvanceconcept,transactionmanage.payrecadvanceprintid',false);
		$this->db->from('transactionmanage');
		$this->db->where('transactionmanage.transactionmanageid',$id);
		$this->db->where('transactionmanage.status',$this->Basefunctions->activestatus);
		$info=$this->db->get()->row();
		$infotag = array(
			'transactiontype'=>$info->salestransactiontypeid,
			'stocktypeid'=>$info->stocktypeid,
			'transactionmodeid'=>$info->transactionmodeid,
			'additionalchargeid'=>$info->additionalchargeid,
			'pureadditionalchargeid'=>$info->pureadditionalchargeid,
			'salesmodeid'=>$info->salesmodeid,
			'displayfieldid'=>$info->salesfieldsid,
			'roundingtype'=>$info->roundingtype,
			'roundoffremove'=>$info->roundoffremove,
			'transactionautotax'=>$info->autotax,
			'discountmodeid'=>$info->discountmodeid,
			'taxmodeid'=>$info->taxtypeid,
			'discountcalctypeid'=>$info->discountcalctypeid,
			'discountdisplayid'=>$info->discountdisplayid,
			'allstocktypeid'=>$info->allstocktypeid,
			'allpaymentstocktypeid'=>$info->allpaymentstocktypeid,
			'serialnumbermastername'=>$info->serialnumbermastername,
			'defaultforttype'=>$info->defaultforttype,
			'chargesedit'=>$info->chargesedit,
			'allowedaccounttypeid'=>$info->allowedaccounttypeid,
			'allprinttemplateid'=>$info->allprinttemplateid,
			'transactionmanagename'=>$info->transactionmanagename,
			'transactionmanageid'=>$info->transactionmanageid,
			'payrecadvanceconcept'=>$info->payrecadvanceconcept,
			'payrecadvanceprintid'=>$info->payrecadvanceprintid
		);
		echo json_encode($infotag);
	}
	public function autoloadfieldvalues()
	{
		$transactiontypeid=$_GET['transactiontypeid'];
		$modeid=$_GET['modeid'];
		$this->db->select('transactionmanage.stocktypeid,transactionmanage.transactionmodeid,transactionmanage.additionalchargeid,transactionmanage.pureadditionalchargeid',false);
		$this->db->from('transactionmanage');
		$this->db->where('transactionmanage.salestransactiontypeid',$transactiontypeid);
		$this->db->where('transactionmanage.salesmodeid',$modeid);
		$this->db->where('transactionmanage.status',$this->Basefunctions->activestatus);
		$info=$this->db->get()->row();
		if(COUNT($info)!=0)
		{
		$infotag = array(
					'stocktypeid'=>$info->stocktypeid,
					'transactionmodeid'=>$info->transactionmodeid,
					'additionalchargeid'=>$info->additionalchargeid,
					'pureadditionalchargeid'=>$info->pureadditionalchargeid
			);
		}
		else
		{
			$infotag = array(
					'stocktypeid'=>'',
					'transactionmodeid'=>'',
					'additionalchargeid'=>'',
					'pureadditionalchargeid'=>''
			);
		}
		echo json_encode($infotag);
	}
	public function getproductchargerow(){
		$datarow = $this->db->where('status',$this->Basefunctions->activestatus)->get('productcharge')->num_rows();
		echo json_encode($datarow);
	}
	/*	charge_groupdropdown */
	public function charge_groupdropdown() {
		$data = $this->db->query(
						"SELECT charge.chargeid,charge.chargename,chargecategory.chargecategoryid,chargecategory.chargecategoryname
						 FROM charge
						 JOIN chargecategory ON chargecategory.chargecategoryid = charge.chargecategoryid
						 WHERE charge.status = 1 AND charge.chargecategoryid != 8
						 ORDER BY charge.chargecategoryid asc" 
						);
		return $data;
	}
	/*	purchasecharge_groupdropdown */
	public function purchasecharge_groupdropdown() {
		$data = $this->db->query(
				"SELECT purchasecharge.purchasechargeid,purchasecharge.purchasechargename,chargecategory.chargecategoryid,chargecategory.chargecategoryname
						 FROM purchasecharge
						 JOIN chargecategory ON chargecategory.chargecategoryid = purchasecharge.chargecategoryid
						 WHERE purchasecharge.status = 1
						 ORDER BY purchasecharge.chargecategoryid asc"
				);
		return $data;
	}
	// Check for updates
	public function checkforupdates() {
		$this->load->database();
		$status = 'Failed';
		$companyid = $_GET['datarowid'];
		$this->db->select('productversion.versionno,productversion.versiondate',false);
		$this->db->from('productversion');
		$this->db->where('productversion.companyid',$companyid);
		$this->db->where('productversion.status',$this->Basefunctions->activestatus);
		$this->db->order_by('productversion.productversionid',"desc");
		$this->db->limit(1);
		$info=$this->db->get()->row();
		if($info != '') {
			$versionnumber = $info->versionno;
			$versiondate = $info->versiondate;
			$planid = $this->Basefunctions->singlefieldfetch('planid','companyid','company',$companyid);
			
			// connect server - release details database
			$serverdb = $this->load->database('serverdb', TRUE); // all_releasedetails
			$serverdb->select('versiondate,versionno');
			$serverdb->where('companyid=',$companyid);
			$serverdb->where('versionno=',$versionnumber);
			$serverrelease = $serverdb->get('latestversion');
			$serverreleasedetails = $serverrelease->last_row();
			
			if($serverreleasedetails->versionno == $versionnumber) {
				
				$serverdb->select('versionno,releasenotes,versiondate');
				$serverdb->where("((companyid = $companyid OR companyid = '0') AND versionno !=  $versionnumber)", NULL, FALSE);
				$serverdb->where('planid=',$planid);
				$serverdb->where('versiondate>=',$serverreleasedetails->versiondate);
				$serverdb->order_by("versiondate", "desc");
				$querynew = $serverdb->get('latestversion');
				
				if(!empty($querynew->result_array())) {
					$data['serverversionList'] = $querynew->result_array();
					$data['companyid'] = $companyid;
					$status = $this->load->view('Company/releasedetails', $data);
				} else {
					$data['serverversionList'] = "No Release";
					$status = $data['serverversionList'];
				}
			} else {
				$status = 'Failed';
			}
		} else {
			$status = 'Failed';
		}
		echo $status;
	}
	// Check for AMC Details
	public function checkamcdetails() {
		$companyid = $_POST['compid'];
		// Need to update it later (create random number for AMC details)
		$randomnogenerate = $this->Basefunctions->varrandomnumbergenerator(103,'stocktakingnumber',1,'');
		$uniquecomp_emailid = $this->Basefunctions->singlefieldfetch('emailid','companyid','company',$companyid);
		
		// Retrieve details from all_releasedetails
		$serverdb = $this->load->database('serverdb', TRUE); // all_releasedetails
		$serverdb->select('amcstatus,periodofinstallation',false);
		$serverdb->from('company');
		//$serverdb->db->where('companyid',$companyid);
		$serverdb->where('emailid',trim($uniquecomp_emailid));
		$serverdb->where('status',$this->Basefunctions->activestatus);
		$serverdb->limit(1);
		$cinfo = $serverdb->get()->row();
		if($cinfo != '') {
			$amcstatus = $cinfo->amcstatus;
			$periodofinstallation = $cinfo->periodofinstallation;
			
			//If AMC Period of installation falls under 1 year
			$todaydate=date('Y-m-d');
			$new = new DateTime($todaydate);
			$old = new DateTime($periodofinstallation);
			if ( $old->modify('+1 year') < $new)  {
				$amcperiod = "inactive";
			} else {
				$amcperiod = "active";
			}
			
			// If any condition Amc period or Amc Status
			if($amcstatus == 1 || $amcperiod == 'active') {
				
				// Update Random number generate in company table
				$verificationcodeandtime = array(
					'verificationcode'=>$randomnogenerate,
					'verificationcodedatetime'=>date($this->Basefunctions->datef)
				);
				$update = array_merge($verificationcodeandtime,$this->Crudmodel->updatedefaultvalueget());
				$serverdb->where('emailid',$uniquecomp_emailid);
				$serverdb->where('status',$this->Basefunctions->activestatus);
				$serverdb->update('company',$update);
				
				// PHP Mailer to send verification code to mail id
				$logempid = $this->Basefunctions->userid;
				$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$logempid);
				$empemail = $this->Basefunctions->generalinformaion('employee','emailid','employeeid',$logempid);
				$empemail = 'kumaresan@aucventures.com'; // Comment line
				$mail = new PHPMailer(true);
				/* try {
					$mail->IsSMTP();
					$mail->SMTPAuth   = true;
					$mail->SMTPSecure = "tls";
					$mail->Username   = "AKIATQIHVKJXZS2D2452";
					$mail->Password   = "BN2oAjkebotY/R2eR/h2ghepAb7KEuHyhPnqIGl0bjuz";
					$mail->Host       = "email-smtp.us-west-2.amazonaws.com";
					$mail->Port       = 587;
					$mail->SetFrom('arvind@aucventures.com','[Sales Neuron]');
					$mail->Subject   = "Sales Neuron: Verification Code for Updating Software";
					$expdate = date("YmdHis", strtotime('+1 days'));
					// Verification Code to be sent via Email
					$data = 'Verification Code : '.$randomnogenerate.'</br>';
					$body='<html><body>Hello '.$empname.',<br><br> Here is the '.$data.'<br><br>From <br>SalesNeuron Team</body></html>';
					$newbody = $body;
					$mail->IsHTML(true);
					$mail->MsgHTML($newbody);
					$mail->AddAddress(trim($empemail));
					$mail->AltBody="Reg: Verification Code for updating software";
					$mail->send();
				} catch (phpmailerException $e) {
					//Pretty error messages from PHPMailer
					echo $e->errorMessage();
				} catch (Exception $e) {
					//Boring error messages from anything else!
					echo $e->getMessage();
				} */
				$status = 'active';
			} else {
				$status = 'inactive';
			}
		} else {
			$status = 'amc';
		}
		echo $status;
	}
	// Check for AMC Details
	public function checkverificationcodeanddatetime() {
		
		$companyid = $_POST['compid'];
		$verificationcode = $_POST['verificationcode'];
		$presentdatetime=date('Y-m-d H:i:s');
		$uniquecomp_emailid = $this->Basefunctions->singlefieldfetch('emailid','companyid','company',$companyid);
		
		// Retrieve details from all_releasedetails
		$serverdb = $this->load->database('serverdb', TRUE); // all_releasedetails
		$serverdb->select('verificationcode,verificationcodedatetime',false);
		$serverdb->from('company');
		$serverdb->where('emailid',trim($uniquecomp_emailid));
		$serverdb->where('status',$this->Basefunctions->activestatus);
		$serverdb->limit(1);
		$cinfo = $serverdb->get()->row();
		if($cinfo != '') {
			$tabverificationcode = $cinfo->verificationcode;
			$tabverificationcodedatetime = $cinfo->verificationcodedatetime;
			if($verificationcode == $tabverificationcode) {
				$date_a = new DateTime($presentdatetime);
				$date_b = new DateTime($tabverificationcodedatetime);
				$interval = date_diff($date_a,$date_b);
				if(($interval->format('%h')) >= 1 && (($interval->format('%i')) > 0)) {
					/* $serverdb->set('verificationcodestatus','inactive');
					$serverdb->where('id',$cid);
					$serverdb->update('company'); */
					$data['randommessage'] = "Random Verification Code is inactive due to late response. Please resend verification code to your email id.";
					$result = 'Inactivetime';
				} else {
					$data['randommessage'] = "Random Verification Code has been successfully verified. Kindly Dont switch off your system.";
					$result = 'Active';
				}
			} else {
				$data['randommessage'] = "Random Verification Code is wrong. Please check your mail and try again!";
				$result = 'Inactive';
			}
		} else {
			$data['randommessage'] = "Something went problem. Please try again later!";
			$result = 'Failed';
		}
		echo json_encode(array('resultdata'=>$result,'randommessage'=>$data['randommessage']));
	}
	// Rate Calc settings create and update
	public function ratecalcsettingcreatemodels() {
		$primarypurityid = $_POST['primarypurityid'];
		$nonprimarypurityid = $_POST['nonprimarypurityid'];
		$ratecalculationid = $_POST['ratecalculationid'];
		$ratepercentvalue = $_POST['ratepercentvalue'];
		$check = $this->ratecalcdetailsexistcheck($primarypurityid,$nonprimarypurityid);
		$ratecalcdetailsarrray = array(
			'primarypurityid'=>$primarypurityid,
			'nonprimarypurityid'=>$nonprimarypurityid,
			'ratecalculationid'=>$ratecalculationid,
			'ratepercentvalue'=>$ratepercentvalue,
			'industryid'=>$this->Basefunctions->industryid,
			'branchid'=>$this->Basefunctions->branchid
		);
		if($check == 'FALSE') {
			$insertarray = array_merge($ratecalcdetailsarrray,$this->Crudmodel->defaultvalueget());
			$this->db->insert('ratecalcdetails',array_filter($insertarray));
		} else {
			$update = array_merge($ratecalcdetailsarrray,$this->Crudmodel->updatedefaultvalueget());
			//update data
			$this->db->where('ratecalcdetailsid',$check);
			$this->db->where('status',$this->Basefunctions->activestatus);
			$this->db->update('ratecalcdetails',$update);
		}
		echo 'SUCCESS';
	}
	// Old Details Check in table
	public function ratecalcdetailsexistcheck($primarypurityid,$nonprimarypurityid) {
		$data = 'FALSE';
		$this->db->select('ratecalcdetailsid');
		$this->db->from('ratecalcdetails');
		$this->db->where('ratecalcdetails.primarypurityid',$primarypurityid);
		$this->db->where('ratecalcdetails.nonprimarypurityid',$nonprimarypurityid);
		$this->db->where('ratecalcdetails.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data = $row->ratecalcdetailsid;
			}
		} else {
			$data = 'FALSE';
		}
		return $data;
	}
	// Rate Calc settings retrieve data
	public function ratecalcsettingretrivemodel(){
		$primaryid = $_GET['primaryid'];
		$id=$_GET['primaryid'];
		$this->db->select('ratecalcdetails.primarypurityid,ratecalcdetails.nonprimarypurityid,ratecalcdetails.ratecalculationid,ratecalcdetails.ratepercentvalue,ratecalcdetails.ratecalcdetailsid',false);
		$this->db->from('ratecalcdetails');
		$this->db->where('ratecalcdetails.ratecalcdetailsid',$primaryid);
		$this->db->where('ratecalcdetails.status',$this->Basefunctions->activestatus);
		$info=$this->db->get()->row();
		$infotag = array(
				'ratecalcdetailsid'=>$info->ratecalcdetailsid,
				'primarypurityid'=>$info->primarypurityid,
				'nonprimarypurityid'=>$info->nonprimarypurityid,
				'ratecalculationid'=>$info->ratecalculationid,
				'ratepercentvalue'=>$info->ratepercentvalue
		);
		echo json_encode($infotag);
	}
	// Rate Calc delete
	public function ratecalcdetailsdeletemodels() {
		$id = $_GET['primaryid'];
		$delete = array('status'=>0);
		$deletearray =array_merge($delete,$this->Crudmodel->updatedefaultvalueget());
		$this->db->where('ratecalcdetailsid',$id);
		$this->db->update('ratecalcdetails',$deletearray);
		echo 'SUCCESS';
	}
	// Success Email To Customer
	public function updatesuccessemail() {
		$companyid = $_POST['compid'];
		
		// PHP Mailer to send verification code to mail id
		$logempid = $this->Basefunctions->userid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$logempid);
		$empemail = $this->Basefunctions->generalinformaion('employee','emailid','employeeid',$logempid);
		$empemail = 'kumaresan@aucventures.com'; // Comment line
		$mail = new PHPMailer(true);
		try {
			$mail->IsSMTP();
			$mail->SMTPAuth   = true;
			$mail->SMTPSecure = "tls";
			$mail->Username   = "AKIATQIHVKJXZS2D2452";
			$mail->Password   = "BN2oAjkebotY/R2eR/h2ghepAb7KEuHyhPnqIGl0bjuz";
			$mail->Host       = "email-smtp.us-west-2.amazonaws.com";
			$mail->Port       = 587;
			$mail->SetFrom('arvind@aucventures.com','[Sales Neuron]');
			$mail->Subject   = "Sales Neuron: Verification Code for Updating Software";
			$expdate = date("YmdHis", strtotime('+1 days'));
			// Verification Code to be sent via Email
			$data = 'Software Updation Details';
			$body='<html><body>Hello '.$empname.',<br><br> Software Updated Successfully. Kindly restart the software and also clear browser history to get your new updated details.<br><br>From <br>SalesNeuron Team</body></html>';
			$newbody = $body;
			$mail->IsHTML(true);
			$mail->MsgHTML($newbody);
			$mail->AddAddress(trim($empemail));
			$mail->AltBody="Reg: Verification Code for updating software";
			//$mail->send();
		} catch (phpmailerException $e) {
			//Pretty error messages from PHPMailer
			echo $e->errorMessage();
		} catch (Exception $e) {
			//Boring error messages from anything else!
			echo $e->getMessage();
		}
		echo 'SUCCESS';
	}
}