<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Base extends MX_Controller {
	//To load the Register View
	function __construct() {
    	parent::__construct();
    	$this->load->helper('formbuild');
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Dashboardfieldgeneration');
		$this->load->model('Printtemplates/Printtemplatesmodel');
		$this->load->view('Base/formfieldgeneration');
    }
	public function index() {
		/* $moduleid = array(1);
		$data['dashboradviewdata'] = $this->Basefunctions->dashboarddataviewbydropdown($moduleid); */
	}
	//dash board generate
	public function dashboardcreatedatafetch() {
		if(isset($_POST['data'])) {
			$dashboardid = $_POST['data'];
		} else {
			$dashboardid = 1;
		}
		if(!isset($_POST['rowid'])) {
			$rowid = 1;
		} else {
			$rowid =$_POST['rowid'];
		}
		if(isset($_POST['minidashboard'])){
			$minidashboardstatus = 1;
		}else{
			$minidashboardstatus = 0;
		}
		if(isset($_POST['notestatus'])){
			$notestatus = $_POST['notestatus'];
		}else{
			$notestatus = 2; // main dashboard
		}
		$dashboardchartdata = $this->Basefunctions->dashboarddatainformationfetch($dashboardid,$minidashboardstatus,$notestatus);
		$this->Dashboardfieldgeneration->dashboardcreate($dashboardchartdata,$rowid);
	}
	//simple widget generation
	public function basicinformationvalfetch() {
		$tablname = $_POST['tabname'];
		$fieldnames = $_POST['fieldname'];
		$jointablnames = $_POST['jointabname'];
		$joinfieldids = $_POST['joinfiledids'];
		$recordcount = $_POST['datacount'];	
		$rowid=$_POST['rowid'];	
		//
		$tablename = implode(',',explode('|',$tablname));
		$fieldname = implode(',',explode('|',$fieldnames));
		$jointablename = implode(',',explode('|',$jointablnames));
		$joinfieldid = implode(',',explode('|',$joinfieldids));
		$dashboarddata=array('moduleid'=>$_POST['dashboardmoduleid'],'charttype'=>$_POST['dashboardboardname']);	
		echo $this->Basefunctions->basicinformationvalfetchmodel($tablename,$fieldname,$jointablename,$joinfieldid,$recordcount,$dashboarddata,$rowid);
	}
	//for view
	//view name check
	public function dynamicviewnamecheck() {
		$this->Basefunctions->dynamicviewnamecheckmodel();
	}
	//grid header cloumn information
	public function gridheaderinformationfetch() {
		$creationid = $_GET['viewid'];
		$information = $this->Basefunctions->gridinformationfetchmodel($creationid);
		echo json_encode($information);
	}
	/* Fetch Main grid header,data,footer information */
	public function gridinformationfetch() {
		$creationid = $_GET['viewid'];
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$viewcolmoduleids = $_GET['viewfieldids'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$filterid = isset($_GET['filter']) ? $_GET['filter'] : '';
		$conditionname = isset($_GET['conditionname']) ? $_GET['conditionname'] : '';
		$filtervalue = isset($_GET['filtervalue']) ? $_GET['filtervalue'] : '';
		$filter = array('0'=>$filterid,'1'=>$conditionname,'2'=>$filtervalue);
		$footer = isset($_GET['footername']) ? $_GET['footername'] : 'empty';
		$sortcol = isset($_GET['sortcol']) ? $_GET['sortcol'] : '';
		$sortord = isset($_GET['sortord']) ? $_GET['sortord'] : '';
		$result = $this->Basefunctions->generatemainview($creationid,$pagenum,$rowscount,$sortcol,$sortord,$primaryid,$viewcolmoduleids,$filter);
		$device = $this->Basefunctions->deviceinfo();
		/*if($device=='phone') {
			$datas = mobileviewgriddatagenerate($result,$primarytable,$primaryid,$width,$height);
		} else {
		*/
			$datas = viewgriddatagenerate($result,$primarytable,$primaryid,$width,$height,$footer);
		//}
	    echo json_encode($datas);
	}
	/* Local non-live grid heder information fetch */
	public function localgirdheaderinformationfetch() {
		$moduleid = $_GET['moduleid'];
		$tabgrpid = $_GET['tabgroupid'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modname = $_GET['modulename'];
		$colinfo = $this->Basefunctions->localgridheaderinformationfetchmodel($moduleid,$tabgrpid);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobileviewformtogriddatagenerate($colinfo,$modname,$width,$height);
		} else {
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height);
		}
		echo json_encode($datas);
	}
	//fetch json data information
	public function gridvalinformationfetch() {
		$creationid = $_GET['viewid'];
		$rowid = $_GET['parentid'];
		$viewcolmoduleids = $_GET['viewfieldids'];
		$grid=$this->Basefunctions->getpagination();
		$result=$this->Basefunctions->viewgridrecorddynamicdatainfofetch($grid['sidx'],$grid['sord'],$grid['start'],$grid['limit'],$grid['wh'],$creationid,$rowid,$viewcolmoduleids);
		$pageinfo=$this->Basefunctions->page();
		header("Content-type: text/xml;charset=utf-8");
		$s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .=  "<rows>";
        $s .= "<page>".$_GET['page']."</page>";
        $s .= "<total>".$pageinfo['totalpages']."</total>";
        $s .= "<records>".$pageinfo['count']."</records>";
		$count = 0;
		if(count($result[0]) > 0) {
			$count = count($result[0]['colmodelname']);
		}
		foreach($result[1]->result() as $row) {
			$s .= "<row id='". $row->$rowid ."'>";
			for($i=0;$i<$count;$i++) {
				if($result[0]['colmodeluitype'][$i] == 20) {
					$value = $row->$result[0]['colmodelname'][$i].$row->rolename.$row->empgrpname;
					$s .= "<cell><![CDATA[".$value."]]></cell>";
				} else {
					if($result[0]['colmodelname'][$i]=='commonid') {
						$value = $row->$result[0]['colmodelname'][$i];
						$table = $result[0]['coltablename'][$i];
						$relval = $this->Basefunctions->getrelatedmodulefieldvalues($value,$_GET['maintabinfo'],$table,$row->$rowid);
						$relval = (($relval!='')?$relval:'');
						$s.="<cell><![CDATA[".$relval."]]></cell>";
					} else {
						$s.="<cell><![CDATA[".$row->$result[0]['colmodelname'][$i]."]]></cell>";
					}
				}
			}
			$s .= "</row>";
		}
		$s .= "</rows>";  
		echo $s;
	}
	/* Dynamically set header col size */
	public function headercolumnresizeset() {
		$this->Basefunctions->headercolumnresizesetmodel();
	}
	//fetch drop down value info
	public function fetchdddataviewddval() {
       $this->Basefunctions->fetchdddataviewddvalmodel();
    }
	//fetch drop down value info with uitype
	public function fetchdduitypedataviewddval() {
       $this->Basefunctions->fetchdduitypedataviewddvalmodel();
    } 
	//field name based drop down value
	public function fieldnamebesdddvalue(){
		$this->Basefunctions->fieldnamebesdddvaluemodel();
	}
	//lead conversion field name based drop down value
	public function leadconversionfieldnamebesdddvalue(){
		$this->Basefunctions->leadconversionfieldnamebesdddvaluemodel();
	}
	//field name based drop down value with condition
	public function fieldnamebesdddvaluewithcond(){
		$this->Basefunctions->fieldnamebesdddvaluewithcondmodel();
	}
	//field name based drop down value
	public function fieldnamebesdpicklistddvalue(){
		$this->Basefunctions->fieldnamebesdpicklistddvaluemodel();
	}
	//dynamic view create form
	public function viewcreatecon() {
		$this->Basefunctions->viewcreateconmodel();
	}
	//get contact depend data
	public function fetchcontactvalue(){
		$this->Basefunctions->fetchcontactvaluedata();
	}
	//check box value get
	public function checkboxvalueget(){
		 $this->Basefunctions->checkboxvaluegetmodel();
	}
	//default view set
	public function viewdefaultset() {
		$empid = $this->Basefunctions->userid;
		$vmaxid = $_POST['viewid'];
		$moduleid = $_POST['moduleid'];
		$mid = $this->Basefunctions->getmoduleid($moduleid);
		$this->Basefunctions->viewdefaultsetmodel($empid,$vmaxid,$mid);
	}
	//employee group drop down val fetch
	public function viewemployeegroupsdata() {
		$datas = $this->Basefunctions->userspecificgroupdropdownvalfetch();
		if(count($datas)>0) {
			echo json_encode($datas);
		} else {
			echo json_encode(array('status'=>'fail'));
		}
	}
	//clone data
	public function viewdataclone() {
		$empid = $this->Basefunctions->userid;
		$vmaxid = $_POST['viewid'];
		$moduleid = $_POST['moduleid'];
		$parenttable = 'viewcreation';
		$childtabinfo = 'viewcreationcondition';
		$fieldinfo = 'viewcreationid';
		$fieldvalue = $vmaxid;
		$updatecloumn = 'viewcreationname';
		$viewid = $this->Basefunctions->viewdataclonemodel($parenttable,$childtabinfo,$fieldinfo,$fieldvalue);
		if($updatecloumn!='') {
			$this->db->query(' UPDATE '.$parenttable.' SET '.$updatecloumn.' = concat('.$updatecloumn.'," clone") where '.$fieldinfo.' = '.$viewid.'');
		}
		echo 'TRUE';
	}
	//dynamic view delete
	public function gridinformationdelete() {
		$this->Basefunctions->gridinformationdeletemodel();
	}
	//dynamic view edit data fetch
	public function fetchvieweditdetails() {
		$this->Basefunctions->fetchvieweditdetailsmodel();
	}
	public function fetchdefval(){
		$this->Basefunctions->fetchdefvalmodel();
	}
	//dynamic view update form
	public function viewupdatecondition() {
		$this->Basefunctions->viewupdateconmodel();
	}
	//pagination set page
	public function generatepage($p) {
		if($p=='NaN')  {
			$p=1; //all
		} else {
			$p=$p;//harcoded value
		}
		return $p;
	}
	//condition grid header information fetch
	public function viewconddatafetch() {
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$modname = $_GET['modulename'];
		$colinfo = $this->Basefunctions->viewconddatafetchmodel($modid);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height);
		} else {
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height);
		}
		echo json_encode($datas);
	}
	//condition grid details fetch
	public function  conditiongriddatafetch() {
		$this->Basefunctions->conditiongriddatafetchmodel();
	}
	//For view end code
	//drop down value set with multiple condition
	public function fetchdddatawithmultiplecond() {
		$this->Basefunctions->fetchdddatawithmultiplecondmodel();
	}
	//random number generation
	public function generatenumber() {
		$moduleid=$_GET['moduleid'];
		$table=$_GET['table'];
		$gennumber=$this->Basefunctions->generatenumber($moduleid,$table);
		$paymentnumber=$this->Basefunctions->generatepaymentnumber();
		$a=array('number'=>$gennumber,'paymentnumber'=>$paymentnumber);
		echo json_encode($a);
	}
	//payment number//salesorder-invoice-purchaseorder
	public function paymentnumber() {
		$paymentnumber=$this->Basefunctions->generatepaymentnumber();
		$a=array('paymentnumber'=>$paymentnumber);
		echo json_encode($a);
	}
	//retrieve dropdown options
	public function getdropdownoptions() {
		$table=$_GET['table'];
		$this->Basefunctions->getdropdownoptions($table);
	}
	//retrive address
	public function getcrmaddress() {
		$this->Basefunctions->getcrmaddress();
	}
	/*
	*It returns the Product/item details
	*/
	public function getproductdetails() {
		$id=$_GET['id'];
		$this->Basefunctions->getproductdetails($id);
	}
	//terms and condition data fetch
	public function termsandcontdatafetch() {
		$this->Basefunctions->termsandcontdatafetchmodel();
	}	
	//terms and condition -- content fetch function
	public function tandccontentfetch() {
		$filename = $_GET['filename'];
		$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$filename);
		echo json_encode($tccontent);
	}
	//get default currency
	public function getdefaultcurrency() {
		$data=$this->Basefunctions->getdefaultcurrency();
		echo $data;
	}
	//price booc view
	public function pricebookview() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'pricebookdetail.pricebookdetailid') : 'pricebookdetail.pricebookdetailid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'ASC') : 'ASC';
		$colinfo = array('colmodelname'=>array('pricebookdetailid','pricebookname','currencyname','sellingprice','description','currencyid'),'colmodelindex'=>array('pricebookdetail.pricebookdetailid','pricebook.pricebookname','currency.currencyname','pricebookdetail.sellingprice','pricebook.description','pricebook.currencyid'),'coltablename'=>array('pricebookdetail','pricebook','currency','pricebookdetail','pricebook','pricebook'),'uitype'=>array('2','2','2','2','2','2'),'colname'=>array('Pricebook Detail Id','Pricebook Name','Currency Name','Selling Price','Description','Currency Id'),'colsize'=>array('200','200','200','200','200','200'));
		$result=$this->Basefunctions->pricebookview($primarytable,$sortcol,$sortord,$pagenum,$rowscount);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Pricebook List',$width,$height);
		} else {
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Pricebook List',$width,$height);
		}
		echo json_encode($datas);
	}
	/* Fetch user branch information */
	public function userbranch() {
		$userid=$this->Basefunctions->userid;
		$employeedata=$this->db->select('employeename,branchid')->from('employee')->where('employeeid',$userid)->get();
		$emprow=$employeedata->row();
		$employeename=$emprow->employeename;
		$branchid=$emprow->branchid;
		$array=array('employeename'=>$employeename,'branchid'=>$branchid);
		echo json_encode($array);
	}
	//unique name restriction
	public function uniquedynamicviewnamecheck(){
		$this->Basefunctions->uniquedynamicviewnamecheckmodel();
	}
	//unique short name restriction
	public function uniqueshortdynamicviewnamecheck(){
		$this->Basefunctions->uniqueshortdynamicviewnamecheckmodel();
	}
	//check cancel status
	public function checkcancelstatus() {
		$this->Basefunctions->checkcancelstatus();
	}
	//check valid email 
	public function checkvalidemail() {
		$this->Basefunctions->checkvalidemail();
	}
	//get valid email 
	public function getvalidemaildetail() {
		$this->Basefunctions->getvalidemaildetail();
	}
	//get account details
	public function getaccountid() {
		$this->Basefunctions->getaccountid();
	}
	//attribute value fetch
	public function attributevalfetch() {
		$moduleid = $_GET['moduleid'];
		$this->Basefunctions->attributevaluedataddfetch($moduleid);
	}
	public function attributedropdownvaluefetch() {
		$this->Basefunctions->attributedropdownvaluefetchmodel();
	}
	//random number generator //ramesh
	public function randomnumgenerate() {
		$moduleid = $_POST['mid'];
		$table = $_POST['tabname'];
		$filedname = $_POST['fname'];
		$modulefieldid = $_POST['fnameid'];
		$randomnum = $this->Basefunctions->randomnumbergenerator($moduleid,$table,$filedname,$modulefieldid);
		echo json_encode($randomnum);
	}
	//fetch drop down value info
	public function pdfpreviewfetchdddataviewddval() {
       $this->Basefunctions->pdfpreviewfetchdddataviewddvalmodel();
    }
	//fetch drop down value info - Tag template
	public function tagtemplatesloadddval() {
       $this->Basefunctions->tagtemplatesloadddvalmodel();
    }
	//check the data is active or deleted(used on notifications/audit log hyperlink)
	public function checkvalidrecord(){
		$this->Basefunctions->checkvalidrecord();
	}	
	//order based product
	public function getorderbasedproduct(){
		$i=0;
		$ordertypeid = $_GET['ordertypeid'];
		$value='';
		$product = array();
		$softwareindustryid = $this->Basefunctions->industryid;
		if($softwareindustryid == 4){
			if($_GET['producthide'] == 'Yes'){
				$this->db->select("product.productid,category.categoryid,category.categoryname,product.productname");
				$this->db->from("product");
				$this->db->join("category","category.categoryid=product.categoryid");
				$this->db->where_in('product.ordertypeid',array(4,6));
				$this->db->where('product.sales','Yes');
				$this->db->where_not_in("product.ordertypeid",array($ordertypeid));
				$this->db->where("product.status",1);
			}else{
				$this->db->select("product.productid,category.categoryid,category.categoryname,product.productname");
				$this->db->from("product");
				$this->db->join("category","category.categoryid=product.categoryid");
				$this->db->where('product.sales','Yes');
				$this->db->where("product.ordertypeid",$ordertypeid);
				$this->db->where("product.status",1);
			}			
		}else{
			$this->db->select("product.productid,category.categoryid,category.categoryname,product.productname");
			$this->db->from("product");
			$this->db->join("category","category.categoryid=product.categoryid");
			$this->db->where('product.sales','Yes');
			$this->db->where("product.ordertypeid",$ordertypeid);
			$this->db->where("product.status",1);
		}		
		$data = $this->db->get();
		$pid = 1;
		foreach($data->result() as $info) {	
			$date = date("Y-m-d");
			if($softwareindustryid != 4){
				$this->db->select("product.productid");
				$this->db->from("product");
				$this->db->where("product.productid",$info->productid);
				$this->db->where("product.endoflife >",$date);
				$result = $this->db->get();
				if($result->num_rows() > 0) {
					$product[$i] = array('optclass'=>$info->categoryname,'optionclass'=>$info->productid.'pclass','data-productidhidden'=>$info->productname,'value'=>$info->productid,'PId'=>$pid);
					$i++;
					$pid++;
				}
			}else{
				$product[$i] = array('optclass'=>$info->categoryname,'optionclass'=>$info->productid.'pclass','data-productidhidden'=>$info->productname,'value'=>$info->productid,'PId'=>$pid);
				$i++;
				$pid++;
			}						
		}		
		echo json_encode($product);
	}
	//order based price book fetch function
	public function getpricebookdetail(){
		$territoryid = $_GET['territoryid'];
		if($territoryid == ''){
			$pricebook=$this->db->select('pricebookid,pricebookname')
				->from('pricebook')
				->where('industryid',$this->Basefunctions->industryid)
				->where('status',$this->Basefunctions->activestatus)
				->get();
		}else{
			$pricebook=$this->db->select('pricebookid,pricebookname')
				->from('pricebook')
				->where('territoryid like','%'.$territoryid.'%')
				->where('industryid',$this->Basefunctions->industryid)
				->where('status',$this->Basefunctions->activestatus)
				->get();
		}		
		$pricebookarr = [];
		 foreach($pricebook->result() as $value)
		 {
		 	$pricebookarr[]=array('id'=>$value->pricebookid,'name'=>$value->pricebookname);
		 }
		 echo json_encode($pricebookarr);
	}
	/*
	*Retrives the product records for product search grid
	*/
	public function getproductsearch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$industryid = $_GET['industryid'];
		if(isset($_GET['ordertype'])){
			$ordertype = $_GET['ordertype'];
		}else{
			$ordertype = 1;
		}		
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'product.productid') : 'product.productid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'ASC') : 'ASC';
		$colinfo = array('colmodelname'=>array('categoryname','productname','attributevalues'),'colmodelindex'=>array('category.categoryname','product.productname','product.attributevalues'),'coltablename'=>array('categoryname','productname','attributevalues'),'uitype'=>array('2','2','2'),'colname'=>array('Category','Product Name','Attribute Values'),'colsize'=>array('200','200','200'));
		$result=$this->Basefunctions->getproductsearch($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$ordertype,$industryid);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Product List',$width,$height);
		} else {
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Product List',$width,$height);
		}
		echo json_encode($datas);
	}	
	//view createuserid and login userid check
	public function viewcreateuseridget() {
		$this->Basefunctions->viewcreateuseridgetmodel();
	}
	/*
	*Retrives the Widget related data based on "widget data mapping".
	*@param $moduleid the moduleid
	*@param $widgetid related widget
	*/
	public function getwidgetrelateddata() {
		$primaryid = trim($_GET['rowid']);
		$moduleid = trim($_GET['moduleid']);
		$widgetid = trim($_GET['widgetid']);
		if($primaryid > 0 AND $moduleid > 1 AND $widgetid > 1){
			$output=$this->Basefunctions->getwidgetrelateddata($primaryid,$moduleid,$widgetid);
			echo $output;
		}
		else{
			echo json_encode(array('mstatus'=>'Failed'));
		}
	}	
	/*
	*Retrives the quote or salesorder or invoice or purchaseorder data on module for data transmissions.
	*@param 
	*/
	public function getmoduledatanumber() {		
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		if (in_array($modid, [216,85,94])) {
			$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'quote.quoteid') : 'quote.quoteid';
			$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
			$colinfo = array('colmodelname'=>array('quotedate','quotenumber','accountname','crmstatusname'),'colmodelindex'=>array('quote.quotedate','quote.quotenumber','account.accountname','crmstatus.crmstatusname'),'coltablename'=>array('quote','quote','account','crmstatus'),'uitype'=>array('8','14','19','17'),'colname'=>array('Quote Date','Quote Number','Account Name','Quote Status'),'colsize'=>array('200','200','200','200'));
			$primarytable = 'quote';
			$primaryid = 'quoteid';
		}
		else if (in_array($modid, [217,87,96])){
			$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'salesorder.salesorderid') : 'salesorder.salesorderid';
			$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
			$colinfo = array('colmodelname'=>array('salesorderdate','salesordernumber','accountname','crmstatusname'),'colmodelindex'=>array('salesorder.salesorderdate','salesorder.salesordernumber','account.accountname','crmstatus.crmstatusname'),'coltablename'=>array('salesorder','salesorder','account','crmstatus'),'uitype'=>array('8','14','19','17'),'colname'=>array('SO Date','Salesorder Number','Account Name','Status'),'colsize'=>array('200','200','200','200'));
			$primarytable = 'salesorder';
			$primaryid = 'salesorderid';
		}
		else if (in_array($modid, [226,86,95])){
			$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'invoice.invoiceid') : 'invoice.invoiceid';
			$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
			$colinfo = array('colmodelname'=>array('invoicedate','invoicenumber','accountname','crmstatusname'),'colmodelindex'=>array('invoice.invoicedate','invoice.invoicenumber','account.accountname','crmstatus.crmstatusname'),'coltablename'=>array('invoice','invoice','account','crmstatus'),'uitype'=>array('8','14','19','17'),'colname'=>array('Invoice Date','Invoice Number','Account Name','Status'),'colsize'=>array('200','200','200','200'));
			$primarytable = 'invoice';
			$primaryid = 'invoiceid';
		}
		else if($modid == 249){
			$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'materialrequisition.materialrequisitionid') : 'materialrequisition.materialrequisitionid';
			$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
			$colinfo = array('colmodelname'=>array('requisitiondate','materialrequisitionnumber','accountname','requisitionstatusname'),'colmodelindex'=>array('materialrequisition.requisitiondate','materialrequisition.materialrequisitionnumber','account.accountname','requisitionstatus.requisitionstatusname'),'coltablename'=>array('materialrequisition','materialrequisition','account','requisitionstatus'),'uitype'=>array('8','14','19','17'),'colname'=>array('Material Reqisition Date','Material Reqisition Number','Account Name','Status'),'colsize'=>array('200','200','200','200'));
			$primarytable = 'materialrequisition';
			$primaryid = 'materialrequisitionid';
		}
		else if (in_array($modid, [225,88,99])) {
			$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'purchaseorder.purchaseorderid') : 'purchaseorder.purchaseorderid';
			$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
			$colinfo = array('colmodelname'=>array('purchaseordernumber','purchaseorderdate','accountname','crmstatusname'),'colmodelindex'=>array('purchaseorder.purchaseordernumber','purchaseorder.purchaseorderdate','account.accountname','crmstatus.crmstatusname'),'coltablename'=>array('purchaseorder','purchaseorder','account','crmstatus'),'uitype'=>array('14','8','19','17'),'colname'=>array('Purchase Order Number','Purchase Order Date','Purchase Order To','Status'),'colsize'=>array('200','200','200','200'));
			$primarytable = 'purchaseorder';
			$primaryid = 'purchaseorderid';
		}
		$result=$this->Basefunctions->getmoduledatanumber($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$modid);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Module Number',$width,$height);
		} else {
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Module Number',$width,$height);
		}
		echo json_encode($datas);
	}
	//used to fetch the conversation data 
	public function getconversiondata(){
		if($_POST['moduleid'] > 0 AND $_POST['primaryid'] > 0 AND $_POST['tomoduleid'] > 0 ){
			$this->Basefunctions->getconversiondata();
		}
	}
	//fetch company name
	public function getcompanyname() {
		$this->Basefunctions->getcompanyname();
	}
	// click to call function
	public function clicktocallfunction() {
		$this->Basefunctions->clicktocallfunctionmodel();
	}
	//mobile number dd load
	public function mobilenumberddload() {
		$this->Basefunctions->mobilenumberddloadmodel();
	}
	//mobile number information fetch
	public function mobilenumberinformationfetch() {
		$moduleid = $_GET['viewid'];
		$recordid = $_GET['recordid'];
		$mobilenumber = $this->Basefunctions->mobilenumberinformationfetchmodel($moduleid,$recordid);
		echo json_encode($mobilenumber);
	}
	//sms icon / click to call- module drop down load
	public function moduleddload() {
		$this->Basefunctions->moduleddloadmodel();
	}
	//record id that selected on that module
	public function mobilenumberfetchbasedonmodule() {
		$this->Basefunctions->mobilenumberfetchbasedonmodulemodel();
	}
	//fetch the related module of main module
	public function fetchlinkmoduleidfrommainmodule() {
		$moduleid = $_GET['moduleid'];
		$data = $this->Basefunctions->fetchlinkmoduleidfrommainmodule($moduleid);
		if(!empty($data)) {
			echo 'Success';
		} else {
			echo 'Failed';
		}
		print_r($data);
	}
	//unique field name value check
	public function uniquefieldvalcheck() {
		$this->Basefunctions->uniquefieldvalcheckmodel();
	}
	//fetch update information on every application update
	public function fetchupdatedata(){
		$this->Basefunctions->fetchupdatedatasetmodel();
	}
	//check valid email and do not call for account/lead/contact
	public function checkvalidemailanddonotmail() {
		$this->Basefunctions->checkvalidemailanddonotmail();
	}
	//check valid call for account/lead/contact
	public function validatecall() {
		$this->Basefunctions->validatecall();
	}

	/*
	Vardaan Code Integration Start
	*/
	//Delete function =>Checking Single Table Level Mapping
	public function checkmapping() {
		$deleteid=explode(',',$_GET['datarowid']);
		$result=$this->Basefunctions->singlelevelmapping($_GET['level'],$deleteid,$_GET['table'],$_GET['fieldid']);
		echo $result;
	}
	//Delete function =>Checking Multi Table Level Mapping
	public function multilevelmapping() {
		$deleteid=explode(',',$_GET['datarowid']);
		$result=$this->Basefunctions->multilevelmapping($_GET['level'],$deleteid,$_GET['table'],$_GET['fieldid'],$_GET['table1'],$_GET['fieldid1'],$_GET['table2'],$_GET['fieldid2']);
		echo $result;
	}
	/* Stock Entry */
	// Load - Condition based on UItype
	public function loadconditionbyuitype() {
		$this->Basefunctions->loadconditionbyuitype();
	}
	/* Sales => Transaction Start. */
	/** *	get the rate of metal/purity */
	public function getrate() {		
		$purity = $_GET['purity'];
		$randomnum = $this->Basefunctions->getrate($purity);
		echo json_encode($randomnum);			
	}
	//check unique name
	public function checkuniquename() {	
		$table = $_GET['table'];
		$fieldname = $_GET['fieldname'];
		$value = trim($_GET['value']);
		$editid = trim($_GET['editid']);
		$this->Basefunctions->checkuniquename($table,$fieldname,$value,$editid);
	}
	//related module list fetch
	public function relatedmodulelistfetchmodel() {
		$this->Basefunctions->relatedmodulelistfetchmodel();
	}
	//check report type 
	public function checkreporttype() {
		$arrcalccolumns=$this->Basefunctions->checkreporttypemodel();
		echo json_encode($arrcalccolumns);
	}
	//view create and edit form data fetch
	public function viewcreateandecitformfetch() {
		$id = $_GET['moduleid'];
		$moduleid = $this->Basefunctions->getmoduleid(explode(',',$id));
		$viewfieldsmoduleids = array($moduleid);
		$viewmoduleid = array($moduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['moduleids'] = $viewmoduleid;
		$this->load->view('Base/viewcreateandeditform',$data);
	}
	//view delete form details fetch
	public function viewdeleteformfetch() {
		$this->load->view('Base/viewdeleteform');
	}
	// get print template type
	public function getprinttemplatemode() {
		$templateid = $_POST['templateid'];
		$printtypeid = $this->Basefunctions->singlefieldfetch('printtypeid','printtemplatesid','printtemplates',$templateid);
		$html = 'No';
		if($printtypeid == 3) {
			$html = 'Yes';
		} else if($printtypeid == 4) {
			$html = 'Excel';
		} else if($printtypeid == 2) {
			$html = 'pdf';
		}
		echo $html;
	}
	//template pdf preview 
	public function templatefilepdfpreview() {
		$id = trim($_GET['id']);
		$randomnumber = trim($_GET['randomnumber']);
		$templateid = trim($_GET['templateid']);
		$pdfmoduleid = trim($_GET['pdfmoduleid']);
		if(isset($_GET['viewtype'])) { 
			$viewtype = trim($_GET['viewtype']);
		} else {
			$viewtype = "print";
		}
		if(isset($_GET['snumber'])) { 
		$snumber = trim($_GET['snumber']);
		}else{$snumber = '';}
		if(isset($_GET['stocksessionid'])) { 
		$stocksessionid = trim($_GET['stocksessionid']);
		}else{$stocksessionid = '';}
		if(isset($_GET['stockdate'])) { 
		$stockdate = trim($_GET['stockdate']);
		}else{$stockdate = '';}
		if(isset($_GET['stocktodate'])) { 
		$stocktodate = trim($_GET['stocktodate']);
		}else{$stocktodate = '';}
		if(isset($_GET['htmlaccountid'])) { 
		$htmlaccountid = trim($_GET['htmlaccountid']);
		}else{$htmlaccountid = '1';}
		$print_data=array('templateid'=> $templateid,'Templatetype'=>'Printtemp','primaryset'=>'sales.salesid','primaryid'=>$id,'stockdate'=>$stockdate,'stocktodate'=>$stocktodate,'snumber'=>$snumber,'allstocksessionid'=>$stocksessionid,'pdfmoduleid'=>$pdfmoduleid,'randomnumber'=>$randomnumber,'viewtype'=>$viewtype,'htmlaccountid'=>$htmlaccountid);
		$data = $this->Printtemplatesmodel->generateprinthtmlfile($print_data);
		echo $data;
	}
	//condition grid details fetch
	public function  reportconditiongriddatafetch() {
		$this->Basefunctions->reportconditiongriddatafetch();
	}
	//condition header information fetch
	public function reportconddatafetch() {
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$modname = $_GET['modulename'];
		$colinfo = $this->Basefunctions->reportconddatafetchmodel($modid);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height);
		} else {
			$datas = localviewgridheadergenerate($colinfo,$modname,$width,$height);
		}
		echo json_encode($datas);
	}
	//set report column with grouping-
	public function setreportcolumndata() {
		$viewfieldsmoduleids=array_filter(explode(',',$_GET['moduleid']));
		$data=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$prev = ' ';
		foreach($data as $key) {
			$amid=explode(",",$key->aggregatemethodid);
			$this->db->select('GROUP_CONCAT(aggregatemethodname) as aggregatemethodname,GROUP_CONCAT(aggregatemethodkey) as aggregatemethodkey',false);
			$this->db->from('aggregatemethod');
			$this->db->where_in('aggregatemethodid',$amid);
			$this->db->where('status',$this->Basefunctions->activestatus);
			$sumval=$this->db->get()->row();
			echo '<option data-aggregatemethodkey='.$sumval->aggregatemethodkey.' data-aggregatemethodid='.$key->aggregatemethodid.' data-applysummary="" data-selecttype="" data-selecttypename="" data-uitype='.$key->uitypeid .'   value=' . $key->viewcreationcolumnid . '>	'. $key->modulename .' => ' . $key->viewcreationcolumnname . '</option>';
		}
	}
	//set report column with grouping-
	public function setmainviewcolumndata() {
		$viewfieldsmoduleids=array_filter(explode(',',$_GET['moduleid']));
		$data=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$prev = ' ';
		foreach($data as $key) {
			$amid=explode(",",$key->aggregatemethodid);
			$this->db->select('GROUP_CONCAT(aggregatemethodname) as aggregatemethodname,GROUP_CONCAT(aggregatemethodkey) as aggregatemethodkey',false);
			$this->db->from('aggregatemethod');
			$this->db->where_in('aggregatemethodid',$amid);
			$this->db->where('status',$this->Basefunctions->activestatus);
			$sumval=$this->db->get()->row();
			echo '<option data-aggregatemethodkey='.$sumval->aggregatemethodkey.' data-aggregatemethodid='.$key->aggregatemethodid.' data-applysummary="" data-selecttype="" data-selecttypename="" data-uitype='.$key->uitypeid .'   value=' . $key->viewcreationcolumnid . '>	'.$key->viewcreationcolumnname . '</option>';
		}
	}
	// Load - Aggregate Method based on UItype
	public function loadaggregatemethodbyuitype() {
		$this->Basefunctions->loadaggregatemethodbyuitype();
	}
	//editer valuefetch editervaluefetch
	public function editervaluefetch() {
		$filename = $_POST['filename'];
		$filename = explode(',',$filename);
		for($i=0;$i<count($filename);$i++) {
			$newfilename = explode('/',$filename[$i]);
			if(read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1])) {
				$tccontent[$i] = read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]);
			} else {
				$tccontent = "Fail";
			}
		}
		echo json_encode($tccontent);
	}
	// assign user validdate
	public function assignuservalid() {
		$this->Basefunctions->assignuservalid();
	}
	//request atraining mail sent
	public function requestatrainigmailsent() {
		$this->Basefunctions->requestatrainigmailsentmodel();
	}
	//upgrade plan mail sent
	public function upgradeplanmailsent() {
		$this->Basefunctions->upgradeplanmailsentmodel();
	}
	//get id of stock entry and transaction
	public function stockentrytransactionidfetch() {
		ini_set('max_execution_time', 0);
		$moduleid = $_POST['moduleid'];
		if($moduleid == 50) {
			$result = $this->db->query('SELECT itemtagid as primaryid FROM `itemtag` where status not in (0,3)');
		} else {
			$result = $this->db->query('SELECT salesid as primaryid FROM `sales` where status not in (0,3)');
		}
		foreach($result->result() AS $row) {
			$this->Basefunctions->stocktableentryfunction($row->primaryid,$moduleid);
		}
		echo 'Success';
	}
	//base modulefilter datafetch
	public function basemodulefilterdatafetch() {
		$this->Basefunctions->basemodulefilterdatafetchmodel();
	}
	//load username except assigned
	public function loadusername() {
		$this->Basefunctions->loadusername();
	}
	//function call for form fields generation
	public function gridaddfetch() {
	$modtabgrp = unserialize(base64_decode($_GET['modtabgrp']));
		 formfieldstemplategenerator($modtabgrp);
	}
	//Category field - required information
	public function categorytreerequiredfield() {
		$this->Basefunctions->categorytreerequiredfield();
	}
	// Auto Complete Address
	public function autocompleteaddress() {
		$this->Basefunctions->autocompleteaddress();
	}
	// Auto Complete Area
	public function autocompletearea() {
		$this->Basefunctions->autocompletearea();
	}
	// Auto Complete Postal Code
	public function autocompletepostalcode() {
		$this->Basefunctions->autocompletepostalcode();
	}
	// Auto Complete City
	public function autocompletecity() {
		$this->Basefunctions->autocompletecity();
	}
	// Retrive Itemtag Details
	public function retrieveitemtagdetails() {
		$this->Basefunctions->retrieveitemtagdetails();
	}
}