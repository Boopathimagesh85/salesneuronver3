<!-- For Date Formatter -->
<?php 
$CI =& get_instance();
$appdateformat = $CI->Basefunctions->appdateformatfetch(); 
?>
<style>
 body {
      font-family: 'Roboto', sans-serif;
      -webkit-font-smoothing: antialiased;
    }

    .c-btn {
      font-size: 14px;
      text-transform: capitalize;
      font-weight: 600;
      display: inline-block;
      line-height: 36px;
      cursor: pointer;
      text-align: center;
      text-transform: uppercase;
      min-width: 88px;
      height: 36px;
      margin: 10px 8px;
      padding: 0 8px;
      text-align: center;
      letter-spacing: .5px;
      border-radius: 2px;
      background: #F1F1F1;
      color: #393939;
      transition: background 200ms ease-in-out;
      box-shadow: 0 3.08696px 5.82609px 0 rgba(0, 0, 0, 0.16174), 0 3.65217px 12.91304px 0 rgba(0, 0, 0, 0.12435);
    }

    .c-btn--flat {
      background: transparent;
      margin: 10px 8px;
      min-width: 52px;
    }

    .c-btn:hover {
      background: rgba(153, 153, 153, 0.2);
      color: #393939;
    }

    .c-btn:active {
      box-shadow: 0 9.6087px 10.78261px 0 rgba(0, 0, 0, 0.17217), 0 13.56522px 30.3913px 0 rgba(0, 0, 0, 0.15043);
    }

    .c-btn--flat, .c-btn--flat:hover, .c-btn--flat:active {
      box-shadow: none;
    }
.multiselectclass
{
height: 8.2rem;
    display: block;
    padding:0px;
    background-color: #f5f5f5;
    border: 15px solid #394b53;
    outline: none;
}
.multiselectclass optgroup{
padding:5px;
}
.multiselectclass  option{
    padding: 8px 10px;
    font-size: 13px;
    font-weight: 500;
}
.multiselectclass  option:hover{
background-color:#eeeeee !important;

}
.form-control{
background-color:#f5f5f5 !important;
}
.searchbox{
background-color:#394b53 !important;
margin:0 15px !important;
width: 80% !important;
}
.updownbutton .btn{
 margin-top:15px;
 margin-right:10px;
}
.multiselect-btns{*padding-top:5rem;}
.multiselect-btns .btn{width:100%;height:30px;line-height:30px;margin:2px 0 5px 0;}
.multiselect-btns .btn i{vertical-align: middle;}
.reportdatashowshide {
    color: transparent !important;
}
.reportdatashowshide:hover {
    color: inherit !important;
}
.viewpaddingbtm{
	padding-bottom:1.5rem;
}
.viewpaddingbtm:last-child{
	padding-right: 0px;
}
.viewpaddingbtm:first-child{
	padding-left: 5px;
}
.addformunderheader {
height:0.3rem !important;
}
@media (min-width: 1024px) and (max-width: 1220px) { 
	 .viewnameformError.formError .formErrorContent{max-width:110px;}
	 .multiselect-btns .btn{font-size:0.7rem;padding:0 1rem;}
 }
</style>
<link href="<?php echo base_url();?>css/datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<?php
	$dataset['formtype'] = 'viewcrud';
	$this->load->view('Base/singlemainviewformwithgrid',$dataset);
?>
<div class="large-12 columns addformunderheader">&nbsp;</div>
<div class="large-12 columns scrollbarclass addformcontainer actionbarformcontainer" style="background-color:#eceff1 !important;height:640px !important;">
	<div class="row">&nbsp;</div>
	<form method="POST" name="viewcreateform" class="viewcreateformclearform" action ="" id="viewcreateform">
		<div id="viewsubformspan1" class="hidviewsubform">
			<span id="viewformconditionvalidation" class="validationEngineContainer">
				<div class="large-12 columns viewcleardataform vieweditformvalidation validationEngineContainer">
					<span class="viewformvalidation validationEngineContainer">
						<div class="large-2 columns viewpaddingbtm ">
							<div class="large-12 columns borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #617d8a;height:13.5rem;">
								<div class="large-12 columns headerformcaptionstyle">View Details</div>
								<div class="input-field large-12 columns">
									<input type="text" class="validate[required,funcCall[viewnamecheck],maxSize[25],custom[onlyLetterNumberSp]]" id="viewname" name="viewname"  tabindex="6" data-prompt-position="bottomLeft">
									<label for="viewname">View Name <span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="large-12 columns">
									<div class="large-12 columns end paddingzero">
										<input type="checkbox" id="viewdefault" class="checkboxcls filled-in" name="viewdefault"  tabindex="7" data-hidname='viewdefaultopt' value="No">
										<label for="viewdefault">Default</label>
										<input type="hidden" id="viewdefaultopt" name="viewdefaultopt" value="No">
									</div>
									<div class="large-12 columns end paddingzero">
										<input type="checkbox" id="viewfavourite"  class="checkboxcls filled-in" name="viewfavourite"  tabindex="8" data-hidname='viewpublicopt' value="No">
										<label for="viewfavourite">Public</label>
										<input type="hidden" id="viewpublicopt" name="viewpublicopt" value="No">
									</div>
								</div>
							</div>
						</div>
						   <div class="large-3 medium-5 columns viewpaddingbtm" id="reportcalculationgridwidth">	
								<div class="large-12 columns borderstyle paddingzero" style="background-color: #617d8a;">
								<div class="large-12 columns headerformcaptionstyle">Select Columns</div>
								<select name="from[]" id="viewmaincolumnname" class="multiselectclass" size="8" multiple="multiple" name="viewmaincolumnname">
									
								</select>
								</div>
							</div>
							<div class="large-1 medium-2 columns multiselect-btns viewpaddingbtm">
								<button type="button" id="viewmaincolumnname_rightAll" class="btn btn-block selectcolbtn"><i class="material-icons">fast_forward</i></button>
								<button type="button" id="viewmaincolumnname_rightSelected" class="btn btn-block"><i class="material-icons selectcolbtn">keyboard_arrow_right</i></button>
								<button type="button" id="viewmaincolumnname_leftSelected" class="btn btn-block"><i class="material-icons selectcolbtn">keyboard_arrow_left</i></button>
								<button type="button" id="viewmaincolumnname_leftAll" class="btn btn-block"><i class="material-icons selectcolbtn">fast_rewind</i></button>
								<button type="button" id="viewmaincolumnname_leftAll" data-selectddid = "viewmaincolumnname_to" class="btn btn-block multiselectupdown" value="Up"><i class="material-icons selectcolbtn">keyboard_arrow_up</i></button>
								<button type="button" id="viewmaincolumnname_leftAll" data-selectddid = "viewmaincolumnname_to" class="btn btn-block multiselectupdown" value="Down"><i class="material-icons selectcolbtn">keyboard_arrow_down</i></button>
								<!-- <input type="button" class="btn btn-block multiselectupdown" data-selectddid = "viewmaincolumnname_to" value="Up"> 
								<input type="button" class="btn btn-block multiselectupdown" data-selectddid = "viewmaincolumnname_to" value="Down" style="padding-left:15px;">-->
							</div>
							<div class="large-3 medium-5 columns viewpaddingbtm">
								<div class="large-12 columns borderstyle paddingzero" style="background-color:#617d8a">
									<div class="large-12 columns headerformcaptionstyle" >Selected Columns</div>
									<select name="to[]" id="viewmaincolumnname_to" class="multiselectclass" size="8" multiple="multiple">
									</select>
								</div>
							</div>
							<div class="large-3 columns viewpaddingbtm">
								<div class="large-12 columns borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #617d8a; min-height: 13.4rem;">
									<div class="large-12 columns headerformcaptionstyle">Date Details</div>
									<div class="static-field large-6 columns">
							<label>Date Column</label>
								<select data-placeholder="Select" data-prompt-position="topLeft:14,36" class="chzn-select chosenwidth viewdropdownchange" tabindex="10" name="view_datefield" id="view_datefield">
								<option></option>
								</select> 
							<input type="hidden" name="" id="" value="" />
						</div>
						<div class="static-field large-6 columns">
							<label>Range</label>
								<select data-placeholder="Select" data-prompt-position="topLeft:14,36" class="chzn-select chosenwidth viewdropdownchange" tabindex="10" name="view_range" id="view_range">
								<option></option>
								<option value="custom" data-mode="instant">Custom</option>
								 <optgroup label="Day">	
									<option value="-1 day" data-mode="instant">Yesterday</option>
									<option value="0 day" data-mode="instant">Today</option>
									<option value="1 day" data-mode="instant">Tomorrow</option>
									<option value="-6 day" data-mode="direct">Last 7 Days</option>
									<option value="-29 day" data-mode="direct">Last 30 Days</option>
									<option value="-59 day" data-mode="direct">Last 60 Days</option>
									<option value="-89 day" data-mode="direct">Last 90 Days</option>
									<option value="-119 day" data-mode="direct">Last 120 Days</option>
									<option value="6 day" data-mode="direct">Next 7 Days</option>
									<option value="29 day" data-mode="direct">Next 30 Days</option>
									<option value="59 day" data-mode="direct">Next 60 Days</option>
									<option value="89 day" data-mode="direct">Next 90 Days</option> 
									<option value="119 day" data-mode="direct">Next 120 Days</option>
								 </optgroup>
								<optgroup label="Calendar Week">
									<option value="lastweek" data-mode="switch">Last Week</option>
									<option value="currentweek" data-mode="switch">Current Week</option>
									<option value="nextweek" data-mode="switch">Next Week</option>
								</optgroup>
								<optgroup label="Calendar Month">
									<option value="lastmonth" data-mode="switch">Last Month</option>
									<option value="currentmonth" data-mode="switch">Current Month</option>
									<option value="nextmonth" data-mode="switch">Next Month</option>
									<option value="currentandpreviousmonth" data-mode="switch">Current and Previous Month</option>
									<option value="currentandnextmonth" data-mode="switch">Current and Next Month</option>		
								</optgroup>
								</select> 
							<input type="hidden" name="" id="" value="" />
						</div>
						<div class="input-field large-6 columns viewcondclear viewdatepickerdiv" id="">
							<input type="text" class="fordatepicicon" data-dateformater="<?php echo $appdateformat; ?>" id="view_startdate" name="view_startdate"  tabindex="" >
							<label for="view_startdate">Start Date</label>
						</div>
						<div class="input-field large-6 columns viewcondclear viewdatepickerdiv" id="">
							<input type="text" class="fordatepicicon" data-dateformater="<?php echo $appdateformat; ?>" id="view_enddate" name="view_enddate"  tabindex="" >
							<label for="view_enddate">End Date</label>
						</div>
						<div class="input-field large-6 columns viewcondclear hidedisplay viewdatetimepickerdiv" id="">
							<input type="text" class="fordatepicicon"  id="view_startdatetime" name="view_startdatetime"  tabindex="" >
							<label for="view_startdatetime">Start Date</label>
						</div>
						<div class="input-field large-6 columns viewcondclear hidedisplay viewdatetimepickerdiv" id="">
							<input type="text" class="fordatepicicon"  id="view_enddatetime" name="view_enddatetime"  tabindex="" >
							<label for="view_enddatetime">End Date</label>
						</div>
					</div>
				</div>
					</span>
				</div>
				<div class="row">&nbsp;</div>
				<div class="large-12 columns">
					<div class="large-3 columns viewpaddingbtm footercleardataform">
						<div class="large-12 columns borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;background: #617d8a">
							<div class="large-12 columns headerformcaptionstyle">Conditions</div>
							<div class="static-field large-12 columns">
								<label>Field Name <span class="mandatoryfildclass">*</span></label>
								<select data-placeholder="Select" data-prompt-position="topLeft:14,36" class="validate[required] chzn-select chosenwidth viewdropdownchange" tabindex="10" name="viewcondcolumn" id="viewcondcolumn">
									<option></option>
								</select>  
								<input type="hidden" name="condcolumnid" id="condcolumnid" value="" />
							</div>
							<div class="static-field large-6 columns hidedisplay">
								<label>Aggregate Method<span class="mandatoryfildclass">*</span></label>
								<select id="viewaggregate" class="validate[required] chzn-select chosenwidth dropdownchange select2-offscreen whitecss forsucesscls" name="viewaggregate"  data-placeholder="Select" data-prompt-position="bottomLeft:14,36" title="">
									<option value="ACTUAL">Actual Values</option>
								</select>
							</div>
							<div class="static-field large-12 columns viewcondclear">
								<label>Condition <span class="mandatoryfildclass">*</span></label>
								<select data-placeholder="Select" data-prompt-position="topLeft:14,36" class="validate[required] chzn-select chosenwidth " tabindex="11" name="viewcondition" id="viewcondition">
									<option></option>
									</select> 
							</div>
							<div class="input-field large-12 columns viewcondclear" id="viewcondvaluedivhid">
								<input type="text" class="validate[required,maxSize[100]]" id="viewcondvalue" name="viewcondvalue"  tabindex="12" >
								<label for="viewcondvalue">Value <span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="static-field large-12 columns viewcondclear" id="viewddcondvaluedivhid">
								<label>Value<span class="mandatoryfildclass">*</span></label>
								<select data-placeholder="Select" data-prompt-position="topLeft:14,36" class="validate[required] chzn-select chosenwidth valdropdownchange" tabindex="13" name="viewddcondvalue" id="viewddcondvalue"  multiple="multiple">
									<option></option>
								</select>  
							</div>
							<div class="input-field large-12 columns viewcondclear" id="viewdatecondvaluedivhid">
								<input type="text" class="validate[required]" data-dateformater="<?php echo $appdateformat; ?>" id="viewdatecondvalue" name="viewdatecondvalue" tabindex="12" >
								<label for="viewdatecondvalue">Value <span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="large-12 columns viewcondclear">
								<input type="hidden" class="" id="finalviewcondvalue" name="finalviewcondvalue"  tabindex="12" >
								<input type="hidden" class="" id="finalviewcondvaluename" name="finalviewcondvaluename"  tabindex="12" >
							</div>
							<div class="static-field large-12 columns viewcondclear" id="viewandorconddivhid">
								<label>AND/OR <span class="mandatoryfildclass condmandclass">*</span></label>
								<select data-placeholder="Select" data-prompt-position="topLeft:14,36" class="validate[required] chzn-select chosenwidth" tabindex="13" name="viewandorcond" id="viewandorcond">
									<option></option>
									<option value="AND">AND</option>
									<option value="OR">OR</option>
								</select>  
							</div>
							<div class="large-12 columns">&nbsp;</div>
							<div class="large-12 columns centertext">
								<input type="button" class="btn" id="viewaddcondsubbtn" Value="Submit" name="viewaddcondsubbtn"  tabindex="14" >
							</div>
							<div class="large-12 columns">&nbsp;</div>
						</div>
					</div>
					<div class="large-9 columns viewgridcolorstyle paddingzero borderstyle">
						<div class="large-12 columns headerformcaptionstyle " style="padding: 0.1rem 0 0;">
							<div class="large-6 medium-6 small-6 columns" style="text-align:left;display:inline-block;line-height: 34px;white-space: nowrap;overflow:hidden !important;text-overflow: ellipsis;">
								Conditions to Apply
							</div>
							<div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height: 1.8;">
								<span id="conddeleteicon" title="Delete" class="material-icons">delete</span>
							</div>
						</div>
						<?php
							$device = $this->Basefunctions->deviceinfo();
							if($device=='phone') {
								echo '<div class="large-12 columns paddingzero forgetinggridname" id="viewcreateconditiongridwidth"><div class="desktop row-content inner-gridcontent" id="viewcreateconditiongrid" style="height:318px;top:0px;">
									<!-- Table header content , data rows & pagination for mobile-->
								</div>
								<footer class="inner-gridfooter footercontainer" id="viewcreateconditiongridfooter">
									<!-- Footer & Pagination content -->
								</footer></div>';
							} else {
								echo '<div class="large-12 columns forgetinggridname" id="viewcreateconditiongridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="viewcreateconditiongrid" style="max-width:2000px; height:318px;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
								<!--<div class="inner-gridfooter footer-content footercontainer" id="viewcreateconditiongridfooter">
									 Footer & Pagination content 
								</div>--></div>';
							}
						?>
					</div>
				</div>
				<div class="row">&nbsp;</div>
			</span>
		</div>
		<input type="hidden" name="viewcreatemoduleid" id="viewcreatemoduleid" value="<?php echo implode(',',$moduleids);?>" />
		<input type="hidden" name="viewcolstatus" id="viewcolstatus" value="2" />
		<input type="hidden" name="footercolstatus" id="footercolstatus" value="2" />
		<input type="hidden" name="viewcondrowid" id="viewcondrowid" value="0" />
		<input type="hidden" name="footerid" id="footerid" value="0" />
		<input type="hidden" name="conrowcolids" id="conrowcolids" value="0" />
		<input type="hidden" name="viewdduserviewid" id="viewdduserviewid" value="" />
		<input type="hidden" name="viewactiontype" id="viewactiontype" value="0" />
		<input type="hidden" name="defviewfiledids" id="defviewfiledids" value ="" />
		<input type="hidden" name="footerrowcolids" id="footerrowcolids" value="0" />
	</form>
</div>
<script src="<?php echo base_url();?>js/Base/views.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/Reportsview/multiselect.js" type="text/javascript"></script>
<!--<script src="<?php //echo base_url();?>js/plugins/datepicker.standalone.js"></script>
<script src="<?php //echo base_url();?>js/plugins/moment.min.js"></script>-->
<script type="text/javascript">
$(document).ready(function($) {
	$("#viewmaincolumnname,#viewmaincolumnname_to").select2('destroy');	
    $('#viewmaincolumnname').multiselect({
        search: {
            left: '<input type="text" name="q" class="form-control large-12 columns searchbox" placeholder="Search..." />',
            right: '<input type="text" name="q" class="form-control large-12 columns searchbox" placeholder="Search..." />',
        },
		 keepRenderingSort: true
    });
	$('#reportgroupby1').multiselect({
        search: {
            left: '<input type="text" name="q1" class="form-control large-12 columns searchbox" placeholder="Search..." />',
            right: '<input type="text" name="q1" class="form-control large-12 columns searchbox" placeholder="Search..." />',
        },
		 keepRenderingSort: true
    });
	$('.multiselectupdown').click(function(){
        var selectddid = $(this).data("selectddid");
		var $op = $('#'+selectddid+' option:selected');
        $this = $(this);
        if($op.length){
            ($this.val() == 'Up') ? 
                $op.first().prev().before($op) : 
                $op.last().next().after($op);
        }
    });
});
</script>