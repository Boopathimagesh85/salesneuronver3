<?php
	//instead of this ( This will replace (this->) pointer )
	$CI =& get_instance();
	//Menu list fetch function
	$modulelist = $CI->Basefunctions->usermenugeneration();
	$userdetails = $this->Basefunctions->simpledropdownwithcond('employeeid','employeename,lastname,emailid','employee','employeeid',$this->Basefunctions->logemployeeid);
	foreach($userdetails as $user) {
		$empname = $user->employeename;
		$emplastname = $user->lastname;
		$empemailid = $user->emailid;
	}
?>
<ul id="slide-out" class="side-nav fixed" style="padding-top:0px;left: -100px;">
	<li id="homeicon" class="lm-title-bar">
	    <span class="lm-title">Sales Neuron</span>
		<span class="usernamedisplayleftmenu"><?php echo $empname.' '.$emplastname; ?></span>
	</li>
  	<div class="topmenu-panel" style="height:550px;">
       <li>
			<ul class="main-menu">
			<?php 
				$menulist = $this->Basefunctions->modulelistgenerate($modulelist);
				echo $menulist['modulelistdata'];
				$noofcat = $menulist['numofcategory'];
			?>
			</ul>
		</li>	 
	</div>
</ul>