<!-- Notification overlay -->
<div class="overlay" id="notificationdivoverlay">		
	<div class="row show-for-medium-up">&nbsp;</div>
	<div class="row show-for-medium-up">&nbsp;</div>
	<div class="row show-for-medium-up">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="notification-alert">
	<div class="row">&nbsp;</div>
		<div class="large-12 medium-7 large-centered medium-centered columns borderstyle notiprofilewidgetheight">
			<div class="notification-card">
				<div class="notification-header">
					<div class="notification-title">Notifications</div>
					<div class="notification-status">
						<span id="activefeeds" class="notificationstatusactive">New</span><span style="color:#ffffff">|</span>
						<span id="mylist" class="">My</span><span style="color:#ffffff">|</span>
						<span id="alllist" data-activates="notificationlist" class="notidropdown-button" style="padding-right:12px">All</span>
						<ul id="notificationlist" class="dropdown-content">
							<li id="all" class=""><a>All</a></li>
							<li id="today" class=""><a>Today</a></li>
							<li id="yesterday" class=""><a>Yesterday</a></li>
							<li id="thisweek" class=""><a>Last Week</a></li>
							<li id="thismonth" class=""><a>Current Month</a></li>
							<li id="lastmonth" class=""><a>Last Month</a></li>
						</ul>
					</div>
				</div>
				<ul id="feedsliveid" class="notification-content"></ul>
			</div>
			<input type="hidden" name="notificationdatatype" id="notificationdatatype" value=""/>
		</div>
		<div class="row">&nbsp;</div>
	</div>
</div>
<script>
$(document).ready(function(){
	$("#activefeeds").click(function() {
		$(this).addClass('notificationstatusactive');
		$("#mylist").removeClass('notificationstatusactive');
		$("#alllist").removeClass('notificationstatusactive');
		$('#notificationdatatype').val('newactivefeeds');
		$("#alllist").text('All');
		var datatype = 'newactivefeeds';
		notiicationdatalod(datatype);
	});
	$("#all").click(function() {
		$(this).addClass('notificationstatusactive');
		$("#activefeeds").removeClass('notificationstatusactive');
		$("#mylist").removeClass('notificationstatusactive');
		$("#alllist").addClass('notificationstatusactive');
		$('#notificationdatatype').val('all');
		$("#alllist").text('All');
		var datatype = 'all';
		notiicationdatalod(datatype);
	});	
	$("#mylist").click(function() {
		$("#mylist").addClass('notificationstatusactive');
		$("#alllist").removeClass('notificationstatusactive');
		$("#activefeeds").removeClass('notificationstatusactive');
		$('#notificationdatatype').val('my');
		var datatype = 'my';
		notiicationdatalod(datatype)
	});	 
	$("#today").click(function() {
		$("#mylist").removeClass('notificationstatusactive');
		$("#alllist").addClass('notificationstatusactive');
		$("#activefeeds").removeClass('notificationstatusactive');
		$('#notificationdatatype').val('today');
		$("#alllist").text('Today');
		var datatype = 'today';
		notiicationdatalod(datatype)
	});
	$("#yesterday").click(function() {
		$("#mylist").removeClass('notificationstatusactive');
		$("#alllist").addClass('notificationstatusactive');
		$("#activefeeds").removeClass('notificationstatusactive');
		$('#notificationdatatype').val('yesterday');
		var datatype = 'yesterday';	
		$("#alllist").text('Yesteday');
		notiicationdatalod(datatype)
	});
	$("#thisweek").click(function() {
		$("#mylist").removeClass('notificationstatusactive');
		$("#alllist").addClass('notificationstatusactive');
		$("#activefeeds").removeClass('notificationstatusactive');
		$('#notificationdatatype').val('thisweek');
		var datatype = 'thisweek';	
		$("#alllist").text('Last week');
		notiicationdatalod(datatype)
	});
	$("#lastmonth").click(function() {
		$("#mylist").removeClass('notificationstatusactive');
		$("#alllist").addClass('notificationstatusactive');
		$("#activefeeds").removeClass('notificationstatusactive');
		$('#notificationdatatype').val('lastmonth');
		$("#alllist").text('Last Month');
		var datatype = 'lastmonth';
		notiicationdatalod(datatype)
	});
	$("#thismonth").click(function() {
		$("#mylist").removeClass('notificationstatusactive');
		$("#alllist").addClass('notificationstatusactive');
		$("#activefeeds").removeClass('notificationstatusactive');
		$('#notificationdatatype').val('thismonth');
		$("#alllist").text('Current Month');
		var datatype = 'thismonth';	
		notiicationdatalod(datatype)
	});
	$('.notidropdown-button').dropdown({
		inDuration: 300,
		outDuration: 225,
		constrain_width: false, // Does not change width of dropdown to that of the activator
		hover: false, // Activate on hover
		gutter: 0, // Spacing from edge
		belowOrigin: true, // Displays dropdown below the button
		alignment: 'right' // Displays dropdown with edge aligned to the left of button
	});
});
function notiicationdatalod(datatype) {
	$.ajax({
		url:base_url+"Home/newgetnotificationdata",
		data: {datatype:datatype,lastnode:0,status:1},			
		type:"POST",
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			var loop=data.length;			
			feedcontent(loop,'feedsliveid','feedsliveidclass',data);
		}
	});
}
</script>