<div class="large-12 columns paddingzero formheader">
<?php
$device = $this->Basefunctions->deviceinfo();
if(isset($formtype)) {
	$formtype = $formtype;
} else {
	$formtype = 'addform';
}
		if($formtype == 'multipleaddform'){
			echo '<div class="large-12 columns multiheadercaptionstyle headerradius paddingzero">';
			$this->load->view('Base/mainviewheader');
		} else {
			echo '<div class="large-12 columns headercaptionstyle headerradius paddingzero">';
			echo '<div class="large-6 medium-6 small-6 columns headercaptionleft">
				<ul class="module-view">
					<li>
						<span class="gridcaptionpos" style="position: relative;left: -10px;"><span>'.$gridtitle.'</span></span>
					</li>
				</ul>
			</div>';
		}
		if($formtype == 'addform') {
			$this->load->view('Base/formheadericons');
		} else if($formtype == 'stepper') {
			echo '<div class="large-6 medium-6 small-6 columns righttext mainaction">';
				if($moduelid == 36) {
					echo '<span class="addbtnclass icon-box" id="rulealertsave" >
							<input id="" name="" tabindex="" value="Save" class="alertbtnyes  ffield" type="button">
							
						</span>
						<span id="rulealertupdate" class="updatebtnclass icon-box hidedisplay">
							<input id="" name="" tabindex="" value="Save" class="alertbtnyes  ffield" type="button">
							
						</span>';
				}
				$this->load->view('Base/usermenugenerate');
			echo '</div>';
		} else if($formtype == 'multipleaddform') {
			
		} else if($formtype == 'report') {
			echo '<div class="large-6 medium-6 small-12 columns righttext addformheadericonstyle addformaction">
			<span id="generatereport" class="icon-box" title="Save">
						<input id name value="Save" class="alertbtnyes  ffield" type="button">
						</span>
					<span id="closeoverlay" class="icon-box" title="Close">
						<input id name value="Close" class="alertbtnno summaryicon ffield" type="button">
						</span>';
	
		
		echo '</div>';
		}
	echo '</div>';
?>
<?php if($formtype == 'addform') { ?> <!-- add form mobile tab group creation -->
	<?php  if($device!='phone') { ?>
		<div class="large-12 columns addformunderheader">&nbsp;</div>
		<div class="large-12 columns tabgroupstyle desktoptabgroup">
			<?php
				$ulattr = array("class"=>"tabs");
				$uladdinfo = "data-tab";
				$tabgrpattr = array("class"=>"tab-title sidebaricons");
				$tabstatus = "active";
				$dataname = "subform";
				echo tabgroupgenerate($ulattr,$uladdinfo,$tabgrpattr,$tabstatus,$dataname,$modtabgrp);
				//Value Getting for Tablet and mobile Dropdown
				$dropdowntabval = array();
				$m=1;
				foreach($modtabgrp as $value) {
					$dropdowntabval[$m]=$value['tabgrpname'];
					$m++;
				}
			?>
		</div>
	<?php } else { ?>
		<div class="large-12 columns tabgroupstyle mobiletabgroup">
			<?php
				$ulattr = array("class"=>"tabs");
				$uladdinfo = "data-tab";
				$tabgrpattr = array("class"=>"tab-title sidebaricons");
				$tabstatus = "active";
				$dataname = "subform";
				echo mobiletabgroupgenerate($ulattr,$uladdinfo,$tabgrpattr,$tabstatus,$dataname,$modtabgrp);
				//Value Getting for Tablet and mobile Dropdown
				$dropdowntabval = array();
				$m=1;
				foreach($modtabgrp as $value) {
					$dropdowntabval[$m]=$value['tabgrpname'];
					$m++;
				}
			?>
		</div>
	<?php } ?>
<?php } else if($formtype == 'stepper') { ?> <!-- Stepper tab group creation -->
	<div class="large-12 columns steppercontainer">
	<div class="mdl-card mdl-shadow--2dp large-centered medium-centered">
		<div class="mdl-card__supporting-text">
			<div class="mdl-stepper-horizontal-alternative large-12 columns">
				<?php
					$ulattr = array("class"=>"tabs");
					$uladdinfo = "data-tab";
					$tabgrpattr = array("class"=>"tab-title sidebariconstab activetab");
					$tabstatus = "active";
					$dataname = "subform";
					echo steppertabgroupgenerate($ulattr,$uladdinfo,$tabgrpattr,$tabstatus,$dataname,$modtabgrp);
				?>
			</div>
		</div>
	</div>
	</div>
<?php } else if($formtype == 'multipleaddform') { ?>
	<!--  Tab Group generation -->
	<?php  if($device!='phone') {?>
		<div class="large-12 columns addformunderheader">&nbsp;</div>
		<div class="large-12 columns tabgroupstyle desktoptabgroup" style="height: 32px;">
			<?php
				$uldata = '<ul class="multitabs " data-tab style="position:relative;left:-10px; top:-8px;">'.PHP_EOL;
				$uladdinfo = "data-tab";
				$tabgrpattr = array("class"=>"dbtab-title dbsidebaricons");
				$tabstatus = "active";
				$dataname = "dbsubform";
				foreach($tabgrpattr as $key => $values) {
					$class = $key;
					$val = $values;
				}
				$m = 1;
				$litab = "";
				$dropdowntabval = array();
				foreach($moddashboardtabgrp as $value) {
					if($m == 1) {
						$litab .= '<li '.$class.'="'.$val.' '.$tabstatus.' ftab" data-'.$dataname.'="'.$m.'" id="tab'.$m.'"><span class="waves-effect waves-ripple waves-light">'.$value['tabgrpname'].'</span></li>'.PHP_EOL;
					} else {
						$litab .= '<li '.$class.'="'.$val.'" data-'.$dataname.'="'.$m.'" id="tab'.$m.'"><span class="waves-effect waves-ripple waves-light" style="left:10px;position:relative;">'.$value['tabgrpname'].'</span></li>'.PHP_EOL;
					}
					$dropdowntabval[$m]=$value['tabgrpname'];
					$m++;
				}
				$litab .= '<li class="morelabel dbtab-title dbsidebaricons hidedisplay" data-dbsubform="x"><span class="waves-effect waves-ripple waves-light">More</span></li>';
				$litab .= '<li id="moretab" class="tab-title moretab hidedisplay"><span class="dropdown-button" data-activates="mastertabgroupmoredropdown">More<i class="material-icons">details</i></span><ul id="mastertabgroupmoredropdown" class="dropdown-content"></ul></li>';
				$ulc = close("ul");
				if(isset($multimoduleid)) {
					$multimoduleid = $multimoduleid;
				} else {
					$multimoduleid = '1';
				}
				$multiaction = '';
				if($multimoduleid != 1) {
					$mmodid = explode(',',$multimoduleid);
					$n=1;
					$size = 10;
					$value = array();
					$des = array();
					$title = array();
					foreach($mmodid as $modid) {
						$actionassign = $this->Basefunctions->actionassign($modid);
						$nl = $this->Basefunctions->modulelinkfetch('module','modulelink','moduleid',$modid);
						foreach($actionassign as $key){
							$value[$key->toolbarnameid] = $key->toolbarname;
							$des[] = $key->description;
							$title[] = $key->toolbartitle;
						}
						$multiaction .=  multiactionmenu($value,$size,$des,$title,strtolower($nl),'icon24 ',$n);
						$n++;
					}
				}
				
				$reult = $uldata.$litab.$ulc.$multiaction;
				echo $reult;
			?>
		</div>
	<?php } else {?>
		<div class="large-12 columns tabgroupstyle mobiletabgroup">
			<?php
				$uldata = '<ul class="tabs" data-tab>'.PHP_EOL;
				$uladdinfo = "data-tab";
				$tabgrpattr = array("class"=>"dbtab-title dbsidebaricons");
				$tabstatus = "active";
				$dataname = "dbsubform";
				foreach($tabgrpattr as $key => $values) {
					$class = $key;
					$val = $values;
				}
				$m = 1;
				$litab = "";
				$dropdowntabval = array();
				foreach($moddashboardtabgrp as $value) {
					if($m == 1) {
						$litab .= '<li '.$class.'="'.$val.' '.$tabstatus.' ftab" data-'.$dataname.'="'.$m.'" id="tab'.$m.'"><span class="waves-effect waves-ripple waves-light">'.$value['tabgrpname'].'</span></li>'.PHP_EOL;
					} else {
						$litab .= '<li '.$class.'="'.$val.'" data-'.$dataname.'="'.$m.'" id="tab'.$m.'"><span class="waves-effect waves-ripple waves-light">'.$value['tabgrpname'].'</span></li>'.PHP_EOL;
					}
					$dropdowntabval[$m]=$value['tabgrpname'];
					$m++;
				}
				$ulc = close("ul");
				$reult = $uldata.$litab.$ulc;
				echo $reult;
			?>
		</div>
	<?php }?>
<?php } else { ?>
	<?php  if($device!='phone') { ?>
		<div class="large-12 columns addformunderheader">&nbsp;</div>
		<div class="large-12 columns tabgroupstyle desktoptabgroup">
			<?php
				$ulattr = array("class"=>"tabs");
				$uladdinfo = "data-tab";
				$tabgrpattr = array("class"=>"tab-title sidebaricons");
				$tabstatus = "active";
				$dataname = "subform";
				echo tabgroupgenerate($ulattr,$uladdinfo,$tabgrpattr,$tabstatus,$dataname,$modtabgrp);
				//Value Getting for Tablet and mobile Dropdown
				$dropdowntabval = array();
				$m=1;
				foreach($modtabgrp as $value) {
					$dropdowntabval[$m]=$value['tabgrpname'];
					$m++;
				}
			?>
		</div>
	<?php } else { ?>
		<div class="large-12 columns tabgroupstyle mobiletabgroup">
			<?php
				$ulattr = array("class"=>"tabs");
				$uladdinfo = "data-tab";
				$tabgrpattr = array("class"=>"tab-title sidebaricons");
				$tabstatus = "active";
				$dataname = "subform";
				echo mobiletabgroupgenerate($ulattr,$uladdinfo,$tabgrpattr,$tabstatus,$dataname,$modtabgrp);
				//Value Getting for Tablet and mobile Dropdown
				$dropdowntabval = array();
				$m=1;
				foreach($modtabgrp as $value) {
					$dropdowntabval[$m]=$value['tabgrpname'];
					$m++;
				}
			?>
		</div>
	<?php } ?>
<?php }?>
</div>