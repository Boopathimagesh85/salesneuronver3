<!--Selection Overlay-->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="dashboardselectoverlay">
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-3 medium-6 small-10 large-centered medium-centered small-centered columns" style="">
			<span id="metaloverlaywizard" class=""> 
				<div class="row">&nbsp;</div>
				<div class="row" style="background:#465a63">
					<div class="large-12 columns headerformcaptionstyle">
						<div class="large-10 medium-10 small-10 columns" style="background:none">Dashboard Selection</div>
						<div class="large-2 medium-2 small-2 columns" id="dashboardselectioncloseid" style="text-align:right;cursor:pointer;"><i class="material-icons" style="cursor:pointer" title="Close">cancel</i></div>
					</div>
					<div class="large-12 columns">
						<label>Select Dashboard</label>
						<select class="chzn-select" id="dashboardviewid" name="dashboardviewid">
							<?php
							$prev = ' ';
							$selected = "";
							$userid = $this->Basefunctions->logemployeeid;
							foreach($dashboradviewdata as $key):
								$cur = $key->dashboardfolderid;
								$a ="";
								if($prev != $cur ) {
									if($prev != " ") {
										echo '</optgroup>';
									}
									echo '<optgroup  label="'.str_replace(' ','',$key->dashboardfoldername).'" class="'.str_replace(' ','',$key->dashboardfoldername).'">';
									$prev = $key->dashboardfolderid;
									$a = "pclass";
								}
								if($userid == $key->createuserid && $key->setdefault) {
									$selected = "selected=selected";
								}
								?>
								<option value="<?php echo $key->dashboardid;?>" <?php echo $selected; ?> ><?php echo $key->dashboardname;?></option>
							<?php endforeach;?>
						</select>
					</div>
				</div>
				<div class="row" style="background:#465a63">&nbsp;</div>
			</span>
		</div>
	</div>
</div>