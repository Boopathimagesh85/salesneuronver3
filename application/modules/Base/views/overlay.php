<!-- Get Base URL -->
<?php
	$name='';
	$email='';
	$dataset=$this->db->select('employeeid,employeename,lastname,emailid,salutation.salutationname,mobilenumber')->from('employee')->join('salutation','salutation.salutationid=employee.salutationid')->where('employee.employeeid',$this->Basefunctions->logemployeeid)->get();
	foreach($dataset->result() as $row) {
		$name=$row->salutationname.$row->employeename.$row->lastname;
		$email=$row->emailid;
		$mobile = $row->mobilenumber;
	}
?>
	<!--Single alert-->
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay alertsoverlay overlayalerts" id="alerts">	
			<div class="row">&nbsp;</div>
			<div class="row sectiontoppadding"></div>
			<div class="overlaybackground">
				<div class="alert-panel">
					<div class="alertmessagearea">
					<div class="row">&nbsp;</div>
					<div class="alert-message alertinputstyle">Please select a row</div>
					</div>
					<div class="alertbuttonarea">
						<span class="firsttab" tabindex="1000"></span>
						<input type="button" id="alertsclose" name="" value="Ok" class="alertbtnyes  alertsoverlaybtn" >
						<span class="lasttab" tabindex="1003"></span>
					</div>
					<div class="row">&nbsp;</div>
					<div class="row">&nbsp;</div>
				</div>
			</div>
		</div>
	</div>
	<!--Double Alert Overlay-->
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay" id="alertsdouble" style=" z-index:45;">	
			<div class="row">&nbsp;</div>
			<div class="row sectiontoppadding"></div>
			<div class="overlaybackground">
				<div class="alert-panel">
					<div class="alertmessagearea">
					<div class="row">&nbsp;</div>
					<div class="alert-message alertinputstyledouble">Please select a row</div>
					</div>
					<div class="alertbuttonarea">
						<span class="firsttab" tabindex="1000"></span>
						<input type="button" id="alertsdoubleclose" name="" value="Ok" class="alertbtnyes  alertsdoubleoverlaybtn">
						<span class="lasttab" tabindex="1003"></span>
					</div>
					<div class="row">&nbsp;</div>
					<div class="row">&nbsp;</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Verify this alert used are not -->
	<div class="overlay alertsoverlay overlayalerts" id="alertsyn" style="display:none;">	
		<div class="swMain alertspos">
			<div class="alertsinputpos" >
				<input type="text" value="Delete the data ?" class="alertinputstyle">
				<span class="disablealerttxt"></span>
			</div>
			<div class="alertsbtnposyes" >					
				<input type="button" id="alertscloseyes" name="" value="Yes" class="btn formbuttonsalert">				
			</div>	
			<div class="alertsbtnposno" >					
				<input type="button" id="alertscloseno" name="" value="No" class="btn formbuttonsalert">				
			</div>
		</div>	
	</div>
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay alertsoverlay overlayalerts" id="alertscloseyn">	
			<div class="row">&nbsp;</div>
			<div class="row sectiontoppadding"></div>
			<div class="overlaybackground">
				<div class="alert-panel">
					<div class="alertmessagearea">
					<div class="row">&nbsp;</div>
					<div class="alert-message alertinputstyle">Delete this data?</div>
					</div>
					<div class="alertbuttonarea">
						<span class="firsttab" tabindex="1000"></span>
						<input type="button" id="alertsfcloseyes" name="" tabindex="1001" value="Yes" class="alertbtnyes  ffield" >
						<input type="button" id="alertsfcloseno" name="" value="No" tabindex="1002" class="flloop alertbtnno alertsoverlaybtn" >
						<span class="lasttab" tabindex="1003"></span>
					</div>
					<div class="row">&nbsp;</div>
				</div>
			</div>
		</div>
	</div>
	
	<input type="hidden" name="trackdate" id="trackdate"/>	
	<button id="trackdateset" name="trackdateset" type="button" style="display:none;"></button>
	<!--For Validation-->
	<div class="overlay alertsoverlay overlayalerts" id="validationalerts" style="display:none;">	
		<div class="swMain alertspos">
			<div class="alertsinputpos" >
				<input type="text" value="Please Select The row"  class="alertinputstyle">
				<span class="disablealerttxt"></span>
			</div>
			<div class="alertsbtnpos" >					
				<input type="button" id="validationalertsclose" name="" value="Ok" class="btn formbuttonsalert alertsoverlaybtn">				
			</div>	
		</div>	
	</div>	
	<!--For Ajax Call Processing-->
	<div class="overlay overlayalerts" id="processingsrc" style="display:none;">	
		<div class="swMain alertspos">
			<div class="alertsinputpos" >
				<input type="text" value="Process Going On ....." disabled class="processsubmitclass">
			</div>
		</div>	 
	</div>
	<!-- Template Print Overlay  Overlay-->
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="templatepdfoverlay" style="overflow-y: scroll;overflow-x: hidden;">		
			<div class="row sectiontoppadding">&nbsp;</div>
			<div class="overlaybackground">
				<div class="alert-panel" style="background-color:#465a63">
					<form method="POST" name="pdfviewoverlayform"  id="pdfviewoverlayform" action="" enctype="" class="clearformtaxoverlay">
						<span id="pdfviewoverlayformvalidation" class="validationEngineContainer"> 
							<span id="pdfsaveoverlayformvalidation" class="validationEngineContainer"> 
							<div class="alertmessagearea">
							<div class="alert-title" style="text-align:left;">PDF </div>
							<span class="firsttab" tabindex="999"></span>
							<div class="large-12">
								<label style="text-align:left;">Select Template<span class="mandatoryfildclass">*</span></label>
								<select data-placeholder="Select" data-prompt-position="topLeft:14,36" class="chzn-select dropdownchange ffieldd validate[required]" tabindex="1000" name="pdftemplateprview" id="pdftemplateprview">
								</select>
							</div>
							<div class="input-field large-12 medium-12 small-12 hidedisplay" id="dateofstockdivhid">
								<label for="dateofstock" class="">Date</label>
								<input name="dateofstock" id="dateofstock" class="fordatepicicon" readonly="readonly" tabindex="147"  data-prompt-position="bottomLeft" data-dateformater="dd-mm-yy" style="" type="text">
							</div>
							<div class="input-field large-12 medium-12 small-12 hidedisplay" id="dateofstocktodivhid">
									<label for="dateofstockto" class="">To Date</label>
									<input name="dateofstockto" id="dateofstockto" class="fordatepicicon" readonly="readonly" tabindex="147"  data-prompt-position="bottomLeft" data-dateformater="dd-mm-yy" style="" type="text">
							</div>
							</div>
							<input type="hidden" id="defaultpdftemplateid" name="defaultpdftemplateid" value="">
							<div class="alertbuttonarea">
								<input type="button" class="alertbtnyes" value="Preview" name="pdffilepreviewintab" tabindex="1001" id="pdffilepreviewintab">
								<input type="button" class="flloop alertbtnno" value="Cancel" name="pdfpreviewcancel" tabindex="1002" id="pdfpreviewcancel">
								<span class="lasttab" tabindex="1003"></span>
							</div>
							<!-- hidden fields -->
							
							</span>
						</span>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- Tag Template Print Overlay  Overlay-->
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="tagtemplateoverlay" style="overflow-y: scroll;overflow-x: hidden;">		
			<div class="row sectiontoppadding">&nbsp;</div>
			<div class="overlaybackground">
				<div class="alert-panel" style="background-color:#465a63">
					<form method="POST" name="tagtemplateoverlayform"  id="tagtemplateoverlayform" action="" enctype="" class="clearformtaxoverlay">
						<span id="tagtemplateoverlayformvalidation" class="validationEngineContainer"> 
							<span id="tagtemplatesaveoverlayformvalidation" class="validationEngineContainer"> 
							<div class="alertmessagearea">
							<div class="alert-title">Tag Template</div>
							<span class="firsttab" tabindex="999"></span>
							<div class="large-12">
								<label>Select Template<span class="mandatoryfildclass">*</span></label>
								<select data-placeholder="Select" data-prompt-position="topLeft:14,36" class="chzn-select dropdownchange ffieldd validate[required]" tabindex="1000" name="tagtemplateprview" id="tagtemplateprview">
								</select>
							</div>
							<div class="input-field large-12 medium-12 small-12 hidedisplay" id="dateofstockdivhid">
								<label for="dateofstock" class="">Date</label>
								<input name="dateofstock" id="dateofstock" class="fordatepicicon" readonly="readonly" tabindex="147"  data-prompt-position="bottomLeft" data-dateformater="dd-mm-yy" style="" type="text">
							</div>
							<div class="input-field large-12 medium-12 small-12 hidedisplay" id="dateofstocktodivhid">
									<label for="dateofstockto" class="">To Date</label>
									<input name="dateofstockto" id="dateofstockto" class="fordatepicicon" readonly="readonly" tabindex="147"  data-prompt-position="bottomLeft" data-dateformater="dd-mm-yy" style="" type="text">
							</div>
							</div>
							<div class="alertbuttonarea">
								<input type="button" class="alertbtn" value="Submit" name="tagtempprintinprinter" tabindex="1001" id="tagtempprintinprinter">
								<input type="button" class="flloop alertbtn" value="Cancel" name="tagtempprintcancel" tabindex="1002" id="tagtempprintcancel">
								<span class="lasttab" tabindex="1003"></span>
							</div>
							<!-- hidden fields -->
							
							</span>
						</span>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- Check and remove this below  Print template overlay @Maddy-->
	<!-- Template Print Overlay  Overlay-->
	<!-- <div class="large-12 columns" style="position:absolute;">
		<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="templateprintoverlay" style="overflow-y: scroll;overflow-x: hidden;">		
			<div class="row sectiontoppadding">&nbsp;</div>
			<div class="large-4 medium-6 large-centered medium-centered columns" >
				<form method="POST" name=""  id="" action="" enctype="">
					<span id="" class="validationEngineContainer"> 
						<div class="row">&nbsp;</div>
						<div class="row" style="background:#f5f5f5">
							<div class="large-12 columns headerformcaptionstyle">
								<div class="small-10 columns" style="background:none">Print</div>
								<div class="small-1 columns" id="printclose" style="text-align:right;cursor:pointer;background:none">X</div>
							</div>
							<div class="large-12 columns">             
								<label>Select Template</label>
								<select data-placeholder="Select" data-prompt-position="topRight:0,35" class="chzn-select dropdownchange ffieldd" tabindex="2" name="printtemplatepreview" id="printtemplatepreview">
								</select>                  
							</div>
							<div class="large-12 columns">&nbsp;</div>
							<div class="large-12 columns">&nbsp;</div>
						</div>
						<div style="background:#f5f5f5" class="row">
							<div class="small-4 medium-4 large-4 columns">
								<input type="button" class="btn formbuttonsalert" value="Preview" name="" id="">	
							</div>
							<div class="small-4 medium-4 large-4 columns">
								<input type="button" class="btn formbuttonsalert" value="Print" name="" id="">	
							</div>
	  						<div class="small-4 medium-4 large-4 columns">
								<input type="button" class="btn formbuttonsalert" value="Cancel" name="printpreviewcancel" id="printpreviewcancel">	
							</div>
							<div class="medium-4 large-4 columns">&nbsp;</div>
							<div class="large-12 columns">&nbsp;</div>
						</div>
	
						<div class="row">&nbsp;</div>
						 
						<input type="hidden" name="taxcatid" id="taxcatid" />
					</span>
				</form>
			</div>
		</div>
	</div> -->
	<!--- process overlay-->
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay" id="processoverlay" style="overflow-y: hidden;overflow-x: hidden;z-index:100;">		
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div><div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="large-2 columns end large-centered">
				<div class="large-12 columns card-panel" style="text-align: left; background: #fff;padding-bottom: 0px; ">
					<div class="small-12 large-12 medium-12 columns paddingzero">
					<div class="row">&nbsp;</div>
						<span style="display:inline-block;height:60px;vertical-align:middle;text-align: center;width: 100%;"><i class="material-icons">cached</i>&nbsp; <span style="top:-8px;position:relative;"> Processing...</span></span>
					</div>
				</div>
			</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
		</div>
	</div>
	<!-- hidden fields -->
	<input type="hidden" name="printpdfid" id="printpdfid" value="2" />
	<input type="hidden" name="printsnumber" id="printsnumber" value="" />
	<input type="hidden" name="stocksessionid" id="stocksessionid" value="" />
	
	<!--sms icon overlay -->
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay alertsoverlay overlayalerts" id="mobilenumbershow" style="overflow-y: scroll;overflow-x: hidden;">
			<div class="row sectiontoppadding">&nbsp;</div>
			<div class="large-3 medium-6 large-centered medium-centered columns paddingzero " >
				<form method="POST" name="convertoverlayform" style="" id="convertoverlayform" action="" enctype="" class="clearconvertoverlayform">
					<div class="alert-panel" style="background-color:#465a63">
						<div class="alertmessagearea">
							<div class="alert-title">Select Mobile Number</div>
							<div class="alert-message">
								<span class="firsttab" tabindex="1000"></span>
								<div id="smsmoduledivhid" class="static-field overlayfield">  
									<label>Module<span class="mandatoryfildclass">*</span></label>
									<select class="chzn-select validate[required] ffieldd" data-placeholder="Select" data-prompt-position="topLeft:14,36" id="smsmodule" name="smsmodule" tabindex="1001">
										<option value="">Select</option>
									</select>
								</div>
								<div class="row">&nbsp;</div>
								<div class="static-field overlayfield">  
									<label>Mobile Number<span class="mandatoryfildclass">*</span></label>
									<select class="chzn-select validate[required]" multiple data-placeholder="Select" data-prompt-position="topLeft:14,36" id="smsmobilenumid" name="smsmobilenumid" tabindex="1002">
										<option value="">Select</option>
									</select>
								</div>
								<input type="hidden" name="smsrecordid" id="smsrecordid" />
								<input type="hidden" name="smsmoduleid" id="smsmoduleid" />
								<input type="hidden" name="smsmobilenum" id="smsmobilenum" />
							</div>
						</div>
						<div class="alertbuttonarea">
							<input type="button" id="mobilenumbersubmit" name="mobilenumbersubmit" value="Submit" tabindex="1003" class="alertbtnyes" >
							<input type="button" id="mobilenumberoverlayclose" name="" value="Cancel" tabindex="1004" class="alertbtnno flloop  alertsoverlaybtn" >
							<span class="lasttab" tabindex="1005"></span>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Call icon Overlay -->
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay alertsoverlay overlayalerts" id="c2cmobileoverlay" style="overflow-y: scroll;overflow-x: hidden;">
			<div class="row sectiontoppadding">&nbsp;</div>
			<div class="large-3 medium-6 large-centered medium-centered columns paddingzero" >
				<form method="POST" name="c2cmobileoverlayform"  id="c2cmobileoverlayform" action="" enctype="" class="clearconvertoverlayform">
					<div class="alert-panel" style="background-color:#465a63">
						<div class="alertmessagearea">
							<div class="alert-title">Select Mobile Number</div>
							<div class="alert-message">
								<span class="firsttab" tabindex="1000"></span>
								<div id="callmoduledivhid" class="static-field overlayfield">  
									<label>Module<span class="mandatoryfildclass">*</span></label>
									<select class="chzn-select validate[required] ffieldd" data-placeholder="Select" data-prompt-position="topLeft:14,36" id="callmodule" name="callmodule" tabindex="1001">
										<option value="">Select</option>
									</select>
								</div>
								<div class="static-field overlayfield">  
									<label>Mobile Number<span class="mandatoryfildclass">*</span></label>
									<select class="chzn-select validate[required]" data-placeholder="Select" data-prompt-position="topLeft:14,36" id="callmobilenum" name="callmobilenum" tabindex="1002">
										<option value="">Select</option>
									</select>
								</div>
								<input type="hidden" name="callrecordid" id="callrecordid" />
								<input type="hidden" name="callmoduleid" id="callmoduleid" />
								<input type="hidden" name="calcount" id="calcount" />
							</div>
						</div>
						<div class="alertbuttonarea">
							<input type="button" id="callnumbersubmit" name="callnumbersubmit" value="Submit" tabindex="1003" class="alertbtnyes" >
							<input type="button" id="c2cmobileoverlayclose" name="" value="Cancel" tabindex="1004" class="alertbtnno flloop  alertsoverlaybtn" >
							<span class="lasttab" tabindex="1005"></span>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Base delete overlay for combined modules -->
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay alertsoverlay overlayalerts" id="basedeleteoverlayforcommodule">
			<div class="row sectiontoppadding">&nbsp;</div>
			<div class="overlaybackground">
				<div class="alert-panel">
					<div class="alertmessagearea">
					<div class="row">&nbsp;</div>
					<div class="alert-message basedelalerttxt">Delete this data?</div>
					</div>
					<div class="alertbuttonarea">
						<span class="firsttab" tabindex="1000"></span>
						<input type="button" id="" name="" value="Delete" tabindex="1001" class="alertbtnyes ffield commodyescls">
						<input type="button" id="basedeleteno" name="" tabindex="1002" value="Cancel" class="alertbtnno flloop  alertsoverlaybtn" >
						<span class="lasttab" tabindex="1003"></span>
					</div>
					<div class="row">&nbsp;</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Tag Image Display in overlay. -->
	<div class="large-12 small-12 medium-12 columns" style="position:absolute;">
		<div class="overlay" id="salesdetailimageoverlaypreview" style="overflow-y: scroll;overflow-x: hidden;">
			<div class="large-12 columns end paddingbtm ">
					<div class="large-6 columns cleardataform z-depth-5 border-modal-8px" style="background: #ffffff;border-radius: 6px !important;">
						<div class="large-12 columns sectionheaderformcaptionstyle">Tag Image Preview</div>
						<div class="large-12 columns">
							<div id="salesdetailtagimagepreview" class="large-12 columns uploadcontainerstyle" style="overflow: scroll;height:27rem; padding:0px; border:1px solid #CCCCCC"></div>
							<div class="row">&nbsp;</div>
							<div class="row">&nbsp;</div>
							<div class="large-12" style="">
							<div class="tagimagepreviewcls">
								<input type="button" value="Close" id="salesdetailimagepreviewclose" class="btn leftformsbtn" tabindex="124">
							</div>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
	<!-- Itemtag - Image Preview Base -->
	<div class="large-12 small-12 medium-12 columns" style="position:absolute;">
		<div class="overlay" id="baseitemtagimgpreview" style="overflow-y: scroll;overflow-x: hidden;z-index:44;">
			<div class="large-12 columns end paddingbtm ">
				<div class="large-9 columns cleardataform z-depth-5 border-modal-8px" style="background: #ffffff;border-radius: 6px !important;">
					<div class="large-9 columns sectionheaderformcaptionstyle">Tag Image Preview</div>
					<div class="prevnexthideshow">
						<div id="baseitemtagprevid" class="large-12 columns material-icons baseitemtagprevid" style="width:20px; vertical-align:middle;">chevron_left</div>
					</div>
					<div class="large-6 columns">
						<div id="baseitemtagimagepreview" class="large-12 columns uploadcontainerstyle" style="overflow: scroll;height:27rem; padding:0px; border:1px solid #CCCCCC"></div>
					</div>
					<div class="large-6 columns">
						<div id="itemtagdetailsection" class="large-12 columns" style="height:27rem; padding:0px; border:1px solid #CCCCCC; font-weight: 100; padding-top: 10px; padding-bottom: 10px; color: black; font-size: 1.1rem; display: inline-block; font-family: 'Nunito semibold', sans-serif; line-height: 45px; padding-left: 10px;">
							<div>BARCODE : <span id="imgpreview_itemtagno"></span></div>
							<div>RFID TAG NO : <span id="imgpreview_rfidtagno"></span></div>
							<div>CATEGORY : <span id="imgpreview_category"></span></div>
							<div>PRODUCT : <span id="imgpreview_product"></span></div>
							<div>PURITY : <span id="imgpreview_purity"></span></div>
							<div>STORAGE : <span id="imgpreview_storage"></span></div>
							<div>GROSS WEIGHT : <span id="imgpreview_grossweight"></span></div>
							<div>STONE WEIGHT : <span id="imgpreview_stoneweight"></span></div>
							<div>NET WEIGHT : <span id="imgpreview_netweight"></span></div>
						</div>
					</div>
					<div class="large-12" style="">
						<div class="row">&nbsp;</div>
						<div class="row">&nbsp;</div>
						<div class="tagimagepreviewcls">
							<input type="button" value="Close" id="baseitemtagimgprevclose" class="btn leftformsbtn" tabindex="124">
						</div>
					</div>
					<div class="prevnexthideshow">
						<div id="baseitemtagnextid" class="large-12 columns material-icons baseitemtagnextid" style="width:20px; vertical-align:middle;">chevron_right</div>
					</div>
				</div>
			</div>
		</div>
	</div>