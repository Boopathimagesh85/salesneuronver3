<?php
	$CI =& get_instance();
	$appdateformat = $CI->Basefunctions->appdateformatfetch();
	$device = $this->Basefunctions->deviceinfo();
?>
	<div class="row" style="max-width:100%">
		<div class="off-canvas-wrap" data-offcanvas>
			<div class="inner-wrap">
				<?php
				if($gridenable == "yes") {
				?>
					<div id="<?php echo $griddisplayid;?>" class="gridviewdivforsh">
						<?php 
							$this->load->view('Base/mainviewheader');
						?>
						<div class="desktop actionmenucontainer headeraction-menu ">
							<ul  class="module-view" style="position:relative;left:-1px;">
								<li class="view-change-dropdown" style="position:relative;height:36px;border-bottom-style: solid;left: 17px;">
									<select id="dynamicdddataview" class="chzn-select dropdownchange" data-placeholder="Select View"  tabindex="" name="dynamicdddataview" title="" style="border-radius: 2px;border-bottom:5px; border-bottom-color:red;position:relative;height:32px;color:black !important;">
										<?php
											$userid = $this->Basefunctions->logemployeeid;
											foreach($dynamicviewdd as $key):
											$select="";
											/* if($userid == $key->employeeid && $key->viewcreationid == $key->viewid) {
												$select = 'selected="selected"';
											} */
										?>
											<option value="<?php echo $key->viewcreationname;?>" data-dynamicdddataviewid="<?php echo $key->viewcreationid;?>" <?php echo $select; ?> ><?php echo $key->viewcreationname;?></option>
										<?php endforeach;?>
									</select>
									
								</li>
						
							
								<li class="view-change-icon">
										<span class="icon-box" id="" style="position: relative;left:0px;margin-right:20px;top:-5px;"><i title="help" id="viewtoggle" class="material-icons" style="font-size: 2.0rem;line-height: 1.0;color: #788db4;left:18px;position:relative;">filter_list</i><span class="actiontitle">Filter</span></span>
								<div class="drop-container viewdropbox dropdown-button view-btn-for-2nd-head" data-activates="viewdrop">
										<span class="icon-box" style="position:relative;top: -17px;font-size:13px">View<i title="" class="material-icons" id="viewcategory" style="position:relative;left:3px;">details</i></span>
											<ul id="viewdrop" class="viewnavaction-drop view-dropdown-for-2nd-head arrow_box">
												<li id="viewaddicon" class="viewaddiconclass"><span class="icon-box">Create</span></li>
												<li id="viewediticon" class="viewediticonclass"><span class="icon-box">Edit</span></li>
												<li id="viewdeleteicon" class="viewdeleteiconclass"><span class="icon-box">Delete</span></li>
												<li id="viewcloneicon" class="viewcloneiconclass"><span class="icon-box">Clone</span></li>
											</ul>
										</span>
									</div>							
								</li>
							</ul>
							<?php 
							
							
								$device = $this->Basefunctions->deviceinfo();
								$prev = " ";
								$new = 0;
								$new1 = 0;
								$newp = 0;
								echo '<ul class="actiontoggle-view tabaction" style="position:relative;left:3px;top: -10px;">';
								echo '<li class="action-icons" style="position:relative;top: 2px;line-height: 1em;">';
								foreach($actionassign as $key):
									if($key->toolbarname!='') {
										$cur = $key->toolbarcategoryid;
										if($cur == 1){
											if($prev!=$cur) {
												$prev=$key->toolbarcategoryid;
											}
											$fname = preg_replace('/[^A-Za-z0-9]/', '',$key->toolbarname);
											$name = strtolower($fname);
											echo '<span id="'.$name.'icon" class="icon-box '.$name.'iconclass add-form-for-2nd-head">Add</span>&nbsp;&nbsp';
												
										} else {
											$catname = $key->toolbarcategoryname;
											$cname = preg_replace('/[^A-Za-z0-9]/', '',$key->toolbarcategoryname);
											$categoryname = strtolower($cname);
											if($prev!=$cur) {
												$prev=$key->toolbarcategoryid;
												if($prev != " ") {
													echo '<div class="drop-container advanceddropbox dropdown-button more-btn-for-2nd-head" data-activates="'.$categoryname.'drop"><span class="icon-box" style="position:relative;top:-1px;" ><i title="help" id="viewcategory" class="material-icons" style="font-size: 1.6rem;line-height: 1.0;color: #fff;left:-8px;top:0px;position:relative;">more_horiz</i></span>
													<ul id="'.$categoryname.'drop" class="navaction-drop arrow_box" style="width:200px;top: 56px !important;"><li id="peronalsettingicon" style="color:white;background-color:#2c2f48;height: 60px;    top: -8px; position: relative;border-top-left-radius: 4px;border-top-right-radius: 4px;"><i title="actionicons" id="actionicons" class="material-icons" style="font-size: 2rem;line-height: 1.9;color: #fff;">apps</i><span style="position:relative;top:-12px;color:#fff !important;font-size:0.9rem;font-family:
			"Nunito, sans-serif;"">Actions Panel</span></li><!--<li Class="action-drophead"><span>'.$catname.'</span></li>-->';
												}
											}
											$fname = preg_replace('/[^A-Za-z0-9]/', '',$key->toolbarname);
											$name = strtolower($fname);
											if($prev ==$cur) {
												if($name != 'viewselect' && $name != 'reload'){
													echo '<li id="'.$name.'icon"><span><i title="actionicons" id="actionicons" class="material-icons '.$key->description.' '.$name.'iconclass" title="'.$key->toolbartitle.'" style="font-size: 1.4rem;line-height: 1.9;color: #788db4;">'.$key->description.'</i><span style="position:relative;top:-5px;">'.$key->toolbartitle.'</span></span></li>'.PHP_EOL;
												}
											}
											$new++;
										}
										
									}
								endforeach;
								if($new != 0){
									echo '</ul></span></div>';
								}
								echo '</li>
								</ul>';
								
								
								
							?>
						</div>
						
						
						
						
						
						
						<!-- Dashboard Code Start here -->
						<?php
						if(isset($dashboradviewdata)) {
						?>
							<div id="dashboardcontainer" style="overflow-y: scroll;position: relative;width: 100%;background-color:#e0e0e0;" class="hidedisplay">
								<?php
								$this->load->view('Base/dashboardselectionoverlay');
								?>
								<div class="large-12 medium-12 column" style="height: 2.4rem;padding-top: 0.3rem;right: 1rem;text-align: right;font-size: 1.2rem;">
									<span id="dashboardselecticon" style="cursor:pointer" class="" title="Dashboard"><i class="material-icons">dashboard</i></span>
									<span id="dashboardpreviousicon" style="cursor:pointer" class="" title="Previous"> <i class="material-icons">keyboard_arrow_left</i></span>
									<span id="dashboardnexticon" style="cursor:pointer" class="" title="Next"><i class="material-icons">keyboard_arrow_right</i> </span>
									<input type="hidden" name="dashboardrecordid" id="dashboardrecordid" value="" />
									<span id="closedashboardform" style="cursor:pointer" class="" title="Close"><i class="material-icons">close</i> </span>
								</div>
								<div id="widgetdata" class=""></div>
							</div> 
						<?php
						}
						?>
							<div id="mainviewgriddivhid" class="large-12 medium-12 columns paddingzero fullgridview" style="">
								<div id="<?php echo $gridtableid;?>width" class="touchgrid-container viewgridcolorstyle borderstyle" style="margin: 10px 10px 7px 15px;">
									<div class="desktop row-content" id="<?php echo $gridtableid;?>">
										<!-- Table content[Header & Record Rows] -->
									</div>
									<div class="footer-content footercontainer" id="<?php echo $gridtableid;?>footer">
										<!-- Footer Previous / Next page -->
									</div>
								</div>
							</div>
							<!-- Filter Code Start here -->
							<div id="filterviewdivhid" class="large-3 medium-3 columns paddingzero hidedisplay">
						</div>
					</div>
				<?php
				}
				?>	

				
				<!-- Form Generation -->
				<?php
				
				// (2) Enter the modtabgrp hidden input on every page
//echo '<input id="modtabgrp" type="hidden" name="modtabgrp" value="'.base64_encode(serialize($modtabgrp)).'">';
				 foreach($forminfo as $value) {
					echo '<div  id="'.$value['id'].'" class="'.$value['class'].'">'.PHP_EOL;
					echo $this->load->view(''.$value['formname'].'');
					echo '</div>'.PHP_EOL;
				 }
							 
				?>
				
				<!--View Creation Overlay-->
				<div class="hidedisplay" id="viewcreationformdiv">
					
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" name="deviceinfofinder" id="deviceinfofinder" value="<?php echo $device; ?>"/>
	<!-- date format hidden fields -->
	<?php
		$compappdate=$this->Basefunctions->phpmysqlappdateformatfetch();
	?>
	<input type="hidden" name="compdateformat" id="compdateformat" value="<?php echo $compappdate['phpformat']; ?>" />
	<input type="hidden" name="moduleviewid" id="moduleviewid" value="<?php echo implode(',',$moduleid); ?>" />
	<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<div class="large-12 columns" style="position:absolute;"><div class="overlay" id="viewdeleteoverlayconform" style="z-index:120;"></div></div>