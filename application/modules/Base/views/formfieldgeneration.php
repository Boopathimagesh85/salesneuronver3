<?php
	//tree creation
	

	function createTree($items = array(), $parentId = 0,$removeid = 0) {
		echo " <ul class='dl-submenu'> ";
		if($removeid == 0){
				echo '<li><a href="#" style="font-weight:bold;">Remove<i class="material-icons parentclear">close</i></a></li>';
		}
		if(count($items) > 0 ) {
			foreach ($items[$parentId] as $item) {
				echo '<li data-listname="'.$item['name'].'" data-level="'.$item['level'].'"  data-listid="'.$item['id'].'" ><a href="#">'.$item['name'].'</a>';//level title
				$curId = $item['id'];
				//if there are children
				if (!empty($items[$curId])) {
					createTree($items, $curId,1);
				}
				echo '</li>';
			}
		}
		echo '</ul>';
	}
	
	
	//form fields generation based on template
	

	
	function formfieldstemplategenerator($modtabgrpinfo,$gridtype=null,$innergridtype='overlay') {
		$CI =& get_instance();
		$gridid = 1;
		$tabgrpid = 1;
		$prevgrpid = "";
		$fieldnames = array();
		$fieldtables = array();
		$tabcolmnname = array();
		$parenttabname = array();
		$gridname = array();
		$editornames = array();
		$grdpartabname = array();
		$anfieldnames = array();
		$anfieldnameid = array();
		$anmoduleid = array();
		$anfiledtab = array();
		$attachfieldname = array();
		$attachcolname = array();
		$attachuitypeid = array();
		$attachfieldid = array();
		$editor = 'No';
		$fupload = 'No';
		$i = 100;
		$mm=1;
		$readmode = "";
		$tabgroupcount = 1;
		$tabsectioncount = 1;
		foreach($modtabgrpinfo as $values) {
			if($tabgrpid == 1) {
				echo '<div class="hiddensubform transitionhiddenform form-active" id="subformspan'.$tabgrpid.'">'; //tab group creation (1 st group)
				$tabgrpid++;
			} else {
				echo '<div class="hidedisplay hiddensubform transitionhiddenform" id="subformspan'.$tabgrpid.'">'; //tab group creation (other group)
				$tabgrpid++;
			}
			$moduleid = $values['moduleid'];
			$templateid = $values['templateid'];
			$tabgroupid = $values['tabpgrpid'];
			$tabgrpname = $values['tabgrpname'];
			$usertabgrpname = $values['usertabgrpname'];
			if($templateid == '1') { //template 1 - without grid
				$modtabsecfrmfkdsgrp = $CI->Basefunctions->moduletabsecfieldsgeneration($moduleid,$tabgroupid);
				//grouping tab group value
				$tempgrplevel=0;
				$newgrpkey=0;
				$tabgroup=array();
				foreach ($modtabsecfrmfkdsgrp as $key => $val) {
					if($tempgrplevel==$val['modtabgrpid']) {
						$tabgroup[$tempgrplevel][$newgrpkey]=$val;
					} else {
						$tabgroup[$val['modtabgrpid']][$newgrpkey]=$val;
					}
					$newgrpkey++;       
				}
				$tabsectiongrp = array();
				if(count($tabgroup) > 0) {
					//group tab section within tab group
					$tempgrpseclevel=0;
					$newgrpseckey=0;
					foreach($tabgroup as $key => $value) {
					   foreach($value as $key1 => $val) {
							if($tempgrpseclevel==$val['tabsectionid']) {
								$tabsectiongrp[$key][$tempgrpseclevel][$newgrpseckey]=$val;
							} else {
								$tabsectiongrp[$key][$val['tabsectionid']][$newgrpseckey]=$val;
							}
							$newgrpseckey++; 
						}
					}
				}
				$device = $CI->Basefunctions->deviceinfo();
				//form section and elements generation
				foreach($tabsectiongrp as $grpkey => $grpvalarr) {
					$prevgrpsecid = "";
					$tabsectioncount = 1;
					foreach($grpvalarr as $seckey => $secvalarr) { //section creation
					if($device=='phone') 
					{
						echo '<div class="large-4 columns end paddingbtmmobile tabletinfo">';
					}
					else 
					{
						echo '<div class="large-4 columns end paddingbtm tabletinfo">';
					}
						foreach($secvalarr as $fieldkey => $fieldval) { //form field creation
							$curgrpsecid = $fieldval['tabsectionid'];
							if($prevgrpsecid != $curgrpsecid) {
								$prevgrpsecid=$fieldval['tabsectionid'];
							if($device=='phone') 
								{
									echo '<div class="large-12 columns cleardataformmobile" style="padding-left: 0 !important; padding-right: 0 !important;">';//tab section padding open
								echo '<div class="large-12 columns headerformcaptionstylemobile">'.$fieldval['tabsectionname'].'</div>';
								}
								else {
								echo '<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">';//tab section padding open	
								echo '<div class="large-12 columns headerformcaptionstyle">'.$fieldval['tabsectionname'].'</div>';
								}
								
								
								//function call for fields generation
								if($fieldval['colname'] != "salutationid") {
									$colmtypeid = $fieldval['fieldcolmtypeid'];
									echo fieldgenerate($fieldval,$i,$colmtypeid);
								}
							} else {
								//function call for fields generation
								if($fieldval['colname'] != "salutationid") {
									$colmtypeid = $fieldval['fieldcolmtypeid'];
									echo fieldgenerate($fieldval,$i,$colmtypeid);
								}
							}
							if($fieldval['uitypeid'] == 20) {
								$fieldsinfo = explode(',',$fieldval['ddparenttable']);
								if($fieldsinfo[1]!="") { $fieldnames[] = $fieldsinfo[1]; }
								if($fieldval['tabname']!="") { $fieldtables[] = $fieldval['tabname']; }
								if($fieldsinfo[1]!="") { $tabcolmnname[] = $fieldsinfo[1]; }
								if($fieldval['parenttable']!="") { $parenttabname[] = $fieldval['parenttable']; }
							}
							if($fieldval['fieldname']!="" && $fieldval['tabname']!="" && $fieldval['colname']!="" && $fieldval['parenttable']!="") {
								$fieldnames[] = $fieldval['fieldname'];
								$fieldtables[] = $fieldval['tabname'];
								$tabcolmnname[] = $fieldval['colname'];
								$parenttabname[] = $fieldval['parenttable'];
							}
							if($fieldval['uitypeid'] == 14) {
								$anfieldnames[] = $fieldval['fieldname'];
								$anfieldnameid[] = $fieldval['modfieldid'];
								$anfiledtab[] = $fieldval['tabname'];
								$anmoduleid[] = $moduleid;
							}
							if($fieldval['uitypeid'] == 15 || $fieldval['uitypeid'] == 16) {
								$attachfieldname[] = $fieldval['fieldname'];
								$attachcolname[] = $fieldval['colname'];
								$attachuitypeid[] = $fieldval['uitypeid'];
								$attachfieldid[] = $fieldval['modfieldid'];
								$fupload = "Yes";
							}
							if($fieldval['uitypeid'] == 24) {
								$editor = "Yes";
							}
							$readmode = "";
							$mandatoryopt = "";
							if($fieldval['fieldmode'] != "No") {
								$i++;
							}
						}
						echo '<div class="large-12 columns">&nbsp;</div>';
						echo '</div>'; //tab section padding close
						echo '</div>'; //tab section close
					}
				}
				$industryid = $CI->Basefunctions->industryid;
				if($moduleid == '207' && $industryid == '2') {
					echo '<div id="image-container1" class="large-4 columns end paddingbtm">
							<div class="large-12 columns cleardataform paddingzero">
							<div class="large-12 columns headerformcaptionstyle">Image Capture</div>
							<div class="large-12 medium-12 small-12 columns">
							<div id="webcamattachdisplay" class="image-containerbox large-12 medium-12 small-12 columns uploadcontainerstyle card-panel"></div>
							</div>
							<div class="large-12 columns" style="text-align: right">
							<label></label>
							<input id="take-snapshot" class="frmtogridbutton addbtnclass addkeyboard btn" type="button" name="" tabindex="" value="Capture">
							</div>
							</div>
							</div>
							<div id="image-container2" class="large-4 columns end paddingbtm hidedisplay">
							<div class="large-12 columns cleardataform paddingzero">
							<div class="large-12 columns headerformcaptionstyle">Image Preview</div>
							<div class="large-12 medium-12 small-12 columns">
							<div id="captureimagedisplay" class="image-containerbox large-12 medium-12 small-12 columns uploadcontainerstyle card-panel"></div>
							</div>
							<div class="large-12 columns" style="text-align: right">
							<textarea class="hidedisplay" id="imgurldata"></textarea>
							<textarea class="hidedisplay" id="imgurldatadelete"></textarea>
							<label></label>
							<input id="savecapturedimg" class="addbtnclass addkeyboard btn" type="button" name="" tabindex="" value="Save">
							<input id="cancelcapturedimg" class="btn" type="button" name="" tabindex="" value="Cancel">
							</div>
							</div>
							</div>';
				}
			} else if($templateid == '2') { //template 2 - with grid
				$modtabsecfrmfkdsgrp = $CI->Basefunctions->moduletabsecfieldsgeneration($moduleid,$tabgroupid);
				$device = $CI->Basefunctions->deviceinfo();
				 //grouping tab group value
				$tempgrplevel='0';
				$newgrpkey=0;
				$tabmodule=array();
				 foreach ($modtabsecfrmfkdsgrp as $key => $val) {
					if($tempgrplevel == ''.$val['modulename'].'') {
						$tabmodule[$tempgrplevel][$newgrpkey]=$val;
					} else {
						$tabmodule[''.$val['modulename'].''][$newgrpkey]=$val;
					}
					$newgrpkey++;
				}
				$tabsectiongrp = array();
				if(count($tabmodule) > 0) {
					//group tab section within tab group
					$tempgrpseclevel=0;
					$newgrpseckey=0;
					foreach($tabmodule as $key => $value) {
						foreach($value as $key1 => $val) {
							if($tempgrpseclevel==$val['tabsectionid']) {
								$tabsectiongrp[$key][$tempgrpseclevel][$newgrpseckey]=$val;
							} else {
								$tabsectiongrp[$key][$val['tabsectionid']][$newgrpseckey]=$val;
							}
							$newgrpseckey++; 
						}
					}
				}
				//create form section and elements
				$prevgrpid = "";
				${'$griddata'.$gridid} = array();
				foreach($tabsectiongrp as $grpkey => $grpvalarr) {
					//$str = str_replace(" ","",$grpkey);
					$str = preg_replace('/[^A-Za-z0-9]/', '', $grpkey);
					$modname = strtolower($str);
					$name = substr( strtolower($tabgrpname),0,3);
					echo '<div class="large-12 columns scrollbarclass padding-space-open-for-form">';//div for container
					$prevgrpsecid = "";
					if($innergridtype == 'overlay') {
						echo '<div class="closed effectbox-overlay effectbox-default" id="'.$modname.$name.'overlay">';
					} else {
						echo '<div class="" id="">';//id="'.$modname.$name.'overlay"
					}
					
					foreach($grpvalarr as $seckey => $secvalarr) { //section creation 
						if($innergridtype == 'overlay') {
							echo '<div class="large-4 columns gridformclear end paddingbtm large-offset-4">'; //tab section open
						} else {
							echo '<div class="large-4 columns gridformclear end paddingbtm">'; //tab section open
						}
						foreach($secvalarr as $fieldkey => $fieldval) { //form field creation
							$curgrpsecid = $fieldval['tabsectionid'];
							if($prevgrpsecid != $curgrpsecid) {
								$prevgrpsecid=$fieldval['tabsectionid'];
								$name = substr( strtolower($tabgrpname),0,3);
								echo '<div id="'.$modname.$name.'addgrid'.$mm.'validation" class="large-12 columns validationEngineContainer cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">';//tab add section padding open
								echo '<div id="'.$modname.$name.'editgrid'.$mm.'validation" class="large-12 columns validationEngineContainer" style="padding-left: 0 !important; padding-right: 0 !important;">';//tab edit section padding open
								if($innergridtype == 'overlay') {
									echo '<div class="large-12 columns sectionheaderformcaptionstyle">'.$fieldval['tabsectionname'].'</div>';
									echo '<div class="large-12 columns sectionpanel">';
								} else {
									echo '<div class="large-12 columns headerformcaptionstyle">'.$fieldval['tabsectionname'].'</div>';
									echo '<div class="">';
								}
								//function call for fields generation
							   if($fieldval['colname'] != "salutationid") {
									$colmtypeid = $fieldval['fieldcolmtypeid'];
									echo fieldgenerate($fieldval,$i,$colmtypeid);
								}
							} else {
								//function call for fields generation
								if($fieldval['colname'] != "salutationid") {
									$colmtypeid = $fieldval['fieldcolmtypeid'];
									echo fieldgenerate($fieldval,$i,$colmtypeid);
								}
							}
							$readmode = "";
							$mandatoryopt = "";
							if($fieldval['fieldmode'] != "No") {
								$i++;
							}
							${'$griddata'.$gridid}[] = $fieldval['fieldname'];
							${'$gridfielddata'.$gridid}[] = $fieldval['colname'];
							${'$griddatauitype'.$gridid}[] = $fieldval['uitypeid'];
							${'$grddatatabname'.$gridid}[] = $fieldval['tabname'];
							${'$grddatapartabname'.$gridid}[] = $fieldval['parenttable'];
							$grdpartabname[] = $fieldval['parenttable'];
							if($fieldval['uitypeid'] == 20) {
								$fieldsinfo = explode(',',$fieldval['ddparenttable']);
								if($fieldsinfo[1]!="") { $fieldnames[] = $fieldsinfo[1]; }
								if($fieldval['tabname']!="") { $fieldtables[] = $fieldval['tabname']; }
								if($fieldsinfo[1]!="") { $tabcolmnname[] = $fieldsinfo[1]; }
								if($fieldval['parenttable']!="") { $parenttabname[] = $fieldval['parenttable']; }
							}
							if($fieldval['fieldname']!="" && $fieldval['tabname']!="" && $fieldval['colname']!="" && $fieldval['parenttable']!="") {
								$fieldnames[] = $fieldval['fieldname'];
								$fieldtables[] = $fieldval['tabname'];
								$tabcolmnname[] = $fieldval['colname'];
								$parenttabname[] = $fieldval['parenttable'];
							}
							if($fieldval['uitypeid'] == 14) {
								$anfieldnames[] = $fieldval['fieldname'];
								$anfieldnameid[] = $fieldval['modfieldid'];
								$anfiledtab[] = $fieldval['tabname'];
								$anmoduleid[] = $moduleid;
							}
							if($fieldval['uitypeid'] == 15 || $fieldval['uitypeid'] == 16) {
								$attachfieldname[] = $fieldval['fieldname'];
								$attachcolname[] = $fieldval['colname'];
								$attachuitypeid[] = $fieldval['uitypeid'];
								$attachfieldid[] = $fieldval['modfieldid'];
								$fupload = "Yes";
							}
							if($fieldval['uitypeid'] == 24) {
								$editor = "Yes";
							}
						}
						if($innergridtype == 'overlay') {
							echo '</div><div class="divider large-12 columns"></div>';
							echo '<div class="large-12 columns submitkeyboard sectionalertbuttonarea"  style="text-align: right">';
						} else {
							echo '</div><div class="">&nbsp;</div>';
							echo '<div class="large-12 columns submitkeyboard sectionalertbuttonarea"  style="text-align: right">';
						}
						if($device!='phone') {
							//echo label('&nbsp;',array());
						}
						//Update button
						$upbutattr = array('class'=>'frmtogridbutton updatebtnclass updatekeyboard hidedisplayfwg alertbtnyes','tabindex'=>''.$i.'','value'=>'Submit','data-frmtogriddataid'=>''.$gridid.'','data-frmtogridname'=>''.$modname.$name.'addgrid'.$mm.'');
						echo button(''.$modname.$name.'updatebutton',$upbutattr);
						//Add button
						$butattr = array('class'=>'frmtogridbutton addbtnclass addkeyboard alertbtnyes','tabindex'=>''.$i.'','value'=>'Save','data-frmtogriddataid'=>''.$gridid.'','data-frmtogridname'=>''.$modname.$name.'addgrid'.$mm.'');
						echo button(''.$modname.$name.'addbutton',$butattr);
						//cancel button
						if($innergridtype == 'overlay') {
							$canbutattr = array('class'=>'alertbtnno addsectionclose cancelkeyboard leftcloseposition','tabindex'=>''.$i.'','value'=>'Close','data-frmtogriddataid'=>''.$gridid.'','data-frmtogridname'=>''.$modname.$name.'addgrid'.$mm.'');
							echo button(''.$modname.$name.'cancelbutton',$canbutattr);
						}
						echo close('div');
						echo '</div>'; //tab add section padding close
						echo '</div>'; //tab edit section padding close
						echo '</div>'; //tab section close
						//hidden fields
						$datas = implode(',',${'$griddata'.$gridid});
						echo hidden('gridcolnames'.$gridid,$datas);
						$flddatas = implode(',',${'$gridfielddata'.$gridid});
						echo hidden('gridfieldnames'.$gridid,$flddatas);
						$uidatas = implode(',',${'$griddatauitype'.$gridid});
						echo hidden('gridcoluitype'.$gridid,$uidatas);
						$griddatatabname = implode(',',${'$grddatatabname'.$gridid});
						echo hidden('griddatatabnameinfo'.$gridid,$griddatatabname);
						$griddatapartabname = implode(',',${'$grddatapartabname'.$gridid});
						echo hidden('griddatapartabnameinfo'.$gridid,$griddatatabname);
						echo hidden('griddataprimayid'.$gridid,'');
						$hideovedata = ''.$modname.$name.'addbutton'.','.''.$modname.$name.'updatebutton'.','.''.$modname.$name.'cancelbutton';
						echo hidden('griddataoverlayhide'.$gridid,$hideovedata);
					}
					echo '</div>';
					//Inner grid generation
					if($device=='phone') {
						$grdname = substr( strtolower($tabgrpname),0,3);
						$grdtabname = preg_replace('/[^A-Za-z0-9]/', '', $grdname);
						$name = strtolower($grdtabname);
						echo '<div class="large-8 columns paddingbtm">';
						echo '<div class="large-12 columns paddingzero">';
						echo '<div class="large-12 columns viewgridcolorstyle" style="padding-left:0;padding-right:0;">';
						echo '<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0 0rem;">';
						echo '<div class="large-6 medium-6 small-6 columns lefttext">';
						echo '<span>'.ucwords($tabgrpname).' List <span class="fwgpaging-box '.$name.'gridfootercontainer"></span></span>';
						echo '</div>';
						echo '<div class="large-6 medium-6 small-6 columns innergridicon innergridiconhover righttext">';
						echo '<span id="'.$modname.$name.'ingridaddspan'.$mm.'"><i id="'.$modname.$name.'ingridadd'.$mm.'" class="material-icons addiconclass" title="Add" style="padding-right:0rem;">add</i></span>';
						echo '<span id="'.$modname.$name.'ingrideditspan'.$mm.'" style="display:none;"><i id="'.$modname.$name.'ingridedit'.$mm.'" class="material-icons editiconclass" title="Edit" style="padding-right:0rem;">edit</i></span>';
						echo '<span id="'.$modname.$name.'ingriddelspan'.$mm.'" ><i id="'.$modname.$name.'ingriddel'.$mm.'" class="material-icons deleteiconclass" title="Delete" style="padding-right:0rem">delete</i></span>';
						echo '<span id="'.$modname.$name.'ingridselspan'.$mm.'" style="display:none;"><i id="'.$modname.$name.'ingridsel'.$mm.'" class="material-icons checkboxiconclass unchecked" title="Choose" style="padding-right:0rem;">select_all</i></span>';
						echo '<span id="'.$modname.$name.'ingridserspan'.$mm.'" style="display:none;"><i id="'.$modname.$name.'ingridserspan'.$mm.'" class="searchiconclass" title="Search" style="padding-right:0rem;">search</i></span>';
						echo '</div>';
						echo '</div>';
						echo '<div class="large-12 forgetinggridname columns" id="'.$modname.$name.'addgrid'.$mm.'width" style="padding-left:0;padding-right:0;">';
						if($moduleid == 252 || $moduleid == 256 || $moduleid == 205 || $moduleid == 206 || $moduleid == 269 || $moduleid == 30) {
							$footercontent = 'footer-content';
						} else {
							$footercontent = '';
						}
						if($gridtype=='grid') {
							echo '<table id="'.$modname.$name.'addgrid'.$mm.'"> </table> ';
							echo '<div id="'.$modname.$name.'addgridnav'.$mm.'" class="w100"></div> ';
						} else {
							$device = $CI->Basefunctions->deviceinfo();
							if($device=='phone') {
								echo '<div class="desktop row-content inner-gridcontent" id="'.$modname.$name.'addgrid'.$mm.'" style="height:420px;top:0px;">';
								echo '</div>';
								echo '<div class="footercontainer inner-gridfooter" id="'.$modname.$name.'addgrid'.$mm.'footer">';
								echo '</div>';
							} else {
								echo '<div class="desktop row-content inner-gridcontent" id="'.$modname.$name.'addgrid'.$mm.'" style="height:420px !important;top:0px;"> </div>';
								echo '<div class="footercontainer '.$footercontent.'" id="'.$modname.$name.'addgrid'.$mm.'footer"> </div>';
							}
						}
						echo '</div>';
					} else {
						$grdname = substr( strtolower($tabgrpname),0,3);
						$grdtabname = preg_replace('/[^A-Za-z0-9]/', '', $grdname);
						$name = strtolower($grdtabname);
						if($innergridtype == 'overlay') {
							echo '<div class="large-12">'; // columns innergridpaddingbtm
						} else {
							echo '<div class="large-8 columns innergridpaddingbtm">';
						}
						
						echo '<div class="large-12 columns paddingzero viewgridcolorstyle borderstyle">';
						echo '<div class="large-12 columns" style="padding-left:0;padding-right:0;">';
						echo '<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0 0rem;height:auto">';
						echo '<div class="large-6 medium-6 small-12 columns lefttext small-only-text-center">';
						echo '<span>'.ucwords($tabgrpname).' List <span class="fwgpaging-box '.$name.'gridfootercontainer"></span></span>';
						echo '</div>';
						echo '<div class="large-6 medium-6 small-12 columns innergridicon innergridiconhover righttext small-only-text-center">';
						if($innergridtype == 'overlay') {
						echo '<span id="'.$modname.$name.'ingridaddspan'.$mm.'"><i id="'.$modname.$name.'ingridadd'.$mm.'" class="material-icons addiconclass" title="Add" style="padding-right:0rem;">add</i></span>';
						}
					echo '<span id="'.$modname.$name.'ingrideditspan'.$mm.'" style="display:none;"><i id="'.$modname.$name.'ingridedit'.$mm.'" class="material-icons editiconclass" title="Edit" style="padding-right:0rem;">edit</i></span>';
						echo '<span id="'.$modname.$name.'ingriddelspan'.$mm.'"><i id="'.$modname.$name.'ingriddel'.$mm.'" class="material-icons deleteiconclass" title="Delete" style="padding-right:0rem">delete</i></span>';
						echo '<span id="'.$modname.$name.'ingridselspan'.$mm.'" style="display:none;"><i id="'.$modname.$name.'ingridsel'.$mm.'" class="material-icons checkboxiconclass unchecked" title="Choose" style="padding-right:0rem;">select_all</i></span>';
						echo '<span id="'.$modname.$name.'ingridserspan'.$mm.'" style="display:none;"><i id="'.$modname.$name.'ingridserspan'.$mm.'" class="material-icons searchiconclass" title="Search" style="padding-right:0rem;">search</i></span>';
						echo '</div>';
						echo '</div>';
						echo '<div class="large-12 forgetinggridname columns " id="'.$modname.$name.'addgrid'.$mm.'width" style="padding-left:0;padding-right:0;height:420px;top:0px;">';
						if($moduleid == 252 || $moduleid == 256 || $moduleid == 205 || $moduleid == 206 || $moduleid == 269 || $moduleid == 30) {
							$footercontent = 'footer-content';
						} else {
							$footercontent = '';
						}
						if($gridtype=='grid') {
							echo '<table id="'.$modname.$name.'addgrid'.$mm.'"> </table> ';
							echo '<div id="'.$modname.$name.'addgridnav'.$mm.'" class="w100"></div> ';
						} else {
							$device = $CI->Basefunctions->deviceinfo();
							if($device=='phone') {
								echo '<div class="desktop row-content inner-gridcontent" id="'.$modname.$name.'addgrid'.$mm.'" style="height:480px;top:0px;">';
								echo '</div>';
								echo '<div class="footercontainer inner-gridfooter" id="'.$modname.$name.'addgrid'.$mm.'footer">';
								echo '</div>';
							} else {
								echo '<div class="desktop row-content inner-gridcontent" id="'.$modname.$name.'addgrid'.$mm.'" style="height:382px;top:0px;"> </div>';
								echo '<div class="footercontainer '.$footercontent.'" id="'.$modname.$name.'addgrid'.$mm.'footer"> </div>';
							}
						}
						echo '</div>';
					}
					echo '</div>';
					//echo '<div class="large-6 columns">&nbsp;</div>';
					$modtabsecgen = $CI->Basefunctions->moduletabsecfieldfetchsummary($moduleid,$tabgroupid);
					if(count($modtabsecgen)>=1) {
						echo '<div class="large-12 columns paddingzero">';
						echo '<div class="large-12 columns">&nbsp;</div>';
						echo '<div class="large-12 columns cleardataform" style="background:#f5f5f5;padding-left:0;padding-right:0" >';
						echo '<div class="large-12 columns headerformcaptionstyle">Summary</div>';
						echo '<div class="large-12 columns">&nbsp;</div>';
						echo '<div class="large-12 columns" >';
						$rightside =1;
						foreach($modtabsecgen as $fkey => $fval) { //form field creation
							$readmode = "";
							if($fval['readmode'] == "Yes") {
								$readmode = 'readonly';
							}
							$fildmode = "";
							if($fval['fieldmode'] == "No") {
								$fildmode = ' hidedisplay';
							}
							if($rightside == count($modtabsecgen)){
								//$rightclass = 'pull-right';
								$rightclass = '';
							} else {
								$rightclass = '';
							}
							echo '<div class="large-2 columns paddingzero '.$rightclass.'" >';
							echo '<div class="input-field large-12 medium-6 small-12 columns'.$fildmode.'"><input name="'.$fval["fieldname"].'" type="text" id="'.$fval["fieldname"].'" value="" class="small-only-text-center" '.$readmode.'><label for="'.$fval["fieldname"].'">'.$fval["filedlabel"].'</label></div>';
							echo '</div>';
							if($fval['uitypeid'] == 20) {
								$fieldsinfo = explode(',',$fval['ddparenttable']);
								if($fieldsinfo[1]!="") { $fieldnames[] = $fieldsinfo[1]; }
								if($fval['tabname']!="") { $fieldtables[] = $fval['tabname']; }
								if($fieldsinfo[1]!="") { $tabcolmnname[] = $fieldsinfo[1]; }
								if($fval['parenttable']!="") { $parenttabname[] = $fval['parenttable']; }
							}
							if($fval['fieldname']!="" && $fval['tabname']!="" && $fval['colname']!="" && $fval['parenttable']!="") {
								$fieldnames[] = $fval['fieldname'];
								$fieldtables[] = $fval['tabname'];
								$tabcolmnname[] = $fval['colname'];
								$parenttabname[] = $fval['parenttable'];
							}
							$rightside++;
						}
						echo '<div class="large-12 columns" >&nbsp;</div>';
						echo '</div>';
						echo '</div>';
						echo '</div>';
					}
					echo '</div>';
					echo '</div>';
					echo '<div class="row">&nbsp;</div>';
					echo '<div class="row">&nbsp;</div>';
					echo "</div>";//add form container close
					$gridname[] = $modname.$name.'addgrid'.$mm;
					$mm++;
					$gridid++;
				}
			} else if($templateid == '3') { //text editor
				$modtabsecfrmfkdsgrp = $CI->Basefunctions->moduletabsecfieldsgeneration($moduleid,$tabgroupid);
				 //grouping tab group value
				$tempgrplevel='0';
				$newgrpkey=0;
				$tabmodule=array();
				 foreach ($modtabsecfrmfkdsgrp as $key => $val) {
					if($tempgrplevel == ''.$val['modulename'].'') {
						$tabmodule[$tempgrplevel][$newgrpkey]=$val;
					} else {
						$tabmodule[''.$val['modulename'].''][$newgrpkey]=$val;
					}
					$newgrpkey++;
				}
				$tabsectiongrp = array();
				if(count($tabmodule) > 0) {
					//group tab section within tab group
					$tempgrpseclevel=0;
					$newgrpseckey=0;
					foreach($tabmodule as $key => $value) {
						foreach($value as $key1 => $val) {
							if($tempgrpseclevel==$val['tabsectionid']) {
								$tabsectiongrp[$key][$tempgrpseclevel][$newgrpseckey]=$val;
							} else {
								$tabsectiongrp[$key][$val['tabsectionid']][$newgrpseckey]=$val;
							}
							$newgrpseckey++; 
						}
					}
				}
				//create form section and elements
				//$tabgrpid = 1;
				$prevgrpid = "";
				${'$griddata'.$gridid} = array();
				foreach($tabsectiongrp as $grpkey => $grpvalarr) {
					//$str = str_replace(" ","",$grpkey);
					$str = preg_replace('/[^A-Za-z0-9]/', '', $grpkey);
					$modname = strtolower($str);
					echo '<div class="large-12 columns scrollbarclass paddingzero">';//div for container
					$prevgrpsecid = "";
					//$i = 100;
					foreach($grpvalarr as $seckey => $secvalarr) { //section creation 
						echo '<div class="large-4 columns end paddingbtm">'; //tab section open
						foreach($secvalarr as $fieldkey => $fieldval) { //form field creation
							$curgrpsecid = $fieldval['tabsectionid'];
							if($prevgrpsecid != $curgrpsecid) {
								$prevgrpsecid=$fieldval['tabsectionid'];
								echo '<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">';//tab section padding open
								echo '<div class="large-12 columns headerformcaptionstyle">'.$fieldval['tabsectionname'].'</div>';
								//function call for fields generation
							   if($fieldval['colname'] != "salutationid") {
									$colmtypeid = $fieldval['fieldcolmtypeid'];
									echo fieldgenerate($fieldval,$i,$colmtypeid);
								}
							} else {
								//function call for fields generation
								if($fieldval['colname'] != "salutationid") {
									$colmtypeid = $fieldval['fieldcolmtypeid'];
									echo fieldgenerate($fieldval,$i,$colmtypeid);
								}
							}
							if($fieldval['uitypeid'] == 20) {
								$fieldsinfo = explode(',',$fieldval['ddparenttable']);
								if($fieldsinfo[1]!="") { $fieldnames[] = $fieldsinfo[1]; }
								if($fieldval['tabname']!="") { $fieldtables[] = $fieldval['tabname']; }
								if($fieldsinfo[1]!="") { $tabcolmnname[] = $fieldsinfo[1]; }
								if($fieldval['parenttable']!="") { $parenttabname[] = $fieldval['parenttable']; }
							}
							if($fieldval['fieldname']!="" && $fieldval['tabname']!="" && $fieldval['colname']!="" && $fieldval['parenttable']!="") {
								$fieldnames[] = $fieldval['fieldname'];
								$fieldtables[] = $fieldval['tabname'];
								$tabcolmnname[] = $fieldval['colname'];
								$parenttabname[] = $fieldval['parenttable'];
							}
							if($fieldval['uitypeid'] == 14) {
								$anfieldnames[] = $fieldval['fieldname'];
								$anfieldnameid[] = $fieldval['modfieldid'];
								$anfiledtab[] = $fieldval['tabname'];
								$anmoduleid[] = $moduleid;
							}
							if($fieldval['uitypeid'] == 15 || $fieldval['uitypeid'] == 16) {
								$attachfieldname[] = $fieldval['fieldname'];
								$attachcolname[] = $fieldval['colname'];
								$attachuitypeid[] = $fieldval['uitypeid'];
								$attachfieldid[] = $fieldval['modfieldid'];
								$fupload = "Yes";
							}
							if($fieldval['uitypeid'] == 24) {
								$editor = "Yes";
							}
							$readmode = "";
							$mandatoryopt = "";
							//$i++;
							if($fieldval['fieldmode'] != "No") {
								$i++;
							}
						}
						echo '<div class="large-12 columns">&nbsp;</div>';
						echo '</div>'; //tab section padding close
						echo '</div>'; //tab section close
					}
					$tgname = preg_replace('/[^A-Za-z0-9]/', '', $tabgrpname);
					$tabgrpname = strtolower($tgname);
					//Text Editor
					echo '<div class="large-8 columns paddingbtm texteditor-container">';
					echo '<div class="large-12 columns paddingzero">';
					echo '<div class="large-12 columns viewgridcolorstyle paddingzero borderstyle" id="templateeditor_div" style="height:560px;">';
					echo '<div class="large-12 columns headerformcaptionstyle editorpadding" style="padding: 0.2rem 0 0.4rem;">';
					echo '<span>'.ucfirst($fieldval['usertabgrpname']).' Editor</span>';
					echo '</div>';
					echo '<div class="large-12 columns paddingzero">';
					echo '<div class="large-12 column">&nbsp;</div>';
					echo "<div id='".$modname.$tabgrpname."_editor'>";
					echo "</div>";
					echo "<script>
					$(document).ready(function(){
						    if('".$modname.$tabgrpname."_editor' == 'printtemplatestemplatedesign_editor'){
							$.FroalaEditor.DefineIcon('addspan', {NAME: 'info'});
							$.FroalaEditor.DefineIcon('removespan', {NAME: 'info'});
							$.FroalaEditor.RegisterCommand('addspan', {
							title: 'addspan',
							focus: false,
							undo: false,
							refreshAfterCallback: false,
							callback: function() {
							 var string = $('#printtemplatestemplatedesign_editor').froalaEditor('html.getSelected');
							 var val=string.replace(/<[\/]{0,1}(p)[^><]*>/ig,'').replace(/[{}]/g, '');
							 var myString = (val.substr(val.toLowerCase().indexOf('cl:') + 3)).replace(/\s/g, ''); 
							 if(myString!='')
							{
								 
								$('#printtemplatestemplatedesign_editor').froalaEditor('format.apply', 'span', {id:myString,class: 'formulaclass'} );
								formula_array.push({'name':myString,'username':myString+'|'});
							}
							}
							});
							
							$.FroalaEditor.RegisterCommand('removespan', {
							title: 'removespan',
							focus: false,
							undo: false,
							refreshAfterCallback: false,
							callback: function() {
							 var string = $('#printtemplatestemplatedesign_editor').froalaEditor('html.getSelected');
							 var val=string.replace(/<[\/]{0,1}(p)[^><]*>/ig,'');
							 var myString = (val.substr(val.toLowerCase().indexOf('cl:') + 3)).replace(/\s/g, '');
							 if(myString!='')
							{
								$('#printtemplatestemplatedesign_editor').froalaEditor('format.remove', 'span', {id:myString,class: 'formulaclass' });
								 formula_array.pop({'name':myString,'username':myString+'|'});
							}
							}
							});
							}
							$('#".$modname.$tabgrpname."_editor').froalaEditor({inlineMode: false, alwaysBlank: true,toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'fontFamily', 'fontSize', '|', 'color', 'emoticons', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent','insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '|', 'quote', 'insertHR', 'undo', 'redo', 'clearFormatting', 'selectAll','help', 'html','addspan','removespan']});
					        
				});
					</script> ";
					echo '</div>';
					echo '</div>';
					echo '<div class="large-12 columns paddingzero" id="texteditor_div" style="display:none;">';
					echo '<div class="large-12 column">&nbsp;</div>';
					echo '<div class="large-8 columns paddingbtm texteditor-container">
							<div class="large-12 columns paddingzero borderstyle cleardataform">
								<div class="large-12 columns viewgridcolorstyle" style="padding-left:0;padding-right:0;margin-top: 0px !important;">
									<div class="large-12 columns headerformcaptionstyle" style="padding: 6px 0px 0px 13px;">
									<span>Tag Editor</span>
									</div>
									<div class="large-12 columns" style="padding-left:0;padding-right:0;height:450px;background-color:#fff;">
										<textarea class="" name="tageditor"  id="tageditor" rows="20" cols="40" tabindex="118" spellcheck="false" data-validation-engine="" data-errormessage="please enter printers prn format" style="height:450px;width:100% !important;"></textarea>
									</div>
								</div>
							</div>
						</div>';
					echo '</div>';
					echo '</div>';
					echo '<div class="large-4 columns">&nbsp;</div>';
					echo '</div>';
					echo '</div>';
					echo '<div class="row">&nbsp;</div>';
					echo '<div class="row">&nbsp;</div>';
					$editornames[] = $modname.$tabgrpname.'_editor';
					echo "</div>";//add form container close
					$editor = "Yes";
				}
			}
			echo "</div>"; //tab group close
		}
		//hidden text box contain data insert informations
		$fieldname = dataexplode($fieldnames);
		$fieldtable = dataexplode($fieldtables);
		$tabcolname = dataexplode($tabcolmnname);
		$partabname = dataexplode($parenttabname);
		$grdname = dataexplode($gridname);
		$editorname = dataexplode($editornames);
		$griddatapartabname = dataexplode($grdpartabname);
		$anfieldname = dataexplode($anfieldnames);
		$anfieldnameids = dataexplode($anfieldnameid);
		$anmoduleids = dataexplode($anmoduleid);
		$anmoduletable = dataexplode($anfiledtab);
		$attachfldname = dataexplode($attachfieldname);
		$attachcolname = dataexplode($attachcolname);
		$attachuitype = dataexplode($attachuitypeid);
		$attachfieldid = dataexplode($attachfieldid);	
		
		echo hidden('elementsname',$fieldname);
		echo hidden('elementstable',$fieldtable);
		echo hidden('elementscolmn',$tabcolname);
		echo hidden('elementspartabname',$partabname);
		echo hidden('gridnameinfo',$grdname);
		echo hidden('griddatapartabnameinfo',$griddatapartabname);
		echo hidden('editornameinfo',$editorname);
		echo hidden('anfieldnameinfo',$anfieldname);
		echo hidden('anfieldnameidinfo',$anfieldnameids);
		echo hidden('anfieldnamemodinfo',$anmoduleids);
		echo hidden('anfieldtabinfo',$anmoduletable);
		echo hidden('attachfieldnames',$attachfldname);
		echo hidden('attachcolnames',$attachcolname);
		echo hidden('attachuitypeids',$attachuitype);
		echo hidden('attachfieldids',$attachfieldid);
		if($editor == "Yes") {
			froalaeditorfileslinks();			
		}
		if($fupload == "Yes") {
		echo '<script src="'.base_url().'js/plugins/dropbox/dropbox.js" type="text/javascript" id="dropboxjs" data-app-key="0a1wlbnnevlhces"></script>';
		}
	}
	//form fields generation based on fields with grid
	
	

	
	function formfieldsgeneratorwithgrid($modtabsecfrmfkdsgrp,$filedmodids = null,$gridtype=null) {
		//grouping tab group value
		$tempgrplevel='0';
		$newgrpkey=0;
		$tabmodule[]=array();
		 foreach ($modtabsecfrmfkdsgrp as $key => $val) {
		 	$menuname = $val['menuname'];
			if($tempgrplevel == ''.$val['modulename'].'') {
				$tabmodule[$tempgrplevel][$newgrpkey]=$val;
			} else {
				$tabmodule[''.$val['modulename'].''][$newgrpkey]=$val;
			}
			$newgrpkey++;
		}
		$tabsectiongrp = array();
		if(count($tabmodule) > 0) {
			//group tab section within tab group
			$tempgrpseclevel=0;
			$newgrpseckey=0;
			foreach($tabmodule as $key => $value) {
				foreach($value as $key1 => $val) {
					if($tempgrpseclevel==$val['tabsectionid']) {
						$tabsectiongrp[$key][$tempgrpseclevel][$newgrpseckey]=$val;
					} else {
						$tabsectiongrp[$key][$val['tabsectionid']][$newgrpseckey]=$val;
					}
					$newgrpseckey++; 
				}
			}
		}
		//create form section and elements
		$tabgrpid = 1;
		$prevgrpid = "";
		$data = array();
		
		foreach($tabsectiongrp as $grpkey => $grpvalarr) {
			$gridname = array();
			$fieldnames = array();
			$fieldtables = array();
			$tabcolmnname = array();
			$parenttabname = array();
			$anfieldnames = array();
			$anfieldnameid = array();
			$anmoduleid = array();
			$anfiledtab = array();
			$attachfieldname = array();
			$attachcolname = array();
			$attachuitypeid = array();
			$attachfieldid = array();
			$fupload = 'No';
			$editor = 'No';
			$str = preg_replace('/[^A-Za-z0-9]/', '', $grpkey);
			$modname = strtolower($str);
			if($tabgrpid == 1) {
			//Inner grid generation
			$CI =& get_instance();
			$device = $CI->Basefunctions->deviceinfo();
			echo '<div class="">';
				$CI =& get_instance();
				$marray = array($filedmodids[0]);
				$filtertabgroup = $CI->Basefunctions->moduletabgroupdetailsfetch($marray);
				$filtertabsecfield = $CI->Basefunctions->moduletabsectiondetailsfetch($marray);
				$filtercount = count($filtertabgroup);
				$content = '';
				//echo '</div>';
			if($device=='phone') {
					echo '<div class="large-12 columns innergridpaddingbtm fullgridview">';
					echo '<div class="large-12 columns paddingzero">';
					echo '<div class="large-12 columns viewgridcolorstyle" style="padding-left:0;padding-right:0;">';
					echo '<div class="large-12 columns forgetinggridname borderstyle" id="'.$modname.'addgridwidth" style="padding-left:0;padding-right:0;">';
					if($gridtype=='grid') {
						echo '<table id="'.$modname.'addgrid"> </table> ';
						echo '<div id="'.$modname.'addgridnav" class="w100"></div> ';
					} else {
						echo '<div class="desktop row-content inner-gridcontent" id="'.$modname.'addgrid" style="top:0px;">';
						echo '</div>';
						echo '<div class="footercontainer footer-content" id="'.$modname.'addgridfooter">';
						echo '</div>';
					}
					echo '</div>';
				} else {
					echo '<div class="large-12 medium-12 columns paddingzero fullgridview">';
					echo '<div class="touchgrid-container viewgridcolorstyle borderstyle" style="margin: 10px 10px 7px 15px;" id="'.$modname.'addgridwidth">';
					if($gridtype=='grid') {
						echo '<table id="'.$modname.'addgrid"> </table> ';
						echo '<div id="'.$modname.'addgridnav" class="w100"></div> ';
					} else {
						echo '<div class="desktop row-content" id="'.$modname.'addgrid">';
						echo '</div>';
						echo '<div class="footercontainer footer-content" id="'.$modname.'addgridfooter">';
						echo '</div>';
					}
					echo '</div>';
				}
				echo '</div>';
				//echo '<div class="large-3 medium-3 columns paddingzero">
				
				echo '<div class="large-12 small-12 medium-12 columns" style="position:absolute;">
				<div class="overlay large-12 columns mblnopadding" id="overlayfilterdisplaymainview" style="z-index:40;height: 100% !important;">	
				<div class="large-4 large-offset-8 columns" style="padding-right: 0em !important;height:100% !important;padding-left:0px !important;padding: 0px !important;height: 100% !important;">
				<div class="large-12 column paddingzero" style="position: relative;left: 4px;height: 100% !important;">
				<div id="overlayfilterdisplaymainviewformvalidate" class="validationEngineContainer" style="height: 100% !important;">
				<div class="large-12 columns end" style="padding: 0px !important;height: 100% !important;">
				<div class="large-12 columns cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff; height: 100% !important;">
				
				<div class="searchfilteroverlay filterbox ui-draggable borderstylefilter" style="margin: 0px 10px 10px 0px; touch-action: none; -moz-user-select: none;"><div class="large-12 columns noticombowidget-box filterheaderborderstyle">
						<div class="large-12 medium-12 columns end activeheader filertabbgcolor" id="filterview" style="padding-top:0.3rem!important;padding-bottom:0.5rem!important;text-align: left;" data-griddataid="'.$modname.'addgrid">
							<span id="fiteractive" class="tab-titlemini active filtermini">
						<span style="left:5px;position:relative;font-size:1.2rem;font-family:Nunito semibold, sans-serif;font-weight: 100;">Filter Panel </span>
						<input type="button" class="alertbtnno addsectionclose leftcloseposition filterdisplaymainviewclose" value="Close" tabindex="130" style="left: 27em !important;"></span></div>			
							</div>';
				echo '<div class="filterview1">
				<div class="row">&nbsp;</div>
				<div class="large-12 medium-12 small-12 columns end filterheight filterbodyborderstyle" id="fbuildercat1" >
				<div class="wrappercontent">
				<ul class="fbulderheadermenu">
				<span id="'.$modname.'filterformconditionvalidation" class="validationEngineContainer">';
				for($fl=0;$fl<$filtercount;$fl++) {
					$content .='<div class= elementlist'.$fl.'" style="">
									<ul class="elementiconsul" style="padding-left: 1rem;padding-right: 1rem;">
									<div id=""><label>'.$filtertabgroup[$fl]['tabsecname'].'<span class="mandatoryfildclass">*</span></label><select data-placeholder="Select Columns" data-prompt-position="topLeft:14,36" id="'.$modname.'filtercolumn" class="validate[required] chzn-select dropdownchange multicolumnfilter" name="'.$modname.'filtercolumn" tabindex="9"><option></option>';
					$prev = " ";
					foreach($filtertabsecfield as $key) {
						if($filtertabgroup[$fl]['tabsecid'] == $key->moduletabsectionid){
							$cur = $key->moduletabsectionid;
							$content .='<option value="'.$key->viewcreationcolumnid.'" data-indexname="'.$key->viewcreationcolmodelindexname.'" data-uitype="'.$key->uitypeid.'" data-modname="'.$modname.'" data-label="'.$key->viewcreationcolumnname.'" data-moduleid="'.$key->viewcreationmoduleid.'" data-fldview="'.$key->fieldview.'" data-fldrestrict="'.$key->fieldrestrict.'">'.$key->viewcreationcolumnname.'</option>';
						}
					}
					$content .='</select></div></ul></div>';
				}
				$content .='<div class="large-12 columns viewcondclear" style="color:black !important;">
								<label>Condition <span class="mandatoryfildclass">*</span></label>
								<select data-placeholder="" data-prompt-position="bottomLeft:14,36" class="validate[required] chzn-select chosenwidth " tabindex="11" name="'.$modname.'filtercondition" id="'.$modname.'filtercondition">
								<option></option>
								<option value="Equalto">Equalto</option>
								<option value="NotEqual">NotEqual</option>
								<option value="Startwith">Startwith</option>
								<option value="Endwith">Endwith</option>
								<option value="Middle">Middle</option>
								</select>
								</div>
								<div class="input-field large-12 columns viewcondclear " id="'.$modname.'filtercondvaluedivhid">
								<input type="text" class="validate[required,maxSize[100]]" id="'.$modname.'filtercondvalue" name="'.$modname.'filtercondvalue"  tabindex="12" >
								<label for="'.$modname.'filtercondvalue">Value <span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="large-12 columns viewcondclear" id="'.$modname.'filterddcondvaluedivhid">
								<label>Value<span class="mandatoryfildclass">*</span></label>
								<select data-placeholder="" data-prompt-position="bottomLeft:14,36" class="validate[required] chzn-select chosenwidth valdropdownchange" tabindex="13" name="'.$modname.'filterddcondvalue" id="'.$modname.'filterddcondvalue">
								<option></option>
								</select>
								</div>
								<div class="large-12 columns viewcondclear" id="'.$modname.'filterdatecondvaluedivhid">
								<label>Value <span class="mandatoryfildclass">*</span></label>
								<input type="text" class="validate[required]" data-dateformater="<?php echo $appdateformat; ?>" id="'.$modname.'filterdatecondvalue" name="'.$modname.'filterdatecondvalue" tabindex="12" >
								</div>
								<div class="large-12 columns viewcondclear">
								<input type="hidden" class="" id="'.$modname.'finalfiltercondvalue" name="'.$modname.'finalfiltercondvalue"  tabindex="12" >
								<input type="hidden" class="" id="'.$modname.'filterfinalviewconid" name="'.$modname.'filterfinalviewconid"  tabindex="12" >
								</div>
								<div class="large-12 columns">&nbsp;</div>
								<div class="large-12 columns text-center">
								<input type="button" style="width:16%" class="btn formbuttonsalert" id="'.$modname.'filteraddcondsubbtn" Value="Submit" name="'.$modname.'filteraddcondsubbtn"  tabindex="14" >
								</div>
								<div class="large-12 columns">&nbsp;</div></span>
								<div class="large-12 columns">
								<div id="'.$modname.'filterconddisplay" class="large-12 medium-12 small-12 columns uploadcontainerstyle uploadcontainerstyle1">
								</div>
								<input type="hidden" id="'.$modname.'conditionname" value="" name="'.$modname.'conditionname">
								<input type="hidden" id="'.$modname.'filterid" value="" name="'.$modname.'filterid">
								<input type="hidden" id="'.$modname.'filtervalue" value="" name="'.$modname.'filtervalue">
								</div>
								<div class="large-12 columns">&nbsp;</div></span>';
				$content .='</div></div></div></div>';
				echo $content;
echo ' <div id="textenterdiv" class="mininotesview hidedisplay large-12 medium-12 small-12 columns filterbodyborderstyle mininotesview1"><div class="large-12 columns paddingzero"><div class="large-12 columns paddingzero" style="text-align:left;cursor:pointer"><input id="unique_rowid" type="hidden"></div><div class="large-12 columns hidedisplay titleinputspan">&nbsp;</div><div class="large-12 columns paddingzero"><div class="atsearchdiv hidedisplay"></div><div class="large-12 columns paddingzero atsearchdiv1"><div class="large-12 columns fiterconversbar paddingzero"><span id="convmulitplefileuploader"></span><span class="uploadattachments attachbtnstyle" title="Attachment" style="cursor:pointer;"><i class="material-icons">attachment</i></span><textarea class="mention messagetoenterstyle" rows="2" placeholder="Enter Your Message Here..." id="homedashboardconvid" name="homedashboardconvid"></textarea><span class="sendbtnstyle" id="postsubmit"><i class="material-icons">send</i></span><span class="sendbtnstyle hidedisplay" id="editpostsubmit"><i class="material-icons">send</i></span><span class="convfileupload hidedisplay" style="position:relative;top:0.1rem;"><!--<span id="c_fileid" class="attachmentname" data-filepath="">--></span><input id="cc_filename" type="hidden"><input id="cc_filepath" type="hidden"></div><div class="large-6 medium-6  small-6 columns" style="text-align:right;"><!--<span><select class="chzn-select" style="width:35%" id="c_privacy" "><option value="1">Public</option><option value="2">OnlyMe</option></select></span><span class="postbtnstyle" id="postsubmit">Post</span><span class="postbtnstyle hidedisplay" id="editpostsubmit">Post</span><span class="postbtnstyle hidedisplay" id="postcancel">Cancel</span>--></div><input type="hidden" name="minimoduleid" id="minimoduleid" value=""/><input type="hidden" name="minicommonid" id="minicommonid" value=""/> </div></div></div><div class="large-12 columns scrollbarclass"  style="height: 32rem;overflow-x:hidden"><div class="clearfix"></div><ul id="noteswidget" class="member-list"></ul></div><div class="large-12 columns">&nbsp;</div></div>';
				echo '</div></div></div></div></div></div>'; //All div close here after first div
				echo '</div>'; // first div Close here
				echo '</div>';
				echo '</div>';
				echo '<div class="hidedisplay" id="viewcreationformdiv"></div>';
				echo '<div class="hiddensubform" id="subformspan'.$tabgrpid.'">'; //tab group creation (1 st group)
				echo '<div class="large-12 columns addformunderheader">&nbsp;</div>';//div for Empty space
				echo '<div class="large-12 columns scrollbarclass addformcontainer singlesectionaddform">';//div for container
				echo '<div class="closed effectbox-overlay effectbox-default" id="groupsectionoverlay">';
				echo '<div class="large-12 columns mblnopadding" style="padding-left:0px !important;">';
				echo '<form method="POST" name="'.$modname.'addform" class="" action ="" id="'.$modname.'addform" style="height:100% !important;">';
				echo '<span id="'.$modname.'formaddwizard" class="validationEngineContainer" >';
				echo '<span id="'.$modname.'formeditwizard" class="validationEngineContainer">';
				$tabgrpid++;
			} else {
				echo '<div class="hidedisplay hiddensubform" id="subformspan'.$tabgrpid.'">'; //tab group creation (other group st group)
				echo '<div class="large-12 columns addformunderheader">&nbsp;</div>';//div for Empty space
				echo '<div class="large-12 columns scrollbarclass addformcontainer">';//div for container
				echo '<div class="large-3 medium-4 columns paddingzero filterview hidedisplay">';
				echo '</div>';
				echo '<div class="closed effectbox-overlay effectbox-default" id="'.$modname.'groupsectionoverlay">';
				echo '<div class="large-12 columns">';
				echo '<div class="row">&nbsp;</div>';
				echo '<form method="POST" name="'.$modname.'addform" class="" action ="" id="'.$modname.'addform">';
				echo '<span id="'.$modname.'formaddwizard" class="validationEngineContainer" >';
				echo '<span id="'.$modname.'formeditwizard" class="validationEngineContainer">';
				$tabgrpid++;
			}
			$prevgrpsecid = "";
			$i = 100;
			foreach($grpvalarr as $seckey => $secvalarr) { //section creation 
				echo '<div class="large-4 large-offset-8 columns" style="padding-right: 0em !important;height:100% !important;padding-left:0px !important;">'; //tab section open
				foreach($secvalarr as $fieldkey => $fieldval) { //form field creation
					$curgrpsecid = $fieldval['tabsectionid'];
					if($prevgrpsecid != $curgrpsecid) {
						$prevgrpsecid=$fieldval['tabsectionid'];
						echo '<div class="large-12 columns cleardataform border-modal-8px" style="padding-left: 0 !important; padding-right: 0 !important;">';//tab section padding open
						echo '<div class="large-12 columns sectionheaderformcaptionstyle">'.$fieldval['tabsectionname'].'</div>';
						echo '<div class="large-12 columns sectionpanel">';
						//function call for fields generation
					   if($fieldval['colname'] != "salutationid") {
							$colmtypeid = $fieldval['fieldcolmtypeid'];
							echo fieldgenerate($fieldval,$i,$colmtypeid);
						}
					} else {
						//function call for fields generation
						if($fieldval['colname'] != "salutationid") {
							$colmtypeid = $fieldval['fieldcolmtypeid'];
							echo fieldgenerate($fieldval,$i,$colmtypeid);
						}
					}
					if($fieldval['uitypeid'] == 20) {
						$fieldsinfo = explode(',',$fieldval['ddparenttable']);
						if($fieldsinfo[1]!="") { $fieldnames[] = $fieldsinfo[1]; }
						if($fieldval['tabname']!="") { $fieldtables[] = $fieldval['tabname']; }
						if($fieldsinfo[1]!="") { $tabcolmnname[] = $fieldsinfo[1]; }
						if($fieldval['parenttable']!="") { $parenttabname[] = $fieldval['parenttable']; }
					}
					if($fieldval['fieldname']!="" && $fieldval['tabname']!="" && $fieldval['colname']!="" && $fieldval['parenttable']!="") { 
						$fieldnames[] = $fieldval['fieldname'];
						$fieldtables[] = $fieldval['tabname'];
						$tabcolmnname[] = $fieldval['colname'];
						$parenttabname[] = $fieldval['parenttable'];
					}
					if($fieldval['uitypeid'] == 14) {
						$anfieldnames[] = $fieldval['fieldname'];
						$anfieldnameid[] = $fieldval['modfieldid'];
						$anfiledtab[] = $fieldval['tabname'];
						$anmoduleid[] = $filedmodids[0];
					}
					if($fieldval['uitypeid'] == 15 || $fieldval['uitypeid'] == 16) {
						$attachfieldname[] = $fieldval['fieldname'];
						$attachcolname[] = $fieldval['colname'];
						$attachuitypeid[] = $fieldval['uitypeid'];
						$attachfieldid[] = $fieldval['modfieldid'];
						$fupload = "Yes";
					}
					if($fieldval['uitypeid'] == 24) {
						$editor = "Yes";
					}
					$readmode = "";
					$mandatoryopt = "";
					if($fieldval['fieldmode'] != "No") {
						$i++;
					}
					//$i++;
				}
				echo '</div>';
				echo '<div class="divider large-12 columns"></div>';
				echo '<div class="large-12 columns submitkeyboard sectionalertbuttonarea"  style="text-align: right">';
				//update button
				$butattr = array('class'=>'hidedisplayfwg updatekeyboard alertbtnyes','value'=>'Save','tabindex'=>$i++);
				echo button(''.$modname.'dataupdatesubbtn',$butattr);
				//Add button
				$butattr = array('class'=>'addkeyboard alertbtnyes','value'=>'Save','tabindex'=>$i++);
				echo button(''.$modname.'savebutton',$butattr);
				//Cancel button
				$butattr = array('class'=>'alertbtnno addsectionclose leftcloseposition','value'=>'Close','tabindex'=>$i++);
				echo button('',$butattr);
				echo close('div');
				
				echo '</div>'; //tab section padding close
				echo '</div>'; //tab section close
			}
			echo '</span>';
			echo '</span>';
			//hidden text box contain data insert informations
			$fieldname = dataexplode($fieldnames);
			$fieldtable = dataexplode($fieldtables);
			$tabcolname = dataexplode($tabcolmnname);
			$partabname = dataexplode($parenttabname);
			$anfieldname = dataexplode($anfieldnames);
			$anfieldnameids = dataexplode($anfieldnameid);
			$anmoduleids = dataexplode($anmoduleid);
			$anmoduletable = dataexplode($anfiledtab);
			$attachfldname = dataexplode($attachfieldname);
			$attachcolname = dataexplode($attachcolname);
			$attachuitype = dataexplode($attachuitypeid);
			$attachfieldid = dataexplode($attachfieldid);
			
			echo hidden(''.$modname.'elementsname',$fieldname);
			echo hidden(''.$modname.'elementstable',$fieldtable);
			echo hidden(''.$modname.'elementscolmn',$tabcolname);
			echo hidden(''.$modname.'elementspartabname',$partabname);
			echo hidden(''.$modname.'primarydataid','');
			echo hidden(''.$modname.'sortcolumn','');
			echo hidden(''.$modname.'sortorder','');
			echo hidden(''.$modname.'anfieldnameinfo',$anfieldname);
			echo hidden(''.$modname.'anfieldnameidinfo',$anfieldnameids);
			echo hidden(''.$modname.'anfieldnamemodinfo',$anmoduleids);
			echo hidden(''.$modname.'anfieldtabinfo',$anmoduletable);
			echo hidden(''.$modname.'attachfieldnames',$attachfldname);
			echo hidden(''.$modname.'attachcolnames',$attachcolname);
			echo hidden(''.$modname.'attachuitypeids',$attachuitype);
			echo hidden(''.$modname.'attachfieldids',$attachfieldid);
			//hidden text box view columns field module ids
			$val = implode(',',$filedmodids);
			echo hidden(''.$modname.'viewfieldids',$val);
			echo hidden('viewfieldids',$val);
			echo hidden('moduleid',$val);
			echo '</form>';
			echo '</div>';
			echo '</div>';
			echo '</div>';
			//echo '<div class="large-4 columns">&nbsp;</div>';
			echo '</div>';
			echo '</div>';
			echo '<div class="row">&nbsp;</div>';
			echo "</div>";//add form container close
			
			echo "</div>"; //tab group close
			if($editor == "Yes") {
				froalaeditorfileslinks();
			}
			if($fupload == "Yes") {
				echo '<script src="'.base_url().'js/plugins/dropbox/dropbox.js" type="text/javascript" id="dropboxjs" data-app-key="0a1wlbnnevlhces"></script>';
			}
		}
	}
	//form fields generation for dashboard - multi module cocnept
	function dashboardformfieldsgeneratorwithgrid($modtabsecfrmfkdsgrp,$tabgrpid,$modid,$gridtype=null) {
		//grouping tab group value
		$tempgrplevel='dashboard';
		$newgrpkey=0;
		$tabmodule[]=array();
		$modid = explode(',',$modid);
		foreach ($modtabsecfrmfkdsgrp as $key => $val) {
			if($tempgrplevel == ''.$val['modulename'].'') {
				$tabmodule[$tempgrplevel][$newgrpkey]=$val;
			} else {
				$tabmodule[''.$val['modulename'].''][$newgrpkey]=$val;
			}
			$newgrpkey++;
		}
		$tabsectiongrp = array();
		if(count($tabmodule) > 0) {
			//group tab section within tab group
			$tempgrpseclevel=0;
			$newgrpseckey=0;
			foreach($tabmodule as $key => $value) {
				foreach($value as $key1 => $val) {
					if($tempgrpseclevel==$val['tabsectionid']) {
						$tabsectiongrp[$key][$tempgrpseclevel][$newgrpseckey]=$val;
					} else {
						$tabsectiongrp[$key][$val['tabsectionid']][$newgrpseckey]=$val;
					}
					$newgrpseckey++; 
				}
			}
		}
		//create form section and elements
		$tabgrpid = $tabgrpid;
		$prevgrpid = "";
		$data = array();
		$multimoduleid = '';
		$k=0;
		foreach($tabsectiongrp as $grpkey => $grpvalarr) {
			$fieldnames = array();
			$fieldtables = array();
			$tabcolmnname = array();
			$parenttabname = array();
			$anfieldnames = array();
			$anfieldnameid = array();
			$anmoduleid = array();
			$anfiledtab = array();
			$attachfieldname = array();
			$attachcolname = array();
			$attachuitypeid = array();
			$attachfieldid = array();
			$editor = "No";
			$fupload = "No";
			$str = str_replace(" ","",$grpkey);
			$modname = strtolower($str);
			$multimoduleid = $modid[$k];
			$CI =& get_instance();
			$device = $CI->Basefunctions->deviceinfo();
			//action generation
			if($device != 'phone') {
				$size = 10;
				$m=0;
				$menumore = '';
				$value = array();
				$des = array();
				$title = array();
				$CI =& get_instance();
				$actionassign = $CI->Basefunctions->actionassign($multimoduleid);
				foreach($actionassign as $key):
				$value[$key->toolbarnameid] = $key->toolbarname;
				$des[] = $key->description;
				$title[] = $key->toolbartitle;
				endforeach;
				echo '<ul id="'.$modname.'advanceddrop" class="singleaction-drop arrow_box">';
				foreach($value as $key => $value) {
					$fname = preg_replace('/[^A-Za-z0-9]/', '', $value);
					$name = strtolower($fname);
					if($name != 'add'){
						$menumore .= '<li id="'.$modname.$name.'icon"><span class="icon-box"><i class="icon24 '.$des[$m].' '.$name.'iconclass " title="'.$title[$m].'"></i>'.$title[$m].'</span></li>';
					}
					$m++;
				}
				echo $menumore;
				echo "</ul>";
			}
			
			if($tabgrpid == 1) {
				echo '<div class="dbhiddensubform transitionhiddenform" id="dbsubformspan'.$tabgrpid.'">'; //tab group creation (1 st group)
				echo '<div class="large-12 columns addformunderheader">&nbsp;</div>';//div for Empty space
				echo '<div class="large-12 columns scrollbarclass addformcontainer">';//div for container
				$CI =& get_instance();
				$filtertabgroup = $CI->Basefunctions->formmoduletabgroupdetailsfetch($modid[$k]);
				$filtertabsecfield = $CI->Basefunctions->formmoduletabsectiondetailsfetch($modid[$k]);
				$filtercount = count($filtertabgroup);
				$content ='';
				echo '<div class="closed effectbox-overlay effectbox-default" id="'.$modname.'sectionoverlay">';
				echo '<div class="large-12 columns mblnopadding">';
				echo '<form method="POST" name="'.$modname.'addform" class="" action ="" id="'.$modname.'addform" style="height:100% !important;">';
				echo '<span id="'.$modname.'formaddwizard" class="validationEngineContainer" >';
				echo '<span id="'.$modname.'formeditwizard" class="validationEngineContainer">';
				$tabgrpid++;
				$k++;
			} else {
				echo '<div class="dbhiddensubform hidedisplay transitionhiddenform" id="dbsubformspan'.$tabgrpid.'">'; //tab group creation (1 st group)
				echo '<div class="large-12 columns addformunderheader">&nbsp;</div>';//div for Empty space
				echo '<div class="large-12 columns scrollbarclass addformcontainer">';//div for container
				$CI =& get_instance();
				$filtertabgroup = $CI->Basefunctions->formmoduletabgroupdetailsfetch($modid[$k]);
				$filtertabsecfield = $CI->Basefunctions->formmoduletabsectiondetailsfetch($modid[$k]);
				$filtercount = count($filtertabgroup);
				$content ='';
				echo '<div class="closed effectbox-overlay effectbox-default" id="'.$modname.'sectionoverlay">';
				echo '<div class="large-12 columns mblnopadding" style="padding-left:0px !important;">';
				echo '<form method="POST" name="'.$modname.'addform" class="" action ="" id="'.$modname.'addform" style="height:100% !important;">';
				echo '<span id="'.$modname.'formaddwizard" class="validationEngineContainer" >';
				echo '<span id="'.$modname.'formeditwizard" class="validationEngineContainer">';
				$tabgrpid++;
				$k++;
			}
			$prevgrpsecid = "";
			$i = 100;
			foreach($grpvalarr as $seckey => $secvalarr) { //section creation 
				echo '<div class="large-4 large-offset-8 columns paddingbtm" style="padding-right: 0em !important;padding-left:0px !important;">';
				foreach($secvalarr as $fieldkey => $fieldval) { //form field creation
					$curgrpsecid = $fieldval['tabsectionid'];
					if($prevgrpsecid != $curgrpsecid) {
						$prevgrpsecid=$fieldval['tabsectionid'];
						echo '<div class="large-12 columns cleardataform border-modal-8px z-depth-5" style="padding-left: 0 !important; padding-right: 0 !important;">';//tab section padding open
						echo '<div class="large-12 columns sectionheaderformcaptionstyle">'.$fieldval['tabsectionname'].'</div>';
						echo '<div class="large-12 columns sectionpanel">';
						//function call for fields generation
					   if($fieldval['colname'] != "salutationid") {
							$colmtypeid = $fieldval['fieldcolmtypeid'];
							echo fieldgenerate($fieldval,$i,$colmtypeid);
						}
					} else {
						//function call for fields generation
						if($fieldval['colname'] != "salutationid") {
							$colmtypeid = $fieldval['fieldcolmtypeid'];
							echo fieldgenerate($fieldval,$i,$colmtypeid);
						}
					}
					if($fieldval['uitypeid'] == 20) {
						$fieldsinfo = explode(',',$fieldval['ddparenttable']);
						if($fieldsinfo[1]!="") { $fieldnames[] = $fieldsinfo[1]; }
						if($fieldval['tabname']!="") { $fieldtables[] = $fieldval['tabname']; }
						if($fieldsinfo[1]!="") { $tabcolmnname[] = $fieldsinfo[1]; }
						if($fieldval['parenttable']!="") { $parenttabname[] = $fieldval['parenttable']; }
					}
					if($fieldval['fieldname']!="" && $fieldval['tabname']!="" && $fieldval['colname']!="" && $fieldval['parenttable']!="") { 
						$fieldnames[] = $fieldval['fieldname'];
						$fieldtables[] = $fieldval['tabname'];
						$tabcolmnname[] = $fieldval['colname'];
						$parenttabname[] = $fieldval['parenttable'];
					}
					if($fieldval['uitypeid'] == 14) {
						$anfieldnames[] = $fieldval['fieldname'];
						$anfieldnameid[] = $fieldval['modfieldid'];
						$anfiledtab[] = $fieldval['tabname'];
						$anmoduleid[] = $moduleid;
					}
					if($fieldval['uitypeid'] == 15 || $fieldval['uitypeid'] == 16) {
						$attachfieldname[] = $fieldval['fieldname'];
						$attachcolname[] = $fieldval['colname'];
						$attachuitypeid[] = $fieldval['uitypeid'];
						$attachfieldid[] = $fieldval['modfieldid'];
						$fupload = "Yes";
					}
					if($fieldval['uitypeid'] == 24) {
						$editor = "Yes";
					}
					$readmode = "";
					$mandatoryopt = "";
					if($fieldval['fieldmode'] != "No") {
						$i++;
					}
					//$i++;
				}
				echo '</div>';
				echo '<div class="divider large-12 columns"></div>';
				echo '<div class="large-12 columns submitkeyboard sectionalertbuttonarea"  style="text-align: right">';
				//update button
				$butattr = array('class'=>'hidedisplayfwg updatekeyboard alertbtnyes','value'=>'Save','tabindex'=>$i++);
				echo button(''.$modname.'dataupdatesubbtn',$butattr);
				//Add button
				$butattr = array('class'=>'addkeyboard alertbtnyes','value'=>'Save','tabindex'=>$i++);
				echo button(''.$modname.'savebutton',$butattr);
				//Cancel button
				$butattr = array('class'=>'alertbtnno addsectionclose leftcloseposition','value'=>'Close','tabindex'=>$i++);
				echo button('',$butattr);
				echo close('div');
				
				echo '</div>'; //tab section padding close
				echo '</div>'; //tab section close
				$modulelink = $fieldval['modlink'];
			}
			echo '</span>';
			echo '</span>';
			//hidden text box contain data insert informations
			$fieldname = dataexplode($fieldnames);
			$fieldtable = dataexplode($fieldtables);
			$tabcolname = dataexplode($tabcolmnname);
			$partabname = dataexplode($parenttabname);
			$anfieldname = dataexplode($anfieldnames);
			$anfieldnameids = dataexplode($anfieldnameid);
			$anmoduleids = dataexplode($anmoduleid);
			$anmoduletable = dataexplode($anfiledtab);
			$attachfldname = dataexplode($attachfieldname);
			$attachcolname = dataexplode($attachcolname);
			$attachuitype = dataexplode($attachuitypeid);
			$attachfieldid = dataexplode($attachfieldid);
			
			echo hidden(''.$modname.'elementsname',$fieldname);
			echo hidden(''.$modname.'elementstable',$fieldtable);
			echo hidden(''.$modname.'elementscolmn',$tabcolname);
			echo hidden(''.$modname.'elementspartabname',$partabname);
			echo hidden(''.$modname.'primarydataid','');
			echo hidden(''.$modname.'sortcolumn','');
			echo hidden(''.$modname.'sortorder','');
			echo hidden(''.$modname.'anfieldnameinfo',$anfieldname);
			echo hidden(''.$modname.'anfieldnameidinfo',$anfieldnameids);
			echo hidden(''.$modname.'anfieldnamemodinfo',$anmoduleids);
			echo hidden(''.$modname.'anfieldtabinfo',$anmoduletable);
			echo hidden(''.$modname.'attachfieldnames',$attachfldname);
			echo hidden(''.$modname.'attachcolnames',$attachcolname);
			echo hidden(''.$modname.'attachuitypeids',$attachuitype);
			echo hidden(''.$modname.'attachfieldids',$attachfieldid);
			echo hidden(''.$modname.'viewfieldids',$multimoduleid);
			echo '</form>';
			echo '</div>';
			echo '</div>';
			$ds = DIRECTORY_SEPARATOR;
			$nl = explode('/',$modulelink);
			echo '<script src="'.base_url().'js'.'/'.$nl[0].'/'.strtolower($nl[0]).'.js" type="text/javascript"></script>';
			
			//Inner grid generation
			if($device=='phone') {
				echo '<div class="large-12 columns innergridpaddingbtm '.$modname.'fullgridview">';
				echo '<div class="large-12 columns paddingzero">';
				echo '<div class="large-12 columns viewgridcolorstyle borderstyle" style="padding-left:0;padding-right:0;">';
				echo '<div class="large-12 columns headerformcaptionstyle" style="height:auto;line-height:1.8rem;padding: 0.2rem 0 0rem;">';
				echo '<span class="large-6 medium-6 small-6 columns lefttext">'.$grpkey.' List<span class="fwgpaging-box '.$modname.'gridfootercontainer"></span></span>';
				$spattr = array("class"=>"large-6 medium-6 small-6 columns innergridicon innergridiconhover  righttext");
				echo spanopen($spattr);
				//action generation
				$size = 10;
				$value = array();
				$des = array();
				$title = array();
				$CI =& get_instance();
				$actionassign = $CI->Basefunctions->actionassign($fieldval['moduleid']);
				foreach($actionassign as $key):
				$value[$key->toolbarnameid] = $key->toolbarname;
				$des[] = $key->description;
				$title[] = $key->toolbartitle;
				endforeach;
				echo actionmenu($value,$size,$des,$title,strtolower($nl[0]),'icon24 ');
				echo '<span id="'.$modname.'viewtoggle" class="" title="View Toggle"><i class="material-icons">filter_list</i></span>';
				echo close('span');
				echo '</div>';
				echo '<div class="large-12 columns forgetinggridname" id="'.$modname.'addgridwidth" style="padding-left:0;padding-right:0;">';
				if($gridtype=='grid') {
					echo '<table id="'.$modname.'addgrid"> </table> ';
					echo '<div id="'.$modname.'addgridnav" class="w100"></div> ';
				} else {
					echo '<div class="desktop row-content inner-gridcontent  viewgridcolorstyle borderstyle" id="'.$modname.'addgrid" style="height:420px;top:0px;">';
					echo '</div>';
					echo '<div class="footer-content" id="'.$modname.'addgridfooter">';
					echo '</div>';
				}
				echo '</div>';
			} else {
				echo '<div class="large-9 columns innergridpaddingbtm '.$modname.'fullgridview"  style="padding-top: 8px;">';
				echo '<div class="large-12 columns paddingzero">';
				echo '<div class="large-12 columns viewgridcolorstyle" style="padding-left:0;padding-right:0;">';
				$spattr = array("class"=>"large-6 medium-6 small-12 columns innergridicon innergridiconhover small-only-text-center righttext");
				echo divopen($spattr);
				echo close('div');
				echo '</div>';
				echo '<div class="large-12 columns forgetinggridname viewgridcolorstyle borderstyle" id="'.$modname.'addgridwidth" style="padding-left:0;padding-right:0;">';
				if($gridtype=='grid') {
					echo '<table id="'.$modname.'addgrid"> </table> ';
					echo '<div id="'.$modname.'addgridnav" class="w100"></div> ';
				} else {
					echo '<div class="desktop row-content inner-gridcontent" id="'.$modname.'addgrid" style="height:470px;top:0px;">';
					echo '</div>';
					echo '<div class="footer-content" id="'.$modname.'addgridfooter">';
					echo '</div>';
				}
				echo '</div>';
			}
			echo '</div>';
			echo '</div>';
			echo '<div class="large-3 medium-3 columns paddingzero"><div class="searchfilteroverlay filterbox ui-draggable borderstylefilter" style="margin: 10px 10px 10px 0px; touch-action: none; -moz-user-select: none;"><div class="large-12 columns noticombowidget-box filterheaderborderstyle">
							<div class="large-12 medium-12 columns end noticomboheader activeheader filertabbgcolor" id="filterview" style="padding-top:0.4rem!important;padding-left:0.7rem !important;padding-bottom:0.5rem!important" data-griddataid="'.$modname.'addgrid"><span id="fiteractive" class="tab-titlemini sidebariconsmini active filtermini"><i title="" class="material-icons" style="top:5px;position:relative;">search</i><span style="left:5px;position:relative;font-size:0.9rem;">Filter Panel</span></span></div>
							<!--<div class="large-4 medium-4 columns end noticomboheader activeheader" id="minidashboard" style="padding-top:0.4rem!important;padding-left:0.2rem !important;padding-bottom:0.5rem!important" data-griddataid="'.$modname.'addgrid"><span id="dashboardactive" class="tab-titlemini sidebariconsmini dashboardmini"><i title="" class="material-icons">first_page</i><span>Dashboard</span></span></div>-->
							</div>';
			echo '<div class="filterview1">
				<div class="row">&nbsp;</div>
				<div class="large-12 medium-12 small-12 columns end filterheight filterbodyborderstyle" id="fbuildercat1">
				<div class="wrappercontent">
				<ul class="fbulderheadermenu">
				<span id="'.$modname.'filterformconditionvalidation" class="validationEngineContainer">';
			for($fl=0;$fl<$filtercount;$fl++) {
				$content .='<div class= elementlist'.$fl.'" style="">
									<ul class="elementiconsul" style="padding-left: 1rem;padding-right: 1rem;">
									<div class="" id=""><label>'.$filtertabgroup[$fl]['tabsecname'].'<span class="mandatoryfildclass">*</span></label><select data-placeholder="Select Columns" data-prompt-position="topLeft:14,36" id="'.$modname.'filtercolumn" class="validate[required] chzn-select dropdownchange multicolumnfilter" name="'.$modname.'filtercolumn" tabindex="9"><option></option>';
				$prev = " ";
				foreach($filtertabsecfield as $key) {
					if($filtertabgroup[$fl]['tabsecid'] == $key->moduletabsectionid){
						$cur = $key->moduletabsectionid;
						$content .='<option value="'.$key->viewcreationcolumnid.'" data-indexname="'.$key->viewcreationcolmodelindexname.'" data-uitype="'.$key->uitypeid.'" data-modname="'.$modname.'" data-label="'.$key->viewcreationcolumnname.'" data-moduleid="'.$key->viewcreationmoduleid.'" data-fldview="'.$key->fieldview.'" data-fldrestrict="'.$key->fieldrestrict.'">'.$key->viewcreationcolumnname.'</option>';
					}
				}
				$content .='</select></div></ul></div>';
			}
			$content .='<div class="large-12 columns viewcondclear">
								<label>Condition <span class="mandatoryfildclass">*</span></label>
								<select data-placeholder="" data-prompt-position="bottomLeft:14,36" class="validate[required] chzn-select chosenwidth " tabindex="11" name="'.$modname.'filtercondition" id="'.$modname.'filtercondition">
								<option></option>
								<option value="Equalto">Equalto</option>
								<option value="NotEqual">NotEqual</option>
								<option value="Startwith">Startwith</option>
								<option value="Endwith">Endwith</option>
								<option value="Middle">Middle</option>
								</select>
								</div>
								<div class="input-field large-12 columns viewcondclear " id="'.$modname.'filtercondvaluedivhid">
								<input type="text" class="validate[required,maxSize[100]]" id="'.$modname.'filtercondvalue" name="'.$modname.'filtercondvalue"  tabindex="12" >
								<label for="'.$modname.'filtercondvalue">Value <span class="mandatoryfildclass">*</span></label>
								</div>
								<div class="large-12 columns viewcondclear" id="'.$modname.'filterddcondvaluedivhid">
								<label>Value<span class="mandatoryfildclass">*</span></label>
								<select data-placeholder="" data-prompt-position="bottomLeft:14,36" class="validate[required] chzn-select chosenwidth valdropdownchange" tabindex="13" name="'.$modname.'filterddcondvalue" id="'.$modname.'filterddcondvalue">
								<option></option>
								</select>
								</div>
								<div class="large-12 columns viewcondclear" id="'.$modname.'filterdatecondvaluedivhid">
								<label>Value <span class="mandatoryfildclass">*</span></label>
								<input type="text" class="validate[required]" data-dateformater="<?php echo $appdateformat; ?>" id="'.$modname.'filterdatecondvalue" name="'.$modname.'filterdatecondvalue" tabindex="12" >
								</div>
								<div class="large-12 columns viewcondclear">
								<input type="hidden" class="" id="'.$modname.'finalfiltercondvalue" name="'.$modname.'finalfiltercondvalue"  tabindex="12" >
								<input type="hidden" class="" id="'.$modname.'filterfinalviewconid" name="'.$modname.'filterfinalviewconid"  tabindex="12" >
								</div>
								<div class="large-12 columns">&nbsp;</div>
								<div class="large-12 columns text-center">
								<input type="button" style="width:16%" class="btn formbuttonsalert" id="'.$modname.'filteraddcondsubbtn" Value="Submit" name="'.$modname.'filteraddcondsubbtn"  tabindex="14" >
								</div>
								<div class="large-12 columns">&nbsp;</div></span>
								<div class="large-12 columns">
								<div id="'.$modname.'filterconddisplay" class="large-12 medium-12 small-12 columns uploadcontainerstyle" style="height:10rem; border:1px solid #CCCCCC;overflow-y:auto;overflow-x:hidden;">
								</div>
								<input type="hidden" id="'.$modname.'conditionname" value="" name="'.$modname.'conditionname">
								<input type="hidden" id="'.$modname.'filterid" value="" name="'.$modname.'filterid">
								<input type="hidden" id="'.$modname.'filtervalue" value="" name="'.$modname.'filtervalue">
								</div>
								<div class="large-12 columns">&nbsp;</div></span>';
			$content .='</div></div></div></div>';
			echo $content;
echo ' <div id="textenterdiv" class="mininotesview hidedisplay large-12 medium-12 small-12 columns filterbodyborderstyle mininotesview1"><div class="large-12 columns paddingzero"><div class="large-12 columns paddingzero" style="text-align:left;cursor:pointer"><input id="unique_rowid" type="hidden"></div><div class="large-12 columns hidedisplay titleinputspan">&nbsp;</div><div class="large-12 columns paddingzero"><div class="atsearchdiv hidedisplay"></div><div class="large-12 columns paddingzero atsearchdiv1"><div class="large-12 columns fiterconversbar paddingzero"><span id="convmulitplefileuploader"></span><span class="uploadattachments attachbtnstyle" title="Attachment" style="cursor:pointer;"><i class="material-icons">attachment</i></span><textarea class="mention messagetoenterstyle" rows="2" placeholder="Enter Your Message Here..." id="homedashboardconvid" name="homedashboardconvid"></textarea><span class="sendbtnstyle" id="postsubmit"><i class="material-icons">send</i></span><span class="sendbtnstyle hidedisplay" id="editpostsubmit"><i class="material-icons">send</i></span><span class="convfileupload hidedisplay" style="position:relative;top:0.1rem;"><!--<span id="c_fileid" class="attachmentname" data-filepath="">--></span><input id="cc_filename" type="hidden"><input id="cc_filepath" type="hidden"></div><div class="large-6 medium-6  small-6 columns" style="text-align:right;"><!--<span><select class="chzn-select" style="width:35%" id="c_privacy" "><option value="1">Public</option><option value="2">OnlyMe</option></select></span><span class="postbtnstyle" id="postsubmit">Post</span><span class="postbtnstyle hidedisplay" id="editpostsubmit">Post</span><span class="postbtnstyle hidedisplay" id="postcancel">Cancel</span>--></div><input type="hidden" name="minimoduleid" id="minimoduleid" value=""/><input type="hidden" name="minicommonid" id="minicommonid" value=""/> </div></div></div><div class="large-12 columns scrollbarclass"  style="height: 32rem;overflow-x:hidden"><div class="clearfix"></div><ul id="noteswidget" class="member-list"></ul></div><div class="large-12 columns">&nbsp;</div></div>';
			//echo '<div class="large-4 columns">&nbsp;</div>';
			
			echo '</div>';
			echo '<div class="row">&nbsp;</div>';
			echo "</div>";//add form container close
			echo "</div>"; //tab group close
			if($editor == "Yes") {
				froalaeditorfileslinks();
			}
			if($fupload == "Yes") {
				// echo '<link href="'.base_url().'css/uploadfile.css" rel="stylesheet">';
				echo '<script src="'.base_url().'js/plugins/dropbox/dropbox.js" type="text/javascript" id="dropboxjs" data-app-key="0a1wlbnnevlhces"></script>';
			}
		}
	}
	function dataexplode($datas = array()) {
		$newstring = "";
		if(count($datas) >0 ) {
			$newstring = implode(',',$datas);
		}
		return $newstring;
	}
	//form fields generation for dashboard - only for EMR master module (HRM industry)
	function newdashboardformfieldsgeneratorwithgrid($modtabsecfrmfkdsgrp,$tabgrpid,$moduletype,$gridtype=null) {
		$CI =& get_instance();
		//grouping tab group value
		$tempgrplevel='dashboard';
		$newgrpkey=0;
		$tabmodule[]=array();
		$modid = explode(',',$moduletype['moduleid']);
		$modtypeid = explode(',',$moduletype['modtypeid']);
		foreach ($modtabsecfrmfkdsgrp as $key => $val) {
			if($tempgrplevel == ''.$val['modulename'].'') {
				$tabmodule[$tempgrplevel][$newgrpkey]=$val;
			} else {
				$tabmodule[''.$val['modulename'].''][$newgrpkey]=$val;
			}
			$newgrpkey++;
		}
		$tabsectiongrp = array();
		if(count($tabmodule) > 0) {
			//group tab section within tab group
			$tempgrpseclevel=0;
			$newgrpseckey=0;
			foreach($tabmodule as $key => $value) {
				foreach($value as $key1 => $val) {
					if($tempgrpseclevel==$val['tabsectionid']) {
						$tabsectiongrp[$key][$tempgrpseclevel][$newgrpseckey]=$val;
					} else {
						$tabsectiongrp[$key][$val['tabsectionid']][$newgrpseckey]=$val;
					}
					$newgrpseckey++;
				}
			}
		}
		//create form section and elements
		$tabgrpid = $tabgrpid;
		$prevgrpid = "";
		$data = array();
		$k=0;
		$mm=1;
		foreach($tabsectiongrp as $grpkey => $grpvalarr) {
			//print_r($grpvalarr);
			$gridid = 1;
			$gridname = array();
			$grdpartabname = array();
			$fieldnames = array();
			$fieldtables = array();
			$tabcolmnname = array();
			$parenttabname = array();
			$anfieldnames = array();
			$anfieldnameid = array();
			$anmoduleid = array();
			$anfiledtab = array();
			$attachfieldname = array();
			$attachcolname = array();
			$attachuitypeid = array();
			$attachfieldid = array();
			${'$griddata'.$gridid} = array();
			${'$gridfielddata'.$gridid} = array();
			${'$griddatauitype'.$gridid} = array();
			${'$grddatatabname'.$gridid} = array();
			${'$grddatapartabname'.$gridid} = array();
			$editor = "No";
			$fupload = "No";
			$multimoduleid = $modid[$k];
			$str = str_replace(" ","",$grpkey);
			$modname = strtolower($str);
			$CI =& get_instance();
			$device = $CI->Basefunctions->deviceinfo();
			//action generation
			if($device != 'phone') {
				$size = 10;
				$m=0;
				$menumore = '';
				$value = array();
				$des = array();
				$title = array();
				$CI =& get_instance();
				$actionassign = $CI->Basefunctions->actionassign($multimoduleid);
				foreach($actionassign as $key):
				$value[$key->toolbarnameid] = $key->toolbarname;
				$des[] = $key->description;
				$title[] = $key->toolbartitle;
				endforeach;
				echo '<ul id="'.$modname.'advanceddrop" class="singleaction-drop arrow_box">';
				foreach($value as $key => $value) {
					$fname = preg_replace('/[^A-Za-z0-9]/', '', $value);
					$name = strtolower($fname);
					if($name != 'add'){
						$menumore .= '<li id="'.$modname.$name.'icon"><span class="icon-box"><i class="icon24 '.$des[$m].' '.$name.'iconclass " title="'.$title[$m].'"></i>'.$title[$m].'</span></li>';
					}
					$m++;
				}
			$menumore .= '<li id="'.$modname.'viewtoggle"><span class="icon-box"><i class="material-icons" title="View Toggle">filter_list</i>View Toggle</span></li>';
				echo $menumore;
				echo "</ul>";
			}
			if($tabgrpid == 1) {
				echo '<div class="dbhiddensubform" id="dbsubformspan'.$tabgrpid.'">'; //tab group creation (1 st group)
				echo '<div class="large-12 columns addformunderheader">&nbsp;</div>';//div for Empty space
				echo '<div class="large-12 columns scrollbarclass addformcontainer">';//div for container
				echo '<div class="closed effectbox-overlay effectbox-default" id="'.$modname.'sectionoverlay">';
				echo '<div class="large-12 columns paddingzero">';
				if($modtypeid[$k] == 'mvsec' || $modtypeid[$k] == 'mvfwgrid') {
					echo '<div class="large-12 columns headercaptionstyle headerradius paddingzero">';
					echo '<div class="large-6 medium-6 small-8 columns headercaptionleft">
							<span data-activates="slide-out" class="headermainmenuicon button-collapse" title="Menu"><i class="material-icons">menu</i></span>
							<span class="gridcaptionpos"><span>'.ucwords($modname).'</span></span>
						  </div>';
					echo '<div class="large-5 medium-6 small-4 columns addformheadericonstyle righttext">
						<span title="Save" id="'.$modname.'dataupdatesubbtn" class="updatebtnclass icon-box hidedisplay"><i class="material-icons">save</i><span class="actiontitle">Save</span> </span>
						<span title="Save" id="'.$modname.'dataaddsbtn" class="addbtnclass icon-box"><i class="material-icons">save</i><span class="actiontitle">Save</span> </span>
						<span title="Close" class="icon-box '.$modname.'fwgsectionoverlayclose"><i class="material-icons">close</i><span class="actiontitle">Close</span> </span>
						</div>';
					echo '</div>';
					echo '<div class="large-12 columns tabgroupstyle">
							<ul data-tab="" class="tabs">
							<li id="tab1" data-subform="1" class="tab-title sidebaricons active ftab"><span>'.$modname.'</span></li>
							</ul>
						 </div>';
				}
				
				if($modtypeid[$k] == 'mvsec' || $modtypeid[$k] == 'mvfwgrid') {
					echo '<div class="large-12 columns addformunderheader">&nbsp;</div>';
					echo '<div class="large-12 columns addformcontainer">';
					echo '<div class="row" class="large-12 columns">&nbsp;</div>';
				}
				echo '<form method="POST" name="'.$modname.'addform" class="" action ="" id="'.$modname.'addform">';
				echo '<span id="'.$modname.'formaddwizard" class="validationEngineContainer" >';
				echo '<span id="'.$modname.'formeditwizard" class="validationEngineContainer">';
				$tabgrpid++;
				//$k++;
			} else {
				if($modtypeid[$k] == 'mvsec') {
					echo '<div class="dbhiddensubform hidedisplay" id="dbsubformspan'.$tabgrpid.'">'; //tab group creation (1 st group)
					echo '<div class="large-12 columns addformunderheader">&nbsp;</div>';//div for Empty space
					echo '<div class="large-12 columns scrollbarclass addformcontainer">';//div for container
					echo '<div class="closed effectbox-overlay effectbox-default" id="'.$str.'sectionoverlay">';
					echo '<div class="large-12 columns">';
					echo '<div class="row">&nbsp;</div>';
					if($modtypeid[$k] == 'mvsec' || $modtypeid[$k] == 'mvfwgrid') {
						echo '<div class="large-12 columns headercaptionstyle headerradius paddingzero">';
						echo '<div class="large-6 medium-6 small-6 columns headercaptionleft">
							<span data-activates="slide-out" class="headermainmenuicon button-collapse" title="Menu"><i class="material-icons">menu</i></span>
							<span class="gridcaptionpos"><span>'.ucwords($modname).'</span></span>
						  </div>';
						echo '<div class="large-5 medium-6 small-6 columns addformheadericonstyle righttext small-only-text-center">
<span title="Save" id="'.$modname.'dataupdatesubbtn" class="updatebtnclass icon-box hidedisplay"><i class="material-icons">save</i><span class="actiontitle">Save</span> </span>
<span title="Save" id="'.$modname.'dataaddsbtn" class="addbtnclass icon-box"><i class="material-icons">save</i><span class="actiontitle">Save</span> </span>
<span title="Close" class="icon-box '.$modname.'fwgsectionoverlayclose"><i class="material-icons">close</i><span class="actiontitle">Close</span> </span>
</div>';
						echo '</div>';
						echo '<div class="large-12 columns tabgroupstyle">
							<ul data-tab="" class="tabs">
							<li id="tab1" data-subform="1" class="tab-title sidebaricons active ftab"><span>'.$modname.'</span></li>
							</ul>
						 </div>';
					}
					
					if($modtypeid[$k] == 'mvsec' || $modtypeid[$k] == 'mvfwgrid') {
						echo '<div class="large-12 columns addformunderheader">&nbsp;</div>';//div for Empty space
					echo '<div class="large-12 columns scrollbarclass addformcontainer">';//div for container
					}
					echo '<form method="POST" name="'.$modname.'addform" class="" action ="" id="'.$modname.'addform">';
					echo '<span id="'.$modname.'formaddwizard" class="validationEngineContainer" >';
					echo '<span id="'.$modname.'formeditwizard" class="validationEngineContainer">';
				} else if($modtypeid[$k] == 'fwgrid') {
					echo '<div class="dbhiddensubform hidedisplay" id="dbsubformspan'.$tabgrpid.'">'; //tab group creation (1 st group)
					echo '<div class="large-12 columns addformunderheader">&nbsp;</div>';//div for Empty space
					echo '<div class="large-12 columns scrollbarclass addformcontainer">';//div for container
					echo '<div class="closed effectbox-overlay effectbox-default " id="'.$modname.'sectionoverlay">';
					echo '<div class="large-12 columns paddingzero">';
					echo '<form method="POST" name="'.$modname.'addform" class="" action ="" id="'.$modname.'addform">';
					echo '<span id="'.$modname.'formaddwizard" class="validationEngineContainer" >';
					echo '<span id="'.$modname.'formeditwizard" class="validationEngineContainer">';
				} else if($modtypeid[$k] == 'mvfwgrid') {
					echo '<div class="dbhiddensubform hidedisplay" id="dbsubformspan'.$tabgrpid.'">'; //tab group creation (1 st group)
					echo '<div class="large-12 columns addformunderheader">&nbsp;</div>';//div for Empty space
					echo '<div class="large-12 columns scrollbarclass addformcontainer">';//div for container
					echo '<div class="closed effectbox-overlay effectbox-default" id="'.$modname.'sectionoverlay">';
					echo '<div class="large-12 columns paddingzero">';
					if($modtypeid[$k] == 'mvsec' || $modtypeid[$k] == 'mvfwgrid') {
						echo '<div style="height:auto;" class="large-12 columns headercaptionstyle headerradius paddingzero">';
						echo '<div style="" class="large-6 medium-6 small-6 columns headercaptionleft">
								<span  class="headermainmenuicon" title="Menu"><i class="material-icons">menu</i></span>
								<span class="gridcaptionpos"><span>'.ucwords($modname).'</span></span>
							 </div>';
						echo '<div class="large-5 medium-6 small-6 columns addformheadericonstyle righttext small-only-text-center">
<span title="Save" id="'.$modname.'dataupdatesubbtn" class="updatebtnclass icon-box hidedisplay"><i class="material-icons">save</i><span class="actiontitle">Save</span> </span>
<span title="Save" id="'.$modname.'dataaddsbtn" class="addbtnclass icon-box"><i class="material-icons">save</i><span class="actiontitle">Save</span> </span>
<span title="Close" class="icon-box '.$modname.'fwgsectionoverlayclose"><i class="material-icons">close</i><span class="actiontitle">Close</span> </span>
</div>';
						echo '</div>';
						echo '<div class="large-12 columns tabgroupstyle">
							<ul data-tab="" class="tabs">
							<li id="tab1" data-subform="1" class="tab-title sidebaricons active ftab"><span>'.$modname.'</span></li>
							</ul>
						 </div>';
					}
					
					if($modtypeid[$k] == 'mvsec' || $modtypeid[$k] == 'mvfwgrid') {
						echo '<div class="large-12 columns addformunderheader">&nbsp;</div>';//div for Empty space
						echo '<div class="large-12 columns scrollbarclass addformcontainer">';//div for container
					}
					echo '<form method="POST" name="'.$modname.'addform" class="" action ="" id="'.$modname.'addform">';
					echo '<span id="'.$modname.'formaddwizard" class="validationEngineContainer" >';
					echo '<span id="'.$modname.'formeditwizard" class="validationEngineContainer">';
					
				}
				$tabgrpid++;
				//$k++;
			}
			$prevgrpsecid = "";
			$i = 100;
			foreach($grpvalarr as $seckey => $secvalarr) { //section creation
				if($modtypeid[$k] == 'mvsec') {
					echo '<div class="large-4 columns end paddingbtm">';
				} else  if($modtypeid[$k] == 'fwgrid') {
					echo '<div class="large-4 large-offset-8 columns paddingbtm" style="padding-right: 0em !important;">';
				} else  if($modtypeid[$k] == 'mvfwgrid') {
					echo '<div class="large-12 columns scrollbarclass paddingzero">';
					echo '<div class="large-4 columns gridformclear end paddingbtm">';
					echo '<div class="large-12 columns cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;">';
					echo '<div id="'.$modname.'addgrid'.$mm.'validation" class="validationEngineContainer" style="padding-left: 0 !important; padding-right: 0 !important;">';
					echo '<div id="'.$modname.'editgrid'.$mm.'validation" class="validationEngineContainer" style="padding-left: 0 !important; padding-right: 0 !important;">';
				}
				foreach($secvalarr as $fieldkey => $fieldval) { //form field creation
					$curgrpsecid = $fieldval['tabsectionid'];
					$modtabgrpid = $fieldval['modtabgrpid'];
					$tabgrpname = $fieldval['tabgroupname'];
					$name = substr( strtolower($tabgrpname),0,3);
					if($prevgrpsecid != $curgrpsecid) {
						$prevgrpsecid=$fieldval['tabsectionid'];
						if($modtypeid[$k] != 'mvfwgrid') {
						echo '<div class="large-12 columns cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;">';//tab section padding open
						}
						
						if($modtypeid[$k] == 'mvfwgrid' ||$modtypeid[$k] == 'mvsec' ) {
							echo '<div class="large-12 columns headerformcaptionstyle">'.$fieldval['tabsectionname'].'</div>';
						}
						if($modtypeid[$k] == 'fwgrid') {
							echo '<div class="large-12 columns sectionheaderformcaptionstyle">'.$fieldval['tabsectionname'].'</div>';
							echo '<div class="large-12 columns sectionpanel">';
						}
						//function call for fields generation
						if($fieldval['colname'] != "salutationid") {
							$colmtypeid = $fieldval['fieldcolmtypeid'];
							echo fieldgenerate($fieldval,$i,$colmtypeid);
						}
					} else {
						//function call for fields generation
						if($fieldval['colname'] != "salutationid") {
							$colmtypeid = $fieldval['fieldcolmtypeid'];
							echo fieldgenerate($fieldval,$i,$colmtypeid);
						}
					}
					if($modtypeid[$k] == 'mvfwgrid') {
						${'$griddata'.$gridid}[] = $fieldval['fieldname'];
						${'$gridfielddata'.$gridid}[] = $fieldval['colname'];
						${'$griddatauitype'.$gridid}[] = $fieldval['uitypeid'];
						${'$grddatatabname'.$gridid}[] = $fieldval['tabname'];
						${'$grddatapartabname'.$gridid}[] = $fieldval['parenttable'];
						$grdpartabname[] = $fieldval['parenttable'];
					}
					if($fieldval['uitypeid'] == 20) {
						$fieldsinfo = explode(',',$fieldval['ddparenttable']);
						if($fieldsinfo[1]!="") { $fieldnames[] = $fieldsinfo[1]; }
						if($fieldval['tabname']!="") { $fieldtables[] = $fieldval['tabname']; }
						if($fieldsinfo[1]!="") { $tabcolmnname[] = $fieldsinfo[1]; }
						if($fieldval['parenttable']!="") { $parenttabname[] = $fieldval['parenttable']; }
					}
					if($fieldval['fieldname']!="" && $fieldval['tabname']!="" && $fieldval['colname']!="" && $fieldval['parenttable']!="") {
						$fieldnames[] = $fieldval['fieldname'];
						$fieldtables[] = $fieldval['tabname'];
						$tabcolmnname[] = $fieldval['colname'];
						$parenttabname[] = $fieldval['parenttable'];
					}
					if($fieldval['uitypeid'] == 14) {
						$anfieldnames[] = $fieldval['fieldname'];
						$anfieldnameid[] = $fieldval['modfieldid'];
						$anfiledtab[] = $fieldval['tabname'];
						$anmoduleid[] = $modid[$k];
					}
					if($fieldval['uitypeid'] == 15 || $fieldval['uitypeid'] == 16) {
						$attachfieldname[] = $fieldval['fieldname'];
						$attachcolname[] = $fieldval['colname'];
						$attachuitypeid[] = $fieldval['uitypeid'];
						$attachfieldid[] = $fieldval['modfieldid'];
						$fupload = "Yes";
					}
					if($fieldval['uitypeid'] == 24) {
						$editor = "Yes";
					}
					$readmode = "";
					$mandatoryopt = "";
					if($fieldval['fieldmode'] != "No") {
						$i++;
					}
				}
				if($modtypeid[$k] == 'fwgrid') {
					echo '</div>';
					echo '<div class="divider large-12 columns"></div>';
					echo '<div class="large-12 columns sectionalertbuttonarea submitkeyboard"  style="text-align: right">';
				}else {
					echo '<div class="large-12 columns submitkeyboard"  style="text-align: right">';
					echo label('&nbsp;',array());
				}
				
				if($modtypeid[$k] == 'fwgrid') {
					//update button
					$butattr = array('class'=>'hidedisplayfwg updatekeyboard alertbtnyes','value'=>'Save','tabindex'=>$i++);
					echo button(''.$modname.'dataupdatesubbtn',$butattr);
					//Add button
					$butattr = array('class'=>'addkeyboard alertbtnyes','value'=>'Save','tabindex'=>$i++);
					echo button(''.$modname.'savebutton',$butattr);
					//Cancel button
					$butattr = array('class'=>' alertbtnno leftcloseposition'.$modname.'fwgsectionoverlayclose','value'=>'Close','tabindex'=>$i++);
					echo button(''.$modname.'fwgsectionoverlayclose',$butattr);
				} else if($modtypeid[$k] == 'mvfwgrid') {
					//Update button
					$upbutattr = array('class'=>''.$modname.'frmtogridbutton updatebtnclass updatekeyboard hidedisplayfwg btn','tabindex'=>''.$i.'','value'=>'Submit','data-frmtogriddataid'=>''.$gridid.'','data-frmtogridname'=>''.$modname.$name.'addgrid'.$mm.'');
					echo button(''.$modname.$name.'updatebutton',$upbutattr);
					//Add button
					$butattr = array('class'=>''.$modname.'frmtogridbutton addbtnclass addkeyboard btn','tabindex'=>''.$i.'','value'=>'Submit','data-frmtogriddataid'=>''.$gridid.'','data-frmtogridname'=>''.$modname.$name.'addgrid'.$mm.'');
					echo button(''.$modname.$name.'addbutton',$butattr);
				}
				echo close('div');
				if($modtypeid[$k] != 'mvfwgrid') {
					echo '</div>'; //tab section padding close
				}
				if($modtypeid[$k] == 'mvfwgrid') {
					echo '</div>';
					echo '</div>';
					echo '</div>';
				}
				echo '</div>'; 
				
				//tab section close
				if($modtypeid[$k] == 'mvfwgrid') {
					//hidden fields
					$datas = implode(',',${'$griddata'.$gridid});
					echo hidden(''.$modname.'gridcolnames'.$gridid,$datas);
					$flddatas = implode(',',${'$gridfielddata'.$gridid});
					echo hidden(''.$modname.'gridfieldnames'.$gridid,$flddatas);
					$uidatas = implode(',',${'$griddatauitype'.$gridid});
					echo hidden(''.$modname.'gridcoluitype'.$gridid,$uidatas);
					$griddatatabname = implode(',',${'$grddatatabname'.$gridid});
					echo hidden(''.$modname.'griddatatabnameinfo'.$gridid,$griddatatabname);
					$griddatapartabname = implode(',',${'$grddatapartabname'.$gridid});
					echo hidden(''.$modname.'griddatapartabnameinfo'.$gridid,$griddatatabname);
					echo hidden(''.$modname.'griddataprimayid'.$gridid,'');
				}
				
				$modulelink = $fieldval['modlink'];
			}
			if($modtypeid[$k] == 'mvfwgrid') {
				$gridname[] = $modname.$name.'addgrid'.$mm;
				echo '<div class="large-8 columns paddingbtm">';
				echo '<div class="large-12 columns paddingzero">';
				echo '<div class="large-12 columns viewgridcolorstyle" style="padding-left:0;padding-right:0;">';
				echo '<div class="large-12 columns headerformcaptionstyle" style="padding: 0.2rem 0 0rem;height:auto">';
				echo '<div class="large-6 medium-6 small-12 columns lefttext small-only-text-center">';
				echo '<span>'.ucwords($tabgrpname).' List <span class="paging-box '.$name.'gridfootercontainer"></span></span>';
				echo '</div>';
				echo '<div class="large-6 medium-6 small-12 columns innergridicon innergridiconhover righttext small-only-text-center">';
				echo '<span id="'.$modname.$name.'ingrideditspan'.$mm.'" style="display:none;"><i id="'.$modname.$name.'ingridedit'.$mm.'" class="material-icons editiconclass" title="Edit" style="padding-right:0rem;">edit</i></span>';
				echo '<span id="'.$modname.$name.'ingriddelspan'.$mm.'" style=""><i id="'.$modname.$name.'ingriddel'.$mm.'" class="material-icons deleteiconclass" title="Delete" style="padding-right:0rem">delete</i></span>';
				echo '<span id="'.$modname.$name.'ingridselspan'.$mm.'" style="display:none;"><i id="'.$modname.$name.'ingridsel'.$mm.'" class="material-icons checkboxiconclass unchecked" title="Choose" style="padding-right:0rem;">select_all</i></span>';
				echo '<span id="'.$modname.$name.'ingridserspan'.$mm.'" style="display:none;"><i id="'.$modname.$name.'ingridserspan'.$mm.'" class="material-icons searchiconclass" title="Search" style="padding-right:0rem;">search</i></span>';
				echo '</div>';
				echo '</div>';
				echo '<div class="large-12 forgetinggridname columns" id="'.$modname.$name.'addgrid'.$mm.'width" style="padding-left:0;padding-right:0;">';
				$device = $CI->Basefunctions->deviceinfo();
				if($device=='phone') {
					echo '<div class="desktop row-content inner-gridcontent" id="'.$modname.$name.'addgrid'.$mm.'" style="height:420px;top:0px;">';
					echo '</div>';
					echo '<div class="footercontainer inner-gridfooter" id="'.$modname.$name.'addgrid'.$mm.'footer">';
					echo '</div>';
				} else {
					echo '<div class="desktop row-content inner-gridcontent" id="'.$modname.$name.'addgrid'.$mm.'" style="height:485px !important;top:0px;"> </div>';
					echo '<div class="footercontainer inner-gridfooter footer-content" id="'.$modname.$name.'addgrid'.$mm.'footer"> </div>';
				}
				echo '</div>';
				echo '</div>';
				//for summary
				$modtabsecgen = $CI->Basefunctions->moduletabsecfieldfetchsummary($modid[$k],$modtabgrpid);
				if(count($modtabsecgen)>=1) {
					echo '<div class="large-6 columns paddingzero">';
					echo '<div class="large-12 columns">&nbsp;</div>';
					echo '<div class="large-12 columns cleardataform" style="background:#f5f5f5;padding-left:0;padding-right:0" >';
					echo '<div class="large-12 columns headerformcaptionstyle">Summary</div>';
					echo '<div class="large-12 columns">&nbsp;</div>';
					echo '<div class="large-12 columns" >';
					foreach($modtabsecgen as $fkey => $fval) { //form field creation
						$readmode = "";
						if($fval['readmode'] == "Yes") {
							$readmode = 'readonly';
						}
						$fildmode = "";
						if($fval['fieldmode'] == "No") {
							$fildmode = ' hidedisplay';
						}
						echo '<div class="large-12 columns" >';
						echo '<div class="large-6 medium-6 small-12 columns small-only-text-center'.$fildmode.'" ><label>'.$fval["filedlabel"].'</label></div>';
						echo '<div class="large-6 medium-6 small-12 columns'.$fildmode.'"><input name="'.$fval["fieldname"].'" type="text" id="'.$fval["fieldname"].'" value="" class="borderless small-only-text-center" '.$readmode.'></div>';
						echo '</div>';
						if($fval['uitypeid'] == 20) {
							$fieldsinfo = explode(',',$fval['ddparenttable']);
							if($fieldsinfo[1]!="") { $fieldnames[] = $fieldsinfo[1]; }
							if($fval['tabname']!="") { $fieldtables[] = $fval['tabname']; }
							if($fieldsinfo[1]!="") { $tabcolmnname[] = $fieldsinfo[1]; }
							if($fval['parenttable']!="") { $parenttabname[] = $fval['parenttable']; }
						}
						if($fval['fieldname']!="" && $fval['tabname']!="" && $fval['colname']!="" && $fval['parenttable']!="") {
							$fieldnames[] = $fval['fieldname'];
							$fieldtables[] = $fval['tabname'];
							$tabcolmnname[] = $fval['colname'];
							$parenttabname[] = $fval['parenttable'];
						}
					}
					echo '<div class="large-12 columns" >&nbsp;</div>';
					echo '</div>';
					echo '</div>';
					echo '</div>';
				}
				echo '</div>';
				echo '</div>';
				echo '</div>';
			}
			echo '</span>';
			echo '</span>';
			
			//hidden text box contain data insert informations
			$grdname = dataexplode($gridname);
			$grdpartablename = dataexplode($grdpartabname);
			$fieldname = dataexplode($fieldnames);
			$fieldtable = dataexplode($fieldtables);
			$tabcolname = dataexplode($tabcolmnname);
			$partabname = dataexplode($parenttabname);
			$anfieldname = dataexplode($anfieldnames);
			$anfieldnameids = dataexplode($anfieldnameid);
			$anmoduleids = dataexplode($anmoduleid);
			$anmoduletable = dataexplode($anfiledtab);
			$attachfldname = dataexplode($attachfieldname);
			$attachcolname = dataexplode($attachcolname);
			$attachuitype = dataexplode($attachuitypeid);
			$attachfieldid = dataexplode($attachfieldid);
			
			echo hidden(''.$modname.'gridnameinfo',$grdname);
			echo hidden(''.$modname.'elementsname',$fieldname);
			echo hidden(''.$modname.'elementstable',$fieldtable);
			echo hidden(''.$modname.'elementscolmn',$tabcolname);
			echo hidden(''.$modname.'elementspartabname',$partabname);
			echo hidden(''.$modname.'primarydataid','');
			echo hidden(''.$modname.'sortorder','');
			echo hidden(''.$modname.'sortcolumn','');
			echo hidden(''.$modname.'anfieldnameinfo',$anfieldname);
			echo hidden(''.$modname.'anfieldnameidinfo',$anfieldnameids);
			echo hidden(''.$modname.'anfieldnamemodinfo',$anmoduleids);
			echo hidden(''.$modname.'anfieldtabinfo',$anmoduletable);
			echo hidden(''.$modname.'attachfieldnames',$attachfldname);
			echo hidden(''.$modname.'attachcolnames',$attachcolname);
			echo hidden(''.$modname.'attachuitypeids',$attachuitype);
			echo hidden(''.$modname.'attachfieldids',$attachfieldid);
			echo '</form>';
			if($modtypeid[$k] == 'mvsec' || $modtypeid[$k] == 'mvfwgrid') {
				echo '</div>';
			}
			echo '</div>';
			echo '</div>';
			$ds = DIRECTORY_SEPARATOR;
			$nl = explode('/',$modulelink);
			echo '<script src="'.base_url().'js'.'/'.$nl[0].'/'.strtolower($nl[0]).'.js" type="text/javascript"></script>';
			
			//Inner grid generation
			echo '<div class="large-12 columns paddingbtm '.$modname.'fullgridview">';
			echo '<div class="large-12 columns paddingzero">';
			echo '<div class="large-12 columns viewgridcolorstyle" style="padding-left:0;padding-right:0;">';
			echo '<div class="large-12 columns headerformcaptionstyle" style="height:auto;line-height:1.8rem;padding: 0.2rem 0 0rem;">';
			echo '<span class="large-6 medium-6 small-6 columns  lefttext">'.$grpkey.' List<span class="fwgpaging-box '.$modname.'gridfootercontainer"></span></span>';
			$spattr = array("class"=>"large-6 medium-6 small-6 columns innergridicon innergridiconhover  righttext");
			echo spanopen($spattr);
			//action generation
			$size = 10;
			$value = array();
			$des = array();
			$title = array();
			$CI =& get_instance();
			$actionassign = $CI->Basefunctions->actionassign($fieldval['moduleid']);
			foreach($actionassign as $key):
			$value[$key->toolbarnameid] = $key->toolbarname;
			$des[] = $key->description;
			$title[] = $key->toolbartitle;
			endforeach;
			echo actionmenu($value,$size,$des,$title,strtolower($nl[0]),'icon24 ');
			echo '<span id="'.$modname.'viewtoggle" class="" title="View Toggle"><i class="material-icons">filter_list</i></span>';
			echo close('span');
			echo '</div>';
			echo '<div class="large-12 columns forgetinggridname" id="'.$modname.'addgridwidth" style="padding-left:0;padding-right:0;">';
			if($gridtype=='grid') {
				echo '<table id="'.$modname.'addgrid"> </table> ';
				echo '<div id="'.$modname.'addgridnav" class="w100"></div> ';
			} else {
				echo '<div class="desktop row-content inner-gridcontent" id="'.$modname.'addgrid" style="height:420px;top:0px;">';
				echo '</div>';
				echo '<div class="footercontainer inner-gridfooter" id="'.$modname.'addgridfooter">';
				echo '</div>';
			}
			echo '</div>';
			echo '</div>';
			echo '</div>';
			echo '</div>';
			echo '<div class="row">&nbsp;</div>';
			echo "</div>";//add form container close
			echo "</div>"; //tab group close
			if($editor == "Yes") {
				froalaeditorfileslinks();
			}
			if($fupload == "Yes") {
				echo '<script src="'.base_url().'js/plugins/dropbox/dropbox.js" type="text/javascript" id="dropboxjs" data-app-key="0a1wlbnnevlhces"></script>';
			}
			$k++;
		}
	}
	//form fields generation for dashboard - only for pick list module
	function picklistdashboardformfieldsgeneratorwithgrid($modtabsecfrmfkdsgrp,$tabgrpid,$modid,$gridtype=null) {
		//grouping tab group value
		$tempgrplevel='dashboard';
		$newgrpkey=0;
		$tabmodule[]=array();
		$modid = explode(',',$modid);
		foreach ($modtabsecfrmfkdsgrp as $key => $val) {
			if($tempgrplevel == ''.$val['modulename'].'') {
				$tabmodule[$tempgrplevel][$newgrpkey]=$val;
			} else {
				$tabmodule[''.$val['modulename'].''][$newgrpkey]=$val;
			}
			$newgrpkey++;
		}
		$tabsectiongrp = array();
		if(count($tabmodule) > 0) {
			//group tab section within tab group
			$tempgrpseclevel=0;
			$newgrpseckey=0;
			foreach($tabmodule as $key => $value) {
				foreach($value as $key1 => $val) {
					if($tempgrpseclevel==$val['tabsectionid']) {
						$tabsectiongrp[$key][$tempgrpseclevel][$newgrpseckey]=$val;
					} else {
						$tabsectiongrp[$key][$val['tabsectionid']][$newgrpseckey]=$val;
					}
					$newgrpseckey++;
				}
			}
		}
		//create form section and elements
		$tabgrpid = $tabgrpid;
		$prevgrpid = "";
		$data = array();
		$k=0;
		foreach($tabsectiongrp as $grpkey => $grpvalarr) {
			//print_r($grpkey);
			//print_r($grpvalarr);
			$fieldnames = array();
			$fieldtables = array();
			$tabcolmnname = array();
			$parenttabname = array();
			$anfieldnames = array();
			$anfieldnameid = array();
			$anmoduleid = array();
			$anfiledtab = array();
			$attachfieldname = array();
			$attachcolname = array();
			$attachuitypeid = array();
			$attachfieldid = array();
			$editor = "No";
			$fupload = "No";
			$str = str_replace(" ","",$grpkey);
			$modname = strtolower($str);
			$multimoduleid= $modid[$k];
			$CI =& get_instance();
			$device = $CI->Basefunctions->deviceinfo();
			//action generation
			if($device != 'phone') {
				$size = 10;
				$m=0;
				$menumore = '';
				$value = array();
				$des = array();
				$title = array();
				$CI =& get_instance();
				$actionassign = $CI->Basefunctions->actionassign($multimoduleid);
				foreach($actionassign as $key):
				$value[$key->toolbarnameid] = $key->toolbarname;
				$des[] = $key->description;
				$title[] = $key->toolbartitle;
				endforeach;
				echo '<ul id="'.$modname.'advanceddrop" class="singleaction-drop arrow_box">';
				foreach($value as $key => $value) {
					$fname = preg_replace('/[^A-Za-z0-9]/', '', $value);
					$name = strtolower($fname);
					if($name != 'add'){
						$menumore .= '<li id="'.$modname.$name.'icon"><span class="icon-box"><i class="icon24 '.$des[$m].' '.$name.'iconclass " title="'.$title[$m].'"></i>'.$title[$m].'</span></li>';
					}
					$m++;
				}
				$menumore .= '<li id="'.$modname.'viewtoggle"><span class="icon-box"><i class="" title="View Toggle">filter_list</i>View Toggle</span></li>';
				echo $menumore;
				echo "</ul>";
			}
			if($tabgrpid == 1) {
				echo '<div class="dbhiddensubform" id="dbsubformspan'.$tabgrpid.'">'; //tab group creation (1 st group)
				echo '<div class="large-12 columns addformunderheader">&nbsp;</div>';//div for Empty space
				echo '<div class="large-12 columns scrollbarclass addformcontainer">';//div for container
				echo '<div class="large-3 medium-4 columns paddingzero '.$modname.'filterview hidedisplay">';
				$CI =& get_instance();
				$filtertabgroup = $CI->Basefunctions->formmoduletabgroupdetailsfetch($modid[$k]);
				$filtertabsecfield = $CI->Basefunctions->formmoduletabsectiondetailsfetch($modid[$k]);
				$filtercount = count($filtertabgroup);
				$content ='<div class="innersearchfilteroverlay filterbox ui-draggable borderstylefilter">
		<div class="large-12 columns innercombowidget-box"><div class="large-1 medium-1 small-1 columns paddingzero"><span class="icon-box" style="padding:0.7rem 0;"><i id="'.$modname.'closefiltertoggle" class="material-icons" title="close">arrow_back</i></span></div><div class="large-10 medium-10 small-11 columns paddingzero end comboheader" style="padding:0.6rem 0 !important">Filter Panel</div></div>
				<div class="dashboardview hidedisplay">
				<div class="row">&nbsp;</div>
				</div>
				<div class="filterview">
				<div class="row">&nbsp;</div>
				<div class="large-12 medium-12 small-12 columns end filterheight" id="fbuildercat1" style="padding:10px 15px;overflow-y:auto;overflow-x:hidden;">
				<div class="wrappercontent">
				<ul class="fbulderheadermenu">';
				for($fl=0;$fl<$filtercount;$fl++) {
					$content .='<div class="bgcolorfbelements elementlist'.$fl.'">
					<ul class="elementiconsul" style="padding-left: 1rem;padding-right: 1rem;">
						<div class="" id=""><label>'.$filtertabgroup[$fl]['tabsecname'].'<span class="mandatoryfildclass">*</span></label><select data-placeholder="Select Columns" data-prompt-position="topLeft:14,36" id="'.$modname.'filtercolumn" class="validate[required] chzn-select dropdownchange multicolumnfilter" name="'.$modname.'filtercolumn" tabindex="9"><option></option>';
					$prev = " ";
					foreach($filtertabsecfield as $key) {
						if($filtertabgroup[$fl]['tabsecid'] == $key->moduletabsectionid){
							$cur = $key->moduletabsectionid;
							$content .='<option value="'.$key->viewcreationcolumnid.'" data-indexname="'.$key->viewcreationcolmodelindexname.'" data-uitype="'.$key->uitypeid.'" data-modname="'.$modname.'" data-label="'.$key->viewcreationcolumnname.'" data-moduleid="'.$key->viewcreationmoduleid.'" data-fldview="'.$key->fieldview.'" data-fldrestrict="'.$key->fieldrestrict.'">'.$key->viewcreationcolumnname.'</option>';
						}
					}
					$content .='</select></div></ul></div>';
				}
				$content .='<span id="'.$modname.'filterformconditionvalidation" class="validationEngineContainer"><div class="large-12 columns viewcondclear">
					<label>Condition <span class="mandatoryfildclass">*</span></label>
					<select data-placeholder="" data-prompt-position="bottomLeft:14,36" class="validate[required] chzn-select chosenwidth " tabindex="11" name="'.$modname.'filtercondition" id="'.$modname.'filtercondition">
						<option></option>
						<option value="Equalto">Equalto</option>
						<option value="NotEqual">NotEqual</option>
						<option value="Startwith">Startwith</option>
						<option value="Endwith">Endwith</option>
						<option value="Middle">Middle</option>
					</select>
				</div>
				<div class="input-field large-12 columns viewcondclear" id="'.$modname.'filtercondvaluedivhid">
					<input type="text" class="validate[required,maxSize[100]]" id="'.$modname.'filtercondvalue" name="'.$modname.'filtercondvalue"  tabindex="12" >
					<label for="'.$modname.'filtercondvalue">Value <span class="mandatoryfildclass">*</span></label>
				</div>
				<div class="large-12 columns viewcondclear" id="'.$modname.'filterddcondvaluedivhid">
					<label>Value<span class="mandatoryfildclass">*</span></label>
					<select data-placeholder="" data-prompt-position="bottomLeft:14,36" class="validate[required] chzn-select chosenwidth valdropdownchange" tabindex="13" name="'.$modname.'filterddcondvalue" id="'.$modname.'filterddcondvalue">
						<option></option>
					</select>
				</div>
				<div class="large-12 columns viewcondclear" id="'.$modname.'filterdatecondvaluedivhid">
					<label>Value <span class="mandatoryfildclass">*</span></label>
					<input type="text" class="validate[required]" data-dateformater="<?php echo $appdateformat; ?>" id="'.$modname.'filterdatecondvalue" name="'.$modname.'filterdatecondvalue" tabindex="12" >
				</div>
				<div class="large-12 columns viewcondclear">
					<input type="hidden" class="" id="'.$modname.'finalfiltercondvalue" name="'.$modname.'finalfiltercondvalue"  tabindex="12" >
				</div>
				<div class="large-12 columns">&nbsp;</div>
					<div class="large-12 columns">
						<input type="button" style="width:16%;" class="btn formbuttonsalert" id="'.$modname.'filteraddcondsubbtn" Value="Submit" name="'.$modname.'filteraddcondsubbtn"  tabindex="14" >
					</div>
				<div class="large-12 columns">&nbsp;</div></span>
				<div class="large-12 columns">
					<div id="'.$modname.'filterconddisplay" class="large-12 medium-12 small-12 columns uploadcontainerstyle uploadcontainerstyle1">
					</div>
					<input type="hidden" id="'.$modname.'conditionname" value="" name="'.$modname.'conditionname">
				</div>
				<div class="large-12 columns">&nbsp;</div></span>';
				$content .='</div></div></div></div>';
				echo $content;
				echo '</div>';
				echo '<div class="row">&nbsp;</div>';
				echo '<form method="POST" name="'.$modname.'addform" action ="" id="'.$modname.'addform">';
				echo '<span id="'.$modname.'formaddwizard" class="validationEngineContainer" >';
				echo '<span id="'.$modname.'formeditwizard" class="validationEngineContainer">';
				$tabgrpid++;
				$k++;
			} else {
				echo '<div class="dbhiddensubform hidedisplay" id="dbsubformspan'.$tabgrpid.'">'; //tab group creation (1 st group)
				echo '<div class="large-12 columns addformunderheader">&nbsp;</div>';//div for Empty space
				echo '<div class="large-12 columns scrollbarclass addformcontainer">';//div for container
				echo '<div class="large-3 medium-4 columns paddingzero '.$modname.'filterview hidedisplay">';
				$CI =& get_instance();
				$filtertabgroup = $CI->Basefunctions->formmoduletabgroupdetailsfetch($modid[$k]);
				$filtertabsecfield = $CI->Basefunctions->formmoduletabsectiondetailsfetch($modid[$k]);
				$filtercount = count($filtertabgroup);
				$content ='<div class="innersearchfilteroverlay filterbox ui-draggable borderstylefilter">
				<div class="large-12 columns innercombowidget-box"><div class="large-1 medium-1 small-1 columns paddingzero"><span class="icon-box" style="padding:0.7rem 0;"><i id="'.$modname.'closefiltertoggle" class="material-icons" title="close">arrow_back</i></span></div><div class="large-10 medium-10 small-11 columns paddingzero end comboheader" style="padding:0.6rem 0 !important">Filter Panel</div></div>
				<div class="dashboardview hidedisplay">
				<div class="row">&nbsp;</div>
				</div>
				<div class="filterview">
				<div class="row">&nbsp;</div>
				<div class="large-12 medium-12 small-12 columns end filterheight" id="fbuildercat1" style="padding:10px 15px;overflow-y:auto;overflow-x:hidden;">
				<div class="wrappercontent">
				<ul class="fbulderheadermenu">';
				for($fl=0;$fl<$filtercount;$fl++) {
					$content .='<div class="bgcolorfbelements elementlist'.$fl.'" style="">
					<ul class="elementiconsul" style="padding-left: 1rem;padding-right: 1rem;">
						<div class="" id=""><label>'.$filtertabgroup[$fl]['tabsecname'].'<span class="mandatoryfildclass">*</span></label><select data-placeholder="Select Columns" data-prompt-position="topLeft:14,36" id="'.$modname.'filtercolumn" class="validate[required] chzn-select dropdownchange multicolumnfilter" name="'.$modname.'filtercolumn" tabindex="9"><option></option>';
					$prev = " ";
					foreach($filtertabsecfield as $key) {
						if($filtertabgroup[$fl]['tabsecid'] == $key->moduletabsectionid){
							$cur = $key->moduletabsectionid;
							$content .='<option value="'.$key->viewcreationcolumnid.'" data-indexname="'.$key->viewcreationcolmodelindexname.'" data-uitype="'.$key->uitypeid.'" data-modname="'.$modname.'" data-label="'.$key->viewcreationcolumnname.'" data-moduleid="'.$key->viewcreationmoduleid.'" data-fldview="'.$key->fieldview.'" data-fldrestrict="'.$key->fieldrestrict.'">'.$key->viewcreationcolumnname.'</option>';
						}
					}
					$content .='</select></div></ul></div>';
				}
				$content .='<span id="'.$modname.'filterformconditionvalidation" class="validationEngineContainer"><div class="large-12 columns viewcondclear">
					<label>Condition <span class="mandatoryfildclass">*</span></label>
					<select data-placeholder="" data-prompt-position="bottomLeft:14,36" class="validate[required] chzn-select chosenwidth " tabindex="11" name="'.$modname.'filtercondition" id="'.$modname.'filtercondition">
						<option></option>
						<option value="Equalto">Equalto</option>
						<option value="NotEqual">NotEqual</option>
						<option value="Startwith">Startwith</option>
						<option value="Endwith">Endwith</option>
						<option value="Middle">Middle</option>
					</select>
				</div>
				<div class="input-field large-12 columns viewcondclear" id="'.$modname.'filtercondvaluedivhid">
					<input type="text" class="validate[required,maxSize[100]]" id="'.$modname.'filtercondvalue" name="'.$modname.'filtercondvalue"  tabindex="12" >
					<label for="'.$modname.'filtercondvalue">Value <span class="mandatoryfildclass">*</span></label>
				</div>
				<div class="large-12 columns viewcondclear" id="'.$modname.'filterddcondvaluedivhid">
					<label>Value<span class="mandatoryfildclass">*</span></label>
					<select data-placeholder="" data-prompt-position="bottomLeft:14,36" class="validate[required] chzn-select chosenwidth valdropdownchange" tabindex="13" name="'.$modname.'filterddcondvalue" id="'.$modname.'filterddcondvalue">
						<option></option>
					</select>
				</div>
				<div class="large-12 columns viewcondclear" id="'.$modname.'filterdatecondvaluedivhid">
					<label>Value <span class="mandatoryfildclass">*</span></label>
					<input type="text" class="validate[required]" data-dateformater="<?php echo $appdateformat; ?>" id="'.$modname.'filterdatecondvalue" name="'.$modname.'filterdatecondvalue" tabindex="12" >
				</div>
				<div class="large-12 columns viewcondclear">
					<input type="hidden" class="" id="'.$modname.'finalfiltercondvalue" name="'.$modname.'finalfiltercondvalue"  tabindex="12" >
				</div>
				<div class="large-12 columns">&nbsp;</div>
					<div class="large-12 columns">
						<input type="button" style="margin-left: 33%;margin-top: 5%;width: 16%;" class="btn formbuttonsalert" id="'.$modname.'filteraddcondsubbtn" Value="Submit" name="'.$modname.'filteraddcondsubbtn"  tabindex="14" >
					</div>
				<div class="large-12 columns">&nbsp;</div></span>
				<div class="large-12 columns">
					<div id="'.$modname.'filterconddisplay" class="large-12 medium-12 small-12 columns uploadcontainerstyle uploadcontainerstyle1">
					</div>
					<input type="hidden" id="'.$modname.'conditionname" value="" name="'.$modname.'conditionname">
				</div>
				<div class="large-12 columns">&nbsp;</div></span>';
				$content .='</div></div></div></div>';
				echo $content;
				echo '</div>';
				echo '<div class="row">&nbsp;</div>';
				echo '<form method="POST" name="'.$modname.'addform" class="" action ="" id="'.$modname.'addform">';
				echo '<span id="'.$modname.'formaddwizard" class="validationEngineContainer" >';
				echo '<span id="'.$modname.'formeditwizard" class="validationEngineContainer">';
				$tabgrpid++;
				$k++;
			}
			$prevgrpsecid = "";
			$i = 100;
			foreach($grpvalarr as $seckey => $secvalarr) { //section creation
				echo '<div class="large-4 columns paddingbtm">';
				foreach($secvalarr as $fieldkey => $fieldval) { //form field creation
					$curgrpsecid = $fieldval['tabsectionid'];
					if($prevgrpsecid != $curgrpsecid) {
						$prevgrpsecid=$fieldval['tabsectionid'];
						echo '<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">';//tab section padding open
						echo '<div class="large-12 columns headerformcaptionstyle">'.$fieldval['tabsectionname'].'</div>';
						//function call for fields generation
						if($fieldval['colname'] != "salutationid") {
							$colmtypeid = $fieldval['fieldcolmtypeid'];
							echo fieldgenerate($fieldval,$i,$colmtypeid);
						}
					} else {
						//function call for fields generation
						if($fieldval['colname'] != "salutationid") {
							$colmtypeid = $fieldval['fieldcolmtypeid'];
							echo fieldgenerate($fieldval,$i,$colmtypeid);
						}
					}
					if($fieldval['uitypeid'] == 20) {
						$fieldsinfo = explode(',',$fieldval['ddparenttable']);
						if($fieldsinfo[1]!="") { $fieldnames[] = $fieldsinfo[1]; }
						if($fieldval['tabname']!="") { $fieldtables[] = $fieldval['tabname']; }
						if($fieldsinfo[1]!="") { $tabcolmnname[] = $fieldsinfo[1]; }
						if($fieldval['parenttable']!="") { $parenttabname[] = $fieldval['parenttable']; }
					}
					if($fieldval['fieldname']!="" && $fieldval['tabname']!="" && $fieldval['colname']!="" && $fieldval['parenttable']!="") {
						$fieldnames[] = $fieldval['fieldname'];
						$fieldtables[] = $fieldval['tabname'];
						$tabcolmnname[] = $fieldval['colname'];
						$parenttabname[] = $fieldval['parenttable'];
					}
					if($fieldval['uitypeid'] == 14) {
						$anfieldnames[] = $fieldval['fieldname'];
						$anfieldnameid[] = $fieldval['modfieldid'];
						$anfiledtab[] = $fieldval['tabname'];
						$anmoduleid[] = $moduleid;
					}
					if($fieldval['uitypeid'] == 15 || $fieldval['uitypeid'] == 16) {
						$attachfieldname[] = $fieldval['fieldname'];
						$attachcolname[] = $fieldval['colname'];
						$attachuitypeid[] = $fieldval['uitypeid'];
						$attachfieldid[] = $fieldval['modfieldid'];
						$fupload = "Yes";
					}
					if($fieldval['uitypeid'] == 24) {
						$editor = "Yes";
					}
					$readmode = "";
					$mandatoryopt = "";
					if($fieldval['fieldmode'] == "No") {
						$i++;
					}
					//$i++;
				}
				echo '<div class="large-12 columns submitkeyboard"  style="text-align: right">';
				echo label('&nbsp;',array());
				//update button
				$butattr = array('class'=>'hidedisplayfwg updatekeyboard btn','value'=>'Submit','tabindex'=>$i++);
				echo button(''.$modname.'dataupdatesubbtn',$butattr);
				//Add button
				$butattr = array('class'=>'addkeyboard btn','value'=>'Submit','tabindex'=>$i++);
				echo button(''.$modname.'savebutton',$butattr);
	
				echo close('div');
	
				echo '</div>'; //tab section padding close
				echo '</div>'; //tab section close
				$modulelink = $fieldval['modlink'];
			}
			echo '</span>';
			echo '</span>';
			//hidden text box contain data insert informations
			$fieldname = dataexplode($fieldnames);
			$fieldtable = dataexplode($fieldtables);
			$tabcolname = dataexplode($tabcolmnname);
			$partabname = dataexplode($parenttabname);
			$anfieldname = dataexplode($anfieldnames);
			$anfieldnameids = dataexplode($anfieldnameid);
			$anmoduleids = dataexplode($anmoduleid);
			$anmoduletable = dataexplode($anfiledtab);
			$attachfldname = dataexplode($attachfieldname);
			$attachcolname = dataexplode($attachcolname);
			$attachuitype = dataexplode($attachuitypeid);
			$attachfieldid = dataexplode($attachfieldid);
				
			echo hidden(''.$modname.'elementsname',$fieldname);
			echo hidden(''.$modname.'elementstable',$fieldtable);
			echo hidden(''.$modname.'elementscolmn',$tabcolname);
			echo hidden(''.$modname.'elementspartabname',$partabname);
			echo hidden(''.$modname.'primarydataid','');
			echo hidden(''.$modname.'sortorder','');
			echo hidden(''.$modname.'sortcolumn','');
			echo hidden(''.$modname.'anfieldnameinfo',$anfieldname);
			echo hidden(''.$modname.'anfieldnameidinfo',$anfieldnameids);
			echo hidden(''.$modname.'anfieldnamemodinfo',$anmoduleids);
			echo hidden(''.$modname.'anfieldtabinfo',$anmoduletable);
			echo hidden(''.$modname.'attachfieldnames',$attachfldname);
			echo hidden(''.$modname.'attachcolnames',$attachcolname);
			echo hidden(''.$modname.'attachuitypeids',$attachuitype);
			echo hidden(''.$modname.'attachfieldids',$attachfieldid);
			echo '</form>';
			$ds = DIRECTORY_SEPARATOR;
			$nl = explode('/',$modulelink);
			echo '<script src="'.base_url().'js'.'/'.$nl[0].'/'.strtolower($nl[0]).'.js" type="text/javascript"></script>';
				
			//Inner grid generation
			echo '<div class="large-8 columns paddingbtm '.$modname.'fullgridview">';
			echo '<div class="large-12 columns paddingzero">';
			echo '<div class="large-12 columns viewgridcolorstyle" style="padding-left:0;padding-right:0;">';
			echo '<div class="large-12 columns headerformcaptionstyle" style="height:auto;line-height:1.8rem;padding: 0.2rem 0 0rem;">';
			echo '<span class="large-6 medium-6 small-12 columns small-only-text-center lefttext">'.$grpkey.' List<span class="fwgpaging-box '.$modname.'gridfootercontainer"></span></span>';
			$spattr = array("class"=>"large-6 medium-6 small-12 columns innergridicon innergridiconhover small-only-text-center righttext");
			echo spanopen($spattr);
			echo close('span');
			echo '</div>';
			echo '<div class="large-12 columns forgetinggridname" id="'.$modname.'addgridwidth" style="padding-left:0;padding-right:0;">';
			if($gridtype=='grid') {
				echo '<table id="'.$modname.'addgrid"> </table> ';
				echo '<div id="'.$modname.'addgridnav" class="w100"></div> ';
			} else {
				echo '<div class="desktop row-content inner-gridcontent  viewgridcolorstyle borderstyle" id="'.$modname.'addgrid" style="height:450px;top:0px;">';
				echo '</div>';
				echo '<div class="footercontainer footer-content" id="'.$modname.'addgridfooter">';
				echo '</div>';
			}
			echo '</div>';
			echo '</div>';
			echo '</div>';
			echo '</div>';
			echo '<div class="row">&nbsp;</div>';
			echo "</div>";//add form container close
			echo "</div>"; //tab group close
			if($editor == "Yes") {
				froalaeditorfileslinks();
			}
			if($fupload == "Yes") {
				echo '<script type="text/javascript" src="https://js.live.net/v6.0/OneDrive.js" id="onedrive-js" client-id="000000004C18E325"></script>';
			}
		}
	}
	//form fields generation
	function fieldgenerate($fieldval,$tabindex,$colmtypeid) {
		//instead of this ( This will replace (this->) pointer )
		$CI =& get_instance();
		//declare module name
		$str = str_replace(" ","",$fieldval['modulename']);
		$modulename = strtolower($str);
		$mlength="";
		if($fieldval['maxlength'] != '0') {
			$mlength = ",maxSize[".$fieldval['maxlength']."]";
		}
		//custom validation
		$rule="";
		if($fieldval['customrule'] != "") {
			$rule = ','.$fieldval['customrule'];
		}
		//unique validation
		$unirule="";
		if($fieldval['modfieldunique'] == "Yes") {
			$unirule = ',funcCall[uniquefieldvalidate]';
		}
		$mandatoryopt = ""; //mandatory option set
		$mandlab = "";
		if($fieldval['mandmode'] == "Yes") {
			$mandatoryopt = 'validate[required'.$mlength.$rule.']';
			$mandlab = "<span class='mandatoryfildclass'>*</span>";
		} else {
			if($fieldval['maxlength'] != '0') {
				$mandatoryopt = "validate[maxSize[".$fieldval['maxlength'].$rule."]]";
			}
		}
		//readonly mode set
		$readmode = "";
		if($fieldval['readmode'] == "Yes") {
			$readmode = ' readonly';
		}
		//active mode set
		$fildmode = "";
		if($fieldval['fieldmode'] == "No") {
			$fildmode = ' hidedisplay';
			$tabindex = '';
		}
		//field column style
		if($colmtypeid == 1) {
			$colstyle = "large-12 medium-12 small-12 columns";
		} else if($colmtypeid == 2) {
			$colstyle = "large-6 medium-6 small-6 columns";
		}
		switch( $fieldval['uitypeid'] ) {
			case 2:
				//text box
				if($fieldval['mandmode'] == "Yes") {
					$mandatoryopt = 'validate[required'.$mlength.$rule.']';
					$salmandatoryopt = 'validate[required]';
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				} else {
					$mandatoryopt = '';
					$salmandatoryopt = '';
				}
				if($fieldval['fieldname'] == "name") {
					$txtval="";
					echo '<div class="static-field large-12 columns'.$fildmode.'" id="salutationiddivhid">';
					$attr=array();
					$labval = $fieldval['filedlabel'].$mandlab;
					echo label($labval,$attr);
					$name = 'salutationid';
					$value = array();
					$defval = array();
					$tablename = 'salutation';
					$tabid = $tablename.'id';
					$tabfieldname = $tablename.'name';
					$ddowndata = $CI->Basefunctions->dynamicdropdownvalfetch($fieldval['moduleid'],$tablename,$tabfieldname,$tabid);
					foreach($ddowndata as $key):
						$value[$key->Id] = $key->Name;
						if( $key->defaultopt == 'Yes' || $key->defaultopt == '1' ) {
							$defval[] = $key->Id;
							$txtval = $key->Id;
						}
					endforeach;
					$txtval = ( ($txtval!='')? $txtval : 1 );
					$valoption = "id";
					$dataattr = array('salutationidhidden'=>'id');
					$seldataattr = array("placeholder"=>"","prompt-position"=>"topRight:20,35",'defvalattr'=>''.$txtval.'');
					$option = array('style'=>"width:28%;display:inline-block;position:relative;top:-5px;","class"=>'chzn-select ddcahangeclass '.$salmandatoryopt.'',"tabindex"=>"".$tabindex."",'data-ddid'=>"salutationid","data"=>$seldataattr);
					$multiple = $fieldval['multiple'];
					echo dropdown($name,$value,$valoption,$dataattr,$multiple,$option,$defval); 
					$txtname = $fieldval['fieldname'];
					$txtval = $fieldval['defvalue'];
					$txtattr = array('style'=>'width:72%;display:inline-block;height:1.78rem;text-indent:10px;','class'=>''.$mandatoryopt.'','tabindex'=>''.$tabindex.'','readonly'=>''.$readmode.'','data-defvalattr'=>''.$txtval.'');
					echo text($txtname,$txtval,$txtattr);
					//echo hidden('salutationid','');
					echo '</div>';
					break;
				} else {
					if($fieldval['mandmode'] == "Yes") {
						$mandatoryopt = 'validate[required'.$mlength.$rule.']';
						$mandlab = "<span class='mandatoryfildclass'>*</span>";
					} else {
						if($fieldval['maxlength'] != '0') {
							$mandatoryopt = "validate[maxSize[".$fieldval['maxlength'].$rule."]]";
						}
					}
					$labval = $fieldval['filedlabel'].$mandlab;
					$txtname = $fieldval['fieldname'];
					$labattr = array('for'=>''.$txtname.'','class'=>'');
					$txtval = $fieldval['defvalue'];
					$divattr = array('class'=>'input-field '.$colstyle.''.$fildmode.'','id'=>''.$txtname.'divhid');
					$typeinfo = "text";
					$txtattr = array('class'=>''.$mandatoryopt.'','type'=>''.$typeinfo.'','tabindex'=>''.$tabindex.'','readonly'=>''.$readmode.'','data-prompt-position'=>'bottomLeft','data-defvalattr'=>''.$txtval.'');
					return textspan($divattr,$labval,$labattr,$txtname,$txtval,$txtattr);
					break;
				}

			case 3:
				//text area
				$rowsize = $fieldval['decisize'];
				$labval = $fieldval['filedlabel'].$mandlab;
				$txtname = $fieldval['fieldname'];
				$labattr = array('for'=>''.$txtname.'','class'=>'');
				$txtval = $fieldval['defvalue'];
				$divattr = array('class'=>'input-field '.$colstyle.''.$fildmode.'','id'=>''.$txtname.'divhid');
				$typeinfo = "text";
				$txtattr = array('class'=>'materialize-textarea '.$mandatoryopt.'','type'=>''.$typeinfo.'','tabindex'=>''.$tabindex.'','readonly'=>''.$readmode.'','rows'=>''.$rowsize.'','data-prompt-position'=>'bottomLeft:1,50','data-defvalattr'=>''.$txtval.'');
				return textareaspan($divattr,$labval,$labattr,$txtname,$txtval,$txtattr);
				break;

			case 4:
				//Integer
				$mandlab = "";
				if($fieldval['mandmode'] == "Yes") {
					$mandatoryopt = 'validate[required,custom[integer]'.$mlength.$rule.']';
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				} else {
					$mandatoryopt = 'validate[custom[integer]'.$mlength.$rule.']';
				}
				$labval = $fieldval['filedlabel'].$mandlab;
				$txtname = $fieldval['fieldname'];
				$labattr = array('for'=>''.$txtname.'','class'=>'');
				$txtval = $fieldval['defvalue'];
				$divattr = array('class'=>'input-field '.$colstyle.''.$fildmode.'','id'=>''.$txtname.'divhid');
				$typeinfo = "tel";
				$txtattr = array('class'=>''.$mandatoryopt.'','type'=>''.$typeinfo.'','tabindex'=>''.$tabindex.'','readonly'=>''.$readmode.'','data-prompt-position'=>'bottomLeft','data-defvalattr'=>''.$txtval.'');
				return textspan($divattr,$labval,$labattr,$txtname,$txtval,$txtattr);
				break;

			case 5:
				//Decimal
				$mandlab = "";
				$decsize = $fieldval['decisize'];
				if($decsize == '') { $decsize=2; }
				if($fieldval['mandmode'] == "Yes") {
					$mandatoryopt = 'validate[required,custom[number],decval['.$decsize.']'.$mlength.$rule.']';
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				} else {
					$mandatoryopt = 'validate[custom[number],decval['.$decsize.']'.$mlength.$rule.']';
				}
				$labval = $fieldval['filedlabel'].$mandlab;
				$txtname = $fieldval['fieldname'];
				$labattr = array('for'=>''.$txtname.'','class'=>'');
				$txtval = $fieldval['defvalue'];
				$typeinfo = "number";
				$divattr = array('class'=>'input-field '.$colstyle.''.$fildmode.'','id'=>''.$txtname.'divhid');
				$txtattr = array('class'=>''.$mandatoryopt.'','type'=>''.$typeinfo.'','tabindex'=>''.$tabindex.'','readonly'=>''.$readmode.'','data-prompt-position'=>'bottomLeft','data-defvalattr'=>''.$txtval.'');
				return textspan($divattr,$labval,$labattr,$txtname,$txtval,$txtattr);
				break;

			case 6:
				//Percentage
				$mandlab = "";
				$decsize = $fieldval['decisize'];
				if($decsize == '') { $decsize=2; }
				if($fieldval['mandmode'] == "Yes") {
					$mandatoryopt = 'validate[required,custom[number],min[0.1],max[100],decval['.$decsize.']'.$rule.']';
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				} else {
					$mandatoryopt = 'validate[custom[number],min[0.1],max[100],decval['.$decsize.']'.$rule.']';
				}
				$labval = $fieldval['filedlabel'].$mandlab;
				$txtname = $fieldval['fieldname'];
				$labattr = array('for'=>''.$txtname.'','class'=>'');
				$txtval = $fieldval['defvalue'];
				$divattr = array('class'=>'input-field '.$colstyle.''.$fildmode.'','id'=>''.$txtname.'divhid');
				$typeinfo = "number";
				$txtattr = array('class'=>''.$mandatoryopt.'','type'=>''.$typeinfo.'','tabindex'=>''.$tabindex.'','readonly'=>''.$readmode.'','data-prompt-position'=>'bottomLeft','data-defvalattr'=>''.$txtval.'');
				return textspan($divattr,$labval,$labattr,$txtname,$txtval,$txtattr);
				break;

			case 7:
				//Currency
				$mandlab = "";
				$decsize = $fieldval['decisize'];
				if($decsize == '') { $decsize=2; }
				if($fieldval['mandmode'] == "Yes") {
					$mandatoryopt = 'validate[required,custom[number],decval['.$decsize.']'.$mlength.$rule.']';
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				} else {
					$mandatoryopt = 'validate[custom[number],decval['.$decsize.']'.$mlength.$rule.']';
				}
				$labval = $fieldval['filedlabel'].$mandlab;
				$txtname = $fieldval['fieldname'];
				$labattr = array('for'=>''.$txtname.'','class'=>'');				
				$txtval = $fieldval['defvalue'];
				$divattr = array('class'=>'input-field '.$colstyle.''.$fildmode.'','id'=>''.$txtname.'divhid');
				$txtattr = array('class'=>''.$mandatoryopt.'','tabindex'=>''.$tabindex.'','readonly'=>''.$readmode.'','data-prompt-position'=>'bottomLeft','data-defvalattr'=>''.$txtval.'');
				return textspan($divattr,$labval,$labattr,$txtname,$txtval,$txtattr);
				break;

			case 8:
				//Date
				$mindate = '0'; //feature date [null-enable / 0-disable]
				$maxdate = 'null'; //past date [null-enable / 0-disable]
				$chmonth = 'null';
				$chyear = 'null';	
				$currentdate = '';
				if($fieldval['ddparenttable']!='') {
					$restrict = explode('|',$fieldval['ddparenttable']);
					$mindate = ($restrict[0]=='No'?'null':'0');
					$maxdate = ($restrict[1]=='No'?'null':'0');
					if(isset($restrict[2])){
						$chmonth = ($restrict[2]=='No'?'null':'0');
						$chyear = ($restrict[2]=='No'?'null':'0');
					}					
					if($restrict[0] === 'Yes' && $restrict[1] === 'Yes'){
						$mindate = 'null';
						$maxdate = 'null';
					}
					if(isset($restrict[2])){
						if($restrict[2] === 'Yes'){
							$chmonth = true;
							$chyear = true;
						}	
					}					
				}				
				$appdateformat = $CI->Basefunctions->appdateformatfetch();
				$mandlab = "";
				$mandatoryopt = ""; //mandatory option set
				if($fieldval['mandmode'] == "Yes") {
					$mandatoryopt = 'validate[required'.$rule.']';
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				}
				echo "<script>
					$(document).ready(function(){
						$('#".$fieldval['fieldname']."').datetimepicker({
							dateFormat: '".$appdateformat."',
							showTimepicker :false,
							minDate: ".$mindate.",
							maxDate: ".$maxdate.",
							changeMonth: ".$chmonth.",
							changeYear: ".$chyear.",	
							yearRange : '1947:c+100',
							onSelect: function () {
								var checkdate = $(this).val();
								if(checkdate != '') {
									$(this).next('span').remove('.btmerrmsg'); 
									$(this).removeClass('error');
									$(this).prev('div').remove();
								}
							},
							onClose: function () {
								$('#".$fieldval['fieldname']."').focus();
							}
						}).click(function() {
							$(this).datetimepicker('show');
						});";
					if(isset($restrict[3])){
						if($restrict[3]== 'Yes'){
							$currentdate = date('d-m-Y');
							echo "$('#".$fieldval['fieldname']."').datetimepicker('setDate', '".$currentdate."' );";
						}else{
							if($fieldval['defvalue']!="") {
								echo "$('#".$fieldval['fieldname']."').datetimepicker('setDate', '".$fieldval['defvalue']."' );";
								$currentdate = $fieldval['defvalue'];
							}
						}
					}else{
						if($fieldval['defvalue']!="") {
							echo "$('#".$fieldval['fieldname']."').datetimepicker('setDate', '".$fieldval['defvalue']."' );";
							$currentdate = $fieldval['defvalue'];
						}
					}
					
				echo "});
					</script> ";
				$labval = $fieldval['filedlabel'].$mandlab;
				$txtname = $fieldval['fieldname'];
				$labattr = array('for'=>''.$txtname.'','class'=>'');
				$txtval = $currentdate;
				$typeinfo = "date";
				$divattr = array('class'=>'static-field '.$colstyle.''.$fildmode.'','id'=>''.$txtname.'divhid');
				$txtattr = array('class'=>''.$mandatoryopt.' fordatepicicon','type'=>''.$typeinfo.'','tabindex'=>''.$tabindex.'','readonly'=>'readonly','data-prompt-position'=>'bottomLeft','data-defvalattr'=>''.$txtval.'','data-dateformater'=>''.$appdateformat.'');
				return datetimetextspan($divattr,$labval,$labattr,$txtname,$txtval,$txtattr);				
				break;
			
			case 9:
				//time
				$mandlab = "";
				$mandatoryopt = ""; //mandatory option set
				if($fieldval['mandmode'] == "Yes") {
					$mandatoryopt = 'validate[required'.$rule.']';
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				}
				$time = "";
				if($fieldval['defvalue'] != '') {
					$time = "'setTime':'".$fieldval['defvalue']."',";
				}
				echo "<script>
					$(document).ready(function(){
						$('#".$fieldval['fieldname']."').timepicker({
							'step':15,
							'timeFormat': 'H:i',
							'scrollDefaultNow':true
						});
						$('#".$fieldval['fieldname']."').on('changeTime', function() {
						    $('#".$fieldval['fieldname']."').focus();
						});
					});
					</script>";
				$labval = $fieldval['filedlabel'].$mandlab;
				$txtname = $fieldval['fieldname'];
				$labattr = array('for'=>''.$txtname.'','class'=>'');
				$txtval = $fieldval['defvalue'];
				$divattr = array('class'=>'input-field '.$colstyle.''.$fildmode.'','id'=>''.$txtname.'divhid');
				$txtattr = array('class'=>''.$mandatoryopt.' fortimepicicon','tabindex'=>''.$tabindex.'','data-prompt-position'=>'bottomLeft','data-defvalattr'=>''.$txtval.'');
				return datetimetextspan($divattr,$labval,$labattr,$txtname,$txtval,$txtattr);
				break;
			
			case 10:
				//Email
				$mandlab = "";
				if($fieldval['mandmode'] == "Yes") {
					$mandatoryopt = 'validate[required,custom[multimail],minSize[0],maxSize[100]'.$rule.$unirule.']';
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				} else {
					$mandatoryopt = 'validate[custom[multimail],minSize[0],maxSize[100]'.$rule.$unirule.']';
				}
				$labval = $fieldval['filedlabel'].$mandlab;
				$txtname = $fieldval['fieldname'];
				$labattr = array('for'=>''.$txtname.'','class'=>'');
				$txtval = $fieldval['defvalue'];
				$typeinfo = "email";
				$divattr = array('class'=>'input-field '.$colstyle.''.$fildmode.'','id'=>''.$txtname.'divhid');
				$txtattr = array('class'=>''.$mandatoryopt.'','type'=>''.$typeinfo.'','tabindex'=>''.$tabindex.'','readonly'=>''.$readmode.'','data-prompt-position'=>'bottomLeft','data-defvalattr'=>''.$txtval.'','data-colmname'=>''.$fieldval['colname'].'','data-tablname'=>''.$fieldval['tabname'].'');
				return textspan($divattr,$labval,$labattr,$txtname,$txtval,$txtattr);
				break;		
			case 11:
				//Phone
				$mandlab = "";
				if($fieldval['mandmode'] == "Yes") {
					$mandatoryopt = 'validate[required,custom[phone]'.$rule.$unirule.']';
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				} else {
					$mandatoryopt = 'validate[custom[phone]'.$rule.$unirule.']';
				}
				$labval = $fieldval['filedlabel'].$mandlab;
				$txtname = $fieldval['fieldname'];
				$labattr = array('for'=>''.$txtname.'','class'=>'');
				$txtval = $fieldval['defvalue'];
				$divattr = array('class'=>'input-field '.$colstyle.''.$fildmode.'','id'=>''.$txtname.'divhid');
				$typeinfo = "number";
				$txtattr = array('class'=>''.$mandatoryopt.'','type'=>''.$typeinfo.'','tabindex'=>''.$tabindex.'','readonly'=>''.$readmode.'','data-prompt-position'=>'bottomLeft','data-defvalattr'=>''.$txtval.'','data-colmname'=>''.$fieldval['colname'].'','data-tablname'=>''.$fieldval['tabname'].'');
				return textspan($divattr,$labval,$labattr,$txtname,$txtval,$txtattr);
				break;

			case 12:
				//URL
				$mandlab = "";
				if($fieldval['mandmode'] == "Yes") {
					$mandatoryopt = 'validate[required,custom[csturl]'.$mlength.$rule.']';
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				} else {
					$mandatoryopt = 'validate[custom[csturl]'.$mlength.$rule.']';
				}
				$labval = $fieldval['filedlabel'].$mandlab;
				$txtname = $fieldval['fieldname'];
				$labattr = array('for'=>''.$txtname.'','class'=>'');
				$txtval = $fieldval['defvalue'];
				$divattr = array('class'=>'input-field '.$colstyle.''.$fildmode.'','id'=>''.$txtname.'divhid');
				$typeinfo = "url";
				$txtattr = array('class'=>''.$mandatoryopt.'','type'=>''.$typeinfo.'','tabindex'=>''.$tabindex.'','readonly'=>''.$readmode.'','data-prompt-position'=>'bottomLeft','data-defvalattr'=>''.$txtval.'');
				return textspan($divattr,$labval,$labattr,$txtname,$txtval,$txtattr);
				break;

			case 13:
				//check box
				$mandatoryopt = ""; //mandatory option set
				$mandlab = "";
				$defval = $fieldval['defvalue'];
				$chkattr = "";
				$chkval = "No";
				if($defval == 'Yes') {
					$chkattr = " checked='checked'";
					$chkval = "Yes";
				} else {
					$chkval = "No";
				}
				if($fieldval['mandmode'] == "Yes") {
					$mandatoryopt = 'validate[required'.$rule.']';
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				}
				if($fieldval['ddparenttable']!='') {
					$condition = explode('|',$fieldval['ddparenttable']);
					echo "<script>
					$(document).ready(function(){							
						$('#".$fieldval['fieldname']."cboxid').click(function() {
							if ($(this).is(':checked')) {
								$('#".$condition[1]."').show();
							} else {
								$('#".$condition[1]."').hide();		
							}
						});
					});
					</script>";
				}				
				echo '<div class="input-field '.$colstyle.''.$fildmode.'" id="'.$fieldval['fieldname'].'divhid">';
				echo '<input type="checkbox" id="'.$fieldval['fieldname'].'cboxid" name="'.$fieldval['fieldname'].'cboxid" class="checkboxcls radiochecksize filled-in '.$mandatoryopt.'" data-hidname="'.$fieldval['fieldname'].'"'.$chkattr.' tabindex='.$tabindex.' />';
				echo '<label for="'.$fieldval['fieldname'].'cboxid">'.$fieldval['filedlabel'].$mandlab.'</label>';
				echo '<input name="'.$fieldval['fieldname'].'" type="hidden" id="'.$fieldval['fieldname'].'" value="'.$chkval.'"  data-defvalattr="'.$chkval.'" />';
				echo '</div>';
				break;

			case 14:
				//Auto Number
				$mandlab = "";
				$mandatoryopt = ""; //mandatory option set
				if($fieldval['mandmode'] == "Yes") {
					$mandatoryopt = 'validate[required'.$mlength.$rule.']';
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				} else {
					$mandatoryopt = "";
				}
				$labval = $fieldval['filedlabel'].$mandlab;
				$txtname = $fieldval['fieldname'];
				$labattr = array('for'=>''.$txtname.'','class'=>'');
				$txtval = $fieldval['defvalue'];
				$divattr = array('class'=>'input-field '.$colstyle.''.$fildmode.'','id'=>''.$txtname.'divhid');
				$txtattr = array('class'=>''.$mandatoryopt.'','tabindex'=>''.$tabindex.'','readonly'=>''.$readmode.'','data-prompt-position'=>'bottomLeft','data-defvalattr'=>''.$txtval.'');
				return textspan($divattr,$labval,$labattr,$txtname,$txtval,$txtattr);
				break;

			case 15:
				//Attachment
				$mulfile = $fieldval['multiple']=='Yes' ? 'true' : 'false';
				echo '<div class="'.$colstyle.''.$fildmode.'" id="'.$fieldval['fieldname'].'divhid">';
				echo '<label>'.$fieldval['filedlabel'].$mandlab.'</label>';
			echo '<span class="driveiconsize filechooserdrive" id="desk_'.$fieldval['fieldname'].'" data-type="desktop" data-buttonid="'.$fieldval['fieldname'].'" title="From Computer"><i class="material-icons">computer</i></span>&nbsp;';
				$industryid = $CI->Basefunctions->industryid;
				if($fieldval['moduleid'] == '207') {
				echo '<span class="icon24 icon24-add_a_photo driveiconsize" id="imgbox_'.$fieldval['fieldname'].'" data-type="imgbox" data-buttonid="imgbox_'.$fieldval['fieldname'].'" title="Capture Image"></span>';
				}
				//drop box
				echo '<span style="display:none;" id="spandbchoose_'.$fieldval['fieldname'].'"><input type="dropbox-chooser" name="selected-file" id="dbchoose_'.$fieldval['fieldname'].'" data-multiselect="'.$mulfile.'" data-link-type="preview" data-extensions="'.implode(',',explode('|',$fieldval['customrule'])).'" /></span>';
				echo '</div>';
				echo hidden($fieldval['fieldname'],'');
				echo '<div class="large-12 medium-12 small-12 columns'.$fildmode.'">&nbsp</div>';
				echo '<div class="large-12 medium-12 small-12 columns'.$fildmode.'">';
				echo '<div class="large-12 medium-12 small-12 columns uploadcontainerstyle'.$fildmode.' card-panel" style="height:10rem; border:1px solid #CCCCCC;overflow-y:auto;overflow-x:hidden;" id="'.$fieldval['fieldname'].'attachdisplay">';
				echo '</div>';
				echo '</div>';
				/* File Upload overlay [from computer]*/
				echo '<div class="large-12 columns" style="position:absolute;">
						<div class="overlay alertsoverlay overlayalerts" id="'.$fieldval['fieldname'].'uploadoverlay" style="overflow-y: scroll;overflow-x: hidden">
						<div class="row">&nbsp;</div>
						<div class="row">&nbsp;</div>
						<div class="row">&nbsp;</div>
						<div class="row">&nbsp;</div>
						<div class="row">&nbsp;</div>
						<div class="overlaybgborder" >
							<div class="row">&nbsp;</div>
							<div class="large-4 medium-6 large-centered medium-centered columns newcleardataform" style="left: 15px;position:relative;">
							<div class="large-12 columns sectionheaderformcaptionstyle">File Upload</div>
								<div class="row">&nbsp;</div>
								<div class="row">&nbsp;</div>
								<div class="large-12 medium-12 small-12 columns">  
									<span id="'.$fieldval['fieldname'].'mulitplefileuploader">upload</span>
								</div>
								<div class="row">&nbsp;</div>
								<div class="row">&nbsp;</div>
								<div class="small-12 small-centered medium-12 large-12 columns text-right">
									<input type="button" id="'.$fieldval['fieldname'].'overlayclose" name="'.$fieldval['fieldname'].'overlayclose" value="Cancel" class="alertbtnno" tabindex="1001">
								</div>
								<div class="row">&nbsp;</div>
							</div>
							<div class="row">&nbsp;</div>
						</div>
					</div>
				</div>';
				/* file upload script */
				$mulfile = $fieldval['multiple']=='Yes' ? 'true' : 'false';
				$types = implode(',',explode('|',$fieldval['customrule']));
				$upfiletypes = str_replace( '.','',$types );
				$upfiletypes = $upfiletypes!=''? $upfiletypes : '*';
				echo '<script>
					$(document).ready(function(){
						/* close file upload overlay */
						$("#'.$fieldval['fieldname'].'overlayclose").click(function(){
							$("#'.$fieldval['fieldname'].'uploadoverlay").hide();
							$(".ajax-file-upload-error").remove();
						});
						/* file upload btn click */
						$("#'.$fieldval['fieldname'].'btn").click(function(){
							var count = fname_'.$fieldval['modfieldid'].'.length;
							if(count >= 10){
								alertpopup("Upto 10 files can only be uploaded at a time");
							} else {
								$(".triggerupload'.$fieldval['fieldname'].'fileupload:last").trigger("click");
							}
						});
						/* file upload variable declaration */
						var filecount =0; 
						var newfilecount =-1;
						fileuploadmoname = "'.$fieldval['fieldname'].'fileupload";
						fname_'.$fieldval['modfieldid'].' = [];
						fsize_'.$fieldval['modfieldid'].' = [];
						ftype_'.$fieldval['modfieldid'].' = [];
						fpath_'.$fieldval['modfieldid'].' = [];
						upfrom_'.$fieldval['modfieldid'].' = [];
						/* File upload from computer */
						var '.$fieldval['fieldname'].'settings = {
							url: base_url+"upload.php",
							method: "POST",
							fileName: "myfile",
							multiple: "'.$mulfile.'",
							allowedTypes: "'.$upfiletypes.'",
							dataType:"json",
							async:false,
							//maxFileCount: 10,		
							onSelect: function (files) {
				            	filecount = files.length;
				            },
							onSuccess:function(files,data,xhr) {
								$("#processoverlay").show();
								console.log(filecount);
								console.log(newfilecount);
								newfilecount++;
								var count = fname_'.$fieldval['modfieldid'].'.length;
								var fdelid = count-1;
								if(data != "Size" && data != "MaxSize" && data != "MaxFile" && data != "LowSize") {
									//setTimeout(function(){
									var arr =$.parseJSON(data);
									if(newfilecount < filecount) {
										//file mapping
										fname_'.$fieldval['modfieldid'].'.push(arr.fname);
										fsize_'.$fieldval['modfieldid'].'.push(arr.fsize);
										ftype_'.$fieldval['modfieldid'].'.push(arr.ftype);
										fpath_'.$fieldval['modfieldid'].'.push(arr.path);
										upfrom_'.$fieldval['modfieldid'].'.push("2");
										$("#'.$fieldval['fieldname'].'").val(fname_'.$fieldval['modfieldid'].');
										$("#'.$fieldval['fieldname'].'_size").val(fsize_'.$fieldval['modfieldid'].');
										$("#'.$fieldval['fieldname'].'_type").val(ftype_'.$fieldval['modfieldid'].');
										$("#'.$fieldval['fieldname'].'_path").val(fpath_'.$fieldval['modfieldid'].');
										$("#'.$fieldval['fieldname'].'_fromid").val(upfrom_'.$fieldval['modfieldid'].');';
												echo 'var spantag = $("<span style=';echo "'word-wrap:break-word;'"; echo '>"+arr.fname+" ("+arr.fsize+" )</span><i data-fileid='; echo '"+fdelid+"'; echo " class='material-icons documentsdownloadclsbtn ".$fieldval['fieldname']."docclsbtn'>close</i><br/>"; echo '");';
											echo 'spantag.appendTo("#'.$fieldval['fieldname'].'attachdisplay");
											//delete files
											$(".'.$fieldval['fieldname'].'docclsbtn").click(function() {
												var id = $(this).data("fileid");
												var count = fname_'.$fieldval['modfieldid'].'.length;
												for(var k=0;k<count;k++) {
													if( k == id) {
														fname_'.$fieldval['modfieldid'].'.splice($.inArray(fname_'.$fieldval['modfieldid'].'[id],fname_'.$fieldval['modfieldid'].'),1);
														fsize_'.$fieldval['modfieldid'].'.splice($.inArray(fsize_'.$fieldval['modfieldid'].'[id],fsize_'.$fieldval['modfieldid'].'),1);
														ftype_'.$fieldval['modfieldid'].'.splice($.inArray(ftype_'.$fieldval['modfieldid'].'[id],ftype_'.$fieldval['modfieldid'].'),1);
														fpath_'.$fieldval['modfieldid'].'.splice($.inArray(fpath_'.$fieldval['modfieldid'].'[id],fpath_'.$fieldval['modfieldid'].'),1);
														upfrom_'.$fieldval['modfieldid'].'.splice($.inArray(upfrom_'.$fieldval['modfieldid'].'[id],upfrom_'.$fieldval['modfieldid'].'),1);
														setTimeout(function(){
															$("#'.$fieldval['fieldname'].'").val(fname_'.$fieldval['modfieldid'].');
															$("#'.$fieldval['fieldname'].'_size").val(fsize_'.$fieldval['modfieldid'].');
															$("#'.$fieldval['fieldname'].'_type").val(ftype_'.$fieldval['modfieldid'].');
															$("#'.$fieldval['fieldname'].'_path").val(fpath_'.$fieldval['modfieldid'].');
															$("#'.$fieldval['fieldname'].'_fromid").val(upfrom_'.$fieldval['modfieldid'].');
														},100);
													}
												}
												$(this).prev("span").remove();
												$(this).next("br").remove();
												$(this).remove();
											});
											if(newfilecount == filecount-1){
												$(".ajax-file-upload-error").remove();			
												$(".ajax-file-upload-container").empty();
												$("#'.$fieldval['fieldname'].'uploadoverlay").hide();
												filecount =0; 
												newfilecount =-1;		
												$("#processoverlay").hide();		
												alertpopupdouble("File uploaded successfully");				
											}							
										} else {
											$(".ajax-file-upload-error").remove();			
											$(".ajax-file-upload-container").empty();
											$("#'.$fieldval['fieldname'].'uploadoverlay").hide();
											filecount =0; 
											newfilecount =-1;
											$("#processoverlay").hide();		
											alertpopupdouble("File uploaded successfully");
										} 
									//},100);
								} else if(data == "MaxSize") {
									alertpopupdouble("You have reached your maximun storage limit. if you want to upload documents you want to buy the storage limit from add ons");
									$("#processoverlay").hide();
									$(".ajax-file-upload-container").empty();
									$("#'.$fieldval['fieldname'].'uploadoverlay").hide();
								} else if(data == "MaxFile") {
									alertpopupdouble("Upto 10 files can only be uploaded at a time. "+count+" files Uploaded Successfully");
									$("#processoverlay").hide();
									//$(".ajax-file-upload-container").empty();
									$("#'.$fieldval['fieldname'].'uploadoverlay").hide();
								} else if(data == "Size") {
									alertpopupdouble("Maximum File size can be 5mb only");
									$("#processoverlay").hide();
									$(".ajax-file-upload-container").empty();
									$("#'.$fieldval['fieldname'].'uploadoverlay").hide();
								} else if(data == "LowSize") {
									alertpopupdouble("File Size is too low");
									$("#processoverlay").hide();
									$(".ajax-file-upload-container").empty();
									$("#'.$fieldval['fieldname'].'uploadoverlay").hide();
								} else {
									alertpopupdouble("Error Uploading File. Please Try Again");
									$("#processoverlay").hide();
									$(".ajax-file-upload-container").empty();
									$("#'.$fieldval['fieldname'].'uploadoverlay").hide();
								}
							},
							onError: function(files,status,errMsg) {
								$("#processoverlay").hide();
							}
						}
						//file upload settings
						$("#'.$fieldval['fieldname'].'mulitplefileuploader").uploadFile('.$fieldval['fieldname'].'settings);
						
						//for box file picker
						$("#box_'.$fieldval['fieldname'].'").click(function(){
							var count = fname_'.$fieldval['modfieldid'].'.length;
							boxSelect.launchPopup();
						});
						var boxoptions = {
						    clientId: "w3ujqsxjp70fkmf3yg8in02tqzkhudke",
						    linkType: "direct",
						    multiselect: "true"
						};
						var boxSelect = new BoxSelect(boxoptions);
						// Register a success callback handler
						boxSelect.success(function(response) {
							for(var i=0; i < response.length; i++) {
								console.log(response);
								var count = fname_'.$fieldval['modfieldid'].'.length;
								var fdelid = count-1;
								var bytes = response[i].size+" KB";
								var type = "image/png";
						  		fname_'.$fieldval['modfieldid'].'.push(response[i].name);
								fsize_'.$fieldval['modfieldid'].'.push(bytes);
								ftype_'.$fieldval['modfieldid'].'.push(type);
								fpath_'.$fieldval['modfieldid'].'.push(response[i].url);
								upfrom_'.$fieldval['modfieldid'].'.push("5");
								$("#'.$fieldval['fieldname'].'").val(fname_'.$fieldval['modfieldid'].');
								$("#'.$fieldval['fieldname'].'_size").val(fsize_'.$fieldval['modfieldid'].');
								$("#'.$fieldval['fieldname'].'_type").val(ftype_'.$fieldval['modfieldid'].');
								$("#'.$fieldval['fieldname'].'_path").val(fpath_'.$fieldval['modfieldid'].');
								$("#'.$fieldval['fieldname'].'_fromid").val(upfrom_'.$fieldval['modfieldid'].');';
							echo 'var spantag = $("<span style=';echo "'word-wrap:break-word;'"; echo '>"+response[i].name+" ("+bytes+")</span><i data-fileid='; echo '"+fdelid+"'; echo " class='material-icons icon24 documentsdownloadclsbtn ".$fieldval['fieldname']."docclsbtn'>close</i><br/>"; echo '");';
								echo 'spantag.appendTo("#'.$fieldval['fieldname'].'attachdisplay");
								$(".'.$fieldval['fieldname'].'docclsbtn").click(function() {
									var id = $(this).data("fileid");
									var count = fname_'.$fieldval['modfieldid'].'.length;
									for(var k=0;k<count;k++) {
										if( k == id) {
											fname_'.$fieldval['modfieldid'].'.splice($.inArray(fname_'.$fieldval['modfieldid'].'[id],fname_'.$fieldval['modfieldid'].'),1);
											fsize_'.$fieldval['modfieldid'].'.splice($.inArray(fsize_'.$fieldval['modfieldid'].'[id],fsize_'.$fieldval['modfieldid'].'),1);
											ftype_'.$fieldval['modfieldid'].'.splice($.inArray(ftype_'.$fieldval['modfieldid'].'[id],ftype_'.$fieldval['modfieldid'].'),1);
											fpath_'.$fieldval['modfieldid'].'.splice($.inArray(fpath_'.$fieldval['modfieldid'].'[id],fpath_'.$fieldval['modfieldid'].'),1);
											upfrom_'.$fieldval['modfieldid'].'.splice($.inArray(upfrom_'.$fieldval['modfieldid'].'[id],upfrom_'.$fieldval['modfieldid'].'),1);
											setTimeout(function(){
												$("#'.$fieldval['fieldname'].'").val(fname_'.$fieldval['modfieldid'].');
												$("#'.$fieldval['fieldname'].'_size").val(fsize_'.$fieldval['modfieldid'].');
												$("#'.$fieldval['fieldname'].'_type").val(ftype_'.$fieldval['modfieldid'].');
												$("#'.$fieldval['fieldname'].'_path").val(fpath_'.$fieldval['modfieldid'].');
												$("#'.$fieldval['fieldname'].'_fromid").val(upfrom_'.$fieldval['modfieldid'].');
											},100);
										}
									}
									$(this).prev("span").remove();
									$(this).next("br").remove();
									$(this).remove();
								});
								$("#processoverlay").hide();
								alertpopup("File uploaded successfully");	
							}
						});
						// Register a cancel callback handler
						boxSelect.cancel(function() {
						    console.log("The user clicked cancel or closed the popup");
						});
						//boxSelect.launchPopup();
						boxSelect.closePopup();
						//var isSupported = boxSelect.isBrowserSupported();
						
						//Drop Box
						/* Drop box file link */
						$("#db_'.$fieldval['fieldname'].'").click(function(){
							var count = fname_'.$fieldval['modfieldid'].'.length;
							Dropbox.choose(options);
						});
						options = {success: function(files){
							var datalength = files.length;
							var a = files;
							console.log(a);
							alert(a);
							for(var i=0; i < datalength; i++) {
								var file = a[i].name;
								var extension = file.substr( (file.lastIndexOf('.') +1) );
								var size = parseInt(a[i].bytes)/1024;
								var fsize = size.toFixed(2);
								fsize = fsize + " KB";
								//file mapping
								var count = fname_'.$fieldval['modfieldid'].'.length;
								var fdelid = count-1;
								fname_'.$fieldval['modfieldid'].'.push(a[i].name);
								fsize_'.$fieldval['modfieldid'].'.push(fsize);
								ftype_'.$fieldval['modfieldid'].'.push(extension);
								fpath_'.$fieldval['modfieldid'].'.push(a[i].link);
								upfrom_'.$fieldval['modfieldid'].'.push("3");
								$("#'.$fieldval['fieldname'].'").val(fname_'.$fieldval['modfieldid'].');
								$("#'.$fieldval['fieldname'].'_size").val(fsize_'.$fieldval['modfieldid'].');
								$("#'.$fieldval['fieldname'].'_type").val(ftype_'.$fieldval['modfieldid'].');
								$("#'.$fieldval['fieldname'].'_path").val(fpath_'.$fieldval['modfieldid'].');
								$("#'.$fieldval['fieldname'].'_fromid").val(upfrom_'.$fieldval['modfieldid'].');';
								echo 'var spantag = $("<span style=';echo "'word-wrap:break-word;'"; echo '>"+a[i].name+" ("+fsize+")</span><i data-fileid='; echo '"+fdelid+"'; echo " class='material-icons documentsdownloadclsbtn ".$fieldval['fieldname']."docclsbtn'>close</i><br/>"; echo '");';
								echo 'spantag.appendTo("#'.$fieldval['fieldname'].'attachdisplay");
								//delete files
								$(".'.$fieldval['fieldname'].'docclsbtn").click(function() {
									var id = $(this).data("fileid");
									var count = fname_'.$fieldval['modfieldid'].'.length;
									for(var k=0;k<count;k++) {
										if( k == id) {
											fname_'.$fieldval['modfieldid'].'.splice($.inArray(fname_'.$fieldval['modfieldid'].'[id],fname_'.$fieldval['modfieldid'].'),1);
											fsize_'.$fieldval['modfieldid'].'.splice($.inArray(fsize_'.$fieldval['modfieldid'].'[id],fsize_'.$fieldval['modfieldid'].'),1);
											ftype_'.$fieldval['modfieldid'].'.splice($.inArray(ftype_'.$fieldval['modfieldid'].'[id],ftype_'.$fieldval['modfieldid'].'),1);
											fpath_'.$fieldval['modfieldid'].'.splice($.inArray(fpath_'.$fieldval['modfieldid'].'[id],fpath_'.$fieldval['modfieldid'].'),1);
											upfrom_'.$fieldval['modfieldid'].'.splice($.inArray(upfrom_'.$fieldval['modfieldid'].'[id],upfrom_'.$fieldval['modfieldid'].'),1);
											setTimeout(function(){
												$("#'.$fieldval['fieldname'].'").val(fname_'.$fieldval['modfieldid'].');
												$("#'.$fieldval['fieldname'].'_size").val(fsize_'.$fieldval['modfieldid'].');
												$("#'.$fieldval['fieldname'].'_type").val(ftype_'.$fieldval['modfieldid'].');
												$("#'.$fieldval['fieldname'].'_path").val(fpath_'.$fieldval['modfieldid'].');
												$("#'.$fieldval['fieldname'].'_fromid").val(upfrom_'.$fieldval['modfieldid'].');
											},100);
										}
									}
									$(this).prev("span").remove();
									$(this).next("br").remove();
									$(this).remove();
								});
								$("#processoverlay").hide();
								alertpopup("File uploaded successfully");
							}						
						},
						cancel: function() {
						},
						linkType: "direct",
						multiselect: true,
						//extensions: [],
						};	
															
					$("#imgbox_filename").click(function(){	
						var count = fname_'.$fieldval['modfieldid'].'.length;
						$("#taglivedisplay").empty();
						$("#captureimagedisplay").empty();
						//$("#resultbtnset").addClass("hidedisplay");
						//$(".snapshotcapturebtn").removeClass("hidedisplay");			
						addscreencapture();
					});
					$("#imagecaptureclose").click(function(){
						$("#imagecaptureoverlay").fadeOut();
					});
					
					function addscreencapture(){
						// Web cam capture Add screen function			
						var sayCheese = new SayCheese("#webcamattachdisplay", {video: true});
						sayCheese.start();
						$("#take-snapshot").click(function(){
							var width = 0, height = 0;
							sayCheese.takeSnapshot(width, height);
							$("#image-container1").addClass("hidedisplay");
							$("#image-container2").removeClass("hidedisplay");
							$("#savecapturedimg").show();					
						});
						sayCheese.on("snapshot", function(snapshot){			  
							var img = document.createElement("img");
							$(img).on("load", function(){
								$("#captureimagedisplay").empty();
								$("#captureimagedisplay").prepend(img);
							});
							img.src = snapshot.toDataURL("image/png");
							$("#imgurldata").val(img.src);
						});
						$("#savecapturedimg").click(function(){
							var dataurlimg =  $("#imgurldata").val();
							success(dataurlimg);
							$("#image-container1").removeClass("hidedisplay");
							$("#image-container2").addClass("hidedisplay");
							$("#savecapturedimg").hide();
						});
						$("#cancelcapturedimg").click(function(){
							var dataurlimg =  $("#imgurldata").val("");
							$("#image-container1").removeClass("hidedisplay");
							$("#image-container2").addClass("hidedisplay");
							$("#savecapturedimg").hide();
						});		
						$("#resetcapturedimg").click(function(){
							$("#imgurldata").val("");		
							$("#captureimagedisplay").empty();
							$("#savecapturedimg").show();				
						});
					}
					{// Webcam add Function
						function success(imageData)  {
							var url = base_url+"webcam-document.php";
							var params = {image: imageData};
							$.post(url, params, function(data) {		
								var arr =$.parseJSON(data);
								var count = fname_'.$fieldval['modfieldid'].'.length;
								var fdelid = count-1;
								//file mapping
								fname_'.$fieldval['modfieldid'].'.push(arr.fname);
								fsize_'.$fieldval['modfieldid'].'.push(arr.fsize);
								ftype_'.$fieldval['modfieldid'].'.push(arr.ftype);
								fpath_'.$fieldval['modfieldid'].'.push(arr.path);
								upfrom_'.$fieldval['modfieldid'].'.push("2");
								$("#'.$fieldval['fieldname'].'").val(fname_'.$fieldval['modfieldid'].');
								$("#'.$fieldval['fieldname'].'_size").val(fsize_'.$fieldval['modfieldid'].');
								$("#'.$fieldval['fieldname'].'_type").val(ftype_'.$fieldval['modfieldid'].');
								$("#'.$fieldval['fieldname'].'_path").val(fpath_'.$fieldval['modfieldid'].');
								$("#'.$fieldval['fieldname'].'_fromid").val(upfrom_'.$fieldval['modfieldid'].');';
									echo 'var spantag = $("<span style=';echo "'word-wrap:break-word;'"; echo '>"+arr.fname+" ("+arr.fsize+" )</span><i data-fileid='; echo '"+fdelid+"'; echo " class='material-icons documentsdownloadclsbtn ".$fieldval['fieldname']."docclsbtn'>close</i><br/>"; echo '");';
									echo 'spantag.appendTo("#'.$fieldval['fieldname'].'attachdisplay");
									//delete files
									$(".'.$fieldval['fieldname'].'docclsbtn").click(function() {
										var id = $(this).data("fileid");
										var count = fname_'.$fieldval['modfieldid'].'.length;
										for(var k=0;k<count;k++) {
											if( k == id) {
												fname_'.$fieldval['modfieldid'].'.splice($.inArray(fname_'.$fieldval['modfieldid'].'[id],fname_'.$fieldval['modfieldid'].'),1);
												fsize_'.$fieldval['modfieldid'].'.splice($.inArray(fsize_'.$fieldval['modfieldid'].'[id],fsize_'.$fieldval['modfieldid'].'),1);
												ftype_'.$fieldval['modfieldid'].'.splice($.inArray(ftype_'.$fieldval['modfieldid'].'[id],ftype_'.$fieldval['modfieldid'].'),1);
												fpath_'.$fieldval['modfieldid'].'.splice($.inArray(fpath_'.$fieldval['modfieldid'].'[id],fpath_'.$fieldval['modfieldid'].'),1);
												upfrom_'.$fieldval['modfieldid'].'.splice($.inArray(upfrom_'.$fieldval['modfieldid'].'[id],upfrom_'.$fieldval['modfieldid'].'),1);
												setTimeout(function(){
													$("#'.$fieldval['fieldname'].'").val(fname_'.$fieldval['modfieldid'].');
													$("#'.$fieldval['fieldname'].'_size").val(fsize_'.$fieldval['modfieldid'].');
													$("#'.$fieldval['fieldname'].'_type").val(ftype_'.$fieldval['modfieldid'].');
													$("#'.$fieldval['fieldname'].'_path").val(fpath_'.$fieldval['modfieldid'].');
													$("#'.$fieldval['fieldname'].'_fromid").val(upfrom_'.$fieldval['modfieldid'].');
												},100);
											}
										}
										$(this).prev("span").remove();
										$(this).next("br").remove();
										$(this).remove();
									});
									$(".ajax-file-upload-container").remove();
									$("#'.$fieldval['fieldname'].'uploadoverlay").hide();
								$("#processoverlay").hide();
								$("#webcamattachdisplay").empty();
								$("#captureimagedisplay").empty();
								$("#'.$fieldval['fieldname'].'uploadoverlay").hide();
								//ends
							});
							}
						}
						// for google drive
						$("#gd_'.$fieldval['fieldname'].'").click(function(){
							onApiLoad();
						});
						{//google Drive Code
							var developerKey = "AIzaSyBuu86le5tdjxl4CX9325SjExNk5W8ks1E";
							var clientId = "156167503741-929le84nvsgc1gs1b14p327oo795fmed.apps.googleusercontent.com";
							var scope = ["https://www.googleapis.com/auth/drive"];
							function handleClientLoad() {
								gapi.client.setApiKey(developerKey);
							}
							var pickerApiLoaded = false;
							var oauthToken;
						    function onApiLoad() {
						        gapi.load("auth", {"callback": onAuthApiLoad});
						        gapi.load("picker", {"callback": onPickerApiLoad});
							}
							function onApiLoadDelete() {
								gapi.load("auth", {"callback": onAuthApiLoad});
							}
						    function onAuthApiLoad() {
						        window.gapi.auth.authorize(
						        {
						        	"client_id": clientId,
						       		"scope": scope,
						        	"immediate": false
						        },
						        handleAuthResult);
							}
							function onAuthApiLoadDelete() {
						        window.gapi.auth.authorize(
						        {
							        "client_id": clientId,
							        "scope": scope,
							        "immediate": false
						        },
						        handleAuthResultDelete);
							}
						    function onPickerApiLoad() {
						        pickerApiLoaded = true;
						        //createPicker();
							}
						    function handleAuthResult(authResult) {
							    if (authResult && !authResult.error) {
								    oauthToken = authResult.access_token;
								    createPicker();
								}
							}
						    function handleAuthResultDelete(authResult) {
						        if (authResult && !authResult.error) {
						      		oauthToken = authResult.access_token;
								}
							}
							function deleteFile(fileidval) {
								gapi.client.load("drive", "v2", function() {
									var request = gapi.client.drive.files.delete({
										"fileId": fileidval //File Id To delete
									});
									request.execute(function(resp) {
										//alert("Deleted Sucessfully");
									});
								});
							}
						      function createPicker() {
						        if (pickerApiLoaded && oauthToken) {
						          var picker = new google.picker.PickerBuilder().
								  		addView(new google.picker.DocsUploadView().setIncludeFolders(true)).
									   addViewGroup(
									    new google.picker.ViewGroup(google.picker.ViewId.DOCS).
										addView(google.picker.ViewId.DOCS_IMAGES_AND_VIDEOS).
										addView(google.picker.ViewId.DOCUMENTS).
										addView(google.picker.ViewId.PRESENTATIONS).
										addView(google.picker.ViewId.SPREADSHEETS).
										addView(google.picker.ViewId.FOLDERS).
										addView(google.picker.ViewId.PDFS).
										addView(google.picker.ViewId.FORMS)).
										
										 addView(new google.picker.MapsView().setCenter(9.9197,78.1194).setZoom(3)).
										 enableFeature(google.picker.Feature.MULTISELECT_ENABLED).
										 addView(new google.picker.ImageSearchView().
											setLicense(google.picker.ImageSearchView.License.REUSE)).
										 addView(google.picker.ViewId.VIDEO_SEARCH).
										 addView(google.picker.ViewId.WEBCAM).
									  		  
									  setOAuthToken(oauthToken).
									  setDeveloperKey(developerKey).
						              setCallback(pickerCallback).
									  build();
						          picker.setVisible(true);
						        }
						      } 
						      function pickerCallback(data) { 
								  if (data[google.picker.Response.ACTION] == google.picker.Action.PICKED) {
									  var doc = data[google.picker.Response.DOCUMENTS];
									  for(var i=0; i < doc.length; i++) {
									  	var count = fname_'.$fieldval['modfieldid'].'.length;
										var fdelid = count-1;
									  		var bytes = doc[i].sizeBytes+" KB";
									  		fname_'.$fieldval['modfieldid'].'.push(doc[i].name);
											fsize_'.$fieldval['modfieldid'].'.push(bytes);
											ftype_'.$fieldval['modfieldid'].'.push(doc[i].mimeType);
											fpath_'.$fieldval['modfieldid'].'.push(doc[i].url);
											upfrom_'.$fieldval['modfieldid'].'.push("4");
											$("#'.$fieldval['fieldname'].'").val(fname_'.$fieldval['modfieldid'].');
											$("#'.$fieldval['fieldname'].'_size").val(fsize_'.$fieldval['modfieldid'].');
											$("#'.$fieldval['fieldname'].'_type").val(ftype_'.$fieldval['modfieldid'].');
											$("#'.$fieldval['fieldname'].'_path").val(fpath_'.$fieldval['modfieldid'].');
											$("#'.$fieldval['fieldname'].'_fromid").val(upfrom_'.$fieldval['modfieldid'].');';
											echo 'var spantag = $("<span style=';echo "'word-wrap:break-word;'"; echo '>"+doc[i].name+" ("+bytes+")</span><i data-fileid='; echo '"+fdelid+"'; echo " class='material-icons documentsdownloadclsbtn ".$fieldval['fieldname']."docclsbtn'>close</i><br/>"; echo '");';
											echo 'spantag.appendTo("#'.$fieldval['fieldname'].'attachdisplay");
											$(".'.$fieldval['fieldname'].'docclsbtn").click(function() {
												var id = $(this).data("fileid");
												var count = fname_'.$fieldval['modfieldid'].'.length;
												for(var k=0;k<count;k++) {
													if( k == id) {
														fname_'.$fieldval['modfieldid'].'.splice($.inArray(fname_'.$fieldval['modfieldid'].'[id],fname_'.$fieldval['modfieldid'].'),1);
														fsize_'.$fieldval['modfieldid'].'.splice($.inArray(fsize_'.$fieldval['modfieldid'].'[id],fsize_'.$fieldval['modfieldid'].'),1);
														ftype_'.$fieldval['modfieldid'].'.splice($.inArray(ftype_'.$fieldval['modfieldid'].'[id],ftype_'.$fieldval['modfieldid'].'),1);
														fpath_'.$fieldval['modfieldid'].'.splice($.inArray(fpath_'.$fieldval['modfieldid'].'[id],fpath_'.$fieldval['modfieldid'].'),1);
														upfrom_'.$fieldval['modfieldid'].'.splice($.inArray(upfrom_'.$fieldval['modfieldid'].'[id],upfrom_'.$fieldval['modfieldid'].'),1);
														setTimeout(function(){
															$("#'.$fieldval['fieldname'].'").val(fname_'.$fieldval['modfieldid'].');
															$("#'.$fieldval['fieldname'].'_size").val(fsize_'.$fieldval['modfieldid'].');
															$("#'.$fieldval['fieldname'].'_type").val(ftype_'.$fieldval['modfieldid'].');
															$("#'.$fieldval['fieldname'].'_path").val(fpath_'.$fieldval['modfieldid'].');
															$("#'.$fieldval['fieldname'].'_fromid").val(upfrom_'.$fieldval['modfieldid'].');
														},100);
													}
												}
												$(this).prev("span").remove();
												$(this).next("br").remove();
												$(this).remove();
											});
											$("#processoverlay").hide();
											alertpopup("File uploaded successfully");		
								 	 }
								  }
							  }
						}
					});
					</script>';
				break;
				
			case 16:
				//Image
				$mulfile = $fieldval['multiple']=='Yes' ? 'true' : 'false';
				echo '<div class="'.$colstyle.''.$fildmode.'" id="'.$fieldval['fieldname'].'divhid">';
				echo '<label>'.$fieldval['filedlabel'].$mandlab.'</label>';
				echo '<span class="driveiconsize filechooserdrive" id="desk_'.$fieldval['fieldname'].'" data-type="desktop" data-buttonid="'.$fieldval['fieldname'].'" title="From Computer"><i class="material-icons">computer</i></span>
				<!--<span class="icon24 icon24-dropbox driveiconsize filechooserdrive" id="db_'.$fieldval['fieldname'].'" data-type="dropbox" data-buttonid="dbchoose_'.$fieldval['fieldname'].'" title="Dropbox"></span>-->';
				//drop box
				//echo '<span style="display:none;" id="spandbchoose_'.$fieldval['fieldname'].'"><input type="dropbox-chooser" name="selected-file" id="dbchoose_'.$fieldval['fieldname'].'" data-multiselect="'.$mulfile.'" data-link-type="direct" data-extensions="images" /></span>';
				echo '</div>';
				echo hidden($fieldval['fieldname'],'');
				echo '<div class="large-12 medium-12 small-12 columns'.$fildmode.'">&nbsp</div>';
				echo '<div class="large-12 medium-12 small-12 columns'.$fildmode.'">';
				echo '<div class="large-12 medium-12 small-12 columns uploadcontainerstyle'.$fildmode.' card-panel" style="height:10rem; border:1px solid #CCCCCC;overflow-y:auto;overflow-x:hidden;" id="'.$fieldval['fieldname'].'attachdisplay">';
				echo '</div>';
				echo '</div>';
				/* File Upload overlay [from computer]*/
				echo '<div class="large-12 columns" style="position:absolute;">
						<div class="overlay alertsoverlay overlayalerts" id="'.$fieldval['fieldname'].'uploadoverlay" style="overflow-y: scroll;overflow-x: hidden">
						<div class="row">&nbsp;</div>
						<div class="row">&nbsp;</div>
						<div class="row">&nbsp;</div>
						<div class="row">&nbsp;</div>
						<div class="row">&nbsp;</div>
						<div class="overlaybgborder">
							<div class="row">&nbsp;</div>
							<div class="large-4 medium-6 large-centered medium-centered columns newcleardataform  paddingzero style="left: 15px;position:relative;">
								<div class="large-12 columns sectionheaderformcaptionstyle">File Upload</div>
								<div class="row">&nbsp;</div>
								<div class="row">&nbsp;</div>
								<div class="large-12 medium-12 small-12 columns">
									<span id="'.$fieldval['fieldname'].'mulitplefileuploader">upload</span>
								</div>
								<div class="row">&nbsp;</div>
								<div class="row">&nbsp;</div>
								<div class="small-12 small-centered medium-12 large-12 columns text-right">
									<input type="button" id="'.$fieldval['fieldname'].'overlayclose" name="'.$fieldval['fieldname'].'overlayclose" value="Cancel" class="alertbtnno" tabindex="1001">
								</div>
								<div class="row">&nbsp;</div>
							</div>
							<div class="row">&nbsp;</div>
						</div>
					</div>
				</div>';
				/* file upload script */
				$types = implode(',',explode('|',$fieldval['customrule']));
				$upfiletypes = str_replace( '.','',$types );
				$upfiletypes = $upfiletypes!=''? $upfiletypes : '*';
				echo '<script>
					$(document).ready(function(){
						//$(".ajax-file-upload>form>input").removeAttr("disabled");
						/* close file upload overlay */
						$("#'.$fieldval['fieldname'].'overlayclose").click(function(){
							$(".ajax-file-upload-error").remove();
							$("#'.$fieldval['fieldname'].'uploadoverlay").hide();
						});
						/* file upload btn click */
						$("#'.$fieldval['fieldname'].'btn").click(function(){
							$(".triggerupload'.$fieldval['fieldname'].'fileupload:last").trigger("click");
						});
						/* File upload from computer */
						fileuploadmoname = "'.$fieldval['fieldname'].'fileupload";
						var '.$fieldval['fieldname'].'settings = {
							url: base_url+"upload.php",
							method: "POST",
							fileName: "myfile",
							multiple: '.$mulfile.',
							allowedTypes: "'.$upfiletypes.'",
							dataType:"json",
							async:false,
							//maxFileCount: 10,
							onSuccess:function(files,data,xhr) {
								if(data != "Size" && data != "MaxSize" && data != "MaxFile" && data != "LowSize") {
									$("#processoverlay").show();
									setTimeout(function(){
										var arr =$.parseJSON(data);
										/* file mapping */
										$("#'.$fieldval['fieldname'].'").val(arr.fname);
										$("#'.$fieldval['fieldname'].'_size").val(arr.fsize);
										$("#'.$fieldval['fieldname'].'_type").val(arr.ftype);
										$("#'.$fieldval['fieldname'].'_path").val(arr.path);
										$("#'.$fieldval['fieldname'].'_fromid").val(2);
										/* display image */
										$("#'.$fieldval['fieldname'].'attachdisplay").empty();';
										echo 'var img = $("<img style=';echo "'height:100%;width:100%;'"; echo '>'; echo '");';
										echo 'img.attr("src",base_url+arr.path);';
										echo 'img.appendTo("#'.$fieldval['fieldname'].'attachdisplay");';
										echo '$("#'.$fieldval['fieldname'].'attachdisplay").append("'; echo "<i class='material-icons documentslogodownloadclsbtn ".$fieldval['fieldname']."docclsbtn' data-txtid='".$fieldval['fieldname']."'>close</i>"; echo '");';
										echo '/* delete files */
										$(".'.$fieldval['fieldname'].'docclsbtn").click(function() {
											var id = $(this).data("txtid");
											$(this).remove();
											$("#"+id+"attachdisplay").empty();
											$("#"+id).val("");
											$("#"+id+"_size").val("");
											$("#"+id+"_type").val("");
											$("#"+id+"_path").val("");
											$("#"+id+"_fromid").val("");
										});		
										$("#processoverlay").hide();
										$(".ajax-file-upload-container").empty();
										$("#'.$fieldval['fieldname'].'uploadoverlay").hide();
										alertpopupdouble("File uploaded successfully");
									},100);
								} else if(data == "MaxSize") {
									alertpopupdouble("You have reached your maximun storage limit. if you want to upload documents you want to buy the storage limit from add ons");
									$("#processoverlay").hide();
									$(".ajax-file-upload-container").empty();
									$("#'.$fieldval['fieldname'].'uploadoverlay").hide();
								} else if(data == "MaxFile") {
									alertpopupdouble("Upto 10 files can only be uploaded at a time. "+count+" files Uploaded Successfully");
									$("#processoverlay").hide();
									$(".ajax-file-upload-container").empty();
									$("#'.$fieldval['fieldname'].'uploadoverlay").hide();
								} else if(data == "Size") {
									alertpopupdouble("Maximum File size can be 5mb only");
									$("#processoverlay").hide();
									$(".ajax-file-upload-container").empty();
									$("#'.$fieldval['fieldname'].'uploadoverlay").hide();
								} else if(data == "LowSize") {
									alertpopupdouble("File Size is too low");
									$("#processoverlay").hide();
									$(".ajax-file-upload-container").empty();
									$("#'.$fieldval['fieldname'].'uploadoverlay").hide();
								} else {
									alertpopupdouble("Error Uploading File. Please Try Again");
									$("#processoverlay").hide();
									$(".ajax-file-upload-container").empty();
									$("#'.$fieldval['fieldname'].'uploadoverlay").hide();
								}
							},
							onError: function(files,status,errMsg) {
								$("#processoverlay").hide();
								$("#'.$fieldval['fieldname'].'uploadoverlay").hide();
							}
						}
						//file upload settings
						$("#'.$fieldval['fieldname'].'mulitplefileuploader").uploadFile('.$fieldval['fieldname'].'settings);
						{//google Drive Code
						// for google drive
						$("#gd_'.$fieldval['fieldname'].'").click(function(){
							onApiLoad();
						});					
						var developerKey = "AIzaSyC87Zb2jNYFzh3rHLZVAdCNdl6ndiYJs3A";
						var clientId = "143955342181-mpbeckiu5n1d4sd06pg4uoss5h5fuo1q.apps.googleusercontent.com";
						var scope = ["https://www.googleapis.com/auth/drive"];
						function handleClientLoad() {
							gapi.client.setApiKey(developerKey);
						}
						var pickerApiLoaded = false;
						var oauthToken;
						function onApiLoad() {
							gapi.load("auth", {"callback": onAuthApiLoad});
							gapi.load("picker", {"callback": onPickerApiLoad});
						}
						function onApiLoadDelete() {
							gapi.load("auth", {"callback": onAuthApiLoad});
						}
						function onAuthApiLoad() {
							window.gapi.auth.authorize(
							{
								"client_id": clientId,
								"scope": scope,
								"immediate": false
							},
							handleAuthResult);
						}
						function onAuthApiLoadDelete() {
							window.gapi.auth.authorize(
							{
								"client_id": clientId,
								"scope": scope,
								"immediate": false
							},
							handleAuthResultDelete);
						}
						function onPickerApiLoad() {
							pickerApiLoaded = true;
							//createPicker();
						}
						function handleAuthResult(authResult) {
							if (authResult && !authResult.error) {
								oauthToken = authResult.access_token;
								createPicker();
							}
						}
						function handleAuthResultDelete(authResult) {
							if (authResult && !authResult.error) {
								oauthToken = authResult.access_token;
							}
						}
						function deleteFile(fileidval) {
							gapi.client.load("drive", "v2", function() {
								var request = gapi.client.drive.files.delete({
									"fileId": fileidval //File Id To delete
								});
								request.execute(function(resp) {
									//alert("Deleted Sucessfully");
								});
							});
						}
						  function createPicker() {
							if (pickerApiLoaded && oauthToken) {
							  var picker = new google.picker.PickerBuilder().
									addView(new google.picker.DocsUploadView().setIncludeFolders(true)).
								  	addView(google.picker.ViewId.DOCS_IMAGES).
									addView(new google.picker.ImageSearchView().
									setLicense(google.picker.ImageSearchView.License.REUSE)).
								  setOAuthToken(oauthToken).
								  setDeveloperKey(developerKey).
								  setCallback(pickerCallback).
								  build();
							  picker.setVisible(true);
							}
						  } 
						function pickerCallback(data) { 
							if (data[google.picker.Response.ACTION] == google.picker.Action.PICKED) {
								var doc = data[google.picker.Response.DOCUMENTS];
								console.log(doc);
								for(var i=0; i < doc.length; i++) {
									var file = doc[i].name;
									var extension = file.substr( (file.lastIndexOf('.') +1) );
									var size = parseInt(doc[i].sizeBytes) / 1024;
									var fsize = size.toFixed(2);
									fsize = fsize + " KB";
									/* file mapping */
									$("#'.$fieldval['fieldname'].'").val(doc[i].name);
									$("#'.$fieldval['fieldname'].'_size").val(fsize);
									$("#'.$fieldval['fieldname'].'_type").val(doc[i].mimeType);
									$("#'.$fieldval['fieldname'].'_path").val(doc[i].url);
									$("#'.$fieldval['fieldname'].'_fromid").val(4);
									/* display image */
									$("#'.$fieldval['fieldname'].'attachdisplay").empty();';
									echo 'var img = $("<img style=';echo "'height:100%;width:100%;'"; echo '>'; echo '");';
									echo 'img.attr("src", doc[i].url);';
									echo 'img.appendTo("#'.$fieldval['fieldname'].'attachdisplay");';
										echo '$("#'.$fieldval['fieldname'].'attachdisplay").append("'; echo "<i class='material-icons documentslogodownloadclsbtn ".$fieldval['fieldname']."docclsbtn'>close</i>"; echo '");';
									echo '//delete files
									$(".'.$fieldval['fieldname'].'docclsbtn").click(function() {
										$(this).remove();
										$("#'.$fieldval['fieldname'].'attachdisplay").empty();
										$("#'.$fieldval['fieldname'].'").val("");
										$("#'.$fieldval['fieldname'].'_size").val("");
										$("#'.$fieldval['fieldname'].'_type").val("");
										$("#'.$fieldval['fieldname'].'_path").val("");
										$("#'.$fieldval['fieldname'].'_fromid").val("");
									});
									$("#processoverlay").hide();
									alertpopup("File uploaded successfully");		
								}
							}
						}
					}
					//for box file picker
					$("#box_'.$fieldval['fieldname'].'").click(function() {
						boxSelect.launchPopup();
					});
					var boxoptions = {
						clientId: "w3ujqsxjp70fkmf3yg8in02tqzkhudke",
						linkType: "direct",
						multiselect: "false"
					};
					var boxSelect = new BoxSelect(boxoptions);
					// Register a success callback handler
					boxSelect.success(function(response) {
						for(var i=0; i < response.length; i++) {
							var file = response[i].name;
							var extension = file.substr( (file.lastIndexOf('.') +1) );
							var size = parseInt(response[i].size) / 1024;
							var fsize = size.toFixed(2);
							fsize = fsize + " KB";
							/* file mapping */
							$("#'.$fieldval['fieldname'].'").val(response[i].name);
							$("#'.$fieldval['fieldname'].'_size").val(fsize);
							$("#'.$fieldval['fieldname'].'_type").val(extension);
							$("#'.$fieldval['fieldname'].'_path").val(response[i].url);
							$("#'.$fieldval['fieldname'].'_fromid").val(5);
							/* display image */
							$("#'.$fieldval['fieldname'].'attachdisplay").empty();';
							echo 'var img = $("<img style=';echo "'height:100%;width:100%;'"; echo '>'; echo '");';
							echo 'img.attr("src", response[i].url);';
							echo 'img.appendTo("#'.$fieldval['fieldname'].'attachdisplay");';
							echo '$("#'.$fieldval['fieldname'].'attachdisplay").append("'; echo "<i class='material-icons documentslogodownloadclsbtn ".$fieldval['fieldname']."docclsbtn'>close</i>"; echo '");';
							echo '//delete files
							$(".'.$fieldval['fieldname'].'docclsbtn").click(function() {
								$(this).remove();
								$("#'.$fieldval['fieldname'].'attachdisplay").empty();
								$("#'.$fieldval['fieldname'].'").val("");
								$("#'.$fieldval['fieldname'].'_size").val("");
								$("#'.$fieldval['fieldname'].'_type").val("");
								$("#'.$fieldval['fieldname'].'_path").val("");
								$("#'.$fieldval['fieldname'].'_fromid").val("");
							});
							$("#processoverlay").hide();
							alertpopup("File uploaded successfully");	
						}
					});
					// Register a cancel callback handler
					boxSelect.cancel(function() {
						console.log("The user clicked cancel or closed the popup");
					});
					//boxSelect.launchPopup();
					boxSelect.closePopup();
					//var isSupported = boxSelect.isBrowserSupported();
					/* Drop box file link */
						$("#db_'.$fieldval['fieldname'].'").click(function(){
							Dropbox.choose('.$fieldval['fieldname'].'options);
						});
						'.$fieldval['fieldname'].'options = {success: function(files){
								var datalength = files.length;
								var a = files;
								console.log(files);
								for(var i=0; i < datalength; i++) {
								var file = a[i].name;
								var extension = file.substr( (file.lastIndexOf('.') +1) );
								var size = parseInt(a[i].bytes) / 1024;
								var fsize = size.toFixed(2);
								fsize = fsize + " KB";
								/* file mapping */
								$("#'.$fieldval['fieldname'].'").val(a[i].name);
								$("#'.$fieldval['fieldname'].'_size").val(fsize);
								$("#'.$fieldval['fieldname'].'_type").val(extension);
								$("#'.$fieldval['fieldname'].'_path").val(a[i].thumbnailLink);
								$("#'.$fieldval['fieldname'].'_fromid").val(3);
								/* display image */
								$("#'.$fieldval['fieldname'].'attachdisplay").empty();';
								echo 'var img = $("<img style=';echo "'height:100%;width:100%;'"; echo '>'; echo '");';
								echo 'img.attr("src", a[i].thumbnailLink);';
								echo 'img.appendTo("#'.$fieldval['fieldname'].'attachdisplay");';
								
								echo '$("#'.$fieldval['fieldname'].'attachdisplay").append("'; echo "<i class='material-icons documentslogodownloadclsbtn ".$fieldval['fieldname']."docclsbtn'>close</i>"; echo '");';
								echo '//delete files
								$(".'.$fieldval['fieldname'].'docclsbtn").click(function() {
									$(this).remove();
									$("#'.$fieldval['fieldname'].'attachdisplay").empty();
									$("#'.$fieldval['fieldname'].'").val("");
									$("#'.$fieldval['fieldname'].'_size").val("");
									$("#'.$fieldval['fieldname'].'_type").val("");
									$("#'.$fieldval['fieldname'].'_path").val("");
									$("#'.$fieldval['fieldname'].'_fromid").val("");
								});
								$("#processoverlay").hide();
								alertpopup("File uploaded successfully");
							}
						},
						cancel: function() {
						},
						//linkType: "preview",
						multiselect: false,
						extensions: [".jpg",".jpeg",".exif",".png",".tiff",".gif",".ppm",".pgm",".pbm",".pnm",".bpg"],
					};
				});	
				</script>';
				break;

			case 17:
				//drop down
				$mandlab = "";
				$mandatoryopt = ""; //mandatory option set
				$txtval="";
				$tablename = substr($fieldval['colname'],0,-2);
				$tabid = $tablename.'id';
				$tabfieldname = $tablename.'name';
				$funcalldata = $CI->Basefunctions->picklistgroupddfetch($fieldval['moduleid'],$tablename,$tabfieldname,$tabid);
				if($fieldval['mandmode'] == "Yes") {
					if (!empty($funcalldata)) {
						$mandatoryopt = 'validate[required'.$rule.']';
					} else{
						$mandatoryopt = 'validate[required'.$rule.']';
					}
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				} else{
					if (!empty($funcalldata)) {
						$mandatoryopt = '';
					} else{
						$mandatoryopt = '';
					}
				}
				$divattr = array('class'=>'static-field '.$colstyle.'' .$fildmode.'','id'=>''.$fieldval['fieldname'].'divhid');
				$labval = $fieldval['filedlabel'].$mandlab;
				$labattr = array();
				$name = $fieldval['fieldname'];
				$value = array();
				$defval = array();
				$ddowndata = $CI->Basefunctions->dynamicdropdownvalfetch($fieldval['moduleid'],$tablename,$tabfieldname,$tabid);
				foreach($ddowndata as $key):
					$value[$key->Id] = $key->Name;
					if( $key->defaultopt == 'Yes' || $key->defaultopt == '1' ) {
						$defval[] = $key->Id;
						$txtval = $key->Id;
					}
				endforeach;
				$txtval = ( ($txtval!='')? $txtval : 1 );
				$valoption = "id";
				$dataattr = array(''.$fieldval['fieldname'].'hidden'=>'name');
				$multiple = $fieldval['multiple'];
				if (!empty($funcalldata)) {
					$funcalldata = implode(',',$funcalldata);
					$seldataattr = array("placeholder"=>"",'defvalattr'=>''.$txtval.'','dependency'=>''.$funcalldata.'');
					$funcall = 'picklistgroup';
				} else {
					$seldataattr = array("placeholder"=>"",'defvalattr'=>''.$txtval.'');
					$funcall = '';
				}
				$option = array("class"=>"chzn-select ".$funcall." ".$mandatoryopt."".$readmode."","tabindex"=>"".$tabindex."","data"=>$seldataattr,'data-prompt-position'=>'bottomLeft:14,36');
				$hidname = '';
				$hidval = '';
				echo dropdownspandefault($divattr,$labval,$labattr,$name,$value,$valoption,$dataattr,$multiple,$option,$defval,$hidname,$hidval);
				break;

			case 18:
				//group drop down
				$mandlab = "";
				$txtval="";
				$mandatoryopt = ""; //mandatory option set
				if($fieldval['mandmode'] == "Yes") {
					$mandatoryopt = 'validate[required'.$rule.']';
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				}
				$divattr = array('class'=>'static-field '.$colstyle.''.$fildmode.'','id'=>''.$fieldval['fieldname'].'divhid');
				$labval = $fieldval['filedlabel'].$mandlab;
				$labattr = array();
				$name = $fieldval['fieldname'];
				$value = array();
				$defval = array();
				$tablename = substr($fieldval['colname'],0,-2);
				$tabfieldname = $tablename.'name';
				$value = array();
				$dataval = array();
				if($fieldval['tabname'] != "") {
					$ddowndata = $CI->Basefunctions->dynamicgroupdropdownvalfetch($fieldval['moduleid'],$tablename,$fieldval['ddparenttable'],$tabfieldname);
					$prev = ' ';
					$grpname = '';
					foreach($ddowndata as $key):
						$cur = $key->PId;
						$a ="";
						if($prev != $cur) {
							if( $prev != '' ) {
								$value[$grpname] = $dataval;
								$dataval = array();
							}
							$dataval[$key->CId] = $key->CName;
							if( $key->defaultopt == 'Yes' || $key->defaultopt == '1' ) {
								$defval[] = $key->CId;
							}
							$prev = $key->PId;
							$grpname = $key->PName;
						} else {
							$dataval[$key->CId] = $key->CName;
							if( $key->defaultopt == 'Yes' || $key->defaultopt == '1' ) {
								$defval[] = $key->CId;
								$txtval = $key->CId;
							}
						}
					endforeach;
					$value[$grpname] = $dataval;
				}
				$txtval = ( ($txtval!='')? $txtval : 1 );
				$valoption = "id";
				$dataattr = array(''.$fieldval['fieldname'].'hidden'=>'name');
				$multiple = $fieldval['multiple'];
				$seldataattr = array("placeholder"=>"",'defvalattr'=>''.$txtval.'');
				$option = array("class"=>"chzn-select ".$mandatoryopt."".$readmode."","tabindex"=>"".$tabindex."","data"=>$seldataattr,'data-prompt-position'=>'bottomLeft:14,36');
				$hidname = '';
				$hidval = '';
				echo groupdropdownspandefault($divattr,$labval,$labattr,$name,$value,$valoption,$dataattr,$multiple,$option,$defval,$hidname,$hidval);
				break;

			case 19:
				//drop down for parent table
				$mandlab = "";
				$txtval = "";
				$mandatoryopt = ""; //mandatory option set
				if($fieldval['mandmode'] == "Yes") {
					$mandatoryopt = 'validate[required'.$rule.']';
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				}
				$divattr = array('class'=>'static-field '.$colstyle.''.$fildmode.'','id'=>''.$fieldval['fieldname'].'divhid');
				$labval = $fieldval['filedlabel'].$mandlab;
				$labattr = array();
				$name = $fieldval['fieldname'];
				$value = array();
				$tablename = substr($fieldval['colname'],0,-2);
				$tabid = $tablename.'id';
				$tabfieldname = $tablename.'name';
				if($fieldval['tabname'] != "") {
					$ddowndata = $CI->Basefunctions->dynamicdropdownvalfetchparent($tablename,$tabfieldname,$tabid,$fieldval['defvalue']);
					if( ($tablename=='lead' && $tabfieldname=='leadname') || ($tablename=='lead' && $tabfieldname=='lastname') || ($tablename=='contact' && $tabfieldname=='contactname') || ($tablename=='contact' && $tabfieldname=='lastname') ) {
						foreach($ddowndata as $key):
							$sname = (($key->SName!='')? $key->SName.' ':'');
							$lname = (($key->LName!='')? ' '.$key->LName:'');
							$value[$key->Id] = $sname.$key->Name.$lname;
						endforeach;
					} else if( $tablename=='currency' ) {
						foreach($ddowndata as $key):
							$cname = (($key->Name!='')? $key->Name.' - ':'');
							$value[$key->Id] = $cname.$key->CName;
						endforeach;
					} else {
						foreach($ddowndata as $key):
							$value[$key->Id] = $key->Name;
						endforeach;
					}
				}
				$txtval = ( ($txtval!='')? $txtval : 1 );
				$valoption = "id";
				$dataattr = array(''.$fieldval['fieldname'].'hidden'=>'name');
				$multiple = $fieldval['multiple'];
				$seldataattr = array("placeholder"=>"",'defvalattr'=>''.$txtval.'');
				$option = array("class"=>"chzn-select ".$mandatoryopt."".$readmode."","tabindex"=>"".$tabindex."","data"=>$seldataattr,'data-prompt-position'=>'bottomLeft:14,36');
				$hidname = '';
				$hidval = '';
				echo dropdownspan($divattr,$labval,$labattr,$name,$value,$valoption,$dataattr,$multiple,$option,$hidname,$hidval);
				break;

			case 20:
				//group drop down with multiple data attribute
				$mandlab = "";
				$txtval="";
				$mandatoryopt = ""; //mandatory option set
				if($fieldval['mandmode'] == "Yes") {
					$mandatoryopt = 'validate[required'.$rule.']';
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				}
				$divattr = array('class'=>'static-field '.$colstyle.''.$fildmode.'','id'=>''.$fieldval['fieldname'].'divhid');
				$labval = $fieldval['filedlabel'].$mandlab;
				$labattr = array();
				$name = $fieldval['fieldname'].'ddid';
				$value = array();
				$defval = array();
				$tablename1 = substr($fieldval['colname'],0,-2);
				$fieldsinfo = explode(',',$fieldval['ddparenttable']);
				$tablename2 = $fieldsinfo[0];
				$value = array();
				$dataval = array();
				$groupid = array();
				$ddowndata = $CI->Basefunctions->userspecificgroupdropdownvalfetch();
				$prev = ' ';
				$grpname = '';
				for ($i=0;$i<count($ddowndata);$i++) {
					$cur = $ddowndata[$i]['PId'];
					$a ="";
					if($prev != $cur) {
						if( $prev != '' ) {
							$value[$grpname] = $dataval;
						}
						$dataval = array();
						$dataval[$ddowndata[$i]['CId']] = $ddowndata[$i]['CName'];
						$groupid[] = $ddowndata[$i]['PId'];
						$prev = $ddowndata[$i]['PId'];
						$grpname = $ddowndata[$i]['PName'];
					} else {
						$dataval[$ddowndata[$i]['CId']] = $ddowndata[$i]['CName'];
						$groupid[] = $ddowndata[$i]['PId'];
					}
				}
				$value[$grpname] = $dataval;
				$txtval = ( ($txtval!='')? $txtval : 1 );
				$valoption = "id";
				$dataattr = array();
				$multiple = $fieldval['multiple'];
				$seldataattr = array("placeholder"=>"",'defvalattr'=>''.$txtval.'');
				$option = array("class"=>"chzn-select dropdownchange ".$mandatoryopt."".$readmode."","tabindex"=>"".$tabindex."","data"=>$seldataattr,'data-prompt-position'=>'bottomLeft:14,36');
				$hidname = '';
				$hidval = '';
				echo groupdropdownspanuserspecific($divattr,$labval,$labattr,$name,$value,$valoption,$dataattr,$multiple,$option,$groupid,$hidname);
				echo '<input name="'.$fieldval['fieldname'].'" type="hidden" id="'.$fieldval['fieldname'].'" value="1" data-defvalattr="1" class="empdd"/>';
				echo '<input name="'.$fieldsinfo[1].'" type="hidden" id="'.$fieldsinfo[1].'" value="1" data-defvalattr="1" />';
				break;

			case 21:
				//tree generate
				$mandlab = "";
				$mandatoryopt = ""; //mandatory option set
				$fieldtablename1 = substr($fieldval['colname'],0,-2);
				if($fieldval['mandmode'] == "Yes") {
					$mandatoryopt = 'validate[required'.$rule.']';
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				}
				echo divopen( array("class"=>"large-12 medium-12 small-12 columns ".$fieldval['tabname']."treediv" ,'id'=>''.$fieldval['fieldname'].'divhid') );
				echo '<label>'.$fieldval['filedlabel'].$mandlab.'</label>';
				echo divopen( array("id"=>"dl-menu","class"=>"dl-menuwrapper","style"=>"z-index:2;margin-bottom: 1rem") );
				$type="button";
				echo '<button name="treebutton" type="'.$type.'" id="treebutton" class="btn dl-trigger treemenubtnclick" tabindex="'.$tabindex.'" style="height:2.2rem;color:#ffffff;font-size: 1.45rem;padding: 0.1rem;padding-top:0.3rem;background-color:#546E7A;"><i class="material-icons">format_indent_increase</i></button>';
				$txtoptions = array("style"=>"width:83%;display:inline;position:absolute;height:2.12rem;padding-left: 5px;","readonly"=>"readonly","tabindex"=>"".$tabindex."","class"=>"".$mandatoryopt."");
				echo text('parent'.$fieldval['tabname'].'','',$txtoptions);
				echo hidden($fieldval['fieldname'],'');
				echo '<ul class="dl-menu maintreegenerate large-12 medium-6 small-12 column" id="'.$modulename.'listuldata">';
				$datatree = $CI->Basefunctions->treecreatemodel($fieldval['ddparenttable'],$fieldtablename1);
				createTree($datatree);
				echo close('ul');
				echo close('div');
				echo '<input type="hidden" value="'.$mandatoryopt.'" id="treevalidationcheck" name="treevalidationcheck">';
				echo '<input type="hidden" value="'.$fieldval['filedlabel'].'" id="treefiledlabelcheck" name="treefiledlabelcheck">';
				echo close('div');
				break;

			case 22:
				//password
				$mandlab = "";
				$mandatoryopt = ""; //mandatory option set
				if($fieldval['mandmode'] == "Yes") {
					$mandatoryopt = 'validate[required,minSize[6]'.$mlength.$rule.']';
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				} else {
					$mandatoryopt = 'validate[minSize[6]'.$mlength.$rule.']';
				}
				$labval = $fieldval['filedlabel'].$mandlab;
				$txtname = $fieldval['fieldname'];
				$labattr = array('for'=>''.$txtname.'','class'=>'');
				$txtval = $fieldval['defvalue'];
				$typeinfo = "password";
				$divattr = array('class'=>'input-field '.$colstyle.''.$fildmode.'','id'=>''.$fieldval['fieldname'].'divhid');
				$txtattr = array('class'=> ''.$mandatoryopt.'','type'=>''.$typeinfo.'','tabindex'=>''.$tabindex.'','readonly'=>''.$readmode.'','data-prompt-position'=>'bottomLeft');
				return passwordspan($divattr,$labval,$labattr,$txtname,$txtval,$txtattr);
				break;

			case 23:
				//attribute set view create
				$mandlab = "";
				$txtval = "";
				$mandatoryopt = ""; //mandatory option set
				if($fieldval['mandmode'] == "Yes") {
					$mandatoryopt = 'validate[required'.$rule.']';
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				}
				$attrdata = $CI->Basefunctions->attributeviewdataddfetch($fieldval['moduleid']);
				foreach($attrdata as $key) {
					$labval = $key['Name'].$mandlab;
					$labattr = array();
					$name = $key['Name'];
					$value = array();
					$defval = array();
					$attributeid = $key['Id'];
					$ddowndata = $CI->Basefunctions->dynamicattributedropdownvalfetch($attributeid);
					foreach($ddowndata as $key):
						$value[$key->Id] = $key->Name;
					endforeach;
					$txtval = ( ($txtval!='')? $txtval : 1 );
					$valoption = "id";
					$dataattr = array(''.$fieldval['fieldname'].'hidden'=>'name');
					$multiple = $fieldval['multiple'];
					$seldataattr = array("placeholder"=>"",'defvalattr'=>''.$txtval.'');
					$option = array("class"=>"chzn-select ".$mandatoryopt."".$readmode."","tabindex"=>"".$tabindex."","data"=>$seldataattr,'data-prompt-position'=>'bottomLeft:14,36');
					$hidname = '';
					$hidval = '';
					$divattr = array('class'=>''.$colstyle.''.$fildmode.'','id'=>''.$name.'divhid');
					echo dropdownspandefault($divattr,$labval,$labattr,$name,$value,$valoption,$dataattr,$multiple,$option,$defval,$hidname,$hidval);
				}
				break;
				
			case 24:
			//Rich text editor
				$mandlab = "";
				$mandatoryopt = ""; //mandatory option set
				if($fieldval['mandmode'] == "Yes") {
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				}
				echo '<div class="large-12 column">';
					echo '<label>'.$fieldval['filedlabel'].$mandlab.'</label>';
					echo "<div id='".$fieldval['fieldname']."'>";
					echo "</div>";
				echo "</div>";
				echo "<script>
					$(document).ready(function(){
						$('#".$fieldval['fieldname']."').froalaEditor({inlineMode: false, alwaysBlank: true,toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'fontFamily', 'fontSize', '|', 'color', 'emoticons', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent','insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '|', 'quote', 'insertHR', 'undo', 'redo', 'clearFormatting', 'selectAll','help','html','addspan','removespan']});
					});
					</script>";
				froalaeditorfileslinks();			
				break;

			case 25:
				//drop down
				$mandlab = "";
				$txtval = "";
				$mandatoryopt = ""; //mandatory option set
				if($fieldval['mandmode'] == "Yes") {
					$mandatoryopt = 'validate[required'.$rule.']';
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				}
				$divattr = array('class'=>'static-field '.$colstyle.''.$fildmode.'','id'=>''.$fieldval['fieldname'].'divhid');
				$labval = $fieldval['filedlabel'].$mandlab;
				$labattr = array();
				$name = $fieldval['fieldname'];
				$value = array();
				$tablename = substr($fieldval['ddparenttable'],0,-2);
				$tabid = $tablename.'id';
				$tabfieldname = $tablename.'name';
				if($fieldval['tabname'] != "") {
					$ddowndata = $CI->Basefunctions->dynamicdropdownvalfetchparent($tablename,$tabfieldname,$tabid,$fieldval['defvalue']);
					if( ($tablename=='lead' && $tabfieldname=='leadname') || ($tablename=='lead' && $tabfieldname=='lastname') || ($tablename=='contact' && $tabfieldname=='contactname') || ($tablename=='contact' && $tabfieldname=='lastname') ) {
						foreach($ddowndata as $key):
							$sname = (($key->SName!='')? $key->SName.' ':'');
							$lname = (($key->LName!='')? ' '.$key->LName:'');
							$value[$key->Id] = $sname.$key->Name.$lname;
						endforeach;
					} else {
						foreach($ddowndata as $key):
							$value[$key->Id] = $key->Name;
						endforeach;
					}
				}
				$txtval = ( ($txtval!='')? $txtval : 1 );
				$valoption = "id";
				$dataattr = array(''.$fieldval['fieldname'].'hidden'=>'name');
				$multiple = $fieldval['multiple'];
				$seldataattr = array("placeholder"=>"",'defvalattr'=>''.$txtval.'');
				$option = array("class"=>"chzn-select ".$mandatoryopt."".$readmode."","tabindex"=>"".$tabindex."","data"=>$seldataattr,'data-prompt-position'=>'bottomLeft:14,36');
				$hidname = '';
				$hidval = '';
				echo dropdownspan($divattr,$labval,$labattr,$name,$value,$valoption,$dataattr,$multiple,$option,$hidname,$hidval);
				break;
				
			case 26:
				//drop down for parent table with condition (Relation drop down)
				$mandlab = "";
				$txtval = "";
				$mandatoryopt = ""; //mandatory option set
				if($fieldval['mandmode'] == "Yes") {
					$mandatoryopt = 'validate[required'.$rule.']';
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				}
				$divattr = array('class'=>'static-field '.$colstyle.''.$fildmode.'','id'=>''.$fieldval['fieldname'].'divhid');
				$labval = $fieldval['filedlabel'].$mandlab;
				$labattr = array();
				$name = $fieldval['fieldname'];
				$value = array();
				$tablename = substr($fieldval['colname'],0,-2);
				$tabid = $tablename.'id';
				$tabfieldname = $tablename.'name';
				if($fieldval['tabname'] != "") {
					if($fieldval['ddparenttable'] != "||"){
						$ddowndata = $CI->Basefunctions->dynamicdropdownvalfetchwithcond($tablename,$tabfieldname,$tabid,$fieldval['ddparenttable']);
						if($ddowndata != ''){
							foreach($ddowndata as $key):
							$value[$key->Id] = $key->Name;
							endforeach;
						}	
					}
				}
				$txtval = ( ($txtval!='')? $txtval : 1 );
				$valoption = "id";
				$dataattr = array(''.$fieldval['fieldname'].'hidden'=>'name');
				$multiple = $fieldval['multiple'];
				$seldataattr = array("placeholder"=>"",'defvalattr'=>''.$txtval.'');
				$option = array("class"=>"chzn-select ".$mandatoryopt."".$readmode."","tabindex"=>"".$tabindex."","data"=>$seldataattr,'data-prompt-position'=>'bottomLeft:14,36');
				$hidname = '';
				$hidval = '';
				echo dropdownspan($divattr,$labval,$labattr,$name,$value,$valoption,$dataattr,$multiple,$option,$hidname,$hidval);
				break;

			case 27:
				//Module drop down value fetch
				$mandlab = "";
				$txtval="";
				$mandatoryopt = ""; //mandatory option set
				if($fieldval['mandmode'] == "Yes") {
					$mandatoryopt = 'validate[required'.$rule.']';
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				}
				$divattr = array('class'=>'static-field '.$colstyle.''.$fildmode.'','id'=>''.$fieldval['fieldname'].'divhid');
				$labval = $fieldval['filedlabel'].$mandlab;
				$labattr = array();
				$name = $fieldval['fieldname'];
				$value = array();
				$tablename = substr($fieldval['colname'],0,-2);
				$tabid = $tablename.'id';
				$tabfieldname = $tablename.'name';
				if($fieldval['tabname'] != "") {
					$ddowndata = $CI->Basefunctions->dynamicmoduledropdownvalfetch($fieldval['moduleid']);
					foreach($ddowndata as $key):
						$value[$key->Id] = $key->Name;
					endforeach;
				}
				$txtval = ( ($txtval!='')? $txtval : 1 );
				$valoption = "id";
				$dataattr = array(''.$fieldval['fieldname'].'hidden'=>'name');
				$multiple = $fieldval['multiple'];
				$seldataattr = array("placeholder"=>"",'defvalattr'=>''.$txtval.'');
				$option = array("class"=>"chzn-select ".$mandatoryopt."".$readmode."","tabindex"=>"".$tabindex."","data"=>$seldataattr,'data-prompt-position'=>'bottomLeft:14,36');
				$hidname = '';
				$hidval = '';
				echo dropdownspan($divattr,$labval,$labattr,$name,$value,$valoption,$dataattr,$multiple,$option,$hidname,$hidval);
				break;
				
			case 28:
				//drop down with user specific condition
				$mandlab = "";
				$mandatoryopt = ""; //mandatory option set
				$txtval="";
				if($fieldval['mandmode'] == "Yes") {
					$mandatoryopt = 'validate[required'.$rule.']';
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				}
				$divattr = array('class'=>'static-field '.$colstyle.''.$fildmode.'','id'=>''.$fieldval['fieldname'].'divhid');
				$labval = $fieldval['filedlabel'].$mandlab;
				$labattr = array();
				$name = $fieldval['fieldname'];
				$value = array();
				$defval = array();
				$contioninfo = $fieldval['ddparenttable'];
				$conddatas = explode('|',$contioninfo);
				$ddowndata = $CI->Basefunctions->dynamicdropdownvalfetchwithusercond($conddatas[0],$conddatas[1],$conddatas[2],$conddatas[3],$conddatas[4]);
				foreach($ddowndata as $key):
					$value[$key->Id] = $key->Name;
					if( $key->defaultopt == 'Yes' || $key->defaultopt == '1' ) {
						$defval[] = $key->Id;
						$txtval = $key->Id;
					}
				endforeach;
				$txtval = ( ($txtval!='')? $txtval : 1 );
				$valoption = "id";
				$dataattr = array(''.$fieldval['fieldname'].'hidden'=>'name');
				$multiple = $fieldval['multiple'];
				$seldataattr = array("placeholder"=>"",'defvalattr'=>''.$txtval.'');
				$option = array("class"=>"chzn-select ".$mandatoryopt."".$readmode."","tabindex"=>"".$tabindex."","data"=>$seldataattr,'data-prompt-position'=>'bottomLeft:14,36');
				$hidname = '';
				$hidval = '';
				echo dropdownspandefault($divattr,$labval,$labattr,$name,$value,$valoption,$dataattr,$multiple,$option,$defval,$hidname,$hidval);
				break;
			
			case 29:
				//group drop down
				$mandlab = "";
				$txtval="";
				$mandatoryopt = ""; //mandatory option set
				if($fieldval['mandmode'] == "Yes") {
					$mandatoryopt = 'validate[required'.$rule.']';
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				}
				$divattr = array('class'=>'static-field '.$colstyle.''.$fildmode.'','id'=>''.$fieldval['fieldname'].'divhid');
				$labval = $fieldval['filedlabel'].$mandlab;
				$labattr = array();
				$name = $fieldval['fieldname'];
				$value = array();
				$defval = array();
				$tablename = substr($fieldval['colname'],0,-2);
				$tabfieldname = $tablename.'name';
				$value = array();
				$dataval = array();
				if($fieldval['tabname'] != "") {
					$ddowndata = $CI->Basefunctions->parentdynamicgroupdropdownvalfetch($fieldval['moduleid'],$tablename,$fieldval['ddparenttable'],$tabfieldname);
					$prev = ' ';
					$grpname = '';
					foreach($ddowndata as $key):
						$cur = $key->PId;
						$a ="";
						if($prev != $cur) {
							if( $prev != '' ) {
								$value[$grpname] = $dataval;
								$dataval = array();
							}
							$dataval[$key->CId] = $key->CName;
							$prev = $key->PId;
							$grpname = $key->PName;
						} else {
							$dataval[$key->CId] = $key->CName;
						}
					endforeach;
					$value[$grpname] = $dataval;
				}
				$txtval = ( ($txtval!='')? $txtval : 1 );
				$valoption = "id";
				$dataattr = array(''.$fieldval['fieldname'].'hidden'=>'name');
				$multiple = $fieldval['multiple'];
				$seldataattr = array("placeholder"=>"",'defvalattr'=>''.$txtval.'');
				$option = array("class"=>"chzn-select ".$mandatoryopt."".$readmode."","tabindex"=>"".$tabindex."","data"=>$seldataattr,'data-prompt-position'=>'bottomLeft:14,36');
				$hidname = '';
				$hidval = '';
				echo groupdropdownspandefault($divattr,$labval,$labattr,$name,$value,$valoption,$dataattr,$multiple,$option,$defval,$hidname,$hidval);
				break;
				
			case 30:
				//tags component
				$mandatoryopt='';
				$mandlab='';
				if($fieldval['mandmode'] == "Yes") {
					$mandatoryopt = 'validate[required]';
					$mandlab = "<span class='mandatoryfildclass'>*</span>";
				}
				echo "<script>
					$(document).ready(function(){
						var ddndatas = '".$fieldval['defvalue']."';
						ddndatas = ddndatas.replace(/\|/g,',');
						var ddsdatas = ddndatas.split(',');
						$('#".$fieldval['fieldname']."').select2({
							tags:ddsdatas,
							tokenSeparators: [','],
						});
						setTimeout(function(){
							$('#".$fieldval['fieldname']."').select2('val',ddsdatas);
							$('#".$fieldval['fieldname']."').trigger('change');
						},1000);
					});
				</script>";
				$labval = $fieldval['filedlabel'].$mandlab;
				$labattr = array();
				$txtname = $fieldval['fieldname'];
				$txtval = $fieldval['defvalue'];
				$divattr = array('class'=>''.$colstyle.''.$fildmode.'','id'=>''.$txtname.'divhid');
				$txtattr = array('class'=>''.$mandatoryopt.'','tabindex'=>''.$tabindex.'','data-prompt-position'=>'bottomLeft','data-defvalattr'=>''.$txtval.'','data-elementtype'=>'tag');
				return tagspan($divattr,$labval,$labattr,$txtname,$txtval,$txtattr);
				break;
				
			default :
				//default content
				break;
		}
	}
	//froala editor file link
	
	
	
	function froalaeditorfileslinks() {
		echo '<link href="'.base_url().'css/froala/froala_all_css_infoinside.css" rel="stylesheet" type="text/css">';
		echo '<script src="'.base_url().'js/plugins/froala/froala_editor_plus_all_plugins.min.js" type="text/javascript"></script>';
    }
	

?>