<!--Delete Conformation Overlay-->
<div class="row toppadding">&nbsp;</div>
<div class="large-3 medium-6 small-10 large-centered medium-centered small-centered columns">
	<div class="overlaybackground">
		<div class="alert-panel">
			<div class="alertmessagearea">
				<div class="row">&nbsp;</div>
				<div class="alert-message">Delete this data?</div>
			</div>
			<div class="alertbuttonarea">
				<span class="firsttab" tabindex="1000"></span>
				<input type="button" id="viewdeleteconyes" name="" value="Delete" tabindex="1001" class="alertbtnyes ffield " >
				<input type="button" id="viewdeleteconno" name="" value="Cancel" tabindex="1002" class="alertbtnno flloop  alertsoverlaybtn" >
				<span class="lasttab" tabindex="1003"></span>
			</div>
			<div class="row">&nbsp;</div>
		</div>
	</div>
</div>