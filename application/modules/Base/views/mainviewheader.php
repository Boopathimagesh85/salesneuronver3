<?php
	if(isset($otype)) {
		$otype = $otype;
	} else {
		$otype = 'form';
	}
	//user information
	$CI =& get_instance();
	$name='';
	$email='';
	$img="";
	$imgfromid = "";
	$path = "";
	$empid = "";
	$dataset=$CI->db->select('employeeid,employeename,lastname,emailid,employeeimage,employeeimage_fromid,employeeimage_path,salutation.salutationname,mobilenumber')->from('employee')->join('salutation','salutation.salutationid=employee.salutationid')->where('employee.employeeid',$CI->Basefunctions->logemployeeid)->get();
	foreach($dataset->result() as $row) {
		$name=$row->employeename;
		$email=$row->emailid;
		$img=$row->employeeimage;
		$imgfromid = $row->employeeimage_fromid;
		$path = $row->employeeimage_path;
		$empid = $row->employeeid;
		$mobile = $row->mobilenumber;
	}
	$path = '<span class=""><span id="logouticon" style="color:white;"><i title="logout" id="logouticon" class="material-icons" style="font-size: 1.6rem;line-height:2rem;color: black;">power_settings_new</i></span></span>';
	$ratedisplaymainview = $this->Basefunctions->get_company_settings('ratedisplaymainview');
	$mainjshidtakeordercategory = $this->Basefunctions->get_company_settings('takeordercategory');
	$mainjshidtransactioncategoryfield = $this->Basefunctions->get_company_settings('transactioncategoryfield');
	$amountround =  $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); //amount round-off
	$vendoruserroleid = $this->Basefunctions->userroleid;
?>
<div class="desktop actionmenucontainer action-menu">
	<ul class="module-view">
		<li>
		<div class="homepagediv" style="position: relative;left: 0px;">
				 <span class="icon-box homepageicon" id="homepageicon" style="position:relative;top:9px"><i title="menu" class="material-icons" style="font-size: 28px;line-height: 1.4;">menu</i><span class="actiontitle">Menu Icon</span></span>
			</div>
		</li>
	</ul>
	<ul class="module-view">
		<li>
			<span class="module-name"><?php echo $gridtitle; ?></span>
		</li>
	</ul>
	<?php if($otype == 'form') { 	
		$device = $this->Basefunctions->deviceinfo();
			if($device == 'phone') {
			} else {
				if($vendoruserroleid != 3) {
		echo '<ul class="module-view" style="top: 15px;left: -8px;position: relative;">
				<li data-activates="createshortcut" id="createcheck">
					<span class="createicon"><span style="position:relative;top:-3px;left:0px;">Create</span></span>
				</li>
			</ul>
			<ul id="createshortcut" class="action-drop arrow_box head-help-ul" style="width: 200px;white-space: nowrap;position: absolute;top: 60px;left: 185px;opacity: 1; ">
				<li id="supportcenter" style="color:white;background-color:#2c2f48;height: 60px;top: -8px; position: relative;border-top-left-radius: 4px;border-top-right-radius: 4px;"><i title="supportcenter" id="supportcenter" class="material-icons" style="font-size: 2rem;line-height: 1.9;color: #fff;">add_box</i><span style="position:relative;top:-12px;color:#fff !important;font-size:0.9rem;font-family:
			"Nunito, sans-serif;"">Shortcuts Panel</span></li>
			<li  id="accountshortcut"><i title="upgradeplan" id="upgradeplanicon" class="material-icons" style="font-size: 1.4rem;line-height: 2.0rem;color: #788db4;">person_add</i><span style="position:relative;top:-5px;">Account</span></li>
				<li  id="productshortcut"><i title="upgradeplan" id="upgradeplanicon" class="material-icons" style="font-size: 1.4rem;line-height: 2.0rem;color: #788db4;">add_circle</i><span style="position:relative;top:-5px;">Product</span></li>
				<li  id="stockshortcut"><i title="upgradeplan" id="upgradeplanicon" class="material-icons" style="font-size: 1.4rem;line-height: 2.0rem;color: #788db4;">library_add</i><span style="position:relative;top:-5px;">Stock Entry</span></li>
				<li id="transactionshortcut"><i title="upgradeplan" id="upgradeplanicon" class="material-icons" style="font-size: 1.4rem;line-height: 2.0rem;color: #788db4;">add_shopping_cart</i><span style="position:relative;top:-5px;">Transaction</span></li>
				<li id="chitshortcut"><i title="upgradeplan" id="upgradeplanicon" class="material-icons" style="font-size: 1.4rem;line-height: 2.0rem;color: #788db4;">note_add</i><span style="position:relative;top:-5px;">Chit Entry</span></li>
				<li id="productaddonshortcut"><i title="upgradeplan" id="upgradeplanicon" class="material-icons" style="font-size: 1.4rem;line-height: 2.0rem;color: #788db4;">add_circle</i><span style="position:relative;top:-5px;">Product Addon</span></li>
			</ul>';							
			}	
?>
		<?php if($otype == 'form') { ?>
		<?php if($ratedisplaymainview == '1' && $vendoruserroleid != '3' && $device != 'phone' && $device != 'tablet') { ?>
			<div class="ratedisplaytopmenu large-5 small-5 medium-5 columns" style="left: 25% !important;font-size: 1.1rem;position: relative;top: 25px;font-family: 'Nunito semibold', sans-serif;">
				<?php
					$ratedisplaypurityvalue = explode(',',$this->Basefunctions->get_company_settings('ratedisplaypurityvalue'));
					$totalcount = count($ratedisplaypurityvalue);
					$i = 0;
					foreach($ratedisplaypurityvalue as $value) {
						$purityshortname = $this->Basefunctions->singlefieldfetch('shortname','purityid','purity',$value);
						$todayrate = $this->Basefunctions->get_todaygoldratevalue('currentrate',$value);
						if(($totalcount-1) == $i) {
							echo '<span class="'.$purityshortname.'" >'.$purityshortname.': '.(round($todayrate,$amountround)).'</span>';
						} else {
							echo '<span class="'.$purityshortname.'" >'.$purityshortname.': '.(round($todayrate,$amountround)).' | </span>';
						}
						$i++;
					}
				?>
			</div>
			<?php } ?>
			<?php } }?>	
		<div class="top-header-details-for-all">
			<div class="head-notification">
			<?php
				if($device=='phone') {
					
					echo '<span class="icon-box bellnotificationclass" id="bellnotification" style="position:relative;top:5px;"><i title="notification" id="bellnotificationicon" class="material-icons" style="font-size: 1.8rem;line-height: 1.0;color: #ff5252;left: -40px; position: relative;">notifications</i><span class="actiontitle">Notification</span></span>';
					echo '<span id="personalsettingsicon" style="position:relative;top:5px;color:white;"><i title="personalsettings" id="personalsettingsicon" class="material-icons" style="font-size: 1.7rem;line-height: 2rem;color: #788db4;left: -35px; position: relative;">people</i></span>';
					echo '<span id="logouticon" style="position:relative;top:5px;color:white;"><i title="logout" id="logouticon" class="material-icons" style="font-size: 1.7rem;line-height: 2rem;color: #788db4;left: 45px; position: relative;top: -33px;">power_settings_new</i></span>';
				} else {
					
					echo '<span class="icon-box bellnotificationclass" id="bellnotification"><i title="notification" id="bellnotificationicon" class="material-icons" style="font-size: 1.8rem;line-height: 1.0;color: #ff5252;left: -40px; position: relative;">notifications</i><span class="actiontitle">Notification</span></span>';
					echo '<span id="personalsettingsicon" style="color:white;"><i title="personalsettings" id="personalsettingsicon" class="material-icons" style="font-size: 1.7rem;line-height: 2rem;color: #788db4;left: -35px; position: relative;">people</i></span>';
					echo '<span id="logouticon" style="color:white;"><i title="logout" id="logouticon" class="material-icons" style="font-size: 1.7rem;line-height: 2rem;color: #788db4;left: 45px; position: relative;top: -33px;">power_settings_new</i></span>';
				}?>
			</div>
		</div>
	<?php } ?>
</div>
<input type ="hidden" id="mainjshidtransactioncategoryfield" value="<?php echo $mainjshidtransactioncategoryfield?>"/>
<input type ="hidden" id="mainjshidtakeordercategory" value="<?php echo $mainjshidtakeordercategory?>"/>