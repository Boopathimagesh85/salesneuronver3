<style type="text/css">
.rppdropdown {
	left:50% !important;
}
</style>
<?php
	if(isset($formtype)) {
		$formtype = $formtype;
	} else {
		$formtype = 'singleform';
	} 
	if(isset($viewtype)) {
		$viewtype = $viewtype;
	} else {
		$viewtype = 'enable';
	}
	$device = $this->Basefunctions->deviceinfo();
?>
		<div class="large-12 columns paddingzero viewheader">
		<?php if($formtype == 'singleform') { ?>
		<?php } ?> 	
	<?php if($formtype == 'singleform') { ?> <!-- Single Section Add Form with grid Full view (Like Serial Master module)-->
			<?php $this->load->view('Base/mainviewheader');
			echo '<div class="desktop actionmenucontainer headeraction-menu">'; ?>
	<?php } else if($formtype == 'logaudmodform') { //<!-- For Audit Log /Login History / Module Editor -->
?>
			<div class="desktop actionmenucontainer headeraction-menu ">
				<ul class="module-view">
					<li class="view-change-icon" style="position: relative; height: 36px;top: -8px;left: -9px;">
						<div class="drop-container viewdropbox dropdown-button disable-dropdown-btn-for-all" >
							<span class="icon-box" style="position: relative;top: -17px;font-size: 1.0em !important;margin-right: 16px;color: black;line-height: 1.9rem;font-family: 'Nunito', sans-serif;">Basic View</span>
						</div>	
					</li>
				</ul>
				<?php if($gridid == 'moduleeditorgrid') { // for module manager?> 
					<ul class="actiontoggle-view tabaction" style="position:relative;left:3px;top: -15px;">
						<li class="act
						ion-icons" style="position:relative;top: 2px;line-height: 1em;">
							<span id="modulebuildericon" class="icon-box addiconclass add-form-for-2nd-head">Add</span>&nbsp;&nbsp;
							<div class="drop-container advanceddropbox dropdown-button more-btn-for-2nd-head" data-activates="advanceddrop"><span class="icon-box" style="position:relative;top:-1px;"><i title="help" id="viewcategory" class="material-icons" style="font-size: 1.6rem;line-height: 1.0;color: #fff;left:-8px;top:0px;position:relative;">more_horiz</i></span></div>
							<ul id="advanceddrop" class="navaction-drop arrow_box" style="left: 1067.98px;">
								<li id="modulecustomizeicon"><span class="icon-box"><i class="material-icons editiconclass" title="Module Customize">edit</i>Customization</span></li>
								<li id="moduledeleteicon"><span class="icon-box"><i class="material-icons" title="Disable">visibility_off</i>Disable</span></li>
								<li id="moduleenableicon"><span class="icon-box"><i class="material-icons" title="Enable">visibility</i>Enable</span></li>
							</ul>
						</li>
					</ul>
				<?php } else if($gridid == 'loginhistorygrid') { //for login historey and auditlog ?>
				 <ul class="toggle-view tabaction" style="position:relative;left:-2px;">
					 <li class="action-icons" style="position:relative;top: 2px;line-height: 1em;">
					 	<span id="exporticon" class="icon-box add-form-for-2nd-head">Export</span>&nbsp;&nbsp;
					 </li>
				</ul>
				<?php } else { //for login historey and auditlog ?>
				 <ul class="toggle-view tabaction" style="position:relative;left:-2px;top:-5px !important;">
					 	<div class="drop-container advanceddropbox dropdown-button more-btn-for-2nd-head" data-activates="advanceddrop"><span class="icon-box" style="position:relative;"><i title="help" id="viewcategory" class="material-icons" style="font-size: 1.6rem;line-height: 1.0;color: #fff;left:-8px;top:0px;position:relative;">more_horiz</i></span></div>
						<ul id="advanceddrop" class="singleaction-drop arrow_box" style="width:200px;top: 56px !important;">
						<li id="peronalsettingicon" style="color:white;background-color:#2c2f48;height: 60px;    top: -8px; position: relative;border-top-left-radius: 4px;border-top-right-radius: 4px;"><i title="actionicons" id="actionicons" class="material-icons" style="font-size: 2rem;line-height: 1.9;color: #fff;">apps</i><span style="position:relative;top:-12px;color:#fff !important;font-size:0.9rem; font-family: 'Nunito',sans-serif !important;">Actions Panel</span></li>
							<li id="addfilter" class="hidedisplay"><span class="icon-box"><i title="actionicons" id="actionicons" class="material-icons" style="font-size: 2rem;line-height: 1.9;color: black;">filter</i>Filter</span></li>
							<li id="exporticon"><span class="icon-box"><i title="actionicons" id="actionicons" class="material-icons" style="font-size: 2rem;line-height: 1.9;color: black;">list</i>Excel</span></li>
						</ul>
				</ul>
				<?php }?>
			</div>
	<?php } else if($formtype == 'workflow') { ?><!-- For Workflow -->
		<?php
			$device = $this->Basefunctions->deviceinfo();
		   	echo '<div class="desktop actionmenucontainer headeraction-menu ">
					<ul class="module-view">
						<li class="view-change-icon" style="position: relative;height: 36px;left: -10px;top:-5px;">
							<div class="drop-container viewdropbox dropdown-button" style="border-radius: 2px;color: black;  font-size: 14px; height:32px;padding-left:10px;padding-right:10px;position:relative;padding-top:6px;padding-bottom:5px;top:3px;    position: relative;height: 36px;border-bottom-style: solid;left: 17px;">
								<span class="icon-box" style="position: relative;top: -17px;font-size: 1.0em !important;margin-right: 16px;color: black;line-height: 1.9rem;font-family: "Nunito", sans-serif;">Basic View</span>
							</div>
						</li>
					</ul>
					<ul class="actiontoggle-view tabaction" style="position:relative;left:3px;">
					<li class="action-icons" style="position:relative;top: 2px;line-height: 1em;">
						<span id="addicon" class="icon-box addiconclass add-form-for-2nd-head">Add</span>&nbsp;&nbsp;
						<div class="drop-container advanceddropbox dropdown-button more-btn-for-2nd-head" data-activates="advanceddrop"><span class="icon-box" style="position:relative;top:-1px;"><i title="help" id="viewcategory" class="material-icons" style="font-size: 1.6rem;line-height: 1.0;color: #fff;left:-8px;top:0px;position:relative;">more_horiz</i></span></div>
						<ul id="advanceddrop" class="navaction-drop arrow_box">
							<li id="editicon"><span class="icon-box"><i class="material-icons editiconclass" title="Edit">edit</i>Edit</span></li>
							<!--<li id="deleteicon"><span class="icon-box"><i class="material-icons deleteiconclass" title="Delete">delete</i>Delete</span></li>-->
							<li id="disableicon"><span class="icon-box"><i class="material-icons" title="Disable">visibility_off</i>Disable</span></li>
							<li id="enableicon"><span class="icon-box"><i class="material-icons" title="Enable">visibility</i>Enable</span></li>
							<!--<li id="refresh"><span class="icon-box"><i class="material-icons reloadiconclass" title="Reload">refresh</i>Reload</span></li>-->
						</ul>
					</li>
				</ul>
			</div>';
		?>
	<?php } else if($formtype == 'dataimport') { ?> <!-- For Data import -->
		<?php
				echo '<div class="desktop actionmenucontainer headeraction-menu ">
						<ul class="module-view">
					<li class="view-change-icon" style="position: relative; height: 36px;top: -8px;left: -9px;">
						<div class="drop-container viewdropbox dropdown-button disable-dropdown-btn-for-all" >
							<span class="icon-box" style="position: relative;top: -17px;font-size: 1.0em !important;margin-right: 16px;color: black;line-height: 1.9rem;font-family: "Nunito, sans-serif;">Basic View</span>
						</div>	
					</li>
				</ul>
						
						<ul class="actiontoggle-view tabaction" style="position:relative;left:3px;">';
					echo '<li class="action-icons" style="position:relative;top: 2px;line-height: 1em;">
							<span id="addicon" class="icon-box addiconclass add-form-for-2nd-head">Add</span>&nbsp;&nbsp;
							<div class="drop-container advanceddropbox dropdown-button more-btn-for-2nd-head" data-activates="advanceddrop"><span class="icon-box" style="position:relative;"><i title="help" id="viewcategory" class="material-icons" style="font-size: 1.6rem;line-height: 1.0;color: #fff;left:-8px;top:0px;position:relative;">more_horiz</i></span></div>
							<ul id="advanceddrop" class="singleaction-drop arrow_box" style="width:200px;top: 56px !important;">
								<li id="personalsettingicon" style="color:white;background-color:#2c2f48;height: 60px;    top: -8px; position: relative;border-top-left-radius: 4px;border-top-right-radius: 4px;"><i title="actionicons" id="actionicons" class="material-icons" style="font-size: 2rem;line-height: 1.9;color: #fff;">apps</i><span style="position:relative;top:-12px;color:#fff !important;font-size:0.9rem; font-family: "Nunito",sans-serif !important;">Actions Panel</span></li><li id="datamanipulationicon"><span class="icon-box datamanipulationiconclass"><i class="material-icons" title="Data Manipulation">import_export</i>Data Manipulation</span></li>
								<li id="findduplicatesicon"><span class="icon-box findduplicatesiconclass"><i class="material-icons" title="Find Duplicates">find_in_page</i>Find Duplicates</span></li>
								<li id="importlogreload"><span class="icon-box reloadiconclass"><i class="material-icons" title="Reload">refresh</i>Reload</span></li>
							</ul>';
					echo '</li>
				</ul></div>';
		?>
	<?php } else if($formtype == 'reportdashbaord') { ?> <!-- For Dashabordand Reports -->
		<div class="desktop actionmenucontainer headeraction-menu ">	
			<ul class="module-view" style="position:relative;left:-1px;">
				<li class="view-change-dropdown" style="position: relative; height: 36px;border-bottom-style: solid;left: 17px;">
					<select id="mainviewreportfolder" class="chzn-select dropdownchange" data-placeholder="Select Folder"  tabindex="" name="mainviewreportfolder" title="" style="border-radius: 2px;height:32px;">
							<?php
								foreach($folderdata as $key):
								$select="";
							?>
							<?php if($moduleids[0] != 268) {?>
							<option value='all'>All Reports</option>
							 <option value="<?php echo $key->reportfolderid;?>" data-foldername="<?php echo $key->reportfoldername;?>" data-default="<?php echo $key->setdefault;?>" <?php echo $select; ?> ><?php echo $key->reportfoldername;?></option>
							<?php } else { ?>
								 <option value="<?php echo $key->dashboardfolderid;?>" data-foldername="<?php echo $key->dashboardfoldername;?>" data-default="<?php echo $key->setdefault;?>" <?php echo $select; ?> ><?php echo $key->dashboardfoldername;?></option>
							<?php } endforeach;?>
					</select>
				</li>
			</ul>
	<?php } ?>
	<!--  //action icon start -->
	<?php
		if(($formtype != 'logaudmodform') && ($formtype != 'viewcrud') && ($formtype != 'workflow') && ($formtype != 'dataimport')) {
				if($formtype != 'reportdashbaord') {
				 if($viewtype != 'disable') {?>
				<ul  class="module-view" style="position:relative;left:-2px;">
					<li class="view-change-dropdown" style="position: relative;height: 36px;border-bottom-style: solid;left: 17px;">
					<select id="dynamicdddataview" class="chzn-select dropdownchange" data-placeholder="Select View"  tabindex="" name="dynamicdddataview" title="" style="border-radius: 2px;position:relative;height:32px;">
					<?php 
					$userid = $this->Basefunctions->logemployeeid;
					foreach($dynamicviewdd as $key):
					$select="";
					if($userid == $key->employeeid && $key->viewcreationid == $key->viewid) {
						$select = 'selected="selected"';
					}
					?>
							<option value="<?php echo $key->viewcreationname;?>" data-dynamicdddataviewid="<?php echo $key->viewcreationid;?>" <?php echo $select; ?> ><?php echo $key->viewcreationname;?></option>
						<?php endforeach;?>
					</select>
				</li>
				<li>
					<span class="icon-box" id="" style="position: relative;left:0px;margin-right:20px;top:-5px;"><i title="help" id="viewoverlaytoggle" class="material-icons" style="font-size: 2.0rem;line-height: 1.0;color: #788db4;left:18px;position:relative;">filter_list</i><span class="actiontitle">Filter</span></span>
				</li>
				<!--
				<li class="view-change-icon">
				<div class="drop-container viewdropbox dropdown-button view-btn-for-2nd-head" data-activates="viewdrop">
						<span class="icon-box" style="position:relative;top: -17px;">View<i title="" class="material-icons" id="viewcategory" style="position:relative;left:3px;">details</i></span></div>
							<ul id="viewdrop" class="viewnavaction-drop view-dropdown-for-2nd-head arrow_box">
								<li id="viewaddicon" class="viewaddiconclass"><span class="icon-box">Create</span></li>
								<li id="viewediticon" class="viewediticonclass"><span class="icon-box">Edit</span></li>
								<li id="viewdeleteicon" class="viewdeleteiconclass"><span class="icon-box">Delete</span></li>
								<li id="viewcloneicon" class="viewcloneiconclass"><span class="icon-box">Clone</span></li>
							</ul>
						</span>
											
				</li>
				-->
			</ul>
			<?php } else {?>
			<ul  class="module-view" style="position:relative;left:-2px;">
				<li class="view-change-icon" style="position: relative;height: 36px;left: -10px;top:-5px;">
					<div class="drop-container viewdropbox dropdown-button disable-dropdown-btn-for-all" >
						<span class="icon-box" style="position: relative;top: -17px;font-size: 1.0em !important;margin-right: 16px;color: black;line-height: 1.9rem;font-family: 'Nunito', sans-serif;">Basic View</span>
					</div>	
				</li>
				<li>
					<span class="icon-box" id="" style="position: relative;left:0px;margin-right:20px;top:-5px;"><i title="help" id="viewoverlaytoggle" class="material-icons" style="font-size: 2.0rem;line-height: 1.0;color: #788db4;left:18px;position:relative;">filter_list</i><span class="actiontitle">Filter</span></span>
				</li>
			</ul>
			<?php }	}?>
				<?php 
				$device = $this->Basefunctions->deviceinfo();
				$prev = " ";
				$new = 0;
				$new1 = 0;
				$newp = 0;
				echo '<ul class="actiontoggle-view tabaction" style="position: relative;left: 3px;top: -10px;">';
				echo '<li class="action-icons">';
				foreach($actionassign as $key):
				if($key->toolbarname!='') {
					$cur = $key->toolbarcategoryid;
					if($cur == 1){
						if($prev!=$cur) {
							$prev=$key->toolbarcategoryid;
						}
						$fname = preg_replace('/[^A-Za-z0-9]/', '',$key->toolbarname);
						$name = strtolower($fname);
						echo '<span id="'.$name.'icon" '.$cur.' class="icon-box '.$name.'iconclass add-form-for-2nd-head">'.$key->toolbartitle.'</span>&nbsp;&nbsp';
					} else {
						$catname = $key->toolbarcategoryname;
						$cname = preg_replace('/[^A-Za-z0-9]/', '',$key->toolbarcategoryname);
						$categoryname = strtolower($cname);
						if($prev!=$cur) {
							if($prev != " ") {
								echo '<div class="drop-container advanceddropbox dropdown-button more-btn-for-2nd-head" data-activates="'.$categoryname.'drop"><span class="icon-box"><i title="help" id="viewcategory" class="material-icons" style="font-size: 1.6rem;line-height: 1.0;color: #fff;left:-8px;top:0px;position:relative;">more_horiz</i></span><ul id="'.$categoryname.'drop" class="singleaction-drop arrow_box" style="width:200px;top: 56px !important;"><li id="peronalsettingicon" style="color:white;background-color:#2c2f48;height: 60px;    top: -8px; position: relative;border-top-left-radius: 4px;border-top-right-radius: 4px;"><i title="actionicons" id="actionicons" class="material-icons" style="font-size: 2rem;line-height: 1.9;color: #fff;">apps</i><span style="position:relative;top:-12px;color:#fff !important;font-size:0.9rem; font-family: "Nunito",sans-serif !important;">Actions Panel</span></li>';
							}
							$prev=$key->toolbarcategoryid;
						}
						$fname = preg_replace('/[^A-Za-z0-9]/', '',$key->toolbarname);
						$name = strtolower($fname);
						if($prev ==$cur) 
						{
							if($name != 'viewselect' && $name != 'reload')
							{
								
								echo '<li id="'.$name.'icon"><span><i title="actionicons" id="actionicons" class="material-icons '.$key->description.' '.$name.'iconclass" title="'.$key->toolbartitle.'" style="font-size: 1.4rem;line-height: 1.9;color: #788db4;">'.$key->description.'</i><span style="position:relative;top:-5px;">'.$key->toolbartitle.'</span></span></li>'.PHP_EOL;
							}
						}
						$new++;
					}
				}
				endforeach;
				if($new != 0){
					echo '</ul></div>';
				}
				echo '</li>
				</ul>';
			//}
		}
	?>
	<!--  //action icon End -->
<?php if( $formtype == 'singleform' || $formtype == 'reportdashbaord'){ ?>
		 </div> 
<?php } ?>
<!--  Grid Start -->
<?php
	if(isset($gridid)) {
		echo '<div class="large-12 columns paddingzero viewheader">';
		if($formtype == 'logaudmodform') {
				echo '<div class="large-12 columns paddingzero gridview-container" style="top:-2px !important;">';			
		}
		else if($formtype == 'dataimport') 
		{
					echo '<div class="large-12 columns paddingzero gridview-container" style="top:-2px !important;">';			
		}
		else if($formtype == 'workflow') {
			echo '<div class="large-12 columns paddingzero gridview-container" style="top:-2px !important;">';	
		}
		else if($gridid == 'moduleeditorgrid') {
			echo '<div class="large-12 columns paddingzero gridview-container" style="top:-2px !important;">';	
			 
		 }
		else {
			echo '<div class="large-12 columns paddingzero gridview-container" style="top:-8px !important;">';		
		}
			  	echo '<div class="large-12 medium-12 columns paddingzero fullgridview">';
			echo '<div id="'.$gridwidth.'" class="touchgrid-container viewgridcolorstyle borderstyle" style="margin: 10px 10px 7px 15px;"><div class="desktop row-content" id="'.$gridid.'">
					<!-- Table content[Header & Record Rows] for desktop-->
					</div>
			   	<div class="footer-content footercontainer" id="'.$gridfooter.'">
					<!-- Footer & Pagination content -->
				</div></div>';
			echo '</div></div>';
	
		echo '</div>';
		if($gridid != 'reportsgrid' &&  $gridid != 'auditloggrid') {
			if($device!='phone') {
				if(isset($moduleid)) {
					$filterdata = $this->Basefunctions->filterdisplayinnonbasemodule($moduleid,$gridid);
					echo $filterdata;
				}
			}
		}	
	}
?>
<!--  View CRUD Overlay Header Start-->
<?php if($formtype == 'viewcrud') { ?> 
	<div class="large-12 columns headercaptionstyle headerradius paddingzero">
		<div class="large-6 medium-6 small-12 columns headercaptionleft">
			<span data-activates="slide-out" class="menu-icon button-collapse" title="Menu"><i class="material-icons">menu</i></span>
			<span class="gridcaptionpos"><span id="viewheadername">View Creation</span></span>
		</div>
		<div class="large-6 medium-6 small-12 columns addformheadericonstyle righttext small-only-text-center">
			<span id="viewwoconsubmitbtn" class="icon-box" title="Save"><i class="material-icons">save</i> <span class="actiontitle"> Save</span></span>
			<span id="vieweditconsubmitbtn" class="icon-box" title="Save"><i class="material-icons">save</i> <span class="actiontitle"> Save</span></span>
			<span id="viewcloseformiconid" class="icon-box" title="Close"> <i class="material-icons">close</i> <span class="actiontitle"> Close</span></span>
		</div>
	</div>
<?php } ?> 
<!--  View CRUD Overlay Header End -->