<div class="large-12 columns paddingzero formheader">
	<?php
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			echo '<div class="large-12 columns headercaptionstyle headerradius pricebookmastergridformreload paddingzero">
					<div class="large-6 medium-6 small-8 columns headercaptionleft">
					<span  data-activates="slide-out" class="headermainmenuicon button-collapse" title="Menu"><i class="material-icons">menu</i></span>
						<span class="gridcaptionpos"><span>'.$gridtitle.'</span></span>
					</div>
					<div class="large-5 medium-5 small-4 columns foriconcolor mainaction righttext">
						<span id="editicon" class="editiconhideclass icon-box hidedisplay" title="Edit">
						<i class="material-icons" >edit</i>
							<span class="actiontitle">Edit</span>
						</span>
						 <span id="userpasswordov" class="personalicon hidedisplay icon-box" title="Change Password">
							<i class="material-icons>security</i>
							<span class="actiontitle">Change Password</span>
						</span>
						<span id="dataupdatesubbtn" class="personalicon hidedisplay updatebtnclass icon-box" title="Save">
							<i class="material-icons">save</i>
							<span class="actiontitle">Save</span>
						</span>
						<span  id="closeaddform" class="personalicon hidedisplay icon-box" title="Close">
							<i class="material-icons">close</i>
							<span class="actiontitle">Cancel</span>
						</span>
						<span id="billedit" class="icon-box hidedisplay" title="Edit"><i class="material-icons" >edit</i></span>
						<span id="planupgrade" class="icon-box hidedisplay" title="Change Plan"><i class="material-icons">shopping_cart</i></span>
						<span id="planediticon" class="icon-box hidedisplay" style="display:none" title="Plan Edit"><i class="material-icons" >edit</i></span>
						<span id="cancelaccount" class="icon-box hidedisplay" title="Cancel Account"><i class="material-icons">cancel</i></span>';
						$this->load->view('Base/usermenugenerate');
						echo '<span id="groupcloseaddform" class="icon-box groupcloseaddformclass">
					<i class="material-icons" title="Close">close</i>
						</span>';
				echo '</div>';
			echo '</div>';
		} else {
			$this->load->view('Base/mainviewheader');
			  if($moduleid != '41' && $moduleid != '49' && $moduleid != '46' && $moduleid != '263' && $moduleid != '243' && $moduleid != '247' && $moduleid != '248' && $moduleid != '5' && $moduleid != '265' && $moduleid != '91' && $moduleid != '51' && $moduleid != '268' && $moduleid != '103' && $moduleid != '60' )	{
			  	echo '<div class="large-12 columns desktop actionmenucontainer headeraction-menu">
						<ul class="module-view"><li></li>';
				echo '</ul>
				<!--<div class="large-5 medium-5 columns foriconcolor mainaction righttext">-->
					<ul class="toggle-view tabaction">
						<li class="action-icons">
							<span class="editiconhideclass icon-box hidedisplay" id="newediticon" name="newediticon">
								<input id="printtempeditocn" name="printtempeditocn" tabindex="" value="Edit" class="alertbtnyes  ffield" type="button">
							</span>
							 <span id="userpasswordov" class="personalicon hidedisplay icon-box" title="Change Password">
								<input id="" name="" tabindex="" value="Change Password" class="alertbtnyes  ffield" type="button">
							</span>
							<span id="dataupdatesubbtn" class="personalicon hidedisplay updatebtnclass icon-box" title="Save">
								<input id="" name="" tabindex="" value="Save" class="alertbtnyes  ffield" type="button">
							</span>
							<!--<span  id="closeaddform" class="personalicon hidedisplay icon-box" title="Close">
								<i class="icon24 icon24-close" ></i>
								<span class="actiontitle">Cancel</span>
					</span>-->							
							<span id="billedit" class="icon-box editiconclass hidedisplay" title="Edit">
			  					<input id="" name="" tabindex="" value="Edit" class="alertbtnyes  ffield" type="button">
							</span>
							<span id="planupgrade" class="icon-box hidedisplay" title="Change Plan">
			  					<input id="" name="" tabindex="" value="Change Plan" class="alertbtnyes  ffield" type="button">
							</span>
							<span id="planediticon" class="icon-box hidedisplay" style="display:none" title="Plan Edit">
			  					<input id="" name="" tabindex="" value="Plan Edit" class="alertbtnyes  ffield" type="button">
			  				</span>
							<span id="cancelaccount" class="icon-box hidedisplay" title="Cancel Account">
			  					<input id="" name="" tabindex="" value="Cancel Account" class="alertbtnyes  ffield" type="button">
							</span>';
							$this->load->view('Base/usermenugenerate');
							echo '<span id="groupcloseaddform" class="icon-box groupcloseaddformclass">
								<input id="" name="" tabindex="" value="Close" class="alertbtnno  ffield" type="button">
							</span>';
						echo '</li>
						</ul>';
				//echo '</div>';
					echo '</div>';
			  }
		} 
	?>
	<!-- tab group creation -->
	<?php  if($device!='phone') {
		echo '<div class="large-12 columns addformunderheader">&nbsp; </div><div class="large-12 columns tabgroupstyle desktoptabgroup" style="height:32px;">
			<ul class="multitabs" data-tab="" style="position:relative;left:3px;">';
				$i=1;
				foreach ($modtabgrp as $modtab) {
					if($i == 1){
						$activeclass = ' active ftab';
					} else {
						$activeclass = '';
					}
					if($modtab['moduleid']=='243')
					{
						$sideclass = '';
					}else
					{
					$sideclass = 'sidebaricons';
					}
					echo '<li id="tab'.$i.'" class="'.$sideclass.' tab-title'.$activeclass.'" data-subform="'.$i.'">
						<span>'.$modtab['tabgrpname'].'</span>
					</li>';
					$i++;
				}
				echo '<li class="morelabel dbtab-title dbsidebaricons hidedisplay" data-dbsubform="x"><span class="waves-effect waves-ripple waves-light">More</span></li>
			<li id="moretab" class="tab-title moretab hidedisplay"><span  class="dropdown-button" data-activates="mastertabgroupmoredropdown">More<i class="material-icons">details</i></span>
					<ul id="mastertabgroupmoredropdown" class="dropdown-content"></ul>
				</li>
			</ul>';
			if(isset($action)) {
				$menu = "";
				$menustart = '';
				$menuadd = '';
				$menumore = '';
				$count = count($action);
				$m=1;
				for($nl=0;$nl<$count;$nl++) {
					if($nl == 0){
						$id = 'tab'.$m.'class';
						$class= 'tabclass';
					} else {
						$id = 'tab'.$m.'class';
						$class= 'tabclass hidedisplay';
					}
					echo "<ul id='".$id."' class='".$class."' style='position:relative;left:3px;float:right;top:-2px'><li class='action-icons' style='position:relative;top: 2px;line-height: 1em;'>";
					echo '<span id="'.$action[$nl]['actionid'].'" title="'.$action[$nl]['actiontitle'].'" class="icon-box addiconclass add-form-for-2nd-head">'.$action[$nl]['actiontitle'].'</span>'.PHP_EOL;
					if($action[$nl]['actionmore'] == 'Yes') {
						echo '<div class="drop-container advanceddropbox dropdown-button more-btn-for-2nd-head" data-activates="'.$action[$nl]['ulname'].'advanceddrop"><i title="help" id="viewcategory" class="material-icons" style="font-size: 1.6rem;line-height: 1.0;color: #fff;left:-4px;top:0px;position:relative;">more_horiz</i></div>';
					}
					echo "</li></ul>";
					$m++;
					
					
				}
			}
		echo '</div>';
	 } else {
		echo '<div class="large-12 columns tabgroupstyle mobiletabgroup">
			<ul class="tabs" data-tab="">';
				$i=1;
				foreach ($modtabgrp as $modtab) {
					if($i == 1){
						$activeclass = ' active ftab';
					} else {
						$activeclass = '';
					}
					if($modtab['moduleid'] == 263 || $modtab['moduleid'] == 243 || $modtab['moduleid'] == 247 || $modtab['moduleid'] == 248  || $modtab['moduleid'] == 49 || $modtab['moduleid'] == 41){
						$sideclass = 'sidebaricons';
					}else {
						$sideclass = 'sidebaricons';
					}
					echo '<li id="tab'.$i.'" class="'.$sideclass.' tab-title'.$activeclass.'" data-subform="'.$i.'">
						<span>'.$modtab['tabgrpname'].'</span>
					</li>';
					$i++;
				}
			echo '</ul>
		</div>';
	} ?>
</div>