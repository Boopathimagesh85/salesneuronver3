<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="viewddoverlay" style="z-index:110">
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-3 medium-6 small-10 large-centered medium-centered small-centered columns" style="">
			<span id="metaloverlaywizard" class=""> 
				<div class="row">&nbsp;</div>
				<div class="row" style="background:#f5f5f5">
					<div class="large-12 columns headerformcaptionstyle" style="height:auto">
						<div class="large-3 medium-3 small-12 columns" style="line-height:2rem">View</div>
						<div class="large-9 medium-9 small-12 columns righttext small-only-text-center" style="line-height:2rem">
							<i id="viewaddicon" class="material-icons viewaddiconclass viewactionicons" title="View Add">add</i>
							<i id="viewediticon" class="material-icons viewediticonclass viewactionicons" title="View Edit">edit</i>
							<i id="viewdeleteicon" class="material-icons viewdeleteiconclass viewactionicons" title="View Delete">delete</i>
							<i id="viewcloneicon" class="material-icons viewcloneiconclass viewactionicons" title="View Clone">dvr</i>
							<i id="viewselectioncloseid" class="material-icons" style="cursor:pointer" title="Close">close</i>
						</div>
					</div>			
					<div class="large-12 columns">
						<label>Select View</label>
						<select id="dynamicdddataview" class="chzn-select dropdownchange" data-placeholder="Select View"  tabindex="" name="dynamicdddataview" title="">
							<?php
								$userid = $this->Basefunctions->logemployeeid;
								foreach($dynamicviewdd as $key):
									$select="";
									if($userid == $key->employeeid && $key->viewcreationid == $key->viewid) {
										$select = 'selected="selected"';
									}
							?>
								<option value="<?php echo $key->viewcreationname;?>" data-dynamicdddataviewid="<?php echo $key->viewcreationid;?>" <?php echo $select; ?> ><?php echo $key->viewcreationname;?></option>
							<?php endforeach;?>
						</select>
					</div>
				</div>
				<div class="row" style="background:#f5f5f5">&nbsp;</div>
			</span>
		</div>
	</div>
</div>