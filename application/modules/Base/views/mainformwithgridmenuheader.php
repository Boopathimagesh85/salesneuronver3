<div class="large-12 columns paddingzero formheader">
	<?php
	    $j=1;
		foreach ($modtabgrp as $modtab) {
			if($modtab['moduleid'] == 103){
				$showstatus = 1;
			}else{
				$showstatus = 0;
			}
			if($modtab['moduleid'] == 52){
				$saleshowstatus = 1;
			}else{
				$saleshowstatus = 0;
			}
			if($modtab['moduleid'] == 'stone'){
				$stockentryshowstatus = 1;
			}else{
				$stockentryshowstatus = 0;
			}
			if($modtab['moduleid'] == 'company'){
				$companyshowstatus = 1;
			}else{
				$companyshowstatus = 0;
			}
			$j++;
		}
		$device = $this->Basefunctions->deviceinfo();
		if($device == 'phone' || $device == 'tablet') {
			echo '<div class="large-12 columns headercaptionstyle headerradius pricebookmastergridformreload paddingzero">
					<div class="large-6 medium-6 small-6 columns headercaptionleft">';
					if($saleshowstatus == 1) {
						echo '<span class="gridcaptionpos"><span class="transactiontypehead"></span><span class="salesgridcaptionpos">'.$gridtitle.'</span></span>';
					} else {
						echo '<span class="gridcaptionpos"><span class="transactiontypehead"></span><span>'.$gridtitle.'</span></span>';
					}
					echo '</div>';
				if($saleshowstatus == 1) {
					echo '<div class="large-6 medium-6 small-6 columns righttext addformheadericonstyle addformaction tablettranformheadericon" style="padding: 0px !important;">
					<span class="icon-box" id="mainheadericon" ><i class="material-icons" title="Main Header">apps</i></span>	
					<span class="icon-box" id="salespaymenticon" ><i class="material-icons" title="Payment">payment</i></span>	
					<span class="icon-box" id="billcommenticon" ><i class="material-icons" title="Barcode">payment</i></span>
					<span class="icon-box" id="formclearicon" style="display:none;"><i class="material-icons" title="Reload">refresh</i></span>
					<span class="icon-box" id="dataaddsbtn" ><i class="material-icons" title="Save">save</i></span>
					<span class="icon-box" id="closeaddform" ><i class="material-icons" title="Close">close</i></span>';
					echo '</div>';
				} else if($stockentryshowstatus == 1){
					echo '<div class="large-6 medium-6 small-7 columns righttext addformheadericonstyle addformaction tablettranformheadericon">
					<span class="icon-box" id="savestoneentrydata">
						<i title="Save" class="material-icons">save </i>
					</span>
					<span class="icon-box groupcloseaddformclass" id="closestoneentryform">
						<i title="Close" class="material-icons">close </i>
					</span>';
					echo '</div>';
				} else if($companyshowstatus == 1){
				  echo '<div class="large-6 medium-6 small-4 columns addformheadericonstyle righttext">
							<span id="companybtn" class="icon-box" title="Save"><i class="material-icons">save</i></span>
							<span id="closecompanyoverlay" class="icon-box"  title="Close" ><i class="material-icons">close</i></span>
					</div>';
				} else  if($showstatus == 0){
					$this->load->view('Base/formheadericons');
				} else{
					echo '<div class="large-6 medium-6 small-6 columns righttext addformheadericonstyle addformaction tablettranformheadericon">
					<span class="icon-box settingprocess" id="barcodeprocess" name="1"><i class="material-icons" title="Barcode">settings</i></span>
					<span class="icon-box settingprocess hidedisplay" id="rfidprocess" name="2"><i class="material-icons" title="RFID">settings</i></span>
					<span class="icon-box settingprocess hidedisplay" id="rfidtagmachine" name="3"><i class="material-icons" title="RFID Machine">settings</i></span>
					<span class="icon-box" id="dataaddsbtn" ><i class="material-icons" title="Save">save</i></span>
					<span class="icon-box" id="dataupdatesubbtn" ><i class="material-icons" title="Save">save</i>save</i></span>
					<span class="icon-box" id="reloadicon" ><i class="material-icons" title="Reload">refresh</i></span>
					<span class="icon-box" id="closeaddform" ><i class="material-icons" title="Close">close</i></span>';
					echo '</div>';
				 }
			echo '</div>';
		} else {
			echo '<div class="large-12 columns headercaptionstyle headerradius pricebookmastergridformreload paddingzero">
				<div class="large-6 medium-6 columns headercaptionleft">
					<ul class="module-view-form">
						<li>';
						if($saleshowstatus == 1) {
								echo '<span class="gridcaptionpos"><span class="transactiontypehead"></span><span class="salesgridcaptionpos" style="font-size: 1.05rem !important;">'.$gridtitle.'</span></span>';
						} else {
								echo '<span class="gridcaptionpos"><span class="transactiontypehead"></span><span>'.$gridtitle.'</span></span>';
						}
						echo '</li>
					</ul>
				</div>';
			    if($saleshowstatus == 1) {
			    	echo '<div class="large-6 medium-6 small-6 columns righttext addformheadericonstyle addformaction">
							<!--<span class="icon-box " id="salesbulkicon">
							<input id="" name="" tabindex="" value="Bulk Sales" class="alertbtnyes  ffield" type="button">
							<span class="actiontitle">Bulk Sales</span>
						</span>-->
						<span class="icon-box " id="mainheadericon" style="display:none;">
							<input id="" name="" tabindex="" value="Header" class="alertbtnyes  ffield" type="button">
							<span class="actiontitle">Header</span>
						</span>
							<span class="icon-box " id="salesrateicon" style="display:none;">
							<input id="" name="" tabindex="" value="Rate" class="alertbtnyes  ffield" type="button">
							<span class="actiontitle">Rate</span>
						</span>
						<span class="icon-box" id="billcommenticon">
							<input id="" name="" tabindex="" value="Comment" class="alertbtnyes  ffield" type="button">
							<span class="actiontitle">Comment</span>
						</span>
						<span class="icon-box" id="formclearicon">
							<input id="" name="" tabindex="" value="Reload" class="alertbtnyes  ffield" type="button">
							<span class="actiontitle">Reload</span>
						</span>
						<span class="icon-box" id="dataaddsbtn">
							<input id="" name="" tabindex="" style="background-color: #ff8000;" value="Save" class="alertbtnyes  ffield" type="button">
							<span class="actiontitle">Save</span>
						</span>
						<span class="icon-box" id="closeaddform">
							<input id="" name="" tabindex="" value="Close" class="alertbtnno  ffield" type="button">
							<!--<i title="Save" class="material-icons">close </i>-->
							<span class="actiontitle">Close</span>
						</span>';
			    	echo '</div>';
				} else if($stockentryshowstatus == 1) {
						echo '<div class="large-6 medium-6 small-7 columns righttext addformheadericonstyle addformaction">
						<span class="icon-box" id="savestoneentrydata">
							<input id="" name="" tabindex="" value="Save" class="alertbtnyes  ffield" type="button">
							<!--<i title="Save" class="material-icons">save </i>-->
							<span class="actiontitle">Save</span>
						</span>
						<span class="icon-box groupcloseaddformclass" id="closestoneentryform">
							<input id="" name="" tabindex="" value="Close" class="alertbtnno  ffield" type="button">
							<!--<i title="Close" class="material-icons">close </i>-->
							<span class="actiontitle">Close</span>
						</span>';
					echo 	'</div>';
				} else if($companyshowstatus == 1) {
				 	 echo '<div class="large-6 medium-6 small-4 columns addformheadericonstyle righttext">
							<span id="companybtn" class="icon-box" title="Save">
								<input id="" name="" tabindex="" value="Save" class="alertbtnyes  ffield" type="button">
								<!--<i class="material-icons">save</i>-->
								<span class="actiontitle">Save</span>
							</span>
							<span id="closecompanyoverlay" class="icon-box"  title="Close" >
								<input id="" name="" tabindex="" value="Close" class="alertbtnno  ffield" type="button">
								<!--<i class="material-icons">close</i>-->
								<span class="actiontitle">Close</span>
							</span>
						</div>';
				} else if($showstatus == 0) {
						$this->load->view('Base/formheadericons');
				} else {
					echo '<div class="large-6 medium-6 small-4 columns righttext addformheadericonstyle addformaction">
							<span class="icon-box hidedisplay" id="rfidstatusinfo" name="2">
								<input id="rfidstatusinfobtn" name="" tabindex="" value="C" class="alertbtnyes  ffield" type="button">
							</span>
							<span id="barcodeprocess" class="icon-box settingprocess">
								<input id="" name="" tabindex="" value="Barcode" class="alertbtnyes  ffield" type="button">
							</span>
							<span class="icon-box settingprocess hidedisplay" id="rfidprocess" name="2">
								<input id="" name="" tabindex="" value="RFID" class="alertbtnyes  ffield" type="button">
							</span>
							<span class="icon-box settingprocess hidedisplay" id="rfidtagmachine" name="3">
								<input id="" name="" tabindex="" value="RFID Machine" class="alertbtnyes  ffield" type="button">
							</span>
							<span class="icon-box" id="dataaddsbtn" >
								<input id="" name="" tabindex="" value="Save" class="alertbtnyes  ffield" type="button">
							</span>
							<span class="icon-box" id="dataupdatesubbtn" >
								<input id="" name="" tabindex="" value="Save" class="alertbtnyes  ffield" type="button">
							</span>
							<span class="icon-box" id="reloadicon" >
								<input id="" name="" tabindex="" value="Reload" class="alertbtnno  ffield" type="button">
							</span>
							<span class="icon-box" id="closeaddform" >
								<input id="" name="" tabindex="" value="Close" class="alertbtnno  ffield" type="button">
							</span>';
					echo '</div>';
				}
			echo '</div>';
		}
	?>
	<!-- tab group creation -->
	<?php  
	if($modtab['moduleid'] != 52){
	if($device!='phone') {
			
		echo '<div class="large-12 columns addformunderheader">&nbsp;</div><div class="large-12 columns tabgroupstyle desktoptabgroup">
			<ul class="tabs" data-tab="">';
				$i=1;
				foreach ($modtabgrp as $modtab) {
					if($i == 1){
						$activeclass = ' active ftab';
					} else {
						$activeclass = '';
					}
					if($modtab['moduleid'] == 263 || $modtab['moduleid'] == 243 || $modtab['moduleid'] == 48 || $modtab['moduleid'] == 103 || $modtab['moduleid'] == 50 ){
						$sideclass = 'sidebaricons';
					} else if($modtab['moduleid'] == 'company'){
						$sideclass = 'companysidebaricons';
					}else {
						$sideclass = 'sidebariconstab';
					}
					if($modtab['moduleid'] == 'company'){
						echo '<li id="overlaytab'.$i.'" class="'.$sideclass.' tab-title'.$activeclass.'" data-overlaysubform="'.$i.'">
							<span>'.$modtab['tabgrpname'].'</span>
						</li>';
					}else{
						echo '<li id="tab'.$i.'" class="'.$sideclass.' tab-title'.$activeclass.'" data-subform="'.$i.'">
							<span>'.$modtab['tabgrpname'].'</span>
						</li>';
					}
					$i++;
				}
				echo '<li class="morelabel dbtab-title dbsidebaricons hidedisplay" data-dbsubform="x"><span class="waves-effect waves-ripple waves-light">More</span></li>
				<li id="moretab" class="tab-title moretab hidedisplay"><span class="dropdown-button" data-activates="mastertabgroupmoredropdown">More<i class="material-icons">details</i></span>
					<ul id="mastertabgroupmoredropdown" class="dropdown-content"></ul>
				</li>
			</ul>
		</div>';
	 } else {
		echo '<div class="large-12 columns tabgroupstyle mobiletabgroup">
			<ul class="tabs" data-tab="">';
				$i=1;
				foreach ($modtabgrp as $modtab) {
					if($i == 1){
						$activeclass = ' active ftab';
					} else {
						$activeclass = '';
					}
					if($modtab['moduleid'] == 263 || $modtab['moduleid'] == 243 || $modtab['moduleid'] == 48 || $modtab['moduleid'] == 50 || $modtab['moduleid'] == 52){
						$sideclass = 'sidebaricons';
					}  else if($modtab['moduleid'] == 'company'){
						$sideclass = 'companysidebaricons';
					}
					else {
						$sideclass = 'sidebariconstab';
					}
					if($modtab['moduleid'] == 'company'){
						echo '<li id="overlaytab'.$i.'" class="'.$sideclass.' tab-title'.$activeclass.'" data-overlaysubform="'.$i.'">
							<span>'.$modtab['tabgrpname'].'</span>
						</li>';
					}else{
						echo '<li id="tab'.$i.'" class="'.$sideclass.' tab-title'.$activeclass.'" data-subform="'.$i.'">
							<span>'.$modtab['tabgrpname'].'</span>
						</li>';
					}
					$i++;
				}
			echo '</ul>
		</div>';
	} }?>
</div>