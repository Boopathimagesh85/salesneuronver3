<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboardfieldgeneration extends CI_Model {
	public function __construct() {		
		parent::__construct(); 
		$this->load->model('Home/homemodel');
    }
	//dash board creation
	public function dashboardcreate($dashboardchartdata,$rowid)
	{
		$tempgrplevel=0;
		$newgrpkey=0;
		$charttabgroup[]=array();
		foreach ($dashboardchartdata as $key => $val) {
			if($tempgrplevel===$val['charttabid']) {
				$charttabgroup[$tempgrplevel][$newgrpkey]=$val;
			} else {
				$charttabgroup[$val['charttabid']][$newgrpkey]=$val;
			}
			$newgrpkey++;
		}
		$i=1;
		$interval=20;
		echo '<div class="large-12 columns mblnopadding" style="height: 80vh !important;">'.PHP_EOL;
		echo '<input type="hidden" id="wholerowid" value="'.$rowid.'">'.PHP_EOL;
		foreach($charttabgroup as $chartkey => $chartval) {
			foreach($chartval as $ckey => $cval) {
				
				$this->dashboardchartgenerate($cval,$i,$rowid,$interval);
				$interval+=10;
				$i++;
			}
		}
		echo "</div>".PHP_EOL;
	}
	//chart generation
	public function dashboardchartgenerate($chartinfo,$i,$rowid,$interval) {
		$hiddenid='<div></div>';
		if($chartinfo['charttype'] == 'ibconversationwidget'){		
			$hiddenid='<input type="hidden" id="conversationdivid" data-moduleid="'.$chartinfo['mainmoduleid'].'" value="chartgenid'.$i.'" data-rowid="'.$rowid.'"/>';
		}else if($chartinfo['charttype'] == 'ibconversationwidgetmini'){		
			$hiddenid='<input type="hidden" id="conversationdivid" data-moduleid="'.$chartinfo['mainmoduleid'].'" value="chartgenid0" data-rowid="'.$rowid.'"/>';
		}elseif($chartinfo['charttype'] == 'ibnotificationwidget' || $chartinfo['charttype'] == 'ibnotificationwidgetmini'){
			$hiddenid='<input type="hidden" id="notificationdivid" data-moduleid="'.$chartinfo['mainmoduleid'].'" value="chartgenid'.$i.'" data-rowid="'.$rowid.'"/>';
		}
		switch( $chartinfo['dashtabtype'] ) {
			case 1:
				echo '<div class="large-12 columns end" >'.PHP_EOL;
				echo '<span class="fa fa-plus-circle widgetredirecticonhidden" style="display:none" id="chartreloadid'.$i.'"></span>';
				$this->dashboarddatascriptgen($chartinfo,$i,$rowid,$interval);
				echo $hiddenid.PHP_EOL;
				echo '<div class="large-12 columns end widgetheightview" id="chartgenid'.$i.'">'.PHP_EOL;				
				echo '</div>'.PHP_EOL;
				echo '</div>'.PHP_EOL;
				break;
			case 2:
				echo '<div class="large-6 columns end" >'.PHP_EOL; 
				echo '<span class="fa fa-plus-circle widgetredirecticonhidden" style="display:none" id="chartreloadid'.$i.'"></span>';
				$this->dashboarddatascriptgen($chartinfo,$i,$rowid,$interval);
				echo $hiddenid.PHP_EOL;
				echo '<div class="large-12 columns end widgetheightview" id="chartgenid'.$i.'">'.PHP_EOL;				
				echo '</div>'.PHP_EOL;
				echo '</div>'.PHP_EOL;
				break;
			case 3:
				echo '<div class="large-4 columns end" >'.PHP_EOL;
				echo '<span class="fa fa-plus-circle widgetredirecticonhidden" style="display:none" id="chartreloadid'.$i.'"></span>';
				echo $hiddenid.PHP_EOL;
				$this->dashboarddatascriptgen($chartinfo,$i,$rowid,$interval);
				echo '<div class="large-12 columns end widgetheightview" id="chartgenid'.$i.'">'.PHP_EOL;				
				echo '</div>'.PHP_EOL;
				echo '</div>'.PHP_EOL;
				break;
			case 4:
					echo '<div class="large-12 columns end miniwidgetcontent" >'.PHP_EOL;
					echo '<span class="fa fa-plus-circle widgetredirecticonhidden" style="display:none" id="chartreloadid'.$i.'"></span>';
					$this->dashboarddatascriptgen($chartinfo,$i,$rowid,$interval);
					echo $hiddenid.PHP_EOL;
					if($chartinfo['charttype'] == 'ibconversationwidgetmini'){
						//echo '<div class="large-12 columns end widgetheightview" id="chartgenid0">'.PHP_EOL;
					}else{
						echo '<div class="large-12 columns end widgetheightview" id="chartgenid'.$i.'">'.PHP_EOL;
					}
					if($chartinfo['charttype'] != 'ibconversationwidgetmini'){
					 echo '</div>'.PHP_EOL;
					}
					echo '</div>'.PHP_EOL;
					break;
			default :
				break;
		}
	}
	//chart js script generation
	public function dashboarddatascriptgen($chartinfo,$i,$rowid,$interval) {			
		echo "\t<script>".PHP_EOL;
		echo "\t\t$(document).ready(function(){".PHP_EOL;
		echo "\t\t\t";
		echo $this->chartgenerte($chartinfo,$i,$rowid,$interval);
		echo "\t\t});".PHP_EOL;
		echo "\t</script>".PHP_EOL;
	}
	//chart generate
	public function chartgenerte($chartdata,$id,$rowid,$interval) {
		//data set
		$tblname = $chartdata['ptablname'];
		$fieldname = $chartdata['chartlegend'];
		$jointblname = $chartdata['ctablname'];
		$joinfields = $chartdata['chartlegendtype'];
		$rowcount = $chartdata['rowcount'];
		$dashboardfieldid=$chartdata['chartid'];
		$moduleid=$chartdata['moduleid'];	
		$dashboardrowid=$rowid;
		$dateformat = $this->Basefunctions->appdateformatfetch();
		switch($chartdata['charttype']) {
			case 'piechart':
				$cdata = $this->chartdatagenerate($chartdata,0);
				$cdatas = json_decode($cdata,true);
				$chtdata = $this->chartcolumnsData($cdatas,'',2);
				//$data = 'var piechartarray = ["chartgenid'.$id.'",'.$chartdata['chartviewtype'].',"'.$chartdata['charttheme'].'",'.$chartdata['chartangle'].',"'.$chartdata['charttitle'].'",'.$legend.',"'.$chartdata['chartcolor'].'",'.$chtdata.'];';
				$data = 'var piechartarray = ["chartgenid'.$id.'",'.$chtdata.',"'.$chartdata['charttitle'].'","'.$chartdata['chartviewtype'].'","'.$chartdata['chartangle'].'","'.$chartdata['chartlegend'].'","'.$chartdata['chartlegendtype'].'"];';
				$data .= 'piechartfun(piechartarray);';
				return $data;
				break;
			case 'donutchart':
				$cdata = $this->chartdatagenerate($chartdata,0);
				$cdatas = json_decode($cdata,true);
				$chtdata = $this->chartcolumnsData($cdatas,'',2);
				$data = 'var donutchartarray = ["chartgenid'.$id.'",'.$chtdata.',"'.$chartdata['charttitle'].'","'.$chartdata['chartviewtype'].'","'.$chartdata['chartangle'].'","'.$chartdata['chartlegend'].'","'.$chartdata['chartlegendtype'].'"];';
				$data .= 'donutchartfun(donutchartarray);';
				return $data;
				break;
			case 'funnelchart':
				$cdata = $this->chartdatagenerate($chartdata,0);
				$cdatas = json_decode($cdata,true);
				$chtdata = $this->FunnelChartData($cdatas);
				$data = 'var funnelchartarray = ["chartgenid'.$id.'",'.$chtdata.',"'.$chartdata['charttitle'].'","'.$chartdata['charttheme'].'","'.$chartdata['chartlegend'].'","'.$chartdata['chartlegendtype'].'"];';
				$data .= 'funnelchartfun(funnelchartarray);';
				return $data;
				break;
			case 'pyramidchart':
				$cdata = $this->chartdatagenerate($chartdata,0);
				$cdatas = json_decode($cdata,true);
				$chtdata = $this->FunnelChartData($cdatas);
				$data = 'var pyramidchartarray = ["chartgenid'.$id.'",'.$chtdata.',"'.$chartdata['charttitle'].'","'.$chartdata['charttheme'].'","'.$chartdata['chartlegend'].'","'.$chartdata['chartlegendtype'].'"];';
				$data .= 'pyramidchartfun(pyramidchartarray);';
				return $data;
				break;
			case 'columnchart':
				//0-id 1-title 2-theme 3-depth3D  4-rotate 5-legend  6-data				
				$cdata = $this->chartdatagenerate($chartdata,0);
				$cdatas = json_decode($cdata,true);		
				$chtdata = $this->ColumnChartData($cdatas);
				$data = 'var columnchartfunarray = ["chartgenid'.$id.'",'.$chtdata.',"'.$chartdata['charttitle'].'","'.$chartdata['chartviewtype'].'","'.$chartdata['charttheme'].'","'.$chartdata['chartcolor'].'","'.$chartdata['chartlegend'].'","'.$chartdata['chartlegendtype'].'"];';
				$data .= 'columnchartfun(columnchartfunarray);';
				return $data;				
				break;			
			case 'wordcloudchart':
				$cdata = $this->chartdatagenerate($chartdata,0);
				$cdatas = json_decode($cdata,true);
				$chtdata = $this->WordcloudData($cdatas);
				$data = 'var wordcloudarray = ["chartgenid'.$id.'",'.$chtdata.',"'.$chartdata['charttitle'].'","'.$chartdata['chartcolor'].'"];';
				$data .= 'wordcloudfun(wordcloudarray);';
				return $data;
				break;
			case 'linechart':
				$cdata = $this->chartdatagenerate($chartdata,0);
				$cdatas = json_decode($cdata,true);
				$chtdata = $this->arealinechart($cdatas);
				$data = 'var linechartarray = ["chartgenid'.$id.'",'.$chtdata.',"'.$chartdata['charttitle'].'","'.$chartdata['chartviewtype'].'","'.$chartdata['charttheme'].'","'.$chartdata['chartcolor'].'"];';
				$data .= 'linechartfun(linechartarray);';
				return $data;
				break;
			case 'areachart':
				$cdata = $this->chartdatagenerate($chartdata,0);
				$cdatas = json_decode($cdata,true);
				$chtdata = $this->arealinechart($cdatas);
				$data = 'var areachartarray = ["chartgenid'.$id.'",'.$chtdata.',"'.$chartdata['charttitle'].'","'.$chartdata['chartviewtype'].'","'.$chartdata['charttheme'].'","'.$chartdata['chartcolor'].'"];';
				$data .= 'areachartfun(areachartarray);';
				return $data;
				break;			
			case 'stackedchart':			
				$cdata = $this->chartdatagenerate($chartdata,0);			
				$cdatas = json_decode($cdata,true);
				$chtdata = $this->stackchartcolumnsData($cdatas);
				$data = 'var stackchartfunarray = ["chartgenid'.$id.'",'.$chtdata.',"'.$chartdata['charttitle'].'","'.$chartdata['charttheme'].'","'.$chartdata['chartrotate'].'"];';
				$data .= 'stackchartfun(stackchartfunarray);';//print_r($data);
				return $data;	
				break;
			case 'bubblechart':
				//0-id 1-title 2-theme 3-depth3D  4-rotate 5-legend  6-data				
				$cdata = $this->chartdatagenerate($chartdata,0);
				$cdatas = json_decode($cdata,true);				
				$chtdata = $this->bubblechartcolumnsData($cdatas);		
				$data = 'var bubblechartarray = ["chartgenid'.$id.'",'.$chtdata.',"'.$chartdata['charttitle'].'","'.$chartdata['charttheme'].'","'.$chartdata['chartcolor'].'"];';
				$data .= 'bubblechartfun(bubblechartarray);';
				return $data;				
				break;
			case 'angulargaugechart':
				$cdata = $this->chartdatagenerate($chartdata,0);
				$cdatas = json_decode($cdata,true);
				$chtdata = $this->GaugeChartData($cdatas);
				$data = 'var angulargaugechartarray = ["chartgenid'.$id.'",'.$chtdata.',"'.$chartdata['charttitle'].'","'.$chartdata['chartlegend'].'","'.$chartdata['chartlegendtype'].'"];';
				$data .= 'angulargaguechartfun(angulargaugechartarray);';
				return $data;
				break;
			case 'cylinder3dchart':
				//0-id 1-title 2-theme 3-depth3D  4-rotate 5-legend  6-data				
				$cdata = $this->chartdatagenerate($chartdata,0);
				$cdatas = json_decode($cdata,true);	
				$chtdata = $this->columncylinderchartcolumnsData($cdatas);
				$data = 'var cylinder3dfunarray = ["chartgenid'.$id.'",'.$chtdata.',"'.$chartdata['charttitle'].'","'.$chartdata['chartlegend'].'","'.$chartdata['chartlegendtype'].'"];';
				$data .= 'cylinder3dchartfun(cylinder3dfunarray);';
				return $data;				
				break;
			case 'ibtaskwidget':
				$taskdata = $this->widgetgenerate($tblname,$fieldname,$jointblname,$joinfields,$rowcount,$chartdata,$dashboardrowid,$moduleid);
				$statusdata = $this->gettaskstatusdata();
				$assignempdata = $this->viewemployeegroupsdata();
				$tasktoolbar = $this->getwidgettoolbar($moduleid);
				$widgettitle = $chartdata['charttitle'];
				if($widgettitle != ''){
					$widgettitle = $this->Basefunctions->generalinformaion('module','menuname','moduleid',$moduleid);
				}
				$data = 'taskwidgetcreation("'.$widgettitle.'","chartgenid'.$id.'","taskwidgetclass'.$id.'",'.$taskdata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$tasktoolbar.",".$statusdata.",".$assignempdata.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("taskwidgetcreation","Task","chartgenid'.$id.'","taskwidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'",'.json_encode($chartdata).','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$tasktoolbar.');';
				$data.='});';
				return $data;
				break;
			case 'ibtaskwidgetmini':
				$taskdata = $this->widgetgenerate($tblname,$fieldname,$jointblname,$joinfields,$rowcount,$chartdata,$dashboardrowid,$moduleid);
				$statusdata = $this->gettaskstatusdata();
				$assignempdata = $this->viewemployeegroupsdata();
				$tasktoolbar = $this->getwidgettoolbar($moduleid);
				$widgettitle = $chartdata['charttitle'];
				if($widgettitle == ''){
					$widgettitle = $this->Basefunctions->generalinformaion('module','menuname','moduleid',$moduleid);
				}
				$data = 'taskwidgetcreationmini("'.$widgettitle.'","chartgenid'.$id.'","taskwidgetclass'.$id.'",'.$taskdata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$tasktoolbar.",".$statusdata.",".$assignempdata.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("taskwidgetcreationmini","'.$widgettitle.'","chartgenid'.$id.'","taskwidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'",'.json_encode($chartdata).','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$tasktoolbar.');';
				$data.='});';
				return $data;
				break;
			case 'ibdocumentswidget':
				$docdata = $this->widgetgenerate($tblname,$fieldname,$jointblname,$joinfields,$rowcount,$chartdata,$dashboardrowid,$moduleid);
				$doctoolbar = $this->getwidgettoolbar($moduleid);
				$widgettitle = $chartdata['charttitle'];
				if($widgettitle == ''){
					$widgettitle = $this->Basefunctions->generalinformaion('module','menuname','moduleid',$moduleid);
				}
				$data = 'documentswidgetcreation("'.$widgettitle.'","chartgenid'.$id.'","documentswidgetclass'.$id.'",'.$docdata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$doctoolbar.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("documentswidgetcreation","Documents","chartgenid'.$id.'","documentswidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'",'.json_encode($chartdata).','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$doctoolbar.');';
				$data.='});';
				return $data;
				break;
			case 'ibdocumentswidgetmini':
				$docdata = $this->widgetgenerate($tblname,$fieldname,$jointblname,$joinfields,$rowcount,$chartdata,$dashboardrowid,$moduleid);
				$doctoolbar = $this->getwidgettoolbar($moduleid);
				$widgettitle = $chartdata['charttitle'];
				if($widgettitle == ''){
					$widgettitle = $this->Basefunctions->generalinformaion('module','menuname','moduleid',$moduleid);
				}
				$data = 'documentswidgetcreationmini("'.$widgettitle.'","chartgenid'.$id.'","documentswidgetclass'.$id.'",'.$docdata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$doctoolbar.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("documentswidgetcreationmini","Documents","chartgenid'.$id.'","documentswidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'",'.json_encode($chartdata).','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$doctoolbar.');';
				$data.='});';
				return $data;
				break;
			case 'ibactivitieswidget':
				$activitydata = $this->widgetgenerate($tblname,$fieldname,$jointblname,$joinfields,$rowcount,$chartdata,$dashboardrowid,$moduleid);
				$activitytoolbar = $this->getwidgettoolbar($moduleid);
				$widgettitle = $chartdata['charttitle'];
				if($widgettitle == ''){
					$widgettitle = $this->Basefunctions->generalinformaion('module','menuname','moduleid',$moduleid);
				}
				$data = 'activitieswidgetcreation("'.$widgettitle.'","chartgenid'.$id.'","activitieswidgetclass'.$id.'",'.$activitydata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$activitytoolbar.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("activitieswidgetcreation","'.$widgettitle.'","chartgenid'.$id.'","activitieswidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'",'.json_encode($chartdata).','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$activitytoolbar.');';
				$data.='});';
				return $data;
				break;
			case 'ibcustomwidget':
			//customdatawidget($fielddata,$parenttable,$id,$moduleid)
				$customdata = $this->customwidgetgenerate($fieldname,$tblname,$rowid,$moduleid);
				$customeditfileds = $this->customeditfieldsdatafetch($fieldname,$tblname,$rowid,$moduleid);
				$acctoolbar = $this->getwidgettoolbar(204);
				$title=$chartdata['charttitle'];
				if($chartdata['charttitle'] == 'undefined'){
					$title='Widget';
				} 
				$data = 'customwidgetcreation("'.$chartdata['charttitle'].'","chartgenid'.$id.'","customwidgetclass'.$id.'",'.$customdata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.','.$customeditfileds.','.$acctoolbar.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';				
				$data.='});';
				return $data;
				break;	
			case 'ibmaillogwidget':
				//$condfield = $chartdata['condfield'];
				//$condvalue = $chartdata['condfieldval'];
				//$mailogdata = $this->widgetgeneratewithcond($tblname,$fieldname,$jointblname,$joinfields,$condfield,$condvalue,$rowcount);
				$mailogdata = $this->smsloggenerate($chartdata,$dashboardrowid,'Mail');
				//$data = 'mailnotificationwidgetcreation("Mail log","chartgenid'.$id.'","chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.');';
				$data = 'mailnotificationwidgetcreation("Mail Log","chartgenid'.$id.'","contactswidgetclass'.$id.'",'.$mailogdata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("mailnotificationwidgetcreation","Mail Log","chartgenid'.$id.'","","mailnotificationwidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'");';
				$data.='});';
				return $data;
				break;
			case 'ibsmslogwidget':
				//$condfield = $chartdata['condfield'];
				//$condvalue = $chartdata['condfieldval'];
				$smslogdata = $this->smsloggenerate($chartdata,$dashboardrowid,'SMS');
				//print_r($smslogdata);
				$data = 'smsnotificationwidgetcreation("SMS Log","chartgenid'.$id.'","smsnotificationwidgetclass'.$id.'",'.$smslogdata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.');';		
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("smsnotificationwidgetcreation","SMS Log","chartgenid'.$id.'","smsnotificationwidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'");';
				$data.='});';
				return $data;
				break;
			case 'ibreceivesmslogwidget':
				//$condfield = $chartdata['condfield'];
				//$condvalue = $chartdata['condfieldval'];
				$smslogdata = $this->receivesmsloggenerate($chartdata,$dashboardrowid,'SMS');
				//print_r($smslogdata);
				$data = 'receivesmsnotificationwidgetcreation("Receive SMS Log","chartgenid'.$id.'","receivesmsnotificationwidgetclass'.$id.'",'.$smslogdata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.');';		
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("receivesmsnotificationwidgetcreation","SMS Log","chartgenid'.$id.'","receivesmsnotificationwidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'");';
				$data.='});';
				return $data;
				break;
			case 'ibinboundcalllogwidget':
				$inbounddata = $this->inboundcallloggenerate($chartdata,$dashboardrowid,'Calls');
				$data = 'inboundcalllogwidgetcreation("Inbound Call Log","chartgenid'.$id.'","inboundcallwidgetclass'.$id.'",'.$inbounddata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("inboundcalllogwidgetcreation","Inbound Call Log","chartgenid'.$id.'","inboundcallwidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'",'.json_encode($chartdata).','.$dashboardrowid.');';
				$data.='});';
				return $data;
				break;
			case 'iboutboundcalllogwidget':
				$outbounddata = $this->inboundcallloggenerate($chartdata,$dashboardrowid,'Outbound');
				$data = 'outboundcalllogwidgetcreation("Outbound Call Log","chartgenid'.$id.'","outboundcallwidgetclass'.$id.'",'.$outbounddata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("outboundcalllogwidgetcreation","Outbound Call Log","chartgenid'.$id.'","outboundcallwidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'",'.json_encode($chartdata).','.$dashboardrowid.');';
				$data.='});';
				return $data;
				break;
			case 'ibopportunitywidget':
				$oppordata = $this->widgetgenerate($tblname,$fieldname,$jointblname,$joinfields,$rowcount,$chartdata,$dashboardrowid,$moduleid);
				$opptoolbar = $this->getwidgettoolbar($moduleid);
				$widgettitle = $chartdata['charttitle'];
				if($widgettitle == ''){
					$widgettitle = $this->Basefunctions->generalinformaion('module','menuname','moduleid',$moduleid);
				}
				$data = 'opportunitywidgetcreation("'.$widgettitle.'","chartgenid'.$id.'","opportunitywidgetclass'.$id.'",'.$oppordata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$opptoolbar.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("opportunitywidgetcreation","'.$widgettitle.'","chartgenid'.$id.'","opportunitywidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'",'.json_encode($chartdata).','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$opptoolbar.');';
				$data.='});';
				return $data;
				break;
			case 'ibcontactswidget':
				$contactdata = $this->widgetgenerate($tblname,$fieldname,$jointblname,$joinfields,$rowcount,$chartdata,$dashboardrowid,$moduleid);
				$conttoolbar = $this->getwidgettoolbar($moduleid);
				$widgettitle = $chartdata['charttitle'];
				if($widgettitle == ''){
					$widgettitle = $this->Basefunctions->generalinformaion('module','menuname','moduleid',$moduleid);
				}
				$data = 'contactswidgetcreation("'.$widgettitle.'","chartgenid'.$id.'","contactswidgetclass'.$id.'",'.$contactdata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$conttoolbar.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("contactswidgetcreation","'.$widgettitle.'","chartgenid'.$id.'","contactswidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'",'.json_encode($chartdata).','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$conttoolbar.');';
				$data.='});';
				return $data;
				break;
			case 'ibinvoicewidget':
				$invoicedata = $this->widgetgenerate($tblname,$fieldname,$jointblname,$joinfields,$rowcount,$chartdata,$dashboardrowid,$moduleid);
				$invoicetoolbar = $this->getwidgettoolbar($moduleid);
				$widgettitle = $chartdata['charttitle'];
				if($widgettitle == ''){
					$widgettitle = $this->Basefunctions->generalinformaion('module','menuname','moduleid',$moduleid);
				}
				$data = 'invoicewidgetcreation("'.$widgettitle.'","chartgenid'.$id.'","invoicewidgetclass'.$id.'",'.$invoicedata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$invoicetoolbar.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("invoicewidgetcreation","Invoices","chartgenid'.$id.'","invoicewidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'",'.json_encode($chartdata).','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$invoicetoolbar.');';
				$data.='});';
				return $data;
				break;
			case 'ibpaymentwidget':
				$paymentdata = $this->widgetgenerate($tblname,$fieldname,$jointblname,$joinfields,$rowcount,$chartdata,$dashboardrowid,$moduleid);
				$paymenttoolbar = $this->getwidgettoolbar($moduleid);
				$widgettitle = $chartdata['charttitle'];
				if($widgettitle == ''){
					$widgettitle = $this->Basefunctions->generalinformaion('module','menuname','moduleid',$moduleid);
				}
				$data = 'paymentwidgetcreation("'.$widgettitle.'","chartgenid'.$id.'","paymentwidgetclass'.$id.'",'.$paymentdata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$paymenttoolbar.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("paymentwidgetcreation","Payment","chartgenid'.$id.'","invoicewidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'",'.json_encode($chartdata).','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$paymenttoolbar.');';
				$data.='});';
				return $data;
				break;
			case 'ibquotewidget':
				$quotedata = $this->widgetgenerate($tblname,$fieldname,$jointblname,$joinfields,$rowcount,$chartdata,$dashboardrowid,$moduleid);
				$quotetoolbar = $this->getwidgettoolbar($moduleid);
				$widgettitle = $chartdata['charttitle'];
				if($widgettitle == ''){
					$widgettitle = $this->Basefunctions->generalinformaion('module','menuname','moduleid',$moduleid);
				}
				$data = 'quotewidgetcreation("'.$widgettitle.'","chartgenid'.$id.'","quotewidgetclass'.$id.'",'.$quotedata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$quotetoolbar.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("quotewidgetcreation","Quotes","chartgenid'.$id.'","quotewidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'",'.json_encode($chartdata).','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$quotetoolbar.');';
				$data.='});';
				return $data;
				break;			
			case 'ibaccountswidget':
				$accountdata = $this->widgetgenerate($tblname,$fieldname,$jointblname,$joinfields,$rowcount,$chartdata,$dashboardrowid,$moduleid);
				$accounttoolbar = $this->getwidgettoolbar($moduleid);
				$widgettitle = $chartdata['charttitle'];
				if($widgettitle == ''){
					$widgettitle = $this->Basefunctions->generalinformaion('module','menuname','moduleid',$moduleid);
				}
				$data = 'accountswidgetcreation("'.$widgettitle.'","chartgenid'.$id.'","accountswidgetclass'.$id.'",'.$accountdata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$accounttoolbar.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("accountswidgetcreation","'.$widgettitle.'","chartgenid'.$id.'","accountswidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'",'.json_encode($chartdata).','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$accounttoolbar.');';
				$data.='});';
				return $data;
				break;
			case 'ibticketswidget':
				$ticketdata = $this->widgetgenerate($tblname,$fieldname,$jointblname,$joinfields,$rowcount,$chartdata,$dashboardrowid,$moduleid);
				$tickettoolbar = $this->getwidgettoolbar($moduleid);
				$widgettitle = $chartdata['charttitle'];
				if($widgettitle == ''){
					$widgettitle = $this->Basefunctions->generalinformaion('module','menuname','moduleid',$moduleid);
				}
				$data = 'ticketswidgetcreation("'.$widgettitle.'","chartgenid'.$id.'","ticketwidgetclass'.$id.'",'.$ticketdata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$tickettoolbar.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("ticketswidgetcreation","Tickets","chartgenid'.$id.'","ticketwidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'",'.json_encode($chartdata).','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$tickettoolbar.');';
				$data.='});';
				return $data;
				break;
			case 'ibcampaignwidget':
				$campaigndata = $this->widgetgenerate($tblname,$fieldname,$jointblname,$joinfields,$rowcount,$chartdata,$dashboardrowid,$moduleid);
				$campaigntoolbar = $this->getwidgettoolbar($moduleid);
				$widgettitle = $chartdata['charttitle'];
				if($widgettitle == ''){
					$widgettitle = $this->Basefunctions->generalinformaion('module','menuname','moduleid',$moduleid);
				}
				$data = 'campaignwidgetcreation("'.$widgettitle.'","chartgenid'.$id.'","campaignwidgetclass'.$id.'",'.$campaigndata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$campaigntoolbar.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("campaignwidgetcreation","'.$widgettitle.'","chartgenid'.$id.'","campaignwidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'",'.json_encode($chartdata).','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$campaigntoolbar.');';
				$data.='});';
				return $data;
				break;
			case 'ibsalesorderwidget':
				$saleorddata = $this->widgetgenerate($tblname,$fieldname,$jointblname,$joinfields,$rowcount,$chartdata,$dashboardrowid,$moduleid);
				$sotoolbar = $this->getwidgettoolbar($moduleid);
				$widgettitle = $chartdata['charttitle'];
				if($widgettitle == ''){
					$widgettitle = $this->Basefunctions->generalinformaion('module','menuname','moduleid',$moduleid);
				}
				$data = 'salesordergetcreation("'.$widgettitle.'","chartgenid'.$id.'","salesorderwidgetclass'.$id.'",'.$saleorddata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$sotoolbar.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("salesordergetcreation","Sales Order","chartgenid'.$id.'","salesorderwidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'",'.json_encode($chartdata).','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$sotoolbar.');';
				$data.='});';
				return $data;
				break;
			case 'ibpurchaseorderwidget':
				$purchaseorddata = $this->widgetgenerate($tblname,$fieldname,$jointblname,$joinfields,$rowcount,$chartdata,$dashboardrowid,$moduleid);
				$potoolbar = $this->getwidgettoolbar($moduleid);
				$widgettitle = $chartdata['charttitle'];
				if($widgettitle == ''){
					$widgettitle = $this->Basefunctions->generalinformaion('module','menuname','moduleid',$moduleid);
				}
				$data = 'purchaseordergetcreation("'.$widgettitle.'","chartgenid'.$id.'","purchaseorderwidgetclass'.$id.'",'.$purchaseorddata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$potoolbar.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("purchaseordergetcreation","Purchase Order","chartgenid'.$id.'","purchaseorderwidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'",'.json_encode($chartdata).','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$potoolbar.');';
				$data.='});';
				return $data;
				break;
			case 'ibmaterialrequisitionwidget':
				$materialrequisitiondata = $this->widgetgenerate($tblname,$fieldname,$jointblname,$joinfields,$rowcount,$chartdata,$dashboardrowid,$moduleid);
				$mrtoolbar = $this->getwidgettoolbar($moduleid);
				$widgettitle = $chartdata['charttitle'];
				if($widgettitle == ''){
					$widgettitle = $this->Basefunctions->generalinformaion('module','menuname','moduleid',$moduleid);
				}
				$data = 'materialrequisitiongetcreation("'.$widgettitle.'","chartgenid'.$id.'","materialrequisitionwidgetclass'.$id.'",'.$materialrequisitiondata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$mrtoolbar.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("materialrequisitiongetcreation","Material Requisition","chartgenid'.$id.'","materialrequisitionwidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'",'.json_encode($chartdata).','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$mrtoolbar.');';
				$data.='});';
				return $data;
				break;
			case 'ibsolutionwidget':
				$solutiondata = $this->widgetgenerate($tblname,$fieldname,$jointblname,$joinfields,$rowcount,$chartdata,$dashboardrowid,$moduleid);
				$soltoolbar = $this->getwidgettoolbar($moduleid);
				$widgettitle = $chartdata['charttitle'];
				if($widgettitle == ''){
					$widgettitle = $this->Basefunctions->generalinformaion('module','menuname','moduleid',$moduleid);
				}
				$data = 'solutionwidgetcreation("'.$widgettitle.'","chartgenid'.$id.'","solutionwidgetclass'.$id.'",'.$solutiondata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$soltoolbar.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("solutionwidgetcreation","Solutions","chartgenid'.$id.'","solutionwidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'",'.json_encode($chartdata).','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$soltoolbar.');';
				$data.='});';
				return $data;
				break;
			case 'ibsolutioncatwidget':
				$solutioncatdata = $this->widgetgenerate($tblname,$fieldname,$jointblname,$joinfields,$rowcount,$chartdata,$dashboardrowid,$moduleid);
				$socatltoolbar = $this->getwidgettoolbar($moduleid);
				$widgettitle = $chartdata['charttitle'];
				if($widgettitle == ''){
					$widgettitle = $this->Basefunctions->generalinformaion('module','menuname','moduleid',$moduleid);
				}
				$data = 'solutioncategorywidgetcreation("'.$widgettitle.'","chartgenid'.$id.'","solutioncategorywidgetclass'.$id.'",'.$solutioncatdata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$socatltoolbar.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("solutioncategorywidgetcreation","Solutions Category","chartgenid'.$id.'","solutioncategorywidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'",'.json_encode($chartdata).','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$socatltoolbar.');';
				$data.='});';
				return $data;
				break;
			case 'ibcontractwidget':
				$contractdata = $this->widgetgenerate($tblname,$fieldname,$jointblname,$joinfields,$rowcount,$chartdata,$dashboardrowid,$moduleid);
				$contracttoolbar = $this->getwidgettoolbar($moduleid);
				$widgettitle = $chartdata['charttitle'];
				if($widgettitle == ''){
					$widgettitle = $this->Basefunctions->generalinformaion('module','menuname','moduleid',$moduleid);
				}
				$data = 'contractwidgetcreation("'.$widgettitle.'","chartgenid'.$id.'","contractwidgetclass'.$id.'",'.$contractdata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$contracttoolbar.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("contractwidgetcreation","Contracts","chartgenid'.$id.'","contractwidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'",'.json_encode($chartdata).','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$contracttoolbar.');';
				$data.='});';
				return $data;
				break;
			case 'ibleadwidget':
				$leaddata = $this->widgetgenerate($tblname,$fieldname,$jointblname,$joinfields,$rowcount,$chartdata,$dashboardrowid,$moduleid);
				$leadtoolbar = $this->getwidgettoolbar($moduleid);
				$widgettitle = $chartdata['charttitle'];
				if($widgettitle == ''){
					$widgettitle = $this->Basefunctions->generalinformaion('module','menuname','moduleid',$moduleid);
				}
				$data = 'leadwidgetcreation("'.$widgettitle.'","chartgenid'.$id.'","leadwidgetclass'.$id.'",'.$leaddata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$leadtoolbar.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("leadwidgetcreation","'.$widgettitle.'","chartgenid'.$id.'","leadwidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'",'.json_encode($chartdata).','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."',".$leadtoolbar.');';
				$data.='});';
				return $data;
				break;
			case 'ibproductwidget':
				$productdata = $this->widgetgenerate($tblname,$fieldname,$jointblname,$joinfields,$rowcount,$chartdata,$dashboardrowid,$moduleid);
				$widgettitle = $chartdata['charttitle'];
				if($widgettitle == ''){
					$widgettitle = $this->Basefunctions->generalinformaion('module','menuname','moduleid',$moduleid);
				}
				$data = 'productwidgetcreation("'.$widgettitle.'","chartgenid'.$id.'","productwidgetclass'.$id.'",'.$productdata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."'".');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("productwidgetcreation","Products","chartgenid'.$id.'","productwidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'",'.json_encode($chartdata).','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."'".');';
				$data.='});';
				return $data;
				break;
			 case 'ibpricebookwidget':
				$pricebookdata = $this->widgetgenerate($tblname,$fieldname,$jointblname,$joinfields,$rowcount,$chartdata,$dashboardrowid,$moduleid);
				$widgettitle = $chartdata['charttitle'];
				if($widgettitle == ''){
					$widgettitle = $this->Basefunctions->generalinformaion('module','menuname','moduleid',$moduleid);
				}
				$data = 'pricebookwidgetcreation("'.$widgettitle.'","chartgenid'.$id.'","pricebookwidgetclass'.$id.'",'.$pricebookdata.',"chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."'".');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';
				$data.='widegetrelodfun("pricebookwidgetcreation","PriceBook","chartgenid'.$id.'","pricebookwidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'",'.json_encode($chartdata).','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.",'".$dateformat."'".');';
				$data.='});';
				return $data;
				break;
			 case 'ibleadstatuswidget':
				$leaddata = $this->leadstatusgenerate();
				$data = 'leadstatuswidgetcreation("Lead Status","chartgenid'.$id.'","leadwidgetclass'.$id.'",'.$leaddata.');';
				//(widgettitle,appendid,appendclass,data)
				//$data.='$("#chartreloadid'.$id.'").click(function(){';
				//$data.='$("#chartgenid'.$id.'").empty();';
				//$data.='widegetrelodfun("leadwidgetcreation","Leads","chartgenid'.$id.'","leadwidgetclass'.$id.'","chartreloadid'.$id.'",'.$id.',"'.$tblname.'","'.$fieldname.'","'.$jointblname.'","'.$joinfields.'","'.$rowcount.'");';
				//$data.='});';
				return $data;
				break;
			 case 'ibtimeanddatewidget':
				$data = 'timeanddatewidget("chartgenid'.$id.'");';				
				return $data;
				break;
			 case 'ibcalendarwidget':
				$data = 'calendarwidget("chartgenid'.$id.'");';				
				return $data;
				break;		
			 case 'ibconversationwidget':
				$convdata = $this->conversationgenerate($chartdata,$dashboardrowid);
				$data = 'conversationwidgetcreation("Conversation","chartgenid'.$id.'","conversationwidgetclass",'.$convdata.');';		
				return $data;
				break;
			case 'ibconversationwidgetmini':
				$convdata = $this->conversationgenerate($chartdata,$dashboardrowid);
				$data = 'conversationwidgetcreationmini("Conversation","chartgenid0","noteswidget",'.$convdata.');';
				return $data;
				break;
			 case 'ibnotificationwidget':	
				$data = 'loadnotificationlog("chartgenid'.$id.'","all","0");';				
				return $data;
				break;
			case 'ibnotificationwidgetmini':
				$data = 'loadnotificationlog("chartgenid'.$id.'","all","1");';
				return $data;
				break;
			case 'ibsendsmslogwidget':				
				$data = 'sendsmslogwidgetcreation("Quick SMS","chartgenid'.$id.'","chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';				
				$data.='});';
				return $data;
				break;
			case 'ibclicktocallwidget':				
				$data = 'clicktocallwidgetcreation("Click To Call","chartgenid'.$id.'","chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.','.$moduleid.');';
				$data.='$("#chartreloadid'.$id.'").click(function(){';
				$data.='$("#chartgenid'.$id.'").empty();';				
				$data.='});';
				return $data;
				break;
			 default :
				break;
		}
	}	
	//chart data generate
	public function chartdatagenerate($chartdata,$mode) {
		$CI =& get_instance();
		if($mode == 0){		
			$bb= $CI->Basefunctions->dashboardchartdatainformationgenerte($chartdata);
			return $bb;
			//print_r($bb);die();
		}
		else {
			return $datainfo = $CI->Basefunctions->dashboarddatainformationgenerte($ptablname,$ctablname);
		}
	}
	//widget generate
	public function widgetgenerate($tablname,$fieldnames,$jointablnames,$joinfieldids,$rowcounts,$dashboarddata,$rowid,$moduleid) {
		$CI =& get_instance();
		$tablename = implode(',',explode('|',$tablname));
		$fieldname = implode(',',explode('|',$fieldnames));
		$jointablename = implode(',',explode('|',$jointablnames));
		$joinfieldid = implode(',',explode('|',$joinfieldids));
		return $datainfo = $CI->Basefunctions->basicinformationvalfetchmodel(''.$tablename.'',''.$fieldname.'',''.$jointablename.'',''.$joinfieldid.'',$rowcounts,$dashboarddata,$rowid,$moduleid);
	}
	//widget generate
	public function widgetgeneratewithcond($tablname,$fieldnames,$jointablnames,$joinfieldids,$condfield,$condvalue,$rowcounts) {
		$CI =& get_instance();
		$tablename = implode(',',explode('|',$tablname));
		$fieldname = implode(',',explode('|',$fieldnames));
		$jointablename = implode(',',explode('|',$jointablnames));
		$joinfieldid = implode(',',explode('|',$joinfieldids));
		$condfieldname = implode(',',explode('|',$condfield));
		$condfieldval = implode(',',explode('|',$condvalue));
		return $datainfo = $CI->Basefunctions->basicinformationcondvalfetchmodel(''.$tablename.'',''.$fieldname.'',''.$jointablename.'',''.$joinfieldid.'',''.$condfieldname.'',''.$condfieldval.'',$rowcounts);
	}
	//custom widget data fetch
	public function customwidgetgenerate($fieldname,$tblname,$rowid,$moduleid) {
		//print_r($fieldname);		
		return $datainfo = $this->homemodel->customdatawidget($fieldname,$tblname,$rowid,$moduleid);
	}
	//custom edit fields data fetch
	public function customeditfieldsdatafetch($fieldname,$tblname,$rowid,$moduleid) {
		return $datainfo = $this->homemodel->customeditfieldsdatafetchmodel($fieldname,$tblname,$rowid,$moduleid);
	}
	//lead status widget-temporary-groupcount based one
	public function leadstatusgenerate() {
		$CI =& get_instance();		
		return $datainfo = $CI->Basefunctions->leadstatusgeneratewidget();
	}
	//chart column data generate
	public function chartcolumnsData($data,$ccolorvalue,$size) {
		if(!isset($data['fail'])){
		$str = "[";
		if($size == '3') {
			for ($i = 0; $i < count($data); $i++) {
				//$temp=;
				$str .= "{'title':'".addslashes($data[$i]['captionname'])."', 'value':'".$data[$i]['counts']."', 'color':'".$ccolorvalue."'}";
				if ($i != ( count($data)-1) ) {
					$str .=",";
				}
			}
		} else {
			for ($i = 0; $i < count($data); $i++) {
				//$str .= "{'title':'".addslashes($data[$i]['captionname'])."', 'value':'".$data[$i]['counts']."'}";
				if($data[$i]['captionname'] != '')
				{
				$str .= "{ 'values':[".$data[$i]['counts']."] , 'text':'".addslashes($data[$i]['captionname'])."'}";
				if ($i != ( count($data)-1) ) {
					$str .=",";
				}
			  }
			}
		}
		$str .= "]"; 
		return $str;}
	}
	// column chart data generate function
	public function ColumnChartData($data) {
			$str = "[";
				for ($i = 0; $i < count($data); $i++) {
					if($data[$i]['captionname'] != ''){
					$str .= "{ 'values':[".$data[$i]['counts']."], 'text':'".addslashes($data[$i]['captionname'])."'}";
					if ($i != ( count($data)-1) ) {
						$str .=",";
					   }
					}
				}
			$str .= "]";
			return $str;
	}	
	
	// Gauge chart data generate function
	public function GaugeChartData($data) {
		$str = "[";
		for ($i = 0; $i < count($data); $i++) {
			if($data[$i]['captionname'] != ''){
			$str .= "{ 'values':[".$data[$i]['counts']."], 'text':'".addslashes($data[$i]['captionname'])."'}";
			if ($i != ( count($data)-1) ) {
				$str .=",";
				}
			}
		}
		$str .= "]";
		return $str;
	}
	// Area & Line chart data generate function
	public function arealinechart($data) {
		$str = "[";
		for ($i = 0; $i < count($data); $i++) {
			if($data[$i]['captionname'] != ''){
			$str .= "{ 'values':".$data[$i]['counts'].",'text':'".addslashes($data[$i]['captionname'])."'}";
			if ($i != ( count($data)-1) ) {
				$str .=",";
				}
			}
		}
		$str .= "]";
		return $str;
	}
	// Funnel Chart data generate function
	public function FunnelChartData($data) {
		$str = "[";
		for ($i = 0; $i < count($data); $i++) {
			if($data[$i]['captionname'] != ''){
			$str .= "{ 'values':[".$data[$i]['counts']."], 'text':'".addslashes($data[$i]['captionname'])."'}";
			if ($i != ( count($data)-1) ) {
				$str .=",";
				}
			}
		}
		$str .= "]";
		return $str;
	}
	//bubble chart data generate function
	public function bubblechartcolumnsData($dbdata) {
		$str = "[";
		$arrayone=array();
		for ($op = 0; $op < count($dbdata);$op++)
		{
			$str .= '{"values":[["'.$dbdata[$op]['groupone'].'","'.$dbdata[$op]['grouptwo'].'","'.$dbdata[$op]['counts'].'"]]}';
			if ($op != (count($dbdata)-1)) {
				$str .=",";
			}
		}
		$str .= "]";
		return $str;
		}
	//wordcloud chart data generate function
	public function WordcloudData($data) {
		$str = '';
		for ($i = 0; $i < count($data); $i++) {
			if($data[$i]['captionname'] != ""){
				//$replace = array("/","/\/",".","&","!","@","#","$","%","^","_",",");
				//$name = str_replace($replace, " ",$data[$i]['captionname']);
				$str .= $data[$i]['captionname'];
				if ($i != (count($data)-1)) {
					$str .=',';
				}
			}
		}		
		return $str;
	}
	//special function for stack chart generation
	public function stackchartcolumnsData($dbdata) {
		$str = "[";
		$arrayone=array();
		for ($op = 0; $op < count($dbdata);$op++)
		{
			$groupnameone[$op]=$dbdata[$op]['groupone'];
			$graphunique[$op]=$dbdata[$op]['grouptwo'];
		}
		$graphunique=array_values(array_unique($graphunique));
		$groupnameone=array_unique($groupnameone);
		///////
		for ($o = 0; $o < count($dbdata);$o++)
		{
			$key = array_search($dbdata[$o]['groupone'],$groupnameone);
			$arrayone[$groupnameone[$key]][]=array('grouptwo'=>$dbdata[$o]['grouptwo'],'count'=>$dbdata[$o]['counts']);
		}
		$uniquegroupone=array_values($groupnameone);
		//arraydatagenerate
		for ($i = 0; $i < count($uniquegroupone); $i++)
		{
			$str .= '{"values":[';
			$key = array_search($uniquegroupone[$i],$groupnameone);
			for ($p = 0;$p < count($arrayone[$groupnameone[$key]]);$p++)
			{
				$str .= $arrayone[$groupnameone[$key]][$p]['count'];
				if ($p != (count($arrayone[$groupnameone[$key]])-1)) {
				$str .=",";
				}
			}			
			$str .= '],"text":"'.$uniquegroupone[$i].'"}';
			if ($i != (count($uniquegroupone)-1)) {
				$str .=",";
			}
		}
		$str .= "]";
		
		return $str;
		//
	}
	//cylinder3d
	public function columncylinderchartcolumnsData($data){		
		$str = "[";
		for ($i = 0; $i < count($data); $i++) {		
			$str .= "{'values':[".$data[$i]['counts']."], 'text':'".addslashes($data[$i]['captionname'])."'";
			$color = '"background-color-2": "#F0F1F2",
		              "background-color": "#699EBF",
		              "tooltip": {
		              "text":"$%vM",
		              "font-size": 12,
		              "font-color": "#FFFFFF",
		              "background-color":"#699EBF",
		              "bold": true,
		              "font-family": "Helvetica",
		              "padding": 5';
			$str .= ",".$color."}}";
			if ($i != ( count($data)-1) ) {
				$str .=",";
			}
			}
		$str .= "]";
		return $str;
	}	
	//conversation data generation on widget load
	public function conversationgenerate($chartdata,$dashboardrowid)
	{
		$moduleid=$chartdata['mainmoduleid'];
		$rowid=$dashboardrowid;
		$lastnode=0;
		//$this->load->model('Home/homemodel');
		return $datainfo = $this->homemodel->getconversationdata($moduleid,$rowid,$lastnode,'');
	}
	//sms log load on widget load
	public function smsloggenerate($chartdata,$dashboardrowid,$modname)
	{
		$moduleid=$chartdata['moduleid'];
		$rowid=$dashboardrowid;
		$lastnode=0;		
		return $datainfo = $this->homemodel->getsmslogdata($moduleid,$rowid,$lastnode,$modname);
	}
	//sms log load on widget load
	public function receivesmsloggenerate($chartdata,$dashboardrowid,$modname)
	{
		$moduleid=$chartdata['moduleid'];
		$rowid=$dashboardrowid;
		$lastnode=0;		
		return $datainfo = $this->homemodel->receivegetsmslogdata($moduleid,$rowid,$lastnode,$modname);
	}
	//sms log load on widget load
	public function inboundcallloggenerate($chartdata,$dashboardrowid,$modname)
	{
		$moduleid=$chartdata['moduleid'];
		$rowid=$dashboardrowid;
		$lastnode=0;		
		return $datainfo = $this->homemodel->inboundcalllogdata($moduleid,$rowid,$lastnode,$modname);
	}
	/*
	* iterates through the loop gets widget toolbar based on the module
	*/
	public function getwidgettoolbar($moduleid)
	{
		$default_toolbar = array(2=>'hidedisplay',3=>'hidedisplay',4=>'hidedisplay',59=>'');		
		$toolbarid = array(2,3,4,59); //default fixed toolbar ids for widget
		$assignedid = '';
		$userroleid = $this->Basefunctions->userroleid;
		$data = $this->db->select('toolbarnameid')
							->from('usertoolbarprivilege')
							->where('moduleid',$moduleid)
							->where('userroleid',$userroleid)
							->where('status',$this->Basefunctions->activestatus)
							->limit(1)
							->get()
							->result();
		foreach($data as $info)
		{
			$assignedid = $info->toolbarnameid;
		}
		$arr_assignedid = explode(',',$assignedid);		
		for($i=0 ;$i < count($arr_assignedid);$i++)
		{
			if(isset($default_toolbar[$arr_assignedid[$i]]))
			{
				$default_toolbar[$arr_assignedid[$i]] = '';
			}
		}		
		return json_encode($default_toolbar);
	}
	//gewt status data
	public function gettaskstatusdata(){
		$data= array();
		//CASE WHEN c.size = 'S' THEN 1 ELSE 0 END
		$this->db->select('ROUND( +COALESCE(count(CASE WHEN crmtask.crmstatusid in (64) then crmstatusid end),0),0 )as notstrated,ROUND( +COALESCE(count(CASE WHEN crmtask.crmstatusid in (67) then crmstatusid end),0),0 )as completed');
		$this->db->from('crmtask');
		$this->db->where('crmtask.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = array('notstrated'=>$row->notstrated,'completed'=>$row->completed);
			}
		}
		return json_encode($data);
	}
	//employee group drop down val fetch
	public function viewemployeegroupsdata() {
		$datas = $this->Basefunctions->userspecificgroupdropdownvalfetch();
		if(count($datas)>0) {
			return json_encode($datas);
		} else {
			return json_encode(array('status'=>'fail'));
		}
	}
}