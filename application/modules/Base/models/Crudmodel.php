<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//To Perform Insert Update Read and Delete Operations
class Crudmodel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
	//primary key information fetch
	public function primaryinfo($table) {
		$primarydata = $this->db->query("SHOW KEYS FROM ".$table." WHERE Key_name = 'PRIMARY' ")->result();
		foreach($primarydata as $key) {
			$primarykey = $key->Column_name;
		}
		return $primarykey;
	}
	//filter values
	public function filtervalue($fields) {
		$fieldsinfo = array_unique( $fields );
		$fieldname = implode(',',$fieldsinfo);
		return $fieldname;
	}
	//table field data type check
	public function tablefieldtypecheck($tblname,$fieldname) {
		$fields = $this->db->field_data($tblname);
		foreach ($fields as $field) {
			$name =  $field->name;
			if($fieldname == $name) {
				return $field->type;
			}
		}
	}
	//value array generation
	public function formvalueget($formfieldscolmname,$formfieldsname) {
		//data array
		$data = array();
		$i=0;
		foreach($formfieldscolmname as $value) {
			if(ctype_alpha($_GET[$formfieldsname[$i]])) {
				$data[$value] = ucwords( $_GET[$formfieldsname[$i]] );
			} else {
				$data[$value] = $_GET[$formfieldsname[$i]];
			}
			$i++;
		}
		return $data;
	}
    //default value set array for add
	public function defaultvalueget() {
		$defdata['createdate'] = date($this->Basefunctions->datef);
		$defdata['lastupdatedate'] = date($this->Basefunctions->datef);
		$defdata['createuserid'] = $this->Basefunctions->userid;
		$defdata['lastupdateuserid'] = $this->Basefunctions->userid;
		$defdata['status'] = 1;
		return $defdata;
	}
	//next day default value set array for add
	public function nextdaydefaultvalueget() {
		$tomorrow = date("Y-m-d",strtotime("tomorrow"));
		$date = $this->Basefunctions->checkgivendateisholidayornot($tomorrow);
		$updatingtime = $this->Basefunctions->get_company_settings('updatingtime');
		$defdata['createdate'] = $date .' '.$updatingtime;
		$defdata['lastupdatedate'] = $date .' '.$updatingtime;;
		$defdata['createuserid'] = $this->Basefunctions->userid;
		$defdata['lastupdateuserid'] = $this->Basefunctions->userid;
		$defdata['status'] = 1;
		return $defdata;
	}
	//default value set array for delete
	public function deletedefaultvalueget() {
		$defdata['lastupdatedate'] = date($this->Basefunctions->datef);
		$defdata['lastupdateuserid'] = $this->Basefunctions->userid;
		$defdata['status'] = 0;
		return $defdata;
	}
	//default value set array for cancel
	public function canceldefaultvalueget() {
		$defdata['lastupdatedate'] = date($this->Basefunctions->datef);
		$defdata['lastupdateuserid'] = $this->Basefunctions->userid;
		$defdata['status'] = 5;
		return $defdata;
	}
	//nextday default value set array for delete
	public function nextdaydeletedefaultvalueget() {
		$tomorrow = date("Y-m-d",strtotime("tomorrow"));
		$date = $this->Basefunctions->checkgivendateisholidayornot($tomorrow);
		$updatingtime = $this->Basefunctions->get_company_settings('updatingtime');
		$defdata['lastupdatedate'] = $date .' '.$updatingtime;
		$defdata['lastupdateuserid'] = $this->Basefunctions->userid;
		$defdata['status'] = 0;
		return $defdata;
	}
	//default value set array for update
	public function updatedefaultvalueget() {
		$defdata['lastupdatedate'] = date($this->Basefunctions->datef);
		$defdata['lastupdateuserid'] = $this->Basefunctions->userid;
		return $defdata;
	}
	//nextday default value set array for update
	public function nextdayupdatedefaultvalueget() {
		$tomorrow = date("Y-m-d",strtotime("tomorrow"));
		$date = $this->Basefunctions->checkgivendateisholidayornot($tomorrow);
		$updatingtime = $this->Basefunctions->get_company_settings('updatingtime');
		$defdata['lastupdatedate'] = $date .' '.$updatingtime;
		$defdata['lastupdateuserid'] = $this->Basefunctions->userid;
		return $defdata;
	}
	//multiple table data insertion
	public function datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname) {
		$moduleid = $this->modulefieldmoduleidfetch($partablename);
		$m=0;
		$pdata = array();
		foreach( $tableinfo as $tblname ) {
			${'$cdata'.$m} = array();
			$i=0;
			foreach( $formfieldscolmname as $fcolvalue ) {
				if( strcmp( trim($partablename),trim($formfieldstable[$i]) ) == 0 ) { //ptable
					if( isset( $_POST[$formfieldsname[$i]] ) ) {
						$name = explode('_',$formfieldsname[$i]);
						if( !is_array( $_POST[$formfieldsname[$i]]) ) {
							if( ctype_alpha($_POST[$formfieldsname[$i]]) && $_POST[$formfieldsname[$i]] != "" ) {
								if( in_array('password',$name) ) {
									$hash = password_hash($_POST[$formfieldsname[$i]], PASSWORD_DEFAULT);
									$pdata[$fcolvalue] = $hash ;
								} else {
									$uitypeid = $this->modulefielduitypefetch($formfieldsname[$i],$moduleid);
									if($uitypeid=='8') {
										$date = $this->Basefunctions->ymddateconversion( trim($_POST[$formfieldsname[$i]]) );
										$pdata[$fcolvalue] = $date;
									} else {
										$pdata[$fcolvalue] = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
									}
								}
							} else if( $_POST[$formfieldsname[$i]] != "" ) {
								if( in_array('password',$name) ) {
									$hash = password_hash($_POST[$formfieldsname[$i]], PASSWORD_DEFAULT);
									$pdata[$fcolvalue] = $hash ;
								} else {
									$uitypeid = $this->modulefielduitypefetch($formfieldsname[$i],$moduleid);
									if($uitypeid=='8') {
										$date = $this->Basefunctions->ymddateconversion( trim($_POST[$formfieldsname[$i]]) );
										$pdata[$fcolvalue] = $date;
									} else {
										$pdata[$fcolvalue] = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
									}
								}
							}
						} else {
							$pdata[$fcolvalue] = implode(',',$_POST[$formfieldsname[$i]]);
						}
					}
				} else if( strcmp( trim($formfieldstable[$i]),trim($tblname) ) == 0 ) { //ctable
					if( isset( $_POST[$formfieldsname[$i]] ) ) {
						$name = explode('_',$formfieldsname[$i]);
						if( !is_array($_POST[$formfieldsname[$i]]) ) {
							if( ctype_alpha($_POST[$formfieldsname[$i]]) && $_POST[$formfieldsname[$i]] != "" ) {
								if( in_array('password',$name) ) {
									$hash = password_hash($_POST[$formfieldsname[$i]], PASSWORD_DEFAULT);
									${'$cdata'.$m}[$fcolvalue] = $hash ;
								} else {
									$uitypeid = $this->modulefielduitypefetch($formfieldsname[$i],$moduleid);
									if($uitypeid=='8') {
										$date = $this->Basefunctions->ymddateconversion( trim($_POST[$formfieldsname[$i]]) );
										${'$cdata'.$m}[$fcolvalue] = $date;
									} else {
										${'$cdata'.$m}[$fcolvalue] = trim(ucwords($_POST[$formfieldsname[$i]]));
									}
								}
							} else if( $_POST[$formfieldsname[$i]] != "" ) {
								if( in_array('password',$name) ) {
									$hash = password_hash($_POST[$formfieldsname[$i]], PASSWORD_DEFAULT);
									${'$cdata'.$m}[$fcolvalue] = $hash ;
								} else {
									$uitypeid = $this->modulefielduitypefetch($formfieldsname[$i],$moduleid);
									if($uitypeid=='8') {
										$date = $this->Basefunctions->ymddateconversion( trim($_POST[$formfieldsname[$i]]) );
										${'$cdata'.$m}[$fcolvalue] = $date;
									} else {
										${'$cdata'.$m}[$fcolvalue] = trim(ucwords($_POST[$formfieldsname[$i]]));
									}
								}
							}
						} else {
							${'$cdata'.$m}[$fcolvalue] = implode(',',$_POST[$formfieldsname[$i]]);
						}
					}
				}
				$i++;
			}
			$m++;
		}
		//primary key
		$primaryname = $this->primaryinfo($partablename);
		{//parent table insertion
			//default value get
			$defdataarr = $this->defaultvalueget();
			//$newdata = array_merge($pdata,$defdataarr);
			$industryarray = array('industryid'=>$this->Basefunctions->industryid);
			$newinddata = array_merge($pdata,$defdataarr);
			$newdata = array_merge($newinddata,$industryarray);
			//data insertion
			$this->db->insert( $partablename, array_filter($newdata) );
			$primaryid = $this->db->insert_id();
		}
		{//child table insertion
			$m=0;
			foreach( $tableinfo as $tblname ) {
				$defdataarr = $this->defaultvalueget();
				$cnewdata = array();
				if(count(${'$cdata'.$m}) > 0 ) {
					if( strcmp( trim($partablename),trim($tblname) ) != 0 ) {
						//default value get
						${'$cdata'.$m}[$primaryname]=$primaryid;
						$cnewdata = array_merge(${'$cdata'.$m},$defdataarr);
						//data insertion
						$this->db->insert( $tblname, array_filter($cnewdata) );
					}
				}
				$m++;
			}
		}
		return $primaryid;
	}
	//multiple table data insertion with table restriction
	public function datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable) {
		$moduleid = $this->modulefieldmoduleidfetch($partablename);
		//generate value array
		$m=0;
		$pdata = array();
		foreach( $tableinfo as $tblname ) {
			${'$cdata'.$m} = array();
			$i=0;
			if( !in_array(trim($tblname),$restricttable) ) {
				foreach( $formfieldscolmname as $fcolvalue ) {
					if( strcmp( trim($partablename),trim($formfieldstable[$i]) ) == 0 ) { //ptable
						if( isset( $_POST[$formfieldsname[$i]] ) ) {
							$name = explode('_',$formfieldsname[$i]);
							if( !is_array($_POST[$formfieldsname[$i]]) ) {
								if( ctype_alpha($_POST[$formfieldsname[$i]]) && $_POST[$formfieldsname[$i]] != "" ) {
									$txtstring1="";
									if( in_array('password',$name) ) {
										$hash = password_hash($_POST[$formfieldsname[$i]], PASSWORD_DEFAULT);
										$pdata[$fcolvalue] = $hash ;
									} else if( in_array('editorfilename',$name) ) {
										$txtstring1 = trim($_POST[$formfieldsname[$i]]);
										$pdata[$fcolvalue] = trim($this->generatefile($txtstring1));
									} else {
										$uitypeid = $this->modulefielduitypefetch($formfieldsname[$i],$moduleid);
										if($uitypeid=='8') {
											$date = $this->Basefunctions->ymddateconversion( trim($_POST[$formfieldsname[$i]]) );
											$pdata[$fcolvalue] = $date;
										} else {
											$pdata[$fcolvalue] = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
										}
									}
								} else if( $_POST[$formfieldsname[$i]] != "" ) {
									$txtstring2="";
									if( in_array('password',$name) ) {
										$hash = password_hash($_POST[$formfieldsname[$i]], PASSWORD_DEFAULT);
										$pdata[$fcolvalue] = $hash ;
									} else if( in_array('editorfilename',$name) ) {
										$txtstring2 = trim($_POST[$formfieldsname[$i]]);
										$pdata[$fcolvalue] = trim($this->generatefile($txtstring2));
									} else {
										$uitypeid = $this->modulefielduitypefetch($formfieldsname[$i],$moduleid);
										if($uitypeid=='8') {
											$date = $this->Basefunctions->ymddateconversion( trim($_POST[$formfieldsname[$i]]) );
											$pdata[$fcolvalue] = $date;
										} else {
											$pdata[$fcolvalue] = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
										}
									}
								}
							} else {
								$pdata[$fcolvalue] = implode(',',$_POST[$formfieldsname[$i]]);
							}
						}
					} else if( strcmp( trim($formfieldstable[$i]),trim($tblname) ) == 0 && strcmp( trim($partablename),trim($formfieldstable[$i]) ) != 0 ) { //ctable
						if( isset( $_POST[$formfieldsname[$i]] ) ) {
							$name = explode('_',$formfieldsname[$i]);
							if( !is_array($_POST[$formfieldsname[$i]]) ) {
								if( ctype_alpha($_POST[$formfieldsname[$i]]) && $_POST[$formfieldsname[$i]] != "" ) {
									$txtstring3="";
									if( in_array('password',$name) ) {
										$hash = password_hash($_POST[$formfieldsname[$i]], PASSWORD_DEFAULT);
										${'$cdata'.$m}[$fcolvalue] = $hash ;
									} else if( in_array('editorfilename',$name) ) {
										$txtstring3 = trim($_POST[$formfieldsname[$i]]);
										${'$cdata'.$m}[$fcolvalue] = trim($this->generatefile($txtstring3));
									} else {
										$uitypeid = $this->modulefielduitypefetch($formfieldsname[$i],$moduleid);
										if($uitypeid=='8') {
											$date = $this->Basefunctions->ymddateconversion( trim($_POST[$formfieldsname[$i]]) );
											${'$cdata'.$m}[$fcolvalue] = $date;
										} else {
											${'$cdata'.$m}[$fcolvalue] = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
										}
									}
								} else if( $_POST[$formfieldsname[$i]] != "" ) {
									$txtstring4="";
									if( in_array('password',$name) ) {
										$hash = password_hash($_POST[$formfieldsname[$i]], PASSWORD_DEFAULT);
										${'$cdata'.$m}[$fcolvalue] = $hash ;
									} else if( in_array('editorfilename',$name) ) {
										$txtstring4 = trim($_POST[$formfieldsname[$i]]);
										${'$cdata'.$m}[$fcolvalue] = trim($this->generatefile($txtstring4));
									} else {
										$uitypeid = $this->modulefielduitypefetch($formfieldsname[$i],$moduleid);
										if($uitypeid=='8') {
											$date = $this->Basefunctions->ymddateconversion( trim($_POST[$formfieldsname[$i]]) );
											${'$cdata'.$m}[$fcolvalue] = $date;
										} else {
											${'$cdata'.$m}[$fcolvalue] = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
										}
									}
								}
							} else {
								${'$cdata'.$m}[$fcolvalue] = implode(',',$_POST[$formfieldsname[$i]]);
							}
						}
					}
					$i++;
				}
			}
			$m++;
		}
		//primary key
		$primaryname = $this->primaryinfo($partablename);
		{//parent table insertion
			//default value get
			$defdataarr = $this->defaultvalueget();
			$industryarray = array('industryid'=>$this->Basefunctions->industryid);
			$newinddata = array_merge($pdata,$defdataarr);
			$newdata = array_merge($newinddata,$industryarray);
			//data insertion
			$this->db->insert( $partablename, array_filter($newdata) );
			$primaryid = $this->db->insert_id();
		}
		{//child table insertion
			$m=0;
			foreach( $tableinfo as $tblname ) {
				$defdataarr = $this->defaultvalueget();
				$cnewdata = array();
				if(count(${'$cdata'.$m}) > 0 ) {
					if( strcmp( trim($partablename),trim($tblname) ) != 0 ) {
						//if( !in_array(trim($tblname),$restricttable) ) {
							//default value get
							${'$cdata'.$m}[$primaryname]=$primaryid;
							$cnewdata = array_merge(${'$cdata'.$m},$defdataarr);
							//data insertion
							$this->db->insert( $tblname, array_filter($cnewdata) );
						//}
					}
				}
				$m++;
			}
		}
		return $primaryid;
	}
	//fetch module id form module
	public function modulefieldmoduleidfetch($parentable) {
		$modid = 1;
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('moduleid,modulemastertable',false);
		$this->db->from('module');
		$this->db->where('modulemastertable',$parentable);
		$this->db->where("FIND_IN_SET('$industryid',module.industryid) >", 0);
		$datas = $this->db->get()->result();
		foreach($datas as $data) {
			$modid = $data->moduleid;
		}
		return $modid;
	}
	//check ui type id
	public function modulefielduitypefetch($fieldname,$moduleid) {
		$uitype = '1';
		$this->db->select('modulefieldid,uitypeid',false);
		$this->db->from('modulefield');
		$this->db->join('moduleinfo','moduleinfo.moduleid=modulefield.moduletabid'); 
		$this->db->where('fieldname',$fieldname);
		$this->db->where_in('moduletabid',$moduleid);
		$this->db->where('moduleinfo.status',1);
		$datas = $this->db->get()->result();
		foreach($datas as $data) {
			$uitype = $data->uitypeid;
		}
		return $uitype;
	}
	//address insert
	public function addressdatainsert($arrname,$partablename,$primaryid) {
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$arrvalue=array('4','5');
		$primarysecondary=array('2','3');
		$table=$partablename.'address';
		$pretables=array('leadaddress','contactaddress','employeeaddress','accountaddress');
		for($i=0;$i < count($arrname);$i++) {
			if(!in_array($table,$pretables)) {
				$sourceid = ((isset($_POST[$arrname[$i].'addresstype']))? $_POST[$arrname[$i].'addresstype'] : '1' );
				$addr = ((isset($_POST[$arrname[$i].'address']))? $_POST[$arrname[$i].'address'] : '' );
				$addrpcode = ((isset($_POST[$arrname[$i].'pincode']))? $_POST[$arrname[$i].'pincode'] : '' );
				$addrcity = ((isset($_POST[$arrname[$i].'city']))? $_POST[$arrname[$i].'city'] : '' );
				$addrstate = ((isset($_POST[$arrname[$i].'state']))? $_POST[$arrname[$i].'state'] : '' );
				$attrcountry = ((isset($_POST[$arrname[$i].'country']))? $_POST[$arrname[$i].'country'] : '' );
				$address = array(
								$primaryname=>$primaryid,
								'addresstypeid'=>$arrvalue[$i],
								'addresssourceid'=>$sourceid,
								'addressmethod'=>$arrvalue[$i],
								'address'=>$addr,
								'pincode'=>$addrpcode,
								'city'=>$addrcity,
								'state'=>$addrstate,
								'country'=>$attrcountry
							);
				$defdataarr = $this->defaultvalueget();
				$newdata = array_merge($address,$defdataarr);
				$finaladd=array_filter($newdata);			
				$this->db->insert($table,$finaladd);
			} else {
				//special case-detectd lead/contact
				if($table == 'leadaddress' or $table == 'contactaddress' or $table =='employeeaddress') {
					$taddresstypeid=$primarysecondary[$i];
				} else {
					$taddresstypeid=$arrvalue[$i];
				}
				$addr = ((isset($_POST[$arrname[$i].'address']))? $_POST[$arrname[$i].'address'] : '' );
				$addrpcode = ((isset($_POST[$arrname[$i].'pincode']))? $_POST[$arrname[$i].'pincode'] : '' );
				$addrcity = ((isset($_POST[$arrname[$i].'city']))? $_POST[$arrname[$i].'city'] : '' );
				$addrstate = ((isset($_POST[$arrname[$i].'state']))? $_POST[$arrname[$i].'state'] : '' );
				$attrcountry = ((isset($_POST[$arrname[$i].'country']))? $_POST[$arrname[$i].'country'] : '' );
				$address = array(
								$primaryname=>$primaryid,
								'addresstypeid'=>$taddresstypeid,
								'addressmethod'=>$arrvalue[$i],
								'address'=>$addr,
								'pincode'=>$addrpcode,
								'city'=>$addrcity,
								'state'=>$addrstate,
								'country'=>$attrcountry
							);
				$defdataarr = $this->defaultvalueget();
				$newdata = array_merge($address,$defdataarr);
				$finaladd=array_filter($newdata);			
				$this->db->insert($table,$finaladd);
			}			
		}
	}
	//address update
	public function addressdataupdate($arrname,$partablename,$primaryid) {
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$arrvalue=array('4','5');
		$table=$partablename.'address';
		$pretables=array('leadaddress','contactaddress','employeeaddress','accountaddress');
		for($i=0;$i < count($arrname);$i++) {
			if(!in_array($table,$pretables)) {
				if(!isset($_POST[$arrname[$i].'addresstype'])) {
					$_POST[$arrname[$i].'addresstype'] = 1;
				} else {
					if($_POST[$arrname[$i].'addresstype'] == '') {
						$_POST[$arrname[$i].'addresstype'] = 1;
					}
				}
				$sourceid = ((isset($_POST[$arrname[$i].'addresstype']))? $_POST[$arrname[$i].'addresstype'] : '1' );
				$addr = ((isset($_POST[$arrname[$i].'address']))? $_POST[$arrname[$i].'address'] : '' );
				$addrpcode = ((isset($_POST[$arrname[$i].'pincode']))? $_POST[$arrname[$i].'pincode'] : '' );
				$addrcity = ((isset($_POST[$arrname[$i].'city']))? $_POST[$arrname[$i].'city'] : '' );
				$addrstate = ((isset($_POST[$arrname[$i].'state']))? $_POST[$arrname[$i].'state'] : '' );
				$attrcountry = ((isset($_POST[$arrname[$i].'country']))? $_POST[$arrname[$i].'country'] : '' );
				$address = array(
							'addresssourceid'=>$sourceid,
							'address'=>$addr,
							'pincode'=>$addrpcode,
							'city'=>$addrcity,
							'state'=>$addrstate,
							'country'=>$attrcountry
						);
				$defdataarr = $this->updatedefaultvalueget();
				$newdata = array_merge($address,$defdataarr);
				$finaladd=$newdata;
				$table=$partablename.'address';
				$wherearray=array('addressmethod'=>$arrvalue[$i],$primaryname=>$primaryid);
				$check = $this->addresswhereconditioncheck($table,$wherearray,$primaryname,$primaryid);
				if($check == 'TRUE'){
					$this->db->where($wherearray);
					$this->db->update($table,$finaladd);
				} else {
					$newdefdataarr =$this->defaultvalueget();
					$nnewdata = array_merge($address,$newdefdataarr);
					$this->db->insert($table,$nnewdata);
				}
			} else {
				$addr = ((isset($_POST[$arrname[$i].'address']))? $_POST[$arrname[$i].'address'] : '' );
				$addrpcode = ((isset($_POST[$arrname[$i].'pincode']))? $_POST[$arrname[$i].'pincode'] : '' );
				$addrcity = ((isset($_POST[$arrname[$i].'city']))? $_POST[$arrname[$i].'city'] : '' );
				$addrstate = ((isset($_POST[$arrname[$i].'state']))? $_POST[$arrname[$i].'state'] : '' );
				$attrcountry = ((isset($_POST[$arrname[$i].'country']))? $_POST[$arrname[$i].'country'] : '' );
				$address = array(
							'addressmethod'=>$arrvalue[$i],
							 $primaryname=>$primaryid,
							'address'=>$addr,
							'pincode'=>$addrpcode,
							'city'=>$addrcity,
							'state'=>$addrstate,
							'country'=>$attrcountry
						);
				$defdataarr = $this->updatedefaultvalueget();
				$newdata = array_merge($address,$defdataarr);
				$finaladd=$newdata;
				$table=$partablename.'address';
				$wherearray=array('addressmethod'=>$arrvalue[$i],$primaryname=>$primaryid);
				$check = $this->addresswhereconditioncheck($table,$wherearray,$primaryname,$primaryid);
				if($check == 'TRUE'){
					$this->db->where($wherearray);
					$this->db->update($table,$finaladd);
				} else {
					$newdefdataarr =$this->defaultvalueget();
					$nnewdata = array_merge($address,$newdefdataarr);
					$this->db->insert($table,$nnewdata);
				}
			}
		}		
	}
	//address table where condition check
	public function addresswhereconditioncheck($table,$wherearray,$primaryname,$primaryid) {
		$this->db->select($primaryname);
		$this->db->from($table);
		$this->db->where($wherearray);
		$this->db->where($table.'.status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			return 'TRUE';
		} else {
			return 'FALSE';	
		}
	}
	//grid data insert
	public function griddatainsert($primaryname,$gridpartablename,$girddatainfo,$gridrows,$primaryid) {
		$moduleid = $this->modulefieldmoduleidfetch($gridpartablename);
		$m=0;
		$h=1;
		$colcount = 0;
		$defdataarr = $this->defaultvalueget();
		foreach($gridrows as $rowcount) {
			$colcount += $rowcount;
			$gridfieldsname = explode(',',$_POST['gridcolnames'.$h]);
			//filter grid unique fields table
			$gridfieldtabname = explode(',',$_POST['griddatatabnameinfo'.$h]);
			$gridfieldstable = $this->filtervalue($gridfieldtabname);
			$gridtableinfo = explode(',',$gridfieldstable);
			for($i=($colcount-$rowcount);$i<=($colcount-1);$i++) {
				foreach( $gridtableinfo as $gdtblname ) {
					${'$gcdata'.$m} = array();
					$t=0;
					foreach($gridfieldsname AS $gdfldsname) {
						if(strcmp( trim($gridfieldtabname[$t]),trim($gdtblname) ) == 0 ) {
							if( isset( $girddatainfo[$i][$gdfldsname] ) ) {
								$name = explode('_',$gdfldsname);
								if( !is_array($girddatainfo[$i][$gdfldsname]) ) {
									if( ctype_alpha($girddatainfo[$i][$gdfldsname]) && $girddatainfo[$i][$gdfldsname] != '' ) {
										if( in_array('password',$name) ) {
											$hash = password_hash($girddatainfo[$i][$gdfldsname], PASSWORD_DEFAULT);
											${'$gcdata'.$m}[$gdfldsname] = $hash ;
										} else {
											$uitypeid = $this->modulefielduitypefetch($gdfldsname,$moduleid);
											if($uitypeid=='8') {
												$date = $this->Basefunctions->ymddateconversion( trim($girddatainfo[$i][$gdfldsname]) );
												${'$gcdata'.$m}[$gdfldsname] = $date;
											} else {
												${'$cdata'.$m}[$gdfldsname] = ctype_alpha($girddatainfo[$i][$gdfldsname])?trim( ucwords($girddatainfo[$i][$gdfldsname]) ):trim( $girddatainfo[$i][$gdfldsname] );
											}
										}
									} else if( $girddatainfo[$i][$gdfldsname] != "" ) {
										if( in_array('password',$name) ) {
											$hash = password_hash($girddatainfo[$i][$gdfldsname], PASSWORD_DEFAULT);
											${'$gcdata'.$m}[$gdfldsname] = $hash ;
										} else {
											$uitypeid = $this->modulefielduitypefetch($gdfldsname,$moduleid);
											if($uitypeid=='8') {
												$date = $this->Basefunctions->ymddateconversion( trim($girddatainfo[$i][$gdfldsname]) );
												${'$gcdata'.$m}[$gdfldsname] = $date;
											} else {
												${'$gcdata'.$m}[$gdfldsname] = ctype_alpha($girddatainfo[$i][$gdfldsname])?trim( ucwords($girddatainfo[$i][$gdfldsname]) ):trim( $girddatainfo[$i][$gdfldsname] );
											}
										}
									}
								} else {
									${'$gcdata'.$m}[$gdfldsname] = implode(',',$girddatainfo[$i][$gdfldsname]);
								}
							}
						}
					}
					if(count(${'$gcdata'.$m})>0) {
						${'$gcdata'.$m}[$primaryname]=$primaryid;
						$gnewdata = array_merge(${'$gcdata'.$m},$defdataarr);
						//data insertion
						$this->db->insert( $gdtblname, array_filter($gnewdata) );
					}
					$m++;
				}
			}
			$h++;
		}
	}
	//multiple table data update
	public function dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid) {
		$moduleid = $this->modulefieldmoduleidfetch($partablename);
		//generate value array
		$m=0;
		$pdata = array();
		foreach( $tableinfo as $tblname ) {
			${'$cdata'.$m} = array();
			$i=0;
			foreach( $formfieldscolmname as $fcolvalue ) {
				if( strcmp( trim($partablename),trim($formfieldstable[$i]) ) == 0 ) { //ptable
					if( isset( $_POST[$formfieldsname[$i]] ) ) {
						$name = explode('_',$formfieldsname[$i]);
						if( !is_array($_POST[$formfieldsname[$i]]) ) {
							if( ctype_alpha($_POST[$formfieldsname[$i]]) ) {
								if( in_array('password',$name) ) {
									$hash = password_hash($_POST[$formfieldsname[$i]], PASSWORD_DEFAULT);
									$pdata[$fcolvalue] = $hash ;
								} else {							
									$uitypeid = $this->modulefielduitypefetch($formfieldsname[$i],$moduleid);
									if($uitypeid=='8') {
										$date = $this->Basefunctions->ymddateconversion( trim($_POST[$formfieldsname[$i]]) );
										$pdata[$fcolvalue] = $date;
									} else if($uitypeid=='17' || $uitypeid=='18' || $uitypeid=='19' || $uitypeid=='20' || $uitypeid=='23' || $uitypeid=='25' || $uitypeid=='26' || $uitypeid=='27' || $uitypeid=='28' || $uitypeid=='29'){
										$ddval = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
										if($ddval != ''){
											$pdata[$fcolvalue] =$ddval;
										} else {
											$pdata[$fcolvalue] =1;
										}
									}  else {
										$pdata[$fcolvalue] = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
									}
								}
							} else {
								if( in_array('password',$name) ) {
									$hash = password_hash($_POST[$formfieldsname[$i]], PASSWORD_DEFAULT);
									$pdata[$fcolvalue] = $hash ;
								} else {
									//checks for empty value replace with 0-zero for int/decimal types
									$fieldvalue=$_POST[$formfieldsname[$i]];
									if($fieldvalue != '') {
										$pdata[$fcolvalue] = $_POST[$formfieldsname[$i]];
									} else {
										$ftype = $this->tablefieldtypecheck(trim($formfieldstable[$i]),$fcolvalue);
										if($ftype == 'int' || $ftype == 'decimal') {
											$pdata[$fcolvalue] = 0;
										} else {
											$uitypeid = $this->modulefielduitypefetch($formfieldsname[$i],$moduleid);
											if($uitypeid=='8') {
												$date = $this->Basefunctions->ymddateconversion( trim($_POST[$formfieldsname[$i]]) );
												$pdata[$fcolvalue] = $date;
											} else if($uitypeid=='17' || $uitypeid=='18' || $uitypeid=='19' || $uitypeid=='20' || $uitypeid=='23' || $uitypeid=='25' || $uitypeid=='26' || $uitypeid=='27' || $uitypeid=='28' || $uitypeid=='29'){
												$ddval = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
												if($ddval != ''){
													$pdata[$fcolvalue] =$ddval;
												} else {
													$pdata[$fcolvalue] =1;
												}
											}  else {
												$pdata[$fcolvalue] = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
											}
										}
									}							
								}
							}
						} else {
							$pdata[$fcolvalue] = implode(',',$_POST[$formfieldsname[$i]]);
						}
					}
				} else if( strcmp( trim($formfieldstable[$i]),trim($tblname) ) == 0 ) { //ctable
					if( isset( $_POST[$formfieldsname[$i]] ) ) {
						$name = explode('_',$formfieldsname[$i]);
						if( !is_array($_POST[$formfieldsname[$i]]) ) {
							if( ctype_alpha($_POST[$formfieldsname[$i]]) ) {
								if( in_array('password',$name) ) {
									$hash = password_hash($_POST[$formfieldsname[$i]], PASSWORD_DEFAULT);
									${'$cdata'.$m}[$fcolvalue] = $hash ;
								} else {
									$uitypeid = $this->modulefielduitypefetch($formfieldsname[$i],$moduleid);
									if($uitypeid=='8') {
										$date = $this->Basefunctions->ymddateconversion( trim($_POST[$formfieldsname[$i]]) );
										${'$cdata'.$m}[$fcolvalue] = $date;
									} else if($uitypeid=='17' || $uitypeid=='18' || $uitypeid=='19' || $uitypeid=='20' || $uitypeid=='23' || $uitypeid=='25' || $uitypeid=='26' || $uitypeid=='27' || $uitypeid=='28' || $uitypeid=='29'){
										$ddval = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
										if($ddval != ''){
											${'$cdata'.$m}[$fcolvalue] =$ddval;
										} else {
											${'$cdata'.$m}[$fcolvalue] =1;
										}
									} else {
										${'$cdata'.$m}[$fcolvalue] = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
									}
								}
							} else {
								if( in_array('password',$name) ) {
									$hash = password_hash($_POST[$formfieldsname[$i]], PASSWORD_DEFAULT);
									${'$cdata'.$m}[$fcolvalue] = $hash ;
								} else {								
									//checks for empty value replace with 0-zero for int/decimal types
									$fieldvalue=$_POST[$formfieldsname[$i]];
									if($fieldvalue != '') {
										${'$cdata'.$m}[$fcolvalue] = $_POST[$formfieldsname[$i]];
									} else {
										$ftype = $this->tablefieldtypecheck(trim($formfieldstable[$i]),$fcolvalue);
										if($ftype == 'int' || $ftype == 'decimal') {
											${'$cdata'.$m}[$fcolvalue] = 0;
										} else {
											$uitypeid = $this->modulefielduitypefetch($formfieldsname[$i],$moduleid);
											if($uitypeid=='8') {
												$date = $this->Basefunctions->ymddateconversion( trim($_POST[$formfieldsname[$i]]) );
												${'$cdata'.$m}[$fcolvalue] = $date;
											} else if($uitypeid=='17' || $uitypeid=='18' || $uitypeid=='19' || $uitypeid=='20' || $uitypeid=='23' || $uitypeid=='25' || $uitypeid=='26' || $uitypeid=='27' || $uitypeid=='28' || $uitypeid=='29'){
												$ddval = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
												if($ddval != ''){
													${'$cdata'.$m}[$fcolvalue] =$ddval;
												} else {
													${'$cdata'.$m}[$fcolvalue] =1;
												}
											} else {
												${'$cdata'.$m}[$fcolvalue] = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
											}
										}
									}							
								}
							}
						} else {
							${'$cdata'.$m}[$fcolvalue] = implode(',',$_POST[$formfieldsname[$i]]);
						}
					}
				}
				$i++;
			}
			$m++;
		}
		//primary key
		$primaryname = $this->primaryinfo($partablename);
		{//parent table update
			$defdataarr = $this->updatedefaultvalueget();
			$newdata = array_merge($pdata,$defdataarr);
			//update information
			$this->db->where($primaryname,$primaryid);
			$this->db->update($partablename,$newdata);
		}
		{//child table update
			$m=0;
			foreach( $tableinfo as $tblname ) {
				$defdataarr = $this->defaultvalueget();
				$cnewdata = array();
				if(count(${'$cdata'.$m}) > 0 ) {
					if( strcmp( trim($partablename),trim($tblname) ) != 0 ) {
						//update default value
						$chk = $this->filedvaluecheck($tblname,$primaryname,$primaryname,$primaryid);
						if($chk!=0) {
							$defdataarr = $this->updatedefaultvalueget();
							$cnewdata = array_merge(${'$cdata'.$m},$defdataarr);
							//update information
							$this->db->where($primaryname,$primaryid);
							$this->db->update($tblname,$cnewdata);
						} else {
							${'$cdata'.$m}[$primaryname] = $primaryid;
							$defdataarr = $this->defaultvalueget();
							$cnewdata = array_merge(${'$cdata'.$m},$defdataarr);
							//add information
							$this->db->insert($tblname,array_filter($cnewdata));
						}
					}
				}
				$m++;
			}
		}
		return 'TRUE';
	}
	//drop down value check
	public function filedvaluecheck($tablename,$filedname,$filedid,$filedvalue) {
		$dataset = 0;
		$result=$this->db->query(' select '.$filedid.' from '.$tablename.' WHERE '.$filedname.'="'.$filedvalue.'" ')->result();
		foreach($result as $data) {
			$dataset=$data->$filedid;
		}
		return $dataset;
	}
	//data update with restrict
	public function dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable) {
		$moduleid = $this->modulefieldmoduleidfetch($partablename);
		$m=0;
		$pdata = array();
		foreach( $tableinfo as $tblname ) {
			${'$cdata'.$m} = array();
			$i=0;
			if( !in_array(trim($tblname),$restricttable) ) {
				foreach( $formfieldscolmname as $fcolvalue ) {
					if( strcmp( trim($partablename),trim($formfieldstable[$i]) ) == 0 ) { //ptable
						if( isset( $_POST[$formfieldsname[$i]] ) ) {
							$name = explode('_',$formfieldsname[$i]);
							if( !is_array($_POST[$formfieldsname[$i]]) ) {
								if( ctype_alpha($_POST[$formfieldsname[$i]]) ) {
									$txtstring1 = "";
									if( in_array('password',$name) ) {
										$hash = password_hash($_POST[$formfieldsname[$i]], PASSWORD_DEFAULT);
										$pdata[$fcolvalue] = $hash ;
									} else if( in_array('editorfilename',$name) ) {
										$txtstring1 = trim($_POST[$formfieldsname[$i]]);
										$pdata[$fcolvalue] = $this->generatefile($txtstring1);
									}  else {
										$uitypeid = $this->modulefielduitypefetch($formfieldsname[$i],$moduleid);
										//echo $uitypeid.'---'.$formfieldsname[$i];
										if($uitypeid=='8') {
											$date = $this->Basefunctions->ymddateconversion( trim($_POST[$formfieldsname[$i]]) );
											$pdata[$fcolvalue] = $date;
										} else if($uitypeid=='17' || $uitypeid=='18' || $uitypeid=='19' || $uitypeid=='20' || $uitypeid=='23' || $uitypeid=='25' || $uitypeid=='26' || $uitypeid=='27' || $uitypeid=='28' || $uitypeid=='29'){
											$ddval = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
											if($ddval != ''){
												$pdata[$fcolvalue] =$ddval;
											} else {
												$pdata[$fcolvalue] =1;
											}
										} else if($uitypeid=='1'){
											$ddval = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
											if($ddval != ''){
												$pdata[$fcolvalue] =$ddval;
											} else {
												$pdata[$fcolvalue] = 1;
											}
										} else {
											$pdata[$fcolvalue] = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
										}
									}
								} else {
									$txtstring2 = "";
									if( in_array('password',$name) ) {
										$hash = password_hash($_POST[$formfieldsname[$i]], PASSWORD_DEFAULT);
										$pdata[$fcolvalue] = $hash ;
									} else if( in_array('editorfilename',$name) ) {
										$txtstring2 = trim($_POST[$formfieldsname[$i]]);
										$pdata[$fcolvalue] = $this->generatefile($txtstring2);
									} else {
										$uitypeid = $this->modulefielduitypefetch($formfieldsname[$i],$moduleid);
										//echo $uitypeid.'-#-'.$formfieldsname[$i];
										if($uitypeid=='8') {
											$date = $this->Basefunctions->ymddateconversion( trim($_POST[$formfieldsname[$i]]) );
											$pdata[$fcolvalue] = $date;
										} else if($uitypeid=='17' || $uitypeid=='18'|| $uitypeid=='19' || $uitypeid=='20' || $uitypeid=='23' || $uitypeid=='25' || $uitypeid=='26' || $uitypeid=='27' || $uitypeid=='28' || $uitypeid=='29'){
											$ddval = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
											if($ddval != ''){
												$pdata[$fcolvalue] =$ddval;
											} else {
												$pdata[$fcolvalue] =1;
											}
										} else if($uitypeid=='1'){
											$ddval = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
											if($ddval != ''){
												$pdata[$fcolvalue] =$ddval;
											} else {
												$pdata[$fcolvalue] = 1;
											}
										} else {
											$pdata[$fcolvalue] = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
										}
									}
								}
							} else {
								$pdata[$fcolvalue] = implode(',',$_POST[$formfieldsname[$i]]);
							}
						}
					} else if( strcmp( trim($formfieldstable[$i]),trim($tblname) ) == 0 && strcmp( trim($partablename),trim($formfieldstable[$i]) ) != 0 ) { //ctable
						if( isset( $_POST[$formfieldsname[$i]] ) ) {
							$name = explode('_',$formfieldsname[$i]);
							if( !is_array($_POST[$formfieldsname[$i]]) ) {
								if( ctype_alpha($_POST[$formfieldsname[$i]]) ) {
									$txtstring3 = "";
									if( in_array('password',$name) ) {
										$hash = password_hash($_POST[$formfieldsname[$i]], PASSWORD_DEFAULT);
										${'$cdata'.$m}[$fcolvalue] = $hash ;
									} else if( in_array('editorfilename',$name) ) {
										$txtstring3 = trim($_POST[$formfieldsname[$i]]);
										${'$cdata'.$m}[$fcolvalue] = $this->generatefile($txtstring3);
									} else {
										$uitypeid = $this->modulefielduitypefetch($formfieldsname[$i],$moduleid);
										//echo $uitypeid.'-@-'.$formfieldsname[$i];
										if($uitypeid=='8') {
											$date = $this->Basefunctions->ymddateconversion( trim($_POST[$formfieldsname[$i]]) );
											${'$cdata'.$m}[$fcolvalue] = $date;
										} else if($uitypeid=='17' || $uitypeid=='18' || $uitypeid=='19' || $uitypeid=='20' || $uitypeid=='23' || $uitypeid=='25' || $uitypeid=='26' || $uitypeid=='27' || $uitypeid=='28' || $uitypeid=='29'){
											$ddval = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
											if($ddval != ''){
												${'$cdata'.$m}[$fcolvalue] =$ddval;
											} else {
												${'$cdata'.$m}[$fcolvalue] =1;
											}
										} else if($uitypeid=='1'){
											$ddval = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
											if($ddval != ''){
												${'$cdata'.$m}[$fcolvalue] =$ddval;
											} else {
												${'$cdata'.$m}[$fcolvalue] =1;
											}
										} else {
											${'$cdata'.$m}[$fcolvalue] = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
										}
									}
								} else {
									$txtstring4 = "";
									if( in_array('password',$name) ) {
										$hash = password_hash($_POST[$formfieldsname[$i]], PASSWORD_DEFAULT);
										${'$cdata'.$m}[$fcolvalue] = $hash ;
									} else if( in_array('editorfilename',$name) ) {
										$txtstring4 = trim($_POST[$formfieldsname[$i]]);
										${'$cdata'.$m}[$fcolvalue] = $this->generatefile($txtstring4);
									} else {
										$uitypeid = $this->modulefielduitypefetch($formfieldsname[$i],$moduleid);
										//echo $uitypeid.'-*-'.$formfieldsname[$i];
										if($uitypeid=='8') {
											$date = $this->Basefunctions->ymddateconversion( trim($_POST[$formfieldsname[$i]]) );
											${'$cdata'.$m}[$fcolvalue] = $date;
										} else if($uitypeid=='17' || $uitypeid=='18' || $uitypeid=='19' || $uitypeid=='20' || $uitypeid=='23' || $uitypeid=='25' || $uitypeid=='26' || $uitypeid=='27' || $uitypeid=='28' || $uitypeid=='29'){
											$ddval = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
											if($ddval != ''){
												${'$cdata'.$m}[$fcolvalue] =$ddval;
											} else {
												${'$cdata'.$m}[$fcolvalue] =1;
											}
										} else if($uitypeid=='1'){
											$ddval = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
											if($ddval != ''){
												${'$cdata'.$m}[$fcolvalue] =$ddval;
											} else {
												${'$cdata'.$m}[$fcolvalue] =1;
											}
										} else {
											${'$cdata'.$m}[$fcolvalue] = ctype_alpha($_POST[$formfieldsname[$i]])?trim( ucwords($_POST[$formfieldsname[$i]]) ):trim( $_POST[$formfieldsname[$i]] );
										}
									}
								}
							} else {
								${'$cdata'.$m}[$fcolvalue] = implode(',',$_POST[$formfieldsname[$i]]);
							}
						}
					}
					$i++;
				}
			}
			$m++;
		}
		$primaryname = $this->primaryinfo($partablename);
		{
			$defdataarr = $this->updatedefaultvalueget();
			$newdata = array_merge($pdata,$defdataarr);
			$this->db->where($primaryname,$primaryid);
			$this->db->update($partablename,$newdata);
		}
		{
			$m=0;
			foreach( $tableinfo as $tblname ) {
				$cnewdata = array();
				if(count(${'$cdata'.$m}) > 0 ) {
					if( strcmp( trim($partablename),trim($tblname) ) != 0 ) {
						//if( !in_array(trim($tblname),$restricttable) ) {
							$chk = $this->filedvaluecheck($tblname,$primaryname,$primaryname,$primaryid);
							if($chk!=0) {
								$defdataarr = $this->updatedefaultvalueget();
								$cnewdata = array_merge(${'$cdata'.$m},$defdataarr);
								//update information
								$this->db->where($primaryname,$primaryid);
								$this->db->update($tblname,$cnewdata);
							} else {
								${'$cdata'.$m}[$primaryname] = $primaryid;
								$defdataarr = $this->defaultvalueget();
								$cnewdata = array_merge(${'$cdata'.$m},$defdataarr);
								//add information
								$this->db->insert($tblname,array_filter($cnewdata));
							}
						//}
					}
				}
				$m++;
			}
		}
		return 'TRUE';
	}
	//information fetch
	public function dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid) {
		$loguserid = $this->Basefunctions->userid;
		$userid = $this->Basefunctions->userid;
		//select field infromation
		$formfieldscolmname[] = $primaryname;
		$formfieldstable[] = $partablename;
		$newdata = array();
		$i=0;
		foreach($formfieldscolmname as $data) {
			$newdata[] = $formfieldstable[$i].'.'.$data;
			$i++;
		}
		//final selected data
		$selectdata = implode(',',$newdata);
		//text box information
		$formfieldsname[] = 'primarydataid';
		
		//join child table with table
		$joinq = "";
		$fildstable = explode(',',$tablename );
		foreach($fildstable as $tabname) {
			if(strcmp($partablename,$tabname) != 0) {
				$joinq=$joinq.' LEFT OUTER JOIN '.$tabname.' ON '.$tabname.'.'.$primaryname.'='.$partablename.'.'.$primaryname;
			}
		}
		//where conditions
		$wh = '1=1'; 
		if( $primaryid!="" && $primaryname ) {
			$wh = $partablename.'.'.$primaryname.'='.$primaryid;
		}
		$status = $partablename.'.status NOT IN (0)';
		//fetch createuserid and convert as role,group
		$cuserid='';
		$cruserdata = $this->db->query('select createuserid from '.$partablename.' WHERE '.$wh.' AND '.$status);
		foreach($cruserdata->result() as $row) {
			$cuserid = $row->createuserid;
		}
		if($cuserid == $loguserid) { //check current user as record owner
			//fetch data
			$data = $this->db->query('select '.$selectdata.' from '.$partablename.' '.$joinq.' WHERE '.$wh.' AND '.$status);
			if($data->num_rows() >0) {
				foreach($data->result()as $datarow) {
					$i=0;
					foreach($formfieldsname as $row) {
						$uitypeid = $this->modulefielduitypefetch($row,$moduleid);
						$dataname = $formfieldscolmname[$i];
						if($uitypeid=='8') {
							$date = $this->Basefunctions->userdateformatconvert($datarow->$dataname);
							$resdata[$row]=$date;
						} else {
							$resdata[$row]=trim($datarow->$dataname);
						}
						$i++;
					}
				}
				return json_encode($resdata);
			} else {
			   return json_encode(array("fail"=>'Denied'));
			}
		} else if($cuserid!='') { //check other user have permission
			//fetch role and group
			$cusruledata = array();
			$recuserroleid = $this->Basefunctions->fetchrecorduserroleid($cuserid);
			$groupids = $this->Basefunctions->fetchallempgrpids();
			if($recuserroleid != 1) {
				$cusruledata = $this->Basefunctions->customrulefetchbaseduserid($moduleid,$recuserroleid,$groupids);
			}
			//data share rule
			$rulecond = '';
			$custrulecond = '';
			$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
			if($ruleid == 1 || $ruleid == 0) {
				$roleid = $this->Basefunctions->userroleid;
				$assignemp = $this->Basefunctions->checkassigntofield($partablename,'employeetypeid');
				/* $empids = $this->Basefunctions->roleididfetchfromrolesandsubroles($roleid);
				$empid = implode(',',$empids); */
				$empids = $this->Basefunctions->subrolesempidfetch($roleid);
				$subempid = implode(',',$empids);
				$empid = ( ($subempid!="")? $subempid.','.$userid : $userid);
				if($assignemp == 'true') {//assign field exist
					//$rulecond = ( ($empid != '')? ' AND '.$partablename.'.createuserid IN ('.$empid.') ' : ' AND 1=1' );
					//custom rule
					if(sizeof($cusruledata) > 0) {
						$custrulecond = " AND ( ".$partablename.".createuserid IN (".$empid.") OR ( ";
						foreach($cusruledata as $key => $customrule) {
							if($customrule['accesstype'] != '1') {
								if($customrule['totypeid'] == '2') { //group
									$custrulecond .= "(".$partablename.".employeetypeid=".$customrule['totypeid']." AND ".$partablename.".employeeid=".$customrule['sharedto'].")";
									$custrulecond .= ' OR ';
								} else if( ($customrule['totypeid'] == '3' && $customrule['superiorallow'] == 'Yes') ) { //roles with superior
									$uroleids = array();
									$uroleids = $this->Basefunctions->parentuserroleid($customrule['sharedto']);
									array_push($uroleids,$customrule['sharedto']);
									$paruserrole = implode(',',$uroleids);
									$custrulecond .= "(".$partablename.".employeetypeid=".$customrule['totypeid']." AND ".$partablename.".employeeid IN (".$paruserrole.") )";
									$custrulecond .= ' OR ';
								} else if( ($customrule['totypeid'] == '3' && $customrule['sharedto'] == $roleid && $customrule['superiorallow'] == 'No') ) { //roles without superior
									$custrulecond .= "(".$partablename.".employeetypeid=".$customrule['totypeid']." AND ".$partablename.".employeeid=".$customrule['sharedto'].")";
									$custrulecond .= ' OR ';
								} else if( $customrule['totypeid'] == '4' ) { //role and subordinate
									$roleids = $this->Basefunctions->roleididfetchfromrolesandsubroles($customrule['sharedto']);
									$roleid = implode(',',$roleids);
									$custrulecond .= "(".$partablename.".employeetypeid=".$customrule['totypeid']." AND ".$partablename.".employeeid IN (".$roleid.") )";
									$custrulecond .= ' OR ';
								}
							}
						}
						$custrulecond .= "(".$partablename.".employeetypeid=1 AND ".$partablename.".employeeid=".$this->Basefunctions->userid.")";
						$custrulecond .= " ) )";
					} else {
						//$rulecond = ( ($empid != '')? ' AND ('.$partablename.'.createuserid IN ('.$empid.') OR '.$partablename.'.employeetypeid=1 AND '.$partablename.'.employeeid='.$this->Basefunctions->userid.')' : ' AND 1=1' );
						$roleids = $this->Basefunctions->roleididfetchfromrolesandsubroles($roleid);
						$usrroleids = implode(',',$roleids);
						$usrroleids = ( ($usrroleids!='')? $usrroleids:1);
						$usrgrpid =  $this->Basefunctions->groupididfetchuserid($userid);
						$usergroupids = implode(',',$usrgrpid);
						$usergroupids = ( ($usergroupids!='')? $usergroupids:1);
						$rulecond = ( ($empid != '')? ' AND ( '.$partablename.'.createuserid IN ('.$empid.') OR ('.$partablename.'.employeetypeid=1 AND '.$partablename.'.employeeid='.$this->Basefunctions->userid.') OR ('.$partablename.'.employeetypeid=3 AND '.$partablename.'.employeeid='.$roleid.') OR ('.$partablename.'.employeetypeid=4 AND '.$partablename.'.employeeid IN ('.$usrroleids.')) OR ('.$partablename.'.employeetypeid=2 AND '.$partablename.'.employeeid IN ('.$usergroupids.')) )' : ' AND 1=1' );
					}
				} else {	//assign field not exits.
					$empids = $this->Basefunctions->empidfetchfromrolesandsubroles($roleid);
					$subempid = implode(',',$empids);
					$empid = ( ($subempid!="")? $subempid.','.$userid : $userid);
					$rulecond = ( ($empid != '')? ' AND '.$partablename.'.createuserid IN ('.$empid.')' : ' AND 1=1' );
				}
			}
			//fetch data
			$data = $this->db->query('select '.$selectdata.' from '.$partablename.' '.$joinq.' WHERE '.$wh.$rulecond.' AND '.$status);
			if($data->num_rows() >0) {
				foreach($data->result()as $datarow) {
					$i=0;
					foreach($formfieldsname as $row) {
						$uitypeid = $this->modulefielduitypefetch($row,$moduleid);
						$dataname = $formfieldscolmname[$i];
						if($uitypeid=='8') {
							$date = $this->Basefunctions->userdateformatconvert($datarow->$dataname);
							$resdata[$row]=$date;
						} else {
							$resdata[$row]=trim($datarow->$dataname);
						}
						$i++;
					}
				}
				return json_encode($resdata);
			} else {
			   return json_encode(array("fail"=>'Denied'));
			}
		} else {
			return json_encode(array("fail"=>'Denied'));
		}
	}
	//information fetch with restrict
	public function dbdatafetchwithrestrict($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$restricttable,$moduleid) {
		//select field infromation
		$formfieldscolmname[] = $primaryname;
		$formfieldstable[] = $partablename;
		//text box information
		$formfieldsname[] = 'primarydataid';
		$loguserid = $this->Basefunctions->userid;
		$userid = $this->Basefunctions->userid;
		$newdata = array();
		$i=0;
		foreach($formfieldscolmname as $data) {
			if( !in_array(trim($formfieldstable[$i]),$restricttable) ) {
				$newdata[] = $formfieldstable[$i].'.'.$data;
			}
			$i++;
		}
		//final selected data
		$selectdata = implode(',',$newdata);
		//join child table with table
		$joinq = "";
		$fildstable = explode(',',$tablename);
		foreach($fildstable as $tabname) {
			if( !in_array(trim($tabname),$restricttable) ) {
				if(strcmp($partablename,$tabname) != 0) {
					$joinq=$joinq.' LEFT OUTER JOIN '.$tabname.' ON '.$tabname.'.'.$primaryname.'='.$partablename.'.'.$primaryname;
				}
			}
		}
		//where conditions
		$wh = '1=1';
		if( $primaryid!="" && $primaryname ) {
			$wh = $partablename.'.'.$primaryname.'='.$primaryid;
		}
		$status = $partablename.'.status = 1';
		//fetch createuserid and convert as role,group
		$cuserid='';
		$cruserdata = $this->db->query('select createuserid from '.$partablename.' WHERE '.$wh.' AND '.$status);
		foreach($cruserdata->result() as $row) {
			$cuserid = $row->createuserid;
		}
		if($cuserid == $loguserid) { //check current user as record owner
			//fetch data
			$data = $this->db->query('select '.$selectdata.' from '.$partablename.' '.$joinq.' WHERE '.$wh.' AND '.$status);
			if($data->num_rows() >0) {
				foreach($data->result()as $datarow) {
					$i=0;
					foreach($formfieldscolmname as $row) { //formfieldsname
						if( !in_array(trim($formfieldstable[$i]),$restricttable) ) {
							$uitypeid = $this->modulefielduitypefetch($formfieldsname[$i],$moduleid);
							if($uitypeid=='8') {
								$date = $this->Basefunctions->userdateformatconvert($datarow->$row);
								$resdata[$formfieldsname[$i]]=$date;
							} else {
								$resdata[$formfieldsname[$i]]=trim($datarow->$row);
							}
						}
						$i++;
					}
				}
				return json_encode($resdata);
			} else {
			   return json_encode(array("fail"=>'Denied'));
			}
		}  else if($loguserid ==2) {
			$data = $this->db->query('select '.$selectdata.' from '.$partablename.' '.$joinq.' WHERE '.$wh.' AND '.$status);
			if($data->num_rows() >0) {
				foreach($data->result()as $datarow) {
					$i=0;
					foreach($formfieldscolmname as $row) { //formfieldsname
						if( !in_array(trim($formfieldstable[$i]),$restricttable) ) {
							$uitypeid = $this->modulefielduitypefetch($formfieldsname[$i],$moduleid);
							if($uitypeid=='8') {
								$date = $this->Basefunctions->userdateformatconvert($datarow->$row);
								$resdata[$formfieldsname[$i]]=$date;
							} else {
								$resdata[$formfieldsname[$i]]=trim($datarow->$row);
							}
						}
						$i++;
					}
				}
				return json_encode($resdata);
			} else {
				return json_encode(array("fail"=>'Denied'));
			}
		} else if($cuserid!='') { //check other user have permission
			//fetch role and group
			$cusruledata = array();
			$recuserroleid = $this->Basefunctions->fetchrecorduserroleid($cuserid);
			$groupids = $this->Basefunctions->fetchallempgrpids();
			if($recuserroleid != 1) {
				$cusruledata = $this->Basefunctions->customrulefetchbaseduserid($moduleid,$recuserroleid,$groupids);
			}
			//print_r($cusruledata);
			//data share rule
			$rulecond = '';
			$custrulecond = '';
			$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
			//echo $ruleid;
			if($ruleid == 1 || $ruleid == 0) {
				$roleid = $this->Basefunctions->userroleid;
				$assignemp = $this->Basefunctions->checkassigntofield($partablename,'employeetypeid');
				/* $empids = $this->Basefunctions->roleididfetchfromrolesandsubroles($roleid);
				$empid = implode(',',$empids); */
				$empids = $this->Basefunctions->subrolesempidfetch($roleid);
				$subempid = implode(',',$empids);
				$empid = ( ($subempid!="")? $subempid.','.$userid : $userid);
				if($assignemp == 'true') {
					//custom rule
					if( sizeof($cusruledata) > 0 ) {
						$custrulecond = " AND ( ".$partablename.".createuserid IN (".$empid.") OR ( ";
						foreach($cusruledata as $key => $customrule) {
							if($customrule['accesstype'] != '1') {
								if($customrule['totypeid'] == '2') { //group
									$custrulecond .= "(".$partablename.".employeetypeid=".$customrule['totypeid']." AND ".$partablename.".employeeid=".$customrule['sharedto'].")";
									$custrulecond .= ' OR ';
								} else if( ($customrule['totypeid'] == '3' && $customrule['superiorallow'] == 'Yes') ) { //roles with superior
									$uroleids = array();
									$uroleids = $this->Basefunctions->parentuserroleid($customrule['sharedto']);
									array_push($uroleids,$customrule['sharedto']);
									$paruserrole = implode(',',$uroleids);
									$custrulecond .= "(".$partablename.".employeetypeid=".$customrule['totypeid']." AND ".$partablename.".employeeid IN (".$paruserrole.") )";
									$custrulecond .= ' OR ';
								} else if( ($customrule['totypeid'] == '3' && $customrule['sharedto'] == $roleid && $customrule['superiorallow'] == 'No') ) { //roles without superior
									$custrulecond .= "(".$partablename.".employeetypeid=".$customrule['totypeid']." AND ".$partablename.".employeeid=".$customrule['sharedto'].")";
									$custrulecond .= ' OR ';
								} else if( $customrule['totypeid'] == '4' ) { //role and subordinate
									$roleids = $this->Basefunctions->roleididfetchfromrolesandsubroles($customrule['sharedto']);
									$roleid = implode(',',$roleids);
									$custrulecond .= "(".$partablename.".employeetypeid=".$customrule['totypeid']." AND ".$partablename.".employeeid IN (".$roleid.") )";
									$custrulecond .= ' OR ';
								}
							}
						}
						$custrulecond .= "(".$partablename.".employeetypeid=1 AND ".$partablename.".employeeid=".$this->Basefunctions->userid.")";
						$custrulecond .= " ) )";
					} else {
						//$rulecond = ( ($empid != '')? ' AND ('.$partablename.'.createuserid IN ('.$empid.') OR '.$partablename.'.employeetypeid=1 AND '.$partablename.'.employeeid='.$this->Basefunctions->userid.')' : ' AND 1=1' );
						$roleids = $this->Basefunctions->roleididfetchfromrolesandsubroles($roleid);
						$usrroleids = implode(',',$roleids);
						$usrroleids = ( ($usrroleids!='')? $usrroleids:1);
						$usrgrpid =  $this->Basefunctions->groupididfetchuserid($userid);
						$usergroupids = implode(',',$usrgrpid);
						$usergroupids = ( ($usergroupids!='')? $usergroupids:1);
						$rulecond = ( ($empid != '')? ' AND ( '.$partablename.'.createuserid IN ('.$empid.') OR ('.$partablename.'.employeetypeid=1 AND '.$partablename.'.employeeid='.$this->Basefunctions->userid.') OR ('.$partablename.'.employeetypeid=3 AND '.$partablename.'.employeeid='.$roleid.') OR ('.$partablename.'.employeetypeid=4 AND '.$partablename.'.employeeid IN ('.$usrroleids.')) OR ('.$partablename.'.employeetypeid=2 AND '.$partablename.'.employeeid IN ('.$usergroupids.')) )' : ' AND 1=1' );
					}
				} else {//assign field not exits.
					$empids = $this->Basefunctions->empidfetchfromrolesandsubroles($roleid);
					$subempid = implode(',',$empids);
					$empid = ( ($subempid!="")? $subempid.','.$userid : $userid);
					$rulecond = ( ($empid != '')? ' AND '.$partablename.'.createuserid IN ('.$empid.')' : ' AND 1=1' );
				}
			}
			//fetch data
			$data = $this->db->query('select '.$selectdata.' from '.$partablename.' '.$joinq.' WHERE '.$wh.$rulecond.$custrulecond.' AND '.$status);
			if( ($data->num_rows() > 0) ) {
				foreach($data->result()as $datarow) {
					$i=0;
					foreach($formfieldscolmname as $row) { //formfieldsname
						if( !in_array(trim($formfieldstable[$i]),$restricttable) ) {
							$uitypeid = $this->modulefielduitypefetch($formfieldsname[$i],$moduleid);
							if($uitypeid=='8') {
								$date = $this->Basefunctions->userdateformatconvert($datarow->$row);
								$resdata[$formfieldsname[$i]]=$date;
							} else {
								$resdata[$formfieldsname[$i]]=trim($datarow->$row);
							}
						}
						$i++;
					}
				}
				return json_encode($resdata);
			} else {
			   return json_encode(array("fail"=>'Denied'));
			}
		} else {
			return json_encode(array("fail"=>'Denied'));
		}
	}
	//file creation
	public function generatefile($tempcontent) {
		$html_file_name='';
		//@chmod("termscondition/",0766);
		if($tempcontent!='') {
			$html_file_name = 'termscondition/'.trim('tctemplate').'_'.date("YmdHis").mt_rand(1,999999).'.html';
			@chmod($html_file_name,0766);
			@write_file($html_file_name,$tempcontent);
			return $html_file_name;
		}
	}
	//tree delete function
	public function treedatadeletefunction($partabname,$id,$parent) {
		$primaryname = $partabname.'id';
		$parentname = 'parent'.$partabname.'id';
		if($id!=0) {
			$this->db->query("update ".$partabname." set status='0' where ".$primaryname." = ".$id." ");
		}
		$query = $this->db->query("select ".$primaryname." AS Id from ".$partabname." where ".$parentname." = ".$parent." and status = 1");
		foreach($query->result() as $row) {	
			$val=$row->Id;
			$this->treedatadeletefunction($partabname,0,$row->Id);
			$this->db->query("update ".$partabname." set status='0' where ".$primaryname." = ".$val." ");			
		}
	}
	//outer delete concept with restrict
	public function outerdeletefunction($ptable,$ptid,$ctable,$rid) {
		$ludate = date($this->Basefunctions->datef);
		$luuserid= $this->Basefunctions->userid;
		if($ctable !="" ) {
			$count = count($ctable);
			for($i = 0; $i < $count; $i++) {
				$check = $this->verifyidvaluewithrestrict($rid,$ctable[$i],$ptid,$ptable);
				if($check != 'False') {
					$ststus = $ctable[$i].".status=0";
					$lupdate = $ctable[$i].".lastupdatedate='".$ludate;
					$lupuserid = $ctable[$i].".lastupdateuserid=".$luuserid;
					$this->db->query("UPDATE"." ".$ctable[$i]." "."left join"." ".$ptable." "."on"." ".$ptable.".".$ptid."=".$ctable[$i].".".$ptid." SET ".$ststus.",".$lupdate ."',".$lupuserid." WHERE ".$ctable[$i].".".$ptid."=".$rid );
				}
			}
		}
		if($ptable == 'employee') {
			$ststus = $ptable.".status=2";
		} else {
			$ststus = $ptable.".status=0";
		}
		$value = $this->db->query("UPDATE"." ".$ptable." SET ".$ststus.",lastupdatedate='".$ludate."',lastupdateuserid=".$luuserid." WHERE ".$ptable.".".$ptid."=".$rid );
    }
	//outer delete concept
	public function outerdeletefunctionwithoutrestrict($ptable,$ptid,$ctable,$rid) {
		$ludate = date($this->Basefunctions->datef);
		$luuserid= $this->Basefunctions->userid;
		if($ctable !="" ) {
			$count = count($ctable);
			for($i = 0; $i < $count; $i++) {
				$check = $this->verifyidvaluewithoutrestrict($rid,$ctable[$i],$ptid,$ptable);
				if($check != 'False') {
					$ststus = $ctable[$i].".status=0";
					$lupdate = $ctable[$i].".lastupdatedate='".$ludate;
					$lupuserid = $ctable[$i].".lastupdateuserid=".$luuserid;
					$this->db->query("UPDATE"." ".$ctable[$i]." "."left join"." ".$ptable." "."on"." ".$ptable.".".$ptid."=".$ctable[$i].".".$ptid." SET ".$ststus.",".$lupdate ."',".$lupuserid." WHERE ".$ctable[$i].".".$ptid."=".$rid );
				}
			}
		}
		$ststus = $ptable.".status=0";
		$value = $this->db->query("UPDATE"." ".$ptable." SET ".$ststus.",lastupdatedate='".$ludate."',lastupdateuserid=".$luuserid." WHERE ".$ptable.".".$ptid."=".$rid );
    }
	//used to get the primary key of table
	public function primaryname($table) {
		$database = $this->db->database;
		$value = $this->db->query("SELECT `COLUMN_NAME` FROM `information_schema`.`COLUMNS` WHERE (`TABLE_SCHEMA` = '".$database."') AND (`TABLE_NAME` = '$table' ) AND (`COLUMN_KEY` = 'PRI')");
		foreach($value->result() as $row) {
			$result = $row->COLUMN_NAME;
		}
		return $result;
	}
	//used to get the reference table name values
    public function foreignkey($tid,$tname) {
		$database = $this->db->database;
		$i = 0;
		$data = array();
		$value = $this->db->query("SELECT DISTINCT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME IN"." ('".$tid."') "."AND TABLE_NAME != "." ('".$tname."') "."AND TABLE_SCHEMA = '".$database."'");
		if($value->num_rows() >0 ) {
			foreach($value->result() as $row) {
				$data[$i] = $row->TABLE_NAME;
				$i++;
			}
		}
		return $data;
	}
	//check old value in child table
	public function verifyidvaluewithrestrict($rid,$ctable,$ptid,$ptable) {
		$length = strlen($ptable);
		$result = substr($ctable, 0, $length);
		if($result == $ptable){
			$this->db->select($ptid);
			$this->db->from($ctable);
			$this->db->where($ctable.'.'.$ptid,$rid);
			$result = $this->db->get();
			if($result->num_rows() > 0) {
				return "True";
			} else {
				return "False";
			}
		}else {
			return "False";
		} 
	}
	//check old value in child table with out restrict table
	public function verifyidvaluewithoutrestrict($rid,$ctable,$ptid,$ptable) {
		$this->db->select($ptid);
		$this->db->from($ctable);
		$this->db->where($ctable.'.'.$ptid,$rid);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			return "True";
		} else {
			return "False";
		}
	}
}
?>