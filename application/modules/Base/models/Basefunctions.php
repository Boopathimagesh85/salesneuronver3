<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('mailer/class.phpmailer.php');

require 'vendor/autoload.php';
//require 'aws.phar';
//require 'aws/aws-autoloader.php';

use Aws\S3\S3Client;
use Aws\Ses\SesClient;
use Aws\Exception\AwsException;

//To Perform Insert Update Read and Delete Operations
class Basefunctions extends CI_Model {
	function __construct() {
		parent::__construct();
		$uid = $this->session->userdata('logged_in');
		$bid = $this->session->userdata('BranchIds');
		$usrid = $this->session->userdata('UserRoleId');
		$loghistory = $this->session->userdata('loghistory');
		$userdataname = $this->session->userdata('UserDataname');
		$plandata = $this->session->userdata('planids');
		$industryid = $this->session->userdata('industryids');
		$this->userdataname = $userdataname;
		$this->userid = isset($uid['empid']) ? $uid['empid'] : 1;
		$this->branchid = isset($bid['BranchId']) ? $bid['BranchId'] : 1;
		$this->branchstateid = isset($bid['branchstateid']) ? $bid['branchstateid'] : 1;
		
		$this->userroleid = isset($usrid['UserRoleId']) ? $usrid['UserRoleId'] : 1;
		$this->username= isset($uid['username']) ? $uid['username'] : 1;
		$this->logemployeeid= isset($uid['empid']) ? $uid['empid'] : 1;
		$this->planid = isset($plandata['plandataid']) ? $plandata['plandataid'] : 1;
		$this->loghistoryid = isset($loghistory['loghistoryid']) ? $loghistory['loghistoryid'] : 1;
		$this->industryid = isset($industryid['industryid']) ? $industryid['industryid'] : 1;
		$tzone = $this->employeetimezoneset($this->userid);
		date_default_timezone_set($tzone['name']);
	}
	public $salesformtypeparent = array(0,2,3,4,9,10,61,74,78);
    /*Admin Userrole*/
    public $adminuserrole = array(2);
    /*Date Format*/
    public $datef="Y-m-d H:i:s";
    public $timezone = 'UP53';
	/*TAG TEMPLATE MODULE LOADING*/
	public $jewelmodules='7,50,51,59,91,103,153';
	/* Report Vendor Login - Filter Section - Viewcreationcolumns not be loaded */
	public $restrictviewcreationcolids = array('1535','4566','4596','4599','4910','4950');
	/* status concept valiable */
	public $activestatus=1;
	public $deletestatus = 0;
	public $closestatus=11;
	public $cancelstatus=5;
	public $transferstatus = 13;
	/*chitbook Status*/
	public $chitbookstatus = array(11,1);
	/*Vardaan Transaction Status*/
	public $soldstatus = 12;
	public $Inprocessstatus = 14;
	public $approvaloutstatus = 15;
	public $approvaloutreturnstatus = 16;
	public $statusinfo = array('0','3','50');
	
	/* employee default time zone get */
	public function employeetimezoneset($empid) {
		$zonedata = array('name'=>'UTC','val'=>'+0.00');
		if($empid!='') {
			$timedata = $this->db->select('timezone.displaytimezone, timezone.timezonevalue')->from('timezone')->join('employee','employee.timezoneid=timezone.timezoneid','left outer')->where('employee.employeeid',$empid)->get();
			foreach($timedata->result() as $zone) {
				$zonedata['name'] = $zone->displaytimezone;
				$zonedata['val'] = $zone->timezonevalue;
				if($zonedata['name'] == '') {
					$zonedata['name'] = 'UTC';
					$zonedata['val'] = '+0:00';
				}
			}
		}
		return $zonedata;
	}
	/* Get company logo details */
	public function companylogodetails() {
		$info = array('fromid'=>'2','path'=>'','name'=>'');
		$branchid = $this->branchid;
		$this->db->select('company.companyid,companylogo,companylogo_fromid,companylogo_path');
		$this->db->from('company');
		$this->db->join('branch','branch.companyid=company.companyid');
		$this->db->where('branch.branchid',$branchid);
		$result = $this->db->get();
		foreach($result->result() as $data) {
			$info = array('fromid'=>$data->companylogo_fromid,'path'=>$data->companylogo_path,'name'=>$data->companylogo);
		}
		return $info;
	}
	/* device information fetch */
	public function deviceinfo() {
		$this->load->library('Mobiledetect');
		$deviceType = ($this->mobiledetect->isMobile() ? ($this->mobiledetect->isTablet() ? 'tablet' : 'phone') : 'computer');
		return $deviceType;
	}
	/* Shortkey information fetch */
	public function shortcutkeyinfo($moduleid) {
		$data = [];
		$this->db->select('group_concat(keyaction) as keyaction, group_concat(keyactionid) as keyactionid');
		$this->db->from('hotkeys');
		if($moduleid != '') {
			$this->db->where("FIND_IN_SET('$moduleid',hotkeys.moduleid) >", 0);
		} else {
			$this->db->where_not_in("hotkeys.moduleid",1);
		}
		$this->db->where('hotkeys.status',1);
		$qryinfo = $this->db->get();
		if($qryinfo->num_rows() > 0) {
			foreach($qryinfo->result() as $rows) {
				$data['keyaction'] = $rows->keyaction;
				$data['keyactionid'] = $rows->keyactionid;
			}
		} else {
			$data['keyaction'] = 'nokeys';
			$data['keyactionid'] = 'nokeys';
		}
		return $data;
	}
	/* check the module is enabled to login users */
	public function checkusermodule($moduleid,$empid) {
		$status = 'No';
		$userroleid = $this->userroleid;
		$industryid = $this->industryid;
		$device = $this->deviceinfo();
		$i=0;
		$this->db->select("moduleinfo.moduleinfoid");
		$this->db->from("moduleinfo");
		$this->db->join("module","module.moduleid=moduleinfo.moduleid");
		$this->db->where("FIND_IN_SET('$userroleid',moduleinfo.userroleid) >", 0);
		$this->db->where_in("moduleinfo.moduleid",$moduleid);
		$this->db->where("FIND_IN_SET('$industryid',module.industryid) >", 0);
		$this->db->where('module.status',1);
		$this->db->where('moduleinfo.status',1);
		if($device=='phone') {
			$this->db->where_in('module.modulestatus',array(1));
		}
		$querys = $this->db->get();
		if($querys->num_rows() >0) {
			$status = 'Yes';
		} else {
			$status = 'No';
		}
		return $status;
	}
	//fetch max id of table
	public function maximumid($tablename,$fieldname) {
		$data=$this->db->select_max($fieldname)->from($tablename)->get();
		foreach($data->result() as $datas) { return $id = $datas->$fieldname; }
	}
	//fetch max id column with condition
	public function maxidwithcond($tablename,$fieldname,$condname,$condvalue) {
		$data=$this->db->select_max($fieldname)->from($tablename)->where($condname,$condvalue)->get();
		foreach($data->result() as $datas) { return $id = $datas->$fieldname; }
	}
	//Application date format
	public function appdateformatfetch() {
		$this->db->select('dateformatname');
		$this->db->from('dateformat');
		$this->db->join('company','company.dateformatid=dateformat.dateformatid');
		$this->db->where('company.status',1);
		$this->db->order_by('company.companyid','asc');
		$this->db->limit(1,0);
		$data = $this->db->get();
		foreach($data->result() as $datas) { 
			return $datas->dateformatname;
		}
	}
	//get contact value fetch
	public function fetchcontactvaluedata(){
		$id=trim($_GET['contactid']);
		$data=$this->db->select("contact.contactnumber,contact.emailid,contact.landlinenumber,contactaddress.city,contactaddress.address,contactaddress.pincode,contactaddress.state,contactaddress.country")
		->from("contact")
		->join("contactaddress",'contactaddress.contactid=contact.contactid')
		->where('contact.contactid',$id)
		->where('contactaddress.addressmethod',4)
		->get();
		foreach($data->result() as $row){			
			$value = array('contactnumber'=>$row->contactnumber,'emailid'=>$row->emailid,'landlinenumber'=>$row->landlinenumber,'primarycity'=>$row->city,'primarypincode'=>$row->pincode,'primarystate'=>$row->state,'country'=>$row->country);
		}
		$this->db->select('contactcf.streetname,contactcf.primaryareaname,contactcf.insurance,contactcf.insurancenameid');
		$this->db->from('contactcf');
		$this->db->join("contact",'contact.contactid=contactcf.contactid');
		$this->db->where('contact.contactid',$id);
		$this->db->where('contactcf.status',1);
		$result = $this->db->get();
		foreach($result->result() as $datas) {
			$contactcf = array('streetname'=>$datas->streetname,'primaryarea'=>$datas->primaryareaname,'insurance'=>$datas->insurance,'insurancenameid'=>$datas->insurancenameid);
		}
		if(isset($contactcf)){
			$output = array_merge($value,$contactcf);
		}else{
			$output = $value;
		}		
		echo json_encode($output);
	}
	//used for pagination cocnept
	public function constructWhere($s) {
		$qwery = "";
		$qopers = array(
		  'eq'=>" = ",
		  'ne'=>" <> ",
		  'lt'=>" < ",
		  'le'=>" <= ",
		  'gt'=>" > ",
		  'ge'=>" >= ",
		  'bw'=>" LIKE ",
		  'bn'=>" NOT LIKE ",
		  'in'=>" IN ",
		  'ni'=>" NOT IN ",
		  'ew'=>" LIKE ",
		  'en'=>" NOT LIKE ",
		  'cn'=>" LIKE " ,
		  'nc'=>" NOT LIKE " );
		if ($s) {
			$jsona = json_decode($s,true);
			if(is_array($jsona)){
				$gopr = $jsona['groupOp'];
				$rules = $jsona['rules'];
				$i =0;
				foreach($rules as $key=>$val) {
					$field = $val['field'];
					$op = $val['op'];
					$v = $val['data'];
					if($v && $op) {
						$i++;
						// ToSql in this case is absolutley needed
						$v = $this->Basefunctions->ToSql($field,$op,$v);
						if ($i == 1) $qwery = "";
						else $qwery .= " " .$gopr." ";
						switch ($op) {
							// in need other thing
							case 'in' :
							case 'ni' :
								$qwery .= $field.$qopers[$op]." (".$v.")";
								break;
							default:
								$qwery .= $field.$qopers[$op].$v;
								break;
						}
					}
				}
			}
		}
		return $qwery ; 
	}
	function ToSql ($field, $oper, $val) {
		// we need here more advanced checking using the type of the field - i.e. integer, string, float
		switch ($field) {
			case 'id':
				return intval($val);
				break;
			case 'amount':
				break;
			case 'tax':
				break;
			case 'total':
				return floatval($val);
			break;
			default :
				//mysql_real_escape_string is better
				if($oper=='bw' || $oper=='bn') return "'%" . addslashes($val) . "%'";
				else if ($oper=='ew' || $oper=='en') return "'%" . addcslashes($val) . "'";
				else if ($oper=='cn' || $oper=='nc') return "'%" . addslashes($val) . "%'";
				else return "'" . addslashes($val) . "'";
		}
    }
    public function Strip($value) {
		if(get_magic_quotes_gpc() != 0) {
			if(is_array($value)) { 
				if ( array_is_associative($value) ) {
					foreach( $value as $k=>$v)
						$tmp_val[$k] = stripslashes($v);
						$value = $tmp_val; 
				} else {
					for($j = 0; $j < sizeof($value); $j++)
					$value[$j] = stripslashes($value[$j]);
				}
			} else {
				$value = stripslashes($value);
			}
		}
		return $value;
	}
	//simple drop down value fetch
	public function simpledropdown($tablename,$fieldname,$order) {
		$industryid = $this->industryid;
		$this->db->select($fieldname);
		$this->db->from($tablename);
		$this->db->where_not_in('status',array(0,3));
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->order_by($order,"asc");
		$query= $this->db->get();
        return $query->result();
	}
	//simple drop down value fetch with grouping
	public function simpledropdownwithgrp($tablename,$fieldname,$order,$group) {
		$industryid = $this->industryid;
		$this->db->select($fieldname);
		$this->db->from($tablename);
		$this->db->where_not_in('status',array(0,3));
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->order_by($order,"asc");
		$this->db->group_by($group);
		$query= $this->db->get();
        return $query->result();
	}
	//simple drop down value fetch with condition
	public function simpledropdownwithcond($did,$dname,$table,$whfield,$whdata) {
		$i=0;
		$industryid = $this->industryid;
		$mvalu=explode(",",$whdata);
		$this->db->select("$dname,$did");
		$this->db->from($table);
		if($whdata != '') {
			if(count($mvalu)>=2) {
				$this->db->where_in($whfield,$mvalu);
			} else {
				$this->db->where("FIND_IN_SET('$whdata',$whfield) >", 0);
			}
		}
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->where('status',1);
		$this->db->order_by($dname,"asc");
		$result = $this->db->get();
		return $result->result();
    }
	//simple drop down value fetch with condition and order
	public function simpledropdownwithcondorder($did,$dname,$table,$whfield,$whdata,$orderby) {
		$i=0;
		$industryid = $this->industryid;
		$mvalu=explode(",",$whdata);
		$this->db->select("$dname,$did");
		$this->db->from($table);
		if($whdata != '') {
			if(count($mvalu)>=2) {
				$this->db->where_in($whfield,$mvalu);
			} else {
				$this->db->where("FIND_IN_SET('$whdata',$whfield)>",0);
			}
		}
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->where('status',1);
		$this->db->order_by($orderby,"asc");
		$result = $this->db->get();
		return $result->result();
    }
	//simple drop down value fetch with condition
	public function simpledropdownwithmulticond($did,$dname,$table,$whfield,$whdata) {
		$i=0;
		$industryid = $this->industryid;
		$mvalu=explode(",",$whdata);
		$mdata=explode(",",$whfield);
		$this->db->select("$dname,$did",false);
		$this->db->from($table);
		if($whdata != '' && $whfield!='') {
			$i=0;
			foreach($mdata as $wdata) {
				$this->db->where("FIND_IN_SET('$mvalu[$i]',$wdata) >", 0);
				$i++;
			}
		}
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->where('status',1);
		$this->db->order_by($dname,"asc");
		$result = $this->db->get();
		return $result->result();
    }
    //simple grop dropdown
	public function simplegroupdropdown($did,$dname,$table,$whfield,$whdata) {
		$i=0;
		$industryid = $this->industryid;
		$this->db->select("$dname,$did");
		$this->db->from($table);
		if($whdata != '') {
			$this->db->where_in($whfield,$whdata);
		}
		$this->db->where('status',1);
		$this->db->order_by($dname,"asc");
		$result = $this->db->get();
		return $result->result();
    }
    //pagination
    public function getpagination() {
		$page=$_GET['page'];
		$limit=$_GET['rows'];
		$sidx=$_GET['sidx'];
		$sord=$_GET['sord'];
		/*inline-search*/
		$wh = "1";
		$searchOn = $this->Basefunctions->Strip($_REQUEST['_search']);
		if($searchOn=='true') {
			$searchstr = $this->Basefunctions->Strip($_REQUEST['filters']);
			$wh= $this->Basefunctions->constructWhere($searchstr);
		}
		$start = $limit*$page - $limit;
		if ($start<0) 
		$start = 0;
		$paginationval=array('wh'=>$wh,'start'=>$start,'limit'=>$limit,'sidx'=>$sidx,'sord'=>$sord);   
		return $paginationval;
    }
    //get page number
    public function page() {
		$limit=$_GET['rows'];
		$count= $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
		if($limit == 'ALL') {
			$limit=$count;
		} else {
			$limit=$limit;
		}
		if($count >0) {
			$total_pages = ceil($count/$limit);
		} else {
			$total_pages = 0;
		}
		$arr=array('totalpages'=>$total_pages,'count'=>$count);
		return $arr;
    }
	//For dashboard view
	public function dashboarddataviewbydropdown($moduleid) {
		$emoduleid = implode(',',$moduleid);
		$userid = $this->logemployeeid;
		$industryid = $this->industryid;
		$this->db->select('dashboard.dashboardid,dashboard.dashboardname,dashboard.setdefault,dashboard.createuserid,dashboard.dashboardid,dashboardfoldername,dashboard.dashboardfolderid');
		$this->db->from('dashboardfolder');
		$this->db->join('dashboard','dashboard.dashboardfolderid=dashboard.dashboardfolderid');
		$this->db->join('module','module.moduleid=dashboard.moduleid');
		if($emoduleid == 1){
			$this->db->where('( dashboardfolder.createuserid = '.$userid.' AND dashboardfolder.status =1 AND dashboard.status =1 AND dashboard.moduleid in ('.$emoduleid.') AND dashboard.active = "Yes" ) OR ( dashboardfolder.setaspublic = "Yes" AND dashboardfolder.status =1 AND dashboard.status =1 AND dashboard.moduleid in ('.$emoduleid.') AND dashboard.active = "Yes" )');
		} else{
			$this->db->where('( dashboardfolder.createuserid = '.$userid.' AND dashboardfolder.status =1 AND dashboard.status =1 AND dashboard.moduleid in ('.$emoduleid.') AND dashboard.active = "Yes" AND module.status = 1 AND  FIND_IN_SET('.$industryid.',module.industryid) >0 ) OR ( dashboardfolder.setaspublic = "Yes" AND dashboardfolder.status =1 AND dashboard.status =1 AND dashboard.moduleid in ('.$emoduleid.') AND dashboard.active = "Yes"  AND module.status = 1 AND  FIND_IN_SET('.$industryid.',module.industryid) >0 )');
		}
				
		$this->db->order_by('dashboard.dashboardname',"asc");
		$this->db->group_by("dashboard.dashboardid");
		$query= $this->db->get();
		return $query->result();
	}
	//dashboard chart information fetch	
	public function dashboarddatainformationfetch($dashboardid,$minidashboardstatus,$notestatus) {
		$userroleid = $this->Basefunctions->userroleid;
        $i=0;
        $dashboarddatainfo = array();
		$this->db->select('dashboardfield.dashboardfieldid,dashboardfield.dashboardrowid,dashboardfield.dashboardfieldtype,dashboardfield.dashboardfiledname,dashboardfield.fieldviewtype,dashboardfield.fieldtheme,dashboardfield.fieldrotate,dashboardfield.fieldbasecolor,dashboardfield.fieldangle,dashboardfield.fieldlegend,dashboardfield.fieldlegendtype,dashboardfield.fieldparenttablename,dashboardfield.fieldchildtablename,dashboardfield.fieldconditionname,dashboardfield.fieldconditionvalue,dashboardfield.fieldrowcount,dashboardrow.dashboardrowtype,dashboardfield.reportid,dashboardfield.groupfieldid,dashboardfield.calculationfieldid,dashboardfield.operation,dashboardfield.countstatus,a.viewcreationcolumnname as groupname,b.viewcreationcolumnname as splitname,dashboardfield.splitbyid,dashboardfield.twopointid,widget.moduleid,dashboard.moduleid as mainmoduleid');
		$this->db->from('dashboardfield');
		$this->db->join('dashboardrow','dashboardrow.dashboardrowid=dashboardfield.dashboardrowid');
		$this->db->join('dashboard','dashboard.dashboardid=dashboardrow.dashboardid');
		$this->db->join('widget','widget.widgetname=dashboardfield.dashboardfieldtype');
		$this->db->join('viewcreationcolumns as a','dashboardfield.groupfieldid=a.viewcreationcolumnid');		
		$this->db->join('viewcreationcolumns as b','dashboardfield.groupfieldid=b.viewcreationcolumnid');		
		$this->db->where('dashboard.dashboardid',$dashboardid);
		$this->db->where('dashboardfield.status',1);
		$this->db->where('dashboard.status',1);
		if($minidashboardstatus == 1) {
		   $this->db->where('dashboard.setmini','Yes');
		} else {
		   $this->db->where('dashboard.setmini','No');
		}
		if($notestatus == 1) {
			$this->db->where('widget.widgetid','43');
		} else if($notestatus == 0) {
			$this->db->where('widget.widgetid !=','43');
		}
		$this->db->order_by('dashboardrow.dashboardrowid','asc');
		$this->db->order_by('dashboardfield.dashboardfieldid','asc');
		$querytbg = $this->db->get();
		foreach($querytbg->result() as $rows) {
			if($rows->fieldrowcount == true) {
				$groupcolumnname=$rows->groupname.'  '.'Counts';
			} else {
				$groupcolumnname=$rows->groupname.'  '.ucwords($rows->operation);
			}
			$dashboarddatainfo[$i] = array('chartid'=>$rows->dashboardfieldid,'charttabid'=>$rows->dashboardrowid,'charttype'=>$rows->dashboardfieldtype,'charttitle'=>$rows->dashboardfiledname,'chartviewtype'=>$rows->fieldviewtype,'charttheme'=>$rows->fieldtheme,'chartrotate'=>$rows->fieldrotate,'chartcolor'=>$rows->fieldbasecolor,'chartangle'=>$rows->fieldangle,'chartlegend'=>$rows->fieldlegend,'chartlegendtype'=>$rows->fieldlegendtype,'ptablname'=>$rows->fieldparenttablename,'ctablname'=>$rows->fieldchildtablename,'dashtabtype'=>$rows->dashboardrowtype,'condfield'=>$rows->fieldconditionname,'condfieldval'=>$rows->fieldconditionvalue,'rowcount'=>$rows->fieldrowcount,'reportid'=>$rows->reportid,'groupbyid'=>$rows->groupfieldid,'recordcount'=>$rows->fieldconditionname,'calculationfieldid'=>$rows->calculationfieldid,'operation'=>$rows->operation,'chartgroupname'=>$groupcolumnname,'splitname'=>$rows->splitname,'groupone'=>$rows->groupname,'splitbyid'=>$rows->splitbyid,'twopoint'=>$rows->twopointid,'moduleid'=>$rows->moduleid,'mainmoduleid'=>$rows->mainmoduleid);
			$i++;
		}
        return $dashboarddatainfo;
	}
	//dash board chart data count
	public function dashboarddatainformationgenerte($ptablename,$ctablename) {
		$i=0;
		$ptablefield = "".$ptablename."id,".$ptablename."name";
		$countfield = $ctablename.".".$ptablename."id";
		$condifield = $ptablename."id";
		$fields = explode(',',$ptablefield);
		$actsts = $this->Basefunctions->activestatus;
		$forjoin=' JOIN '.$ptablename.' ON '.$ptablename.'.'.$condifield.'='.$ctablename.'.'.$condifield;
		$this->db->select($ptablefield);
		$this->db->from($ptablename);
		$this->db->where_in("status",array(1));
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach ($result->result() as $value) {
				$wh = $ctablename.'.'.$condifield .'='.$value->$fields[0];
				$data = $this->db->query('select count('.$countfield.') AS a from '.$ctablename.' '.$forjoin.' WHERE '. $wh .' AND '.$ctablename.'.status='.$actsts); 
				foreach ($data->result() as $jvalue) {
					$datafield[$i]=array('captionname'=>$value->$fields[1],'counts'=>$jvalue->a);
					$i++;
				}
			}
			return json_encode($datafield);	
		} else {
		   return json_encode(array("fail"=>'FAILED'));
		}
	}
	//simple widget generation
	public function basicinformationvalfetchmodel($ptablename,$fields,$jointable,$joinfieldid,$recordcount,$dashboarddata,$rowid,$moduleid) {
		//dashboard related widgets
		$moduleid_data=1;
		$module_data =$this->db->select('moduleid')
							->from('module')
							->like('modulemastertable',$ptablename,'none')
							->limit(1)
							->get();
		if($module_data->num_rows() > 0){
			foreach($module_data->result() as $info){
				$moduleid_data=$info->moduleid;
			}
		}
		$dashboardmoduleid=$dashboarddata['mainmoduleid'];
		$dashboardname=$dashboarddata['charttype'];
		$dashboardand=' AND 1=1 ';
		$specialjoin='';
		if($dashboardmoduleid > 1){
			 //retrieve dataset
			$dm=$this->db->select('fieldmoduleid,fieldname,specialjoin')->from('relatedwidget')
								->where('moduleid',$dashboardmoduleid)
								->where('widgetname',$dashboardname)
								->where('status',1)->limit(1)->get();
			if($dm->num_rows() > 0){
				foreach($dm->result() as $info){
					$fieldmoduleid=$info->fieldmoduleid;
					$fieldname=$info->fieldname;
					$specialjoin=$info->specialjoin;
				}			
				//invoice special widget(only on salesorder)
				if($dashboardname == 'ibinvoicewidget' and $dashboardmoduleid =="217") {
					$dsalesorder=$this->db->select('salesordernumber')->from('salesorder')
										->where('salesorderid',$rowid)
										->where('status',1)->limit(1)->get()->result();
					foreach($dsalesorder as $info){				
						$salesordernumber=$info->salesordernumber;					
					}
					$dashboardand .=' AND invoice.mname LIKE "'.$salesordernumber.'" ';
				} else {
					if($fieldmoduleid == null or $fieldmoduleid ==''){
						$dashboardand=' AND '.$fieldname.' ='.$rowid.' ';  
					} else{
						$dashboardand =' AND '.$fieldname.' ='.$rowid.' AND '.$fieldmoduleid.'='.$dashboardmoduleid.' ';  
					}
				}
			} 
		}
		//
		$i=0;
		$datafield= array();
		$forjoin="";
		$datafields = explode(',',$fields);
		$jointab = explode(',',$jointable);
		$jointabid = explode(',',$joinfieldid);
		$jid = 0;	
		if($dashboardname != 'ibpaymentwidget'){
			foreach($jointab as $key => $jtabname) {
				if($jtabname!='') {
					$forjoin .=' LEFT JOIN '.$jtabname.' ON '.$jtabname.'.'.$jointabid[$jid].'='.$ptablename.'.'.$jointabid[$jid];
				}
				$jid++;
			}
			//special join
			if($specialjoin != null or $specialjoin !=''){
				$forjoin .=' JOIN '.$specialjoin;
			}
		}else{
			$forjoin .=' JOIN '.$specialjoin;
		}				
		$actsts = $this->Basefunctions->activestatus;
		//datashare rule based records display
		$datasharearray=$this->datasharerulebasedrow($moduleid_data,$ptablename);
		//
		$data = $this->db->query('select '.$fields.' from '.$ptablename.' '.$forjoin.' WHERE '.$ptablename.'.status='.$actsts.''.$dashboardand.''.$datasharearray['rulecond'].''.$datasharearray['custrulecond'].' ORDER BY '.$ptablename.'.'.$ptablename.'id DESC LIMIT 0 , '.$recordcount.'');
		foreach ($data->result() as $vals) {
			foreach($datafields as $datanaems) {
				$fname = explode('.',$datanaems);
				if(count($fname)>1) {
					$datafield[$i][$fname[1]]=$vals->{$fname[1]};
				} else {
					$datafield[$i][$datanaems]=$vals->$datanaems;
				}
			}
			$i++;
		}
		return json_encode($datafield);	
	}
	//simple widget with filter generation
	public function widgetfilterdata($ptablename,$fields,$jointable,$joinfieldid,$recordcount,$filter,$dashboarddata,$rowid,$endlimitid) {
		//endlimit setting
		$endlimitwhere=' AND 1=1';
		$specialjoin='';
		if($endlimitid !=''){		
			$endlimitwhere=' AND '.$ptablename.'.'.$ptablename.'id < '.$endlimitid;
		}
		$moduleid_data=1;
		$module_data =$this->db->select('moduleid')
							->from('module')
							->like('modulemastertable',$ptablename,'none')
							->limit(1)
							->get();
		if($module_data->num_rows() > 0){
			foreach($module_data->result() as $info){
				$moduleid_data=$info->moduleid;
			}
		}
		//dashboard related widgets
		$dashboardmoduleid=$dashboarddata['moduleid'];
		$dashboardname=$dashboarddata['charttype'];
		$dashboardand=' AND 1=1 ';		
		if($dashboardmoduleid > 1){
			//retrieve dataset
			$dm=$this->db->select('fieldmoduleid,fieldname,specialjoin')->from('relatedwidget')
								->where('moduleid',$dashboardmoduleid)
								->where('widgetname',$dashboardname)
								->where('status',1)->limit(1)->get();
			if($dm->num_rows() > 0){
				foreach($dm->result() as $info){
					$fieldmoduleid=$info->fieldmoduleid;
					$fieldname=$info->fieldname;
					$specialjoin=$info->specialjoin;
				}
				if($fieldmoduleid == null or $fieldmoduleid ==''){
					$dashboardand=' AND '.$fieldname.' ='.$rowid.' ';  
				}else{
					$dashboardand =' AND '.$fieldname.' ='.$rowid.' AND '.$fieldmoduleid.'='.$dashboardmoduleid.' ';  
				}
			}
		}
		$i=0;
		$datafield='';
		$forjoin="";
		$datafields = explode(',',$fields);
		$jointab = explode(',',$jointable);
		$jointabid = explode(',',$joinfieldid);
		$jid = 0;
		if($ptablename != 'payment'){
			foreach($jointab as $key => $jtabname) {
				if($jtabname!='') {
					$forjoin .=' LEFT JOIN '.$jtabname.' ON '.$jtabname.'.'.$jointabid[$jid].'='.$ptablename.'.'.$jointabid[$jid];
				}
				$jid++;
			}
			//special join
			if($specialjoin != null or $specialjoin !=''){
				$forjoin .=' JOIN '.$specialjoin;
			}
		} else {
			$forjoin .=' JOIN '.$specialjoin;
		}		
		//datashare rule based records display
		$datasharearray=$this->datasharerulebasedrow($moduleid_data,$ptablename);
		//
		$actsts = $this->Basefunctions->activestatus;
		$data = $this->db->query('select '.$fields.' from '.$ptablename.' '.$forjoin.' WHERE '.$ptablename.'.status='.$actsts.' AND '.$filter.' '.$dashboardand.''.$endlimitwhere.''.$datasharearray['rulecond'].''.$datasharearray['custrulecond'].' ORDER BY '.$ptablename.'.'.$ptablename.'id DESC LIMIT 0 , '.$recordcount.'');
		foreach ($data->result() as $vals) {
			foreach($datafields as $datanaems) {
				$fname = explode('.',$datanaems);
				if(count($fname)>1) {
					$datafield[$i][$fname[1]]=$vals->$fname[1];
				} else {
					$datafield[$i][$datanaems]=$vals->$datanaems;
				}
			}
			$i++;
		}
		return json_encode($datafield);	
	}
	//simple widget generation with condition
	public function basicinformationcondvalfetchmodel($ptablename,$fields,$jointable,$joinfieldid,$condname,$condvalue,$recordcount) {
		$moduleid_data=1;
		$module_data =$this->db->select('moduleid')
							->from('module')
							->like('modulemastertable',$ptablename,'none')
							->limit(1)
							->get();
		if($module_data->num_rows() > 0){
			foreach($module_data->result() as $info){
				$moduleid_data=$info->moduleid;
			}
		}
		$i=0;
		$datafield = '';
		$forjoin = '';
		$cond = '';
		$datafields = explode(',',$fields);
		$jointab = explode(',',$jointable);
		$jointabid = explode(',',$joinfieldid);
		$jid = 0;
		foreach($jointab as $key => $jtabname) {
			if($jtabname!='') {
				$forjoin .=' JOIN '.$jtabname.' ON '.$jtabname.'.'.$jointabid[$jid].'='.$ptablename.'.'.$jointabid[$jid];
			}
			$jid++;
		}
		$cid = 0;
		$condfildname = explode(',',$condname);
		$condfildvalue = explode(',',$condvalue);
		foreach($condfildname as $key => $coname) {
			if($coname!='') {
				$cond .= $coname.'='.$condfildvalue[$cid].' AND ';
			}
			$cid++;
		}
		//datashare rule based records display
		$datasharearray=$this->datasharerulebasedrow($moduleid_data,$ptablename);
		//
		$actsts = $this->Basefunctions->activestatus;
		$data = $this->db->query('select '.$fields.' from '.$ptablename.' '.$forjoin.' WHERE '.$cond.$ptablename.'.status='.$actsts.' '.$datasharearray['rulecond'].''.$datasharearray['custrulecond'].'ORDER BY '.$ptablename.'.'.$ptablename.'id DESC LIMIT 0 , '.$recordcount.'');
		foreach ($data->result() as $vals) {
			foreach($datafields as $datanaems) {
				$fname = explode('.',$datanaems);
				if(count($fname)>1) {
					$datafield[$i][$fname[1]]=$vals->$fname[1];
				} else {
					$datafield[$i][$datanaems]=$vals->$datanaems;
				}
			}
			$i++;
		}
		return json_encode($datafield);	
	}
    //FOR VIEW.
	//view name check
	public function dynamicviewnamecheckmodel() {
		$viewname = $_POST['viewname'];
		$viewmoduleid = explode(',',$_POST['viewmodids']);
		$userid=$this->logemployeeid;
		$message=array('msg'=>'False','user'=>'False');
		if($viewname != "" && $viewmoduleid != "") {
			$result = $this->db->select('viewcreationname,viewcreationid,viewaspublic,createuserid')->from('viewcreation')->where('viewcreation.viewcreationname',$viewname)->where_in('viewcreation.viewcreationmoduleid',$viewmoduleid)->where('viewcreation.status',1)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {					
					$viewid=$row->viewcreationid;
					$message=array('msg'=>$viewid,'user'=>'False');
					if($row->viewaspublic == 'No' and $userid != $row->createuserid){
						$message=array('msg'=>$viewid,'user'=>'True');
					}
				}
			}
		}
		echo json_encode($message);
	}
	//dynamic view drop down
    public function dataviewbydropdown($viewmoduleid) {
		$vmid = $this->getmoduleid($viewmoduleid);
		$userid = $this->logemployeeid;
		$industryid = $this->industryid;
		$povendormanagement = $this->Basefunctions->get_company_settings('povendormanagement');
		$this->db->select('viewcreation.viewcreationid,viewcreation.viewcreationname');
		$this->db->from('viewcreation');
		$this->db->join('moduleinfo','moduleinfo.moduleid=viewcreation.viewcreationmoduleid','left outer');
		$this->db->join('module','module.moduleid=moduleinfo.moduleid','left outer');
		$this->db->where( '( viewcreation.createuserid = '.$userid.' OR viewcreation.viewaspublic = "Yes") AND moduleinfo.moduleid IN ('.$vmid.') AND viewcreation.status = 1 AND moduleinfo.status= 1 AND FIND_IN_SET('.$industryid.',module.industryid) >0' );
		if($vmid == '52') {
			if($this->userroleid != '3') {
				if($povendormanagement != 1) {
					$this->db->where_not_in('viewcreation.viewcreationid',array(213,215,217,218,228,229,230));
				}
				$this->db->where_not_in('viewcreation.viewcreationid',array(211,219,220,221,222,223,224,231,232,233));
			} else {
				$this->db->where_in('viewcreation.viewcreationid',array(211,219,220,221,222,223,224,231,232,233));
			}
		}
		$this->db->order_by('viewcreation.viewdefault',"desc");
		$this->db->order_by('viewcreation.sortby',"asc");
		$this->db->group_by("viewcreation.viewcreationid");
		$query= $this->db->get();
		return $query->result();
    }
 	//dynamic view column name
    public function dataviewdropdowncolumns($moduleids) {
		/*Checking the given moduleid exits in moduletabsection table.
			1. If available means join with moduletabsection and get viewcreationcolumnicon values with tabsection name.
			2. If not available means, remove moduletabsection join and get only viewcreationcolumnicon values.
		*/
    	$industryid = $this->industryid;
		$query=$this->Basefunctions->checkthemoduleexistinmoduletabsec($moduleids);
		if($query->available) {
			$this->db->select('viewcreationcolumns.viewcreationcolumnid,viewcreationcolumns.viewcreationcolumnname,moduletabsection.moduletabsectionname,viewcreationcolumns.moduletabsectionid,viewcreationcolumns.uitypeid,viewcreationcolumns.fieldrestrict,viewcreationcolumns.fieldview,viewcreationcolumns.viewcreationcolmodelindexname,uitype.aggregatemethodid,module.modulename,viewcreationcolumns.viewcreationmoduleid',false);
			$this->db->from('viewcreationcolumns');
			$this->db->join('viewcreationmodule','viewcreationmodule.viewcreationmoduleid=viewcreationcolumns.viewcreationmoduleid','left outer');
			$this->db->join('moduletabsection','moduletabsection.moduletabsectionid=viewcreationcolumns.moduletabsectionid','left outer');
			$this->db->join('moduleinfo','moduleinfo.moduleid=moduletabsection.moduleid','left outer');
			$this->db->join('module','module.moduleid=moduleinfo.moduleid','left outer');
			$this->db->join('uitype','viewcreationcolumns.uitypeid=uitype.uitypeid','left outer');
			$this->db->where_in('viewcreationcolumns.viewcreationmoduleid',$moduleids);
			$this->db->where('viewcreationcolumns.status',$this->activestatus);
			$this->db->where('moduleinfo.userroleid',$this->userroleid);
			$this->db->where('moduleinfo.status',$this->activestatus);
			$this->db->where("FIND_IN_SET('$industryid',module.industryid) >", 0);
			$this->db->where_not_in('viewcreationcolumns.fieldrestrict',array(1));
			$this->db->where_not_in('viewcreationcolumns.viewcreationcolumnviewtype',array(0));
			$this->db->order_by('moduletabsection.moduletabsectionid',"ASC");
			$this->db->order_by('viewcreationcolumns.viewcreationcolumnid',"ASC");
			$query=$this->db->get();
			return  $query->result();
		} else {	
			$this->db->select('viewcreationcolumns.viewcreationcolumnid,viewcreationcolumns.viewcreationcolumnname,viewcreationcolumns.moduletabsectionid,viewcreationcolumns.uitypeid,viewcreationcolumns.fieldrestrict,viewcreationcolumns.fieldview,viewcreationcolumns.viewcreationcolmodelindexname,uitype.aggregatemethodid',false);
			$this->db->from('viewcreationcolumns');
			$this->db->join('viewcreation','viewcreation.viewcreationmoduleid=viewcreationcolumns.viewcreationmoduleid','left outer');
			$this->db->join('viewcreationmodule','viewcreationmodule.viewcreationmoduleid=viewcreation.viewcreationmoduleid','left outer');			
			$this->db->join('uitype','viewcreationcolumns.uitypeid=uitype.uitypeid','left outer');
			$this->db->where_in('viewcreationcolumns.viewcreationmoduleid',$moduleids);
			$this->db->where('viewcreationcolumns.status',$this->activestatus);			
			$this->db->where_not_in('viewcreationcolumns.fieldrestrict',array(1));
			$this->db->where_not_in('viewcreationcolumns.viewcreationcolumnviewtype',array(0));
			$this->db->order_by('viewcreationcolumns.viewcreationcolumnid',"ASC");
			$query=$this->db->get();
			return  $query->result();
		}		
    }
    //VIEW dd vlaue set
    public function fetchdddataviewddvalmodel() {
		$i=0;
		$mvalu ="";
		$whfield ="";
		$dname=$_GET['dataname'];
		$did=$_GET['dataid'];
		$table=$_GET['datatab'];
		$userid = $this->logemployeeid;
		$industryid = $this->industryid;
		if( isset($_GET['whdatafield']) && isset($_GET['whval']) ) {
			$whfield=$_GET['whdatafield'];
			$whdata=$_GET['whval'];
			if($whdata != "") {
				$mvalu=explode(",",$whdata);
			}
		}
		$this->db->select("$dname,$did");
		$this->db->from($table);		
		if($table == 'viewcreation'){//special to restrict private views
			if($mvalu != '' || $whfield != '') {
				//$mvalues=explode(",",$whdata);				
				$this->db->where('( viewcreation.createuserid = '.$userid.' AND viewcreation.viewcreationmoduleid IN ('.$whdata.') AND viewcreation.status = 1 ) OR ( viewcreation.viewaspublic = "Yes" AND viewcreation.viewcreationmoduleid IN ('.$whdata.') AND viewcreation.status = 1 )');
			}			
		} else {
			if($mvalu != '' || $whfield != '') {
				$this->db->where_in($whfield,$mvalu);
			}
			//$this->db->where_not_in('status',array(0,3,4));
			$this->db->where('status',$this->activestatus);
			$this->db->where("FIND_IN_SET('$industryid',industryid) >", 0);
		}
		$this->db->order_by($dname,'asc');
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result()as $row) {
				$data[$i]=array('datasid'=>$row->$did,'dataname'=>$row->$dname);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		} 
    }
	//VIEW dd value set with uitype
    public function fetchdduitypedataviewddvalmodel() {
		$i=0;
		$mvalu ="";
		$dname=$_GET['dataname'];
		$did=$_GET['dataid'];
		$table=$_GET['datatab'];
		$whfield=$_GET['whdatafield'];
		$whdata=$_GET['whval'];
		$uitype=$_GET['uitype'];
		$indexname=$_GET['indexname'];
		if($whdata != "") {
			$mvalu=explode(",",$whdata);
		}
		$this->db->select("$dname,$did,$uitype,$indexname");
		$this->db->from($table);
		if($mvalu != '' || $whfield != '') {
			$this->db->where_in($whfield,$mvalu);
		}
		$this->db->where_not_in('status',array(0,3,4));
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result()as $row) {
				$data[$i]=array('datasid'=>$row->$did,'dataname'=>$row->$dname,'uitype'=>$row->$uitype,'indexname'=>$row->$indexname);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		} 
    }
    //fetch colname & colmodel information fetch model
    public function gridinformationfetchmodel($creationid) {
    	$colids = 1;
    	$modname = '';
    	$modid = 1;
		//fetch show col ids
		$this->db->select('viewcreation.viewcreationcolumnid,viewcreation.viewcreationid,viewcreationmodule.viewcreationmoduleid,viewcreationmodule.viewcreationmodulename,viewcreation.viewcolumnsize',false);
		$this->db->from('viewcreation');
		$this->db->join('viewcreationmodule','viewcreationmodule.viewcreationmoduleid=viewcreation.viewcreationmoduleid');
		$this->db->where_in('viewcreation.viewcreationid',$creationid);
		$this->db->where('viewcreation.status',1);
		$result=$this->db->get()->result();
		foreach($result as $row) {
			$colids=$row->viewcreationcolumnid;
			$modname = $row->viewcreationmodulename;
			$modid = $row->viewcreationmoduleid;
			$colsize = $row->viewcolumnsize;
		}
		$device = $this->deviceinfo();
		if($device=='phone') {
			$headcolids = $this->mobiledatarowheaderidfetch($creationid);
			$viewcolids = explode(',',$colids);
		} else {
			$viewcolids = explode(',',$colids);
		}
		/* for fetch colname & colmodel fetch */
		$i=0;
		$m=0;
		$data = array();
		$colsizes = explode(',',$colsize);
		foreach($viewcolids as $colid) {
			 $this->db->select("viewcreationcolumns.viewcreationcolumnid,
								viewcreationcolumns.viewcreationcolumnname,
								viewcreationcolumns.viewcreationcolmodelname,
								viewcreationcolumns.viewcreationcolmodelindexname,
								viewcreationcolumns.viewcreationcolmodeljointable,
								viewcreationcolumns.viewcreationcolumnid,
								viewcreationcolumns.viewcreationcolmodelaliasname,
								viewcreationcolumns.viewcreationcolmodeltable,
								viewcreationcolumns.viewcreationparenttable,
								viewcreationcolumns.viewcreationmoduleid,
								viewcreationcolumns.viewcreationjoincolmodelname,
								viewcreationcolumns.viewcreationcolumnviewtype,
								viewcreationcolumns.viewcreationtype,
								viewcreationcolumns.viewcreationcolmodelcondname,
								viewcreationcolumns.uitypeid,
								viewcreationcolumns.moduletabsectionid,
								viewcreationcolumns.viewcreationcolmodelcondvalue,
								viewcreationcolumns.viewcreationcolumnicon,
								viewcreationcolumns.viewcreationcolumnsize");
			$this->db->from('viewcreationcolumns');
			$this->db->where('viewcreationcolumns.viewcreationcolumnid',$colid);
			$this->db->where('viewcreationcolumns.status',1);
			$showfield = $this->db->get();
			if($showfield->num_rows() >0) {
				foreach($showfield->result() as $show) {
					$data['colid'][$i]=$show->viewcreationcolumnid;
					$data['colname'][$i]=$show->viewcreationcolumnname;
					$data['colicon'][$i]=$show->viewcreationcolumnicon;
					$data['colsize'][$i]=$colsizes[$m];
					$data['colmodelname'][$i]=$show->viewcreationcolmodelname;
					$data['colmodelaliasname'][$i]=$show->viewcreationcolmodelaliasname;
					$data['coltablename'][$i]=$show->viewcreationcolmodeltable;
					$data['coljointablename'][$i]=$show->viewcreationcolmodeljointable;
					$data['coljoinmodelname'][$i]=$show->viewcreationjoincolmodelname;
					$data['colmodelindex'][$i]=$show->viewcreationcolmodeljointable .'.'.$show->viewcreationcolmodelindexname;
					$data['colmodelparenttable'][$i]=$show->viewcreationparenttable;
					$data['colmodelviewtype'][$i]=$show->viewcreationcolumnviewtype;
					$data['colmodeldatatype'][$i]=$show->viewcreationtype;
					$data['colmodelcondname'][$i]=$show->viewcreationcolmodelcondname;
					$data['colmodelcondvalue'][$i]=$show->viewcreationcolmodelcondvalue;
					$data['colmodelmoduleid'][$i]=$show->viewcreationmoduleid;
					$data['colmodeluitype'][$i]=$show->uitypeid;
					$data['colmodelsectid'][$i]=$show->moduletabsectionid;
					$data['colmodeltype'][$i]='text';
					$data['modname'][$i] = $modname;
					$i++;
				}
			} 
			$m++;
		}
		return $data;
    }
    //filter conditon data fetch
    public function filtergridinformationfetchmodel($creationid,$conditonid) {
    	$colids = 1;
    	$modname = '';
    	$modid = 1;
		//fetch show col ids
		$this->db->select('viewcreation.viewcreationcolumnid,viewcreation.viewcreationid,viewcreationmodule.viewcreationmoduleid,viewcreationmodule.viewcreationmodulename,viewcreation.viewcolumnsize',false);
		$this->db->from('viewcreation');
		$this->db->join('viewcreationmodule','viewcreationmodule.viewcreationmoduleid=viewcreation.viewcreationmoduleid');
		$this->db->where_in('viewcreation.viewcreationid',$creationid);
		$this->db->where('viewcreation.status',1);
		$result=$this->db->get()->result();
		foreach($result as $row) {
			$colids=$row->viewcreationcolumnid;
			$modname = $row->viewcreationmodulename;
			$modid = $row->viewcreationmoduleid;
			$colsize = $row->viewcolumnsize;
		}
		$device = $this->deviceinfo();
		if($device=='phone') {
			$headcolids = $this->mobiledatarowheaderidfetch($creationid);
			$vwcolids = explode(',',$colids);
			$viewcolids = array_unique($vwcolids);
		} else {
			$viewcolids = explode(',',$colids);
			$viewcolids = array_merge($viewcolids,$conditonid);
			$viewcolids = array_unique($viewcolids);
		}
		
    	$i=0;
    	$m=0;
    	$data = array();
    	$colsizes = array();
    	for($l=0;$l< count($viewcolids);$l++) {
    		if (!array_key_exists($l,$colsizes)) {
    			$colsizes[$l]= '200';
    		}
    	}
    	foreach($viewcolids as $colid) {
    		$this->db->select("viewcreationcolumns.viewcreationcolumnid,
								viewcreationcolumns.viewcreationcolumnname,
								viewcreationcolumns.viewcreationcolmodelname,
								viewcreationcolumns.viewcreationcolmodelindexname,
								viewcreationcolumns.viewcreationcolmodeljointable,
								viewcreationcolumns.viewcreationcolumnid,
								viewcreationcolumns.viewcreationcolmodelaliasname,
								viewcreationcolumns.viewcreationcolmodeltable,
								viewcreationcolumns.viewcreationparenttable,
								viewcreationcolumns.viewcreationmoduleid,
								viewcreationcolumns.viewcreationjoincolmodelname,
								viewcreationcolumns.viewcreationcolumnviewtype,
								viewcreationcolumns.viewcreationtype,
								viewcreationcolumns.viewcreationcolmodelcondname,
								viewcreationcolumns.uitypeid,
								viewcreationcolumns.moduletabsectionid,
								viewcreationcolumns.viewcreationcolmodelcondvalue,
								viewcreationcolumns.viewcreationcolumnicon,
								viewcreationcolumns.viewcreationcolumnsize");
    		$this->db->from('viewcreationcolumns');
    		$this->db->where('viewcreationcolumns.viewcreationcolumnid',$colid);
    		$this->db->where_in('viewcreationcolumns.status',array(1,10));
    		$showfield = $this->db->get();
    		if($showfield->num_rows() >0) {
    			foreach($showfield->result() as $show) {
    				$data['colid'][$i]=$show->viewcreationcolumnid;
    				$data['colname'][$i]=$show->viewcreationcolumnname;
    				$data['colicon'][$i]=$show->viewcreationcolumnicon;
    				$data['colsize'][$i]=$colsizes[$m];
    				$data['colmodelname'][$i]=$show->viewcreationcolmodelname;
    				$data['colmodelaliasname'][$i]=$show->viewcreationcolmodelaliasname;
    				$data['coltablename'][$i]=$show->viewcreationcolmodeltable;
    				$data['coljointablename'][$i]=$show->viewcreationcolmodeljointable;
    				$data['coljoinmodelname'][$i]=$show->viewcreationjoincolmodelname;
    				$data['colmodelindex'][$i]=$show->viewcreationcolmodeljointable .'.'.$show->viewcreationcolmodelindexname;
    				$data['colmodelparenttable'][$i]=$show->viewcreationparenttable;
    				$data['colmodelviewtype'][$i]=$show->viewcreationcolumnviewtype;
    				$data['colmodeldatatype'][$i]=$show->viewcreationtype;
    				$data['colmodelcondname'][$i]=$show->viewcreationcolmodelcondname;
    				$data['colmodelcondvalue'][$i]=$show->viewcreationcolmodelcondvalue;
    				$data['colmodelmoduleid'][$i]=$show->viewcreationmoduleid;
    				$data['colmodeluitype'][$i]=$show->uitypeid;
    				$data['colmodelsectid'][$i]=$show->moduletabsectionid;
    				$data['colmodeltype'][$i]='text';
    				$data['modname'][$i] = $modname;
    				$i++;
    			}
    		}
    		$m++;
    	}
    	return $data;
    }
    //mobile data row header id
    public function mobiledatarowheaderidfetch($creationid) {
    	$headcolids = array();
    	$this->db->select('viewcreationcolumns.viewcreationcolumnid',false);
    	$this->db->from('viewcreationcolumns');
    	$this->db->join('viewcreation','viewcreation.viewcreationmoduleid=viewcreationcolumns.viewcreationmoduleid');
    	$this->db->where('viewcreation.viewcreationid',$creationid);
    	$this->db->where('viewcreationcolumns.fieldview',1);
    	$this->db->where('viewcreationcolumns.status',1);
    	$this->db->ORDER_BY('viewcreationcolumns.viewcreationcolumnid','ASC');
    	$this->db->limit(2,0);
    	$datas = $this->db->get();
    	foreach($datas->result() as $row) {
    		$headcolids[] = $row->viewcreationcolumnid;
    	}
    	return $headcolids;
    }
    /* mobile data row view module id */
    public function mobiledataviewmoduleidfetch($creationid) {
    	$modid = 1;
    	$this->db->select('viewcreation.viewcreationmoduleid',false);
    	$this->db->from('viewcreation');
    	$this->db->where('viewcreation.viewcreationid',$creationid);
    	$this->db->where('viewcreation.status',1);
    	$datas = $this->db->get();
    	foreach($datas->result() as $row) {
    		$modid = $row->viewcreationmoduleid;
    	}
    	return $modid;
    }
    /* Local non-live grid header information fetch model */
    public function localgridheaderinformationfetchmodel($moduleid,$tabgroupid) {
    	$userroleid = $this->userroleid;
    	$i=0;
    	$dduitypeids = array('17','18','19','20','23','25','26','27','28','29');
    	$data = array();
    	$this->db->select('modulefield.modulefieldid,modulefield.fieldname,modulefield.columnname,modulefield.fieldlabel,modulefield.uitypeid,');
    	$this->db->from('modulefield');
    	$this->db->join('uitype','uitype.uitypeid=modulefield.uitypeid');
    	$this->db->join('moduletabsection','moduletabsection.moduletabsectionid=modulefield.moduletabsectionid');
    	$this->db->join('moduletabgroup','moduletabgroup.moduletabgroupid=moduletabsection.moduletabgroupid');
    	$this->db->join('module','module.moduleid=moduletabgroup.moduleid');
    	$this->db->where("moduletabgroup.moduletabgroupid", $tabgroupid);
    	$this->db->where("modulefield.moduletabid", $moduleid);
    	$this->db->where("FIND_IN_SET('$userroleid',moduletabsection.userroleid) >", 0);
    	$this->db->where("FIND_IN_SET('$userroleid',modulefield.userroleid) >", 0);
    	$this->db->where_in('modulefield.status',array(1,10));
    	$this->db->where('moduletabsection.moduletabsectiontypeid',1);
    	$this->db->where('moduletabsection.status',1);
    	$this->db->order_by('moduletabsection.sequence','asc');
    	$this->db->order_by('modulefield.sequence','asc');
    	$showfield = $this->db->get();
    	if($showfield->num_rows() >0) {
    		foreach($showfield->result() as $show) {
    			if( in_array($show->uitypeid,$dduitypeids) ) {
    				$data['fieldname'][$i]=$show->fieldname;
    				$data['fieldlabel'][$i]=$show->fieldlabel;
    				$data['colmodelname'][$i]=$show->columnname.'name';
    				$data['colmodelindex'][$i]=$show->columnname;
    				$data['colmodelviewtype'][$i]='1';
    				$data['colmodeltype'][$i]='text';
    				$data['colmodeluitype'][$i]=$show->uitypeid;
    				$data['colmoduleid'][$i]=$moduleid;
    				$i++;
    				$data['fieldname'][$i]=$show->fieldname;
    				$data['fieldlabel'][$i]=$show->fieldlabel;
    				$data['colmodelname'][$i]=$show->columnname;
    				$data['colmodelindex'][$i]=$show->columnname;
    				$data['colmodelviewtype'][$i]='0';
    				$data['colmodeltype'][$i]='text';
    				$data['colmodeluitype'][$i]='2';
    				$data['colmoduleid'][$i]=$moduleid;
    				$i++;
    			} else {
    				$data['fieldname'][$i]=$show->fieldname;
    				$data['fieldlabel'][$i]=$show->fieldlabel;
    				$data['colmodelname'][$i]=$show->columnname;
    				$data['colmodelindex'][$i]=$show->columnname;
    				$data['colmodelviewtype'][$i]='1';
    				$data['colmodeltype'][$i]='text';
    				$data['colmodeluitype'][$i]=$show->uitypeid;
    				$data['colmoduleid'][$i]=$moduleid;
    				$i++;
    			}
    		}
    	}
    	return $data;
    }
    //condition information fetch
    public function conditioninfofetch($viewid) {
		$i=0;
		$multiarray = array();
		//user for get the condition values;
		$this->db->select('viewcreationcolumns.viewcreationcolmodeljointable,viewcreationcolumns.viewcreationcolmodelname,viewcreationcolumns.viewcreationcolmodelindexname,viewcreationcolumns.uitypeid,viewcreationcondition.viewcreationconditionname,viewcreationcondition.viewcreationconditionvalue,viewcreationcondition.viewcreationid,viewcreationcondition.viewcreationandorvalue',false);
		$this->db->from('viewcreationcondition');
		$this->db->join('viewcreationcolumns','viewcreationcolumns.viewcreationcolumnid=viewcreationcondition.viewcreationcolumnid');
		$this->db->where_in('viewcreationcondition.viewcreationid',$viewid);
		$this->db->where_in('viewcreationcondition.status',1);
		$condtionval=$this->db->get();
		foreach($condtionval->result() as $row) {
			$multiarray[$i]=array(
				'0'=>$row->viewcreationcolmodeljointable,
				'1'=>$row->viewcreationcolmodelindexname,
				'2'=>$row->viewcreationconditionname,
				'3'=>$row->viewcreationconditionvalue,
				'4'=>$row->viewcreationandorvalue,
				'5'=>$row->uitypeid
			);
			$i++;
		}
		return $multiarray;
    }
    //fetch dynamic view parent table information (case 1 )
    public function parenttableinfo($leadfieldtab,$viewcolmoduleids) {
		$moduleids = explode(',',$viewcolmoduleids);
		$this->db->select("viewcreationcolumns.viewcreationcolmodeljointable,
		viewcreationcolumns.viewcreationjoincolmodelname,viewcreationcolumns.viewcreationcolmodeltable,
		viewcreationcolumns.viewcreationparenttable");
		$this->db->from('viewcreationcolumns');
		$this->db->where('viewcreationcolumns.status',1);
		$this->db->where('viewcreationcolumns.viewcreationcolmodeltable',$leadfieldtab);
		$this->db->where_in('viewcreationcolumns.viewcreationmoduleid',$moduleids);
		$this->db->limit(1,0);
		$result = $this->db->get();
		return $result;
    }
	//fetch dynamic view parent table information ( case 2 )
    public function parentjointableinfo($leadfieldtab,$viewcolmoduleids) {
		$moduleids = explode(',',$viewcolmoduleids);
		$this->db->select("viewcreationcolumns.viewcreationcolmodeljointable,viewcreationcolumns.viewcreationjoincolmodelname,viewcreationcolumns.viewcreationcolmodeltable,viewcreationcolumns.viewcreationparenttable,viewcreationcolumns.viewcreationcolmodelcondname,viewcreationcolumns.viewcreationcolmodelcondvalue");
		$this->db->from('viewcreationcolumns');
		$this->db->where('viewcreationcolumns.status',1);
		$this->db->where('viewcreationcolumns.viewcreationcolmodeljointable',$leadfieldtab);
		$this->db->where_in('viewcreationcolumns.viewcreationmoduleid',$moduleids);
		$this->db->limit(1,0);
		$result = $this->db->get();
		return $result;
    }
	/* fetch grid data information dynamic view //for view */
    public function viewdynamicdatainfofetch($creationid,$primaryid,$viewcolmoduleids,$pagenum,$rowscount,$sortcol,$sortord,$filter) {
		$whereString="";
		$groupid="";
		$filtercondvalue = "";
		//grid column title information fetch
		$excolinfo = $this->gridinformationfetchmodel($creationid,$viewcolmoduleids);
		$round =  $this->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$softwareindustryid = $this->Basefunctions->industryid;
		//filter condition
		$conditonid = array();
		$filtercondition = array();
		$filtervalue = array();
		
		$colinfo = $this->filtergridinformationfetchmodel($creationid,$conditonid);
		//header colids
		$headcolids = $this->mobiledatarowheaderidfetch($creationid);
		//view moduleid
		$viewmodid = $this->mobiledataviewmoduleidfetch($creationid);
		//main table
		$maintable = $_GET['maintabinfo'];
		$selvalue = $maintable.'.'.$primaryid;		
		$in = 0;
		//generate joins
		$counttable=0;
		if(count($colinfo)>0) {
			$counttable=count($colinfo['coltablename']);
		}
		$jtab = array($maintable);
		$ptab = array($maintable);
		$joinq = "";
		//attribute where condition
		$attrcond = "";
		$attrcondarr = array();
		$x = 0;
		$con = "";
		for($k=0;$k<$counttable;$k++) {
			if($x != 0) { $con = "AND"; }
			//parent join check
			$ptabjoin = "";
			$ptabjoin = $colinfo['colmodelparenttable'][$k];
			if(in_array($ptabjoin,$ptab)) {
				if(!in_array($colinfo['coltablename'][$k],$jtab) && $colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
					array_push($jtab,trim($colinfo['coltablename'][$k]));
					//employee
					$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid LIKE "%1%"';
					//group
					$joinq .= ' LEFT OUTER JOIN employeegroup ON FIND_IN_SET(employeegroup.employeegroupid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid  LIKE "%2%"';
					//user role and sub ordinate
					$joinq .= ' LEFT OUTER JOIN userrole ON FIND_IN_SET(userrole.userroleid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND ('.$maintable.'.employeetypeid LIKE "%3%" OR '.$maintable.'.employeetypeid LIKE "%4%")';
				} else {
					if($colinfo['colmodeldatatype'][$k] == 1) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
							}
						}
					} else if($colinfo['colmodeldatatype'][$k] == 2) {
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else if($colinfo['colmodeldatatype'][$k] == 3) {
						//restrict repeated join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
							}
						}
					}
				}
			} else {
				if($colinfo['colmodeldatatype'][$k] == 1) {
					$result = $this->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status NOT IN (0,3)';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status NOT IN (0,3)';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
							}
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.','.$maintable.'.'.$primaryid.') > 0 AND '.$ptabjoin.'.status NOT IN (0,3)';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 1) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
									}
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 2) {
					$joinresult = $this->parentjointableinfo($ptabjoin,$viewcolmoduleids);
					if($joinresult->num_rows() >0) {
						foreach($joinresult->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
							//condition name
							$condcolname = $show->viewcreationcolmodelcondname;
							//condition value
							$condcolvalue = $show->viewcreationcolmodelcondvalue;
						}
						//restrict repeated parent join
						if(in_array($destab,$ptab)) {
							if($destab != $soucrcetab) {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status NOT IN (0,3)';
								}
							} else {
								if(!in_array($soucrcetab,$jtab)) {
									array_push($jtab,trim($soucrcetab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status NOT IN (0,3)';
								}
							}
						}
						//parent table condition
						if($condcolname != "" && $condcolvalue != "") {
							$attrcond .= " ".$con." ".$srcjointab.'.'.$condcolname."=".$condcolvalue;
							array_push($attrcondarr,$srcjointab.'.'.$joincolname);
							$x = 1;
						}
						//attribute join
						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
							array_push($jtab,trim($colinfo['coltablename'][$k]));
							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
						}
						//condition
						if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
							$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] = $colinfo['colmodelcondvalue'][$k];
							array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
							$x = 1;
						}
					} else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.'='.$maintable.'.'.$primaryid.') > 0 AND '.$ptabjoin.'.status NOT IN (0,3)';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 2) {
								//attribute join
								if(!in_array($colinfo['coltablename'][$k],$jtab)) {
									array_push($jtab,trim($colinfo['coltablename'][$k]));
									$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
								}
								//condition
								if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
									$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
									array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
									$x = 1;
								}
							}
						}
					}
				} else if($colinfo['colmodeldatatype'][$k] == 3) {
					$result = $this->parenttableinfo($ptabjoin,$viewcolmoduleids);
					if($result->num_rows() >0) {
						foreach($result->result() as $show) {
							//user select data child field table with id
							$srcjointab=$show->viewcreationcolmodeljointable;
							//user select data table name
							$soucrcetab=$show->viewcreationcolmodeltable;
							//user select data parent table name with id
							$destab=$show->viewcreationparenttable;
							//join field name
							$joincolname = $show->viewcreationjoincolmodelname;
						}
						//restrict repeated parent join
						if(!in_array($destab,$ptab)) {
							array_push($ptab,$destab);
							if($destab != $soucrcetab) {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status NOT IN (0,3)';
								}
							} else {
								if(!in_array($destab,$jtab)) {
									array_push($jtab,trim($destab));
									$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status NOT IN (0,3)';
								}
							}
						}
						//restrict repeated table join
						$tbl = $colinfo['colmodelparenttable'][$k];
						if($colinfo['coltablename'][$k] == $tbl) {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
							}
						} else {
							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
								array_push($jtab,trim($colinfo['coltablename'][$k]));
								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
							}
						}
					}  else {
						$ptabjoin = $colinfo['colmodelparenttable'][$k];
						if(!in_array($ptabjoin,$ptab)) {
							array_push($ptab,$ptabjoin);
							array_push($jtab,$ptabjoin);
							$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$primaryid.','.$maintable.'.'.$primaryid.') > 0 AND '.$ptabjoin.'.status NOT IN (0,3)';
						}
						if(in_array($ptabjoin,$ptab)) {
							if($colinfo['colmodeldatatype'][$k] == 3) {
								//restrict repeated join
								$tbl = $colinfo['colmodelparenttable'][$k];
								if($colinfo['coltablename'][$k] == $tbl) {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
									}
								} else {
									if(!in_array($colinfo['coltablename'][$k],$jtab)) {
										array_push($jtab,trim($colinfo['coltablename'][$k]));
										$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status NOT IN (0,3)';
									}
								}
							}
						}
					}
				}
			}
			$dduitypeids = array('17','18','19','23','25','26','27','28','29','30');
			//fields fetch
			if($colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].', GROUP_CONCAT(DISTINCT userrole.userrolename) AS rolename, GROUP_CONCAT(DISTINCT employeegroup.employeegroupname) AS empgrpname';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k].',userrole.userrolename AS rolename,employeegroup.employeegroupname AS empgrpname';
				}
			} else if( in_array($colinfo['colmodeluitype'][$k],$dduitypeids) ) {
				$modid = explode(',',$viewcolmoduleids);
				$muloptchk = $this->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
				if($muloptchk == 'Yes') {
					$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].'';
				} else {
					$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
				}
				if($conditonid) {
					if( in_array($colinfo['colid'][$k],$conditonid) ) {
						foreach($conditonid as $nfkey => $nfvalue) {
							if($nfvalue == $colinfo['colid'][$k]) {
								$filterconditionid[] = $filtercondition[$nfkey];
								$filtervalueid[] = $filtervalue[$nfkey];
							}
						}
						if($colinfo['colmodeluitype'][$k] == 25) {
							$fcond = explode(' as ',$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k]);
							$fkey = array_search($colinfo['colid'][$k], $conditonid);
							$filtercondvalue .= $this->filterconditiongenerate($filterconditionid,$filtervalueid,$fcond[0]);
						} else{
							$fkey = array_search($colinfo['colid'][$k], $conditonid);
							$filtercondvalue .= $this->filterconditiongenerate($filterconditionid,$filtervalueid,$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k]);
						}
					}
				}
			} else {
				$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
				if($conditonid) {
					if( in_array($colinfo['colid'][$k],$conditonid, true) ) {
						foreach($conditonid as $nfkey => $nfvalue) {
							if($nfvalue == $colinfo['colid'][$k]) {
								$filterconditionid[] = $filtercondition[$nfkey];
								$filtervalueid[] = $filtervalue[$nfkey];
							}
						}
						$fkey = array_search($colinfo['colid'][$k], $conditonid);
						$adddata = explode(' as',$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k]);
						if(count($adddata)>0){
							$filtercondvalue .= $this->filterconditiongenerate($filterconditionid,$filtervalueid,$adddata[0]);
						} else{
							$filtercondvalue .= $this->filterconditiongenerate($filterconditionid,$filtervalueid,$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k]);
						}
					}
				}
			}
			// jewel modules weight round type based
				if($softwareindustryid == 3)
			  {	
					if($colinfo['colmodeluitype'][$k] == '5'){
						   $selvalue .= ', ROUND('.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k].','.$round.') as '.$colinfo['colmodelaliasname'][$k].'';
					}
			  }
		}
		//attr condition generate
		for($xi=0; $xi < ( count($attrcondarr)-1 ); $xi++ ) {
			$attrcond .= " AND ".$attrcondarr[0].'='.$attrcondarr[$xi+1];
		}
		if( $attrcond == "" ) { $attrcond = "1=1";}
		//custom condition generate
		$cuscondition = " AND 1=1";
		if( isset($_GET['cuscondfieldsname']) ) { 
			$fieldsname = explode(',',$_GET['cuscondfieldsname']);
			if (strpos($_GET['cuscondvalues'], '|') !== false) {
			$condvalues = str_replace("|",",",explode(',',$_GET['cuscondvalues'])); 
			}
			else
			{
				$condvalues = explode(',',$_GET['cuscondvalues']); 
			}
			if($_GET['cuscondtablenames']!=''){$condtables = explode(',',$_GET['cuscondtablenames']);}
			else{$condtables='';}
			if($_GET['cusjointableid']!=''){$condjoinid = explode(',',$_GET['cusjointableid']);}
			else{$condjoinid ='';}
			if(isset($_GET['conditionoperator'])){$conditionoperator = explode(',',$_GET['conditionoperator']);}
			$m=0; 
			foreach($fieldsname AS $values) { 
				if($condvalues[$m] != "") {
					if($condtables!="" && in_array($condtables[$m],$ptab)) {
						$tabname = explode('.',$values);
						//restrict repeated join
						if(!in_array($condtables[$m],$jtab)) {
							array_push($jtab,$condtables[$m]);
							$joinq=$joinq.' LEFT OUTER JOIN '.$condtables[$m].' ON '.$tabname[0].'.'.$condjoinid[$m].'='.$maintable.'.'.$condjoinid[$m];
						}
					}
					if($conditionoperator=='')
					{$cuscondition .= " AND ".$values."=".$condvalues[$m];
					}
					else
					{
						$cuscondition .= $this->varfilterconditiongenerate($conditionoperator[$m],$condvalues[$m],$values);
					}
					$m++;
				}
			}
		}
		$conditionvalarray = $this->conditioninfofetch($creationid);
		$c = count($conditionvalarray);
		if($c > 0) {
			$whereString=$this->conditionalviewwhereclausegeneration($conditionvalarray);
		} else {
			$whereString="";
		}
		$status = $maintable.'.status NOT IN (0,3)';
		$actsts = $this->Basefunctions->activestatus;
		/* fetch module rules based employee list[apply rule] */
		$moduleid = explode(',',$viewcolmoduleids);
		$ruleid =  $this->moduleruleidfetch($moduleid[0]);
		$cusruledata = $this->customrulefetch($moduleid[0]);
		$roleid = $this->userroleid;
		$userid = $this->userid;
		$custrulecond = " ";
		$rulecond="";
		if($ruleid == 0) {
			$assignemp = $this->checkassigntofield($maintable,'employeetypeid');
			$empids = $this->subrolesempidfetch($roleid);
			$subempid = implode(',',$empids);
			$empid = ( ($subempid!="")? $subempid.','.$userid : $userid);
			if($assignemp == 'true') {
				/* custom rule */
				if( sizeof($cusruledata) > 0 ) {
					$custrulecond = " AND ( ".$maintable.".createuserid IN (".$empid.") OR ( ";
					foreach($cusruledata as $key => $customrule) {
						if($customrule['totypeid'] == '2') { //group
							$custrulecond .= "(".$maintable.".employeetypeid=".$customrule['totypeid']." AND ".$maintable.".employeeid=".$customrule['sharedto'].")";
							$custrulecond .= ' OR ';
						} else if( ($customrule['totypeid'] == '3' && $customrule['sharedto'] == $roleid) ) { //roles
							$custrulecond .= "(".$maintable.".employeetypeid=".$customrule['totypeid']." AND ".$maintable.".employeeid=".$customrule['sharedto'].")";
							$custrulecond .= ' OR ';
						} else if( $customrule['totypeid'] == '4' ) { //role and subordinate
							$roleids = $this->roleididfetchfromrolesandsubroles($customrule['sharedto']);
							$usrroleids = implode(',',$roleids);
							$usrroleids = ( ($usrroleids!='')? $usrroleids:1);
							$custrulecond .= "(".$maintable.".employeetypeid=".$customrule['totypeid']." AND ".$maintable.".employeeid IN (".$usrroleids.") )";
							$custrulecond .= ' OR ';
						}
					}
					$custrulecond .= "(".$maintable.".employeetypeid=1 AND ".$maintable.".employeeid=".$this->userid.")";
					$custrulecond .= " ) )";
				} else {
					$roleids = $this->roleididfetchfromrolesandsubroles($roleid);
					$usrroleids = implode(',',$roleids);
					$usrroleids = ( ($usrroleids!='')? $usrroleids:1);
					$usrgrpid =  $this->groupididfetchuserid($userid);
					$usergroupids = implode(',',$usrgrpid);
					$usergroupids = ( ($usergroupids!='')? $usergroupids:1);
					$rulecond = ( ($empid != '')? ' AND ( '.$maintable.'.createuserid IN ('.$empid.') OR ('.$maintable.'.employeetypeid=1 AND '.$maintable.'.employeeid='.$this->userid.') OR ('.$maintable.'.employeetypeid=3 AND '.$maintable.'.employeeid='.$roleid.') OR ('.$maintable.'.employeetypeid=4 AND '.$maintable.'.employeeid IN ('.$usrroleids.')) OR ('.$maintable.'.employeetypeid=2 AND '.$maintable.'.employeeid IN ('.$usergroupids.')) )' : ' AND 1=1' );
				}
			} else {
				$rulecond = ( ($empid != '')? ' AND '.$maintable.'.createuserid IN ('.$empid.')' : ' AND 1=1' );
			}
		}
		/* column sorting */
		$order = '';
		if($sortcol!='' && $sortord!='') {
			$order = 'ORDER BY'.' '.$sortcol.' '.$sortord;
		} else {
			$order = 'ORDER BY'.' '.$maintable.'.'.$maintable.'id DESC';
		}
		/* Pagination */
		$page = $pagenum-1;
		$start = $page * $rowscount;
		$li = ' LIMIT '.$start.','.$rowscount;
		$industryid = $this->industryid;
		if($maintable == 'currency'){
			$industry = "FIND_IN_SET(".$industryid.",".$maintable.".industryid)>0";
		}else{
			$industry = $maintable.'.industryid IN ('.$industryid.')';
		}		
		/* query statements */
		$query = 'select SQL_CALC_FOUND_ROWS '.$selvalue.' from '.$maintable.' '.$joinq.' WHERE '.$status.' AND '.$industry.' AND '.$attrcond.' '.$whereString.''.$cuscondition.''.$rulecond.''.$custrulecond.''.$filtercondvalue.' GROUP BY '.$maintable.'.'.$maintable.'id '.$order.$li;
		$data = $this->db->query($query);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		/* footer generate */
		$footerdata = array();
		$finalresult=array($excolinfo,$data,$page,$footerdata,$headcolids,$viewmodid,$creationid);
		return $finalresult;
    }
    //fetch field name
    public function muliplegeneralinformaion($tblename,$fieldname,$cond,$cvalue) {
    	$data=[];
    	$i=0;
    	$this->db->select($fieldname,false);
    	$this->db->from($tblename);
    	$this->db->where($cond,$cvalue);
    	$result = $this->db->get();
    	if($result->num_rows() >0) {
    		foreach($result->result()as $row) {
    			$data[$i]=$row->$fieldname;
    			$i++;
    		}
    	}
    	return $data;
    }
    //condition information fetch
    public function mainviewgenerateconditioninfo($conditiondataid,$viewid) {
    	$conditionids=explode(",",$conditiondataid);
    	$i=0;
    	$aggdata='ACTUAL';
    	$conditionarray = array();
    	$havingarray = array();
    	$conditioncolids = [];
    	$havingcolids = [];
    	//user for get the condition values;
    	if(COUNT(array_filter($conditionids))>0) {
		$this->db->select('viewcreationcolumns.viewcreationcolumnid,viewcreationcolumns.viewcreationcolmodeljointable,viewcreationcolumns.viewcreationcolmodelname,viewcreationcolumns.viewcreationcolmodelindexname,viewcreationjoincolmodelname,uitypeid,viewcreationandorvalue,viewcreationconditionname,viewcreationconditionvalue,viewcreationparenttable',false);
    		$this->db->from('viewcreationcolumns');
    		$this->db->join('viewcreationcondition','viewcreationcondition.viewcreationcolumnid=viewcreationcolumns.viewcreationcolumnid and viewcreationcondition.status=1');
    		$this->db->join('viewcreation','viewcreation.viewcreationid=viewcreationcondition.viewcreationid');
    		$this->db->where_in('viewcreationcolumns.viewcreationcolumnid',$conditionids);
    		$this->db->where_in('viewcreation.viewcreationid',$viewid);
    		$this->db->where_in('viewcreationcolumns.status',$this->Basefunctions->activestatus);
    		$this->db->order_by("FIELD (viewcreationcolumns.viewcreationcolumnid,".$conditiondataid."),viewcreationcolumns.viewcreationcolumnid");
    		$condtionval=$this->db->get();
    		$j=0;
    		foreach($condtionval->result() as $row) {
    			if($aggdata=='ACTUAL') {
    				$conditionarray[$j] = array(
    						'0'=>$row->viewcreationcolmodeljointable,
    						'1'=>$row->viewcreationcolmodelindexname,
    						'2'=>$row->viewcreationconditionname,
    						'3'=>$row->viewcreationconditionvalue,
    						'4'=>$row->viewcreationandorvalue,
    						'5'=>$row->viewcreationjoincolmodelname,
    						'6'=>$row->uitypeid,
    						'7'=>$row->viewcreationparenttable
    				);
    				$conditioncolids[$j] =$row->viewcreationcolumnid;
    			}
    			//$i++;
    			$j++;
    		}
    		$info=array(
    				'conditionalarry'=>$conditionarray,
    				'viewcolid'=>$conditioncolids,
    				'havingarray'=>$havingarray,
    				'havingcolids'=>$havingcolids
    		);
    	} else {
    		$info=array(
    				'conditionalarry'=>array(),
    				'viewcolid'=>array(),
    				'havingarray'=>array(),
    				'havingcolids'=>array()
    		);
    	}
    	return $info;
    }
    //filter condition data set in array 
	public function filtergenerateconditioninfo($fconditonid,$filtercondition,$filtervalue) {
    	$conditionids=$fconditonid;
    	$i=0;
    	$aggdata='ACTUAL';
    	$fconditionarray = array();
    	$fconditioncolids = array();
    	$havingarray = array();
    	$conditioncolids = [];
    	$havingcolids = [];
    	//user for get the condition values;
    	$j=0;
    	if(COUNT($conditionids)>0) {
    		for($i=0;$i<count($conditionids);$i++) {
				$this->db->select('viewcreationcolumns.viewcreationcolumnid,viewcreationcolumns.viewcreationcolmodeljointable,viewcreationcolumns.viewcreationcolmodelname,viewcreationcolumns.viewcreationcolmodelindexname,viewcreationjoincolmodelname,uitypeid',false);
    			$this->db->from('viewcreationcolumns');
    			$this->db->where_in('viewcreationcolumns.viewcreationcolumnid',$fconditonid[$i]);
    			$this->db->where_in('viewcreationcolumns.status',$this->Basefunctions->activestatus);
    			$condtionval=$this->db->get();
    			foreach($condtionval->result() as $row) {
    				if($j == 0) {
    					$condition = '';
    				} else {
    					$condition = 'AND';
    				}
    				if($aggdata=='ACTUAL') {
    					$fconditionarray[$j] = array(
    						'0'=>$row->viewcreationcolmodeljointable,
    						'1'=>$row->viewcreationcolmodelindexname,
    						'2'=>$filtercondition[$i],
    						'3'=>$filtervalue[$i],
    						'4'=>$condition,
    						'5'=>$row->viewcreationjoincolmodelname,
    						'6'=>$row->uitypeid
    					);
    					$fconditioncolids[$j] =$row->viewcreationcolumnid;
    				}
    				$j++;
    			}
    		}
    		$info=array(
    			'filterarray'=>$fconditionarray,
    			'fviewcolid'=>$fconditioncolids
    		);
    	} else {
    		$info=array(
    			'filterarray'=>array(),
    			'fviewcolid'=>array()
    		);
    	}
		
    	return $info;
    }
    /* fetch data information dynamic view creation //for main view - gowtham*/
    public function generatemainview($creationid,$pagenum,$rowscount,$sortcol,$sortord,$rowid,$viewcolmoduleids,$filter) {
    	$reporttype = '';
    	$this->load->model('Reportsview/Reportsmodel');
    	$filtercondvalue ="";
    	$havingString="";
    	$havingSelect="";
    	$whereString="";
    	$groupid="";
    	$joinq = "";
    	$grpselectstmt=array();
    	$selectquerycolids=array();
    	$filtervalarray =array(); 
    	//main table
    	$maintable = $_GET['maintabinfo'];
		if($this->userroleid == '3' && $maintable == 'sales') { //Hardcoded vendor based login - main view details
			$vendorviewcreationids = array(211,219,220,221,222,223,224,231,232,233);
			if (in_array($creationid, $vendorviewcreationids)) {
				$creationid = $creationid;
			} else {
				$creationid = 211;
			}
		}
		$this->db->select('viewcreationcolumnid,viewdatecolumnid,viewdatemethod,viewmode,viewstartdate,viewenddate,groupbyviewcreationcolumnid,groupbyrange,sortby',false);
		$this->db->from('viewcreation');
		$this->db->where('viewcreation.viewcreationid',$creationid);
		$this->db->where_in('viewcreation.status',$this->Basefunctions->activestatus);
		$condtionval=$this->db->get();
		foreach($condtionval->result() as $row) {
			$selectquerycolids=$row->viewcreationcolumnid;
			$reportdatecolumnid=$row->viewdatecolumnid;
			$reportdatemethod=$row->viewdatemethod;
			$reportmode=$row->viewmode;
			$reportstartdate=$row->viewstartdate;
			$reportenddate=$row->viewenddate;
			$groupbymaxcolids=explode(",",$row->groupbyviewcreationcolumnid);
			$sortbytypemax=explode(",",$row->sortby);;
			$groupbyrange=$row->groupbyrange;
		}
    	//filter condition
    	if(!empty($filter)) {
    		$fconditonid = explode('|',$filter[0]);
    		$filtercondition = explode('|',$filter[1]);
    		$filtervalue = explode('|',$filter[2]);
    	} else {
    		$fconditonid = array();
    		$filtercondition = array();
    		$filtervalue = array();
    	}
    	$summaramids='';
    	$summaramname='';
    	{
    		$conditionid = $this->muliplegeneralinformaion('viewcreationcondition','viewcreationcolumnid','viewcreationid',$creationid);
    		$conditionminarr=implode(',',$conditionid);
    		$conditionmaxarr=array_values(explode(",",implode(',',$conditionid)));
    		/*Generate Where Clause Condition Start	*/
    		$w = '1=1';
    		$conditionvalarray = $this->mainviewgenerateconditioninfo($conditionminarr,$creationid);
			
    		$filtervalarray = $this->filtergenerateconditioninfo($fconditonid,$filtercondition,$filtervalue);
    		$c = count($conditionvalarray['conditionalarry']);
    		if($c>0) {
    			$whereString=$this->reportgeneratewhereclause($conditionvalarray['conditionalarry']);
    			if(COUNT($conditionvalarray['havingarray'])>0) {
    				$havingString=$this->reportgeneratehavingclause($conditionvalarray['havingarray']);
    				$havingSelect=$this->reporthavingclauseselectstmt($conditionvalarray['havingarray']);
    				$havingSelect=$havingSelect['havingselect'];
    			} else {
    				$havingSelect='';
    			}
    			if(!empty($whereString)>0) {
    				$whereString=$whereString;
    			} else {
    				$whereString="1=1 ";
    			}
    		} else {
    			$whereString=$w.' ';
    			$havingString ='';
    		}
    		if(!empty($havingString)) {
    			$havingString=" HAVING ".$havingString;
    		} else {
    			$havingString="";
    		}
    		//filter condition generate
    		if(count($filtervalarray) > 0) {
    			
				$filterwherecondtion = $this->filtergeneratewhereclause($filtervalarray['filterarray'],$maintable);
				
    			if($filterwherecondtion == '') {
    				$filterwherecondtion = " AND 1=1";
    			} else {
    				$filterwherecondtion = " AND ".$filterwherecondtion;
    			}
    		} else {
    			$filterwherecondtion = " AND 1=1";
    		}
    		/*Generate Where Clause Condition End*/
    	}
    	{	/* Generate Group by & Order By condition */
			$grouporder=$this->newreportsortbygroupbyquery($groupbymaxcolids,$sortbytypemax,$groupbyrange);
			
			if($grouporder['groupbyquery']	== ' ' && $groupbyrange == 1){
				$grouporder['groupbyquery'] = 'GROUP BY'.' '.$maintable.'.'.$maintable.'id DESC';
			} 
    	}
    	{ 	/* Join Query Generation Start
    		*/
    		$selectedviewcolids=explode(",",$selectquerycolids);
    		$reportcolids=array_unique(array_merge($conditionmaxarr,$selectedviewcolids));
    		$reportcolids=array_unique(array_merge($filtervalarray['fviewcolid'],$reportcolids));
    		$reportcolids=array_unique(array_merge($grouporder['viewcolids'],$reportcolids));
    		if(COUNT($conditionvalarray['havingarray'])>0){
    			$reportcolids=array_unique(array_merge($reportcolids,$conditionvalarray['havingcolids']));
    		}
    		$join_stmt = $this->newgeneratejoinquery($viewcolmoduleids,$reportcolids,$_GET['maintabinfo']);
    		/* Join Query Generation End */
    	}
    	{
    		/* Select Query Generation Start*/
    		$selectmaintable= $maintable.'.'.$rowid.', ';
    		$selectmaintablewotbl= $rowid.', ';
    		$in = 0;
    		$gids=implode(",",$grouporder['groupbycolids']);
    		$groupidfocolheader=$grouporder['groupbycolids'];
    		/* Combine all the col ids and Select col ids*/
    		$a1=explode(",",$selectquerycolids);
    		$x1=array_merge($a1,$conditionmaxarr);
    		$combineselectcolgroupcol = array_intersect($x1,$grouporder['groupbycolids']);
    		$combineslegrpcondcolids=array_diff($a1,$combineselectcolgroupcol);
    		//$combineslegrpcondcolids=array_merge($combineslegrpcondcolids,$filtervalarray['fviewcolid']);
    		$finalviewcolids=implode(",",array_filter($combineslegrpcondcolids));
    		//Group by Select statements
    		if(count($grouporder['groupbycolids'])!=0) {
    			$grpselectstmt =$this->newgenerateselectstmt($gids,'','','');
    		}
    		$selectstmt =$this->newgenerateselectstmt($finalviewcolids,'','',$summaramids);
    		/* Select Query Generation End*/
    	}
    	
    	//check date column range
    	$datewhere="1";
    	$date = date("Y-m-d");
    	$startdate='';
    	$enddate='';
    	if($reportdatecolumnid > 1 and $reportdatemethod != '') {
    		//get column specific data
    		$daterangecolumn= $this->Reportsmodel->reportdatecolumn($reportdatecolumnid);
    		if($reportmode == 'custom') {
    			$startdate=$reportstartdate;
    			$startdate = date('Y-m-d', strtotime($startdate));
    			$enddate = $reportenddate;
    			$enddate = date('Y-m-d', strtotime($enddate));
    		} else if($reportmode == 'switch') {
    			$r_date=$this->Basefunctions->datesystem($reportdatemethod);
    			$startdate=$r_date['start'];
    			$enddate=$r_date['end'];
    		} else if($reportmode == 'instant') {
    			$dateofinstant = $this->getdatevalue($reportdatemethod,$date);
    			$startdate = $dateofinstant['start'];
    			$enddate = $dateofinstant['end'];
    		} else if($reportmode == 'direct') {
    			$f_date = strtotime(''.$reportdatemethod.'' ,strtotime($date));
    			$f_date = date("Y-m-d",$f_date);
    			if(strtotime($f_date) > strtotime($date)) {
    				$startdate=$date;
    				$enddate=$f_date;
    			} else {
    				$startdate=$f_date;
    				$enddate=$date;
    			}
    		}else if($reportmode == 'before'){
				$f_date = strtotime(''.$reportdatemethod.' day' ,strtotime($date));
				$f_date = date("Y-m-d",$f_date);
				$startdate=$f_date;
				$enddate=$f_date;
			}
    		$date_col=$daterangecolumn['parent'].'.'.$daterangecolumn['columnname'];
			if($reportmode == 'before') {
				$datewhere="DATE(".$date_col.") < '".$enddate."' ";
				$dateshow = 'Before '.$enddate;
			} else {
				if($startdate == $enddate){
					$datewhere="DATE(".$date_col.") = '".$startdate."' ";
				} else {
					$datewhere="DATE(".$date_col.") >= '".$startdate."' AND DATE(".$date_col.") <= '".$enddate."'";
				}
			}
    	} else {
    		$datewhere="1";
    	}
    	if($datewhere == 1){
    		$datewh="1=1 ";
    	} else {
    		$datewh=$datewhere;
    	}
    	$status = $maintable.'.status NOT IN (0,3,8)';
    	$actsts = $this->Basefunctions->activestatus;
    	//grid column title information fetch
    	$viewmodid = $viewcolmoduleids;
    	$headcolids = $finalviewcolids;
    	$colinfo = $this->newreportinformationfetchmodel($grouporder['groupbycolids'],$finalviewcolids,'','',$summaramids,$creationid);
    	/* column sorting */
    	$order = '';
    	if($sortcol!='' && $sortord!='') {
    		$order = 'ORDER BY'.' '.$sortcol.' '.$sortord;
    	} else {
    		$order = 'ORDER BY'.' '.$maintable.'.'.$maintable.'id DESC';
    	}
    	/* Pagination */
    	$page = $pagenum-1;
    	$start = $page * $rowscount;
    	$li = ' LIMIT '.$start.','.$rowscount;
    	$industryid = $this->industryid;
    	if($viewcolmoduleids == 214) {
    		$industry = 'find_in_set('.$industryid.','.$maintable.'.industryid )';
    	} else {
    		$industry = $maintable.'.industryid IN ('.$industryid.')';
    	}
    	
    	if(count($grouporder['groupbycolids'])!=0) {
    		$groupselectstmt=$grpselectstmt['Selectstmt'].",";
    		$groupselectstmtwotbl=$grpselectstmt['Selectstmtwithouttbl'].",";
    		$rollup=" WITH ROLLUP";
    	} else {
    		$groupselectstmt='';
    		$groupselectstmtwotbl='';
    		$rollup="";
    	}
		
		if($this->userroleid != '3') {
			$query = 'SELECT SQL_CALC_FOUND_ROWS '.$groupselectstmtwotbl.' '.$selectmaintablewotbl.' '. $selectstmt['Selectstmtwithouttbl'].' from (select  '.$groupselectstmt.' '.$selectmaintable.' '.$selectstmt['Selectstmt'].' '.$havingSelect.' from '.$maintable.' '.$join_stmt.' WHERE '.$status.' AND '.$industry.' AND '.$whereString.' '.$filterwherecondtion.' AND '.$datewh.' '. $grouporder['groupbyquery'] .' '.$havingString .' '.$order.') AS t '.$li;
		} else {
			$emp_vendoraccid = $this->Basefunctions->singlefieldfetch('accountid','employeeid','employee',$this->userid); //Emp vendor accountid
			if($emp_vendoraccid != '1') {
				$empaccwhere = "sales.accountid = $emp_vendoraccid";
			} else {
				$empaccwhere = "1=1";
			}
			if($maintable == 'sales') {
				$query = 'SELECT SQL_CALC_FOUND_ROWS '.$groupselectstmtwotbl.' '.$selectmaintablewotbl.' '. $selectstmt['Selectstmtwithouttbl'].' from (select  '.$groupselectstmt.' '.$selectmaintable.' '.$selectstmt['Selectstmt'].' '.$havingSelect.' from '.$maintable.' '.$join_stmt.' WHERE sales.salestransactiontypeid IN(18,21,27) AND '.$empaccwhere.' AND '.$status.' AND '.$industry.' AND '.$whereString.' '.$filterwherecondtion.' AND '.$datewh.' '. $grouporder['groupbyquery'] .' '.$havingString .' '.$order.') AS t '.$li;
			} else {
				$query = 'SELECT SQL_CALC_FOUND_ROWS '.$groupselectstmtwotbl.' '.$selectmaintablewotbl.' '. $selectstmt['Selectstmtwithouttbl'].' from (select  '.$groupselectstmt.' '.$selectmaintable.' '.$selectstmt['Selectstmt'].' '.$havingSelect.' from '.$maintable.' '.$join_stmt.' WHERE '.$empaccwhere.' AND '.$status.' AND '.$industry.' AND '.$whereString.' '.$filterwherecondtion.' AND '.$datewh.' '. $grouporder['groupbyquery'] .' '.$havingString .' '.$order.') AS t '.$li;
			}
		}
		
		$query = str_replace("AND 1=1",'',$query);
    	$data = $this->db->query($query);
    	$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
    	$finalresult=array($colinfo,$data,$page,'',$headcolids,$viewmodid,$creationid);
    	return $finalresult;
    }
	//generate page
    public function generatepage($query,$pagenum,$rowscount) {
    	$rowscount = ($rowscount == '' || $rowscount == 'null') ? 1 : $rowscount;	
    	$data = $this->db->query($query);
    	$count = $data->num_rows();
    	/* generate page numbers */
    	$totalpages = ceil($count / $rowscount);
    	$curpage = $pagenum;
    	$pagenum -= 1;
    	$start = $pagenum * $rowscount;
    	return array('start'=>$start,'records'=>$rowscount,'curpage'=>$curpage,'totalpage'=>$totalpages,'recordcount'=>$count);
    }
    /* Pagination generate */
    public function paginationgenerate($curpage,$limit) {
    	$count= $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
    	if($limit == 'ALL') {
    		$limit=$count;
    	} else {
    		$limit=$limit;
    	}
    	if($count >0) {
    		$total_pages = ceil($count/$limit);
    	} else {
    		$total_pages = 0;
    	}
    	$arr=array('records'=>$limit,'curpage'=>$curpage,'totalpage'=>$total_pages,'recordcount'=>$count);
    	return $arr;
    }
    /* fetch grid data information dynamic view //for view */
    public function viewgridrecorddynamicdatainfofetch($sidx,$sord,$start,$limit,$wh,$creationid,$rowid,$viewcolmoduleids) {
    	$whereString="";
    	$groupid="";
    	//grid column title information fetch
    	$colinfo = $this->gridinformationfetchmodel($creationid);
    	//main table
    	$maintable = $_GET['maintabinfo'];
    	$selvalue = $maintable.'.'.$rowid;
    	$in = 0;
    	//generate joins
    	$counttable=0;
    	if(count($colinfo)>0) {
    		$counttable=count($colinfo['coltablename']);
    	}
    	$jtab = array($maintable);
    	$ptab = array($maintable);
    	$joinq = "";
    	//attribute where condition
    	$attrcond = "";
    	$attrcondarr = array();
    	$x = 0;
    	$con = "";
    	for($k=0;$k<$counttable;$k++) {
    		if($x != 0) { $con = "AND"; }
    		//parent join check
    		$ptabjoin = "";
    		$ptabjoin = $colinfo['colmodelparenttable'][$k];
    		if(in_array($ptabjoin,$ptab)) {
    			if(!in_array($colinfo['coltablename'][$k],$jtab) && $colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
    				array_push($jtab,trim($colinfo['coltablename'][$k]));
    				//employee
    				$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid LIKE "%1%"';
    				//group
    				$joinq .= ' LEFT OUTER JOIN employeegroup ON FIND_IN_SET(employeegroup.employeegroupid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$maintable.'.employeetypeid  LIKE "%2%"';
    				//user role and sub ordinate
    				$joinq .= ' LEFT OUTER JOIN userrole ON FIND_IN_SET(userrole.userroleid,'.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND ('.$maintable.'.employeetypeid LIKE "%3%" OR '.$maintable.'.employeetypeid LIKE "%4%")';
    			} else {
    				if($colinfo['colmodeldatatype'][$k] == 1) {
    					//restrict repeated join
    					$tbl = $colinfo['colmodelparenttable'][$k];
    					if($colinfo['coltablename'][$k] == $tbl) {
    						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
    							array_push($jtab,trim($colinfo['coltablename'][$k]));
    							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
    						}
    					} else {
    						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
    							array_push($jtab,trim($colinfo['coltablename'][$k]));
    							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
    						}
    					}
    				} else if($colinfo['colmodeldatatype'][$k] == 2) {
    					//attribute join
    					if(!in_array($colinfo['coltablename'][$k],$jtab)) {
    						array_push($jtab,trim($colinfo['coltablename'][$k]));
    						$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
    					}
    					//condition
    					if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
    						$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
    						array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
    						$x = 1;
    					}
    				} else if($colinfo['colmodeldatatype'][$k] == 3) {
    					//restrict repeated join
    					$tbl = $colinfo['colmodelparenttable'][$k];
    					if($colinfo['coltablename'][$k] == $tbl) {
    						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
    							array_push($jtab,trim($colinfo['coltablename'][$k]));
    							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
    						}
    					} else {
    						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
    							array_push($jtab,trim($colinfo['coltablename'][$k]));
    							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
    						}
    					}
    				}
    			}
    		} else {
    			if($colinfo['colmodeldatatype'][$k] == 1) {
    				$result = $this->parenttableinfo($ptabjoin,$viewcolmoduleids);
    				//print_r($result);
    				if($result->num_rows() >0) {
    					foreach($result->result() as $show) {
    						//user select data child field table with id
    						$srcjointab=$show->viewcreationcolmodeljointable;
    						//user select data table name
    						$soucrcetab=$show->viewcreationcolmodeltable;
    						//user select data parent table name with id
    						$destab=$show->viewcreationparenttable;
    						//join field name
    						$joincolname = $show->viewcreationjoincolmodelname;
    					}
    					//restrict repeated parent join
    					if(!in_array($destab,$ptab)) {
    						array_push($ptab,$destab);
    						if($destab != $soucrcetab) {
    							if(!in_array($destab,$jtab)) {
    								array_push($jtab,trim($destab));
    								$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
    							}
    						} else {
    							if(!in_array($destab,$jtab)) {
    								array_push($jtab,trim($destab));
    								$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
    							}
    						}
    					}
    					//restrict repeated table join
    					$tbl = $colinfo['colmodelparenttable'][$k];
    					if($colinfo['coltablename'][$k] == $tbl) {
    						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
    							array_push($jtab,trim($colinfo['coltablename'][$k]));
    							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
    						}
    					} else {
    						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
    							array_push($jtab,trim($colinfo['coltablename'][$k]));
    							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
    						}
    					}
    				} else {
    					$ptabjoin = $colinfo['colmodelparenttable'][$k];
    					if(!in_array($ptabjoin,$ptab)) {
    						array_push($ptab,$ptabjoin);
    						array_push($jtab,$ptabjoin);
    						$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$rowid.','.$maintable.'.'.$rowid.') > 0 AND '.$ptabjoin.'.status=1';
    					}
    					if(in_array($ptabjoin,$ptab)) {
    						if($colinfo['colmodeldatatype'][$k] == 1) {
    							//restrict repeated join
    							$tbl = $colinfo['colmodelparenttable'][$k];
    							if($colinfo['coltablename'][$k] == $tbl) {
    								if(!in_array($colinfo['coltablename'][$k],$jtab)) {
    									array_push($jtab,trim($colinfo['coltablename'][$k]));
    									$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
    								}
    							} else {
    								if(!in_array($colinfo['coltablename'][$k],$jtab)) {
    									array_push($jtab,trim($colinfo['coltablename'][$k]));
    									$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
    								}
    							}
    						}
    					}
    				}
    			} else if($colinfo['colmodeldatatype'][$k] == 2) {
    				$joinresult = $this->parentjointableinfo($ptabjoin,$viewcolmoduleids);
    				if($joinresult->num_rows() >0) {
    					foreach($joinresult->result() as $show) {
    						//user select data child field table with id
    						$srcjointab=$show->viewcreationcolmodeljointable;
    						//user select data table name
    						$soucrcetab=$show->viewcreationcolmodeltable;
    						//user select data parent table name with id
    						$destab=$show->viewcreationparenttable;
    						//join field name
    						$joincolname = $show->viewcreationjoincolmodelname;
    						//condition name
    						$condcolname = $show->viewcreationcolmodelcondname;
    						//condition value
    						$condcolvalue = $show->viewcreationcolmodelcondvalue;
    					}
    					//restrict repeated parent join
    					if(in_array($destab,$ptab)) {
    						if($destab != $soucrcetab) {
    							if(!in_array($soucrcetab,$jtab)) {
    								array_push($jtab,trim($soucrcetab));
    								$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
    							}
    						} else {
    							if(!in_array($soucrcetab,$jtab)) {
    								array_push($jtab,trim($soucrcetab));
    								$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
    							}
    						}
    					}
    					//parent table condition
    					if($condcolname != "" && $condcolvalue != "") {
    						$attrcond .= " ".$con." ".$srcjointab.'.'.$condcolname."=".$condcolvalue;
    						array_push($attrcondarr,$srcjointab.'.'.$joincolname);
    						$x = 1;
    					}
    					//attribute join
    					if(!in_array($colinfo['coltablename'][$k],$jtab)) {
    						array_push($jtab,trim($colinfo['coltablename'][$k]));
    						$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
    					}
    					//condition
    					if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
    						$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] = $colinfo['colmodelcondvalue'][$k];
    						array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
    						$x = 1;
    					}
    				} else {
    					$ptabjoin = $colinfo['colmodelparenttable'][$k];
    					if(!in_array($ptabjoin,$ptab)) {
    						array_push($ptab,$ptabjoin);
    						array_push($jtab,$ptabjoin);
    						$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$rowid.'='.$maintable.'.'.$rowid.') > 0 AND '.$ptabjoin.'.status=1';
    					}
    					if(in_array($ptabjoin,$ptab)) {
    						if($colinfo['colmodeldatatype'][$k] == 2) {
    							//attribute join
    							if(!in_array($colinfo['coltablename'][$k],$jtab)) {
    								array_push($jtab,trim($colinfo['coltablename'][$k]));
    								$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$colinfo['colmodelparenttable'][$k].'.'.$colinfo['coljoinmodelname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
    							}
    							//condition
    							if($colinfo['colmodelcondname'][$k] != "" && $colinfo['colmodelcondvalue'][$k] != "") {
    								$attrcond .= " ".$con." ".$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelcondname'][$k] ."=". $colinfo['colmodelcondvalue'][$k];
    								array_push($attrcondarr,$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k]);
    								$x = 1;
    							}
    						}
    					}
    				}
    			} else if($colinfo['colmodeldatatype'][$k] == 3) {
    				$result = $this->parenttableinfo($ptabjoin,$viewcolmoduleids);
    				if($result->num_rows() >0) {
    					foreach($result->result() as $show) {
    						//user select data child field table with id
    						$srcjointab=$show->viewcreationcolmodeljointable;
    						//user select data table name
    						$soucrcetab=$show->viewcreationcolmodeltable;
    						//user select data parent table name with id
    						$destab=$show->viewcreationparenttable;
    						//join field name
    						$joincolname = $show->viewcreationjoincolmodelname;
    					}
    					//restrict repeated parent join
    					if(!in_array($destab,$ptab)) {
    						array_push($ptab,$destab);
    						if($destab != $soucrcetab) {
    							if(!in_array($destab,$jtab)) {
    								array_push($jtab,trim($destab));
    								$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$destab.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
    							}
    						} else {
    							if(!in_array($destab,$jtab)) {
    								array_push($jtab,trim($destab));
    								$joinq .= ' LEFT OUTER JOIN '.$soucrcetab.' ON FIND_IN_SET('.$srcjointab.'.'.$joincolname.','.$maintable.'.'.$joincolname.') > 0 AND '.$srcjointab.'.status=1';
    							}
    						}
    					}
    					//restrict repeated table join
    					$tbl = $colinfo['colmodelparenttable'][$k];
    					if($colinfo['coltablename'][$k] == $tbl) {
    						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
    							array_push($jtab,trim($colinfo['coltablename'][$k]));
    							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
    						}
    					} else {
    						if(!in_array($colinfo['coltablename'][$k],$jtab)) {
    							array_push($jtab,trim($colinfo['coltablename'][$k]));
    							$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
    						}
    					}
    				}  else {
    					$ptabjoin = $colinfo['colmodelparenttable'][$k];
    					if(!in_array($ptabjoin,$ptab)) {
    						array_push($ptab,$ptabjoin);
    						array_push($jtab,$ptabjoin);
    						$joinq .= ' LEFT OUTER JOIN '.$ptabjoin.' ON FIND_IN_SET('.$ptabjoin.'.'.$rowid.','.$maintable.'.'.$rowid.') > 0 AND '.$ptabjoin.'.status=1';
    					}
    					if(in_array($ptabjoin,$ptab)) {
    						if($colinfo['colmodeldatatype'][$k] == 3) {
    							//restrict repeated join
    							$tbl = $colinfo['colmodelparenttable'][$k];
    							if($colinfo['coltablename'][$k] == $tbl) {
    								if(!in_array($colinfo['coltablename'][$k],$jtab)) {
    									array_push($jtab,trim($colinfo['coltablename'][$k]));
    									$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$maintable.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
    								}
    							} else {
    								if(!in_array($colinfo['coltablename'][$k],$jtab)) {
    									array_push($jtab,trim($colinfo['coltablename'][$k]));
    									$joinq .= ' LEFT OUTER JOIN '.$colinfo['coltablename'][$k].' ON FIND_IN_SET('.$colinfo['coljointablename'][$k].'.'.$colinfo['coljoinmodelname'][$k].','.$ptabjoin.'.'.$colinfo['colmodelcondname'][$k].') > 0 AND '.$colinfo['coljointablename'][$k].'.status=1';
    								}
    							}
    						}
    					}
    				}
    			}
    		}
    		$dduitypeids = array('17','18','19','23','25','26','27','28','29');
    		//fields fetch
    		if($colinfo['coltablename'][$k] == 'employee' && $colinfo['colmodeluitype'][$k] == '20') {
    			$modid = explode(',',$viewcolmoduleids);
    			$muloptchk = $this->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
    			if($muloptchk == 'Yes') {
    				$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].', GROUP_CONCAT(DISTINCT userrole.userrolename) AS rolename, GROUP_CONCAT(DISTINCT employeegroup.employeegroupname) AS empgrpname';
    			} else {
    				$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k].',userrole.userrolename AS rolename,employeegroup.employeegroupname AS empgrpname';
    			}
    		} else if( in_array($colinfo['colmodeluitype'][$k],$dduitypeids) ) {
    			$modid = explode(',',$viewcolmoduleids);
    			$muloptchk = $this->dropdownmultipleoptioncheck($modid[0],$colinfo['colmodeluitype'][$k],$colinfo['colmodelsectid'][$k],$colinfo['colname'][$k]);
    			if($muloptchk == 'Yes') {
    				$selvalue .= ',GROUP_CONCAT(DISTINCT '.$colinfo['colmodelindex'][$k].') AS '.$colinfo['colmodelname'][$k].'';
    			} else {
    				$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
    			}
    		} else {
    			$selvalue .= ','.$colinfo['coljointablename'][$k].'.'.$colinfo['colmodelaliasname'][$k];
    		}
    	}
    	//attr condition generate
    	for($xi=0; $xi < ( count($attrcondarr)-1 ); $xi++ ) {
    		$attrcond .= " AND ".$attrcondarr[0].'='.$attrcondarr[$xi+1];
    	}
    	if( $attrcond == "" ) { $attrcond = "1=1";}
    	//custom condition generate
    	$cuscondition = " AND 1=1";
    	if( isset($_GET['cuscondfieldsname']) ) {
    		$fieldsname = explode(',',$_GET['cuscondfieldsname']);
    		$condvalues = explode(',',$_GET['cuscondvalues']);
    		$condtables = explode(',',$_GET['cuscondtablenames']);
    		$condjoinid = explode(',',$_GET['cusjointableid']);
    		$m=0;
    		foreach($fieldsname AS $values) {
    			if($condvalues[$m] != "") {
    				if(in_array($condtables[$m],$ptab)) {
    					$tabname = explode('.',$values);
    					//restrict repeated join
    					if(!in_array($condtables[$m],$jtab)) {
    						array_push($jtab,$condtables[$m]);
    						$joinq=$joinq.' LEFT OUTER JOIN '.$condtables[$m].' ON '.$tabname[0].'.'.$condjoinid[$m].'='.$maintable.'.'.$condjoinid[$m];
    					}
    				}
    				$cuscondition .= " AND ".$values."=".$condvalues[$m];
    				$m++;
    			}
    		}
    	}
    	//generate condition
    	$w = '1=1';
    	if($wh == "1") {
    		$wh = $w;
    	}
    	$conditionvalarray = $this->conditioninfofetch($creationid);
    	$c = count($conditionvalarray);
    	if($c > 0) {
    		$whereString=$this->viewwhereclausegeneration($conditionvalarray);
    	} else {
    		$whereString="";
    	}
    	if($maintable == 'employee') {
    		$status = $maintable.'.status NOT IN (3,8)';
    	} else {
    		$status = $maintable.'.status NOT IN (0,3)';
    	}
    	$li= 'LIMIT '.$start.','.$limit;
    	$actsts = $this->Basefunctions->activestatus;
    	//fetch module rules based employee list[apply rule]
    	$moduleid = explode(',',$viewcolmoduleids);
    	$ruleid =  $this->moduleruleidfetch($moduleid[0]);
    	$cusruledata = $this->customrulefetch($moduleid[0]);
    	$roleid = $this->userroleid;
    	$userid = $this->userid;
    	$custrulecond = " ";
    	$rulecond="";
    	if($ruleid == 0) {
    		$assignemp = $this->checkassigntofield($maintable,'employeetypeid');
    		$empids = $this->subrolesempidfetch($roleid);
    		$subempid = implode(',',$empids);
    		$empid = ( ($subempid!="")? $subempid.','.$userid : $userid);
    		if($assignemp == 'true') {
    			//custom rule
    			if( sizeof($cusruledata) > 0 ) {
    				$custrulecond = " AND ( ".$maintable.".createuserid IN (".$empid.") OR ( ";
    				foreach($cusruledata as $key => $customrule) {
    					if($customrule['totypeid'] == '2') { //group
    						$custrulecond .= "(".$maintable.".employeetypeid=".$customrule['totypeid']." AND ".$maintable.".employeeid=".$customrule['sharedto'].")";
    						$custrulecond .= ' OR ';
    					} else if( ($customrule['totypeid'] == '3' && $customrule['sharedto'] == $roleid) ) { //roles
    						$custrulecond .= "(".$maintable.".employeetypeid=".$customrule['totypeid']." AND ".$maintable.".employeeid=".$customrule['sharedto'].")";
    						$custrulecond .= ' OR ';
    					} else if( $customrule['totypeid'] == '4' ) { //role and subordinate
    						$roleids = $this->roleididfetchfromrolesandsubroles($customrule['sharedto']);
    						$usrroleids = implode(',',$roleids);
    						$usrroleids = ( ($usrroleids!='')? $usrroleids:1);
    						$custrulecond .= "(".$maintable.".employeetypeid=".$customrule['totypeid']." AND ".$maintable.".employeeid IN (".$usrroleids.") )";
    						$custrulecond .= ' OR ';
    					}
    				}
    				$custrulecond .= "(".$maintable.".employeetypeid=1 AND ".$maintable.".employeeid=".$this->userid.")";
    				$custrulecond .= " ) )";
    			} else {
    				$roleids = $this->roleididfetchfromrolesandsubroles($roleid);
    				$usrroleids = implode(',',$roleids);
    				$usrroleids = ( ($usrroleids!='')? $usrroleids:1);
    				$usrgrpid =  $this->groupididfetchuserid($userid);
    				$usergroupids = implode(',',$usrgrpid);
    				$usergroupids = ( ($usergroupids!='')? $usergroupids:1);
    				$rulecond = ( ($empid != '')? ' AND ( '.$maintable.'.createuserid IN ('.$empid.') OR ('.$maintable.'.employeetypeid=1 AND '.$maintable.'.employeeid='.$this->userid.') OR ('.$maintable.'.employeetypeid=3 AND '.$maintable.'.employeeid='.$roleid.') OR ('.$maintable.'.employeetypeid=4 AND '.$maintable.'.employeeid IN ('.$usrroleids.')) OR ('.$maintable.'.employeetypeid=2 AND '.$maintable.'.employeeid IN ('.$usergroupids.')) )' : ' AND 1=1' );
    			}
    		} else {
    			$rulecond = ( ($empid != '')? ' AND '.$maintable.'.createuserid IN ('.$empid.')' : ' AND 1=1' );
    		}
    	}
    	//query statements
    	$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$selvalue.' from '.$maintable.' '.$joinq.' WHERE '.$status.' AND '.$attrcond.' AND '.$wh.''.$whereString.''.$cuscondition.''.$rulecond.''.$custrulecond.' GROUP By '.$maintable.'.'.$maintable.'id ORDER BY'.' '.$sidx.' '.$sord.' '.$li);
    	$finalresult=array($colinfo,$data);
    	return $finalresult;
    }
	//get related fields values
	public function getrelatedmodulefieldvalues($datavalid,$maintable,$table,$rowid) {
		$join='';
		$val='';
		if($maintable!=$table) {
			$join=' LEFT OUTER JOIN '.$maintable.' ON '.$maintable.'.'.$maintable.'id='.$table.'.'.$maintable.'id';
		}
		$dataset = $this->db->query(' select moduleid from '.$table.$join.' WHERE '.$table.'.'.$maintable.'id='.$rowid.' ');
		if($dataset->num_rows() > 0) {
			foreach($dataset->result() as $row) {
				$moduleid = $row->moduleid;
			}
		}
		if($moduleid!='') {
			$fieldset = $this->db->query(' select parenttable,tablename,fieldname from primaryfieldmapping WHERE moduleid='.$moduleid.' AND status=1');
			if($fieldset->num_rows() > 0) {
				foreach($fieldset->result() as $datarow) {
					$partab = $datarow->parenttable;
					$tab = $datarow->tablename;
					$fldname = $datarow->fieldname;
				}
				$joinq='';
				if($partab!=$tab) {
					$joinq=' LEFT OUTER JOIN '.$partab.' ON '.$partab.'.'.$partab.'id='.$tab.'.'.$partab.'id';
				}
				$flddataset = $this->db->query(" select $fldname from ".$tab.$joinq." WHERE ".$tab.".".$partab."id=".$datavalid." ");
				if($flddataset->num_rows() > 0) {
					foreach($flddataset->result() as $row) {
						$val = $row->$fldname;
					}
				}
			}
		}
		return $val;
	}
	/* Dynamically set header col size */
	public function headercolumnresizesetmodel() {
		$viewcolid = $_GET['colid'];
		$viewcolwidth = $_GET['colwidth'];
		$viewid = $_GET['viewid'];
		$position = $_GET['position'];
		$this->db->select('viewcreation.viewcreationid,viewcreation.viewcreationcolumnid,viewcreation.viewcolumnsize');
		$this->db->from('viewcreation');
		$this->db->where('viewcreation.viewcreationid',$viewid);
		$datas = $this->db->get();
		if($datas->num_rows() > 0) {
			foreach($datas->result() as $data) {
				$colsize = $data->viewcolumnsize;
				$colsizes = explode(',',$colsize);
				$colsizes[$position] = $viewcolwidth;
				$newcolsize = implode(',',$colsizes);
				$this->db->update('viewcreation',array('viewcolumnsize'=>$newcolsize),array('viewcreationid'=>$viewid));
				echo 'Success';
			}
		} else {
			echo 'Fail';
		}
	}
	//check multiselect option
	public function dropdownmultipleoptioncheck($modid,$uitypeid,$sectionid,$label) {
		$dataopt = 'No';
		$result=$this->db->query(' select modulefieldid,multiple from modulefield where modulefield.moduletabsectionid="'.$sectionid.'" AND modulefield.moduletabid="'.$modid.'" AND modulefield.uitypeid="'.$uitypeid.'" AND modulefield.fieldlabel="'.$label.'" ');
		if($result->num_rows()>0) {
			foreach($result->result() as $data) {
				$dataopt = $data->multiple;
			}
		}
		return $dataopt;
	}
	//table field name check
	public function checkassigntofield($tblname,$fieldname) {
		$result = 'false';
		$fields = $this->db->field_data($tblname);
		foreach ($fields as $field) {
			$name =  $field->name;
			if($fieldname == $name) {
				$result = 'true';
			}
		}
		return $result;
	}
	//module sharing rule info fetch
	public function moduleruleidfetch($moduleid) {
		$ruleid = 1;
		$this->db->select('datashareruletype')->where_in('datasharerule.moduleid',$moduleid);
		$this->db->from('datasharerule');
		$this->db->join('moduleinfo','moduleinfo.moduleid=datasharerule.moduleid');
		$this->db->where('moduleinfo.status',1);
		$ruleresult = $this->db->get();
		foreach($ruleresult->result() as $row) {
			$ruleid = $row->datashareruletype;
		}
		return $ruleid;
	}
	//module custom sharing rule info fetch
	public function customrulefetch($moduleid) {
		$data = array();
		$i=0;
		$this->db->select('datasharecustomruleid,moduleid,accesstype,superiorallow,recordsharedfromtypeid,recordsharedfrom,recordsharedtotypeid,recordsharedto',false);
		$this->db->from('datasharecustomrule');
		$this->db->where('datasharecustomrule.moduleid',$moduleid);
		$this->db->where_not_in('datasharecustomrule.status',array(3,0));
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('moduleid'=>$row->moduleid,'accesstype'=>$row->accesstype,'superiorallow'=>$row->superiorallow,'fromtypeid'=>$row->recordsharedfromtypeid,'totypeid'=>$row->recordsharedtotypeid,'sharedfrom'=>$row->recordsharedfrom,'sharedto'=>$row->recordsharedto);
				$i++;
			}
		}
		return $data;
	}
    //where condition generation - for main view  -normal condition generate
    public function whereclausegeneration($conditionvalarray) {
		$whereString="";
		$m=0;
		foreach ($conditionvalarray as $key) {
			if($m == 0) {
				$key[4] = "";
				$whereString .= " AND (";
			} else {
				if($key[4] != 'AND' and $key[4] != 'OR') {
					$key[4] = 'AND';
				}
			}
			$whereString.=$this->conditiongenerate($key);
			$m++;
		}
		if($whereString !="" ) {
			$whereString .= " )";
		}
		return $whereString;
    }
	//where condition generation - For main view without condition
    public function viewwhereclausegeneration($conditionvalarray) {
		$whereString="";
		$m=0;
		foreach ($conditionvalarray as $key) {
			if($m == 0) {
				$key[4] = "";
				$whereString .= " AND (";
			} else {
				if($key[4] != 'AND' and $key[4] != 'OR') {
					$key[4] = 'AND';
				}
			}
			if( $key[5] == 20 ) {
				//employee
				$whereString.=$this->conditiongenerate($key);
				//group
				$gkey = array(0=>'employeegroup',1=>'employeegroupname',2=>$key[2],3=>$key[3],4=>'OR');
				$whereString.=$this->conditiongenerate($gkey);
				//user role
				$ukey = array(0=>'userrole',1=>'userrolename',2=>$key[2],3=>$key[3],4=>'OR');
				$whereString.=$this->conditiongenerate($ukey);
			} else {
				$whereString.=$this->conditiongenerate($key);
			}
			$m++;
		}
		if($whereString !="") {
			$whereString .= ")";
		}
		return $whereString;
    }
    //conditional where condition generation - for main view with condition
    public function conditionalviewwhereclausegeneration($conditionvalarray) {
    	$whereString="";
    	$count = count($conditionvalarray);
    	$braces = '';
    	$m=0;
    	foreach ($conditionvalarray as $key) {
    		if($m == 0) {
    			$key[4] = "";
    			$whereString .= " AND ".str_repeat("(",$count-1)."";
    		} else {
    			if($key[4] != 'AND' and $key[4] != 'OR') {
    				$key[4] = 'AND';
    			}
    		}
    		if($m >= 1){
    			$braces = ')'; 
    		}
    		if( $key[5] == 20 ) {
    			$braces = '';
    			//employee
    			$whereString.=$this->newconditiongenerate($key,$braces);
    			//group
    			$gkey = array(0=>'employeegroup',1=>'employeegroupname',2=>$key[2],3=>$key[3],4=>'OR');
    			$whereString.=$this->newconditiongenerate($gkey,$braces);
    			//user role
    			$ukey = array(0=>'userrole',1=>'userrolename',2=>$key[2],3=>$key[3],4=>'OR');
    			$whereString.=$this->newconditiongenerate($ukey,$braces);
    		} else {
    			$whereString.=$this->newconditiongenerate($key,$braces);
    		}
    		$m++;
    	}
    	if($whereString !="") {
    		$whereString .= "";
    	}
    	return $whereString;
    }
    //filter condition generation
    public function filterconditiongenerate($filtercondition,$filtervalue,$filterfield) {
    	$whereString = '';
   		for($i=0;$i<count($filtercondition);$i++) {
   			switch($filtercondition[$i]) {
   				case "Equalto":
   					$whereString .=" AND ".$filterfield." = '".$filtervalue[$i]."'";
   					break;
   				case "NotEqual":
   					$whereString .=" AND ".$filterfield." != '".$filtervalue[$i]."'";
   					break;
   				case "Startwith":
   					$whereString .=" AND ".$filterfield." LIKE '".$filtervalue[$i].'%'."'";
   					break;
   				case "Endwith":
   					$whereString .=" AND ".$filterfield." LIKE '".'%'.$filtervalue[$i]."'";
   					break;
   				case "Middle":
   					$whereString .=" AND ".$filterfield." LIKE '".'%'.$filtervalue[$i].'%'."'";
   					break;
   				case "IN":
   					$whereString .=" AND ".$filterfield." IN ('".$filtervalue[$i]."')";
   					break;
   				case "NOT IN":
   					$whereString .=" AND ".$filterfield." NOT IN ('".$filtervalue[$i]."')";
   					break;
   				case "GreaterThan":
   					$whereString .=" AND ".$filterfield." > '" .$filtervalue[$i]."'";
   					break;
   				case "LessThan":
   					$whereString .=" AND ".$filterfield." < '" .$filtervalue[$i]."'";
   					break;
   				case "GreaterThanEqual":
   					$whereString .= " AND ".$filterfield." >= '" .$filtervalue[$i]."'";
   					break;
   				case "LessThanEqual":
   					$whereString .=" AND ".$filterfield." <= '" .$filtervalue[$i]."'";
   					break;
   				case "Like":
   					$whereString .=" AND ".$filterfield." LIKE '" .$filtervalue[$i]."'";
   					break;
   				case "NOT LIKE":
   					$whereString .=" AND ".$filterfield." NOT LIKE '" .$filtervalue[$i]."'";
   					break;
   				default:
   					$whereString .="";
   					break;
   			}
   		}
   		return $whereString;
    }
    public function varfilterconditiongenerate($filtercondition,$filtervalue,$filterfield) {
    	switch($filtercondition) {
    		case "Equalto":
    			return $whereString=" AND ".$filterfield." = '".$filtervalue."'";
    			break;
    		case "NotEqual":
    			return $whereString=" AND ".$filterfield." != '".$filtervalue."'";
    			break;
    		case "Startwith":
    			return $whereString=" AND ".$filterfield." LIKE '".$filtervalue.'%'."'";
    			break;
    		case "Endwith":
    			return $whereString=" AND ".$filterfield." LIKE '".'%'.$filtervalue."'";
    			break;
    		case "Middle":
    			return $whereString=" AND ".$filterfield." LIKE '".'%'.$filtervalue.'%'."'";
    			break;
    		case "IN":
    			return $whereString =" AND ".$filterfield." IN (".$filtervalue.")";
    			break;
    		case "NOT IN":
    			return $whereString =" AND ".$filterfield." NOT IN '".$filtervalue."'";
    			break;
    		case "GreaterThan":
    			return $whereString=" AND ".$filterfield." > '" .$filtervalue."'";
    			break;
    		case "LessThan":
    			return $whereString=" AND ".$filterfield." < '" .$filtervalue."'";
    			break;
    		case "GreaterThanEqual":
    			return $whereString= " AND ".$filterfield." >= '" .$filtervalue."'";
    			break;
    		case "LessThanEqual":
    			return $whereString=" AND ".$filterfield." <= '" .$filtervalue."'";
    			break;
    		default:
    			return $whereString="";
    			break;
    	}
    }
    //condition generation
    public function newconditiongenerate($key,$braces) {
    	switch($key[2]) {
    		case "Equalto":
    			return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." = '".$key[3]."'".$braces;
    			break;
    		case "NotEqual":
    			return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." != '".$key[3]."'".$braces;
    			break;
    		case "Startwith":
    			return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." LIKE '".$key[3].'%'."'".$braces;
    			break;
    		case "Endwith":
    			return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." LIKE '".'%'.$key[3]."'".$braces;
    			break;
    		case "Middle":
    			return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." LIKE '".'%'.$key[3].'%'."'".$braces;
    			break;
    		case "IN":
    			return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." IN ('".$key[3]."')".$braces;
    			break;
    		case "NOT IN":
    			return $whereString.=" ".$key[4]." ".$key[0].'.'.$key[1]." NOT IN ('".$key[3]."')".$braces;
    			break;
    		case "GreaterThan":
    			return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." > '" .$key[3]."'".$braces;
    			break;
    		case "LessThan":
    			return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." < '" .$key[3]."'".$braces;
    			break;
    		case "GreaterThanEqual":
    			return $whereString= " ".$key[4]." ".$key[0].'.'.$key[1]." >= '" .$key[3]."'".$braces;
    			break;
    		case "LessThanEqual":
    			return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." <= '" .$key[3]."'".$braces;
    			break;
    		case "Like":
    			return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." LIKE '" .$key[3]."'".$braces;
    			break;
    		case "NOT LIKE":
    			return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." NOT LIKE '" .$key[3]."'".$braces;
    			break;
    		default:
    			return $whereString="";
    			break;
    	}
    }
	//condition generation
	public function conditiongenerate($key) {
		switch($key[2]) {
			case "Equalto":
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." = '".$key[3]."'";
				break;
			case "NotEqual": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." != '".$key[3]."'";
				break;
			case "Startwith":
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." LIKE '".$key[3].'%'."'";
				break;
			case "Endwith": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." LIKE '".'%'.$key[3]."'";
				break;
			case "Middle": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." LIKE '".'%'.$key[3].'%'."'";
				break;
			case "IN": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." IN ('".$key[3]."')";
				break;
			case "NOT IN": 
				return $whereString.=" ".$key[4]." ".$key[0].'.'.$key[1]." NOT IN ('".$key[3]."')";
				break;
			case "GreaterThan": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." > '" .$key[3]."'";
				break;
			case "LessThan": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." < '" .$key[3]."'";
				break;
			case "GreaterThanEqual": 
				return $whereString= " ".$key[4]." ".$key[0].'.'.$key[1]." >= '" .$key[3]."'";
				break;
			case "LessThanEqual": 
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." <= '" .$key[3]."'";
				break;
			case "Like":
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." LIKE '" .$key[3]."'";
				break;
			case "NOT LIKE":
				return $whereString=" ".$key[4]." ".$key[0].'.'.$key[1]." NOT LIKE '" .$key[3]."'";
				break;
			default:
				return $whereString="";
				break;
		}
	}
    // dynamic view creation model
    public function viewcreateconmodel() {
		$viewname = $_POST['viewname'];
		$columnids = $_POST['viewcolids'];
		$moduleid = $_POST['moduleid'];
		$viewdefault = $_POST['setdefault'];
		$viewpublic = $_POST['setpublic'];
		if(isset($_POST['date_columnid'])) {
			$date_columnid = $_POST['date_columnid'];
			if(is_numeric($date_columnid)){
				$date_columnid = $date_columnid;
			} else {
				$date_columnid = 1;
			}
		} else {
			$date_columnid = 1;
		}
		if(isset($_POST['date_method'])) {
			if($_POST['date_method'] == null){
				$date_method = 1;
			} else {
				$date_method = $_POST['date_method'];
			}
			
		} else {
			$date_method = 1;
		}
		if($_POST['datauitypeid'] == '31'){
			if(isset($_POST['date_starttime'])) {
				$date_start = $this->ymddatetimeconversion($_POST['date_starttime']);
			} else {
				$date_start = 1;
			}
			if(isset($_POST['date_endtime'])) {
				$date_end = $this->ymddatetimeconversion($_POST['date_endtime']);
			} else {
				$date_end = 1;
			}
		}else{
			if(isset($_POST['date_start'])) {
				$date_start = $_POST['date_start'];
			} else {
				$date_start = 1;
			}
			if(isset($_POST['date_end'])) {
				$date_end = $_POST['date_end'];
			} else {
				$date_end = 1;
			}
		}
		if(isset($_POST['date_mode'])) {
			$date_mode = $_POST['date_mode'];
		} else {
			$date_mode = 1;
		}
		$count=$_POST['count'];
		$griddata=$_POST['data'];
		$formdata=json_decode($griddata,true);
		//footer data
		$fcount=$_POST['fcount'];
		$fgriddata=$_POST['fdata'];
		$footergriddata=json_decode($fgriddata,true);
		$empid = $this->userid;
		$status = $this->activestatus;
		$colcount = count(explode(',',$columnids));
		$colsize = array();
		for($k=0;$k<$colcount;$k++) {
			$colsize[$k] = 200;
		}
		$columnsize = implode(',',$colsize);
		//view creation
		$view=array(
				'viewcreationname'=>$viewname,
				'viewcreationcolumnid'=>$columnids,
				'viewcreationmoduleid'=>$moduleid,
				'viewcolumnsize'=>$columnsize,
				'viewdefault'=>$viewdefault,
				'viewaspublic'=>$viewpublic,
				'viewdatecolumnid'=>$date_columnid,
				'viewdatemethod'=>$date_method,
				'viewmode'=>$date_mode,
				'viewstartdate'=>$date_start,
				'viewenddate'=>$date_end,
				'createdate'=>date($this->datef),
				'lastupdatedate'=>'00000-00-00',
				'createuserid'=>$empid,
				'lastupdateuserid'=>$empid,
				'status'=>$status
		);
		$this->db->insert('viewcreation',$view); 
		$viewcmaxid=$this->maximumid('viewcreation','viewcreationid');
		//view condition create
		if ($formdata != "") {
			for ($i=0;$i<$count;$i++) {
				$uitype = $this->generalinformaion('viewcreationcolumns','uitypeid','viewcreationcolumnid',$formdata[$i]['condcolumnid']);
				if($uitype == 8) {
					$condvale = $this->ymddateconversion($formdata[$i]['finalviewcondvalue']);
					$condvalename = $this->ymddateconversion($formdata[$i]['finalviewcondvaluename']);
				} else if($uitype == 31) {
					$condvale = $this->ymddatetimeconversion($formdata[$i]['finalviewcondvalue']);
					$condvalename = $this->ymddatetimeconversion($formdata[$i]['finalviewcondvaluename']);
				} else {
					$condvale = $formdata[$i]['finalviewcondvalue'];
					$condvalename = $formdata[$i]['finalviewcondvaluename'];
				}
				$condcolumn=array(
							'viewcreationid'=>$viewcmaxid,
							'viewcreationcolumnid'=>$formdata[$i]['condcolumnid'],
							'viewcreationconditionname'=>$formdata[$i]['viewcondition'],
							'viewcreationconditionvalue'=>$condvale,
							'viewcreationandorvalue'=>$formdata[$i]['viewandorcond'],
							'viewcreationconditionvaluename'=>$condvalename,
							'createdate'=>date($this->datef),
							'lastupdatedate'=>'00000-00-00',
							'createuserid'=>$empid,
							'lastupdateuserid'=>$empid,
							'status'=>$status
							);
				$datacon=array_filter($condcolumn);	
				$this->db->insert('viewcreationcondition',$datacon);
			}
		}
		//footer data
		if ($footergriddata != "") {
			for ($i=0;$i<$fcount;$i++) {
				$footercolumn=array(
						'viewcreationid'=>$viewcmaxid,
						'viewcreationcolumnid'=>$footergriddata[$i]['footercolid'],
						'viewcreationcolumnvalue'=>$footergriddata[$i]['footervalue'],
						'viewcreationfootername'=>$footergriddata[$i]['footername'],
						'viewcreationfootericon'=>'',//$footergriddata[$i]['footericonname']
						'viewcreationfootercolor'=>'',
						'viewcreationfootercondition'=>$footergriddata[$i]['footercond'],
						'createdate'=>date($this->datef),
						'lastupdatedate'=>'00000-00-00',
						'createuserid'=>$empid,
						'lastupdateuserid'=>$empid,
						'status'=>$status);
				$footercon=array_filter($footercolumn);
				$this->db->insert('viewcreationfooter',$footercon);
			}
		}
		//default set
		if($viewdefault == 'Yes') {
			$this->viewdefaultsetmodel($empid,$viewcmaxid,$moduleid);
		}
		echo "Success";
    }
	//default view set
	public function viewdefaultsetmodel($empid,$vmaxid,$moduleid) {
		$status = $this->Basefunctions->activestatus;
		$setdata=$this->db->select('datapreferenceid')->where('datapreference.employeeid',$empid)->where('datapreference.moduleid',$moduleid)->from('datapreference')->get();
		$datacount = $setdata->num_rows();
		if($datacount == 0) {
			$defview=array(
				'employeeid'=>$empid,
				'moduleid'=>$moduleid,
				'viewcreationid'=>$vmaxid,
				'Status'=>$status
			);
			$this->db->insert('datapreference',$defview); 
		} else {
			$upviewdef=array('viewcreationid'=>$vmaxid);
			$this->db->where('employeeid',$empid);
			$this->db->where('moduleid',$moduleid);
			$this->db->update('datapreference',$upviewdef);
		}
	}
	//used to get the column name of table
	public function columnameinfo($table) {
		$database = $this->db->database;
		$i=0;
		$value = $this->db->query("SELECT `COLUMN_NAME` FROM `information_schema`.`COLUMNS` WHERE (`TABLE_SCHEMA` = '".$database."') AND (`TABLE_NAME` = '$table' )");
		foreach($value->result() as $row) {
			$result[$i] = $row->COLUMN_NAME;
			$i++;
		}
		return $result;
	}
	//primary key infromation fetch
	public function primaryinfo($table) {
		$primarydata = $this->db->query("SHOW KEYS FROM ".$table." WHERE Key_name = 'PRIMARY'")->result();
		foreach($primarydata as $key) {
			$primarykey = $key->Column_name;
		}
		return $primarykey;
	}
	//view data clone
	public function viewdataclonemodel($parenttable,$childtabinfo,$fieldinfo,$fieldvalue) {
		//parent table clone
		$fval = array();
		$ptablfields = $this->columnameinfo($parenttable);
		$ptabprimname = $this->primaryinfo($parenttable);
		foreach($ptablfields as $fields) {
			if( $ptabprimname != $fields ) {
				array_push($fval,$fields);
			}
		}
		$colnames = implode(',',$fval);
		$this->db->query(' INSERT INTO '.$parenttable.'('.$colnames.') SELECT '.$colnames.' FROM '.$parenttable.' WHERE '.$fieldinfo.'='.$fieldvalue.' ');
		$viewid = $this->db->insert_id();
		//update the currentusers in createuserid,lastupdateuserid		
		$viewupdate=array('createuserid'=>$this->Basefunctions->userid,'lastupdateuserid'=>$this->Basefunctions->userid);
		$this->db->where('viewcreationid',$viewid);
		$this->db->update('viewcreation',$viewupdate);
		//child table insertion
		if($childtabinfo != ""){
			$ctables = explode(',',$childtabinfo);
			foreach( $ctables as $tablname ) {
				$primaryname = $this->primaryinfo($tablname);
				$cfields = $this->columnameinfo($tablname);
				$cfval = array();
				foreach($cfields as $cfield) {
					if( $fieldinfo != $cfield && $primaryname != $cfield) {
						array_push($cfval,$cfield);
					}
				}
				$ccolnames = implode(',',$cfval);
				$this->db->query(' INSERT INTO '.$tablname.'('.$fieldinfo.','.$ccolnames.') SELECT '.$viewid.','.$ccolnames.' FROM '.$tablname.' WHERE '.$fieldinfo.'='.$fieldvalue.' ');
			}
		}
		return $viewid;
	}
    //dynamic view delete
    public function gridinformationdeletemodel() {
		$viewid = $_GET['viewid'];
		$loginuserid = $this->userid;
		$date = date($this->datef);
		$cuserid='';
		$status='viewcreation.status NOT IN (0)';
		$cruserdata = $this->db->query('select createuserid from viewcreation WHERE viewcreation.viewcreationid='.$viewid.' AND '.$status);
		foreach($cruserdata->result() as $row) {
			$cuserid = $row->createuserid;
		}
		if($cuserid == $loginuserid) {
			$updatedata=array('lastupdatedate'=>$date,'lastupdateuserid'=>$this->userid,'status'=>0);
			$this->db->update('viewcreation',$updatedata,array('createuserid'=>$this->userid,'viewcreationid'=>$viewid));
			//delete view conditions
			$this->db->update('viewcreationcondition',$updatedata,array('viewcreationid'=>$viewid));
			echo "Success";
		} else {
			echo "Fail";
		}
    }
	 //dynamic view edit data fetch
    public function fetchvieweditdetailsmodel() {
		$viewid = $_GET['userviewid'];
		$this->db->select('viewcreationid,viewcreationname,viewcreationcolumnid,viewdefault,viewaspublic,viewdatecolumnid,,viewdatemethod,viewmode,viewstartdate,viewenddate',false);
		$this->db->from('viewcreation');
		$this->db->where('viewcreationid',$viewid);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$data = array('viewid'=>$row->viewcreationid,'viewname'=>$row->viewcreationname,'viewcolid'=>$row->viewcreationcolumnid,'vpublic'=>$row->viewaspublic,'viewdefault'=>$row->viewdefault,'viewdatecolumnid'=>$row->viewdatecolumnid,'viewdatemethod'=>$row->viewdatemethod,'viewmode'=>$row->viewmode,'viewstartdate'=>$row->viewstartdate,'viewenddate'=>$row->viewenddate);
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
    }
	//user for get the view createuserid check
	public function viewcreateuseridgetmodel() {
		$viewid = $_POST['userviewid'];
		$loginuserid = $this->userid;
		$cuserid='';
		$status='viewcreation.status NOT IN (0)';
		$cruserdata = $this->db->query('select createuserid,viewaspublic from viewcreation WHERE viewcreation.viewcreationid='.$viewid.' AND '.$status);
		foreach($cruserdata->result() as $row) {
			$cuserid = $row->createuserid;
			$public = $row->viewaspublic;
		}
		if($loginuserid == '2' ){
			echo json_encode(array("success"=>'Success'));
		} else {
			if($public == 'Yes') {
				echo json_encode(array("success"=>'Success'));
			} else {
				if($cuserid == $loginuserid) {
					echo json_encode(array("success"=>'Success'));
				} else {
					echo json_encode(array("fail"=>'FAILED'));
				}
			}
		}
	}
	//fetch default value get
	public function fetchdefvalmodel() {
		$viewid = $_GET['userviewid'];
		$moduleid = $_GET['moduleid'];
		$empid = $this->Basefunctions->logemployeeid;
		$this->db->select('viewcreationid');
		$this->db->from('datapreference');
		$this->db->where('datapreference.moduleid',$moduleid);
		$this->db->where('datapreference.employeeid',$empid);
		$this->db->where('datapreference.viewcreationid',$viewid);
		$this->db->where('datapreference.status',1);
		$result = $this->db->get();
		if($result->num_rows() >0){
			$data = 'Yes';
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//work flow condition header information fetch
	public function viewconddatafetchmodel($moduleid) {
		$fnames = array('viewcondrowid','viewandorcond','viewcondcolumn','viewcondition','finalviewcondvalue','finalviewcondvaluename','viewcolstatus','condcolumnid');
		$flabels = array('Condtion Id','AND/OR','Column Name','Condition','Valueid','Value','Status','ViewColumnId');
		$colmodnames = array('viewcondrowid','viewandorcond','viewcondcolumn','viewcondition','finalviewcondvalue','finalviewcondvaluename','viewcolstatus','condcolumnid');
		$colindexnames = array('viewcreationcondition','viewcreationcondition','viewcreationcondition','viewcreationcondition','viewcreationcondition','viewcreationcondition','viewcreationcondition','viewcreationcondition');
		$uitypes = array('2','2','2','2','2','2','2','2');
		$viewtypes = array('0','1','1','1','0','1','0','0');
		$dduitypeids = array('17','18','19','20','23','25','26','27','28','29');
		$data = array();
		$i=0;
		$j=0;
		foreach($fnames as $fname) {
			if(in_array($uitypes[$j],$dduitypeids)) {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j].'name';
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]='0';
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]='2';
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			} else {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			}
			$j++;
		}
		return $data;
	}
	//condition grid details fetch
	public function conditiongriddatafetchmodel() {
		$productdetail = '';
		$viewid = $_GET['userviewid'];
		$this->db->select('SQL_CALC_FOUND_ROWS viewcreationcondition.viewcreationconditionid,viewcreationcondition.viewcreationconditionid,viewcreationcolumns.viewcreationcolumnname,viewcreationcolumns.viewcreationcolumnid,viewcreationcondition.viewcreationconditionname,viewcreationcondition.viewcreationconditionvalue,viewcreationcondition.viewcreationandorvalue,viewcreationcondition.status,viewcreationcondition.viewcreationconditionvaluename',false);
		$this->db->from('viewcreationcondition');
		$this->db->join('viewcreation','viewcreation.viewcreationid=viewcreationcondition.viewcreationid');
		$this->db->join('viewcreationcolumns','viewcreationcolumns.viewcreationcolumnid=viewcreationcondition.viewcreationcolumnid');
		$this->db->where_not_in('viewcreationcondition.status',array(3,0));
		$this->db->where('viewcreationcondition.viewcreationid',$viewid);
		$data=$this->db->get()->result();
		$j=0;
		foreach($data as $value) {
			$productdetail->rows[$j]['id']=$value->viewcreationconditionid;
			$productdetail->rows[$j]['cell']=array(
					$value->viewcreationconditionid,
					$value->viewcreationandorvalue,
					$value->viewcreationcolumnname,
					$value->viewcreationconditionname,
					$value->viewcreationconditionvalue,
					$value->viewcreationconditionvaluename,
					$value->status,
					$value->viewcreationcolumnid
			);
			$j++;
		}
		if($productdetail == '') {
			$productdetail = array('fail'=>'FAILED');
		}
		echo  json_encode($productdetail);
	}
    // dynamic view update model
    public function viewupdateconmodel() {
		$viewname = $_POST['viewname'];
		$columnids = $_POST['viewcolids'];
		$moduleid = $_POST['moduleid'];
		$delecondids = $_POST['conrowcolids'];
		$deletecondid = explode(',',$delecondids);
		$footerdelecondids = $_POST['footerrowcolids'];
		$footerdeletecondid = explode(',',$footerdelecondids);
		$viewid = $_POST['viewdduserviewid'];
		$viewdefault = $_POST['setdefault'];
		$viewpublic = $_POST['setpublic'];
		if(isset($_POST['date_columnid'])) {
			$date_columnid = $_POST['date_columnid'];
			if($date_columnid != 'null') {
				if(!$date_columnid){
					$date_columnid = 1;
				}
			} else {
				$date_columnid = 1;
			}
		} else {
			$date_columnid = 1;
		}
		if(isset($_POST['date_method'])) {
			$date_method = $_POST['date_method'];
		} else {
			$date_method = 1;
		}
		if($_POST['datauitypeid'] == '31'){
			if(isset($_POST['date_starttime'])) {
				$date_start = $this->ymddatetimeconversion($_POST['date_starttime']);
			} else {
				$date_start = 1;
			}
			if(isset($_POST['date_endtime'])) {
				$date_end = $this->ymddatetimeconversion($_POST['date_endtime']);
			} else {
				$date_end = 1;
			}
		}else{
			if(isset($_POST['date_start'])) {
				$date_start = $_POST['date_start'];
			} else {
				$date_start = 1;
			}
			if(isset($_POST['date_end'])) {
				$date_end = $_POST['date_end'];
			} else {
				$date_end = 1;
			}
		}
		if(isset($_POST['date_mode'])) {
			$date_mode = $_POST['date_mode'];
		} else {
			$date_mode = 1;
		}
		$count=$_POST['count'];
		$griddata=$_POST['data'];
		$formdata=json_decode( $griddata,true);
		//footer data
		$fcount=$_POST['fcount'];
		$fgriddata=$_POST['fdata'];
		$footergriddata=json_decode($fgriddata,true);
		$empid = $this->userid;
		$status = $this->activestatus;
		$colcount = count(explode(',',$columnids));
		$colsize = array();
		for($k=0;$k<$colcount;$k++) {
			$colsize[$k] = 200;
		}
		$columnsize = implode(',',$colsize);
		//view creation update
		$date = date($this->datef);
		$updatedata=array(
				'viewcreationname'=>$viewname,
				'viewcreationcolumnid'=>$columnids,
				'viewcolumnsize'=>$columnsize,
				'viewaspublic'=>$viewpublic,
				'viewdefault'=>$viewdefault,
				'viewdatecolumnid'=>$date_columnid,
				'viewdatemethod'=>$date_method,
				'viewmode'=>$date_mode,
				'viewstartdate'=>$date_start,
				'viewenddate'=>$date_end,
				'lastupdatedate'=>$date,
				'lastupdateuserid'=>$this->userid
			);
		$this->db->update('viewcreation',$updatedata,array('viewcreationid'=>$viewid));
		//view condition create
		if($formdata != "") {
			for ( $i=0;$i<$count;$i++ ) {
				if($formdata[$i]['viewcolstatus'] == "2") {
					$uitype = $this->generalinformaion('viewcreationcolumns','uitypeid','viewcreationcolumnid',$formdata[$i]['condcolumnid']);
					if($uitype == 8) {
						$condvale = $this->ymddateconversion($formdata[$i]['finalviewcondvalue']);
						$condvalename = $this->ymddateconversion($formdata[$i]['finalviewcondvaluename']);
					} else if($uitype == 31) {
						$condvale = $this->ymddatetimeconversion($formdata[$i]['finalviewcondvalue']);
						$condvalename = $this->ymddatetimeconversion($formdata[$i]['finalviewcondvaluename']);
					} else {
						$condvale = $formdata[$i]['finalviewcondvalue'];
						$condvalename = $formdata[$i]['finalviewcondvaluename'];
					}
					$condcolumn=array(
						'viewcreationid'=>$viewid,
						'viewcreationcolumnid'=>$formdata[$i]['condcolumnid'],
						'viewcreationconditionname'=>$formdata[$i]['viewcondition'],
						'viewcreationconditionvalue'=>$condvale,
						'viewcreationandorvalue'=>$formdata[$i]['viewandorcond'],
						'viewcreationconditionvaluename'=>$condvalename,
						'createdate'=>date($this->datef),
						'lastupdatedate'=>'00000-00-00',
						'createuserid'=>$empid,
						'lastupdateuserid'=>$empid,
						'status'=>$status
					);
					$datacon=array_filter($condcolumn);	
					$this->db->insert('viewcreationcondition',$datacon);
				}
			}
		}
		//delete condition
		foreach($deletecondid as $conrowid) {
			if($conrowid != 0){
				$updatedcondata=array('lastupdatedate'=>date($this->datef),'lastupdateuserid'=>$empid,'status'=>0);
				$this->db->update('viewcreationcondition',$updatedcondata,array('viewcreationconditionid'=>$conrowid));
			}
		}
		//default set
		if($viewdefault == 'Yes') {
			$this->viewdefaultsetmodel($empid,$viewid,$moduleid);
		}
		echo "Success";
    }
    //for action assign (toll bar)
    public function actionassign($moduleid) {
    	$tollbarid = "";
    	$userroleid = $this->userroleid;
    	$industryid = $this->industryid;
    	//fetch user mod ids
    	/* $usermodids = $this->usermoduleids($userroleid);
    	$modids=array_map('intval',array(1));
    	if(is_array($usermodids)) {
    		$usermodids[]='1';
    		$modids = array_map('intval',$usermodids);
    	} */
		//fetch toolbar ids
		$this->db->select('toolbarnameid');
		$this->db->from('usertoolbarprivilege');
		$this->db->join('moduleinfo','moduleinfo.moduleid=usertoolbarprivilege.moduleid');
		$this->db->join('module','module.moduleid=moduleinfo.moduleid');
		$this->db->where("FIND_IN_SET('$userroleid',usertoolbarprivilege.userroleid) >", 0);
		$this->db->where("FIND_IN_SET('$industryid',module.industryid) >", 0);
		$this->db->where_in("moduleinfo.moduleid",$moduleid); 
		$this->db->where('usertoolbarprivilege.status',1);
		$this->db->group_by('usertoolbarprivilege.usertoolbarprivilegeid');
		$query= $this->db->get();
		foreach ($query->result() as $row) {
			$tollbarid = $row->toolbarnameid;
		}
		
		$tollbarids = explode(',',$tollbarid);
		sort($tollbarids);
		$device = $this->deviceinfo();
		$this->db->select('toolbarname.description,toolbarname.toolbarname,toolbarname.toolbartitle,toolbarname.toolbarwidth,toolbarname.toolbarnameid,toolbarcategory.toolbarcategoryname,toolbarcategory.toolbarcategoryid',false);
		$this->db->from('toolbarname');
		$this->db->join('module','module.moduleid=toolbarname.moduleid');
		$this->db->join('toolbarcategory','toolbarcategory.toolbarcategoryid=toolbarname.toolbarcategoryid');
		//$this->db->where_in('toolbarname.moduleid',$modids);
		$this->db->where_in('toolbarname.toolbarnameid',$tollbarids);
		$this->db->where_in('module.status',array(1,3));
		if($device=='phone') {
			$this->db->where_in('toolbarname.actionstatus',array(1));
		}
		$this->db->where('toolbarname.status',1);
		$this->db->where('toolbarcategory.status',1);
		$this->db->order_by('toolbarcategory.toolbarcategoryid','asc');
		$this->db->order_by('toolbarname.toolbarwidth','asc');
		$tollbar = $this->db->get()->result();
		return $tollbar;
    }
    //fetch user mod ids
    public function usermoduleids($roleid) {
    	$moduleids = array();
    	$this->db->select('SQL_CALC_FOUND_ROWS  moduleinfo.moduleinfoid,moduleinfo.userroleid,moduleinfo.moduleid',false);
    	$this->db->from('moduleinfo');
    	$this->db->join('module','module.moduleid=moduleinfo.moduleid');
    	$this->db->where_not_in('moduleinfo.status',array(3,2,0,10));
    	$this->db->where('moduleinfo.userroleid',$roleid);
    	$result=$this->db->get();
    	foreach($result->result() as $row) {
    		$moduleids[] = $row->moduleid;
    	}
    	return $moduleids;
    }
	//Grid title information
	public function gridtitleinformationfetch($moduleid) {
		$title = '';
		$icon = '';
		$industryid = $this->industryid;
		$this->db->select('menuname,modulename,moduleicon');
		$this->db->from('module');
		$this->db->where("FIND_IN_SET('$industryid',module.industryid) >", 0);
		$this->db->where_in('module.moduleid',$moduleid);
		$this->db->where('module.status',1);
		$qyresult = $this->db->get();
		foreach($qyresult->result() as $rows) {
			if($rows->menuname != "") {
				$title = ucwords($rows->menuname);
			} else {
				$title = ucwords($rows->modulename);
			}
			$icon = $rows->moduleicon;
		}
		$data = array('title'=>$title,'titleicon'=>$icon);
		return $data;
	}
    //Main Navigation Menu generation
    public function usermenugeneration($parentid=1) {
        $modulearray = array();
		$userroleid = $this->userroleid;
		$industryid = $this->industryid;
		$device = $this->deviceinfo();
        $i=0;
		$this->db->select("module.moduleid,moduleinfo.userroleid,module.modulename,module.menuname,module.modulelink,module.menutype,module.moduleicon,module.parentmoduleid,module.modulecategorytypeid");
        $this->db->from("moduleinfo");
        $this->db->join("module","module.moduleid=moduleinfo.moduleid");
        $this->db->where("FIND_IN_SET('$userroleid',moduleinfo.userroleid) >", 0);
        $this->db->where("FIND_IN_SET('$industryid',module.industryid) >", 0);
        $this->db->where('module.status',1);
        $this->db->where('module.menuviewtype',1);
        $this->db->where('moduleinfo.status',1);
        $this->db->where('module.parentmoduleid',$parentid);
        if($device=='phone') {
        	$this->db->where_in('module.modulestatus',array(1));
        }
        $this->db->group_by('moduleinfo.moduleid');
        $this->db->order_by('module.sortorder','asc');
        $querys = $this->db->get();
        foreach($querys->result() as $rows) {
			$name = ( ($rows->menuname != "")? $rows->menuname : $rows->modulename);
			$modulearray[$i] = array('parentnameid'=>$rows->parentmoduleid,'parentname'=>$rows->menuname,'module'=>ucwords($name),'link'=>$rows->modulelink,'mtype'=>$rows->menutype,'modid'=>$rows->moduleid,'modicon'=>$rows->moduleicon,'typeid'=>$rows->modulecategorytypeid);
			$i++;
        }
        return $modulearray;
    }
    //Module tab group generation
    public function moduuletabgroupgeneration($moduleid) {
		$userroleid = $this->userroleid;
		$industryid = $this->industryid;
		$i=0;
		$moduletabgrparray = array();
		$this->db->select('moduletabgroup.moduletabgroupid,moduletabgroup.moduleid,moduletabgroup.moduletabgroupname,moduletabgroup.usermoduletabgroupname,moduletabgroup.moduletabgrouptemplateid');
		$this->db->from('moduletabgroup');
		$this->db->join('module','module.moduleid=moduletabgroup.moduleid');
		$this->db->where_in('moduletabgroup.moduleid',$moduleid);
		$this->db->where("FIND_IN_SET('$userroleid',moduletabgroup.userroleid) >", 0);
		$this->db->where("FIND_IN_SET('$industryid',module.industryid) >", 0);
		$this->db->where('module.status',1);
		$this->db->where('moduletabgroup.status',1);
		$this->db->order_by('moduletabgroup.sequence','asc');
		$querytbg = $this->db->get();
		foreach($querytbg->result() as $rows) {
			$moduletabgrparray[$i] = array('tabpgrpid'=>$rows->moduletabgroupid,'tabgrpname'=>$rows->moduletabgroupname,'usertabgrpname'=>$rows->usermoduletabgroupname,'moduleid'=>$rows->moduleid,'templateid'=>$rows->moduletabgrouptemplateid);
			$i++;
        }
		return $moduletabgrparray;
    }
	//Dashboard tab group generation
    public function moddashboardtabgrpgeneration($moduleid) {
    	//$modid = implode(',',$moduleid);
		$userroleid = $this->userroleid;
		$i=0;
		$moddashboardtabgrparray = array();
		$dashboardmodid = array();
		$dbdata = $this->db->select('moduledashboardid')->from('module')->where_in('moduleid',$moduleid)->where('status',1)->get();
		foreach($dbdata->result() as $datarow) {
			$dbdataid = $datarow->moduledashboardid;
			$dashboardmodid = explode(',',$dbdataid);
			foreach($dashboardmodid as $ids) {
				$this->db->select('module.moduleid,module.modulename');
				$this->db->from('module');
				$this->db->join('moduleinfo','moduleinfo.moduleid=module.moduleid');
				$this->db->where("module.moduleid", $ids);
				$this->db->where("FIND_IN_SET('$userroleid',moduleinfo.userroleid) >", 0);
				$querytbg = $this->db->get();
				foreach($querytbg->result() as $rows) {
					$moddashboardtabgrparray[$i] = array('tabpgrpid'=>$rows->moduleid,'tabgrpname'=>ucwords($rows->modulename));	
					$i++;
				}
			}
		}
		return $moddashboardtabgrparray;
    }
	//Module tab section and fields generation based on tab group mainform loading
    public function moduletabsecfieldsgeneration($moduleid,$tabgroupid) {
		$userroleid = $this->userroleid;
        $i=0;
        $moduletabsecfldsgrparray = array();
        $this->db->select('modulefield.modulefieldid,modulefield.columnname,modulefield.tablename,modulefield.uitypeid,modulefield.fieldname,modulefield.fieldlabel,modulefield.readonly,modulefield.defaultvalue,modulefield.maximumlength,modulefield.mandatory,modulefield.active,modulefield.moduletabsectionid,modulefield.sequence,moduletabsection.moduletabsectionname,moduletabgroup.moduletabgroupid,moduletabgroup.moduletabgroupname,moduletabgroup.usermoduletabgroupname,modulefield.moduletabid,modulefield.multiple,modulefield.ddparenttable,modulefield.parenttable,modulefield.fieldcolumntype,module.modulename,module.modulelink,modulefield.decimalsize,moduletabsection.moduletabsectiontypeid,modulefield.fieldcustomvalidation,userrolemodulefiled.fieldactive,userrolemodulefiled.fieldreadonly,modulefield.unique',false);
		$this->db->from('modulefield');
		$this->db->join('uitype','uitype.uitypeid=modulefield.uitypeid');
		$this->db->join('userrolemodulefiled','userrolemodulefiled.modulefieldid=modulefield.modulefieldid','left outer');
		$this->db->join('moduletabsection','moduletabsection.moduletabsectionid=modulefield.moduletabsectionid');
		$this->db->join('moduletabgroup','moduletabgroup.moduletabgroupid=moduletabsection.moduletabgroupid');
		$this->db->join('module','module.moduleid=moduletabgroup.moduleid');
		$this->db->where("moduletabgroup.moduletabgroupid", $tabgroupid);
		$this->db->where("modulefield.moduletabid", $moduleid);
		$this->db->where("userrolemodulefiled.moduleid", $moduleid);
		$this->db->where("FIND_IN_SET('$userroleid',moduletabsection.userroleid) >", 0);
		$this->db->where("FIND_IN_SET('$userroleid',modulefield.userroleid) >", 0);
		$this->db->where('userrolemodulefiled.userroleid',$userroleid);
		$this->db->where_in('modulefield.status',array(1,10));
		$this->db->where('userrolemodulefiled.status',1);
		$this->db->where('moduletabsection.moduletabsectiontypeid',1);
		$this->db->where('moduletabsection.status',1);
		$this->db->order_by('moduletabsection.sequence','asc');
		$this->db->order_by('modulefield.sequence','asc');
		$this->db->group_by('modulefield.modulefieldid');
		$querytbg = $this->db->get();
		foreach($querytbg->result() as $rows) {
			$moduletabsecfldsgrparray[$i] = array('colname'=>$rows->columnname,'tabname'=>$rows->tablename,'uitypeid'=>$rows->uitypeid,'fieldname'=>$rows->fieldname,'filedlabel'=>$rows->fieldlabel,'readmode'=>$rows->fieldreadonly,'defvalue'=>$rows->defaultvalue,'maxlength'=>$rows->maximumlength,'mandmode'=>$rows->mandatory,'fieldmode'=>$rows->fieldactive,'tabsectionid'=>$rows->moduletabsectionid,'fieldseq'=>$rows->sequence,'tabsectionname'=>$rows->moduletabsectionname,'modtabgrpid'=>$rows->moduletabgroupid,'moduleid'=>$rows->moduletabid,'modulename'=>$rows->modulename,'multiple'=>$rows->multiple,'ddparenttable'=>$rows->ddparenttable,'parenttable'=>$rows->parenttable,'fieldcolmtypeid'=>$rows->fieldcolumntype,'modlink'=>$rows->modulelink,'decisize'=>$rows->decimalsize,'sectiontypeid'=>$rows->moduletabsectiontypeid,'customrule'=>$rows->fieldcustomvalidation,'tabgrpname'=>$rows->moduletabgroupname,'usertabgrpname'=>$rows->usermoduletabgroupname,'modfieldid'=>$rows->modulefieldid,'modfieldunique'=>$rows->unique);
			$i++;
		}
        return $moduletabsecfldsgrparray;
    }
	//summary
	public function moduletabsecfieldfetchsummary($moduleid,$tabgroupid) {
		$userroleid = $this->userroleid;
        $i=0;
        $moduletabsecfldsgrparray = array();
		$this->db->select('modulefield.modulefieldid,modulefield.columnname,modulefield.tablename,modulefield.uitypeid,modulefield.fieldname,modulefield.fieldlabel,modulefield.readonly,modulefield.defaultvalue,modulefield.maximumlength,modulefield.mandatory,modulefield.active,modulefield.moduletabsectionid,modulefield.sequence,moduletabsection.moduletabsectionname,moduletabgroup.moduletabgroupid,modulefield.moduletabid,modulefield.multiple,modulefield.ddparenttable,modulefield.parenttable,modulefield.fieldcolumntype,module.modulename,module.modulelink,modulefield.decimalsize,moduletabsection.moduletabsectiontypeid,modulefield.fieldcustomvalidation,userrolemodulefiled.fieldactive,userrolemodulefiled.fieldreadonly,modulefield.unique',false);
		$this->db->from('modulefield');
		$this->db->join('uitype','uitype.uitypeid=modulefield.uitypeid');
		$this->db->join('userrolemodulefiled','userrolemodulefiled.modulefieldid=modulefield.modulefieldid','left outer');
		$this->db->join('moduletabsection','moduletabsection.moduletabsectionid=modulefield.moduletabsectionid');
		$this->db->join('moduletabgroup','moduletabgroup.moduletabgroupid=moduletabsection.moduletabgroupid');
		$this->db->join('module','module.moduleid=moduletabgroup.moduleid');
		$this->db->where("moduletabgroup.moduletabgroupid", $tabgroupid);
		$this->db->where("modulefield.moduletabid", $moduleid);
		$this->db->where("userrolemodulefiled.moduleid", $moduleid);
		$this->db->where("FIND_IN_SET('$userroleid',moduletabsection.userroleid) >", 0);
		$this->db->where("FIND_IN_SET('$userroleid',modulefield.userroleid) >", 0);
		$this->db->where('userrolemodulefiled.userroleid',$userroleid);
		$this->db->where_in('modulefield.status',array(1,10));
		$this->db->where('userrolemodulefiled.status',1);
		$this->db->where('moduletabsection.moduletabsectiontypeid',2);
		$this->db->where('moduletabsection.status',1);
		$this->db->order_by('moduletabsection.sequence','asc');
		$this->db->order_by('modulefield.sequence','asc');
		$this->db->group_by('modulefield.modulefieldid');
		$querytbg = $this->db->get();
		foreach($querytbg->result() as $rows) {
			$moduletabsecfldsgrparray[$i] = array('colname'=>$rows->columnname,'tabname'=>$rows->tablename,'uitypeid'=>$rows->uitypeid,'fieldname'=>$rows->fieldname,'filedlabel'=>$rows->fieldlabel,'readmode'=>$rows->fieldreadonly,'defvalue'=>$rows->defaultvalue,'maxlength'=>$rows->maximumlength,'mandmode'=>$rows->mandatory,'fieldmode'=>$rows->fieldactive,'tabsectionid'=>$rows->moduletabsectionid,'fieldseq'=>$rows->sequence,'tabsectionname'=>$rows->moduletabsectionname,'modtabgrpid'=>$rows->moduletabgroupid,'moduleid'=>$rows->moduletabid,'modulename'=>$rows->modulename,'multiple'=>$rows->multiple,'ddparenttable'=>$rows->ddparenttable,'parenttable'=>$rows->parenttable,'fieldcolmtypeid'=>$rows->fieldcolumntype,'modlink'=>$rows->modulelink,'decisize'=>$rows->decimalsize,'sectiontypeid'=>$rows->moduletabsectiontypeid,'customrule'=>$rows->fieldcustomvalidation,'modfieldid'=>$rows->modulefieldid,'modfieldunique'=>$rows->unique);
			$i++;
		}
        return $moduletabsecfldsgrparray;
    }
   //Module tab group section and fields generation
    public function moduuletabgrpsecfieldsgeneration($moduleid) {
		$userroleid = $this->userroleid;
		$industryid = $this->industryid;
        $i=0;
        $moduletabsecfldsgrparray = array();
		$this->db->select('modulefield.modulefieldid,modulefield.columnname,modulefield.tablename,modulefield.uitypeid,modulefield.fieldname,modulefield.fieldlabel,modulefield.readonly,modulefield.defaultvalue,modulefield.maximumlength,modulefield.mandatory,modulefield.active,modulefield.moduletabsectionid,modulefield.sequence,moduletabsection.moduletabsectionname,moduletabgroup.moduletabgroupid,modulefield.moduletabid,modulefield.multiple,modulefield.ddparenttable,modulefield.parenttable,modulefield.fieldcolumntype,module.modulename,module.modulelink,modulefield.decimalsize,moduletabsection.moduletabsectiontypeid,modulefield.fieldcustomvalidation,userrolemodulefiled.fieldactive,userrolemodulefiled.fieldreadonly,modulefield.unique,module.menuname',false);
		$this->db->from('modulefield');
		$this->db->join('uitype','uitype.uitypeid=modulefield.uitypeid');
		$this->db->join('userrolemodulefiled','userrolemodulefiled.modulefieldid=modulefield.modulefieldid','left outer');
		$this->db->join('moduletabsection','moduletabsection.moduletabsectionid=modulefield.moduletabsectionid');
		$this->db->join('moduletabgroup','moduletabgroup.moduletabgroupid=moduletabsection.moduletabgroupid');
		$this->db->join('module','module.moduleid=moduletabgroup.moduleid');
		$this->db->where_in("modulefield.moduletabid", $moduleid);
		$this->db->where_in("userrolemodulefiled.moduleid", $moduleid);
		$this->db->where("FIND_IN_SET('$userroleid',moduletabgroup.userroleid)>",0);
		$this->db->where("FIND_IN_SET('$userroleid',moduletabsection.userroleid)>",0);
		$this->db->where("FIND_IN_SET('$userroleid',modulefield.userroleid)>",0);
		$this->db->where("FIND_IN_SET('$industryid',module.industryid)>",0);
		$this->db->where('userrolemodulefiled.userroleid',$userroleid);
		$this->db->where_in('modulefield.status',array(1,10));
		$this->db->where('userrolemodulefiled.status',1);
		$this->db->where('moduletabsection.status',1);
		$this->db->where('moduletabgroup.status',1);
		$this->db->where('module.status',1);
		$this->db->order_by('moduletabgroup.sequence','asc');
		$this->db->order_by('moduletabsection.sequence','asc');
		$this->db->order_by('modulefield.sequence','asc');
		$this->db->group_by('modulefield.modulefieldid');
		$querytbg = $this->db->get();
		foreach($querytbg->result() as $rows) {
			$moduletabsecfldsgrparray[$i] = array('colname'=>$rows->columnname,'tabname'=>$rows->tablename,'uitypeid'=>$rows->uitypeid,'fieldname'=>$rows->fieldname,'filedlabel'=>$rows->fieldlabel,'readmode'=>$rows->fieldreadonly,'defvalue'=>$rows->defaultvalue,'maxlength'=>$rows->maximumlength,'mandmode'=>$rows->mandatory,'fieldmode'=>$rows->fieldactive,'tabsectionid'=>$rows->moduletabsectionid,'fieldseq'=>$rows->sequence,'tabsectionname'=>$rows->moduletabsectionname,'modtabgrpid'=>$rows->moduletabgroupid,'moduleid'=>$rows->moduletabid,'modulename'=>$rows->modulename,'multiple'=>$rows->multiple,'ddparenttable'=>$rows->ddparenttable,'parenttable'=>$rows->parenttable,'fieldcolmtypeid'=>$rows->fieldcolumntype,'modlink'=>$rows->modulelink,'decisize'=>$rows->decimalsize,'sectiontypeid'=>$rows->moduletabsectiontypeid,'customrule'=>$rows->fieldcustomvalidation,'modfieldid'=>$rows->modulefieldid,'modfieldunique'=>$rows->unique,'menuname'=>$rows->menuname);
			$i++;
		}
        return $moduletabsecfldsgrparray;
    }
	//Dash board Module tab group section and fields generation
    public function dashboardmodsecfieldsgeneration($moduleid) {
    	//$modid = implode(',',$moduleid);
		$userroleid = $this->userroleid;
        $i=0;
        $moduletabsecfldsgrparray = array();
		$dashboardmodid = array();
		$dbdata = $this->db->select('module.moduledashboardid')->from('module')->join('moduleinfo','moduleinfo.moduleid=module.moduleid')->where("FIND_IN_SET('$userroleid',moduleinfo.userroleid) >", 0)->where_in('module.moduleid',$moduleid)->where('moduleinfo.status',1)->get();
		foreach($dbdata->result() as $datarow) {
			$dbdataid = $datarow->moduledashboardid;
			$dashboardmodid = explode(',',$dbdataid);
			foreach($dashboardmodid as $ids) {
				$chk = $this->dashboardmodulecheckinfo($ids,$userroleid);
				if($chk=='True') {
					$this->db->select('modulefield.modulefieldid,modulefield.columnname,modulefield.tablename,modulefield.uitypeid,modulefield.fieldname,modulefield.fieldlabel,modulefield.readonly,modulefield.defaultvalue,modulefield.maximumlength,modulefield.mandatory,modulefield.active,modulefield.moduletabsectionid,modulefield.sequence,moduletabsection.moduletabsectionname,moduletabgroup.moduletabgroupid,moduletabgroup.moduletabgroupname,modulefield.moduletabid,modulefield.multiple,modulefield.ddparenttable,modulefield.parenttable,modulefield.fieldcolumntype,module.modulename,module.modulelink,modulefield.decimalsize,moduletabsection.moduletabsectiontypeid,modulefield.fieldcustomvalidation,userrolemodulefiled.fieldactive,userrolemodulefiled.fieldreadonly,modulefield.unique',false);
					$this->db->from('modulefield');
					$this->db->join('uitype','uitype.uitypeid=modulefield.uitypeid');
					$this->db->join('userrolemodulefiled','userrolemodulefiled.modulefieldid=modulefield.modulefieldid','left outer');
					$this->db->join('moduletabsection','moduletabsection.moduletabsectionid=modulefield.moduletabsectionid');
					$this->db->join('moduletabgroup','moduletabgroup.moduletabgroupid=moduletabsection.moduletabgroupid');
					$this->db->join('module','module.moduleid=moduletabgroup.moduleid');
					$this->db->join('moduleinfo','moduleinfo.moduleid=module.moduleid');
					$this->db->where("modulefield.moduletabid",$ids);
					$this->db->where("userrolemodulefiled.moduleid",$ids);
					$this->db->where("module.moduleid",$ids);
					$this->db->where("moduleinfo.moduleid",$ids);
					$this->db->where("FIND_IN_SET('$userroleid',moduletabgroup.userroleid) >", 0);
					$this->db->where("FIND_IN_SET('$userroleid',moduletabsection.userroleid) >", 0);
					$this->db->where("FIND_IN_SET('$userroleid',modulefield.userroleid) >", 0);
					$this->db->where('userrolemodulefiled.userroleid',$userroleid);
					$this->db->where_in('modulefield.status',array(1,10));
					$this->db->where('userrolemodulefiled.status',1);
					$this->db->where('moduletabsection.status',1);
					$this->db->where('moduletabgroup.status',1);
					$this->db->order_by('moduletabgroup.sequence','asc');
					$this->db->order_by('moduletabsection.sequence','asc');
					$this->db->order_by('modulefield.sequence','asc');
					$this->db->group_by('modulefield.modulefieldid');
					$querytbg = $this->db->get();
					foreach($querytbg->result() as $rows) {
						$moduletabsecfldsgrparray[$i] = array('colname'=>$rows->columnname,'tabname'=>$rows->tablename,'uitypeid'=>$rows->uitypeid,'fieldname'=>$rows->fieldname,'filedlabel'=>$rows->fieldlabel,'readmode'=>$rows->fieldreadonly,'defvalue'=>$rows->defaultvalue,'maxlength'=>$rows->maximumlength,'mandmode'=>$rows->mandatory,'fieldmode'=>$rows->fieldactive,'tabsectionid'=>$rows->moduletabsectionid,'fieldseq'=>$rows->sequence,'tabsectionname'=>$rows->moduletabsectionname,'tabgroupname'=>$rows->moduletabgroupname,'modtabgrpid'=>$rows->moduletabgroupid,'moduleid'=>$rows->moduletabid,'modulename'=>$rows->modulename,'multiple'=>$rows->multiple,'ddparenttable'=>$rows->ddparenttable,'parenttable'=>$rows->parenttable,'fieldcolmtypeid'=>$rows->fieldcolumntype,'modlink'=>$rows->modulelink,'decisize'=>$rows->decimalsize,'sectiontypeid'=>$rows->moduletabsectiontypeid,'customrule'=>$rows->fieldcustomvalidation,'modfieldid'=>$rows->modulefieldid,'modfieldunique'=>$rows->unique);
						$i++;
					}
				}
			}
		}
        return $moduletabsecfldsgrparray;
    }
	//check the module is enable
	public function dashboardmodulecheckinfo($moduleid,$roleid) {
		$this->db->select('moduleid');
		$this->db->from('moduleinfo');
		$this->db->where('moduleinfo.moduleid',$moduleid);
		$this->db->where('moduleinfo.userroleid',$roleid);
		$this->db->where('moduleinfo.status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			return 'True';
		} else {
			return 'False';
		}
	}
	//table field name check
	public function tablefieldnamecheck($tblname,$fieldname) {
		$result = 'false';
		$fields = $this->db->field_data($tblname);
		foreach ($fields as $field) {
			$name =  $field->name;
			if($fieldname == $name) {
				$result = 'true';
			}
		}
		return $result;
	}
    //dynamic drop down generation value fetch
    public function dynamicdropdownvalfetch($moduleid,$tablename,$tabfieldname,$fieldprimaryid) {
    	$industryid = $this->industryid;
		$mnamechk = $this->tablefieldnamecheck($tablename,'moduleid');
		$unamechk = $this->tablefieldnamecheck($tablename,'userroleid');
		$sonamechk = $this->tablefieldnamecheck($tablename,'sortorder');
		$userroleid = $this->userroleid;
        $dropdowndata = array();
        $this->db->select(''.$fieldprimaryid.' AS Id'.','.''.$tabfieldname.' AS Name ,setdefault AS defaultopt');
        $this->db->from($tablename);
		if($mnamechk == "true") {
			$this->db->where("FIND_IN_SET('".$moduleid."',moduleid) >", 0);
		}
		if($unamechk == "true") {
			$this->db->where("FIND_IN_SET('".$userroleid."',userroleid) >", 0);
		}
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->where(''.$tablename.'.status', 1);
		if($sonamechk=="true") {
			$this->db->order_by('sortorder','asc');
		} else {
			$this->db->order_by($tabfieldname,'asc');
		}
        $query= $this->db->get();
        return $query->result();
    }
    //dynamic group drop down generation value fetch
    public function dynamicgroupdropdownvalfetch($moduleid,$tablename1,$tablename2,$tabfieldname) {
    	$sonamechk = $this->tablefieldnamecheck($tablename1,'sortorder');
		$userroleid = $this->userroleid;
		$industryid = $this->industryid;
		$fieldprimaryid = $tablename1.'id';
		$parentprimaryid = $tablename2.'id';
		$parentprimaryname = $tablename2.'name';
		$join = ''.$tablename2.'.'.$parentprimaryid.'='.$tablename1.'.'.$parentprimaryid.'';
        $this->db->select(''.$tablename1.'.'.$fieldprimaryid.' AS CId'.','.''.$tablename1.'.'.$tabfieldname.' AS CName ,'.$tablename1.'.setdefault AS defaultopt,'.$tablename2.'.'.$parentprimaryid.' AS PId'.','.''.$tablename2.'.'.$parentprimaryname.' AS PName');
        $this->db->from($tablename1);
		$this->db->join($tablename2,$join);
        $this->db->where("FIND_IN_SET('".$moduleid."',".$tablename1.".moduleid) >", 0);
        $this->db->where("FIND_IN_SET('".$userroleid."',".$tablename1.".userroleid) >", 0); 
		$this->db->where("FIND_IN_SET('".$moduleid."',".$tablename2.".moduleid) >", 0);
        $this->db->where("FIND_IN_SET('".$userroleid."',".$tablename2.".userroleid) >", 0);
        $this->db->where("FIND_IN_SET('".$industryid."',".$tablename1.".industryid) >", 0);
		$this->db->where(''.$tablename1.'.status', 1);
    	if($sonamechk=="true") {
			$this->db->order_by(''.$tablename1.'.sortorder','asc');
		} else {
			$this->db->order_by(''.$tablename1.'.'.$tabfieldname.'','asc');
		}
        $query= $this->db->get();
        return $query->result();
    }
	//parent dynamic drop down generation value fetch
    public function parentdynamicgroupdropdownvalfetch($moduleid,$tablename1,$tablename2,$tabfieldname) {
		$userroleid = $this->userroleid;
		$industryid = $this->industryid;
		$fieldprimaryid = $tablename1.'id';
		$parentprimaryid = $tablename2.'id';
		$parentprimaryname = $tablename2.'name';
		$join = ''.$tablename2.'.'.$parentprimaryid.'='.$tablename1.'.'.$parentprimaryid.'';
        $this->db->select(''.$tablename2.'.'.$parentprimaryid.' AS PId'.','.''.$tablename2.'.'.$parentprimaryname.' AS PName,'.$fieldprimaryid.' AS CId'.','.''.$tabfieldname.' AS CName');
        $this->db->from($tablename1);
		$this->db->join($tablename2,$join);
		$this->db->where(''.$tablename1.'.status', 1);
		$this->db->where(''.$tablename2.'.status', 1);
		$this->db->where("FIND_IN_SET('".$industryid."',".$tablename1.".industryid) >", 0);
        $this->db->order_by(''.$tablename2.'.'.$parentprimaryname.'','asc');
        $this->db->order_by(''.$tablename1.'.'.$tabfieldname.'','asc');
        $query= $this->db->get();
        return $query->result();
    }
	//dynamic drop down generation value fetch for parent
    public function dynamicdropdownvalfetchparent($tablename,$tabfieldname,$fieldprimaryid,$default) {
		$level = "";
		$industryid = $this->industryid;
		if( isset($default) && $default!='null' && $default!='') {
			$level = $default;
		}
		if( ($tablename=='lead' && $tabfieldname=='leadname') || ($tablename=='lead' && $tabfieldname=='lastname') || ($tablename=='contact' && $tabfieldname=='contactname') || ($tablename=='contact' && $tabfieldname=='lastname') ) {
			$this->db->select(''.$fieldprimaryid.' AS Id'.','.''.$tabfieldname.' AS Name,salutation.salutationname AS SName,lastname AS LName');
			$this->db->from($tablename);
			$this->db->join('salutation','salutation.salutationid='.$tablename.'.salutationid','left outer');
			if($level != "") {
				$this->db->where(''.$tablename.'.'.$tablename.'level <=',$default);
			}
			$this->db->where("FIND_IN_SET('".$industryid."',".$tablename.".industryid) >", 0);
			$this->db->where(''.$tablename.'.status', 1);
			$this->db->order_by(''.$tablename.'.'.$tabfieldname.'','asc');
		}  else if($tablename=="currency") {
			$this->db->select(''.$fieldprimaryid.' AS Id'.','.'currencycode AS Name,currencycountry As CName');
			$this->db->from($tablename);
			if($level != "") {
				$this->db->where(''.$tablename.'.'.$tablename.'level <=',$default);
			}
			$this->db->where("FIND_IN_SET('".$industryid."',".$tablename.".industryid) >", 0);
			$this->db->where(''.$tablename.'.status', 1);
			$this->db->order_by(''.$tablename.'.currencycountry','asc');
		}  else if($tablename == "vdraccount") {
			$tablename = str_replace("vdr","",$tablename);
			$fieldprimaryid = str_replace("vdr","",$fieldprimaryid);
			$tabfieldname = str_replace("vdr","",$tabfieldname);
			$this->db->select(''.$fieldprimaryid.' AS Id'.','.''.$tabfieldname.' AS Name');
			$this->db->from($tablename);
			if($level != "") {
				$this->db->where(''.$tablename.'.'.$tablename.'level <=',$default);
			}
			$this->db->where("FIND_IN_SET('".$industryid."',".$tablename.".industryid) >", 0);
			$this->db->where(''.$tablename.'.accounttypeid', '16');
			$this->db->where(''.$tablename.'.status', 1);
			$this->db->order_by(''.$tablename.'.'.$tabfieldname.'','asc');
		} else {
			$this->db->select(''.$fieldprimaryid.' AS Id'.','.''.$tabfieldname.' AS Name');
			$this->db->from($tablename);
			if($level != "") {
				$this->db->where(''.$tablename.'.'.$tablename.'level <=',$default);
			}
			$this->db->where("FIND_IN_SET('".$industryid."',".$tablename.".industryid) >", 0);
			$this->db->where(''.$tablename.'.status', 1);
			$this->db->order_by(''.$tablename.'.'.$tabfieldname.'','asc');
		}
        $query= $this->db->get();
        return $query->result();
    }
	//dynamic drop down generation value fetch with specific condition
    public function dynamicdropdownvalfetchwithusercond($tablename,$tabfieldname,$fieldprimaryid,$condid,$condvalue) {
		$userroleid = $this->userroleid;
		$industryid = $this->industryid;
        $dropdowndata = array();
        $this->db->select(''.$fieldprimaryid.' AS Id'.','.''.$tabfieldname.' AS Name,setdefault AS defaultopt');
        $this->db->from($tablename);
        if (preg_match('/\b,\b/',$condvalue)){
        	$condidarray = explode(',',$condid);
        	$condvaluearray = explode(',',$condvalue);
        	$this->db->where("FIND_IN_SET('".$condvaluearray[0]."',".$condidarray[0].") >", 0);
        	$this->db->where("FIND_IN_SET('".$condvaluearray[1]."',".$condidarray[1].") >", 0);
        }else{
           $this->db->where("FIND_IN_SET('".$condvalue."',".$condid.") >", 0);
        }
        $this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->where(''.$tablename.'.status', 1);
		$this->db->order_by(''.$tablename.'.'.$tabfieldname.'','asc');
        $query= $this->db->get();
        return $query->result();
    }
	//dynamic drop down generation value fetch with condition (Relation component)
    public function dynamicdropdownvalfetchwithcond($tablename,$tabfieldname,$fieldprimaryid,$conddata) {
    	$condinfo = explode('|',$conddata);
    	$whcond = "1=1";
    	$join="";
    	$wcond="";
    	$partable =  $condinfo[0];
    	$tabfieldname = $condinfo[1];
    	$tabname = $condinfo[2];
    	$uitypeid = $condinfo[3];
    	$modid = $this->generalinformaion('modulefield','moduletabid','parenttable',$partable);
    	$moduleid = ( ($modid!=0)? $modid : 1 );
    	//conditions datas
    	$condfname = $condinfo[4];
    	$condtablname = $condinfo[5];
    	$conduitypeid = $condinfo[6];
    	$condname = $condinfo[7];
    	$condvale = $condinfo[8];
    	if( $partable != '' && $tabfieldname != '' && $tabname != '' && $uitypeid != '' ) {
    		if($uitypeid == 17 || $uitypeid == 18 || $uitypeid == 19 || $uitypeid == 20 || $uitypeid == 25 || $uitypeid == 26 || $uitypeid == 28 || $uitypeid == 29) {
    			$ftablename = substr($tabfieldname,0,-2);
    			$ftabfildname = $ftablename.'name';
    			$ftabid = $ftablename.'id';
    			$tabname = $ftablename;
    			if($condfname!='' && $condname!='' && $condvale!='') {
    				if($conduitypeid == 17 || $conduitypeid == 18 || $conduitypeid == 19 || $conduitypeid == 20 || $conduitypeid == 25 || $conduitypeid == 26) {
    					$condfldtablname = substr($condfname,0,-2);
    					$condfldtabid = $condfldtablname.'id';
    					$condfldname = $condfldtablname.'name';
    					if($ftablename!=$tabname) {
    						$join = $join.' LEFT OUTER JOIN '.$tabname.' ON '.$tabname.'.'.$ftabid.'='.$ftablename.'.'.$ftabid;
    					}
    					if($condtablname!=$tabname) {
    						$tabid = $tabname.'id';
    						$join = $join.' LEFT OUTER JOIN '.$condtablname.' ON '.$condtablname.'.'.$tabid.'='.$tabname.'.'.$tabid;
    						if($condfname!=$tabfieldname) {
    							$join = $join.' LEFT OUTER JOIN '.$condfldtablname.' ON '.$condfldtablname.'.'.$condfldtabid.'='.$condtablname.'.'.$condfldtabid;
    						}
    						$conditionvalarray = array( 0 => array($condfldtablname,$condfldname,$condname,$condvale,'AND') );
    						$wcond = $this->whereclausegeneration($conditionvalarray);
    					} else {
    						if($condfname!=$tabfieldname) {
    							$join = $join.' LEFT OUTER JOIN '.$condfldtablname.' ON '.$condfldtablname.'.'.$condfldtabid.'='.$condtablname.'.'.$condfldtabid;
    						}
    						$conditionvalarray = array( 0 => array($condfldtablname,$condfldname,$condname,$condvale,'AND') );
    						$wcond = $this->whereclausegeneration($conditionvalarray);
    					}
    				} else {
    					if($condtablname!=$tabname) {
    						$tabid = $tabname.'id';
    						$join = $join.' LEFT OUTER JOIN '.$condtablname.' ON '.$condtablname.'.'.$tabid.'='.$tabname.'.'.$tabid;
    					}
    					$conditionvalarray = array( 0=>array($condtablname,$condfname,$condname,$condvale,'AND') );
    					$wcond = $this->whereclausegeneration($conditionvalarray);
    				}
    			} else {
    				if($ftablename!=$tabname) {
    					$join = $join.' LEFT OUTER JOIN '.$tabname.' ON '.$tabname.'.'.$ftabid.'='.$ftablename.'.'.$ftabid;
    				}
    			}
    			$mnamechk = $this->tablefieldnamecheck($ftablename,'moduleid');
    			$modcond = ( ($mnamechk == "true") ? 'AND '.$ftablename.'.moduleid='.$moduleid : '');
    			$query = $this->db->query('select '.$ftablename.'.'.$ftabid.' AS Id'.','.$ftablename.'.'.$ftabfildname.' AS Name from '.$ftablename.''.$join.' where '.$whcond.' '.$wcond.''.$modcond.' AND '.$ftablename.'.status NOT IN (0,3) ORDER BY '.$ftablename.'.'.$ftabfildname.'');
    			return $query->result();
    		} else {
    			if($condfname!='' && $condname!='' && $condvale!='') {
    				if($conduitypeid == 17 || $conduitypeid == 18 || $conduitypeid == 19 || $conduitypeid == 20 || $conduitypeid == 25 || $conduitypeid == 26) {
    					$condfldtablname = substr($condfname,0,-2);
    					$condfldtabid = $condfldtablname.'id';
    					$condfldname = $condfldtablname.'name';    					
    					if($condtablname!=$tabname) {
    						$tabid = $tabname.'id';
    						$join = $join.' LEFT OUTER JOIN '.$condtablname.' ON '.$condtablname.'.'.$tabid.'='.$tabname.'.'.$tabid;
    						if($condfname!=$tabfieldname) {
    							$join = $join.' LEFT OUTER JOIN '.$condfname.' ON '.$condfldtablname.'.'.$condfldtabid.'='.$condtablname.'.'.$condfldtabid;
    						}
    						$conditionvalarray = array( 0 => array($condfldtablname,$condfldname,$condname,$condvale,'AND') );
    						$wcond = $this->whereclausegeneration($conditionvalarray);
    					} else {
    						if($condfname!=$tabfieldname) {
    							$join = $join.' LEFT OUTER JOIN '.$condfldtablname.' ON '.$condfldtablname.'.'.$condfldtabid.'='.$condtablname.'.'.$condfldtabid;
    							$conditionvalarray = array( 0 => array($condfldtablname,$condfldname,$condname,$condvale,'AND') );
    							$wcond = $this->whereclausegeneration($conditionvalarray);
    						}    						
    					}
    				} else {
    					if($condtablname!=$tabname) {
    						$tabid = $tabname.'id';
    						$join = $join.' LEFT OUTER JOIN '.$condtablname.' ON '.$condtablname.'.'.$tabid.'='.$tabname.'.'.$tabid;      						
    					}
    					$conditionvalarray = array( 0=>array($condtablname,$condfname,$condname,$condvale,'AND') );
    					$wcond = $this->whereclausegeneration($conditionvalarray);    					
    				}
    			}
    			$mnamechk = $this->tablefieldnamecheck($partable,'moduleid');
    			$modcond = ( ($mnamechk == "true") ? 'AND '.$partable.'.moduleid='.$moduleid : '');
    			$tabid = $tabname.'id'; 
    			$query = $this->db->query( 'select '.$tabname.'.'.$tabid.' AS Id'.','.$tabname.'.'.$tabfieldname.' AS Name from '.$tabname.' '.$join.' where '.$whcond.' '.$wcond.''.$modcond.' AND '.$tabname.'.status NOT IN (0,3) ORDER BY '.$tabname.'.'.$tabfieldname.'');
    			return $query->result();
    		}
    	}
    }
	//dynamic drop down generation value fetch
    public function userspecificgroupdropdownvalfetch() {
		$data = array();
		$industyid = $this->industryid;
		$i=0;
		$tablename = array('employee','employeegroup','userrole','userrole');
		$groupname = array('Users','Groups','Roles','Role and Subordinates');
		$groupkey = array('U','G','R','SR');
		$m=0;
		$pid = 1;
		foreach($tablename as $key => $tabname) {
			$name = $tabname.'name';
			$id = $tabname.'id';
			if($tabname=='employee') {
				$this->db->select(''.$id.' AS Id'.','.''.$name.' AS Name,lastname AS LName,salutationname AS SName');
				$this->db->from($tabname);
				$this->db->join('salutation','salutation.salutationid='.$tabname.'.salutationid','left outer');
				$this->db->where("FIND_IN_SET('".$industyid."',".$tabname.".industryid) >", 0);
				$this->db->where(''.$tabname.'.status',1);
				$this->db->order_by(''.$tabname.'.'.$name.'', 'ASC');
				$result=$this->db->get()->result();
				foreach($result as $row) {
					$sname = (($row->SName!='')? $row->SName.' ':'');
					$lname = (($row->LName!='')? ' '.$row->LName:'');
					$data[$i]=array('CId'=>$row->Id,'CName'=>$sname.$row->Name.$lname,'PId'=>$pid,'PName'=>$groupname[$m],'DDval'=>$groupkey[$m].'-'.$row->Id);
					$i++;
				}
			} else {
				$this->db->select(''.$id.' AS Id'.','.''.$name.' AS Name');
				$this->db->from($tabname);
				$this->db->where(''.$tabname.'.status', 1);
				$this->db->where("FIND_IN_SET('".$industyid."',".$tabname.".industryid) >", 0);
				$this->db->order_by(''.$tabname.'.'.$name.'', 'ASC');
				$result=$this->db->get()->result();
				foreach($result as $row) {
					$data[$i]=array('CId'=>$row->Id,'CName'=>$row->Name,'PId'=>$pid,'PName'=>$groupname[$m],'DDval'=>$groupkey[$m].'-'.$row->Id);
					$i++;
				}
			}
			$m++;
			$pid++;
		}
		return $data;
    }
	//module drop down show
    public function dynamicmoduledropdownvalfetch($moduleid) {
		$userroleid = $this->userroleid;
		$industryid = $this->industryid;
        $dropdowndata = array();
        $this->db->select('module.moduleid AS Id ,module.menuname AS Name');
        $this->db->from('module');
        $this->db->join('moduleinfo','moduleinfo.moduleid=module.moduleid');
        $this->db->where("FIND_IN_SET('".$moduleid."',moduleprivilegeid) >", 0);
        $this->db->where("FIND_IN_SET('".$userroleid."',moduleinfo.userroleid) >", 0);
        $this->db->where("FIND_IN_SET('".$industryid."',module.industryid) >", 0);
		$this->db->where_in('module.status', array(0,1));
        $this->db->order_by('sortorder','asc');
        $query= $this->db->get();
        return $query->result();
    }
	//tree create model
	public function treecreatemodel($tabname,$fieldtable = '') {
		$tname=explode("-",$tabname);
		$tablename = $tname[0];
		$id = $tablename.'id';
		$name = $tablename.'name';
		$industryid = $this->industryid;
		$pdataid = 'parent'.$tablename.'id';
		$level = $tablename.'level';		
		$data = array();
		$this->db->select( ''.$id.' AS Id'.','.''.$name.' AS Name'.','.$pdataid.' AS ParentId'.','.$level.' AS level' );
		$this->db->from($tablename);
		$this->db->where(''.$tablename.'.status',1);
		if(isset($_POST['fieldvalue']) and isset($_POST['fieldname'])){//special case ex:branch based tree
			$this->db->where($_POST['fieldname'],$_POST['fieldvalue']);
		}
		if(isset($tname[1])){//special case 
			$fname=explode(":",$tname[1]);
			$this->db->where($fname[0],$fname[1]);
		}
		if(isset($_POST['rowid'])){//special case ex:branch based tree
			if($_POST['rowid'] > 0){
				$this->db->where(''.$id.' !=',$_POST['rowid']);
				$this->db->where(''.$pdataid.' !=',$_POST['rowid']);
			}
		}
		$this->db->where("FIND_IN_SET('".$industryid."',".$tablename.".industryid) >", 0);
	    $this->db->order_by(''.$tablename.'.'.$name.'','ASC');
		$result = $this->db->get();		
		foreach($result->result() as $row) {
			$data[$row->Id] = array('id'=>$row->Id,'name'=>$row->Name,'pid'=>$row->ParentId,'level'=>$row->level);
		}		
		$itemsByParent = array();
		foreach ($data as $item) {
			if (!isset($itemsByParent[$item['pid']])) {
				$itemsByParent[$item['pid']] = array();
			}
			$itemsByParent[$item['pid']][] = $item;
		}	
		return $itemsByParent;		
	}
	//tree create model - for jewel industry
	public function jewel_treecreatemodel($tablename) {
		$id = $tablename.'id';
		$name = $tablename.'name';
		$pdataid = 'parent'.$tablename.'id';
		$level = $tablename.'level';
		$data = array();
		if($tablename == 'stonecategory') {
			$this->db->select( ''.$id.' AS Id'.','.''.$name.' AS Name'.','.$pdataid.' AS ParentId'.','.$level.' AS level'.','.'loosestone as lstchck');
		} else {
			$this->db->select( ''.$id.' AS Id'.','.''.$name.' AS Name'.','.$pdataid.' AS ParentId'.','.$level.' AS level' );
		}
		$this->db->from($tablename);
		$this->db->where(''.$tablename.'.status',1);
	    $this->db->order_by(''.$tablename.'.'.$id.'','ASC');
		$result = $this->db->get();		
		foreach($result->result() as $row) {
			if($tablename == 'stonecategory') {
				$data[$row->Id] = array('id'=>$row->Id,'name'=>$row->Name,'pid'=>$row->ParentId,'level'=>$row->level,'lstchck'=>$row->lstchck);
			} else {
				$data[$row->Id] = array('id'=>$row->Id,'name'=>$row->Name,'pid'=>$row->ParentId,'level'=>$row->level);
			}
		}
		$itemsByParent = array();
		foreach ($data as $item){
			if (!isset($itemsByParent[$item['pid']])) {
				$itemsByParent[$item['pid']] = array();
			}
			$itemsByParent[$item['pid']][] = $item;
		}		
		return $itemsByParent;
	}
	//tree creation
	function createTree($items = array(), $parentId = 0,$removeid = 0) {
		echo " <ul class='dl-submenu'> ";
		if($removeid == 0){
			echo '<li><a href="#" style="font-weight:bold;">Remove<i class="material-icons parentclear">close</i></a></li>';
		}
		if(count($items) > 0 ) {
			foreach ($items[$parentId] as $item) {
				if(isset($item['lstchck'])) {
					$item['lstchck'] = $item['lstchck'];
				} else {
					$item['lstchck'] = 'No';
				}
				echo '<li data-listname="'.$item['name'].'" data-level="'.$item['level'].'" data-listid="'.$item['id'].'" data-advfield="'.$item['lstchck'].'"><a href="#">'.$item['name'].'</a>';//level title
				$curId = $item['id'];
				//if there are children
				if (!empty($items[$curId])) {
					$this->createTree($items, $curId,1);
				}
				echo '</li>';
			}
		}
		echo '</ul>';
	}
	//tree creation
	function stocktakecreateTree($items = array(), $parentId = 0,$removeid = 0) {
		echo " <ul class='dl-submenu'> ";
		if($removeid == 0){
			echo '<li><a href="#" style="font-weight:bold;">Remove<i class="material-icons parentclear">close</i></a></li>';
		}
		if(count($items) > 0 ) {
			foreach ($items[$parentId] as $item) {
				echo '<li data-listname="'.$item['name'].'" data-level="'.$item['level'].'" data-listid="'.$item['id'].'" ><a href="#">'.$item['name'].'</a>';//level title
				$curId = $item['id'];
				//if there are children
				if (!empty($items[$curId])) {
					$this->createTree($items, $curId,1);
				}
				echo '</li>';
			}
		}
		echo '</ul>';
	}
	//drop down value set with multiple condition
	public function fetchdddatawithmultiplecondmodel() {
		$i=0;
		$dname=$_GET['dataname'];
		$did=$_GET['dataid'];
		$dataattrname=$_GET['othername'];
		$table=$_GET['datatab'];
		$whfield=$_GET['whdatafield'];
		$whdata=$_GET['whval'];
		$mulcond=explode(",",$whfield);
		$mulcondval=explode(",",$whdata);
		$i=0;
		$c=0;
		$this->db->select("$dname,$did,$dataattrname");
		$this->db->from($table);
		foreach($mulcond AS $condname) {
			$cond = explode('|',$mulcondval[$c]);
			$this->db->where_in($condname,$cond);
			$c++;
		}
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {		
			foreach($result->result()as $row) {
				$data[$i]=array('datasid'=>$row->$did,'dataname'=>$row->$dname,'otherdataattrname'=>$row->$dataattrname);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	public function attributedropdownvaluefetchmodel(){
		$attributeid = $_GET['fieldid'];
		$i= 0;
		$this->db->select('attributevalueid AS Id,attributevaluename AS Name');
        $this->db->from('attributevalue');
        $this->db->join("attribute","attribute.attributeid=attributevalue.attributeid");
        $this->db->where("FIND_IN_SET('".$attributeid."',attributevalue.attributeid) >", 0);
		$this->db->where('attributevalue.status', 1);
        $query= $this->db->get();
		if($query->num_rows() > 0){
			foreach($query->result() as $row){
				$data[$i]=array('Id'=>$row->Id,'Name'=>$row->Name);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//attribute set name fetch
	public function attributeviewdataddfetch($moduleid) {
		$data = array();
		$i=0;
		$industryid = $this->industryid;
		$this->db->select("attribute.attributename,attribute.attributeid");
		$this->db->from("attribute");
		$this->db->join("attributesetassign","attributesetassign.attributeid=attribute.attributeid");
		$this->db->join("attributeset","attributeset.attributesetid=attributesetassign.attributesetid");
		$this->db->join("moduleattributesetting","moduleattributesetting.attributesetid=attributeset.attributesetid");
		$this->db->where("moduleattributesetting.moduleid",$moduleid);
		$this->db->where("FIND_IN_SET('".$industryid."',attribute.industryid) >", 0);
		$this->db->where("moduleattributesetting.status",1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result()as $row) {
				$data[$i]=array('Id'=>$row->attributeid,'Name'=>$row->attributename);
				$i++;
			}
		}
		return $data;		
	}
	//attribute value fetch by using attribute name and id
	public function dynamicattributedropdownvalfetch($attributeid) {
        $dropdowndata = array();
        $industryid = $this->industryid;
        $this->db->select('attributevalueid AS Id,attributevaluename AS Name');
        $this->db->from('attributevalue');
        $this->db->join("attribute","attribute.attributeid=attributevalue.attributeid");
        $this->db->where("FIND_IN_SET('".$attributeid."',attributevalue.attributeid) >", 0);
        $this->db->where("FIND_IN_SET('".$industryid."',attributevalue.industryid) >", 0);
		$this->db->where('attributevalue.status', 1);
        $query= $this->db->get();
        return $query->result();
	}	
	//create new payment number 
	public function generatepaymentnumber() {
		$data=$this->db->query("SELECT MAX(paymentid) as paymentid FROM payment");
		foreach($data->result() as $info){
			$maxid=$info->paymentid + 1;
		}
		return $maxid;
	}
	//create new Quote/Invoice/Purchaseorder/salesorder numbers
	public function generatenumber($moduleid,$table) {
		$tableid=$table.'id';
		$fieldnumber=$table.'number';
		$data=$this->db->select('suffix,startingnumber,incrementby,random,prefix')->from('serialnumbermaster')->where('moduleid',$moduleid)->get();
		foreach($data->result() as $info) {
			$suffix=$info->suffix;
			$startnumber=$info->startingnumber;
			$incrementby=$info->incrementby;
			$random=$info->random;
			$prefix=$info->prefix;
		}
		if($random != 'Yes') {
			$datatow=$this->db->select_max($fieldnumber)->from($table)->where_not_in('status',3)->get();//->where_in('industryid',$this->industryid)
			if($datatow->num_rows() > 0) {
				foreach($datatow->result() as $next) {
					$num=$next->$fieldnumber;
				}
				for($k=0;$k<$incrementby;$k++) {
					$num++;
				}
			} else {
				$num=$suffix.$startnumber;
			}			
		} else {			
			//randomcall
			$num=$this->fetchrandomnumber($fieldnumber,$table,$tableid,$suffix,$prefix);
		}
		return $num	;	
	}
	//random number generation
	public function randomnumbergenerator($moduleid,$table,$filedname,$modulefieldid) {
		$num = "";
		$tableid=$table.'id';
		$data=$this->db->select('suffix,startingnumber,incrementby,random,prefix')->from('serialnumbermaster')->where('moduleid',$moduleid)->where('modulefieldid',$modulefieldid)->get();
		foreach($data->result() as $info) {
			$suffix=$info->suffix;
			$prefix=$info->prefix;
			$startnumber=$info->startingnumber;
			$incrementby=$info->incrementby;
			$random=$info->random;
		}
		$a=array('suffix'=>$suffix,'prefix'=>$prefix,'startnumber'=>$startnumber,'incrementby'=>$incrementby,'random'=>$random);	
		if($prefix == "") {
			$prefix=" ";
		}
		$maxnumber='';
		if($random != 'Yes') { // serial number
			if($prefix == " ") {//strict max number
				$datatow=$this->db->select('MAX(CAST( '.$filedname.' AS UNSIGNED )) as '.$filedname.'',false)->from($table)->where_not_in('status',3)->where_in('industryid',$this->industryid)->limit(1,0)->get();
			} else {
				$tableid = $table.'id';
				$primarymax=$this->db->select_max($tableid)
				->from($table)
				->where_not_in('status',3)
				->where_in('industryid',$this->industryid)
				->limit(1,0)->get()->row();
				$pid = $primarymax->$tableid;
				
				$datatow=$this->db->select_max($filedname)
				->from($table)
				->where_not_in('status',3)
				->where_in('industryid',$this->industryid)
				->where_in($table.'.'.$table.'id',$pid)
				->limit(1,0)->get();
			}
			foreach($datatow->result() as $maxrow) {
				$maxnumber=$maxrow->$filedname;
			}
			//checks whether records exits and not null
			if($datatow->num_rows() > 0 and $maxnumber != null) {
				foreach($datatow->result() as $next) {
					$datanum = trim($next->$filedname);
					if($datanum < $startnumber) {
						$num=$prefix.$startnumber.$suffix;
						return trim($num);
					}else {
						if($suffix == "") {					
							$prenumber = explode($prefix,$datanum);
							if($incrementby == '0') {
								if( count($prenumber) < '2') {
									$prenum = $prenumber[0]+1;
									$num = $prefix.$prenum;
									return trim($num);
								} else {
									$precount = strlen($prenumber[1]);
									$prenum = $prenumber[1]+1;
									$newprecount = strlen($prenum);
									$ncount = $precount - $newprecount;
									$appednd = substr($prenumber[1],'0',$ncount);
									$prenum = str_pad($prenum,$precount, $appednd, STR_PAD_LEFT);
									$num = $prefix.$prenum;
									return trim($num);
								}
							} else {
								if( count($prenumber) < '2') {
									$prenum = $prenumber[0]+$incrementby;
									$num = $prefix.$prenum;
									return trim($num);
								} else {
									$prenum = $prenumber[1]+$incrementby;
									$num = $prefix.$prenum;
									return trim($num);
								}
							}
						} else {
							$data = trim($next->$filedname);
							$number = explode($suffix,$data);
							$prenumber = explode($prefix,$number[0]);
							if($incrementby == '0') {
								if( count($prenumber) < '2') {
									$prenum = $prenumber[0]+1;
									$num = $prefix.$prenum.$suffix;
									return trim($num);
								} else {
									$prenum = $prenumber[1]+1;
									$num = $prefix.$prenum.$suffix;
									return trim($num);
								}
							} else {
								if( count($prenumber) < '2') {
									$num = $prenumber[0]+$incrementby;
									return trim($num.$suffix);
								} else {
									$prenum = $prenumber[1]+$incrementby;
									$num = $prefix.$prenum.$suffix;
									return trim($num);
								}
							}
						}
					}
				}
			} else {				
				$num=$prefix.$startnumber.$suffix;
				return trim($num);
			}		
		} else {		
			//randomcall
			$num=$this->fetchrandomnumber($filedname,$table,$tableid,$suffix,$prefix);
			return trim($num);
		}
	}
	//generate the random number//
	public function fetchrandomnumber($fieldnumber,$table,$tableid,$suffix,$prefix){
		$uid=time()+mt_rand(1,99999999);
		$alphanumber=$prefix.$uid.$suffix;
		$data=$this->db->select($fieldnumber)
					->from($table)
					->where($tableid,$alphanumber)
					->get()->num_rows();
        if($data > 0) {
			$this->fetchrandomnumber($fieldnumber,$table,$tableid,$suffix,$prefix);			
		} else {
			return $alphanumber;
		}
	}	
	public function getdropdownoptions($table) {
		$i=0;
		$industryid = $this->industryid;
		$id=$table.'id';
		$name=$table.'name';
		$this->db->select("$id,$name");
		$this->db->from($table);
		if(isset($_GET['accountid']) AND $table == 'contact'){
			if($_GET['accountid'] > 0){
			$this->db->where('accountid',$_GET['accountid']);
			}
		}
		$this->db->where('status',1);
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$data=$this->db->get();
		if($data->num_rows() > 0) {
			foreach($data->result() as $info) {
				$datam[$i] = array('id'=>$info->$id,'name'=>$info->$name);
				$i++;
			}
			echo json_encode($datam);
		}
		else{
			echo json_encode(array('fail'=>'FAILED'));
		}
	}
	public function getcrmaddress() {
		$table=$_GET['table'].'address';
		$id=$_GET['id'];
		$type=$_GET['type'];
		$primaryid=$_GET['table'].'id';
		$this->db->select('address,pincode,city,state,country');
		$this->db->from($table);
		//
		if($type == 'Billing'){
			$this->db->where('addressmethod','4');
		}
		else{
			$this->db->where('addressmethod','5');
		}
		//
		$this->db->where($primaryid,$id);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$this->db->limit(1);
		$data=$this->db->get();
		if($data->num_rows() > 0) {
			foreach($data->result() as $info) {
				$results=array('result'=>true,'address'=>$info->address,'pincode'=>$info->pincode,'city'=>$info->city,'state'=>$info->state,'country'=>$info->country);
			}
			echo json_encode($results);
		}
		else {
			$results=array('result'=>true,'address'=>'','pincode'=>'','city'=>'','state'=>'','country'=>'');
			echo json_encode($results);
		}
	}
	/*
	*It returns the Product/item details
	* @param $id productid
	*/
	public function getproductdetails($id) {
		$productid=$id;		
		$softwareindustryid = $this->Basefunctions->industryid;
		if($softwareindustryid == 4){
			if(isset($_GET['ordertypeid'])){
				if($_GET['ordertypeid'] == 5){
					$this->db->select('productname,product.shortname,product.description,product.unitprice,additionalchargeapplicable,instock,taxmasterid,additionalchargecategoryid,product.uomid,taxable,product.categoryid,product.hasbatch,productcf.durationid');
				}else{
					$this->db->select('productname,product.shortname,product.description,product.unitprice,additionalchargeapplicable,instock,taxmasterid,additionalchargecategoryid,product.uomid,taxable,product.categoryid,product.hasbatch');
				}
			}else{
				$this->db->select('productname,product.shortname,product.description,product.unitprice,additionalchargeapplicable,instock,taxmasterid,additionalchargecategoryid,product.uomid,taxable,product.categoryid,product.hasbatch');
			}
		}else{
			$this->db->select('productname,product.shortname,product.description,product.unitprice,additionalchargeapplicable,instock,taxmasterid,additionalchargecategoryid,product.uomid,taxable,product.categoryid,product.hasbatch');
		}		
		$this->db->from('product');
		$this->db->join('uom','uom.uomid=product.uomid');
		if(isset($_GET['ordertypeid'])){
			if($_GET['ordertypeid'] == 5){
				$this->db->join('productcf','productcf.productid=product.productid');
			}
		}
		$this->db->where('product.productid',$productid);
		$this->db->where('product.status',$this->Basefunctions->activestatus);
		$this->db->limit(1);
		$data=$this->db->get();
		foreach($data->result() as $info) {
			$u_price=$info->unitprice;
			if($u_price == '' OR $u_price == null){
				$u_price = 0;
			}
			if(isset($info->durationid)){
				$duration=$info->durationid;
				if($duration == '' OR $duration == null){
					$duration = '';
				}
			}else{	
				$duration = '';
			}
			$product=array('shortname'=>$info->shortname,'description'=>$info->description,'unitprice'=>$u_price,'instock'=>$info->instock,'taxid'=>$info->taxmasterid,'addcatid'=>$info->additionalchargecategoryid,'uomid'=>$info->uomid,'taxable'=>$info->taxable,'sellingprice'=>$u_price,'categoryid'=>$info->categoryid,'hasbatch'=>$info->hasbatch,'prodinprice'=>0,'duration'=>$duration);
			$uomid=$info->uomid;
			if($uomid != ''){
				$touomarr=array();
				$i=0;
				$this->db->select('productuomconversion.uomtoid,productuomconversion.conversionrate,uom.uomname,uom.symbol,uom.uomprecision,')->from('productuomconversion');
				$this->db->join('uom','uom.uomid=productuomconversion.uomtoid');
				$this->db->where('productuomconversion.productid',$productid);
				$this->db->where('productuomconversion.status',$this->Basefunctions->activestatus);
				$data=$this->db->get();
				if($data->num_rows() > 0){
					foreach($data->result() as $value) {
						$touomarr[$i] = array('uomtoid'=>$value->uomtoid,'uomname'=>$value->uomname,'conversionrate'=>$value->conversionrate,'symbol'=>$value->symbol,'uomprecision'=>$value->uomprecision);
						$i++;
					}
				}else{
					$y=0;
					$this->db->select('uomconversion.uomtoid,uomconversion.conversionrate,uom.uomname,uom.symbol,uom.uomprecision,')->from('uomconversion');
					$this->db->join('uom','uom.uomid=uomconversion.uomtoid');
					$this->db->where('uomconversion.uomid',$uomid);
					$this->db->where('uomconversion.status',$this->Basefunctions->activestatus);
					$data=$this->db->get();
					foreach($data->result() as $value) {
						$touomarr[$y] = array('uomtoid'=>$value->uomtoid,'uomname'=>$value->uomname,'conversionrate'=>$value->conversionrate,'symbol'=>$value->symbol,'uomprecision'=>$value->uomprecision);
						$y++;
					}
				}
				$product['touomarr']=$touomarr;
			}
		}
	if(isset($_GET['pricebook'])){
		if($_GET['pricebook'] > 0){
			$pdprice=$this->db->select('sellingprice')->from('pricebookdetail')
										 ->where('pricebookid',$_GET['pricebook'])
										 ->where('productid',$productid)
										 ->where('status',$this->Basefunctions->activestatus)
										 ->limit(1)
										 ->get();
			foreach($pdprice->result() as $infom){
				$sellingprice = $infom->sellingprice;					
				$product['sellingprice']=$sellingprice;
				$product['prodinprice']= 1;
			}
			
		}
	}
	echo json_encode($product);
	}
	
	//terms and condition data fetch
	public function termsandcontdatafetchmodel() {
		$id = $_GET['id'];
		$this->db->select('filename');
		$this->db->from('termsandcondition');
		$this->db->where('termsandcondition.termsandconditionid',$id);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$data = $row->filename;
			}
			echo json_encode($data);
		}
	}	
	//removes additional tax charge details
	public function deletetaxaddchargedetail($moduleid,$table,$primaryid) {
		
		$child=$table.'detail';
		$childid=$child.'id';
		$modedata=$this->db->select('taxmodeid,additionalchargemodeid')
							->from($table)
							->where($table.'id',$primaryid)
							->get();
		$info=$modedata->row();
		
		$taxmodeid=$info->taxmodeid;
		$softwareindustryid = $this->Basefunctions->industryid;
		if($softwareindustryid != 2){
		$additionalchargemodeid=$info->additionalchargemodeid;
		}
		//child detail
		$modedetaildata=$this->db->select($childid)
							->from($child)
							->where($table.'id',$primaryid)
							->get();
		foreach($modedetaildata->result() as $infom) {
			$modedetailid[]=$infom->$childid;
		}
			
		//delete the additional/tax/discount if exits		
		$moduledelete=array('status'=>$this->Basefunctions->deletestatus,'lastupdatedate'=>date($this->datef),'lastupdateuserid'=>$this->userid);
		if($softwareindustryid != 2){
			$modetype=array($taxmodeid,$additionalchargemodeid);
			$moduledetailtable=array('moduletaxdetail','modulechargedetail');
		}else{
			$modetype=array($taxmodeid);
			$moduledetailtable=array('moduletaxdetail');
		}
		//moduletaxdetail
		foreach($moduledetailtable as $key=>$detailtable) {
			$this->db->where('moduleid',$moduleid);
			$this->db->where('singlegrouptypeid',$modetype[$key]);
			if($modetype[$key] == 2) {					
				$this->db->where_in('id',$modedetailid);
				$this->db->update($detailtable,$moduledelete);
			} elseif($modetype[$key] == 3) {				
				$this->db->where('id',$primaryid);
				$this->db->update($detailtable,$moduledelete);
			}			
		}		
	}
	//cancel additional tax charge details
	public function canceltaxaddchargedetail($moduleid,$table,$primaryid) {
		$child=$table.'detail';
		$childid=$child.'id';
		$modedata=$this->db->select('taxmodeid,discountmodeid,additionalchargemodeid')
							->from($table)
							->where($table.'id',$primaryid)
							->get();
		$info=$modedata->row();
		
		$taxmodeid=$info->taxmodeid;
		$discountmodeid=$info->discountmodeid;
		$additionalchargemodeid=$info->additionalchargemodeid;
		//child detail
		$modedetaildata=$this->db->select($childid)
							->from($child)
							->where($table.'id',$primaryid)
							->get();
		foreach($modedetaildata->result() as $infom) {
			$modedetailid[]=$infom->$childid;
		}
			
		//delete the additional/tax/discount if exits		
		$moduledelete=array('status'=>$this->cancelstatus,'lastupdatedate'=>date($this->datef),'lastupdateuserid'=>$this->userid);
		$modetype=array($taxmodeid,$discountmodeid,$additionalchargemodeid);		
		$moduledetailtable=array('moduletaxdetail','modulediscountdetail','moduleaddchargedetail');
		//moduletaxdetail
		foreach($moduledetailtable as $key=>$detailtable) {
			$this->db->where('moduleid',$moduleid);
			$this->db->where('singlegrouptypeid',$modetype[$key]);
			if($modetype[$key] == 2) {					
				$this->db->where_in('id',$modedetailid);
				$this->db->update($detailtable,$moduledelete);
			} elseif($modetype[$key] == 3) {				
				$this->db->where('id',$primaryid);
				$this->db->update($detailtable,$moduledelete);
			}			
		}		
	}
	//get default currency
	public function getdefaultcurrency() {
		$userid=$this->Basefunctions->userid;		
		$data=$this->db->select('company.companyid,company.currencyid')
						->from('company')
						->join('branch','branch.companyid=company.companyid')
						->join('employee','employee.branchid=branch.branchid')
						->where('employee.employeeid',$userid)
						->limit(1)
						->get();
		$row=$data->row();	
		//retrieve pricebooknumbers
		$pricebookarr=array();
		return json_encode(array('currency'=>$row->currencyid,'pricebook'=>$pricebookarr));
	}
	/**
	* Generate the Pricebook List based on product at Quote/SalesOrder/PurchaseOrder/MaterialRequest/Invoice
	*/
	public function pricebookview($tablename,$sortcol,$sortord,$pagenum,$rowscount)	{
		$pid = trim($_GET['productid']);
		$territory = trim($_GET['territory']);
		$dataset = 'pricebookdetail.pricebookdetailid,pricebook.pricebookname,product.productname,pricebookdetail.sellingprice,pricebook.description,currency.currencyname,pricebook.currencyid';
		$join =' LEFT OUTER JOIN product ON product.productid=pricebookdetail.productid';
		$join .=' LEFT OUTER JOIN pricebook ON pricebook.pricebookid=pricebookdetail.pricebookid';
		$join .=' LEFT OUTER JOIN currency ON currency.currencyid=pricebook.currencyid';
		$status = $tablename.'.status NOT IN (0,3)';
		$where = 'pricebookdetail.productid='.$pid;
		if($territory != 1){
			$where .= ' AND FIND_IN_SET('.$territory.',pricebook.territoryid) >0';
		}		
		/* pagination */
		$query = 'select '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$where.' AND '.$status.' GROUP BY pricebook.pricebookid ORDER BY'.' '.$sortcol.' '.$sortord;
		$page = $this->Basefunctions->generatepage($query,$pagenum,$rowscount);
		/* query */
		$data = $this->db->query('select '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$where.' AND '.$status.' GROUP BY pricebook.pricebookid ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$page['start'].','.$page['records'].'');
		$finalresult=array($data,$page,'');
		return $finalresult;
	}
	/*
	*Retrieves the product records for product search grid-
	*/
	public function getproductsearch($tablename,$sortcol,$sortord,$pagenum,$rowscount,$ordertype) {
		$dataset = 'product.productid,category.categoryname,product.productname,product.attributevalues';
		$join =' LEFT OUTER JOIN category ON category.categoryid=product.categoryid';
		$status = $tablename.'.status NOT IN (0,3)';
		$order = 'product.ordertypeid ='.$ordertype;
		$stockable = 'product.stockable = "Yes"';
		if($ordertype == 1){
			/* pagination */
			$query = 'select '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$status.' ORDER BY'.' '.$sortcol.' '.$sortord;
			$page = $this->Basefunctions->generatepage($query,$pagenum,$rowscount);
			/* query */
			$data = $this->db->query('select '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$status.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$page['start'].','.$page['records'].'');
			$finalresult=array($data,$page,'');
		}else if($ordertype == 0){
			/* pagination */
			$query = 'select '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$status.' AND '.$stockable.' ORDER BY'.' '.$sortcol.' '.$sortord;
			$page = $this->Basefunctions->generatepage($query,$pagenum,$rowscount);
			/* query */
			$data = $this->db->query('select '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$status.' AND '.$stockable.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$page['start'].','.$page['records'].'');
			$finalresult=array($data,$page,'');
		}else{
			/* pagination */
			$query = 'select '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$status.' AND '.$order.' ORDER BY'.' '.$sortcol.' '.$sortord;
			$page = $this->Basefunctions->generatepage($query,$pagenum,$rowscount);
			/* query */
			$data = $this->db->query('select '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$status.' AND '.$order.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$page['start'].','.$page['records'].'');
			$finalresult=array($data,$page,'');
		}
		
		return $finalresult;
	}	
	/*
	*Get the attribute value name for a given attributevaluesid
	*/
	public function getattributevalues($array) {
		$value='';
		$data=$this->db->query("select GROUP_CONCAT(attributevaluename) as groupvalues
								FROM attributevalue
								WHERE attributevalueid IN ($array)"
		                       );
		foreach($data->result() as $info) {
			$value=$info->groupvalues;
		}
		return $value;
	}
	//get add component values only used in crm modules
	public function crmaddressfetch($parentid,$primarydataname,$table) {		
		$this->db->select('addresssourceid,address,pincode,city,state,country');
		$this->db->from($table);
		$this->db->where("$table.$primarydataname",$parentid);
		$this->db->where('status',1);
		$this->db->order_by($primarydataname,'asc');
		$result = $this->db->get();
		$arrname=array('billing','shipping');
		if($result->num_rows() >0) 
		{
			$m=0;
			foreach($result->result() as $row) 
			{
				$data[] = array($arrname[$m].'addresstype'=>$row->addresssourceid,
								$arrname[$m].'address'=>$row->address,
								$arrname[$m].'pincode'=>$row->pincode,
								$arrname[$m].'city'=>$row->city,
								$arrname[$m].'state'=>$row->state,
								$arrname[$m].'country'=>$row->country);
				$m++;
			}
			$finalarray=array_merge($data[0],$data[1]);
		} 
		else {
		   $finalarray=array("fail"=>'FAILED');
		}
		echo json_encode($finalarray);
		//
	}
	//check box value get
	public function checkboxvaluegetmodel(){
		$checkdata = 'Yes,No';
		$checkid = '1,2';
		$cdata = explode(',',$checkdata);
		$cid = explode(',',$checkid);
		for($i=0;$i<count($cdata);$i++){
			$data[$i] = array('datasid'=>$cid[$i],'dataname'=>$cdata[$i]);
			}
			echo json_encode($data);
	}
	//field name based drop down value get
	public function fieldnamebesdddvaluemodel(){
		$i=0;
		$industryid = $this->industryid;
		$fieldname = $_GET['fieldid'];
		if(isset($_GET['moduleid'])) {
			$moduleid = $_GET['moduleid'];
		} else {
			$moduleid = 1;
		}
		$tabname = substr($fieldname, -4);
		if($tabname == 'name') {
			$table = substr($fieldname, 0, -4);
		} else {
			$table = substr($fieldname, 0, -6);
		}
		$check = $this->checkassigntofield($table,'industryid');
		$fieldid = $table.'id';
		if($table == 'sizemaster') {
			$this->db->select($fieldid.','.$fieldname.',product.productname');
		} else {
			$this->db->select($fieldid.','.$fieldname);
		}
		$this->db->from($table);
		if($table == 'sizemaster') {
			$this->db->join('product','product.productid=sizemaster.productid');
		}
		if($moduleid == 137 && $fieldname == 'accountname') {
			$this->db->where('account.accounttypeid',16);
		}
		$this->db->where_in($table.'.status',array(1,11));
		if($check == 'true') {
			$this->db->where("FIND_IN_SET('".$industryid."',".$table.".industryid) >", 0);
		}
		$reult =$this->db->get();
		if($reult->num_rows() > 0){
			if($table == 'sizemaster') {
				foreach($reult->result() as $row){
					$data[$i] = array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname,'productname'=>$row->productname);
					$i++;
				}
			} else {
				foreach($reult->result() as $row){
					$data[$i] = array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname);
					$i++;
				}
			}
			
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	// for lead concersion module filter value load
	public function leadconversionfieldnamebesdddvaluemodel(){
		$i=0;
		$industryid = $this->industryid;
		$fieldname = $_GET['fieldid'];
		$moduleid = $_GET['moduleid'];
		$mfid = $_GET['mfid'];
		$fieldid = 'modulefield';
		$fieldname = 'fieldlabel';
		$table = 'modulefield';
		if($mfid == 1383) {
			$mid = 201;
		} else if($mfid == 1384) {
			$mid = 202;
		} else if($mfid == 1385) {
			$mid = 203;
		} else if($mfid == 1386) {
			$mid = 204;
		}
		$check = $this->checkassigntofield($table,'industryid');
		$fieldid = $table.'id';
		$this->db->select($fieldid.','.$fieldname);
		$this->db->from($table);
		$this->db->where_in('status',array(1,11));
		if($check == 'true') {
			$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		}
		$this->db->where("FIND_IN_SET('".$mid."',moduletabid) >", 0);
		$reult =$this->db->get();
		if($reult->num_rows() > 0) {
			foreach($reult->result() as $row) {
				$data[$i] = array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//field anme fetch value with condition
	public function fieldnamebesdddvaluewithcondmodel(){
		$i=0;
		$fieldname = $_GET['fieldid'];
		$moduleid = $_GET['moduleid'];
		$industryid = $this->industryid;
		$table = substr($fieldname, 0, -4);
		$fieldid = $table.'id';
		$this->db->select($fieldid.','.$fieldname);
		$this->db->from($table);
		$this->db->where("FIND_IN_SET('".$moduleid."',moduleprivilegeid) >", 0);
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->where('status',1);
		$reult =$this->db->get();
		if($reult->num_rows() > 0){
			foreach($reult->result() as $row){
				$data[$i] = array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	public function attributevaluedataddfetch($moduleid) {
		$data = array();
		$industryid = $this->industryid;
		$i=0;
		$this->db->select("attribute.attributename,attribute.attributeid");
		$this->db->from("attribute");
		$this->db->join("attributesetassign","attributesetassign.attributeid=attribute.attributeid");
		$this->db->join("attributeset","attributeset.attributesetid=attributesetassign.attributesetid");
		$this->db->join("moduleattributesetting","moduleattributesetting.attributesetid=attributeset.attributesetid");
		$this->db->where("moduleattributesetting.moduleid",$moduleid);
		$this->db->where("moduleattributesetting.status",1);
		$this->db->where("FIND_IN_SET('".$industryid."',attribute.industryid) >", 0);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result()as $row) {
				$data[$i]=array('Id'=>$row->attributeid,'Name'=>$row->attributename);
				$i++;
			}echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}		
	}
	//field name based drop down value get - picklist 
	public function fieldnamebesdpicklistddvaluemodel(){
		$i=0;
		$moduleid = 'moduleid';
		$fieldname = $_GET['fieldid'];
		$industryid = $this->industryid;
		$userroleid = $this->userroleid;
		$mid = $_GET['moduleid'];
		$tabname = substr($fieldname, -4);
		if($tabname == 'name'){
			$table = substr($fieldname, 0, -4);
		} else {
			$table = substr($fieldname, 0, -6);
		}
		if($_GET['uitype'] == 4 || $_GET['uitype'] == 5) { 
			$table = 'rangemaster';
			$fieldid = $table.'id';
			$fieldname = $table.'name';
		} else {
			$table = $table;
			$fieldid = $table.'id';
			$fieldname= $fieldname;
		}
		$this->db->select($fieldid.','.$fieldname);
		$this->db->from($table);
		if(trim($_GET['uitype']) == 17){
			$this->db->where("FIND_IN_SET('".$mid."',$moduleid) >", 0);
		} 
		$this->db->where("FIND_IN_SET('".$userroleid."',userroleid) >", 0);
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->where('status',1);
		$reult =$this->db->get();
		if($reult->num_rows() > 0){
			foreach($reult->result() as $row){
				$data[$i] = array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//filter values
	public function filtervalue($fields) {
		$fieldsinfo = array_unique( $fields ) ;
		$fieldname = implode(',',$fieldsinfo);
		return $fieldname;
	}
	//unique name check
	public function uniquedynamicviewnamecheckmodel(){
		$industryid = $this->industryid;
		$primaryid=$_POST['primaryid'];
		$accname=$_POST['accname'];
		$elementpartable = explode(',',$_POST['elementspartabname']);
		$partable =  $this->filtervalue($elementpartable);
		$ptid = $partable.'id';
		$ptname = $partable.'name';
		if($partable == 'currency'){
			$ptname = 'currencycode';
		}
		if($accname != "") {
			$result = $this->db->select($ptname.','.$ptid)->from($partable)->where($partable.'.'.$ptname,$accname)->where($partable.'.status',1)->where("FIND_IN_SET('".$industryid."',industryid) >", 0)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->$ptid;
				} 
			} else { echo "False"; }
		}else { echo "False"; }
	}
	//unique short name check
	public function uniqueshortdynamicviewnamecheckmodel(){
		$industryid = $this->industryid;
		$primaryid=$_POST['primaryid'];
		$accname=$_POST['accname'];
		$elementpartable = explode(',',$_POST['elementspartabname']);
		$partable =  $this->filtervalue($elementpartable);
		$ptid = $partable.'id';
		$ptname = $_POST['fieldname'];
		if($accname != "") {
			$result = $this->db->select($ptname.','.$ptid)->from($partable)->where($partable.'.'.$ptname,$accname)->where($partable.'.status',1)->where("FIND_IN_SET('".$industryid."',industryid) >", 0)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->$ptid;
				}
			} else { echo "False"; }
		}else { echo "False"; }
	}
	//cancel data
	public function canceldata($table,$primaryid,$primaryidname,$status) {
		$tablearr=explode(',',$table);
		$modulecancel=array('status'=>$status,'lastupdatedate'=>date($this->datef),'lastupdateuserid'=>$this->userid);
		for($i=0;$i<count($tablearr);$i++)
		{
			$this->db->where($primaryidname,$primaryid);
			$this->db->update($tablearr[$i],$modulecancel);
		}
		echo true;
	}
	//status change the tax,discount,additional charge detail
	public function statuschangetaxaddchargedetail($moduleid,$table,$primaryid,$changestatus) {
		$child=$table.'detail';
		$childid=$child.'id';
		$modedata=$this->db->select('taxmodeid,additionalchargemodeid')
							->from($table)
							->where($table.'id',$primaryid)
							->get();
		$info=$modedata->row();
		
		$taxmodeid=$info->taxmodeid;		
		$additionalchargemodeid=$info->additionalchargemodeid;
		//child detail
		$modedetaildata=$this->db->select($childid)
							->from($child)
							->where($table.'id',$primaryid)
							->get();
		foreach($modedetaildata->result() as $infom) {
			$modedetailid[]=$infom->$childid;
		}
			
		//delete the additional/tax/discount if exits		
		$moduledelete=array('status'=>$changestatus,'lastupdatedate'=>date($this->datef),'lastupdateuserid'=>$this->userid);
		$modetype=array($taxmodeid,$additionalchargemodeid);		
		$moduledetailtable=array('moduletaxdetail','modulechargedetail');
		//moduletaxdetail
		foreach($moduledetailtable as $key=>$detailtable) {
			$this->db->where('moduleid',$moduleid);
			$this->db->where('singlegrouptypeid',$modetype[$key]);
			if($modetype[$key] == 2) {					
				$this->db->where_in('id',$modedetailid);
				$this->db->update($detailtable,$moduledelete);
			} elseif($modetype[$key] == 3) {				
				$this->db->where('id',$primaryid);
				$this->db->update($detailtable,$moduledelete);
			}			
		}		
	}
	//check cancel status
	public function checkcancelstatus() {
		$table=trim($_GET['moduletable']);
		$primaryname=$table.'id';
		$id=trim($_GET['primarydataid']);
		$moduleid=trim($_GET['moduleid']);
		$data=$this->db->select('status')
						->from($table)
						->where($primaryname,$id)
						->get()
						->result();
		foreach($data as $info) {
			$status=$info->status;
		}
		if($status == $this->activestatus || $status == $this->cancelstatus) {
			echo true;
		} else {
			echo false;
		}
	}
	//check email
	public function checkvalidemail() {
		$id=trim($_GET['id']);
		$table=trim($_GET['table']);
		$primaryid=$table.'id';
		$field=trim($_GET['field']);
		$data=$this->db->select($field)->from($table)->where($primaryid,$id)->get()->result();
		foreach($data as $info)
		{
			$email=$info->$field;
		}
		if(filter_var($email, FILTER_VALIDATE_EMAIL))
		{ 
			$a=true;
		}
		else
		{ 
			$a=false;
		}
		echo $a;
	}
	//get valid email id
	public function getvalidemaildetail() {
		$id=trim($_GET['id']);
		$table=trim($_GET['table']);
		$primaryid=$table.'id';
		$field=trim($_GET['field']);
		$data=$this->db->select($field)->from($table)->where($primaryid,$id)->get()->result();
		foreach($data as $info) {
			$email=$info->$field;
		}
		//subject,file details
		$mainprimary=trim($_GET['mainprimary']);
		$mainprimaryfield=trim($_GET['maintable']).'id';
		$maintable=trim($_GET['maintable']);
		$subject='';
		if($mainprimary != '' and $maintable != '') {
			$datam=$this->db->select('subject')->from($maintable)->where($mainprimaryfield,$mainprimary)->get()->result();
			foreach($datam as $info) {
				$subject=$info->subject;
			}
		}
		$array=array('emailid'=>$email,'subject'=>$subject);
		echo json_encode($array);
	}
	//get account id
	public function getaccountid() {
		$id=trim($_GET['id']);
		$table=trim($_GET['table']);
		$primaryid=$table.'id';
		$field=trim($_GET['field']);
		$data=$this->db->select($field)->from($table)->where($primaryid,$id)->get()->result();
		foreach($data as $info) {
			$email=$info->$field;
		}
		$array=array('accountid'=>$email);
		echo json_encode($array);
	}
	//fetch field name
	public function generalinformaion($tblename,$fieldname,$cond,$cvalue) {
		$data=0;
		$this->db->select($fieldname,false);
		$this->db->from($tblename);
		$this->db->where('status',1);
		$this->db->where($cond,$cvalue);
		$this->db->limit(1);
		$result = $this->db->get();
		if($result->num_rows() >0) {		
			foreach($result->result()as $row) {
				$data=$row->$fieldname;
			}		
		}
		return $data;
	}
	//fetch field name
	public function modulelinkfetch($tblename,$fieldname,$cond,$cvalue) {
		$data=0;
		$this->db->select($fieldname,false);
		$this->db->from($tblename);
		$this->db->where($cond,$cvalue);
		$this->db->limit(1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result()as $row) {
				$data=$row->$fieldname;
			}
		}
		return $data;
	}
	//notification log entry
	public function notificationcontentadd($lcid,$ntypeid,$msg,$empid,$moduleid) {
		if($empid) {
			$empid = $empid;
		} else {
			$empid =1;
		}
		$notcdata=array(
			'notificationlogtypeid'=>$ntypeid,
			'moduleid'=>$moduleid,
			'commonid'=>$lcid,
			'employeeid'=>$empid,
			'notificationmessage'=>$msg,
			'createdate'=>date($this->Basefunctions->datef),
			'lastupdatedate'=>date($this->Basefunctions->datef),
			'createuserid'=>$this->Basefunctions->userid,
			'lastupdateuserid'=>$this->Basefunctions->userid,
			'status'=>$this->Basefunctions->activestatus,
			'flag'=>1
		);
		$this->db->insert('notificationlog',$notcdata);
		//notification count entry
		$this->notificationcountentry($empid,$moduleid);
	}
	//notification count increment entrey
	public function notificationcountentry($empid,$moduleid) {
		$status = $this->Basefunctions->activestatus;
		$setdata=$this->db->select('datapreferenceid')->where_in('datapreference.employeeid',$empid)->where('datapreference.moduleid',$moduleid)->from('datapreference')->get();
		$datacount = $setdata->num_rows();
		if($empid){
			$empid = $empid;
		} else{
			$empid =1;
		}
		if($datacount == 0) {
			$defnot=array(
				'employeeid'=>$empid,
				'moduleid'=>$moduleid,
				'viewcreationid'=>1,
				'notificationcountval'=>0,
				'status'=>$status
				);
			$this->db->insert('datapreference',$defnot); 
		} else {
			$this->db->query("UPDATE datapreference SET `notificationcountval` = 1 WHERE `employeeid` in (".$empid.") AND `moduleid` =".$moduleid." AND status = 1");
		}
	}
	//dash board chart data count
	public function dashboardchartdatainformationgenerte($chartdata) {
		$i = 0;
		$charttype = $chartdata['charttype'];
		$reportid = $chartdata['reportid'];
		$groupbyid = $chartdata['groupbyid'];
		$recordcount = $chartdata['recordcount'];
		$calculationfieldid = $chartdata['calculationfieldid'];
		$operation = $chartdata['operation'];	
		$splitbyname = $chartdata['splitname'];	
		$splitbyid = $chartdata['splitbyid'];	
		$twopoint = $chartdata['twopoint'];	
		//checks whether report data exists
		if($reportid > 1 and $groupbyid > 1) {
			//check active report//ready-will be used on go
			$reportstatus=$this->getcurrentstatus($reportid);
			$data=$this->db->select('viewcreationcolmodelname,viewcreationparenttable,viewcreationcolmodeljointable,viewcreationcolmodelindexname')->from('viewcreationcolumns')->where('viewcreationcolumnid',$groupbyid)->get()->result();
			foreach($data as $value) {
				$groupbycolumn = $value->viewcreationcolmodelindexname;
				$tablename = $value->viewcreationcolmodeljointable;
			}
			$select = $tablename.'.'.$groupbycolumn.', count(*) as output';
			$groupby = $tablename.'.'.$groupbycolumn;
			if($charttype == 'stackedchart' || $charttype == 'bubblechart'|| $charttype == 'columnlinechart') {
				$data=$this->db->select('viewcreationcolmodelname,viewcreationparenttable,viewcreationcolmodeljointable,viewcreationcolmodelindexname')->from('viewcreationcolumns')->where('viewcreationcolumnid',$splitbyid)->get();
				foreach($data->result() as $value) {
					$splitgroupbycolumn=$value->viewcreationcolmodelindexname;
					$splittablename=$value->viewcreationcolmodeljointable;
				}				
				if($data->num_rows() > 0) {
					$select = $tablename.'.'.$groupbycolumn.','.$splittablename.'.'.$splitgroupbycolumn.', count(*) as output';
					$groupby = $tablename.'.'.$groupbycolumn.','.$splittablename.'.'.$splitgroupbycolumn;
				}
			}
			if($recordcount == false or $recordcount ==  '') {
				if($calculationfieldid > 1 and $operation !='') {
					$data=$this->db->select('viewcreationcolmodelname,viewcreationparenttable,viewcreationcolmodelindexname')->from('viewcreationcolumns')->where('viewcreationcolumnid',$calculationfieldid)->get()->result();
					foreach($data as $value) {
						$calculatebycolumn=$value->viewcreationcolmodelindexname;
						$calculatetablename=$value->viewcreationparenttable;
					}
					$select = $tablename.'.'.$groupbycolumn.',round('.$operation.'('.$calculatebycolumn.'),2)'.' as output';
					if($charttype == 'stackedchart' || $charttype == 'bubblechart' || $charttype == 'columnlinechart') {
						$select = $tablename.'.'.$groupbycolumn.','.$splittablename.'.'.$splitgroupbycolumn.',round('.$operation.'('.$calculatebycolumn.'),2)'.' as output';
					}
					
				}			
			}
			$selectsummary = array('group'=>$groupby,'select'=>$select);
			$this->load->model('Reports/Reportsmodel');
			$chartdatam = $this->Reportsmodel->chartreportsummary($reportid,$selectsummary);
			if($chartdatam->num_rows() >0) {
				if($charttype == 'stackedchart' || $charttype == 'bubblechart'|| $charttype == 'columnlinechart'){
				   $arcone=array();
				   foreach ($chartdatam->result() as $jvalue) {
				   		$datafield[$i]=array('groupone'=>$jvalue->$groupbycolumn,'grouptwo'=>$jvalue->$splitgroupbycolumn,'counts'=>$jvalue->output);
						$i++;
				   }	
				} else {
					foreach ($chartdatam->result() as $jvalue) {
						$datafield[$i]=array('captionname'=>$jvalue->$groupbycolumn,'counts'=>$jvalue->output);
						$i++;
					}
				}
				return json_encode($datafield);	
			} else {
				if($charttype == 'stackedchart' || $charttype == 'bubblechart'|| $charttype == 'columnlinechart'){
					$datafield=array('groupone'=>'','grouptwo'=>'','counts'=>'');
				} else {
					$datafield=array('captionname'=>'','counts'=>'');
				}
			    return json_encode($datafield);
			} 
		} else {
			if($charttype == 'stackedchart' || $charttype == 'bubblechart'|| $charttype == 'columnlinechart') {
				$datafield=array('groupone'=>'','grouptwo'=>'','counts'=>'');
			} else {
				$datafield=array('captionname'=>'','counts'=>'');
			}
			return json_encode(array());
		}
	}
	//print pdf value set
    public function pdfpreviewfetchdddataviewddvalmodel() {
		$i=0;
		$mvalu ="";
		$whfield ="";
		$moduleid=$_GET['viewfieldids'];
		$dname=$_GET['dataname'];
		$did=$_GET['dataid'];
		if($_GET['column']) {
			$column = $_GET['column'];
		} else {
			$column = 'status';
		}
		if($_GET['columnsecond']) {
			$columnsecond = $_GET['columnsecond'];
		} else {
			$columnsecond = 'status';
		}
		if($_GET['columnthird']) {
			$columnthird = $_GET['columnthird'];
		} else {
			$columnthird = 'status';
		}
		$table=$_GET['datatab'];
		if( isset($_GET['whdatafield']) && isset($_GET['whval']) ) {
			$whfield=$_GET['whdatafield'];
			$whdata=$_GET['whval'];
			if($whdata != "") {
				$mvalu=explode(",",$whdata);
			}
		}
		$this->db->select("$dname,$did,$column,$columnsecond,$columnthird");
		$this->db->from($table);
		if($mvalu != '' || $whfield != '') {
			$this->db->where_in($whfield,$mvalu);
		}
		$this->db->where_in('moduleid',array($moduleid));
		$this->db->where_in('industryid',array($this->industryid));
		$this->db->where_not_in('status',array(0,3,4));
		$this->db->order_by($did,'desc');
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result()as $row) {
				$data[$i]=array('datasid'=>$row->$did,'dataname'=>$row->$dname,'stockdate'=>$row->$column,'printtypeid'=>$row->$columnsecond,'printername'=>$row->$columnthird);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		} 
    }
	//Tag template - print value set
    public function tagtemplatesloadddvalmodel() {
		$i=0;
		$mvalu ="";
		$whfield ="";
		$moduleid=$_GET['viewfieldids'];
		$dname=$_GET['dataname'];
		$did=$_GET['dataid'];
		if($_GET['column']) {
			$column = $_GET['column'];
		} else {
			$column = 'status';
		}
		$table=$_GET['datatab'];
		if( isset($_GET['whdatafield']) && isset($_GET['whval']) ) {
			$whfield=$_GET['whdatafield'];
			$whdata=$_GET['whval'];
			if($whdata != "") {
				$mvalu=explode(",",$whdata);
			}
		}
		$this->db->select("$dname,$did,$column");
		$this->db->from($table);
		if($mvalu != '' || $whfield != '') {
			$this->db->where_in($whfield,$mvalu);
		}
		$this->db->where_in('moduleid',array($moduleid));
		$this->db->where_in('industryid',array($this->industryid));
		$this->db->where_not_in('status',array(0,3,4));
		$this->db->order_by($did,'desc');
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result()as $row) {
				$data[$i]=array('datasid'=>$row->$did,'dataname'=>$row->$dname,'printername'=>$row->$column);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		} 
    }
	//lead status widgets
	public function leadstatusgeneratewidget() {
		$data=array();
		$m=0;
		$leadcount=0;
		$results=$this->db->select('crmstatus.crmstatusname,count(leadid) as leadcount')
					->from('lead')
					->join('crmstatus','crmstatus.crmstatusid=lead.crmstatusid')
					->where('lead.status',$this->activestatus)
					->group_by('lead.crmstatusid')
					->get();
		foreach($results->result()as $row) {
			$leadcount+=$row->leadcount;			
		}
		$data[$m]=array('count'=>$leadcount,'status'=>'Total Lead');
		$m++;
		foreach($results->result()as $row) {			
			$data[$m]=array('count'=>$row->leadcount,'status'=>$row->crmstatusname);
			$m++;				
		}
		return json_encode($data);
	}
	//date format system
	public function datesystem($type){
		$daterange=array();
		switch ($type){		
			case 'lastweek':
				$previous_week = strtotime("-1 week +1 day");
				$start_week = strtotime("last sunday midnight",$previous_week);
				$end_week = strtotime("next saturday",$start_week);
				$start_week = date("Y-m-d",$start_week);
				$end_week = date("Y-m-d",$end_week);
				$daterange=array('start'=>$start_week,'end'=>$end_week);
				break;
			case 'currentweek':
				$start_week = date("Y-m-d",strtotime('last sunday'));
				$end_week = date("Y-m-d",strtotime("saturday this week"));
				$daterange=array('start'=>$start_week,'end'=>$end_week);
				break;
			case 'nextweek':
				$start_week = date("Y-m-d",strtotime('next sunday'));
				$end_week = date("Y-m-d",strtotime("+6 day",strtotime ($start_week)));
				$daterange=array('start'=>$start_week,'end'=>$end_week);
				break;
			case 'lastmonth':
				$start_date = date('Y-m-01', strtotime('previous month'));
				$end_date = date('Y-m-t', strtotime('previous month'));
				$daterange=array('start'=>$start_date,'end'=>$end_date);
				break;
			case 'currentmonth':
				$start_date = date("Y-m-01");
				$end_date = date("Y-m-t");
				$daterange=array('start'=>$start_date,'end'=>$end_date);
				break;
			 case 'nextmonth':
				$start_date = date('Y-m-01', strtotime('next month'));
				$end_date = date("Y-m-t", strtotime('next month'));
				$daterange=array('start'=>$start_date,'end'=>$end_date);
				break; 	
			case 'currentandpreviousmonth':
				$start_date = date('Y-m-01', strtotime('previous month'));
				$end_date = date("Y-m-t");
				$daterange=array('start'=>$start_date,'end'=>$end_date);
				break;	
			case 'currentandnextmonth':
				$start_date = date("Y-m-01");
				$end_date = date("Y-m-t", strtotime('next month'));
				$daterange=array('start'=>$start_date,'end'=>$end_date);
				break;	
		}
		return $daterange;
	}
	//instant date method calculation
	public function getdatevalue($reportdatemethod,$date) {
		$daterange=array();
		switch ($reportdatemethod) {
			case '-1 day':
				$startdate=strtotime ('-1 day', strtotime ($date)) ;
				$startdate=date("Y-m-d",$startdate) ;
				$enddate=$startdate;
				$daterange=array('start'=>$startdate,'end'=>$enddate);
				break;
			case '0 day':
				$startdate=$date ;
				$enddate=$date;
				$daterange=array('start'=>$startdate,'end'=>$enddate);
				break;
			case '1 day':
				$startdate=strtotime ('+1 day' , strtotime ( $date )) ;
				$startdate=date("Y-m-d",$startdate) ;
				$enddate=$startdate;
				$daterange=array('start'=>$startdate,'end'=>$enddate);
				break;
			case '-6 day':
				$startdate=strtotime ('-6 day', strtotime ($date)) ;
				$startdate=date("Y-m-d",$startdate) ;
				$enddate=$startdate;
				$daterange=array('start'=>$startdate,'end'=>$enddate);
				break;
			case '-29 day':
				$startdate=strtotime ('-29 day', strtotime ($date)) ;
				$startdate=date("Y-m-d",$startdate) ;
				$enddate=$startdate;
				$daterange=array('start'=>$startdate,'end'=>$enddate);
				break;
			case '-59 day':
				$startdate=strtotime ('-59 day', strtotime ($date)) ;
				$startdate=date("Y-m-d",$startdate) ;
				$enddate=$startdate;
				$daterange=array('start'=>$startdate,'end'=>$enddate);
				break;
			case '-89 day':
				$startdate=strtotime ('-89 day', strtotime ($date)) ;
				$startdate=date("Y-m-d",$startdate) ;
				$enddate=$startdate;
				$daterange=array('start'=>$startdate,'end'=>$enddate);
				break;
			case '-119 day':
				$startdate=strtotime ('-119 day', strtotime ($date)) ;
				$startdate=date("Y-m-d",$startdate) ;
				$enddate=$startdate;
				$daterange=array('start'=>$startdate,'end'=>$enddate);
				break;
			case '6 day':
				$startdate=strtotime ('6 day', strtotime ($date)) ;
				$startdate=date("Y-m-d",$startdate) ;
				$enddate=$startdate;
				$daterange=array('start'=>$startdate,'end'=>$enddate);
				break;
			case '29 day':
				$startdate=strtotime ('29 day', strtotime ($date)) ;
				$startdate=date("Y-m-d",$startdate) ;
				$enddate=$startdate;
				$daterange=array('start'=>$startdate,'end'=>$enddate);
				break;
			case '59 day':
				$startdate=strtotime ('59 day', strtotime ($date)) ;
				$startdate=date("Y-m-d",$startdate) ;
				$enddate=$startdate;
				$daterange=array('start'=>$startdate,'end'=>$enddate);
				break;
			case '89 day':
				$startdate=strtotime ('89 day', strtotime ($date)) ;
				$startdate=date("Y-m-d",$startdate) ;
				$enddate=$startdate;
				$daterange=array('start'=>$startdate,'end'=>$enddate);
				break;
			case '119 day':
				$startdate=strtotime ('119 day', strtotime ($date)) ;
				$startdate=date("Y-m-d",$startdate) ;
				$enddate=$startdate;
				$daterange=array('start'=>$startdate,'end'=>$enddate);
				break;
		}
		return $daterange;
	}
	//Assigned to employee fetch
	//groups
	public function empidfetchfromgroup($groupids) {
		$gropempids = array();
		$i=0;
		//fetch all group ids
		$tree_string=array();
		$ctress_string = array();
		$grpinfo = $this->groupdatainfofetch($groupids);
		$gropempids = $this->groupempidsfetch($grpinfo);
		return $gropempids;
	}
	//group info fetch
	public function groupdatainfofetch($groupdataids) {
		$grpdata = array();
		$groupids = explode(',',$groupdataids);
		//fetch all group ids
		$i=0;
		$empgrpid1 = $this->parentemployeegroup($groupids);
		$empgrpid2 = $this->parentemployeegroup($empgrpid1);
		$empgroupids = array_merge($groupids,$empgrpid1,$empgrpid2);
		$grpdatainfo = $this->db->select('employeegroupid,employeeid,usergroupid,userroleid,userroleandsubordinateid',false)->from('employeegroup')->where_in('employeegroup.employeegroupid',$empgroupids)->where('employeegroup.status',1)->get();
		foreach($grpdatainfo->result() as $dataset) {
			$grpdata[$i] = array('empid'=>$dataset->employeeid,'empgrpid'=>$dataset->usergroupid,'emproleid'=>$dataset->userroleid,'emprolesuordid'=>$dataset->userroleandsubordinateid);
			$i++;
		}
		return $grpdata;
	}
	//fetch parent employee group ids
	public function parentemployeegroup($groupids) {
		$ids = array();
		$i=0;
		$grpids = $this->db->select('employeegroup.usergroupid',false)->from('employeegroup')->where_in('employeegroup.employeegroupid',$groupids)->get();
		foreach($grpids->result() as $grpid) {
			$dataid = explode(',',$grpid->usergroupid);
			foreach($dataid as $id) {
				$ids[$i] = $id;
				$i++;
			}
		}
		return $ids;
	}
	//fetch group employee ids
	public function groupempidsfetch($grpinfo) {
		$grpempids = array();
		foreach($grpinfo as $grpdata) {
			$empids = array();
			//empid
			$empid = array();
			if($grpdata['empid'] != '') {
				$empid = explode(',',$grpdata['empid']);
			}
			//roleid
			$rempid = array();
			if($grpdata['emproleid'] != '') {
				$rempid = $this->empidfetchfromroles($grpdata['emproleid']);
			}
			//role and sub role id
			$rsempid = array();
			if($grpdata['emprolesuordid'] != '') {
				$rsempid = $this->empidfetchfromrolesandsubroles($grpdata['emprolesuordid']);
			}
			$empids = array_merge($empid,$rempid,$rsempid);
			$grpempids = array_merge($grpempids,$empids);
		}
		return $grpempids;
	}
	//roles
	public function empidfetchfromroles($roleids) {
		$roleempids = array();
		$i=0;
		$rolesid = explode(',',$roleids);
		$empinfo = $this->db->select('employee.employeeid',false)->from('employee')->where_in('employee.userroleid',$rolesid)->where('employee.status',1)->get();
		foreach($empinfo->result() as $ids) {
			$roleempids[$i] = $ids->employeeid;
			$i++;
		}
		return $roleempids;
	}
	//employee id fetch based on roles and sub ordinates
	public function empidfetchfromrolesandsubroles($roleid) {
		$rolesubempids = array();
		//role ids
		$uroleids = array();
		$m=0;
		$rolesid = explode(',',$roleid);
		$roleinfo = $this->db->select('userrole.userroleid',false)->from('userrole')->where_in('userrole.userparentrole',$rolesid)->get();
		foreach($roleinfo->result() as $rids) {
			$uroleids[$m] = $rids->userroleid;
			$m++;
		}
		//merge parent and child
		$uroleids = array_merge($rolesid,$uroleids);
		$roleids = array_unique($uroleids);
		//emp ids
		$i=0;
		$empinfo = $this->db->select('employee.employeeid',false)->from('employee')->where_in('employee.userroleid',$roleids)->where('employee.status',1)->get();
		foreach($empinfo->result() as $ids) {
			$rolesubempids[$i] = $ids->employeeid;
			$i++;
		}
		return $rolesubempids;
	}
	//fetch all employee ids of sub role
	public function subrolesempidfetch($roleid) {
		$rolesubempids = array();
		//role ids
		$uroleids = array();
		
		//get all sub role ids
		$uroleids=$this->subroleuserroleid($roleid);

		//merge parent and child
		$roleids = array_unique($uroleids);
		$roleids = (( count($roleids) >0)? $roleids : array(1));
		//emp ids
		$i=0;
		$empinfo = $this->db->select('employee.employeeid',false)->from('employee')->where_in('employee.userroleid',$roleids)->where('employee.status',1)->get();
		foreach($empinfo->result() as $ids) {
			$rolesubempids[$i] = $ids->employeeid;
			$i++;
		}
		return $rolesubempids;
	}
	//get all sub role ids	
	public function subroleuserroleid($roleid){ //1
		$tree = Array();		
		$tree = $this->getchildroleid($roleid);		
		foreach ($tree as $key => $val) {			
			$ids = $this->subroleuserroleid($val);
			$tree = array_merge($tree, $ids);
		}	
		return $tree;			
	}	
	//get child ids
	public function getchildroleid($role){ //2
		$query=$this->db->select('userroleid')->from('userrole')->where('userparentrole',$role)->get();
		$role_id=array();
		if($query->num_rows() > 0){
			foreach($query->result() as $info){
				if($role!=$info->userroleid) {
					$role_id[]=$info->userroleid;
				}
			}			
		}   
		return $role_id;
	}
	//fetch roleid based on roles and sub ordinates
	public function roleididfetchfromrolesandsubroles($roleid) {
		$rolesubempids = array();
		//role ids
		$uroleids = array();
		$m=0;
		$rolesid = explode(',',$roleid);
		$roleinfo = $this->db->select('userrole.userroleid',false)->from('userrole')->where_in('userrole.userparentrole',$rolesid)->get();
		foreach($roleinfo->result() as $rids) {
			$uroleids[$m] = $rids->userroleid;
			$m++;
		}
		//merge parent and child
		$uroleids = array_merge($rolesid,$uroleids);
		$roleids = array_unique($uroleids);
		return $roleids;
	}
	//get all employee group ids
	public function groupididfetchuserid($empid) {
		//group ids
		$groupids = array();
		$m=0;
		$roleinfo = $this->db->select('employeegroup.employeegroupid',false)->from('employeegroup')->where("FIND_IN_SET('$empid',employeegroup.employeeid) >", 0)->get();
		foreach($roleinfo->result() as $rids) {
			$groupids[$m] = $rids->employeegroupid;
			$m++;
		}
		return $groupids;
	}
	//get all superior role ids
	public function parentuserroleid($parent) {
		$result = $this->db->select('userparentrole')->from('userrole')->where_in('userroleid',$parent)->get();
		// display each child 
		foreach($result->result() as $m) {
			$parents[]=$m->userparentrole;
			if($m->userparentrole == 1 || $m->userparentrole == 2) {
				return $parents;
			} else {
				parentuserroleid( explode(',',$m->userparentrole) );
			}
		}
	}
	//fetch record created user id
	public function checkrecordcreateduser($partablename,$primaryid,$id,$moduleid) {
		$loguserid = $this->userid;
		$userid = $this->userid;
		$data = 0;
		//where conditions
		$wh = '1=1';
		if( $id!="" && $primaryid ) {
			$wh = $partablename.'.'.$primaryid.'='.$id;
		}
		$status = $partablename.'.status = 1';
		//fetch createuserid and convert as role,group
		$cuserid='';
		$cruserdata = $this->db->query('select createuserid from '.$partablename.' WHERE '.$wh.' AND '.$status);
		foreach($cruserdata->result() as $row) {
			$cuserid = $row->createuserid;
		}
		if($cuserid == $loguserid) { //check current user as record owner
			//fetch data
			$result = $this->db->query(' select '.$primaryid.' from '.$partablename.' where '.$primaryid.' = '.$id.' AND status=1 ');
			foreach($result->result() as $row) {
				$data = $row->$primaryid;
			}
		} else if($cuserid == 2){
			//fetch data
			$result = $this->db->query(' select '.$primaryid.' from '.$partablename.' where '.$primaryid.' = '.$id.' AND status=1 ');
			foreach($result->result() as $row) {
				$data = $row->$primaryid;
			}
		} else if($cuserid!='') { //check other user have permission
		
			//fetch role and group
			$cusruledata = array();
			$recuserroleid = $this->Basefunctions->fetchrecorduserroleid($cuserid);
			$groupids = $this->Basefunctions->fetchallempgrpids();
			if($recuserroleid != 1) {
				$cusruledata = $this->Basefunctions->customrulefetchbaseduserid($moduleid,$recuserroleid,$groupids);
			}
			//data share rule
			$rulecond = '';
			$custrulecond = '';
			$roleid = $this->userroleid;
			$ruleid = $this->moduleruleidfetch($moduleid);
			if( $ruleid == 0 || $ruleid == 1 || $ruleid == 2 ) {
				$assignemp = $this->checkassigntofield($partablename,'employeetypeid');
				/* $empids = $this->roleididfetchfromrolesandsubroles($roleid);
				$empid = implode(',',$empids); */
				$empids = $this->subrolesempidfetch($roleid);
				$subempid = implode(',',$empids);
				$empid = ( ($subempid!="")? $subempid.','.$userid : $userid);
				if($assignemp == 'true') {//assign to fields exits
					//custom rule
					if( sizeof($cusruledata) > 0 ) {
						$custrulecond = " AND ( ".$partablename.".createuserid IN (".$empid.") OR ( ";
						foreach($cusruledata as $key => $customrule) {
							if($customrule['accesstype'] != '1') {
								if($customrule['totypeid'] == '2') { //group
									$custrulecond .= "(".$partablename.".employeetypeid=".$customrule['totypeid']." AND ".$partablename.".employeeid=".$customrule['sharedto'].")";
									$custrulecond .= ' OR ';
								} else if( ($customrule['totypeid'] == '3' && $customrule['superiorallow'] == 'Yes') ) { //roles with superior
									$uroleids = array();
									$uroleids = $this->Basefunctions->parentuserroleid($customrule['sharedto']);
									array_push($uroleids,$customrule['sharedto']);
									$paruserrole = implode(',',$uroleids);
									$custrulecond .= "(".$partablename.".employeetypeid=".$customrule['totypeid']." AND ".$partablename.".employeeid IN (".$paruserrole.") )";
									$custrulecond .= ' OR ';
								} else if( ($customrule['totypeid'] == '3' && $customrule['sharedto'] == $roleid && $customrule['superiorallow'] == 'No') ) { //roles without superior
									$custrulecond .= "(".$partablename.".employeetypeid=".$customrule['totypeid']." AND ".$partablename.".employeeid=".$customrule['sharedto'].")";
									$custrulecond .= ' OR ';
								} else if( $customrule['totypeid'] == '4' ) { //role and subordinate
									$roleids = $this->roleididfetchfromrolesandsubroles($customrule['sharedto']);
									$roleid = implode(',',$roleids);
									$custrulecond .= "(".$partablename.".employeetypeid=".$customrule['totypeid']." AND ".$partablename.".employeeid IN (".$roleid.") )";
									$custrulecond .= ' OR ';
								}
							}
						}
						$custrulecond .= "(".$partablename.".employeetypeid=1 AND ".$partablename.".employeeid=".$this->Basefunctions->userid.")";
						$custrulecond .= " ) )";
					} else {
						$roleids = $this->roleididfetchfromrolesandsubroles($roleid);
						$usrroleids = implode(',',$roleids);
						$usrroleids = ( ($usrroleids!='')? $usrroleids:1);
						$usrgrpid =  $this->groupididfetchuserid($userid);
						$usergroupids = implode(',',$usrgrpid);
						$usergroupids = ( ($usergroupids!='')? $usergroupids:1);
						$rulecond = ( ($empid != '')? ' AND ( '.$partablename.'.createuserid IN ('.$empid.') OR ('.$partablename.'.employeetypeid=1 AND '.$partablename.'.employeeid='.$this->userid.') OR ('.$partablename.'.employeetypeid=3 AND '.$partablename.'.employeeid='.$roleid.') OR ('.$partablename.'.employeetypeid=4 AND '.$partablename.'.employeeid IN ('.$usrroleids.')) OR ('.$partablename.'.employeetypeid=2 AND '.$partablename.'.employeeid IN ('.$usergroupids.')) )' : ' AND 1=1' );
					}
				} else {//assign to fields not exits
					$empids = $this->empidfetchfromrolesandsubroles($roleid);
					$subempid = implode(',',$empids);
					$rulecond = ( ($empid != '')? ' AND '.$partablename.'.createuserid IN ('.$subempid.')' : ' AND 1=1' );
				}
			}
			$result = $this->db->query(' select '.$primaryid.' from '.$partablename.' WHERE '.$wh.$rulecond.$custrulecond.' AND '.$status);
			foreach($result->result() as $row) {
				$data = $row->$primaryid;
			}
		}
		return $data;
	}
	//For Enabling fetch record created user id
	public function enablerecordcreateduser($partablename,$primaryid,$id,$moduleid) {
		$loguserid = $this->userid;
		$userid = $this->userid;
		$data = 0;
		//where conditions
		$wh = '1=1';
		if( $id!="" && $primaryid ) {
			$wh = $partablename.'.'.$primaryid.'='.$id;
		}
		$status = $partablename.'.status = 2';
		//fetch createuserid and convert as role,group
		$cuserid='';
		$cruserdata = $this->db->query('select createuserid from '.$partablename.' WHERE '.$wh.' AND '.$status);
		foreach($cruserdata->result() as $row) {
			$cuserid = $row->createuserid;
		}
		if($cuserid == $loguserid) { //check current user as record owner
			//fetch data
			$result = $this->db->query(' select '.$primaryid.' from '.$partablename.' where '.$primaryid.' = '.$id.' AND status=2 ');
			foreach($result->result() as $row) {
				$data = $row->$primaryid;
			}
		} else if($cuserid!='') { //check other user have permission
			//fetch role and group
			$cusruledata = array();
			$recuserroleid = $this->Basefunctions->fetchrecorduserroleid($cuserid);
			$groupids = $this->Basefunctions->fetchallempgrpids();
			if($recuserroleid != 1) {
				$cusruledata = $this->Basefunctions->customrulefetchbaseduserid($moduleid,$recuserroleid,$groupids);
			}
			//data share rule
			$rulecond = '';
			$custrulecond = '';
			$roleid = $this->userroleid;
			$ruleid = $this->moduleruleidfetch($moduleid);
			if( $ruleid == 0 || $ruleid == 1 || $ruleid == 2 ) {
				$assignemp = $this->checkassigntofield($partablename,'employeetypeid');
				/* $empids = $this->roleididfetchfromrolesandsubroles($roleid);
				$empid = implode(',',$empids); */
				$empids = $this->subrolesempidfetch($roleid);
				$subempid = implode(',',$empids);
				$empid = ( ($subempid!="")? $subempid.','.$userid : $userid);
				if($assignemp == 'true') {//assign to fields exits
					//custom rule
					if( sizeof($cusruledata) > 0 ) {
						$custrulecond = " AND ( ".$partablename.".createuserid IN (".$empid.") OR ( ";
						foreach($cusruledata as $key => $customrule) {
							if($customrule['accesstype'] != '1') {
								if($customrule['totypeid'] == '2') { //group
									$custrulecond .= "(".$partablename.".employeetypeid=".$customrule['totypeid']." AND ".$partablename.".employeeid=".$customrule['sharedto'].")";
									$custrulecond .= ' OR ';
								} else if( ($customrule['totypeid'] == '3' && $customrule['superiorallow'] == 'Yes') ) { //roles with superior
									$uroleids = array();
									$uroleids = $this->Basefunctions->parentuserroleid($customrule['sharedto']);
									array_push($uroleids,$customrule['sharedto']);
									$paruserrole = implode(',',$uroleids);
									$custrulecond .= "(".$partablename.".employeetypeid=".$customrule['totypeid']." AND ".$partablename.".employeeid IN (".$paruserrole.") )";
									$custrulecond .= ' OR ';
								} else if( ($customrule['totypeid'] == '3' && $customrule['sharedto'] == $roleid && $customrule['superiorallow'] == 'No') ) { //roles without superior
									$custrulecond .= "(".$partablename.".employeetypeid=".$customrule['totypeid']." AND ".$partablename.".employeeid=".$customrule['sharedto'].")";
									$custrulecond .= ' OR ';
								} else if( $customrule['totypeid'] == '4' ) { //role and subordinate
									$roleids = $this->roleididfetchfromrolesandsubroles($customrule['sharedto']);
									$roleid = implode(',',$roleids);
									$custrulecond .= "(".$partablename.".employeetypeid=".$customrule['totypeid']." AND ".$partablename.".employeeid IN (".$roleid.") )";
									$custrulecond .= ' OR ';
								}
							}
						}
						$custrulecond .= "(".$partablename.".employeetypeid=1 AND ".$partablename.".employeeid=".$this->Basefunctions->userid.")";
						$custrulecond .= " ) )";
					} else {
						$roleids = $this->roleididfetchfromrolesandsubroles($roleid);
						$usrroleids = implode(',',$roleids);
						$usrroleids = ( ($usrroleids!='')? $usrroleids:1);
						$usrgrpid =  $this->groupididfetchuserid($userid);
						$usergroupids = implode(',',$usrgrpid);
						$usergroupids = ( ($usergroupids!='')? $usergroupids:1);
						$rulecond = ( ($empid != '')? ' AND ( '.$partablename.'.createuserid IN ('.$empid.') OR ('.$partablename.'.employeetypeid=1 AND '.$partablename.'.employeeid='.$this->userid.') OR ('.$partablename.'.employeetypeid=3 AND '.$partablename.'.employeeid='.$roleid.') OR ('.$partablename.'.employeetypeid=4 AND '.$partablename.'.employeeid IN ('.$usrroleids.')) OR ('.$partablename.'.employeetypeid=2 AND '.$partablename.'.employeeid IN ('.$usergroupids.')) )' : ' AND 1=1' );
					}
				} else {//assign to fields not exits
					$empids = $this->empidfetchfromrolesandsubroles($roleid);
					$subempid = implode(',',$empids);
					$rulecond = ( ($empid != '')? ' AND '.$partablename.'.createuserid IN ('.$subempid.')' : ' AND 1=1' );
				}
			}
			$result = $this->db->query(' select '.$primaryid.' from '.$partablename.' WHERE '.$wh.$rulecond.$custrulecond.' AND '.$status);
			foreach($result->result() as $row) {
				$data = $row->$primaryid;
			}
		}
		return $data;
	}
	//fetch userroleid of current record
	public function fetchrecorduserroleid($userid) {
		$roleid = 1;
		$dataset = $this->db->select('userroleid')->from('employee')->where('employeeid',$userid)->where('status',1)->get();
		foreach($dataset->result() as $row) {
			$roleid = $row->userroleid;
		}
		return $roleid;
	}
	//fetch all employee group ids
	public function fetchallempgrpids() {
		$empgrpid = array();
		$i=0;
		$dataset = $this->db->select('employeegroupid')->from('employeegroup')->where('status',1)->get();
		foreach($dataset->result() as $row) {
			$empgrpid[$i] = $row->employeegroupid;
			$i++;
		}
		$grpids = implode(',',$empgrpid);
		return $grpids;
	}
	//module custom sharing rule info fetch
	public function customrulefetchbaseduserid($moduleid,$roleid,$grpid) {
		$subroleids = $this->roleididfetchfromrolesandsubroles($roleid);
		$subroleid = implode(',',$subroleids);
		$data = array();
		$i=0;
		$this->db->select('datasharecustomruleid,moduleid,accesstype,superiorallow,recordsharedfromtypeid,recordsharedfrom,recordsharedtotypeid,recordsharedto,status',false);
		$this->db->from('datasharecustomrule');
		$this->db->where_in('datasharecustomrule.moduleid',$moduleid);
		$this->db->where_not_in('datasharecustomrule.status',array(3,0));
		$this->db->where('( (recordsharedfromtypeid=2 AND recordsharedfrom IN ('.$grpid.')) OR (recordsharedfromtypeid=3 AND recordsharedfrom IN ('.$roleid.')) OR (recordsharedfromtypeid=4 AND recordsharedfrom IN ('.$subroleid.')) )');
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('moduleid'=>$row->moduleid,'accesstype'=>$row->accesstype,'superiorallow'=>$row->superiorallow,'fromtypeid'=>$row->recordsharedfromtypeid,'totypeid'=>$row->recordsharedtotypeid,'sharedfrom'=>$row->recordsharedfrom,'sharedto'=>$row->recordsharedto,'status'=>$row->status);
				$i++;
			}
		}
		return $data;
	}
	//sms send - parameters
	public function sendsmsinallmodules($apikey,$senderid,$mobilenum,$textmsg,$scheduledate,$primaryid,$moduleid) {
		if($scheduledate != '') {
			$communicationmode = 'SCHEDULE SMS';
		} else {
			$communicationmode = 'SENT SMS';
		}
		$lengthofchar = strlen($textmsg);
		if($lengthofchar <= '160') {
			$smscount = 1;
		} else if(($lengthofchar > '160') && ($lengthofchar <= '306')) {
			$smscount = 2;	
		} else if(($lengthofchar > '306') && ($lengthofchar <= '459')) {
			$smscount = 3;	
		} else if(($lengthofchar > '459') && ($lengthofchar <= '612')) {
			$smscount = 4;	
		} else if(($lengthofchar > '612') && ($lengthofchar <= '765')) {
			$smscount = 5;
		} else if(($lengthofchar > '765')&& ($lengthofchar <= '922')) {
			$smscount = 6;	
		} else if(($lengthofchar > '922')) {
			$smscount = 7;	
		}
		$data='<?xml version="1.0" encoding="UTF-8"?>
		<api>
		<sms>
		<to>'.$mobilenum.'</to>
		<message>'.$textmsg.'</message>
		<msgid>'.$senderid.'</msgid>
		<sender>'.$senderid.'</sender>
		</sms>
		<response>Y</response>
		<unicode>N</unicode>
		<flash>N</flash>
		<time>'.$scheduledate.'</time>
		</api>';
		$url = "https://api-alerts.kaleyra.com/v4/?api_key="."$apikey&method=sms.xml&xml=".urlencode($data)."&type=json";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$output = curl_exec($ch);
		curl_close($ch);  
		$json_result = json_decode($output, true);
		sleep(15);
		foreach($json_result['data'] as $key => $value) {
			$statusurl = "https://api-alerts.kaleyra.com/v4/?api_key="."$apikey&method=sms.status&id=".$value['id']."";
			$chk=curl_init();
			curl_setopt($chk, CURLOPT_URL, $statusurl);
			curl_setopt($chk, CURLOPT_RETURNTRANSFER, true);
			$statusoutput=curl_exec($chk);
			curl_close($chk);
			$status_update = json_decode($statusoutput, true);
			if(empty($status_update['data'])) {
				$communicationto_update = $mobilenum;
				$groupid_update = $value['id'];
				$communicationstatus_update = $value['status'];
			} else {
				$communicationto_update = $status_update['data'][0]['mobile'];
				$groupid_update = $status_update['data'][0]['id'];
				$communicationstatus_update = $status_update['data'][0]['status'];
			}
			//comm log
			$smslog=array( 
				'commonid'=>$primaryid,
				'moduleid'=>$moduleid,
				'employeeid'=>$this->userid,
				'smstypeid'=>2,
				'smssettingsid'=>2,
				'smssendtypeid'=>2,
				'templatesid'=>1,
				'signatureid'=>1,
				'crmcommunicationmode'=>$communicationmode,
				'communicationfrom'=>0,
				'communicationto'=>$communicationto_update,
				'smscount'=>$smscount,
				'message'=>$textmsg,
				'groupid'=>$groupid_update,
				'communicationstatus'=>$communicationstatus_update,
				'communicationdate'=>date($this->datef),
				'industryid'=>$this->industryid,
				'createdate'=>date($this->datef),
				'lastupdatedate'=>date($this->datef),
				'createuserid'=>$this->userid,
				'lastupdateuserid'=>$this->userid,
				'status'=>$this->activestatus
			);
			$this->db->insert('crmcommunicationlog',$smslog);
			$primaryid = $this->maximumid('crmcommunicationlog','crmcommunicationlogid');
			//notification log entry
			$empid = $this->logemployeeid;
			$empname = $this->generalinformaion('employee','employeename','employeeid',$empid);
			$notimsg = $empname." "."Sent a SMS to"." ".$communicationto_update;
			$this->notificationcontentadd($primaryid,'SMS',$notimsg,1,210);
			//if($smssendtype == 2) {
				$crediturl = "https://api-alerts.kaleyra.com/v4/?api_key=$apikey&method=account.credits";
			/* } else if($smssendtype == 3) {
				$crediturl = "https://api-promo.kaleyra.com/v4/?api_key=$apikey&method=account.credits";
			} */
			$crdurl=curl_init();
			curl_setopt($crdurl, CURLOPT_URL, $crediturl);
			curl_setopt($crdurl, CURLOPT_RETURNTRANSFER, true);
			$creditoutput = curl_exec($crdurl);
			curl_close($crdurl);
			$jsoncreditresult = json_decode($creditoutput, true);
			foreach($jsoncreditresult['data'] as $key => $value) {
				if($value == '') {
					$value = '100';
				}
				$credit = $this->db->query("UPDATE `salesneuronaddonscredit` SET `addonavailablecredit` = $value WHERE `addonstypeid` = 2");
			}
		}  
	}
	//inline user data base set
	public function inlineuserdatabaseactive($databasename) {
		$host = $this->db->hostname;
		$user = $this->db->username;
		$passwd = $this->db->password;
		$dbconfig['hostname'] = $host;
		$dbconfig['username'] = $user;
		$dbconfig['password'] = $passwd;
		$dbconfig['database'] = $databasename;
		$dbconfig['dbdriver'] = 'mysqli';
		$dbconfig['dbprefix'] = '';
		$dbconfig['pconnect'] = TRUE;
		$dbconfig['db_debug'] = TRUE;
		$dbconfig['cache_on'] = FALSE;
		$dbconfig['cachedir'] = '';
		$dbconfig['char_set'] = 'utf8';
		$dbconfig['dbcollat'] = 'utf8_general_ci';
		$dbconfig['swap_pre'] = '';
		$dbconfig['autoinit'] = TRUE;
		$dbconfig['stricton'] = FALSE;
		return $import_db = $this->load->database($dbconfig, TRUE);
	}
	//fetch xml content
	public function parse_response($response) {
		$xml = new SimpleXMLElement($response);
		$result = array();
		$h=0;
		foreach($xml as $key => $value) {
			if( $key == 'to' || $key == 'msgid' || $key == 'status' ) {
				$result[$h][(string)$key] = (string)$value;
				if( count($result[$h]) == '3' ) {
					$h++;
				}
			} else if($key == 'errcode' || $key == 'desc'){
				$result[$h][(string)$key] = (string)$value;
				if( count($result[$h]) == '2' ) {
					$h++;
				}
			} 
		}
		return $result;
	}
	//retrieves whether the record is active now
	public function getcurrentstatus($reportid) {
		$data=$this->db->select('status')
						->from('report')
						->where('reportid',$reportid)
						->get()->result();
		foreach($data as $inf){
			$status = $inf->status;
		}
		return $status;
	}
	//check the data is active or deleted(used on notifications/audit log hyperlink)
	public function checkvalidrecord(){
		$id=trim($_GET['id']);
		$table=trim($_GET['table']);
		$tablefield=$table.'id';
		$msg = false;
		$data = $this->db->select($tablefield)->from($table)->where($tablefield,$id)->where('status',1)->limit(1)->get();
		if($data->num_rows() > 0){
			$msg = true;
		}
		echo json_encode (array('status'=>$msg));
	}
	/*
	*get the exisiting conversion rates between these currencies
	* @param $fromcurrency-currency to be converted
	* @param $tocurrency-the Basecurrency
	*/
	public function getcurrencyconversionrate($fromcurrency,$tocurrency,$quotedate){
		$rate='';		
		$quotedate = date('Y-m-d',strtotime($quotedate));
		$data = $this->db->select('multiplyrate')
									->from('currencyconversion')
									->where('currencyid',$fromcurrency) //basecurrency
									->where('currencytoid',$tocurrency) //given currency
									->where("DATE_FORMAT(STR_TO_DATE(validfrom,'%Y-%m-%d'),'%Y-%m-%d') <=",$quotedate)
									->where("DATE_FORMAT(STR_TO_DATE(validto,'%Y-%m-%d'),'%Y-%m-%d') >=",$quotedate)
									->where('status',$this->Basefunctions->activestatus)
									->limit(1)
									->get();
		if($data->num_rows() > 0){
			foreach($data->result() as $info){
				$rate = $info->multiplyrate;
			}
		} 
		return $rate;
	}
	/*
	* Get the default currency ROUNDING values.
	*/
	public function getdefaultcurrencys() {
		$userid=$this->Basefunctions->userid;		
		$data=$this->db->select('company.companyid,company.currencyid,decimalplaces')
						->from('company')
						->join('branch','branch.companyid=company.companyid')
						->join('employee','employee.branchid=branch.branchid')
						->join('currency','currency.currencyid=company.currencyid')
						->where('employee.employeeid',$userid)
						->limit(1)
						->get();
		$row=$data->row();
		$roundvalue = $row->decimalplaces;
		return $roundvalue;
	}
	/*
	*Delete the Payment made from modules->Invoice/
	*/
	public function deletemodulepayment($moduleid,$transactionid) {
		if($moduleid > 0 && $transactionid > 0){
			$ludate = date($this->Basefunctions->datef);
			$this->db->query("UPDATE payment SET status='".$this->Basefunctions->deletestatus."',lastupdatedate='".$ludate."',lastupdateuserid='".$this->Basefunctions->userid."' WHERE  moduleid='".$moduleid."' AND transactionid = '".$transactionid."' ");
		}
	}
	/*
	*Datashare rule based records-implemented for widget data listing
	*@param integer $moduleid -pass the module
	*return array custom/main rule data for query
	*/
	public function datasharerulebasedrow($moduleid,$maintable){
		$ruleid =  $this->moduleruleidfetch($moduleid);
		$cusruledata = $this->customrulefetch($moduleid);
		$roleid = $this->userroleid;
		$userid = $this->userid;
		$custrulecond = " ";
		$rulecond="";
		if($ruleid == 0) {
			$assignemp = $this->checkassigntofield($maintable,'employeetypeid');
			$empids = $this->subrolesempidfetch($roleid);
			$subempid = implode(',',$empids);
			$empid = ( ($subempid!="")? $subempid.','.$userid : $userid);
			if($assignemp == 'true') {
				//custom rule
				if( sizeof($cusruledata) > 0 ) {
					$custrulecond = " AND ( ".$maintable.".createuserid IN (".$empid.") OR ( ";
					foreach($cusruledata as $key => $customrule) {
						if($customrule['totypeid'] == '2') { //group
							$custrulecond .= "(".$maintable.".employeetypeid=".$customrule['totypeid']." AND ".$maintable.".employeeid=".$customrule['sharedto'].")";
							$custrulecond .= ' OR ';
						} else if( ($customrule['totypeid'] == '3' && $customrule['sharedto'] == $roleid) ) { //roles
							$custrulecond .= "(".$maintable.".employeetypeid=".$customrule['totypeid']." AND ".$maintable.".employeeid=".$customrule['sharedto'].")";
							$custrulecond .= ' OR ';
						} else if( $customrule['totypeid'] == '4' ) { //role and subordinate
							$roleids = $this->roleididfetchfromrolesandsubroles($customrule['sharedto']);
							$roleid = implode(',',$roleids);
							$custrulecond .= "(".$maintable.".employeetypeid=".$customrule['totypeid']." AND ".$maintable.".employeeid IN (".$roleid.") )";
							$custrulecond .= ' OR ';
						}
					}
					$custrulecond .= "(".$maintable.".employeetypeid=1 AND ".$maintable.".employeeid=".$this->userid.")";
					$custrulecond .= " ) )";
				} else {
					$rulecond = ( ($empid != '')? ' AND ('.$maintable.'.createuserid IN ('.$empid.') OR '.$maintable.'.employeetypeid=1 AND '.$maintable.'.employeeid='.$this->userid.')' : ' AND 1=1' );
				}
			} else {
				$rulecond = ( ($empid != '')? ' AND '.$maintable.'.createuserid IN ('.$empid.')' : ' AND 1=1' );
			}
		}
		$array = array('custrulecond'=>$custrulecond,'rulecond'=>$rulecond);
		return	$array;
	}
	/*
	*Retrives the Widget related data based on"widget data mapping".
	*@param $moduleid the moduleid
	*@param $widgetid related widget
	*/
	public function getwidgetrelateddata($primaryid,$moduleid,$widgetid) {
		$wh=array('widgetdatamapping.moduleid'=>$moduleid,'widgetdatamapping.widgetid'=>$widgetid,'widgetdatamapping.status'=>$this->activestatus);
		$w_data=$this->db->select('widgetdatamapping.frommodulefieldid,widgetdatamapping.tomodulefieldid,
									fromw.columnname as fromcolumnname,tow.columnname as tocolumnname,
									fromw.tablename as fromtablename,tow.tablename as totablename,
									fromw.parenttable as fromparenttable,tow.parenttable as toparenttable,
									fromw.fieldname as fromfieldname,tow.fieldname as tofieldname,
									fromw.uitypeid as fromuitypeid,tow.uitypeid as touitypeid',false)
							->from('widgetdatamapping')
							->join('modulefield as fromw','fromw.modulefieldid=widgetdatamapping.frommodulefieldid')
							->join('modulefield as tow','tow.modulefieldid=widgetdatamapping.tomodulefieldid')
							->where($wh)
							->get();
		if($w_data->num_rows() > 0){
			foreach($w_data->result() as $info){
				$field_from_to[]= array(
								'frommodulefieldid'=>$info->frommodulefieldid,'tomodulefieldid'=>$info->tomodulefieldid,
								'fromcolumnname'=>$info->fromcolumnname,'tocolumnname'=>$info->tocolumnname,
								'fromtablename'=>$info->fromtablename,'totablename'=>$info->totablename,
								'fromparenttable'=>$info->fromparenttable,'toparenttable'=>$info->toparenttable,
								'fromfieldname'=>$info->fromfieldname,'tofieldname'=>$info->tofieldname,
								'fromuitypeid'=>$info->fromuitypeid,'touitypeid'=>$info->touitypeid);
				//table and fields information
				if($info->fromuitypeid == $info->touitypeid) {
					$formfieldsname[]= $info->fromfieldname;
					$formfieldstable[] = $info->fromtablename;
					$formfieldscolmname[] = $info->fromcolumnname;
					$elementpartable[] = $info->fromparenttable;
					$tofieldname[] = $info->tofieldname;
					$touitypeid[] = $info->touitypeid;
					$relateto[$info->fromcolumnname]=$info->tofieldname;
				} else {
					if($info->fromtablename.'id' == $info->tocolumnname) {
						$formfieldsname[]= $info->tofieldname;
						$formfieldstable[] = $info->fromtablename;
						$formfieldscolmname[] = $info->fromtablename.'id';
						$elementpartable[] = $info->fromparenttable;
						$tofieldname[] = $info->tofieldname;
						$touitypeid[] = $info->touitypeid;
						$relateto[$info->tofieldname]=$info->tofieldname;
					}
				}				
			}				
			$restricttable = array();				
			//filter unique table
			$this->load->model('Base/Crudmodel');
			$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
			//filter parent table
			$partablename =  $this->Crudmodel->filtervalue($elementpartable);
			//primary key
			$primaryname = $this->Crudmodel->primaryinfo($partablename);
			$data = $this->widgetdbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
			//return $data;
			if(count($data) > 0){
				foreach($relateto as $key=>$value){
					$final[$value] = $data[$key];
				}
				return json_encode(array('wdata'=>$final,'field'=>$tofieldname,'fielduitype'=>$touitypeid,'mstatus'=>'Success'));
			} else {
				return json_encode (array('mstatus'=>'Failed2'));
			}			
		} else {
			return json_encode (array('mstatus'=>'Failed2'));
		}
	}
	//widget  related data set fetch
	public function widgetdbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid) {
		$loguserid = $this->Basefunctions->userid;
		$userid = $this->Basefunctions->userid;
		//select field infromation
		//$formfieldscolmname[] = $primaryname;
		$formfieldstable[] = $partablename;
		$newdata = array();
		$i=0;
		foreach($formfieldscolmname as $data) {
			$newdata[] = $formfieldstable[$i].'.'.$data;
			$i++;
		}
		//final selected data
		$selectdata = implode(',',$newdata);
		//text box information
		//$formfieldsname[] = 'primarydataid';
		
		//join child table with table
		$joinq = "";
		$fildstable = explode(',',$tablename );
		foreach($fildstable as $tabname) {
			if(strcmp($partablename,$tabname) != 0) {
				$joinq=$joinq.' LEFT OUTER JOIN '.$tabname.' ON '.$tabname.'.'.$primaryname.'='.$partablename.'.'.$primaryname;
			}
		}
		//where conditions
		$wh = '1=1'; 
		/* if($primaryid == 1) {
			$wh = $partablename.'.'.$primaryname.'!='.$primaryid;
		} else  */
		if( $primaryid!="" && $primaryname ) {
			$wh = $partablename.'.'.$primaryname.'='.$primaryid;
		}
		$status = $partablename.'.status NOT IN (0)';	
	
		//fetch data
		$data = $this->db->query('select '.$selectdata.' from '.$partablename.' '.$joinq.' WHERE '.$wh.' AND '.$status);
		if($data->num_rows() >0) { 
			foreach($data->result()as $datarow) {
				$i=0;
				foreach($formfieldsname as $row) {
					/* echo $formfieldscolmname[$i];
					echo $row;
					echo $datarow->$formfieldscolmname[$i]; */
					$resdata[$row]=trim($datarow->$formfieldscolmname[$i]);
					$i++;
				}
			}
			//print_r($resdata);die();
			return $resdata;
		} else {
		   return '';
		}
	}
	/*
	*Retrives the quote or salesorder or invoice or purchaseorder data on module for data transmissions.
	*@param 
	*/
	//Folder main grid view
	public function getmoduledatanumber($tablename,$sortcol,$sortord,$pagenum,$rowscount,$module) {
		if (in_array($module, [216,85,94])) {
			$tablename = 'quote';
			$dataset = 'quoteid,quotenumber,quotedate,account.accountname,crmstatus.crmstatusname';
			$join =' LEFT OUTER JOIN account ON account.accountid=quote.accountid';
			$join .=' LEFT OUTER JOIN crmstatus ON crmstatus.crmstatusid=quote.crmstatusid';
			$status = 'quote.status IN (1,2)';
			$where = 'quote.crmstatusid IN (33)';
		}
		else if (in_array($module, [217,87,96])){
			$tablename = 'salesorder';
			$dataset = 'salesorderid,salesordernumber,salesorderdate,account.accountname,crmstatus.crmstatusname';
			$join =' LEFT OUTER JOIN account ON account.accountid=salesorder.accountid';
			$join .=' LEFT OUTER JOIN crmstatus ON crmstatus.crmstatusid=salesorder.crmstatusid';
			$status = 'salesorder.status IN (1,2)';
			$where = 'salesorder.crmstatusid IN (42)';
		}
		else if (in_array($module, [226,86,95])){
			$tablename = 'invoice';
			$dataset = 'invoiceid,invoicenumber,invoicedate,account.accountname,crmstatus.crmstatusname';
			$join =' LEFT OUTER JOIN account ON account.accountid=invoice.accountid';
			$join .=' LEFT OUTER JOIN crmstatus ON crmstatus.crmstatusid=invoice.crmstatusid';
			$status = 'invoice.status IN (1,2)';
			$where = 'invoice.crmstatusid IN (38)';
		}
		else if($module == 249){
			$tablename = 'materialrequisition';
			$dataset = 'materialrequisitionid,materialrequisitionnumber,requisitiondate,account.accountname,requisitionstatus.requisitionstatusname';
			$join =' LEFT OUTER JOIN account ON account.accountid=materialrequisition.accountid';
			$join .=' LEFT OUTER JOIN requisitionstatus ON requisitionstatus.requisitionstatusid=materialrequisition.requisitionstatusid';
			$status = 'materialrequisition.status IN (1,2)';
			$where = 'materialrequisition.requisitionstatusid IN (5)';
		}
		else if (in_array($module, [225,88,99])) {
			$tablename = 'purchaseorder';
			$dataset = 'purchaseorderid,purchaseordernumber,purchaseorderdate,account.accountname,crmstatus.crmstatusname';
			$join =' LEFT OUTER JOIN account ON account.accountid=purchaseorder.toaccountid';
			$join .=' LEFT OUTER JOIN crmstatus ON crmstatus.crmstatusid=purchaseorder.crmstatusid';
			$status = 'purchaseorder.status IN (1,2)';
			$where = 'purchaseorder.crmstatusid IN (48)';
		}
		//pagination
		$query = 'select '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$where.' AND '.$status.' ORDER BY'.' '.$sortcol.' '.$sortord;
		$page = $this->Basefunctions->generatepage($query,$pagenum,$rowscount);
		//query
		$data = $this->db->query('select '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$where.' AND '.$status.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$page['start'].','.$page['records'].'');
		$finalresult=array($data,$page,'');
		return $finalresult;
	}
	/*
	* to get the converstion data of the module
	*/
	public function getconversiondata() {
		$other_details = '';
		$primaryid = $_POST['primaryid']; //quoteid or soid or invoiceid
		$moduleid=$_POST['moduleid'];
		$tomoduleid=$_POST['tomoduleid'];
		$frommodulename = $_POST['frommodule'];
		$primaryfield=$frommodulename.'id';
		$whclause = array('a.tablename'=>$frommodulename,'moduleid'=>$moduleid,'mappingmoduleid'=>$tomoduleid);
		$datamap = $this->db->select('map.moduleconversionmappingid,map.mappingmoduleid,map.moduleid,
									 a.columnname as fromcolumnname, b.columnname as tocolumnname,
									 a.fieldname as fromfieldname, b.fieldname as tofieldname,
									 a.tablename as fromtablename,b.tablename as totablename,
									 a.parenttable as fromparenttable,b.parenttable as toparenttable,
									 a.uitypeid as fromuitypeid,b.uitypeid as touitypeid')
								->from('moduleconversionmapping as map')
								->join('modulefield as a','map.frommodulefieldid=a.modulefieldid')							
								->join('modulefield as b','map.tomodulefieldid=b.modulefieldid')							
								->where($whclause)
								->where_not_in('map.frommodulefieldid',array(1))
								->where_not_in('map.tomodulefieldid',array(1))								
								->where('map.status',$this->Basefunctions->activestatus)
								->get();
		foreach($datamap->result() as $info){			
			$sourcefieldcolumn[]=$info->fromtablename.'.'.$info->fromcolumnname;
			$modulemappingarray[$info->tofieldname] = $info->fromcolumnname;
		}
		//retrieve the data from corresponding tables		
		$sourcefieldcolumn = array_filter($sourcefieldcolumn);
		$selectdata = implode(',',$sourcefieldcolumn);
		$joinq='';
		$wh = $frommodulename.'id='.$primaryid;
		$maindata = $this->db->query('select '.$selectdata.' from '.$frommodulename.' '.$joinq.' WHERE '.$wh);
		foreach($maindata->result() as $key=>$value){
			$output[]=$value;
		}
		$maindatas=json_decode(json_encode($output),true);
		//one to one mapping ex:quote to invoice field equivalent mapping
		$moduledata['main']=$maindatas[0];
		foreach($modulemappingarray as $key=>$value){
			$main_m[$key] = $moduledata['main'][$value];
		}
		$moduledata['main']=$main_m;
		//address data
		if($frommodulename != 'materialrequisition') {
			$moduledata['address']=$this->moduleaddressdata($frommodulename,$primaryid);
			if($frommodulename != 'contact' && $frommodulename != 'contract'){
				//?retrieve summary data
				$ext_data=$this->db->select('pricebookcurrencyid,currentcurrencyid,pricebook_currencyconvrate,taxmodeid,additionalchargemodeid,groupdiscounttypeid,groupdiscountpercent,adjustmenttypeid,adjustmentamount')
				->from($frommodulename)
				->where($primaryfield,$primaryid)
				->limit(1)
				->get();
				foreach($ext_data->result() as $value){
					$other_details = array('pricebookcurrencyid'=>$value->pricebookcurrencyid,'currentcurrencyid'=>$value->currentcurrencyid,'pricebook_currencyconvrate'=>$value->pricebook_currencyconvrate);
					$detail_parameter=array('id'=>$primaryid,'taxmode'=>$value->taxmodeid,
							'chargemode'=>$value->additionalchargemodeid,'discounttype'=>$value->groupdiscounttypeid,'discountpercent'=>$value->groupdiscountpercent,'adjustmenttype'=>$value->adjustmenttypeid,'adjustmentvalue'=>$value->adjustmentamount);
				}
			}			
		} else {
			$moduledata['address']='';
			if($frommodulename != 'contact' && $frommodulename != 'contract'){
				$ext_data=$this->db->select('groupdiscounttypeid,groupdiscountpercent,adjustmenttypeid,adjustmentamount')
				->from($frommodulename)
				->where($primaryfield,$primaryid)
				->limit(1)
				->get();
				foreach($ext_data->result() as $value){
					$detail_parameter=array('id'=>$primaryid,'taxmode'=>0,
							'chargemode'=>0,'discounttype'=>$value->groupdiscounttypeid,'discountpercent'=>$value->groupdiscountpercent,'adjustmenttype'=>$value->adjustmenttypeid,'adjustmentvalue'=>$value->adjustmentamount);
				}
			}			
		}
		if(isset($detail_parameter)){
			$moduledata['summary']=$this->summary_groupdetail($moduleid,$detail_parameter);
		}else{
			$moduledata['summary']='';
		}
		if(isset($other_details)){
			$moduledata['pricedetails']=$other_details;
		}else{
			$moduledata['pricedetails']='';
		}		
		//line details
		if($moduleid == 82){
			$linedetail = $this->retrievelinedetail($moduleid,$primaryid,$frommodulename,$tomoduleid);
			$moduledata['linedetail']= json_encode($linedetail['productdetail']);
			$moduledata['netamount'] = $linedetail['netamount'];
		}else if($moduleid == 80){
			$linedetail = $this->retrievelinedetail($moduleid,$primaryid,$frommodulename,$tomoduleid);
			$moduledata['linedetail']= json_encode($linedetail['productdetail']);
			$moduledata['netamount'] = $linedetail['netamount'];
		}else{
			$moduledata['linedetail'] = $this->retrievelinedetail($moduleid,$primaryid,$frommodulename,$tomoduleid);
		}		
		$moduledata['paymentdetail']=$this->retrievepaymentdetail($moduleid,$primaryid,$frommodulename,$tomoduleid);
		echo json_encode($moduledata);
	}
	//summary group details
	public function summary_groupdetail($moduleid,$param) {
		$discount_json='';
		$tax_json='';
		$charge_json='';
		$adjustment_json='';
		if($param['discounttype'] > 1){			
			$discount_json = array('typeid'=>$param['discounttype'],'value'=>$param['discountpercent']);			
		}		
		if($param['taxmode'] > 1){			
			$tax_json=$this->gettaxdetail($moduleid,$param['taxmode'],$param['id']);
		}
		if($param['chargemode'] > 1){			
			$charge_json=$this->getchargedetail($moduleid,$param['chargemode'],$param['id']);			
		}
		if($param['adjustmenttype'] > 1){			
			$adjustment_json = array('typeid'=>$param['adjustmenttype'],'value'=>$param['adjustmentvalue']);		
		}	
		
	    $retarray = array('grouptax'=>json_encode($tax_json),'groupdiscount'=>json_encode($discount_json),'groupaddcharge'=>json_encode($charge_json),'groupadjustment'=>json_encode($adjustment_json));
	    return $retarray;
	}
	/**
	* Retrieve Tax details for the module records.
	* @param $moduleid - specified moduleid ex(Quote/so/po)
	* @param $mode     - mode of records(group/individual)
	* @param $id       - transaction id, ex(quoteid,soid,..)
	*/
	public function gettaxdetail($moduleid,$mode,$id) {
		$taxdata=$this->db->select('moduletaxdetailid,moduletaxdetail.singlegrouptypeid,moduletaxdetail.taxmasterid,moduletaxdetail.taxid,taxvalue,taxamount,taxruleid,taxname')
							->from('moduletaxdetail')
							->join('tax','tax.taxid=moduletaxdetail.taxid')
							->where('moduleid',$moduleid)
							->where('singlegrouptypeid',$mode)
							->where('moduletaxdetail.status',$this->Basefunctions->activestatus)
							->where_in('id',$id)
							->get();
		if($taxdata->num_rows() > 0){			
			foreach($taxdata->result() as $info) {
				$taxdataarray[]=array('moduletaxdetailid'=>$info->moduletaxdetailid,'taxid'=>$info->taxid,'rate'=>$info->taxvalue,'amount'=>$info->taxamount,'taxname'=>$info->taxname);
				$taxmasterid = $info->taxmasterid;
			}
			$taxdetails['id'] = $taxmasterid;
			$taxdetails['data']= $taxdataarray;			
		} else {
			$taxdetails = [];
		}
		return $taxdetails;
	}
	/**
	* Retrieve Charge details for the module records.
	* @param $moduleid - specified moduleid ex(Quote/so/po)
	* @param $mode     - mode of records(group/individual)
	* @param $id       - transaction id, ex(quoteid,soid,..)
	*/
	public function getchargedetail($moduleid,$mode,$id) {
		$adddata=$this->db->select('modulechargedetailid,modulechargedetail.moduleid,modulechargedetail.singlegrouptypeid,modulechargedetail.additionalchargecategoryid,modulechargedetail.additionalchargetypeid,modulechargedetail.calculationtypeid,modulechargedetail.amount,modulechargedetail.value,id,additionalchargetypename,calculationtypename')
				->from('modulechargedetail')
				->join('additionalchargetype','additionalchargetype.additionalchargetypeid=modulechargedetail.additionalchargetypeid')
				->join('calculationtype','calculationtype.calculationtypeid=modulechargedetail.calculationtypeid')
				->where('modulechargedetail.moduleid',$moduleid)
				->where('modulechargedetail.singlegrouptypeid',$mode)
				->where('modulechargedetail.status',$this->Basefunctions->activestatus)
				->where_in('id',$id)
				->get();
		if($adddata->num_rows() > 0){
			foreach($adddata->result() as $info) {
				$chargedataarray[]=array('modulechargedetailid'=>$info->modulechargedetailid,'additionalchargetypeid'=>$info->additionalchargetypeid,'calculationtypeid'=>$info->calculationtypeid,'amount'=>$info->amount,'value'=>$info->value,'additionalchargetypename'=>$info->additionalchargetypename,'calculationtypename'=>$info->calculationtypename);
				$chargecategoryid=$info->additionalchargecategoryid;
			}
			$chargedetails['id']=$chargecategoryid;
			$chargedetails['data']=$chargedataarray;
		} else {
			$chargedetails = [];
		}
		return $chargedetails;
	}
	/*
	 Module address data fetch
	*/
	public function moduleaddressdata($modulename,$primaryid){		
		$this->db->select('addresssourceid,address,pincode,city,state,country');
		$this->db->from($modulename.'address');
		$this->db->where($modulename.'id',$primaryid);
		$this->db->where('status',1);
		$this->db->order_by($modulename.'addressid','asc');
		$this->db->limit(2);
		$result = $this->db->get();		
		$arrname=array('billing','shipping');
		if($result->num_rows() >0) {
			$m=0;
			foreach($result->result() as $row) {
				$data[] = array($arrname[$m].'addresstype'=>$row->addresssourceid,
								$arrname[$m].'address'=>$row->address,
								$arrname[$m].'pincode'=>$row->pincode,
								$arrname[$m].'city'=>$row->city,
								$arrname[$m].'state'=>$row->state,
								$arrname[$m].'country'=>$row->country);
				$m++;
			}
			$finalarray=array_merge($data[0],$data[1]);
			return $finalarray;
		} else {
		   return array("fail"=>'FAILED');
		}
	}
	/*
	* on convert get product details
	*/
	public function retrievelinedetail($moduleid,$primaryid,$modulename,$tomoduleid) {
		$productdetail = '';
		if( $tomoduleid == 226 ||$tomoduleid == 86 || $tomoduleid == 95|| $tomoduleid == 217 || $tomoduleid == 87 || $tomoduleid == 96){
			if($moduleid == 82){
				$this->db->select('product.productname,contactdetail.membershipplans,duration.durationname,contactdetail.durationid,contactdetail.joiningdate,contactdetail.expirydate,contactdetail.amount');
				$this->db->from('contactdetail');
				$this->db->join('duration','duration.durationid=contactdetail.durationid');
				$this->db->join('product','product.productid=contactdetail.membershipplans');
				$this->db->where('contactid',$primaryid);
				$this->db->where('contactdetail.status',$this->Basefunctions->activestatus);
				$data=$this->db->get();
				if($data->num_rows() > 0){
					$j=0;
					$netamount = 0;
					foreach($data->result() as $value) {
						$netamount += $value->amount;
						$productdetail[$j]=array(
								'productidname'=>$value->productname,
								'productid'=>$value->membershipplans,
								'instock'=>1,
								'quantity'=>1,
								'durationidname'=>$value->durationname,
								'durationid'=>$value->durationid,
								'joiningdate'=>$value->joiningdate,
								'expirydate'=>$value->expirydate,
								'unitprice'=>$value->amount,
								'sellingprice'=>$value->amount,
								'grossamount'=>$value->amount,
								'discountamount'=>'',
								'pretaxtotal'=>$value->amount,
								'taxamount'=>'',
								'taxgriddata'=>'',
								'chargegriddata'=>'',
								'chargeamount'=>'',
								'netamount'=>$value->amount,
								'discountdata'=>'',
								'invoicedetailid'=>0
						);
						$j++;
					}
				}
				$finalvalue = array('productdetail'=>$productdetail,'netamount'=>$netamount);
				return $finalvalue;				
			}else if($moduleid == 80){
				$this->db->select('product.productname,product.productid,duration.durationname,duration.durationid,contractcf.renewalstartdate,contractcf.renewalexpirydate,contractcf.renewalplancost');
				$this->db->from('contractcf');
				$this->db->join('duration','duration.durationid=contractcf.renewaldurationid');
				$this->db->join('product','product.productid=contractcf.renewalmembershipplanname');
				$this->db->where('contractcf.contractid',$primaryid);
				$this->db->where('contractcf.status',$this->Basefunctions->activestatus);
				$data=$this->db->get();
				if($data->num_rows() > 0){
					$j=0;
					$finalnetamount = 0;
					foreach($data->result() as $value) {
						$finalnetamount += $value->renewalplancost;
						$productdetail[$j]=array(
								'productidname'=>$value->productname,
								'productid'=>$value->productid,
								'instock'=>1,
								'quantity'=>1,
								'durationidname'=>$value->durationname,
								'durationid'=>$value->durationid,
								'joiningdate'=>$value->renewalstartdate,
								'expirydate'=>$value->renewalexpirydate,
								'unitprice'=>$value->renewalplancost,
								'sellingprice'=>$value->renewalplancost,
								'grossamount'=>$value->renewalplancost,
								'discountamount'=>'',
								'pretaxtotal'=>$value->renewalplancost,
								'taxamount'=>'',
								'taxgriddata'=>'',
								'chargegriddata'=>'',
								'chargeamount'=>'',
								'netamount'=>$value->renewalplancost,
								'discountdata'=>'',
								'invoicedetailid'=>0
						);
						$j++;
					}
				}
				$finalvalue = array('productdetail'=>$productdetail,'netamount'=>$finalnetamount);
				return $finalvalue;
			}else{
				$primaryname = $modulename.'id';
				$this->db->select(''.$modulename.'detailid AS lineid,productname,linedetail.productid,linedetail.instock,linedetail.quantity,linedetail.unitprice,linedetail.sellingprice,linedetail.grossamount,linedetail.chargeamount,linedetail.taxamount,linedetail.discountamount,linedetail.netamount,linedetail.descriptiondetail,main.taxmodeid,main.additionalchargemodeid,linedetail.discounttypeid,linedetail.discountpercent,linedetail.pretaxtotal');
				$this->db->from(''.$modulename.'detail as linedetail');
				$this->db->join(''.$modulename.' as main','main.'.$primaryname.'=linedetail.'.$primaryname.'');
				$this->db->join('product','product.productid=linedetail.productid');
				$this->db->where('linedetail.'.$modulename.'id',$primaryid);
				$this->db->where('linedetail.status',$this->Basefunctions->activestatus);
				$data=$this->db->get();
				if($data->num_rows() > 0){
					$j=0;
					foreach($data->result() as $value) {
						$detail_parameter=array('id'=>$value->lineid,'taxmode'=>$value->taxmodeid,
								'chargemode'=>$value->additionalchargemodeid,'discounttype'=>$value->discounttypeid,'discountpercent'=>$value->discountpercent);
						$detailarray=$this->module_individualdetail($detail_parameter,$moduleid);
						if($tomoduleid == 86){
							$productdetail[$j]=array(
									'productidname'=>$value->productname,
									'productid'=>$value->productid,
									'instock'=>$value->instock,
									'quantity'=>$value->quantity,
									'durationidname'=>'',
									'durationid'=>'',
									'joiningdate'=>'',
									'expirydate'=>'',
									'unitprice'=>$value->unitprice,
									'sellingprice'=>$value->sellingprice,
									'grossamount'=>$value->grossamount,
									'discountamount'=>$value->discountamount,
									'pretaxtotal'=>$value->pretaxtotal,
									'taxamount'=>$value->taxamount,
									'taxgriddata'=>$detailarray['tax'],
									'chargegriddata'=>$detailarray['addcharge'],
									'chargeamount'=>$value->chargeamount,
									'netamount'=>$value->netamount,
									'discountdata'=>$detailarray['discount'],
									''.$modulename.'detailid'=>0
							);
						}else{
							$productdetail[$j]=array(
									'productidname'=>$value->productname,
									'productid'=>$value->productid,
									'instock'=>$value->instock,
									'quantity'=>$value->quantity,
									'unitprice'=>$value->unitprice,
									'sellingprice'=>$value->sellingprice,
									'grossamount'=>$value->grossamount,
									'discountamount'=>$value->discountamount,
									'pretaxtotal'=>$value->pretaxtotal,
									'taxamount'=>$value->taxamount,
									'chargeamount'=>$value->chargeamount,
									'netamount'=>$value->netamount,
									'taxgriddata'=>$detailarray['tax'],
									'chargegriddata'=>$detailarray['addcharge'],
									'discountdata'=>$detailarray['discount'],
									''.$modulename.'detailid'=>0
							);
						}						
						$j++;
					}
				}
				return  json_encode($productdetail);
			}
		}else if($tomoduleid == 249){
			$primaryname = $modulename.'id';
			$this->db->select(''.$modulename.'detailid AS lineid,productname,linedetail.productid,linedetail.instock,linedetail.quantity,linedetail.unitprice,linedetail.sellingprice,linedetail.grossamount,linedetail.chargeamount,linedetail.taxamount,linedetail.discountamount,linedetail.netamount,linedetail.descriptiondetail,main.taxmodeid,main.additionalchargemodeid,linedetail.discounttypeid,linedetail.discountpercent,linedetail.pretaxtotal');
			$this->db->from(''.$modulename.'detail as linedetail');
			$this->db->join(''.$modulename.' as main','main.'.$primaryname.'=linedetail.'.$primaryname.'');
			$this->db->join('product','product.productid=linedetail.productid');
			$this->db->where('linedetail.'.$modulename.'id',$primaryid);
			$this->db->where('linedetail.status',$this->Basefunctions->activestatus);
			$data=$this->db->get();
			if($data->num_rows() > 0){
				$j=0;
				foreach($data->result() as $value) {
					$detail_parameter=array('id'=>$value->lineid,'taxmode'=>$value->taxmodeid,
							'chargemode'=>$value->additionalchargemodeid,'discounttype'=>$value->discounttypeid,'discountpercent'=>$value->discountpercent);
					$detailarray=$this->module_individualdetail($detail_parameter,$moduleid);
					$productdetail[$j]=array(
							'productidname'=>$value->productname,
							'productid'=>$value->productid,
							'uomidname'=>'',
							'uomid'=>'',
							'touomidname'=>'',
							'touomid'=>'',
							'instock'=>$value->instock,
							'conversionrate'=>'',
							'conversionquantity'=>'',
							'quantity'=>$value->quantity,
							'unitprice'=>$value->unitprice,
							'sellingprice'=>$value->sellingprice,
							'grossamount'=>$value->grossamount,
							'discountamount'=>$value->discountamount,
							'pretaxtotal'=>$value->pretaxtotal,
							'taxamount'=>$value->taxamount,
							'chargeamount'=>$value->chargeamount,
							'netamount'=>$value->netamount,
							'taxgriddata'=>$detailarray['tax'],
							'chargegriddata'=>$detailarray['addcharge'],
							'discountdata'=>$detailarray['discount'],
							''.$modulename.'detailid'=>0,
							'requireddate'=> ''
					);
					$j++;
				}
			}
			return  json_encode($productdetail);
		}else if( $tomoduleid == 225 ||$tomoduleid == 88 || $tomoduleid == 99){
				if($modulename == 'invoice' || $modulename == 'salesorder' || $modulename == 'quote'){
					$primaryname = $modulename.'id';
					$this->db->select(''.$modulename.'detailid AS lineid,productname,linedetail.productid,linedetail.instock,linedetail.quantity,linedetail.unitprice,linedetail.sellingprice,linedetail.grossamount,linedetail.chargeamount,linedetail.taxamount,linedetail.discountamount,linedetail.netamount,linedetail.descriptiondetail,main.taxmodeid,main.additionalchargemodeid,linedetail.discounttypeid,linedetail.discountpercent,linedetail.pretaxtotal');
					$this->db->from(''.$modulename.'detail as linedetail');
					$this->db->join(''.$modulename.' as main','main.'.$primaryname.'=linedetail.'.$primaryname.'');
					$this->db->join('product','product.productid=linedetail.productid');
					$this->db->where('linedetail.'.$modulename.'id',$primaryid);
					$this->db->where('linedetail.status',$this->Basefunctions->activestatus);
					$data=$this->db->get();
					if($data->num_rows() > 0){
						$j=0;
						foreach($data->result() as $value) {
							$detail_parameter=array('id'=>$value->lineid,'taxmode'=>$value->taxmodeid,
									'chargemode'=>$value->additionalchargemodeid,'discounttype'=>$value->discounttypeid,'discountpercent'=>$value->discountpercent);
							$detailarray=$this->module_individualdetail($detail_parameter,$moduleid);
							$productdetail[$j]=array(
									'productidname'=>$value->productname,
									'productid'=>$value->productid,
									'instock'=>$value->instock,
									'quantity'=>$value->quantity,
									'unitprice'=>$value->unitprice,
									'sellingprice'=>$value->sellingprice,
									'grossamount'=>$value->grossamount,
									'pretaxtotal'=>$value->pretaxtotal,
									'taxamount'=>$value->taxamount,
									'chargeamount'=>$value->chargeamount,
									'discountamount'=>$value->discountamount,
									'discountdata'=>$detailarray['discount'],
									'netamount'=>$value->netamount,
									''.$modulename.'detailid'=>0,
									'taxgriddata'=>$detailarray['tax'],
									'chargegriddata'=>$detailarray['addcharge'],
							);
							$j++;
						}
					}
					return  json_encode($productdetail);
				}else if($modulename == 'materialrequisition'){						
					$primaryname = $modulename.'id';
					$this->db->select(''.$modulename.'productdetailsid AS lineid,productname,linedetail.productid,linedetail.quantity,linedetail.unitprice,linedetail.sellingprice,linedetail.grossamount,linedetail.discountamount,linedetail.netamount,linedetail.discounttypeid,linedetail.discountpercent,linedetail.conversionrate,linedetail.uomid,linedetail.touomid,linedetail.instock,linedetail.conversionquantity');
					$this->db->from(''.$modulename.'productdetails as linedetail');
					$this->db->join(''.$modulename.' as main','main.'.$primaryname.'=linedetail.'.$primaryname.'');
					$this->db->join('product','product.productid=linedetail.productid');
					$this->db->where('linedetail.'.$modulename.'id',$primaryid);
					$this->db->where('linedetail.status',$this->Basefunctions->activestatus);
					$data=$this->db->get();
					if($data->num_rows() > 0){
						$j=0;
						foreach($data->result() as $value) {
							$uom_json = array('uomid'=>$value->uomid,'touomid'=>$value->touomid,'conversionrate'=>$value->conversionrate,'conversionquantity'=>$value->conversionquantity);
							$discount_json = array('typeid'=>$value->discounttypeid,'value'=>$value->discountpercent);
							$productdetail[$j]=array(
									'productidname'=>$value->productname,
									'productid'=>$value->productid,
									'instock'=>$value->instock,
									'quantity'=>$value->quantity,
									'unitprice'=>$value->unitprice,
									'sellingprice'=>$value->sellingprice,
									'grossamount'=>$value->grossamount,
									'discountamount'=>$value->discountamount,
									'pretaxtotal'=>'',
									'taxamount'=>'',
									'chargeamount'=>'',
									'netamount'=>$value->netamount,
									'discountdata'=>json_encode($discount_json),
									'purchaseorderdetailid'=>0,
									'taxgriddata'=>[],
									'chargegriddata'=>[],
									'uomdata'=>json_encode($uom_json)
										
							);
							$j++;
						}
					}
					return  json_encode($productdetail);
						
				}
			}else{				
				$primaryname = $modulename.'id';
				$this->db->select(''.$modulename.'productdetailsid AS lineid,productname,linedetail.productid,linedetail.quantity,linedetail.unitprice,linedetail.sellingprice,linedetail.grossamount,linedetail.discountamount,linedetail.netamount,linedetail.discounttypeid,linedetail.discountpercent,linedetail.conversionrate,linedetail.uomid,uom.uomname,linedetail.instock,linedetail.conversionquantity');
				$this->db->from(''.$modulename.'productdetails as linedetail');
				$this->db->join(''.$modulename.' as main','main.'.$primaryname.'=linedetail.'.$primaryname.'');
				$this->db->join('product','product.productid=linedetail.productid');
				$this->db->join('uom','uom.uomid=linedetail.uomid');
				$this->db->where('linedetail.'.$modulename.'id',$primaryid);
				$this->db->where('linedetail.status',$this->Basefunctions->activestatus);
				$data=$this->db->get();
				if($data->num_rows() > 0){
					$j=0;
					foreach($data->result() as $value) {
						$detail_parameter=array('id'=>$value->lineid,'taxmode'=>1,
								'chargemode'=>1,'discounttype'=>$value->discounttypeid,'discountpercent'=>$value->discountpercent);
						$detailarray=$this->module_individualdetail($detail_parameter,$moduleid);
						$productdetail[$j]=array(
								'productidname'=>$value->productname,
								'productid'=>$value->productid,
								'uomidname'=>$value->uomname,
								'uomid'=>$value->uomid,
								'touomidname'=>'',
								'touomid'=>'',
								'conversionrate'=>$value->conversionrate,
								'conversionquantity'=>$value->conversionquantity,
								'instock'=>$value->instock,
								'quantity'=>$value->quantity,
								'unitprice'=>$value->unitprice,
								'sellingprice'=>$value->sellingprice,
								'grossamount'=>$value->grossamount,
								'discountamount'=>$value->discountamount,
								'pretaxtotal'=>'',
								'taxamount'=>'',
								'chargeamount'=>'',
								'netamount'=>$value->netamount,
								'taxgriddata'=>$detailarray['tax'],
								'chargegriddata'=>$detailarray['addcharge'],
								'discountdata'=>$detailarray['discount'],
								''.$modulename.'detailid'=>0
						);
						$j++;
					}
				}
				return  json_encode($productdetail);
		}
	}
	//to get payment details for conversion
	public function retrievepaymentdetail($moduleid,$primaryid,$modulename,$tomoduleid){
		$paymentdetail = '';		
		if($modulename == 'invoice'){
			$data=$this->db->select('paymentid,paymentnumber,paymentdate,paymenttypename,paymentmethodname,paymentamount,payment.paymentreferencenumber,payment.bankname,paymentstatusname,payment.referencedate,paymentdescription,payment.paymentmethodid,payment.paymenttypeid,payment.paymentstatusid,payment.paymentto,payment.payeename')
				->from('payment')		
				->join('paymentmethod','paymentmethod.paymentmethodid=payment.paymentmethodid')
				->join('paymenttype','paymenttype.paymenttypeid=payment.paymenttypeid')
				->join('paymentstatus','paymentstatus.paymentstatusid=payment.paymentstatusid')
				->where('payment.moduleid',$moduleid)
				->where('payment.status',$this->Basefunctions->activestatus)
				->where('payment.transactionid',$primaryid)
				->get();
			if($data->num_rows() > 0){
				$j=0;
				foreach($data->result() as $value) {
					if($value->paymentdescription){
						$description = $value->paymentdescription;
					}else{
						$description = '';
					}
					$paymentdetail[$j]=array(
							'paymentid'=>$value->paymentid,
							'paymentdate'=>$value->paymentdate,
							'paymentto'=>$value->paymentto,
							'paymenttypeidname'=>$value->paymenttypename,
							'paymenttypeid'=>$value->paymenttypeid,
							'paymentmethodidname'=>$value->paymentmethodname,
							'paymentmethodid'=>$value->paymentmethodid,
							'paymentamount'=>$value->paymentamount,
							'paymentreferencenumber'=>$value->paymentreferencenumber,
							'paymentstatusidname'=>$value->paymentstatusname,
							'paymentstatusid'=>$value->paymentstatusid,
							'bankname'=>$value->bankname,
							'referencedate'=>$value->referencedate,
							'payeename'=>$value->payeename,
							'paymentdescription'=>$description,
					);
					$j++;
				}
			}
		}
		return json_encode($paymentdetail);
	}
	//get the detail of tax,additinalcharge,discount detail for individual records
	public function module_individualdetail($param,$moduleid) {
		$discount_json='';
		$tax_json='';
		$charge_json='';
		if($param['discounttype'] > 1) {	
			$discount_json = array('typeid'=>$param['discounttype'],'value'=>$param['discountpercent']);			
		}		
		if($param['taxmode'] > 1) {	
			$tax_json=$this->gettaxdetail($moduleid,$param['taxmode'],$param['id']);
		}
		if($param['chargemode'] > 1) {		
			$charge_json=$this->getchargedetail($moduleid,$param['chargemode'],$param['id']);			
		}			
	   $retarray = array('tax'=>json_encode($tax_json),'discount'=>json_encode($discount_json),'addcharge'=>json_encode($charge_json));
	   return $retarray;
	}
	//retrieve getcompanyname
	public function getcompanyname() {
		$userid=$this->Basefunctions->userid;		
		$data=$this->db->select('company.companyname')
						->from('company')
						->join('branch','branch.companyid=company.companyid')
						->join('employee','employee.branchid=branch.branchid')
						->where('employee.employeeid',$userid)
						->limit(1)
						->get();
		foreach($data->result() as $info) {
			$companyname = $info->companyname;
		}
		echo $companyname;
	}
	//sso login of freshdesk
	public function freshdesk_login_url() {
		$name='';
		$email='';
		$dataset=$this->db->select('employeeid,employeename,lastname,emailid,salutation.salutationname')->from('employee')->join('salutation','salutation.salutationid=employee.salutationid')->where('employee.employeeid',$this->logemployeeid)->get();
		foreach($dataset->result() as $row) {
			$name=$row->salutationname.$row->employeename.$row->lastname;
			$email=$row->emailid;
		}
		$secure = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "");
		$secret = '7303fbf09a427f7d64e192742aea784f';
		$base = 'http'.$secure.'://support.salesneuron.com/';
		return $base . "login/sso/?name=" . urlencode($name) . "&email=" . urlencode($email) . "&hash=" . hash('md5', $name . $email . $secret);
	}
	//click to call function
	public function clicktocallfunctionmodel() {
		$mobilenumber = $_POST['mobilenumber'];
		$triggerurl = "http://api.dialstreet.com/v1/?api_key=Ad69b5606d01b3dca98cd90fe11de9714&method=dial.click2call&output=xml&caller=04433870018&receiver=".$mobilenumber;
		$ch=curl_init();
		curl_setopt($ch, CURLOPT_URL, $triggerurl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$output=curl_exec($ch);
		curl_close($ch);
		$dataset = $this->c2cparse_response($output);
		$count = count($dataset);
		for($i=0;$i<$count;$i++) {
			echo json_encode($dataset[$i]['message']);
		}
	}
	//get response
	public function c2cparse_response($response) {
		$xml = new SimpleXMLElement($response);
		$result = array();
		$h=0;
		foreach($xml as $key => $value) {
			if( $key == 'status' || $key == 'message' ) {
				$result[$h][(string)$key] = (string)$value;
				if( count($result[$h]) == '2' ) {
					$h++;
				}
			}
		}
		return $result;
	}
	//mobile number dd load
	public function mobilenumberddloadmodel() {
		$mobilenumber = $_POST['mobilenumber'];	
		$expmobnum = explode(',',$mobilenumber);
		$j=1;
		for($i=0;$i<count($expmobnum);$i++) {
			$data[$i] = array('datasid'=>$j,'dataname'=>$expmobnum[$i]);
			$j++;
		}
		echo json_encode($data);
	}
	//mobile number information fetch
	public function mobilenumberinformationfetchmodel($moduleid,$recordid) {
		$data = $this->fetchlinkmoduleidfrommainmodule($moduleid);
		if(!empty($data)) {
			$count = count($data);
			$j=0;
			if($count > 0) {
				for ( $i=0; $i < $count ; $i++ ) {
					$multiarray[$j]=array(
						'0'=>$data[$i]['jointable'],
						'1'=>$data[$i]['indexname'],
						'2'=>$data[$i]['tableid'],
						'3'=>$data[$i]['parentable'],
					);
					$j++;
				} 
			} else {
				$multiarray = array();
			}
			$n=0;
			$mobilenumber = array();
			for($k=0;$k<count($multiarray);$k++) {
				$this->db->select($multiarray[$k][1]);
				$this->db->from($multiarray[$k][3]);
				if($multiarray[$k][0] != $multiarray[$k][3]) {
					$this->db->join($multiarray[$k][0],$multiarray[$k][0].'.'.$multiarray[$k][2].'='.$multiarray[$k][3].'.'.$multiarray[$k][2]);
				}
				$this->db->where($multiarray[$k][3].'.'.$multiarray[$k][3].'id',$recordid);
				$this->db->where($multiarray[$k][3].'.status',1);
				$finaldata = $this->db->get();
				if($finaldata->num_rows() > 0) {
					foreach($finaldata->result() as $nrow) {
						$res = $multiarray[$k][1];
						$mobilenumber[$n] = $nrow->$res;
					}
				} 
				$n++;			
			}
			return $mobilenumber;
		} else {
			$mobilenumber = 'No mobile number';
			return $mobilenumber;
		}
	}
	//mobile number view creation column get
	public function fetchlinkmoduleidfrommainmodule($moduleid) {
		$data = array();
		$i= 0;
		$this->db->select('viewcreationcolmodelindexname,viewcreationcolmodeljointable,viewcreationjoincolmodelname,viewcreationparenttable');
		$this->db->from('viewcreationcolumns');
		$this->db->where('viewcreationcolumns.viewcreationmoduleid',$moduleid);
		$this->db->where('viewcreationcolumns.uitypeid',11);
		$this->db->where('viewcreationcolumns.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				$data[$i] = array('indexname'=>$row->viewcreationcolmodelindexname,'jointable'=>$row->viewcreationcolmodeljointable,'tableid'=>$row->viewcreationjoincolmodelname,'parentable'=>$row->viewcreationparenttable);
				$i++;
			}
		} else {
			$data = '';
		}
		return $data;
	}
	//click 2 call and sms icon overlay - module dd load
	public function moduleddloadmodel() {
		$j=0;
		$moduleid = $_POST['moduleid'];
		$recordid = $_POST['recordid'];
		$check = $this->checktypeavaliable($moduleid);
		if($check == "FALSE") {
			$linkmodule = $this->fetchlinkmoduleidfrommainmodule_view($moduleid);
			if(count($linkmodule) != 0) {
				for($i=0;$i < count($linkmodule);$i++) {
					$mobmodid = $this->fetchonlymobilenumbermodules($linkmodule[$i]);
					if(!empty($mobmodid) || $mobmodid != '') {
						$module[$j] = $this->generalinformaion('module','modulename','moduleid',$mobmodid);
						$mid[$j] = $mobmodid;
						$j++;
					}
				} 
				$j=1;
				for($k=0;$k<count($module);$k++) {
					$data[$k] = array('datasid'=>$j,'dataname'=>$module[$k],'moduleid'=>$mid[$k]);
					$j++;
				}
				echo json_encode($data);
			} else{ 
				$module = array();
				echo json_encode($module);
			} 
		} else {
			$typemid = $this->commonmoduleid($moduleid,$recordid);
			$typelinkmid = $this->fetchlinkmoduleidfrommainmodule_view($moduleid);
			if(!empty($typemid)){
				$nmid = array_merge($typemid,$typelinkmid);
			} else{
				$nmid = $typelinkmid;
			}
			if(count($nmid) != 0) {
				for($i=0;$i < count($nmid);$i++) {
					$mobmodid = $this->fetchonlymobilenumbermodules($nmid[$i]);
					if(!empty($mobmodid) || $mobmodid != '') {
						$module[$j] = $this->generalinformaion('module','modulename','moduleid',$mobmodid);
						$mid[$j] = $mobmodid;
						$j++;
					}
				} 
				$j=1;
				for($k=0;$k<count($module);$k++) {
					$data[$k] = array('datasid'=>$j,'dataname'=>$module[$k],'moduleid'=>$mid[$k]);
					$j++;
				}
				echo json_encode($data);
			} else{ 
				$module = array();
				echo json_encode($module);
			} 
		}
	}
	//check the related module dd and common available or not.
	public function checktypeavaliable($moduleid) {
		$mastertable = $this->generalinformaion('module','modulemastertable','moduleid',$moduleid);
		if (($this->db->field_exists('moduleid', $mastertable)) && ($this->db->field_exists('commonid', $mastertable))) {
			return 'TRUE';
		} else {
			return 'FALSE';
		}
	}
	//related to module fetch for activity and task
	public function commonmoduleid($moduleid,$recordid) {
		$i=0;
		$mastertable = $this->generalinformaion('module','modulemastertable','moduleid',$moduleid);
		$this->db->select('moduleid');
		$this->db->from($mastertable);
		$this->db->where($mastertable.'.'.$mastertable.'id',$recordid);
		$this->db->where($mastertable.'.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = $row->moduleid;
				$i++;
			}		
		} else{
			$data = array();
		}
		return $data;
	}
	//fetch link module
	public function fetchlinkmoduleidfrommainmodule_view($moduleid) {
		$data = array();
		$i=0;
		$this->db->select('linkmoduleid');
		$this->db->from('viewcreationcolumns');
		$this->db->like('viewcreationcolumns.linkable','Yes');
		$this->db->where('viewcreationcolumns.viewcreationmoduleid',$moduleid);
		$this->db->where('viewcreationcolumns.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$mid = $row->linkmoduleid;
				if($mid != $moduleid) {
					$data[$i] = $mid;
					$i++;
				}
			}
		} else {
			$data = '';
		}
		return $data;
	}
	//fetch only mobile number with module.
	public function fetchonlymobilenumbermodules($linkmodule) {
		$this->db->select('viewcreationmoduleid');
		$this->db->from('viewcreationcolumns');
		$this->db->where('viewcreationcolumns.viewcreationmoduleid',$linkmodule);
		$this->db->where('viewcreationcolumns.uitypeid',11);
		$this->db->where('viewcreationcolumns.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				$data = $row->viewcreationmoduleid;
			}
		} else {
			$data = '';
		}
		return $data;
	}
	// mobile number fetch based on module
	public function mobilenumberfetchbasedonmodulemodel() {
		$moduleid = $_GET['moduleid']; // mail module
		$linkmoduleid = $_GET['linkmoduleid']; //link module.
		$recordid = $_GET['recordid'];
		$mastertable = $this->generalinformaion('module','modulemastertable','moduleid',$moduleid);
		$linkmastertable = $this->generalinformaion('module','modulemastertable','moduleid',$linkmoduleid);
		if(($this->db->field_exists($linkmastertable.'id', $mastertable))) {
			$linkrecordid = $this->generalinformaion($mastertable,$linkmastertable.'id',$mastertable.'id',$recordid);
		} else {
			$linkrecordid = $this->generalinformaion($mastertable,'commonid',$mastertable.'id',$recordid);	
		}
		$mobilenumber = $this->mobilenumberinformationfetchmodel($linkmoduleid,$linkrecordid);
		if($linkrecordid != 1){
			echo json_encode($mobilenumber);
		} else {
			$mobilenumber = 'No mobile number'; 
			echo json_encode($mobilenumber);
		}
	}
	//sales neuron status value get
	public function statusvalueget($smsstatus) {
		$this->db->select('smssalesneuronname');
		$this->db->from('smsstatus');
		$this->db->where('smsstatus.smsstatusid',$smsstatus);
		$result = $this->db->get();
		if($result->num_rows() > 0 ) {
			foreach($result->result() as $row) {
				$data = $row->smssalesneuronname;
			}
			return $data;
		} else {
			return $smsstatus;
		}
	}
	//php date format fetch
	public function phpmysqlappdateformatfetch() {
		$data = array('phpformat'=>'Y-m-d H:i:s','mysqlformat'=>"%d-%m-%Y");
		$branchid = $this->branchid;
		$companyid = $this->Basefunctions->generalinformaion('branch','companyid','branchid',$branchid);
		$this->db->select('phpdateformat,mysqldateformat');
		$this->db->from('dateformat');
		$this->db->join('company','company.dateformatid=dateformat.dateformatid');
		$this->db->where('company.companyid',$companyid);
		$this->db->where('company.status',1);
		$data = $this->db->get();
		foreach($data->result() as $datas) { 
			$data = array('phpformat'=>$datas->phpdateformat,'mysqlformat'=>$datas->mysqldateformat);
		}
		return $data;
	}
	//date conversion - only date
	public function ymddateconversion($date) {
		$convertdate = $date!=''? date('Y-m-d', strtotime($date)) : '';
		return $convertdate;
	}
	//date time conversion - date with time
	public function ymddatetimeconversion($date) {
		$convertdate = $date!=''? date('Y-m-d H:i:s', strtotime($date)) : '';
		return $convertdate;
	}
	//user date & time format conversion
	public function userdatetimeformatconversion($date) {
		$format = $this->phpmysqlappdateformatfetch();
		$convertdate = ($date!='' && $date!='0000-00-00 00:00:00' && $date!='0000-00-00') ? date($format['phpformat'].' H:m', strtotime($date)) : '';
		return $convertdate;
	}
	//user date format conversion
	public function userdateformatconvert($date) {
		$format = $this->phpmysqlappdateformatfetch();
		$convertdate = ($date!='' && $date!='0000-00-00') ? date($format['phpformat'],strtotime($date)) : '';
		return $convertdate;
	}
	//unique field validation
	public function uniquefieldvalcheckmodel() {
		$vals='False';
		$industryid = $this->industryid;
		$value = trim($_POST['v']);
		$tblname = $_POST['t'];
		$fname = $_POST['c'];
		$tblid = $tblname.'id';
		$datas = $this->db->select($tblid)->from($tblname)->where("FIND_IN_SET('".$industryid."',industryid) >", 0)->where($fname,$value)->where('status',1)->get();
		$count = $datas->num_rows();
		if($count>=1) {
			foreach($datas->result() as $data) {
				$vals = $data->$tblid;
			}
		} else {
			$vals='False';
		}
		echo $vals;
	}
	/****************************
	  Work flow Management start
	****************************/
	//work flow manage in data creation mode
	public function workflowmanageindatacreatemode($modid,$primaryid,$primarytable) {
		$workflowinfo = $this->workflowinformationfetch($modid);
		foreach($workflowinfo as $datas) {
			$wrkflwtrgtypeid = $datas->workflowtriggertypeid;
			$trgtypeid = $datas->triggerfrequencytype;
			$wrkflowid = $datas->workflowid;
			$workflow_modid = $datas->moduleid;
			if($wrkflwtrgtypeid=='1') { //record action
				if($trgtypeid==1 || $trgtypeid==3) {
					//rule criteria
					$rulecritconddata = $this->workflowrulecriteriacond($wrkflowid);
					$i=0;
					$conds=array();
					foreach($rulecritconddata->result() as $conddatas) {
						$condfldname = $conddatas->fieldname;
						$conduitypeid = $conddatas->conditionuitypeid;
						$condtypename = $conddatas->conditionname;
						$condandortype = $conddatas->conditionandorvalue;
						$condvalue = $conddatas->conditionvalue;
						//field value
						if(isset($_POST[$condfldname])) {
							$filedval = $_POST[$condfldname];
							if($conduitypeid=='17' || $conduitypeid=='18' || $conduitypeid=='19' || $conduitypeid=='20' || $conduitypeid=='25' || $conduitypeid=='26' || $conduitypeid=='27' || $conduitypeid=='28' || $conduitypeid=='29') {
								$condvals = explode('-',$condvalue);
								$conds[$i] = array(0=>$filedval,1=>$condvals[0],2=>$condtypename,3=>$condandortype);
								$i++;
							} else {
								$conds[$i] = array(0=>$filedval,1=>$condvalue,2=>$condtypename,3=>$condandortype);
								$i++;
							}
						}
					}
					$wrkflwcond = $this->workflowrulecriteriacondgen($conds);
					$wrkflwconfchk = eval("return ('.$wrkflwcond.');") ? 'True':'false';
					//$wrkflwconfchk = $wrkflwcond ? 'True':'false';
					if($wrkflwcond=='' || $wrkflwconfchk=='True') {
						$sendalertdatas = $this->workflowsendalertinfo($wrkflowid);
						foreach($sendalertdatas as $sendalerdata) {
							//print_r($sendalertdatas);
							$alerttype = $sendalerdata->workflowalerttype;
							if($alerttype=='email') {
								//work flow data alert[mail]
								$templatedata = $this->workflowruleemailalertgen($wrkflowid);
								$userid = $this->userid;
								foreach($templatedata as $datarow) {
									$addemailarrayformat = '';
									$emailid = '';
									$addemailid = $datarow->workflowalertadditonalrecipient;
									if($addemailid != '') {
										$addemailiddata = $this->retrieveallrecipientemailids($datarow->workflowalertadditonalrecipient);
									}
									$manaddemailid = $datarow->workflowmanualalertadditonalrecipient;
									if($manaddemailid != '') {
										if(!empty($addemailiddata)) {
											$additionalusermail = implode(',',$addemailiddata);
											$addemailarrayformat = $additionalusermail.','.$manaddemailid;
										} else {
											$addemailarrayformat = $manaddemailid;
										}
									} else if(!empty($addemailiddata)) {
										$addemailarrayformat = implode(',',$addemailiddata);
									}
									if($datarow->moduletabid == $modid) {
										$parenttable = $primarytable;
										$primaryid = $primaryid;
									} else if($datarow->moduletabid !='4') {
										$primaryid = $this->generalinformaion($parenttable,$datarow->tablename.'id',$parenttable.'id',$primaryid);
										$parenttable = $datarow->tablename;
									}
									if($datarow->moduletabid=='4') {
										$emailid = $this->generalinformaion('employee','emailid','employeeid',$userid);
									} else {
										$emailid = $this->generalinformaion($parenttable,$datarow->columnname,$parenttable.'id',$primaryid);
									}
									if($emailid != '') {
										$this->workflowemailsent($datarow->moduleid,$datarow->emailtemplatesid,$datarow->emailtemplatesemailtemplate_editorfilename,$primarytable,$primaryid,$emailid,$addemailarrayformat);
									}
								}
							} else if($alerttype=='sms') {
								//work flow data alert[sms]
								$templatedata = $this->workflowrulesmsalertgen($wrkflowid);
								$userid = $this->userid;
								foreach($templatedata as $datarow) {
									$empmobnum = '';
									$addmobnosarrayformat = '';
									$addmobilenos = $datarow->workflowalertadditonalrecipient;
									if($addmobilenos != '') {
										$addmobilenosdata = $this->retrieveallrecipientmobilenos($datarow->workflowalertadditonalrecipient);
									}
									$manaddmobilenos = $datarow->workflowmanualalertadditonalrecipient;
									if($manaddmobilenos != '') {
										if(!empty($addmobilenosdata)) {
											$additionalusermobnos = implode(',',$addmobilenosdata);
											$addmobnosarrayformat = $additionalusermobnos.','.$manaddmobilenos;
										} else {
											$addmobnosarrayformat = $manaddmobilenos;
										}
									} else if(!empty($addmobilenosdata)) {
										$addmobnosarrayformat = implode(',',$addmobilenosdata);
									}
									$parenttable = $primarytable;
									$id = $primaryid;
									$tempfile = $datarow->leadtemplatecontent_editorfilename;
									$htmlcontent = $this->smsgenerateprinthtmlfile($datarow->moduleid,$datarow->workflowtemplateid,$tempfile,$primarytable,$primaryid);
									$apikey = $datarow->apikey;
									$senderid = $datarow->senderid;
									$scheduledate = '';
									if($datarow->moduletabid == $modid) {
										$parenttable = $primarytable;
										$primaryid = $primaryid;
									} else if($datarow->moduletabid !='4'){
										$primaryid = $this->generalinformaion($parenttable,$datarow->tablename.'id',$parenttable.'id',$primaryid);
										$parenttable = $datarow->tablename;
									}
									if($datarow->moduletabid=='4') {
										$empmobnum = $this->generalinformaion('employee','mobilenumber','employeeid',$userid);
									} else {
										$empmobnum = $this->generalinformaion($parenttable,$datarow->columnname,$parenttable.'id',$primaryid);
									}
									if($empmobnum!='') {
										$this->sendsmsinallmodules($apikey,$senderid,$empmobnum,$htmlcontent,$scheduledate,$primaryid,$datarow->moduleid);
									}
									if($addmobnosarrayformat != '' || !empty($addmobnosarrayformat)) {
										$addmobnosarrayformat = explode(',',$addmobnosarrayformat);
										foreach($addmobnosarrayformat as $mobnos) {
											$this->sendsmsinallmodules($apikey,$senderid,$mobnos,$htmlcontent,$scheduledate,$primaryid,$datarow->moduleid);
										}
									}
								}
							} else if($alerttype=='call') {
								//work flow data alert[call]
								$templatedata = $this->workflowrulecallsalertgen($wrkflowid);
								$userid = $this->userid;
								foreach($templatedata as $datarow) {
									$addcallnum = $datarow->workflowalertadditonalrecipient;
									$apikey = $datarow->apikey;
									$caller = $datarow->mobilenumber;
									$splay = $datarow->soundfileid;
									$play = $splay.'.sound';
									if($datarow->moduletabid == $modid) {
										$parenttable = $primarytable;
										$primaryid = $primaryid;
									} else if($datarow->moduletabid !='4'){
										$primaryid = $this->generalinformaion($parenttable,$datarow->tablename.'id',$parenttable.'id',$primaryid);
										$parenttable = $datarow->tablename;
									}
									if($datarow->moduletabid=='4') {
										$mobilenumber = $this->generalinformaion('employee','mobilenumber','employeeid',$userid);
										if($mobilenumber!='') {
											$this->workflowcallnotification($apikey,$play,$mobilenumber,$caller);
										}
									} else {
										$mobilenumber = $this->generalinformaion($parenttable,$datarow->columnname,$parenttable.'id',$primaryid);
										if($mobilenumber!='') {
											$this->workflowcallnotification($apikey,$play,$mobilenumber,$caller);
										}
									} 
								}
							} else if($alerttype=='desktop') {
								//work flow data alert[desktop]
								$templatedata = $this->workflowruledesktopalertgen($wrkflowid);
								$userid = $this->userid;
								foreach($templatedata as $datarow) {
									$addemployees = $datarow->workflowalertadditonalrecipient;
									//generate addition user ids from roles,groups
									$adduserids = $this->additionaluseridscheck($addemployees);
									$parenttable = $primarytable;
									$id = $primaryid;
									$tempfile = $datarow->leadtemplatecontent_editorfilename;
									$this->load->model('Printtemplates/Printtemplatesmodel');
									$htmlcontent = $this->filecontentfetch($tempfile);
									if($datarow->moduletabid == $modid) {
										$parenttable = $primarytable;
										$primaryid = $primaryid;
									} else if($datarow->moduletabid !='4'){
										$primaryid = $this->generalinformaion($parenttable,$datarow->tablename.'id',$parenttable.'id',$primaryid);
										$parenttable = $datarow->tablename;
									}
									if($datarow->moduletabid=='4') {
										if($userid!='') {
											$this->workflowdesktopnotification('Added',$modid,$primaryid,$userid,$htmlcontent,$adduserids);
										}
									} else {
										$employeeid = $this->generalinformaion($parenttable,'employeeid',$parenttable.'id',$id);
										if($employeeid!='') {
											$this->workflowdesktopnotification('Added',$modid,$primaryid,$employeeid,$htmlcontent,$adduserids);
										}
									}
								}
							}
						}
						{//work flow task alert
							$alertdatas = $this->workflowruleactiondataalert($wrkflowid,$modid);
							foreach($alertdatas as $data) {
								$taskname = $data->workflowruleactiontaskname;
								$modfieldid = $data->taskalertduedate;
								$alerttimetype = $data->taskalerttype;
								$taskalertdays = $data->taskalertdays;
								$employeeid = $data->taskassignedto;
								$notifyassignee = $data->notifyassignee;
								$extype = $alerttimetype=='plus'?'+':'-';
								$taskduedate = date('Y-m-d',strtotime(date('Y-m-d').' '.$extype.$taskalertdays.' days'));
								$this->workflownewtaskcreate($taskname,$taskduedate,$data->taskstatusid,$data->taskpriorityid,$employeeid,$data->description,$modid);
								if($notifyassignee=='Yes') {
									$sendat='';
									if($modfieldid==1) {
										$sendat = date('Y-m-d H:i:s',strtotime(date('Y-m-d').' '.$extype.$taskalertdays.' days'));
										if($sendat!='') {
											$this->workflowtasknotificationmailsent($taskname,$employeeid,$sendat);
										}
									} else {
										$fielddatas = $this->workflowtaskfieldinformation($modfieldid);
										foreach($fielddatas as $fielddata) {
											$table = $fielddata->tablename;
											$colname = $fielddata->columnname;
											$fieldname = $fielddata->fieldname;
											$primarytable = $fielddata->modulemastertable;
											$tableid = $primarytable.'id';
											$value = $this->generalinformaion($table,$colname,$tableid,$primaryid);
											if($value!='') {
												$taskvalue=$this->ymddateconversion($value);
												$sendat = date('Y-m-d',strtotime($taskvalue.' '.$extype.$taskalertdays.' days')).' '.date('H:i:s');
											}
											if($sendat!='') {
												$this->workflowtasknotificationmailsent($taskname,$employeeid,$sendat);
											}
										}
									}
								}
							}
						}
						{//field update
							$actionfldupdatedatas = $this->workflowactionfieldupdateinformation($wrkflowid,$modid);
							foreach($actionfldupdatedatas as $data) {
								$modfieldid = $data->modulefieldid;
								$value = $data->value;
								$uitypeid = $data->uitypeid;
								$fielddatas = $this->workflowtaskfieldinformation($modfieldid);
								foreach($fielddatas as $fielddata) {
									$table = $fielddata->tablename;
									$colname = $fielddata->columnname;
									$fieldname = $fielddata->fieldname;
									$primarytable = $fielddata->modulemastertable;
									$tableid = $primarytable.'id';
									if($uitypeid==17 || $uitypeid==18 || $uitypeid==19 || $uitypeid==20 || $uitypeid==21 || $uitypeid==23 || $uitypeid==25 || $uitypeid==26 || $uitypeid==27 || $uitypeid==28 || $uitypeid==29) {
										$ddvalue = explode('-',$value);
										$data = array($colname=>$ddvalue[0]);
										$this->db->where($tableid,$primaryid);
										$this->db->update($table,$data);
									} else {
										$data = array($colname=>$value);
										$this->db->where($tableid,$primaryid);
										$this->db->update($table,$data);
									}
								}
							}
						}
					}
				}
			} else if($wrkflwtrgtypeid=='2') {//date action
				$this->load->model('Activity/Activitiesmodel');
				//date based actions
				$fieldid = $datas->triggerfieldid;
				$execfreq = $datas->triggerfrequencytype;
				$exectype = $datas->triggerexecutiontype;
				$freqtype = $datas->triggerexecutionfreqency;
				$exectime = $datas->triggerexecutiontime;
				$repeatevery = $datas->triggerrepeatevery;
				$executiononday = $datas->triggerexecutiononday;
				//check field info
				$fldupdateval = $this->datafieldupdateinformationfetch(array($fieldid));
				foreach($fldupdateval as $flddatas) {
					$colname = $flddatas->columnname;
					$fldname = $flddatas->fieldname;
					$tblname = $flddatas->tablename;
					$ruleexec = 'TRUE';
					//if(isset($_POST[$fldname])) {
					if(isset($fldname)) {
						//$fldval = $_POST[$fldname];
						$fldval = $fldname;
						$dateformat = $this->phpmysqlappdateformatfetch();
						$datef = explode(' ',$dateformat['phpformat']);
						$curdate = date($datef[0]);
						$curtime = date('H:i');
						$curdatetime = $curdate.' '.$curtime;
						$trigdatetime = $fldval.' '.$exectime;
						// execute - Check execution Time
						$current_time = date('H:i', strtotime("+15 minutes", strtotime(date("H:i"))));
						if(date('h:i',strtotime($current_time)) < date('h:i',strtotime($exectime))) {
							$exectime = $exectime;
						} else {
							$exectime = date('H:i',strtotime($current_time));
						}
						if($exectype == 'Before') {
							$exectyperes = '-';
						} else if($exectype == 'After') {
							$exectyperes = '+';
						} else {
							$exectyperes = '';
						}
						if($ruleexec == 'TRUE') {
							//rule criteria
							$rulecritconddata = $this->workflowrulecriteriacond($wrkflowid);
							$i=0;
							$conds=array();
							foreach($rulecritconddata->result() as $conddatas) {
								$condfldname = $conddatas->fieldname;
								$conduitypeid = $conddatas->conditionuitypeid;
								$condtypename = $conddatas->conditionname;
								$condandortype = $conddatas->conditionandorvalue;
								$condvalue = $conddatas->conditionvalue;
								//field value
								if(isset($_POST[$condfldname])) {
									$filedval = $_POST[$condfldname];
									if($conduitypeid=='17' || $conduitypeid=='18' || $conduitypeid=='19' || $conduitypeid=='20' || $conduitypeid=='25' || $conduitypeid=='26' || $conduitypeid=='27' || $conduitypeid=='28' || $conduitypeid=='29') {
										$condvals = explode('-',$condvalue);
										$conds[$i] = array(0=>$filedval,1=>$condvals[0],2=>$condtypename,3=>$condandortype);
										$i++;
									} else {
										$conds[$i] = array(0=>$filedval,1=>$condvalue,2=>$condtypename,3=>$condandortype);
										$i++;
									}
								}
							}
							$wrkflwcond = $this->workflowrulecriteriacondgen($conds);
							$wrkflwconfchk = eval("return ('.$wrkflwcond.');") ? 'True':'false';
							//$wrkflwconfchk = ($wrkflwcond ?'True':'false');
							if($wrkflwcond=='' || $wrkflwconfchk=='True') {
								$sendalertdatas = $this->workflowsendalertinfo($wrkflowid);
								foreach($sendalertdatas as $sendalerdata) {
									$alerttype = $sendalerdata->workflowalerttype;
									if($alerttype=='email') {
										//work flow data alert[mail]
										$templatedata = $this->workflowruleemailalertgen($wrkflowid);
										$userid = $this->userid;
										foreach($templatedata as $datarow) {
											$addemailid = $datarow->workflowalertadditonalrecipient;
											if($datarow->moduletabid == $modid) {
												$parenttable = $primarytable;
												$primaryid = $primaryid;
											} else if($datarow->moduletabid != '4'){
												$primaryid = $this->generalinformaion($parenttable,$datarow->tablename.'id',$parenttable.'id',$primaryid);
												$parenttable = $datarow->tablename;
											}
											if($datarow->moduletabid == '4') {
												$emailid = $this->generalinformaion('employee','emailid','employeeid',$userid);
												if($emailid != '') {
													$this->workflowemailsent($datarow->moduleid,$datarow->emailtemplatesid,$datarow->emailtemplatesemailtemplate_editorfilename,$primarytable,$primaryid,$emailid,$addemailid);
												}
											} else {
												$emailid = $this->generalinformaion($parenttable,$datarow->columnname,$parenttable.'id',$primaryid);
												if($emailid != '') {
													$this->workflowemailsent($datarow->moduleid,$datarow->emailtemplatesid,$datarow->emailtemplatesemailtemplate_editorfilename,$primarytable,$primaryid,$emailid,$addemailid);
												}
											}
										}
									} else if($alerttype=='sms') {
										//work flow data alert[mail]
										$templatedata = $this->workflowrulesmsalertgen($wrkflowid);
										$userid = $this->userid;
										foreach($templatedata as $datarow) {
											$addemailid = $datarow->workflowalertadditonalrecipient;
											$parenttable = $primarytable;
											$id = $primaryid;
											$tempfile = $datarow->leadtemplatecontent_editorfilename;
											$htmlcontent = $this->smsgenerateprinthtmlfile($datarow->moduleid,$datarow->workflowtemplateid,$tempfile,$primarytable,$primaryid);
											$originaleDate = $this->datefieldvalue($datarow->moduleid,$fieldid,$id);
											$apikey = $datarow->apikey;
											$senderid = $datarow->senderid;
											$scheduledate = array();
											$plus3month = date("Y-m-d",strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . " +03 month"));
											$executiondateofmonth = date('m',strtotime($originaleDate));
											$executiondateofday = date('d',strtotime($originaleDate));
											$executiondateofyear = date('Y',strtotime($originaleDate));
											if($plus3month >= $originaleDate) {
												// Kalera Accepting max of 3 month from current date & execution time should be greater than at least 5 mins from current time.
												if($execfreq == 'Once' || $execfreq == 'Daily') {
													if($exectyperes == '') { // On Time of Execution
														if(date('m') == $executiondateofmonth) {
															if(date('d') <= $executiondateofday) {
																$scheduledate = date('Y').'-'.date('m-d',strtotime($originaleDate)).' '.date('h:i A',strtotime($exectime));
															}
														} else {
															$scheduledate = date('Y').'-'.date('m-d',strtotime($originaleDate)).' '.date('h:i A',strtotime($exectime));
														}
													} else {
														$originaleDate = date('Y-m-d', strtotime($originaleDate. " $exectyperes $freqtype days"));
														$executiondateofmonth = date('m',strtotime($originaleDate));
														$executiondateofday = date('d',strtotime($originaleDate));
														if(date('m') == $executiondateofmonth) {
															if(date('d') <= $executiondateofday) {
																$scheduledate = date('Y').'-'.date('m-d',strtotime($originaleDate)).' '.date('h:i A',strtotime($exectime));
															}
														} else {
															$scheduledate = date('Y').'-'.date('m-d',strtotime($originaleDate)).' '.date('h:i A',strtotime($exectime));
														}
													}
												} else if($execfreq == 'Monthly') {
													if($exectyperes == '') {
														$rdate = explode('-',$originaleDate);
														$execution_on_day = date('d',strtotime("$executiononday", strtotime(date('d')))) - 1;
														$execution_on_day = $execution_on_day-1;
														if(date('m') == $executiondateofmonth) {
															if(date('d') < $executiondateofday) {
																$scheduledate = $rdate[0].'-'.$rdate[1].'-'.date('d',strtotime("$execution_on_day Days", strtotime(date('d'))));
															}
														} else {
															$scheduledate = $rdate[0].'-'.$rdate[1].'-'.date('d',strtotime("$execution_on_day Days", strtotime(date('d'))));
														}
													} else {
														$day = 730;
														$sdate = strtotime("+".$day." day", strtotime(date('Y-m-d')));
														$startdate = date("Y-m-d");
														$ts1 = strtotime($startdate);
														$ts2 = strtotime($originaleDate);
														$year1 = date('Y', $ts1);
														$year2 = date('Y', $ts2);
														$month1 = date('m', $ts1);
														$month2 = date('m', $ts2);
														$diff = (($year2 - $year1) * 12) + ($month2 - $month1);
														$count = $diff;
														if($count == 0) {
															$rdate = explode('-',$originaleDate);
															if(date('d') < date('d',strtotime("$executiononday-1", strtotime(date('d'))))) {
																$scheduledate = $rdate[0].'-'.$rdate[1].'-'.date('d',strtotime("$executiononday-1", strtotime(date('d'))));
															}
														} else {
															$executiononday = date('d',strtotime("$executiononday", strtotime(date('d'))));
															for($i=0;$i<$count;$i++) {
																$scheduledate = $this->getstartdate($repeatevery,$startdate,$originaleDate,'16',$executiononday,$executiononday,$i);
															}
														}
													}
												} else if($execfreq == 'Weekly') {
													$startdate = date("Y-m-d");
													$count = 1;
													for($i=0;$i<$count;$i++) {
														$scheduledate = $this->getstartdate($repeatevery,$startdate,$originaleDate,'15',$executiononday,$executiononday,$i);
													}
												} else if($execfreq == 'Yearly') {
													if($exectyperes == '') { // On Time of Execution
														if(date('m') == $executiondateofmonth) {
															if(date('d') <= $executiondateofday) {
																$scheduledate = date('Y').'-'.date('m-d',strtotime($originaleDate)).' '.date('h:i A',strtotime($exectime));
															}
														} else {
															$scheduledate = date('Y').'-'.date('m-d',strtotime($originaleDate)).' '.date('h:i A',strtotime($exectime));
														}
													} else {
														$originaleDate = date('Y-m-d', strtotime($originaleDate. " $exectyperes $freqtype days"));
														$executiondateofmonth = date('m',strtotime($originaleDate));
														$executiondateofday = date('d',strtotime($originaleDate));
														if(date('m') == $executiondateofmonth) {
															if(date('d') <= $executiondateofday) {
																$scheduledate = date('Y').'-'.date('m-d',strtotime($originaleDate)).' '.date('h:i A',strtotime($exectime));
															}
														} else {
															$scheduledate = date('Y').'-'.date('m-d',strtotime($originaleDate)).' '.date('h:i A',strtotime($exectime));
														}
													}
												}
												print_r($scheduledate); die();
												if($scheduledate != '' || !empty($scheduledate)) {
													if($datarow->moduletabid == $modid) {
														$parenttable = $primarytable;
														$primaryid = $primaryid;
													} else if($datarow->moduletabid != '4') {
														$primaryid = $this->generalinformaion($parenttable,$datarow->tablename.'id',$parenttable.'id',$primaryid);
														$parenttable = $datarow->tablename;
													}
													if($datarow->moduletabid == '4') {
														$empmobnum = $this->generalinformaion('employee','mobilenumber','employeeid',$userid);
														if($empmobnum != '') {
															if($execfreq == 'Weekly') {
																foreach($scheduledate as $value) {
																	if($value != '') {
																		$explodedatevalue = explode(',',$value);
																		$schd_datevalue = date('Y-m-d',strtotime($explodedatevalue[0])).' '.date('h:i A',strtotime($exectime));
																		$this->sendsmsinallmodules($apikey,$senderid,$empmobnum,$htmlcontent,$schd_datevalue,$primaryid,$datarow->moduleid);
																	}
																}
															} else {
																$this->sendsmsinallmodules($apikey,$senderid,$empmobnum,$htmlcontent,$scheduledate,$primaryid,$datarow->moduleid);
															}
														}
													} else {
														$empmobnum = $this->generalinformaion($parenttable,$datarow->columnname,$parenttable.'id',$primaryid);
														if($empmobnum != '') {
															if($execfreq == 'Weekly') {
																foreach($scheduledate as $value) {
																	if($value != '') {
																		$explodedatevalue = explode(',',$value);
																		$schd_datevalue = date('Y-m-d',strtotime($explodedatevalue[0])).' '.date('h:i A',strtotime($exectime));
																		$this->sendsmsinallmodules($apikey,$senderid,$empmobnum,$htmlcontent,$schd_datevalue,$primaryid,$datarow->moduleid);
																	}
																}
															} else {
																$this->sendsmsinallmodules($apikey,$senderid,$empmobnum,$htmlcontent,$scheduledate,$primaryid,$datarow->moduleid);
															}
														}
													}
												}
											}
										}
									} else if($alerttype == 'call') {
										//work flow data alert[mail]
										$templatedata = $this->workflowrulecallsalertgen($wrkflowid);
										$userid = $this->userid;
										foreach($templatedata as $datarow) {
											$addcallnum = $datarow->workflowalertadditonalrecipient;
											$apikey = $datarow->apikey;
											$caller = $datarow->mobilenumber;
											$splay = $datarow->soundfileid;
											$play = $splay.'.sound';
											if($datarow->moduletabid == $modid) {
												$parenttable = $primarytable;
												$primaryid = $primaryid;
											} else if($datarow->moduletabid !='4'){
												$primaryid = $this->generalinformaion($parenttable,$datarow->tablename.'id',$parenttable.'id',$primaryid);
												$parenttable = $datarow->tablename;
											}
											if($datarow->moduletabid=='4') {
												$mobilenumber = $this->generalinformaion('employee','mobilenumber','employeeid',$userid);
												if($mobilenumber!='') {
													$this->workflowcallnotification($apikey,$play,$mobilenumber,$caller);
												}
											} else {
												$mobilenumber = $this->generalinformaion($parenttable,$datarow->columnname,$parenttable.'id',$primaryid);
												if($mobilenumber!='') {
													$this->workflowcallnotification($apikey,$play,$mobilenumber,$caller);
												}
											} 
										}									
									} else if($alerttype=='desktop') {
										//work flow data alert[desktop]
										$templatedata = $this->workflowruledesktopalertgen($wrkflowid);
										$userid = $this->userid;
										foreach($templatedata as $datarow) {
											$addemployees = $datarow->workflowalertadditonalrecipient;
											//generate addition user ids from roles,groups
											$adduserids = $this->additionaluseridscheck($addemployees);
											$parenttable = $primarytable;
											$id = $primaryid;
											$tempfile = $datarow->leadtemplatecontent_editorfilename;
											$this->load->model('Printtemplates/Printtemplatesmodel');
											$htmlcontent = $this->filecontentfetch($tempfile);
											if($datarow->moduletabid == $modid) {
												$parenttable = $primarytable;
												$primaryid = $primaryid;
											} else if($datarow->moduletabid !='4'){
												$primaryid = $this->generalinformaion($parenttable,$datarow->tablename.'id',$parenttable.'id',$primaryid);
												$parenttable = $datarow->tablename;
											}
											if($datarow->moduletabid=='4') {
												if($userid!='') {
													$this->workflowdesktopnotification('Added',$modid,$primaryid,$userid,$htmlcontent,$adduserids);
												}
											} else {
												$employeeid = $this->generalinformaion($parenttable,'employeeid',$parenttable.'id',$id,$adduserids);
												if($employeeid!='') {
													$this->workflowdesktopnotification('Added',$modid,$primaryid,$employeeid,$htmlcontent,$adduserids);
												}
											}
										}
									}
								}
								{//work flow task alert
									$alertdatas = $this->workflowruleactiondataalert($wrkflowid,$modid);
									foreach($alertdatas as $data) {
										$taskname = $data->workflowruleactiontaskname;
										$modfieldid = $data->taskalertduedate;
										$alerttimetype = $data->taskalerttype;
										$taskalertdays = $data->taskalertdays;
										$employeeid = $data->taskassignedto;
										$notifyassignee = $data->notifyassignee;
										if($notifyassignee=='Yes') {
											$sendat='';
											$extype = $alerttimetype=='plus'?'+':'-';
											if($modfieldid==1) {
												$sendat = date('Y-m-d H:i:s',strtotime(time().' '.$extype.$taskalertdays.' days'));
												if($sendat!='') {
													$this->workflowtasknotificationmailsent($taskname,$employeeid,$sendat);
												}
											} else {
												$fielddatas = $this->workflowtaskfieldinformation($modfieldid);
												foreach($fielddatas as $fielddata) {
													$table = $fielddata->tablename;
													$colname = $fielddata->columnname;
													$fieldname = $fielddata->fieldname;
													$primarytable = $fielddata->modulemastertable;
													$tableid = $primarytable.'id';
													$value = $this->generalinformaion($table,$colname,$tableid,$primaryid);
													if($value!='') {
														$taskvalue=$this->ymddateconversion($value);
														$sendat = date('Y-m-d',strtotime($taskvalue.' '.$extype.$taskalertdays.' days')).' '.date('H:i:s');
													}
													if($sendat!='') {
														$this->workflowtasknotificationmailsent($taskname,$employeeid,$sendat);
													}
												}
											}
										}
									}
								}
								{//field update
									$actionfldupdatedatas = $this->workflowactionfieldupdateinformation($wrkflowid,$modid);
									foreach($actionfldupdatedatas as $data) {
										$modfieldid = $data->modulefieldid;
										$value = $data->value;
										$uitypeid = $data->uitypeid;
										$fielddatas = $this->workflowtaskfieldinformation($modfieldid);
										foreach($fielddatas as $fielddata) {
											$table = $fielddata->tablename;
											$colname = $fielddata->columnname;
											$fieldname = $fielddata->fieldname;
											$primarytable = $fielddata->modulemastertable;
											$tableid = $primarytable.'id';
											if($uitypeid==17 || $uitypeid==18 || $uitypeid==19 || $uitypeid==20 || $uitypeid==21 || $uitypeid==23 || $uitypeid==25 || $uitypeid==26 || $uitypeid==27 || $uitypeid==28 || $uitypeid==29) {
												$ddvalue = explode('-',$value);
												$data = array($colname=>$ddvalue[0]);
												$this->db->where($tableid,$primaryid);
												$this->db->update($table,$data);
											} else {
												$data = array($colname=>$value);
												$this->db->where($tableid,$primaryid);
												$this->db->update($table,$data);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	//work flow manage in data update mode
	public function workflowmanageindataupdatemode($modid,$primaryid,$primarytable,$condstatvals) {
		$workflowinfo = $this->workflowinformationfetch($modid);
		$i=0;
		foreach($workflowinfo as $wkfdatas) {
			$wrkflwtrgtypeid = $wkfdatas->workflowtriggertypeid;
			if($wrkflwtrgtypeid=='1') {//record action
				$trgtypeid = $wkfdatas->triggerfrequencytype;
				$condinue = 'FALSE';
				if($trgtypeid==2 || $trgtypeid==3 || $trgtypeid==4) {
					if($trgtypeid==2 || $trgtypeid==3) {
						$condinue = 'TRUE';
					} else if($trgtypeid==4) {
						if($condstatvals[$i]!='') {
							if($condstatvals[$i]) {
								$condinue = 'TRUE';
							} else {
								$condinue = 'FALSE';
							}
						} else {
							$condinue = 'TRUE';
						}
					} else {
						$condinue = 'FALSE';
					}
				}
				$i++;
				if($condinue == 'TRUE') {
					//rule criteria
					$wrkflowid = $wkfdatas->workflowid;
					$rulecritconddata = $this->workflowrulecriteriacond($wrkflowid);
					$i=0;
					$conds=array();
					foreach($rulecritconddata->result() as $conddatas) {
						$condfldname = $conddatas->fieldname;
						$conduitypeid = $conddatas->conditionuitypeid;
						$condtypename = $conddatas->conditionname;
						$condandortype = $conddatas->conditionandorvalue;
						$condvalue = $conddatas->conditionvalue;
						//field value condition check
						if(isset($_POST[$condfldname])) {
							$filedval = $_POST[$condfldname];
							if($conduitypeid=='17' || $conduitypeid=='18' || $conduitypeid=='19' || $conduitypeid=='20' || $conduitypeid=='25' || $conduitypeid=='26' || $conduitypeid=='27' || $conduitypeid=='28' || $conduitypeid=='29') {
								$condvals = explode('-',$condvalue);
								$conds[$i] = array(0=>$filedval,1=>$condvals[0],2=>$condtypename,3=>$condandortype);
								$i++;
							} else {
								$conds[$i] = array(0=>$filedval,1=>$condvalue,2=>$condtypename,3=>$condandortype);
								$i++;
							}
						}
					}
					$wrkflwcond = $this->workflowrulecriteriacondgen($conds);
					$wrkflwconfchk = eval("return ('.$wrkflwcond.');") ? 'True':'false';
					//$wrkflwconfchk = ($wrkflwcond ?'True':'false');
					if($wrkflwcond=='' || $wrkflwconfchk=='True') {
						$sendalertdatas = $this->workflowsendalertinfo($wrkflowid);
						foreach($sendalertdatas as $sendalerdata) {
							$alerttype = $sendalerdata->workflowalerttype;
							if($alerttype=='email') {
								{//work flow data alert -- email
									$templatedata = $this->workflowruleemailalertgen($wrkflowid);
									$userid = $this->userid;
									foreach($templatedata as $datarow) {
										$addemailid = $datarow->workflowalertadditonalrecipient;
										if($addemailid != '') {
											$addemailiddata = $this->retrieveallrecipientemailids($datarow->workflowalertadditonalrecipient);
										}
										$manaddemailid = $datarow->workflowmanualalertadditonalrecipient;
										if($manaddemailid != '') {
											if(!empty($addemailiddata)) {
												$additionalusermail = implode(',',$addemailiddata);
												$addemailarrayformat = $additionalusermail.','.$manaddemailid;
											} else {
												$addemailarrayformat = $manaddemailid;
											}
										} else if(!empty($addemailiddata)) {
											$addemailarrayformat = implode(',',$addemailiddata);
										}
										if($datarow->moduletabid == $modid) {
											$parenttable = $primarytable;
											$primaryid = $primaryid;
										} else if($datarow->moduletabid !='4'){
											$primaryid = $this->generalinformaion($parenttable,$datarow->tablename.'id',$parenttable.'id',$primaryid);
											$parenttable = $datarow->tablename;
										}
										if($datarow->moduletabid=='4') {
											$empemailid = $this->generalinformaion('employee','emailid','employeeid',$userid);
											if($empemailid!='') {
												$this->workflowemailsent($datarow->moduleid,$datarow->emailtemplatesid,$datarow->emailtemplatesemailtemplate_editorfilename,$primarytable,$primaryid,$empemailid,$addemailid);
											}
										} else {
											$emailid = $this->generalinformaion($parenttable,$datarow->columnname,$parenttable.'id',$primaryid);
											if($emailid!='') {
												$this->workflowemailsent($datarow->moduleid,$datarow->emailtemplatesid,$datarow->emailtemplatesemailtemplate_editorfilename,$primarytable,$primaryid,$emailid,$addemailid);
											}
										}
									}
								}
							} else if($alerttype=='sms') {
								//work flow data alert[mail]
								$templatedata = $this->workflowrulesmsalertgen($wrkflowid);
								$userid = $this->userid;
								foreach($templatedata as $datarow) {
									$addmobnosarrayformat = '';
									$empmobnum = '';
									$addmobilenos = $datarow->workflowalertadditonalrecipient;
									if($addmobilenos != '') {
										$addmobilenosdata = $this->retrieveallrecipientmobilenos($datarow->workflowalertadditonalrecipient);
									}
									$manaddmobilenos = $datarow->workflowmanualalertadditonalrecipient;
									if($manaddmobilenos != '') {
										if(!empty($addmobilenosdata)) {
											$additionalusermobnos = implode(',',$addmobilenosdata);
											$addmobnosarrayformat = $additionalusermobnos.','.$manaddmobilenos;
										} else {
											$addmobnosarrayformat = $manaddmobilenos;
										}
									} else if(!empty($addmobilenosdata)) {
										$addmobnosarrayformat = implode(',',$addmobilenosdata);
									}
									$parenttable = $primarytable;
									$id = $primaryid;
									$tempfile = $datarow->leadtemplatecontent_editorfilename;
									$htmlcontent = $this->smsgenerateprinthtmlfile($datarow->moduleid,$datarow->workflowtemplateid,$tempfile,$primarytable,$primaryid);
									$apikey = $datarow->apikey;
									$senderid = $datarow->senderid;
									$scheduledate = '';
									if($datarow->moduletabid == $modid) {
										$parenttable = $primarytable;
										$primaryid = $primaryid;
									} else if($datarow->moduletabid !='4'){
										$primaryid = $this->generalinformaion($parenttable,$datarow->tablename.'id',$parenttable.'id',$primaryid);
										$parenttable = $datarow->tablename;
									}
									if($datarow->moduletabid=='4') {
										$empmobnum = $this->generalinformaion('employee','mobilenumber','employeeid',$userid);
									} else {
										$empmobnum = $this->generalinformaion($parenttable,$datarow->columnname,$parenttable.'id',$primaryid);
									}
									if($empmobnum!='') {
										$this->sendsmsinallmodules($apikey,$senderid,$empmobnum,$htmlcontent,$scheduledate,$primaryid,$datarow->moduleid);	
									}
									if($addmobnosarrayformat != '' || !empty($addmobnosarrayformat)) {
										$addmobnosarrayformat = explode(',',$addmobnosarrayformat);
										foreach($addmobnosarrayformat as $mobnos) {
											$this->sendsmsinallmodules($apikey,$senderid,$mobnos,$htmlcontent,$scheduledate,$primaryid,$datarow->moduleid);
										}
									}
								}
							} else if($alerttype=='call') {
								//work flow data alert[mail]
								$templatedata = $this->workflowrulecallsalertgen($wrkflowid);
								$userid = $this->userid;
								foreach($templatedata as $datarow) {
									$addcallnum = $datarow->workflowalertadditonalrecipient;
									$apikey = $datarow->apikey;
									$caller = $datarow->mobilenumber;
									$splay = $datarow->soundfileid;
									$play = $splay.'.sound';
									if($datarow->moduletabid == $modid) {
										$parenttable = $primarytable;
										$primaryid = $primaryid;
									} else if($datarow->moduletabid !='4'){
										$primaryid = $this->generalinformaion($parenttable,$datarow->tablename.'id',$parenttable.'id',$primaryid);
										$parenttable = $datarow->tablename;
									}
									if($datarow->moduletabid=='4') {
										$mobilenumber = $this->generalinformaion('employee','mobilenumber','employeeid',$userid);
										if($mobilenumber!='') {
											$this->workflowcallnotification($apikey,$play,$mobilenumber,$caller);
										}
									} else {
										$mobilenumber = $this->generalinformaion($parenttable,$datarow->columnname,$parenttable.'id',$primaryid);
										if($mobilenumber!='') {
											$this->workflowcallnotification($apikey,$play,$mobilenumber,$caller);
										}
									}
								}
							} else if($alerttype=='desktop') {
								//work flow data alert[desktop]
								$templatedata = $this->workflowruledesktopalertgen($wrkflowid);
								$userid = $this->userid;
								foreach($templatedata as $datarow) {
									$addemployees = $datarow->workflowalertadditonalrecipient;
									//generate addition user ids from roles,groups
									$adduserids = $this->additionaluseridscheck($addemployees);
									$parenttable = $primarytable;
									$id = $primaryid;
									$tempfile = $datarow->leadtemplatecontent_editorfilename;
									$this->load->model('Printtemplates/Printtemplatesmodel');
									$htmlcontent = $this->filecontentfetch($tempfile);
									if($datarow->moduletabid == $modid) {
										$parenttable = $primarytable;
										$primaryid = $primaryid;
									} else if($datarow->moduletabid !='4'){
										$primaryid = $this->generalinformaion($parenttable,$datarow->tablename.'id',$parenttable.'id',$primaryid);
										$parenttable = $datarow->tablename;
									}
									if($datarow->moduletabid=='4') {
										if($userid!='') {
											$this->workflowdesktopnotification('Updated',$modid,$primaryid,$userid,$htmlcontent,$adduserids);
										}
									} else {
										$employeeid = $this->generalinformaion($parenttable,'employeeid',$parenttable.'id',$id);
										if($employeeid!='') {
											$this->workflowdesktopnotification('Updated',$modid,$primaryid,$employeeid,$htmlcontent,$adduserids);
										}
									}
								}
							}
						}
						{//work flow task alert
							$alertdatas = $this->workflowruleactiondataalert($wrkflowid,$modid);
							foreach($alertdatas as $data) {
								$taskname = $data->workflowruleactiontaskname;
								$modfieldid = $data->taskalertduedate;
								$alerttimetype = $data->taskalerttype;
								$taskalertdays = $data->taskalertdays;
								$employeeid = $data->taskassignedto;
								$notifyassignee = $data->notifyassignee;
								if($notifyassignee=='Yes') {
									$sendat='';
									$extype = $alerttimetype=='plus'?'+':'-';
									if($modfieldid==1) {
										$sendat = date('Y-m-d H:i:s',strtotime(date('Y-m-d').' '.$extype.$taskalertdays.' days'));
										if($sendat!='') {
											$this->workflowtasknotificationmailsent($taskname,$employeeid,$sendat);
										}
									} else {
										$fielddatas = $this->workflowtaskfieldinformation($modfieldid);
										foreach($fielddatas as $fielddata) {
											$table = $fielddata->tablename;
											$colname = $fielddata->columnname;
											$fieldname = $fielddata->fieldname;
											$primarytable = $fielddata->modulemastertable;
											$tableid = $primarytable.'id';
											$value = $this->generalinformaion($table,$colname,$tableid,$primaryid);
											if($value!='') {
												$taskvalue=$this->ymddateconversion($value);
												$sendat = date('Y-m-d',strtotime($taskvalue.' '.$extype.$taskalertdays.' days')).' '.date('H:i:s');
											}
											if($sendat!='') {
												$this->workflowtasknotificationmailsent($taskname,$employeeid,$sendat);
											}
										}
									}
								}
							}
						}
						{//field update
							$actionfldupdatedatas = $this->workflowactionfieldupdateinformation($wrkflowid,$modid);
							foreach($actionfldupdatedatas as $data) {
								$modfieldid = $data->modulefieldid;
								$value = $data->value;
								$uitypeid = $data->uitypeid;
								$fielddatas = $this->workflowtaskfieldinformation($modfieldid);
								foreach($fielddatas as $fielddata) {
									$table = $fielddata->tablename;
									$colname = $fielddata->columnname;
									$fieldname = $fielddata->fieldname;
									$primarytable = $fielddata->modulemastertable;
									$tableid = $primarytable.'id';
									if($uitypeid==17 || $uitypeid==18 || $uitypeid==19 || $uitypeid==20 || $uitypeid==21 || $uitypeid==23 || $uitypeid==25 || $uitypeid==26 || $uitypeid==27 || $uitypeid==28 || $uitypeid==29) {
										$ddvalue = explode('-',$value);
										$data = array($colname=>$ddvalue[0]);
										$this->db->where($tableid,$primaryid);
										$this->db->update($table,$data);
									} else {
										$data = array($colname=>$value);
										$this->db->where($tableid,$primaryid);
										$this->db->update($table,$data);
									}
								}
							}
						}
					}
				}
			} else if($wrkflwtrgtypeid=='2') {//date action
				//date based actions
				$fieldid = $datas->triggerfieldid;
				$execfreq = $datas->triggerfrequencytype;
				$exectype = $datas->triggerexecutiontype;
				$freqtype = $datas->triggerexecutionfreqency;
				$exectime = $datas->triggerexecutiontime;
				//check field info
				$fldupdateval = $this->datafieldupdateinformationfetch(array($fieldid));
				foreach($fldupdateval as $flddatas) {
					$colname = $flddatas->columnname;
					$fldname = $flddatas->fieldname;
					$tblname = $flddatas->tablename;
					$ruleexec = 'FALSE';
					//if(isset($_POST[$fldname])) {
					if(isset($fldname)) {
						//$fldval = $_POST[$fldname];
						$fldval = $fldname;
						$dateformat = $this->phpmysqlappdateformatfetch();
						$datef = explode(' ',$dateformat['phpformat']);
						$curdate = date($datef[0]);
						$curtime = date('H:i');
						$curdatetime = $curdate.' '.$curtime;
						$trigdatetime = $fldval.' '.$exectime;
						//$extype = (($exectype=='Before')?'-':($exectype=='After')?'+':'-');
						$extype = ($exectype=='Before')?'-':(($exectype=='After')?'+':'-');
						if($execfreq=='Once') {
							if($exectype=='On') {
								$ruleexec = 'TRUE';
							} else {
								$trigdatetime = date($datef[0],strtotime($fldval.' '.$extype.$execfreq.' days')).' '.$exectime;
								if($trigdatetime==$curdatetime) {
									$ruleexec = 'TRUE';
								}
							}
						} else if($execfreq=='Weekly') {
							if($exectype=='On') {
								$ruleexec = 'TRUE';
							} else {
								$trigdatetime = date($datef[0],strtotime($fldval.' '.$extype.$execfreq.' days')).' '.$exectime;
								if($trigdatetime==$curdatetime) {
									$ruleexec = 'TRUE';
								}
							}
						} else if($execfreq=='Monthly') {
							if($exectype=='On') {
								$ruleexec = 'TRUE';
							} else {
								$trigdatetime = date($datef[0],strtotime($fldval.' '.$extype.$execfreq.' days')).' '.$exectime;
								if($trigdatetime==$curdatetime) {
									$ruleexec = 'TRUE';
								}
							}
						} else if($execfreq=='Yearly') {
							if($exectype=='On') {
								$ruleexec = 'TRUE';
							} else {
								$trigdatetime = date($datef[0],strtotime($fldval.' '.$extype.$execfreq.' days')).' '.$exectime;
								if($trigdatetime==$curdatetime) {
									$ruleexec = 'TRUE';
								}
							}
						}
						if($ruleexec == 'TRUE') {
							//rule criteria
							$rulecritconddata = $this->workflowrulecriteriacond($wrkflowid);
							$i=0;
							$conds=array();
							foreach($rulecritconddata->result() as $conddatas) {
								$condfldname = $conddatas->fieldname;
								$conduitypeid = $conddatas->conditionuitypeid;
								$condtypename = $conddatas->conditionname;
								$condandortype = $conddatas->conditionandorvalue;
								$condvalue = $conddatas->conditionvalue;
								//field value
								if(isset($_POST[$condfldname])) {
									$filedval = $_POST[$condfldname];
									if($conduitypeid=='17' || $conduitypeid=='18' || $conduitypeid=='19' || $conduitypeid=='20' || $conduitypeid=='25' || $conduitypeid=='26' || $conduitypeid=='27' || $conduitypeid=='28' || $conduitypeid=='29') {
										$condvals = explode('-',$condvalue);
										$conds[$i] = array(0=>$filedval,1=>$condvals[0],2=>$condtypename,3=>$condandortype);
										$i++;
									} else {
										$conds[$i] = array(0=>$filedval,1=>$condvalue,2=>$condtypename,3=>$condandortype);
										$i++;
									}
								}
							}
							$wrkflwcond = $this->workflowrulecriteriacondgen($conds);
							$wrkflwconfchk = eval("return ('.$wrkflwcond.');") ? 'True':'false';
							//$wrkflwconfchk = ($wrkflwcond ?'True':'false');
							if($wrkflwcond=='' || $wrkflwconfchk=='True') {
								$sendalertdatas = $this->workflowsendalertinfo($wrkflowid);
								foreach($sendalertdatas as $sendalerdata) {
									$alerttype = $sendalerdata->workflowalerttype;
									if($alerttype=='email') {
										{//work flow data alert
											$templatedata = $this->workflowruleemailalertgen($wrkflowid);
											$userid = $this->userid;
											foreach($templatedata as $datarow) {
												$addemailid = $datarow->workflowalertadditonalrecipient;
												if($datarow->moduletabid == $modid) {
													$parenttable = $primarytable;
													$primaryid = $primaryid;
												} else if($datarow->moduletabid !='4'){
													$primaryid = $this->generalinformaion($parenttable,$datarow->tablename.'id',$parenttable.'id',$primaryid);
													$parenttable = $datarow->tablename;
												}
												if($datarow->moduletabid=='4') {
													$emailid = $this->generalinformaion('employee','emailid','employeeid',$userid);
													if($emailid!='') {
														$this->workflowemailsent($datarow->moduleid,$datarow->emailtemplatesid,$datarow->emailtemplatesemailtemplate_editorfilename,$primarytable,$primaryid,$emailid,$addemailid);
													}
												} else {
													$emailid = $this->generalinformaion($parenttable,$datarow->columnname,$parenttable.'id',$primaryid);
													if($emailid!='') {
														$this->workflowemailsent($datarow->moduleid,$datarow->emailtemplatesid,$datarow->emailtemplatesemailtemplate_editorfilename,$primarytable,$primaryid,$emailid,$addemailid);
													}
												}
											}
										}
									} else if($alerttype=='sms') { 
										//work flow data alert[mail]
										$templatedata = $this->workflowrulesmsalertgen($wrkflowid);
										$userid = $this->userid;
										foreach($templatedata as $datarow) {
											$addemailid = $datarow->workflowalertadditonalrecipient;
											$parenttable = $primarytable;
											$id = $primaryid;
											$tempfile = $datarow->leadtemplatecontent_editorfilename;
											$htmlcontent = $this->smsgenerateprinthtmlfile($datarow->moduleid,$datarow->workflowtemplateid,$tempfile,$primarytable,$primaryid);
											$apikey = $datarow->apikey;
											$senderid = $datarow->senderid;
											$scheduledate = '';
											if($datarow->moduletabid == $modid) {
												$parenttable = $primarytable;
												$primaryid = $primaryid;
											} else if($datarow->moduletabid !='4'){
												$primaryid = $this->generalinformaion($parenttable,$datarow->tablename.'id',$parenttable.'id',$primaryid);
												$parenttable = $datarow->tablename;
											}
											if($datarow->moduletabid=='4') {
												$empmobnum = $this->generalinformaion('employee','mobilenumber','employeeid',$userid);
												if($empmobnum!='') {
													$this->sendsmsinallmodules($apikey,$senderid,$empmobnum,$htmlcontent,$scheduledate,$primaryid,$datarow->moduleid);
												}
											} else {
												$empmobnum = $this->generalinformaion($primarytable,$datarow->columnname,$primarytable.'id',$primaryid);
												if($empmobnum!='') {
													$this->sendsmsinallmodules($apikey,$senderid,$empmobnum,$htmlcontent,$scheduledate,$primaryid,$datarow->moduleid);	
												}
											}
										}
									} else if($alerttype=='call') {
										//work flow data alert[mail]
										$templatedata = $this->workflowrulecallsalertgen($wrkflowid);
										$userid = $this->userid;
										foreach($templatedata as $datarow) {
											$addcallnum = $datarow->workflowalertadditonalrecipient;
											$apikey = $datarow->apikey;
											$caller = $datarow->mobilenumber;
											$splay = $datarow->soundfileid;
											$play = $splay.'.sound';
											if($datarow->moduletabid == $modid) {
												$parenttable = $primarytable;
												$primaryid = $primaryid;
											} else if($datarow->moduletabid !='4') {
												$primaryid = $this->generalinformaion($parenttable,$datarow->tablename.'id',$parenttable.'id',$primaryid);
												$parenttable = $datarow->tablename;
											}
											if($datarow->moduletabid=='4') {
												$mobilenumber = $this->generalinformaion('employee','mobilenumber','employeeid',$userid);
												if($mobilenumber!='') {
													$this->workflowcallnotification($apikey,$play,$mobilenumber,$caller);
												}
											} else {
												$mobilenumber =  $this->generalinformaion($primarytable,$datarow->columnname,$primarytable.'id',$primaryid);
												if($mobilenumber!='') {
													$this->workflowcallnotification($apikey,$play,$mobilenumber,$caller);
												}
											}
										}
									} else if($alerttype=='desktop') {
										//work flow data alert[desktop]
										$templatedata = $this->workflowruledesktopalertgen($wrkflowid);
										$userid = $this->userid;
										foreach($templatedata as $datarow) {
											$addemployees = $datarow->workflowalertadditonalrecipient;
											//generate addition user ids from roles,groups
											$adduserids = $this->additionaluseridscheck($addemployees);
											$parenttable = $primarytable;
											$id = $primaryid;
											$tempfile = $datarow->leadtemplatecontent_editorfilename;
											$this->load->model('Printtemplates/Printtemplatesmodel');
											$htmlcontent = $this->filecontentfetch($tempfile);
											if($datarow->moduletabid == $modid) {
												$parenttable = $primarytable;
												$primaryid = $primaryid;
											} else if($datarow->moduletabid !='4'){
												$primaryid = $this->generalinformaion($parenttable,$datarow->tablename.'id',$parenttable.'id',$primaryid);
												$parenttable = $datarow->tablename;
											}
											if($datarow->moduletabid=='4') {
												if($userid!='') {
													$this->workflowdesktopnotification('Updated',$modid,$primaryid,$userid,$htmlcontent,$adduserids);
												}
											} else {
												$employeeid = $this->generalinformaion($parenttable,'employeeid',$parenttable.'id',$id);
												if($employeeid!='') {
													$this->workflowdesktopnotification('Updated',$modid,$primaryid,$employeeid,$htmlcontent,$adduserids);
												}
											}
										}
									}
								}
								{//work flow task alert
									$alertdatas = $this->workflowruleactiondataalert($wrkflowid,$modid);
									foreach($alertdatas as $data) {
										$taskname = $data->workflowruleactiontaskname;
										$modfieldid = $data->taskalertduedate;
										$alerttimetype = $data->taskalerttype;
										$taskalertdays = $data->taskalertdays;
										$employeeid = $data->taskassignedto;
										$notifyassignee = $data->notifyassignee;
										if($notifyassignee=='Yes') {
											$sendat='';
											$extype = $alerttimetype=='plus'?'+':'-';
											if($modfieldid==1) {
												$sendat = date('Y-m-d H:i:s',strtotime(time().' '.$extype.$taskalertdays.' days'));
												if($sendat!='') {
													$this->workflowtasknotificationmailsent($taskname,$employeeid,$sendat);
												}
											} else {
												$fielddatas = $this->workflowtaskfieldinformation($modfieldid);
												foreach($fielddatas as $fielddata) {
													$table = $fielddata->tablename;
													$colname = $fielddata->columnname;
													$fieldname = $fielddata->fieldname;
													$primarytable = $fielddata->modulemastertable;
													$tableid = $primarytable.'id';
													$value = $this->generalinformaion($table,$colname,$tableid,$primaryid);
													if($value!='') {
														$taskvalue=$this->ymddateconversion($value);
														$sendat = date('Y-m-d',strtotime($taskvalue.' '.$extype.$taskalertdays.' days')).' '.date('H:i:s');
													}
													if($sendat!='') {
														$this->workflowtasknotificationmailsent($taskname,$employeeid,$sendat);
													}
												}
											}
										}
									}
								}
								{//field update
									$actionfldupdatedatas = $this->workflowactionfieldupdateinformation($wrkflowid,$modid);
									foreach($actionfldupdatedatas as $data) {
										$modfieldid = $data->modulefieldid;
										$value = $data->value;
										$uitypeid = $data->uitypeid;
										$fielddatas = $this->workflowtaskfieldinformation($modfieldid);
										foreach($fielddatas as $fielddata) {
											$table = $fielddata->tablename;
											$colname = $fielddata->columnname;
											$fieldname = $fielddata->fieldname;
											$primarytable = $fielddata->modulemastertable;
											$tableid = $primarytable.'id';
											if($uitypeid==17 || $uitypeid==18 || $uitypeid==19 || $uitypeid==20 || $uitypeid==21 || $uitypeid==23 || $uitypeid==25 || $uitypeid==26 || $uitypeid==27 || $uitypeid==28 || $uitypeid==29) {
												$ddvalue = explode('-',$value);
												$data = array($colname=>$ddvalue[0]);
												$this->db->where($tableid,$primaryid);
												$this->db->update($table,$data);
											} else {
												$data = array($colname=>$value);
												$this->db->where($tableid,$primaryid);
												$this->db->update($table,$data);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	//work flow manage in data delete mode
	public function workflowmanageindatadeletemode($modid,$id,$primaryname,$primarytable) {
		$primaryid = $id;
		$workflowinfo = $this->workflowinformationfetch($modid);
		foreach($workflowinfo as $datas) {
			$wrkflwtrgtypeid = $datas->workflowtriggertypeid;
			if($wrkflwtrgtypeid=='1') {//record action
				$trgtypeid = $datas->triggerfrequencytype;
				if($trgtypeid==5) {
					//rule criteria
					$wrkflowid = $datas->workflowid;
					$rulecritconddata = $this->workflowrulecriteriacond($wrkflowid);
					$i=0;
					$conds=array();
					foreach($rulecritconddata->result() as $conddatas) {
						$condfldname = $conddatas->fieldname;
						$conduitypeid = $conddatas->conditionuitypeid;
						$condtypename = $conddatas->conditionname;
						$condandortype = $conddatas->conditionandorvalue;
						$condvalue = $conddatas->conditionvalue;
						//field value
						$mnamechk = $this->tablefieldnamecheck($conddatas->tablename,$conddatas->columnname);
						if($mnamechk=='true') {
							$fieldval = $this->generalinformaion($conddatas->tablename,$conddatas->columnname,$primaryname,$id);
							if($conduitypeid=='17' || $conduitypeid=='18' || $conduitypeid=='19' || $conduitypeid=='20' || $conduitypeid=='25' || $conduitypeid=='26' || $conduitypeid=='27' || $conduitypeid=='28' || $conduitypeid=='29') {
								$condvals = explode('-',$condvalue);
								$conds[$i] = array(0=>$fieldval,1=>$condvals[0],2=>$condtypename,3=>$condandortype);
								$i++;
							} else {
								$conds[$i] = array(0=>$fieldval,1=>$condvalue,2=>$condtypename,3=>$condandortype);
								$i++;
							}
						}
					}
					$wrkflwcond = $this->workflowrulecriteriacondgen($conds);
					$wrkflwconfchk = eval("return ('.$wrkflwcond.');") ? 'True':'false';
					//$wrkflwconfchk = ($wrkflwcond ?'True':'false');
					if($wrkflwcond=='' || $wrkflwconfchk=='True') {
						$sendalertdatas = $this->workflowsendalertinfo($wrkflowid);
						foreach($sendalertdatas as $sendalerdata) {
							$alerttype = $sendalerdata->workflowalerttype;
							if($alerttype=='email') {
								{//work flow data alert //mail
									$templatedata = $this->workflowruleemailalertgen($wrkflowid);
									$userid = $this->userid;
									foreach($templatedata as $datarow) {
										$addemailid = $datarow->workflowalertadditonalrecipient;
										if($datarow->moduletabid == $modid) {
											$parenttable = $primarytable;
											$primaryid = $primaryid;
										} else if($datarow->moduletabid !='4'){
											$primaryid = $this->generalinformaion($parenttable,$datarow->tablename.'id',$parenttable.'id',$primaryid);
											$parenttable = $datarow->tablename;
										}
										if($datarow->moduletabid=='4') {
											$empemailid = $this->generalinformaion('employee','emailid','employeeid',$userid);
											if($empemailid!='') {
												$this->workflowemailsent($datarow->moduleid,$datarow->emailtemplatesid,$datarow->emailtemplatesemailtemplate_editorfilename,$primarytable,$id,$empemailid,$addemailid);
											}
										} else {
											$emailid = $this->generalinformaion($parenttable,$datarow->columnname,$parenttable.'id',$primaryid);
											if($emailid!='') {
												$this->workflowemailsent($datarow->moduleid,$datarow->emailtemplatesid,$datarow->emailtemplatesemailtemplate_editorfilename,$primarytable,$id,$emailid,$addemailid);
											}
										}
									}
								}
							}  else if($alerttype=='sms') {
								//work flow data alert[mail]
								$templatedata = $this->workflowrulesmsalertgen($wrkflowid);
								$userid = $this->userid;
								foreach($templatedata as $datarow) {
									$addemailid = $datarow->workflowalertadditonalrecipient;
									$parenttable = $primarytable;
									$id = $primaryid;
									$tempfile = $datarow->leadtemplatecontent_editorfilename;
									$htmlcontent = $this->smsgenerateprinthtmlfile($datarow->moduleid,$datarow->workflowtemplateid,$tempfile,$primarytable,$primaryid);
									$apikey = $datarow->apikey;
									$senderid = $datarow->senderid;
									$scheduledate = '';
									if($datarow->moduletabid == $modid) {
										$parenttable = $primarytable;
										$primaryid = $primaryid;
									} else if($datarow->moduletabid !='4') {
										$primaryid = $this->generalinformaion($parenttable,$datarow->tablename.'id',$parenttable.'id',$primaryid);
										$parenttable = $datarow->tablename;
									}
									if($datarow->moduletabid=='4') {
										$empmobnum = $this->generalinformaion('employee','mobilenumber','employeeid',$userid);
										if($empmobnum!='') {
											$this->sendsmsinallmodules($apikey,$senderid,$empmobnum,$htmlcontent,$scheduledate,$id,$datarow->moduleid);
										}
									} else {
										$empmobnum =$this->generalinformaion($primarytable,$datarow->columnname,$primarytable.'id',$primaryid);
										if($empmobnum!='') {
											$this->sendsmsinallmodules($apikey,$senderid,$empmobnum,$htmlcontent,$scheduledate,$id,$datarow->moduleid);	
										}
									}
								}
							} else if($alerttype=='call') {
								//work flow data alert[mail]
								$templatedata = $this->workflowrulecallsalertgen($wrkflowid);
								$userid = $this->userid;
								foreach($templatedata as $datarow) {
									$addcallnum = $datarow->workflowalertadditonalrecipient;
									$apikey = $datarow->apikey;
									$caller = $datarow->mobilenumber;
									$splay = $datarow->soundfileid;
									$play = $splay.'.sound';
									if($datarow->moduletabid == $modid) {
										$parenttable = $primarytable;
										$primaryid = $primaryid;
									} else if($datarow->moduletabid !='4') {
										$primaryid = $this->generalinformaion($parenttable,$datarow->tablename.'id',$parenttable.'id',$primaryid);
										$parenttable = $datarow->tablename;
									}
									if($datarow->moduletabid=='4') {
										$mobilenumber = $this->generalinformaion('employee','mobilenumber','employeeid',$userid);
										if($mobilenumber!='') {
											$this->workflowcallnotification($apikey,$play,$mobilenumber,$caller);
										}
									} else {
										$mobilenumber = $this->generalinformaion($primarytable,$datarow->columnname,$primarytable.'id',$primaryid);
										if($mobilenumber!='') {
											$this->workflowcallnotification($apikey,$play,$mobilenumber,$caller);
										}
									}
								}
							} else if($alerttype=='desktop') {
								//work flow data alert[desktop]
								$templatedata = $this->workflowruledesktopalertgen($wrkflowid);
								$userid = $this->userid;
								foreach($templatedata as $datarow) {
									$addemployees = $datarow->workflowalertadditonalrecipient;
									//generate addition user ids from roles,groups
									$adduserids = $this->additionaluseridscheck($addemployees);
									$parenttable = $primarytable;
									$tempfile = $datarow->leadtemplatecontent_editorfilename;
									$this->load->model('Printtemplates/Printtemplatesmodel');
									$htmlcontent = $this->filecontentfetch($tempfile);
									if($datarow->moduletabid == $modid) {
										$parenttable = $primarytable;
										$primaryid = $primaryid;
									} else if($datarow->moduletabid !='4') {
										$primaryid = $this->generalinformaion($parenttable,$datarow->tablename.'id',$parenttable.'id',$primaryid);
										$parenttable = $datarow->tablename;
									}
									if($datarow->moduletabid=='4') {
										if($userid!='') {
											$this->workflowdesktopnotification('Deleted',$modid,$id,$userid,$htmlcontent,$adduserids);
										}
									} else {
										$employeeid = $this->generalinformaion($parenttable,'employeeid',$parenttable.'id',$id);
										if($employeeid!='') {
											$this->workflowdesktopnotification('Deleted',$modid,$id,$employeeid,$htmlcontent,$adduserids);
										}
									}
								}
							}
						}
						{//work flow task alert
							$alertdatas = $this->workflowruleactiondataalert($wrkflowid,$modid);
							foreach($alertdatas as $data) {
								$taskname = $data->workflowruleactiontaskname;
								$modfieldid = $data->taskalertduedate;
								$alerttimetype = $data->taskalerttype;
								$taskalertdays = $data->taskalertdays;
								$employeeid = $data->taskassignedto;
								$notifyassignee = $data->notifyassignee;
								if($notifyassignee=='Yes') {
									$sendat='';
									$extype = $alerttimetype=='plus'?'+':'-';
									if($modfieldid==1) {
										$sendat = date('Y-m-d H:i:s',strtotime(time().' '.$extype.$taskalertdays.' days'));
										if($sendat!='') {
											$this->workflowtasknotificationmailsent($taskname,$employeeid,$sendat);
										}
									} else {
										$fielddatas = $this->workflowtaskfieldinformation($modfieldid);
										foreach($fielddatas as $fielddata) {
											$table = $fielddata->tablename;
											$colname = $fielddata->columnname;
											$fieldname = $fielddata->fieldname;
											$primarytable = $fielddata->modulemastertable;
											$tableid = $primarytable.'id';
											$value = $this->generalinformaion($table,$colname,$tableid,$primaryid);
											if($value!='') {
												$taskvalue=$this->ymddateconversion($value);
												$sendat = date('Y-m-d',strtotime($taskvalue.' '.$extype.$taskalertdays.' days')).' '.date('H:i:s');
											}
											if($sendat!='') {
												$this->workflowtasknotificationmailsent($taskname,$employeeid,$sendat);
											}
										}
									}
								}
							}
						}
						{//field update
							$actionfldupdatedatas = $this->workflowactionfieldupdateinformation($wrkflowid,$modid);
							foreach($actionfldupdatedatas as $data) {
								$modfieldid = $data->modulefieldid;
								$value = $data->value;
								$uitypeid = $data->uitypeid;
								$fielddatas = $this->workflowtaskfieldinformation($modfieldid);
								foreach($fielddatas as $fielddata) {
									$table = $fielddata->tablename;
									$colname = $fielddata->columnname;
									$fieldname = $fielddata->fieldname;
									$primarytable = $fielddata->modulemastertable;
									$tableid = $primarytable.'id';
									if($uitypeid==17 || $uitypeid==18 || $uitypeid==19 || $uitypeid==20 || $uitypeid==21 || $uitypeid==23 || $uitypeid==25 || $uitypeid==26 || $uitypeid==27 || $uitypeid==28 || $uitypeid==29) {
										$ddvalue = explode('-',$value);
										$data = array($colname=>$ddvalue[0]);
										$this->db->where($tableid,$primaryid);
										$this->db->update($table,$data);
									} else {
										$data = array($colname=>$value);
										$this->db->where($tableid,$primaryid);
										$this->db->update($table,$data);
									}
								}
							}
						}
					}
				}
			} else if($wrkflwtrgtypeid=='2') {//date action
				$wrkflowid = $datas->workflowid;
				//date based actions
				$fieldid = $datas->triggerfieldid;
				$execfreq = $datas->triggerfrequencytype;
				$exectype = $datas->triggerexecutiontype;
				$freqtype = $datas->triggerexecutionfreqency;
				$exectime = $datas->triggerexecutiontime;
				//check field info
				$fldupdateval = $this->datafieldupdateinformationfetch(array($fieldid));
				foreach($fldupdateval as $flddatas) {
					$colname = $flddatas->columnname;
					$fldname = $flddatas->fieldname;
					$tblname = $flddatas->tablename;
					$ruleexec = 'FALSE';
					$mnamechk = $this->tablefieldnamecheck($tblname,$colname);
					if($mnamechk=='true') {
						$fldval = $this->generalinformaion($tblname,$colname,$primaryname,$id);
						$dateformat = $this->phpmysqlappdateformatfetch();
						$datef = explode(' ',$dateformat['phpformat']);
						$curdate = date($datef[0]);
						$curtime = date('H:i');
						$curdatetime = $curdate.' '.$curtime;
						$trigdatetime = $fldval.' '.$exectime;
						//$extype = (($exectype=='Before')?'-':($exectype=='After')?'+':'-');
						$extype = ($exectype=='Before')?'-':(($exectype=='After')?'+':'-');
						if($execfreq=='Once') {
							if($exectype=='On') {
								$ruleexec = 'TRUE';
							} else {
								$trigdatetime = date($datef[0],strtotime($fldval.' '.$extype.$execfreq.' days')).' '.$exectime;
								if($trigdatetime==$curdatetime) {
									$ruleexec = 'TRUE';
								}
							}
						} else if($execfreq=='Weekly') {
							if($exectype=='On') {
								$ruleexec = 'TRUE';
							} else {
								$trigdatetime = date($datef[0],strtotime($fldval.' '.$extype.$execfreq.' days')).' '.$exectime;
								if($trigdatetime==$curdatetime) {
									$ruleexec = 'TRUE';
								}
							}
						} else if($execfreq=='Monthly') {
							if($exectype=='On') {
								$ruleexec = 'TRUE';
							} else {
								$trigdatetime = date($datef[0],strtotime($fldval.' '.$extype.$execfreq.' days')).' '.$exectime;
								if($trigdatetime==$curdatetime) {
									$ruleexec = 'TRUE';
								}
							}
						} else if($execfreq=='Yearly') {
							if($exectype=='On') {
								$ruleexec = 'TRUE';
							} else {
								$trigdatetime = date($datef[0],strtotime($fldval.' '.$extype.$execfreq.' days')).' '.$exectime;
								if($trigdatetime==$curdatetime) {
									$ruleexec = 'TRUE';
								}
							}
						}
						if($ruleexec == 'TRUE') {
							//rule criteria
							$rulecritconddata = $this->workflowrulecriteriacond($wrkflowid);
							$i=0;
							$conds=array();
							foreach($rulecritconddata->result() as $conddatas) {
								$condfldname = $conddatas->fieldname;
								$conduitypeid = $conddatas->conditionuitypeid;
								$condtypename = $conddatas->conditionname;
								$condandortype = $conddatas->conditionandorvalue;
								$condvalue = $conddatas->conditionvalue;
								//field value
								$mnamechk = $this->tablefieldnamecheck($conddatas->tablename,$conddatas->columnname);
								if($mnamechk=='true') {
									$fieldval = $this->generalinformaion($conddatas->tablename,$conddatas->columnname,$primaryname,$id);
									if($conduitypeid=='17' || $conduitypeid=='18' || $conduitypeid=='19' || $conduitypeid=='20' || $conduitypeid=='25' || $conduitypeid=='26' || $conduitypeid=='27' || $conduitypeid=='28' || $conduitypeid=='29') {
										$condvals = explode('-',$condvalue);
										$conds[$i] = array(0=>$fieldval,1=>$condvals[0],2=>$condtypename,3=>$condandortype);
										$i++;
									} else {
										$conds[$i] = array(0=>$fieldval,1=>$condvalue,2=>$condtypename,3=>$condandortype);
										$i++;
									}
								}
							}
							$wrkflwcond = $this->workflowrulecriteriacondgen($conds);
							//$result = eval('return ('.$wrkflwcond.');'); // This is working condition
							$wrkflwconfchk = eval("return ('.$wrkflwcond.');") ? 'True':'false';
							//$wrkflwconfchk = ($wrkflwcond ?'True':'false');
							if($wrkflwcond=='' || $wrkflwconfchk=='True') {
								$sendalertdatas = $this->workflowsendalertinfo($wrkflowid);
								foreach($sendalertdatas as $sendalerdata) {
									$alerttype = $sendalerdata->workflowalerttype;
									if($alerttype=='email') {
										{//work flow data alert
											$templatedata = $this->workflowruleemailalertgen($wrkflowid);
											$userid = $this->userid;
											foreach($templatedata as $datarow) {
												$addemailid = $datarow->workflowalertadditonalrecipient;
												if($datarow->moduletabid == $modid) {
													$parenttable = $primarytable;
													$primaryid = $primaryid;
												} else if($datarow->moduletabid !='4') {
													$primaryid = $this->generalinformaion($parenttable,$datarow->tablename.'id',$parenttable.'id',$primaryid);
													$parenttable = $datarow->tablename;
												}
												if($datarow->moduletabid=='4') {
													$empemailid = $this->generalinformaion('employee','emailid','employeeid',$userid);
													if($empemailid!='') {
														$this->workflowemailsent($datarow->moduleid,$datarow->emailtemplatesid,$datarow->emailtemplatesemailtemplate_editorfilename,$primarytable,$primaryid,$empemailid,$addemailid);
													}
												} else {
													$emailid = $emailid = $this->generalinformaion($parenttable,$datarow->columnname,$parenttable.'id',$primaryid);
													if($emailid!='') {
														$this->workflowemailsent($datarow->moduleid,$datarow->emailtemplatesid,$datarow->emailtemplatesemailtemplate_editorfilename,$primarytable,$primaryid,$emailid,$addemailid);
													}
												}
											}
										}
									}  else if($alerttype=='sms') {
										//work flow data alert[mail]
										$templatedata = $this->workflowrulesmsalertgen($wrkflowid);
										$userid = $this->userid;
										foreach($templatedata as $datarow) {
											$addemailid = $datarow->workflowalertadditonalrecipient;
											$parenttable = $primarytable;
											$id = $primaryid;
											$tempfile = $datarow->leadtemplatecontent_editorfilename;
											$htmlcontent = $this->smsgenerateprinthtmlfile($datarow->moduleid,$datarow->workflowtemplateid,$tempfile,$primarytable,$primaryid);
											$apikey = $datarow->apikey;
											$senderid = $datarow->senderid;
											$scheduledate = '';
											if($datarow->moduletabid == $modid) {
												$parenttable = $primarytable;
												$primaryid = $primaryid;
											} else if($datarow->moduletabid !='4') {
												$primaryid = $this->generalinformaion($parenttable,$datarow->tablename.'id',$parenttable.'id',$primaryid);
												$parenttable = $datarow->tablename;
											}
											if($datarow->moduletabid=='4') {
												$empmobnum = $this->generalinformaion('employee','mobilenumber','employeeid',$userid);
												if($empmobnum!='') {
													$this->sendsmsinallmodules($apikey,$senderid,$empmobnum,$htmlcontent,$scheduledate,$primaryid,$datarow->moduleid);
												}
											} else {
												$empmobnum = $this->generalinformaion($primarytable,$datarow->columnname,$primarytable.'id',$primaryid);
												if($empmobnum!='') {
													$this->sendsmsinallmodules($apikey,$senderid,$empmobnum,$htmlcontent,$scheduledate,$primaryid,$datarow->moduleid);	
												}
											}
										}
									} else if($alerttype=='call') {
										//work flow data alert[mail]
										$templatedata = $this->workflowrulecallsalertgen($wrkflowid);
										$userid = $this->userid;
										foreach($templatedata as $datarow) {
											$addcallnum = $datarow->workflowalertadditonalrecipient;
											$apikey = $datarow->apikey;
											$caller = $datarow->mobilenumber;
											$splay = $datarow->soundfileid;
											$play = $splay.'.sound';
											if($datarow->moduletabid == $modid) {
												$parenttable = $primarytable;
												$primaryid = $primaryid;
											} else if($datarow->moduletabid !='4') {
												$primaryid = $this->generalinformaion($parenttable,$datarow->tablename.'id',$parenttable.'id',$primaryid);
												$parenttable = $datarow->tablename;
											}
											if($datarow->moduletabid=='4') {
												$mobilenumber = $this->generalinformaion('employee','mobilenumber','employeeid',$userid);
												if($mobilenumber!='') {
													$this->workflowcallnotification($apikey,$play,$mobilenumber,$caller);
												}
											} else {
												$mobilenumber = $this->generalinformaion($parenttable,$datarow->columnname,$parenttable.'id',$primaryid);
												if($mobilenumber!='') {
													$this->workflowcallnotification($apikey,$play,$mobilenumber,$caller);
												}
											}
										}
									} else if($alerttype=='desktop') {
										//work flow data alert[desktop]
										$templatedata = $this->workflowruledesktopalertgen($wrkflowid);
										$userid = $this->userid;
										foreach($templatedata as $datarow) {
											$addemployees = $datarow->workflowalertadditonalrecipient;
											//generate addition user ids from roles,groups
											$adduserids = $this->additionaluseridscheck($addemployees);
											$parenttable = $primarytable;
											$id = $primaryid;
											$tempfile = $datarow->leadtemplatecontent_editorfilename;
											$this->load->model('Printtemplates/Printtemplatesmodel');
											$htmlcontent = $this->filecontentfetch($tempfile);
											if($datarow->moduletabid == $modid) {
												$parenttable = $primarytable;
												$primaryid = $primaryid;
											} else if($datarow->moduletabid !='4') {
												$primaryid = $this->generalinformaion($parenttable,$datarow->tablename.'id',$parenttable.'id',$primaryid);
												$parenttable = $datarow->tablename;
											}
											if($datarow->moduletabid=='4') {
												if($userid!='') {
													$this->workflowdesktopnotification('Deleted',$modid,$id,$userid,$htmlcontent,$adduserids);
												}
											} else {
												$employeeid = $this->generalinformaion($parenttable,$datarow->columnname,$parenttable.'id',$primaryid);
												if($employeeid!='') {
													$this->workflowdesktopnotification('Deleted',$modid,$id,$employeeid,$htmlcontent,$adduserids);
												}
											}
										}
									}
								}
								{//work flow task alert
									$alertdatas = $this->workflowruleactiondataalert($wrkflowid,$modid);
									foreach($alertdatas as $data) {
										$taskname = $data->workflowruleactiontaskname;
										$modfieldid = $data->taskalertduedate;
										$alerttimetype = $data->taskalerttype;
										$taskalertdays = $data->taskalertdays;
										$employeeid = $data->taskassignedto;
										$notifyassignee = $data->notifyassignee;
										if($notifyassignee=='Yes') {
											$sendat='';
											$extype = $alerttimetype=='plus'?'+':'-';
											if($modfieldid==1) {
												$sendat = date('Y-m-d H:i:s',strtotime(time().' '.$extype.$taskalertdays.' days'));
												if($sendat!='') {
													$this->workflowtasknotificationmailsent($taskname,$employeeid,$sendat);
												}
											} else {
												$fielddatas = $this->workflowtaskfieldinformation($modfieldid);
												foreach($fielddatas as $fielddata) {
													$table = $fielddata->tablename;
													$colname = $fielddata->columnname;
													$fieldname = $fielddata->fieldname;
													$primarytable = $fielddata->modulemastertable;
													$tableid = $primarytable.'id';
													$value = $this->generalinformaion($table,$colname,$tableid,$primaryid);
													if($value!='') {
														$taskvalue=$this->ymddateconversion($value);
														$sendat = date('Y-m-d',strtotime($taskvalue.' '.$extype.$taskalertdays.' days')).' '.date('H:i:s');
													}
													if($sendat!='') {
														$this->workflowtasknotificationmailsent($taskname,$employeeid,$sendat);
													}
												}
											}
										}
									}
								}
								{//field update
									$actionfldupdatedatas = $this->workflowactionfieldupdateinformation($wrkflowid,$modid);
									foreach($actionfldupdatedatas as $data) {
										$modfieldid = $data->modulefieldid;
										$value = $data->value;
										$uitypeid = $data->uitypeid;
										$fielddatas = $this->workflowtaskfieldinformation($modfieldid);
										foreach($fielddatas as $fielddata) {
											$table = $fielddata->tablename;
											$colname = $fielddata->columnname;
											$fieldname = $fielddata->fieldname;
											$primarytable = $fielddata->modulemastertable;
											$tableid = $primarytable.'id';
											if($uitypeid==17 || $uitypeid==18 || $uitypeid==19 || $uitypeid==20 || $uitypeid==21 || $uitypeid==23 || $uitypeid==25 || $uitypeid==26 || $uitypeid==27 || $uitypeid==28 || $uitypeid==29) {
												$ddvalue = explode('-',$value);
												$data = array($colname=>$ddvalue[0]);
												$this->db->where($tableid,$primaryid);
												$this->db->update($table,$data);
											} else {
												$data = array($colname=>$value);
												$this->db->where($tableid,$primaryid);
												$this->db->update($table,$data);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	//work flow - get user ids from roles & groups
	public function additionaluseridscheck($addemployees){
		$i = 0;
		$emptypeids = array();
		$empdataids = array();
		if($addemployees!='' && $addemployees!='1') {
			$empgrprolempids = explode(',',$addemployees);
			foreach($empgrprolempids as $empgrproleid) {
				$empidinfo = explode(':',$empgrproleid);
				$emptype = $empidinfo[0];
				if($emptype == 1) {
					$empdataids[$i] = $empidinfo[1];
					$i++;
				} else if($emptype == 2) {
					$empdataids[$i] = $this->empidfetchfromgroup($empidinfo[1]);
					$i++;
				} else if($emptype == 3) {
					$empdataids[$i] = $this->empidfetchfromroles($empidinfo[1]);
					$i++;
				} else if($emptype == 4) {
					$empdataids[$i] = $this->empidfetchfromrolesandsubroles($empidinfo[1]);
					$i++;
				}
			}
		}
		return $empdataids;
	}
	//work flow manage -- for old row values fetch
	public function workflowmanageolddatainfofetch($modid,$primaryid) {
		$wkflwfldupdateinfo = $this->workflowinformationfetch($modid);
		$i=0;
		$condstatvals=array();
		foreach($wkflwfldupdateinfo as $wkupdate) {
			$wrkflwtrgtypeid = $wkupdate->workflowtriggertypeid;
			if($wrkflwtrgtypeid=='1') {//record action
				$trgtypeid = $wkupdate->triggerfrequencytype;
				$fldupdatecondtype = $wkupdate->triggerexecutionfreqency;
				$condstatus='';
				if($trgtypeid==4) {
					$modfieldids = explode(',',$wkupdate->triggerfieldid);
					$fldupdateval = $this->datafieldupdateinformationfetch($modfieldids);
					//generate condition
					$fldcondvals = array();
					foreach($fldupdateval AS $dataset) {
						$fldcondvals[$dataset->fieldname] = $this->generalinformaion($dataset->tablename,$dataset->columnname,$dataset->tablename.'id',$primaryid);
					}
					$condstatus = $this->fieldupdateconditiongen($fldcondvals,$fldupdatecondtype);
				}
				$condstatvals[$i]=$condstatus;
				$i++;
			}
		}
		return $condstatvals;
	}
	//workflow notification
	public function workflowinformationfetch($moduleid) {
		$this->db->select('workflow.workflowid,workflow.workflowtriggertypeid,workflow.workflowactive,workflowtrigger.triggerfieldid,workflowtrigger.triggerfrequencytype,workflowtrigger.triggerexecutiontype,workflowtrigger.triggerexecutionfreqency,workflowtrigger.triggerexecutiontime,workflow.moduleid,workflowtrigger.triggerrepeatevery,workflowtrigger.triggerexecutiononday');
		$this->db->from('workflow');
		$this->db->join('workflowtrigger','workflowtrigger.workflowid=workflow.workflowid');
		$this->db->where('workflow.workflowactive','Yes');
		$this->db->where('workflow.moduleid',$moduleid);
		$this->db->where('workflow.status',1);
		$wrkdatasets = $this->db->get()->result();
		return $wrkdatasets;
	}
	//workflow rule criteria conditions
	public function workflowrulecriteriacond($wrkflowid) {
		$this->db->select('workflowtriggercondition.workflowid,workflowtriggercondition.conditionname,workflowtriggercondition.conditionvalue,workflowtriggercondition.conditionandorvalue,workflowtriggercondition.conditionuitypeid,modulefield.modulefieldid,modulefield.fieldname,modulefield.columnname,modulefield.tablename');
		$this->db->from('workflowtriggercondition');
		$this->db->join('modulefield','modulefield.modulefieldid=workflowtriggercondition.modulefieldid');
		$this->db->where('workflowtriggercondition.workflowid',$wrkflowid);
		$this->db->where('workflowtriggercondition.status',1);
		$datasets = $this->db->get();
		return $datasets;
	}
	//workflow rule generate
	public function workflowruleemailalertgen($workflowid) {
		$this->db->select('workflowalertadditonalrecipient,workflowmanualalertadditonalrecipient,modulefield.fieldname,modulefield.columnname,modulefield.tablename,modulefield.moduletabid,emailtemplates.emailtemplatesemailtemplate_editorfilename,emailtemplates.moduleid,emailtemplates.emailtemplatesid',false);
		$this->db->from('workflowruleactionalert');
		$this->db->join('emailtemplates','emailtemplates.emailtemplatesid=workflowruleactionalert.workflowtemplateid');
		$this->db->join('modulefield','workflowruleactionalert.workflowalertrecipient=modulefield.modulefieldid');
		$this->db->where('workflowruleactionalert.workflowid',$workflowid);
		$this->db->where('workflowruleactionalert.workflowalerttype','email');
		$this->db->where('workflowruleactionalert.status',1);
		$datas = $this->db->get()->result();
		return $datas;
	}
	//work flow send alert information fetch
	public function workflowsendalertinfo($workflowid) {
		$this->db->select('workflowruleactionalertid,workflow.workflowid,workflowalerttype,workflowalertname',false);
		$this->db->from('workflowruleactionalert');
		$this->db->join('workflow','workflow.workflowid=workflowruleactionalert.workflowid');
		$this->db->where('workflowruleactionalert.workflowid',$workflowid);
		$datas = $this->db->get()->result();
		return $datas;
	}
	//work flow rule action task
	public function workflowruleactiondataalert($workflowid,$moduleid) {
		$this->db->select('workflowruleactiontaskid,workflowruleactiontaskname,moduleid,taskalertduedate,taskalerttype,taskalertdays,taskstatusid,taskpriorityid,taskassignedto,notifyassignee,description');
		$this->db->from('workflowruleactiontask');
		$this->db->where('workflowid',$workflowid);
		$this->db->where('moduleid',$moduleid);
		$datas = $this->db->get()->result();
		return $datas;
	}
	//work flow rule action task
	public function workflowactionfieldupdateinformation($workflowid,$moduleid) {
		$this->db->select('workflowruleactionfieldid,workflowruleactionfieldname,moduleid,modulefieldid,uitypeid,value');
		$this->db->from('workflowruleactionfield');
		$this->db->where('workflowid',$workflowid);
		$this->db->where('moduleid',$moduleid);
		$datas = $this->db->get()->result();
		return $datas;
	}
	//data field update information fetch
	public function datafieldupdateinformationfetch($modfieldids) {
		$info = $this->db->select('modulefieldid,columnname,fieldname,tablename')->from('modulefield')->where_in('modulefieldid',$modfieldids)->where('status',1)->get()->result();
		return $info;
	}
	//field update condition generate
	public function fieldupdateconditiongen($fldcondvals,$fldupdatecondtype) {
		$condname = (($fldupdatecondtype=='All')?' AND ':' OR ');
		$m=0;
		$whereString="";
		foreach($fldcondvals AS $fname=>$val) {
			if($val!='') {
				if(isset($_POST[$fname])) {
					if($m==0) {
						$whereString.= ''.$_POST[$fname].'!='.$val.'';
					} else if($m==1) {
						$whereString='('.$whereString.$condname.''.$_POST[$fname].'!='.$val.')';
					} else if($m>=2) {
						$whereString.=''.$condname.''.$_POST[$fname].'!='.$val.')';
					}
					$m++;
				}
			}
		}
		return $whereString;
	}
	//sms template fetch
	public function smsgenerateprinthtmlfile($moduleid,$templateid,$tempfile,$parenttable,$primaryid) {
		$this->load->model('Printtemplates/Printtemplatesmodel');
		//defaults
		$print_data=array('templateid'=> $templateid, 'Templatetype'=>'Printtemp','primaryset'=>$parenttable.'.'.$parenttable.'id','primaryid'=>$primaryid);
		$printdetails['moduleid'] = '1';
		$htmldatacontents='';
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('templates.leadtemplatecontent_editorfilename,templates.templatesid,templates.moduleid,templates.templatesname');
		$this->db->from('templates');
		$this->db->where('templates.templatesid',$templateid);
		$this->db->where("FIND_IN_SET('$industryid',templates.industryid) >", 0);
		$result = $this->db->get();
		foreach($result->result() as $rowdata) {
			$printdetails['contentfile'] =  $rowdata->leadtemplatecontent_editorfilename;
			$printdetails['moduleid'] =  $rowdata->moduleid;
			$printdetails['templatename'] = $rowdata->templatesname;
		}
		$parenttable = $this->Basefunctions->generalinformaion('module','modulemastertable','moduleid',$printdetails['moduleid']);
	
		//body data
		$bodycontent = $this->Printtemplatesmodel->filecontentfetch($printdetails['contentfile']);
		$bodycontent = preg_replace('~a10s~','&nbsp;', $bodycontent);
		$bodycontent = preg_replace('~<br>~','<br />', $bodycontent);
		$bodycontent = $this->Printtemplatesmodel->removespecialchars($bodycontent);
		$bodyhtml = $this->Printtemplatesmodel->generateprintingdata($bodycontent,$parenttable,$print_data,$printdetails['moduleid']);
		$bodyhtml = $this->Printtemplatesmodel->addspecialchars($bodyhtml);
		return $bodyhtml;
	}
	// Date Field Value
	public function datefieldvalue($moduleid,$fieldid,$primaryid) {
		$resultvalue = '';
		$this->db->select('modulefieldid,moduletabid,columnname,tablename,fieldname');
		$this->db->from('modulefield');
		$this->db->where('modulefield.modulefieldid',$fieldid);
		$this->db->limit(1);
		$result = $this->db->get();
		foreach($result->result() as $rowdata) {
			$resultvalue = $this->singlefieldfetch($rowdata->fieldname,$rowdata->tablename.'id',$rowdata->tablename,$primaryid);
		}
		return $resultvalue;
	}
	//work flow email sent
	public function workflowemailsent($modid,$tempid,$tempfile,$parenttable,$id,$emailid,$addemailid) {
		$print_data=array('templateid'=> $tempid, 'Templatetype'=>'Printtemp','primaryset'=>$parenttable.'.'.$parenttable.'id','primaryid'=>$id);
		$htmlcontent = $this->emailgenerateprinthtmlfile($print_data);
		//mail sent
		//fetch sender email id
		$userid=$this->userid;
		$sendermail = $this->generalinformaion('employee','emailid','employeeid',$userid);
		$subject = $this->generalinformaion('emailtemplates','subject','emailtemplatesid',$tempid);
		$filename = $this->getfieldvalue('filename',$tempid);
		$filepath = $this->getfieldvalue('filename_path',$tempid);
		$sendermail = (($sendermail!='0')?$sendermail:'noreply@salesneuron.com');
		$sendername = $this->generalinformaion('employee','employeename','employeeid',$userid);
		$sendername = (($sendername!='0')?$sendername:'Sales Neuron');
		//$this->load->helper('phpmailer');
		$mail = new PHPMailer(true);
		try {
			$mail->IsSMTP();
			$mail->SMTPAuth   = true;
			$mail->SMTPSecure = "tls";
			$mail->Username   = "AKIATQIHVKJXZS2D2452";
			$mail->Password   = "BN2oAjkebotY/R2eR/h2ghepAb7KEuHyhPnqIGl0bjuz";
			$mail->Host       = "email-smtp.us-west-2.amazonaws.com";
			$mail->Port       = 587;
			//$mail->SetFrom($sendermail,$sendername);
			$mail->SetFrom('arvind@aucventures.com','AUC Ventures'); //This line should be commented after giving code to customer
			$mail->Subject   = $subject;
			$mail->IsHTML(true);
			$mail->MsgHTML($htmlcontent);
			if($modid != 86){
				//$emails = ( ($addemailid!='')?$emailid.','.$addemailid :$emailid );
				$emailids = explode(',',$emailid);
			} else {
				$contactid = $this->generalinformaion('invoice','contactid','invoiceid',$id);
				$emails = $this->generalinformaion('contact','emailid','contactid',$contactid);
				$emailids = explode(',',$emails);
			}
			if(!empty($filepath) && !empty($filename)) {
				for($i=0; $i<count($filename); $i++) {
					$mail->AddAttachment($filepath[$i],$filename[$i]);
				}
			}
			foreach($emailids as $email) {
				$mail->AddAddress(trim($email));
				$mail->AltBody="This is text only alternative body.";
			}
			if($addemailid != '') {
				$addemailids = explode(',',$addemailid);
				foreach($addemailids as $additionalemail) {
					$mail->AddBCC(trim($additionalemail));
				}
			}
			$mail->send();
		}	catch (phpmailerException $e) {
			echo $e->errorMessage(); //Pretty error messages from PHPMailer
		} catch (Exception $e) {
			echo $e->getMessage(); //Boring error messages from anything else!
		}
	}
	//get the value of given column name
	public function getfieldvalue($field,$tempid){
		$data= array();
		$this->db->select($field);
		$this->db->from('crmfileinfo');
		$this->db->where_in('moduleid',209);
		$this->db->where('commonid',$tempid);
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data[] = $row->$field;
			}
		}
		return $data;
	}
	//Email template fetch
	public function emailgenerateprinthtmlfile($print_data) {
		$this->load->model('Printtemplates/Printtemplatesmodel');
		//defaults
		$printdetails['moduleid'] = '1';
		$htmldatacontents='';
		$industryid = $this->Basefunctions->industryid;
		$templateid = ( (isset($print_data['templateid']))? $print_data['templateid'] : 1);
		$id = ( (isset($print_data['primaryid']))? $print_data['primaryid'] : 1);
		$this->db->select('emailtemplates.emailtemplatesemailtemplate_editorfilename,emailtemplates.emailtemplatesid,emailtemplates.moduleid,emailtemplates.emailtemplatesname');
		$this->db->from('emailtemplates');
		$this->db->where('emailtemplates.emailtemplatesid',$templateid);
		$this->db->where("FIND_IN_SET('$industryid',emailtemplates.industryid) >", 0);
		$result = $this->db->get();
		foreach($result->result() as $rowdata) {
			$printdetails['contentfile'] =  $rowdata->emailtemplatesemailtemplate_editorfilename;
			$printdetails['moduleid'] =  $rowdata->moduleid;
			$printdetails['templatename'] = $rowdata->emailtemplatesname;
		}
		$parenttable = $this->Basefunctions->generalinformaion('module','modulemastertable','moduleid',$printdetails['moduleid']);
	
		//body data
		$bodycontent = $this->Printtemplatesmodel->filecontentfetch($printdetails['contentfile']);
		$bodycontent = preg_replace('~a10s~','&nbsp;', $bodycontent);
		$bodycontent = preg_replace('~<br>~','<br />', $bodycontent);
		$bodycontent = $this->Printtemplatesmodel->removespecialchars($bodycontent);
		$bodyhtml = $this->Printtemplatesmodel->generateprintingdata($bodycontent,$parenttable,$print_data,$printdetails['moduleid']);
		$bodyhtml = $this->Printtemplatesmodel->addspecialchars($bodyhtml);
		return $bodyhtml;
	}
	//read file
	public function filecontentfetch($filename) {
		$filecontent = "";
		if( file_exists($filename) && is_readable($filename) ) {
			$newfilename = explode('/',$filename);
			$myfile = @fopen($newfilename[0].DIRECTORY_SEPARATOR.$newfilename[1], "r");
			$filecontent = fread($myfile,filesize($newfilename[0].DIRECTORY_SEPARATOR.$newfilename[1]));
			fclose($myfile);
		}
		return $filecontent;
	}
	//where condition generation
    public function workflowrulecriteriacondgen($conditionvalarray) {
		$whereString="";
		$m=0;
		foreach ($conditionvalarray as $key) {
			if($m == 0) {
				$key[3] = "";
				$whereString .= "";
			} else {
				if($key[3] != 'AND' AND $key[3] != 'OR') {
					$key[3] = 'AND';
				}
			}
			$whereString.=$this->workflowconditiongenerate($key);
			if($m==1) {
				$whereString="(".$whereString.")";
			} else if($m>=2) {
				$whereString.=")";
			}
			$m++;
		}
		return $whereString;
    }
	//condition generation
	public function workflowconditiongenerate($key) {
		switch($key[2]) {
			case "Equalto":
				return $whereString=( (is_array($key[0]))? " ".$key[3]." in_array('".$key[0]."',".$key[1].")" : " ".$key[3]." '".$key[0]."' == '".$key[1]."'" );
				break;
			case "NotEqual":
				return $whereString=( (is_array($key[0]))? " ".$key[3]." !in_array('".$key[0]."',".$key[1].")" : " ".$key[3]." '".$key[0]."' != '".$key[1]."'" );
				break;
			case "Startwith":
				return $whereString=( (is_array($key[0]))? " ".$key[3]." in_array('".$key[0]."',".$key[1].")" : " ".$key[3]." '".$key[0]."' LIKE '".$key[1].'%'."'" );
				break;
			case "Endwith":
				return $whereString=( (is_array($key[0]))? " ".$key[3]." in_array('".$key[0]."',".$key[1].")" : " ".$key[3]." '".$key[0]."' LIKE '".'%'.$key[1]."'" );
				break;
			case "Middle":
				return $whereString=( (is_array($key[0]))? " ".$key[3]." in_array('".$key[0]."',".$key[1].")" : " ".$key[3]." '".$key[0]."' LIKE '".'%'.$key[1].'%'."'" );
				break;
			case "IN":
				return $whereString=( (is_array($key[0]))? " ".$key[3]." in_array('".$key[0]."',".$key[1].")" : " ".$key[3]." '".$key[0]."' IN '".$key[1]."'" );
				break;
			case "NOT IN":
				return $whereString=( (is_array($key[0]))? " ".$key[3]." in_array('".$key[0]."',".$key[1].")" : " ".$key[3]." '".$key[0]."' NOT IN '".$key[1]."'" );
				break;
			case "GreaterThan":
				return $whereString=( (is_array($key[0]))? " ".$key[3]." in_array('".$key[0]."',".$key[1].")" : " ".$key[3]." '".$key[0]."' > '" .$key[1]."'" );
				break;
			case "LessThan":
				return $whereString=( (is_array($key[0]))? " ".$key[3]." in_array('".$key[0]."',".$key[1].")" : " ".$key[3]." '".$key[0]."' < '" .$key[1]."'" );
				break;
			case "GreaterThanEqual":
				return $whereString=( (is_array($key[0]))? " ".$key[3]." in_array('".$key[0]."',".$key[1].")" : " ".$key[3]." '".$key[0]."' >= '" .$key[1]."'" );
				break;
			case "LessThanEqual":
				return $whereString=( (is_array($key[0]))? " ".$key[3]." in_array('".$key[0]."',".$key[1].")" : " ".$key[3]." '".$key[0]."' <= '" .$key[1]."'" );
				break;
			default:
				return $whereString="";
				break;
		}
	}
	//sms work flow
	public function workflowrulesmsalertgen($workflowid) {
		$this->db->select('workflowalertadditonalrecipient,workflowmanualalertadditonalrecipient,modulefield.fieldname,modulefield.columnname,modulefield.tablename,modulefield.moduletabid,workflowruleactionalert.workflowtemplateid,templates.leadtemplatecontent_editorfilename,templates.moduleid,smssettings.senderid,smsprovidersettings.apikey,workflowtransactionmode,workflowtransactiontype',false);
		$this->db->from('workflowruleactionalert');
		$this->db->join('templates','templates.templatesid=workflowruleactionalert.workflowtemplateid');
		$this->db->join('smssettings','smssettings.smssettingsid=workflowruleactionalert.workflowsenderid');
		$this->db->join('smsprovidersettings','smsprovidersettings.smsprovidersettingsid=smssettings.smsprovidersettingsid');
		$this->db->join('modulefield','workflowruleactionalert.workflowalertrecipient=modulefield.modulefieldid');
		$this->db->where('workflowruleactionalert.workflowid',$workflowid);
		$this->db->where('workflowruleactionalert.workflowalerttype','sms');
		$this->db->where('workflowruleactionalert.status',1);
		$datas = $this->db->get()->result();
		return $datas;
	}
	//call work flow notification sent
	public function workflowrulecallsalertgen($workflowid) {
		$this->db->select('workflowalertadditonalrecipient,modulefield.fieldname,modulefield.columnname,modulefield.tablename,modulefield.moduletabid,calltemplates.calltemplatecontent_editorfilename,calltemplates.moduleid,callsettings.mobilenumber,callsettings.apikey,workflowtransactionmode,workflowtransactiontype,sounds.soundfileid',false);
		$this->db->from('workflowruleactionalert');
		$this->db->join('calltemplates','calltemplates.calltemplatesid=workflowruleactionalert.workflowtemplateid');
		$this->db->join('callsettings','callsettings.callsettingsid=workflowruleactionalert.workflowsenderid');
		$this->db->join('sounds','sounds.soundsid=workflowruleactionalert.workflowcallsoundid');
		$this->db->join('modulefield','workflowruleactionalert.workflowalertrecipient=modulefield.modulefieldid');
		$this->db->where('workflowruleactionalert.workflowid',$workflowid);
		$this->db->where('workflowruleactionalert.workflowalerttype','call');
		$this->db->where('workflowruleactionalert.status',1);
		$datas = $this->db->get()->result();
		return $datas;
	}
	//desktop work flow concept
	public function workflowruledesktopalertgen($workflowid) {
		$this->db->select('workflowalertadditonalrecipient,modulefield.fieldname,modulefield.columnname,modulefield.tablename,modulefield.moduletabid,templates.leadtemplatecontent_editorfilename,templates.moduleid,workflowtransactionmode,workflowtransactiontype',false);
		$this->db->from('workflowruleactionalert');
		$this->db->join('templates','templates.templatesid=workflowruleactionalert.workflowtemplateid');
		$this->db->join('modulefield','workflowruleactionalert.workflowalertrecipient=modulefield.modulefieldid');
		$this->db->where('workflowruleactionalert.workflowid',$workflowid);
		$this->db->where('workflowruleactionalert.workflowalerttype','desktop');
		$this->db->where('workflowruleactionalert.status',1);
		$datas = $this->db->get()->result();
		return $datas;
	}
	//call sent function
	public function workflowcallnotification($apikey,$play,$mobilenumber,$caller) {
		$xmlurl = "http://obd.solutionsinfini.com/api/v1/?api_key=".$apikey."&method=voice.call&format=xml&play=".$play."&numbers=".$mobilenumber;
		$ch=curl_init();
		curl_setopt($ch, CURLOPT_URL, $xmlurl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$output=curl_exec($ch);
		curl_close($ch);
		$dataset = $this->callparse_response($output);
		for($i=0;$i<count($dataset);$i++) {
			$date= date($this->Basefunctions->datef);
			$userid= $this->Basefunctions->userid;
			$activestatus = $this->Basefunctions->activestatus;
			$campaign = array(
				'callcampaignname'=>'Work Flow Call',
				'callgroupsid'=>1,
				'callsegmentsid'=>1,
				'totalsubscribercount'=>'1',
				'validsubscribercount'=>'1',
				'smssendtypeid'=>'2',
				'callnumbertypeid'=>2,
				'callernumber'=>$caller,
				'mobilenumber'=>$mobilenumber,
				'soundsid'=>1,
				'retriesvalue'=>1,
				'callintervalid'=>1,
				'campaignuniqueid'=>$dataset[$i]['id'],
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
				'createdate'=>$date,
				'lastupdatedate'=>$date,
				'status'=>$activestatus
			);
			$this->db->insert('callcampaign',$campaign);
		}
	}
	//work flow desktop notification insertion
	public function workflowdesktopnotification($type,$moduleid,$recordid,$employeeid,$htmlcontent,$addusers) {
		$notifyempids = array();
		$addusers[] = $employeeid;
		foreach($addusers as $employeeids) {
			if(is_array($employeeids)) {
				foreach($employeeids as $empid) {
					if(!in_array($empid,$notifyempids) && $empid!='1') {
						array_push($notifyempids,$empid);
						$notcdata=array(
							'notificationlogtypeid'=>$type,
							'moduleid'=>$moduleid,
							'commonid'=>$recordid,
							'employeeid'=>$empid,
							'notificationmessage'=>$htmlcontent,
							'createdate'=>date($this->Basefunctions->datef),
							'lastupdatedate'=>date($this->Basefunctions->datef),
							'createuserid'=>$this->Basefunctions->userid,
							'lastupdateuserid'=>$this->Basefunctions->userid,
							'status'=>2,
							'flag'=>1
						);
						$this->db->insert('notificationlog',$notcdata);
					}
				}
			} else {
				if(!in_array($employeeids,$notifyempids) && $employeeids!='1') {
					array_push($notifyempids,$employeeids);
					$notcdata=array(
							'notificationlogtypeid'=>$type,
							'moduleid'=>$moduleid,
							'commonid'=>$recordid,
							'employeeid'=>$employeeids,
							'notificationmessage'=>$htmlcontent,
							'createdate'=>date($this->Basefunctions->datef),
							'lastupdatedate'=>date($this->Basefunctions->datef),
							'createuserid'=>$this->Basefunctions->userid,
							'lastupdateuserid'=>$this->Basefunctions->userid,
							'status'=>2,
							'flag'=>1
						);
					$this->db->insert('notificationlog',$notcdata);
				}
			}
		}
	}
	//work flow alert field information fetch
	public function workflowtaskfieldinformation($modfieldid) {
		$this->db->select('moduletabid,columnname,fieldname,tablename,uitypeid,modulemastertable');
		$this->db->from('modulefield');
		$this->db->join('module','module.moduleid=modulefield.moduletabid');
		$this->db->where('modulefieldid',$modfieldid);
		$datas = $this->db->get()->result();
		return $datas;
	}
	//send task notification mail
	public function workflowtasknotificationmailsent($taskname,$employeeid,$send_at) {
		$userid=$this->userid;
		$sendermail = $this->generalinformaion('employee','emailid','employeeid',$userid);
		$sendermail = (($sendermail!='0')?$sendermail:'noreply@salesneuron.com');
		$sendername = $this->generalinformaion('employee','employeename','employeeid',$userid);
		$sendername = (($sendername!='0')?$sendername:'Sales Neuron');
		$tomailid = $this->generalinformaion('employee','emailid','employeeid',$employeeid);
		//$recipient_emails = $tomailid; // customer emailid
		$recipient_emails = 'kumaresan@aucventures.com';
		$mail = new PHPMailer(true);
		try {
			$mail->IsSMTP();
			$mail->SMTPAuth   = true;
			$mail->SMTPSecure = "tls";
			$mail->Username   = "AKIATQIHVKJXZS2D2452";
			$mail->Password   = "BN2oAjkebotY/R2eR/h2ghepAb7KEuHyhPnqIGl0bjuz";
			$mail->Host       = "email-smtp.us-west-2.amazonaws.com";
			$mail->Port       = 587;
			//$mail->SetFrom($sendermail,$sendername);
			$mail->SetFrom('arvind@aucventures.com','AUC Ventures'); //This line should be commented after giving code to customer
			$mail->Subject   = $taskname;
			$mail->IsHTML(true);
			$mail->MsgHTML('New Task has assigned to you');
			$mail->AddAddress(trim($recipient_emails));
			$mail->AltBody="This is text only alternative body.";
			$mail->send();
		}	catch (phpmailerException $e) {
			echo $e->errorMessage(); //Pretty error messages from PHPMailer
		} catch (Exception $e) {
			echo $e->getMessage(); //Boring error messages from anything else!
		}
	}
	//fetch xml content
	public function callparse_response($response) {
		$xml = new SimpleXMLElement($response);
		$result = array();
		$h=0;
		foreach ($xml->data as $element) {
		     foreach($element as $key => $val) {
			 $result[$h][(string)$key] = (string)$val;
		     }
		     $h++;
		}
		return $result;
	}
	//notification template fetch based on modules
	public function notificationtemplatedatafetch($moduleid,$primaryid,$parenttable,$type) {
		$tempfile = $this->notificationtemplatenamefetch($moduleid,$type);
		if($tempfile != '') {
			$htmlcontent = $this->filecontentfetch($tempfile);
		} else {
			$htmlcontent = '';
		}
		return $htmlcontent;
	}
	//notification template file name fetch
	public function notificationtemplatenamefetch($moduleid,$type) {
		$industryid = $this->industryid;
		$this->db->select('leadtemplatecontent_editorfilename');
		$this->db->from('templates');
		$this->db->where('templates.moduleid',$moduleid);
		$this->db->where('templates.setdefault','Yes');
		$this->db->where('templates.notificationlogtypeid',$type);
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->where('templates.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = $row->leadtemplatecontent_editorfilename;
			}
		} else {
			$data = '';
		}
		return $data;
	}
	/****************************
	  Work flow Management End
	****************************/
	/* check demo data clear details */
	public function checkdemodatadetails() {
		$compid = 1;
		$empid = $this->Basefunctions->userid;
		$compquery = $this->db->query('select company.companyid,company.mastercompanyid from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid where employeeid='.$empid.'')->result();
		foreach($compquery as $key) {
			$compid = $key->companyid;
		}
		//get demo data details
		$demodatas = array('demodata'=>'Yes','planup'=>'No');
		$demodataset = $this->db->select('generalsettingid,demodata,planupgrade')->from('generalsetting')->where('companyid',$compid)->get();
		foreach($demodataset->result() as $datas) {
			$demodatas['demodata'] = $datas->demodata;
			$demodatas['planup'] = $datas->planupgrade;
		}
		return $demodatas;
	}
	/* Subscripe user information */
	public function subcribeuserinfo() {
		$tzone = $this->Basefunctions->employeetimezoneset($this->Basefunctions->logemployeeid);
		$datasets = array('name'=>'','email'=>'','userid'=>'','zone'=>'');
		$dataset=$this->db->select('employeeid,employeename,lastname,emailid,salutation.salutationname')->from('employee')->join('salutation','salutation.salutationid=employee.salutationid')->where('employee.employeeid',$this->Basefunctions->logemployeeid)->get();
		foreach($dataset->result() as $row) {
			$name=$row->salutationname.$row->employeename.$row->lastname;
			$email=$row->emailid;
			$empid = $row->employeeid;
			$datasets = array('name'=>$name,'email'=>$email,'userid'=>$empid,'zone'=>$tzone['name']);
		}
		return $datasets;
	}
	/* Subscription currecny */
	public function subscribecurrency()	{
		$cid = 120;
		$curinfo = array('currid'=>$cid,'cursymbol'=>'$','code'=>'USD');
		$empid = $this->Basefunctions->userid;
		$compquery = $this->db->query('select company.companyid,company.currencyid,currency.symbol,currency.currencycode from company join branch ON branch.companyid=company.companyid join employee ON employee.branchid=branch.branchid join currency ON currency.currencyid=company.currencyid where employeeid='.$empid.'')->result();
		foreach($compquery as $key) {
			$curinfo = array('currid'=>$key->currencyid,'cursymbol'=>$key->symbol,'code'=>$key->currencycode);
		}
		return $curinfo;
	}
	//fetch support overlay update information
	public function fetchupdatedatasetmodel() {
		$updatedata = array();
		$masdbname = $this->db->masterdb;
		$userdb = $this->db->database;
		$masdb = $this->inlineuserdatabaseactive($masdbname);
		$i=0;
		$udata=$masdb->select('supportupdateset.supportupdatesetid,supportupdateset.updatecontent')->from('supportupdateset')->where('supportupdateset.status',1)->get();
		foreach($udata->result() as $datas) {
			$updatedata[$i] = $datas->updatecontent;
			$i++;
		}
		$this->inlineuserdatabaseactive($userdb);
		//echo $cdb;die();
		echo json_encode($updatedata);
	} 
	//check email
	public function checkvalidemailanddonotmail() {
		$id=trim($_GET['id']);
		$table=trim($_GET['table']);
		$primaryid=$table.'id';
		$field=trim($_GET['field']);
		$donotmail=trim($_GET['donotmail']);
		//for valid mail
		$data=$this->db->select($field)->from($table)->where($primaryid,$id)->get()->result();
		foreach($data as $info) {
			$email=$info->$field;
		}
		if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$a=true;
		} else {
			$a=false;
		}
		//for don not mail
		$maildata=$this->db->select($donotmail)->from($table)->where($primaryid,$id)->get()->result();
		foreach($maildata as $minfo) {
			$mdonotemail=$minfo->$donotmail;
		}
		echo $a.','.$mdonotemail;
	}
	//check email
	public function validatecall() {
		$id=trim($_GET['id']);
		$table=trim($_GET['table']);
		$primaryid=$table.'id';
		$field=trim($_GET['field']);
		$data=$this->db->select($field)->from($table)->where($primaryid,$id)->get()->result();
		foreach($data as $info) {
			$call=$info->$field;
		}
		echo $call;
	}
	/* Vardaan Integration start */
	/* Checking the page previllage based on their role and Plan */
	public function pageaccesschecker($moduleid) { 
	 	$userroleid=$this->session->userdata('UserRoleId')['UserRoleId'];
		$userplanid=$this->session->userdata('planids')['plandataid'];
		$roleaccess=$this->db->select('COUNT(moduleinfoid) as isrolevalid')
						->from('moduleinfo')
						->where('moduleid',$moduleid)
						->where('userroleid',$userroleid)
						->where('moduleinfo.status',$this->Basefunctions->activestatus)
						->get()->row();
		$planaccess=$this->db->select('COUNT(planinfoid) as isplanvalid')
						->from('planinfo')
						->where_in('moduleid',$moduleid)
						->where('planinfoid',$userplanid)
						->where('status',$this->Basefunctions->activestatus)
						->get()->row();
		$pageaccess=array(
			'rolevalid'=>$roleaccess->isrolevalid,
			'planvalid'=>$planaccess->isplanvalid
		);
		return $pageaccess;
	}
	/* ** Access the company setting value of given data set */
	public function get_company_settings($value) {
		$info = $this->db->select($value)
							->from('companysetting')
							->limit(1)
							->get()
							->row();
		return $info->$value;
	}
	/* ** Access Today's Gold Rate value */
	public function get_todaygoldratevalue($value,$purityid) {
		$resultdata = 0;
		$data = $this->db->query(
						"SELECT CASE WHEN $value > 0 THEN $value ELSE 0 END as $value
						 FROM rate
						 WHERE purityid = $purityid
						 ORDER BY purityid desc" 
						);
		foreach($data->result_array() AS $row) {
			$resultdata = $row[$value];
		}
		if($resultdata) {
			$resultdata = $resultdata;
		} else {
			$resultdata = 0;
		}
		return $resultdata;
	}
	/* ** Access Today's Silver Rate value */
	public function get_todaysilverratevalue($value,$purityid) {
		$resultdata = 0;
		$data = $this->db->query(
						"SELECT CASE WHEN $value > 0 THEN $value ELSE 0 END as $value
						 FROM rate
						 WHERE purityid = $purityid
						 ORDER BY purityid desc" 
						);
		foreach($data->result_array() AS $row) {
			$resultdata = $row[$value];
		}
		if($resultdata) {
			$resultdata = $resultdata;
		} else {
			$resultdata = 0;
		}
		return $resultdata;
	}
	/**
	*	purity_groupdropdown
	*/
	public function purity_groupdropdown() {
		$data = $this->db->query(
						"SELECT metal.metalid,metal.metalname,metal.purity_id,purity.purityname,purity.purityid,purity.melting,purity.purityrateid
						 FROM purity
						 JOIN metal ON metal.metalid = purity.metalid
						 WHERE purity.status = 1
						 ORDER BY purity.metalid asc" 
						);
		return $data;
	}
	// Primary Purity Details
	public function primarypurity_groupdropdown() {
		$data = $this->db->query(
						"SELECT metal.metalid,metal.metalname,metal.purity_id,purity.purityname,purity.purityid,purity.melting,purity.purityrateid
						 FROM purity
						 JOIN metal ON metal.metalid = purity.metalid
						 WHERE purity.status = 1 AND primaryid = 1
						 ORDER BY purity.metalid asc" 
						);
		return $data;
	}
	// Non Primary Purity Details
	public function nonprimarypurity_groupdropdown() {
		$data = $this->db->query(
						"SELECT metal.metalid,metal.metalname,metal.purity_id,purity.purityname,purity.purityid,purity.melting,purity.purityrateid
						 FROM purity
						 JOIN metal ON metal.metalid = purity.metalid
						 WHERE purity.status = 1 AND primaryid = 0
						 ORDER BY purity.metalid asc" 
						);
		return $data;
	}
	/*	charge_groupdropdown */
	public function charge_groupdropdown() {
		$data = $this->db->query(
						"SELECT charge.chargeid,charge.chargename,chargecategory.chargecategoryid,chargecategory.chargecategoryname
						 FROM charge
						 JOIN chargecategory ON chargecategory.chargecategoryid = charge.chargecategoryid
						 WHERE charge.status = 1
						 ORDER BY charge.chargecategoryid asc" 
						);
		return $data;
	}
	/*	default_charge_groupdropdown */
	public function default_charge_groupdropdown() {
		$data = $this->db->query(
				"SELECT charge.chargeid,charge.chargename,chargecategory.chargecategoryid,chargecategory.chargecategoryname
						 FROM charge
						 JOIN chargecategory ON chargecategory.chargecategoryid = charge.chargecategoryid
						 WHERE charge.status = 1 and charge.chargecategoryid in(2,3)
						 ORDER BY charge.chargecategoryid asc"
				);
		return $data;
	}
	/*	purchasecharge_groupdropdown */
	public function purchasecharge_groupdropdown() {
		$data = $this->db->query(
				"SELECT purchasecharge.purchasechargeid,purchasecharge.purchasechargename,chargecategory.chargecategoryid,chargecategory.chargecategoryname
						 FROM purchasecharge
						 JOIN chargecategory ON chargecategory.chargecategoryid = purchasecharge.chargecategoryid
						 WHERE purchasecharge.status = 1
						 ORDER BY purchasecharge.chargecategoryid asc"
				);
		return $data;
	}
	/* vardaan serial number generate	*/
	public function salesvarrandomnumbergenerator($moduleid,$fieldname,$fieldtype,$serialnumbermastername,$salesdate) {
		$num = "";
		$maxnumber='';
		$currentnumber="";
		if($moduleid == 52 && $serialnumbermastername != ''){
			$this->db->select('suffix,startingnumber,digitslength,incrementby,serialnumbertypeid,prefix,table,methodtype,methodtypefield,currentnumber,intialnumber,estimationnumbermodeid,startdate,enddate,varserialnumbermasterid');
			$this->db->from('varserialnumbermaster');
			$this->db->where('moduleid',$moduleid);
			$this->db->where('startdate <=',$salesdate);
			$this->db->where('enddate >=',$salesdate);
			$this->db->where('methodtype',$fieldtype);
			$this->db->where('serialnumbermastername',strtolower($serialnumbermastername));
			$this->db->where('status',1);
			$this->db->order_by('varserialnumbermasterid',"asc");
			$this->db->limit(1);
			
		}else{
			$this->db->select('suffix,startingnumber,digitslength,incrementby,serialnumbertypeid,prefix,table,methodtype,methodtypefield,currentnumber,intialnumber,estimationnumbermodeid,startdate,enddate,varserialnumbermasterid');
			$this->db->from('varserialnumbermaster');
			$this->db->where('moduleid',$moduleid);
			if($fieldtype > 1) {
				$this->db->where('methodtype',$fieldtype);
			}
			$this->db->where('status',1);
			$this->db->order_by('varserialnumbermasterid',"asc");
			$this->db->limit(1);
		}
		$datam = $this->db->get();
		if($datam->num_rows() > 0) {//get the data based on given random
			foreach($datam->result() as $info) {
				$suffix = $info->suffix;
				$prefix = $info->prefix;	
				$varserialnumbermasterid = $info->varserialnumbermasterid;
				$startnumber = $info->startingnumber;
				$currentnumber = $info->currentnumber;
				$intialnumber = $info->intialnumber;
				$digitslength = $info->digitslength;
				$incrementby = $info->incrementby;
				$random = $info->serialnumbertypeid;
				$table = $info->table;
				$tableid = $table.'id';
				$fieldname = $fieldname;		
				$methodtype = $info->methodtype;		
				$methodtypefield = $info->methodtypefield;	
				$estimationnumbermodeid = $info->estimationnumbermodeid;
				if($random == 3) // serial number
				{ 
					if($currentnumber == 0){ //first time
						$currentnumber = (int)$startnumber;
					}else {
						$currentnumber = (int)$currentnumber+1;
					}
					$currentnumber=str_pad($currentnumber,$digitslength, '0', STR_PAD_LEFT);
					$num=$prefix.$currentnumber.$suffix;
					$current_number=array('currentnumber'=>$currentnumber);
					$this->db->where('varserialnumbermasterid',$varserialnumbermasterid);
					$this->db->update('varserialnumbermaster',$current_number);
					if($moduleid == 52 && $serialnumbermastername != ''){
						return array(trim($num),$currentnumber);
					} else {
						return trim($num);
					}
				} 
				else if($random == 2) //randomcall
				{
					$num=$this->fetchrandomnumber($fieldname,$table,$tableid,$suffix,$prefix);
					if($moduleid == 52) {
						return array(trim($num),trim($num));
					} else {
						return trim($num);
					}
				}
			}			
		}
		else //if there is not data based on given type then get generalproperty
		{ 
			$random = 2;
			$uid=time()+mt_rand(1,99999999);
			if($moduleid == 52) {
				return array(trim($uid),trim($uid));
			} else {
				return trim($uid);
			}
		}
	}
	
	/* vardaan serial number generate without using transaction type id - Instead of using transactionmanageid with serialnumbermaster	*/
	public function serialnumbergeneratevardaan($moduleid,$fieldname,$fieldtype,$serialnumbermastername,$salesdate) {
		$num = "";
		$maxnumber='';
		$currentnumber="";
		if($moduleid == 52 && $serialnumbermastername != ''){
			$this->db->select('suffix,startingnumber,digitslength,incrementby,serialnumbertypeid,prefix,table,methodtype,methodtypefield,currentnumber,intialnumber,estimationnumbermodeid,startdate,enddate,varserialnumbermasterid');
			$this->db->from('varserialnumbermaster');
			$this->db->where('moduleid',$moduleid);
			$this->db->where('startdate <=',$salesdate);
			$this->db->where('enddate >=',$salesdate);
			/* $this->db->where('methodtype',$fieldtype); */
			$this->db->where('serialnumbermastername',strtolower($serialnumbermastername));
			$this->db->where('status',1);
			$this->db->order_by('varserialnumbermasterid',"asc");
			$this->db->limit(1);
			
		}else{
			$this->db->select('suffix,startingnumber,digitslength,incrementby,serialnumbertypeid,prefix,table,methodtype,methodtypefield,currentnumber,intialnumber,estimationnumbermodeid,startdate,enddate,varserialnumbermasterid');
			$this->db->from('varserialnumbermaster');
			$this->db->where('moduleid',$moduleid);
			/* if($fieldtype > 1) {
				$this->db->where('methodtype',$fieldtype);
			} */
			$this->db->where('status',1);
			$this->db->order_by('varserialnumbermasterid',"asc");
			$this->db->limit(1);
		}
		$datam = $this->db->get();
		if($datam->num_rows() > 0) {//get the data based on given random
			foreach($datam->result() as $info) {
				$suffix = $info->suffix;
				$prefix = $info->prefix;	
				$varserialnumbermasterid = $info->varserialnumbermasterid;
				$startnumber = $info->startingnumber;
				$currentnumber = $info->currentnumber;
				$intialnumber = $info->intialnumber;
				$digitslength = $info->digitslength;
				$incrementby = $info->incrementby;
				$random = $info->serialnumbertypeid;
				$table = $info->table;
				$tableid = $table.'id';
				$fieldname = $fieldname;		
				$methodtype = $info->methodtype;		
				$methodtypefield = $info->methodtypefield;	
				$estimationnumbermodeid = $info->estimationnumbermodeid;
				if($random == 3) // serial number
				{ 
					if($currentnumber == 0){ //first time
						$currentnumber = (int)$startnumber;
					}else {
						$currentnumber = (int)$currentnumber+1;
					}
					$currentnumber=str_pad($currentnumber,$digitslength, '0', STR_PAD_LEFT);
					$num=$prefix.$currentnumber.$suffix;
					$current_number=array('currentnumber'=>$currentnumber);
					$this->db->where('varserialnumbermasterid',$varserialnumbermasterid);
					$this->db->update('varserialnumbermaster',$current_number);
					if($moduleid == 52 && $serialnumbermastername != ''){
						return array(trim($num),$currentnumber);
					} else {
						return trim($num);
					}
				} 
				else if($random == 2) //randomcall
				{
					$num=$this->fetchrandomnumber($fieldname,$table,$tableid,$suffix,$prefix);
					if($moduleid == 52) {
						return array(trim($num),trim($num));
					} else {
						return trim($num);
					}
				}
			}			
		}
		else //if there is not data based on given type then get generalproperty
		{ 
			$random = 2;
			$uid=time()+mt_rand(1,99999999);
			if($moduleid == 52) {
				return array(trim($uid),trim($uid));
			} else {
				return trim($uid);
			}
		}
	}
	
	/* vardaan serial number generate	*/
	public function varrandomnumbergenerator($moduleid,$fieldname,$fieldtype,$transactionmodeid) {
		$num = "";
		$maxnumber='';
		$currentnumber="";
		$this->db->select('varserialnumbermasterid,suffix,startingnumber,digitslength,incrementby,serialnumbertypeid,prefix,table,methodtype,methodtypefield,currentnumber,intialnumber,estimationnumbermodeid');
		$this->db->from('varserialnumbermaster');
		$this->db->where('moduleid',$moduleid);
		$this->db->where('fieldname',$fieldname);
		if($fieldtype > 1) {
			$this->db->where('methodtype',$fieldtype);
		}
		$this->db->where('status',1);
		$this->db->limit(1);
		$datam = $this->db->get();
		
		if($datam->num_rows() > 0) {//get the data based on given random
			foreach($datam->result() as $info) {
				$suffix = $info->suffix;
				$prefix = $info->prefix;
				$startnumber = $info->startingnumber;
				$currentnumber = $info->currentnumber;
				$intialnumber = $info->intialnumber;
				$digitslength = $info->digitslength;
				$incrementby = $info->incrementby;
				$random = $info->serialnumbertypeid;
				$table = $info->table;
				$tableid = $table.'id';
				$fieldname = $fieldname;		
				$methodtype = $info->methodtype;		
				$methodtypefield = $info->methodtypefield;	
				$estimationnumbermodeid = $info->estimationnumbermodeid;
				$varserialnumbermasterid = $info->varserialnumbermasterid;
			}			
		}
		else //if there is not data based on given type then get generalproperty
		{ 
			$this->db->select('varserialnumbermasterid,suffix,startingnumber,digitslength,incrementby,serialnumbertypeid,prefix,table,methodtype,methodtypefield,currentnumber,intialnumber,estimationnumbermodeid');
			$this->db->from('varserialnumbermaster');
			$this->db->where('moduleid',$moduleid);
			$this->db->where('fieldname',$fieldname);
			$this->db->where('methodtype',1);
			$this->db->where('status',1);
			$this->db->limit(1);
			$datam = $this->db->get();
			if($datam->num_rows() > 0)
			{
				foreach($datam->result() as $info)
				{
					$suffix = $info->suffix;
					$prefix = $info->prefix;
					$startnumber = $info->startingnumber;
					$currentnumber = $info->currentnumber;
					$intialnumber = $info->intialnumber;
					$digitslength = $info->digitslength;
					$incrementby = $info->incrementby;
					$random = $info->serialnumbertypeid;
					$table = $info->table;
					$tableid = $table.'id';
					$fieldname = $fieldname;		
					$methodtype = 1;		
					$methodtypefield = '';
					$estimationnumbermodeid = $info->estimationnumbermodeid;
					$varserialnumbermasterid = $info->varserialnumbermasterid;
				}
			}
			else{
				$random = '';
			}
		}
	
		/* if($prefix == "")
		{
			$prefix = " ";
		}		
		if($random == 3) // serial number
		{ 
			if($prefix == " ") //strict max number
			{
				$this->db->select('MAX(CAST( '.$fieldname.' AS UNSIGNED )) as '.$fieldname.'',false);
				$this->db->from($table);
				if($methodtype > 1){
					if($methodtype == 2 || $methodtype == 3) {
						$this->db->where($methodtypefield,11);
					}else{
						$this->db->where($methodtypefield,$methodtype);	
					}
				}
				$this->db->where_not_in('status',3);
				$this->db->limit(1);
				$datatow = $this->db->get();			
			}
			else
			{				
				$this->db->select_max($fieldname);
				$this->db->from($table);
				if($methodtype > 1){
					if($methodtype == 2 || $methodtype == 3) {
						$this->db->where($methodtypefield,11);
					}else{
						$this->db->where($methodtypefield,$methodtype);	
					}
				}
				$this->db->where_not_in('status',3);
				$this->db->like($fieldname,$prefix, 'both'); 
				$this->db->limit(1,0);
				$datatow = $this->db->get();
			}			
			foreach($datatow->result() as $maxrow){
				$maxnumber = $maxrow->$fieldname;
			}
			//checks whether records exits and not null
			if($intialnumber != 0)
			{
				$startnumber=str_pad($startnumber,$digitslength, '0', STR_PAD_LEFT);
				$num=$prefix.$startnumber.$suffix;
				$current_number=array('intialnumber'=>0);
				$this->db->where('moduleid',$moduleid)->where('methodtype',$methodtype);
				$this->db->update('varserialnumbermaster',$current_number);
				return trim($num);
			}else {
					if($datatow->num_rows() > 0 and $maxnumber != null) { 
						
						foreach($datatow->result() as $next) {
							$datanum = trim($next->$fieldname);
							if($datanum < $startnumber) {
								$num=$prefix.$startnumber.$suffix;
								$this->currentformatnumber($startnumber,$moduleid,$methodtype);
								return trim($num);
							} 
							else {
								if($suffix == "") {						
								//$prenumber = explode($prefix,$datanum);
								if($incrementby == '0') {
										if( $currentnumber < 2) {
											$prenum = (int)$currentnumber+1;
											$prenum=str_pad($prenum,$digitslength, '0', STR_PAD_LEFT);
											$num = $prefix.$prenum;
											$this->currentformatnumber($prenum,$moduleid,$methodtype);
											return trim($num);
										} else {
											$prenum = (int)$currentnumber+1;
											$prenum=str_pad($prenum,$digitslength, '0', STR_PAD_LEFT);
											$num = $prefix.$prenum;
											$this->currentformatnumber($prenum,$moduleid,$methodtype);
											return trim($num);
										}
								} else { 
										if( $currentnumber < 2) {
											$prenum = (int)$currentnumber+$incrementby;
											$prenum=str_pad($prenum,$digitslength, '0', STR_PAD_LEFT);
											$num = $prefix.$prenum;
											$this->currentformatnumber($prenum,$moduleid,$methodtype);
											return trim($num);
										} else {
											$prenum = (int)$currentnumber+$incrementby;
											$prenum=str_pad($prenum,$digitslength, '0', STR_PAD_LEFT);
											$num = $prefix.$prenum;
											$this->currentformatnumber($prenum,$moduleid,$methodtype);
											return trim($num);
										}
								}
								}
								else {
									$data = trim($next->$fieldname);
									$number = explode($suffix,$data);
									$prenumber = explode($prefix,$number[0]);
									if($incrementby == 0) {
										if( $currentnumber < 2) {
											$prenum = $currentnumber+1;
											$prenum=str_pad($prenum,$digitslength, '0', STR_PAD_LEFT);
											$num = $prefix.$prenum.$suffix;
											$this->currentformatnumber($prenum,$moduleid,$methodtype);
											return trim($num);
										} else {
											$prenum = (int)$currentnumber+1;
											$prenum=str_pad($prenum,$digitslength, '0', STR_PAD_LEFT);
											$num = $prefix.$prenum.$suffix;
											$this->currentformatnumber($prenum,$moduleid,$methodtype);
											return trim($num);
										}
									} else {
										if($currentnumber < 2) {
											$num = (int)$currentnumber+$incrementby;
											$num=str_pad($num,$digitslength, '0', STR_PAD_LEFT); 
											$this->currentformatnumber($num,$moduleid,$methodtype);
											return trim($prefix.$num.$suffix);
										} else {
											$prenum = (int)$currentnumber+$incrementby;
											$prenum=str_pad($prenum,$digitslength, '0', STR_PAD_LEFT); 
											$num = $prefix.$prenum.$suffix;
											$this->currentformatnumber($prenum,$moduleid,$methodtype);
											return trim($num);
										}
									}
								}
								
							}
						}
					} 
					else {			
						$startnumber=str_pad($startnumber,$digitslength, '0', STR_PAD_LEFT);
						$num=$prefix.$startnumber.$suffix;
						$this->currentformatnumber($startnumber,$moduleid,$methodtype);
						return trim($num);
					}		
			}
		} 
		
		 */
		if($random == 3) // serial number
		{ 
			if($currentnumber == 0){ //first time
				$currentnumber = (int)$startnumber;
			}else {
				$currentnumber = ((int)$currentnumber)+1;
			}
			$currentnumber=str_pad($currentnumber,$digitslength, '0', STR_PAD_LEFT);
			$num=$prefix.$currentnumber.$suffix;
			$current_number=array('currentnumber'=>$currentnumber);
			$this->db->where('varserialnumbermasterid',$varserialnumbermasterid);
			$this->db->update('varserialnumbermaster',$current_number);
			return trim($num);
		}
		else if($random == 2) //randomcall
		{
			$num=$this->fetchrandomnumber($fieldname,$table,$tableid,$suffix,$prefix);
			return trim($num);
			
		}
		else {
			$uid=time()+mt_rand(1,99999999);
			return trim($uid);
		}
	}
	//current format number
	function currentformatnumber($num,$moduleid,$methodtype){ //update current number in serial number master
		$current_number=array('currentnumber'=>$num);
		$this->db->where('moduleid',$moduleid)->where('methodtype',$methodtype);
		$this->db->update('varserialnumbermaster',$current_number);
	}
	/*	Convert dd/mm/yy TO yy/mm/dd for mysql-date fields	*/
	function ymd_format($date) {
		$final = '';
		if($date != '') {
			$final = implode("-", array_reverse(explode("-", $date)));
		}		
		return $final;
	}	
	/**
	*return the data of a single field/single record 
	*/
	public function singlefieldfetch($field,$primaryfield,$table,$primaryid) {
		$this->db->select($field);
		$this->db->from($table);
		$this->db->where($primaryfield,$primaryid);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$this->db->limit(1);
		$qryinfo = $this->db->get();		
		if($qryinfo->num_rows()>0){
		$info = $qryinfo->row();
		return $info->$field;
		}else{
		return 1;
		}
	}
	/**
	*return the data of a single field/single record  with out active status
	*/
	public function singlefieldfetchinactive($field,$primaryfield,$table,$primaryid) {
		$this->db->select($field);
		$this->db->from($table);
		$this->db->where($primaryfield,$primaryid);
		$this->db->limit(1);
		$qryinfo = $this->db->get();
		if($qryinfo->num_rows()>0){
			$info = $qryinfo->row();
			return $info->$field;
		}else{
			return 1;
		}
	}
	// return the data of a multiple field/single record  with out active status
	public function multiplefieldfetchinactive($field,$primaryfield,$table,$primaryid) {
		$data = '';
		$pdata = explode(',',$primaryid);
		for($i=0;$i<count($pdata);$i++){
			$this->db->select($field);
			$this->db->from($table);
			$this->db->where($primaryfield,$pdata[$i]);
			$this->db->limit(1);
			$qryinfo = $this->db->get();
			if($qryinfo->num_rows()>0){
				$info = $qryinfo->row();
				$data .= $info->$field;
			}
		}
		return $data;
	}
	//inactive_log
	public function inactive_log(){
		$inactive_log=array('lastupdateuserid'=>$this->userid,					 
					       'lastupdatedate'=>date($this->datef),
						   'status'=>$this->closestatus);
		return $inactive_log;
	}
	//delete_log
	public function delete_log(){
		$delete_log=array('lastupdateuserid'=>$this->userid,					 
					       'lastupdatedate'=>date($this->datef),
						   'status'=>$this->deletestatus);
		return $delete_log;
	}
	//cancel_log
	public function cancel_log(){
		$delete_log=array('lastupdateuserid'=>$this->userid,
				'lastupdatedate'=>date($this->datef),
				'status'=>$this->cancelstatus);
		return $delete_log;
	}
	//simple drop down lookup value fetch
	public function simpledropdownlookup($tablename,$fieldname,$order,$chitlookupgroup) {		
		$industryid = $this->industryid;
		$this->db->select($fieldname);
		$this->db->from($tablename);
		$this->db->where('status',1);
		$this->db->where('chittypegroup',$chitlookupgroup);
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->order_by($order,"asc");
		$query= $this->db->get();
        return $query->result();
	}
	//dynamic drop down generation value fetch for two table
    public function dynamicgroupdropdownmultiple($moduleid,$tablename1,$tablename2) {
		//$userroleid = $this->Basefunctions->userroleid; 
        $module ='moduleid';
		$fieldprimaryid = $tablename1.'id';
		$parentprimaryid = $tablename2.'id';
		$parentprimaryname = $tablename2.'name';
		$fieldprimaryname = $tablename1.'name';
		$query=$this->db->query('(select 1 as templatetype,'.$fieldprimaryname.' as name,'.$fieldprimaryid.'  as id from '.$tablename1.' where status = 1 and '.$module.'='.$moduleid.') union all (select 2 as templatetype,'.$parentprimaryname.' as name, '.$parentprimaryid.' from '.$tablename2.' where status = 1 and '.$module.'='.$moduleid.');');
        return $query->result();
    }
	/*
	Checking Single level mapping table values.
	*/
	public function singlelevelmapping($level,$deleteid,$tablename,$fieldid) {		
		switch ($level) {
		case 1:
			$this->db->select("COUNT(".$tablename."id) as val");
			$this->db->from($tablename);
			$this->db->where_in($fieldid,$deleteid);
			$this->db->where('status',1);
			$query= $this->db->get()->row();
			echo $query->val;exit;
			break;
				
		}
	}
	/*
	 Checking Multi level mapping table values.
	 */
	public function multilevelmapping($level,$deleteid,$tablename,$fieldid,$tablename1,$fieldid1,$tablename2,$fieldid2) {		
		switch ($level) {
			case 1:
				$this->db->select("COUNT(".$tablename."id) as val");
				$this->db->from($tablename);
				$this->db->where_in($fieldid,$deleteid);
				$this->db->where('status',1);
				$query= $this->db->get()->row();
				echo $query->val;
				break;
			case 2:
				$this->db->select("COUNT(".$tablename."id) as val");
				$this->db->from($tablename);
				$this->db->where_in($fieldid,$deleteid);
				$this->db->where('status',1);
				$query= $this->db->get()->row();
				if($query->val > 0){
					echo $query->val;
				} else {
					$this->db->select("COUNT(".$tablename1."id) as val");
					$this->db->from($tablename1);
					$this->db->where_in($fieldid1,$deleteid);
					$this->db->where('status',1);
					$query1= $this->db->get()->row();
					echo $query1->val;
				}
				break;
			case 3:
				$this->db->select("COUNT(".$tablename."id) as val");
				$this->db->from($tablename);
				$this->db->where_in($fieldid,$deleteid);
				$this->db->where('status',1);
				$query= $this->db->get()->row();
				if($query->val > 0) {
					echo $query->val;
				} else {
					$this->db->select("COUNT(".$tablename1."id) as val");
					$this->db->from($tablename1);
					$this->db->where_in($fieldid1,$deleteid);
					$this->db->where('status',1);
					$query1= $this->db->get()->row();
					if($query1->val){
						echo $query1->val;
					} else{
						$this->db->select("COUNT(".$tablename2."id) as val");
						$this->db->from($tablename2);
						$this->db->where_in($fieldid2,$deleteid);
						$this->db->where('status',1);
						$query2= $this->db->get()->row();
						echo $query2->val;
					}
				}
				break;
		}
	}
	/**
	*scheme name amount retrieve
	*/
	public function schemeamountretrive() {
		$id=$_GET['primaryid'];
		$this->db->select('chitscheme.chitamounts,chitscheme.noofmonths,chitscheme.baseamount,chitscheme.luckydraw,chitscheme.gift,chitscheme.variablemodeid');
		$this->db->from('chitscheme');
		$this->db->where('chitscheme.chitschemeid',$id);
		$this->db->where('chitscheme.status',$this->Basefunctions->activestatus);
		$info=$this->db->get()->row();	        
		$jsonarray=array(
						'amount'=>$info->chitamounts,
						'noofmonths'=>$info->noofmonths,
						'baseamount'=>$info->baseamount,
						'luckydraw'=>$info->luckydraw,
						'gift'=>$info->gift,
						'variablemodeid'=>$info->variablemodeid
						);
		echo json_encode($jsonarray);
	}
	/*Stock Entry */
	//check integer value
	public function checkintegervalue($value,$returnvalue) {
		if(is_numeric($value)) {
			return $value;
		} else {
			return $returnvalue;
		}
	}
	/*	counter_groupdropdown */
	public function counter_groupdropdown() {
		$industryid = $this->industryid;
		$this->db->select('countername,counterid,parentcounterid,counterdefault,counterdefaultid');
		$this->db->from('counter');
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->where('storagetypeid',2);
		$this->db->where('status',$this->Basefunctions->activestatus);		
		$this->db->order_by('counterid','asc');		
		$info = $this->db->get();
		return $info;
	}
	/*	counter_groupdropdown_notdefault */
	public function counter_groupdropdown_notdefault() {
		$industryid = $this->industryid;
		$this->db->select('countername,counterid,parentcounterid,parentcounterid,counterdefault,counterdefaultid,maxquantity,countertypeid');
		$this->db->from('counter');
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->where('storagetypeid',2);
		/* $this->db->where('counterdefault',0);
		$this->db->where('counterdefaultid',0); */
		$this->db->where_not_in('counterdefaultid',array(3,5,10,11));
		$this->db->where('status',$this->Basefunctions->activestatus);
		$this->db->order_by('counterid','asc');
		$info = $this->db->get();
		return $info;
	}
	// product display except bullion
	public function product_nonbullion_dd() {
		$category_level = $this->Basefunctions->get_company_settings('category_level'); //category_level
		$this->db->select('product.productname,product.productid,product.categoryid,category.categoryid,category.categoryname,category.categorylevel,productstorage.counterid,product.bullionapplicable,product.taxable,product.taxmasterid,product.purityid');
		$this->db->from('product');
		$this->db->join('category','category.categoryid = product.categoryid');
		$this->db->join('productstorage','productstorage.productid = product.productid');
		$this->db->where('product.bullionapplicable','No');
		$this->db->where('product.status',$this->Basefunctions->activestatus);
		$this->db->order_by('category.categoryid','asc');
		$this->db->order_by('category.categoryname','asc');
		$this->db->order_by('product.productname','asc');
		$info = $this->db->get();
		return $info;
	}
	/* stone_groupdropdown */
	public function stone_groupdropdown() {
		$data = $this->db->query("SELECT st.stonetypeid,st.stonetypename,se.stonename,se.stoneid FROM stone As se
			LEFT JOIN stonecategory AS sc ON sc.stonecategoryid=se.stonecategoryid and sc.status NOT IN(3,0) 
			LEFT JOIN stonetype AS st ON st.stonetypeid=sc.stonetypeid and st.status NOT IN(3,0) where se.stonemodeid NOT IN (2) AND se.status NOT IN (3, 0) ORDER BY se.stonetypeid asc ,se.stoneid desc");
		return $data;
	}
	//Account Group DD
	public function accountgroup_dd($type) {
			$query = $this->db->query(
					   "SELECT account.accountname,account.accountid,accounttype.accounttypename,account.accounttypeid,account.accounttaxstatus,account.mobilenumber,
						account.accountshortname
						FROM account
						JOIN accounttype ON accounttype.accounttypeid = account.accounttypeid
						WHERE account.status =1 and account.accounttypeid in (".$type.")
						ORDER BY account.accounttypeid asc
						");
			return $query;
	}
	/**
	*module field property retrieve validation/enable disabled
	*@param1 moduleid - moduleid of form
	*@param2 formname - formname where proprty applied
	*/
	public function retrivemodulerule($moduleid,$formname) {		
		$validatearray = array();
		$enablearray = array();
		$hidearray = array();
		$hideapplyarray = array();
		$keyarr = array();
		$fieldsarray = array(); 
		$counter_license = $this->Basefunctions->get_company_settings('counter');
		$this->db->select('modulefieldrule.moduleid,modulefieldrule.formname,modulefieldrule.validate,modulefieldrule.disable,modulefieldrule.hide,modulefieldrule.hideapply,roundtype.roundtypevalue,modulefieldrule.keyonename,modulefieldrule.keytwoname,modulefieldrule.formfieldidname,modulefieldrule.roundtypeid');
		$this->db->from('modulefieldrule');
		$this->db->join('roundtype','roundtype.roundtypeid = modulefieldrule.roundtypeid');
		$this->db->where('modulefieldrule.moduleid',$moduleid);
		$this->db->where('modulefieldrule.formname',$formname);
		$this->db->where('modulefieldrule.status',$this->Basefunctions->activestatus);
		$this->db->order_by('modulefieldrule.modulefieldruleid','asc');
		$data = $this->db->get()->result();
		
		foreach($data as $info)
		{
			$key = $info->keyonename.'_'.$info->keytwoname;
			if(!in_array($key,$keyarr))
			{
				array_push($keyarr,$key);
				$validatearray[$key] = array(); //intialize the key array - validate
				$enablearray[$key] = array();  //initialize the key array - enable/disable		
				$hidearray[$key] = array();  //initialize the key array - hide/show
				$hideapplyarray[$key] = array(); //intialize to apply -special case.
				$fieldsarray[$key] = array(); 
			}			
			//validation
			//generate validate
			
			$validate = '';
			if(($info->validate != '') or ($info->roundtypeid != '1') )
			{	
					$validate = "validate[";
					if($info->validate != '')
					{
						$validate .= $info->validate.',';
					}
					if($info->roundtypeid != '1' && $info->roundtypeid != '6' ) 
					{
						$validate .= "decval[".$info->roundtypevalue."],";
					}
					
					$validate = rtrim($validate, ','); //to remove trailing comma
					$validate .= "]";
			}
			$validatearray[$key] = array_merge($validatearray[$key],array($info->formfieldidname => $validate));
			// enable/disable
			$enable = 'enabled';
			if($info->disable == 'YES')
			{
				$enable = 'disabled';
			}
			if($info->formfieldidname == 'counter' or $info->formfieldidname == 'counterid' or $info->formfieldidname == 'fromcounterid' )
			{
				if($counter_license == 'NO')
				{ 
					$enable = 'disabled'; 
				}
				else if($counter_license == 'YES' && $info->disable == 'YES')
				{ 
					$enable = 'disabled'; 
				}
				
			}
			if($info->formfieldidname == 'lotid') 
			{
				$enable = ''; 
			} 
			$enablearray[$key] = array_merge($enablearray[$key],array($info->formfieldidname =>$enable));
			// show/hide			
			$hide = '';
			if($info->hide == 'YES' && $info->hideapply == 'YES')
			{
				$hide = 'hide';
			}
			$hidearray[$key] = array_merge($hidearray[$key],array($info->formfieldidname =>$hide));
			$fieldsarray[$key] = array_merge($fieldsarray[$key],array($info->formfieldidname =>$info->formfieldidname));
		}
		$main_array = array(
								'validate'=>$validatearray,
								'enable'=>$enablearray,
								'hide'=>$hidearray,
								'fields'=>$fieldsarray,
							);	
		//print_r($hidearray);		die();	
		return $main_array;	
		
	}
	/**
	*display readonly based on licence company settings
	*/
	public function get_company_displaysetting($value) {
		$info = $this->db->select($value)
							->from('companysetting')
							->limit(1)
							->get()
							->row();
		if($value != '') {
			return $info->$value;
		} else {
			return 'readonly';
		}
	}
	/*
	*	product_dd-level based dropdown values
	*/
	public function product_dd() {
		$industryid = $this->industryid;
		$info =$this->db->query("SELECT `product`.`productname`, `product`.`productid`, `product`.`categoryid`,`product`.`weightcalculationtypeid`, `category`.`categoryid`, `category`.`categoryname`,`category`.`taxmasterid` as categorytaxid,`category`.`categorylevel`, `product`.`counterid` as counterid, `product`.`bullionapplicable`, `product`.`taxable`, group_concat(`tax`.`taxname`, '-',`tax`.`taxid`) as taxid,group_concat(distinct(`tax`.`taxid`)) as taxdataid,`taxmaster`.`taxmasterid` as taxmasterid, `product`.`purityid`,`product`.`chargeid`,`product`.`purchasechargeid`,`product`.`stoneapplicable`,`product`.`size`,`product`.`productstonecalctypeid`,`product`.`tagtypeid`,`product`.`stoneid`,`product`.`loosestone` FROM `product` JOIN `category` ON `category`.`categoryid` = `product`.`categoryid` JOIN `taxmaster` ON `taxmaster`.`taxmasterid` = `product`.`taxmasterid` LEFT JOIN `tax` ON `tax`.`taxmasterid` = `taxmaster`.`taxmasterid` JOIN `charge` ON `charge`.`chargeid` = `product`.`chargeid` JOIN `purchasecharge` ON `purchasecharge`.`purchasechargeid` = `product`.`purchasechargeid` WHERE `product`.`status` = 1 and `product`.`categoryid` not in (4)  and `product`.`industryid` = ".$industryid." GROUP BY `product`.`productid` ORDER BY `product`.`productid` DESC");
		return $info;
	}
	//dynamic view column name
    public function vardataviewdropdowncolumns($moduleids) {
		//restrict the categorycolumns
		$restrictcategorycounter = $this->restrictcategorycountervccbase();
		$this->db->select('viewcreationcolumns.viewcreationcolumnid,viewcreationcolumns.viewcreationcolumnname,viewcreationcolumns.uitypeid',false);
		$this->db->from('viewcreationcolumns');		
		$this->db->where_in('viewcreationcolumns.viewcreationmoduleid',$moduleids);
		$this->db->where('viewcreationcolumns.status',$this->activestatus);
		if(count($restrictcategorycounter) > 0) {
			$this->db->where_not_in('viewcreationcolumns.viewcreationcolmodelname',$restrictcategorycounter);
		}
		$this->db->where_not_in('viewcreationcolumns.viewcreationcolumnviewtype',array(0));		
		$this->db->order_by('viewcreationcolumns.viewcreationcolumnid',"ASC");
		$query=$this->db->get();
		return  $query->result();
    }
	//restrict the categorycolumns
	public function restrictcategorycountervccbase() {
		$restrictcategorycounter = array();
		$categorylevel = $this->Basefunctions->get_company_settings('category_level');
		$counterlevel = $this->Basefunctions->get_company_settings('counter_level');
		$j=0;
		for($i=$counterlevel+1;$i<=6;$i++) {
			$restrictcategorycounter[$j] = 'counter_'.$i.'';
			$j++;
		}
		for($i=$categorylevel+1;$i<=6;$i++) {
			$restrictcategorycounter[$j] = 'category_'.$i.'';
			$j++;
		}
		return $restrictcategorycounter;
	}
	// Load => Report builder Condition based on UI tpye
	public function loadconditionbyuitype() {
		$result=$this->db->select('whereclauseid',false)->where('uitypeid',$_GET['uitypeid'])
		->where_in('status',array(1,3))->get('uitype')->row()->whereclauseid;
		$result=explode(',',$result);
		$this->db->select('whereclause.whereclauseid,whereclause.whereclausename',false);
		$this->db->from('whereclause');
		$this->db->where_in('whereclause.whereclauseid',$result);
		$this->db->where_in('whereclause.status',$this->Basefunctions->activestatus);
		$data=$this->db->get();
		if($data->num_rows() >0) {
			$i=0;
			foreach($data->result()as $row) {
				$ddata[$i]=array('whereclausename'=>$row->whereclausename,'whereclauseid'=>$row->whereclauseid);
				$i++;
			}
			echo json_encode($ddata);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}		
	}
	/*Sales => Transaction  Start */	
	/** *	get the rate of metal/purity */
	public function getrate($purity) {		
		$data = $this->db->select('currentrate')
					->from('rate')
					->where('purityid',$purity)
					->where('status',$this->activestatus)
					->order_by('createdate','desc')
					->limit(1)
					->get();
		if($data->num_rows()){
			foreach($data->result() as $info){
				$rate = $info->currentrate;
			}
			$ratearr = array('rate'=>$rate);
		}
		else{
			$ratearr = array('rate'=>0);
		}
		return $ratearr;
	}
	// check unique name function
	public function checkuniquename($table,$fieldname,$value,$editid) {	
		if($editid!=''){//Edit Page
		$check = 'NO';
		$this->db->select($fieldname);
		$this->db->from($table);
		$this->db->where($fieldname,trim($value));
		$this->db->where($table."id !=",$editid);	
		$this->db->where('status',1);
		$info=$this->db->get()->num_rows();
		if($info > 0){
			$check = 'YES';
		}
		echo trim($check);
		}
		else{ //Add Page
		$check = 'NO';
		$this->db->select($fieldname);
		$this->db->from($table);
		$this->db->where($fieldname,trim($value));
		$this->db->where('status',1);
		$info=$this->db->get()->num_rows();
		if($info > 0){
			$check = 'YES';
		}
		echo trim($check);	
		}
	}
	//sales transaction type dd load
	public function salestransactiontypedropdown() {
		$userroleid = $this->Basefunctions->userroleid;
		$data = $this->db->select('salestransactiontype.salestransactiontypeid,salestransactiontype.salestransactiontypename,salestransactiontype.salestransactiontypelabel,salestransactiontype.summarylabel,salestransactiontype.stocktyperelationid')
					->from('salestransactiontype')
					->where("FIND_IN_SET('52',salestransactiontype.moduleid) >", 0)
					->where("FIND_IN_SET('$userroleid',salestransactiontype.userroleid) >", 0)
					->where('salestransactiontype.status',$this->Basefunctions->activestatus)
					->get();
		return $data;
	}
	// stock type dd load
	public function stocktypedropdown() {
		$userroleid = $this->Basefunctions->userroleid;	
		$data = $this->db->select('stocktype.stocktypeid,stocktype.stocktypename,stocktype.stocktypelabel,stocktype.shortname')
					->from('stocktype')
					->where("FIND_IN_SET('52',stocktype.moduleid) >", 0)
					->where("FIND_IN_SET('$userroleid',stocktype.userroleid) >", 0)
					->where('stocktype.status',$this->Basefunctions->activestatus)
					->order_by('stocktype.stocktypeid','asc')
					->get();
		return $data;
	}
	//main view filter tabgroup fetch
	public function moduletabgroupdetailsfetch($moduleid) {
		$userroleid = $this->userroleid;
		$industryid = $this->industryid;
		$i=0;
		$moduletabgrparray = array();
		$this->db->select('moduletabsection.moduletabsectionname,viewcreationcolumns.moduletabsectionid');
		$this->db->from('viewcreationcolumns');
		$this->db->join('moduletabsection','moduletabsection.moduletabsectionid=viewcreationcolumns.moduletabsectionid');
		$this->db->join('module','module.moduleid=moduletabsection.moduleid');
		$this->db->join('moduleinfo','moduleinfo.moduleid=moduletabsection.moduleid');
		$this->db->where_in('moduletabsection.moduleid',$moduleid);
		$this->db->where("FIND_IN_SET('$userroleid',moduletabsection.userroleid) >", 0);
		$this->db->where("FIND_IN_SET('$industryid',module.industryid) >", 0);
		$this->db->where('moduletabsection.status',1);
		$this->db->where('module.status',1);
		$this->db->where('moduleinfo.status',1);
		$this->db->group_by('moduletabsection.moduletabsectionname');
		$this->db->order_by('moduletabsection.moduletabsectionid','asc');
		$querytbg = $this->db->get();
		foreach($querytbg->result() as $rows) {
			$moduletabgrparray[$i] = array('tabsecid'=>$rows->moduletabsectionid,'tabsecname'=>$rows->moduletabsectionname);
			$i++;
		}
		return $moduletabgrparray;
	}
	//form with grid filter fetch
	public function formmoduletabgroupdetailsfetch($moduleid) {
		$userroleid = $this->userroleid;
		$industryid = $this->industryid;
		$i=0;
		$moduletabgrparray = array();
		$this->db->select('moduletabsection.moduletabsectionname,viewcreationcolumns.moduletabsectionid');
		$this->db->from('viewcreationcolumns');
		$this->db->join('moduletabsection','moduletabsection.moduletabsectionid=viewcreationcolumns.moduletabsectionid');
		$this->db->join('module','module.moduleid=moduletabsection.moduleid');
		$this->db->join('moduleinfo','moduleinfo.moduleid=moduletabsection.moduleid');
		$this->db->where('moduletabsection.moduleid',$moduleid);
		$this->db->where("FIND_IN_SET('$userroleid',moduletabsection.userroleid) >", 0);
		$this->db->where("FIND_IN_SET('$industryid',module.industryid) >", 0);
		$this->db->where('moduletabsection.status',1);
		$this->db->group_by('moduletabsection.moduletabsectionname');
		$this->db->order_by('moduletabsection.moduletabsectionid','asc');
		$querytbg = $this->db->get();
		foreach($querytbg->result() as $rows) {
			$moduletabgrparray[$i] = array('tabsecid'=>$rows->moduletabsectionid,'tabsecname'=>$rows->moduletabsectionname);
			$i++;
		}
		return $moduletabgrparray;
	}
	// module tab sectio detail fetch based on given modules
	public function moduletabsectiondetailsfetch($moduleids) {
		$modid = $this->Basefunctions->getmoduleid($moduleids);
		$this->db->select('viewcreationcolumns.viewcreationcolumnid,viewcreationcolumns.viewcreationcolumnname,moduletabsection.moduletabsectionname,viewcreationcolumns.moduletabsectionid,viewcreationcolumns.uitypeid,viewcreationcolumns.fieldrestrict,viewcreationcolumns.fieldview,viewcreationcolumns.viewcreationcolmodelindexname,viewcreationcolumns.uitypeid,viewcreationcolumns.viewcreationcolmodelindexname,viewcreationcolumns.viewcreationmoduleid',false);
		$this->db->from('viewcreationcolumns');
		$this->db->join('moduletabsection','moduletabsection.moduletabsectionid=viewcreationcolumns.moduletabsectionid');
		$this->db->join('moduletabgroup','moduletabgroup.moduletabgroupid=moduletabsection.moduletabgroupid');
		$this->db->join('module','module.moduleid=viewcreationcolumns.viewcreationmoduleid');
		$this->db->join('moduleinfo','moduleinfo.moduleid=moduleinfo.moduleid');
		$this->db->where_in('viewcreationcolumns.viewcreationmoduleid',$modid);
		$this->db->where_in('moduleinfo.moduleid',array(1));
		$this->db->where('viewcreationcolumns.status',$this->activestatus);
		$this->db->where_not_in('viewcreationcolumns.fieldrestrict',array(1));
		$this->db->where_not_in('viewcreationcolumns.viewcreationcolumnviewtype',array(0));
		$this->db->order_by('moduletabsection.moduletabsectionid',"ASC");
		$this->db->order_by('viewcreationcolumns.viewcreationcolumnid',"ASC");
		$this->db->group_by('viewcreationcolumns.viewcreationcolumnid');
		$query=$this->db->get();
		return  $query->result();
	}
	//form with grid module tab section deatil fetch
	public function formmoduletabsectiondetailsfetch($moduleids) {
		$this->db->select('viewcreationcolumns.viewcreationcolumnid,viewcreationcolumns.viewcreationcolumnname,moduletabsection.moduletabsectionname,viewcreationcolumns.moduletabsectionid,viewcreationcolumns.uitypeid,viewcreationcolumns.fieldrestrict,viewcreationcolumns.fieldview,viewcreationcolumns.viewcreationcolmodelindexname,viewcreationcolumns.uitypeid,viewcreationcolumns.viewcreationcolmodelindexname,viewcreationcolumns.viewcreationmoduleid',false);
		$this->db->from('viewcreationcolumns');
		$this->db->join('moduletabsection','moduletabsection.moduletabsectionid=viewcreationcolumns.moduletabsectionid');
		$this->db->join('moduletabgroup','moduletabgroup.moduletabgroupid=moduletabsection.moduletabgroupid');
		$this->db->where('viewcreationcolumns.viewcreationmoduleid',$moduleids);
		$this->db->where('viewcreationcolumns.status',$this->activestatus);
		$this->db->where_not_in('viewcreationcolumns.fieldrestrict',array(1));
		$this->db->where_not_in('viewcreationcolumns.viewcreationcolumnviewtype',array(0));
		$this->db->order_by('moduletabsection.moduletabsectionid',"ASC");
		$this->db->order_by('viewcreationcolumns.viewcreationcolumnid',"ASC");
		$query=$this->db->get();
		return  $query->result();
	}
	// get  scheme name
	public function schemenameload() {
		$schemenames = $this->db->select('chitschemeid,chitschemename,maxschemeamount,minnoofmonths,chitlookupsid,intrestmodeid,variablemodeid')->from('chitscheme')->where('status',$this->Basefunctions->activestatus)->get();
		$i=0;
		foreach($schemenames->result() as $schemenamesdata) {
			$dataschemenames[$i] = array('chitschemeid'=>$schemenamesdata->chitschemeid,'schemename'=>$schemenamesdata->chitschemename,'maxschemeamount'=>$schemenamesdata->maxschemeamount,'minnoofmonths'=>$schemenamesdata->minnoofmonths,'chitlookupsid'=>$schemenamesdata->chitlookupsid,'intrestmodeid'=>$schemenamesdata->intrestmodeid,'variablemodeid'=>$schemenamesdata->variablemodeid);
			$i++;
		}
		$datas = ( ($i==0)? json_encode(array('fail'=>'FAILED')) : json_encode($dataschemenames) );
		echo $datas;
	}
	//simple drop down with condition multiple
	public function simpledropdownwithcondmultiple($did,$dname,$table,$whfield,$whdata) {
		$industryid = $this->industryid;
		$this->db->select("$dname,$did");
		$this->db->from($table);
		$this->db->where_in($whdata,$whfield);
		$this->db->where('status',1);
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$result = $this->db->get();
		return $result->result();
	}
	//check the module exist in module tab section
	public function checkthemoduleexistinmoduletabsec($moduleids) {
		$this->db->select('COUNT(moduletabsectionid) as available',false);
		$this->db->from('moduletabsection');
		$this->db->where_in('moduletabsection.moduleid',$moduleids);
		$this->db->where('moduletabsection.status',$this->activestatus);
		$query=$this->db->get()->row();
		return $query;
	}
	// load related module
	public function relatedmodulelistfetchmodel() {
		$moddatas = array();
		$relmodids = array();
		$moduleid = trim($_POST['moduleid']);
		$relmoduleids = $this->db->select('modulerelationid,relatedmoduleid')->from('modulerelation')->where('moduleid',$moduleid)->get();
		foreach($relmoduleids->result() as $reldata) {
			$relmodids[] = $reldata->relatedmoduleid;
		}
		$i=0;
		$modids = implode(',',$relmodids);
		$moduleids = ( ($modids!="")? $modids.','.$moduleid : $moduleid );
		$modids = explode(',',$moduleids);
		$allmoduleids = implode(',',array_unique($modids));
		$genmod = array();
		$modinfo = $this->db->query('select module.moduleid,module.modulename from module join moduleinfo ON moduleinfo.moduleid=module.moduleid where module.moduleid IN ('.$allmoduleids.') AND module.status=1 AND moduleinfo.status=1 group by moduleinfo.moduleid',false);
		foreach($modinfo->result() as $moddata) {
			$modtype = ( ($moddata->moduleid==$moduleid)?'':((in_array($moddata->moduleid,$genmod))?'GEN:':'REL:') );
			$moddatas[$i] = array('datasid'=>$moddata->moduleid,'dataname'=>$moddata->modulename,'datatype'=>$modtype);
			$i++;
		}
		$datas = (($i==0)? json_encode(array('fail'=>'FAILED')) : json_encode($moddatas));
		echo $datas;
	}
	/* fetch data information dynamic report creation //for Report */
    public function reportdynamicdatainfofetch($creationid,$pagenum,$rowscount,$sortcol,$sortord,$rowid,$viewcolmoduleids,$reporttype) {	
		$this->load->model('Reportsview/Reportsmodel');
		$filtercondvalue ="";
		$havingString="";
		$whereString="";
		$groupid="";
		$joinq = "";
		$grpselectstmt="";
		//header colids
		$headcolids = $this->reportmobiledatarowheaderidfetch($creationid);
		//view moduleid
		$viewmodid = $this->reportmobiledataviewmoduleidfetch($creationid);
		{
			/*Generate Where Clause Condition Start	*/
			$w = '1=1';
			$conditionvalarray = $this->Reportsmodel->generateconditioninfo($creationid);
			$c = count($conditionvalarray);
			if($c>0) {
				$whereString=$this->Reportsmodel->generatewhereclause($conditionvalarray['conditionalarry']);
				$havingString=$this->Reportsmodel->generatehavingclause($conditionvalarray['conditionalarry']);
				if(!empty($whereString)>0){$whereString=$whereString;}else{$whereString="1=1";}
			} else {
				$whereString=$w;
				$havingString=$w; 
			}		
			if(!empty($havingString)){
				$havingString=" HAVING ".$havingString;
			}
			else{
				$havingString="";
			}
			/*Generate Where Clause Condition End*/
		}
		
		{
			$selectedviewcolids=explode(",",$_GET['viewfieldids']);
			$reportcolids=array_unique(array_merge($conditionvalarray['viewcolid'],$selectedviewcolids));
			/* Generate Join & Group by & Order By condition */
			$grouporder=$this->Reportsmodel->reportsortbygroupbyquery($creationid);
			$grpcolids=implode(",",$grouporder['groupbycolids']);
			$reportcolids=array_unique(array_merge($grouporder['viewcolids'],$reportcolids));
		}
		{
			/* Join Query Generation Start*/
			$join_stmt = $this->Reportsmodel->generatejoinquery($viewcolmoduleids,$reportcolids,$_GET['maintabinfo']);
			/* Join Query Generation End */
		}
		{
			/* Select Query Generation Start*/
			//main table
			$maintable = $_GET['maintabinfo'];
			$selectmaintable= $maintable.'.'.$rowid.', ';
			$selectmaintablewotbl= $rowid.', ';
			$in = 0;
			/* View Creation column & Group by ids spiting */
			$a1=explode(",",$_GET['viewfieldids']);
			$x1=array_merge($a1,$conditionvalarray['viewcolid']);
			// Checking the view col with group by col and get separate the 
			$groupcolids = array_intersect($a1,$grouporder['groupbycolids']);	
			$viewcolids=array_diff($x1,$groupcolids); 
			if(count($grouporder['groupbycolids'])==0) {
				$gids=implode(",",$grouporder['groupbycolids']);
				$groupidfocolheader=$grouporder['groupbycolids'];
			} else {
				$gids=implode(",",$grouporder['groupbycolids']);
				$groupidfocolheader=$grouporder['groupbycolids'];
			}
			$vids=implode(",",$viewcolids);
			if(count($grouporder['groupbycolids'])!=0){ 
				$grpselectstmt =$this->Reportsmodel->generateselectstmt($gids);
			} else {
				$grpselectstmt['Selectstmt']='';
			}
			$selectstmt =$this->Reportsmodel->generateselectstmt($vids);
			/* Select Query Generation End*/
		}
		//get report type
		$dateformat = $this->Basefunctions->phpmysqlappdateformatfetch();
		$mysqlformat = $dateformat['mysqlformat'];
		$rpdata = $this->db->query('select reporttypeid,reportdatecolumnid,reportdatemethod,reportmode,DATE_FORMAT(reportstartdate,"'.$mysqlformat.'") AS startdate,DATE_FORMAT(reportenddate,"'.$mysqlformat.'") AS enddate from report where report.reportid='.$creationid.' LIMIT 1');
		foreach($rpdata->result() as $inf){
			$reporttypeid=$inf->reporttypeid;
			$reportdatecolumnid=$inf->reportdatecolumnid;
			$reportdatemethod=$inf->reportdatemethod;
			$reportmode=$inf->reportmode;
			$reportstartdate=$inf->startdate;
			$reportenddate=$inf->enddate;
		}
		//check date column range
		$datewhere="1";
		$date = date("Y-m-d");
		if($reportdatecolumnid > 1 and $reportdatemethod != '') {
			//get column specific data
			$daterangecolumn= $this->Reportsmodel->reportdatecolumn($reportdatecolumnid);
			if($reportmode == 'custom'){
				$startdate=$reportstartdate;
				$startdate = date('Y-m-d', strtotime($startdate));
				$enddate = $reportenddate;
				$enddate = date('Y-m-d', strtotime($enddate));
				
			}
			elseif($reportmode == 'switch'){
				$r_date=$this->Basefunctions->datesystem($reportdatemethod);
				$startdate=$r_date['start'];
				$enddate=$r_date['end'];
			}
			elseif($reportmode == 'instant'){	
				if($reportdatemethod == '-1 day'){
				 $startdate=strtotime ('-1 day', strtotime ($date)) ;
				 $startdate=date("Y-m-d",$startdate) ;
				 $enddate=$startdate;	
				}
				elseif($reportdatemethod =='0 day'){
					$startdate=$date ;
					$enddate=$date; 					
				}
				elseif($reportdatemethod =='1 day'){
					$startdate=strtotime ('+1 day' , strtotime ( $date )) ;
					$startdate=date("Y-m-d",$startdate) ;
					$enddate=$startdate;
				}
			}
			elseif($reportmode == 'direct'){
				$f_date = strtotime(''.$reportdatemethod.'' ,strtotime($date));
				$f_date = date("Y-m-d",$f_date);
				if(strtotime($f_date) > strtotime($date)){
					$startdate=$date;
					$enddate=$f_date;
				}
				 else{
					$startdate=$f_date;
					$enddate=$date;
				}
			}else if($reportmode == 'before'){
				$f_date = strtotime(''.$reportdatemethod.' day' ,strtotime($date));
				$f_date = date("Y-m-d",$f_date);
				$startdate=$f_date;
				$enddate=$f_date;
			}
			$date_col=$daterangecolumn['parent'].'.'.$daterangecolumn['columnname'];
			if($reportmode == 'before') {
				$datewhere="DATE(".$date_col.") < '".$enddate."' ";
				$dateshow = 'Before '.$enddate;
			} else {
				if($startdate == $enddate) {
					$datewhere="DATE(".$date_col.") = '".$startdate."' ";
				} else {
					$datewhere="DATE(".$date_col.") >= '".$startdate."' AND DATE(".$date_col.") <= '".$enddate."'";
				}
			}			
		}
		else{
			$datewhere="1";
		}
		
		if($datewhere == 1){
			$datewh="1=1 ";
		}
		else{
			$datewh=$datewhere;
		}
		
		$status = $maintable.'.status NOT IN (0,3)';		
		$actsts = $this->Basefunctions->activestatus;
		//grid column title information fetch
			
		$colinfo = $this->reportinformationfetchmodel($creationid,$grouporder['groupbycolids'],$viewcolids);
		
		/* Pagination */
		$page = $pagenum-1;
		$start = $page * $rowscount;
		$li = ' LIMIT '.$start.','.$rowscount;
		
		if(count($grouporder['groupbycolids'])!=0)
		{	$groupselectstmt=$grpselectstmt['Selectstmt'].",";
			$groupselectstmtwotbl=$grpselectstmt['Selectstmtwithouttbl'].",";
			$rollup=" WITH ROLLUP";
		}
		else
		{ $groupselectstmt='';
		  $groupselectstmtwotbl='';
		  $rollup="";
		}		
		
		if($reporttype=='TV')
		{
			$query = 'SELECT '.$groupselectstmt.' '.$selectmaintable.' '.$selectstmt['Selectstmt'].' from '.$maintable.' '.$join_stmt.' WHERE '.$status.' AND '.$whereString.' AND '.$datewh. ' '.$havingString. ' '. $grouporder['orderbyquery'];
		}
		else
		{
			$query = 'SELECT SQL_CALC_FOUND_ROWS '.$groupselectstmtwotbl.' '.$selectmaintablewotbl.' '. $selectstmt['Selectstmtwithouttbl'].' from (select  '.$groupselectstmt.' '.$selectmaintable.' '.$selectstmt['Selectstmt'].' from '.$maintable.' '.$join_stmt.' WHERE '.$status.' AND '.$whereString.' AND '.$datewh. ' '. $grouporder['groupbyquery'] .' '.$havingString .' ) AS t '. $grouporder['groupbyquerywotbl'] . ' ' .$rollup;
		}
		
		$data = $this->db->query($query);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		
		{// 6 Level Grouping Creation
			$countofgrpcol=count($grouporder['groupcol']);
			for($i=$countofgrpcol;$i<=5;$i++)
			{	array_push($grouporder['groupcol'],'');
				array_push($grouporder['groupbycolids'],'');
			}
			$groupcolsummaryids=$grouporder['groupbycolids'];
			$groupcolsummarycolumns=$grouporder['groupcol'];
		}
		$finalresult=array($colinfo,$data,$page,'',$headcolids,$viewmodid,$creationid,$groupcolsummaryids,$groupcolsummarycolumns);
		return $finalresult;
    }
	//fetch colname & colmodel information fetch model
    public function reportinformationfetchmodel($creationid,$groupids,$viewids) {
    	$colids = 1;
    	$modname = '';
    	$modid = 1;
		//fetch show col ids
		$this->db->select('report.viewcreationcolumnid,report.reportid,viewcreationmodule.viewcreationmoduleid,viewcreationmodule.viewcreationmodulename',false);
		$this->db->from('report');
		$this->db->join('viewcreationmodule','viewcreationmodule.viewcreationmoduleid=report.moduleid');
		$this->db->where_in('report.reportid',$creationid);
		$this->db->where('report.status',1);
		$result=$this->db->get()->result();
		foreach($result as $row) {
			$colids=$row->viewcreationcolumnid;
			$modname = $row->viewcreationmodulename;
			$modid = $row->viewcreationmoduleid;
			$colsize = "200";
		}
		$device = $this->deviceinfo();
		if($device=='phone') {
			$headcolids = $this->mobiledatarowheaderidfetch($creationid);
			$vwcolids = explode(',',$colids);
			$vwmgcolids = array_merge($vwcolids,$headcolids);
			$viewcolids = array_unique($vwmgcolids);
		} else {
			$viewcolids = explode(',',$colids);
		}
		/* for fetch colname & colmodel fetch */
		$i=0;
		$m=0;
		$data = array();
		$colsizes = explode(',',$colsize);	
		
		$colids= array_merge($groupids,$viewids);	
		
		foreach($colids as $colid) {
			$this->db->select("viewcreationcolumns.viewcreationcolumnid,
								viewcreationcolumns.viewcreationcolumnname,
								viewcreationcolumns.viewcreationcolmodelname,
								viewcreationcolumns.viewcreationcolmodelindexname,
								viewcreationcolumns.viewcreationcolmodeljointable,
								viewcreationcolumns.viewcreationcolumnid,
								viewcreationcolumns.viewcreationcolmodelaliasname,
								viewcreationcolumns.viewcreationcolmodeltable,
								viewcreationcolumns.viewcreationparenttable,
								viewcreationcolumns.viewcreationmoduleid,
								viewcreationcolumns.viewcreationjoincolmodelname,
								viewcreationcolumns.viewcreationcolumnviewtype,
								viewcreationcolumns.viewcreationtype,
								viewcreationcolumns.viewcreationcolmodelcondname,
								viewcreationcolumns.uitypeid,
								viewcreationcolumns.calcoperation,
								viewcreationcolumns.moduletabsectionid,
								viewcreationcolumns.viewcreationcolmodelcondvalue,
								viewcreationcolumns.viewcreationcolumnicon,
								viewcreationcolumns.viewcreationcolumnsize");
			$this->db->from('viewcreationcolumns');
			$this->db->where('viewcreationcolumns.viewcreationcolumnid',$colid);
			$this->db->where('viewcreationcolumns.status',1);
			$showfield = $this->db->get();
			if($showfield->num_rows() >0) {
				foreach($showfield->result() as $show) {
					$data['colid'][$i]=$show->viewcreationcolumnid;
					$data['colname'][$i]=$show->viewcreationcolumnname;
					$data['colicon'][$i]=$show->viewcreationcolumnicon;
					$data['colsize'][$i]="200";
					$data['colmodelname'][$i]=$show->viewcreationcolmodelname;
					$data['colmodelaliasname'][$i]=$show->viewcreationcolmodelaliasname;
					$data['coltablename'][$i]=$show->viewcreationcolmodeltable;
					$data['coljointablename'][$i]=$show->viewcreationcolmodeljointable;
					$data['coljoinmodelname'][$i]=$show->viewcreationjoincolmodelname;
					$data['colmodelindex'][$i]=$show->viewcreationcolmodeljointable .'.'.$show->viewcreationcolmodelindexname;
					$data['colmodelparenttable'][$i]=$show->viewcreationparenttable;
					$data['colmodelviewtype'][$i]=$show->viewcreationcolumnviewtype;
					$data['colmodeldatatype'][$i]=$show->viewcreationtype;
					$data['colmodelcondname'][$i]=$show->viewcreationcolmodelcondname;
					$data['colmodelcondvalue'][$i]=$show->viewcreationcolmodelcondvalue;
					$data['colmodelmoduleid'][$i]=$show->viewcreationmoduleid;
					$data['colmodeluitype'][$i]=$show->uitypeid;
					$data['colmodelsectid'][$i]=$show->moduletabsectionid;
					$data['colmodeltype'][$i]='text';
					$data['modname'][$i] = $modname;
					$data['calcoperation'][$i]=$show->calcoperation;
					$i++;
				}
			}
			$m++;
		}
		
		return $data;
    }
	//mobile data row header id
    public function reportmobiledatarowheaderidfetch($creationid) {
    	$headcolids = array();
    	$this->db->select('viewcreationcolumns.viewcreationcolumnid',false);
    	$this->db->from('viewcreationcolumns');
    	$this->db->join('report','report.moduleid=viewcreationcolumns.viewcreationmoduleid');
    	$this->db->where('report.reportid',$creationid);
    	$this->db->where('viewcreationcolumns.fieldview',1);
    	$this->db->where('viewcreationcolumns.status',1);
    	$this->db->ORDER_BY('viewcreationcolumns.viewcreationcolumnid','ASC');
    	$this->db->limit(2,0);
    	$datas = $this->db->get();
    	foreach($datas->result() as $row) {
    		$headcolids[] = $row->viewcreationcolumnid;
    	}
    	return $headcolids;
    }
	/* mobile data row view module id */
    public function reportmobiledataviewmoduleidfetch($creationid) {
    	$modid = 1;
    	$this->db->select('report.moduleid',false);
    	$this->db->from('report');
    	$this->db->where('report.reportid',$creationid);
    	$this->db->where('report.status',1);
    	$datas = $this->db->get();
    	foreach($datas->result() as $row) {
    		$modid = $row->moduleid;
    	}
    	return $modid;
    }
	// check report type model
	public function checkreporttypemodel() {
		$viewcolid=explode(',',$_REQUEST['viewcolid']);		
		$this->db->select('GROUP_CONCAT(viewcreationcolumnname) as colname,COUNT(viewcreationcolumnid) as countofid',false);
    	$this->db->from('viewcreationcolumns');
    	$this->db->where_in('viewcreationcolumnid',$viewcolid);
		$this->db->where('calcoperation!=','');
		$result=$this->db->get()->result();
		return $result[0];
	}
    // load related module
	public function relatedmodulelistfetchforprint() {
		$moddatas = array();
		$relmodids = array();
		$moduleid = trim($_POST['moduleid']);
		$relmoduleids = $this->db->select('relatedmoduleid')->from('modulerelation')->where('moduleid',$moduleid)->where_not_in('relatedmoduleid',$moduleid)->group_by('modulerelation.relatedmoduleid')->get();
		//print_r($relmoduleids->result()); exit;
		foreach($relmoduleids->result() as $reldata) {
			$relmodids[] = $reldata->relatedmoduleid;
		}
		$i=0;
		$modids = implode(',',$relmodids);
		$moduleids = ( ($modids!="")? $modids : $moduleid );
		$modids = explode(',',$moduleids);
		$allmoduleids = implode(',',array_unique($modids));
		$genmod = array();
		$modinfo = $this->db->query('select module.moduleid,module.modulename from module where module.moduleid IN ('.$allmoduleids.') AND module.status=1',false);
		foreach($modinfo->result() as $moddata) {
			$moddatas[$i] = array('datasid'=>$moddata->moduleid,'dataname'=>$moddata->modulename);
			$i++;
		}
		$datas = (($i==0)? json_encode(array('fail'=>'FAILED')) : json_encode($moddatas));
		echo $datas;
	}	
	//report condition header information fetch
	public function reportconddatafetchmodel($moduleid) {
		$fnames = array('reportcondrowid','reportandorcond','reportcondcolumn','reportcondition','reportaggregate','finalreportcondvalue','finalreportcondvalueid','reportcolstatus','reportcondcolumnid');
		$flabels = array('Condtion Id','AND/OR','Column Name','Condition','Aggregate Method','Valuename','Valueid','Status','ViewColumnId');
		$colmodnames = array('reportcondrowid','reportandorcond','reportcondcolumn','reportcondition','reportaggregate','finalreportcondvalue','finalreportcondvalueid','reportcolstatus','reportcondcolumnid');
		$colindexnames = array('reportcondition','reportcondition','reportcondition','viewcreationcondition','reportcondition','reportcondition','reportcondition','reportcondition','reportcondition');
		$uitypes = array('2','2','2','2','2','2','2','2','2');
		$viewtypes = array('0','1','1','1','1','1','0','0','0');
		$dduitypeids = array('17','18','19','20','20','23','25','26','27','28','29');
		$data = array();
		$i=0;
		$j=0;
		foreach($fnames as $fname) {
			if(in_array($uitypes[$j],$dduitypeids)) {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j].'name';
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]='0';
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]='2';
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			} else {
				$data['fieldname'][$i]=$fname;
				$data['fieldlabel'][$i]=$flabels[$j];
				$data['colmodelname'][$i]=$colmodnames[$j];
				$data['colmodelindex'][$i]=$colindexnames[$j];
				$data['colmodelviewtype'][$i]=$viewtypes[$j];
				$data['colmodeltype'][$i]='text';
				$data['colmodeluitype'][$i]=$uitypes[$j];
				$data['colmoduleid'][$i]=$moduleid;
				$i++;
			}
			$j++;
		}
		return $data;
	}
	//report condition grid details fetch
	public function reportconditiongriddatafetch() {
		$productdetail = '';
		$viewid = $_GET['userviewid'];
		$this->db->select('SQL_CALC_FOUND_ROWS reportcondition.reportconditionid,reportcondition.reportconditionid,viewcreationcolumns.viewcreationcolumnname,viewcreationcolumns.viewcreationcolumnid,reportcondition.viewcreationconditionname,reportcondition.viewcreationaggregatename,reportcondition.viewcreationconditionvalue,reportcondition.viewcreationconditionvaluename,reportcondition.viewcreationandorvalue,reportcondition.status',false);
		$this->db->from('reportcondition');
		$this->db->join('report','report.reportid=reportcondition.reportid');
		$this->db->join('viewcreationcolumns','viewcreationcolumns.viewcreationcolumnid=reportcondition.viewcreationcolumnid');
		$this->db->where('reportcondition.status',$this->Basefunctions->activestatus);
		$this->db->where('reportcondition.reportid',$viewid);
		$data=$this->db->get()->result();
		$j=0;
		foreach($data as $value) {
			$productdetail->rows[$j]['id']=$value->reportconditionid;
			$productdetail->rows[$j]['cell']=array(
					$value->reportconditionid,
					$value->viewcreationandorvalue,
					$value->viewcreationcolumnname,
					$value->viewcreationconditionname,
					$value->viewcreationaggregatename,
					$value->viewcreationconditionvaluename,
					$value->viewcreationconditionvalue,
					$value->status,
					$value->viewcreationcolumnid
			);
			$j++;
		}
		if($productdetail == '') {
			$productdetail = array('fail'=>'FAILED');
		}
		echo  json_encode($productdetail);
	}
	//get enabled moduleid
	public function getmoduleid($moduleid) {
		$data = '';
		$roleid = $this->userroleid;
		$industryid = $this->industryid;
		$this->db->select('moduleinfo.moduleid');
		$this->db->from('moduleinfo');
		$this->db->join('module','module.moduleid=moduleinfo.moduleid');
		$this->db->where_in('moduleinfo.moduleid',$moduleid);
		$this->db->where('moduleinfo.userroleid',$roleid);
		$this->db->where("FIND_IN_SET('".$industryid."',module.industryid) >", 0);
		$this->db->where('moduleinfo.status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {
			foreach($result->result() as $row) {
				$data = $row->moduleid;
			}
		}
		return $data;
	}
	// stone,stone addons,
	//simple drop down value fetch
	public function simpledropdown_var($tablename,$fieldname,$order) {
		$this->db->select($fieldname);
		$this->db->from($tablename);
		$this->db->where_not_in('status',array(0,3));
		$this->db->order_by($order,"asc");
		$query= $this->db->get();
		return $query->result();
	}
	//simple drop down value fetch with condition
	public function simpledropdownwithcond_var($did,$dname,$table,$whfield,$whdata) {
		$i=0;
		$mvalu=explode(",",$whdata);
		$this->db->select("$dname,$did");
		$this->db->from($table);
		if($whdata != '') {
			if(count($mvalu)>=2) {
				$this->db->where_in($whfield,$mvalu);
			} else {
				$this->db->where("FIND_IN_SET('$whdata',$whfield) >", 0);
			}
		}
		$this->db->where('status',1);
		$this->db->order_by($dname,"asc");
		$result = $this->db->get();
		return $result->result();
	}
	//simple grop dropdown
	public function simplegroupdropdown_var($did,$dname,$table,$whfield,$whdata) {
		$i=0;
		$this->db->select("$dname,$did");
		$this->db->from($table);
		if($whdata != '') {
			$this->db->where_in($whfield,$whdata);
		}
		$this->db->where('status',1);
		$this->db->order_by($dname,"asc");
		$result = $this->db->get();
		return $result->result();
	}
	public function simpledropdownwithcondmultiple_var($did,$dname,$table,$whfield,$whdata) {
		$industryid = $this->industryid;
		$this->db->select("$dname,$did");
		$this->db->from($table);
		$this->db->where_in($whdata,$whfield);
		$this->db->where('status',1);
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$result = $this->db->get();
		return $result->result();
	}
	//simple drop down value fetch with condition and order
	public function simpledropdownwithcondorder_var($did,$dname,$table,$whfield,$whdata,$orderby) {
		$i=0;
		$mvalu=explode(",",$whdata);
		$this->db->select("$dname,$did");
		$this->db->from($table);
		if($whdata != '') {
			if(count($mvalu)>=2) {
				$this->db->where_in($whfield,$mvalu);
			} else {
				$this->db->where("FIND_IN_SET('$whdata',$whfield)>",0);
			}
		}
		$this->db->where('status',1);
		$this->db->order_by($orderby,"asc");
		$result = $this->db->get();
		return $result->result();
	}
	/* fetch data information dynamic report creation //for Report */
    public function generatereport($creationid,$pagenum,$rowscount,$sortcol,$sortord,$rowid,$viewcolmoduleids,$reporttype,$filter=array(),$filetypename,$reportmodulesectionid=1,$reportname = 'General') {
		$checkstockreportdatefield = 0;
		if(isset($_GET['transferyesno'])){
			$transferyesno = $_GET['transferyesno'];
		} else{ 
			$transferyesno = 'no';
		}
		if($transferyesno == '') {
			$transferyesno = 'no';
		}
		$this->load->model('Reportsview/Reportsmodel');
		if($viewcolmoduleids == 49 || $viewcolmoduleids == 41) {
			$maintable = $this->Basefunctions->fetchmodueltabsectiotable($reportmodulesectionid);
			$rowid = $maintable.'id';
		} else {
			$maintable = $_GET['maintabinfo'];
			$rowid = $rowid;
		}
		$filtercondvalue ="";
		$havingString="";
		$havingSelect="";
		$whereString="";
		$groupid="";
		$joinq = "";
		$grpselectstmt="";
		$selectquerycolids=array();
		if($reporttype=='TV') {
			$selectquerycolids=ltrim($_GET['tabularviewcolids'],',');
			$summaramids=array_filter(explode('|',$_GET['summaramids']));
			$summaramname='';
		} else {
			$selectquerycolids=ltrim($_GET['summarviewcolids'],',');
			$summaramids=array_filter(explode('|',$_GET['summaramids']));
			$summaramname=array_filter(explode('|',$_GET['summaramname']));
		}
		if(!empty($filter)) {
			$fconditonid = explode('|',$filter[0]);
			$filtercondition = explode('|',$filter[1]);
			$filtervalue = explode('|',$filter[2]);
		} else {
			$fconditonid = array();
			$filtercondition = array();
			$filtervalue = array();
		}
		{
			$conditionminarr=$_GET['reportcreateconditionids'];
			$conditionmaxarr=array_values(explode(",",$_GET['reportcreateconditionids']));
			/*Generate Where Clause Condition Start	*/
			$w = '1=1';
			
			/** Stock Report - Retrieve for Date separation Starts Here **/
			if($_GET['reportcreateconditionids']) {
				$checkviewcolids = explode(",",$_GET['reportcreateconditionids']);
				if (in_array('4198', $checkviewcolids)) {
					$checkstockreportdatefield = 1;
					$takedatevalueforstockreports = $this->newgenerateconditioninfoforstockdate($_GET['reportcreateconditionids'],$_GET['reportconditiondata']);
					$datevalueforstockreports = $this->reportgeneratewhereclause($takedatevalueforstockreports['conditionalarry']);
				} else {
					$checkstockreportdatefield = 0;
				}
			}
			/** Stock Report - Retrieve for Date separation Ends Here **/
			
			$conditionvalarray = $this->newgenerateconditioninfo($_GET['reportcreateconditionids'],$_GET['reportconditiondata'],$fconditonid);
			$filtervalarray = $this->filtergenerateconditioninfo($fconditonid,$filtercondition,$filtervalue);
			$c = count($conditionvalarray['conditionalarry']);	
			if($c>0){
				$whereString=$this->reportgeneratewhereclause($conditionvalarray['conditionalarry']);
				if(COUNT($conditionvalarray['havingarray'])>0) {
					$havingString=$this->reportgeneratehavingclause($conditionvalarray['havingarray']);
					$havingSelect=$this->reporthavingclauseselectstmt($conditionvalarray['havingarray']);
					$havingSelect=$havingSelect['havingselect'];
				} else {
					$havingSelect='';
				}
				if(!empty($whereString)>0) {
					$whereString=$whereString;
				} else {
					$whereString="1=1";
				}
			} else {
				$whereString=$w;
				$havingString=$w;
			}
			if(!empty($havingString)) {
				$havingString=" HAVING ".$havingString;
			} else {
				$havingString="";
			}
			//filter condition generate
			if(count($filtervalarray) > 0) {
				$filterwherecondtion = $this->filtergeneratewhereclause($filtervalarray['filterarray'],$maintable);
				if($filterwherecondtion == '') {
					$filterwherecondtion = " AND 1=1";
				} else {
					$filterwherecondtion = " AND ".$filterwherecondtion;
				}
			} else {
				$filterwherecondtion = " AND 1=1";
			}
			/*Generate Where Clause Condition End*/
		}
		{	/* Generate Group by & Order By condition */
			$groupbymaxcolids=explode(",",$_GET['groupcolids']);
			$sortbytypemax=explode(",",$_GET['sortby']);
			$groupbyrange=$_GET['groupbyrange'];
			$grouporder=$this->newreportsortbygroupbyquery($groupbymaxcolids,$sortbytypemax,$groupbyrange);
		}
		{ 	/* Join Query Generation Start
			*/	
			//print_r($filtervalarray);
			$selectedviewcolids=explode(",",$_GET['viewfieldids']);
			$reportcolids=array_unique(array_merge($conditionmaxarr,$selectedviewcolids));
			$reportcolids=array_unique(array_merge($filtervalarray['fviewcolid'],$reportcolids));
			$reportcolids=array_unique(array_merge($grouporder['viewcolids'],$reportcolids));
			if(COUNT($conditionvalarray['havingarray'])>0) {
				$reportcolids=array_unique(array_merge($reportcolids,$conditionvalarray['havingcolids']));
			}
			//print_r($reportcolids);die();
			$join_stmt = $this->newgeneratejoinquery($viewcolmoduleids,$reportcolids,$_GET['maintabinfo']);
			/* Join Query Generation End */
		}
		{/* Select Query Generation Start*/
			//main table
			
			$selectmaintable= $maintable.'.'.$rowid.', ';
			$sumselectmaintable= "'' as ".$rowid.', ';
			$selectmaintablewotbl= $rowid.', ';
			$in = 0;
			$gids=implode(",",$grouporder['groupbycolids']);
			$groupidfocolheader=$grouporder['groupbycolids'];
			/* Combine all the col ids and Select col ids*/	
			$a1=explode(",",$selectquerycolids);
			$x1=array_merge($a1,$conditionmaxarr);
			$combineselectcolgroupcol = array_intersect($a1,$grouporder['groupbycolids']);	
			$combineslegrpcondcolids=array_diff($a1,$combineselectcolgroupcol);				
			$finalviewcolids=implode(",",array_filter($combineslegrpcondcolids));
			//Group by Select statements
			//$grpselectstmt['Selectstmt']='';
			if(count($grouporder['groupbycolids'])!=0) { 
				$grpselectstmt =$this->newgenerateselectstmt($gids,$reporttype,'','');
			}
			$selectstmt =$this->newgenerateselectstmt($finalviewcolids,$reporttype,$_GET['summarviewcolids'],$summaramids);
			/* Select Query Generation End*/
			//print_r($selectstmt['Selectstmt']);
			
		}	
		$reportdatecolumnid=$_GET['date_columnid'];
		$reportdatemethod=$_GET['date_method'];
		$reportmode=$_GET['date_mode'];
		$reportstartdate=$_GET['date_start'];
		$reportenddate=$_GET['date_end'];
		//check date column range
		$datewhere="1";
		$date = date("Y-m-d");
		$startdate = $date;
		$enddate = $date;
		$dateshow = '';
		/** Advanced Report for Date Filter but not using right now by Kumaresan **/
		if($reportdatecolumnid > 1 and $reportdatemethod != '') {
			//get column specific data
			$daterangecolumn= $this->Reportsmodel->reportdatecolumn($reportdatecolumnid);
			if($reportmode == 'custom'){
				$startdate=$reportstartdate;
				$startdate = date('Y-m-d', strtotime($startdate));
				$enddate = $reportenddate;
				$enddate = date('Y-m-d', strtotime($enddate));			
			} else if($reportmode == 'switch'){
				$r_date=$this->Basefunctions->datesystem($reportdatemethod);
				$startdate=$r_date['start'];
				$enddate=$r_date['end'];
			} else if($reportmode == 'instant'){
				$dateofinstant = $this->getdatevalue($reportdatemethod,$date);
				$startdate = $dateofinstant['start'];
				$enddate = $dateofinstant['end'];
			} else if($reportmode == 'direct') {
				$f_date = strtotime(''.$reportdatemethod.'' ,strtotime($date));
				$f_date = date("Y-m-d",$f_date);
				if(strtotime($f_date) > strtotime($date)) {
					$startdate=$date;
					$enddate=$f_date;
				} else {
					$startdate=$f_date;
					$enddate=$date;
				}
			}else if($reportmode == 'before'){
				$f_date = strtotime(''.$reportdatemethod.' day' ,strtotime($date));
				$f_date = date("Y-m-d",$f_date);
				$startdate=$f_date;
				$enddate=$f_date;
			}
			$date_col=$daterangecolumn['parent'].'.'.$daterangecolumn['columnname'];
			if($reportmode == 'before') {
				$datewhere="DATE(".$date_col.") < '".$enddate."' ";
				$dateshow = 'Before '.$enddate;
			} else {
				if($startdate == $enddate) {
					$datewhere="DATE(".$date_col.") = '".$startdate."' ";
				} else {
					$datewhere="DATE(".$date_col.") >= '".$startdate."' AND DATE(".$date_col.") <= '".$enddate."'";
				}
				$dateshow = $startdate.' To '.$enddate;
			}
			
		} else {
			$datewhere="1";
			$dateshow = 'ALL Data';	
		}
		if($datewhere == 1){
			$datewh="1=1 ";
		} else {
			$datewh=$datewhere;
		}
		$status = $maintable.'.status NOT IN (0,3,5)';		
		$actsts = $this->Basefunctions->activestatus;
		//grid column title information fetch
		//$viewmodid = $this->reportmobiledataviewmoduleidfetch($creationid);
		$viewmodid = $_GET['reportmodule'];
		$headcolids = $finalviewcolids;
		$colinfo = $this->newreportinformationfetchmodel($grouporder['groupbycolids'],$finalviewcolids,$reporttype,$_GET['summarviewcolids'],$summaramids,'');
		/* Pagination */
		if($pagenum != '')
		{
			$page = $pagenum-1;
			$start = $page * $rowscount;
			$li = ' LIMIT '.$start.','.$rowscount;
		}else{
			$li = ' LIMIT 0,100000000';
		}
		
		$industryid = $this->industryid;
		//$industry = $maintable.'.industryid IN ('.$industryid.')';
		if($viewcolmoduleids == 244 || $viewcolmoduleids == 243) {
			$industry = 'find_in_set('.$industryid.',module.industryid )';
		} else {
			$industry = 'find_in_set('.$industryid.','.$maintable.'.industryid )';
		}
		
		if(count($grouporder['groupbycolids'])!=0) {
			$groupselectstmt=$grpselectstmt['Selectstmt'].",";
			$groupselectstmtwotbl=$grpselectstmt['Selectstmtwithouttbl'].",";
			$sumgroupselectstmtwotbl=$grpselectstmt['Selecttabsum'].',';
		} else {
			$groupselectstmt='';
			$groupselectstmtwotbl='';
			$sumgroupselectstmtwotbl='';
			$rollup="";
		}
		
		// Userrole Based report generation
		$userroleid = $this->Basefunctions->userroleid;
		if($userroleid == 3 && $maintable == 'sales') {
			$loginuserid = $this->userid;
			$reportfilteraccountid = $this->singlefieldfetch('accountid','employeeid','employee',$loginuserid); // Retrieve Accountid of login Person
			if($reportfilteraccountid > 1) {
				$reportfilteraccountidcond = " AND sales.accountid IN($reportfilteraccountid) ";
			} else {
				$reportfilteraccountidcond = " AND 1=1 ";
			}
		} else {
			$reportfilteraccountidcond = " AND 1=1 ";
		}
		
		if($reporttype=='TV') {
			if(!empty($_REQUEST['tabularcondition'])) {
				$customcondition=$_REQUEST['tabularcondition']."=".'"'.$_REQUEST['tabularconditionvalue'].'"';
			} else {
				$customcondition="1=1";
			}
			if($sortcol != '' && $sortord != '') {
				$grouporder['orderbyquery'] = "ORDER BY $sortcol $sortord ";
			}
			if($_GET['summarviewcolids'] != '') {
				$query = '(SELECT '.$groupselectstmt.' '.$selectmaintable.' '.$selectstmt['Selectstmt'].' from '.$maintable.' '.$join_stmt.' WHERE '.$status.' AND '.$industry.' AND '.$whereString.' '.$filterwherecondtion.' AND '.$datewh. $reportfilteraccountidcond.' AND '. $customcondition. ' '. $grouporder['orderbyquery'].' '. $li.')';
				
				$query1 = '(SELECT SQL_CALC_FOUND_ROWS '.$groupselectstmt.' '.$selectmaintable.' '.$selectstmt['Selectstmt'].' from '.$maintable.' '.$join_stmt.' WHERE '.$status.' AND '.$industry.' AND '.$whereString.' '.$filterwherecondtion.' AND '.$datewh. $reportfilteraccountidcond.' AND '. $customcondition. ' '. $grouporder['orderbyquery'].' '. $li.')';
				
				$sumquery = '(SELECT '.$sumgroupselectstmtwotbl.' '.$sumselectmaintable.' '.$selectstmt['Selecttabsum'].' from '.$maintable.' '.$join_stmt.' WHERE '.$status.' AND '.$industry.' AND '.$whereString.' '.$filterwherecondtion.' AND '.$datewh. $reportfilteraccountidcond.' AND '. $customcondition. ' '. $grouporder['orderbyquery'].')';
				
				$query = $query.' UNION ALL '.$sumquery;
			} else {
				$query = 'SELECT SQL_CALC_FOUND_ROWS '.$groupselectstmt.' '.$selectmaintable.' '.$selectstmt['Selectstmt'].' from '.$maintable.' '.$join_stmt.' WHERE '.$status.' AND '.$industry.' AND '.$whereString.' '.$filterwherecondtion.' AND '.$datewh. $reportfilteraccountidcond.' AND '. $customcondition. ' '. $grouporder['orderbyquery'].' '. $li;
			}
			
		}  else {
			$whererollup = '';
			$grouprollup = '';
			$grouprollupselect = '';
			$rollup = "";
			$inselwhere = '';
			$inselect = '';
			if(count($grouporder['groupbycolids'])>0) {
				
				if($_GET['summaryrollup'] != '' && $_GET['summaryrollup'] >= 0 ) {
					$wgpfields = explode(",",$groupselectstmtwotbl);
					$wgpfields1 = str_replace('group by', '', $grouporder['groupbyquerywotbl']);
					$wgpfields1 = $wgpfields1.",";
					$wgpfields1 = explode(",",$wgpfields1);
					$rolcheck = 0;
					for($i=0;$i<count($wgpfields);$i++){
						if($wgpfields[$i] != ''){
							if($i==0){
								$rollup=" WITH ROLLUP";
								$whererollup = 'having ('.$wgpfields[$i].' = "Summary" or ('.$wgpfields[$i].' != "Total" )';
								$inselwhere  = '('.$wgpfields[$i].' = "Summary" ';
								$inselect = $wgpfields[$i].',';
								$grouprollup = ' group by '.$wgpfields1[$i].'';
								$grouprollupselect = "IFNULL(".$wgpfields[$i].", 'Summary')  as ".$wgpfields[$i].",";
							} 
							if($i<=$_GET['summaryrollup'] && $_GET['summaryrollup'] > 0){ 
								$whererollup .= ' or ('.$wgpfields[$i].' != "Total" ) or ('.$wgpfields[$i].' = "Total" )';
								$inselwhere  .= 'or '.$wgpfields[$i].' = "Total"';
								$inselect .= $wgpfields[$i].',';
								if($i != 0){
									$grouprollupselect .= "IFNULL(".$wgpfields[$i].", 'Total')  as ".$wgpfields[$i].",";
									$grouprollup .= ','.$wgpfields1[$i].'';
								}
							}
							else if($i>$_GET['summaryrollup']){ 
								
								if(($i-$_GET['summaryrollup']) == 1){
									$inselwhere  .= ')';
									$whererollup .= ' )';
									$rolcheck = 1;
								}
								$whererollup .= ' AND ('.$wgpfields[$i].' != "removetotal" ) ';
								$inselect .= "(CASE WHEN ".$wgpfields[$i]." IS NULL and ".$inselwhere." THEN 'Total' WHEN ".$wgpfields[$i]." IS NULL THEN 'removetotal'  ELSE ".$wgpfields[$i]." END) as  ".$wgpfields[$i].",";
								$grouprollupselect .= $wgpfields[$i].",";
								$grouprollup .= ','.$wgpfields1[$i].'';
							}
						}
					}
					if($rollup != '' && $rolcheck == 0){
						$whererollup .= ')';
					}
				}
			}
			if($_GET['reportingmode'] == '0' || $_GET['reportingmode'] == ''  ) {
				//$rollup="";
				if($rollup == ''){
					$query = 'SELECT SQL_CALC_FOUND_ROWS '.$groupselectstmtwotbl.' '.$selectmaintablewotbl.' '. $selectstmt['Selectstmtwithouttbl'].' from (select  '.$groupselectstmt.' '.$selectmaintable.' '.$selectstmt['Selectstmt'].' '.$havingSelect.' from '.$maintable.' '.$join_stmt.' WHERE '.$status.' AND '.$industry.' AND '.$whereString.' '.$filterwherecondtion.' AND '.$datewh.' '.$reportfilteraccountidcond.' '. $grouporder['groupbyquery'].$rollup.' '.$havingString .' ) AS t ';
				
				}else{
					$query = 'SELECT SQL_CALC_FOUND_ROWS '.$inselect.' '.$selectmaintablewotbl.' '.$selectstmt['selectforoutsideonlycol'].' from (SELECT  '.$grouprollupselect.' '.$selectmaintablewotbl.' '. $selectstmt['selectforrollup'].' from (select  '.$groupselectstmt.' '.$selectmaintable.' '.$selectstmt['Selectstmt'].' '.$havingSelect.' from '.$maintable.' '.$join_stmt.' WHERE '.$status.' AND '.$industry.' AND '.$whereString.' '.$filterwherecondtion.' AND '.$datewh.' '.$reportfilteraccountidcond.' '. $grouporder['groupbyquery'].' '.$havingString .' ) AS t  '. $grouprollup.' '.$rollup.' ) AS t '.$whererollup.'';
				}
				
			} else {
				$transfertagwhere = "";
				$transfertagwhere = "";
				$salesid = "";
				$itemtagid = "";
				$mode = $_GET['reportingmode'];
				$whereclauses = '';
				if($transferyesno == 'no') {
					$transfertagwhere = "and itemtag.tagtypeid not in (14,16) ";
					$partialtagselectgwt = " COALESCE(sum(salesdetail.grossweight),0) ";
					$partialtagselectnwt = " COALESCE(sum(salesdetail.netweight),0) ";
					$pagwt = "- t.outpartialgrossweight ";
					$panwt = "- t.outpartialnetweight ";
					$supagwt = " - sum(outpartialgrossweight)";
					$supanwt = " - sum(outpartialnetweight)";
				}else{
					$partialtagselectgwt = " COALESCE(sum(itemtag.grossweight),0) ";
					$partialtagselectnwt = " COALESCE(sum(itemtag.netweight),0) ";
					$pagwt = "";
					$panwt = "";
					$supagwt = "";
					$supanwt = "";
				}
				/** Stock REport - Date separation done by Kumaresan **/
				if($checkstockreportdatefield == 1 && $datevalueforstockreports != '') {
					$datesplitbytwo = explode(" AND ",$datevalueforstockreports);
					$stockreportstartdate = explode("'",$datesplitbytwo[0]);
					$startdate = $stockreportstartdate[1];
					if(count($datesplitbytwo) > 1) {
						if($datesplitbytwo[1] != '') {
							$stockreportenddate = explode("'",$datesplitbytwo[1]);
							$enddate = $stockreportenddate[1];
						}
					} else {
						$enddate = $startdate;
					}
				}
				/** Stock REport - Date separation done by Kumaresan **/
				if($filterwherecondtion != '') {
					if (strpos($filterwherecondtion, 'stockdate') !== false) {
						$splitfilterwherecondition = $this->removedatefromwherecondition($filterwherecondtion);
						$startdate = $splitfilterwherecondition[0];
						$enddate = $splitfilterwherecondition[1];
						$filterwherecondtion = $splitfilterwherecondition[2];
					}
				}
				$mainquery = $this->stockreportquerygeneratefunction($startdate,$enddate,$datewh,$filterwherecondtion,$whereString,$grouporder['groupbyquery'],$join_stmt,$transfertagwhere,$partialtagselectgwt,$partialtagselectnwt,$pagwt,$panwt,$supagwt,$supanwt,$salesid,$itemtagid,$mode,$whereclauses);
				if($rollup == ''){
					$query = 'SELECT SQL_CALC_FOUND_ROWS '.$groupselectstmtwotbl.' '.$selectmaintablewotbl.' '. $selectstmt['Selectstmtwithouttbl'].' from (select  '.$groupselectstmt.' '.$selectmaintable.' '.$selectstmt['Selectstmt'].' '.$havingSelect.' from ('.$mainquery.') as '.$maintable.' '.$join_stmt.' WHERE '.$status.' AND '.$industry.' AND '.$whereString.' '.$filterwherecondtion. $grouporder['groupbyquery'].$rollup.' '.$havingString .' ) AS t ';
				}else{
					$query = 'SELECT SQL_CALC_FOUND_ROWS '.$inselect.' '.$selectmaintablewotbl.' '.$selectstmt['selectforoutsideonlycol'].' from (SELECT  '.$grouprollupselect.' '.$selectmaintablewotbl.' '. $selectstmt['selectforrollup'].' from (select  '.$groupselectstmt.' '.$selectmaintable.' '.$selectstmt['Selectstmt'].' '.$havingSelect.' from ('.$mainquery.') as '. $maintable.' '.$join_stmt.' WHERE '.$status.' AND '.$industry.' AND '.$whereString.' '.$filterwherecondtion.' '. $grouporder['groupbyquery'].' '.$havingString .' ) AS t  '. $grouprollup.' '.$rollup.' ) AS t '.$whererollup.'';
					
				}
				
			}
		}
		$data = $this->db->query($query);
		//$rdata = $this->db->query($cquery);
		if($reporttype=='TV') {
			if($_GET['summarviewcolids'] != '') {
				$this->db->query($query1);
			}
		}
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		{// 6 Level Grouping Creation groupbyrange
			$countofgrpcol=count($grouporder['groupcol']);
			$groupcolsummaryids=$grouporder['groupbycolids'];
			$groupcolsummarycolumns=$grouporder['groupcol'];
		}
		//
		if($filetypename == 'html') {
			$html_file_name = 'htmlreports/'.trim('report').'_'.date("YmdHis").mt_rand(1,999999).'.html';
		} else {
			$html_file_name ='';
		}
		$finalresult=array($colinfo,$data,$page,'',$headcolids,$viewmodid,$creationid,$groupcolsummaryids,$groupcolsummarycolumns,$html_file_name);
		if($filetypename == 'html') {
			$this->htmldatagenerateinreprotfile($reportname,$finalresult,$fconditonid,$filtercondition,$filtervalue,$startdate,$enddate,$dateshow);
		}
		return $finalresult;
    }
	// Remove date details from Where Condition - Specially for Stock Reports
	public function removedatefromwherecondition($filterwherecondtion) {
		$startdate = '';
		$enddate = '';
		$conditionstring = '';
		if($filterwherecondtion != '') {
			$splitvalue = explode("AND",$filterwherecondtion);
			for($i = 0; $i< count($splitvalue); $i++) {
				if(trim($splitvalue[$i]) != '') {
					if(strpos($splitvalue[$i], 'stock.stockdate') !== false) {
						$stockreportstartdate = explode("'",$splitvalue[$i]);
						$startdate = $stockreportstartdate[1];
					} else if(strpos($splitvalue[$i], 'stockdate <') !== false) {
						$stockreportenddate = explode("'",$splitvalue[$i]);
						$enddate = $stockreportenddate[1];
					} else {
						$conditionstring .= 'AND '.$splitvalue[$i];
					}
				}
			}
		}
		$result = array($startdate,$enddate,$conditionstring);
		return $result;
	}
	//stockreport main query generate
	public function stockreportquerygeneratefunction($startdate,$enddate,$datewh,$filterwherecondtion,$whereString,$grouporder,$join_stmt,$transfertagwhere,$partialtagselectgwt,$partialtagselectnwt,$pagwt,$panwt,$supagwt,$supanwt,$salesid,$itemtagid,$mode,$whereclauses) {
	$havingwhere = '';
	$modefourtagcounterwhere = '';
	$modefoursalescounterwhere = '';
	$modefourcumcounterwhere = '';
	$tagwtselect = '';
	$taginwtselect = '';
	$tagoutwtselect = '';
	if($mode == 1){
		$tagwhere = ' itemtag.tagdate < "'.$startdate.'" '.$transfertagwhere.'';
		$saleswhere = ' sales.salesdate < "'.$startdate.'"';
		$cummulativewhere = ' and cummulativestock.stockdate < "'.$startdate.'"';
	}else if($mode == 2){
		$tagwhere = ' itemtag.itemtagid in ('.$itemtagid.')';
		$saleswhere = ' sales.salesid in ('.$salesid.')';
		$cummulativewhere = '';
		$havingwhere = 'having stockreporttypeid not in (2,4,5) ';
	}else if($mode == 3){ //
		$counter_license = $this->Basefunctions->get_company_settings('counter');
		$tagcounterwhere = ' 1=1 ';		   
		$counterwhere = '1=1';
		$salescounterwhere = '1=1';
		if($counter_license == 'YES') {
			$tagcounterwhere = '(itemtag.counterid = "'.$whereclauses['counterid'].'" or itemtag.fromcounterid = "'.$whereclauses['counterid'].'" or itemtag.stockincounterid	 = "'.$whereclauses['counterid'].'")';
			$salescounterwhere = '(salesdetail.counterid = "'.$whereclauses['counterid'].'" or salesdetail.processcounterid = "'.$whereclauses['counterid'].'")';
		}/*  else {
			$tagcounterwhere = '(itemtag.counterid = "11" or itemtag.fromcounterid = "11" or itemtag.stockincounterid	 = "11")';
			$salescounterwhere = '(salesdetail.counterid = "11" or salesdetail.processcounterid = "11")';
		} */
		$tagwhere = '(itemtag.productid = "'.$whereclauses['productid'].'" or itemtag.processproductid = "'.$whereclauses['productid'].'") and itemtag.purityid = "'.$whereclauses['purityid'].'" and itemtag.branchid = "'.$whereclauses['branchid'].'" and '.$tagcounterwhere.' and itemtag.tagtypeid not in (2,4,5,14)';
		
		$saleswhere = '(salesdetail.productid = "'.$whereclauses['productid'].'" or salesdetail.processproductid = "'.$whereclauses['productid'].'") and salesdetail.purityid = "'.$whereclauses['purityid'].'" and sales.branchid = "'.$whereclauses['branchid'].'" and '.$salescounterwhere.' and salesdetail.stocktypeid not in (11,26,27,28,29,31,37,38,39,57,62,66,74,75,76,78,79,82,83,84)';
		if($whereclauses['stockreporttypeid'] != ''){
			$havingwhere = 'having stockreporttypeid = "'.$whereclauses['stockreporttypeid'].'" ';
		}
		if($whereclauses['salesid'] != ''){
			$saleswhere .= 'and  sales.salesid  != "'.$whereclauses['salesid'].'" ';
		}
		$cummulativewhere = '';
	}else if($mode == 4){ //Storage Management
		
		$tagwhere = ' itemtag.tagdate < "'.$startdate.'" '.$transfertagwhere.'';
		$saleswhere = ' sales.salesdate < "'.$startdate.'"';
		$cummulativewhere = ' and cummulativestock.stockdate < "'.$startdate.'"';
		$tagwtselect = '+ t.intagwt - t.outtagwt';
		$taginwtselect = '+ t.intagwt ';
		$tagoutwtselect = '+ t.outtagwt';
		$modefourtagcounterwhere = '';
		$modefoursalescounterwhere = '';
		$modefourcumcounterwhere = '';
		$modefourcountermainquery = '';
		if(isset($whereclauses['counterid'])){
			if($whereclauses['counterid'] != ''){
				$modefourtagcounterwhere = 'and (itemtag.counterid = "'.$whereclauses['counterid'].'" or itemtag.fromcounterid = "'.$whereclauses['counterid'].'" or itemtag.stockincounterid	 = "'.$whereclauses['counterid'].'") ';
				$modefoursalescounterwhere = 'and (salesdetail.counterid = "'.$whereclauses['counterid'].'" or salesdetail.processcounterid = "'.$whereclauses['counterid'].'") ';
				$modefourcumcounterwhere = 'and (cummulativestock.counterid = "'.$whereclauses['counterid'].'") ';
				$modefourcountermainquery = 'where stock.counterid = '.$whereclauses['counterid'].' ';
			}
		}
	}
	//opening query generate
	$opendingquery = '
	select t.industryid,t.primid as stockid,t.stockdate, t.branchid, t.accountid, t.purityid, t.productid, t.counterid, t.stockreporttypeid, sum(t.ingrossweight - t.outgrossweight '.$tagwtselect.' '.$pagwt.') as openinggrossweight, 0 as ingrossweight, 0 as outgrossweight, 0 as closinggrossweight, sum(t.innetweight - t.outnetweight '.$tagwtselect.' '.$panwt.') as openingnetweight, 0 as innetweight, 0 as outnetweight, 0 as closingnetweight, sum(t.inpieces - t.outpieces) as openingpieces, 0 as inpieces, 0 as outpieces ,0 as closingpieces,sum(t.initemrate - t.outitemrate) as openingitemrate, 0 as initemrate, 0 as outitemrate ,0 as closingitemrate,0 as outpartialgrossweight,0 as outpartialnetweight,sum(t.intagwt - t.outtagwt) as openingtagweight,0 as intagwt,0 as outtagwt,0 as closingtagweight,SUM(t.indiacaratweight - t.outdiacaratweight) as openingdiacaratweight, 0 as indiacaratweight, 0 as outdiacaratweight, 0 as closingdiacaratweight,SUM(t.indiapieces - t.outdiapieces) as openingdiapieces, 0 as indiapieces, 0 as outdiapieces, 0 as closingdiapieces, 1 as status  from 
	(
	
	select  cummulativestock.industryid,cummulativestockid as primid,cummulativestock.stockdate,cummulativestock.branchid,cummulativestock.accountid,cummulativestock.purityid,cummulativestock.productid,cummulativestock.counterid,cummulativestock.stockreporttypeid ,
	COALESCE(sum(cummulativestock.ingrossweight),0) as ingrossweight,
	COALESCE(sum(cummulativestock.innetweight),0) as innetweight,
	COALESCE(sum(cummulativestock.inpieces),0) as inpieces,
	COALESCE(sum(cummulativestock.initemrate),0) as initemrate,
	COALESCE(sum(cummulativestock.indiacaratweight),0) as indiacaratweight,
	COALESCE(sum(cummulativestock.indiapieces),0) as indiapieces,
	COALESCE(sum(cummulativestock.outgrossweight),0) as outgrossweight,
	COALESCE(sum(cummulativestock.outnetweight),0) as outnetweight,
	COALESCE(sum(cummulativestock.outpieces),0) as outpieces,
	COALESCE(sum(cummulativestock.outitemrate),0) as outitemrate,
	COALESCE(sum(cummulativestock.outdiacaratweight),0) as outdiacaratweight,
	COALESCE(sum(cummulativestock.outdiapieces),0) as outdiapieces,
	0 as outpartialgrossweight,
	0 as outpartialnetweight,
	0 as intagwt,
	0 as outtagwt
	FROM cummulativestock
	WHERE cummulativestock.status not in (0,3)  '.$cummulativewhere.' '.$modefourcumcounterwhere.'
	group by cummulativestock.cummulativestockid
	 '.$havingwhere.' 
	
	UNION  all
	 
	select  itemtag.industryid,itemtag.itemtagid as primid,itemtag.tagdate as stockdate,itemtag.branchid,
	(CASE WHEN itemtag.accountid NOT IN (1) THEN itemtag.accountid ELSE "" END) as accountid,itemtag.purityid,
	(CASE WHEN itemtag.tagtypeid in (2,3,4,5,14,16,18,13) THEN productid WHEN itemtag.tagtypeid in (11) THEN itemtag.processproductid ELSE 1 END) as  productid,
	(CASE WHEN itemtag.tagtypeid in (2,4,5,18) THEN itemtag.stockincounterid WHEN itemtag.tagtypeid in (3,11,13,14,16) THEN counterid ELSE 1 END) as  counterid,
	(CASE 
	 WHEN itemtag.tagtypeid in (2) THEN 2 
	 WHEN itemtag.tagtypeid in (4) THEN 4 
	 WHEN itemtag.tagtypeid in (5) THEN 5 
	 WHEN itemtag.tagtypeid in (18) THEN 18
	 WHEN itemtag.tagtypeid in (3,16) THEN 3 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (2) THEN 2 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (5) THEN 5 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (6) THEN 4 
	 WHEN itemtag.tagtypeid = 13 and  itemtag.tagentrytypeid in (2,3,5,6) THEN 14 
	 WHEN itemtag.tagtypeid = 11 and  itemtag.tagentrytypeid in (2,3,5,6) THEN 6 ELSE 1 END) as  stockreporttypeid ,
	 COALESCE(itemtag.grossweight,0) as ingrossweight,
	 COALESCE(itemtag.netweight,0) as innetweight,
	 COALESCE(itemtag.pieces,0) as inpieces,
	 COALESCE(itemtag.itemrate,0) as initemrate,
	 COALESCE(sum(stoneentry.caratweight),0) as indiacaratweight,
	 COALESCE(sum(stoneentry.pieces),0) as indiapieces,
	 0 as outgrossweight,
	 0 as outnetweight,
	 0 as outpieces,
	 0 as outitemrate,
	 0 as outdiacaratweight,
	 0 as outdiapieces,
	 0 as outpartialgrossweight,
	 0 as outpartialnetweight,
	 COALESCE(itemtag.tagweight,0) as intagwt,
	 0 as outtagwt
	 FROM itemtag
	 LEFT OUTER JOIN stoneentry ON stoneentry.itemtagid = itemtag.itemtagid AND stoneentry.stonetypeid = 2 AND stoneentry.status NOT IN (0,3)
	 WHERE itemtag.status not in (0,3) and '.$tagwhere.' '.$modefourtagcounterwhere.'
	 group by itemtag.itemtagid
	 '.$havingwhere.' 	
	 UNION  all
	
	select itemtag.industryid,itemtag.itemtagid as primid,itemtag.tagdate as stockdate,itemtag.branchid,
	(CASE WHEN itemtag.accountid NOT IN (1) THEN itemtag.accountid ELSE "" END) as accountid,itemtag.purityid,
	(CASE WHEN itemtag.tagentrytypeid in (2,3,5,6,7,13) THEN productid WHEN itemtag.tagentrytypeid in (4,10,11,12) THEN itemtag.processproductid ELSE 1 END) as  productid,
	(CASE WHEN itemtag.tagentrytypeid in (7) THEN itemtag.stockincounterid  ELSE fromcounterid END) as  counterid, 
	(CASE 
	WHEN itemtag.tagentrytypeid = 2  THEN 2
	WHEN itemtag.tagentrytypeid = 7  THEN 18
	WHEN itemtag.tagentrytypeid = 3  THEN 3 
	WHEN itemtag.tagentrytypeid = 4  THEN 16
	WHEN itemtag.tagentrytypeid = 5  THEN 5
	WHEN itemtag.tagentrytypeid = 6  THEN 4
	WHEN itemtag.tagentrytypeid = 10  THEN 7
	WHEN itemtag.tagentrytypeid = 11  THEN 6
	WHEN itemtag.tagentrytypeid = 12  THEN 8
	WHEN itemtag.tagentrytypeid = 13  THEN 14
	ELSE 1 END) as  stockreporttypeid ,
	0 as ingrossweight,
    0 as innetweight,
    0 as inpieces,
	0 as initemrate,
	0 as indiacaratweight,
	0 as indiapieces,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.grossweight,0) END) as outgrossweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.netweight,0) END) as outnetweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.pieces,0) END) as outpieces,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.itemrate,0) END) as outitemrate,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(sum(stoneentry.caratweight),0) END) as outdiacaratweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(sum(stoneentry.pieces),0) END) as outdiapieces,
	0 as outpartialgrossweight,
	0 as outpartialnetweight,
	0 as intagwt,
	COALESCE(itemtag.tagweight,0) as outtagwt
	FROM itemtag
	LEFT OUTER JOIN stoneentry ON stoneentry.itemtagid = itemtag.itemtagid AND stoneentry.stonetypeid = 2 AND stoneentry.status NOT IN (0,3)
	WHERE itemtag.status not in (0,3) and '.$tagwhere.' '.$modefourtagcounterwhere.'
	group by itemtag.itemtagid
	 '.$havingwhere.' 
	 UNION  all

	select sales.industryid,salesdetail.salesdetailid as primid ,sales.salesdate as stockdate,sales.branchid,
	(CASE WHEN itemtag.accountid NOT IN (1) THEN itemtag.accountid ELSE "" END) as accountid,salesdetail.purityid,
	(CASE WHEN salesdetail.stocktypeid in (17,19,20,24,49,62,63,65,66,80,81) THEN salesdetail.productid WHEN salesdetail.stocktypeid in (13) THEN salesdetail.processproductid ELSE 1 END) as  productid,
	(CASE WHEN salesdetail.stocktypeid in (17,19,20,24,49,65,66,80,81,62,63) THEN salesdetail.counterid WHEN salesdetail.stocktypeid in (13) THEN salesdetail.processcounterid ELSE 1 END) as  counterid,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN 8
	 WHEN salesdetail.stocktypeid in (19) THEN 6
	 WHEN salesdetail.stocktypeid in (20) THEN 7
	 WHEN salesdetail.stocktypeid in (17,80,81) THEN 16
	 WHEN salesdetail.stocktypeid in (24) THEN 9
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 2 THEN 2
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 5 THEN 5
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 4 THEN 4 
	 WHEN salesdetail.stocktypeid in (63) THEN 11
	 WHEN salesdetail.stocktypeid in (66) THEN 3
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (2) THEN 16 
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (3) THEN 6
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid > 10 THEN 3
	 ELSE 1 END) as  stockreporttypeid ,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.grossweight - salesdetail.grossweight,0)   WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.grossweight,0) END) as ingrossweight,
	 (CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.netweight - salesdetail.netweight,0) WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.netweight,0) END) as innetweight,
	 (CASE  WHEN salesdetail.stocktypeid in (13) THEN 1 WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.pieces,0) END) as inpieces,
	 (CASE  WHEN salesdetail.stocktypeid in (20,62,65) THEN COALESCE(itemtag.itemrate,0) ELSE 0 END) as initemrate,
	 0 as indiacaratweight,
	 0 as indiapieces,
	 0 as outgrossweight,
	 0 as outnetweight,
	 0 as outpieces,
	 0 as outitemrate,
	 0 as outdiacaratweight,
	 0 as outdiapieces,
	 0 as outpartialgrossweight,
	 0 as outpartialnetweight,
	 COALESCE(itemtag.tagweight,0) as intagwt,
	 0 as outtagwt
	 
	 FROM salesdetail
	 join sales on sales.salesid = salesdetail.salesid
	 join product on product.productid = salesdetail.productid
	 join itemtag on itemtag.itemtagid = salesdetail.itemtagid
	 join companysetting on companysetting.companysettingid = 1
	 WHERE salesdetail.status not in (0,3) and sales.status not in (0,3) and sales.salestransactiontypeid != 1 and sales.salestransactiontypeid != 16 and salesdetail.stocktypeid in (19,20,17,80,81,24,62,63,65,66,13,49) and '.$saleswhere.' '.$modefoursalescounterwhere.'
	  group by salesdetail.salesdetailid
	  '.$havingwhere.' 
	 UNION  all
	 
	select sales.industryid,salesdetail.salesdetailid as primid ,sales.salesdate as stockdate,sales.branchid,
	(CASE WHEN itemtag.accountid NOT IN (1) THEN itemtag.accountid ELSE "" END) as accountid,salesdetail.purityid,
	(CASE WHEN salesdetail.stocktypeid in (11,12,13,25,62,63,65,66,49,73) THEN salesdetail.productid  ELSE 1 END) as  productid,
	(CASE WHEN salesdetail.stocktypeid in (11,12,13,25,62,63,49,73,65,66) THEN salesdetail.counterid  ELSE 1 END) as  counterid,
	(CASE 
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 2 THEN 2
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 4 THEN 4
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 5 THEN 5
	 WHEN salesdetail.stocktypeid in (12,63) THEN 3 
	 WHEN salesdetail.stocktypeid in (66) THEN 10
	 WHEN salesdetail.stocktypeid in (65) THEN 11
	 WHEN salesdetail.stocktypeid in (25) THEN 9
	 WHEN salesdetail.stocktypeid in (73) THEN 14
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (2) THEN 16 
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (3) THEN 6
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid > 10 THEN 3
	 ELSE 1 END) as  stockreporttypeid ,
	 0 as ingrossweight,
	 0 as innetweight,
	 0 as inpieces,
	 0 as initemrate,
	 0 as indiacaratweight,
	 0 as indiapieces,
	(CASE WHEN salesdetail.stocktypeid in (13) THEN '.$partialtagselectgwt.'  WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0 ELSE  COALESCE(salesdetail.grossweight,0) END) as outgrossweight,
    (CASE  WHEN salesdetail.stocktypeid in (13) THEN '.$partialtagselectnwt.'  WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0  ELSE   COALESCE(salesdetail.netweight,0) END) as outnetweight,
    (CASE  WHEN salesdetail.stocktypeid in (13) THEN 1 WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0  ELSE COALESCE(salesdetail.pieces,0) END) as outpieces,
    (CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(itemtag.itemrate,0) ELSE 0 END) as outitemrate,
	(CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(sum(salesstoneentry.caratweight),0) ELSE 0 END) as outdiacaratweight,
	(CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(sum(salesstoneentry.pieces),0) ELSE 0 END) as outdiapieces,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.grossweight - salesdetail.grossweight,0)  ELSE 0 END) as outpartialgrossweight,
	 (CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.netweight - salesdetail.netweight,0) ELSE 0 END) as outpartialnetweight,
	 0 as intagwt,
	 COALESCE(itemtag.tagweight,0) as outtagwt
	 FROM salesdetail
	 join sales on sales.salesid = salesdetail.salesid
	 join itemtag on itemtag.itemtagid = salesdetail.itemtagid
	 join product on product.productid = salesdetail.productid
	 LEFT OUTER JOIN salesstoneentry ON salesstoneentry.salesdetailid = salesdetail.salesdetailid AND stonetypeid = 2 AND salesstoneentry.status NOT IN (0,3)
	 WHERE salesdetail.status not in (0,3) and sales.status not in (0,3) and sales.salestransactiontypeid != 1 and sales.salestransactiontypeid != 16 and salesdetail.stocktypeid in (11,13,12,25,62,63,65,66,49,73)  and '.$saleswhere.' '.$modefoursalescounterwhere.' 
	  group by salesdetail.salesdetailid
	  '.$havingwhere.' 
	 ) as t
	 group by branchid,accountid,purityid,productid,counterid,stockreporttypeid
	';
	//other detail fetch
	if($mode == 1 || $mode == 4){
	$otherquery = ' 

	select t.industryid,t.primid as stockid,t.stockdate, t.branchid, t.accountid, t.purityid, t.productid, t.counterid, t.stockreporttypeid, 0 as openinggrossweight, sum(ingrossweight '.$taginwtselect.') as ingrossweight, sum(outgrossweight '.$tagoutwtselect.') as outgrossweight,0 as closinggrossweight, 0 as openingnetweight, sum(innetweight '.$taginwtselect.') as innetweight, sum(outnetweight '.$tagoutwtselect.') as outnetweight, 0 as closingnetweight, 0 as openingpieces,sum(inpieces) as inpieces, sum(outpieces) as outpieces, 0 as closingpieces,0 as openingitemrate,sum(initemrate) as initemrate, sum(outitemrate) as outitemrate, 0 as closingitemrate, sum(t.outpartialgrossweight) as outpartialgrossweight ,sum(t.outpartialnetweight) as outpartialnetweight,0 as openingtagweight,sum(t.intagwt) as intagwt,sum(t.outtagwt) as outtagwt,0 as closingtagweight, 0 as openingdiacaratweight, SUM(indiacaratweight) as indiacaratweight, SUM(outdiacaratweight) as outdiacaratweight, 0 as closingdiacaratweight, 0 as openingdiapieces, SUM(indiapieces) as indiapieces, SUM(outdiapieces) as outdiapieces, 0 as closingdiapieces, 1 as status from 
	(
	
	select  cummulativestock.industryid,cummulativestockid as primid,cummulativestock.stockdate,cummulativestock.branchid,cummulativestock.accountid,cummulativestock.purityid,cummulativestock.productid,cummulativestock.counterid,cummulativestock.stockreporttypeid ,
	COALESCE(sum(cummulativestock.ingrossweight),0) as ingrossweight,
	COALESCE(sum(cummulativestock.innetweight),0) as innetweight,
	COALESCE(sum(cummulativestock.inpieces),0) as inpieces,
	COALESCE(sum(cummulativestock.initemrate),0) as initemrate,
	COALESCE(sum(cummulativestock.indiacaratweight),0) as indiacaratweight,
	COALESCE(sum(cummulativestock.indiapieces),0) as indiapieces,
	COALESCE(sum(cummulativestock.outgrossweight),0) as outgrossweight,
	COALESCE(sum(cummulativestock.outnetweight),0) as outnetweight,
	COALESCE(sum(cummulativestock.outpieces),0) as outpieces,
	COALESCE(sum(cummulativestock.outitemrate),0) as outitemrate,
	COALESCE(sum(cummulativestock.outdiacaratweight),0) as outdiacaratweight,
	COALESCE(sum(cummulativestock.outdiapieces),0) as outdiapieces,
	0 as outpartialgrossweight,
	0 as outpartialnetweight,
	0 as intagwt,
	0 as outtagwt
	FROM cummulativestock
	WHERE cummulativestock.status not in (0,3) and cummulativestock.stockdate >="'.$startdate.'" AND cummulativestock.stockdate <= "'.$enddate.'" '.$modefourcumcounterwhere.'
	group by cummulativestock.cummulativestockid
	
	UNION  all
	
	select  itemtag.industryid,itemtag.itemtagid as primid,itemtag.tagdate as stockdate,itemtag.branchid,
	(CASE WHEN itemtag.accountid NOT IN (1) THEN itemtag.accountid ELSE "" END) as accountid,itemtag.purityid,
	(CASE WHEN itemtag.tagtypeid in (2,3,4,5,14,16,18,13) THEN productid WHEN itemtag.tagtypeid in (11) THEN itemtag.processproductid ELSE 1 END) as  productid,
	(CASE WHEN itemtag.tagtypeid in (2,4,5,18) THEN itemtag.stockincounterid WHEN itemtag.tagtypeid in (3,11,13,14,16) THEN counterid ELSE 1 END) as  counterid,
	(CASE 
	 WHEN itemtag.tagtypeid in (2) THEN 2 
	 WHEN itemtag.tagtypeid in (4) THEN 4 
	 WHEN itemtag.tagtypeid in (5) THEN 5 
	 WHEN itemtag.tagtypeid in (18) THEN 18
	 WHEN itemtag.tagtypeid in (3,16) THEN 3 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (2) THEN 2 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (5) THEN 5 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (6) THEN 4 
	 WHEN itemtag.tagtypeid = 13 and  itemtag.tagentrytypeid in (2,3,5,6) THEN 14 
	 WHEN itemtag.tagtypeid = 11 and  itemtag.tagentrytypeid in (2,3,5,6) THEN 6 ELSE 1 END) as  stockreporttypeid ,
	 COALESCE(itemtag.grossweight,0) as ingrossweight,
	 COALESCE(itemtag.netweight,0) as innetweight,
	 COALESCE(itemtag.pieces,0) as inpieces,
	 COALESCE(itemtag.itemrate,0) as initemrate,
	 COALESCE(sum(stoneentry.caratweight),0) as indiacaratweight,
	 COALESCE(sum(stoneentry.pieces),0) as indiapieces,
	 0 as outgrossweight,
	0 as outnetweight,
	0 as outpieces,
	0 as outitemrate,
	0 as outdiacaratweight,
	0 as outdiapieces,
	0 as outpartialgrossweight,
	0 as outpartialnetweight,
	COALESCE(itemtag.tagweight,0) as intagwt,
	0 as outtagwt
	 FROM itemtag
	 LEFT OUTER JOIN stoneentry ON stoneentry.itemtagid = itemtag.itemtagid AND stoneentry.stonetypeid = 2 AND stoneentry.status NOT IN (0,3)
	 WHERE itemtag.status not in (0,3) and itemtag.tagdate >="'.$startdate.'" AND itemtag.tagdate <= "'.$enddate.'" '.$transfertagwhere.' '.$modefourtagcounterwhere.'
	group by itemtag.itemtagid
		
	 UNION  all
	
	select itemtag.industryid,itemtag.itemtagid as primid,itemtag.tagdate as stockdate,itemtag.branchid,
	(CASE WHEN itemtag.accountid NOT IN (1) THEN itemtag.accountid ELSE "" END) as accountid,itemtag.purityid,
	(CASE WHEN itemtag.tagentrytypeid in (2,3,5,6,7,13) THEN productid WHEN itemtag.tagentrytypeid in (4,10,11,12) THEN itemtag.processproductid ELSE 1 END) as  productid,
	(CASE WHEN itemtag.tagentrytypeid in (7) THEN itemtag.stockincounterid  ELSE fromcounterid END) as  counterid, 
	(CASE 
	WHEN itemtag.tagentrytypeid = 2  THEN 2
	WHEN itemtag.tagentrytypeid = 7  THEN 18
	WHEN itemtag.tagentrytypeid = 3  THEN 3 
	WHEN itemtag.tagentrytypeid = 4  THEN 16
	WHEN itemtag.tagentrytypeid = 5  THEN 5
	WHEN itemtag.tagentrytypeid = 6  THEN 4
	WHEN itemtag.tagentrytypeid = 10  THEN 7
	WHEN itemtag.tagentrytypeid = 11  THEN 6
	WHEN itemtag.tagentrytypeid = 12  THEN 8
	WHEN itemtag.tagentrytypeid = 13  THEN 14
	ELSE 1 END) as  stockreporttypeid ,
	0 as ingrossweight,
    0 as innetweight,
    0 as inpieces,
	0 as initemrate,
	0 as indiacaratweight,
	0 as indiapieces,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.grossweight,0) END) as outgrossweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.netweight,0) END) as outnetweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.pieces,0) END) as outpieces,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.itemrate,0) END) as outitemrate,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(sum(stoneentry.caratweight),0) END) as outdiacaratweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(sum(stoneentry.pieces),0) END) as outdiapieces,
	0 as outpartialgrossweight,
	0 as outpartialnetweight,
	0 as intagwt,
	COALESCE(itemtag.tagweight,0) as outtagwt
	FROM itemtag
	LEFT OUTER JOIN stoneentry ON stoneentry.itemtagid = itemtag.itemtagid AND stoneentry.stonetypeid = 2 AND stoneentry.status NOT IN (0,3)
	WHERE itemtag.status not in (0,3) and itemtag.tagdate >="'.$startdate.'" AND itemtag.tagdate <= "'.$enddate.'" '.$transfertagwhere.' '.$modefourtagcounterwhere.'
	group by itemtag.itemtagid
	
	 UNION  all

	select sales.industryid,salesdetail.salesdetailid as primid ,sales.salesdate as stockdate,sales.branchid,
	(CASE WHEN itemtag.accountid NOT IN (1) THEN itemtag.accountid ELSE "" END) as accountid,salesdetail.purityid,
	(CASE WHEN salesdetail.stocktypeid in (17,19,20,24,49,62,63,65,66,80,81) THEN salesdetail.productid WHEN salesdetail.stocktypeid in (13) THEN salesdetail.processproductid ELSE 1 END) as  productid,
	(CASE WHEN salesdetail.stocktypeid in (17,19,20,24,49,65,66,80,81,62,63) THEN salesdetail.counterid WHEN salesdetail.stocktypeid in (13) THEN salesdetail.processcounterid ELSE 1 END) as  counterid,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN 8
	 WHEN salesdetail.stocktypeid in (19) THEN 6
	 WHEN salesdetail.stocktypeid in (20) THEN 7
	 WHEN salesdetail.stocktypeid in (17,80,81) THEN 16
	 WHEN salesdetail.stocktypeid in (24) THEN 9
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 2 THEN 2
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 5 THEN 5
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 4 THEN 4 
	 WHEN salesdetail.stocktypeid in (63) THEN 11
	 WHEN salesdetail.stocktypeid in (66) THEN 3
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (2) THEN 16 
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (3) THEN 6
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid > 10 THEN 3
	 ELSE 1 END) as  stockreporttypeid ,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.grossweight - salesdetail.grossweight,0)   WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.grossweight,0) END) as ingrossweight,
	 (CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.netweight - salesdetail.netweight,0) WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.netweight,0) END) as innetweight,
	 (CASE  WHEN salesdetail.stocktypeid in (13) THEN 1 WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.pieces,0) END) as inpieces,
	 (CASE  WHEN salesdetail.stocktypeid in (20,62,65) THEN COALESCE(itemtag.itemrate,0) ELSE 0 END) as initemrate,
	 0 as indiacaratweight,
	 0 as indiapieces,
	 0 as outgrossweight,
	 0 as outnetweight,
	 0 as outpieces,
	 0 as outitemrate,
	 0 as outdiacaratweight,
	 0 as outdiapieces,
	 0 as outpartialgrossweight,
	 0 as outpartialnetweight,
	 COALESCE(itemtag.tagweight,0) as intagwt,
	 0 as outtagwt
	 FROM salesdetail
	 join sales on sales.salesid = salesdetail.salesid
	 join itemtag on itemtag.itemtagid = salesdetail.itemtagid
	 join product on product.productid = salesdetail.productid
	 join companysetting on companysetting.companysettingid = 1
	 WHERE salesdetail.status not in (0,3) and sales.status not in (0,3) and sales.salestransactiontypeid != 1 and sales.salestransactiontypeid != 16 and salesdetail.stocktypeid in (19,20,17,80,81,24,62,63,65,66,13,49) and sales.salesdate >="'.$startdate.'" AND sales.salesdate <= "'.$enddate.'" '.$modefoursalescounterwhere.' 
	 group by salesdetail.salesdetailid
	 
	 UNION  all
	 
	select sales.industryid,salesdetail.salesdetailid as primid ,sales.salesdate as stockdate,sales.branchid,
	(CASE WHEN itemtag.accountid NOT IN (1) THEN itemtag.accountid ELSE "" END) as accountid,salesdetail.purityid,
	(CASE WHEN salesdetail.stocktypeid in (11,12,13,25,62,63,65,66,49,73) THEN salesdetail.productid  ELSE 1 END) as  productid,
	(CASE WHEN salesdetail.stocktypeid in (11,12,13,25,62,63,49,73,65,66) THEN salesdetail.counterid  ELSE 1 END) as  counterid,
	(CASE 
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 2 THEN 2
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 4 THEN 4
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 5 THEN 5
	 WHEN salesdetail.stocktypeid in (12,63) THEN 3 
	 WHEN salesdetail.stocktypeid in (66) THEN 10
	 WHEN salesdetail.stocktypeid in (65) THEN 11
	 WHEN salesdetail.stocktypeid in (25) THEN 9
	  WHEN salesdetail.stocktypeid in (73) THEN 14
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (2) THEN 16 
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (3) THEN 6
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid > 10 THEN 3
	 ELSE 1 END) as  stockreporttypeid ,
	 0 as ingrossweight,
	 0 as innetweight,
	 0 as inpieces,
	 0 as initemrate,
	 0 as indiacaratweight,
	 0 as indiapieces,
	(CASE WHEN salesdetail.stocktypeid in (13) THEN '.$partialtagselectgwt.'  WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0 ELSE  COALESCE(salesdetail.grossweight,0) END) as outgrossweight,
    (CASE  WHEN salesdetail.stocktypeid in (13) THEN '.$partialtagselectnwt.'  WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0  ELSE   COALESCE(salesdetail.netweight,0) END) as outnetweight,
    (CASE  WHEN salesdetail.stocktypeid in (13) THEN 1 WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0  ELSE COALESCE(salesdetail.pieces,0) END) as outpieces,
    (CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(itemtag.itemrate,0) ELSE 0 END) as outitemrate,
	(CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(sum(salesstoneentry.caratweight),0) ELSE 0 END) as outdiacaratweight,
	(CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(sum(salesstoneentry.pieces),0) ELSE 0 END) as outdiapieces,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.grossweight - salesdetail.grossweight,0)  ELSE 0 END) as outpartialgrossweight,
	 (CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.netweight - salesdetail.netweight,0) ELSE 0 END) as outpartialnetweight,
	 0 as intagwt,
	 COALESCE(itemtag.tagweight,0) as outtagwt
	 FROM salesdetail
	 join sales on sales.salesid = salesdetail.salesid
	 join itemtag on itemtag.itemtagid = salesdetail.itemtagid
	 join product on product.productid = salesdetail.productid
	 LEFT OUTER JOIN salesstoneentry ON salesstoneentry.salesdetailid = salesdetail.salesdetailid AND stonetypeid = 2 AND salesstoneentry.status NOT IN (0,3)
	 WHERE salesdetail.status not in (0,3) and sales.status not in (0,3) and sales.salestransactiontypeid != 1 and sales.salestransactiontypeid != 16 and salesdetail.stocktypeid in (11,13,12,25,62,63,65,66,49,73)  and sales.salesdate >="'.$startdate.'" AND sales.salesdate <= "'.$enddate.'" '.$modefoursalescounterwhere.' 
	 group by salesdetail.salesdetailid
	 
	 ) as t
	 group by branchid,accountid,purityid,productid,counterid,stockreporttypeid
	 ';
	 }
	  if($mode == 1) {
		$mainquery = 'select stock.stockid,stock.industryid,stock.stockdate, stock.branchid, stock.accountid, stock.purityid, stock.productid, stock.counterid, stock.stockreporttypeid, openinggrossweight, sum(ingrossweight) as ingrossweight, sum(outgrossweight) as outgrossweight,(sum(openinggrossweight) + sum(ingrossweight) - sum(outgrossweight) '.$supagwt.' ) as closinggrossweight,openingnetweight, sum(innetweight) as innetweight, sum(outnetweight) as outnetweight, (sum(openingnetweight) + sum(innetweight) - sum(outnetweight) '.$supanwt.' ) as closingnetweight, openingpieces,sum(inpieces) as inpieces, sum(outpieces) as outpieces, (sum(openingpieces) + sum(inpieces) - sum(outpieces)) as closingpieces , openingitemrate,sum(initemrate) as initemrate, sum(outitemrate) as outitemrate, (sum(openingitemrate) + sum(initemrate) - sum(outitemrate)) as closingitemrate,openingtagweight,sum(intagwt) as intagwt,sum(outtagwt) as outtagwt,(sum(openingtagweight) + sum(intagwt) - sum(outtagwt)) as closingtagweight, openingdiacaratweight, SUM(indiacaratweight) as indiacaratweight, SUM(outdiacaratweight) as outdiacaratweight, (SUM(openingdiacaratweight) + SUM(indiacaratweight) - SUM(outdiacaratweight)) as closingdiacaratweight, openingdiapieces, SUM(indiapieces) as  indiapieces, SUM(outdiapieces) as outdiapieces, (SUM(openingdiapieces) + SUM(indiapieces) - SUM(outdiapieces)) as closingdiapieces, stock.status from ('.$opendingquery.' UNION ALL '.$otherquery.') as stock '.$join_stmt.'GROUP BY branchid,accountid,purityid,productid,counterid,stockreporttypeid';
		return $mainquery;
	  } else if($mode == 2) {
		$mainquery = 'select stock.stockid,stock.industryid,stock.stockdate, stock.branchid, stock.accountid, stock.purityid, stock.productid, stock.counterid, stock.stockreporttypeid, openinggrossweight,openingnetweight, openingpieces,openingitemrate,stock.status from ('.$opendingquery.') as stock '.$join_stmt.'GROUP BY branchid,accountid,purityid,productid,counterid,stockreporttypeid';
	    return $mainquery;
	  } else if($mode == 3) {
		$quwhere = 'where stock.productid = "'.$whereclauses['productid'].'" and stock.counterid = "'.$whereclauses['counterid'].'" and stock.branchid = "'.$whereclauses['branchid'].'" and stock.purityid = "'.$whereclauses['purityid'].'"' ;
		$mainquery = 'select openinggrossweight,openingnetweight,openingpieces,openingitemrate from ('.$opendingquery.') as stock '.$join_stmt.' '.$quwhere.'GROUP BY branchid,accountid,purityid,productid,counterid,stockreporttypeid';
	    return $mainquery;
	 } else if($mode == 4) {
		$weightround =  $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2);
		$mainquery = 'select stock.stockid,stock.industryid,stock.stockdate, stock.branchid, stock.accountid, stock.purityid, stock.productid, stock.counterid, stock.stockreporttypeid, round((sum(openinggrossweight) + counterforstock.boxweight ),'.$weightround.') as openinggrossweight, round(sum(ingrossweight),'.$weightround.') as ingrossweight, round(sum(outgrossweight),'.$weightround.') as outgrossweight,round((sum(openinggrossweight) + sum(ingrossweight) - sum(outgrossweight) '.$supagwt.' + counterforstock.boxweight  + counterforstock.lasterrrorwt ),'.$weightround.') as closinggrossweight,round((sum(openingnetweight) + counterforstock.boxweight ),'.$weightround.') as openingnetweight,round(sum(innetweight),'.$weightround.') as innetweight,round(sum(outnetweight),'.$weightround.') as outnetweight, round((sum(openingnetweight) + sum(innetweight) - sum(outnetweight) '.$supanwt.' + counterforstock.boxweight),'.$weightround.')  as closingnetweight, round(sum(openingpieces),0) as openingpieces,round(sum(inpieces),0) as inpieces, round(sum(outpieces),0) as outpieces, round((sum(openingpieces) + sum(inpieces) - sum(outpieces) + counterforstock.lasterrorpieces),0) as closingpieces , sum(openingitemrate) as openingitemrate,sum(initemrate) as initemrate, sum(outitemrate) as outitemrate, (sum(openingitemrate) + sum(initemrate) - sum(outitemrate)) as closingitemrate,round(sum(openingtagweight),'.$weightround.') as openingtagweight,round(sum(intagwt),'.$weightround.') as intagwt,round(sum(outtagwt),'.$weightround.') as outtagwt,round((sum(openingtagweight) + sum(intagwt) - sum(outtagwt)),'.$weightround.') as closingtagweight,round(counterforstock.lasterrrorwt,'.$weightround.') as lasterrrorwt,round(counterforstock.boxweight,'.$weightround.') as boxweight,counterforstock.lasterrorpieces,round((sum(openinggrossweight) + sum(ingrossweight) - sum(outgrossweight) '.$supagwt.'  - (sum(openingtagweight) + sum(intagwt) - sum(outtagwt))),'.$weightround.')  as jewelsweight,stock.status from ('.$opendingquery.' UNION ALL '.$otherquery.') as stock  join counter as counterforstock on counterforstock.counterid = stock.counterid'.$join_stmt.'  '.$modefourcountermainquery.' GROUP BY stock.counterid';
	    return $mainquery;
	  }
	}	
	//html data generate and written in file
	public function htmldatagenerateinreprotfile($reportname,$result,$fconditonid,$filtercondition,$filtervalue,$startdate,$enddate,$dateshow) {
		$fieldhideshowids = $this->get_company_settings('reportscolumnfieldhideshow');
		$salessalesdetailid = explode(',',$fieldhideshowids);
		$today = date("d-m-Y");
		$today_time=date('g:i:s a');
		$htmlfile =  $result[9];
		$resultcoldata=$result[0];
		$groupbycolumns=$result[8];
		$moduleid=$result[5];
		$result=$result[1];				
		//titlec content
		$count = 0;
		$celtitle = $resultcoldata['colname'];
		$titlecount = count($celtitle);
		$dataname = $resultcoldata['colmodelname'];
		$uitype = $resultcoldata['colmodeluitype'];
		//print_r($uitype );
		//HEADER OF HTML REPORT
		$content = '';
		$content .= '<table style="width: 100%;table-layout: fixed;word-break: break-all;"><tbody style=""><tr><td style="border:1px solid rgb(0,0,0);width: 100%; text-align: center;"><strong>'.$reportname.'</strong><br></td></tr></tbody></table>';
		$content .= '<table style="width: 100%;table-layout: fixed;word-break: break-all;"><tbody style=""><tr><td style="border:1px solid rgb(0,0,0);width: 100%; text-align: center;"><strong>'.$dateshow.' ON '.$today.' '.$today_time.'</strong><br></td></tr></tbody></table>';
		//filter data display
		$filtercontent = $this->filtervalueget($fconditonid,$filtercondition,$filtervalue);
		$content .= $filtercontent;
		//COLUMNS OF THE REPORT
		$content .= '<table style="width: 100%;table-layout: fixed;word-break: break-all;"><tbody style=""><tr>';
		$width =  100/$titlecount;
		for($i=0;$i<($titlecount);++$i) {
			$headername = $celtitle[$i];
			$SUMtype =  explode('SUM ',$celtitle[$i]);
			if(isset($SUMtype[1])) {
				$headername = $SUMtype[1];
			}
			$MINtype =  explode('MIN ',$celtitle[$i]);
			if(isset($MINtype[1])) {
				$headername = $MINtype[1];
			}
			$AVGtype =  explode('AVG ',$celtitle[$i]);
			if(isset($AVGtype[1])) {
				$headername = $AVGtype[1];
			}
			$ACTUALtype =  explode('ACTUAL ',$celtitle[$i]);
			if(isset($ACTUALtype[1])) {
				$headername = $ACTUALtype[1];
			}
			if($uitype[$i] == 19) {
				$width = '12';
			} else {
				$width = '10';
			}
			if(in_array($resultcoldata['colid'][$i],$salessalesdetailid)) { } else {
				$content .= '<td style="border:1px solid rgb(0,0,0); text-align: center;width:'.$width.'%;"><strong>'.$headername.'</strong><br></td>';
			}
		}
		$content .= '</tr></tbody></table>';
		//BODY OF HTML REPORT
		$content .= '<table style="width: 100%;table-layout: fixed;word-break: break-all;"><tbody style="">';
		$g=1;
		$repeativearr=array();
		$recordcount = $result->num_rows();
		foreach($result->result() as $row) {
			$getcount=count($dataname);
			$content .= '<tr>';
			for($i=0;$i<$getcount;$i++) {
				if($uitype[$i] != 4 && $uitype[$i] != 5 && $uitype[$i] != 6 && $uitype[$i] != 11 && $uitype[$i] != 14) {
					$allign = 'left';
				} else {
					$allign = 'right';
				}
				if($uitype[$i] == 19) {
					$width = '12';
				} else {
					$width = '10';
				}
				$dname = $dataname[$i];
				if($i<count($groupbycolumns)){
					if(in_array($dname,$groupbycolumns)) {
						if($row->$dname == null){
							if($moduleid != 104) {
								$value = '';
							} else {
								$value = 'Total';
							}
						} else {
							if($i != 0 ) {
								$value = $row->$dname;
							} else {
								if(in_array($row->$dname,$repeativearr)) {
									$value = '';
								} else {
									$repeativearr[$i] = $row->$dname;
									$value = $row->$dname;
								}
							}
						}
					}
				} else {
					if($dname == 'SUMexcesspieces' || $dname == 'SUMrepieces' || $dname == 'SUMequalpieces') { //for product rol report
						$value = $row->$dname;
						if($value == 0) {
							$value = '';
						} else {
							$value = $value;
						}
					} else {
						$value = $row->$dname;
					}
				}
				if(in_array($resultcoldata['colid'][$i],$salessalesdetailid)) { } else {
					$content .= '<td style="border:1px solid rgb(0,0,0); text-align: '.$allign.';width:'.$width.'%;">'.$value.'<br></td>';
				}
			}
			$g++;
			$content .= '</tr>';
		}
		$content .= '</tbody></table>';
		@chmod($htmlfile,0766);
		@write_file($htmlfile,$content);
	}
	//html format filter value fetch
	public function filtervalueget($fconditonid,$filtercondition,$filtervalue) {
		$filtercontent = '';
		$dduitypeids = array('17','18','19','23','25','26','27','28','29');
		if(!empty($fconditonid)) {
			$filtercontent .= '<table style="width: 100%;"><tbody style="">';
			for($i=0;$i<count($fconditonid);$i++) {
				if($fconditonid[$i] != 'undefined' && $fconditonid[$i] != '') {
					$uitype = $this->generalinformaion('viewcreationcolumns','uitypeid','viewcreationcolumnid',$fconditonid[$i]);
					if(in_array($uitype,$dduitypeids)){
						$tablename = $this->generalinformaion('viewcreationcolumns','viewcreationcolmodeltable','viewcreationcolumnid',$fconditonid[$i]);
						$table =  explode(' as ',$tablename);
						if(isset($table[1])) {
							$tablename = $table[0];
						}
						$label = $this->generalinformaion('viewcreationcolumns','viewcreationcolumnname','viewcreationcolumnid',$fconditonid[$i]);
						$value = $this->fetchvalue($tablename,$tablename.'name',$tablename.'id',$filtervalue[$i]);
						$filtercontent .= '<tr><td style="width:15%;"><strong>'.$label.'</strong></td><td style="width:85%;">'.$value.'</td></tr>';
					} else {
						$label = $this->generalinformaion('viewcreationcolumns','viewcreationcolumnname','viewcreationcolumnid',$fconditonid[$i]);
						$filtercontent .= '<tr><td style="width:15%;"><strong>'.$label.'</strong></td><td style="width:85%;">'.$filtercondition[$i].'  '.$filtervalue[$i].'</td></tr>';
					}
				}
			}
			$filtercontent .= '</tbody></table>';
		}
		return $filtercontent;
	}
	//filter value fetch
	public function fetchvalue($tablename,$fieldname,$fieldid,$priamryid) {
		$priamryid = explode(',',$priamryid);
		$i=0;
		$data ='';
		$this->db->select($fieldname);
		$this->db->from($tablename);
		$this->db->where('status',1);
		$this->db->where_in($fieldid,$priamryid);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$value = $row->$fieldname;
				if($data == '') {
					$data .= $value;
				} else {
					$data .= ' , '.$value;
				}
			}
		}
		return $data;
	}
	//condition information fetch	
    public function newgenerateconditioninfo($reportconditiondataid,$reportconditiondata,$fconditonid) {
		$conditiongriddata=json_decode($reportconditiondata);
		$conditionids=explode(",",$reportconditiondataid);
		$i=0;
		$conditionarray = array();
		$havingarray = array();
		$conditioncolids = [];
		$havingcolids = []; 
		//user for get the condition values;
		if(COUNT(array_filter($conditionids))>0) {
			$this->db->select('viewcreationcolumns.viewcreationcolumnid,viewcreationcolumns.viewcreationcolmodeljointable,viewcreationcolumns.viewcreationcolmodelname,viewcreationcolumns.viewcreationcolmodelindexname,viewcreationjoincolmodelname,uitypeid',false);
			$this->db->from('viewcreationcolumns');
			$this->db->where_in('viewcreationcolumns.viewcreationcolumnid',$conditionids);
			$this->db->where_in('viewcreationcolumns.status',$this->Basefunctions->activestatus);
			$this->db->order_by("FIELD (viewcreationcolumnid,".$reportconditiondataid."),viewcreationcolumnid");
			$condtionval=$this->db->get();
			$j=0;
			foreach($condtionval->result() as $row) {
				if($row->viewcreationcolumnid != '4198') {
					if(empty($fconditonid)) {
						if($conditiongriddata[$j]->reportaggregate=='ACTUAL') {
							$conditionarray[$j] = array(
													'0'=>$row->viewcreationcolmodeljointable,
													'1'=>$row->viewcreationcolmodelindexname,
													'2'=>$conditiongriddata[$j]->reportcondition,
													'3'=>$conditiongriddata[$j]->finalreportcondvalueid,
													'4'=>$conditiongriddata[$j]->reportandorcond,
													'5'=>$row->viewcreationjoincolmodelname,
													'6'=>$row->uitypeid
												);
							$conditioncolids[$j] = $row->viewcreationcolumnid;
							$i++;
							$j++;
						} else {
							$havingarray[$j] = array(
													'0'=>$row->viewcreationcolmodeljointable,
													'1'=>$row->viewcreationcolmodelindexname,
													'2'=>$conditiongriddata[$j]->reportcondition,
													'3'=>$conditiongriddata[$j]->finalreportcondvalueid,
													'4'=>$conditiongriddata[$j]->reportandorcond,
													'5'=>$row->viewcreationjoincolmodelname,
													'6'=>$row->uitypeid,
													'7'=>$conditiongriddata[$j]->reportaggregate,
												);
							$havingcolids[$j] =$row->viewcreationcolumnid;
							$i++;
							$j++;
						}
					} else {
						if(COUNT($fconditonid)>0) {
							if(!in_array($row->viewcreationcolumnid, $fconditonid)) {
								if($conditiongriddata[$j]->reportaggregate=='ACTUAL') {
									$conditionarray[$j] = array(
															'0'=>$row->viewcreationcolmodeljointable,
															'1'=>$row->viewcreationcolmodelindexname,
															'2'=>$conditiongriddata[$j]->reportcondition,
															'3'=>$conditiongriddata[$j]->finalreportcondvalueid,
															'4'=>$conditiongriddata[$j]->reportandorcond,
															'5'=>$row->viewcreationjoincolmodelname,
															'6'=>$row->uitypeid
														);
									$conditioncolids[$j] = $row->viewcreationcolumnid;
									$i++;
									$j++;
								} else {
									$havingarray[$j] = array(
															'0'=>$row->viewcreationcolmodeljointable,
															'1'=>$row->viewcreationcolmodelindexname,
															'2'=>$conditiongriddata[$j]->reportcondition,
															'3'=>$conditiongriddata[$j]->finalreportcondvalueid,
															'4'=>$conditiongriddata[$j]->reportandorcond,
															'5'=>$row->viewcreationjoincolmodelname,
															'6'=>$row->uitypeid,
															'7'=>$conditiongriddata[$j]->reportaggregate,
														);
									$havingcolids[$j] =$row->viewcreationcolumnid;
									$i++;
									$j++;
								}
							}
						} else {
							if($conditiongriddata[$j]->reportaggregate=='ACTUAL') {
								$conditionarray[$j] = array(
														'0'=>$row->viewcreationcolmodeljointable,
														'1'=>$row->viewcreationcolmodelindexname,
														'2'=>$conditiongriddata[$j]->reportcondition,
														'3'=>$conditiongriddata[$j]->finalreportcondvalueid,
														'4'=>$conditiongriddata[$j]->reportandorcond,
														'5'=>$row->viewcreationjoincolmodelname,
														'6'=>$row->uitypeid
													);
								$conditioncolids[$j] = $row->viewcreationcolumnid;
								$i++;
								$j++;
							} else {
								$havingarray[$j] = array(
														'0'=>$row->viewcreationcolmodeljointable,
														'1'=>$row->viewcreationcolmodelindexname,
														'2'=>$conditiongriddata[$j]->reportcondition,
														'3'=>$conditiongriddata[$j]->finalreportcondvalueid,
														'4'=>$conditiongriddata[$j]->reportandorcond,
														'5'=>$row->viewcreationjoincolmodelname,
														'6'=>$row->uitypeid,
														'7'=>$conditiongriddata[$j]->reportaggregate,
													);
								$havingcolids[$j] =$row->viewcreationcolumnid;
								$i++;
								$j++;
							}
						}
					}
				}
			}
			$info=array(
					'conditionalarry'=>$conditionarray,
					'viewcolid'=>$conditioncolids,
					'havingarray'=>$havingarray,
					'havingcolids'=>$havingcolids
			);
		} else {
			$info=array(
				'conditionalarry'=>array(),
				'viewcolid'=>array(),
				'havingarray'=>array(),
				'havingcolids'=>array()
			);
		}
		return $info;
    }
	// Stock Reports - Stock Date Fetch
    public function newgenerateconditioninfoforstockdate($reportconditiondataid,$reportconditiondata) {
		$conditiongriddata=json_decode($reportconditiondata);
		$conditionids=explode(",",$reportconditiondataid);
		$i=0;
		$conditionarray = array();
		$havingarray = array();
		$conditioncolids = [];
		$havingcolids = []; 
		//user for get the condition values;
		if(COUNT(array_filter($conditionids))>0){
			$this->db->select('viewcreationcolumns.viewcreationcolumnid,viewcreationcolumns.viewcreationcolmodeljointable,viewcreationcolumns.viewcreationcolmodelname,viewcreationcolumns.viewcreationcolmodelindexname,viewcreationjoincolmodelname,uitypeid',false);
			$this->db->from('viewcreationcolumns');
			$this->db->where_in('viewcreationcolumns.viewcreationcolumnid',$conditionids);
			$this->db->where_in('viewcreationcolumns.status',$this->Basefunctions->activestatus);
			$this->db->order_by("FIELD (viewcreationcolumnid,".$reportconditiondataid."),viewcreationcolumnid");
			$condtionval=$this->db->get();
			$j=0;
			foreach($condtionval->result() as $row) {
				if($conditiongriddata[$j]->reportcondcolumnid == 4198) {
					if($conditiongriddata[$j]->reportaggregate=='ACTUAL') {
						$conditionarray[$j] = array(
												'0'=>$row->viewcreationcolmodeljointable,
												'1'=>$row->viewcreationcolmodelindexname,
												'2'=>$conditiongriddata[$j]->reportcondition,
												'3'=>$conditiongriddata[$j]->finalreportcondvalueid,
												'4'=>$conditiongriddata[$j]->reportandorcond,
												'5'=>$row->viewcreationjoincolmodelname,
												'6'=>$row->uitypeid
											);
						$conditioncolids[$j] = $row->viewcreationcolumnid;
					} else {
						$havingarray[$j] = array(
												'0'=>$row->viewcreationcolmodeljointable,
												'1'=>$row->viewcreationcolmodelindexname,
												'2'=>$conditiongriddata[$j]->reportcondition,
												'3'=>$conditiongriddata[$j]->finalreportcondvalueid,
												'4'=>$conditiongriddata[$j]->reportandorcond,
												'5'=>$row->viewcreationjoincolmodelname,
												'6'=>$row->uitypeid,
												'7'=>$conditiongriddata[$j]->reportaggregate,
											);
						$havingcolids[$j] =$row->viewcreationcolumnid;
					}
				}
				$i++;
				$j++;
			}
			$info=array(
					'conditionalarry'=>$conditionarray,
					'viewcolid'=>$conditioncolids,
					'havingarray'=>$havingarray,
					'havingcolids'=>$havingcolids
			);
		} else {
			$info=array(
				'conditionalarry'=>array(),
				'viewcolid'=>array(),
				'havingarray'=>array(),
				'havingcolids'=>array()
			);
		}
		return $info;
    }	
	//report groupby/sortby record 
	public function newreportsortbygroupbyquery($groupbycolidsmax,$sortbytype,$groupbyrange) {
		if(!empty($groupbycolidsmax)) {
			$groupbycolidsmin=implode(",",$groupbycolidsmax);
			if(COUNT(array_filter($groupbycolidsmax))>0) {
				$data=$this->db->select('viewcreationcolumns.viewcreationcolumnid,viewcreationcolumns.viewcreationcolmodelname,viewcreationcolumns.viewcreationcolmodeljointable,viewcreationcolumns.viewcreationtype,viewcreationcolumns.viewcreationcolmodelindexname')
					->from('viewcreationcolumns')
					->where_in('viewcreationcolumns.viewcreationcolumnid',$groupbycolidsmax)
					->where('viewcreationcolumns.status',1)
					->order_by("FIELD (viewcreationcolumnid,".$groupbycolidsmin."),viewcreationcolumnid")
					->get();
				if($data->num_rows() > 0) {
					$viecolids=array();
					$groupby=array();
					$groupbyname=array();
					$groupquery=" group by ";
					$groupquerywotbl=" group by ";
					$orderbyquery=" order by ";
					$i=0;
					foreach($data->result() as $info) {	
						if($groupbyrange>$i) {
							$groupby[]=$info->viewcreationcolumnid;
							$groupbyname[]=$info->viewcreationcolmodelname;
							$groupquery.=$info->viewcreationcolmodeljointable.'.'.$info->viewcreationcolmodelindexname.' '.$sortbytype[$i].',';			
							$groupquerywotbl.=$info->viewcreationcolmodelname.' '.$sortbytype[$i].',';	
							$orderbyquery.=$info->viewcreationcolmodeljointable.'.'.$info->viewcreationcolmodelindexname.' '.$sortbytype[$i].',';
							
						} else {
							$groupquery=" ";
							$groupquerywotbl=" ";
						}
							
						$viecolids[]=$info->viewcreationcolumnid;
						$i++;
					}
					$groupquery=rtrim($groupquery, ",");//group by query with table name
					$groupquerywotbl=rtrim($groupquerywotbl, ",");//group by query WITHOUT table name
					$orderbyquery=rtrim($orderbyquery, ",");
					$final=$groupquery.' '.$orderbyquery;
				} else {
					$final = " ";
					$viecolids[]='';
					$groupby=array();
					$groupbyname[]='';
					$groupquery=" ";
					$groupquerywotbl=" ";
					$orderbyquery=" ";
				}
				$info=array(
					'query'=>$final,
					'viewcolids'=>$viecolids,
					'groupbycolids'=>$groupby,
					'groupbyquery'=>$groupquery,
					'orderbyquery'=>$orderbyquery,
					'groupcol'=>$groupbyname,
					'groupbyquerywotbl'=>$groupquerywotbl,
				);
			} else {
				$info=array(
				'query'=>" ",
				'viewcolids'=>array(),
				'groupbycolids'=>array(),
				'groupbyquery'=>" ",
				'orderbyquery'=>"",
				'groupcol'=>array(),
				'groupbyquerywotbl'=>" "
				);
			}
		} else {
			$info=array(
				'query'=>" ",
				'viewcolids'=>array(),
				'groupbycolids'=>array(),
				'groupbyquery'=>" ",
				'orderbyquery'=>"",
				'groupcol'=>array(),
				'groupbyquerywotbl'=>" "
				);
		}
		return $info;
	}
	//return the related join parameters
	public function newgeneratejoinquery($mainmoduleid,$viewfieldids,$parenttable) {
		$joined_table= array();
		$dduitypeids = array('17','18','19','23','25','26','27','28','29');
		$join_query_string='';
		$newjoin = '';
		$this->db->select("viewcreationcolumns.viewcreationmoduleid,viewcreationcolumns.viewcreationcolumnid,viewcreationcolumns.viewcreationcolmodeltable,viewcreationcolumns.uitypeid",false);
		$this->db->from('viewcreationcolumns');
		$this->db->where('viewcreationcolumns.status',1);
		$this->db->where_in('viewcreationcolumns.viewcreationcolumnid',$viewfieldids);
		$this->db->group_by(array("viewcreationmoduleid", "viewcreationcolmodeltable"));
		$this->db->ORDER_BY("FIELD (viewcreationcolumns.viewcreationmoduleid,".$mainmoduleid.") DESC,viewcreationcolumns.viewcreationmoduleid ASC,viewcreationcolumns.viewcreationcolumnid ASC");		
		$data = $this->db->get();
		$result=$data->result();
		if($data->num_rows() > 0) {	
			foreach($data->result() as $info) {		
				$join_query= array();
				$join_a = '';
				$newparenttable = $info->viewcreationcolmodeltable;
				$newmoduleid = $info->viewcreationmoduleid;
				$uitypeid = $info->uitypeid;
				if($parenttable!=$newparenttable) {
					if(!in_array($newparenttable,$joined_table)) { 
						do { 
							$intbl=$newparenttable;
							$jinfo=$this->db->select('moduleid,parenttable,parentfieldname,relatedmoduleid,relatedtable,relatedfieldname,joinpriority,complexrelatedtable,jointext')
								->from('modulerelation')
								->where_in('moduleid',array($mainmoduleid,$newmoduleid))
								->where('relatedtable',$newparenttable)
								->group_by("relatedtable")
								->ORDER_BY("FIELD (moduleid,".$mainmoduleid.")DESC,moduleid ASC")		
								->where('status',1)						
								->get();
							$joininfo=$jinfo->row();
							if($jinfo->num_rows() > 0) {//Alias based split the related table 
								$jointext = $joininfo->jointext;
								$relatedtblsting=explode(" as ",$joininfo->relatedtable);
								if(isset($relatedtblsting[1])) {
									$relatedtblname=$relatedtblsting[1];
								} else {
									$relatedtblname=$relatedtblsting[0];
								}
								if($joininfo->complexrelatedtable != ''){
									$join_query[] = $joininfo->complexrelatedtable;
								}else{
								if($uitypeid == 20) {
									//employee
									$join_query []= ' '.$jointext.' '.$joininfo->relatedtable.' ON FIND_IN_SET('.$joininfo->parenttable.'.'.$joininfo->parentfieldname.','.$relatedtblname.'.'.$joininfo->relatedfieldname.') > 0 AND '.$parenttable.'.employeetypeid LIKE "%1%"';
									//group
									$join_query []= ' '.$jointext.' employeegroup ON FIND_IN_SET(employeegroup.employeegroupid,'.$parenttable.'.'.$joininfo->relatedfieldname.') > 0 AND '.$parenttable.'.employeetypeid  LIKE "%2%"';
									//user role and sub ordinate
									$join_query []= ' '.$jointext.' userrole ON FIND_IN_SET(userrole.userroleid,'.$parenttable.'.'.$joininfo->relatedfieldname.') > 0 AND ('.$parenttable.'.employeetypeid LIKE "%3%" OR '.$parenttable.'.employeetypeid LIKE "%4%")';
								} else if(in_array($info->uitypeid,$dduitypeids)){
									$muloptchk = $this->generalinformaion('viewcreationcolumns','multiple','viewcreationcolumnid',$info->viewcreationcolumnid);
									if($muloptchk == 'Yes'){
										$join_query[]= ' '.$jointext.' '.$joininfo->relatedtable.' ON FIND_IN_SET('.$relatedtblname.'.'.$joininfo->relatedfieldname.','.$joininfo->parenttable.'.'.$joininfo->parentfieldname.') > 0';
									} else {
										$join_query[]= ' '.$jointext.' '.$joininfo->relatedtable.' ON '.$relatedtblname.'.'.$joininfo->relatedfieldname.'='.$joininfo->parenttable.'.'.$joininfo->parentfieldname;
									}
								} else if($uitypeid == 32){
									$join_query[]= ' '.$jointext.' '.$joininfo->relatedtable.' ON '.$relatedtblname.'.'.$joininfo->relatedfieldname.'='.$joininfo->parenttable.'.'.$joininfo->parentfieldname;
								} else {
									$relatedfieldsting=explode(" and ",$joininfo->relatedfieldname);
									if(isset($relatedfieldsting[1])) {
										$join_query[]= ' '.$jointext.' '.$joininfo->relatedtable.' ON '.$relatedtblname.'.'.$relatedfieldsting[0].'='.$joininfo->parenttable.'.'.$joininfo->parentfieldname.' AND '.$relatedfieldsting[1];
									} else {
										$join_query[]= ' '.$jointext.' '.$joininfo->relatedtable.' ON '.$relatedtblname.'.'.$joininfo->relatedfieldname.'='.$joininfo->parenttable.'.'.$joininfo->parentfieldname;
									}
								}
								}
								$joined_table[]=$joininfo->relatedtable;
								$newparenttable=$joininfo->parenttable;
								$newmoduleid=$joininfo->moduleid;
							} else {
								break;
							}
						}while(($parenttable!=$newparenttable)&&(!in_array($newparenttable,$joined_table)));
						$join_a = implode(" ",array_reverse($join_query));
					}
				}
				$join_query_string = $join_query_string.' '.$join_a;
			}
			$join_query_string = $join_query_string.' '.$newjoin;
			return $join_query_string;
        }
	}
	//using this function we are generating the select query of report
	public function newgenerateselectstmt($reportcolids,$reporttype,$summarviewcolids,$summaryamids) {  
				$select=',';
				$tabsumselect=',';
				$selectwithouttbl=',';
				$selectforrollup=',';
				$selectforoutsideonlycol=',';
				$roundweight=$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); // weight round
				$roundamount=$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); // amount round
				$roundrate=$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); // rate round
				$roundmelting=$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); // melting round
				$dia_weight_round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',8);
				$fieldids = explode(',',$reportcolids);
				$data=$this->db->select('viewcreationcolumns.viewcreationcolumnid,viewcreationcolumns.viewcreationcolmodelname,viewcreationcolmodeltable,uitypeid,viewcreationcolmodelaliasname as colmodelaliasname,viewcreationcolumns.selectstatement,viewcreationcolmodeljointable as coljointablename,viewcreationcolumns.roundtype,roundtype.roundtypevalue,viewcreationcolumns.uitypeid')
					->from('viewcreationcolumns')
					->join('roundtype','roundtype.roundtypeid=viewcreationcolumns.roundtype','left outer' )
					->where_in('viewcreationcolumns.viewcreationcolumnid',$fieldids)
					->where('viewcreationcolumns.status',$this->Basefunctions->activestatus)
					->order_by('FIELD(viewcreationcolumns.viewcreationcolumnid,'.$reportcolids.')')
					->get();
				$aggregatemethod=$this->getaggregatemethod();
				$amtype=array();	
				foreach($aggregatemethod as $row) {
					$amtype[$row->aggregatemethodid]=array(
						$row->aggregatemethodkey,
						$row->aggregatemethodname
					);
				}
				$summarycolids=explode(',',$summarviewcolids);
				$dduitypeids = array('17','18','19','23','25','26','27','28','29','30');
				if($data->num_rows() > 0) {
					if($reporttype=='TV') {
						$v = 0;
						foreach($data->result() as $info) {
							$viewcolids[$v] =  $info->viewcreationcolumnid;
							$v++;
							//Alias based split the related table 
							$colmodelaliasname=explode(" as ",$info->colmodelaliasname);
							if(isset($colmodelaliasname[1])) {
								$columnname=$colmodelaliasname[1];
							} else {
								$columnname=$colmodelaliasname[0];
							}
							if(isset($info->roundtypevalue)) {
								$roundvalue=$info->roundtypevalue;
							} else {
								$roundvalue=2;
							}
							$selecttype = $this->Basefunctions->singlefieldfetch('selecttype','viewcreationcolumnid','viewcreationcolumns',$info->viewcreationcolumnid); //select type
							if($selecttype == 5) {
								$selectquery = $this->Basefunctions->singlefieldfetchinactive('selectquery','viewcreationcolumnid','viewcreationcolumns',$info->viewcreationcolumnid); //selectquery
								$select .= 'ROUND('.$selectquery.','.$roundvalue.') as '.$info->colmodelaliasname.',';
								if(!empty($summaryamids)) {
									$position=array_search($info->viewcreationcolumnid,$summarycolids);
									if($position !== false) {
										$sumarr=array_filter(explode(',',ltrim($summaryamids[$position],',')));
										foreach($sumarr as $am) {
											if($amtype[$am][0] != "ACTUAL") {
												$tabsumselect .= 'ROUND('.$amtype[$am][0].'('.$selectquery.'),'.$roundvalue.') as '.$columnname.',';
											} else {
												$tabsumselect .= "'' as ".$columnname.',';
											}
										}
									} else {
										$tabsumselect .= "'' as ".$columnname.',';
									}
								}	
							} else if($selecttype == 3) {

								$selectquery = $this->Basefunctions->singlefieldfetchinactive('selectquery','viewcreationcolumnid','viewcreationcolumns',$info->viewcreationcolumnid); //selectquery
								if($info->uitypeid==4 || $info->uitypeid==5 || $info->uitypeid==6 || $info->uitypeid==7) {
									$select .= 'ROUND('.$selectquery.','.$roundvalue.') as '.$info->colmodelaliasname.',';
								} else {
									$select .= '('.$selectquery.') as '.$info->colmodelaliasname.',';
								}
								//for tabular summary - start
								if(!empty($summaryamids)) {
									$position=array_search($info->viewcreationcolumnid,$summarycolids);
									if($position !== false) {
										$sumarr=array_filter(explode(',',ltrim($summaryamids[$position],',')));
										foreach($sumarr as $am) {
											if($amtype[$am][0] != "ACTUAL") {
												$tabsumselect .= 'ROUND('.$amtype[$am][0].'('.$selectquery.'),'.$roundvalue.') as '.$columnname.',';
											} else {
												$tabsumselect .= "'' as ".$columnname.',';
											}
										}
									} else {
										$tabsumselect .= "'' as ".$columnname.',';
									}
								}								
								//for tabular summary - end
							}else {
								if($info->uitypeid==4 || $info->uitypeid==5 || $info->uitypeid==6 || $info->uitypeid==7) {
									$select .= ' ROUND(('.$info->coljointablename.'.'.$colmodelaliasname[0].'),'.$roundvalue.') as '.$columnname.',';
									$selectwithouttbl .= ' ROUND(('.$columnname.'),'.$roundvalue.') as '.$columnname.',';
									//for tabular summary - start
									if(!empty($summaryamids)) {
										$position=array_search($info->viewcreationcolumnid,$summarycolids);
										if($position !== false) {
											$sumarr=array_filter(explode(',',ltrim($summaryamids[$position],',')));
											foreach($sumarr as $am) {
												if($amtype[$am][0] != "ACTUAL") {
													$tabsumselect .= 'ROUND('.$amtype[$am][0].'('.$info->coljointablename.'.'.$colmodelaliasname[0].'),'.$roundvalue.') as '.$columnname.',';
												} else {
													$tabsumselect .= "'' as ".$columnname.',';
												}
											}
										} else {
											$tabsumselect .= "'' as ".$columnname.',';
										}
									}else {$tabsumselect .= "'' as ".$columnname.','; }
									//for tabular summary - end
								} else if($info->uitypeid == 20) {
									//echo $info->coljointablename;
									if($info->coljointablename == 'employee'){
										$select .= ''.$info->coljointablename .'.'.$info->colmodelaliasname.',userrole.userrolename AS rolename,employeegroup.employeegroupname AS empgrpname,';
										$selectwithouttbl .= $info->colmodelaliasname.',rolename,empgrpname,';
										$tabsumselect .=  "'' as ".$columnname.',';
									} else {
										$select .= $info->coljointablename .'.'.$info->colmodelaliasname.',';
										$selectwithouttbl .= $columnname.',';
										$tabsumselect .=  "'' as ".$columnname.',';
									}
								} else if(in_array($info->uitypeid,$dduitypeids)) {
									$muloptchk = $this->generalinformaion('viewcreationcolumns','multiple','viewcreationcolumnid',$info->viewcreationcolumnid);
									if($muloptchk == 'Yes') {
										if(isset($colmodelaliasname[1])) {
											$select .= 'GROUP_CONCAT(DISTINCT '.$info->coljointablename .'.'.$colmodelaliasname[0].') AS '.$colmodelaliasname[1].',';
										} else {
											$select .= 'GROUP_CONCAT(DISTINCT '.$info->coljointablename .'.'.$info->colmodelaliasname.') AS '.$info->colmodelaliasname.',';
										}
										$selectwithouttbl .= $columnname.',';
										$tabsumselect .=  "'' as ".$columnname.',';
									} else {
										$select .= $info->coljointablename .'.'.$info->colmodelaliasname.',';
										$selectwithouttbl .= $columnname.',';
										$tabsumselect .=  "'' as ".$columnname.',';
									}
								} else {
									$select .= $info->coljointablename .'.'.$info->colmodelaliasname.',';
									$selectwithouttbl .= $columnname.',';
									$tabsumselect .=  "'' as ".$columnname.',';
								}
							}
						}
					}
					if($reporttype=='SV') {
						$i = 0;
						foreach($data->result() as $info) { 
							$newArray = array_values((array)$info);					
							$viewcolids[$i] =  $info->viewcreationcolumnid;
							//Alias based split the related table 
							$colmodelaliasname=explode(" as ",$info->colmodelaliasname);
							if(isset($colmodelaliasname[1])) {
								$columnname=$colmodelaliasname[1];
							} else {
								$columnname=$colmodelaliasname[0];
							}
							if(isset($info->roundtypevalue)) {
								$roundvalue=$info->roundtypevalue;
							} else {
								$roundvalue=2;
							}
							$selecttype = $this->Basefunctions->singlefieldfetch('selecttype','viewcreationcolumnid','viewcreationcolumns',$info->viewcreationcolumnid);
							if($selecttype == 5) {
								$selectquery = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',$info->viewcreationcolumnid);
								$position=array_search($info->viewcreationcolumnid,$summarycolids);
								if($position !== false) {
									$sumarr=array_filter(explode(',',ltrim($summaryamids[$position],',')));
									foreach($sumarr as $am) {
										$select .= 'ROUND(('.$selectquery.'),'.$roundvalue.') as '.$amtype[$am][1].''.$info->colmodelaliasname.',';
										$selectwithouttbl .= 'ROUND('.$amtype[$am][1].''.$columnname.','.$roundvalue.') as '.$amtype[$am][1].''.$columnname.',';
										if($amtype[$am][1] != "ACTUAL") {
											$selectforrollup .= 'ROUND('.$amtype[$am][1].'('.$amtype[$am][1].''.$columnname.'),'.$roundvalue.') as '.$amtype[$am][1].''.$columnname.',';
										} else {
											$selectforrollup .= 'ROUND('.$amtype[$am][1].''.$columnname.','.$roundvalue.') as '.$amtype[$am][1].''.$columnname.',';
										}
										//$selectforrollup .= 'ROUND('.$amtype[$am][1].'('.$amtype[$am][1].''.$columnname.'),'.$roundvalue.') as '.$amtype[$am][1].''.$columnname.',';
										$selectforoutsideonlycol .= $amtype[$am][1].''.$columnname.',';
									}
								} else {
									$select .= 'ROUND(('.$info->coljointablename.'.'.$info->colmodelaliasname.'),'.$roundvalue.') as '.$info->colmodelaliasname.',';
									$selectwithouttbl .= 'ROUND(('.$columnname.'),'.$roundvalue.') as '.$columnname.',';
									$selectforrollup .= 'ROUND(('.$columnname.'),'.$roundvalue.') as '.$columnname.',';
									$selectforoutsideonlycol .= $columnname.',';
								}
							}  else if($selecttype == 3) {
								$selectquery = $this->Basefunctions->singlefieldfetchinactive('selectquery','viewcreationcolumnid','viewcreationcolumns',$info->viewcreationcolumnid); //selectquery
								if($info->uitypeid==4 || $info->uitypeid==5 || $info->uitypeid==6 || $info->uitypeid==7) {
									$select .= ''.$selectquery.' as SUM'.$columnname.',';
									$selectwithouttbl .= 'ROUND((SUM'.$columnname.'),'.$roundvalue.') as SUM'.$columnname.',';
									$selectforrollup .= 'ROUND((SUM'.$columnname.'),'.$roundvalue.') as SUM'.$columnname.',';
								} else {
									$select .= '('.$selectquery.') as SUM'.$columnname.',';
									$selectwithouttbl .= '(SUM'.$columnname.')) as SUM'.$columnname.',';
									$selectforrollup .= 'ROUND((SUM'.$columnname.'),'.$roundvalue.') as SUM'.$columnname.',';
								}
								$selectforoutsideonlycol .= 'SUM'.$columnname.',';
							} else {
								$position=array_search($info->viewcreationcolumnid,$summarycolids);
								if($position !== false) {
									$sumarr=array_filter(explode(',',ltrim($summaryamids[$position],',')));
									foreach($sumarr as $am) {
										if($amtype[$am][0] == "ACTUAL")
										{
											if($info->uitypeid==4 || $info->uitypeid==5 || $info->uitypeid==6 || $info->uitypeid==7) {
												$select .= ' ROUND(('.$info->coljointablename.'.'.$colmodelaliasname[0].'),'.$roundvalue.') as '.$amtype[$am][1].''.$columnname.',';
												$selectwithouttbl .= ' ROUND(('.$amtype[$am][1].''.$columnname.'),'.$roundvalue.') as '. $amtype[$am][1].''.$columnname.',';
												//$selectforrollup .= ' ROUND(('.$amtype[$am][1].'('.$amtype[$am][1].''.$columnname.')),'.$roundvalue.') as '. $amtype[$am][1].''.$columnname.',';
												$selectforrollup .= ' ROUND('.$amtype[$am][1].''.$columnname.','.$roundvalue.') as '. $amtype[$am][1].''.$columnname.',';
											}
											else{
												
												$select .= '('.$info->coljointablename.'.'.$colmodelaliasname[0].') as '.$amtype[$am][1].''.$columnname.',';
												$selectwithouttbl .= $amtype[$am][1].''.$columnname.',';
												$selectforrollup .= $amtype[$am][1].''.$columnname.',';
											}
											
										}
										else{
											$select .= 'ROUND('.$amtype[$am][0].'('.$info->coljointablename.'.'.$colmodelaliasname[0].'),'.$roundvalue.') as '.$amtype[$am][1].''.$columnname.',';
											$selectwithouttbl .= 'ROUND('.$amtype[$am][1].''.$columnname.','.$roundvalue.') as '.$amtype[$am][1].''.$columnname.',';
											$selectforrollup .= ' ROUND(('.$amtype[$am][1].'('.$amtype[$am][1].''.$columnname.')),'.$roundvalue.') as '. $amtype[$am][1].''.$columnname.',';
										}
										$selectforoutsideonlycol .= $amtype[$am][1].''.$columnname.',';
									}
								} else {
									if($info->uitypeid==4 || $info->uitypeid==5 || $info->uitypeid==6 || $info->uitypeid==7) {
										$select .= 'ROUND(('.$info->coljointablename.'.'.$info->colmodelaliasname.'),'.$roundvalue.') as '.$info->colmodelaliasname.',';
										$selectwithouttbl .= 'ROUND(('.$columnname.'),'.$roundvalue.') as '.$columnname.',';
										$selectforrollup .= 'ROUND(('.$columnname.'),'.$roundvalue.') as '.$columnname.',';
									} else {
										$select .= ''.$info->coljointablename.'.'.$info->colmodelaliasname.',';
										$selectwithouttbl .= ''.$columnname.',';
										$selectforrollup .= ''.$columnname.',';
									}
									$selectforoutsideonlycol .= $columnname.',';
								}
							}
							$i++;					
						}
					}
					if($reporttype=='') {
						$j = 0;
						foreach($data->result() as $info) {
							$viewcolids[$j] =  $info->viewcreationcolumnid;
							$j++;
							//Alias based split the related table 
							$colmodelaliasname=explode(" as ",$info->colmodelaliasname);
							if(isset($colmodelaliasname[1])){
								$columnname=$colmodelaliasname[1];
							} else {
								$columnname=$colmodelaliasname[0];
							}
							if(isset($info->roundtypevalue)) {
								$roundvalue=$info->roundtypevalue;
							} else {
								$roundvalue=2;
							}
							if($info->uitypeid == 20) {
								//echo $info->coljointablename;
								if($info->coljointablename == 'employee'){
									$select .= ''.$info->coljointablename .'.'.$info->colmodelaliasname.',userrole.userrolename AS rolename,employeegroup.employeegroupname AS empgrpname,';
									$selectwithouttbl .= $info->colmodelaliasname.',rolename,empgrpname,';
								} else {
									$select .= $info->coljointablename .'.'.$info->colmodelaliasname.',';
									$selectwithouttbl .= $columnname.',';
								}
							} else if(in_array($info->uitypeid,$dduitypeids)) {
								$muloptchk = $this->generalinformaion('viewcreationcolumns','multiple','viewcreationcolumnid',$info->viewcreationcolumnid);
								if($muloptchk == 'Yes') {
									if(isset($colmodelaliasname[1])) {
										$select .= 'GROUP_CONCAT(DISTINCT '.$info->coljointablename .'.'.$colmodelaliasname[0].') AS '.$colmodelaliasname[1].',';
									} else {
										$select .= 'GROUP_CONCAT(DISTINCT '.$info->coljointablename .'.'.$info->colmodelaliasname.') AS '.$info->colmodelaliasname.',';
									}
									$selectwithouttbl .= $columnname.',';
								} else {
									$select .= $info->coljointablename .'.'.$info->colmodelaliasname.',';
									$selectwithouttbl .= $columnname.',';
								}
							} else if($info->uitypeid == 4 || $info->uitypeid == 5){
								/* if($softwareindustryid == 3) { */
									$select .= 'ROUND(('.$info->coljointablename.'.'.$info->colmodelaliasname.'),'.$roundvalue.') as '.$info->colmodelaliasname.',';
								/* } else {

									$select .= $info->coljointablename .'.'.$info->colmodelaliasname.',';
								} */
								$selectwithouttbl .= $columnname.',';
							} else {
									$select .= $info->coljointablename .'.'.$info->colmodelaliasname.',';
									$selectwithouttbl .= $columnname.',';
							}
						}
					}
					if($reporttype=='PT') {
						
						$k = 0;
						foreach($data->result() as $info) {
							$viewcolids[$k] =  $info->viewcreationcolumnid;
							$k++;
							//Alias based split the related table
							$colmodelaliasname=explode(" as ",$info->colmodelaliasname);
							if(isset($colmodelaliasname[1])){
								$columnname=$colmodelaliasname[1];
							} else {
								$columnname=$colmodelaliasname[0];
							}
							$selecttype = $this->Basefunctions->singlefieldfetch('selecttype','viewcreationcolumnid','viewcreationcolumns',$reportcolids); //select type
							if($selecttype == 2){
								$selectquery = $this->Basefunctions->singlefieldfetch('selectquery','viewcreationcolumnid','viewcreationcolumns',$reportcolids); //selectquery
								$selectquery = str_replace("roundamount",$roundamount,$selectquery);
								$selectquery = str_replace("roundweight",$roundweight,$selectquery);
								$selectquery = str_replace("roundrate",$roundrate,$selectquery);
								$selectquery = str_replace("roundmelting",$roundmelting,$selectquery);
								$select = $selectquery;
							}else{
								if($info->uitypeid == 20) {
									//echo $info->coljointablename;
									if($info->coljointablename == 'employee'){
										$select .= ''.$info->coljointablename .'.'.$info->colmodelaliasname.',userrole.userrolename AS rolename,employeegroup.employeegroupname AS empgrpname,';
										$selectwithouttbl .= $info->colmodelaliasname.',rolename,empgrpname,';
									} else {
										$select .= $info->coljointablename .'.'.$info->colmodelaliasname.',';
										$selectwithouttbl .= $columnname.',';
									}
								}  else if($info->uitypeid == 8) {
								$format = $this->Basefunctions->phpmysqlappdateformatfetch();
								$select .= 'DATE_FORMAT('.$info->coljointablename .'.'.$info->colmodelaliasname.' ,"'.$format['mysqlformat'].'") as '.$info->colmodelaliasname.',';
								} else if(in_array($info->uitypeid,$dduitypeids)) {
									$muloptchk = $this->generalinformaion('viewcreationcolumns','multiple','viewcreationcolumnid',$info->viewcreationcolumnid);
									if($muloptchk == 'Yes') {
										if(isset($colmodelaliasname[1])) {
											$select .= 'GROUP_CONCAT(DISTINCT '.$info->coljointablename .'.'.$colmodelaliasname[0].') AS '.$colmodelaliasname[1].',';
										} else {
											$select .= 'GROUP_CONCAT(DISTINCT '.$info->coljointablename .'.'.$info->colmodelaliasname.') AS '.$info->colmodelaliasname.',';
										}
										$selectwithouttbl .= $columnname.',';
										} else {
										$select .= $info->coljointablename .'.'.$info->colmodelaliasname.',';
										$selectwithouttbl .= $columnname.',';
										}
								 }else if($info->uitypeid == 5) {//round the weight fields based on decimal licence

										 $select .= ', ROUND(('.$info->coljointablename.'.'.$info->colmodelaliasname.'),'.$info->roundtypevalue.') as columdata';	
								 }else{
									 $select .= $info->coljointablename .'.'.$info->colmodelaliasname.',';
									 $selectwithouttbl .= $columnname.',';
								 }
								}
						   }
						}
				}	
				$select = rtrim($select, ",");		
				$select = ltrim($select, ",");
				$selectwithouttbl = rtrim($selectwithouttbl, ",");		
				$selectwithouttbl = ltrim($selectwithouttbl, ",");
				$selectforrollup = rtrim($selectforrollup, ",");		
				$selectforrollup = ltrim($selectforrollup, ",");
				$selectforoutsideonlycol = rtrim($selectforoutsideonlycol, ",");		
				$selectforoutsideonlycol = ltrim($selectforoutsideonlycol, ",");
				$tabsumselect = rtrim($tabsumselect, ",");		
				$tabsumselect = ltrim($tabsumselect, ",");
				$info=array(
					'Selectstmt'=>$select,
					'Selectstmtwithouttbl'=>$selectwithouttbl,
					'selectforrollup'=>$selectforrollup,
					'Selecttabsum'=>$tabsumselect,
					'selectforoutsideonlycol'=>$selectforoutsideonlycol
				);
				return $info;			
			}
	//aggregate method based on each fields
	public function getaggregatemethod() {
		$result=$this->db->select('aggregatemethodid,aggregatemethodname,aggregatemethodkey')
			->from('aggregatemethod')
			->where('aggregatemethod.status',$this->Basefunctions->activestatus)
			->get()->result();
		return $result;	
	}
	//fetch colname & colmodel information fetch model
    public function newreportinformationfetchmodel($grouporder,$finalviewcolids,$reporttype,$summarviewcolids,$summaryamids,$viewid = '') {
    	$colids = 1;
    	$modname = '';
    	$modid = 1;
		
		/* for fetch colname & colmodel fetch */
		$i = 0;
		$m = 0;
		$data = array();
		$fieldids = explode(',',$finalviewcolids);
		$reportcolids = array_merge($grouporder,$fieldids);	
		$orderbycolids = implode(',',$reportcolids);
		$columnsize = array();
		$csize = array();
		if($viewid != '') {
			$columnsize = $this->generalinformaion('viewcreation','viewcolumnsize','viewcreationid',$viewid);
			$columnsize = explode(',',$columnsize);
		}
		$csize = array_fill(count($columnsize)+1,count($reportcolids),'200');
		$csize = array_merge($columnsize,$csize);
		
		/* done for report view grid, above code will not take it */
		if($viewid == '') {
			$columnsize = array();
			$csize = array();
			for($k=0; $k<count($reportcolids); $k++) {
				$columnsize = $this->generalinformaion('viewcreationcolumns','viewcreationcolumnsize','viewcreationcolumnid',$reportcolids[$k]);
				$csize[] = $columnsize;
			}
		}
		$this->db->select("viewcreationcolumns.viewcreationcolumnid,
								viewcreationcolumns.viewcreationcolumnname,
								viewcreationcolumns.viewcreationcolmodelname,
								viewcreationcolumns.viewcreationcolmodelindexname,
								viewcreationcolumns.viewcreationcolmodeljointable,
								viewcreationcolumns.viewcreationcolumnid,
								viewcreationcolumns.viewcreationcolmodelaliasname,
								viewcreationcolumns.viewcreationcolmodeltable,
								viewcreationcolumns.viewcreationparenttable,
								viewcreationcolumns.viewcreationmoduleid,
								viewcreationcolumns.viewcreationjoincolmodelname,
								viewcreationcolumns.viewcreationcolumnviewtype,
								viewcreationcolumns.viewcreationtype,
								viewcreationcolumns.viewcreationcolmodelcondname,
								viewcreationcolumns.uitypeid,
								viewcreationcolumns.calcoperation,
								viewcreationcolumns.moduletabsectionid,
								viewcreationcolumns.viewcreationcolmodelcondvalue,
								viewcreationcolumns.viewcreationcolumnicon,
								viewcreationcolumns.viewcreationcolumnsize,
								module.modulename");
		$this->db->from('viewcreationcolumns');
		$this->db->join('module','module.moduleid=viewcreationcolumns.viewcreationmoduleid');
		$this->db->where_in('viewcreationcolumns.viewcreationcolumnid',$reportcolids);
		$this->db->where('viewcreationcolumns.status',1);
		$this->db->order_by('FIELD(viewcreationcolumns.viewcreationcolumnid,'.$orderbycolids.')');
		$showfield = $this->db->get();
		$aggregatemethod=$this->getaggregatemethod();
		$amtype=array();	
		foreach($aggregatemethod as $row) {
			$amtype[$row->aggregatemethodid]=array(
				$row->aggregatemethodkey,
				$row->aggregatemethodname
			);
		}
		$summarycolids=explode(',',$summarviewcolids);
		if($showfield->num_rows() >0) {
			if($reporttype=='TV') {
				foreach($showfield->result() as $show) {
					$data['colid'][$i]=$show->viewcreationcolumnid;
					$data['colname'][$i]=$show->viewcreationcolumnname;
					$data['colicon'][$i]=$show->viewcreationcolumnicon;
					$data['colsize'][$i]="200";
					$data['colmodelname'][$i]=$show->viewcreationcolmodelname;
					$data['colmodelaliasname'][$i]=$show->viewcreationcolmodelaliasname;
					$data['coltablename'][$i]=$show->viewcreationcolmodeltable;
					$data['coljointablename'][$i]=$show->viewcreationcolmodeljointable;
					$data['coljoinmodelname'][$i]=$show->viewcreationjoincolmodelname;
					$data['colmodelindex'][$i]=$show->viewcreationcolmodeljointable .'.'.$show->viewcreationcolmodelindexname;
					$data['colmodelparenttable'][$i]=$show->viewcreationparenttable;
					$data['colmodelviewtype'][$i]=$show->viewcreationcolumnviewtype;
					$data['colmodeldatatype'][$i]=$show->viewcreationtype;
					$data['colmodelcondname'][$i]=$show->viewcreationcolmodelcondname;
					$data['colmodelcondvalue'][$i]=$show->viewcreationcolmodelcondvalue;
					$data['colmodelmoduleid'][$i]=$show->viewcreationmoduleid;
					$data['colmodeluitype'][$i]=$show->uitypeid;
					$data['colmodelsectid'][$i]=$show->moduletabsectionid;
					$data['colmodeltype'][$i]='text';
					$data['modname'][$i] = $show->modulename;
					$data['calcoperation'][$i]=$show->calcoperation;
					$i++;
				}
			} else {
				foreach($showfield->result() as $show) {
					$position=array_search($show->viewcreationcolumnid,$summarycolids);					
					if($position!== false) {
						$sumarr=array_filter(explode(',',ltrim($summaryamids[$position],',')));
						foreach($sumarr as $am) {
							$data['colid'][$i]=$show->viewcreationcolumnid;
							$data['colname'][$i]=$amtype[$am][1].' '.$show->viewcreationcolumnname;
							$data['colicon'][$i]=$show->viewcreationcolumnicon;
							$data['colsize'][$i]="200";
							$data['colmodelname'][$i]=$amtype[$am][1].''.$show->viewcreationcolmodelname;
							$data['colmodelaliasname'][$i]=$amtype[$am][1].' '.$show->viewcreationcolmodelaliasname;
							$data['coltablename'][$i]=$show->viewcreationcolmodeltable;
							$data['coljointablename'][$i]=$show->viewcreationcolmodeljointable;
							$data['coljoinmodelname'][$i]=$show->viewcreationjoincolmodelname;
							$data['colmodelindex'][$i]=$show->viewcreationcolmodeljointable .'.'.$show->viewcreationcolmodelindexname;
							$data['colmodelparenttable'][$i]=$show->viewcreationparenttable;
							$data['colmodelviewtype'][$i]=$show->viewcreationcolumnviewtype;
							$data['colmodeldatatype'][$i]=$show->viewcreationtype;
							$data['colmodelcondname'][$i]=$show->viewcreationcolmodelcondname;
							$data['colmodelcondvalue'][$i]=$show->viewcreationcolmodelcondvalue;
							$data['colmodelmoduleid'][$i]=$show->viewcreationmoduleid;
							$data['colmodeluitype'][$i]=$show->uitypeid;
							$data['colmodelsectid'][$i]=$show->moduletabsectionid;
							$data['colmodeltype'][$i]='text';
							$data['modname'][$i] = $show->modulename;
							$data['calcoperation'][$i]=$show->calcoperation;
							$i++;
						}
					} else {
						$data['colid'][$i]=$show->viewcreationcolumnid;
						$data['colname'][$i]=$show->viewcreationcolumnname;
						$data['colicon'][$i]=$show->viewcreationcolumnicon;
						$data['colsize'][$i]=$csize[$i];
						$data['colmodelname'][$i]=$show->viewcreationcolmodelname;
						$data['colmodelaliasname'][$i]=$show->viewcreationcolmodelaliasname;
						$data['coltablename'][$i]=$show->viewcreationcolmodeltable;
						$data['coljointablename'][$i]=$show->viewcreationcolmodeljointable;
						$data['coljoinmodelname'][$i]=$show->viewcreationjoincolmodelname;
						$data['colmodelindex'][$i]=$show->viewcreationcolmodeljointable .'.'.$show->viewcreationcolmodelindexname;
						$data['colmodelparenttable'][$i]=$show->viewcreationparenttable;
						$data['colmodelviewtype'][$i]=$show->viewcreationcolumnviewtype;
						$data['colmodeldatatype'][$i]=$show->viewcreationtype;
						$data['colmodelcondname'][$i]=$show->viewcreationcolmodelcondname;
						$data['colmodelcondvalue'][$i]=$show->viewcreationcolmodelcondvalue;
						$data['colmodelmoduleid'][$i]=$show->viewcreationmoduleid;
						$data['colmodeluitype'][$i]=$show->uitypeid;
						$data['colmodelsectid'][$i]=$show->moduletabsectionid;
						$data['colmodeltype'][$i]='text';
						$data['modname'][$i] = $show->modulename;
						$data['calcoperation'][$i]=$show->calcoperation;
						$i++;
					}
				}
			}
		}
		$m++;
		return $data;
    }
	//report where clause generation
	public function reportgeneratehavingclause($conditionvalarray) {	
		$havingString="";
		$i= 0;
		foreach ($conditionvalarray as $key) {
			if($i == 0){$key[4] = '';}
			$i++;
			$field ="(HV".$key[7]."".$key[1].")";
			switch($key[2]) {
				case "Equalto": 
					$havingString .= " ".$key[4]." ".$field. " = '".$key[3]."'";;
					break;
				case "NotEqual": 
					$havingString .= " ".$key[4]." ".$field. " != '" .$key[3]."'";;
					break;
				case "Like":
				//	$havingString .= $key[0].'.'.$key[1]. " LIKE '".$key[3]"'"." ".  $key[4]." ";
				//	break;
				case "Startwith":
					$havingString .= " ". $key[4]." ".$field. " LIKE '".$key[3].'%'."'";
					break;
				case "Endwith": 
					$havingString .= " ". $key[4]." ".$field. " LIKE '".'%'.$key[3]."'";
					break;
				case "Contains": 
					$havingString .= " ". $key[4]." ".$field. " LIKE '".'%'.$key[3].'%'."'";
					break;
				case "IN": //specials
					$havingString .= " ". $key[4]." ".$field. " IN (".$key[3].")";
					break;
				case "NOT IN": 
					$havingString .= " ". $key[4]." ".$field. " NOT IN (".$key[3].")";
					break;
				case "GreaterThan":
					$havingString .= " ". $key[4]." ".$field. " > '" .$key[3]."'";
					break;
				case "LessThan": 
					$havingString .= " ". $key[4]." ".$field. " < '" .$key[3]."'";
					break;
				case "GreaterThanEqual": 
					$havingString .= " ". $key[4]." ".$field." >= '" .$key[3]."'";
					break;
				case "LessThanEqual": 
					$havingString .= " ". $key[4]." ".$field. " <= '" .$key[3]."'";
					break;
				case "IS NULL": 
					$havingString .= " ". $key[4]." ".$field. " IS NULL '" .$key[3]."'";
					break;
				case "NOT LIKE": 
					$havingString .= " ". $key[4]." ".$field. " NOT LIKE '" .$key[3]."'";
					break;
				case "IS NOT NULL": 
					$havingString .= " ". $key[4]." ".$field. " IS NOT NULL '" .$key[3]."'";
					break;
				case "Multiple":
					$whereString .= " ". $key[4]." ".$key[7].'.'.$field. " = '".$key[3]."'";
					break;
				default:
					$havingString .= "";
					break;
			}
		}
		//$havingString= substr($havingString, 0, -4);
		return $havingString;
    }
	public function reporthavingclauseselectstmt($conditionvalarray)  {	
		$havingselect=",";
		foreach ($conditionvalarray as $key) {
			$field ="HV".$key[7]."".$key[1];
			$havingselect .= 'ROUND('.$key[7].'('.$key[1].'),2) as '.$field.',';
		}
		$select = rtrim($havingselect,",");		
		$info=array(
			'havingselect'=>$select
		);
		return $info;
	}
	//where condition generation-new
    public function reportgeneratewhereclause($conditionvalarray) {
		$current_date = date("Y-m-d");
		$whereString="";
		$havingString="";
        $i = 0;
		foreach ($conditionvalarray as $key) {
			if($i == 0){$key[4] = '';}
			$i++;
			/*if($key[6]!=4 && $key[6]!=5){*/ 
			$dduitypes = array(17,18,19,20,25,26,32);
			if(in_array($key[6],$dduitypes)){
				$field = $key[5];
			} else {
				$field = $key[1];
			}
			if($key[6] == 8) {
				if($key[2] == 'Custom') {
					$date_split = explode("|",$key[3]);
				} else {
					$key[3] = date("Y-m-d", strtotime( $key[3]));
				}
			}	
			
			switch($key[2]) {
				case "Equalto": 
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " = '".$key[3]."'";
					break;
				case "NotEqual": 
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " != '" .$key[3]."'";
					break;
				case "Like":
				//	$whereString .= " ". $key[4]." ".$key[0].'.'.$key[1]. " LIKE '".$key[3]"'"." ".  $key[4]." ";
				//	break;
				case "Startwith":
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " LIKE '".$key[3].'%'."'";
					break;
				case "Endwith": 
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " LIKE '".'%'.$key[3]."'";
					break;
				case "Contains": 
					$whereString .= $key[0].'.'.$field. " LIKE '".'%'.$key[3].'%'."'";
					break;
				case "IN": //specials
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " IN (".$key[3].")";
					break;
				case "NOT IN": 
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " NOT IN (".$key[3].")";
					break;
				case "GreaterThan": 
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " > '" .$key[3]."'";
					break;
				case "LessThan": 
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " < '" .$key[3]."'";
					break;
				case "GreaterThanEqual": 
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field." >= '" .$key[3]."'";
					break;
				case "LessThanEqual": 
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " <= '" .$key[3]."'";
					break;
				case "IS NULL": 
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " IS NULL '" .$key[3]."'";
					break;
				case "NOT LIKE": 
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " NOT LIKE '" .$key[3]."'";
					break;
				case "IS NOT NULL": 
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " IS NOT NULL '" .$key[3]."'";
					break;
				case "Multiple":
					$whereString .= " ". $key[4]." ".$key[7].'.'.$field. " = '".$key[3]."'";
					break;
				case "Custom":
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$date_split[0]."' AND ".$field. " <= '".$date_split[1]."'";
					break;
				case "Yesterday":
					$date_value = $this->getdatevalue('-1 day',$current_date);
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " = '".$date_value['start']."'";
					break;
				case "Today":
					$date_value = $this->getdatevalue('0 day',$current_date);
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " = '".$date_value['start']."'";
					break;
				case "Tomorrow":
					$date_value = $this->getdatevalue('1 day',$current_date);
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " = '".$date_value['start']."'";
					break;
				case "Last 7 Days":
					$f_date = strtotime('-6 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Last 30 Days":
					$f_date = strtotime('-29 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Last 60 Days":
					$f_date = strtotime('-59 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Last 90 Days":
					$f_date = strtotime('-89 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Last 120 Days":
					$f_date = strtotime('-119 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Next 7 Days":
					$f_date = strtotime('6 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Next 30 Days":
					$f_date = strtotime('29 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Next 60 Days":
					$f_date = strtotime('59 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Next 90 Days":
					$f_date = strtotime('89 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Next 120 Days":
					$f_date = strtotime('119 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Last Week":
					$date_value = $this->datesystem('lastweek',$current_date);
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$date_value['start']."' AND ".$field. " <= '".$date_value['end']."'";
					break;
				case "Current Week":
					$date_value = $this->datesystem('currentweek',$current_date);
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$date_value['start']."' AND ".$field. " <= '".$date_value['end']."'";
					break;
				case "Next Week":
					$date_value = $this->datesystem('nextweek',$current_date);
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$date_value['start']."' AND ".$field. " <= '".$date_value['end']."'";
					break;
				case "Last Month":
					$date_value = $this->datesystem('lastmonth',$current_date);
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$date_value['start']."' AND ".$field. " <= '".$date_value['end']."'";
					break;
				case "Current Month":
					$date_value = $this->datesystem('currentmonth',$current_date);
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$date_value['start']."' AND ".$field. " <= '".$date_value['end']."'";
					break;
				case "Next Month":
					$date_value = $this->datesystem('nextmonth',$current_date);
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$date_value['start']."' AND ".$field. " <= '".$date_value['end']."'";
					break;
				case "Current and Previous Month":
					$date_value = $this->datesystem('currentandpreviousmonth',$current_date);
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$date_value['start']."' AND ".$field. " <= '".$date_value['end']."'";
					break;
				case "Current and Next Month":
					$date_value = $this->datesystem('currentandnextmonth',$current_date);
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$date_value['start']."' AND ".$field. " <= '".$date_value['end']."'";
					break;
				case "Before 7 Days":
					$f_date = strtotime('-6 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Before 30 Days":
					$f_date = strtotime('-29 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Before 60 Days":
					$f_date = strtotime('-59 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Before 90 Days":
					$f_date = strtotime('-89 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Before 120 Days":
					$f_date = strtotime('-119 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				default:
					$whereString .= "";
					break;
			}
		    /*}*/
		}
		//$whereString= substr($whereString, 0, -4);
		
		return $whereString;
    }
    //where condition generation-new for filter
    public function filtergeneratewhereclause($conditionvalarray,$maintable) {
		$current_date = date("Y-m-d");
    	$whereString="";
    	$havingString="";
    	foreach($conditionvalarray as $key) {
    		$dduitypes = array('17','18','19','20','23','25','26','27','28','29','30');
    		if(in_array($key[6],$dduitypes)) {
    			if($key[6] == 20) {
					if(strpos($key[3], ':')) {
						$data = explode(':',$key[3]);
					} else {
						$data = explode('--',$key[3]);
					}
    				$key[3] = $data[1];
    				$key[0] = $maintable;
    				$type = ' AND '.$maintable.'.employeetypeid='.$data[0];
    				$field = $key[5];
    				
    			} else {
    				$field = $key[5];
    				$type = '';
    			}
    		} else if ($key[6] == 4 || $key[6] == 5) {
				$field = $key[1];
				$type = '';
				$from = $this->generalinformaion('rangemaster','fromrange','rangemasterid',$key[3]);
				$to = $this->generalinformaion('rangemaster','torange','rangemasterid',$key[3]);
			} else {
    			$field = $key[1];
				$type = '';
    		}
			if($key[6] == 8) {
				if($key[2] == 'Custom') {
					$date_split = explode(":",$key[3]);
				} else {
					$key[3] = date("Y-m-d", strtotime( $key[3]));
				}
			}
    		switch($key[2]) {
    			case "Equalto":
    				$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " = '".$key[3]."'";
    				break;
    			case "NotEqual":
    				$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " != '" .$key[3]."'";
    				break;
    			case "Like":
    				//	$whereString .= " ". $key[4]." ".$key[0].'.'.$key[1]. " LIKE '".$key[3]"'"." ".  $key[4]." ";
    				//	break;
    			case "Startwith":
    				$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " LIKE '".$key[3].'%'."'";
    				break;
    			case "Endwith":
    				$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " LIKE '".'%'.$key[3]."'";
    				break;
    			case "Contains":
    				$whereString .= $key[0].'.'.$field. " LIKE '".'%'.$key[3].'%'."'";
    				break;
    			case "IN": //specials
    				$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " IN (".$key[3].")".$type;
    				break;
    			case "NOT IN":
    				$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " NOT IN (".$key[3].")".$type;
    				break;
				case "Middle":
   					$whereString .=" ". $key[4]." ".$key[0].'.'.$field. " LIKE '".'%'.$key[3].'%'."'";
   					break;
    			case "GreaterThan":
    				$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " > '" .$key[3]."'";
    				break;
    			case "LessThan":
    				$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " < '" .$key[3]."'";
    				break;
    			case "GreaterThanEqual":
    				$whereString .= " ". $key[4]." ".$key[0].'.'.$field." >= '" .$key[3]."'";
    				break;
    			case "LessThanEqual":
    				$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " <= '" .$key[3]."'";
    				break;
    			case "IS NULL":
    				$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " IS NULL '" .$key[3]."'";
    				break;
    			case "NOT LIKE":
    				$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " NOT LIKE '" .$key[3]."'";
    				break;
    			case "IS NOT NULL":
    				$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " IS NOT NULL '" .$key[3]."'";
    				break;
    			case "Multiple":
    				$whereString .= " ". $key[4]." ".$key[7].'.'.$field. " = '".$key[3]."'";
    				break;
				case "Between":
    				$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " BETWEEN '".$from."' AND '".$to."'";
    				break;
				case "Custom":
					if(!isset($date_split[1])) {
						$date_split[1] = $date_split[0];
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$date_split[0]."' AND ".$field. " <= '".$date_split[1]."'";
					break;
				case "Yesterday":
					$date_value = $this->getdatevalue('-1 day',$current_date);
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " = '".$date_value['start']."'";
					break;
				case "Today":
					$date_value = $this->getdatevalue('0 day',$current_date);
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " = '".$date_value['start']."'";
					break;
				case "Tomorrow":
					$date_value = $this->getdatevalue('1 day',$current_date);
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " = '".$date_value['start']."'";
					break;
				case "Last 7 Days":
					$f_date = strtotime('-6 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Last 30 Days":
					$f_date = strtotime('-29 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Last 60 Days":
					$f_date = strtotime('-59 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Last 90 Days":
					$f_date = strtotime('-89 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Last 120 Days":
					$f_date = strtotime('-119 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Next 7 Days":
					$f_date = strtotime('6 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Next 30 Days":
					$f_date = strtotime('29 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Next 60 Days":
					$f_date = strtotime('59 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Next 90 Days":
					$f_date = strtotime('89 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Next 120 Days":
					$f_date = strtotime('119 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Last Week":
					$date_value = $this->datesystem('lastweek',$current_date);
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$date_value['start']."' AND ".$field. " <= '".$date_value['end']."'";
					break;
				case "Current Week":
					$date_value = $this->datesystem('currentweek',$current_date);
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$date_value['start']."' AND ".$field. " <= '".$date_value['end']."'";
					break;
				case "Next Week":
					$date_value = $this->datesystem('nextweek',$current_date);
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$date_value['start']."' AND ".$field. " <= '".$date_value['end']."'";
					break;
				case "Last Month":
					$date_value = $this->datesystem('lastmonth',$current_date);
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$date_value['start']."' AND ".$field. " <= '".$date_value['end']."'";
					break;
				case "Current Month":
					$date_value = $this->datesystem('currentmonth',$current_date);
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$date_value['start']."' AND ".$field. " <= '".$date_value['end']."'";
					break;
				case "Next Month":
					$date_value = $this->datesystem('nextmonth',$current_date);
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$date_value['start']."' AND ".$field. " <= '".$date_value['end']."'";
					break;
				case "Current and Previous Month":
					$date_value = $this->datesystem('currentandpreviousmonth',$current_date);
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$date_value['start']."' AND ".$field. " <= '".$date_value['end']."'";
					break;
				case "Current and Next Month":
					$date_value = $this->datesystem('currentandnextmonth',$current_date);
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$date_value['start']."' AND ".$field. " <= '".$date_value['end']."'";
					break;
				case "Before 7 Days":
					$f_date = strtotime('-6 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Before 30 Days":
					$f_date = strtotime('-29 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Before 60 Days":
					$f_date = strtotime('-59 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Before 90 Days":
					$f_date = strtotime('-89 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
				case "Before 120 Days":
					$f_date = strtotime('-119 day' ,strtotime($current_date));
					$f_date = date("Y-m-d",$f_date);
					if(strtotime($f_date) > strtotime($current_date)) {
						$startdate=$current_date;
						$enddate=$f_date;
					} else {
						$startdate=$f_date;
						$enddate=$current_date;
					}
					$whereString .= " ". $key[4]." ".$key[0].'.'.$field. " >= '".$startdate."' AND ".$field. " <= '".$enddate."'";
					break;
    			default:
    				$whereString .= "";
    				break;
    		}
    		/*}*/
    	}
    	return $whereString;
    }
	//picklist dependency data fetch for set attribute
	public function picklistgroupddfetch($moduleid,$tablename,$tabfieldname,$tabid) {
		$data = array();
		$i=0;
		$this->db->select('fieldname');
		$this->db->from('modulefield');
		$this->db->where_in('modulefield.uitypeid',18);
		$this->db->where('modulefield.moduletabid',$moduleid);
		$this->db->like('modulefield.ddparenttable',$tablename);
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0 ) {
			foreach($result->result() as $row) {
				$data[$i] = $row->fieldname;
			}
		}
		return $data;
	}
	//tree level generate
	public function gettreelevel($id,$table) {
		$info=$this->db->select($table.'level')
		->from($table)
		->where($table.'id',$id)
		->get()
		->row();
		$field = $table.'level';
		return ($info->$field + 1);
	}
	// Load => viewaggregate load based on UI tpye.
	public function loadaggregatemethodbyuitype() {
		$result=$this->db->select('aggregatemethodid',false)->where('uitypeid',$_GET['uitypeid'])
		->where_in('status',array(1,3))->get('uitype')->row()->aggregatemethodid;
		$result=explode(',',$result);
		$this->db->select('aggregatemethodid,aggregatemethodname,aggregatemethodkey,whereclauseid',false);
		$this->db->from('aggregatemethod');
		$this->db->where_in('aggregatemethod.aggregatemethodid',$result);
		$this->db->where_in('aggregatemethod.status',$this->Basefunctions->activestatus);
		$data=$this->db->get();
		if($data->num_rows() >0) {	
			$i=0;
			foreach($data->result()as $row) {
				$ddata[$i]=array('aggregatemethodname'=>$row->aggregatemethodname,'aggregatemethodkey'=>$row->aggregatemethodkey,'whereclauseid'=>$row->whereclauseid);
				$i++;
			}
			echo json_encode($ddata);
		} else {	
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//stock table data entry function
	public function stocktableentryfunction($primaryid,$moduleid) {
		/* $combovalues = $this->getcombovaluesusingmainquery($primaryid,$moduleid);
		for($k=0;$k<count($combovalues);$k++) {
			$grossweight = $combovalues[$k]['grossweight'];
			$netweight = $combovalues[$k]['netweight'];
			$pieces = $combovalues[$k]['pieces'];
			$stockdate = $combovalues[$k]['stockdate'];
			$branchid = $combovalues[$k]['branchid'];
			$purityid = $combovalues[$k]['purityid'];
			$productid = $combovalues[$k]['productid'];
			$counterid = $combovalues[$k]['counterid'];
			$stockreporttypeid = $combovalues[$k]['stockreporttypeid'];
			 $checkstockexist = $this->checkcomboisexistinstocktable($combovalues[$k]['stockdate'],$combovalues[$k]['branchid'],$combovalues[$k]['purityid'],$combovalues[$k]['productid'],$combovalues[$k]['counterid'],$combovalues[$k]['stockreporttypeid']);
			if($checkstockexist != 0) { //if stock already exist
				$this->updatestoclinclosewt($checkstockexist,$grossweight,$netweight,$pieces,$stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid);
			} else { //if stock not exist
				
				$this->stockdetailsdatainsertion($grossweight,$netweight,$pieces,$stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid);
			}
			//currentstock data entry
			$this->currentstockupdate($stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid);
		} */
	}
	//stock update on delete operation
	public function stockupdateondelete($primaryid,$moduleid) {
		/* $combovalues = $this->getcombovaluesusingmainquery($primaryid,$moduleid);
		if($combovalues != 'nodata') {
			for($k=0;$k<count($combovalues);$k++) {
				$grossweight = $combovalues[$k]['grossweight'];
				$netweight = $combovalues[$k]['netweight'];
				$pieces = $combovalues[$k]['pieces'];
				$stockdate = $combovalues[$k]['stockdate'];
				$branchid = $combovalues[$k]['branchid'];
				$purityid = $combovalues[$k]['purityid'];
				$productid = $combovalues[$k]['productid'];
				$counterid = $combovalues[$k]['counterid'];
				$stockreporttypeid = $combovalues[$k]['stockreporttypeid'];
				$operation = 'delete';
				//print_r($combovalues[$k]);
				$checkstockexist = $this->checkcomboisexistinstocktable($combovalues[$k]['stockdate'],$combovalues[$k]['branchid'],$combovalues[$k]['purityid'],$combovalues[$k]['productid'],$combovalues[$k]['counterid'],$combovalues[$k]['stockreporttypeid']);
				//update query to add/substract the stock data based on combo
				$this->updatestoclinclosewt($checkstockexist,$grossweight,$netweight,$pieces,$stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid,$operation);
				//currentstock data entry
				$this->currentstockupdate($stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid);
			}
		}
		//delete the stock data with opending and closing is zero
		$this->db->where('openinggrossweight',0);
		$this->db->where('closinggrossweight',0);
		$this->db->where('openingpieces',0);
		$this->db->where('closingpieces',0);
		$this->db->where_not_in('status',3);
		$this->db->delete('stock'); */
	}
	//check the value exist in stock table
	public function checkcomboisexistinstocktable($stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid) {
		$stockid= 0;
		$this->db->select('stockid');
		$this->db->from('stock');
		$this->db->where('stock.branchid',$branchid);
		$this->db->where('stock.purityid',$purityid);
		$this->db->where('stock.productid',$productid);
		$this->db->where('stock.counterid',$counterid);
		$this->db->where('stock.stockreporttypeid',$stockreporttypeid);
		$this->db->where('stock.stockdate',$stockdate);
		$data=$this->db->get();
		if($data->num_rows() >0) {
			foreach($data->result()as $row) {
				$stockid= $row->stockid;
			}
		} else {
			$stockid= 0;
		}
		return $stockid;
	}
	//stockdetails update
	public function updatestoclinclosewt($stockid,$grossweight,$netweight,$pieces,$stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid,$operation = 'other') {
		if($operation == 'delete'){
			if($grossweight >= 0 && $pieces >= 0) {
				$this->db->query('UPDATE stock SET ingrossweight = ingrossweight - '.$grossweight.',closinggrossweight = closinggrossweight - '.$grossweight.',innetweight = innetweight - '.$netweight.',closingnetweight = closingnetweight - '.$netweight.',inpieces = inpieces - '.$pieces.',closingpieces = closingpieces - '.$pieces.' WHERE stockid = '.$stockid.'');
			} else {
				$gwt = abs($grossweight);
				$nwt = abs($netweight);
				$pec = abs($pieces);
				//echo $stockid.'--'.$gwt.'---'.$nwt.'---'.$pec.'---';
				$this->db->query('UPDATE stock SET outgrossweight = outgrossweight - '.$gwt.',closinggrossweight = closinggrossweight + '.$gwt.',outnetweight = outnetweight - '.$nwt.',closingnetweight = closingnetweight + '.$nwt.',outpieces = outpieces - '.$pec.',closingpieces = closingpieces + '.$pec.' WHERE stockid = '.$stockid.'');
			}
		} 
		else {
			if($grossweight >= 0 && $pieces >= 0) {
				$this->db->query('UPDATE stock SET ingrossweight = ingrossweight + '.$grossweight.',closinggrossweight = closinggrossweight + '.$grossweight.',innetweight = innetweight + '.$netweight.',closingnetweight = closingnetweight + '.$netweight.',inpieces = inpieces + '.$pieces.',closingpieces = closingpieces + '.$pieces.' WHERE stockid = '.$stockid.'');
			} else {
				$gwt = abs($grossweight);
				$nwt = abs($netweight);
				$pec = abs($pieces);
				$this->db->query('UPDATE stock SET outgrossweight = outgrossweight +'.$gwt.',closinggrossweight = closinggrossweight -'.$gwt.',outnetweight = outnetweight + '.$nwt.',closingnetweight = closingnetweight - '.$nwt.',outpieces = outpieces + '.$pec.',closingpieces = closingpieces -'.$pec.' WHERE stockid = '.$stockid.'');
			}
		}
		$this->stockdetailsupdateafterstockdate($grossweight,$netweight,$pieces,$stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid,$operation); 
	}
	//stock details update based on the stock date
	public function stockdetailsupdateafterstockdate($grossweight,$netweight,$pieces,$stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid,$operation) { // update query for greater the stock date
		//echo $operation;
		if($operation == 'delete') {
			if($grossweight >= 0 && $pieces >= 0) {
				$this->db->query('UPDATE stock SET openinggrossweight = openinggrossweight - '.$grossweight.',closinggrossweight = closinggrossweight - '.$grossweight.',openingnetweight = openingnetweight - '.$netweight.',closingnetweight = closingnetweight - '.$netweight.',openingpieces = openingpieces - '.$pieces.',closingpieces = closingpieces - '.$pieces.' WHERE branchid = '.$branchid.' AND purityid = '.$purityid.' AND productid = '.$productid.' AND counterid ='.$counterid.' AND stockreporttypeid ='.$stockreporttypeid.' AND `stockdate` > "'.$stockdate.'"');
			} else {
				$gwt = abs($grossweight);
				$nwt = abs($netweight);
				$pec = abs($pieces);
				$this->db->query('UPDATE stock SET openinggrossweight = openinggrossweight + '.$gwt.',closinggrossweight = closinggrossweight + '.$gwt.',openingnetweight = openingnetweight + '.$nwt.',closingnetweight = closingnetweight + '.$nwt.',openingpieces = openingpieces + '.$pec.',closingpieces = closingpieces + '.$pec.' WHERE branchid = '.$branchid.' AND purityid = '.$purityid.' AND productid = '.$productid.' AND counterid ='.$counterid.' AND stockreporttypeid ='.$stockreporttypeid.' AND `stockdate` > "'.$stockdate.'"');
			}
		} else {
			if($grossweight >= 0 && $pieces >= 0) {
				$this->db->query('UPDATE stock SET openinggrossweight = openinggrossweight + '.$grossweight.',closinggrossweight = closinggrossweight + '.$grossweight.',openingnetweight = openingnetweight + '.$netweight.',closingnetweight = closingnetweight + '.$netweight.',openingpieces = openingpieces + '.$pieces.',closingpieces = closingpieces + '.$pieces.' WHERE branchid = '.$branchid.' AND purityid = '.$purityid.' AND productid = '.$productid.' AND counterid ='.$counterid.' AND stockreporttypeid ='.$stockreporttypeid.' AND `stockdate` > "'.$stockdate.'"');
			} else {
				$gwt = abs($grossweight);
				$nwt = abs($netweight);
				$pec = abs($pieces);
				$this->db->query('UPDATE stock SET openinggrossweight = openinggrossweight - '.$gwt.',closinggrossweight = closinggrossweight - '.$gwt.',openingnetweight = openingnetweight -'.$nwt.',closingnetweight = closingnetweight -'.$nwt.',openingpieces = openingpieces - '.$pec.',closingpieces = closingpieces -'.$pec.' WHERE branchid = '.$branchid.' AND purityid = '.$purityid.' AND productid = '.$productid.' AND counterid ='.$counterid.' AND stockreporttypeid ='.$stockreporttypeid.' AND `stockdate` > "'.$stockdate.'"');
			}
		}
	}
	//if stock data set not in the stock table mean insert the data using the following function
	public function stockdetailsdatainsertion($grossweight,$netweight,$pieces,$stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid) {
		//fetch all the details from the stock table  with combo except the date and where less than passed date along with fetching only one row with date in desc order and limit 0,1
		$lessdatedataset = $this->fetchstockdetailsleassthanpasseddate($stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid);
		$PMAX = 'No';//varialbe for identification of which if loop it went
		$closinggwt = 0;
		$closingnwt = 0;
		$closingpcs = 0;
		//check if any row is available if yes ->get closing wt & if not set closing weight as 0
		
		if(!empty($lessdatedataset)) {
			$PMAX ='Yes';
			$prevmaxdate = $lessdatedataset['stockdate'];
			$closinggwt = $lessdatedataset['clgwt'];
			$closingnwt = $lessdatedataset['clnwt'];
			$closingpcs = $lessdatedataset['clpcs'];
		} else {
			$PMAX ='No';
			$closinggwt = 0;
			$closingnwt = 0;
			$closingpcs = 0;
		}
		//stock data insetion based on the above values
		$this->stockdatainsertwithabovefetchdetails($stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid,$closinggwt,$closingnwt,$closingpcs);
		//update query
		$stockid = $this->checkcomboisexistinstocktable($stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid);
		$this->updatestoclinclosewt($stockid,$grossweight,$netweight,$pieces,$stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid);
	}
	//insert the missed date stock details
	public function insertmisseddatestockdetails($misseddatearray,$closingdetails,$stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid) {
		$defdate = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$status = 1;
		$i=0;
		foreach($misseddatearray as $misseddate) {
			$stockdata[$i] = array(
				'stockdate'=>$misseddate,
				'stockmaxdate'=>$misseddate,
				'branchid'=>$branchid,
				'purityid'=>$purityid,
				'productid'=>$productid,
				'counterid'=>$counterid,
				'stockreporttypeid'=>$stockreporttypeid,
				'openinggrossweight'=>$closingdetails['clgwt'],
				'ingrossweight'=>0,
				'outgrossweight'=>0,
				'closinggrossweight'=>$closingdetails['clgwt'],
				'openingnetweight'=>$closingdetails['clnwt'],
				'innetweight'=>0,
				'outnetweight'=>0,
				'closingnetweight'=>$closingdetails['clnwt'],
				'openingpieces'=>$closingdetails['clpcs'],
				'inpieces'=>0,
				'outpieces'=>0,
				'closingpieces'=>$closingdetails['clpcs'],
				'createdate'=>$defdate,
				'lastupdatedate'=>$defdate,
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
				'status'=>$status
			);
			$i++;
		}
		//print_r($stockdata);
		$this->db->insert_batch('stock',$stockdata);
	}
	//get closing weight values
	public function getclosingweightvalue($stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid) {
		$stockdetails= array();
		$data=$this->db->query('select stockid,closinggrossweight,closingnetweight,closingpieces from stock WHERE branchid = '.$branchid.' AND purityid = '.$purityid.' AND productid = '.$productid.' AND counterid ='.$counterid.' AND stockreporttypeid ='.$stockreporttypeid.' AND `stockdate` = "'.$stockdate.'"');
		if($data->num_rows() >0) {
			foreach($data->result()as $row) {
				$stockdetails= array('clgwt'=>$row->closinggrossweight,'clnwt'=>$row->closingnetweight,'clpcs'=>$row->closingpieces);
			}
		} else {
			$stockdetails= array();
		}
		return $stockdetails;
	}
	//fetch stock date leass than the pssed date
	public function fetchstockdetailsleassthanpasseddate($stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid) {
		$stockdetails= array();
		$data = $this->db->query('select stockid,closinggrossweight,closingnetweight,closingpieces,stockdate from stock WHERE branchid = '.$branchid.' AND purityid = '.$purityid.' AND productid = '.$productid.' AND counterid ='.$counterid.' AND stockreporttypeid ='.$stockreporttypeid.' AND `stockdate` < "'.$stockdate.' order by stockdate desc limit 0,1"');
		if($data->num_rows() >0) {
			foreach($data->result()as $row) {
					$stockdetails= array('stockdate'=>$row->stockdate,'clgwt'=>$row->closinggrossweight,'clnwt'=>$row->closingnetweight,'clpcs'=>$row->closingpieces);
			}
		} else {
			$stockdetails= array();
		}
		return $stockdetails;
	}
	//insert the stock data - in/out as zero and operninf and closing as closing weight that we got in above this function call
	public function stockdatainsertwithabovefetchdetails($stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid,$grosswt,$netwt,$pieces) {
		$stockdata = array(
			'stockdate'=>$stockdate,
			'stockmaxdate'=>$stockdate,
			'branchid'=>$branchid,
			'purityid'=>$purityid,
			'productid'=>$productid,
			'counterid'=>$counterid,
			'stockreporttypeid'=>$stockreporttypeid,
			'openinggrossweight'=>$grosswt,
			'ingrossweight'=>0,
			'outgrossweight'=>0,
			'closinggrossweight'=>$grosswt,
			'openingnetweight'=>$netwt,
			'innetweight'=>0,
			'outnetweight'=>0,
			'closingnetweight'=>$netwt,
			'openingpieces'=>$pieces,
			'inpieces'=>0,
			'outpieces'=>0,
			'closingpieces'=>$pieces
		);
		$defdataarr = $this->Crudmodel->defaultvalueget();
		$newstockdata = array_merge($stockdata,$defdataarr);
		$this->db->insert('stock', $newstockdata);
	}
	//get max date betwen the passed date and last active date
	public function fetchmaxdatebetweenpassedandlastactivedate($lastactivedate,$stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid) {
		
		$data = $this->db->query('select MAX(stockdate) as stockdate from stock WHERE branchid = '.$branchid.' AND purityid = '.$purityid.' AND productid = '.$productid.' AND counterid ='.$counterid.' AND stockreporttypeid ='.$stockreporttypeid.' AND `stockdate` < "'.$stockdate.'" AND `stockdate` > "'.$lastactivedate.'"');
		
		if($data->num_rows() >0) {
			foreach($data->result()as $row) {
				$stockdetails= $row->stockdate;
			}
		} else {
			$stockdetails= '';
		}
		return $stockdetails;
	}
	//get max date that should be greater than the given stok date
	public function fetchmaxdategreaterthanpassed($stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid) {
		
		$data = $this->db->query('select stockdate from stock WHERE branchid = '.$branchid.' AND purityid = '.$purityid.' AND productid = '.$productid.' AND counterid ='.$counterid.' AND stockreporttypeid ='.$stockreporttypeid.' AND `stockdate` > "'.$stockdate.'" order by stockdate asc limit 0,1');
		
		if($data->num_rows() >0) {
			foreach($data->result()as $row) {
				$stockdetails= $row->stockdate;
			}
		} else {
			$stockdetails = 'none';
		}
		return $stockdetails;
	}
	//used for getting the all data set for stock entry
	public function getcombovaluesusingmainquery($primaryid,$moduleid) {
		$ddata=array();
		if($moduleid == 50) { // for itemtag module
			$query = 'select itemtag.tagdate as stockdate,itemtag.branchid,itemtag.purityid,
			(CASE WHEN itemtag.tagtypeid in (2,3,4,5,13,14,16) THEN productid WHEN itemtag.tagtypeid in (11) THEN processproductid ELSE 1 END) as  productid,
			(CASE WHEN itemtag.tagtypeid in (2,4,5) THEN stockincounterid WHEN itemtag.tagtypeid in (3,11,13,14,16) THEN counterid ELSE 1 END) as  counterid,
			(CASE 
			 WHEN itemtag.tagtypeid in (2) THEN 2 
			 WHEN itemtag.tagtypeid in (4) THEN 4 
			 WHEN itemtag.tagtypeid in (5) THEN 5 
			 WHEN itemtag.tagtypeid in (3,16) THEN 3 
			 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in(5) THEN 5 
			 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in(6) THEN 4 
			 WHEN itemtag.tagtypeid = 13 and  itemtag.tagentrytypeid in(2,3) THEN 14 
			 WHEN itemtag.tagtypeid = 11 and  itemtag.tagentrytypeid in (2,3) THEN 6 ELSE 1 END) as  stockreporttypeid ,
			 COALESCE(sum(itemtag.grossweight),0) as grossweight,
			 COALESCE(sum(itemtag.netweight),0) as netweight,
			 COALESCE(sum(itemtag.pieces),0) as pieces
			 FROM itemtag
			 WHERE itemtag.status not in (0,3) and itemtag.itemtagid = '.$primaryid.'
			 group by branchid,purityid,productid,counterid,stockreporttypeid 
			
			 UNION  all
			
			select itemtag.tagdate,itemtag.branchid,itemtag.purityid,
			(CASE WHEN itemtag.tagentrytypeid in (2,3,5,6,10,12) THEN productid WHEN itemtag.tagentrytypeid in (4,11) THEN processproductid ELSE 1 END) as  productid,
			fromcounterid as  counterid,
			(CASE 
			WHEN itemtag.tagentrytypeid = 2  THEN 2
			WHEN itemtag.tagentrytypeid = 3  THEN 3 
			WHEN itemtag.tagentrytypeid = 4  THEN 16
			WHEN itemtag.tagentrytypeid = 5  THEN 5
			WHEN itemtag.tagentrytypeid = 6  THEN 4
			WHEN itemtag.tagentrytypeid = 10  THEN 7
			WHEN itemtag.tagentrytypeid = 11  THEN 6
			WHEN itemtag.tagentrytypeid = 12  THEN 8
			ELSE 1 END) as  stockreporttypeid ,
			-COALESCE(sum(itemtag.grossweight),0) as grossweight,
			-COALESCE(sum(itemtag.netweight),0) as netweight,
			-COALESCE(sum(itemtag.pieces),0) as pieces 
			FROM itemtag
			WHERE itemtag.status not in (0,3) and itemtag.itemtagid = '.$primaryid.'
			group by branchid,purityid,productid,counterid,stockreporttypeid';
		} else { //for sales module
			$query = 'select sales.salesdate as stockdate,sales.branchid,salesdetail.purityid,
			(CASE WHEN salesdetail.stocktypeid in (19,20,17,77,80,81,24,62,63,65,66) THEN productid WHEN salesdetail.stocktypeid in (13) THEN pendingproductid ELSE 1 END) as  productid,
			(CASE WHEN salesdetail.stocktypeid in (17,80,81,77,19,20,24,65,66) THEN counterid WHEN salesdetail.stocktypeid in (13,62,63) THEN processcounterid ELSE 1 END) as  counterid,
			(CASE 
			 WHEN salesdetail.stocktypeid in (13) THEN 8
			 WHEN salesdetail.stocktypeid in (19) THEN 6
			 WHEN salesdetail.stocktypeid in (20) THEN 7
			 WHEN salesdetail.stocktypeid in (17,80,81,77) and companysetting.purchaseproduct = 1 THEN 16
			 WHEN salesdetail.stocktypeid in (17,80,81,77) and companysetting.purchaseproduct = 2 THEN 3 
			 WHEN salesdetail.stocktypeid in (24) THEN 9
			 WHEN salesdetail.stocktypeid in (62,65) THEN 11
			 WHEN salesdetail.stocktypeid in (63,66) THEN 10
			 ELSE 1 END) as  stockreporttypeid ,
			 COALESCE(sum(salesdetail.grossweight),0) as grossweight,
			 COALESCE(sum(salesdetail.netweight),0) as netweight,
			 COALESCE(sum(salesdetail.pieces),0) as pieces
			 FROM salesdetail
			 join sales on sales.salesid = salesdetail.salesid
			 join companysetting on companysetting.companysettingid = 1
			 WHERE salesdetail.status not in (0,3) and sales.salestransactiontypeid != 16 and salesdetail.salesdetailid = '.$primaryid.'  and salesdetail.stocktypeid in (19,20,17,77,80,81,24,62,63,65,66,13)
			 group by branchid,purityid,productid,counterid,stockreporttypeid 
			
			 UNION  all
			 
			select sales.salesdate as stockdate,sales.branchid,salesdetail.purityid,
			(CASE WHEN salesdetail.stocktypeid in (11,74,13,12,16,62,63,25,65,66,73) THEN salesdetail.productid WHEN salesdetail.stocktypeid in (77) THEN pendingproductid ELSE 1 END) as  productid,
			(CASE WHEN salesdetail.stocktypeid in (11,74,13,12,16,25,62,63,73,49) THEN salesdetail.counterid WHEN salesdetail.stocktypeid in (65,66,77) THEN processcounterid ELSE 1 END) as  counterid,
			(CASE 
			 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 2 THEN 2
			 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 4 THEN 4
			 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 5 THEN 5
			 WHEN salesdetail.stocktypeid in (12,63,49) THEN 3 
			 WHEN salesdetail.stocktypeid in (16,66) THEN 10
			 WHEN salesdetail.stocktypeid in (74,65) THEN 11
			 WHEN salesdetail.stocktypeid in (77,25) THEN 9
			 WHEN salesdetail.stocktypeid = 73 THEN 14
			 ELSE 1 END) as  stockreporttypeid ,
			-COALESCE(sum(salesdetail.grossweight),0) as grossweight,
			-COALESCE(sum(salesdetail.netweight),0) as netweight,
			-COALESCE(sum(salesdetail.pieces),0) as pieces
			 FROM salesdetail
			 join sales on sales.salesid = salesdetail.salesid
			 join itemtag on itemtag.itemtagid = salesdetail.itemtagid
			 WHERE salesdetail.status not in (0,3) and sales.salestransactiontypeid != 16 and salesdetail.salesdetailid = '.$primaryid.' and salesdetail.stocktypeid in (11,74,13,12,16,62,63,25,65,66,73,77)
			 group by branchid,purityid,productid,counterid,stockreporttypeid';
		}
		$fquery = $this->db->query($query);
		$i=0;
		if($fquery->num_rows() > 0) {
			foreach($fquery->result()as $row) {
				$ddata[$i] = array('stockdate'=>$row->stockdate,'branchid'=>$row->branchid,'purityid'=>$row->purityid,'productid'=>$row->productid,'counterid'=>$row->counterid,'stockreporttypeid'=>$row->stockreporttypeid,'grossweight'=>$row->grossweight,'netweight'=>$row->netweight,'pieces'=>$row->pieces);
				$i++;
			}
			return $ddata;
		} else {
			return $ddata = array();
		}
	}
	//current stock entry based on the combo values
	public function currentstockupdate($stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid) {
		//get the close weight of combo		
		$currentclosewt = $this->currentweightvaluefetch($stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid);
		//delete exist combo from current stock
		$this->db->where('currentstock.branchid',$branchid);
		$this->db->where('currentstock.purityid',$purityid);
		$this->db->where('currentstock.productid',$productid);
		$this->db->where('currentstock.counterid',$counterid);
		$this->db->where('currentstock.stockreporttypeid',$stockreporttypeid);
		$this->db->delete('currentstock');
		//after delete insert the data
		$this->insercurrentstockdata($currentclosewt,$stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid);	
	}
	//get closing weight values
	public function currentweightvaluefetch($stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid) {
		$stockdetails= array();
		$data=$this->db->query('select t.stockdate as stockdate, t.branchid, t.purityid, t.productid, t.counterid, t.stockreporttypeid, t.closinggrossweight as openinggrossweight, 0 as ingrossweight, 0 as outgrossweight, t.closinggrossweight as  closinggrossweight, t.closingnetweight as openingnetweight, 0 as innetweight, 0 as outnetweight, t.closingnetweight as closingnetweight, t.closingpieces as openingpieces, 0 as inpieces, 0 as outpieces ,t.closingpieces as  closingpieces ,t.createdate ,t.lastupdatedate, t.createuserid, t.lastupdateuserid, t.status from stock t inner join 
		(SELECT *,MAX(stockdate) as max_date
		FROM stock
		WHERE branchid = '.$branchid.' AND purityid = '.$purityid.' AND productid = '.$productid.' AND counterid ='.$counterid.' AND stockreporttypeid ='.$stockreporttypeid.'
		GROUP BY  branchid ,purityid,productid,counterid,stockreporttypeid) a
		on a.branchid = t.branchid and a.purityid = t.purityid and a.productid = t.productid  and a.counterid = t.counterid 
		 and a.stockreporttypeid = t.stockreporttypeid  and a.max_date = t.stockdate');
		if($data->num_rows() >0) {
			foreach($data->result()as $row) {
				$stockdetails= array('clgwt'=>$row->closinggrossweight,'clnwt'=>$row->closingnetweight,'clpcs'=>$row->closingpieces);
			}
		} else {
			$stockdetails= array('clgwt'=>0,'clnwt'=>0,'clpcs'=>0);
		}
		return $stockdetails;
	}
	//inser current stock data with combo values
	public function insercurrentstockdata($currentclosewt,$stockdate,$branchid,$purityid,$productid,$counterid,$stockreporttypeid) {
		$currentstockdata = array(
			'stockdate'=>$stockdate,
			'branchid'=>$branchid,
			'purityid'=>$purityid,
			'productid'=>$productid,
			'counterid'=>$counterid,
			'stockreporttypeid'=>$stockreporttypeid,
			'grossweight'=>$currentclosewt['clgwt'],
			'netweight'=>$currentclosewt['clnwt'],
			'pieces'=>$currentclosewt['clpcs'],
		);
		$defdataarr = $this->Crudmodel->defaultvalueget();
		$currentnewstockdata = array_merge($currentstockdata,$defdataarr);
		$this->db->insert('currentstock', $currentnewstockdata);
	}
	//module list generate
	public function modulelistgenerate($modulelist) {
		$prevparenttitle = "";
		$m = 1;
		$tabind = 592;
		$modulelistdata = '';
		$data = array('modulelistdata'=>$modulelistdata,'numofcategory'=>$m);
		foreach($modulelist as $row) {
			if($row['parentname'] != "") {
				if($row['parentname'] != $prevparenttitle) {
					if($m !=1) {
						$modulelistdata .= '</ul></li>';
					}
					$modulelistdata .= '<li class="main-category leftmenucategory">';
					if($row['typeid'] == 1) {
						$modulelistdata .= '<a class="module"  href="'.base_url().$row['link'].'" tabindex="'.$tabind.'">';
					} else {
						$modulelistdata .= '<a class="category" tabindex="'.$tabind.'">';
					}
					$modulelistdata .= '<span class="category-title maincattitle">'.$row['parentname'];
					$modulelistdata .= '</span>';
					$modulelistdata .= '</a>';
					$modulelistdata .= '<ul class="sub-menu sub-menu-1">';
					$prevparenttitle = $row['parentname'];
					if($row['typeid'] == 2) {
						$data = $this->submodulelistgenerate($row['modid'],$tabind);
						$modulelistdata .= $data['modulelistdata'];
						$tabind = $data['tabindex'];
					}
					
					$m++;
				}
				$tabind++;
			}
		}
		$data = array('modulelistdata'=>$modulelistdata,'numofcategory'=>$m);
		return $data;
	}
	//here we will generate the sub module list of main modules
	public function submodulelistgenerate($parentcatid,$tabind) {
		$submodulelist = $this->usermenugeneration($parentcatid);
		$submodulelistdata = '';
		$k=1;
		$data = array('modulelistdata'=>'','tabindex'=>$tabind);
		foreach($submodulelist as $srow) {
			if($srow['typeid'] == 1) {
				$submodulelistdata .= '<li class="sub-category contentlist">';
				if($srow['mtype'] == 'External')
				{
					$submodulelistdata .= '<a class="module" href="'.$srow['link'].'" tabindex="'.$tabind.'">';
				}else{
					$submodulelistdata .= '<a class="module" href="'.base_url().$srow['link'].'" tabindex="'.$tabind.'">';
				}
			} else {
				$submodulelistdata .= '<li class="sub-category">';
				$submodulelistdata .= '<a class="category" tabindex="'.$tabind.'">';
			}
			$submodulelistdata .= '<span class="category-title">'.$srow['parentname'];
			$submodulelistdata .= '</span>';
			$submodulelistdata .= '</a>';
			if($srow['typeid'] == 2) {
				$submodulelistdata .= '<ul class="sub-menu sub-menu-2">';
				$data = $this->submodulelistgenerate($srow['modid'],$tabind);
				$submodulelistdata .= $data['modulelistdata'];
				$tabind = $data['tabindex'];
				$submodulelistdata .= '</ul>';
			}
			$submodulelistdata .= '</li>';
			$tabind++;
			$k++;
		}
		$data = array('modulelistdata'=>$submodulelistdata,'tabindex'=>$tabind);
		return $data;
	}
	//public function get all dropdowns
	public function getalldropdownbasedonmodule($modid) {
		//17,18,19,20,21,23,25,26,27,28,29
		$uitype = array(17,18,19,20,21,23,25,26,27,28,29,13);
		$industryid = $this->industryid;
		$i=0;
		$data= array();
		$this->db->select('viewcreationcolumnid,viewcreationcolmodelindexname,viewcreationcolumnname,viewcreationparenttable,multiple,uitypeid,viewcreationcolumns.viewcreationmoduleid');
		$this->db->from('viewcreationcolumns');
		$this->db->join('viewcreationmodule','viewcreationmodule.viewcreationmoduleid=viewcreationcolumns.viewcreationmoduleid','left outer');
		$this->db->join('moduletabsection','moduletabsection.moduletabsectionid=viewcreationcolumns.moduletabsectionid','left outer');
		$this->db->join('module','module.moduleid=moduletabsection.moduleid','left outer');
		$this->db->join('moduleinfo','moduleinfo.moduleid=module.moduleid','left outer');
		$this->db->where('viewcreationcolumns.status',1);
		$this->db->where('moduleinfo.status',1);
		$this->db->where_in('viewcreationcolumns.uitypeid',$uitype);
		$this->db->where_in('viewcreationcolumns.viewcreationmoduleid',$modid);
		$this->db->where("FIND_IN_SET('$industryid',module.industryid) >", 0);
		$this->db->group_by("viewcreationcolumns.viewcreationcolumnid");
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row){
				$data[$i] = array('columnaname'=>$row->viewcreationcolmodelindexname,'fieldlabel'=>$row->viewcreationcolumnname,'parenttable'=>$row->viewcreationparenttable,'multiple'=>$row->multiple,'uitypeid'=>$row->uitypeid,'moduletabid'=>$row->viewcreationmoduleid,'modulefieldid'=>$row->viewcreationcolumnid);
				$i++;
			}
		}
		return $data;
	}
	//display all dropdowns
	public function showalldropdowns($moduledd) {
		$content = '';
		$multattr = '';
		$tabindex = 10000;
		$i=0;
		$data = array('content'=>$content,'length'=>$i);
		foreach($moduledd as $row) {
			$multiple = $row['multiple'];
			$moduleid = $row['moduletabid'];
			$tablename = substr($row['columnaname'],0,-4);
			$fieldprimaryid = $tablename.'id';
			$tabfieldname = $tablename.'name';
			$multattr = 'multiple=multple';
			if($row['uitypeid'] == 20){
				$mandlab = "";
				$txtval="";
				$mandatoryopt = ""; //mandatory option set
				$divattr = array('class'=>'elementlist large-12 columns static-field','id'=>'');
				$labval = $row['fieldlabel'].$mandlab;
				$labattr = array();
				$name = 'ddfilerchange'.$i;
				$value = array();
				$defval = array();
				$value = array();
				$dataval = array();
				$groupid = array();
				$ddowndata = $this->Basefunctions->userspecificgroupdropdownvalfetch();
				$prev = ' ';
				$grpname = '';
				for ($k=0;$k<count($ddowndata);$k++) {
					$cur = $ddowndata[$k]['PId'];
					$a ="";
					if($prev != $cur) {
						if( $prev != '' ) {
							$value[$grpname] = $dataval;
						}
						$dataval = array();
						$dataval[$ddowndata[$k]['CId']] = $ddowndata[$k]['CName'];
						$groupid[] = $ddowndata[$k]['PId'];
						$prev = $ddowndata[$k]['PId'];
						$grpname = $ddowndata[$k]['PName'];
					} else {
						$dataval[$ddowndata[$k]['CId']] = $ddowndata[$k]['CName'];
						$groupid[] = $ddowndata[$k]['PId'];
					}
				}
				$value[$grpname] = $dataval;
				$txtval = ( ($txtval!='')? $txtval : 1 );
				$valoption = "id";
				$dataattr = array();
				$multiple = '';
				$seldataattr = array("placeholder"=>"",'defvalattr'=>''.$txtval.'','modulefieldid'=>''.$row['modulefieldid'].'','uitype'=>''.$row['uitypeid'].'');
				$option = array("class"=>"chzn-select ddfilerchange ".$mandatoryopt."","tabindex"=>"".$tabindex."","data"=>$seldataattr,'data-prompt-position'=>'bottomLeft:14,36');
				$hidname = '';
				$hidval = '';
				$content .= groupdropdownspanuserspecific($divattr,$labval,$labattr,$name,$value,$valoption,$dataattr,$multiple,$option,$groupid,$hidname);
			} else if($row['uitypeid'] == 13) {
				$content .= '<div class="input-field large-12 medium-12 small-12 columns" >';
				$content .= '<input type="checkbox" name="ddfilerchange'.$i.'" id="ddfilerchange'.$i.'" data-table="'.$tablename.'"  data-hidname="checkbox'.$i.'"  data-fieldname="'.$tabfieldname.'" data-fieldid="'.$fieldprimaryid.'" data-modulefieldid="'.$row['modulefieldid'].'" data-uitype="'.$row['uitypeid'].'" class="ddfilerchange checkboxcls radiochecksize filled-in" tabindex="'.$tabindex.'" /><label for="ddfilerchange'.$i.'">'.$row['fieldlabel'].'</label><input name="checkbox'.$i.'" id="checkbox'.$i.'" value="No" data-defvalattr="No" type="hidden">';
				$content .= '</div>';
			} else {
				$content .= '<div class="elementlist large-12 medium-12 small-12 columns static-field" ><div class="" id=""><label>'.$row['fieldlabel'].'</label><select data-placeholder="" data-prompt-position="topLeft:14,36" id="ddfilerchange'.$i.'" name="ddfilerchange'.$i.'" data-table="'.$tablename.'"  data-fieldname="'.$tabfieldname.'" data-fieldid="'.$fieldprimaryid.'" data-modulefieldid="'.$row['modulefieldid'].'" data-uitype="'.$row['uitypeid'].'" class="chzn-select ddfilerchange" tabindex="'.$tabindex.'" '.$multattr.'><option></option>';
				if($row['uitypeid'] == 17 || $row['uitypeid'] == 18 || $row['uitypeid'] == 28 || $row['uitypeid'] == 25) {
					if($row['modulefieldid'] == 3013) { //gst based tax display
						$dddata = $this->vendornamedisplay($tablename,$tabfieldname,$fieldprimaryid,'');
						foreach($dddata as $key) {
							$content .= '<option value="'.$key->Id.'" data-id"'.$key->Id.'" data-name"'.$key->Name.'">'.$key->Name.'</option>';
						}
					} else {
						$dddata = $this->dynamicdropdownvalfetch($moduleid,$tablename,$tabfieldname,$fieldprimaryid);
						foreach($dddata as $key) {
							$content .= '<option value="'.$key->Id.'" data-id"'.$key->Id.'" data-name"'.$key->Name.'">'.$key->Name.'</option>';
						}
					}
				} else if($row['uitypeid'] == 19 || $row['uitypeid'] == 21) {
					if($row['modulefieldid'] == 136) { //for size master dd
						$dddata = $this->sizemasterdisplay($tablename,$tabfieldname,$fieldprimaryid,'');
						foreach($dddata as $key) {
							$content .= '<option value="'.$key->Id.'" data-id"'.$key->Id.'" data-name"'.$key->Name.'">'.$key->productname.' - '.$key->Name.'</option>';
						}
					} else if($row['modulefieldid'] == 148) { //vedor name display
						$dddata = $this->vendornamedisplay($tablename,$tabfieldname,$fieldprimaryid,'');
						foreach($dddata as $key) {
							$content .= '<option value="'.$key->Id.'" data-id"'.$key->Id.'" >'.$key->Name.'</option>';
						}
					} else {
						$dddata = $this->Basefunctions->dynamicdropdownvalfetchparent($tablename,$tabfieldname,$fieldprimaryid,'');
						foreach($dddata as $key) {
							$content .= '<option value="'.$key->Id.'" data-id"'.$key->Id.'" data-name"'.$key->Name.'">'.$key->Name.'</option>';
						}
					}
				} else if($row['uitypeid'] == 27) {
					$dddata = $this->dynamicmoduledropdownvalfetch($moduleid);
					foreach($dddata as $key) {
						$content .= '<option value="'.$key->Id.'" data-id"'.$key->Id.'" data-name"'.$key->Name.'">'.$key->Name.'</option>';
					}
				}
				$content .='</select></div></div>';
			}
			$tabindex++;
			$i++;
		}
		$data = array('content'=>$content,'length'=>$i);
		return $data;
	}
	//display size master
	public function sizemasterdisplay($tablename,$tabfieldname,$fieldprimaryid,$default) {
		$level = "";
		$industryid = $this->Basefunctions->industryid;
		$this->db->select(''.$fieldprimaryid.' AS Id'.','.''.$tabfieldname.' AS Name,product.productname');
		$this->db->from($tablename);
		if($level != "") {
			$this->db->where(''.$tablename.'.'.$tablename.'level <=',$default);
		}
		$this->db->join("product",'product.productid=sizemaster.productid');
		$this->db->where("FIND_IN_SET('".$industryid."',".$tablename.".industryid) >", 0);
		$this->db->where(''.$tablename.'.status', 1);
		$this->db->order_by(''.$tablename.'.'.$tabfieldname.'','asc');
        $query= $this->db->get();
        return $query->result();
	}
	//display vendor name
	public function vendornamedisplay($tablename,$tabfieldname,$fieldprimaryid,$default) {
		$level = "";
		$industryid = $this->Basefunctions->industryid;
		$this->db->select(''.$fieldprimaryid.' AS Id'.','.''.$tabfieldname.' AS Name');
		$this->db->from($tablename);
		if($level != "") {
			$this->db->where(''.$tablename.'.'.$tablename.'level <=',$default);
		}
		if($tablename == 'taxmaster'){
			$this->db->where("taxmaster.gsttax",'Yes');
		} else {
			$this->db->where("account.accounttypeid",16);
		}
		$this->db->where("FIND_IN_SET('".$industryid."',".$tablename.".industryid) >", 0);
		$this->db->where(''.$tablename.'.status', 1);
		$this->db->order_by(''.$tablename.'.'.$tabfieldname.'','asc');
        $query= $this->db->get();
        return $query->result();
	}
	//user icon profile overlay display
	public function userprofileiconoverlay() {
		$empid = $this->logemployeeid;	
		$this->db->select('employeename');
		$this->db->from('employee');
		$this->db->where('employee.employeeid',$empid);
		$data=$this->db->get();
		foreach($data->result()as $row) {
			$name= $row->employeename;
		}
		$profileoverlay = '';
		$profileoverlay .= '<ul id="userdrop" class="action-drop arrow_box" style="position:absolute;top:43px !important;background:#546e7a;width: 200px;">
		<li id="peronalsettingiconcheck" style="color:white;background-color:#2c2f48;height: 60px;    top: -8px; position: relative;border-top-left-radius: 4px;border-top-right-radius: 4px;"><i title="profileimage" id="profileimage" class="material-icons" style="font-size: 2rem;line-height: 1.9;color: #fff;">account_circle</i><span style="position:relative;top:-12px;color:#fff !important;font-size:0.9rem;font-family:"Nunito, sans-serif;"">'.$name.'</span></li>
		<li id="peronalsettingiconmain" style="color:white;"><i title="personalsettings" id="personalsettingsicon" class="material-icons" style="font-size: 1.4rem;line-height: 1.0;color: #788db4;">settings</i><span style="position:relative;top:-5px;">Personal Settings</span></li>';
		if($empid=='2') {
			$ddataset = $this->checkdemodatadetails();
			$disp = (($empid!='2' || $ddataset['demodata'] == 'No')?' hidedisplay':'');
			$sicondisp = (($ddataset == 'Yes')?' style="display:none"':'');
			$profileoverlay .=  '<li id="dataclearicon" class="'.$disp.'" style="color:white;"><i title="demodata" id="demodataicon" class="material-icons" style="font-size: 1.4rem;line-height: 2.0rem;color: #788db4;">clear_all</i><span style="position:relative;top:-5px;">Clear Demo Data</span></li>';
		}
		$profileoverlay .=  '<li id="logouticon" style="color:white;"><i title="logout" id="logouticon" class="material-icons" style="font-size: 1.4rem;line-height:2rem;color: #788db4;">power_settings_new</i><span style="position:relative;top:-5px;">Logout</span></li>';
		$profileoverlay .=  '</ul>';
		return $profileoverlay;
	}
	//get all module based field except the dropdown for filter
	public function filterdataviewdropdowncolumns($moduleids) {
		$industryid = $this->industryid;
		$query=$this->Basefunctions->checkthemoduleexistinmoduletabsec($moduleids);
		if($query->available) {
			$this->db->select('viewcreationcolumns.viewcreationcolumnid,viewcreationcolumns.viewcreationcolumnname,moduletabsection.moduletabsectionname,viewcreationcolumns.moduletabsectionid,viewcreationcolumns.uitypeid,viewcreationcolumns.fieldrestrict,viewcreationcolumns.fieldview,viewcreationcolumns.viewcreationcolmodelindexname,uitype.aggregatemethodid,module.modulename,viewcreationcolumns.viewcreationmoduleid',false);
			$this->db->from('viewcreationcolumns');
			$this->db->join('viewcreationmodule','viewcreationmodule.viewcreationmoduleid=viewcreationcolumns.viewcreationmoduleid','left outer');
			$this->db->join('moduletabsection','moduletabsection.moduletabsectionid=viewcreationcolumns.moduletabsectionid','left outer');
			$this->db->join('moduleinfo','moduleinfo.moduleid=moduletabsection.moduleid','left outer');
			$this->db->join('module','module.moduleid=moduleinfo.moduleid','left outer');
			$this->db->join('uitype','viewcreationcolumns.uitypeid=uitype.uitypeid','left outer');
			$this->db->where_in('viewcreationcolumns.viewcreationmoduleid',$moduleids);
			$this->db->where('viewcreationcolumns.status',$this->activestatus);
			$this->db->where('moduleinfo.userroleid',$this->userroleid);
			$this->db->where('moduleinfo.status',$this->activestatus);
			$this->db->where("FIND_IN_SET('$industryid',module.industryid) >", 0);
			$this->db->where_not_in('viewcreationcolumns.fieldrestrict',array(1));
			$this->db->where_not_in('uitype.uitypeid',array(17,18,19,20,21,23,25,26,27,28,29,13));
			$this->db->where_not_in('viewcreationcolumns.viewcreationcolumnviewtype',array(0));
			$this->db->order_by('moduletabsection.moduletabsectionid',"ASC");
			$this->db->order_by('viewcreationcolumns.viewcreationcolumnid',"ASC");
			$query=$this->db->get();
			return  $query->result();
		} else {
			$this->db->select('viewcreationcolumns.viewcreationcolumnid,viewcreationcolumns.viewcreationcolumnname,viewcreationcolumns.moduletabsectionid,viewcreationcolumns.uitypeid,viewcreationcolumns.fieldrestrict,viewcreationcolumns.fieldview,viewcreationcolumns.viewcreationcolmodelindexname,uitype.aggregatemethodid',false);
			$this->db->from('viewcreationcolumns');
			$this->db->join('viewcreation','viewcreation.viewcreationmoduleid=viewcreationcolumns.viewcreationmoduleid','left outer');
			$this->db->join('viewcreationmodule','viewcreationmodule.viewcreationmoduleid=viewcreation.viewcreationmoduleid','left outer');
			$this->db->join('uitype','viewcreationcolumns.uitypeid=uitype.uitypeid','left outer');
			$this->db->where_in('viewcreationcolumns.viewcreationmoduleid',$moduleids);
			$this->db->where('viewcreationcolumns.status',$this->activestatus);
			$this->db->where_not_in('viewcreationcolumns.fieldrestrict',array(1));
			$this->db->where_not_in('uitype.uitypeid',array(17,18,19,20,21,23,25,26,27,28,29,13));
			$this->db->where_not_in('viewcreationcolumns.viewcreationcolumnviewtype',array(0));
			$this->db->order_by('viewcreationcolumns.viewcreationcolumnid',"ASC");
			$query=$this->db->get();
			return  $query->result();
		}
	}
	// user assign validate
	public function assignuservalid(){
		$id = $_POST['id'];
		$columnname = $_POST['columnname'];
		$table = $_POST['table'];
		$this->db->select($columnname);
		$this->db->from($table);
		$this->db->where("status",$this->Basefunctions->activestatus);
		$this->db->where($columnname,$id);
		$result = $this->db->get();
		$dataarray = array('rows'=>$result->num_rows());
		echo json_encode($dataarray);
	}
	//request a training mail sent
	public function requestatrainigmailsentmodel() {
		$this->load->helper('phpmailer');
		$name = $_POST['rname'];
		$mobile = $_POST['rmobile'];
		$trainingfor = $_POST['trainingfor'];
		$date = $_POST['rdate'];
		$time = $_POST['rtime'];
		$comments = $_POST['rcomments'];
		$logempid = $this->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$logempid);
		$empemail = $this->Basefunctions->generalinformaion('employee','emailid','employeeid',$logempid);
		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->SMTPAuth   = true;
		$mail->SMTPSecure = "tls";
		$mail->Username   = "arvind@salesneuron.com";
		$mail->Password   = "X9WnMXpxhC8YEOUPuQ3oZw";
		$mail->Host       = "smtp.mandrillapp.com";
		$mail->Port       = 587;
		$mail->SetFrom($empemail,'[Sales Neuron]'.$empname.'');
		$mail->Subject   = "Sales Neuron: Request a Training";
		$expdate = date("YmdHis", strtotime('+1 days'));
		//new
		$data = 'Name: '.$name.'</br>';
		$data .= 'Mobile: '.$mobile.'</br>';
		$data .= 'Training For: '.$trainingfor.'</br>';
		$data .= 'Date: '.$date.'</br>';
		$data .= 'Time: '.$time.'</br>';
		$data .= 'Comments: '.$comments.'</br>';
		$body='<html><body>'.$data.'</body></html>';
		$newbody = $body;
		$mail->IsHTML(true);
		$mail->MsgHTML($newbody);
		$mail->AddAddress('arvind@aucventures.com');
		$mail->AddBCC('gowtham@aucventures.com');
		$mail->AddBCC('madasamy@aucventures.com');
		$mail->AltBody="Request a Training";
		if($mail->send()) {
			echo 'success';
		} else {
			echo 'fail';
		}
	}
	//retrieve getcompanyname
	public function getcompanydetails() {
		$userid=$this->Basefunctions->userid;		
		$data=$this->db->select('company.companyname,company.mastercompanyid')
						->from('company')
						->join('branch','branch.companyid=company.companyid')
						->join('employee','employee.branchid=branch.branchid')
						->where('employee.employeeid',$userid)
						->limit(1)
						->get();
		foreach($data->result() as $info) {
			$company['name'] = $info->companyname;
			$company['mastercompanyid'] = $info->mastercompanyid;
		}
		return $companyname;
	}
	//upgrade plan mail sent
	public function upgradeplanmailsentmodel() {
		$this->load->helper('phpmailer');
		$name = $_POST['uname'];
		$mobile = $_POST['umobile'];
		$companyname = $_POST['ucompanyname'];
		$comments = $_POST['ucomments'];
		$logempid = $this->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$logempid);
		$empemail = $this->Basefunctions->generalinformaion('employee','emailid','employeeid',$logempid);
		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->SMTPAuth   = true;
		$mail->SMTPSecure = "tls";
		$mail->Username   = "arvind@salesneuron.com";
		$mail->Password   = "X9WnMXpxhC8YEOUPuQ3oZw";
		$mail->Host       = "smtp.mandrillapp.com";
		$mail->Port       = 587;
		$mail->SetFrom($empemail,'[Sales Neuron]'.$empname.'');
		$mail->Subject   = "Sales Neuron: Upgrade Plan";
		$expdate = date("YmdHis", strtotime('+1 days'));
		//new
		$data = 'Name : '.$name.'</br>';
		$data .= 'Mobile : '.$mobile.'</br>';
		$data .= 'Company Name : '.$companyname.'</br>';
		$data .= 'Mastercompanyid : '.$umastercompid.'</br>';
		$data .= 'Comments : '.$comments.'</br>';
		$body='<html><body>'.$data.'</body></html>';
		$newbody = $body;
		$mail->IsHTML(true);
		$mail->MsgHTML($newbody);
		$mail->AddAddress('arvind@aucventures.com');
		$mail->AddBCC('gowtham@aucventures.com');
		$mail->AddBCC('madasamy@aucventures.com');
		$mail->AltBody="Upgrade Pan";
		if($mail->send()) {
			echo 'success';
		} else {
			echo 'fail';
		}
	}
	//non-base module based tab section fetch in report
	public function fetchmodueltabsectiotable($reportmodulesectionid) {
		$this->db->select('viewcreationparenttable');
		$this->db->from('viewcreationcolumns');
		$this->db->where('viewcreationcolumns.moduletabsectionid',$reportmodulesectionid);
		$this->db->group_by('viewcreationcolumns.moduletabsectionid');
		$this->db->limit(1,0);
		$data = $this->db->get();
		foreach($data->result() as $datas) { 
			return $datas->viewcreationparenttable;
		}
	}
	//filter data display for nonbase module in main view with overlayform
	public function filterdisplayinnonbasemodule($moduleid,$gridid) {
		$marray = array($moduleid);
		$filtertabgroup = $this->moduletabgroupdetailsfetch($marray);
		$filtertabsecfield = $this->moduletabsectiondetailsfetch($marray);
		$modulename = $this->modulelinkfetch('module','modulelink','moduleid',$moduleid);
		$modname = strtolower($modulename);
		$filtercount = count($filtertabgroup);
		$appdateformat = $this->appdateformatfetch();
		$content = '';
		$content .= '<!--<div class="large-3 medium-3 columns paddingzero">-->	
		<div class="large-12 small-12 medium-12 columns" style="position:absolute;">
		<div class="overlay large-12 columns mblnopadding" id="filterdisplaymainviewnonbase" style="z-index: 40; height: 100% !important; display: none;">
			<div class="large-4 large-offset-8 columns" style="padding-right: 0em !important;height:100% !important;padding-left:0px !important;padding: 0px !important;">
				<div class="large-12 column paddingzero" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff; height: 100% !important;">
						<div class="searchfilteroverlay filterbox ui-draggable borderstylefilter" style="margin: 0px 10px 10px 0px; touch-action: none; -moz-user-select: none;box-shadow:none;">
							<div class="large-12 columns noticombowidget-box filterheaderborderstyle">
								<div class="large-12 medium-12 columns end noticomboheader activeheader filertabbgcolor" id="filterview" style="padding-top:0.4rem!important;padding-left:0.7rem !important;padding-bottom:0.5rem!important" data-griddataid="'.$gridid.'">
									<span id="fiteractive" class="tab-titlemini sidebariconsmini active filtermini">
										<div class="large-4 columns sectionheaderformcaptionstyle" style="text-align: left;top: -10px !important;">Filter</div>
										<!--<div class="large-4 columns sectionheaderformcaptionstyle filterdisplaymainviewclearclose" style="text-align: left;top: -10px !important;">Clear</div>-->
										<input type="button" class="alertbtnno addsectionclose leftcloseposition nbfilterdisplaymainviewclose" value="Close" tabindex="130" style="left: 10em;">
									</span>
								</div>
							<!--<div class="large-4 medium-4 columns end noticomboheader activeheader" id="minidashboard" style="padding-top:0.4rem!important;padding-left:0.2rem !important;padding-bottom:0.5rem!important" data-griddataid="'.$gridid.'">
									<span id="dashboardactive" class="tab-titlemini sidebariconsmini dashboardmini">
										<span>Dashboard</span>
									</span>
								</div>
							<div class="large-6 medium-6 columns end noticomboheader activeheader" id="mininotes" style="padding-top:0.4rem!important;padding-left:0.7rem !important;padding-bottom:0.5rem!important" data-griddataid="'.$gridid.'">
								<span id="notiactive" class="tab-titlemini sidebariconsmini notesmini">									
									<span>Notes</span>
								</span>
							</div>-->
						</div>';
		$content .= '<div class="filterview1">
				<div class="row">&nbsp;</div>
				<div class="large-12 medium-12 small-12 columns end filterheight filterbodyborderstyle" id="fbuildercat1" style="padding:10px 15px;overflow-y:auto;overflow-x:hidden;background-color: rgb(236, 239, 241);box-shadow: none;">
				<div class="wrappercontent">
				<ul class="fbulderheadermenu">
				<span id="'.$modname.'filterformconditionvalidation" class="validationEngineContainer" height: 100% !important;>';
		for($fl=0;$fl<$filtercount;$fl++) {
			$content .='<div class= elementlist'.$fl.'" style="">
									<ul class="elementiconsul" style="padding-left: 1rem;padding-right: 1rem;">
									<div class="" id=""><label>'.$filtertabgroup[$fl]['tabsecname'].'<span class="mandatoryfildclass">*</span></label><select data-placeholder="Select Columns" data-prompt-position="topLeft:14,36" id="'.$modname.'filtercolumn" class="validate[required] chzn-select dropdownchange multicolumnfilter" name="'.$modname.'filtercolumn" tabindex="9"><option></option>';
			$prev = " ";
			foreach($filtertabsecfield as $key) {
				if($filtertabgroup[$fl]['tabsecid'] == $key->moduletabsectionid){
					$cur = $key->moduletabsectionid;
					$content .='<option value="'.$key->viewcreationcolumnid.'" data-indexname="'.$key->viewcreationcolmodelindexname.'" data-uitype="'.$key->uitypeid.'" data-modname="'.$modname.'" data-label="'.$key->viewcreationcolumnname.'" data-moduleid="'.$key->viewcreationmoduleid.'" data-fldview="'.$key->fieldview.'" data-fldrestrict="'.$key->fieldrestrict.'">'.$key->viewcreationcolumnname.'</option>';
				}
			}
			$content .='</select></div></ul></div>';
		}
		$content .='<div class="large-12 columns viewcondclear">
						<label>Condition <span class="mandatoryfildclass">*</span></label>
						<select data-placeholder="" data-prompt-position="bottomLeft:14,36" class="validate[required] chzn-select chosenwidth " tabindex="11" name="'.$modname.'filtercondition" id="'.$modname.'filtercondition">
							<option></option>
							<option value="Equalto">Equalto</option>
							<option value="NotEqual">NotEqual</option>
							<option value="Startwith">Startwith</option>
							<option value="Endwith">Endwith</option>
							<option value="Middle">Middle</option>
						</select>
					</div>
					<div class="input-field large-12 columns viewcondclear " id="'.$modname.'filtercondvaluedivhid">
						<input type="text" class="validate[required,maxSize[100]]" id="'.$modname.'filtercondvalue" name="'.$modname.'filtercondvalue"  tabindex="12" >
						<label for="'.$modname.'filtercondvalue">Value <span class="mandatoryfildclass">*</span></label>
					</div>
					<div class="large-12 columns viewcondclear hidedisplay" id="'.$modname.'filterddcondvaluedivhid">
						<label>Value<span class="mandatoryfildclass">*</span></label>
						<select data-placeholder="" data-prompt-position="bottomLeft:14,36" class="validate[required] chzn-select chosenwidth valdropdownchange" tabindex="13" name="'.$modname.'filterddcondvalue" id="'.$modname.'filterddcondvalue">
							<option></option>
						</select>
					</div>
					<script>
					$(document).ready(function(){
						$("#'.$modname.'filterdatecondvalue").datetimepicker({
							minDate: null,
							maxDate: null,
							onSelect: function () {
								var checkdate = $(this).val();
								if(checkdate != "") {
									$(this).next("span").remove(".btmerrmsg"); 
									$(this).removeClass("error");
									$(this).prev("div").remove();
								}
							},
							onClose: function () {
								$("#'.$modname.'filterdatecondvalue").focus();
							}
						}).click(function() {
							$(this).datetimepicker("show");
						});
					});
					</script>
					<div class="large-12 columns viewcondclear hidedisplay" id="'.$modname.'filterdatecondvaluedivhid">
						<label>Value <span class="mandatoryfildclass">*</span></label>
						<input type="text" class="validate[required]" data-dateformater="'.$appdateformat.'" id="'.$modname.'filterdatecondvalue" name="'.$modname.'filterdatecondvalue" tabindex="12" >
					</div>
					<div class="large-12 columns viewcondclear hidedisplay">
						<input type="hidden" class="" id="'.$modname.'finalfiltercondvalue" name="'.$modname.'finalfiltercondvalue"  tabindex="12" >
						<input type="hidden" class="" id="'.$modname.'filterfinalviewconid" name="'.$modname.'filterfinalviewconid"  tabindex="12" >
					</div>
					<div class="large-12 columns">&nbsp;</div>
					<div class="large-12 columns text-center">
						<input type="button" style="width:16%" class="btn formbuttonsalert" id="'.$modname.'filteraddcondsubbtn" Value="Submit" name="'.$modname.'filteraddcondsubbtn"  tabindex="14" >
					</div>
					<div class="large-12 columns">&nbsp;</div></span>
					<div class="large-12 columns">
						<div id="'.$modname.'filterconddisplay" class="large-12 medium-12 small-12 columns uploadcontainerstyle" style="height:10rem; border:1px solid #CCCCCC;overflow-y:auto;overflow-x:hidden;">
						</div>
						<input type="hidden" id="'.$modname.'conditionname" value="" name="'.$modname.'conditionname">
						<input type="hidden" id="'.$modname.'filterid" value="" name="'.$modname.'filterid">
						<input type="hidden" id="'.$modname.'filtervalue" value="" name="'.$modname.'filtervalue">
					</div>
					<div class="large-12 columns">&nbsp;</div>
				</span>';
		$content .='</div></div></div>';
		$content .= '</div></div></div></div></div>';
		return $content;
	}
	//base module filter fetch
	public function basemodulefilterdatafetchmodel() {
		$moduleid = $_POST['moduleid'];
		$gridtableid = $_POST['gridid'];
		$appdateformat = $this->Basefunctions->appdateformatfetch();
		$moduledd = $this->Basefunctions->getalldropdownbasedonmodule($moduleid);
		$filtertabgroup = $this->Basefunctions->moduletabgroupdetailsfetch($moduleid);
		$filtertabsecfield = $this->Basefunctions->filterdataviewdropdowncolumns($moduleid);
		$content ='<div class="large-12 small-12 medium-12 columns" style="position:absolute;">
		<div class="overlay large-12 columns mblnopadding" id="filterdisplaymainview" style="z-index:40;display: block;">	
			<div class="large-4 large-offset-8 columns" style="padding-right: 0em !important;height:100% !important;padding-left:0px !important;padding: 0px !important;">
				<div class="large-12 column paddingzero" style="position: relative;left: 4px;">
					<div id="filterdisplaymainviewformvalidate" class="validationEngineContainer">
					<div class="large-12 columns end" style="padding: 0px !important;">
						<div class="large-12 columns cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff">
							<div class="searchfilteroverlay filterbox ui-draggable" style="margin: 10px 10px 10px 0px; touch-action: none; -moz-user-select: none;box-shadow: none;">
								<div class="large-12 columns noticombowidget-box filterheaderborderstyle">
									<div class="large-12 medium-12 columns end noticomboheader activeheader filertabbgcolor" id="filterview" style="padding-top:0.4rem!important;padding-left:0.7rem !important;padding-bottom:0.5rem!important" data-griddataid="'.$gridtableid.'">
										<div class="large-4 columns sectionheaderformcaptionstyle" style="text-align: left;top: -10px !important;">Filter</div>
										<span id="fiteractive" class="tab-titlemini sidebariconsmini active filtermini" style="left: 100px !important;">
											<input type="button" class="addkeyboard filterdisplaymainviewclearclose alertbtnyes" value="Clear" tabindex="129">
											<input type="button" class="alertbtnno addsectionclose leftcloseposition filterdisplaymainviewclose" value="Close" tabindex="130">
										</span>
									</div>
								</div>
							</div>
							<!--<div class="large-4 medium-4 columns end noticomboheader activeheader" id="minidashboard" style="padding-top:0.4rem!important;padding-left:0.2rem !important;padding-bottom:0.5rem!important" data-griddataid="'.$gridtableid.'"><span id="dashboardactive" class="tab-titlemini sidebariconsmini dashboardmini"><span>Dashboard</span></span></div>
<div class="large-5 medium-5 columns end noticomboheader activeheader" id="mininotes" style="padding-top:0.4rem!important;padding-left:0.7rem !important;padding-bottom:0.5rem!important" data-griddataid="'.$gridtableid.'"><span id="notiactive" class="tab-titlemini sidebariconsmini notesmini"><span>Notes</span></span></div><div class="large-1 medium-1 columns paddingzero"><span id="closefiltertoggle"  class="icon-box" style="padding:0.4rem 0;"><i class="material-icons" title="close">close</i></span></div>--></div>
				<div class="dashboardview hidedisplay">
				<div class="row">&nbsp;</div>
				<div class="large-12 medium-12 small-12 columns end filterheight filterbodyborderstyle" id="dashboardwidget" style="padding:10px 10px;overflow-y:auto;overflow-x:hidden;position:relative;">
				</div>
				</div>
			   <div id="textenterdiv" class="mininotesview hidedisplay large-12 medium-12 small-12 columns" style="padding:0px 0px;position:relative;height: 35rem;"><div class="large-12 columns paddingzero"><div class="large-12 columns paddingzero" style="text-align:left;cursor:pointer"><input id="unique_rowid" type="hidden"></div><div class="large-12 columns hidedisplay titleinputspan">&nbsp;</div><div class="large-12 columns paddingzero"><div class="atsearchdiv hidedisplay"></div><div style="background:#f5f5f5;border-collapse:collapse;border-color:#dbdbdb;border-style:solid;border-width:2px;" class="large-12 columns paddingzero"><div class="large-12 columns fiterconversbar paddingzero"><!--<span id="convmulitplefileuploader"></span><span class="uploadattachments attachbtnstyle" title="Attachment" style="cursor:pointer;"><i class="material-icons">attachment</i></span>--><textarea class="mention messagetoenterstyle" rows="2" placeholder="Enter Your Message Here..." id="homedashboardconvid" name="homedashboardconvid"></textarea><span class="sendbtnstyle" id="postsubmit" style=""><i class="material-icons">send</i></span><span class="sendbtnstyle hidedisplay" id="editpostsubmit" style=""><i class="material-icons">send</i></span><span class="convfileupload hidedisplay" style="position:relative;top:0.1rem;"><!--<span id="c_fileid" class="attachmentname" data-filepath="">--></span><input id="cc_filename" type="hidden"><input id="cc_filepath" type="hidden"></div><div class="large-6 medium-6  small-6 columns" style="text-align:right;"><!--<span><select class="chzn-select" style="width:35%" id="c_privacy" "><option value="1">Public</option><option value="2">OnlyMe</option></select></span><span class="postbtnstyle" id="postsubmit">Post</span><span class="postbtnstyle hidedisplay" id="editpostsubmit">Post</span><span class="postbtnstyle hidedisplay" id="postcancel">Cancel</span>--></div><input type="hidden" name="minimoduleid" id="minimoduleid" value=""/><input type="hidden" name="minicommonid" id="minicommonid" value=""/> </div></div></div><div class="large-12 columns scrollbarclass"  style="height: 32rem;overflow-x:hidden"><div class="clearfix"></div><ul id="noteswidget" class="member-list"></ul></div><div class="large-12 columns">&nbsp;</div></div>
				<div class="filterview1">
				<div class="row">&nbsp;</div>
				<div class="large-12 medium-12 small-12 columns end filterheight filterbodyborderstyle sheepbar" data-x="false" data-y="true" id="fbuildercat1" style="    box-shadow: none;padding:10px 10px;position:relative;overflow-y:auto;overflow-x:hidden;background-color:#eceff1;height: 93.5vh !important;box-shadow: none;"><span id="filterformconditionvalidation" class="validationEngineContainer">';
				
				
		$content .= '<div class="filterfieldbox" style="">
						<div class="wrappercontent">
							<div class="elementlist large-12 columns static-field" >
								<div class="" id=""><label>Fields</label><select data-placeholder="" data-prompt-position="topLeft:14,36" id="filtercolumn" class="validate[required] chzn-select dropdownchange columnfilter" name="viewcolname" tabindex="9"><option></option>';
								$prev = ' ';
								foreach($filtertabsecfield as $key):
								$cur = $key->moduletabsectionid;
								$a ="";
								if($prev != $cur ) {
									if($prev != " ") {
										$content.= '</optgroup>';
									}
									$content.= '<optgroup  label="'.$key->moduletabsectionname.'" class="'.str_replace(' ','',$key->moduletabsectionname).'">';
									$prev = $key->moduletabsectionid;
									$a = "pclass";
								}
								$selopt="";
								if($key->fieldview == '1') {
									$selopt="selected='selected'";
								}
								$content .='<option value="'.$key->viewcreationcolumnid.'" data-indexname="'.$key->viewcreationcolmodelindexname.'" data-uitype="'.$key->uitypeid.'" data-moduleid="'.$key->viewcreationmoduleid.'" data-label="'.$key->viewcreationcolumnname.'" data-fldview="'.$key->fieldview.'" data-fldrestrict="'.$key->fieldrestrict.'">'.$key->viewcreationcolumnname.'</option>';
								endforeach;
			$content .='</select></div></div>';
			$content .='<div class="static-field large-12 columns viewcondclear">
						<label>Condition <span class="mandatoryfildclass">*</span></label>
						<select data-placeholder="" data-prompt-position="topLeft:14,36" class="validate[required] chzn-select chosenwidth " tabindex="11" name="filtercondition" id="filtercondition">
							<option></option>
							</select>
					</div>
					<div class="input-field large-12 columns viewcondclear" id="filtercondvaluedivhid">
						<input type="text" class="validate[required,maxSize[100]]" id="filtercondvalue" name="filtercondvalue"  tabindex="12" >
						<label for="filtercondvalue">Value <span class="mandatoryfildclass">*</span></label>
					</div>
					<div class="static-field large-12 columns viewcondclear" id="filterddcondvaluedivhid">
						<label>Value<span class="mandatoryfildclass">*</span></label>
						<select data-placeholder="" data-prompt-position="topLeft:14,36" class=" chzn-select chosenwidth valdropdownchange" tabindex="13" name="filterddcondvalue" id="filterddcondvalue">
							<option></option>
						</select>
					</div>
					<div class="static-field large-12 columns viewcondclear" id="filterassignddcondvaluedivhid">
						<label>Value<span class="mandatoryfildclass">*</span></label>
						<select data-placeholder="" data-prompt-position="topLeft:14,36" class="chzn-select chosenwidth valdropdownchange" tabindex="13" name="filterassignddcondvalue" id="filterassignddcondvalue">
							<option></option>
						</select>
					</div>
					<div class="input-field large-12 columns viewcondclear" id="filterdatecondvaluedivhid">
						<input type="text" class="" data-dateformater="';
		$content .= $appdateformat.'" id="filterdatecondvalue" name="filterdatecondvalue" tabindex="12" >
						<label for="filterdatecondvalue">Value <span class="mandatoryfildclass">*</span></label>
					</div>
					<div class="filterchecklistview hidedisplay" id="filterchechboxcondvaluedivhid">
						<!--<div class="filter-field large-12 columns">
							<input id="filtercheckbox0" name="" class="checkboxcls radiochecksize filled-in " data-hidname="" tabindex="14" value="" type="checkbox">
							<label for="filtercheckbox0">Dr</label><input name="donotmail" id="donotmail" value="No" data-defvalattr="No" type="hidden">
						</div>
						<div class="filter-field large-12 columns">
							<input id="filtercheckbox1" name="" class="checkboxcls radiochecksize filled-in " data-hidname="" tabindex="14" value="" type="checkbox">
							<label for="filtercheckbox1">Mr</label><input name="donotmail" id="donotmail" value="No" data-defvalattr="No" type="hidden">
						</div>
						<div class="filter-field large-12 columns">
							<input id="filtercheckbox2" name="" class="checkboxcls radiochecksize filled-in " data-hidname="" tabindex="14" value="" type="checkbox">
							<label for="filtercheckbox2">Ms</label><input name="donotmail" id="donotmail" value="No" data-defvalattr="No" type="hidden">
						</div>-->
					</div>
					<div class="large-12 columns viewcondclear">
						<input type="hidden" class="" id="filterfinalviewcondvalue" name="filterfinalviewcondvalue"  tabindex="12" ><input type="hidden" class="" id="filterfinalviewconid" name="filterfinalviewconid"  tabindex="12" >
					</div>
					<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns text-center">
							<input type="button" style="width:16%" class="alertbtnyes formbuttonsalert" id="filteraddcondsubbtn" Value="Submit" name="filteraddcondsubbtn"  tabindex="14" >
						</div>
					<div class="large-12 columns">&nbsp;</div></span>
					<div class="large-12 columns">
						<input type="hidden" id="filterid" value="" name="filterid">
						<input type="hidden" id="conditionname" value="" name="conditionname">
						<input type="hidden" id="filtervalue" value="" name="filtervalue">
						<input type="hidden" id="ddfilterid" value="" name="ddfilterid">
						<input type="hidden" id="ddconditionname" value="" name="ddconditionname">
						<input type="hidden" id="ddfiltervalue" value="" name="ddfiltervalue">
					</div>
					<div class="large-12 columns">&nbsp;</div>
					<div class="large-12 columns">
						<div id="filterconddisplay" class="large-12 medium-12 small-12 columns uploadcontainerstyle" style="height:10rem; border:1px solid #CCCCCC;overflow-y:auto;overflow-x:hidden;"></div>
					</div>
					<div class="large-12 columns">&nbsp;</div><div class="large-12 columns">&nbsp;</div>';
		$data = $this->Basefunctions->showalldropdowns($moduledd);
		$content .= $data['content'];
		$content .= '<input type="hidden" value="'.$data['length'].'" id="filterdropdownlenth" name="filterdropdownlenth"/>';
		$content .='<div class="row">&nbsp;</div><div class="row">&nbsp;</div>';
		$content .='</div></div>';
		
		$content .= '</span></div>

							
							
							
						</div>
						</div>
				</div>
			</div>		
		</div>';
		echo $content;
	}
	//check given date is holiday or not
	public function checkgivendateisholidayornot($date) {
		$holidayid = $this->get_company_settings('recurrencedayid');
		$days = $this->getholidayname($holidayid);
		$timestamp = strtotime($date);
		$day = date("l", $timestamp);
		if(in_array($day,$days)) {
			$nextdate = date('Y-m-d',strtotime("+1 day", strtotime($date)));
			$ndate = $this->checkgivendateisholidayornot($nextdate);
			return $ndate;
		} else {
			$date = $this->ymddateconversion($date);
			return $date;
		}
	}
	//get holiday nae from comapny setting and check the given day
	public function getholidayname($holidayid) {
		$userroleid = $this->userroleid;
		$industryid = $this->industryid;
		$device = $this->deviceinfo();
		$i=0;
		$data = array();
		$this->db->select('recurrencedayname');
		$this->db->from('recurrenceday');
		$this->db->where_in('recurrenceday.recurrencedayid',$holidayid);
		$this->db->where("FIND_IN_SET('2',recurrenceday.moduleid) >", 0);
		$this->db->where("FIND_IN_SET('$industryid',recurrenceday.industryid) >", 0);
		$this->db->where('recurrenceday.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = $row->recurrencedayname;
				$i++;
			}
		}
		return $data;
	}
	// load username without/with assigned
	public function loadusername() {
		$result=array();
		$tablename = $_POST['table'];
		if($_POST['datastatus'] == 1) {
			$data = $this->db->query("SELECT employee.employeeid, employee.employeename FROM `employee` LEFT JOIN ".$tablename." ON ".$tablename.".employeeid=employee.employeeid and ".$tablename.".status = 1 WHERE ".$tablename.".`employeeid` is null and employee.status = 1 GROUP BY employee.employeeid");
		}else if($_POST['datastatus'] == 0) {
			$data = $this->db->query("SELECT employee.employeeid, employee.employeename FROM `employee` JOIN ".$tablename." ON ".$tablename.".employeeid=employee.employeeid and ".$tablename.".status = 1 WHERE ".$tablename.".status = 1 GROUP BY employee.employeeid");
		}
		$i=0;
		if($data->num_rows() > 0){
			foreach($data->result() as $user_name)
			{
				$result[]=array(
						'employeeid' => $user_name->employeeid,
						'employeename' => $user_name->employeename,
				);
				$i++;
			}
		}else{
			$result='';
		}
		echo json_encode($result);
	}
	public function replace_between($str, $needle_start, $needle_end, $replacement) {
		$pos = strpos($str, $needle_start);
		$start = $pos === false ? 0 : $pos + strlen($needle_start);

		$pos = strpos($str, $needle_end, $start);
		$end = $start === false ? strlen($str) : $pos;
	 
		return substr_replace($str,$replacement,  $start, $end - $start);
	}
	// get hostname
	public function gethostdataname() {
		$hostname = '';
		$this->db->select('hostname');
		$this->db->from('company');
		$this->db->where('status',1);
		$this->db->limit(1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			$hostname = $result->row()->hostname;
		}
		return $hostname;
	}
	// get static ip address
	public function getstaticipaddress() {
		$staticip = '';
		$this->db->select('staticipaddress');
		$this->db->from('company');
		$this->db->where('status',1);
		$this->db->limit(1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			$staticip = $result->row()->staticipaddress;
		}
		return $staticip;
	}
	public function generatecleandb() {
		die();	
		$data=$this->db->query("SELECT * FROM cleandbgenerate where status = 1 and type = 2");
		foreach($data->result() as $info){
			$table=$info->dbtablename;
			$autoincrement=$info->autoincrement;
			$deletefromid=$info->deletefromid;
			$setautoincrement=$info->setautoincrement;
			if($autoincrement == 'y')
			{
				$datatable=$this->db->query("SHOW KEYS FROM ".$table."  WHERE Key_name = 'PRIMARY'");
				foreach($datatable->result() as $infotable){
					$primid=$infotable->Column_name;
					print_r($table);print_r('</br>');
					$this->db->query(" DELETE FROM ".$table." WHERE  ".$table.". ".$primid." >= ".$deletefromid."");
					$this->db->query("ALTER TABLE ".$table." auto_increment = ".$setautoincrement." ");
					
				}
			}
		}
	}
	// Retrieve Emailids from users or roles or groups list
	public function retrieveallrecipientemailids($rulealertadduserrecep) {
		$dataarray = array();
		$dataexplode = explode(',',$rulealertadduserrecep);
		foreach($dataexplode as $result) {
			if($result != '') {
				$id = $result[0];
				if($id == 1) { // Employee or users
					$emailid =  $this->Basefunctions->singlefieldfetch('emailid','employeeid','employee',substr($result, 2));
					if($emailid != '') {
						if(!in_array($emailid, $dataarray, true)) {
							array_push($dataarray,$emailid);
						}
					}
				} else if($id == 2) { // Groups
					$groupid = substr($result, 2);
					if($groupid == 2) {
						$groupsemailid =  $this->retrievegroupsemailid($groupid);
						foreach($groupsemailid as $eid) {
							foreach($eid as $emailid) {
								if(!in_array($emailid, $dataarray, true)) {
									array_push($dataarray,$emailid);
								}
							}
						}
					}
				} else if($id == 3) { // Roles
					$roleid = substr($result, 2);
					if($roleid != 1) {
						$rolesemailid =  $this->retrievegroupsemailid($roleid);
						foreach($rolesemailid as $eid) {
							foreach($eid as $emailid) {
								if(!in_array($emailid, $dataarray, true)) {
									array_push($dataarray,$emailid);
								}
							}
						}
					}
				}  else if($id == 4) { // Role and Subordinates
					$rolesubid = substr($result, 2);
					if($rolesubid != 1) {
						$rolesubemailid =  $this->retrievegroupsemailid($rolesubid);
						foreach($rolesubemailid as $eid) {
							foreach($eid as $emailid) {
								if(!in_array($emailid, $dataarray, true)) {
									array_push($dataarray,$emailid);
								}
							}
						}
					}
				}
			}
		}
		return $dataarray;
	}
	// Retrieve Mobile Numbers from users or roles or groups list
	public function retrieveallrecipientmobilenos($rulealertadduserrecep) {
		$dataarray = array();
		$dataexplode = explode(',',$rulealertadduserrecep);
		foreach($dataexplode as $result) {
			if($result != '') {
				$id = $result[0];
				if($id == 1) { // Employee or users
					$mobileno =  $this->Basefunctions->singlefieldfetch('mobilenumber','employeeid','employee',substr($result, 2));
					if($mobileno != '') {
						if(!in_array($mobileno, $dataarray, true)) {
							array_push($dataarray,$mobileno);
						}
					}
				} else if($id == 2) { // Groups
					$groupid = substr($result, 2);
					if($groupid == 2) {
						$groupsmobileno =  $this->retrievegroupsmobileno($groupid);
						foreach($groupsmobileno as $eid) {
							foreach($eid as $mobileno) {
								if(!in_array($mobileno, $dataarray, true)) {
									array_push($dataarray,$mobileno);
								}
							}
						}
					}
				} else if($id == 3) { // Roles
					$roleid = substr($result, 2);
					if($roleid != 1) {
						$rolesmobileno =  $this->retrievegroupsmobileno($roleid);
						foreach($rolesmobileno as $eid) {
							foreach($eid as $mobileno) {
								if(!in_array($mobileno, $dataarray, true)) {
									array_push($dataarray,$mobileno);
								}
							}
						}
					}
				}  else if($id == 4) { // Role and Subordinates
					$rolesubid = substr($result, 2);
					if($rolesubid != 1) {
						$rolesubmobileno =  $this->retrievegroupsmobileno($rolesubid);
						foreach($rolesubmobileno as $eid) {
							foreach($eid as $mobileno) {
								if(!in_array($mobileno, $dataarray, true)) {
									array_push($dataarray,$mobileno);
								}
							}
						}
					}
				}
			}
		}
		return $dataarray;
	}
	public function updatechargecloneforchild($categoryid,$cloneproductid)
	{
		//$categoryarray= array($categoryid);
		$categorylevel = $this->Basefunctions->get_company_settings('category_level');
		$currentcategorylevel = $this->Basefunctions->singlefieldfetch('categorylevel','categoryid','category',$categoryid);
		$currentparentproductid = $this->Basefunctions->singlefieldfetch('parentproductid','categoryid','category',$categoryid);
		$this->db->query("UPDATE product SET parentproductid='".$cloneproductid."' WHERE categoryid IN (".$categoryid.") and status not in (0,3) and (parentproductid = ".$currentparentproductid." or parentproductid = '1' )" );
		for($i=$currentcategorylevel+1;$i<=$categorylevel;$i++) {
			$data=$this->db->query("select GROUP_CONCAT(categoryid) as categoryid FROM category WHERE parentcategoryid IN (".$categoryid.")  and (parentproductid = ".$currentparentproductid." or parentproductid = '1' )" );
			foreach($data->result() as $info) {
				if($info->categoryid != ''){
					//$categoryarray = explode(',',$info->categoryid); 
					$this->db->query("UPDATE category SET parentproductid='".$cloneproductid."' WHERE categoryid IN (".$info->categoryid.") and status not in (0,3) and (parentproductid = ".$currentparentproductid." or parentproductid = '1' )" );
					$this->db->query("UPDATE product SET parentproductid='".$cloneproductid."' WHERE categoryid IN (".$info->categoryid.") and status not in (0,3) and (parentproductid = ".$currentparentproductid." or parentproductid = '1' )" );
					$categoryid = $info->categoryid;
			   }
			}
			
		}
	}
	public function updatechargecloneforproducttable($categoryid,$primaryid)
	{
		$cloneproductid = $this->Basefunctions->singlefieldfetch('parentproductid','categoryid','category',$categoryid);
		$update_last=array(
				'parentproductid'=>$cloneproductid
		);
		$this->db->where('productid',$primaryid);
		$this->db->update('product',$update_last);
	}
	function base64_to_jpeg($base64_string, $output_file )
	{
		$ifp = fopen( $output_file, "wb" );
		$base64img = str_replace('data:image/png;base64,', '', $base64_string);	
		fwrite( $ifp, base64_decode($base64img) ); 
		fclose( $ifp ); 
		return( $output_file ); 
	}
	function compress($source, $destination, $quality)
	{ 
		$info = getimagesize($source); 
		if ($info['mime'] == 'image/jpeg') 
			$image = imagecreatefromjpeg($source); 
		elseif ($info['mime'] == 'image/gif') 
			$image = imagecreatefromgif($source); 
		elseif ($info['mime'] == 'image/png') 
			$image = imagecreatefrompng($source); 
		imagejpeg($image, $destination, $quality); 
		return $destination; 
	}
	/* ** Access the company setting value of given data set */
	public function getall_company_settings($value) {
		$info = $this->db->select($value)
							->from('companysetting')
							->limit(1)
							->get()
							->row();
		return $info;
	}
	public function getall_rounds($rountypeid) {
		$info = $this->db->select('roundtypevalue,roundtypeid')
							->from('roundtype')
							->where_in('roundtype.roundtypeid',$rountypeid)
							->get()
							->row();
		return $info;
	}
	// category tree field - required information
	public function categorytreerequiredfield() {
		$information = "";
		$tablename = 'category' ;
		$mandatoryopt = ""; //mandatory option set
		$information .= divopen( array("id"=>"dl-promenucategory","class"=>"dl-menuwrapper","style"=>"z-index:2;margin-bottom: 1rem") );
		$type="button";
		$information .= '<button name="treebutton" type="button" id="prodcategorylistbutton" class="btn dl-trigger treemenubtnclick" tabindex="" style="height:2.2rem;color:#ffffff;font-size: 1.45rem;padding: 0.1rem;padding-top:0.3rem;background-color:#546e7a;"><i class="material-icons">format_indent_increase</i></button>';
		$txtoptions = array("style"=>"width:83%;display:inline;position:absolute;height:2.12rem;padding-left: 5px;","readonly"=>"readonly","tabindex"=>"","class"=>"");
		$information .= text('categorynamehidden','',$txtoptions);
		$information .= '<ul class="dl-menu maintreegenerate large-12 medium-6 small-12 column" id="prodcategorylistuldata">';
		$dataset = $this->jewel_treecreatemodel($tablename);
		$resultvalue = $this->createTree($dataset,0);
		$information .= close('ul');
		$information .= close('div');
		return $resultvalue;
	}
	// Workflow New Task Created 
	public function workflownewtaskcreate($taskname,$taskduedate,$taskstatusid,$taskpriorityid,$employeeid,$description,$moduleid) {
		// Task Creation
		$taskcreate = array(
			'commonid'=>'1',
			'leadcontacttypeid'=>'2',
			'moduleid'=>$moduleid,
			'crmtaskname'=>$taskname,
			'employeeid'=>$employeeid,
			'taskstartdate'=>$taskduedate,
			'priorityid'=>$taskpriorityid,
			'crmstatusid'=>$taskstatusid,
			'description'=>$description,
			'industryid'=>$this->industryid,
			'branchid'=>$this->branchid
		);
		$taskcreate = array_merge($taskcreate,$this->Crudmodel->defaultvalueget());
	    $this->db->insert('crmtask',array_filter($taskcreate));
	}
	//used to get the date based on the recurrence selection -- daily/weekly/monthly/yearly
	public function getstartdate($crmrecurrenceeveryday,$sdate,$enddate,$crmrecurrencefreqid,$crmrecurrenceontheday,$crmrecurrencedayid,$count) {
		if($crmrecurrencefreqid == 14) { // for daily
			if($count == 0) {
				$daydate =$sdate.','.$enddate;
				$daydate = array($daydate);
			} else {
				$sdate = strtotime("+".$crmrecurrenceeveryday." day", strtotime($sdate));
				$edate = strtotime("+".$crmrecurrenceeveryday." day", strtotime($enddate));
				$daydate =date("d-m-Y", $sdate).','.date('d-m-Y',$edate);
				$daydate = array($daydate);
			}
			return $daydate;
		} else if($crmrecurrencefreqid == 15) { //for weekly
			$ssdate = array();
			$weekdate = '';
			$datetime1 = date_create($sdate);
			$datetime2 = date_create($enddate);
			$ccc = $datetime1->diff($datetime2)->days;
			$newcount = round($ccc/7);//week number
			$dayvalue = explode(',',$crmrecurrencedayid);
			$weekday = $this->getweekday($dayvalue);
			$j=0;
			for($i=0;$i<$newcount;$i++) {
				$value = 7 * $crmrecurrenceeveryday;
				$num = $i * $value;
				$newdate = strtotime("+".$num." day", strtotime($sdate));
				$date = date("d-m-Y", $newdate);
				$dt = new DateTime($date);
				$dayname = $dt->format('l');
				$startdate[$j] = $date;
				$day[$j] = $dayname;
				$edate = strtotime("+".$num." day", strtotime($enddate));
				$endate = date("d-m-Y", $edate);
				$edt = new DateTime($endate);
				$edayname = $edt->format('l');
				$eenddate[$j] = $endate;
				$eday[$j] = $edayname; 
				$j++;
				$weekdate = $this->getweekdate($startdate,$weekday);
				$endweekdate = $this->getweekdate($eenddate,$weekday);
			}
			if($weekdate != '') {
				$ccount = count($weekdate);
				$n=0;
				for($m=0;$m<$ccount;$m++) {
					$ssdate[$n] = $weekdate[$m].','.$endweekdate[$m];
					$n++;
				}
			}
			return $ssdate; 
		} else if($crmrecurrencefreqid == 16) { // for monthly
			if($count == 0) {
				$daydate =$sdate.','.$enddate;
				$mdate = array($daydate);
			} else {
				//for start date
				$month = explode('-',$sdate);
				if($month[1] == '01' || $month[1] == '03' || $month[1] == '05' || $month[1] == '07'|| $month[1] == '08' || $month[1] == '10' || $month[1] == '12'){
					$day = $crmrecurrenceeveryday * 31;
				} else if($month[1] == '04' || $month[1] == '06' || $month[1] == '09' || $month[1] == '11') {
					$day = $crmrecurrenceeveryday * 30;
				} else {
					if(($month[2] % 4) == 0) {
						$day = $crmrecurrenceeveryday * 29;
					} else {
						$day = $crmrecurrenceeveryday * 28;
					}
				}
				//for end date
				$endmonth = explode('-',$enddate);
				if($endmonth[1] == '01' || $endmonth[1] == '03' || $endmonth[1] == '05' || $endmonth[1] == '07'|| $endmonth[1] == '08' || $endmonth[1] == '10' || $endmonth[1] == '12'){
					$eday = $crmrecurrenceeveryday * 31;
				} else if($endmonth[1] == '04' || $endmonth[1] == '06' || $endmonth[1] == '09' || $endmonth[1] == '11') {
					$eday = $crmrecurrenceeveryday * 30;
				} else {
					if(($month[2] % 4) == 0) {
						$eday = $crmrecurrenceeveryday * 29;
					} else {
						$eday = $crmrecurrenceeveryday * 28;
					}
				}
				$reccuurday = $crmrecurrenceontheday - 1;
				//for start date
				$sdate = strtotime("+".$day." day", strtotime($sdate));
				$date = date("d-m-Y", $sdate);
				$date1 =  explode('-',$date);
				$sdata = $reccuurday.'-'.$date1[1].'-'.$date1[2];
				//for end date
				$enddate = strtotime("+".$eday." day", strtotime($enddate));
				$edate = date("d-m-Y", $enddate);
				$edate1 =  explode('-',$edate);
				$edata = $reccuurday.'-'.$edate1[1].'-'.$edate1[2];
				//final start and end date
				$daydate =$sdata.','.$edata;
				$mdate = array($daydate);
			}
			return $mdate;
		} else if($crmrecurrencefreqid == 19) {// for yearly
			if($count == 0) {
				$daydate =$sdate.','.$enddate;
				$daydate = array($daydate);
				return $daydate;
			} else {
				//for start date
				$year = explode('-',$sdate);
				if(($year[2] % 4) == 0) {
					$day = $crmrecurrenceeveryday * 366;
				} else {
					$day = $crmrecurrenceeveryday * 365;
				}
				$sdate = strtotime("+".$day." day", strtotime($sdate));
				//for end date
				$eyear = explode('-',$enddate);
				if(($eyear[2] % 4) == 0) {
					$eday = $crmrecurrenceeveryday * 366;
				} else {
					$eday = $crmrecurrenceeveryday * 365;
				}
				$enddate = strtotime("+".$eday." day", strtotime($enddate));
				$daydate =date("d-m-Y", $sdate).','.date("d-m-Y", $enddate);
				$ydate = array($daydate);
				return $ydate;
			}
		}
	}
	//get week day
	public function getweekday($dayvalue) { 
		$count = count($dayvalue);
		for($i=0;$i<$count;$i++) {
			if($dayvalue[$i] == 2){
				$weekday[$i] = 'Sunday';
			} else if($dayvalue[$i] == 3) {
				$weekday[$i] = 'Monday';
			} else if($dayvalue[$i] == 4) {
				$weekday[$i] = 'Wednesday';
			} else if($dayvalue[$i] == 5) {
				$weekday[$i] = 'Tuesday';
			} else if($dayvalue[$i] == 6) {
				$weekday[$i] = 'Thursday';
			} else if($dayvalue[$i] == 7) {
				$weekday[$i] = 'Friday';
			} else if($dayvalue[$i] == 8) {
				$weekday[$i] = 'Saturday';
			}
		}
		return $weekday;
	}
	//week date get based on the user data
	public function getweekdate($startdate,$weekday) {
		$count = count($startdate);
		$k=0;
		for($i=0;$i<$count;$i++) {
			for($j=0;$j<7;$j++) {
				$newdate = strtotime("+".$j." day", strtotime($startdate[$i]));
				$date = date("d-m-Y", $newdate);
				$dt = new DateTime($date);
				$dayname = $dt->format('l');
				if(in_array($dayname,$weekday)) {
					$wdate[$k] = $date;
					$k++;
				}
			}
		}
		return $wdate;
	}
	// Auto Complete Address Details Fetch
	public function autocompleteaddress() {
		$search = $_POST['term'];
		$query = $this->db->query("SELECT DISTINCT(address) as address FROM accountaddress WHERE addresstypeid = 4 and status = 1 AND address LIKE '%$search%' ");
		foreach($query->result() as $row){
			if($row->address != null || $row->address != '') {
				$data[] = $row->address;
			}
		}
		echo json_encode(array('address'=>$data));
	}
	// Auto Complete Area Details Fetch
	public function autocompletearea() {
		$search = $_POST['term'];
		$query = $this->db->query("SELECT DISTINCT(primaryarea) as primaryarea FROM accountcf WHERE status = 1 AND primaryarea LIKE '%$search%' ");
		foreach($query->result() as $row){
			if($row->primaryarea != null || $row->primaryarea != '') {
				$data[] = $row->primaryarea;
			}
		}
		echo json_encode(array('primaryarea'=>$data));
	}
	// Auto Complete Postal Code Details Fetch
	public function autocompletepostalcode() {
		$search = $_POST['term'];
		$query = $this->db->query("SELECT DISTINCT(pincode) as pincode FROM accountaddress WHERE addresstypeid = 4 and status = 1 AND pincode LIKE '%$search%' ");
		foreach($query->result() as $row){
			if($row->pincode != null || $row->pincode != '') {
				$data[] = $row->pincode;
			}
		}
		echo json_encode(array('pincode'=>$data));
	}
	// Auto Complete City Details Fetch
	public function autocompletecity() {
		$search = $_POST['term'];
		$query = $this->db->query("SELECT DISTINCT(city) as city FROM accountaddress WHERE addresstypeid = 4 and status = 1 AND city LIKE '%$search%' ");
		foreach($query->result() as $row){
			if($row->city != null || $row->city != '') {
				$data[] = $row->city;
			}
		}
		echo json_encode(array('city'=>$data));
	}
	// Retrieve Itemtag Details in Image Preview details
	public function retrieveitemtagdetails() {
		$weightround =  $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2);
		if(isset($_POST['itemtagid'])) {
			$itemtagid = $_POST['itemtagid'];
			if($itemtagid == '') {
				$itemtagid = 1;
			}
		} else {
			$itemtagid = 1;
		}
		if(isset($_POST['barcodeno'])) {
			$barcodeno = trim($_POST['barcodeno']);
		} else {
			$barcodeno = '';
		}
		if($itemtagid != 1) {
			$data = $this->db->query("SELECT itemtag.itemtagid, itemtag.itemtagnumber, itemtag.rfidtagno, category.categoryname, product.productname, counter.countername, purity.purityname, itemtag.grossweight, itemtag.stoneweight, itemtag.netweight, itemtag.tagimage FROM itemtag LEFT OUTER JOIN product ON product.productid = itemtag.productid LEFT OUTER JOIN category ON category.categoryid = product.categoryid LEFT OUTER JOIN counter ON counter.counterid = itemtag.counterid LEFT OUTER JOIN purity ON purity.purityid = itemtag.purityid WHERE itemtag.itemtagid IN ($itemtagid) order by itemtag.itemtagid DESC");
		} else if($barcodeno != '') {
			$data = $this->db->query("SELECT itemtag.itemtagid, itemtag.itemtagnumber, itemtag.rfidtagno, category.categoryname, product.productname, counter.countername, purity.purityname, itemtag.grossweight, itemtag.stoneweight, itemtag.netweight, itemtag.tagimage FROM itemtag LEFT OUTER JOIN product ON product.productid = itemtag.productid LEFT OUTER JOIN category ON category.categoryid = product.categoryid LEFT OUTER JOIN counter ON counter.counterid = itemtag.counterid LEFT OUTER JOIN purity ON purity.purityid = itemtag.purityid WHERE itemtag.itemtagnumber = '$barcodeno' order by itemtag.itemtagid DESC");
		}
		if($data->num_rows() > 0) {
			foreach($data->result() as $info) {
				$output = array('itemtagnumber'=>$info->itemtagnumber,'rfidtagno'=>$info->rfidtagno,'categoryname'=>$info->categoryname,'productname'=>$info->productname,'countername'=>$info->countername,'purityname'=>$info->purityname,'grossweight'=>ROUND($info->grossweight,$weightround),'stoneweight'=>ROUND($info->stoneweight,$weightround),'netweight'=>ROUND($info->netweight,$weightround),'tagimage'=>$info->tagimage);
			}
		} else {
			$output = array('fail'=>'FAILED');
		}
		echo json_encode($output);
	}
}
?>