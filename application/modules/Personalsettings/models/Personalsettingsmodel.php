<?php if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );
class Personalsettingsmodel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
	}
	//information fetch
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementpartable']);
		$restricttable = explode(',',$_GET['resctable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $this->Basefunctions->logemployeeid;
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetchwithrestrict($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$restricttable,$moduleid);
		echo $result;
	}
	// num of employees
	public function numofrows() {
		$this->db->select('employeeid');
		$this->db->from('employee');
		$this->db->where('status',1);
		$qryinfo = $this->db->get();
		if($qryinfo->num_rows()>0){
			$info = $qryinfo->num_rows();
			return $info;
		}else{
			return 1;
		}
	}
	//update data information
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['primarydataid'];
		$restricttable = explode(',',$_POST['resctable']);
		$result = $this->Crudmodel->dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable);
		//address insertion
		$arrname=array('primary','secondary');
		$this->Crudmodel->addressdataupdate($arrname,$partablename,$primaryid);	
		//Mail Settings update
		$mailproid=$_POST['mailproviderid'];
		$imapport=trim($_POST['incomingport']);
		$smptport=trim($_POST['outgoingport']);
		$imaphost=trim($_POST['incomingserver']);
		$smpthost=trim($_POST['outgoingserver']);
		if(isset($_POST['outgoingssl'])){
			$insmtp=trim($_POST['outgoingssl']);
		}else{
			$insmtp='';
		}
		$inimap=trim($_POST['incomingssl']);
		$inssl=trim($_POST['inssl']);
		$outssl=trim($_POST['outssl']);
		$password=trim($_POST['emailpassword']);
		$username=trim($_POST['emailusername']);
		$password=base64_encode(strrev($password));
		if($_POST['incomingssl'] != '' || $_POST['incomingssl'] != null) {
			if(strtolower($inssl) == 'ssl' || strtolower($inssl) == 'tls') {
				$imaphost=$imaphost;
			}
		}
		if(isset($_POST['outgoingssl'])){
		if($_POST['outgoingssl'] != '' || $_POST['outgoingssl'] != null) {
			if(strtolower($outssl) == 'ssl' || strtolower($outssl) == 'tls') {
				$smpthost=$smpthost;
			}
		}
		}else{
			$insmtp='';
		}
		$data=$this->db->select('mailsettingid')
						->from('mailsetting')
						->where('status',$this->Basefunctions->activestatus)
						->where('employeeid',$this->Basefunctions->userid)
						->order_by('mailsettingid','desc')
						->limit(1)
						->get();
		if($data->num_rows() > 0) {
			//update script
			foreach($data->result() as $info) {
				$mailsettingid=$info->mailsettingid;
			}
			$updatearray=array('mailproviderid'=>$mailproid,'imapssl'=>$inimap,'smtpssl'=>$insmtp,'default_host'=>$imaphost,'default_port'=>$imapport,'smtp_server'=>$smpthost,'smtp_port'=>$smptport,'lastupdatedate'=>date($this->Basefunctions->datef),'lastupdateuserid'=>$this->Basefunctions->userid,'status'=>$this->Basefunctions->activestatus,'emailusername'=>$username,'emailpassword'=>$password);
			$this->db->where('mailsettingid',$mailsettingid);
			$this->db->update('mailsetting',$updatearray);
		} else {
			//insert script
			if($mailproid!='') {
				$insertarray=array('employeeid'=>$this->Basefunctions->userid,'mailproviderid'=>$mailproid,'imapssl'=>$inimap,'smtpssl'=>$insmtp,'default_host'=>$imaphost,'default_port'=>$imapport,'smtp_server'=>$smpthost,'smtp_port'=>$smptport,'createdate'=>date($this->Basefunctions->datef),'lastupdatedate'=>date($this->Basefunctions->datef),'createuserid'=>$this->Basefunctions->userid,'lastupdateuserid'=>$this->Basefunctions->userid,'status'=>$this->Basefunctions->activestatus,'emailusername'=>$username,'emailpassword'=>$password);
				$this->db->insert('mailsetting',$insertarray);
			}
		}
		echo 'TRUE';
	}
	//primary and secondary address value fetch
	public function primaryaddressvalfetchmodel() {
		$accid = $_GET['dataprimaryid'];	
		$this->db->select('addresssourceid,address,pincode,city,state,country');
		$this->db->from('employeeaddress');
		$this->db->where('employeeaddress.employeeid',$accid);
		$this->db->where('employeeaddress.status',1);
		$this->db->order_by('employeeaddressid','asc');
		$result = $this->db->get();
		$arrname=array('primary','secondary');
		if($result->num_rows() >0) {
			$m=0;
			foreach($result->result() as $row) {
				$data[] = array(
								$arrname[$m].'addresstype'=>$row->addresssourceid,
								$arrname[$m].'address'=>$row->address,
								$arrname[$m].'pincode'=>$row->pincode,
								$arrname[$m].'city'=>$row->city,
								$arrname[$m].'state'=>$row->state,
								$arrname[$m].'country'=>$row->country);
				$m++;
			}
			$finalarray=array_merge($data[0],$data[1]);
			echo json_encode($finalarray);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//password update
	public function passwordupdatemodel() {
		$id = $_POST['empid'];
		$newpassword = $_POST['newpassword'];
		$hash = password_hash($newpassword, PASSWORD_DEFAULT);	
		$password = array('password'=>$hash);
		$this->db->where('employee.employeeid',$id);
		$this->db->update('employee',$password);
		echo "TRUE";
	}
	//old password check
	public function oldpasswordcheckmodel() {
		$primaryid = $this->Basefunctions->logemployeeid;
		$pass = "";
		$oldpassword = $_GET['oldpassword'];
		$this->db->select('employee.password');
		$this->db->from('employee');
		$this->db->where('employee.employeeid',$primaryid);
		$this->db->where('employee.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$pass = $row->password;
			}
		}
		if (password_verify($oldpassword, $pass)) {
			echo "TRUE";
		} else {
			echo "Fail";
		}
	}
	//mail settings details fetch
	public function mailsettingsdetialfetchmodel() {
		$id = $_GET['dataprimaryid'];
		$this->db->select('mailsetting.emailusername,emailpassword,default_host,default_port,smtp_server,smtp_port,imapssl,smtpssl,mailproviderid');
		$this->db->from('mailsetting');
		$this->db->where('mailsetting.employeeid',$id);
		$this->db->where('mailsetting.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$pass=$row->emailpassword;
				$password=base64_decode($pass);
				$inserver = explode('//',$row->default_host);
				$outserver = explode('//',$row->smtp_server);
				$iserver = count($inserver)>=2 ? $inserver[1] : count($inserver)>=1 ? $inserver[0] : '';
				$oserver = count($outserver)>=2 ? $outserver[1] : count($outserver)>=1 ? $outserver[0] : '';
				$data = array('username'=>$row->emailusername,'password'=>$password,'incmeserver'=>$iserver,'incmeport'=>$row->default_port,'outcomeserver'=>$oserver,'outcomeport'=>$row->smtp_port,'incomessl'=>$row->imapssl,'outgngssl'=>$row->smtpssl,'mailproid'=>$row->mailproviderid);
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//sms-mail signature insert/Update
	public function smsmailsignatureinsert($signaturetypeid,$signaturename,$primaryid,$editordata) {
		$value = $this->smsmailsignaturecheck($signaturetypeid,$primaryid);
		$date = date($this->Basefunctions->datef);
		$file_name = 'termscondition/template_'.trim($signaturename)."_".time().".html";
		$html_file_name = 'template_'.trim($signaturename)."_".time().".html";
		$fp = fopen('termscondition/'.$html_file_name,"w");
		fwrite($fp, $editordata);
		fclose($fp);
		if($value != "False") {
			$array = array('signaturename'=>$signaturename,'userssignature_editorfilename'=>$file_name,'lastupdatedate'=>$date,'lastupdateuserid'=>$this->Basefunctions->userid);
			$this->db->where_in('signature.signaturetypeid',array($signaturetypeid));
			$this->db->where_in('signature.employeeid',array($primaryid));
			$this->db->update('signature',$array);
		} else {
			$array = array('signaturetypeid'=>$signaturetypeid,'signaturename'=>$signaturename,'employeeid'=>$primaryid,'userssignature_editorfilename'=>$file_name,'createdate'=>$date,'createuserid'=>$this->Basefunctions->userid,'lastupdatedate'=>$date,'lastupdateuserid'=>$this->Basefunctions->userid,'status'=>$this->Basefunctions->activestatus);
			$this->db->insert('signature',$array);
		}
	}
	//sms-mail signature check - if its available or not
	public function smsmailsignaturecheck($signaturetypeid,$primaryid) {
		$this->db->select('signatureid');
		$this->db->from('signature');
		$this->db->where('signature.signaturetypeid',$signaturetypeid);
		$this->db->where('signature.employeeid',$primaryid);
		$this->db->where('signature.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data = "TRUE";
			}
			return $data;
		} else {
			$data = "False";
			return $data;
		}	
	}
	//mail setting provide details fetch
	public function mailsettingdatafetchmodel() {
		$id = $_POST['providerid'];
		$this->db->select('incommingserver,incommingport,outgoingserver,outgoingport,outgoingssl,incomingssl');
		$this->db->from('mailprovider');
		$this->db->where('mailprovider.mailproviderid',$id);
		$this->db->where('mailprovider.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$inserver = $row->incommingserver;
				$outserver = $row->outgoingserver;
				if($inserver != '' || $outserver != '') {
					$inserver = explode('//',$row->incommingserver);
					$outserver = explode('//',$row->outgoingserver);
					$iserver = $inserver[1];
					$oserver = $outserver[1];
				} else {
					$iserver = '';
					$oserver = '';
				}
				$data = array('incmeserver'=>$iserver,'incmeport'=>$row->incommingport,'outcomeserver'=>$oserver,'outcomeport'=>$row->outgoingport,'incomessl'=>$row->incomingssl,'outgngssl'=>$row->outgoingssl);
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//information fetch
	public function detailviewdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$restricttable) {
		//select field infromation
		$formfieldscolmname[] = $primaryname;
		$formfieldstable[] = $partablename;
		//text box information
		$formfieldsname[] = 'primarydataid';
		$loguserid = $this->Basefunctions->userid;
		$userid = $this->Basefunctions->userid;
		$newdata = array();
		$i=0;
		foreach($formfieldscolmname as $data) {
			if( !in_array(trim($formfieldstable[$i]),$restricttable) ) {
				$newdata[] = $formfieldstable[$i].'.'.$data;
			}
			$i++;
		}
		//final selected data
		$selectdata = implode(',',$newdata);
		//join child table with table
		$joinq = "";
		$fildstable = explode(',',$tablename );
		foreach($fildstable as $tabname) {
			if( !in_array(trim($tabname),$restricttable) ) {
				if(strcmp($partablename,$tabname) != 0) {
					$joinq=$joinq.' LEFT OUTER JOIN '.$tabname.' ON '.$tabname.'.'.$primaryname.'='.$partablename.'.'.$primaryname;
				}
			}
		}
		//where conditions
		$wh = '1=1';
		if( $primaryid!="" && $primaryname ) {
			$wh = $partablename.'.'.$primaryname.'='.$primaryid;
		}
		$status = $partablename.'.status = 1';
		//fetch createuserid and convert as role,group
		$cuserid='';
		$cruserdata = $this->db->query('select employeeid from '.$partablename.' WHERE '.$wh.' AND '.$status);
		foreach($cruserdata->result() as $row) {
			$cuserid = $row->employeeid;
		}
		//fetch data
		$data = $this->db->query('select '.$selectdata.' from '.$partablename.' '.$joinq.' WHERE '.$wh.' AND '.$status);
		if($data->num_rows() >0) {
			foreach($data->result()as $datarow) {
				$i=0;
				foreach($formfieldscolmname as $row) { //formfieldsname
					if( !in_array(trim($formfieldstable[$i]),$restricttable) ) {
						$resdata[$formfieldsname[$i]]=trim($datarow->$row);
					}
					$i++;
				}
			}
			return json_encode($resdata);
		} else {
		   return json_encode(array("fail"=>'Denied'));
		}
	}
	//check the mail add ons added or not 
	public function subaccountdetailsfetchmodel() {
		$this->db->select('generalsettingid');
		$this->db->from('generalsetting');
		$this->db->where('generalsetting.companyid',2);
		$this->db->where('generalsetting.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			echo 'TRUE';
		} else {
			echo 'FALSE';
		}
	}
	//Simple function to store a given refresh_token on a database
	function setRefreshToken ($email, $token) {
		if (isset($token) && isset($email)) {
			//$result = mysqli_query($con,"UPDATE mytable SET refresh_token='" . $token . "'");
		}
	}
	//Retrieves the refresh_token from our database.
	function getRefreshToken ($email) {
		$this->db->select('generalsettingid');
		$this->db->from('generalsetting');
		$this->db->where('generalsetting.companyid',2);
		$this->db->where('generalsetting.status',1);
		$result = $this->db->get();
		if ($rows == 0) {
			return "";
		} else {
			$row = mysqli_fetch_array($result);
			return $row['Refresh'];
		}
	}
}