<!-- Password overlay -->

<div class="large-12 small-12 medium-12 columns" style="position:absolute;">
		<div class="overlay" id="passwordoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
			<div class="row sectiontoppadding">&nbsp;</div>
			<div class="large-4 medium-4 small-12 large-centered medium-centered small-centered columns ">
				<div class="large-12 column paddingzero">
					<div class="row">&nbsp;</div>
					<form method="POST" name="userpasswordform" style="" id="userpasswordform" action="" enctype="" class="clearpassworddataform">
					<div id="userpasswordvalidation" class="validationEngineContainer">
					<div class="large-12 columns end paddingbtm ">
						<div class="large-12 columns cleardataform z-depth-5 border-modal-8px" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff">
							<div class="large-12 columns sectionheaderformcaptionstyle">Change Password</div>
							<div class="large-12 columns sectionpanel" style="padding: 15px 15px;">
								<div class="static-field  overlayfield">
									<label>Old Password<span class="mandatoryfildclass">*</span></label>
							<input type="password" id="oldpassword" name="oldpassword" value="" class="funcCall[oldpasswordcheck] validate[required]" >
								</div>
								
								<div class="input-field overlayfield">
									<label>New Password<span class="mandatoryfildclass">*</span></label>
							<input type="password" id="newpassword" name="newpassword" value="" class="validate[required,minSize[6],maxSize[30]]" >
								</div>
								<div class="static-field overlayfield">
									<label>Confirm Password<span class="mandatoryfildclass">*</span></label>
							<input type="password" id="confirmnewpassword" name="confirmnewpassword" value="" class="validate[required,minSize[6],maxSize[30],equals[newpassword]]" >
							<input type="hidden" id="empid" name="empid" value="" class="" >
								</div>
								<div class="static-field overlayfield oldtagdatadiv hidedisplay">
							     <label>Old Item Details<span class="mandatoryfildclass" id="">*</span></label>
								   <select id="tagolditemdetails" name="tagolditemdetails" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="105">
									<option value=""></option>
								   </select>
							    </div>
							
							<div class="divider large-12 columns"></div>
							<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">
								<input type="hidden" name="itemtagid" id="itemtagid">
								<input type="hidden" name="tagtypetransferid" id="tagtypetransferid">
								<input type="hidden" name="purity" id="purity">
								<input type="hidden" name="product" id="product">
								<input type="hidden" name="grossweighttransfer" id="grossweighttransfer">
								<input type="hidden" name="stoneweighttransfer" id="stoneweighttransfer">
								<input type="hidden" name="netweighttransfer" id="netweighttransfer">
								<input type="hidden" name="transferpieces" id="transferpieces">
								<input type="hidden" name="transfermelting" id="transfermelting">
								<input type="hidden" name="transferfromcounterid" id="transferfromcounterid">
								<input type="button" id="passwordeditsbtbtn" name="passwordeditsbtbtn" value="Submit" class="alertbtn" >
								
								<input type="button" id="passwordoverlaycancel" name="" value="Cancel" tabindex="107" class="flloop alertbtn alertsoverlaybtn" ></div>
						
							</div>
							</div>
						</div>
						</div>
					</form>
					
				</div>
			</div>		
		</div>
	
	</div>
	<script type="text/javascript">
	
	$('#passwordoverlaycancel').click(function(){
			clearform('userpasswordform');
			$('#passwordoverlay').fadeOut();
		});
	</script>