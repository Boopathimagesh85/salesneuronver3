<style type="text/css">
#s2id_salutationid {
position:relative;
top:-6px !important;
}
</style>
<?php
	$dataset['action'][0] = array('actionid'=>'newediticon','actiontitle'=>'Edit','actionmore'=>'Yes','ulname'=>'printtemplate');
	$dataset['moduleid'] = '265';
	$this->load->view('Base/formwithgridmenuheader',$dataset);
?>
<div class="personalsettingaddform" style="opacity:0;">
<div class="large-12 columns addformunderheader">&nbsp;</div>
<div class="large-12 columns scrollbarclass addformcontainer">
	<!-- More Action DD start -- Gowtham -->
	<ul id="printtemplateadvanceddrop" class="multinavaction-drop arrow_box" style="white-space: nowrap; position: absolute; top: 34.8px; left: -62.1666px; opacity: 1; display: none;">
		<li id="userpasswordov"><span class="icon-box"><i class="material-icons" title="Cange Password">security</i>Change Password</span></li>
		<li id="dataupdatesubbtn" class="updatebtnclass"><span class="icon-box"><i class="" title="Submit"><i class="material-icons">save</i></i>Submit</span></li>
	</ul>
	<!-- More Action DD END -- Gowtham -->
	<div class="row">&nbsp;</div>
	<form method="POST" name="dataaddform" class="" action ="" id="dataaddform">
		<span id="formaddwizard" class="validationEngineContainer" >
			<span id="formeditwizard" class="validationEngineContainer">
				<?php
                    //function call for form fields generation
					formfieldstemplategenerator($modtabgrp);
                ?>
			</span>
		</span>
		<!--hidden values-->
		<?php
			echo hidden('primarydataid','');
			$value = 'employeeaddress,mailsetting';
			echo hidden('resctable',$value);
			//hidden text box view columns field module ids
			$modid = $this->Basefunctions->getmoduleid($filedmodids);
			echo hidden('viewfieldids',$modid);
			echo hidden('employeeid',$numrows);
		?>
	</form>
	<!--Password overlay-->
	<?php
		$this->load->view('passwordoverlay');
	?>
</div>
</div>
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="personalsettingsoverlay" >	
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="alert-panel">
			<div class="alertmessagearea">
				<div class="alert-title">Confirmation</div>
				<div class="alert-message">
					Do You Want to Cancel it?
				</div>
			</div>
			<div class="alertbuttonarea">
				<span class="firsttab" tabindex="1000"></span>
				<input type="hidden" id="geventid" name="gdeleventid" value="" class="" >
				<input type="button" id="personalsettingscloseyes" name="" value="Yes" tabindex="1001" class="alertbtn ffield" >
				<input type="button" id="personalsettingscloseno" name="" value="No" tabindex="1002" class="alertbtn flloop  alertsoverlaybtn" >	
				<span class="lasttab" tabindex="1003"></span>
			</div>
		</div>
	</div>
</div>