<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
	<script src="<?php echo base_url();?>js/Personalsettings/personalsettings.js" type="text/javascript"></script> 
</head>
<body>
	<?php
		$device = $this->Basefunctions->deviceinfo();
		$dataset['gridenable'] = "no"; //no
		$dataset['griddisplayid'] = "employeegriddisplay";
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['spanattr'] = array();
		$dataset['gridtableid'] = "employeegrid";
		$dataset['moduleid'] = $moduleids;
		$dataset['griddivid'] = "employeegriddiv";
		$dataset['forminfo'] = array(array('id'=>'personalsettingsform','class'=>'','formname'=>'personalsettingsform'));
		if($device=='phone') {
			$this->load->view('Base/gridmenuheadermobile',$dataset);
			$this->load->view('Base/overlaymobile');
		} else {
			$this->load->view('Base/gridmenuheader',$dataset);
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
		$this->load->view('Base/basedeleteform');
	?>
</body>
	<?php $this->load->view('Base/bottomscript'); ?>
</html>