<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Personalsettings extends MX_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Personalsettings/Personalsettingsmodel');  	
	}
	//Default Function
	public function index() {
		$moduleid = array(265);
		sessionchecker($moduleid);
		//action
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(265);
		$viewmoduleid = array(265);
		$info = array(265);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$data['numrows']=$this->Personalsettingsmodel->numofrows($info);
      	$this->load->view('Personalsettings/personalsettingsview',$data);       
	}
	//information fetch
	public function fetchformdataeditdetails() {
		$moduleid = 265;
		$this->Personalsettingsmodel->informationfetchmodel($moduleid);
	}	
	//Update old data
    public function datainformationupdate() {
        $this->Personalsettingsmodel->datainformationupdatemodel();
    }
	//primary and secondary address value fetch
	public function primaryaddressvalfetch()
	{
		$this->Personalsettingsmodel->primaryaddressvalfetchmodel();
	}
	//editor value fetch 	
	public function editervaluefetch() {
		$filename = $_GET['filename'];
		$newfilename = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1])) {
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]);
			echo json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			echo json_encode($tccontent);
		}
	}
	//password update
	public function passwordupdate() {
		$this->Personalsettingsmodel->passwordupdatemodel();
	}	
	//old password validate
	public function oldpasswordcheck() {
		$this->Personalsettingsmodel->oldpasswordcheckmodel();
	}
	//mail settings details fetch
	public function mailsettingsdetialfetch() {
		$this->Personalsettingsmodel->mailsettingsdetialfetchmodel();
	}
	//mail details fetch on the provider
	public function mailsettingdatafetch() {
		$this->Personalsettingsmodel->mailsettingdatafetchmodel();
	}
	//check the mail add ons added or not 
	public function subaccountdetailsfetch() {
		$this->Personalsettingsmodel->subaccountdetailsfetchmodel();
	}
	//PHP google calendar authorization
	public function calendarauthorization() {
		$this->Personalsettingsmodel->calendarauthorizationmodel();
	}
}