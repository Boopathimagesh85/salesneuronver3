<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Taxname extends MX_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->view('Base/formfieldgeneration');
        $this->load->helper('formbuild');
		$this->load->model('Taxname/Taxnamemodel');	
    }
    //first basic hitting view
    public function index() {        
    	$moduleid = array(221);
    	sessionchecker($moduleid);
		//action
		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(221);
		$viewmoduleid = array(221);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
      	$this->load->view('Taxname/taxnameview',$data);
	}
	//create taxregion
	public function newdatacreate() {  
    	$this->Taxnamemodel->newdatacreatemodel();
	}
	//information fetchtaxregion
	public function fetchformdataeditdetails() {
		$moduleid = 221;
		$this->Taxnamemodel->informationfetchmodel($moduleid);
	}
	//update taxregion
    public function datainformationupdate() {
        $this->Taxnamemodel->datainformationupdatemodel();
    }
	//delete taxregion
    public function deleteinformationdata() {
        $moduleid = 221;
		$this->Taxnamemodel->deleteoldinformation($moduleid);
    } 
	//unique name restriction
	public function uniquedynamicviewnamecheck(){
		$this->Taxnamemodel->uniquedynamicviewnamecheckmodel();
	}
	/**
	*Retrieves the Tax Of Simple Type for compound taxes
	*@returns the simple tax types
	*/
	public function getsimpletax(){
		$this->Taxnamemodel->getsimpletax();
	}
	//tax category reload in tax name
	public function fetchdddataviewddval(){
		$this->Taxnamemodel->fetchdddataviewddvalmodel();
	}
}