<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Taxnamemodel extends CI_Model{
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//taxregion create
	public function newdatacreatemodel()  {
		//table and fields information
		$formfieldsname = explode(',',$_POST['taxnameelementsname']);
		$formfieldstable = explode(',',$_POST['taxnameelementstable']);
		$formfieldscolmname = explode(',',$_POST['taxnameelementscolmn']);
		$elementpartable = explode(',',$_POST['taxnameelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);		
		$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
		$taxid=$this->db->insert_id();
		//update the compound tax on compound type
		if($_POST['taxruleid'] == '3' AND $_POST['compoundon'] != '' ){
			$taxid=$this->db->insert_id();
			$compoundon=array('compoundon'=>trim($_POST['compoundon']));
			$this->db->where('taxid',$taxid);
			$this->db->update('tax',$compoundon);
		}
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' created Tax - '.$_POST['taxname'].'';
		$this->Basefunctions->notificationcontentadd($taxid,'Added',$activity ,$userid,221);
		echo 'TRUE';
	}
	//Retrive taxregion data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['taxmasterdetailselementsname']);
		$formfieldstable = explode(',',$_GET['taxmasterdetailselementstable']);
		$formfieldscolmname = explode(',',$_GET['taxmasterdetailselementscolmn']);
		$elementpartable = explode(',',$_GET['taxmasterdetailselementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['taxmasterdetailsprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		//retrieve compound tax data
		$mdata=$this->db->query("SELECT compoundon FROM tax
						  WHERE taxid = $primaryid");
		foreach($mdata->result() as $info){
			$compounddata['compoundon']=$info->compoundon;
		}
		$result = json_decode($result,true);
		$cresult= array_merge($result,$compounddata);
		$result=json_encode($cresult);	
		echo $result;
	}
	//taxregion update
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['taxnameelementsname']);
		$formfieldstable = explode(',',$_POST['taxnameelementstable']);
		$formfieldscolmname = explode(',',$_POST['taxnameelementscolmn']);
		$elementpartable = explode(',',$_POST['taxnameelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['taxnameprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		//update the compound tax on compound type
		$ludate = date($this->Basefunctions->datef);
		$luuserid= $this->Basefunctions->userid;
		if($_POST['taxruleid'] == '3' AND $_POST['compoundon'] != '' ){		
			$compoundon=array('compoundon'=>trim($_POST['compoundon']),'lastupdatedate'=>$ludate,'lastupdateuserid'=>$luuserid);
			$this->db->where('taxid',$primaryid);
			$this->db->update('tax',$compoundon);
		}
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$activity = ''.$user.' updated Tax - '.$_POST['taxname'].'';
		$this->Basefunctions->notificationcontentadd($primaryid,'Updated',$activity ,$userid,221);
		echo $result;
	}
	//taxregion delete
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['taxmasterdetailselementstable']);
		$parenttable = explode(',',$_GET['taxmasterdetailselementspartable']);
		$id = $_GET['taxmasterdetailsprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		$ludate = date($this->Basefunctions->datef);
		$luuserid= $this->Basefunctions->userid;
		//audit-log
		$userid = $this->Basefunctions->logemployeeid;
		$user = $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid);
		$taxname = $this->Basefunctions->singlefieldfetch('taxname','taxid','tax',$id);
		$activity = ''.$user.' deleted Tax - '.$taxname.'';
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				if($this->Basefunctions->industryid !=3){
					$delete=array('status'=>0,'lastupdatedate'=>$ludate,'lastupdateuserid'=>$luuserid);
					$this->db->where_in('taxid',$id);
					$this->db->update('taxterritories',$delete);
				}
				$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,221);
				echo "TRUE";
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			if($this->Basefunctions->industryid !=3){
				$delete=array('status'=>0,'lastupdatedate'=>$ludate,'lastupdateuserid'=>$luuserid);
				$this->db->where_in('taxid',$id);
				$this->db->update('taxterritories',$delete);
			}
			$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,221);
			echo "TRUE";
		}
	}	//unique name check
	public function uniquedynamicviewnamecheckmodel() {
		$primaryid=$_POST['primaryid'];
		$accname=$_POST['accname'];
		$elementpartable = explode(',',$_POST['elementspartabname']);
		$partable =  $this->Crudmodel->filtervalue($elementpartable);
		$ptid = $partable.'id';
		$ptname ='taxname';
		if($accname != "") {
			$result = $this->db->select($ptname.','.$ptid)->from($partable)->where($partable.'.'.$ptname,$accname)->where($partable.'.status',1)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->$ptid;
				} 
			} else { echo "False"; }
		}else { echo "False"; }
	}
	/**
	*Retrieves the Tax Of Simple Type for compound taxes
	*@returns the simple tax types
	*/
	public function getsimpletax(){
		$array=array();
		$data=$this->db->select('taxname,taxid,taxrate')
					->from('tax')
					->where('taxruleid',2) //simple tax type
					->get()
					->result();
		foreach($data as $info){
			$array[]=array('id'=>$info->taxid,'name'=>$info->taxname,'rate'=>$info->taxrate);
		}
		echo json_encode($array);
	}
	//tax category reload in tax name
	public function fetchdddataviewddvalmodel() {
		$i = 0;
		$fieldname = $_GET['dataname'];
		$fieldid = $_GET['dataid'];
		$tablename = $_GET['datatab'];
		$this->db->select("$fieldid,$fieldname,setdefault,gsttax");
		$this->db->from("$tablename");
		$this->db->where("$tablename".'.status',1);
		$result=$this->db->get();
		if($result->num_rows() > 0){
			foreach($result->result() as $row) {
				$data[$i]=array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname,'setdefault'=>$row->setdefault,'gstapply'=>$row->gsttax);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
}