<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<span class="gridviewdivforsh">
				<div class="large-12 columns headercaptionstyle headerradius">
					<div class="large-6 medium-6 small-12 columns headercaptionleft">
						<span class="fa fa-align-justify left-off-canvas-toggle headermainmenuicon"> </span>			
						<input type="button" class="btn hidedisplay" name="updatebtn" id="updatebtn" /> 
						<span class="gridcaptionpos">
							<?php echo span($maingridtitle,$spanattr); ?> <span>-</span> <span id="viewgridheaderid"></span>
						</span>
					</div>
					<div class="large-5 medium-5 small-12 columns foriconcolor righttext small-only-text-center"> 
						<!-- Action menu generation -->
						<?php
							$size = 6;
							$value = array();
							$des = array();
							$title = array();
							foreach($actionassign as $key):
								$value[$key->toolbarnameid] = $key->toolbarname;
								$des[] = $key->description;
								$title[] = $key->toolbartitle;
							endforeach;
							echo actionmenu($value,$size,$des,$title,'taxname','icon-w ');
							$menucount = count($value);
							//user menu
							$this->load->view('Base/usermenugenerate');
						?>
						   <!-- Settings action events generation -->
						<?php
							if($menucount > $size) {
								 echo '<nav class="top-bar" data-topbar style="background:none; height:0px; display: inline-block;line-height: 25px;"> <!--data-options="is_hover: false"-->
									<section class="top-bar-section" style="top:-18px">
										<ul class="right" style="height: 20px !important">
										  <li class="has-dropdown">
											<span id="" title="" class="fa fa-ellipsis-v fa-2x" style="padding-top:0.2rem;padding-right:0.5rem"> </span>
											<ul class="dropdown" style="width:12rem">';
													$attr = array("style"=>"font-size:1.3rem");
													echo actionmoremenu($value,$size,$attr,$des,$title,'taxcategory','icon-w ');
											echo '</ul>
										  </li>
										</ul>
									</section>
								</nav>';
							}
						?>
					</div>
				</div>
				<!-- tab group creation -->
				<div class="large-12 columns tabgroupstyle">
					 <?php
						$ulattr = array("class"=>"tabs");
						$uladdinfo = "data-tab";
						$tabgrpattr = array("class"=>"tab-title sidebaricons");
						$tabstatus = "active";
						$dataname = "subform";
						echo tabgroupgenerate($ulattr,$uladdinfo,$tabgrpattr,$tabstatus,$dataname,$modtabgrp);
					?>
				</div>			
				<?php
					//function call for form fields generation
					formfieldsgeneratorwithgrid($modtabsecfrmfkdsgrp,$filedmodids);
				?>
			</span>
		</div>
	</div>
</div>
<!--Overlay-->			
<div>
	<?php $this->load->view('Base/viewselectionoverlay'); ?> 
</div>