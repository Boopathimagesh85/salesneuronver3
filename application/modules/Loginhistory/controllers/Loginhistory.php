<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Loginhistory extends CI_Controller {
	function __construct() {
    	parent::__construct();	
		$this->load->model('Base/Basefunctions');
		$this->load->model('Loginhistory/Loginhistorymodel');
		$this->load->helper('formbuild');
		$this->load->library('excel');
    }
	public function index() {
		$moduleid = array(245);
		sessionchecker($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$this->load->view('Loginhistory/loginhistoryview',$data);
	}
	/* Login history information fetch */
	public function loginhistorygriddatafetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'loginhistory.loginhistoryid') : 'loginhistory.loginhistoryid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('employeename','emailid','ipaddress','signintime','signouttime','loginstatus','browsername','platformname'),'colmodelindex'=>array('employee.employeename','employee.emailid','loginhistory.ipaddress','loginhistory.signintime','loginhistory.signouttime','loginhistory.loginstatus','loginhistory.browsername','loginhistory.platformname'),'coltablename'=>array('employee','employee','loginhistory','loginhistory','loginhistory','loginhistory','loginhistory','loginhistory'),'uitype'=>array('2','2','2','31','31','2','2','2'),'colname'=>array('User Name','Email Id','IP Address','Sign In Time','Sign Out Time','Status','Browser','Platform'),'colsize'=>array('150','200','150','150','150','100','400','250'));
		$result=$this->Loginhistorymodel->loginhistorygriddatafetchmodel($primarytable,$sortcol,$sortord,$pagenum,$rowscount);
		$device = $this->Basefunctions->deviceinfo();
		if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Login History',$width,$height);
		} else {
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Login History',$width,$height);
		}
		echo json_encode($datas);
	}
	//excel report
	public function exceldataexport() {
		$this->Loginhistorymodel->exceldataexportmodel();
	}
}