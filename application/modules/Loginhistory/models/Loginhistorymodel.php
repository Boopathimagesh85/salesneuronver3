<?php
Class Loginhistorymodel extends CI_Model {
	public function __construct() {
		parent::__construct(); 
    }
	//Login history information view
	public function loginhistorygriddatafetchmodel($tablename,$sortcol,$sortord,$pagenum,$rowscount) {
		$dataset = 'loginhistory.loginhistoryid,employee.employeename,employee.emailid,loginhistory.ipaddress,loginhistory.signintime,loginhistory.signouttime,loginhistory.loginstatus,loginhistory.browsername,loginhistory.platformname';
		$join=' LEFT OUTER JOIN employee ON employee.employeeid=loginhistory.employeeid';
		$join.=' LEFT OUTER JOIN status ON status.status=loginhistory.status';
		$status = $tablename.'.status IN (1,2)';
		$status .= $tablename.'.industry IN ('.$this->Basefunctions->industryid.')';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		//query
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$status.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($data,$page,'');
		return $finalresult;
	}
	//data information fetch
	public function datainformationfetch() {
		$format = $this->Basefunctions->phpmysqlappdateformatfetch();
		$dataset = 'loginhistory.loginhistoryid,employee.employeename,employee.emailid,loginhistory.ipaddress,DATE_FORMAT(loginhistory.signintime,"'.$format["mysqlformat"].' %H:%i") AS signintime,DATE_FORMAT(loginhistory.signouttime,"'.$format["mysqlformat"].' %H:%i") AS signouttime,loginhistory.loginstatus,loginhistory.browsername,loginhistory.platformname';
		$tablename = 'loginhistory';
		$join=' LEFT OUTER JOIN employee ON employee.employeeid=loginhistory.employeeid';
		$join.=' LEFT OUTER JOIN status ON status.status=loginhistory.status';
		$status = $tablename.'.status IN (1,2)';
		$status = $tablename.'.industryid IN ('.$this->Basefunctions->industryid.')';
		$w = '1=1';
		$data = $this->db->query('select '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$status.' ');
		return $data;
	}
	//export data
	public function exceldataexportmodel() {
		//data fetch
		$moduleid = $_POST['moduleid'];
		$title = $_POST['modulename'];
		$modulename = $_POST['modulename'];
		$name = preg_replace('/[^A-Za-z0-9]/', '', $title);
		$fname = strtolower($name);
		$datatitle = 'User Name,Email Id,Ip Address,SignIn Time,SignOut Time,Status,Browser,Platform';
		$datainfo = 'employeename,emailid,ipaddress,signintime,signouttime,loginstatus,browsername,platformname';
		$result = $this->datainformationfetch(); //print_r($result->result()); exit;
		$this->excel->setActiveSheetIndex(0);
		$sheet = $this->excel->getActiveSheet();
		$sheet->setTitle($title);
		$count = 0;
		$celtitle = explode(',',$datatitle);
		$titlecount = count($celtitle);
		$dataname = explode(',',$datainfo);
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(13);
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);	
		$this->excel->getActiveSheet()->setCellValue('A1','Sl.No');
		$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(50);
		$x=1;
		$h='B';
		for($i=0;$i<$titlecount;$i++) {
			$this->excel->getActiveSheet()->getStyle($h.$x)->getFont()->setSize(13);
			$this->excel->getActiveSheet()->getStyle($h.$x)->getFont()->setBold(true);	
			$this->excel->getActiveSheet()->setCellValue($h.$x,$celtitle[$i]);
			$this->excel->getActiveSheet()->getColumnDimension($h)->setAutoSize(50);
			$h++;
		}
		$m=1;
		$tot_rows = 1;
		$rows=2;
		foreach($result->result() as $row) {
			$c='B';
			$this->excel->getActiveSheet()->getStyle('A'.$rows)->getFont()->setBold(false);
			$this->excel->getActiveSheet()->setCellValue('A'.$rows,$m);
			$this->excel->getActiveSheet()->getStyle('A'.$rows)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			if ($rows % 2 == 0) {
				$this->excel->getActiveSheet()->getStyle('A'.$rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$this->excel->getActiveSheet()->getStyle('A'.$rows)->getFill()->getStartColor()->setARGB('e4f3fd');
			}
			$m++;
			for($i=0;$i<$titlecount;$i++) {
				$dname = $dataname[$i];
				$data = trim($row->$dname) == '00-00-0000 00:00' ? '' : trim($row->$dname);
				$this->excel->getActiveSheet()->getStyle($c.$rows)->getFont()->setBold(false);
				$this->excel->getActiveSheet()->getStyle($c.$rows)->getAlignment()->setWrapText(true);		
				$this->excel->getActiveSheet()->setCellValue($c.$rows,trim($data));
				$this->excel->getActiveSheet()->getStyle($c.$rows)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				if( is_numeric(trim($data)) ) {
					$this->excel->getActiveSheet()->getStyle($c.$rows)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_GENERAL);
					$this->excel->getActiveSheet()->getStyle($c.$rows)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				}
				if ($rows % 2 == 0) {
					$this->excel->getActiveSheet()->getStyle($c.$rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
					$this->excel->getActiveSheet()->getStyle($c.$rows)->getFill()->getStartColor()->setARGB('e4f3fd');
				}
				$c++;
			}
			$rows++;
			$tot_rows++;
		}
		ob_clean();
		$time = date("d-m-Y-g-i-s-a");
		$filename=$fname.'_'.$time.'.xlsx';
		header("Content-type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8;");
		header("Content-type:application/octetstream");
		header("Content-Disposition:attachment;filename=".$filename."");
		header("Content-Transfer-Encoding:binary");
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
		$objWriter->save('php://output');
		//notification trigger in export
		$empid = $this->Basefunctions->logemployeeid;
		$empname = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$empid);
		$notimsg = $empname." "."Exported ".$modulename;
		$this->Basefunctions->notificationcontentadd(1,'Export',$notimsg,1,$moduleid);
	}
}
?>