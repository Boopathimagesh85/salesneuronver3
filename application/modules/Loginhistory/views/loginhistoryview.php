<!DOCTYPE HTML>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); 
	$device = $this->Basefunctions->deviceinfo();
	?>
	
</head>
<body class="hidedisplay">
<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<div id="loginhistoryview" class="gridviewdivforsh">
				<div class="large-12 columns paddingzero viewheader">
				<?php
						$dataset['gridtitle'] = $gridtitle['title'];
						$dataset['titleicon'] = $gridtitle['titleicon'];
						$dataset['formtype'] = 'logaudmodform';
						$dataset['gridid'] = 'loginhistorygrid';
						$dataset['gridwidth'] = 'loginhistorygridwidth';
						$dataset['gridfooter'] = 'loginhistorygridfooter';
						$this->load->view('Base/mainviewheader',$dataset);
						$dataset['moduleid'] = '245';
						$this->load->view('Base/singlemainviewformwithgrid',$dataset);
					?>
				</div>
			</div>			
		</div>
	</div>
</div>
<?php
		if($device=='phone') {
			$this->load->view('Base/overlaymobile');
		} else {
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/modulelist');
	?>
<!-- Login More information Overlay -->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts" id="loginhistoryoverlay">
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="large-9 medium-9 large-centered medium-centered columns borderstyle">
			<form method="POST" name="convertoverlayform" style="" id="convertoverlayform" action="" enctype="" class="">
				<span id="convertoverlayvalidation" class="validationEngineContainer">
					<div class="row" style="background:#ffffff">
						<div class="large-12 columns headerformcaptionstyle">
							<div class="small-10 columns" style="background:none">Login History</div>
							<div class="small-1 columns" id="historyoverlayclose" style="text-align:right;cursor:pointer;background:none;position: relative;
    left: 100px;">X</div>
						</div>
						<div class="large-12 columns">
							<div class="large-3 columns"><label>User Name : </label></div>
							<div class="large-9 columns"><label id="ovusername"></label></div>
						</div>
						<div class="large-12 columns">
							<div class="large-3 columns"><label>Email Id : </label></div>
							<div class="large-9 columns"><label id="emailid"></label></div>
						</div>
						<div class="large-12 columns">
							<div class="large-3 columns"><label>IP Address : </label></div>
							<div class="large-9 columns"><label id="ipaddress"></label></div>
						</div>
						<div class="large-12 columns">
							<div class="large-3 columns"><label>Sign In Time : </label></div>
							<div class="large-9 columns"><label id="signintime"></label></div>
						</div>
						<div class="large-12 columns">
							<div class="large-3 columns"><label>Sign Out Time : </label></div>
							<div class="large-9 columns"><label id="signouttime"></label></div>
						</div>
						<div class="large-12 columns">
							<div class="large-3 columns"><label>Status : </label></div>
							<div class="large-9 columns"><label id="status"></label></div>
						</div>
						<div class="large-12 columns">
							<div class="large-3 columns"><label>Browser Name : </label></div>
							<div class="large-9 columns"><label id="browsername"></label></div>
						</div>
						<div class="large-12 columns">
							<div class="large-3 columns"><label>Platform Name : </label></div>
							<div class="large-9 columns"><label id="platformname"></label></div>
						</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
				</span>
			</form>
		</div>
	</div>
</div>
</body>
	<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<?php $this->load->view('Base/bottomscript'); ?>
	<!-- js File -->
	<script src="<?php echo base_url();?>js/Loginhistory/loginhistory.js" type="text/javascript"></script>	
</html>