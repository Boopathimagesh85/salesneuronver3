<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<div id="productaddonview" class="gridviewdivforsh">
				<?php
					$loggedinuserroleid = $this->Basefunctions->userroleid;
					$dataset['gridtitle'] = $gridtitle;
					$dataset['titleicon'] = $titleicon;
					$dataset['modtabgrp'] = $modtabgrp;
					$dataset['gridid'] = 'productvendorgrid';
					$dataset['gridwidth'] = 'productvendorgridwidth';
					$dataset['gridfooter'] = 'productvendorgridfooter';
					$dataset['viewtype'] = 'disable';
					$dataset['moduleid'] = '139';
					$this->load->view('Base/singlemainviewformwithgrid.php',$dataset);
				?>
			</div>
		<div id="productvendorformdiv" class="singlesectionaddform">
		<input type ="hidden" id="weight_round" name="weight_round" value="<?php echo $weight_round; ?>">
		<div class="large-12 columns addformunderheader">&nbsp;</div>
		<div class="large-12 columns scrollbarclass addformcontainer">
			<div class="row mblhidedisplay">&nbsp;</div>
			<div id="productvendorcharge">
				<div class="large-12 columns mblnopadding">
					<div class="closed effectbox-overlay effectbox-default " id="groupsectionoverlay">
					<div class="row sectiontoppadding">&nbsp;</div>
					<form method="POST" name="productvendorform" class="productvendorform cleardataform"  id="productvendorform">
						<div id="productvendoraddvalidate" class="validationEngineContainer" >			
							<div id="productvendorupdatevalidate" class="validationEngineContainer" >			
								<div class="large-4 large-offset-4 columns paddingbtm">	
									<div class="large-12 columns cleardataform  border-modal-8px" style="padding-left: 0 !important; padding-right: 0 !important;">
									<div class="large-12 columns sectionheaderformcaptionstyle">Product Vendor</div>
										<div class="large-12 columns sectionpanel">
										<div class="static-field large-12 columns ">
											<label>Product Name<span class="mandatoryfildclass" id="productid_req">*</span></label>
											<select id="productid" name="productid" class="chzn-select" data-validation-engine="validate[required]" data-prompt-position="topLeft:14,36" tabindex="100">
												<option value="" data-purityid="0"></option>
												<?php foreach($product->result() as $key):
												?>
												<option value="<?php echo $key->productid;?>" data-counterid="<?php echo $key->counterid;?>" data-productname="<?php echo $key->productname;?>" data-purityid="<?php echo $key->purityid;?>" data-chargeid="<?php echo $key->chargeid;?>" data-purchasechargeid="<?php echo $key->purchasechargeid;?>" data-size="<?php echo $key->size;?>"> <?php if($mainlicense['productidshow'] == 'YES') { echo $key->productid.' - '.$key->productname; } else { echo $key->productname;} ?></option>
												<?php endforeach;?>
											</select>
										</div>
										<div class="static-field large-12 columns" id="sizemasteriddivhid">
											<label>Size</label>
											<select id="sizemasterid" name="sizemasterid" class="chzn-select" data-validation-engine="" tabindex="103" >
												<?php foreach($sizemaster->result() as $key):?>
												<option value="<?php echo $key->sizemasterid;?>" data-productid="<?php echo $key->productid;?>" data-sizemastername="<?php echo $key->sizemastername;?>">
												<?php echo $key->sizemastername;?></option>
												<?php endforeach;?>		
											</select>
										</div>
										<div class="static-field large-12 columns">
											<label>Vendor<span class="mandatoryfildclass" id="accountid_req">*</span></label>
											<select id="accountid" name="accountid" class="chzn-select" data-validation-engine="validate[required]" tabindex="103" >
												<?php foreach($account->result() as $key):?>
												<option value="<?php echo $key->accountid;?>" data-accountid="<?php echo $key->accountid;?>" data-accountname="<?php echo $key->accountname;?>">
												<?php echo $key->accountname;?></option>
												<?php endforeach;?>		
											</select>
										</div>
										<div class="input-field large-6 columns ">
											<input type="text" class="" id="productvendorfromweight" name="productvendorfromweight" value="" tabindex="106" data-validation-engine="validate[required,decval[<?php echo $weight_round?>],custom[number],min[0.1]]">
											<label for="productvendorfromweight">From Weight<span class="mandatoryfildclass">*</span></label>
										</div>
										<div class="input-field large-6 columns ">
											<input type="text" class="" data-validategreat="" id="productvendortoweight" name="productvendortoweight" value="" tabindex="107" data-validation-engine="validate[required,decval[<?php echo $weight_round?>],custom[number]]">
											<label for="productvendortoweight">To Weight<span class="mandatoryfildclass">*</span></label>
										</div>
										<div class="row">&nbsp;</div>
										</div>
										<div class="divider large-12 columns"></div>
										<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">
											<input type ="hidden" value="" id="primaryproductvendorid" name="primaryproductvendorid"/>
											<input type ="hidden" id="productvendorsortcolumn" name="productvendorsortcolumn"/>
											<input type ="hidden" id="productvendorsortorder" name="productvendorsortorder"/>
											<input type="button" class="alertbtn addkeyboard" id="productvendoradd" name="productvendoradd" value="Submit" tabindex="">
											<input type="button" class="alertbtn updatekeyboard" id="productvendorupdate" name="productvendorupdate" value="Submit" tabindex="" style="display: none;">	
											<input type="button" class="alertbtn addsectionclose"  value="Cancel" name="cancel" tabindex="">
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
					</div>
				</div>
		   </div>
		</div>
	</div>
		</div>			
	</div>
</div>
<!--Productvendor Double Alert Overlay-->
<div class="large-12 columns" style="position:absolute;">
		<div class="overlay alertsoverlay overlayalerts" id="productrolalertsdouble">	
			<div class="row">&nbsp;</div>
			<div class="row sectiontoppadding"></div>
			<div class="">
				<div class="row">&nbsp;</div>
				<div class="alert-panel">
					<div class="alertmessagearea">
					<div class="alert-title">Product Vendor Details</div>
					<div class="alert-message">
					<span class="" style="display:block;padding-bottom:5px;font-size:0.9rem">Product : <span id="alertproductname"></span> </span>
						<span class="" style="display:block;padding-bottom:5px;font-size:0.9rem">Vendor: <span id="alerttypename"></span> </span>
						<span class="" style="display:block;padding-bottom:5px;font-size:0.9rem">Size Master : <span id="alertsizemastername"></span> </span>
						<span class="" style="display:block;padding-bottom:5px;font-size:0.9rem">From weight : <span id="alertfromweight"></span> </span>
						<span class="" style="display:block;padding-bottom:5px;font-size:0.9rem">To weight : <span id="alerttoweight"></span> </span>
						<div class="large-12 column">&nbsp;</div>
						<span class="" style="display:block;padding-bottom:5px;font-size:0.9rem"> Already The above combination of rol created.Do you want to edit the rol?</span>
						<div class="large-12 column">&nbsp;</div>
					</div>
					</div>
					<div class="alertbuttonarea">
						<span class="firsttab" tabindex="1000"></span>
						<input type="button" id="alertsdoubleeditproductvendor" name="" tabindex="1001" value="Yes" class="alertbtn  ffield" >
						<input type="button" id="alertsdoublecloseproductvendor" name="" value="No" tabindex="1002" class="flloop alertbtn alertsoverlaybtn" >
						<input type="hidden" id="editproductvendorid" name="editproductvendorid">
						<span class="lasttab" tabindex="1003"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!----Product charge copy overlay----->
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay alertsoverlay overlayalerts resetoverlay resetindividualoverlay" id="productaddoncopyalertsdouble" style="overflow-y: auto;overflow-x: hidden;">		
		<div class="row sectiontoppadding">&nbsp;</div>		
		<div class="large-4 medium-6 small-11 large-centered medium-centered small-centered columns" >
			<form method="POST" name="billnumberoverlayform" style="" id="billnumberoverlayform" action="" enctype="" class="overlayborder clearformbillnumberoverlay">
				<span id="billnumberoverlayvalidation" class="validationEngineContainer"> 
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 columns sectionheaderformcaptionstyle">
							Product Details
						</div>
						<div class="large-12 columns" style="background-color: #617d8a;">
						<div class="static-field large-12 columns">
							<label>Copy From<span class="mandatoryfildclass"></span></label>								
							<select id="chargedproduct" name="chargedproduct" class="chzn-select" data-prompt-position="topLeft:14,36" tabindex="100">
								<option value=""></option>
								
							</select>
						</div>
						</div>
					<?php
							$device = $this->Basefunctions->deviceinfo();
							if($device=='phone') {
								echo '<div class="large-12 columns paddingzero forgetinggridname" id="productchargecopygridwidth"><div id="productchargecopygrid" class="inner-row-content inner-gridcontent" style="height:300px;">
									<!-- Table header content , data rows & pagination for mobile-->
								</div>
								<!--<footer class="inner-gridfooter footercontainer" id="productchargecopygridfooter">
									 Footer & Pagination content 
								</footer>--></div>';
							} else {
								echo '<div class="large-12 columns forgetinggridname" id="productchargecopygridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="productchargecopygrid" style="max-width:2000px; height:300px;top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
							<!--<div class="inner-gridfooter footer-content footercontainer" id="productchargecopygridfooter">
									 Footer & Pagination content 
								</div>--></div>';
							}
						?></div>
					<div class="row" style="background:#f5f5f5">
						<div class="large-12 columns sectionalertbuttonarea" style="text-align: right">
							<input type="button" id="productchargecopygridsubmit" name="productchargecopygridsubmit" value="Submit" class="alertbtn">
							<input type="button" id="productchargeclose" name="productchargeclose" value="Cancel" class="alertbtn">
						</div>
					</div>
					<!-- hidden fields -->
					<input type="hidden" name="chargebasedproductid" id="chargebasedproductid" />
				</span>
			</form>
		</div>
	</div>
</div>
	