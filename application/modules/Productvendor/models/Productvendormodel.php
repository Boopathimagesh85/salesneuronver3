<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Productvendormodel extends CI_Model{
    public function __construct(){
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//sizemaster dd load function
	public function sizemasterdd() {
		$industryid = $this->Basefunctions->industryid;
		$info =$this->db->query("SELECT `sizemaster`.`sizemastername`, `sizemaster`.`sizemasterid`, `sizemaster`.`productid` FROM `sizemaster`  WHERE `sizemaster`.`status` = 1 ORDER BY `sizemaster`.`sizemasterid` DESC");
		return $info;
	}
	//account dd load function
	public function accountdd() {
		$industryid = $this->Basefunctions->industryid;
		$info =$this->db->query("SELECT `account`.`accountname`, `account`.`accountid` FROM `account`  WHERE `account`.`status` = 1 and `account`.`accountid` not in (2,3) and `account`.`accounttypeid` in (16) ORDER BY `account`.`accountid` DESC");
		return $info;
	}
	public function createproductvendor() {
		$productid = $_POST['productid'];
		if(isset($_POST['sizemasterid'])) {
			$sizemasterid=$_POST['sizemasterid'];
		} else {
			$sizemasterid=1;
		}
		$accountid =$_POST['accountid'];
		$productvendorfromweight =$_POST['productvendorfromweight'];
		$productvendortoweight =$_POST['productvendortoweight'];
		$chargearray = array(
			'productid'=>$productid,
			'sizemasterid'=>$sizemasterid,
			'accountid'=>$accountid,
			'productvendorfromweight'=>$productvendorfromweight,
			'productvendortoweight'=>$productvendortoweight,
			'industryid'=>$this->Basefunctions->industryid,
			'branchid'=>$this->Basefunctions->branchid
		);
		$chargearray = array_merge($chargearray,$this->Crudmodel->defaultvalueget());
		$this->db->insert('productvendor',array_filter($chargearray));
		echo 'SUCCESS';
}
	/**	*productchargedelete	*/
	public function productchargedelete() {
		$primaryid = $_GET['primaryid'];
		$this->db->where_in('productvendorid',$primaryid);
		$this->db->update('productvendor',$this->Basefunctions->delete_log());
		echo 'SUCCESS';
	}
	/**	* data retrive	*/
	public function productvendorretrieve() {
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2);
		$primary_id = $_GET['primaryid'];
		$this->db->select('productvendorid,productid,sizemasterid,accountid,ROUND(productvendor.productvendorfromweight,'.$round.') as productvendorfromweight,ROUND(productvendor.productvendortoweight,'.$round.') as productvendortoweight',false);
		$this->db->from('productvendor');
		$this->db->where('productvendor.productvendorid',$primary_id);				
		$this->db->limit(1);				
		$info=$this->db->get()->row();			
		$jsonarray=array(
			'primaryproductvendorid' => $info->productvendorid,
			'productid' => $info->productid,
			'sizemasterid'=> $info->sizemasterid,
			'accountid'=> $info->accountid,
			'productvendorfromweight' => $info->productvendorfromweight,
			'productvendortoweight' =>$info->productvendortoweight
		);
		echo json_encode($jsonarray);
	}
	/**	*update product vendor	*/
	public function productvendorupdate() {
		$productid = $_POST['productid'];
		if(isset($_POST['sizemasterid'])) {
			$sizemasterid=$_POST['sizemasterid'];
		} else {
			$sizemasterid=1;
		}
		$accountid =$_POST['accountid'];
		$productvendorfromweight =$_POST['productvendorfromweight'];
		$productvendortoweight =$_POST['productvendortoweight'];
		$primaryid = $_POST['primaryid'];
		$chargearray = array(
			'productid'=>$productid,
			'sizemasterid'=>$sizemasterid,
			'accountid'=>$accountid,
			'productvendorfromweight'=>$productvendorfromweight,
			'productvendortoweight'=>$productvendortoweight,
			'industryid'=>$this->Basefunctions->industryid,
			'branchid'=>$this->Basefunctions->branchid
		);
		$chargearray = array_merge($chargearray,$this->Crudmodel->defaultvalueget());
		$this->db->where_in('productvendorid',$primaryid);
		$this->db->update('productvendor',array_filter($chargearray));
         echo 'SUCCESS';
	}
	//check weight already exist or not
	public function checkweightexistmodel() {
		$productid = $_POST['productid'];
		$accountid = $_POST['accountid'];
		$sizemasterid = $_POST['sizemasterid'];
		$toweight = $_POST['toweight'];
		$fromweight = $_POST['fromweight'];
		$primaryid = $_POST['primaryid'];
		if($primaryid == ''){
			$primaryid = 1;
		}
		$round = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2);
		
		$table = 'productvendor';
		$tableprimaryid = $table.'id';
      	$this->db->select($tableprimaryid);
        $this->db->from($table);
		$this->db->where('productvendor.accountid',$accountid);
		$this->db->where('productvendor.productid',$productid);
		$this->db->where('productvendor.industryid',$this->Basefunctions->industryid);
		$this->db->where('productvendor.sizemasterid',$sizemasterid);
		if($primaryid != 1) {
			$this->db->where_not_in('productvendor.productvendorid',$primaryid);
		}
		$this->db->where("productvendortoweight >=",$toweight);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$res=$this->db->get();
		if($res->num_rows() > 0) {
			$data['count'] =$res->num_rows();
			$data['primaryproductvendorid']= $res->row()->$tableprimaryid;
		} else {
			$data['count'] = 0 ;
			$data['primaryproductvendorid']= '';
		}
		echo json_encode($data);
	}
}