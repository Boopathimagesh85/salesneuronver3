<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Productvendor extends MX_Controller{
    public function __construct(){
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Productvendor/Productvendormodel');	
		$this->load->model('Itemtag/Itemtagmodel');	
    }
    //first basic hitting view
    public function index() {
    	$moduleid = array(139);
    	sessionchecker($moduleid);
		//action
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		/* Product Addon
			*/
			$data['purity']=$this->Basefunctions->purity_groupdropdown();	
			$data['planstatus'] = $this->Basefunctions->planid;
			//counter-license.
			$data['mainlicense'] = array(
											'counter'=>$this->Basefunctions->get_company_settings('counter'),
					'chargedataid'=>$this->Basefunctions->get_company_settings('chargeid'),
					'purchasechargedataid'=>$this->Basefunctions->get_company_settings('purchasechargeid'),
					'chargecalcoption'=>$this->Basefunctions->get_company_settings('chargecalcoption'),
					'chargecalculation'=>$this->Basefunctions->get_company_settings('chargecalculation'),
					'productidshow'=>$this->Basefunctions->get_company_settings('productidshow'),
					'taxapplicable'=>$this->Basefunctions->get_company_settings('taxapplicable'),
					'businesschargeid'=>$this->Basefunctions->get_company_settings('businesschargeid'),
					'businesspurchasechargeid'=>$this->Basefunctions->get_company_settings('businesspurchasechargeid'),
					'stone'=>$this->Basefunctions->get_company_settings('stone'),
					'purchasewastage'=>$this->Basefunctions->get_company_settings('purchasewastage')
										); 
			$data['accounttype'] = $this->Basefunctions->simpledropdownwithcond('accounttypeid','accounttypename','accounttype','accounttypeid','16,6');
			$data['accountgroup']=$this->Basefunctions->simpledropdown_var('accountgroup','accountgroupid,accountgroupname,accounttypeid','accountgroupname');
			$data['weight_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
			$data['melting_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',4); //melting round-off
			$data['amount_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',3); 
			$data['jewelstatus']=1;
			$data['product'] = $this->Itemtagmodel->product_dd();
			$data['purity'] = $this->Basefunctions->purity_groupdropdown();
			$data['categoryid'] = $this->Basefunctions->get_company_settings('categoryid');
			$data['categoryname'] = $this->Basefunctions->singlefieldfetch('categoryname','categoryid','category',$data['categoryid']);
			$data['mode'] = $this->Basefunctions->simpledropdownwithcond('salestransactiontypeid','salestransactiontypename','salestransactiontype','salestransactiontypeid','9,11');
		$data['account'] = $this->Productvendormodel->accountdd();
		$data['sizemaster'] = $this->Productvendormodel->sizemasterdd();
		$this->load->view('Productvendor/productvendorview',$data);
	}
	public function createproductvendor() 
	{  
    	$this->Productvendormodel->createproductvendor();
    }
	/**	*delete product addon    */
	public function productchargedelete() 
	{  
    	$this->Productvendormodel->productchargedelete();
    }
	/**	*retrieve product addon  */
	public function productvendorretrieve() 
	{  
    	$this->Productvendormodel->productvendorretrieve();
    }
	/**	*update product addon    */
	public function productvendorupdate() 
	{  
    	$this->Productvendormodel->productvendorupdate();
    }
	//check weight already exist or not
	public function checkweightexist() {
		$this->Productvendormodel->checkweightexistmodel();
	}
}