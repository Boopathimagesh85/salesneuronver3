<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Storagemanagementmodel extends CI_Model
{
	private $storagemanagementmodule = 59;
    public function __construct()
    {
        parent::__construct();
		$this->load->model( 'Base/Basefunctions');
		$this->load->model( 'Base/Crudmodel');
		$this->load->model('Printtemplates/Printtemplatesmodel');
    }
	// get summary data based on counter
	public function loadsummary(){
		$counterid = $_POST['counter'];
		$counterarray = '';
		$storagemanagementdate = date('Y-m-d', strtotime($_POST['storagemanagementdate'])) ;
		$weightround =  $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2);
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('counterid,boxweight,comment,lastprintdatetime,lasttagtemplateid,lastprinttemplatesid');
		$this->db->from('counter');
		$this->db->where('counterid',$counterid);
		$counterdata = $this->db->get();
		if($counterdata->num_rows()>0) {
			foreach($counterdata->result() as $info) {
				$boxweight = number_format((float)($info->boxweight), $weightround, '.', '');
				$lastprintdatetime = $info->lastprintdatetime;
				$comment = $info->comment;
				$lasttagtemplateid = $info->lasttagtemplateid;
				$lastprinttemplatesid = $info->lastprinttemplatesid;
			}
		}
		$partialtagselectgwt = " COALESCE(sum(itemtag.grossweight),0) ";
		$partialtagselectnwt = " COALESCE(sum(itemtag.netweight),0) ";
		$pagwt = "";
		$panwt = "";
		$supagwt = "";
		$supanwt = "";
		$transfertagwhere = "";
		$startdate='';
		$enddate='';
		$datewh='';
		$filterwherecondtion='';
		$whereString='';
		$grouporder='';
		$join_stmt='';
		$join_stmt='';
		$salesidstr ='';
		$itemtagidstr='';
		$mode = "4";
		$whereclauses['counterid'] = $counterid;
		$startdate = date('Y-m-d', strtotime($lastprintdatetime));
		$printyesno ='0';
		if($storagemanagementdate < $startdate || $lastprintdatetime == '0000-00-00 00:00:00'){
			$startdate = $storagemanagementdate;
			if($lastprintdatetime != '0000-00-00 00:00:00'){
				$printyesno ='1';
			}
		}
		$enddate = $storagemanagementdate;
		$mainquery = $this->Basefunctions->stockreportquerygeneratefunction($startdate,$enddate,$datewh,$filterwherecondtion,$whereString,$grouporder,$join_stmt,$transfertagwhere,$partialtagselectgwt,$partialtagselectnwt,$pagwt,$panwt,$supagwt,$supanwt,$salesidstr,$itemtagidstr,$mode,$whereclauses);
		$data = $this->db->query($mainquery);
		foreach ($data->result() as $jvalue) {
			if($jvalue->counterid == $counterid)
			{
				$counterarray = array('openingwt'=>$jvalue->openinggrossweight,'inwt'=>$jvalue->ingrossweight,'outwt'=>$jvalue->outgrossweight,'closewt'=>$jvalue->closinggrossweight,'openingpcs'=>$jvalue->openingpieces,'inpcs'=>$jvalue->inpieces,'outpcs'=>$jvalue->outpieces,'closepcs'=>$jvalue->closingpieces,'boxweight'=>$boxweight,'lastprintdatetime'=>$lastprintdatetime,'description'=>$comment,'print'=>$printyesno,'lasttagtemplateid'=>$lasttagtemplateid,'lastprinttemplatesid'=>$lastprinttemplatesid,'openingtagweight'=>$jvalue->openingtagweight,'intagwt'=>$jvalue->intagwt,'outtagwt'=>$jvalue->outtagwt,'closingtagweight'=>$jvalue->closingtagweight,'lasterrrorwt'=>$jvalue->lasterrrorwt,'lasterrorpieces'=>$jvalue->lasterrorpieces);
			}
		}
		echo json_encode($counterarray);
	}
	// get summary data based on counter
	public function addstoragemanagement(){
		$counter = $_POST['counter'];
		if(isset($_POST['tagtemplateid'])) {
			$_POST['tagtemplateid'] = $_POST['tagtemplateid'];
		}else {
			$_POST['tagtemplateid'] = 1;
		}
		if(isset($_POST['printtemplateid'])) {
			$_POST['printtemplateid'] = $_POST['printtemplateid'];
		}else {
			$_POST['printtemplateid'] = 1;
		}
		$weightround =  $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2);
		$closingjewelsweight = number_format((float)($_POST['closingweight'] - $_POST['counterweight'] - $_POST['closingtagweight']- $_POST['lasterrrorwt']), $weightround, '.', '');
		$insertarray = array(
						'storagemanagementdate'=>$this->Basefunctions->ymd_format($_POST['storagemanagementdate']),
						'session'=>$_POST['session'],
						'sessionid'=>$_POST['sessionid'],
						'employeeid'=>$_POST['salespersonid'],
						'mode'=>$_POST['mode'],
						'counterid'=>$_POST['counter'],
						'counterweight'=>$_POST['counterweight'],
						'description'=>$_POST['description'],
						'openingweight'=>$_POST['openingweight'],
						'openingpieces'=>$_POST['openingpieces'],
						'inweight'=>$_POST['inweight'],
						'inpieces'=>$_POST['inpieces'],
						'outweight'=>$_POST['outweight'],
						'outpieces'=>$_POST['outpieces'],
						'closingweight'=>$_POST['closingweight'],
						'closingpieces'=>$_POST['closingpieces'],
						'manualclosingweight'=>$_POST['manualclosingweight'],
						'manualclosingpieces'=>$_POST['manualclosingpieces'],
						'closingjewelsweight'=>$closingjewelsweight,
						'errorweight'=>$_POST['errorweight'],
						'errorpieces'=>$_POST['errorpieces'],
						'tagtemplateid'=>$_POST['tagtemplateid'],
						'printtemplatesid'=>$_POST['printtemplateid'],
						'lastprintdatetime'=>$_POST['lastprintdatetime'],
						'industryid'=>$this->Basefunctions->industryid
					   );
		$insertarray = array_merge($insertarray,$this->Crudmodel->defaultvalueget());
		$this->db->insert('storagemanagement',array_filter($insertarray));
		$primaryid = $this->db->insert_id();
		// counter - update last print date & time
		if($_POST['mode'] == 'One' && $_POST['counter'] != '1') {
			$errorwt = number_format((float)($_POST['lasterrrorwt'] + $_POST['errorweight']), $weightround, '.', ''); 
			$errorpieces = number_format((int)($_POST['lasterrorpieces'] + $_POST['errorpieces']), 0, '.', ''); 
			$lastprint = array('lastprintdatetime'=>date($this->Basefunctions->datef),'lasterrrorwt'=>$errorwt,'lasterrorpieces'=>$errorpieces,'lasttagtemplateid'=>$_POST['tagtemplateid'],'lastprinttemplatesid'=>$_POST['printtemplateid'],'comment'=>$_POST['description'],'boxweight'=>$_POST['counterweight']);
			$lastprint = array_merge($lastprint,$this->Crudmodel->updatedefaultvalueget()); 
			$this->db->where('counterid',$_POST['counter']);
			$this->db->update('counter',$lastprint);
		}
		if(isset($_POST['tagtemplateid']) && $_POST['tagtemplateid'] != '' && $_POST['tagtemplateid'] != '1') {
			$print_data=array('templateid'=> $_POST['tagtemplateid'], 'Templatetype'=>'Tagtemp','primaryset'=>'storagemanagement.storagemanagementid','primaryid'=>$primaryid);
			$data = $this->Printtemplatesmodel->generatlabelprint($print_data);
			$array = array(
				'status'=>'SUCCESS',
			);
		} else if(isset($_POST['printtemplateid']) && $_POST['printtemplateid'] != '' && $_POST['printtemplateid'] != '1') {
			$array = array(
				'storagemgmtid'=>$primaryid,
				'status'=>'SUCCESS',
				'templateid'=>$_POST['printtemplateid'],
				'salesnumber'=>'STMGMT-'.$primaryid.'-'.$_POST['printtemplateid']
			);
		} else {
			$array = array(
				'status'=>'SUCCESS',
			);
		}
		echo json_encode($array);
	}
	/*	structure counter_groupdropdown_notdefault */
	public function counter_groupdropdown_notdefault() {
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('countername,counterid,boxweight,comment,lastprintdatetime');
		$this->db->from('counter');
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->where('storagetypeid',2);
		/* $this->db->where('counterdefault',0);
		$this->db->where('counterdefaultid',0); */
		$this->db->where('status',$this->Basefunctions->activestatus);
		$this->db->order_by('counterid','asc');
		$info = $this->db->get();
		return $info;
	}
	// load structure counter
	public function loadproductcounter(){
		$industryid = $this->Basefunctions->industryid;
		$this->db->select('countername,counterid,boxweight,comment,lastprintdatetime');
		$this->db->from('counter');
		$this->db->where("FIND_IN_SET('".$industryid."',industryid) >", 0);
		$this->db->where('storagetypeid',2);
		$this->db->where('counterdefault',0);
		$this->db->where('counterdefaultid',0);
		$this->db->where('status',$this->Basefunctions->activestatus);
		$this->db->order_by('counterid','asc');
		$counterdata = $this->db->get();
		if($counterdata->num_rows()>0) {
			foreach($counterdata->result() as $info) {
				$counterarray[] = array(
									'counterid'=>$info->counterid,	
									'countername'=>$info->countername,	
									'boxweight'=>$info->boxweight,	
									'comment'=>$info->comment,	
									'lastprintdatetime'=>$info->lastprintdatetime
								  );
			}
		}else {
				$counterarray = '';
		}
		echo json_encode($counterarray);
	}
	//size master inner grid work
    public function viewdynamicdatainfofetch($tablename,$sortcol,$sortord,$pagenum,$rowscount,$primaryid,$storagemanagementdate,$lastprintdatetime) {
    	$weightround =  $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-o
		$dataset ='counter.counterid,counter.countername,employee.employeename';
    	$join =' LEFT JOIN counter ON counter.counterid=storagemanagement.counterid';
    	$join .=' LEFT JOIN employee ON employee.employeeid=storagemanagement.employeeid';
    	$status=$tablename.'.status='.$this->Basefunctions->activestatus;
    	$extracond = '';
    	/* pagination */
    	$pageno = $pagenum-1;
    	$start = $pageno * $rowscount;
		$startdate = date('Y-m-d', strtotime($lastprintdatetime));
		$storagemanagementdate = date('Y-m-d', strtotime($storagemanagementdate));
		$print ='0';
		if($storagemanagementdate < $startdate || $lastprintdatetime == '0000-00-00 00:00:00'){
			$startdate = $storagemanagementdate;
		}
		$startdate = date('Y-m-d', strtotime($startdate));
		$enddate = date('Y-m-d', strtotime($storagemanagementdate));
    	/* query */
    	$result = $this->db->query('select sales.billdate as datetime,salestransactiontype.salestransactiontypename as transactiontypeid,salesnumber as salesnumber,stocktype.stocktypename as stocktypeid,"" as tagtypeid,"" as tagentrytypeid,purity.purityname as purityid,purity.purityname as purityid,product.productname as productid,round(salesdetail.grossweight,'.$weightround.') as grossweight,round(salesdetail.stoneweight,'.$weightround.') as stoneweight,round(salesdetail.netweight,'.$weightround.') as netweight,round(salesdetail.pieces,0) as pieces,round(itemtag.tagweight,'.$weightround.') as tagwt,employeename as employeeid from sales join salesdetail on salesdetail.salesid = sales.salesid join salestransactiontype on salestransactiontype.salestransactiontypeid = sales.salestransactiontypeid join stocktype on stocktype.stocktypeid = salesdetail.stocktypeid join purity on purity.purityid = salesdetail.purityid join product on product.productid = salesdetail.productid join itemtag on itemtag.itemtagid = salesdetail.itemtagid join employee on employee.employeeid = sales.employeeid where sales.salestransactiontypeid not in (16) and sales.status not in (0,3) and salesdetail.status not in (0,3) and sales.salesdate >= "'.$startdate.'" and sales.salesdate <= "'.$enddate.'" and (salesdetail.counterid ='.$primaryid.' or salesdetail.processcounterid ='.$primaryid.')  group by salesdetail.salesdetailid
		union all 
		select itemtag.createdate as datetime,"" as transactiontypeid,itemtagnumber as salesnumber,"" as stocktypeid,tagtypename as tagtypeid,tagentrytypename as tagentrytypeid,purity.purityname as purityid,purity.purityname as purityid,product.productname as productid,round(itemtag.grossweight,'.$weightround.') as grossweight ,round(itemtag.stoneweight,'.$weightround.') as stoneweight,round(itemtag.netweight,'.$weightround.') as netweight,round(itemtag.pieces,0) as pieces,itemtag.tagweight as tagwt,employeename as employeeid from itemtag  join tagtype on tagtype.tagtypeid = itemtag.tagtypeid join tagentrytype on tagentrytype.tagentrytypeid = tagentrytype.tagentrytypeid join purity on purity.purityid = itemtag.purityid join product on product.productid = itemtag.productid join employee on employee.employeeid = itemtag.createuserid where itemtag.status not in (0,3) and itemtag.tagdate >= "'.$startdate.'" and itemtag.tagdate <= "'.$enddate.'" and (itemtag.counterid ='.$primaryid.' or itemtag.fromcounterid ='.$primaryid.' or itemtag.stockincounterid ='.$primaryid.') group by itemtag.itemtagid');
    	$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
    	$data=array();
    	$i=1;
    	foreach($result->result() as $row) {
    		$data[$i]=array('id'=>$i,$row->datetime,$row->transactiontypeid,$row->salesnumber,$row->stocktypeid,$row->tagtypeid,$row->tagentrytypeid,$row->purityid,$row->productid,$row->grossweight,$row->stoneweight,$row->netweight,$row->pieces,$row->tagwt,$row->employeeid);
    		$i++;
    	}
    	$finalresult=array('0'=>$data,'1'=>$page,'2'=>'json');
    	return $finalresult;
    }
}