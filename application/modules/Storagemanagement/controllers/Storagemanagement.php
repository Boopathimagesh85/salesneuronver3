<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Storagemanagement extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('formbuild');
		$this->load->model('Sales/Salesmodel');
		$this->load->model('Storagemanagement/Storagemanagementmodel');
	}
    //first basic hitting view
    public function index()
    {
		$moduleid = array(59);
		sessionchecker($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['actionassign'] = $this->Basefunctions->actionassign($moduleid);
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['weight_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',2); //weight round-off
		$data['employeeid'] = $this->Basefunctions->userid;
		$data['employeedata'] = $this->Basefunctions->simpledropdownwithcond('employeeid','employeename,employeenumber','employee','employeeid',$this->Basefunctions->userid);
		$data['counter']=$this->Storagemanagementmodel->counter_groupdropdown_notdefault();
		$data['tagtemplate'] = $this->Basefunctions->simplegroupdropdown_var('tagtemplateid','tagtemplatename','tagtemplate','moduleid',$moduleid);
		$data['printtemplate'] = $this->Basefunctions->simplegroupdropdown_var('printtemplatesid','printtemplatesname','printtemplates','moduleid',$moduleid);
		$data['boxwttemplate'] = $this->Basefunctions->get_company_settings('boxwttemplate');
		$this->load->view('Storagemanagement/storagemanagementview',$data);
    }
	public function serialnumbergenerate(){
		$serialnumber = $this->Basefunctions->varrandomnumbergenerator(59,'sessionid',1,''); echo $serialnumber;
	}
	// get summary data based on counter
	public function loadsummary(){
		$this->Storagemanagementmodel->loadsummary();
	}
	// get summary data based on counter
	public function addstoragemanagement(){
		$this->Storagemanagementmodel->addstoragemanagement();
	}
	// load structure counter
	public function loadproductcounter(){
		$this->Storagemanagementmodel->loadproductcounter();
	}
	/* Fetch inner grid header,data,footer information */
	public function gridinformationfetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'counter.counterid') : 'counter.counterid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array(
		'colid'=>array('1','1','1','1','1','1','1','1','1','1','1','1','1','1'),
		'colname'=>array('Datetime','Transaction Type','Transaction No','Stock Type','Tag Type','From Type','Purity','Product','Grosswt','Stone Wt','Net Wt','Pieces','Tag wt','Employee'),'colicon'=>array('','','','','','','','','','','','','',''),
		'colsize'=>array('200','125','125','125','125','125','125','125','100','100','100','100','100','100'),
		'colmodelname'=>array('datetime','transactiontypeid','salesnumber','stocktypeid','tagtypeid','tagentrytypeid','purityid','productid','grossweight','stoneweight','netweight','pieces','tagwt','employeeid'),
		'colmodelindex'=>array('datetime','transactiontypeid','salesnumber','stocktypeid','tagtypeid','tagentrytypeid','purityid','productid','grossweight','stoneweight','netweight','pieces','tagwt','employeeid'),
		'coltablename'=>array('counter','sales','sales','stocktype','tagtype','tagentrytype','purity','product','salesdetail','salesdetail','salesdetail','salesdetail','itemtag','salesdetail'),
		'uitype'=>array('2','2','2','2','2','2','2','2','2','2','2','2','2','2'),
		'modname'=>array('datetime','transactiontypeid','salesnumber','stocktypeid','tagtypeid','tagentrytype','purityid','productid','grossweight','stoneweight','netweight','pieces','tagwt','employeeid'));
		$result=$this->Storagemanagementmodel->viewdynamicdatainfofetch($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$primaryid,$_GET['storagemanagementdate'],$_GET['lastprintdatetime']);
		$device = $this->Basefunctions->deviceinfo();
		/* if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'sizemasterinner',$width,$height);
		} else { */
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'sizemasterinner',$width,$height);
		//}
		echo json_encode($datas);
	}
}