<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Controller Class
class Smsmaster extends MX_Controller{
	//To load the Register View
	function __construct() {
    	parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Smsmaster/Smsmastermodel');
	}
	public function index() {
		$moduleid = array(208);
		sessionchecker($moduleid);
		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(208);
		$viewmoduleid = array(208);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Smsmaster/smsmasterview',$data);
	}
	//Create New data
	public function newdatacreate() {
    	$this->Smsmastermodel->newdatacreatemodel();
    }
	//information fetch
	public function fetchformdataeditdetails() {
		$moduleid = 208;
		$this->Smsmastermodel->informationfetchmodel($moduleid);
	}
	//Update old data
    public function datainformationupdate() {
        $this->Smsmastermodel->datainformationupdatemodel();
    }
	//delete old informtion
    public function deleteinformationdata() {
		$moduleid = 208;
        $this->Smsmastermodel->deleteoldinformation($moduleid);
    } 
	//drop down value set with multiple condition
	public function fetchmaildddatawithmultiplecond() {
		$this->Smsmastermodel->fetchmaildddatawithmultiplecondmodel();
	}
	//Folder insertion
	public function smsmasterfolderinsert() {
		$this->Smsmastermodel->smsmasterfolderinsertmodel();
	}
	//Folder Grid View
	public function smstemplatefoldernamegridfetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'foldername.foldernameid') : 'foldername.foldernameid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
		$colinfo = array('colmodelname'=>array('foldernamename','description','setdefault'),'colmodelindex'=>array('foldername.foldernamename','foldername.description','foldername.setdefault'),'coltablename'=>array('foldername','foldername','foldername'),'uitype'=>array('2','2','2'),'colname'=>array('Folder Name','Description','Default'),'colsize'=>array('300','300','200'));
		$result=$this->Smsmastermodel->smstemplatefoldervaluegetgridxmlview($primarytable,$sortcol,$sortord,$pagenum,$rowscount);
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$finalresult=array($result,$page,'');
		$device = $this->Basefunctions->deviceinfo();
	//	if($device=='phone') {
		//	$datas = mobilegirdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Folder List',$width,$height);
		//} else {
			$datas = girdviewdatagenerate($finalresult,$colinfo,$primarytable,$primaryid,$modid,'Folder List',$width,$height);
		//}
		echo json_encode($datas);
	}
	//Folder Data Fetch
	public function smstemplatefolderdatafetch() {
		$this->Smsmastermodel->smstemplatefolderdatafetchmodel();
	}
	//Folder Update
	public function smsmplatefolderupdate() {
		$this->Smsmastermodel->smsmplatefolderupdatemodel();
	}
	//Folder Delete
	public function folderdeleteoperation() {
		$this->Smsmastermodel->folderdeleteoperationmodel();
	}
	//Folder drop down Fetch Data
	public function fetchdddataviewddval() {
		$this->Smsmastermodel->fetchdddataviewddvalmodel();
	}
	//Related module get
	public function relatedmoduleidget() {
		//$this->Smsmastermodel->relatedmoduleidgetmodel();
		$this->Basefunctions->relatedmodulelistfetchforprint();
	}
	//senderid dd value fetch based on the settings name
	public function smsssenderddreload() {
		$this->Smsmastermodel->smsssenderddreloadmodel();
	}
	//editor value fetch
	public function editervaluefetch() {
		$filename = $_GET['filename'];
		$newfilename = explode('/',$filename);
		if(read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]))	{
			$tccontent = read_file('termscondition'.DIRECTORY_SEPARATOR.$newfilename[1]);
			echo json_encode($tccontent);
		} else {
			$tccontent = "Fail";
			echo json_encode($tccontent);
		}
	}
	public function modulefieldload() {
		$viewfieldsmoduleids =array_filter(explode(',',$_POST['moduleid']));
		$viewfieldsreletedmoduleids =array_filter(explode(',',$_POST['relatedmoduleid']));
		$all_moduleid=array_merge($viewfieldsmoduleids,$viewfieldsreletedmoduleids);
		$data = $this->Smsmastermodel->modulefieldload($all_moduleid);
		echo $data;
	}
	//notification type get
	public function notificationtypeget() {
		$this->Smsmastermodel->notificationtypegetmodel();
	}
	//unique name restriction
	public function folderuniquenamecheck(){
		$this->Smsmastermodel->folderuniquenamecheckmodel();
	}
	//Template name unique value
	public function templatesnameunique(){
		$this->Smsmastermodel->templatesnameuniquemodel();
	}
}