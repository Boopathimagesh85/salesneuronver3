<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Model File
class smsmastermodel extends CI_Model{
    public function __construct() {
		parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//To Create New data
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$restricttable = explode(',',$_POST['resctable']);
		$result = $this->Crudmodel->datainsertwithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$restricttable);
		if(isset($_POST['smssendtypeid'])) {
			$smssendtype = $_POST['smssendtypeid'];
		} else {
			$smssendtype = 1;
		}
		/* if($smssendtype == '2') {
			$username = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$this->Basefunctions->userid);
			$companyname = $this->getcompanyname();
			{//mail sent to arvind@salesneuron.com
				$datacontent = "<p>Dear Sir/Madam,</p>
					<p>This is automatic mail sent from&nbsp;<a href='http://salesneuron.com/' target='_blank'>salesneuron.com</a>&nbsp;regarding SMS Template request for one of the customers.</p>
					<p>Below is the Sender id based SMS Template information:</p>";
				$datacontent .= "<p>Username : ".$username." </p> </p><p>Company Name : ".$companyname." </p><p> Template Name : ".$_POST['templatesname']." </p><p>Sender ID : ".$_POST['senderid']." </p><p>Template Content : ".$_POST['leadtemplatecontent_editorfilename']." </p> </p>
					<p>Please check the details and approve the sender id based template at the earliest. Reply to this mail with your status update.</p>
					<p>Looking forward for approval at the earliest. Please call on +91 72000 70822 for any verification/doubts clarifications.</p><p>Regards,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
					<p><strong>Customer Success Team</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p><p>    Sales Neuron    <strong>AUC VENTURES PVT LTD</strong></p><p>No:6, Palani Andavar Koil Street,Villivakkam,Chennai-600049.&nbsp;   </p><p>Landline : 044-42639131&nbsp;| &nbsp;Mobile : +91 7200070822|&nbsp;<a href='http://www.aucventures.com/' target='_blank'>www.salesneuron.com</a></p>";
				$tomailid = array(array('email'=>'gowtham@aucventures.com','type' => 'to'));
				$message = array(
						'html' => $datacontent,
						'text' => '',
						'subject' => 'Reg SMS Template Approval',
						'from_email' => 'arvind@salesneuron.com',
						'from_name' => 'Arvind',
						'to' => $tomailid,
						'track_opens' => true,
						'track_clicks' => true,
						'inline_css' => true,
						'url_strip_qs' => true,
						'important' => true,
						'auto_text' => true,
						'auto_html' => true,
					);
				$send_at ='';
				$this->mailsentfunction($message,$send_at);
			}
		} */
		echo 'TRUE';
	}
	//mail notification sent
	public function mailsentfunction($message,$send_at) {
		try {
			$mandrill = new Mandrill('X9WnMXpxhC8YEOUPuQ3oZw');
			$async = true; //for bulk message
			$ip_pool = '';
			$result = $mandrill->messages->send($message, $async, $ip_pool, $send_at);
		} catch(Mandrill_Error $e) {
			// Mandrill errors are thrown as exceptions
			echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			throw $e;
		}
	}
	//retrieve getcompanyname
	public function getcompanyname() {
		$userid=$this->Basefunctions->userid;
		$data=$this->db->select('company.companyname')
		->from('company')
		->join('branch','branch.companyid=company.companyid')
		->join('employee','employee.branchid=branch.branchid')
		->where('employee.employeeid',$userid)
		->limit(1)
		->get();
		foreach($data->result() as $info) {
			$companyname = $info->companyname;
		}
		return $companyname;
	}
	//information fetch
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementpartable']);
		$restricttable = explode(',',$_GET['resctable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['dataprimaryid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetchwithrestrict($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$restricttable,$moduleid);
		echo $result;
	}
	//update data information
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['elementsname']);
		$formfieldstable = explode(',',$_POST['elementstable']);
		$formfieldscolmname = explode(',',$_POST['elementscolmn']);
		$elementpartable = explode(',',$_POST['elementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['primarydataid'];
		$restricttable = explode(',',$_POST['resctable']);
		$result = $this->Crudmodel->dataupdatewithrestrict($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid,$restricttable);
		if(isset($_POST['smssendtypeid'])) {
			$smssendtype = $_POST['smssendtypeid'];
		} else {
			$smssendtype = 1;
		}
		if($smssendtype == '2') {
			$username = $this->Basefunctions->generalinformaion('employee','employeename','employeeid',$this->Basefunctions->userid);
			$companyname = $this->getcompanyname();
			{//mail sent to arvind@salesneuron.com
				$datacontent = "<p>Dear Sir/Madam,</p>
					<p>This is automatic mail sent from&nbsp;<a href='http://salesneuron.com/' target='_blank'>salesneuron.com</a>&nbsp;regarding SMS Template request for one of the customers.</p>
					<p>Below is the Sender id based SMS Template information:</p>";
				$datacontent .= "<p>Username : ".$username." </p> </p><p>Company Name : ".$companyname." </p><p> Template Name : ".$_POST['templatesname']." </p><p>Sender ID : ".$_POST['senderid']." </p><p>Template Content : ".$_POST['leadtemplatecontent_editorfilename']." </p>
					<p>Please check the details and approve the sender id based template at the earliest. Reply to this mail with your status update.</p>
					<p>Looking forward for approval at the earliest. Please call on +91 72000 70826 for any verification/doubts clarifications.</p><p>Regards,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
					<p><strong>Customer Success Team</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p><p>    Sales Neuron    <strong>AUC VENTURES PVT LTD</strong></p><p>No:6, Palani Andavar Koil Street,Villivakkam,Chennai-600049.&nbsp;   </p><p>Landline : 044-42639131&nbsp;| &nbsp;Mobile : +91 7200070822|&nbsp;<a href='http://www.aucventures.com/' target='_blank'>www.salesneuron.com</a></p>";
				$tomailid = array(array('email'=>'templates@solutioninfini.com','type' => 'to'),array('email'=>'arvind@aucventures.com','type' => 'cc'));
				$message = array(
						'html' => $datacontent,
						'text' => '',
						'subject' => 'Reg SMS Template Approval',
						'from_email' => 'arvind@salesneuron.com',
						'from_name' => 'Arvind',
						'to' => $tomailid,
						'track_opens' => true,
						'track_clicks' => true,
						'inline_css' => true,
						'url_strip_qs' => true,
						'important' => true,
						'auto_text' => true,
						'auto_html' => true,
					);
				$send_at ='';
				$this->mailsentfunction($message,$send_at);
			}
		}
		echo $result;
	}
	//delete old information
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				echo "TRUE";
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			echo "TRUE";
		}
	}
	//dropdown value set with multiple condition
	public function fetchmaildddatawithmultiplecondmodel() {
		$i=0;
		$dname=$_GET['dataname'];
		$did=$_GET['dataid'];
		$dataattrname1=$_GET['othername1'];
		$dataattrname2=$_GET['othername2'];
		$table=$_GET['datatab'];
		$whfield=$_GET['whdatafield'];
		$whdata=$_GET['whval'];
		$mulcond=explode(",",$whfield);
		$mulcondval=explode(",",$whdata);
		$i=0;
		$this->db->select("$dname,$did,$dataattrname1,$dataattrname2");
		$this->db->from($table);
		foreach($mulcond AS $condname) {
			if($mulcondval[$i] != "0" && $mulcondval[$i] != "") {
				$this->db->where($condname,$mulcondval[$i]);
			}
			$i++;
		}
		$this->db->where('status',1);
		$result = $this->db->get();
		if($result->num_rows() >0) {	
			foreach($result->result()as $row) {
				$data[$i]=array('datasid'=>$row->$did,'dataname'=>$row->$dname,'otherdataattrname1'=>$row->$dataattrname1,'otherdataattrname2'=>$row->$dataattrname2);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//Folder Insertion
	public function smsmasterfolderinsertmodel() {
		$foldername = $_POST['foldername'];
		$description = $_POST['description'];
		$default = $_POST['setdefault'];
		if($default == ''){
			$default = 'No';
		}
		$date = date($this->Basefunctions->datef);
		$folderarray = array(
						'foldernamename'=>$foldername,
						'description'=>$description,
						'moduleid'=>'208',
						'setdefault'=>$default,
						'userroleid'=>$this->Basefunctions->userroleid,
						'createdate'=>$date,
						'lastupdatedate'=>$date,
						'createuserid'=>$this->Basefunctions->userid,
						'lastupdateuserid'=>$this->Basefunctions->userid,
						'status'=>$this->Basefunctions->activestatus
					);
		$this->db->insert('foldername',$folderarray);
		$primaryid = $this->db->insert_id();
		if($default == 'Yes') {
			$defultset = array(
				'setdefault'=>'No',
				'lastupdatedate'=>date($this->Basefunctions->datef),
				'lastupdateuserid'=>$this->Basefunctions->userid,
				'status'=>$this->Basefunctions->activestatus
			);
			$this->db->where_not_in('foldername.foldernameid',array($primaryid));
			$this->db->where_in('foldername.moduleid',array(208));
			$this->db->where_in('foldername.status',array(1));
			$this->db->update('foldername',$defultset);
		}
		echo "TRUE";
	}
	//Folder main grid view
	public function smstemplatefoldervaluegetgridxmlview($tablename,$sortcol,$sortord,$pagenum,$rowscount) {
		$dataset = 'foldername.foldernamename,foldername.foldernameid,foldername.description,foldername.setdefault';
		$join ='';
		$status = $tablename.'.status IN (1,2)';
		$where = $tablename.'.moduleid IN (208)';
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		//query
		$data = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$where.' AND '. $status.' ORDER BY'.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		return $data;
	}
	//Folder Edit Data Fetch
	public function smstemplatefolderdatafetchmodel() {
		$folderid = $_GET['datarowid'];
		$this->db->select('SQL_CALC_FOUND_ROWS  foldername.foldernamename,foldername.foldernameid,foldername.description,foldername.setdefault',false);
		$this->db->from('foldername');
		$this->db->where('foldername.foldernameid',$folderid);
		$this->db->where('foldername.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data=array('id'=>$row->foldernameid,'foldername'=>$row->foldernamename,'description'=>$row->description,'setdefault'=>$row->setdefault);
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//Folder Update
	public function smsmplatefolderupdatemodel() {
		$foldernameid = $_POST['foldernameid'];
		$foldername = $_POST['foldername'];
		$description = $_POST['description'];
		$default = $_POST['setdefault'];
		$date = date($this->Basefunctions->datef);
		$folderarray = array(
						'foldernamename'=>$foldername,
						'description'=>$description,
						'setdefault'=>$default,
						'userroleid'=>$this->Basefunctions->userroleid,
						'createdate'=>$date,
						'lastupdatedate'=>$date,
						'createuserid'=>$this->Basefunctions->userid,
						'lastupdateuserid'=>$this->Basefunctions->userid,
						'status'=>$this->Basefunctions->activestatus
					);
		$this->db->where('foldername.foldernameid',$foldernameid);
		$this->db->update('foldername',$folderarray);
		if($default == 'Yes') {
			$defultset = array(
				'setdefault'=>'No',
				'lastupdatedate'=>date($this->Basefunctions->datef),
				'lastupdateuserid'=>$this->Basefunctions->userid,
				'status'=>$this->Basefunctions->activestatus
			);
			$this->db->where_not_in('foldername.foldernameid',array($foldernameid));
			$this->db->where_in('foldername.moduleid',array(208));
			$this->db->where_in('foldername.status',array(1));
			$this->db->update('foldername',$defultset);
		}
		echo "TRUE";
	}
	//Folder Delete
	public function folderdeleteoperationmodel() {
		$folderid = $_GET['id'];
		$sndefault = $this->Basefunctions->singlefieldfetch('sndefault','foldernameid','foldername',$folderid); //default record
		if($sndefault != '2') {
			$date = date($this->Basefunctions->datef);
			$folderarray = array(
							'lastupdatedate'=>$date,
							'lastupdateuserid'=>$this->Basefunctions->userid,
							'status'=>0
						);
			$this->db->where('foldername.foldernameid',$folderid);
			$this->db->update('foldername',$folderarray);
			echo "TRUE";
		} else {
			echo "DEFAULT";
		}
	}
	//Folder Drop Down Reload function
	public function fetchdddataviewddvalmodel() {
		$i = 0;
		$fieldname = $_GET['dataname'];
		$fieldid = $_GET['dataid'];
		$tablename = $_GET['datatab'];
		$moduleid = $_GET['moduleid'];
		$this->db->select("$fieldid,$fieldname,setdefault");
		$this->db->from("$tablename");
		$this->db->where_in("$tablename".'.moduleid',array($moduleid));
		$this->db->where("$tablename".'.status',1);
		$result=$this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i]=array('datasid'=>$row->$fieldid,'dataname'=>$row->$fieldname,'setdefault'=>$row->setdefault);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		} 
	}
	//Related Modules id get
	public function relatedmoduleidgetmodel() {
		$moddatas = array();
		$relmodids = array();
		$moduleid = $_POST['moduleid'];
		$genmod = array(2,3,265);
		$relmoduleids = $this->db->select('modulerelationid,relatedmoduleid')->from('modulerelation')->where('moduleid',$moduleid)->get();
		foreach($relmoduleids->result() as $reldata) {
			$relmodids[] = $reldata->relatedmoduleid;
		}
		$i=0;
		$modids = implode(',',$relmodids);
		$moduleids = ( ($modids!="")? $modids.','.$moduleid : $moduleid );
		$modids = explode(',',$moduleids);
		$allmodid = array_merge($modids,$genmod);
		$allmoduleids = implode(',',array_unique($allmodid));
		$modinfo = $this->db->query('select module.moduleid,module.modulename from module join moduleinfo ON moduleinfo.moduleid=module.moduleid where module.moduleid IN ('.$allmoduleids.') AND module.status=1 AND moduleinfo.status=1 group by moduleinfo.moduleid',false);
		foreach($modinfo->result() as $moddata) {
			$modtype = ( ($moddata->moduleid==$moduleid)?'':((in_array($moddata->moduleid,$genmod))?'GEN:':'REL:') );
			$moddatas[$i] = array('datasid'=>$moddata->moduleid,'dataname'=>$moddata->modulename,'datatype'=>$modtype);
			$i++;
		}
		$datas = ( ($i==0)? json_encode(array('fail'=>'FAILED')) : json_encode($moddatas) );
		echo $datas;
	}
	//senderid dd value fetch based on the settings name
	public function smsssenderddreloadmodel() {
		$i=0;
		$industryid = $this->Basefunctions->industryid;
		$smssendtype=$_GET['smssendtype'];
		$this->db->select('smssettings.smssettingsid,smssettings.senderid,smssettings.apikey');
		$this->db->from('smssettings');
		$this->db->join('smsprovidersettings','smsprovidersettings.smsprovidersettingsid=smssettings.smsprovidersettingsid');
		$this->db->join('smssendtype','smssendtype.smssendtypeid=smsprovidersettings.smssendtypeid');
		$this->db->where('smsprovidersettings.smssendtypeid',$smssendtype);
		$this->db->where("FIND_IN_SET('$industryid',smssettings.industryid) >", 0);
		$this->db->where('smssettings.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->smssettingsid,'dataname'=>$row->senderid,'apikey'=>$row->apikey);
				$i++;
			}
			echo json_encode($data);
		} else {
		   echo json_encode(array("fail"=>'FAILED'));
		}
	}
	//merge module based form fields load
	public function modulefieldload($moduleid) {
		$printfield=[];
		$printfield1=[];
		$result_array=[];
		$this->db->select('viewcreationmoduleid,viewcreationcolmodelname,viewcreationcolmodelaliasname,viewcreationcolmodelindexname,viewcreationcolumnname,fieldprintname,viewcreationcolumnid');
		$this->db->from('viewcreationcolumns');
		$this->db->where_in('viewcreationcolumns.viewcreationmoduleid',$moduleid);
		$this->db->where('viewcreationcolumns.status',$this->Basefunctions->activestatus);
		$this->db->order_by('viewcreationcolumns.viewcreationcolumnid','asc');
		$data = $this->db->get()->result();
		$optgroupid=1;
		foreach($data as $info)
		{
			$printfield[] = array(
					'pname'=>'Field Column',
					'optgroupid'=>1,
					'pid'=>$info->viewcreationcolumnid,
					'id'=>$info->viewcreationmoduleid,
					'label'=>$info->viewcreationcolumnname,
					'key'=>'{'.$info->fieldprintname.'}',
			);
		}
		return json_encode($printfield);
	}
	// notification type get
	public function notificationtypegetmodel() {
		$i=0;
		$mid = $_POST['moduleid'];
		$this->db->select('notificationlogtypeid,notificationlogtypename');
		$this->db->from('notificationlogtype');
		$this->db->where("FIND_IN_SET('$mid',notificationlogtype.moduleid) >", 0);
		$this->db->where('notificationlogtype.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0 ) {
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->notificationlogtypeid,'dataname'=>$row->notificationlogtypename);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array('fail'=>'FAILED'));
		}
	}
	//unique name check
	public function folderuniquenamecheckmodel(){
		$industryid = $this->Basefunctions->industryid;
		$primaryid=$_POST['primaryid'];
		$accname=$_POST['accname'];
		$moduleid = 208;
		$elementpartable = explode(',',$_POST['elementspartabname']);
		$partable =  $this->Basefunctions->filtervalue($elementpartable);
		$ptid = $partable.'id';
		$ptname = $partable.'name';
		if($partable == 'currency'){
			$ptname = 'currencycode';
		}
		if($accname != "") {
			$result = $this->db->select($ptname.','.$ptid)->from($partable)->where($partable.'.'.$ptname,$accname)->where($partable.'.status',1)->where("FIND_IN_SET('".$industryid."',industryid) >", 0)->where("FIND_IN_SET('".$moduleid."',moduleid) >", 0)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->$ptid;
				}
			} else { echo "False"; }
		}else { echo "False"; }
	}
	// Template Unique name
	public function templatesnameuniquemodel(){
		$industryid = $this->Basefunctions->industryid;
		$templatetypeid = $_POST['templatetypeid'];
		$templatesname = $_POST['templatesname'];
		if($templatesname != "") {
			$result = $this->db->select('templates.templatesid')->from('templates')->where('templates.templatetypeid',$templatetypeid)->where('templates.templatesname',$templatesname)->where('templates.status',1)->where("FIND_IN_SET('$industryid',templates.industryid) >", 0)->get();
			if($result->num_rows() > 0) {
				foreach($result->result()as $row) {
					echo $row->templatesid;
				}
			} else {
				echo "False";
			}
		} else {
			echo "False";
		}
	}
}