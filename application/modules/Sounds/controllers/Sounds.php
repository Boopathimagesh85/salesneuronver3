<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sounds extends MX_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Sounds/Soundsmodel');
	  }
    //first basic hitting view
    public function index() {
    	$moduleid = array(17);
		sessionchecker($moduleid);
		//action
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(17);
		$viewmoduleid = array(17);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
      	$this->load->view('Sounds/soundsview',$data);
	}
	//sound insertion
	public function soundsfileuploadindialstreet() {
		$this->Soundsmodel->soundsfileuploadindialstreethmodel();
	}
	//sound delete
	public function soundsdelete() {
		$this->Soundsmodel->soundsdeletemodel();
	}
	//sms settings name dd reload
	public function callsettingsddload() {
		$this->Soundsmodel->callsettingsddloadmodel();
	}
}