<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Soundsmodel extends CI_Model{
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//sms settings credential add
	public function soundsfileuploadindialstreethmodel() {
		//master company id get
		$mastercompanyid = $this->Basefunctions->generalinformaion('company','mastercompanyid','companyid',2);
		$soundsname = $_POST['soundsname'];
		$filename = $_POST['filename'];
		$filesize = $_POST['filesize'];
		$filetype = $_POST['filetype'];
		$filepath = $_POST['filepath'];
		$basepath = $_POST['basepath'];
		$filefrom = $_POST['filefrom'];
		$callsettingsid = $_POST['callsettingsid'];
		$apikey = $_POST['apikey'];
		if($filefrom == 2){
			$file_name_with_full_path = $basepath.$filepath;
		} else {
			$file_name_with_full_path = $filepath;
		}
		$target_url = "http://api-voice.solutionsinfini.com/v1/?api_key=".$apikey."&method=voice.upload&output=xml&title=".$filename."&soundfile=".$file_name_with_full_path;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$target_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);
		curl_close ($ch);
		$dataset = $this->parse_response($response);
		for($i=0;$i<count($dataset);$i++) {
			$date= date($this->Basefunctions->datef);
			$userid= $this->Basefunctions->userid;
			$activestatus = $this->Basefunctions->activestatus;
			$sounddetails=array(
				'soundsname'=>$soundsname,
				'soundfileid'=>$dataset[$i]['id'],
				'filename'=>$filename,
				'filename_size'=>$filesize,
				'filename_type'=>$filetype,
				'filename_path'=>$filepath,
				'industryid'=>$this->Basefunctions->industryid,
				'branchid'=>$this->Basefunctions->branchid,
				'createdate'=>$date,
				'lastupdatedate'=>$date,
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
				'status'=>$activestatus
			);
			$this->db->insert('sounds',$sounddetails);
		}
		echo 'TRUE';
	}
	public function parse_response($response) {
		$xml = new SimpleXMLElement($response);
		$result = array();
		$h=0;
		foreach ($xml->data as $element) {
		     foreach($element as $key => $val) {
			 	$result[$h][(string)$key] = (string)$val;
		     }
		     $h++;
		}
		return $result;
	}
	//sound delete
	public function soundsdeletemodel() {
		$id = $_POST['primaryid'];
		$cdate = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$delete = array(
			'lastupdatedate'=>$cdate,
			'lastupdateuserid'=>$userid,
			'status'=>0
		);
		$this->db->where('sounds.soundsid',$id);
		$this->db->update('sounds',$delete);
		echo "TRUE";
	}
	//sms settings drop down reload
	public function callsettingsddloadmodel() {
		$i=0;
		$this->db->select('username,callsettingsid,apikey');
		$this->db->from('callsettings');
		$this->db->where('callsettings.callservicetypeid',3);
		$this->db->where('callsettings.callservicemodeid',3);
		$this->db->where('callsettings.status',1);
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[$i] = array('datasid'=>$row->callsettingsid,'dataname'=>$row->username,'apikey'=>$row->apikey);
				$i++;
			}
			echo json_encode($data);
		} else {
			echo json_encode(array("fail"=>'FAILED'));
		}
	}
}