<?php $device = $this->Basefunctions->deviceinfo(); ?>
<style type="text/css">
	#rateinnergrid .gridcontent{
		height: 75vh !important;
	}
</style>
<div id="rateview" class="gridviewdivforsh">
	<?php
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['gridid'] = 'rategrid';
		$dataset['gridwidth'] = 'rategridwidth';
		$dataset['gridfooter'] = 'rategridfooter';
		$dataset['viewtype'] = 'disable';
		$dataset['moduleid'] = '43';
		$this->load->view('Base/singlemainviewformwithgrid.php',$dataset);
		$defaultrecordview = $this->Basefunctions->get_company_settings('mainviewdefaultview');
		echo hidden('mainviewdefaultview',$defaultrecordview);
	?>
</div>
</div>
</div>
</div>
<div id="rateformadd" class="hidedisplay" style="">
<input id="rate_round" name="rate_round" value="<?php echo $rate_round; ?>" type="hidden">
	<div class="large-12 columns paddingzero formheader">
		<?php
		$device = $this->Basefunctions->deviceinfo();
			if($device=='phone') {
				echo '<div class="large-12 columns headercaptionstyle headerradius addformreloadtablet paddingzero">
					<div class="large-6 medium-6 small-6 columns headercaptionleft">
						<span class="gridcaptionpos"><span>'.$gridtitle['title'].'</span></span>
					</div>
					<div class="large-5 medium-6 small-6 columns addformheadericonstyle addformaction righttext">
					<span class="icon-box" id="addsectionclose" ><i class="material-icons" title="Close">close</i></span>
					</div>';
			echo '</div>';
		} else {
			echo '<div class="large-12 columns headercaptionstyle headerradius addformreloadtablet paddingzero">
					<div class="large-6 medium-6 small-6 columns headercaptionleft">
						<span class="gridcaptionpos"><span>'.$gridtitle['title'].'</span></span>
					</div>
					<div class="large-6 medium-6 small-6 columns addformheadericonstyle addformaction righttext">
						<span id="addrate" class="addbtnclass icon-box"><input id="" name="" tabindex="186" value="Save" class="alertbtnyes  ffield" type="button"></span>
						<span id="addsectionclose" class="icon-box"><input id="" name="" tabindex="186" value="Close" class="alertbtnno  ffield" type="button"></span>
					</div>';
			echo '</div>';
		}
		?>
		<div class='large-12 columns addformunderheader'>&nbsp;</div>
		<!-- tab group creation -->
		<?php  if($device!='phone') {?>
			<div class="large-12 columns tabgroupstyle desktoptabgroup">
				<ul class="tabs" data-tab="">
					<li id="tab1" class="tab-title sidebaricons active ftabnew" data-subform="1">
						<span class="waves-effect waves-ripple waves-light">Rate</span>
					</li>
				</ul>
			</div>
		<?php } else {?>
			<div class="large-12 columns tabgroupstyle mobiletabgroup">
				<ul class="tabs" data-tab="">
					<li id="tab1"  class="tab-title sidebaricons active ftabnew" data-subform="1">
						<span  class="waves-effect waves-ripple waves-light">Rate</span>
					</li>
				</ul>
			</div>
		<?php }?>
	</div>
	
	<div class="large-12 columns addformunderheader">&nbsp; </div>
		<div class="large-12 columns addformcontainer padding-space-open-for-form" style="">
			<div class="row mblhidedisplay">&nbsp;</div>
			<div id="subformspan1" class="hiddensubform"> 
			<?php
					if($device=='phone') { ?>
					<div class="large-4 columns paddingbtm" style="padding: 0px !important;">	
						<div class="large-12 columns cleardataform" style="padding:0px !important;background: #f2f3fa !important;box-shadow: none;">
							<div class="large-12 columns headerformcaptionstyle" style="background: #f2f3fa !important;" >Rate Filter</div>
					<?php } else { ?>
					<div class="large-4 columns paddingbtm" style="padding-right:1.8rem !important;padding-bottom:1rem !important;">	
						<div class="large-12 columns cleardataform borderstyle" style="padding-left: 0 !important; padding-right: 0 !important;">
							<div class="large-12 columns headerformcaptionstyle" >Rate Filter</div>
					<?php } ?>
							<div class="static-field large-12 columns">
								<label>Branch</label>						
								<select id="branchid" name="branchid" class="chzn-select" data-prompt-position="bottomLeft:14,36" tabindex="101">
									<option value=""></option>
									 <?php $prev = ' ';
										foreach($branch as $key):
									?> 
									<option value="<?php echo $key->branchid;?>"><?php echo $key->branchname;?></option>
									<?php  endforeach;?>						
								</select>
							</div>
							<div class="static-field large-12 columns ">
								<label>Metal</label>
								<select id="metalid" name="metalid" class="chzn-select" data-prompt-position="topLeft:14,36" tabindex="102">
								<option value=""></option> 
								<?php foreach($metal as $key):?>
									<option data-name="<?php echo $key->metalname;?>" value="<?php echo $key->metalid;?>">
										<?php echo $key->metalname;?></option>
								<?php endforeach;?>	
								</select>
							</div>
						     <div class="static-field large-12 columns ">
								<label>Purity</label>						
								<select id="purityid" name="purityid" class="chzn-select dropdownchange" data-prompt-position="topLeft:14,36" tabindex="103">
								<option value=""></option> 
								<?php $prev = ' ';
								foreach($purity->result() as $key):
								$cur = $key->metalid;
								$a="";
								if($prev != $cur )
								{
									if($prev != " ")
									{
										 echo '</optgroup>';
									}
									echo '<optgroup  label="'.$key->metalname.'">';
									$prev = $key->metalid;
								}
								?>
								<option value="<?php echo $key->purityid;?>"><?php echo $key->purityname;?></option>
								<?php endforeach;?>
								</select>
							</div>
							<input id="ratearray" name="ratearray" value="<?php echo $ratearray; ?>" tabindex="" type="hidden">
							<div class="divider large-12 columns"></div>
								<?php
					if($device=='phone') { ?>
							<div class="large-12 columns sectionalertbuttonarea" style="text-align:center;">			
					<?php } else { ?>
<div class="large-12 columns sectionalertbuttonarea" style="text-align:right">			
					<?php } ?>					
								<input id="resetfilter" type="button" class="alertbtnyes" name="" value="Reset" tabindex="" style="margin-right: 7px;">
							</div>  
						</div>
					</div>
					<div class="large-8 columns viewgridcolorstyle borderstyle" style="padding-left:0;padding-right:0;top:20px;">	
						<?php
							$device = $this->Basefunctions->deviceinfo();
							if($device=='phone') {
								echo '<div class="large-12 columns paddingzero forgetinggridname" id="rateinnergridwidth"><div class="inner-row-content inner-gridcontent" id="rateinnergrid" style="height:74vh !important; top:0px;">
									<!-- Table header content , data rows & pagination for mobile-->
								</div>
								<footer class="inner-gridfooter footercontainer" id="rateinnergridfooter">
									<!-- Footer & Pagination content -->
								</footer></div>';
							} else {
								echo '<div class="large-12 columns forgetinggridname" id="rateinnergridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="rateinnergrid" style="max-width:2000px; height:75vh !important; top:0px;">
								<!-- Table content[Header & Record Rows] for desktop-->
								</div>
								<div class="inner-gridfooter footer-content footercontainer" id="rateinnergridfooter">
									<!-- Footer & Pagination content -->
								</div></div>';
							}
						?>	
				    </div>
			</div>
		</div>
	</div>
</div>