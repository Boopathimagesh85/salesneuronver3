<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('Base/headerfiles'); ?>
	<?php $this->load->view('Base/overlay'); ?>
	<?php  $this->load->view('Base/basedeleteform');
	$this->load->view('Base/modulelist'); ?>
	<style>
        input[type=text].rowtext{height: 1.2rem; margin: 0;color: #222;}
            
            input:not([type]):focus:not([readonly]), input[type=text]:focus:not([readonly]), input[type=password]:focus:not([readonly]), input[type=email]:focus:not([readonly]), input[type=url]:focus:not([readonly]), input[type=time]:focus:not([readonly]), input[type=date]:focus:not([readonly]), input[type=datetime]:focus:not([readonly]), input[type=datetime-local]:focus:not([readonly]), input[type=tel]:focus:not([readonly]), input[type=number]:focus:not([readonly]), input[type=search]:focus:not([readonly]), textarea.materialize-textarea:focus:not([readonly])
            {
                color:#000000 !important;
            }
            }
    </style>
</head>
<body>
	<div class="row" style="max-width:100%">
		<div class="">
			<div class="inner-wrap">
				<!-- Add Form -->
				<?php $this->load->view('utilityform'); ?>
			</div>
		</div>
	</div>
</body>
<div class="large-12 columns" style="position:absolute;"><div class="overlay" id="viewdeleteoverlayconform" style="z-index:120;"></div></div>
<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
<?php $this->load->view('Base/bottomscript'); ?>
<?php include 'js/Utility/utility.php' ?>
</html>
