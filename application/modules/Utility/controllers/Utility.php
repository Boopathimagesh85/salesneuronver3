<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Utility extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('formbuild');
		$this->load->model('Utility/Utilitymodel');		
    }
    //first basic hitting view
    public function index()
    {
		$moduleid = array(43);
		sessionchecker($moduleid);
		$data['filedmodids'] = $moduleid;
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['actionassign'] = $this->Basefunctions->actionassign($moduleid);
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['amount_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //rate round-off
		$data['purity']=$this->Basefunctions->purity_groupdropdown();
		$data['metal']=$this->Basefunctions->simpledropdown('metal','metalid,metalname','metalname');
		$data['branch'] = $this->Basefunctions->simpledropdown('branch','branchid,branchname','branchid');
		$data['branchid'] =$this->Basefunctions->branchid;
		$data['ratearray'] = $this->Basefunctions->get_company_settings('purityrate'); // purity for rate based on metal
		$data['rate_round'] = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5);
		$this->load->view('Utility/utilityview',$data);
    }
	/**	* create a new rate entry	*/
	public function addrate()
	{
		$this->Utilitymodel->addrate();
	}
	public function sales_addrate()
	{
		$this->Utilitymodel->sales_addrate();
	}
	public function ratedelete()
	{
		$this->Utilitymodel->ratedelete(); //
	}
	/* Fetch Main grid header,data,footer information */
	public function gridinformationfetch() {
		$primarytable = $_GET['maintabinfo'];
		$primaryid = $_GET['primaryid'];
		$pagenum = $_GET['page'];
		$rowscount = $_GET['records'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		$modid = $_GET['moduleid'];
		$branchid = $_GET['branchid'];
		$metalid = $_GET['metalid'];
		$purityid = $_GET['purityid'];
	
		$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'purity.purityid') : 'purity.purityid';
		$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
	
		$colinfo = array('colid'=>array('1','1','1','1','1','1','1','1'),'colname'=>array('Branch','Metal','Purity','Branch Id','Metalid','Primary ID','Purity ID','Rate'),'colicon'=>array('','','','','','','',''),'colsize'=>array('200','200','200','200','200','200','200','200'),'colmodelname'=>array('innerbranchname','innermetalname','innerpurityname','innerbranchid','innermetalid','innerprimaryid','innerpurityid','innercurrentrate'),'colmodelindex'=>array('branch.branchname','metal.metalname','purity.purityname','purity.branchid','purity.metalid','purity.primaryid','purity.purityid','rate.currentrate'),'coltablename'=>array('branch','purity','metal','purity','purity','metal','purity','purity','rate'),'uitype'=>array('2','2','2','2','2','2','2','2'),'modname'=>array('Branch','Metal','Purity','Branch ID','Metal ID','Primary ID','Purity ID','Rate'));
	
		$result=$this->Utilitymodel->viewdynamicdatainfofetch($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$branchid,$metalid,$purityid);
		$device = $this->Basefunctions->deviceinfo();
		/* if($device=='phone') {
			$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'rateinner',$width,$height);
		} else { */
			$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'rateinner',$width,$height,'TEXT');
		//}
		echo json_encode($datas);
	}
	// Retrieve Rate Calc Details
	public function retrieveratecalculationdetails() {
		$this->Utilitymodel->retrieveratecalculationdetails();
	}
}