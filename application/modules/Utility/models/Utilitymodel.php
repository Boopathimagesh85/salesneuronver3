<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Utilitymodel extends CI_Model
{
	private $ratemodule = 43;
    public function __construct()
    {
        parent::__construct();
		$this->load->model( 'Base/Basefunctions');
		$this->load->model( 'Base/Crudmodel');
    }
	/**	*create a new rate entry	*/
	public function addrate()
	{
		$displaydate = date("Y-m-d");
		$griddata=$_POST['rateaddongriddata'];
		$formdata=json_decode( $griddata, true );
		$count = count($formdata);
		for( $i=0; $i <$count ; $i++ ) {
			$branchid = $formdata[$i]['branchid'];
			$metalid = $formdata[$i]['metalid'];
			$purityid = $formdata[$i]['purityid'];
			$rate = $formdata[$i]['rate'];
			$rateround=$this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5);
			$rate = round($rate,$rateround);
			//combination verifiy
			$check = $this->ratecombinationcheck($branchid,$metalid,$purityid,$displaydate,$rate);
			if($check == 'TRUE') {// already exist
				$update = array('currentrate'=>$rate);
				$this->db->where_in('purityid',$purityid);
				$this->db->where('metalid',$metalid);
				$this->db->where('displaydate',$displaydate);
				$this->db->update('rate',$update);
			} else {// not exist  
				// check already rate combinations
				$this->db->where('metalid',$metalid);
				$this->db->where_in('purityid',$purityid);
				$this->db->from('rate');
				$result =   $this->db->get();
				if($result->num_rows() > 0){
					$update = array('currentstatus'=>0);
					$this->db->where_in('purityid',$purityid);
					$this->db->where('metalid',$metalid);
					$this->db->update('rate',$update);
				}
				$insert=array(
					'metalid' => $metalid,
					'purityid' => $purityid,
					'currentrate' =>$rate,
					'displaydate' => $displaydate,
					'industryid'=>$this->Basefunctions->industryid,
					'branchid'=>$branchid,
					'currentstatus' => $this->Basefunctions->activestatus,
					'status' => $this->Basefunctions->activestatus
				);
				$insert = array_merge($insert,$this->Crudmodel->defaultvalueget());
				$this->db->insert('rate',array_filter($insert));
				$primaryid = $this->db->insert_id();
				//audit-log
				$user = $this->Basefunctions->username;
				$userid = $this->Basefunctions->logemployeeid;
				$activity = ''.$user.' created rate ';
				$this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,$this->ratemodule);
			}
		}
		echo 'SUCCESS';
	}
	public function ratecombinationcheck($branchid,$metalid,$purityid,$displaydate,$rate){
		$data ='FALSE';
		$this->db->select('rateid,currentrate');
		$this->db->from('rate');
		$this->db->where('rate.metalid',$metalid);
		$this->db->where('rate.purityid',$purityid);
		$this->db->where('rate.displaydate',$displaydate);
		$this->db->where('rate.currentrate',$rate);
		$this->db->where('rate.status','1');
		$result = $this->db->get();
		if($result->num_rows() > 0) {
			$data = 'TRUE';
		}
		return $data;
	}
	public function sales_addrate() // item rate added in transaction
	{
		$displaydate = date("Y-m-d");
		$purity=explode(',',$_POST['purityrate']);
		$purityid=count($purity);
		for($i = 0; $i < $purityid; $i++){
			$insert=array(
					'metalid' => $_POST['metal'],
					'purityid' => $purity[$i],
					'currentrate' => $_POST['itemrate'],
					'displaydate' => $displaydate,
					'industryid'=>$this->Basefunctions->industryid,
					'status' => $this->Basefunctions->activestatus
			);
			$insert = array_merge($insert,$this->Crudmodel->defaultvalueget());
			$this->db->insert('rate',array_filter($insert));
		}
		echo 'SUCCESS';
	}
	public function ratedelete()
	{
		$id=$_GET['primaryid'];
		//inactivate lot
		$inactive = $this->Basefunctions->delete_log();
		$this->db->where('rateid',$id);
		$this->db->update('rate',$inactive);
		//audit-log
		$rate = $this->Basefunctions->singlefieldfetch('currentrate','rateid','rate',$id);
		$user = $this->Basefunctions->username;
		$userid = $this->Basefunctions->logemployeeid;
		$activity = ''.$user.' deleted rate - '.$rate.'';
		$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,$this->ratemodule);
		echo 'SUCCESS';
	}
	public function viewdynamicdatainfofetch($tablename,$sortcol,$sortord,$pagenum,$rowscount,$branchid,$metalid,$purityid) {
		$dataset ='purity.branchid,branch.branchname,metal.metalname,purity.purityname,purity.metalid,purity.purityid,purity.primaryid';
		$join =' LEFT JOIN branch ON branch.branchid=purity.branchid';
		$join .=' LEFT JOIN metal ON metal.metalid=purity.metalid';
		$status=$tablename.'.status='.$this->Basefunctions->activestatus.'';
		$extracond = '';
		if($branchid != '') {
			$extracond .= ' AND purity.branchid='.$branchid.'';
		}
		if($metalid != '') {
			$extracond .= ' AND purity.metalid='.$metalid.'';
		}
		if($purityid != '') {
			$extracond .= ' AND purity.purityid='.$purityid.'';
		}
		/* pagination */
		$pageno = $pagenum-1;
		$start = $pageno * $rowscount;
		/* query */
		$result = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '. $status.$extracond.' ORDER BY '.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
		$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
		$data=array();
		$i=1;
		foreach($result->result() as $row) {
			$branchid = $row->branchid;
			$metalid = $row->metalid;
			$purityid = $row->purityid;
			$primaryid = $row->primaryid;
			$ratedetail = $this->getratedetails($metalid,$purityid);
			$rateid =$i;
			$branchname = $row->branchname;
			$metalname =$row->metalname;
			$purityname =$row->purityname;
			$currentrate =$ratedetail['currentrate'];
			$data[$i]=array('id'=>$rateid,$branchname,$metalname,$purityname,$branchid,$metalid,$primaryid,$purityid,$currentrate);
			$i++;
		}
		$finalresult=array('0'=>$data,'1'=>$page,'2'=>'json');
		return $finalresult;
	}
	public function getratedetails($metalid,$purityid){
		$rateround = $this->Basefunctions->singlefieldfetch('roundtypevalue','roundtypeid','roundtype',5); //Rate round-off
		$data = array('rateid'=>'','currentrate'=>'0');
		$this->db->select('rateid,ROUND(currentrate,'.$rateround.') as currentrate');
		$this->db->from('rate');
		$this->db->where('rate.metalid',$metalid);
		$this->db->where('rate.purityid',$purityid);
		$this->db->where('rate.status','1');
		$result = $this->db->get();
		$i=0;
		foreach($result->result() as $row) {
			$data['currentrate'] = $row->currentrate;
		}
		return $data;
	}
	public function retrieveratecalculationdetails() {
		$purityid = $_GET['purityid'];
		$this->db->select('ratecalcdetails.nonprimarypurityid,ratecalcdetails.ratecalculationid,ratecalcdetails.ratepercentvalue,ratecalcdetails.ratecalcdetailsid',false);
		$this->db->from('ratecalcdetails');
		$this->db->where('ratecalcdetails.primarypurityid',$purityid);
		$this->db->where('ratecalcdetails.status',$this->Basefunctions->activestatus);
		$info=$this->db->get();
		$i=0;
		if($info->num_rows()>0) {
			foreach($info->result() as $value) {
				$infotag[$i] = array('nonprimarypurityid'=>$value->nonprimarypurityid,'ratecalculationid'=>$value->ratecalculationid,'ratepercentvalue'=>$value->ratepercentvalue,'ratecalcdetailsid'=>$value->ratecalcdetailsid);
				$i++;
			}
		}
		$datas = (($i==0)? json_encode(array('fail'=>'FAILED')) : json_encode($infotag));
		echo $datas;
	}
}