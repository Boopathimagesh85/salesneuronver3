<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Chargescategory extends MX_Controller{
    public function __construct() {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Chargescategory/Chargescategorymodel');	
    }
    //first basic hitting view
    public function index() {        
    	$moduleid = array(222);
    	sessionchecker($moduleid);
		/*action*/
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp'] = $this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp'] = $this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		//view
		$viewfieldsmoduleids = array(222);
		$viewmoduleid = array(222);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
      	$this->load->view('Chargescategory/chargescategoryview',$data);
	}
	//create itemcounter
	public function newdatacreate() {  
    	$this->Chargescategorymodel->newdatacreatemodel();
    }
	//information fetchitemcounter
	public function fetchformdataeditdetails() {
		$moduleid = array(222);
		$this->Chargescategorymodel->informationfetchmodel($moduleid);
	}
	//update itemcounter
    public function datainformationupdate() {
        $this->Chargescategorymodel->datainformationupdatemodel();
    }
	//delete itemcounter
    public function deleteinformationdata() {
    	$moduleid = array(222);
		$this->Chargescategorymodel->deleteoldinformation($moduleid);
    }
	//unique name check
	public function namecheckfunction() {
		$this->Chargescategorymodel->namecheckfunctionmodel();
	}
}