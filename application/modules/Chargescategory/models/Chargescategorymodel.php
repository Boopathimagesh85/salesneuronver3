<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Chargescategorymodel extends CI_Model{
    public function __construct() {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
    }
	//Additional charge create
	public function newdatacreatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['chargescategoryelementsname']);
		$formfieldstable = explode(',',$_POST['chargescategoryelementstable']);
		$formfieldscolmname = explode(',',$_POST['chargescategoryelementscolmn']);
		$elementpartable = explode(',',$_POST['chargescategoryelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
		
		$userroleid = $this->Basefunctions->userroleid;
		$primaryid = $this->db->insert_id(); //last inserted id
		$this->db->query(" UPDATE ".$fildstable." SET moduleid =223 ,  sortorder = sortorder+".$primaryid.", userroleid = ".$userroleid." WHERE additionalchargecategoryid = ".$primaryid." ");
		$setdefault = $_POST['setdefault'];
		$this->setdefaultupdate($setdefault,$primaryid);
		echo 'TRUE';
	}
	//Retrieve Additional charge data for edit
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['additionalchargeelementsname']);
		$formfieldstable = explode(',',$_GET['additionalchargeelementstable']);
		$formfieldscolmname = explode(',',$_GET['additionalchargeelementscolmn']);
		$elementpartable = explode(',',$_GET['additionalchargeelementpartable']);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['additionalchargeprimarydataid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	// charges category update
	public function datainformationupdatemodel() {
		//table and fields information
		$formfieldsname = explode(',',$_POST['chargescategoryelementsname']);
		$formfieldstable = explode(',',$_POST['chargescategoryelementstable']);
		$formfieldscolmname = explode(',',$_POST['chargescategoryelementscolmn']);
		$elementpartable = explode(',',$_POST['chargescategoryelementspartabname']);
		//filter unique parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		//filter unique fields table
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['chargescategoryprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		$setdefault = $_POST['setdefault'];
		$this->setdefaultupdate($setdefault,$primaryid);
		echo $result;
	}
	//charges category  delete
	public function deleteoldinformation($moduleid)	{
		$formfieldstable = explode(',',$_GET['additionalchargeelementstable']);
		$parenttable = explode(',',$_GET['additionalchargeelementspartable']);
		$id = $_GET['additionalchargeprimarydataid'];
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=array('additionalchargetype');
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				//delete other status
				$chargenamedelete = array('status'=>$this->Basefunctions->deletestatus,'lastupdateuserid'=>$this->Basefunctions->userid,'lastupdatedate'=>date($this->Basefunctions->datef));
				//retrieve charge territory
				$chargetypeids=array();
				$additionalchargetype=$this->db->query("SELECT additionalchargetypeid from additionalchargetype WHERE additionalchargecategoryid =$id AND status = 1");
				foreach($additionalchargetype->result() as $info){
					$chargetypeids[] = $info->additionalchargetypeid;
				}
				if(count($chargetypeids) > 0){
					$this->db->where_in('additionalchargetypeid',$chargetypeids);
					$this->db->update('chargeterritories',$chargenamedelete);
				}
				$this->db->where('additionalchargecategoryid',$id);
				$this->db->update('additionalchargetype',$chargenamedelete);
				echo "TRUE";
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			//retrieve charge territory
			$additionalchargetype=$this->db->query("SELECT additionalchargetypeid from additionalchargetype WHERE additionalchargecategoryid =$id AND status = 1");
			foreach($additionalchargetype->result() as $info){
				$chargetypeids[] = $info->additionalchargetypeid;
			}
			if(count($chargetypeids) > 0){
				$this->db->where_in('additionalchargetypeid',$chargetypeids);
				$this->db->update('chargeterritories',$chargenamedelete);
			}
			$this->db->where('additionalchargecategoryid',$id);
			$this->db->update('additionalchargetype',$chargenamedelete);
			echo "TRUE";
		}
	} 
	//unique name check
	public function namecheckfunctionmodel() {
		$name = $_POST['name'];
		$this->db->select('additionalchargecategoryname');
		$this->db->from('additionalchargecategory');
		$this->db->where_in('additionalchargecategory.additionalchargecategoryname',$name);
		$this->db->where('additionalchargecategory.status',1);
		$result  = $this->db->get();
		if($result->num_rows() > 0) {
			echo "TRUE";
		} else {
			echo "False";
		}
	}
	//set default update
	public function setdefaultupdate($setdefault,$chargeid) {
		if($setdefault != 'No') {
			$updatearray = array('setdefault'=>'No');
			$this->db->where_not_in('additionalchargecategory.additionalchargecategoryid',$chargeid);
			$this->db->update('additionalchargecategory',$updatearray);
		}
	}
}