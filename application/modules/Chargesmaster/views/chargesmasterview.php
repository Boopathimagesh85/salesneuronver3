<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
</head>
<style type="text/css">
	#chargeterritoriesadvanceddrop, #chargesnameadvanceddrop, #chargescategoryadvanceddrop{
		left: -82.1406px !important;
	}
	.large-12.columns.cleardataform.z-depth-5{
		border: 4px solid #ffffff;
	}
</style>
<body>
<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<?php
				$modid = '222,223,21';
				$dataset['gridtitle'] = $gridtitle['title'];
				$dataset['titleicon'] = $gridtitle['titleicon'];
				$dataset['formtype'] = 'multipleaddform';
				$dataset['multimoduleid'] = $modid;
				$this->load->view('Base/mainviewaddformheader',$dataset);
			?>
			<div class="large-12 columns scrollbarclass paddingzero chargesmastertouch">
				<div class="mastermodules">
				<?php
					
					//function call for dash board form fields generation
					dashboardformfieldsgeneratorwithgrid($moddbfrmfieldsgen,1,$modid);
				?>		
				</div>	
			</div>
		</div>
	</div>
</div>
<?php
	$device = $this->Basefunctions->deviceinfo();
	if($device=='phone') {
		$this->load->view('Base/overlaymobile');
	} else {
		$this->load->view('Base/overlay');
	}
	$this->load->view('Base/modulelist');
?>
</body>
<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
<?php $this->load->view('Base/bottomscript'); ?>
<script>
	//Tabgroup Dropdown More
	$("#tab1").click(function()
	{
		$('#chargescategorydataupdatesubbtn').hide();
		$('#chargescategoryreloadicon,#chargescategorysavebutton').show();
		resetFields();  mastertabid=1; masterfortouch = 0;
		$(".tabclass").addClass('hidedisplay');
		$("#tab1class").removeClass('hidedisplay');	
	});
	$("#tab2").click(function()
	{
		$('#chargesnamedataupdatesubbtn').hide();
		$('#chargesnamereloadicon,#chargesnamesavebutton').show();
		$(".tabclass").addClass('hidedisplay');
		$("#tab2class").removeClass('hidedisplay');
		dynamicloaddropdown('additionalchargecategoryid','additionalchargecategoryid','additionalchargecategoryname','additionalchargecategoryid','additionalchargecategory','','');
		setTimeout(function()
		{			
			chargesnameaddgrid();
		},10);		
		resetFields();
		mastertabid=2; 
		masterfortouch = 0;
	});
	$("#tab3").click(function()
	{
		$('#chargeterritoriesdataupdatesubbtn').hide();
		$('#chargeterritoriesreloadicon,#chargeterritoriessavebutton').show();
		$(".tabclass").addClass('hidedisplay');
		$("#tab3class").removeClass('hidedisplay');
		setTimeout(function()
		{			
			chargeterritoriesaddgrid();
		},10);
		resetFields();
		loadchargesname();
		mastertabid=2; 
		masterfortouch = 0;
	});
	$(document).ready(function(){
		{//enable support icon
			$(".supportoverlay").css("display","inline-block")
		}	
		{//for keyboard global variable
			 viewgridview = 0;
			 innergridview = 1;
		 }
	});	
</script>
</html>