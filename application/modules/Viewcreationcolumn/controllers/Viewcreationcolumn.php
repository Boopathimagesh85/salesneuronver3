<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Viewcreationcolumn extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Viewcreationcolumn/Viewcreationcolumnmodel');
		$this->load->model('Base/Basefunctions');
    }
	private $viewcreationcolumnid = 56;
    //first basic hitting view
    public function index()
    {	
		$moduleid=$this->viewcreationcolumnid;
		sessionchecker($moduleid);
		$userid = $this->Basefunctions->userid;
		//action		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['module']=$this->Basefunctions->simpledropdown('module','moduleid,modulename','moduleid');
		$data['uitype']=$this->Basefunctions->simpledropdown('uitype','uitypeid,uitypename,roundtypeid','uitypeid');
		$data['roundtype']=$this->Basefunctions->simpledropdown('roundtype','roundtypeid,roundtypename','roundtypeid');
		$data['prefix'] = $this->Basefunctions->singlefieldfetch('prefix','moduleid','varserialnumbermaster',55);
		$data['suffix'] = $this->Basefunctions->singlefieldfetch('suffix','moduleid','varserialnumbermaster',55);
		$viewfieldsmoduleids = array($moduleid);
		$viewmoduleid = array($moduleid);
		$data['moduleids'] = $viewmoduleid;
		$data['filedmodids'] = $viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid); 
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('viewcreationcolumnview',$data);
    }
    //load-columnname
    public function loadcolumnname(){
    	$viewfieldsmoduleids =array_filter(explode(',',$_POST['moduleid']));
    	$viewfieldsreletedmoduleids =array_filter(explode(',',$_POST['relatedmoduleid']));
    	$all_moduleid=array_merge($viewfieldsmoduleids,$viewfieldsreletedmoduleids);
    	$data=$this->Basefunctions->dataviewdropdowncolumns($all_moduleid);
    	echo json_encode($data);
    }
    public function viewcreationcolumncreate(){
    	$this->Viewcreationcolumnmodel->viewcreationcolumncreate();
    }
    public function retrieve(){
    	$this->Viewcreationcolumnmodel->retrieve();
    }
    public function update(){
    	$this->Viewcreationcolumnmodel->update();
    }
    public function delete(){
    	$this->Viewcreationcolumnmodel->delete();
    }
    public function retrievequery()
    {
    	$this->Viewcreationcolumnmodel->retrievequery();
    }
    public function loadautocompletedata()
    {
    	$this->Viewcreationcolumnmodel->loadautocompletedata();
    }
    public function loadtabsection()
    {
    	$data=$this->Viewcreationcolumnmodel->loadtabsection($_POST['moduleid']);
    	echo json_encode($data);
    }
    public function gridinformationfetch() {
    	$primarytable = $_GET['maintabinfo'];
    	$primaryid = $_GET['primaryid'];
    	$pagenum = $_GET['page'];
    	$rowscount = $_GET['records'];
    	$width = $_GET['width'];
    	$height = $_GET['height'];
    	$modid = $_GET['moduleid'];
    	$salesid=$_GET['filter'];
    
    	$sortcol = isset($_GET['sortcol']) ? ($_GET['sortcol']!='' ? $_GET['sortcol'] : 'viewcreationcolumns.viewcreationcolumnid') : 'viewcreationcolumns.viewcreationcolumnid';
    	$sortord = isset($_GET['sortord']) ? ($_GET['sortord']!='' ? $_GET['sortord'] : 'DESC') : 'DESC';
    
    	$colinfo = array('colmodelname'=>array('modulename','uitypename','moduletabsectionname','viewcreationcolmodelaliasname','viewcreationcolmodeltable','viewcreationcolumnname'),'colmodelindex'=>array('module.modulename','uitype.uitypename','moduletabsection.moduletabsectionname','viewcreationcolumns.viewcreationcolmodelaliasname','viewcreationcolumns.viewcreationcolmodeltable','viewcreationcolumns.viewcreationcolumnname'),'coltablename'=>array('module','uitype','moduletabsection','viewcreationcolumns','viewcreationcolumns','viewcreationcolumns'),'uitype'=>array('19','19','19','2','2','2'),'colname'=>array('Module','Uitype','Module Tab Section','Field','Table','Column Name'),'colsize'=>array('133','120','150','100','100','100'));
    
    	$result=$this->Viewcreationcolumnmodel->viewdynamicdatainfofetch($primarytable,$sortcol,$sortord,$pagenum,$rowscount,$salesid);
    	$device = $this->Basefunctions->deviceinfo();
    //	if($device=='phone') {
    	//	$datas = mobilegirdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Groups',$width,$height);
    	//} else {
    		$datas = girdviewdatagenerate($result,$colinfo,$primarytable,$primaryid,$modid,'Groups',$width,$height);
    	//}
    	echo json_encode($datas);
    }
    public function checkuniquename() {
    	$table = $_GET['table'];
    	$fieldname = $_GET['fieldname'];
    	$value = trim($_GET['value']);
    	$editid = trim($_GET['editid']);
    	$this->Viewcreationcolumnmodel->checkuniquename($table,$fieldname,$value,$editid);
    }
}