<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('Base/headerfiles'); ?>
	<?php $this->load->view('Base/basedeleteform'); ?>
</head>
<body>
	<?php
		$device = $this->Basefunctions->deviceinfo();
		$dataset['gridenable'] = "yes"; //no
		$dataset['griddisplayid'] = "mergetextcreationview";
		$dataset['gridtitle'] = $gridtitle['title'];
		$dataset['titleicon'] = $gridtitle['titleicon'];
		$dataset['spanattr'] = array();
		$dataset['gridtableid'] = "viewcreationcolumngrid";
		$dataset['moduleid'] = array(56);
		$dataset['griddivid'] = "viewcreationcolumngriddiv";
		$dataset['forminfo'] = array(array('id'=>'mergetextformadd','class'=>'hidedisplay','formname'=>'viewcreationcolumnform'));
		if($device=='phone') {
			$this->load->view('Base/gridmenuheadermobile',$dataset);
			$this->load->view('Base/overlaymobile');
			$this->load->view('Base/viewselectionoverlay');
		} else {
			$this->load->view('Base/gridmenuheader',$dataset);
			$this->load->view('Base/overlay');
		}
		$this->load->view('Base/basedeleteform');
		$this->load->view('Base/modulelist');
	?>
	
	<?php
		$this->load->view('Base/basedeleteformforcombainedmodules');
	?>	
	
	</body>
	<div class="large-12 columns" style="position:absolute;"><div class="overlay" id="viewdeleteoverlayconform" style="z-index:120;"></div></div>
	<?php $this->load->view('Base/bottomscript'); ?>
	<script src="<?php echo base_url();?>js/Base/views.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/mention/mention.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/mention/bootstrap-typeahead.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/Viewcreationcolumn/viewcreationcolumn.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/plugins/jqgrid/jquery.jqGrid.src.min.js" type="text/javascript"></script>
</html>
