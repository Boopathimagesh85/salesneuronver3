<?php
$device = $this->Basefunctions->deviceinfo();
if($device=='phone') {
	echo '<div class="large-12 columns headercaptionstyle headerradius paddingzero">
			<div class="large-6 medium-6 small-6 columns headercaptionleft">
				<span  data-activates="slide-out" class="headermainmenuicon button-collapse" title="Menu"><i class="material-icons">menu</i></span>
				<span class="gridcaptionpos"><span>'.$gridtitle.'</span></span>
			</div>';
			$this->load->view('Base/formheadericons');
	echo '</div>';
} else {
	echo '<div class="large-12 columns headercaptionstyle headerradius paddingzero">
			<div class="large-6 medium-6 small-6 columns headercaptionleft">
				<span  data-activates="slide-out" class="headermainmenuicon button-collapse" title="Menu"><i class="material-icons">menu</i></span>
				<span class="gridcaptionpos"><span>'.$gridtitle.'</span></span>
			</div>';
			$this->load->view('Base/formheadericons');
	echo '</div>';
}
?>

<!--Tab Group-->
<div class="large-12 columns tabgroupstyle show-for-large-up ">
	<ul class="tabs" data-tab="">
		<li class="tab-title sidebaricons ftab active" data-subform="1">
			<span><?php echo $gridtitle;  ?></span>
		</li>
	</ul>
</div>

<!--For Tablet and Mobile view Dropdown Script-->
<script>
	$(document).ready(function(){
		$("#tabgropdropdown").change(function(){ 
			var tabgpid = $("#tabgropdropdown").val(); 
			console.log($('.sidebaricons[data-subform="'+tabgpid+'"]').trigger('click'));
		});
	});
</script>
<div class="large-12 columns addformunderheader">&nbsp; </div>
<div class="large-12 columns addformcontainer">
	<div class="row">&nbsp;</div>
	<div id="subformspan1" class="hiddensubform"> 
		<form method="POST" name="viewcreationcolumnform" class="viewcreationcolumnform"  id="viewcreationcolumnform">
			<div id="viewcreationcolumnaddwizard" class="validationEngineContainer">				
			<div id="viewcreationcolumneditwizard" class="validationEngineContainer">
			<div class="large-4 columns paddingbtm">	
					<div class="large-12 columns cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;">
						<div class="large-12 columns headerformcaptionstyle" >ViewCreation Column Details</div>
						<div class="static-field large-12 columns">
								<label>Module<span class="mandatoryfildclass">*</span></label>
								<select class="chzn-select validate[required]" id="moduleid" name="moduleid" data-prompt-position="topLeft:14,36">
									<option value=""></option> 
									<?php foreach($module as $key):?>
									<option  value="<?php echo $key->moduleid;?>" ><?php echo $key->modulename;?></option>
									<?php endforeach;?>	
								</select>
						</div>
						<div class="static-field large-12 columns">
								<label>Uitype<span class="mandatoryfildclass">*</span></label>
								<select class="chzn-select validate[required]" id="uitypeid" name="uitypeid" data-prompt-position="topLeft:14,36">
									<option value=""></option> 
									<?php foreach($uitype as $key):?>
									<option  value="<?php echo $key->uitypeid;?>" data-roundtype="<?php echo $key->roundtypeid;?>"><?php echo $key->uitypename;?></option>
									<?php endforeach;?>	
								</select>
						</div>
						<div class="static-field large-12 columns">
								<label>Tab Section<span class="mandatoryfildclass">*</span></label>
								<select class="chzn-select validate[required]" data-prompt-position="" id="tabsectionid" name="tabsectionid">
									<option value=""></option>									
								</select>
						</div>
						<div class="input-field large-12 columns">
								<input id="fieldname" class="validate[maxSize[100],required]" type="text" data-prompt-position="bottomLeft" tabindex="" name="fieldname">
								<label for="fieldname">Field Name<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="input-field large-12 columns">
								<input id="tablename" class="validate[maxSize[100],required]" type="text" data-prompt-position="bottomLeft" tabindex="" name="tablename">
								<label for="tablename">Table Name<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="input-field large-12 columns">
								<input id="viewcreationcolumnname" class="validate[maxSize[100],required,funcCall[viewcheckuniquename]]" type="text" data-prompt-position="bottomLeft" tabindex="" name="viewcreationcolumnname" data-primaryid="" data-table="viewcreationcolumns" data-validatefield="viewcreationcolumnname">
								<label for="viewcreationcolumnname">Column Name<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="input-field large-12 columns">
								<input id="parenttable" class="validate[maxSize[100],required,]" type="text" data-prompt-position="bottomLeft" tabindex="" name="parenttable">
								<label for="parenttable">Parent Table<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="input-field large-12 columns">
								<input id="joincolumnname" class="validate[maxSize[100],required]" type="text" data-prompt-position="bottomLeft" tabindex="" name="joincolumnname">
								<label for="joincolumnname">Join Column Name<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="static-field large-6 columns">
									<label>Linkable</label>							
									<select data-validation-engine="" class="chzn-select" id="linkableoption" name="linkableoption" data-prompt-position="topLeft:14,36">
										<option value="">Select</option>
										<option value="No">No</option>
										<option value="Yes">Yes</option>
									</select>
								</div>
							<div class="static-field large-6 columns hidedisplay relatedmodulediv">
								<label>Related Module</label>
								<select class="chzn-select" data-prompt-position="" id="relatedmoduleid" name="relatedmoduleid">
									<option value=""></option>									
								</select>
								<input type="hidden" name="relatedmodule" id="relatedmodule">
						</div>
							<div class="static-field large-12 columns">
								<label>Round<span class="mandatoryfildclass">*</span></label>
								<select class="chzn-select validate[required]" id="roundtype" name="roundtype" data-prompt-position="topLeft:14,36">
									<option value=""></option> 
									<?php foreach($roundtype as $key):?>
									<option  value="<?php echo $key->roundtypeid;?>" ><?php echo $key->roundtypename;?></option>
									<?php endforeach;?>	
								</select>
						</div>
						 <input type="hidden" name="viewactiontype" id="viewactiontype" value="0" />
					<div class="large-12 columns "> &nbsp; </div>
					</div>
				</div>
				<div class="large-8 columns paddingbtm">
				    <div class="large-12 columns cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;">
						<div class="large-12 columns headerformcaptionstyle" >Query Details</div>
						 <div class="input-field large-12 columns">
								<textarea id="querybox" class="validate[maxSize[500],required] materialize-textarea" data-prompt-position="bottomLeft:14,36" tabindex="" name="querybox" style="height:200px;"></textarea>
								<label for="querybox"><span class="mandatoryfildclass">*</span>Query Box</label>
						</div>
						<div class="large-12 columns ">  
						<input type="button" class="btn leftformsbtn" id="whereclauseicon" name="" value="Where" tabindex="">
						&nbsp;
						<input type="button" class="btn leftformsbtn" id="whereclauseokicon" name="" value="OK" tabindex="">
						</div>
						<div class="input-field large-12 columns">
								<textarea id="sqlquery" class="validate[maxSize[500]] materialize-textarea" tabindex="" name="sqlquery" style="height:139px;" readonly></textarea>
								<label for="sqlquery">SQL Query</label>
						</div>
						    <input type="hidden" id="viewcreationcolumnformprimaryid" name="viewcreationcolumnformprimaryid"/>
						    <input type="hidden" id="viewcreationcolumnformprimaryname" name="viewcreationcolumnformprimaryname"/>
						    <input type="hidden" id="prefix" name="prefix" value="<?php echo $prefix; ?>"/>
						    <input type="hidden" id="suffix" name="suffix" value="<?php echo $suffix; ?>"/>
						    <div class="large-12 columns "> &nbsp; </div>
					</div>
				</div>		
			</div>	
			</div>	
		</form>
	</div>
</div>
<div class="large-12 columns" style="position:absolute;">
	<div class="overlay" id="whereclausesearchoverlay" style="overflow-y: scroll;overflow-x: hidden;z-index:40">		
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>		
		<div class="large-10 medium-10 large-centered medium-centered columns">
			<div class="large-10 medium-10 large-centered medium-centered columns">
				<div class="large-12 columns sectionheaderformcaptionstyle">
					Where Clause Search
				</div>
				<div class="large-5 medium-6 small-6 columns addformheadericonstyle" style="text-align:right;padding-right:0 !important">
					<span id="whereclausesearchclose" class="" title="Close"><i class="material-icons" title="Close">close</i> </span>
				</div>
			</div>
			<?php
					$device = $this->Basefunctions->deviceinfo();
					if($device=='phone') {
						echo '<div class="large-12 columns paddingzero forgetinggridname" id="whereclausegridwidth"><div class=" inner-row-content inner-gridcontent" id="whereclausegrid" style="height:420px;top:0px;">
							<!-- Table header content , data rows & pagination for mobile-->
						</div>
						<footer class="inner-gridfooter footercontainer" id="whereclausegridfooter">
							<!-- Footer & Pagination content -->
						</footer></div>';
					} else {
						echo '<div class="large-12 columns forgetinggridname" id="whereclausegridwidth" style="padding-left:0;padding-right:0;"><div class="desktop row-content inner-gridcontent" id="whereclausegrid" style="max-width:2000px; height:420px;top:0px;">
						<!-- Table content[Header & Record Rows] for desktop-->
						</div>
						<div class="inner-gridfooter footer-content footercontainer" id="whereclausegridfooter">
							<!-- Footer & Pagination content -->
						</div></div>';
					}
		?>
		</div>
	</div>
</div>
<style type="text/css">
#mainviewgriddivhid {
    top: -10px !important;
    position: relative;
}
</style>