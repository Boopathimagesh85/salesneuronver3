<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Viewcreationcolumnmodel extends CI_Model
{
   	private $viewcreationcolumnid = 56;	
	public function __construct()
    {
        parent::__construct();
		$this->load->model( 'Base/Basefunctions' );
		$this->load->model('Base/Crudmodel');
    }
    public function viewcreationcolumncreate(){
    
    	$fieldname=$_POST['fieldname'];
    	$tablename=$_POST['tablename'];
    	if(isset($_POST['relatedmoduleid']) && !empty($_POST['relatedmoduleid'])){
    		$relatedmoduleid = $_POST['relatedmoduleid'];
    	}else{
    		$relatedmoduleid = 0;
    	}
    	if (preg_match('/\bas\b/',$fieldname)){
    		$fieldsplit = preg_split('/\bas\b/', $fieldname);
    		$viewcreationcolmodelname=$fieldsplit[1];
    		$viewcreationcolmodelindexname= $fieldsplit[0];
    	}else{
    		$viewcreationcolmodelname=$fieldname;
    		$viewcreationcolmodelindexname=$fieldname;
    	}
    	if (preg_match('/\bas\b/',$tablename)){
    		$tablesplit = preg_split('/\bas\b/', $tablename);
    		$viewcreationcolmodeljointable=$tablesplit[1];
    	}else{
    		$viewcreationcolmodeljointable=$tablename;
    	}
    	if (preg_match('/\bselect\b/',$_POST['querybox']) || preg_match('/\bSELECT\b/',$_POST['querybox'])){
    	    $selecttype = 3;   // manual
    	}else{
    		$selecttype = 2;  // semi auto
    	}
		   	 $insert = array(
						'viewcreationmoduleid' => $_POST['moduleid'],
						'uitypeid' => $_POST['uitypeid'],
						'moduletabsectionid' => $_POST['tabsectionid'],
						'viewcreationcolmodelname' => $viewcreationcolmodelname,
						'viewcreationcolmodelaliasname' => $_POST['fieldname'],
		    			'viewcreationcolmodelindexname' => $viewcreationcolmodelindexname,
		    			'viewcreationcolmodeltable' => $_POST['tablename'],
		    			'viewcreationcolmodeljointable' => $viewcreationcolmodeljointable,
    					'roundtype' => $_POST['roundtype'],
    	 				'viewcreationcolumnname'=>$_POST['viewcreationcolumnname'],
		    	 		'linkable' => $_POST['linkableoption'],
			   	 		'viewcreationparenttable'=>$_POST['parenttable'],
			   	 		'viewcreationjoincolmodelname' => $_POST['joincolumnname'],
		    	 		'linkmoduleid' => $relatedmoduleid,
    	 				'selectquery' => $_POST['querybox'],
    	 		        'selecttype' => $selecttype
		    			);
    	$insert = array_merge($insert,$this->Crudmodel->defaultvalueget());
	    $this->db->insert('viewcreationcolumns',array_filter($insert));
	    $primaryid = $this->db->insert_id();
	    //audit-log
	    $user = $this->Basefunctions->username;
	    $userid = $this->Basefunctions->logemployeeid;
	    $activity = ''.$user.' created viewcreationcolumns - '.$_POST['viewcreationcolumnname'].'';
	    $this->Basefunctions->notificationcontentadd($primaryid,'Added',$activity ,$userid,$this->viewcreationcolumnid);
	    echo 'SUCCESS';
    }
    public function retrieve(){
    	$id=$_GET['primaryid'];
    	$this->db->select('viewcreationcolumns.viewcreationcolumnid,viewcreationcolumns.viewcreationcolumnname,viewcreationcolumns.viewcreationmoduleid,viewcreationcolumns.uitypeid,viewcreationcolumns.moduletabsectionid,viewcreationcolumns.viewcreationcolmodelaliasname,viewcreationcolumns.viewcreationcolmodeltable,viewcreationcolumns.roundtype,viewcreationcolumns.selectquery,viewcreationcolumns.linkable,viewcreationcolumns.linkmoduleid,viewcreationcolumns.viewcreationparenttable,viewcreationcolumns.viewcreationjoincolmodelname');
    	$this->db->from('viewcreationcolumns');
    	$this->db->where('viewcreationcolumnid',$id);
    	$info=$this->db->get()->row();
    	$jsonarray=array(
    			'viewcreationcolumnformprimaryid'=>$info->viewcreationcolumnid,
    			'viewcreationcolumnformprimaryname'=>$info->viewcreationcolumnname,
    			'viewcreationcolumnname'=>$info->viewcreationcolumnname,
    			'moduleid'=>$info->viewcreationmoduleid,
    			'uitypeid'=>$info->uitypeid,
    			'tabsectionid'=>$info->moduletabsectionid,
    			'fieldname'=>$info->viewcreationcolmodelaliasname,
    			'tablename'=>$info->viewcreationcolmodeltable,
    			'roundtype'=>$info->roundtype,
    			'querybox'=>$info->selectquery,
    			'linkableoption'=>$info->linkable,
    			'relatedmoduleid'=>$info->linkmoduleid,
    			'parenttable'=>$info->viewcreationparenttable,
    			'joincolumnname'=>$info->viewcreationjoincolmodelname,
    	);
    	echo json_encode($jsonarray);
    }
    public function update(){
    	$id=$_POST['primaryid'];
    	$fieldname=$_POST['fieldname'];
    	$tablename=$_POST['tablename'];
    	if(isset($_POST['relatedmoduleid']) && !empty($_POST['relatedmoduleid'])){
    		$relatedmoduleid = $_POST['relatedmoduleid'];
    	}else{
    		$relatedmoduleid = 0;
    	}
    	if (preg_match('/\bas\b/',$fieldname)){
    		$fieldsplit = preg_split('/\bas\b/', $fieldname);
    		$viewcreationcolmodelname=$fieldsplit[1];
    		$viewcreationcolmodelindexname= $fieldsplit[0];
    	}else{
    		$viewcreationcolmodelname=$fieldname;
    		$viewcreationcolmodelindexname=$fieldname;
    	}
    	if (preg_match('/\bas\b/',$tablename)){
    		$tablesplit = preg_split('/\bas\b/', $tablename);
    		$viewcreationcolmodeljointable=$tablesplit[1];
    	}else{
    		$viewcreationcolmodeljointable=$tablename;
    	}
    	if (preg_match('/\bselect\b/',$_POST['querybox']) || preg_match('/\bSELECT\b/',$_POST['querybox'])){
    		$selecttype = 3;   // manual
    	}else {
    		$selecttype = 2;  // semi auto
    	}
		$update = array(
    					'viewcreationmoduleid' => $_POST['moduleid'],
						'uitypeid' => $_POST['uitypeid'],
						'moduletabsectionid' => $_POST['tabsectionid'],
						'viewcreationcolmodelname' => $viewcreationcolmodelname,
						'viewcreationcolmodelaliasname' => $_POST['fieldname'],
		    			'viewcreationcolmodelindexname' => $viewcreationcolmodelindexname,
		    			'viewcreationcolmodeltable' => $_POST['tablename'],
		    			'viewcreationcolmodeljointable' => $viewcreationcolmodeljointable,
    					'roundtype' => $_POST['roundtype'],
    	 				'viewcreationcolumnname'=>$_POST['viewcreationcolumnname'],
						'viewcreationparenttable'=>$_POST['parenttable'],
						'viewcreationjoincolmodelname' => $_POST['joincolumnname'],
						'linkable' => $_POST['linkableoption'],
		    	 		'linkmoduleid' => $relatedmoduleid,
    	 				'selectquery' => $_POST['querybox'],
    	 		        'selecttype' => $selecttype
		   );
    	$update = array_merge($update,$this->Crudmodel->updatedefaultvalueget());
    	$this->db->where('viewcreationcolumnid', $id);
    	$this->db->update('viewcreationcolumns',$update);
    	// notification log
    	$user = $this->Basefunctions->username;
    	$userid = $this->Basefunctions->logemployeeid;
    	$activity = ''.$user.' updated viewcreationcolumn-'.$_POST['viewcreationcolumnname'].'';
    	$this->Basefunctions->notificationcontentadd($id,'Updated',$activity ,$userid,$this->viewcreationcolumnid);
    	echo 'SUCCESS';
    }
    public function delete(){
    	$id=$_GET['primaryid'];
    	//delete stone charge
    	$delete = $this->Crudmodel->deletedefaultvalueget();
    	$this->db->where('viewcreationcolumnid',$id);
    	$this->db->update('viewcreationcolumns',$delete);
    	// notification log
    	$viewcreationcolumn = $this->Basefunctions->singlefieldfetch('viewcreationcolumnname','viewcreationcolumnid','viewcreationcolumns',$id);
    	$user = $this->Basefunctions->username;
    	$userid = $this->Basefunctions->logemployeeid;
    	$activity = ''.$user.' deleted merge text-'.$viewcreationcolumn.'';
    	$this->Basefunctions->notificationcontentadd($id,'Deleted',$activity ,$userid,$this->viewcreationcolumnid);
    	echo 'SUCCESS';
    }
    public function retrievequery()
    {
    	$result_array=[];
    	$round_array=[];
    	$uniqueid_array=[];
    	$querybox=$_POST['querybox'];
    	$uniqueroundarray=$_POST['uniqueroundarray'];
    	$this->db->select('whereclausecreation.wherequery,whereclausecreation.whereclauseid');
    	$this->db->from('whereclausecreation');
    	$this->db->where_in('whereclauseid',$querybox);
    	$this->db->where('status',$this->Basefunctions->activestatus);
    	$info_last=$this->db->get();
    	foreach($info_last->result() as $data){
    		$round_array[$data->whereclauseid]=$data->wherequery;
    	}
    	$this->db->select('roundtype.roundtypename,roundtype.roundtypevalue');
    	$this->db->from('roundtype');
    	$this->db->where_in('roundtypename',$uniqueroundarray);
    	$this->db->where('status',$this->Basefunctions->activestatus);
    	$info_last_round=$this->db->get();
    	foreach($info_last_round->result() as $data){
    		$uniqueid_array[$data->roundtypename]=$data->roundtypevalue;
    	}
    	$result_array=array_merge($round_array,$uniqueid_array);
    	echo json_encode($result_array);
    }
    public function loadautocompletedata(){
    	$result_array=[];
    	$round_array=[];
    	$viewcreation_Columnarray=[];
    	$this->db->select('viewcreationcolumns.viewcreationcolumnname,viewcreationcolumns.viewcreationcolmodeltable,viewcreationcolumns.viewcreationcolmodelname');
    	$this->db->from('viewcreationcolumns');
    	$this->db->where('status',$this->Basefunctions->activestatus);
    	$viewcreationarray=$this->db->get();
    	foreach($viewcreationarray->result() as $data){
    		$viewcreation_Columnarray[]=array('name' =>$data->viewcreationcolumnname,'username'=>$data->viewcreationcolmodeltable.'.'.$data->viewcreationcolmodelname);
    	}
    	$this->db->select('roundtype.roundtypekey,roundtype.roundtypename');
    	$this->db->from('roundtype');
    	$this->db->where('status',$this->Basefunctions->activestatus);
    	$info_last=$this->db->get();
    	foreach($info_last->result() as $data){
    		$round_array[]=array('name' =>$data->roundtypekey,'username'=>$data->roundtypename);
    	}
    	$result_array=array_merge($viewcreation_Columnarray,$round_array);
    	echo json_encode($result_array);
    }
    public function loadtabsection($moduleid){
    	$this->db->select('moduletabsection.moduletabsectionid,moduletabsection.moduletabsectionname',false);
    	$this->db->from('moduletabsection');
    	$this->db->where('moduletabsection.moduleid',$moduleid);
    	$this->db->where('moduletabsection.status',$this->Basefunctions->activestatus);
    	$this->db->order_by('moduletabsection.moduletabsectionid',"ASC");
    	$query=$this->db->get();
    	return  $query->result();
        }
        public function viewdynamicdatainfofetch($tablename,$sortcol,$sortord,$pagenum,$rowscount) {
        
        	$dataset ='viewcreationcolumns.viewcreationcolumnid,module.modulename,uitype.uitypename,moduletabsection.moduletabsectionname,viewcreationcolumns.viewcreationcolmodelaliasname,viewcreationcolumns.viewcreationcolmodeltable,viewcreationcolumns.viewcreationcolumnname';
        	$join=' LEFT OUTER JOIN module ON module.moduleid=viewcreationcolumns.viewcreationmoduleid';
        	$join.=' LEFT OUTER JOIN uitype ON uitype.uitypeid=viewcreationcolumns.uitypeid';
        	$join.=' LEFT OUTER JOIN moduletabsection ON moduletabsection.moduletabsectionid=viewcreationcolumns.moduletabsectionid';
        	$cuscondition='';
        	$status='viewcreationcolumns.status='.$this->Basefunctions->activestatus.'';
        
        	/* pagination */
        	$pageno = $pagenum-1;
        	$start = $pageno * $rowscount;
        	/* query */
        	$result = $this->db->query('select SQL_CALC_FOUND_ROWS '.$dataset.' from '.$tablename.' '.$join.' WHERE '.$cuscondition.' '. $status.' ORDER BY '.' '.$sortcol.' '.$sortord.' LIMIT '.$start.','.$rowscount.'');
        	$page = $this->Basefunctions->paginationgenerate($pagenum,$rowscount);
        	$data=array();
        	$i=0;
        	foreach($result->result() as $row) {
        		$viewcreationcolumnid = $row->viewcreationcolumnid;
        		$modulename = $row->modulename;
        		$uitypename = $row->uitypename;
        		$moduletabsectionname = $row->moduletabsectionname;
        		$viewcreationcolmodelaliasname = $row->viewcreationcolmodelaliasname;
        		$viewcreationcolmodeltable = $row->viewcreationcolmodeltable;
        		$viewcreationcolumnname = $row->viewcreationcolumnname;
        		$data[$i]=array('id'=>$viewcreationcolumnid,$modulename,$uitypename,$moduletabsectionname,$viewcreationcolmodelaliasname,$viewcreationcolmodeltable,$viewcreationcolumnname);
        		$i++;
        	}
        	$finalresult=array('0'=>$data,'1'=>$page,'2'=>'json');
        	return $finalresult;
        
        }
        public function checkuniquename($table,$fieldname,$value,$editid)
        {
        	if($editid!=''){//Edit Page
        		$check = 'NO';
        		$this->db->select($fieldname);
        		$this->db->from($table);
        		$this->db->where($fieldname,trim($value));
        		$this->db->where("viewcreationcolumnid !=",$editid);
        		$this->db->where('status',1);
        		$info=$this->db->get()->num_rows();
        		if($info > 0){
        			$check = 'YES';
        		}
        		echo trim($check);
        	}
        	else{ //Add Page
        		$check = 'NO';
        		$this->db->select($fieldname);
        		$this->db->from($table);
        		$this->db->where($fieldname,trim($value));
        		$this->db->where('status',1);
        		$info=$this->db->get()->num_rows();
        		if($info > 0){
        			$check = 'YES';
        		}
        		echo trim($check);
        	}
        }
 }