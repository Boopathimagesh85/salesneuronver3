<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Treatment extends MX_Controller {
	function __construct() {
		parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Treatment/Treatmentmodel');
		$this->load->view('Base/formfieldgeneration');
	}
	public function index() {
		$moduleid= array(70);
		sessionchecker($moduleid);
		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp']=$this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp']=$this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle']=$this->Basefunctions->gridtitleinformationfetch($moduleid);
		$viewfieldsmoduleids = array(70);
		$viewmoduleid = array(70);
		$data['moduleids']=$viewmoduleid;
		$data['filedmodids']=$viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Treatment/Treatmentview',$data);
	}
	public function newdatacreate() {
		$this->Treatmentmodel->newdatacreatemodel();
	}
	public function fetchformdataeditdetails() {
		$moduleid= array(70);
		$this->Treatmentmodel->informationfetchmodel($moduleid);
	}
	public function datainformationupdate() {
		$this->Treatmentmodel->datainformationupdatemodel();
	}
	public function deleteinformationdata() {
		$moduleid= array(70);
		$this->Treatmentmodel->deleteoldinformation($moduleid);
	}
	//treatment inner grid details fetch
	public function treatmentdetailfetch() {
		$this->Treatmentmodel->treatmentdetailfetchmodel();
	}
}