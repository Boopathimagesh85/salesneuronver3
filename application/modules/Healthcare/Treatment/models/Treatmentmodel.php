<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Treatmentmodel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
	}
	public function newdatacreatemodel() {
		$cdate = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$status	= $this->Basefunctions->activestatus;
		$treatmentmodel = $_POST['treatmentmodel'];
		$summaryquantity = $_POST['summaryquantity'];
		$sumcost = $_POST['summarycost'];
		$summarydiscount = $_POST['summarydiscount'];
		$summarynetamount = $_POST['summarynetamount'];
		$contactid = $_POST['contactid'];
		$treatmentarray = array(
			'treatmentmodel'=>$treatmentmodel,
			'summaryquantity'=>$summaryquantity,
			'summarycost'=>$sumcost,
			'summarydiscount'=>$summarydiscount,
			'summarynetamount'=>$summarynetamount,
			'contactid'=>$contactid,
			'industryid'=>$this->Basefunctions->industryid,
			'branchid'=>$this->Basefunctions->branchid,
			'createdate'=>$cdate,
			'lastupdatedate'=>$cdate,
			'createuserid'=>$userid,
			'lastupdateuserid'=>$userid,
			'status'=>$status
		);
		$treatmentdetail = array();
		$this->db->insert('treatment',$treatmentarray);
		$primaryid = $this->db->insert_id();
		$gridrows = $_POST['numofrows'];
		$girddata = $_POST['griddatas'];
		$girddatainfo = json_decode($girddata, true);
		$defdataarr = $this->Crudmodel->defaultvalueget();
		for($i=0;$i<$gridrows;$i++){
			$girddatainfo[$i]['treatmentid']=$primaryid;
			$girddatainfo[$i]['quantity']=$girddatainfo[$i]['treatmentquantity'];
			$girddatainfo[$i]['comments']=$girddatainfo[$i]['treatmentcomments'];
			unset($girddatainfo[$i]['treatmenttreatmentdetailsid']);
			unset($girddatainfo[$i]['contactid']);
			unset($girddatainfo[$i]['productidname']);
			unset($girddatainfo[$i]['treatmentmodel']);
			unset($girddatainfo[$i]['treatmentquantity']);
			unset($girddatainfo[$i]['treatmentcomments']);
			$treatmentdetail = array_merge($girddatainfo[$i],$defdataarr);
			$this->db->insert('treatmenttreatmentdetails',$treatmentdetail);
		}
		echo 'TRUE';
	}
	public function informationfetchmodel($moduleid) {
		//table and fields information
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementspartabname']);
		$restricttable = array();
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//filter parent table
		$partablename =  $this->Crudmodel->filtervalue($elementpartable);
		$primaryid = $_GET['dataprimaryid'];
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetchwithrestrict($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$tablename,$partablename,$formfieldstable,$restricttable,$moduleid);
		echo $result;
	}
	public function datainformationupdatemodel() {
		$cdate = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$status	= $this->Basefunctions->activestatus;
		$treatmentmodel = $_POST['treatmentmodel'];
		$summaryquantity = $_POST['summaryquantity'];
		$sumcost = $_POST['summarycost'];
		$summarydiscount = $_POST['summarydiscount'];
		$summarynetamount = $_POST['summarynetamount'];
		$contactid = $_POST['contactid'];
		$primaryid = $_POST['treatmentprimarydataid'];
		$treatmentdetaildelete = $_POST['treatmentdetaildelete'];
		$treatmentarray = array(
				'treatmentmodel'=>$treatmentmodel,
				'summaryquantity'=>$summaryquantity,
				'summarycost'=>$sumcost,
				'summarydiscount'=>$summarydiscount,
				'summarynetamount'=>$summarynetamount,
				'contactid'=>$contactid,
				'createdate'=>$cdate,
				'lastupdatedate'=>$cdate,
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
				'status'=>$status
		);
		$this->db->where('treatment.treatmentid',$primaryid);
		$this->db->update('treatment',$treatmentarray);
		$treatmentdetail = array();
		$gridrows = $_POST['numofrows'];
		$girddata = $_POST['griddatas'];
		$girddatainfo = json_decode($girddata, true);
		$defdataarr = $this->Crudmodel->defaultvalueget();
		for($i=0;$i<$gridrows;$i++) {
			if(!$girddatainfo[$i]['treatmenttreatmentdetailsid']){
				$girddatainfo[$i]['treatmentid']=$primaryid;
				$girddatainfo[$i]['quantity']=$girddatainfo[$i]['treatmentquantity'];
				$girddatainfo[$i]['comments']=$girddatainfo[$i]['treatmentcomments'];
				unset($girddatainfo[$i]['treatmenttreatmentdetailsid']);
				unset($girddatainfo[$i]['contactid']);
				unset($girddatainfo[$i]['productidname']);
				unset($girddatainfo[$i]['treatmentmodel']);
				unset($girddatainfo[$i]['treatmentquantity']);
				unset($girddatainfo[$i]['treatmentcomments']);
				$treatmentdetail = array_merge($girddatainfo[$i],$defdataarr);
				$this->db->insert('treatmenttreatmentdetails',$treatmentdetail);
			}
		}
		$delrowid = explode(',',$treatmentdetaildelete);
		for($k=0;$k<count($delrowid);$k++){
			$treatmentupdatearray = array('lastupdatedate'=>$cdate,'lastupdateuserid'=>$userid,'status'=>0);
			$this->db->where('treatmenttreatmentdetails.treatmenttreatmentdetailsid',$delrowid[$k]);
			$this->db->update('treatmenttreatmentdetails',$treatmentupdatearray);
		}
		echo 'TRUE';
	}
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primarydataid'];
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				echo 'TRUE';
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			echo 'TRUE';
		}
	}
	public function treatmentdetailfetchmodel() {
		$productdetail = '';
		$primarydataid=trim($_GET['primarydataid']);
		$this->db->select('treatmenttreatmentdetailsid,treatmenttreatmentdetails.productid,product.productname,treatmenttreatmentdetails.quantity,treatmenttreatmentdetails.cost,treatmenttreatmentdetails.discount,treatmenttreatmentdetails.netamount,treatmenttreatmentdetails.comments');
		$this->db->from('treatmenttreatmentdetails');
		$this->db->join('product','product.productid=treatmenttreatmentdetails.productid');
		$this->db->where('treatmenttreatmentdetails.treatmentid',$primarydataid);
		$this->db->where('treatmenttreatmentdetails.status',$this->Basefunctions->activestatus);
		$data=$this->db->get();
		if($data->num_rows() > 0) {
			$j=0;
			foreach($data->result() as $value) {
				$productdetail->rows[$j]['id'] = $j;
				$softwareindustryid = $this->Basefunctions->industryid;
				$productdetail->rows[$j]['cell']=array(
					'',
					$value->productname,
					$value->productid,
					$value->quantity,
					$value->cost,
					$value->discount,
					$value->netamount,
					$value->comments,
					'',
					$value->treatmenttreatmentdetailsid,	
				);
				$j++;
			}
		}
		echo  json_encode($productdetail);
	}
}