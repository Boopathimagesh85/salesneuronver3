<!DOCTYPE html>
<html lang='en'>
	<head>
		<?php $this->load->view('Base/headerfiles'); ?>
	</head>
	<body class=''>
		<?php
			$moduleid = implode(',',$moduleids);
			$device = $this->Basefunctions->deviceinfo();
			$dataset['gridenable'] = 'yes';
			$dataset['moduleid']=$moduleids;
			$dataset['griddisplayid'] = 'Treatmentcreationview';
			$dataset['gridtitle'] = $gridtitle['title'];
			$dataset['titleicon'] = $gridtitle['titleicon'];
			$dataset['spanattr'] = array();
			$dataset['gridtableid'] = 'Treatmentaddgrid';
			$dataset['griddivid'] = 'Treatmentaddgridnav';
			$dataset['forminfo'] = array(array('id'=>'Treatmentcreationformadd','class'=>'hidedisplay','formname'=>'Treatmentcreationform'));
			if($device=='phone') {
				$this->load->view('Base/gridmenuheadermobile',$dataset);
				$this->load->view('Base/overlaymobile');
				$this->load->view('Base/viewselectionoverlay');
			} else {
				$this->load->view('Base/gridmenuheader',$dataset);
				$this->load->view('Base/overlay');
			}
		?>
	</body>
		<?php $this->load->view('Base/bottomscript'); ?>
		<script src='<?php echo base_url();?>js/Treatment/treatment.js' type='text/javascript'></script>
</html>