<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Casesheet extends MX_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Casesheet/Casesheetmodel');
		$this->load->helper('formbuild');
	}
	public function index() {
		$moduleid= array(60);
		sessionchecker($moduleid);
		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp']=$this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp']=$this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle']=$this->Basefunctions->gridtitleinformationfetch($moduleid);
		$viewfieldsmoduleids = array(60);
		$viewmoduleid = array(60);
		$data['moduleids']=$viewmoduleid;
		$data['filedmodids']=$viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Casesheet/Casesheetview',$data);
	}
	public function newdatacreate() {
		$this->Casesheetmodel->newdatacreatemodel();
	}
	public function fetchformdataeditdetails() {
		$moduleid=array(60);
		$this->Casesheetmodel->informationfetchmodel($moduleid);
	}
	public function datainformationupdate() {
		$this->Casesheetmodel->datainformationupdatemodel();
	}
	public function deleteinformationdata() {
		$moduleid=array(60);
		$this->Casesheetmodel->deleteoldinformation($moduleid);
	}
}