<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Emrdocuments extends MX_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Emrdocuments/Emrdocumentsmodel');
		$this->load->helper('formbuild');
	}
	public function index() {
		$moduleid= array(64);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp']=$this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp']=$this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle']=$this->Basefunctions->gridtitleinformationfetch($moduleid);
		$viewfieldsmoduleids = array(64);
		$viewmoduleid = array(64);
		$data['moduleids']=$viewmoduleid;
		$data['filedmodids']=$viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Emrdocuments/Emrdocumentsview',$data);
	}
	public function newdatacreate() {
		$this->Emrdocumentsmodel->newdatacreatemodel();
	}
	public function fetchformdataeditdetails() {
		$moduleid=array(64);
		$this->Emrdocumentsmodel->informationfetchmodel($moduleid);
	}
	public function datainformationupdate() {
		$this->Emrdocumentsmodel->datainformationupdatemodel();
	}
	//delete old information
	public function deleteinformationdata(){
		$moduleid = array(64);
		$this->Emrdocumentsmodel->deleteoldinformation($moduleid);
	}
}