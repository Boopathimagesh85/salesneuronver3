<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Emrdocumentsmodel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
	}
	public function newdatacreatemodel() {
		$cdate = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$status	= $this->Basefunctions->activestatus;
		$folname = $_POST['foldernameid'];
		$typeid = $_POST['typeid'];
		$keywords = $_POST['tags'];
		$moduleid = 202;
		$commonid = $_POST['nameid'];
		$uploadid = $_POST['attachment_fromid'];
		//$fileid = $_POST['fileid'];
		$filename = $_POST['attachment'];
		$flesize = $_POST['attachment_size'];
		$filetype = $_POST['attachment_type'];
		$filepath = $_POST['attachment_path'];
		$fname = explode(',',$filename);
		$fsize = explode(',',$flesize);
		$ftype = explode(',',$filetype);
		$fpath = explode(',',$filepath);
		$count = count($fname);
		//looping for multi insertion
		for($i=0;$i < $count;$i++) {
			if($fname[$i] != '') {
				$document = array(
						'commonid'=>$commonid,
						'moduleid'=>$moduleid,
						'filename'=>$fname[$i],
						'typeid'=>$typeid,
						'filename_size'=>$fsize[$i],
						'filename_type'=>$ftype[$i],
						'filename_path'=>$fpath[$i],
						'filename_fromid'=>$uploadid,
						'foldernameid'=>$folname,
						'keywords'=>$keywords,
						'industryid'=>$this->Basefunctions->industryid,
						'branchid'=>$this->Basefunctions->branchid,
						'createdate'=>$cdate,
						'lastupdatedate'=>$cdate,
						'createuserid'=>$userid,
						'lastupdateuserid'=>$userid,
						'status'=>$status
				);
				$this->db->insert('crmfileinfo',$document);
			}
		}
		echo "TRUE";
	}
	public function informationfetchmodel($moduleid) {
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementspartabname']);
		$partablename = $this->Crudmodel->filtervalue($elementpartable);
		$fieldstable = $this->Crudmodel->filtervalue($formfieldstable);
		$primaryid = $_GET['dataprimaryid'];
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$fieldstable,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	public function datainformationupdatemodel() {
		$formfieldsname = explode(',',$_POST['emrdocumentselementsname']);
		$formfieldstable = explode(',',$_POST['emrdocumentselementstable']);
		$formfieldscolmname = explode(',',$_POST['emrdocumentselementscolmn']);
		$categoryelementpartable = explode(',',$_POST['emrdocumentselementspartabname']);
		$partablename =  $this->Crudmodel->filtervalue($categoryelementpartable);
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['emrdocumentsprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		echo $result;
	}
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$msg='False';
		$id = $_GET['primarydataid'];
		$filename = $this->Basefunctions->generalinformaion('crmfileinfo','filename','crmfileinfoid',$id);
		//filter unique table
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		//parent table grouping
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		//primary key
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		//delete operation
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		{//workflow management -- for data delete
			$this->Basefunctions->workflowmanageindatadeletemode(207,$id,$primaryname,$partabname);
		}
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				$msg='Denied';
			} else {
				//notification log
				$notimsg = $this->Basefunctions->notificationtemplatedatafetch(207,$id,$partabname,4);
				if($notimsg != '') {
					$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,1,207);
				}
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				$msg='TRUE';
			}
		} else {
			//notification log
			$notimsg = $this->Basefunctions->notificationtemplatedatafetch(207,$id,$partabname,4);
			if($notimsg != '') {
				$this->Basefunctions->notificationcontentadd($id,'Deleted',$notimsg,1,207);
			}
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			$msg='TRUE';
		}
		echo $msg;
	}
}