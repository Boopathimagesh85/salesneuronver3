<!DOCTYPE html>
<html lang='en'>
	<head>
		<?php $this->load->view('Base/headerfiles'); ?>
		<script src='<?php echo base_url();?>js/Medicalhistory/medicalhistory.js' type='text/javascript'></script>
	</head>
	<body class=''>
		<?php
			$moduleid = implode(',',$moduleids);
			$device = $this->Basefunctions->deviceinfo();
			$dataset['gridenable'] = 'yes';
			$dataset['moduleid']=$moduleids;
			$dataset['griddisplayid'] = 'Medicalhistorycreationview';
			$dataset['gridtitle'] = $gridtitle['title'];
			$dataset['titleicon'] = $gridtitle['titleicon'];
			$dataset['spanattr'] = array();
			$dataset['gridtableid'] = 'Medicalhistoryaddgrid';
			$dataset['griddivid'] = 'Medicalhistoryaddgridnav';
			$dataset['forminfo'] = array(array('id'=>'Medicalhistorycreationformadd','class'=>'hidedisplay','formname'=>'Medicalhistorycreationform'));
			if($device=='phone') {
				$this->load->view('Base/gridmenuheadermobile',$dataset);
				$this->load->view('Base/overlaymobile');
				$this->load->view('Base/viewselectionoverlay');
			} else {
				$this->load->view('Base/gridmenuheader',$dataset);
				$this->load->view('Base/overlay');
			}
			$this->load->view('Base/basedeleteform');
		?>
	</body>
		<?php $this->load->view('Base/bottomscript'); ?>
</html>