<div class='large-12 columns headercaptionstyle headerradius addformreloadtablet'>
	<div class='large-6 medium-6 small-12 columns headercaptionleft'>
		<span class='fa fa-align-justify left-off-canvas-toggle headermainmenuicon'> </span>
		<span class='gridcaptionpos'>
			<span>
				<?php echo $gridtitle; ?>
			</span>
		</span>
	</div>
	<?php $this->load->view('Base/formheadericons'); ?>  
</div>
<div class='large-12 columns tabgroupstyle show-for-large-up'>
	<?php
		$ulattr = array('class'=>'tabs');
		$uladdinfo = 'data-tab';
		$tabgrpattr = array('class'=>'tab-title sidebaricons');
		$tabstatus = 'active';
		$dataname = 'subform';
		echo tabgroupgenerate($ulattr,$uladdinfo,$tabgrpattr,$tabstatus,$dataname,$modtabgrp);
		$dropdowntabval = array();
		$m=1;
		foreach($modtabgrp as $value) {
			$dropdowntabval[$m]=$value['tabgrpname'];
			$m++;
		}
	?>
</div>
<div class='large-12 columns tabgroupstyle centertext show-for-medium-down tabgrpddstyle' >
	<span class='tabmoiconcontainer'>
		<select id='tabgropdropdown' class='chzn-select' style='width:40%'>
			<?php
				foreach($dropdowntabval as $tabid=>$tabname) {
					echo '<option value='.$tabid.'>'.$tabname.'</option>';
				}
			?>
		</select>
	</span>
</div>
<script>
	$(document).ready(function(){
		$('#tabgropdropdown').change(function(){
			var tabgpid = $('#tabgropdropdown').val(); 
			$('.sidebaricons[data-subform='+tabgpid+']').trigger('click');
		});
	});
</script>
<div class='large-12 columns addformunderheader'>&nbsp;</div>
<div class='large-12 columns scrollbarclass addformcontainer tabtouchaddcontainer'>
	<div class='row'>&nbsp;</div>
	<form method='POST' name='Medicalhistorydataaddform' class='' action ='' id='Medicalhistorydataaddform' enctype='multipart/form-data'>
		<span id='Medicalhistoryformaddwizard' class='validationEngineContainer'>
			<span id='Medicalhistoryformeditwizard' class='validationEngineContainer'>
				<?php
					formfieldstemplategenerator($modtabgrp);
				?>
			</span>
		</span>
		<?php
			echo hidden('primarydataid','');
			echo hidden('sortorder','');
			echo hidden('sortcolumn','');
			$value = '';
			echo hidden('resctable',$value);
			$modid = $this->Basefunctions->getmoduleid($filedmodids);
			echo hidden('viewfieldids',$modid);
		?>
	</form>
</div>