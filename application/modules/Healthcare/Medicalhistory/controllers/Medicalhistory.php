<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Medicalhistory extends MX_Controller {
	function __construct() {
		parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Medicalhistory/Medicalhistorymodel');
		$this->load->view('Base/formfieldgeneration');
	}
	public function index() {
		$moduleid= array(62);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp']=$this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp']=$this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle']=$this->Basefunctions->gridtitleinformationfetch($moduleid);
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids']=$viewmoduleid;
		$data['filedmodids']=$viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Medicalhistory/Medicalhistoryview',$data);
	}
	public function newdatacreate() {
		$this->Medicalhistorymodel->newdatacreatemodel();
	}
	public function fetchformdataeditdetails() {
		$moduleid=array(62);
		$this->Medicalhistorymodel->informationfetchmodel($moduleid);
	}
	public function datainformationupdate() {
		$this->Medicalhistorymodel->datainformationupdatemodel();
	}
	public function deleteinformationdata() {
		$moduleid=array(62);
		$this->Medicalhistorymodel->deleteoldinformation($moduleid);
	}
}