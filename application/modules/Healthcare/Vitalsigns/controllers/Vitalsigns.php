<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Vitalsigns extends MX_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Vitalsigns/Vitalsignsmodel');
		$this->load->helper('formbuild');
	}
	public function index() {
		$moduleid = array(58);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp']=$this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp']=$this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle']=$this->Basefunctions->gridtitleinformationfetch($moduleid);
		$viewfieldsmoduleids = $moduleid;
		$viewmoduleid = $moduleid;
		$data['moduleids']=$viewmoduleid;
		$data['filedmodids']=$viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Vitalsigns/Vitalsignsview',$data);
	}
	public function newdatacreate() {
		$this->Vitalsignsmodel->newdatacreatemodel();
	}
	public function fetchformdataeditdetails() {
		$moduleid=array(58);
		$this->Vitalsignsmodel->informationfetchmodel($moduleid);
	}
	public function datainformationupdate() {
		$this->Vitalsignsmodel->datainformationupdatemodel();
	}
	public function deleteinformationdata() {
		$moduleid=array(58);
		$this->Vitalsignsmodel->deleteoldinformation($moduleid);
	}
}