<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Base Header File -->
	<?php $this->load->view('Base/headerfiles'); ?>
	<?php $this->load->view('Base/bottomscript'); ?>
<style>
.emrpatient .select2-container .select2-choice{color:#fff;margin:0 auto;}
</style>
</head>
<body class="hidedisplay">
<?php
$this->load->view('Base/modulelist'); 
?>
<div class="row" style="max-width:100%">
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<?php
				$modid = '62,58,59,60,67,61,64,68,70,69';
				$dataset['gridtitle'] = $gridtitle['title'];
				$dataset['titleicon'] = $gridtitle['titleicon'];
				$dataset['formtype'] = 'multipleaddform';
				$dataset['multimoduleid'] = $modid;
				$this->load->view('Base/mainviewaddformheader',$dataset);
			?>
			<div class="large-12 columns scrollbarclass paddingzero productmastertouch">
				<div class="mastermodules">
					<?php
						//mvsec - main view with section
						//fwgrd - form with grid
						//mvfwgrid - main view with form with grid
						$modtypeid = 'mvsec,fwgrid,fwgrid,fwgrid,fwgrid,fwgrid,fwgrid,fwgrid,mvfwgrid,mvfwgrid';
						$module = array('moduleid'=>$modid,'modtypeid'=>$modtypeid);
						//function call for dash board form fields generation
						newdashboardformfieldsgeneratorwithgrid($moddbfrmfieldsgen,1,$module);
					?>
				</div>		
			</div>
			<a class="exit-off-canvas"></a>
		</div>
	</div>
</div>
<?php
	$device = $this->Basefunctions->deviceinfo();
	if($device=='phone') {
		$this->load->view('Base/overlaymobile');
	} else {
		$this->load->view('Base/overlay');
	}
?>
</body>
	<div id="supportoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<div id="notificationoverlayappend" class="large-12 columns paddingzero" style="position:absolute;"></div>
	<script>
	$(document).ready(function(){
		{//enable support icon
			$(".supportoverlay").css("display","inline-block")
		}
	});
	//patient-emrmaster session check
		var ca = document.cookie.split(';');
		for(var i = 0; i <ca.length; i++) {
	       var emrmasterpatientid =  ca[i].split('=');
	       if(emrmasterpatientid[0] == 'patientid'){
		       $("#emrdynamicdddataview").val(emrmasterpatientid[1]);
	       }
	    }
	//from master to patient
	$("#emrmasterpatientid").click(function() {
		 var patientdata = $("#emrdynamicdddataview").select2('data');
		if (patientdata) {
			sessionStorage.setItem("patientdata",patientdata.id);
			window.location = base_url+'Contact';
		} else {
			alertpopup("Please select a patient");
		}
	});
	$("#emrdynamicdddataview").change(function(){
		$("#processoverlay").show();
		var patientsids = $("#emrdynamicdddataview").val();
		$("#medicalhistorycontactid").val(patientsids);
		$("#vitalsignscontactid").val(patientsids);
		$("#allergycontactid").val(patientsids);
		$("#casesheetcontactid").val(patientsids);
		$("#familyhistorcontactid").val(patientsids);
		$("#immunizationcontactid").val(patientsids);
		$("#emrdocumentscontactid").val(patientsids);
		$("#labordercontactid").val(patientsids);
		$("#contactid").val(patientsids);
		$("#prescriptioncontactid").val(patientsids);
		Medicalhistoryaddgrid();
		Vitalsignsaddgrid();
		Allergyaddgrid();
		Casesheetaddgrid();
		Familyhistoryaddgrid();
		Immunizationaddgrid();
		Emrdocumentsaddgrid();
		Laborderaddgrid();
		treatmentaddgrid();
		Prescriptionsaddgrid();
		$("#processoverlay").fadeOut();
	});
	$("#medicalhistoryviewtoggle").hide();
	$("#vitalsignsviewtoggle").hide();
	$("#allergyviewtoggle").hide();
	$("#casesheetviewtoggle").hide();
	$("#familyhistoryviewtoggle").hide();
	$("#immunizationviewtoggle").hide();
	$("#emrdocumentsviewtoggle").hide();
	$("#laborderviewtoggle").hide();
	$("#treatmentviewtoggle").hide();
	$("#prescriptionsviewtoggle").hide();
	{// Date of Birth Date Picker
		var dateformetdata = $('#birthdate').attr('data-dateformater');
		$('#birthdate').datetimepicker({
			dateFormat: dateformetdata,
			showTimepicker :false,
			minDate: null,
			changeMonth: true,
			changeYear: true,
			maxDate:0,
			yearRange : '1947:c+100',
			onSelect: function () {
				var checkdate = $(this).val();
				if(checkdate != '') {
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
				}
			},
			onClose: function () {
				$('#birthdate').focus();
			}
		});
	}
	$("#tab1").click(function(){
		$(".tabclass").addClass('hidedisplay');
		$("#tab1class").removeClass('hidedisplay');
	});
	$("#tab2").click(function(){
		$(".tabclass").addClass('hidedisplay');
		$("#tab2class").removeClass('hidedisplay');
	});
	$("#tab3").click(function(){
		$(".tabclass").addClass('hidedisplay');
		$("#tab3class").removeClass('hidedisplay');
	});
	$("#tab4").click(function(){
		$(".tabclass").addClass('hidedisplay');
		$("#tab4class").removeClass('hidedisplay');
	});
	$("#tab5").click(function(){
		$(".tabclass").addClass('hidedisplay');
		$("#tab5class").removeClass('hidedisplay');
	});
	$("#tab6").click(function(){
		$(".tabclass").addClass('hidedisplay');
		$("#tab6class").removeClass('hidedisplay');
	});
	$("#tab7").click(function(){
		$(".tabclass").addClass('hidedisplay');
		$("#tab7class").removeClass('hidedisplay');
	});
	$("#tab8").click(function(){
		$(".tabclass").addClass('hidedisplay');
		$("#tab8class").removeClass('hidedisplay');
	});
	$("#tab9").click(function(){
		$(".tabclass").addClass('hidedisplay');
		$("#tab9class").removeClass('hidedisplay');
	});
	$("#tab10").click(function(){
		$(".tabclass").addClass('hidedisplay');
		$("#tab10class").removeClass('hidedisplay');
	});
	</script>	
</html>