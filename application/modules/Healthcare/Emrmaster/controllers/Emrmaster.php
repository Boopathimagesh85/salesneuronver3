<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Emrmaster extends MX_Controller
{
    public function __construct() {
        parent::__construct();
		$this->load->helper('formbuild');
		$this->load->view('Base/formfieldgeneration');
		$this->load->model('Emrmaster/Emrmastermodel');
    }
    //first basic hitting view
    public function index() {
    	$moduleid = array(272);
    	sessionchecker($moduleid);
		/*action*/
		$data['moddashboardtabgrp'] = $this->Basefunctions->moddashboardtabgrpgeneration(272);
		//die();
		$data['moddbfrmfieldsgen'] = $this->Emrmastermodel->dashboardmodsecfieldsgeneration(272);
		$data['gridtitle'] = $this->Basefunctions->gridtitleinformationfetch($moduleid);
		$data['patientdetails'] = $this->Emrmastermodel->patientfetchmodel('75');
		$this->load->view('Emrmaster/emrmasterview',$data);
	}
}