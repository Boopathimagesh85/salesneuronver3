<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Emrmastermodel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
		$softwareindustryid = $this->Basefunctions->industryid;
    }
    
    //For patients data fetch
    public function patientfetchmodel($moduleid) {
    	
    	$tablename = 'contact';
    	$value = array();
    	$fieldval = array();
    	$tabid = $tablename.'id';
    	$tabfieldname = $tablename.'name';
    	$dd = $this->Basefunctions->dynamicdropdownvalfetchparent($tablename,$tabfieldname,$tabid,'');
    	return $dd;
	    //print_r($dd); die;
    }
    //Dash board Module tab group section and fields generation
    public function dashboardmodsecfieldsgeneration($moduleid) {
    	$userroleid = $this->Basefunctions->userroleid;
    	$i=0;
    	$moduletabsecfldsgrparray = array();
    	$dashboardmodid = array();
    	$dbdata = $this->db->select('module.moduledashboardid')->from('module')->join('moduleinfo','moduleinfo.moduleid=module.moduleid')->where("FIND_IN_SET('$userroleid',moduleinfo.userroleid) >", 0)->where_in('module.moduleid',array($moduleid))->where('moduleinfo.status',1)->get();
    	foreach($dbdata->result() as $datarow) {
    		$dbdataid = $datarow->moduledashboardid;
    		$dashboardmodid = explode(',',$dbdataid);
    		foreach($dashboardmodid as $ids) {
    			$chk = $this->dashboardmodulecheckinfo($ids,$userroleid);
    			if($chk=='True') {
    				$this->db->select('modulefield.modulefieldid,modulefield.columnname,modulefield.tablename,modulefield.uitypeid,modulefield.fieldname,modulefield.fieldlabel,modulefield.readonly,modulefield.defaultvalue,modulefield.maximumlength,modulefield.mandatory,modulefield.active,modulefield.moduletabsectionid,modulefield.sequence,moduletabsection.moduletabsectionname,moduletabgroup.moduletabgroupid,moduletabgroup.moduletabgroupname,modulefield.moduletabid,modulefield.multiple,modulefield.ddparenttable,modulefield.parenttable,modulefield.fieldcolumntype,module.modulename,module.modulelink,modulefield.decimalsize,moduletabsection.moduletabsectiontypeid,modulefield.fieldcustomvalidation,userrolemodulefiled.fieldactive,userrolemodulefiled.fieldreadonly,modulefield.unique',false);
    				$this->db->from('modulefield');
    				$this->db->join('uitype','uitype.uitypeid=modulefield.uitypeid');
    				$this->db->join('userrolemodulefiled','userrolemodulefiled.modulefieldid=modulefield.modulefieldid','left outer');
    				$this->db->join('moduletabsection','moduletabsection.moduletabsectionid=modulefield.moduletabsectionid');
    				$this->db->join('moduletabgroup','moduletabgroup.moduletabgroupid=moduletabsection.moduletabgroupid');
    				$this->db->join('module','module.moduleid=moduletabgroup.moduleid');
    				$this->db->join('moduleinfo','moduleinfo.moduleid=module.moduleid');
    				$this->db->where("modulefield.moduletabid",$ids);
    				$this->db->where("userrolemodulefiled.moduleid",$ids);
    				$this->db->where("module.moduleid",$ids);
    				$this->db->where("moduleinfo.moduleid",$ids);
    				$this->db->where("FIND_IN_SET('$userroleid',moduletabgroup.userroleid) >", 0);
    				$this->db->where("FIND_IN_SET('$userroleid',moduletabsection.userroleid) >", 0);
    				$this->db->where("FIND_IN_SET('$userroleid',modulefield.userroleid) >", 0);
    				$this->db->where('userrolemodulefiled.userroleid',$userroleid);
    				$this->db->where_in('modulefield.status',array(1,10));
    				$this->db->where('userrolemodulefiled.status',1);
    				$this->db->where('moduletabsection.moduletabsectiontypeid',1);
    				$this->db->where('moduletabsection.status',1);
    				$this->db->where('moduletabgroup.status',1);
    				$this->db->order_by('moduletabgroup.sequence','asc');
    				$this->db->order_by('moduletabsection.sequence','asc');
    				$this->db->order_by('modulefield.sequence','asc');
    				$this->db->group_by('modulefield.modulefieldid');
    				$querytbg = $this->db->get();
    				foreach($querytbg->result() as $rows) {
    					$moduletabsecfldsgrparray[$i] = array('colname'=>$rows->columnname,'tabname'=>$rows->tablename,'uitypeid'=>$rows->uitypeid,'fieldname'=>$rows->fieldname,'filedlabel'=>$rows->fieldlabel,'readmode'=>$rows->fieldreadonly,'defvalue'=>$rows->defaultvalue,'maxlength'=>$rows->maximumlength,'mandmode'=>$rows->mandatory,'fieldmode'=>$rows->fieldactive,'tabsectionid'=>$rows->moduletabsectionid,'fieldseq'=>$rows->sequence,'tabsectionname'=>$rows->moduletabsectionname,'tabgroupname'=>$rows->moduletabgroupname,'modtabgrpid'=>$rows->moduletabgroupid,'moduleid'=>$rows->moduletabid,'modulename'=>$rows->modulename,'multiple'=>$rows->multiple,'ddparenttable'=>$rows->ddparenttable,'parenttable'=>$rows->parenttable,'fieldcolmtypeid'=>$rows->fieldcolumntype,'modlink'=>$rows->modulelink,'decisize'=>$rows->decimalsize,'sectiontypeid'=>$rows->moduletabsectiontypeid,'customrule'=>$rows->fieldcustomvalidation,'modfieldid'=>$rows->modulefieldid,'modfieldunique'=>$rows->unique);
    					$i++;
    				}
    			}
    		}
    	}
    	return $moduletabsecfldsgrparray;
    }
    //check the module is enable
    public function dashboardmodulecheckinfo($moduleid,$roleid) {
    	$this->db->select('moduleid');
    	$this->db->from('moduleinfo');
    	$this->db->where('moduleinfo.moduleid',$moduleid);
    	$this->db->where('moduleinfo.userroleid',$roleid);
    	$result = $this->db->get();
    	if($result->num_rows() >0) {
    		return 'True';
    	} else {
    		return 'False';
    	}
    }
}