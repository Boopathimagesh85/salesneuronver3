<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Prescriptions extends MX_Controller {
	function __construct() {
		parent::__construct();
		$this->load->helper('formbuild');
		$this->load->model('Prescriptions/Prescriptionsmodel');
		$this->load->view('Base/formfieldgeneration');
	}
	public function index() {
		$moduleid= array(69);
		sessionchecker($moduleid);
		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp']=$this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp']=$this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle']=$this->Basefunctions->gridtitleinformationfetch($moduleid);
		$viewfieldsmoduleids = array(69);
		$viewmoduleid = array(69);
		$data['moduleids']=$viewmoduleid;
		$data['filedmodids']=$viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Prescriptions/Prescriptionsview',$data);
	}
	public function newdatacreate() {
		$this->Prescriptionsmodel->newdatacreatemodel();
	}
	public function fetchformdataeditdetails() {
		$moduleid=array(69);
		$this->Prescriptionsmodel->informationfetchmodel($moduleid);
	}
	public function datainformationupdate() {
		$this->Prescriptionsmodel->datainformationupdatemodel();
	}
	public function deleteinformationdata() {
		$moduleid=array(69);
		$this->Prescriptionsmodel->deleteoldinformation($moduleid);
	}
	//treatment inner grid details fetch
	public function prescriptionsdetailfetch() {
		$this->Prescriptionsmodel->prescriptionsdetailfetchmodel();
	}
}