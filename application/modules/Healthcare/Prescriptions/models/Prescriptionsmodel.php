<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Prescriptionsmodel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
	}
	public function newdatacreatemodel() {
		$cdate = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$status	= $this->Basefunctions->activestatus;
		$prescriptionname = $_POST['prescriptionname'];
		$tabletinmorning = $_POST['tabletinmorning'];
		$tabletsinnoon = $_POST['tabletsinnoon'];
		$tabletsinnight = $_POST['tabletsinnight'];
		$contactid = $_POST['prescriptioncontactid'];
		$prescriptionarray = array(
				'prescriptionname'=>$prescriptionname,
				'tabletinmorning'=>$tabletinmorning,
				'tabletsinnoon'=>$tabletsinnoon,
				'tabletsinnight'=>$tabletsinnight,
				'totaltablets'=>'0',
				'beforefood'=>'0',
				'afterfood'=>'0',
				'contactid'=>$contactid,
				'industryid'=>$this->Basefunctions->industryid,
				'branchid'=>$this->Basefunctions->branchid,
				'createdate'=>$cdate,
				'lastupdatedate'=>$cdate,
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
				'status'=>$status
		);
		$prescriptiondetail = array();
		$this->db->insert('prescriptions',$prescriptionarray);
		$primaryid = $this->db->insert_id();
		$gridrows = $_POST['numofrows'];
		$girddata = $_POST['griddatas'];
		$girddatainfo = json_decode($girddata, true);
		$defdataarr = $this->Crudmodel->defaultvalueget();
		for($i=0;$i<$gridrows;$i++) {
			$girddatainfo[$i]['prescriptionsid']=$primaryid;
			$girddatainfo[$i]['productid']=$girddatainfo[$i]['drugnameid'];
			unset($girddatainfo[$i]['prescriptionsprescriptiondetailsid']);
			unset($girddatainfo[$i]['prescriptioncontactid']);
			unset($girddatainfo[$i]['drugnameidname']);
			unset($girddatainfo[$i]['drugnameid']);
			unset($girddatainfo[$i]['intakeidname']);
			unset($girddatainfo[$i]['timeframeidname']);
			unset($girddatainfo[$i]['modeidname']);
			unset($girddatainfo[$i]['prescriptionname']);
			$prescriptiondetail = array_merge($girddatainfo[$i],$defdataarr);
			$this->db->insert('prescriptionsprescriptiondetails',$prescriptiondetail);
		}
		echo 'TRUE';
	}
	public function informationfetchmodel($moduleid) {
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementspartabname']);
		$partablename = $this->Crudmodel->filtervalue($elementpartable);
		$fieldstable = $this->Crudmodel->filtervalue($formfieldstable);
		$primaryid = $_GET['dataprimaryid'];
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$fieldstable,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	public function datainformationupdatemodel() {
		$cdate = date($this->Basefunctions->datef);
		$userid = $this->Basefunctions->userid;
		$status	= $this->Basefunctions->activestatus;
		$prescriptionname = $_POST['prescriptionname'];
		$tabletinmorning = $_POST['tabletinmorning'];
		$tabletsinnoon = $_POST['tabletsinnoon'];
		$tabletsinnight = $_POST['tabletsinnight'];
		$contactid = $_POST['prescriptioncontactid'];
		$primaryid = $_POST['prescriptionsprimarydataid'];
		$prescriptionsdetaildelete = $_POST['prescriptionsdetaildelete'];
		$prescriptionarray = array(
				'prescriptionname'=>$prescriptionname,
				'tabletinmorning'=>$tabletinmorning,
				'tabletsinnoon'=>$tabletsinnoon,
				'tabletsinnight'=>$tabletsinnight,
				'totaltablets'=>'0',
				'beforefood'=>'0',
				'afterfood'=>'0',
				'contactid'=>$contactid,
				'createdate'=>$cdate,
				'lastupdatedate'=>$cdate,
				'createuserid'=>$userid,
				'lastupdateuserid'=>$userid,
				'status'=>$status
		);
		$prescriptiondetail = array();
		$this->db->where('prescriptions.prescriptionsid',$primaryid);
		$this->db->update('prescriptions',$prescriptionarray);
		$gridrows = $_POST['numofrows'];
		$girddata = $_POST['griddatas'];
		$girddatainfo = json_decode($girddata, true);
		$defdataarr = $this->Crudmodel->defaultvalueget();
		for($i=0;$i<$gridrows;$i++) {
			if(!$girddatainfo[$i]['prescriptionsprescriptiondetailsid']){
				$girddatainfo[$i]['prescriptionsid']=$primaryid;
				$girddatainfo[$i]['productid']=$girddatainfo[$i]['drugnameid'];
				unset($girddatainfo[$i]['prescriptionsprescriptiondetailsid']);
				unset($girddatainfo[$i]['prescriptioncontactid']);
				unset($girddatainfo[$i]['drugnameidname']);
				unset($girddatainfo[$i]['drugnameid']);
				unset($girddatainfo[$i]['intakeidname']);
				unset($girddatainfo[$i]['timeframeidname']);
				unset($girddatainfo[$i]['modeidname']);
				unset($girddatainfo[$i]['prescriptionname']);
				$prescriptiondetail = array_merge($girddatainfo[$i],$defdataarr);
				$this->db->insert('prescriptionsprescriptiondetails',$prescriptiondetail);
			}
		}
		$delrowid = explode(',',$prescriptionsdetaildelete);
		for($k=0;$k<count($delrowid);$k++) {
			$treatmentupdatearray = array('lastupdatedate'=>$cdate,'lastupdateuserid'=>$userid,'status'=>0);
			$this->db->where('prescriptionsprescriptiondetails.prescriptionsprescriptiondetailsid',$delrowid[$k]);
			$this->db->update('prescriptionsprescriptiondetails',$treatmentupdatearray);
		}
		echo 'TRUE';
	}
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primarydataid'];
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				echo 'TRUE';
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			echo 'TRUE';
		}
	}
	//prescription details fetch
	public function prescriptionsdetailfetchmodel(){
		$primarydataid=trim($_GET['primarydataid']);
		$this->db->select('prescriptionsprescriptiondetailsid,prescriptionsprescriptiondetails.productid,prescriptionsprescriptiondetails.night,prescriptionsprescriptiondetails.noon,prescriptionsprescriptiondetails.morning,prescriptionsprescriptiondetails.duration,prescriptionsprescriptiondetails.modeid,prescriptionsprescriptiondetails.timeframeid,prescriptionsprescriptiondetails.intakeid,product.productname,mode.modename,timeframe.timeframename,intake.intakename');
		$this->db->from('prescriptionsprescriptiondetails');
		$this->db->join('product','product.productid=prescriptionsprescriptiondetails.productid');
		$this->db->join('mode','mode.modeid=prescriptionsprescriptiondetails.modeid');
		$this->db->join('timeframe','timeframe.timeframeid=prescriptionsprescriptiondetails.timeframeid');
		$this->db->join('intake','intake.intakeid=prescriptionsprescriptiondetails.intakeid');
		$this->db->where('prescriptionsprescriptiondetails.prescriptionsid',$primarydataid);
		$this->db->where('prescriptionsprescriptiondetails.status',$this->Basefunctions->activestatus);
		$data=$this->db->get();
		if($data->num_rows() > 0) {
			$j=0;
			foreach($data->result() as $value) {
				$productdetail->rows[$j]['id'] = $value->prescriptionsprescriptiondetailsid;
				$softwareindustryid = $this->Basefunctions->industryid;
				$productdetail->rows[$j]['cell']=array(
						'',
						$value->productname,
						$value->productid,
						$value->intakename,
						$value->intakeid,
						$value->modename,
						$value->modeid,
						$value->duration,
						$value->morning,
						$value->noon,
						$value->night,
						'',
						$value->prescriptionsprescriptiondetailsid,
				);
				$j++;
			}
		}
		echo  json_encode($productdetail);
	}
}