<!DOCTYPE html>
<html lang='en'>
	<head>
		<?php $this->load->view('Base/headerfiles'); ?>
		<script src='<?php echo base_url();?>js/Prescriptions/prescriptions.js' type='text/javascript'></script>
	</head>
	<body class=''>
		<?php
			$moduleid = implode(',',$moduleids);
			$device = $this->Basefunctions->deviceinfo();
			$dataset['gridenable'] = 'yes';
			$dataset['moduleid']=$moduleids;
			$dataset['griddisplayid'] = 'Prescriptionscreationview';
			$dataset['gridtitle'] = $gridtitle['title'];
			$dataset['titleicon'] = $gridtitle['titleicon'];
			$dataset['spanattr'] = array();
			$dataset['gridtableid'] = 'Prescriptionsaddgrid';
			$dataset['griddivid'] = 'Prescriptionsaddgridnav';
			$dataset['forminfo'] = array(array('id'=>'Prescriptionscreationformadd','class'=>'hidedisplay','formname'=>'Prescriptionscreationform'));
			if($device=='phone') {
				$this->load->view('Base/gridmenuheadermobile',$dataset);
				$this->load->view('Base/overlaymobile');
				$this->load->view('Base/viewselectionoverlay');
			} else {
				$this->load->view('Base/gridmenuheader',$dataset);
				$this->load->view('Base/overlay');
			}
			$this->load->view('Base/basedeleteform');
		?>
	</body>
		<?php $this->load->view('Base/bottomscript'); ?>
</html>