<!DOCTYPE html>
<html lang='en'>
	<head>
		<?php $this->load->view('Base/headerfiles'); ?>
		<?php $this->load->view('Base/bottomscript'); ?>
		<script src='<?php echo base_url();?>js/Familyhistory/familyhistory.js' type='text/javascript'></script>
	</head>
	<body >
		<?php
			$device = $this->Basefunctions->deviceinfo();
			$dataset['gridenable'] = 'yes';
			$dataset['griddisplayid'] = 'Familyhistorycreationview';
			$dataset['maingridtitle'] =  $gridtitle['title'];
			$dataset['gridtitle'] = $gridtitle['title'];
			$dataset['titleicon'] = $gridtitle['titleicon'];
			$dataset['spanattr'] = array();
			$dataset['gridtableid'] = 'Familyhistoryaddgrid';
			$dataset['griddivid'] = 'Familyhistoryaddgridnav';
			$this->load->view('Familyhistory/Familyhistorycreationform',$dataset);
			$this->load->view('Base/basedeleteform');
			if($device=='phone') {
				$this->load->view('Base/overlaymobile');
			} else {
				$this->load->view('Base/overlay');
			}
			$this->load->view('Base/modulelist');
		?>
	</body>				
</html>