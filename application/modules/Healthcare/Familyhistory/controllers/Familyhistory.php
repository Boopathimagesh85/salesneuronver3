<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Familyhistory extends MX_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Familyhistory/Familyhistorymodel');
		$this->load->helper('formbuild');
	}
	public function index() {
		$moduleid= array(67);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp']=$this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp']=$this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle']=$this->Basefunctions->gridtitleinformationfetch($moduleid);
		$viewfieldsmoduleids = array(67);
		$viewmoduleid = array(67);
		$data['moduleids']=$viewmoduleid;
		$data['filedmodids']=$viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Familyhistory/Familyhistoryview',$data);
	}
	public function newdatacreate() {
		$this->Familyhistorymodel->newdatacreatemodel();
	}
	public function fetchformdataeditdetails() {
		$moduleid=array(67);
		$this->Familyhistorymodel->informationfetchmodel($moduleid);
	}
	public function datainformationupdate() {
		$this->Familyhistorymodel->datainformationupdatemodel();
	}
	public function deleteinformationdata() {
		$moduleid=array(67);
		$this->Familyhistorymodel->deleteoldinformation($moduleid);
	}
}