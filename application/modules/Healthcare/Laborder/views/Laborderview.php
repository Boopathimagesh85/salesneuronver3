<!DOCTYPE html>
<html lang='en'>
	<head>
		<?php $this->load->view('Base/headerfiles'); ?>
	</head>
	<body >
		<?php
			$device = $this->Basefunctions->deviceinfo();
			$dataset['gridenable'] = 'yes';
			$dataset['griddisplayid'] = 'Labordercreationview';
			$dataset['maingridtitle'] =  $gridtitle['title'];
			$dataset['gridtitle'] = $gridtitle['title'];
			$dataset['titleicon'] = $gridtitle['titleicon'];
			$dataset['spanattr'] = array();
			$dataset['gridtableid'] = 'Laborderaddgrid';
			$dataset['griddivid'] = 'Laborderaddgridnav';
			$this->load->view('Laborder/Labordercreationform',$dataset);
			$this->load->view('Base/basedeleteform');
			if($device=='phone') {
				$this->load->view('Base/overlaymobile');
			} else {
				$this->load->view('Base/overlay');
			}
		?>
	</body>
		<?php $this->load->view('Base/bottomscript'); ?>
		<script src='<?php echo base_url();?>js/Laborder/laborder.js' type='text/javascript'></script>		
</html>