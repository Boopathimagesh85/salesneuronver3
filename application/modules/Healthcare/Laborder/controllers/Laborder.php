<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Laborder extends MX_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Laborder/Labordermodel');
		$this->load->helper('formbuild');
	}
	public function index() {
		$moduleid= array(68);
		sessionchecker($moduleid);
		
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp']=$this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp']=$this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle']=$this->Basefunctions->gridtitleinformationfetch($moduleid);
		$viewfieldsmoduleids = array(68);
		$viewmoduleid = array(68);
		$data['moduleids']=$viewmoduleid;
		$data['filedmodids']=$viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Laborder/Laborderview',$data);
	}
	public function newdatacreate() {
		$this->Labordermodel->newdatacreatemodel();
	}
	public function fetchformdataeditdetails() {
		$moduleid=array(68);
		$this->Labordermodel->informationfetchmodel($moduleid);
	}
	public function datainformationupdate() {
		$this->Labordermodel->datainformationupdatemodel();
	}
	public function deleteinformationdata() {
		$moduleid=array(68);
		$this->Labordermodel->deleteoldinformation($moduleid);
	}
}