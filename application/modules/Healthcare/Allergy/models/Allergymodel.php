<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Allergymodel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->model('Base/Basefunctions');
		$this->load->model('Base/Crudmodel');
	}
	public function newdatacreatemodel() {
		$formfieldsname = explode(',',$_POST['allergyelementsname']);
		$formfieldstable = explode(',',$_POST['allergyelementstable']);
		$formfieldscolmname = explode(',',$_POST['allergyelementscolmn']);
		$categoryelementpartable = explode(',',$_POST['allergyelementspartabname']);
		$partablename =  $this->Crudmodel->filtervalue($categoryelementpartable);
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$result = $this->Crudmodel->datainsert($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname);
		echo 'TRUE';
	}
	public function informationfetchmodel($moduleid) {
		$formfieldsname = explode(',',$_GET['elementsname']);
		$formfieldstable = explode(',',$_GET['elementstable']);
		$formfieldscolmname = explode(',',$_GET['elementscolmn']);
		$elementpartable = explode(',',$_GET['elementspartabname']);
		$partablename = $this->Crudmodel->filtervalue($elementpartable);
		$fieldstable = $this->Crudmodel->filtervalue($formfieldstable);
		$primaryid = $_GET['dataprimaryid'];
		$primaryname = $this->Crudmodel->primaryinfo($partablename);
		$result = $this->Crudmodel->dbdatafetch($formfieldsname,$formfieldscolmname,$primaryname,$primaryid,$fieldstable,$partablename,$formfieldstable,$moduleid);
		echo $result;
	}
	public function datainformationupdatemodel() {
		$formfieldsname = explode(',',$_POST['allergyelementsname']);
		$formfieldstable = explode(',',$_POST['allergyelementstable']);
		$formfieldscolmname = explode(',',$_POST['allergyelementscolmn']);
		$categoryelementpartable = explode(',',$_POST['allergyelementspartabname']);
		$partablename =  $this->Crudmodel->filtervalue($categoryelementpartable);
		$fildstable = $this->Crudmodel->filtervalue($formfieldstable);
		$tableinfo = explode(',',$fildstable);
		$primaryid = $_POST['allergyprimarydataid'];
		$result = $this->Crudmodel->dataupdate($partablename,$tableinfo,$formfieldsname,$formfieldstable,$formfieldscolmname,$primaryid);
		echo $result;
	}
	public function deleteoldinformation($moduleid) {
		$formfieldstable = explode(',',$_GET['elementstable']);
		$parenttable = explode(',',$_GET['parenttable']);
		$id = $_GET['primarydataid'];
		$tablename =  $this->Crudmodel->filtervalue($formfieldstable);
		$partabname =  $this->Crudmodel->filtervalue($parenttable);
		$primaryname = $this->Crudmodel->primaryinfo($partabname);
		$ctable=$this->Crudmodel->foreignkey($primaryname,$partabname);
		$ruleid = $this->Basefunctions->moduleruleidfetch($moduleid);
		if($ruleid == 1 || $ruleid == 2) {
			$chek = $this->Basefunctions->checkrecordcreateduser($partabname,$primaryname,$id,$moduleid);
			if($chek == 0) {
				echo 'Denied';
			} else {
				$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
				echo 'TRUE';
			}
		} else {
			$this->Crudmodel->outerdeletefunction($partabname,$primaryname,$ctable,$id);
			echo 'TRUE';
		}
	}
	//allergy name fetch function
	public function allergennamegetmodel() {
		$id = $_GET['typeid'];
		$this->db->select('allergennameid,allergennamename,sortorder',false);
		$this->db->from('allergenname');
		$this->db->where('allergenname.allergytypeid',$id);
		$this->db->where('allergenname.status',1);
		$this->db->order_by('sortorder', 'ASC');
		$result = $this->db->get();
		if($result ->num_rows() > 0) {
			foreach($result->result() as $row) {
				$data[] = array('id'=>$row->allergennameid,'allergenname'=>$row->allergennamename);
			}
			echo json_encode($data);
		} else {
			echo json_encode(array('failed'=>'Failure'));
		}
	}
}