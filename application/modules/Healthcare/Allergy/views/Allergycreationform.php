<div class='row' style='max-width:100%'>
	<div class='off-canvas-wrap' data-offcanvas>
		<div class='inner-wrap'>
			<div class='gridviewdivforsh'>
				<header class='header-content'>
					<?php
						$device = $this->Basefunctions->deviceinfo();
						$comlogo = $this->Basefunctions->companylogodetails();
						$logopath=($comlogo['path']!='') ? ($comlogo['fromid']=='2' ? ( file_exists($comlogo['path']) && is_readable($comlogo['path']) ? base_url().$comlogo['path'] : base_url().'img/logo.png' ) : $comlogo['path'] ) : base_url().'img/logo.png';
						if($device=='phone') {
							echo "<div class='large-6 medium-6 small-6 columns headerpadding'>
								<span class='menu-icon'><i class='left-off-canvas-toggle material-icons'>menu</i></span>
								<span class='grid-logo'><img src='.$logopath.' width='100'></span>
							</div>";
						} else {
							echo "<div class='large-6 medium-6 small-6 columns headerpadding'>
								<span class='menu-icon'><i class='left-off-canvas-toggle material-icons'>menu</i></span>
								<span class='grid-logo'><span class='module-title'>".$gridtitle."</span></span>
							</div>";
						}
					?>
					<div class='large-6 medium-6 small-6 columns text-right mainaction headerpadding'>
					<?php
						$this->load->view('Base/usermenugenerate');
					?>
					</div>
				</header>
				<div class='desktop actionmenucontainer action-menu'>
					<ul class='module-view'>
						<li class='user-img'>
					<span class='paging-box smsgroupgridfootercontainer'></span>
						</li>
					</ul>
					<?php
						if($device=='phone') {
							echo "<ul class='actionicons-view'>
								<li class='mobfilterspan'><i class='icon icon-filter'></i></li>
								<li>
									<nav class='top-bar' data-topbar style='display: inline-block; line-height: 25px; background:none;' role='navigation'>
										<section class='top-bar-section' style='top:-15px; '>
											<ul class='right' style='height: 20px !important; background-color:#dae1e4;'>
												<li class='has-dropdown' style='padding:0;'>
													<span class='action-click'><i class='icon icon-show8'></i></span>";
														$prev = '';
														$m=1;
														foreach($actionassign as $key):
														if($m==1) {
															echo "<ul class='actionoverlay'>";
															$m=2;
														}
														if($key->toolbarname!='') {
															$cur = $key->toolbarcategoryid;
															$fname = preg_replace('/[^A-Za-z0-9]/', '',$key->toolbarname);
															$name = strtolower($fname);
															if($prev!=$cur) {
																if($prev=='') {
																	echo "<li id='.$name.'icon'><i class='icon-w '.$key->description.' '.$name.'iconclass' title='.$key->toolbartitle.'></i></li>";
																} else {
																	echo "<li id=''.$name.'icon' style='border-top:1px solid #dae1e4;'><i class='icon-w '.$key->description.' '.$name.'iconclass' title='.$key->toolbartitle.'></i></li>";
																}
																$prev=$key->toolbarcategoryid;
																} else {
																	echo "<li id=''.$name.'icon'><i class='icon-w '.$key->description.' '.$name.'iconclass' title='.$key->toolbartitle.''></i></li>";
																}
														}
														endforeach;
														if($m==2) {
															echo '</ul>';
														}
											echo '</li>
												<script>
													$(document).ready(function() {
														setTimeout(function(){
															var height=$(window).height();
															height = height-105;';
														echo "$('.actionicons-view .actionoverlay').attr('style', 'height: '+height+'px !important');";
														echo '},10);
													});
												</script>
											</ul>
										</section>
									</nav>
								</li>
							</ul>';
						} else {
						$prev = '';
						$new=0;
						$new1=0;
						$newp = 0;
						echo "<ul class='toggle-view tabaction'>";
						echo "<li class='action-icons'>";
						foreach($actionassign as $key):
						if($key->toolbarname!='') {
							$cur = $key->toolbarcategoryid;
							if($cur == 1){
								if($prev!=$cur) {
									$prev=$key->toolbarcategoryid;
								}
								$fname = preg_replace('/[^A-Za-z0-9]/', '',$key->toolbarname);
								$name = strtolower($fname);
								if($name != 'viewselect' && $name != 'reload'){
									echo '<span class="icon-box"><i id="'.$name.'icon" class="icon24 '.$key->description.' '.$name.'iconclass" title="'.$key->toolbartitle.'"></i></span>'.PHP_EOL;
								}
							}
						}
						endforeach;
						foreach($actionassign as $key):
						if($key->toolbarname!='') {
								$cur = $key->toolbarcategoryid;
								if($cur == 2) {
									$catname = $key->toolbarcategoryname;
									$cname = preg_replace('/[^A-Za-z0-9]/', '',$key->toolbarcategoryname);
									$categoryname = strtolower($cname);
									if($prev!=$cur) {
										if($prev != '') {
											echo "<div class='drop-container advanceddropbox'><span class='icon-box'><i title='' class='material-icons' id=''.$categoryname.'category'>settings_applications</i><ul id=''.$categoryname.'drop' class='action-drop arrow_box'><li Class='action-drophead'><span>'.$catname.'</span></li>";
										}
										$prev=$key->toolbarcategoryid;
									}
									$fname = preg_replace('/[^A-Za-z0-9]/', '',$key->toolbarname);
									$name = strtolower($fname);
									if($prev ==$cur) {
										echo "<li id=''.$name.'icon'><span class='icon-box'><i class='icon24 '.$key->description.' '.$name.'iconclass' title=''.$key->toolbartitle.''></i>'.$key->toolbartitle.'</span></li>".PHP_EOL;
									}
									$newp++;
								}
							}
							endforeach;
							if($newp != 0){
								echo '</ul></span></div>';
							}
							foreach($actionassign as $key):
								if($key->toolbarname!='') {
									$cur = $key->toolbarcategoryid;
									if($cur == 3) {
										$catname = $key->toolbarcategoryname;
										$cname = preg_replace('/[^A-Za-z0-9]/', '',$key->toolbarcategoryname);
										$categoryname = strtolower($cname);
										if($prev!=$cur) {
											if($prev != '') {
												echo "<div class='drop-container communicationdropbox'><span class='icon-box'><i title='' class='icon24 icon-sphere' id=''.$categoryname.'category'></i><ul id=''.$categoryname.'drop' class='action-drop arrow_box'><li Class='action-drophead'><span>'.$catname.'</span></li>";
											}
											$prev=$key->toolbarcategoryid;
										}
										$fname = preg_replace('/[^A-Za-z0-9]/', '',$key->toolbarname);
										$name = strtolower($fname);
										if($prev ==$cur) {
											echo "<li id=''.$name.'icon'><span class='icon-box'><i  class='icon24 '.$key->description.' '.$name.'iconclass' title=''.$key->toolbartitle.''></i>'.$key->toolbartitle.'</span></li>".PHP_EOL;
										}
										$new++;
									}
								}
								endforeach;
								if($new != 0){
									echo '</ul></span></div>';
								}
								foreach($actionassign as $key):
								if($key->toolbarname!='') {
									$cur = $key->toolbarcategoryid;
									if($cur == 4) {
										$catname = $key->toolbarcategoryname;
										$cname = preg_replace('/[^A-Za-z0-9]/', '',$key->toolbarcategoryname);
										$categoryname = strtolower($cname);
										if($prev!=$cur) {
											if($prev != '') {
												echo "<div class='drop-container settingsdropbox'><span class='icon-box'><i title='' class='material-icons' id=''.$categoryname.'category'>settings</i><ul id=''.$categoryname.'drop' class='action-drop arrow_box'><li Class='action-drophead'><span>'.$catname.'</span></li>";
											}
											$prev=$key->toolbarcategoryid;
											}
											$fname = preg_replace('/[^A-Za-z0-9]/', '',$key->toolbarname);
											$name = strtolower($fname);
											if($prev ==$cur) {
												echo "<li id=''.$name.'icon'><span class='icon-box'><i class='icon24 '.$key->description.' '.$name.'iconclass' title=''.$key->toolbartitle.''></i>'.$key->toolbartitle.'</span></li>".PHP_EOL;
											}
												$new1++;
											}
										}
										endforeach;
										if($new1 != 0){
											echo '</ul></span></div>';
										}
										echo "<span class='icon-box' id='viewtoggle' title='Filter'><i class='material-icons'>filter_list</i></span>".PHP_EOL;
										echo '</li></ul>';
									}
								?>
							</div>
							<div class='addformcontainerauditlog'>
							<?php
								$this->load->view('Base/formfieldgeneration');
								formfieldsgeneratorwithgrid($modtabsecfrmfkdsgrp,$filedmodids);
							?>
							</div>
						</div>
						<a class='exit-off-canvas'></a>
					</div>
				</div>
			</div>
		<div>
			<?php $this->load->view('Base/viewselectionoverlay'); ?>
		</div>
