<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Allergy extends MX_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Allergy/Allergymodel');
		$this->load->helper('formbuild');
	}
	public function index() {
		$moduleid= array(59);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp']=$this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp']=$this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle']=$this->Basefunctions->gridtitleinformationfetch($moduleid);
		$viewfieldsmoduleids = array(59);
		$viewmoduleid = array(59);
		$data['moduleids']=$viewmoduleid;
		$data['filedmodids']=$viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Allergy/Allergyview',$data);
	}
	public function newdatacreate() {
		$this->Allergymodel->newdatacreatemodel();
	}
	public function fetchformdataeditdetails() {
		$moduleid=array(59);
		$this->Allergymodel->informationfetchmodel($moduleid);
	}
	public function datainformationupdate() {
		$this->Allergymodel->datainformationupdatemodel();
	}
	public function deleteinformationdata() {
		$moduleid=array(59);
		$this->Allergymodel->deleteoldinformation($moduleid);
	}
	//allergy type based allergy name
	public function allergynameget(){
		$this->Allergymodel->allergennamegetmodel();
	}
}