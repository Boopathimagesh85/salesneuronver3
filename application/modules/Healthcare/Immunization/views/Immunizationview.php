<!DOCTYPE html>
<html lang='en'>
	<head>
		<?php $this->load->view('Base/headerfiles'); ?>
	</head>
	<body >
		<?php
			$device = $this->Basefunctions->deviceinfo();
			$dataset['gridenable'] = 'yes';
			$dataset['griddisplayid'] = 'Immunizationcreationview';
			$dataset['maingridtitle'] =  $gridtitle['title'];
			$dataset['gridtitle'] = $gridtitle['title'];
			$dataset['titleicon'] = $gridtitle['titleicon'];
			$dataset['spanattr'] = array();
			$dataset['gridtableid'] = 'Immunizationaddgrid';
			$dataset['griddivid'] = 'Immunizationaddgridnav';
			$this->load->view('Immunization/Immunizationcreationform',$dataset);
			$this->load->view('Base/basedeleteform');
			if($device=='phone') {
				$this->load->view('Base/overlaymobile');
			} else {
				$this->load->view('Base/overlay');
			}
			$this->load->view('Base/modulelist');
		?>
	</body>
		<?php $this->load->view('Base/bottomscript'); ?>
		<script src='<?php echo base_url();?>js/Immunization/immunization.js' type='text/javascript'></script>		
</html>
