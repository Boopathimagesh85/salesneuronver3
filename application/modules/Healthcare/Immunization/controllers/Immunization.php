<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Immunization extends MX_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Immunization/Immunizationmodel');
		$this->load->helper('formbuild');
	}
	public function index() {
		$moduleid= array(61);
		sessionchecker($moduleid);
		$data['actionassign']=$this->Basefunctions->actionassign($moduleid);
		$data['modtabgrp']=$this->Basefunctions->moduuletabgroupgeneration($moduleid);
		$data['modtabsecfrmfkdsgrp']=$this->Basefunctions->moduuletabgrpsecfieldsgeneration($moduleid);
		$data['gridtitle']=$this->Basefunctions->gridtitleinformationfetch($moduleid);
		$viewfieldsmoduleids = array(61);
		$viewmoduleid = array(61);
		$data['moduleids']=$viewmoduleid;
		$data['filedmodids']=$viewfieldsmoduleids;
		$data['dynamicviewdd']=$this->Basefunctions->dataviewbydropdown($viewmoduleid);
		$data['dataviewcol']=$this->Basefunctions->dataviewdropdowncolumns($viewfieldsmoduleids);
		$this->load->view('Immunization/Immunizationview',$data);
	}
	public function newdatacreate() {
		$this->Immunizationmodel->newdatacreatemodel();
	}
	public function fetchformdataeditdetails() {
		$moduleid=array(61);
		$this->Immunizationmodel->informationfetchmodel($moduleid);
	}
	public function datainformationupdate() {
		$this->Immunizationmodel->datainformationupdatemodel();
	}
	public function deleteinformationdata() {
		$moduleid=array(61);
		$this->Immunizationmodel->deleteoldinformation($moduleid);
	}
}