<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

ERROR - 2020-01-10 10:36:31 --> Query error: Unknown column 'account.accountname' in 'field list' - Invalid query: SELECT SQL_CALC_FOUND_ROWS accountname,(CASE WHEN productname IS NULL and (accountname = "Summary" ) THEN 'Total' WHEN productname IS NULL THEN 'removetotal'  ELSE productname END) as  productname, stockid,  SUMopeningpieces,SUMopeninggrossweight,SUMopeningdiapieces,SUMopeningdiacaratweight,SUMinpieces,SUMingrossweight,SUMindiapieces,SUMindiacaratweight,SUMoutpieces,SUMoutgrossweight,SUMoutdiapieces,SUMoutdiacaratweight,SUMclosingpieces,SUMclosinggrossweight,SUMclosingdiapieces,SUMclosingdiacaratweight from (SELECT  IFNULL(accountname, 'Summary')  as accountname,productname, stockid,   ROUND((SUM(SUMopeningpieces)),0) as SUMopeningpieces, ROUND((SUM(SUMopeninggrossweight)),2) as SUMopeninggrossweight, ROUND((SUM(SUMopeningdiapieces)),2) as SUMopeningdiapieces, ROUND((SUM(SUMopeningdiacaratweight)),2) as SUMopeningdiacaratweight, ROUND((SUM(SUMinpieces)),0) as SUMinpieces, ROUND((SUM(SUMingrossweight)),2) as SUMingrossweight, ROUND((SUM(SUMindiapieces)),2) as SUMindiapieces, ROUND((SUM(SUMindiacaratweight)),2) as SUMindiacaratweight, ROUND((SUM(SUMoutpieces)),0) as SUMoutpieces, ROUND((SUM(SUMoutgrossweight)),2) as SUMoutgrossweight, ROUND((SUM(SUMoutdiapieces)),2) as SUMoutdiapieces, ROUND((SUM(SUMoutdiacaratweight)),2) as SUMoutdiacaratweight, ROUND((SUM(SUMclosingpieces)),0) as SUMclosingpieces, ROUND((SUM(SUMclosinggrossweight)),2) as SUMclosinggrossweight, ROUND((SUM(SUMclosingdiapieces)),2) as SUMclosingdiapieces, ROUND((SUM(SUMclosingdiacaratweight)),2) as SUMclosingdiacaratweight from (select  account.accountname,product.productname, stock.stockid,  ROUND(SUM(stock.openingpieces),0) as SUMopeningpieces,ROUND(SUM(stock.openinggrossweight),2) as SUMopeninggrossweight,ROUND(SUM(stock.openingdiapieces),2) as SUMopeningdiapieces,ROUND(SUM(stock.openingdiacaratweight),2) as SUMopeningdiacaratweight,ROUND(SUM(stock.inpieces),0) as SUMinpieces,ROUND(SUM(stock.ingrossweight),2) as SUMingrossweight,ROUND(SUM(stock.indiapieces),2) as SUMindiapieces,ROUND(SUM(stock.indiacaratweight),2) as SUMindiacaratweight,ROUND(SUM(stock.outpieces),0) as SUMoutpieces,ROUND(SUM(stock.outgrossweight),2) as SUMoutgrossweight,ROUND(SUM(stock.outdiapieces),2) as SUMoutdiapieces,ROUND(SUM(stock.outdiacaratweight),2) as SUMoutdiacaratweight,ROUND(SUM(stock.closingpieces),0) as SUMclosingpieces,ROUND(SUM(stock.closinggrossweight),2) as SUMclosinggrossweight,ROUND(SUM(stock.closingdiapieces),2) as SUMclosingdiapieces,ROUND(SUM(stock.closingdiacaratweight),2) as SUMclosingdiacaratweight  from (select stock.stockid,stock.industryid,stock.stockdate, stock.branchid, stock.accountid, stock.purityid, stock.productid, stock.counterid, stock.stockreporttypeid, openinggrossweight, sum(ingrossweight) as ingrossweight, sum(outgrossweight) as outgrossweight,(sum(openinggrossweight) + sum(ingrossweight) - sum(outgrossweight)  - sum(outpartialgrossweight) ) as closinggrossweight,openingnetweight, sum(innetweight) as innetweight, sum(outnetweight) as outnetweight, (sum(openingnetweight) + sum(innetweight) - sum(outnetweight)  - sum(outpartialnetweight) ) as closingnetweight, openingpieces,sum(inpieces) as inpieces, sum(outpieces) as outpieces, (sum(openingpieces) + sum(inpieces) - sum(outpieces)) as closingpieces , openingitemrate,sum(initemrate) as initemrate, sum(outitemrate) as outitemrate, (sum(openingitemrate) + sum(initemrate) - sum(outitemrate)) as closingitemrate,openingtagweight,sum(intagwt) as intagwt,sum(outtagwt) as outtagwt,(sum(openingtagweight) + sum(intagwt) - sum(outtagwt)) as closingtagweight, openingdiacaratweight, SUM(indiacaratweight) as indiacaratweight, SUM(outdiacaratweight) as outdiacaratweight, (SUM(openingdiacaratweight) + SUM(indiacaratweight) - SUM(outdiacaratweight)) as closingdiacaratweight, openingdiapieces, SUM(indiapieces) as  indiapieces, SUM(outdiapieces) as outdiapieces, (SUM(openingdiapieces) + SUM(indiapieces) - SUM(outdiapieces)) as closingdiapieces, stock.status from (
	select t.industryid,t.primid as stockid,t.stockdate, t.branchid, t.accountid, t.purityid, t.productid, t.counterid, t.stockreporttypeid, sum(t.ingrossweight - t.outgrossweight  - t.outpartialgrossweight ) as openinggrossweight, 0 as ingrossweight, 0 as outgrossweight, 0 as closinggrossweight, sum(t.innetweight - t.outnetweight  - t.outpartialnetweight ) as openingnetweight, 0 as innetweight, 0 as outnetweight, 0 as closingnetweight, sum(t.inpieces - t.outpieces) as openingpieces, 0 as inpieces, 0 as outpieces ,0 as closingpieces,sum(t.initemrate - t.outitemrate) as openingitemrate, 0 as initemrate, 0 as outitemrate ,0 as closingitemrate,0 as outpartialgrossweight,0 as outpartialnetweight,sum(t.intagwt - t.outtagwt) as openingtagweight,0 as intagwt,0 as outtagwt,0 as closingtagweight,SUM(t.indiacaratweight - t.outdiacaratweight) as openingdiacaratweight, 0 as indiacaratweight, 0 as outdiacaratweight, 0 as closingdiacaratweight,SUM(t.indiapieces - t.outdiapieces) as openingdiapieces, 0 as indiapieces, 0 as outdiapieces, 0 as closingdiapieces, 1 as status  from 
	(
	
	select  cummulativestock.industryid,cummulativestockid as primid,cummulativestock.stockdate,cummulativestock.branchid,cummulativestock.accountid,cummulativestock.purityid,cummulativestock.productid,cummulativestock.counterid,cummulativestock.stockreporttypeid ,
	COALESCE(sum(cummulativestock.ingrossweight),0) as ingrossweight,
	COALESCE(sum(cummulativestock.innetweight),0) as innetweight,
	COALESCE(sum(cummulativestock.inpieces),0) as inpieces,
	COALESCE(sum(cummulativestock.initemrate),0) as initemrate,
	COALESCE(sum(cummulativestock.indiacaratweight),0) as indiacaratweight,
	COALESCE(sum(cummulativestock.indiapieces),0) as indiapieces,
	COALESCE(sum(cummulativestock.outgrossweight),0) as outgrossweight,
	COALESCE(sum(cummulativestock.outnetweight),0) as outnetweight,
	COALESCE(sum(cummulativestock.outpieces),0) as outpieces,
	COALESCE(sum(cummulativestock.outitemrate),0) as outitemrate,
	COALESCE(sum(cummulativestock.outdiacaratweight),0) as outdiacaratweight,
	COALESCE(sum(cummulativestock.outdiapieces),0) as outdiapieces,
	0 as outpartialgrossweight,
	0 as outpartialnetweight,
	0 as intagwt,
	0 as outtagwt
	FROM cummulativestock
	WHERE cummulativestock.status not in (0,3)   and cummulativestock.stockdate < "2020-01-10" 
	group by cummulativestock.cummulativestockid
	  
	
	UNION  all
	 
	select  itemtag.industryid,itemtag.itemtagid as primid,itemtag.tagdate as stockdate,itemtag.branchid,itemtag.accountid,itemtag.purityid,
	(CASE WHEN itemtag.tagtypeid in (2,3,4,5,14,16,18,13) THEN productid WHEN itemtag.tagtypeid in (11) THEN itemtag.processproductid ELSE 1 END) as  productid,
	(CASE WHEN itemtag.tagtypeid in (2,4,5,18) THEN itemtag.stockincounterid WHEN itemtag.tagtypeid in (3,11,13,14,16) THEN counterid ELSE 1 END) as  counterid,
	(CASE 
	 WHEN itemtag.tagtypeid in (2) THEN 2 
	 WHEN itemtag.tagtypeid in (4) THEN 4 
	 WHEN itemtag.tagtypeid in (5) THEN 5 
	 WHEN itemtag.tagtypeid in (18) THEN 18
	 WHEN itemtag.tagtypeid in (3,16) THEN 3 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (2) THEN 2 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (5) THEN 5 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (6) THEN 4 
	 WHEN itemtag.tagtypeid = 13 and  itemtag.tagentrytypeid in (2,3,5,6) THEN 14 
	 WHEN itemtag.tagtypeid = 11 and  itemtag.tagentrytypeid in (2,3,5,6) THEN 6 ELSE 1 END) as  stockreporttypeid ,
	 COALESCE(itemtag.grossweight,0) as ingrossweight,
	 COALESCE(itemtag.netweight,0) as innetweight,
	 COALESCE(itemtag.pieces,0) as inpieces,
	 COALESCE(itemtag.itemrate,0) as initemrate,
	 COALESCE(sum(stoneentry.caratweight),0) as indiacaratweight,
	 COALESCE(sum(stoneentry.pieces),0) as indiapieces,
	 0 as outgrossweight,
	 0 as outnetweight,
	 0 as outpieces,
	 0 as outitemrate,
	 0 as outdiacaratweight,
	 0 as outdiapieces,
	 0 as outpartialgrossweight,
	 0 as outpartialnetweight,
	 COALESCE(itemtag.tagweight,0) as intagwt,
	 0 as outtagwt
	 FROM itemtag
	 LEFT OUTER JOIN stoneentry ON stoneentry.itemtagid = itemtag.itemtagid AND stoneentry.stonetypeid = 2
	 WHERE itemtag.status not in (0,3) and  itemtag.tagdate < "2020-01-10" and itemtag.tagtypeid not in (14,16)  
	 group by itemtag.itemtagid
	  	
	 UNION  all
	
	select itemtag.industryid,itemtag.itemtagid as primid,itemtag.tagdate as stockdate,itemtag.branchid,itemtag.accountid,itemtag.purityid,
	(CASE WHEN itemtag.tagentrytypeid in (2,3,5,6,7,13) THEN productid WHEN itemtag.tagentrytypeid in (4,10,11,12) THEN itemtag.processproductid ELSE 1 END) as  productid,
	(CASE WHEN itemtag.tagentrytypeid in (7) THEN itemtag.stockincounterid  ELSE fromcounterid END) as  counterid, 
	(CASE 
	WHEN itemtag.tagentrytypeid = 2  THEN 2
	WHEN itemtag.tagentrytypeid = 7  THEN 18
	WHEN itemtag.tagentrytypeid = 3  THEN 3 
	WHEN itemtag.tagentrytypeid = 4  THEN 16
	WHEN itemtag.tagentrytypeid = 5  THEN 5
	WHEN itemtag.tagentrytypeid = 6  THEN 4
	WHEN itemtag.tagentrytypeid = 10  THEN 7
	WHEN itemtag.tagentrytypeid = 11  THEN 6
	WHEN itemtag.tagentrytypeid = 12  THEN 8
	WHEN itemtag.tagentrytypeid = 13  THEN 14
	ELSE 1 END) as  stockreporttypeid ,
	0 as ingrossweight,
    0 as innetweight,
    0 as inpieces,
	0 as initemrate,
	0 as indiacaratweight,
	0 as indiapieces,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.grossweight,0) END) as outgrossweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.netweight,0) END) as outnetweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.pieces,0) END) as outpieces,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.itemrate,0) END) as outitemrate,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(sum(stoneentry.caratweight),0) END) as outdiacaratweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(sum(stoneentry.pieces),0) END) as outdiapieces,
	0 as outpartialgrossweight,
	0 as outpartialnetweight,
	0 as intagwt,
	COALESCE(itemtag.tagweight,0) as outtagwt
	FROM itemtag
	LEFT OUTER JOIN stoneentry ON stoneentry.itemtagid = itemtag.itemtagid AND stoneentry.stonetypeid = 2
	WHERE itemtag.status not in (0,3) and  itemtag.tagdate < "2020-01-10" and itemtag.tagtypeid not in (14,16)  
	group by itemtag.itemtagid
	  
	 UNION  all

	select sales.industryid,salesdetail.salesdetailid as primid ,sales.salesdate as stockdate,sales.branchid,itemtag.accountid,salesdetail.purityid,
	(CASE WHEN salesdetail.stocktypeid in (17,19,20,24,49,62,63,65,66,80,81) THEN salesdetail.productid WHEN salesdetail.stocktypeid in (13) THEN salesdetail.processproductid ELSE 1 END) as  productid,
	(CASE WHEN salesdetail.stocktypeid in (17,19,20,24,49,65,66,80,81,62,63) THEN salesdetail.counterid WHEN salesdetail.stocktypeid in (13) THEN salesdetail.processcounterid ELSE 1 END) as  counterid,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN 8
	 WHEN salesdetail.stocktypeid in (19) THEN 6
	 WHEN salesdetail.stocktypeid in (20) THEN 7
	 WHEN salesdetail.stocktypeid in (17,80,81) THEN 16
	 WHEN salesdetail.stocktypeid in (24) THEN 9
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 2 THEN 2
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 5 THEN 5
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 4 THEN 4 
	 WHEN salesdetail.stocktypeid in (63) THEN 11
	 WHEN salesdetail.stocktypeid in (66) THEN 3
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (2) THEN 16 
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (3) THEN 6
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid > 10 THEN 3
	 ELSE 1 END) as  stockreporttypeid ,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.grossweight - salesdetail.grossweight,0)   WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.grossweight,0) END) as ingrossweight,
	 (CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.netweight - salesdetail.netweight,0) WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.netweight,0) END) as innetweight,
	 (CASE  WHEN salesdetail.stocktypeid in (13) THEN 1 WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.pieces,0) END) as inpieces,
	 (CASE  WHEN salesdetail.stocktypeid in (20,62,65) THEN COALESCE(itemtag.itemrate,0) ELSE 0 END) as initemrate,
	 0 as indiacaratweight,
	 0 as indiapieces,
	 0 as outgrossweight,
	 0 as outnetweight,
	 0 as outpieces,
	 0 as outitemrate,
	 0 as outdiacaratweight,
	 0 as outdiapieces,
	 0 as outpartialgrossweight,
	 0 as outpartialnetweight,
	 COALESCE(itemtag.tagweight,0) as intagwt,
	 0 as outtagwt
	 
	 FROM salesdetail
	 join sales on sales.salesid = salesdetail.salesid
	 join product on product.productid = salesdetail.productid
	 join itemtag on itemtag.itemtagid = salesdetail.itemtagid
	 join companysetting on companysetting.companysettingid = 1
	 WHERE salesdetail.status not in (0,3) and sales.status not in (0,3) and sales.salestransactiontypeid != 1 and sales.salestransactiontypeid != 16 and salesdetail.stocktypeid in (19,20,17,80,81,24,62,63,65,66,13,49) and  sales.salesdate < "2020-01-10" 
	  group by salesdetail.salesdetailid
	   
	 UNION  all
	 
	select sales.industryid,salesdetail.salesdetailid as primid ,sales.salesdate as stockdate,sales.branchid,itemtag.accountid,salesdetail.purityid,
	(CASE WHEN salesdetail.stocktypeid in (11,12,13,25,62,63,65,66,49,73) THEN salesdetail.productid  ELSE 1 END) as  productid,
	(CASE WHEN salesdetail.stocktypeid in (11,12,13,25,62,63,49,73,65,66) THEN salesdetail.counterid  ELSE 1 END) as  counterid,
	(CASE 
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 2 THEN 2
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 4 THEN 4
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 5 THEN 5
	 WHEN salesdetail.stocktypeid in (12,63) THEN 3 
	 WHEN salesdetail.stocktypeid in (66) THEN 10
	 WHEN salesdetail.stocktypeid in (65) THEN 11
	 WHEN salesdetail.stocktypeid in (25) THEN 9
	 WHEN salesdetail.stocktypeid in (73) THEN 14
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (2) THEN 16 
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (3) THEN 6
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid > 10 THEN 3
	 ELSE 1 END) as  stockreporttypeid ,
	 0 as ingrossweight,
	 0 as innetweight,
	 0 as inpieces,
	 0 as initemrate,
	 0 as indiacaratweight,
	 0 as indiapieces,
	(CASE WHEN salesdetail.stocktypeid in (13) THEN  COALESCE(sum(salesdetail.grossweight),0)   WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0 ELSE  COALESCE(salesdetail.grossweight,0) END) as outgrossweight,
    (CASE  WHEN salesdetail.stocktypeid in (13) THEN  COALESCE(sum(salesdetail.netweight),0)   WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0  ELSE   COALESCE(salesdetail.netweight,0) END) as outnetweight,
    (CASE  WHEN salesdetail.stocktypeid in (13) THEN 1 WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0  ELSE COALESCE(salesdetail.pieces,0) END) as outpieces,
    (CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(itemtag.itemrate,0) ELSE 0 END) as outitemrate,
	(CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(sum(salesstoneentry.caratweight),0) ELSE 0 END) as outdiacaratweight,
	(CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(sum(salesstoneentry.pieces),0) ELSE 0 END) as outdiapieces,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.grossweight - salesdetail.grossweight,0)  ELSE 0 END) as outpartialgrossweight,
	 (CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.netweight - salesdetail.netweight,0) ELSE 0 END) as outpartialnetweight,
	 0 as intagwt,
	 COALESCE(itemtag.tagweight,0) as outtagwt
	 FROM salesdetail
	 join sales on sales.salesid = salesdetail.salesid
	 join itemtag on itemtag.itemtagid = salesdetail.itemtagid
	 join product on product.productid = salesdetail.productid
	 LEFT OUTER JOIN salesstoneentry ON salesstoneentry.salesdetailid = salesdetail.salesdetailid AND stonetypeid = 2
	 WHERE salesdetail.status not in (0,3) and sales.status not in (0,3) and sales.salestransactiontypeid != 1 and sales.salestransactiontypeid != 16 and salesdetail.stocktypeid in (11,13,12,25,62,63,65,66,49,73)  and  sales.salesdate < "2020-01-10"  
	  group by salesdetail.salesdetailid
	   
	 ) as t
	 group by branchid,accountid,purityid,productid,counterid,stockreporttypeid
	 UNION ALL  

	select t.industryid,t.primid as stockid,t.stockdate, t.branchid, t.accountid, t.purityid, t.productid, t.counterid, t.stockreporttypeid, 0 as openinggrossweight, sum(ingrossweight ) as ingrossweight, sum(outgrossweight ) as outgrossweight,0 as closinggrossweight, 0 as openingnetweight, sum(innetweight ) as innetweight, sum(outnetweight ) as outnetweight, 0 as closingnetweight, 0 as openingpieces,sum(inpieces) as inpieces, sum(outpieces) as outpieces, 0 as closingpieces,0 as openingitemrate,sum(initemrate) as initemrate, sum(outitemrate) as outitemrate, 0 as closingitemrate, sum(t.outpartialgrossweight) as outpartialgrossweight ,sum(t.outpartialnetweight) as outpartialnetweight,0 as openingtagweight,sum(t.intagwt) as intagwt,sum(t.outtagwt) as outtagwt,0 as closingtagweight, 0 as openingdiacaratweight, SUM(indiacaratweight) as indiacaratweight, SUM(outdiacaratweight) as outdiacaratweight, 0 as closingdiacaratweight, 0 as openingdiapieces, SUM(indiapieces) as indiapieces, SUM(outdiapieces) as outdiapieces, 0 as closingdiapieces, 1 as status from 
	(
	
	select  cummulativestock.industryid,cummulativestockid as primid,cummulativestock.stockdate,cummulativestock.branchid,cummulativestock.accountid,cummulativestock.purityid,cummulativestock.productid,cummulativestock.counterid,cummulativestock.stockreporttypeid ,
	COALESCE(sum(cummulativestock.ingrossweight),0) as ingrossweight,
	COALESCE(sum(cummulativestock.innetweight),0) as innetweight,
	COALESCE(sum(cummulativestock.inpieces),0) as inpieces,
	COALESCE(sum(cummulativestock.initemrate),0) as initemrate,
	COALESCE(sum(cummulativestock.indiacaratweight),0) as indiacaratweight,
	COALESCE(sum(cummulativestock.indiapieces),0) as indiapieces,
	COALESCE(sum(cummulativestock.outgrossweight),0) as outgrossweight,
	COALESCE(sum(cummulativestock.outnetweight),0) as outnetweight,
	COALESCE(sum(cummulativestock.outpieces),0) as outpieces,
	COALESCE(sum(cummulativestock.outitemrate),0) as outitemrate,
	COALESCE(sum(cummulativestock.outdiacaratweight),0) as outdiacaratweight,
	COALESCE(sum(cummulativestock.outdiapieces),0) as outdiapieces,
	0 as outpartialgrossweight,
	0 as outpartialnetweight,
	0 as intagwt,
	0 as outtagwt
	FROM cummulativestock
	WHERE cummulativestock.status not in (0,3) and cummulativestock.stockdate >="2020-01-10" AND cummulativestock.stockdate <= "2020-01-10" 
	group by cummulativestock.cummulativestockid
	
	UNION  all
	
	select  itemtag.industryid,itemtag.itemtagid as primid,itemtag.tagdate as stockdate,itemtag.branchid,itemtag.accountid,itemtag.purityid,
	(CASE WHEN itemtag.tagtypeid in (2,3,4,5,14,16,18,13) THEN productid WHEN itemtag.tagtypeid in (11) THEN itemtag.processproductid ELSE 1 END) as  productid,
	(CASE WHEN itemtag.tagtypeid in (2,4,5,18) THEN itemtag.stockincounterid WHEN itemtag.tagtypeid in (3,11,13,14,16) THEN counterid ELSE 1 END) as  counterid,
	(CASE 
	 WHEN itemtag.tagtypeid in (2) THEN 2 
	 WHEN itemtag.tagtypeid in (4) THEN 4 
	 WHEN itemtag.tagtypeid in (5) THEN 5 
	 WHEN itemtag.tagtypeid in (18) THEN 18
	 WHEN itemtag.tagtypeid in (3,16) THEN 3 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (2) THEN 2 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (5) THEN 5 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (6) THEN 4 
	 WHEN itemtag.tagtypeid = 13 and  itemtag.tagentrytypeid in (2,3,5,6) THEN 14 
	 WHEN itemtag.tagtypeid = 11 and  itemtag.tagentrytypeid in (2,3,5,6) THEN 6 ELSE 1 END) as  stockreporttypeid ,
	 COALESCE(itemtag.grossweight,0) as ingrossweight,
	 COALESCE(itemtag.netweight,0) as innetweight,
	 COALESCE(itemtag.pieces,0) as inpieces,
	 COALESCE(itemtag.itemrate,0) as initemrate,
	 COALESCE(sum(stoneentry.caratweight),0) as indiacaratweight,
	 COALESCE(sum(stoneentry.pieces),0) as indiapieces,
	 0 as outgrossweight,
	0 as outnetweight,
	0 as outpieces,
	0 as outitemrate,
	0 as outdiacaratweight,
	0 as outdiapieces,
	0 as outpartialgrossweight,
	0 as outpartialnetweight,
	COALESCE(itemtag.tagweight,0) as intagwt,
	0 as outtagwt
	 FROM itemtag
	 LEFT OUTER JOIN stoneentry ON stoneentry.itemtagid = itemtag.itemtagid AND stoneentry.stonetypeid = 2
	 WHERE itemtag.status not in (0,3) and itemtag.tagdate >="2020-01-10" AND itemtag.tagdate <= "2020-01-10" and itemtag.tagtypeid not in (14,16)  
	group by itemtag.itemtagid
		
	 UNION  all
	
	select itemtag.industryid,itemtag.itemtagid as primid,itemtag.tagdate as stockdate,itemtag.branchid,itemtag.accountid,itemtag.purityid,
	(CASE WHEN itemtag.tagentrytypeid in (2,3,5,6,7,13) THEN productid WHEN itemtag.tagentrytypeid in (4,10,11,12) THEN itemtag.processproductid ELSE 1 END) as  productid,
	(CASE WHEN itemtag.tagentrytypeid in (7) THEN itemtag.stockincounterid  ELSE fromcounterid END) as  counterid, 
	(CASE 
	WHEN itemtag.tagentrytypeid = 2  THEN 2
	WHEN itemtag.tagentrytypeid = 7  THEN 18
	WHEN itemtag.tagentrytypeid = 3  THEN 3 
	WHEN itemtag.tagentrytypeid = 4  THEN 16
	WHEN itemtag.tagentrytypeid = 5  THEN 5
	WHEN itemtag.tagentrytypeid = 6  THEN 4
	WHEN itemtag.tagentrytypeid = 10  THEN 7
	WHEN itemtag.tagentrytypeid = 11  THEN 6
	WHEN itemtag.tagentrytypeid = 12  THEN 8
	WHEN itemtag.tagentrytypeid = 13  THEN 14
	ELSE 1 END) as  stockreporttypeid ,
	0 as ingrossweight,
    0 as innetweight,
    0 as inpieces,
	0 as initemrate,
	0 as indiacaratweight,
	0 as indiapieces,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.grossweight,0) END) as outgrossweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.netweight,0) END) as outnetweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.pieces,0) END) as outpieces,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.itemrate,0) END) as outitemrate,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(sum(stoneentry.caratweight),0) END) as outdiacaratweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(sum(stoneentry.pieces),0) END) as outdiapieces,
	0 as outpartialgrossweight,
	0 as outpartialnetweight,
	0 as intagwt,
	COALESCE(itemtag.tagweight,0) as outtagwt
	FROM itemtag
	LEFT OUTER JOIN stoneentry ON stoneentry.itemtagid = itemtag.itemtagid AND stoneentry.stonetypeid = 2
	WHERE itemtag.status not in (0,3) and itemtag.tagdate >="2020-01-10" AND itemtag.tagdate <= "2020-01-10" and itemtag.tagtypeid not in (14,16)  
	group by itemtag.itemtagid
	
	 UNION  all

	select sales.industryid,salesdetail.salesdetailid as primid ,sales.salesdate as stockdate,sales.branchid,itemtag.accountid,salesdetail.purityid,
	(CASE WHEN salesdetail.stocktypeid in (17,19,20,24,49,62,63,65,66,80,81) THEN salesdetail.productid WHEN salesdetail.stocktypeid in (13) THEN salesdetail.processproductid ELSE 1 END) as  productid,
	(CASE WHEN salesdetail.stocktypeid in (17,19,20,24,49,65,66,80,81,62,63) THEN salesdetail.counterid WHEN salesdetail.stocktypeid in (13) THEN salesdetail.processcounterid ELSE 1 END) as  counterid,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN 8
	 WHEN salesdetail.stocktypeid in (19) THEN 6
	 WHEN salesdetail.stocktypeid in (20) THEN 7
	 WHEN salesdetail.stocktypeid in (17,80,81) THEN 16
	 WHEN salesdetail.stocktypeid in (24) THEN 9
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 2 THEN 2
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 5 THEN 5
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 4 THEN 4 
	 WHEN salesdetail.stocktypeid in (63) THEN 11
	 WHEN salesdetail.stocktypeid in (66) THEN 3
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (2) THEN 16 
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (3) THEN 6
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid > 10 THEN 3
	 ELSE 1 END) as  stockreporttypeid ,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.grossweight - salesdetail.grossweight,0)   WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.grossweight,0) END) as ingrossweight,
	 (CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.netweight - salesdetail.netweight,0) WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.netweight,0) END) as innetweight,
	 (CASE  WHEN salesdetail.stocktypeid in (13) THEN 1 WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.pieces,0) END) as inpieces,
	 (CASE  WHEN salesdetail.stocktypeid in (20,62,65) THEN COALESCE(itemtag.itemrate,0) ELSE 0 END) as initemrate,
	 0 as indiacaratweight,
	 0 as indiapieces,
	 0 as outgrossweight,
	 0 as outnetweight,
	 0 as outpieces,
	 0 as outitemrate,
	 0 as outdiacaratweight,
	 0 as outdiapieces,
	 0 as outpartialgrossweight,
	 0 as outpartialnetweight,
	 COALESCE(itemtag.tagweight,0) as intagwt,
	 0 as outtagwt
	 FROM salesdetail
	 join sales on sales.salesid = salesdetail.salesid
	 join itemtag on itemtag.itemtagid = salesdetail.itemtagid
	 join product on product.productid = salesdetail.productid
	 join companysetting on companysetting.companysettingid = 1
	 WHERE salesdetail.status not in (0,3) and sales.status not in (0,3) and sales.salestransactiontypeid != 1 and sales.salestransactiontypeid != 16 and salesdetail.stocktypeid in (19,20,17,80,81,24,62,63,65,66,13,49) and sales.salesdate >="2020-01-10" AND sales.salesdate <= "2020-01-10"  
	 group by salesdetail.salesdetailid
	 
	 UNION  all
	 
	select sales.industryid,salesdetail.salesdetailid as primid ,sales.salesdate as stockdate,sales.branchid,itemtag.accountid,salesdetail.purityid,
	(CASE WHEN salesdetail.stocktypeid in (11,12,13,25,62,63,65,66,49,73) THEN salesdetail.productid  ELSE 1 END) as  productid,
	(CASE WHEN salesdetail.stocktypeid in (11,12,13,25,62,63,49,73,65,66) THEN salesdetail.counterid  ELSE 1 END) as  counterid,
	(CASE 
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 2 THEN 2
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 4 THEN 4
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 5 THEN 5
	 WHEN salesdetail.stocktypeid in (12,63) THEN 3 
	 WHEN salesdetail.stocktypeid in (66) THEN 10
	 WHEN salesdetail.stocktypeid in (65) THEN 11
	 WHEN salesdetail.stocktypeid in (25) THEN 9
	  WHEN salesdetail.stocktypeid in (73) THEN 14
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (2) THEN 16 
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (3) THEN 6
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid > 10 THEN 3
	 ELSE 1 END) as  stockreporttypeid ,
	 0 as ingrossweight,
	 0 as innetweight,
	 0 as inpieces,
	 0 as initemrate,
	 0 as indiacaratweight,
	 0 as indiapieces,
	(CASE WHEN salesdetail.stocktypeid in (13) THEN  COALESCE(sum(salesdetail.grossweight),0)   WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0 ELSE  COALESCE(salesdetail.grossweight,0) END) as outgrossweight,
    (CASE  WHEN salesdetail.stocktypeid in (13) THEN  COALESCE(sum(salesdetail.netweight),0)   WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0  ELSE   COALESCE(salesdetail.netweight,0) END) as outnetweight,
    (CASE  WHEN salesdetail.stocktypeid in (13) THEN 1 WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0  ELSE COALESCE(salesdetail.pieces,0) END) as outpieces,
    (CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(itemtag.itemrate,0) ELSE 0 END) as outitemrate,
	(CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(sum(salesstoneentry.caratweight),0) ELSE 0 END) as outdiacaratweight,
	(CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(sum(salesstoneentry.pieces),0) ELSE 0 END) as outdiapieces,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.grossweight - salesdetail.grossweight,0)  ELSE 0 END) as outpartialgrossweight,
	 (CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.netweight - salesdetail.netweight,0) ELSE 0 END) as outpartialnetweight,
	 0 as intagwt,
	 COALESCE(itemtag.tagweight,0) as outtagwt
	 FROM salesdetail
	 join sales on sales.salesid = salesdetail.salesid
	 join itemtag on itemtag.itemtagid = salesdetail.itemtagid
	 join product on product.productid = salesdetail.productid
	 LEFT OUTER JOIN salesstoneentry ON salesstoneentry.salesdetailid = salesdetail.salesdetailid AND stonetypeid = 2
	 WHERE salesdetail.status not in (0,3) and sales.status not in (0,3) and sales.salestransactiontypeid != 1 and sales.salestransactiontypeid != 16 and salesdetail.stocktypeid in (11,13,12,25,62,63,65,66,49,73)  and sales.salesdate >="2020-01-10" AND sales.salesdate <= "2020-01-10"  
	 group by salesdetail.salesdetailid
	 
	 ) as t
	 group by branchid,accountid,purityid,productid,counterid,stockreporttypeid
	 ) as stock   LEFT OUTER JOIN product ON product.productid=stock.productid  LEFT OUTER JOIN stockreporttype ON stockreporttype.stockreporttypeid=stock.stockreporttypeid   GROUP BY branchid,accountid,purityid,productid,counterid,stockreporttypeid) as stock   LEFT OUTER JOIN product ON product.productid=stock.productid  LEFT OUTER JOIN stockreporttype ON stockreporttype.stockreporttypeid=stock.stockreporttypeid    WHERE stock.status NOT IN (0,3) AND find_in_set(3,stock.industryid ) AND   stockreporttype.stockreporttypeid IN (2)  AND 1=1  group by account.accountname asc,product.productname asc  ) AS t   group by   accountname asc,productname asc  WITH ROLLUP ) AS t having (accountname = "Summary" or (accountname != "Total" ) ) AND (productname != "removetotal" ) 
ERROR - 2020-01-10 10:49:33 --> Query error: Unknown column 'account.accountname' in 'field list' - Invalid query: SELECT SQL_CALC_FOUND_ROWS productname,(CASE WHEN accountname IS NULL and (productname = "Summary" ) THEN 'Total' WHEN accountname IS NULL THEN 'removetotal'  ELSE accountname END) as  accountname, stockid,  SUMopeningpieces,SUMopeninggrossweight,SUMopeningdiapieces,SUMopeningdiacaratweight,SUMinpieces,SUMingrossweight,SUMindiapieces,SUMindiacaratweight,SUMoutpieces,SUMoutgrossweight,SUMoutdiapieces,SUMoutdiacaratweight,SUMclosingpieces,SUMclosinggrossweight,SUMclosingdiapieces,SUMclosingdiacaratweight from (SELECT  IFNULL(productname, 'Summary')  as productname,accountname, stockid,   ROUND((SUM(SUMopeningpieces)),0) as SUMopeningpieces, ROUND((SUM(SUMopeninggrossweight)),2) as SUMopeninggrossweight, ROUND((SUM(SUMopeningdiapieces)),2) as SUMopeningdiapieces, ROUND((SUM(SUMopeningdiacaratweight)),2) as SUMopeningdiacaratweight, ROUND((SUM(SUMinpieces)),0) as SUMinpieces, ROUND((SUM(SUMingrossweight)),2) as SUMingrossweight, ROUND((SUM(SUMindiapieces)),2) as SUMindiapieces, ROUND((SUM(SUMindiacaratweight)),2) as SUMindiacaratweight, ROUND((SUM(SUMoutpieces)),0) as SUMoutpieces, ROUND((SUM(SUMoutgrossweight)),2) as SUMoutgrossweight, ROUND((SUM(SUMoutdiapieces)),2) as SUMoutdiapieces, ROUND((SUM(SUMoutdiacaratweight)),2) as SUMoutdiacaratweight, ROUND((SUM(SUMclosingpieces)),0) as SUMclosingpieces, ROUND((SUM(SUMclosinggrossweight)),2) as SUMclosinggrossweight, ROUND((SUM(SUMclosingdiapieces)),2) as SUMclosingdiapieces, ROUND((SUM(SUMclosingdiacaratweight)),2) as SUMclosingdiacaratweight from (select  product.productname,account.accountname, stock.stockid,  ROUND(SUM(stock.openingpieces),0) as SUMopeningpieces,ROUND(SUM(stock.openinggrossweight),2) as SUMopeninggrossweight,ROUND(SUM(stock.openingdiapieces),2) as SUMopeningdiapieces,ROUND(SUM(stock.openingdiacaratweight),2) as SUMopeningdiacaratweight,ROUND(SUM(stock.inpieces),0) as SUMinpieces,ROUND(SUM(stock.ingrossweight),2) as SUMingrossweight,ROUND(SUM(stock.indiapieces),2) as SUMindiapieces,ROUND(SUM(stock.indiacaratweight),2) as SUMindiacaratweight,ROUND(SUM(stock.outpieces),0) as SUMoutpieces,ROUND(SUM(stock.outgrossweight),2) as SUMoutgrossweight,ROUND(SUM(stock.outdiapieces),2) as SUMoutdiapieces,ROUND(SUM(stock.outdiacaratweight),2) as SUMoutdiacaratweight,ROUND(SUM(stock.closingpieces),0) as SUMclosingpieces,ROUND(SUM(stock.closinggrossweight),2) as SUMclosinggrossweight,ROUND(SUM(stock.closingdiapieces),2) as SUMclosingdiapieces,ROUND(SUM(stock.closingdiacaratweight),2) as SUMclosingdiacaratweight  from (select stock.stockid,stock.industryid,stock.stockdate, stock.branchid, stock.accountid, stock.purityid, stock.productid, stock.counterid, stock.stockreporttypeid, openinggrossweight, sum(ingrossweight) as ingrossweight, sum(outgrossweight) as outgrossweight,(sum(openinggrossweight) + sum(ingrossweight) - sum(outgrossweight)  - sum(outpartialgrossweight) ) as closinggrossweight,openingnetweight, sum(innetweight) as innetweight, sum(outnetweight) as outnetweight, (sum(openingnetweight) + sum(innetweight) - sum(outnetweight)  - sum(outpartialnetweight) ) as closingnetweight, openingpieces,sum(inpieces) as inpieces, sum(outpieces) as outpieces, (sum(openingpieces) + sum(inpieces) - sum(outpieces)) as closingpieces , openingitemrate,sum(initemrate) as initemrate, sum(outitemrate) as outitemrate, (sum(openingitemrate) + sum(initemrate) - sum(outitemrate)) as closingitemrate,openingtagweight,sum(intagwt) as intagwt,sum(outtagwt) as outtagwt,(sum(openingtagweight) + sum(intagwt) - sum(outtagwt)) as closingtagweight, openingdiacaratweight, SUM(indiacaratweight) as indiacaratweight, SUM(outdiacaratweight) as outdiacaratweight, (SUM(openingdiacaratweight) + SUM(indiacaratweight) - SUM(outdiacaratweight)) as closingdiacaratweight, openingdiapieces, SUM(indiapieces) as  indiapieces, SUM(outdiapieces) as outdiapieces, (SUM(openingdiapieces) + SUM(indiapieces) - SUM(outdiapieces)) as closingdiapieces, stock.status from (
	select t.industryid,t.primid as stockid,t.stockdate, t.branchid, t.accountid, t.purityid, t.productid, t.counterid, t.stockreporttypeid, sum(t.ingrossweight - t.outgrossweight  - t.outpartialgrossweight ) as openinggrossweight, 0 as ingrossweight, 0 as outgrossweight, 0 as closinggrossweight, sum(t.innetweight - t.outnetweight  - t.outpartialnetweight ) as openingnetweight, 0 as innetweight, 0 as outnetweight, 0 as closingnetweight, sum(t.inpieces - t.outpieces) as openingpieces, 0 as inpieces, 0 as outpieces ,0 as closingpieces,sum(t.initemrate - t.outitemrate) as openingitemrate, 0 as initemrate, 0 as outitemrate ,0 as closingitemrate,0 as outpartialgrossweight,0 as outpartialnetweight,sum(t.intagwt - t.outtagwt) as openingtagweight,0 as intagwt,0 as outtagwt,0 as closingtagweight,SUM(t.indiacaratweight - t.outdiacaratweight) as openingdiacaratweight, 0 as indiacaratweight, 0 as outdiacaratweight, 0 as closingdiacaratweight,SUM(t.indiapieces - t.outdiapieces) as openingdiapieces, 0 as indiapieces, 0 as outdiapieces, 0 as closingdiapieces, 1 as status  from 
	(
	
	select  cummulativestock.industryid,cummulativestockid as primid,cummulativestock.stockdate,cummulativestock.branchid,cummulativestock.accountid,cummulativestock.purityid,cummulativestock.productid,cummulativestock.counterid,cummulativestock.stockreporttypeid ,
	COALESCE(sum(cummulativestock.ingrossweight),0) as ingrossweight,
	COALESCE(sum(cummulativestock.innetweight),0) as innetweight,
	COALESCE(sum(cummulativestock.inpieces),0) as inpieces,
	COALESCE(sum(cummulativestock.initemrate),0) as initemrate,
	COALESCE(sum(cummulativestock.indiacaratweight),0) as indiacaratweight,
	COALESCE(sum(cummulativestock.indiapieces),0) as indiapieces,
	COALESCE(sum(cummulativestock.outgrossweight),0) as outgrossweight,
	COALESCE(sum(cummulativestock.outnetweight),0) as outnetweight,
	COALESCE(sum(cummulativestock.outpieces),0) as outpieces,
	COALESCE(sum(cummulativestock.outitemrate),0) as outitemrate,
	COALESCE(sum(cummulativestock.outdiacaratweight),0) as outdiacaratweight,
	COALESCE(sum(cummulativestock.outdiapieces),0) as outdiapieces,
	0 as outpartialgrossweight,
	0 as outpartialnetweight,
	0 as intagwt,
	0 as outtagwt
	FROM cummulativestock
	WHERE cummulativestock.status not in (0,3)   and cummulativestock.stockdate < "2020-01-10" 
	group by cummulativestock.cummulativestockid
	  
	
	UNION  all
	 
	select  itemtag.industryid,itemtag.itemtagid as primid,itemtag.tagdate as stockdate,itemtag.branchid,itemtag.accountid,itemtag.purityid,
	(CASE WHEN itemtag.tagtypeid in (2,3,4,5,14,16,18,13) THEN productid WHEN itemtag.tagtypeid in (11) THEN itemtag.processproductid ELSE 1 END) as  productid,
	(CASE WHEN itemtag.tagtypeid in (2,4,5,18) THEN itemtag.stockincounterid WHEN itemtag.tagtypeid in (3,11,13,14,16) THEN counterid ELSE 1 END) as  counterid,
	(CASE 
	 WHEN itemtag.tagtypeid in (2) THEN 2 
	 WHEN itemtag.tagtypeid in (4) THEN 4 
	 WHEN itemtag.tagtypeid in (5) THEN 5 
	 WHEN itemtag.tagtypeid in (18) THEN 18
	 WHEN itemtag.tagtypeid in (3,16) THEN 3 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (2) THEN 2 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (5) THEN 5 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (6) THEN 4 
	 WHEN itemtag.tagtypeid = 13 and  itemtag.tagentrytypeid in (2,3,5,6) THEN 14 
	 WHEN itemtag.tagtypeid = 11 and  itemtag.tagentrytypeid in (2,3,5,6) THEN 6 ELSE 1 END) as  stockreporttypeid ,
	 COALESCE(itemtag.grossweight,0) as ingrossweight,
	 COALESCE(itemtag.netweight,0) as innetweight,
	 COALESCE(itemtag.pieces,0) as inpieces,
	 COALESCE(itemtag.itemrate,0) as initemrate,
	 COALESCE(sum(stoneentry.caratweight),0) as indiacaratweight,
	 COALESCE(sum(stoneentry.pieces),0) as indiapieces,
	 0 as outgrossweight,
	 0 as outnetweight,
	 0 as outpieces,
	 0 as outitemrate,
	 0 as outdiacaratweight,
	 0 as outdiapieces,
	 0 as outpartialgrossweight,
	 0 as outpartialnetweight,
	 COALESCE(itemtag.tagweight,0) as intagwt,
	 0 as outtagwt
	 FROM itemtag
	 LEFT OUTER JOIN account ON account.accountid = itemtag.accountid
	 LEFT OUTER JOIN stoneentry ON stoneentry.itemtagid = itemtag.itemtagid AND stoneentry.stonetypeid = 2
	 WHERE itemtag.status not in (0,3) and  itemtag.tagdate < "2020-01-10" and itemtag.tagtypeid not in (14,16)  
	 group by itemtag.itemtagid
	  	
	 UNION  all
	
	select itemtag.industryid,itemtag.itemtagid as primid,itemtag.tagdate as stockdate,itemtag.branchid,itemtag.accountid,itemtag.purityid,
	(CASE WHEN itemtag.tagentrytypeid in (2,3,5,6,7,13) THEN productid WHEN itemtag.tagentrytypeid in (4,10,11,12) THEN itemtag.processproductid ELSE 1 END) as  productid,
	(CASE WHEN itemtag.tagentrytypeid in (7) THEN itemtag.stockincounterid  ELSE fromcounterid END) as  counterid, 
	(CASE 
	WHEN itemtag.tagentrytypeid = 2  THEN 2
	WHEN itemtag.tagentrytypeid = 7  THEN 18
	WHEN itemtag.tagentrytypeid = 3  THEN 3 
	WHEN itemtag.tagentrytypeid = 4  THEN 16
	WHEN itemtag.tagentrytypeid = 5  THEN 5
	WHEN itemtag.tagentrytypeid = 6  THEN 4
	WHEN itemtag.tagentrytypeid = 10  THEN 7
	WHEN itemtag.tagentrytypeid = 11  THEN 6
	WHEN itemtag.tagentrytypeid = 12  THEN 8
	WHEN itemtag.tagentrytypeid = 13  THEN 14
	ELSE 1 END) as  stockreporttypeid ,
	0 as ingrossweight,
    0 as innetweight,
    0 as inpieces,
	0 as initemrate,
	0 as indiacaratweight,
	0 as indiapieces,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.grossweight,0) END) as outgrossweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.netweight,0) END) as outnetweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.pieces,0) END) as outpieces,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.itemrate,0) END) as outitemrate,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(sum(stoneentry.caratweight),0) END) as outdiacaratweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(sum(stoneentry.pieces),0) END) as outdiapieces,
	0 as outpartialgrossweight,
	0 as outpartialnetweight,
	0 as intagwt,
	COALESCE(itemtag.tagweight,0) as outtagwt
	FROM itemtag
	LEFT OUTER JOIN account ON account.accountid = itemtag.accountid
	LEFT OUTER JOIN stoneentry ON stoneentry.itemtagid = itemtag.itemtagid AND stoneentry.stonetypeid = 2
	WHERE itemtag.status not in (0,3) and  itemtag.tagdate < "2020-01-10" and itemtag.tagtypeid not in (14,16)  
	group by itemtag.itemtagid
	  
	 UNION  all

	select sales.industryid,salesdetail.salesdetailid as primid ,sales.salesdate as stockdate,sales.branchid,itemtag.accountid,salesdetail.purityid,
	(CASE WHEN salesdetail.stocktypeid in (17,19,20,24,49,62,63,65,66,80,81) THEN salesdetail.productid WHEN salesdetail.stocktypeid in (13) THEN salesdetail.processproductid ELSE 1 END) as  productid,
	(CASE WHEN salesdetail.stocktypeid in (17,19,20,24,49,65,66,80,81,62,63) THEN salesdetail.counterid WHEN salesdetail.stocktypeid in (13) THEN salesdetail.processcounterid ELSE 1 END) as  counterid,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN 8
	 WHEN salesdetail.stocktypeid in (19) THEN 6
	 WHEN salesdetail.stocktypeid in (20) THEN 7
	 WHEN salesdetail.stocktypeid in (17,80,81) THEN 16
	 WHEN salesdetail.stocktypeid in (24) THEN 9
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 2 THEN 2
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 5 THEN 5
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 4 THEN 4 
	 WHEN salesdetail.stocktypeid in (63) THEN 11
	 WHEN salesdetail.stocktypeid in (66) THEN 3
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (2) THEN 16 
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (3) THEN 6
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid > 10 THEN 3
	 ELSE 1 END) as  stockreporttypeid ,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.grossweight - salesdetail.grossweight,0)   WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.grossweight,0) END) as ingrossweight,
	 (CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.netweight - salesdetail.netweight,0) WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.netweight,0) END) as innetweight,
	 (CASE  WHEN salesdetail.stocktypeid in (13) THEN 1 WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.pieces,0) END) as inpieces,
	 (CASE  WHEN salesdetail.stocktypeid in (20,62,65) THEN COALESCE(itemtag.itemrate,0) ELSE 0 END) as initemrate,
	 0 as indiacaratweight,
	 0 as indiapieces,
	 0 as outgrossweight,
	 0 as outnetweight,
	 0 as outpieces,
	 0 as outitemrate,
	 0 as outdiacaratweight,
	 0 as outdiapieces,
	 0 as outpartialgrossweight,
	 0 as outpartialnetweight,
	 COALESCE(itemtag.tagweight,0) as intagwt,
	 0 as outtagwt
	 
	 FROM salesdetail
	 join sales on sales.salesid = salesdetail.salesid
	 join product on product.productid = salesdetail.productid
	 join itemtag on itemtag.itemtagid = salesdetail.itemtagid
	 LEFT OUTER JOIN account ON account.accountid = itemtag.accountid
	 join companysetting on companysetting.companysettingid = 1
	 WHERE salesdetail.status not in (0,3) and sales.status not in (0,3) and sales.salestransactiontypeid != 1 and sales.salestransactiontypeid != 16 and salesdetail.stocktypeid in (19,20,17,80,81,24,62,63,65,66,13,49) and  sales.salesdate < "2020-01-10" 
	  group by salesdetail.salesdetailid
	   
	 UNION  all
	 
	select sales.industryid,salesdetail.salesdetailid as primid ,sales.salesdate as stockdate,sales.branchid,itemtag.accountid,salesdetail.purityid,
	(CASE WHEN salesdetail.stocktypeid in (11,12,13,25,62,63,65,66,49,73) THEN salesdetail.productid  ELSE 1 END) as  productid,
	(CASE WHEN salesdetail.stocktypeid in (11,12,13,25,62,63,49,73,65,66) THEN salesdetail.counterid  ELSE 1 END) as  counterid,
	(CASE 
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 2 THEN 2
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 4 THEN 4
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 5 THEN 5
	 WHEN salesdetail.stocktypeid in (12,63) THEN 3 
	 WHEN salesdetail.stocktypeid in (66) THEN 10
	 WHEN salesdetail.stocktypeid in (65) THEN 11
	 WHEN salesdetail.stocktypeid in (25) THEN 9
	 WHEN salesdetail.stocktypeid in (73) THEN 14
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (2) THEN 16 
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (3) THEN 6
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid > 10 THEN 3
	 ELSE 1 END) as  stockreporttypeid ,
	 0 as ingrossweight,
	 0 as innetweight,
	 0 as inpieces,
	 0 as initemrate,
	 0 as indiacaratweight,
	 0 as indiapieces,
	(CASE WHEN salesdetail.stocktypeid in (13) THEN  COALESCE(sum(salesdetail.grossweight),0)   WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0 ELSE  COALESCE(salesdetail.grossweight,0) END) as outgrossweight,
    (CASE  WHEN salesdetail.stocktypeid in (13) THEN  COALESCE(sum(salesdetail.netweight),0)   WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0  ELSE   COALESCE(salesdetail.netweight,0) END) as outnetweight,
    (CASE  WHEN salesdetail.stocktypeid in (13) THEN 1 WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0  ELSE COALESCE(salesdetail.pieces,0) END) as outpieces,
    (CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(itemtag.itemrate,0) ELSE 0 END) as outitemrate,
	(CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(sum(salesstoneentry.caratweight),0) ELSE 0 END) as outdiacaratweight,
	(CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(sum(salesstoneentry.pieces),0) ELSE 0 END) as outdiapieces,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.grossweight - salesdetail.grossweight,0)  ELSE 0 END) as outpartialgrossweight,
	 (CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.netweight - salesdetail.netweight,0) ELSE 0 END) as outpartialnetweight,
	 0 as intagwt,
	 COALESCE(itemtag.tagweight,0) as outtagwt
	 FROM salesdetail
	 join sales on sales.salesid = salesdetail.salesid
	 join itemtag on itemtag.itemtagid = salesdetail.itemtagid
	 join product on product.productid = salesdetail.productid
	 LEFT OUTER JOIN account ON account.accountid = itemtag.accountid
	 LEFT OUTER JOIN salesstoneentry ON salesstoneentry.salesdetailid = salesdetail.salesdetailid AND stonetypeid = 2
	 WHERE salesdetail.status not in (0,3) and sales.status not in (0,3) and sales.salestransactiontypeid != 1 and sales.salestransactiontypeid != 16 and salesdetail.stocktypeid in (11,13,12,25,62,63,65,66,49,73)  and  sales.salesdate < "2020-01-10"  
	  group by salesdetail.salesdetailid
	   
	 ) as t
	 group by branchid,accountid,purityid,productid,counterid,stockreporttypeid
	 UNION ALL  

	select t.industryid,t.primid as stockid,t.stockdate, t.branchid, t.accountid, t.purityid, t.productid, t.counterid, t.stockreporttypeid, 0 as openinggrossweight, sum(ingrossweight ) as ingrossweight, sum(outgrossweight ) as outgrossweight,0 as closinggrossweight, 0 as openingnetweight, sum(innetweight ) as innetweight, sum(outnetweight ) as outnetweight, 0 as closingnetweight, 0 as openingpieces,sum(inpieces) as inpieces, sum(outpieces) as outpieces, 0 as closingpieces,0 as openingitemrate,sum(initemrate) as initemrate, sum(outitemrate) as outitemrate, 0 as closingitemrate, sum(t.outpartialgrossweight) as outpartialgrossweight ,sum(t.outpartialnetweight) as outpartialnetweight,0 as openingtagweight,sum(t.intagwt) as intagwt,sum(t.outtagwt) as outtagwt,0 as closingtagweight, 0 as openingdiacaratweight, SUM(indiacaratweight) as indiacaratweight, SUM(outdiacaratweight) as outdiacaratweight, 0 as closingdiacaratweight, 0 as openingdiapieces, SUM(indiapieces) as indiapieces, SUM(outdiapieces) as outdiapieces, 0 as closingdiapieces, 1 as status from 
	(
	
	select  cummulativestock.industryid,cummulativestockid as primid,cummulativestock.stockdate,cummulativestock.branchid,cummulativestock.accountid,cummulativestock.purityid,cummulativestock.productid,cummulativestock.counterid,cummulativestock.stockreporttypeid ,
	COALESCE(sum(cummulativestock.ingrossweight),0) as ingrossweight,
	COALESCE(sum(cummulativestock.innetweight),0) as innetweight,
	COALESCE(sum(cummulativestock.inpieces),0) as inpieces,
	COALESCE(sum(cummulativestock.initemrate),0) as initemrate,
	COALESCE(sum(cummulativestock.indiacaratweight),0) as indiacaratweight,
	COALESCE(sum(cummulativestock.indiapieces),0) as indiapieces,
	COALESCE(sum(cummulativestock.outgrossweight),0) as outgrossweight,
	COALESCE(sum(cummulativestock.outnetweight),0) as outnetweight,
	COALESCE(sum(cummulativestock.outpieces),0) as outpieces,
	COALESCE(sum(cummulativestock.outitemrate),0) as outitemrate,
	COALESCE(sum(cummulativestock.outdiacaratweight),0) as outdiacaratweight,
	COALESCE(sum(cummulativestock.outdiapieces),0) as outdiapieces,
	0 as outpartialgrossweight,
	0 as outpartialnetweight,
	0 as intagwt,
	0 as outtagwt
	FROM cummulativestock
	WHERE cummulativestock.status not in (0,3) and cummulativestock.stockdate >="2020-01-10" AND cummulativestock.stockdate <= "2020-01-10" 
	group by cummulativestock.cummulativestockid
	
	UNION  all
	
	select  itemtag.industryid,itemtag.itemtagid as primid,itemtag.tagdate as stockdate,itemtag.branchid,itemtag.accountid,itemtag.purityid,
	(CASE WHEN itemtag.tagtypeid in (2,3,4,5,14,16,18,13) THEN productid WHEN itemtag.tagtypeid in (11) THEN itemtag.processproductid ELSE 1 END) as  productid,
	(CASE WHEN itemtag.tagtypeid in (2,4,5,18) THEN itemtag.stockincounterid WHEN itemtag.tagtypeid in (3,11,13,14,16) THEN counterid ELSE 1 END) as  counterid,
	(CASE 
	 WHEN itemtag.tagtypeid in (2) THEN 2 
	 WHEN itemtag.tagtypeid in (4) THEN 4 
	 WHEN itemtag.tagtypeid in (5) THEN 5 
	 WHEN itemtag.tagtypeid in (18) THEN 18
	 WHEN itemtag.tagtypeid in (3,16) THEN 3 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (2) THEN 2 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (5) THEN 5 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (6) THEN 4 
	 WHEN itemtag.tagtypeid = 13 and  itemtag.tagentrytypeid in (2,3,5,6) THEN 14 
	 WHEN itemtag.tagtypeid = 11 and  itemtag.tagentrytypeid in (2,3,5,6) THEN 6 ELSE 1 END) as  stockreporttypeid ,
	 COALESCE(itemtag.grossweight,0) as ingrossweight,
	 COALESCE(itemtag.netweight,0) as innetweight,
	 COALESCE(itemtag.pieces,0) as inpieces,
	 COALESCE(itemtag.itemrate,0) as initemrate,
	 COALESCE(sum(stoneentry.caratweight),0) as indiacaratweight,
	 COALESCE(sum(stoneentry.pieces),0) as indiapieces,
	 0 as outgrossweight,
	0 as outnetweight,
	0 as outpieces,
	0 as outitemrate,
	0 as outdiacaratweight,
	0 as outdiapieces,
	0 as outpartialgrossweight,
	0 as outpartialnetweight,
	COALESCE(itemtag.tagweight,0) as intagwt,
	0 as outtagwt
	 FROM itemtag
	 LEFT OUTER JOIN account ON account.accountid = itemtag.accountid
	 LEFT OUTER JOIN stoneentry ON stoneentry.itemtagid = itemtag.itemtagid AND stoneentry.stonetypeid = 2
	 WHERE itemtag.status not in (0,3) and itemtag.tagdate >="2020-01-10" AND itemtag.tagdate <= "2020-01-10" and itemtag.tagtypeid not in (14,16)  
	group by itemtag.itemtagid
		
	 UNION  all
	
	select itemtag.industryid,itemtag.itemtagid as primid,itemtag.tagdate as stockdate,itemtag.branchid,itemtag.accountid,itemtag.purityid,
	(CASE WHEN itemtag.tagentrytypeid in (2,3,5,6,7,13) THEN productid WHEN itemtag.tagentrytypeid in (4,10,11,12) THEN itemtag.processproductid ELSE 1 END) as  productid,
	(CASE WHEN itemtag.tagentrytypeid in (7) THEN itemtag.stockincounterid  ELSE fromcounterid END) as  counterid, 
	(CASE 
	WHEN itemtag.tagentrytypeid = 2  THEN 2
	WHEN itemtag.tagentrytypeid = 7  THEN 18
	WHEN itemtag.tagentrytypeid = 3  THEN 3 
	WHEN itemtag.tagentrytypeid = 4  THEN 16
	WHEN itemtag.tagentrytypeid = 5  THEN 5
	WHEN itemtag.tagentrytypeid = 6  THEN 4
	WHEN itemtag.tagentrytypeid = 10  THEN 7
	WHEN itemtag.tagentrytypeid = 11  THEN 6
	WHEN itemtag.tagentrytypeid = 12  THEN 8
	WHEN itemtag.tagentrytypeid = 13  THEN 14
	ELSE 1 END) as  stockreporttypeid ,
	0 as ingrossweight,
    0 as innetweight,
    0 as inpieces,
	0 as initemrate,
	0 as indiacaratweight,
	0 as indiapieces,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.grossweight,0) END) as outgrossweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.netweight,0) END) as outnetweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.pieces,0) END) as outpieces,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.itemrate,0) END) as outitemrate,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(sum(stoneentry.caratweight),0) END) as outdiacaratweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(sum(stoneentry.pieces),0) END) as outdiapieces,
	0 as outpartialgrossweight,
	0 as outpartialnetweight,
	0 as intagwt,
	COALESCE(itemtag.tagweight,0) as outtagwt
	FROM itemtag
	LEFT OUTER JOIN account ON account.accountid = itemtag.accountid
	LEFT OUTER JOIN stoneentry ON stoneentry.itemtagid = itemtag.itemtagid AND stoneentry.stonetypeid = 2
	WHERE itemtag.status not in (0,3) and itemtag.tagdate >="2020-01-10" AND itemtag.tagdate <= "2020-01-10" and itemtag.tagtypeid not in (14,16)  
	group by itemtag.itemtagid
	
	 UNION  all

	select sales.industryid,salesdetail.salesdetailid as primid ,sales.salesdate as stockdate,sales.branchid,itemtag.accountid,salesdetail.purityid,
	(CASE WHEN salesdetail.stocktypeid in (17,19,20,24,49,62,63,65,66,80,81) THEN salesdetail.productid WHEN salesdetail.stocktypeid in (13) THEN salesdetail.processproductid ELSE 1 END) as  productid,
	(CASE WHEN salesdetail.stocktypeid in (17,19,20,24,49,65,66,80,81,62,63) THEN salesdetail.counterid WHEN salesdetail.stocktypeid in (13) THEN salesdetail.processcounterid ELSE 1 END) as  counterid,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN 8
	 WHEN salesdetail.stocktypeid in (19) THEN 6
	 WHEN salesdetail.stocktypeid in (20) THEN 7
	 WHEN salesdetail.stocktypeid in (17,80,81) THEN 16
	 WHEN salesdetail.stocktypeid in (24) THEN 9
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 2 THEN 2
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 5 THEN 5
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 4 THEN 4 
	 WHEN salesdetail.stocktypeid in (63) THEN 11
	 WHEN salesdetail.stocktypeid in (66) THEN 3
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (2) THEN 16 
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (3) THEN 6
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid > 10 THEN 3
	 ELSE 1 END) as  stockreporttypeid ,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.grossweight - salesdetail.grossweight,0)   WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.grossweight,0) END) as ingrossweight,
	 (CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.netweight - salesdetail.netweight,0) WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.netweight,0) END) as innetweight,
	 (CASE  WHEN salesdetail.stocktypeid in (13) THEN 1 WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.pieces,0) END) as inpieces,
	 (CASE  WHEN salesdetail.stocktypeid in (20,62,65) THEN COALESCE(itemtag.itemrate,0) ELSE 0 END) as initemrate,
	 0 as indiacaratweight,
	 0 as indiapieces,
	 0 as outgrossweight,
	 0 as outnetweight,
	 0 as outpieces,
	 0 as outitemrate,
	 0 as outdiacaratweight,
	 0 as outdiapieces,
	 0 as outpartialgrossweight,
	 0 as outpartialnetweight,
	 COALESCE(itemtag.tagweight,0) as intagwt,
	 0 as outtagwt
	 FROM salesdetail
	 join sales on sales.salesid = salesdetail.salesid
	 join itemtag on itemtag.itemtagid = salesdetail.itemtagid
	 join product on product.productid = salesdetail.productid
	 join companysetting on companysetting.companysettingid = 1
	 LEFT OUTER JOIN account ON account.accountid = itemtag.accountid
	 WHERE salesdetail.status not in (0,3) and sales.status not in (0,3) and sales.salestransactiontypeid != 1 and sales.salestransactiontypeid != 16 and salesdetail.stocktypeid in (19,20,17,80,81,24,62,63,65,66,13,49) and sales.salesdate >="2020-01-10" AND sales.salesdate <= "2020-01-10"  
	 group by salesdetail.salesdetailid
	 
	 UNION  all
	 
	select sales.industryid,salesdetail.salesdetailid as primid ,sales.salesdate as stockdate,sales.branchid,itemtag.accountid,salesdetail.purityid,
	(CASE WHEN salesdetail.stocktypeid in (11,12,13,25,62,63,65,66,49,73) THEN salesdetail.productid  ELSE 1 END) as  productid,
	(CASE WHEN salesdetail.stocktypeid in (11,12,13,25,62,63,49,73,65,66) THEN salesdetail.counterid  ELSE 1 END) as  counterid,
	(CASE 
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 2 THEN 2
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 4 THEN 4
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 5 THEN 5
	 WHEN salesdetail.stocktypeid in (12,63) THEN 3 
	 WHEN salesdetail.stocktypeid in (66) THEN 10
	 WHEN salesdetail.stocktypeid in (65) THEN 11
	 WHEN salesdetail.stocktypeid in (25) THEN 9
	  WHEN salesdetail.stocktypeid in (73) THEN 14
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (2) THEN 16 
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (3) THEN 6
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid > 10 THEN 3
	 ELSE 1 END) as  stockreporttypeid ,
	 0 as ingrossweight,
	 0 as innetweight,
	 0 as inpieces,
	 0 as initemrate,
	 0 as indiacaratweight,
	 0 as indiapieces,
	(CASE WHEN salesdetail.stocktypeid in (13) THEN  COALESCE(sum(salesdetail.grossweight),0)   WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0 ELSE  COALESCE(salesdetail.grossweight,0) END) as outgrossweight,
    (CASE  WHEN salesdetail.stocktypeid in (13) THEN  COALESCE(sum(salesdetail.netweight),0)   WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0  ELSE   COALESCE(salesdetail.netweight,0) END) as outnetweight,
    (CASE  WHEN salesdetail.stocktypeid in (13) THEN 1 WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0  ELSE COALESCE(salesdetail.pieces,0) END) as outpieces,
    (CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(itemtag.itemrate,0) ELSE 0 END) as outitemrate,
	(CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(sum(salesstoneentry.caratweight),0) ELSE 0 END) as outdiacaratweight,
	(CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(sum(salesstoneentry.pieces),0) ELSE 0 END) as outdiapieces,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.grossweight - salesdetail.grossweight,0)  ELSE 0 END) as outpartialgrossweight,
	 (CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.netweight - salesdetail.netweight,0) ELSE 0 END) as outpartialnetweight,
	 0 as intagwt,
	 COALESCE(itemtag.tagweight,0) as outtagwt
	 FROM salesdetail
	 join sales on sales.salesid = salesdetail.salesid
	 join itemtag on itemtag.itemtagid = salesdetail.itemtagid
	 join product on product.productid = salesdetail.productid
	 LEFT OUTER JOIN account ON account.accountid = itemtag.accountid
	 LEFT OUTER JOIN salesstoneentry ON salesstoneentry.salesdetailid = salesdetail.salesdetailid AND stonetypeid = 2
	 WHERE salesdetail.status not in (0,3) and sales.status not in (0,3) and sales.salestransactiontypeid != 1 and sales.salestransactiontypeid != 16 and salesdetail.stocktypeid in (11,13,12,25,62,63,65,66,49,73)  and sales.salesdate >="2020-01-10" AND sales.salesdate <= "2020-01-10"  
	 group by salesdetail.salesdetailid
	 
	 ) as t
	 group by branchid,accountid,purityid,productid,counterid,stockreporttypeid
	 ) as stock   LEFT OUTER JOIN product ON product.productid=stock.productid  LEFT OUTER JOIN stockreporttype ON stockreporttype.stockreporttypeid=stock.stockreporttypeid   GROUP BY branchid,accountid,purityid,productid,counterid,stockreporttypeid) as stock   LEFT OUTER JOIN product ON product.productid=stock.productid  LEFT OUTER JOIN stockreporttype ON stockreporttype.stockreporttypeid=stock.stockreporttypeid    WHERE stock.status NOT IN (0,3) AND find_in_set(3,stock.industryid ) AND   stockreporttype.stockreporttypeid IN (2)  AND 1=1  group by product.productname asc,account.accountname asc  ) AS t   group by   productname asc,accountname asc  WITH ROLLUP ) AS t having (productname = "Summary" or (productname != "Total" ) ) AND (accountname != "removetotal" ) 
ERROR - 2020-01-10 11:04:08 --> Query error: You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near ') 
	 group by itemtag.itemtagid
	 having stockreporttypeid not in (2,4,5)  	
	 U' at line 62 - Invalid query: SELECT SQL_CALC_FOUND_ROWS accountname,(CASE WHEN productname IS NULL and (accountname = "Summary" ) THEN 'Total' WHEN productname IS NULL THEN 'removetotal'  ELSE productname END) as  productname, stockid,  SUMopeningpieces,SUMopeninggrossweight,SUMopeningdiapieces,SUMopeningdiacaratweight,SUMinpieces,SUMingrossweight,SUMindiapieces,SUMindiacaratweight,SUMoutpieces,SUMoutgrossweight,SUMoutdiapieces,SUMoutdiacaratweight,SUMclosingpieces,SUMclosinggrossweight,SUMclosingdiapieces,SUMclosingdiacaratweight from (SELECT  IFNULL(accountname, 'Summary')  as accountname,productname, stockid,   ROUND((SUM(SUMopeningpieces)),0) as SUMopeningpieces, ROUND((SUM(SUMopeninggrossweight)),2) as SUMopeninggrossweight, ROUND((SUM(SUMopeningdiapieces)),2) as SUMopeningdiapieces, ROUND((SUM(SUMopeningdiacaratweight)),2) as SUMopeningdiacaratweight, ROUND((SUM(SUMinpieces)),0) as SUMinpieces, ROUND((SUM(SUMingrossweight)),2) as SUMingrossweight, ROUND((SUM(SUMindiapieces)),2) as SUMindiapieces, ROUND((SUM(SUMindiacaratweight)),2) as SUMindiacaratweight, ROUND((SUM(SUMoutpieces)),0) as SUMoutpieces, ROUND((SUM(SUMoutgrossweight)),2) as SUMoutgrossweight, ROUND((SUM(SUMoutdiapieces)),2) as SUMoutdiapieces, ROUND((SUM(SUMoutdiacaratweight)),2) as SUMoutdiacaratweight, ROUND((SUM(SUMclosingpieces)),0) as SUMclosingpieces, ROUND((SUM(SUMclosinggrossweight)),2) as SUMclosinggrossweight, ROUND((SUM(SUMclosingdiapieces)),2) as SUMclosingdiapieces, ROUND((SUM(SUMclosingdiacaratweight)),2) as SUMclosingdiacaratweight from (select  account.accountname,product.productname, stock.stockid,  ROUND(SUM(stock.openingpieces),0) as SUMopeningpieces,ROUND(SUM(stock.openinggrossweight),2) as SUMopeninggrossweight,ROUND(SUM(stock.openingdiapieces),2) as SUMopeningdiapieces,ROUND(SUM(stock.openingdiacaratweight),2) as SUMopeningdiacaratweight,ROUND(SUM(stock.inpieces),0) as SUMinpieces,ROUND(SUM(stock.ingrossweight),2) as SUMingrossweight,ROUND(SUM(stock.indiapieces),2) as SUMindiapieces,ROUND(SUM(stock.indiacaratweight),2) as SUMindiacaratweight,ROUND(SUM(stock.outpieces),0) as SUMoutpieces,ROUND(SUM(stock.outgrossweight),2) as SUMoutgrossweight,ROUND(SUM(stock.outdiapieces),2) as SUMoutdiapieces,ROUND(SUM(stock.outdiacaratweight),2) as SUMoutdiacaratweight,ROUND(SUM(stock.closingpieces),0) as SUMclosingpieces,ROUND(SUM(stock.closinggrossweight),2) as SUMclosinggrossweight,ROUND(SUM(stock.closingdiapieces),2) as SUMclosingdiapieces,ROUND(SUM(stock.closingdiacaratweight),2) as SUMclosingdiacaratweight  from (select stock.stockid,stock.industryid,stock.stockdate, stock.branchid, stock.accountid, stock.purityid, stock.productid, stock.counterid, stock.stockreporttypeid, openinggrossweight,openingnetweight, openingpieces,openingitemrate,stock.status from (
	select t.industryid,t.primid as stockid,t.stockdate, t.branchid, t.accountid, t.purityid, t.productid, t.counterid, t.stockreporttypeid, sum(t.ingrossweight - t.outgrossweight  - t.outpartialgrossweight ) as openinggrossweight, 0 as ingrossweight, 0 as outgrossweight, 0 as closinggrossweight, sum(t.innetweight - t.outnetweight  - t.outpartialnetweight ) as openingnetweight, 0 as innetweight, 0 as outnetweight, 0 as closingnetweight, sum(t.inpieces - t.outpieces) as openingpieces, 0 as inpieces, 0 as outpieces ,0 as closingpieces,sum(t.initemrate - t.outitemrate) as openingitemrate, 0 as initemrate, 0 as outitemrate ,0 as closingitemrate,0 as outpartialgrossweight,0 as outpartialnetweight,sum(t.intagwt - t.outtagwt) as openingtagweight,0 as intagwt,0 as outtagwt,0 as closingtagweight,SUM(t.indiacaratweight - t.outdiacaratweight) as openingdiacaratweight, 0 as indiacaratweight, 0 as outdiacaratweight, 0 as closingdiacaratweight,SUM(t.indiapieces - t.outdiapieces) as openingdiapieces, 0 as indiapieces, 0 as outdiapieces, 0 as closingdiapieces, 1 as status  from 
	(
	
	select  cummulativestock.industryid,cummulativestockid as primid,cummulativestock.stockdate,cummulativestock.branchid,cummulativestock.accountid,cummulativestock.purityid,cummulativestock.productid,cummulativestock.counterid,cummulativestock.stockreporttypeid ,
	COALESCE(sum(cummulativestock.ingrossweight),0) as ingrossweight,
	COALESCE(sum(cummulativestock.innetweight),0) as innetweight,
	COALESCE(sum(cummulativestock.inpieces),0) as inpieces,
	COALESCE(sum(cummulativestock.initemrate),0) as initemrate,
	COALESCE(sum(cummulativestock.indiacaratweight),0) as indiacaratweight,
	COALESCE(sum(cummulativestock.indiapieces),0) as indiapieces,
	COALESCE(sum(cummulativestock.outgrossweight),0) as outgrossweight,
	COALESCE(sum(cummulativestock.outnetweight),0) as outnetweight,
	COALESCE(sum(cummulativestock.outpieces),0) as outpieces,
	COALESCE(sum(cummulativestock.outitemrate),0) as outitemrate,
	COALESCE(sum(cummulativestock.outdiacaratweight),0) as outdiacaratweight,
	COALESCE(sum(cummulativestock.outdiapieces),0) as outdiapieces,
	0 as outpartialgrossweight,
	0 as outpartialnetweight,
	0 as intagwt,
	0 as outtagwt
	FROM cummulativestock
	WHERE cummulativestock.status not in (0,3)   
	group by cummulativestock.cummulativestockid
	 having stockreporttypeid not in (2,4,5)  
	
	UNION  all
	 
	select  itemtag.industryid,itemtag.itemtagid as primid,itemtag.tagdate as stockdate,itemtag.branchid,itemtag.accountid,itemtag.purityid,
	(CASE WHEN itemtag.tagtypeid in (2,3,4,5,14,16,18,13) THEN productid WHEN itemtag.tagtypeid in (11) THEN itemtag.processproductid ELSE 1 END) as  productid,
	(CASE WHEN itemtag.tagtypeid in (2,4,5,18) THEN itemtag.stockincounterid WHEN itemtag.tagtypeid in (3,11,13,14,16) THEN counterid ELSE 1 END) as  counterid,
	(CASE 
	 WHEN itemtag.tagtypeid in (2) THEN 2 
	 WHEN itemtag.tagtypeid in (4) THEN 4 
	 WHEN itemtag.tagtypeid in (5) THEN 5 
	 WHEN itemtag.tagtypeid in (18) THEN 18
	 WHEN itemtag.tagtypeid in (3,16) THEN 3 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (2) THEN 2 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (5) THEN 5 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (6) THEN 4 
	 WHEN itemtag.tagtypeid = 13 and  itemtag.tagentrytypeid in (2,3,5,6) THEN 14 
	 WHEN itemtag.tagtypeid = 11 and  itemtag.tagentrytypeid in (2,3,5,6) THEN 6 ELSE 1 END) as  stockreporttypeid ,
	 COALESCE(itemtag.grossweight,0) as ingrossweight,
	 COALESCE(itemtag.netweight,0) as innetweight,
	 COALESCE(itemtag.pieces,0) as inpieces,
	 COALESCE(itemtag.itemrate,0) as initemrate,
	 COALESCE(sum(stoneentry.caratweight),0) as indiacaratweight,
	 COALESCE(sum(stoneentry.pieces),0) as indiapieces,
	 0 as outgrossweight,
	 0 as outnetweight,
	 0 as outpieces,
	 0 as outitemrate,
	 0 as outdiacaratweight,
	 0 as outdiapieces,
	 0 as outpartialgrossweight,
	 0 as outpartialnetweight,
	 COALESCE(itemtag.tagweight,0) as intagwt,
	 0 as outtagwt
	 FROM itemtag
	 LEFT OUTER JOIN account ON account.accountid = itemtag.accountid
	 LEFT OUTER JOIN stoneentry ON stoneentry.itemtagid = itemtag.itemtagid AND stoneentry.stonetypeid = 2
	 WHERE itemtag.status not in (0,3) and  itemtag.itemtagid in () 
	 group by itemtag.itemtagid
	 having stockreporttypeid not in (2,4,5)  	
	 UNION  all
	
	select itemtag.industryid,itemtag.itemtagid as primid,itemtag.tagdate as stockdate,itemtag.branchid,itemtag.accountid,itemtag.purityid,
	(CASE WHEN itemtag.tagentrytypeid in (2,3,5,6,7,13) THEN productid WHEN itemtag.tagentrytypeid in (4,10,11,12) THEN itemtag.processproductid ELSE 1 END) as  productid,
	(CASE WHEN itemtag.tagentrytypeid in (7) THEN itemtag.stockincounterid  ELSE fromcounterid END) as  counterid, 
	(CASE 
	WHEN itemtag.tagentrytypeid = 2  THEN 2
	WHEN itemtag.tagentrytypeid = 7  THEN 18
	WHEN itemtag.tagentrytypeid = 3  THEN 3 
	WHEN itemtag.tagentrytypeid = 4  THEN 16
	WHEN itemtag.tagentrytypeid = 5  THEN 5
	WHEN itemtag.tagentrytypeid = 6  THEN 4
	WHEN itemtag.tagentrytypeid = 10  THEN 7
	WHEN itemtag.tagentrytypeid = 11  THEN 6
	WHEN itemtag.tagentrytypeid = 12  THEN 8
	WHEN itemtag.tagentrytypeid = 13  THEN 14
	ELSE 1 END) as  stockreporttypeid ,
	0 as ingrossweight,
    0 as innetweight,
    0 as inpieces,
	0 as initemrate,
	0 as indiacaratweight,
	0 as indiapieces,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.grossweight,0) END) as outgrossweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.netweight,0) END) as outnetweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.pieces,0) END) as outpieces,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.itemrate,0) END) as outitemrate,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(sum(stoneentry.caratweight),0) END) as outdiacaratweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(sum(stoneentry.pieces),0) END) as outdiapieces,
	0 as outpartialgrossweight,
	0 as outpartialnetweight,
	0 as intagwt,
	COALESCE(itemtag.tagweight,0) as outtagwt
	FROM itemtag
	LEFT OUTER JOIN account ON account.accountid = itemtag.accountid
	LEFT OUTER JOIN stoneentry ON stoneentry.itemtagid = itemtag.itemtagid AND stoneentry.stonetypeid = 2
	WHERE itemtag.status not in (0,3) and  itemtag.itemtagid in () 
	group by itemtag.itemtagid
	 having stockreporttypeid not in (2,4,5)  
	 UNION  all

	select sales.industryid,salesdetail.salesdetailid as primid ,sales.salesdate as stockdate,sales.branchid,itemtag.accountid,salesdetail.purityid,
	(CASE WHEN salesdetail.stocktypeid in (17,19,20,24,49,62,63,65,66,80,81) THEN salesdetail.productid WHEN salesdetail.stocktypeid in (13) THEN salesdetail.processproductid ELSE 1 END) as  productid,
	(CASE WHEN salesdetail.stocktypeid in (17,19,20,24,49,65,66,80,81,62,63) THEN salesdetail.counterid WHEN salesdetail.stocktypeid in (13) THEN salesdetail.processcounterid ELSE 1 END) as  counterid,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN 8
	 WHEN salesdetail.stocktypeid in (19) THEN 6
	 WHEN salesdetail.stocktypeid in (20) THEN 7
	 WHEN salesdetail.stocktypeid in (17,80,81) THEN 16
	 WHEN salesdetail.stocktypeid in (24) THEN 9
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 2 THEN 2
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 5 THEN 5
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 4 THEN 4 
	 WHEN salesdetail.stocktypeid in (63) THEN 11
	 WHEN salesdetail.stocktypeid in (66) THEN 3
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (2) THEN 16 
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (3) THEN 6
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid > 10 THEN 3
	 ELSE 1 END) as  stockreporttypeid ,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.grossweight - salesdetail.grossweight,0)   WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.grossweight,0) END) as ingrossweight,
	 (CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.netweight - salesdetail.netweight,0) WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.netweight,0) END) as innetweight,
	 (CASE  WHEN salesdetail.stocktypeid in (13) THEN 1 WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.pieces,0) END) as inpieces,
	 (CASE  WHEN salesdetail.stocktypeid in (20,62,65) THEN COALESCE(itemtag.itemrate,0) ELSE 0 END) as initemrate,
	 0 as indiacaratweight,
	 0 as indiapieces,
	 0 as outgrossweight,
	 0 as outnetweight,
	 0 as outpieces,
	 0 as outitemrate,
	 0 as outdiacaratweight,
	 0 as outdiapieces,
	 0 as outpartialgrossweight,
	 0 as outpartialnetweight,
	 COALESCE(itemtag.tagweight,0) as intagwt,
	 0 as outtagwt
	 
	 FROM salesdetail
	 join sales on sales.salesid = salesdetail.salesid
	 join product on product.productid = salesdetail.productid
	 join itemtag on itemtag.itemtagid = salesdetail.itemtagid
	 LEFT OUTER JOIN account ON account.accountid = itemtag.accountid
	 join companysetting on companysetting.companysettingid = 1
	 WHERE salesdetail.status not in (0,3) and sales.status not in (0,3) and sales.salestransactiontypeid != 1 and sales.salestransactiontypeid != 16 and salesdetail.stocktypeid in (19,20,17,80,81,24,62,63,65,66,13,49) and  sales.salesid in () 
	  group by salesdetail.salesdetailid
	  having stockreporttypeid not in (2,4,5)  
	 UNION  all
	 
	select sales.industryid,salesdetail.salesdetailid as primid ,sales.salesdate as stockdate,sales.branchid,itemtag.accountid,salesdetail.purityid,
	(CASE WHEN salesdetail.stocktypeid in (11,12,13,25,62,63,65,66,49,73) THEN salesdetail.productid  ELSE 1 END) as  productid,
	(CASE WHEN salesdetail.stocktypeid in (11,12,13,25,62,63,49,73,65,66) THEN salesdetail.counterid  ELSE 1 END) as  counterid,
	(CASE 
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 2 THEN 2
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 4 THEN 4
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 5 THEN 5
	 WHEN salesdetail.stocktypeid in (12,63) THEN 3 
	 WHEN salesdetail.stocktypeid in (66) THEN 10
	 WHEN salesdetail.stocktypeid in (65) THEN 11
	 WHEN salesdetail.stocktypeid in (25) THEN 9
	 WHEN salesdetail.stocktypeid in (73) THEN 14
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (2) THEN 16 
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (3) THEN 6
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid > 10 THEN 3
	 ELSE 1 END) as  stockreporttypeid ,
	 0 as ingrossweight,
	 0 as innetweight,
	 0 as inpieces,
	 0 as initemrate,
	 0 as indiacaratweight,
	 0 as indiapieces,
	(CASE WHEN salesdetail.stocktypeid in (13) THEN  COALESCE(sum(salesdetail.grossweight),0)   WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0 ELSE  COALESCE(salesdetail.grossweight,0) END) as outgrossweight,
    (CASE  WHEN salesdetail.stocktypeid in (13) THEN  COALESCE(sum(salesdetail.netweight),0)   WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0  ELSE   COALESCE(salesdetail.netweight,0) END) as outnetweight,
    (CASE  WHEN salesdetail.stocktypeid in (13) THEN 1 WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0  ELSE COALESCE(salesdetail.pieces,0) END) as outpieces,
    (CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(itemtag.itemrate,0) ELSE 0 END) as outitemrate,
	(CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(sum(salesstoneentry.caratweight),0) ELSE 0 END) as outdiacaratweight,
	(CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(sum(salesstoneentry.pieces),0) ELSE 0 END) as outdiapieces,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.grossweight - salesdetail.grossweight,0)  ELSE 0 END) as outpartialgrossweight,
	 (CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.netweight - salesdetail.netweight,0) ELSE 0 END) as outpartialnetweight,
	 0 as intagwt,
	 COALESCE(itemtag.tagweight,0) as outtagwt
	 FROM salesdetail
	 join sales on sales.salesid = salesdetail.salesid
	 join itemtag on itemtag.itemtagid = salesdetail.itemtagid
	 join product on product.productid = salesdetail.productid
	 LEFT OUTER JOIN account ON account.accountid = itemtag.accountid
	 LEFT OUTER JOIN salesstoneentry ON salesstoneentry.salesdetailid = salesdetail.salesdetailid AND stonetypeid = 2
	 WHERE salesdetail.status not in (0,3) and sales.status not in (0,3) and sales.salestransactiontypeid != 1 and sales.salestransactiontypeid != 16 and salesdetail.stocktypeid in (11,13,12,25,62,63,65,66,49,73)  and  sales.salesid in ()  
	  group by salesdetail.salesdetailid
	  having stockreporttypeid not in (2,4,5)  
	 ) as t
	 group by branchid,accountid,purityid,productid,counterid,stockreporttypeid
	) as stock   LEFT OUTER JOIN product ON product.productid=stock.productid  LEFT OUTER JOIN stockreporttype ON stockreporttype.stockreporttypeid=stock.stockreporttypeid   LEFT OUTER JOIN account ON account.accountid=stock.accountid GROUP BY branchid,accountid,purityid,productid,counterid,stockreporttypeid) as stock   LEFT OUTER JOIN product ON product.productid=stock.productid  LEFT OUTER JOIN stockreporttype ON stockreporttype.stockreporttypeid=stock.stockreporttypeid   LEFT OUTER JOIN account ON account.accountid=stock.accountid  WHERE stock.status NOT IN (0,3) AND find_in_set(3,stock.industryid ) AND   stockreporttype.stockreporttypeid IN (2)  AND 1=1  group by account.accountname asc,product.productname asc  ) AS t   group by   accountname asc,productname asc  WITH ROLLUP ) AS t having (accountname = "Summary" or (accountname != "Total" ) ) AND (productname != "removetotal" ) 
ERROR - 2020-01-10 06:42:11 --> Severity: error --> Exception: syntax error, unexpected '' WHEN itemtag.accountid NOT I' (T_CONSTANT_ENCAPSED_STRING) D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11655
ERROR - 2020-01-10 06:42:12 --> Severity: error --> Exception: syntax error, unexpected '' WHEN itemtag.accountid NOT I' (T_CONSTANT_ENCAPSED_STRING) D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11655
ERROR - 2020-01-10 06:42:12 --> Severity: error --> Exception: syntax error, unexpected '' WHEN itemtag.accountid NOT I' (T_CONSTANT_ENCAPSED_STRING) D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11655
ERROR - 2020-01-10 06:42:13 --> Severity: error --> Exception: syntax error, unexpected '' WHEN itemtag.accountid NOT I' (T_CONSTANT_ENCAPSED_STRING) D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11655
ERROR - 2020-01-10 06:42:13 --> Severity: error --> Exception: syntax error, unexpected '' WHEN itemtag.accountid NOT I' (T_CONSTANT_ENCAPSED_STRING) D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11655
ERROR - 2020-01-10 06:42:13 --> Severity: error --> Exception: syntax error, unexpected '' WHEN itemtag.accountid NOT I' (T_CONSTANT_ENCAPSED_STRING) D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11655
ERROR - 2020-01-10 06:44:43 --> Severity: error --> Exception: syntax error, unexpected '' END) as accountid,itemtag.pu' (T_CONSTANT_ENCAPSED_STRING) D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11687
ERROR - 2020-01-10 06:44:43 --> Severity: error --> Exception: syntax error, unexpected '' END) as accountid,itemtag.pu' (T_CONSTANT_ENCAPSED_STRING) D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11687
ERROR - 2020-01-10 06:44:44 --> Severity: error --> Exception: syntax error, unexpected '' END) as accountid,itemtag.pu' (T_CONSTANT_ENCAPSED_STRING) D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11687
ERROR - 2020-01-10 06:46:02 --> Severity: error --> Exception: syntax error, unexpected '' END) as accountid,itemtag.pu' (T_CONSTANT_ENCAPSED_STRING) D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11687
ERROR - 2020-01-10 06:46:02 --> Severity: error --> Exception: syntax error, unexpected '' END) as accountid,itemtag.pu' (T_CONSTANT_ENCAPSED_STRING) D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11687
ERROR - 2020-01-10 06:46:02 --> Severity: error --> Exception: syntax error, unexpected '' END) as accountid,itemtag.pu' (T_CONSTANT_ENCAPSED_STRING) D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11687
ERROR - 2020-01-10 06:48:22 --> Severity: error --> Exception: syntax error, unexpected '' END) as accountid,salesdetai' (T_CONSTANT_ENCAPSED_STRING) D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11774
ERROR - 2020-01-10 06:48:22 --> Severity: error --> Exception: syntax error, unexpected '' END) as accountid,salesdetai' (T_CONSTANT_ENCAPSED_STRING) D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11774
ERROR - 2020-01-10 06:48:22 --> Severity: error --> Exception: syntax error, unexpected '' END) as accountid,salesdetai' (T_CONSTANT_ENCAPSED_STRING) D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11774
ERROR - 2020-01-10 11:51:51 --> Severity: Notice --> Trying to get property 'whereclauseid' of non-object D:\xampp\htdocs\salesneuronver3\application\modules\Reportsview\models\Reportsmodel.php 2431
ERROR - 2020-01-10 11:51:51 --> Severity: Notice --> Trying to get property 'aggregatemethodid' of non-object D:\xampp\htdocs\salesneuronver3\application\modules\Reportsview\models\Reportsmodel.php 2452
ERROR - 2020-01-10 12:23:15 --> Severity: Notice --> Undefined variable: reportconditiondataid D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11242
ERROR - 2020-01-10 12:23:15 --> Severity: error --> Exception: Too few arguments to function Basefunctions::newgenerateconditioninfo(), 2 passed in D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php on line 11250 and exactly 3 expected D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 12218
ERROR - 2020-01-10 12:24:10 --> Severity: error --> Exception: Too few arguments to function Basefunctions::newgenerateconditioninfo(), 2 passed in D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php on line 11250 and exactly 3 expected D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 12218
ERROR - 2020-01-10 12:29:16 --> Severity: Notice --> Trying to get property 'whereclauseid' of non-object D:\xampp\htdocs\salesneuronver3\application\modules\Reportsview\models\Reportsmodel.php 2431
ERROR - 2020-01-10 12:29:16 --> Severity: Notice --> Trying to get property 'aggregatemethodid' of non-object D:\xampp\htdocs\salesneuronver3\application\modules\Reportsview\models\Reportsmodel.php 2452
ERROR - 2020-01-10 12:40:57 --> Query error: Unknown column '1viewcreationcolumns.status' in 'where clause' - Invalid query: SELECT viewcreationcolumns.viewcreationcolumnid, viewcreationcolumns.viewcreationcolmodeljointable, viewcreationcolumns.viewcreationcolmodelname, viewcreationcolumns.viewcreationcolmodelindexname, viewcreationjoincolmodelname, uitypeid
FROM `viewcreationcolumns`
WHERE `viewcreationcolumns`.`viewcreationcolumnid` IN(4198)
AND `1viewcreationcolumns`.`status` IN(1)
ERROR - 2020-01-10 12:43:10 --> Query error: Unknown column '1viewcreationcolumns.status' in 'where clause' - Invalid query: SELECT viewcreationcolumns.viewcreationcolumnid, viewcreationcolumns.viewcreationcolmodeljointable, viewcreationcolumns.viewcreationcolmodelname, viewcreationcolumns.viewcreationcolmodelindexname, viewcreationjoincolmodelname, uitypeid
FROM `viewcreationcolumns`
WHERE `viewcreationcolumns`.`viewcreationcolumnid` IN(4198)
AND `1viewcreationcolumns`.`status` IN(1)
ERROR - 2020-01-10 13:38:07 --> Severity: Notice --> Undefined offset: 1 D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11546
ERROR - 2020-01-10 09:08:45 --> Severity: Compile Error --> Cannot use isset() on the result of an expression (you can use "null !== expression" instead) D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11546
ERROR - 2020-01-10 09:08:45 --> Severity: Compile Error --> Cannot use isset() on the result of an expression (you can use "null !== expression" instead) D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11546
ERROR - 2020-01-10 09:08:45 --> Severity: Compile Error --> Cannot use isset() on the result of an expression (you can use "null !== expression" instead) D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11546
ERROR - 2020-01-10 09:09:30 --> Severity: Compile Error --> Cannot use isset() on the result of an expression (you can use "null !== expression" instead) D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11547
ERROR - 2020-01-10 09:09:31 --> Severity: Compile Error --> Cannot use isset() on the result of an expression (you can use "null !== expression" instead) D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11547
ERROR - 2020-01-10 09:09:31 --> Severity: Compile Error --> Cannot use isset() on the result of an expression (you can use "null !== expression" instead) D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11547
ERROR - 2020-01-10 09:09:45 --> Severity: Compile Error --> Cannot use isset() on the result of an expression (you can use "null !== expression" instead) D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11547
ERROR - 2020-01-10 09:09:45 --> Severity: Compile Error --> Cannot use isset() on the result of an expression (you can use "null !== expression" instead) D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11547
ERROR - 2020-01-10 09:09:45 --> Severity: Compile Error --> Cannot use isset() on the result of an expression (you can use "null !== expression" instead) D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11547
ERROR - 2020-01-10 13:40:11 --> Severity: Notice --> Undefined offset: 1 D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11546
ERROR - 2020-01-10 09:10:39 --> Severity: error --> Exception: syntax error, unexpected ';', expecting ',' or ')' D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11546
ERROR - 2020-01-10 09:10:39 --> Severity: error --> Exception: syntax error, unexpected ';', expecting ',' or ')' D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11546
ERROR - 2020-01-10 09:10:39 --> Severity: error --> Exception: syntax error, unexpected ';', expecting ',' or ')' D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11546
ERROR - 2020-01-10 13:41:23 --> Severity: Notice --> Undefined offset: 1 D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11547
ERROR - 2020-01-10 13:44:04 --> Severity: Warning --> print_r() expects at least 1 parameter, 0 given D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11544
ERROR - 2020-01-10 14:59:25 --> Severity: Notice --> Undefined index: summaryrollup D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11465
ERROR - 2020-01-10 14:59:25 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11509
ERROR - 2020-01-10 14:59:26 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11509
ERROR - 2020-01-10 15:00:08 --> Severity: Notice --> Undefined index: summaryrollup D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11465
ERROR - 2020-01-10 15:00:08 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11509
ERROR - 2020-01-10 15:00:08 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11509
ERROR - 2020-01-10 15:10:41 --> Severity: Notice --> Undefined index: summaryrollup D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11465
ERROR - 2020-01-10 15:10:41 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11509
ERROR - 2020-01-10 15:10:41 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11509
ERROR - 2020-01-10 15:14:32 --> Severity: Notice --> Undefined index: summaryrollup D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11465
ERROR - 2020-01-10 15:14:32 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11509
ERROR - 2020-01-10 15:14:32 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11509
ERROR - 2020-01-10 10:47:29 --> Severity: error --> Exception: syntax error, unexpected ')' D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11193
ERROR - 2020-01-10 10:47:29 --> Severity: error --> Exception: syntax error, unexpected ')' D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11193
ERROR - 2020-01-10 10:47:30 --> Severity: error --> Exception: syntax error, unexpected ')' D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11193
ERROR - 2020-01-10 10:47:30 --> Severity: error --> Exception: syntax error, unexpected ')' D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11193
ERROR - 2020-01-10 10:47:31 --> Severity: error --> Exception: syntax error, unexpected ')' D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11193
ERROR - 2020-01-10 15:18:02 --> Severity: Notice --> Array to string conversion D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11201
ERROR - 2020-01-10 15:20:30 --> Severity: Notice --> Array to string conversion D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11201
ERROR - 2020-01-10 15:23:32 --> Severity: Notice --> Undefined index: summaryrollup D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11478
ERROR - 2020-01-10 15:23:32 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11522
ERROR - 2020-01-10 15:23:32 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11522
ERROR - 2020-01-10 15:36:17 --> Severity: Notice --> Undefined index: summaryrollup D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11465
ERROR - 2020-01-10 15:36:17 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11509
ERROR - 2020-01-10 15:36:17 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11509
ERROR - 2020-01-10 15:37:58 --> Severity: Notice --> Undefined index: summaryrollup D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11465
ERROR - 2020-01-10 15:37:58 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11509
ERROR - 2020-01-10 15:37:58 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11509
ERROR - 2020-01-10 15:38:39 --> Severity: Notice --> Undefined index: summaryrollup D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11465
ERROR - 2020-01-10 15:38:39 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11509
ERROR - 2020-01-10 15:38:39 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11509
ERROR - 2020-01-10 15:39:35 --> Severity: Notice --> Undefined index: summaryrollup D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11465
ERROR - 2020-01-10 15:39:35 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11509
ERROR - 2020-01-10 15:39:35 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11509
ERROR - 2020-01-10 15:39:48 --> Severity: Notice --> Undefined index: summaryrollup D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11465
ERROR - 2020-01-10 15:39:48 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11509
ERROR - 2020-01-10 15:39:48 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11509
ERROR - 2020-01-10 15:48:07 --> Severity: Notice --> Undefined index: summaryrollup D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11465
ERROR - 2020-01-10 15:48:07 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11509
ERROR - 2020-01-10 15:48:07 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11509
ERROR - 2020-01-10 15:48:30 --> Severity: Notice --> Undefined index: summaryrollup D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11465
ERROR - 2020-01-10 15:48:30 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11509
ERROR - 2020-01-10 15:48:30 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11509
ERROR - 2020-01-10 15:50:08 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11509
ERROR - 2020-01-10 15:50:08 --> Severity: Notice --> Undefined index: reportingmode D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 11509
ERROR - 2020-01-10 15:51:03 --> Severity: Warning --> Division by zero D:\xampp\htdocs\salesneuronver3\application\modules\Base\models\Basefunctions.php 2061
ERROR - 2020-01-10 16:45:05 --> Query error: FUNCTION aug6th_finaljeweldb.Average does not exist - Invalid query: SELECT SQL_CALC_FOUND_ROWS purityname,(CASE WHEN productname IS NULL and (purityname = "Summary" ) THEN 'Total' WHEN productname IS NULL THEN 'removetotal'  ELSE productname END) as  productname, stockid,  Averageclosingnetweight,SUMclosingnetweight,MINclosingnetweight,COUNTclosingpieces,SUMclosingpieces,MINclosingpieces from (SELECT  IFNULL(purityname, 'Summary')  as purityname,productname, stockid,   ROUND((Average(Averageclosingnetweight)),2) as Averageclosingnetweight, ROUND((SUM(SUMclosingnetweight)),2) as SUMclosingnetweight, ROUND((MIN(MINclosingnetweight)),2) as MINclosingnetweight, ROUND((COUNT(COUNTclosingpieces)),0) as COUNTclosingpieces, ROUND((SUM(SUMclosingpieces)),0) as SUMclosingpieces, ROUND((MIN(MINclosingpieces)),0) as MINclosingpieces from (select  purity.purityname,product.productname, stock.stockid,  ROUND(AVG(stock.closingnetweight),2) as Averageclosingnetweight,ROUND(SUM(stock.closingnetweight),2) as SUMclosingnetweight,ROUND(MIN(stock.closingnetweight),2) as MINclosingnetweight,ROUND(COUNT(stock.closingpieces),0) as COUNTclosingpieces,ROUND(SUM(stock.closingpieces),0) as SUMclosingpieces,ROUND(MIN(stock.closingpieces),0) as MINclosingpieces  from stock   LEFT OUTER JOIN purity ON purity.purityid=stock.purityid  LEFT OUTER JOIN product ON product.productid=stock.productid  LEFT OUTER JOIN stockreporttype ON stockreporttype.stockreporttypeid=stock.stockreporttypeid   WHERE stock.status NOT IN (0,3) AND find_in_set(3,stock.industryid ) AND   stockreporttype.stockreporttypeid IN (2,3)  AND 1=1 AND 1=1   group by purity.purityname asc,product.productname asc  ) AS t   group by   purityname asc,productname asc  WITH ROLLUP ) AS t having (purityname = "Summary" or (purityname != "Total" ) ) AND (productname != "removetotal" ) 
ERROR - 2020-01-10 16:45:17 --> Query error: FUNCTION aug6th_finaljeweldb.Average does not exist - Invalid query: SELECT SQL_CALC_FOUND_ROWS purityname,(CASE WHEN productname IS NULL and (purityname = "Summary" ) THEN 'Total' WHEN productname IS NULL THEN 'removetotal'  ELSE productname END) as  productname, stockid,  Averageclosingnetweight,SUMclosingnetweight,MINclosingnetweight,COUNTclosingpieces,SUMclosingpieces,MINclosingpieces from (SELECT  IFNULL(purityname, 'Summary')  as purityname,productname, stockid,   ROUND((Average(Averageclosingnetweight)),2) as Averageclosingnetweight, ROUND((SUM(SUMclosingnetweight)),2) as SUMclosingnetweight, ROUND((MIN(MINclosingnetweight)),2) as MINclosingnetweight, ROUND((COUNT(COUNTclosingpieces)),0) as COUNTclosingpieces, ROUND((SUM(SUMclosingpieces)),0) as SUMclosingpieces, ROUND((MIN(MINclosingpieces)),0) as MINclosingpieces from (select  purity.purityname,product.productname, stock.stockid,  ROUND(AVG(stock.closingnetweight),2) as Averageclosingnetweight,ROUND(SUM(stock.closingnetweight),2) as SUMclosingnetweight,ROUND(MIN(stock.closingnetweight),2) as MINclosingnetweight,ROUND(COUNT(stock.closingpieces),0) as COUNTclosingpieces,ROUND(SUM(stock.closingpieces),0) as SUMclosingpieces,ROUND(MIN(stock.closingpieces),0) as MINclosingpieces  from (select stock.stockid,stock.industryid,stock.stockdate, stock.branchid, stock.accountid, stock.purityid, stock.productid, stock.counterid, stock.stockreporttypeid, openinggrossweight, sum(ingrossweight) as ingrossweight, sum(outgrossweight) as outgrossweight,(sum(openinggrossweight) + sum(ingrossweight) - sum(outgrossweight)  - sum(outpartialgrossweight) ) as closinggrossweight,openingnetweight, sum(innetweight) as innetweight, sum(outnetweight) as outnetweight, (sum(openingnetweight) + sum(innetweight) - sum(outnetweight)  - sum(outpartialnetweight) ) as closingnetweight, openingpieces,sum(inpieces) as inpieces, sum(outpieces) as outpieces, (sum(openingpieces) + sum(inpieces) - sum(outpieces)) as closingpieces , openingitemrate,sum(initemrate) as initemrate, sum(outitemrate) as outitemrate, (sum(openingitemrate) + sum(initemrate) - sum(outitemrate)) as closingitemrate,openingtagweight,sum(intagwt) as intagwt,sum(outtagwt) as outtagwt,(sum(openingtagweight) + sum(intagwt) - sum(outtagwt)) as closingtagweight, openingdiacaratweight, SUM(indiacaratweight) as indiacaratweight, SUM(outdiacaratweight) as outdiacaratweight, (SUM(openingdiacaratweight) + SUM(indiacaratweight) - SUM(outdiacaratweight)) as closingdiacaratweight, openingdiapieces, SUM(indiapieces) as  indiapieces, SUM(outdiapieces) as outdiapieces, (SUM(openingdiapieces) + SUM(indiapieces) - SUM(outdiapieces)) as closingdiapieces, stock.status from (
	select t.industryid,t.primid as stockid,t.stockdate, t.branchid, t.accountid, t.purityid, t.productid, t.counterid, t.stockreporttypeid, sum(t.ingrossweight - t.outgrossweight  - t.outpartialgrossweight ) as openinggrossweight, 0 as ingrossweight, 0 as outgrossweight, 0 as closinggrossweight, sum(t.innetweight - t.outnetweight  - t.outpartialnetweight ) as openingnetweight, 0 as innetweight, 0 as outnetweight, 0 as closingnetweight, sum(t.inpieces - t.outpieces) as openingpieces, 0 as inpieces, 0 as outpieces ,0 as closingpieces,sum(t.initemrate - t.outitemrate) as openingitemrate, 0 as initemrate, 0 as outitemrate ,0 as closingitemrate,0 as outpartialgrossweight,0 as outpartialnetweight,sum(t.intagwt - t.outtagwt) as openingtagweight,0 as intagwt,0 as outtagwt,0 as closingtagweight,SUM(t.indiacaratweight - t.outdiacaratweight) as openingdiacaratweight, 0 as indiacaratweight, 0 as outdiacaratweight, 0 as closingdiacaratweight,SUM(t.indiapieces - t.outdiapieces) as openingdiapieces, 0 as indiapieces, 0 as outdiapieces, 0 as closingdiapieces, 1 as status  from 
	(
	
	select  cummulativestock.industryid,cummulativestockid as primid,cummulativestock.stockdate,cummulativestock.branchid,cummulativestock.accountid,cummulativestock.purityid,cummulativestock.productid,cummulativestock.counterid,cummulativestock.stockreporttypeid ,
	COALESCE(sum(cummulativestock.ingrossweight),0) as ingrossweight,
	COALESCE(sum(cummulativestock.innetweight),0) as innetweight,
	COALESCE(sum(cummulativestock.inpieces),0) as inpieces,
	COALESCE(sum(cummulativestock.initemrate),0) as initemrate,
	COALESCE(sum(cummulativestock.indiacaratweight),0) as indiacaratweight,
	COALESCE(sum(cummulativestock.indiapieces),0) as indiapieces,
	COALESCE(sum(cummulativestock.outgrossweight),0) as outgrossweight,
	COALESCE(sum(cummulativestock.outnetweight),0) as outnetweight,
	COALESCE(sum(cummulativestock.outpieces),0) as outpieces,
	COALESCE(sum(cummulativestock.outitemrate),0) as outitemrate,
	COALESCE(sum(cummulativestock.outdiacaratweight),0) as outdiacaratweight,
	COALESCE(sum(cummulativestock.outdiapieces),0) as outdiapieces,
	0 as outpartialgrossweight,
	0 as outpartialnetweight,
	0 as intagwt,
	0 as outtagwt
	FROM cummulativestock
	WHERE cummulativestock.status not in (0,3)   and cummulativestock.stockdate < "2020-01-10" 
	group by cummulativestock.cummulativestockid
	  
	
	UNION  all
	 
	select  itemtag.industryid,itemtag.itemtagid as primid,itemtag.tagdate as stockdate,itemtag.branchid,
	(CASE WHEN itemtag.accountid NOT IN (1) THEN itemtag.accountid ELSE "" END) as accountid,itemtag.purityid,
	(CASE WHEN itemtag.tagtypeid in (2,3,4,5,14,16,18,13) THEN productid WHEN itemtag.tagtypeid in (11) THEN itemtag.processproductid ELSE 1 END) as  productid,
	(CASE WHEN itemtag.tagtypeid in (2,4,5,18) THEN itemtag.stockincounterid WHEN itemtag.tagtypeid in (3,11,13,14,16) THEN counterid ELSE 1 END) as  counterid,
	(CASE 
	 WHEN itemtag.tagtypeid in (2) THEN 2 
	 WHEN itemtag.tagtypeid in (4) THEN 4 
	 WHEN itemtag.tagtypeid in (5) THEN 5 
	 WHEN itemtag.tagtypeid in (18) THEN 18
	 WHEN itemtag.tagtypeid in (3,16) THEN 3 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (2) THEN 2 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (5) THEN 5 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (6) THEN 4 
	 WHEN itemtag.tagtypeid = 13 and  itemtag.tagentrytypeid in (2,3,5,6) THEN 14 
	 WHEN itemtag.tagtypeid = 11 and  itemtag.tagentrytypeid in (2,3,5,6) THEN 6 ELSE 1 END) as  stockreporttypeid ,
	 COALESCE(itemtag.grossweight,0) as ingrossweight,
	 COALESCE(itemtag.netweight,0) as innetweight,
	 COALESCE(itemtag.pieces,0) as inpieces,
	 COALESCE(itemtag.itemrate,0) as initemrate,
	 COALESCE(sum(stoneentry.caratweight),0) as indiacaratweight,
	 COALESCE(sum(stoneentry.pieces),0) as indiapieces,
	 0 as outgrossweight,
	 0 as outnetweight,
	 0 as outpieces,
	 0 as outitemrate,
	 0 as outdiacaratweight,
	 0 as outdiapieces,
	 0 as outpartialgrossweight,
	 0 as outpartialnetweight,
	 COALESCE(itemtag.tagweight,0) as intagwt,
	 0 as outtagwt
	 FROM itemtag
	 LEFT OUTER JOIN stoneentry ON stoneentry.itemtagid = itemtag.itemtagid AND stoneentry.stonetypeid = 2
	 WHERE itemtag.status not in (0,3) and  itemtag.tagdate < "2020-01-10" and itemtag.tagtypeid not in (14,16)  
	 group by itemtag.itemtagid
	  	
	 UNION  all
	
	select itemtag.industryid,itemtag.itemtagid as primid,itemtag.tagdate as stockdate,itemtag.branchid,
	(CASE WHEN itemtag.accountid NOT IN (1) THEN itemtag.accountid ELSE "" END) as accountid,itemtag.purityid,
	(CASE WHEN itemtag.tagentrytypeid in (2,3,5,6,7,13) THEN productid WHEN itemtag.tagentrytypeid in (4,10,11,12) THEN itemtag.processproductid ELSE 1 END) as  productid,
	(CASE WHEN itemtag.tagentrytypeid in (7) THEN itemtag.stockincounterid  ELSE fromcounterid END) as  counterid, 
	(CASE 
	WHEN itemtag.tagentrytypeid = 2  THEN 2
	WHEN itemtag.tagentrytypeid = 7  THEN 18
	WHEN itemtag.tagentrytypeid = 3  THEN 3 
	WHEN itemtag.tagentrytypeid = 4  THEN 16
	WHEN itemtag.tagentrytypeid = 5  THEN 5
	WHEN itemtag.tagentrytypeid = 6  THEN 4
	WHEN itemtag.tagentrytypeid = 10  THEN 7
	WHEN itemtag.tagentrytypeid = 11  THEN 6
	WHEN itemtag.tagentrytypeid = 12  THEN 8
	WHEN itemtag.tagentrytypeid = 13  THEN 14
	ELSE 1 END) as  stockreporttypeid ,
	0 as ingrossweight,
    0 as innetweight,
    0 as inpieces,
	0 as initemrate,
	0 as indiacaratweight,
	0 as indiapieces,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.grossweight,0) END) as outgrossweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.netweight,0) END) as outnetweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.pieces,0) END) as outpieces,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.itemrate,0) END) as outitemrate,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(sum(stoneentry.caratweight),0) END) as outdiacaratweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(sum(stoneentry.pieces),0) END) as outdiapieces,
	0 as outpartialgrossweight,
	0 as outpartialnetweight,
	0 as intagwt,
	COALESCE(itemtag.tagweight,0) as outtagwt
	FROM itemtag
	LEFT OUTER JOIN stoneentry ON stoneentry.itemtagid = itemtag.itemtagid AND stoneentry.stonetypeid = 2
	WHERE itemtag.status not in (0,3) and  itemtag.tagdate < "2020-01-10" and itemtag.tagtypeid not in (14,16)  
	group by itemtag.itemtagid
	  
	 UNION  all

	select sales.industryid,salesdetail.salesdetailid as primid ,sales.salesdate as stockdate,sales.branchid,
	(CASE WHEN itemtag.accountid NOT IN (1) THEN itemtag.accountid ELSE "" END) as accountid,salesdetail.purityid,
	(CASE WHEN salesdetail.stocktypeid in (17,19,20,24,49,62,63,65,66,80,81) THEN salesdetail.productid WHEN salesdetail.stocktypeid in (13) THEN salesdetail.processproductid ELSE 1 END) as  productid,
	(CASE WHEN salesdetail.stocktypeid in (17,19,20,24,49,65,66,80,81,62,63) THEN salesdetail.counterid WHEN salesdetail.stocktypeid in (13) THEN salesdetail.processcounterid ELSE 1 END) as  counterid,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN 8
	 WHEN salesdetail.stocktypeid in (19) THEN 6
	 WHEN salesdetail.stocktypeid in (20) THEN 7
	 WHEN salesdetail.stocktypeid in (17,80,81) THEN 16
	 WHEN salesdetail.stocktypeid in (24) THEN 9
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 2 THEN 2
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 5 THEN 5
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 4 THEN 4 
	 WHEN salesdetail.stocktypeid in (63) THEN 11
	 WHEN salesdetail.stocktypeid in (66) THEN 3
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (2) THEN 16 
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (3) THEN 6
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid > 10 THEN 3
	 ELSE 1 END) as  stockreporttypeid ,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.grossweight - salesdetail.grossweight,0)   WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.grossweight,0) END) as ingrossweight,
	 (CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.netweight - salesdetail.netweight,0) WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.netweight,0) END) as innetweight,
	 (CASE  WHEN salesdetail.stocktypeid in (13) THEN 1 WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.pieces,0) END) as inpieces,
	 (CASE  WHEN salesdetail.stocktypeid in (20,62,65) THEN COALESCE(itemtag.itemrate,0) ELSE 0 END) as initemrate,
	 0 as indiacaratweight,
	 0 as indiapieces,
	 0 as outgrossweight,
	 0 as outnetweight,
	 0 as outpieces,
	 0 as outitemrate,
	 0 as outdiacaratweight,
	 0 as outdiapieces,
	 0 as outpartialgrossweight,
	 0 as outpartialnetweight,
	 COALESCE(itemtag.tagweight,0) as intagwt,
	 0 as outtagwt
	 
	 FROM salesdetail
	 join sales on sales.salesid = salesdetail.salesid
	 join product on product.productid = salesdetail.productid
	 join itemtag on itemtag.itemtagid = salesdetail.itemtagid
	 join companysetting on companysetting.companysettingid = 1
	 WHERE salesdetail.status not in (0,3) and sales.status not in (0,3) and sales.salestransactiontypeid != 1 and sales.salestransactiontypeid != 16 and salesdetail.stocktypeid in (19,20,17,80,81,24,62,63,65,66,13,49) and  sales.salesdate < "2020-01-10" 
	  group by salesdetail.salesdetailid
	   
	 UNION  all
	 
	select sales.industryid,salesdetail.salesdetailid as primid ,sales.salesdate as stockdate,sales.branchid,
	(CASE WHEN itemtag.accountid NOT IN (1) THEN itemtag.accountid ELSE "" END) as accountid,salesdetail.purityid,
	(CASE WHEN salesdetail.stocktypeid in (11,12,13,25,62,63,65,66,49,73) THEN salesdetail.productid  ELSE 1 END) as  productid,
	(CASE WHEN salesdetail.stocktypeid in (11,12,13,25,62,63,49,73,65,66) THEN salesdetail.counterid  ELSE 1 END) as  counterid,
	(CASE 
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 2 THEN 2
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 4 THEN 4
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 5 THEN 5
	 WHEN salesdetail.stocktypeid in (12,63) THEN 3 
	 WHEN salesdetail.stocktypeid in (66) THEN 10
	 WHEN salesdetail.stocktypeid in (65) THEN 11
	 WHEN salesdetail.stocktypeid in (25) THEN 9
	 WHEN salesdetail.stocktypeid in (73) THEN 14
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (2) THEN 16 
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (3) THEN 6
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid > 10 THEN 3
	 ELSE 1 END) as  stockreporttypeid ,
	 0 as ingrossweight,
	 0 as innetweight,
	 0 as inpieces,
	 0 as initemrate,
	 0 as indiacaratweight,
	 0 as indiapieces,
	(CASE WHEN salesdetail.stocktypeid in (13) THEN  COALESCE(sum(salesdetail.grossweight),0)   WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0 ELSE  COALESCE(salesdetail.grossweight,0) END) as outgrossweight,
    (CASE  WHEN salesdetail.stocktypeid in (13) THEN  COALESCE(sum(salesdetail.netweight),0)   WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0  ELSE   COALESCE(salesdetail.netweight,0) END) as outnetweight,
    (CASE  WHEN salesdetail.stocktypeid in (13) THEN 1 WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0  ELSE COALESCE(salesdetail.pieces,0) END) as outpieces,
    (CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(itemtag.itemrate,0) ELSE 0 END) as outitemrate,
	(CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(sum(salesstoneentry.caratweight),0) ELSE 0 END) as outdiacaratweight,
	(CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(sum(salesstoneentry.pieces),0) ELSE 0 END) as outdiapieces,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.grossweight - salesdetail.grossweight,0)  ELSE 0 END) as outpartialgrossweight,
	 (CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.netweight - salesdetail.netweight,0) ELSE 0 END) as outpartialnetweight,
	 0 as intagwt,
	 COALESCE(itemtag.tagweight,0) as outtagwt
	 FROM salesdetail
	 join sales on sales.salesid = salesdetail.salesid
	 join itemtag on itemtag.itemtagid = salesdetail.itemtagid
	 join product on product.productid = salesdetail.productid
	 LEFT OUTER JOIN salesstoneentry ON salesstoneentry.salesdetailid = salesdetail.salesdetailid AND stonetypeid = 2
	 WHERE salesdetail.status not in (0,3) and sales.status not in (0,3) and sales.salestransactiontypeid != 1 and sales.salestransactiontypeid != 16 and salesdetail.stocktypeid in (11,13,12,25,62,63,65,66,49,73)  and  sales.salesdate < "2020-01-10"  
	  group by salesdetail.salesdetailid
	   
	 ) as t
	 group by branchid,accountid,purityid,productid,counterid,stockreporttypeid
	 UNION ALL  

	select t.industryid,t.primid as stockid,t.stockdate, t.branchid, t.accountid, t.purityid, t.productid, t.counterid, t.stockreporttypeid, 0 as openinggrossweight, sum(ingrossweight ) as ingrossweight, sum(outgrossweight ) as outgrossweight,0 as closinggrossweight, 0 as openingnetweight, sum(innetweight ) as innetweight, sum(outnetweight ) as outnetweight, 0 as closingnetweight, 0 as openingpieces,sum(inpieces) as inpieces, sum(outpieces) as outpieces, 0 as closingpieces,0 as openingitemrate,sum(initemrate) as initemrate, sum(outitemrate) as outitemrate, 0 as closingitemrate, sum(t.outpartialgrossweight) as outpartialgrossweight ,sum(t.outpartialnetweight) as outpartialnetweight,0 as openingtagweight,sum(t.intagwt) as intagwt,sum(t.outtagwt) as outtagwt,0 as closingtagweight, 0 as openingdiacaratweight, SUM(indiacaratweight) as indiacaratweight, SUM(outdiacaratweight) as outdiacaratweight, 0 as closingdiacaratweight, 0 as openingdiapieces, SUM(indiapieces) as indiapieces, SUM(outdiapieces) as outdiapieces, 0 as closingdiapieces, 1 as status from 
	(
	
	select  cummulativestock.industryid,cummulativestockid as primid,cummulativestock.stockdate,cummulativestock.branchid,cummulativestock.accountid,cummulativestock.purityid,cummulativestock.productid,cummulativestock.counterid,cummulativestock.stockreporttypeid ,
	COALESCE(sum(cummulativestock.ingrossweight),0) as ingrossweight,
	COALESCE(sum(cummulativestock.innetweight),0) as innetweight,
	COALESCE(sum(cummulativestock.inpieces),0) as inpieces,
	COALESCE(sum(cummulativestock.initemrate),0) as initemrate,
	COALESCE(sum(cummulativestock.indiacaratweight),0) as indiacaratweight,
	COALESCE(sum(cummulativestock.indiapieces),0) as indiapieces,
	COALESCE(sum(cummulativestock.outgrossweight),0) as outgrossweight,
	COALESCE(sum(cummulativestock.outnetweight),0) as outnetweight,
	COALESCE(sum(cummulativestock.outpieces),0) as outpieces,
	COALESCE(sum(cummulativestock.outitemrate),0) as outitemrate,
	COALESCE(sum(cummulativestock.outdiacaratweight),0) as outdiacaratweight,
	COALESCE(sum(cummulativestock.outdiapieces),0) as outdiapieces,
	0 as outpartialgrossweight,
	0 as outpartialnetweight,
	0 as intagwt,
	0 as outtagwt
	FROM cummulativestock
	WHERE cummulativestock.status not in (0,3) and cummulativestock.stockdate >="2020-01-10" AND cummulativestock.stockdate <= "2020-01-10" 
	group by cummulativestock.cummulativestockid
	
	UNION  all
	
	select  itemtag.industryid,itemtag.itemtagid as primid,itemtag.tagdate as stockdate,itemtag.branchid,
	(CASE WHEN itemtag.accountid NOT IN (1) THEN itemtag.accountid ELSE "" END) as accountid,itemtag.purityid,
	(CASE WHEN itemtag.tagtypeid in (2,3,4,5,14,16,18,13) THEN productid WHEN itemtag.tagtypeid in (11) THEN itemtag.processproductid ELSE 1 END) as  productid,
	(CASE WHEN itemtag.tagtypeid in (2,4,5,18) THEN itemtag.stockincounterid WHEN itemtag.tagtypeid in (3,11,13,14,16) THEN counterid ELSE 1 END) as  counterid,
	(CASE 
	 WHEN itemtag.tagtypeid in (2) THEN 2 
	 WHEN itemtag.tagtypeid in (4) THEN 4 
	 WHEN itemtag.tagtypeid in (5) THEN 5 
	 WHEN itemtag.tagtypeid in (18) THEN 18
	 WHEN itemtag.tagtypeid in (3,16) THEN 3 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (2) THEN 2 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (5) THEN 5 
	 WHEN itemtag.tagtypeid = 14 and  itemtag.tagentrytypeid in (6) THEN 4 
	 WHEN itemtag.tagtypeid = 13 and  itemtag.tagentrytypeid in (2,3,5,6) THEN 14 
	 WHEN itemtag.tagtypeid = 11 and  itemtag.tagentrytypeid in (2,3,5,6) THEN 6 ELSE 1 END) as  stockreporttypeid ,
	 COALESCE(itemtag.grossweight,0) as ingrossweight,
	 COALESCE(itemtag.netweight,0) as innetweight,
	 COALESCE(itemtag.pieces,0) as inpieces,
	 COALESCE(itemtag.itemrate,0) as initemrate,
	 COALESCE(sum(stoneentry.caratweight),0) as indiacaratweight,
	 COALESCE(sum(stoneentry.pieces),0) as indiapieces,
	 0 as outgrossweight,
	0 as outnetweight,
	0 as outpieces,
	0 as outitemrate,
	0 as outdiacaratweight,
	0 as outdiapieces,
	0 as outpartialgrossweight,
	0 as outpartialnetweight,
	COALESCE(itemtag.tagweight,0) as intagwt,
	0 as outtagwt
	 FROM itemtag
	 LEFT OUTER JOIN stoneentry ON stoneentry.itemtagid = itemtag.itemtagid AND stoneentry.stonetypeid = 2
	 WHERE itemtag.status not in (0,3) and itemtag.tagdate >="2020-01-10" AND itemtag.tagdate <= "2020-01-10" and itemtag.tagtypeid not in (14,16)  
	group by itemtag.itemtagid
		
	 UNION  all
	
	select itemtag.industryid,itemtag.itemtagid as primid,itemtag.tagdate as stockdate,itemtag.branchid,
	(CASE WHEN itemtag.accountid NOT IN (1) THEN itemtag.accountid ELSE "" END) as accountid,itemtag.purityid,
	(CASE WHEN itemtag.tagentrytypeid in (2,3,5,6,7,13) THEN productid WHEN itemtag.tagentrytypeid in (4,10,11,12) THEN itemtag.processproductid ELSE 1 END) as  productid,
	(CASE WHEN itemtag.tagentrytypeid in (7) THEN itemtag.stockincounterid  ELSE fromcounterid END) as  counterid, 
	(CASE 
	WHEN itemtag.tagentrytypeid = 2  THEN 2
	WHEN itemtag.tagentrytypeid = 7  THEN 18
	WHEN itemtag.tagentrytypeid = 3  THEN 3 
	WHEN itemtag.tagentrytypeid = 4  THEN 16
	WHEN itemtag.tagentrytypeid = 5  THEN 5
	WHEN itemtag.tagentrytypeid = 6  THEN 4
	WHEN itemtag.tagentrytypeid = 10  THEN 7
	WHEN itemtag.tagentrytypeid = 11  THEN 6
	WHEN itemtag.tagentrytypeid = 12  THEN 8
	WHEN itemtag.tagentrytypeid = 13  THEN 14
	ELSE 1 END) as  stockreporttypeid ,
	0 as ingrossweight,
    0 as innetweight,
    0 as inpieces,
	0 as initemrate,
	0 as indiacaratweight,
	0 as indiapieces,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.grossweight,0) END) as outgrossweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.netweight,0) END) as outnetweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.pieces,0) END) as outpieces,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(itemtag.itemrate,0) END) as outitemrate,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(sum(stoneentry.caratweight),0) END) as outdiacaratweight,
	(CASE WHEN itemtag.tagentrytypeid = 7 and itemtag.status in (17)  THEN 0  ELSE  COALESCE(sum(stoneentry.pieces),0) END) as outdiapieces,
	0 as outpartialgrossweight,
	0 as outpartialnetweight,
	0 as intagwt,
	COALESCE(itemtag.tagweight,0) as outtagwt
	FROM itemtag
	LEFT OUTER JOIN stoneentry ON stoneentry.itemtagid = itemtag.itemtagid AND stoneentry.stonetypeid = 2
	WHERE itemtag.status not in (0,3) and itemtag.tagdate >="2020-01-10" AND itemtag.tagdate <= "2020-01-10" and itemtag.tagtypeid not in (14,16)  
	group by itemtag.itemtagid
	
	 UNION  all

	select sales.industryid,salesdetail.salesdetailid as primid ,sales.salesdate as stockdate,sales.branchid,
	(CASE WHEN itemtag.accountid NOT IN (1) THEN itemtag.accountid ELSE "" END) as accountid,salesdetail.purityid,
	(CASE WHEN salesdetail.stocktypeid in (17,19,20,24,49,62,63,65,66,80,81) THEN salesdetail.productid WHEN salesdetail.stocktypeid in (13) THEN salesdetail.processproductid ELSE 1 END) as  productid,
	(CASE WHEN salesdetail.stocktypeid in (17,19,20,24,49,65,66,80,81,62,63) THEN salesdetail.counterid WHEN salesdetail.stocktypeid in (13) THEN salesdetail.processcounterid ELSE 1 END) as  counterid,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN 8
	 WHEN salesdetail.stocktypeid in (19) THEN 6
	 WHEN salesdetail.stocktypeid in (20) THEN 7
	 WHEN salesdetail.stocktypeid in (17,80,81) THEN 16
	 WHEN salesdetail.stocktypeid in (24) THEN 9
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 2 THEN 2
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 5 THEN 5
	 WHEN salesdetail.stocktypeid in (65) and itemtag.tagtypeid = 4 THEN 4 
	 WHEN salesdetail.stocktypeid in (63) THEN 11
	 WHEN salesdetail.stocktypeid in (66) THEN 3
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (2) THEN 16 
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (3) THEN 6
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid > 10 THEN 3
	 ELSE 1 END) as  stockreporttypeid ,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.grossweight - salesdetail.grossweight,0)   WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.grossweight,0) END) as ingrossweight,
	 (CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.netweight - salesdetail.netweight,0) WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.netweight,0) END) as innetweight,
	 (CASE  WHEN salesdetail.stocktypeid in (13) THEN 1 WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (2) THEN 0 ELSE COALESCE(salesdetail.pieces,0) END) as inpieces,
	 (CASE  WHEN salesdetail.stocktypeid in (20,62,65) THEN COALESCE(itemtag.itemrate,0) ELSE 0 END) as initemrate,
	 0 as indiacaratweight,
	 0 as indiapieces,
	 0 as outgrossweight,
	 0 as outnetweight,
	 0 as outpieces,
	 0 as outitemrate,
	 0 as outdiacaratweight,
	 0 as outdiapieces,
	 0 as outpartialgrossweight,
	 0 as outpartialnetweight,
	 COALESCE(itemtag.tagweight,0) as intagwt,
	 0 as outtagwt
	 FROM salesdetail
	 join sales on sales.salesid = salesdetail.salesid
	 join itemtag on itemtag.itemtagid = salesdetail.itemtagid
	 join product on product.productid = salesdetail.productid
	 join companysetting on companysetting.companysettingid = 1
	 WHERE salesdetail.status not in (0,3) and sales.status not in (0,3) and sales.salestransactiontypeid != 1 and sales.salestransactiontypeid != 16 and salesdetail.stocktypeid in (19,20,17,80,81,24,62,63,65,66,13,49) and sales.salesdate >="2020-01-10" AND sales.salesdate <= "2020-01-10"  
	 group by salesdetail.salesdetailid
	 
	 UNION  all
	 
	select sales.industryid,salesdetail.salesdetailid as primid ,sales.salesdate as stockdate,sales.branchid,
	(CASE WHEN itemtag.accountid NOT IN (1) THEN itemtag.accountid ELSE "" END) as accountid,salesdetail.purityid,
	(CASE WHEN salesdetail.stocktypeid in (11,12,13,25,62,63,65,66,49,73) THEN salesdetail.productid  ELSE 1 END) as  productid,
	(CASE WHEN salesdetail.stocktypeid in (11,12,13,25,62,63,49,73,65,66) THEN salesdetail.counterid  ELSE 1 END) as  counterid,
	(CASE 
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 2 THEN 2
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 4 THEN 4
	 WHEN salesdetail.stocktypeid in (11,13,62) and itemtag.tagtypeid = 5 THEN 5
	 WHEN salesdetail.stocktypeid in (12,63) THEN 3 
	 WHEN salesdetail.stocktypeid in (66) THEN 10
	 WHEN salesdetail.stocktypeid in (65) THEN 11
	 WHEN salesdetail.stocktypeid in (25) THEN 9
	  WHEN salesdetail.stocktypeid in (73) THEN 14
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (2) THEN 16 
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid in (3) THEN 6
	 WHEN salesdetail.stocktypeid in (49) and product.categoryid > 10 THEN 3
	 ELSE 1 END) as  stockreporttypeid ,
	 0 as ingrossweight,
	 0 as innetweight,
	 0 as inpieces,
	 0 as initemrate,
	 0 as indiacaratweight,
	 0 as indiapieces,
	(CASE WHEN salesdetail.stocktypeid in (13) THEN  COALESCE(sum(salesdetail.grossweight),0)   WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0 ELSE  COALESCE(salesdetail.grossweight,0) END) as outgrossweight,
    (CASE  WHEN salesdetail.stocktypeid in (13) THEN  COALESCE(sum(salesdetail.netweight),0)   WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0  ELSE   COALESCE(salesdetail.netweight,0) END) as outnetweight,
    (CASE  WHEN salesdetail.stocktypeid in (13) THEN 1 WHEN salesdetail.stocktypeid in (49) and issuereceipttypeid in (3) THEN 0  ELSE COALESCE(salesdetail.pieces,0) END) as outpieces,
    (CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(itemtag.itemrate,0) ELSE 0 END) as outitemrate,
	(CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(sum(salesstoneentry.caratweight),0) ELSE 0 END) as outdiacaratweight,
	(CASE  WHEN salesdetail.stocktypeid in (11,13,65,62) THEN COALESCE(sum(salesstoneentry.pieces),0) ELSE 0 END) as outdiapieces,
	(CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.grossweight - salesdetail.grossweight,0)  ELSE 0 END) as outpartialgrossweight,
	 (CASE 
	 WHEN salesdetail.stocktypeid in (13) THEN COALESCE(itemtag.netweight - salesdetail.netweight,0) ELSE 0 END) as outpartialnetweight,
	 0 as intagwt,
	 COALESCE(itemtag.tagweight,0) as outtagwt
	 FROM salesdetail
	 join sales on sales.salesid = salesdetail.salesid
	 join itemtag on itemtag.itemtagid = salesdetail.itemtagid
	 join product on product.productid = salesdetail.productid
	 LEFT OUTER JOIN salesstoneentry ON salesstoneentry.salesdetailid = salesdetail.salesdetailid AND stonetypeid = 2
	 WHERE salesdetail.status not in (0,3) and sales.status not in (0,3) and sales.salestransactiontypeid != 1 and sales.salestransactiontypeid != 16 and salesdetail.stocktypeid in (11,13,12,25,62,63,65,66,49,73)  and sales.salesdate >="2020-01-10" AND sales.salesdate <= "2020-01-10"  
	 group by salesdetail.salesdetailid
	 
	 ) as t
	 group by branchid,accountid,purityid,productid,counterid,stockreporttypeid
	 ) as stock   LEFT OUTER JOIN purity ON purity.purityid=stock.purityid  LEFT OUTER JOIN product ON product.productid=stock.productid  LEFT OUTER JOIN stockreporttype ON stockreporttype.stockreporttypeid=stock.stockreporttypeid  GROUP BY branchid,accountid,purityid,productid,counterid,stockreporttypeid) as stock   LEFT OUTER JOIN purity ON purity.purityid=stock.purityid  LEFT OUTER JOIN product ON product.productid=stock.productid  LEFT OUTER JOIN stockreporttype ON stockreporttype.stockreporttypeid=stock.stockreporttypeid   WHERE stock.status NOT IN (0,3) AND find_in_set(3,stock.industryid ) AND   stockreporttype.stockreporttypeid IN (2,3)  AND 1=1  group by purity.purityname asc,product.productname asc  ) AS t   group by   purityname asc,productname asc  WITH ROLLUP ) AS t having (purityname = "Summary" or (purityname != "Total" ) ) AND (productname != "removetotal" ) 
ERROR - 2020-01-10 16:47:38 --> Query error: You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near '1SELECT SQL_CALC_FOUND_ROWS billnumber, lotid,  ACTUALbillrefno,ACTUALlotnumber,' at line 1 - Invalid query: 1SELECT SQL_CALC_FOUND_ROWS billnumber, lotid,  ACTUALbillrefno,ACTUALlotnumber,SUMcurrentgrossweight,SUMcurrentpieces from (SELECT  IFNULL(billnumber, 'Summary')  as billnumber, lotid,  ACTUALbillrefno,ACTUALlotnumber, ROUND((SUM(SUMcurrentgrossweight)),2) as SUMcurrentgrossweight, ROUND((SUM(SUMcurrentpieces)),0) as SUMcurrentpieces from (select  lot.billnumber, lot.lotid,  (lot.billrefno) as ACTUALbillrefno,(lot.lotnumber) as ACTUALlotnumber,ROUND(SUM(lotpending.currentgrossweight),2) as SUMcurrentgrossweight,ROUND(SUM(lotpending.currentpieces),0) as SUMcurrentpieces  from lot   LEFT OUTER JOIN lottype ON lottype.lottypeid=lot.lottypeid  join (SELECT lot.lotid,
ROUND((lot.grossweight- coalesce(SUM(itemtag.grossweight),0) + lot.errorgrossweight),5) as currentgrossweight,
ROUND((lot.netweight - coalesce(SUM(itemtag.netweight),0) + lot.errornetweight),5) as currentnetweight,
ROUND((lot.pieces - coalesce(SUM(itemtag.pieces),0) + lot.errorpieces),0) as currentpieces
FROM lot
LEFT JOIN itemtag ON itemtag.lotid = lot.lotid
WHERE (itemtag.status in (1,5,12,15) or itemtag.status is null)
GROUP BY lot.lotid ) as lotpending on lotpending.lotid = lot.lotid   LEFT OUTER JOIN itemtag ON itemtag.lotid=lot.lotid and itemtag.status not in (0) or itemtag.status is null  LEFT OUTER JOIN status ON status.status=itemtag.status  WHERE lot.status NOT IN (0,3) AND find_in_set(3,lot.industryid ) AND   lottype.lottypeid IN (2) AND status.statusname = 'Active'  AND 1=1 AND 1=1   group by lot.billnumber desc  ) AS t   group by   billnumber desc  WITH ROLLUP ) AS t having (billnumber = "Summary" or (billnumber != "Total" ))
ERROR - 2020-01-10 18:53:33 --> Severity: Notice --> Trying to get property 'whereclauseid' of non-object D:\xampp\htdocs\salesneuronver3\application\modules\Reportsview\models\Reportsmodel.php 2435
ERROR - 2020-01-10 18:53:33 --> Severity: Notice --> Trying to get property 'aggregatemethodid' of non-object D:\xampp\htdocs\salesneuronver3\application\modules\Reportsview\models\Reportsmodel.php 2456
ERROR - 2020-01-10 14:47:30 --> 404 Page Not Found: /index
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:14 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:15 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 19:29:16 --> Severity: Warning --> A non-numeric value encountered D:\xampp\htdocs\salesneuronver3\application\modules\Sales\controllers\Sales.php 1136
ERROR - 2020-01-10 14:59:57 --> 404 Page Not Found: /index
ERROR - 2020-01-10 15:14:41 --> 404 Page Not Found: /index
