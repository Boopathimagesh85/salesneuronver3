<?php
/**
*
*/

    function remove_unknown_fields($raw_data, $expected_fields){
      // print_r($raw_data); print_r($expected_fields);
      $expected_fields = array_merge($expected_fields, array('primary'));
      $expected_fields = array_merge($expected_fields, array('secondary')); 
        $new_data = array();
        foreach($raw_data as $field_name=> $field_value){
            if($field_value != "" && in_array($field_name, array_values($expected_fields))){
                $new_data[$field_name] = $field_value;
            }
        }
        return $new_data;
    }

    /**
     * Get extension
     */
    function get_extension($imagetype)
   {
       if(empty($imagetype)) return false;
       switch($imagetype)
       {
           case 'image/bmp': return '.bmp';
           case 'image/cis-cod': return '.cod';
           case 'image/gif': return '.gif';
           case 'image/ief': return '.ief';
           case 'image/jpeg': return '.jpg';
           case 'image/pipeg': return '.jfif';
           case 'image/tiff': return '.tif';
           case 'image/x-cmu-raster': return '.ras';
           case 'image/x-cmx': return '.cmx';
           case 'image/x-icon': return '.ico';
           case 'image/x-portable-anymap': return '.pnm';
           case 'image/x-portable-bitmap': return '.pbm';
           case 'image/x-portable-graymap': return '.pgm';
           case 'image/x-portable-pixmap': return '.ppm';
           case 'image/x-rgb': return '.rgb';
           case 'image/x-xbitmap': return '.xbm';
           case 'image/x-xpixmap': return '.xpm';
           case 'image/x-xwindowdump': return '.xwd';
           case 'image/png': return '.png';
           case 'image/x-jps': return '.jps';
           case 'image/x-freehand': return '.fh';
           default: return false;
       }
   }