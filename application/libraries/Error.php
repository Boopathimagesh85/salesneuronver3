<?php // if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 


class Error{

function ErrorHandler($errno, $errstr, $errfile, $errline)
{
//error_reporting(E_ALL); **rcsr**
	if($errno < 3) return true;

	// Get current log file content
	if(file_exists(dirname(__FILE__) . "/error.log")){
		$content = file_get_contents(dirname(__FILE__) . "/error.log");
	}else{
		$fileHandle = fopen("error.log", 'w') or trigger_error("Can't create log file", E_USER_WARNING);
		fclose($fileHandle);
	}
	
	// Write new log line
	$content .= "[" . date("Y/m/d - h:i:s") . "] " . $errno . " - " . $errstr . " (" . $errfile . ":" . $errline . ")\n";
	
	// Write log to file
	try{
		file_put_contents( dirname(__FILE__) . "/error.log", $content);
	}catch(Exception $e){
		
	}
	
	return true;
}
//error_reporting(E_ALL); **rcsr**
//set_error_handler("ErrorHandler"); **rcsr**
}
?>