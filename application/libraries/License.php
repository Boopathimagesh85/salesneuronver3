<?php
class License 
{
	/*License setting for Vardaan Application (SHX-show/enabled)(HHX-hide/disabled) */
	public $generallicense = array(
								'vactypegroup'=>'SHX',
								'oldcalculationtypegroup'=>'SHX',
								'accountcreategroup'=>'HHX',
								'itemcounter'=> false, //true-itemcounter present,false-itemcounter absent
							 );
	/**/
public $itemcounterlicense = array(
						'strictcounterstock'=>false,
						'autocountername'=>true,
						'defaultweight'=>false,
						'counterstartwt'=>0,'counterendwt'=>1000,
						'counterquantity'=>false,
						'extendcountertype'=>true,//not under usage
						'validatename'=>true, //not under usage
						'weightrangelicense'=>1 //1-ordinary,2-comma method(2,3,4),3-intervalmethod
	);
	/*product licensing*/
	public $productvac = array(	
								 'defaultweight'=>false,
								 'vacstartwt'=>0,'vacendwt'=>1000,
								 'weightrangelicense'=>2  //1-ordinary,2-comma method(2,3,4),3-intervalmethod	
						        );
	public $productrol = array(	
								 'defaultweight'=>false,
								 'rolstartwt'=>0,'rolendwt'=>50,
								 'weightrangelicense'=>2  //1-ordinary,2-comma method(2,3,4),3-intervalmethod	
						);
	public $product = array(
								'image'=>true,
								'imagemethod'=>2,//1-capture,2-uploading
								'category_to_product'=>true,
								'directproduct'=>true,
								'counter'=>true,								
							);
	/*purchase licensing*/
	public $purchaselicense = array(
								/*34-PureWt,35-TotalAmount*/
								'calctype'=>array(34,35),
								/*2-normal,3-approval,91-ds normal,92-ds approval*/
								'purchasetype'=>array(2,3,91,92),
								'plottouch'=>'SHX',
								'plotmelt'=>'SHX',
								'plotstone'=>'SHX',
								'plotaddamt'=>'SHX',
								'plotwst'=>'SHX',
								'plotpiece'=>'SHX',
								'plotpure'=>'SHX',
								'plotimage'=>'SHX',
								'plotaddautocalc'=>'no'
						    );
	/*pricetag specific license*/
	public $pricetag= array(
							'purchase'=>false,//no purchase related entries.
							'purchasepricetag'=>false,
							'pricetagstockmaintain'=>false
						);	
	/* metal master license */
	public $metalmasterlicense = array(
									
							);
	/*vendor license */
	public $vendorlicense = array(
							'vendoropnbalgroup'=>'SHX',
							'vendorfreeintersetgrp'=>'SHX',
							'vendorcontactgrp'=>'SHX',
							'vendormoreinfogrp'=>'SHX',
							'vendorgroupinfogrp'=>'SHX'
						);
}