<?php
	include_once('leadlicence.php');
	require_once('application/third_party/mandrill-api-php/src/Mandrill.php');
	error_reporting(0);
	//data base config
	$mashost = $CONF["sql_host"];
	$masuser = $CONF["sql_user"];
	$maspasswd = $CONF["sql_passwd"];
	$masdb=$CONF["sql_db"];
	$con=mysqli_connect($mashost,$masuser,$maspasswd)or die(mysqli_error($con));
	mysqli_select_db($con,$masdb)or die(mysqli_error($con));
	{
		//get all databases list
		$i=0;
		$mdbqy = mysqli_query($con,'select userplaninfoid,databasenameinfo from userplaninfo where activationstatus="Yes" AND status=1 group by databasenameinfo') or die(mysqli_error($con));
		while($datas = mysqli_fetch_array($mdbqy,MYSQLI_ASSOC)) {
			$cdbname[$i] = $datas['databasenameinfo'];
			$i++;
		}
		foreach($cdbname as $dbname) {
			$chkdb = mysqli_query($con,'SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = "'.$dbname.'" ');
			$count = mysqli_num_rows($chkdb);
			if( $count>=1 ) {
				mysqli_select_db($con,$dbname) or die(mysqli_error($con));
				//fetch mail send report from mandrill app.
				try {
					$mandrill = new Mandrill('X9WnMXpxhC8YEOUPuQ3oZw');
					//fetch all message id from client db
					$tabchk = tableexitcheck($con,$dbname,'crmmaillog');
					if($tabchk>=1) {
						$tblcolmnchk = tablecolumnexitcheck($con,$dbname,'crmmaillog','messageuniqueid');
						if($tblcolmnchk>=1) {
							$midqy = mysqli_query($con,'select crmmaillogid,messageuniqueid,mailstatus from crmmaillog where messageuniqueid!="" AND status=1 AND mailstatus != "invalid" AND mailstatus != "soft-bounced"')or die(mysqli_error($con));
							while($middata = mysqli_fetch_array($midqy,MYSQLI_ASSOC)) {
								$id = $middata['messageuniqueid'];
								$result = $mandrill->messages->info($id);
								//$result = $mandrill->messages->content($id);
								if($result) {
									//update data sets
									if($result['_id']==$id && ($result['state'] !='invalid' || $result['state'] !='soft-bounced')) {
										$upqy = mysqli_query($con,'UPDATE crmmaillog SET mailstatus="'.$result['state'].'",opencount='.$result['opens'].',clickcount='.$result['clicks'].' WHERE messageuniqueid="'.$result['_id'].'"');
									}
								}
							}
						}
					}
				} catch(Mandrill_Error $e) {
					echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
					throw $e;
				}
			}
		}
	}
	//check the table status
	function tableexitcheck($con,$database,$newtable) {
		$counttab=0;
		$tabexits = mysqli_query($con,"SELECT COUNT(*) AS tabcount FROM information_schema.tables WHERE table_schema = '".$database."'AND table_name = '".$newtable."'");
		$counttab = mysqli_num_rows($tabexits);
		return $counttab;
	}
	//check the table column name status
	function tablecolumnexitcheck($con,$dbname,$tblname,$fieldname) {
		$countcolmn=0;
		$colmnexits = mysqli_query($con,"SELECT COLUMN_NAME FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '".$dbname."' AND TABLE_NAME = '".$tblname."' AND COLUMN_NAME = '".$fieldname."'");
		$countcolmn = mysqli_num_rows($colmnexits);
		return $countcolmn;
	}
?>