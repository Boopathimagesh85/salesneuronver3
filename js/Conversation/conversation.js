$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
		rtaddcount=0;
		rulecondcount=0;
		globalid='all';
	}
	{// For Height
		var stnewheightaddform = $( window ).height();
		var conversationboxheight = stnewheightaddform -166;
		var conversationboxheightmbl = stnewheightaddform -100;
		var deviceas = deviceinfo;
		if(deviceas!="phone"){
			$(".conversationbox").css('height',''+conversationboxheight+'px');
		}else{
			$(".conversationboxmbl").css('height',''+conversationboxheightmbl+'px');
		}
		
	}
	{// window resize
		$(window).resize(function(){
			var stnewheightaddform = $( window ).height();
			var fixedheightaddform = stnewheightaddform -60;
			var conversationboxheight = stnewheightaddform -166;
			var conversationboxheightmbl = stnewheightaddform -100;
			$(".studiocontainer").css('height',''+fixedheightaddform+'px');
			var deviceas = deviceinfo;
			if(deviceas!="phone"){
				$(".conversationbox").css('height',''+conversationboxheight+'px');
			}else{
				$(".conversationboxmbl").css('height',''+conversationboxheightmbl+'px');
			}
		});
	}
	{//conversation submit
		$("#postsubmit").click(function() {
			var conversationmsg = $('#homedashboardconvid').val();
			var conversationtt = $('#homedashboardtitle').val();
			var file=$('#cc_filepath').val();
			var filename=$('#c_fileid').text();
			var privacy=$('#c_privacy').val();	
			var moduleid = 267;
			var rowid = 1;
			if(conversationmsg != "") {
				$.ajax({
					url:base_url+'Home/conversationcreate',				
					data:{convmsg:conversationmsg,convtitle:conversationtt,moduleid:moduleid,rowid:rowid,privacy:privacy,file:file,fname:filename},
					type:"POST",
					async:false,
					cache:false,
					success :function(msg) {
						var nmsg = $.trim(msg);
						if(nmsg == true) {
							$('#homedashboardconvid').val('');
							$('#homedashboardtitle').val('');
							$('#cc_filepath').val('');
							$('#c_fileid').text('');
							$('#c_privacy').val('');
							$('.convfileupload').hide();
							$(".addtitlestyle").removeClass('hidedisplay');
							$('.titleinputspan').addClass('hidedisplay');
							loadconversationlog();					
						}
					}
				});
			}
		});
	}
	$("#conversationmoduleid").change(function(){
		var moduleid=$("#conversationmoduleid").val();
		var rowid=1;
		var filter='all';
		$.ajax({
			url:base_url+"Home/getconversationdata",
			data: {lastnode:0,moduleid:moduleid,rowid:rowid,filter:filter},			
			type:"POST",
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {							
				var loop=data.length;			
				conversationdatablogcreation(data);			
			}
		});
	});
	$("#conversationfilter").change(function(){
		var moduleid = 267;
		var rowid=1;
		var filter=$("#conversationfilter").val();
		$.ajax({
			url:base_url+"Home/getconversationdata",
			data: {lastnode:0,moduleid:moduleid,rowid:rowid,filter:filter},			
			type:"POST",
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {							
				var loop=data.length;			
				conversationdatablogcreation(data);			
			}
		});
	});
	$("#followedbrmearea").addClass('hidedisplay');
	$("#reportmaincolumnname").select2Sortable();
	{// Tool Bar click function 		
		//refreshicon 
		$("#reportfolderrefreshicon").click(function() {
			resetFields();
		});
	}
	$("#conversation").click(function(){
		$("#conversationarea").show();
		$("#autofollowaddform").hide(); //addform auto follow 
		$("#followedbrmearea").addClass('hidedisplay');
		$("#followedbrmearea").hide();
		$("#followedbyme").addClass('hidedisplay');
		$("#unnoticedrecords").addClass('hidedisplay');
	});
	$("#autofollowrule").click(function(){
		autofollowgrid();
		autofollowconditiongrid();	
		$("#followedbrmearea").removeClass('hidedisplay');
		$("#followedbrmearea").show();
		$("#followedbyme").addClass('hidedisplay');
		$("#autofollowaddform").hide(); //addform auto follow 
		$("#followedbyme").addClass('hidedisplay');
		$("#unnoticedrecords").addClass('hidedisplay');
		$("#conversationarea").addClass('hidedisplay');
		$("#conversationarea").hide('');
	});
	$("#followedbymeid").click(function(){
		followedbymegrid();
		$("#followedbyme").removeClass('hidedisplay');
		$("#autofollowaddform").hide(); //addform auto follow 
		$("#followedbrmearea").addClass('hidedisplay');
		$("#followedbrmearea").hide();
		$("#unnoticedrecords").addClass('hidedisplay');
		$("#conversationarea").addClass('hidedisplay');
		$("#conversationarea").hide('');
	});
	$("#unnoticedrecordsid").click(function() {
		unnoticedgrid();
		$("#unnoticedrecords").removeClass('hidedisplay');
		$("#autofollowaddform").hide(); //addform auto follow
		$("#followedbyme").addClass('hidedisplay');
		$("#followedbrmearea").addClass('hidedisplay');
		$("#followedbrmearea").hide();
		$("#conversationarea").addClass('hidedisplay');
		$("#conversationarea").hide('');
	});
	//load report list based on folder-names
	$('#mainreportfolder label').click(function() {
		var id = $(this).attr('data-parentreportfolderid');
		if(id == 'conversation') {
			$("#conversationarea").show();
			$("#autofollowaddform").hide(); //addform auto follow 
			$("#followedbrmearea").addClass('hidedisplay');
			$("#followedbrmearea").hide();
			$("#followedbyme").addClass('hidedisplay');
			$("#unnoticedrecords").addClass('hidedisplay');
		} else if((id == 'autofollowrule') || (id == 'untouch')) {
			autofollowgrid();
			autofollowconditiongrid();	
			$("#followedbrmearea").removeClass('hidedisplay');
			$("#followedbrmearea").show();
			$("#followedbyme").addClass('hidedisplay');
			$("#autofollowaddform").hide(); //addform auto follow 
			$("#followedbyme").addClass('hidedisplay');
			$("#unnoticedrecords").addClass('hidedisplay');
			$("#conversationarea").addClass('hidedisplay');
			$("#conversationarea").hide('');
		} else if((id == 'followedbyme') || (id == 'untouch')) {
			followedbymegrid();
			$("#followedbyme").removeClass('hidedisplay');
			$("#autofollowaddform").hide(); //addform auto follow 
			$("#followedbrmearea").addClass('hidedisplay');
			$("#followedbrmearea").hide();
			$("#unnoticedrecords").addClass('hidedisplay');
			$("#conversationarea").addClass('hidedisplay');
			$("#conversationarea").hide('');
		} else if((id == 'unnoticedrecords') || (id == 'untouch')) {
			unnoticedgrid();
			$("#unnoticedrecords").removeClass('hidedisplay');
			$("#autofollowaddform").hide(); //addform auto follow
			$("#followedbyme").addClass('hidedisplay');
			$("#followedbrmearea").addClass('hidedisplay');
			$("#followedbrmearea").hide();
			$("#conversationarea").addClass('hidedisplay');
			$("#conversationarea").hide('');
		}
	});
	//Auto follow addform show
	$("#autofollowaddicon").click(function() {
		$("#followedbrmearea").hide();
		$("#autofollowaddform").show();
		$("#autoddcondvaluedivhid").hide();
		$("#autoddcondvaluedivhid").hide();
		$("#autodatecondvaluedivhid").hide();
		$("#finalviewcondvalue").hide();
		$("#followedbrmearea").addClass('hidedisplay');
		$("#conversationarea").addClass('hidedisplay');
		$("#followedbyme").addClass('hidedisplay');
		$("#autorulemoduleid").attr('readonly',false);
		$("#autorulepublic").val('No');
		$("#autoandordivhid").hide();
	});
	$(".addtitlestyle").click(function() {
		$(this).addClass('hidedisplay');
		$('.titleinputspan').removeClass('hidedisplay');
	});
	$(".titleshield").click(function() {
		$(".addtitlestyle").trigger('click');
	});
	//drag and drop hide
	setTimeout(function() {
	$('.fileuploaddraganddpcls').attr('disabled','disabled');
	$(".fileuploaddraganddpcls").hide();
	$(".convfileupload").hide();
	},100);
	{// file upload
		$("#convmulitplefileuploader").hide();
		$(".ajax-file-upload-filename").hide();
		$(".ajax-upload-dragdrop").hide();
		setTimeout(function(){
			$(".ajax-file-upload-container").hide();
			$(".ajax-file-upload-filename").hide(); 
		}, 100);
		fileuploadmoname = 'convfileupload';
		var conversationsettings = {
			url: base_url+"upload.php",
			method: "POST",
			multiple:false,
			showStatusAfterSuccess: false,
			fileName: "myfile",
			allowedTypes: "*",
			dataType:'json',
			async:false,
			onSuccess:function(files,data,xhr) {
				if(data != "MaxSize" && data != "Size" && data != "MaxFile"){
					var arr = jQuery.parseJSON(data);	
					var c_filepath=arr.path;
					$(".convfileupload").show();
					$('#cc_filepath').val(c_filepath);
					$('#c_fileid').text(arr.fname);
				}else if(data == "Size"){
					alertpopupdouble("File Size exceeds the limit. You can upload upto 5mb at a time.");
				} else if(data == "MaxSize"){
					alertpopupdouble("You have reached your maximun storage limit. if you want to upload documents you want to buy the storage limit from add ons");
				} else if(data == "MaxFile"){
					alertpopupdouble("Maximum number fo files exceeded.");
				}else{
					alertpopupdouble("Upload Failed");
				}
			},
			onError: function(files,status,errMsg) {
			}
		}
		$("#convmulitplefileuploader").uploadFile(conversationsettings);
		$(".uploadattachments").click(function(){
			$(".ajax-file-upload>form>input:first").trigger('click');
		});
		{//file upload removal-
			$('#removefile').click(function(){	
				$('#c_fileid').text('');
				$('#cc_filepath').val('');
				$('#cc_filename').val('');
				$(".convfileupload").hide();
			});
		}
	}
	//conversation log data fetch
	loadconversationlog();
	$('.fileuploaddraganddpcls').attr('disabled','disabled');
	$(".fileuploaddraganddpcls").hide();
	{//auto follow rules concept
		//module drop down change function
		$("#autorulemoduleid").change(function() {
			var id = $(this).val();
			var modname = $("#autorulemoduleid").find('option:selected').data('modname');
			$("#modulename").val(modname);
			if(id != '' || id != null || id != undefined) {
				autorulecreationfieldget(id);
				setTimeout(function() {
					$("#autocondition").select2('val','');
					$("#autoandor").select2('val','');
					$("#autocondvalue").val('');
					$("#jointable").val('');
					$("#indexname").val('');
					$("#autocolumnname,#autoddcondvalue").select2('val','');
					$("#autoddcondvalue").empty();
				},100);
			} 	
		});
	}
	//autocolumnname  hidden value assign
	$("#autocolumnname").change(function() {
		var fieldlable = $("#autocolumnname").val();
		var ffname = $("#autocolumnname").find('option:selected').data('ffname');
		var jointable = $("#autocolumnname").find('option:selected').data('jointable');
		var indexname = $("#autocolumnname").find('option:selected').data('indexname');
		var id = $("#autocolumnname").find('option:selected').data('id');
		var moduleid = $("#moduleid").val();
		$("#comumnname").val(ffname);
		$("#jointable").val(jointable);
		$("#indexname").val(indexname);
		$("#fieldid").val(id);
		var uitypeid = $("#autocolumnname").find('option:selected').data('uitype');
		$('#autocondition').empty();
		$.ajax({
			url:base_url+"Base/loadconditionbyuitype?uitypeid="+uitypeid, 
			dataType:'json',
			async:false,
			success: function(data) { 
				$.each(data, function(index){					
					$('#autocondition')
					.append($("<option></option>")
					.attr("value",data[index]['whereclausename'])
					.text(data[index]['whereclausename']));
				});	
			}
		});
		if(uitypeid == '2' || uitypeid == '3' || uitypeid == '4'|| uitypeid == '5' || uitypeid == '6' || uitypeid == '7' || uitypeid == '10' || uitypeid == '11' || uitypeid == '12' || uitypeid == '14') {
			showhideintextbox('autoddcondvalue','autocondvalue');
			$('#autocondvalue').val('');
			$("#autodatecondvaluedivhid").hide();
			$("#autodatecondvalue").removeClass('validate[required]');
			$("#autoddcondvalue").removeClass('validate[required]');
			$("#autocondvalue").addClass('validate[required,maxSize[100]]');
		} else if(uitypeid == '17' || uitypeid == '28') {
			$("#autodatecondvaluedivhid").hide();
			$("#autodatecondvalue").removeClass('validate[required]');
			showhideintextbox('autocondvalue','autoddcondvalue');
			$("#autocondvalue").removeClass('validate[required,maxSize[100]]');
			$("#autoddcondvalue").addClass('validate[required]');
			$("#autocondvalue").datepicker("disable");
			fieldviewnamebespicklistdddvalue(indexname);
		}else if(uitypeid == '18' || uitypeid == '19'|| uitypeid == '20' || uitypeid == '21'|| uitypeid == '25' || uitypeid == '26' || uitypeid == '27' || uitypeid == '29' ) {
			$("#autodatecondvaluedivhid").hide();
			showhideintextbox('autocondvalue','autoddcondvalue');
			$("#autodatecondvalue").removeClass('validate[required]');
			$("#autocondvalue").removeClass('validate[required,maxSize[100]]');
			$("#autoddcondvalue").addClass('validate[required]');
			$("#autocondvalue").datepicker("disable");
			fieldviewnamebesdddvalue(indexname);
		} else if(uitypeid == '13') {
			$("#autodatecondvaluedivhid").hide();
			$("#autodatecondvalue").removeClass('validate[required]');
			showhideintextbox('autocondvalue','autoddcondvalue');
			$("#autoddcondvalue").addClass('validate[required]');
			$("#autocondvalue").removeClass('validate[required,maxSize[100]]');
			$("#autocondvalue").datepicker("disable");
			newcheckboxvalueget('autoddcondvalue');
		} else if(uitypeid == '8') {
			$("#autodatecondvaluedivhid").show();
			$("#autoddcondvaluedivhid,#autocondvaluedivhid").hide();
			$('#autocondvalue').val('');
			$("#autoddcondvalue,#autocondvalue").removeClass('validate[required]');
			if(fieldlable == 'Date of Birth'){
				$('#autodatecondvalue').datetimepicker('destroy');
				var dateformetdata = $('#autodatecondvalue').attr('data-dateformater');
				$('#autodatecondvalue').datetimepicker({
					dateFormat: 'dd-mm-yy',
					showTimepicker :false,
					minDate: null,
					changeMonth: true,
					changeYear: true,
					maxDate:0,
					yearRange : '1947:c+100',
					onSelect: function () {
						var checkdate = $(this).val();
						if(checkdate != ''){
							$("#finalautocondvalue").val(checkdate);
						}
					},
					onClose: function () {
						$('#autodatecondvalue').focus();
					}
				}).click(function() {
					$(this).datetimepicker('show');
				});
			} else {
				$('#autodatecondvalue').datetimepicker('destroy');
				var dateformetdata = $('#autodatecondvalue').attr('data-dateformater');
				$('#autodatecondvalue').datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: 0,
					onSelect: function () {
						var checkdate = $(this).val();
						$("#finalautocondvalue").val(checkdate);
					},
					onClose: function () {  
						$('#autodatecondvalue').focus();
					}
				}).click(function() {
					$(this).datetimepicker('show');
				});
			}
		} else if(uitypeid == '9') {
			$('#autocondvalue').val('');
			$("#autoddcondvalue").removeClass('validate[required]');
			$('#autocondvalue').timepicker({
				showTimepicker: true,
				timeOnly:true,
				showSecond: true,
				timeFormat: 'HH:mm:ss',
				onSelect: function () {
					var checkdate = $(this).val();
					if(checkdate != ''){
						$("#finalautocondvalue").val(checkdate);
					}
				},
				onClose: function () {
					$('#autocondvalue').focus();
				}
			}).click(function() {
			$(this).datetimepicker('show');
		});
		} else {
			alertpopup('Please choose another field name');
		}
	});
	{//Rule criteria cond create
		$('#autoaddcondsubbtn').click(function() {
			andorcondvalidationreset();
			setTimeout(function(){
				$('#viewformconditionvalidation').validationEngine('validate');
			},50);
		});
		$('#viewformconditionvalidation').validationEngine({
			onSuccess:function() {
				rulecondcount++;
				formtogriddata('autofollowconditiongrid','ADD','last','');
				clearform('viewcondclear');
				/* Data row select event */
				datarowselectevt();
				if(rulecondcount==1) {
					$('.mandatoryfildclass').text('*');
					$('#autoandor').addClass('validate[required]');
					var dropdownid =['3','autocolumnname','autocondition','autoandor'];
					dropdownfailureerror(dropdownid);
					var condcnt = $('#autofollowconditiongrid .gridcontent div.data-content div').length;
					if(condcnt==0) {
						$("#autoandordivhid").hide();
					} else {
						$("#autoandordivhid").show();
					}
				} else {
					var condcnt = $('#autofollowconditiongrid .gridcontent div.data-content div').length;
					if(condcnt==0) {
						$('.mandatoryfildclass').text('');
						$("#autoandordivhid").hide();
						$('#autoandor').removeClass('validate[required]');
						var dropdownid =['2','autocolumnname','autocondition'];
						dropdownfailureerror(dropdownid);
					} else {
						$('.mandatoryfildclass').text('*');
						$("#autoandordivhid").show();
						$('#autoandor').addClass('validate[required]');
						var dropdownid =['3','autocolumnname','autocondition','autoandor'];
						dropdownfailureerror(dropdownid);
					}
				}
			},
			onFailure:function() {
				var dropdownid =['3','autocolumnname','autocondition','autoandor'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//focus out in value
		$("#autocondvalue").focusout(function(){
			var value = $("#autocondvalue").val();
			$("#finalautocondvalue").val(value);
		});
		//dropdown change value
		$("#autoddcondvalue").change(function(){
			var value = $("#autoddcondvalue").val();
			$("#finalautocondvalue").val(value);
		});
    } 
	$("#conddeleteicon").click(function() {
		var datarowid = $('#autofollowconditiongrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			if(datarowid != '0') {
				var did = $('#conditiondeleteid').val();
				if(did == '' || did == null) {
					$('#conditiondeleteid').val(datarowid); 
				} else {
					$('#conditiondeleteid').val(did+','+datarowid);
				}
			}
			/*delete grid data*/
			deletegriddatarows('autofollowconditiongrid',datarowid);
			var ccondcnt = $('#autofollowconditiongrid .gridcontent div.data-content div').length;
			if(ccondcnt == '' || ccondcnt == null){
				$("#autorulemoduleid").attr('readonly',false);
			}
			if(ccondcnt==0) {
				$('.mandatoryfildclass').text('');
				$("#autoandordivhid").hide();
				$('#autoandord').removeClass('validate[required]');
			} else {
				$('.mandatoryfildclass').text('*');
				$("#autoandordivhid").show();
				$('#autoandord').addClass('validate[required]');
			}
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#autoruleadd").click(function(){
		$('#autorulebasicvalidation').validationEngine('validate');
	});
	$('#autorulebasicvalidation').validationEngine({
		onSuccess:function() {
			autoruleinsertion();
		},
		onFailure:function() {
			var dropdownid =['4','autocolumnname','autocondition','autoandor','autoddcondvalue'];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	//auto follow rule edit
	$("#autofollowediticon").click(function() {
		var datarowid = $('#autofollowgrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			$("#autofollowaddform,#autoruleupdate").show();
			$("#followedbrmearea,#autoruleadd").hide();
			$("#autoddcondvaluedivhid").hide();
			$("#autoddcondvaluedivhid").hide();
			$("#autodatecondvaluedivhid").hide();
			$("#finalviewcondvalue").hide();
			cleargriddata('autofollowconditiongrid');
			$("#autorulemoduleid").attr('readonly',true);
			$("#processoverlay").show();
			autofollowruleeditdatafetch(datarowid);
		} else {
			alertpopup("Please select a row");
		}
	});
	//auto follow rule delete
	$("#autofoloweddeleteicon").click(function() {
		var datarowid = $('#autofollowgrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			$("#basedeleteoverlay").fadeIn();
			$("#basedeleteyes").focus();
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#basedeleteyes").click(function() {
		var datarowid = $('#autofollowgrid div.gridcontent div.active').attr('id');
		autofollowruledelete(datarowid);
	});
	//autofollow grid reload icon
	$("#autofolowedreloadicon").click(function(){
		autofollowgrid();
	});
	$("#autoruleupdate").click(function(){
		$('#autoruleeditbasicvalidation').validationEngine('validate');
	});
	$('#autoruleeditbasicvalidation').validationEngine({
		onSuccess:function() {
			autoruleupdate();
		},
		onFailure:function() {
			var dropdownid =['4','autocolumnname','autocondition','autoandor','autoddcondvalue'];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	$("#moduleid").change(function(){
		autofollowgrid();
	});
	{// Function For Check box
		$('.checkboxcls').click(function() {
			if ($(this).is(':checked')) {
				$(this).val('Yes');
			} else {
				$(this).val('No');
			}		
		});
	}
	//close button
	$("#autoruleclose").click(function() {
		cleargriddata('autofollowconditiongrid');
		resetFields();	
		$("#autofollowaddform").hide();
		$("#followedbrmearea").show();
	});
	//followed by me
	$("#followmoduleid").change(function(){
		var moduleid = $("#followmoduleid").val();
		$("#followviewid").select2('val','');
		$("#followruleid").select2('val','');
		viewdatafetch(moduleid);
		followbyrule(moduleid);
	});
	//rule change
	$("#followruleid").change(function(){
		followedbymegrid();
	});
	//rule change
	$("#followviewid").change(function(){
		followedbymegrid();
	});
	//un follow icon click
	$("#unfollowrecordid").click(function(){
		var moduleid = $("#followmoduleid").val();
		var viewid = $("#followviewid").val();
		var ruleid = $("#followruleid").val();
		if(moduleid) {
			if(viewid) {
				if(ruleid) {
					var ids = jQuery("#followedbymegrid").jqGrid('getGridParam','selarrrow');
					if(ids != "") {
						unfollowrecords(ids,moduleid);
					} else {
						alertpopup('Please Select the record(s) to unfollow');
					}
				} else {
					alertpopup('Please select the Rule');
				}
			} else {
				alertpopup('Please select the View');
			}
		} else {
			alertpopup('Please select the Module');
		}
	});
	// un noticed record module id change
	$("#unnoticedmoduleid").click(function() {
		unnoticedgrid();
	});
	// slider change
	$("#slider").mouseup(function() {	
		var daycount = $("#slider").slider("value");
		if(daycount) {
			unnoticedgrid();
		} else {
			alertpopup('Please select the number of day(s)');
		}
	});
	//
	$("#homedashboardconvid").keydown(function() {
	});
});
{//GRA- Reply upload
function replycontainerupload(){
	setTimeout(function() {		
		$('.fileuploaddraganddpcls').attr('disabled','disabled');
		$(".fileuploaddraganddpcls").hide();
		$(".ajax-file-upload-container").remove();
	},100);
	$(".convfileupload1").hide();
	$(".ajax-file-upload-filename").hide();
		fileuploadmoname = 'convfileupload1';
		var conversationsettings1 = {
			url: base_url+"upload.php",
			method: "POST",
			fileName: "myfile",
			multiple:false,
			showStatusAfterSuccess: false,
			allowedTypes: "*",
			dataType:'json',
			async:false,
			onSuccess:function(files,data,xhr) {	
				if(data != "MaxSize" && data != "Size" && data != "MaxFile"){
					var arr = jQuery.parseJSON(data);	
					var c_filepath=arr.path;
					$(".convfileupload1").show();
					$('#cc_filepath').val(c_filepath);
					$('#c_fileid'+c_reply_rowid+'').text(arr.fname);
				}else if(data == "Size"){
					alertpopupdouble("File Size exceeds the limit. You can upload upto 5mb at a time.");
				} else if(data == "MaxSize"){
					alertpopupdouble("You have reached your maximun storage limit. if you want to upload documents you want to buy the storage limit from add ons");
				} else if(data == "MaxFile"){
					alertpopupdouble("Maximum number fo files exceeded.");
				}else{
					alertpopupdouble("Upload Failed");
				}
			},
			onError: function(files,status,errMsg) {
			}
		}
		$("#convmulitplefileuploader1").uploadFile(conversationsettings1);
		$(".uploadattachments1").click(function(){
			$("#convmulitplefileuploader1:last>.ajax-upload-dragdrop>.ajax-file-upload>form>input:last").trigger('click');
		});
		{//file upload removal-
			$('#removefile').click(function(){
				$('#c_fileid').text('');
				$('#cc_filepath').val('');
				$('#cc_filename').val('');
				$(".convfileupload").hide();
			});
		}
	}
}
{// Autofollow View Grid
	function autofollowgrid(page,rowcount) {	
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = $('#autofollowgrid').width();
		var wheight = $('#autofollowgrid').height();
		/*col sort*/
		var sortcol = $('.autofollowruleheadercolsort').hasClass('datasort') ? $('.autofollowruleheadercolsort.datasort').data('sortcolname') : '';
		var sortord =  $('.autofollowruleheadercolsort').hasClass('datasort') ? $('.autofollowruleheadercolsort.datasort').data('sortorder') : '';
		var headcolid = $('.autofollowruleheadercolsort').hasClass('datasort') ? $('.autofollowruleheadercolsort.datasort').attr('id') : '0';
		var moduleid = $("#moduleid").val();
		$.ajax({
			url:base_url+"Conversation/autorulelist?maintabinfo=autofollowrule&primaryid=autofollowruleid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+"&moduleid="+moduleid,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#autofollowgrid').empty();
				$('#autofollowgrid').append(data.content);
				$('#autofollowgridfooter').empty();
				$('#autofollowgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('autofollowgrid');
				//row header grouping
				datarowheadergroup('autofollowgrid');
				{//sorting
					$('.autofollowruleheadercolsort').click(function(){
						$('.autofollowruleheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#autofollowrulepgnumcnt li .page-text .active').data('rowcount');
						auditloggrid(page,rowcount);
					});
					sortordertypereset('autofollowruleheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#autofollowrulepgnumcnt li .page-text .active').data('rowcount');
						autofollowgrid(page,rowcount);
					});
					$('.pagerowcount').click(function(){
						var page = $('ul#autofollowrulepgnum li.active').data('pagenum');
						var rowcount = $(this).data('rowcount');
						autofollowgrid(page,rowcount);
					});
					$('.pvpagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#autofollowrulepgnumcnt li .page-text .active').data('rowcount');
						autofollowgrid(page,rowcount);
					});
					$('#auditlogpgrowcount').change(function(){
						var rowcount = $(this).val();
						var page = $('ul#autofollowrulepgnum li.active').data('pagenum');
						autofollowgrid(page,rowcount);
					});
				}
			},
		});
	}
}
//Create View Grid
function autofollowconditiongrid() {
	var wwidth = '1290';
	var wheight = $("#autofollowconditiongrid").height();
	$.ajax({
		url:base_url+"Conversation/autofollowruleconditioninformationfetch?moduleid=267&width="+wwidth+"&height="+wheight+"&modulename=Auto Follow Rule Conditions",
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#autofollowconditiongrid").empty();
			$("#autofollowconditiongrid").append(data.content);
			$("#autofollowconditiongridfooter").empty();
			$("#autofollowconditiongridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('autofollowconditiongrid');
		},
	});
}
{// Followed by me View Grid
	function followedbymegrid(page,rowcount) {
		var mid = $("#followmoduleid").val();
		var ruleid = $("#followruleid").val();
		var viewid = $("#followviewid").val();
		$.ajax({
			url:base_url+"Conversation/parenttableget?moduleid="+mid,
			dataType:'json',
			async:false,
			cache:false,
			success:function(pdata) {
				if(pdata != "") {
					maintabinfo = pdata;
					parentid = maintabinfo+"id";
				} else {
					viewid = '7';
					maintabinfo = 'account';
					parentid = 'accountid';
				}
			},
		});
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = $('#followedbymegrid').width();
		var wheight = $('#followedbymegrid').height();
		//col sort
		var sortcol = $('.unnoticedrecordlistheadercolsort').hasClass('datasort') ? $('.unnoticedrecordlistheadercolsort.datasort').data('sortcolname') : '';
		var sortord =  $('.unnoticedrecordlistheadercolsort').hasClass('datasort') ? $('.unnoticedrecordlistheadercolsort.datasort').data('sortorder') : '';
		var headcolid = $('.unnoticedrecordlistheadercolsort').hasClass('datasort') ? $('.unnoticedrecordlistheadercolsort.datasort').attr('id') : '0';
		$.ajax({
			url:base_url+"Conversation/gridvalinformationfetch?maintabinfo="+maintabinfo+"&primaryid="+parentid+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid='+mid+"&viewid="+viewid+"&checkbox=false"+"&ruleid="+ruleid,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#followedbymegrid').empty();
				$('#followedbymegrid').append(data.content);
				$('#followedbymegridfooter').empty();
				$('#followedbymegridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				/*column resize*/
				columnresize('followedbymegrid');
				{//sorting
					$('.unnoticedrecordlistheadercolsort').click(function(){
						$('.unnoticedrecordlistheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $('ul#unnoticedrecordlistpgnum li.active').data('pagenum');
						var rowcount = $('ul#unnoticedrecordlistpgnumcnt li .page-text .active').data('rowcount');
						followedbymegrid(page,rowcount);
					});
					sortordertypereset('unnoticedrecordlistheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#unnoticedrecordlistpgnumcnt li .page-text .active').data('rowcount');
						followedbymegrid(page,rowcount);
					});
					$('.pagerowcount').click(function(){
						var page = $('ul#unnoticedrecordlistpgnum li.active').data('pagenum');
						var rowcount = $(this).data('rowcount');
						followedbymegrid(page,rowcount);
					});
					$('.pvpagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#unnoticedrecordlistpgnumcnt li .page-text .active').data('rowcount');
						followedbymegrid(page,rowcount);
					});
					$('#inviteuserlistpgrowcount').change(function(){
						var rowcount = $(this).val();
						var page = $('ul#unnoticedrecordlistpgnum li.active').data('pagenum');
						followedbymegrid(page,rowcount);
					});
				}
			
			}
		});
		
	}
}	
{// Unnoticed records View Grid
	function unnoticedgrid(page,rowcount) {
		// To View the Content Area 
		var id = $("#unnoticedmoduleid").val();
		if(id != "" && id != 1) {
			$.ajax({
				url:base_url+"Conversation/defaultviewfetch?modid="+id,
				async:false,
				cache:false,
				success:function(data) {
					if(data != "") {
						viewid = data;
					} else {
						viewid = '7';
					}
				},
			});
			$.ajax({
				url:base_url+"Conversation/parenttableget?moduleid="+id,
				dataType:'json',
				async:false,
				cache:false,
				success:function(pdata) {
					if(pdata != "") {
						maintabinfo = pdata;
						parentid = maintabinfo+"id";
					} else {
						maintabinfo = 'account';
						parentid = 'accountid';
					}
				},
			});
		} else {
			viewid = '7';
			maintabinfo = 'account';
			parentid = 'accountid';
		}
		var daycount = '12';
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = $('#unnoticedgrid').width();
		var wheight = $('#unnoticedgrid').height();
		//col sort
		var sortcol = $('.unnoticedrecordlistheadercolsort').hasClass('datasort') ? $('.unnoticedrecordlistheadercolsort.datasort').data('sortcolname') : '';
		var sortord =  $('.unnoticedrecordlistheadercolsort').hasClass('datasort') ? $('.unnoticedrecordlistheadercolsort.datasort').data('sortorder') : '';
		var headcolid = $('.unnoticedrecordlistheadercolsort').hasClass('datasort') ? $('.unnoticedrecordlistheadercolsort.datasort').attr('id') : '0';
		$.ajax({
			url:base_url+"Conversation/unnoticedrecordfetch?maintabinfo="+maintabinfo+"&primaryid="+parentid+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid='+id+"&viewid="+viewid+"&daycount="+daycount+"&checkbox=false",
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#unnoticedgrid').empty();
				$('#unnoticedgrid').append(data.content);
				$('#unnoticedgridfooter').empty();
				$('#unnoticedgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				/*column resize*/
				columnresize('unnoticedgrid');
				{//sorting
					$('.unnoticedrecordlistheadercolsort').click(function(){
						$('.unnoticedrecordlistheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $('ul#unnoticedrecordlistpgnum li.active').data('pagenum');
						var rowcount = $('ul#unnoticedrecordlistpgnumcnt li .page-text .active').data('rowcount');
						unnoticedgrid(page,rowcount);
					});
					sortordertypereset('unnoticedrecordlistheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#unnoticedrecordlistpgnumcnt li .page-text .active').data('rowcount');
						unnoticedgrid(page,rowcount);
					});
					$('.pagerowcount').click(function(){
						var page = $('ul#unnoticedrecordlistpgnum li.active').data('pagenum');
						var rowcount = $(this).data('rowcount');
						unnoticedgrid(page,rowcount);
					});
					$('.pvpagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#unnoticedrecordlistpgnumcnt li .page-text .active').data('rowcount');
						unnoticedgrid(page,rowcount);
					});
					$('#inviteuserlistpgrowcount').change(function(){
						var rowcount = $(this).val();
						var page = $('ul#unnoticedrecordlistpgnum li.active').data('pagenum');
						unnoticedgrid(page,rowcount);
					});
				}
			
			}
		});
	}
}
//conversation data fetch
function loadconversationlog() {
	var moduleid=267;
	var rowid=1;
	var filter='all';
	$.ajax({
		url:base_url+"Home/getconversationdata",
		data: {lastnode:0,moduleid:moduleid,rowid:rowid,filter:filter},
		type:"POST",
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {						
			var loop=data.length;			
			conversationdatablogcreation(data);	
		}
	});
}
//conversation data show
function conversationdatablogcreation(data) {
	var conversationappend=$("#conversationdatablog");	
	conversationappend.empty('');
	var employeeurl=base_url+'Employee';
	for(var i=0; i<data.length;i++) {
		if(data[i]['profile'] !== null){	
			var imgurl = '<img class="convers-user-img" id="" title="" src="'+data[i]['profile']+'">';
		}else{
			var imgurl = '<div class="convers-user-icon"><span class=""><i class="material-icons">account_circle</i></span></div>';
		}
		var file = "";
		var filefolder ="";
		var editablefield='';
		if(data[i]['profile'] != '' || data[i]['profile'] !== null || data[i]['profile'] !== 'undefined'){
			if(data[i]['profile']) {
				if(data[i]['fromid'] == '2' ) {
					var imgurl='<img class="convers-user-img" id="" title="" src="'+base_url+data[i]['profile']+'">';
				} else {
					var imgurl = '<img class="convers-user-img" id="" title="" src="'+data[i]['profile']+'">';
				}
			}
		}
		if(data[i]['file']) {
			var file=data[i]['file'];
			var filefolder=base_url+data[i]['filepath'];
			var document= '<div class="large-6 columns" style="text-align:left"><span class="convfiledownload">Attached File : <a class="c_filedownload" data-file="'+filefolder+'">'+file+'</a></span></div>';
		} else {
			var document= '<div class="large-6 columns" style="text-align:left"></div>';
		}
		if (data[i]['editable'] == "yes") {
			var editablefield='<span class="convopicons c_edit" data-rowid="'+data[i]['c_id']+'" title="Edit"><i class="material-icons">edit</i></span><span class="convopicons c_delete" data-rowid="'+data[i]['c_id']+'" title="Delete"><i class="material-icons">delete</i></span>'
		}
		var rcount = data[i]['count'];
		if( rcount != 0) {
			var creplycount = data[i]['count']+' reply';
		} else {
			var creplycount = '';
		}
		conversationappend.append('<div class="large-12  columns mblnopadding conversations"><div class="large-12 medium-12 small-12 columns mblnopadding totalcotainerconv"><div class="large-1 medium-1 small-1 column paddingzero"><div class="image"><span class="">'+imgurl+'</span></div></div><div class="large-11 medium-11 small-11"><div class="large-12"><div class="large-6 messager-name"><div class="large-12"><a href='+employeeurl+' data-moduleid="4" target="_blank" data-rowid='+data[i]['userid']+'>'+data[i]['username']+'</a></div><div class="large-12"></div></div><div class="large-6"><span></span>&nbsp;<span class=""><span></span></span><span title="View Reply"></span><span class="" title="Reply"></span><span class="" title="Edit"></span><span class="" title="Delete"></span></div><div class="large-12 messager-text">'+data[i]['c_name']+'</div></div><div class="large-12"><span class="conversationtime" title="">'+data[i]['time']+'</span></div></div></div></div>');
	}
	$(".totalcotainerconv").hover(function() {
		$( this ).find('.iconsetconv').css('opacity','1');}, function(){
		$( this ).find('.iconsetconv').css('opacity','0');
	});
	//conversation delete
	$('.c_delete').click(function() {
		var c_delete_rowid=$(this).attr("data-rowid");
		if(c_delete_rowid !='' ||c_delete_rowid != null){
			$.ajax({
				url:base_url+"Home/conversationdelete",
				data:{rowid:c_delete_rowid},
				type:"POST",
				async:false,
				cache:false,
				success :function(msg) {
					loadconversationlog();
				}
			});
		}
	});
	//conversation editdata
	$('.c_edit').click(function() {
		var actionid=$(this).attr("data-actionid");
		var c_edit_rowid=$(this).attr("data-rowid");
		var i=$(this).attr("data-eq");
		$('#postsubmit').addClass('hidedisplay');
		$("#editpostsubmit").removeClass('hidedisplay');
		$('#postcancel').removeClass('hidedisplay');
		$('#cc_filepath').val('');
		$('#c_fileid').text('');
		if(c_edit_rowid !='' ||c_edit_rowid != null){
			$.ajax({
				url:base_url+"Home/getconversationeditdata",
				data:{rowid:c_edit_rowid},
				type:"POST",
				async:false,
				dataType:'json',
				cache:false,
				success :function(data) {
					//set the data
					setTimeout(function(){
						$("#homedashboardtitle").val(data.c_title);
						$("#homedashboardconvid").val(data.c_message);
						$('#unique_rowid').val(data.c_rowid);
						if(checkValue(data.c_file) == true){
							$('.convfileupload').show();
							$('#cc_filepath').val(data.c_filepath);
							$('#c_fileid').text(data.c_file);
						} else{
							$("#removefile").trigger('click');
						}
					}, 100);
					$('#homedashboardconvid').val(data.c_name);
					$('.addtitlestyle').trigger('click');
				}
			});
		}
		//removes container edit area
		$('.edipostcancel').click(function(){
			$(this).parent('div').parent('div').remove();
		});
		//Edit post submit
		$('#editpostsubmit').click(function() {
			//get the reply data  
			var rowid=$('#unique_rowid').val();
			var moduleid='267';
			var conversationmsg = $('#homedashboardconvid').val();
			var conversationtt = '';
			var file=$('#cc_filepath').val(); 
			var privacy=$('#c_privacy').val();
			var filename = $('#c_fileid').text();
			if(rowid) {
				$.ajax({
					url:base_url+"Home/updateconversationdata",
					data:{rowid:rowid,moduleid:moduleid,c_message:conversationmsg,convtitle:conversationtt,privacy:privacy,file:file,filename:filename},
					type:"POST",
					async:false,	
					cache:false,
					success :function(msg) {
						$('#unique_rowid').val('');
						$('#homedashboardconvid').val('');
						$('#homedashboardtitle').val('');
						$('#cc_filepath').val('');
						$('#c_privacy').val('');
						$('#c_fileid').text('');
						$(".addtitlestyle").removeClass('hidedisplay');
						$('.titleinputspan').addClass('hidedisplay');
						$('#editpostsubmit,#postcancel').addClass('hidedisplay');
						$('#postsubmit').removeClass('hidedisplay');
						$('.convfileupload').hide();
						//reload the div
						loadconversationlog();
					}
				});
			}
		});
		//scrollbarclass
		$('.c_scroll').animate({scrollTop:0},'slow');
		//upload container
		var conversationsettings1 = {
				url: base_url+"upload.php",
				method: "POST",
				multiple:false,
				showStatusAfterSuccess: false,
				fileName: "myfile",
				allowedTypes: "*",
				dataType:'json',
				async:false,
				onSuccess:function(files,data,xhr) {
					if(data != "MaxSize" && data != "Size" && data != "MaxFile"){
						var arr = jQuery.parseJSON(data);
						$('.convfileupload'+c_edit_rowid+'').show();
						$('#cc_filepath'+c_edit_rowid+'').val(arr.path);
						$('#c_fileid'+c_edit_rowid+'').text(arr.fname);
					}else if(data == "Size"){
						alertpopupdouble("File Size exceeds the limit. You can upload upto 5mb at a time.");
					} else if(data == "MaxSize"){
						alertpopupdouble("You have reached your maximun storage limit. if you want to upload documents you want to buy the storage limit from add ons");
					} else if(data == "MaxFile"){
						alertpopupdouble("Maximum number fo files exceeded.");
					}else{
						alertpopupdouble("Upload Failed");
					}
				},
				onError: function(files,status,errMsg) {
				}
			}
			$('#convmulitplefileuploader'+c_edit_rowid+'').uploadFile(conversationsettings1);
			$('.uploadattachments').click(function(){
				$("#convmulitplefileuploader"+c_edit_rowid+":last>.ajax-upload-dragdrop>.ajax-file-upload>form>input:last").trigger('click');
			});
	});
	$('.mention').smention(""+base_url+""+"Home/getuser",{
		after : " "
	});
	$(".replyaction").click(function() {
		$('.replyactioncontainer').remove();
		var actionid=$(this).attr("data-actionid");
		var c_reply_rowid=$(this).attr("data-rowid");
		var i=$(this).attr("data-eq");
		$(this).parent('div').parent('div').parent('div').parent('div').append('<div class="large-12 columns paddingzero replyactioncontainer"><div class="large-12 columns">&nbsp;</div><div class="large-12 columns conversbar paddingzero"><span id="convmulitplefileuploader'+c_reply_rowid+'"></span><span title="Attachment" class="" style=""></span><textarea name="homedashboardconvid" id="replytextarea" placeholder="Enter Your Message Here..." rows="3" class="mention convmessagetoenterstyle"></textarea><input type="hidden" id="replyrowid"/><input type="hidden" id="actionid" value="'+actionid+'"/><div class="atsearchdiv hidedisplay"></div><span style="" id="" class="sendbtnstyle replyactionsave"><i class="material-icons">send</i></span><span style="" id="" class="sendbtnstyle replyactioncancel"><i class="material-icons">cancel</i></span><span class="attachdisplay"></span></span></span></div><div class="large-12 columns">&nbsp;</div></div>');
		$('#replyrowid').val(c_reply_rowid);
		//removes the on reply area
		$('.replyactioncancel').click(function(){
			$(this).parent('div').parent('div').remove();
		});
		//on reply submit
		$('.replyactionsave').click(function(){
			//get the reply data
			var actionid=$('#actionid').val();
			var reply_rowid=$('#replyrowid').val();
			var reply_message=$('#replytextarea').val();
			var file=$('#cc_filepath'+actionid+'').val();
			var filename=$('#c_fileid'+actionid+'').text();
			if(checkValue(reply_rowid) == true  && checkValue(reply_message) == true) {
				var status = conversationreplyinsert(reply_rowid,reply_message,file,filename);
				if(status == true) {
					$(".totalviewcontainer").remove();
					$(".viewcancelaction:eq(" + i + ")").trigger('click');
					$(this).parent('div').parent('div').remove();
					//append recent-comments
					var cc='commentbox'+reply_rowid;	
					$("#"+actionid+"").trigger('click');		
				}
			} else {
				alertpopup("Please enter the data in container");
			}
		});
		$('.mention').smention(""+base_url+""+"Home/getuser",{
			after : " "
		});
		// GRA- upload container
		{
			setTimeout(function() {		
			$('.fileuploaddraganddpcls').attr('disabled','disabled');
			$(".fileuploaddraganddpcls").hide();
			$(".ajax-file-upload-container").remove();
			$(".ajax-upload-dragdrop").hide();
			$('.convfileupload'+c_reply_rowid+'').hide();
			},100);
			fileuploadmoname = 'convfileupload'+c_reply_rowid+'';
			var conversationsettings1 = {
				url: base_url+"upload.php",
				method: "POST",
				fileName: "myfile",
				multiple:false,
				showStatusAfterSuccess: false,
				allowedTypes: "*",
				dataType:'json',
				async:false,
				dragDropStr:"",
				uploadStr:"",
				onSuccess:function(files,data,xhr) {	
					if(data != "MaxSize" && data != "Size" && data != "MaxFile"){
						var arr = jQuery.parseJSON(data);	
						var c_filepath=arr.path;
						$('.convfileupload'+c_reply_rowid+'').show();
						$('#cc_filepath'+c_reply_rowid+'').val(c_filepath);
						$('#c_fileid'+c_reply_rowid+'').text(arr.fname);
					}else if(data == "Size"){
						alertpopupdouble("File Size exceeds the limit. You can upload upto 5mb at a time.");
					} else if(data == "MaxSize"){
						alertpopupdouble("You have reached your maximun storage limit. if you want to upload documents you want to buy the storage limit from add ons");
					} else if(data == "MaxFile"){
						alertpopupdouble("Maximum number fo files exceeded.");
					}else{
						alertpopupdouble("Upload Failed");
					}
				},
				onError: function(files,status,errMsg) {
				}
			}
			$('#convmulitplefileuploader'+c_reply_rowid+'').uploadFile(conversationsettings1);
			$('.uploadattachments'+c_reply_rowid+'').click(function(){
				$("#convmulitplefileuploader"+c_reply_rowid+":last>.ajax-upload-dragdrop>.ajax-file-upload>form>input:last").trigger('click');
			});
			{//file upload removal-
				$('#removefile'+actionid+'').click(function(){
					$('#c_fileid'+c_reply_rowid+'').text('');
					$('#cc_filepath'+c_reply_rowid+'').val('');
					$('#cc_filename'+c_reply_rowid+'').val('');
					$(".convfileupload"+c_reply_rowid+"").hide();
				});
			}
		}
	});
	//on click its shows the reply's to that post
	$(".viewaction").click(function() {
		var actionid=$(this).attr("id");
		var c_comment_rowid=$(this).attr("data-rowid");
		var i=$(this).attr("data-eq");
		var commentdata=loadcommentdata(c_comment_rowid);		
		if(checkValue(c_comment_rowid) == true && commentdata.length > 0) {
			//vshow,vcancel.
			$(".viewaction:eq("+i+")").hide();
			$(".vcancel:eq("+i+")").removeClass('hidedisplay');
			var employeeurl=base_url+'Employee';		
			for(var i=0; i<commentdata.length;i++) {
				var imgurl = base_url+"img/blackuserimg.png";
				var replyeditablefield='';
				var rcount = commentdata[i]['count'];
				if(commentdata[i]['profile'] !== null){	
					var imgurl = '<img class="convers-user-img" id="" title="" src="'+commentdata[i]['profile']+'">';
				}else{
					var imgurl = '<div class="convers-user-icon"><span class=""><i class="material-icons">account_circle</i></span></div>';
				}
				if( rcount != 0){
					var replycount = commentdata[i]['count']+' reply';
				} else {
					var replycount = '';
				}
				if(commentdata[i]['profile'] != '' || commentdata[i]['profile'] !== null || commentdata[i]['profile'] !== 'undefined'){
					if(commentdata[i]['profile']) {
						if(commentdata[i]['fromid'] == '2' ) {
							var imgurl='<img class="convers-user-img" id="" title="" src="'+base_url+commentdata[i]['profile'];+'">';
						} else {
							var imgurl = '<img class="convers-user-img" id="" title="" src="'+commentdata[i]['profile']+'">';
						}
					}
				}
				if (commentdata[i]['editable'] == "yes") {
					var replyeditablefield='<span class="convopicons creply_delete" data-rowid="'+commentdata[i]['c_id']+'" data-actionid="'+actionid+'"  title="Delete" data-eq="'+i+'"><i class="material-icons">delete</i></span>';
				}
				var commentbox='commentbox'+c_comment_rowid;
				if(commentdata[i]['filepath']) {
					var filefolder=base_url+commentdata[i]['filepath'];
					var file=commentdata[i]['filename'];
					var document= '<div class="large-6 columns" style="text-align:left"><span class="convfiledownload">Attached File : <a class="c_filedownload" data-file="'+filefolder+'">'+file+'</a></span></div>';
				} else {
					var document= '<div class="large-6 columns" style="text-align:left"></div>';
				}
				$(this).parent('div').parent('div').parent('div').parent('div').append('<div class="large-12 columns mblnopadding"><div class="large-12 columns paddingzero rtotalviewcontainer totalviewcontainer'+c_comment_rowid+'"><div class="large-12 columns mblnopadding"> &nbsp;</div><div class="large-12 medium-12 small-12 columns mblnopadding totalcotainerconv '+commentbox+'"><div class="large-1 small-1 column paddingzero">'+imgurl+'</div><div class="large-11 small-11 columns talk-bubble tri-right left-top z-depth-1"><div class="large-12 columns paddingzero"><div class="large-6 columns paddingzero"><div class="large-6 column paddingzero"><div class="large-6 columns conversationnamestyle" style="color:#df6c6e"><a href='+employeeurl+' data-moduleid="4" target="_blank" data-rowid='+commentdata[i]['userid']+'>'+commentdata[i]['username']+'</a></div><div class="large-12 columns conversationtitlestyle"></div></div></div><div style="text-align: right; opacity: 0;" class="large-6 columns paddingzero iconsetconv"><span style="font-size: 1rem; vertical-align: top; line-height: 24px;" data-id="replycount'+commentdata[i]['c_id']+'">'+replycount+'</span>&nbsp;'+replyeditablefield+'</div><div class="large-12 columns messagecontentstyle paddingzero">'+commentdata[i]['c_name']+'</div></div><div class="large-12 columns paddingzero"><span class="conversationtime" title="'+commentdata[i]['c_date']+'">'+commentdata[i]['time']+'</span></div></div></div></div></div>');
				//reply edit
				$('.rreplyedit'+commentdata[i]['c_id']+'').click(function() {
					var id = $(this).attr('id');
					var rowid = $("#"+id+"").attr('data-rowid');
					var actionid = $("#"+id+"").attr('data-actionid');
					var eq = $("#"+id+"").attr('data-eq');
					var rclass = $(".totalviewcontainer ");
					replyeditcontainerfun(rclass,id,rowid,eq,actionid);
				});
			}
			// Color Change
			var colors = ["#575757","#575757","#575757"];                
			var rand = Math.floor(Math.random()*colors.length);           
			//reply hovers
			$( ".rtotalviewcontainer" ).hover(function() {
				$( this ).find('.iconsetconv').css('opacity','1');}, function(){
				$( this ).find('.iconsetconv').css('opacity','1');
			});
			$(".replyviewaction").click(function() {
				var id = $(this).attr('id');
				var rowid = $("#"+id+"").attr('data-rowid');
				var eq = $("#"+id+"").attr('data-eq');
				var rclass = "replytotalviewcontainer"+rowid+"";
				replymoreconversationblog(rclass,id,rowid,eq);
			});
			$(".replyviewcancelaction").click(function() {
				var i=$(this).attr("data-eq");
				var rowid = $(this).attr('data-rowid');
				$(".rreplytotalviewcontainer"+rowid+"").remove();
				$("#replyviewaction"+rowid+"").show();
				$(".vcancel"+rowid+"").addClass('hidedisplay');
			});
			$(".rreplyaction").click(function() {
				var id = $(this).attr('id');
				var actionid = $(this).attr('data-actionid');
				var rowid = $("#"+id+"").attr('data-rowid'); 
				var eq = $("#"+id+"").attr('data-eq');
				var rclass = $(".rreplytotalviewcontainer"+rowid+"");
				replyactioncontainerfun(rclass,id,rowid,eq,actionid);
			});
			//conversation delete
			$('.creply_delete').click(function() {
				var appendid=$('#conversationdatablog').val();
				var c_delete_rowid= $(this).attr('data-rowid');
				var rclass = $(".rtotalviewcontainer");
				var i=$(this).attr("data-eq");
				if(c_delete_rowid !='' ||c_delete_rowid != null){
					$.ajax({
						url:base_url+"Home/conversationdelete",
						data:{rowid:c_delete_rowid},
						type:"POST",
						async:false,
						cache:false,
						success :function(msg) {
							//remove the specific reply
							rclass.remove();
							//append recent-comments
							$("#"+actionid+"").trigger('click');
						}
					});
				}
			});
		}
		$(".c_filedownload").click(function(){
			var file=$(this).attr('data-file');
			if(checkValue(file) == true){
				var ExportPath = base_url+'Home/download';
				var filepath=base_url+file;					
				var path=file;
				var form = $('<form action="'+ExportPath +'" class="hidedisplay" name="filedownload" id="filedownload" method="POST" target="_blank"><input type="hidden" name="path" id="path" value="'+file+'" /><input type="hidden" name="filename" id="filename" value="'+path+'" /></form>');
				$(form).appendTo('body');
				form.submit();
			}
		});
	});
	//hide the view box
	$(".viewcancelaction").click(function(){
		var i=$(this).attr("data-eq"); //
		var rowid=$(this).attr("data-rowid");
		$(".totalviewcontainer"+rowid+"").remove();
		$(".viewaction:eq("+i+")").show();
		$(".vcancel:eq("+i+")").addClass('hidedisplay');
	});
}
//load the comment data for a conversation
function loadcommentdata(c_comment_rowid) {
	var mdata=[];
	$.ajax({
		url:base_url+"Home/getconversationcomment",
		data: {rowid:c_comment_rowid,lastnode:0},			
		type:"POST",
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {	
			mdata=data;
		}
	});
	return mdata;
}
//insert reply to conversation data
function conversationreplyinsert(reply_rowid,reply_message,file,filename) {
	var status='';		
	$.ajax({
		url:base_url+"Conversation/createreply",
		data: {lastnode:0,message:reply_message,rowid:reply_rowid,file:file,filename:filename},
		async:false,			
		type:"POST",
		cache:false,
		success :function(msg) {							
			status=msg;	
		}
	});
	return status;		
}

//edit container fetch GRA
function replyeditcontainerfun(rclass,id,c_reply_rowid,i,actionid) {
	$('.replyeditcontainer').remove();
	$('.replyactioncontainer').remove();
	$("#"+id+"").parent('div').parent('div').parent('div').parent('div').append('<div class="large-12 columns paddingzero replyeditcontainer"><textarea class="mention messagetoenterstyle" rows="2" id="homedashboardconvid'+c_reply_rowid+'" name="homedashboardconvid"></textarea><input type="hidden" id="unique_rowid'+c_reply_rowid+'"/><input type="hidden" id="actionid" value="'+actionid+'"/><div class="atsearchdiv hidedisplay"></div><div style="background:#cccccc;" class="large-12 columns paddingzero"><div class="large-6 columns"><span id="convmulitplefileuploader'+c_reply_rowid+'"></span><span style="line-height:1.6rem;display:inline-block;top:0.3rem;position:relative;"><span style="position:relative;cursor:pointer;" class=" uploadattachments'+c_reply_rowid+'" title="Attachment"><i class="material-icons">attachment</i></span><span style="line-height:1.2rem;"><span class="convfileupload" style="font-size:0.9rem;position:relative;"> : <span id="c_fileid'+c_reply_rowid+'" data-filepath="" class="uploadattachspan"></span><input type="hidden" id="cc_filepath'+c_reply_rowid+'"/><span id="removefile" class="" style="margin-left:5px;position:relative;cursor:pointer;top: 0.3rem;"><i class="material-icons" title="close">close</i></span></span></span></div><div class="large-6 columns" style="text-align:right"><span class="postbtnstyle " id="editpostsubmit'+c_reply_rowid+'">Submit</span><span class="postbtnstyle editpostcancel" id="editpostcancel">Cancel</span></div></div><div class="large-12 columns">&nbsp;</div></div>');
	
	if(c_reply_rowid !='' ||c_reply_rowid != null){
		$.ajax({
			url:base_url+"Home/getconversationeditdata",
			data:{rowid:c_reply_rowid},
			type:"POST",
			async:false,
			dataType:'json',
			cache:false,
			success :function(data) {
				//set the data
				setTimeout(function(){
					$('#homedashboardconvid'+c_reply_rowid+'').val(data.c_message);
					$('#unique_rowid'+c_reply_rowid+'').val(c_reply_rowid);
					if(checkValue(data.c_file) == true){
						$(".convfileupload").show();
						$('#cc_filepath'+c_reply_rowid+'').val(data.c_filepath);
						$('#c_fileid'+c_reply_rowid+'').text(data.c_file);
					}
					$('#homedashboardconvid').val(data.c_message);
				},10);
				$('.addtitlestyle').trigger('click');
			}
		});
	} 
	// GRA- upload container
		{
			setTimeout(function() {		
			$('.fileuploaddraganddpcls').attr('disabled','disabled');
			$(".fileuploaddraganddpcls").hide();
			$(".ajax-upload-dragdrop").hide();
			$('.convfileupload'+c_reply_rowid+'').hide();
			},100);
			fileuploadmoname = 'convfileupload'+c_reply_rowid+'';
			var conversationsettings1 = {
				url: base_url+"upload.php",
				method: "POST",
				fileName: "myfile",
				multiple:false,
				showStatusAfterSuccess: false,
				allowedTypes: "*",
				dataType:'json',
				async:false,
				onSuccess:function(files,data,xhr) {
					if(data != "MaxSize" && data != "Size" && data != "MaxFile"){
						var arr = jQuery.parseJSON(data);
						var c_filepath=arr.path;
						$('.convfileupload'+c_reply_rowid+'').show();
						$('#cc_filepath'+c_reply_rowid+'').val(c_filepath);
						$('#c_fileid'+c_reply_rowid+'').text(arr.fname);
					}else if(data == "Size"){
						alertpopupdouble("File Size exceeds the limit. You can upload upto 5mb at a time.");
					} else if(data == "MaxSize"){
						alertpopupdouble("You have reached your maximun storage limit. if you want to upload documents you want to buy the storage limit from add ons");
					} else if(data == "MaxFile"){
						alertpopupdouble("Maximum number fo files exceeded.");
					}else{
						alertpopupdouble("Upload Failed");
					}
				},
				onError: function(files,status,errMsg) {
				}
			}
			$('#convmulitplefileuploader'+c_reply_rowid+'').uploadFile(conversationsettings1);
			$('.uploadattachments'+c_reply_rowid+'').click(function(){
				$("#convmulitplefileuploader"+c_reply_rowid+":last>.ajax-upload-dragdrop>.ajax-file-upload>form>input:last").trigger('click');
			});
			{//file upload removal-
				$('#removefile').click(function(){	
					$('#c_fileid'+c_reply_rowid+'').text('');
					$('#cc_filepath'+c_reply_rowid+'').val('');
					$('#cc_filename'+c_reply_rowid+'').val('');
					$(".convfileupload").hide();
				});
			}
		}	$('#unique_rowid'+c_reply_rowid+'').val(c_reply_rowid);
	//removes the on reply area
	$('.editpostcancel').click(function(){
		$('.replyeditcontainer').remove();
	});
	$('.mention').smention(""+base_url+""+"Home/getuser",{
		after : " "
	});
}
//reply container fetch
function replyactioncontainerfun(rclass,id,c_reply_rowid,i,actionid) {
	$('.replyactioncontainer').remove();
	$('.replyeditcontainer').remove();
	$("#"+id+"").parent('div').parent('div').parent('div').parent('div').append('<div class="large-12 columns paddingzero replyactioncontainer"><div class="large-12 columns">&nbsp;</div><div class="large-12 columns"><textarea id="replytextarea" class="mention" placeholder="Enter your reply here..."></textarea><input type="hidden" id="replyrowid"/><input type="hidden" id="actionid" value="'+actionid+'"/><div class="atsearchdiv hidedisplay"></div><div style="background:#cccccc;" class="large-12 columns paddingzero"><div class="large-12 columns"><span id="convmulitplefileuploader'+c_reply_rowid+'"></span><span style="line-height:1.6rem;display:inline-block;top:0.3rem;position:relative;"><span style="position:relative;cursor:pointer;" class="uploadattachments'+c_reply_rowid+'" title="Attachment"><i class="material-icons">attachment</i></span></span><span style="line-height:1.2rem;"><span class="convfileupload'+c_reply_rowid+'" style="font-size:0.9rem;position:relative;"> : <span id="c_fileid'+c_reply_rowid+'" data-filepath="" class="uploadattachspan"></span><input type="hidden" id="cc_filepath'+c_reply_rowid+'"/><span id="removefile'+c_reply_rowid+'" class="" style="margin-left:5px;position:relative;cursor:pointer;top: 0.3rem;"><i class="material-icons" title="close">close</i></span></span></span></div></div><div class="large-12 columns">&nbsp;</div><div class="large-12 columns"><span class="postbtnstyle replyactionsave">Reply</span><span class="postbtnstyle rreplyactioncancel">Cancel</span></div><div class="large-12 columns">&nbsp;</div></div>');
	
	// GRA- upload container
		{
			setTimeout(function() {		
			$('.fileuploaddraganddpcls').attr('disabled','disabled');
			$(".fileuploaddraganddpcls").hide();
			$(".ajax-file-upload-container").remove();
			$(".ajax-upload-dragdrop").hide();
			$('.convfileupload'+c_reply_rowid+'').hide();
			},100);
			fileuploadmoname = 'convfileupload'+c_reply_rowid+'';
			var conversationsettings1 = {
				url: base_url+"upload.php",
				method: "POST",
				multiple:false,
				showStatusAfterSuccess: false,
				fileName: "myfile",
				allowedTypes: "*",
				dataType:'json',
				async:false,
				onSuccess:function(files,data,xhr) {
					if(data != "MaxSize" && data != "Size" && data != "MaxFile"){
						var arr = jQuery.parseJSON(data);
						$('.convfileupload'+c_reply_rowid+'').show();
						$('#cc_filepath'+c_reply_rowid+'').val(arr.path);
						$('#c_fileid'+c_reply_rowid+'').text(arr.fname);
					}else if(data == "Size"){
						alertpopupdouble("File Size exceeds the limit. You can upload upto 5mb at a time.");
					} else if(data == "MaxSize"){
						alertpopupdouble("You have reached your maximun storage limit. if you want to upload documents you want to buy the storage limit from add ons");
					} else if(data == "MaxFile"){
						alertpopupdouble("Maximum number fo files exceeded.");
					}else{
						alertpopupdouble("Upload Failed");
					}
				},
				onError: function(files,status,errMsg) {
				}
			}
			$('#convmulitplefileuploader'+c_reply_rowid+'').uploadFile(conversationsettings1);
			$('.uploadattachments'+c_reply_rowid+'').click(function(){
				$("#convmulitplefileuploader"+c_reply_rowid+":last>.ajax-upload-dragdrop>.ajax-file-upload>form>input:last").trigger('click');
			});
			{//file upload removal-
				$('#removefile'+c_reply_rowid+'').click(function(){	
					$('#c_fileid').text('');
					$('#cc_filepath'+c_reply_rowid+'').val('');
					$('#cc_filename'+c_reply_rowid+'').val('');
					$(".convfileupload"+c_reply_rowid+"").hide();
				});
			}
			}	$('#replyrowid').val(c_reply_rowid);
	//removes the on reply area
	$('.rreplyactioncancel').click(function(){
		$(this).parent('div').parent('div').remove();
	});
	//on reply submit
	$('.replyactionsave').click(function(){
		//get the reply data
		var reply_rowid=$('#replyrowid').val();
		var actionid=$('#actionid').val();
		var reply_message=$('#replytextarea').val();
		var file=$('#cc_filepath'+reply_rowid+'').val();
		var filename=$('#c_fileid'+reply_rowid+'').text();
		if(checkValue(reply_rowid) == true  && checkValue(reply_message) == true) {
			var status=conversationreplyinsert(reply_rowid,reply_message,file,filename);
			if(status == true) {
				rclass.remove();
				$(this).parent('div').parent('div').remove();
				$("#"+actionid+"").trigger('click');
				//append recent-comments
				var cc='commentbox'+reply_rowid;					
			}
		} else {
			alertpopup("No Empty Reply");
		}
	});
	$('.mention').smention(""+base_url+""+"Home/getuser",{
		after : " "
	});
}
//reply conversation data blog
function replymoreconversationblog(rclass,id,rowid,eq) {
	$("."+rclass+"").remove();
	var c_comment_rowid=rowid;
	var i=eq;
	var commentdata=loadcommentdata(c_comment_rowid);
	if(checkValue(c_comment_rowid) == true && commentdata.length > 0) {
		//vshow,vcancel.
		$("#"+id+"").hide();
		$("#replyviewaction"+rowid+":eq("+i+")").hide();
		$(".vcancel"+rowid+"").removeClass('hidedisplay');
		var employeeurl=base_url+'Employee';		
		for(var i=0; i<commentdata.length;i++) {
			var imgurl = base_url+"img/blackuserimg.png";
			var replyeditablefield='';
			if(commentdata[i]['profile'] !== null){	
				var imgurl = '<img class="convers-user-img" id="" title="" src="'+commentdata[i]['profile']+'">';
			}else{
				var imgurl = '<div class="convers-user-icon"><span class=""><i class="material-icons">account_circle</i></span></div>';
			}
			if(commentdata[i]['profile'] != '' || commentdata[i]['profile'] !== null || commentdata[i]['profile'] !== 'undefined') {
				if(commentdata[i]['profile']) {
					if(commentdata[i]['fromid'] == '2' ) {
						var imgurl=base_url+commentdata[i]['profile'];
					} else {
						var imgurl = commentdata[i]['profile'];
					}
				}
			}
			if (commentdata[i]['editable'] == "yes") {
				var replyeditablefield='<span class="convopicons rreplyedit'+commentdata[i]['c_id']+'" id="rreplyedit'+commentdata[i]['c_id']+'" data-eq="'+i+'" data-actionid="'+id+'" data-rowid="'+commentdata[i]['c_id']+'" data-parentid="'+rowid+'" title="Edit"><i class="material-icons">edit</i></span><span class="convopicons creply_delete" data-rowid="'+commentdata[i]['c_id']+'" data-parentid="'+rowid+'" title="Delete" data-eq="'+i+'"><i class="material-icons">delete</i></span>';
			}
			var rcount = commentdata[i]['count'];
			if( rcount != 0) {
				var replycount = commentdata[i]['count']+' reply';
			} else {
				var replycount = '';
			}
			var commentbox='commentbox'+c_comment_rowid;		
			if(commentdata[i]['filepath']) {
				var filefolder=base_url+commentdata[i]['filepath'];
				var file=commentdata[i]['filename'];
				var document= '<div class="large-6 columns" style="text-align:left"><span class="convfiledownload">Attached File : <a class="c_filedownload" data-file="'+filefolder+'">'+file+'</a></span></div>';
			} else {
				var document= '<div class="large-6 columns" style="text-align:left"></div>';
			}
			$("#"+id+"").parent('div').parent('div').parent('div').parent('div').append('<div class="large-12 columns paddingzero rreplytotalviewcontainer'+c_comment_rowid+' "><div class="large-12 columns"> &nbsp;</div><div class="large-12 columns mblnopadding"><div class="large-12 medium-12 small-12 columns mblnopadding totalcotainerconv'+c_comment_rowid+' '+commentbox+'"><div class="large-1 small-1  column paddingzero">'+imgurl+'></div><div class="large-11 columns converstooltip" style="border:1px solid #cccccc; border-radius:6px;"><div class="large-9 mesium-9 small-8  columns paddingzero"><div class="large-10 column paddingzero"><div class="large-12 columns conversationnamestyle" style="color:#df6c6e"><a href='+employeeurl+' data-moduleid="4" target="_blank" data-rowid='+commentdata[i][' userid ']+'>'+commentdata[i]['username']+'</a></div><div class="large-12 columns conversationtitlestyle">'+commentdata[i]['c_title']+'</div></div><div class="large-12 columns messagecontentstyle">'+commentdata[i]['c_name']+'</div></div><div class="large-3 medium-3 small-4 columns paddingzero" style="text-align:right"><span title="'+commentdata[i]['c_date']+'" class="conversationtime">'+commentdata[i]['time']+'</span></div>'+document+'<div class="large-12 columns paddingzero iconsetconv" style="text-align:right;"><span data-id="replycount'+commentdata[i]['c_id']+'">'+replycount+'</span><span class="vcancel'+commentdata[i]['c_id']+' hidedisplay"><span class="rreplyviewcancelaction convopicons" data-rowid="'+commentdata[i]['c_id']+'" data-eq="'+i+'"title="Close Comment"><i class="material-icons">visibility_off</i></span></span><span class="rreplyviewaction convopicons" id="replyviewaction'+commentdata[i]['c_id']+'" data-eq="'+i+'" data-rowid="'+commentdata[i]['c_id']+'" title="View Comment"><i class="material-icons">visibility</i></span><span class="rrreplyaction convopicons cc_reply" id="rreplyaction'+commentdata[i]['c_id']+'" data-eq="'+i+'" data-rowid="'+commentdata[i]['c_id']+'" data-actionid="replyviewaction'+commentdata[i]['c_id']+'" title="Reply"><i class="material-icons">reply</i></span>'+replyeditablefield+'</div></div></div></div></div>');
		
				//reply edit
				$('.rreplyedit'+commentdata[i]['c_id']+'').click(function() {
					var id = $(this).attr('id');
					var rowid = $("#"+id+"").attr('data-rowid'); 
					var actionid = $("#"+id+"").attr('data-actionid');
					var parentid = $("#"+id+"").attr('data-parentid');
					var eq = $("#"+id+"").attr('data-eq');
					var rclass = $(".rreplytotalviewcontainer"+parentid+"");
					replyeditcontainerfun(rclass,id,rowid,eq,actionid);
				});
				// Color Change
				var colors = ["#df6c6e","#df6c6e","#df6c6e"];                
				var rand = Math.floor(Math.random()*colors.length);           
				$(".c_filedownload").click(function(){
					var file=$(this).attr('data-file');
					if(checkValue(file) == true){
						var ExportPath = base_url+'Home/download';
						var filepath=base_url+file;					
						var path=file;
						var form = $('<form action="'+ExportPath +'" class="hidedisplay" name="filedownload" id="filedownload" method="POST" target="_blank"><input type="hidden" name="path" id="path" value="'+file+'" /><input type="hidden" name="filename" id="filename" value="'+path+'" /></form>');
						$(form).appendTo('body');
						form.submit();
					}
				});
			
		}
	}
		//reply hovers
		$( ".totalviewcontainer" ).hover(function() {
			$( this ).find('.iconsetconv').css('opacity','1');}, function(){
			$( this ).find('.iconsetconv').css('opacity','1');
		});
		$(".rreplyviewaction").click(function() {
			var id = $(this).attr('id');
			var rowid = $(this).attr('data-rowid');
			var eq = $(this).attr('data-eq');
			var rclass = "rreplytotalviewcontainer"+rowid+"";
			replymoreconversationblog(rclass,id,rowid,eq);
		});
		$(".rreplyviewcancelaction").click(function() {
			var i=$(this).attr("data-eq");
			var rowid = $(this).attr('data-rowid');
			$(".rreplytotalviewcontainer"+rowid+"").remove();
			$("#replyviewaction"+rowid+"").show();
			$(".vcancel"+rowid+"").addClass('hidedisplay');
		});
		$(".rrreplyaction").click(function() {
			var id = $(this).attr('id');
			var rowid = $("#"+id+"").attr('data-rowid');
			var actionid = $(this).attr('data-actionid'); 
			var eq = $("#"+id+"").attr('data-eq');
			var rclass = $(".rreplytotalviewcontainer"+rowid+"");
			replyactioncontainerfun(rclass,id,rowid,eq,actionid);
		});
	}
//view creation field get
function autorulecreationfieldget(ids) {
	$.ajax({
		url: base_url + "Conversation/filedsdropdownload?ids="+ids+'&autonum=no',
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$('#autocolumnname').empty();
			$('#autocolumnname').append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#autocolumnname')
					.append($("<option></option>")
					.attr("data-ffname",data[index]['dataname'])
					.attr("data-jointable",data[index]['jointable'])
					.attr("data-indexname",data[index]['indexname'])
					.attr("data-uitype",data[index]['uitype'])
					.attr("data-id",data[index]['datasid'])
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}
		},
	});
}
function showhideintextbox(hideid,showid) {
	$("#"+hideid+"divhid").hide();
	$("#"+showid+"divhid").show();
}
//field name based drop down value
function fieldviewnamebespicklistdddvalue(indexname){
	var moduleid = $("#autorulemoduleid").val();
	$.ajax({
		url: base_url + "Conversation/fieldviewnamebespicklistdddvalue?fieldname="+indexname+"&moduleid="+moduleid,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$('#autoddcondvalue').empty();
			$('#autoddcondvalue').append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#autoddcondvalue')
					.append($("<option></option>")
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}
			$('#autoddcondvalue').trigger('change');			
		},
	});
}
//field name based drop down value
function fieldviewnamebesdddvalue(indexname){
	var moduleid = $("#autorulemoduleid").val();
	$.ajax({
		url: base_url + "Conversation/viewfieldnamebesdddvalue?fieldname="+indexname+"&moduleid="+moduleid,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$('#autoddcondvalue').empty();
			$('#autoddcondvalue').append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#autoddcondvalue')
					.append($("<option></option>")
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}
			$('#autoddcondvalue').trigger('change');			
		},
	});
}
//check box value get
function newcheckboxvalueget(dropdownname) {
	$.ajax({
		url: base_url + "Massupdatedelete/newcheckboxvalueget",
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$("#"+dropdownname+"").empty();
			$("#"+dropdownname+"").append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$("#"+dropdownname+"")
					.append($("<option></option>")
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}
			$("#"+dropdownname+"").trigger('change');			
		},
	});
}
//auto rule insertion
function autoruleinsertion() {
	var rulename = $("#autorulename").val();
	var moduleid = $("#autorulemoduleid").val();
	var publicc = $("#autorulepublic").val();
	if(publicc){
		publicc = 'No';
	}
	//rule criteria cond
	if(deviceinfo != 'phone') {
		var criteriacnt = $('#autofollowconditiongrid .gridcontent div.data-content div').length;
	} else {
		var criteriacnt = $('#autofollowconditiongrid .gridcontent div.wrappercontent div').length;
	}	
	var griddata = getgridrowsdata('autofollowconditiongrid');
	var criteriagriddata = JSON.stringify(griddata);
	$.ajax({
		url: base_url + "Conversation/autoruleinsertion",
		data:"rulename="+rulename+"&moduleid="+moduleid+"&public="+publicc+"&criteriacnt="+criteriacnt+"&criteriagriddata="+criteriagriddata,
		type:'post',
		cache:false,
        success: function(data) {
			var msg = $.trim(data);
			if(msg=='TRUE') {	
				alertpopup('Auto rule inserted successfully');
				cleargriddata('autofollowconditiongrid');
				$("#moduleid").select2('val','');
				autofollowgrid();
				resetFields();	
				$("#autofollowaddform").hide();
				$("#followedbrmearea").show();
			}
		}
	});
}
//auto follow rule delete
function autofollowruledelete(datarowid) {
	$.ajax({
		url: base_url + "Conversation/autoruledelete",
		data:"datarowid="+datarowid,
		type:'post',
		cache:false,
        success: function(data) {
			var msg = $.trim(data);
			if(msg=='TRUE') {	
				alertpopup('Auto rule deleted successfully');
				autofollowgrid();
				$("#basedeleteoverlay").fadeOut();
			}
		}
	});
}
//auto follow rule edit 
function autofollowruleeditdatafetch(datarowid) {
	$.ajax({
		url: base_url + "Conversation/autoruleeditdatafetch",
		data:"datarowid="+datarowid,
		type:'post',
		cache:false,
		dataType: 'json',
		async:false,
        success: function(data) {
			var rulename = data['rulename'];
			var module = data['module'];
			var publicc = data['publicc'];
			$("#autoruleid").val(datarowid);
			$("#autorulename").val(rulename);
			$("#autorulemoduleid").select2('val',module).trigger('change');
			if (publicc == 'Yes') {
				$('#autorulepublic').attr('checked',true);
				$('#autorulepublic').val('Yes');
			} else {
				$('#autorulepublic').attr('checked',false);
				$('#autorulepublic').val('No');
			}
			setTimeout(function() {
				ruleconditioneditdatafetch(datarowid);
			},10);
			$("#processoverlay").hide();
		}
	});
}
function ruleconditioneditdatafetch(datarowid) {
	if(datarowid!='') {
		$.ajax({
			url:base_url+'Conversation/autofollowruledconditionfetchgrid?primarydataid='+datarowid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					loadinlinegriddata('autofollowconditiongrid',data.rows,'json');
					/* data row select event */
					datarowselectevt();
					/* column resize */
					columnresize('autofollowconditiongrid');
				}
			},
		});
	}
}
//auto follow rule update
function autoruleupdate() {
	var ruleid = $("#autoruleid").val();
	var rulename = $("#autorulename").val();
	var moduleid = $("#autorulemoduleid").val();
	var publicc = $("#autorulepublic").val();
	var conrowcolids = $("#conditiondeleteid").val();
	if(deviceinfo != 'phone'){
		var criteriacnt = $('#autofollowconditiongrid .gridcontent div.data-content div').length;
	}else{
		var criteriacnt = $('#autofollowconditiongrid .gridcontent div.wrappercontent div').length;
	}
	var griddata = getgridrowsdata('autofollowconditiongrid');
	var criteriagriddata = JSON.stringify(griddata);
	$.ajax({
		url: base_url + "Conversation/autoruleupdate",
		data:"rulename="+rulename+"&moduleid="+moduleid+"&public="+publicc+"&criteriacnt="+criteriacnt+"&criteriagriddata="+criteriagriddata+"&ruleid="+ruleid+"&conrowcolids="+conrowcolids,
		type:'post',
		cache:false,
        success: function(data) {
			var msg = $.trim(data);
			if(msg=='TRUE') {	
				alertpopup('Auto rule updated successfully');
				cleargriddata('autofollowconditiongrid');
				autofollowgrid();
				resetFields();	
				$("#autofollowaddform").hide();
				$("#followedbrmearea").show();
			}
		}
	});
}
//view data fetch
function viewdatafetch(moduleid) {
	var dropdownname = "followviewid";
	$.ajax({
		url: base_url + "Conversation/viewfetchbasedonmodule?moduleid="+moduleid,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$("#"+dropdownname+"").empty();
			$("#"+dropdownname+"").append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$("#"+dropdownname+"")
					.append($("<option></option>")
					.attr("data-columnid",data[index]['columnid'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}
		},
	});
}
//follow by rule get
function followbyrule(moduleid) {
	var dropdownname = "followruleid";
	$.ajax({
		url: base_url + "Conversation/autorulefetchbasedonmodule?moduleid="+moduleid,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$("#"+dropdownname+"").empty();
			$("#"+dropdownname+"").append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$("#"+dropdownname+"")
					.append($("<option></option>")
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}
		},
	});
}
//UN follow records
function unfollowrecords(ids,moduleid) {
	$.ajax({
		url: base_url + "Conversation/unfollowrecords?moduleid="+moduleid+"&ids="+ids,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$("#"+dropdownname+"").empty();
			$("#"+dropdownname+"").append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$("#"+dropdownname+"")
					.append($("<option></option>")
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}
		},
	});
}
// Slider Control for Unnoticed records
$(function() {
    $( "#slider" ).slider({
		value:15,
		min: 1,
		max: 180,
		step: 1,
		slide: function( event, ui ) {
			$( "#amount" ).val( ui.value +  " Days"  );
		}
    });
    $( "#amount" ).val($("#slider").slider("value")+" Days");
});
function deletegriddatarows(gridname,rowid) {
	if(rowid!='') {
		if(deviceinfo != 'phone'){
			$('#'+gridname+' .gridcontent div#'+rowid).remove();
			var j=1;
		}else{
			$('#'+gridname+' .gridcontent div#'+rowid).remove();
		}		
	}
}
//AND/OR  condition validation reset
function andorcondvalidationreset() {
	var viewcount = $('#autofollowconditiongrid .gridcontent div.data-content div').length;
	if(viewcount == 0) {
		$('#autoandor').removeClass('validate[required]');
		$('.mandatoryfildclass').text('');
	} else {
		$('#autoandor').addClass('validate[required]');
		$('.mandatoryfildclass').text('*');
	}
}