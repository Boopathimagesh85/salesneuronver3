$(document).ready(function() {
	$(document).foundation();
    // Maindiv height width change
    maindivwidth();
	var maindiv = $('.maindiv').width();
	var a = screen.width;
	reportsviewgridwidth = ((a * maindiv)/100);	
	transactiontypeeditstatus = 0;
	companysettingsfieldlabel = ['weightdecisize-label',''];
	companysettingsfielddesc = [''];
	// Grid Calling Functions
    companyjqgrid();
    if(softwareindustryid == 3){
		{//Date Range
			//daterangefunction();
		}
    	transactiongrid();
    	oldjewelgrid();
		defuaultproductchargedata = $('#defaultproductchargeid').val();
		defuaultproductpurchasechargedata = $('#defaultproductpurchasechargeid').val();
		saleschargehideshow(defuaultproductchargedata,'chargeid'); // sales charge hide/show based on company setting
		saleschargehideshow(defuaultproductpurchasechargedata,'purchasechargeid'); // purchase charge hide/show based on company setting
		setTimeout(function() {
    		$('.settingsdropbox').show();
    		$('#companysettingicon').css('display','block');
		 },50);
		 devicenoautogenerate();
	}else{
    	setTimeout(function() {
    		$('.settingsdropbox').hide();
    		$('#companysettingicon').css('display','none');
    		$('#dynamicdddataview').val();
    	},50);
	}
	
    //crud action
	crudactionenable();
	//close
    var addcloseinfo = ["closeaddform", "companygriddisplay", "companyaddformdiv"]
    addclose(addcloseinfo);
    //companysettingform
    var companysettingaddcloseinfo = ["closecompanyoverlay", "companygriddisplay", "companysettingview"]
    addclose(companysettingaddcloseinfo);
    {
		var editorname = $('#editornameinfo').val();
		froalaarray=[editorname,'html.set',''];
	}
    {//transaction section open for mobile
		$("#transactionaddicon").click(function(){
			sectionpanelheight('transactionsectionoverlay');
			$("#transactionsectionoverlay").removeClass("closed");
			$("#transactionsectionoverlay").addClass("effectbox");
			Materialize.updateTextFields();
			firstfieldfocus();
		});
	}
    {//transaction section overlay close
		$("#transactionaddsectionclose").click(function(){
			$("#transactionsectionoverlay").removeClass("effectbox");
			$("#transactionsectionoverlay").addClass("closed");
		});
	}
    {//oldjewel section open for mobile
		$("#oldjeweladdicon").click(function(){
			sectionpanelheight('oldjewelsectionoverlay');
			$("#oldjewelsectionoverlay").removeClass("closed");
			$("#oldjewelsectionoverlay").addClass("effectbox");
			Materialize.updateTextFields();
			firstfieldfocus();
		});
	}
	$('#companysettingform label').click(function() {
		var detailalert = $(this).attr('data-description');
		alertpopup(detailalert);
	});
    {//oldjewel section overlay close
		$("#oldjeweladdsectionclose").click(function(){
			$("#oldjewelsectionoverlay").removeClass("effectbox");
			$("#oldjewelsectionoverlay").addClass("closed");
		});
	}
	{// View creation Function
		//view by dropdown change
		$('#dynamicdddataview').change(function(){
			refreshgrid();
		});
	}
    $(".chzn-selectcomp").select2().select2('val', 'Mobile');
	{//validation for Company Add  
		$('#dataaddsbtn').click(function(e) {
			$('.ftab').trigger('click');
			$("#formaddwizard").validationEngine('validate');
		});
		$("#formaddwizard").validationEngine({
			onSuccess: function() {
				newdataaddfun();
			},
			onFailure: function() {
				var dropdownid =['1','currencylocaleid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{
		//Branch Management
		$("#branchmanagementicon").click(function(){
			window.location =base_url+'Branch';
		});
	}
	{// For touch
		fortabtouch = 0;
	}
	{//action events refresh
		$("#reloadicon").click(function() {
			$("#processoverlay").show();
			refreshgrid();
			$('#companylogoattachdisplay').empty();
		});
		$( window ).resize(function() {
			maingridresizeheightset('companygrid');
			sectionpanelheight('transactionsectionoverlay');
			sectionpanelheight('oldjewelsectionoverlay');
		});
		//formclear resetting
		$("#formclearicon").click(function() {
			$('#companylogoattachdisplay').empty();
		});
		$("#detailedviewicon").click(function() {
			var datarowid = $('#companygrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				//Function Call For Edit
				froalaset(froalaarray);
				getformdata(datarowid);
				Materialize.updateTextFields();
				showhideiconsfun('summryclick','companyaddformdiv');
				$("#companylogodivhid").addClass('hidedisplay');
				$(".documentslogodownloadclsbtn").hide();
				$("#dataaddsbtn,#formclearicon").hide();
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				$("#tab1").trigger('click');
				setTimeout(function() {
					 $('#tabgropdropdown').select2('enable');
				 },50);
			} else {
				alertpopup("Please select a row");
			}
		});
		{// Redirected form notification overlay and widget and audit log
			var rdatarowid = sessionStorage.getItem("datarowid");		
			if(rdatarowid != '' && rdatarowid != null){
				 setTimeout(function() {
					getformdata(rdatarowid);
					showhideiconsfun('summryclick','companyaddformdiv');
					sessionStorage.removeItem("datarowid");
					Materialize.updateTextFields();
				},50);
			}
		}
		$("#editfromsummayicon").click(function() {
			showhideiconsfun('editfromsummryclick','companyaddformdiv');
			$(".documentslogodownloadclsbtn").show();
			$("#companylogodivhid").removeClass('hidedisplay');
		});
		//update company information
		$('#dataupdatesubbtn').click(function(e) {
			$("#formeditwizard").validationEngine('validate');
		});
		$("#formeditwizard").validationEngine({
			onSuccess: function() {
				updateformdata();
			},
			onFailure: function() {
				var dropdownid =['1','currencylocaleid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}	
	// company  setting overlay
	$('#companysettingicon').click(function(){
		addslideup('companygriddisplay','companysettingview');
		dropdownhover();
		getcompanydetails();
		/* On Click => Load Basic tab as first.
		*/
		$('#companytap li').removeClass('active');
		$("#companytap li:first").trigger("click");
		$("#companytap li:first").addClass("active");
		getproductchargerow();
		Materialize.updateTextFields();    
		//For Keyboard Shortcut Variables
		viewgridview = 0;
		addformview = 1;
	});
	$('.tab4').click(function(){
		setTimeout(function(){
			transactiongrid();
		},10)
	});
	$('#closecompanyoverlay').click(function(){
		//resetFields();
		 $(".ftabcmpnyoverlay").trigger('click');
		 $("#transactiontype,#salesmodeid").attr('readonly',false);
	});
	{//company settings Overlay Top Bar Code for tabbing
		$(".sidebar").click(function()
		{
			$(".tab-title").removeClass('active');
			$(this).addClass('active');
			var subfomval = $(this).data('subforms');
			$(".hiddensubform").hide();
			$("#companysettingform #subformspan"+subfomval+"").show();
		});	
	}
	// company setting update
	{
		$("#companybtn").click(function() {
			$('#transactiontype,#stocktypeid,#transactionmodeid,#transactiontotalamount,#transactionpureweight,#jeweltransactiontype,#netweightcalculationid,#jeweltypeid,#rateamount,#weightless,#salesmodeid,#oldjewelpurityid,#rateaddition,#roundingtype,#roundoffremove').attr('data-validation-engine','');
			$("#companysettingvalidate").validationEngine('validate');
		});
		jQuery("#companysettingvalidate").validationEngine({
			onSuccess: function() {
				updatecompanysetting(); 
				$('#transactiontype,#stocktypeid,#transactionmodeid,#transactiontotalamount,#transactionpureweight,#jeweltransactiontype,#netweightcalculationid,#jeweltypeid,#rateamount,#weightless,#salesmodeid,#oldjewelpurityid,#rateaddition,#roundingtype,#roundoffremove').attr('data-validation-engine','validate[required]');
			},
			onFailure: function() {			
				var dropdownid = ['17','weightdecisize','meltdecisize','amountround','rateround','lotcreationopt','counterlevel','categorylevel','counteroption','stoneoption','imageoption','taxoption','pdfoption','scaleoption','olditemdetails','tagtype','netwtcalctype'];
				dropdownfailureerror(dropdownid);
			}
		});	
	}
	$('.formtogridtransaction').click(function(){
		var field = $(this).attr('id');
		if(field == "transactiontypeadd"){
			$('#transactiontypeid').val(0);
		} else {
		}
		var transactiontype=$('#transactiontype').val();
		if(transactiontype == 14){ 
			$('#stocktypeid').attr('data-validation-engine','');
			$('.stockreq').addClass('hidedisplay');
		}else{ 
			$('#stocktypeid').attr('data-validation-engine','validate[required]');
			$('.stockreq').removeClass('hidedisplay');
		}
			$("#frmtransactionsetting").validationEngine('validate');
	});
	jQuery("#frmtransactionsetting").validationEngine({		
		onSuccess: function() {
			addform();
			Materialize.updateTextFields();
		},
		onFailure: function() {
			$("#transactiontype,#salesmodeid").attr('readonly',false);

			var dropdownid =['6','transactiontype','stocktypeid','transactionmode','transactiontotalamount','transactionpureweight'];
			dropdownfailureerror(dropdownid);
		}
	});
	//old jewel add form
	$('.formtogridoldjewel').click(function(){
		$("#oldjewelsettingform").validationEngine('validate');
	});
	jQuery("#oldjewelsettingform").validationEngine({		
		onSuccess: function() {	
			oldjeweladdform();
			Materialize.updateTextFields();
		},
		onFailure: function(){
			var dropdownid =['3','jeweltransactiontype','jewelstocktypeid','netweightcalculationid'];
			dropdownfailureerror(dropdownid);
		}
	});
	//old jewel delete
	$('#oldjeweldelete').click(function(){
		var datarowid =  $('#oldjewelgrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			if($("#transactiontypeadd").is(":disabled")){
				salertpopup("A Record Under Edit Form");
			} else {
				delteoldjeweldetails(datarowid);
			}						
		} else {
			alertpopup("Please Select The row");
		}
	});
	//transaction type refresh-form
	$("#transactiontyperefresh").click(function(){
		addchargeoperation = 'ADD';
		$('#transactiontypeedit').hide();
		$('#transactiontypeadd').show();
		clearform('transactiontypeclear');
	});
	$("#transactiontypeediticon").click(function() {		
		var datarowid = $('#transactiongrid div.gridcontent div.active').attr('id');
		if(datarowid) {		
			$('#addicon,#transactiontypeadd,#transactiontypeediticon,#deleteicon').hide();
			$('#transactiontypeedit').show();
			$('#editsave').show();
			$("#transactiontype,#salesmodeid").attr('readonly',true);
			retrive();
		} else {
			alertpopup("Please Select The row");
		}
	});
	$("#transactiontypedelete").click(function() {
		var datarowid =  $('#transactiongrid div.gridcontent div.active').attr('id');
			if(datarowid){
				if($("#transactiontypeadd").is(":disabled")){
					alertpopup("A Record Under Edit Form");
				} else {
					$("#basedeleteoverlay").fadeIn();
				}						
			} else {
				alertpopup("Please Select The row");
			}
	});
	//old jewel edit
	$("#oldjewelediticon").click(function(){
		var datarowid = $('#oldjewelgrid div.gridcontent div.active').attr('id');
		if(datarowid) {		
			$('#addicon,#oldjewelediticon,#deleteicon').hide();
			$('#oldjeweladd').hide();
			$('#oldjeweledit').show();
			oldjewelretrive();
		} else {
			alertpopup("Please Select The row");
		}
	});
	$("#basedeleteyes").click(function() {
		recorddelete();
	});
	//transaction type auto load the fields
	$('#transactiontype').change(function(){
	  if(transactiontypeeditstatus == 0){
		 $('#salesmodeid').select2('val','');
		}
		modehideshow($(this).val());
		calculationtypehideshow($(this).val());
		ttbasedstypeshowhide();
	});
	$('#salesmodeid').change(function(){
		var transactiontype = $('#transactiontype').find('option:selected').val();
		stocktypeshowhide($(this).val(),transactiontype);
		autoloadfieldvalues($(this).val(),transactiontype);
	});
	{//filter
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,companyjqgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
	}
	$("#metalpurityid").change(function(){	
				
			$("#metalpurityid option").siblings("option").prop('disabled', false);
			if($(this).val()==null || $(this).val()== '')
			{
			$("#metalpurityid option").attr("disabled", false); 
			}
			else{
			$("#metalpurityid option").filter(":selected").siblings("option").prop('disabled', true);
			}			
		 });
		$('#transactionmodeid').change(function(){
			var val = $(this).val();
			var transactiontype = $('#transactiontype').find('option:selected').val();
			var salesmodeid = $('#salesmodeid').find('option:selected').val();
			$('#transactionpureweight,#transactiontotalamount').select2('val','');
			if(val == 4){
				/* if(transactiontype == 9 && salesmodeid == 3)
				{
					$('.transactioncalc').addClass('hidedisplay');
					$('#transactionpureweight,#transactiontotalamount').attr('data-validation-engine','');
				}else{ */
					$('.transactioncalc').removeClass('hidedisplay');
					$('#transactionpureweight,#transactiontotalamount').attr('data-validation-engine','validate[required]');
				//}
			}else{
				$('.transactioncalc').addClass('hidedisplay');
				$('#transactionpureweight,#transactiontotalamount').attr('data-validation-engine','');
			}
			stocktypeshowhide($('#salesmodeid').val(),transactiontype);
		});
		//old jewel change
		$('#olditemdetails').change(function(){
			var data = $('#olditemdetails').select2('data');
			var finalResult = [];
			for( item in $('#olditemdetails').select2('data') ) {
				finalResult.push(data[item].id);
			};
			var selectid = finalResult.join(',');
		});
		$('#transactionpureweight').change(function(){
			var val = $(this).val();
			val = val.toString();
			 totalamounthideshow('transactiontotalamount',val);
		});
		$('#transactiontotalamount').change(function(){
			var val = $(this).val();
			val = val.toString();
			 totalamounthideshow('transactionpureweight',val);
		});
	{//scheme method change
		$("#schememethodid").change(function() {
			var schememethodid= $("#schememethodid").val();
			if(schememethodid == 2) {
				$("#schemedivhid").attr('class','static-field large-6 columns');
			} else {
				$("#schemedivhid").attr('class','static-field large-6 columns hidedisplay');
			}
		});
	}
	{//tax type change
		$("#oldjewelsumtaxapplyid").change(function() {
			var sumtaxapplyid = $("#oldjewelsumtaxapplyid").val();
			if(sumtaxapplyid == 1) {
				$("#oldjewelsumtaxid").attr('readonly',false);
			} else {
				$("#oldjewelsumtaxid").select2('val','').trigger('change');
				$("#oldjewelsumtaxid").attr('readonly',true);
			}
		});
	}
	$("#counteroption").change(function() {
		var val = $(this).val();
		fieldenabledisable(val,'counterlevel');
	});
	$("#lotdetailsopt").change(function() {
		var val = $(this).val();
		fieldenabledisable(val,'lotcreationopt');
	});
	$("#stoneoption").change(function() {
		var val = $(this).val();
		fieldenabledisable(val,'stonelevel');
		fieldenabledisable(val,'netwtcalctype');
	});
	$("#chargecalcoption").change(function() {
		$('#chargecalculation').select2('val','');
		var val = $(this).val();
		chargecalchideshow(val);
	});
	{// Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
	{// Function For GST
		//in company setting gst enable /disable -- if yes - gst tab will display. if not gst tab will be in hidden mode
		if($("#gstapplicableid").val() == 1) {
			$("#tab3").removeClass('hidedisplay');
		} else {
			$("#tab3").addClass('hidedisplay');
		}
		//gst rate check box
		$("#taxmasteriddivhid").hide();
		$('#gstratescboxid').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
				$("#taxmasteriddivhid").show();
			} else {
				$('#'+name+'').val('No');
				$("#taxmasteriddivhid").hide();
			}
		});
		//lut/bond details check box
		$("#applicablefromdatedivhid,#applicabletodatedivhid,#lutbondnumberdivhid").hide();
		$('#lutbonddetailscboxid').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
				$("#applicablefromdatedivhid,#applicabletodatedivhid,#lutbondnumberdivhid").show();
			} else {
				$('#'+name+'').val('No');
				$("#applicablefromdatedivhid,#applicabletodatedivhid,#lutbondnumberdivhid").hide();
			}
		});
	}
	$('#lockingtime').timepicker({
		'step':15,
		'timeFormat': 'H:i',
		'scrollDefaultNow':true,
		'disableTextInput':true,//restrict text input in time picker
		'disableTouchKeyboard':true,
	});
	$('#lockingtime').on('changeTime', function() {
		$('#lockingtime').focus();
	});
	$('#updatingtime').timepicker({
		'step':15,
		'timeFormat': 'H:i',
		'scrollDefaultNow':true,
		'disableTextInput':true,//restrict text input in time picker
		'disableTouchKeyboard':true,
	});
	$('#updatingtime').on('changeTime', function() {
		$('#updatingtime').focus();
	});
	$('#oldjewelvoucher').change(function(){
		var val = $(this).find('option:selected').val();
		$('#oldjewelvoucheredit').select2('val',val).trigger('change');
	});
	// Check for Updates
	$(document).on( "click", "#checkforupdatesicon", function() {
		var datarowid = $('#companygrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			$.ajax({
				url: base_url+"Company/checkforupdates",
				data: {datarowid:datarowid},
				type: "GET",
				cache:false,
				success: function(data) {
					if(data == 'No Release') {
						alertpopup('No Release were updated');
					} else if(data == 'Failed') {
						alertpopup('Please contact SalesNeuron Admin!');
					} else {
						$('.ajaxupdateData').html(data);
						$('#releasedetailsoverlay').fadeIn();
					}
				},
			});
		} else {
			alertpopup("Please Select The row");
		}
	});
	// Overlay close
	$(document).on( "click", ".overlayclose", function() {
		$('.ajaxupdateData').html('');
		$('#releasedetailsoverlay').fadeOut();
	});
	// Updater call for check amc details and email check
	$(document).on( "click", ".checkamcdetails", function() {
		var compid = $('#hiddencompanyid').val();
		if(compid) {
			$.ajax({
				url: base_url+"Company/checkamcdetails",
				data: {compid:compid},
				type: "POST",
				cache:false,
				success: function(data) {
					if(data == 'active') {
						$('#releasedetailsoverlay').fadeOut();
						$('#verificationcodeoverlay').fadeIn();
					} else if(data == 'inactive') {
						alertpopup('AMC is inactive! Please contact Salesneuron Admin!');
					} else {
						alertpopup('Please contact SalesNeuron Admin!');
					}
				},
			});
		} else {
			alertpopup("Company Id not found. Please try again!");
		}
	});
	// Check verification code and verification time check
	$(document).on( "click", ".verificationcodesubmit", function() {
		var compid = $('#hiddencompanyid').val();
		var verificationcode = $('#checkverificationcode').val();
		if(compid) {
			$.ajax({
				url: base_url+"Company/checkverificationcodeanddatetime",
				data: {compid:compid,verificationcode:verificationcode},
				type: "POST",
				cache:false,
				success: function(data) {
					if(data == 'active') {
						releasenoteslog(compid);
					} else if(data == 'Inactive') {
						$('#checkverificationcode').val('');
						alertpopup('Verification Code is inactive! It will be valid only one Hour! Please resend it again and continue updation.');
					} else {
						alertpopup("Something went wrong. Please try again!");
					}
				},
			});
		} else {
			alertpopup("Company Id not found. Please try again!");
		}
	});
	//Overlay Close to clear all datas in hidden
	$(document).on( "click", ".overlayclosedetails", function() {
		$('.ajaxupdateData').html('');
		$('#releasedetailsoverlay').fadeOut();
		$('#verificationcodeoverlay').fadeOut();
	});
});
// Check Schema db and input value
function releasenoteslog(compid) {
	$.ajax({
		url: base_url+"releasenoteslog/releasenoteslog.php",
		data: {compid:compid},
		type: "GET",
		cache:false,
		success: function(data) {
			
		},
	});
}
//company count fetch
function companycount() {
	$.ajax({
        url: base_url+"Company/companycountfetch",
        data: "",
		type: "POST",
		cache:false,
        success: function(data) {
           $('#companycount').val(data);
        },
    });
}
//view create success function
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $("#viewfieldids").val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','ViewCreationName','ViewCreationId','viewcreation','ViewCreationModuleId',viewmoduleids);
	if(viewname != "dontset") {
		setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		cleargriddata('viewcreateconditiongrid');
	} else {
		$('#dynamicdddataview').trigger('change');
	}
}
// Company View Grid
function companyjqgrid(page,rowcount) {
	if(Operation == 1){
		var rowcount = $('#companypgrowcount').val();
		var page = $('.paging').data('pagenum');
	} else{
		var rowcount = $('#companypgrowcount').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 30 : rowcount;
	}
	var wwidth = $('#companygrid').width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.companyheadercolsort').hasClass('datasort') ? $('.companyheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.companyheadercolsort').hasClass('datasort') ? $('.companyheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.companyheadercolsort').hasClass('datasort') ? $('.companyheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#viewfieldids').val();
	var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
	var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
	var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=company&primaryid=companyid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#companygrid').empty();
			$('#companygrid').append(data.content);
			$('#companygridfooter').empty();
			$('#companygridfooter').append(data.footer);
			//getaddinfo();
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('companygrid');
			{//sorting
				$('.companyheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.companyheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.companyheadercolsort').hasClass('datasort') ? $('.companyheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.companyheadercolsort').hasClass('datasort') ? $('.companyheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					var sortpos = $('#companygrid .gridcontent').scrollLeft();
					companyjqgrid(page,rowcount);
					$('#companygrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('companyheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					Operation = 0;
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					companyjqgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#companypgrowcount').change(function(){
					Operation = 0;
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = 1;
					companyjqgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			//onclick 
			$(".gridcontent").click(function(){
				var datarowid = $('#companygrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#companypgrowcount').material_select();
			setTimeout(function(){
				$('#processoverlay').hide();
			},500);
		},
	});
}
{//transaction type grids
function transactiongrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 30 : rowcount;
	var wwidth = $('#transactiongrid').width();
	var wheight = $('#transactiongrid').height();
	/*col sort*/
	var sortcol = $('.companyheadercolsort').hasClass('datasort') ? $('.companyheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.companyheadercolsort').hasClass('datasort') ? $('.companyheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.companyheadercolsort').hasClass('datasort') ? $('.companyheadercolsort.datasort').attr('id') : '0';	
	var filterid = '';
	var userviewid =131;
	var viewfieldids =2; 
	var footername = 'transaction';
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=transactionmanage&primaryid=transactionmanageid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&footername='+footername,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#transactiongrid').empty();
			$('#transactiongrid').append(data.content);
			$('#transactiongridfooter').empty();
			$('#transactiongridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('transactiongrid');
			{//sorting
				$('.companyheadercolsort').click(function(){
					$('.companyheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					transactiongrid(page,rowcount);
				});
				sortordertypereset('companyheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					transactiongrid(page,rowcount);
				});
				$('#transactionpgrowcount').change(function(){
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('.pvpagnumclass').data('pagenum');
					transactiongrid(page,rowcount);
				});
			}
			//Material select
			$('#transactionpgrowcount').material_select();	
		},
	});
 }
}
{//transaction type grids
	function oldjewelgrid(page,rowcount) {
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 30 : rowcount;
		var wwidth = $('#oldjewelgrid').width();
		var wheight = $('#oldjewelgrid').height();
		/*col sort*/
		var sortcol = $('.companyheadercolsort').hasClass('datasort') ? $('.companyheadercolsort.datasort').data('sortcolname') : '';
		var sortord =  $('.companyheadercolsort').hasClass('datasort') ? $('.companyheadercolsort.datasort').data('sortorder') : '';
		var headcolid = $('.companyheadercolsort').hasClass('datasort') ? $('.companyheadercolsort.datasort').attr('id') : '0';	
		var filterid = '';
		var userviewid =183;
		var viewfieldids =2; 
		var footername = 'oldjewel';
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=oldjeweldetails&primaryid=oldjeweldetailsid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&footername='+footername,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#oldjewelgrid').empty();
				$('#oldjewelgrid').append(data.content);
				$('#oldjewelgridfooter').empty();
				$('#oldjewelgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('oldjewelgrid');
				{//sorting
					$('.companyheadercolsort').click(function(){
						$('.companyheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						oldjewelgrid(page,rowcount);
					});
					sortordertypereset('companyheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						oldjewelgrid(page,rowcount);
					});
					$('#oldjewelpgrowcount').change(function(){
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('.pvpagnumclass').data('pagenum');
						oldjewelgrid(page,rowcount);
					});
				}
				//Material select
				$('#oldjewelpgrowcount').material_select();	
			},
		});
	 }
}
//crud operation
function crudactionenable() {
	$("#addicon").click(function() {
		var ccount = $('#companygrid .gridcontent div.data-content div').length;
		$('#companycount').val(ccount);
		var compcount = $('#companycount').val();
		if((compcount == 0)) {
			froalaset(froalaarray);
			addslideup('companygriddisplay', 'companyaddformdiv');
			Materialize.updateTextFields();
			resetFields();
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			showhideiconsfun('addclick','companyaddformdiv');
			$(".documentslogodownloadclsbtn").show();
			$('#companylogoattachdisplay').empty();
			//form field first focus
			firstfieldfocus();
			$("#companylogodivhid").removeClass('hidedisplay');
		} else {
			$("#companyalerts").fadeIn();
			fortabtouch = 0;
			setTimeout(function(){
				$("#alertsclose").focus();
				//For Keyboard Shortcut Variables
				viewgridview = 1;
				addformview = 0;
				addformupdate = 0;
			},100);
		}
		Operation = 0; //for pagination
	});
	$("#editicon").click(function() {
		var datarowid = $('#companygrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			$(".addbtnclass").addClass('hidedisplay');
			$(".updatebtnclass").removeClass('hidedisplay');
			showhideiconsfun('editclick','companyaddformdiv');
			$(".documentslogodownloadclsbtn").show();
			$('#companylogoattachdisplay').empty();
			$("#emailid").attr('readonly',true);
			$("#companylogodivhid").removeClass('hidedisplay');
			froalaset(froalaarray);
			getformdata(datarowid);
			$("#tab1").trigger('click');
			Materialize.updateTextFields();
			firstfieldfocus();
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			fortabtouch = 1;
			Operation = 1; //for pagination
		} else {
			alertpopup("Please select a row");
		}
	});
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#companypgnum li.active').data('pagenum');
		var rowcount = $('ul#companypgnumcnt li .page-text .active').data('rowcount');
		companyjqgrid(page,rowcount);
	}
}
//new data add submit function
function newdataaddfun() {
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditoradd(editorname);
    var formdata = $("#dataaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Company/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				$(".ftab").trigger('click');
				resetFields();
				$('#companyaddformdiv').hide();
				$('#companygriddisplay').fadeIn(1000);
				refreshgrid();
				alertpopup(savealert);
				//For Keyboard Shortcut Variables
				viewgridview = 1;
				addformview = 0;
            }
        },
    });
}
//old information show in form
function getformdata(datarowid) {
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementpartable = $('#elementspartabname').val();
	$.ajax({
		url:base_url+"Company/fetchformdataeditdetails?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				$('#companyaddformdiv').hide();
				$('#companygriddisplay').fadeIn(100);
				refreshgrid();
				//For Touch
				smsmasterfortouch = 0;
			} else {
				addslideup('companygriddisplay','companyaddformdiv');
				var txtboxname = elementsname + ',primarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
				if(data['gstrates'] == 'Yes'){
					$("#taxmasteriddivhid").show();
				} else {
					$("#taxmasteriddivhid").hide();
				}
				if(data['lutbonddetails'] == 'Yes'){
					$("#applicablefromdatedivhid,#applicabletodatedivhid,#lutbondnumberdivhid").show();
				} else {
					$("#applicablefromdatedivhid,#applicabletodatedivhid,#lutbondnumberdivhid").hide();
				}
				var attachfldname = $('#attachfieldnames').val();
				var attachcolname = $('#attachcolnames').val();
				var attachuitype = $('#attachuitypeids').val();
				var attachfieldid = $('#attachfieldids').val();
				attachedfiledisplay(attachfldname,attachcolname,attachuitype,attachfieldid,data);
				editordatafetch(froalaarray,data);
				//For Keyboard Shortcut Variables
				addformupdate = 1;
				viewgridview = 0;
				addformview = 1;
			}
		},
	});
}
//udate old information
function updateformdata() {
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditordataget(editorname);
	var formdata = $("#dataaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Company/datainformationupdate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				$(".ftab").trigger('click');
				resetFields();
				$('#companyaddformdiv').hide();
				$('#companygriddisplay').fadeIn(1000);
				refreshgrid();
				alertpopup(savealert);
				//For Keyboard Shortcut Variables
				addformupdate = 0;
				viewgridview = 1;
				addformview = 0;
            }
        },
    });
}
function updatecompanysetting() {
	var purity = $('#purityid').val();
	var ratepurity = $('#ratepurityid').val();
	var metalpurity = $('#metalpurityid').val();
	var chargedata = $('#chargeid').val();
	var purchasechargedata = $('#purchasechargeid').val();
	var businesschargedata = $('#businesschargeid').val();
	var businesspurchasechargedata = $('#businesspurchasechargeid').val();
	var accounttax = $('#accounttaxid').val();
	var olditemdata=$('#olditemdetails').val();
	var lotdata=$('#lotdata').val();
	var bullionproductdata=$('#bullionproduct').val();
	var purchasetransactionmodedata=$('#purchasetransactionmodeid').val();
	var productroltype = $('#productroltype').val();
	var recurrenceday = $('#recurrencedayid').val();
	var loosesaleschargedata = $('#loosesalescharge').val();
	var loosepurchasechargedata = $('#loosepurchasecharge').val();
	var itemtagauthuserroleid = $('#itemtagauthuserroleid').val();
	var tagchargesfrom = $('#tagchargesfrom').val();
	var rfidnoofdigits = $('#rfidnoofdigits').val();
	var pancardrestrictamt = $('#pancardrestrictamt').val();
	var cashreceiptlimit = $('#cashreceiptlimit').val();
	var cashissuelimit = $('#cashissuelimit').val();
	var touchchargesedit = $('#touchchargesedit').val();
	var discountbilllevelpercent = $('#discountbilllevelpercent').val();
	var untagwtvalidate = $('#untagwtvalidate').val();
	var untagwtshow = $('#untagwtshow').val();
	var approvaloutrfid = $('#approvaloutrfid').val();
	var salesauthuserrole = $('#salesauthuserrole').val();
	var chituserroleauth = $('#chituserroleauth').val();
	var ratedisplaypurityvalue = $('#ratedisplaypurityvalue').val();
	var form = $("#companysettingform").serialize();
	var amp = '&';
	var datainformation = amp + form;
	setTimeout(function(){
		$('#processoverlay').show();
	},25);
	$.ajax(
    {
        url: base_url +"Company/companysettingupdate", 
        data: "datas=" + datainformation+amp+"purity="+purity+amp+"accounttax="+accounttax+amp+"olditemdata="+olditemdata+amp+"chargedata="+chargedata+amp+"bullionproductdata="+bullionproductdata+amp+"purchasechargedata="+purchasechargedata+amp+"purchasetransactionmodedata="+purchasetransactionmodedata+amp+"businesschargedata="+businesschargedata+amp+"businesspurchasechargedata="+businesspurchasechargedata+amp+"ratepurity="+ratepurity+amp+"metalpurity="+metalpurity+amp+"lotdata="+lotdata+"&productroltypeid="+productroltype+"&recurrenceday="+recurrenceday+amp+"loosesaleschargedata="+loosesaleschargedata+amp+"loosepurchasechargedata="+loosepurchasechargedata+amp+"itemtagauthuserroleid="+itemtagauthuserroleid+amp+"tagchargesfrom="+tagchargesfrom+amp+"rfidnoofdigits="+rfidnoofdigits+amp+"pancardrestrictamt="+pancardrestrictamt+amp+"cashreceiptlimit="+cashreceiptlimit+amp+"cashissuelimit="+cashissuelimit+amp+"touchchargesedit="+touchchargesedit+amp+"discountbilllevelpercent="+discountbilllevelpercent+amp+"untagwtvalidate="+untagwtvalidate+amp+"untagwtshow="+untagwtshow+amp+"approvaloutrfid="+approvaloutrfid+amp+"salesauthuserrole="+salesauthuserrole+amp+"chituserroleauth="+chituserroleauth+amp+"ratedisplaypurityvalue="+ratedisplaypurityvalue,
		type: "POST",
        success: function(msg)
        {
			if ($.trim(msg) == 'SUCCESS') {
				$('#processoverlay').hide();
				alertpopup('Updated Successfully');
				 $('#subformspan3').find('input:text').val('');
				 $(".ftab").trigger('click');
			 	$('#companysettingview').hide();
				$('#companygriddisplay').fadeIn(1000);
				resetFields();
				$('#companysettingview').hide();
				$('#companygriddisplay').fadeIn(1000);
                //Keyboard Globals
				viewgridview = 1;
            } else {
				$('#processoverlay').hide();
				alertpopup('error');
			}
        },
    });
}
function getcompanydetails() {
	$.ajax({
		url: base_url + "Company/getcompanydata",
		dataType:'json',
		async:false,
		success: function(msg) { 
			var newddappend="";
			var elementsname=['92','companysettingid','weightdecisize','meltdecisize','amountround','rateround','lotcreationopt','counteroption','stoneoption','imageoption','taxoption','pdfoption','scaleoption','netwtcalctype','tagtype','counterlevel','categorylevel','stonelevel','purchasedata','purityid','needezetap','askemailsmsfields','needcashcheque','olditemdetails','barcodeoption','bullionproduct','lotdetailsopt','chargecalcoption','salesreturntypeid','billingtypeid','defaultbankid','defaultcashinhandid','chargecalculation','oldjewelproducttype','schemestatus','categoryid','approvaloutcalculation','schememethodid','schemetypeid','stocknotify','productidshow','vacchargeshow','chargeaccountgroup','taxtypeid','discounttypeid','oldjewelsumtaxapplyid','oldjewelsumtaxid','rateadditiondefault','estimateconcept','lotpieces','purchasewastage','salesrate','purchaserate','paymentmodetype','oldjewelvoucher','discountpassword','discountcalc','taxcalc','discountauthorization','taxchargeinclude','storagequantity','purewtcalctype','oldjeweldetails','storagedisplay','wastagecalctype','gstapplicable','grossweighttrigger','productroltypeid','reorderqty','salesdatechangeablefor','salespersonshowhide','cashcountershowhide','purchasedisplay','lockingtime','updatingtime','recurrencedayid','lotweightlimit','chargerequired','sizestatus','oldjewelvoucheredit','roundvaluelimit','advancecalc','customerwise','vendorwise','weightvalidate','prizetaggst','orderimageoption','transactionimageoption','weightcheck','loosestonecharge','stoneweightcheck','tagautoimage','drivename','itemtagauthrole','chituserroleauth','tagchargesfrom','rfidnoofdigits','pancardrestrictamt','cashreceiptlimit','cashissuelimit','touchchargesedit','discountbilllevelpercent','untagwtvalidate','untagwtshow','approvaloutrfid','salesauthuserrole','multitagbarcode','estimatequicktotal','estimatedefaultset','caratwtcalcvalue','untagstoneenable','purchaseerrorwt','chitcompletealert','accountmobilevalidation','lottolerancepercent','lottolerancevalue','flatweightwastagespan','salespaymenthideshow','povendormanagement','takeordercategory','accountypehideshow','takeorderduedate','chitinteresttype','accountmobileunique','ratedisplaymainview','mainviewdefaultview','transactioncategoryfield','transactionemployeeperson','poautomaticvendor','receiveorderconcept','productaddoninfo','receiveordertemplateid','rejectreviewordertemplateid','oldjewelsstonedetails','chargeconversionfield','billamountcalculationtrigger','oldjewelsstonecalc','purchasemodstonedetails','discountauthmoduleview','ratedisplaypurityvalue','salesreturnbillcustomers','salesreturnitemstatus','itemtagsavefocus','questionaireapplicable','stonepiecescalc','itemtagvendorvalidate','accountsummaryinfo','paymentsavebutton','salesreturnautomatic','popuphideshow','partialtagdate','salesreturnuserroleauth','printtemplatehideshow','lottolerancediactvalue','lotdiaweightlimit'];
			var dropdowns = ['87','weightdecisize','meltdecisize','amountround','rateround','lotcreationopt','counteroption','stoneoption','imageoption','taxoption','pdfoption','scaleoption','netwtcalctype','tagtype','counterlevel','categorylevel','stonelevel','purchasedata','purityid','needezetap','askemailsmsfields','needcashcheque','olditemdetails','barcodeoption','bullionproduct','lotdetailsopt','chargecalcoption','salesreturntypeid','billingtypeid','defaultbankid','defaultcashinhandid','chargecalculation','oldjewelproducttype','schemestatus','categoryid','approvaloutcalculation','schememethodid','schemetypeid','stocknotify','productidshow','vacchargeshow','chargeaccountgroup','taxtypeid','discounttypeid','oldjewelsumtaxapplyid','oldjewelsumtaxid','estimateconcept','lotpieces','purchasewastage','salesrate','purchaserate','paymentmodetype','oldjewelvoucher','discountpassword','discountcalc','taxcalc','discountauthorization','taxchargeinclude','storagequantity','purewtcalctype','oldjeweldetails','storagedisplay','wastagecalctype','gstapplicable','grossweighttrigger','productroltypeid','reorderqty','salesdatechangeablefor','salespersonshowhide','cashcountershowhide','purchasedisplay','recurrencedayid','chargerequired','sizestatus','oldjewelvoucheredit','advancecalc','customerwise','vendorwise','weightvalidate','prizetaggst','orderimageoption','transactionimageoption','weightcheck','loosestonecharge','stoneweightcheck','tagautoimage','drivename','itemtagauthrole','chituserroleauth','tagchargesfrom','rfidnoofdigits','pancardrestrictamt','cashreceiptlimit','cashissuelimit','touchchargesedit','discountbilllevelpercent','untagwtvalidate','untagwtshow','approvaloutrfid','salesauthuserrole','multitagbarcode','estimatequicktotal','estimatedefaultset','caratwtcalcvalue','untagstoneenable','purchaseerrorwt','chitcompletealert','accountmobilevalidation','lottolerancepercent','flatweightwastagespan','salespaymenthideshow','creditinfohideshow','povendormanagement','takeordercategory','accountypehideshow','takeorderduedate','chitinteresttype','accountmobileunique','ratedisplaymainview','mainviewdefaultview','transactioncategoryfield','transactionemployeeperson','poautomaticvendor','receiveorderconcept','productaddoninfo','receiveordertemplateid','rejectreviewordertemplateid','oldjewelsstonedetails','chargeconversionfield','billamountcalculationtrigger','oldjewelsstonecalc','purchasemodstonedetails','discountauthmoduleview','ratedisplaypurityvalue','salesreturnbillcustomers','salesreturnitemstatus','itemtagsavefocus','questionaireapplicable','stonepiecescalc','itemtagvendorvalidate','accountsummaryinfo','paymentsavebutton','salesreturnautomatic','popuphideshow','partialtagdate','salesreturnuserroleauth','printtemplatehideshow','oldpurchasecredittype'];
		   var txtboxname = elementsname;
			textboxsetvalue(txtboxname,txtboxname,msg,dropdowns);
			var olditemdata = msg.olditemdetails.split(",");
			var chargedata = msg.chargeid.split(",");
			var purchasechargedata = msg.purchasechargeid.split(",");
			var businesschargedata = msg.businesschargeid.split(",");
			var businesspurchasechargedata = msg.businesspurchasechargeid.split(",");
			var bullionproductdata = msg.bullionproduct.split(",");
			var purchasetransactionmodedata = msg.purchasetransactionmodeid.split(",");
			var ratepuritydata = msg.ratepurityid.split(",");
			var metalpuritydata = msg.metalpurityid.split(",");
			var lotdata = msg.lotdata.split(",");
			var productroltypeid = msg.productroltypeid.split(",");
			var recurrencedayid = msg.recurrencedayid.split(",");
			var splitdeviceno = msg.deviceno.split(",");
			var loosesalescharge = msg.loosesalescharge.split(",");
			var loosepurchasecharge = msg.loosepurchasecharge.split(",");
			var itemtagauthrole = msg.itemtagauthrole.split(",");
			var chituserroleauth = msg.chituserroleauth.split(",");
			var salesauthuserrole = msg.salesauthuserrole.split(",");
			var ratedisplaypurityvalue = msg.ratedisplaypurityvalue.split(",");
			var salesreturnuserroleauth = msg.salesreturnuserroleauth.split(",");
			$(".chzn-select").select2({containerCss: {display: "block"}});
			setTimeout(function(){
				if(olditemdata != ''){
				   $('#olditemdetails').select2('val',olditemdata).trigger('change');
				}
				if(chargedata != ''){
				   $('#chargeid').select2('val',chargedata).trigger('change');
				}
				if(purchasechargedata != ''){
					   $('#purchasechargeid').select2('val',purchasechargedata).trigger('change');
					}
				if(businesschargedata != ''){
				   $('#businesschargeid').select2('val',businesschargedata).trigger('change');
				}
				if(businesspurchasechargedata != ''){
					   $('#businesspurchasechargeid').select2('val',businesspurchasechargedata).trigger('change');
					}
				if(bullionproductdata != ''){
					   $('#bullionproduct').select2('val',bullionproductdata).trigger('change');
				}
				if(purchasetransactionmodedata != ''){
					   $('#purchasetransactionmodeid').select2('val',purchasetransactionmodedata).trigger('change');
				}
				if(ratepuritydata != ''){
					   $('#ratepurityid').select2('val',ratepuritydata).trigger('change');
				}
				if(metalpuritydata != ''){
					   $('#metalpurityid').select2('val',metalpuritydata).trigger('change');
				}
				if(lotdata != ''){
					   $('#lotdata').select2('val',lotdata).trigger('change');
				}
				if(productroltypeid != '' || productroltypeid != 0){
					   $('#productroltype').select2('val',productroltypeid).trigger('change');
				}
				if(recurrencedayid != '' || recurrencedayid != 0){
					$('#recurrencedayid').select2('val',recurrencedayid).trigger('change');
				}
				if(splitdeviceno != ''){
					$('#deviceno').select2('val',splitdeviceno).trigger('change');
				}
				if(loosesalescharge != ''){
					$('#loosesalescharge').select2('val',loosesalescharge).trigger('change');
				}
				if(loosepurchasecharge != ''){
					$('#loosepurchasecharge').select2('val',loosepurchasecharge).trigger('change');
				}
				if(itemtagauthrole != ''){
					$('#itemtagauthuserroleid').select2('val',itemtagauthrole).trigger('change');
				}
				if(salesauthuserrole != ''){
					$('#salesauthuserrole').select2('val',salesauthuserrole).trigger('change');
				}
				if(chituserroleauth != ''){
					$('#chituserroleauth').select2('val',chituserroleauth).trigger('change');
				}
				if(ratedisplaypurityvalue != ''){
					$('#ratedisplaypurityvalue').select2('val',ratedisplaypurityvalue).trigger('change');
				}
				if(salesreturnuserroleauth != ''){
					$('#salesreturnuserroleauth').select2('val',salesreturnuserroleauth).trigger('change');
				}
				$('#tagchargesfrom').select2('val',msg.tagchargesfrom).trigger('change');
				$('#rfidnoofdigits').val(msg.rfidnoofdigits);
				$('#pancardrestrictamt').val(msg.pancardrestrictamt);
				$('#cashreceiptlimit').val(msg.cashreceiptlimit);
				$('#cashissuelimit').val(msg.cashissuelimit);
				$('#lottolerancevalue').val(msg.lottolerancevalue);
				$('#salesreturndays').val(msg.salesreturndays);
				$('#touchchargesedit').select2('val',msg.touchchargesedit).trigger('change');
				$('#discountbilllevelpercent').select2('val',msg.discountbilllevelpercent).trigger('change');
				$('#untagwtvalidate').select2('val',msg.untagwtvalidate).trigger('change');
				$('#untagwtshow').select2('val',msg.untagwtshow).trigger('change');
				$('#approvaloutrfid').select2('val',msg.approvaloutrfid).trigger('change');
				$('#edittagcharge').select2('val',msg.edittagcharge).trigger('change');
				$('#multitagbarcode').select2('val',msg.multitagbarcode).trigger('change');
				$('#estimatequicktotal').select2('val',msg.estimatequicktotal).trigger('change');
				$('#estimatedefaultset').select2('val',msg.estimatedefaultset).trigger('change');
				$('#caratwtcalcvalue').select2('val',msg.caratwtcalcvalue).trigger('change');
				$('#untagstoneenable').select2('val',msg.untagstoneenable).trigger('change');
				$('#chitcompletealert').select2('val',msg.chitcompletealert).trigger('change');
				$('#accountmobilevalidation').select2('val',msg.accountmobilevalidation).trigger('change');
				$('#lottolerancepercent').select2('val',msg.lottolerancepercent).trigger('change');
				$('#flatweightwastagespan').select2('val',msg.flatweightwastagespan).trigger('change');
				$('#salespaymenthideshow').select2('val',msg.salespaymenthideshow).trigger('change');
				$('#creditinfohideshow').select2('val',msg.creditinfohideshow).trigger('change');
				$('#povendormanagement').select2('val',msg.povendormanagement).trigger('change');
				$('#takeordercategory').select2('val',msg.takeordercategory).trigger('change');
				$('#accountypehideshow').select2('val',msg.accountypehideshow).trigger('change');
				$('#takeorderduedate').select2('val',msg.takeorderduedate).trigger('change');
				$('#chitinteresttype').select2('val',msg.chitinteresttype).trigger('change');
				$('#accountmobileunique').select2('val',msg.accountmobileunique).trigger('change');
				$('#ratedisplaymainview').select2('val',msg.ratedisplaymainview).trigger('change');
				$('#mainviewdefaultview').select2('val',msg.mainviewdefaultview).trigger('change');
				$('#transactioncategoryfield').select2('val',msg.transactioncategoryfield).trigger('change');
				$('#transactionemployeeperson').select2('val',msg.transactionemployeeperson).trigger('change');
				$('#poautomaticvendor').select2('val',msg.poautomaticvendor).trigger('change');
				$('#receiveorderconcept').select2('val',msg.receiveorderconcept).trigger('change');
				$('#productaddoninfo').select2('val',msg.productaddoninfo).trigger('change');
				$('#oldjewelsstonedetails').select2('val',msg.oldjewelsstonedetails).trigger('change');
				$('#chargeconversionfield').select2('val',msg.chargeconversionfield).trigger('change');
				$('#billamountcalculationtrigger').select2('val',msg.billamountcalculationtrigger).trigger('change');
				$('#oldjewelsstonecalc').select2('val',msg.oldjewelsstonecalc).trigger('change');
				$('#purchasemodstonedetails').select2('val',msg.purchasemodstonedetails).trigger('change');
				$('#discountauthmoduleview').select2('val',msg.discountauthmoduleview).trigger('change');
				$('#salesreturnbillcustomers').select2('val',msg.salesreturnbillcustomers).trigger('change');
				$('#salesreturnitemstatus').select2('val',msg.salesreturnitemstatus).trigger('change');
				$('#itemtagsavefocus').select2('val',msg.itemtagsavefocus).trigger('change');
				$('#questionaireapplicable').select2('val',msg.questionaireapplicable).trigger('change');
				$('#stonepiecescalc').select2('val',msg.stonepiecescalc).trigger('change');
				$('#itemtagvendorvalidate').select2('val',msg.itemtagvendorvalidate).trigger('change');
				$('#accountsummaryinfo').select2('val',msg.accountsummaryinfo).trigger('change');
				$('#paymentsavebutton').select2('val',msg.paymentsavebutton).trigger('change');
				$('#salesreturnautomatic').select2('val',msg.salesreturnautomatic).trigger('change');
				$('#popuphideshow').select2('val',msg.popuphideshow).trigger('change');
				$('#partialtagdate').select2('val',msg.partialtagdate).trigger('change');
				$('#printtemplatehideshow').select2('val',msg.printtemplatehideshow).trigger('change');
				$('#oldpurchasecredittype').select2('val',msg.oldpurchasecredittype).trigger('change');
				$('#purchaseerrorwt').val(msg.purchaseerrorwt);
				$('#receiveordertemplateid').val(msg.receiveordertemplateid);
				$('#rejectreviewordertemplateid').val(msg.rejectreviewordertemplateid);
				$('#placeorderdefaultaccid').val(msg.placeorderdefaultaccid);
				$('#estimatedefaultaccid').val(msg.estimatedefaultaccid);
				$('#lottolerancediactvalue').val(msg.lottolerancediactvalue);
				$('#lotdiaweightlimit').val(msg.lotdiaweightlimit);
			},25);
			$("#schememethodid,#taxtypeid,#oldjewelsumtaxapplyid,#lotdetailsopt,#scaleoption,#counteroption,#stoneoption").trigger('change');
			if(msg.chargecalcoption != 3){ // charge mode both
				$("#chargecalcoption").trigger('change');
			}
			$('#boxwttemplate').select2('val',msg.boxwttemplate);
		},
	});
}
function addform() {
	var datarowid = $('#transactiongrid div.gridcontent div.active').attr('id');
	var totalamountdata = $('#transactiontotalamount').val();
	var pureweightdata = $('#transactionpureweight').val();
	var displayfields = $('#displayfieldid').val();
   	var formdata = $("#frmtransactionsetting").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	 $.ajax({
        url: base_url +"Company/transactionsetting",
        data: "datas=" +datainformation+amp+"pureweightdata="+pureweightdata+amp+"totalamountdata="+totalamountdata+"&displayfields="+displayfields+"&transactiontypeeditstatus="+transactiontypeeditstatus+"&datarowid="+datarowid,
		type: "POST",
        success: function(msg)  {
        	var nmsg =  $.trim(msg);
			if (nmsg == 'SUCCESS'){
				transactiongrid();
				$("#transactiontype,#salesmodeid").attr('readonly',false);
				$("#transactiontypeadd,#transactiontypeediticon").show();
				$("#transactiontypeedit").hide();
				clearform('transactionsettingform');
				alertpopup(savealert);
				
            } else {
				alertpopup(submiterror);
			}          
        },
    });
}
//old jewel detials add form
function oldjeweladdform() {
   	var formdata = $("#oldjewelsettingform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	$.ajax({
		url: base_url +"Company/oldjewelsettingcreate",
        data: "datas=" +datainformation,
		type: "POST",
        success: function(msg)  {
			var nmsg =  $.trim(msg);
			if (nmsg == 'SUCCESS'){
				oldjewelgrid();
				clearform('oldjewelsettingform');
				$('#oldjewelediticon').show();
				alertpopup(savealert);
            } else {
				alertpopup(submiterror);
			}          
        },
    });
}
//delete accountgroup
function recorddelete() {	
	var datarowid=$('#transactiongrid div.gridcontent div.active').attr('id');
	$.ajax({
        url: base_url + "Company/transactionsettingdelete?primaryid="+datarowid,
        success: function(msg) {
			var nmsg =  $.trim(msg);
			$("#basedeleteoverlay").fadeOut();
		    if (nmsg == 'SUCCESS') {
				 transactiongrid();
				alertpopup(deletealert);
            } else {
				alertpopup(submiterror);
            }           
        },
    });
}
//
function delteoldjeweldetails(datarowid) {
	$.ajax({
        url: base_url + "Company/oldjeweldetailsdelete?primaryid="+datarowid,
        success: function(msg) {
			var nmsg =  $.trim(msg);
			$("#basedeleteoverlay").fadeOut();
		    if (nmsg == 'SUCCESS') {
		    	oldjewelgrid();
				alertpopup(deletealert);
            } else {
				alertpopup(submiterror);
            }           
        },
    });
}
function retrive() {
	var elementsname=['16','transactiontype','calculationtype','stocktypeid','transactionmodeid','billingtypeid','transactiontotalamount','transactionpureweight','salesmodeid','displayfieldid','roundingtype','roundoffremove','transactionautotax','discountmodeid','discountcalctypeid','discountdisplayid','taxmodeid'];
	var datarowid= $('#transactiongrid div.gridcontent div.active').attr('id');
	//form field first focus
	firstfieldfocus();
	$.ajax({
		url:base_url+"Company/transactionsettingretrive?primaryid="+datarowid, 
		dataType:'json',
		async:false,
		success: function(data) {
			$("#transactiontypeadd").hide();
			$("#transactiontypeedit").show();
			var txtboxname = elementsname;
			var dropdowns = ['15','transactiontype','calculationtype','stocktypeid','transactionmodeid','billingtypeid','transactiontotalamount','transactionpureweight','salesmodeid','roundingtype','roundoffremove','transactionautotax','discountmodeid','discountcalctypeid','discountdisplayid','taxmodeid'];
			textboxsetvalue(txtboxname,txtboxname,data,dropdowns);
			transactiontypeeditstatus = 1;
			$('#transactiontype,#transactionmodeid').trigger('change');
			$('#stocktypeid').trigger('change');
			$('#transactionmanagename').val(data.transactionmanagename);
			$('#allstocktypeid').val(data.allstocktypeid);
			$('#allowedaccounttypeid').val(data.allowedaccounttypeid);
			$('#allprinttemplateid').val(data.allprinttemplateid);
			$('#serialmastername').val(data.serialnumbermastername);
			$('#defaultforttype').val(data.defaultforttype);
			$('#payrecadvanceconcept').val(data.payrecadvanceconcept);
			$('#payrecadvanceprintid').val(data.payrecadvanceprintid);
			var additionalchargedata=data.additionalchargeid.split(",");
			var pureadditionalchargedata=data.pureadditionalchargeid.split(",");
			var displayfielddata=data.displayfieldid.split(",");
			setTimeout(function() {
				if(additionalchargedata != '') {
					$('#transactiontotalamount').select2('val',additionalchargedata).trigger('change');
				}
				if(pureadditionalchargedata != '') {
					$('#transactionpureweight').select2('val',pureadditionalchargedata).trigger('change');
				}
				if(displayfielddata != '') {
					$('#displayfieldid').select2('val',displayfielddata).trigger('change');
				}
			},100);
			Materialize.updateTextFields();
		}		
	});
}
//function old jewel retrive
function oldjewelretrive() {
	var elementsname=['6','jeweltransactiontype','jeweltypeid','oldjewelpurityid','netweightcalculationid','rateamount','weightless','transactiontypeid'];
	var datarowid= $('#oldjewelgrid div.gridcontent div.active').attr('id');
	//form field first focus
	firstfieldfocus();
	$.ajax({
		url:base_url+"Company/oldjewelsettingretrive?primaryid="+datarowid, 
		dataType:'json',
		async:false,
		success: function(data) {
			var txtboxname = elementsname;
			var dropdowns =elementsname; 
			textboxsetvalue(txtboxname,txtboxname,data,dropdowns);
			$("#rateamount").val(data.rateamount);
			$("#weightless").val(data.weightless);
			Materialize.updateTextFields();
		}		
	});
}
function autoloadfieldvalues(modeid,transactiontypeid)
{
	var elementsname=['4','stocktypeid','transactionmodeid','transactiontotalamount','transactionpureweight'];
	$.ajax(
	{
		url:base_url+"Company/autoloadfieldvalues?transactiontypeid="+transactiontypeid+"&modeid="+modeid, 
		dataType:'json',
		async:false,
		success: function(data)
		{
			if(data.calculationtype!='')
			{
				$("#transactiontypeadd").hide();
				$("#transactiontypeedit").show();				
			}
			else
			{
				$("#transactiontypeadd").show();
				$("#transactiontypeedit").hide();
			}
			var txtboxname = elementsname;
			var dropdowns = ['4','stocktypeid','transactionmodeid','transactiontotalamount','transactionpureweight'];
			textboxsetvalue(txtboxname,txtboxname,data,dropdowns);
			$('#transactionmodeid').trigger('change');
			var additionalchargedata=data.additionalchargeid.split(",");
			var pureadditionalchargedata=data.pureadditionalchargeid.split(",");
			setTimeout(function(){
				if(additionalchargedata != ''){
					   $('#transactiontotalamount').select2('val',additionalchargedata).trigger('change');
				}
				if(pureadditionalchargedata != ''){
					   $('#transactionpureweight').select2('val',pureadditionalchargedata).trigger('change');
				}
			},100);
			Materialize.updateTextFields();
		}		
	});
}
function ttbasedstypeshowhide(){
	   if(transactiontypeeditstatus == 0){
			$("#stocktypeid").select2('val','').trigger('change');
		}
        //show/hide stocktype options
		var stocktype_arr = $('#transactiontype').find('option:selected').data('stocktyperelation');
		var stocktype_arr = stocktype_arr.toString();		
		if(checkValue(stocktype_arr) == true){		
			$("#stocktypeid option").addClass("ddhidedisplay");
			$("#stocktypeid optgroup").addClass("ddhidedisplay");
			$("#stocktypeid option").prop('disabled',true);
			var stocktype_array = stocktype_arr.split(",");
			for(var opi =0;opi < stocktype_array.length;opi++)
			{
				$("#stocktypeid option[value='"+stocktype_array[opi]+"']").closest('optgroup').removeClass("ddhidedisplay");
				$("#stocktypeid option[value='"+stocktype_array[opi]+"']").removeClass("ddhidedisplay");
				$("#stocktypeid option[value='"+stocktype_array[opi]+"']").prop('disabled',false);
			}
		} 
		else 
		{
			$("#stocktypeid option").removeClass("ddhidedisplay");
			$("#stocktypeid optgroup").removeClass("ddhidedisplay");
			$("#stocktypeid option").prop('disabled',false);
		}
		$("#stocktypeid option:enabled").closest('optgroup').removeClass("ddhidedisplay");	
	}
function totalamounthideshow(fieldid,val){
    	$("#"+fieldid+" option").removeClass("ddhidedisplay");
		$("#"+fieldid+" option").prop('disabled',false);
		if(val != ''){
			if(val.indexOf(',') > -1) {
				var fielddata = val.split(',');
				for(j=0;j<(fielddata.length);j++) {
					$("#"+fieldid+" option[value="+fielddata[j]+"]").addClass("ddhidedisplay");
					$("#"+fieldid+" option[value="+fielddata[j]+"]").prop('disabled',true);
				}
			}else{
				$("#"+fieldid+" option[value="+val+"]").addClass("ddhidedisplay");
				$("#"+fieldid+" option[value="+val+"]").prop('disabled',true);
			}
		}
}
function modehideshow(val){
	var stocktypeid = 'salesmodeid';
	var mode = 3;
	var payemntmodeid = 2;
	if(val == 9 || val == 11 || val == 20 || val == 24){ // sales,purchase & take order
		$("#"+stocktypeid+" option[value="+payemntmodeid+"]").removeClass("ddhidedisplay");
		$("#"+stocktypeid+" option[value="+payemntmodeid+"]").prop('disabled',false);
		$("#"+stocktypeid+" option[value="+mode+"]").addClass("ddhidedisplay");
		$("#"+stocktypeid+" option[value="+mode+"]").prop('disabled',true);
	}else if(val == 22 || val == 23){ // payment issue/receipt
	   
		$("#"+stocktypeid+" option[value="+mode+"]").removeClass("ddhidedisplay");
		$("#"+stocktypeid+" option[value="+mode+"]").prop('disabled',false);
		$("#"+stocktypeid+" option[value="+payemntmodeid+"]").addClass("ddhidedisplay");
		$("#"+stocktypeid+" option[value="+payemntmodeid+"]").prop('disabled',true);
	}else{
		$("#"+stocktypeid+" option[value="+mode+"]").addClass("ddhidedisplay");
		$("#"+stocktypeid+" option[value="+mode+"]").prop('disabled',true);
	}
}
function calculationtypehideshow(val){
	var stocktypeid = 'transactionmodeid';
	var typeid = '';
	$("#"+stocktypeid+" option").removeClass("ddhidedisplay");
		$("#"+stocktypeid+" option").prop('disabled',false);
}
function stocktypeshowhide(val,transactiontype){
	var stocktypeid = 'stocktypeid';
	if(transactiontype == '11' || transactiontype == '9' || transactiontype == '22' || transactiontype == '23' || transactiontype == '20') {
		$("#"+stocktypeid+" option").addClass("ddhidedisplay");
		$("#"+stocktypeid+" option").prop('disabled',true);
		var typeid = '';
		if(val == '2'){
			if(transactiontype == '11'){
				typeid = '11,12,13,16,19,20,74,75';  // sales type
			}else if(transactiontype == '20'){
				typeid = '19,75,82';  // sales type
			}else if(transactiontype == '9'){
				typeid = '17,73,77,80,81';  // purchase type
			}
		}else if(val == '3'){
			var calctype = $("#transactionmodeid").find('option:selected').val();
			if(calctype == 2) { // amount
				typeid = '26,27,28,29,31,39';   // payment type
			} else if(calctype == 3) { //pure wt
				typeid = '37,38,49,57,70,78,79';   // payment type
			} else if(calctype == 4) { // pure wt am
				typeid = '26,27,28,29,31,37,38,39,49,57,70,78,79';   // payment type
			} else { //other
				typeid = '26,27,28,29,31,37,38,39,49,57,70,78,79';   // payment type
			}
		}
		if(typeid.indexOf(',') > -1) {
			var stocktype = typeid.split(',');
			for(j=0;j<(stocktype.length);j++) {
				$("#"+stocktypeid+" option[value="+stocktype[j]+"]").removeClass("ddhidedisplay");
				$("#"+stocktypeid+" option[value="+stocktype[j]+"]").prop('disabled',false);
			}
		}
	}
} 
function dropdownhover(){
	setTimeout(function(){
		$(".select2-container").hover(function() {
			$( this ).find('.s2resetbtn').css('opacity','1');
		},function(){
			$( this ).find('.s2resetbtn').css('opacity','0');
		});
	},1000);
}
function saleschargehideshow(businesschargedata,ddname){
	$("#"+ddname+" option").addClass("ddhidedisplay");
	$("#"+ddname+" option").prop('disabled',true);
	$("#"+ddname+" optgroup").addClass("ddhidedisplay");
	if(businesschargedata !='' && businesschargedata != 'null'){ 
		if(businesschargedata.indexOf(',') > -1) {
			var charge = businesschargedata.split(',');
				for(j=0;j<(charge.length);j++) {
					$("#"+ddname+" option[value="+charge[j]+"]").removeClass("ddhidedisplay");
					$("#"+ddname+" option[value="+charge[j]+"]").prop('disabled',false);
					$("#"+ddname+" option[value="+charge[j]+"]").closest('optgroup').removeClass("ddhidedisplay");
				}
		}else{
			$("#"+ddname+" option[value="+businesschargedata+"]").removeClass("ddhidedisplay");
			$("#"+ddname+" option[value="+businesschargedata+"]").prop('disabled',false);
			$("#"+ddname+" option[value="+businesschargedata+"]").closest('optgroup').removeClass("ddhidedisplay");
		}
	}
}
function fieldenabledisable(val,field){
	if(val == 'YES' || val == 'Yes'){
		$("#"+field+"").attr('readonly',false);
	}else{
		$("#"+field+"").attr('readonly',true);
	}
}
function chargecalchideshow(val){
	var ddname ="chargecalculation";
	$("#"+ddname+" option").addClass("ddhidedisplay");
	$("#"+ddname+" option").prop('disabled',true);
	var data ='';
	if(val != 3){ // charge mode min,max
		if(val == 1){  // min
			data = 2;
		}else if(val == 2){ // max
			data = 1;
		}
		$("#"+ddname+" option[value="+data+"]").removeClass("ddhidedisplay");
		$("#"+ddname+" option[value="+data+"]").prop('disabled',false);
		$("#"+ddname+"").select2('val',data).trigger('change');
	}else{
		$("#"+ddname+" option").removeClass("ddhidedisplay");
		$("#"+ddname+" option").prop('disabled',false);
	}
}
function getproductchargerow(){
	$.ajax(
	{
		url:base_url+"Company/getproductchargerow", 
		dataType:'json',
		async:false,
		success: function(data)
		{
		  	if(data == 0){
				$("#chargecalcoption,#chargecalculation").attr('readonly',false);
			}else{
				$("#chargecalcoption,#chargecalculation").attr('readonly',true);
			}
		}		
	});
}
{//date Range
	function daterangefunction() {
		$('#applicablefromdate').datetimepicker("destroy");
		$('#applicabletodate').datetimepicker("destroy");
		var startdateformater = $('#applicablefromdate').attr('data-dateformater');
		var startDateTextBox = $('#applicablefromdate');
		var endDateTextBox = $('#applicabletodate');
		var minimumdate = $("#applicablefromdate");
		var maximumdate = $("#applicablefromdate");
		startDateTextBox.datetimepicker({
			//timeFormat: 'HH:mm z',
			showTimepicker :false,
			dateFormat: startdateformater,
			changeMonth: true,
			changeYear: true,
			//minDate:null,
			onClose: function(dateText, inst) {
				if (endDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						endDateTextBox.datetimepicker('setDate', testStartDate);
						Materialize.updateTextFields();
				} else {
					endDateTextBox.val(dateText);
					Materialize.updateTextFields();
				}
				$('#applicablefromdate').focus();
			},
			onSelect: function (selectedDateTime) {
				endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
				minimumdate.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
				maximumdate.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
				var checkdate = $(this).val();
				if(checkdate != '') {
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
					$(this).prev('div').remove();
					//$('.activityenddateformError').remove();
					$('#applicabletodate').removeClass('error');
					Materialize.updateTextFields();
				}
			}
		});
		endDateTextBox.datetimepicker({
			//timeFormat: 'HH:mm z',
			dateFormat: startdateformater,
			showTimepicker :false,
			changeMonth: true,
			changeYear: true,
			//minDate:0,
			onClose: function(dateText, inst) {
				if (startDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						Materialize.updateTextFields();
						maximumdate.datetimepicker('setDate', testEndDate);
				} else {
					Materialize.updateTextFields();
				}
				$('#applicabletodate').focus();
			},
			onSelect: function (selectedDateTime) {
				minimumdate.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
				maximumdate.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
				Materialize.updateTextFields();
			}
		});
	}
}
//deviceno automatic tokenzation
function devicenoautogenerate(){
		 $("#deviceno").select2({
			tags: [],
			tokenSeparators: [',', ' '],
			tokenizer: function(input, selection, callback) {

				// no comma no need to tokenize
				if (input.indexOf(',') < 0 && input.indexOf(' ') < 0)
					return;

				var parts = input.split(/,| /);
				for (var i = 0; i < parts.length; i++) {
					var part = parts[i];
					part = part.trim();

					callback({id:part,text:part});
				}
			}
		});
	}