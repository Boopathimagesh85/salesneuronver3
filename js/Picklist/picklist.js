$(document).ready(function(){	
	{//Foundation Initialization
		$(document).foundation();
	}
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		picklistaddgrid('','');
		crudactionenable();
	}
	$("#picklistviewtoggle").hide();
	{
	//keyboard shortcut reset global variable
		viewgridview=0;
		addformview=0;
	}
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			picklistaddgrid();
		});
	}
	$( window ).resize(function() {
		innergridresizeheightset('picklistaddgrid');
	});
	//submit button show hide
	$('#picklistsavebutton').show();
	$('#picklistdataupdatesubbtn').hide();
	//module change events
	{
		$('#picklistmoduleid').trigger('change');
		$('#picklistmoduleid').change(function(){
			cleargriddata('picklistaddgrid');
			$("#picklistname").select2('val',' ');
			$("#picklistvalue").val('');
			var id = $(this).val();
			dropdownmodulevalset('picklistname','picklistnameidhidden','modfiledcolumn','fieldlabel','modulefieldid','columnname','modulefield','moduletabid,uitypeid',''+id+',17|18');
		});
	}
	{//validation for pick list data add 
		$('#picklistsavebutton').on('mousedown click',function(e){
			if(e.which == 1 || e.which === undefined) {
				$("#picklistvalue").focusout();
				$("#picklistformaddwizard").validationEngine('validate');
				// For Touch
				masterfortouch = 0;
			}
		});
		$("#picklistformaddwizard").validationEngine({
			onSuccess: function() {
				$('#picklistsavebutton').attr('disabled','disabled');
				$("#picklistvalue").removeClass('validate[required,maxSize[100],funcCall[picklistnamecheck]]');
				picklistnewdataaddfun();
			},
			onFailure: function() {
				var dropdownid =['2','picklistmoduleid','picklistname'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//pick list name drop down change
		$('#picklistname').change(function(){
			$('#picklistvalue').val('');
			var tabnameid = $('#picklistname').find('option:selected').data('modfiledcolumn');
			var picklistmoduleid = $('#picklistmoduleid').val();
			if($('#picklistname').val() == '') {
				cleargriddata('picklistaddgrid');
				picklistaddgrid('','');
			} else if($('#picklistname').val() != "" && picklistmoduleid!= ""){
				clearform('ui-search-toolbar');
				picklistaddgrid(tabnameid,picklistmoduleid)
			}
		});
	}
	{//tool bar click function	
		//picklist reload icon
		$("#picklistreloadicon").click(function(){
			cleargriddata('picklistaddgrid');
			$("#picklistdeleteicon,#picklistsavebutton,#picklistaddicon").show();
			$("#picklistdataupdatesubbtn").hide();
			resetFields();
			$('#picklistaddgrid').empty();
			$('#picklistaddgridfooter').empty();
		});
	}
	{//picklist updation
		$('#picklistdataupdatesubbtn').on('mousedown click',function(e) {
			if(e.which == 1 || e.which === undefined) {
				$("#picklistvalue").focusout();
				$("#picklistformeditwizard").validationEngine('validate');
			}
		});
		jQuery("#picklistformeditwizard").validationEngine({
			onSuccess: function() {
				$("#picklistvalue").removeClass('validate[required,maxSize[100],funcCall[picklistnamecheck]]');
				picklistdataupdatefunction();
			},
			onFailure: function() {
				var dropdownid =['2','picklistmoduleid','picklistname'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	$("#tab1").click(function() {
		setTimeout(function(){
			$("input:text:visible:first").focus();
		},100);
		cleargriddata('picklistaddgrid');
	});
	setTimeout(function(){
		$("input:text:visible:first").focus();
	},100);	
	{//filter work
		//for toggle
		$('#picklistviewtoggle').click(function(){
			if ($(".filterview").is(":visible")) {
				$('.picklistfilterview').addClass("hidedisplay");
				$('.picklistfullgridview').removeClass("large-9");
				$('.picklistfullgridview').addClass("large-12");
			}else{
				$('.picklistfullgridview').removeClass("large-12");
				$('.picklistfullgridview').addClass("large-9");
				$('.picklistfilterview').removeClass("hidedisplay");
			}			
		});
		$('#picklistclosefiltertoggle').click(function(){
			$('.picklistfilterview').addClass("hidedisplay");
			$('.picklistfullgridview').removeClass("large-9");
			$('.picklistfullgridview').addClass("large-12");
		});
		//filter
		$("#picklistfilterddcondvaluedivhid").hide();
		$("#picklistfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#picklistfiltercondvalue").focusout(function(){
				var value = $("#picklistfiltercondvalue").val();
				$("#picklistfinalfiltercondvalue").val(value);
			});
			$("#picklistfilterddcondvalue").change(function(){
				var value = $("#picklistfilterddcondvalue").val();
				$("#picklistfinalfiltercondvalue").val(value);
			});
		}
		picklistfiltername = [];
		$("#picklistfilteraddcondsubbtn").click(function() {
			$("#picklistfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#picklistfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				var columnid =$("#picklistfiltercolumn").val();
				var label =$("#picklistfiltercolumn").find('option:selected').attr('data-label');
				var condition = $("#picklistfiltercondition").val();
				var value = $("#picklistfinalfiltercondvalue").val();
				var data = columnid+"|"+condition+"|"+value;
				var count = picklistfiltername.length;
				$("#picklistfilterconddisplay").append("<span style='word-wrap:break-word;'>"+data+"</span><i class='material-icons picklistfilterdocclsbtn' data-fileid='"+count+"'>close</i>");
				picklistfiltername.push(data);
				$("#picklistconditionname").val(picklistfiltername);
				picklistaddgrid();
				$(".picklistfilterdocclsbtn").click(function() {
					var id = $(this).data("fileid");
					var count = picklistfiltername.length;
					for(var k=0;k<count;k++) {
						if( k == id) {
							var nvalue = $("#picklistconditionname").val();
							var filternewcond = nvalue.split(',');
							picklistfiltername = jQuery.grep(filternewcond, function(value) {
							  return value != filternewcond[id];
							});
							$("#picklistconditionname").val(picklistfiltername);
							picklistaddgrid();
						}
					}
					$(this).prev("span").remove();
					$(this).next("br").remove();
					$(this).remove();
				});
				$("#picklistfiltercolumn").select2('val','');
				$("#picklistfiltercondition").select2('val','');
				$("#picklistfilterddcondvalue").select2('val','');
				$("#picklistfiltercondvalue").val('');
				$("#picklistfilterfinalviewcondvalue").val('');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	$('#picklistaddform').find('input, textarea, button, select').attr('disabled',true);
	{//add icon click
		$('#picklistaddicon').click(function(){
			$('#picklistaddform').find('input, textarea, button, select').attr('disabled',false);
			$("#picklistvalue").removeClass('validate[required,maxSize[100],funcCall[picklistnamecheck]]');
			$("#picklistvalue").addClass('validate[required,maxSize[100],funcCall[picklistnamecheck]]');
			firstfieldfocus();
		});
	} 
});
//Picklist Add Grid
function picklistaddgrid(tabnameid,picklistmoduleid,page,rowcount) {
	if(tabnameid != '') {
		var tabname = tabnameid.substring(0,tabnameid.length-2);
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = $('#picklistaddgridwidth').width();
		var wheight = $('#picklistaddgridwidth').height();
		//col sort
		var sortcol = $("#picklistsortcolumn").val();
		var sortord = $("#picklistsortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.picklistvalueheadercolsort').hasClass('datasort') ? $('.picklistvalueheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.picklistvalueheadercolsort').hasClass('datasort') ? $('.picklistvalueheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.picklistvalueheadercolsort').hasClass('datasort') ? $('.picklistvalueheadercolsort.datasort').attr('id') : '0';
		$.ajax({
			url:base_url+"Picklist/dropdowntabdatafetch?maintabinfo="+tabname+"&primaryid="+tabnameid+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid='+picklistmoduleid+"&checkbox=true",
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#picklistaddgrid').empty();
				$('#picklistaddgrid').append(data.content);
				$('#picklistaddgridfooter').empty();
				$('#picklistaddgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				/*column resize*/
				columnresize('picklistaddgrid');
				innergridresizeheightset('picklistaddgrid');
				{//sorting
					$('.picklistvalueheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.picklistvalueheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						var sortcol = $('.picklistvalueheadercolsort').hasClass('datasort') ? $('.picklistvalueheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.picklistvalueheadercolsort').hasClass('datasort') ? $('.picklistvalueheadercolsort.datasort').data('sortorder') : '';
						$("#picklistsortorder").val(sortord);
						$("#picklistsortcolumn").val(sortcol);
						picklistaddgrid(tabnameid,picklistmoduleid,page,rowcount);
						$("#processoverlay").hide();
					});
					sortordertypereset('picklistaddgrid',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						picklistaddgrid(tabnameid,picklistmoduleid,page,rowcount);
						$("#processoverlay").hide();
					});
					$('#picklistvaluepgrowcount').change(function(){
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('#prev').data('pagenum');
						picklistaddgrid(tabnameid,picklistmoduleid,page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//header check box
				$(".picklistvalue_headchkboxclass").click(function() {
					checkboxclass('picklistvalue_headchkboxclass','picklistvalue_rowchkboxclass');
					getcheckboxrowid();
				});
				//row based check box
				$(".picklistvalue_rowchkboxclass").click(function(){
					$('.picklistvalue_headchkboxclass').removeAttr('checked');
					getcheckboxrowid();
				});
				if(deviceinfo != 'phone') {
					{//Grid data row sorting
						var tablnameid = $('#picklistname').find('option:selected').data('modfiledcolumn');
						var len = tablnameid.length;
						var tablname = tablnameid.substring(0,(len-2));
						var sortable = document.getElementById("picklistvalue-sort");
						Sortable.create(sortable,{
							group: 'picklistelements',
							onEnd: function (evt) {
								picksortorderupdate('picklistaddgrid',tablname,tablnameid,'sortorder');
						    },
						});
					}
				}
				$('#picklistvaluepgrowcount').material_select();
			}
		});
	}
}
//sort order data update
function picksortorderupdate(gridname,tablename,tableid,sortfieldname) {
	var datarowid = getgridallrowids(gridname);
	$.ajax({
		url:base_url + "Picklist/datarowsorting",
		data:"datas=" + "&rowids="+datarowid+'&tablename='+tablename+'&primaryid='+tableid+'&sortfield='+sortfieldname,
		type:"POST",
		aync:false,
		cache:false,
		success: function(msg) {
		},
	});
}
function checkboxclass(headerclass,rowclass) {
	if($("."+headerclass+"").prop('checked') == true) {
		$("."+rowclass+"").attr('checked', true);
	} else {
		$("."+rowclass+"").removeAttr('checked');
	}
}
function getcheckboxrowid() {
	var selected = [];
	$('.gridcontent input:checked').each(function() {
		selected.push($(this).attr('data-rowid'));
	});
}
function crudactionenable() {
	{//pick list add icon click
		$('#picklistaddicon').click(function(){
			$("#picklistsectionoverlay").removeClass("closed");
			$("#picklistsectionoverlay").addClass("effectbox");
			$('#picklistdataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#picklistsavebutton,#picklistaddicon').show();
			$('#picklistmoduleid,#picklistname').prop('disabled',false);
		});
	}
	$("#picklistediticon").click(function(e){
		e.preventDefault();
		$('#picklistaddform').find('input, textarea, button, select').attr('disabled',false);
		var tabnameid = $('#picklistname').find('option:selected').data('modfiledcolumn');
		var tabname = tabnameid.substring(0,tabnameid.length-2);
		var filedname = tabname+'name';
		var selrow = getselectedrowids('picklistaddgrid');
		if(selrow.length == 1) {
			$.ajax({
				url:base_url+"Picklist/editpicklistuseridcheck",
				type:'POST',
				data:"id="+selrow+"&tabnameid="+tabnameid,
				cache:false,
				success:function(data) {
					var nmsg =  $.trim(data);
					if(nmsg == 'TRUE') {
						$("#picklistsectionoverlay").removeClass("closed");
						$("#picklistsectionoverlay").addClass("effectbox");
						var id = $('#picklistaddgrid div.gridcontent div.active').attr('id');
						var name = getgridcolvalue('picklistaddgrid',selrow,filedname,'');
						$("#picklistvalue").val(name);
						$("#picklistid").val(id);
						$('#picklistsavebutton,#picklistdeleteicon,#picklistaddicon').hide();
						$('#picklistdataupdatesubbtn').show().removeClass('hidedisplayfwg');
						$('#picklistmoduleid,#picklistname').prop('disabled',true);
						firstfieldfocus();
						// For Touch
						masterfortouch = 1;	 
						//Keyboard short cut
						saveformview = 1;
						Materialize.updateTextFields();
					} else {
						alertpopup("You cant Edit this record");
					}
				}
			});
		} else {
			alertpopup("Please select single row");
		}
	});
	$("#picklistdeleteicon").click(function(e){
		e.preventDefault();
		var tabnameid = $('#picklistname').find('option:selected').data('modfiledcolumn');
		var datarowid = getselectedrowids('picklistaddgrid');
		if(datarowid.length>=1) {
			combainedmoduledeletealert('picklistdeleteyes');
			$("#picklistdeleteyes").click(function(){
				$.ajax({
					url:base_url+"Picklist/deletepicklistvalue",
					type:'POST',
					data:"id="+datarowid+"&tabnameid="+tabnameid,
					cache:false,
					success:function(data) {
						var nmsg =  $.trim(data);
						if(nmsg == 'TRUE') {
							picklistrefreshgrid();
							$("#basedeleteoverlayforcommodule").fadeOut();
							$("#basedeleteoverlayforcommodule").hide();
							alertpopup('Record deleted successfully');
						} else {
							$("#basedeleteoverlayforcommodule").fadeOut();
							$("#basedeleteoverlayforcommodule").hide();
							alertpopup("You cant delete this record");
						}
					}
				});
				$(this).unbind();
			});
		} else {
			alertpopup("Please select a row");
		}	
	});
}
{//refresh grid
	function picklistrefreshgrid() {
		var tabnameid = $('#picklistname').find('option:selected').data('modfiledcolumn');
		var picklistmoduleid = $('#picklistmoduleid').val();
		var page = $('ul#picklistvaluepgnum li.active').data('pagenum');
		var rowcount = $('ul#picklistvaluepgnumcnt li .page-text .active').data('rowcount');
		picklistaddgrid(tabnameid,picklistmoduleid,page,rowcount);
	}
}
//new data add submit function
function picklistnewdataaddfun() {
	var picklistmoduleid = $("#picklistmoduleid").val();
	var picklistname = $("#picklistname").val();
	var tabnameid = $('#picklistname').find('option:selected').data('modfiledcolumn');
    var formdata = $("#picklistaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	$.ajax({
		url: base_url + "Picklist/newdatacreate",
        data: "datas=" + datainformation+"&tabnameid="+tabnameid,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	resetFields();
            	firstfieldfocus();
				$('#picklistname').select2('val',picklistname);
				$('#picklistmoduleid').select2('val',picklistmoduleid);
				$("#picklistsavebutton").attr('disabled',false);
				$("#picklistvalue").addClass('validate[required,maxSize[100],funcCall[picklistnamecheck]]');
				picklistrefreshgrid();
				alertpopup('Data Stored Successfully');
			} else {
				$("#picklistsavebutton").attr('disabled',false);
			}
        },
    });
}
//picklist updation
function picklistdataupdatefunction() {
	var picklistmoduleid = $("#picklistmoduleid").val();
	var picklistname = $("#picklistname").val();
	var tabnameid = $('#picklistname').find('option:selected').data('modfiledcolumn');
	var id = $("#picklistid").val();
    var formdata = $("#picklistaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	$.ajax({
        url: base_url + "Picklist/picklistdataupdatefun",
        data: "datas=" + datainformation+"&tabnameid="+tabnameid+"&id="+id+"&picklistmoduleid="+picklistmoduleid,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
			if(nmsg == 'TRUE') {
				resetFields();
            	firstfieldfocus();
				$('#picklistname').select2('val',picklistname);
				$('#picklistmoduleid').select2('val',picklistmoduleid);
				picklistrefreshgrid();
				$("#picklistdataupdatesubbtn").hide();
				$("#picklistsavebutton,#picklistdeleteicon,#picklistaddicon").show();
				$("#picklistvalue").addClass('validate[required,maxSize[100],funcCall[picklistnamecheck]]');
				alertpopup('Data Updated Successfully');
				$('#picklistmoduleid,#picklistname').prop('disabled',false);
				// For Touch
				masterfortouch = 0;
				//Keyboard short cut
				saveformview = 0;
			}
        },
    });
}
//drop down values set for view
function dropdownmodulevalset(ddname,dataattrname,otherdataattrname,dataname,dataid,otherdataname,datatable,whfield,whvalue) {
	$('#'+ddname+'').val('');
	$('#'+ddname+'').empty();
	$('#'+ddname+'').append($("<option></option>"));
	$.ajax({
		url:base_url+'Base/fetchdddatawithmultiplecond?dataname='+dataname+'&dataid='+dataid+'&datatab='+datatable+'&whdatafield='+whfield+'&whval='+whvalue+'&othername='+otherdataname,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#'+ddname+'')
					.append($("<option></option>")
					.attr("data-"+dataattrname,data[index]['datasid'])
					.attr("data-"+otherdataattrname,data[index]['otherdataattrname'])
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}	
			$('#'+ddname+'').trigger('change');
		},
	});
}
//picklistnamecheck
function picklistnamecheck() {
	var moduleid = $("#picklistmoduleid").val();
	var primaryid = $("#picklistid").val();
	var accname = $("#picklistvalue").val();
	var elementpartable = $('#picklistname').find('option:selected').data('modfiledcolumn');
	var nmsg = "";
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Picklist/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&moduleid="+moduleid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != "") {
				if(primaryid != nmsg) {
					return "Name already exists!";
				}
			} else {
				return "Name already exists!";
			}
		} 
	}
}