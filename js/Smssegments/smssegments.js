$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}
	METHOD = 'ADD';
	{// Grid Calling Function
		smssegmentsviewgrid();
		//crud action
		crudactionenable();
	}
	{// For touch
		fortabtouch = 0;
		cntaddcount = 0;
	}
	{//inner-form-with-grid
		$("#segmentscriingridadd1").click(function(){
			//Sectionpanel height set
			sectionpanelheight('segmentscrioverlay');
			$("#segmentscrioverlay").removeClass("closed");
			$("#segmentscrioverlay").addClass("effectbox");
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$("#segmentscricancelbutton").click(function(){
			$("#segmentscrioverlay").removeClass("effectbox");
			$("#segmentscrioverlay").addClass("closed");
		});
		$("#segmentssegingridadd2").click(function(){
			//Sectionpanel height set
			sectionpanelheight('segmentssegoverlay');
			$("#segmentssegoverlay").removeClass("closed");
			$("#segmentssegoverlay").addClass("effectbox");
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$("#segmentssegcancelbutton").click(function(){
			$("#segmentssegoverlay").removeClass("effectbox");
			$("#segmentssegoverlay").addClass("closed");
		});
	}
	{// Button show hide
		$("#segmentssegingriddelspan2").hide();
	}
	{
		$('#templatetypeid').change(function() {
			var ttid = $("#templatetypeid").val();
			segmentgroupdd(ttid);
		});
	}
	$("#criteriaddfieldvaluedivhid").hide();
	$("#segmentssegingriddel2").hide();
	$("#segmentssegaddbutton").hide();
	{// Close Add Screen
		var addclosesmssegments =["closeaddform","smssegmentsview","smssegmentsformdiv",""];
		addclose(addclosesmssegments);
		var addcloseviewcreation =["viewcloseformiconid","smssegmentsview","viewcreationformdiv",""];
		addclose(addcloseviewcreation);
	}
	$('.frmtogridbutton').click(function(){
		var viewcount = $('#segmentscriaddgrid1 .gridcontent div.data-content div').length;
		if(viewcount == 0) {
			$('#massandorcondid').removeClass('validate[required]');
			andorcondvalidationreset();
		} else {
			$('#massandorcondid').addClass('validate[required]');
		}
		var operation = $(this).attr('id');
		var i = $(this).data('frmtogriddataid');
		var gridname = $(this).data('frmtogridname');			
		gridformtogridvalidatefunction(gridname,operation);
		griddataid = i;
		gridynamicname = gridname;
		$("#"+gridname+"validation").validationEngine('validate');
		METHOD == 'ADD';
		segmentssegaddgrid2();
		andorcondvalidationreset();
	});
	$("#formclearicon").click(function(){
		var elementname = $('#elementsname').val();
		elementdefvalueset(elementname);
		//for autonumber
		randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
		setdefaultproperty();
		//cleargrid data
		cleargriddata('segmentscriaddgrid1');
		METHOD = 'ADD';
	});
	$('#tab2').click(function() {
		var smsgroupsid = $("#smsgroupsid").val();
		if(smsgroupsid != ''){
			$('.active').removeClass('active');
			$(this).addClass('active');
			$(this).find('span').removeAttr('style');
			$("#subformspan2").show();
			$("#subformspan1").hide();
		} else {
			alertpopup('Please select Group name.');
			$('#tab1').trigger('click');
			setTimeout(function() {
				//transtion 
				$('#subformspan1').addClass('form-active');
			},500);
			$("#subformspan1").show();
			$("#subformspan2").hide();
			$('#tabgropdropdown').select2('val','1');
		}
		//for touch
		mastertabid=2;
		Materialize.updateTextFields();
	});
	//list name change
	$("#smsgroupsid").change(function() {
		var groupname = $("#smsgroupsid").find('option:selected').data('smsgroupsidhidden');
		$("#smsgroupsidname").val(groupname);
		cleargriddata('segmentscriaddgrid1');
		$("#subscriberid").val('');
		segmentssegaddgrid2();
	});
	//Condition name change
	$("#conditionid").change(function() {
		var condname = $("#conditionid").find('option:selected').data('conditionidhidden');
		$("#condition").val(condname);
	});
	//Condition name change
	$("#massandorcondid").change(function() {
		var andorname = $("#massandorcondid").find('option:selected').data('massandorcondidhidden');
		$("#massandorcond").val(andorname);
	});
	//formfields  hidden value assign
	$("#filedid").change(function() {
		var fieldlable = $("#filedid").val();
		var ffname = $("#filedid").find('option:selected').data('ffname');
		$("#filedidname").val(fieldlable);
		var jointable = $("#filedid").find('option:selected').data('jointable');
		var indexname = $("#filedid").find('option:selected').data('indexname');
		var id = $("#filedid").find('option:selected').data('id');
		$("#jointable").val(jointable);
		$("#indexname").val(indexname);
		$("#fieldid").val(id);
		var uitypeid = $("#filedid").find('option:selected').data('uitype');
		if(uitypeid == '2' || uitypeid == '3' || uitypeid == '4'|| uitypeid == '5' || uitypeid == '6' || uitypeid == '7' || uitypeid == '10' || uitypeid == '11' || uitypeid == '12' || uitypeid == '14') {
			showhideintextbox('criteriaddfieldvalue','value');
			$('#value').val('');
			$('#value').datetimepicker("destroy");
			$("#criteriaddfieldvalue").removeClass('validate[required]');
			$("#value").prop("disabled", false);
			$("#value").addClass('validate[required,maxSize[100]]');
		} else if(uitypeid == '17' || uitypeid == '28' ) {
			showhideintextbox('value','criteriaddfieldvalue');
			$("#criteriaddfieldvalue").addClass('validate[required]');
			$("#value").removeClass('validate[required,maxSize[100]]');
			var fieldname = $("#filedid").find('option:selected').data('indexname');
			fieldmassnamebesdpicklistddvalue(fieldname);
		} else if(uitypeid == '18' || uitypeid == '19'|| uitypeid == '20' || uitypeid == '21'|| uitypeid == '25' || uitypeid == '26' || uitypeid == '27'  || uitypeid == '29'){
			showhideintextbox('value','criteriaddfieldvalue');
			$("#criteriaddfieldvalue").addClass('validate[required]');
			$("#value").removeClass('validate[required,maxSize[100]]');
			var fieldname = $("#filedid").find('option:selected').data('indexname');
			fieldmassnamebesdddvalue(fieldname);
		} else if(uitypeid == '13') {
			showhideintextbox('value','criteriaddfieldvalue');
			$("#criteriaddfieldvalue").addClass('validate[required]');
			$("#value").removeClass('validate[required] validate[required,maxSize[100]]');
			$("#value").datepicker("disable");
			checkboxvalueget('criteriaddfieldvalue');
		}
	});
	//dropdown change value
	$("#criteriaddfieldvalue").change(function(){
		var value = $("#criteriaddfieldvalue").val();
		$("#critreiavalue").val(value);
	});
	//focus out in value
	$("#value").focusout(function(){
		var value = $("#value").val();
		$("#critreiavalue").val(value);
	});
	{//check box class
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#autoupdate').val('Yes');
			} else {
				$('#autoupdate').val('No');
			}
		});
	}
	//email segment submit
	$("#dataaddsbtn").click(function(e) {
		$("#segmentssegaddgrid2validation").validationEngine('validate');
	});
	jQuery("#segmentssegaddgrid2validation").validationEngine({
		onSuccess: function() {
			smsegmentinsertion();
		},
		onFailure: function() {
			var dropdownid =['',''];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}	
	});
	//updateCommands
	$("#dataupdatesubbtn").click(function(e) {
		$("#segmentssegeditgrid2validation").validationEngine('validate');
	});
	jQuery("#segmentssegeditgrid2validation").validationEngine({
		onSuccess: function() {
			smssegmentupdateval();
		},
		onFailure: function() {
			var dropdownid =['',''];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}	
	});
	//criteria delete
	$("#segmentscriingriddel1").click(function() {
		var datarowid = $('#segmentscriaddgrid1 div.gridcontent div.active').attr('id');
		if(datarowid) {		
			if(METHOD == 'UPDATE') {
				alertpopup("A Record Under Edit Form");
			} else {
				if(datarowid != 1) {
					var did = $('#conrowcolids').val();
					if(did != '') {
						$('#conrowcolids').val(datarowid);
					} else {
						$('#conrowcolids').val(did+','+datarowid);
					}
				}	
				/*delete grid data*/
				deletegriddatarow('segmentscriaddgrid1',datarowid);
				var viewcount = $('#segmentscriaddgrid1 .gridcontent div.data-content div').length;
				if(viewcount == 0){
					$('#smsgroupsid').attr('readonly',false);
				} else {
					$('#smsgroupsid').attr('readonly',true);
				}
				segmentssegaddgrid2();
			}
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#reloadicon").click(function() {
		refreshgrid();
	});
	$( window ).resize(function() {
		maingridresizeheightset('smssegmentsviewgrid');
		//Sectionpanel height set
		sectionpanelheight('segmentscrioverlay');
		sectionpanelheight('segmentssegoverlay');
	});
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,smssegmentsviewgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			smssegmentsviewgrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
});
function showhideintextbox(hideid,showid) {
	$("#"+hideid+"divhid").hide();
	$("#"+showid+"divhid").show();
}
//check box value get
function checkboxvalueget(dropdownname) {
	$.ajax({
		url: base_url + "Smssegments/checkboxvalueget",
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$("#"+dropdownname+"").empty();
			$("#"+dropdownname+"").append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					if(data[index]['uitype'] != '15' || data[index]['uitype'] != '16')
					{
						$("#"+dropdownname+"")
						.append($("<option></option>")
						.attr("value",data[index]['dataname'])
						.text(data[index]['dataname']));
					}
				});
			}
			$("#"+dropdownname+"").trigger('change');			
		},
	});
}
//SMS Segments View Grid
function smssegmentsviewgrid(page,rowcount) {
	if(Operation == 1){
		var rowcount = $('#segmentspgrowcount').val();
		var page = $('.paging').data('pagenum');
	} else{
		var rowcount = $('#segmentspgrowcount').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	}
	var wwidth = $('#smssegmentsviewgrid').width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.segmentsheadercolsort').hasClass('datasort') ? $('.segmentsheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.segmentsheadercolsort').hasClass('datasort') ? $('.segmentsheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.segmentsheadercolsort').hasClass('datasort') ? $('.segmentsheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#viewfieldids').val();
	var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
	var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
	var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=segments&primaryid=segmentsid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#smssegmentsviewgrid').empty();
			$('#smssegmentsviewgrid').append(data.content);
			$('#smssegmentsviewgridfooter').empty();
			$('#smssegmentsviewgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('smssegmentsviewgrid');
			{//sorting
				$('.segmentsheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.segmentsheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#smssegmentspgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.segmentsheadercolsort').hasClass('datasort') ? $('.segmentsheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.segmentsheadercolsort').hasClass('datasort') ? $('.segmentsheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					var sortpos = $('#smssegmentsviewgrid .gridcontent').scrollLeft();
					smssegmentsviewgrid(page,rowcount);
					$('#smssegmentsviewgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('segmentsheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					Operation = 0;
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					smssegmentsviewgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#segmentspgrowcount').change(function(){
					Operation = 0;
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = 1;
					smssegmentsviewgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			//Material select
			$('#segmentspgrowcount').material_select();
		},
	});
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function() {
			segmentscriaddgrid1();
			{//Filed name load
				var id = '271';
				conditionfieldnamelist(id);
			}
			addslideup('smssegmentsview','smssegmentsformdiv');
			resetFields();
			firstfieldfocus();
			//For tablet and mobile Tab section reset
			$('#autoupdate').val('No');
			$('#tabgropdropdown').select2('val','1');
			fortabtouch = 0;
			$("#tab1").trigger('click');
			$("#smsgroupsid").trigger('change');
			$('#smsgroupsid, #templatetypeid').select2('readonly',false);
			cleargriddata('segmentscriaddgrid1');
			$("#massandorcondiddivhid").hide();
			$("#dataaddsbtn").show();
			$("#dataupdatesubbtn").hide();
			Materialize.updateTextFields();
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
		});
		$("#editicon").click(function() {
			var datarowid = $('#smssegmentsviewgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				segmentscriaddgrid1();
				{//Filed name load
					var id = '271';
					conditionfieldnamelist(id);
				}
				addslideup('smssegmentsview','smssegmentsformdiv');
				smssegmentsdatafetch(datarowid);
				$("#templatetypeid, #smsgroupsid").select2('readonly',true);
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				$("#segmentssegaddbutton,#segmentssegupdatebutton").hide();
				$("#dataaddsbtn").hide();
				$("#dataupdatesubbtn").show();
				//For Keyboard Shortcut Variables
				addformupdate = 1;
				viewgridview = 0;
				addformview = 1;
				Materialize.updateTextFields();
				Operation = 1; //for pagination
			} else {
				alertpopup('Please select the row');
			}
		});
		$("#deleteicon").click(function() {
			var datarowid = $('#smssegmentsviewgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				$('#primarydataid').val(datarowid);
				$("#basedeleteoverlay").fadeIn();
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function() {
			var datarowid = $('#primarydataid').val();
			smssegmentdelete(datarowid);
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#smssegmentspgnum li.active').data('pagenum');
		var rowcount = $('ul#smssegmentspgnumcnt li .page-text .active').data('rowcount');
		smssegmentsviewgrid(page,rowcount);
	}
}
//AND/OR  condition validation reset
function andorcondvalidationreset() {
	var viewcount = $('#segmentscriaddgrid1 .gridcontent div.data-content div').length;
	if(viewcount == 0) {
		$('#massandorcondid').removeClass('validate[required]');
		$('#massandorcondiddivhid > label > span').text('');
		$('#massandorcondiddivhid').hide();
	} else {
		$('#massandorcondiddivhid').show();
		$('#massandorcondid').addClass('validate[required]');
		$('#massandorcondiddivhid > label > span').text('*');
	}
}
//SMS Criteria Add Grid
function segmentscriaddgrid1() {
	var wwidth = $("#segmentscriaddgrid1").width();
	var wheight = $("#segmentscriaddgrid1").height();
	$.ajax({
		url:base_url+"Base/localgirdheaderinformationfetch?tabgroupid=157&moduleid=25&width="+wwidth+"&height="+wheight+"&modulename=smssgecreteria",
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#segmentscriaddgrid1").empty();
			$("#segmentscriaddgrid1").append(data.content);
			$("#segmentscriaddgrid1footer").empty();
			$("#segmentscriaddgrid1footer").append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('segmentscriaddgrid1');
			var hideprodgridcol = ['smsgroupsid','filedid','conditionid','value','criteriaddfieldvalue','massandorcondid','jointable','indexname','criteriaddfieldvaluename','criteriaid','condition','massandorcond','fieldid'];
			gridfieldhide('segmentscriaddgrid1',hideprodgridcol);
		},
	});
}
//SMS Segment Add Grid
function segmentssegaddgrid2(page,rowcount) {
	var smsgroupsid = $("#smsgroupsid").val();
	var id = '271';
	if(id != "") {
		$.ajax({
			url:base_url+"Smssegments/defaultviewfetch?modid="+id,
			async:false,
			cache:false,
			success:function(data) {
				if(data != "") {
					viewid = data;
					$("#defaultviewcreationid").val(viewid);
				} else {
					viewid = '28';
					$("#defaultviewcreationid").val(viewid);
				}
			},
		});
		$.ajax({
			url:base_url+"Smssegments/parenttableget?moduleid="+id,
			dataType:'json',
			async:false,
			cache:false,
			success:function(pdata) {
				if(pdata != "") {
					maintabinfo = pdata;
					$("#parenttable").val(pdata);
					parentid = maintabinfo+"id";
					$("#parenttableid").val(parentid);
				} else {
					maintabinfo = 'subscribers';
					parentid = 'subscribersid';
				}
			},
		});
	} else {
		viewid = '28';
		maintabinfo = 'subscribers';
		parentid = 'subscribersid';
		$("#defaultviewcreationid").val(viewid);
		$("#parenttable").val(maintabinfo);
		$("#parenttableid").val(parentid);
	}
	//criteria grid details
	var gridData = getgridrowsdata('segmentscriaddgrid1');
	var cricount = $('#segmentscriaddgrid1 .gridcontent div.data-content div').length;
	var criteriagriddata = JSON.stringify(gridData);
	
	/* var page = $('.paging').data('pagenum');
	var rowcount = $('#subscriberlistpgrowcount').val(); */
	
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#segmentssegaddgrid2').width();
	var wheight = $('#segmentssegaddgrid2').height();
	//col sort
	var sortcol = $('.subscriberlistheadercolsort').hasClass('datasort') ? $('.subscriberlistheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.subscriberlistheadercolsort').hasClass('datasort') ? $('.subscriberlistheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.subscriberlistheadercolsort').hasClass('datasort') ? $('.subscriberlistheadercolsort.datasort').attr('id') : '0';
	$.ajax({
		url:base_url+"Smssegments/gridvalinformationfetch?maintabinfo="+maintabinfo+"&primaryid="+parentid+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+"&viewid="+viewid+"&smsgroupsid="+smsgroupsid+"&checkbox=true&moduleid="+id+"&griddata="+criteriagriddata+"&cricount="+cricount,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#segmentssegaddgrid2').empty();
			$('#segmentssegaddgrid2').append(data.content);
			$('#segmentssegaddgrid2footer').empty();
			$('#segmentssegaddgrid2footer').append(data.footer);
			//data row select event
			datarowselectevt();
			/*column resize*/
			columnresize('segmentssegaddgrid2');
			{//sorting
				$('.subscriberlistheadercolsort').click(function(){
					$('.subscriberlistheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('ul#subscriberlistpgnum li.active').data('pagenum');
					var rowcount = $('ul#subscriberlistpgnumcnt li .page-text .active').data('rowcount');
					var sortpos = $('#segmentssegaddgrid2 .gridcontent').scrollLeft();
					segmentssegaddgrid2(page,rowcount);
					$('#segmentssegaddgrid2 .gridcontent').scrollLeft(sortpos);//scroll to sorted position
				});
				sortordertypereset('subscriberlistheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					//var rowcount = $('ul#subscriberlistpgnum li .page-text .active').data('rowcount');
					var rowcount = $('#subscriberlistpgrowcount').val();
					segmentssegaddgrid2(page,rowcount);
				});
				$('.pagerowcount').click(function(){
					var page = $('ul#subscriberlistpgnum li.active').data('pagenum');
					var rowcount = $(this).data('rowcount');
					segmentssegaddgrid2(page,rowcount);
				});
				$('#subscriberlistpgrowcount').change(function(){
					var rowcount = $(this).val();
					var page = $('ul#subscriberlistpgnum li.active').data('pagenum');
					segmentssegaddgrid2(page,rowcount);
				});
			}
			//header check box
			$(".subscriberlist_headchkboxclass").click(function() {
				checkboxclass('subscriberlist_headchkboxclass','subscriberlist_rowchkboxclass');
				if($(".subscriberlist_headchkboxclass").prop('checked') == true) {
					$(".subscriberlist_rowchkboxclass").attr('checked', true);
				} else {
					$('.subscriberlist_rowchkboxclass').removeAttr('checked');
				}
				getcheckboxrowid(id);
			});
			//row based check box
			$(".subscriberlist_rowchkboxclass").click(function(){
				$('.subscriberlist_headchkboxclass').removeAttr('checked');
				getcheckboxrowid(id);
			});
			//Material select
			$('#subscriberlistpgrowcount').material_select();
			checktheselectedinvitevalues();
		}
	});
	andorcondvalidationreset();
}
function checkboxclass(headerclass,rowclass) {
	if($("."+headerclass+"").prop('checked') == true) {
		$("."+rowclass+"").attr('checked', true);
	} else {
		$("."+rowclass+"").removeAttr('checked');
	}
}
function getcheckboxrowid(invitemoduleid) {
	var selected = [];
	$('.gridcontent input:checked').each(function() {
		selected.push($(this).attr('data-rowid'));
	});
	$("#subscriberid").val(selected);
}
//field name show
function conditionfieldnamelist(ids) {
	$('#filedid').empty();
	$('#filedid').val('');
	$.ajax({
		url: base_url + "Smssegments/viewdropdownload?ids="+ids+"&autonum=No",
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			$('#filedid').empty();
			$('#filedid').append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#filedid')
					.append($("<option></option>")
					.attr("data-jointable",data[index]['jointable'])
					.attr("data-indexname",data[index]['indexname'])
					.attr("data-uitype",data[index]['uitype'])
					.attr("data-id",data[index]['datasid'])
					.attr("data-filedidhidden",data[index]['dataname'])
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}
		},
	});
}
//check the selected invite user 
function checktheselectedinvitevalues() {
	var leadid = $("#subscriberid").val();
	if(leadid != '') {
		var lid = leadid.split(',');
		checkboxcheck(lid);
	}
}
function checkboxcheck(checkid) {
	for(var i=0;i <checkid.length;i++){
		$('#subscriberlist_rowchkbox'+checkid[i]).attr('checked',true);
	}
} 
//sms segmentation submit form
function smsegmentinsertion() {
	var ttid = $("#templatetypeid").val();
	var autoupdate = $("#autoupdate").val();
	var segmentname = $("#smssegmentsname").val();
	var decription = $("#description").val();
	var groupid = $("#smsgroupsid").val();
	var ids = $("#subscriberid").val();
	var gridname = 'segmentscriaddgrid1';
	var gridnames = gridname.split(',');
	var datalength = gridnames.length;
	var noofrows=0;
	var addgriddata='';
	for(var j=0;j<datalength;j++) {
		if(j!=0) {
			addgriddata = addgriddata+'&'+getgridrowsdata(gridnames[j]);
			noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;;
		} else {
			addgriddata = getgridrowsdata(gridnames[j]);
			noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;;
		}
	}
	var sendformadddata = JSON.stringify(addgriddata);
	if(ids != '') {
		$.ajax(  {
			url: base_url + "Smssegments/newsegmentdatacreate",
			data: "segment=" + segmentname+"&decription="+decription+"&autoupdate="+autoupdate+"&groupid="+groupid+"&subscriberids="+ids+"&griddatas="+sendformadddata+"&numofrows="+noofrows+"&typeid="+ttid,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					resetFields();
					$('#smssegmentsformdiv').hide();
					$('#smssegmentsview').fadeIn(1000);
					refreshgrid();
					cleargriddata('segmentscriaddgrid1');
					cleargriddata('segmentssegaddgrid2');
					$("#smsgroupsid").select('val',1).trigger('change');
					alertpopup(savealert);
				}
			}
		});
	} else {
		alertpopup('Please select the subscribers for this segments');
	}
}
//email segments data fetch
function smssegmentsdatafetch(datarowid) {
	$.ajax({
		url: base_url + "Smssegments/smssegmentsdatafetch?ids="+datarowid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			var segment = data.segnmae;
			var groupid = data.groupid;
			var subsid = data.subsid;
			var autoupdate = data.autoupdate;
			var description = data.description;
			var typeid = data.typeid;
			$("#templatetypeid").select2('val',typeid);
			$("#smsgroupsid").select2('val',groupid);
			$("#smssegmentsname").val(segment);
			$("#description").val(description);
			$("#subscriberid").val(subsid);
			$("#segmentid").val(datarowid);
			if (autoupdate == 'Yes') {
				$('#autoupdatecboxid').attr('checked',true);
				$('#autoupdate').val('Yes');
			} else {
				$('#autoupdatecboxid').attr('checked',false);
				$('#autoupdate').val('No');
			}
			segmentcriteriadetail(datarowid);
			setTimeout(function(){
				$("#tab2").trigger('click');
				segmentssegaddgrid2();
			},100);
			Materialize.updateTextFields();
		},
	});
}
//email segmentation update form
function smssegmentupdateval() {
	var typeid = $("#templatetypeid").val();
	var segmentid = $("#segmentid").val();
	var autoupdate = $("#autoupdate").val();
	var segmentname = $("#smssegmentsname").val();
	var decription = $("#description").val();
	var groupid = $("#smsgroupsid").val();
	var deleterowid = $("#conrowcolids").val();
	var ids = $("#subscriberid").val();
	var gridname = $('#gridnameinfo').val();
	var gridnames = gridname.split(',');
	var datalength = gridnames.length;
	var noofrows=0;
	var addgriddata='';
	for(var j=0;j<datalength;j++) {
		if(j!=0) {
			addgriddata = addgriddata+'&'+getgridrowsdata(gridnames[j]);
			noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;;
		} else {
			addgriddata = getgridrowsdata(gridnames[j]);
			noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;;
		}
	}
	var sendformadddata = JSON.stringify(addgriddata);
	if(ids != '') {
		$.ajax(  {
			url: base_url + "Smssegments/updatesegmentdatacreate",
			data: "segment=" + segmentname+"&decription="+decription+"&autoupdate="+autoupdate+"&groupid="+groupid+"&subscriberids="+ids+"&count="+noofrows+"&viewdata="+sendformadddata+"&segmentid="+segmentid+"&deleterowid="+deleterowid+"&typeid="+typeid,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					resetFields();
					$('#smssegmentsformdiv').hide();
					$('#smssegmentsview').fadeIn(1000);
					refreshgrid();
					cleargriddata('segmentscriaddgrid1');
					cleargriddata('segmentssegaddgrid2');
					$("#smsgroupsid").select('val',1).trigger('change');
					$("#smssegmentsviewgrid").trigger('reloadGrid');
					$("#smssegmentupdate").hide();
					$("#smssegmentupdate").show();
					alertpopup(savealert);
				} else if (nmsg == "false") {
				
				}
			},
		});
	} else {
		alertpopup('Please select the subscribers for this segments');
	}
}
//sms segments delete icon
function smssegmentdelete(datarowid) {
	$.ajax({
		url: base_url + "Smssegments/smssegmentsdelete?ids="+datarowid,
		cache:false,
		success: function(data) {
			var nmsg =  $.trim(data);
			refreshgrid();
			if (nmsg == 'TRUE') {
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Segments Deleted Successfully');
			}
		},
	});
}
//field name based drop down value - picklist
function fieldmassnamebesdpicklistddvalue(fieldid){
	var moduleid = $("#viewfieldids").val();
	$.ajax({
		url: base_url + "Smssegments/fieldnamebesdpicklistddvalue?fieldid="+fieldid+"&moduleid="+moduleid,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$('#criteriaddfieldvalue').empty();
			$('#criteriaddfieldvalue').append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#v')
					.append($("<option></option>")
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}
			$('#criteriaddfieldvalue').trigger('change');			
		},
	});
}
//main drop down value
function fieldmassnamebesdddvalue(fieldid){
	var moduleid = $("#viewfieldids").val();
	$.ajax({
		url: base_url + "Smssegments/fieldnamebesdddvalue?fieldid="+fieldid+"&moduleid="+moduleid,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$('#criteriaddfieldvalue').empty();
			$('#criteriaddfieldvalue').append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#criteriaddfieldvalue')
					.append($("<option></option>")
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}
			$('#criteriaddfieldvalue').trigger('change');			
		},
	});
}
{// Form to grid base function //Used on the new add's/edit's
	function gridformtogridvalidatefunction(gridnamevalue,method) {
	//Function call for validate			
		jQuery("#"+gridnamevalue+"validation").validationEngine({
			onSuccess: function() {
				var i = griddataid;
				var gridname = gridynamicname;
				var coldatas = $('#gridcolnames'+i+'').val();
				var columndata = coldatas.split(',');
				var coluiatas = $('#gridcoluitype'+i+'').val();
				var columnuidata = coluiatas.split(',');
				var datalength = columndata.length;
				/*Form to Grid*/
				if(METHOD == 'ADD' || METHOD == '') {
					formtogriddata(gridname,METHOD,'last','');
				} else if(METHOD == 'UPDATE') {
					var selectedrow = $('#'+gridname+' div.gridcontent div.active').attr('id');
					formtogriddata(gridname,METHOD,'',selectedrow);
				}
				/* Hide columns */
				var hideprodgridcol = ['smsgroupsid','filedid','conditionid','value','criteriaddfieldvalue','massandorcondid','jointable','indexname','criteriaddfieldvaluename','criteriaid','condition','massandorcond','fieldid'];
				gridfieldhide('segmentscriaddgrid1',hideprodgridcol);
				/* Data row select event */
				datarowselectevt();
				$("#filedid").select2('val','');
				$("#conditionid").select2('val','');
				$("#massandorcondid").select2('val','');
				$("#massandorcondid").select2('val','');
				$("#value,#criteriaddfieldvalue").val('');
			},
			onFailure: function() {	
				var dropdownid =['1','productid'];
				dropdownfailureerror(dropdownid);
			}
		});
	}	
}
function segmentcriteriadetail(datarowid) {
	if(datarowid!='') {
		$.ajax({
			url:base_url+'Smssegments/smscreteriagriddatafetch?primarydataid='+datarowid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					loadinlinegriddata('segmentscriaddgrid1',data.rows,'json');
					/* data row select event */
					datarowselectevt();
					/* column resize */
					columnresize('segmentscriaddgrid1');
					var hideprodgridcol = ['smsgroupsid','filedid','conditionid','value','criteriaddfieldvalue','massandorcondid','jointable','indexname','criteriaddfieldvaluename','criteriaid','condition','massandorcond','fieldid'];
				}
			},
		});
	}
}
function segmentgroupdd(typeid) {
	$('#smsgroupsid').select2('val','');
	$('#smsgroupsid').empty();
	$('#smsgroupsid').append($("<option></option>"));
	$.ajax({
        url: base_url + "Smssegments/segmentgroupddfetch?typeid="+typeid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#smsgroupsid')
					.append($("<option></option>")
					.attr("data-smsgroupsidhidden",data[index]['dataname'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}
        },
    });
}