$(document).ready(function() {	
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		categoryaddgrid();
		categorycrudactionenable();
	}
	CATEGORYDELETE = 0;
	retrievestatus = 0;
	marketplacemasterstatus = $('#marketplacemasterstatus').val();
	//hidedisplay
	$('#categorydataupdatesubbtn,#groupcloseaddform').hide();
	$('#categorysavebutton').show();
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			categoryaddgrid();
		});
	}
	{
		//validation for category Add  
		$('#categorysavebutton').click(function(e) {
			if(e.which == 1 || e.which === undefined) {
				$("#categoryformaddwizard").validationEngine('validate');
				masterfortouch = 0;
			}
		});
		jQuery("#categoryformaddwizard").validationEngine( {
			onSuccess: function() {
				$('#processoverlay').show();
				categoryaddformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//update category information
		$('#categorydataupdatesubbtn').click(function(e) {
			if(e.which == 1 || e.which === undefined) {
				$("#categoryformeditwizard").validationEngine('validate');
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			}
		});
		jQuery(categoryformeditwizard).validationEngine({
			onSuccess: function() {
				$('#processoverlay').show();
				categoryupdateformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	{//counter hierarchy
		$('#categorylistuldata li').click(function() {
			var name=$(this).attr('data-listname');
			var listid=$(this).attr('data-listid');
			var level = parseInt($(this).attr('data-level'));
			var maxlevel = parseInt($('#maxcategorylevel').val());
			$('#parentcategory').val(name);
			$('#parentcategoryid').val(listid);
			if(level >= maxlevel){
				alertpopup('Maximum category level reached');
				return false;
			}
			var editid = $('#categoryprimarydataid').val();
			if(listid != editid) {
				categoryrefreshgrid();
			} else {
				$('#parentcategory').val('');
				$('#parentcategoryid').val(1);
				alertpopup('You cant choose the same category as parentcategory');
			}
		});	
	}
	{//Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			}else{
				$('#'+name+'').val('No');
			}		
		});
	}
   {//category action
		$( window ).resize(function() {
			maingridresizeheightset('categoryaddgrid');
			sectionpanelheight('groupsectionoverlay');
		});
   }
	 //Category-report session check
	{// Redirected form Home
		var uniquelreportsession = sessionStorage.getItem("reportunique8"); 
		if(uniquelreportsession != '' && uniquelreportsession != null) {
		}
	}	
	{//filter work
		//for toggle
		$('#categoryviewtoggle').click(function() {
			if ($(".categoryfilterslide").is(":visible")) {
				$('div.categoryfilterslide').addClass("filterview-moveleft");
			}else{
				$('div.categoryfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#categoryclosefiltertoggle').click(function(){
			$('div.categoryfilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#categoryfilterddcondvaluedivhid").hide();
		$("#categoryfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#categoryfiltercondvalue").focusout(function(){
				var value = $("#categoryfiltercondvalue").val();
				$("#categoryfinalfiltercondvalue").val(value);
				$("#categoryfilterfinalviewconid").val(value);
			});
			$("#categoryfilterddcondvalue").change(function(){
				var value = $("#categoryfilterddcondvalue").val();
				var fvalue = $("#categoryfilterddcondvalue option:selected").attr('data-ddid');
				if( $('#s2id_categoryfilterddcondvalue').hasClass('select2-container-multi') ) {
					categorymainfiltervalue=[];
					$('#categoryfilterddcondvalue option:selected').each(function(){
					    var $cvalue =$(this).attr('data-ddid');
					    categorymainfiltervalue.push($cvalue);
						$("#categoryfinalfiltercondvalue").val(categorymainfiltervalue);
					});
					$("#categoryfilterfinalviewconid").val(value);
				} else {
					$("#categoryfinalfiltercondvalue").val(fvalue);
					$("#categoryfilterfinalviewconid").val(value);
				}
			});
		}
		categoryfiltername = [];
		$("#categoryfilteraddcondsubbtn").click(function() {
			$("#categoryfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#categoryfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(categoryaddgrid,'category');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//Addform section overlay close
		$(".addsectionclose").click(function(){
			var treevalidation = $('#treevalidationcheck').val();
			var treelabel = $('#treefiledlabelcheck').val();
			resetFields();
			$("#groupsectionoverlay").removeClass("effectbox");
			$("#groupsectionoverlay").addClass("closed");
			$('#treevalidationcheck').val(treevalidation);
			$('#treefiledlabelcheck').val(treelabel);
			$('#parentcategoryid').val('');
			$('#defaultcategory').val('No');
			$('#featured').val('No');
			$('#featured').prop('checked', false);
			$('#categoryimageattachdisplay').empty();
			$('#gstrates').val('No');
			$('#gstratescboxid').prop('checked', false);
			$("#taxmasteriddivhid").hide();
		});
	}
	//gst rate check box jubair
	$("#taxmasteriddivhid").hide();
	$('#gstratescboxid').click(function(){
		var name = $(this).data('hidname');
		$("span","#taxmasteriddivhid label").remove();
		if ($(this).is(':checked')) {
			$('#'+name+'').val('Yes');
			$("#taxmasteriddivhid").show();
			$('#taxmasteriddivhid label').append('<span class="mandatoryfildclass">*</span>');
			$('#taxmasterid').addClass('validate[required]');
		} else {
			$('#'+name+'').val('No');
			$("#taxmasteriddivhid").hide();
			$('#taxmasterid').removeClass('validate[required]');
		}
	});
	if($("#gstapplicableid").val() != 1) { // if gst is not enabled in company setting
			$("#gstratesdivhid").hide();
	} else {// if gst is  enabled  in company setting
			$("#gstratesdivhid").show();
	}
});
{//view create success function
	function viewcreatesuccfun(viewname){
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}	
}
//Documents Add Grid
function categoryaddgrid(page,rowcount) {
	var defrecview = $('#mainviewdefaultview').val();
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
	var wwidth = $('#categoryaddgrid').width();
	var wheight = $(window).height();
	var parentcategoryid = $('#parentcategoryid').val();
	var cuscondtablenames = 'category';
	var cusjointableid = 'categoryid';
	var cuscondfildname = 'category.parentcategoryid';
	var cuscondvalue = ''+parentcategoryid+'';
	//label set to grid header
	var viewname = $('#dynamicdddataview').val();
	$('#viewgridheaderid').text(viewname);
	//col sort
	var sortcol = $("#categorysortcolumn").val();
	var sortord = $("#categorysortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.categoryheadercolsort').hasClass('datasort') ? $('.categoryheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.categoryheadercolsort').hasClass('datasort') ? $('.categoryheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.categoryheadercolsort').hasClass('datasort') ? $('.categoryheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#categoryviewfieldids').val();
	userviewid = userviewid == '' ? '56' : userviewid;
	var filterid = $("#categoryfilterid").val();
	var conditionname = $("#categoryconditionname").val();
	var filtervalue = $("#categoryfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=category&primaryid=categoryid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#categoryaddgrid').empty();
			$('#categoryaddgrid').append(data.content);
			$('#categoryaddgridfooter').empty();
			$('#categoryaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('categoryaddgrid');
			{//sorting
				$('.categoryheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.categoryheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.categoryheadercolsort').hasClass('datasort') ? $('.categoryheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.categoryheadercolsort').hasClass('datasort') ? $('.categoryheadercolsort.datasort').data('sortorder') : '';
					$("#categorysortorder").val(sortord);
					$("#categorysortcolumn").val(sortcol);
					var sortpos = $('#categoryaddgrid .gridcontent').scrollLeft();
					categoryaddgrid(page,rowcount);
					$('#categoryaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('categoryheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					categoryaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});	
				$('#categorypgrowcount').change(function(){ //unitofmeasureaddgridfooter
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					categoryaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#categoryaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#categorypgrowcount').material_select();
		},
	});	
}
function categorycrudactionenable() {
	
	$('#addicon').click(function(e){
		resetFields();
		e.preventDefault();
		sectionpanelheight('groupsectionoverlay');
		$("#groupsectionoverlay").removeClass("closed");
		$("#groupsectionoverlay").addClass("effectbox");
		firstfieldfocus();
		retrievestatus = 0;
		$('#categorydataupdatesubbtn').hide().removeClass('hidedisplayfwg');
		$('#categorysavebutton').show();
		defaulthideshow();
		$('#categoryprimarydataid,#categorysortorder').val('');
		$('#taxmasterid').removeClass('validate[required]');
		Materialize.updateTextFields();
	});
	$("#editicon").click(function(e) {
		e.preventDefault();
		var datarowid = $('#categoryaddgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			var editcheck = loosestonecheck(datarowid);
			if(editcheck == 'no') {
				var treevalidation = $('#treevalidationcheck').val();
				var treelabel = $('#treefiledlabelcheck').val();
				resetFields();
				retrievestatus = 1;
				sectionpanelheight('groupsectionoverlay');
				$("#groupsectionoverlay").removeClass("closed");
				$("#groupsectionoverlay").addClass("effectbox");
				$('#treevalidationcheck').val(treevalidation);
				$('#treefiledlabelcheck').val(treelabel);
				$(".addbtnclass").addClass('hidedisplay');
				$(".updatebtnclass").removeClass('hidedisplay');
				$('#categorysortorder').val('');
				categorygetformdata(datarowid);
				firstfieldfocus();
				masterfortouch = 1;
				defaulthideshow();
			} else {
				alertpopup("Category is linked with Loose Stone Item.");
			}
		} else {
			alertpopup("Please select a row");
		}
		$('#taxmasterid').removeClass('validate[required]');
		Materialize.updateTextFields();
	});
	$("#deleteicon").click(function(){
		var datarowid = $('#categoryaddgrid div.gridcontent div.active').attr('id');
		if(datarowid){	
			var editcheck = loosestonecheck(datarowid);
			if(editcheck == 'no') {
				$("#categoryprimarydataid").val(datarowid);
				$(".addbtnclass").removeClass('hidedisplay');
				$(".updatebtnclass").addClass('hidedisplay');
				$.ajax({ // checking whether this value is already in usage.
					url: base_url + "Base/multilevelmapping?level="+2+"&datarowid="+datarowid+"&table=category&fieldid=parentcategoryid&table1=product&fieldid1=categoryid&table2=''&fieldid2=''",
					success: function(msg) { 
					if(msg > 0){
						alertpopup("Kindly delete the product linked to category or delete the sub category linked to category.");						
					}else{
						combainedmoduledeletealert('categorydeleteyes');
						if(CATEGORYDELETE == 0) {
							$("#categorydeleteyes").click(function(){
								var datarowid = $("#categoryprimarydataid").val();
								categoryrecorddelete(datarowid);
							});
						}
						CATEGORYDELETE = 1;
					}
					},
				});
			} else {
				alertpopup("Category is linked with Loose Stone Item.");
			}
		} else {
			alertpopup("Please select a row");
		}
	});
}
{//refresh grid
	function categoryrefreshgrid() {
		var page = $(this).data('pagenum');
		var rowcount = $('ul#categorypgnumcnt li .page-text .active').data('rowcount');
		categoryaddgrid(page,rowcount);
	}
}
//create counter
function categoryaddformdata(){
	var categorysortorder = $('#categorysortorder').val();
    var formdata = $("#categoryaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Category/newdatacreate",
        data: "datas=" + datainformation+"&categorysortorder="+categorysortorder,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {     
            	$('#processoverlay').hide();
            	$(".addsectionclose").trigger("click");
				alertpopup(savealert);
				categoryrefreshgrid();
				setTimeout(function(){categorytreereloadfun(0);},500); 
		    }else if(nmsg == 'LEVEL') {
				$('#processoverlay').hide();
				alertpopup('Maximum level of sub category reached !!! cannot create category further.');
			}
        },
    });
}
//tree reload
function categorytreereloadfun(datarowid){
	var tablename = "category";
	var mandfield = $('#treevalidationcheck').val();
	var fieldlab = $('#treefiledlabelcheck').val();
	$.ajax({
		url: base_url + "Category/treedatafetchfun",
        data: "tabname="+tablename+'&mandval='+mandfield+'&fieldlabl='+fieldlab+'&rowid='+datarowid,
		type: "POST",
		cache:false,
        success: function(data) {
			$('.'+tablename+'treediv').empty();
			$('.'+tablename+'treediv').append(data);
			$('#categorylistuldata li').click(function() {
				var level = parseInt($(this).attr('data-level'));
				var maxlevel = parseInt($('#maxcategorylevel').val());
				var name=$(this).attr('data-listname');
				var listid=$(this).attr('data-listid');
				$('#parentcategory').val(name);
				$('#parentcategoryid').val(listid);
				categoryrefreshgrid();
				if(level >= maxlevel){
					alertpopup('Maximum category level reached');
					return false;
				}
			});
			$(function() {
				$( '#dl-menu' ).dlmenu({
						animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
				});
			});
            if(retrievestatus == 1) {			
				gettheparentcounter(datarowid);
			}
        },
	 });
}
//retrive counter data on edit
function categorygetformdata(datarowid){
	var categoryelementsname = $('#categoryelementsname').val();
	var categoryelementstable = $('#categoryelementstable').val();
	var categoryelementscolmn = $('#categoryelementscolmn').val();
	var categoryelementpartable = $('#categoryelementspartabname').val();
	$.ajax({
		url:base_url+"Category/fetchformdataeditdetails?categoryprimarydataid="+datarowid+"&categoryelementsname="+categoryelementsname+"&categoryelementstable="+categoryelementstable+"&categoryelementscolmn="+categoryelementscolmn+"&categoryelementpartable="+categoryelementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data){
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				//$('#smsmastercreationformadd').hide();
				//$('#smsmastercreationview').fadeIn(1000);
				categoryrefreshgrid();
				//For Touch
				smsmasterfortouch = 0;
			} else if((data.fail) == 'sndefault'){
				$("#groupsectionoverlay").addClass("closed");
				alertpopup('Cannot Edit/Delete default records');
				resetFields();
			} else {
				var txtboxname = categoryelementsname + ',categoryprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = categoryelementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
				if(data['gstrates'] == 'Yes'){
					$("#taxmasteriddivhid").show();
				} else {
					$("#taxmasteriddivhid").hide();
				}
				var attachfldname = $('#categoryattachfieldnames').val();
				var attachcolname = $('#categoryattachcolnames').val();
				var attachuitype = $('#categoryattachuitypeids').val();
				var attachfieldid = $('#categoryattachfieldids').val();
				attachedfiledisplay(attachfldname,attachcolname,attachuitype,attachfieldid,data);
			}
		}
	});
	categorytreereloadfun(datarowid);
	$('#categorydataupdatesubbtn').show().removeClass('hidedisplayfwg');
	$('#categorysavebutton').hide();
}
//update counter
function categoryupdateformdata(){
	var categorysortorder = $('#categorysortorder').val();
	var parentcategoryid=$("#parentcategoryid").val();
	var editcategoryid=$("#categoryprimarydataid").val();
		if(parentcategoryid!=editcategoryid){
			var formdata = $("#categoryaddform").serialize();
			var amp = '&';
			var datainformation = amp + formdata;
		    $.ajax({
		        url: base_url + "Category/datainformationupdate",
		        data: "datas=" + datainformation+"&categorysortorder="+categorysortorder,
				type: "POST",
				cache:false,
		        success: function(msg) {
					var nmsg =  $.trim(msg);
		            if (nmsg == 'TRUE') {
		            	$('#processoverlay').hide();
		            	$(".addsectionclose").trigger("click");
						$('#categorydataupdatesubbtn').hide();
						$('#categorysavebutton').show();
						categoryrefreshgrid();						
						alertpopup(updatealert);
						setTimeout(function(){categorytreereloadfun(0);},500); 	
						//for touch
						masterfortouch = 0;					
		            }else if(nmsg == 'FAILURE'){
		            	$('#processoverlay').hide();
						alertpopup("Check your Parent Category mapping. ");
					}else if(nmsg == 'LEVEL') {
						$('#processoverlay').hide();
						alertpopup('Maximum level of sub category reached !!! cannot create category further.');
					}           
		        },
		    });
		}
		 else{
			 $('#processoverlay').hide();
			alertpopup("Don't map with same category");
		}
}
//delete counter
function categoryrecorddelete(datarowid) {
	var categoryelementstable = $('#categoryelementstable').val();
	var elementspartable = $('#categoryelementspartabname').val();
    $.ajax({
        url: base_url + "Category/deletetreeinformationdata?categoryprimarydataid="+datarowid+"&categoryelementstable="+categoryelementstable+"&categoryparenttable="+elementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	var treevalidation = $('#treevalidationcheck').val();
    			var treelabel = $('#treefiledlabelcheck').val();
    			resetFields();
    			$('#treevalidationcheck').val(treevalidation);
    			$('#treefiledlabelcheck').val(treelabel);
    			$('#parentcategoryid').val('');
    			$('#defaultcategory').val('No');
    			$('#featured').val('No');
    			$('#categoryimageattachdisplay').empty();
            	categoryrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				setTimeout(function(){categorytreereloadfun(0);},500); 
				alertpopup('Deleted successfully');
			} else if (nmsg == "Denied")  {
				categoryrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Permission denied');
            } else if(nmsg == "sndefault") {
				categoryrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Cannot Edit/Delete default records');
				$("#processoverlay").hide();
			}
        },
    });
}
//load the parent id
function gettheparentcounter(datarowid){
	$.ajax({               
		url:base_url+"Category/getcounterparentid?id="+datarowid,
		dataType:'json',
		async:false,
		cache:false,
		success:function (data)	{
			$('#parentcategoryid').val(data.parentcategoryid);
			$('#parentcategory').val(data.categoryname);
		}
	});
}
function defaulthideshow() {
	if(softwareindustryid == 3){
		if(marketplacemasterstatus == 1){
			$('#defaultcategorydivhid').addClass('hidedisplay');
			$('#featureddivhid').removeClass('hidedisplay');
		}else{
			$('#featureddivhid,#defaultcategorydivhid').addClass('hidedisplay');
		}
	} else {
		$('#featureddivhid').addClass('hidedisplay');
		$('#defaultcategorydivhid').removeClass('hidedisplay');
	}
}
//categorynamecheck
function categorynamecheck() {
	var primaryid = $("#categoryprimarydataid").val();
	var accname = $("#categoryname").val();
	var elementpartable = $('#categoryelementspartabname').val();
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Category name already exists!";
				}
			}else {
				return "Category name already exists!";
			}
		} 
	}
}
//categoryshortnamecheck
function categoryshortnamecheck() {
	var primaryid = $("#categoryprimarydataid").val();
	var accname = $("#categorysuffix").val();
	var fieldname = 'categorysuffix';
	var elementpartable = $('#categoryelementspartabname').val();
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniqueshortdynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Category short name already exists!";
				}
			}else {
				return "Category short name already exists!";
			}
		} 
	}
}
// Loose stone category check
function loosestonecheck(primaryid) {
	var resultval = 'no';
	if( primaryid !="" ) {
		$.ajax({
			url:base_url+"Category/loosestonecheck",
			data:"data=&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
				if(nmsg == 'Yes') {
					resultval = 'yes';
				} else {
					resultval = 'no';
				}
			},
		});
	}
	return resultval;
}