$(document).ready(function() {	
	{// Foundation Initialization
		$(document).foundation();
	}
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		termsandconditionviewgrid();
		crudactionenable();
	}
	$('#formclearicon').click(function(){
		froalaset(froalaarray);
		$("#formfields").empty();
	});
	{//Close Add Screen
		var addcloseoppocreation =["closeaddform","termsandconditioncreationview","termsandconditioncreationformadd"]
		addclose(addcloseoppocreation);	
		var addcloseviewcreation =["viewcloseformiconid","termsandconditioncreationview","viewcreationformdiv",""];
		addclose(addcloseviewcreation);
	}
	froalaarray=['termsandconditionstermsandconditions_editor','html.set',''];
	{// For touch
		fortabtouch = 0;
	}
	{//Toolbar Icon Change Function View
		$("#reloadicon").click(function() {
			refreshgrid();
		});
		$( window ).resize(function() {
			maingridresizeheightset('termsandconditionviewgrid');
		});
	}
	//close
	$('#closeaddoppocreation').click(function() {
		$('#dynamicdddataview').trigger('change');
	});
	{//view by drop down change
		$('#dynamicdddataview').change(function() {
			termsandconditionviewgrid();
		});
	}
	{//validation for Company Add  
		$('#dataaddsbtn').click(function() {
			$('.ftab').trigger('click');
			$("#formaddwizard").validationEngine('validate');
		});
		jQuery("#formaddwizard").validationEngine( {
			onSuccess: function() {
				newdataaddfun();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//update company information
		$('#dataupdatesubbtn').click(function() {
			$('.ftab').trigger('click');
			$("#formeditwizard").validationEngine('validate');
		});
		jQuery("#formeditwizard").validationEngine({
			onSuccess: function() {
				dataupdateinformationfun();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	//module change events
	$('#tancmoduleid').change(function() {
		var id = $(this).val();
		smsdropdownmodulevalset('formfields','formfieldsidhidden','modfiledcolumn','modfiledtable','fieldlabel','modulefieldid','columnname','tablename','modulefield','moduletabid,uitypeid',''+id+',0');
	});
	//module fields change events
	$('#formfields').change(function() {
		if( $(this).val()!= "" ){
			var table = $(this).find('option:selected').data('modfiledtable');
			var tabcolumn = $(this).find('option:selected').data('modfiledcolumn');
			var mtext = '{'+table+'.'+tabcolumn+'}';
			var oldmtext = $('#mergetext').val();
			var ftext = oldmtext +' '+ mtext;
			$('#mergetext').val(mtext);
		}
	});
	{//Function For Check box
		$('.checkboxcls').click(function() {
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
	{//filter
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,termsandconditionviewgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
	}
	$("#termsconditionstermsandconditions_editor").keydown(function() {
		$('a[href*="https://froala.com/wysiwyg-editor"]').parent().addClass('neweditor');
		$(".neweditor").css('z-index',0);
	});
	$("#termsconditionstermsandconditions_editor").mouseup(function() {
		$('a[href*="https://froala.com/wysiwyg-editor"]').parent().addClass('neweditor');
		$(".neweditor").css('z-index',0);
	});
});
//drop down values set for view
function smsdropdownmodulevalset(ddname,dataattrname,otherdataattrname1,otherdataattrname2,dataname,dataid,otherdataname1,otherdataname2,datatable,whfield,whvalue) {
	$('#'+ddname+'').empty();
	$('#'+ddname+'').append($("<option></option>"));
	$.ajax({
		url:base_url+'Termsandcondition/fetchmaildddatawithmultiplecond?dataname='+dataname+'&dataid='+dataid+'&datatab='+datatable+'&whdatafield='+whfield+'&whval='+whvalue+'&othername1='+otherdataname1+'&othername2='+otherdataname2,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#'+ddname+'')
					.append($("<option></option>")
					.attr("data-"+dataattrname,data[index]['datasid'])
					.attr("data-"+otherdataattrname1,data[index]['otherdataattrname1'])
					.attr("data-"+otherdataattrname2,data[index]['otherdataattrname2'])
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}	
			$('#'+ddname+'').trigger('change');
		},
	});
}
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','ViewCreationName','ViewCreationId','viewcreation','ViewCreationModuleId',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
{// Main Grid Function
	function termsandconditionviewgrid(page,rowcount) {
		if(Operation == 1){
			var rowcount = $('#termsconditionspgrowcount').val();
			var page = $('.paging').data('pagenum');
		} else{
			var rowcount = $('#termsconditionspgrowcount').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		}
		var wwidth = $('#termsandconditionviewgrid').width();
		var wheight = $(window).height();
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.termsconditionsheadercolsort').hasClass('datasort') ? $('.termsconditionsheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.termsconditionsheadercolsort').hasClass('datasort') ? $('.termsconditionsheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.termsconditionsheadercolsort').hasClass('datasort') ? $('.termsconditionsheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = $('#viewfieldids').val();
		var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
		var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
		var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=termsandcondition&primaryid=termsandconditionid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#termsandconditionviewgrid').empty();
				$('#termsandconditionviewgrid').append(data.content);
				$('#termsandconditionviewgridfooter').empty();
				$('#termsandconditionviewgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('termsandconditionviewgrid');
				{//sorting
					$('.termsconditionsheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.termsconditionsheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#termsandconditionspgnumcnt li .page-text .active').data('rowcount');
						var sortcol = $('.termsconditionsheadercolsort').hasClass('datasort') ? $('.termsconditionsheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.termsconditionsheadercolsort').hasClass('datasort') ? $('.termsconditionsheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						var sortpos = $('#termsandconditionviewgrid .gridcontent').scrollLeft();
						termsandconditionviewgrid(page,rowcount);
						$('#termsandconditionviewgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('termsconditionsheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						Operation = 0;
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						termsandconditionviewgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#termsconditionspgrowcount').change(function(){
						Operation = 0;
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = 1;
						termsandconditionviewgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//Material select
				$('#termsconditionspgrowcount').material_select();
			},
		});
	}
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function(e) {
			e.preventDefault();
			addslideup('termsandconditioncreationview','termsandconditioncreationformadd');
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			var froalaarray=['termsconditionstermsandconditions_editor','html.set',''];
			froalaset(froalaarray);
			$("#formfields").empty();
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			fortabtouch = 0;
			firstfieldfocus();
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
		});
		$("#editicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#termsandconditionviewgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				addslideup('termsandconditioncreationview','termsandconditioncreationformadd');
				$(".addbtnclass").addClass('hidedisplay');
				$(".updatebtnclass").removeClass('hidedisplay');
				editformdatainformationshow(datarowid);
				Materialize.updateTextFields();
				$("#formfields").empty();
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				fortabtouch = 1;
				//For Keyboard Shortcut Variables
				addformupdate = 1;
				viewgridview = 0;
				addformview = 1;
				Operation = 1; //for pagination
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#deleteicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#termsandconditionviewgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#primarydataid').val(datarowid);
				$("#basedeleteoverlay").fadeIn();
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function() {
			var datarowid = $('#primarydataid').val();
			recorddelete(datarowid);
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#termsandconditionspgnum li.active').data('pagenum');
		var rowcount = $('ul#termsandconditionspgnumcnt li .page-text .active').data('rowcount');
		termsandconditionviewgrid(page,rowcount);
	}
}
//new data add submit function
function newdataaddfun() {
	var amp = '&';
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditoradd(editorname);
	$("#termsandconditionstermsandconditions_editorfilename").val(editordata);
	var formdata = $("#dataaddform").serialize();
	var datainformation = amp + formdata;
	if(editordata != "") {
		$.ajax({
			url: base_url + "index.php/Termsandcondition/newdatacreate",
			data: "datas=" + datainformation,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					resetFields();
					$('#termsandconditioncreationformadd').hide();
					$('#termsandconditioncreationview').fadeIn(1000);
					refreshgrid();
					froalaset(froalaarray);
					alertpopup(savealert);
					//For Keyboard Shortcut Variables
					viewgridview = 1;
					addformview = 0;
				}
			},
		});
	} else {
		alertpopup('Please enter the values in template editor');
	}
}
//old information show in form
function editformdatainformationshow(datarowid) {
	$.ajax({
		url:base_url+"index.php/Termsandcondition/termsandcontdatafetch?id="+datarowid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			tandceditervalueget(data.filename);
			var moduleid = data.moduleid;
			var modpriid = data.moduleprivilegeid;
			var moduleprivilegeid = modpriid.split(',');
			var name = data.name;
			$("#tancmoduleid").select2('val',moduleid).trigger('change');
			$("#moduleprivilegeid").select2('val',moduleprivilegeid).trigger('change');
			$("#termsandconditionname").val(name);
			$("#primarydataid").val(datarowid);
			if(data.setdefault == 'Yes') {
				$('.checkboxcls').attr('checked','checked');
				$("#setdefault").val('Yes');
			} else {
				$('.checkboxcls').removeAttr('checked');
				$("#setdefault").val('No');
			}
			setTimeout(function() {
				$("#tancmoduleid").trigger('change');
			}, 500);
		},
	});
}
//editor value fetch
function tandceditervalueget(filename) {
	$.ajax({
		url: base_url+"index.php/Termsandcondition/editervaluefetch?filename="+filename,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			froalaedit(data,'termsandconditionstermsandconditions_editor');
		},
	});
}
//udate old information
function dataupdateinformationfun() {
	var amp = '&';
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditoradd(editorname);
	$("#termsandconditionstermsandconditions_editorfilename").val(editordata);
	var formdata = $("#dataaddform").serialize();
	var datainformation = amp + formdata;
	if(editordata != "") {
		$.ajax({
			url: base_url + "index.php/Termsandcondition/datainformationupdate",
			data: "datas=" + datainformation,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					resetFields();
					$('#termsandconditioncreationformadd').hide();
					$('#termsandconditioncreationview').fadeIn(1000);
					refreshgrid();
					froalaset(froalaarray);
					alertpopup(savealert);
					//For Keyboard Shortcut Variables
					addformupdate = 0;
					viewgridview = 1;
					addformview = 0;
				}
			},
		});
	} else {
		alertpopup('Please enter the values in template editor');
	}
	setTimeout(function() {
		var closetrigger = ["closeaddleadcreation"];
		triggerclose(closetrigger);
	}, 1000);
}
//udate old information
function recorddelete(datarowid) {
	var elementstable = $('#elementstable').val();
	var elementspartable = $('#elementspartabname').val();
    $.ajax({
        url: base_url + "index.php/Termsandcondition/deleteinformationdata?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	refreshgrid();
				alertpopup('Deleted successfully');
				$("#basedeleteoverlay").fadeOut();
            } else if (nmsg == "Denied") {
            	refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Permission denied');
			}
        },
    });
}