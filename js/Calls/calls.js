$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Grid Calling Function
		callsviewgrid();
	}
	{// For touch
		fortabtouch = 0;
		cntaddcount = 0;
	}
	{ // To hide addform tour guide
		$("#addformtour").hide();
	}
	//reload icon click
	$("#reloadicon").click(function(){
		refreshgrid();
	});
	//Call Settings
	$("#callsettingsicon").click(function(){
		window.location =base_url+'Callsettings';
	});
	$( window ).resize(function() {
		maingridresizeheightset('callsviewgrid');
	});
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,callsviewgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			callsviewgrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
});
//Email Segments View Grid
function callsviewgrid(page,rowcount) {
	if(Operation == 1){
		var rowcount = $('#callspgrowcount').val();
		var page = $('.paging').data('pagenum');
	} else{
		var rowcount = $('#callspgrowcount').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	}
	var wwidth = $('#callsviewgrid').width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.callsheadercolsort').hasClass('datasort') ? $('.callsheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.callsheadercolsort').hasClass('datasort') ? $('.callsheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.callsheadercolsort').hasClass('datasort') ? $('.callsheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = 18;
	var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
	var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
	var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=calldetails&primaryid=calldetailsid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#callsviewgrid').empty();
			$('#callsviewgrid').append(data.content);
			$('#callsviewgridfooter').empty();
			$('#callsviewgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('callsviewgrid');
			{//sorting
				$('.callsheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.callsheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#callspgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.callsheadercolsort').hasClass('datasort') ? $('.callsheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.callsheadercolsort').hasClass('datasort') ? $('.callsheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					var sortpos = $('#callsviewgrid .gridcontent').scrollLeft();
					callsviewgrid(page,rowcount);
					$('#callsviewgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('callsheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					Operation = 0;
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					callsviewgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#callspgrowcount').change(function(){
					Operation = 0;
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = 1;
					callsviewgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			//onclick 
			$(".gridcontent").click(function(){
				var datarowid = $('#callsviewgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#callspgrowcount').material_select();
		},
	});	
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#callspgnum li.active').data('pagenum');
		var rowcount = $('ul#callspgnumcnt li .page-text .active').data('rowcount');
		callsviewgrid(page,rowcount);
	}
}
//dial street data fetch
function calldetailsfetchfromdialstreet() {
	$.ajax({
		url:base_url+"Calls/callsdetailsfetch",
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			refreshgrid();
		},
	});
}