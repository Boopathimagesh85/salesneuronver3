$(document).ready(function(){	
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		purityaddgrid();
		puritycrudactionenable();
	}	
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			purityaddgrid();
		});
	}
	PURITYDELETE = 0;
	setTimeout(function() {
		$(".ajax-file-upload>form>input").removeAttr("disabled");//remove disabled attribute for uploadFile
		}, 1000);
	//hidedisplay
	$('#puritydataupdatesubbtn,#groupcloseaddform').hide();
	$('#puritysavebutton').show();
	{
		 $("#purityreloadicon").click(function(){
			clearform('cleardataform');
			purityrefreshgrid();
			$("#puritydataupdatesubbtn").hide();
			$("#puritysavebutton").show();
	});
		$( window ).resize(function() {
			maingridresizeheightset('purityaddgrid');
			sectionpanelheight('groupsectionoverlay');
		});
		//validation for  Add  
		$('#puritysavebutton').click(function(e) {
			if(e.which == 1 || e.which === undefined) {
				masterfortouch = 0;
				$("#purityformaddwizard").validationEngine('validate');
			}
		});
		jQuery("#purityformaddwizard").validationEngine({
			onSuccess: function() {
				purityaddformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//update purity information
		$('#puritydataupdatesubbtn').click(function(e){
			if(e.which==1 || e.which === undefined) {
				$("#purityformeditwizard").validationEngine('validate');
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			}
		});
		jQuery("#purityformeditwizard").validationEngine({
			onSuccess: function() {
				purityupdateformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	//for toggle
	$('#purityviewtoggle').click(function() {
		if ($(".purityfilterslide").is(":visible")) {
			$('div.purityfilterslide').addClass("filterview-moveleft");
		} else {
			$('div.purityfilterslide').removeClass("filterview-moveleft");
		}
	});
	$('#purityclosefiltertoggle').click(function() {
		$('div.purityfilterslide').removeClass("filterview-moveleft");
	});
	//filter
	$("#purityfilterddcondvaluedivhid").hide();
	$("#purityfilterdatecondvaluedivhid").hide();
	{//field value set
		$("#purityfiltercondvalue").focusout(function() {
			var value = $("#purityfiltercondvalue").val();
			$("#purityfinalfiltercondvalue").val(value);
			$("#purityfilterfinalviewconid").val(value);
		});
		$("#purityfilterddcondvalue").change(function() {
			var value = $("#purityfilterddcondvalue").val();
			puritymainfiltervalue=[];
			$('#purityfilterddcondvalue option:selected').each(function() {
			    var $pvalue =$(this).attr('data-ddid');
			    puritymainfiltervalue.push($pvalue);
				$("#purityfinalfiltercondvalue").val(puritymainfiltervalue);
			});
			$("#purityfilterfinalviewconid").val(value);
		});
	}
	purityfiltername = [];
	$("#purityfilteraddcondsubbtn").click(function() {
		$("#purityfilterformconditionvalidation").validationEngine("validate");	
	});
	$("#purityfilterformconditionvalidation").validationEngine({
		onSuccess: function() {
			dashboardmodulefilter(purityaddgrid,'purity');
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	$("#tab2").click(function()	{
		loadmetaldropdown('metalid');
	});
	//Kumaresan - Default round value set to the field
	$("#melting").change(function() {
		var meltvalue = $('#meltinground').val();
		if($.trim($(this).val()) == ''){
			$(this).val();
		}else{
			var meltingval = parseFloat($("#melting").val()).toFixed(meltvalue);
			$('#melting').val(meltingval);
		}
	});
	$("#melting").keypress(function(e){
		 var keyCode = e.which;
		 var start = e.target.selectionStart;
		 var end = e.target.selectionEnd;
		 var meltvalue = $('#meltinground').val();
		 if($(this).val().substring(start, end) == '') {
			 // Not allow special 
			  if ( ((keyCode >= 48 && keyCode <= 57) || keyCode == 8 || keyCode == 46 || keyCode == 0)) {
				  var newValue = this.value;
					if(keyCode != 8 && keyCode != 0){
						if (hasDecimalPlace(newValue, meltvalue)) {
							e.preventDefault();
						}
					}		
			 }else{
				 e.preventDefault();
			 }
		 }else{
			if (((keyCode >= 48 && keyCode <= 57) || keyCode == 8 || keyCode == 46 || keyCode == 0)) {
			}else{
				e.preventDefault();
			}
		 }
	});
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}

//Documents Add Grid
function purityaddgrid(page,rowcount) {
	var defrecview = $('#mainviewdefaultview').val();
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
	var wwidth = $('#purityaddgrid').width();
	var wheight = $(window).height();
	var viewname = $('#dynamicdddataview').val();
	$('#viewgridheaderid').text(viewname);
	//col sort
	var sortcol = $("#puritysortcolumn").val();
	var sortord = $("#puritysortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.purityheadercolsort').hasClass('datasort') ? $('.purityheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.purityheadercolsort').hasClass('datasort') ? $('.purityheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.purityheadercolsort').hasClass('datasort') ? $('.purityheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#purityviewfieldids').val();
	userviewid = userviewid == '' ? '117' : userviewid;
	var filterid = $("#purityfilterid").val();
	var conditionname = $("#purityconditionname").val();
	var filtervalue = $("#purityfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=purity&primaryid=purityid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#purityaddgrid').empty();
			$('#purityaddgrid').append(data.content);
			$('#purityaddgridfooter').empty();
			$('#purityaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('purityaddgrid');
			{//sorting
				$('.purityheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.purityheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.purityheadercolsort').hasClass('datasort') ? $('.purityheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.purityheadercolsort').hasClass('datasort') ? $('.purityheadercolsort.datasort').data('sortorder') : '';
					$("#puritysortorder").val(sortord);
					$("#puritysortcolumn").val(sortcol);
					purityaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				sortordertypereset('purityheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					purityaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});	
				$('#puritypgrowcount').change(function(){ //unitofmeasureaddgridfooter
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					purityaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#purityaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#puritypgrowcount').material_select();
		},
	});	
}
function puritycrudactionenable() {
	{//add icon click
		$('#addicon').click(function(e){
			e.preventDefault();
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			$('#puritydataupdatesubbtn').hide();
			$('#puritysavebutton').show();
			meltingroundset();
			firstfieldfocus();
			$("#purityprimarydataid").val('');
			$('#metalid').prop('disabled',false);
		});
	}
	$("#editicon").click(function(e){
		e.preventDefault();
		var datarowid = $('#purityaddgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			resetFields();
			$('#puritydataupdatesubbtn').show();
			$('#puritysavebutton').hide();
			puritygetformdata(datarowid);
			puritymapcheckproduct(datarowid);
			meltingroundset();
			firstfieldfocus();
			masterfortouch = 1;
			Materialize.updateTextFields();
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#deleteicon").click(function(){
		var datarowid = $('#purityaddgrid div.gridcontent div.active').attr('id');		
		if(datarowid){
			clearform('cleardataform');
			$("#purityprimarydataid").val(datarowid);
			$(".addbtnclass").removeClass('hidedisplay');
			$(".updatebtnclass").addClass('hidedisplay');
			$.ajax({ // checking whether this value is already in usage.
				url: base_url + "Base/multilevelmapping?level="+1+"&datarowid="+datarowid+"&table=product&fieldid=purityid&table1=rate&fieldid1=purityid&table2=''&fieldid2=''",
				success: function(msg) { 
				if(msg > 0){ 
					alertpopup("Kindly delete the product linked to purity");						
				}else{
					combainedmoduledeletealert('puritydeleteyes');
					if(PURITYDELETE == 0) {
						$("#puritydeleteyes").click(function(){
							var datarowid =$("#purityprimarydataid").val();
							purityrecorddelete(datarowid);
						});
					}
					PURITYDELETE = 1;
				}
				},
				});
			} else {
			alertpopup("Please select a row");
		}
	});
	{//Addform section overlay close
		$(".addsectionclose").click(function(){
			resetFields();
			$("#groupsectionoverlay").removeClass("effectbox");
			$("#groupsectionoverlay").addClass("closed");
		});
	}
}
{//refresh grid
	function purityrefreshgrid() {
		var page = $('ul#puritypgnum li.active').data('pagenum');
		var rowcount = $('ul#puritypgnumcnt li .page-text .active').data('rowcount');
		purityaddgrid(page,rowcount);
	}
}
//new data add submit function
function purityaddformdata() {
    var formdata = $("#purityaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax( {
        url: base_url + "Purity/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				resetFields();
				purityrefreshgrid();
				$("#groupsectionoverlay").removeClass("effectbox");
				$("#groupsectionoverlay").addClass("closed");
				alertpopup(savealert);			
            }
        },
    });
}
//old information show in form
function puritygetformdata(datarowid) {
	var purityelementsname = $('#purityelementsname').val();
	var purityelementstable = $('#purityelementstable').val();
	var purityelementscolmn = $('#purityelementscolmn').val();
	var purityelementpartable = $('#purityelementspartabname').val();
	$.ajax( {
		url:base_url+"Purity/fetchformdataeditdetails?purityprimarydataid="+datarowid+"&purityelementsname="+purityelementsname+"&purityelementstable="+purityelementstable+"&purityelementscolmn="+purityelementscolmn+"&purityelementpartable="+purityelementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				purityrefreshgrid();
			} else if((data.fail) == 'sndefault'){
				$("#groupsectionoverlay").addClass("closed");
				alertpopup('Cannot Edit/Delete default records');
			} else {
				var txtboxname = purityelementsname + ',purityprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = purityelementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
			}
			
		}
	});
	$('#puritydataupdatesubbtn').show().removeClass('hidedisplayfwg');
	$('#puritysavebutton').hide();
}
//update old information
function purityupdateformdata() {
	var formdata = $("#purityaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Purity/datainformationupdate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				$('#puritydataupdatesubbtn').hide();
				$('#puritysavebutton').show();
				resetFields();
				$("#groupsectionoverlay").removeClass("effectbox");
				$("#groupsectionoverlay").addClass("closed");
				purityrefreshgrid();
				$('#purityimageattachdisplay').empty();
				alertpopup(updatealert);
				//for touch
				masterfortouch = 0;				
            }
        },
    });
	setTimeout(function() {
		var closetrigger = ["puritycloseadd"];
		triggerclose(closetrigger);
	}, 1000);
}
//update old information.
function purityrecorddelete(datarowid) {
	var purityelementstable = $('#purityelementstable').val();
	var elementspartable = $('#purityelementspartabname').val();
    $.ajax({
        url: base_url + "Purity/deleteinformationdata?purityprimarydataid="+datarowid+"&purityelementstable="+purityelementstable+"&purityparenttable="+elementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	purityrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Deleted successfully');
            } else if (nmsg == "Denied")  {
            	purityrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Permission denied');
            } else if( nmsg == "sndefault") {
				purityrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Cannot Edit/Delete default records');
			}
        },
    });
}
//puritynamecheck
function puritynamecheck() {
	var primaryid = $("#purityprimarydataid").val();
	var accname = $("#purityname").val();
	var elementpartable = $('#purityelementspartabname').val();
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Purity name already exists!";
				}
			}else {
				return "Purity name already exists!";
			}
		} 
	}
}
//purityshortnamecheck
function purityshortnamecheck() {
	var primaryid = $("#purityprimarydataid").val();
	var accname = $("#shortname").val();
	var fieldname = 'shortname';
	var elementpartable = $('#purityelementspartabname').val();
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniqueshortdynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Purity short name already exists!";
				}
			}else {
				return "Purity short name already exists!";
			}
		} 
	}
}
//*load metal dropdown*//
function loadmetaldropdown(id){
	$('#'+id+'').empty();
	$.ajax({
		url:base_url+'Purity/loadmetal',
		dataType:'json',
		async:false,
		success: function(data) {
		  if((data.fail) == 'FAILED') {
		  } else {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++) {
					newddappend += "<option value ='" +data[m]['metalid']+ "'>"+data[m]['metalname']+"</option>";
				}						
				$('#'+id+'').append(newddappend);						
		  }    
		  $('#'+id+'').select2('val','').trigger('change');
		},
	});
}
function meltingroundset(){
	var meltvalue = $('#meltinground').val();
	$('#melting').removeClass('validate[required,maxSize[100]] forsucesscls');
	$('#melting').addClass('validate[required,min[1],max[100],custom[number],decval['+meltvalue+']] forsucesscls');
}
// check purity map in product
function puritymapcheckproduct(datarowid) {
	$.ajax({
		url:base_url+'Purity/puritymapcheckproduct',
		data: "purityid=" + datarowid,
		dataType:'json',
		type:"post",
		async:false,
		success: function(data) {
		  if(data == '1') {
			  $('#metalid').prop('disabled',true);
		  } else {
			  $('#metalid').prop('disabled',false);
		  }    
		},
	});
}