$(document).ready(function() 
{ 
	{// Foundation Initialization
        $(document).foundation();
    }
	TREETYPE= 'ADD';
	treestatus = 0;
	$('#counterid').removeClass('validate[required]');
	$('#counteriddivhid').hide();
	{// Maindiv height width change
        maindivwidth();
    }   
    {// Grid Calling
	    stonegrid();
    }
	rate_round = $('#rate_round').val();
    stoneeditstatus = 0;
   // category tree
{
	$('#dl-promenucategory').dlmenu({
	});
}
{// View by drop down change
    $('#dynamicdddataview').change(function(){
    	stonegrid();
    });
}
//Reload
$("#reloadicon").click(function(){
	refreshgrid();
});
// stone clearGridData
$('#formclearicon').click(function(){
	resetFields();
});
	//Kumaresan - Purchase rate and Sales rate round value
	{
		
		$('#purchaserate').change(function() {
			$('#purchaserate').removeClass('validate[required,custom[number],decval[5],maxSize[15]] forsucesscls');
			$('#purchaserate').addClass('validate[required,custom[number],decval['+rate_round+'],maxSize[15],min[0]');
			$('#stonerate').addClass('validate[required,custom[number],decval['+rate_round+'],maxSize[15],min[0]]');
			$("#stonerate").val(0);
		});
		$('#stonerate').change(function() {
			$('#stonerate').removeClass('validate[required,custom[number],decval[5],maxSize[15]] forsucesscls');
			$('#stonerate').addClass('validate[required,custom[number],decval['+rate_round+'],maxSize[15],min[0]]');
			if(parseFloat($('#purchaserate').val()) <= parseFloat($('#stonerate').val())) {
				return true;
			}else{ 
					$("#stonerate").val('');
					alertpopup('Sales rate should be greater than/equal to Purchase rate');
					return false; 
			}
		});
	}
// display category based on stone type change
$('#stonetype').change(function(){
	var stonetype=$('#stonetype').val();
	var stonemode = $('#stonetype').find('option:selected').val();
	if(stonetype==2)
	{
		$('#stoneshape,#stonecolor,#stoneclarity,#stonecut,#stonecertification,#stonepolish,#stonesymmetry,#stonefluorescence,#stonetablefrom,#stonetableto,#stonedepthfrom,#stonedepthto').prop('disabled', false);
	}
	else if(stonetype==3 || stonetype == 4)
	{
		$('#stoneshape,#stonecolor,#stoneclarity,#stonecut,#stonecertification,#stonepolish,#stonesymmetry,#stonefluorescence,#stonetablefrom,#stonetableto,#stonedepthfrom,#stonedepthto').prop('disabled', true);
		$('#stoneshape,#stonecolor,#stoneclarity,#stonecut,#stonecertification,#stonepolish,#stonesymmetry,#stonefluorescence,#stonetablefrom,#stonetableto,#stonedepthfrom,#stonedepthto').select2('val','');
	}
	$('#parentstonecategoryid').val('');
	 stonecategorytreereloadfun(stonetype,stonemode,TREETYPE);
});	
//stone close icon
var addcloseinfo =["closeaddform","stonecreationview","stonecreationformadd",""];
addclose(addcloseinfo);
$('#closeaddform').click(function(){
	$('#stonetype').prop('disabled', false);
});
//Edit
$("#editicon").click(function(){
	var datarowid = $('#stonegrid div.gridcontent div.active').attr('id');
	if(datarowid){
		stoneeditstatus=1;
		TREETYPE= 'EDIT';
		$("#processoverlay").show();
		$('.updatebtnclass').show();
		$('.addbtnclass,.editformsummmarybtnclass').hide();
		$("#formclearicon").hide();
		$('.ftab').trigger('click');
		retrive(datarowid);
		Materialize.updateTextFields();
		firstfieldfocus();
		//treemenu css fix
		//$('.dl-menuwrapper input').prop("disabled", false);
		//For Keyboard Shortcut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		Operation = 1; //for pagination
	} else {
		alertpopup("Please select a row");
	}
});
// parent category select
$('#stonemasterlistuldata li').click(function() { 
			var name=$(this).attr('data-listname');
			var listid=$(this).attr('data-listid'); 
			$('#parentstonecategory').val(name);
			$('#parentstonecategoryid').val(listid);
});
{ // crud operations
//stone add icon
$('#addicon').click(function(){
	addslideup('stonecreationview','stonecreationformadd');
	showhideiconsfun('addclick','stonecreationformadd');
	//$('#parentstonecategory').attr('disabled', true);
	//setTimeout(function(){
		$('.updatebtnclass,.editformsummmarybtnclass').addClass('hidedisplay');
		$('.addbtnclass').removeClass('hidedisplay');
		$('.addbtnclass').show();
		$('.updatebtnclass,.editformsummmarybtnclass').hide();
	//},25);
	TREETYPE= 'ADD';
	resetFields();
	$('#primarydataid').val('');
	$('#stonetype').select2('val',1);
	$('#stonetype').trigger('change');
	$('#stonemodeid').prop('disabled',false);
	firstfieldfocus();
	$('.ftab').trigger('click');
	//treemenu css fix
	//setTimeout(function(){
	$('#treebutton').prop("disabled", false);
	//},1000);
	//For Keyboard Shortcut Variables
	viewgridview = 0;
	addformview = 1;
	Operation = 0; //for pagination
	$('#counterid').removeClass('validate[required]');
	$('#counteriddivhid').hide();
	$("#stonegstapplicablecboxid").prop('checked',true);
	$("#gstcalculationtypeiddivhid,#supplytypeiddivhid,#stonegsthsncodedivhid,#stonegsthsndescriptiondivhid,#taxmasteriddivhid").show();
});
$("#dataaddsbtn").click(function(){
	$('#parentstonecategory').attr('disabled', false);
	$("#formaddwizard").validationEngine('validate');
});
$("#dataupdatesubbtn").click(function(){
	$('#parentstonecategory').attr('disabled', false);
	$("#formeditwizard").validationEngine('validate');
});
}
//Delete stone
$("#deleteicon").click(function(){
	var datarowid = $('#stonegrid div.gridcontent div.active').attr('id');
	if(datarowid){
		$('#primarydataid').val(datarowid);
		$.ajax({
			url: base_url + "Base/multilevelmapping?level="+3+"&datarowid="+datarowid+"&table=stoneentry&fieldid=stoneid&table1=salesstoneentry&fieldid1=stoneid&table2=stonechargedetail&fieldid2=stoneid",
			success: function(msg) {
				if(msg > 0){
					alertpopup("This stone is already in usage. You cannot delete this stone");						
				}else{
					var getresult = checkstoneentry(datarowid,1,'delete');
					if(getresult == 'FAIL') {
						alertpopup("This stone is already in usage. You cannot delete this stone");
					} else {
						$("#basedeleteoverlay").fadeIn();
						$("#basedeleteyes").focus();
					}
				}
			},
		});
	} else {
		alertpopup("Please select a row");
	}	
});
//stone yes delete
$("#basedeleteyes").click(function() { 
	var datarowid = $('#stonegrid div.gridcontent div.active').attr('id');
	var elementstable = $('#elementstable').val();
	var elementspartable = $('#elementspartabname').val();
	$.ajax({
		url: base_url + "Stone/stonedelete?primaryid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable,
		success: function(msg) {				
			var nmsg =  $.trim(msg);
			refreshgrid();
			$("#basedeleteoverlay").fadeOut();
			if (nmsg == "TRUE") {
				alertpopup('Deleted successfully');
			} else  {					
			}				
		},
	});
});
//Detailed view
$("#detailedviewicon").click(function() {
	resetFields();			
	var datarowid = $('#stonegrid div.gridcontent div.active').attr('id');
	if (datarowid) {
		TREETYPE= 'Detail';
		$("#formclearicon").hide();
		$('.editformsummmarybtnclass').show();
		$('.updatebtnclass,.addbtnclass').hide();
		$('.updatebtnclass,.addbtnclass').addClass('hidedisplay');
		$('.editformsummmarybtnclass').removeClass('hidedisplay');
		//Function Call For Edit
		retrive(datarowid);
		showhideiconsfun('summryclick','stonecreationformadd');
		//treemenu css fix
		$('#parentstonecategory').prop("disabled", true);
		//setTimeout(function(){
		$('.dl-menuwrapper button').prop("disabled", "disabled");
		//},100);
		$('.ftab').trigger('click');
		Materialize.updateTextFields();
	} else {
		alertpopup("Please select a row");
	}		
});
//Edit Detailed
$("#editfromsummayicon").click(function(){
	$('.editformsummmarybtnclass').hide();
	$('.updatebtnclass').show();
	showhideiconsfun('editfromsummryclick','stonecreationformadd');
	var stonetype=$('#stonetype').val();
	//treemenu css fix
	if(stonetype==2)
	{
		$('#stonetype,#stoneshape,#stonecolor,#stoneclarity,#stonecut,#stonecertification,#stonepolish,#stonesymmetry,#stonefluorescence,#stonetablefrom,#stonetableto,#stonedepthfrom,#stonedepthto').attr('disabled', false);
	}
	else if(stonetype==3 || stonetype == 4)
	{
		
		$('#stonetype,#stoneshape,#stonecolor,#stoneclarity,#stonecut,#stonecertification,#stonepolish,#stonesymmetry,#stonefluorescence,#stonetablefrom,#stonetableto,#stonedepthfrom,#stonedepthto').attr('disabled', true);
	}
	setTimeout(function(){
		$('#stonetype').prop('disabled',true);
		$('#parentstonecategory').prop("disabled", true);
		//$('#parentstonecategory').attr("readonly", true);
	},10);
	checktreestatus();
});
{ // validation add/edit
	jQuery("#formaddwizard").validationEngine(
			{
				onSuccess: function(){
					addform();
				},
				onFailure: function() {
					var dropdownid =['1','stonesize'];
					dropdownfailureerror(dropdownid);
					alertpopup(validationalert);
					$('#parentstonecategory').attr('disabled', true);
										
				}
			});
	//edit	
	jQuery("#formeditwizard").validationEngine(
	{
		onSuccess: function()
		{
			$('#stonename').attr('data-primaryid','');
			updateform();
		},
		onFailure: function()
		{		
			var dropdownid =['1','stonesize'];
			dropdownfailureerror(dropdownid);	
			alertpopup(validationalert);
			$('#parentstonecategory').attr('disabled', true);
		}
	});
}
$('#stonetablefrom').change(function(){
	$("#stonetableto").val(0);
});
$('#stonedepthfrom').change(function(){
	$("#stonedepthto").val(0);
});
$('#stonemodeid').change(function(){
	counterhideshow();
	var stonetype = $('#stonetype').find('option:selected').val();
	var stonemode = $('#stonemodeid').find('option:selected').val();
	stonecategorytreereloadfun(stonetype,stonemode,TREETYPE);
	if(stonemode == 2) {
		$('#taxmasterid').select2('val',3).trigger('change');
		$('#stonegsthsncode').val('7102');
	} else {
		$('#taxmasterid').select2('val',2).trigger('change');
		$('#stonegsthsncode').val('');
	}
});
{ // Filter Work by kumaresan
	$('#viewtoggle').click(function(){
		if(filterstatus == 0) {
			$("#processoverlay").show();
			$("#viewtoggle").show();
			var moduleid = $("#viewfieldids").val();
			getfilterdatas(moduleid,stonegrid);
		}
		$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
		$('#filterdisplaymainview').fadeIn();
		filterstatus = 1;
	});
	$(document).on("click",".filterdisplaymainviewclearclose",function() {
		$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
		$('#filterconddisplay').children().remove();
		$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
		$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
		stonegrid();
	});
	$(document).on( "click", ".filterdisplaymainviewclose", function() {
		$('#filterdisplaymainview').fadeOut();
	});
}
//stone addon
$("#stoneaddonicon").click(function(){
	window.location =base_url+'Stoneaddons';
});
// stone category
$("#stonecategoryicon").click(function(){
	window.location =base_url+'Stonecategory';
});
//gst field hide
$("#gstcalculationtypeiddivhid,#supplytypeiddivhid,#stonegsthsncodedivhid,#stonegsthsndescriptiondivhid,#taxmasteriddivhid").hide();
{//Function For Check box
	$('.checkboxcls').click(function() {
		var name = $(this).data('hidname');
		if ($(this).is(':checked')) {
			$('#'+name+'').val('Yes');
		} else {
			$('#'+name+'').val('No');
		}
		if(name == 'stonegstapplicable') {
			gstdatahideshow($('#'+name+'').val());
		}				
	});
}
});
//Create
function addform()
{
	var stonetablefrom=$('#stonetablefrom').val();
	var stonetableto=$('#stonetableto').val();
	if(stonetablefrom != ''){
		if(stonetableto == ''){
			alertpopup('Please enter table to value');
			return false;
		}
	}
	if(stonetableto != ''){
		if(stonetablefrom == ''){
			alertpopup('Please enter table from value');
			return false;
		}
	}
	var stonedepthfrom=$('#stonedepthfrom').val();
	var stonedepthto=$('#stonedepthto').val();
	if(stonedepthfrom != ''){
		if(stonedepthto == ''){
			alertpopup('Please enter depth to value');
			return false;
		}
	}
	if(stonedepthto != ''){
		if(stonedepthfrom == ''){
			alertpopup('Please enter depth from value');
			return false;
		}
	}
	$('#processoverlay').show();
	var formdata = $("#dataaddform").serialize(); 
	var amp = '&';
	var datainformation = amp + formdata;
	var loosecounter = $('#counterid').val();  
	
	$.ajax(
    {
        url: base_url +"Stone/stonecreate",
        data: "datas=" +datainformation+amp+"loosecounter="+loosecounter,
		type: "POST",
		async:false,
		cache:false,
        success: function(msg) 
        {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$('#parentstonecategory').attr('disabled', true);
				$(".ftab").trigger('click');
	            resetFields();
				$('#stonecreationformadd').hide();
				$('#stonecreationview').fadeIn(1000);
				alertpopup(savealert);
				refreshgrid();
				//For Keyboard Shortcut Variables
				viewgridview = 1;
				addformview = 0;
				$('#processoverlay').hide();
				
			}
        },
    });
}
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $("#viewcreatemoduleid").val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','ViewCreationName','ViewCreationId','viewcreation','ViewCreationModuleId',viewmoduleids);
	if(viewname != "dontset")
	{
		setTimeout(function()
		{
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		$('#viewcreateconditiongrid').jqGrid("clearGridData", true);
	}
	else
	{
		$('#dynamicdddataview').trigger('change');
	}
}
function stonegrid(page,rowcount) {
	var defrecview = $('#mainviewdefaultview').val();
	if(Operation == 1){
		var rowcount = $('#stonemasterpgrowcount').val();
		var page = $('.paging').data('pagenum');
	} else{
		var rowcount = $('#stonemasterpgrowcount').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
	}
	var wwidth = $('#stonegrid').width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.stonemasterheadercolsort').hasClass('datasort') ? $('.stonemasterheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.stonemasterheadercolsort').hasClass('datasort') ? $('.stonemasterheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.stonemasterheadercolsort').hasClass('datasort') ? $('.stonemasterheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#viewfieldids').val();
	var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
	var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
	var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=stone&primaryid=stoneid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#stonegrid').empty();
			$('#stonegrid').append(data.content);
			$('#stonegridfooter').empty();
			$('#stonegridfooter').append(data.footer);
			//getaddinfo();
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('stonegrid');
			{//sorting
				$('.stonemasterheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.stonemasterheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.stonemasterheadercolsort').hasClass('datasort') ? $('.stonemasterheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.stonemasterheadercolsort').hasClass('datasort') ? $('.stonemasterheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					var sortpos = $('#stonegrid .gridcontent').scrollLeft();
					stonegrid(page,rowcount);
					$('#stonegrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('stonemasterheadercolsort',headcolid,sortord);
			}
			//onclick 
			$(".gridcontent").click(function(){
				var datarowid = $('#stonegrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			{//pagination
					$('.pvpagnumclass').click(function(){
						Operation = 0;
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						stonegrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#stonemasterpgrowcount').change(function(){
						Operation = 0;
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = 1;
						stonegrid(page,rowcount);
						$("#processoverlay").hide();
					});
			}
			//Material select
			$('#stonemasterpgrowcount').material_select();
		},
	});
}
function retrive(datarowid){
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementpartable = $('#elementspartabname').val();
	var resctable = $('#resctable').val();
	$(".addbtnclass,.editformsummmarybtnclass").addClass('hidedisplay');
	$(".updatebtnclass").removeClass('hidedisplay');
	addslideup('stonecreationview','stonecreationformadd');
	resetFields();
	//setTimeout(function(){
		$("#stonename").focus();
	//},100);
	$.ajax({
		url:base_url+"Stone/stoneretrieve?primaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable+"&resctable="+resctable,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			var txtboxname = elementsname+ ',primarydataid';
			var textboxname = {};
			textboxname = txtboxname.split(',');
			var removeItem = 'parentstonecategoryid';
			textboxname = jQuery.grep(textboxname, function(value) {
					return value != removeItem;
			});
			var dropdowns = [];
			textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
			$('#stonename').attr('data-primaryid',datarowid);
			$("#processoverlay").hide();
			$('#stonetype').select2('val',data.stonetype);
			if(data.stonetype==2)
			{
				$('#stoneshape,#stonecolor,#stoneclarity,#stonecut,#stonecertification,#stonepolish,#stonesymmetry,#stonefluorescence,#stonetablefrom,#stonetableto,#stonedepthfrom,#stonedepthto').prop('disabled', false);
			}
			else if(data.stonetype==3 || data.stonetype == 4)
			{
				$('#stoneshape,#stonecolor,#stoneclarity,#stonecut,#stonecertification,#stonepolish,#stonesymmetry,#stonefluorescence,#stonetablefrom,#stonetableto,#stonedepthfrom,#stonedepthto').prop('disabled', true);
				$('#stoneshape,#stonecolor,#stoneclarity,#stonecut,#stonecertification,#stonepolish,#stonesymmetry,#stonefluorescence,#stonetablefrom,#stonetableto,#stonedepthfrom,#stonedepthto').select2('val','');
			}
			$('#stonetype').prop('disabled',true);
			$('#purchaserate').val(parseFloat(data.purchaserate).toFixed(rate_round));
			$('#stonerate').val(parseFloat(data.stonerate).toFixed(rate_round));
			$('#stonemodeid').prop('disabled',true);
			if(data.stonegstapplicable == 'Yes'){
				$("#gstcalculationtypeiddivhid,#supplytypeiddivhid,#stonegsthsncodedivhid,#stonegsthsndescriptiondivhid,#taxmasteriddivhid").show();
			}else{
				$("#gstcalculationtypeiddivhid,#supplytypeiddivhid,#stonegsthsncodedivhid,#stonegsthsndescriptiondivhid,#taxmasteriddivhid").hide();
			}
			stonecategorytreereloadfun(data.stonetype,data.stonemodeid,TREETYPE);
			setTimeout(function() {
				stonecategorynamefetch(datarowid);
				checkstoneentry(datarowid,data.stonemodeid,'edit');
			},1000);
			counterhideshow();
		}
	});
}
function updateform()
{
	var stonetablefrom=$('#stonetablefrom').val();
	var stonetableto=$('#stonetableto').val();
	if(stonetablefrom != ''){
		if(stonetableto == ''){
			alertpopup('Please enter table to value');
			return false;
		}
	}
	if(stonetableto != ''){
		if(stonetablefrom == ''){
			alertpopup('Please enter table from value');
			return false;
		}
	}
	var stonedepthfrom=$('#stonedepthfrom').val();
	var stonedepthto=$('#stonedepthto').val();
	if(stonedepthfrom != ''){
		if(stonedepthto == ''){
			alertpopup('Please enter depth to value');
			return false;
		}
	}
	if(stonedepthto != ''){
		if(stonedepthfrom == ''){
			alertpopup('Please enter depth from value');
			return false;
		}
	}
	var stonemodeid = $('#stonemodeid').val();
	$('#processoverlay').show();
	var formdata = $("#dataaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	var datarowid = $('#stonegrid div.gridcontent div.active').attr('id');
	var loosecounter = $('#counterid').val();
	$.ajax({
        url: base_url + "Stone/stoneupdate",
        data: "datas=" + datainformation +"&primaryid="+ datarowid+"&loosecounter="+ loosecounter+"&stonemodeid="+ stonemodeid,
		type: "POST",
		async:false,
		cache:false,
        success: function(msg) 
        {
			var nmsg =  $.trim(msg);
			Materialize.updateTextFields();
			addslideup('stonecreationformadd','stonecreationview');
			$('#parentstonecategory').attr('disabled', true);
			$('#stonetype').prop('disabled',false);
            if (nmsg == 'TRUE'){
            	refreshgrid();
				$('#processoverlay').hide();
				alertpopup(savealert);
				
            } else {
				$('#processoverlay').hide();
			}
          //For Keyboard Shortcut Variables
			addformupdate = 0;
			viewgridview = 1;
			addformview = 0;
        },
    });
}
//stone type based category  treeload
function stonecategorytreereloadfun(stonetype,stonemode,type)
{
	var tablename = "stonecategory";
	var mandfield = $('#treevalidationcheck').val();
	var fieldlab = $('#treefiledlabelcheck').val();
	stonemode = typeof stonemode == 'undefined' ? 1 : stonemode;
	$.ajax({
		url: base_url + "Stone/treedatafetchfun",
		data: "tabname="+tablename+'&mandval='+mandfield+'&fieldlabl='+fieldlab+'&stonetype='+stonetype+"&type="+type+'&stonemode='+stonemode,
		type: "POST",
		cache:false,
		success: function(data) {
			$('.'+tablename+'treediv').empty();
			$('.'+tablename+'treediv').append(data);
			$('#stonemasterlistuldata li').click(function() {
				var name=$(this).attr('data-listname');
				var listid=$(this).attr('data-listid');
				$('#parentstonecategory').val(name);
				$('#parentstonecategoryid,#proparentcategoryid').val(listid);
			});
			$(function() {
				$('#dl-menu').dlmenu({
					animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
				});
			});
		},
	});
}	
	{//refresh grid
		function refreshgrid() {
			var page = $('ul#stonemasterpgnum li.active').data('pagenum');
			var rowcount = $('ul#stonemasterpgnumcnt li .page-text .active').data('rowcount');
			stonegrid(page,rowcount);
		}
	}
	{// stone category name value fetch
		function stonecategorynamefetch(pid) {
			$.ajax({
				url:base_url+"Stone/stonecategorynamefetch?id="+pid,
				dataType:'json',
				async:false,
				cache:false,
				success:function (data)	{
					$('#parentstonecategoryid').val(data.parentcategoryid);
					$('#parentstonecategory').val(data.categoryname);
				}
			});
		}
	}
	//stone unique name check
	function stonenamecheck() {
		var primaryid = $("#primarydataid").val();
		var accname = $("#stonename").val();
		var elementpartable = $('#elementspartabname').val();
		if( accname !="" ) {
			$.ajax({
				url:base_url+"Base/uniquedynamicviewnamecheck",
				data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
				type:"post",
				async:false,
				cache:false,
				success :function(msg) {
					nmsg = $.trim(msg);
				},
			});
			if(nmsg != "False") {
				if(primaryid != ""){
					if(primaryid != nmsg) {
						return "Stone name already exists!";
					}
				} else {
					return "Stone name already exists!";
				}
			} 
		}
	}
	function stoneshortnamecheck() {
			var primaryid = $("#primarydataid").val();
			var accname = $("#shortname").val();
			var fieldname = 'shortname';
			var elementpartable = $('#elementspartabname').val();
			if( accname !="" ) {
				$.ajax({
					url:base_url+"Base/uniqueshortdynamicviewnamecheck",
					data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
					type:"post",
					async:false,
					cache:false,
					success :function(msg) {
						nmsg = $.trim(msg);
					},
				});
				if(nmsg != "False") {
					if(primaryid != ""){
						if(primaryid != nmsg) {
							return "Short name already exists!";
						}
					}else {
						return "Short name already exists!";
					}
				}
			}
   }
function gstdatahideshow(hidedata) {
	if(hidedata == 'Yes'){
		var gstselect = 2;
		var stonemode = $('#stonemodeid').find('option:selected').val();
		$('#stonegsthsncode').val('');
		if(stonemode == 2) {
			gstselect = 3;
			$('#stonegsthsncode').val('7102');
		}
		$("#gstcalculationtypeiddivhid,#supplytypeiddivhid,#stonegsthsncodedivhid,#stonegsthsndescriptiondivhid,#taxmasteriddivhid").show();
		$('#taxmasterid').select2('val',gstselect);
		$('#taxmasterid').addClass('validate[required]');
	}else{
		$("#gstcalculationtypeiddivhid,#supplytypeiddivhid,#stonegsthsncodedivhid,#stonegsthsndescriptiondivhid,#taxmasteriddivhid").hide();
		$('#gstcalculationtypeid,#supplytypeid,#taxmasterid').select2('val','');
		$('#stonegsthsncode,#stonegsthsndescription').val('');
		$('#taxmasterid').removeClass('validate[required]');
	}	
}
//Kumaresan - Validate Table From and To
function validatetablevalues(){
	if(parseFloat($("#stonetableto").val()) < parseFloat($("#stonetablefrom").val())) {
		return "Should be greater than Table From";
	}
}
//Kumaresan - Validate Depth From and To
function validatedepthvalues(){
	if(parseFloat($("#stonedepthto").val()) < parseFloat($("#stonedepthfrom").val())) {
		return "Should be greater than Table From";
	}
}
function checkstoneentry(datarowid,status,type) {
	var typeret = '';
	$.ajax({
		url:base_url+"Stone/checkstoneentry",
		data:"stoneid="+datarowid+"&status="+status,
		type:"post",
		async:false,
		cache:false,
		success :function(msg) {
			nmsg = $.trim(msg);
			if(type == 'delete') {
				typeret = nmsg
			} else {
				if(nmsg == 'FAIL') {
					setTimeout(function() {
						$('#treebutton').prop('disabled',true);
					},50);
					treestatus =1;
					$('.dl-menuwrapper button').prop("disabled", "disabled");
				}else{
					setTimeout(function() {
						$('#treebutton').prop('disabled',false);
					},50);
					treestatus = 0;
					$('.dl-menuwrapper button').prop("disabled", "");
				}
			}
		},
	});
	if(type == 'delete') {
		return typeret;
	}
}
function checktreestatus(){
	if(treestatus == 1){
		$('.dl-menuwrapper button').prop("disabled", "disabled");
	}else if(treestatus == 0) {
		$('.dl-menuwrapper button').prop("disabled", "");
	}
}
function counterhideshow() {
	var stonemode = $('#stonemodeid').find('option:selected').val();
	if($('#counterstatus').val() == 'YES' && stonemode == 2){
		$('#counteriddivhid').show();
		$('#counterid').addClass('validate[required]');
	}else {
		$('#counterid').removeClass('validate[required]');
		$('#counteriddivhid').hide();
	}
}