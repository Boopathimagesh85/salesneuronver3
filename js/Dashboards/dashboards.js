$(document).ready(function() 
{
	globalid='all';
	{// Foundation Initialization
		$(document).foundation();
	}
	{// For Height
		var stnewheightaddform = $( window ).height();
		var fixedheightaddform = stnewheightaddform-60;
		$(".studiocontainer").css('height',''+fixedheightaddform+'px');
	}
	{//Keyboard Shortcut in nonbase
		viewgridview = 3;
	}
	
	{//folder open for mobile
		$("#folderaddicon").click(function(){
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			Materialize.updateTextFields();
			firstfieldfocus();
		});
	}
	{//Search Report list-report
		$('#reportssearch').bind("change keyup", function() {
            searchWord = $(this).val();
            if (searchWord.length >= 1) {
				$(".reportsfolderlist").hide();
				$("#appendreportvalue").show();
				$("#appendreportvalue").html('');
                $('ul.search-list-report li').each(function() {
                    text = $(this).text();
                    if (text.match(RegExp(searchWord, 'i'))) {
                        var alllinks = $(this).html();
						$("#appendreportvalue").append(alllinks);
                    }
                });
				$('#appendreportvalue label').click(function(){
					var id = $(this).attr('data-parentreportfolderid');
					globalid=id;
					dasboardsgrid();
				});	
            } else if (searchWord.length <= 1) { 
				$(".reportsfolderlist").show();
				$("#appendreportvalue").hide();
                $('ul.search-list-report li').each(function(){
                    text = $(this).text();
                    if (text.match(RegExp(searchWord, 'i'))) {
                        var alllinks = $(this).html();
						$("#appendreportvalue").html('');
                    }
                });
            }
        });
	}
	{// Grid Calling Function
		dasboardsgrid();
	}
	{//load folder-names
		loaddashboardfoldername();
	}	
	{// Full Screen View
		$('#exitfulscrnreport').hide();
		$("#fulscrnreport").click(function(){
			$('#fulscrnreport').hide();
			$('#exitfulscrnreport').show();			
			$("#reportarea").removeClass('large-9').removeClass('medium-9').addClass('large-12');
			$("#leftpanearea").removeClass('large-3').addClass('hidedisplay');
			dasboardsgrid();
		});
		$("#exitfulscrnreport").click(function(){			
			$('#exitfulscrnreport').hide();
			$('#fulscrnreport').show();
			$("#reportarea").removeClass('large-12').addClass('large-9').addClass('medium-9');
			$("#leftpanearea").removeClass('hidedisplay').addClass('large-3');
			dasboardsgrid();
		});
	}
	$("#dashboardsviewtoggle").addClass('hidedisplay');
	{// Folder CRUD Overlay
		//Open Folder Operations overlay
		$("#foldericon").click(function(){			
			addslideup('reportsview','folderoverlaydiv');
			folderoperationgrid();
			$('#editreportfolderbtn').hide();
			$('#addreportfolderbtn').show();
			$('#dashboardfolderrefreshicon').show();
			resetFields();
		});
		//Close Folder Operations overlay
		$("#groupcloseaddform").click(function(){			
			addslideup('folderoverlaydiv','reportsview');
		});
		//deleteicon
		$("#dashboardfolderdeleteicon").click(function(){
			var datarowid = $('#folderoperationgrid div.gridcontent div.active').attr('id');
			if(datarowid){		
				$("#basedeleteoverlay").fadeIn();
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		});
		//refreshicon 
		$("#dashboardfolderrefreshicon").click(function(){
			resetFields();
			$("#addreportfolderbtn").show();
			$("#dashboardfolderdeleteicon").show();
			$("#editreportfolderbtn").hide();
			refreshfoldergrid();
		});
	}
		{// Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			}else{
				$('#'+name+'').val('No');
			}
		
		});
	}
	{// Tool Bar click function 
		//Add
		$("#dashboardaddicon").click(function(){
			window.location =base_url+'Dashboardbuilder';
		});
		//Widget Connectors
		$("#widgetconnectorsicon").click(function(){
			window.location =base_url+'Widgetdatamapping';
		});
		//Module Connectors
		$("#moduleconnectorsicon").click(function(){
			window.location =base_url+'Moduleconversionmapping';
		});
	}
	//CREATE
	//create reportfolder
	$('#addreportfolderbtn').click(function(e){			
		$("#addreportfoldervalidate").validationEngine('validate');			
	});
	jQuery("#addreportfoldervalidate").validationEngine({
		onSuccess: function() {				
			var formdata = $("#reportfolderform").serialize();
			var amp = '&';
			var datainformation = amp + formdata;
			$.ajax({
				url: base_url + "Dashboards/createdashboardfolder",
				data: "datas=" + datainformation,
				type: "POST",
				cache:false,
				success: function(msg) {
					if(msg == true){
						resetFields();
						refreshfoldergrid();
						$( "#reportfoldersetpublic" ).prop( "checked", false );
						$( "#reportfoldersetdefault" ).prop( "checked", false );
						loaddashboardfoldername();
					} else{		
					}
				},
			});
		},
		onFailure: function() {	
		}
	});
	$("#reportfolderediticon").click(function(){
		var reportfolderid = $('#folderoperationgrid div.gridcontent div.active').attr('id');
		if (reportfolderid) {
			$('#editprimarydataid').val(reportfolderid);
			$("#reportfoldersetpublic").prop( "checked", false );
			$("#reportfoldersetdefault").prop( "checked", false );
			$('#addreportfolderbtn').hide();
			$('#dashboardfolderdeleteicon').hide();
			$('#editreportfolderbtn').show();
			$('#reportfolderrefreshicon').hide();
			$.ajax({
				url:base_url+"Dashboards/getdashboarddetails?reportid="+reportfolderid,
				contentType:'application/json; charset=utf-8',
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
						$('#reportsfoldername').val(data.reportfoldername);
						$('#reportfolderdescription').val(data.description);
						if(data.setaspublic === 'Yes'){
							$( "#reportfoldersetpublic" ).prop( "checked", true );
							$('#reportfoldersetpublic').val('Yes');
						}
						else{
							$( "#reportfoldersetpublic" ).prop( "checked", false );
							$('#reportfoldersetpublic').val('No');
						}				
						if(data.setdefault === 'Yes'){	
							$( "#reportfoldersetdefault" ).prop( "checked", true );
							$('#reportfoldersetdefault').val('Yes');
						}
						else{
							$( "#reportfoldersetdefault" ).prop( "checked", false );
							$('#reportfoldersetdefault').val('No');
						}
						
					},
			});	
		} 
		else {
			alertpopup("Please select a row");
		}
	});
	//edit reportfolder
	$('#editreportfolderbtn').click(function(e){
		
		$("#editreportfoldervalidate").validationEngine('validate');			
	});
	jQuery("#editreportfoldervalidate").validationEngine({
		onSuccess: function() {
		//chkbox settings
		if ($('#reportfoldersetpublic').is(':checked')) {
			$('#reportfoldersetpublic').val('Yes');
		}else{
			$('#reportfoldersetpublic').val('No');
		}
		if ($('#reportfoldersetdefault').is(':checked')) {
			$('#reportfoldersetdefault').val('Yes');
		}else{
			$('#reportfoldersetdefault').val('No');
		}
			var dashboardfolderid=$('#editprimarydataid').val();
			var formdata = $("#reportfolderform").serialize();
			var amp = '&';
			var datainformation = amp + formdata;
			$.ajax({
			url: base_url + "Dashboards/updatedashboardfolder",
			data: "datas=" + datainformation+"&dashboardfolderid="+dashboardfolderid,
			type: "POST",
			cache:false,
			success: function(msg) {
				if(msg == true){
				resetFields();
				refreshfoldergrid();
				$( "#reportfoldersetpublic" ).prop( "checked", false );
				$( "#reportfoldersetdefault" ).prop( "checked", false );
				loaddashboardfoldername();
				}else{			
				}
			},
			});
		$('#editreportfolderbtn').hide();
		$('#addreportfolderbtn').show();	
		$('#dashboardfolderdeleteicon').show();	
		},
		onFailure: function() {	
		}
	});
	$("#basedeleteyes").click(function(){
	$("#basedeleteoverlay").fadeOut();
		var datarowid = $('#folderoperationgrid div.gridcontent div.active').attr('id');
		$.ajax({
			url: base_url + "Dashboards/deletedashboardfolder?dashboardfolderid="+datarowid,
			cache:false,
			success: function(msg) {
				if(msg == true){	
				$('#dashboardfolderrefreshicon,#reloadicon').trigger('click');
				loaddashboardfoldername();				
				alertpopup("Dashboard Folder Deleted successfully !!!");		
				}else{			
				}
			},
			});
	});
	//reload
	$("#reloadicon").click(function(){	
		refreshgrid();		
	});
	{//refresh grid
		function refreshgrid() {
			var page = $(this).data('pagenum');
			var rowcount = $('ul#dashboardviewpgnumcnt li .page-text .active').data('rowcount');
			dasboardsgrid(page,rowcount);
		}
	}
	{//refresh grid
		function refreshfoldergrid() {
			var page = $(this).data('pagenum');
			var rowcount = $('ul#reportlistpgnumcnt li .page-text .active').data('rowcount');
			folderoperationgrid(page,rowcount);
		}
	}
	$( window ).resize(function() {
		maingridresizeheightset('dasboardsgrid');
	});
	//delete dashboard data
	$("#deleteicon").click(function(){
		var datarowid = $('#dasboardsgrid div.gridcontent div.active').attr('id');
		if(datarowid) {	
			$("#dashboarddeleteoverlay").fadeIn();
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#reportdeleteyes").click(function(){
		$("#dashboarddeleteoverlay").fadeOut();
		var id='all';
		var datarowid = $('#dasboardsgrid div.gridcontent div.active').attr('id');
		$.ajax({
			url: base_url + "Dashboards/dashboarddelete?primarydataid="+datarowid,
			cache:false,
			success: function(msg) {
				if(msg == true){			
					alertpopup("Dashboard Deleted successfully !!!");
					dasboardsgrid();
				}
			},
		});
	});
	//edit icon
	$("#dashboardediticon").click(function(){
		var datarowid = $('#dasboardsgrid div.gridcontent div.active').attr('id');
		if(datarowid){		
			sessionStorage.setItem("dashboardid",datarowid);
			window.location = base_url+'Dashboardbuilder';
		} else {
			alertpopup("Please select a row");
		}	
	});
	/* load report list based on folder-names */
	$('#dashboardfolder').change(function(){
		var id = $(this).val();
		globalid=id;
		dasboardsgrid();
	});
	//filter
	$("#dashboardsfilterddcondvaluedivhid").hide();
	$("#dashboardsfilterdatecondvaluedivhid").hide();
	{	//field value set
		$("#dashboardsfiltercondvalue").focusout(function(){
			var value = $("#dashboardsfiltercondvalue").val();
			$("#dashboardsfinalfiltercondvalue").val(value);
			$("#dashboardsfilterfinalviewconid").val(value);
		});
		$("#dashboardsfilterddcondvalue").change(function(){
			var value = $("#dashboardsfilterddcondvalue").val();
			dashboardsmainfiltervalue=[];
			$('#dashboardsfilterddcondvalue option:selected').each(function(){
			    var $mvalue =$(this).attr('data-ddid');
			    dashboardsmainfiltervalue.push($mvalue);
				$("#dashboardsfinalfiltercondvalue").val(dashboardsmainfiltervalue);
			});
			$("#dashboardsfilterfinalviewconid").val(value);
		});
	}
	dashboardsfiltername = [];
	$("#dashboardsfilteraddcondsubbtn").click(function() {
		$("#dashboardsfilterformconditionvalidation").validationEngine("validate");	
	});
	$("#dashboardsfilterformconditionvalidation").validationEngine({
		onSuccess: function() {
		dashboardmodulefilter(dasboardsgrid,'dashboards');
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
});
function loaddashboardfoldername() {
	$("#mainreportfolder").empty();
	$("#mainreportfolder").append('<li class="repfolderlistulstyle" data-parentreportfolderid=all><label class="repfolderlistheader"><i class="material-icons" style="margin-right:5px; vertical-align: middle;">folder</i><span>All Dashboard</span></label></li>');
	$.ajax({
		url:base_url+"Dashboards/loaddashboardfoldername", 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			var device = $('#device').val();
			if(device=='phone') {
				$('#dashboardfolder').empty();
				$('#dashboardfolder').append($("<option value='all'>All Dashboard</option>"));
				$.each(data, function(index) {
					$('#dashboardfolder')
					.append($("<option></option>")
					.attr("value",data[index]['dashboardfolderid'])
					.text(data[index]['dashboardfoldername']));
				});
				$('#dashboardfolder').trigger('change');
			} else {
				$.each(data, function(index){
					$("#mainreportfolder").append('<li class="repfolderlistulstyle"><label class="repfolderlistheader" data-parentreportfolderid='+data[index]['dashboardfolderid']+'><i class="material-icons" style="margin-right:5px;vertical-align:middle;">folder</i><span>'+data[index]['dashboardfoldername']+'</span></label></li>');
				});	
				//load report list based on folder-names
				$('#mainreportfolder label').click(function() {
					var id = $(this).attr('data-parentreportfolderid');
					globalid=id;
					dasboardsgrid();
				});
			}
		}
	});
}
{// Dashboards View Grid
	function dasboardsgrid(page,rowcount) {
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 10 : rowcount;
		var wwidth = $('#dasboardsgrid').width();
		var wheight = $(window).height();
		/*col sort*/
		var sortcol = $("#dasboardssortcolumn").val();
		var sortord = $("#dasboardssortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.dashboardviewheadercolsort').hasClass('datasort') ? $('.dashboardviewheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.dashboardviewheadercolsort').hasClass('datasort') ? $('.dashboardviewheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.dashboardviewheadercolsort').hasClass('datasort') ? $('.dashboardviewheadercolsort.datasort').attr('id') : '0';
		var filterid = $("#dashboardsfilterid").val();
		var conditionname = $("#dashboardsconditionname").val();
		var filtervalue = $("#dashboardsfiltervalue").val();
		$.ajax({
			url:base_url+"Dashboards/viewdashboardlist?dashboardfolderid="+globalid+"&moduleid=268&maintabinfo=dashboard&primaryid=dashboardid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+"&sortcol="+sortcol+"&sortord="+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#dasboardsgrid').empty();
				$('#dasboardsgrid').append(data.content);
				$('#dasboardsgridfooter').empty();
				$('#dasboardsgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('dasboardsgrid');
				{//sorting
					$('.dashboardviewheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.dashboardviewheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#dashboardviewpgnumcnt li .page-text .active').data('rowcount');
						var sortcol = $('.dashboardviewheadercolsort').hasClass('datasort') ? $('.dashboardviewheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.dashboardviewheadercolsort').hasClass('datasort') ? $('.dashboardviewheadercolsort.datasort').data('sortorder') : '';
						$("#dasboardssortorder").val(sortord);
						$("#dasboardssortcolumn").val(sortcol);
						dasboardsgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					sortordertypereset('dashboardviewheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						dasboardsgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#dashboardviewpgrowcount').change(function(){
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('#prev').data('pagenum');
						dasboardsgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//Material select
				$('#dashboardviewpgrowcount').material_select();
			},
		});
	}
}
{// Reports Folder Operation Grid Overlay
	function folderoperationgrid(page,rowcount) 
	{
		/*pagination*/
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 10 : rowcount;
		var wwidth = $('#folderoperationgrid').width();
		var wheight = $('#folderoperationgrid').height();
		/* col sort */
		var sortcol = $('.reportlistheadercolsort').hasClass('datasort') ? $('.reportlistheadercolsort.datasort').data('sortcolname') : '';
		var sortord =  $('.reportlistheadercolsort').hasClass('datasort') ? $('.reportlistheadercolsort.datasort').data('sortorder') : '';
		var headcolid = $('.reportlistheadercolsort').hasClass('datasort') ? $('.reportlistheadercolsort.datasort').attr('id') : '0';
		$.ajax({
			url:base_url+"Dashboards/viewdashboardfolder?maintabinfo=dashboardfolder&primaryid=dashboardfolderid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=268',
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#folderoperationgrid').empty();
				$('#folderoperationgrid').append(data.content);
				$('#folderoperationgridfooter').empty();
				$('#folderoperationgridfooter').append(data.footer);
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('folderoperationgrid');
				{/* sorting */
					$('.reportlistheadercolsort').click(function(){
						$('.reportlistheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $('ul#reportlistpgnum li.active').data('pagenum');
						var rowcount = $('ul#reportlistpgnumcnt li .page-text .active').data('rowcount');
						folderoperationgrid(page,rowcount);
					});
					sortordertypereset('reportlistheadercolsort',headcolid,sortord);
				}
				{/* pagination */
					$('.pagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#reportlistpgnumcnt li .page-text .active').data('rowcount');
						folderoperationgrid(page,rowcount);
					});
					$('.pagerowcount').click(function(){
						var page = $('ul#reportlistpgnum li.active').data('pagenum');
						var rowcount = $(this).data('rowcount');
						folderoperationgrid(page,rowcount);
					});
					$('.pvpagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#reportlistpgnumcnt li .page-text .active').data('rowcount');
						folderoperationgrid(page,rowcount);
					});
					$('#reportlistpgrowcount').change(function(){
						var rowcount = $(this).val();
						var page = $('ul#reportlistpgnum li.active').data('pagenum');
						folderoperationgrid(page,rowcount);
					});
				}
			},
		});		
	}
}