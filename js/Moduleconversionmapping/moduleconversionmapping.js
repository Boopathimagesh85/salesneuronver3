$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}
	{//Grid Settings and calling
		moduledatamappingaddgrid();
		firstfieldfocus();
		//crud action
		crudactionenable();
	}
	{//keyboard shortcut reset global variable
		viewgridview = 0;
		innergridview = 1;
	}
	$("#editicon").hide();
	//reload
	$("#reloadicon").click(function() {
		resetFields();
		refreshgrid();
	});
	$( window ).resize(function() {
		maingridresizeheightset('moduledatamappingaddgrid');
		sectionpanelheight('groupsectionoverlay');
	});
	{//validation for Company Add
		$('#moduledatamappingsavebutton').click(function() {
			$("#moduledatamappingformaddwizard").validationEngine('validate');
		});
		jQuery("#moduledatamappingformaddwizard").validationEngine( {
			onSuccess: function() {
				widgetmappingcreate();
			},
			onFailure: function() {
				var dropdownid =['1','leadfieldid'];
				dropdownfailureerror(dropdownid);			
				alertpopup(validationalert);
			}
		});
	}
	{//validation for Company Add
		$('#moduledatamappingdataupdatesubbtn').click(function() {
			$('.ftab').trigger('click');
			$("#moduledatamappingformeditwizard").validationEngine('validate');
		});
		jQuery("#moduledatamappingformeditwizard").validationEngine( {
			onSuccess: function() {
				mappingeditdataaddfun();
			},
			onFailure: function() {
				var dropdownid =['1','leadfieldid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	//moduleid
	$("#moduleid").change(function() {
		var val = $(this).val();
		var mapval = $('#mappingmoduleid').val();
		if(val > 1 && mapval > 1 && val!=mapval) {
			loadmodulefields('frommodulefieldid','tomodulefieldid',val,mapval);
		}
	});
	//mapping moduleid
	$("#mappingmoduleid").change(function() {
		var val = $('#moduleid').val();
		var mapval = $(this).val();
		if(val > 1 && mapval > 1 && val!=mapval) {
			loadmodulefields('frommodulefieldid','tomodulefieldid',val,mapval);
		}
	});
	{//filter work
		//for toggle
		$('#moduledatamappingviewtoggle').click(function() {
			if ($(".moduledatamappingfilterslide").is(":visible")) {
				$('div.moduledatamappingfilterslide').addClass("filterview-moveleft");
			}else{
				$('div.moduledatamappingfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#moduledatamappingclosefiltertoggle').click(function(){
			$('div.moduledatamappingfilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#moduledatamappingfilterddcondvaluedivhid").hide();
		$("#moduledatamappingfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#moduledatamappingfiltercondvalue").focusout(function(){
				var value = $("#moduledatamappingfiltercondvalue").val();
				$("#moduledatamappingfinalfiltercondvalue").val(value);
			});
			$("#moduledatamappingfilterddcondvalue").change(function(){
				var value = $("#moduledatamappingfilterddcondvalue").val();
				if( $('#moduledatamappingfilterddcondvalue').hasClass('select2-container-multi') ) {
					moduledatamappingmainfiltervalue=[];
					$('#moduledatamappingfilterddcondvalue option:selected').each(function(){
					    var $cvalue =$(this).attr('data-ddid');
					    moduledatamappingmainfiltervalue.push($cvalue);
						$("#moduledatamappingfinalfiltercondvalue").val(moduledatamappingmainfiltervalue);
					});
					$("#moduledatamappingfilterfinalviewconid").val(value);
				} else {
					$("#moduledatamappingfinalfiltercondvalue").val(value);
					$("#moduledatamappingfilterfinalviewconid").val(value);
				}
			});
		}
		moduledatamappingfiltername = [];
		$("#moduledatamappingfilteraddcondsubbtn").click(function() {
			$("#moduledatamappingfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#moduledatamappingfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(moduledatamappingaddgrid,'moduledatamapping');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	$('#groupcloseaddform').click(function(){
		window.location =base_url+'Dashboards';
	});
});
//new data add submit function
function widgetmappingcreate() {
    var formdata = $("#moduledatamappingaddform").serialize();
	var moduleid = $('#moduleid').find('option:selected').val();
	var mappingmoduleid = $('#mappingmoduleid').find('option:selected').val();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax( {
        url: base_url + "Moduleconversionmapping/newdatacreate",
        data: "datas=" + datainformation+"&moduleid="+moduleid+"&mappingmoduleid="+mappingmoduleid,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				$(".ftab").trigger('click');
				$(".addsectionclose").trigger("click");
				resetFields();
				refreshgrid();
			 }
        },
    });
}
//edit operation
function mappingeditdataaddfun() {
	 var formdata = $("#moduledatamappingaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax( {
        url: base_url + "Moduleconversionmapping/editdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	refreshgrid();
				$(".ftab").trigger('click');
				resetFields();
				//Keyboard short cut
				saveformview = 0;
			 }
        },
    });
}
//validation of not equals
function checknotqual() {
	var a = $('#moduleid').val();
	var b = $('#mappingmoduleid').val();
	if(a == b){
		return ('Both Module and MappingModule should not be same');
	}
}
//Menu Category Add Grid
function moduledatamappingaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#moduledatamappingaddgrid').width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#moduledatamappingsortcolumn").val();
	var sortord = $("#moduledatamappingsortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.moduledatamappingheadercolsort').hasClass('datasort') ? $('.moduledatamappingheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.moduledatamappingheadercolsort').hasClass('datasort') ? $('.moduledatamappingheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.moduledatamappingheadercolsort').hasClass('datasort') ? $('.moduledatamappingheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#moduledatamappingviewfieldids').val();
	var filterid = $("#moduledatamappingfilterid").val();
	var conditionname = $("#moduledatamappingconditionname").val();
	var filtervalue = $("#moduledatamappingfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=moduleconversionmapping&primaryid=moduleconversionmappingid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#moduledatamappingaddgrid').empty();
			$('#moduledatamappingaddgrid').append(data.content);
			$('#moduledatamappingaddgridfooter').empty();
			$('#moduledatamappingaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('moduledatamappingaddgrid');
			maingridresizeheightset('moduledatamappingaddgrid');
			{//sorting
				$('.moduledatamappingheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.moduledatamappingheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.moduledatamappingheadercolsort').hasClass('datasort') ? $('.moduledatamappingheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.moduledatamappingheadercolsort').hasClass('datasort') ? $('.moduledatamappingheadercolsort.datasort').data('sortorder') : '';
					$("#moduledatamappingsortorder").val(sortord);
					$("#moduledatamappingsortcolumn").val(sortcol);
					var sortpos = $('#moduledatamappingaddgrid .gridcontent').scrollLeft();
					moduledatamappingaddgrid(page,rowcount);
					$('#moduledatamappingaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('moduledatamappingheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					moduledatamappingaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#moduledatamappingpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					moduledatamappingaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#moduledatamappingaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#moduledatamappingpgrowcount').material_select();	
		},
	});
}
{//crud operation
	function crudactionenable() {
		{//add icon click
			$('#addicon').click(function(e){
				sectionpanelheight('groupsectionoverlay');
				$("#groupsectionoverlay").removeClass("closed");
				$("#groupsectionoverlay").addClass("effectbox");
				e.preventDefault();
				$("#groupsectionoverlay").removeClass("closed");
				$("#groupsectionoverlay").addClass("effectbox");
				$('#moduledatamappingdataupdatesubbtn').hide().removeClass('hidedisplayfwg');
				$('#moduledatamappingsavebutton').show();
			});
		}
		$('#deleteicon').click(function(e) {
			e.preventDefault();
			var datarowid = $('#moduledatamappingaddgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				$("#basedeleteoverlay").fadeIn();
				$('#moduledatamappingprimarydataid').val(datarowid);
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function() {
			var datarowid = $('#moduledatamappingprimarydataid').val();
			mappingrecorddelete(datarowid);
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#moduledatamappingpgnum li.active').data('pagenum');
		var rowcount = $('ul#moduledatamappingpgnumcnt li .page-text .active').data('rowcount');
		moduledatamappingaddgrid(page,rowcount);
	}
}
{// Record delete function
	function mappingrecorddelete(datarowid) {
		$.ajax({
			url: base_url + "Moduleconversionmapping/deleteinformationdata?primarydataid="+datarowid,
			cache:false,
			success: function(msg)  {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					refreshgrid();
					alertpopup('Record deleted successfully');
					$("#basedeleteoverlay").fadeOut();
				} else if (nmsg == "false") {
				}
			},
		});
	}
}
function loadmodulefields(frommodulefield,tomodulefield,val,mapval) {
	$('#'+frommodulefield+',#'+tomodulefield+'').empty();
	$('#'+frommodulefield+',#'+tomodulefield+'').append($("<option></option>"));
	$.ajax({
		url:base_url+'Moduleconversionmapping/modulefieldname',
		data:{sourcemoduleid:val,destmoduleid:mapval},
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				var newddappend="";
				var source=data.source;
				var newlength=source.length;
				for(var m=0;m < newlength;m++){	
					newddappend += "<option value ='"+source[m]['datasid']+"' data-id='"+source[m]['datasid']+"' data-uitype='"+source[m]['uitype']+"''>"+source[m]['dataname']+ "</option>";
				}
				//destination
				var newddappends="";
				var destination=data.destination;
				var newlengths=destination.length;
				for(var m=0;m < newlengths;m++){	
					newddappends += "<option value ='"+destination[m]['datasid']+"' data-id='"+destination[m]['datasid']+"' data-uitype='"+destination[m]['uitype']+"''>"+destination[m]['dataname']+ "</option>";
				}
			}
			$('#'+frommodulefield+'').append(newddappend);
			$('#'+tomodulefield+'').append(newddappends);
			$('#'+frommodulefield+',#'+tomodulefield+'').trigger('change');
		},
	});
}