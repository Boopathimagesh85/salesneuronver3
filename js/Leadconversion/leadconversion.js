$(document).ready(function(){
	{//Foundation Initialization
		$(document).foundation();
	}
	{//Grid Settings and calling
		leadconversionaddgrid();
		//crud action
		crudactionenable();
	}
	{//keyboard shortcut reset global variable
		 viewgridview = 0;
		 innergridview = 1;
	}
	$("#editicon").hide();
	//reload
	$("#reloadicon").click(function(){
		resetFields();
		refreshgrid();
		$('#leadconversionaddform').find('input, textarea, button, select').attr('disabled',true);
		$('#leadconversionaddform').find("select").select2('disable');
	});
	$('.addsectionclose').click(function(){
		resetFields();
	});
	$( window ).resize(function() {
		maingridresizeheightset('leadconversionaddgrid');
		sectionpanelheight('groupsectionoverlay');
	});
	{//validation for Company Add  
		$('#leadconversionsavebutton').click(function() {
			$("#leadconversionformaddwizard").validationEngine('validate');
		});
		jQuery("#leadconversionformaddwizard").validationEngine({
			onSuccess: function() {
				leadmappingnewdataaddfun();
			},
			onFailure: function() {
				var dropdownid =['1','leadfieldid'];
				dropdownfailureerror(dropdownid);				
				alertpopup(validationalert);
			}
		});
	}
	$('#leadconversionaddform').find('input, textarea, button, select').attr('disabled',true);
	$('#leadconversionaddform').find("select").select2('disable');
	if(softwareindustryid == 2){
		leaddownmodulevalset('leadfieldid','formfieldsidhidden','modulefieldid','uitypeid','fieldlabel','modulefieldid','modulefieldid','uitypeid','modulefield','moduletabid,uitypeid','92,0');
	}else if(softwareindustryid == 4){
		leaddownmodulevalset('leadfieldid','formfieldsidhidden','modulefieldid','uitypeid','fieldlabel','modulefieldid','modulefieldid','uitypeid','modulefield','moduletabid,uitypeid','83,0');
	}else if(softwareindustryid == 5){
		leaddownmodulevalset('leadfieldid','formfieldsidhidden','modulefieldid','uitypeid','fieldlabel','modulefieldid','modulefieldid','uitypeid','modulefield','moduletabid,uitypeid','128,0');
	}else{
		leaddownmodulevalset('leadfieldid','formfieldsidhidden','modulefieldid','uitypeid','fieldlabel','modulefieldid','modulefieldid','uitypeid','modulefield','moduletabid,uitypeid','201,0');
	}
	
	//lead drop down change
	$("#leadfieldid").change(function() {
		var value = $("#leadfieldid").find('option:selected').data('uitype'); 
		if(softwareindustryid == 1){
			fielddropdownloadfunction('accountfieldid','modulefieldid','fieldlabel','modulefield','202','moduletabid',value);
			fielddropdownloadfunction('contactfieldid','modulefieldid','fieldlabel','modulefield','203','moduletabid',value);
			fielddropdownloadfunction('opprtunityfieldid','modulefieldid','fieldlabel','modulefield','204','moduletabid',value);
		}else if(softwareindustryid == 2){
			fielddropdownloadfunction('accountfieldid','modulefieldid','fieldlabel','modulefield','74','moduletabid',value);
			fielddropdownloadfunction('contactfieldid','modulefieldid','fieldlabel','modulefield','75','moduletabid',value);
			fielddropdownloadfunction('opprtunityfieldid','modulefieldid','fieldlabel','modulefield','93','moduletabid',value);
		}else if(softwareindustryid == 3){
			fielddropdownloadfunction('accountfieldid','modulefieldid','fieldlabel','modulefield','91','moduletabid',value);
			fielddropdownloadfunction('contactfieldid','modulefieldid','fieldlabel','modulefield','203','moduletabid',value);
			fielddropdownloadfunction('opprtunityfieldid','modulefieldid','fieldlabel','modulefield','204','moduletabid',value);
		}else if(softwareindustryid == 4){
			fielddropdownloadfunction('accountfieldid','modulefieldid','fieldlabel','modulefield','81','moduletabid',value);
			fielddropdownloadfunction('contactfieldid','modulefieldid','fieldlabel','modulefield','82','moduletabid',value);
			fielddropdownloadfunction('opprtunityfieldid','modulefieldid','fieldlabel','modulefield','84','moduletabid',value);
		}else if(softwareindustryid == 5){
			fielddropdownloadfunction('accountfieldid','modulefieldid','fieldlabel','modulefield','129','moduletabid',value);
			fielddropdownloadfunction('contactfieldid','modulefieldid','fieldlabel','modulefield','130','moduletabid',value);
			fielddropdownloadfunction('opprtunityfieldid','modulefieldid','fieldlabel','modulefield','131','moduletabid',value);
		}	
		$('.cleardataform').validationEngine('hideAll');
	});
	$('#groupcloseaddform').click(function(){
		window.location =base_url+'Lead';
	});
	{//filter work
		//for toggle
		$('#leadconversionviewtoggle').click(function() {
			if ($(".leadconversionfilterslide").is(":visible")) {
				$('div.leadconversionfilterslide').addClass("filterview-moveleft");
			}else{
				$('div.leadconversionfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#leadconversionclosefiltertoggle').click(function(){
			$('div.leadconversionfilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#leadconversionfilterddcondvaluedivhid").hide();
		$("#leadconversionfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#leadconversionfiltercondvalue").focusout(function(){
				var value = $("#leadconversionfiltercondvalue").val();
				$("#leadconversionfinalfiltercondvalue").val(value);
			});
			$("#leadconversionfilterddcondvalue").change(function(){
				var value = $("#leadconversionfilterddcondvalue").val();
				if( $('#s2id_leadconversionfilterddcondvalue').hasClass('select2-container-multi') ) {
					leadconversionmainfiltervalue=[];
					$('#leadconversionfilterddcondvalue option:selected').each(function(){
					    var $value =$(this).attr('data-ddid');
					    leadconversionmainfiltervalue.push($value);
						$("#leadconversionfinalfiltercondvalue").val(leadconversionmainfiltervalue);
					});
					$("#leadconversionfilterfinalviewconid").val(value);
				} else {
					$("#leadconversionfinalfiltercondvalue").val(value);
					$("#leadconversionfilterfinalviewconid").val(value);
				}
			});
		}
		leadconversionfiltername = [];
		$("#leadconversionfilteraddcondsubbtn").click(function() {
			$("#leadconversionfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#leadconversionfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(leadconversionaddgrid,'leadconversion');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
});
//drop down values set for view
function leaddownmodulevalset(ddname,dataattrname,otherdataattrname1,otherdataattrname2,dataname,dataid,otherdataname1,otherdataname2,datatable,whfield,whvalue) {
	$('#'+ddname+'').empty();
	$('#'+ddname+'').append($("<option></option>"));
	$.ajax({
		url:base_url+'Mailmaster/fetchmaildddatawithmultiplecond?dataname='+dataname+'&dataid='+dataid+'&datatab='+datatable+'&whdatafield='+whfield+'&whval='+whvalue+'&othername1='+otherdataname1+'&othername2='+otherdataname2,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#'+ddname+'')
					.append($("<option></option>")
					.attr("data-uitype",data[index]['otherdataattrname2'])
					.attr("data-leadid",data[index]['otherdataattrname1'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}	
		},
	});
}
function fielddropdownloadfunction(ddname,fieldid,fieldname,tablename,moduleid,moduletabid,value) {
	$('#'+ddname+'').empty();
	$('#'+ddname+'').append($("<option></option>"));
	$.ajax({
		url:base_url+'Leadconversion/fetchdddatawithcond?fieldid='+fieldid+'&fieldname='+fieldname+'&tablename='+tablename+'&moduleid='+moduleid+'&value='+value+"&moduletabid="+moduletabid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#'+ddname+'')
					.append($("<option></option>")
					.attr("data-id",data[index]['datasid'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}	
			$('#'+ddname+'').trigger('change');
		},
	});
}
//new data add submit function
function leadmappingnewdataaddfun() {
    var formdata = $("#leadconversionaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax( {
        url: base_url + "Leadconversion/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				$("#groupsectionoverlay").removeClass("effectbox");
				$("#groupsectionoverlay").addClass("closed");	
				refreshgrid();
				$('#leadconversionaddform').find('input, textarea, button, select').attr('disabled',false);
				$('#leadconversionaddform').find("select").select2('disable');
			 }
        },
    });
}
//Menu Category Add Grid
function leadconversionaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $(window).width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#leadconversionsortcolumn").val();
	var sortord = $("#leadconversionsortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.leadconversionheadercolsort').hasClass('datasort') ? $('.leadconversionheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.leadconversionheadercolsort').hasClass('datasort') ? $('.leadconversionheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.leadconversionheadercolsort').hasClass('datasort') ? $('.leadconversionheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#leadconversionviewfieldids').val();
	var filterid = $("#leadconversionfilterid").val();
	var conditionname = $("#leadconversionconditionname").val();
	var filtervalue = $("#leadconversionfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=leadconversionmapping&primaryid=leadconversionmappingid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#leadconversionaddgrid').empty();
			$('#leadconversionaddgrid').append(data.content);
			$('#leadconversionaddgridfooter').empty();
			$('#leadconversionaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('leadconversionaddgrid');
			maingridresizeheightset('leadconversionaddgrid');
			{//sorting
				$('.leadconversionheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.leadconversionheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.leadconversionheadercolsort').hasClass('datasort') ? $('.leadconversionheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.leadconversionheadercolsort').hasClass('datasort') ? $('.leadconversionheadercolsort.datasort').data('sortorder') : '';
					$("#leadconversionsortorder").val(sortord);
					$("#leadconversionsortcolumn").val(sortcol);
					var sortpos = $('#leadconversionaddgrid .gridcontent').scrollLeft();
					leadconversionaddgrid(page,rowcount);
					$('#leadconversionaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('leadconversionheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					leadconversionaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#leadconversionpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					leadconversionaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#leadconversionaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#leadconversionpgrowcount').material_select();
		},
	});
}
{//crud operation
	function crudactionenable() {
		{//add icon click
			$('#addicon').click(function(e){
				e.preventDefault();
				sectionpanelheight('groupsectionoverlay');
				$("#groupsectionoverlay").removeClass("closed");
				$("#groupsectionoverlay").addClass("effectbox");
				$('#leadconversionaddform').find('input, textarea, button, select').attr('disabled',false);
				$('#leadconversionaddform').find("select").select2('enable');
				firstfieldfocus();
				Materialize.updateTextFields();
			});
		}
		$('#deleteicon').click(function(e){
			e.preventDefault();
			var datarowid = $('#leadconversionaddgrid div.gridcontent div.active').attr('id');
			if(datarowid){		
				$('#leadconversionprimarydataid').val(datarowid);
				$("#basedeleteoverlay").fadeIn();
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
			Materialize.updateTextFields();
		});
		$("#basedeleteyes").click(function(){
			var datarowid = $('#leadconversionprimarydataid').val();
			leadmaprecorddelete(datarowid);
		});
	}
}
{
	$("#accountfieldid").change(function(){
		var accountid = $(this).val();
		checfieldexist('accountfieldid',accountid);
	});
	$("#contactfieldid").change(function(){
		var accountid = $(this).val();
		checfieldexist('contactfieldid',accountid);
	});
	$("#opprtunityfieldid").change(function(){
		var accountid = $(this).val();
		checfieldexist('opprtunityfieldid',accountid);
	});	
	function checfieldexist(fieldname,fieldid){
		if(fieldid){
			$.ajax({
				url:base_url+'Leadconversion/checkfieldexist?fieldname='+fieldname+'&fieldid='+fieldid,
				type:"post",
				async:false,
				cache:false,
				success :function(msg) {
					var nmsg = $.trim(msg);
					if (nmsg == 'TRUE') {
						$("#"+fieldname).select2('val','').trigger('change');
						alertpopup("Field name already exist. Kindly remove the field in lead conversion. To map new conversion for this field");
					}
				},				
			});		
		}		
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#leadconversionpgnum li.active').data('pagenum');
		var rowcount = $('ul#leadconversionpgnumcnt li .page-text .active').data('rowcount');
		leadconversionaddgrid(page,rowcount);
	}
}
{// Record delete function
	function leadmaprecorddelete(datarowid)	{
		$.ajax({
			url: base_url + "Leadconversion/deleteinformationdata?primarydataid="+datarowid,
			cache:false,
			success: function(msg)  {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					refreshgrid();
					alertpopup('Record deleted successfully');
					$("#basedeleteoverlay").fadeOut();
				}
			},
		});
	}
}
//account field checked
function accountfieldcheck() {
	var fieldid = $("#accountfieldid").val();
	var editid = $("#editid").val();
	var tablename = "leadconversionmapping";
	var tableid = "accountid";
	var nmsg = "";
	if( fieldid !="" ) {
		$.ajax({
			url:base_url+"Leadconversion/accountfieldnamecheck",
			data:"data=&fieldid="+fieldid+"&tablename="+tablename+"&primaryid="+editid+"&tableid="+tableid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			return "Account field name already exists!";
		} 
	}
}
//Contact field checked
function contactfieldcheck(){
	var fieldid = $("#contactfieldid").val();
	var editid = $("#editid").val();
	var tablename = "leadconversionmapping";
	var tableid = "contactid";
	var nmsg = "";
	if( fieldid !="" ) {
		$.ajax({
			url:base_url+"Leadconversion/accountfieldnamecheck",
			data:"data=&fieldid="+fieldid+"&tablename="+tablename+"&primaryid="+editid+"&tableid="+tableid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			return "Contact field name already exists!";
		} 
	}
}
//Opportunity field checked
function opportunityfieldcheck(){
	var fieldid = $("#opprtunityfieldid").val();
	var editid = $("#editid").val();
	var tablename = "leadconversionmapping";
	var tableid = "opportunityid";
	var nmsg = "";
	if( fieldid !="" ) {
		$.ajax({
			url:base_url+"Leadconversion/accountfieldnamecheck",
			data:"data=&fieldid="+fieldid+"&tablename="+tablename+"&primaryid="+editid+"&tableid="+tableid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			return "Opportunity field name already exists!";
		} 
	}
}
//Unique Field Name check
function leaduniquefieldnamecheck() {
	var leadfieldid = $('#leadfieldid').val();
	var primaryid = $('#leadconversionprimarydataid').val();
	var nmsg = "";
	if( leadfieldid != "" ) {
		$.ajax({
			url:base_url+"Leadconversion/leaduniquefieldnamecheck",
			data:"data=&leadfieldid="+leadfieldid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Field Name already exists! Please select some other field.";
				}
			} else {
				return "Field Name already exists! Please select some other field.";
			}
		} 
	}
}