$(document).ready(function() {

	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		storageaddgrid();
		firstfieldfocus();
		storagecrudactionenable();
		setTimeout(function(){parenttreereloadfun(0);},300);
	}
	STORAGEDELETE = 0;
	retrievestatus = 0;
	//hidedisplay
	$('#storagedataupdatesubbtn,#groupcloseaddform').hide();
	$('#storagesavebutton').show();
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			storageaddgrid();
		});
	}
	round = $("#weight_round").val();
	{//validation for Add  
		$('#storagesavebutton').click(function(e) {
			if(e.which == 1 || e.which === undefined) {
				$("#storageformaddwizard").validationEngine('validate');
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			}
		});
		jQuery("#storageformaddwizard").validationEngine( {
			onSuccess: function() {
				storageaddformdata();
			},
			onFailure: function() {
				var dropdownid =['1','storagecategoryid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//update information
		$('#storagedataupdatesubbtn').click(function(e) {
			if(e.which == 1 || e.which === undefined) {
				$("#storageformeditwizard").validationEngine('validate');
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			}
		});
		jQuery("#storageformeditwizard").validationEngine({
			onSuccess: function() {
				storageupdateformdata();
			},
			onFailure: function() {
				var dropdownid =['1','storagecategoryid'];
				alertpopup(validationalert);
			}	
		});
	}
	{//action events
		$("#storagereloadicon").click(function() {
			clearform('cleardataform');
			$('#storageaddform').find('input, textarea, button, select').attr('disabled',true);
			$('#storageaddform').find("select").select2('disable');
			storagerefreshgrid();
			$('#storagedataupdatesubbtn').hide();
			$('#storagesavebutton').show();
			$("#deleteicon").show();
		});
		$( window ).resize(function() {
			maingridresizeheightset('storageaddgrid');
			sectionpanelheight('groupsectionoverlay');
		});
	}
	{//filter work
		//for toggle
		$('#storageviewtoggle').click(function() {
			if ($(".storagefilterslide").is(":visible")) {
				$('div.storagefilterslide').addClass("filterview-moveleft");
			}else{
				$('div.storagefilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#storageclosefiltertoggle').click(function(){
			$('div.storagefilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#storagefilterddcondvaluedivhid").hide();
		$("#storagefilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#storagefiltercondvalue").focusout(function(){
				var value = $("#storagefiltercondvalue").val();
				$("#storagefinalfiltercondvalue").val(value);
				$("#storagefilterfinalviewconid").val(value);
			});
			$("#storagefilterddcondvalue").change(function(){
				var value = $("#storagefilterddcondvalue").val();
				var fvalue = $("#storagefilterddcondvalue option:selected").attr('data-ddid');
				storageemainfiltervalue=[];
				if( $('#s2id_storagefilterddcondvalue').hasClass('select2-container-multi') ) {
					storageemainfiltervalue=[];
					$('#storagefilterddcondvalue option:selected').each(function(){
					    var $cvalue =$(this).attr('data-ddid');
					    storageemainfiltervalue.push($cvalue);
						$("#storagefinalfiltercondvalue").val(storageemainfiltervalue);
					});
					$("#storagefilterfinalviewconid").val(value);
				} else {
					$("#storagefinalfiltercondvalue").val(fvalue);
					$("#storagefilterfinalviewconid").val(value);
				}
			});
		}
		storagefiltername = [];
		$("#storagefilteraddcondsubbtn").click(function() {
			$("#storagefilterformconditionvalidation").validationEngine("validate");	
		});
		$("#storagefilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(storageaddgrid,'storage');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//Addform section overlay close
		$(".addsectionclose").click(function(){
			resetFields();
			$("#groupsectionoverlay").removeClass("effectbox");
			$("#groupsectionoverlay").addClass("closed");
			$("#deleteicon").show();
			$("#parentcounterid").val("");
			$("#storagetypeid").prop("disabled", false); 
		});
	}
	{//storage hierarchy
		$('#storagelistuldata li').click(function() {
			var name=$(this).attr('data-listname');
			var listid=$(this).attr('data-listid');
			var level = parseInt($(this).attr('data-level'));
			var maxlevel = parseInt($('#maxcategorylevel').val());
			$('#parentcounter').val(name);
			$('#parentcounterid').val(listid);
			if(level >= maxlevel){
				alertpopup('Maximum storage level reached');
				return false;
			}
			var editid = $('#storageprimarydataid').val();
			if(listid != editid) {
				storagerefreshgrid();
			} else {
				alertpopup('You cant choose the same storage as parentstorage');
			}
		});	
	}
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
//Documents Add Grid
function storageaddgrid(page,rowcount) {
	var defrecview = $('#mainviewdefaultview').val();
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
	var wwidth = $('#storageaddgrid').width();
	var wheight = $(window).height();
	var viewname = $('#dynamicdddataview').val();
	$('#viewgridheaderid').text(viewname);
	//col sort
	var sortcol = $("#storagesortcolumn").val();
	var sortord = $("#storagesortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.storageheadercolsort').hasClass('datasort') ? $('.storageheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.storageheadercolsort').hasClass('datasort') ? $('.storageheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.storageheadercolsort').hasClass('datasort') ? $('.storageheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#storageviewfieldids').val();
	userviewid = userviewid == '' ? '65' : userviewid;
	var filterid = $("#storagefilterid").val();
	var conditionname = $("#storageconditionname").val();
	var filtervalue = $("#storagefiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=counter&primaryid=counterid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#storageaddgrid').empty();
			$('#storageaddgrid').append(data.content);
			$('#storageaddgridfooter').empty();
			$('#storageaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('storageaddgrid');
			{//sorting
				$('.storageheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.storageheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.storageheadercolsort').hasClass('datasort') ? $('.storageheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.storageheadercolsort').hasClass('datasort') ? $('.storageheadercolsort.datasort').data('sortorder') : '';
					$("#storagesortorder").val(sortord);
					$("#storagesortcolumn").val(sortcol);
					var sortpos = $('#storageaddgrid .gridcontent').scrollLeft();
					storageaddgrid(page,rowcount);
					$('#storageaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('storageheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					storageaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#storagepgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					storageaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#storageaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#storagepgrowcount').material_select();
		},
	});
}
function storagecrudactionenable() {
	{//add icon click
		$('#addicon').click(function(e){
			resetFields();
			e.preventDefault();
			boxweighthideshow();
			maxquantityrequire();
			$('#storagetypeid').select2('val',2).trigger('change');
			var storagedisplay = $('#storagedisplay').val();
			if(storagedisplay == 1) {
				$('#countertypeid').select2('val',3).trigger('change');
			}else {
				$('#countertypeid').select2('val',storagedisplay).trigger('change');
			}
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			retrievestatus = 0;
			$('#storagedataupdatesubbtn').hide();
			$('#storagesavebutton').show();
			Materialize.updateTextFields();
			firstfieldfocus();
			$("#storageprimarydataid").val('');
		});
	}
	$("#editicon").click(function(e) {
		e.preventDefault();
		var datarowid = $('#storageaddgrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			resetFields();
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			$('#storagedataupdatesubbtn').show();
			$('#storagesavebutton').hide();
			retrievestatus = 1;
			boxweighthideshow();
			maxquantityrequire();
			storageeditformdatainformationshow(datarowid);
			Materialize.updateTextFields();
			$("#storagetypeid").prop("disabled", true); 
			firstfieldfocus();
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#deleteicon").click(function() {
		var datarowid = $('#storageaddgrid div.gridcontent div.active').attr('id');
		if(datarowid){		
			$("#storageprimarydataid").val(datarowid);
			$.ajax({ // checking whether this value is already in usage.
				url: base_url + "Base/multilevelmapping?level="+2+"&datarowid="+datarowid+"&table=counter&fieldid=parentcounterid&table1=product&fieldid1=counterid&table2=''&fieldid2=''",
				success: function(msg) { 
				if(msg > 0){
					alertpopup("Kindly delete the product linked to storage or delete the sub storage linked to storage.");						
				}else{
					combainedmoduledeletealert('storagedeleteyes');
					if(STORAGEDELETE == 0) {
						$("#storagedeleteyes").click(function(){
							var datarowid = $("#storageprimarydataid").val();
							storagerecorddelete(datarowid);
							//$(this).unbind();
						});
					}
						STORAGEDELETE =1;
				}
			},
			});
		} else {
			alertpopup("Please select a row");
		}	
	});
	//Storage print
	$("#printicon").click(function(){
		tagtemplatesload(7); //Tagtemplate function from Base JS
		var datarowid = $('#storageaddgrid div.gridcontent div.active').attr('id');
		var templateid = 0;
		if(datarowid) {
			$("#tagtemplateoverlay").fadeIn();
		} else {
			alertpopup(selectrowalert);
		}
	});
	//TagTemplate Overlay Submit
	$("#tagtempprintinprinter").click(function() {
		var datarowid = $('#storageaddgrid div.gridcontent div.active').attr('id');
		var templateid = $("#tagtemplateprview").find('option:selected').val();
		if(datarowid){
				$.ajax({
				url: base_url + "Storage/storagereprint?primaryid="+datarowid+"&templateid="+templateid,
				success: function(msg)  {
					var nmsg =  $.trim(msg);
					if (nmsg == 'DBERROR') {	
						alertpopup('Error on template query!!!Please choose other template!!!');
					}else if(nmsg == 'SUCCESS'){
						$("#tagtemplateoverlay").fadeOut();
						alertpopup('Processed');
					} else {
						alertpopup(msg);
					}
				},
			});
		} else {
			alertpopup(selectrowalert);
		}
	});
	//TagTemplate Overlay Cancel
	$("#tagtempprintcancel").click(function(){
		$("#tagtemplateoverlay").fadeOut();
	});
}
{//refresh grid
	function storagerefreshgrid() {
		var page = $('ul#storagepgnum li.active').data('pagenum');
		var rowcount = $('ul#storagepgnumcnt li .page-text .active').data('rowcount');
		storageaddgrid(page,rowcount);
	}
}
//new data add submit function
function storageaddformdata() {
	var formdata = $("#storageaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax( {
        url: base_url + "Storage/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				resetFields();
				storagerefreshgrid();
				setTimeout(function(){parenttreereloadfun(0);},500);
				$("#groupsectionoverlay").removeClass("effectbox");
				$("#groupsectionoverlay").addClass("closed");
				alertpopup(savealert);				
            }else if(nmsg == 'LEVEL') {
				alertpopup('Maximum level of sub storage reached !!! cannot create category further.');
			}
        },
    });
}
//old information show in form
function storageeditformdatainformationshow(datarowid) {
	var storageelementsname = $('#storageelementsname').val();
	var storageelementstable = $('#storageelementstable').val();
	var storageelementscolmn = $('#storageelementscolmn').val();
	var storageelementpartable = $('#storageelementspartabname').val();
	$.ajax(	{
		url:base_url+"Storage/fetchformdataeditdetails?storageprimarydataid="+datarowid+"&storageelementsname="+storageelementsname+"&storageelementstable="+storageelementstable+"&storageelementscolmn="+storageelementscolmn+"&storageelementpartable="+storageelementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				storagerefreshgrid();
			} else if((data.fail) == 'sndefault'){
				resetFields();
				$("#groupsectionoverlay").addClass("closed");
				alertpopup('Cannot Edit/Delete default records');
			} else {
				addslideup('taskview','taskaddform');				
				var txtboxname = storageelementsname + ',storageprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = storageelementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
				if(softwareindustryid == 3){
					$('#boxweight').val(parseFloat(data.boxweight).toFixed(round));
				}
				Materialize.updateTextFields();
			}
		}
	});
	parenttreereloadfun(datarowid);
	$('#storagedataupdatesubbtn').show().removeClass('hidedisplayfwg');
	$('#storagesavebutton').hide();
}
//udate old information
function storageupdateformdata() {
	var parentstorageid=$("#parentcounterid").val();
	var editstorageid=$("#storageprimarydataid").val();
		if(parentstorageid!=editstorageid){
			var formdata = $("#storageaddform").serialize();
			var amp = '&';
			var datainformation = amp + formdata;
		    $.ajax({
		        url: base_url + "Storage/datainformationupdate",
		        data: "datas=" + datainformation,
				type: "POST",
				cache:false,
		        success: function(msg) {
					var nmsg =  $.trim(msg);
		            if (nmsg == 'TRUE') {
						$('#storagedataupdatesubbtn').hide();
						$('#storagesavebutton').show();
						storagerefreshgrid();
						setTimeout(function(){parenttreereloadfun(0);},500);
						$("#groupsectionoverlay").removeClass("effectbox");
						$("#groupsectionoverlay").addClass("closed");
						resetFields();
						$("#storagetypeid").prop("disabled", false); 
						alertpopup(savealert);			
		            }else if(nmsg == 'FAILURE'){
						alertpopup("Check your Parent Storage mapping. ");
					}else if(nmsg == 'LEVEL') {
						alertpopup('Maximum level of sub storage reached !!! cannot create category further.');
					}
		        },
		    });
	}
	 else{
		alertpopup("Don't map with same storage");
	}
}
//udate old information
function storagerecorddelete(datarowid) {
	var storageelementstable = $('#storageelementstable').val();
	var storageelementspartable = $('#storageelementspartabname').val();
    $.ajax({
        url: base_url + "Storage/deleteinformationdata?storageprimarydataid="+datarowid+"&storageelementstable="+storageelementstable+"&storageparenttable="+storageelementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	storagerefreshgrid();
            	$("#parentcounterid").val("");
				$("#basedeleteoverlayforcommodule").fadeOut();
				setTimeout(function(){parenttreereloadfun(0);},500);
				alertpopup('Deleted successfully');
            } else if (nmsg == "Denied")  {
            	storagerefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Permission denied');
            }  else if(nmsg == "sndefault") {
				storagerefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Cannot Edit/Delete default records');
				$("#processoverlay").hide();
			}
        },
    });
}
{// Tree reload
	function parenttreereloadfun(datarowid)	{
		var tablenamespecial = "counter-counter.storagetypeid:3";
		var tablename = "counter";
		var mandfield = $('#treevalidationcheck').val();
		var fieldlab = $('#treefiledlabelcheck').val();
		var parentcounterid = $('#parentcounterid').val();
		$.ajax({
			url: base_url + "Storage/treedatafetchfun",
			data: "tabname="+tablenamespecial+'&mandval='+mandfield+'&fieldlabl='+fieldlab+'&rowid='+datarowid+'&primaryid='+parentcounterid,
			type: "POST",
			cache:false,
			success: function(data) {
				$('.'+tablename+'treediv').empty();
				$('.'+tablename+'treediv').append(data);
				$('#storagelistuldata li').click(function() {
					var level = parseInt($(this).attr('data-level'));
					var maxlevel = parseInt($('#maxcategorylevel').val());
					var name=$(this).attr('data-listname');
					var listid=$(this).attr('data-listid');
					$('#parentcounter').val(name);
					$('#parentcounterid').val(listid);
					if(level >= maxlevel){
						alertpopup('Maximum storage level reached');
						return false;
					}
				});
				$(function() {
					$('#dl-menu').dlmenu({
						animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
					});
				});
				if(retrievestatus == 1) {
					gettheparentcounter(datarowid);
				}
			},
		});
	}
}
//load the parent id
function gettheparentcounter(datarowid){
	$.ajax({               
		url:base_url+"Storage/getcounterparentid?id="+datarowid,
		dataType:'json',
		async:false,
		cache:false,
		success:function (data)	{
			$('#parentcounterid').val(data.parentcounterid);
			$('#parentcounter').val(data.countername);
		}
	});
}
function boxweighthideshow(){
	if(softwareindustryid == 3){
    	$('#boxweightdivhid').removeClass('hidedisplay');
		$('#boxweight').removeClass('validate[custom[number],decval[2],maxSize[15]] forsucesscls');
		$('#boxweight').addClass('validate[custom[number],decval['+round+'],maxSize[15]]');
    }else{
		$('#boxweightdivhid').addClass('hidedisplay');
	}
}
function maxquantityrequire(){
	$("span","#maxquantitydivhid label").remove();
	$("span","#boxweightdivhid label").remove();
	if(softwareindustryid == 3){
		var maxquanity = $('#storagequantity').val();
		if(maxquanity == 1){
			$('#maxquantity').removeClass('validate[custom[integer],maxSize[10]]').addClass('validate[required,custom[integer],maxSize[10]]');
			$('#maxquantitydivhid label').append('<span class="mandatoryfildclass">*</span>');
			$('#boxweight').removeClass('validate[custom[number],decval['+round+'],maxSize[15]]').addClass('validate[required,custom[number],decval['+round+'],maxSize[15]]');
			$('#boxweightdivhid label').append('<span class="mandatoryfildclass">*</span>');
		}else{
			$('#maxquantity').addClass('validate[custom[integer],maxSize[10]]').removeClass('validate[required,custom[integer],maxSize[10]]');
			$("span","#maxquantitydivhid label").remove();
			$('#boxweight').addClass('validate[custom[number],decval['+round+'],maxSize[15]]').removeClass('validate[required,custom[number],decval['+round+'],maxSize[15]]');
			$("span","#boxweightdivhid label").remove();
		}
	}
}
//storagenamecheck
function storagenamecheck() {
	var primaryid = $("#storageprimarydataid").val();
	var accname = $("#storagename").val();
	var elementpartable = $('#storageelementspartabname').val();
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Storage name already exists!";
				}
			}else {
				return "Storage name already exists!";
			}
		}
	}
}
//storageshortnamecheck
function storageshortnamecheck() {
	var primaryid = $("#storageprimarydataid").val();
	var accname = $("#storagesuffix").val();
	var fieldname = 'shortname';
	var elementpartable = $('#storageelementspartabname').val();
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniqueshortdynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Storage short name already exists!";
				}
			}else {
				return "Storage short name already exists!";
			}
		} 
	}
}
//RFID Tagnumber unique check
function rfidtagnocheck() {
	var primaryid = $("#storageprimarydataid").val();
	var accname = $("#rfidtagno").val();
	var elementpartable = 'counter';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Storage/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			datatype :"text",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "RFID Tagno already exists!";
				}
			} else {
				return "RFID Tagno already exists!";
			}
		} 
	}
}
function tagtemplatesload(viewfieldids) {
	$('#tagtemplateprview,#printtemplatepreview').empty();
	$('#tagtemplateprview,#printtemplatepreview').append($("<option value=''></option>"));
	$.ajax({
		url:base_url+'Base/tagtemplatesloadddval?dataname=tagtemplatename&dataid=tagtemplateid&datatab=tagtemplate&viewfieldids='+viewfieldids+"&column=printername",
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#tagtemplateprview,#printtemplatepreview')
					.append($("<option></option>")
					.attr("data-foldernameidhidden",data[index]['dataname'])
					.attr("data-printername",data[index]['printername'])
					.attr("data-moduleid",viewfieldids)
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}
			$('#tagtemplateprview,#printtemplatepreview').trigger('change');
		},
	});
}