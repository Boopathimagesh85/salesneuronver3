$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		chargeterritoriesaddgrid();
		firstfieldfocus();
		chargeterritoriescrudactionenable();
	}
	//hidedisplay
	$('#chargeterritoriesdataupdatesubbtn').hide();
	$('#chargeterritoriessavebutton').show();
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			chargeterritoriesaddgrid();
		});
	}
	{
		 $("#chargeterritoriesreloadicon").click(function(){
			clearform('cleardataform');
			chargeterritoriesrefreshgrid();
			$('#chargeterritoriesimagedisplay').empty();
			$('#chargeterritoriesdataupdatesubbtn').hide();
			$('#chargeterritoriessavebutton').show();
		});	
		$( window ).resize(function() {
			innergridresizeheightset('chargeterritoriesaddgrid');
			sectionpanelheight('chargeterritoriessectionoverlay');
		});		 
		//validation for  Add  
		$('#chargeterritoriessavebutton').click(function(e) {
			if(e.which == 1 || e.which === undefined) {
				masterfortouch = 0;
				$("#chargeterritoriesformaddwizard").validationEngine('validate');
			}
		});
		jQuery("#chargeterritoriesformaddwizard").validationEngine( {
			onSuccess: function() {
				$('#chargeterritoriessavebutton').attr('disabled','disabled');
				chargeterritoriesaddformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//update information
		$('#chargeterritoriesdataupdatesubbtn').click(function(e) {
			if(e.which == 1 || e.which === undefined) {
				$("#chargeterritoriesformeditwizard").validationEngine('validate');
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			}
		});
		jQuery("#chargeterritoriesformeditwizard").validationEngine({
			onSuccess: function() {
				$('#chargeterritoriesdataupdatesubbtn').attr('disabled','disabled');
				chargeterritoriesupdateformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	{//charge type fetch - direct/percentage
		$("#additionalchargetypeid").click(function(){
			var charge = $("#additionalchargetypeid").val();
			getchergetype(charge);
		});
	}
	{//filter work
		//for toggle
		$('#chargeterritoriesviewtoggle').click(function() {
			if ($(".chargeterritoriesfilterslide").is(":visible")) {
				$('div.chargeterritoriesfilterslide').addClass("filterview-moveleft");
			}else{
				$('div.chargeterritoriesfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#chargeterritoriesclosefiltertoggle').click(function(){
			$('div.chargeterritoriesfilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#chargeterritoriesfilterddcondvaluedivhid").hide();
		$("#chargeterritoriesfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#chargeterritoriesfiltercondvalue").focusout(function(){
				var value = $("#chargeterritoriesfiltercondvalue").val();
				$("#chargeterritoriesfinalfiltercondvalue").val(value);
			});
			$("#chargeterritoriesfilterddcondvalue").change(function(){
				var value = $("#chargeterritoriesfilterddcondvalue").val();
				$("#chargeterritoriesfinalfiltercondvalue").val(value);
				if( $('#s2id_chargeterritoriesfilterddcondvalue').hasClass('select2-container-multi') ) {
					chargeterritoriesmainfiltervalue=[];
					$('#chargeterritoriesfilterddcondvalue option:selected').each(function(){
					    var $ctvalue =$(this).attr('data-ddid');
					    chargeterritoriesmainfiltervalue.push($ctvalue);
						$("#chargeterritoriesfinalfiltercondvalue").val(chargeterritoriesmainfiltervalue);
					});
					$("#chargeterritoriesfilterfinalviewconid").val(value);
				} else {
					$("#chargeterritoriesfinalfiltercondvalue").val(value);
					$("#chargeterritoriesfinalfiltercondvalue").val(value);
				}
			});
		}
		chargeterritoriesfiltername = [];
		$("#chargeterritoriesfilteraddcondsubbtn").click(function() {
			$("#chargeterritoriesfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#chargeterritoriesfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(chargeterritoriesaddgrid,'chargeterritories');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset"){
			setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
//Documents Add Grid
function chargeterritoriesaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#chargeterritoriesaddgrid").width();
	var wheight = $("#chargeterritoriesaddgrid").height();
	var viewname = $('#dynamicdddataview').val();
	$('#viewgridheaderid').text(viewname);
	//col sort
	var sortcol = $("#chargeterritoriessortcolumn").val();
	var sortord = $("#chargeterritoriessortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.chargeterritoriesheadercolsort').hasClass('datasort') ? $('.chargeterritoriesheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.chargeterritoriesheadercolsort').hasClass('datasort') ? $('.chargeterritoriesheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.chargeterritoriesheadercolsort').hasClass('datasort') ? $('.chargeterritoriesheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#chargeterritoriesviewfieldids').val();
	if(userviewid != '') {
		userviewid= '73';
	}
	var filterid = $("#chargeterritoriesfilterid").val();
	var conditionname = $("#chargeterritoriesconditionname").val();
	var filtervalue = $("#chargeterritoriesfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=chargeterritories&primaryid=chargeterritoriesid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#chargeterritoriesaddgrid').empty();
			$('#chargeterritoriesaddgrid').append(data.content);
			$('#chargeterritoriesaddgridfooter').empty();
			$('#chargeterritoriesaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('chargeterritoriesaddgrid');
			{//sorting
				$('.chargeterritoriesheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.chargeterritoriesheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#chargeregionpgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.chargeterritoriesheadercolsort').hasClass('datasort') ? $('.chargeterritoriesheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.chargeterritoriesheadercolsort').hasClass('datasort') ? $('.chargeterritoriesheadercolsort.datasort').data('sortorder') : '';
					$("#chargeterritoriessortorder").val(sortord);
					$("#chargeterritoriessortcolumn").val(sortcol);
					var sortpos = $('#chargeterritoriesaddgrid .gridcontent').scrollLeft();
					chargeterritoriesaddgrid(page,rowcount);
					$('#chargeterritoriesaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('chargeterritoriesheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					chargeterritoriesaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#chargeregionpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					chargeterritoriesaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				//Material select
				$('#chargeregionpgrowcount').material_select();
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#chargeterritoriesaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
		},
	});	
}
function chargeterritoriescrudactionenable() {
	{//add icon click
		$('#chargeterritoriesaddicon').click(function(e){
			sectionpanelheight('chargeterritoriessectionoverlay');
			$("#chargeterritoriessectionoverlay").removeClass("closed");
			$("#chargeterritoriessectionoverlay").addClass("effectbox");
			e.preventDefault();
			resetFields();
			Materialize.updateTextFields();
			firstfieldfocus();
			$('#chargeterritoriesdataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#chargeterritoriessavebutton').show();
		});
	}
	$("#chargeterritoriesediticon").click(function(e){
		e.preventDefault();
		var datarowid = $('#chargeterritoriesaddgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			resetFields();
			$(".addbtnclass").addClass('hidedisplay');
			$(".updatebtnclass").removeClass('hidedisplay');
			$('#chargeterritoriesimagedisplay').empty();
			chargeterritoriesgetformdata(datarowid);
			Materialize.updateTextFields();
			firstfieldfocus();
			masterfortouch = 1;
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#chargeterritoriesdeleteicon").click(function(e){
		e.preventDefault();
		var datarowid = $('#chargeterritoriesaddgrid div.gridcontent div.active').attr('id');			
		if(datarowid){
			clearform('cleardataform');
			$(".addbtnclass").removeClass('hidedisplay');
			$(".updatebtnclass").addClass('hidedisplay');			
			combainedmoduledeletealert('chargeterritoriesdeleteyes');
			$("#chargeterritoriesdeleteyes").click(function(){
				var datarowid = $('#chargeterritoriesaddgrid div.gridcontent div.active').attr('id'); 
				chargeterritoriesrecorddelete(datarowid);
				$(this).unbind();
			});
		} else {
			alertpopup("Please select a row");
		}
	});
}
{//refresh grid
	function chargeterritoriesrefreshgrid() {
		var page = $(this).data('pagenum');
		var rowcount = $('ul#chargeregionpgnumcnt li .page-text .active').data('rowcount');
		chargeterritoriesaddgrid(page,rowcount);
	}
}
//new data add submit function
function chargeterritoriesaddformdata() {
    var formdata = $("#chargeterritoriesaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax( {
        url: base_url + "Chargeterritories/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	$(".addsectionclose").trigger("click");
				resetFields();
				chargeterritoriesrefreshgrid();
				$("#chargeterritoriessavebutton").attr('disabled',false);
				$('#chargeterritoriesimagedisplay').empty();
				alertpopup(savealert);			
            }
        },
    });
}
{//Addform section overlay close
	$(".addsectionclose").click(function(){
		resetFields();
		$("#chargeterritoriessectionoverlay").removeClass("effectbox");
		$("#chargeterritoriessectionoverlay").addClass("closed");
	});
}
//old information show in form
function chargeterritoriesgetformdata(datarowid) {
	var chargeterritorieselementsname = $('#chargeterritorieselementsname').val();
	var chargeterritorieselementstable = $('#chargeterritorieselementstable').val();
	var chargeterritorieselementscolmn = $('#chargeterritorieselementscolmn').val();
	var chargeterritorieselementpartable = $('#chargeterritorieselementspartabname').val();
	$.ajax(	{
		url:base_url+"Chargeterritories/fetchformdataeditdetails?chargeterritoriesprimarydataid="+datarowid+"&chargeterritorieselementsname="+chargeterritorieselementsname+"&chargeterritorieselementstable="+chargeterritorieselementstable+"&chargeterritorieselementscolmn="+chargeterritorieselementscolmn+"&chargeterritorieselementpartable="+chargeterritorieselementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data){
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				chargeterritoriesrefreshgrid();
			} else {
				sectionpanelheight('chargeterritoriessectionoverlay');
				$("#chargeterritoriessectionoverlay").removeClass("closed");
				$("#chargeterritoriessectionoverlay").addClass("effectbox");
				var txtboxname = chargeterritorieselementsname + ',chargeterritoriesprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = chargeterritorieselementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
				$("#chargeterritoriesfileuploadfromid").select2("val","");
				var image = data['chargeterritoriesimage'];
				if(image !="")
				{
					var id = data['fileuploadfromid'];
					if(id == 2){
						$('#chargeterritoriesimagedisplay').empty();
						var img = $('<img id="chargeterritoriesimagedynamic" style="height:100%;width:100%">');
						img.attr('src', base_url+image);
						img.appendTo('#chargeterritoriesimagedisplay');
						$('#chargeterritoriesimagedisplay').append('<i class="fa fa-times documentslogodownloadclsbtn"></i>');
						$("#chargeterritoriesfileuploadfromid").select2("val","");
						$(".documentslogodownloadclsbtn").click(function(){
							$(this).remove();
							$('#chargeterritoriesimagedisplay').empty();
							$('#chargeterritoriesimage').val('');
						});
					}
					else{
						$('#chargeterritoriesimagedisplay').empty();
						var img = $('<img id="chargeterritoriesimagedynamic" style="height:100%;width:100%">');
						img.attr('src',image);
						img.appendTo('#chargeterritoriesimagedisplay');
						$('#chargeterritoriesimagedisplay').append('<i class="fa fa-times documentslogodownloadclsbtn"></i>');
						$("#chargeterritoriesfileuploadfromid").select2("val","");
						$(".documentslogodownloadclsbtn").click(function(){
							$(this).remove();
							$('#chargeterritoriesimagedisplay').empty();
							$('#chargeterritoriesimage').val('');
						});
					}
				}
				else{
					$('#chargeterritoriesimagedisplay').empty();
					$("#chargeterritoriesfileuploadfromid").select2("val","");
				}
				$("#additionalchargetypeid").trigger('change');
			}
			
		}
	});
	$('#chargeterritoriesdataupdatesubbtn').show().removeClass('hidedisplayfwg');
	$('#chargeterritoriessavebutton').hide();
}
//udate old information
function chargeterritoriesupdateformdata() {
	var formdata = $("#chargeterritoriesaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Chargeterritories/datainformationupdate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				$('#chargeterritoriesdataupdatesubbtn').hide();
				$('#chargeterritoriessavebutton').show();
				$(".addsectionclose").trigger("click");
				resetFields();
				$("#chargeterritoriesdataupdatesubbtn").attr('disabled',false);
				chargeterritoriesrefreshgrid();
				$('#chargeterritoriesimagedisplay').empty();
				alertpopup(savealert);
				//for touch
				masterfortouch = 0;				
            }
        },
    });
	setTimeout(function() {
		var closetrigger = ["chargeterritoriescloseadd"];
		triggerclose(closetrigger);
	}, 1000);
}
//udate old information
function chargeterritoriesrecorddelete(datarowid) {
	var chargeterritorieselementstable = $('#chargeterritorieselementstable').val();
	var elementspartable = $('#chargeterritorieselementspartabname').val();
    $.ajax({
        url: base_url + "Chargeterritories/deleteinformationdata?chargeterritoriesprimarydataid="+datarowid+"&chargeterritorieselementstable="+chargeterritorieselementstable+"&chargeterritoriesparenttable="+elementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	chargeterritoriesrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Deleted successfully');
            }  else if (nmsg == "Denied")  {
            	chargeterritoriesrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Permission denied');
            }
        },
    });
}
//chargeterritoriesnamecheck
function chargeterritoriesnamecheck() {
	var primaryid = $("#chargeterritoriesprimarydataid").val();
	var accname = $("#chargeterritoriesname").val();
	var elementpartable = $('#chargeterritorieselementspartabname').val();
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "chargeterritories name already exists!";
				}
			}else {
				return "chargeterritories name already exists!";
			}
		} 
	}
}
//load the charges name dynamically after recent changes
function loadchargesname(){
	$('#additionalchargetypeid').empty();
	$('#additionalchargetypeid').append($("<option></option>"));
	$.ajax({
		url:base_url+'Chargeterritories/getchargename',
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++) {
					newddappend += "<option value = '" +data[m]['id']+ "'>"+data[m]['name']+"</option>";
				}
				//after run					
				$('#additionalchargetypeid').append(newddappend);
			}
		},
	});
}
function getchergetype(charge) {
	$.ajax({
		url:base_url+'Chargeterritories/getchergetype?chargeid='+charge,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				if(data == 3){
					$("#regionvalue").removeClass('validate[required,custom[number],decval[2],maxSize[100]] forsucesscls');
					$("#regionvalue").addClass('validate[required,custom[number],min[0],max[100],decval[2]]]');
				} else {
					$("#regionvalue").removeClass('validate[required,custom[number],min[0],max[100],decval[2]]]');
					$("#regionvalue").addClass('validate[required,custom[number],decval[2],maxSize[100]] forsucesscls');
				}
				
			}
		},
	});
}