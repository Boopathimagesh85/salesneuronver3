$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}
	{//date range function
		daterangefunction('fromdate','todate');
	}
	{// Grid Calling Function
		auditloggrid();
	}
	$('#addfilter').removeClass('hidedisplay');
	{//add icon click
		$('#addfilter').click(function(){
			$("#auditlogsectionoverlay").removeClass("closed");
			$("#auditlogsectionoverlay").addClass("effectbox");
		});
	}
	{//Addform section overlay close
		$(".addsectionclose").click(function(){
		   resetFields();
			$("#auditlogsectionoverlay").removeClass("effectbox");
			$("#auditlogsectionoverlay").addClass("closed");
		});
	}
	{//export history//excel & CSV
		$('#exporticon').click(function(){
			var type = $("#auditlogaction").val();
			var userid = $("#auditloguser").val();
			var moduleid = $("#auditmodule").val();
			var fromdate = $("#fromdate").val();
			var todate = $("#todate").val();
			var ExportPath = base_url+"Auditlog/exceldataexport";
			var form = $('<form action="'+ExportPath+'" class="hidedisplay" name="excelexport" id="excelexport" method="POST" target="_blank"><input name="type" id="type" value="'+type+'"><input name="userid" id="userid" value="'+userid+'"><input name="moduleid" id="moduleid" value="'+moduleid+'"><input name="fromdate" id="fromdate" value="'+fromdate+'"><input name="todate" id="todate" value="'+todate+'"></form>');
			$(form).appendTo('body');
			form.submit();
		});
	}
	{// Full Screen View
		$('#exitfulscrnlog').hide();
		$("#fulscrnlog").click(function(){
			$('#fulscrnlog').hide();
			$('#exitfulscrnlog').show();			
			$("#logviewarea").removeClass('large-9').removeClass('medium-9').addClass('large-12');
			$("#leftpanearea").removeClass('large-3').addClass('hidedisplay');
			auditloggrid();
		});
		$("#exitfulscrnlog").click(function(){
			$('#exitfulscrnlog').hide();
			$('#fulscrnlog').show();
			$("#logviewarea").removeClass('large-12').addClass('large-9').addClass('medium-9');
			$("#leftpanearea").removeClass('hidedisplay').addClass('large-3');
			auditloggrid();			
		});
	}
	//filter
	$("#auditlogfiltersubbtn").click(function() {
		$("#processoverlay").show();
		setTimeout(function() {
			auditloggrid();
			$("#processoverlay").hide();
		},100);
	});
	//reload icon
	$("#reloadicon").click(function(){
		$("#fromdate").val('');
		$("#todate").val('');
		$("#auditlogaction").select2('val',1);
		$("#auditloguser").select2('val','');
		$("#auditmodule").select2('val','');
		$("#processoverlay").show();
		setTimeout(function() {
			auditloggrid();
			$("#processoverlay").hide();
		},100);
	});
	$(window).resize(function() {
		maingridresizeheightset('auditloggrid');
	});
	{
		$('#fromdate').datetimepicker({
			dateFormat: 'dd-mm-yy',
			showTimepicker :false,
			minDate: null,
			maxDate: 0,
			onSelect: function () {
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
					$(this).prev('div').remove();
				}
			},
			onClose: function () {
				$('#fromdate').focus();
			}
		}).click(function() {
			$(this).datetimepicker('show');
		});
		Materialize.updateTextFields();
		$('#fromdate').datetimepicker('setDate', '' );
		$('#todate').datetimepicker({
			dateFormat: 'dd-mm-yy',
			showTimepicker :false,
			minDate: null,
			maxDate: 0,
			onSelect: function () {
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
					$(this).prev('div').remove();
				}
			},
			onClose: function () {
				$('#todate').focus();
			}
		}).click(function() {
			$(this).datetimepicker('show');
		});
		$('#todate').datetimepicker('setDate', '' );
	}
	$(window).resize(function() {
		maingridresizeheightset('auditloggrid');
	});
	$('#groupcloseaddform').click(function(){
		window.location =base_url+'Employee';
	});
	$(".notiredirecturl").click(function(){
		var redirecturl = $(this).attr('data-redirecturl');
		var datarowid = $(this).attr('data-datarowid');
		sessionStorage.setItem("datarowid",datarowid);
		window.location = redirecturl;	
	});
});
{// Auditlog View Grid
	function auditloggrid(page,rowcount) {
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 30 : rowcount;
		var wwidth = $("#auditloggrid").width();
		var wheight = $(window).height();
		/*col sort*/
		var sortcol = $("#auditlogsortcolumn").val();
		var sortord = $("#auditlogsortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.auditlogheadercolsort').hasClass('datasort') ? $('.auditlogheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.auditlogheadercolsort').hasClass('datasort') ? $('.auditlogheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.auditlogheadercolsort').hasClass('datasort') ? $('.auditlogheadercolsort.datasort').attr('id') : '0';
		//action based
		var action = $("#auditlogaction").val();
		//user id
		var userid = $("#auditloguser").val();
		//module based
		var moduleid = $("#auditmodule").val();
		//from date based
		var fromdate = $("#fromdate").val();
		//todate based
		var todate = $("#todate").val();
		$.ajax({
			url:base_url+"Auditlog/auditloginformationshow?action="+action+"&userid="+userid+"&moduleid="+moduleid+"&maintabinfo=notificationlog&primaryid=notificationlogid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+"&sortcol="+sortcol+"&sortord="+sortord+"&todate="+todate+"&fromdate="+fromdate,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#auditloggrid').empty();
				$('#auditloggrid').append(data.content);
				$('#auditloggridfooter').empty();
				$('#auditloggridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('auditloggrid');
				//row header grouping
				datarowheadergroup('auditloggrid');
				{//sorting
					$('.auditlogheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.auditlogheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#auditlogpgnumcnt li .page-text .active').data('rowcount');
						var sortcol = $('.auditlogheadercolsort').hasClass('datasort') ? $('.auditlogheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.auditlogheadercolsort').hasClass('datasort') ? $('.auditlogheadercolsort.datasort').data('sortorder') : '';
						$("#auditlogsortorder").val(sortord);
						$("#auditlogsortcolumn").val(sortcol);
						auditloggrid(page,rowcount);
						$("#processoverlay").hide();
					});
					sortordertypereset('auditlogheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						auditloggrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#auditlogpgrowcount').change(function(){
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('#prev').data('pagenum');
						auditloggrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//Material select
				$('#auditlogpgrowcount').material_select();
			},
		});
	}
}
{//date Range
	function daterangefunction(generalstartdate,generatenddate){ 
		var dateformetdata = $('#'+generalstartdate+'').attr('data-dateformater');
		var startDateTextBox = $('#'+generalstartdate+'');
		var endDateTextBox = $('#'+generatenddate+'');
		startDateTextBox.datetimepicker({ 
			timeFormat: 'HH:mm z',
			dateFormat: dateformetdata,
			minDate:null,
			maxDate:0,
			onClose: function(dateText, inst) {
				if (endDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						endDateTextBox.datetimepicker('setDate', testStartDate);
				}
				else {
					endDateTextBox.val(dateText);
				}
				$('#'+generalstartdate+'').focus();
			},
			onSelect: function (selectedDateTime) {
				endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
					$(this).prev('div').remove();
					$('.hasDatepicker').removeClass('error');
				}
			}
		}).click(function() {
			$(this).datetimepicker('show');
		});
		endDateTextBox.datetimepicker({
			timeFormat: 'HH:mm z',
			dateFormat: dateformetdata,
			minDate:null,
			maxDate:0,
			onClose: function(dateText, inst) {
				if (startDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						startDateTextBox.datetimepicker('setDate', testEndDate);
				} else {
					startDateTextBox.val(dateText);
				}
				$('#'+generatenddate+'').focus();
			},
			onSelect: function (selectedDateTime){
				startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
			}
		}).click(function() {
			$(this).datetimepicker('show');
		});
	}
}