$(document).ready(function() {
	$('#invoicesproupdatebutton,#invoicespayupdatebutton').removeClass('updatebtnclass');
	$('#invoicesproupdatebutton,#invoicespayupdatebutton').removeClass('hidedisplayfwg');
	$('#invoicesproaddbutton,#invoicespayaddbutton').removeClass('addbtnclass');
	$("#invoicesproupdatebutton,#invoicespayupdatebutton").hide();
	PAYMETHOD ='ADD';
	METHOD ='ADD';
	PRECISION = 0;
	PRODUCTTAXABLE ='Yes';
	QUANTITY_PRECISION = 0;
	PRICBOOKCONV_VAL = 0; 
	COMPANY_NAME = '';
	CONV_ARR = [];
	MODULEID = 0;
	UPDATEID = 0;
	PAYUPDATEID = 0;
    {	
		// Foundation Initialization
        $(document).foundation();
		// Maindiv height width change
        maindivwidth();       
    }
    $('#currencyconverticon').removeClass("hidedisplay")
	{//module showhides
		$("#moduleid").select2('val','').trigger('change');
		$("#moduleid option").addClass("ddhidedisplay").addClass("ddclose");
		if(softwareindustryid == 1){
			$("#moduleid option[value=216]").removeClass("ddhidedisplay").removeClass("ddclose");
			$("#moduleid option[value=217]").removeClass("ddhidedisplay").removeClass("ddclose");
		}else if(softwareindustryid == 2){
			$("#moduleid option[value=94]").removeClass("ddhidedisplay").removeClass("ddclose");
			$("#moduleid option[value=96]").removeClass("ddhidedisplay").removeClass("ddclose");
		}else if(softwareindustryid == 4){
			$("#moduleid option[value=85]").removeClass("ddhidedisplay").removeClass("ddclose");
			$("#moduleid option[value=87]").removeClass("ddhidedisplay").removeClass("ddclose");
		}
	}
    {
		var editorname = $('#editornameinfo').val();
		froalaarray=[editorname,'html.set',''];
	}
	{// field break
		$("<div class='row'></div>").insertAfter("#paymentstatusiddivhid");
	}
	{//Enables the Edit Buttons
		$('#invoicesproingrideditspan1').removeAttr('style');
		$('#invoicespayingrideditspan2').removeAttr('style');
	}
	{// Shipping billing address details fetching
		$("#billingaddresstype,#shippingaddresstype").change(function(){
			var id=$(this).attr('id');
			setaddress(id);
		});
		//shipping /billing hiding
		$("#billingaddresstype option[value='6']").remove();
		$("#shippingaddresstype option[value='5']").remove();
	}
	//buttoon hide
	$("#invoicesproupdatebutton").hide();
	$("#invoicespayupdatebutton").hide();
	{// Grid Calling
        leadinvoicegrid();
        getmoduleid();
        //crud action
		crudactionenable();
    }
	$(".dropdownchange").change(function(){
		var dataattrname = ['dataidhidden'];
		var dropdownid =['employeeidddid'];
		var textboxvalue = ['employeetypeid'];
		var selectid = $(this).attr('id');
		var index = dropdownid.indexOf(selectid);
		var selected=$('#'+selectid+'').find('option:selected');
		var datavalue=selected.data(''+dataattrname[index]+'');
		$('#'+textboxvalue[index]+'').val(datavalue);
		if(selectid == 'employeeidddid') {
			var selected=$('#'+selectid+'').find('option:selected');
			var datavalue=selected.data('ddid');
			$('#employeeid').val(datavalue);
		}
	});
	{// Formclear resetting 
		$("#formclearicon").click(function(){
            var elementname = $('#elementsname').val();
		    elementdefvalueset(elementname);
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			setdefaultproperty();
			$('#paymentto').val(COMPANY_NAME);			
			//cleargrid data
			cleargriddata('invoicesproaddgrid1');
			cleargriddata('invoicespayaddgrid2');
			froalaset(froalaarray);
			METHOD = 'ADD';
			PAYMETHOD ='ADD';
			//empty option of depedency dropdowns
			$('#opportunityid,#contactid').empty();
		});
	}	
	{//paymenttypeid
		$("#paymenttypeid").change(function(){
			var val = $(this).val();
			if(checkValue(val) == true){				
				var amount = $('#balanceamount').val();
				if(val == 3){ //full
					$("#paymentamount").val(amount);
					Materialize.updateTextFields();
				}
				else{
					$("#paymentamount").val('');
				}
			}
		});
		//payment methods
		$("#paymentmethodid").change(function(){
			var val = $(this).val();
			if(checkValue(val) == true){				
				if(val == 2){ //cash
					$('#banknamedivhid,#referencedatedivhid').hide();					
				} else { // neft-rtgs-dd-cheque-ecs-c.card-d.card
					$('#banknamedivhid,#referencedatedivhid').show();
				}
			}
		});
		//writeoffamount
		$("#writeoffamount").focusout(function(){
			if(isNaN($("#writeoffamount").val())){
				$("#writeoffamount").val(0);
			}
			var val = parseFloat($(this).val());
			if(val > 0 || val == 0){
				gridsummeryvaluefetch('','','');
			} else {
				$("#writeoffamount").val(0);
			}
		});
	}
	{//module id changes events
		$("#moduleid").change(function(){
			var val =$(this).val();
			if(val > 0){	
				var invoicedate = $('#invoicedate').val();
				var invoicenumber = $('#invoicenumber').val();
				var invoicetype = $('#ordertypeid').val();				
				var modulenumber = $('#modulenumber').val();
				if(checkValue(modulenumber) == true){ //reset the elements ony if any numbers are set
					$("#formclearicon").trigger('click');
					$('#moduleid').select2('val',val);
					$('#invoicedate').val(invoicedate);
					$('#invoicenumber').val(invoicenumber);
					$('#ordertypeid').select2('val',invoicetype);
				}
			} else {
				cleargriddata('modulenumbersearchgrid');
			}			
		});
		//mnsearchsubmit
		$("#mnsearchsubmit").click(function(){
			var datarowid = $('#modulenumbersearchgrid div.gridcontent div.active').attr('id');
			if(datarowid){
				var sourcemodule = $('#moduleid').val();
				var prevmodulenumber = $('#modulenumber').val();
				if($.inArray(sourcemodule, ["216","85","94"]) != -1) {
					var number =  getgridcolvalue('modulenumbersearchgrid',datarowid,'quotenumber','');
				}
				if($.inArray(sourcemodule, ["217","87","96"]) != -1) {
					var number =  getgridcolvalue('modulenumbersearchgrid',datarowid,'salesordernumber','');
				}	
				$('#modulenumber').val(number);
				$('#mnsearchoverlay').fadeOut('slow');				
				var destinatemodule =MODULEID;
				//data clear
				//Reset the invoice elements				
				var invoicedate = $('#invoicedate').val();
				var invoicenumber = $('#invoicenumber').val();
				var invoicetype = $('#ordertypeid').val();
					if((prevmodulenumber != number) ||(checkValue(prevmodulenumber) == false)){ //reset the elements ony if any numbers are set
						$("#formclearicon").trigger('click');
						$('#moduleid').select2('val',sourcemodule);
						$('#invoicedate').val(invoicedate);
						$('#invoicenumber').val(invoicenumber);
						$('#modulenumber').val(number);
						$('#ordertypeid').select2('val',invoicetype);
						//dataretrievals
						conversiondatamapping(datarowid,sourcemodule,destinatemodule);
						$('#ordertypeid,#accountid,#opportunityid,#contactid').attr('readonly','readonly');	
						Materialize.updateTextFields();
					}							
					cleargriddata('modulenumbersearchgrid');//unselect the selected row
			} else {
				alertpopup('Please select row');
			}
		});
	}	
	{
		$("#territoryid").change(function(){
			var id = $(this).val();
			if(id){
				$("#pricebookid").empty();
				$.ajax({
					url: base_url+"Base/getpricebookdetail?territoryid="+id,
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						newoptions="<option></option>";
						$.each(data, function(index) {
							newoptions += "<option data-pricebookidhidden ='"+data[index]['name']+"' value ='"+data[index]['id']+"'>"+data[index]['name']+"</option>";								
						});
						$('#pricebookid').append(newoptions);
						$('#pricebookid').select2('val','').trigger('change');
					},
				});
			}else{
				$("#pricebookid").empty();
				$.ajax({
					url: base_url+"Base/getpricebookdetail?territoryid="+id,
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						$("#pricebookid").empty();
						newoptions="<option></option>";
						$.each(data, function(index) {
							newoptions += "<option data-pricebookidhidden ='"+data[index]['name']+"' value ='"+data[index]['id']+"'>"+data[index]['name']+"</option>";							
						});
						$('#pricebookid').append(newoptions);
						$('#pricebookid').select2('val','').trigger('change');
					},
				});
			}
		});
		// Account change	
		$("#accountid").change(function()
		{
			var accountid=$(this).val();
			getaddress(accountid,'account','Billing');		
			getaddress(accountid,'account','Shipping');
			Materialize.updateTextFields();
			$('#billingaddresstype,#shippingaddresstype').select2('val','2');
			//get dependent values
			var id=$(this).val();
			if(id != ''){			
				//opportunity
				$('#opportunityid').empty();
				var table = 'opportunity';				
				$.ajax({
				url:base_url+'Quote/accountdependddata?table='+table+'&id='+id,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
				  if((data.fail) == 'FAILED') {
				  } else {
						newoptions="";
						$.each(data, function(index) {
							newoptions += "<option value ='"+data[index]['id']+"'>"+data[index]['name']+"</option>";							
						});
				  }
				  $('#opportunityid').append(newoptions);
				  $('#opportunityid').select2('val','').trigger('change');
				},
				});
				//opportunity
				$('#contactid').empty();
				var table = 'contact';				
				$.ajax({
				url:base_url+'Quote/accountdependddata?table='+table+'&id='+id,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
				  if((data.fail) == 'FAILED') {
				  } else {
					    newoptions="";
						$.each(data, function(index) {
							newoptions += "<option value ='"+data[index]['id']+"'>"+data[index]['name']+"</option>";
						});
				  }
				  $('#contactid').append(newoptions);		
				  $('#contactid').select2('val','').trigger('change');
				},
				});	
			}else{				
				//opportunity
				$('#opportunityid').empty();
				var table = 'opportunity';				
				$.ajax({
				url:base_url+'Quote/accountdependddata?table='+table+'&id='+id,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
				  if((data.fail) == 'FAILED') {
				  } else {
						newoptions="";
						$.each(data, function(index) {
							newoptions += "<option value ='"+data[index]['id']+"'>"+data[index]['name']+"</option>";							
						});
				  }
				  $('#opportunityid').append(newoptions);
				  $('#opportunityid').select2('val','').trigger('change');
				},
				});
				//opportunity
				$('#contactid').empty();
				var table = 'contact';				
				$.ajax({
				url:base_url+'Quote/accountdependddata?table='+table+'&id='+id,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
				  if((data.fail) == 'FAILED') {
				  } else {
					    newoptions="";
						$.each(data, function(index) {
							newoptions += "<option value ='"+data[index]['id']+"'>"+data[index]['name']+"</option>";
						});
				  }
				  $('#contactid').append(newoptions);	
				  $('#contactid').select2('val','').trigger('change');
				},
				});				
			}
		});		
	}
	if(softwareindustryid == 2 || softwareindustryid == 4){
		$("#contactid").change(function(){
			var contactid=$(this).val();
			if(checkValue(contactid) == true){
				$('#patientid').val('');
				$('#streetname').val('');
				$('#billingaddress').val('');
				$('#billingcity').val('');
				$('#billingpincode').val('');
				$('#billingstate').val('');	
				$('#mobilenumber').val('');
				$('#email').val('');														
				$('#insurancenameid').select2('val','');
				$.ajax({
					url:base_url+'Base/fetchcontactvalue?contactid='+contactid,
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						$('#patientid').val(data.contactnumber);
						$('#streetname').val(data.streetname);
						$('#billingaddress').val(data.primaryarea);
						$('#billingcity').val(data.primarycity);
						$('#billingpincode').val(data.primarypincode);
						$('#billingstate').val(data.primarystate);
						$('#billingcountry').val(data.country);	
						$('#mobilenumber').val(data.landlinenumber);
						$('#email').val(data.emailid);														
						$('#insurancenameid').select2('val',data.insurancenameid);
						Materialize.updateTextFields();
					}
				});	
			}
		});
	}
	{//pricebooks		
		$("#pricebookid").change(function()
		{			
			var val= $(this).val();
			var currency = $('#currencyid').val();
			if(checkValue(val) == true){
				var product = $('#productid').val();
				if(product != ''|| product != null){
					$("#productid").select2('val','');
					$('#instock,#quantity,#unitprice,#sellingprice,#grossamount,#pretaxtotal,#chargeamount,#discountamount,#taxamount,#netamount').val('');
				}
				$.ajax({
				url:base_url+'Quote/pricebookcurrency?pricebook='+val+'&currency='+currency,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if(data.status != 'FAILED'){
						if(currency == data.currencyid){
						} else  {	
							var conversionrate = data.rate; //to get conversion rate
							$('#currentcurrency').select2('val',currency).trigger('change').attr("disabled","disabled");
							$('#pricebookcurrency').select2('val',data.currencyid).trigger('change').attr("disabled","disabled");		
							$('#pricebook_currencyconv').val(conversionrate);								
							$('#pricebook_currencyconv').focus();							
							$("#pricebookcurrencyconvoverlay").fadeIn();
							Materialize.updateTextFields();
						}
					}
				},
				});
				$("#pricebookclickevent").hide();
			}else{
				$("#pricebookclickevent").show();
				var product = $('#productid').val();
				if(product != ''|| product != null){
					$("#productid").select2('val','');
					$('#instock,#quantity,#unitprice,#sellingprice,#grossamount,#pretaxtotal,#chargeamount,#discountamount,#taxamount,#netamount').val('');
				}
			}
		});	
	}	
	{// For touch
		fortabtouch = 0;
	}
	{// Toolbar click function Reload,Summary,Formtogrid
		$("#cloneicon").click(function(){
			QUANTITY_PRECISION =0;
			var datarowid = $('#leadinvoicegrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#processoverlay").show();
		        invoicesproaddgrid1();
				invoicespayaddgrid2();
				// Append The Currency data
				appendcurrency('currencyid');
				invoiceclonedatafetchfun(datarowid);
				$('#pricebookclickevent').show;
				$('#paymentto').val(COMPANY_NAME);
				//for autonumber
				randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
				showhideiconsfun('editclick','invoicecreationformadd');
				$("#invoicesproingriddel1,#invoicespayingriddel2").show();
				$(".fr-element").attr("contenteditable", 'true');
				$("#pricebookclickevent,#productsearchevent").show();
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				Operation = 0; //for pagination
			} else {
				alertpopup("Please select a row");			
			}
		});
		//cancel data
		$("#cancelno").click(function(){
			$("#basecanceloverlay").fadeOut();
		});
		$("#cancelyes").click(function(){
			var datarowid = $('#leadinvoicegrid div.gridcontent div.active').attr('id');
			var cancelresult=checkcancelstatus(MODULEID,'invoice',datarowid);
			if(cancelresult == true)
			{
			$.ajax({
			url: base_url + "Invoice/canceldata?primarydataid="+datarowid+"&invoicemodule="+MODULEID,
			cache:false,
			success: function(msg)  {
				var nmsg =  $.trim(msg);
				if (nmsg == true) {
					$("#basecanceloverlay").fadeOut();
					refreshgrid();					
				} 
				else if (nmsg == "false") {
				}
			},
		  });
		  }
		  else{
			alertpopup("This record cannot be Cancelled");
		  }
		});
		$("#cancelicon").click(function(){
			var datarowid = $('#leadinvoicegrid div.gridcontent div.active').attr('id');
			if(datarowid){
				var stage = invoicestage(datarowid);
			    if(stage == 38){
					alertpopup("Invoice Booked!!!Cannot Change To Cancel");
			    }	
				else if(stage == 39){
					$('#cancelmessage').text("Record Already Cancelled.Do u want revert to Draft?");
					$("#basecanceloverlay").fadeIn();				
					$("#cancelyes").focus();
			    } 				
				else {
					$('#cancelmessage').text("Do You Want To Cancel This Record?");
					$("#basecanceloverlay").fadeIn();				
					$("#cancelyes").focus();
			    }				
			}
			else {
				alertpopup("Please select a row");
			}		
		});
		$("#bookedicon").click(function(){
			var datarowid = $('#leadinvoicegrid div.gridcontent div.active').attr('id');
			if(datarowid){
				var stage = invoicestage(datarowid);
			    if(stage == 38){
					$('#bookedmessage').text("Invoice Already Booked.Do u want revert to Draft?");
					$("#bookedoverlay").fadeIn();
					$("#bookedyes").focus();
			    } else  if(stage == 37){
					$('#bookedmessage').text("Book Invoice?");
					$("#bookedoverlay").fadeIn();
					$("#bookedyes").focus();
			    } else {
					alertpopup("Invoice Can't Change to Booked Stage.");
				}				
			}
			else {
				alertpopup("Please select a row");
			}	
		});
		$("#bookedyes").click(function(){
			var datarowid = $('#leadinvoicegrid div.gridcontent div.active').attr('id');
			$.ajax({
				url: base_url + "Invoice/bookinvoice?primarydataid="+datarowid+"&invoicemodule="+MODULEID,
				cache:false,
				success: function(msg)  {
					var nmsg =  $.trim(msg);
					if (nmsg == true) {
						$("#bookedoverlay").fadeOut();
						refreshgrid();					
					} 					
				},
			});		
		});	
		$("#reloadicon").click(function(){
			refreshgrid();
			froalaset(froalaarray);
		});
		$( window ).resize(function() {
			maingridresizeheightset('leadinvoicegrid');
			sectionpanelheight('invoicesprooverlay');
		});
		{//inner-form-with-grid
			$("#invoicesproingridadd1").click(function(){
				sectionpanelheight('invoicesprooverlay');
				$("#invoicesprooverlay").removeClass("closed");
				$("#invoicesprooverlay").addClass("effectbox");
				Materialize.updateTextFields();
				firstfieldfocus();
			});
			$("#invoicesprocancelbutton").click(function(){
				clearform('gridformclear');
				$("#invoicesprooverlay").removeClass("effectbox");
				$("#invoicesprooverlay").addClass("closed");
				$('#invoicesproupdatebutton').hide();	//hide the UPDATE button(inner - productgrid)
				$('#invoicesproaddbutton').show();//display the ADD button(inner-productgrid)*/	
			});
			$("#invoicespayingridadd2").click(function(){
				clearform('gridformclear');
				sectionpanelheight('invoicespayoverlay');
				$("#invoicespayoverlay").removeClass("closed");
				$("#invoicespayoverlay").addClass("effectbox");
				Materialize.updateTextFields();
				firstfieldfocus();
			});
			$("#invoicespaycancelbutton").click(function(){
				clearform('gridformclear');
				$("#invoicespayoverlay").removeClass("effectbox");
				$("#invoicespayoverlay").addClass("closed");
				$('#invoicespayupdatebutton').hide();	//hide the UPDATE button(inner - productgrid)
				$('#invoicespayaddbutton').show();//display the ADD button(inner-productgrid)*/	
			});
		}
		$("#detailedviewicon").click(function(){
			var datarowid = $('#leadinvoicegrid div.gridcontent div.active').attr('id');
			if (datarowid) {				
				//Function Call For Edit
				$("#processoverlay").show();
		        invoicesproaddgrid1();
				invoicespayaddgrid2();
				// Append The Currency data
				appendcurrency('currencyid');
				froalaset(froalaarray);
				invoiceeditdatafetchfun(datarowid);
				showhideiconsfun('summryclick','invoicecreationformadd');
				$("#invoicesproingriddel1,#invoicespayingriddel2").hide();
				$("#invoicesproingridedit1,#invoicespayingridedit2").hide();
				$(".froala-element").css('pointer-events','none');
				$("#pricebookclickevent,#productsearchevent").hide();
				$(".fr-element").attr("contenteditable", 'false');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				setTimeout(function() {
					 $('#tabgropdropdown').select2('enable');
				 },50);
			} else {
				alertpopup("Please select a row");
			}
			Materialize.updateTextFields();
		});
		{// Redirected form notification overlay and widget and audit log
			var rdatarowid = sessionStorage.getItem("datarowid");		
			if(rdatarowid != '' && rdatarowid != null) {
				 setTimeout(function() {
					$("#processoverlay").show();
			        invoicesproaddgrid1();
					invoicespayaddgrid2();
					// Append The Currency data
					appendcurrency('currencyid');
					froalaset(froalaarray);
					invoiceeditdatafetchfun(rdatarowid);
					showhideiconsfun('summryclick','invoicecreationformadd');
					sessionStorage.removeItem("datarowid");
					Materialize.updateTextFields();
				},50);
			}
		}
		$("#editfromsummayicon").click(function(){		
			var datarowid = $('#leadinvoicegrid div.gridcontent div.active').attr('id');
			if (datarowid) {
			var stage=invoicestage(datarowid);
				if(stage == 38){ //Booked
					alertpopup("This Invoice is Booked !!!");
				} else if(stage == 39){ //cancel
					alertpopup("This Invoice is Cancelled");
				} else if(stage == 37){ //draft				
					showhideiconsfun('editfromsummryclick','invoicecreationformadd');
					$("#invoicesproingriddel1,#invoicespayingriddel2").show();
					$("#invoicesproingridedit1,#invoicespayingridedit2").show();
					$("#pricebookclickevent,#productsearchevent").show();
					$(".fr-element").attr("contenteditable", 'true');
					$(".froala-element").css('pointer-events','auto');
				}else{
					alertpopup("Invoice Status is mandatory. Kindly update status using Edit Action");
				}
			}
		});
		//send mail
		//mail validate-
		$("#mailicon").click(function(){
			var datarowid = $('#leadinvoicegrid div.gridcontent div.active').attr('id');
			var accountid = getaccountid(datarowid,'invoice','accountid');
			if (datarowid) 
			{
				var validemail=validateemail(accountid,'account','emailid');
				if(validemail == true){
					var emailtosend = getvalidemailid(accountid,'account','emailid','invoice',datarowid);
					sessionStorage.setItem("forcomposemailid",emailtosend.emailid);
					sessionStorage.setItem("forsubject",emailtosend.subject);
					sessionStorage.setItem("moduleid",MODULEID);
					sessionStorage.setItem("recordid",datarowid);
					var fullurl = 'erpmail/?_task=mail&_action=compose';
					window.open(''+base_url+''+fullurl+'');
				}
				else{
					alertpopup("Invalid email address");
				}
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#invoicesproingriddel1").click(function(){
			var datarowid = $('#invoicesproaddgrid1 div.gridcontent div.active').attr('id');
			if(datarowid)
			{		
				if(METHOD == 'UPDATE'){
					alertpopup("A Record Under Edit Form");
				}
				else{					
					deletegriddatarow('invoicesproaddgrid1',datarowid);
					var productrecords = $('#invoicesproaddgrid1 .gridcontent div.data-content div');
					var productrecords = productrecords.length;
					var currentmode = $('#currentmode').val();
					if(productrecords == 0 )
					{
						$('#ordertypeid,#taxmasterid,#additionalchargecategoryid,#pricebookid').select2('val','');
						$('#ordertypeid,#taxmasterid,#additionalchargecategoryid,#pricebookid').select2("readonly", false);	
						$('#ordertypeid,#taxmasterid,#additionalchargecategoryid,#pricebookid').trigger('change');
						PRICBOOKCONV_VAL = 0;
						if(softwareindustryid != 2){
							$("#productid").empty();
						}
						//Empty the Tax/Charge data-because its dependent
						$('#grouptaxgriddata,#groupchargegriddata,#grouptaxamount,#groupchargeamount').val('');
						$("#pricebookclickevent").show();
					}
				gridsummeryvaluefetch('','','');
				}
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#invoicespayingriddel2").click(function(){
			var datarowid = $('#invoicespayaddgrid2 div.gridcontent div.active').attr('id');		
			if(datarowid){
				if(PAYMETHOD == 'UPDATE'){
					alertpopup("A Record Under Edit Form");
				}
				else {
					deletegriddatarow('invoicespayaddgrid2',datarowid);
					gridsummeryvaluefetch('','','');
				}
			}
			else {
				alertpopup("Please select a row");
			}
		});
	}	
    {// Close Add Screen
        var addcloseacccreation =["closeaddform","invoicecreationview","invoicecreationformadd"]
        addclose(addcloseacccreation);
		var addcloseviewcreation =["viewcloseformiconid","invoicecreationview","viewcreationformdiv",""];
		addclose(addcloseviewcreation);
    }
	{// View by drop down change
        $('#dynamicdddataview').change(function(){
            leadinvoicegrid();
        });
    }
	{// Validation for Invoice Add  
		$('#dataaddsbtn').click(function(e){
			$('#dataaddsbtn').addClass('singlesubmitonly');
			$('.ftab').trigger('click');
			$('#quantity').removeClass('validate[required,custom[number],decval[3],maxSize[100]]');
			$('#sellingprice').removeClass('validate[required,custom[number],decval[7],maxSize[100]]');
			$('#netamount').removeClass('validate[required,custom[number],decval[7],maxSize[100]]');
			$('#productid').removeClass('validate[required]');
			$('#grossamount').removeClass('validate[required,custom[number],decval[7],maxSize[100]]');
			$('#paymentdate').removeClass('validate[required]');
			$('#paymenttypeid').removeClass('validate[required]');
			$('#paymentmethodid').removeClass('validate[required]');
			$('#paymentamount').removeClass('validate[required,custom[number],decval[2],maxSize[100]]');
			if(softwareindustryid == 4){
				$('#joiningdate').removeClass('validate[required]');
				$('#expirydate').removeClass('validate[required]');
			}
			$("#formaddwizard").validationEngine('validate');
		});
		jQuery("#formaddwizard").validationEngine({
			onSuccess: function() {
				if(deviceinfo != 'phone'){
					var getrownumber = $('#invoicesproaddgrid1 .gridcontent div.data-content div').length;
				}else{
					var getrownumber = $('#invoicesproaddgrid1 .gridcontent div.wrappercontent div').length;
				}				
				if(getrownumber != 0){
					$("#processoverlay").show();
					var status = $("#crmstatusid").val();
					if(status == null || status == undefined || status == ''){
						$("#crmstatusid").select2('val',37).trigger("change");
					}
					invoicecreate();
					$('#dataaddsbtn').removeClass('singlesubmitonly');
				}
				else{
					alertpopup('Enter The Product!!');
					$('#dataaddsbtn').removeClass('singlesubmitonly');
					$('#quantity').addClass('validate[required,custom[number],decval[3],maxSize[100]]');
					$('#sellingprice').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#netamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#productid').addClass('validate[required]');
					$('#grossamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');					
					$('#paymentdate').addClass('validate[required]');
					$('#paymenttypeid').addClass('validate[required]');
					$('#paymentmethodid').addClass('validate[required]');
					$('#paymentamount').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
					if(softwareindustryid == 4){
						$('#joiningdate').addClass('validate[required]');
						$('#expirydate').addClass('validate[required]');
					}
				}					
			},
			onFailure: function() {
				var dropdownid =['1','patientid'];
				dropdownfailureerror(dropdownid);				
				alertpopup(validationalert);	
					$('#dataaddsbtn').removeClass('singlesubmitonly');
					$('#quantity').addClass('validate[required,custom[number],decval[3],maxSize[100]]');
					$('#sellingprice').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#netamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#productid').addClass('validate[required]');
					$('#grossamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');					
					$('#paymentdate').addClass('validate[required]');
					$('#paymenttypeid').addClass('validate[required]');
					$('#paymentmethodid').addClass('validate[required]');
					$('#paymentamount').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
					if(softwareindustryid == 4){
						$('#joiningdate').addClass('validate[required]');
						$('#expirydate').addClass('validate[required]');
					}
			}
		});
	}
	{// Update Invoice information
		$('#dataupdatesubbtn').click(function(e) {
			$('#dataupdatesubbtn').addClass('singlesubmitonly');
			$('.ftab').trigger('click');
			$('#quantity').removeClass('validate[required,custom[number],decval[3],maxSize[100]]');
			$('#sellingprice').removeClass('validate[required,custom[number],decval[7],maxSize[100]]');
			$('#netamount').removeClass('validate[required,custom[number],decval[7],maxSize[100]]');
			$('#productid').removeClass('validate[required]');
			$('#grossamount').removeClass('validate[required,custom[number],decval[7],maxSize[100]]');			
			$('#paymentdate').removeClass('validate[required]');
			$('#paymenttypeid').removeClass('validate[required]');
			$('#paymentmethodid').removeClass('validate[required]');
			$('#paymentamount').removeClass('validate[required,custom[number],decval[2],maxSize[100]]');
			if(softwareindustryid == 4){
				$('#joiningdate').removeClass('validate[required]');
				$('#expirydate').removeClass('validate[required]');
			}
			$("#formeditwizard").validationEngine('validate');
		});
		jQuery("#formeditwizard").validationEngine({
			onSuccess: function()
			{
				if(deviceinfo != 'phone'){
					var getrownumber = $('#invoicesproaddgrid1 .gridcontent div.data-content div').length;
				}else{
					var getrownumber = $('#invoicesproaddgrid1 .gridcontent div.wrappercontent div').length;
				}				
				if(getrownumber != 0){
					$("#processoverlay").show();
					invoiceupdate();
					$('#dataupdatesubbtn').removeClass('singlesubmitonly');
				}else{
					alertpopup('Enter The Product...');
					$('#dataupdatesubbtn').removeClass('singlesubmitonly');
					$('#quantity').addClass('validate[required,custom[number],decval[3],maxSize[100]]');
					$('#sellingprice').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#netamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#productid').addClass('validate[required]');
					$('#grossamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');					
					$('#paymentdate').addClass('validate[required]');
					$('#paymenttypeid').addClass('validate[required]');
					$('#paymentmethodid').addClass('validate[required]');
					$('#paymentamount').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
					if(softwareindustryid == 4){
						$('#joiningdate').addClass('validate[required]');
						$('#expirydate').addClass('validate[required]');
					}
				}				
			},
			onFailure: function()
			{
				var dropdownid =['1','accountid'];
				dropdownfailureerror(dropdownid);
				$('#dataupdatesubbtn').removeClass('singlesubmitonly');
				$('#quantity').addClass('validate[required,custom[number],decval[3],maxSize[100]]');
				$('#sellingprice').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
				$('#netamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
				$('#productid').addClass('validate[required]');
				$('#grossamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');					
				$('#paymentdate').addClass('validate[required]');
				$('#paymenttypeid').addClass('validate[required]');
				$('#paymentmethodid').addClass('validate[required]');
				$('#paymentamount').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
				if(softwareindustryid == 4){
					$('#joiningdate').addClass('validate[required]');
					$('#expirydate').addClass('validate[required]');
				}
				alertpopup(validationalert);
			}	
		});
	}
	{// Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			}else{
				$('#'+name+'').val('No');
			}		
		});
	}
	{// For Form to Grid
		griddataid = 0;
		gridynamicname = '';
		var gridname = $('#gridnameinfo').val();
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		$('.frmtogridbutton').click(function(){	
			var clickgridname = $(this).data('frmtogridname');
			if(clickgridname =='invoicesproaddgrid1'){  
				$('#invoicedetailid').val(0);
				$('.resetindividualoverlay').find('input[type=text],select,input[type=hidden]').val('');
				var i = $(this).data('frmtogriddataid');
				var gridname = $(this).data('frmtogridname');
				gridformtogridvalidatefunction(gridname);
				griddataid = i;
				gridynamicname = gridname;
				$("#"+gridname+"validation").validationEngine('validate');
				$("#quantity").removeClass();
				$("#quantity").addClass('validate[required,custom[number],decval[3],maxSize[100]]');
			}
			else if(clickgridname =='invoicespayaddgrid2'){ 
				$('#paymentid').val(0);
				if(deviceinfo != 'phone'){
					var productrows = $('#invoicesproaddgrid1 .gridcontent div.data-content div').length;
				}else{
					var productrows = $('#invoicesproaddgrid1 .gridcontent div.wrappercontent div').length;
				}			
				var gridpaymentamount = parseFloat(getgridcolvalue('invoicespayaddgrid2','','paymentamount','sum'));
				var totalpayable = parseFloat($('#totalpayable').val());
				var paymentamount = parseFloat($('#paymentamount').val());
				if(PAYMETHOD != 'UPDATE'){
					if(productrows == 0){
						alertpopup('Please Enter The Product First');
					}
					else if(paymentamount <= 0){
						alertpopup('Amount should be greater than Zero');					
					}				
					else if(gridpaymentamount == totalpayable){
						alertpopup('Payment already done for this invoice');	
					}
					else{
						//array manipulate to set the array details					
						var i = $(this).data('frmtogriddataid');
						var gridname = $(this).data('frmtogridname');
						gridformtogridvalidatefunction(gridname);
						griddataid = i;
						gridynamicname = gridname;
						$("#"+gridname+"validation").validationEngine('validate');
					}
				}else{
					if(productrows == 0){
						alertpopup('Please Enter The Product First');
					}
					else if(paymentamount <= 0){
						alertpopup('Amount should be greater than Zero');					
					}	
					else{
						//array manipulate to set the array details					
						var i = $(this).data('frmtogriddataid');
						var gridname = $(this).data('frmtogridname');
						gridformtogridvalidatefunction(gridname);
						griddataid = i;
						gridynamicname = gridname;
						$("#"+gridname+"validation").validationEngine('validate');
					}
				}
			}
			//materialize update
			setTimeout(function(){					
				Materialize.updateTextFields();
			},150);
		});
	}
	{// Terms and condition value fetch
		$("#termsandconditionid").change(function()
		{
			var id = $("#termsandconditionid").val();
			tandcdatafrtch(id);
		});
	}
	{// Product change events 
		$("#ordertypeid").change(function() {
    		if(softwareindustryid == 4){
    			var ordertype = $(this).val();
        		if(ordertype == 5){        			
        			$("#instockdivhid").addClass('hidedisplay');
        			$("#instock").removeClass('validate[custom[number],decval[3],maxSize[100]]');
        			$('#quantity').attr("readonly", true);
        			$("#durationid,#joiningdate,#expirydate").val("");
        			$("#durationiddivhid,#joiningdatedivhid,#expirydatedivhid").removeClass('hidedisplay');
        			gridfieldhide('invoicesproaddgrid1',['instock']);
        			gridfieldshow('invoicesproaddgrid1',['durationidname','expirydate','joiningdate']);
        		}else{
        			$("#durationiddivhid,#joiningdatedivhid,#expirydatedivhid").addClass('hidedisplay');
        			$('#quantity').attr("readonly",false);
        			$("#instockdivhid").removeClass('hidedisplay');
        			$("#instock").addClass('validate[custom[number],decval[3],maxSize[100]]');
        			gridfieldshow('invoicesproaddgrid1',['instock']);
        			gridfieldhide('invoicesproaddgrid1',['durationidname','durationid','expirydate','joiningdate']);
        		}
    		}    		
    		var product = $("#productid").val();
    		if(product){   
    			$('#productid').select2('val','').trigger('change');
    			$('#instock,#quantity,#unitprice,#sellingprice,#grossamount,#pretaxtotal,#chargeamount,#discountamount,#taxamount,#netamount').val('');
    			$("#productid").empty();
    		}
    	});
    	if(softwareindustryid == 4){
    		$('#joiningdate').change(function(){
    			dateexpiry();
    		});
    		$('#durationid').change(function(){
    			var joiningdate = $('#joiningdate').datepicker('getDate');
    			if(joiningdate){
    				dateexpiry();
    			}
    		});
    		function dateexpiry(){
    			var duration = $('#durationid').val();
    			var joiningdate = $('#joiningdate').datepicker('getDate');
    			var month = 0;
    			if(duration == 2){
    				month = 1;
    				var CurrentDate = new Date(joiningdate);
    				CurrentDate.setMonth(CurrentDate.getMonth() + month);								
    				$('#expirydate').datetimepicker('setDate',CurrentDate);
    			}else if(duration == 3){
    				month = 3;
    				var CurrentDate = new Date(joiningdate);
    				CurrentDate.setMonth(CurrentDate.getMonth() + month);								
    				$('#expirydate').datetimepicker('setDate',CurrentDate);
    			}else if(duration == 4){
    				month = 6;
    				var CurrentDate = new Date(joiningdate);
    				CurrentDate.setMonth(CurrentDate.getMonth() + month);								
    				$('#expirydate').datetimepicker('setDate',CurrentDate);
    			}else if(duration == 5){
    				month = 12;
    				var CurrentDate = new Date(joiningdate);
    				CurrentDate.setMonth(CurrentDate.getMonth() + month);								
    				$('#expirydate').datetimepicker('setDate',CurrentDate);
    			}
    		}
    	}
		if(softwareindustryid != 2){
	    	$("#productid").select2().on("select2-focus", function() {
	    		var productid = $("#productid").val();
	    		if(productid == ''|| productid == null){
	    			var ordertype = $('#ordertypeid').val();
	    			if(ordertype){
	    				$.ajax({
	    					url:base_url+"Base/getorderbasedproduct?ordertypeid="+ordertype+"&producthide=No",
	    					dataType:'json',
	    					async:false,
	    					cache:false,
	    					success:function(data) {
	    						$('#productid').empty();
	    						prev = ' ';
	    						var optclass = [];
	    						$.each(data, function(index) {
	    							var cur = data[index]['PId'];
	    							if(prev != cur ) {
	    								if(prev != " ") {
	    									$('#productid').append($("</optgroup>"));
	    								}
	    								if(jQuery.inArray(data[index]['optclass'], optclass) == -1) {
	    									optclass.push(data[index]['optclass']);
	    									$('#productid').append($("<optgroup  label='"+data[index]['optclass']+"' class='"+data[index]['optclass']+"'>")); 
	    								} 
	    								$("#productid optgroup[label='"+data[index]['optclass']+"']")
	        							.append($("<option></option>")
	        							.attr("class",data[index]['optionclass'])
	        							.attr("value",data[index]['value'])
	        							.attr("data-productidhidden",data[index]['data-productidhidden'])
	        							.text(data[index]['data-productidhidden']));
	    								prev = data[index]['PId'];
	    							}
	    							
	    						}); 
	    						$('#productid').select2('val','').trigger("change");
	    					}
	    				});
	    			}else{
	    				$('#productid').empty();
    					setTimeout(function(){
    						$("#productid").select2("close");
    						alertpopup("Select the Invoice Type to Load the Product");
    					},100);
	    			}
	    		}    		
	    	});
		}
		$("#productid").change(function(){
			var productval = $(this).find('option:selected').data('productidhidden');
			var ul = $('#invoicesproaddgrid1 div.gridcontent .inline-list');
			var lis = ul.children('li');
			var isInList = false;
			for(var i = 0; i < lis.length; i++){
			    if(lis[i].innerHTML === productval) {
			        isInList = true;
			        break;
			    }
			}
			if(isInList){
				 alertpopup("Product already Exist. Kindly choose another Product");
				 $('#productid').select2('val','');
				 $('#instock,#quantity,#unitprice,#sellingprice,#grossamount,#pretaxtotal,#chargeamount,#discountamount,#taxamount,#netamount').val('');
			}else{
				$("#quantity").removeClass();
				$("#quantity").addClass('validate[required,custom[number],decval[3],maxSize[100]]');
				$('#discountoverlay,#additionaloverlay,#taxoverlay').find('input:text').val('');
				$('#instock,#quantity,#unitprice,#sellingprice,#grossamount,#pretaxtotal,#chargeamount,#discountamount,#taxamount,#netamount').val('');
				$('#additionalcategory').select2('val','').trigger('change');
				var val=$(this).val();
				getproductdetails(val);	
				Materialize.updateTextFields();
				$('.error').removeClass('error');
				$(".btmerrmsg").remove();
				$('.formError').remove();
				var inputs = $(this).closest('form').find(':input');
				inputs.eq( inputs.index(this)+ 1 ).focus();
			}			
		});
	}	
	{// Calculation type
		$(".caltypedd").change(function()
		{var addctype = $(this).attr('id');
			var elementposition = $(this).data('val');
			var addvalue='addval_'+elementposition;
			var addamts='addamount_'+elementposition;
			setzero([addvalue,'grossamount']);
			var calctype=$("#"+addctype+"").val();
			var value=$('#'+addvalue+'').val();
			var price=$('#grossamount').val();
			var addamt=0;
			switch (calctype)
			{ 
				case '2': 
					var addamt=value;
				break;
				case '3':
					var addamt=(parseFloat(value/100)*parseFloat(price));
				break;
			}
			var faddamt=parseFloat(addamt);
			$('#'+addamts+'').val(faddamt.toFixed(2));	
		});
	}
	{// Addamount overlay submit
		$("#addamtsubmit").click(function()	{
			var addamount = $("#totaladdselling").val();
			$("#additionalamount").val(addamount);
			additionalamountvalueadd();
			$("#additionaloverlay").fadeOut();
		});
		$('#addamtsubmit').click(function(e){
			$("#additionaloverlayvalidation").validationEngine('validate');			
		});
		jQuery("#additionaloverlayvalidation").validationEngine({
				onSuccess: function() {
					$("#additionaloverlay").fadeOut();
				},
				onFailure: function() {	
				}
		});
	}	
	{// Pricebook submit
		$("#pricebooksubmit").click(function()
		{
			var selectedrow = $('#pricebookgrid div.gridcontent div.active').attr('id');
			if(selectedrow) 
			{
				var sellingprice = getgridcolvalue('pricebookgrid',selectedrow,'sellingprice','');
				var currencyid = getgridcolvalue('pricebookgrid',selectedrow,'currencyid','');	
				var current_currencyid=$("#currencyid").val();
				
				if(current_currencyid == currencyid){
					$('#sellingprice').val(sellingprice).trigger('focusout'); //sets the value and triggerchange calc
					$("#pricebookoverlay").fadeOut();	
				} else if(current_currencyid != currencyid) {	
					var invoicedate = $('#invoicedate').val();
					var conversionrate = getcurrencyconversionrate(currencyid,current_currencyid,invoicedate); //to get conversion rate
					$('#currentcurrency').select2('val',current_currencyid).trigger('change').attr("disabled","disabled");
					$('#pricebookcurrency').select2('val',currencyid).trigger('change').attr("disabled","disabled");					
					$("#pricebook_sellingprice").val(sellingprice);					
					$('#pricebook_currencyconv').val(conversionrate);
					$("#pricebookcurrencyconvoverlay").fadeIn();
					Materialize.updateTextFields();
				}			
			}
			else
			{
				alertpopup('Please select a row');
			} 
		});
	}
	/*retrieve the conversion rate(pricebook currency to current currency)*/
	function getcurrencyconversionrate(currencyid,current_currencyid,invoicedate){
		var rate = '';
		if(checkValue(currencyid) == true && checkValue(current_currencyid) == true){
			$.ajax({
				url:base_url+"index.php/Base/getcurrencyconversionrate",
				data:{fromcurrency:currencyid,tocurrency:current_currencyid,quotedate:invoicedate},
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					rate = data.rate;					
				}
			});
			return rate;
		} else {
			return rate;
		}
	}
	{
		// Redirected form Home		
		add_fromredirect("invoiceaddsrc",MODULEID);
	}
	{// Invoice Product Grid Edit
		$("#invoicesproingridedit1").click(function()
		{
			var selectedrow = $('#invoicesproaddgrid1 div.gridcontent div.active').attr('id');			
			if(selectedrow){
				sectionpanelheight('invoicesprooverlay');
				$("#invoicesprooverlay").removeClass("closed");
				$("#invoicesprooverlay").addClass("effectbox");
				$('#productid').select2('focus');
				gridtoformdataset('invoicesproaddgrid1',selectedrow);
				Materialize.updateTextFields();
				$("#quantity").removeClass();
				$("#quantity").addClass('validate[required,custom[number],decval[3],maxSize[100]]');
				var productid = $('#productid').val();
				var pricebook = $('#pricebookid').val();
				$.ajax({
					url:base_url+'Base/getproductdetails?id='+productid+'&pricebook='+pricebook,
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						QUANTITY_PRECISION = data.uomprecision;
						$("#quantity").removeClass('validate[required,custom[number],decval[3],maxSize[100]]');
						$("#quantity").addClass('validate[required,custom[number],decval['+QUANTITY_PRECISION+'],maxSize[100]]');
					}
				});
				$('#invoicesproupdatebutton').show();	//display the UPDATE button(inner - productgrid)
				$('#invoicesproaddbutton').hide();	//display the ADD button(inner-productgrid) */
				METHOD = 'UPDATE';
				UPDATEID = selectedrow;
			} else {
				alertpopup('Please Select The Row To Edit');
			}
		});
	}
	{// Invoice Payment Grid Edit
		$("#invoicespayingridedit2").click(function()
		{
			var selectedrow = $('#invoicespayaddgrid2 div.gridcontent div.active').attr('id');
			if(selectedrow){
				sectionpanelheight('invoicespayoverlay');
				$("#invoicespayoverlay").removeClass("closed");
				$("#invoicespayoverlay").addClass("effectbox");
				gridtoformdataset('invoicespayaddgrid2',selectedrow);	
				var paymenttypeid = $("#paymenttypeid").val();
				if(paymenttypeid != ''){
					if(paymenttypeid == 2){
						$("#paymenttypeid option[value='3']").wrap('<span/>');
						$("#paymenttypeid option[value='4']").wrap('<span/>');
					}
					if(paymenttypeid == 3){
						$("#paymenttypeid option[value='2']").wrap('<span/>');
						$("#paymenttypeid option[value='4']").wrap('<span/>');
					}
					if(paymenttypeid == 4){
						$("#paymenttypeid option[value='2']").wrap('<span/>');
						$("#paymenttypeid option[value='3']").wrap('<span/>');
					}
				}				  
				$('#invoicespayupdatebutton').show();	//display the UPDATE button/
				$('#invoicespayaddbutton').hide();	//display the ADD button/
				PAYMETHOD = 'UPDATE';
				PAYUPDATEID = selectedrow;
				Materialize.updateTextFields();
			} else {
				alertpopup('Please Select The Row To Edit');
			}
		});
	}	
	//salesorder-reportsession check
	{// Redirected form Home
		var uniquelreportsession = sessionStorage.getItem("reportunique226"); 
		if(uniquelreportsession != '' && uniquelreportsession != null) {
			setTimeout(function() { 
				invoiceeditdatafetchfun(uniquelreportsession);
				showhideiconsfun('summryclick','invoicecreationformadd');
				$("#invoicesproingriddel1,#invoicespayingriddel2").hide();
				sessionStorage.removeItem("reportunique226");
			},50);	
		}
	}
	//patient-appointment session check
	{// Redirected form patient
		if(softwareindustryid == 2){
			var uniquepatientsession = sessionStorage.getItem("patientid");
			if(uniquepatientsession != '' && uniquepatientsession != null) {
				setTimeout(function() {
					$('#addicon').trigger('click');
					$("#contactid").select2('val',uniquepatientsession);
					sessionStorage.removeItem("patientid");
				},100);
			}
		}
	}
	{
		$("#invoicespayupdatebutton").click(function() {			
			var paymenttypeid = $("#paymenttypeid").val();
			$('#invoicespayaddbutton').show();
			$('#invoicespayupdatebutton').hide();
		});
	}
	{
		$('#printicon').click(function(){
			var datarowid = $('#leadinvoicegrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$("#templatepdfoverlay").fadeIn();
				var viewfieldids = $("#viewfieldids").val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup("Please select a row");
			}
		});
		//payment icon
		$('#paymenticon').click(function(){
			var datarowid = $('#leadinvoicegrid div.gridcontent div.active').attr('id');
			if(datarowid){
				var stage=invoicestage(datarowid);
				if(stage == 38){ //Booked					
					var paymentmoduleid = MODULEID;
					var paymenttransactionid = datarowid;					
					sessionStorage.setItem("paymentmoduleid",paymentmoduleid);
					sessionStorage.setItem("paymenttransactionid",paymenttransactionid);				
					window.location = base_url+'Payment';
				}else {
					alertpopup("Booked Invoice Only will lead to Payment");
				}				
			} else {
				alertpopup("Please select a row");
			}
			
		});
		//data manipulation icon
		$("#datamanipulationicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();	
			sessionStorage.setItem("datamanipulateid",viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		//Customize icon
		$("#customizeicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();		
			sessionStorage.setItem("customizeid",viewfieldids);
			window.location = base_url+'Formeditor';
		});
		//Find Duplication icon
		$("#findduplicatesicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();		
			sessionStorage.setItem("finddubid",viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	{// Date REstriction
		var dateformetinv  = 0;
		dateformetinv = $('#invoicedate').attr('data-dateformater');
		$('#invoicedate').datetimepicker({
			dateFormat: dateformetinv,
			showTimepicker :false,
			minDate: 0,
			onSelect: function () {
				getandsetdate($('#invoicedate').val());
			},
			onClose: function () {
				$('#invoicedate').focus();
			}
		});
	}
	{
		$("#invoicespayupdatebutton").click(function() {			
			var paymenttypeid = $("#paymenttypeid").val();
			if(paymenttypeid != ''){
				if(paymenttypeid == 2){
					$("#paymenttypeid option[value='3']").unwrap();
					$("#paymenttypeid option[value='4']").unwrap();
				}
				if(paymenttypeid == 3){
					$("#paymenttypeid option[value='2']").unwrap();
					$("#paymenttypeid option[value='4']").unwrap();
				}
				if(paymenttypeid == 4){
					$("#paymenttypeid option[value='2']").unwrap();
					$("#paymenttypeid option[value='3']").unwrap();
				}
			}
			$('#invoicespayaddbutton').show();
			$('#invoicespayupdatebutton').hide();
		});
	}		
	{// Dashboard Icon Click Event
		$('#dashboardicon').click(function(){
			var datarowid = $('#leadinvoicegrid div.gridcontent div.active').attr('id');
			if (datarowid) {		
				$("#processoverlay").show();
				$.getScript(base_url+"js/plugins/uploadfile/jquery.uploadfile.min.js");
				$.getScript(base_url+"js/plugins/zingchart/zingchart.min.js");
				$.getScript('js/Home/home.js',function(){
					dynamicchartwidgetcreate(datarowid);
				});
				$("#dashboardcontainer").removeClass('hidedisplay');
				$('.actionmenucontainer').addClass('hidedisplay');
				$('.fullgridview').addClass('hidedisplay');
				$('.footercontainer').addClass('hidedisplay');
				$(".forgetinggridname").addClass('hidedisplay');
				$("#processoverlay").hide();
			} else {
				alertpopup("Please select a row");
			}
		});
	}	
	{ //auto trigger
		generatemoduleiconafterload('modulenumber','modulenumberevent');
		generateiconafterload('sellingprice','pricebookclickevent','sellingpricedivhid');
		productgenerateiconafterload('productid','productsearchevent');		
	}		
	{ //
		$("#pricebookclickevent").click(function(){
			if(checkVariable('productid') == true) {
				$("#pricebookoverlay").fadeIn();
				$('#pricebooksubmit').focus();
				pricebookgrid();
			} else {
				cleargriddata('pricebookgrid');
			}
		});
		$("#productsearchevent").click(function(){
			if(softwareindustryid != 2){
				var ordertype = $('#ordertypeid').val();
				if(ordertype){
					$("#productsearchoverlay").fadeIn();
					$("#productsearchclose").focus();
					productsearchgrid();
				}else{
					alertpopup("Select the Quote Type to Load the Product");
				}	
			}else{
				$("#productsearchoverlay").fadeIn();
				productsearchgrid();
			}
		});
		$("#pricebookclose").click(function(){
			$("#pricebookoverlay").fadeOut();
			$('#grossamount').focus();
		});
		$("#pricebookcurrencyconvclose").click(function()
		{
			$("#pricebookcurrencyconvoverlay").fadeOut();
		});
		$("#productsearchclose").click(function()
		{
			$("#productsearchoverlay").fadeOut();
			$('#quantity').focus();
		});
	}	
	{
		//currency overlay
		$("#currencyconverticon").click(function(){
			var productrecords = $('#invoicesproaddgrid1 .gridcontent div.data-content div').length;
			if(productrecords == 0){
				$("#currencyconvoverlay").fadeIn();
				$.ajax({
					url:base_url+"Base/getdefaultcurrency", 
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {						
						$('#defaultcurrency').select2('val',data.currency).trigger('change').attr("readonly","readonly");
					}
				});
			}					
		});
		$("#currencyconvclose").click(function() {
			$("#currencyconvoverlay").fadeOut();
		});
		//currency 
		$("#convcurrency").change(function() {
			var defaultcurrency = $("#currencyid").val();
			var currencyid = $(this).val();
			var quotedate = $("#invoicedate").val();
			if(currencyid > 0) {
					var selected=$('#currencyid').find('option:selected');
					var datavalue=selected.data('precision');
					PRECISION = parseFloat(datavalue);
				if(datavalue == '' || datavalue == null) {
					PRECISION = 0;
				}
				$.ajax({
					url:base_url+"Quote/getconversionrate?fromcurrency="+defaultcurrency+"&tocurrency="+currencyid+"&quotedate="+quotedate, 
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						if(data.status == true){
							$('#currencyconv').val(data.rate);
							Materialize.updateTextFields();
						}else{
							$('#currencyconv').val(0);
							Materialize.updateTextFields();
						}
					}
				});
			} else {
				PRECISION = 0;
				$('#currencyconv').val(0);
			}
			
		});
		$('#currencyconv_submit').click(function(e){
			$("#currencyconv_validate").validationEngine('validate');			
		});
		jQuery("#currencyconv_validate").validationEngine({
			onSuccess: function() { 
				var convcurrency=$("#convcurrency").val();	 //convert currency
				var currencyconv=$("#currencyconv").val();	 //convert rate
				$("#currencyid").select2('val',convcurrency).trigger("change");
				$('#currencyconvresionrate').val(currencyconv);
				$("#productid").select2('val','').trigger("change");
				$("#pricebookid").select2('val','').trigger("change");
				$("#currencyconvoverlay").fadeOut();
			},
			onFailure: function() {	
			}
		});		
		$("#currencyid").change(function(){			
			var currencyid = $(this).val();
			if(currencyid > 0){
				var selected=$('#currencyid').find('option:selected');
				var datavalue=selected.data('precision');
				PRECISION = parseFloat(datavalue);
				if(datavalue == '' || datavalue == null){
					PRECISION = 0;
				}	
				
			} else {
				PRECISION = 0;
			}
		});
	}
	{ // Grid overlay operations
		$("#discountamount").click(function(){			
			var product=checkVariable('productid');
			var discount_data = $('#discountdata').val();
			var parse_discount = $.parseJSON(discount_data);			
			if(parse_discount == null){ //if no previous data exits then refresh the discount fields
				$('#discounttypeid').select2('val','').trigger('change');
				$('#discountpercent').val('');				
			} else {								
				$('#discounttypeid').select2('val',parse_discount.typeid).trigger('change');
				$('#discountpercent').val(parse_discount.value);
			}			
			$("#discountoverlay").fadeIn();
			setTimeout(function()
			{
				discount_total();
				Materialize.updateTextFields();
				$("#discountclose").focus();
			},100);
		});		
		$("#discounttypeid").change(function(){
			$('#discountpercent').val(0);
			setTimeout(function()
			{
				discount_total();
				Materialize.updateTextFields();
			},100);
		});
		$("#discountpercent").focusout(function(){			
			setTimeout(function()
			{
				discount_total();
				Materialize.updateTextFields();
			},100);
		});
		//discountrefresh-individual
		$("#discountrefresh").click(function(){
			$('#discounttypeid').select2('val','2').trigger('change');
			$('#discountpercent').val('');
			$('#singlediscounttotal').val('');	
		});
		//*group discount click*//
		$("#groupdiscountamount").click(function()
		{			
			var discount_data = $('#groupdiscountdata').val();
			var parse_discount = $.parseJSON(discount_data);				
			if(parse_discount == null){ //if no previous data exits then refresh the discount fields
				$('#groupdiscounttypeid').select2('val','').trigger('change');
				$('#groupdiscountpercent').val('');				
			} else {							
				$('#groupdiscounttypeid').select2('val',parse_discount.typeid).trigger('change');
				$('#groupdiscountpercent').val(parse_discount.value);
			}
			$("#summarydiscountoverlay").fadeIn();				
			setTimeout(function()
			{
				group_discount_total();
				Materialize.updateTextFields();
			},100);			
		});
		//*group discount close*//		
		$("#summarydiscountclose").click(function()
		{
			$("#summarydiscountoverlay").fadeOut();
			var value=parseFloat($('#groupdiscountpercent').val());
			if(value == '' || value == null || isNaN(value)){
				$('#groupdiscounttypeid').select2('val','').trigger('change');
			}
			$('#grouppretaxtotal').focus();
		});
		//*Summary Discount validation*//
		$('#summarydiscountadd').click(function(e){
			$("#summarydiscountvalidation").validationEngine('validate');			
		});
		jQuery("#summarydiscountvalidation").validationEngine({
			onSuccess: function() {											
				var discount_json ={}; //declared as object
				discount_json.typeid = $('#groupdiscounttypeid').val(); //discount typeid
				discount_json.value = $('#groupdiscountpercent').val();	//discount values		
				$('#groupdiscountdata').val(JSON.stringify(discount_json)); //saves discountdata in hiddenfield(JSON)
				var netamount = parseFloat(getgridcolvalue('invoicesproaddgrid1','','netamount','sum'));
				var discount_amount=discountcalculation(discount_json,netamount); //to calculate discount
				$('#groupdiscountamount').val(discount_amount);					
				$("#summarydiscountoverlay").fadeOut();
				//?set a trigger point here
				gridsummeryvaluefetch('','','');
			},
			onFailure: function() {	
			}
		});
		//discountrefresh-group
		$("#groupdiscountrefresh").click(function(){
			$('#groupdiscounttypeid').select2('val','2').trigger('change');
			$('#groupdiscountpercent').val('0');
			$('#groupdiscounttotal').val('');
			Materialize.updateTextFields();
		});
		$("#groupdiscounttypeid").change(function(){
			$('#groupdiscountpercent').val(0);
			setTimeout(function()
			{
				group_discount_total();
				Materialize.updateTextFields();
			},100);
		});
		$("#groupdiscountpercent").focusout(function(){			
			setTimeout(function()
			{
				group_discount_total();
				Materialize.updateTextFields();
			},100);
		});
		//tax
		$("#taxamount").click(function(){ 
			//identify the type(group/individual);
			var type = '';
			var taxmasterid = $('#taxmasterid').val();
			if(taxmasterid){
				type = 3;
			}else{
				type = 2;
			}
			$("#taxcategory").focus();
			if(type == 2 && checkVariable('productid') ==true && PRODUCTTAXABLE == 'Yes'){	//individual
				var g_data = $('#taxgriddata').val();
				var tax_category_id= 'taxcategory';
				var main_data = $.parseJSON(g_data);			
				if(!main_data || main_data == null){
					$("#taxoverlay").fadeIn();
					$('#'+tax_category_id+'').select2('val','').trigger('change');
					taxgrid();
				} else {
					var grid_data = main_data.data;
					if (grid_data.length > 0){		
						$("#taxoverlay").fadeIn();
						taxgrid();
						$('#'+tax_category_id+'').select2('val',main_data.id);
						loadgriddata('taxgrid',grid_data);
					} else {
						$("#taxoverlay").fadeIn();
						taxgrid();
						$('#'+tax_category_id+'').select2('val','').trigger('change');
					} 
				}				
				setTimeout(function()
				{
					tax_data_total();
				},200);
			}
		});
		$("#grouptaxamount").click(function(){			
				//identify the type(group/individual);
				var type = '';
				var taxmasterid = $('#taxmasterid').val();
				if(taxmasterid){
					type = 3;
				}else{
					type = 2;
				}
				if(type == 3){	//group
					var g_data = $('#grouptaxgriddata').val();
					var tax_category_id= 'taxcategory'; //?				 
					var main_data = $.parseJSON(g_data);			
					if(!main_data || main_data == null){						
						$("#taxoverlay").fadeIn();
						taxgrid();
						$('#taxcategory').select2('val',$('#taxmasterid').val()).trigger('change');
					} else {
						var grid_data = main_data.data;
						if (grid_data.length > 0) {
							$("#taxoverlay").fadeIn();
							taxgrid();
							$('#'+tax_category_id+'').select2('val',main_data.id);
							loadgriddata('taxgrid',grid_data);
						} else {
							$("#taxoverlay").fadeIn();
							taxgrid();
							$('#taxcategory').select2('val',$('#taxmasterid').val()).trigger('change');
						} 
					}
				}
				setTimeout(function()
				{
					tax_data_total();
				},200);
			});
		//edit/delete on overlay
		$("#taxcategory").change(function(){
			var id = $(this).val();
			var type = '';
			var taxmasterid = $('#taxmasterid').val();
			if(taxmasterid){
				type = 3;
			}else{
				type = 2;
			}
			var territory = $('#territoryid').val(); //region
			var totalnetamount = parseFloat($('#totalnetamount').val())-parseFloat($('#groupdiscountamount').val());//group
			cleargriddata('taxgrid');			
			if(id != ''){
				if(type == 2){
				var grossamount=parseFloat($('#grossamount').val());
				var discountamount=parseFloat($('#discountamount').val());
				var finalamount=parseFloat(grossamount)-parseFloat(discountamount);
				} else {
				var finalamount= 0;
				}
				if(id != '') {
					var transmethod = MODULEID;
					$.ajax({
						url:base_url+'Quote/taxmasterload?taxmasterid='+id
						+'&finalamount='+finalamount+'&territory='+territory+'&type='+type+'&totalnetamount='+totalnetamount+'&transmethod='+transmethod,
						dataType:'json',
						async:false,
						cache:false,
						success: function(data) {
							if((data.fail) == 'FAILED') {
							} else {
								loadinlinegriddata('taxgrid',data.rows,'json');
								setTimeout(function() {
									tax_data_total();
								},200);
								/* data row select event */
								datarowselectevt();
								/* column resize */
								columnresize('chargesgrid');
							}
						},
					});
				}
			}
		});		
		$("#taxdeleteicon").click(function(){
				var id = $('#taxgrid div.gridcontent div.active').attr('id');
				if(id){
					deletegriddatarow('taxgrid',id);
					setTimeout(function()
					{
						tax_data_total();
					},100);
					if(deviceinfo != 'phone'){
						if($('#taxgrid .gridcontent div.data-content div').length == 0){
							$('#taxcategory').select2('val','').trigger('change');
						}
					}else{
						if($('#taxgrid .gridcontent div.wrappercontent div').length == 0){
							$('#taxcategory').select2('val','').trigger('change');
						}
					}
					
				} else {
					alertpopup('Select Tax Record');
				}
		});
		$("#taxclearicon").click(function(){
			cleargriddata('taxgrid');			
			$('#taxcategory').select2('val','').trigger('change');
			setTimeout(function()
			{
				tax_data_total();
			},200);
		});
		$("#chargeclearicon").click(function(){
			cleargriddata('chargesgrid');
			$('#chargecategory').select2('val','').trigger('change');
			setTimeout(function()
			{
				charge_data_total();
			},200);
		});
		$("#chargedeleteicon").click(function(){
				var id = $('#chargesgrid div.gridcontent div.active').attr('id');
				if(id){
					deletegriddatarow('chargesgrid',id);
					setTimeout(function()
					{
						charge_data_total();
					},200);
					if(deviceinfo != 'phone'){
						if($('#chargesgrid .gridcontent div.data-content div').length == 0){
							$('#chargecategory').select2('val','').trigger('change');
						}
					}else{
						if($('#chargesgrid .gridcontent div.wrappercontent div').length == 0){
							$('#chargecategory').select2('val','').trigger('change');
						}
					}
				} else {
					alertpopupdouble('Select Charge Record');
				}
		});
		$("#chargeamount").click(function(){
			//identify the type(group/individual);
			var type = '';
			var additionalchargecategoryid = $('#additionalchargecategoryid').val();
			if(additionalchargecategoryid){
				type = 3;
			}else{
				type = 2;
			}
			if(type == 2 && checkVariable('productid') ==true){	//individual
				var g_data = $('#chargegriddata').val();
				var charge_category_id= 'chargecategory';			
				var main_data = $.parseJSON(g_data);			
				if(!main_data || main_data == null){
					$("#additionaloverlay").fadeIn();
					chargesgrid();
					$('#'+charge_category_id+'').select2('val','').trigger('change');
				} else {
					var grid_data = main_data.data;
					if (grid_data.length > 0) {
						$("#additionaloverlay").fadeIn();
						chargesgrid();
						setTimeout(function(){
							$('#'+charge_category_id+'').select2('val',main_data.id);
							console.log(grid_data);
							loadgriddata('chargesgrid',grid_data);
						},200);
					} else {
						$("#additionaloverlay").fadeIn();
						chargesgrid();
						$('#'+charge_category_id+'').select2('val','').trigger('change');
					} 
				}
				setTimeout(function()
				{
					charge_data_total();
					$("#s2id_chargecategory").select2('focus');
				},200);
			}  
		});
		//summary additional amount overlay
		$("#groupchargeamount").click(function()
		{
			//identify the type(group/individual);
			var type = '';
			var additionalchargecategoryid = $('#additionalchargecategoryid').val();
			if(additionalchargecategoryid){
				type = 3;
			}else{
				type = 2;
			}
			if(type == 3){	//group
				var g_data = $('#groupchargegriddata').val();
				var charge_category_id= 'chargecategory';		
				var main_data = $.parseJSON(g_data);			
				if(!main_data || main_data == null){
					$('#chargecategory').select2('val',$('#additionalchargecategoryid').val()).trigger('change');
					$("#additionaloverlay").fadeIn();
					chargesgrid();
				} else {
					var grid_data = main_data.data;
					if (grid_data.length > 0) {
						$("#additionaloverlay").fadeIn();
						chargesgrid();
						$('#'+charge_category_id+'').select2('val',main_data.id);
						loadgriddata('chargesgrid',grid_data);
					} else {
						$("#additionaloverlay").fadeIn();
						chargesgrid();
						$('#chargecategory').select2('val',$('#additionalchargecategoryid').val()).trigger('change');
					} 
				}
				setTimeout(function()
				{
					charge_data_total();
				},200);
				$("#chargecategory").focus();
			} 
		});
		//invite overlay fade out
		$("#discountclose").click(function()
		{
			$("#discountoverlay").fadeOut();
			var value=parseFloat($('#discountpercent').val());
			if(value == '' || value == null || isNaN(value)){
				$('#discounttypeid').select2('val','').trigger('change');
			}
			$('#pretaxtotal').focus();
		});
		//recurrence overlay fadeOut
		$("#taxclose").click(function()
		{
			productnetcalculate();	
			var taxid = $("#taxcatid").val();
			var product = $("#productid").val();		
			$("#taxoverlay").fadeOut();
		});
		$("#additionalclose").click(function(){
			productnetcalculate();	
			var id = $("#addcatid").val();
			var product=checkVariable('productid');	
			$("#additionaloverlay").fadeOut();
		});
		$('#discountsubmit').click(function(e){
			
			$("#individualdiscountvalidation").validationEngine('validate');			
		});
		$("#individualdiscountvalidation").validationEngine({
			onSuccess: function() {									
				setTimeout(function()
				{
					setzero(['grossamount']); //sets zero on EMPTY					
					var discount_json ={}; //declared as object
					discount_json.typeid = $('#discounttypeid').val(); //discount typeid
					discount_json.value = $('#discountpercent').val();	//discount values		
					$('#discountdata').val(JSON.stringify(discount_json)); //saves discount data in hiddenfield(JSON object)
					var grossamount = $('#grossamount').val();
					var discount_amount=discountcalculation(discount_json,grossamount); //to calculate discount
					$('#discountamount').val(discount_amount);
					$('#quantity').trigger('focusout');		//after succesful discount trigger other events	
				},10);
				$("#discountoverlay").fadeOut();
			},
			onFailure: function() {	
			}
		});	
		//charge change calculations
		$("#chargecategory").change(function()
		{
			var id = $(this).val();
			var additionalchargecategoryid = $('#additionalchargecategoryid').val(); //2-individual 3-group
			var typeid = '';
			if(additionalchargecategoryid){
				typeid = 3;
			}else{
				typeid = 2;
			}
			var territory = $('#territoryid').val(); //region
			var conversionrate = $('#currencyconvresionrate').val(); //conversion rate
			var currency = $('#currencyid').val(); //conversion rate
			cleargriddata('chargesgrid');
			var grossamount = $('#grossamount').val();
			var totalnetamount = $('#totalnetamount').val();
			if(id != ''){
				var transmethod = MODULEID;
				$.ajax({
					url:base_url+'Quote/chargecategoryload?chargecategoryid='+id+'&grossamount='+grossamount+'&territory='+territory+'&conversionrate='+conversionrate+'&currency='+currency+'&typeid='+typeid+'&totalnetamount='+totalnetamount+'&transmethod='+transmethod,
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						if((data.fail) == 'FAILED') {
						} else {
							loadinlinegriddata('chargesgrid',data.rows,'json');
							setTimeout(function() {
								charge_data_total();
							},200);
							/* data row select event */
							datarowselectevt();
							/* column resize */
							columnresize('chargesgrid');
						}
					},
				});
			}			
		});		
		//pricebook_currencyconv_submit
		{
			$('#pricebook_currencyconv_submit').click(function(e){
				$("#pricebookcurrencyconv_validate").validationEngine('validate');			
			});
			jQuery("#pricebookcurrencyconv_validate").validationEngine({
					onSuccess: function() { 
						var pb_sellingprice=$("#pricebook_sellingprice").val();	 //pricebook sellingprice
						var pb_currentcurrency = $("#currentcurrency").val();
						var pb_pricebookcurrency = $("#pricebookcurrency").val();
						var pb_conversionrate=$("#pricebook_currencyconv").val();	 //pricebook sellingprice
						$("#currentcurrencyid").val(pb_currentcurrency);
						$("#pricebookcurrencyid").val(pb_pricebookcurrency);
						$("#pricebook_currencyconvrate").val(pb_conversionrate);
						PRICBOOKCONV_VAL = pb_conversionrate;
						var productid = $("#productid").val();
						if(productid){
							var finalvalue = listprice(pb_sellingprice,pb_conversionrate);
							var conversionrate = $('#currencyconvresionrate').val();
							finalvalue = listprice(finalvalue,conversionrate);
							$('#sellingprice').val(finalvalue).trigger('focusout'); //sets the value and triggerchange calc
						}	
						$("#pricebookcurrencyconvoverlay,#pricebookoverlay").fadeOut();
					},
					onFailure: function() {	
					}
			});
		}
		{//adjustment overlay
			$("#adjustmentamount").click(function()
			{
				var adjustment_data = $('#groupadjustmentdata').val();
				var parse_adjustment = $.parseJSON(adjustment_data);				
				if(parse_adjustment == null){ //if no previous data exits then refresh the adjusttment fields
					$('#adjustmenttypeid').select2('val','2').trigger('change');
					$('#adjustmentvalue').val(0);				
				} else {							
					$('#adjustmenttypeid').select2('val',parse_adjustment.typeid).trigger('change');
					$('#adjustmentvalue').val(parse_adjustment.value);
				}				
				$("#adjustmentoverlay").fadeIn();
				Materialize.updateTextFields();
			});
			$("#adjustmentclose").click(function()
			{
				clearform('clearadjustmentform');
				$("#adjustmentoverlay").fadeOut();
				$('#grandtotal').focus();
			});
			$("#adjustmentadd").click(function()
			{
				$('#adjustmenttypeid').trigger('change'); //to liveup adjustmentdd
				setzero(['adjustmentvalue']);				
				var adjustment_json ={}; //declared as object
				adjustment_json.typeid = $('#adjustmenttypeid').val(); //adjustment type
				adjustment_json.value = $('#adjustmentvalue').val(); // adjustment value			
				$('#groupadjustmentdata').val(JSON.stringify(adjustment_json));				
				var adjustment_value=parseFloat($('#adjustmentvalue').val());				
				$("#adjustmentamount").val(adjustment_value.toFixed(PRECISION));
				$("#adjustmentoverlay").fadeOut();
				//?trigger point for calculation
				gridsummeryvaluefetch('','','');
			});
		}
	}
	{//productsearchsubmit
		$("#sellingprice").focusout(function()
		{
			var vl=$(this).val();
			if(checkVariable('sellingprice') == false || isNaN(vl)){	
				$("#sellingprice").val(0);
			}
			$("#quantity").trigger('focusout');
		});
		$("#quantity").focusout(function()
		{			
			var temo_qty=$('#quantity').val();
			if(isNaN(temo_qty)){
				$('#quantity').val(1);
			}else{
				if(softwareindustryid == 4){
					if(checkVariable('quantity') == true ){
						$("#quantity").removeClass('validate[required,custom[number],decval[3],maxSize[100]]');
						$("#quantity").addClass('validate[required,custom[number],decval['+QUANTITY_PRECISION+'],maxSize[100]]');
						var round_qty = parseFloat(this.value);
						$(this).val(round_qty.toFixed(QUANTITY_PRECISION));				
						var quan = $("#quantity").val();
						var sellprice = $("#sellingprice").val();
						var gross = parseFloat(quan) * parseFloat(sellprice);
						$("#grossamount").val(gross.toFixed(PRECISION));
						setTimeout(function(){
							var discount_json = $.parseJSON($('#discountdata').val()); //individual discount data					
							var discount_amount=discountcalculation(discount_json,gross); //to calculate discount					
							$('#discountamount').val(discount_amount);
							//pretax-total
							var pretax = parseFloat($('#grossamount').val())-parseFloat($('#discountamount').val());
							$("#pretaxtotal").val(pretax.toFixed(PRECISION));
						},10);	
						update_taxgriddata();
						if(softwareindustryid != 2){
							update_chargegriddata();
						}				
						productnetcalculate();
					}
				}else{
					var stock = parseInt($('#instock').val());
					var qty = parseInt($('#quantity').val());
					if(qty > stock){
						alertpopup("Stock should be greater than quantity");
						$("#quantity").val(1);
					}else{
						if(checkVariable('quantity') == true ){
							$("#quantity").removeClass('validate[required,custom[number],decval[3],maxSize[100]]');
							$("#quantity").addClass('validate[required,custom[number],decval['+QUANTITY_PRECISION+'],maxSize[100]]');
							var round_qty = parseFloat(this.value);
							$(this).val(round_qty.toFixed(QUANTITY_PRECISION));				
							var quan = $("#quantity").val();
							var sellprice = $("#sellingprice").val();
							var gross = parseFloat(quan) * parseFloat(sellprice);
							$("#grossamount").val(gross.toFixed(PRECISION));
							setTimeout(function(){
								var discount_json = $.parseJSON($('#discountdata').val()); //individual discount data					
								var discount_amount=discountcalculation(discount_json,gross); //to calculate discount					
								$('#discountamount').val(discount_amount);
								//pretax-total
								var pretax = parseFloat($('#grossamount').val())-parseFloat($('#discountamount').val());
								$("#pretaxtotal").val(pretax.toFixed(PRECISION));
							},10);	
							update_taxgriddata();
							if(softwareindustryid != 2){
								update_chargegriddata();
							}				
							productnetcalculate();
						}
					}
				}				
			}						
		});
		$("#productsearchsubmit").click(function()
		{
			var selectedrow = $('#productsearchgrid div.gridcontent div.active').attr('id');			
			if(selectedrow){
				$('#productid').select2('val',selectedrow).trigger('change');
				$("#productsearchoverlay").fadeOut();
			}			
		});		
		$("#modulenumberevent").click(function()
		{
			var val = $('#moduleid').val();
			if(val > 0){
				$('#mnsearchoverlay').fadeIn();	
				$("#mnsearchsubmit").focus();
				if($.inArray(val, ["216","85","94"])!= -1) {
					modulenumbersearchgrid(val);
				} else if($.inArray(val, ["217","87","96"]) != -1) {
					modulenumbersearchgrid(val);
				}
			} else {
				alertpopup("Kindly select module to search");
			}
		});
		$("#mnsearchclose").click(function()
		{
			$('#mnsearchoverlay').fadeOut();
			$("#s2id_accountid").select2('focus');
		});
	}	
	{//redirected from quote to invoices.
		var quotetoinvoice = sessionStorage.getItem("convertquotetoinvoice"); 
		if(quotetoinvoice != '' && quotetoinvoice != null) {
			setTimeout(function() {
				sessionStorage.removeItem("convertquotetoinvoice");
				$("#addicon").trigger('click');
				if(softwareindustryid == 1){
					$('#moduleid').select2('val','216').trigger('change');
					var sourcemodule = 216; //quote
				}else if(softwareindustryid == 2){
					$('#moduleid').select2('val','94').trigger('change');
					var sourcemodule = 94; //quote
				}else if(softwareindustryid == 4){
					$('#moduleid').select2('val','85').trigger('change');
					var sourcemodule = 85; //quote
				}				
				var destinatemodule = MODULEID; //invoice
				//modulenumber
				var no = getmodulenumber('quote',quotetoinvoice);
				$('#modulenumber').val(no);	
				//dataretrievals
				conversiondatamapping(quotetoinvoice,sourcemodule,destinatemodule);
				Materialize.updateTextFields();
				
			},100);	
		}
	}
	{//redirect from contact to invoice
		var contactotinvoice = sessionStorage.getItem("convertcontacttoinvoice");
		if(contactotinvoice != '' && contactotinvoice != null) {
			setTimeout(function() {
				sessionStorage.removeItem("convertcontacttoinvoice");
				$("#addicon").trigger('click');
				if(softwareindustryid == 1){
					var sourcemodule = 203; //contact
				}else if(softwareindustryid == 2){
					var sourcemodule = 75; //contact
				}else if(softwareindustryid == 4){
					var sourcemodule = 82; //contact
				}				
				var destinatemodule = MODULEID; //invoice
				conversiondatamapping(contactotinvoice,sourcemodule,destinatemodule);
				Materialize.updateTextFields();				
			},100);	
		}
	}
	{//redirect from contact to invoice
		var contracttoinvoice = sessionStorage.getItem("convertcontracttoinvoice");
		if(contracttoinvoice != '' && contracttoinvoice != null) {
			setTimeout(function() {
				sessionStorage.removeItem("convertcontracttoinvoice");
				$("#addicon").trigger('click');
				if(softwareindustryid == 4){
					var sourcemodule = 80; //contract
				}else{
					var sourcemodule = 230; //contract
				}			
				var destinatemodule = MODULEID; //invoice
				conversiondatamapping(contracttoinvoice,sourcemodule,destinatemodule);
				Materialize.updateTextFields();				
			},100);	
		}
	}
	{//redirected from salesorder to invoices.
		var sotoinvoice = sessionStorage.getItem("convertsalesordertoinvoice"); 
		if(sotoinvoice != '' && sotoinvoice != null) {
			setTimeout(function() {
				sessionStorage.removeItem("convertsalesordertoinvoice");
				$("#addicon").trigger('click');
				if(softwareindustryid == 1){
					$('#moduleid').select2('val','217').trigger('change');
					var sourcemodule = 217; //so
				}else if(softwareindustryid == 2){
					$('#moduleid').select2('val','96').trigger('change');
					var sourcemodule = 96; //so
				}else if(softwareindustryid == 4){
					$('#moduleid').select2('val','87').trigger('change');
					var sourcemodule = 87; //so
				}				
				var destinatemodule = MODULEID; //invoice
				//modulenumber
				var no = getmodulenumber('salesorder',sotoinvoice);
				$('#modulenumber').val(no);	
				//dataretrievals
				conversiondatamapping(sotoinvoice,sourcemodule,destinatemodule);				
				Materialize.updateTextFields();
			},100);	
		}
	}
	{ //Click to call and sms icon code - gowtham
		$("#mobilenumberoverlayclose").click(function() {
			$("#mobilenumbershow").hide();
		});	
		//sms icon click
		$("#smsicon").click(function() {
			var datarowid = $('#leadinvoicegrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $("#viewfieldids").val();
				$.ajax({
					url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						if(data != 'No mobile number') {
							for(var i=0;i<data.length;i++) {
								if(data[i] != '') {
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$("#mobilenumbershow").show();
								$("#smsmoduledivhid").hide();
								$("#smsrecordid").val(datarowid);
								$("#smsmoduleid").val(viewfieldids);
								mobilenumberload(mobilenumber,'smsmobilenumid');
							} else if(mobilenumber.length == 1) {
								sessionStorage.setItem("mobilenumber",mobilenumber);
								sessionStorage.setItem("viewfieldids",viewfieldids);
								sessionStorage.setItem("recordis",datarowid);
								window.location = base_url+'Sms';
							} else {
								alertpopup("Invalid mobile number");
							}
						} else {
							$("#mobilenumbershow").show();
							$("#smsmoduledivhid").show();
							$("#smsrecordid").val(datarowid);
							$("#smsmoduleid").val(viewfieldids);
							moduledropdownload(viewfieldids,datarowid,'smsmodule');
						}
					},
				});
			} else {
				alertpopup("Please select a row");
			}
		});	
		//sms module change
		$("#smsmodule").change(function() {
			var moduleid =	$("#smsmoduleid").val(); //main module
			var linkmoduleid =	$("#smsmodule").val(); // link module
			var recordid = $("#smsrecordid").val();
			if(linkmoduleid != '' || linkmoduleid != null) {
				mobilenumberinformationfetch(moduleid,linkmoduleid,recordid,'smsmobilenumid');
			}
		});
		//call module change
		$("#callmodule").change(function() {
			var moduleid =	$("#callmoduleid").val(); //main module
			var linkmoduleid =	$("#callmodule").val(); // link module
			var recordid = $("#callrecordid").val();
			if(linkmoduleid != '' || linkmoduleid != null) {
				mobilenumberinformationfetch(moduleid,linkmoduleid,recordid,'callmobilenum');
			}
		});
		//sms mobile number change
		$("#smsmobilenumid").change(function() {
			var data = $('#smsmobilenumid').select2('data');
			var finalResult = [];
			for( item in $('#smsmobilenumid').select2('data')) {
				finalResult.push(data[item].id);
			};
			var selectid = finalResult.join(',');
			$("#smsmobilenum").val(selectid);
		});
		//sms mobile overlay submit
		$("#mobilenumbersubmit").click(function() {
			var datarowid = $("#smsrecordid").val();
			var viewfieldids =$("#smsmoduleid").val();
			var mobilenumber =$("#smsmobilenum").val();
			if(mobilenumber != '' || mobilenumber != null) {
				sessionStorage.setItem("mobilenumber",mobilenumber);
				sessionStorage.setItem("viewfieldids",viewfieldids);
				sessionStorage.setItem("recordis",datarowid);
				window.location = base_url+'Sms';
			} else {
				alertpopup('Please Select the mobile number...');
			}	
		});
		//click to call submit
		$("#outgoingcallicon").click(function() {
			var datarowid = $('#leadinvoicegrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $("#viewfieldids").val();
				$.ajax({
					url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						if(data != 'No mobile number') {
							for(var i=0;i<data.length;i++){
								if(data[i] != ''){
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$("#c2cmobileoverlay").show();
								$("#callmoduledivhid").hide();
								$("#calcount").val(mobilenumber.length);
								$("#callrecordid").val(datarowid);
								$("#callmoduleid").val(viewfieldids);
								mobilenumberload(mobilenumber,'callmobilenum');
							} else if(mobilenumber.length == 1){
								clicktocallfunction(mobilenumber);
							} else {
								alertpopup("Invalid mobile number");
							}
						} else {
							$("#c2cmobileoverlay").show();
							$("#callmoduledivhid").show();
							$("#callrecordid").val(datarowid);
							$("#callmoduleid").val(viewfieldids);
							moduledropdownload(viewfieldids,datarowid,'callmodule');
						}
					},
				});
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#c2cmobileoverlayclose").click(function() {
			$("#c2cmobileoverlay").hide();
		});
		//click to call submit
		$("#callnumbersubmit").click(function()  {
			var mobilenum = $("#callmobilenum").val();
			if(mobilenum == '' || mobilenum == null) {
				alertpopup("Please Select the mobile number to call");
			} else {
				clicktocallfunction(mobilenum);
			}
		});
	}
	{//filter
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,leadinvoicegrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
	}
});
	{// Get and Set date
		function getandsetdate(dateset){ 
			$('#paymentduedate').datetimepicker('option', 'minDate', dateset);
		}
	}

{// view create success function
    function viewcreatesuccfun(viewname)
	{
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');			
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
{// Main view Grid
	function leadinvoicegrid(page,rowcount) {
		if(Operation == 1){
			var rowcount = $('#invoicespgrowcount').val();
			var page = $('.paging').data('pagenum');
		} else{
			var rowcount = $('#invoicespgrowcount').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		}
		var wwidth = $('#leadinvoicegrid').width();
		var wheight = $(window).height();
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.invoicesheadercolsort').hasClass('datasort') ? $('.invoicesheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.invoicesheadercolsort').hasClass('datasort') ? $('.invoicesheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.invoicesheadercolsort').hasClass('datasort') ? $('.invoicesheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = $('#viewfieldids').val();
		var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
		var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
		var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=invoice&primaryid=invoiceid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#leadinvoicegrid').empty();
				$('#leadinvoicegrid').append(data.content);
				$('#leadinvoicegridfooter').empty();
				$('#leadinvoicegridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('leadinvoicegrid');
				{//sorting
					$('.invoicesheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.invoicesheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#invoicespgnumcnt li .page-text .active').data('rowcount');
						var sortcol = $('.invoicesheadercolsort').hasClass('datasort') ? $('.invoicesheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.invoicesheadercolsort').hasClass('datasort') ? $('.invoicesheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						var sortpos = $('#leadinvoicegrid .gridcontent').scrollLeft();
						leadinvoicegrid(page,rowcount);
						$('#leadinvoicegrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('invoicesheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						Operation = 0;
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						leadinvoicegrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#invoicespgrowcount').change(function(){
						Operation = 0;
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = 1;//$('#prev').data('pagenum');
						leadinvoicegrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//onclick 
				$(".gridcontent").click(function(){
					var datarowid = $('#leadinvoicegrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				{// Redirected form Home
					var invoiceideditvalue = sessionStorage.getItem("invoiceidforedit"); 
					if(invoiceideditvalue != null){
						setTimeout(function(){ 
							invoiceeditdatafetchfun(invoiceideditvalue);
							sessionStorage.removeItem("invoiceidforedit");
						},100);	
					}
				}
				//Material select
				$('#invoicespgrowcount').material_select();
			},
		});
	}
}
{
	function getmoduleid(){
		$.ajax({
			url:base_url+'Invoice/getmoduleid',
			async:false,
			cache:false,
			success: function(data) {
				MODULEID=data;
			}
		});
	}
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function(e) {
	        invoicesproaddgrid1();
			invoicespayaddgrid2();
			e.preventDefault();
			// Append The Currency data
			appendcurrency('currencyid');
			addslideup('invoicecreationview','invoicecreationformadd');
			resetFields();
			COMPANY_NAME = getcompanyname();
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			showhideiconsfun('addclick','invoicecreationformadd');
			$("#invoicesproingriddel1,#invoicespayingriddel2").show();	
			$(".fr-element").attr("contenteditable", 'true');
			$("#pricebookclickevent,#productsearchevent").show();
			//form field first focus
			firstfieldfocus();
			//for autonumber
            var elementname = $('#elementsname').val();
		    elementdefvalueset(elementname);
		    $('#pricebookclickevent').show;
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			setTimeout(function() {
				$("#termsandconditionid").trigger('change');
			},10);
			froalaset(froalaarray);
			$('#paymentto').val(COMPANY_NAME);
			clearformgriddata();
			if(softwareindustryid != 2){
				$("#productid").empty();
			}
			$('#defaultcurrency').select2('val','');
			$('#convcurrency').select2('val','');
			$('#currencyconv').val('');
			$('.resetoverlay').find('input[type=text],select,input[type=hidden]').val('');			
			setdefaultproperty();
			$("#moduleid,#modulenumber").removeAttr("readonly");
			//disable fields
			$('#ordertypeid,#additionalchargecategoryid,#taxmasterid,#pricebookid,#accountid,#contactid,#opportunityid,#mname').select2("val",'');
			$('#ordertypeid,#additionalchargecategoryid,#taxmasterid,#pricebookid,#accountid,#contactid,#opportunityid,#mname').select2("readonly", false);
			$('#currentmode').val(1);
			var invdate = $("#invoicedate").val();
			getandsetdate(invdate);
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			fortabtouch = 0;
			METHOD = 'ADD';
			Materialize.updateTextFields();
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
		});
		$("#editicon").click(function(e) {
			e.preventDefault();
			QUANTITY_PRECISION =0;
			var datarowid = $('#leadinvoicegrid div.gridcontent div.active').attr('id');
			if (datarowid) {
		        invoicesproaddgrid1();
				invoicespayaddgrid2();
				// Append The Currency data
				appendcurrency('currencyid');
				$('#pricebookclickevent').show;
				var stage=invoicestage(datarowid);
				if(stage == 38){ //Booked
					alertpopup("This Invoice is Booked !!!");
				} else if(stage == 39){ //cancel
					alertpopup("This Invoice is Cancelled");
				} else {
					$("#processoverlay").show();
					froalaset(froalaarray);
					invoiceeditdatafetchfun(datarowid);
					$('#paymentto').val(COMPANY_NAME);
					showhideiconsfun('editclick','invoicecreationformadd');
					$("#invoicesproingriddel1,#invoicespayingriddel2").show();
					$(".fr-element").attr("contenteditable", 'true');
					fortabtouch = 1;
					METHOD = 'ADD';
					//For tablet and mobile Tab section reset
					$('#tabgropdropdown').select2('val','1');
				}
				Operation = 1; //for pagination
			} else {
				alertpopup("Please select a row");			
			}
			Materialize.updateTextFields();
		});
		$("#deleteicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#leadinvoicegrid div.gridcontent div.active').attr('id');
			if(datarowid){		
				var stage=invoicestage(datarowid);
				if(stage == 38){ //Booked
					alertpopup("Invoice Booked!!!Cannot Delete!!");
				} else if(stage == 39){ //Cancel
					alertpopup("This Invoice is Cancelled");
				} 
				else {
					$("#basedeleteoverlay").fadeIn();
					$('#primarydataid').val(datarowid);
					$("#basedeleteyes").focus();
				}
			}
			else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function() {
			var datarowid = $('#primarydataid').val();
			invoicemaindelete(datarowid);
		});
	}
}
/*
* reappend currency for dataattribute-(used because there is no data attribute in base dd)
*/
function appendcurrency(id) {
	$('#'+id+'').empty();
	$('#'+id+'').append($("<option></option>"));
	$.ajax({
		url: base_url+"index.php/Quote/specialcurrency",
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {				
			if((data.fail) == 'FAILED') {
			} else {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++) {
					newddappend += "<option data-precision ='" +data[m]['decimal']+ "'  value = '" +data[m]['currencyid']+ "'>"+data[m]['currencyname']+"</option>";
				}
				//after run
				$('#'+id+'').append(newddappend);
			}	
		},
	});
}
{//refresh grid
	function refreshgrid() {
		var page = $(this).data('pagenum');
		var rowcount = $('ul#invoicespgnumcnt li .page-text .active').data('rowcount');
		leadinvoicegrid(page,rowcount);
	}
}
{// Invoice product grid
	function invoicesproaddgrid1() 
	{
		var wwidth = $("#invoicesproaddgrid1").width();
		var wheight = $("#invoicesproaddgrid1").height();
		var tabgroupid = 0;
		if(MODULEID == 226){
			tabgroupid = 64;
		}else if(MODULEID == 95){
			tabgroupid = 251;
		}else if(MODULEID == 86){
			tabgroupid = 215;
		}
		$.ajax({
			url:base_url+"Base/localgirdheaderinformationfetch?tabgroupid="+tabgroupid+"&moduleid="+MODULEID+"&width="+wwidth+"&height="+wheight+"&modulename=invoiceproduct",
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$("#invoicesproaddgrid1").empty();
				$("#invoicesproaddgrid1").append(data.content);
				$("#invoicesproaddgrid1footer").empty();
				$("#invoicesproaddgrid1footer").append(data.footer);
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('invoicesproaddgrid1');
				$("#durationiddivhid,#joiningdatedivhid,#expirydatedivhid").addClass('hidedisplay');
				gridfieldhide('invoicesproaddgrid1',['durationidname','durationid','expirydate','joiningdate']);
				var hideprodgridcol = ['taxgriddata','chargegriddata','discountdata','invoicedetailid'];
				gridfieldhide('invoicesproaddgrid1',hideprodgridcol);
			},
		});		
		}
}
{// Invoice payment grid
	function invoicespayaddgrid2() 
	{
		var wwidth = $("#invoicespayaddgrid2").width();
		var wheight = $("#invoicespayaddgrid2").height();
		var tabgroupid = 0;
		if(MODULEID == 226){
			tabgroupid = 81;
		}else if(MODULEID == 95){
			tabgroupid = 253;
		}else if(MODULEID == 86){
			tabgroupid = 217;
		}
		$.ajax({
			url:base_url+"Base/localgirdheaderinformationfetch?tabgroupid="+tabgroupid+"&moduleid="+MODULEID+"&width="+wwidth+"&height="+wheight+"&modulename=invoicepayment",
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$("#invoicespayaddgrid2").empty();
				$("#invoicespayaddgrid2").append(data.content);
				$("#invoicespayaddgrid2footer").empty();
				$("#invoicespayaddgrid2footer").append(data.footer);
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('invoicespayaddgrid2');
				var hideprodgridcol = ['paymentid'];
				gridfieldhide('invoicespayaddgrid2',hideprodgridcol);
			},
		});

	}
}	
{// New data add submit function
	function invoicecreate(){
		var amp = '&';		
		//
		var addgriddata='';
		var gridname = $('#gridnameinfo').val();
		if(gridname != '') {
			var gridnames  = [];
			var gridnames = gridname.split(',');
			var datalength = gridnames.length;
			var noofrows=0;
			var addgriddata='';
			if(deviceinfo != 'phone'){
				for(var j=0;j<datalength;j++) {
					if(j!=0) {
						addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
						noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
					} else {
						addgriddata = getgridrowsdata(gridnames[j]);
						noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
					}
				}
			}
			else{
				for(var j=0;j<datalength;j++) {
					if(j!=0) {
						addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
						noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
					} else {
						addgriddata = getgridrowsdata(gridnames[j]);
						noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
					}
				}
			}			
		}		
		var sendformadddata = JSON.stringify(addgriddata); 
		var tandcdata = 0;
		var noofrows = noofrows;		
		var elementsname = $('#elementsname').val();
		var elementstable = $('#elementstable').val();
		var elementscolmn = $('#elementscolmn').val();
		var elementspartabname = $('#elementspartabname').val();
		var griddatapartabnameinfo = $('#griddatapartabnameinfo').val();
		var resctable = $('#resctable').val();			
		var conv_arr = CONV_ARR;
		//editor data
		var editorname = $('#editornameinfo').val();
		var editordata = froalaeditoradd(editorname);
		var formdata = $("#dataaddform").serialize();		
		var datainformation = amp + formdata;
		$.ajax({
			url: base_url + "Invoice/invoicecreate",
			data: "datas=" + datainformation+amp+"griddatas="+sendformadddata+amp+"numofrows="+noofrows+amp+"elementsname="+elementsname+amp+"elementstable="+elementstable+amp+"elementscolmn="+elementscolmn+amp+"elementspartabname="+elementspartabname+amp+"resctable="+resctable+amp+"griddatapartabnameinfo="+griddatapartabnameinfo+amp+"tandcdata="+tandcdata+amp+"conv_arr="+conv_arr+amp+"invoicemodule="+MODULEID,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					$(".ftab").trigger('click');				
					resetFields();
					$('#invoicecreationformadd').hide();
					$('#invoicecreationview').fadeIn(1000);
					refreshgrid();	
					clearformgriddata();
					$('.resetoverlay').find('input[type=text],select,input[type=hidden]').val('');
					$('#additionalcategory,#discountcalculationtype').select2('val','').trigger('change');
					alertpopup(savealert);
					$('#quantity').addClass('validate[required,custom[number],decval[3],maxSize[100]]');
					$('#sellingprice').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#netamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#productid').addClass('validate[required]');
					$('#grossamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#paymentdate').addClass('validate[required]');
					$('#paymenttypeid').addClass('validate[required]');
					$('#paymentmethodid').addClass('validate[required]');
					$('#paymentamount').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
					//For Keyboard Shortcut Variables
					viewgridview = 1;
					addformview = 0;
					$("#processoverlay").hide();
				} else if (msg == "Disallowed Key Characters.") {
					alertpopup("Restricted symbols (&^!#()) used.Please remove and try again");
					$("#processoverlay").hide();
				} else {
					alertpopup("Error during create.Please refresh and try again");
					$("#processoverlay").hide();
				}
				$('#dataaddsbtn').removeClass('singlesubmitonly');
			},
		});
	}
}
{// Old information show in form
	function retrieveinvoicedata(datarowid)
	{
		var elementsname = $('#elementsname').val();
		var elementstable = $('#elementstable').val();
		var elementscolmn = $('#elementscolmn').val();
		var elementpartable = $('#elementspartabname').val();
		var resctable = $('#resctable').val();
		$.ajax({
			url:base_url+"Invoice/fetchformdataeditdetails?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable+"&resctable="+resctable+"&invoicemodule="+MODULEID,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {				
				if((data.fail) == 'Denied') {
					alertpopup('Permission denied');
					$(".ftab").trigger('click');
					$('#invoicecreationformadd').hide();
					$('#invoicecreationview').fadeIn(1000);
					refreshgrid()
					//For Keyboard Shortcut Variables
					addformupdate = 0;
					viewgridview = 1;
					addformview = 0;
					//For Left Menu Confirmation
					discardmsg = 0;
					$("#processoverlay").hide();
				} else {
					addslideup('invoicecreationview','invoicecreationformadd');					
					var txtboxname = elementsname + ',primarydataid';
					var textboxname = {};
					textboxname = txtboxname.split(',');
					var dropdowns = [];
					textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
					$('#pricebookcurrency').val(data['pricebookcurrencyid']);
					$('#pricebook_currencyconv').val(data['pricebook_currencyconvrate']);
					$('#currentcurrency').val(data['currentcurrencyid']);
					$('#currencyconv').val(data['currencyconvresionrate']);
					$('#convcurrency').val(data['currencyid']);
					$('#pricebookcurrencyid').val(data['pricebookcurrencyid']);
					$('#pricebook_currencyconvrate').val(data['pricebook_currencyconvrate']);
					$('#currentcurrencyid').val(data['currentcurrencyid']);
					PRICBOOKCONV_VAL = data['pricebook_currencyconvrate'];
					var attachfldname = $('#attachfieldnames').val();
					var attachcolname = $('#attachcolnames').val();
					var attachuitype = $('#attachuitypeids').val();
					var attachfieldid = $('#attachfieldids').val();
					attachedfiledisplay(attachfldname,attachcolname,attachuitype,attachfieldid,data);
					var ext_data = data.summary;
					//summary data settings//
					{
						$('#grouptaxgriddata').val(ext_data['grouptax']);
						$('#groupchargegriddata').val(ext_data['groupaddcharge']);
						$('#groupadjustmentdata').val(ext_data['groupadjustment']);
						$('#groupdiscountdata').val(ext_data['groupdiscount']);
						$('#paidamount').val(ext_data['paidamount']);
						$('#writeoffamount').val(ext_data['writeoffamount']);
						$('#balanceamount').val(ext_data['balanceamount']);
						$('#totalpayable').val(ext_data['totalpayable']);
					}					
					var pid = data['parentinvoiceid'];
					if(softwareindustryid == 4){		    			
		        		if(data['ordertypeid'] == 5){
		        			$('#quantity').removeClass('validate[required,custom[number],decval[3],maxSize[100]]');
		        			$("#instockdivhid").addClass('hidedisplay');
		        			$('#quantity').attr("readonly", true);
		        			$("#durationiddivhid,#joiningdatedivhid,#expirydatedivhid").removeClass('hidedisplay');
		        			gridfieldhide('invoicesproaddgrid1',['instock']);
		        			gridfieldshow('invoicesproaddgrid1',['durationidname','expirydate','joiningdate']);
		        		}else{
		        			$("#durationiddivhid,#joiningdatedivhid,#expirydatedivhid").addClass('hidedisplay');
		        			$("#instockdivhid").removeClass('hidedisplay');
		        			$('#quantity').attr("readonly", false);
		        			gridfieldshow('invoicesproaddgrid1',['instock']);
		        			gridfieldhide('invoicesproaddgrid1',['durationidname','expirydate','joiningdate']);
		        		}
		    		}
					invoicegriddetail(datarowid);				
					primaryaddvalfetch(datarowid);
					editordatafetch(froalaarray,data);
					$("#processoverlay").hide();
				}
			}
		});	
	}
}
{// Salesorder product and payment detail
	function invoicegriddetail(pid) 
	{	
		//salesorder product detail		
		if(pid!='') {
			$.ajax({
				url:base_url+'Invoice/invoiceproductdetailfetch?primarydataid='+pid+"&invoicemodule="+MODULEID,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if((data.fail) == 'FAILED') {
					} else {
						loadinlinegriddata('invoicesproaddgrid1',data.rows,'json');
						/* data row select event */
						datarowselectevt();
						/* column resize */
						columnresize('invoicesproaddgrid1');
						var hideprodgridcol = ['taxgriddata','chargegriddata','discountdata','invoicedetailid'];
						gridfieldhide('invoicesproaddgrid1',hideprodgridcol);
					}
				},
			});
		}
		//salesorder payment detail		
		if(pid!='') {
			$.ajax({
				url:base_url+'Invoice/retrievepaymentdetail?primarydataid='+pid+"&invoicemodule="+MODULEID,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if((data.fail) == 'FAILED') {
					} else {
						loadinlinegriddata('invoicespayaddgrid2',data.rows,'json');
						/* data row select event */
						datarowselectevt();
						/* column resize */
						columnresize('invoicespayaddgrid2');
						var hideprodgridcol = ['paymentid'];
						gridfieldhide('invoicespayaddgrid2',hideprodgridcol);
					}
				},
			});
		}
	}
}
{// Primary address value fetch function 
	function primaryaddvalfetch(datarowid) {
		$.ajax({
			url:base_url+"Invoice/primaryaddressvalfetch?dataprimaryid="+datarowid+"&addtype=Billing", 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				var datanames = ['12','billingaddresstype','billingaddress','billingpincode','billingcity','billingstate','billingcountry','shippingaddresstype','shippingaddress','shippingpincode','shippingcity','shippingstate','shippingcountry'];
				var textboxname = ['12','billingaddresstype','billingaddress','billingpincode','billingcity','billingstate','billingcountry','shippingaddresstype','shippingaddress','shippingpincode','shippingcity','shippingstate','shippingcountry'];
				var dropdowns = ['2','billingaddresstype','shippingaddresstype'];
				textboxsetvalue(textboxname,datanames,data,dropdowns); 
			}
		});
	}
}
{// Update old information
	function invoiceupdate()
	{
		var amp = '&';
		var gridname = $('#gridnameinfo').val();
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		var noofrows=0;
		var addgriddata='';
		if(deviceinfo != 'phone'){
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				}
			}
		}else{
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				}
			}
		}		
		var sendformadddata = JSON.stringify(addgriddata); 
		var noofrows = noofrows;
		//editor data
		var editorname = $('#editornameinfo').val();
		var editordata = froalaeditoradd(editorname);
		var resctable = $('#resctable').val();	
		var formdata = $("#dataaddform").serialize();
		var datainformation = amp + formdata;		
		$.ajax({
			url: base_url + "Invoice/datainformationupdate",
			data: "datas=" + datainformation+amp+"griddatas="+sendformadddata+amp+"numofrows="+noofrows+"&invoicemodule="+MODULEID,
			type: "POST",
			cache:false,
			success: function(msg){
				if (msg == true){
					$(".ftab").trigger('click');				
					resetFields();
					$('#invoicecreationformadd').hide();
					$('#invoicecreationview').fadeIn(1000);
					refreshgrid()
					clearformgriddata();
					$('.resetoverlay').find('input[type=text],select,input[type=hidden]').val('');
					$('#additionalcategory,#discountcalculationtype,#taxmasterid,#additionalchargecategoryid,#summarydiscountcalculationtype').select2('val','').trigger('change');
					alertpopup(savealert);
					$('#quantity').addClass('validate[required,custom[number],decval[3],maxSize[100]]');
					$('#sellingprice').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#netamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#productid').addClass('validate[required]');
					$('#grossamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#paymentdate').addClass('validate[required]');
					$('#paymenttypeid').addClass('validate[required]');
					$('#paymentmethodid').addClass('validate[required]');
					$('#paymentamount').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
					//For Keyboard Shortcut Variables
					addformupdate = 0;
					viewgridview = 1;
					addformview = 0;
					$("#processoverlay").hide();
				}else if (msg == "Disallowed Key Characters.") {
					alertpopup("Restricted symbols (&^!#()) used.Please remove and try again");
					$("#processoverlay").hide();
				} else {
					alertpopup("Error during update.Please close and try again");
					$("#processoverlay").hide();
				}
				$('#dataupdatesubbtn').removeClass('singlesubmitonly'); 			
			},
		});
	}
}
{// Record delete function
	function invoicemaindelete(datarowid)
	{
		var elementstable = $('#elementstable').val();
		var elementspartable = $('#elementspartabname').val();
		$.ajax({
			url: base_url + "Invoice/invoicedelete?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable+"&invoicemodule="+MODULEID,
			cache:false,
			success: function(msg)  {
				var nmsg =  $.trim(msg);
				refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				if (nmsg == "TRUE") { 					
					alertpopup('Deleted successfully');
				} else if (nmsg == "Denied") {					
					alertpopup('Permission denied');
				}
			},
		});
	}
}
{// Setaddress
	function setaddress(id){
		var value=$('#'+id+'').val();
		if(id == 'billingaddresstype'){
		var type='Billing';
		}
		else{
		var type='Shipping';
		}
		//account address
		if(value == 2){
			var accountid=$('#accountid').val();
			getaddress(accountid,'account',type);
			Materialize.updateTextFields();
		}
		else if(value == 3){   			
			var addressid =$('#contactid').val();
			getaddress(addressid,'contact',type);
			Materialize.updateTextFields();
		}
		//swap the billing to shipping
		else if(value == 5){
			$('#billingaddress').val($('#shippingaddress').val());
			$('#billingpincode').val($('#shippingpincode').val());
			$('#billingcity').val($('#shippingcity').val());
			$('#billingstate').val($('#shippingstate').val());
			$('#billingcountry').val($('#shippingcountry').val());
			Materialize.updateTextFields();
		}
		//swap the shipping to billing
		else if(value == 6){
			$('#shippingaddress').val($('#billingaddress').val());
			$('#shippingpincode').val($('#billingpincode').val());
			$('#shippingcity').val($('#billingcity').val());
			$('#shippingstate').val($('#billingstate').val());
			$('#shippingcountry').val($('#billingcountry').val());
			Materialize.updateTextFields();
		}
	}
}
{// Get Address
	function getaddress(id,table,type)
	{

		if(id != null && id != '' && table != null && table !='')
		{
			var append=type.toLowerCase();
			$.ajax({
			url:base_url+'Base/getcrmaddress?table='+table+'&id='+id+'&type='+type,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) 
			{
				if(data.result == true)
				{
					var datanames = ['5','address','pincode','city','state','country'];
					var textboxname = ['5',''+append+'address',''+append+'pincode',''+append+'city',''+append+'state',''+append+'country'];
					textboxsetvalue(textboxname,datanames,data,[]);
				}			
			},
			});
		}
	}
}
{// Terms and condition data fetch
	function tandcdatafrtch(id)
	{
		$.ajax({
				url:base_url+"Invoice/termsandcontdatafetch?id="+id,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data)
				{
					filenamevaluefetch(data);
				},
			});
	}
}
{// Editor value fetch
	function filenamevaluefetch(filename)
	{
		//checks whether the Terms & condition is empty
		if(filename == '' || filename == null )
		{			
			if($("#invoicestc_editor").length){
				froalaset(froalaarray);
			}
		}
		else{
			$.ajax({
				url: base_url + "Invoice/editervaluefetch?filename="+filename,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) 
				{
					froalaedit(data,'invoicestc_editor');
				},
			});
		}	
	}
}
{// Get product details - quote
	function getproductdetails(val)
	{
		if(checkValue(val) == true){
		var pricebook = $('#pricebookid').val();
		var uomurl=base_url+'Uommaster';
		$.ajax({
			url:base_url+'Base/getproductdetails?id='+val+'&pricebook='+pricebook,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data){
					var datanames = ['1','description'];
					var textboxname = ['1','descriptiondetail'];
					textboxsetvalue(textboxname,datanames,data,[]);
					productstoragefetch(val);
					if(jQuery.type(data.symbol) === "undefined"){
					}else{
						$("#instockdivhid label").html('Stock Quantity  <a href="'+uomurl+'" style="color:#575757;">('+data.symbol+')</a>'); //set the specified product's uom symbol with link
					}
					QUANTITY_PRECISION = data.uomprecision; //set's the specified product quantity rounding.
					PRODUCTTAXABLE = data.taxable;
					var unitprice = data.unitprice;
					if(unitprice == ''){
						var unitprice = 0;
					}							
					if(softwareindustryid == 4){
						if(ordertypeid == 5){
							$("#durationid").select2('val',data.durationid);							
						}else{				
							$("#durationid").select2('val',2);
						}
					}
					if(data.prodinprice == 0){
						$("#pricebookclickevent").show();
						var currencyconvresionrate =$('#currencyconvresionrate').val();
						if(softwareindustryid == 2){
							if(data.sellingprice != ''){
								var list_price = listprice(data.sellingprice,currencyconvresionrate);
								$('#sellingprice').val(list_price);
							}else if(data.unitprice != ''){
								var list_price = listprice(data.unitprice,currencyconvresionrate);
								$('#sellingprice').val(list_price);
							} else{
								$('#sellingprice').val(0);
							}
						}else{
							var list_price = listprice(unitprice,currencyconvresionrate);
							$('#unitprice').val(list_price);					
							//calculate selling price
							var list_price = listprice(data.sellingprice,currencyconvresionrate);
							$('#sellingprice').val(list_price);
						}
					}else{
						$("#pricebookclickevent").hide();
						var currencyconvresionrate =$('#currencyconvresionrate').val();
						var sellcurrencyconvresionrate =PRICBOOKCONV_VAL;
						if(softwareindustryid == 2){
							if(data.sellingprice != ''){
								var conversell = listprice(data.sellingprice,currencyconvresionrate);
								var list_price = listprice(conversell,sellcurrencyconvresionrate);
								$('#sellingprice').val(list_price);
							}else if(data.unitprice != ''){
								var list_price = listprice(data.unitprice,sellcurrencyconvresionrate);
								$('#sellingprice').val(list_price);
							} else{
								$('#sellingprice').val(0);
							}
						}else{
							var list_price = listprice(unitprice,currencyconvresionrate);
							$('#unitprice').val(list_price);					
							//calculate selling price
							var conversell = listprice(data.sellingprice,currencyconvresionrate);
							var list_price = listprice(conversell,sellcurrencyconvresionrate);
							$('#sellingprice').val(list_price);
						}
					}																	
					$('#quantity').val('1');									
					//gross amount
					$("#grossamount").val($('#sellingprice').val());
					//pretax-total
					setzero(['grossamount','discountamount']);
					var pretax = parseFloat($('#grossamount').val())-parseFloat($('#discountamount').val());
					$("#pretaxtotal").val(pretax.toFixed(PRECISION));
					productnetcalculate();
			},
		});
		}
	}
}
{// Discount calculation
	
	function itemcalculation()
	{
		setzero(['sellingprice','discountamount','taxamount','additionalamount','grossamount']);
		var sp=parseFloat($('#grossamount').val());
		var discount=parseFloat($('#discountamount').val());
		var tax=parseFloat($('#taxamount').val());
		if(isNaN(tax) || tax == "")
		{
			tax = 0;
		}
		var addamt=parseFloat($('#additionalamount').val());
		if(isNaN(addamt) || addamt=="")
		{
			addamt = 0;
		}		
		var output=parseFloat(sp+tax+addamt-discount);
		$('#netamount').val(output.toFixed(2));	
		$("#discountoverlay").fadeOut();
	}
	
}
{// Tax overlay selling value assign
	function sellingvalueset(taxvalue,i)
	{
		var selprice = $("#sellingprice").val();
		var value = (parseFloat(taxvalue) / 100);
		var taxtot = (parseFloat(value) * parseFloat(selprice));
		$("#sellingamt"+i).val(taxtot);
		var selamt = $("#sellingamt"+i).val();
		quoteseiprice.push(selamt);
		quotetaxvalue.push(taxvalue);
	}
}
{// Value assign function
	function valueasignfunction()
	{
		var selvalue = (parseFloat($("#sellingamt0").val())+parseFloat($("#sellingamt1").val()));
		$("#totalselling").val(selvalue);
		$("#taxamount").val(selvalue);
	}
}

{// Additional amount add
	function valueassignforadditional(value,cval)
	{
		var addamount = $("#addamount"+cval+"").val();
		var sellingamount = $("#sellingprice").val();
		if(value ==2)
		{
			var addval = parseFloat(addamount)+parseFloat(sellingamount);
			$("#addsellingamt"+cval+"").val(addval);
		}
		else if(value == 3)
		{
			var sellingamount = $("#sellingprice").val();
			var addvalue = (parseFloat(addamount) / 100);
			var addamttot = parseFloat(addvalue) * parseFloat(sellingamount);
			$("#addsellingamt"+cval+"").val(addamttot.toFixed());
		}
	}
}
{// Product Net Calculation	
	function productnetcalculate()
	{
		setzero(['grossamount','discountamount','taxamount','chargeamount']);
		var grossamount=parseFloat($('#grossamount').val());
		var discount=parseFloat($('#discountamount').val());	
		var tax=parseFloat($('#taxamount').val());
		var charge=parseFloat($('#chargeamount').val());
		if(softwareindustryid == 2){
			var output=(grossamount-discount+tax).toFixed(PRECISION);
		}else{
			var output=(grossamount-discount+tax+charge).toFixed(PRECISION);
		}
		$('#netamount').val(output);	
	}
}
{// Grid summery value fetch
	function gridsummeryvaluefetch(gridname,gridsumcolname,sumfieldid)
	{
		var taxmode = '';
		var taxmasterid = $("#taxmasterid").val();
		if(taxmasterid){
			taxmode = 3;
		}else{
			taxmode = 2;
		}		
		var totalnetamount = parseFloat(getgridcolvalue('invoicesproaddgrid1','','netamount','sum'));
        $('#totalnetamount').val(totalnetamount.toFixed(PRECISION));
		//pretax-total
		setzero(['totalnetamount','groupdiscountamount']);
		var pretax = parseFloat($('#totalnetamount').val())-parseFloat($('#groupdiscountamount').val());
		$("#grouppretaxtotal").val(pretax);
		//update group discount.
		var gross = totalnetamount;
		var discount_json = $.parseJSON($('#groupdiscountdata').val()); //individual discount data					
		var discount_amount=discountcalculation(discount_json,gross); //to calculate discount					
		$('#groupdiscountamount').val(discount_amount);
		if(taxmode == 3){
			//?update the tax overlaydata
			update_group_taxgriddata();
		}
		if(softwareindustryid != 2){
			var additionalchargecategoryid = $("#additionalchargecategoryid").val();
			var chargemode = '';
			if(additionalchargecategoryid){
				chargemode = 3;
			}else{
				chargemode = 2;
			}
			if(chargemode == 3){
				//?update the charge overlaydata
				update_group_chargegriddata();
			}
		}		
		setTimeout(function(){
			calculatesummarydetail();
		},5); 
	}	
}
{//group special function autoset the tax and charges in the summary
	$("#taxmasterid").change(function() {
		taxgrid();
		if(checkVariable('taxmasterid') == true){
			//identify the type(group/individual);
			var type = '';
			var taxmasterid = $('#taxmasterid').val();
			if(taxmasterid){
				type = 3;
			}else{
				type = 2;
			}
			if(type == 3){
				$('#grouptaxgriddata').val('');
				setTimeout(function() {
					$('#taxcategory').select2('val',$('#taxmasterid').val()).trigger('change');
				},200);
				setTimeout(function() {
					var griddataid = 'grouptaxgriddata';
					var tax_category_id = 'taxmasterid';
					var tax_amount_field = 'grouptaxamount';
					var tax_json ={}; //declared as object
					tax_json.id = $('#'+tax_category_id+'').val(); //categoryid
					tax_json.data = getgridrowsdata("taxgrid");//taxgrid				
					$('#'+griddataid+'').val(JSON.stringify(tax_json));
					var totalcharge = parseFloat(getgridcolvalue('taxgrid','','amount','sum'));
					$('#'+tax_amount_field+'').val(totalcharge.toFixed(PRECISION));
					calculatesummarydetail();
				},1000);
			}				
		}
	});
	$("#additionalchargecategoryid").change(function() {
		chargesgrid();
		if(checkVariable('additionalchargecategoryid') == true){				
			$('#groupchargegriddata').val('');
			setTimeout(function() {
				$('#chargecategory').select2('val',$('#additionalchargecategoryid').val()).trigger('change');
			},200);
			setTimeout(function() {
				var griddataid = 'groupchargegriddata';
				var charge_category_id = 'additionalchargecategoryid';
				var charge_amount_field = 'groupchargeamount';	
				var charge_json ={}; //declared as object
				charge_json.id = $('#'+charge_category_id+'').val(); //categoryid
				charge_json.data = getgridrowsdata('chargesgrid');	//chargesgrid			
				$('#'+griddataid+'').val(JSON.stringify(charge_json));
				var totalcharge = parseFloat(getgridcolvalue('chargesgrid','','amount','sum'));
				$('#'+charge_amount_field+'').val(totalcharge.toFixed(PRECISION));
				calculatesummarydetail();
			},1000);
		}
	});
}
{// Summary value calculation
	function calculatesummarydetail()
	{		
		setzero(['totalnetamount','groupdiscountamount','grouptaxamount','groupchargeamount','adjustmentamount','writeoffamount']);
		var totalnetamount = parseFloat($("#totalnetamount").val());
		var discount = parseFloat($("#groupdiscountamount").val());
		var tax = parseFloat($("#grouptaxamount").val());
		var charge = parseFloat($("#groupchargeamount").val());
		var adjustment =parseFloat($("#adjustmentamount").val());
		var writeoffamount =parseFloat($("#writeoffamount").val());
		if(softwareindustryid == 2){
			var pre_adjustment_total = totalnetamount - discount + tax;
		}else{
			var pre_adjustment_total = totalnetamount - discount + tax + charge	;
		}
		if(adjustment > 0){
			var adjustment_data=$('#groupadjustmentdata').val();			
			var parse_adjustment = $.parseJSON(adjustment_data);
			if(parse_adjustment != null){
				if(parse_adjustment.typeid == 3){
					var grandtotal = pre_adjustment_total - adjustment ;
				} else {
					var grandtotal = pre_adjustment_total + adjustment;
				}
			}
		} else {
			var grandtotal=pre_adjustment_total;
		}
		$("#grandtotal,#totalpayable").val(parseFloat(grandtotal).toFixed(PRECISION));	
		//special amount fields
		var paidamount = parseFloat(getgridcolvalue('invoicespayaddgrid2','','paymentamount','sum'));		
		$("#paidamount").val(paidamount.toFixed(PRECISION));
		var balanceamount = parseFloat(grandtotal)-parseFloat(paidamount)-parseFloat(writeoffamount) ;
		$("#balanceamount").val(balanceamount.toFixed(PRECISION));		
		Materialize.updateTextFields();
	}	
}
{// Summary Detail Edit
	function setsummarydataedit(data)
	{
		var textboxname =['11','discountmodeid','taxmasterid','additionalchargecategoryid','adjustmenttype','adjustmentvalue','summarynetamount','writeoffamount','balanceamount','mname'];
		var datanames =['11','discountmodeid','taxmasterid','additionalchargecategoryid','adjustmenttype','adjustmentvalue','summarynetamount','writeoffamount','balanceamount','mname'];
		var dropdowns =['7','discountmodeid','taxmasterid','additionalchargecategoryid','adjustmenttype','mname'];
		var modedata=data.mode;
		textboxsetvalue(textboxname,datanames,data.mode,dropdowns);	
		var mode=data.mode;	
	}
}
{// Validate percentage
	function validatepercentage(field)
	{
		var type =$('#discountcalculationtype').val();
		var value=field.attr('id');
		var num=parseFloat($('#'+value+'').val());
		if(type == 3){
			if(num < 0 || num > 100)
			return "percent value 0%-100%";			
		}
	}
}
{// Summary Validate Percentage
	function summaryvalidatepercentage(field)
	{
		var type =$('#summarydiscountcalculationtype').val();
		var value=field.attr('id');
		var num=parseFloat($('#'+value+'').val());
		if(type == 3){
			if(num < 0 || num > 100)
			return "percent value 0%-100%";		
		}
	}
}
{// Form to Grid validation
	function gridformtogridvalidatefunction(gridnamevalue)
	{//Function call for validate 
		$("#"+gridnamevalue+"validation").validationEngine({
			onSuccess: function() {  
				var i = griddataid;
				var gridname = gridynamicname;
				var coldatas = $('#gridcolnames'+i+'').val();
				var columndata = coldatas.split(',');
				var coluiatas = $('#gridcoluitype'+i+'').val();
				var columnuidata = coluiatas.split(',');
				var datalength = columndata.length;			
				//summary calculate
				if(gridname =='invoicesproaddgrid1'){				
					//disable the type
					$('#ordertypeid,#taxmasterid,#additionalchargecategoryid,#pricebookid').attr("readonly", true);
					if(METHOD == 'ADD' || METHOD == ''){ //ie add operation	
						formtogriddata(gridname,METHOD,'last','');
					}
					if(METHOD == 'UPDATE'){  //ie edit operation
						formtogriddata(gridname,METHOD,'',UPDATEID);
					}
					/* Hide columns */
					var hideprodgridcol = ['taxgriddata','chargegriddata','discountdata','invoicedetailid'];
					gridfieldhide('invoicesproaddgrid1',hideprodgridcol);
					/* Data row select event */
					datarowselectevt();						
					METHOD = 'ADD'; //reset
				}
				else if(gridname =='invoicespayaddgrid2'){	
					if(PAYMETHOD == 'ADD' || PAYMETHOD == ''){ //ie add operation	
						formtogriddata(gridname,PAYMETHOD,'last','');
					}
					if(PAYMETHOD == 'UPDATE'){  //ie edit operation
						formtogriddata(gridname,PAYMETHOD,'',PAYUPDATEID);
					}
					/* Data row select event */
					datarowselectevt();	
					PAYMETHOD = 'ADD'; //reset				
				}
				
			//set the new payment number
			var moduleid=217;
			setTimeout(function(){					
				$('#paymentto').val(COMPANY_NAME);		
			},150); 
			$('#invoicesproupdatebutton,#invoicesproupdatebutton').hide();	//hide the UPDATE button(inner - productgrid)
			$('#invoicesproaddbutton,#invoicesproaddbutton').show();//display the ADD button(inner-productgrid)*/			
			clearform('gridformclear');
			griddataid = 0;
			gridynamicname = '';
			//close product section
			$('#invoicesprocancelbutton').trigger('click');
			$("#invoicespaycancelbutton").trigger('click');
			//summary calculate
			var gridsumcolname = ['grossamount','discountamount','taxamount','chargeamount','netamount','paymentamount'];
			var sumfieldid = ['summarygrossamount','summarydiscountamount','grouptaxamount','summaryadditionalchargeamount','summarynetamount','paidamount'];
			var gridname = ['invoicesproaddgrid1','invoicesproaddgrid1','invoicesproaddgrid1','invoicesproaddgrid1','invoicesproaddgrid1','invoicespayaddgrid2'];
			gridsummeryvaluefetch(gridname,gridsumcolname,sumfieldid);	
			$('#taxgriddata,#chargegriddata,#discountdata').val(''); //reset the data
			$('#taxcategory,#chargecategory').select2('val',''); //reset the category
			cleargriddata('taxgrid');
			cleargriddata('chargesgrid');
			},
			onFailure: function() {
				var dropdownid =['1','productid'];
				dropdownfailureerror(dropdownid);
			}
		});
	}
	
}
{// Edit Function
	function invoiceeditdatafetchfun(datarowid){
		clearformgriddata();
		if($("#invoicestc_editor").length){			
			froalaset(froalaarray);
		}
		$('.resetoverlay').find('input[type=text],select,input[type=hidden]').val('');		
		$(".addbtnclass").addClass('hidedisplay');
		$(".updatebtnclass").removeClass('hidedisplay');
		$("#formclearicon").hide(); 
		retrieveinvoicedata(datarowid);
		var toresetdate = $("#validdate").val();
		firstfieldfocus();
		//disable fields
		setTimeout(function(){
			$('#ordertypeid,#taxmasterid,#additionalchargecategoryid,#pricebookid').attr("readonly", true);
			$("#accountid,#contactid,#opportunityid").select2("readonly", false);
			},50);		
		$('#currentmode').val(2);
		var invdate = $("#invoicedate").val();
		getandsetdate(invdate);
		$("#validdate").val(toresetdate);
		//For Keyboard Shortcut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		Materialize.updateTextFields();
	}
}
{// Clone Function
	function invoiceclonedatafetchfun(datarowid){
		clearformgriddata();		
		if($("#invoicestc_editor").length){
			froalaset(froalaarray);
		}
		$('.resetoverlay').find('input[type=text],select,input[type=hidden]').val('');
		$(".addbtnclass").removeClass('hidedisplay');
		$(".updatebtnclass").addClass('hidedisplay');
		$("#formclearicon").hide(); 
		retrieveinvoicedata(datarowid);
		var toresetdate = $("#validdate").val();
		firstfieldfocus();
		//disable fields
		setTimeout(function(){
			$('#ordertypeid,#taxmasterid,#additionalchargecategoryid,#pricebookid').attr("readonly", true);
			$("#accountid,#contactid,#opportunityid").select2("readonly", false);
			},50);	
		$('#currentmode').val(2);
		var invdate = $("#invoicedate").val();
		getandsetdate(invdate);
		$("#validdate").val(toresetdate);
		//For Keyboard Short cut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		Materialize.updateTextFields();
	}
}
{// Retrieves the salesorder
	function getmoduledata(datarowid)
	{
		var elementsname ="accountid,opportunityid,contactid,crmstatusid,employeetypeid,employeeid,currencyid,pricebookid,taxmasterid,additionalchargecategoryid,carriertypeid,shippingdetail";
		var elementstable = 'salesorder,salesorder,salesorder,salesorder,salesorder,salesorder,salesorder,salesorder,salesorder,salesorder,salesorder,salesorder,salesorder';//tablemae
		var elementscolmn = "accountid,opportunityid,contactid,crmstatusid,employeetypeid,employeeid,currencyid,pricebookid,taxmasterid,additionalchargecategoryid,carriertypeid,shippingdetail";//tablefieldname
		var elementpartable = 'salesorder';
		$.ajax({
			url:base_url+"Invoice/getmoduledata?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable, 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) 
			{
				var txtboxname = elementsname ;
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);		
			}
		});
		$.ajax({
			url:base_url+"Invoice/getsoaddressdetail?dataprimaryid="+datarowid+"&addtype=Billing", 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				var datanames = ['12','billingaddresstype','billingaddress','billingpincode','billingcity','billingstate','billingcountry','shippingaddresstype','shippingaddress','shippingpincode','shippingcity','shippingstate','shippingcountry'];
				var textboxname = ['12','billingaddresstype','billingaddress','billingpincode','billingcity','billingstate','billingcountry','shippingaddresstype','shippingaddress','shippingpincode','shippingcity','shippingstate','shippingcountry'];
				var dropdowns = ['2','billingaddresstype','shippingaddresstype'];
				textboxsetvalue(textboxname,datanames,data,dropdowns); 
			}
		});
		//productdetailarray
		$.ajax({
			url:base_url+'Invoice/getsoproductdetail?primarydataid='+datarowid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					loadinlinegriddata('invoicesproaddgrid1',data.rows,'json');
					/* data row select event */
					datarowselectevt();
					/* column resize */
					columnresize('invoicesproaddgrid1');
					var hideprodgridcol = ['taxgriddata','chargegriddata','discountdata','invoicedetailid'];
					gridfieldhide('invoicesproaddgrid1',hideprodgridcol);
				}
			}
		});
	}
}
{// Generate icon after loads data - Workaround
	function generatemoduleiconafterload(fieldid,clickevnid){
		$("#"+fieldid+"").css('display','inline').css('width','75%');
		$("#"+fieldid+"").after('<span class="innerfrmicon" id="'+clickevnid+'" style="position:relative;left:5px;top:3px;cursor:pointer"><i class="material-icons">search</i></span>'); 		
		$('label[for="'+fieldid+'"]').css('width','65%');
	}
	function generateiconafterload(fieldid,clickevnid,adddiv){
		$("#"+fieldid+"").css('display','inline').css('width','75%');
		$("#"+fieldid+"").after('<span class="innerfrmicon" id="'+clickevnid+'" style="position:relative;left:5px;top:3px;cursor:pointer"><i class="material-icons">library_books</i></span>'); 		
		$("<div class='row'></div>").insertAfter("#"+adddiv+"");
		$('label[for="'+fieldid+'"]').css('width','65%');
	}
	//this is for the product search/attribute icon-dynamic append
	function productgenerateiconafterload(fieldid,clickevnid){
		$("#s2id_"+fieldid+"").css('display','inline-block').css('width','90%');
		$("#"+fieldid+"").after('<span class="innerfrmicon" id="'+clickevnid+'" style="position:absolute;right:15px;top:15px;cursor:pointer"><i class="material-icons">search</i></span>');
	}
}
	/*
	* Invoice stage-data retrival for Lost-Cancel-Draft-Convert
	*/
	function invoicestage(id){
		var invoicestage =0;
		if(checkValue(id) == true ){
			$.ajax({
			url:base_url+"index.php/Invoice/invoicecurrentstage",
			data:{invoiceid:id},
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
					invoicestage = data.invoicestage;
				}		
			});
			return invoicestage;
		} 
	}
	function conversiondatamapping(rowid,sourcemodule,destinatemodule){
		if(sourcemodule == 216 || sourcemodule == 85 || sourcemodule == 94){
			frommodule = 'quote';
			CONV_ARR = ["quote",rowid];
		}else if(sourcemodule == 217 || sourcemodule == 87 || sourcemodule == 96){
			frommodule = 'salesorder';
			CONV_ARR = ["salesorder",rowid];
		}else if(sourcemodule == 82) {
			frommodule = 'contact';
			CONV_ARR = ["contact",rowid];
		}else if(sourcemodule == 80) {
			frommodule = 'contract';
			CONV_ARR = ["contract",rowid];
		}
		$.ajax({
			url:base_url+"Base/getconversiondata", 
			type: "POST",
			data:{moduleid:sourcemodule,primaryid:rowid,tomoduleid:destinatemodule,frommodule:frommodule},
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
			clearformgriddata();
			resetconversiondata(data.main);	
			//address data set
			var datanames = ['12','billingaddresstype','billingaddress','billingpincode','billingcity','billingstate','billingcountry','shippingaddresstype','shippingaddress','shippingpincode','shippingcity','shippingstate','shippingcountry'];
			var textboxname = ['12','billingaddresstype','billingaddress','billingpincode','billingcity','billingstate','billingcountry','shippingaddresstype','shippingaddress','shippingpincode','shippingcity','shippingstate','shippingcountry'];
			var dropdowns = ['2','billingaddresstype','shippingaddresstype'];
			textboxsetvalue(textboxname,datanames,data.address,dropdowns);
			if(sourcemodule == 82 || sourcemodule == 80){
				$("#ordertypeid").select2("val",5).trigger("change");				
			}
			if(sourcemodule == 82){
				$("#contactid").select2("val",rowid).trigger("change");
			}
			if(softwareindustryid == 2){
				$("#contactid").trigger("change");
			}
			var myArr = $.parseJSON(data.linedetail);		
			var gridlength = myArr.length;
			var j=0;
			for(var i=0;i< gridlength;i++){
				addinlinegriddata('invoicesproaddgrid1',j+1,myArr[i],'json');
				j++;
			}
			/*data row select event*/
			datarowselectevt();
			/*column resize*/
			columnresize('invoicesproaddgrid1');
			if(data.paymentdetail != null){
				var myArr1 = $.parseJSON(data.paymentdetail);			
				var gridlength1 = myArr1.length;
				var j1=0;
				for(var i1=0;i1< gridlength1;i1++){
					addinlinegriddata('invoicespayaddgrid2',j1+1,myArr1[i1],'json');
					j1++;
				}
				/*data row select event*/
				datarowselectevt();
				/*column resize*/
				columnresize('invoicespayaddgrid2');
			}			
			var price_data = data.pricedetails;
			$('#pricebookcurrency').val(price_data.pricebookcurrencyid);
			$('#pricebook_currencyconv').val(price_data.pricebook_currencyconvrate);
			$('#currentcurrency').val(price_data.currentcurrencyid);
			$('#currencyconv').val(price_data.currencyconvresionrate);
			$('#convcurrency').val(price_data.currencyid);
			$('#pricebookcurrencyid').val(price_data.pricebookcurrencyid);
			$('#pricebook_currencyconvrate').val(price_data.pricebook_currencyconvrate);
			$('#currentcurrencyid').val(price_data.currentcurrencyid);
			PRICBOOKCONV_VAL = price_data.pricebook_currencyconvrate;
			{	//summary data settings//
				var ext_data = data.summary;					
				$('#grouptaxgriddata').val(ext_data.grouptax);
				$('#groupchargegriddata').val(ext_data.groupaddcharge);
				$('#groupadjustmentdata').val(ext_data.groupadjustment);
				$('#groupdiscountdata').val(ext_data.groupdiscount);
			}
			if(sourcemodule == 82){
				$("#totalnetamount").val(data.netamount);
				$('#grouppretaxtotal').val(data.netamount);
				$("#grandtotal").val(data.netamount);
			}else if(sourcemodule == 80){
				$("#totalnetamount").val(data.netamount);
				$('#grouppretaxtotal').val(data.netamount);
				$("#grandtotal").val(data.netamount);
			}
			setTimeout(function(){
				calculatesummarydetail();
			},80);
			var tandc=data.main;
			var filename=tandc.invoicestermsandconditions_editorfilename;
			$("#termsandconditionid").select2('val','');
				if(checkValue(filename) == true){
					filenamevaluefetch(filename); //sets the source terms and condition
				}		
			$('#taxmasterid,#additionalchargecategoryid,#pricebookid').attr("readonly", true);
			$("#moduleid,#modulenumber").attr("readonly","readonly");	
			}
		});
	}
/*
*get the quotenumber/so number/invoice number for given id
*/
function getmodulenumber(table,id){		
	var n='';
	$.ajax({
		url: base_url+"Quote/getmodulenumber",
		data:{table:table,id:id},
		async:false,
		cache:false,
		success: function(data) {				
			n=data;
		},
	});
	return n;
}
/*
*get the company names
*/
function getcompanyname(){		
	var n='';
	$.ajax({
		url: base_url+"Base/getcompanyname",		
		async:false,
		cache:false,
		success: function(data) {				
			n=data;
		},
	});
	return n;
}
//product storage get - in stock 
function productstoragefetch(productid) {
	$.ajax({
		url:base_url+"Invoice/productstoragefetchfun?productid="+productid,
		dataType : 'json',
		async:false,
		cache:false,
		success :function(data) {
			nmsg = $.trim(data);
			$("#instock").val(nmsg)
		},
	});
}