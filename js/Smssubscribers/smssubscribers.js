$(document).ready(function(){
	$('body').fadeIn(1500);
	$(document).foundation();
	maindivwidth();
	{
		smssubscribersaddgrid();
		//crud action
		crudactionenable();
	}
	{
		var addclosesmssubscriberscreation =['closeaddform','smssubscriberscreationview','smssubscriberscreationformadd'];
		addclose(addclosesmssubscriberscreation);
		var addcloseviewcreation =["viewcloseformiconid","smssubscriberscreationview","viewcreationformdiv",""];
		addclose(addcloseviewcreation);
	}
	{
		$('#dynamicdddataview').change(function(){
			smssubscribersaddgrid();
		});
	}
	{
		fortabtouch = 0;
	}
	{//toolbar action
		//To open non base form frommapplication
		$('#fromapplicationicon').click(function(){
			addslideup('smssubscriberscreationview','smssubscribersaddformadd');
			$(".ftabnew").addClass("active");
			resetFields();
			clearchecks();
			cleargriddata('smsfromapplicationaddgrid');
			$("#moduleid").attr('readonly',false);
			$("#smsgroupsid").select2('readonly',false);
			$('#fromapptemplatetypeid').select2('val','2');
			groupnameddfromapp('2');
		});
		$('#reloadicon').click(function(){
			refreshgrid();
		});
		$('#formclearicon').click(function(){
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
		});
	}
	{
		$('#templatetypeid').change(function() {
			var ttid = $("#templatetypeid").val();
			groupnamedd(ttid);
			hideunwantedonload(ttid);
		});
	}
	{
		$('#fromapptempaltetypeid').change(function() {
			var ttid = $("#fromapptempaltetypeid").val();
			groupnameddfromapp(ttid);
		});
	}
	{//create and update button
		$('#dataaddsbtn').click(function(){
			$('.ftab').trigger('click');
			$('#smssubscribersformaddwizard').validationEngine('validate');
		});
		$('#smssubscribersformaddwizard').validationEngine({
			onSuccess: function() {
				$('#dataaddsbtn').attr('disabled','disabled');
				smssubscribersnewdataaddfun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
		$('#dataupdatesubbtn').click(function(){
			$('.ftab').trigger('click');
			$('#smssubscribersformeditwizard').validationEngine('validate');
		});
		$('#smssubscribersformeditwizard').validationEngine({
			onSuccess: function() {
				$('#dataupdatesubbtn').attr('disabled','disabled');
				smssubscribersdataupdatefun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
	}
	{
		griddataid = 0;
		gridynamicname = '';
		var gridname = $('#gridnameinfo').val();
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		for(var j=0;j<datalength;j++) {
			gridformtogridvalidatefunction(gridnames[j]);
		}
		$('.frmtogridbutton').click(function(){
			var i = $(this).data('frmtogriddataid');
			var gridname = $(this).data('frmtogridname');
			gridformtogridvalidatefunction(gridname);
			griddataid = i;
			gridynamicname = gridname;
			$('#'+gridname+'validation').validationEngine('validate');
		});
	}
	{//checkbox class
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
	//module based view drop down load 
	$("#moduleid").change(function() {
		var invitemoduleid = $("#moduleid").val();
		if(invitemoduleid == '247' || invitemoduleid== '246' || invitemoduleid == '1'){
			$('#viewid').empty();
			$('#viewid').select2('val','');
			$("#viewid").attr('readonly',true);
			smsfromapplicationaddgrid();
		} else {
			$("#viewid").attr('readonly',false);
			viewdropdownload(invitemoduleid);
		}
	});
	//view drop down change
	$("#viewid").change(function() {
		smsfromapplicationaddgrid();
	});
	{//From Application Close
		var sppaddcloseemailsubscriberscreation =['smsfrmappcloseform','smssubscriberscreationview','smssubscribersaddformadd'];
		addclose(sppaddcloseemailsubscriberscreation);
	}
	//from application  submit function
	$("#smsappsubmit").click(function() {
		$("#smssubscriberallicationvalidationadd").validationEngine('validate');
	});
	jQuery("#smssubscriberallicationvalidationadd").validationEngine({
		onSuccess: function() {
			var rowids = getselectedrowids('smsfromapplicationaddgrid');
			if(rowids != '') {
				fromapplicationsubscriberadd();
			} else {
				alertpopup('Please select the List(s)');
			}
		},
		onFailure: function() {
			var dropdownid =['2','smsgroupsid','moduleid'];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}	
	});
	//import icon
	$('#importicon').click(function(){
		var viewfieldids = $("#viewfieldids").val();
		sessionStorage.setItem("importmoduleid",viewfieldids);
		window.location = base_url+'Dataimport';
	});
	//sms group change type get
	$("#campaigngroupsid").change(function() {
		var groupid = $("#campaigngroupsid").val();
		grouptypeidget(groupid);
	});
	$("#smsgroupsid").change(function() {
		var groupid = $("#smsgroupsid").val();
		grouptypeidget(groupid);
	});
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,smssubscribersaddgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			smssubscribersaddgrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
});
function gridformtogridvalidatefunction(gridnamevalue){
	jQuery('#'+gridnamevalue+'validation').validationEngine({
		validateNonVisibleFields:false,
		onSuccess: function() {
			var i = griddataid;
			var gridname = gridynamicname;
			var n = jQuery('#'+gridname+'').jqGrid('getGridParam', 'records');
			var coldatas = $('#gridcolnames'+i+'').val();
			var columndata = coldatas.split(',');
			var coluiatas = $('#gridcoluitype'+i+'').val();
			var columnuidata = coluiatas.split(',');
			var datalength = columndata.length;
			var dataarr={};
			var n = $('#'+gridname+'').jqGrid('getGridParam', 'records');
			var dataname = '';
			for(var j=0;j<datalength;j++) {
				if( columnuidata[j] == '17' || columnuidata[j] == '18' || columnuidata[j] == '19' || columnuidata[j] == '20' || columnuidata[j] == '23' || columnuidata[j] == '25' || columnuidata[j] == '28' ) {
					dataname = $('#'+columndata[j]+'').find('option:selected').data(''+columndata[j]+'hidden');
					dataarr[''+columndata[j]+'name'] = dataname;
					dataarr[''+columndata[j]+''] = $('#'+columndata[j]+'').val();
				} else {
					dataarr[''+columndata[j]+''] = $('#'+columndata[j]+'').val();
				}
			}
			$('#'+gridname+'').jqGrid('addRowData',n + 1, dataarr, 'last');
			clearform('gridformclear');
			griddataid = 0;
			gridynamicname = '';
		},
		onFailure: function() {
		}
	});
}
function smssubscribersaddgrid(page,rowcount) {
	if(Operation == 1){
		var rowcount = $('#subscriberspgrowcount').val();
		var page = $('.paging').data('pagenum');
	} else{
		var rowcount = $('#subscriberspgrowcount').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	}
	var wwidth = $('#smssubscribersaddgrid').width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.subscribersheadercolsort').hasClass('datasort') ? $('.subscribersheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.subscribersheadercolsort').hasClass('datasort') ? $('.subscribersheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.subscribersheadercolsort').hasClass('datasort') ? $('.subscribersheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#viewfieldids').val();
	var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
	var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
	var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=subscribers&primaryid=subscribersid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#smssubscribersaddgrid').empty();
			$('#smssubscribersaddgrid').append(data.content);
			$('#smssubscribersaddgridfooter').empty();
			$('#smssubscribersaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('smssubscribersaddgrid');
			{//sorting
				$('.subscribersheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.subscribersheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#smssubscriberspgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.subscribersheadercolsort').hasClass('datasort') ? $('.subscribersheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.subscribersheadercolsort').hasClass('datasort') ? $('.subscribersheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					var sortpos = $('#smssubscribersaddgrid .gridcontent').scrollLeft();
					smssubscribersaddgrid(page,rowcount);
					$('#smssubscribersaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('subscribersheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					Operation = 0;
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					smssubscribersaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#subscriberspgrowcount').change(function(){
					Operation = 0;
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = 1;
					smssubscribersaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			//Material select
			$('#subscriberspgrowcount').material_select();
		},
	});
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function(e) {
			smsfromapplicationaddgrid();
			e.preventDefault();
			addslideup('smssubscriberscreationview','smssubscriberscreationformadd');
			resetFields();
			randomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');
			$('.addbtnclass').show();
			$('.updatebtnclass').hide();
			showhideiconsfun('addclick','smssubscriberscreationformadd');
			firstfieldfocus();
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			$('#templatetypeid').select2('val','2');
			groupnamedd('2');
			hideunwantedonload('2');
			$('#templatetypeid, #campaigngroupsid').select2('readonly',false);
			$('#tabgropdropdown').select2('val','1');
			fortabtouch = 0;
			Materialize.updateTextFields();
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
		});
		$("#editicon").click(function(e) {
			smsfromapplicationaddgrid();
			e.preventDefault();
			var datarowid = $('#smssubscribersaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('.updatebtnclass').show();
				$('.addbtnclass').hide();
				smssubscribersdataupdateinfofetchfun(datarowid);
				firstfieldfocus();
				showhideiconsfun('editclick','smssubscriberscreationformadd');
				fortabtouch = 1;
				var ttid = $("#templatetypeid").val();
				hideunwantedonload(ttid);
				groupnamedd(ttid);
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				Materialize.updateTextFields();
				Operation = 1; //for pagination
			} else {
				alertpopup('Please select a row');
			}
		});
		$("#deleteicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#smssubscribersaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#primarydataid').val(datarowid);
				$("#basedeleteoverlay").fadeIn();
				$("#basedeleteyes").focus();
			} else {
				alertpopup('Please select a row');
			}
		});
		$("#basedeleteyes").click(function() {
			var datarowid = $('#primarydataid').val();
			smssubscribersrecorddelete(datarowid);
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#smssubscriberspgnum li.active').data('pagenum');
		var rowcount = $('ul#smssubscriberspgnumcnt li .page-text .active').data('rowcount');
		smssubscribersaddgrid(page,rowcount);
	}
	function frmapprefreshgrid() {
		var page = $('ul#subscriberlistfooterpgnum li.active').data('pagenum');
		var rowcount = $('ul#subscriberlistfooterpgnumcnt li .page-text .active').data('rowcount');
		smsfromapplicationaddgrid(page,rowcount);
	}
}
function smsfromapplicationaddgrid(page,rowcount) {
	var invitemoduleid = $("#moduleid").val();
	var viewid = $("#viewid").val();
	var aptypeid = $("#fromapptempaltetypeid").val();
	if(viewid === null || viewid === '' || viewid === undefined) { 
		viewid = '3'; 
	}
	if(invitemoduleid === null || invitemoduleid === '' || invitemoduleid === undefined) { 
		invitemoduleid = '201'; 
	}
	$.ajax({
		url:base_url+"Smssubscribers/parenttableget?moduleid="+invitemoduleid,
		dataType:'json',
		async:false,
		cache:false,
		success:function(pdata) {
			if(pdata.fail != "FAILED") {
				maintabinfo = pdata;
				parentid = maintabinfo+"id";
			} else {
				maintabinfo = '';
				parentid = '';
			}
		},
	});
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#smsfromapplicationaddgrid').width();
	var wheight = $('#smsfromapplicationaddgrid').height();
	//col sort
	var sortcol = $('.subscriberlistfooterheadercolsort').hasClass('datasort') ? $('.subscriberlistfooterheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.subscriberlistfooterheadercolsort').hasClass('datasort') ? $('.subscriberlistfooterheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.subscriberlistfooterheadercolsort').hasClass('datasort') ? $('.subscriberlistfooterheadercolsort.datasort').attr('id') : '0';
	var footername = 'fromapplication';
	$.ajax({
		url:base_url+"Smssubscribers/fromapplicationgrigdatafetch?maintabinfo="+maintabinfo+"&primaryid="+parentid+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid='+invitemoduleid+"&viewid="+viewid+"&aptypeid="+aptypeid+"&checkbox=true"+'&footername='+footername,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#smsfromapplicationaddgrid').empty();
			$('#smsfromapplicationaddgrid').append(data.content);
			$('#smsfromapplicationaddgridfooter').empty();
			$('#smsfromapplicationaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			/*column resize*/
			columnresize('smsfromapplicationaddgrid');
			{//sorting
				$('.subscriberlistfooterheadercolsort').click(function(){
					$('.subscriberlistfooterheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('.pvpagnumclass').data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortpos = $('#smsfromapplicationaddgrid .gridcontent').scrollLeft();
					smsfromapplicationaddgrid(page,rowcount);
					$('#smsfromapplicationaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
				});
				sortordertypereset('subscriberlistfooterheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					smsfromapplicationaddgrid(page,rowcount);
				});
				$('#fromapplicationpgrowcount').change(function(){
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('.pvpagnumclass').data('pagenum');
					smsfromapplicationaddgrid(page,rowcount);
				});
			}
			//Material select
			$('#fromapplicationpgrowcount').material_select();
			//header check box
			$(".fromappli_headchkboxclass").click(function() {
				checkboxclass('fromappli_headchkboxclass','fromappli_rowchkboxclass');
				getcheckboxrowid(invitemoduleid);
			});
			//row check box
			$(".fromappli_rowchkboxclass").click(function(){
				$('.fromappli_headchkboxclass').removeAttr('checked');
				getcheckboxrowid(invitemoduleid);
			});
			
			checktheselectedinvitevalues(invitemoduleid);
		}
	});
}
function checkboxclass(headerclass,rowclass) {
	if($("."+headerclass+"").prop('checked') == true) {
		$("."+rowclass+"").attr('checked', true);
	} else {
		$("."+rowclass+"").removeAttr('checked');
	}
}
function getcheckboxrowid(invitemoduleid) {
	var selected = [];
	$('.gridcontent input:checked').each(function() {
		selected.push($(this).attr('data-rowid'));
	});
	inviteselectuserfetch(invitemoduleid,selected);
}
//check the selected invite user 
function checktheselectedinvitevalues(invitemoduleid) {
	var leadid = $("#leadid").val();
	var contactid = $("#contactid").val();
	var userid = $("#userid").val();
	var accountid = $("#accountid").val();
	if(invitemoduleid == '201') {
		if(leadid != '') {
			var lead = leadid.split('|');
			var lid = lead[1].split(',');
			checkboxcheck(invitemoduleid,lid);
		}
	} else if(invitemoduleid == '203') {
		if(contactid != '') {
			var contact = contactid.split('|');
			var cid = contact[1].split(',');
			checkboxcheck(invitemoduleid,cid);
		}
	} else if(invitemoduleid == '4') {
		if(userid != '') {
			var user = userid.split('|');
			var uid = user[1].split(',');
			checkboxcheck(invitemoduleid,uid);
		}
	} else if(invitemoduleid == '202') {
		if(accountid != '') {
			var account = accountid.split('|');
			var aid = account[1].split(',');
			checkboxcheck(invitemoduleid,aid);
		}
	}
}
function checkboxcheck(invitemoduleid,checkid) {
	for(var i=0;i <checkid.length;i++) {
		if(invitemoduleid == 201){
			$('#leads_rowchkbox'+checkid[i]).attr('checked',true);
		} else if(invitemoduleid == 203) {
			$('#contacts_rowchkbox'+checkid[i]).attr('checked',true);
		} else if(invitemoduleid == 4) {
			$('#employee_rowchkbox'+checkid[i]).attr('checked',true);
		} else if(invitemoduleid == 202) {
			$('#accounts_rowchkbox'+checkid[i]).attr('checked',true);
		}
	}
}
function smssubscribersnewdataaddfun() {
	var amp = '&';
	var addgriddata='';
	var gridname = $('#gridnameinfo').val();
	if(gridname != '') {
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		var noofrows=0;
		var addgriddata='';
		for(var j=0;j<datalength;j++) {
			if(j!=0) {
				addgriddata = addgriddata+'&'+getgridrowsdata(gridnames[j]);
				noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;;
			} else {
				addgriddata = getgridrowsdata(gridnames[j]);
				noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;;
			}
		}
	}
	var sendformadddata = JSON.stringify(addgriddata);
	var editordata = '';
	var neweditordata = '';
	var editorname = $('#editornameinfo').val();
	if(editorname != '') {
		var editornames  = [];
		editornames = editorname.split(',');
		$.each( editornames, function( key, name ) {
			editordata = $('#'+name+'').editable('getHTML');
			var fname = [];
			fname = name.split('_');
			neweditordata += '&'+fname[0]+'_editorfilename='+editordata;
			$('#'+fname[0]+'_editorfilename').val(editordata);
		});
	}
	var formdata = $('#smssubscribersdataaddform').serialize();
	var datainformation = amp + formdata;
	var mobnumber = $("#mobilenumber").val();
	$.ajax({
		url:base_url+'Smssubscribers/newdatacreate',
		data:'datas='+datainformation+amp+'griddatas='+sendformadddata+amp+'numofrows='+noofrows,
		type:'POST',
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$('.ftab').trigger('click');
				resetFields();
				$('#smssubscriberscreationformadd').hide();
				$('#smssubscriberscreationview').fadeIn(1000);
				refreshgrid();
				$('#dataaddsbtn').attr('disabled',false);
				alertpopup(savealert);
			} else if (nmsg == 'false') {
			}
		},
	});
}
function smssubscribersdataupdateinfofetchfun(datarowid) {
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementpartable = $('#elementspartabname').val();
	var resctable = $('#resctable').val();
	$.ajax({
		url:base_url+'Smssubscribers/fetchformdataeditdetails?dataprimaryid='+datarowid+'&elementsname='+elementsname+'&elementstable='+elementstable+'&elementscolmn='+elementscolmn+'&elementspartabname='+elementpartable+'&resctable='+resctable,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				$('.ftab').trigger('click');
				resetFields();
				$('#smssubscriberscreationformadd').hide();
				$('#smssubscriberscreationview').fadeIn(1000);
				refreshgrid();
				$('#dataupdatesubbtn').attr('disabled',false);
			} else {
				addslideup('smssubscriberscreationview','smssubscriberscreationformadd');				
				var txtboxname = elementsname + ',primarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
				setTimeout(function() {
					$("#campaigngroupsid").select2('val', data.campaigngroupsid);
					$("#templatetypeid, #campaigngroupsid").select2('readonly',true);
				}, 500);
				//For Keyboard Shortcut Variables
				addformupdate = 1;
				viewgridview = 0;
				addformview = 1;
			}
		}
	});
}
function smssubscribersdataupdatefun() {
	var formdata = $('#smssubscribersdataaddform').serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	var editordata = '';
	var neweditordata = '';
	var editorname = $('#editornameinfo').val();
	if(editorname != '') {
		var editornames  = [];
		editornames = editorname.split(',');
		$.each( editornames, function( key, name ) {
			editordata = $('#'+name+'').editable('getHTML');
			var fname = [];
			fname = name.split('_');
			neweditordata += '&'+fname[0]+'_editorfilename='+editordata;
			$('#'+fname[0]+'_editorfilename').val(editordata);
		});
	}
	$.ajax({
		url: base_url+'Smssubscribers/datainformationupdate',
		data: 'datas='+datainformation+'&datass='+neweditordata,
		type: 'POST',
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$('.ftab').trigger('click');
				resetFields();
				$('#smssubscriberscreationformadd').hide();
				$('#smssubscriberscreationview').fadeIn(1000);
				refreshgrid();
				$('#dataupdatesubbtn').attr('disabled',false);
				alertpopup(savealert);
			} else if (nmsg == 'false') {
			}
		},
	});
}
function smssubscribersrecorddelete(datarowid) {
	var elementstable = $('#elementstable').val();
	var elementspartable = $('#elementspartabname').val();
	$.ajax({
		url: base_url + 'Smssubscribers/deleteinformationdata?primarydataid='+datarowid+'&elementstable='+elementstable+'&parenttable='+elementspartable,
		async:false,
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				refreshgrid();
				$('#basedeleteoverlay').fadeOut();
				alertpopup('Deleted successfully');
			} else if (nmsg == 'Denied') {
				refreshgrid();
				$('#basedeleteoverlay').fadeOut();
				alertpopup('Permission denied');
			}
		}
	});
}
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $('#viewfieldids').val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
	if(viewname != 'dontset') {
		setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		cleargriddata('viewcreateconditiongrid');
	} else {
		$('#dynamicdddataview').trigger('change');
	}
}
//invite user module drop down load
function moduledropdownload() {
	$('#moduleid').empty();
	$('#moduleid').append($("<option></option>"));
	$.ajax({
		url: base_url+"Smssubscribers/moduledropdownloadfun",
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++) {
					newddappend += "<option value = '" +data[m]['datasid']+ "'>"+data[m]['dataname']+ " </option>";
				}
				//after run
				$('#moduleid').append(newddappend);
			}	
		},
	});
}
//view drop down load
function viewdropdownload(invitemoduleid) {
	$('#viewid').empty();
	$.ajax({
		url: base_url+"Smssubscribers/viewdropdownloadfun?moduleid="+invitemoduleid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++) {
					newddappend += "<option value = '" +data[m]['datasid']+ "'>"+data[m]['dataname']+ " </option>";
				}
				//after run
				$('#viewid').append(newddappend);
			}	
			$('#viewid').trigger('change');
		},
	});
}
//selected row data get - from application grid
function inviteselectuserfetch(invitemoduleid,datarowid) {
	var	inviteuser =  invitemoduleid+'|'+datarowid;
	if(invitemoduleid == '201') {
		$("#leadid").val(inviteuser);
		var lead = $("#leadid").val();
	} else if(invitemoduleid == '203') {
		$("#contactid").val(inviteuser);
		var contact = $("#contactid").val();
	} else if(invitemoduleid == '4') {
		$("#userid").val(inviteuser);
		var user = $("#userid").val();
	} else if(invitemoduleid == '202' || invitemoduleid == '91') {
		$("#accountid").val(inviteuser);
		var account = $("#accountid").val();
	}
}
//from application form add
function fromapplicationsubscriberadd() {
	var formdata = $("#smssubscribersappdataaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	$.ajax({
        url: base_url + "Smssubscribers/fromappsubscriberadd",
        data: "data=" + datainformation,
		type: "POST",
		async:false,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE'){
				resetFields();
				cleargriddata('smsfromapplicationaddgrid');
				$("#leadid").val('');
				$("#contactid").val('');
				$("#userid").val('');
				$("#accountid").val('');
				$("#smsappsubmit").show();
				$("#smsappupdate").hide();
				$("#smssubscribersaddformadd").fadeOut();
				$("#smssubscriberscreationview").fadeIn();
				refreshgrid();
				frmapprefreshgrid();
				alertpopup(savealert);
			}
        },
    });
}
//sms group dd load
function smsgroupddload(process) {
	$('#smsgroupsid').empty();
	$('#smsgroupsid').append($("<option></option>"));
	$.ajax({
        url: base_url + "Smssubscribers/smsgroupdddatafetch?process="+process,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#smsgroupsid')
					.append($("<option></option>")
					.attr("data-smsgroupsidhidden",data[index]['dataname'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}
        },
    });
}
function grouptypeidget(groupid) {
	$.ajax({
        url: base_url + "Smssubscribers/smsgroupidget?groupid="+groupid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) != 'FAILED') {
				$("#smsgrouptypeid").val(data);
			} 
        },
    });
}
function hideunwantedonload(ttid){
	$("#emailid").removeClass('validate[required,custom[multimail],minSize[0],maxSize[100],funcCall[smssubscribersemailidcheck]]');
	$("#emailid").addClass('validate[required,custom[multimail],min[0],max[100],funcCall[smssubscribersemailidcheck]]');
	if(ttid == 2) {
		$("#emailid").removeClass('validate[required,custom[multimail],min[0],max[100],funcCall[smssubscribersemailidcheck]]');
		$("#emailiddivhid").hide();
		$("#mobilenumberdivhid").show();
		$("#mobilenumber").addClass('validate[required,custom[phone],funcCall[smssubscribersmobnocheck]]');
	}
	if(ttid == 3){
		$("#emailid").removeClass('validate[required,custom[multimail],min[0],max[100],funcCall[smssubscribersemailidcheck]]');
		$("#emailiddivhid").hide();
		$("#mobilenumberdivhid").show();
		$("#mobilenumber").addClass('validate[required,custom[phone],funcCall[smssubscribersmobnocheck]]');
	}
	if(ttid == 5){
		$("#emailiddivhid").show();
		$("#emailid").addClass('validate[required,custom[multimail],min[0],max[100],funcCall[smssubscribersemailidcheck]]');
		$("#mobilenumber").removeClass('validate[required,custom[phone],funcCall[smssubscribersmobnocheck]]');
		$("#mobilenumberdivhid").hide();
		$("smsgrouptypeid").removeClass('validate[maxSize[100]]');
	}
}
function groupnamedd(typeid) {
	$('#campaigngroupsid').select2("val", "");
	$('#campaigngroupsid').empty();
	$('#campaigngroupsid').append($("<option></option>"));
	$.ajax({
        url: base_url + "Smssubscribers/groupnameddfetch?typeid="+typeid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#campaigngroupsid')
					.append($("<option></option>")
					.attr("data-campaigngroupsidhidden",data[index]['dataname'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}
        },
    });
}
function groupnameddfromapp(typeid) {
	$('#smsgroupsid').select2("val", "");
	$('#smsgroupsid').empty();
	$('#smsgroupsid').append($("<option></option>"));
	$.ajax({
        url: base_url + "Smssubscribers/groupnameddfetch?typeid="+typeid+"&app=app",
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#smsgroupsid')
					.append($("<option></option>")
					.attr("data-modname",data[index]['dataname'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}
        },
    });
}
function clearchecks(){
	$('#leadid, #contactid, #userid, #accountid').val('');
}
//Edit unique Email Id check - Kumaresan
function smssubscribersemailidcheck() {
	var primaryid = $('#primarydataid').val();
	var emailid = $('#emailid').val();
	var nmsg = "";
	if( emailid != "" ) {
		$.ajax({
			url:base_url+"Smssubscribers/uniquedynamicviewemailidcheck",
			data:"data=&emailid="+emailid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Email Id already exists!";
				}
			} else {
				return "Email Id already exists!";
			}
		} 
	}
}
//Edit unique Mobile Number check - Kumaresan
function smssubscribersmobnocheck() {
	var templatetypeid = $('#templatetypeid').val();
	var primaryid = $('#primarydataid').val();
	var mobilenumber = $('#mobilenumber').val();
	var nmsg = "";
	if( mobilenumber != "" ) {
		$.ajax({
			url:base_url+"Smssubscribers/uniquedynamicviewmobnocheck",
			data:"data=&mobilenumber="+mobilenumber+"&templatetypeid="+templatetypeid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Mobile Number already exists!";
				}
			} else {
				return "Mobile Number already exists!";
			}
		} 
	}
}
