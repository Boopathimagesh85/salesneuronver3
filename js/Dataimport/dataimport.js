$(document).ready(function(){
	{//Foundation Initialization
		$(document).foundation();
	}
	{//Grid Settings and calling
		datimportviewgrid();	
		confirmgrid();	
		firstfieldfocus();
	}
	$("#closeaddform").removeClass('hidedisplay');
	{// For Import Form Details
		$("#addicon").click(function(){
			addslideup('dataimportview','importdetailsform');
			resetFields();
			$("#dataimpfnamesapn").text("");
			$("#dataimportfilename").val("");
			importdefaultvalset();
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
		});
	}
	{// Close Add Screen
		var addclosedataimport =["closeaddform","dataimportview","importdetailsform",""];
		addclose(addclosedataimport);
	}
	reloadvar = 0;
	{//close reset stepper
		$("#alertsfcloseyes").click(function() {
			$('.mdl-stepper-step').each(function () {
				$(this).removeClass("active-step");
				$(this).removeClass("editable-step");
			});
			$("#tab1").trigger("click");
		});
	}
	
	{
		$('#datamanipulationicon').click(function(){
			window.location = base_url+'Massupdatedelete';
		});
		$('#findduplicatesicon').click(function(){
			window.location = base_url+'Findduplicates';
		});
	}
	{// From Computer
		fileuploadmoname = 'dataimportfileupload';
		var dataimportsettings = {
			url: base_url+"upload.php",
			method: "POST",
			fileName: "myfile",
			dataType:'json',
			multiple:false,
			showStatusAfterSuccess: false,
			showFileCounter:false,
			showFileSize:false,
			showFileName:false,
			maxFileSize:'5097152',
			maxDisplaySize : '5 Mb',
			allowedTypes:'CSV,VCF',
			async:false,
			dragDrop: false,
			onSuccess:function(files,data,xhr) {
				if(data != "MaxSize" && data != "Size" && data != "MaxFile" && data != "LowSize"){
					var arr =$.parseJSON(data);
					$('#dataimportfilename').val(arr.path);
					$('#dataimpfnamesapn').text(arr.fname);
					internalcolumnnamegen();
					columndatamappinggridload();
				} else if(data == "Size") {
					alertpopupdouble("File Size exceeds the limit. You can upload upto 5mb at a time.");
				} else if(data == "MaxSize") {
					alertpopupdouble("You have reached your maximun storage limit. if you want to upload documents you want to buy the storage limit from add ons");
				} else if(data == "MaxFile") {
					alertpopupdouble("Maximum number of files exceeded.");
				} else if(data == "LowSize") {
					alertpopupdouble("File size is too low. Please Upload the correct file");
				} else {
					alertpopupdouble("Upload Failed");
				}
			},
			onError: function(files,status,errMsg) {
				alert(errMsg);
				$('#dataimportfilename').val('');
			}
		}
		$("#dataimportfileuploader").uploadFile(dataimportsettings);
		//file upload drop down change function
		$("#dataimportfileuploadtrigger").click(function() {
			$(".ajax-file-upload>form>input:first").trigger('click');
		});
	}
	$('#importmoduleid').trigger('change');
	
	{//Tab Selection
		//tab group click event restriction
		$('#tab1 span').removeAttr('style');
		$('#tab1').click(function() {
			$('#tab2,#tab3').removeClass('activetab');
			$(this).addClass('active-step');
			$(this).addClass('active activetab');
			$("#subformspan1").show();
			$("#subformspan2,#subformspan3").hide();
		});
		$('#tab2').click(function() {
			var checktabmod = $("#importmoduleid").val();
			var checktabfile = $("#dataimportfilename").val();
			if(checktabmod == '' || checktabfile == '') {
				alertpopup('Please enter the mandatory field...');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
			} else {
				$("#tab1").addClass('editable-step');
				$(this).addClass('active-step activetab');
				$('#tab1,#tab3').removeClass('activetab');
				$("#subformspan2").show();
				$("#subformspan1,#subformspan3").hide();
			}
			//for keyboard shortcut
			tabid =2;
		});
		$('#tab3').click(function() {
			//for keyboard shortcut
			tabid =3;
			var checktabmod = $("#importmoduleid").val();
			var checktabfile = $("#dataimportfilename").val();
			if(checktabmod == '' || checktabfile == '') {
				alertpopup('Please enter the mandatory field...')
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
			} else {
				$("#tab2").addClass('editable-step');
				$(this).addClass('active-step activetab');
				$('#tab2,#tab1').removeClass('activetab');
				$("#subformspan3").show();
				$("#subformspan2,#subformspan1").hide();
			}
		});
	}
	//internal column name fetching
	$('#importmoduleid').change(function() {
		columndatamappinggridload();
		var modid = $('#importmoduleid').val();
		if( modid != "" ) {
			$.ajax({
				url:base_url+"Dataimport/datacolumninformationfetch?moduleid="+modid,
				dataType:'json',
				async:false,
				cache:false,
				success:function(data) {
					$('#dataimpinternalcolname,#dataimpmergerec').empty();
					$('#dataimpinternalcolname').append($("<option value=''>select</option>"));
					var j=1;
					prev = ' ';
					$.each( data, function( key ) {
						var cur = data[key]['sectionid'];
						if(prev != cur ) {
							if(prev != " ") {
								$('#dataimpinternalcolname').append($("</optgroup>"));
								$('#dataimpmergerec').append($("</optgroup>"));
							}
							$('#dataimpinternalcolname').append($("<optgroup label='"+data[key]['sectionname']+"' class='"+data[key]['sectionname']+"'>"));
							$('#dataimpmergerec').append($("<optgroup label='"+data[key]['sectionname']+"' class='"+data[key]['sectionname']+"'>"));
							prev = data[key]['sectionid'];
						}
						$("#dataimpinternalcolname optgroup[label='"+data[key]['sectionname']+"']")
						.append($("<option></option>")
						.attr("data-uitype",data[key]['uitypeid'])
						.attr("data-tablename",data[key]['tablename'])
						.attr("data-columnname",data[key]['columnname'])
						.attr("data-parenttable",data[key]['parenttable'])
						.attr("data-moduleid",data[key]['moduletabid'])
						.attr("value",data[key]['internalcolumnname'])
						.text(data[key]['internalcolumnname']));
						
						$("#dataimpmergerec optgroup[label='"+data[key]['sectionname']+"']")
						.append($("<option></option>")
						.attr("data-uitype",data[key]['uitypeid'])
						.attr("data-tablename",data[key]['tablename'])
						.attr("data-columnname",data[key]['columnname'])
						.attr("data-parenttable",data[key]['parenttable'])
						.attr("data-moduleid",data[key]['moduletabid'])
						.attr("value",data[key]['internalcolumnname'])
						.text(data[key]['internalcolumnname']));
						j++;
					});
				},
			});
			$('#dataimpinternalcolname,#dataimpmergerec').trigger('change');
		}
		if(modid == '201') {
			$('#impleadsourcespan').removeClass('hidedisplay');
			$('#impassigntospan').removeClass('hidedisplay');
		} else {
			$('#impleadsourcespan').addClass('hidedisplay');
			$('#impassigntospan').addClass('hidedisplay');
		}
	});
	{//action events
		//import log grid reload
		$('#importlogreload').click(function() {
			datimportviewgrid();
		});
		//data import submit
		$('#dataimportconfirmbtn').click(function() {
			$('.ftab').trigger('click');
			$('#adddataimportfiletabvalidate').validationEngine('validate');
		});
		$('#adddataimportfiletabvalidate').validationEngine({
			onSuccess: function() {
				var mergefield = $('#mergefildvalueset').val();
				var totalmissedrecords = $('#confirmgrid .gridcontent div.data-content div').length;
				if(totalmissedrecords > 0) {
					alertpopup('Please map Unmapped column lists!');
				} else {
					if(mergefield == '') {
						dataimportexcel();
					} else {
						var mfieldset = [];
						var mfieldset = mergefield.split(',');
						if( mfieldset.length >=2 ) {
							dataimportexcel();
						} else {
							$('.ftab').trigger('click');
							alertpopup('Select Minimum Two Merge Fields!');
						}
					}
				}
			},
			onFailure: function() {
				var dropdownid =['3','importmoduleid','importfiletypeid','dataimpmergerec'];
				dropdownfailureerror(dropdownid);
				$("#s2id_moduleid").find('ul').addClass('error');
				alertpopup(validationalert);
			}
		});
		//new data column mapping
		$('#importdatamapaddbtn').click(function(){
			$('#importdatamapaddformvalidate').validationEngine('validate');
		});
		$('#importdatamapaddformvalidate').validationEngine({
			onSuccess: function() {
				importcolumnmap();
			},
			onFailure: function() {
				var dropdownid =['2','dataimportcolnames','dataimpinternalcolname'];
				dropdownfailureerror(dropdownid);
				$("#s2id_moduleid").find('ul').addClass('error');
				alertpopup(validationalert);
			}
		});
		//remove column mapped data
		$('#colmapdelicon').click(function(){
			var datarowid = $('#fieldmappingaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				/*delete grid data*/
				deletegriddatarow('fieldmappingaddgrid',datarowid);
				unmappedcolumnamelist();
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	{//merge field
		$('#dataimpmergerec').attr('disabled',true);
		$('#importduplicate').change(function(){
			var dupopt = $('#importduplicate').val();
			if(dupopt != "") {
				$('#dataimpmergerec').addClass('validate[required]');
				$('#mergcritmandoptspan').text('*');
				$('#dataimpmergerec').trigger('change');
				$('#dataimpmergerec').attr('disabled',false);
			} else {
				$('#dataimpmergerec').removeClass('validate[required]');
				$('#mergcritmandoptspan').text('');
				$('#dataimpmergerec').select2('val','');
				$('#dataimpmergerec').trigger('change');
				$('#dataimpmergerec').attr('disabled',true);
			}
		});
		//merge criteria fields
		$("#dataimpmergerec").change(function() {
			$("#s2id_dataimpmergerec,.select2-results").removeClass('error');
			$("#s2id_dataimpmergerec").find('ul').removeClass('error');
			var data = $('#dataimpmergerec').select2('data');
			var finalResult = [];
			for( item in data ) {
				finalResult.push(data[item].id);
			};
			var mergefields = finalResult.join(',');
			$('#mergefildvalueset').val(mergefields);
		});
	}
	{//Import Conversion From All modules
		var impmoduleid = sessionStorage.getItem("importmoduleid"); 
		if(impmoduleid != null) {
			setTimeout(function(){
				$("#addicon").trigger('click');
				$('#importmoduleid').select2('val',impmoduleid).trigger('change');
				sessionStorage.removeItem("importmoduleid");
			},100);				
		}
	}
	{
		$("#tab2").click(function() {
			if(reloadvar == 0) {
				setTimeout(function() {
					fieldmappingaddgrid();
					columndatamappinggridload();
				}, 100);
			}
		});
		$("#tab3").click(function() {
			reloadvar = 1;
			setTimeout(function() {
				confirmgrid();
				unmappedcolumnamelist();
			}, 100);
		});
	}
	//filter
	$("#dataimportfilterddcondvaluedivhid").hide();
	$("#dataimportfilterdatecondvaluedivhid").hide();
	{	//field value set
		$("#dataimportfiltercondvalue").focusout(function(){
			var value = $("#dataimportfiltercondvalue").val();
			$("#dataimportfinalfiltercondvalue").val(value);
			$("#dataimportfilterfinalviewconid").val(value);
		});
		$("#dataimportfilterddcondvalue").change(function(){
			var value = $("#dataimportfilterddcondvalue").val();
			dataimportmainfiltervalue=[];
			$('#dataimportfilterddcondvalue option:selected').each(function(){
			    var $mvalue =$(this).attr('data-ddid');
			    dataimportmainfiltervalue.push($mvalue);
				$("#dataimportfinalfiltercondvalue").val(dataimportmainfiltervalue);
			});
			$("#dataimportfilterfinalviewconid").val(value);
		});
	}
	dataimportfiltername = [];
	$("#dataimportfilteraddcondsubbtn").click(function() {
		$("#dataimportfilterformconditionvalidation").validationEngine("validate");	
	});
	$("#dataimportfilterformconditionvalidation").validationEngine({
		onSuccess: function() {
		dashboardmodulefilter(datimportviewgrid,'dataimport');
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
});
//data import new column map
function importcolumnmap() {
	var impcolname = $('#dataimportcolnames').val();
	var intcollablname = $('#dataimpinternalcolname').val();
	var intcolname = $('#dataimpinternalcolname').find('option:selected').data('columnname');
	var inttablname = $('#dataimpinternalcolname').find('option:selected').data('tablename');
	var intcoluitype = $('#dataimpinternalcolname').find('option:selected').data('uitype');
	var intpartablname = $('#dataimpinternalcolname').find('option:selected').data('parenttable');
	var intcolmodid = $('#dataimpinternalcolname').find('option:selected').data('moduleid');
	{
		var n = $('#fieldmappingaddgrid .gridcontent div.data-content div').length;
		var dataarr = {};
		dataarr['importcolumnname'] = impcolname;
		dataarr['internalcolumnname'] = intcollablname;
		dataarr['columnname'] = intcolname;
		dataarr['tablename'] = inttablname;
		dataarr['uitypeid'] = intcoluitype;
		dataarr['parenttable'] = intpartablname;
		dataarr['moduletabid'] = intcolmodid;
		addinlinegriddata('fieldmappingaddgrid',n+1,dataarr,'json');
	}
	clearform('clearmapdataform');
	/*unmapped field mapping*/
	setTimeout(function(){
		unmappedcolumnamelist();
		datarowselectevt();
	},400);
}
//data import
function dataimportexcel() {
	$("#processoverlay").show();
	var formdata = $("#dataimportfileform").serialize(); 
	var amp = '&';
	var mapfieldcount = $('#fieldmappingaddgrid .gridcontent div.data-content div').length;
	var mapfielddata = getgridrowsdata('fieldmappingaddgrid');
	var fieldmappingdata = JSON.stringify(mapfielddata);
	var datainformation = amp+formdata+amp+'count='+mapfieldcount+amp+'gridfielddata='+fieldmappingdata;
	$.ajax({
		url:base_url+"Dataimport/newuserdatacreationfromfile",
		data:"datas="+datainformation,
		type:"POST",
		aync:false,
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$(".tab-title").removeClass('active');
				$("#subformspan1").addClass('active');
				$(".ftab").addClass('active');
				$(".hiddensubform").hide();
				$("#subformspan1").show();
				$('#dataimpfnamesapn').text('');
				//keyboard Shortcut
				viewgridview = 0; 
				resetFields();
				importdefaultvalset();
				$('#importdetailsform').hide();
				$('#dataimportview').fadeIn(1000);
				datimportviewgrid();
				$("#processoverlay").hide();
				alertpopup('Data imported successfully!');
			} else {
				$(".tab-title").removeClass('active');
				$("#subformspan1").addClass('active');
				$(".ftab").addClass('active');
				$(".hiddensubform").hide();
				$("#subformspan1").show();
				$('#dataimpfnamesapn').text('');
				//keyboard Shortcut
				viewgridview = 0;
				resetFields();
				importdefaultvalset();
				$('#importdetailsform').hide();
				$('#dataimportview').fadeIn(1000);
				datimportviewgrid();
				$("#processoverlay").hide();
				alertpopup('Data import operation fail!');
			}
		},
	});
}
function importdefaultvalset() {
	$('#importmoduleid').trigger('change');
	$('#importduplicate').trigger('change');
	$('#importfiletypeid').select2('val','csv');
	$('#importcharencode').select2('val','UTF-8');
	$('#importdelimiter').select2('val',',');
	$('#dataimpmergerec').empty();
	$('#dataimportfilename').val('');
}
//import column name fetch
function internalcolumnnamegen() {
	var filename = $('#dataimportfilename').val();
	var delimiter = $('#importdelimiter').val();
	var modid = $('#importmoduleid').val();
	if( filename!= "" ) {
		$.ajax({
			url:base_url+"Dataimport/dataimportcolumninformationfetch?fname="+filename+"&delm="+delimiter,
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#dataimportcolnames').empty();
				$('#dataimportcolnames').append($("<option value=''>select</option>"));
				var j=1;
				var impcolname = [];
				$.each(data, function(key,value) {
					$('#dataimportcolnames')
					.append($("<option></option>")
					.attr("value",value)
					.text(value));
					j++;
					impcolname.push(value);
				});
				$('#importcolnameinfo').val(impcolname.join(','));
			},
		});
		$('#dataimportcolnames').trigger('change');
	}
}
//column name mapping gird load
function columndatamappinggridload() {
	var filename = $('#dataimportfilename').val();
	var modid = $('#importmoduleid').val();
	var delimiter = $('#importdelimiter').val();
	if( filename!= "" && modid!="") {
		cleargriddata('fieldmappingaddgrid');
		$.ajax({
			url:base_url+"Dataimport/dataimportcolumnmatchgridinfo?fname="+filename+'&moduleid='+modid+"&delm="+delimiter,
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$.each( data, function( key, value ) {
					var n = $('#fieldmappingaddgrid .gridcontent div.data-content div').length;
					var dataarr = {};
					dataarr['importcolumnname'] = data[key]['importcolumnname'];
					dataarr['internalcolumnname'] = data[key]['internalcolumnname'];
					dataarr['columnname'] = data[key]['columnname'];
					dataarr['tablename'] = data[key]['tablename'];
					dataarr['uitypeid'] = data[key]['uitypeid'];
					dataarr['parenttable'] = data[key]['parenttable'];
					dataarr['moduletabid'] = data[key]['moduletabid'];
					addinlinegriddata('fieldmappingaddgrid',n + 1,dataarr,'json');
				});
			},
		});
		/*unmapped field mapping*/
		setTimeout(function(){
			unmappedcolumnamelist();
			datarowselectevt();
		},400);
	}
}
//un mapped column name mpapping
function unmappedcolumnamelist() {
	var rowids = getgridallrowids('fieldmappingaddgrid')
	cleargriddata('confirmgrid');
	var impmapcolnames = [];
	var colnames = [];
	for( index in rowids ) {
		var colnameval = getgridcolvalue('fieldmappingaddgrid',rowids[index],'importcolumnname','')
		impmapcolnames.push(colnameval);
	};
	var impcolname = $('#importcolnameinfo').val();
	colnames = impcolname.split(',');
	$.each( colnames, function( key, value ) {
		if( $.inArray( value, impmapcolnames ) == -1 ) {
			var n = $('#confirmgrid .gridcontent div.data-content div').length;
			var unmapdataarr = {};
			unmapdataarr['unmappedimpcolname'] = value;
			addinlinegriddata('confirmgrid',n + 1,unmapdataarr,'json');
		}
	});
}

//Data Import View Grid
function datimportviewgrid(page,rowcount) {
	var defrecview = $('#mainviewdefaultview').val();
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
	var wwidth = $('#datimportviewgrid').width();
	var wheight = $(window).height();
	/*col sort*/
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.dataimportheadercolsort').hasClass('datasort') ? $('.dataimportheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.dataimportheadercolsort').hasClass('datasort') ? $('.dataimportheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.dataimportheadercolsort').hasClass('datasort') ? $('.dataimportheadercolsort.datasort').attr('id') : '0';
	var userviewid = 186;
	var viewfieldids = '253';
	var filterid = $("#dataimportfilterid").val();
	var conditionname = $("#dataimportconditionname").val();
	var filtervalue = $("#dataimportfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=activitylog&primaryid=activitylogid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#datimportviewgrid').empty();
			$('#datimportviewgrid').append(data.content);
			$('#datimportviewgridfooter').empty();
			$('#datimportviewgridfooter').append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('datimportviewgrid');
			{//sorting
				$('.dataimportheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.dataimportheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.dataimportheadercolsort').hasClass('datasort') ? $('.dataimportheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.dataimportheadercolsort').hasClass('datasort') ? $('.dataimportheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					var sortpos = $('#datimportviewgrid .gridcontent').scrollLeft();
					datimportviewgrid(page,rowcount);
					$('#datimportviewgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('dataimportheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					datimportviewgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#dataimportpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					datimportviewgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				//Material select
				$('#dataimportpgrowcount').material_select();
			}
		},
	});
}
//Field Mapping Add Grid
function fieldmappingaddgrid() {
	var wwidth = $("#fieldmappingaddgrid").width();
	var wheight = $("#fieldmappingaddgrid").height();
	$.ajax({
		url:base_url+"Dataimport/fieldmapheaderinformationfetch?moduleid=253&width="+wwidth+"&height="+wheight+"&modulename=fieldmapgrid",
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#fieldmappingaddgrid").empty();
			$("#fieldmappingaddgrid").append(data.content);
			$("#fieldmappingaddgridfooter").empty();
			$("#fieldmappingaddgridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('fieldmappingaddgrid');
		},
	});
}
//Confirm Add Grid
function confirmgrid() {
	var wwidth = $("#confirmgrid").width();
	var wheight = $("#confirmgrid").height();
	$.ajax({
		url:base_url+"Dataimport/confirmgridheaderinformationfetch?moduleid=253&width="+wwidth+"&height="+wheight+"&modulename=confirmgrid",
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#confirmgrid").empty();
			$("#confirmgrid").append(data.content);
			$("#confirmgridfooter").empty();
			$("#confirmgridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('confirmgrid');
		},
	});
}