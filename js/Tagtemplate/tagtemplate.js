$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}
	{// Grid Calling Function
		tagprintviewgrid();
	}
	{// For touch
		fortabtouch = 0;
	}
	hiddencomputername = $("#hiddencomputername").val();
	{// For Import Form Details
		$("#addicon").click(function() {
			addslideup('tagtemplatecreationview','tagtemplateformmadd');
			resetFields();
			$('#primaryid').val('');
			$("#dataupdatesubbtn").hide();
			$("#dataaddsbtn").show();
			$('#tabgropdropdown').select2('val','1');
			fortabtouch = 0;
			getcmd();
			$('.fileuploaddraganddpcls').hide();
			$('#tagsetdefaultcboxid').prop('checked',false);
			$("#module").prop('disabled',false);
			$('#tagsetdefault').val('No');
			$('#modulefield').empty();
			$('#computername').val(hiddencomputername);
			firstfieldfocus();
			autogenmaterialupdate();
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
			Materialize.updateTextFields();
		});
		//Reload
		$("#reloadicon").click(function() {
			clearinlinesrchandrgrid('tagprintviewgrid');
		});
		//print footer edit
		$("#editicon").click(function() {
			var datarowid = $('#tagprintviewgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				addslideup('tagtemplatecreationview','tagtemplateformmadd');
				$("#dataupdatesubbtn").show();
				$("#dataaddsbtn").hide();
				$('#primaryid').val('');
				$('#computername').val(hiddencomputername);
				printtemplateeditdata(datarowid);
				//form field first focus
				firstfieldfocus();
				fortabtouch = 1;
				//For Keyboard Shortcut Variables
				addformupdate = 1;
				viewgridview = 0;
				addformview = 1;
				$('.fileuploaddraganddpcls').hide();
				Operation = 1; //for pagination
				Materialize.updateTextFields();
			} else {
				alertpopup(selectrowalert);
			}
		});
	}
	{// Close Add Screen
		var addcloseprintheader =["closeaddform","tagtemplatecreationview","tagtemplateformmadd",""];
		addclose(addcloseprintheader);
		var addcloseviewcreation =["viewcloseformiconid","tagtemplatecreationview","viewcreationformdiv",""];
		addclose(addcloseviewcreation);
	}
	$('#closeaddform').click(function() {
         $('#tagimage,#tagbtw').val('');		
	});
	{//form events.
		//brand changes
		$("#brand").change(function() {
			var value = $(this).val();
			$('#modelno').select2('val','');
			autoprintermodel(value);
			Materialize.updateTextFields();
		});
		//template changes
		$("#printtemplate").change(function(){
			var value = $(this).val();			
			if(checkValue(value) == true){
				var image = $('#printtemplate').find('option:selected').data('image');
				var imgurl = base_url+image;				
				$('#tagsampleimage').empty();
				if(checkValue(image) == true){
					$('#tagsampleimage').html('<img src="'+imgurl+'">');
				}				
				var file = $('#printtemplate').find('option:selected').data('file');
				filenamevaluefetch(file);
			}
		});
		//module changes
		$("#module").change(function(){
			$('#mergetext').val('');
			$('#whereclauseid,#modulefield').select2('val','');
			var moduleid = $(this).val();
			loadrelatedmodule(moduleid);
			loadmodulefield(moduleid);
			loadtagtemplate();
			loadwherecalusefield(moduleid);
			Materialize.updateTextFields();
		});
		$('#relatedmoduleid').change(function() {
			var moduleid = $('#module').val();
			var data1 = $(this).val();
			$('#relatedmodule').val(data1)
			loadmodulefield(moduleid);
		});
		//generate merge texts
		$('#modulefield').change(function(){
			var value = $(this).val();
			$('#mergetext').val('');
			$('#whereclauseid').select2('val','');
			if(checkValue(value) == true){
				var colname = $('#modulefield').find('option:selected').data('colname');
				var id = $.trim($('#modulefield').find('option:selected').data('id'));
				var whereclaues=$.trim($('#whereclauseid').val());
				var mtext='';
				if(id != ''){
					   mtext+='{';
					if(id !=''){
						 mtext+=id+'.'+colname;
					}
					if(whereclaues != '')
					{ 
						 mtext+='|w:'+whereclaues;
					}
					 mtext+='}';
			   }
				$('#mergetext').val(mtext);
			}
			Materialize.updateTextFields();
		});
		$('#whereclauseid').change(function(){
			var value = $(this).val();
			$('#mergetext').val('');
			//if(checkValue(value) == true){
				var colname = $('#modulefield').find('option:selected').data('colname');
				var id = $.trim($('#modulefield').find('option:selected').data('id'));
				var whereclaues=$.trim($('#whereclauseid').val());
				var mtext='';
				if(id != ''){
					   mtext+='{';
					if(id !=''){
						 mtext+=id+'.'+colname;
					}
					if(whereclaues != '')
					{ 
						 mtext+='|w:'+whereclaues;
					}
					 mtext+='}';
			   }
				$('#mergetext').val(mtext);
			//}
			Materialize.updateTextFields();
		});
		//refreshthe form 
		$('#formclearicon').click(function(){
			resetFields();
		});
		//create new templates
		$('#dataaddsbtn').mousedown(function(){
			$("#tagprintvalidate").validationEngine('validate');
		});
		$("#tagprintvalidate").validationEngine( {
			onSuccess: function() {
				var tageditor = $.trim($('#tageditor').val());
				if(tageditor == '') {
					alertpopup('Please give the value in editor');
				}else{
					addtagprint();
				}
			},
			onFailure: function() {	
				var dropdownid =['4','brand','modelno','tagsize','module'];
				dropdownfailureerror(dropdownid);				
			}
		});
		//update templates -
		$('#dataupdatesubbtn').click(function(){
			$("#updatetagprintvalidate").validationEngine('validate');
		});
		jQuery("#updatetagprintvalidate").validationEngine( {
			onSuccess: function() {
				var tageditor = $.trim($('#tageditor').val());
				if(tageditor == '') {
					alertpopup('Please give the value in editor');
				}else{
					updatetagprint();
				}	
			},
			onFailure: function() {	
				var dropdownid =['4','brand','modelno','tagsize','module'];
				dropdownfailureerror(dropdownid);				
			}
		});
		//Delete
		$("#deleteicon").click(function() {
			var datarowid = $('#tagprintviewgrid div.gridcontent div.active').attr('id');
			/* if(softwareindustryid == 3) {
				var table='itemtag';
		    } else {
		    	var table='stockmanagement';
		    } */
			if(datarowid) {
				$.ajax({
					//url: base_url + "Base/checkmapping?level="+1+"&datarowid="+datarowid+"&table="+table+"&fieldid=tagtemplateid",
					url: base_url + "Tagtemplate/checkmapping?datarowid="+datarowid,
					success: function(msg) { 
						if(msg > 0) {
							alertpopup("This tag print is already in usage. You cannot delete this tag print design.");						
						} else {
							$("#basedeleteoverlay").fadeIn();
							$("#basedeleteyes").focus();
						}
					},
				});
			} else {
				alertpopup(selectrowalert);
			}	
		});
		//itemtag yes delete
		$("#basedeleteyes").click(function() {
			var datarowid = $('#tagprintviewgrid div.gridcontent div.active').attr('id'); 
			$.ajax({
				url: base_url + "Tagtemplate/tagprintdelete?primaryid="+datarowid,
				success: function(msg)  {				
					var nmsg =  $.trim(msg);
					refreshgrid();
					$("#basedeleteoverlay").fadeOut();
					if (nmsg == "SUCCESS") { 					
						alertpopup(deletealert);
					} else if (nmsg == "DEFAULT") {					
						alertpopup('Default templates cannot be deleted');
					} else {					
						alertpopup(submiterror);
					}				
				},
			});
		});
	}
	{// File upload				
		fileuploadmoname = 'tagfileupload';
		var companysettings = {
			url: base_url+"upload_file.php",
			method: "POST",
			fileName: "myfile",
			allowedTypes: "prn",
			dataType:'json',
			async:false,			
			onSuccess:function(files,data,xhr) { 
				if(data != 'Size') {
					 // prn file data get
					var prnfile =data.split('*'); 
					var arr = prnfile[0].split(',');
					var name = $('.dyimageupload').data('imgattr');
					$('#tagimage').val(arr[3]); 
					$('#prnfilename').val(prnfile[0]); 
					$('#tageditor').val(prnfile[1]);
					Materialize.updateTextFields();
					alertpopup('Your file is uploaded successfully.');
					} else {
					alertpopup('Image size too large. Image size must be less than 5 megabytes.');
				}
			},
			onError: function(files,status,errMsg) {
			}
		}
		$("#taglogomulitplefileuploader").uploadFile(companysettings);		
		//file upload drop down change function
		$("#tagfileuploadfromid").click(function(){
			$(".triggeruploadtagfileupload:last").trigger('click');
		});
	}
	{// File upload	btw file			
		fileuploadmoname = 'tagfileuploadbtw';
		var companysettings_btw = {
			url: base_url+"upload_file.php",
			method: "POST",
			fileName: "myfile",
			allowedTypes: "btw",
			dataType:'json',
			async:false,			
			onSuccess:function(files,data,xhr) {
				if(data != 'Size') {
					var arr = data.split(',');
					var name = $('.dyimageupload').data('imgattr');
					$('#tagbtw').val(arr[3]); 
					$('#btwfilename').val(arr[0]);
					Materialize.updateTextFields();
					alertpopup('Your file is uploaded successfully.');
				} else {
					alertpopup('Image size too large. Image size must be less than 5 megabytes.');
				}
			},
			onError: function(files,status,errMsg) {}
		}
		$("#taglogomulitplefileuploaderbtw").uploadFile(companysettings_btw);		
		//file upload drop down change function
		$("#tagfileuploadfromidbtw").click(function(){
			$(".triggeruploadtagfileuploadbtw:last").trigger('click');
		});
	}
	{// Image upload file			
		fileuploadmoname = 'tagimageupload';
		var companysettings_image = {
			url: base_url+"upload_file.php",
			method: "POST",
			fileName: "myfile",
			allowedTypes: "png,jpg,jpeg,bmp,gif",
			dataType:'json',
			async:false,			
			onSuccess:function(files,data,xhr) {
				if(data != 'Size') {
					var arr = data.split(',');
					var name = $('.dyimageupload').data('imgattr');
					$('#uploadimage').val(arr[3]); 
					$('#barimagename').val(arr[0]);
					Materialize.updateTextFields();
					alertpopup('Your file is uploaded successfully.');
				} else {
					alertpopup('Image size too large. Image size must be less than 5 megabytes.');
				}
			},
			onError: function(files,status,errMsg) {}
		}
		$("#tagimagemulitplefileuploaderbtw").uploadFile(companysettings_image);		
		//file upload drop down change function
		$("#tagimageuploadfromidbtw").click(function(){
			$(".triggeruploadtagimageupload:last").trigger('click');
		});
	}
	// close preview icon
	$('#closepreviewicon').click(function(){
		$('#tagimageoverlay').fadeOut();
		// location.reload(true);
	});
	{ // set default template for tag printing 
	$('#tagdefault').click(function(){
			var countinfo = sliderimg.getCurrentSlideCount();
			var tagtemplateid=$('#tagprintpreimg'+countinfo).data('id');
			tagsetdefault(tagtemplateid);
    	});
	}
	{ // test print select template
		$('#tagtestprint').click(function(){
			var countinfo = sliderimg.getCurrentSlideCount();
			var tagtemplateid=$('#tagprintpreimg'+countinfo).data('id');
			tagtestprint(tagtemplateid);
		});
	}
	// mention plugin
	/* $.getJSON(base_url+'Tagtemplate/loadautocompletedata', function(data) {
		$("#tageditor").mention({
		users: data,
		after : " "
		});
	}); */
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,tagprintviewgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			tagprintviewgrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
	{//Function For Check box
		$('.checkboxcls').click(function() {
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
});
//tag add print
function addtagprint() {
	var form = $("#tagprintform").serialize();
	var amp = '&';
	var datainformation = amp + form;
	var prn = $('#tageditor').val();
	$.ajax({
        url: base_url +"Tagtemplate/createtagtemplate",
        data: "datas=" + datainformation+amp+"prn="+prn,
		type: "POST",
        success: function(msg) {
			var nmsg =  $.trim(msg);
			$(".ftab").trigger('click');
			resetFields();			
			$('#tagtemplateformmadd').hide();
			$('#tagtemplatecreationview').fadeIn(50);
			refreshgrid();
			if(nmsg == 'SUCCESS') {
				$('#tagimage,#tagbtw').val('');
				alertpopup(savealert);
			} else {
				alertpopup('Error on creating new template ,please try again');
			}
			//For Keyboard Shortcut Variables
			viewgridview = 1;
			addformview = 0;
		},
	});
}
//Print Header View Grid
function tagprintviewgrid(page,rowcount) {
	var defrecview = $('#mainviewdefaultview').val();
	if(Operation == 1){
		var rowcount = $('#tagtemplatepgrowcount').val();
		var page = $('.paging').data('pagenum');
	} else{
		var rowcount = $('#tagtemplatepgrowcount').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
	}
	var wwidth = $('#tagprintviewgrid').width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.tagtemplateheadercolsort').hasClass('datasort') ? $('.tagtemplateheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.tagtemplateheadercolsort').hasClass('datasort') ? $('.tagtemplateheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.tagtemplateheadercolsort').hasClass('datasort') ? $('.tagtemplateheadercolsort.datasort').attr('id') : '0';
	var userviewid = 67;
	var viewfieldids = 38;
	var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
	var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
	var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=tagtemplate&primaryid=tagtemplateid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#tagprintviewgrid').empty();
			$('#tagprintviewgrid').append(data.content);
			$('#tagprintviewgridfooter').empty();
			$('#tagprintviewgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('tagprintviewgrid');
			{//sorting
				$('.tagtemplateheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.tagtemplateheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.tagtemplateheadercolsort').hasClass('datasort') ? $('.tagtemplateheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.tagtemplateheadercolsort').hasClass('datasort') ? $('.tagtemplateheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					tagprintviewgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				sortordertypereset('tagtemplateheadercolsort',headcolid,sortord);
			}
			//onclick 
			$(".gridcontent").click(function(){
				var datarowid = $('#tagprintviewgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			{//pagination
				$('.pvpagnumclass').click(function(){
					Operation = 0;
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					tagprintviewgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#tagtemplatepgrowcount').change(function(){
					Operation = 0;
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = 1;//$('#prev').data('pagenum');
					tagprintviewgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			//Material select
			$('#tagtemplatepgrowcount').material_select();
		},
	});
}
function loadtagtemplate() {
	var value = $('#module').val();				
	//var moduleid = 	
	$('#printtemplate').empty();
	$('#printtemplate').append($("<option></option>"));
	if(checkValue(value) == true){
		$.ajax({
			url: base_url+"Tagtemplate/tagtemplates",
			dataType:'json',
			async:false,
			success: function(data) {				
				if((data.fail) == 'FAILED') {
				} else {
					var newddappend="";
					var newlength=data.length;
					for(var m=0;m < newlength;m++) {
						newddappend +="<option data-image='"+data[m]['image']+"' data-file='"+data[m]['filelocation']+"' value = '" +data[m]['tagtemplateid']+ "'>"+data[m]['tagtemplatename']+"</option>";					
					}
					//after run
					$('#printtemplate').append(newddappend);
				}	
			},
		});
	}	
}
function filenamevaluefetch(filename) {
	//checks whether empty
	if(filename == '' || filename == null )	{
		$("#tageditor").editable("setHTML", "");
	} else {
		$.ajax({
			url: base_url + "Tagtemplate/gettemplatesource?templatename="+filename,
			async:false,
			cache:false,
			success: function(data) {
				if(data != 'Fail') {					
					$("#tageditor").val(data);
				} else {
					$("#tageditor").val(data);
				}
			},
		});
	}	
}
//print template edit data retrieval
function printtemplateeditdata(datarowid) {
	var elementsname=['8','module','templatename','printername','printcommand','prnfilename','btwfilename','barimagename','tagsetdefault'];
	$.ajax({
		url: base_url+"Tagtemplate/retrievetemplate",
		data:{primarydataid:datarowid},
		dataType: 'json',
		async:false,
		cache:false,
		success: function(data) {
			var txtboxname = elementsname;
			var dropdowns = [1,'module'];
			textboxsetvalue(txtboxname,txtboxname,data,dropdowns);
			Materialize.updateTextFields();
			var filename = data['filelocation'];
			var image = data['image']; 
			if(data['tagsetdefault'] == 'Yes'){
				$("#tagsetdefaultcboxid").prop('checked',true);
			}else {
				$("#tagsetdefaultcboxid").prop('checked',false);
			}
			//image settings
			var image = $('#printtemplate').find('option:selected').data('image');
			var imgurl = base_url+image;				
			$('#tagsampleimage').empty();
			if(checkValue(image) == true){
				$('#tagsampleimage').html('<img src="'+imgurl+'">');
			}			
			$('#module').trigger('change');
			if(data['rows'] == 0 ) {
				$("#module").prop('disabled',false);
			}else {
				$("#module").prop('disabled',true);
			}
			filenamevaluefetch(filename);
			$('#primaryid').val(datarowid);
		},
	});
}
//update the tag print edit
function updatetagprint() {
	var tagtemplateid = $('#tagprintviewgrid div.gridcontent div.active').attr('id');
	var form = $("#tagprintform").serialize();
	var amp = '&';
	var datainformation = amp + form;
	var prn = $('#tageditor').val();
	var module = $('#module').find('option:selected').val();
	$.ajax({
        url: base_url +"Tagtemplate/updatetagtemplate",
        data: "datas=" + datainformation+amp+"prn="+prn+amp+"tagtemplateid="+tagtemplateid+amp+"module="+module,
		type: "POST",
        success: function(msg) {
			var nmsg =  $.trim(msg);
			$(".ftab").trigger('click');
			resetFields();			
			$('#tagtemplateformmadd').hide();
			$('#tagtemplatecreationview').fadeIn(50);
			refreshgrid();
			if(nmsg == 'SUCCESS'){
				alertpopup(updatealert);
			} else {
				alertpopup('Error on creating new template ,please try again');
			}
			//For Keyboard Shortcut Variables
			addformupdate = 0;
			viewgridview = 1;
			addformview = 0;
		},
	});
}
function getmodule() {
	$('#module').empty();
	$('#module').append($("<option></option>"));
	 $.ajax({
        url: base_url + "Tagtemplate/getmodule",
       	async:false,
		cache:false,
		dataType:'json',
		success: function(data) { 
			$.each(data, function(index) {
				$('#module')
				.append($("<option></option>")
				.attr("value",data[index]['moduleid'])
				.text(data[index]['modulename']));
			});
        },
    });
}
function getcmd() {
	$.ajax({
        url: base_url + "Tagtemplate/getcmd",
       	async:false,
		cache:false,
		dataType:'json',
		success: function(data) { 
			$('#printcommand').val(data);				
			Materialize.updateTextFields();
        },
    });
}
// tag set default template
function tagsetdefault(tagtemplateid){
	setTimeout(function(){
		$('#processoverlay').show();
	},25);
	$.ajax({
        url: base_url + "Tagtemplate/setdefaulttemplate",
        data: "tagtemplateid="+tagtemplateid,
      	async:false,
		cache:false,
		type: "POST",
		dataType:'text',
		success: function(data) { 
			if(data == 'SUCCESS'){
				$('#processoverlay').hide();
				$('#tagimageoverlay').fadeOut();
				alertpopup('Template is set default successfully');
			} else {				
				$('#processoverlay').hide();
				alertpopup(submiterror);
			} 					
        },
    });
}
// tag test print
function tagtestprint(tagtemplateid) {
	setTimeout(function(){
		$('#processoverlay').show();
	},25);
	$.ajax({
        url: base_url + "itemtag/tagtestprint",
        data: "tagtemplateid="+tagtemplateid,
      	async:false,
		cache:false,
		type: "POST",
		dataType:'text',
		success: function(data) { 
			if(data == 'SUCCESS'){
				$('#processoverlay').hide();
				$('#tagimageoverlay').fadeOut();
				alertpopup('Test Printed successfully');
			} else {				
				$('#processoverlay').hide();
				alertpopup(submiterror);
			} 					
        },
    });
}
// load tag template image
function loadtagimage(){
		$.ajax({
        url: base_url + "Tagtemplate/loadtagimage",
        async:false,
		cache:false,
		type: "POST",
		dataType:'text',
		success: function(data) { 
		   $('#tagimagedisplay').empty();
		   $('#tagimagedisplay').html(data);
        },
    });
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#tagtemplatepgnum li.active').data('pagenum');
		var rowcount = $('ul#tagtemplatepgnumcnt li .page-text .active').data('rowcount');
		tagprintviewgrid(page,rowcount);
	}
}
function loadrelatedmodule(moduleid){
	$('#relatedmoduleid').empty();
	$.ajax({
		url: base_url + "Base/relatedmodulelistfetchmodel",			
		data: "moduleid="+moduleid,
		dataType:'json',
		type: "POST",
		async:false,
		cache:false,
		success: function(data) {					
			var newddappend="";
			var newlength=data.length;
			for(var m=0;m < newlength;m++){
				if(moduleid != data[m]['datasid']){
				newddappend += "<option value ='"+data[m]['datasid']+"' data-mergemoduleidhidden='"+data[m]['datasid']+"' data-mergemoduletype='"+data[m]['datatype']+"'>"+data[m]['dataname']+ " </option>";
				}
			}
			//after run
			$('#relatedmoduleid').append(newddappend);
		},
	});
}
function loadmodulefield(moduleid){
	var relatedmoduleid=$('#relatedmodule').val();
	$('#modulefield').empty();
	$('#modulefield').append($("<option value=''>Select</option>"));
	if(checkValue(moduleid) == true || checkValue(relatedmoduleid) == true){				
		$.ajax({
		url: base_url+"Tagtemplate/modulefieldload",
		data:{moduleid:moduleid,relatedmoduleid:relatedmoduleid},
		dataType:'json',
		type: "POST",
		async:false,
		success: function(data) {	
			dropdownid='modulefield';
			if((data.fail) == 'FAILED') {
			} else {
				prev = ' ';
				$.each(data, function(index) {
					var cur = data[index]['optgroupid'];
					if(prev != cur ) {
						if(prev != " ") {
							$('#modulefield').append($("</optgroup>"));
						}
						$('#modulefield').append($("<optgroup  label='"+data[index]['pname']+"'>"));
						prev = data[index]['optgroupid'];
					}
					$("#"+dropdownid+" optgroup[label='"+data[index]['pname']+"']")
					.append($("<option></option>")
					.attr("data-optgroupid",data[index]['optgroupid'])
					.attr("data-key",data[index]['key'])
					.attr("value",data[index]['pid'])
					.attr("data-id",data[index]['pid'])
					.attr("data-colname",data[index]['label'])
					.text(data[index]['label']));
				});
			}	
		},
		});
	}
}
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $("#viewcreatemoduleid").val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','ViewCreationName','ViewCreationId','viewcreation','ViewCreationModuleId',viewmoduleids);
	if(viewname != "dontset") {
		setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		cleargriddata('viewcreateconditiongrid');
	} else {
		$('#dynamicdddataview').trigger('change');
	}
}
function autogenmaterialupdate(){
	 if($('#printcommand').val().length > 0){
		  $('#printcommand').siblings('label, i').addClass('active');
	 }else{}
}
function loadwherecalusefield(moduleid){
	$('#whereclauseid').empty();
	$('#whereclauseid').append($("<option value=''>None</option>"));
	if(checkValue(moduleid) == true){				
		$.ajax({
		url: base_url+"Printtemplates/whereclauseload",
		data:{moduleid:moduleid},
		dataType:'json',
		type: "POST",
		async:false,
		success: function(data) {	
				$.each(data, function(index) {
				$("#whereclauseid")
				.append($("<option></option>")
				.attr("value",data[index]['id'])
				.text(data[index]['id']));
			});
		},
		});
  	}
}
function varcheckuniquename() {
	var primaryid = $('#primaryid').val();
	var accname = $("#templatename").val();
	var elementpartable = 'tagtemplate';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Template name already exists!";
				}
			}else {
				return "Template name already exists!";
			}
		} 
	}
}