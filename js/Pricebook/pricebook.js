$(document).ready(function() {
	{   //Foundation Initialization
		$(document).foundation();
	}	
	{//Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		pricebookaddgrid();
		firstfieldfocus();
		pricebookcrudactionenable();
	}
	//hidedisplay
	$('#pricebookdataupdatesubbtn').hide();
	$('#pricebooksavebutton').show();
	{//view by drop down change
		$('#dynamicdddataview').change(function(){	
			pricebookaddgrid();
		});
	}
	{
	//keyboard shortcut To reset global variable
		viewgridview=0;
	}
	{//validation for Company Add  
		$('#pricebooksavebutton').mousedown(function(e) {
			$('#pricebookname').mouseout();
			if(e.which == 1 || e.which === undefined) {
				$("#pricebookformaddwizard").validationEngine('validate');
				masterfortouch = 0;
			}	
		});
		jQuery("#pricebookformaddwizard").validationEngine( {
			onSuccess: function() {
				$('#pricebooksavebutton').attr('disabled','disabled');
				pricbookinsert();
			},
			onFailure: function() {
				var dropdownid =['2','currencyid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		$( window ).resize(function() {
			innergridresizeheightset('pricebookaddgrid');
		});
	}
	{//update company information
		$('#pricebookdataupdatesubbtn').mousedown(function(e) {
			$('#pricebookname').mouseout();
			if(e.which == 1 || e.which === undefined) {
				$("#pricebookformeditwizard").validationEngine('validate');
				resetFields();
			}
		});
		jQuery("#pricebookformeditwizard").validationEngine( {
			onSuccess: function() {
				pricebookupdateformdata();
			},
			onFailure: function() {
				var dropdownid =['2','currencyid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	//price book name check
	$("#pricebookname").mouseout(function(){
		var primaryid = $("#pricebookprimarydataid").val();
		var accname = $("#pricebookname").val();
		var elementpartable = $('#pricebookelementspartabname').val();
		var nmsg = "";
		if( accname !="" ) {
			$.ajax({
				url:base_url+"Base/uniquedynamicviewnamecheck",
				data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
				type:"post",
				async:false,
				cache:false,
				success :function(msg) {
					nmsg = $.trim(msg);
				},
			});
			if(nmsg != "False") {
				if(primaryid != ""){
					if(primaryid != nmsg) {
						alertpopup("Pricebook name already exists!");
						$("#pricebookname").val("");
					}
				} else {
					alertpopup("Pricebook name already exists!");
					$("#pricebookname").val("");
				}
			} 
		}
	});		
	{//filter work
		//for toggle
		$('#pricebookviewtoggle').click(function() {
			if ($(".pricebookfilterslide").is(":visible")) {
				$('div.pricebookfilterslide').addClass("filterview-moveleft");
			}else{
				$('div.pricebookfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#pricebookclosefiltertoggle').click(function(){
			$('div.pricebookfilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#pricebookfilterddcondvaluedivhid").hide();
		$("#pricebookfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#pricebookfiltercondvalue").focusout(function(){
				var value = $("#pricebookfiltercondvalue").val();
				$("#pricebookfinalfiltercondvalue").val(value);
				$("#pricebookfilterfinalviewconid").val(value);
			});
			$("#pricebookfilterddcondvalue").change(function(){
				var value = $("#pricebookfilterddcondvalue").val();
				pricebookmainfiltervalue=[];
				$('#pricebookfilterddcondvalue option:selected').each(function(){
				    var $pvalue =$(this).attr('data-ddid');
				    pricebookmainfiltervalue.push($pvalue);
					$("#pricebookfinalfiltercondvalue").val(pricebookmainfiltervalue);
				});
				$("#pricebookfilterfinalviewconid").val(value);
			});
		}
		pricebookfiltername = [];
		$("#pricebookfilteraddcondsubbtn").click(function() {
			$("#pricebookfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#pricebookfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(pricebookaddgrid,'pricebook');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}	
}
//Documents Add Grid
function pricebookaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#pricebookaddgrid").width();
	var wheight = $("#pricebookaddgrid").height();
	var viewname = $('#dynamicdddataview').val();
	$('#viewgridheaderid').text(viewname);
	//col sort
	var sortcol = $("#pricebooksortcolumn").val();
	var sortord = $("#pricebooksortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.pricebookheadercolsort').hasClass('datasort') ? $('.pricebookheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.pricebookheadercolsort').hasClass('datasort') ? $('.pricebookheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.pricebookheadercolsort').hasClass('datasort') ? $('.pricebookheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = 218;
	if(userviewid != '') {
		userviewid= '62';
	}
	var filterid = $("#pricebookfilterid").val();
	var conditionname = $("#pricebookconditionname").val();
	var filtervalue = $("#pricebookfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=pricebook&primaryid=pricebookid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#pricebookaddgrid').empty();
			$('#pricebookaddgrid').append(data.content);
			$('#pricebookaddgridfooter').empty();
			$('#pricebookaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('pricebookaddgrid');
			{//sorting
				$('.pricebookheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.pricebookheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#pricebookpgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.pricebookheadercolsort').hasClass('datasort') ? $('.pricebookheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.pricebookheadercolsort').hasClass('datasort') ? $('.pricebookheadercolsort.datasort').data('sortorder') : '';
					$("#pricebooksortorder").val(sortord);
					$("#pricebooksortcolumn").val(sortcol);
					var sortpos = $('#pricebookaddgrid .gridcontent').scrollLeft();
					pricebookaddgrid(page,rowcount);
					$('#pricebookaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('pricebookheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					pricebookaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#pricebookpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					pricebookaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#pricebookaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#pricebookpgrowcount').material_select();
		},
	});	
}
function pricebookcrudactionenable() {	
	$('#pricebookaddicon').click(function(e){
		$("#pricebooksectionoverlay").removeClass("closed");
		$("#pricebooksectionoverlay").addClass("effectbox");
		e.preventDefault();
		currencydynamicloaddropdown();
		resetFields();
		Materialize.updateTextFields();
		firstfieldfocus();
		$('#pricebookdataupdatesubbtn').hide().removeClass('hidedisplayfwg');
		$('#pricebooksavebutton').show();
	});
	$("#pricebookediticon").click(function(e){
		e.preventDefault();
		var datarowid = $('#pricebookaddgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			resetFields();
			pricebookretrievedata(datarowid);
			Materialize.updateTextFields();
			$("#pricebooksavebutton").hide();
			$("#pricebookdataupdatesubbtn").show().removeClass('hidedisplayfwg');
			firstfieldfocus();
			//For Touch work  
			masterfortouch = 1;
			//Keyboard short cut
			saveformview = 1;
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#pricebookdeleteicon").click(function(e) {		
		e.preventDefault();
		var datarowid = $('#pricebookaddgrid div.gridcontent div.active').attr('id');
		if(datarowid) {		
			clearform('cleardataform');
			$("#pricebookprimarydataid").val(datarowid);
			$(".addbtnclass").removeClass('hidedisplay');
			$(".updatebtnclass").addClass('hidedisplay');
			combainedmoduledeletealert('pricebookdelete');
			$("#pricebookdelete").click(function(){
				var datarowid = $("#pricebookprimarydataid").val();
				pricebookrecorddelete(datarowid);
				$(this).unbind();
			});
		} else {
			alertpopup("Please select a row");
		}
	});
}
{//refresh grid
	function pricebookrefreshgrid() {
		var page = $('ul#pricebookpgnum li.active').data('pagenum');
		var rowcount = $('ul#pricebookpgnumcnt li .page-text .active').data('rowcount');
		pricebookaddgrid(page,rowcount);
	}
}
//price book inert
function pricbookinsert() {
	var formdata = $("#pricebookaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	var territory = $('#territoryid').val();
    $.ajax( {
        url: base_url + "Pricebook/newdatacreate",
        data: "datas=" + datainformation+"&territory="+territory,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	$(".addsectionclose").trigger("click");
				resetFields();
				pricebookrefreshgrid();
				$("#pricebooksavebutton").attr('disabled',false); 
				alertpopup(savealert);
            }
        },
    });
}
{//Addform section overlay close
	$(".addsectionclose").click(function(){
		resetFields();
		$("#pricebooksectionoverlay").removeClass("effectbox");
		$("#pricebooksectionoverlay").addClass("closed");
	});
}
//old information show in form
function pricebookretrievedata(datarowid) {
	var pricebookelementsname = $('#pricebookelementsname').val();
	var pricebookelementstable = $('#pricebookelementstable').val();
	var pricebookelementscolmn = $('#pricebookelementscolmn').val();
	var pricebookelementpartable = $('#pricebookelementspartabname').val();
	$.ajax( {
		url:base_url+"Pricebook/fetchformdataeditdetails?pricebookprimarydataid="+datarowid+"&pricebookelementsname="+pricebookelementsname+"&pricebookelementstable="+pricebookelementstable+"&pricebookelementscolmn="+pricebookelementscolmn+"&pricebookelementpartable="+pricebookelementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				pricebookrefreshgrid();
			}else {
				$("#pricebooksectionoverlay").removeClass("closed");
				$("#pricebooksectionoverlay").addClass("effectbox");
				var txtboxname = pricebookelementsname + ',pricebookprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = pricebookelementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
				//Compound Tax Application
				if(data.territoryid != ''){
					var vcselectid=data.territoryid;
					var selectid = vcselectid.split(',');
					selectid = jQuery.grep(selectid, function(n){ return (n); });
					$('#territoryid').select2('val',selectid).trigger('change');
				}
			}
		}
	});
}
//update old information
function pricebookupdateformdata() {
	var formdata = $("#pricebookaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	var territory = $('#territoryid').val();
    $.ajax({
        url: base_url + "Pricebook/datainformationupdate",
        data: "datas=" + datainformation+"&territory="+territory,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if(nmsg == 'TRUE'){
				$('#pricebookdataupdatesubbtn').hide();
				$('#pricebooksavebutton').show();
				$(".addsectionclose").trigger("click");
				resetFields();
				pricebookrefreshgrid();
				alertpopup(savealert);
				//For Touch work  
				masterfortouch = 0;
				//Keyboard short cut
				saveformview = 0;
			}
        },
    });	
}
//Record Delete
function pricebookrecorddelete(datarowid) {
	var pricebookelementstable = $('#pricebookelementstable').val();
	var pricebookelementspartable = $('#pricebookelementspartabname').val();
    $.ajax({
        url: base_url + "Pricebook/deleteinformationdata?pricebookprimarydataid="+datarowid+"&pricebookelementstable="+pricebookelementstable+"&pricebookelementspartable="+pricebookelementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	pricebookrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Deleted successfully');
            }else if (nmsg == "Denied")  {
            	pricebookrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Permission denied');
            }
        },
    });
}
//currency dd load
function currencydynamicloaddropdown() {
	$('#currencyid').empty();
	$('#currencyid').append($("<option></option>"));
	$.ajax({
		url:base_url+"Pricebook/currencyddload",
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#currencyid')
					.append($("<option></option>")
					.attr("data-currencyid",data[index]['datasid'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}	
		},
	});
}