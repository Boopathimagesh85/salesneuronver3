$(document).ready(function() {	
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		taxterritoriesaddgrid();
		firstfieldfocus();
		crudactionenable();
	}
	//hidedisplay
	$('#taxterritoriesdataupdatesubbtn').hide();
	$('#taxterritoriessavebutton').show();
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			taxterritoriesaddgrid();
		});
	}
	{//reload
		$("#taxterritoriesreloadicon").click(function(){
			clearform('cleardataform');
			refreshgrid();
			$('#taxterritoriesimagedisplay').empty();
			$('#taxterritoriesdataupdatesubbtn').hide();
			$('#taxterritoriessavebutton').show();
	});
		$( window ).resize(function() {
			innergridresizeheightset('taxterritoriesaddgrid');
			sectionpanelheight('taxterritoriessectionoverlay');
		});
		//validation for  Add  
		$('#taxterritoriessavebutton').click(function(e) { 
			if(e.which == 1 || e.which === undefined) {
				masterfortouch = 0;
				$("#taxterritoriesformaddwizard").validationEngine('validate');
			}	
		});
		jQuery("#taxterritoriesformaddwizard").validationEngine( {
			onSuccess: function() {
				$('#taxterritoriessavebutton').attr('disabled','disabled');
				taxterritoriesaddformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//update company information
		$('#taxterritoriesdataupdatesubbtn').click(function(e) { 
			if(e.which == 1 || e.which === undefined) {
				$("#taxterritoriesformeditwizard").validationEngine('validate');
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			}
		});
		jQuery("#taxterritoriesformeditwizard").validationEngine({
			onSuccess: function() {
				$('#taxterritoriesdataupdatesubbtn').attr('disabled','disabled');
				taxterritoriesupdateformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	$("#tab3").click(function() {
		taxterritoriesaddgrid();
	});
	{//filter work
		//for toggle
		$('#taxterritoriesviewtoggle').click(function() {
			if ($(".taxterritoriesfilterslide").is(":visible")) {
				$('div.taxterritoriesfilterslide').addClass("filterview-moveleft");
			}else{
				$('div.taxterritoriesfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#taxterritoriesclosefiltertoggle').click(function(){
			$('div.taxterritoriesfilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#taxterritoriesfilterddcondvaluedivhid").hide();
		$("#taxterritoriesfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#taxterritoriesfiltercondvalue").focusout(function(){
				var value = $("#taxterritoriesfiltercondvalue").val();
				$("#taxterritoriesfinalfiltercondvalue").val(value);
				$("#taxterritoriesfilterfinalviewconid").val(value);
			});
			$("#taxterritoriesfilterddcondvalue").change(function(){
				var value = $("#taxterritoriesfilterddcondvalue").val();
				if( $('#s2id_taxterritoriesfilterddcondvalue').hasClass('select2-container-multi') ) {
					taxterritoriesmainfiltervalue=[];
					$('#taxterritoriesfilterddcondvalue option:selected').each(function(){
					    var $ttvalue =$(this).attr('data-ddid');
					    taxterritoriesmainfiltervalue.push($ttvalue);
						$("#taxterritoriesfinalfiltercondvalue").val(taxterritoriesmainfiltervalue);
					});
					$("#taxterritoriesfilterfinalviewconid").val(value);
				} else {
					$("#taxterritoriesfinalfiltercondvalue").val(value);
					$("#taxterritoriesfilterfinalviewconid").val(value);
				}
			});
		}
		taxterritoriesfiltername = [];
		$("#taxterritoriesfilteraddcondsubbtn").click(function() {
			$("#taxterritoriesfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#taxterritoriesfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				var columnid =$("#taxterritoriesfiltercolumn").val();
				var label =$("#taxterritoriesfiltercolumn").find('option:selected').attr('data-label');
				var condition = $("#taxterritoriesfiltercondition").val();
				var value = $("#taxterritoriesfinalfiltercondvalue").val();
				var data = columnid+"|"+condition+"|"+value;
				var count = taxterritoriesfiltername.length;
				var usecond = label+' is '+condition+' '+value;
				$("#taxterritoriesfilterconddisplay").append("<span class='innnerfilterresult'>"+usecond+" <i class='material-icons taxterritoriesfilterdocclsbtn' data-fileid='"+count+"'>close</i></span>");
				taxterritoriesfiltername.push(data);
				$("#taxterritoriesconditionname").val(taxterritoriesfiltername);
				taxterritoriesaddgrid();
				$(".taxterritoriesfilterdocclsbtn").click(function() {
					var id = $(this).data("fileid");
					var count = taxterritoriesfiltername.length;
					for(var k=0;k<count;k++) {
						if( k == id) {
							var nvalue = $("#taxterritoriesconditionname").val();
							var filternewcond = nvalue.split(',');
							taxterritoriesfiltername = jQuery.grep(filternewcond, function(value) {
							  return value != filternewcond[id];
							});
							$("#taxterritoriesconditionname").val(taxterritoriesfiltername);
							taxterritoriesaddgrid();
						}
					}
					$(this).parent("span").remove();
					$(this).next("br").remove();
					$(this).remove();
				});
				$("#taxterritoriesfiltercolumn").select2('val','');//filterddcondvalue
				$("#taxterritoriesfiltercondition").select2('val','');
				$("#taxterritoriesfilterddcondvalue").select2('val','');
				$("#taxterritoriesfiltercondvalue").val('');
				$("#taxterritoriesfilterfinalviewcondvalue").val('');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}

//Documents Add Grid
function taxterritoriesaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = '1245';
	var wheight = $("#taxterritoriesaddgridwidth").height();
	var viewname = $('#dynamicdddataview').val();
	$('#viewgridheaderid').text(viewname);
	//col sort
	var sortcol = $("#taxterritoriessortcolumn").val();
	var sortord = $("#taxterritoriessortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.taxterritoriesheadercolsort').hasClass('datasort') ? $('.taxterritoriesheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.taxterritoriesheadercolsort').hasClass('datasort') ? $('.taxterritoriesheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.taxterritoriesheadercolsort').hasClass('datasort') ? $('.taxterritoriesheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#taxterritoriesviewfieldids').val();
	if(userviewid != '') {
		userviewid= '76';
	}
	var filterid = $("#taxterritoriesfilterid").val();
	var conditionname = $("#taxterritoriesconditionname").val();
	var filtervalue = $("#taxterritoriesfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=taxterritories&primaryid=taxterritoriesid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#taxterritoriesaddgrid').empty();
			$('#taxterritoriesaddgrid').append(data.content);
			$('#taxterritoriesaddgridfooter').empty();
			$('#taxterritoriesaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('taxterritoriesaddgrid');
			innergridresizeheightset('taxterritoriesaddgrid');
			{//sorting
				$('.taxterritoriesheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.taxterritoriesheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#taxregionpgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.taxterritoriesheadercolsort').hasClass('datasort') ? $('.taxterritoriesheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.taxterritoriesheadercolsort').hasClass('datasort') ? $('.taxterritoriesheadercolsort.datasort').data('sortorder') : '';
					$("#taxterritoriessortorder").val(sortord);
					$("#taxterritoriessortcolumn").val(sortcol);
					var sortpos = $('#taxterritoriesaddgrid .gridcontent').scrollLeft();
					taxterritoriesaddgrid(page,rowcount);
					$('#taxterritoriesaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('taxterritoriesheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					taxterritoriesaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#taxregionpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					taxterritoriesaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#taxterritoriesaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#taxregionpgrowcount').material_select();
		},
	});
}
function crudactionenable() {
	{//add icon click
		$('#taxterritoriesaddicon').click(function(e){
			sectionpanelheight('taxterritoriessectionoverlay');
			$("#taxterritoriessectionoverlay").removeClass("closed");
			$("#taxterritoriessectionoverlay").addClass("effectbox");
			e.preventDefault();
			resetFields();
			Materialize.updateTextFields();
			firstfieldfocus();
			$('#taxterritoriesdataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#taxterritoriessavebutton').show();
		});
	}
	$("#taxterritoriesediticon").click(function(e) {
		e.preventDefault();
		var datarowid = $('#taxterritoriesaddgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			resetFields();
			$(".addbtnclass").addClass('hidedisplay');
			$(".updatebtnclass").removeClass('hidedisplay');
			$('#taxterritoriesimagedisplay').empty();
			taxterritoriesgetformdata(datarowid);
			Materialize.updateTextFields();
			firstfieldfocus();
			masterfortouch = 1;
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#taxterritoriesdeleteicon").click(function(e) {
		e.preventDefault();
		var datarowid = $('#taxterritoriesaddgrid div.gridcontent div.active').attr('id');		
		if(datarowid) {
			clearform('cleardataform');
			$(".addbtnclass").removeClass('hidedisplay');
			$(".updatebtnclass").addClass('hidedisplay');			
			combainedmoduledeletealert('taxterritoriesdeleteyes');
			$("#taxterritoriesdeleteyes").click(function(){
				var datarowid = $('#taxterritoriesaddgrid div.gridcontent div.active').attr('id');
				taxterritoriesrecorddelete(datarowid);
				$(this).unbind();
			});
		} else {
			alertpopup("Please select a row");
		}
	});
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#taxregionpgnum li.active').data('pagenum');
		var rowcount = $('ul#taxregionpgnumcnt li .page-text .active').data('rowcount');
		taxterritoriesaddgrid(page,rowcount);	
	}
}
//new data add submit function
function taxterritoriesaddformdata() {
    var formdata = $("#taxterritoriesaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax( {
        url: base_url + "Taxterritories/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	$(".addsectionclose").trigger("click");
				resetFields();
				refreshgrid();
				$("#taxterritoriessavebutton").attr('disabled',false);
				$('#taxterritoriesimagedisplay').empty();
				alertpopup(savealert);			
            }
        },
    });
}
{//Addform section overlay close
	$(".addsectionclose").click(function(){
		resetFields();
		$("#taxterritoriessectionoverlay").removeClass("effectbox");
		$("#taxterritoriessectionoverlay").addClass("closed");
	});
}
//old information show in form
function taxterritoriesgetformdata(datarowid) {
	var taxterritorieselementsname = $('#taxterritorieselementsname').val();
	var taxterritorieselementstable = $('#taxterritorieselementstable').val();
	var taxterritorieselementscolmn = $('#taxterritorieselementscolmn').val();
	var taxterritorieselementpartable = $('#taxterritorieselementspartabname').val();
	$.ajax(	{
		url:base_url+"Taxterritories/fetchformdataeditdetails?taxterritoriesprimarydataid="+datarowid+"&taxterritorieselementsname="+taxterritorieselementsname+"&taxterritorieselementstable="+taxterritorieselementstable+"&taxterritorieselementscolmn="+taxterritorieselementscolmn+"&taxterritorieselementpartable="+taxterritorieselementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				clearinlinesrchandrgrid('taxterritoriesaddgrid');
			} else {
				sectionpanelheight('taxterritoriessectionoverlay');
				$("#taxterritoriessectionoverlay").removeClass("closed");
				$("#taxterritoriessectionoverlay").addClass("effectbox");
				var txtboxname = taxterritorieselementsname + ',taxterritoriesprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = taxterritorieselementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
				$("#taxterritoriesfileuploadfromid").select2("val","");
				var image = data['taxterritoriesimage'];
				if(image !="") {
					var id = data['fileuploadfromid'];
					if(id == 2){
						$('#taxterritoriesimagedisplay').empty();
						var img = $('<img id="taxterritoriesimagedynamic" style="height:100%;width:100%">');
						img.attr('src', base_url+image);
						img.appendTo('#taxterritoriesimagedisplay');
						$('#taxterritoriesimagedisplay').append('<i class="fa fa-times documentslogodownloadclsbtn"></i>');
						$("#taxterritoriesfileuploadfromid").select2("val","");
						$(".documentslogodownloadclsbtn").click(function(){
							$(this).remove();
							$('#taxterritoriesimagedisplay').empty();
							$('#taxterritoriesimage').val('');
						});
					} else{
						$('#taxterritoriesimagedisplay').empty();
						var img = $('<img id="taxterritoriesimagedynamic" style="height:100%;width:100%">');
						img.attr('src',image);
						img.appendTo('#taxterritoriesimagedisplay');
						$('#taxterritoriesimagedisplay').append('<i class="fa fa-times documentslogodownloadclsbtn"></i>');
						$("#taxterritoriesfileuploadfromid").select2("val","");
						$(".documentslogodownloadclsbtn").click(function(){
							$(this).remove();
							$('#taxterritoriesimagedisplay').empty();
							$('#taxterritoriesimage').val('');
						});
					}
				} else{
					$('#taxterritoriesimagedisplay').empty();
					$("#taxterritoriesfileuploadfromid").select2("val","");
				}
			}
			
		}
	});
	$('#taxterritoriesdataupdatesubbtn').show().removeClass('hidedisplayfwg');
	$('#taxterritoriessavebutton').hide();
}
//udate old information
function taxterritoriesupdateformdata() {
	var formdata = $("#taxterritoriesaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Taxterritories/datainformationupdate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	$(".addsectionclose").trigger("click");
				$('#taxterritoriesdataupdatesubbtn').hide();
				$('#taxterritoriessavebutton').show();
				resetFields();
				$("#taxterritoriesdataupdatesubbtn").attr('disabled',false);
				refreshgrid();
				$('#taxterritoriesimagedisplay').empty();
				alertpopup(savealert);
				//for touch
				masterfortouch = 0;				
            }
        },
    });
	setTimeout(function() {
		var closetrigger = ["taxterritoriescloseadd"];
		triggerclose(closetrigger);
	}, 1000);
}
//udate old information
function taxterritoriesrecorddelete(datarowid) {
	var taxterritorieselementstable = $('#taxterritorieselementstable').val();
	var elementspartable = $('#taxterritorieselementspartabname').val();
    $.ajax({
        url: base_url + "Taxterritories/deleteinformationdata?taxterritoriesprimarydataid="+datarowid+"&taxterritorieselementstable="+taxterritorieselementstable+"&taxterritoriesparenttable="+elementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
			refreshgrid();
			$("#basedeleteoverlayforcommodule").fadeOut();
            if (nmsg == 'TRUE'){				
				alertpopup('Deleted successfully');
            }  else if (nmsg == "Denied")  {				
				alertpopup('Permission denied');
            }
        },
    });
}
//taxterritoriesnamecheck
function taxterritoriesnamecheck() {
	var primaryid = $("#taxterritoriesprimarydataid").val();
	var accname = $("#taxterritoriesname").val();
	var elementpartable = $('#taxterritorieselementspartabname').val();
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "taxterritories name already exists!";
				}
			} else {
				return "taxterritories name already exists!";
			}
		} 
	}
}
//load new taxnames
function loadtaxname(){
	$('#taxid').empty();
	$('#taxid').append($("<option></option>"));
	$.ajax({
		url:base_url+'Taxterritories/gettaxname',
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++) {
			newddappend += "<option value = '" +data[m]['id']+ "'>"+data[m]['name']+"</option>";
				}
				//after run					
				$('#taxid').append(newddappend);
			}
		},
	});
}