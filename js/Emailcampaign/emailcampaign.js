$(document).ready(function() {	
	{// Foundation Initialization
		$(document).foundation();
	}
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		emailcampaignviewgrid();
		//crud action
		crudactionenable();
	}
	{//inner-form-with-grid
		$("#emailcampaignsubingridadd1").click(function(){
			sectionpanelheight('emailcampaignsuboverlay');
			$("#emailcampaignsuboverlay").removeClass("closed");
			$("#emailcampaignsuboverlay").addClass("effectbox");
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$("#emailcampaignsubcancelbutton").click(function(){
			$("#emailcampaignsuboverlay").removeClass("effectbox");
			$("#emailcampaignsuboverlay").addClass("closed");
		});
	}
	$("#emailcampaignsubaddbutton,#mailcampaignsubingriddel1").hide();
	//read only for mobile number text area
	$("#communicationto,#description").attr('readonly',true);
	// froala editor base pass array
	// default arrays
	subscriber =[];
	dndnumbers =[];
	nondndnumbers =[];
	valnumbers =[];
	invnumbers= [];
	$('#formclearicon').click(function() {
		$("#tab1").trigger('click');
		$("#emaillistsid,#emailsegmentsid").val('');
		subscriber =[];
		$("#emailtemplatesid").empty();
		$("#emaillistsid,#emailsegmentsid").select2('val','');
		$("#segmentsubscriberid,#autoupdate,#selectedsegmentsubscriberid,#grouptypeid,#groupmoduleid").val('');
		cleargriddata('emailcampaignsubaddgrid1');
		var froalaarray=['emailcampaigntemplate_editor','html.set',''];
		froalaset(froalaarray);
	});
	//Close Add Screen
	var addcloseoppocreation =["closeaddform",'emailcampaigncreationview','emailcampaigncreationformadd']
	addclose(addcloseoppocreation);	
	var addcloseviewcreation =["viewcloseformiconid","emailcampaigncreationview","viewcreationformdiv",""];
	addclose(addcloseviewcreation);
	// For touch
		fortabtouch = 0;
	//Toolbar Icon Change Function View
	$("#reloadicon").click(function() {
		refreshgrid();
	});
	$( window ).resize(function() {
		maingridresizeheightset('emailcampaignviewgrid');
		sectionpanelheight('emailcampaignsuboverlay');
	});
	{
		//Email Groups
		$("#groupsiconicon").click(function(){
			var type = 'Email';
			sessionStorage.setItem("emailmoduletype",type);
			window.location =base_url+'Smsgroups';
		});
		// Email Segments
		$("#segmentsiconicon").click(function(){
			window.location =base_url+'Smssegments';
		});
		// Email Subscribers
		$("#subscribersiconicon").click(function(){
			window.location =base_url+'Smssubscribers';
		});
	}
	//group dd change
	$("#emaillistsid").change(function() {
		$("#selectedsegmentsubscriberid,#segmentsubscriberid,#autoupdate").val('');
		$('#emailsegmentsid').select2('val','');
		var groupid  = $("#emailsegmentsid").val();
		var listid  = $("#emaillistsid").val();
		defaultmailidfromnameget(listid);
		segmentdropdownvalfetch(listid);
		//grid load
		emailcampaignsubaddgrid1();
		Materialize.updateTextFields();
	});
	//close
	$('#closeaddoppocreation').click(function() {
		$('#dynamicdddataview').trigger('change');
	});
	//view by drop down change
	$('#dynamicdddataview').change(function() {
		emailcampaignviewgrid();
	});
	//send sms
	$('#dataaddsbtn').click(function() {
		$("#formaddwizard").validationEngine('validate');
	});
	jQuery("#formaddwizard").validationEngine({
		onSuccess: function() {
			mailsentprocess();
		},
		onFailure: function() {
			var dropdownid =['2','smstypeid','senderid'];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	//segment drop down change
	$('#emailsegmentsid').change(function() {
		var maillistid = $("#emaillistsid").val();
		if(maillistid) {
			var id = $(this).val();
			var selectid = id;
			if(selectid == '') {
				$("#segmentsubscriberid,#autoupdate").val('');
			}
			relatedsubscriberdatafetch(selectid);
			emailcampaignsubaddgrid1();
		} else {
			$('#emaillistsid').select2('val','');
			alertpopup('Please Select the Mail List');
		}
	});
	$("#tab2").click(function() {
		var campaignname = $("#emailcampaignname").val();
		var groupname = $("#emaillistsid").val();
		var subscriberid = $("#selectedsegmentsubscriberid").val();
		if(campaignname != '' && groupname != '') {
			if(subscriberid != '') {
				folderddvalget('foldernameid');
				signaturevalget();
			} else {
				$("#tab1").click();
				setTimeout(function() {
					//transtion 
					$('#subformspan1').addClass('form-active');
				},500);
				alertpopup('Please select the Email Campaign subscribers');
			}
		} else {
			$("#tab1").click();
			setTimeout(function() {
				//transtion 
				$('#subformspan1').addClass('form-active');
			},500);
			alertpopup('Please select the Email Campaign name and List name');
		}
		Materialize.updateTextFields();
	});
	$("#tab3").click(function() {
		var campaignname = $("#emailcampaignname").val();
		var groupname = $("#emaillistsid").val();
		var subscriberid = $("#selectedsegmentsubscriberid").val();
		if(campaignname != '' && groupname != '') {
			if(subscriberid != '') {
				var ctype = $("#emailcampaigntypeid").val();
				$("#campaigntypeid").select2('val',ctype);
				var canme = $("#emailcampaignname").val();
				$("#campaignname").val(canme);
				var list = $("#emaillistsid").val();
				$("#maillistsid").select2('val',list);
				var segment = $("#emailsegmentsid").val();
				$("#mailsegmentsid").select2('val',segment);
				var fromname = $("#fromname").val();
				$("#cfromname").val(fromname);
				var frommailid = $("#frommailid").val();
				$("#cfrommailid").val(frommailid);
				var segment = $("#emailtemplatesid").val();
				$("#templatesid").select2('val',segment);
				var signature = $("#signatureid").val();
				$("#csignatureid").select2('val',signature);
			} else {
				$("#tab1").click();
				setTimeout(function() {
					//transtion 
					$('#subformspan1').addClass('form-active');
				},500);
				alertpopup('Please select the Email Campaign subscribers');
			}
		} else {
			$("#tab1").click();
			setTimeout(function() {
				//transtion 
				$('#subformspan1').addClass('form-active');
			},500);
			alertpopup('Please select the Email Campaign name and List name');
		}
		Materialize.updateTextFields();
	});
	$('#emailcampaigntypeid').change(function(){
		var ctid = $('#emailcampaigntypeid').val();
		if(ctid == 2){
			$('.fr-toolbar').hide();
		}else{
			$('.fr-toolbar').show();
		}
	});
	$("#foldernameid").change(function() {
		var folderid = $("#foldernameid").val();
		var moduleid = $("#groupmoduleid").val();
		var typeid = $("#grouptypeid").val();
		defaulttemaplteget(folderid,moduleid,typeid);
	});
	$("#emailtemplatesid").change(function() {
		var tempid = $("#emailtemplatesid").val();
		var tempname = $("#emailtemplatesid").find('option:selected').data('tempname');
		tandceditervalueget(tempname)
	});
	//template name change
	$('#leadtemplateid').change(function() {
		var tempid = $('#leadtemplateid').val();
		$.ajax( {
			url:base_url+"SmsCampaign/fetchtemplatedetails?templid="+tempid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				var msgfilename = data.msg;
				templateeditervalueget(msgfilename);
			}
		});
	});
	//signature file value get
	$("#signatureid").change(function() {
		var sigid = $("#signatureid").val();
		if(sigid != '') {
			var mailsignature = $(this).find('option:selected').data('tempname');
			var froalaarray=['emailcampaigntemplate_editor','html.get',true];
		     var oldcontent = froalaset(froalaarray);
			 signaturewitheditordata(mailsignature,oldcontent);
		}
	});
	//time picker
	$('#communicationtime').timepicker({
		'step':15,
		'timeFormat': 'H:i',
		'scrollDefaultNow':true,
		'disableTextInput':true,//restrict text input in time picker
		'disableTouchKeyboard':true,
	});
	$('#communicationtime').on('changeTime', function() {
		$('#communicationtime').focus();
	});
	{//schedule time
		$("#communicationtime").change(function() {
			var date = $("#communicationdate").val();
			var time = $("#communicationtime").val();
			setTimeout(function() {
			$.ajax( {
				url:base_url+"Emailcampaign/scheduletimecalculate?time="+time+"&date="+date, 
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {                
					if(data != 'True') {
						alertpopup('You cant schedule mail on current time');
						$("#communicationtime").val('');
					}
				}
			});
			},100);
		});
	}
	{//message focus out
		$("#leadtemplatecontent").focusout(function() {
			var recordid = $('#segmentsubscriberid').val();
			if(recordid != '') { 
				recordid = recordid;
			} else {
				recordid = '2';
			}
			var content = $('#leadtemplatecontent').val();
			var regExp = /\{([^}]+)\}/g;
			var datacontent = content.match(regExp);
			if(datacontent != null) {
				var newcontent = "";
				$.ajax({
					url:base_url+'SmsCampaign/smsmergcontinformationfetch',
					data:'content='+datacontent+"&recordid="+recordid,
					type:'POST',
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						var newcontent = $('#leadtemplatecontent').val();
						for (var i = 0; i < datacontent.length; i++) {
							newcontent = newcontent.replace(''+datacontent[i]+'',''+data[i]+'');
						}
						$('#description').val(newcontent);
					},
				});
			} else {
				$('#description').val(content);
			}
		});
	}
	//Available credits check
	$("#senderid").change(function(){
		var apikey = $("#senderid").find('option:selected').data('apikey');
		creditsdetailfetch(apikey);
	});
	//preview click
	$("#printheadericonbtn").click(function() {
		var subscriberid = $("#selectedsegmentsubscriberid").val();
		if (subscriberid) {
			//alert();
			var primaryid = $("#selectedsegmentsubscriberid").val();
			var recordid = primaryid.split(',');
			var templateid = $('#emailtemplatesid').val();
			var froalaarray=['emailcampaigntemplate_editor','html.get',true];
		     var content = froalaset(froalaarray);
			$("#editordata").val(content);
			var newcontent = $("#editordata").val();
			var moduleid = $('#emailtemplatesid').find('option:selected').data('moduleid');
			var templatename = $('#emailtemplatesid').find('option:selected').data('name');
			var previewpath = base_url+"Emailcampaign/templatefilepdfpreview";
			var formpreview = $('<form action="'+previewpath +'" class="hidedisplay" name="pdffilepreview" id="pdffilepreview" method="POST" target="_blank"><input type="hidden" name="templateid" id="templateid" value="'+templateid+'" /><input type="hidden" name="content" id="content" value="'+newcontent+'" /><input type="hidden" name="templatename" id="templatename" value="'+templatename+'" /><input type="hidden" name="templatoption" id="templatoption" value="I" /><input type="hidden" name="primaryid" id="primaryid" value="'+recordid[0]+'" /><input type="hidden" name="moduleid" id="moduleid" value="'+moduleid+'" /></form>');
			$(formpreview).appendTo('body');
			formpreview.submit();
		} else {
			alertpopup('Please select a row');
		}
	});
	//test mail overlay show
	$("#pdfheadericonbtn").click(function() {
		var ctype = $("#emailcampaigntypeid").val();
		var cname = $("#emailcampaignname").val();
		var listid = $("#emaillistsid").val();
		var subscriberid = $("#selectedsegmentsubscriberid").val();
		var froalaarray=['emailcampaigntemplate_editor','html.get',true];
		 var content = froalaset(froalaarray);
		if(ctype != '' && cname != '' && listid != '') {
			if(subscriberid != '') {
				if(content != '') {
					$("#testmailoverlayoverlay").fadeIn();
				} else {
					alertpopup('Please Enter Data in Mail Campaign Editor'); 
				}
			} else {
				alertpopup('Please select the Mail campaign subscribers'); 
			}
		} else {
			alertpopup('Please Enter the campaign type or campaign name or mail group');
		}
	});
	//test mail overlay close
	$("#testmailoverlayclose,#testmailnobtn").click(function() {
		$("#testmailoverlayoverlay").fadeOut();
	});
	//test mail sent
	$("#testmailyesbtn").click(function() {
		$("#testmailoverlayvalidation").validationEngine('validate');
	});
	jQuery("#testmailoverlayvalidation").validationEngine({
		onSuccess: function() {
			testmailsentprocess();
		},
		onFailure: function() {			
			alertpopup(validationalert);
		}	
	});
	//detailed view
	$("#detailedviewicon").click(function() {
		var datarowid = $('#emailcampaignviewgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			$("#maillogoverlay").show();
			$("#campaignid").val(datarowid);
			$("#datarowid").val(datarowid);
			cleargriddata('mailloggrid');
			mailloggriddatafetch(datarowid);
			Materialize.updateTextFields();
		} else {
			alertpopup("Please select a row");
		}
	});
	//detail view close
	$("#maillogclose").click(function() {
		$("#maillogoverlay").hide();
	});
	$("#pdfheadericonbtn>i").removeClass('icon24-description');
	$("#pdfheadericonbtn>i").addClass('icon24-drafts');
	$('#pdfheadericonbtn>i>span').text('Test Mail');
	{//export history//excel & CSV
		$('#maillogcsv').click(function() {
			var primaryid = $("#campaignid").val();
			var ExportPath = base_url+"Emailcampaign/exceldataexport";
			var form = $('<form action="'+ExportPath+'" class="hidedisplay" name="excelexport" id="excelexport" method="POST" target="_blank"><input name="primaryid" id="primaryid" value="'+primaryid+'"></form>');
			$(form).appendTo('body');
			form.submit();
		});
	}
	//resent mail
	$("#resendmail").click(function() {
		var datarowid = $('#mailloggrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			var status = getgridcolvalue('mailloggrid',datarowid,'mailstatus','');
			if(status == 'invalid') {
			 	$("#resentmailoverlay").fadeIn();
			 	$("#maillogid").val(datarowid);
			}else {
				alertpopup('You can resent mail only invalid mail status');
			}
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#resentmailyesbtn").click(function() {
		var datarowid = $("#maillogid").val();
		var mailid = $("#resentmailid").val();
		$.ajax({
			url: base_url + "Emailcampaign/resentmaildata",
			data: "datarowid=" + datarowid+"&mailid="+mailid,
			type: "POST",
			success: function(creditdata) {
				alertpopup('Mail Resent Successfully');
			},
		});
	});
	$("#resaentmailclose,#resentmailnobtn").click(function(){
		$("#resentmailoverlay").fadeOut();
		$("#resentmailid").val('');
		$("#maillogid").val('');
	});
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,emailcampaignviewgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			emailcampaignviewgrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
});
function mailloggriddatafetch(datarowid) {
	if(viewid!='') {
		$.ajax({
			url:base_url+'Emailcampaign/getmaillogdata?recordid='+datarowid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					loadinlinegriddata('mailloggrid',data.rows,'json');
					/* data row select event */
					datarowselectevt();
					/* column resize */
					columnresize('mailloggrid');
					var hideprodgridcol = [];
					gridfieldhide('mailloggrid',hideprodgridcol);
				}
			},
		});
	}
}
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','ViewCreationName','ViewCreationId','viewcreation','ViewCreationModuleId',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
{// Main Grid Function
	function emailcampaignviewgrid(page,rowcount) {
		if(Operation == 1){
			var rowcount = $('#emailcampaignpgrowcount').val();
			var page = $('.paging').data('pagenum');
		} else{
			var rowcount = $('#emailcampaignpgrowcount').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		}
		var wwidth = $("#emailcampaignviewgrid").width();
		var wheight = $(window).height();
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.emailcampaignheadercolsort').hasClass('datasort') ? $('.emailcampaignheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.emailcampaignheadercolsort').hasClass('datasort') ? $('.emailcampaignheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.emailcampaignheadercolsort').hasClass('datasort') ? $('.emailcampaignheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = $('#viewfieldids').val();
		var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
		var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
		var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=emailcampaign&primaryid=emailcampaignid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#emailcampaignviewgrid').empty();
				$('#emailcampaignviewgrid').append(data.content);
				$('#emailcampaignviewgridfooter').empty();
				$('#emailcampaignviewgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('emailcampaignviewgrid');
				{//sorting
					$('.emailcampaignheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.emailcampaignheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#mailcampaignpgrowcount li .page-text .active').data('rowcount');
						var sortcol = $('.emailcampaignheadercolsort').hasClass('datasort') ? $('.emailcampaignheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.emailcampaignheadercolsort').hasClass('datasort') ? $('.emailcampaignheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						var sortpos = $('#emailcampaignviewgrid .gridcontent').scrollLeft();
						emailcampaignviewgrid(page,rowcount);
						$('#emailcampaignviewgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('emailcampaignheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						Operation = 0;
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						emailcampaignviewgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#emailcampaignpgrowcount').change(function(){
						Operation = 0;
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = 1;
						emailcampaignviewgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					//Material select
					$('#emailcampaignpgrowcount').material_select();
				}
				//onclick 
				$(".gridcontent").click(function(){
					var datarowid = $('#emailcampaignviewgrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
			},
		});
	}
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function(e) {
			e.preventDefault();
			emailcampaignsubaddgrid1();
			mailloggrid();
			addslideup('emailcampaigncreationview','emailcampaigncreationformadd');
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass,.printheadericonspan,.pdfheadericonspan").removeClass('hidedisplay');
			resetFields();
			$("#tab1").trigger('click');
			setTimeout(function() {
				//transtion 
				$('#subformspan1').addClass('form-active');
			},500);
			$("#emaillistsid,#emailsegmentsid").val('');
			subscriber =[];
			$("#emailtemplatesid").empty();
			$("#emaillistsid,#emailsegmentsid").select2('val','');
			$("#segmentsubscriberid,#autoupdate,#selectedsegmentsubscriberid,#grouptypeid,#groupmoduleid").val('');
			cleargriddata('emailcampaignsubaddgrid1');
			var froalaarray=['emailcampaigntemplate_editor','html.set',''];
		     froalaset(froalaarray);
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			//For Touch
			smsmasterfortouch = 0;
			firstfieldfocus();
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Materialize.updateTextFields();
			Operation = 0; //for pagination
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#mailcampaignpgnum li.active').data('pagenum');
		var rowcount = $('ul#mailcampaignpgrowcount li .page-text .active').data('rowcount');
		emailcampaignviewgrid(page,rowcount);
	}
}
//sms master folder  Grid
function emailcampaignsubaddgrid1(page,rowcount) {
	var emaillistsid = $("#emaillistsid").val();
	var typeid = $("#grouptypeid").val();
	//var typeid = '';
	var segmentid = $("#emailsegmentsid").val();
	if(emaillistsid == ''){ emaillistsid == '1'; }
	var subscriberid = $("#segmentsubscriberid").val();
	if(subscriberid == ''){ subscriberid == ''; }
	var autoupdate = $("#autoupdate").val();
	var id = '271';
	if(id != "") {
		$.ajax({
			url:base_url+"Emailcampaign/defaultviewfetch?modid="+id,
			async:false,
			cache:false,
			success:function(data) {
				if(data != "") {
					viewid = data;
					$("#defaultviewcreationid").val(viewid);
				} else {
					viewid = '271';
					$("#defaultviewcreationid").val(viewid);
				}
			},
		});
		$.ajax({
			url:base_url+"Emailcampaign/parenttableget?moduleid="+id,
			dataType:'json',
			async:false,
			cache:false,
			success:function(pdata) {
				if(pdata != "") {
					maintabinfo = pdata;
					$("#parenttable").val(pdata);
					parentid = maintabinfo+"id";
					$("#parenttableid").val(parentid);
				} else {
					maintabinfo = 'emailsubscribers';
					parentid = 'emailsubscribersid';
					$("#parenttable").val(maintabinfo);
					$("#parenttableid").val(parentid);
				}
			},
		});
	} else {
		viewid = '271';
		maintabinfo = 'subscribers';
		parentid = 'subscribersid';
		$("#defaultviewcreationid").val(viewid);
		$("#parenttable").val(maintabinfo);
		$("#parenttableid").val(parentid);
	}
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#emailcampaignsubaddgrid1').width();
	var wheight = $('#emailcampaignsubaddgrid1').height();
	//col sort
	var sortcol = $('.emailsubscriberlistheadercolsort').hasClass('datasort') ? $('.emailsubscriberlistheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.emailsubscriberlistheadercolsort').hasClass('datasort') ? $('.emailsubscriberlistheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.emailsubscriberlistheadercolsort').hasClass('datasort') ? $('.emailsubscriberlistheadercolsort.datasort').attr('id') : '0';
	$.ajax({
		url:base_url+"Emailcampaign/emailsubscribervalueget?maintabinfo="+maintabinfo+"&primaryid="+parentid+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=270'+"&emaillistsid="+emaillistsid+"&segmentid="+segmentid+"&subscriberid="+subscriberid+"&typeid="+typeid+"&autoupdate="+autoupdate+"&checkbox=true&viewid="+viewid,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#emailcampaignsubaddgrid1').empty();
			$('#emailcampaignsubaddgrid1').append(data.content);
			$('.subgridfootercontainer').empty();
			$('.subgridfootercontainer').append(data.footer);
			//data row select event
			datarowselectevt();
			/*column resize*/
			columnresize('emailcampaignsubaddgrid1');
			{//sorting
				$('.emailsubscriberlistheadercolsort').click(function(){
					$('.emailsubscriberlistheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('ul#emailsubscriberlistpgnum li.active').data('pagenum');
					var rowcount = $('ul#emailsubscriberlistpgnumcnt li .page-text .active').data('rowcount');
					emailcampaignsubaddgrid1(page,rowcount);
				});
				sortordertypereset('emailsubscriberlistheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('ul#emailsubscriberlistpgnumcnt li .page-text .active').data('rowcount');
					emailcampaignsubaddgrid1(page,rowcount);
				});
				$('.pagerowcount').click(function(){
					var page = $('ul#emailsubscriberlistpgnum li.active').data('pagenum');
					var rowcount = $(this).data('rowcount');
					emailcampaignsubaddgrid1(page,rowcount);
				});
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('ul#emailsubscriberlistpgnumcnt li .page-text .active').data('rowcount');
					emailcampaignsubaddgrid1(page,rowcount);
				});
				$('#emailsubscriberlistpgrowcount').change(function(){
					var rowcount = $(this).val();
					var page = $('ul#emailsubscriberlistpgnum li.active').data('pagenum');
					emailcampaignsubaddgrid1(page,rowcount);
				});
			}
			//header check box
			$(".inviteuserlist_headchkboxclass").click(function() {
				if($(".inviteuserlist_headchkboxclass").prop('checked') == true) {
					$(".inviteuserlist_rowchkboxclass").attr('checked', true);
				} else {
					$('.inviteuserlist_rowchkboxclass').removeAttr('checked');
				}
				getcheckboxrowid();
			});
			//row based check box
			$(".inviteuserlist_rowchkboxclass").click(function(){
				$('.inviteuserlist_headchkboxclass').removeAttr('checked');
				getcheckboxrowid();
			});
		}
	});
	Materialize.updateTextFields();
}
function getcheckboxrowid() {
	var selected = [];
	$('.gridcontent input:checked').each(function() {
		selected.push($(this).attr('data-rowid'));
	});
	$("#selectedsegmentsubscriberid").val(selected);
	$("#validsubscribercount").val(selected.length);
	//total subscriber count
	var allrowids = getgridallrowids('emailcampaignsubaddgrid1');
	$("#totalsubscribercount").val(allrowids.length);
}
function mailloggrid(){
	var wwidth = $("#mailloggrid").width();
	var wheight = $("#mailloggrid").height();
	$.ajax({
		url:base_url+"Emailcampaign/maillogdatafetch?moduleid=1&width="+wwidth+"&height="+wheight+"&modulename=Mail Log",
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#mailloggrid").empty();
			$("#mailloggrid").append(data.content);
			$("#mailloggridfooter").empty();
			$("#mailloggridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('mailloggrid');
		},
	});
}
//editor value fetch
function tandceditervalueget(filename) {
	$.ajax({
		url: base_url+"Emailcampaign/editervaluefetch?filename="+filename,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			froalaedit(data,'emailcampaigntemplate_editor');
		},
	});
}
//signature with editor data
function signaturewitheditordata(filename,oldcontent) {
	$.ajax({
		url: base_url+"Emailcampaign/sigeditervaluefetch?filename="+filename,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			if(data != 'Fail') {
				var regExp = /a10s/g;
				var datacontent = data.match(regExp);
				if(datacontent !== null) {	
					for (var i = 0; i < datacontent.length; i++) {
						data = data.replace(''+datacontent[i]+'','&nbsp;');
					}
				}
				var newcontent = oldcontent+'<br/>'+data;
				
				$("#emailcampaigntemplate_editor").froalaEditor("html.set", newcontent);
			} else {
				$("#emailcampaigntemplate_editor").froalaEditor("html.set", oldcontent);
			}
		},
	});
}
{//Related modules drop down load 
	function relatedsubscriberdatafetch(id) {
		$.ajax({
			url:base_url+'Emailcampaign/relatedsubscriberidget',
			data:'segmentid='+id,
			type:'POST',
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) != 'FAILED') {
					var autoupdate = data.autoupdate;
					$("#autoupdate").val(autoupdate);
					$("#segmentsubscriberid").val(data.subscriberid);
					$("#segmentsubscriberid").val($("#segmentsubscriberid").val());
				}
			},
		});
	}
}
//total subscriber data getAttention
function totalsubscriberget(datarowid) {
	subscriber.push(datarowid);
	$("#totalsubscribercount").val(datarowid.length);
	 $("#selectedsegmentsubscriberid").val( subscriber );
	var selsegment = $("#selectedsegmentsubscriberid").val();
	var splitvalues = selsegment.split(',');
	var count = splitvalues.length;
	var uniquesegid = [];
	$.each(splitvalues, function(i, value){
		if($.inArray(value, uniquesegid) === -1) uniquesegid.push(value);
	}); 
	$("#selectedsegmentsubscriberid").val(jQuery.unique( uniquesegid ));
	$("#validsubscribercount").val(datarowid.length);
}
//credit detail fetch function
function creditsdetailfetch(apikey) {
	$.ajax({
		url: base_url + "SmsCampaign/accountcresitdetailsfetch",
		data: "apikey=" + apikey,
		type: "POST",
		dataType:'json',
		async:false,
		cache:false,
		success: function(creditdata) {
			if((creditdata.fail) != 'FAILED') {
				$("#totalavailabecredit").val(creditdata);
			}
		},
	});
}
//default template get
function defaulttemaplteget(folderid,moduleid,typeid) {
	$('#emailtemplatesid,#templatesid').empty();
	$('#emailtemplatesid,#templatesid').append($("<option></option>"));
	$.ajax({
		url: base_url + "Emailcampaign/defeulttemplatevalueget",
		data:"folderid="+folderid+"&moduleid="+moduleid+"&typeid="+typeid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#emailtemplatesid,#templatesid')
					.append($("<option></option>")
					.attr("data-tempname",data[index]['tempname'])
					.attr("data-modulename",data[index]['modulename'])
					.attr("data-moduleid",data[index]['moduleid'])
					.attr("data-name",data[index]['dataname'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}	
		}
	});
}
//default signature get
function signaturevalget() {
	$('#signatureid,#csignatureid').empty();
	$('#signatureid,#csignatureid').append($("<option></option>"));
	$.ajax({
		url: base_url + "Emailcampaign/defeultsignaturevalueget",
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#signatureid,#csignatureid')
					.append($("<option></option>")
					.attr("data-tempname",data[index]['tempname'])
					.attr("data-name",data[index]['dataname'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}	
		}
	});
}
function checktheselectedinvitevalues() {
	var subscriberid = $("#selectedsegmentsubscriberid").val();
	if(subscriberid != '') {
		var subid = subscriberid.split(',');
		checkboxcheck(subid);
	}
}
function checkboxcheck(subid) {
	for(var i=0;i <subid.length;i++){
		jQuery("#emailcampaignsubaddgrid1").setSelection(subid[i],true);
	}
}
//mail id and from name get
function defaultmailidfromnameget(listid) {
	$.ajax({
		url:base_url+'Emailcampaign/mailidandfromnameget',
		data:'listid='+listid,
		type:'POST',
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) != 'FAILED') {
				var fromname = data.fname;
				var frommail = data.fmail;
				var typeid = data.typeid;
				var moduleid = data.moduleid;
				var subject = data.subject;
				$("#fromname").val(fromname);
				$("#frommailid").val(frommail);
				$("#grouptypeid").val(typeid);
				$("#groupmoduleid").val(moduleid);
				$("#subject").val(subject);
			}
		},
	});
} 
//valid mobile number get
function validmobilenumberget(mobilenum) {
	var apikey = $("#senderid").find('option:selected').data('apikey');
	if(mobilenum != '' && apikey != '') {
		$.ajax({
			url:base_url+"SmsCampaign/dndmobilenumbercheck?mobilenum="+mobilenum+"&apikey="+apikey,  
			dataType:'json',
			async:false,
			cache:false,
			success: function(creditdata) {    
				var len = creditdata.data.length;
				for(var i=0;i<len;i++) {
					if(creditdata.data[i]['status'] == 'YES IN DND' || creditdata.data[i]['status'] == 'NOT IN DND') {
						var validnumbers = creditdata.data[i]['number'];
						valnumbers.push(validnumbers);								
					}
				}
				$("#communicationto").val(jQuery.unique(valnumbers));
			}
		});
	}
}
function generaltypedatafetch(mobnum) {
	$.ajax({
		url:base_url+'SmsCampaign/mobilenumbasedsubscriberidget',
		data:'mobnum='+mobnum,
		type:'POST',
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) != 'FAILED') {
				messagecontentget(data);
			}
		},
	});
}
//message content get
function messagecontentget(subsid){
	var ccontent = [];
	for(var i=0;i<subsid.length;i++) {
		var content = $('#leadtemplatecontent').val();
		var regExp = /\{([^}]+)\}/g;
		var datacontent = content.match(regExp);
		if(datacontent != null) {
			$.ajax({
				url:base_url+'SmsCampaign/smsmergcontinformationfetch',
				data:'content='+datacontent+"&recordid="+subsid[i]['susciberid'],
				type:'POST',
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					var newcontent = $('#leadtemplatecontent').val();
					for (var i = 0; i < datacontent.length; i++) {
						newcontent = newcontent.replace(''+datacontent[i]+'',''+data[i]+'');
					}
				},
			});
		}
	}
}
//sms tempalte load
function smstemplateload(fromappmod,grouptype) {
	$('#leadtemplateid').empty();
	$('#leadtemplateid').append($("<option></option>"));
	$.ajax({
		url: base_url+"SmsCampaign/smstemplateddvalfetch?fromappmod="+fromappmod+"&grouptype="+grouptype,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#leadtemplateid')
					.append($("<option></option>")
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}	
		}
	});
} 
//template load
function folderddvalget(ddname) {
	$('#foldernameid').empty();
	$('#foldernameid').append($("<option></option>"));
	$.ajax({
		url: base_url+"Emailcampaign/folderddvalget",
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#foldernameid')
					.append($("<option></option>")
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}	
		}
	});
}
//segments dd load load
function segmentdropdownvalfetch(listid) {
	$('#emailsegmentsid,#mailsegmentsid').empty();
	$('#emailsegmentsid,#mailsegmentsid').append($("<option value=''>Select</option>"));
	$.ajax({
		url: base_url+"Emailcampaign/segmentddload?listid="+listid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#emailsegmentsid,#mailsegmentsid')
					.append($("<option value=''>Select</option>")
					.attr("data-emailsegmentsidhidden",data[index]['dataname'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}	
		}
	});
}
//mail sent
function mailsentprocess() {
	var amp = '&';
	$("#defaultsubseditorval").val('');
	//editor data
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditoradd(editorname);
	var subscriberid = $("#selectedsegmentsubscriberid").val();
	var froalaarray=['emailcampaigntemplate_editor','html.get',true];
	var content = froalaset(froalaarray);
	var regExp = /\{([^}]+)\}/g;
	var datacontent = content.match(regExp);
	var moduleid = $("#emailtemplatesid").find('option:selected').data('moduleid');
	var parenttablename = $('#emailtemplatesid').find('option:selected').data('modulename');
	var templatename = $('#emailtemplatesid').find('option:selected').data('name');
	if(datacontent != null) {
		alert(content);
		//var newcontent = [];
		var ncontent = "";
		var fcontent = "";
		$("#defaultsubseditorval").val(content);
		var edata = $("#defaultsubseditorval").val();
		$.ajax({
			url:base_url+'Emailcampaign/mailmergcontinformationfetch',
			data:{ recordid: subscriberid,moduleid:moduleid,parenttablename:parenttablename,content:edata },
			type:'POST',
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				$("#defaultsubseditorval").val(data);
			},
		});
	} else {
		$("#defaultsubseditorval").val(content);
	}
	var formdata= $("#dataaddform").serialize();
	var maildatas = amp + formdata ;
	if(editordata != '') {
		$("#processoverlay").show();
		$.ajax({
			url: base_url + "Emailcampaign/campaignmailsent",
			data: "datas=" + maildatas,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					$(".ftab").trigger('click');
					resetFields();
					$("#tab1").trigger('click');
					setTimeout(function() {
						//transtion 
						$('#subformspan1').addClass('form-active');
					},500);
					$("#emaillistsid,#emailsegmentsid").val('');
					subscriber =[];
					$("#emailtemplatesid").empty();
					$("#emaillistsid,#emailsegmentsid").select2('val','');
					$("#segmentsubscriberid,#autoupdate,#selectedsegmentsubscriberid,#grouptypeid,#groupmoduleid").val('');
					cleargriddata('emailcampaignsubaddgrid1');
					var froalaarray=['emailcampaigntemplate_editor','html.set',''];
		            froalaset(froalaarray);
					$('#emailcampaigncreationformadd').hide();
					$('#emailcampaigncreationview').fadeIn(1000);
					refreshgrid();
					$("#processoverlay").hide();
					alertpopup("Mail Sent Successfully.Delivered to your inbox soon");
				} else if (nmsg == "COUNT") {
					$("#processoverlay").hide();
					alertpopup("Your mail credentials is too low. so you cant sent mail");
				} else if (msg == "Disallowed Key Characters."){
					$("#processoverlay").hide();
					alertpopup("Restricted symbols (&^!#()) used.Please remove and try again");
				} else {	
					$("#processoverlay").hide();	
					alertpopup("Error during campaign sent.Please refresh and try again");
				}
			},
		});
	} else {
		alertpopup('Please enter the Editor data for this campaign');
	}
}
//test mail sent
function testmailsentprocess() {
	var amp = '&';
	var subscriberid = $("#selectedsegmentsubscriberid").val();
	var recordid = subscriberid.split(",");
	var froalaarray=['emailcampaigntemplate_editor','html.get',true];
	var content = froalaset(froalaarray);
	var regExp = /\{([^}]+)\}/g;
	var datacontent = content.match(regExp);
	var moduleid = $("#templatesid").find('option:selected').data('moduleid');
	var parenttablename = $('#templatesid').find('option:selected').data('modulename');
	var templatename = $('#templatesid').find('option:selected').data('name');
	if(datacontent != null) {
		var newcontent = "";
		$.ajax({
			url:base_url+'Emailcampaign/mailmergcontinformationfetch',
			data:'content='+datacontent+"&recordid="+recordid[0]+"&moduleid="+moduleid+"&parenttablename="+parenttablename,
			type:'POST',
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				var j=0;
				var froalaarray=['emailcampaigntemplate_editor','html.get',true];
		        var newcontent = froalaset(froalaarray);
				for (var i = 0; i < datacontent.length; i++) {
					newcontent = newcontent.replace(''+datacontent[i]+'',''+data[j][i]+'');
				}
				$("#defaultsubseditorval").val(newcontent);
			},
		});
	}else {
		$("#defaultsubseditorval").val(content);
	}
	var formdata = $("#dataaddform").serialize();
	var datainformation = amp + formdata;
	var testmailform = $("#testmailform").serialize();
	var testmaildata = amp + testmailform;
	$.ajax(  {
        url: base_url + "Emailcampaign/testmailcampaignsent",
        data: "datas=" + datainformation+"&testmaildata="+testmaildata,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				$("#testmailid").val('');
				$("#testmailoverlayoverlay").fadeOut();
				alertpopup('Test mail Sent Success fully');
            } else if (nmsg == "COUNT") {
				$("#testmailid").val('');
				$("#testmailoverlayoverlay").fadeOut();
				alertpopup('Your mail credentials is too low. so please update your mail credentials');
            }
        },
    });
}