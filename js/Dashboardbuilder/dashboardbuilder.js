$(document).ready(function() {
	{ //Foundation Initialization
		$(document).foundation();
	}
	{ //show/hide
		$('#chartsettings').hide();
		$('#customwidgetsettings').hide();
		$("#dbdatasavebtn").show();
		$("#dbdataupdateicon").hide();
	}
	editstatus = 0;
	bcount = 0;
	settingtabenable = 0;
	{ // Onload call the Report fetech event
		resetsourcereport(1);
	}
	$("#reportgroupbyid").change(function() {
		$('#reportgroupbyidtext').val($(this).val());
	});
	$("#reportsplitbyid").change(function() {
		$('#reportsplitbyidtext').val($(this).val());
	});
	/* Close report builder */
	$('#dashboardclose').click(function() {
		dashboardcloseconfirmalertmsg('closedashboardform','Do you wish to close form?');
		$("#closedashboardform").val('Close');
		$("#closedashboardform").click(function() {
			window.location=base_url+'Dashboards';
			$('#closedashboardform').unbind();
		});
	});
	{// For Height
		var totalcontainerleftheight = $( window ).height();
		$("#leftsidepanel").css('height',''+totalcontainerleftheight+'px');
		$("#draganddropcontainer").css('height',''+totalcontainerleftheight+'px');
		
		var reducedheightfordoparea =  totalcontainerleftheight - 50;
		$(".dropareadiv").css('height',''+reducedheightfordoparea+'px');
		
		var reducedheight =  totalcontainerleftheight - 92;
		$("#dbuildercontainer2").css('height',''+reducedheight+'px');
		$("#dbuildercontainer3").css('height',''+reducedheight+'px');
	}
	{//Tabs Show hide
		$('.active span').removeAttr('style');
		$("#settingstabid").click(function() {	
			$('.active span i').removeClass('icon24-w');
			$('.active span i').addClass('icon24');
			$('.active span i').addClass('icon24');
			$('.active').removeClass('active');
			$(this).addClass('active');
			$('.active span').removeAttr('style');
			$('.active span i').removeClass('icon24');
			$('.active span i').addClass('icon24-w');
			$("#dbuildercontainer2,#dbuildercontainer3").hide();
			$("#dbuildercontainer1").show();
			Materialize.updateTextFields();
		});
		$("#widgettabid").click(function() {	
			$('.active span i').removeClass('icon24-w');
			$('.active span i').removeClass('icon24');
			$('.active span i').addClass('icon24');
			$('.active').removeClass('active');
			$(this).addClass('active');
			$('.active span').removeAttr('style');
			$('.active span i').removeClass('icon24');
			$('.active span i').addClass('icon24-w');
			$("#dbuildercontainer2").show();
			$("#dbuildercontainer1,#dbuildercontainer3").hide();
			Materialize.updateTextFields();
			var id = 'dbsetmini';
			if ( $('#'+id+'').prop('checked') == true ) {
				$('#miniwidgetcounthidden').val($('#miniwidgetcount').val());
				setTimeout(function(){
					if(editstatus == 0){
				       addminicolumncreate();
					}
				},100);
			} else {
			}
		});
		$("#propertiestabid").click(function() {
			if(settingtabenable == 0){
                alertpopup('Please choose widget or charts');
			}else{
				$('.active span i').removeClass('icon24-w');
				$('.active span i').addClass('icon24');
				$('.active').removeClass('active');
				$(this).addClass('active');
				$('.active span').removeAttr('style');
				$('.active span i').removeClass('icon24');
				$('.active span i').addClass('icon24-w');
				$("#dbuildercontainer3").show();
				$("#dbuildercontainer1,#dbuildercontainer2").hide();
				Materialize.updateTextFields();
			}
		});
	}
	{//Full Screen Preview
		$("#normalviewpreview").hide();
		$("#fullviewpreview").click(function() {	
			$(this).hide();
			$("#normalviewpreview").show();
			$("#leftsidepanel").addClass('hidedisplay');
			$("#draganddropcontainer").removeClass('large-9').addClass('large-12');
			$("#previewcaption").removeClass('hidedisplay');
		});	
		$("#normalviewpreview").click(function() {	
			$(this).hide();
			$("#fullviewpreview").show();
			$("#leftsidepanel").removeClass('hidedisplay');
			$("#draganddropcontainer").removeClass('large-12').addClass('large-9');
			$("#previewcaption").addClass('hidedisplay');
		});
	}
	//accordian
	//Validation for module selction
	if($("#modulename").val() == ''){	
		$("#blockspan").show();
	}else{
		$("#blockspan").hide();
	}
	$("#blockspan").click(function() {
		alertpopup('Please Select the module name');
	});	
	{//Search Dashboard widgets
		$('#dashboardwidgetfilter').bind("change keyup", function() {
            searchWord = $(this).val();
            if (searchWord.length >= 1) { 
				$("#appendwidgetvalue").html('');
                $('ul.search-list-widgets .bgcolorfbelements li').each(function() {
                    text = $(this).text();
                    if (text.match(RegExp(searchWord, 'i'))) {
                        var alllinks = $(this).html();
						$("#appendwidgetvalue").append(alllinks).css('width','5rem');
                    }
                });
				$("#dashboardwidgetfilter").focusout(function(){
					 $("#appendwidgetvalue").children('div').each(function() { 
					  var id = this.id; 
					 {//Task Widget Drag Element Declaration
							var draggableallwidget = document.getElementById(id);
							draggableallwidget.addEventListener('dragstart', dragStart, false);
							draggableallwidget.addEventListener('dragend'  , dragEnd  , false);
					 }
					});
				});
            }else if (searchWord.length <= 1) {
                $('ul.search-list-widgets .bgcolorfbelements li').each(function() {
                    text = $(this).text();
                    if (text.match(RegExp(searchWord, 'i'))) {
                        var alllinks = $(this).html();
						$("#appendwidgetvalue").html('');
                    }
                });
            }
        });
	}
	{//Search Dashboard widgets - Mini
		$('#dashboardwidgetfiltermini').bind("change keyup", function() {
            searchWord = $(this).val();
            if (searchWord.length >= 1) { 
				$("#appendwidgetvaluemini").html('');
                $('ul.search-list-widgets .bgcolorfbelementsmini li').each(function() {
                    text = $(this).text();
                    if (text.match(RegExp(searchWord, 'i'))) {
                        var alllinks = $(this).html();
						$("#appendwidgetvaluemini").append(alllinks).css('width','5rem');
                    }
                });
				$("#dashboardwidgetfiltermini").focusout(function(){
					 $("#appendwidgetvaluemini").children('div').each(function() { 
					  var id = this.id; 
					 {//Task Widget Drag Element Declaration
							var draggableallwidgetmini = document.getElementById(id);
							draggableallwidgetmini.addEventListener('dragstart', dragStart, false);
							draggableallwidgetmini.addEventListener('dragend'  , dragEnd  , false);
					 }
					});
				});
            }else if (searchWord.length <= 1) {
                $('ul.search-list-widgets .bgcolorfbelementsmini li').each(function() {
                    text = $(this).text();
                    if (text.match(RegExp(searchWord, 'i'))) {
                        var alllinks = $(this).html();
						$("#appendwidgetvaluemini").html('');
                    }
                });
            }
        });
	}
	{//Layout Selection
		var col = 0;
		var rowcount = 0;
		$("#addonecolumn").click(function(){
			var one = col++;
			$("#spanforaddtab").append("<li class='large-12 column forcontainerhover'><span id='containermove"+rowcount+"' class='material-icons newsortsec' style='position: absolute; right: 2.5rem; z-index: 1;cursor:pointer;opacity:0;color:#cccccc''>drag_handle</span><span id='containerclose"+rowcount+"' class='material-icons removecontainer1' style='position: absolute; right: 1rem; z-index: 1;cursor:pointer;opacity:0;color:#cccccc'>close</span><ul id='ulid"+rowcount+"' data-ulidnum='"+rowcount+"' data-sectiontype='1'><li id='widgetliid"+one+"' class='mydrag' data-widgetlinum='0'><div class='large-12 column widgetheight forgetid' id='widgetcontainer"+one+"' data-rownum='"+rowcount+"'></div></li></ul><input type='hidden' id='chartrowinfo"+rowcount+"' name='chartrowinfo[]' data-chartrownum='"+rowcount+"' value='"+rowcount+",1,Home' /></li>");
			//To Delete Container
			$(".removecontainer1").click(function(){ 
				var getchildrenid = $(this).next().find('.widgetredirecticon').attr('id');
				$("#"+getchildrenid+"").trigger('click');
				$(this).parent('li').remove();
			});
			{//Close Hover button
				$( ".forcontainerhover" ).hover(
				  function() {
					$( this ).children('span').css('opacity','1');
				  }, function() {
					$( this ).children('span').css('opacity','0');
				  }
				);
			}
			// dashboard builder sorting
			var dbcontainer = document.getElementById("ulid"+rowcount+"");
			Sortable.create(dbcontainer, {
				animation: 150,
				group:'elementsgrp',
				draggable: '.mydrag', 
				handle: '.widgethandler',
				onSort: function (evt) {
				},
				onAdd: function (evt) {
					var dragelm = evt.item.id;
					$('#'+dragelm+'').find("div:first").removeClass("large-4 large-6").addClass("large-12");
					var oldindex =evt.oldIndex;
					
					var newindex =evt.newIndex;
					
					//current ul id and its li elements
					var dragelmul = evt.item.parentNode.id;
					var movefirstliid =$('#'+dragelmul+' li:eq(0)').attr('id'); 
					var movesecondliid =$('#'+dragelmul+' li:eq(1)').attr('id'); 
					var movethirdliid =$('#'+dragelmul+' li:eq(2)').attr('id');
					var movefourthliid =$('#'+dragelmul+' li:eq(3)').attr('id');
					var dragfromelm = evt.from.id;
				
					
					//Sectiontype Condition Checking to drop element(li) in to from(UL)
					var sectiontype = $('#'+dragfromelm+'').data('sectiontype');
					 if(sectiontype==1 && oldindex==0 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-6").addClass("large-12");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
						//For to -- end
					}else if(sectiontype==1 && oldindex==0 && newindex==1){
						var propchangeliid = $('#'+movefirstliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-6").addClass("large-12");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
						//For to -- end
						
					}
					 //2 column to 1 column
					else if(sectiontype==2 && oldindex==0 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
						//For to -- end
					}else if(sectiontype==2 && oldindex==0 && newindex==1){
						var propchangeliid = $('#'+movefirstliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					} else if(sectiontype==2 && oldindex==1 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').appendTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==2 && oldindex==1 && newindex==1){
						var propchangeliid = $('#'+movefirstliid+'').appendTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==2 && oldindex==2 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==2 && oldindex==2 && newindex==1){
						var propchangeliid = $('#'+movefirstliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					} 
					// 3 column to 1 column
					else if(sectiontype==3 && oldindex==0 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==3 && oldindex==0 && newindex==1){
						var propchangeliid = $('#'+movefirstliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==3 && oldindex==1 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').appendTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==3 && oldindex==1 && newindex==1){
						var propchangeli = $('#'+movefirstliid+'').appendTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==3 && oldindex==2 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').appendTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==3 && oldindex==2 && newindex==1){
						var propchangeliid = $('#'+movefirstliid+'').appendTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==3 && oldindex==3 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').appendTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==3 && oldindex==3 && newindex==1){
						var propchangeliid = $('#'+movefirstliid+'').appendTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}  
				},
			});
			rowcount++;
		});
		$("#addtwocolumn").click(function(){
			var one = col++;
			var two = col++;
			$("#spanforaddtab").append("<li class='large-12 column forcontainerhover' style=''><span id='containermove"+rowcount+"' class='material-icons newsortsec' style='position: absolute; right: 2.5rem; z-index: 1;cursor:pointer;opacity:0;color:#cccccc''>drag_handle</span><span id='containerclose"+rowcount+"' class='material-icons removecontainer2' style='position: absolute; right: 1rem; z-index: 1;cursor:pointer;opacity:0;color:#cccccc''>close</span><ul id='ulid"+rowcount+"' data-ulidnum='"+rowcount+"' data-sectiontype='2'><li id='widgetliid"+one+"' class='mydrag' data-widgetlinum='0'><div class='large-6 column widgetheight forgetid' id='widgetcontainer"+one+"' data-rownum='"+rowcount+"'></div></li><li id='widgetliid"+two+"' class='mydrag' data-widgetlinum='1'><div class='large-6 column widgetheight forgetid' id='widgetcontainer"+two+"' data-rownum='"+rowcount+"'></div></li></ul><input type='hidden' id='chartrowinfo"+rowcount+"' name='chartrowinfo[]' data-chartrownum='"+rowcount+"' value='"+rowcount+",2,Home' /></li>");
			//To Delete Container
			$(".removecontainer2").click(function(){
				var getchildrenid = $(this).next().find('.widgetredirecticon').attr('id');
				$("#"+getchildrenid+"").trigger('click');
				var gettwochildrenid = $(this).next().find('.widgetredirecticon').attr('id');
				$("#"+gettwochildrenid+"").trigger('click');
				$(this).parent('li').remove();
			});
			{//Close Hover button
				$( ".forcontainerhover" ).hover(
				  function() {
					$( this ).children('span').css('opacity','1');
				  }, function() {
					$( this ).children('span').css('opacity','0');
				  }
				);
			}
			// dashboard builder sorting
			var dbcontaine = document.getElementById("ulid"+rowcount+"");
			Sortable.create(dbcontaine, {
				animation: 150,
				group:'elementsgrp',
				draggable: '.mydrag', 
				handle: '.widgethandler',
				onAdd: function (evt) {
					// on Add to change the section property (2-column) so -> large-6
					var dragelm = evt.item.id;
					$('#'+dragelm+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
					
					var dragelmul = evt.item.parentNode.id;
					var movefirstliid =$('#'+dragelmul+' li:eq(0)').attr('id'); 
					var movesecondliid =$('#'+dragelmul+' li:eq(1)').attr('id'); 
					var movethirdliid =$('#'+dragelmul+' li:eq(2)').attr('id');
					var movefourthliid =$('#'+dragelmul+' li:eq(3)').attr('id');
					var dragfromelm = evt.from.id;					
					//current moving li id
					var dragel = evt.item.id;					
					// 2 column to 2 column
					var sectiontype = $('#'+dragfromelm+'').data('sectiontype');
					var oldindex = $('#'+dragel+'').data('widgetlinum'); 
					var newindex =evt.newIndex;
					if(sectiontype==2 && oldindex==1 && newindex==1){
						var propchangeliid = $('#'+movethirdliid+'').appendTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontaine.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(1)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==2 && oldindex==1 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').appendTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontaine.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(1)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==2 && oldindex==0 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontaine.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==2 && oldindex==0 && newindex==1){
						var propchangeliid = $('#'+movethirdliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontaine.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(1)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==2 && oldindex==0 && newindex==2){
						var propchangeliid = $('#'+movefirstliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontaine.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(1)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==2 && oldindex==1 && newindex==2){
						var propchangeliid = $('#'+movesecondliid+'').appendTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontaine.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(1)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					} 
					// 3 column to 2 column
					else if(sectiontype==3 && oldindex==0 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontaine.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==3 && oldindex==0 && newindex==1){
						var propchangeliid = $('#'+movethirdliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontaine.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(1)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==3 && oldindex==0 && newindex==2){
						var propchangeliid = $('#'+movethirdliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontaine.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(1)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==3 && oldindex==1 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontaine.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end 
					}else if(sectiontype==3 && oldindex==1 && newindex==1){
						var propchangeliid = $('#'+movethirdliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+dragfromelm+' li:eq(0)').after($('#'+movethirdliid+''));
						$('#'+movethirdliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontaine.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(1)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==3 && oldindex==1 && newindex==2){
						var propchangeliid = $('#'+movethirdliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontaine.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(1)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==3 && oldindex==2 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+movesecondliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontaine.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==3 && oldindex==2 && newindex==1){
						var propchangeliid = $('#'+movethirdliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+dragfromelm+' li:eq(1)').after($('#'+movethirdliid+''));
						$('#'+movethirdliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontaine.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(1)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==3 && oldindex==2 && newindex==2){
						var propchangeliid = $('#'+movethirdliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontaine.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(1)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}
					//1 column to 2 column
					else if(sectiontype==1 && oldindex==0 && newindex==0) {
						var propchangeliid = $('#'+movesecondliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-4").addClass("large-12");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontaine.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
					}else if(sectiontype==1 && oldindex==0 && newindex==1) {
						var propchangeliid = $('#'+movethirdliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-4").addClass("large-12");
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-4").addClass("large-12");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontaine.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(1)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
					}else if(sectiontype==1 && oldindex==0 && newindex==2) {
						var propchangeliid = $('#'+movesecondliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-4").addClass("large-12");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontaine.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(1)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}
				},
				onEnd: function (evt) {
					//from 
				 	var dragfromul = evt.from.id;
					var fromliIds = $('#'+dragfromul+' li').map(function(i,n) {
						return $(n).attr('id');
					});
					$.each( fromliIds, function( i, liid ) {
						$('#'+dragelmul+' li#'+liid+'').data('widgetlinum',i);
					});
					//to
					var dragelmul = evt.item.parentNode.id;
					var liIds = $('#'+dragelmul+' li').map(function(i,n) {
						return $(n).attr('id');
					});
					$.each( liIds, function( i, liid ) {
						$('#'+dragelmul+' li#'+liid+'').data('widgetlinum',i);
					});
				},
			});
			rowcount++;
		});
		$("#addthreecolumn").click(function(){
			var one = col++;
			var two = col++;
			var three = col++;
			$("#spanforaddtab").append("<li class='large-12 column forcontainerhover threecolumn' style=''><span id='containermove"+rowcount+"' class='material-icons newsortsec' style='position: absolute; right: 2.5rem; z-index: 1;cursor:pointer;opacity:0;color:#cccccc''>drag_handle</span><span id='containerclose"+rowcount+"' class='material-icons removecontainer3' style='position: absolute; right: 1rem; z-index: 1;cursor:pointer;opacity:0;color:#cccccc''>close</span><ul id='ulid"+rowcount+"' data-ulidnum='"+rowcount+"' data-sectiontype='3'><li id='widgetliid"+one+"' class='mydrag' data-widgetlinum='0'><div class='large-4 column widgetheight forgetid myclass' id='widgetcontainer"+one+"' data-rownum='"+rowcount+"'></div></li><li id='widgetliid"+two+"' class='mydrag' data-widgetlinum='1'><div class='large-4 column widgetheight forgetid myclass' id='widgetcontainer"+two+"' data-rownum='"+rowcount+"'></div></li><li id='widgetliid"+three+"' class='mydrag' data-widgetlinum='2'><div class='large-4 column widgetheight forgetid myclass' id='widgetcontainer"+three+"' data-rownum='"+rowcount+"'></div></li></ul><input type='hidden' id='chartrowinfo"+rowcount+"' name='chartrowinfo[]' data-chartrownum='"+rowcount+"' value='"+rowcount+",3,Home' /></li");
			//To Delete Container
			$(".removecontainer3").click(function(){
				var getchildrenid = $(this).next().find('.widgetredirecticon').attr('id');
				$("#"+getchildrenid+"").trigger('click');
				var gettwochildrenid = $(this).next().find('.widgetredirecticon').attr('id');
				$("#"+gettwochildrenid+"").trigger('click');
				var getthreechildrenid = $(this).next().find('.widgetredirecticon').attr('id');
				$("#"+getthreechildrenid+"").trigger('click');
				$(this).parent('li').remove();
			});
			{//Close Hover button
				$( ".forcontainerhover" ).hover(
				  function() {
					$( this ).children('span').css('opacity','1');
				  }, function() {
					$( this ).children('span').css('opacity','0');
				  }
				);
			}
			// dashboard builder sorting
			var dbcontain = document.getElementById("ulid"+rowcount+"");
			//new Sortable(dbcontain);
			Sortable.create(dbcontain, {
				animation: 150,
				group:'elementsgrp',
				draggable:'.mydrag', 
				handle: '.widgethandler',
				onSort: function (evt) {
				},
				onAdd: function (evt) {
					// on Add to change the section property (3-column) so -> large-4
					var dragelm = evt.item.id;
					var a =$('#'+dragelm+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
					var dragelmul = evt.item.parentNode.id;
					var moveliid =$('#'+dragelmul+' li:last-child').attr('id');					
					var dragel = evt.item.id;
					var dragelmul = evt.item.parentNode.id;
					var movefirstliid =$('#'+dragelmul+' li:eq(0)').attr('id');
					var movesecondliid =$('#'+dragelmul+' li:eq(1)').attr('id'); 
					var movethirdliid =$('#'+dragelmul+' li:eq(2)').attr('id');
					var movefourthliid =$('#'+dragelmul+' li:eq(3)').attr('id');
					var dragfromelm = evt.from.id;
					
					//3 column to 3 column
					var sectiontype = $('#'+dragfromelm+'').data('sectiontype');
					var oldindex = $('#'+dragel+'').data('widgetlinum'); 
					var newindex =evt.newIndex;
					if(sectiontype==3 && oldindex==0 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==3 && oldindex==0 && newindex==1){
						var propchangeliid = $('#'+movethirdliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(1)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==3 && oldindex==0 && newindex==2){
						var propchangeliid = $('#'+movefourthliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(2)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==3 && oldindex==0 && newindex==3){
						var propchangeliid = $('#'+movefourthliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(2)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==3 && oldindex==1 && newindex==0){
						var propchangeliid = $('#'+movethirdliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==3 && oldindex==1 && newindex==1){
						var propchangeliid = $('#'+movethirdliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(1)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==3 && oldindex==1 && newindex==2){
						var propchangeliid = $('#'+movefourthliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(2)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==3 && oldindex==1 && newindex==3){
						var propchangeliid = $('#'+movefourthliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(2)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==3 && oldindex==2 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==3 && oldindex==2 && newindex==1){
						var propchangeliid = $('#'+movesecondliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(1)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==3 && oldindex==2 && newindex==2){
						var propchangeliid = $('#'+movefourthliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(2)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==3 && oldindex==2 && newindex==3){
						var propchangeliid = $('#'+movefourthliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(2)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}					
					// 2 column to 3 column
					else if(sectiontype==2 && oldindex==0 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==2 && oldindex==0 && newindex==1){
						var propchangeliid = $('#'+movethirdliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(1)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==2 && oldindex==0 && newindex==2){
						var propchangeliid = $('#'+movefourthliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(2)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==2 && oldindex==0 && newindex==3){
						var propchangeliid = $('#'+movefourthliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(2)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==2 && oldindex==1 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').appendTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==2 && oldindex==1 && newindex==1){
						var propchangeliid = $('#'+movethirdliid+'').appendTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(1)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==2 && oldindex==1 && newindex==2){
						var propchangeliid = $('#'+movesecondliid+'').appendTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(2)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==2 && oldindex==1 && newindex==3){
						var propchangeliid = $('#'+movefourthliid+'').appendTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(2)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}
					//1 column to 3 column
					else if(sectiontype==1 && oldindex==0 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-4").addClass("large-12");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==1 && oldindex==0 && newindex==1){
						var propchangeliid = $('#'+movethirdliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-4").addClass("large-12");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(1)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==1 && oldindex==0 && newindex==2){
						var propchangeliid = $('#'+movefourthliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-4").addClass("large-12");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(2)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==1 && oldindex==0 && newindex==3){
						var propchangeliid = $('#'+movefourthliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-4").addClass("large-12");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==1 && oldindex==1 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-4").addClass("large-12");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==1 && oldindex==1 && newindex==1){
						var propchangeliid = $('#'+movethirdliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-4").addClass("large-12");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(1)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==1 && oldindex==1 && newindex==2){
						var propchangeliid = $('#'+movefourthliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-4").addClass("large-12");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(2)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}else if(sectiontype==1 && oldindex==1 && newindex==3){
						var propchangeliid = $('#'+movefourthliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-4").addClass("large-12");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(2)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					} else if(sectiontype==1 && oldindex!=1 && oldindex!=0){
						var propchangeliid = $('#'+moveliid+'').appendTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-6").addClass("large-12");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontain.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq('+newindex+')').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
					}
				},
				onEnd: function (evt) {
					//from 
				 	var dragfromul = evt.from.id;
					var fromliIds = $('#'+dragfromul+' li').map(function(i,n) {
						return $(n).attr('id');
					});
					$.each( fromliIds, function( i, liid ) {
						$('#'+dragelmul+' li#'+liid+'').data('widgetlinum',i);
					});
					//to
					var dragelmul = evt.item.parentNode.id;
					var liIds = $('#'+dragelmul+' li').map(function(i,n) {
						return $(n).attr('id');
					});
					$.each( liIds, function( i, liid ) {
						$('#'+dragelmul+' li#'+liid+'').data('widgetlinum',i);
					});
				},
			});
			rowcount++;
		});
		 // dashboard builder sorting
		var dbmaincontainer = document.getElementById("spanforaddtab");
		new Sortable(dbmaincontainer);
		Sortable.create(dbmaincontainer, {
			animation: 150,
			draggable:'.mydragmain', 
			handle: '.newsortsec',
		});    
	}
	{//Widget tab Accordian
		accordian();
		$(".chartfbuilderaccclick").click(function() {
			$(this).find('.toggleicons').text(' add ');
			var fbuildercatlist = $(this).data('elementcat');
			if($(".elementlist"+fbuildercatlist+"").hasClass( "hideotherelements" )){
				$(".elementlist"+fbuildercatlist+"").slideUp( "slow", function() {	
					$(".hideotherelements").removeClass('hideotherelements');	
				});
			} else {
				$(this).find('.toggleicons').text(' remove ');
				$(".hideotherelements").slideUp( "slow", function() {
					$('.fbuilderaccclick').find('.toggleicons').text(' add ');
					setTimeout(function() {
						var ff = $(".hideotherelements").prev('li').find('.toggleicons').text(' remove ');
					},10); 
					$(".hideotherelements").removeClass('hideotherelements');
				});
				$(".elementlist"+fbuildercatlist+"").slideDown( "slow", function() {	
					$(".elementlist"+fbuildercatlist+"").addClass('hideotherelements');
				});
			}
		});
	}
	{//Select type Exisiting or New
		$("#selecttype").change(function(){
			var charttype = $("#selecttype").val();
			if(charttype == '1'){
				$("#existingchart").show();
				$("#tocreatechart").hide();
				$('.search-list-widgets').removeClass('search-list-widgets');
				$("#existingchart").addClass('search-list-widgets');
				$("#appendwidgetvalue").text('');
				$("#dashboardwidgetfilter").val('');
				
			}else{
				$("#existingchart").hide();
				$("#tocreatechart").show();
				$('.search-list-widgets').removeClass('search-list-widgets');
				$("#tocreatechart").addClass('search-list-widgets');
				$("#appendwidgetvalue").text('');
				$("#dashboardwidgetfilter").val('');
			}
		});
		drageventlistener();
	}
	function drageventlistener() {
		{//Pie Drag Element Declaration
			var draggablepiechart = document.getElementById("piechart");
			draggablepiechart.addEventListener('dragstart', dragStart, false);
			draggablepiechart.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Donut Drag Element Declaration
			var draggabledonutchart = document.getElementById("donutchart");
			draggabledonutchart.addEventListener('dragstart', dragStart, false);
			draggabledonutchart.addEventListener('dragend'  , dragEnd  , false);
		}	
		{//Funnel Drag Element Declaration
			var draggablefunnelchart = document.getElementById("funnelchart");
			draggablefunnelchart.addEventListener('dragstart', dragStart, false);
			draggablefunnelchart.addEventListener('dragend'  , dragEnd  , false);
		}	
		{//Pyramid Drag Element Declaration
			var draggablepyramidchart = document.getElementById("pyramidchart");
			draggablepyramidchart.addEventListener('dragstart', dragStart, false);
			draggablepyramidchart.addEventListener('dragend'  , dragEnd  , false);
		}	
		{//Column Chart Drag Element Declaration
			var draggablecolumnchartchart = document.getElementById("columnchart");
			draggablecolumnchartchart.addEventListener('dragstart', dragStart, false);
			draggablecolumnchartchart.addEventListener('dragend'  , dragEnd  , false);
		}			
		{//Stacked Chart Drag Element Declaration
			var draggablestackedchart = document.getElementById("stackedchart");
			draggablestackedchart.addEventListener('dragstart', dragStart, false);
			draggablestackedchart.addEventListener('dragend'  , dragEnd  , false);
		}	
		{//Anuglar Gauge Chart Drag Element Declaration
			var draggableanuglargaugechart = document.getElementById("anuglargaugechart");
			draggableanuglargaugechart.addEventListener('dragstart', dragStart, false);
			draggableanuglargaugechart.addEventListener('dragend'  , dragEnd  , false);
		}	
		{//Bubble Chart Drag Element Declaration
			var draggablebubblechartchart = document.getElementById("bubblechart");
			draggablebubblechartchart.addEventListener('dragstart', dragStart, false);
			draggablebubblechartchart.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Area Chart Drag Element Declaration
			var draggableareachart = document.getElementById("areachart");
			draggableareachart.addEventListener('dragstart', dragStart, false);
			draggableareachart.addEventListener('dragend'  , dragEnd  , false);
		}	
		{//Line Chart Drag Element Declaration
			var draggablelinechart = document.getElementById("linechart");
			draggablelinechart.addEventListener('dragstart', dragStart, false);
			draggablelinechart.addEventListener('dragend'  , dragEnd  , false);
		}	
		{//Task Widget Drag Element Declaration
			var draggabletaskwidget = document.getElementById("ibtaskwidget");
			draggabletaskwidget.addEventListener('dragstart', dragStart, false);
			draggabletaskwidget.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Documents Widget Drag Element Declaration
			var draggabledocumentswidget = document.getElementById("ibdocumentswidget");
			draggabledocumentswidget.addEventListener('dragstart', dragStart, false);
			draggabledocumentswidget.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Task Widget Drag Element Declaration - Mini
			var draggabletaskwidgetmini = document.getElementById("ibtaskwidgetmini");
			draggabletaskwidgetmini.addEventListener('dragstart', dragStart, false);
			draggabletaskwidgetmini.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Documents Widget Drag Element Declaration - Mini
			var draggabledocumentswidgetmini = document.getElementById("ibdocumentswidgetmini");
			draggabledocumentswidgetmini.addEventListener('dragstart', dragStart, false);
			draggabledocumentswidgetmini.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Activities Widget Drag Element Declaration
			var draggableactivitieswidget = document.getElementById("ibactivitieswidget");
			draggableactivitieswidget.addEventListener('dragstart', dragStart, false);
			draggableactivitieswidget.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Conversation Widget Drag Element Declaration
			var draggableconversationwidget = document.getElementById("ibconversationwidget");
			draggableconversationwidget.addEventListener('dragstart', dragStart, false);
			draggableconversationwidget.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Mail Log Widget Drag Element Declarations
			var draggablemaillogwidget = document.getElementById("ibmaillogwidget");
			draggablemaillogwidget.addEventListener('dragstart', dragStart, false);
			draggablemaillogwidget.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Notification Widget Drag Element Declaration
			var draggablenotificationwidget = document.getElementById("ibnotificationwidget");
			draggablenotificationwidget.addEventListener('dragstart', dragStart, false);
			draggablenotificationwidget.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Notification Widget Drag Element Declaration - Mini
			var draggablenotificationwidgetmini = document.getElementById("ibnotificationwidgetmini");
			draggablenotificationwidgetmini.addEventListener('dragstart', dragStart, false);
			draggablenotificationwidgetmini.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Opportunity Widget Drag Element Declaration
			var draggableopportunitywidget = document.getElementById("ibopportunitywidget");
			draggableopportunitywidget.addEventListener('dragstart', dragStart, false);
			draggableopportunitywidget.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Contacts Widget Drag Element Declaration
			var draggablecontactswidget = document.getElementById("ibcontactswidget");
			draggablecontactswidget.addEventListener('dragstart', dragStart, false);
			draggablecontactswidget.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Invoice Widget Drag Element Declaration
			var draggableinvoicewidget = document.getElementById("ibinvoicewidget");
			draggableinvoicewidget.addEventListener('dragstart', dragStart, false);
			draggableinvoicewidget.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Quote Widget Drag Element Declaration
			var draggablequotewidget = document.getElementById("ibquotewidget");
			draggablequotewidget.addEventListener('dragstart', dragStart, false);
			draggablequotewidget.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Lead Status Widget Drag Element Declaration
			var draggableleadstatuswidget = document.getElementById("ibleadstatuswidget");
			draggableleadstatuswidget.addEventListener('dragstart', dragStart, false);
			draggableleadstatuswidget.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Accounts Widget Drag Element Declaration
			var draggableaccountswidget = document.getElementById("ibaccountswidget");
			draggableaccountswidget.addEventListener('dragstart', dragStart, false);
			draggableaccountswidget.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Tickets Widget Drag Element Declaration
			var draggableticketswidget = document.getElementById("ibticketswidget");
			draggableticketswidget.addEventListener('dragstart', dragStart, false);
			draggableticketswidget.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Campaign Widget Drag Element Declaration
			var draggablecampaignwidget = document.getElementById("ibcampaignwidget");
			draggablecampaignwidget.addEventListener('dragstart', dragStart, false);
			draggablecampaignwidget.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Sales order Widget Drag Element Declaration
			var draggablesaleorderwidget = document.getElementById("ibsalesorderwidget");
			draggablesaleorderwidget.addEventListener('dragstart', dragStart, false);
			draggablesaleorderwidget.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Purchase order Widget Drag Element Declaration
			var draggablepurchaseorderwidget = document.getElementById("ibpurchaseorderwidget");
			draggablepurchaseorderwidget.addEventListener('dragstart', dragStart, false);
			draggablepurchaseorderwidget.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Solution Widget Drag Element Declaration
			var draggablesolutionswidget = document.getElementById("ibsolutionwidget");
			draggablesolutionswidget.addEventListener('dragstart', dragStart, false);
			draggablesolutionswidget.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Solution Category Widget Drag Element Declaration
			var draggablesolutionscatwidget = document.getElementById("ibsolutioncatwidget");
			draggablesolutionscatwidget.addEventListener('dragstart', dragStart, false);
			draggablesolutionscatwidget.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Contract Widget Drag Element Declaration
			var draggablecontractwidget = document.getElementById("ibcontractwidget");
			draggablecontractwidget.addEventListener('dragstart', dragStart, false);
			draggablecontractwidget.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Lead Widget Drag Element Declaration
			var draggableleadwidget = document.getElementById("ibleadwidget");
			draggableleadwidget.addEventListener('dragstart', dragStart, false);
			draggableleadwidget.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Time and date Widget Drag Element Declaration
			var draggabletimeanddatewidget = document.getElementById("ibtimeanddatewidget");
			draggabletimeanddatewidget.addEventListener('dragstart', dragStart, false);
			draggabletimeanddatewidget.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Calendar Widget Drag Element Declaration
			var draggablecalendarwidget = document.getElementById("ibcalendarwidget");
			draggablecalendarwidget.addEventListener('dragstart', dragStart, false);
			draggablecalendarwidget.addEventListener('dragend'  , dragEnd  , false);
		}
		{//Product Widget Drag Element Declaration
			var draggableproductwidget = document.getElementById("ibproductwidget");
			draggableproductwidget.addEventListener('dragstart', dragStart, false);
			draggableproductwidget.addEventListener('dragend'  , dragEnd  , false);
		}
		{//PriceBook Widget Drag Element Declaration
			var draggablepricebookwidget = document.getElementById("ibpricebookwidget");
			draggablepricebookwidget.addEventListener('dragstart', dragStart, false);
			draggablepricebookwidget.addEventListener('dragend'  , dragEnd  , false);
		}	
		{//Drop traget Area
			var droptarget = document.getElementById("draganddropcontainer");
			droptarget.addEventListener('dragenter', dragEnter,false);
			droptarget.addEventListener('dragover', dragOver,false);
			droptarget.addEventListener('dragleave', dragLeave,false);
			droptarget.addEventListener('drop', dropc, false);
		}
	}
	function resetdrageventlistener(val) {
		if(val > 0) {
			$.ajax({
				url: base_url + "Dashboardbuilder/regeneratedraggable",
				data: "moduleid="+val,
				type: "POST",
				dataType:'json',
				cache:false,
				success: function(data) {
					for(var k=0;k < data.length;k++) {
						var draggable = document.getElementById(data[k]);
						draggable.addEventListener('dragstart', dragStart, false);
						draggable.addEventListener('dragend'  , dragEnd  , false);
					}
				},
			});
		}
	}
   /* Draggable event handlers */
    function dragStart(event) {		
        event.dataTransfer.setData('Text', event.target.id);
	 }
    function dragEnd(event) {
	 }
    /* Drop target event handlers */
    function dragEnter(event) {
	}
    function dragOver(event) {
        event.preventDefault();
        return false;
    }
    function dragLeave(event) {
    }
	//var bcount = 0;
    function dropc(event) {
		event.preventDefault();
		var data = event.dataTransfer.getData("Text");
		if(data == 'piechart') {
			var charttype = "pie";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				//bcount = 4;
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-11 column paddingzero"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler" style="cursor:move">drag_handle</span></div><span class="material-icons graphremoveicon removecharts myhandle" title="Remove">close</span><div class="graphstyletop" data-chartcount="'+bcount+'" data-charttypedata="'+charttype+'" ><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',chartcreated'+bcount+',Chart Title,false,,false,circle" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"><div id="chartcreated'+bcount+'" class="graph400x300"></div></div></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						var piechartarray = ['chartcreated'+bcount+'','(Demo)Pie Chart','false'];
						piechartfun(piechartarray);
					});
					bcount++;
					//For get and Set value
					valuesettingfunction();
					//For Remove Charts
					$('.removecharts').click(function(){
						var toremoveblock = $(this).parent('div').parent('div').attr('id');
						$("#"+toremoveblock+"").removeClass('toblockmearge');
						$(this).parent('div').remove();
						$('#chartsettings').hide();
					});
					 // dashboard container  sorting - without grid or editor
					var dbinnercontainer = document.getElementById("spanforaddtab");
					new Sortable(dbinnercontainer);
					Sortable.create(dbinnercontainer, {
						animation: 150,
						group: 'dbinnercontainer',
						draggable: '.myid', 
					}); 
					
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		} else if(data == 'donutchart') {
			var charttype = "donut";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id'); 
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-11 column paddingzero"><div class="large-1 columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><span class="material-icons graphremoveicon removecharts myhandle" title="Remove">close</span><div class="graphstyletop" data-chartcount="'+bcount+'" data-charttypedata="'+charttype+'" ><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',chartcreated'+bcount+',Chart Title,false,,false,circle" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"><div id="chartcreated'+bcount+'" class="graph400x300"></div></div></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						var donutchartarray = ['chartcreated'+bcount+'','(Demo)Donut Chart','false'];
						donutchartfun(donutchartarray);
					});
					bcount++;
					//For get and Set value
					valuesettingfunction();
					//For Remove Charts
					$('.removecharts').click(function(){
						var toremoveblock = $(this).parent('div').parent('div').attr('id');
						$("#"+toremoveblock+"").removeClass('toblockmearge');
						$(this).parent('div').remove();
						$('#chartsettings').hide();
					});
					 // dashboard container  sorting - without grid or editor
					var dbinnercontainer = document.getElementById("ulid"+rowcount+"");
					new Sortable(dbinnercontainer);
					Sortable.create(dbinnercontainer, {
						animation: 150,
						group: 'elementsgrp',
						draggable: '.myid',
					}); 
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		} else if(data == 'funnelchart') { 
			var charttype = "funnel";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-11 column paddingzero"><div class="large-1 columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><span class="material-icons graphremoveicon removecharts" title="Remove">close</span><div class="graphstyletop" data-chartcount="'+bcount+'" data-charttypedata="'+charttype+'" ><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',chartcreated'+bcount+',Chart Title,vertical,false,circle" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"><div id="chartcreated'+bcount+'" class="graph400x300"></div></div></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						var funnelchartarray = ['chartcreated'+bcount+'','(Demo)Funnel Chart','vertical'];
						funnelchartfun(funnelchartarray);
					});
					bcount++;
					//For get and Set value
					valuesettingfunction();
					//For Remove Charts
					$('.removecharts').click(function(){
						var toremoveblock = $(this).parent('div').parent('div').attr('id');
						$("#"+toremoveblock+"").removeClass('toblockmearge');
						$(this).parent('div').remove();
					});
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		} else if(data == 'pyramidchart') {
			var charttype = "pyramid";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-11 column paddingzero"><div class="large-1 columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><span class="material-icons graphremoveicon removecharts" title="Remove">close</span><div class="graphstyletop" data-chartcount="'+bcount+'" data-charttypedata="'+charttype+'" ><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',chartcreated'+bcount+',Chart Title,vertical,false,circle" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"><div id="chartcreated'+bcount+'" class="graph400x300"></div></div></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						var pyramidchartarray = ['chartcreated'+bcount+'','(Demo)Pyramid Chart','vertical'];
						pyramidchartfun(pyramidchartarray);
					});
					bcount++;		
					//For get and Set value
					valuesettingfunction();
					//For Remove Charts
					$('.removecharts').click(function(){
						var toremoveblock = $(this).parent('div').parent('div').attr('id');
						$("#"+toremoveblock+"").removeClass('toblockmearge');
						$(this).parent('div').remove();
						$('#chartsettings').hide();
					});
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}
		else if(data == 'columnchart') {  
			var charttype = "column";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
			
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-11 column paddingzero"><div class="large-1 columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><span class="material-icons graphremoveicon removecharts" title="Remove">close</span><div class="graphstyletop" data-chartcount="'+bcount+'" data-charttypedata="'+charttype+'" ><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',chartcreated'+bcount+',Chart Title,false,vertical,bar,false,circle" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"><div id="chartcreated'+bcount+'" class="graph400x300"></div></div></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
					var columnchartarray = ['chartcreated'+bcount+'','(Demo)Column Chart','vertical'];
					columnchartfun(columnchartarray);
					});
					bcount++;
					//For get and Set value
					valuesettingfunction();
					//For Remove Charts
					$('.removecharts').click(function(){
						var toremoveblock = $(this).parent('div').parent('div').attr('id');
						$("#"+toremoveblock+"").removeClass('toblockmearge');
						$(this).parent('div').remove();
						$('#chartsettings').hide();
					});
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}
		else if(data == 'stackedchart')
		{  
			var charttype = "stacked";
			$(".forgetid").mouseenter(function(){
					var currentblockid = $(this).attr('id'); 
					var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-11 column paddingzero"><div class="large-1 columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><span class="material-icons graphremoveicon removecharts" title="Remove">close</span><div class="graphstyletop" data-chartcount="'+bcount+'" data-charttypedata="'+charttype+'" ><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',chartcreted'+bcount+',Chart Title,vertical,normal" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"><div id="chartcreated'+bcount+'" class="graph400x300"></div></div></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						var stackedchartarray = ['chartcreated'+bcount+'','(Demo)Stacked Chart','vertical','normal'];
						stackedchartfun(stackedchartarray);
					});
					bcount++;		
					//For get and Set value
					valuesettingfunction();
					//For Remove Charts
					$('.removecharts').click(function(){
						var toremoveblock = $(this).parent('div').parent('div').attr('id');
						$("#"+toremoveblock+"").removeClass('toblockmearge');
						$(this).parent('div').remove();
						$('#chartsettings').hide();
					});
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}
		else if(data == 'anuglargaugechart') {
			var charttype = "angulargauge";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id'); 
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-11 column paddingzero"><div class="large-1 columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><span class="material-icons graphremoveicon removecharts myhandle" title="Remove">close</span><div class="graphstyletop" data-chartcount="'+bcount+'" data-charttypedata="'+charttype+'" ><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',chartcreated'+bcount+',Chart Title,false,circle" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"><div id="chartcreated'+bcount+'" class="graph400x300"></div></div></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						var angulargaugechartarray = ['chartcreated'+bcount+'','(Demo)Gauge Chart','gauge'];
						angulargaguechartfun(angulargaugechartarray);
					});
					bcount++;		
					//For get and Set value
					valuesettingfunction();
					//For Remove Charts
					$('.removecharts').click(function(){
						var toremoveblock = $(this).parent('div').parent('div').attr('id');
						$("#"+toremoveblock+"").removeClass('toblockmearge');
						$(this).parent('div').remove();
						$('#chartsettings').hide();
					});
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}
		else if(data == 'bubblechart') 
		{  
			var charttype = "bubble";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-11 column paddingzero"><div class="large-1 columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><span class="material-icons graphremoveicon removecharts myhandle" title="Remove">close</span><div class="graphstyletop" data-chartcount="'+bcount+'" data-charttypedata="'+charttype+'" ><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',chartcreated'+bcount+',Chart Title,vertical,circle" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"><div id="chartcreated'+bcount+'" class="graph400x300"></div></div></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						var bubblechartarray = ['chartcreated'+bcount+'','(Demo)Bubble Chart','vertical','circle'];
						bubblechartfun(bubblechartarray);
					});
					bcount++;
					//For get and Set value
					valuesettingfunction();
					//For Remove Charts
					$('.removecharts').click(function(){
						var toremoveblock = $(this).parent('div').parent('div').attr('id');
						$("#"+toremoveblock+"").removeClass('toblockmearge');
						$(this).parent('div').remove();
						$('#chartsettings').hide();
					});
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		} 
		else if(data == 'wordcloud') {
				var charttype = "wordcloud";
				$(".forgetid").mouseenter(function(){
					var currentblockid = $(this).attr('id'); 
					var rowid = $('#'+currentblockid+'').data('rownum');
					if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-11 column paddingzero"><div class="large-1 columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><span class="material-icons graphremoveicon removecharts myhandle" title="Remove">close</span><div class="graphstyletop" data-chartcount="'+bcount+'" data-charttypedata="'+charttype+'" ><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',chartcreated'+bcount+',Chart Title,spiral" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"><div id="chartcreated'+bcount+'" class="graph400x300"></div></div></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							var wordcloudarray = ['chartcreated'+bcount+'','(Demo)Wordcloud','wordcloud','spiral'];
							wordcloudfun(wordcloudarray);
						});
						bcount++;
						//For get and Set value
						valuesettingfunction();
						//For Remove Charts
						$('.removecharts').click(function(){
							var toremoveblock = $(this).parent('div').parent('div').attr('id');
							$("#"+toremoveblock+"").removeClass('toblockmearge');
							$(this).parent('div').remove();
							$('#chartsettings').hide();
						});
						 // dashboard container  sorting - without grid or editor
						var dbinnercontainer = document.getElementById("ulid"+rowcount+"");
						new Sortable(dbinnercontainer);
						Sortable.create(dbinnercontainer, {
							animation: 150,
							group: 'elementsgrp',
							draggable: '.myid',
						}); 
					} else {
						alertpopup('Please Drop In Another Block');
						$(".forgetid").unbind();
					}
				});
		}
		else if(data == 'areachart') {
				var charttype = "area";
				$(".forgetid").mouseenter(function(){
					var currentblockid = $(this).attr('id'); 
					var rowid = $('#'+currentblockid+'').data('rownum');
					if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-11 column paddingzero"><div class="large-1 columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><span class="material-icons graphremoveicon removecharts myhandle" title="Remove">close</span><div class="graphstyletop" data-chartcount="'+bcount+'" data-charttypedata="'+charttype+'" ><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',chartcreated'+bcount+',Chart Title,false,horizontal,segmented" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"><div id="chartcreated'+bcount+'" class="graph400x300"></div></div></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							var areachartarray = ['chartcreated'+bcount+'','(Demo)Area Chart','horizontal'];
							areachartfun(areachartarray);
						});
						bcount++;
						//For get and Set value
						valuesettingfunction();
						//For Remove Charts
						$('.removecharts').click(function(){
							var toremoveblock = $(this).parent('div').parent('div').attr('id');
							$("#"+toremoveblock+"").removeClass('toblockmearge');
							$(this).parent('div').remove();
							$('#chartsettings').hide();
						});
						 // dashboard container  sorting - without grid or editor
						var dbinnercontainer = document.getElementById("ulid"+rowcount+"");
						new Sortable(dbinnercontainer);
						Sortable.create(dbinnercontainer, {
							animation: 150,
							group: 'elementsgrp',
							draggable: '.myid', 
						}); 
					} else {
						alertpopup('Please Drop In Another Block');
						$(".forgetid").unbind();
					}
				});
		} 
		else if(data == 'linechart') {
				var charttype = "line";
				$(".forgetid").mouseenter(function(){
					var currentblockid = $(this).attr('id'); 
					var rowid = $('#'+currentblockid+'').data('rownum');
					if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-11 column paddingzero"><div class="large-1 columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><span class="material-icons graphremoveicon removecharts myhandle" title="Remove">close</span><div class="graphstyletop" data-chartcount="'+bcount+'" data-charttypedata="'+charttype+'" ><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',chartcreated'+bcount+',Chart Title,false,horizontal,segmented,false,circle" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"><div id="chartcreated'+bcount+'" class="graph400x300"></div></div></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							var linechartarray = ['chartcreated'+bcount+'','(Demo)Line Chart','horizontal'];
							linechartfun(linechartarray);
						});
						bcount++;
						//For get and Set value
						valuesettingfunction();
						//For Remove Charts
						$('.removecharts').click(function(){
							var toremoveblock = $(this).parent('div').parent('div').attr('id');
							$("#"+toremoveblock+"").removeClass('toblockmearge');
							$(this).parent('div').remove();
							$('#chartsettings').hide();
						});
						 // dashboard container  sorting - without grid or editor
						var dbinnercontainer = document.getElementById("ulid"+rowcount+"");
						new Sortable(dbinnercontainer);
						Sortable.create(dbinnercontainer, {
							animation: 150,
							group: 'elementsgrp',
							draggable: '.myid',
						}); 
					} else {
						alertpopup('Please Drop In Another Block');
						$(".forgetid").unbind();
					}
				});
			} 		
		else if(data == 'ibtaskwidget') {
			var charttype = "ibtaskwidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="taskwidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',crmtask,crmtask.crmtaskid|crmtaskname|taskstartdate|crmstatusid|crmtask.priorityid|priorityname|employeename,priority|employee,priorityid|employeeid,4,Task" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						taskwidgetcreation('Task',2,'taskwidgetid','taskwidgetclass','closetaskwidget',rowid);
					});
					$(".predefinedwidget#ibtaskwidget").hide();
					bcount++;
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		} else if(data == 'ibtaskwidgetmini') {
			var charttype = "ibtaskwidgetmini";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				//var rowid = $('#'+currentblockid+'').data('rownum');
				var rowid = 0;
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="taskwidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',crmtask,crmtask.crmtaskid|crmtaskname|taskstartdate|crmstatusid|crmtask.priorityid|priorityname|employeename,priority|employee,priorityid|employeeid,4,Task" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						taskwidgetcreation('Task',2,'taskwidgetid','taskwidgetclass','closetaskwidget',rowid);
					});
					$(".predefinedwidget#ibtaskwidgetmini").hide();
					bcount++;
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}
		else if(data == 'ibdocumentswidget') {
			var charttype = "ibdocumentswidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id'); 
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="documentswidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',crmfileinfo,crmfileinfo.crmfileinfoid|filename|modulename|employeename,module|employee,moduleid|employeeid,4,Documents" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						documentswidgetcreation('Documents',2,'documentswidgetid','documentswidgetclass','closedocumentswidget',rowid);
					});
					$(".predefinedwidget#ibdocumentswidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}else if(data == 'ibdocumentswidgetmini') {
			var charttype = "ibdocumentswidgetmini";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id'); 
				var rowid = 0;
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="documentswidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',crmfileinfo,crmfileinfo.crmfileinfoid|filename|modulename|employeename,module|employee,moduleid|employeeid,4,Documents" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						documentswidgetcreation('Documents',2,'documentswidgetid','documentswidgetclass','closedocumentswidget',rowid);
					});
					$(".predefinedwidget#ibdocumentswidgetmini").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}
		else if(data == 'ibactivitieswidget') {
			var charttype = "ibactivitieswidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="activitieswidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',crmactivity,crmactivity.crmactivityid|crmactivityname|activitystartdate|activitystarttime|employeename|location,employee,employeeid,4,Activities" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						activitieswidgetcreation('Activities',2,'activitieswidgetid','activitieswidgetclass','closeactivitieswidget',rowid);
					});
					$(".predefinedwidget#ibactivitieswidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		} else if(data == 'ibnotificationwidget') {  
			var charttype = "ibnotificationwidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="notificationwidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',Notification" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						notificationswidgetcreation('Notification',2,'notificationwidgetid','notificationwidgetclass','closenotificationwidget',rowid);
					});
					$(".predefinedwidget#ibnotificationwidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}else if(data == 'ibnotificationwidgetmini') {  
			var charttype = "ibnotificationwidgetmini";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				//var rowid = $('#'+currentblockid+'').data('rownum');
				var rowid = 0;
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="notificationwidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',Notification" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						notificationswidgetcreation('Notification',2,'notificationwidgetid','notificationwidgetclass','closenotificationwidget',rowid);
					});
					$(".predefinedwidget#ibnotificationwidgetmini").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}else if(data == 'ibmaillogwidget') {
			var charttype = "ibmaillogwidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="maillogwidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',crmmaillog,crmmaillog.crmmaillogid|communicationto|communicationdate,,,crmmaillog.subject,3,4,Mail Log" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						maillogwidgetcreation('Mail Log',2,'maillogwidgetid','maillogwidgetclass','closemaillogwidget',rowid);
					});
					$(".predefinedwidget#ibmaillogwidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		} else if(data == 'ibsmslogwidget') {  
			var charttype = "ibsmslogwidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="smslogwidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',crmcommunicationlog,crmcommunicationlog.crmcommunicationlogid|communicationto|communicationdate,,,crmcommunicationlog.crmcommunicationmode,2,4,SMS Log" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						smslogwidgetcreation('SMS Log',2,'smslogwidgetid','smslogwidgetclass','closesmslogwidget',rowid);
					});
					$(".predefinedwidget#ibsmslogwidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		} else if(data == 'ibreceivesmslogwidget') {  
			var charttype = "ibreceivesmslogwidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="receivesmslogwidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',crmcommunicationlog,crmcommunicationlog.crmcommunicationlogid|communicationto|communicationdate,,,crmcommunicationlog.crmcommunicationmode,3,4,Receive SMS Log" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						receivesmslogwidgetcreation('Receive SMS Log',2,'receivesmslogwidgetid','receivesmslogwidgetclass','receiveclosesmslogwidget',rowid);
					});
					$(".predefinedwidget#ibreceivesmslogwidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}
		else if(data == 'ibsendsmslogwidget') { 
			var charttype = "ibsendsmslogwidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0" id="sendsmslogwidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',Send SMS" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						sendsmslogwidgetcreation('Send SMS',2,'sendsmslogwidgetid','smslogwidgetclass','closesmslogwidget',rowid);
					});
					$(".predefinedwidget#ibsendsmslogwidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}  else if(data == 'ibclicktocallwidget') { 
			var charttype = "ibclicktocallwidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0" id="clicktocalllogwidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',Click To call" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						clicktocallwidgetcreation('Click To call',2,'clicktocalllogwidgetid','clicktocallwidgetclass','closeclicktocallwidget',rowid);
					});
					$(".predefinedwidget#ibclicktocallwidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}  else if(data == 'ibinboundcalllogwidget') { 
			var charttype = "ibinboundcalllogwidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="inboundcalllogwidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',calldetails,calldetails.calldetailsid|caller|tonumber|location|provider,,,4,Inbound Call Log" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						inboundcalllogwidgetcreation('Inbound Call Log',2,'inboundcalllogwidgetid','incalllogwidgetclass','inboundclosecalllogwidget',rowid);
					});
					$(".predefinedwidget#ibinboundcalllogwidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}
		else if(data == 'iboutboundcalllogwidget') { 
			var charttype = "iboutboundcalllogwidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="outboundcalllogwidget" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',callcampaign,callcampaign.callcampaignid|callcampaignname|callernumber|mobilenumber|smssendtypeid,,,4,Outbound Call Log" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						outboundcalllogwidgetcreation('Outbound Call Log',2,'outboundcalllogwidget','outcalllogwidgetclass','outboundclosecalllogwidget',rowid);
					});
					$(".predefinedwidget#iboutboundcalllogwidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}
		else if(data == 'ibopportunitywidget') {  
			var charttype = "ibopportunitywidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0" id="opportunitywidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',opportunity,opportunity.opportunityid|opportunityname|amount|expectedclosedate,,,4,Opportunity" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						opportunitywidgetcreation('Opportunity',2,'opportunitywidgetid','opportunitywidgetclass','closeopportunitywidget',rowid);
					});
					$(".predefinedwidget#ibopportunitywidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		} else if(data == 'ibcontactswidget') {  
			var charttype = "ibcontactswidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="contactswidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',contact,contact.contactid|contactname|designationtypename|mobilenumber|emailid,designationtype,designationtypeid,4,Contacts" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						contactswidgetcreation('Contacts',2,'contactswidgetid','contactswidgetclass','closecontactswidget',rowid);
					});
					$(".predefinedwidget#ibcontactswidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		} else if(data == 'ibpaymentwidget') {  
				var charttype = "ibpaymentwidget";
				$(".forgetid").mouseenter(function(){
					var currentblockid = $(this).attr('id');
					var rowid = $('#'+currentblockid+'').data('rownum');
					if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="paymentwidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',payment,payment.paymentamount|paymentdate|paymentid|paymenttype.paymenttypename|paymentmethod.paymentmethodname,employee,employeeid,4,Payment" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							paymentwidgetcreation('Payment',2,'paymentwidgetid','paymentwidgetclass','closepaymentwidget',rowid);
						});
						$(".predefinedwidget#ibpaymentwidget").hide();
						bcount++;		
						//For get and Set value
						valuesettingfunction();
					} else {
						alertpopup('Please Drop In Another Block');
						$(".forgetid").unbind();
					}
				});
			} else if(data == 'ibinvoicewidget') {  
			var charttype = "ibinvoicewidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="invoicewidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',invoice,invoice.invoiceid|invoicenumber|totalnetamount|invoicedate|employeename,employee,employeeid,4,Invoice" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						invoicewidgetcreation('Invoice',2,'invoicewidgetid','invoicewidgetclass','closeinvoicewidget',rowid);
					});
					$(".predefinedwidget#ibinvoicewidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		} else if(data == 'ibquotewidget') {  
			var charttype = "ibquotewidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="quotewidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',quote,quote.quoteid|quotenumber|totalnetamount|quotedate|employeename,employee,employeeid,4,Quote" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						quotewidgetcreation('Quote',2,'quotewidgetid','quotewidgetclass','closequotewidget',rowid);
					});
					$(".predefinedwidget#ibquotewidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		} else if(data == 'ibleadstatuswidget') {  
			var charttype = "ibleadstatuswidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding-top:0.4rem 0 0 0;" id="leadstatuswidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',Lead Status" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						leadstatuswidgetcreation('Lead Status',2,'leadstatuswidgetid','leadstatuswidgetclass','closeleadstatuswidget',rowid);
					});
					$(".predefinedwidget#ibleadstatuswidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}
		else if(data == 'ibaccountswidget') 
		{  
			var charttype = "ibaccountswidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="accountswidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',account,account.accountid|account.accountname|employee.employeename|source.sourcename,employee|source,employeeid|sourceid,4,Accounts" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						accountswidgetcreation('Accounts',2,'accountswidgetid','accountswidgetclass','closeaccountswidget',rowid);
					});
					$(".predefinedwidget#ibaccountswidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}
			else if(data == 'ibticketswidget') {  
			var charttype = "ibticketswidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="ticketswidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',ticket,ticketid|ticketnumber|ticketdate|employeename,employee,employeeid,4,Ticktes" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						ticketswidgetcreation('Ticktes',2,'ticketswidgetid','ticketswidgetclass','closeticketswidget',rowid);
					});
					$(".predefinedwidget#ibticketswidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}
		else if(data == 'ibcampaignwidget') {  
			var charttype = "ibcampaignwidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="campaignwidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',campaign,campaignid|campaignname|startdate|employeename,employee,employeeid,4,Campaign" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						campaignwidgetcreation('Campaign',2,'campaignwidgetid','campaignwidgetclass','closecampaignwidget',rowid);
					});
					$(".predefinedwidget#ibcampaignwidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}
		else if(data == 'ibsalesorderwidget') {  
			var charttype = "ibsalesorderwidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="salesorderwidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',salesorder,salesorder.salesorderid|salesordernumber|salesorderdate|employeename|totalnetamount,employee,employeeid,4,Sales Order" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						salesorderwidgetcreation('Sales Order',2,'salesorderwidgetid','salesorderwidgetclass','closesalesorderwidget',rowid);
					});
					$(".predefinedwidget#ibsalesorderwidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}
		else if(data == 'ibpurchaseorderwidget') {  
			var charttype = "ibpurchaseorderwidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="purchaseorderwidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',purchaseorder,purchaseorder.purchaseorderid|purchaseordernumber|purchaseorderdate|employeename|totalnetamount,employee,employeeid,4,Purchase Order" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						purchaseorderwidgetcreation('Purchase Order',2,'purchaseorderwidgetid','purchaseorderwidgetclass','closepurchaseorderwidget',rowid);
					});
					$(".predefinedwidget#ibpurchaseorderwidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}
		else if(data == 'ibsolutionwidget') {  
			var charttype = "ibsolutionwidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="solutionswidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',knowledgebase,knowledgebaseid|knowledgebasenumber|knowledgebasedate|crmstatusname,crmstatus,crmstatusid,4,Solutions" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						solutionswidgetcreation('Solutions',2,'solutionswidgetid','solutionswidgetclass','closesolutionswidget',rowid);
					});
					$(".predefinedwidget#ibsolutionwidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}
		else if(data == 'ibsolutioncatwidget') {  
			var charttype = "ibsolutioncatwidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="solutionscatwidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',knowledgebasecategory,knowledgebasecategoryid|knowledgebasecategoryname|knowledgebasecategorysuffix,,,4,Solutions Category" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						solutionscatwidgetcreation('Solutions Category',2,'solutionscatwidgetid','solutionscatwidgetclass','closesolutionscatwidget',rowid);
					});
					$(".predefinedwidget#ibsolutioncatwidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}
		else if(data == 'ibcontractwidget') {  
			var charttype = "ibcontractwidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="contractwidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',contract,contractid|contractnumber|employeename|contractdate,employee,employeeid,4,Contract" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						contractwidgetcreation('Contract',2,'contractwidgetid','contractwidgetclass','closecontractwidget',rowid);
					});
					$(".predefinedwidget#ibcontractwidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}
		else if(data == 'ibleadwidget') {  
			var charttype = "ibleadwidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="leadwidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',lead,leadid|leadname|employeename|leadnumber,employee,employeeid,4,Lead" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						leadwidgetcreation('Lead',2,'leadwidgetid','leadwidgetclass','closeleadwidget',rowid);
					});
					$(".predefinedwidget#ibleadwidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}
		else if(data == 'ibtimeanddatewidget') {  
			var charttype = "ibtimeanddatewidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="timeanddatewidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',Time and Date" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						timeanddatewidgetcreation('Time and Date',1,'timeanddatewidgetid','timeanddatewidgetclass','closetimeanddatewidget',rowid);
					});
					$(".predefinedwidget#ibtimeanddatewidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}
		else if(data == 'ibcalendarwidget') {  
			var charttype = "ibcalendarwidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0" id="calendarwidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',Calendar" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						calendarwidgetcreation('Calendar',1,'calendarwidgetid','calendarwidgetclass','closecalendarwidget',rowid);
					});
					$(".predefinedwidget#ibcalendarwidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}
		else if(data == 'ibconversationwidget') {  
			var charttype = "ibconversationwidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="conversationwidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',Conversation" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						conversationwidgetcreation('Conversation',1,'conversationwidgetid','conversationwidgetclass','closeconversationwidget',rowid);
					});
					$(".predefinedwidget#ibconversationwidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}
		else if(data == 'ibmaterialrequisitionwidget') {  
			var charttype = "ibmaterialrequisitionwidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="materialrequisitionid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',materialrequisition,materialrequisition.materialrequisitionid|materialrequisition.netamount|materialrequisitionnumber|requisitiondate,materialrequisition,materialrequisitionid,4,Material Requisition" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						materialrequisitionwidgetcreation('Material Requisition',2,'materialrequisitionid','materialrequisitionwidgetclass','closematerialrequisitionwidget',rowid);
					});
					$(".predefinedwidget#ibmaterialrequisitionwidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}
		else if(data == 'ibproductwidget') {  
			var charttype = "ibproductwidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="productwidgetid"  data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',product,productid|productname|categoryname,category,categoryid,4,Product" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						productwidgetcreation('Product',2,'productwidgetid','productwidgetclass','closeproductwidget',rowid);
					});
					$(".predefinedwidget#ibproductwidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
		}
		else if(data == 'ibpricebookwidget') {  
			var charttype = "ibpricebookwidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="pricebookwidgetid" data-rowtypeid="'+rowid+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',pricebook,pricebook.pricebookid|pricebookname|currencyname,currency,currencyid,4,Pricebook" id="chartnameinfo'+bcount+'" data-chartrowid="'+rowid+'"></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						pricebookwidgetcreation('Pricebook',2,'pricebookwidgetid','pricebookwidgetclass','closepricebookwidget',rowid);
					});
					$(".predefinedwidget#ibpricebookwidget").hide();
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
	    }
		else if(data == 'ibcustomwidget'){  
			var charttype = "ibcustomwidget";
			$(".forgetid").mouseenter(function(){
				var currentblockid = $(this).attr('id');
				var rowid = $('#'+currentblockid+'').data('rownum');
				if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {  	
					$("#"+currentblockid+"").addClass('toblockmearge');
					$("#"+currentblockid+"").append('<div class="large-12 columns end customwidgetclassclick" style="padding:0.4rem 0 0 0;" id="customwidgetid'+bcount+'" data-chartcount="'+bcount+'"><input name="chartnameinfo[]" type="hidden" value="" id="chartnameinfo'+bcount+'"  data-chartcount="'+bcount+'" data-chartrowid="'+rowid+'" data-customrelatedmodules="" data-customdataset="" data-customwidgetlabel=""></div>');
					$(".forgetid").unbind();
					$(document).ready(function(){
						customwidgetcreation('Custom','customwidgetid','customwidgetclass','closecustomwidget',rowid,bcount);
					});
					bcount++;		
					//For get and Set value
					valuesettingfunction();
				} else {
					alertpopup('Please Drop In Another Block');
					$(".forgetid").unbind();
				}
			});
	    }
		return false;
	}
	{//Legend show hide
		$('#legendtype').change(function(){
			if ($('#legendtype').is(':checked')) {
				$('#legendtype').val('true');
				$('#showifchecked').show();
			} else {
				$('#legendtype').val('false');
				$('#showifchecked').hide();
			}
		});
		//rotate
		$('#rotatechart').change(function(){
			if ($('#rotatechart').is(':checked')) {
				$('#rotatechart').val('true');
			} else {
				$('#rotatechart').val('false');
			}
		});	
		$('#selectmode').change(function(){
			var charttypedetail = $('#chatytype').val();
			if(charttypedetail == 'area'){	
				var mode = $('#selectmode').val();
				if(mode == 'vertical'){
				$('#chartview').attr('checked', false);
				$('#attrchartview').hide();
				}
				else{
					$('#attrchartview').show();
				}
			}
			if(charttypedetail == 'line'){
				var mode = $('#selectmode').val();
				if(mode == 'vertical'){
				$('#chartview').attr('checked', false);
				$('#attrchartview').hide();
				}
				else{
					$('#attrchartview').show();
				}
			}
		});	
		//3D chart
		$('#chartview').change(function(){
			if ($('#chartview').is(':checked')) {
				$('#chartview').val('true');
				var charttypedetail = $('#chatytype').val();
				if(charttypedetail == 'area'){					 
					 $("#selectmode").attr('readonly','readonly');
				}
				if(charttypedetail == 'line'){					
					 $("#selectmode").attr('readonly','readonly');
				}				
			} else {
				$('#chartview').val('false');
				var charttypedetail = $('#chatytype').val();
				if(charttypedetail == 'area'){					
					$("#selectmode").removeAttr('readonly');
				}
				if(charttypedetail == 'line'){					
					$("#selectmode").removeAttr('readonly');
				}
			}
		});	
	}
	{//custom widget data settings
		$("#customdatafields").change(function() {
			var data = $('#customdatafields').select2('data');
			var finalResult = [];
			for( item in $('#customdatafields').select2('data')) {
				finalResult.push(data[item].id);
			};
			var selectid = finalResult.join(',');
			$("#relatedcolumnid").val(selectid);
		});
		//custom widget submit
		$("#customwidgetsubmit").click(function(){
			$("#customwidgetvalidation").validationEngine('validate');
		});
		jQuery("#customwidgetvalidation").validationEngine({
			onSuccess: function() {
				//show/hide
				$('#chartsettings').hide();
				$('#customwidgetsettings').hide();
				$('#commonwidget').hide();
				//organize related fields
				var custommoduledatatype = [];		
				//organize selected fields 
				var customcolumns = [];
				var formfieldsidhidden = [];
				//var customrelatedmodule = [];
				var customrelatedmodule = '';
				var mc=0;
				var datafiled= $("#relatedcolumnid").val();
				var moduleid =$('#modulename').val();
				// data fields drop down data fetch
				$.ajax({
					url: base_url + "Dashboardbuilder/datafielddatafetch",
					data: "datafiled="+datafiled+"&moduleid="+moduleid,
					type: "POST",
					async:false,
					cache:false,
					success: function(data) {					
						customcolumns = data;
						customcolumns = customcolumns.replace(/,/g, "|");
					},
				}); 
				var customrelatedmodule = $('#customrelatedmodule').val();		
				formfieldsidshidden = formfieldsidhidden.toString();
				var chartnum = $("#currentchartnum").val();
				var rowid = $('#chartnameinfo'+chartnum+'').data('chartrowid');
				var chartcount = $('#chartnameinfo'+chartnum+'').data('chartcount');
				var relatedmoduleids = $('#customdatafields').val();
				var relatedmoduleids = '';
				var customrelatedmodule = $('#customrelatedmodule').val();
				var customrelatedmodule = '';
				var widgetlabel = $('#customwidgetname').val();
				if(widgetlabel == ''){
					widgetlabel='Custom Widget';
				}		
				var charttype='ibcustomwidget';
				var parenttablename=$("#modulename").find('option:selected').data('parenttable');
				var customchartinfo = rowid+','+charttype+','+widgetlabel+','+parenttablename+','+customcolumns+','+customrelatedmodule+','+formfieldsidshidden;
				var chartid = $("#currentchartnum").val();
				$('#chartnameinfo'+chartcount+'').val(customchartinfo);
				$('#customwidgetname').val('');
				$('#customrelatedmodule,#customdatafields').select2('val','');
			},
			onFailure: function() {		
				alertpopup(validationalert);
			}
		});
		//custom widget submit
		$("#commonwidgetsubmit").click(function(){
			var commonwidgetname= $("#commonwidgetname").val();
			if(commonwidgetname != '') {
				$("#commonwidgetvalidation").validationEngine('validate');
			} else {
				alertpopup('Please enter the widget name');
			}
		});
		jQuery("#commonwidgetvalidation").validationEngine({
			onSuccess: function() {
				var commonwidgetname= $("#commonwidgetname").val();
				var chartid = $("#chartid").val();
				$("#widgettitle"+chartid+"").text(commonwidgetname);
				var chartdata = $("#chartnameinfo"+chartid+"").val();
				var data = chartdata.split(',');
				var length = data.length-1;
				data[length] = commonwidgetname;
				$("#chartnameinfo"+chartid+"").val(data);
				$("#commonwidgetname").val('');
				$("#commonwidget").hide();
			},
			onFailure: function() {
				alertpopup(validationalert);
			}
		});
	}
	{// To Change Attributes
	$("#createchart").click(function(){	
		var charttype=$('#chatytype').val();
		var groupby=$('#reportgroupbyidtext').val();
		var splitby=$('#reportsplitbyidtext').val();	
		if(charttype == 'stacked' || charttype == 'bubble'){
			if(checkValue(groupby) == true && checkValue(splitby) == true){
				if(groupby == splitby){
					alertpopup('Enter Different GroupBy-SplitBy Values');
				}
				else{
					$("#individualchartvalidation").validationEngine('validate');
				}				
			}else{
				alertpopup('Enter Grouping/Split Values');
			} 
		}
		else if (charttype == 'cylinder3d' || charttype == 'angulargauge' || charttype == 'column'|| charttype == 'pie' ||charttype == 'donut' || charttype == 'funnel' || charttype == 'pyramid' || charttype == 'wordcloud' || charttype == 'area' || charttype == 'line')
		{
			if(checkValue(groupby) == true){
				$("#individualchartvalidation").validationEngine('validate');				
			}
			else{
				alertpopup('Enter GroupBy');
			}
		}	
	});
    jQuery("#individualchartvalidation").validationEngine({
		onSuccess: function() {
			//show/hide
			$('#chartsettings').hide();
			$('#customwidgetsettings').hide();
			var chartviewval = $("#chartview").val();
			var titleval = $("#charttitle").val();
			var angelval =  parseInt($("#anglevalue").val());
			var stacktype = $("#stacktype").val();
			var chartmodeval = $("#selectmode").val();
			var chartaspect = $("#chartaspect").val();
			var legendstyleval = $("#legendstyle").val();
			var legendval = $('#legendtype').val();
			var charttypevalue = $("#chatytype").val();
			var chartnum = $("#currentchartnum").val();
			chartelementsdataorganize(chartnum);
			switch (charttypevalue) { 
				case 'pie': 
					var piechartarray = ['chartcreated'+chartnum+'',titleval,chartviewval,angelval,legendval,legendstyleval];
					piechartfun(piechartarray);
					clearform('clearchartform');
					break;
				case 'donut': 
					var donutchartarray = ['chartcreated'+chartnum+'',titleval,chartviewval,angelval,legendval,legendstyleval];
					donutchartfun(donutchartarray);
					clearform('clearchartform');
					break;
				case 'funnel': 
					var funnelchartarray = ['chartcreated'+chartnum+'',titleval,chartmodeval,legendval,legendstyleval];
					funnelchartfun(funnelchartarray);
					clearform('clearchartform');
					break;
				case 'pyramid': 
					var pyramidchartarray = ['chartcreated'+chartnum+'',titleval,chartmodeval,legendval,legendstyleval];
					pyramidchartfun(pyramidchartarray);
					clearform('clearchartform');
					break;
				case 'column':
					var columnchartarray = ['chartcreated'+chartnum+'',titleval,chartmodeval,chartviewval,chartaspect,legendval,legendstyleval];
					columnchartfun(columnchartarray);
					clearform('clearchartform');
					break;				
				case 'stacked':
					var stackedchartarray = ['chartcreated'+chartnum+'',titleval,chartmodeval,stacktype];
					stackedchartfun(stackedchartarray);
					clearform('clearchartform');
					break;
				case 'angulargauge':
					var angulargaugechartarray = ['chartcreated'+chartnum+'',titleval,'gauge',legendval,legendstyleval];
					angulargaguechartfun(angulargaugechartarray);
					clearform('clearchartform');
					break;
				case 'wordcloud': 
					var wordcloudarray = ['chartcreated'+chartnum+'',titleval,'wordcloud',chartaspect];
					wordcloudfun(wordcloudarray);
					clearform('clearchartform');
					break;		
				case 'bubble':
					var bubblechartarray = ['chartcreated'+chartnum+'',titleval,chartmodeval,chartaspect];
					bubblechartfun(bubblechartarray);
					clearform('clearchartform');
					break;
				case 'area':
					var areachartarray = ['chartcreated'+chartnum+'',titleval,chartmodeval,chartviewval,chartaspect];
					areachartfun(areachartarray);
					clearform('clearchartform');
					break;
				case 'line':
					var linechartarray = ['chartcreated'+chartnum+'',titleval,chartmodeval,chartviewval,chartaspect];
					linechartfun(linechartarray);
					clearform('clearchartform');
					break;				
				default:
					alert('Nobody');
					clearform('clearchartform');
					break;
			}
		},
		onFailure: function() {		
		alertpopup(validationalert);
		}
	});		
	}
	{
		$('#dbsetdefault,#dbsetactive').click(function(){
			var id = $(this).attr('id');
			if ( $('#'+id+'').prop('checked') == true ) {
				$('#'+id+'').val('Yes');
				$('#'+id+'hidden').val('Yes');
			} else {
				$('#'+id+'').val('No');
				$('#'+id+'hidden').val('No');
			}
		});
		//dashboard save
		$('#dbdatasavebtn').click(function(){
			$('#settingstabid').trigger('click');
			$('#normalviewpreview').trigger('click');
			$("#dashboarddatainfoformvalidate").validationEngine('validate');
		});
		$("#dashboarddatainfoformvalidate").validationEngine({
			onSuccess: function() {
				$("#processoverlay").show();
				dashboarddatacreate();
				//show/hide
				$('#chartsettings').hide();
				$('#customwidgetsettings').hide();
			},
			onFailure: function() {
				var dropdownid =['2','modulename','dbfoldername'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//dashboard save
		$('#dbdataupdateicon').click(function(){
			$('#settingstabid').trigger('click');
			$('#normalviewpreview').trigger('click');
			$("#dashboardudatedatainfoformvalidate").validationEngine('validate');
		});
		$("#dashboardudatedatainfoformvalidate").validationEngine({
			onSuccess: function() {
				dashboarddataupdate();
				//show/hide
				$('#chartsettings').hide();
				$('#customwidgetsettings').hide();
			},
			onFailure: function() {
				var dropdownid =['2','modulename','dbfoldername'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//chart data events- 
		$("#reportid").change(function(){
			var val=$(this).val();
			resetchartparam(val);
		});
		$("#customrelatedmodule").change(function(){
			if($("#customrelatedmodule").val() != '' || $("#customrelatedmodule").val() != null){
				customwidgetdatafield();
			}
		});
		$("#modulename").change(function(){ 
			var val=$(this).find('option:selected').val();
			if(val == '1'){ // mini dashboard checkbox hide show
				$('#minidashboardcheckspan,#miniwidgetcountspan').hide();
			}else{
				$('#minidashboardcheckspan').show();
				if ( $('#dbsetmini').prop('checked') == true ) {
					$('#miniwidgetcountspan').show();
				}else{
					$('#miniwidgetcountspan').hide();
				}
			}
			var modulename = $("#modulename option:selected").text();
			$('#currentmodulename').val(modulename);
			//resetsourcereport(val);
			if(val == null){	
				$("#blockspan").show();
			}else{
				$("#blockspan").hide();
			}
			//load dataset
			if(val > 0){
				$( "#spanforaddtab" ).empty();
				$('#existingchart').empty();
				$.ajax({
					url: base_url + "Dashboardbuilder/dashboardwidgetmenu",
					data: "moduleid="+val,
					type: "POST",
					async:false,
					cache:false,
					success: function(data) {					
						$('#existingchart').append(data);
						setTimeout(function(){
							accordian();
							resetdrageventlistener(val); //reset the draggable js events
						},1000);
						
					},
				});				
				$('#customrelatedmodule').empty();
				$.ajax({
					url: base_url + "Dashboardbuilder/relatedmodulelistfetchmodel",
					data: "moduleid="+val,
					dataType:'json',
					type: "POST",
					async:false,
					cache:false,
					success: function(data) {					
						var newddappend="";
						var newlength=data.length;
						for(var m=0;m < newlength;m++){
							newddappend += "<option value ='"+data[m]['datasid']+"' data-mergemoduleidhidden='"+data[m]['datasid']+"' data-mergemoduletype='"+data[m]['datatype']+"'>"+data[m]['dataname']+ " </option>";
						}
						//after run
						$('#customrelatedmodule').append(newddappend);
					},
				});
				//retrieve custom data fields
				customwidgetdatafield();							
			}
		});
		$('#recordcount').prop('checked', true);
		$('#recordcount').change(function(){
			if ($('#recordcount').is(':checked')) {
				$('#recordcount').val('true');	
				$('#aggregatefieldid,#operation').select2('val','').trigger('change');
				$('.star').text('');
				$('#aggregatefieldid,#operation').removeClass('validate[required]');
				$('#datafield,#operationlabel').hide();
			} else {
			}
		});
	}
	$("#dbdataclearicon").click(function() {
		window.location=base_url+'Dashboardbuilder';	
	});
	{//Mass update/ Mass Delete /Export Conversion From All modules
		var dashboardid = sessionStorage.getItem("dashboardid"); 
		if(dashboardid != null){
			setTimeout(function(){
				$("#dashboardid").val(dashboardid);
				dashboardeditdatafetch(dashboardid);
				$("#dbdatasavebtn").hide();
				$("#dbdataupdateicon").show();
				sessionStorage.removeItem("dashboardid");
				setTimeout(function(){
					Materialize.updateTextFields();
				},1000);
			},100);		
		}
	}
	$('#dbsetmini').click(function(){
			var id = $(this).attr('id');
			if ( $('#'+id+'').prop('checked') == true ) {
				$('#'+id+'').val('Yes');
				$('#'+id+'hidden').val('Yes');
				$('#miniwidget').removeClass('hidedisplay');
				$('#fullwidget').addClass('hidedisplay');
				$('#miniwidgetcountspan').show();
				$("#miniwidgetcount").addClass('validate[custom[number],required]');
			} else {
				$('#'+id+'').val('No');
				$('#'+id+'hidden').val('No');
				$('#miniwidget').addClass('hidedisplay');
				$('#fullwidget').removeClass('hidedisplay');
				$('#miniwidgetcountspan').hide();
				$("#miniwidgetcount").removeClass('validate[custom[number],required]');
			}
		});
});
//dashboard unique name check
function dashboardnamecheck() {
	var primaryid = $("#dashboardid").val();
	var accname = $("#dashboardname").val();
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname=dashboard&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Dashboard Name already exists!";
				}
			} else {
				return "Dashboard Name already exists!";
			}
		} 
	}
}
//confirmation alert popup
function dashboardcloseconfirmalertmsg(idname,msg) {
	$("#basedeleteoverlayforcommodule").show();
	$(".basedelalerttxt").text(msg);
	$(".commodyescls").attr('id',idname);
	$(".commodyescls").focus();
}
{// Get and Set values
	function valuesettingfunction() {
		$(".graphstyletop").click(function(){
			//show/hide
			$('#chartsettings').show();
			$('#customwidgetsettings').hide();
			$("#commonwidget").hide();
			// For Set and Get the value based on chart 
			var chartid = $(this).data('chartcount');
			var getcharttype = $(this).data('charttypedata');
			$("#chatytype").val(getcharttype);
			$("#currentchartnum").val(chartid);
			$('#charttitle').focus();
			settingtabenable = 1;
			$("#propertiestabid").trigger('click');
			chartfiledsshowhide(getcharttype);
			//chart aspect function
			chartaspect(getcharttype);
			chartelementsdatareorganize(chartid);
			Materialize.updateTextFields();
		});
		$(".customwidgetclassclick").click(function(){
			//show/hide
			$('#chartsettings').hide();
			$('#customwidgetsettings').show();
			$("#commonwidget").hide();
			// For Set and Get the value based on chart 
			var chartid = $(this).data('chartcount');
			var getcharttype = $(this).data('charttypedata');
			$("#chatytype").val(getcharttype);
			$("#currentchartnum").val(chartid);
			$('#charttitle').focus();
			settingtabenable = 1;
			$("#propertiestabid").trigger('click');
			var info = $('#chartnameinfo'+chartid+'').val();
			var chartinfo = info.split(",");
			if(chartinfo.length > 5) {		
				var label = chartinfo[2];
				$('#customwidgetname').val(label);
			}
		});
		$(".widgetclass").click(function() {
			//show/hide
			$('#chartsettings').hide();
			$('#customwidgetsettings').hide();
			$("#commonwidget").show();
			settingtabenable = 1;
			var chartid = $(this).data('rowtypeid');
			$("#chartid").val(chartid);
			var chartdata = $("#chartnameinfo"+chartid+"").val();
			var data = chartdata.split(',');
			var length = data.length-1;
			$("#commonwidgetname").val(data[length]);
			$("#propertiestabid").trigger('click');
		});
	}
	//set values to properties
	function chartelementsdatareorganize(chartid) {	
		var info = $('#chartnameinfo'+chartid+'').val();
		var chartinfo = info.split(",");
		if(chartinfo[1] == 'pie') {
			$('#chatytype').val(chartinfo[1]);
			$("#charttitle").val(chartinfo[3]);
			var chkboxinfo = {'chartview':chartinfo[4]};
			chkboxvalueset(chkboxinfo);			
			$("#anglevalue").val(chartinfo[5]);
			var chkboxinfo = {'legendtype':chartinfo[6]};
			chkboxvalueset(chkboxinfo);
			$("#legendstyle").select2('val',chartinfo[7]).trigger('change');
			//chart-report-date
			$("#reportid").select2('val',chartinfo[8]).trigger('change');
			setTimeout(function() {
				$("#reportgroupbyid").select2('val',chartinfo[9]).trigger('change');
				$("#aggregatefieldid").select2('val',chartinfo[11]).trigger('change');
			},300);	
			setTimeout(function() {
				$("#operation").select2('val',chartinfo[12]).trigger('change');
			},50);	
			var chkboxinfo = {'recordcount':chartinfo[10]};
			chkboxvalueset(chkboxinfo);
		} 
		else if(chartinfo[1] == 'donut') {
			$('#chatytype').val(chartinfo[1]);
			$("#charttitle").val(chartinfo[3]);
			var chkboxinfo = {'chartview':chartinfo[4]};
			chkboxvalueset(chkboxinfo);			
			$("#anglevalue").val(chartinfo[5]);
			var chkboxinfo = {'legendtype':chartinfo[6]};
			chkboxvalueset(chkboxinfo);
			$("#legendstyle").select2('val',chartinfo[7]).trigger('change');
			//chart-report-date
			$("#reportid").select2('val',chartinfo[8]).trigger('change');
			setTimeout(function() {
				$("#reportgroupbyid").select2('val',chartinfo[9]).trigger('change');
				$("#aggregatefieldid").select2('val',chartinfo[11]).trigger('change');
			},300);	
			setTimeout(function() {
				$("#operation").select2('val',chartinfo[12]).trigger('change');
			},50);	
			var chkboxinfo = {'recordcount':chartinfo[10]};
			chkboxvalueset(chkboxinfo);
		}
		else if(chartinfo[1] == 'funnel') {
			$('#chatytype').val(chartinfo[1]);			
			$("#charttitle").val(chartinfo[3]);
			$("#selectmode").select2('val',chartinfo[4]).trigger('change');
			var chkboxinfo = {'legendtype':chartinfo[5]};
			chkboxvalueset(chkboxinfo);
			$("#legendstyle").select2('val',chartinfo[6]).trigger('change');
			//chart-report-date
			$("#reportid").select2('val',chartinfo[7]).trigger('change');
			setTimeout(function(){			
			$("#reportgroupbyid").select2('val',chartinfo[8]).trigger('change');
			$("#aggregatefieldid").select2('val',chartinfo[10]).trigger('change');
			},300);
			setTimeout(function(){$("#operation").select2('val',chartinfo[11]).trigger('change');},50);
			var chkboxinfo = {'recordcount':chartinfo[9]};
			chkboxvalueset(chkboxinfo);
		}
		else if(chartinfo[1] == 'pyramid') {
			$('#chatytype').val(chartinfo[1]);			
			$("#charttitle").val(chartinfo[3]);
			$("#selectmode").select2('val',chartinfo[4]).trigger('change');
			var chkboxinfo = {'legendtype':chartinfo[5]};
			chkboxvalueset(chkboxinfo);
			$("#legendstyle").select2('val',chartinfo[6]).trigger('change');
			//chart-report-date
			$("#reportid").select2('val',chartinfo[7]).trigger('change');
			setTimeout(function(){			
			$("#reportgroupbyid").select2('val',chartinfo[8]).trigger('change');
			$("#aggregatefieldid").select2('val',chartinfo[10]).trigger('change');
			},300);
			setTimeout(function(){$("#operation").select2('val',chartinfo[11]).trigger('change');},50);
			var chkboxinfo = {'recordcount':chartinfo[9]};
			chkboxvalueset(chkboxinfo);
		} 
		else if(chartinfo[1] == 'column') {
			$('#chatytype').val(chartinfo[1]);
			$("#charttitle").val(chartinfo[3]);
			var chkboxinfo = {'chartview':chartinfo[4]};
			chkboxvalueset(chkboxinfo);
			$("#selectmode").select2('val',chartinfo[5]).trigger('change');
			$("#chartaspect").select2('val',chartinfo[6]).trigger('change');
			
			var chkboxinfo = {'legendtype':chartinfo[7]};
			chkboxvalueset(chkboxinfo);			
			$("#legendstyle").select2('val',chartinfo[8]).trigger('change');			
			//chart-report-date
			$("#reportid").select2('val',chartinfo[9]).trigger('change');
			setTimeout(function(){			
			$("#reportgroupbyid").select2('val',chartinfo[10]).trigger('change');
			$("#aggregatefieldid").select2('val',chartinfo[12]).trigger('change');
			},300);
			setTimeout(function(){$("#operation").select2('val',chartinfo[13]).trigger('change');},50);
			var chkboxinfo = {'recordcount':chartinfo[11]};
			chkboxvalueset(chkboxinfo);
		}
		else if(chartinfo[1] == 'angulargauge') {
			$('#chatytype').val(chartinfo[1]);
			$("#charttitle").val(chartinfo[3]);					
			var chkboxinfo = {'legendtype':chartinfo[4]};
			chkboxvalueset(chkboxinfo);			
			$("#legendstyle").select2('val',chartinfo[5]).trigger('change');
			//chart-report-date
			$("#reportid").select2('val',chartinfo[6]).trigger('change');
			setTimeout(function(){			
			$("#reportgroupbyid").select2('val',chartinfo[7]).trigger('change');			
			$("#aggregatefieldid").select2('val',chartinfo[9]).trigger('change');
			},300);	
			setTimeout(function(){$("#operation").select2('val',chartinfo[10]).trigger('change');	},50);	
			var chkboxinfo = {'recordcount':chartinfo[8]};
			chkboxvalueset(chkboxinfo);
		}
		else if(chartinfo[1] == 'wordcloud') {
			$('#chatytype').val(chartinfo[1]);
			$("#charttitle").val(chartinfo[3]);
			$("#chartaspect").select2('val',chartinfo[4]).trigger('change');			
			//chart-report-date
			$("#reportid").select2('val',chartinfo[5]).trigger('change');
			setTimeout(function(){			
			$("#reportgroupbyid").select2('val',chartinfo[6]).trigger('change');
			},300);			
			var chkboxinfo = {'recordcount':chartinfo[7]};
			chkboxvalueset(chkboxinfo);
		}
		else if(chartinfo[1] == 'area') {
			$('#chatytype').val(chartinfo[1]);
			$("#charttitle").val(chartinfo[3]);
			var chkboxinfo = {'chartview':chartinfo[4]};
			chkboxvalueset(chkboxinfo);
			$("#selectmode").select2('val',chartinfo[5]).trigger('change');
			$("#chartaspect").select2('val',chartinfo[6]).trigger('change');
			
			var chkboxinfo = {'legendtype':chartinfo[7]};
			chkboxvalueset(chkboxinfo);			
			$("#legendstyle").select2('val',chartinfo[8]).trigger('change');			
			//chart-report-date
			$("#reportid").select2('val',chartinfo[9]).trigger('change');
			setTimeout(function(){			
			$("#reportgroupbyid").select2('val',chartinfo[10]).trigger('change');
			$("#aggregatefieldid").select2('val',chartinfo[12]).trigger('change');
			},300);
			setTimeout(function(){$("#operation").select2('val',chartinfo[13]).trigger('change');},50);
			var chkboxinfo = {'recordcount':chartinfo[11]};
			chkboxvalueset(chkboxinfo);
		}
		else if(chartinfo[1] == 'line') {
			$('#chatytype').val(chartinfo[1]);
			$("#charttitle").val(chartinfo[3]);
			var chkboxinfo = {'chartview':chartinfo[4]};
			chkboxvalueset(chkboxinfo);
			$("#selectmode").select2('val',chartinfo[5]).trigger('change');
			$("#chartaspect").select2('val',chartinfo[6]).trigger('change');
			
			var chkboxinfo = {'legendtype':chartinfo[7]};
			chkboxvalueset(chkboxinfo);			
			$("#legendstyle").select2('val',chartinfo[8]).trigger('change');			
			//chart-report-date
			$("#reportid").select2('val',chartinfo[9]).trigger('change');
			setTimeout(function(){			
			$("#reportgroupbyid").select2('val',chartinfo[10]).trigger('change');
			$("#aggregatefieldid").select2('val',chartinfo[12]).trigger('change');
			},300);
			setTimeout(function(){$("#operation").select2('val',chartinfo[13]).trigger('change');},50);
			var chkboxinfo = {'recordcount':chartinfo[11]};
			chkboxvalueset(chkboxinfo);
		}		
		else if(chartinfo[1] == 'stacked') {
			$('#chatytype').val(chartinfo[1]);
			$("#charttitle").val(chartinfo[3]);
			$("#selectmode").select2('val',chartinfo[4]).trigger('change');
			$("#stacktype").select2('val',chartinfo[5]).trigger('change');
			var chkboxinfo = {'legendtype':chartinfo[6]};
			chkboxvalueset(chkboxinfo);			
			$("#legendstyle").select2('val',chartinfo[7]).trigger('change');
			//chart-report-date
			$("#reportid").select2('val',chartinfo[8]).trigger('change');
			setTimeout(function(){			
			$("#reportgroupbyid").select2('val',chartinfo[9]).trigger('change');
			$("#reportsplitbyid").select2('val',chartinfo[10]).trigger('change');						
			$("#aggregatefieldid").select2('val',chartinfo[12]).trigger('change');},300);			
			setTimeout(function(){$("#operation").select2('val',chartinfo[13]).trigger('change');},50);
			var chkboxinfo = {'recordcount':chartinfo[11]};
			chkboxvalueset(chkboxinfo); 
		}		
		else if(chartinfo[1] == 'bubble') {
			$('#chatytype').val(chartinfo[1]);
			$("#charttitle").val(chartinfo[3]);
			$("#selectmode").select2('val',chartinfo[4]).trigger('change');
			$("#chartaspect").select2('val',chartinfo[5]).trigger('change');
			//chart-report-date
			$("#reportid").select2('val',chartinfo[6]).trigger('change');
			setTimeout(function(){			
			$("#reportgroupbyid").select2('val',chartinfo[7]).trigger('change');
			$("#reportsplitbyid").select2('val',chartinfo[8]).trigger('change');	
			$("#aggregatefieldid").select2('val',chartinfo[10]).trigger('change');},300);
			setTimeout(function(){$("#operation").select2('val',chartinfo[11]).trigger('change');},50);
			var chkboxinfo = {'recordcount':chartinfo[9]};
			chkboxvalueset(chkboxinfo);
		}
		
	}
	
	//set properties to chart text box
	function chartelementsdataorganize(chartid) {
		var rowid = $('#chartnameinfo'+chartid+'').data('chartrowid');
		var charttype = $('#chatytype').val();
		var chartview = $("#chartview").val();
		var angle = $("#anglevalue").val();
		var stacktype = $("#stacktype").val();
		var selectmode = $("#selectmode").val();
		var chartaspect = $("#chartaspect").val();
		var title = $("#charttitle").val();
		var legendval = $("#legendtype").val();
		var legstyle = $("#legendstyle").val();
		//report-group-chart-data
		var sourcereport = $("#reportid").val();
		var recordcount = $("#recordcount").val();
		var datafield = $("#aggregatefieldid").val();
		var operation = $("#operation").val();
		var twopointgroup = $("#reporttwopointvalue").val();
		//
		var groupby=$("#reportgroupbyidtext").val();//alias reportgroupbyid
		var groupsplitby=$("#reportsplitbyidtext").val();//alias reportsplitbyid
		if(recordcount == true || recordcount == 'true')
		{
			var datafield = 1;		
		}		
		if(charttype == 'wordcloud'){
			recordcount = "false"
		}
		if(charttype == 'pie') {
			var piechartinfo = rowid+','+charttype+',chartcreated'+chartid+','+title+','+chartview+','+angle+','+legendval+','+legstyle+','+sourcereport+','+groupby+','+recordcount+','+datafield+','+operation;
			$('#chartnameinfo'+chartid+'').val(piechartinfo);
		} else if(charttype == 'donut') {
			var donut = rowid+','+charttype+',chartcreated'+chartid+','+title+','+chartview+','+angle+','+legendval+','+legstyle+','+sourcereport+','+groupby+','+recordcount+','+datafield+','+operation;
			$('#chartnameinfo'+chartid+'').val(donut);
		}  else if(charttype == 'funnel') {
			var funnel = rowid+','+charttype+',chartcreated'+chartid+','+title+','+selectmode+','+legendval+','+legstyle+','+sourcereport+','+groupby+','+recordcount+','+datafield+','+operation;
			$('#chartnameinfo'+chartid+'').val(funnel);
		} else if(charttype == 'pyramid') {
			var pyramid = rowid+','+charttype+',chartcreated'+chartid+','+title+','+selectmode+','+legendval+','+legstyle+','+sourcereport+','+groupby+','+recordcount+','+datafield+','+operation;
			$('#chartnameinfo'+chartid+'').val(pyramid);
		}
		else if(charttype == 'column') {
			var column = rowid+','+charttype+',chartcreated'+chartid+','+title+','+chartview+','+selectmode+','+chartaspect+','+legendval+','+legstyle+','+sourcereport+','+groupby+','+recordcount+','+datafield+','+operation;
				$('#chartnameinfo'+chartid+'').val(column);
			}	
		else if(charttype == 'bubble') {
			var bubble = rowid+','+charttype+',chartcreated'+chartid+','+title+','+selectmode+','+chartaspect+','+sourcereport+','+groupby+','+groupsplitby+','+recordcount+','+datafield+','+operation;
				$('#chartnameinfo'+chartid+'').val(bubble);
			}	
		else if(charttype == 'stacked') {
			var stacked = rowid+','+charttype+',chartcreated'+chartid+','+title+','+selectmode+','+stacktype+','+legendval+','+legstyle+','+sourcereport+','+groupby+','+groupsplitby+','+recordcount+','+datafield+','+operation;
			$('#chartnameinfo'+chartid+'').val(stacked);
		}	
		else if(charttype == 'angulargauge') {
			var angulargauge = rowid+','+charttype+',chartcreated'+chartid+','+title+','+legendval+','+legstyle+','+sourcereport+','+groupby+','+recordcount+','+datafield+','+operation;
			$('#chartnameinfo'+chartid+'').val(angulargauge);
		}
		else if(charttype == 'area') {
			var area = rowid+','+charttype+',chartcreated'+chartid+','+title+','+chartview+','+selectmode+','+chartaspect+','+legendval+','+legstyle+','+sourcereport+','+groupby+','+recordcount+','+datafield+','+operation;
			$('#chartnameinfo'+chartid+'').val(area);
		}
		else if(charttype == 'line') {
			var line = rowid+','+charttype+',chartcreated'+chartid+','+title+','+chartview+','+selectmode+','+chartaspect+','+legendval+','+legstyle+','+sourcereport+','+groupby+','+recordcount+','+datafield+','+operation;
			$('#chartnameinfo'+chartid+'').val(line);
		}
		else if(charttype == 'wordcloud') {
			var wordcloud = rowid+','+charttype+',chartcreated'+chartid+','+title+','+chartaspect+','+sourcereport+','+groupby+','+recordcount;
			$('#chartnameinfo'+chartid+'').val(wordcloud);
		}
	}
	//chart aspect function
	function chartaspect(getcharttype){
		if(getcharttype == 'column'){
			var select = $('#chartaspect');
			var newOptions = {
							'bar':'Bar',
			                'cylinder' : 'Cylinders',
			                'cone' : 'Cones',
			                'pyramid' : 'Pyramids',
			                'histogram' : 'Histograms'
			            };
			$('option', select).remove();
			$.each(newOptions, function(text, key) {
			    var option = new Option(key, text);
			    select.append($(option));
			});
		}
		if(getcharttype == 'area'){
			var select = $('#chartaspect');
			var newOptions = {
							'segmented':'Segmented',
			                'spline' : 'Spline',
			                'stepped' : 'Stepped',
			            };
			$('option', select).remove();
			$.each(newOptions, function(text, key) {
			    var option = new Option(key, text);
			    select.append($(option));
			});
		}
		if(getcharttype == 'line'){
			var select = $('#chartaspect');
			var newOptions = {
							'segmented':'Segmented',
			                'spline' : 'Spline',
			                'stepped' : 'Stepped',
			                'jumped': 'Jumped'
			            };
			$('option', select).remove();
			$.each(newOptions, function(text, key) {
			    var option = new Option(key, text);
			    select.append($(option));
			});
		}
		if(getcharttype == 'bubble'){
			var select = $('#chartaspect');
			var newOptions = {
							'circle':'Circle',
			                'triangle' : 'Triangle',
			                'square' : 'Square',
			                'diamond': 'Diamond',
			                'plus':'Plus',
			                'cross':'Cross',
			                'star5':'Star',
			                'rpoly5':'Polygon',
			                'gear7':'Gear',
			            };
			$('option', select).remove();
			$.each(newOptions, function(text, key) {
			    var option = new Option(key, text);
			    select.append($(option));
			});
		}
		if(getcharttype == 'wordcloud'){
			var select = $('#chartaspect');
			var newOptions = {
							'spiral':'Spiral',
			                'flow-center' : 'Flow Center',
			                'flow-top' : 'Flow Top',
			            };
			$('option', select).remove();
			$.each(newOptions, function(text, key) {
			    var option = new Option(key, text);
			    select.append($(option));
			});
		}
	}
	//values show hide
	function chartfiledsshowhide(getcharttype) {
	
		if(getcharttype == 'pie' || getcharttype == 'donut') {
			$('#attrchartview,#attrchartangle,#attrchartleg').show();
			$('#chartsplitby,#selectmodelabel,#chartaspectlabel,#selecttype').hide();
		} else if(getcharttype == 'funnel' || getcharttype == 'pyramid') {
			$('#attrchartleg,#selectmodelabel').show();
			$('#attrchartview,#attrchartangle,#chartsplitby,#chartaspectlabel,#selecttype').hide();
		}
		else if(getcharttype == 'column' ) {		
			$('#attrchartview,#attrchartleg,#chartaspectlabel,#selectmodelabel').show();
			$('#attrchartangle,#chartsplitby,#selecttype').hide();			
		}		
		else if(getcharttype == 'bubble') {
			$('#chartsplitby,#selectmodelabel,#chartaspectlabel').show();
			$('#attrchartview,#attrchartangle,#datafield,#operationlabel,#selecttype,#attrchartleg').hide();
		}
		else if(getcharttype == 'stacked') {
			$('#chartsplitby,#selectmodelabel,#selecttype').show();
			$('#attrchartview,#attrchartangle,#attrchartleg,#datafield,#operationlabel,#chartaspectlabel').hide();
		}		
		else if(getcharttype == 'area' ) {		
			$('#attrchartview,#selectmodelabel,#chartaspectlabel').show();
			$('#attrchartangle,#chartsplitby,#attrchartleg,#selecttype').hide();			
		}	
		else if(getcharttype == 'line' ) {		
			$('#attrchartview,#selectmodelabel,#chartaspectlabel').show();
			$('#attrchartangle,#chartsplitby,#attrchartleg,#selecttype').hide();				
		}	
		else if(getcharttype == 'wordcloud' ) {	
			$('#chartaspectlabel').show();
			$('#attrchartleg,#attrchartview,#attrchartangle,#chartsplitby,#selectmodelabel,#selecttype,#datafield,#operationlabel').hide();
		}
		else if(getcharttype == 'angulargauge') {		
			$('#attrchartleg').show();
			$('#attrchartview,#selectmodelabel,#chartaspectlabel,#chartsplitby,#attrchartangle,#selecttype').hide();
		}
	}
	//check box value set fun
	function chkboxvalueset(chkboxinfo) {
		$.each( chkboxinfo, function( key, value ) {
			$('#'+key+'').val(value);
			if(value == 'true') {
				$('#'+key+'').prop('checked', true);
			} else {
				$('#'+key+'').prop('checked', false);
			}
			$('#'+key+'').trigger('change');
		});
	}
	//dashboard create
	function dashboarddatacreate() {
		var dashboardinfo=$("#dbdatainfoform").serialize();
		var dashboarddata=$("#newmodulegenform").serialize();
		if(dashboardinfo != "" ) { 
		    if(dashboarddata != '') {
				$.ajax({
					url:base_url+"Dashboardbuilder/dashboradbuildercreate",
					data:"data=&" +dashboarddata+"&dbinfodata=&"+dashboardinfo,
					type:"post",
					cache:false,
					success :function(msg) {
						var nmsg = $.trim(msg);
						if(nmsg == 'Success') {
							$("#processoverlay").hide();
							setTimeout(function(){alertpopup('Dashboard Created Successfully!');},100);							
							window.location=base_url+'Dashboards';									
						} else if(nmsg == 'Fail') {
							alertpopup('Please Enter the Chart / Custom widget details.')
						}
					}
				});
			} else {
				alertpopup('Please Add Some widgets to create dashboard');
			}
		} 
	}
	//dashboard update
	function dashboarddataupdate() {
		var dashboardinfo=$("#dbdatainfoform").serialize();
		var dashboarddata=$("#newmodulegenform").serialize();
		if(dashboardinfo != "") {
			if(dashboarddata != '') {
				$.ajax({
					url:base_url+"Dashboardbuilder/dashboradbuilderupdate",
					data:"data=&" +dashboarddata+"&dbinfodata=&"+dashboardinfo,
					type:"post",
					cache:false,
					success :function(msg) {
						var nmsg = $.trim(msg);
						if(nmsg == 'Success') {
							setTimeout(function(){alertpopup('Dashboard Updated Successfully!');},100);
							window.location=base_url+'Dashboards';									
						} else if(nmsg == 'Fail') {
							alertpopup('Please Enter the Chart / Custom widget details.')
						}
					}
				});
			} else {
				alertpopup('Please Add Some widgets to update dashboard');
			}
		} 
	}
}
{//Pie Chart
	function piechartfun(piechartval)
	{
		var type = 'pie';
		if(piechartval[2] == 'false'){
			type = "pie";
		}
		else{
			type = "pie3d";
		}	
		var angle = 270;
		if(piechartval[3] != ''){
			angle = piechartval[3]
		}else{
			angle = 270
		}
		
		var data = {
				"gui":{
				    "context-menu":{
				        "button":{
				            "visible":0
				        }
				    },
				    "behaviors":[
				        {
				            "id":"DownloadPDF",
				            "enabled":"none"
				        },	
				        {
				            "id":"DownloadSVG",
				            "enabled":"none"
				        },	
				        {
				            "id":"Reload",
				            "enabled":"none"
				        },	
				        {
				            "id":"FullScreen",
				            "enabled":"none"
				        },	
				        {
				            "id":"Print",
				            "enabled":"none"
				        },	
				        {
				            "id":"SaveAsImage",
				            "enabled":"none"
				        },	
				        {
				            "id":"ViewSource",
				            "enabled":"none"
				        },	
				    ]
				},
			 "graphset": [
			        {
			    	 "type":type,
			    	 "title":{
			    		    "text":piechartval[1],
			    		    "background-color":"#546e7a",
			    		    "font-color":"#fff"
			    		},			    	        
			         "scale-r":{
			             "ref-angle":angle, 
			             "layout":"auto"
			         },
			    	 "plot":{
			    	        "offset-r":"1%", //To apply globally.
		    	        	"value-box":{
		    	                "visible":true, //Specify your visibility: true or false.
		    	                "placement":"out", //Specify your placement: "out", "in", or "center".
		    	                "text":"%t(%v)", //Specify your value box text.
		    	            	},
			    	        "tooltip":{
			    	            "visible":true, //Specify your visibility: true or false.
			    	            "text":"%t(%v)",
			    	        	}
			    	    },	
			    	 "series": [
			    	            {"values":[9], "text":"Attempted to Contact"},
			    	            {"values":[8], "text":"Hot Lead"},
			    	            {"values":[7], "text":"Not Contacted"},
			    	            {"values":[6], "text":"Lost Lead"},	    	            
			    	            {"values":[8], "text":"Contacted"},
			    	            {"values":[9], "text":"Contact in Future"}
			    	        ]
			        }]
	    }
		if(piechartval[4] == 'true'){			
		    data = {
				"gui":{
				    "context-menu":{
				        "button":{
				            "visible":0
				        }
				    },
				    "behaviors":[
				        {
				            "id":"DownloadPDF",
				            "enabled":"none"
				        },	
				        {
				            "id":"DownloadSVG",
				            "enabled":"none"
				        },	
				        {
				            "id":"Reload",
				            "enabled":"none"
				        },	
				        {
				            "id":"FullScreen",
				            "enabled":"none"
				        },	
				        {
				            "id":"Print",
				            "enabled":"none"
				        },	
				        {
				            "id":"SaveAsImage",
				            "enabled":"none"
				        },	
				        {
				            "id":"ViewSource",
				            "enabled":"none"
				        },	
				    ]
				},
			 "graphset": [
			        {
			    	 "type":type,
			    	 "title":{
			    		    "text":piechartval[1],
			    		    "background-color":"#546e7a",
			    		    "font-color":"#fff"
			    		},			    	  
			         "scale-r":{
			             "ref-angle":angle, 
			             "layout":"auto"
			         },
			    	 "plot":{
			    	        "offset-r":"1%", //To apply globally.		    	        	
			    	        "tooltip":{
			    	            "visible":true, //Specify your visibility: true or false.
			    	            "text":"%t(%v)",
			    	        	}
			    	    },	
			    	 "legend":{
			    		 "marker":{
			    	          "type":piechartval[5],
			    		 }
			    	 },
			    	 "series": [
			    	            {"values":[9], "text":"Attempted to Contact"},
			    	            {"values":[8], "text":"Hot Lead"},
			    	            {"values":[7], "text":"Not Contacted"},
			    	            {"values":[6], "text":"Lost Lead"},	    	            
			    	            {"values":[8], "text":"Contacted"},
			    	            {"values":[9], "text":"Contact in Future"}
			    	        ]
			        }]
	    }
	}
		zingchart.render({
		    id:piechartval[0] ,
		    theme :'classic',
		    data: data,
		    width: '100%',
		    height: "auto",
		   
		});
	}
}
{//Donut Chart
	function donutchartfun(donutchartval)
	{ 
		var type = 'ring';
		if(donutchartval[2] == 'false'){
			type = "ring";
		}
		else{
			type = "ring3d";
		}
		var angle = 300;
		if(donutchartval[3] != ''){
			angle = donutchartval[3]
		}else{
			angle = 300
		}
		var data = {
				"gui":{
				    "context-menu":{
				        "button":{
				            "visible":0
				        }
				    },
				    "behaviors":[
				        {
				            "id":"DownloadPDF",
				            "enabled":"none"
				        },	
				        {
				            "id":"DownloadSVG",
				            "enabled":"none"
				        },	
				        {
				            "id":"Reload",
				            "enabled":"none"
				        },	
				        {
				            "id":"FullScreen",
				            "enabled":"none"
				        },	
				        {
				            "id":"Print",
				            "enabled":"none"
				        },	
				        {
				            "id":"SaveAsImage",
				            "enabled":"none"
				        },	
				        {
				            "id":"ViewSource",
				            "enabled":"none"
				        },	
				    ]
				},
			 "graphset": [ 
			              {
					    	 "type":type,
					    	 "title":{
					    		    "text":donutchartval[1],
					    		    "background-color":"#546e7a",
					    		    "font-color":"#fff"
					    		},				    	
					         "scale-r":{
					             "ref-angle":angle, 
					             "layout":"auto"
					             },
					    	 "plot":{
					    	        "offset-r":"1%", //To apply globally.
					    	        "slice":"35%" ,
				    	        	"value-box":{
				    	                "visible":true, //Specify your visibility: true or false.
				    	                "placement":"out", //Specify your placement: "out", "in", or "center".
				    	                "text":"%t(%v)", //Specify your value box text.
				    	            	},
					    	        "tooltip":{
					    	            "visible":true, //Specify your visibility: true or false.
					    	            "text":"%t(%v)",
					    	        	}
					    	    },
					    	 "series": [
					    	            {"values":[9], "text":"Draft"},
					    	            {"values":[8], "text":"Waiting for Confirmation"},
					    	            {"values":[7], "text":"Cancelled"},
					    	            {"values":[6], "text":"Confirmed"},
					    	            {"values":[4], "text":"Delivered"}
					    	        ]
			              		}
				    	    ]
		    }
		if(donutchartval[4] == 'true'){
			data = {
					"gui":{
					    "context-menu":{
					        "button":{
					            "visible":0
					        }
					    },
					    "behaviors":[
					        {
					            "id":"DownloadPDF",
					            "enabled":"none"
					        },	
					        {
					            "id":"DownloadSVG",
					            "enabled":"none"
					        },	
					        {
					            "id":"Reload",
					            "enabled":"none"
					        },	
					        {
					            "id":"FullScreen",
					            "enabled":"none"
					        },	
					        {
					            "id":"Print",
					            "enabled":"none"
					        },	
					        {
					            "id":"SaveAsImage",
					            "enabled":"none"
					        },	
					        {
					            "id":"ViewSource",
					            "enabled":"none"
					        },	
					    ]
					},
				 "graphset": [ 
				              {
						    	 "type":type,
						    	 "title":{
						    		    "text":donutchartval[1],
						    		    "background-color":"#546e7a",
						    		    "font-color":"#fff"
						    		},				    	
						         "scale-r":{
						             "ref-angle":angle, 
						             "layout":"auto"
						             },
						    	 "plot":{
						    	        "offset-r":"1%", //To apply globally.
						    	        "slice":"35%" ,					    	        	
						    	        "tooltip":{
						    	            "visible":true, //Specify your visibility: true or false.
						    	            "text":"%t(%v)",
						    	        	}
						    	    },
						    	  "legend":{
							    		 "marker":{
							    	          "type":donutchartval[5],
							    		 }
							    	 },   
						    	 "series": [
						    	            {"values":[9], "text":"Draft"},
						    	            {"values":[8], "text":"Waiting for Confirmation"},
						    	            {"values":[7], "text":"Cancelled"},
						    	            {"values":[6], "text":"Confirmed"},
						    	            {"values":[4], "text":"Delivered"}
						    	        ]
				              		}
					    	    ]
			    }
		}
			zingchart.render({
			    id:donutchartval[0] ,
			    theme :'classic',
			    data: data,
			    width: "100%",
			    height: "auto",			   
			});
	}
}
{//Funnel Chart
	function funnelchartfun(funnelchartval)
	{ 
		var type = 'funnel';
		if(funnelchartval[2] == 'vertical'){
			type = "funnel";
		}
		else{
			type = "hfunnel";
		}
		var data = {
					"gui":{
					    "context-menu":{
					        "button":{
					            "visible":0
					        }
					    },
				    "behaviors":[
				        {
				            "id":"DownloadPDF",
				            "enabled":"none"
				        },	
				        {
				            "id":"DownloadSVG",
				            "enabled":"none"
				        },	
				        {
				            "id":"Reload",
				            "enabled":"none"
				        },	
				        {
				            "id":"FullScreen",
				            "enabled":"none"
				        },	
				        {
				            "id":"Print",
				            "enabled":"none"
				        },	
				        {
				            "id":"SaveAsImage",
				            "enabled":"none"
				        },	
				        {
				            "id":"ViewSource",
				            "enabled":"none"
				        },	
				        {
				        	"id":"LogScale",
				        	"enabled":"none"
				        }
				    ]
				},
			 "graphset": [ 
			              {
					    	 "type":type,
					    	 "title":{
					    		    "text":funnelchartval[1],
					    		    "background-color":"#546e7a",
					    		    "font-color":"#fff"
					    		},
					    	 "scale-x": {	
					    	 	},
					    	 "scale-y": {
					                "values": [
					                      "Manufacturing",
					                      "ERP",
					                      "Service Provider",
					                      "Government"
					                  ], 
					                 },
					    	 "plot": {
					                 "tooltip-text": "%t(%v)",
					                 "scales": "scale-x,scale-y",		                
					             },
					         
					    	 "series": [
					    	            {"values":[60], "text":"Manufacturing"},
					    	            {"values":[35], "text":"ERP"},
					    	            {"values":[20], "text":"Service Provider"},
					    	            {"values":[10], "text":"Government"},
					    	           ]
					    }
			         ]
		}
		if(funnelchartval[3] == 'true'){
			data = {
					"gui":{
					    "context-menu":{
					        "button":{
					            "visible":0
					        }
					    },
				    "behaviors":[
				        {
				            "id":"DownloadPDF",
				            "enabled":"none"
				        },	
				        {
				            "id":"DownloadSVG",
				            "enabled":"none"
				        },	
				        {
				            "id":"Reload",
				            "enabled":"none"
				        },	
				        {
				            "id":"FullScreen",
				            "enabled":"none"
				        },	
				        {
				            "id":"Print",
				            "enabled":"none"
				        },	
				        {
				            "id":"SaveAsImage",
				            "enabled":"none"
				        },	
				        {
				            "id":"ViewSource",
				            "enabled":"none"
				        },	
				        {
				        	"id":"LogScale",
				        	"enabled":"none"
				        }
				    ]
				},
			 "graphset": [ 
			              {
					    	 "type":type,
					    	 "title":{
					    		    "text":funnelchartval[1],
					    		    "background-color":"#546e7a",
					    		    "font-color":"#fff"
					    		},					    	 
					    	 "plot": {
					                 "tooltip-text": "%t(%v)",
					             },
					         "legend":{
						    		 "marker":{
						    	          "type":funnelchartval[4],
						    		 }
						    	 },   
					    	 "series": [
					    	            {"values":[60], "text":"Manufacturing"},
					    	            {"values":[35], "text":"ERP"},
					    	            {"values":[20], "text":"Service Provider"},
					    	            {"values":[10], "text":"Government"},
					    	           ]
					    }
			         ]
			}
		}	              
		zingchart.render({
		    id:funnelchartval[0] ,
		    theme :'classic',
		    data:data, 
		    width: "100%",
		    height: "auto",
		    
		});
	}
}
{//Pyramid Chart
	function pyramidchartfun(pyramidchartval)
	{ 
		var type = 'vertical';
		if(pyramidchartval[2] == 'vertical'){
			type = "funnel";
		}
		else{
			type = "hfunnel";
		}
		var data = {
					"gui":{
						    "context-menu":{
						        "button":{
						            "visible":0
						          }
						      },
				    "behaviors":[
							        {
							            "id":"DownloadPDF",
							            "enabled":"none"
							        },	
							        {
							            "id":"DownloadSVG",
							            "enabled":"none"
							        },	
							        {
							            "id":"Reload",
							            "enabled":"none"
							        },	
							        {
							            "id":"FullScreen",
							            "enabled":"none"
							        },	
							        {
							            "id":"Print",
							            "enabled":"none"
							        },	
							        {
							            "id":"SaveAsImage",
							            "enabled":"none"
							        },	
							        {
							            "id":"ViewSource",
							            "enabled":"none"
							        },	
							        {
							        	"id":"LogScale",
							        	"enabled":"none"
							        }
							   ]
				},
			 "graphset": [ 
			              {
			            	 "type":type,
			 		    	 "title":{
			 		    		    "text":pyramidchartval[1],
			 		    		    "background-color":"#546e7a",
			 		    		    "font-color":"#fff"
			 		    		},
		 		    		 "scale-x": {
		 			    	 	},
		 			    	 "scale-y": {
		 			    		 	mirrored: true,
		 			                "values": [
		 			                      "Manufacturing",
		 			                      "ERP",
		 			                      "Service Provider",
		 			                      "Government"
		 			                  ], 
		 			                 },
		 			    	 "plot": {
		 			                 "tooltip-text": "%t(%v)",
		 			                 "scales": "scale-x,scale-y",		                
		 			             },
		 			    	 "series": [
		 			    	            {"values":[60], "text":"Manufacturing"},
		 			    	            {"values":[35], "text":"ERP"},
		 			    	            {"values":[20], "text":"Service Provider"},
		 			    	            {"values":[10], "text":"Government"},
		 			    	           ]
			              }
			         ]
		}
		if(pyramidchartval[3] == 'true')
		{
			data = {
					"gui":{
						    "context-menu":{
						        "button":{
						            "visible":0
						        }
						    },
						    "behaviors":[
						        {
						            "id":"DownloadPDF",
						            "enabled":"none"
						        },	
						        {
						            "id":"DownloadSVG",
						            "enabled":"none"
						        },	
						        {
						            "id":"Reload",
						            "enabled":"none"
						        },	
						        {
						            "id":"FullScreen",
						            "enabled":"none"
						        },	
						        {
						            "id":"Print",
						            "enabled":"none"
						        },	
						        {
						            "id":"SaveAsImage",
						            "enabled":"none"
						        },	
						        {
						            "id":"ViewSource",
						            "enabled":"none"
						        },	
						        {
						        	"id":"LogScale",
						        	"enabled":"none"
						        }
						    ]
						},
				    "graphset": [ 
					              {
				            	 "type":type,
				 		    	 "title":{
				 		    		    "text":pyramidchartval[1],
				 		    		    "background-color":"#546e7a",
				 		    		    "font-color":"#fff"
				 		    		},					    	 
			 		    		"scale-x": {
						    	 	},
						    	 "scale-y": {
						    		 	mirrored: true,
						                 },
						    	 "plot": {
						                 "tooltip-text": "%t(%v)",
						                 "scales":"scale-x,scale-y",		                
						             },
						         "legend":{
							    		 "marker":{
							    	          "type":pyramidchartval[4],
							    		 }
							    	 },
						    	 "series": [
						    	            {"values":[60], "text":"Manufacturing"},
						    	            {"values":[35], "text":"ERP"},
						    	            {"values":[20], "text":"Service Provider"},
						    	            {"values":[10], "text":"Government"},
						    	           ]
							      }
					           ]
					}
	  }
	  zingchart.render({
		    id:pyramidchartval[0] ,
		    theme :'classic',
		    data: data,
		    width: "100%",
		    height: "auto",
		});
	}
}
{//Angular Gauge Chart
	function angulargaguechartfun(angulargaugechartval)
	{
		var data = {
				"gui":{
				    "context-menu":{
				        "button":{
				            "visible":0
				        }
				    },
				    "behaviors":[
				        {
				            "id":"DownloadPDF",
				            "enabled":"none"
				        },	
				        {
				            "id":"DownloadSVG",
				            "enabled":"none"
				        },	
				        {
				            "id":"Reload",
				            "enabled":"none"
				        },	
				        {
				            "id":"FullScreen",
				            "enabled":"none"
				        },	
				        {
				            "id":"Print",
				            "enabled":"none"
				        },	
				        {
				            "id":"SaveAsImage",
				            "enabled":"none"
				        },	
				        {
				            "id":"ViewSource",
				            "enabled":"none"
				        },	
				    ]
				},
			 "graphset": [
			        {
		    	 "type":angulargaugechartval[2],
		    	 "title":{
		    		    "text":angulargaugechartval[1],
		    		    "background-color":"#546e7a",
		    		    "font-color":"#fff"
		    		},
		    	 "plot":{
		    		    "csize":"10px",
		    		    "tooltip-text": "%t(%v)",
		    		  },
		    	 "scale-r":{
		    			    "center":{
		    			      "size":5,
		    			    },
		    			    "aperture":"240"
		    		},		    		
		    	"series":[
						  {
						    "values":[90],"text":"Hot Lead"
						  },
						  {
						    "values":[20],"text":"Lost Lead"
						  },
						  {
						    "values":[50],"text":"Contacted"
						  },
	    		          ]
		    	}
			]
		}
		if(angulargaugechartval[3] == 'true' ){
			data = {
					"gui":{
					    "context-menu":{
					        "button":{
					            "visible":0
					        }
					    },
					    "behaviors":[
					        {
					            "id":"DownloadPDF",
					            "enabled":"none"
					        },	
					        {
					            "id":"DownloadSVG",
					            "enabled":"none"
					        },	
					        {
					            "id":"Reload",
					            "enabled":"none"
					        },	
					        {
					            "id":"FullScreen",
					            "enabled":"none"
					        },	
					        {
					            "id":"Print",
					            "enabled":"none"
					        },	
					        {
					            "id":"SaveAsImage",
					            "enabled":"none"
					        },	
					        {
					            "id":"ViewSource",
					            "enabled":"none"
					        },	
					    ]
					},
				 "graphset": [
				        {
			    	 "type":angulargaugechartval[2],
			    	 "title":{
			    		    "text":angulargaugechartval[1],
			    		    "background-color":"#546e7a",
			    		    "font-color":"#fff"
			    		},
			    	 "plot":{
			    		    "csize":"10px"
			    		  },
			    	 "scale-r":{
			    			    "center":{
			    			      "size":5,
			    			    },
			    			   "aperture":"240"
			    		},	
			    	"legend":{
				    		 "marker":{
				    	          "type":angulargaugechartval[4],
				    		 }
				    	 }, 	
			    	"series":[
		    		          {
		    		            "values":[90],"text":"Hot Lead"
		    		          },
		    		          {
			    		        "values":[20],"text":"Lost Lead"
			    		      },
			    		      {
				    		    "values":[50],"text":"Contacted"
				    		  },
		    		          ]
			    	}
			]
		}
	}
		zingchart.render({
		    id:angulargaugechartval[0] ,
		    theme :'classic',
		    data: data,
		    width: "100%",
		    height: "auto",		    
		 });
		
	}
}
{// Area Chart function
	function areachartfun(areachartval)
	{
		
		var type = 'area';
		if(areachartval[2] == 'horizontal'){
			type = "area";
		}
		else{
			type = "varea";
		}		
		if(areachartval[3] == 'true'){
			if(areachartval[2] == 'horizontal'){
				type = "area3d";
			}
		}
		var data = {
				"gui":{
				    "context-menu":{
				        "button":{
				            "visible":0
				        }
				    },
				    "behaviors":[
				        {
				            "id":"DownloadPDF",
				            "enabled":"none"
				        },	
				        {
				            "id":"DownloadSVG",
				            "enabled":"none"
				        },	
				        {
				            "id":"Reload",
				            "enabled":"none"
				        },	
				        {
				            "id":"FullScreen",
				            "enabled":"none"
				        },	
				        {
				            "id":"Print",
				            "enabled":"none"
				        },	
				        {
				            "id":"SaveAsImage",
				            "enabled":"none"
				        },	
				        {
				            "id":"ViewSource",
				            "enabled":"none"
				        },	
				        {
				        	"id":"LogScale",
				        	"enabled":"none"
				        }
				    ]
				},
			 "graphset": [
			        {		
			    	 "type":type,
			    	 "3d-aspect":{
				         "true3d":false,
				     },
			    	 "title":{
			    		    "text":areachartval[1],
			    		    "background-color":"#546e7a",
			    		    "font-color":"#fff"
			    		},
			    	 "plot":{ 
			    		    "aspect":areachartval[4] 
			    		  }, 	  
			    	 "plotarea": {
			    			   "adjust-layout":true, /* For automatic margin adjustment. */
								"margin-top":"40px"
			    			  },
			    	 "scale-x": {  
			    		 "labels":["Hot","Lost","Future","Contacted","Warm"]
			    	 },
			    	 "series":[ 
			    	           {"values":[20,10,30,38,15]}, 
			    	           
			    	         ] 
			        }
			    ]
		    	 
		    }
		zingchart.render({
		    id:areachartval[0] ,
		    theme :'classic',
		    data: data,
		    width: '100%',
		    height: 'auto',
		});
	}
}
{// Line Chart function
	function linechartfun(linechartval)
	{
		var type = 'line';
		if(linechartval[2] == 'horizontal'){
			type = "line";
		}
		else{
			type = "vline";
		}		
		if(linechartval[3] == 'true'){
			if(linechartval[2] == 'horizontal'){
				type = "line3d";
			}
		}
		zingchart.render({
		    id:linechartval[0] ,
		    theme :'classic',
		    width: "100%",
		    height: "auto",
		    data: {
		    	"gui":{
				    "context-menu":{
				        "button":{
				            "visible":0
				        }
				    },
				    "behaviors":[
				        {
				            "id":"DownloadPDF",
				            "enabled":"none"
				        },	
				        {
				            "id":"DownloadSVG",
				            "enabled":"none"
				        },	
				        {
				            "id":"Reload",
				            "enabled":"none"
				        },	
				        {
				            "id":"FullScreen",
				            "enabled":"none"
				        },	
				        {
				            "id":"Print",
				            "enabled":"none"
				        },	
				        {
				            "id":"SaveAsImage",
				            "enabled":"none"
				        },	
				        {
				            "id":"ViewSource",
				            "enabled":"none"
				        },	
				        {
				        	"id":"LogScale",
				        	"enabled":"none"
				        }
				    ]
				},
			 "graphset": [
			        {
				    	 "type":type,
				    	 "3d-aspect":{
					         "true3d":false,
					     },
				    	 "title":{
				    		    "text":linechartval[1],
				    		    "background-color":"#546e7a",
				    		    "font-color":"#fff"
				    		},
				    	"plot":{ 
				    		    "aspect":linechartval[4] 
				    		  },
		    		    "plotarea": {
		    			   "adjust-layout":true, /* For automatic margin adjustment. */
								"margin-top":"40px"
		    			  },
				    	 "scale-x": {  
				    		 "labels":["Hot","Lost","Future","Contacted","Warm"]
				    	 },
				    	 "series":[ 
				    	           {"values":[20,10,30,38,15]}, 
				    	           
				    	         ] 
			        }
			    ]
		    }
		});
	}
}
{//Bubble
	function bubblechartfun(bubblechartval)
	{
		var type = "bubble"
	    if(bubblechartval[2] == 'vertical'){
	    	type = "bubble"
	    }
	    else{
	    	type = "hbubble"
	    }
		var data = {
					"gui":{
					    "context-menu":{
					        "button":{
					            "visible":0
					        }
					    },
				    "behaviors":[
				        {
				            "id":"DownloadPDF",
				            "enabled":"none"
				        },	
				        {
				            "id":"DownloadSVG",
				            "enabled":"none"
				        },	
				        {
				            "id":"Reload",
				            "enabled":"none"
				        },	
				        {
				            "id":"FullScreen",
				            "enabled":"none"
				        },	
				        {
				            "id":"Print",
				            "enabled":"none"
				        },	
				        {
				            "id":"SaveAsImage",
				            "enabled":"none"
				        },	
				        {
				            "id":"ViewSource",
				            "enabled":"none"
				        },	
				        {
				        	"id":"LogScale",
				        	"enabled":"none"
				        }
				    ]
				},
			 "graphset": [
			        {
			    	 "type":type,
			    	 "title":{
			    		    "text":bubblechartval[1],
			    		    "background-color":"#546e7a",
			    		    "font-color":"#fff"
			    		},
			    	"plot":{
				    		 "marker":{
				                 "type":bubblechartval[3]
				             },
			    		  },
			    	"series":[
								{
								    "values":[
										      ['Draft','Bax','3'],
										      ['Booked','Orange','5'],
										      ['Lost','HCL','9'],
										      ['Cancel','Max','3'],
										     ]
								  },
								 {
									  "values":[
												['Booked','Bax','6'],
												['Draft','Orange','5'],
												['Cancel','HCL','3'],
												['Lost','Max','3'],
									           ]
									  
								 },
								 {
									  "values":[
												['Lost','Bax','9'],
											    ['Draft','Orange','5'],
											    ['Booked','HCL','6'],
											    ['Draft','Max','3']
									           ]
								 }  
								 
						     ]
			        }
			        ]
		    	}
		zingchart.render({
		    id:bubblechartval[0] ,
		    theme :'classic',
		    data: data,
		    width: "100%",
		    height: "auto",		    
		    });		
	}
}
{//Column
	function columnchartfun(columnchartval)
	{	
		var type = 'bar';
		if(columnchartval[2] == 'vertical'){
			type = "bar";
		}
		else{
			type = "hbar";
		}		
		if(columnchartval[3] == 'true'){
			if(columnchartval[2] == 'vertical'){
				type = "bar3d";
			}
			else{
				type = "hbar3d";
			}		
		}
		var data = {
				"gui":{
				    "context-menu":{
				        "button":{
				            "visible":0
				        }
				    },
				    "behaviors":[
				        {
				            "id":"DownloadPDF",
				            "enabled":"none"
				        },	
				        {
				            "id":"DownloadSVG",
				            "enabled":"none"
				        },	
				        {
				            "id":"Reload",
				            "enabled":"none"
				        },	
				        {
				            "id":"FullScreen",
				            "enabled":"none"
				        },	
				        {
				            "id":"Print",
				            "enabled":"none"
				        },	
				        {
				            "id":"SaveAsImage",
				            "enabled":"none"
				        },	
				        {
				            "id":"ViewSource",
				            "enabled":"none"
				        },	
				        {
				        	"id":"LogScale",
				        	"enabled":"none"
				        }
				    ]
				},
			 "graphset": [
			        {
				     "type":type,
				     "3d-aspect":{
				         "true3d":false,
				     },
				     "title":{
				    	 "text":columnchartval[1],
				    	 "background-color":"#546e7a",
			    		 "font-color":"#fff"
			    		},		    		
			    	 "plotarea": {
			    		   "adjust-layout":true, /* For automatic margin adjustment. */
							"margin-top":"40px"
			    		  },
			    	"plot":{
		    			    "aspect":columnchartval[4]
		    			  },
			    	 "scale-x": {		    		
			    		 "labels":["Hot","Attempted","Lost","Junk","Contacted","Future"], /* Scale Labels */	
			    	 	},	    	 
			    	 "series": [
			    	            {"values":[2500,2000,1322,1122,1114,984]}
			    	           ]
			        }
			    ]
		    }
		if(columnchartval[5] == 'true'){
			data = {
					"gui":{
					    "context-menu":{
					        "button":{
					            "visible":0
					        }
					    },
					    "behaviors":[
					        {
					            "id":"DownloadPDF",
					            "enabled":"none"
					        },	
					        {
					            "id":"DownloadSVG",
					            "enabled":"none"
					        },	
					        {
					            "id":"Reload",
					            "enabled":"none"
					        },	
					        {
					            "id":"FullScreen",
					            "enabled":"none"
					        },	
					        {
					            "id":"Print",
					            "enabled":"none"
					        },	
					        {
					            "id":"SaveAsImage",
					            "enabled":"none"
					        },	
					        {
					            "id":"ViewSource",
					            "enabled":"none"
					        },	
					        {
					        	"id":"LogScale",
					        	"enabled":"none"
					        }
					    ]
					},
				 "graphset": [
				        {
					     "type":type,
					     "3d-aspect":{
					         "true3d":false,
					     },
					     "title":{
					    	 "text":columnchartval[1],
					    	 "background-color":"#546e7a",
				    		 "font-color":"#fff"
				    		},		    		
				    	 "plotarea": {
				    		    "adjust-layout":true, /* For automatic margin adjustment. */
								"margin-top":"40px"
				    		  },
				    	"plot":{
			    			    "aspect":columnchartval[4]
			    			  },
			    		 "legend":{	
			    			 "marker":{
			    	          "type":columnchartval[6],
			    			 }
			    		 },
				    	 "series": [
				    	            {"values":[2500],"text":"Hot"},
				    	            {"values":[2000],"text":"Attempted"},
				    	            {"values":[1322],"text":"Lost"},
				    	            {"values":[1122],"text":"Junk"},
				    	            {"values":[1114],"text":"Contacted"},
				    	            {"values":[984],"text":"Future"}
				    	           ]
				        }
				      ]
			    }
			
		}
		zingchart.render({
		    id:columnchartval[0] ,
		    theme :'classic',
		    data: data,
		    width: "100%",
		    height: "auto",
		});
	}
}
{// Wordcloud function
	function wordcloudfun(wordcloudval){		
		zingchart.render({
		    id:wordcloudval[0] ,
		    theme :'classic',
		    width: "100%",
		    height: "auto",
		    data: {
		    		"gui":{
					    "context-menu":{
					        "button":{
					            "visible":0
					        }
					    },
				    "behaviors":[
					        {
					            "id":"DownloadPDF",
					            "enabled":"none"
					        },	
					        {
					            "id":"DownloadSVG",
					            "enabled":"none"
					        },	
					        {
					            "id":"Reload",
					            "enabled":"none"
					        },	
					        {
					            "id":"FullScreen",
					            "enabled":"none"
					        },	
					        {
					            "id":"Print",
					            "enabled":"none"
					        },	
					        {
					            "id":"SaveAsImage",
					            "enabled":"none"
					        },	
					        {
					            "id":"ViewSource",
					            "enabled":"none"
					        },	
					        {
					        	"id":"LogScale",
					        	"enabled":"none"
					        }
					    ]
				},
			 "graphset": [
			        {
			    	"type" : wordcloudval[2],
			    	"title":{
				    	 "text":wordcloudval[1],
				    	 "background-color":"#546e7a",
			    		  "font-color":"#fff"
			    		},
		    		"options":{ // Object
	    			    "text":"Home,Accounts,Leads,Contacts,Content,Documents,Collabaration,SMS,E-Mail,Sales,Leads,Purchase,Quotation,Invoice,Sales Order,Calls,Tasks,Calendar,Modules,Users,Profile,Activity,Notification,Templates,Payments,Support,Ticket,Solutions,Contract,Product,Price", // String
	    			    "max-font-size":50,
	    			    "aspect":wordcloudval[3]
	    			  }
			        }
			       ]
		    	
		    	}
		    });
	}
}
{//Stacked
	function stackedchartfun(stackedchartval) {
		var type = 'bar';
		if(stackedchartval[2] == 'vertical'){
			type = 'bar';
		}
		else {
			type = 'hbar';
		}
		var data = {
					"gui":{
						"context-menu":{
								"button":{
									"visible":0
								}
							},
				    "behaviors":[
				        {
				            "id":"DownloadPDF",
				            "enabled":"none"
				        },	
				        {
				            "id":"DownloadSVG",
				            "enabled":"none"
				        },	
				        {
				            "id":"Reload",
				            "enabled":"none"
				        },	
				        {
				            "id":"FullScreen",
				            "enabled":"none"
				        },	
				        {
				            "id":"Print",
				            "enabled":"none"
				        },	
				        {
				            "id":"SaveAsImage",
				            "enabled":"none"
				        },	
				        {
				            "id":"ViewSource",
				            "enabled":"none"
				        },	
				        {
				        	"id":"LogScale",
				        	"enabled":"none"
				        }
				    ]
				},
			 "graphset": [
			        {
				     "type":type,
				     "title":{
				    	 "text":stackedchartval[1],
				    	 "background-color":"#546e7a",
			    		 "font-color":"#fff"
			    		},		    	 
			    	 "plot":{
			    		    "stacked":true,
			    		    "stack-type":stackedchartval[3], /* Optional specification */
			    		  },
			    	 "scale-x": {
					    		 "labels":["Lost","Contacted","Hot","Future"], /* Scale Labels */
					    	 	},				 
			    	 "series": [
		    		             {"values":[10,20,30,40]},
		    		             {"values":[15,15,19,20]},
		    		             {"values":[10,20,30,40]},
		    		             ]
			        }
			    ]
		    }
		zingchart.render({
		    id:stackedchartval[0] ,
		    theme :'classic',
		    data: data,
		    width: "100%",
		    height: "auto",
		});
	}
}
{// Widget Creation Task
	function taskwidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){ 
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler" style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem;overflow-y:auto;"></div></div>');
		for(var i=0; i<datalen;i++ );
		{ 
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-10 medium-10 small-10 columns paddingzero"><div class="large-12 columns tasknamestyle ">Demo Required for '+i+'</div><div class="large-12 columns taskassignnamestyle">Ram Prakesh</div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">05/04 - '+i+'pm</span></div></div><div class="large-12 columns">&nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibtaskwidget").show();
		});
	}
}
{// Widget Creation Documents
	function documentswidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;;line-height:2rem;;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem;overflow-y:auto;"></div></div>');
		for(var i=0; i<datalen;i++ )
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-10 medium-10 small-10 columns paddingzero"><div class="large-12 columns tasknamestyle ">testingodf'+i+'.pdf </div><div class="large-12 columns taskassignnamestyle">Ram Prakesh</div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">Accounts'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibdocumentswidget").show();
		});
	}
}
{// Widget Creation Activities
	function activitieswidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem;overflow-y:auto;"></div></div>');
		for(var i=0; i<datalen;i++ )
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-10 medium-10 small-10 columns paddingzero"><div class="large-12 columns tasknamestyle ">Demo shown '+i+' </div><div class="large-12 columns taskassignnamestyle">Ram Prakesh @ Chennai - '+i+' </div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">05/04 - '+i+'pm</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibactivitieswidget").show();
		});
	}
}
{// Widget Creation Notifications
	function notificationswidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+' <span style="color:#df6c6e">[152]</span></span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right"><span class="mynotificationcls" style="border-right: 1px solid #ffffff;background:#df6c6e">All</span><span class="mynotificationcls">My</span><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem;overflow-y:auto;"></div></div>');
		for(var i=0; i<datalen;i++ )
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-10 medium-10 small-10 columns paddingzero"><div class="large-12 medium-12 small-12 columns paddingzero"><div class="large-1 medium-1 small-1 column" style="padding-right:0"><span class="" style="color:#686969;font-size:1.5rem;padding-top:0.8rem;top:0.8rem;position:relative"></span></div><div class="large-11 medium-11 small-11 columns notificationlinesstyle"><span style="color:#df6c6e">Ramesh</span> Created a new task For <span style="color:#23bab5">Arvind</span></div></div></div><div class="large-2 medium-2 small-2 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">'+i+' sec ago</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibnotificationwidget").show();
		});
	}
}
{// Widget Creation Mail Log
	function maillogwidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem;overflow-y:scroll;"></div></div>');
		for(var i=0; i<datalen;i++ )
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock tkmediumpr paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-12 medium-12 small-12 columns paddingzero"><div class="large-12 columns tasknamestyle">manju_'+i+'0@hotmail.com <span class="redcolor">(Manju'+i+')</span></div><div class="large-12 columns taskassignnamestyle">Regards Customer List</div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">12/01/1'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibmaillogwidget").show();
		});
	}
}
{// Widget Creation sms Log
	function smslogwidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem;overflow-y:auto;"></div></div>');
		for(var i=0; i<datalen;i++)
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock tkmediumpr paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-12 medium-12 small-12 columns paddingzero"><div class="large-12 columns tasknamestyle">Gowtham Sent a SMS to - +919988556633 Status - 1000647991-1|DELIVRD</div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">12/01/1'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibsmslogwidget").show();
		});
	}
}
{// Widget Creation Receive sms Log
	function receivesmslogwidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem;overflow-y:auto;"></div></div>');
		for(var i=0; i<datalen;i++)
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock tkmediumpr paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-12 medium-12 small-12 columns paddingzero"><div class="large-12 columns tasknamestyle">Gowtham Received a SMS From - +919988556633 </div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">12/01/1'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibreceivesmslogwidget").show();
		});
	}
}
{// Widget Creation send sms Log sender
	function sendsmslogwidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns end" ><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns" ><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;" ><span id='+closeid+' class="material-icons widgetredirecticon" title="close">close</span></div></div><div class="large-12 columns" style="height:11rem; overflow-y:scroll"><div id="quicksms" class="large-12 columns"><div class="large-12 columns" ><div class="large-12 columns"><div class="large-6 columns tasknamestyle paddingzero"> Mobile Number</div></div><div class="large-12 columns"><input id="" class="" type="text" data-prompt-position="topLeft" tabindex="" name="mobile number"></div></div><div class="large-12 columns paddingzero"><div class="large-4 medium-4 small-12 columns"><div class="large-12 columns tasknamestyle "> Type </div><div class="large-12 columns"> <select class="chzn-select" data-placeholder="Select" data-prompt-position="topLeft" id="" name="" ><option value="">select</option></select></div></div><div class="large-4 medium-4 small-12 columns"><div class="large-12 columns tasknamestyle "> Sender</div><div class="large-12 columns"><select class="chzn-select" data-placeholder="Select" data-prompt-position="topLeft" id="" name="" ><option value="">select</option></select></div></div><div class="large-4 medium-4 small-12 columns"><div class="large-12 columns tasknamestyle "> Template </div><div class="large-12 columns"><select class="chzn-select" data-placeholder="Select" data-prompt-position="topLeft" id="" name="" ><option value="">select</option></select></div></div></div><div class="large-12 columns"> &nbsp </div><div class="large-12 columns"><div class="large-12 columns"><textarea id="" data-prompt-position="topLeft" rows="2" tabindex="" name="MessageContent"></textarea></div></div></div></div>');
		$('.chzn-select').select2();
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibclicktocallwidget").show();
		});
	}
}
{// Widget Creation Click To Call
	function clicktocallwidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns end"> <div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler" style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"> <div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">Outgoing Call</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns" style="height:11rem; overflow-y:scroll"> <div id="" class="large-8 medium-8 columns small-12 large-centered medium-centered"> <label>Module<span class="mandatoryfildclass">*</span></label> <select class="chzn-select validate[required]" data-placeholder="Select" data-prompt-position="topLeft:14,36" id="c2cmoduleid" name="c2cmoduleid" tabindex="103"> <option value="">Select</option> </select> </div><div class="row">&nbsp;</div><div id="" class="large-8 medium-8 columns small-12 large-centered medium-centered">  <label>Related Module<span class="mandatoryfildclass">*</span></label><select class="chzn-select validate[required]" data-placeholder="Select" data-prompt-position="topLeft:14,36" id="c2crelmoduleid" name="c2crelmoduleid" tabindex="103"> <option value="">Select</option> </select> </div><div class="row">&nbsp;</div><div class="large-8 medium-8 columns small-12 large-centered medium-centered"> <label>Mobile Number<span class="mandatoryfildclass">*</span></label> <select class="chzn-select validate[required]" data-placeholder="Select" data-prompt-position="topLeft:14,36" id="c2crecordid" name="c2crecordid" tabindex="103"> <option value="">Select</option> </select> </div><div class="row">&nbsp;</div><div class="large-8 medium-8 columns small-12 large-centered medium-centered"> <label>Mobile Number<span class="mandatoryfildclass">*</span></label> <select class="chzn-select validate[required]" data-placeholder="Select" data-prompt-position="topLeft:14,36" id="c2cmobilenum" name="c2cmobilenum" tabindex="103"> <option value="">Select</option> </select> </div><div class="row">&nbsp;</div><div class="row">&nbsp;</div><div class="row">&nbsp;</div><div class="large-12 "> <div class="medium-4 large-4 columns">&nbsp;</div><div class="small-4 small-centered medium-4 large-4 columns"> <input type="button" id="c2csubmit" name="c2csubmit" value="Submit" class="formbuttonsalert"> </div><div class="medium-4 large-4 columns">&nbsp;</div><div class="large-12 columns">&nbsp;</div></div></div></div>');
		$('.chzn-select').select2();
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibclicktocallwidget").show();
		});
	}
}
{// Widget Creation sms Log
	function opportunitywidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem;overflow-y:auto;"></div></div>');
		for(var i=0; i<datalen;i++ )
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock tkmediumpr paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-12 medium-12 small-12 columns paddingzero"><div class="large-12 columns tasknamestyle">Opportunity Name '+i+'</div><div class="large-12 columns taskassignnamestyle">Amount : 21554'+i+'</div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">12/01/1'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibopportunitywidget").show();
		});
	}
}
{// Widget Creation inbound call Log
	function inboundcalllogwidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem"></div></div>');
		for(var i=0; i<datalen;i++)
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock tkmediumpr paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-12 medium-12 small-12 columns paddingzero"><div class="large-12 columns tasknamestyle">919988556633 Received a Call From - 919988556633 </div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">12/01/1'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibinboundcalllogwidget").show();
		});
	}
}
{// Widget Creation outbound call log
	function outboundcalllogwidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem"></div></div>');
		for(var i=0; i<datalen;i++)
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock tkmediumpr paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-12 medium-12 small-12 columns paddingzero"><div class="large-12 columns tasknamestyle">919988556633 Received a Call From - 919988556633 </div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">12/01/1'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#iboutboundcalllogwidget").show();
		});
	}
}
{// Widget Creation Contacts
	function contactswidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem;overflow-y:auto;"></div></div>');
		for(var i=0; i<datalen;i++ )
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock paddingzero"><div class="large-12 columns paddingzero"><div class="large-12 medium-21 small-12 columns paddingzero"><div class="large-12 columns tasknamestyle">Arvind ,Manager</div><div class="large-12 columns taskassignnamestyle"><span class="redcolor">M:</span> +91 9854'+i+'2454'+i+' <span class="redcolor">E:</span> arvind@aucventures.com</div></div></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibcontactswidget").show();
		});
	}
}
{// Widget Creation Payment
	function paymentwidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem;overflow-y:auto;"></div></div>');
		for(var i=0; i<datalen;i++ )
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock tkmediumpr paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-12 columns tasknamestyle"><span class="redcolor">Paid Amount:</span> 142232'+i+' <span class="redcolor">Type:</span>Full </div><div class="large-12 columns taskassignnamestyle">Mode:<span class="tealcolor">Cash </span></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">12/01/1'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibpaymentwidget").show();
		});
	}
}
{// Widget Creation Invoice
	function invoicewidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem;overflow-y:auto;"></div></div>');
		for(var i=0; i<datalen;i++ )
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock tkmediumpr paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-12 columns tasknamestyle"><span class="redcolor">Inv.No :</span> AX774'+i+' <span class="redcolor">Amount:</span> 142232'+i+'</div><div class="large-12 columns taskassignnamestyle">Assign To : <span class="tealcolor"> Ram Krisna </span></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">12/01/1'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibinvoicewidget").show();
		});
	}
}
{// Widget Creation Quote
	function quotewidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem;overflow-y:auto;"></div></div>');
		for(var i=0; i<datalen;i++ )
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock tkmediumpr paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-12 columns tasknamestyle"><span class="redcolor">Quote.No :</span> QX774'+i+' <span class="redcolor">Amount:</span> 3432'+i+'</div><div class="large-12 columns taskassignnamestyle">Assign To : <span class="tealcolor"> Arjun </span></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">12/01/1'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibquotewidget").show();
		});
	}
}
{// Widget Creation Status
	function leadstatuswidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem"></div></div>');
		var statusarray = ['Total Leads','Hot Leads','Warm Leads','Not yet'];
		for(var i=0; i<datalen;i++ )
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><span class="dbcaptionnamestyle">'+statusarray[i]+'</span></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="countnumberstyle">23'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibleadstatuswidget").show();
		});
	}
}
{// Widget Creation Accounts
	function accountswidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem;overflow-y:auto;"></div></div>');
		for(var i=0; i<datalen;i++ )
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-10 medium-10 small-10 columns paddingzero"><div class="large-12 columns tasknamestyle ">Account Name '+i+' </div><div class="large-12 columns taskassignnamestyle">Neal Caffrey</div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">Advertisement'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibaccountswidget").show();
		});
	}
}
{// Widget Creation Tickets
	function ticketswidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem;overflow-y:auto;"></div></div>');
		for(var i=0; i<datalen;i++ )
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-10 medium-10 small-10 columns paddingzero"><div class="large-12 columns tasknamestyle ">Ticket No : 124541'+i+' </div><div class="large-12 columns taskassignnamestyle">Deepa Rajan</div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">2014-10-1'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibticketswidget").show();
		});
	}
}
{// Widget Creation Campaign
	function campaignwidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem;overflow-y:auto;"></div></div>');
		for(var i=0; i<datalen;i++ )
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-10 medium-10 small-10 columns paddingzero"><div class="large-12 columns tasknamestyle ">Campaign Name : New Name'+i+' </div><div class="large-12 columns taskassignnamestyle">Deepa Rajan</div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">2014-10-1'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibcampaignwidget").show();
		});
	}
}
{// Widget Creation Sales Order
	function salesorderwidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem;overflow-y:auto;"></div></div>');
		for(var i=0; i<datalen;i++ )
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-10 medium-10 small-10 columns paddingzero"><div class="large-12 columns tasknamestyle "><span class="redcolor">Sales.Or.No :</span> SO146729691'+i+' </div><div class="large-12 columns taskassignnamestyle">Assign to : <span class="tealcolor">Rani</span></div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">2014-10-1'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibsalesorderwidget").show();
		});
	}
}
{// Widget Creation purchase Order 
	function purchaseorderwidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem;overflow-y:auto;"></div></div>');
		for(var i=0; i<datalen;i++ )
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-10 medium-10 small-10 columns paddingzero"><div class="large-12 columns tasknamestyle "><span class="redcolor">Purchase.Or.No :</span> PO146729691'+i+' </div><div class="large-12 columns taskassignnamestyle">Assign to : <span class="tealcolor">Rani</span></div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">2014-10-1'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibpurchaseorderwidget").show();
		});
	}
}
{// Widget Creation material requistion
	function materialrequisitionwidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem"></div></div>');
		for(var i=0; i<datalen;i++ )
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-10 medium-10 small-10 columns paddingzero"><div class="large-12 columns tasknamestyle "><span class="redcolor">Material Req.No :</span> MR146729691'+i+' </div><div class="large-12 columns taskassignnamestyle"><span class=""></span></div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">2014-10-1'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibmaterialrequisitionwidget").show();
		});
	}
}
{// Widget Creation Solutions
	function solutionswidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem;overflow-y:auto;"></div></div>');
		for(var i=0; i<datalen;i++ )
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-10 medium-10 small-10 columns paddingzero"><div class="large-12 columns tasknamestyle ">Serial Number: SN146729691'+i+' </div><div class="large-12 columns taskassignnamestyle">Status : Approved </div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">2014-10-1'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibsolutionwidget").show();
		});
	}
}
{// Widget Creation Solutions Category
	function solutionscatwidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem;overflow-y:auto;"></div></div>');
		for(var i=0; i<datalen;i++ )
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-10 medium-10 small-10 columns paddingzero"><div class="large-12 columns tasknamestyle ">Solution Name: Regards Entey '+i+' </div><div class="large-12 columns taskassignnamestyle">Parent : Test Solution </div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">2014-10-1'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibsolutioncatwidget").show();
		});
	}
}
{// Widget Creation Contract
	function contractwidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem;overflow-y:auto;"></div></div>');
		for(var i=0; i<datalen;i++ )
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-10 medium-10 small-10 columns paddingzero"><div class="large-12 columns tasknamestyle ">Contract No : CN554564'+i+' </div><div class="large-12 columns taskassignnamestyle">Assign To : Micheal </div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">2014-10-1'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibcontractwidget").show();
		});
	}
}
{// Widget Creation Lead
	function leadwidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem;overflow-y:auto;"></div></div>');
		for(var i=0; i<datalen;i++ )
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-10 medium-10 small-10 columns paddingzero"><div class="large-12 columns tasknamestyle ">Lead Name : Ramya '+i+' </div><div class="large-12 columns taskassignnamestyle">Assign To : Rajesh </div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">651'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibleadwidget").show();
		});
	}
}
{// Widget Creation Time and Date
	function timeanddatewidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem"></div></div>');
		for(var i=0; i<datalen;i++ )
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock paddingzero" style="height:5rem;"><div class="large-12 columns paddingzero"><div class="large-12 column daysstyledb" style="height:5rem;line-height:2.5rem;"> Thursday 9 October 2014</div><div class="large-12 column timeandsecstyledb"  style="height:5rem;line-height:5rem;"> HH:MM:SS</div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibtimeanddatewidget").show();
		});
	}
}
{// Widget Creation Calendar
	function calendarwidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem;overflow-y:auto;"></div></div>');
		for(var i=0; i<datalen;i++ )
		{
			$("#"+appendclass+"").append('<img src="'+base_url+'img/builders/calendar.jpg" style="with:100%">');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibcalendarwidget").show();
		});
	}
}
{// Widget conversation Calendar
	function conversationwidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){		
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id='+closeid+' title="Add" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass c_scroll" id="'+appendclass+'" style="height: 11rem;overflow-y:scroll"><span class="large-12 columns" id="textenterdiv"><div class="large-12 columns paddingzero"><div class="large-12 columns paddingzero" style="text-align:right"><span class="addtitlestyle">Add Title</span><span class="titleinputspan hidedisplay" ><input id="homedashboardtitle" placeholder="Enter Tile..." type="text"/><input type="hidden" id="unique_rowid"/></span></div> <div class="large-12 columns hidedisplay titleinputspan">&nbsp;</div> <div class="large-12 columns paddingzero"><textarea class="mention messagetoenterstyle" rows="2" placeholder="Enter Your Message Here..." id="homedashboardconvid" name="homedashboardconvid"></textarea><div class="atsearchdiv hidedisplay"></div><div style="background:#cccccc" class="large-12 columns paddingzero"><div class="large-6 columns"><span id="convmulitplefileuploader"></span><span style="position:relative;top:0.3rem;cursor:pointer" class="icon icon-attachment" title="Attachment"></span></div><div class="large-6 columns" style="text-align:right"><span><select class="chzn-select" style="width:20%"></select></span><span class="postbtnstyle" id="postsubmit">Post</span><span class="postbtnstyle hidedisplay" id="editpostsubmit">Post</span><span class="postbtnstyle hidedisplay" id="postcancel">Cancel</span></div></div><div class="large-12 columns">&nbsp;</div></div></div></span></div></div>');	
		var employeeurl='#';		
		for(var i=0; i<1;i++) 
		{
			//var imgurl = base_url+"img/blackuserimg.png";
			var imgurl = '<div class="convers-user-img"><span class=""><i class="material-icons">account_circle</i></span></div>';
			var editablefield='<span class="convopicons c_edit" title="Edit"><i class="material-icons">edit</i></span><span class="convopicons c_delete" title="Delete"><i class="material-icons">delete</i></span>'
			$("#"+appendclass+"").append('<div class="large-12 columns"><div class="large-12 medium-12 small-12 columns totalcotainerconv" style="border:1px solid #cccccc"><div class="large-9 mesium-9 small-9  columns paddingzero"><div class="large-2 column"><span class="conversationpropic"><span class=""><i class="material-icons">account_circle</i></span>'+imgurl+'</div><div class="large-10 column paddingzero"> <div class="large-12 columns conversationnamestyle" style="color:#df6c6e">Mathew</div><div class="large-12 columns conversationtitlestyle">xyz</div></div><div class="large-12 columns messagecontentstyle">john</div></div><div class="large-3 medium-3 small-3 columns paddingzero" style="text-align:right"><span title="" class="conversationtime">1 hour ago</span><br><span class="">'+imgurl+'</span></div><div class="large-12 columns paddingzero iconsetconv"  style="text-align:right;opacity:0"><span class="convopicons c_reply" title="Reply"><i class="material-icons">reply</i></span>'+editablefield+'</div></div></div><div class="large-12 columns"> &nbsp;</div>'); 
		} 		
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibconversationwidget").show();
		});
		$('.chzn-select').select2();
	}
}
{// Widget Creation Product
	function productwidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem;overflow-y:auto;"></div></div>');
		for(var i=0; i<datalen;i++ )
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-10 medium-10 small-10 columns paddingzero"><div class="large-12 columns tasknamestyle ">Product Name:iphone 6 '+i+' </div><div class="large-12 columns taskassignnamestyle">Category : Mobile </div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">651'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibproductwidget").show();
		});
	}
}
{// Widget Creation PriceBook
	function pricebookwidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,rowid){
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-1 myhandle columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><div class="large-11 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption" id="widgettitle'+rowid+'">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2rem;"><span id="'+closeid+'" class="material-icons widgetredirecticon">close</span></div></div><div class="large-12 columns scrollbarclass" id="'+appendclass+'" style="height: 11rem;overflow-y:auto;"></div></div>');
		for(var i=0; i<datalen;i++)
		{
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-10 medium-10 small-10 columns paddingzero"><div class="large-12 columns tasknamestyle ">Price Book:WeekEnd Sale</div><div class="large-12 columns taskassignnamestyle">Currency : US Dollar </div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">651'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibpricebookwidget").show();
		});
	}
}
//customwidgetcreation
{
	function customwidgetcreation(widgettitle,appendid,appendclass,closeid,rowid,count){	
		var closeid = closeid+count;
		var appendid = appendid+count;
		$("#"+closeid+"").click(function(){
			var parentdivid = $("#"+appendid+"").parent().attr('id');
			$('#'+parentdivid+'').removeClass('toblockmearge');
			$('#'+appendid+'').remove();
			$("#ibcustomwidget"+count+"").show();
		});
		var modulename = $("#modulename option:selected").text();
		$("#"+appendid+"").append('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption">CustomWidget(sample)</span></div><div class="large-6 medium-6 small-6 columns"  style="text-align:right"></div></div><div class="large-12 columns paddingzero"  style="height: 11rem;overflow-y:scroll"><div class="large-6 medium-6 small-12 columns" style="line-height:1.4rem;"><div class="large-12 columns"><div class="" style="color:#546e7a; font-size:0.8rem;">Contact Name</div><span class="" style="font-size:0.9rem;">Rajesh</span></div><div class="large-12">&nbsp;</div><div class="large-12 columns"><div class="" style="color:#546e7a; font-size:0.8rem;">Lead Status</div><span class="" style="font-size:0.9rem;">Active</span></div><div class="large-12">&nbsp;</div><div class="large-12 columns"><div class="" style="color:#546e7a; font-size:0.8rem;">Mobile number</div><span class="" style="font-size:0.9rem;">+91-9894541207</span></div><div class="large-12">&nbsp;</div></div><div class="large-6 medium-6 small-12 columns" style="line-height:1.4em;"><div class="large-12 columns"><div class="" style="color:#546e7a; font-size:0.8rem;">Account Name</div><span class="" style="font-size:0.9rem;">Rajesh & co</span></div><div class="large-12">&nbsp;</div><div class="large-12 columns"><div class="" style="color:#546e7a; font-size:0.8rem;">Lead Quality</div><span class="" style="font-size:0.9rem;">Super</span></div><div class="large-12">&nbsp;</div><div class="large-12 columns"><div class="" style="color:#546e7a; font-size:0.8rem;">Email Id</div><span class="" style="font-size:0.9rem;">Gowtham@aucventures.com</span></div><div class="large-12">&nbsp;</div></div><div class="large-12 columns"><div class="large-12 columns" style="line-height:1.5em;">');
	}
}
{//Chart DATA change events-related functions
	function resetchartparam(id) {
		$('#reportgroupbyid,#aggregatefieldid,#reportsplitbyid').empty();
		$('#reportgroupbyid').append($("<option></option>"));
		var groupdropdowns="#reportgroupbyid";
		if($('#chatytype').val() == 'stacked' || $('#chatytype').val() == 'bubble'){
			var groupdropdowns="#reportgroupbyid,#reportsplitbyid";
		}
		var moduleid=$('#modulename').val();		
		if(moduleid > 0 && id > 0 ){
			$.ajax({
				url:base_url+'Dashboardbuilder/loadgroupdropdowns?id='+id+'&moduleid='+moduleid,
				dataType:'json',
				cache:false,
				success: function(data) {   
					//reload groupby field
					group = data.group;
					$(''+groupdropdowns+'').empty();
					$('#reportgroupbyid').append($("<option></option>"));
					$('#reportsplitbyid').append($("<option></option>"));
					$.each(group, function(index) {
						$(''+groupdropdowns+'')
						.append($("<option></option>")					
						.attr("value",group[index]['datasid'])					
						.text(group[index]['dataname']));					
					});
					//reload calculated data
					calculation =data.calculation;
					$.each(data.calculation, function(index) {
						$('#aggregatefieldid')
						.append($("<option></option>")					
						.attr("value",calculation[index]['datasid'])					
						.text(calculation[index]['dataname']));					
					});
				},             
			});
		}
	}
	function resetsourcereport(id) {
		$('#reportid').empty();
		$('#reportid').append($("<option></option>"));		
		$.ajax({
			url:base_url+'Dashboardbuilder/loadreportlist?moduleid='+id,
			dataType:'json',
			cache:false,
			success: function(data) {   
				//reload groupby field
				$.each(data, function(index) {
					$('#reportid')
					.append($("<option></option>")					
					.attr("value",data[index]['datasid'])					
					.text(data[index]['dataname']));					
				});			
			},             
		});
		$('#reportid').select2('val','').trigger('change')
	}
	function accordian() {
		$(".fbuilderaccclick").click(function() {
			$(this).find('.toggleicons').text(' add ');
			var fbuildercatlist = $(this).data('elementcat');
			if($(".elementlist"+fbuildercatlist+"").hasClass( "hideotherelements" )){
				$(".elementlist"+fbuildercatlist+"").slideUp( "slow", function() {	
					$(".hideotherelements").removeClass('hideotherelements');	
				});
			} else {
				$(this).find('.toggleicons').text(' remove ');
				$(".hideotherelements").slideUp( "slow", function() {
					$('.fbuilderaccclick').find('.toggleicons').text(' add ');
					setTimeout(function() {
						var ff = $(".hideotherelements").prev('li').find('.toggleicons').text(' remove ');
					},10);
				  $(".hideotherelements").removeClass('hideotherelements');
				});
				$(".elementlist"+fbuildercatlist+"").slideDown( "slow", function() {	
					$(".elementlist"+fbuildercatlist+"").addClass('hideotherelements');
				});
			}
		});
    }	
}
// custom widget data filed function
function customwidgetdatafield() {
	$('#customdatafields').select2('val','');
	$('#customdatafields').empty();
	var modulename = $('#modulename').val();
	var customrelatedmodule = $('#customrelatedmodule').val();
	$.ajax({
		url: base_url + "Dashboardbuilder/customwidgetdatafield",
		data: "moduleid="+modulename+"&relatedmoduleids="+customrelatedmodule,
		dataType:'json',
		type: "POST",
		async:false,
		cache:false,
		success: function(data){					
			var newddappend="";
			var newlength=data.length;
			prev = ' ';
			for(var m=0;m < newlength;m++){		
				var cur = data[m]['moduleid'];
				if(prev != cur ) {
					if(prev != " ") {					
						newddappend +="</optgroup>";
					}
					newddappend +="<optgroup  label='"+data[m]['modulename']+" Info.' class=''>";					
					prev = data[m]['moduleid'];
				}
				newddappend += "<option value ='"+data[m]['datasid']+"-"+data[m]['dataname']+"' data-formfieldsidhidden='"+data[m]['datasid']+"' data-modfiledcolumn='"+data[m]['otherdataattrname1']+"' data-modfiledtable='"+data[m]['otherdataattrname2']+"' data-moduleid='"+data[m]['moduleid']+"' data-modulelabel='"+data[m]['modulefield']+"'>"+data[m]['dataname']+ "</option>";
			}
			//after run
			$('#customdatafields').append(newddappend);
		},
	});
}
//dashboard edit data fetch
function dashboardeditdatafetch(dashboardid) {
	$.ajax({
		url:base_url+'Dashboardbuilder/dashboardbuildereditdatafetch?dashboardid='+dashboardid,
		dataType:'json',
		cache:false,
		success: function(data) {   
			if((data.fail) != 'FAILED') {
				var mid = data.mid;	
				$("#modulename").select2('val',mid).trigger('change');
				var dashboardname = data.dashboardname;	
				$("#dashboardname").val(dashboardname);
				var folderid = data.folderid;
				$("#dbfoldername").select2('val',folderid);
				var description = data.description;
				$("#dbdescription").val(description);
				var setdefault = data.setdefault;	
				var active = data.active;
				var dbsetmini = data.dbsetmini;	
				if(setdefault == 'Yes') {
					$('#dbsetdefault').attr('checked',true);
					$('#dbsetdefaulthidden').val(setdefault);
				} else {
					$('#dbsetdefault').attr('checked',false);
					$('#dbsetdefaulthidden').val(setdefault);
				}
				if(active == 'Yes') {
					$('#dbsetactive').attr('checked',true);
					$('#dbsetactivehidden').val(active);
				} else {
					$('#dbsetactive').attr('checked',false);
					$('#dbsetactivehidden').val(active);
				}
				if(dbsetmini == 'Yes') {
					$('#dbsetmini').attr('checked',true);
					$('#dbsetminihidden').val(dbsetmini);
					$('#miniwidgetcount').val(data.miniwidgetcount);
					$('#miniwidgetcounthidden').val(data.miniwidgetcount);
					$('#miniwidget').removeClass('hidedisplay');
					$('#fullwidget').addClass('hidedisplay');
					$('#miniwidgetcountspan').show();
					editstatus = 1;
				} else {
					$('#dbsetmini').attr('checked',false);
					$('#dbsetminihidden').val(dbsetmini);
					$('#miniwidget').addClass('hidedisplay');
				    $('#fullwidget').removeClass('hidedisplay');
				    $('#miniwidgetcountspan').hide();
					editstatus = 0;
				}
				dashboardrowdatafetch(dashboardid);
			}			
		},             
	});
}
//dashboard row data fetch
function dashboardrowdatafetch(dashboardid) {
	$.ajax({
		url:base_url+'Dashboardbuilder/dashboardbuilderrowdatafetch?dashboardid='+dashboardid,
		dataType:'json',
		cache:false,
		success: function(data) {   
			if((data.fail) != 'FAILED') {
				var rowid = data['rowid'];
				$("#dashboardrowid").val(rowid);
				for(var i=0;i < data['rowtype'].length;i++) {
					if(data['rowtype'][i] == 1) {
						$("#addonecolumn").trigger('click');
					} else if(data['rowtype'][i] == 2) {
						$("#addtwocolumn").trigger('click');
					} else if(data['rowtype'][i] == 3) {
						$("#addthreecolumn").trigger('click');
					}else if(data['rowtype'][i] == 4) {
						addminicolumncreate();
					}
				}
				dashboardfielddatafetch(dashboardid,rowid);
			}
		}
	});
}
//dashboard widget data fetch
function dashboardfielddatafetch(dashboardid,frowid) {
	$.ajax({
		url:base_url+'Dashboardbuilder/dashboardbuilderwidgetdatafetch?dashboardid='+dashboardid+"&rowid="+frowid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) { 
			if((data.fail) != 'FAILED') {
				bcount = data.length;
				 for(var i=0;i < data.length;i++) {
					var chartview = data[i].viewtype;
					var theme = data[i].theme;
					var angle = data[i].angle;
					var title = data[i].filedname;
					var legend = data[i].legend;
					var legstyle = data[i].legendtype;
					var basecolor = data[i].basecolor;
					var rotate = data[i].rotate;
					var rotate = data[i].rotate;
					var reportid = data[i].reportid;
					var groupfield = data[i].groupfiled;
					var countstatus = data[i].countstatus;
					var calculationfield = data[i].calculationfield;
					var operation = data[i].operation;
					if(data[i].widgetname == 'piechart') {
						var charttype = "pie";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						if(!$("#"+currentblockid+"").hasClass('toblockmearge')) {
							$("#"+currentblockid+"").addClass('toblockmearge');
							$("#"+currentblockid+"").append('<div class="large-11 column paddingzero"><div class="large-1 columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><span class="material-icons graphremoveicon removecharts myhandle" title="Remove">close</span><div class="graphstyletop" data-chartcount="'+i+'" data-charttypedata="'+charttype+'" ><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',chartcreated'+i+','+title+','+chartview+','+angle+','+legend+','+legstyle+','+reportid+','+groupfield+','+countstatus+','+calculationfield+','+operation+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"><div id="chartcreated'+i+'" class="graph400x300"></div></div></div>');
							$(".forgetid").unbind();
							$(document).ready(function(){
								updatechartelementsdataorganize(i,data);
								var piechartarray = ['chartcreated'+i+'',title,chartview,angle,legend,legstyle];
								piechartfun(piechartarray);
							});
							//For get and Set value
							valuesettingfunction();
							//For Remove Charts
							$('.removecharts').click(function(){
								var toremoveblock = $(this).parent('div').parent('div').attr('id');
								$("#"+toremoveblock+"").removeClass('toblockmearge');
								$(this).parent('div').remove();
							});
							 // dashboard container  sorting - without grid or editor
							var dbinnercontainer = document.getElementById("spanforaddtab");
							new Sortable(dbinnercontainer);
							Sortable.create(dbinnercontainer, {
								animation: 150,
								group: 'dbinnercontainer',
								draggable: '.myid',
							}); 
							
						} else {
							alertpopup('Please Drop In Another Block');
							$(".forgetid").unbind();
						}
					} else if(data[i].widgetname == 'donutchart') {
						var charttype = "donut";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-11 column paddingzero"><div class="large-1 columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><span class="material-icons graphremoveicon removecharts myhandle" title="Remove">close</span><div class="graphstyletop" data-chartcount="'+i+'" data-charttypedata="'+charttype+'" ><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',chartcreated'+i+','+title+','+chartview+','+angle+','+legend+','+legstyle+','+reportid+','+groupfield+','+countstatus+','+calculationfield+','+operation+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"><div id="chartcreated'+i+'" class="graph400x300"></div></div></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							updatechartelementsdataorganize(i,data);
							var donutchartarray = ['chartcreated'+i+'',title,chartview,angle,legend,legstyle];
							donutchartfun(donutchartarray);
						});
						//For get and Set value
						valuesettingfunction();
						//For Remove Charts
						$('.removecharts').click(function(){
							var toremoveblock = $(this).parent('div').parent('div').attr('id');
							$("#"+toremoveblock+"").removeClass('toblockmearge');
							$(this).parent('div').remove();
						});						
					} else if(data[i].widgetname == 'funnelchart') { 
						var charttype = "funnel";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-11 column paddingzero"><div class="large-1 columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><span class="material-icons graphremoveicon removecharts" title="Remove">close</span><div class="graphstyletop" data-chartcount="'+i+'" data-charttypedata="'+charttype+'" ><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',chartcreated'+i+','+title+','+theme+','+legend+','+legstyle+','+reportid+','+groupfield+','+countstatus+','+calculationfield+','+operation+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"><div id="chartcreated'+i+'" class="graph400x300"></div></div></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							updatechartelementsdataorganize(i,data);
							var funnelchartarray = ['chartcreated'+i+'',title,theme,legend,legstyle];
							funnelchartfun(funnelchartarray);
						});
						//For get and Set value
						valuesettingfunction();
						//For Remove Charts
						$('.removecharts').click(function(){
							var toremoveblock = $(this).parent('div').parent('div').attr('id');
							$("#"+toremoveblock+"").removeClass('toblockmearge');
							$(this).parent('div').remove();
						});
					} else if(data[i].widgetname == 'pyramidchart') {
						var charttype = "pyramid";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-11 column paddingzero"><div class="large-1 columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><span class="material-icons graphremoveicon removecharts" title="Remove">close</span><div class="graphstyletop" data-chartcount="'+i+'" data-charttypedata="'+charttype+'" ><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',chartcreated'+i+','+title+','+theme+','+legend+','+legstyle+','+reportid+','+groupfield+','+countstatus+','+calculationfield+','+operation+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"><div id="chartcreated'+i+'" class="graph400x300"></div></div></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							updatechartelementsdataorganize(i,data);
							var pyramidchartarray = ['chartcreated'+i+'',title,theme,legend,legstyle];
							pyramidchartfun(pyramidchartarray);
						});
						//For get and Set value
						valuesettingfunction();
						//For Remove Charts
						$('.removecharts').click(function(){
							var toremoveblock = $(this).parent('div').parent('div').attr('id');
							$("#"+toremoveblock+"").removeClass('toblockmearge');
							$(this).parent('div').remove();
						});
					} else if(data[i].widgetname == 'angulargaugechart') {
						var charttype = "angulargauge";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-11 column paddingzero"><div class="large-1 columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><span class="material-icons graphremoveicon removecharts myhandle" title="Remove">close</span><div class="graphstyletop" data-chartcount="'+i+'" data-charttypedata="'+charttype+'" ><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',chartcreated'+i+','+title+','+legend+','+legstyle+','+reportid+','+groupfield+','+countstatus+','+calculationfield+','+operation+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"><div id="chartcreated'+i+'" class="graph400x300"></div></div></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							updatechartelementsdataorganize(i,data);
							var angulargaugechartarray = ['chartcreated'+i+'',title,'gauge',legend,legstyle];
							angulargaguechartfun(angulargaugechartarray);
						});
						//For get and Set value
						valuesettingfunction();
						//For Remove Charts
						$('.removecharts').click(function(){
							var toremoveblock = $(this).parent('div').parent('div').attr('id');
							$("#"+toremoveblock+"").removeClass('toblockmearge');
							$(this).parent('div').remove();
						});
					} else if(data[i].widgetname == 'wordcloudchart') {
						var charttype = "wordcloud";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-11 column paddingzero"><div class="large-1 columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><span class="material-icons graphremoveicon removecharts myhandle" title="Remove">close</span><div class="graphstyletop" data-chartcount="'+i+'" data-charttypedata="'+charttype+'" ><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',chartcreated'+i+',Chart Title,," id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"><div id="chartcreated'+i+'" class="graph400x300"></div></div></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							updatechartelementsdataorganize(i,data);
							var wordcloudarray = ['chartcreated'+i+'',chartview,theme,angle,title,legendval,basecolor];
							wordcloudfun(wordcloudarray);
						});
						//For get and Set value
						valuesettingfunction();
						//For Remove Charts
						$('.removecharts').click(function(){
							var toremoveblock = $(this).parent('div').parent('div').attr('id');
							$("#"+toremoveblock+"").removeClass('toblockmearge');
							$(this).parent('div').remove();
						});
					}
					else if(data[i].widgetname == 'areachart') { 
						var charttype = "area";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-11 column paddingzero"><div class="large-1 columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><span class="material-icons graphremoveicon removecharts" title="Remove">close</span><div class="graphstyletop" data-chartcount="'+i+'" data-charttypedata="'+charttype+'" ><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',chartcreated'+i+','+title+','+chartview+','+theme+','+basecolor+','+legend+','+legstyle+','+reportid+','+groupfield+','+countstatus+','+calculationfield+','+operation+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"><div id="chartcreated'+i+'" class="graph400x300"></div></div></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							updatechartelementsdataorganize(i,data);
							var areachartarray = ['chartcreated'+i+'',title,theme,chartview,basecolor];
							areachartfun(areachartarray);
						});
						//For get and Set value
						valuesettingfunction();
						//For Remove Charts
						$('.removecharts').click(function(){
							var toremoveblock = $(this).parent('div').parent('div').attr('id');
							$("#"+toremoveblock+"").removeClass('toblockmearge');
							$(this).parent('div').remove();
						});
					} 
					else if(data[i].widgetname == 'linechart') { 
						var charttype = "line";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-11 column paddingzero"><div class="large-1 columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><span class="material-icons graphremoveicon removecharts" title="Remove">close</span><div class="graphstyletop" data-chartcount="'+i+'" data-charttypedata="'+charttype+'" ><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',chartcreated'+i+','+title+','+chartview+','+theme+','+basecolor+','+legend+','+legstyle+','+reportid+','+groupfield+','+countstatus+','+calculationfield+','+operation+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"><div id="chartcreated'+i+'" class="graph400x300"></div></div></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							updatechartelementsdataorganize(i,data);
							var linechartarray = ['chartcreated'+i+'',title,theme,chartview,basecolor];
							linechartfun(linechartarray);
						});
						//For get and Set value
						valuesettingfunction();
						//For Remove Charts
						$('.removecharts').click(function(){
							var toremoveblock = $(this).parent('div').parent('div').attr('id');
							$("#"+toremoveblock+"").removeClass('toblockmearge');
							$(this).parent('div').remove();
						});
					} else if(data[i].widgetname == 'bubblechart') {
						var charttype = "bubble";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-11 column paddingzero"><div class="large-1 columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><span class="material-icons graphremoveicon removecharts myhandle" title="Remove">close</span><div class="graphstyletop" data-chartcount="'+i+'" data-charttypedata="'+charttype+'" ><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',chartcreated'+i+','+title+','+theme+','+basecolor+','+''+','+reportid+','+groupfield+','+countstatus+','+calculationfield+','+operation+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"><div id="chartcreated'+i+'" class="graph400x300"></div></div></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							updatechartelementsdataorganize(i,data);
							var bubblechartarray = ['chartcreated'+i+'',title,theme,basecolor];
							bubblechartfun(bubblechartarray);
						});
						//For get and Set value
						valuesettingfunction();
						//For Remove Charts
						$('.removecharts').click(function(){
							var toremoveblock = $(this).parent('div').parent('div').attr('id');
							$("#"+toremoveblock+"").removeClass('toblockmearge');
							$(this).parent('div').remove();
						});
					} else if(data[i].widgetname == 'columnchart') {
						var charttype = "column";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-11 column paddingzero"><div class="large-1 columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><span class="material-icons graphremoveicon removecharts" title="Remove">close</span><div class="graphstyletop" data-chartcount="'+i+'" data-charttypedata="'+charttype+'" ><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',chartcreated'+i+','+title+','+chartview+','+theme+','+basecolor+','+legend+','+legstyle+','+reportid+','+groupfield+','+countstatus+','+calculationfield+','+operation+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"><div id="chartcreated'+i+'" class="graph400x300"></div></div></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							//update function
							updatechartelementsdataorganize(i,data);
							var columnchartarray = ['chartcreated'+i+'',title,theme,chartview,basecolor,legend,legstyle];
							columnchartfun(columnchartarray);
						});
						//For get and Set value
						valuesettingfunction();
						//For Remove Charts
						$('.removecharts').click(function(){
							var toremoveblock = $(this).parent('div').parent('div').attr('id');
							$("#"+toremoveblock+"").removeClass('toblockmearge');
							$(this).parent('div').remove();
						});
					} else if(data[i].widgetname == 'stackedchart') {
						var charttype = "stacked";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-11 column paddingzero"><div class="large-1 columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><span class="material-icons graphremoveicon removecharts" title="Remove">close</span><div class="graphstyletop" data-chartcount="'+i+'" data-charttypedata="'+charttype+'" ><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',chartcreted'+i+','+title+','+theme+','+rotate+','+''+','+legend+','+legstyle+','+reportid+','+groupfield+','+countstatus+','+calculationfield+','+operation+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"><div id="chartcreated'+i+'" class="graph400x300"></div></div></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							updatechartelementsdataorganize(i,data);
							var stackedchartarray = ['chartcreated'+i+'',title,theme,rotate];
							stackedchartfun(stackedchartarray);
						});
						//For get and Set value
						valuesettingfunction();
						//For Remove Charts
						$('.removecharts').click(function(){
							var toremoveblock = $(this).parent('div').parent('div').attr('id');
							$("#"+toremoveblock+"").removeClass('toblockmearge');
							$(this).parent('div').remove();
						});
					} else if(data[i].widgetname == 'cylinder3dchart') {
						var charttype = "cylinder3d";				
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-11 column paddingzero"><div class="large-1 columns end" style="padding:0.4rem 0 0 0;"><span  class="material-icons widgethandler"style="cursor:move">drag_handle</span></div><span class="material-icons graphremoveicon removecharts" title="Remove">close</span><div class="graphstyletop" data-chartcount="'+i+'" data-charttypedata="'+charttype+'" ><input name="chartnameinfo[]" type="hidden"  value="'+rowid+','+charttype+',chartcreated'+i+',Chart Title,false,circle" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"><div id="chartcreated'+i+'" class="graph400x300"></div></div></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							updatechartelementsdataorganize(i,data);
							var cylinder3dchartarray = ['chartcreated'+i+'','','',theme,title,rotate,legendval,chartview,angle];
							cylinder3dchartfun(cylinder3dchartarray);
						});
						//For get and Set value
						valuesettingfunction();
						//For Remove Charts
						$('.removecharts').click(function(){
							var toremoveblock = $(this).parent('div').parent('div').attr('id');
							$("#"+toremoveblock+"").removeClass('toblockmearge');
							$(this).parent('div').remove();
						});
					}  else if(data[i].widgetname == 'ibtaskwidget') {
						var charttype = "ibtaskwidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="taskwidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',crmtask,crmtask.crmtaskid|crmtaskname|taskstartdate|taskstarttime|employeename,employee,employeeid,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							taskwidgetcreation(data[i].filedname,2,'taskwidgetid','taskwidgetclass','closetaskwidget',i);
						});
						$(".predefinedwidget#ibtaskwidget").hide();
						//For get and Set value
						valuesettingfunction();
					}else if(data[i].widgetname == 'ibtaskwidgetmini') {
						var charttype = "ibtaskwidgetmini";
						var currentblockid = 'widgetcontainer'+i+'';
						//var rowid = $('#'+currentblockid+'').data('rownum');
						var rowid = 0;
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="taskwidgetid"  data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',crmtask,crmtask.crmtaskid|crmtaskname|taskstartdate|taskstarttime|employeename,employee,employeeid,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							taskwidgetcreation(data[i].filedname,2,'taskwidgetid','taskwidgetclass','closetaskwidget',i);
						});
						$(".predefinedwidget#ibtaskwidgetmini").hide();
						//For get and Set value
						valuesettingfunction();
					}
					else if(data[i].widgetname == 'ibdocumentswidget') {
						var charttype = "ibdocumentswidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="documentswidgetid"  data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',crmfileinfo,crmfileinfo.crmfileinfoid|filename|modulename|employeename,module|employee,moduleid|employeeid,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							documentswidgetcreation(data[i].filedname,2,'documentswidgetid','documentswidgetclass','closedocumentswidget',i);
						});
						$(".predefinedwidget#ibdocumentswidget").hide();
						//For get and Set value
						valuesettingfunction();
					}else if(data[i].widgetname == 'ibdocumentswidgetmini') {
						var charttype = "ibdocumentswidgetmini";
						var currentblockid = 'widgetcontainer'+i+'';
						//var rowid = $('#'+currentblockid+'').data('rownum');
						var rowid = 0;
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="documentswidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',crmfileinfo,crmfileinfo.crmfileinfoid|filename|modulename|employeename,module|employee,moduleid|employeeid,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							documentswidgetcreation(data[i].filedname,2,'documentswidgetid','documentswidgetclass','closedocumentswidget',i);
						});
						$(".predefinedwidget#ibdocumentswidgetmini").hide();
						//For get and Set value
						valuesettingfunction();
					} else if(data[i].widgetname == 'ibactivitieswidget') {
						var charttype = "ibactivitieswidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="activitieswidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',crmactivity,crmactivity.crmactivityid|crmactivityname|activitystartdate|activitystarttime|employeename|location,employee,employeeid,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							activitieswidgetcreation(data[i].filedname,2,'activitieswidgetid','activitieswidgetclass','closeactivitieswidget',i);
						});
						$(".predefinedwidget#ibactivitieswidget").hide();
						//For get and Set value
						valuesettingfunction();
					} else if(data[i].widgetname == 'ibnotificationwidget') {
						var charttype = "ibnotificationwidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="notificationwidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+','+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							notificationswidgetcreation(data[i].filedname,2,'notificationwidgetid','notificationwidgetclass','closenotificationwidget',i);
						});
						$(".predefinedwidget#ibnotificationwidget").hide();
						//For get and Set value
						valuesettingfunction();
					}  else if(data[i].widgetname == 'ibnotificationwidgetmini') {
						var charttype = "ibnotificationwidgetmini";
						var currentblockid = 'widgetcontainer'+i+'';
						//var rowid = $('#'+currentblockid+'').data('rownum');
						var rowid = 0;
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="notificationwidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+','+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							notificationswidgetcreation(data[i].filedname,2,'notificationwidgetid','notificationwidgetclass','closenotificationwidget',i);
						});
						$(".predefinedwidget#ibnotificationwidgetmini").hide();
						//For get and Set value
						valuesettingfunction();
					}else if(data[i].widgetname == 'ibmaillogwidget') {
						var charttype = "ibmaillogwidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="maillogwidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',crmmaillog,crmmaillog.crmmaillogid|communicationto|communicationdate,,,crmmaillog.subject,3,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							maillogwidgetcreation(data[i].filedname,2,'maillogwidgetid','maillogwidgetclass','closemaillogwidget',i);
						});
						$(".predefinedwidget#ibmaillogwidget").hide();
						//For get and Set value
						valuesettingfunction();
					} else if(data[i].widgetname == 'ibsmslogwidget') {
						var charttype = "ibsmslogwidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="smslogwidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',crmcommunicationlog,crmcommunicationlog.crmcommunicationlogid|communicationto|communicationdate,,,crmcommunicationlog.crmcommunicationmode,2,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							smslogwidgetcreation(data[i].filedname,2,'smslogwidgetid','smslogwidgetclass','closesmslogwidget',i);
						});
						$(".predefinedwidget#ibsmslogwidget").hide();
						//For get and Set value
						valuesettingfunction();
					} else if(data[i].widgetname == 'ibreceivesmslogwidget') {
						var charttype = "ibreceivesmslogwidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="receivesmslogwidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',crmcommunicationlog,crmcommunicationlog.crmcommunicationlogid|communicationto|communicationdate,,,crmcommunicationlog.crmcommunicationmode,3,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							receivesmslogwidgetcreation(data[i].filedname,2,'receivesmslogwidgetid','receivesmslogwidgetclass','receiveclosesmslogwidget',i);
						});
						$(".predefinedwidget#ibreceivesmslogwidget").hide();
						//For get and Set value
						valuesettingfunction();
					} else if(data[i].widgetname == 'ibsendsmslogwidget') {
						var charttype = "ibsendsmslogwidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0" id="sendsmslogwidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+','+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							sendsmslogwidgetcreation(data[i].filedname,2,'sendsmslogwidgetid','smslogwidgetclass','closesmslogwidget',i);
						});
						$(".predefinedwidget#ibsendsmslogwidget").hide();
						//For get and Set value
						valuesettingfunction();
					} else if(data[i].widgetname == 'ibclicktocallwidget') {
						var charttype = "ibclicktocallwidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0" id="clicktocalllogwidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+','+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							clicktocallwidgetcreation(data[i].filedname,2,'clicktocalllogwidgetid','clicktocallwidgetclass','closeclicktocallwidget',i);
						});
						$(".predefinedwidget#ibclicktocallwidget").hide();
						//For get and Set value
						valuesettingfunction();
					} else if(data[i].widgetname == 'ibinboundcalllogwidget') {
						var charttype = "ibinboundcalllogwidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="inboundcalllogwidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',calldetails,calldetails.calldetailsid|caller|tonumber|location|provider,,,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							inboundcalllogwidgetcreation(data[i].filedname,2,'inboundcalllogwidgetid','incalllogwidgetclass','inboundclosecalllogwidget',i);
						});
						$(".predefinedwidget#ibinboundcalllogwidget").hide();
						//For get and Set value
						valuesettingfunction();
					} else if(data[i].widgetname == 'iboutboundcalllogwidget') {
						var charttype = "iboutboundcalllogwidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="outboundcalllogwidget" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',callcampaign,callcampaign.callcampaignid|callcampaignname|callernumber|mobilenumber|smssendtypeid,,,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							outboundcalllogwidgetcreation(data[i].filedname,2,'outboundcalllogwidget','outcalllogwidgetclass','outboundclosecalllogwidget',i);
						});
						$(".predefinedwidget#iboutboundcalllogwidget").hide();
						//For get and Set value
						valuesettingfunction();
					} else if(data[i].widgetname == 'ibopportunitywidget') {
						var charttype = "ibopportunitywidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0" id="opportunitywidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',opportunity,opportunity.opportunityid|opportunityname|amount|expectedclosedate,,,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							opportunitywidgetcreation(data[i].filedname,2,'opportunitywidgetid','opportunitywidgetclass','closeopportunitywidget',i);
						});
						$(".predefinedwidget#ibopportunitywidget").hide();
						//For get and Set value
						valuesettingfunction();
					} else if(data[i].widgetname == 'ibcontactswidget') {
						var charttype = "ibcontactswidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="contactswidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',contact,contact.contactid|contactname|designationtypename|mobilenumber|emailid,designationtype,designationtypeid,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							contactswidgetcreation(data[i].filedname,2,'contactswidgetid','contactswidgetclass','closecontactswidget',i);
						});
						$(".predefinedwidget#ibcontactswidget").hide();
						//For get and Set value
						valuesettingfunction();
					} else if(data[i].widgetname == 'ibinvoicewidget') {
						var charttype = "ibinvoicewidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="invoicewidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',invoice,invoice.invoiceid|invoicenumber|totalnetamount|invoicedate|employeename,employee,employeeid,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							invoicewidgetcreation(data[i].filedname,2,'invoicewidgetid','invoicewidgetclass','closeinvoicewidget',i);
						});
						$(".predefinedwidget#ibinvoicewidget").hide();
						//For get and Set value
						valuesettingfunction();
					}else if(data[i].widgetname == 'ibpaymentwidget') {
						var charttype = "ibpaymentwidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="paymentwidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',payment,payment.paymentamount|paymentdate|paymentid|paymenttype.paymenttypename|paymentmethod.paymentmethodname,employee,employeeid,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							paymentwidgetcreation(data[i].filedname,2,'paymentwidgetid','paymentwidgetclass','closepaymentwidget',i);
						});
						$(".predefinedwidget#ibpaymentwidget").hide();
						//For get and Set value
						valuesettingfunction();
					} else if(data[i].widgetname == 'ibquotewidget') {
						var charttype = "ibquotewidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="quotewidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',quote,quote.quoteid|quotenumber|totalnetamount|quotedate|employeename,employee,employeeid,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							quotewidgetcreation(data[i].filedname,2,'quotewidgetid','quotewidgetclass','closequotewidget',i);
						});
						$(".predefinedwidget#ibquotewidget").hide();
						//For get and Set value
						valuesettingfunction();
					} else if(data[i].widgetname == 'ibleadstatuswidget') {
						var charttype = "ibleadstatuswidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding-top:0.4rem 0 0 0;" id="leadstatuswidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+','+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							leadstatuswidgetcreation(data[i].filedname,2,'leadstatuswidgetid','leadstatuswidgetclass','closeleadstatuswidget',i);
						});
						$(".predefinedwidget#ibleadstatuswidget").hide();
						//For get and Set value
						valuesettingfunction();
					} else if(data[i].widgetname == 'ibaccountswidget') {
						var charttype = "ibaccountswidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="accountswidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',account,accountid|accountname|employeename|sourcename,employee|source,employeeid|sourceid,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							accountswidgetcreation(data[i].filedname,2,'accountswidgetid','accountswidgetclass','closeaccountswidget',i);
						});
						$(".predefinedwidget#ibaccountswidget").hide();
						//For get and Set value
						valuesettingfunction();
					}  else if(data[i].widgetname == 'ibticketswidget') {
						var charttype = "ibticketswidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="ticketswidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',ticket,ticketid|ticketnumber|ticketdate|employeename,employee,employeeid,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							ticketswidgetcreation(data[i].filedname,2,'ticketswidgetid','ticketswidgetclass','closeticketswidget',i);
						});
						$(".predefinedwidget#ibticketswidget").hide();
						//For get and Set value
						valuesettingfunction();
					}  else if(data[i].widgetname == 'ibcampaignwidget') {
						var charttype = "ibcampaignwidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="campaignwidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',campaign,campaignid|campaignname|startdate|employeename,employee,employeeid,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							campaignwidgetcreation(data[i].filedname,2,'campaignwidgetid','campaignwidgetclass','closecampaignwidget',i);
						});
						$(".predefinedwidget#ibcampaignwidget").hide();
						//For get and Set value
						valuesettingfunction();
					}  else if(data[i].widgetname == 'ibsalesorderwidget') {
						var charttype = "ibsalesorderwidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="salesorderwidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',salesorder,salesorder.salesorderid|salesordernumber|salesorderdate|employeename|totalnetamount,employee,employeeid,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							salesorderwidgetcreation(data[i].filedname,2,'salesorderwidgetid','salesorderwidgetclass','closesalesorderwidget',i);
						});
						$(".predefinedwidget#ibsalesorderwidget").hide();
						//For get and Set value
						valuesettingfunction();
					} else if(data[i].widgetname == 'ibpurchaseorderwidget') {
						var charttype = "ibpurchaseorderwidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="purchaseorderwidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',purchaseorder,purchaseorder.purchaseorderid|purchaseordernumber|purchaseorderdate|employeename|netamount,employee,employeeid,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							purchaseorderwidgetcreation(data[i].filedname,2,'purchaseorderwidgetid','purchaseorderwidgetclass','closepurchaseorderwidget',i);
						});
						$(".predefinedwidget#ibpurchaseorderwidget").hide();
						//For get and Set value
						valuesettingfunction();
					}  else if(data[i].widgetname == 'ibsolutionwidget') {
						var charttype = "ibsolutionwidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="solutionswidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',knowledgebase,knowledgebaseid|knowledgebasenumber|knowledgebasedate|crmstatusname,crmstatus,crmstatusid,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							solutionswidgetcreation(data[i].filedname,2,'solutionswidgetid','solutionswidgetclass','closesolutionswidget',i);
						});
						$(".predefinedwidget#ibsolutionwidget").hide();
						//For get and Set value
						valuesettingfunction();
					}  else if(data[i].widgetname == 'ibsolutioncatwidget') {
						var charttype = "ibsolutioncatwidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="solutionscatwidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',knowledgebasecategory,knowledgebasecategoryid|knowledgebasecategoryname|knowledgebasecategorysuffix,,,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							solutionscatwidgetcreation(data[i].filedname,2,'solutionscatwidgetid','solutionscatwidgetclass','closesolutionscatwidget',i);
						});
						$(".predefinedwidget#ibsolutioncatwidget").hide();
						//For get and Set value
						valuesettingfunction();
					}  else if(data[i].widgetname == 'ibcontractwidget') {
						var charttype = "ibcontractwidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="contractwidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',contract,contractid|contractnumber|employeename|contractdate,employee,employeeid,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							contractwidgetcreation(data[i].filedname,2,'contractwidgetid','contractwidgetclass','closecontractwidget',i);
						});
						$(".predefinedwidget#ibcontractwidget").hide();
						//For get and Set value
						valuesettingfunction();
					} else if(data[i].widgetname == 'ibleadwidget') {  
						var charttype = "ibleadwidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="leadwidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',lead,leadid|leadname|employeename|leadnumber,employee,employeeid,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							leadwidgetcreation(data[i].filedname,2,'leadwidgetid','leadwidgetclass','closeleadwidget',i);
						});
						$(".predefinedwidget#ibleadwidget").hide();
						//For get and Set value
						valuesettingfunction();
					}  else if(data[i].widgetname == 'ibtimeanddatewidget') {
						var charttype = "ibtimeanddatewidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="timeanddatewidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+','+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							timeanddatewidgetcreation(data[i].filedname,1,'timeanddatewidgetid','timeanddatewidgetclass','closetimeanddatewidget',i);
						});
						$(".predefinedwidget#ibtimeanddatewidget").hide();
						//For get and Set value
						valuesettingfunction();
					} else if(data[i].widgetname == 'ibcalendarwidget') {
						var charttype = "ibcalendarwidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0" id="calendarwidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+','+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							calendarwidgetcreation(data[i].filedname,1,'calendarwidgetid','calendarwidgetclass','closecalendarwidget',i);
						});
						$(".predefinedwidget#ibcalendarwidget").hide();
						//For get and Set value
						valuesettingfunction();
					} else if(data[i].widgetname == 'ibconversationwidget') {
						var charttype = "ibconversationwidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="conversationwidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+','+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							conversationwidgetcreation(data[i].filedname,1,'conversationwidgetid','conversationwidgetclass','closeconversationwidget',i);
						});
						$(".predefinedwidget#ibconversationwidget").hide();
						//For get and Set value
						valuesettingfunction();
					} else if(data[i].widgetname == 'ibmaterialrequisitionwidget') {
						var charttype = "ibmaterialrequisitionwidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="materialrequisitionid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',materialrequisition,materialrequisition.materialrequisitionid|materialrequisition.netamount|materialrequisitionnumber|requisitiondate,materialrequisition,materialrequisitionid,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							materialrequisitionwidgetcreation(data[i].filedname,2,'materialrequisitionid','materialrequisitionwidgetclass','closematerialrequisitionwidget',i);
						});
						$(".predefinedwidget#ibmaterialrequisitionwidget").hide();
						//For get and Set value
						valuesettingfunction();
					} else if(data[i].widgetname == 'ibproductwidget') {
						var charttype = "ibproductwidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="productwidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',product,productid|productname|categoryname,category,categoryid,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							productwidgetcreation(data[i].filedname,2,'productwidgetid','productwidgetclass','closeproductwidget',i);
						});
						$(".predefinedwidget#ibproductwidget").hide();
						//For get and Set value
						valuesettingfunction();
					} else if(data[i].widgetname == 'ibpricebookwidget') {
						var charttype = "ibpricebookwidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum');
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end widgetclass" style="padding:0.4rem 0 0 0;" id="pricebookwidgetid" data-rowtypeid="'+i+'"><input name="chartnameinfo[]" type="hidden" value="'+rowid+','+charttype+',pricebook,pricebook.pricebookid|pricebookname|currencyname,currency,currencyid,4,'+title+'" id="chartnameinfo'+i+'" data-chartrowid="'+rowid+'"></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							pricebookwidgetcreation(data[i].filedname,2,'pricebookwidgetid','pricebookwidgetclass','closepricebookwidget',i);
						});
						$(".predefinedwidget#ibpricebookwidget").hide();
						//For get and Set value
						valuesettingfunction();
					} else if(data[i].widgetname == 'ibcustomwidget') {
						var charttype = "ibcustomwidget";
						var currentblockid = 'widgetcontainer'+i+'';
						var rowid = $('#'+currentblockid+'').data('rownum'); 
						$("#"+currentblockid+"").addClass('toblockmearge');
						$("#"+currentblockid+"").append('<div class="large-12 columns end customwidgetclassclick" style="padding:0.4rem 0 0 0;" id="customwidgetid'+i+'" data-chartcount="'+i+'"><input name="chartnameinfo[]" type="hidden" value="" id="chartnameinfo'+i+'"  data-chartcount="'+i+'" data-chartrowid="'+rowid+'" data-customrelatedmodules="" data-customdataset="" data-customwidgetlabel=""></div>');
						$(".forgetid").unbind();
						$(document).ready(function(){
							customwidgetdataset(i,data);
							customwidgetcreation(data[i].filedname,'customwidgetid','customwidgetclass','closecustomwidget',rowid,i);
						});
						//For get and Set value
						valuesettingfunction();
					}
				} 
			}
		}
	});
}
//set properties to chart text box
function updatechartelementsdataorganize(chartid,data) {
	var rowid = $('#chartnameinfo'+chartid+'').data('chartrowid');
	var charttype = data[chartid].widgetname;
	var chartview = data[chartid].viewtype;
	var selectmode = data[chartid].theme;
	var chartaspect = data[chartid].basecolor;
	var angle = data[chartid].angle;
	var stacktype = data[chartid].rotate;
	var title = data[chartid].filedname;
	var legendval = data[chartid].legend;
	var legendstyle = data[chartid].legendtype;	
	var sourcereport = data[chartid].reportid;
	var recordcount = data[chartid].countstatus;
	var datafield = data[chartid].calculationfield;
	var operation = data[chartid].operation;
	var twopointgroup = data[chartid].twopointid;
	var groupby = data[chartid].groupfiled;
	var groupsplitby = data[chartid].splitbyid;
	if(recordcount == true || recordcount == 'true') {
		var datafield = 1;		
	}		
	if(charttype == 'pie') {
		var piechartinfo = rowid+','+charttype+',chartcreated'+chartid+','+title+','+chartview+','+angle+','+legendval+','+legendstyle+','+ sourcereport+','+groupby+','+recordcount+','+datafield+','+operation;
		$('#chartnameinfo'+chartid+'').val(piechartinfo);
	}  else if(charttype == 'donut') {
		var donut = rowid+','+charttype+',chartcreated'+chartid+','+title+','+chartview+','+angle+','+legendval+','+legendstyle+','+ sourcereport+','+groupby+','+recordcount+','+datafield+','+operation;
		$('#chartnameinfo'+chartid+'').val(donut);
	} else if(charttype == 'pyramid') {
		var pyramid = rowid+','+charttype+',chartcreated'+chartid+','+title+','+selectmode+','+legendval+','+legendstyle+','+sourcereport+','+groupby+','+recordcount+','+datafield+','+operation;
		$('#chartnameinfo'+chartid+'').val(pyramid);
	} else if(charttype == 'funnel') {
		var funnel = rowid+','+charttype+',chartcreated'+chartid+','+title+','+selectmode+','+legendval+','+legendstyle+','+sourcereport+','+groupby+','+recordcount+','+datafield+','+operation;
		$('#chartnameinfo'+chartid+'').val(funnel);
	}
	else if(charttype == 'column') {
		var column = rowid+','+charttype+',chartcreated'+chartid+','+title+','+chartview+','+selectmode+','+chartaspect+','+legendval+','+legendstyle+','+sourcereport+','+groupby+','+recordcount+','+datafield+','+operation;
		$('#chartnameinfo'+chartid+'').val(column);
	}
	else if(charttype == 'stacked') {
		var stacked = rowid+','+charttype+',chartcreated'+chartid+','+title+','+selectmode+','+stacktype+','+legendval+','+legendstyle+','+sourcereport+','+groupby+','+groupsplitby+','+recordcount+','+datafield+','+operation;
		$('#chartnameinfo'+chartid+'').val(stacked);
	}
	else if(charttype == 'bubble') {
		var bubble = rowid+','+charttype+',chartcreated'+chartid+','+title+','+selectmode+','+chartaspect+','+sourcereport+','+groupby+','+groupsplitby+','+recordcount+','+datafield+','+operation;
		$('#chartnameinfo'+chartid+'').val(bubble);
	}
	else if(charttype == 'angulargauge') {
		var angulargauge = rowid+','+charttype+',chartcreated'+chartid+','+title+','+legendval+','+legendstyle+','+sourcereport+','+groupby+','+recordcount+','+datafield+','+operation;
		$('#chartnameinfo'+chartid+'').val(angulargauge);
	}
	else if(charttype == 'area') {
		var area = rowid+','+charttype+',chartcreated'+chartid+','+title+','+chartview+','+selectmode+','+chartaspect+','+legendval+','+legendstyle+','+sourcereport+','+groupby+','+recordcount+','+datafield+','+operation;
		$('#chartnameinfo'+chartid+'').val(area);
	}
	else if(charttype == 'line') {
		var line = rowid+','+charttype+',chartcreated'+chartid+','+title+','+chartview+','+selectmode+','+chartaspect+','+legendval+','+legendstyle+','+sourcereport+','+groupby+','+recordcount+','+datafield+','+operation;
		$('#chartnameinfo'+chartid+'').val(line);
	}
	else if(charttype == 'wordcloud') {
		var wordcloud = rowid+','+charttype+',chartcreated'+chartid+','+chartview+','+theme+','+angle+','+title+','+legendval+','+legendstyle+','+basecolor+','+sourcereport+','+groupby+','+recordcount+','+datafield+','+operation;
		$('#chartnameinfo'+chartid+'').val(wordcloud);
	}
}
//custom widget data set on update
function customwidgetdataset(chartid,data) {
	var rowid = $('#chartnameinfo'+chartid+'').data('chartrowid');
	var charttype = data[chartid].widgetname;
	var widgetlabel = data[chartid].filedname;
	var parenttablename = data[chartid].parenttbl;
	var customcolumns = data[chartid].legend;
	var customrelatedmodule = '';
	var formfieldsidshidden = '';
	var customchartinfo = rowid+','+charttype+','+widgetlabel+','+parenttablename+','+customcolumns+','+customrelatedmodule+','+formfieldsidshidden;
	$('#chartnameinfo'+chartid+'').val(customchartinfo);
}
// mini column widget
function addminicolumncreate(){
	       var col = 0;
		   var rowcount = 0;
			var one = col++;
			var two = col++;
			var loop = '';
			var widgetcount = $('#miniwidgetcounthidden').val();
			$("#spanforaddtab").empty();
			 $("#spanforaddtab").append("<li class='large-12 column forcontainerhover' style=''><span id='containermove"+rowcount+"' class='material-icons newsortsec' style='position: absolute; right: 2.5rem; z-index: 1;cursor:pointer;opacity:0;color:#cccccc''>drag_handle</span><span id='containerclose"+rowcount+"' class='material-icons removecontainer1' style='position: absolute; right: 1rem; z-index: 1;cursor:pointer;opacity:0;color:#cccccc''>close</span><ul id='ulid"+rowcount+"' data-ulidnum='"+rowcount+"' data-sectiontype='2'>");
			for(loop = 0 ; loop < widgetcount; loop++){
			$("#spanforaddtab").append("<li id='widgetliid"+loop+"' class='mydrag' data-widgetlinum='0'><div class='large-12 column widgetheight forgetid' id='widgetcontainer"+loop+"' data-rownum='"+loop+"'></div></li>");
			}
			$("#spanforaddtab").append("</ul><input type='hidden' id='chartrowinfo"+rowcount+"' name='chartrowinfo[]' data-chartrownum='"+rowcount+"' value='"+rowcount+",4,Home' /></li>"); 
			//To Delete Container
			$(".removecontainer1").click(function(){ 
				var getchildrenid = $(this).next().find('.widgetredirecticon').attr('id');
				$("#"+getchildrenid+"").trigger('click');
				$(this).parent('li').remove();
			});
			{//Close Hover button
				$( ".forcontainerhover" ).hover(
				  function() {
					$( this ).children('span').css('opacity','1');
				  }, function() {
					$( this ).children('span').css('opacity','0');
				  }
				);
			}
			// dashboard builder sorting
			var dbcontainer = document.getElementById("ulid"+rowcount+"");
			//new Sortable(dbcontainer);
			Sortable.create(dbcontainer, {
				animation: 150,
				group:'elementsgrp',
				draggable: '.mydrag', 
				handle: '.widgethandler',
				onSort: function (evt) {
				},
				onAdd: function (evt) {
					var dragelm = evt.item.id;
					$('#'+dragelm+'').find("div:first").removeClass("large-4 large-6").addClass("large-12");
					var oldindex =evt.oldIndex;
					
					var newindex =evt.newIndex;;
					
					//current ul id and its li elements
					var dragelmul = evt.item.parentNode.id;
					var movefirstliid =$('#'+dragelmul+' li:eq(0)').attr('id'); 
					var movesecondliid =$('#'+dragelmul+' li:eq(1)').attr('id'); 
					var movethirdliid =$('#'+dragelmul+' li:eq(2)').attr('id');
					var movefourthliid =$('#'+dragelmul+' li:eq(3)').attr('id');
					var dragfromelm = evt.from.id;
				
					
					//Sectiontype Condition Checking to drop element(li) in to from(UL)
					var sectiontype = $('#'+dragfromelm+'').data('sectiontype');
					 if(sectiontype==1 && oldindex==0 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-6").addClass("large-12");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
						//For to -- end
					}else if(sectiontype==1 && oldindex==0 && newindex==1){
						var propchangeliid = $('#'+movefirstliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-6").addClass("large-12");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
						//For to -- end
						
					}
					 //2 column to 1 column
					else if(sectiontype==2 && oldindex==0 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						//For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset);
						//For to -- end
					}else if(sectiontype==2 && oldindex==0 && newindex==1){
						var propchangeliid = $('#'+movefirstliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					} else if(sectiontype==2 && oldindex==1 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').appendTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==2 && oldindex==1 && newindex==1){
						var propchangeliid = $('#'+movefirstliid+'').appendTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==2 && oldindex==2 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==2 && oldindex==2 && newindex==1){
						var propchangeliid = $('#'+movefirstliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-4 large-12").addClass("large-6");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					} 
					// 3 column to 1 column
					else if(sectiontype==3 && oldindex==0 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==3 && oldindex==0 && newindex==1){
						var propchangeliid = $('#'+movefirstliid+'').prependTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==3 && oldindex==1 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').appendTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==3 && oldindex==1 && newindex==1){
						var propchangeli = $('#'+movefirstliid+'').appendTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==3 && oldindex==2 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').appendTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==3 && oldindex==2 && newindex==1){
						var propchangeliid = $('#'+movefirstliid+'').appendTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==3 && oldindex==3 && newindex==0){
						var propchangeliid = $('#'+movesecondliid+'').appendTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}else if(sectiontype==3 && oldindex==3 && newindex==1){
						var propchangeliid = $('#'+movefirstliid+'').appendTo('#'+dragfromelm+'').attr('id');
						$('#'+propchangeliid+'').find("div:first").removeClass("large-6 large-12").addClass("large-4");
						 //For from -- start
						var rowid = $('#'+dragfromelm+'').prependTo('#'+dragfromelm+'').attr('data-ulidnum');
						var firstdiv = $('#'+propchangeliid+'').find("div:first").attr('id');
						var seconddiv = $('#'+firstdiv+'').find("div:first").attr('id');
						var inputid = $('#'+seconddiv+'').children("input:hidden").val();
						inputid = typeof inputid == 'undefined' ? '' : inputid;
						var reset = inputid.split(",");
						reset[0] = rowid;
						$('#'+seconddiv+'').children("input:hidden").val(reset);
						//For from -- end
						//For to -- start
						var newdragelmul = dbcontainer.id;
						var newmovefirstliid =$('#'+newdragelmul+' li:eq(0)').attr('id');
						var newfirstdiv = $('#'+newmovefirstliid+'').find("div:first").attr('id');
						var newseconddiv = $('#'+newfirstdiv+'').find("div:first").attr('id');
						var newinputid = $('#'+newseconddiv+'').children("input:hidden").val();
						var newrowid = $('#'+newdragelmul+'').attr('data-ulidnum');
						newinputid = typeof newinputid == 'undefined' ? '' : newinputid;
						var newreset = newinputid.split(",");
						newreset[0] = newrowid;
						$('#'+newseconddiv+'').children("input:hidden").val(newreset); 
						//For to -- end
					}  
				},
			});
			rowcount++;
}
function miniwidgetsetheight() {
	//$('.widgetheight').css('height','100em');
}