var inci = 0;
var iden = 0;
var grp = 0;
var cner = 0;
var grpnum = 0;
$(document).ready(function(){
	var format = $('#dataformatter').val();
	var nowdate = $.datepicker.formatDate(format,new Date());
	$("#propertypanel").validationEngine();
	setTimeout(function(){
		$(".steppercategory1").trigger('click');
		$("#newmodulecrename").focus();
	},200);
	{// For Height
		builderformheight();
	}
	$( window ).resize(function() {
		builderformheight();
	});
	{// Next Step
		$("#nextstep").click(function(){
			var modulename = $("#newmodulecrename").val();
			if(modulename == '') {
				$(".stepperul2").hide();
			} else {
				$('steppercategory3').find('.toggleicons').addClass('icon24build-chevron-down');
				$('steppercategory3').find('.toggleicons').removeClass('icon24build-chevron-up');
				$(".stepperul1").slideUp('slow');
				$(".stepperul3").slideUp('slow');
				$(".stepperul2").slideDown('slow');
				$('.steppercategory2').find('.toggleicons').addClass('icon24build-chevron-up');
				$('.steppercategory2').find('.toggleicons').removeClass('icon24build-chevron-down');
				setTimeout(function(){
					//$('.steppervertical').animate({scrollTop:380},'slow');
				},200);
			}
		});	
	}
	{//Stepper toggle accordian
		$(".steppercat").click(function() {
			$(this).find('.toggleicons').addClass('icon24build-chevron-down');
			$(this).find('.toggleicons').removeClass('icon24build-chevron-up');
			var steppercatlist = $(this).data('steppercat');
			if($(".stepperul"+steppercatlist+"").hasClass("stepperulhideotherelements")) {
				$(".stepperul"+steppercatlist+"").slideUp("slow",function() {
					$(".stepperulhideotherelements").removeClass('stepperulhideotherelements');
				});
			}else {
				$(this).find('.toggleicons').removeClass('icon24build-chevron-down');
				$(this).find('.toggleicons').addClass('icon24build-chevron-up');
				$(".stepperulhideotherelements").slideUp("slow", function() {
					$('.steppercat').find('.icon24build-chevron-up').removeClass('icon24build-chevron-up').addClass('icon24build-chevron-down');
					setTimeout(function(){
						var ff = $(".stepperulhideotherelements").prev('li').find('.icon24build-chevron-down').removeClass('icon24build-chevron-down').addClass('icon24build-chevron-up');},10);
					$(".stepperulhideotherelements").removeClass('stepperulhideotherelements');
				});
				$(".stepperul"+steppercatlist+"").slideDown( "slow", function() {
					$(".stepperul"+steppercatlist+"").addClass('stepperulhideotherelements');
				});
			}
		});
	}
	{//Overlay Side Bar Code
		$(".fbuilderaccclick").click(function() {
			/*$(this).find('.toggleicons').addClass('fa-plus-square-o');
			$(this).find('.toggleicons').removeClass('fa-minus-square-o');
			var fbuildercatlist = $(this).data('elementcat');
			if($(".elementlist"+fbuildercatlist+"").hasClass( "hideotherelements" )){
				$(".elementlist"+fbuildercatlist+"").slideUp( "slow", function() {	
					$(".hideotherelements").removeClass('hideotherelements');	
				});
			} else {
				$(this).find('.toggleicons').removeClass('fa-plus-square-o');
				$(this).find('.toggleicons').addClass('fa-minus-square-o');
				$(".hideotherelements").slideUp( "slow", function() {
					$('.fbuilderaccclick').find('.fa-minus-square-o').removeClass('fa-minus-square-o').addClass('fa-plus-square-o');
					 setTimeout(function(){
                  var ff = $(".hideotherelements").prev('li').find('.fa-plus-square-o').removeClass('fa-plus-square-o').addClass('fa-minus-square-o');},10); 
				  $(".hideotherelements").removeClass('hideotherelements');
				});
				$(".elementlist"+fbuildercatlist+"").slideDown( "slow", function() {	
					$(".elementlist"+fbuildercatlist+"").addClass('hideotherelements');
				});
			}*/
		});
    }
	{//Foundation Initialization
		$(document).foundation();
	}
	{//for tabs
		$('.active span').removeAttr('style');
		$("#moduleinfoid").click(function() {	
			$('.fbtab-title span i').removeClass('icon24-w').addClass('icon24');
			$('.active').removeClass('active'); 
			$(this).addClass('active');
			$('.active span').removeAttr('style');
			$("#fbuildercat2,#fbuildercat3").hide();
			$("#fbuildercat1").show();
			$('.active span i').removeClass('icon24').addClass('icon24-w');
			Materialize.updateTextFields();
		});
		$("#formelementsid").click(function() {	
			$('.fbtab-title span i').removeClass('icon24-w').addClass('icon24');
			$('.active').removeClass('active');
			$(this).addClass('active');
			$('.active span').removeAttr('style');
			$("#fbuildercat2").show();
			$("#fbuildercat1,#fbuildercat3").hide();
			$('.active span i').removeClass('icon24').addClass('icon24-w');
			Materialize.updateTextFields();
		});	
		$("#formpropid").click(function() {	
			$('.fbtab-title span i').removeClass('icon24-w').addClass('icon24');
			$('.active').removeClass('active');
			$(this).addClass('active');
			$('.active span').removeAttr('style');
			$("#fbuildercat3").show();
			$("#fbuildercat1,#fbuildercat2").hide();
			$('.active span i').removeClass('icon24').addClass('icon24-w');
			Materialize.updateTextFields();
		});	
		$("#newmodulecrename").focusout(function() {	
			var moname = $("#newmodulecrename").val();
			if(moname == ''){
				$("#blockspan").show();
			}else{
				$("#blockspan").hide();
			}
		});
		$("#blockspan").click(function() {	
			alertpopup('Please Enter The Module Name');
		});	
	}
	{//Full Screen Preview
		$("#normalviewpreview").hide();
		$("#fullviewpreview").click(function() {	
			$(this).hide();
			$("#normalviewpreview").show();
			$("#leftsidepanel").addClass('hidedisplay');
			$("#droptarget1").removeClass('large-9 medium-9').addClass('large-12 medium-12');
			$("#previewcaption").removeClass('hidedisplay');
		});	
		$("#normalviewpreview").click(function() {	
			$(this).hide();
			$("#fullviewpreview").show();
			$("#leftsidepanel").removeClass('hidedisplay');
			$("#droptarget1").removeClass('large-12 medium-12').addClass('large-9 medium-9');
			$("#previewcaption").addClass('hidedisplay');
		});
	}
	
	var withoutgriddraggable = document.getElementById("withoutgridgen");
    withoutgriddraggable.addEventListener('dragstart', dragStart, false);
    withoutgriddraggable.addEventListener('dragend'  , dragEnd  , false);
	
	var withgriddraggable = document.getElementById("sectionwithgridgen");
    withgriddraggable.addEventListener('dragstart', dragStart, false);
    withgriddraggable.addEventListener('dragend'  , dragEnd  , false);
	
	var witheditordraggable = document.getElementById("sectionwitheditorgen");
    witheditordraggable.addEventListener('dragstart', dragStart, false);
    witheditordraggable.addEventListener('dragend'  , dragEnd  , false);
	
	var sectiondraggable = document.getElementById("sectiongen");
    sectiondraggable.addEventListener('dragstart', dragStart, false);
    sectiondraggable.addEventListener('dragend'  , dragEnd  , false);

    var droptarget = document.getElementById("droptarget1");
    droptarget.addEventListener('dragenter', dragEnter,false);
    droptarget.addEventListener('dragover', dragOver,false);
    droptarget.addEventListener('dragleave', dragLeave,false);
    droptarget.addEventListener('drop', dropc, false);

    /* Draggable event handlers */
    function dragStart(event) {
        event.dataTransfer.setData('Text', event.target.id);
    }
    function dragEnd(event) {
    }
    /* Drop target event handlers */
    function dragEnter(event) {
		//event.target.style.border = "2px dashed #ff0000";
    }
    function dragOver(event) {
		event.preventDefault();
		return false;
    }
    function dragLeave(event) {
		//event.target.style.border = "none";
    }
    function dropc(event) {
		event.preventDefault();
		var data = event.dataTransfer.getData("Text");
		if(data == 'withoutgridgen') {
			var countoftab = $('#appendtab li').length;
			if(countoftab > 0){	
				var formtype = $('#formtype').val();
				if(formtype == 1){							
					alertpopup("For Overlay Form Only One Tab Group is allowed");
				}else{
					withoutgridgen();
				}
			}else{
				withoutgridgen();
			}
			function withoutgridgen(){
				var grpdatanum = grp+1;
				if(grp == 0) {
					$("#formtype").attr("readonly","readonly");
					$("#spanforaddtab").append('<div class="large-12 column tabgroupstyle"><ul class="tabs" id="appendtab" data-tab=""><li class="tab-title sidebaricons"><span class="tabclsbtn icon24build icon24build-close"></span><span class="spantabgrpelemgen activefbuilder" contenteditable="true" data-spantabgrpelemgennum="'+grp+'">Tab Group'+grpdatanum+'</span><input name="spantabgrpinfo[]" type="hidden" id="spantabgrpinfo'+grp+'" value="1,'+grp+',Tab Group'+grpdatanum+'" /></li></ul></div><div class="large-12 column" id="forreaddtabs">&nbsp;</div><ul id="spanforaddsection'+grp+'" class="toappendlastcontainer hidecontainer appendtabcurrentplace currentab" data-spantabgrpelemnum="'+grp+'" data-tabgrptype="1"></ul>');
					//Tab group name set
					$('.spantabgrpelemgen').focusout(function(){
						var lettercount = $(this).text().length; 
						var tabid = $(this).data('spantabgrpelemgennum');
						if(lettercount == 0) {
							$(this).text('Tab Group'+(tabid+1)+'');
						}
						var tabgrpname = $(this).text();
						var type = $('#spanforaddsection'+tabid+'').data('tabgrptype');
						$('#spantabgrpinfo'+tabid+'').val(type+','+tabid+','+tabgrpname);
					});
					{//For tab Close Hover button
						$( ".sidebaricons" ).hover(
						  function() {
							$( this ).find('.tabclsbtn').css('opacity','1');
						  }, function() {
							$( this ).find('.tabclsbtn').css('opacity','0');
						  }
						);
					}
					{//for remove tab and Contect area tabclsbtn
						$(".tabclsbtn").click(function(){
							var formtype = $('#formtype').val();
							if(formtype == 1){		
								alertpopup("For Overlay Form Only One Tab Group is Allowed.You Cannot Remove This Tab Group");
							}else{
								$(this).closest("li").remove();	
								var getidfordelsection = $(this).siblings("span.spantabgrpelemgen:first").data('spantabgrpelemgennum');	
								$("#spanforaddsection"+getidfordelsection+"").remove();
								$(".spantabgrpelemgen:first").trigger('click');
								var countoftab = $('#appendtab li').size();
								if(countoftab == 0) {
									$("#spanforaddtab div #appendtab").empty();
									$("#formtype").removeAttr("readonly");
									$('#forreaddtabs').addClass('forreaddtabs');
								}
							}							
						});
					}					
					//To Prevent Line Break -- Enter Key
					$(".activefbuilder").keypress(function(e){ return e.which != 13; });
					grp++;
				} else {
					$("#formtype").attr("readonly","readonly");
					$('.activefbuilder').removeClass('activefbuilder');
					$("#appendtab").append('<li class="tab-title sidebaricons"><span class="tabclsbtn icon24build icon24build-close"></span><span class="spantabgrpelemgen activefbuilder" contenteditable="true" data-spantabgrpelemgennum="'+grp+'">Tab Group'+grpdatanum+'</span><input name="spantabgrpinfo[]" type="hidden" id="spantabgrpinfo'+grp+'" value="1,'+grp+',Tab Group'+grpdatanum+'" /></li>');
					if($('#forreaddtabs').hasClass('forreaddtabs')) {
						$('<ul id="spanforaddsection'+grp+'" class="hidecontainer hidedisplay toappendlastcontainer" data-spantabgrpelemnum="'+grp+'" data-tabgrptype="1"></ul>').insertAfter('.forreaddtabs');
					} else {
						$('<ul id="spanforaddsection'+grp+'" class="hidecontainer hidedisplay toappendlastcontainer" data-spantabgrpelemnum="'+grp+'" data-tabgrptype="1"></ul>').insertAfter('.toappendlastcontainer:last');
					}
					//Tab group name set
					$('.spantabgrpelemgen').focusout(function(){						
						var lettercount = $(this).text().length; 
						var tabgrpname = $(this).text();
						var tabid = $(this).data('spantabgrpelemgennum');
						if(lettercount == 0){
							$(this).text('Tab Group'+(tabid+1)+'');
						}
						var type = $('#spanforaddsection'+tabid+'').data('tabgrptype');
						$('#spantabgrpinfo'+tabid+'').val(type+','+tabid+','+tabgrpname);
					});		
					$(".spantabgrpelemgen").click(function(){						
						$(".hidecontainer").removeClass('currentab').addClass('hidedisplay');
						$(".appendtabcurrentplace").removeClass('appendtabcurrentplace');
						$(".activefbuilder").removeClass('activefbuilder');
						$(this).addClass('activefbuilder');
						//To Prevent Line Break -- Enter Key
						$(".activefbuilder").keypress(function(e){ return e.which != 13; });
						var tabclicksh = $(this).data('spantabgrpelemgennum');
						$("#spanforaddsection"+tabclicksh+"").addClass('appendtabcurrentplace').addClass('currentab').removeClass('hidedisplay');
						$('.spantabgrpelemgen').trigger('focusout');
					});
					//$('.spantabgrpelemgen.activefbuilder').trigger('click');
					{//For tab Close Hover button
						$( ".sidebaricons" ).hover(
						  function() {
							$( this ).find('.tabclsbtn').css('opacity','1');
						  }, function() {
							$( this ).find('.tabclsbtn').css('opacity','0');
						  }
						);
					}
					{//for remove tab and Contect area tabclsbtn
						$(".tabclsbtn").click(function(){						
							$(this).closest("li").remove();	
							var getidfordelsection = $(this).siblings("span.spantabgrpelemgen:first").data('spantabgrpelemgennum');	
							$("#spanforaddsection"+getidfordelsection+"").remove();
							$(".spantabgrpelemgen:first").trigger('click');
							var countoftab = $('#appendtab li').size();
							if(countoftab == 0){
								$("#spanforaddtab div #appendtab").empty();
								$("#formtype").removeAttr("readonly");
								$('#forreaddtabs').addClass('forreaddtabs');
							}
						});	
					}
					
					//Reorder of tabs
					//tab group sorting
					var appendtab = document.getElementById("appendtab");
					new Sortable(appendtab);
					Sortable.create(appendtab, {
						group: "tabgrp",
						animation: 150,
					});
					grp++;
					cner++;
				}
			}			
		} else if(data == 'sectionwithgridgen') { //section with grid
			if($(".appendtabcurrentplace").hasClass('currentab')) {
				grpnum = $(".currentab").data('spantabgrpelemnum');
				if($(".appendtabcurrentplace li").hasClass('forrestrict') || $(".appendtabcurrentplace li").hasClass('forrestrictsec')) {
					alertpopup("You can't do this action");
				} else {
					$(".appendtabcurrentplace").append('<li class="forrestrict"><div class="large-12 column">&nbsp;</div><div id="spanforaddelements'+inci+'" data-forsorting="'+inci+'" data-spanforaddelements="'+inci+'" class="large-4 column end paddingbtm"><div class="large-12 columns cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff"><div class="large-12 columns headerformcaptionstyle spansectionelemgen" contenteditable="true" data-spansectionelemgennum="'+inci+'">Tab Section <span class="icon24build icon24build-close secremovebtn"></span></div><input name="spantabsectinfo[]" type="hidden" id="spantabsectinfo'+inci+'" value="'+grpnum+','+inci+',Tab Section'+'" /><ul class="sortable" id="sortable'+inci+'"> </ul></div><div class="large-12 column" style="background: #f5f5f5;margin-top:0.01rem">&nbsp;</div></div><div class="large-8 column"><img src="'+base_url+'img/builders/samplegrid.jpg"/></li>');
					
					//template type define
					var tabtype = $(".currentab").data('spantabgrpelemnum');
					$('#spanforaddsection'+tabtype+'').data('tabgrptype',2);
					$('.spantabgrpelemgen').trigger('focusout');
					
					/* For Delete Section */
					$(".secremovebtn").click(function(){
						var removesection = $(this).closest('div').data('spansectionelemgennum');
						$("#spanforaddelements"+removesection+"").parent('li').remove();
					});
					
					//tab section sorting - section with grid
					var tabsectionele = document.getElementById("sortable"+inci+"");
					Sortable.create(tabsectionele, {
						group: 'secgridelem'
					});
					
					$(document).ready(function() {
						//section name set
						$('.spansectionelemgen').focusout(function(){
							var tabsecname = $(this).text();
							var scid = $(this).data('spansectionelemgennum');
							grpnum = $(".currentab").data('spantabgrpelemnum');
							$('#spantabsectinfo'+scid+'').val(grpnum+','+scid+','+tabsecname);
						});
						var textboxdraggable = document.getElementById("textboxgen");
						textboxdraggable.addEventListener('dragstart', dragStartx, false);
						textboxdraggable.addEventListener('dragend'  , dragEndx  , false);
						
						var textareadraggable = document.getElementById("textareagen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("integerboxgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("decimalboxgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("percentgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("currencygen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("dategen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("timegen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("emailgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("phonegen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("urlgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("checkboxgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("autonumbergen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var dropdowndraggable = document.getElementById("dropdowngen");
						dropdowndraggable.addEventListener('dragstart', dragStart, false);
						dropdowndraggable.addEventListener('dragend'  , dragEnd  , false);
				
						var dropdowndraggable = document.getElementById("passwordgen");
						dropdowndraggable.addEventListener('dragstart', dragStart, false);
						dropdowndraggable.addEventListener('dragend'  , dragEnd  , false);
				
						var dropdowndraggable = document.getElementById("texteditorgen");
						dropdowndraggable.addEventListener('dragstart', dragStart, false);
						dropdowndraggable.addEventListener('dragend'  , dragEnd  , false);
						/* Draggable event handlers */
						function dragStartx(event) {
							event.dataTransfer.setData('Text', event.target.id);
						}
						function dragEndx(event) {
						}
						var sdroptarget = document.getElementById("spanforaddelements"+inci+"");
						sdroptarget.addEventListener('dragenter', dragEnter  , false);
						sdroptarget.addEventListener('dragover' , dragOver   , false);
						sdroptarget.addEventListener('dragleave', dragLeave  , false);
						sdroptarget.addEventListener('drop',drop,false);
						
						/* Drop target event handlers */
						function dragEnter(event) {
							//event.target.style.border = "2px dashed #ff0000";
						}

						function dragOver(event) {
							event.preventDefault(); 
							return false;
						}
						function dragLeave(event) {
							//event.target.style.border = "none";
						}
						//drop function call
						function drop(event) {
							event.preventDefault(); 
							var data = event.dataTransfer.getData("Text");
							var setdataid =  $(this).attr('id'); 
							var secids = $('#'+setdataid+'').data('spanforaddelements');
							if(data == 'textboxgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Text</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="2" data-datasectionid="'+secids+'" value="'+secids+',2,Text,,100,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id'); 
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'textareagen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Comments</label><textarea id="frmliveelm'+iden+'" rows="3"></textarea><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="3" data-datasectionid="'+secids+'" value="'+secids+',3,Comments,,100,3,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var tareasortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'integerboxgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Integer</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="4" data-datasectionid="'+secids+'" value="'+secids+',4,Integer,,100,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'decimalboxgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Decimal</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="5" data-datasectionid="'+secids+'" value="'+secids+',5,Decimal,,100,2,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'percentgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Percent</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="6" data-datasectionid="'+secids+'" value="'+secids+',6,Percent,,100,2,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
								
							} else if(data == 'currencygen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Currency</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="7" data-datasectionid="'+secids+'" value="'+secids+',7,Currency,,100,2,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'dategen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Date</label><input type="text" id="frmliveelm'+iden+'" value="'+nowdate+'"><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="8" data-datasectionid="'+secids+'" value="'+secids+',8,Date,'+nowdate+',,No,Yes,No,No,Yes,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'timegen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Time</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="9" data-datasectionid="'+secids+'" value="'+secids+',9,Time,,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'emailgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">EMail</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="10" data-datasectionid="'+secids+'" value="'+secids+',10,EMail,,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'phonegen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Phone</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="11" data-datasectionid="'+secids+'" value="'+secids+',11,Phone,,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'urlgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">URL</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="12" data-datasectionid="'+secids+'" value="'+secids+',12,URL,,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'checkboxgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><input type="checkbox" id="frmliveelm'+iden+'" value="" class="filled-in"><label for="frmliveelm'+iden+'" id="frmelemlivespantit'+iden+'">Check Box</label><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="13" data-datasectionid="'+secids+'" value="'+secids+',13,Check Box,,No,Yes,No,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'autonumbergen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Auto Number</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="14" data-datasectionid="'+secids+'" value="'+secids+',14,Auto Number,,,,No,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'attachmentgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Attachment</label><img src="'+base_url+'img/imgupload.jpg" class=""/><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="15" data-datasectionid="'+secids+'" value="'+secids+',15,Attachment,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'imgattachgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Image Upload</label><img src="'+base_url+'img/imgupload.jpg" class=""/><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="16" data-datasectionid="'+secids+'" value="'+secids+',16,Image Upload,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'dropdowngen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Picklist</label><select data-placeholder="Select" class="chzn-select" id="frmliveelm'+iden+'"></select><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="17" data-datasectionid="'+secids+'" value="'+secids+',17,Picklist,,,,No,Yes,No,No,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var ddsortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'passwordgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Password</label><input type="password" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="22" data-datasectionid="'+secids+'" value="'+secids+',22,Password,,100,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'texteditorgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Text Editor</label><div id="frmliveelm'+iden+'"><img src="'+base_url+'img/texteditor.jpg" class=""/> </div><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="24" data-datasectionid="'+secids+'" value="'+secids+',24,Text Editor,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'ddrelationgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Relation</label><select data-placeholder="Select" class="chzn-select" id="frmliveelm'+iden+'"></select><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="26" data-datasectionid="'+secids+'" value="'+secids+',26,Relation,,,,,,,,No,Yes,No,No,No"></div></li>');
								//For Remove Form Elements 
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							}
							iden++;
							//div click for properties
							$('.frmelediv').find('input, textarea, button, select').attr('disabled','disabled');
							$('.frmelediv').click(function(){
								$('.columnselectionclass').removeClass('columnselectionclass');
								$(this).addClass('columnselectionclass');
								var id = $(this).data('frmeledivattr');
								$('#elemidentiryid').val(id);
								clearform('propertiesclear');
								defaultoptionsset();
								setTimeout(function(){
									fromelementsvaluereorganize(id); 
									Materialize.updateTextFields();
								},200);
								$('#formpropid').trigger('click');
								setTimeout(function(){$('#frmelemattrlablname').focus().setCursorPosition(0);},300);
								$('#propertypanel').css('display','block');
								$(".propertytext").addClass('hidedisplay');
								//Toggle Property pannel
								propertyslidetoggle();
							});
							return false;
						}
						inci++;
					});	
				}
			}
		} else if(data == 'sectionwitheditorgen') { //section with editor
			if($(".appendtabcurrentplace").hasClass('currentab')) {
				grpnum = $(".currentab").data('spantabgrpelemnum');
				if($(".appendtabcurrentplace li").hasClass('forrestrict') || $(".appendtabcurrentplace li").hasClass('forrestrictsec')) {
					alertpopup("You can't do this action");
				} else {
					$(".appendtabcurrentplace").append('<li class="forrestrict"><div class="large-12 column">&nbsp;</div><div id="spanforaddelements'+inci+'" data-forsorting="'+inci+'" data-spanforaddelements="'+inci+'" class="large-4 column end paddingbtm"><div class="large-12 columns cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff"><div class="large-12 columns headerformcaptionstyle spansectionelemgen" contenteditable="true" data-spansectionelemgennum="'+inci+'">Tab Section <span class="icon24build icon24build-close secremovebtn"></span></div><input name="spantabsectinfo[]" type="hidden" id="spantabsectinfo'+inci+'" value="'+grpnum+','+inci+',Tab Section'+'" /><ul class="sortable" id="sortable'+inci+'"> </ul></div><div class="large-12 column" style="background: #f5f5f5;margin-top:0.01rem">&nbsp;</div></div><div class="large-8 column"><img src="'+base_url+'img/builders/sampleeditor.jpg"/></li>');
					
					//template type define
					var tabtype = $(".currentab").data('spantabgrpelemnum');
					$('#spanforaddsection'+tabtype+'').data('tabgrptype',3);
					$('.spantabgrpelemgen').trigger('focusout');
					
					/* For Delete Section */
					$(".secremovebtn").click(function(){
						var removesection = $(this).closest('div').data('spansectionelemgennum');
						$("#spanforaddelements"+removesection+"").parent('li').remove();
					});
					//tab section sorting - section with editor
					var tabsectionele = document.getElementById("sortable"+inci+"");
					Sortable.create(tabsectionele, {
						group: 'seceditorelem'
					});
					
					$(document).ready(function(){
						//section name set
						$('.spansectionelemgen').focusout(function(){
							var tabsecname = $(this).text();
							var scid = $(this).data('spansectionelemgennum');
							grpnum = $(".currentab").data('spantabgrpelemnum');
							$('#spantabsectinfo'+scid+'').val(grpnum+','+scid+','+tabsecname);
						});
						var textboxdraggable = document.getElementById("textboxgen");
						textboxdraggable.addEventListener('dragstart', dragStartx, false);
						textboxdraggable.addEventListener('dragend'  , dragEndx  , false);
						
						var textareadraggable = document.getElementById("textareagen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("integerboxgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("decimalboxgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("percentgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("currencygen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("dategen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("timegen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("emailgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("phonegen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("urlgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("checkboxgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("autonumbergen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var dropdowndraggable = document.getElementById("dropdowngen");
						dropdowndraggable.addEventListener('dragstart', dragStart, false);
						dropdowndraggable.addEventListener('dragend'  , dragEnd  , false);
				
						var dropdowndraggable = document.getElementById("passwordgen");
						dropdowndraggable.addEventListener('dragstart', dragStart, false);
						dropdowndraggable.addEventListener('dragend'  , dragEnd  , false);
				
						var dropdowndraggable = document.getElementById("texteditorgen");
						dropdowndraggable.addEventListener('dragstart', dragStart, false);
						dropdowndraggable.addEventListener('dragend'  , dragEnd  , false);
						/* Draggable event handlers */
						function dragStartx(event) {
							event.dataTransfer.setData('Text', event.target.id);
						}
						function dragEndx(event) {
						}
						var sdroptarget = document.getElementById("spanforaddelements"+inci+"");
						sdroptarget.addEventListener('dragenter', dragEnter  , false);
						sdroptarget.addEventListener('dragover' , dragOver   , false);
						sdroptarget.addEventListener('dragleave', dragLeave  , false);
						sdroptarget.addEventListener('drop',drop,false);
						
						/* Drop target event handlers */
						function dragEnter(event) {
							//event.target.style.border = "2px dashed #ff0000";
						}

						function dragOver(event) {
							event.preventDefault();
							return false;
						}
						function dragLeave(event) {
							//event.target.style.border = "none";
						}
						//drop function call
						function drop(event) {
							event.preventDefault(); 
							var data = event.dataTransfer.getData("Text");
							var setdataid =  $(this).attr('id'); 
							var secids = $('#'+setdataid+'').data('spanforaddelements');
							if(data == 'textboxgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Text</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="2" data-datasectionid="'+secids+'" value="'+secids+',2,Text,,100,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id'); 
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'textareagen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Comments</label><textarea id="frmliveelm'+iden+'" rows="3"></textarea><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="3" data-datasectionid="'+secids+'" value="'+secids+',3,Comments,,100,3,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var tareasortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'integerboxgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Integer</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="4" data-datasectionid="'+secids+'" value="'+secids+',4,Integer,,100,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'decimalboxgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Decimal</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="5" data-datasectionid="'+secids+'" value="'+secids+',5,Decimal,,100,2,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'percentgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Percent</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="6" data-datasectionid="'+secids+'" value="'+secids+',6,Percent,,100,2,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'currencygen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Currency</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="7" data-datasectionid="'+secids+'" value="'+secids+',7,Currency,,100,2,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'dategen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Date</label><input type="text" id="frmliveelm'+iden+'" value="'+nowdate+'"><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="8" data-datasectionid="'+secids+'" value="'+secids+',8,Date,'+nowdate+',,No,Yes,No,No,Yes,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'timegen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Time</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="9" data-datasectionid="'+secids+'" value="'+secids+',9,Time,,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'emailgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">EMail</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="10" data-datasectionid="'+secids+'" value="'+secids+',10,EMail,,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'phonegen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Phone</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="11" data-datasectionid="'+secids+'" value="'+secids+',11,Phone,,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'urlgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">URL</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="12" data-datasectionid="'+secids+'" value="'+secids+',12,URL,,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
								
							} else if(data == 'checkboxgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><input type="checkbox" id="frmliveelm'+iden+'" value="" class="filled-in"><label for="frmliveelm'+iden+'" id="frmelemlivespantit'+iden+'">Check Box</label><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="13" data-datasectionid="'+secids+'" value="'+secids+',13,Check Box,,No,Yes,No,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'autonumbergen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Auto Number</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="14" data-datasectionid="'+secids+'" value="'+secids+',14,Auto Number,,,,No,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'attachmentgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Attachment</label><img src="'+base_url+'img/imgupload.jpg" class=""/><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="15" data-datasectionid="'+secids+'" value="'+secids+',15,Attachment,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'imgattachgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Image Upload</label><img src="'+base_url+'img/imgupload.jpg" class=""/><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="16" data-datasectionid="'+secids+'" value="'+secids+',16,Image Upload,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'dropdowngen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Picklist</label><select data-placeholder="Select" class="chzn-select" id="frmliveelm'+iden+'"></select><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="17" data-datasectionid="'+secids+'" value="'+secids+',17,Picklist,,,,No,Yes,No,No,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var ddsortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'passwordgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Password</label><input type="password" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="22" data-datasectionid="'+secids+'" value="'+secids+',22,Password,,100,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'texteditorgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Text Editor</label><div id="frmliveelm'+iden+'"><img src="'+base_url+'img/texteditor.jpg" class=""/> </div><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="24" data-datasectionid="'+secids+'" value="'+secids+',24,Text Editor,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'ddrelationgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Relation</label><select data-placeholder="Select" class="chzn-select" id="frmliveelm'+iden+'"></select><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="26" data-datasectionid="'+secids+'" value="'+secids+',26,Relation,,,,,,,,No,Yes,No,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							}
							iden++;
							//div click for properties
							$('.frmelediv').find('input, textarea, button, select').attr('disabled','disabled');
							$('.frmelediv').click(function(){
								$('.columnselectionclass').removeClass('columnselectionclass');
								$(this).addClass('columnselectionclass');
								var id = $(this).data('frmeledivattr');
								$('#elemidentiryid').val(id);
								clearform('propertiesclear');
								defaultoptionsset();
								setTimeout(function(){
									fromelementsvaluereorganize(id); 
									Materialize.updateTextFields();
								},200);
								$('#formpropid').trigger('click');
								setTimeout(function(){
								$('#frmelemattrlablname').focus().setCursorPosition(0);},300);
								$('#propertypanel').css('display','block');
								$(".propertytext").addClass('hidedisplay');
								//Toggle Property pannel
								propertyslidetoggle();
								
							});
							return false;
						}
						inci++;
					});	
				}
			}
		} else if(data == 'sectiongen') { //section
			if($(".appendtabcurrentplace").hasClass('currentab')) {
				grpnum = $(".currentab").data('spantabgrpelemnum');
				if($(".appendtabcurrentplace li").hasClass('forrestrict')) {
					alertpopup("You can't do this action");
				} else {					
					$(".appendtabcurrentplace").append('<li class="forrestrictsec newsortsec"><div class="large-4 column paddingbtm end forselectsection"><div class="large-12 columns cleardataform ddddd" style="padding-left: 0 !important; padding-right: 0 !important;background: #ffffff" id="spanforaddelements'+inci+'" data-spanforaddelements="'+inci+'" data-forsorting="'+inci+'"><div class="large-12 columns headerformcaptionstyle spansectionelemgen" contenteditable="true" data-spansectionelemgennum="'+inci+'">Tab Section </div><span class="icon24build icon24build-close secremovebtn"></span><input name="spantabsectinfo[]" type="hidden" id="spantabsectinfo'+inci+'" value="'+grpnum+','+inci+',Tab Section" /><ul class="sortable" id="sortable'+inci+'"> </ul></div><div class="large-12 column" style="background: #f5f5f5;margin-top:0.01rem">&nbsp;</div></div></li>');
					/* Hover for remove span */
					$( "#spanforaddelements"+inci+"" ).hover(
						function() {
						$( this ).find("span:first").css("display","block");	
							 
						}, function() {
						$( this ).find("span:first").css("display","none");	
					});
					/* For Delete Section */
					$(".secremovebtn").click(function(){
						var removesection = $(this).parent('div').data('spanforaddelements');
						$("#spanforaddelements"+removesection+"").parent('.forselectsection').parent('li').remove();
					});
					
					//tab section sorting - without grid or editor
					var tabsection = document.getElementById("spanforaddsection"+grpnum+"");
					new Sortable(tabsection);
					Sortable.create(tabsection, {
						animation: 150,
						group:'tabsection',
						draggable: '.newsortsec',
						handle: '.spansectionelemgen',
						store: {
							get: function (sortable) {
								var order = localStorage.getItem(sortable.options.group);
								return order ? order.split('|') : [];
							},
							set: function (sortable) {
								var order = sortable.toArray();
								localStorage.setItem(sortable.options.group, order.join('|'));
							}
						},
					});
					//tab section elements sorting - only section elements
					var tabsectionele = document.getElementById("sortable"+inci+"");
					Sortable.create(tabsectionele, {
						group: 'sectionelement'
					});
					//for section Selection
					$("#spanforaddelements"+inci+"").click(function(){
						$('.sectionselector').removeClass('sectionselector');
						$(this).closest('.forselectsection').addClass('sectionselector');
					});
					//template type define
					var tabtype = $(".currentab").data('spantabgrpelemnum');
					$('#spanforaddsection'+tabtype+'').data('tabgrptype',1);
					$('.spantabgrpelemgen').trigger('focusout');
					
					$(document).ready(function(){
						//section name set
						$('.spansectionelemgen').focusout(function(){
							var tabsecname = $(this).text();
							var scid = $(this).data('spansectionelemgennum');
							grpnum = $(".currentab").data('spantabgrpelemnum');
							$('#spantabsectinfo'+scid+'').val(grpnum+','+scid+','+tabsecname);
						});
						var textboxdraggable = document.getElementById("textboxgen");
						textboxdraggable.addEventListener('dragstart', dragStartx, false);
						textboxdraggable.addEventListener('dragend'  , dragEndx  , false);
						
						var textareadraggable = document.getElementById("textareagen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("integerboxgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("decimalboxgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("percentgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("currencygen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("dategen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("timegen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("emailgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("phonegen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("urlgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("checkboxgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("autonumbergen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var dropdowndraggable = document.getElementById("dropdowngen");
						dropdowndraggable.addEventListener('dragstart', dragStart, false);
						dropdowndraggable.addEventListener('dragend'  , dragEnd  , false);
				
						var dropdowndraggable = document.getElementById("passwordgen");
						dropdowndraggable.addEventListener('dragstart', dragStart, false);
						dropdowndraggable.addEventListener('dragend'  , dragEnd  , false);
				
						var dropdowndraggable = document.getElementById("texteditorgen");
						dropdowndraggable.addEventListener('dragstart', dragStart, false);
						dropdowndraggable.addEventListener('dragend'  , dragEnd  , false);
						
						/* Draggable event handlers */
						function dragStartx(event) {
							event.dataTransfer.setData('Text', event.target.id);
						}
						function dragEndx(event) {
						}
						var sdroptarget = document.getElementById("spanforaddelements"+inci+"");
						sdroptarget.addEventListener('dragenter', dragEnter  , false);
						sdroptarget.addEventListener('dragover' , dragOver   , false);
						sdroptarget.addEventListener('dragleave', dragLeave  , false);
						sdroptarget.addEventListener('drop',drop,false);
						
						/* Drop target event handlers */
						function dragEnter(event) {
							//event.target.style.border = "2px dashed #ff0000";
						}
						function dragOver(event) {
							event.preventDefault();
							return false;
						}
						function dragLeave(event) {
							//event.target.style.border = "none";
						}
						//drop function call
						function drop(event) {
							event.preventDefault(); 
							var data = event.dataTransfer.getData("Text");
							var setdataid =  $(this).attr('id');  
							var secids = $('#'+setdataid+'').data('spanforaddelements');
							if(data == 'textboxgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Text</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="2" data-datasectionid="'+secids+'" value="'+secids+',2,Text,,100,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id'); 
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'textareagen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Comments</label><textarea id="frmliveelm'+iden+'" rows="3"></textarea><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="3" data-datasectionid="'+secids+'" value="'+secids+',3,Comments,,100,3,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var tareasortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'integerboxgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Integer</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="4" data-datasectionid="'+secids+'" value="'+secids+',4,Integer,,100,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'decimalboxgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Decimal</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="5" data-datasectionid="'+secids+'" value="'+secids+',5,Decimal,,100,2,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'percentgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Percent</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="6" data-datasectionid="'+secids+'" value="'+secids+',6,Percent,,100,2,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'currencygen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Currency</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="7" data-datasectionid="'+secids+'" value="'+secids+',7,Currency,,100,2,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'dategen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Date</label><input type="text" id="frmliveelm'+iden+'" value="'+nowdate+'"><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="8" data-datasectionid="'+secids+'" value="'+secids+',8,Date,'+nowdate+',,No,Yes,No,No,Yes,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'timegen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Time</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="9" data-datasectionid="'+secids+'" value="'+secids+',9,Time,,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'emailgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">EMail</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="10" data-datasectionid="'+secids+'" value="'+secids+',10,EMail,,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'phonegen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Phone</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="11" data-datasectionid="'+secids+'" value="'+secids+',11,Phone,,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'urlgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">URL</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="12" data-datasectionid="'+secids+'" value="'+secids+',12,URL,,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'checkboxgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><input type="checkbox" id="frmliveelm'+iden+'" value="" class="filled-in"><label for="frmliveelm'+iden+'" id="frmelemlivespantit'+iden+'">Check Box</label><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="13" data-datasectionid="'+secids+'" value="'+secids+',13,Check Box,,No,Yes,No,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'autonumbergen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Auto Number</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="14" data-datasectionid="'+secids+'" value="'+secids+',14,Auto Number,,,,No,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'attachmentgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Attachment</label><img src="'+base_url+'img/imgupload.jpg" class=""/><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="15" data-datasectionid="'+secids+'" value="'+secids+',15,Attachment,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'imgattachgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Image Upload</label><img src="'+base_url+'img/imgupload.jpg" class=""/><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="16" data-datasectionid="'+secids+'" value="'+secids+',16,Image Upload,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'dropdowngen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Picklist</label><select data-placeholder="Select" class="chzn-select" id="frmliveelm'+iden+'"></select><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="17" data-datasectionid="'+secids+'" value="'+secids+',17,Picklist,,,,No,Yes,No,No,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var ddsortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'passwordgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Password</label><input type="password" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="22" data-datasectionid="'+secids+'" value="'+secids+',22,Password,,100,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'texteditorgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Text Editor</label><div id="frmliveelm'+iden+'"><img src="'+base_url+'img/texteditor.jpg" class=""/> </div><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="24" data-datasectionid="'+secids+'" value="'+secids+',24,Text Editor,,No,Yes,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							} else if(data == 'ddrelationgen') {
								$(this).find('ul').append('<li><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-close2 elementremovebtn" title="Remove"></span></span><label id="frmelemlivespantit'+iden+'">Relation</label><select data-placeholder="Select" class="chzn-select" id="frmliveelm'+iden+'"></select><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="26" data-datasectionid="'+secids+'" value="'+secids+',26,Relation,,,,,,,,No,Yes,No,No,No"></div></li>');
								//For Remove Form Elements
								forremoveformelements();
								//For Sorting Elements
								var getclassforsorting = $(this).attr('id');
								var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									
							}
							iden++;
							//div click for properties
							$('.frmelediv').find('input, textarea, button, select').attr('disabled','disabled');
							$('.frmelediv').click(function(){
								$('.columnselectionclass').removeClass('columnselectionclass');
								$(this).addClass('columnselectionclass');
								var id = $(this).data('frmeledivattr');
								$('#elemidentiryid').val(id);
								clearform('propertiesclear');
								defaultoptionsset();
								setTimeout(function(){
									fromelementsvaluereorganize(id); 
									Materialize.updateTextFields();
								},200);
								$('#formpropid').trigger('click');
								setTimeout(function(){
								$('#frmelemattrlablname').focus().setCursorPosition(0);},300);
								$('#propertypanel').css('display','block');
								$(".propertytext").addClass('hidedisplay');
								//Toggle Property pannel
								propertyslidetoggle();
							});
							return false;
						}
						inci++;
					});	
				}
			}
			return false;
		}
    }
	{//check box event
		//Mandatory,Active,Multiple option,Check box set default check box
		$('#fieldactive,#addalphasortorder').click(function() {
			var attrid = $('#elemidentiryid').val();
			if ($(this).is(':checked')) {
				$(this).val('Yes');
				fromelementsvalueorganize(attrid);
			} else {
				$(this).val('No');
				fromelementsvalueorganize(attrid);
			}
		});
		//Read only Check box set default check box
		$('#fieldreadonly').click(function() {
			var attrid = $('#elemidentiryid').val();
			if ($(this).is(':checked')) {
				$(this).val('Yes');
				$("#frmliveelm"+attrid+"").attr('readonly','readonly').css('background','#dddddd');
				fromelementsvalueorganize(attrid);
			} else {
				$(this).val('No');
				$("#frmliveelm"+attrid+"").removeAttr('readonly').css('background','#ffffff');
				fromelementsvalueorganize(attrid);
			}
		});
		//Mandatory
		$('#fildmandatory').click(function() {
			var attrid = $('#elemidentiryid').val();
			if ($(this).is(':checked')) {
				$(this).val('Yes');
				fromelementsvalueorganize(attrid);
				$("#frmelemlivespantit"+attrid+"").append('<span id="mandfildid'+attrid+'" class="mandatoryfildclass">*</span>');
			} else {
				$(this).val('No');
				fromelementsvalueorganize(attrid);
				$('#mandfildid'+attrid+'').remove();
			}
		});
		//multiple option
		$('#addfieldmultiple').click(function() {
			var attrid = $('#elemidentiryid').val();
			if ($(this).is(':checked')) {
				$(this).val('Yes');
				$("#frmliveelm"+attrid+"").attr('multiple','multiple');
				fromelementsvalueorganize(attrid);
			} else {
				$(this).val('No');
				$("#frmliveelm"+attrid+"").removeAttr('multiple');
				fromelementsvalueorganize(attrid);
			}
		});
		//for column type
		$('#fieldcolumntype').click(function(){
			var attrid = $('#elemidentiryid').val();
			if ($(this).is(':checked')) {
				$(this).val('Yes');
				$("#columndivid"+attrid+"").removeClass('large-12').addClass('large-6 end');
				fromelementsvalueorganize(attrid);
			} else {
				$(this).val('No');
				$("#columndivid"+attrid+"").removeClass('large-6 end').addClass('large-12');
				fromelementsvalueorganize(attrid);
			}
		});
		//check box default checked option set
		$('#addfieldsetdef').click(function() {
			var attrid = $('#elemidentiryid').val();
			if ($(this).is(':checked')) {
				$(this).val('Yes');
				$("#frmliveelm"+attrid+"").prop('checked', true);
				fromelementsvalueorganize(attrid);
			} else {
				$(this).val('No');
				$("#frmliveelm"+attrid+"").prop('checked', false);
				fromelementsvalueorganize(attrid);
			}
		});
		//Unique Check box validation
		$('#featuredate,#pastdate').click(function() {
			var attrid = $('#elemidentiryid').val();
			if ($(this).is(':checked')) {
				$(this).val('Yes');
				fromelementsvalueorganize(attrid);
			} else {
				$(this).val('No');
				fromelementsvalueorganize(attrid);
			}
		});
	}
	{//properties set
		//label
		$('#frmelemattrlablname').change(function(){
			var attrid = $('#elemidentiryid').val();
			var txt = $(this).val();
			$('#frmelemlivespantit'+attrid+'').text(txt);
			$('#fildmandatory').trigger('click');
			$('#fildmandatory').trigger('click');
			//fildmandatory
			fromelementsvalueorganize(attrid);
		});
		//default value
		$('#frmelemattrdefname').change(function(){
			var attrid = $('#elemidentiryid').val();
			var txt = $(this).val();
			$('#frmliveelm'+attrid+'').val(txt);
			fromelementsvalueorganize(attrid);
		});
		//date change event
		$('#dateaddfielddefval').change(function(){
			var attrid = $('#elemidentiryid').val();
			var txt = $("#dateaddfielddefval").val();
			$('#frmliveelm'+attrid+'').val(txt);
			fromelementsvalueorganize(attrid);
		});
		//time default value
		$('#timeaddfielddefval').change(function(){
			var attrid = $('#elemidentiryid').val();
			var txt = $("#timeaddfielddefval").val();
			$('#frmliveelm'+attrid+'').val(txt);
			fromelementsvalueorganize(attrid);
		});
		//length
		$('#frmelemattrlength').change(function(){
			var val = $('#frmelemattrlength').val();
			var pattern = new RegExp(/^[0-9\ ]+$/);
			var match = pattern.exec(val);
			if (match == null) {
				$('#frmelemattrlength').val('100');
			}
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//decimal
		$('#frmelemdecilength').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//description
		$('#frmelemattrdesc').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//prefix change event
		$('#frmelemprefix').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//auto start number change event
		$('#frmelemstartnum').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//suffix change event
		$('#frmelemsuffix').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//text area rows
		$('#frmelemattrrows').change(function(){
			var attrid = $('#elemidentiryid').val();
			var rows = $(this).val();
			$('#frmliveelm'+attrid+'').removeAttr("rows");
			$('#frmliveelm'+attrid+'').attr("rows", rows);
			fromelementsvalueorganize(attrid);
		});
		//automatic number type
		$('#attrautonumbertype').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//drop down value set
		$("#frmelemdatavalue").select2({
			tags:[''],
		});
		//drop down value change event
		$('#frmelemdatavalue').change(function(){
			var select = $('#frmliveelm'+attrid+'');
			select.children().remove();
			$('#dddefaultvalue').children().remove();
			var ddndatas = $("#frmelemdatavalue").val();
			var ddsdatas = ddndatas.split(',');
			$('#dddefaultvalue').append($("<option>").val('').text(''));
			for(var hh=0;hh < ddsdatas.length;hh++) {
				select.append($("<option>").val(ddsdatas[hh]).text(ddsdatas[hh]));
				$('#dddefaultvalue').append($("<option>").val(ddsdatas[hh]).text(ddsdatas[hh]));
			}
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//relation module name change event
		$('#attrmodulename').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
			var id=$('#attrmodulename').find('option:selected').data('datamoduleid');
			dropdownmodulefilednameset(id);
		});
		//drop down default value change event
		$('#dddefaultvalue').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//module action assign
		$('#moduleactionname').change(function(){
			var valids = $(this).val();
			$('#modactionslist').val(valids);
			$('.viewformvalidation').validationEngine('hide');
			$("#s2id_moduleactionname,.select2-results").removeClass('error');
			$("#s2id_moduleactionname").find('ul').removeClass('error');
		});
		//field name change event
		$('#frmdispelefiledname').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//condition field name change event
		$('#frmelefiledname').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//Condition change event
		$('#frmelecondname').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//condition value change event
		$('#frmelmcondval').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
	}
	{
		//module save
		$('#modbuildersavebtn').click(function(){
			$("#normalviewpreview").trigger('click');
			var checkstepper1 = $('.stepperul1').hasClass('stepperulhideotherelements'); 
			if(checkstepper1 == false){
				$(".steppercategory1").trigger('click');
			}
			$('#moduleinfoid').trigger('click');
			$("#formmodulenamvalidate").validationEngine('validate');
		});
		$("#formmodulenamvalidate").validationEngine( {
			onSuccess: function() {
				moduledatacreate();
			},
			onFailure: function() {				
				$("#s2id_moduleactionname").find('ul').addClass('error');
				alertpopup(validationalert);
			}
		});
		//content delete
		$('#modulebuilddelicon').click(function(){
			$('#moduleinfoid').trigger('click');
			$('.frmelediv').find('input, textarea, button, select').attr('disabled','disabled');
			$('#spanforaddtab').empty();
			inci = 0;
			iden = 0;
			grp = 0;
			cner = 0;
			$('#newmodulecrename').val('');
			$('#moduleactionname').select2("val", "");
			$("#formtype").select2('val','').removeAttr("readonly");
			$("#elementsgroups").hide();
		});
	}
	//show hide elementsgroup
	{
		$('#formtype').on('change', function() {					
			if ( this.value == '1')
			{
				$("#elementsgroups").show();
				$("#sectiongen").hide();
				$("#sectionwitheditorgen").hide();
			}
			else if( this.value == '2'){
				$("#elementsgroups").show();
				$("#sectiongen").show();
				$("#sectionwitheditorgen").show();
			}else{
				$("#elementsgroups").hide();
			}				     
		});		
	}
});
//form elements value organize
function fromelementsvalueorganize(attrid) {
	var uitypeid = $('#frmelmattrinfo'+attrid+'').data('frmeleuitypeattr');
	var secid = $('#frmelmattrinfo'+attrid+'').data('datasectionid');
	var lablname = $('#frmelemattrlablname').val();
	var labldefval = $('#frmelemattrdefname').val();
	var datedefval = $('#dateaddfielddefval').val();
	var timedefval = $('#timeaddfielddefval').val();
	var txtlen = $('#frmelemattrlength').val();
	var desc = $('#frmelemattrdesc').val();
	var mandopt = $('#fildmandatory').val();
	var activeopt = $('#fieldactive').val();
	var readonlyopt = $('#fieldreadonly').val();
	var rowlen = $('#frmelemattrrows').val();
	var multiselect = $('#addfieldmultiple').val();
	var columtype = $('#fieldcolumntype').val();
	var decsize = $('#frmelemdecilength').val();
	var autosuffix = $('#frmelemsuffix').val();
	var startnum = $('#frmelemstartnum').val();
	var autoprefix = $('#frmelemprefix').val();
	var autonumtype = $('#attrautonumbertype').val();
	var sortorder = $('#addalphasortorder').val();
	var ddvalue = $('#frmelemdatavalue').val();
	var ddmodvalue = $('#attrmodulename').val();
	var ddfieldname = $('#frmdispelefiledname').val();
	var dddefvalue = $('#dddefaultvalue').val();
	var ddcondname = $('#frmelecondname').val();
	var ddcondval = $('#frmelmcondval').val();
	var chkbxdef = $('#addfieldsetdef').val();
	var featdateenable = $('#featuredate').val();
	var pastdateenable = $('#pastdate').val();
	if(uitypeid == '2') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+labldefval+','+txtlen+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype;
	} else if(uitypeid == '3') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+labldefval+','+txtlen+','+rowlen+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype;
	} else if(uitypeid == '4') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+labldefval+','+txtlen+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype;
	} else if(uitypeid == '5') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+labldefval+','+txtlen+','+decsize+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype;
	} else if(uitypeid == '6') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+labldefval+','+txtlen+','+decsize+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype;
	}  else if(uitypeid == '7') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+labldefval+','+txtlen+','+decsize+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype;
	} else if(uitypeid == '8') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+datedefval+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype+','+featdateenable+','+pastdateenable;
	} else if(uitypeid == '9') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+timedefval+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype;
	} else if(uitypeid == '10') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+labldefval+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype;
	} else if(uitypeid == '11') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+labldefval+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype;
	} else if(uitypeid == '12') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+labldefval+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype;
	} else if(uitypeid == '13') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+chkbxdef+','+columtype;
	} else if(uitypeid == '14') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+autoprefix+','+startnum+','+autosuffix+','+autonumtype+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype;
	} else if(uitypeid == '15') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype;
	} else if(uitypeid == '16') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype;
	} else if(uitypeid == '17') {
		ddvalue = ddvalue.replace(/\,/g, '|');
		if(dddefvalue == 'null') {
			dddefvalue='';
		}
		var attrinfo = secid+','+uitypeid+','+lablname+','+ddvalue+','+dddefvalue+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+multiselect+','+columtype+','+sortorder;
	} else if(uitypeid == '22') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+labldefval+','+txtlen+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype;
	} else if(uitypeid == '24') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype;
	} else if(uitypeid == '26') {
		var condfildname = $('#frmelefiledname').val();
		var colname = $('#frmelefiledname').find('option:selected').data('colnamehid');
		var tblname = $('#frmelefiledname').find('option:selected').data('tablenamehid');
		var partblname = $('#frmelefiledname').find('option:selected').data('parenttablehid');
		var flduitypeid = $('#frmelefiledname').find('option:selected').data('uitypeidhid');
		var conditions = colname+'|'+tblname+'|'+partblname+'|'+flduitypeid;
		var attrinfo = secid+','+uitypeid+','+lablname+','+ddmodvalue+','+ddfieldname+','+condfildname+','+ddcondname+','+ddcondval+','+desc+','+conditions+','+mandopt+','+activeopt+','+readonlyopt+','+multiselect+','+columtype;
	}
	$('#frmelmattrinfo'+attrid+'').val(attrinfo);
}
//form elements values reorganize
function fromelementsvaluereorganize(id) {
	formelementsfieldsshowhide(id);
	var attrinfo = $('#frmelmattrinfo'+id+'').val();
	var attrdatas = attrinfo.split(',');
	var uitypeid = attrdatas[1];
	if(attrdatas[2] != "") {
		$('#frmelemattrlablname').val(attrdatas[2]);
	}
	if(uitypeid == '2') {
		$('#frmelemattrdefname').val(attrdatas[3]);
		$('#frmelemattrlength').val(attrdatas[4]);
		$('#frmelemattrdesc').val(attrdatas[5]);
		var chkboxinfo = {'fildmandatory':attrdatas[6],'fieldactive':attrdatas[7],'fieldreadonly':attrdatas[8],'fieldcolumntype':attrdatas[9]};
		chkboxvalueset(chkboxinfo);
	} else if(uitypeid == '3') {
		$('#frmelemattrdefname').val(attrdatas[3]);
		$('#frmelemattrlength').val(attrdatas[4]);
		$('#frmelemattrrows').val(attrdatas[5]);
		$('#frmelemattrdesc').val(attrdatas[6]);
		var chkboxinfo = {'fildmandatory':attrdatas[7],'fieldactive':attrdatas[8],'fieldreadonly':attrdatas[9],'fieldcolumntype':attrdatas[10]};
		chkboxvalueset(chkboxinfo);
	} else if(uitypeid == '4') {
		$('#frmelemattrdefname').val(attrdatas[3]);
		$('#frmelemattrlength').val(attrdatas[4]);
		$('#frmelemattrdesc').val(attrdatas[5]);
		var chkboxinfo = {'fildmandatory':attrdatas[6],'fieldactive':attrdatas[7],'fieldreadonly':attrdatas[8],'fieldcolumntype':attrdatas[9]};
		chkboxvalueset(chkboxinfo);
	} else if(uitypeid == '5') {
		$('#frmelemattrdefname').val(attrdatas[3]);
		$('#frmelemattrlength').val(attrdatas[4]);
		$('#frmelemdecilength').val(attrdatas[5]);
		$('#frmelemattrdesc').val(attrdatas[6]);
		var chkboxinfo = {'fildmandatory':attrdatas[7],'fieldactive':attrdatas[8],'fieldreadonly':attrdatas[9],'fieldcolumntype':attrdatas[10]};
		chkboxvalueset(chkboxinfo);
	} else if(uitypeid == '6') {
		$('#frmelemattrdefname').val(attrdatas[3]);
		$('#frmelemattrlength').val(attrdatas[4]);
		$('#frmelemdecilength').val(attrdatas[5]);
		$('#frmelemattrdesc').val(attrdatas[6]);
		var chkboxinfo = {'fildmandatory':attrdatas[7],'fieldactive':attrdatas[8],'fieldreadonly':attrdatas[9],'fieldcolumntype':attrdatas[10]};
		chkboxvalueset(chkboxinfo);
	} else if(uitypeid == '7') {
		$('#frmelemattrdefname').val(attrdatas[3]);
		$('#frmelemattrlength').val(attrdatas[4]);
		$('#frmelemdecilength').val(attrdatas[5]);
		$('#frmelemattrdesc').val(attrdatas[6]);
		var chkboxinfo = {'fildmandatory':attrdatas[7],'fieldactive':attrdatas[8],'fieldreadonly':attrdatas[9],'fieldcolumntype':attrdatas[10]};
		chkboxvalueset(chkboxinfo);
	} else if(uitypeid == '8') {
		$('#dateaddfielddefval').val(attrdatas[3]);
		$('#frmelemattrdesc').val(attrdatas[4]);
		var chkboxinfo = {'fildmandatory':attrdatas[5],'fieldactive':attrdatas[6],'fieldreadonly':attrdatas[7],'fieldcolumntype':attrdatas[8],'featuredate':attrdatas[9],'pastdate':attrdatas[10]};
		chkboxvalueset(chkboxinfo);
	} else if(uitypeid == '9') {
		$('#timeaddfielddefval').val(attrdatas[3]);
		$('#frmelemattrdesc').val(attrdatas[4]);
		var chkboxinfo = {'fildmandatory':attrdatas[5],'fieldactive':attrdatas[6],'fieldreadonly':attrdatas[7],'fieldcolumntype':attrdatas[8]};
		chkboxvalueset(chkboxinfo);
	} else if(uitypeid == '10') {
		$('#frmelemattrdefname').val(attrdatas[3]);
		$('#frmelemattrdesc').val(attrdatas[4]);
		var chkboxinfo = {'fildmandatory':attrdatas[5],'fieldactive':attrdatas[6],'fieldreadonly':attrdatas[7],'fieldcolumntype':attrdatas[8]};
		chkboxvalueset(chkboxinfo);
	} else if(uitypeid == '11') {
		$('#frmelemattrdefname').val(attrdatas[3]);
		$('#frmelemattrdesc').val(attrdatas[4]);
		var chkboxinfo = {'fildmandatory':attrdatas[5],'fieldactive':attrdatas[6],'fieldreadonly':attrdatas[7],'fieldcolumntype':attrdatas[8]};
		chkboxvalueset(chkboxinfo);
	} else if(uitypeid == '12') {
		$('#frmelemattrdefname').val(attrdatas[3]);
		$('#frmelemattrdesc').val(attrdatas[4]);
		var chkboxinfo = {'fildmandatory':attrdatas[5],'fieldactive':attrdatas[6],'fieldreadonly':attrdatas[7],'fieldcolumntype':attrdatas[8]};
		chkboxvalueset(chkboxinfo);
	} else if(uitypeid == '13') {
		$('#frmelemattrdesc').val(attrdatas[3]);
		var chkboxinfo = {'fildmandatory':attrdatas[4],'fieldactive':attrdatas[5],'fieldreadonly':attrdatas[6],'addfieldsetdef':attrdatas[7],'fieldcolumntype':attrdatas[8]};
		chkboxvalueset(chkboxinfo);
	} else if(uitypeid == '14') {
		$('#frmelemprefix').val(attrdatas[3]);
		$('#frmelemstartnum').val(attrdatas[4]);
		$('#frmelemsuffix').val(attrdatas[5]);
		$('#frmelemattrdesc').val(attrdatas[7]);
		var chkboxinfo = {'fildmandatory':attrdatas[8],'fieldactive':attrdatas[9],'fieldreadonly':attrdatas[10],'fieldcolumntype':attrdatas[11]};
		chkboxvalueset(chkboxinfo);
		$('#attrautonumbertype').val(attrdatas[6]).trigger('change');
	} else if(uitypeid == '15') {
		$('#frmelemattrdesc').val(attrdatas[3]);
		var chkboxinfo = {'fildmandatory':attrdatas[4],'fieldactive':attrdatas[5],'fieldreadonly':attrdatas[6],'fieldcolumntype':attrdatas[7]};
		chkboxvalueset(chkboxinfo);
	} else if(uitypeid == '16') {
		$('#frmelemattrdesc').val(attrdatas[3]);
		var chkboxinfo = {'fildmandatory':attrdatas[4],'fieldactive':attrdatas[5],'fieldreadonly':attrdatas[6],'fieldcolumntype':attrdatas[7]};
		chkboxvalueset(chkboxinfo);
	} else if(uitypeid == '17') {
		var ddndatas = attrdatas[3]
		ddndatas = ddndatas.replace(/\|/g, ',');
		$("#frmelemdatavalue").val(ddndatas);
		var ddsdatas = ddndatas.split(',');
		$("#frmelemdatavalue").select2({
			tags:ddsdatas,
		});
		$('#frmelemattrdesc').val(attrdatas[5]);
		var chkboxinfo = {'fildmandatory':attrdatas[6],'fieldactive':attrdatas[7],'fieldreadonly':attrdatas[8],'addfieldmultiple':attrdatas[9],'fieldcolumntype':attrdatas[10],'addalphasortorder':attrdatas[11]};
		chkboxvalueset(chkboxinfo);
		setTimeout(function(){
			$("#frmelemdatavalue").trigger('change');
			$('#dddefaultvalue').val(attrdatas[4]);
			$('#dddefaultvalue').trigger('change');				
		},50);
	} else if(uitypeid == '22') {
		$('#frmelemattrdefname').val(attrdatas[3]);
		$('#frmelemattrlength').val(attrdatas[4]);
		$('#frmelemattrdesc').val(attrdatas[5]);
		var chkboxinfo = {'fildmandatory':attrdatas[6],'fieldactive':attrdatas[7],'fieldreadonly':attrdatas[8],'fieldcolumntype':attrdatas[9]};
		chkboxvalueset(chkboxinfo);
	} else if(uitypeid == '24') {
		$('#frmelemattrdesc').val(attrdatas[3]);
		var chkboxinfo = {'fildmandatory':attrdatas[4],'fieldactive':attrdatas[5],'fieldreadonly':attrdatas[6],'fieldcolumntype':attrdatas[7]};
		chkboxvalueset(chkboxinfo);
	} else if(uitypeid == '26') {
		$("#attrmodulename").select2('val',attrdatas[3]);
		$("#frmelecondname").select2('val',attrdatas[6]);
		$('#frmelmcondval').val(attrdatas[7]);
		$('#frmelemattrdesc').val(attrdatas[8]);
		var chkboxinfo = {'fildmandatory':attrdatas[10],'fieldactive':attrdatas[11],'fieldreadonly':attrdatas[12],'addfieldmultiple':attrdatas[13],'fieldcolumntype':attrdatas[14]};
		chkboxvalueset(chkboxinfo);
		setTimeout(function(){
			$("#attrmodulename").trigger('change');
			$("#frmelecondname").trigger('change');
			$('#frmdispelefiledname').val(attrdatas[4]);
			$('#frmelefiledname').val(attrdatas[5]);
			$("#frmelefiledname").trigger('change');
			$("#frmdispelefiledname").trigger('change');
		},50);
	}
}
//form elements fields show hide
function formelementsfieldsshowhide(id) {
	var uiid = $('#frmelmattrinfo'+id+'').data('frmeleuitypeattr');
	if(uiid == '3') {
		var showdataarr = ['readonlyattr','columntypeattr','defval','attrrowlen','attrdatalen'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','timedefval','attrmodopt','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrdatarandomopt','datedefval','attrdatadecilen','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrdispfldname','featuredatespan','pastdatespan'];
		datahideapply(hidedataarr);
	} else if(uiid == '4') {
		var showdataarr = ['readonlyattr','columntypeattr','defval','attrdatalen'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','timedefval','attrmodopt','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrdatarandomopt','datedefval','attrdatadecilen','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrdispfldname','featuredatespan','pastdatespan'];
		datahideapply(hidedataarr);
	} else if(uiid == '5') {
		var showdataarr = ['readonlyattr','columntypeattr','defval','attrdatalen','attrdatadecilen'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','timedefval','attrmodopt','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrdatarandomopt','datedefval','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrdispfldname','featuredatespan','pastdatespan'];
		datahideapply(hidedataarr);
	} else if(uiid == '6') {
		var showdataarr = ['readonlyattr','columntypeattr','defval','attrdatadecilen'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','timedefval','attrmodopt','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrdatarandomopt','datedefval','attrmodfldcond','attrmodfldval','attrfldname','attrdatalen','attrchkboxsetdef','attrdispfldname','featuredatespan','pastdatespan'];
		datahideapply(hidedataarr);
	} else if(uiid == '7') {
		var showdataarr = ['readonlyattr','columntypeattr','defval','attrdatalen'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','timedefval','attrmodopt','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrdatarandomopt','datedefval','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrdispfldname','featuredatespan','pastdatespan'];
		datahideapply(hidedataarr);
	} else if(uiid == '8') {
		var showdataarr = ['readonlyattr','columntypeattr','datedefval','featuredatespan','pastdatespan'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','timedefval','attrmodopt','attrdatalen','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrdatarandomopt','defval','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrdispfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '9') {
		var showdataarr = ['readonlyattr','columntypeattr','timedefval'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','attrmodopt','attrdatalen','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrdatarandomopt','defval','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrdispfldname','featuredatespan','pastdatespan'];
		datahideapply(hidedataarr);
	} else if(uiid == '10') {
		var showdataarr = ['readonlyattr','columntypeattr','defval'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrmodopt','attrdatalen','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrdatarandomopt','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrdispfldname','featuredatespan','pastdatespan'];
		datahideapply(hidedataarr);
	} else if(uiid == '11') {
		var showdataarr = ['readonlyattr','columntypeattr','defval'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrmodopt','attrdatalen','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrdatarandomopt','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrdispfldname','featuredatespan','pastdatespan'];
		datahideapply(hidedataarr);
	} else if(uiid == '12') {
		var showdataarr = ['readonlyattr','columntypeattr','defval'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrmodopt','attrdatalen','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrdatarandomopt','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrdispfldname','featuredatespan','pastdatespan'];
		datahideapply(hidedataarr);
	} else if(uiid == '13') {
		var showdataarr = ['readonlyattr','columntypeattr','attrchkboxsetdef'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrmodopt','attrdatalen','defval','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrdatarandomopt','attrmodfldcond','attrmodfldval','attrfldname','attrdispfldname','featuredatespan','pastdatespan'];
		datahideapply(hidedataarr);
	} else if(uiid == '14') {
		var showdataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','readonlyattr','columntypeattr','attrdatarandomopt'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrrowlen','attrdatadecilen','datedefval','timedefval','attrmodopt','attrdatalen','defval','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrdispfldname','featuredatespan','pastdatespan'];
		datahideapply(hidedataarr);
	} else if(uiid == '15') {
		var showdataarr = [];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrdatarandomopt','attrmodopt','attrdatalen','defval','readonlyattr','columntypeattr','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrdispfldname','featuredatespan','pastdatespan'];
		datahideapply(hidedataarr);
	} else if(uiid == '16') {
		var showdataarr = [];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrdatarandomopt','attrmodopt','attrdatalen','defval','readonlyattr','columntypeattr','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrdispfldname','featuredatespan','pastdatespan'];
		datahideapply(hidedataarr);
	} else if(uiid == '17') {
		var showdataarr = ['readonlyattr','columntypeattr','mulchkopt','dataplvalattr','attrsortopt','dddefval'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrdatarandomopt','attrmodopt','attrdatalen','defval','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrdispfldname','featuredatespan','pastdatespan'];
		datahideapply(hidedataarr);
	} else if(uiid == '22') {
		var showdataarr = ['readonlyattr','columntypeattr','attrdatalen','defval'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrdatarandomopt','attrsortopt','dddefval','mulchkopt','dataplvalattr','attrmodopt','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrdispfldname','featuredatespan','pastdatespan'];
		datahideapply(hidedataarr);
	} else if(uiid == '24') {
		var showdataarr = [];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrdatarandomopt','attrsortopt','dddefval','attrdatalen','defval','readonlyattr','columntypeattr','mulchkopt','dataplvalattr','attrmodopt','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrdispfldname','featuredatespan','pastdatespan'];
		datahideapply(hidedataarr);
	} else if(uiid == '26') {
		var showdataarr = ['readonlyattr','columntypeattr','mulchkopt','attrmodopt','attrmodfldcond','attrmodfldval','attrfldname','attrdispfldname'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrdatarandomopt','attrsortopt','dddefval','attrdatalen','defval','dataplvalattr','attrchkboxsetdef','featuredatespan','pastdatespan'];
		datahideapply(hidedataarr);
	} else {
		var showdataarr = ['readonlyattr','columntypeattr','defval','attrdatalen'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','mulchkopt','attrrowlen','attrdatadecilen','datedefval','timedefval','attrdatarandomopt','attrsortopt','dataplvalattr','dddefval','attrmodopt','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrdispfldname','featuredatespan','pastdatespan'];
		datahideapply(hidedataarr);
	}
}
function datahideapply(hidedataarr) {
	for(var i=0;i<hidedataarr.length;i++){
		$('#'+hidedataarr[i]+'').addClass('hidedisplay');
	}
}
function datashowapply(showdataarr) {
	for(var j=0;j<showdataarr.length;j++){
		$('#'+showdataarr[j]+'').removeClass('hidedisplay');
	}
}
//module name validate
function modulenamecheck() {
	var modname = $('#newmodulecrename').val();
	var nmsg = "";
	$.ajax({
		url:base_url+"Modulebuilder/modulenamecheck",
		data:"data=&modulename="+modname,
		type:"post",
		async:false,
		cache:false,
		success :function(msg) {
			nmsg = $.trim(msg);
		},
	});
	if(nmsg == "True") {
		return "Module already exists!";
	}
}
//module name validate
function picklisttabvalidate() {
	var attrid = $('#elemidentiryid').val();
	var uitypeid = $('#frmelmattrinfo'+attrid+'').data('frmeleuitypeattr');
	if(uitypeid == "17") {
		var picklistname = $('#frmelemattrlablname').val();
		var nmsg = "";
		$.ajax({
			url:base_url+"Modulebuilder/picklistnamecheck",
			data:"data=&picklistname="+picklistname,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg == "True") {
			return "Auto Relation Is Created!";
		}
	}
}
//validate length text box
function lengthvalidate() {
	$('#frmelemattrlength').val();
}
//module data create
function moduledatacreate() {
	$("#processoverlay").show();
	$('.frmelediv').find('input, textarea, button, select').attr('disabled',false);
	var formbuilddata=$("#newmodulegenform").serialize();
	var modname = $('#newmodulecrename').val();
	var modactions = $('#modactionslist').val();
	if(formbuilddata != "") {
		$.ajax({
			url:base_url+"Modulebuilder/modbuildercreate",
			data:"data=&" +formbuilddata+"&modulename="+modname+"&moduleaction="+modactions,
			type:"post",
			cache:false,
			success :function(msg) {
				var nmsg = $.trim(msg);
				if(nmsg == 'Success') {
					$('.frmelediv').find('input, textarea, button, select').attr('disabled','disabled');
					$('#spanforaddtab').empty();
					inci = 0;
					iden = 0;
					grp = 0;
					cner = 0;
					$('#newmodulecrename').val('');
					$('#moduleactionname').select2("val", "");
					clearform('propertiesclear');
					$("#processoverlay").fadeOut();
					alertpopup('Module Created Successfully!');
					setTimeout(function(){
						window.location=base_url+"Moduleeditor";
					},300);
				} else {
					$("#processoverlay").fadeOut();
					alertpopup('Module Creation Failed!');
				}
			}
		});
	}
}
//default value set fun
function defaultoptionsset() {
	$('#fieldactive').prop('checked', true);
	$('#fildmandatory,#fieldactive,#fieldreadonly,#addfieldmultiple,#addchkboxfielddef,#fieldcolumntype,#addalphasortorder').trigger('change');
}
function checkboxvaluereset(chkboxinfo) {
	$.each( chkboxinfo, function( key,value ) {
		if ( $('#'+value+'').prop('checked') == true ){ 
			$('#'+value+'').val('Yes');
		} else {
			$('#'+value+'').val('No');
		}
	});
}
//check box value set fun
function chkboxvalueset(chkboxinfo) {
	$.each( chkboxinfo, function( key, value ) {
		$('#'+key+'').val(value);
		if(value == 'Yes') {
			$('#'+key+'').prop('checked', true);
		} else {
			$('#'+key+'').prop('checked', false);
		}
	});
}
function forremoveformelements(){
	$( ".rmvfbelements" ).hover(
		function() {
		$(this).css("opacity","1");	
		}, function() {
		$(this).css("opacity","0");
	});
	$(".elementremovebtn").click(function(){
		$( this ).parent(".rmvfbelements").parent(".hovertoremove").remove();
		clearform('propertiesdataclear');
		$('#fieldactive').prop('checked', true);
		var chkboxinfo = ['fildmandatory','fieldactive','fieldreadonly','addfieldmultiple','fieldcolumntype','addalphasortorder'];
		checkboxvaluereset(chkboxinfo);
		$('#formelementsid').trigger('click');
		if($("#forproppanelshow").hasClass('hideotherelements')) {		
		} else {
			$('.fortrigger4').trigger('click');
		}
	});
}
{//Focus First Letter
	$.fn.setCursorPosition = function(pos) {
		this.each(function(index, elem) {
			if (elem.setSelectionRange) {
			  elem.setSelectionRange(pos, pos);
			} else if (elem.createTextRange) {
			  var range = elem.createTextRange();
			  range.collapse(true);
			  range.moveEnd('character', pos);
			  range.moveStart('character', pos);
			  range.select();
			}
		});
		return this;
	};
}
{//Reload Restriction
	function disableF5(e) { 
		if ((e.which || e.keyCode) == 116) {
			e.preventDefault(); 
			$('#reloadrestriction').fadeIn();
			$('#reloadpageyes').focus();
		}
	}
}
//drop down value set
function dropdownmodulefilednameset(moduleid) {
	$('#frmdispelefiledname,#frmelefiledname').empty();
	$('#frmdispelefiledname,#frmelefiledname').append($("<option value=''>select</option>"));
	$.ajax({
		url:base_url+'Modulebuilder/ddcondfieldnamefetch?id='+moduleid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#frmdispelefiledname,#frmelefiledname')
					.append($("<option></option>")
					.attr("data-colnamehid",data[index]['datacolmname'])
					.attr("data-tablenamehid",data[index]['datatblname'])
					.attr("data-parenttablehid",data[index]['datapartable'])
					.attr("data-uitypeidhid",data[index]['datauitype'])
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}
			$('#frmdispelefiledname,#frmelefiledname').trigger('change');
		},
	});
}
//Propert panel slide toggle
function propertyslidetoggle(){
	$('.stepperul3').slideDown("slow");
	$('.steppercategory3').find('.toggleicons').addClass('icon24build-chevron-up');
	$('.steppercategory3').find('.toggleicons').removeClass('icon24build-chevron-down');
	$('.stepperul2').slideUp("slow");
	$('.steppercategory2').find('.toggleicons').addClass('icon24build-chevron-down');
	$('.steppercategory2').find('.toggleicons').removeClass('icon24build-chevron-up');
}

function builderformheight(){
	{// For Height
		var totalcontainerleftheight = $( window ).height();
		$("#leftsidepanel").css('height',''+totalcontainerleftheight+'px');
		$("#droptarget1").css('height',''+totalcontainerleftheight+'px');
		var reducedheight =  totalcontainerleftheight - 35;
		$(".dropareadiv").css('height',''+reducedheight+'px');
		var reducedheightproppanel =  totalcontainerleftheight - 39;
		$(".steppervertical").css('height',''+reducedheightproppanel+'px');
	}
}