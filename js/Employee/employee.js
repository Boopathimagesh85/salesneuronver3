$(document).ready(function()
{
	$(document).foundation();
	// Maindiv height width change
	maindivwidth();
	var maindiv = $('.maindiv').width();
	var a = screen.width;
	reportsviewgridwidth = ((a * maindiv)/100);	
	// Grid Calling Functions
	employeejqgrid();
	//crud action
	crudactionenable();
	var addcloseinfo = ["closeaddform", "employeegriddisplay", "employeeform"]
    addclose(addcloseinfo);
	var addcloseviewcreation =["viewcloseformiconid","employeegriddisplay","viewcreationformdiv",""];
	addclose(addcloseviewcreation);
	{
		var editorname = $('#editornameinfo').val();
		froalaarray=[editorname,'html.set',''];
	}
	if(softwareindustryid == 3){
    	 defaultstateid = $('#defaultstateid').val();
    	 defaultcountryid = $('#defaultcountryid').val();
	}
	{// For touch
		fortabtouch = 0;
	}
	{
		//employee view by dropdown change
		$('#dynamicdddataview').change(function(){
			refreshgrid();
		});
		//detail view icon click
		$("#detailedviewicon").click(function() {
			var datarowid = $('#employeegrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$.ajax({
					url:base_url+"Employee/employeeidcheck?employeeid="+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						if((data.fail) != 'FAILED'){
			                froalaset(froalaarray);
							$(".addbtnclass").addClass('hidedisplay');
							$(".updatebtnclass").removeClass('hidedisplay');
							$("#formclearicon").hide(); 
							$('#employeeimageattachdisplay').empty();
							getformdata(datarowid);
							Materialize.updateTextFields();
							firstfieldfocus();
							$("#employeeimagedivhid").addClass('hidedisplay');
							showhideiconsfun('summryclick','employeeform');	
							$(".fr-element").attr("contenteditable", 'false');
							//For tablet and mobile Tab section reset
							$('#tabgropdropdown').select2('val','1');
							setTimeout(function() {
								 $('#tabgropdropdown').select2('enable');
							 },50);
						} else {
							alertpopup('You Dont have a Permission to View the Deactive User');
						}
					},
				});
			} else {
				alertpopup("Please select a row");
			}
		});
		{// Redirected form notification overlay and widget and audit log
			var rdatarowid = sessionStorage.getItem("datarowid");		
			if(rdatarowid != '' && rdatarowid != null){
				 setTimeout(function() {
					froalaset(froalaarray);
					$(".addbtnclass").addClass('hidedisplay');
					$(".updatebtnclass").removeClass('hidedisplay');
					$("#formclearicon").hide(); 
					$('#employeeimageattachdisplay').empty();
					getformdata(rdatarowid);
					showhideiconsfun('summryclick','employeeform');
					sessionStorage.removeItem("datarowid");
					Materialize.updateTextFields();
				},50);
			}
		}
		$("#editfromsummayicon").click(function() {
			$('#emailid').attr('readonly',true);
			showhideiconsfun('editfromsummryclick','employeeform');
			$(".fr-element").attr("contenteditable", 'true');
			$("#employeeimagedivhid").removeClass('hidedisplay');
		});
		$("#reloadicon").click(function(){
			refreshgrid();
			froalaset(froalaarray);
			$('#employeeimageattachdisplay').empty();
			$('#employeeimage').val('');
		});
		$( window ).resize(function() {
			maingridresizeheightset('employeegrid');
		});
		//formclear resetting
		$("#formclearicon").click(function(){
			froalaset(froalaarray);
			$('#employeeimageattachdisplay').empty();
			defaulttimezoneset();
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
		});
		$('#dataaddsbtn').click(function(e){
			$('.ftab').trigger('click');
			$("#formaddwizard").validationEngine('validate');
		});
		jQuery("#formaddwizard").validationEngine({
			onSuccess: function() {
				addformdata();
			},
			onFailure: function() {
				var dropdownid =['2','branchid','userroleid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//update employee information
		$('#dataupdatesubbtn').click(function(e) {
			$('.ftab').trigger('click');
			$("#formeditwizard").validationEngine('validate');
		});
		$("#formeditwizard").validationEngine({
			onSuccess: function() {
				updateformdata();
			},
			onFailure: function() {
				var dropdownid =['1','branchid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}	
	{// Redirected form Home
		var uniquelreportsession = sessionStorage.getItem("reportunique4");		
		if(uniquelreportsession != '' && uniquelreportsession != null) {	
			setTimeout(function() {
				var datarowid=uniquelreportsession;		
				if (datarowid) {
					$.ajax({
						url:base_url+"Employee/employeeidcheck?employeeid="+datarowid,
						dataType:'json',
						async:false,
						cache:false,
						success :function(data) {
							if((data.fail) != 'FAILED') {
								showhideiconsfun('summryclick','employeeform');	
                                 froalaset(froalaarray);								
								showhideiconsfun('summryclick','employeeform');		
								$(".addbtnclass").addClass('hidedisplay');
								$(".updatebtnclass").removeClass('hidedisplay');
								$('#employeeimageattachdisplay').empty();
								getformdata(datarowid);
								firstfieldfocus();
							} else {
								alertpopup('You Dont have a Permission to View the Deactive User');
							} 
						},
					});
				}
				sessionStorage.removeItem("reportunique4");
			},50);	
		} 
	}
	{ //address type change
		$("#primaryaddresstype option[value='7']").remove();
		$("#secondaryaddresstype option[value='8']").remove();
		$("#primaryaddresstype,#secondaryaddresstype").change(function(){
			var id=$(this).attr('id');
			setaddress(id);
		});
	}
	{
		// Login History
		$("#loginhistoryicon").click(function(){
			window.location =base_url+'Loginhistory';
		});
		// Audit Log 
		$("#auditlogicon").click(function(){
			window.location =base_url+'Auditlog';
		});
		// Groups Manager
		$("#groupsmanagericon").click(function(){
			window.location =base_url+'Groups';
		});
	}
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,employeejqgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			employeejqgrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
	if(loggedinuser != 2){
		$("#visibleicon").parent('span').addClass('hidedisplay');
		$("#invisibleicon").parent('span').addClass('hidedisplay');
	}
	{
		$("#employeetypeid").change(function(){
			var emptypeid = $(this).val();
			if(emptypeid == 2) {
				$('#accountid,#userchittypeid').select2('val','');
				$('#accountiddivhid,#userchittypeiddivhid').addClass('hidedisplay');
				$("#accountid,#userchittypeid").attr('class','');
			} else if(emptypeid == 3) {
				$('#userchittypeiddivhid').addClass('hidedisplay');
				$("#userchittypeid").attr('class','');
				$('#accountiddivhid').removeClass('hidedisplay');
				$("#accountid").attr('class','validate[required]');
			} else if(emptypeid == 4) {
				$('#accountiddivhid').addClass('hidedisplay');
				$("#accountid").attr('class','');
				$('#userchittypeiddivhid').removeClass('hidedisplay');
				$("#userchittypeid").attr('class','validate[required]');
			}
		});
	}
});
//view create success function
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $("#viewfieldids").val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','ViewCreationName','ViewCreationId','viewcreation','ViewCreationModuleId',viewmoduleids);
	if(viewname != "dontset") {
		setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		cleargriddata('viewcreateconditiongrid');
	} else {
		$('#dynamicdddataview').trigger('change');
	}
}
// Employee View Grid
function employeejqgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 30 : rowcount;
	var wwidth = $('#employeegrid').width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.employeeheadercolsort').hasClass('datasort') ? $('.employeeheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.employeeheadercolsort').hasClass('datasort') ? $('.employeeheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.employeeheadercolsort').hasClass('datasort') ? $('.employeeheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#viewfieldids').val();
	var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
	var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
	var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=employee&primaryid=employeeid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#employeegrid').empty();
			$('#employeegrid').append(data.content);
			$('#employeegridfooter').empty();
			$('#employeegridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('employeegrid');
			{//sorting
				$('.employeeheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.employeeheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('ul#employeepgnum li.active').data('pagenum');
					var rowcount = $('ul#employeepgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.employeeheadercolsort').hasClass('datasort') ? $('.employeeheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.employeeheadercolsort').hasClass('datasort') ? $('.employeeheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					var sortpos = $('#employeegrid .gridcontent').scrollLeft();
					employeejqgrid(page,rowcount);
					$('#employeegrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('employeeheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					employeejqgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#employeepgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					employeejqgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				//Material select
				$('#employeepgrowcount').material_select();
			}
			//onclick 
			$(".gridcontent").click(function(){
				var datarowid = $('#employeegrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
		},
	});
}
{//crud operation
	function crudactionenable() {
		//add icon click
		$("#addicon").click(function() {
			addslideup('employeegriddisplay','employeeform');
			resetFields();
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');			
			$('#emailid').attr('readonly',false);
			$('#primarydataid').val('');
			$('#employeeimageattachdisplay').empty();
			$('#employeeimage').val('');
			froalaset(froalaarray);
			showhideiconsfun('addclick','employeeform');
			$("#employeeimagedivhid").removeClass('hidedisplay');
			if(softwareindustryid == 3){
				 $('#primarystate').select2('val',defaultstateid);
				 $('#primarycountry').select2('val',defaultcountryid);
				 $('#branchid').select2('val',2);
				 $('#employeetypeid').select2('val',2);
				 $('#employeetypeid').trigger('change');
				 loadvendoraccount();
				 passwordvalidate();
			}
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			Materialize.updateTextFields();
			firstfieldfocus();
			//default time zone set
			defaulttimezoneset();			
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			$(".fr-element").attr("contenteditable", 'true');
			fortabtouch = 0;
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
		});
		//edit icon click
		$("#editicon").click(function(){
			$('#emailid').attr('readonly',true);
			var datarowid = $('#employeegrid div.gridcontent div.active').attr('id');
			if(datarowid){
				if(softwareindustryid == 3){
					passwordvalidate();
				}
				$.ajax({
					url:base_url+"Employee/employeeidcheck?employeeid="+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						if((data.fail) != 'FAILED') {
							showhideiconsfun('editclick','employeeform');
							froalaset(froalaarray);
							$(".addbtnclass").addClass('hidedisplay');
							$(".updatebtnclass").removeClass('hidedisplay');
							$('#employeeimageattachdisplay').empty();
							loadvendoraccount();
							getformdata(datarowid);
							Materialize.updateTextFields();
							firstfieldfocus();
							//For tablet and mobile Tab section reset
							$('#tabgropdropdown').select2('val','1');
							$("#employeeimagedivhid").removeClass('hidedisplay');
							$(".fr-element").attr("contenteditable", 'true');
							fortabtouch = 1;
							//For Keyboard Shortcut Variables
							addformupdate = 1;
							viewgridview = 0;
							addformview = 1;
						} else {
							alertpopup('You Dont have a Permission to Edit the Deactive User');
						} 
					},
				});
			} else {
				alertpopup("Please select a row");
		   }	
		});
		$("#invisibleicon").click(function(){
			var datarowid = $('#employeegrid div.gridcontent div.active').attr('id');
			if(datarowid){
				$('#primarydataid').val(datarowid);
				ncombinedmodalertdynamictxt('empdeleteyes','Disable the data?','Disable');
				$("#empdeleteyes").focus();
				$("#empdeleteyes").click(function(){
					var datarowid = $('#primarydataid').val();
					recordisable(datarowid);
					$(this).unbind();
				});
			} else {
				alertpopup("Please select a row");
			}
		});
		
		$("#visibleicon").click(function(){
			var datarowid = $('#employeegrid div.gridcontent div.active').attr('id');
			if(datarowid){
				$('#primarydataid').val(datarowid);
				ncombinedmodalertdynamictxt('empdeleteyes','Enable the data?','Enable');
				$("#empdeleteyes").focus();
				$("#empdeleteyes").click(function(){
					var datarowid = $('#primarydataid').val();
					recordenable(datarowid);
					$(this).unbind();
				});
			} else {
				alertpopup("Please select a row");
			}
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#employeepgnum li.active').data('pagenum');
		var rowcount = $('ul#employeepgnumcnt li .page-text .active').data('rowcount');
		employeejqgrid(page,rowcount);
	}
}
//disable user from login
function recorddelete(datarowid) {
	var elementstable = $('#elementstable').val();
	var elementspartable = $('#elementspartabname').val();
    $.ajax({
        url: base_url + "Employee/deleteinformationdata?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				refreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('User disabled successfully');
            } else if (nmsg == "Denied") {
            	refreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Permission denied');
			} else if (nmsg == "FALSE") {
				refreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup("It is the primary user and is always enabled");
			}
        },
    });
}
//Enable user from login
function recordenable(datarowid) {
	var elementstable = $('#elementstable').val();
	var elementspartable = $('#elementspartabname').val();
    $.ajax({
        url: base_url + "Employee/enableinformationdata?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				refreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('User enabled successfully');
            } else if (nmsg == "Denied") {
            	refreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('User is already Enable mode.');
			} else if (nmsg == "FALSE") {
				refreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup("It is the primary user and is always enabled");
			} else if (nmsg == "MAXUSER") {
				refreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup("User limit is exist.Please buy more users");
			}
        },
    });
}
//record disable
function recordisable(datarowid) {
	var elementstable = $('#elementstable').val();
	var elementspartable = $('#elementspartabname').val();
    $.ajax({
        url: base_url + "Employee/dsiableinformationdata?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				refreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('User disabled successfully');
            } else if (nmsg == "Denied") {
            	refreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('User is already Disable mode.');
			} else if (nmsg == "FALSE") {
				refreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup("It is primary user of the application and hence cannot be disabled");
			}
        },
    });
}
//new user add submit function
function addformdata() {
    var amp = '&';
	//editor data
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditoradd(editorname);
	var formdata = $("#dataaddform").serialize();
	var datainformation = amp + formdata;
	$.ajax({
        url: base_url + "Employee/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				$(".ftab").trigger('click');
				resetFields();
				$('#employeeform').hide();
				$('#employeegriddisplay').fadeIn(1000);
				refreshgrid();
				alertpopup(savealert);
				$('#employeeimageattachdisplay').empty();
            	froalaset(froalaarray);
				//For Keyboard Shortcut Variables
				viewgridview = 1;
				addformview = 0;
            } else {
				$(".ftab").trigger('click');
				resetFields();
				$('#employeeform').hide();
				$('#employeegriddisplay').fadeIn(1000);
				refreshgrid();
				alertpopup(nmsg);
				$('#employeeimageattachdisplay').empty();
				froalaset(froalaarray);
				//For Keyboard Shortcut Variables
				viewgridview = 1;
				addformview = 0;
			}
        },
    });
}
//old information show in form
function getformdata(datarowid) {
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementpartable = $('#elementspartabname').val();
	var resctable = $('#resctable').val();
	$.ajax({
		url:base_url+"Employee/fetchformdataeditdetails?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable+"&resctable="+resctable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				$('#employeeform').hide();
				$('#employeegriddisplay').fadeIn(1000);
				refreshgrid();
			} else {
				addslideup('employeegriddisplay','employeeform');
				var txtboxname = elementsname + ',primarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
				var attachfldname = $('#attachfieldnames').val();
				var attachcolname = $('#attachcolnames').val();
				var attachuitype = $('#attachuitypeids').val();
				var attachfieldid = $('#attachfieldids').val();
				attachedfiledisplay(attachfldname,attachcolname,attachuitype,attachfieldid,data);
				primaryaddvalfetch(datarowid);
				editordatafetch(froalaarray,data);
				$('#employeetypeid').trigger('change');
				if(data.signaturetypeid == ""){
					$("#signaturetypeid").select2("val","1");
					$("#signaturetypeid").val(1);
				}
			}
			$("#password").val('');
		}
	});
}
//primary address value fetch function 
function primaryaddvalfetch(datarowid) {
	$.ajax({
		url:base_url+"Employee/primaryaddressvalfetch?dataprimaryid="+datarowid+"&addtype=Billing", 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			var datanames = ['12','primaryaddresstype','primaryaddress','primarypincode','primarycity','primarystate','primarycountry','secondaryaddresstype','secondaryaddress','secondarypincode','secondarycity','secondarystate','secondarycountry'];
			var textboxname = ['12','primaryaddresstype','primaryaddress','primarypincode','primarycity','primarystate','primarycountry','secondaryaddresstype','secondaryaddress','secondarypincode','secondarycity','secondarystate','secondarycountry'];
			var dropdowns = ['2','primaryaddresstype','secondaryaddresstype'];
			textboxsetvalue(textboxname,datanames,data,dropdowns); 
		}
	});
}
//unique email id check
function employeemaailidcheck() {
	var value = $('#emailid').val();
	var primaryid = $('#primarydataid').val();
	var nmsg = "";
	if(primaryid == '') {
		if( value !="" ) {
			$.ajax({
				url:base_url+"Employee/employeeuniqueemailidcheck",
				data:"data=&email="+value,
				type:"post",
				async:false,
				cache:false,
				success :function(msg) {
					nmsg = $.trim(msg);
				},
			});
			if(nmsg != "False") {
				if(primaryid != ""){
					if(primaryid != nmsg) {
						return "Email Id name already exists!";
					}
				} else {
					return "Email Id name already exist!";
				}
			} 
		}
	}
}
//udate old information
function updateformdata() {
	var sigid = $("#signaturetypeid").val();
	if(sigid == '' || sigid == null){ $("#signaturetypeid").val(1); }
	var amp = '&';
	//editor data
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditoradd(editorname);
	var formdata = $("#dataaddform").serialize();
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Employee/datainformationupdate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				resetFields();
				froalaset(froalaarray);
				$(".ftab").trigger('click');
				$('#employeeform').hide();
				$('#employeegriddisplay').fadeIn(1000);
				refreshgrid();
				alertpopup(savealert);
				//For Keyboard Shortcut Variables
				addformupdate = 0;
				viewgridview = 1;
				addformview = 0;
            }
        },
    });
}
//set default time zone set
function defaulttimezoneset() {
	$.ajax({
        url: base_url + "Employee/defaulttimezoneget",
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$("#timezoneid").select2('val',data);
		 },
    });
}
//set address
function setaddress(id) {
	var value=$('#'+id+'').val();
	if(id == 'primaryaddresstype') {
		var type='primary';
	} else {
		var type='secondary';
	}
	//swap the billing to shipping
	if(value == 8) {
		$('#primaryaddress').val($('#secondaryaddress').val());
		$('#primaryarea').val($('#secondaryarea').val());
		$('#primarypincode').val($('#secondarypincode').val());
		$('#primarycity').val($('#secondarycity').val());
		$('#primarystate').val($('#secondarystate').val());
		$('#primarycountry').val($('#secondarycountry').val());	
	} else if(value == 7) { //swap the shipping to billing
		$('#secondaryaddress').val($('#primaryaddress').val());
		$('#secondaryarea').val($('#primaryarea').val());
		$('#secondarypincode').val($('#primarypincode').val());
		$('#secondarycity').val($('#primarycity').val());
		$('#secondarystate').val($('#primarystate').text());
		$('#secondarycountry').val($('#primarycountry').text());	
	}
	Materialize.updateTextFields();
}
{// Alert pop-up For Base Delete with dynamic messages //Ramesh
	function ncombinedmodalertdynamictxt(idname,content,value) {
		$('#basedeleteoverlayforcommodule').fadeIn();
		$('.commodyescls').attr('id',idname);
		$('.commodyescls').val(value);
		$('.commodyescls').focus();
		$('.basedelalerttxt').text(content);
	}
}

function employeemobilenumbercheck() {
	var primaryid = $("#primarydataid").val();
	var accname = $("#mobilenumber").val();
	var fieldname = 'mobilenumber';
	var elementpartable = 'employee';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniqueshortdynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Mobile number already exists!";
				}
			}else {
				return "Mobile number already exists!";
			}
		} 
	}
}

function employeephonenumbercheck() {
	var primaryid = $("#primarydataid").val();
	var accname = $("#landlinenumber").val();
	var fieldname = 'landlinenumber';
	var elementpartable = 'employee';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniqueshortdynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Phone number already exists!";
				}
			}else {
				return "Phone number already exists!";
			}
		} 
	}
}
function employeeothernumbercheck() {
	var primaryid = $("#primarydataid").val();
	var accname = $("#othernumber").val();
	var fieldname = 'othernumber';
	var elementpartable = 'employee';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniqueshortdynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Other number already exists!";
				}
			}else {
				return "Other number already exists!";
			}
		} 
	}
}
//Employee name check
function employeenamecheck() {
	var primaryid = $("#primarydataid").val();
	var accname = $("#name").val();
	var elementpartable = $('#elementspartabname').val();
	var nmsg = "";
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "First Name already exists!";
				}
			} else {
				return "First Name already exists!";
			}
		} 
	} 
}
// check confirm password
function passwordvalidate() {
		$('#confirmpassword').removeClass('validate[required,minSize[6],maxSize[20]]');
		$('#confirmpassword').addClass('validate[required,minSize[6],maxSize[20],equals[password]]');
}
// Load vendor accounts
function loadvendoraccount(){ // load account name
	$('#accountid').empty();	
    $('#accountid').append($("<option></option>"));
	var tablename = 'employee';
	$.ajax({
        url: base_url + "Employee/loadvendorname",
		method: "POST",
        async:false,
		dataType:'json',
		success: function(msg)
        { 
			for(var i = 0; i < msg.length; i++) {
				 $('#accountid')
				.append($("<option></option>")
				.attr('data-accountidhidden',msg[i]['accountname'])
				.attr("value",msg[i]['accountid'])
				.text(msg[i]['accountname']));
			} 
        },
    });
}