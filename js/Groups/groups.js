$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}
	userid = [];
	ugroupid = [];
	rolesid = [];
	useransroleid = [];	
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		groupsaddgrid();
		firstfieldfocus();
	}
	{//Tabgroup on more Dropdown
		autocollapse();
		//$(window).on('resize', autocollapse);
	}
	{//keyboard shortcut reset global variable
		 viewgridview = 0;
		 innergridview = 1;
	}
	//$("#closeaddform").removeClass('hidedisplay');
	{
		$("#addicon").click(function(){
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			$('#userupdatebtn').hide();
			$('#usersubmitbtn').show();
			resetFields();
			Materialize.updateTextFields();
			firstfieldfocus();
		});
	}
	$("#ddgroupmembers").change(function() {
		$("#s2id_ddgroupmembers,.select2-results").removeClass('error');
		$("#s2id_ddgroupmembers").find('ul').removeClass('error');
		var data = $('#ddgroupmembers').select2('data');
		var finalResult = [];
		for( item in $('#ddgroupmembers').select2('data') ) {
			finalResult.push(data[item].id);
		};
		var selectid = finalResult.join(',');
		$("#groupmembers").val(selectid);
	});
	$('#usersubmitbtn').show();
	$('#userupdatebtn').hide();
	$('#groupsmanagerviewtoggle').hide();
	//new group creation
	$('#usersubmitbtn').click(function(e) {
		if(e.which==1 || e.which === undefined) {
			$("#usergroupcreateform").validationEngine('validate');
			//For Touch
			masterfortouch = 0;
		}
	});
	$("#usergroupcreateform").validationEngine({
		onSuccess: function() {
			usersubmitform();
		},
		onFailure: function() {
			if($('#ddgroupmembers').val()===null) {
				$("#s2id_ddgroupmembers").find('ul').addClass('error');
			}
			alertpopup(validationalert);
		}
	});
	$('#reloadicon').show();
	//edit user group
	$("#editicon").click(function(){
		var datarowid = $('#groupsaddgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			groupeditdatafetch(datarowid);			
			//Keyboard short cut
			saveformview = 1;
			$('#usersubmitbtn').hide();
			$('#userupdatebtn').show();
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
		} else {
			alertpopup("Please select a row");
		}
		
		//For Touch
		masterfortouch = 1;
		Materialize.updateTextFields();
		firstfieldfocus();
	});
	//update group creation
	$('#userupdatebtn').click(function(e) {
		if(e.which==1 || e.which === undefined) {
			$("#usergropupdateform").validationEngine('validate');
		}
	});
	$("#usergropupdateform").validationEngine({
		onSuccess: function() {
			groupdataupdate();
		},
		onFailure: function() {
			var dropdownid =['1','ddgroupmembers'];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
			$("#s2id_ddgroupmembers").find('ul').addClass('error');
		}	
	});
	//delete form
	$("#deleteicon").click(function(e) {
		e.preventDefault();
		var datarowid = $('#groupsaddgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			combainedmoduledeletealert('groupdelete');
			$("#groupdelete").click(function(){
				var datarowid = $('#groupsaddgrid div.gridcontent div.active').attr('id');
				groupdeletefunction(datarowid);
				$(this).unbind();
			});
		} else {
			alertpopup("Please select a row");
		}
	});
	//reload icon
	$("#reloadicon").click(function() {
		$('#usersubmitbtn').show();
		$('#userupdatebtn').hide();
		resetFields();
		refreshgrid();
		empgroupdrodownreset('1');
	});
	$(window).resize(function() {
		maingridresizeheightset('groupsaddgrid');
		sectionpanelheight('groupsectionoverlay');
	});
	$("#closeaddform").click(function()	{
		window.location =base_url+'Employee';
	});
	$('#groupcloseaddform').click(function(){
		window.location =base_url+'Employee';
	});
	groupsfiltername = [];
	$("#productaddonfilteraddcondsubbtn").click(function() {
		$("#groupsfilterformconditionvalidation").validationEngine("validate");	
	});
	$("#groupsfilterformconditionvalidation").validationEngine({
		onSuccess: function() {
		dashboardmodulefilter(groupsaddgrid,'groups');
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
});
//Groups Add Grid
function groupsaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 10 : rowcount;
	var wwidth = $("#groupsaddgrid").width();
	var wheight = $(window).height();
	/*col sort*/
	var sortcol = $("#groupssortcolumn").val();
	var sortord = $("#groupssortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.groupsheadercolsort').hasClass('datasort') ? $('.groupsheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.groupsheadercolsort').hasClass('datasort') ? $('.groupsheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.groupsheadercolsort').hasClass('datasort') ? $('.groupsheadercolsort.datasort').attr('id') : '0';
	$.ajax({
		url:base_url+"Groups/groupdatafetch?maintabinfo=employeegroup&primaryid=employeegroupid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=246',
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#groupsaddgrid').empty();
			$('#groupsaddgrid').append(data.content);
			$('#groupsaddgridfooter').empty();
			$('#groupsaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('groupsaddgrid');
			{//sorting
				$('.groupsheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.groupsheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.groupsheadercolsort').hasClass('datasort') ? $('.groupsheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.groupsheadercolsort').hasClass('datasort') ? $('.groupsheadercolsort.datasort').data('sortorder') : '';
					$("#groupssortorder").val(sortord);
					$("#groupssortcolumn").val(sortcol);
					var sortpos = $('#groupsaddgrid .gridcontent').scrollLeft();
					groupsaddgrid(page,rowcount);
					$('#groupsaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('groupsheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					groupsaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#groupspgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					groupsaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				//Material select
				$('#groupspgrowcount').material_select();
			}

		},
	});
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#groupspgnum li.active').data('pagenum');
		var rowcount = $('ul#loginhistorypgnumcnt li .page-text .active').data('rowcount');
		groupsaddgrid(page,rowcount);
	}
}
//create user group
function usersubmitform() {
	var formdata = $("#usergroupcreateform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	$.ajax({
        url: base_url + "Groups/groupsubmitdata",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				resetFields();
				refreshgrid();
				$(".addsectionclose").trigger("click");
				alertpopup('Data Stored Successfully');
				empgroupdrodownreset('1');
            }
        },
    });
}
//user user group
function groupdataupdate() {
	var formdata = $("#usergroupcreateform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	$.ajax({
        url: base_url + "Groups/groupupdatedata",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if(nmsg == 'TRUE') {
				resetFields();
				$('#usersubmitbtn').show();
				$('#userupdatebtn').hide();
				refreshgrid();
				$(".addsectionclose").trigger("click");
				alertpopup('Data Updated Successfully');
				empgroupdrodownreset('1');
            }
			//For Touch
			masterfortouch = 0;
			//Keyboard short cut
			saveformview = 0;
        },
    });
}
//delete function
function groupdeletefunction(datarowid) {
	$.ajax({
        url: base_url + "Groups/gropdatadelete?datarowid="+datarowid,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Record deleted successfully');
				refreshgrid();
				empgroupdrodownreset('1');
            } else{
            	$("#basedeleteoverlayforcommodule").fadeOut();
            	refreshgrid();
            	alertpopup("You have created the data sharing rule for this group.So you can't delete this group");
            }
        },
    });
}
//edit data information fetch
function groupeditdatafetch(datarowid) {
	$.ajax({
        url: base_url+"Groups/groupeditdatainfofetch?datarowid="+datarowid,
		dataType:"json",
		cache:false,
        success: function(data) {
			$('#primaryid').val(datarowid);
			$('#usergroupname').val(data['name']);
			$('#userdescription').val(data['desc']);
			var groupids = data['id'].split(',');
			$('#ddgroupmembers').select2('val', groupids).trigger('change');
			Materialize.updateTextFields();
        },
    });
}
//print template name check
function groupnamecheck() {
	var value = $('#usergroupname').val();
	var primaryid = $('#primaryid').val();
	var nmsg = "";
	if( value !="" ) {
		$.ajax({
			url:base_url+"Groups/groupuniquename",
			data:"data=&groupname="+value,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != "") {
				if(primaryid != nmsg) {
					return "Name already exists!";
				}
			} else {
				return "Name already exists!";
			}
		}
	}
}
//employee group dd reset
function empgroupdrodownreset(groupid) {
	$.ajax({
        url: base_url + "Groups/employeegropdropdownvalfetch",
		dataType: "json",
		async:false,
		cache:false,
        success: function(data)  {
			if(data['status']!='fail') {
				$('#ddgroupmembers').empty();
				$('#ddgroupmembers').append('<option></option>');
				prev = ' ';
				$.each(data, function(index) {
					var cur = data[index]['PId'];
					if(prev != cur && groupid != data[index]['CId'] && data[index]['DDval'] != "G") {
						if(prev != " ") {
							$('#ddgroupmembers').append($("</optgroup>"));
						} 
						$('#ddgroupmembers').append($("<optgroup  label='"+data[index]['PName']+"' class='"+data[index]['PName']+"'>"));
						prev = data[index]['PId'];
					}
					if(groupid != data[index]['CId'] && data[index]['DDval'] != "G") {
						$("#ddgroupmembers optgroup[label='"+data[index]['PName']+"']")
						.append($("<option></option>")
						.attr("value",data[index]['DDval'])
						.attr("data-groupusersid",data[index]['CId'])
						.attr("data-grouusertypeid",data[index]['PId'])
						.text(data[index]['CName']));
					}
				});
			}
			$('#ddgroupmembers').trigger('change');
        },
    });
}
