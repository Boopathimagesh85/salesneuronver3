$(document).ready(function() {	
	{// Foundation Initialization
		$(document).foundation();
	}
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		smscampaignviewgrid();
		//crud action
		crudactionenable();
	}
	{//inner-form-with-grid
		$("#smscampaignsubingridadd1").click(function(){
			sectionpanelheight('smscampaignsuboverlay');
			$("#smscampaignsuboverlay").removeClass("closed");
			$("#smscampaignsuboverlay").addClass("effectbox");
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$("#smscampaignsubcancelbutton").click(function(){
			$("#smscampaignsuboverlay").removeClass("effectbox");
			$("#smscampaignsuboverlay").addClass("closed");
		});
	}
	//read only for mobile number text area
	$("#communicationto,#description").attr('readonly',true);
	// default arrays
	subscriber =[];
	dndnumbers =[];
	nondndnumbers =[];
	valnumbers =[];
	invnumbers= [];
	$('#formclearicon').click(function(){
		$("#formfields").empty();
		$("#templatetypeid").val('2');
		$("#tab1").trigger('click');
		$("#smsgroupsid,#smssegmentsubscriber").val('');
		$("#smsgroupsid,#smssegmentsubscriber").select2('val','');
		$("#segmentsubscriberid,#autoupdate,#selectedsegmentsubscriberid,#grouptypeid,#groupmoduleid").val('');
		smssubscriberrefreshgrid();
		$("#leadtemplatecontentdivhid").find('p').css('opacity','0');
	});
	//Close Add Screen
	var addcloseoppocreation =["closeaddform","smscampaigncreationview","smscampaigncreationformadd"]
	addclose(addcloseoppocreation);	
	var addcloseviewcreation =["viewcloseformiconid","smscampaigncreationview","viewcreationformdiv",""];
	addclose(addcloseviewcreation);
	// For touch
	fortabtouch = 0;
	$("#reloadicon").click(function(){
		refreshgrid();
	});
	$( window ).resize(function() {
		maingridresizeheightset('smscampaignviewgrid');
		sectionpanelheight('smscampaignsuboverlay');
	});
	$("#smscampaignsubingriddel1,#smscampaignsubaddbutton").hide();
	//group dd change
	$("#smsgroupsid").change(function() {
		$("#selectedsegmentsubscriberid,#segmentsubscriberid,#autoupdate").val('');
		$('#smssegmentsid').select2('val','');
		var groupid  = $("#smsgroupsid").val();
		grouptypeget(groupid);
		segmentdropdownvalfetch(groupid);
		//grid load
		smscampaignsubaddgrid1();
		var total = $("#smssubscriberlist-sort > .data-rows").length;
		$("#totalsubscribercount").val(total);
		Materialize.updateTextFields();
	});
	//close
	$('#closeaddoppocreation').click(function(){
		$('#dynamicdddataview').trigger('change');
	});
	//view by drop down change
	$('#dynamicdddataview').change(function(){
		smscampaignviewgrid();
	});
	{
		//SMS Groups
		$("#groupsiconicon").click(function(){
			var type = 'SMS';
			sessionStorage.setItem("smsmoduletype",type);
			window.location =base_url+'Smsgroups';
		});
		//SMS Segments
		$("#segmentsiconicon").click(function(){
			window.location =base_url+'Smssegments';
		});
		//SMS Subscribers
		$("#subscribersiconicon").click(function(){
			window.location =base_url+'Smssubscribers';
		});
	}
	//send sms
	$('#dataaddsbtn').click(function() {
		$("#formaddwizard").validationEngine('validate');
	});
	jQuery("#formaddwizard").validationEngine({
		onSuccess: function() {
			var apikey = $("#senderid").find('option:selected').data('apikey');
			var sendername = $("#senderid").find('option:selected').data('sendername');
			var smscount= $("#smscount").val();
			var content = $('#leadtemplatecontent').val();
			var regExp = /\{([^}]+)\}/g;
			var datacontent = content.match(regExp);
			var mobnum = $("#communicationto").val();
			$("#processoverlay").show();
			if(datacontent != null) {
				var grouptype = $("#defgrpid").val();
				$.ajax({
					url:base_url+'Smscampaign/mobilenumbasedsubscriberidget',
					data:'mobnum='+mobnum+"&grouptype="+grouptype,
					type:'POST',
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) { 
						var ccontent = [];
						if((data.fail) != 'FAILED') {
							for(var i =0;i <data.length;i++) {
								ccontent.push(data[i]['recordid']);
							}
						}
						messagecontentget(ccontent)
					},
				});
			} else {
				var message = $("#description").val();
				$("#defaultsubseditorval").val(message);
			}
			var formdata= $("#dataaddform").serialize();
			var amp = '&';
			var smsdatas = amp + formdata ;
			$.ajax({                       
				url:base_url+"Smscampaign/leadsmssend",  
				data: "data="+smsdatas+"&apikey="+apikey+"&sendername="+sendername+"&datacontent="+datacontent+"&smscount="+smscount, 
				dataType:'json',
				type:'POST',
				async:false,
				cache:false,
				success: function(msg) {
					if(msg == 'success') {
						resetFields();
						$('#smscampaigncreationformadd').hide();
						$('#smscampaigncreationview').fadeIn(1000);
						$("#leadtemplatecontentdivhid").find('p').css('opacity','0');
						$("#segmentsubscriberid,#autoupdate,#selectedsegmentsubscriberid,#grouptypeid,#groupmoduleid").val('');
						smscampaignviewgrid();
						$("#processoverlay").hide();
						alertpopup('SMS Sent successfully');
					} else if(msg == 'Credits') {
						resetFields();
						$('#smscampaigncreationformadd').hide();
						$('#smscampaigncreationview').fadeIn(1000);
						$("#leadtemplatecontentdivhid").find('p').css('opacity','0');
						$("#segmentsubscriberid,#autoupdate,#selectedsegmentsubscriberid,#grouptypeid,#groupmoduleid").val('');
						smscampaignviewgrid();
						$("#processoverlay").hide();
						alertpopup('You dont have specified SMS credits to sent this SMS.Please update your Addons SMS Credits');
					} else if(msg == 'Your IP Address is not valid') {
						resetFields();
						$('#smscampaigncreationformadd').hide();
						$('#smscampaigncreationview').fadeIn(1000);
						$("#leadtemplatecontentdivhid").find('p').css('opacity','0');
						smscampaignviewgrid();
						$("#processoverlay").hide();
						alertpopup(msg);
					} else if(msg == 'Invalid Credentials or Inactive Account') {
						resetFields();
						$('#smscampaigncreationformadd').hide();
						$('#smscampaigncreationview').fadeIn(1000);
						$("#leadtemplatecontentdivhid").find('p').css('opacity','0');
						smscampaignviewgrid();
						$("#processoverlay").hide();
						alertpopup(msg);
					}
				},                
			});
		},
		onFailure: function() {
			var dropdownid =['2','smstypeid','senderid'];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	//segment drop down change
	$('#smssegmentsid').change(function() {
		var smsgroupid = $("#smsgroupsid").val();
		if(smsgroupid) {
			var id = $(this).val();
			var selectid = id;
			if(selectid == '') {
				$("#segmentsubscriberid,#autoupdate").val('');
			}
			relatedsubscriberdatafetch(selectid);
			smscampaignsubaddgrid1();
		} else {
			$('#smssegmentsid').select2('val','');
			alertpopup('Please Select the SMS groups');
		}
	});
	$("#tab2").click(function() {
		var campaignname = $("#smscampaignname").val();
		var groupname = $("#smsgroupsid").val();
		var subscriberid = $("#selectedsegmentsubscriberid").val();
		if(campaignname != '' && groupname != '') {
			if(subscriberid != '') {
				$("#communicationto,#leadtemplatecontent").val('');
				$("#leadtemplatecontentdivhid").find('p').css('opacity','0');
				mobilenumberget(subscriberid);
				$("#smstypeid").select2('val','2');
				defaulttemaplteget(groupname);
				$("#leadtemplatecontent").trigger('focusout');
				Materialize.updateTextFields();
			} else {
				$("#tab1").trigger('click');
				setTimeout(function() {
					//transtion 
					$('#subformspan1').addClass('form-active');
				},700);
				alertpopup('Please select the SMS Campaign subscribers');
			}
		} else {
			$("#tab1").trigger('click');
			setTimeout(function() {
				//transtion 
				$('#subformspan1').addClass('form-active');
			},500);
			alertpopup('Please select the SMS Campaign name and Group name');
		}
	});
	//unique mobile number get
	$("#communicationtodivhid").append('<span id="uniqueid" class="icon icon-filter" style="color:#546E7A;font-size:1rem;cursor:pointer;position:relative;float:right; padding-top:5px;top:3px;" title="Unique"></span><span id="dndnumber" class="icon icon-cancel-circle" title="DND" style="color:#546E7A;font-size:1rem;cursor:pointer;position:relative;float:right; padding-top:5px;top:3px;margin-right:15px;"></span>'); 
	$("#uniqueid").click(function() {
		var mobilenum = $("#communicationto").val();
		setTimeout(function(){
			var splitvalues = mobilenum.split(',');
			var count = splitvalues.length;
			var uniqumobilenum = [];
			$.each(splitvalues, function(i, value){
				if($.inArray(value, uniqumobilenum) === -1) uniqumobilenum.push(value);
			});
			$("#communicationto").val('');
			$("#communicationto").val(jQuery.unique( uniqumobilenum ));
			$("#leadtemplatecontent").trigger('focusout');
			$("#leadtemplatecontent").trigger('keydown');
		},100);
	});
	$("#dndnumber").click(function() {
		var mobilenum = $("#communicationto").val();
		var senderid = $("#senderid").val();
		$("#totalmobilenubers").val('');
		$("#totalnondndnumbers").val('');
		$("#totaldndnumbers").val('');
		$("#nondndnumbers").val('');
		$("#dndnumbers").val('');
		dndnumbers= [];
		nondndnumbers= [];
		invnumbers= [];
		if(senderid != '') {
			var apikey = $("#senderid").find('option:selected').data('apikey');
			if(mobilenum != '') {
				$.ajax({
					url:base_url+"Smscampaign/dndmobilenumbercheck?mobilenum="+mobilenum+"&apikey="+apikey,  
					dataType:'json',
					async:false,
					cache:false,
					success: function(creditdata) {    
						$('#mobiledndovrelay').fadeIn();					
						var len = creditdata.data.length;
						for(var i=0;i<len;i++) {
							if(creditdata.data[i]['status'] == 'YES IN DND') {
								var dndnum = creditdata.data[i]['number'];
								dndnumbers.push(dndnum);								
							} else if(creditdata.data[i]['status'] == 'NOT IN DND'){
								var nondndnum = creditdata.data[i]['number'];
								nondndnumbers.push(nondndnum);		
							} else if(creditdata.data[i]['status'] == 'INVALID'){
								var invalidnumbers = creditdata.data[i]['number'];
								invnumbers.push(invalidnumbers);		
							}
						}
						var splitvalues = mobilenum.split(',');
						var count = splitvalues.length;
						var uniqumobilenum = [];
						$.each(splitvalues, function(i, value){
							if($.inArray(value, uniqumobilenum) === -1) uniqumobilenum.push(value);
						});
						$("#totalmobilenubers").val(count);
						$("#totalinvalidnumbers").val(invnumbers.length);
						$("#totalnondndnumbers").val(nondndnumbers.length);
						$("#totaldndnumbers").val(dndnumbers.length);
						$("#nondndnumbers").val(nondndnumbers);
						$("#dndnumbers").val(dndnumbers);
					}
				});
			}
		} else {
			alertpopup('Please select the sender id');
		}
	});
	//dnd number overlay close
	$("#dndmobileoverlayclose").click(function() {
		$("#totalmobilenubers").val('');
		$("#totalnondndnumbers").val('');
		$("#totaldndnumbers").val('');
		$("#nondndnumbers").val('');
		$("#dndnumbers").val('');
		dndnumbers= [];
		nondndnumbers= [];
		$('#mobiledndovrelay').fadeOut();
	});
	$("#dndyesbtn").click(function() {
		$('#mobiledndovrelay').fadeOut();
		dndnumbers= [];
		nondndnumbers= [];
	});
	$("#dndnobtn").click(function() {
		var nondndnum = $("#nondndnumbers").val();
		$("#communicationto").val(nondndnum);
		$('#mobiledndovrelay').fadeOut();
		$("#leadtemplatecontent").trigger('keydown');
	});
	//template name change
	$('#leadtemplateid').change(function() {
		var tempid = $('#leadtemplateid').val();
		if(tempid) {
			var smssendtype = $("#smssendtypeid").val();
			if(smssendtype) {
				$.ajax( {
					url:base_url+"Smscampaign/fetchtemplatedetails?templid="+tempid, 
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {                
						var msgfilename = data.msg;
						templateeditervalueget(msgfilename);
						$("#signatureid").trigger('change');
					}
				});
			} else {
				$('#leadtemplateid').select2('val','');
				alertpopup('Please Select the Transactionmode');
			}
		} else {
			$("#leadtemplatecontent").text('');
			$("#signatureid").trigger('change');
		}
		Materialize.updateTextFields();
	});	
	//signature file value get
	$("#signatureid").change(function() {
		var sigid = $("#signatureid").val();//typeof
		if(sigid) {
			var smssignature = $(this).find('option:selected').data('smssig');
			var oldcontent = $("#leadtemplatecontent").val();
			var message = oldcontent+''+smssignature;
			var lengthofchar = message.length;
			$("#leadtemplatecontentdivhid").find('p').css('opacity','0');
			smscountdatafetch(message,lengthofchar);
			$("#leadtemplatecontent").val(message);
			$('#description').val(message);
			Materialize.updateTextFields();
		}
	});
	//Template Count
	$('#leadtemplatecontent').keydown(function(e) {
		$("#leadtemplatecontentdivhid").find('p').css('opacity','0');
		var $this = $(this);
		setTimeout(function() {
			var text = $this.val();
			var lengthofchar = text.length;
			smscountdatafetch(text,lengthofchar); 
		}, 0);
	});
	//sms type change
	$("#smstypeid").change(function() {
		var text =$('#leadtemplatecontent').val();
		var lengthofchar = text.length;
		smscountdatafetch(text,lengthofchar);
	});
	//time picker
	$('#communicationtime').timepicker({
		'step':15,
		'timeFormat': 'H:i',
		'scrollDefaultNow':true,
		'disableTextInput':true,//restrict text input in time picker
		'disableTouchKeyboard':true,
	});
	$('#communicationtime').on('changeTime', function() {
		$('#communicationtime').focus();
	});
	{//schedule time
		$("#communicationtime").change(function(){
			var date = $("#communicationdate").val();
			var time = $("#communicationtime").val();
			$.ajax( {
				url:base_url+"Smscampaign/scheduletimecalculate?time="+time+"&date="+date,  
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {                
					if(data != 'True') {
						alertpopup('You cant schedule message on current time');
						$("#communicationtime").val('');
					}
				}
			});
		});
	}
	{//message focus out
		$("#leadtemplatecontent").focusout(function() {
			var recordid = $('#segmentsubscriberid').val();
			var moduleid = $('#defgrpmodid').val();
			var leadtemplateid = $('#leadtemplateid').val();
			if(recordid != '') { 
				recordid = recordid;
			}else {
				recordid = '2';
			}
			var content = $('#leadtemplatecontent').val();
			var regExp = /\{([^}]+)\}/g;
			var datacontent = content.match(regExp);
			if(datacontent != null){
				var newcontent = "";
				$.ajax({
					url:base_url+'Smscampaign/smsmergcontinformationfetch',
					data:'content='+datacontent+"&recordid="+recordid+"&moduleid=271"+"&leadtemplateid="+leadtemplateid,
					type:'POST',
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						var j=0;
						var newcontent = $("#leadtemplatecontent").val();
						var fcontent = '';
						for(var j=0;j < data.length; j++) {
							if(fcontent != '') {
								fcontent = fcontent+'|||'+data[j];
							} else {
								fcontent = data[j];
							}
						}
						$("#description").val(fcontent);
						$("#defaultsubseditorval").val(fcontent);
					},
				});
			} else {
				$('#description').val(content);
			}
		});
	}
	$("#communicationto").focusout(function() {
		var text = $("#leadtemplatecontent").val();
		var lengthofchar = text.length;
		smscountdatafetch(text,lengthofchar);
	});
	{//sms send type change
		$("#smssendtypeid").change(function() {
			var typeid = $("#smssendtypeid").val();
			if(typeid == '2') {
				var addontype = '2';
			} else if(typeid == '3') {
				var addontype = '4';
			} else if(typeid == '4') {
				var addontype = '5';
			} 
			if(typeid == '2'){
				$('#senderid').removeClass('validate[required]');
				$('#senderid').addClass('validate[required]');
			}else {
				$('#senderid').removeClass('validate[required]');
			}
			smssenderidvalueget('senderid');
			creditsdetailfetch(addontype);
			//
		});
	}
	//Available credits check
	$("#senderid").change(function(){
		var apikey = $("#senderid").find('option:selected').data('apikey');
		smstemplateload('leadtemplateid');
	});
	{// Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			}else{
				$('#'+name+'').val('No');
			}
		});
	}
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,smscampaignviewgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			smscampaignviewgrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
});
//drop down values set for view
function smsdropdownmodulevalset(ddname,dataattrname,otherdataattrname1,otherdataattrname2,dataname,dataid,otherdataname1,otherdataname2,datatable,whfield,whvalue) {
	$('#'+ddname+'').empty();
	$('#'+ddname+'').append($("<option></option>"));
	$.ajax({
		url:base_url+'Smsmaster/fetchmaildddatawithmultiplecond?dataname='+dataname+'&dataid='+dataid+'&datatab='+datatable+'&whdatafield='+whfield+'&whval='+whvalue+'&othername1='+otherdataname1+'&othername2='+otherdataname2,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#'+ddname+'')
					.append($("<option></option>")
					.attr("data-"+dataattrname,data[index]['datasid'])
					.attr("data-"+otherdataattrname1,data[index]['otherdataattrname1'])
					.attr("data-"+otherdataattrname2,data[index]['otherdataattrname2'])
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}	
			$('#'+ddname+'').trigger('change');
		},
	});
}
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','ViewCreationName','ViewCreationId','viewcreation','ViewCreationModuleId',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
{// Main Grid Function
	function smscampaignviewgrid(page,rowcount) {
		if(Operation == 1){
			var rowcount = $('#smscampaignpgrowcount').val();
			var page = $('.paging').data('pagenum');
		} else{
			var rowcount = $('#smscampaignpgrowcount').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		}
		var wwidth = $("#smscampaignviewgrid").width();
		var wheight = $(window).height();
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.smscampaignheadercolsort').hasClass('datasort') ? $('.smscampaignheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.smscampaignheadercolsort').hasClass('datasort') ? $('.smscampaignheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.smscampaignheadercolsort').hasClass('datasort') ? $('.smscampaignheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = $('#viewfieldids').val();
		var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
		var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
		var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=smscampaign&primaryid=smscampaignid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#smscampaignviewgrid').empty();
				$('#smscampaignviewgrid').append(data.content);
				$('#smscampaignviewgridfooter').empty();
				$('#smscampaignviewgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('smscampaignviewgrid');
				{//sorting
					$('.smscampaignheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.smscampaignheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#smscampaignpgnumcnt li .page-text .active').data('rowcount');
						var sortcol = $('.smscampaignheadercolsort').hasClass('datasort') ? $('.smscampaignheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.smscampaignheadercolsort').hasClass('datasort') ? $('.smscampaignheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						var sortpos = $('#smscampaignviewgrid .gridcontent').scrollLeft();
						smscampaignviewgrid(page,rowcount);
						$('#smscampaignviewgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('smscampaignheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						Operation = 0;
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						smscampaignviewgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#smscampaignpgrowcount').change(function(){
						Operation = 0;
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = 1;
						smscampaignviewgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//onclick 
				$(".gridcontent").click(function(){
					var datarowid = $('#smscampaignviewgrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				//Material select
				$('#smscampaignpgrowcount').material_select();
			},
		});
	}
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function(e) {
			smscampaignsubaddgrid1();
			//signature drop down load
			smssignaturevalueget('signatureid');
			//sender id drop down load
			e.preventDefault();
			addslideup('smscampaigncreationview','smscampaigncreationformadd');
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			$("#formfields").empty();
			$("#smssendtypeid").val('2').trigger('change');
			$("#templatetypeid").val('2');
			$('#tab1').trigger('click');
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			smssubscriberrefreshgrid();
			//For Touch
			smsmasterfortouch = 0;
			firstfieldfocus();
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#smscampaignpgnum li.active').data('pagenum');
		var rowcount = $('ul#smscampaignpgnumcnt li .page-text .active').data('rowcount');
		smscampaignviewgrid(page,rowcount);
	}
	function smssubscriberrefreshgrid() {
		var page = $('ul#smssubscriberlistpgnum li.active').data('pagenum');
		var rowcount = $('ul.smssubscriberlistpgnumcnt li .page-text .active').data('rowcount');
		smscampaignsubaddgrid1(page,rowcount);
	}
}
//sms master folder  Grid
function smscampaignsubaddgrid1(page,rowcount) {
	var smsgroupsid = $("#smsgroupsid").val();
	var typeid = $("#grouptypeid").val();
	if(smsgroupsid == '') {
		smsgroupsid == '1';
	}
	var segmentid = $("#smssegmentsid").val();
	var smssegmentsubscriber = $("#segmentsubscriberid").val();
	var autoupdate = $("#autoupdate").val();
	if(smssegmentsubscriber == '') {
		smssegmentsubscriber == '';
	}
	var id = '271';
	if(id != "") {
		$.ajax({
			url:base_url+"Smscampaign/defaultviewfetch?modid="+id,
			async:false,
			cache:false,
			success:function(data) {
				if(data != "") {
					viewid = data;
					$("#defaultviewcreationid").val(viewid);
				} else {
					viewid = '28';
					$("#defaultviewcreationid").val(viewid);
				}
			},
		});
		$.ajax({
			url:base_url+"Smscampaign/parenttableget?moduleid="+id,
			dataType:'json',
			async:false,
			cache:false,
			success:function(pdata) {
				if(pdata != "") {
					maintabinfo = pdata;
					$("#parenttable").val(pdata);
					parentid = maintabinfo+"id";
					$("#parenttableid").val(parentid);
				} else {
					maintabinfo = 'smssubscribers';
					parentid = 'smssubscribersid';
					$("#parenttable").val(maintabinfo);
					$("#parenttableid").val(parentid);
				}
			},
		});
	} else {
		viewid = '28';
		maintabinfo = 'subscribers';
		parentid = 'subscribersid';
		$("#defaultviewcreationid").val(viewid);
		$("#parenttable").val(maintabinfo);
		$("#parenttableid").val(parentid);
	}
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#smscampaignsubaddgrid1').width();
	var wheight = $('#smscampaignsubaddgrid1').height();
	//col sort
	var sortcol = $('.smssubscriberlistheadercolsort').hasClass('datasort') ? $('.smssubscriberlistheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.smssubscriberlistheadercolsort').hasClass('datasort') ? $('.smssubscriberlistheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.smssubscriberlistheadercolsort').hasClass('datasort') ? $('.smssubscriberlistheadercolsort.datasort').attr('id') : '0';
	$.ajax({
		url:base_url+"Smscampaign/smssubscribervalueget?maintabinfo="+maintabinfo+"&primaryid="+parentid+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=271'+"&smsgroupsid="+smsgroupsid+"&segmentid="+segmentid+"&smssegmentsubscriber="+smssegmentsubscriber+"&typeid="+typeid+"&autoupdate="+autoupdate+"&checkbox=true",
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#smscampaignsubaddgrid1').empty();
			$('#smscampaignsubaddgrid1').append(data.content);
			$('#smscampaignsubaddgrid1footer').empty();
			$('#smscampaignsubaddgrid1footer').append(data.footer);
			//data row select event
			datarowselectevt();
			/*column resize*/
			columnresize('smscampaignsubaddgrid1');
			{//sorting
				$('.smssubscriberlistheadercolsort').click(function(){
					$('.smssubscriberlistheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('ul#smssubscriberlistpgnum li.active').data('pagenum');
					var rowcount = $('ul#smssubscriberlistpgnumcnt li .page-text .active').data('rowcount');
					var sortpos = $('#smscampaignsubaddgrid1 .gridcontent').scrollLeft();
					smscampaignsubaddgrid1(page,rowcount);
					$('#smscampaignsubaddgrid1 .gridcontent').scrollLeft(sortpos);//scroll to sorted position
				});
				sortordertypereset('smssubscriberlistheadercolsort',headcolid,sortord);
			}
			{//pagination
				/* $('.pagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('ul#smssubscriberlistpgnumcnt li .page-text .active').data('rowcount');
					smscampaignsubaddgrid1(page,rowcount);
				}); */
				$('.pagerowcount').click(function(){
					var page = $('ul#smssubscriberlistpgnum li.active').data('pagenum');
					var rowcount = $(this).data('rowcount');
					smscampaignsubaddgrid1(page,rowcount);
				});
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					//var rowcount = $('ul#smssubscriberlistpgnumcnt li .page-text .active').data('rowcount');
					var rowcount = $('#smssubscriberlistpgrowcount').val();
					smscampaignsubaddgrid1(page,rowcount);
				});
				$('#smssubscriberlistpgrowcount').change(function(){
					var rowcount = $(this).val();
					var page = $('ul#smssubscriberlistpgnum li.active').data('pagenum');
					smscampaignsubaddgrid1(page,rowcount);
				});
			}
			//header check box
			$(".smssubscriberlist_headchkboxclass").click(function() {
				if($(".smssubscriberlist_headchkboxclass").prop('checked') == true) {
					$(".smssubscriberlist_rowchkboxclass").attr('checked', true);
				} else {
					$('.smssubscriberlist_rowchkboxclass').removeAttr('checked');
				}
				getcheckboxrowid();
			});
			//row based check box
			$(".smssubscriberlist_rowchkboxclass").click(function(){
				$('.smssubscriberlist_headchkboxclass').removeAttr('checked');
				getcheckboxrowid();
			});
			//Material select
			$('#smssubscriberlistpgrowcount').material_select();
		}
	});
}
function getcheckboxrowid() {
	var selected = [];
	$('.gridcontent input:checked').each(function() {
		selected.push($(this).attr('data-rowid'));
	});
	var total = $("#smssubscriberlist-sort > .data-rows").length;
	$("#totalsubscribercount").val(total);
	$("#selectedsegmentsubscriberid").val(selected);
	$("#segmentsubscriberid").val(selected);
	$("#validsubscribercount").val(selected.length);
	Materialize.updateTextFields();
}
//new data add submit function
function newdataaddfun() {
    var formdata = $("#dataaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	var message = $("#leadtemplatecontent_editorfilename").val();
	var neweditordata = '&leadtemplatecontent_editorfilename='+message;
    $.ajax(  {
        url: base_url + "Smsmaster/newdatacreate",
        data: "datas=" + datainformation+"&datass="+neweditordata,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				resetFields();
				$('#smsmastercreationformadd').hide();
				$('#smsmastercreationview').fadeIn(1000);
				refreshgrid();
				smssubscriberrefreshgrid();
				alertpopup(savealert);
            }
        },
    });
}
//old information show in form
function editformdatainformationshow(datarowid) {
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementpartable = $('#elementspartabname').val();
	$.ajax(	{
		url:base_url+"Smsmaster/fetchformdataeditdetails?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data)	{
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				$('#smsmastercreationformadd').hide();
				$('#smsmastercreationview').fadeIn(1000);
				refreshgrid();
				//For Touch
				smsmasterfortouch = 0;
			} else {
				var txtboxname = elementsname + ',primarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
				tandceditervalueget(data.leadtemplatecontent_editorfilename);
				setTimeout(function(){
					$("#moduleid").trigger('change');
				}, 500);
			}
		}
	});
}
//editor value fetch
function tandceditervalueget(filename) {
	$.ajax({
		url: base_url+"Smsmaster/editervaluefetch?filename="+filename,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			if(data != 'Fail') {
				$("#leadtemplatecontent_editorfilename").val(data);
			} else {
				$("#leadtemplatecontent_editorfilename").val('');
			}
		},
	});
}
//update old information
function dataupdateinformationfun() {
	var formdata = $("#dataaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	$.ajax({
        url: base_url + "Smsmaster/datainformationupdate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				resetFields();
				$('#smsmastercreationformadd').hide();
				$('#smsmastercreationview').fadeIn(1000);
				refreshgrid();
				smssubscriberrefreshgrid();
				alertpopup(savealert);
				//For Touch
				smsmasterfortouch = 0;
            }
        },
    });
	setTimeout(function()  {
		var closetrigger = ["closeaddleadcreation"];
		triggerclose(closetrigger);
	}, 1000);
}
//udate old information
function recorddelete(datarowid) {
	var elementstable = $('#elementstable').val();
	var elementspartable = $('#elementspartabname').val();
    $.ajax({
        url: base_url + "Smsmaster/deleteinformationdata?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Deleted successfully');
            } else if (nmsg == "false")  {
            	refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Permission denied');
            }
        },
    });
}
{//Related modules drop down load 
	function relatedsubscriberdatafetch(id) {
		$.ajax({
			url:base_url+'Smscampaign/relatedsubscriberidget',
			data:'segmentid='+id,
			type:'POST',
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) != 'FAILED') {
					var autoupdate = data.autoupdate;
					$("#autoupdate").val(autoupdate);
					$("#segmentsubscriberid").val(data.subscriberid);
					$("#segmentsubscriberid").val($("#segmentsubscriberid").val());
				}
			},
		});
	}
}
//total subscriber data getAttention
function totalsubscriberget(datarowid) {
	subscriber.push(datarowid);
	$("#totalsubscribercount").val(datarowid.length);
	$("#selectedsegmentsubscriberid").val(datarowid);
	var selsegment = $("#selectedsegmentsubscriberid").val();
	$("#validsubscribercount").val(datarowid.length);
}
//mobile number get
function mobilenumberget(subscriberid) {
	$.ajax({
		url:base_url+'Smscampaign/mobilenumberget',
		data:'subscriberid='+subscriberid,
		type:'POST',
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) != 'FAILED') {
				$("#communicationto").val(data);
			}
		},
	});
}
//Template value fetch
function templateeditervalueget(filename) {
	if(filename != '') {
		$.ajax({
			url: base_url+"Smscampaign/editervaluefetch?filename="+filename,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if(data != 'Fail') {
					var lengthofchar = data.length;
					$("#leadtemplatecontentdivhid").find('p').css('opacity','0');
					smscountdatafetch(data,lengthofchar);
					$("#leadtemplatecontent").val(data);
					$('#description').val(data);
				} else {
					$("#leadtemplatecontentdivhid").find('p').css('opacity','0');
					$("#leadtemplatecontent").val('');
				}
			},
		});
	}
}
{//SMS Signature value get
	function smssignaturevalueget(ddname) {
		$('#'+ddname+'').empty();
		$('#'+ddname+'').append($("<option></option>"));
		$.ajax({
			url: base_url+"Smscampaign/smssignatureddvalfetch",
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#'+ddname+'')
						.append($("<option></option>")
						.attr("data-smssig",data[index]['smssig'])
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
					});
				}	
				$('#'+ddname+'').trigger('change');
			},
		});
	}
}
//credit detail fetch function
function creditsdetailfetch(addontype) {
	$.ajax({
		url: base_url + "Smscampaign/accountcresitdetailsfetch",
		data: "addontype=" + addontype,
		type: "POST",
		cache:false,
		success: function(creditdata) {
			if((creditdata.fail) != 'FAILED') {
				$("#totalavailabecredit").val(creditdata);
			}
		},
	});
}
//default template and signature get
function defaulttemaplteget(groupname) {
	$.ajax({
		url: base_url + "Smscampaign/defeulttemplatevalueget",
		data: "groupid=" + groupname,
		type: "POST",
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) != 'FAILED') {
				var template = data.template;
				var signature = data.signature;
				var senderid = data.senderid;
				var smstype = data.smstype;
				var moduleid = data.moduleid;
				$("#senderid").select2('val',senderid);
				$("#senderid").trigger('change');
				$("#smstypeid").select2('val',smstype);
				$("#smstypeid").trigger('change');
				//template value load
				var checkmodule = 	moduleid.split(',');
				if(checkmodule.length == '1'){
					if(checkmodule == '1'){
						checkmodule = '271';
						groupname = '2';
					}
					var fromappmod = checkmodule;
					var grouptype = groupname;
					$("#defgrpid").val(grouptype);
					$("#defgrpmodid").val(fromappmod);
				} else {
					var fromappmod = '271';
					var grouptype = '2';
					$("#defgrpid").val(grouptype);
					$("#defgrpmodid").val(fromappmod);
				}
				$("#signatureid").select2('val',signature);
				if(signature != '' && signature != 1) {
					$("#signatureid").trigger('change');
				}
			}
		},
	});
}
//sms count get
function smscountdatafetch(text,lengthofchar) {
	var typeid = $("#smstypeid").val();
	if( typeid == '' ) { 
		typeid == '2';
	}
	var smscount = 0;
	if(typeid == '2' || typeid == '3') {
		var maxcount = 1000;
		if(lengthofchar <= '160') {
			smscount = 1;
		} else if((lengthofchar > '160') && (lengthofchar <= '306')) {
			smscount = 2;	
		} else if((lengthofchar > '306') && (lengthofchar <= '459')) {
			smscount = 3;	
		} else if((lengthofchar > '459') && (lengthofchar <= '612')) {
			smscount = 4;	
		} else if((lengthofchar > '612') && (lengthofchar <= '765')) {
			smscount = 5;	
		} else if((lengthofchar > '765')) {
			smscount = 6;	
		}
	} else if(typeid == '4') {
		var maxcount = 500;
		if(lengthofchar <= '70') {
			smscount = 1;
		}else if((lengthofchar > '70') && (lengthofchar <= '134')) {
			smscount = 2;	
		} else if((lengthofchar > '134') && (lengthofchar <= '201')) {
			smscount = 3;	
		} else if((lengthofchar > '201') && (lengthofchar <= '268')) {
			smscount = 4;	
		} else if((lengthofchar > '268') && (lengthofchar <= '335')) {
			smscount = 5;	
		} else if((lengthofchar > '335') && (lengthofchar <= '402')) {
			smscount = 6;	
		} else if((lengthofchar > '402') ) {
			smscount = 7;	
		}
	}
	$("#smscount").val(smscount);
	$("#leadtemplatecontentdivhid").find('p').remove();
	$("#leadtemplatecontentdivhid").append('<p align="right" style="color:#546E7A;font-size:0.8rem; margin:0;opacity:1;"<span style="color:#546E7A;font-size:0.7rem; padding-left:30px;">'+lengthofchar+' characters(s) <span style="padding-left:30px;">'+smscount+' SMS</span><span style="padding-left:30px;"> Max '+maxcount+' Chars</span></span></p>');
	if(lengthofchar > 0) {
	} else { 
		$("#leadtemplatecontentdivhid").find('span').remove();
	}
	//total sms campaign message count 
	var mobnum = $("#communicationto").val();
	var nsplit = mobnum.split(',');
	var mlength = nsplit.length;
	var totalcampsms = mlength * smscount; 
	$("#totalsmscreditofcampaign").val(totalcampsms)
}
{//used for check the selected values in subscriber grid
	function checktheselectedinvitevalues() {
		var subscriberid = $("#selectedsegmentsubscriberid").val();
		if(subscriberid != '') {
			var subid = subscriberid.split(',');
			checkboxcheck(subid);
		}
	}
	function checkboxcheck(subid) {
		for(var i=0;i <subid.length;i++){
			jQuery("#smscampaignsubaddgrid1").setSelection(subid[i],true);
		}
	}
}
//group type get
function grouptypeget(groupid) {
	$.ajax({
		url:base_url+'Smscampaign/grouptypeidget',
		data:'groupid='+groupid,
		type:'POST',
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) != 'FAILED') {
				var typeid = data.typeid;
				var moduleid = data.moduleid;
				$("#grouptypeid").val(typeid);
				$("#groupmoduleid").val(moduleid);
			}
		},
	});
} 
//valid mobile number get
function validmobilenumberget(mobilenum) {
	var apikey = $("#senderid").find('option:selected').data('apikey');
	if(mobilenum != '' && apikey != '') {
		$.ajax({
			url:base_url+"Smscampaign/dndmobilenumbercheck?mobilenum="+mobilenum+"&apikey="+apikey,  
			dataType:'json',
			async:false,
			cache:false,
			success: function(creditdata) {    
				var len = creditdata.data.length;
				for(var i=0;i<len;i++) {
					if(creditdata.data[i]['status'] == 'YES IN DND' || creditdata.data[i]['status'] == 'NOT IN DND') {
						var validnumbers = creditdata.data[i]['number'];
						valnumbers.push(validnumbers);								
					}
				}
				$("#communicationto").val(jQuery.unique(valnumbers));
			}
		});
	}
}
//message content get
function messagecontentget(subsid) {
	var ncontent = "";
	var fcontent = "";
	var moduleid = $('#defgrpmodid').val();
	var leadtemplateid = $('#leadtemplateid').val();
	var content = $('#leadtemplatecontent').val();
	var regExp = /\{([^}]+)\}/g;
	var datacontent = content.match(regExp);
	if(datacontent != null) {
		$.ajax({
			url:base_url+'Smscampaign/smsmergcontinformationfetch',
			data:'content='+datacontent+"&recordid="+subsid+"&moduleid=271"+"&leadtemplateid="+leadtemplateid,
			type:'POST',
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				for(var j=0;j < data.length; j++) {
					if(fcontent != '') {
						fcontent = fcontent+'|||'+data[j];
					} else {
						fcontent = data[j];
					}
				}
				$("#description").val(fcontent);
				$("#defaultsubseditorval").val(fcontent);
			},
		});
	}
}
//sms template load
function smstemplateload(ddname) {
	var moduleid = $("#defgrpmodid").val();
	var grouptypeid = $("#defgrpid").val();
	if(!grouptypeid){
		grouptypeid = 1;
	}
	if(!moduleid){
		moduleid = 1;
	}
	var senderid = $("#senderid").val();
	var smsendtype = $("#smssendtypeid").val();
	if(senderid) {
		$('#'+ddname+'').empty();
		$('#'+ddname+'').append($("<option></option>"));
		$.ajax({
			url: base_url+"Smscampaign/smstemplateddvalfetch?moduleid="+moduleid+"&grouptype="+grouptypeid+"&senderid="+senderid+"&smsendtype="+smsendtype,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
					if(smsendtype == '2') {
						alertpopup('Template not avaliable for selected Sender ID and Transaction mode.');
					} else {
						alertpopup('Template not avaliable for selected Transaction mode.');
					}
				} else {
					$.each(data, function(index) {
						$('#'+ddname+'')
						.append($("<option></option>")
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
					});
				}	
			}
		});
	} else {
	}
} 
//segments dd load load
function segmentdropdownvalfetch(listid) {
	$('#smssegmentsid').empty();
	$('#smssegmentsid').append($("<option></option>"));
	$.ajax({
		url: base_url+"Smscampaign/segmentddload?listid="+listid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#smssegmentsid')
					.append($("<option></option>")
					.attr("data-smssegmentsidhidden",data[index]['dataname'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}	
		}
	});
}
{//Sender Id drop down value get
	function smssenderidvalueget(ddname) {
		var typeid = $("#smssendtypeid").val();
		$('#'+ddname+'').empty();
		$('#'+ddname+'').append($("<option></option>"));
		$.ajax({
			url: base_url+"Smscampaign/smssenderddvalfetch?typeid="+typeid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#'+ddname+'')
						.append($("<option></option>")
						.attr("data-apikey",data[index]['apikey'])
						.attr("data-sendername",data[index]['dataname'])
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
					});
				}	
				$('#'+ddname+'').trigger('change');
			},
		});
	}
}