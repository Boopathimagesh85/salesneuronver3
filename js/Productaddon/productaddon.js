$(document).ready(function() {
		
		$(document).foundation();
		POUOM = 'ADD';
	    POROL = 'ADD';
	    POSTORAGE = 'ADD';
	    POATTR = 'ADD';
	   	UPDATEID = 0;
	    productaddonedit = 0;
	    PRODUCTADDONDELETE = 0;
	    // Maindiv height width change
	    maindivwidth();
		var maindiv = $('.maindiv').width();
		var a = screen.width;
		reportsviewgridwidth = ((a * maindiv)/100);
		weight_round = $('#weight_round').val();
		amount_round = $('#amount_round').val();
		chargecalculation = $('#chargecalculation').val();
	    purchasewastagestatus = $('#purchasewastagestatus').val();
		chargecalcoption = $('#chargecalcoption').val();
	  	 $('#groupcloseaddform').hide();
		 $('#productaddonviewtoggle').hide();
		{// Grid Calling Functions
		    producttypegrid();
		}
		$( window ).resize(function() {
			 sectionpanelheight('groupsectionoverlay');
			 maingridresizeheightset('producttypegrid');
		 });
       {// For touch
			fortabtouch = 0;
		}
		{ // Overlay form modules - filter display
			$('#viewoverlaytoggle').on('click',function() {
				$('#filterdisplaymainviewnonbase').css({"display":"block"});
			});
			$('.nbfilterdisplaymainviewclose').on('click',function() {
				$('#filterdisplaymainviewnonbase').css({"display":"none"});
			});
		}
		
	  /* Vardaan addon Integration 
	*/		
		{//product charge validate	
			//validation for product charge Add  
			$('#productchargeadd').click(function() 
			{
				$("#productaddonvalidate").validationEngine('validate');
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			});
			jQuery("#productaddonvalidate").validationEngine(
			{
				onSuccess: function()
				{
			      productchargeadd();
                },
				onFailure: function()
				{
						var dropdownid =['2','chargeproductid','addon_purityid'];
						dropdownfailureerror(dropdownid);
				}
			});
			//update product information.
			$('#productchargeupdate').click(function()
			{
				$("#productupdateonvalidate").validationEngine('validate');
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			});
			jQuery("#productupdateonvalidate").validationEngine({
				onSuccess: function()
				{
					productchargeupdate();
				},
				onFailure: function()
				{
					var dropdownid =['3','chargenameid','chargetypeid','chargepurityid'];
					dropdownfailureerror(dropdownid);
				}	
			});
			
			$("#editicon").click(function(){			
				var datarowid = $('#producttypegrid div.gridcontent div.active').attr('id');
				if(datarowid){
					sectionpanelheight('groupsectionoverlay');
					$("#groupsectionoverlay").removeClass("closed");
					$("#groupsectionoverlay").addClass("effectbox");
					$("#chargeproductid,#addon_purityid,#accounttypeid,#chargeaccountgroup,#salestransactiontypeid").prop('disabled',true);
					$("#chargeendweight").prop('readonly',true);
					productchargeretrive(datarowid);
					$('#productchargeupdate').show();
					$('#productchargeadd,#productaddondeleteicon').hide();
				} else {
					alertpopup(selectrowalert);
				}
			});
	
			//Delete
			$("#deleteicon").click(function(){			
				var datarowid = $.trim($('#producttypegrid div.gridcontent div.active').attr('id'));
				if(datarowid != ''){
					var status = 'false';
						status = checkproductaddon(datarowid); 
							if(status == 'true'){ 
								alertpopupdouble('Restrict to delete. Because the existing records are having higher than selected range.');
								return false;
							}
							if(status == 'false') {
								combainedmoduledeletealert('addondeleteyes','Are you sure,you want to delete this addon?');
								if(PRODUCTADDONDELETE == 0) {
									$("#addondeleteyes").click(function(){				
										productchargedelete();
									});
								}
								PRODUCTADDONDELETE = 1;
							}
				}else{
					alertpopup(selectrowalert);
				}
			});
		}
		
	 
	// purity select single group
		 $("#purityid").change(function(){	
			var label=$('#purityid :selected').parent().attr('label');
			if($(this).val()==null)
			{
				$("#purityid option").attr("disabled", false); 
			}
			else{
				$("#purityid optgroup > option[label!="+label+"]").attr('disabled','disabled');
				$("#purityid optgroup[label="+label+"]").children().removeAttr('disabled');
			}
		 }); 
     // charge applicable optgroup select
		$("#chargeid").change(function(){	
				
			$("#chargeid option").siblings("option").prop('disabled', false);
			if($(this).val()==null || $(this).val()== '')
			{
			$("#chargeid option").attr("disabled", false); 
			}
			else{
			$("#chargeid option").filter(":selected").siblings("option").prop('disabled', true);
			}			
		 });
		$("#purchasechargeid").change(function(){	
			
			$("#purchasechargeid option").siblings("option").prop('disabled', false);
			if($(this).val()==null || $(this).val()== '')
			{
			$("#purchasechargeid option").attr("disabled", false); 
			}
			else{
			$("#purchasechargeid option").filter(":selected").siblings("option").prop('disabled', true);
			}			
		 });
		 //product changes//
		$("#chargeproductid").change(function(){
			var val = $(this).val();
			$('#chargeaccountgroup').select2('val','');
			if(productaddonedit == 0){
			  $('#chargestartweight,#chargeendweight,.chargedata').val('');
			}
			// set purity based on product
            var purityid=$("#chargeproductid").find('option:selected').data('purityid');
            if(purityid != '') {
            	 purityid=purityid.toString();
            	 purityshowhide(purityid);
                if (purityid.indexOf(',') > -1) { 
                    var dataarray = purityid.split(",");
					$('#addon_purityid').select2('val','');
					$('#addon_purityid').prop('disabled',false);
                 } else { 
                    if(purityid != 1){
						$('#addon_purityid').select2('val',purityid).trigger('change');
						$('#addon_purityid').prop('disabled',true);
                    }
                }
            }
			Materialize.updateTextFields();
		});
		$("#addon_purityid").change(function(){
			if(productaddonedit == 0){
				$('#accounttypeid').select2('val',6).trigger('change');
			}
		});
	$('.addsectionclose').click(function(){
		$("#productaddonsectionoverlay").addClass("closed");
		$("#productaddonsectionoverlay").removeClass("effectbox");
	});
	{// Counter hierarchy
		$('#productlistuldata li').click(function() {
			var name=$(this).attr('data-listname');
			var listid=$(this).attr('data-listid');
			var primaryid = $("#primarydataid").val();
			$('#parentproduct').val(name);
			$('#categoryid').val(listid);
		});	
	}
	$('#accounttypeid').change(function(){
		var val = $(this).val();
		var accountgroupid = $('#accountgroupid').val();
		var accounttypeidcompany = $('#accounttypeidcompany').val();
		if(accountgroupid == ''){
		  $("#chargeaccountgroup").select2('val',1).trigger('change');
		}else{ // account group set based on company setting
		  if(val == accounttypeidcompany){
		     $("#chargeaccountgroup").select2('val',accountgroupid).trigger('change');
		  }else{
			 $("#chargeaccountgroup").select2('val',1).trigger('change');
		  }
		  if(val == 16){
			  $('.modediv').removeClass('hidedisplay');
			  $('#salestransactiontypeid').attr('data-validation-engine','validate[required]');
			  $('#salestransactiontypeid').select2('val',9).trigger('change');
		  }else{
			  $('.modediv').addClass('hidedisplay');
			  $('#salestransactiontypeid').attr('data-validation-engine','');
			  $('#salestransactiontypeid').select2('val','');
		  }
		}
		var salestransactiontypeid = $('#salestransactiontypeid').val();
		if(salestransactiontypeid == 9 && purchasewastagestatus == 'Yes'){ // purchase
			$('#chargewastageweightdiv').removeClass('hidedisplay');
			$('#chargewastageweight').attr('data-validation-engine','validate[required,decval['+weight_round+'],custom[number],min[0.001]]');
		}else{
			$('#chargewastageweightdiv').addClass('hidedisplay');
			$('#chargewastageweight').attr('data-validation-engine','');
		}
		if(productaddonedit == 0){
			$("#chargestartweight,#chargeendweight,#chargewastageweight").val('0');
		}
		accountgroupshowhide();
		$("#productaddonform .chargedetailsdiv").addClass('hidedisplay');
		$("#productaddonform .chargedetailsdiv .chargedata").val(0);
		$("#productaddonform .chargedetailsdiv .chargedata").attr('data-validation-engine','');
		var productid=$('#chargeproductid').val();
		var purityid=$('#addon_purityid').val();
		var accountgroupid=$('#chargeaccountgroup').val();
		if(productaddonedit == 0){
			getmaxcharge(productid,purityid,val,accountgroupid);
		}
		if(val == 6){
			var chargeid=$("#chargeproductid").find('option:selected').data('chargeid');
		}else if(val == 16){
			var chargeid=$("#chargeproductid").find('option:selected').data('purchasechargeid');
		}
		$.ajax({
		url:base_url+"Productaddon/chargedetails",
		data:{chargeid:chargeid},
		type: "POST",
		dataType:'json',
		async:false,
		cache:false,
		success:function(msg) {
			 chargecategoryid = msg['chargekeyword'];
		},
	   });	
	   $('.addonchargeiconchange').removeClass('hidedisplay');
		if(chargecategoryid != '' || chargecategoryid != 1) {
			var chargecategoryidstring =chargecategoryid.toString();
						var makingkey=[];
						var wastagekey=[];
						var hallmarkkey=[];
						var flatkey=[];
						var range = '';
						 var firstindexwastage = 0;
						 var firstindexwastagecount = 0;
						 var firstwastagecharge = '';
						 var firstwastagechargename = '';
						 var firstwastagechargeid = '';
						 var firstwastagechargekey = '';
						 var firstindexmaking = 0;
						 var firstindexmakingcount = 0;
						 var firstmakingcharge ='';
						 var firstmakingchargename ='';
						 var firstmakingchargeid ='';
						 var firstmakingchargekey ='';
						 var firstindexhallmark = 0;
						 var firstindexhallmarkcount = 0;
						 var firsthallmarkcharge ='';
						 var firsthallmarkchargename ='';
						 var firsthallmarkchargeid ='';
						 var firsthallmarkchargekey ='';
						 var firstindexflat = 0;
						 var firstindexflatcount = 0;
						 var firstflatcharge ='';
						 var firstflatchargename ='';
						 var firstflatchargeid ='';
						 var firstflatchargekey ='';
			if (chargecategoryidstring.indexOf(',') > -1) {
				var chargedetails=chargecategoryidstring.split(",");
				if(chargedetails == ''){
					}else{
						$.each(chargedetails, function (index, value) {
							var splitdatakey = value.split(":");
							$("#productaddonform .charge"+splitdatakey[0]).removeClass('hidedisplay');
							var minvalidate = 0.001;
							if(splitdatakey[0] == 6) {// flat charges no min value checking
								var minvalidate = 0;
							}
							$("#productaddonform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[number],min['+minvalidate+']]');
						    if(chargecalcoption == 1){
								  range = 'MIN';
							}else if(chargecalcoption == 2){
								  range = 'MAX';
							}
							if(splitdatakey[0] == '2'){  // making
								if(splitdatakey[2] == 'MC-GM-N' || splitdatakey[2] == 'MC-GM-G' || splitdatakey[2] == 'MC-FT') { // making charges
									$("#productaddonform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[number],decval['+amount_round+'],min[0.001],maxSize[15]]');
								} else if(splitdatakey[2] == 'MC-PI') { //per piece wise
									$("#productaddonform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[onlyWholeNumber],min[1],maxSize[15]]');
								}
							    if(firstindexmaking == 0){
								   firstindexmaking++;
							       firstmakingcharge = splitdatakey[2]+'-'+range;
                             	   firstmakingchargename = splitdatakey[1];
								   firstmakingchargeid = splitdatakey[3];
                                   firstmakingchargekey = splitdatakey[2]+'.'+range;								   
								}
								firstindexmakingcount++;
								var makingfinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
								makingkey.push(makingfinal);
								$('#makingiconchange').attr('keywordarray',''+makingkey+'');
								$('#makingiconchange').attr('keywordcalc',''+firstmakingcharge+'');
								$('#making').attr('keyword',''+firstmakingcharge+'');
								$('#making').attr('keywordcalc',''+firstmakingchargekey+'');
								$('#making').attr('chargeid',''+firstmakingchargeid+'');
								$('#makingspan').attr('spankeyword',''+firstmakingcharge+'');
								$('#makingspan').text(''+firstmakingchargename+'');
								if(firstindexmakingcount == 1){
									$('#makingiconchange').addClass('hidedisplay');
								}else{
									$('#makingiconchange').removeClass('hidedisplay');
								}
							}else if(splitdatakey[0] == '3'){ // wastage
								if(splitdatakey[2] == 'WS-WT') { // wastage flat weight
									$("#productaddonform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[number],decval['+weight_round+'],min[0.001],maxSize[15]]');
								} else if(splitdatakey[2] == 'WS-PT-N' || splitdatakey[2] == 'WS-PT-G' || splitdatakey[2] == 'TV') { //except wastage flat weight
									$("#productaddonform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[number],decval['+2+'],min[0.01],maxSize[15]]');
								}
							    if(firstindexwastage == 0){
									firstindexwastage++;
								    firstwastagecharge = splitdatakey[2]+'-'+range;
									firstwastagechargename = splitdatakey[1];	
									firstwastagechargeid = splitdatakey[3];
									firstwastagechargekey = splitdatakey[2]+'.'+range;	
								}
								firstindexwastagecount++;
								var wastagefinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
								wastagekey.push(wastagefinal);
								$('#wastageiconchange').attr('keywordarray',''+wastagekey+'');
								$('#wastageiconchange').attr('keywordcalc',''+firstwastagecharge+'');
								$('#wastage').attr('keyword',''+firstwastagecharge+'');
								$('#wastage').attr('keywordcalc',''+firstwastagechargekey+'');
								$('#wastage').attr('chargeid',''+firstwastagechargeid+'');
								$('#wastagespan').attr('spankeyword',''+firstwastagecharge+'');
								$('#wastagespan').text(''+firstwastagechargename+'');
								if(firstindexwastagecount == 1){
									$('#wastageiconchange').addClass('hidedisplay');
								}else{
									$('#wastageiconchange').removeClass('hidedisplay');
								}
							}else if(splitdatakey[0] == '4'){ // hallmark
								if(splitdatakey[2] == 'HM-FT') { // flat charges
									$("#productaddonform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[number],decval['+amount_round+'],min[0.001],maxSize[15]]');
								} else if(splitdatakey[2] == 'HM-PI') { //per pieces wise
									$("#productaddonform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[onlyWholeNumber],min[1],maxSize[15]]');
								}
							    if(firstindexhallmark == 0){
									firstindexhallmark++;
								    firsthallmarkcharge = splitdatakey[2]+'-'+range;
									firsthallmarkchargename = splitdatakey[1];	
									firsthallmarkchargeid = splitdatakey[3];
									firsthallmarkchargekey = splitdatakey[2]+'.'+range;	
								}
								firstindexhallmarkcount++;
								var hallmarkfinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
								hallmarkkey.push(hallmarkfinal);
								$('#hallmarkiconchange').attr('keywordarray',''+hallmarkkey+'');
								$('#hallmarkiconchange').attr('keywordcalc',''+firsthallmarkcharge+'');
								$('#hallmark').attr('keyword',''+firsthallmarkcharge+'');
								$('#hallmark').attr('keywordcalc',''+firsthallmarkchargekey+'');
								$('#hallmark').attr('chargeid',''+firsthallmarkchargeid+'');
								$('#hallmarkspan').attr('spankeyword',''+firsthallmarkcharge+'');
								$('#hallmarkspan').text(''+firsthallmarkchargename+'');
								if(firstindexhallmarkcount == 1){
									$('#hallmarkiconchange').addClass('hidedisplay');
								}else{
									$('#hallmarkiconchange').removeClass('hidedisplay');
								}
							}else if(splitdatakey[0] == '6'){  // flat
								if(splitdatakey[2] == 'FC-WT') { // flat weight
									$("#productaddonform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[number],decval['+weight_round+'],min[0],maxSize[15]]');
								} else if(splitdatakey[2] == 'FC') { // flat charges
									$("#productaddonform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[number],decval['+amount_round+'],min[0],maxSize[15]]');
								}
								if(firstindexflat == 0){
								   firstindexflat++;
								   firstflatcharge = splitdatakey[2]+'-'+range;
								   firstflatchargename = splitdatakey[1];
								   firstflatchargeid = splitdatakey[3];
								   firstflatchargekey = splitdatakey[2]+'.'+range;								   
								}
								firstindexflatcount++;
								var flatfinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
								flatkey.push(flatfinal);
								$('#flaticonchange').attr('keywordarray',''+flatkey+'');
								$('#flaticonchange').attr('keywordcalc',''+firstflatcharge+'');
								$('#flat').attr('keyword',''+firstflatcharge+'');
								$('#flat').attr('keywordcalc',''+firstflatchargekey+'');
								$('#flat').attr('chargeid',''+firstflatchargeid+'');
								$('#flatspan').attr('spankeyword',''+firstflatcharge+'');
								$('#flatspan').text(''+firstflatchargename+'');
								if(firstindexflatcount == 1){
									$('#flaticonchange').addClass('hidedisplay');
								}else{
									$('#flaticonchange').removeClass('hidedisplay');
								}
							}
							if(splitdatakey[0] == 5) { //Certification flat
								$("#productaddonform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[number],decval['+amount_round+'],min[0.001],maxSize[15]]');
							}
                         });
					}
			}else{
					var splitdatakey = chargecategoryidstring.split(":");
					$("#productaddonform .charge"+splitdatakey[0]).removeClass('hidedisplay');
					var minvalidate = 0.001;
					if(splitdatakey[0] == 6) {// flat charges no min value checking
					 var minvalidate = 0;
					}
					$("#productaddonform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[number],min['+minvalidate+']]');
					
					if(chargecalcoption == 1){
						  range = 'MIN';
					}else if(chargecalcoption == 2){
						  range = 'MAX';
					}
					if(splitdatakey[0] == '2'){  // making
						if(splitdatakey[2] == 'MC-GM-N' || splitdatakey[2] == 'MC-GM-G' || splitdatakey[2] == 'MC-FT') { // making charges
							$("#productaddonform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[number],decval['+amount_round+'],min[0.001],maxSize[15]]');
						} else if(splitdatakey[2] == 'MC-PI') { //per pieces wise
							$("#productaddonform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[onlyWholeNumber],min[1],maxSize[15]]');
						}
						if(firstindexmaking == 0){
						   firstindexmaking++;
						   firstmakingcharge = splitdatakey[2]+'-'+range;
						   firstmakingchargename = splitdatakey[1];
						   firstmakingchargeid = splitdatakey[3];
						   firstmakingchargekey = splitdatakey[2]+'.'+range;								   
						}
						var makingfinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
						makingkey.push(makingfinal);
						$('#makingiconchange').attr('keywordarray',''+makingkey+'');
						$('#makingiconchange').attr('keywordcalc',''+firstmakingcharge+'');
						$('#making').attr('keyword',''+firstmakingcharge+'');
						$('#making').attr('keywordcalc',''+firstmakingchargekey+'');
						$('#making').attr('chargeid',''+firstmakingchargeid+'');
						$('#makingspan').attr('spankeyword',''+firstmakingcharge+'');
						$('#makingspan').text(''+firstmakingchargename+'');
					}else if(splitdatakey[0] == '3'){ // wastage
						if(splitdatakey[2] == 'WS-WT') { // wastage flat weight
							$("#productaddonform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[number],decval['+weight_round+'],min[0.001],maxSize[15]]');
						} else if(splitdatakey[2] == 'WS-PT-N' || splitdatakey[2] == 'WS-PT-G' || splitdatakey[2] == 'TV') { //except wastage flat weight
							$("#productaddonform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[number],decval['+2+'],min[0.01],maxSize[15]]');
						}
						if(firstindexwastage == 0){
							firstindexwastage++;
							firstwastagecharge = splitdatakey[2]+'-'+range;
							firstwastagechargename = splitdatakey[1];	
							firstwastagechargeid = splitdatakey[3];
							firstwastagechargekey = splitdatakey[2]+'.'+range;	
						}
						var wastagefinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
						wastagekey.push(wastagefinal);
						$('#wastageiconchange').attr('keywordarray',''+wastagekey+'');
						$('#wastageiconchange').attr('keywordcalc',''+firstwastagecharge+'');
						$('#wastage').attr('keyword',''+firstwastagecharge+'');
						$('#wastage').attr('keywordcalc',''+firstwastagechargekey+'');
						$('#wastage').attr('chargeid',''+firstwastagechargeid+'');
						$('#wastagespan').attr('spankeyword',''+firstwastagecharge+'');
						$('#wastagespan').text(''+firstwastagechargename+'');
					}else if(splitdatakey[0] == '4'){ // hallmark
						if(splitdatakey[2] == 'HM-FT') { // flat charges
							$("#productaddonform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[number],decval['+amount_round+'],min[0.001],maxSize[15]]');
						} else if(splitdatakey[2] == 'HM-PI') { //per pieces wise
							$("#productaddonform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[onlyWholeNumber],min[1],maxSize[15]]');
						}
						if(firstindexhallmark == 0){
							firstindexhallmark++;
							firsthallmarkcharge = splitdatakey[2]+'-'+range;
							firsthallmarkchargename = splitdatakey[1];	
							firsthallmarkchargeid = splitdatakey[3];
							firsthallmarkchargekey = splitdatakey[2]+'.'+range;	
						}
						var hallmarkfinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
						hallmarkkey.push(hallmarkfinal);
						$('#hallmarkiconchange').attr('keywordarray',''+hallmarkkey+'');
						$('#hallmarkiconchange').attr('keywordcalc',''+firsthallmarkcharge+'');
						$('#hallmark').attr('keyword',''+firsthallmarkcharge+'');
						$('#hallmark').attr('keywordcalc',''+firsthallmarkchargekey+'');
						$('#hallmark').attr('chargeid',''+firsthallmarkchargeid+'');
						$('#hallmarkspan').attr('spankeyword',''+firsthallmarkcharge+'');
						$('#hallmarkspan').text(''+firsthallmarkchargename+'');
					 }else if(splitdatakey[0] == '6'){  // flat
						if(splitdatakey[2] == 'FC-WT') { // flat weight
							$("#productaddonform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[number],decval['+weight_round+'],min[0],maxSize[15]]');
						} else if(splitdatakey[2] == 'FC') { // flat charges
							$("#productaddonform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[number],decval['+amount_round+'],min[0],maxSize[15]]');
						}
						if(firstindexflat == 0){
						   firstindexflat++;
						   firstflatcharge = splitdatakey[2]+'-'+range;
						   firstflatchargename = splitdatakey[1];
						   firstflatchargeid = splitdatakey[3];
						   firstflatchargekey = splitdatakey[2]+'.'+range;								   
						}
						var flatfinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
						flatkey.push(flatfinal);
						$('#flaticonchange').attr('keywordarray',''+flatkey+'');
						$('#flaticonchange').attr('keywordcalc',''+firstflatcharge+'');
						$('#flat').attr('keyword',''+firstflatcharge+'');
						$('#flat').attr('keywordcalc',''+firstflatchargekey+'');
						$('#flat').attr('chargeid',''+firstflatchargeid+'');
						$('#flatspan').attr('spankeyword',''+firstflatcharge+'');
						$('#flatspan').text(''+firstflatchargename+'');
					}
					if(splitdatakey[0] == 5) { //Certification flat
						$("#productaddonform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[number],decval['+amount_round+'],min[0.001],maxSize[15]]');
					}
					$('.addonchargeiconchange').addClass('hidedisplay');
				}
      }
		Materialize.updateTextFields();
	});
	$('.chargedata').change(function(){
		var minid = $(this).attr('minid');
		var maxid = $(this).attr('maxid');
		var minvalue = $("#"+minid+"").val();
		var maxvalue = $("#"+maxid+"").val();
		if(parseInt(minvalue) >= parseInt(maxvalue)){
			if(parseInt(maxvalue) != 0){
			  alertpopup('Please Enter The Maximum Vale Greaterthan '+minvalue+'');
			  $(this).val('');
			}
		}
	});
	$('#chargeaccountgroup').change(function(){
		var productid=$('#chargeproductid').find('option:selected').val();
		var purityid=$('#addon_purityid').find('option:selected').val();
		var accountgroupid=$('#chargeaccountgroup').find('option:selected').val();
		var accounttypeid=$('#accounttypeid').find('option:selected').val();
		if(productaddonedit == 0){
			getmaxcharge(productid,purityid,accounttypeid,accountgroupid);
		}
	});
	$('#cloneicon').click(function(){
		$('#productaddoncopyalertsdouble').fadeIn();
		loadchargedproduct();
		productchargecopygrid();
		$("#copytopcat").select2('val',0).trigger('change');
	});
	$("#copytopcat").change(function(){	
			var val = $(this).val();
			$("#clonecategoryid").select2('val','').trigger('change');
			if(val == 0){
				 $('#productaddonchargegrid').show(); 
				 $('#cateogrydiv').hide(); 
			}else if(val == 1){
				$('#productaddonchargegrid').hide(); 
				 $('#cateogrydiv').show(); 
			}
		 });
	$('#chargedproduct').change(function(){
		var productid = $(this).val();
		var chargeid = $(this).find('option:selected').attr('data-chargeid');
		var purityid = $(this).find('option:selected').attr('data-purityid');
		cleargriddata('productchargecopygrid');
		$(".productchargecopygrid_headchkboxclass").attr('checked', false);
        $('#chargebasedproductid').val('');
		if(productid != ''){
		  chargeproductgriddatafetch(productid,chargeid,purityid);
		}else{
			
		}
	});
	$("#productchargeclose").click(function() {
 	 	cleargriddata('productchargecopygrid');
 	 	$("#chargedproduct").select2('val','');
 		$("#productaddoncopyalertsdouble").fadeOut();
 	});
	$('#productchargecopygridsubmit').click(function(){
		var mainproductid = $('#chargedproduct').find('option:selected').val();
		var clonecategoryid = $('#clonecategoryid').find('option:selected').val();
		var copytopcat = $('#copytopcat').find('option:selected').val();
		var subproductid = $('#chargebasedproductid').val();
		if(checkVariable('chargebasedproductid') == true || copytopcat == 1) {
		  productchargecopy(mainproductid,subproductid,clonecategoryid,copytopcat);
		}else{
			alertpopupdouble('Select the Product');
		}
	});
	$('#alertsdoubleeditproductaddon').click(function(){
	   $('#productaddonalertsdouble').fadeOut();
	   $('#productchargeupdate').show();
	   $('#productchargeadd').hide();
	   productchargeretrive($('#editchargeid').val());
	});
	$('#salestransactiontypeid').change(function(){
		var val = $(this).val();
		if(val == 9 && purchasewastagestatus == 'Yes'){ // purchase
			$('#chargewastageweightdiv').removeClass('hidedisplay');
			$('#chargewastageweight').attr('data-validation-engine','validate[required,decval['+weight_round+'],custom[number],min[0.001]]');
		}else{
			$('#chargewastageweightdiv').addClass('hidedisplay');
			$('#chargewastageweight').attr('data-validation-engine','');
		}
	});
	$("#addicon").click(function(){
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			$("#chargeproductid,#addon_purityid,#accounttypeid,#chargeaccountgroup,#salestransactiontypeid").prop('disabled',false);
			$("#chargeendweight").prop('readonly',false);
			$("#s2id_chargeproductid").select2('focus');
			$('#productchargeupdate').hide();
			$('#productchargeadd').show();
			productaddonedit = 0;
			resetFields();
			Materialize.updateTextFields();
	});
	// charge icon changeable
    $('.addonchargeiconchange').click(function(){
		var spancharge = $(this).attr('keywordcalc');
		var chargearray = {};
		chargearray = $(this).attr('keywordarray');
		chargearray=chargearray.split(',');
		var spanindex = '';
		var spanmanipulate = '';
		var spanchargevalue = '';
		var spanchargeindex = '';
		$.each(chargearray, function(key, value) {
			var spankey = value.split(":");
			if(spancharge == spankey[0]){
			 spanindex =key;
			}
		}); 
		if(spanindex < chargearray.length-1){
			spanmanipulate = spanindex+1;
			spanchargevalue = chargearray[spanmanipulate].split(":");
			spanchargeindex = spanchargevalue[2].split(".");
			$("#productaddonform .chargedetailsdiv input[keyword="+spancharge).attr('keywordcalc',''+spanchargevalue[2]+'');
			$("#productaddonform .chargedetailsdiv input[keyword="+spancharge).attr('chargeid',''+spanchargevalue[3]+'');
			$("#productaddonform .chargedetailsdiv input[keyword="+spancharge).attr('keyword',''+spanchargevalue[0]+'');
			$("#productaddonform .chargedetailsdiv label i[keywordcalc="+spancharge).attr('keywordcalc',''+spanchargevalue[0]+'');
			$("#productaddonform .chargedetailsdiv span[spankeyword="+spancharge).text(''+spanchargevalue[1]+'');
			$("#productaddonform .chargedetailsdiv span[spankeyword="+spancharge).attr('spankeyword',''+spanchargevalue[0]+'');
		}else{
			spanchargevalue = chargearray[0].split(":");
			spanchargeindex = spanchargevalue[2].split(".");
			$("#productaddonform .chargedetailsdiv input[keyword="+spancharge).attr('keywordcalc',''+spanchargevalue[2]+'');
			$("#productaddonform .chargedetailsdiv input[keyword="+spancharge).attr('chargeid',''+spanchargevalue[3]+'');					
			$("#productaddonform .chargedetailsdiv input[keyword="+spancharge).attr('keyword',''+spanchargevalue[0]+'');
			$("#productaddonform .chargedetailsdiv label i[keywordcalc="+spancharge).attr('keywordcalc',''+spanchargevalue[0]+'');
			$("#productaddonform .chargedetailsdiv span[spankeyword="+spancharge).text(''+spanchargevalue[1]+'');
			$("#productaddonform .chargedetailsdiv span[spankeyword="+spancharge).attr('spankeyword',''+spanchargevalue[0]+'');
		}
	});
    //filter
	$("#productaddonfilterddcondvaluedivhid").hide();
	$("#productaddonfilterdatecondvaluedivhid").hide();
	{	//field value set
		$("#productaddonfiltercondvalue").focusout(function(){
			var value = $("#productaddonfiltercondvalue").val();
			$("#productaddonfinalfiltercondvalue").val(value);
			$("#productaddonfilterfinalviewconid").val(value);
		});
		$("#productaddonfilterddcondvalue").change(function(){
			var value = $("#productaddonfilterddcondvalue").val();
			productaddonmainfiltervalue=[];
			$('#productaddonfilterddcondvalue option:selected').each(function(){
			    var $mvalue =$(this).attr('data-ddid');
			    productaddonmainfiltervalue.push($mvalue);
				$("#productaddonfinalfiltercondvalue").val(productaddonmainfiltervalue);
			});
			$("#productaddonfilterfinalviewconid").val(value);
		});
	}
    productaddonfiltername = [];
	$("#productaddonfilteraddcondsubbtn").click(function() {
		$("#productaddonfilterformconditionvalidation").validationEngine("validate");	
	});
	$("#productaddonfilterformconditionvalidation").validationEngine({
		onSuccess: function() {
		dashboardmodulefilter(producttypegrid,'productaddon');
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
});
	//product checkbox
	function productcheckboxaction(name) {
		switch(name){				
			case 'taxable':			
				if($('#'+name+'cboxid').is(':checked')){
					$("#taxmasterid").prop("disabled",false);
				} else {					
					$('#taxmasterid').select2('val','').trigger('change');
					$("#taxmasterid").prop("disabled",true);
				}
			break;
		}
	}
	/*Vardaan Integration Start
	*/
	function productchargeadd()
	{
		var chargedetails = new Array();
		var chargeids = new Array();
		var chargevalues = new Array();
		var cond = '';
		$('.chargedata:visible').each(function(){
			 if($.trim($(this).val()) == '') {
		     }else{
			     var keyword = $(this).attr('keywordcalc');
				 var res=keyword.split(".");
				 if(chargecalculation == 2){
					 cond = 'MIN';
				 }else{
					 cond = 'MAX';
				 }
				 if(res[1] == cond){
				   chargevalues.push($(this).val());
				 }
				 chargedetails.push($(this).attr('keywordcalc')+':'+$(this).val());
				 chargeids.push($(this).attr('chargeid'));
			 }
		 }); 
		var res =chargeids.toString()
		var arr = $.unique(res.split(','));
		var chargeiddata = arr.join(",");
		var chargeaccountgroup = $('#chargeaccountgroup').find('option:selected').val(); 
	    var chargepurityid = $('#addon_purityid').find('option:selected').val();
	    var chargeproductid = $('#chargeproductid').find('option:selected').val();
	    var accounttypeid = $('#accounttypeid').find('option:selected').val();
		var formdata = $("#productaddonform").serialize();	
		var amp = '&';	
		var datainformation = amp + formdata;
		$('#processoverlay').show();
		$.ajax(
	    {
	        url: base_url +"Productaddon/createproductaddon",
	        data: "datas=" + datainformation+"&multiaccountgroup="+chargeaccountgroup+"&multipurityid="+chargepurityid+amp+"chargedata="+chargedetails+amp+"chargeiddata="+chargeiddata+amp+"chargevalues="+chargevalues+amp+"chargeproductid="+chargeproductid+amp+"accounttypeid="+accounttypeid,
			type: "POST",
			async:false,
	        success: function(msg) 
	        {
				var nmsg =  $.trim(msg);
                if (nmsg == 'SUCCESS'){
					var wgt = 0.1;
					var weight_tmp = parseFloat($('#weight_round').val());
					if(weight_tmp == 0){
						wgt = 1;
					} else if(weight_tmp == 1) {
						wgt = 0.1;
					} else if(weight_tmp == 2) {
						wgt = 0.01;
					} else if(weight_tmp == 3) {
						wgt = 0.001;
					}
					else if(weight_tmp == 4) {
						wgt = 0.001;
					}
					var endvalue = (parseFloat($('#chargeendweight').val()) + parseFloat(wgt)).toFixed(weight_tmp);
					$('#processoverlay').hide();
					$('#chargestartweight').val(endvalue);
					$('#chargeendweight').val('').focus();
					$('.chargedata,#chargewastageweight').val(0);
				} else {
					$('#processoverlay').hide();
					alertpopup(submiterror);
				}
				producttyperefreshgrid();		
	        },
	    });
	}
	function productchargedelete()
	{	
	    var primaryid = $('#producttypegrid div.gridcontent div.active').attr('id');
		$('#processoverlay').show();
		$.ajax({
	        url: base_url + "Productaddon/productchargedelete?primaryid="+primaryid,
			async:false,
	        success: function(msg) 
	        {
				var nmsg =  $.trim(msg);
				producttyperefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
			    if (nmsg == 'SUCCESS') {	
					$('#processoverlay').hide();
					alertpopup(deletealert);
	            } else {
					$('#processoverlay').hide();
					alertpopup(submiterror);
	            }
	        },
	    });
	}
	function productchargeretrive(datarowid)
	{
		$('#processoverlay').show();
		var elementsname = ['12','chargestartweight','chargeendweight','chargenameid','chargetypeid','primaryproductchargeid','chargeproductid','chargeproductname','editproductid','accounttypeid','salestransactiontypeid','chargeaccountgroup','chargewastageweight'];
		//form field first focus
		firstfieldfocus();
		$.ajax(
		{
			url:base_url+"Productaddon/productchargeretrieve?primaryid="+datarowid, 
			dataType:'json',
			async:false,
			success: function(data)
			{ 
				$('#processoverlay').hide();
				var txtboxname = elementsname;
				var dropdowns = ['5','chargeproductid','addon_purityid','chargetypeid','salestransactiontypeid','chargeaccountgroup'];
				textboxsetvalue(txtboxname,txtboxname,data,dropdowns);
				productaddonedit=1;
				$('#chargeproductid').trigger('change');
					// set dropdown for account type
						var accounttypeid=data.accounttypeid;
						var acctype=accounttypeid.split(","); 
						if(acctype.length == 1){
							$('#accounttypeid').select2('val',data.accounttypeid).trigger('change');
						}else {
							$('#accounttypeid').select2('val','1');
						}
						var chargedetails=data.chargedetails.split(",");
				if(chargedetails == ''){
				}else{
					$('.chargedata').val(0);
					$.each(chargedetails, function (index, value) {
						 var res=value.split(":");
						 var chargekey = res[0].split(".");
						 var condkey = '';
                         if(chargecalculation == 2){
							 condkey = 'MIN';
						 }else{
							 condkey = 'MAX';
						 }					 
						 if(chargekey[0] == 'WS-PT-N' || chargekey[0] == 'WS-WT' || chargekey[0] == 'WS-PT-G'){
							 if(chargekey[0] == 'WS-PT-N'){
								 $("#wastagespan").text('Wastage Percent Net');
							 }else if(chargekey[0] == 'WS-WT'){
								 $("#wastagespan").text('Wastage Flat Weight');
							 }else if(chargekey[0] == 'WS-PT-G'){
								 $("#wastagespan").text('Wastage % Gross');
							 }
							 $("#wastage").attr('keywordcalc',''+res[0]+'');
							 $("#wastage").attr('keyword',''+chargekey[0]+'-'+condkey);
							 $("#wastagespan").attr('spankeyword',''+chargekey[0]+'-'+condkey);
							 $("#wastageiconchange").attr('keywordcalc',''+chargekey[0]+'-'+condkey);
						 }else if(chargekey[0] == 'MC-GM-N' || chargekey[0] == 'MC-FT' || chargekey[0] == 'MC-PI' || chargekey[0] == 'MC-GM-G'){
							 if(chargekey[0] == 'MC-GM-N'){
								 $("#makingspan").text('Making Per Gram Net');
							 }else if(chargekey[0] == 'MC-FT'){
								 $("#makingspan").text('Making Flat');
							 }else if(chargekey[0] == 'MC-PI'){
								 $("#makingspan").text('Making Per Piece');
							 }else if(chargekey[0] == 'MC-GM-G'){
								 $("#makingspan").text('Making PerGram Gross');
							 }
							 $("#making").attr('keywordcalc',''+res[0]+'');
							 $("#making").attr('keyword',''+chargekey[0]+'-'+condkey);
							 $("#makingspan").attr('spankeyword',''+chargekey[0]+'-'+condkey);
							 $("#makingiconchange").attr('keywordcalc',''+chargekey[0]+'-'+condkey);
						 }else if(chargekey[0] == 'HM-FT' || chargekey[0] == 'HM-PI'){
							 if(chargekey[0] == 'HM-FT'){
								 $("#hallmarkspan").text('Hallmark Flat');
							 }else if(chargekey[0] == 'HM-PI'){
								 $("#hallmarkspan").text('Hallmark Per Piece');
							 }
							 $("#hallmark").attr('keywordcalc',''+res[0]+'');
							 $("#hallmark").attr('keyword',''+chargekey[0]+'-'+condkey);
							 $("#hallmarkspan").attr('spankeyword',''+chargekey[0]+'-'+condkey);
							 $("#hallmarkiconchange").attr('keywordcalc',''+chargekey[0]+'-'+condkey);
						 }else if(chargekey[0] == 'FC-WT' || chargekey[0] == 'FC'){
							 if(chargekey[0] == 'FC-WT'){
								 $("#flatspan").text('Flat Weight');
							 }else if(chargekey[0] == 'FC'){
								 $("#flatspan").text('Flat Amount');
							 }
							 $("#flat").attr('keywordcalc',''+res[0]+'');
							 $("#flat").attr('keyword',''+chargekey[0]+'-'+condkey);
							 $("#flatspan").attr('spankeyword',''+chargekey[0]+'-'+condkey);
							 $("#flaticonchange").attr('keywordcalc',''+chargekey[0]+'-'+condkey);
						 }
						  $('#productaddonform input[keywordcalc="'+res[0]).val(res[1]); 
					});
				}
				var purityid = data.addon_purityid;
				var dataarray = purityid.split(",");
				dataarray = jQuery.grep(dataarray, function(n){ return (n); });
				$('#addon_purityid').select2('val',dataarray).trigger('change');
				$('#chargeaccountgroup').select2('val',data.chargeaccountgroup);
				//disable the purity-accoungroup on edit
				$('#chargepurityid').prop('disabled',true);
				$('#salestransactiontypeid').trigger('change');
				 Materialize.updateTextFields();				
			}
		});
	}
	function productchargeupdate()
	{
		var chargedetails = new Array();
		var chargeids = new Array();
		var chargevalues = new Array();
		var cond = '';
		$('.chargedata:visible').each(function(){
			 if($.trim($(this).val()) == '') {
		     }else{
				 var keyword = $(this).attr('keywordcalc');
				 var res=keyword.split(".");
				if(chargecalculation == 2){
					 cond = 'MIN';
				 }else{
					 cond = 'MAX';
				 }
				 if(res[1] == cond){
				   chargevalues.push($(this).val());
				 }
				 chargedetails.push($(this).attr('keywordcalc')+':'+$(this).val());
				 chargeids.push($(this).attr('chargeid'));
			 }
		 });
		var res =chargeids.toString()
		var arr = $.unique(res.split(','));
		var chargeiddata = arr.join(","); 
		var addon_type = $('#viewproductaddon').val();
		var formdata = $("#productaddonform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		var datarowid = $('#primaryproductchargeid').val();
	    var chargeaccountgroup = $('#chargeaccountgroup').find('option:selected').val(); 
	    var chargepurityid = $('#addon_purityid').find('option:selected').val();
	    var chargeproductid = $('#chargeproductid').find('option:selected').val();
	    var accounttypeid = $('#accounttypeid').find('option:selected').val();
	    var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
		setTimeout(function(){
			$('#processoverlay').show();
		},25);
		$.ajax({
	        url: base_url + "Productaddon/productchargeupdate",
	        data: "datas=" + datainformation +"&primaryid="+ datarowid+"&addontype="+addon_type+"&multiaccountgroup="+chargeaccountgroup+"&multipurityid="+chargepurityid+"&accounttypeid="+accounttypeid+amp+"chargedata="+chargedetails+amp+"chargeiddata="+chargeiddata+amp+"chargevalues="+chargevalues+amp+"chargeproductid="+chargeproductid+amp+"salestransactiontypeid="+salestransactiontypeid,
			type: "POST",
			async:false,
	        success: function(msg) 
	        {
				var nmsg =  $.trim(msg);
				$("#productaddonsectionoverlay").addClass("closed");
		        $("#productaddonsectionoverlay").removeClass("effectbox");
				clearform('productaddonform');
	            producttyperefreshgrid();
				$('#productchargeupdate').hide();
				$('#productchargeadd,#productaddondeleteicon').show();	
				$('#chargepurityid').prop('disabled',false);
				$('.productbutton').show();
				$('.producttext').hide();
				 if (nmsg == 'SUCCESS'){
					 $(".addsectionclose").trigger("click");
					setTimeout(function(){
						$('#processoverlay').hide();
						 
					},25);
					alertpopup(updatealert);
	            } else {
					setTimeout(function(){
						$('#processoverlay').hide();
					},25);
					alertpopup(submiterror);
				}             
	        },
	    });
	}
	{// Product Type View Grid
		function producttypegrid(page,rowcount) {
		var defrecview = $('#mainviewdefaultview').val();
		var accointid='';
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
		var wwidth = $( window ).width();
		var wheight = $( window ).height();
		/*col sort*/
		var sortcol = $("#producttypesortcolumn").val();
		var sortord = $("#producttypesortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.producttypeheadercolsort').hasClass('datasort') ? $('.producttypeheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord){
			sortord = sortord;
		} else{
			var sortord =  $('.producttypeheadercolsort').hasClass('datasort') ? $('.producttypeheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.producttypeheadercolsort').hasClass('datasort') ? $('.producttypeheadercolsort.datasort').attr('id') : '0';	
		var filterid = $("#productaddonfilterid").val();
		var conditionname = $("#productaddonconditionname").val();
		var filtervalue = $("#productaddonfiltervalue").val();
	$.ajax({
		url:base_url+"Productaddon/gridinformationfetch?maintabinfo=productcharge&primaryid=productchargeid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=57'+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#producttypegrid').empty();
			$('#producttypegrid').append(data.content);
			$('#producttypegridfooter').empty();
			$('#producttypegridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('producttypegrid');
			{//sorting
				$('.producttypeheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.producttypeheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.producttypeheadercolsort').hasClass('datasort') ? $('.producttypeheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.producttypeheadercolsort').hasClass('datasort') ? $('.producttypeheadercolsort.datasort').data('sortorder') : '';
					$("#chitamountsortorder").val(sortord);
					$("#chitamountsortcolumn").val(sortcol);
					producttypegrid(page,rowcount);
					$("#processoverlay").hide();
				});
				sortordertypereset('producttypeheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function() {
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					producttypegrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#producttypepgrowcount').change(function() {
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					producttypegrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#producttypegrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#producttypepgrowcount').material_select();
		},
	});
}
{//refresh grid
	function producttyperefreshgrid() {
		//var page = $('ul#productpgnum li.active').data('pagenum');
		//var rowcount = $('ul#productpgnumcntpgnum li .page-text .active').data('rowcount');
		var page = $('#producttypegridfooter .paging').data('pagenum'); 
		var rowcount = $('#producttypegridfooter .pagerowcount').data('rowcount'); 
		producttypegrid(page,rowcount);
	}
}
function getmaxcharge(productid,purityid,accounttypeid,accountgroupid){ 
	$.ajax({
	        url: base_url + "Productaddon/getmaxcharge",
	        data: "productid=" + productid +"&purityid="+ purityid+"&accountgroupid="+accountgroupid+"&accounttypeid="+accounttypeid,
			type: "POST",
			dataType:'json',
			async:false,
	        success: function(msg) 
	        {
				$('#chargestartweight').val(msg['endweight']);
			}
	 });
}
function loadproduct(){
	 $('#chargeproductid').empty();	
     $('#chargeproductid').append($("<option></option>"));
	$.ajax({
	        url: base_url + "Productaddon/loadproduct",
	        async:false,
			dataType:'json',
	        success: function(data) 
	        {
				var productidshow = $('#productidshow').val();
					if(productidshow == 'YES') { 
					    $.each(data, function(index) {
						 $('#chargeproductid')
							.append($("<option></option>")
							.attr("data-chargeid",data[index]['chargeid'])
							.attr("data-purchasechargeid",data[index]['purchasechargeid'])
							.attr("data-productname",data[index]['productname'])
							.attr("data-counterid",data[index]['counterid'])
							.attr("data-purityid",data[index]['purityid'])
							.attr("value",data[index]['productid'])
							.text(data[index]['productid']+'-'+data[index]['productname'])); 
							});
					}else{
					  $.each(data, function(index) {
						 $('#chargeproductid')
							.append($("<option></option>")
							.attr("data-chargeid",data[index]['chargeid'])
							.attr("data-purchasechargeid",data[index]['purchasechargeid'])
							.attr("data-productname",data[index]['productname'])
							.attr("data-counterid",data[index]['counterid'])
							.attr("data-purityid",data[index]['purityid'])
							.attr("value",data[index]['productid'])
							 .text(data[index]['productname'])); 
						  });
					}
				
				
			}
	 });
}
function checkgreaterval()
{
	if(parseFloat($("#chargestartweight").val())<= parseFloat($("#chargeendweight").val()))
	{return true;}else{return "*End wt > start wt";}
	
}
function accountgroupshowhide(){
	var accounttypeid = $('#accounttypeid').val();
	$("#chargeaccountgroup option").addClass("ddhidedisplay");
	$("#chargeaccountgroup optgroup").addClass("ddhidedisplay");
	$("#chargeaccountgroup option").prop('disabled',true);
	$("#chargeaccountgroup option[data-accountype="+1+"]").removeClass("ddhidedisplay");
	$("#chargeaccountgroup option[data-accountype="+1+"]").closest('optgroup').removeClass("ddhidedisplay");
	$("#chargeaccountgroup option[data-accountype="+1+"]").prop('disabled',false);
	$("#chargeaccountgroup option[data-accountype="+accounttypeid+"]").removeClass("ddhidedisplay");
	$("#chargeaccountgroup option[data-accountype="+accounttypeid+"]").closest('optgroup').removeClass("ddhidedisplay");
	$("#chargeaccountgroup option[data-accountype="+accounttypeid+"]").prop('disabled',false);
}
function purityshowhide(purityid){
	$("#addon_purityid option").addClass("ddhidedisplay");
	$("#addon_purityid optgroup").addClass("ddhidedisplay");
	$("#addon_purityid option").prop('disabled',true);
	if (purityid.indexOf(',') > -1) {
		var purity = purityid.split(',');
		for(j=0;j<(purity.length);j++) {
			$("#addon_purityid option[value="+purity[j]+"]").removeClass("ddhidedisplay");
			$("#addon_purityid option[value="+purity[j]+"]").closest('optgroup').removeClass("ddhidedisplay");
			$("#addon_purityid option[value="+purity[j]+"]").prop('disabled',false);
		}
	}else{
			var purity = purityid;
			$("#addon_purityid option[value="+purity+"]").removeClass("ddhidedisplay");
			$("#addon_purityid option[value="+purity+"]").closest('optgroup').removeClass("ddhidedisplay");
			$("#addon_purityid option[value="+purity+"]").prop('disabled',false);
		
	}
}
function countershowhide(){
	var counterdataid=$('#counterdataid').val();
	   if(counterdataid == 'NO'){
			$('#counteriddivhid').addClass('hidedisplay');
			$('#counterid').removeClass('validate[required]');
	   }else{
			$('#counteriddivhid').removeClass('hidedisplay');
			$('#counterid').addClass('validate[required]');
	   }
}
function loadchargedproduct(){
	$('#chargedproduct').empty();	
    $('#chargedproduct').append($("<option></option>"));
	$.ajax({
	        url: base_url + "Productaddon/loadchargedproduct",
	        async:false,
			dataType:'json',
	        success: function(data) 
	        {
				$.each(data, function(index) {
					 $('#chargedproduct')
					.append($("<option></option>")
					.attr("data-chargeid",data[index]['chargeid'])
					.attr("data-productname",data[index]['productname'])
					.attr("data-purityid",data[index]['purityid'])
					.attr("value",data[index]['productid'])
					.text(data[index]['productid']+'-'+data[index]['productname'])); 
				});
				
			}
	 });

}
//bill number grid load
function productchargecopygrid() {
	var wwidth = $("#productchargecopygrid").width();
	var wheight = $("#productchargecopygrid").height();
	$.ajax({
		url:base_url+"Productaddon/productchargecopygridgirdheaderinformationfetch?moduleid=90&width="+wwidth+"&height="+wheight+"&modulename=productchargecopygrid",
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#productchargecopygrid").empty();
			$("#productchargecopygrid").append(data.content);
			$("#productchargecopygridfooter").empty();
			$("#productchargecopygridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('productchargecopygrid');
			//header check box
			$(".productchargecopygrid_headchkboxclass").click(function() {
				$(".productchargecopygrid_rowchkboxclass").prop('checked', false);
				if($(".productchargecopygrid_headchkboxclass").prop('checked') == true) {
					$(".productchargecopygrid_rowchkboxclass").prop('checked', true);
				} else {
					$(".productchargecopygrid_rowchkboxclass").prop('checked', false);
				}
				getcheckboxrowid('productchargecopygrid');
			});
		},
	});	
}
function getcheckboxrowid(gridid) {
	var selected = [];
	$('#'+gridid+' .gridcontent input:checked').each(function() {
		selected.push($(this).attr('data-rowid'));
	});
	$("#chargebasedproductid").val(selected);
}
function chargeproductgriddatafetch(productid,chargeid,purityid){
	$.ajax({
		url:base_url+"Productaddon/chargeproductgriddatafetch",
		dataType:'json',
		data:{chargeid:chargeid,purityid:purityid,productid:productid},
		type: "POST",
		async:false,
		cache:false,
		success:function(data) {
			if(data != ''){
				loadinlinegriddata('productchargecopygrid',data.rows,'json','checkbox');
				//row based check box
				$(".productchargecopygrid_rowchkboxclass").click(function(){
					$('.productchargecopygrid_headchkboxclass').removeAttr('checked');
					getcheckboxrowid('productchargecopygrid');
				});
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('productchargecopygrid');
			}else{
				alertpopupdouble('Product Not Available');
			}
		},
	});	
}
function productchargecopy(mainproductid,subproductid,clonecategoryid,copytopcat){
	$('#processoverlay').show();
	$.ajax({
		url:base_url+"Productaddon/productchargecopy",
		data:{mainproductid:mainproductid,subproductid:subproductid,clonecategoryid:clonecategoryid,copytopcat:copytopcat},
		type: "POST",
		async:false,
		cache:false,
		success:function(msg) {
			$('#processoverlay').hide();
			if(msg == 'SUCCESS'){
				alertpopup('Charge Copied Successfully');
				producttyperefreshgrid();
				$('#productchargeclose').trigger('click');
			}else{
				alertpopup(submiterror);
			}
		},
	});	
}
function saleschargehideshow(businesschargedata,ddname){
	$("#"+ddname+" option").addClass("ddhidedisplay");
	$("#"+ddname+" option").prop('disabled',true);
	$("#"+ddname+" optgroup").addClass("ddhidedisplay");
	if(businesschargedata !='' && businesschargedata != 'null'){ 
		if(businesschargedata.indexOf(',') > -1) {
			var charge = businesschargedata.split(',');
				for(j=0;j<(charge.length);j++) {
					$("#"+ddname+" option[value="+charge[j]+"]").removeClass("ddhidedisplay");
					$("#"+ddname+" option[value="+charge[j]+"]").prop('disabled',false);
					$("#"+ddname+" option[value="+charge[j]+"]").closest('optgroup').removeClass("ddhidedisplay");
				}
		}else{
			$("#"+ddname+" option[value="+businesschargedata+"]").removeClass("ddhidedisplay");
			$("#"+ddname+" option[value="+businesschargedata+"]").prop('disabled',false);
			$("#"+ddname+" option[value="+businesschargedata+"]").closest('optgroup').removeClass("ddhidedisplay");
		}
	}
}
function stonehideshow(){
	if(stonestatus == 'YES'){
		$('#stonedivhid').removeClass('hidedisplay');
		$('#descriptiondivhid').removeClass('large-12 medium-12 small-12').addClass('large-6 medium-6 small-6');
	}else{
		$('#stonedivhid').addClass('hidedisplay');
		$('#descriptiondivhid').addClass('large-12 medium-12 small-12').removeClass('large-6 medium-6 small-6');
	}
}
function checkproductaddon(datarowid){  // check product addon
	var status ='false';
	$.ajax({
        url: base_url + "Productaddon/checkproductaddon",
        data: "datarowid=" + datarowid,
		type: "POST",
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) 
        {
			if(data > 0)
			{ 
				status ='true';
			}
        }
    });
	return status;
}
}