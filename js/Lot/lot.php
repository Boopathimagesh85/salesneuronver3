<script>
$(document).ready(function() 
{
	{//Foundation Initialization
		$(document).foundation();
	}
	{//tag file global variables
		addchargeoperation = 'ADD'; //identify whether the add.charge is edit/add mode.
		MODE = 'ADD'; //for validation purpose
	}
	{// Grid Calling Function
		lotgrid();
	}
	{//keyboard shortcut reset global variable
		 viewgridview = 0;
		 innergridview = 1;
	}
	{ // Overlay form modules - filter display
		$('#viewoverlaytoggle').on('click',function() {
			$('#filterdisplaymainviewnonbase').css({"display":"block"});
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#lotfilterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#lotfilterconddisplay').children().remove();
			$('#lotfilterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#lotfilterid,#lotconditionname,#lotfiltervalue").val('');
			lotgrid();
		});
		$(document).on( "click", ".nbfilterdisplaymainviewclose", function() {
			$('#filterdisplaymainviewnonbase').css({"display":"none"});
		});
	}
	lotdata = $('#lotdata').val();
	lottype = $('#lottype').val();
	stoneweightcheck = $('#stoneweightcheck').val();
	csadminpassword = $('#adminpassword').val();
	weight_round = $('#weight_round').val(); //weight round
	dia_weight_round = $('#dia_weight_round').val(); //Diamond weight round
	validate_caratwt = $('#validate_caratwt').val(); // Validate from form page
	validate_grosswt = $('#validate_grosswt').val(); // Validate from form page
	$('#groupcloseaddform').hide();
	$('#lotviewtoggle').hide();
	$(window).resize(function() {
		maingridresizeheightset('lotgrid');
		sectionpanelheight('groupsectionoverlay');
	});
	$('.lotdatehide').hide(); //Sapna - arvind changes				
	purchasewastage ='No';
	wastagecalctype = 1;
	{
		$("#addicon").click(function(){
			resetFields();
			//setTimeout(function(){
				getcurrentsytemdate('lotdate');
			//},100);
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			$('#caratweight,#grossweight,#pieces').prop('readonly',false);
			$('#product,#purityid,#accountid,#lotcreationopt').prop('disabled',false);
			//setTimeout(function(){
				setlottype();
			//},100);
			setrequirelotdata();
			var ddname = 'product';
			$("#"+ddname+" option").removeClass("ddhidedisplay");
			$("#"+ddname+" option").prop('disabled',false);
			var ddpurityname = "purityid";
			$("#"+ddpurityname+" option").removeClass("ddhidedisplay");
			$("#"+ddpurityname+" optgroup").removeClass("ddhidedisplay");
			$("#"+ddpurityname+" option").prop('disabled',false);
			$('#product,#purityid,#prodcategorylistbutton').prop('disabled',false);
			$("#purityid option").removeClass("ddhidedisplay");
			$("#purityid option").prop('disabled',false);
			$("#purityid option").select2('val','');
			$('#lotupdate').hide();
			$('#lotcreate').show();
			$('#stoneweight_div,#stoneapplicablediv').removeClass('hidedisplay');
			stonewtvalidate();
			$("#purityid").select2('focus');
			$('#proparentcategoryid,#salesdetailid').val('');
			$("#stoneapplicable").select2('val','No');
			Materialize.updateTextFields();
		});
	}
	{// Tool Bar click function
		//Delete
		$("#deleteicon").click(function(){
			var datarowid = $('#lotgrid div.gridcontent div.active').attr('id');
			if(datarowid){
				var purchasestatus = getgridcolvalue('lotgrid',datarowid,'purchase','');
				var receiveorderstatus = getgridcolvalue('lotgrid',datarowid,'receiveorder','');
				if(purchasestatus == 'Yes') {
					alertpopup('Unable to delete purchase based Lot');
				} else if(receiveorderstatus == 'Yes') {
					alertpopup('Unable to delete Receive Order based Lot');
				} else{	
					var checklot = checklotstatus(datarowid);
					if($('#lotupdate').is(":visible")){
						alertpopup('Lot under edit.cannot delete,please refresh and delete');
					} else if(checklot == 'yes'){
						$("#basedeleteoverlay").fadeIn();
						$(".ffield").focus();
					} else {
						alertpopup('This lot is used in tagging');
					}
				}
				//
			} else {
				alertpopup(selectrowalert);
			}
		});
		$("#basedeleteyes").click(function(){
			lotdelete();
		});
		//Edit
		$("#editicon").click(function(){			
			var datarowid = $('#lotgrid div.gridcontent div.active').attr('id');
			if(datarowid){
				var status = getgridcolvalue('lotgrid',datarowid,'status','');
				var purchasestatus = getgridcolvalue('lotgrid',datarowid,'purchase','');
				var receiveorderstatus = getgridcolvalue('lotgrid',datarowid,'receiveorder','');
				if(status == 'Close') {
					alertpopup('Please choose active status');
				} else if(purchasestatus == 'Yes') {
					alertpopup('Unable to edit purchase based Lot');
				} else if(receiveorderstatus == 'Yes') {
					alertpopup('Unable to edit Receive Order based Lot');
				} else {
					$('#lotupdate').show();
					$('#lotcreate').hide();
					lotretrive();
					MODE = 'EDIT';
					sectionpanelheight('groupsectionoverlay');
					$("#groupsectionoverlay").removeClass("closed");
					$("#groupsectionoverlay").addClass("effectbox");
					$('#salesdetailid').val('');
					setrequirelotdata();
					$('#lotcreationopt').prop('disabled',true);
				}
			} else {
				alertpopup(selectrowalert);
			}
		});
		// manual open close lot
		$("#statuschangeicon").click(function(){
			var datarowid = $('#lotgrid').jqGrid('getGridParam', 'selrow'); 
			if(datarowid){
				var lotid = $('#lotgrid').jqGrid('getCell',datarowid,'lotid');
				var status = $('#lotgrid').jqGrid('getCell',datarowid,'statusname');
				var currentgrossweight = $('#lotgrid').jqGrid('getCell',datarowid,'currentgrossweight');
				var pendinggrosswt=Math.round(currentgrossweight);
				
				var checklot = checklotstatus(datarowid);
				if(checklot == 'no'){
				if(status == 'close') {
					var statusid=4;
				}else if(status == 'active') {
					var statusid=1;
				} 				
				else { 
				}
				if(pendinggrosswt > 0)
				{
					alertpopup("Lot have pending weight,So you can't change the status ");
				}
				else{
				combainedmoduledeletealert('lotmanual','Are you sure,you want to change status ?');				
				$("#lotmanual").click(function(){				
					lotmanualopenclose(lotid,statusid);
					$(this).unbind();
				});
				}
				}
				else{
					alertpopup('Tagging not created for this lot.');
				}
			}else {
				alertpopup(selectrowalert);
			}
		});
	}
	{
		//auto calculate ->grosswt/netweight/stoneweight
		$(".weightcalculate").change(function(event){		
			setzero(['caratweight','grossweight','stoneweight','netweight']);
			var round = $('#roundweight').val();
			var fieldid = $(this).attr("id");
			var type = $('#'+fieldid+'').data('calc');
			//var regxg = /^\d+(?:\.\d\d\d?)?$/;
			 var regxg =/^(\d*\.?\d*)$/;
			if(type == 'GWT'){
				var grosswt = $('#'+fieldid+'').val();
				  if (!regxg.test(grosswt)) {
				  }else{
					  if (grosswt.toString().indexOf('.') != -1) {
							var substr = grosswt.split('.');
							var roundlength=substr[1].length; 
						}else{
							var roundlength=0;
						}
						if(round < roundlength)
						{
							return false;
						}
					    var stonewt = $('#stoneweight').val();
						var netwt = (parseFloat(grosswt)-parseFloat(stonewt)).toFixed(round);
						var thisval=parseFloat($("#grossweight").val()).toFixed(round);
						$("#grossweight").val(thisval);
						$('#stoneweight').val(parseFloat(stonewt).toFixed(round));
						$('#netweight').val(netwt).trigger('change');
						Materialize.updateTextFields();
				  }
			} else if(type == 'SWT') {
				var grosswt = $('#grossweight').val();
				var stonewt = $('#'+fieldid+'').val();
				 if (!regxg.test(stonewt)) {
				  }else{
					  if (stonewt.toString().indexOf('.') != -1) {
							var substr = stonewt.split('.');
							var roundlength=substr[1].length; 
						}else{
							var roundlength=0;
						}
						if(round < roundlength)
						{
							return false;
						}
					var thisval=parseFloat($("#stoneweight").val()).toFixed(round);
					var netwt = (parseFloat(grosswt)-parseFloat(stonewt)).toFixed(round);
					$('#stoneweight').val(thisval);
					$('#netweight').val(netwt).trigger('change');
					Materialize.updateTextFields();
				  }
			} else if(type == 'NWT') {
				var grosswt = $('#grossweight').val();
				var netwt = $('#netweight').val();
				 if (!regxg.test(netwt)) {
				  }else{
					  if (netwt.toString().indexOf('.') != -1) {
							var substr = netwt.split('.');
							var roundlength=substr[1].length; 
						}else{
							var roundlength=0;
						}
						if(round < roundlength)
						{
							return false;
						}
					var thisval=parseFloat($("#netweight").val()).toFixed(round);
					var stonewt = (parseFloat(grosswt)-parseFloat(netwt)).toFixed(round);
					$('#stoneweight').val(stonewt);
					$('#netweight').val(thisval);
					Materialize.updateTextFields();
				}
			}
		});
	}
	{//**Lot form validation**//
		//lot create validate
		$('#lotcreate').click(function() 
		{
			//$('#purityid').prop('disabled',false);
			$("#addlotvalidate").validationEngine('validate');
			$(".mmvalidationnewclass .formError").addClass("errorwidth");
		});
		jQuery("#addlotvalidate").validationEngine(
		{
			onSuccess: function(){
				var stonewt = $('#stoneweight').val();
				var stonestatus = $.trim($('#product').find('option:selected').data('stone'));
				if(stonestatus == 'Yes' && stoneweightcheck == 'Yes') {
					if(parseFloat(stonewt) <= 0){
						alertpopup('Please give stonewt greater than zero');
						return false;
					}
				}
				lotadd();
			},
			onFailure: function(){
				var dropdownid =['1','purityid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//lot update validate
		$('#lotupdate').click(function(){
			$("#editlotvalidate").validationEngine('validate');
			$(".mmvalidationnewclass .formError").addClass("errorwidth");
		});
		jQuery("#editlotvalidate").validationEngine({
			onSuccess: function(){
				var stonewt = $('#stoneweight').val();
				var stonestatus = $.trim($('#product').find('option:selected').data('stone'));
				if(stonestatus == 'Yes') {
					if(parseFloat(stonewt) <= 0){
						alertpopup('Please give stonewt greater than zero');
						return false;
					}
				}	
				lotupdate();
			},
			onFailure: function(){
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	//for date picker
	var dateformetqt  = 0;
	dateformetqt= $('#lotdate').attr('data-dateformater');
	$('#lotdate').datetimepicker({
		dateFormat: dateformetqt,
		showTimepicker :false,
		//minDate: -1,
		onSelect: function () {
			//getandsetdate($('#lotdate').val())
		},
		onClose: function () {
			$('#lotdate').focus();
		}
	});
	getcurrentsytemdate('lotdate');
	setTimeout(function(){
		$('#purityid').select2('focus');
	},100);
	$('#closeloticon').click(function(){
        var datarowid = $('#lotgrid div.gridcontent div.active').attr('id');
			if(datarowid){
				var status = getgridcolvalue('lotgrid',datarowid,'status','');
				if(status == 'Close'){
					alertpopup('Already this lot closed');
				}else{
					lotmanualclose(datarowid);
			    }
			}else {
				alertpopup(selectrowalert);
			}				
	});
	$('#reopenloticon').click(function(){
        var datarowid = $('#lotgrid div.gridcontent div.active').attr('id');
			if(datarowid){
				var status = getgridcolvalue('lotgrid',datarowid,'status','');
				if(status == 'Active'){
					alertpopup('Already this lot active');
				}else{
					lotmanualopen(datarowid);
			   }
			}else {
				alertpopup(selectrowalert);
			}				
	});
	{
		$('#errorweighticon').click(function(){
			$('#errorgrossweight,#errornetweight,#errorpieces,#vadminpassword').val('');
			var datarowid = $('#lotgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				$("#errorweightoverlay").fadeIn();
				$('#errorgrossweight').focus();
				$('#errornetweight').prop('disabled',true);
				errorweightvalidate(datarowid);
			} else {
				alertpopup(selectrowalert);
			}
		});
		//Error Weightform overlay submit
		$('#formerrorweightsave').click(function(){
				$("#errorweightform").validationEngine('validate');
		});
		jQuery("#errorweightform").validationEngine({
			onSuccess: function() {
				var datarowid = $('#lotgrid div.gridcontent div.active').attr('id');
				var errorgrossweight = $("#errorgrossweight").val();
				var errornetweight = $("#errornetweight").val();
				var errorcaratweight = $("#errorcaratweight").val();
				var errorpieces = $("#errorpieces").val();
				var adminpassword = $('#vadminpassword').val();
				var caratwt = getgridcolvalue('lotgrid',datarowid,'caratweight','');
				if(caratwt > 0) {
					var chkval = errorcaratweight;
				} else {
					var chkval = errorgrossweight;
				}
				if(chkval !='' || chkval != 0) {
					if(csadminpassword == adminpassword) {
						$.ajax({
							url: base_url +"Lot/updateerrorweightform",
							data: "primaryid="+datarowid+"&errorgrossweight="+errorgrossweight+"&errornetweight="+errornetweight+"&errorcaratweight="+errorcaratweight+"&errorpieces="+errorpieces,
							type: "POST",
							success: function(msg) {
								$("#errorweightoverlay").fadeOut();
								alertpopup(updatealert);
								lotgrid();
							},
						});
					} else {
						alertpopupdouble('Entered password is wrong.');
					}
				} else {
					alertpopupdouble('Please give values in required field.');
				}
			},
			onFailure: function() {	
				//alertpopup(validationalert);
			}
		});
	}
	$('#formerrorweightcancel').click(function(){
		clearform('errorweightform');
		$("#errorweightoverlay").fadeOut();
	});
	//filter
	$("#lotfilterddcondvaluedivhid").hide();
	$("#lotfilterdatecondvaluedivhid").hide();
	{	//field value set
		$("#lotfiltercondvalue").focusout(function(){
			var value = $("#lotfiltercondvalue").val();
			$("#lotfinalfiltercondvalue").val(value);
			$("#lotfilterfinalviewconid").val(value);
		});
		$("#lotfilterddcondvalue").change(function(){
			var value = $("#lotfilterddcondvalue").val();
			lotmainfiltervalue=[];
			$('#lotfilterddcondvalue option:selected').each(function(){
			    var $mvalue =$(this).attr('data-ddid');
			    lotmainfiltervalue.push($mvalue);
				$("#lotfinalfiltercondvalue").val(lotmainfiltervalue);
			});
			$("#lotfilterfinalviewconid").val(value);
		});
		$("#lotfilterdatecondvalue").change(function(){
			var value = $("#lotfilterdatecondvalue").val();
			$("#lotfinalfiltercondvalue").val(value);
			$("#lotfilterfinalviewconid").val(value);
		});
	}
	lotfiltername = [];
	$("#lotfilteraddcondsubbtn").click(function() {
		$("#lotfilterformconditionvalidation").validationEngine("validate");	
	});
	$("#lotfilterformconditionvalidation").validationEngine({
		onSuccess: function() {
			dashboardmodulefilter(lotgrid,'lot');
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	$('#lotcreationopt').change(function(){
		$('#purchasebillno,#product,#purityid,#accountid,#prodparentcategoryname').select2('val','');//Sapna - Arvind changes
		$('#caratweight,#grossweight,#stoneweight,#netweight,#pieces,#proparentcategoryid').val(0);
		var val = $(this).val();
		if(lottype == 4) { // both option company setting
			lottypehideshow(val);
		}else{
			$("#purchasebillno").removeClass('validate[required]');
		}
	});
	// purchasebillno change
	$('#purchasebillno').change(function(){
		var val = $(this).val();
		$("#purchasedetailoverlay").fadeIn();
		purchasedetailgrid();
		Materialize.updateTextFields();
	});
	$("#purchasedetailclose").click(function() {
		$('#purchasebillno').select2('val','');
		$('#suppliernumber').val('');
		$("#purchasedetailoverlay").fadeOut();
	});
	$("#purchasedetailsubmit").click(function() {
		var salesdetailsid =  $.trim($('#purchasedetailgrid div.gridcontent div.active').attr('id'));
		if(salesdetailsid != '') {
			purchasedetailset(salesdetailsid);
			$("#purchasedetailoverlay").fadeOut();
		} else {
			alertpopup('Please Select The Row');
		}
	});
	// category tree
	{
		$('#dl-promenucategory').dlmenu({
		animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
		});
	}
	// parent category select
	$('#prodcategorylistuldata li').click(function() {
		var name=$(this).attr('data-listname');
		var listid=$.trim($(this).attr('data-listid')); 
		var level = parseInt($(this).attr('data-level'));
		$('#prodparentcategoryname').val(name);
		$('#proparentcategoryid').val(listid);
		loadproductbasedoncategory(listid,'product');
		getpuritybasedoncategory(listid);
		$('#product,#purityid').select2('val','');
		$('#stoneweight_div,#stoneapplicablediv').removeClass('hidedisplay');
		$("#stoneapplicable").select2('val','No');
		stonewtvalidate();
	});
	$('#product').change(function() {
		$('#caratweight,#grossweight,#stoneweight,#netweight,#pieces,#description').val('');
		$('#accountid').select2('val','');
		var purity = $.trim($(this).find('option:selected').data('purityid'));
		var stonestatus = $.trim($(this).find('option:selected').data('stone'));
		loadpuritybasedoncategory(purity,'purityid',1);
		if($(this).find('option:selected').val() != ''){
			stoneweighthideshow(stonestatus);
		}else if($(this).find('option:selected').val() == ''){
			var listid=$('#proparentcategoryid').val();
			getpuritybasedoncategory(listid);
			$('#stoneweight_div,#stoneapplicablediv').removeClass('hidedisplay');
			$("#stoneapplicable").select2('val','No');
			stonewtvalidate();
		}
		$('#caratweight_div,#grossweight_div,#netweight_div').removeClass('hidedisplay');
		if(purity == 11) {
			$('#caratweight_div').removeClass('hidedisplay');
			$('#grossweight_div,#netweight_div').addClass('hidedisplay');
			$('#caratweight').addClass(validate_caratwt);
			$('#grossweight,#netweight').removeClass(validate_grosswt);
			$('#grossweight,#netweight').val(0);
		} else {
			$('#caratweight_div').addClass('hidedisplay');
			$('#grossweight_div,#netweight_div').removeClass('hidedisplay');
			$('#grossweight,#netweight').addClass(validate_grosswt);
			$('#caratweight').removeClass(validate_caratwt);
			$('#caratweight').val(0);
		}
	});
	$('#errorgrossweight').change(function(){
		$('#errornetweight').val($('#errorgrossweight').val());
	});
});
{//lot grid view
	function lotgrid(page,rowcount) {
		var defrecview = $('#mainviewdefaultview').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
		var wwidth = $('#lotgrid').width();
		var wheight =  $(window).height();
		/*col sort*/
		var sortcol = $("#lotsortcolumn").val();
		var sortord = $("#lotsortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.lotheadercolsort').hasClass('datasort') ? $('.lotheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.lotheadercolsort').hasClass('datasort') ? $('.lotheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.lotheadercolsort').hasClass('datasort') ? $('.lotheadercolsort.datasort').attr('id') : '0';
		var filterid = $("#lotfilterid").val();
		var conditionname = $("#lotconditionname").val();
		var filtervalue = $("#lotfiltervalue").val();
		$.ajax({
			url:base_url+"Lot/lotview?maintabinfo=lot&primaryid=lotid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=44'+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#lotgrid').empty();
				$('#lotgrid').append(data.content);
				$('#lotgridfooter').empty();
				$('#lotgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('lotgrid');
				{//sorting
					$('.lotheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.lotheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount =  $('.pagerowcount').data('rowcount');
						var sortcol = $('.lotheadercolsort').hasClass('datasort') ? $('.lotheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.lotheadercolsort').hasClass('datasort') ? $('.lotheadercolsort.datasort').data('sortorder') : '';
						$("#lotsortorder").val(sortord);
						$("#lotsortcolumn").val(sortcol);
						var sortpos = $('#lotgrid .gridcontent').scrollLeft();
						lotgrid(page,rowcount);
						$('#lotgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('lotheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function() {
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						lotgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#lotpgrowcount').change(function() {
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('#prev').data('pagenum');
						lotgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//Material select
				$('#lotpgrowcount').material_select();
			},
		});	
	}
}
{//CRUD Related functions
	//Create
	function lotadd()
	{
		MODE = 'ADD';
		var formdata = $("#lotform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
			setTimeout(function(){
			$('#processoverlay').show();
		},25);
		var billno = $.trim($('#purchasebillno').find('option:selected').data('billnumber'));
		var product = $.trim($('#product').find('option:selected').val());
		var purityid = $.trim($('#purityid').find('option:selected').val());
		var accountid = $.trim($('#accountid').find('option:selected').val());
		var stoneapplicable = $.trim($('#stoneapplicable').find('option:selected').val());
		$.ajax({
			url: base_url +"Lot/lotcreate",
			data: "datas=" + datainformation+amp+"billno="+billno+amp+"product="+product+amp+"purityid="+purityid+amp+"accountid="+accountid+amp+"stoneapplicable="+stoneapplicable,
			type: "POST",
			success: function(msg) {
				var nmsg =  $.trim(msg);
				$('#purityid').select2('focus');			
				if (nmsg == 'SUCCESS') {
					lotgrid();
					$(".addsectionclose").trigger("click");					
					setTimeout(function(){
						$('#processoverlay').hide();
					},25);
					alertpopup(savealert);
				} else if (nmsg == 'FAIL') {
					$('#processoverlay').hide();
					alertpopup('Create Lotnumber format in serial master');
				} else {
					setTimeout(function(){
						$('#processoverlay').hide();
					},25);
					alertpopup(submiterror);
				}
			},
		});
	}
	//lot detail update
	function lotupdate()
	{
		MODE = 'ADD';
		var formdata = $("#lotform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		var datarowid = $('#lotprimaryid').val();
			setTimeout(function(){
			$('#processoverlay').show();
		},25);
		var product = $.trim($('#product').find('option:selected').val());
		var purityid = $.trim($('#purityid').find('option:selected').val());
		var accountid = $.trim($('#accountid').find('option:selected').val());
		var stoneapplicable = $.trim($('#stoneapplicable').find('option:selected').val());
		$.ajax({
			url: base_url + "Lot/lotupdate",
			data: "datas=" + datainformation +"&primaryid="+ datarowid+amp+"product="+product+amp+"purityid="+purityid+amp+"accountid="+accountid+amp+"stoneapplicable="+stoneapplicable,
			type: "POST",
			success: function(msg) {
				var nmsg =  $.trim(msg);
				$('#lotupdate').hide();
				$('#lotcreate').show();
				$('#product,#purityid,#prodcategorylistbutton').prop('disabled',false);
				if (nmsg == 'SUCCESS'){
					lotgrid();
					$(".addsectionclose").trigger("click");	
					setTimeout(function(){
						$('#processoverlay').hide();
						getcurrentsytemdate('lotdate');
						Materialize.updateTextFields();
					},100);
					alertpopup(updatealert);
				} else {
					setTimeout(function(){
						$('#processoverlay').hide();
						Materialize.updateTextFields();
					},25);
					alertpopup(submiterror);
				}		   
			},
		});
	}
	//retrieve
	function lotretrive()
	{
		var elementsname = ['18','lotnumber','lotdate','product','purityid','caratweight','grossweight','stoneweight','netweight','accountid','description','lotprimaryid','tagweight','tagstoneweight','pieces','lotcreationopt','proparentcategoryid','prodparentcategoryname','tagsumpieces','tagnetweight'];
		var datarowid = $('#lotgrid div.gridcontent div.active').attr('id');
		$.ajax({
			url:base_url+"Lot/lotretrive?primaryid="+datarowid,
			dataType:'json',
			async:false,
			success: function(data) {
				if(data.proparentcategoryid != '' && data.proparentcategoryid != 1) {
				  getpuritybasedoncategory(data.proparentcategoryid);
				  loadproductbasedoncategory(data.proparentcategoryid,'product');
				} else {
					var ddname = "product";
					$("#"+ddname+" option").removeClass("ddhidedisplay");
					$("#"+ddname+" option").prop('disabled',false);
					$('#product').select2('val',data.product);
				}
				var txtboxname = elementsname;
				var dropdowns = [4,'product','purityid','accountid','lotcreationopt'];
				textboxsetvalue(txtboxname,txtboxname,data,dropdowns);
				var stonestatus = $.trim($('#product').find('option:selected').data('stone'));
				var purity = $.trim($('#product').find('option:selected').data('purityid'));
				if(data.product == 1) {
					$('#stoneweight_div,#stoneapplicablediv').removeClass('hidedisplay');
					stonewtvalidate();
				} else {
					stoneweighthideshow(stonestatus);
				}
				if(purity != '') {
					loadpuritybasedoncategory(purity,'purityid',1);
				} else {
					var ddname = "purityid";
					$("#"+ddname+" option").removeClass("ddhidedisplay");
					$("#"+ddname+" option").prop('disabled',false);
				}
				$('#purityid').select2('val',data.purityid);
				$("#stoneapplicable").select2('val',data.stoneapplicable);
				if(data.lotused == '1') {
					$('#product,#purityid,#prodcategorylistbutton').prop('disabled',true);
				} else if(data.lotused == '0') {
					$('#product,#purityid,#prodcategorylistbutton').prop('disabled',false);
				}
				Materialize.updateTextFields();
			}
		});
	}
	//delete lot information
	function lotdelete(datarowid)
	{	
		var datarowid= $('#lotgrid div.gridcontent div.active').attr('id');
		setTimeout(function(){
			$('#processoverlay').show();
		},25);
		$.ajax({
			url: base_url + "Lot/lotdelete",
			data:{primaryid:datarowid},
			success: function(msg) {
				var nmsg =  $.trim(msg);
				$("#basedeleteoverlay").fadeOut();
				if (nmsg == 'SUCCESS') {	
						lotgrid();
						setTimeout(function(){
						$('#processoverlay').hide();
					},25);
					alertpopup(deletealert);
				} else {
						setTimeout(function(){
						$('#processoverlay').hide();
					},25);
					alertpopup(submiterror);
				}
			},
		});
	}
}
//
function checklotstatus(lotid){	
    var out = '';
	$.ajax({
        url: base_url + "Lot/checklotstatus",
        data:{primaryid:lotid},
		async:false,
		success: function(msg) 
        {
			out =  $.trim(msg);			
        },
    });
	return out;
}
// lot manual close
function lotmanualclose(lotid) {
	setTimeout(function(){
		$('#processoverlay').show();
	},25);
	$.ajax({
        url: base_url + "Lot/manualclose",
        data:{primaryid:lotid},
		async:false,
		success: function(msg) 
        {
			if(msg == 'SUCCESS'){
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				lotgrid();
				alertpopup('Lot Closed successfully');
			}else {
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
	        }			
        } 
    });
}
// lot manual close
function lotmanualopen(lotid) {
	setTimeout(function(){
		$('#processoverlay').show();
	},25);
	$.ajax({
        url: base_url + "Lot/manualopen",
        data:{primaryid:lotid},
		async:false,
		success: function(msg) 
        {
			if(msg == 'SUCCESS'){
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				lotgrid();
				alertpopup('Lot Opened successfully');
			}else {
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
	        }			
        } 
    });
}
//validation on edit for grossweight to not enter the greater to tag weight
function checktagweight(){
	var pendingtagweight =parseFloat($('#tagweight').val());	
	if(MODE == 'EDIT'){
		var grossweight = parseFloat($('#grossweight').val());		
		if(parseFloat(grossweight) < parseFloat(pendingtagweight)){
			return 'Gwt must not be less than tag wt '+pendingtagweight+'';
		}
	}
}
function checktagnetweight(){
	var pendingtagweight =parseFloat($('#tagnetweight').val());	
	if(MODE == 'EDIT'){
		var grossweight = parseFloat($('#netweight').val());		
		if(parseFloat(grossweight) < parseFloat(pendingtagweight)){
			return 'Nwt must not be less than tag wt '+pendingtagweight+'';
		}
	}
}
//validation on edit for pieces to not enter the greater to tag pieces
function checktagpieces(){
	var pendingtagpieces =parseFloat($('#tagsumpieces').val());	
	if(MODE == 'EDIT'){
		var pieces = parseFloat($('#pieces').val());		
		if(parseFloat(pieces) < parseFloat(pendingtagpieces)){
			return 'Pcs must not be less than tag pcs '+pendingtagpieces+'';
		}
	}
}
//validation on edit for grossweight to not enter the greater to tag weight
function checktagstoneweight(){
	var pendingtagstoneweight =parseFloat($('#tagstoneweight').val());	
	if(MODE == 'EDIT'){
		var stoneweight = parseFloat($('#stoneweight').val());		
		if(parseFloat(stoneweight) < parseFloat(pendingtagstoneweight)){
			return 'St wt must not be less than tag st wt '+pendingtagstoneweight+'';
		}
	}
}
function setrequirelotdata(){
	$("span","#productdivid label").remove();
	$("span","#purityiddivid label").remove();
	$("span","#accountiddivid label").remove();
	$("span","#categorydivhid label").remove();
	var lotdetails=lotdata.split(",");
	$.each(lotdetails, function (index, value) {
		if(value == 1){ // product
			$('#product').addClass('validate[required]');
			$('#productdivid label').append('<span class="mandatoryfildclass">*</span>');
		}else if(value == 2){ // purity
			$('#purityid').addClass('validate[required]');
			$('#purityiddivid label').append('<span class="mandatoryfildclass">*</span>');
		}else if(value == 3){ // account
			$('#accountid').addClass('validate[required]');
			$('#accountiddivid label').append('<span class="mandatoryfildclass">*</span>');
		}else if(value == 4){ // category
			$('#prodparentcategoryname').addClass('validate[required]');
			$('#categorydivhid label').append('<span class="mandatoryfildclass">*</span>');
		}
	});
}
// set lot type based on company setting
function setlottype() {
	ddname = 'lotcreationopt';
	if(lottype == 4) { // both
		$('#lotcreationopt').addClass('validate[required]');
		$("#"+ddname+"").select2('val',3).trigger('change');
	}else{
		$('#lotcreationopt').removeClass('validate[required]');
		$("#"+ddname+" option").addClass("ddhidedisplay");
		$("#"+ddname+" option").prop('disabled',true);
		$("#"+ddname+" option[value="+lottype+"]").removeClass("ddhidedisplay");
		$("#"+ddname+" option[value="+lottype+"]").prop('disabled',false);
		$("#"+ddname+"").select2('val',lottype).trigger('change');
		$("#"+ddname+"").prop('readonly',true);
		lottypehideshow(lottype);
	}
}
// load purchase bill no - without lot creation
function loadbillno() { 
   $('#purchasebillno').empty();
   $('#purchasebillno').append($("<option></option>"));
   $.ajax({
			url: base_url+"Lot/loadbillnumber",
			dataType:'json',
			type: "POST",
			async:false,
			success: function(msg) {
				$.each(msg, function(index) {
				$("#purchasebillno")
				.append($("<option></option>")
				.attr("data-billnumber",msg[index]['billno'])
				.attr("value",msg[index]['salesid'])
				.text(msg[index]['billno']));
			});
			},
		});
}
// purchase overlay grid
function purchasedetailgrid(page,rowcount) {
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = $("#purchasedetailgrid").width();
		var wheight = $("#purchasedetailgrid").height();
		/*col sort*/
		var sortcol = $("#purcahsedetailsortcolumn").val();
		var sortord = $("#purcahsedetailsortcolumn").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.purcahsedetailheadercolsort').hasClass('datasort') ? $('.purcahsedetailheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.purcahsedetailheadercolsort').hasClass('datasort') ? $('.purcahsedetailheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.purcahsedetailheadercolsort').hasClass('datasort') ? $('.purcahsedetailheadercolsort.datasort').attr('id') : '0';
		var purchasebillid = $('#purchasebillno').find('option:selected').val();
		$.ajax({
			url:base_url+"Lot/purchasegridinformationfetch?maintabinfo=sales&primaryid=salesid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+"&sortcol="+sortcol+"&sortord="+sortord+"&moduleid=52"+"&purchasebillid="+purchasebillid,			
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#purchasedetailgrid').empty();
				$('#purchasedetailgrid').append(data.content);
				$('#purchasedetailgridfooter').empty();
				$('#purchasedetailgridfooter').append(data.footer);
				/*data row select event*/
				datarowselectevt();
				/*column resize*/
				columnresize('purchasedetailgrid');
				{//sorting
					$('.purcahsedetailheadercolsort').click(function(){
						$('.purcahsedetailheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#purcahsedetailgridpgnumcnt li .page-text .active').data('rowcount');
						var sortcol = $('.purcahsedetailheadercolsort').hasClass('datasort') ? $('.purcahsedetailheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.purcahsedetailheadercolsort').hasClass('datasort') ? $('.purcahsedetailheadercolsort.datasort').data('sortorder') : '';
						$("#purcahsedetailsortorder").val(sortord);
						$("#purcahsedetailsortcolumn").val(sortcol);
						var sortpos = $('#purcahsedetailgrid .gridcontent').scrollLeft();
						takeorderdetailgrid(page,rowcount);
						$('#purchasedetailgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					});
					sortordertypereset('purcahsedetailheadercolsort',headcolid,sortord);
				}
			   var hideprodgridcol = ['innerpurityid','innerproductid','innercategoryid','accountid'];
				gridfieldhide('purchasedetailgrid',hideprodgridcol);
				//row based check box
				$(".takeorderdetail_rowchkboxclass").click(function() {
					$('.takeorderdetail_headchkboxclass').prop( "checked", false );
				});
			},
		});
	}
	// set purchase details in form
	function purchasedetailset(rowid) {
		var gridname = 'purchasedetailgrid';
		var purityid = getgridcolvalue(gridname,rowid,'innerpurityid','');
		var productid = getgridcolvalue(gridname,rowid,'innerproductid','');
		var category = getgridcolvalue(gridname,rowid,'category','');
		var categoryid = getgridcolvalue(gridname,rowid,'innercategoryid','');							  
		var vendorid = getgridcolvalue(gridname,rowid,'accountid','');
		var grosswt = getgridcolvalue(gridname,rowid,'innergrosswt','');
		var stonewt = getgridcolvalue(gridname,rowid,'innerstonewt','');
		var netwt = getgridcolvalue(gridname,rowid,'innernetwt','');
		var caratwt = getgridcolvalue(gridname,rowid,'innercaratwt','');
		var purity = getgridcolvalue(gridname,rowid,'innerpurityid','');
		var pieces = getgridcolvalue(gridname,rowid,'innerpieces','');
		var billrefno = getgridcolvalue(gridname,rowid,'billrefno','');
		$('#product').select2('val',productid).trigger('change');
		$('#purityid').select2('val',purityid);
		$('#accountid').select2('val',vendorid);
		$('#grossweight').val(grosswt);
		$('#stoneweight').val(stonewt);
		$('#netweight').val(netwt);
		$('#caratweight').val(caratwt);
		$('#salesdetailid').val(rowid);
		$('#pieces').val(pieces);
		$('#proparentcategoryid').val(categoryid); //Sapna - arvind changes
		$('#prodparentcategoryname').val(category); //Sapna - arvind changes
		$('#suppliernumber').val(billrefno);
		Materialize.updateTextFields();
	}
	function loadproductbasedoncategory(categoryid,ddname) { // load product based on category tree selection
	        if(categoryid == '')  {
				$("#"+ddname+"").select2('val','');
				$("#"+ddname+" option").removeClass("ddhidedisplay");
				$("#"+ddname+" option").prop('disabled',false);
			}else {
				$("#"+ddname+"").select2('val','');
				$("#"+ddname+" option").addClass("ddhidedisplay");
				$("#"+ddname+" option").prop('disabled',true);
				var categorydefaultid = 1;
				$("#"+ddname+" option[data-categoryid="+categorydefaultid+"]").removeClass("ddhidedisplay");
				$("#"+ddname+" option[data-categoryid="+categorydefaultid+"]").prop('disabled',false);
				$("#"+ddname+" option[data-categoryid="+categoryid+"]").removeClass("ddhidedisplay");
				$("#"+ddname+" option[data-categoryid="+categoryid+"]").prop('disabled',false);
			}
	}
	function loadpuritybasedoncategory(categoryid,ddname,status) { // load purity based on product selection
		if(categoryid == 1){
			$("#"+ddname+" optgroup").addClass("ddhidedisplay");
		    $("#"+ddname+" option").addClass("ddhidedisplay");
			$("#"+ddname+" option").prop('disabled',true);
			$("#"+ddname+"").select2('val','');
			$("#"+ddname+"").prop('disabled',false);
		}else if(categoryid == ''){
			$("#"+ddname+" optgroup").removeClass("ddhidedisplay");
		    $("#"+ddname+" option").removeClass("ddhidedisplay");
			$("#"+ddname+" option").prop('disabled',false);
			$("#"+ddname+"").select2('val','');
			$("#"+ddname+"").prop('disabled',false);
		}else{
			$("#"+ddname+" optgroup").addClass("ddhidedisplay");
		    $("#"+ddname+" option").addClass("ddhidedisplay");
			$("#"+ddname+" option").prop('disabled',true);
			$("#"+ddname+"").select2('val','');
			if(categoryid.indexOf(',') > -1) {
				$("#"+ddname+"").prop('disabled',false);
				var purity = categoryid.split(',');
				for(j=0;j<(purity.length);j++) {
					$("#"+ddname+" option[value="+purity[j]+"]").removeClass("ddhidedisplay");
					$("#"+ddname+" option[value="+purity[j]+"]").prop('disabled',false);
					$("#"+ddname+" option[value="+purity[j]+"]").parent().removeClass("ddhidedisplay");
				}
			}else{
				$("#"+ddname+" option[value="+categoryid+"]").removeClass("ddhidedisplay");
				$("#"+ddname+" option[value="+categoryid+"]").prop('disabled',false);
				//$("#"+ddname+"").select2('val',categoryid);
				$("#"+ddname+"").select2('val',categoryid).trigger('change');
				/* if(status == 1){
					$("#"+ddname+"").prop('disabled',true);
				} */
				$("#"+ddname+" option[value="+categoryid+"]").parent().removeClass("ddhidedisplay");
			}
		}
	}
	function getpuritybasedoncategory(listid) {
		$.ajax({
			url: base_url+"Lot/getpuritybasedoncategory",
			data:{primaryid:listid},
			dataType:'json',
			type: "POST",
			async:false,
			success: function(msg) {
				loadpuritybasedoncategory(msg,'purityid',0);
			},
		});
	}
	function lottypehideshow(val){
		if(val == 2) { // automatic
			$('.purchasebill').removeClass('hidedisplay');
			loadbillno();
			$("#purchasebillno").addClass('validate[required]');
			// $("#categorydivhid").addClass('hidedisplay');//Sapna -arvind changes made hidden
			$("#categorydivhid").removeClass('hidedisplay');//Sapna -arvind changes
			$('#caratweight,#grossweight,#stoneweight,#pieces').prop('readonly',true);
			$('#product,#purityid,#accountid,#prodcategorylistbutton').prop('disabled',true);//Sapna -arvind changes
		}else if(val == 3) { // manual
			$('.purchasebill').addClass('hidedisplay');
			$("#purchasebillno").removeClass('validate[required]');
			$("#categorydivhid").removeClass('hidedisplay');
			$('#caratweight,#grossweight,#stoneweight,#pieces').prop('readonly',false);
			$('#product,#purityid,#accountid,#prodcategorylistbutton').prop('disabled',false);
		}
	}
	function stoneweighthideshow(stonestatus) {
		if(stonestatus == 'Yes') {
			$('#stoneweight_div,#stoneapplicablediv').removeClass('hidedisplay');
			$("#stoneapplicable").select2('val','No');
			stonewtvalidate();
		} else {
			$('#stoneweight').removeClass('validate[required,decval['+weight_round+'],funcCall[validategreater],funcCall[checktagstoneweight],custom[number],maxSize[15]]');
			$('#stoneweight_div,#stoneapplicablediv').addClass('hidedisplay');
			$("#stoneapplicable").select2('val','No');
		}
	}
	function stonewtvalidate() {
		$('#stoneweight').addClass('validate[required,decval['+weight_round+'],funcCall[validategreater],funcCall[checktagstoneweight],custom[number],maxSize[15]]');
	}
	function errorweightvalidate(datarowid) {
		var caratwt = getgridcolvalue('lotgrid',datarowid,'caratweight','');
		$('#errorgrossweight_div,#errornetweight_div,#errorcaratweight_div').removeClass('hidedisplay');
		if(caratwt > 0) {
			$('#errorcaratweight_div').removeClass('hidedisplay');
			$('#errorgrossweight_div,#errornetweight_div').addClass('hidedisplay');
			$('#errorcaratweight').attr('data-validation-engine','validate[required,decval['+dia_weight_round+'],maxSize[15]]');
			$('#errorgrossweight').attr('data-validation-engine','');
		} else {
			$('#errorcaratweight_div').addClass('hidedisplay');
			$('#errorgrossweight_div,#errornetweight_div').removeClass('hidedisplay');
			$('#errorgrossweight').attr('data-validation-engine','validate[required,decval['+weight_round+'],maxSize[15]]');
			$('#errorcaratweight').attr('data-validation-engine','');
		}
		$('#vadminpassword').addClass('validate[required]');
	}
</script>