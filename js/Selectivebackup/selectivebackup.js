$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}
	{// Grid Calling Function
		backupgrid();
	}
	$('#groupcloseaddform').hide();
	{ //keyboard shortcut reset global variable
		viewgridview = 0;
	}
	$("#backupsettingsmailoverlay").click(function(){
			$('#salescommentoverlay').fadeIn();		
			firstfieldfocus();
		});
		$("#backupsettingsclose").click(function(){
			$('#salescommentoverlay').fadeOut();			
	});
	
	
	{// Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}		
		});
	}
	{ //Backup location creation Overlay
		$('#backuplocation,#backupdelete,#backupupload,#performanceboost,#accledgerarchive').click(function(){
			$('.drivenamedd-div').show(); 
			$('.performanceop-div').hide();  
			$('.accledgerop-div').hide(); 
			$('#drivename').addClass('validate[required]');					
			var name = $(this).attr('id');
			if(name == 'backuplocation'){
				$('#selbackupheader').text('Selective Backup'); 
				$('#backupopmode').val(1); 
			}else if(name == 'backupdelete'){
				$('#selbackupheader').text('Latest (last) Selective Backup Delete'); 
				$('#backupopmode').val(2); 
			}else if(name == 'backupupload'){
				$('#selbackupheader').text('Selective Backup Upload'); 
				$('#backupopmode').val(3); 
			}
			else if(name == 'performanceboost'){
				$('#selbackupheader').text('Performance Booster'); 
				$('#backupopmode').val(4); 
				$('.drivenamedd-div').hide(); 
				$('.performanceop-div').show();  
				$('#drivename').removeClass('validate[required]');			
			}
			else if(name == 'accledgerarchive'){
				$('#selbackupheader').text('Accounts Ledger Archive and Clearance'); 
				$('#backupopmode').val(5); 
				$('.drivenamedd-div').hide(); 
				$('.accledgerop-div').show(); 
				$('#drivename').attr('data-validation-engine','');
			}
			var drive = $('#backupdrive').val();
			$('#drivename').select2('val',drive).trigger('change');
			$('#adminpassword').val('').focus();
			$('#backuplocationovrelay').show();
		});
		$('#backupoverlayclose').click(function(){
			$('#backupopmode').val(0); 
			$('#backuplocationovrelay').fadeOut();
			
		});
	}
	
	{//download backup file
		//data base settings update
		$('#locationsubmit').click(function(e){
			$("#locationvalidationwizard").validationEngine('validate');
		});
		jQuery("#locationvalidationwizard").validationEngine({
			onSuccess: function() {
			  var name = $('#backupopmode').val();
			  var maxaccledgerid = 0;
			  var uploadsucess = 0;
			  $("#processoverlay").show();
			  if(name == '1'){
					$.ajax({type:'POST',async:false, url:base_url+'selectbackup/backup.php', data:$('#locationsetform').serialize(), 
					success: function(response) {
						if(response == 'Success') {
							$('#processoverlay').hide();
							alertpopup('Data backup created successfully!');
							backuprefreshgrid();
							$('#backuplocationovrelay').fadeOut();
							$('#backupopmode').val(0); 
						} else if(response == 'WRONG'){
							$('#processoverlay').hide();
							alertpopup('Your admin password is wrong');
							$('#adminpassword').val('').focus();
						}else if(response == 'FAIL'){
							$('#processoverlay').hide();
							alertpopup('Missing backup folder and file in your backup drive');
							$('#backuplocationovrelay').fadeOut();
							$('#backupopmode').val(0); 
						}
				 },
			   });
			 }else if(name == '2'){
				var backupids = $('#backupgrid div.gridcontent div.active').attr('id');
				if(backupids) {
					var userid=$('#updateuser').val();
					var drivename=$('#drivename').val();
					var adminpassword=$('#adminpassword').val();
					$.ajax({
						url:base_url+"selectbackup/deletebkp.php?primaryid="+backupids+"&drivename="+drivename+"&adminpassword="+adminpassword+"&backupuser="+userid,
						async:false,
						success: function(msg) {
							if(msg=='SUCCESS') {
								$('#processoverlay').hide();
								alertpopup("Select file are deleted");
								backuprefreshgrid();
								$('#backuplocationovrelay').fadeOut();
								$('#backupopmode').val(0); 
							}else if(msg=='CANNOT') {
								$('#processoverlay').hide();
								alertpopup("This is not the latest backup. kindly delete latest backup first.");
								$('#backuplocationovrelay').fadeOut();
								$('#backupopmode').val(0); 
							}else if(msg=='UNAUTHORIZE') {
								$('#processoverlay').hide();
								alertpopup("You don't have permission to delete");
								$('#backuplocationovrelay').fadeOut();
								$('#backupopmode').val(0); 
							}else if(msg == 'WRONG'){
								$('#processoverlay').hide();
								alertpopup('Your admin password is wrong');
								$('#adminpassword').val('').focus();
							}else if(msg == 'FAIL'){
								$('#processoverlay').hide();
								alertpopup('Missing backup folder and file in your backup drive');
								$('#backuplocationovrelay').fadeOut();
								$('#backupopmode').val(0); 
							}else if(msg == 'UPLOAD'){
								$('#processoverlay').hide();
								alertpopup('Cannot Delete Upload entry');
								$('#backuplocationovrelay').fadeOut();
								$('#backupopmode').val(0); 
							}
							
						},
					});
				} else {
					$('#processoverlay').hide();
					alertpopup('Please select a row');
				} 
			 }else if(name == '3'){
				 $.ajax({type:'POST',async:false, url:base_url+'selectbackup/uploadselbkp.php', data:$('#locationsetform').serialize(), success: function(response) {
						
						if(response > 0) {
							maxaccledgerid = response;
							uploadsucess = 1;
						} else if(response == 'WRONG'){
							$('#processoverlay').hide();
							alertpopup('Your admin password is wrong');
							$('#adminpassword').val('').focus();
						}else if(response == 'FAIL'){
							$('#processoverlay').hide();
							alertpopup('Missing backup folder and file in your backup drive');
							$('#backuplocationovrelay').fadeOut();
							$('#backupopmode').val(0); 
						}else if(response == 'error'){
							$('#processoverlay').hide();
							alertpopup('Kindly Inform software provider there is error at few places while importing.');
							$('#backuplocationovrelay').fadeOut();
							$('#backupopmode').val(0); 
						}else if(response == 'previouserror'){
							$('#processoverlay').hide();
							alertpopup('Kindly Contact software provider,Still Previous Import Error is unresolved.');
							$('#backuplocationovrelay').fadeOut();
							$('#backupopmode').val(0); 
						}
				  },
			    });
				if(maxaccledgerid > 0){
				  maxaccledgerid = 1;
				  $.ajax({type:'POST',async:false, url:base_url+'Selectivebackup/accledgerupdate',data:{maxaccledgerid:maxaccledgerid}, success: function(response) {
						if(response=='Success') {
							$('#processoverlay').hide();
							alertpopup('Data Uploaded successfully');
							backuprefreshgrid();
							$('#backuplocationovrelay').fadeOut();
							$('#backupopmode').val(0); 
						}else {
							$('#processoverlay').hide();
							alertpopup('Kindly Inform software provider there is error While Syncing the account ledger.');
							$('#backuplocationovrelay').fadeOut();
							$('#backupopmode').val(0); 
						}
					},
				  });
			  }else if(uploadsucess == 1){
				$('#processoverlay').hide();
				alertpopup('Data Uploaded successfully. But Accountledger is not Re-synced yet.');
				backuprefreshgrid();
				$('#backuplocationovrelay').fadeOut();
				$('#backupopmode').val(0); 
			  }
			 }
			 else if(name == '4'){
				 
				 var adminpassword=$('#adminpassword').val();
				 var performanceop=$('#performanceop').find('option:selected').val();
				 $.ajax({type:'POST',async:false, url:base_url+'Selectivebackup/performanceboost',data:{adminpassword:adminpassword,performanceop:performanceop}, success: function(response) {
						if(response=='Success') {
							$('#processoverlay').hide();
							alertpopup('Performance Boosted successfully!');
							backuprefreshgrid();
							$('#backuplocationovrelay').fadeOut();
							$('#backupopmode').val(0); 
						} else if(response == 'WRONG'){
							$('#processoverlay').hide();
							alertpopup('Your admin password is wrong');
							$('#adminpassword').val('').focus();
						}else if(response == 'FAIL'){
							$('#processoverlay').hide();
							alertpopup('No latest Backup found.Kindly Create backup then Try To delete');
							$('#adminpassword').val('').focus();
						}else {
							$('#processoverlay').hide();
							alertpopup('Kindly Inform software provider there is error at few places while performanceboost operation.');
							$('#backuplocationovrelay').fadeOut();
							$('#backupopmode').val(0); 
						}
						
				  },
			    });
			 }
			 else if(name == '5'){
				 var adminpassword=$('#adminpassword').val();
				 var accledgerop=$('#accledgerop').find('option:selected').val();
				 $.ajax({type:'POST',async:false, url:base_url+'Selectivebackup/accledgeroperation',data:{adminpassword:adminpassword,accledgerop:accledgerop}, success: function(response) {
						
						if(response=='Success') {
							$('#processoverlay').hide();
							alertpopup('Data Uploaded successfully!');
							backuprefreshgrid();
							$('#backuplocationovrelay').fadeOut();
							$('#backupopmode').val(0); 
						} else if(response == 'WRONG'){
							$('#processoverlay').hide();
							alertpopup('Your admin password is wrong');
							$('#adminpassword').val('').focus();
						}else if(response == 'error'){
							$('#processoverlay').hide();
							alertpopup('Kindly Inform software provider there is error at few places while importing.');
							$('#backuplocationovrelay').fadeOut();
							$('#backupopmode').val(0); 
						}
						
				  },
			    });
			 }
			},
			onFailure: function() {
			
			}
		});
	}
	{//Check box Operation
	
		$('#reccheckbox').change(function() {
				if($(this).is(":checked")) {
					$("#recurspan").show();
			} else{
				$("#recurspan").hide();
			}     
		});
	}
});
{//backup grid
	function backupgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 30 : rowcount;
	var wwidth = $('#backupgrid').width();
	var wheight = $('#backupgrid').height();
	/*col sort*/
	var sortcol = $('.backupheadercolsort').hasClass('datasort') ? $('.backupheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.backupheadercolsort').hasClass('datasort') ? $('.backupheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.backupheadercolsort').hasClass('datasort') ? $('.backupheadercolsort.datasort').attr('id') : '0';
	var userviewid ='';
	var viewfieldids ='';
	$.ajax({
		url:base_url+"Selectivebackup/gridinformationfetch?viewid="+userviewid+"&maintabinfo=selectivebackup&primaryid=selectivebackupid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=60'+"&checkbox=true",
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#backupgrid').empty();
			$('#backupgrid').append(data.content);
			$('#backupgridfooter').empty();
			$('#backupgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('backupgrid');
			{//sorting
				$('.backupheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.backupheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortpos = $('#itemtaggrid .gridcontent').scrollLeft();
					backupgrid(page,rowcount);
					$('#smscampaignsubaddgrid1 .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('backupheadercolsort',headcolid,sortord);
			}
			{//pagination
					$('.pvpagnumclass').click(function(){
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						backupgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#backuppgrowcount').change(function(){
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('#prev').data('pagenum');
						backupgrid(page,rowcount);
						$("#processoverlay").hide();
					});
			}
			//Material select
				$('#backuppgrowcount').material_select();
		},
	});
}
}
function backuprefreshgrid() {
	var page = $('ul#backuppgnum li.active').data('pagenum');
	var rowcount = $('ul#backuppgnumcnt li .page-text .active').data('rowcount');
	backupgrid(page,rowcount);
}
