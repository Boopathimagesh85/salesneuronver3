$(document).ready(function() 
{
	{//Foundation Initialization
		$(document).foundation();
		rtaddcount=0;
		cntaddcount=0;
		htmlview = 0;
		globalid='all';
		htmlfilename="";
	}
	//$('#savereporttodb').hide();
	{//Support overlay enable
		$('.supportoverlay').css("display","block");
	}
	{	/* Reports Creation Overlay Start */	
		{//Date Range
			daterangefunction();
		}
		{// Grid Calling Function
			reportcreateconditiongrid();
		}
		$("#reportcondediticon").hide();
		/* Onload => Load the folder dd values
		*/
		loadreportfolderin();
		/* Override the select2 default functionalities.
			Select2 into List box conversion.
		*/
		$('#viewtoggle').click(function() {
			$("#overlayreportsfilterdisplay").css('display','block');
			$('#overlayreportsfilterdisplay').fadeIn();
		});
		$('#filterclose').click(function() {
			$('#overlayreportsfilterdisplay').fadeOut();
			$("#overlayreportsfilterdisplay").css('display','none');
		});
		{//folder open for mobile
			$("#folderaddicon").click(function(){
				sectionpanelheight('groupsectionoverlay');
				$("#groupsectionoverlay").removeClass("closed");
				$("#groupsectionoverlay").addClass("effectbox");
				Materialize.updateTextFields();
				firstfieldfocus();
			});
		}
		$("#reportmaincolumnname,#reportmaincolumnname_to").select2('destroy');	
		$("#reportgroupby1,#reportgroupby1_to").select2('destroy');	
		$("#reportsviewtoggle").addClass('hidedisplay');
		//newoverlay for reports
		$('#newoverlay').click(function() {
			$("#newoverlayform").show(); 
			getreportsdata();
			$("#reporttype").val('TV');
		});
		$('#closeoverlay').click(function() {
			if($("#overlaystatus").val()!='') {
				$("#newoverlayform").hide(); 
			} else {
				$("#newoverlayform").hide(); 
				$("#reportviewoverlay").hide();
				
			}
		});
		$('#closetabularoverlay').click(function() {
			$("#tabularviewoverlay").fadeOut();
			cleargriddata('tabulargrid');
		});
		$('#reptransfericon').click(function() {
			var transferyesno = $('#transferyesno').val();
			if(transferyesno == 'no') {
				$('#transferyesno').val('yes');
				$('#reptransfericon').text('Trnf Yes');
			} else {
				$('#transferyesno').val('no');
				$('#reptransfericon').text('Trnf No');
			}
		});
		 
		{ /* Group / Order By */
			$(".orderby").click(function(){
				var viewids = [];
				var selectedviewids=$("#reportgroupby1_to option:selected").val();
				if(selectedviewids!='')
				{	
					$('#reportgroupby1_to :selected').each(function(i, selected) {
						viewids[i] = $(selected).val();
					});
					if(viewids.length<=1)
					{	
						var currenttext=$("#reportgroupby1_to option[value="+selectedviewids+"]").text().split("-");
						var selectedname=$(this).val(); //alert(selectedviewids);
						$("#reportgroupby1_to option[value="+selectedviewids+"]").attr('data-bytype',selectedname);
						$("#reportgroupby1_to option[value="+selectedviewids+"]").text(currenttext[0]+" - " +selectedname);
					}
					else
					{
						alertpopup("Please select single value.");
					}
				}
				else
				{
					alertpopup("Please select value.");
				}
			});
			
			$("#reportgroupby1_rightSelected").click(function() {
				var k = 0;
				$('#reportgroupby1_to option').each(function(i, selected) {
					if($(this).attr('data-bytype') == '') {
						$(this).attr('data-bytype','asc');
						var currenttext=$(this).text();
						$(this).text(currenttext+'- asc');
						k++;
					}
				});
				$('#groupbyrange').val(k);
			});
			$("#reportgroupby1_leftSelected").click(function() {
				$('#reportgroupby1 option').each(function(i, selected) {
					if($(this).attr('data-bytype') != '') {
						$(this).attr('data-bytype','');
						var currenttext=$(this).text().split("-");
						$(this).text(currenttext[0]);
					}
				});
				var h = 0;
				$('#reportgroupby1_to option').each(function(i, selected) {
					if($(this).attr('data-bytype') != '') {
						h--;
					}
				});
				$('#groupbyrange').val(Math.abs(h));
			});
		}
		{
			{ 	/* CODE CLEANING Start
				*/
				//loads the module name based on MenuCategory Selection		
				$("#reportcategory").change(function(){  
					$("#reportmodule").empty();
					$("#reportmodule").select2('val','')
					$('#reportmodule').append($("<option></option>"));
					var partentid=$(this).val();
					$.ajax({
							url:base_url+"Reportsview/loadmodulenamebasedcategory?partentid="+partentid, 
							dataType:'json',
							async:false,
							success: function(data) { 
									$.each(data, function(index){					
										$('#reportmodule')
										.append($("<option></option>")
										.attr("value",data[index]['moduleid'])
										.text(data[index]['modulename']));
									});	
							    }
						  });	
				  });
					
				/*  Report Module dd change.
					Tbl : Fetch value from module table based on Menu Category 
				*/ 
				$('#reportmodule').change(function() {
					cleargriddata('reportcreateconditiongrid');
					cleargriddata('reportcalculationgrid');
					clearform('reportcondclear');
					clearform('reportgroupbysortby');
					clearform('reportcalculationform');	
					$("#reportmaincolumnname,#reportmaincolumnname_to,#reportgroupby1,#reportgroupby1_to").empty();
					$("#reportmaincolumnname,#reportmaincolumnname_to,#reportgroupby1,#reportgroupby1_to,#reportcalculationon,#reportcondcolumn,#report_datefield,#report_range").select2('val','');
					$("#reportgroupby1,#reportcalculationon,#reportcondcolumn,#report_startdate,#report_enddatedate").empty();
					$('#reportsortby1,#reportsortby2,#reportsortby3,#reportsortby4').attr('checked', false);
					$("#reportmaincolumnname,#reportmaincolumnname_to").empty();
					var moduleid = $(this).val();
					var reporttypeid = $('#reporttypeid').find('option:selected').val();
					if(checkValue(moduleid) == true){
						if(moduleid  == 49 || moduleid  == 41) {
							$("#reportmoduletabdiv").removeClass('hidedisplay');
							loadtabsectionname(moduleid);
						} else {
							$("#reportmoduletabdiv").addClass('hidedisplay');
							if(reporttypeid != 5) {
								loadcolumnname(moduleid);
								//*related to modules load*//
								$('#reportrelatedmodule').empty();
								$.ajax({
									url: base_url + "Dashboardbuilder/relatedmodulelistfetchmodel",			
									data: "moduleid="+moduleid,
									dataType:'json',
									type: "POST",
									async:false,
									cache:false,
									success: function(data) {
										var newddappend="";
										var newlength=data.length;
										for(var m=0;m < newlength;m++){
											if(moduleid != data[m]['datasid']){
											newddappend += "<option value ='"+data[m]['datasid']+"' data-mergemoduleidhidden='"+data[m]['datasid']+"' data-mergemoduletype='"+data[m]['datatype']+"'>"+data[m]['dataname']+ " </option>";
											}
										}
										//after run
										$('#reportrelatedmodule').append(newddappend);
									},
								});
							}
						}
					}
				});
				//module tab group change
				$("#reportmoduletab").change(function() {
					var moduleid = $("#reportmodule").val();
					var tabsction = $("#reportmoduletab").val();
					loadcolumnname(moduleid,tabsction);
				});
				
			/*  Related Module dd change.
				Tbl : Module Relation Table.
			*/ 
			$('#reportrelatedmodule').change(function() {
				$("#s2id_reportrelatedmodule,.select2-results").removeClass('error');
				$("#s2id_reportrelatedmodule").find('ul').removeClass('error');
				//resetvalues
				cleargriddata('reportcreateconditiongrid');
				clearform('reportcondclear');
				clearform('reportgroupbysortby');
				$("#reportmaincolumnname,#reportmaincolumnname_to,#reportgroupby1,#reportgroupby1_to").empty();
				$("#reportmaincolumnname,#reportmaincolumnname_to,#reportgroupby1,#reportgroupby1_to,#reportcalculationon,#reportcondcolumn,#report_datefield,#report_range").select2('val','');
				$("#reportgroupby1,#reportgroupby1_to,#reportcalculationon,#reportcondcolumn,#report_startdate,#report_enddatedate").empty();
				$('#reportsortby1,#reportsortby2,#reportsortby3').attr('checked', false);
				$("#reportmaincolumnname,#reportmaincolumnname_to").empty();				
				/* While close all related module means,it return null value.So View creation column for main module not loading in DD.To Avoid this we add this condition*/
				var releationmoduleids='';
				if($(this).val()!=null) {
					releationmoduleids=$(this).val(); 
				} else {
					releationmoduleids=$("#reportmodule").val();
				}
				//load the modules. 
				var related_module = releationmoduleids;		
				if(checkValue(related_module) == true){			
					var moduleid = $('#reportmodule').val();
					var selectmoduleid = moduleid;
					var combined_module = moduleid+','+related_module;
					loadcolumnname(combined_module);
				}
			});
			/* Advanced Filter Condition Edit => Set the values to DD & Textbox. */
			$('#reportcondediticon').click(function(){
				var viewconrowid = $('#reportcreateconditiongrid div.gridcontent div.active').attr('id');
				if(viewconrowid != "") {
					var reportcondcolumnid=getgridcolvalue('reportcreateconditiongrid',viewconrowid,'reportcondcolumn','');
					var reportcondition=getgridcolvalue('reportcreateconditiongrid',viewconrowid,'reportcondition','');
					var reportaggregate=getgridcolvalue('reportcreateconditiongrid',viewconrowid,'reportaggregate','');
					var reportandorcond=getgridcolvalue('reportcreateconditiongrid',viewconrowid,'reportandorcond','');
					var finalreportcondvalueid=getgridcolvalue('reportcreateconditiongrid',viewconrowid,'finalreportcondvalueid','');
					var finalreportcondvalue=getgridcolvalue('reportcreateconditiongrid',viewconrowid,'finalreportcondvalue','');
					$("#reportcondcolumn").select2('val',reportcondcolumnid).trigger("change");
					$("#reportaggregate").select2('val',reportaggregate).trigger("change");
					$("#reportcondition").select2('val',reportcondition);							
					$("#reportandorcond").select2('val',reportandorcond);
					if($("#reportcondvaluedivhid").is(":visible")==true) {
						$("#reportcondvalue").val(finalreportcondvalueid);
						$("#finalreportcondvalueid").val(finalreportcondvalueid);
						$("#finalreportcondvalue").val(finalreportcondvalue);
					} else if($("#reportddcondvaluedivhid").is(":visible")==true) {
						coditionvalue=finalreportcondvalueid.split(",");
						coditionname=finalreportcondvalue.split(",");
						$("#reportddcondvalue").select2('val',coditionvalue);
						$("#finalreportcondvalueid").val(coditionvalue);
						$("#finalreportcondvalue").val(coditionname);
					} else if($("#reportdatecondvaluedivhid").is(":visible")==true) {
						$("#reportdatecondvalue").val(finalreportcondvalueid);
					}
					var condcnt = $('#reportcreateconditiongrid .gridcontent div.data-content div').length; 
					if(condcnt<=1) {
						$('.condmandclass').text('');
						$("#reportandorconddivhid").hide();
						$('#reportandorcond').removeClass('validate[required]');
						var dropdownid =['2','reportcondcolumn','reportcondition'];
					} else {
						if(getgridcolvalue('reportcreateconditiongrid',viewconrowid,'reportandorcond','')!='') {		
							$('.condmandclass').text('*');
							$("#reportandorconddivhid").show();
							$('#reportandorcond').addClass('validate[required]');
							var dropdownid =['3','reportcondcolumn','reportcondition','reportandorcond'];
						} else {
							$('.condmandclass').text('');
							$("#reportandorconddivhid").hide();
							$('#reportandorcond').removeClass('validate[required]');
						}
					}
					$("#editconditionid").val(viewconrowid);
					Materialize.updateTextFields();
				} else {
					alertpopup("Please select a row");
				}
				});
				/* Advanced Filter Condition Delete => Delete the grid row and check AND/OR Condition
				*/
				$('#reportconddeleteicon').click(function(){
					var viewconrowid = $('#reportcreateconditiongrid div.gridcontent div.active').attr('id');
					if (viewconrowid != "") {
						deletegriddatarow('reportcreateconditiongrid',viewconrowid);
						if(viewconrowid != '') {
							var did = $('#reportconrowcolids').val();
							$('#reportconrowcolids').val(did+','+viewconrowid);
						}
						var condcnt = $('#reportcreateconditiongrid .gridcontent div.data-content div').length;
						if(condcnt==0) {
							$('.condmandclass').text('');
							$("#reportandorconddivhid").hide();
							$('#reportandorcond').removeClass('validate[required]');
							var dropdownid =['2','reportcondcolumn','reportcondition'];
						} else {
							$('.condmandclass').text('*');
							$("#reportandorconddivhid").show();
							$('#reportandorcond').addClass('validate[required]');
							var dropdownid =['3','reportcondcolumn','reportcondition','reportandorcond'];
						}
					 } else {
						alertpopup("Please select a row");
					 }
				});	
				/* Set final condition value to hidden textbox.*/
				{
					$("#reportcondvalue").focusout(function(){
						var value = $("#reportcondvalue").val();
						$("#finalreportcondvalue,#finalreportcondvalueid").val(value);
					});
					$("#reportddcondvalue").change(function(){
						var value = $("#reportddcondvalue").val();			
						var data = $(this).select2('data');
						var finalResult = [];
						var finaltextResult = [];
						for( item in $(this).select2('data') ) {
							finalResult.push(data[item].text);
						};
						var selectid = finalResult.join(',');
						$("#finalreportcondvalueid").val(value);
						$("#finalreportcondvalue").val(selectid);
					});
				}
				$('#report_startdate,#report_enddate').attr("disabled",true);
				//report range change function
				$('#report_range').change(function() {
					if($(this).val() == 'custom'){
						$('#startdatefieldcondvaluedivhid,#enddatefieldcondvaluedivhid').show();
						$('#report_startdate,#report_enddate').addClass('validate[required]');
						var uitypeid = $("#report_datefield").find('option:selected').data('uitypeid');
						if(uitypeid == '31'){
							$('.reportdatepickerdiv').addClass('hidedisplay');
							$('.reportdatetimepickerdiv').removeClass('hidedisplay');
							datetimepickerstartendtrigger();
						}else{
							$('.reportdatepickerdiv').removeClass('hidedisplay');
							$('.reportdatetimepickerdiv').addClass('hidedisplay');
							$('#report_startdate,#report_enddate').removeAttr('disabled');
							$('#report_startdate').datetimepicker('option', 'minDate', null);
						}
					}else{
						$('#report_startdate,#report_enddate').val('');
						$('#report_startdate,#report_enddate').attr("disabled",true);
						$('#report_startdate,#report_enddate').removeClass('validate[required]');
						$('#startdatefieldcondvaluedivhid,#enddatefieldcondvaluedivhid').hide();						
					}
				});
				//Advanced filter => Form to Grid.
				$('#reportaddcondsubbtn').click(function() {
					andorcondvalidationreset();
					setTimeout(function(){
						$("#reportformconditionvalidation").validationEngine('validate');
					},50);
				});
				jQuery("#reportformconditionvalidation").validationEngine({
					onSuccess: function() {
						var gridname='reportcreateconditiongrid';						
						cntaddcount++;		
						/*Form to Grid*/
						if($("#editconditionid").val()==''){
							formtogriddata_jewel(gridname,'ADD','last','');
						} else{
							var selectedrow = $('#'+gridname+' div.gridcontent div.active').attr('id');
							formtogriddata_jewel(gridname,'UPDATE','',selectedrow);
						}
						clearform('reportcondclear');
						/* Data row select event */
						datarowselectevt();				
						if(cntaddcount==1){
							$('.condmandclass').text('*');
							$('#reportandorcond').addClass('validate[required]');
							var dropdownid =['4','reportcondcolumn','reportcondition','reportaggregate','reportandorcond'];
							dropdownfailureerror(dropdownid);
							var condcnt = $('#reportcreateconditiongrid .gridcontent div.data-content div').length;
							if(condcnt==0) {
								$("#reportandorconddivhid").hide();
							} else {
								$("#reportandorconddivhid").show();
							}
						} else {
							var condcnt = $('#reportcreateconditiongrid .gridcontent div.data-content div').length;
							if(condcnt==0) {
								$('.condmandclass').text('');
								$("#reportandorconddivhid").hide();
								$('#reportandorcond').removeClass('validate[required]');
								var dropdownid =['3','reportcondcolumn','reportcondition','reportaggregate'];
								dropdownfailureerror(dropdownid);
							} else {
								$('.condmandclass').text('*');
								$("#reportandorconddivhid").show();
								$('#reportandorcond').addClass('validate[required]');
								var dropdownid =['4','reportcondcolumn','reportcondition','reportaggregate','reportandorcond'];
								dropdownfailureerror(dropdownid);
							}
						}
						andorcondvalidationreset();
					},
					onFailure: function() {
						var dropdownid =['5','reportcondcolumn','reportcondition','reportaggregate','reportandorcond','reportcondvalue'];
						dropdownfailureerror(dropdownid);
						alertpopup(validationalert);
						if($('#viewcolname').val() == '') {
							$("#s2id_viewcolname").find('ul').addClass('error');
						}
					}				
				});
			}
			/* Select Column */
			$("#reportmaincolumnname_rightSelected,#reportmaincolumnname_rightAll").click(function(){
				$('#reportmaincolumnname_to option').each(function(i, selected) {
					if($(this).attr('data-applysummary') == '') {
						$(this).attr('data-applysummary','no');				
					}
				});
			});
			/* Summary overlay  */
			 $('#discountclose').click(function(){ 
				 $("#summaryoverlay").fadeOut();
			 });	
			 $('#btnsummary').click(function() {
				 if($('#reportmaincolumnname_to :selected').length!=0) {
					$("#summaryoverlay").fadeIn();
					$("#aggregatemethod").html('');
					var uitype = [];
					var aggregatemethodid = [];
					var aggregatemethodkey = [];
					$('#reportmaincolumnname_to :selected').each(function(i, selected) {
						uitype[i] = $(selected).attr('data-uitype');
						aggregatemethodid[i] = $(selected).attr('data-aggregatemethodid');
						aggregatemethodkey[i] = $(selected).attr('data-aggregatemethodkey');
					});
					if(uitype.length<=1) {
						var value=$("#reportmaincolumnname_to option:selected").val();
						var uitype=$("#reportmaincolumnname_to option:selected").attr('data-uitype');
						var selectedamids=$("#reportmaincolumnname_to option:selected").attr('data-selecttype').split(",");
						var aggregatemethodid=$("#reportmaincolumnname_to option:selected").attr('data-aggregatemethodid').split(",");
						var aggregatemethodkey=$("#reportmaincolumnname_to option:selected").attr('data-aggregatemethodkey').split(",");
						var inputs=[];
						var ids=[];
						for(i=0;i<aggregatemethodid.length;i++) {
							if($.inArray(aggregatemethodid[i],selectedamids)!== -1) {
								var chk='checked';
							} else {
								var chk='';
							}
							inputs.push('<div class="input-field overlayfield"><input type="checkbox" id="chk_'+aggregatemethodid[i]+'" name="'+aggregatemethodkey[i]+'" class="filled-in checkboxcls radiochecksize summarychk" data-id="'+value+'" value="'+aggregatemethodid[i]+'" '+chk+'><label for="chk_'+aggregatemethodid[i]+'">'+aggregatemethodkey[i]+'</label>');
						}
						$("#aggregatemethod").append(inputs);	
						$("#newoverlay2").show();
					} else {
						alertpopup("Please select single value.");
					}			
				} else {
					alertpopup("Please select value.");
				}
			 });
			 //summary overlay submit
			 $("#btnsummaryfieldcheck").click(function() {
				var selectedviewids=$('.summarychk:checked').map(function() {return this.getAttribute('data-id');}).get().join(',');
				var selectedamids=$('.summarychk:checked').map(function() {return this.value;}).get().join(',');
				var selectedname=$('.summarychk:checked').map(function() {return this.name;}).get().join(',');
				//var selectedname = [];
				//$('.summarychk:checked').map(function() {alert(this.name);});
				selectedviewids=$.unique(selectedviewids.split(','));
				if(selectedamids.length!=0) {
					$("#reportmaincolumnname_to option[value="+selectedviewids+"]").attr("data-selecttype",selectedamids);
					$("#reportmaincolumnname_to option[value="+selectedviewids+"]").attr("data-selecttypename",selectedname);
					var currenttext=$("#reportmaincolumnname_to option[value="+selectedviewids+"]").text().split("-");
					$("#reportmaincolumnname_to option[value="+selectedviewids+"]").text(currenttext[0]+" - " +selectedname);
				} else {
					var optionval=$("#reportmaincolumnname_to option:selected").val();
					$("#reportmaincolumnname_to option[value="+optionval+"]").attr("data-selecttype",'');
					$("#reportmaincolumnname_to option[value="+optionval+"]").attr("data-selecttypename",'');
					var currenttext=$("#reportmaincolumnname_to option[value="+optionval+"]").text().split("-");
					$("#reportmaincolumnname_to option[value="+optionval+"]").text(currenttext[0]);				
				}
				$("#newoverlay2").hide();
			 });
		}		
		{
			{ // Generate Report
				$('#generatereport').click(function() {
					$("#actiontype").val('GR'); // GR => Generate Report 
					$("#hdntabularoverlay").val('NO');
					$("#reportfilterid").val('');
					$("#reportddfilterid").val('');
					$("#reportconditionname").val('');
					$("#reportddconditionname").val('');
					$("#reportfiltervalue").val('');
					$("#reportddfiltervalue").val('');
					$("#validatereportbasicdetails").validationEngine('validate');
				});
				$('#savereporttodb').click(function() {
					$("#actiontype").val('SR'); // SR => Save Report
					$('.ftab').trigger('click');
					$("#hdntabularoverlay").val('NO');
					$("#validatereportbasicdetails").validationEngine('validate');	
				});	
				jQuery("#validatereportbasicdetails").validationEngine({
					onSuccess: function() {
						var reporttypeid = $('#reporttypeid').find('option:selected').val();
						if(reporttypeid == 5 && $("#actiontype").val() == 'GR') {
							$('.htmlreportexportdiv').addClass('hidedisplay');
							$('#printhtmlreport').removeClass('hidedisplay');
							generatehtmlprintview();
						} else {
							if(reporttypeid == 5 && $("#actiontype").val() == 'SR') {
								savereporttodb();
							} else {
								$('#printhtmlreport').addClass('hidedisplay');
								$('.htmlreportexportdiv').removeClass('hidedisplay');
								var count = $('#reportmaincolumnname_to option').length; 
								if(count != '') {
									$("#processoverlay").show();
									$("#reportnameval").text($("#reportname").val());
									if($("#actiontype").val()=='SR') {
										savereporttodb();
									} else {
										var page = $(this).data('pagenum');
										var rowcount = $('.pagerowcount').data('rowcount');	
										generatereport('Report',page,rowcount);
										reportfiltercolumnfetch();
									}
								} else {
									$('.ftab2').trigger('click');
									$('#reportmaincolumnname_to').select2("focus");			
									$("#reportmaincolumnname_to").addClass('error');	
								}
							}
						}
					},
					onFailure: function() {
						var dropdownid =['4','reporttype','reportfolderid','reportmodule','reportrelatedmodule'];
						dropdownfailureerror(dropdownid);
					}
				});
			}
			//view edit submit validation
			$('#mainreportlisteditsubmit').click(function() {
				$("#validatereportbasiceditdetails").validationEngine('validate');
			});
			$("#validatereportbasiceditdetails").validationEngine({
				onSuccess: function() {
					dynamicreportgridedit();
				},
				onFailure: function() {
				var dropdownid =['1','viewcolname'];
					dropdownfailureerror(dropdownid);
				}
			});
		}
		$("#filtersearch").click(function() {
			generatereport();
		});
		{//view condition drop down change function-reportcondcolumn
			//hide process
			$("#reportddcondvaluedivhid").hide();
			$("#reportdatecondvaluedivhid").hide();
			$("#reportcondcolumn").change(function(){
				var datavb = $("#reportcondcolumn").find('option:selected').data('reportcondcolumnid');
				$("#reportcondcolumnid").val(datavb);
				$("#reportddcondvalue").select2('val','').trigger('change');			
				var data = $("#reportcondcolumn").find('option:selected').data('uitypeid');
				//Based on reportcondition dropdwon seletection, the condition will load from uitype table
				$("#reportcondition,#reportaggregate").empty();
				$("#reportcondition,#reportaggregate").select2('val','')
				$('#reportcondition').append($("<option></option>"));
				$('#reportaggregate').append($("<option value='ACTUAL'>Actual Values</option>"));
				var partentid=$(this).val();
				$.ajax({
					url:base_url+"Reportsview/loadconditionbyuitype?uitypeid="+data, 
					dataType:'json',
					async:false,
					success: function(data) { 
							$.each(data, function(index){					
								$('#reportcondition')
								.append($("<option></option>")
								.attr("value",data[index]['whereclausename'])
								.text(data[index]['whereclausename']));
							});	
					}
				});
				$.ajax({
					url:base_url+"Reportsview/loadaggregatemethodbyuitype?uitypeid="+data, 
					dataType:'json',
					async:false,
					success: function(data) {
						$.each(data, function(index) {				
							$('#reportaggregate')
							.append($("<option></option>")
							.attr("value",data[index]['aggregatemethodkey'])
							.attr("data-whereclauseid",data[index]['whereclauseid'])
							.text(data[index]['aggregatemethodname']));
						});	
						$('#reportaggregate').select2('val','ACTUAL');
					}
				});
				if(data == '2' || data == '3' || data == '4'|| data == '5' || data == '6'|| data == '7'|| data == '10' || data == '11' || data == '12'|| data == '14') {
					showhideintextbox('reportddcondvalue','reportcondvalue');
					$("#reportdatecondvaluedivhid,#startdatefieldcondvaluedivhid,#enddatefieldcondvaluedivhid").hide();
					$("#reportcondvalue").datepicker("disable");
					$("#reportcondvalue").val('');
					$("#reportcondvalue").addClass('validate[required,maxSize[100]]'); 
					$("#reportddcondvalue").removeClass('validate[required]');
					$("#reportdatecondvalue").removeClass('validate[required]');		
				} else if(data == '17' ) {		
					showhideintextbox('reportcondvalue','reportddcondvalue');
					$("#reportdatecondvaluedivhid,#startdatefieldcondvaluedivhid,#enddatefieldcondvaluedivhid").hide();
					$("#reportddcondvalue").select2('val',"");
					$("#reportddcondvalue").addClass('validate[required]');
					$("#reportdatecondvalue").removeClass('validate[required]');
					$("#reportcondvalue").removeClass('validate[required,maxSize[100]]');
					$("#reportcondvalue").datepicker("disable");
					var fieldname = $("#reportcondcolumn").find('option:selected').data('indexname');
					reportfieldnamebesdpicklistddvalue(fieldname,'reportddcondvalue',data);
				} else if(data == '18' || data == '19' || data == '21' || data == '22' || data == '23' || data == '25' || data == '26' || data == '27' || data == '28') {
					showhideintextbox('reportcondvalue','reportddcondvalue');
					$("#reportdatecondvaluedivhid,#startdatefieldcondvaluedivhid,#enddatefieldcondvaluedivhid").hide();
					$("#reportddcondvalue").addClass('validate[required]');
					$("#reportddcondvalue").select2('val',"");
					$("#reportdatecondvalue").removeClass('validate[required]');
					$("#reportcondvalue").removeClass('validate[required,maxSize[100]]');
					$("#reportcondvalue").datepicker("disable");
					var fieldname = $("#reportcondcolumn").find('option:selected').data('indexname');
					fieldnamebesdddvalue(fieldname,'reportddcondvalue'); 
				} else if(data == '13') {
					$("#reportcondvalue").datepicker("disable");
					showhideintextbox('reportcondvalue','reportddcondvalue');
					$("#reportdatecondvalue").removeClass('validate[required]');
					$("#reportddcondvalue").select2('val',"");
					$("#reportddcondvalue").addClass('validate[required]');
					$("#reportcondvalue").removeClass('validate[required,maxSize[100]]');
					checkboxvalueget();
				} else if(data == '20') {
					showhideintextbox('reportcondvalue','reportddcondvalue');
					$("#reportdatecondvaluedivhid,#startdatefieldcondvaluedivhid,#enddatefieldcondvaluedivhid").hide();
					$("#reportddcondvalue").addClass('validate[required]');
					$("#reportddcondvalue").select2('val',"");
					$("#reportdatecondvalue").removeClass('validate[required]');
					$("#reportcondvalue").removeClass('validate[required,maxSize[100]]');
					$("#reportcondvalue").datepicker("disable");
					var fieldname = $("#reportcondcolumn").find('option:selected').data('indexname');
					empgroupdrodownset('reportddcondvalue');
					//reportcondvalue
				} else if(data == '8' || data == '31' || data == '32') {
					//for hide
					$("#reportddcondvaluedivhid").hide();
					$("#reportcondvaluedivhid").hide();
					$("#reportdatecondvaluedivhid").hide();
					$('#reportdatecondvalue,#reportcondvalue').val('');
					//$("#reportcondvalue").datepicker("disable");
					$("#reportdatecondvalue").removeClass('validate[required]');
					$("#reportddcondvalue").removeClass('validate[required]');
					$("#reportcondvalue").removeClass('validate[required,maxSize[100]]');
				} else {
					alertpopup('Please choose another field name');
				}
			});
		}
		$("#reportcondition").change(function() {
			if($(this).val() == 'Custom') {
				$('#report_startdate,#report_enddate').removeAttr('disabled');
				$('#startdatefieldcondvaluedivhid,#enddatefieldcondvaluedivhid').show();
				$('#report_startdate,#report_enddate').addClass('validate[required]');
				$('#reportdatecondvalue').val($('#report_startdate').val()+'|'+$('#report_enddate').val());
				$("#finalreportcondvalueid").val($('#reportdatecondvalue').val());
				$("#finalreportcondvalue").val($('#reportdatecondvalue').val());
			} else {
				$('#startdatefieldcondvaluedivhid,#enddatefieldcondvaluedivhid').hide();
				$("#report_startdate,#report_enddate").removeClass('validate[required]');
			}
		});
		$("#report_startdate").change(function(){
			$('#reportdatecondvalue').val($('#report_startdate').val()+'|'+$('#report_enddate').val());
			$("#finalreportcondvalueid").val($('#reportdatecondvalue').val());
			$("#finalreportcondvalue").val($('#reportdatecondvalue').val());
		});
		$("#report_enddate").change(function(){
			$('#reportdatecondvalue').val($('#report_startdate').val()+'|'+$('#report_enddate').val());
			$("#finalreportcondvalueid").val($('#reportdatecondvalue').val());
			$("#finalreportcondvalue").val($('#reportdatecondvalue').val());
		});
		/* commented by vishal -  since filter condition must be laaded based on only uitype. this function was developed earlier - only usecase was for count, so remvoed it
		$("#reportaggregate").change(function(){
			if($(this).val()!='ACTUAL') { 
				$("#reportddcondvaluedivhid").hide();
				$("#reportdatecondvaluedivhid").hide();
				$("#reportddcondvalue").removeClass('validate[required]');
				$("#reportdatecondvalue").removeClass('validate[required]');
				$("#reportcondvalue").addClass('validate[required]');
				$("#reportcondvaluedivhid").show();			
				$("#reportcondition").empty();
				$("#reportcondition").select2('val','')
				$('#reportcondition').append($("<option></option>"));
				var whereclauseid=$("#reportaggregate option[value="+$(this).val()+"]").attr('data-whereclauseid');
				$.each(whereclauseid.split(","), function(i,e){								
					$('#reportcondition')
					.append($("<option></option>")
					.attr("value",e)
					.text(e));
				});		
			} else { 
				$("#reportcondcolumn").trigger('change');
			}
		}); */
	/* Reports Creation Overlay end */
	}
	{// For Height
		var stnewheightaddform = $( window ).height();
		var fixedheightaddform = stnewheightaddform -60;
		$(".studiocontainer").css('height',''+fixedheightaddform+'px');
	}
	{//Report View PopUp
		var addcloseinfo = ["closeaddform", "reportsformdiv", "reportviewoverlay"]
		addclose(addcloseinfo);
		$("#closeaddform").click(function() {
			reportsgrid(); //Reload the grid
		});
		//Reload
		$("#reportdetailreload").click(function(){
			customreportgrid($("#reportcreationid").val());		
		});
		//export history//excel & CSV
		filetype = 'xlsx';
		$('#reportdetailedexcel,#reportdetailedcsv').click(function() {
			$("#exportovelay").fadeIn();
			var method=$(this).attr("id");
			if(method == 'reportdetailedcsv') {
				filetype = 'csv';
			} else {
				filetype = 'xlsx';
			}
		});
		$('#exportallyes').click(function() {
			var exportid=$(this).attr("id");
			if(exportid == 'exportallno') {
				generatereport(filetype,'','','No');
			} else {
				generatereport(filetype,'','','Yes');
			}
			$("#exportovelay").fadeOut();
		});
		$('#exportallno').click(function() {
			$("#exportovelay").fadeOut();
		});
		//export history//excel & CSV	
		$('#pdf').click(function(){
			$("#exportovelay").fadeIn();
			filetype = 'pdf';
		});
	}
	{	// Report Type Change
		$("#reporttypeid").change(function() {
			var value = $(this).val();
			if(value == 5) {
				$("#tab2,#columnviewlists").addClass('hidedisplay');
				$("#tab3").removeClass('hidedisplay');
				// Clear Menu category and main module
				$('#reportcategory,#reportmodule').select2('val','').trigger('change');
				// Empty column fields in grid.
				$("#reportmaincolumnname,#reportmaincolumnname_to,#reportgroupby1,#reportgroupby1_to").empty();
				$('#reportgroupby1').empty();
				$('#reportgroupby1').select2('val','').trigger('change');
				$("#reportmaincolumnname").empty();
				$("#reportcalculationon").empty();
				$("#report_datefield").empty();
				$("#report_datefield").append("<option></option>");
				$('#reportmaincolumnname_to').removeClass('validate[required]');
				// HTML Type based trigger change
				$('#htmlfieldmodeid').select2('val',1).trigger('change');
				$('#htmlaccountid').select2('val',1).trigger('change');
			} else {
				$("#tab3").addClass('hidedisplay');
				$("#tab2,#columnviewlists").removeClass('hidedisplay');
				$('#printtemplateid').val(0);
				$('#htmlfieldmodeid').select2('val','').trigger('change');
				$('#htmlaccountid').select2('val',1).trigger('change');
				$('#reportmaincolumnname_to').addClass('validate[required]');
			}
		});
		// HTML Report Mode - Date Condition retrieve values
		$("#htmlfieldmodeid").change(function() {
			var value = $(this).val();
			var uitypevalue = $(this).find('option:selected').data('uitypeid');
			$("#htmlfieldcondition").empty();
			if(value) {
				$.ajax({
					url:base_url+"Reportsview/loadconditionbyuitype?uitypeid="+uitypevalue, 
					dataType:'json',
					async:false,
					success: function(data) {
						$.each(data, function(index){					
							$('#htmlfieldcondition')
							.append($("<option></option>")
							.attr("value",data[index]['whereclausename'])
							.text(data[index]['whereclausename']));
						});
					}
				});
			}
		});
		// HTML Date Field based other fields showing
		$("#htmlfieldcondition").change(function() {
			$('#htmlreportfromdatediv,#htmlreporttodatediv').addClass('hidedisplay');
			$('#htmlreportfromdate,#htmlreporttodate').val('');
			var value = $(this).val();
			if(value) {
				if(value == "Custom") {
					$('#htmlreportfromdatediv,#htmlreporttodatediv').removeClass('hidedisplay');
				} else {
					$('#htmlreportfromdatediv,#htmlreporttodatediv').addClass('hidedisplay');
				}
			}
		});
		// HTML view print data
		$("#printhtmlreport").click(function() {
			var reporttypeid = $('#reporttypeid').find('option:selected').val();
			if(reporttypeid == 5) {
				$('#htmlprintingdiv').printThis();
			} else {
				alertpopup('This option is only for HTML View data!');
			}
		});	
	}
	{ // HTML Report Date Field Change
		$('#htmlreportfromdate').datetimepicker("destroy");
		$('#htmlreporttodate').datetimepicker("destroy");
		var dateformetdata = $('#htmlreportfromdate').attr('data-dateformater');
		var startDateTextBox = $('#htmlreportfromdate');
		var endDateTextBox = $('#htmlreporttodate');
		startDateTextBox.datetimepicker({
			dateFormat: dateformetdata,
			showTimepicker :false,
			minDate: null,
			changeMonth: true,
			changeYear: true,
			maxDate:0,
			yearRange : "1947:c+100",
			onClose: function(dateText, inst) {
				if (endDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						endDateTextBox.datetimepicker('setDate', testStartDate);
				}
				else {
					endDateTextBox.val(dateText);
				} 
				$('#htmlreportfromdate').focus();
			},
			onSelect: function (selectedDateTime){
				endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
					$(this).prev('div').remove();
				} 
			}
		}).click(function() {
			$(this).datetimepicker('show');
		});
		endDateTextBox.datetimepicker({
			dateFormat: dateformetdata,
			showTimepicker :false,
			minDate: null,
			changeMonth: true,
			changeYear: true,
			maxDate:0,
			yearRange : "1947:c+100",
			onClose: function(dateText, inst) {
				if (startDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
					startDateTextBox.datetimepicker('setDate', testEndDate);
				}
				else {
					startDateTextBox.val(dateText);
				}
				$('#htmlreporttodate').focus();
			},
			onSelect: function (selectedDateTime) {
				startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
			}
		}).click(function() {
			$(this).datetimepicker('show');
		});
	}
	{	// Summary View OR Detailed View
		$("#reporttv").click(function(){
			var doublesummarycheck=0;
			$('#reportmaincolumnname_to option').each(function(i, selected) {
				
				if ($(this).attr('data-selecttype').indexOf(',') > 0) {
					doublesummarycheck=1;
				}
			});
			if(doublesummarycheck == 0)
			{
				$("#reportfilterid").val('');
				$("#reportddfilterid").val('');
				$("#reportconditionname").val('');
				$("#reportddconditionname").val('');
				$("#reportfiltervalue").val('');
				$("#reportddfiltervalue").val('');
				$("#reporttypeid").val('2');
				$("#reporttype").val('TV');
				$("#reporttypename").text('  Tabular View');
				$("#generatereport").trigger("click");
			}
			else{
				alertpopup("Please Assign only one summary for columns, to view tabular view report");
			}
		});
		$("#reportsv").click(function() {
			var summarycheck=0;
			$('#reportmaincolumnname_to option').each(function(i, selected) {
				if ($(this).attr('data-selecttype') != '' && $(this).val()!='') {
					summarycheck=1;
				}
			});
			if(summarycheck == '1') {
				$("#reportfilterid").val('');
				$("#reportddfilterid").val('');
				$("#reportconditionname").val('');
				$("#reportddconditionname").val('');
				$("#reportfiltervalue").val('');
				$("#reportddfiltervalue").val('');
				$("#reporttypeid").val('3');
				$("#reporttype").val('SV');
				$("#reporttypename").text('  Summary View');
				$("#generatereport").trigger("click");
			} else {
				alertpopup("To see summary view,please add atleast one summary view column.");
			}
		});
		$("#reporthv").click(function(){
			filetype = 'html';
			generatereport(filetype,'','','No');				
		});
	}
	$('#recurrencedetaildiv').hide();
	{//Search Report list-report
		$('#reportssearch').bind("change keyup", function() {
            searchWord = $(this).val();
            if (searchWord.length >= 1) {
				$(".reportsfolderlist").hide();
				$("#appendreportvalue").show();
				$("#appendreportvalue").html('');
                $('ul.search-list-report li').each(function() {
                    text = $(this).text();
                    if (text.match(RegExp(searchWord, 'i'))) {
                        var alllinks = $(this).html();
						$("#appendreportvalue").append(alllinks);
                    }
                });
				$('#appendreportvalue label').click(function(){
					var id = $(this).attr('data-parentreportfolderid');
					globalid=id;
					reportsgrid();
				});	
            } else if (searchWord.length <= 1) { 
				$(".reportsfolderlist").show();
				$("#appendreportvalue").hide();
                $('ul.search-list-report li').each(function(){
                    text = $(this).text();
                    if (text.match(RegExp(searchWord, 'i'))) {
                        var alllinks = $(this).html();
						$("#appendreportvalue").html('');
                    }
                });
            }
        });
	}
	{// Grid Calling Function
		reportsgrid();
		folderoperationgrid();
	}
	$( window ).resize(function() {
		maingridresizeheightset('reportsgrid');
	});
	{// Tool Bar click function 		
		//Add
		$("#addicon").click(function(){
			$("#reporttype").val('TV');
			$("#reportviewoverlay").fadeIn();
			$("#newoverlayform").show(); 
			$("#hdnreportid").val('');
			$("#closeaddform").addClass('hidedisplay');
			$("#reportmaincolumnname,#reportmaincolumnname_to,#reportgroupby1,#reportgroupby1_to,#groupbyrange").empty();
			$("#reportmaincolumnname,#reportmaincolumnname_to,#reportgroupby1,#reportgroupby1_to,#reportcalculationon,#reportcondcolumn,#report_datefield,#report_range,#groupbyrange").select2('val','');
			$("#reportname,#reportfolderid,#reportcategory,#reportmodule,#reportrelatedmodule,#reportdescription").select2('val','');
			$('#reportbasicdetails')[0].reset();
			
			clearform('reportbasicdetails');
			cleargriddata('customreportgrid');
			$("#overlaystatus").val('');
			$('#reporttypeid').val('2').trigger('change');
		});
		//reload
		$("#reloadicon").click(function(){
			cleargriddata('reportsgrid');
			globalid='all';
			reportsgrid();
		});
		$("#editicon").click(function(){			
			var ids = $('#reportsgrid div.gridcontent div.active').attr('id');
			if(ids)
			{
				$("#processoverlay").show();
				var reportname = $('#reportsgrid div.gridcontent div.active ul').find('li:first').text();
				$("#reportnameval").text(reportname);
				$("#reporttypename").text('  Tabular View');
				$("#reporttype").val('TV');
				newretrivereport(ids);	
				$("#reportviewoverlay").fadeIn();	
				$("#generatereport").trigger("click");
				$("#newoverlay").trigger("click");
				$("#overlaystatus").val('');
			}
			else
			{	alertpopup("Please select a row");
			}			
		});
		$("#deleteicon").click(function(){
			var datarowid = $('#reportsgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				$("#reportdeleteoverlay").fadeIn();
				$("#reportdeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		}); 
		$("#reportdeleteyes").click(function(){
			$("#reportdeleteoverlay").fadeOut();
			var id='all';
			var datarowid = $('#reportsgrid div.gridcontent div.active').attr('id');
			$.ajax({
				url: base_url + "Reportsview/reportdelete?primarydataid="+datarowid,
				cache:false,
				success: function(msg) {
					if(msg == true) {
						alertpopup("Report Deleted successfully !!!");
						reportsgrid();
					}else if(msg == "sndefault") {
						alertpopup('Cannot Delete default records');
						reportsgrid();
						$("#processoverlay").hide();
					}
				},
			});
		});
		$('#editreportfolderbtn').hide();
		$('#addreportfolderbtn').show();
		//editicon
		$("#reportfolderediticon").click(function(){
			var reportfolderid = $('#folderoperationgrid div.gridcontent div.active').attr('id');
			if (reportfolderid) {
				$('#editprimarydataid').val(reportfolderid);
				$("#reportfoldersetpublic").prop( "checked", false );
				$("#reportfoldersetdefault").prop( "checked", false );
				$('#addreportfolderbtn').hide();
				$('#editreportfolderbtn').show();
				$('#reportfolderrefreshicon').hide();
				$.ajax({
					url:base_url+"Reportsview/getreportdetails?reportid="+reportfolderid,
					contentType:'application/json; charset=utf-8',
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						$('#reportsfoldername').val(data.reportfoldername);
						$('#reportfolderdescription').val(data.description);
						if(data.setaspublic === 'Yes'){
							$( "#reportfoldersetpublic" ).prop( "checked", true );
							$('#reportfoldersetpublic').val('Yes');
						}
						else{
							$( "#reportfoldersetpublic" ).prop( "checked", false );
							$('#reportfoldersetpublic').val('No');
						}				
						if(data.setdefault === 'Yes'){	
							$( "#reportfoldersetdefault" ).prop( "checked", true );
							$('#reportfoldersetdefault').val('Yes');
						}
						else{
							$( "#reportfoldersetdefault" ).prop( "checked", false );
							$('#reportfoldersetdefault').val('No');
						}	
					},
				});
			} else {
				alertpopup("Please select a row");
			}
		});
		//deleteicon
		$("#reportfolderdeleteicon").click(function(){
			var datarowid = $('#folderoperationgrid div.gridcontent div.active').attr('id');
			if(datarowid){		
				deletefolderddvalfetch(datarowid);
				$.ajax({
					url:base_url+'Reportsview/checkfolderhavereports?reportfolderid='+datarowid,
					success: function(result) { 
					if(result > 0){
					$("#folderdeleteformoverlay").fadeIn();
					$("#basedeleteyes1").focus();
					$("#delroleid").val(datarowid);			
					}else{
						$("#basedeleteoverlay").fadeIn();
						$("#basedeleteyes").focus();
					}
					},
					});		
			}
			else
			{	alertpopup("Please select a row");
			}	
		});
		
		$("#basedeleteyes").click(function(){
			$("#basedeleteoverlay").fadeOut();
			var datarowid = $('#folderoperationgrid div.gridcontent div.active').attr('id');
			$.ajax({
				url: base_url + "Reportsview/deletemptyreportfolder?reportfolderid="+datarowid,
				cache:false,
				success: function(msg) {
					if(msg == true){
						$('#reportfolderrefreshicon,#reloadicon').trigger('click');
						loadreportfoldername();
						alertpopup("Folder Deleted successfully !!!");		
					} else {			
					}
				},
			});
		});
		
		$("#basedeleteyes1").click(function(){ 
		$("#reportfolderdeletevalidation").validationEngine('validate');
		});
		jQuery("#reportfolderdeletevalidation").validationEngine({
		onSuccess: function() {
			var updatefolderid = $('#conformfolderid').val();
			var delfolderid = $('#delfolderid').val();
			folderdeletefunction(delfolderid,updatefolderid);
		},
		onFailure: function() { 
			var dropdownid =['1','conformfolderid'];
			dropdownfailureerror(dropdownid);
		}
	});
		//refreshicon 
		$("#reportfolderrefreshicon").click(function(){
			resetFields();
			refreshfoldergrid();
			$( "#reportfoldersetpublic" ).prop( "checked", false );
			$( "#reportfoldersetdefault" ).prop( "checked", false );
			$('#editreportfolderbtn').hide();
			$('#addreportfolderbtn').show();
		});
	}
	{//refresh grid
		function refreshfoldergrid() {
			var page = $(this).data('pagenum');
			var rowcount = $('ul#reportlistpgnumcnt li .page-text .active').data('rowcount');
			folderoperationgrid(page,rowcount);
		}
	}
	{//Close Report
		$("#reportcloseaddform").click(function(){
			window.location =base_url+'Reports';
		});
	}
	{// Full Screen View
		$('#exitfulscrnreport').hide();
		$("#fulscrnreport").click(function(){
			$('#fulscrnreport').hide();
			$('#exitfulscrnreport').show();
			$("#reportarea").removeClass('large-9').removeClass('medium-9').addClass('large-12');
			$("#leftpanearea").removeClass('large-3').addClass('hidedisplay');
			reportsgrid();
		});
		$("#exitfulscrnreport").click(function(){
			$('#exitfulscrnreport').hide();
			$('#fulscrnreport').show();
			$("#reportarea").removeClass('large-12').addClass('large-9').addClass('medium-9');
			$("#leftpanearea").removeClass('hidedisplay').addClass('large-3');
			reportsgrid();
		});
	}
	{//Folder CRUD Overlay
		$("#foldericon").click(function(){
			addslideup('reportsview','folderoverlaydiv');
			$('#editreportfolderbtn').hide();
			$('#addreportfolderbtn').show();
			$('#reportfolderrefreshicon').show();
			resetFields();
			folderoperationgrid();
		});
		$("#closefoldercrudoverlay").click(function(){
			addslideup('folderoverlaydiv','reportsview');
		});
		//create reportfolder
		$('#addreportfolderbtn').click(function(e){
			$("#addreportfoldervalidate").validationEngine('validate');
		});
		jQuery("#addreportfoldervalidate").validationEngine({
			onSuccess: function() {
				var formdata = $("#reportfolderform").serialize();
				var amp = '&';
				var datainformation = amp + formdata;
				$.ajax({
					url: base_url + "Reportsview/createreportfolder",
					data: "datas=" + datainformation,
					type: "POST",
					cache:false,
					success: function(msg) {
						if(msg == true) {
							resetFields();
							refreshfoldergrid();							
							$( "#reportfoldersetpublic" ).prop( "checked", false );
							$( "#reportfoldersetdefault" ).prop( "checked", false );
							loadreportfoldername();
							alertpopup(savealert);
						} else {				
						}
					},
				});
			},
			onFailure: function() {	
			}
		});
		//edit reportfolder
		$('#editreportfolderbtn').click(function(e){
			$("#editreportfoldervalidate").validationEngine('validate');			
		});
		jQuery("#editreportfoldervalidate").validationEngine({
			onSuccess: function() {
				//chkbox settings
				$('#reportfolderrefreshicon').show();
				if ($('#reportfoldersetpublic').is(':checked')) {
					$('#reportfoldersetpublic').val('Yes');
				} else {
					$('#reportfoldersetpublic').val('No');
				}
				if ($('#reportfoldersetdefault').is(':checked')) {
					$('#reportfoldersetdefault').val('Yes');
				} else {
					$('#reportfoldersetdefault').val('No');
				}
				var reportfolderid=$('#editprimarydataid').val();
				var formdata = $("#reportfolderform").serialize();
				var amp = '&';
				var datainformation = amp + formdata;
				$.ajax({
					url: base_url + "Reportsview/updatereportfolder",
					data: "datas=" + datainformation+"&reportfolderid="+reportfolderid,
					type: "POST",
					cache:false,
					success: function(msg) {
						if(msg == true){
							resetFields();
							refreshfoldergrid();
							$( "#reportfoldersetpublic" ).prop( "checked", false );
							$( "#reportfoldersetdefault" ).prop( "checked", false );
							loadreportfoldername();
							alertpopup(updatealert);
						} else {	
						}
					},
				});
				$('#editreportfolderbtn').hide();
				$('#addreportfolderbtn').show();	
			},
			onFailure: function() {
			}
		});
	}
	{// Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
			if(name == 'schedulereport') {
				var recurr=$('#'+name+'').val();
				if (recurr == 'Yes') {
					$('#recurrencedetaildiv').show();
				} else {
					$('#recurrencedetaildiv').hide();
				}
			}
		});
	}	
	{//load folder-names
		loadreportfoldername();
	}
	{//Check box Filter
		$(".checkboxfilter").click(function(){
			if($(this).attr('checked')) {
				var labelname = $(this).data('labelname'); //For Label Name
				var currentvalue = $(this).val(); // For Value
				$('#filtercontainer').append('<li><span>'+labelname+' : '+currentvalue+' </span><span class="fa fa-times filterliststyle" id="" title="Remove"> </span></li>');
				// Remove Filter
				$(".filterliststyle").click(function(){
					$(this).parent('li').remove();
				});
			} else {
			}
		});
	}
	{//Drop Down Filter
		$(".dropdownfilter").change(function(){
			var labelname = $(this).data('labelname'); //For Label Name
			var currentvalue = $(this).val(); // For Value
			var parenidvalue = $(this).attr('id'); // For Value 
			$('#filtercontainer').append('<li class="dd" data-parentname="'+parenidvalue+'"><span>'+labelname+' : '+currentvalue+' </span><span class="fa fa-times filterliststyle" id="" title="Remove"> </span></li>');
			// Remove Filter
			$(".filterliststyle").click(function(){
				$(this).parent('li').remove();
			});
		});
	}
	//load report list based on folder-names
	$('#mainreportfolder label').click(function() {
		var id = $(this).attr('data-parentreportfolderid');
		globalid=id;
		reportsgrid();
	});
	{//report clone
		$('#reportcloneicon').click(function(){
			var viewmoduleid = $('#reportsgrid div.gridcontent div.active').attr('id');
			if(viewmoduleid){
				$.ajax({
					url:base_url+"Reportsview/reportdataclone",
					data: "datas=&reportid="+viewmoduleid,
					type: "POST",
					async:false,
					cache:false,
					success: function(msg) {
						if(msg == 'TRUE') {
							alertpopup('Record has been clone !');
							//reload -dynamicdddataview-after clone
							reportsgrid();
						}
					}
				});
			} else {
				alertpopup('Please select a row');
			}
		});
	}
	$("#detailedviewicon").click(function(){
		
		var ids = $('#reportsgrid div.gridcontent div.active').attr('id');
		if(ids)
		{
			$('#reportsgrid div.gridcontent div.active').trigger("dblclick");	
		}
		else
		{	alertpopup("Please select a row");
		} 
	});
	/* load report list based on folder-names */
	$('#mainviewreportfolder').change(function(){
		var id = $(this).val();
		globalid=id;
		reportsgrid();
	});
	reportsviewfiltername = [];
	$("#productaddonfilteraddcondsubbtn").click(function() {
		$("#reportsviewfilterformconditionvalidation").validationEngine("validate");	
	});
	$("#reportsviewfilterformconditionvalidation").validationEngine({
		onSuccess: function() {
		dashboardmodulefilter(reportsgrid,'reportsview');
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	// date filter data clear
	$("#standardreportconddeleteicon").click(function() {
			$('#report_datefield').select2('val','');
			$('#report_range').select2('val',0);
			$('#report_startdate,#report_enddate').val('00-00-0000');
	});
	// Image Preview
	$(document).on( "click", "#salesrpttagimagepreview", function() {
		var datarowid = $(this).closest('div').attr('id');
		var salesdetailid = $('#customreportgriddiv .gridcontent div#'+datarowid+' ul a .salesdetailid-class').text();
		salesdetailid = typeof salesdetailid == 'undefined' ? '1' : salesdetailid;
		var salesid = $('#customreportgriddiv .gridcontent div#'+datarowid+' ul a .salesid-class').text();
		salesid = typeof salesid == 'undefined' ? '1' : salesid;
		if(salesid != '1' || salesdetailid != '1') {
			$.ajax({
				url:base_url+"Reportsview/retrievesalesdetailimage",
				data: "salesdetailid="+salesdetailid+"&salesid="+salesid,
				type: "POST",
				async:false,
				cache:false,
				success: function(msg) {
					var totalimages = JSON.parse(msg);
					if (totalimages == 'FAILED') {
						alertpopupdouble('No Image to preview');
					} else {
						$('#salesdetailimageoverlaypreview').fadeIn();
						$('#salesdetailtagimagepreview').empty();
						var imagepreview = new Array();
						for (var i = 0; i < totalimages.length; i++) {
							imagepreview = totalimages[i].split(',');
							for (var j = 0; j < imagepreview.length; j++) {
								var img = $('<img id="companylogodynamic" style="height:100%;width:100%;">');
								img.attr('src', base_url+imagepreview[j]);
								img.appendTo('#salesdetailtagimagepreview');
							}
						}
					}
				}
			});
		} else {
			alertpopupdouble('No Image to preview');
		}
	});
	// Image Preview
	$(document).on( "click", "#stockrpttagimagepreview", function() {
		var datarowid = $(this).closest('div').attr('id');
		var stocktagid = $('#customreportgriddiv .gridcontent div#'+datarowid+' ul a .itemtagid-class').text();
		stocktagid = typeof stocktagid == 'undefined' ? '1' : stocktagid;
		var typename = 'itemtag';
		if(stocktagid == '') {
			stocktagid = $('#customreportgriddiv .gridcontent div#'+datarowid+' ul a .salesdetailid-class').text();
			stocktagid = typeof stocktagid == 'undefined' ? '1' : stocktagid;
			if(stocktagid == '') {
				stocktagid = 1;
			}
			typename = 'salesdetailid';
		}
		if(stocktagid != '1') {
			$.ajax({
				url:base_url+"Reportsview/retrievestocktagimage",
				data: "stocktagid="+stocktagid+"&typename="+typename,
				type: "POST",
				async:false,
				cache:false,
				success: function(msg) {
					var totalimages = JSON.parse(msg);
					if (totalimages == 'FAILED') {
						alertpopupdouble('No Image to preview');
					} else {
						$('#salesdetailimageoverlaypreview').fadeIn();
						$('#salesdetailtagimagepreview').empty();
						var imagepreview = new Array();
						for (var i = 0; i < totalimages.length; i++) {
							imagepreview = totalimages[i].split(',');
							for (var j = 0; j < imagepreview.length; j++) {
								var img = $('<img id="companylogodynamic" style="height:100%;width:100%;">');
								img.attr('src', base_url+imagepreview[j]);
								img.appendTo('#salesdetailtagimagepreview');
							}
						}
					}
				}
			});
		} else {
			alertpopupdouble('No Image to preview');
		}
	});
	//Image preview close icon
	$(document).on("click","#salesdetailimagepreviewclose",function() {
		$('#salesdetailtagimagepreview').empty();
		$('#salesdetailimageoverlaypreview').fadeOut();
	});
	$(document).on( "click", "#noimagetopreview", function() {
		alertpopupdouble('No Image to preview');
	});	
});
{// Reports View Grid
	function reportsgrid(page,rowcount) {
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = $(window).width();
		var wheight = $(window).height();
		/*col sort*/
		var sortcol = $('.reportviewheadercolsort').hasClass('datasort') ? $('.reportviewheadercolsort.datasort').data('sortcolname') : '';
		var sortord =  $('.reportviewheadercolsort').hasClass('datasort') ? $('.reportviewheadercolsort.datasort').data('sortorder') : '';
		var headcolid = $('.reportviewheadercolsort').hasClass('datasort') ? $('.reportviewheadercolsort.datasort').attr('id') : '0';
		var mainviewreportfolder = $("#mainviewreportfolder").val();
		$.ajax({
			url:base_url+"Reportsview/viewreportlist?reportfolderid="+globalid+"&moduleid=35&maintabinfo=report&primaryid=reportid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+"&sortcol="+sortcol+"&sortord="+sortord+"&reportfolder="+mainviewreportfolder,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#reportsgrid').empty();
				$('#reportsgrid').append(data.content);
				$('#reportsgridfooter').empty();
				$('#reportsgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('reportsgrid');
				datarowheadergroup('reportsgrid');
				{//sorting
					$('.reportviewheadercolsort').click(function(){
						$('.reportviewheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						var sortpos = $('#reportsgrid .gridcontent').scrollLeft();
						reportsgrid(page,rowcount);
						$('#reportsgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					});
					sortordertypereset('reportviewheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						reportsgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#reportviewpgrowcount').change(function(){
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page =1;
						reportsgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				/// *Kumaresan* commented this code bcz double click should not trigger.
				/* $('div.gridcontent div.data-rows').dblclick(function(){
					$("#processoverlay").show();
					var ids = $('#reportsgrid div.gridcontent div.active').attr('id');
					if(ids) {
						//var livefilter = getgridcolvalue('reportsgrid',ids,'livefilter','');
						/* if(livefilter == 'Yes') {
							var reportname = $('#reportsgrid div.gridcontent div.active ul').find('li:first').text();
							$("#reportnameval").text(reportname);
							$("#reporttypename").text('  Tabular View');
							$("#reporttype").val('TV');
							$("#closeaddform").removeClass('hidedisplay');
							newretrivereport(ids);	
							$("#reportviewoverlay").fadeIn();
							$("#reportnameval").text($("#reportname").val());
							$('#customreportgrid .grid-view').empty();
							$('#customreportgriddivfooter').empty();
							reportfiltercolumnfetch();
						} else { */
							/* var reportname = $('#reportsgrid div.gridcontent div.active ul').find('li:first').text();
							$("#reportnameval").text(reportname);
							$("#reporttypename").text('  Tabular View');
							$("#reporttype").val('TV');
							$("#closeaddform").removeClass('hidedisplay');
							newretrivereport(ids);	
							$("#reportviewoverlay").fadeIn();
							$("#newoverlay").trigger("click");
							$("#overlaystatus").val('');
							$("#tab2").trigger('click');
						//}
						setTimeout(function(){
							$("#generatereport").trigger("click");
						},200);
						
					} else {
						alertpopup("Please select a row");
					}
					$("#processoverlay").hide();
				}); */
				//Material select
				$('#reportviewpgrowcount').material_select();
			},
		});
	}	
}
{// Reports Folder Operation Grid Overlay
	function folderoperationgrid(page,rowcount) 
	{
		/*pagination*/
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = $('#folderoperationgrid').width();
		var wheight = $('#folderoperationgrid').height();
		/* col sort */
		var sortcol = $('.reportlistheadercolsort').hasClass('datasort') ? $('.reportlistheadercolsort.datasort').data('sortcolname') : '';
		var sortord =  $('.reportlistheadercolsort').hasClass('datasort') ? $('.reportlistheadercolsort.datasort').data('sortorder') : '';
		var headcolid = $('.reportlistheadercolsort').hasClass('datasort') ? $('.reportlistheadercolsort.datasort').attr('id') : '0';
		$.ajax({
			url:base_url+"Reportsview/viewreportfolder?maintabinfo=reportfolder&primaryid=reportfolderid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=35',
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#folderoperationgrid').empty();
				$('#folderoperationgrid').append(data.content);
				$('#folderoperationgridfooter').empty();
				$('#folderoperationgridfooter').append(data.footer);
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('folderoperationgrid');
				{/* sorting */
					$('.reportlistheadercolsort').click(function(){
						$('.reportlistheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $('ul#reportlistpgnum li.active').data('pagenum');
						var rowcount = $('ul#reportlistpgnumcnt li .page-text .active').data('rowcount');
						var sortpos = $('#folderoperationgrid .gridcontent').scrollLeft();
						folderoperationgrid(page,rowcount);
						$('#folderoperationgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					});
					sortordertypereset('reportlistheadercolsort',headcolid,sortord);
				}
				{/* pagination */
					$('.pagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#reportlistpgnumcnt li .page-text .active').data('rowcount');
						folderoperationgrid(page,rowcount);
					});
					$('.pagerowcount').click(function(){
						var page = $('ul#reportlistpgnum li.active').data('pagenum');
						var rowcount = $(this).data('rowcount');
						folderoperationgrid(page,rowcount);
					});
					$('.pvpagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#reportlistpgnumcnt li .page-text .active').data('rowcount');
						folderoperationgrid(page,rowcount);
					});
					$('#reportlistpgrowcount').change(function(){
						var rowcount = $(this).val();
						var page = $('ul#reportlistpgnum li.active').data('pagenum');
						folderoperationgrid(page,rowcount);
					});
				}
				{//Grid data row sorting
					var sortable = document.getElementById("reportlist-sort");
					Sortable.create(sortable,{
						group: 'reportlistelement',
						onEnd: function (evt) {
							sortorderupdate('folderoperationgrid','module','moduleid','sortorder');
					    },
					});
				}
			},
		});
	}
}
function sortorderupdate(gridname,tablename,tableid,sortfieldname) {
	var datarowid = getgridallrowids(gridname);
	$.ajax({
		url:base_url + "Reportsview/datarowsorting",
		data:"datas="+"&rowids="+datarowid+'&tablename='+tablename+'&primaryid='+tableid+'&sortfield='+sortfieldname,
		type:"POST",
		aync:false,
		cache:false,
		success: function(msg) {
			loadreportfoldername();
		},
	});
}
function loadreportfoldername() {
	$("#reportfolderid,#mainviewreportfolder").empty();
	$.ajax({
		url:base_url+"Reportsview/loadreportfoldername", 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			$('#reportfolderid').append($("<option></option>"));
			$('#mainviewreportfolder').append($("<option value='all'>All Report</option>"));
			$.each(data, function(index) {
				$('#reportfolderid,#mainviewreportfolder')
				.append($("<option></option>")
				.attr("value",data[index]['reportfolderid'])
				.text(data[index]['reportfoldername']));
			});
		}
	});	
}
	//grouping grid
	function customreportgrid(id,page,rowcount) {	
		var reporttype=$("#reporttype").val();
		$('#customreportgriddiv').empty();
		if(Operation == 1){
			var rowcount = $('#customreportpgrowcount').val();
			var page = $('.paging').data('pagenum');
		} else{
			var rowcount = $('#customreportpgrowcount').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? 10 : rowcount;
		}
		var wwidth = $("#customreportgrid").width();
		var wheight = $(window).height();
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.customheadercolsort').hasClass('datasort') ? $('.customheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord){
			sortord = sortord;
		} else{
			var sortord =  $('.customheadercolsort').hasClass('datasort') ? $('.customheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.customheadercolsort').hasClass('datasort') ? $('.customheadercolsort.datasort').attr('id') : '0';
		var userviewid=id;
		$("#reportcreationid").val(userviewid);
		$.ajax({
			url:base_url+"Reportsview/reportgridinformationfetch?reportid="+userviewid+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&reporttype='+reporttype,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#customreportgriddiv').empty();
				$('#customreportgriddiv').append(data.content);
				$('#customreportgriddivfooter').empty();
				$('#customreportgriddivfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('customreportgriddiv');
				{//sorting
					$('.customheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.customheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						var sortpos = $('#customreportgrid .gridcontent').scrollLeft();
						customreportgrid(page,rowcount);
						$('#customreportgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						var sortcol = $('.customheadercolsort').hasClass('datasort') ? $('.customheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.customheadercolsort').hasClass('datasort') ? $('.customheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						$("#processoverlay").hide();
					});
					sortordertypereset('customheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						Operation = 0;
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						customreportgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#customreportpgrowcount').change(function(){
						Operation = 0;
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = 1;//$('#prev').data('pagenum');
						customreportgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//Material select
				$('#customreportpgrowcount').material_select();
			},
		});
	}
	function folderdeletefunction(delfolderid,updatefolderid){
			var datarowid = $('#folderoperationgrid div.gridcontent div.active').attr('id');
			$.ajax({
				url: base_url + "Reportsview/deletereportfolder?primaryid="+datarowid+"&updatefolderid="+updatefolderid,
				cache:false,
				success: function(msg) {
					if(msg == true){
						$('#reportfolderrefreshicon,#reloadicon').trigger('click');
						loadreportfoldername();
						$("#conformfolderid").select2('val','');
						alertpopup("Folder Deleted successfully !!!");		
					} else {			
					}
				},
			});
		}
	//delete role dd val fetch
function deletefolderddvalfetch(folderid) {
	$('#conformfolderid').empty();
	$.ajax({
		url:base_url+'Reportsview/deletefolderddvalfetch?reportfolderid='+folderid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			$.each(data, function(index){					
				$('#conformfolderid')
				.append($("<option></option>")
				.attr("value",data[index]['reportfolderid'])
				.attr("data-setdefault",data[index]['setdefault'])
				.text(data[index]['reportfoldername']));
			});	
		},
	});
}
{
/* Reports Creation Overlay  Start*/
function newretrivereport(reportids)
{		
	var textboxname=['19','reporttypeid','reportfolderid','reportname','reportdescription','schedulereport','reportmodule','reportfrequency','ondays','recurrencytime','fileformat','recurrenceemail','recurrencerecipient','report_datefield','report_range','report_startdate','report_enddate','reportcategory','reportingmode','summaryrollup','printtemplateid','htmlfieldmodeid','htmlfieldcondition','htmlaccountid'];
	var data=['19','reporttypeid','reportfolderid','reportname','reportdescription','schedulereport','reportmodule','reportfrequency','ondays','recurrencytime','fileformat','recurrenceemail','recurrencerecipient','reportdatecolumnid','reportdatemethod','reportstartdate','reportenddate','reportcategory','reportingmode','summaryrollup','printtemplateid','htmlfieldmodeid','htmlfieldcondition','htmlaccountid'];
	var dropdowns=['9','reporttypeid','reportfolderid','reportmodule','recurrencytime','fileformat','recurrencerecipient','report_datefield','report_range','reportcategory','htmlfieldmodeid','htmlfieldcondition','htmlaccountid'];
	loadreportfolderin();
	//retrieve and set data
	$.ajax({
		url:base_url+"Reportsview/newretrievereport?dataprimaryid="+reportids, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			
			$('#reporttypeid').val(data['basic']['reporttypeid']).trigger('change');
			//Set Category Values.-New
			$("#reportcategory").select2('val',data['basic']['reportcategory']).trigger('change');			
			textboxsetvalue(textboxname,textboxname,data['basic'],dropdowns);
			
			$('#reportmodule').trigger('change');
			var relatedcolids = data['basic']['reportrelatedmoduleid'];
			var relatedcolid = relatedcolids.split(',');
			if(relatedcolid.length!='' && relatedcolid!=1){
			$('#reportrelatedmodule').select2('val', relatedcolid).trigger('change');
			}
			if(data['basic']['moduletabsectionid'] != 1 && data['basic']['moduletabsectionid'] != 0) {
				$('#reportmoduletab').select2('val', data['basic']['moduletabsectionid']).trigger('change');
			}
			if(data['basic']['reporttypeid'] == 5) {
				$('#printtemplateid').val(data['basic']['printtemplateid']);
				$('#htmlfieldmodeid').select2('val', data['basic']['htmlfieldmodeid']);
				$('#htmlfieldcondition').select2('val', data['basic']['htmlfieldcondition']);
				$('#htmlreportfromdate').val(data['basic']['htmlreportfromdate']);
				$('#htmlreporttodate').val(data['basic']['htmlreporttodate']);
				$('#htmlaccountid').select2('val', data['basic']['htmlaccountid']);
			}
			//set the multi-select related field values
			var viewcolids = data['basic']['reportmaincolumnid'];	
			var tabularcolids=data['basic']['tabularcolids'];	
			var summarycolids=data['basic']['summarycolids'];			
			var aggregatemethodids=data['basic']['aggregatemethodids'];	
			var aggregatemethodnames=data['basic']['aggregatemethodname'];
			if(data['basic']['aggregatemethodids']!= null){
				var amids=aggregatemethodids.split("|");
			}
			if(data['basic']['aggregatemethodname']!= null){
				var amname=aggregatemethodnames.split("|");
			}
			var groupbycolids=data['basic']['groupbycolids'];

			var sortby=data['basic']['sortby'];
			var sortbyarr=sortby.split(",");
			var groupbyrange=data['basic']['groupbycolrange'];
			var transfer=data['basic']['transfer'];
			if(data['basic']['reporttypeid'] != 5) {
				$.each(viewcolids.split(","), function(i,e){			
					$("#reportmaincolumnname option[value="+e+"]").attr("selected",true);
					$("#reportmaincolumnname_rightSelected").trigger('click');
				});
			}
			if(summarycolids != null || summarycolids != '')	{
				$.each(summarycolids.split(","), function(i,e){
					$("#reportmaincolumnname_to option[value="+e+"]").attr('data-selecttype',amids[i]);
					$("#reportmaincolumnname_to option[value="+e+"]").attr('data-selecttypename',amname[i]);
					
					var currenttext=$("#reportmaincolumnname_to option[value="+e+"]").text();
					$("#reportmaincolumnname_to option[value="+e+"]").text(currenttext+" - "+ amname[i]);
				});
			}
			if(data['basic']['reporttypeid'] != 5) {
				$.each(tabularcolids.split(","), function(i,e){			
					$("#reportmaincolumnname_to option[value="+e+"]").attr('data-applysummary','no');	
				});
				$.each(groupbycolids.split(","), function(i,e){			
					$("#reportgroupby1 option[value="+e+"]").attr("selected",true);
					$("#reportgroupby1_rightSelected").trigger('click');
				});
				$.each(groupbycolids.split(","), function(i,e){	
					$("#reportgroupby1_to option[value="+e+"]").attr('data-bytype',sortbyarr[i]);	
					var currenttext=$("#reportgroupby1_to option[value="+e+"]").text().split("-");
					$("#reportgroupby1_to option[value="+e+"]").text(currenttext[0]+" - "+ sortbyarr[i]);	
				});
			}
			$("#groupbyrange").val(groupbyrange);
			$("#transferyesno").val(transfer);
			if(transfer.toLowerCase() == 'yes') {
				$('#reptransfericon').text('Trnf Yes');
			} else {
				$('#reptransfericon').text('Trnf No');
			}
			newreportconditiongriddatafetch(reportids);
			//for filter
			$('#report_datefield').select2('val', data['basic']['reportdatecolumnid']).trigger('change');
			$('#report_range').select2('val', data['basic']['reportdatemethod']).trigger('change');
			$('#report_startdate').val(data['basic']['reportstartdate']);						
			$('#report_enddate').val(data['basic']['reportenddate']);	
			//For Keyboard Shortcut Variables
			addformupdate = 1;
			viewgridview = 0;
			addformview = 1;
			$("#hdnreportid").val(reportids);
			$("#processoverlay").hide();
			Materialize.updateTextFields();
		}
	});
}
//generate report
function generatereport(filetype,page,rowcount,exporttype ='No') {
	$("#processoverlay").show();
	$('#htmlprintingdiv').empty();
	$('#htmlprintingdiv').addClass('hidedisplay');
	var viewcolids = [];
	var tabularviewcolids = [];
	var summaryviewcolids = [];
	var summaramids = [];
	var summaramname = [];
	var samids = [];
	var groupcolids=[];
	var sortby=[];
	var j=0;
	var m=0;
	var k=0;
	var difference='';
	var commondstr=0;
	var formattype = $("#reporttypeid").val();
	$('#reportmaincolumnname_to option').each(function(i, selected) {
		if ($(this).attr('data-selecttype') != '') { 
			summaryviewcolids[m]=$(this).val();	
			summaramids[m]=$(this).attr('data-selecttype')+"|";
			summaramname[m]=$(this).attr('data-selecttypename')+"|";
			samids.push($(this).attr('data-selecttype'));
			m++;
		}
		if($(this).attr('data-applysummary')=='no') {
			tabularviewcolids[j]=$(this).val();	
			j++;
		}
		viewcolids[i]=$(this).val();
	});
	$('#reportgroupby1_to option').each(function(i, selected) {
		if($(this).attr('data-bytype') != '') {
			sortby[k]=$(this).attr('data-bytype');				
			groupcolids[k]=$(this).val();
			k++;
		}
	});
	var groupbyrange=$("#groupbyrange").val();
	var gcolids = groupcolids.slice(0,groupbyrange);			
	var common = $.grep(summaryviewcolids, function(element) {
		return $.inArray(element, gcolids) !== -1;
	});		 
	if(common.length!=0) {
		$("#processoverlay").hide();
		alertpopup("Summary columns and group by columns can't be same.");
		return false;
	}
	if(formattype == 3) { // summary  format check for summary columns
	var summarycheck=0;	
	$('#reportmaincolumnname_to option').each(function(i, selected) {
		if ($(this).attr('data-selecttype') != '' && $(this).val()!='') {
			summarycheck=1;
		}
	});
	if(summarycheck == '0') {
		$("#processoverlay").hide();
		alertpopup("To see summary view,please add atleast one summary view column.");
		return false;
	}
	}
	else if(formattype == 2) 
	{
		var doublesummarycheck=0;
		$('#reportmaincolumnname_to option').each(function(i, selected) {
			if ($(this).attr('data-selecttype').indexOf(',') > 0) {
				doublesummarycheck=1;
			}
		});
		if(doublesummarycheck == 1)
		{
			$("#processoverlay").hide();
			alertpopup("To see tabular view,please add only one or nil summary for selected column.");
			return false;
		}
			
	}
	{//daterange data
		var date_columnid=$('#report_datefield').val();
		var date_method=$('#report_range').val();
		var date_mode=$('#report_range').find('option:selected').data('mode');	
		var date_start=$('#report_startdate').val();
		var date_end=$('#report_enddate').val();
	}
	var reportconditioncount = $('#reportcreateconditiongrid .gridcontent div.data-content div').length;
	var griddata = getgridrowsdata('reportcreateconditiongrid');
	var reportconditiondata = JSON.stringify(griddata);
	var reportconditionid =getgridallrowids('reportcreateconditiongrid');
	if(reportconditionid != '') {
		var reportcreateconditionids=[];
		for(var i = 0; i < reportconditionid.length; i++) {
			var rowId = reportconditionid[i];
			reportcreateconditionids[i]= getgridcolvalue('reportcreateconditiongrid',rowId,'reportcondcolumnid','');
		}
	} else {
		reportcreateconditionids='';
	}		
	var formdatam = $("#reportbasicdetails").serialize();
	var amp = '&';
	var reportbasicdetails = amp + formdatam;
	var reportrelatedmodule=$('#reportrelatedmodule').val();
	var reportname = $("#reportname").val();
	var reportmodule = $("#reportmodule").val();
	if($.trim($('#hdnreportconditiondata').val().toUpperCase())==$.trim(reportconditiondata.toUpperCase())) {
		commondstr=1;
	} else {
		difference=1;
	}	
	if($.trim($('#hdngroupcolids').val().toUpperCase())==$.trim(groupcolids)) {
		commondstr=1;
	} else { 
		difference=2;
	}
	if($.trim($('#hdnsortby').val())==$.trim(sortby)) {
		commondstr=1;
	} else {
		difference=3;
	}
	if($.trim($('#hdngroupbyrange').val().toUpperCase())==$.trim(groupbyrange)) {
		commondstr=1;
	} else { 
		difference=4;
	}
	if($.trim($('#hdnviewcolids').val().toUpperCase())==$.trim(viewcolids)) {
		commondstr=1;
	} else {
		difference=5;
	}		
	if($.trim($('#hdntabularviewcolids').val().toUpperCase())==$.trim(tabularviewcolids)) {
		commondstr=1;
	} else {
		difference=6;
	}
	if($.trim($('#hdnsummarviewcolids').val().toUpperCase())==$.trim(summaryviewcolids)) {
		commondstr=1;
	} else {
		difference=7;
	}
	if($.trim($('#hdnsummaramids').val().toUpperCase())==$.trim(summaramids)) {
		commondstr=1;
	} else {
		difference=8;
	}
	if($.trim($('#hdndate_columnid').val().toUpperCase())==$.trim(date_columnid)) {
		commondstr=1;
	} else {
		difference=9;
	}
	if($.trim($('#hdndate_method').val().toUpperCase())==$.trim(date_method)) { 
		commondstr=1;
	} else {
		difference=10;
	}
	if($.trim($('#hdndate_mode').val().toUpperCase())==$.trim(date_mode)) { 
		commondstr=1;
	} else {
		difference=11;
	}
	if($.trim($('#hdndate_start').val().toUpperCase())==$.trim(date_start)) { 
		commondstr=1;
	} else {
		difference=12;
	}
	if($.trim($('#hdndate_end').val().toUpperCase())==$.trim(date_end)) {
		commondstr=1;
	} else {
		difference=13;
	}
	if($.trim($('#hdnreportname').val())==$.trim($('#reportname').val())) {
		commondstr=1;
	} else {
		difference=14;
	}
	if($.trim($('#hdnreportfolderid').val())==$.trim($('#reportfolderid').val())) {
		commondstr=1;
	} else {
		difference=15;
	}
	if($.trim($('#hdnreportmodule').val())==$.trim($('#reportmodule').val())) {
		commondstr=1;
	} else {
		difference=16;
	}
	if($.trim($('#hdnreportrelatedmodules').val())==$.trim($('#reportrelatedmodule').val())) {
		commondstr=1;
	} else {
		difference=17;
	}
	if($.trim($('#hdnreportdescription').val())==$.trim($('#reportdescription').val())) {
		commondstr=1;
	} else {
		difference=18;
	}
	/* Tabular Overlay Part */
	var tabularcondition ='';
	var tabularconditionvalue ='';
	if($("#hdntabularoverlay").val()=='YES') {
		var tabularcondition = $("#hdntabularcondition").attr('data-condition');
		var tabularconditionvalue = $("#hdntabularcondition").attr('data-values');
		rftype = 'TV';
	} else {
		var rftype = $("#reporttype").val();
	}
	/* Tabular Overlay Part */
	var wwidth = $(window).width();
	var wheight = $(window).height();
	if(Operation == 1){
		var rowcount = $('#customreportpgrowcount').val();
		var page = $('.paging').data('pagenum');
	} else{
		var rowcount = $('#customreportpgrowcount').val();
		page = typeof page == 'undefined' &&  typeof page !='' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' && typeof rowcount!='' ? 100 : rowcount;
	}
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol) {
		sortcol = sortcol;
	} else {
		var sortcol = $('.customheadercolsort').hasClass('datasort') ? $('.customheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.customheadercolsort').hasClass('datasort') ? $('.customheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.customheadercolsort').hasClass('datasort') ? $('.customheadercolsort.datasort').attr('id') : '0';
	var footername = 'customreport';
	/* Decide the function want to call*/
	var functionurl='';
	var filterid = $("#reportfilterid").val()+'|'+$("#reportddfilterid").val();
	var conditionname = $("#reportconditionname").val()+'|'+$("#reportddconditionname").val();
	var filtervalue = $("#reportfiltervalue").val()+'|'+$("#reportddfiltervalue").val();
	var formattype = $("#reporttypeid").val();
	var transferyesno = $("#transferyesno").val();
	var reportingmode = $("#reportingmode").val();
	var summaryrollup = $("#summaryrollup").val();
	if(formattype == 2) {
		rftype = 'TV';	
		$("#reporttypename").text('  Tabular View');
	} else if(formattype == 3) {
		rftype = 'SV';	
		$("#reporttypename").text('  Summary View');
	} else if(formattype == 5) {
		rftype = 'HTML';	
		$("#reporttypename").text('  HTML View');
	}
	if(filetype=='xlsx' || filetype=='csv' || filetype=='pdf') {
		if(filetype=='pdf') {
			functionurl='Reportsview/newpdfdataexport';
			target = '_blank';
		} else {
			functionurl='Reportsview/newexceldataexport';
			target = '_blank';
		}  
		var form = $("<form action="+functionurl+" class='hidedisplay' name='excelexport' id='excelexport' method='POST' target='"+target+"'><input type='hidden' name='reportconditioncount' id='reportconditioncount' value='"+reportconditioncount+"' /><input type='hidden' name='reportconditiondata' id='reportconditiondata' value="+reportconditiondata.replace(/("[^ "]*")|\s/g, "$1")+" /><input type='hidden' name='reportbasicdetails' id='reportbasicdetails' value="+formdatam +" /><input type='hidden' name='reportname' id='reportname' value="+reportname.replace(/("[^ "]*")|\s/g, "00")+" /><input type='hidden' name='groupcolids' id='groupcolids' value='"+groupcolids +"' /><input type='hidden' name='sortby' id='sortby' value='"+sortby +"' /><input type='hidden' name='groupbyrange' id='groupbyrange' value="+groupbyrange+" /><input type='hidden' name='viewcolids' id='viewcolids' value='"+viewcolids+"' /><input type='hidden' name='tabularviewcolids' id='tabularviewcolids' value='"+tabularviewcolids+"' /><input type='hidden' name='summarviewcolids' id='summarviewcolids' value='"+summaryviewcolids +"' /><input type='hidden' name='summaramids' id='summaramids' value='"+summaramids +"' /><input type='hidden' name='summaramname' id='summaramname' value='"+summaramname +"' /><input type='hidden' name='date_columnid' id='date_columnid' value="+date_columnid +" /><input type='hidden' name='date_method' id='date_method' value='"+date_method+"' /><input type='hidden' name='date_mode' id='date_mode' value='"+date_mode+"' /><input type='hidden' name='date_start' id='date_start' value='"+date_start+"' /><input type='hidden' name='date_end' id='date_end' value='"+date_end+"' /><input type='hidden' name='reportrelatedmodules' id='reportrelatedmodules' value="+reportrelatedmodule +" /><input type='hidden' name='wwidth' id='wwidth' value="+wwidth +" /><input type='hidden' name='wheight' id='wheight' value="+wheight +" /><input type='hidden' name='reportcreateconditionids' id='reportcreateconditionids' value='"+reportcreateconditionids+"' /><input type='hidden' name='samids' id='samids' value="+samids +" /><input type='hidden' name='rftype' id='rftype' value="+rftype +" /><input type='hidden' name='filetypename' id='filetypename' value="+filetype+" /><input type='hidden' name='reportmodule' id='reportmodule' value="+reportmodule+" ><input type='hidden' name='filter' id='filter' value="+filterid+" /><input type='hidden' name='conditionname' id='conditionname' value="+conditionname+" /><input type='hidden' name='filtervalue' id='filtervalue' value="+filtervalue+" /><input type='hidden' name='rowcount' id='rowcount' value="+rowcount+" /><input type='hidden' name='reportexportall' id='reportexportall' value="+exporttype+" /><input type='hidden' name='page' id='page' value="+page+" /><input type='hidden' name='transferyesno' id='transferyesno' value="+transferyesno+" /><input type='hidden' name='summaryrollup' id='summaryrollup' value="+summaryrollup+" /><input type='hidden' name='reportingmode' id='reportingmode' value="+reportingmode+" /></form>");
		$(form).appendTo('body');
		form.submit();
	} else {
		
		functionurl='Reportsview/generatereport';
		$.ajax({
			url:base_url+""+functionurl+"?reportconditioncount="+reportconditioncount+"&reportconditiondata="+reportconditiondata+"&reportbasicdetails="+reportbasicdetails+"&groupcolids="+groupcolids+"&sortby="+sortby+"&groupbyrange="+groupbyrange+"&viewcolids="+viewcolids+"&tabularviewcolids="+tabularviewcolids+"&summarviewcolids="+summaryviewcolids+"&summaramids="+summaramids+"&summaramname="+summaramname+"&date_columnid="+date_columnid+"&date_method="+date_method+"&date_mode="+date_mode+"&date_start="+date_start+"&date_end="+date_end+"&reportrelatedmodules="+reportrelatedmodule+"&wwidth="+wwidth+"&wheight="+wheight+"&reportcreateconditionids="+reportcreateconditionids+"&samids="+samids+"&rftype="+rftype+"&filetypename="+filetype+"&page="+page+"&records="+rowcount+"&tabularcondition="+tabularcondition+"&tabularconditionvalue="+tabularconditionvalue+"&footername="+footername+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue+'&sortcol='+sortcol+'&sortord='+sortord+"&reportname="+reportname+"&formattype="+formattype+"&transferyesno="+transferyesno+"&reportingmode="+reportingmode+"&summaryrollup="+summaryrollup,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				var filename = data.filename;
				if(filetype == 'html') {
					htmlfilename = base_url+filename;
					window.open(htmlfilename, '_blank');
				} else {
					var data = data.rdata;
					if($("#hdntabularoverlay").val()=='YES') {
						$('#tabulargriddiv').empty();
						$('#tabulargriddiv').append(data.content);
						$('#tabulargriddivfooter').empty();
						$('#tabulargriddivfooter').append(data.footer);
						$("#overlaystatus").val(1); // For close the form overlay event
						//data row select event
						datarowselectevt();
						//column resize
						columnresize('tabulargriddiv');
						{//sorting
							 $('.customheadercolsort').click(function() {
								//alert('asas');
								$("#processoverlay").show();
								$('.customheadercolsort').removeClass('datasort');
								$(this).addClass('datasort');
								var page = $(this).data('pagenum');
								var rowcount = $('ul#custompgnumcnt li .page-text .active').data('rowcount');
								var sortcol = $('.customheadercolsort').hasClass('datasort') ? $('.customheadercolsort.datasort').data('sortcolname') : '';
								var sortord =  $('.customheadercolsort').hasClass('datasort') ? $('.customheadercolsort.datasort').data('sortorder') : '';
								$("#sortorder").val(sortord);
								$("#sortcolumn").val(sortcol);
								var sortpos = $('#generatereport .gridcontent').scrollLeft();
								generatereport('',page,rowcount);
								$('#generatereport .gridcontent').scrollLeft(sortpos);//scroll to sorted position
								$("#processoverlay").hide();
							});
							sortordertypereset('reportsheadercolsort',headcolid,sortord);
						}
						{//pagination
							$('#tabulargrid .pvpagnumclass').click(function(){
								var page = $(this).data('pagenum');
								var rowcount = $('.pagerowcount').data('rowcount');
								generatereport('',page,rowcount);
							});
							$('#tabulargrid #pgrowcount').change(function(){
								var rowcount = $(this).val();
								$('.pagerowcount').attr('data-rowcount',rowcount);
								$('.pagerowcount').text(rowcount);
								var page = $('.pvpagnumclass').data('pagenum');
								generatereport('',page,rowcount);
							});
						}
						//Material select
						$('#tabulargrid .pagedropdown').material_select();
						$("#closeoverlay").trigger('click');
						$("#processoverlay").hide();
					} else {
						$('#customreportgrid').removeClass('hidedisplay');
						$('#customreportgriddiv').empty();
						$('#customreportgriddiv').append(data.content);
						$('#customreportgriddivfooter').empty();
						$('#customreportgriddivfooter').append(data.footer);
						$("#overlaystatus").val(1); // For close the form overlay event
						//data row select event
						datarowselectevt();
						//column resize
						columnresize('customreportgriddiv');
						{//sorting
							if(rftype == 'TV') {
								$('.customheadercolsort').click(function() {
									$("#processoverlay").show();
									$('.customheadercolsort').removeClass('datasort');
									$(this).addClass('datasort');
									var page = $(this).data('pagenum');
									var rowcount = $('ul#custompgnumcnt li .page-text .active').data('rowcount');
									var sortpos = $('#generatereport .gridcontent').scrollLeft();
									generatereport('',page,rowcount);
									$('#generatereport .gridcontent').scrollLeft(sortpos);//scroll to sorted position
									var sortcol = $('.customheadercolsort').hasClass('datasort') ? $('.customheadercolsort.datasort').data('sortcolname') : '';
									var sortord =  $('.customheadercolsort').hasClass('datasort') ? $('.customheadercolsort.datasort').data('sortorder') : '';
									$("#sortorder").val(sortord);
									$("#sortcolumn").val(sortcol);
									$("#processoverlay").hide();
								});
								sortordertypereset('customheadercolsort',headcolid,sortord);
							}
						}
						{//pagination
							$('.pvpagnumclass').click(function() {
								$("#processoverlay").show();
								var page = $(this).data('pagenum');
								var rowcount = $('.pagerowcount').data('rowcount');
								generatereport('',page,rowcount);
								$("#processoverlay").hide();
							});
							$('#customreportpgrowcount').change(function() {
								$("#processoverlay").show();
								var rowcount = $(this).val();
								$('.pagerowcount').attr('data-rowcount',rowcount);
								$('.pagerowcount').text(rowcount);
								var page = 1;
								generatereport('',page,rowcount);
								$("#processoverlay").hide();
							});
						}
						//Material select
						$('#customreportgrid .pagedropdown').material_select();
						$("#closeoverlay").trigger('click');
						$("#processoverlay").hide();
					}
				}
			}
		});
		
	}
	$("#processoverlay").hide();
}
function generatehtmlprintview() {
	var randomnumber = Math.floor((Math.random() * 100) + 1);
	var templateid = $('#printtemplateid').val();
	var htmlfieldcondition = $('#htmlfieldcondition').find('option:selected').val();
	var htmlaccountid = $('#htmlaccountid').find('option:selected').val();
	var htmlreportfromdate = "";
	var htmlreporttodate = "";
	var pdfmoduleid = $('#reportmodule').find('option:selected').val();
	var viewtype = "report";
	if(htmlfieldcondition != "") {
		if(htmlfieldcondition == "Custom") {
			htmlreportfromdate = $('#htmlreportfromdate').val();
			htmlreporttodate = $('#htmlreporttodate').val();
		} else {
			var datevalue = retrievedatevaluehtmlreport();
			htmlreportfromdate = datevalue[0];
			htmlreporttodate = datevalue[1];
		}
		$.ajax({
			url:base_url+'Base/templatefilepdfpreview',
			data:{id:"",snumber:"",templateid:templateid,stockdate:htmlreportfromdate,stocktodate:htmlreporttodate,stocksessionid:"",pdfmoduleid:pdfmoduleid,randomnumber:randomnumber,viewtype:viewtype,htmlaccountid:htmlaccountid},
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if(nmsg != '') {
					$("#reportnameval").text($("#reportname").val());
					$('#customreportgrid').addClass('hidedisplay');
					$('#customreportgriddiv').empty();
					$("#closeaddform,#htmlprintingdiv").removeClass('hidedisplay');
					$('#htmlprintingdiv').empty();
					$('#htmlprintingdiv').append(nmsg);
					$("#overlaystatus").val(1);
					$("#closeoverlay").trigger('click');
				} else {
					alertpopup('Error on preview,try again!!!');
				}
			},
		});
	}
}
function retrievedatevaluehtmlreport() {
	var datearrayvalue = [];
	var htmlfieldcondition = $('#htmlfieldcondition').find('option:selected').val();
	$.ajax({
		url:base_url+"Reportsview/retrievedatevaluehtmlreport?htmlfieldcondition="+htmlfieldcondition,
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			if(data != '') {
				datearrayvalue.push(data[0]);
				datearrayvalue.push(data[1]);
			}
		}
	});
	return datearrayvalue;
}
function savereporttodb() {
	var reporttypeid = $('#reporttypeid').find('option:selected').val();
	var viewcolids = [];
	var tabularviewcolids = [];
	var summaryviewcolids = [];
	var summaramids = [];	
	var summaramname = [];						
	var groupcolids=[];
	var sortby=[];
	var j=0;
	var m=0;
	var k=0;
	$('#reportmaincolumnname_to option').each(function(i, selected) {
		if ($(this).attr('data-selecttype') != '') {
			summaryviewcolids[m]=$(this).val();	
			summaramids[m]=$(this).attr('data-selecttype')+"|";
			summaramname[m]=$(this).attr('data-selecttypename')+"|";
			m++;
		}
		if($(this).attr('data-applysummary')=='no'){
			tabularviewcolids[j]=$(this).val();	
			j++;
		}
		viewcolids[i]=$(this).val();
	});
	$('#reportgroupby1_to option').each(function(i, selected) {
		sortby[k]=$(this).attr('data-bytype');				
		groupcolids[k]=$(this).val();
		k++;
	});
	var groupbyrange=$("#groupbyrange").val();
	var transferyesno=$("#transferyesno").val();
	var reportingmode=$("#reportingmode").val();
	var summaryrollup=$("#summaryrollup").val();
		
	{//daterange data
		if($.trim($('#report_datefield').find('option:selected').val()) == '')
		{
			var date_columnid=1;
		}else
		{
			var date_columnid=$('#report_datefield').val();
		}
		//alert($.trim($('#report_range').find('option:selected').val()));
		//alert($.trim($('#report_range').find('option:selected').data('mode')));
		if($.trim($('#report_range').find('option:selected').val()) == '')
		{
			var date_method=1;
		}else{
			var date_method=$('#report_range').val();
		}
		if($.trim($('#report_range').find('option:selected').data('mode')) == '')
		{
			var date_mode=1;
		}else{
			var date_mode=$('#report_range').find('option:selected').data('mode');
		}
		var date_start=$('#report_startdate').val();
		var date_end=$('#report_enddate').val();
	}
	var reportconditioncount = $('#reportcreateconditiongrid .gridcontent div.data-content div').length;
	var griddata = getgridrowsdata('reportcreateconditiongrid');
	var reportconditiondata = JSON.stringify(griddata);
	var reportconditionid =getgridallrowids('reportcreateconditiongrid');
	if(reportconditionid != '') {	
		var reportcreateconditionids=[];
		for (var i = 0; i < reportconditionid.length; i++) {
			var rowId = reportconditionid[i];
			reportcreateconditionids[i]= getgridcolvalue('reportcreateconditiongrid',rowId,'reportcondcolumnid','');
		}
	} else {	
		reportcreateconditionids='';
	}
	var formdatam = $("#reportbasicdetails").serialize();
	var amp = '&';
	var reportbasicdetails = amp + formdatam;
	var reportrelatedmodule=$('#reportrelatedmodule').val();
	var hdnreportid=$('#hdnreportid').val();
	page = typeof page == 'undefined' &&  typeof page !='' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' && typeof rowcount!='' ? 100 : rowcount;
	var wwidth = $(window).width();
	var wheight = $(window).height();
	var sortcol = $('.reportsheadercolsort').hasClass('datasort') ? $('.reportsheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.reportsheadercolsort').hasClass('datasort') ? $('.reportsheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.reportsheadercolsort').hasClass('datasort') ? $('.reportsheadercolsort.datasort').attr('id') : '0';
	var manualjoin=$("#manualjoin").val();
	var whereclausestring=$("#whereclausestring").val();
	var havingstring=$("#havingstring").val();
	var havingselect=$("#havingselect").val();
	var printtemplateid = $("#printtemplateid").val();
	var htmlfieldmodeid = $("#htmlfieldmodeid").find('option:selected').val();
	var htmlfieldcondition = $("#htmlfieldcondition").find('option:selected').val();
	var htmlreportfromdate = $("#htmlreportfromdate").val();
	var htmlreporttodate = $("#htmlreporttodate").val();
	$.ajax({
		url:base_url+"Reportsview/savereporttodb?reportconditioncount="+reportconditioncount+"&reportconditiondata="+reportconditiondata+"&reportbasicdetails="+reportbasicdetails+"&groupcolids="+groupcolids+"&sortby="+sortby+"&groupbyrange="+groupbyrange+"&viewcolids="+viewcolids+"&tabularviewcolids="+tabularviewcolids+"&summarviewcolids="+summaryviewcolids+"&summaramids="+summaramids+"&summaramname="+summaramname+"&date_columnid="+date_columnid+"&date_method="+date_method+"&date_mode="+date_mode+"&date_start="+date_start+"&date_end="+date_end+"&reportrelatedmodules="+reportrelatedmodule+"&reportcreateconditionids="+reportcreateconditionids+"&hdnreportid="+hdnreportid+"&transferyesno="+transferyesno+"&reportingmode="+reportingmode+"&summaryrollup="+summaryrollup+"&havingselect="+havingselect+"&havingstring="+havingstring+"&whereclausestring="+whereclausestring+"&manualjoin="+manualjoin+"&printtemplateid="+printtemplateid+"&htmlfieldmodeid="+htmlfieldmodeid+"&htmlfieldcondition="+htmlfieldcondition+"&htmlreportfromdate="+htmlreportfromdate+"&htmlreporttodate="+htmlreporttodate,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			if(data == 'sndefault') {
				$("#processoverlay").hide();
				alertpopup('Cannot save default records.');
			} else {	
				alertpopup(savealert);
				$("#hdnreportid").val(data);
				if(reporttypeid != 5) {
					generatereport();
				}
				$("#processoverlay").hide();
				setTimeout(function() {
					$("#alertsclose").trigger("click");
					$("#closeaddform").trigger("click");
				},1000); 
			}
		}
	});
}	
function getreportsdata()
{
	var viewcolids = [];
	var tabularviewcolids = [];
	var summaryviewcolids = [];
	var summaramids = [];
	var summaramname = [];
	var samids = [];
	
	var groupcolids=[];
	var sortby=[];
	var j=0;
	var m=0;
	var k=0;
	$('#reportmaincolumnname_to option').each(function(i, selected) {
		if ($(this).attr('data-selecttype') != '') { 
			summaryviewcolids[m]=$(this).val();	
			summaramids[m]=$(this).attr('data-selecttype')+"|";
			summaramname[m]=$(this).attr('data-selecttypename')+"|";
			samids.push($(this).attr('data-selecttype'));
			m++;
		}
		if($(this).attr('data-applysummary')=='no'){ 
			tabularviewcolids[j]=$(this).val();	
			j++;
		}
		viewcolids[i]=$(this).val();
	});
	$('#reportgroupby1_to option').each(function(i, selected) {
		sortby[k]=$(this).attr('data-bytype');				
		groupcolids[k]=$(this).val();
		k++;
	});
	var groupbyrange=$("#groupbyrange").val();
	
	
	var gcolids = groupcolids.slice(0,groupbyrange);			
	var common = $.grep(summaryviewcolids, function(element) {
		return $.inArray(element, gcolids) !== -1;
	});		 
	if(common.length!=0)
	{
		$("#processoverlay").hide();
		alertpopup("Summary columns and group by columns can't be same.");
		return false;
	}
	{//daterange data
		var date_columnid=$('#report_datefield').val();
		var date_method=$('#report_range').val();
		var date_mode=$('#report_range').find('option:selected').data('mode');	
		var date_start=$('#report_startdate').val();
		var date_end=$('#report_enddate').val();
	}
	var reportconditioncount = $('#reportcreateconditiongrid .gridcontent div.data-content div').length;
	var griddata = getgridrowsdata('reportcreateconditiongrid');
	var reportconditiondata = JSON.stringify(griddata);
	
	var reportconditionid =getgridallrowids('reportcreateconditiongrid');
	if(reportconditionid != '')
	{	var reportcreateconditionids=[];
		for (var i = 0; i < reportconditionid.length; i++) 
		{
		var rowId = reportconditionid[i];
		reportcreateconditionids[i]= getgridcolvalue('reportcreateconditiongrid',rowId,'reportcondcolumnid','');
		}
	}
	else
	{	reportcreateconditionids='';
	}		

	var formdatam = $("#reportbasicdetails").serialize();
	var amp = '&';
	var reportbasicdetails = amp + formdatam;
	var reportrelatedmodule=$('#reportrelatedmodule').val();
	var reportmodule = $("#reportmodule").val();
	
	$("#hdnreportconditioncount").val(reportconditioncount);
	$("#hdnreportconditiondata").val(reportconditiondata);
	$("#hdnreportcreateconditionids").val(reportcreateconditionids);
	$("#hdngroupcolids").val(groupcolids);
	$("#hdnsortby").val(sortby);
	$("#hdngroupbyrange").val(groupbyrange);
	$("#hdnviewcolids").val(viewcolids);
	$("#hdntabularviewcolids").val(tabularviewcolids);
	$("#hdnsummarviewcolids").val(summaryviewcolids);			
	$("#hdnsummaramids").val(summaramids);
	
	$("#hdndate_columnid").val(date_columnid);
	$("#hdndate_method").val(date_method);
	$("#hdndate_mode").val(date_mode);
	$("#hdndate_start").val(date_start);
	$("#hdndate_end").val(date_end);			
	
	$("#hdnreportname").val($("#reportname").val());
	$("#hdnreportfolderid").val($("#reportfolderid").val());
	$("#hdnreportmodule").val(reportmodule);
	$("#hdnreportrelatedmodules").val(reportrelatedmodule);		
	$("#hdnreportdescription").val($("#reportdescription").val());
	if($('#reportcondcolumn').val() != '') {
		$('#reportcondcolumn').trigger('change');
	}
}
function newreportconditiongriddatafetch(viewid) {
	if(viewid!='') {
		$.ajax({
			url:base_url+'Base/reportconditiongriddatafetch?userviewid='+viewid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else { 
					loadinlinegriddata('reportcreateconditiongrid',data.rows,'json');
					/* data row select event */
					datarowselectevt();
					/* column resize */
					columnresize('reportcreateconditiongrid');
					var hideprodgridcol = [];
					gridfieldhide('reportcreateconditiongrid',hideprodgridcol);
					var condcnt = $('#reportcreateconditiongrid .gridcontent div.data-content div').length;
					if(condcnt==0) {
						$("#reportandorconddivhid").hide();
					} else {
						$("#reportandorconddivhid").show();
					}
				}
			},
		});
	}
}
	function tabularoverlay(a) {
		if($("#reporttype").val()=='SV') {
			$("#actiontype").val('GR'); // GR => Generate Report
			$("#hdntabularoverlay").val('YES'); 
			$("#hdntabularcondition").attr('data-condition',$.trim(a.getAttribute('data-name'))); 
			$("#hdntabularcondition").attr('data-values',$.trim(a.text)); 
			$('.ftab').trigger('click');
			$("#tabularviewoverlay").fadeIn();	
			$("#validatereportbasicdetails").validationEngine('validate');
		}
	}
}
/* Reports Creation Overlay  End*/	

{	/* CODE CLEANING Start*/

	//loads the report folder names into the report create/edit form
	function loadreportfolderin()
	{ 
		$("#reportfolderid").empty();
		$('#reportfolderid').append($("<option></option>"));
		$.ajax({
			url:base_url+"Reportsview/loadreportfoldername", 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				$.each(data, function(index){					
					$('#reportfolderid')
					.append($("<option></option>")
					.attr("value",data[index]['reportfolderid'])
					.attr("data-setdefault",data[index]['setdefault'])
					.text(data[index]['reportfoldername']));
				});	
				$('#reportfolderid option[data-setdefault="Yes"]').attr('selected','selected').trigger('change');
			}
		});
	}
	/* Load view creation column based on module and relation module selection
		1. Select column
		2. Date filter column
		3. Advanced filter
		4. Group by column
	*/
	function loadcolumnname(moduleid,tabsction=1)
	{
		$("#reportmaincolumnname,#reportmaincolumnname_to,#reportgroupby1,#reportgroupby1_to").empty();
		$('#reportgroupby1').empty();
		$('#reportgroupby1').select2('val','').trigger('change');		
		$("#reportmaincolumnname").empty();
		$("#reportcalculationon").empty();
		$("#report_datefield").empty();
		$("#report_datefield").append("<option></option>");
		$.ajax({
			url:base_url+"Reportsview/setreportcolumndata?moduleid="+moduleid+"&tabsction="+tabsction,
			async:false,
			success: function(data) {
				$("#reportmaincolumnname").append(data);	
			}
		});
		$.ajax({
			url:base_url+"Reportsview/loadcolumnname?moduleid="+moduleid+"&tabsction="+tabsction,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {			
				$.each(data, function(index){		
					//calculation field load
					var nval = data[index]['viewcreationcolumnname']+'('+data[index]['modulename']+")";
					if(data[index]['uitypeid'] == '8' ) {
						$('#report_datefield')
							.append($("<option></option>")
							.attr("data-uitypeid",data[index]['uitypeid'])
							.attr("value",data[index]['viewcreationcolumnid'])				
							.text(nval));
							$('#reportcondcolumn')
							.append($("<option></option>")
							.attr("data-indexname",data[index]['viewcreationcolmodelindexname'])
							.attr("data-dbindexname",data[index]['viewcreationjoincolmodelname'])
							.attr("data-uitypeid",data[index]['uitypeid'])
							.attr("data-reportcondcolumnid",data[index]['viewcreationcolumnid'])
							.attr("value",data[index]['viewcreationcolumnname'])
							.text(nval));
							
							$('#reportgroupby1')
							.append($("<option></option>")
							.attr("value",data[index]['viewcreationcolumnid'])
							.attr("data-applygroupby",'')
							.attr("data-bytype",'')
							.text(nval));
					} else if(data[index]['uitypeid'] == '31' ) { // datetime
						$('#report_datefield')
							.append($("<option></option>")
							.attr("data-uitypeid",data[index]['uitypeid'])
							.attr("value",data[index]['viewcreationcolumnid'])				
							.text(nval));
						
						$('#reportcondcolumn')
							.append($("<option></option>")
							.attr("data-indexname",data[index]['viewcreationcolmodelindexname'])
							.attr("data-dbindexname",data[index]['viewcreationjoincolmodelname'])
							.attr("data-uitypeid",data[index]['uitypeid'])
							.attr("data-reportcondcolumnid",data[index]['viewcreationcolumnid'])
							.attr("value",data[index]['viewcreationcolumnname'])
							.text(nval));
						$('#reportgroupby1')
							.append($("<option></option>")
							.attr("value",data[index]['viewcreationcolumnid'])
							.attr("data-applygroupby",'')
							.attr("data-bytype",'')
							.text(nval));
					} else {
						
						$('#reportcondcolumn')
							.append($("<option></option>")
							.attr("data-indexname",data[index]['viewcreationcolmodelindexname'])
							.attr("data-dbindexname",data[index]['viewcreationjoincolmodelname'])
							.attr("data-uitypeid",data[index]['uitypeid'])
							.attr("data-reportcondcolumnid",data[index]['viewcreationcolumnid'])
							.attr("value",data[index]['viewcreationcolumnname'])
							.text(nval));
						$('#reportgroupby1')
							.append($("<option></option>")
							.attr("value",data[index]['viewcreationcolumnid'])
							.attr("data-applygroupby",'')
							.attr("data-bytype",'')
							.text(nval));
					}
				});
			}
		});
	}
	//date Range
	function daterangefunction()
	{
		$('#report_startdate').datetimepicker("destroy");
		$('#report_enddate').datetimepicker("destroy");
		var dateformetdata = $('#report_startdate').attr('data-dateformater');
		var startDateTextBox = $('#report_startdate');
		var endDateTextBox = $('#report_enddate');
		startDateTextBox.datetimepicker({
			dateFormat: dateformetdata,
			showTimepicker :false,
			minDate: null,
			changeMonth: true,
			changeYear: true,
			maxDate:0,
			yearRange : "1947:c+100",
			onClose: function(dateText, inst) {
				if (endDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						endDateTextBox.datetimepicker('setDate', testStartDate);
				}
				else {
					endDateTextBox.val(dateText);
				} 
				$('#report_startdate').focus();
			},
			onSelect: function (selectedDateTime){
				endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
					$(this).prev('div').remove();
				} 
			}
		}).click(function() {
			$(this).datetimepicker('show');
		});
		endDateTextBox.datetimepicker({
			dateFormat: dateformetdata,
			showTimepicker :false,
			minDate: null,
			changeMonth: true,
			changeYear: true,
			maxDate:0,
			yearRange : "1947:c+100",
			onClose: function(dateText, inst) {
				if (startDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
					startDateTextBox.datetimepicker('setDate', testEndDate);
				}
				else {
					startDateTextBox.val(dateText);
				}
				$('#report_enddate').focus();
			},
			onSelect: function (selectedDateTime) {
				startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
			}
		}).click(function() {
			$(this).datetimepicker('show');
		});
	}
	{ // Advanced Filter Condition =>Field Name UIType Based Loading the condition values.(TEXT?DD?DATE)
		function showhideintextbox(hideid,showid)
		{
			$("#"+hideid+"divhid").hide();
			$("#"+showid+"divhid").show();
		}
		//field name based drop down value - picklist
		function reportfieldnamebesdpicklistddvalue(fieldid,dropdownid,data)
		{
			var moduleid = $("#reportmodule").val();
			$.ajax({
				url: base_url + "Base/fieldnamebesdpicklistddvalue?fieldid="+fieldid+"&moduleid="+moduleid+"&uitype="+data,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					$('#'+dropdownid+'').empty();
					$('#'+dropdownid+'').append($("<option></option>"));
					if((data.fail) == 'FAILED') {
					} else {
						$.each(data, function(index) {
							$("#"+dropdownid+"")
							.append($("<option></option>")
							.attr("value",data[index]['datasid'])
							.attr("data-valuename",data[index]['dataname'])
							.text(data[index]['dataname']));
						});
					}
					$('#'+dropdownid+'').trigger('change');			
				},
			});
		}
		//field name based drop down value
		function fieldnamebesdddvalue(fieldid,dropdownid)
		{	
			$.ajax({
				url: base_url + "Base/fieldnamebesdddvalue?fieldid="+fieldid,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) 
				{
					$("#"+dropdownid+"").empty();
					$("#"+dropdownid+"").append($("<option></option>"));
					if((data.fail) == 'FAILED') {
					} else {
						$.each(data, function(index) {
							$("#"+dropdownid+"")
							.append($("<option></option>")
							.attr("value",data[index]['datasid'])
							.attr("data-valuename",data[index]['dataname'])
							.text(data[index]['dataname']));
						});
					}
					$("#"+dropdownid+"").trigger('change');			
				},
			});
		}
		//check box value get
		function checkboxvalueget()
		{
			$.ajax({
				url: base_url + "Base/checkboxvalueget",
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) 
				{
					$('#reportddcondvalue').empty();
					$('#reportddcondvalue').append($("<option></option>"));
					if((data.fail) == 'FAILED') {
					} else {
						$.each(data, function(index) {
							if(data[index]['uitype'] != '15' || data[index]['uitype'] != '16')
							{
								$('#reportddcondvalue')
								.append($("<option></option>")
								.attr("value",data[index]['dataname'])
								.attr("data-finalreportcondvalueid",data[index]['datasid'])
								.text(data[index]['dataname']));
							}
						});
					}	
				},
			});
		}
		function empgroupdrodownset(dropdownid) {
			$.ajax({
				url: base_url + "Base/viewemployeegroupsdata",
				dataType: "json",
				async: false,
				cache:false,
				success: function(data) {
					if(data['status']!='fail') {
						$('#'+dropdownid+'').empty();
						prev = ' ';
						$.each(data, function(index) {
							var cur = data[index]['PId'];
							if(prev != cur ) {
								if(prev != " ") {
									$('#'+dropdownid+'').append($("</optgroup>"));
								}
								$('#'+dropdownid+'').append($("<optgroup  label='"+data[index]['PName']+"' class='"+data[index]['PName']+"'>"));
								prev = data[index]['PId'];
							}
							$("#"+dropdownid+" optgroup[label='"+data[index]['PName']+"']")
							.append($("<option></option>")
							.attr("class",data[index]['CId']+'pclass')
							.attr("value",data[index]['CId'])
							.attr("data-ddid",data[index]['CId'])
							.attr("data-dataidhidden",data[index]['PId'])
							.text(data[index]['CName']));
						});
					}
				},
			});
		}
		//AND/OR  condition validation reset
		function andorcondvalidationreset()
		{
			var viewcount = $('#reportcreateconditiongrid .gridcontent div.data-content div').length;
			if(viewcount < 1) { 
				$('#reportandorcond').removeClass('validate[required]');
				$('.condmandclass').text('');
			}else 
			{ 		
				if(getgridcolvalue('reportcreateconditiongrid',$("#editconditionid").val(),'reportandorcond','')!='')
				{	$('#reportandorcond').addClass('validate[required]'); 
				}
				else
				{	$('#reportandorcond').removeClass('validate[required]');		
				}				
				$('.condmandclass').text('*');
			}
		}
		//View condition Grid
		function reportcreateconditiongrid(page,rowcount) {
			//GRA to restrict grid reload and take width of hidden grid
			$('#newoverlayform').css('visibility','hidden').show();
			$('#newoverlayform #subformspan2').css('display', 'block !important').show();
			var wwidth = $('#newoverlayform #reportcreateconditiongrid').width();
			$('#newoverlayform #subformspan2').css('display', 'block !important').hide();
			$('#newoverlayform').css('visibility','visible').hide();
			var wheight = $("#reportcreateconditiongrid").height();
			$.ajax({
				url:base_url+"Base/reportconddatafetch?moduleid=1&width="+wwidth+"&height="+wheight+"&modulename=Condition Grid",
				dataType:'json',
				async:false,
				cache:false,
				success:function(data) {
					$("#reportcreateconditiongrid").empty();
					$("#reportcreateconditiongrid").append(data.content);
					$("#reportcreateconditiongridfooter").empty();
					$("#reportcreateconditiongridfooter").append(data.footer);
					/* data row select event */
					datarowselectevt();
					/* column resize */
					columnresize('reportcreateconditiongrid');
				},
			});
		}
		function reportconditiongriddatafetch(viewid) {
			if(viewid!='') {
				$.ajax({
					url:base_url+'Base/reportconditiongriddatafetch?userviewid='+viewid,
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						if((data.fail) == 'FAILED') {
						} else { 
							loadinlinegriddata('reportcreateconditiongrid',data.rows,'json');
							/* data row select event */
							datarowselectevt();
							/* column resize */
							columnresize('reportcreateconditiongrid');
							var hideprodgridcol = [];
							gridfieldhide('reportcreateconditiongrid',hideprodgridcol);
							var condcnt = $('#reportcreateconditiongrid .gridcontent div.data-content div').length;
							if(condcnt==0) {
								$("#reportandorconddivhid").hide();
							} else {
								$("#reportandorconddivhid").show();
							}
						}
					},
				});
			}
		}
}		
	//report unique name check
	function reportnamecheck() {
		var primaryid = $("#hdnreportid").val();
		var accname = $("#reportname").val();
		var elementpartable = 'report';
		if( accname !="" ) {
			$.ajax({
				url:base_url+"Base/uniquedynamicviewnamecheck",
				data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
				type:"post",
				async:false,
				cache:false,
				success :function(msg) {
					nmsg = $.trim(msg);
				},
			});
			if(nmsg != "False") {
				if(primaryid != ""){
					if(primaryid != nmsg) {
						return "Report name already exists!";
					}
				} else {
					return "Report name already exists!";
				}
			} 
		}
	}
	function datetimepickerstartendtrigger() {
		const picker = new MaterialDatePicker()
			.on('submit', (val) => {
			 var valuedate = val.toDate() +'\n';
			 $('#report_startdatetime').val(convert(valuedate));
			 Materialize.updateTextFields();
			})
			document.querySelector('#report_startdatetime')
			.addEventListener('click', () => picker.open() || picker.set(moment()));
		const pickerend = new MaterialDatePicker()
			.on('submit', (val) => {
			 var valuedateend = val.toDate() +'\n';
			 $('#report_enddatetime').val(convert(valuedateend));
			 Materialize.updateTextFields();
			})
			document.querySelector('#report_enddatetime')
			.addEventListener('click', () => pickerend.open() || pickerend.set(moment()));
	}
}
function reportfiltercolumnfetch() {
	$("#filtercolumndisplay").empty();
	var moduleid = $("#reportmodule").val();
	var relatedmoduleid = $("#reportrelatedmodule").val();
	var moduletabsecid = $("#reportmoduletab").val();
	$.ajax({
		url:base_url+"Reportsview/reportfiltercolumnfetch",
		data:"moduleid="+moduleid+"&relatedmoduleid="+relatedmoduleid+"&moduletabsecid="+moduletabsecid,
		type:"post",
		dataType:'json',
		async:false,
		cache:false,
		success :function(msg) {
			var filtercontent = msg;
			$("#filtercolumndisplay").append(filtercontent);
			$(".chzn-select").select2({containerCss: {display: "block"}});
			//for dropdown change
			$(".reportddfilerchange").change(function() {
				$("#processoverlay").show();
				reportdropdownfilterchange(generatereport);
				$("#processoverlay").hide();
			});
			reportfiltername = [];
			reportcondition = [];
			reportvalue = [];
			$("#reportfilteraddcondsubbtn").click(function() {
				$("#reportfilterformconditionvalidation").validationEngine("validate");	
			});
			$("#reportfilterformconditionvalidation").validationEngine({
				onSuccess: function() {
					reportmainviewfilterwork(generatereport); 
				},
				onFailure: function() {
					var dropdownid =[];
					dropdownfailureerror(dropdownid);
					alertpopup(validationalert);
				}
			});
			//For main view with grid modules(Only for base modules)
			reportmainfiltervalue = [];
			$(".reportcolumnfilter").change(function() {
				var cfvallue = $(this).val();
				$(".reportcolumnfilter").select2('val','');
				$(".reportcolumnfilter").removeClass('filteractive');
				$(this).select2('val',cfvallue);
				$(this).addClass('filteractive');
				var data = $(this).find("option:selected").data("uitype");
				$('#reportfiltercondition').empty();
				$.ajax({
					url:base_url+"Base/loadconditionbyuitype?uitypeid="+data, 
					dataType:'json',
					async:false,
					success: function(data) { 
						$.each(data, function(index){					
							$('#reportfiltercondition')
							.append($("<option></option>")
							.attr("value",data[index]['whereclausename'])
							.text(data[index]['whereclausename']));
						});	
					}
				});
				var fieldname = $(this).find("option:selected").data("indexname");
				var moduleid = $(this).find("option:selected").data("moduleid");
				var fieldlable = $(this).find("option:selected").data("label");
				if(data == "2" || data == "3" || data == "4"|| data == "5" || data == "6"|| data == "7"|| data == "10" || data == "11" || data == "12"|| data == "14"|| data == "30") {
					showhideintextbox("reportfilterddcondvalue","reportfiltercondvalue");
					$("#reportfilterdatecondvaluedivhid,#reportfilterdatecondtovaluedivhid").hide();
					$("#reportfilterassignddcondvaluedivhid").hide();
					$("#reportfiltercondvalue").val("");
					//$("#reportfiltercondvalue").timepicker("remove");
					$("#reportfiltercondvalue").removeAttr('class');
					$("#reportfilterddcondvalue").removeClass("validate[required]");
					$("#reportfilterdatecondvalue,#reportfilterdatecondtovalue").removeClass("validate[required]");
					$("#reportfilterassignddcondvalue").removeClass("validate[required]");
					fieldhidedisplay(data,'reportfiltercondvalue');
				} else if(data == "17" || data == "28") {
					showhideintextbox("reportfiltercondvalue","reportfilterddcondvalue");
					$("#reportfilterassignddcondvaluedivhid").hide();
					$("#reportfilterdatecondvaluedivhid,#reportfilterdatecondtovaluedivhid").hide();
					$("#reportfilterddcondvalue").select2("val","");
					$("#reportfilterddcondvalue").addClass("validate[required]");
					$("#reportfilterdatecondvalue,#reportfilterdatecondtovalue").removeClass("validate[required]");
					$("#reportfilterassignddcondvalue").removeClass("validate[required]");
					$("#reportfiltercondvalue").removeAttr('class');
					$("#reportfiltercondvalue").datepicker("disable");
					fieldnamebesdpicklistddvalue(fieldname,data,"reportfilterddcondvalue",moduleid);
				} else if(data == "18" || data == "19" || data == "21" || data == "22" || data == "25" || data == "26" || data == "29" ) {
					showhideintextbox("reportfiltercondvalue","reportfilterddcondvalue");
					$("#reportfilterassignddcondvaluedivhid").hide();
					$("#reportfilterdatecondvaluedivhid,#reportfilterdatecondtovaluedivhid").hide();
					$("#reportfilterddcondvalue").addClass("validate[required]");
					$("#reportfilterddcondvalue").select2("val","");
					$("#reportfilterdatecondvalue,#reportfilterdatecondtovalue").removeClass("validate[required]");
					$("#reportfiltercondvalue").removeAttr('class');
					$("#reportfilterassignddcondvalue").removeClass("validate[required]");
					$("#reportfiltercondvalue").datepicker("disable");
					fieldnamebesdddvalue(fieldname,"reportfilterddcondvalue",moduleid); 
				} else if(data == "20") {
					showhideintextbox("reportfilterddcondvalue","reportfilterassignddcondvalue");
					$("#reportfiltercondvaluedivhid").hide();
					$("#reportfilterdatecondvaluedivhid,#reportfilterdatecondtovaluedivhid").hide();
					$("#reportfilterassignddcondvalue").addClass("validate[required]");
					$("#reportfilterddcondvalue").select2("val","");
					$("#reportfilterddcondvalue").removeClass("validate[required]");
					$("#reportfilterdatecondvalue,#reportfilterdatecondtovalue").removeClass("validate[required]");
					$("#reportfiltercondvalue").removeAttr('class');
					$("#reportfiltercondvalue").datepicker("disable");
					empgroupdrodownset("reportfilterassignddcondvalue");
				} else if(data == "13") {
					$("#reportfiltercondvalue").datepicker("disable");
					showhideintextbox("reportfiltercondvalue","reportfilterddcondvalue");
					$("#reportfilterassignddcondvaluedivhid").hide();
					$("#reportfilterdatecondvalue,#reportfilterdatecondtovalue").removeClass("validate[required]");
					$("#reportfilterassignddcondvalue").removeClass("validate[required]");
					$("#reportfilterddcondvalue").select2("val","");
					$("#reportfilterddcondvalue").addClass("validate[required]");
					$("#reportfiltercondvalue").removeAttr('class');
					checkboxvalueget("reportfilterddcondvalue");
				} else if(data == "8") {
					//for hide
					$("#reportfilterdatecondvalue,#reportfilterdatecondtovalue").datetimepicker("destroy");
					$("#reportfilterddcondvaluedivhid").hide();
					$("#reportfilterassignddcondvaluedivhid").hide();
					$("#reportfiltercondvaluedivhid").hide();
					$("#reportfilterdatecondvaluedivhid,#reportfilterdatecondtovaluedivhid").show();
					$("#reportfilterdatecondvalue,#reportfilterdatecondtovalue").val("");
					$("#reportfilterddcondvalue").removeClass("validate[required]");
					$("#reportfiltercondvalue").removeAttr('class');
					$("#reportfilterdatecondvalue,#reportfilterdatecondtovalue").addClass("validate[required]");
					$("#reportfilterassignddcondvalue").removeClass("validate[required]");
					if(fieldlable == "Date of Birth" || fieldlable == "Start Date" || fieldlable == "End Date") {
						var dateformetdata = $("#reportfilterdatecondvalue").attr("data-dateformater");
						$("#reportfilterdatecondvalue").datetimepicker({
							dateFormat: dateformetdata,
							showTimepicker :false,
							minDate: null,
							changeMonth: true,
							changeYear: true,
							maxDate:0,
							yearRange : "1947:c+100",
							onSelect: function () {
								var checkdate = $(this).val();
								if(checkdate != "") {
									$("#reportfilterdatecondtovalue").val(checkdate);
									$("#reportfilterfinalviewcondvalue").val(checkdate+':'+checkdate);
									$("#reportfilterfinalviewconid").val(checkdate+':'+checkdate);
								}
							},
							onClose: function () {
								$("#reportfilterdatecondvalue").focus();
							}
						}).click(function() {
							$(this).datetimepicker("show");
						});
						$("#reportfilterdatecondtovalue").datetimepicker({
							dateFormat: dateformetdata,
							showTimepicker :false,
							minDate: null,
							changeMonth: true,
							changeYear: true,
							maxDate:0,
							yearRange : "1947:c+100",
							onSelect: function () {
								var checkdate = $(this).val();
								var fromdate = $("#reportfilterdatecondvalue").val();
								if(fromdate == "") {
									$("#reportfilterdatecondvalue").val(checkdate);
									fromdate = checkdate;
								}
								if(checkdate != "" && fromdate != "") {
									$("#reportfilterdatecondtovalue").val(checkdate);
									$("#reportfilterfinalviewcondvalue").val(fromdate+':'+checkdate);
									$("#reportfilterfinalviewconid").val(fromdate+':'+checkdate);
								}
							},
							onClose: function () {
								$("#reportfilterdatecondtovalue").focus();
							}
						}).click(function() {
							$(this).datetimepicker("show");
						});
					} else {
						var dateformetdata = $("#reportfilterdatecondvalue").attr("data-dateformater");
						$("#reportfilterdatecondvalue").datetimepicker({
							dateFormat: dateformetdata,
							showTimepicker :false,
							minDate: null,
							changeMonth: true,
							changeYear: true,
							maxDate:0,
							yearRange : "1947:c+100",
							onSelect: function () {
								var checkdate = $(this).val();
								$("#reportfilterdatecondtovalue").val(checkdate);
								$("#reportfilterfinalviewcondvalue").val(checkdate+':'+checkdate);
								$("#reportfilterfinalviewconid").val(checkdate+':'+checkdate);
							},
							onClose: function () {
								$("#reportfilterdatecondvalue").focus();
							}
						}).click(function() {
							$(this).datetimepicker("show");
						});
						var dateformetdata = $("#reportfilterdatecondtovalue").attr("data-dateformater");
						$("#reportfilterdatecondtovalue").datetimepicker({
							dateFormat: dateformetdata,
							showTimepicker :false,
							minDate: null,
							changeMonth: true,
							changeYear: true,
							maxDate:0,
							yearRange : "1947:c+100",
							onSelect: function () {
								var fromdate = $("#reportfilterdatecondvalue").val();
								var checkdate = $(this).val();
								if(fromdate == "") {
									$("#reportfilterdatecondvalue").val(checkdate);
									fromdate = checkdate;
								}
								$("#reportfilterfinalviewcondvalue").val(fromdate+':'+checkdate);
								$("#reportfilterfinalviewconid").val(fromdate+':'+checkdate);
							},
							onClose: function () {
								$("#reportfilterdatecondtovalue").focus();
							}
						}).click(function() {
							$(this).datetimepicker("show");
						});
					}
					Materialize.updateTextFields();
				} else if(data == "9") {
					$("#reportfilterdatecondvalue,#reportfilterdatecondtovalue").datetimepicker("destroy");
					//for hide
					$("#reportfilterddcondvaluedivhid").hide();
					$("#reportfilterassignddcondvaluedivhid").hide();
					$("#reportfiltercondvaluedivhid").show();
					$("#reportfilterdatecondvaluedivhid,#reportfilterdatecondtovaluedivhid").hide();
					$("#reportfiltercondvalue").val("");
					$("#reportfilterddcondvalue").removeClass("validate[required]");
					$("#reportfiltercondvalue").addClass("validate[required,maxSize[100]]");
					$("#reportfilterdatecondvalue,#reportfilterdatecondtovalue").removeClass("validate[required]");
					$("#reportfilterassignddcondvalue").removeClass("validate[required]");
					$("#reportfiltercondvalue").timepicker({
						"step":15,
						"timeFormat": "H:i",
						"scrollDefaultNow": true,
					});
					var checkdate = $("#reportfiltercondvalue").val();
					if(checkdate != ""){
						$("#reportfilterfinalviewcondvalue").val(checkdate);
						$("#reportfilterfinalviewconid").val(checkdate);
					}
				} else if(data == "27") {
					showhideintextbox("reportfiltercondvalue","reportfilterddcondvalue");
					$("#reportfilterdatecondvaluedivhid,#reportfilterdatecondtovaluedivhid").hide();
					$("#reportfilterassignddcondvaluedivhid").hide();
					$("#reportfilterddcondvalue").addClass("validate[required]");
					$("#reportfilterddcondvalue").select2("val","");
					$("#reportfilterdatecondvalue,#reportfilterdatecondtovalue").removeClass("validate[required]");
					$("#reportfilterassignddcondvalue").removeClass("validate[required]");
					$("#reportfiltercondvalue").removeAttr('class');
					$("#reportfiltercondvalue").datepicker("disable");
					fieldnamebesdddvaluewithcond(fieldname,"reportfilterddcondvalue"); 
				} else if(data == "23") {
					showhideintextbox("filtercondvalue","filterddcondvalue");
					$("#reportfilterdatecondvaluedivhid,#reportfilterdatecondtovaluedivhid").hide();
					$("#reportfilterassignddcondvaluedivhid").hide();
					$("#reportfilterddcondvalue").addClass("validate[required]");
					$("#reportfilterddcondvalue").select2("val","");
					$("#reportfilterdatecondvalue,#reportfilterdatecondtovalue").removeClass("validate[required]");
					$("#reportfilterassignddcondvalue").removeClass("validate[required]");
					$("#reportfiltercondvalue").removeAttr('class');
					$("#reportfiltercondvalue").datepicker("disable");
					attributepurityvalueget(fieldname,"reportfilterddcondvalue"); 
				} else {
					alertpopup("Please choose another field name");
				}
				{//field value set
					$("#reportfiltercondvalue").focusout(function(){
						var value = $("#reportfiltercondvalue").val();
						$("#reportfilterfinalviewcondvalue").val(value);
						$("#reportfilterfinalviewconid").val(value);
					});
					$("#reportfilterddcondvalue").change(function(){
						var value = $("#reportfilterddcondvalue").val();
						reportmainfiltervalue=[];
						$('#reportfilterddcondvalue option:selected').each(function(){
						    var $value =$(this).attr('data-ddid');
						    reportmainfiltervalue.push($value);
							$("#reportfilterfinalviewcondvalue").val(reportmainfiltervalue);
						});
						$("#reportfilterfinalviewconid").val(value);
					});
					//only for assign to
					$("#reportfilterassignddcondvalue").change(function() {
						var valuename = $("#reportfilterassignddcondvalue").val();
						var id =$('#reportfilterassignddcondvalue').find('option:selected').data('ddid');
						var typeid =$('#reportfilterassignddcondvalue').find('option:selected').data('dataidhidden');
						var value = typeid+'--'+id;
						$("#reportfilterfinalviewcondvalue").val(value);
						$("#reportfilterfinalviewconid").val(valuename);
					});
				}
			});
			//condition dd change
			$("#reportfiltercondition").change(function() {
				var whereid = $("#reportfiltercondition").val();
				var uitype = $(".reportcolumnfilter").find("option:selected").data("uitype");
				var fieldname = $(".reportcolumnfilter").find("option:selected").data("indexname");
				var moduleid = $(".reportcolumnfilter").find("option:selected").data("moduleid");
				if(whereid == 'Between') {
					showhideintextbox("reportfiltercondvalue","reportfilterddcondvalue");
					$("#reportfilterassignddcondvaluedivhid").hide();
					$("#reportfilterdatecondvaluedivhid,#reportfilterdatecondtovaluedivhid").hide();
					$("#reportfilterddcondvalue").select2("val","");
					$("#reportfilterddcondvalue").addClass("validate[required]");
					$("#reportfilterdatecondvalue").removeClass("validate[required]");
					$("#reportfilterassignddcondvalue").removeClass("validate[required]");
					$("#reportfiltercondvalue").removeAttr('class');
					$("#reportfiltercondvalue").datepicker("disable");
					fieldnamebesdpicklistddvalue(fieldname,uitype,"reportfilterddcondvalue",moduleid);
				} else {
					$('#reportfiltercondvalue').prop('readonly',false);
					showhideintextbox("reportfilterddcondvalue","reportfiltercondvalue");
					$("#reportfilterdatecondvaluedivhid,#reportfilterdatecondtovaluedivhid").hide();
					$("#reportfilterassignddcondvaluedivhid").hide();
					$("#reportfiltercondvalue").val("");
					//$("#reportfiltercondvalue").timepicker("remove");
					$("#reportfiltercondvalue").removeAttr('class');
					$("#reportfilterddcondvalue").removeClass("validate[required]");
					$("#reportfilterdatecondvalue").removeClass("validate[required]");
					$("#reportfilterassignddcondvalue").removeClass("validate[required]");
					fieldhidedisplay(uitype,'reportfiltercondvalue');
					$("#reportfilterdatecondvaluedivhid,#reportfilterdatecondtovaluedivhid").hide();
					$("#reportfilterdatecondvalue,#reportfilterdatecondtovalue").removeClass("validate[required]");
					if(uitype == 8) {
						if(whereid == 'Custom') {
							$("#reportfiltercondvaluedivhid").hide();
							$("#reportfiltercondvalue").val("");
							$("#reportfilterdatecondvaluedivhid,#reportfilterdatecondtovaluedivhid").show();
							$("#reportfilterdatecondvalue,#reportfilterdatecondtovalue").addClass("validate[required]");
						} else {
							$("#reportfilterdatecondvaluedivhid,#reportfilterdatecondtovaluedivhid").hide();
							$("#reportfilterdatecondvalue,#reportfilterdatecondtovalue").removeClass("validate[required]");
							$("#reportfiltercondvaluedivhid").show();
							$("#reportfiltercondvalue").val("");
							$("#reportfiltercondvalue").val(whereid);
							$('#reportfiltercondvalue').prop('readonly',true);
						}
					} else {
						$("#reportfiltercondvaluedivhid").show();
						$("#reportfilterdatecondvaluedivhid,#reportfilterdatecondtovaluedivhid").hide();
					}
				}
			});
			function showhideintextbox(hideid,showid){
				$("#"+hideid+"divhid").hide();
				$("#"+showid+"divhid").show();
			}
		},
	});
}
//dropdownfilterwork
function reportdropdownfilterchange(gridname) {
	var ddlength = $("#reportfilterdropdownlenth").val();
	var modulefieldid = ''
	var filtervalue = '';
	var filtercondition = '';
	for(var i=0;i<ddlength;i++) {
		var uitype = $("#reportddfilerchange"+i+"").attr('data-uitype');
		if(uitype == 13) {
			var id = $("#reportddfilerchange"+i+"").attr('data-modulefieldid');
			var condition = 'Equalto';
			var value = $("#checkbox"+i+"").val();
			if(value == 'No') {
				value = '';
			}
		} else {
			var id = $("#reportddfilerchange"+i+"").attr('data-modulefieldid');
			var value = $("#reportddfilerchange"+i+"").val();
			var condition = 'IN';
		}
		if(value != ''){
			if(modulefieldid != '') {
				modulefieldid = modulefieldid+'|'+id;
			} else {
				modulefieldid = id;
			}
			if(filtervalue != '') {
				filtervalue = filtervalue+'|'+value;
			} else {
				filtervalue = value;
			}
			if(filtercondition != '') {
				filtercondition = filtercondition+'|'+condition;
			} else {
				filtercondition = condition;
			}
		}
	}
	$("#reportddfilterid").val(modulefieldid);
	$("#reportddconditionname").val(filtercondition);
	$("#reportddfiltervalue").val(filtervalue);
}
//filter work for main view module
function reportmainviewfilterwork(gridname) {
	var columnid =$(".filteractive").val();
	var label =$(".filteractive").find('option:selected').attr('data-label');
	var condition = $("#reportfiltercondition").val();
	var value = $("#reportfilterfinalviewcondvalue").val();
	var valuename = $("#reportfilterfinalviewconid").val();
	{ // get filterdata for verification
		var fid = $("#reportfilterid").val();
		var cname = $("#reportconditionname").val();
		var fvalue = $("#reportfiltervalue").val();
	}
	var count = reportfiltername.length;
	var usecond = label+' is '+condition+' '+valuename;
	$("#reportfilterconddisplay").append("<span class='innnerfilterresult'>"+usecond+" <i id='reportfilterdocclsbtn"+count+"' class='material-icons reportfilterdocclsbtn' style='position:relative' data-fileid='"+count+"'>close</i></span>");
	if(reportfiltername.length>0){
		var filterid = $("#reportfilterid").val()+'|'+columnid;
		var filtercondition = $("#reportconditionname").val()+'|'+condition;
		var filtervalue = $("#reportfiltervalue").val()+'|'+value;
		$("#reportfilterid").val(filterid);
		$("#reportconditionname").val(filtercondition);
		$("#reportfiltervalue").val(filtervalue);
	} else {
		$("#reportfilterid").val(columnid);
		$("#reportconditionname").val(condition);
		$("#reportfiltervalue").val(value);
	}
	reportfiltername.push(columnid);
	reportcondition.push(condition);
	reportvalue.push(value);
	$("#reportfilterdocclsbtn"+count+"").click(function() {
		var id = $(this).data("fileid");
		//for condition value split
		reportfiltername.splice($.inArray(reportfiltername[id],reportfiltername),1);
		var fname = filterarrayget(reportfiltername);
		console.log(fname);
		$("#reportfilterid").val(fname);
		//for filter id split
		reportcondition.splice($.inArray(reportcondition[id],reportcondition),1);
		var fid = filterarrayget(reportcondition);
		console.log(fid);
		$("#reportconditionname").val(fid);
		//for filter value
		reportvalue.splice($.inArray(reportvalue[id],reportvalue),1);
		var fval = filterarrayget(reportvalue);
		console.log(fval);
		$("#reportfiltervalue").val(fval);
		$(this).parent("span").remove();
		$(this).next("br").remove();
		$(this).remove();
	});
	$(".reportcolumnfilter").select2('val','');//filterddcondvalue
	$("#reportfiltercondition").select2('val','');
	$("#reportfilterddcondvalue").select2('val','');
	$("#reportfiltercondvalue").val('');
	$("reportfilterfinalviewcondvalue").val('');
}
function loadtabsectionname(moduleid) {
	$('#reportmoduletab').empty();
	$('#reportmoduletab').append("<option></option>")
	$.ajax({
		url:base_url+"Reportsview/loadmoduletabsction?moduleid="+moduleid, 
		dataType:'json',
		async:false,
		success: function(data) {
			$.each(data, function(index){				
				$('#reportmoduletab')
				.append($("<option></option>")
				.attr("value",data[index]['id'])
				.text(data[index]['name']));
			});	
		}
	});
}
//Kumaresan - Report Folder name unique check
function reportfoldernamecheck() {
	var primaryid = $("#editprimarydataid").val();
	var accname = $("#reportsfoldername").val();
	var elementpartable = 'reportfolder';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Folder name already exists!";
				}
			}else {
				return "Folder name already exists!";
			}
		} 
	}
}
function reportopenclick(ids) {
	$("#processoverlay").show();
	if(ids) {
		var reportname = $('#reportsgrid div.gridcontent div.active ul').find('li:first').text();
		$("#reportnameval").text(reportname);
		$("#reporttypename").text('  Tabular View');
		$("#reporttype").val('TV');
		$("#closeaddform").removeClass('hidedisplay');
		newretrivereport(ids);	
		$("#reportviewoverlay").fadeIn();
		//$("#newoverlay").trigger("click");
		//$("#overlaystatus").val('');
		//$("#tab2").trigger('click');
		setTimeout(function(){
			$("#generatereport").trigger("click");
		},200);
		
	} else {
		alertpopup("Please select a row");
	}
	$("#processoverlay").hide();
}