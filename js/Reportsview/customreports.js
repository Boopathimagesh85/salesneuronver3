$(document).ready(function() 
{	
	$('.alertjusthere').live('click', function() {
		var originid=$(this).data('originid');
		var destinationid=$(this).data('destinationid');
		var rowid=$(this).data('rowid');
		var columnname=$(this).data('columnname');
		var columntable=$(this).data('columntable');
		if(originid == destinationid){
			sessionStorage.setItem("reportunique"+destinationid+"",rowid);
		}
		else{		
			var destinationrowid=getdestinationrowid(rowid,columntable,columnname);
			sessionStorage.setItem("reportunique"+destinationid+"",destinationrowid);
		}
	});
	{// Foundation Initialization
		$(document).foundation();		
	}
	{// To hide Recurrence Details
		$('#recurrencedetaildiv').hide();
	}
	{// Grid Calling Function		
		customreportgrid();	
		customreportsummarygrid();	
	}
	{// Close Report
		$("#closeaddform").click(function(){
			window.location =base_url+'Reportsview';
		});
	}
	$('div.gridcontent div.data-rows').dblclick(function(){
			var ids = $('#reportsgrid div.gridcontent div.active').attr('id');
			//opens the report list with that parameters
			$("#reportviewoverlay").fadeIn();
			customreportgrid();
	});

	{// Full Screen View
		$('#exitfulscrnreport').hide();
		$("#fulscrnreport").click(function(){
			$('#fulscrnreport').hide();
			$('#exitfulscrnreport').show();			
			$("#reportarea").removeClass('large-10').addClass('large-12');
			$("#leftpanearea").removeClass('large-2').addClass('hidedisplay');
		});
		$("#exitfulscrnreport").click(function(){			
			$('#exitfulscrnreport').hide();
			$('#fulscrnreport').show();
			$("#reportarea").removeClass('large-12').addClass('large-10');
			$("#leftpanearea").removeClass('hidedisplay').addClass('large-2');
		});
	}	
	{// Check box Filter
		$(".checkboxfilter").click(function(){	
			if($(this).attr('checked')) {
					var labelname = $(this).data('labelname'); //For Label Name
					var currentvalue = $(this).val(); // For Value
					$('#filtercontainer').append('<li><span>'+labelname+' : '+currentvalue+' </span><span class="fa fa-times filterliststyle" id="" title="Remove"> </span></li>');
					{// Remove Filter
							$(".filterliststyle").click(function(){
							$(this).parent('li').remove();
						});
					}
				} else {
					
				}
		});
	}
	//reloads the current grid
	$("#reportdetailreload").click(function(){	
	location.reload();
	});
	{// Drop Down Filter
		$(".dropdownfilter").change(function(){	
			var labelname = $(this).data('labelname'); //For Label Name
			var currentvalue = $(this).val(); // For Value
			var parenidvalue = $(this).attr('id'); // For Value 
			$('#filtercontainer').append('<li class="dd" data-parentname="'+parenidvalue+'"><span>'+labelname+' : '+currentvalue+' </span><span class="fa fa-times filterliststyle" id="" title="Remove"> </span></li>');
			{// Remove Filter
					$(".filterliststyle").click(function(){
					$(this).parent('li').remove();
				});
			}
		});
	}
});
{// Reports View Grid	
	function customreportsummarygrid() 
	{
	$("#customreportsummarygrid").jqGrid('GridUnload');
		$("#customreportsummarygrid").jqGrid(
		{
			url:"",
			datatype: "local",
			colNames: ['Field Name','Calculation','Value'],
			colModel: 
			[
				{name: 'reportname',index: 'reportname',width: 45},
				{name: 'reportdescription',index: 'reportdescription',width: 45},			  
				{name: 'reporttypename',index: 'reporttypename',width: 45}				
			],
			pager: '#customreportsummarygriddiv',
			sortname: 'reportname',
			multiselect: false,
			toolbar: [false, "top"],
			height: '100%',
			width:'auto',			
			loadonce:true,
			
		});
		//Inner Grid Height and Width Set Based On Screen Size
		innergridwidthsetbasedonscreen('customreportsummarygrid');
		}
}
{//generation of colmodel for dynamic data viewing grid
function reportcolumnsData(data)
	{
		var colwidth = [100,100,100,100,100,100,100,100,100,100];
		var formater = "";
		var str = "[";
		var Linkable= data.linkable;
		var Linkpath= data.linkpath;
		var Dataname= data.colmodelname;
		var Dataindex= data.colmodelindex;
		var Datatype= data.colmodeltype;
		var Dataviewtype= data.colmodelviewtype;
		var orginmodule=data.colmodelmoduleid;
		var linkmodule=data.linkmoduleid;
		var refcolumnname=data.coljoinmodelname;
		var refcolumntable=data.colmodelparenttable;
		var grplen=data.groupcolumnname
		//grouping data allocate
		var group=data.groupcolumnname;
		var len=group.length;
		var columnarray=[];
		for (var i = 0; i < len-1; i++) 
		{			
			str = str + "{name:'" + group[i]+i + "', index:'" + group[i] + "',resizable:false,hidden:true},";
			columnarray[i]=	group[i];		
		}
		for (var i = 0; i < Dataname.length; i++) 
		{
		
			var grpstatus=jQuery.inArray(Dataname[i], columnarray);
			if(grpstatus == -1){
				gattr='';
			}
			else{
				var grplength=(data.groupcolumnname).length;
				if(grpstatus == 0){
					gattr=", cellattr: function(rowId, cellValue, rawObject, cm, rdata) {  return groupgrid(rowId, cellValue, rawObject, cm, rdata);}";}
				else if(grpstatus == 1){
					gattr=", cellattr: function(rowId, cellValue, rawObject, cm, rdata) {  return groupgridtwo(rowId, cellValue, rawObject, cm, rdata);}";
				}
				else{
					gattr='';
				}
			}
			var dataview = "";
			var vtype = Dataviewtype[i];
			if(vtype == "0") { dataview = 'hidden: true ,'; }
			if (Dataname[i] == "Id") {		
				if(Datatype[i] == "date") {				
					str = str + "{name:'" + Dataname[i] + "', index:'" + Dataindex[i] + "', hidden: true, formatter:'date',formatoptions: {srcformat: 'ISO8601Long',newformat: 'm-d-Y h:i:s'},}";
				} else {			
					str = str + "{name:'" + Dataname[i] + "', index:'" + Dataindex[i] + "', hidden: true,}";
				}
			} else {		
				if(Datatype[i] == "date") {
					str = str + "{name:'" + Dataname[i] + "', index:'" + Dataindex[i] + "', "+dataview+" editable: true,formatter:'date',formatoptions: {srcformat: 'ISO8601Long', newformat: 'm-d-Y h:i:s'},}";
				} else {
					//most comes here
					//if its linker its here					
					if(Linkable[i] == 'Yes'){
						var linkpage=base_url+Linkpath[i];
						str = str + "{name:'" + Dataname[i] + "', index:'" + Dataindex[i] + "', "+dataview+" editable: true,resizable:false,sortable: false,formatter:function (cellvalue, options, rowObject){return LinkGenerator(cellvalue, options, rowObject,'"+Linkpath[i]+"','"+orginmodule[i]+"','"+linkmodule[i]+"','"+refcolumnname[i]+"','"+refcolumntable[i]+"');}" + gattr + "}";
					}
					else{
						str = str + "{name:'" + Dataname[i] + "', index:'" + Dataindex[i] + "', "+dataview+" editable: true,resizable:false,sortable: false" + gattr + "}";
					}
				}
			}
			if (i != Dataname.length - 1) {
				str = str + ",";
			}
		}		
		str = str + "]";
		return str;
	}
}
//
function LinkGenerator(cellvalue,options,rowObject,linkpath,origin,destination,columnname,columntable)
{	
	cellvalue = $.trim(cellvalue); //remove whitespaces from the value	
	if(checkValue(cellvalue) == true && cellvalue != 'undefined'){
		return '<a href="'+ base_url+linkpath +'" target="_blank" class="alertjusthere" datasamemodule="'+ base_url+linkpath +'" data-rowid="'+options['rowId']+'" data-originid="'+origin+'" data-destinationid="'+destination+'" data-columnname="'+columnname+'" data-columntable="'+columntable+'" >' + cellvalue + '</a>';
	}
	else{
		return '<a href=""  class="" data-rowid="">' + cellvalue + '</a>';
	}
}
//this function retrieves the primary id of that selected values
function getdestinationrowid(rowid,tablename,columnname)
{
	var id='';
	$.ajax({
			url:base_url+"Reportsview/getdestinationrowid?tablename="+tablename+"&primarid="+rowid+"&columnname="+columnname,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
					id=data.id;
			}
		});	
		return id;
}
//grouping grid
function customreportgrid_old() 
{	
		var userviewid=$('#hiddenrptid').val();
		$("#customreportgrid").jqGrid('GridUnload');
		$.ajax({
			url:base_url+"Reportsview/reportgridinformationfetchmodel?reportid="+userviewid,
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				if(data.groupcolumnname != 0){//on Grouping
				var colsname = reportcolumnsname(data.colname,data.groupcolumnname);
				}
				else{//No Grouping
				var colsname = data.colname;
				}
				var colData = '';
				colData = reportcolumnsData(data);
				colData = eval('{' + colData + '}');				
				$("#customreportgrid").jqGrid({	url:base_url+"Reportsview/summaryreportgridvalinformationfetch?viewid="+userviewid,
					//url:'',
					datatype: "xml",
					colNames:colsname,
					colModel:colData,
					pager:'#customreportgriddiv',
					sortname:'',
					sortorder:'desc',
					viewrecords:false,
					loadonce:true,
					rownumbers: false,
					toolbar: [false, "top"],
					rowNum:100,
					rowList:[100,200,300,400,500,600],
					height: '100%',
					width: 'auto',					
					loadComplete: function(data) {
						jQuery("#customreportsummarygrid").jqGrid('setGridParam', {datatype: "xml",url: base_url + 'Reports/reportcalculationdataview?viewid=' + userviewid}).trigger('reloadGrid');
					},
				});
			jQuery("#customreportgrid").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : false});
			},
		});
		//Inner Grid Height and Width Set Based On Screen Size
		innergridwidthsetbasedonscreen('customreportgrid');
		//For Left Padding Grid header data
		$(".labelcustomreportgrid tr th").css('padding-left','1rem');
} 
	
	function reportcolumnsname(columnname,groupname)
	{
		var group=groupname;
		var len=group.length;
		var newArray=[];
		for (var i = 0; i < len-1; i++) 
		{			
			 newArray[i]= groupname[i];
		}
		newArray = jQuery.grep(newArray, function(n, i){
			  return (n !== "" && n != null);
			});
		newArray = $.merge(newArray,columnname);	
		return newArray;	
	}
	function groupgrid(rowId, cellValue, rawObject, cm, rdata)
	{	
		var data = rdata;
		var arr = [];
		for (var prop in data) {
			arr.push(data[prop]);
		}
		if (arr[0] > 0) {
			return   result = ' rowspan=' + '"' + arr[0] + '"';
		} else if (arr[0] == 0) {
			return   result = ' style="display:none"';
		}
	}
	//two groupind grid
	function groupgridtwo(rowId, cellValue, rawObject, cm, rdata)
	{		
		var data = rdata;
		var arr = [];
		for (var prop in data) {
			arr.push(data[prop]);
		}
		if (arr[1] > 0) {
			return   result = ' rowspan=' + '"' + arr[1] + '"';
		} else if (arr[0] == 0) {
			return   result = ' style="display:none"';
		}
}
	//grouping grid
	function customreportgrid(page,rowcount)
	{	
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = $(window).width();
		var wheight = $(window).height();
		var sortcol = $('.reportsheadercolsort').hasClass('datasort') ? $('.reportsheadercolsort.datasort').data('sortcolname') : '';
		var sortord =  $('.reportsheadercolsort').hasClass('datasort') ? $('.reportsheadercolsort.datasort').data('sortorder') : '';
		var headcolid = $('.reportsheadercolsort').hasClass('datasort') ? $('.reportsheadercolsort.datasort').attr('id') : '0';
		var userviewid=$('#hiddenrptid').val();

		$.ajax({
			url:base_url+"Reportsview/reportgridinformationfetch?reportid="+userviewid+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#customreportgriddiv').empty();
				$('#customreportgriddiv').append(data.content);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('customreportgriddiv');
				{//sorting
					$('.reportsheadercolsort').click(function(){
						$('.reportsheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#reportspgnumcnt li .page-text .active').data('rowcount');
						customreportgrid(page,rowcount);
					});
					sortordertypereset('reportsheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						customreportgrid(page,rowcount);
					});
				}
			},
		});
	} 