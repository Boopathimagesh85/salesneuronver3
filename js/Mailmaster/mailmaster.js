$(document).ready(function() {	
	{// Foundation Initialization
		$(document).foundation();
	}
	{// Maindiv height width change
		maindivwidth();
		documentsdelid =[];
	}
	METHOD = 'ADD';
	{//Grid Calling
		mailcreategrid();
		//crud action
		crudactionenable();
		//default value set
		$("#emailtemplatesfolingrideditspan1").show();
		{//grid width fix
			$("#tab1").click(function(){
				emailtemplatesfoladdgrid1();
			});
			$("#tab3").click(function(){
				//emailtemplatesattaddgrid2();
			});
		}
	}
	{//inner-form-with-grid
		$("#emailtemplatesfolingridadd1").click(function(){
			//Sectionpanel height set
			sectionpanelheight('emailtemplatesfoloverlay');
			$("#emailtemplatesfoloverlay").removeClass("closed");
			$("#emailtemplatesfoloverlay").addClass("effectbox");
			$('#foldernamename,#description').val('');
			$('#setdefaultcboxid,#publiccboxid').prop('checked', false);
			$("#setdefault,#public").val('No');
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$("#emailtemplatesfolcancelbutton").click(function(){
			$("#emailtemplatesfoloverlay").removeClass("effectbox");
			$("#emailtemplatesfoloverlay").addClass("closed");
			$("#emailtemplatesfolingridadd1,#emailtemplatesfolingridedit1,#emailtemplatesfolingriddel1").show();
		});
		$("#emailtemplatesattingridadd2").click(function(){
			//Sectionpanel height set
			sectionpanelheight('emailtemplatesattoverlay');
			$("#emailtemplatesattoverlay").removeClass("closed");
			$("#emailtemplatesattoverlay").addClass("effectbox");
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$("#emailtemplatesattcancelbutton").click(function(){
			$("#emailtemplatesattoverlay").removeClass("effectbox");
			$("#emailtemplatesattoverlay").addClass("closed");
		});
	}
	froalaarray=['emailtemplatesemailtemplate_editor','html.set',''];
	{//Close Add Screen
		var addcloseoppocreation =["closeaddform","mailmastercreationview","mailmastercreationformadd"]
		addclose(addcloseoppocreation);	
		var addcloseviewcreation =["viewcloseformiconid","mailmastercreationview","viewcreationformdiv",""];
		addclose(addcloseviewcreation);
	}
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			mailcreategrid();
		});
	}
	{
		$('button > i.fa-upload').click(function(){
			$('.fr-form').trigger('click');
		});
	}
	{//view by drop down change
		$('#emailcampaigntypeid').change(function(){
			var ctid = $('#emailcampaigntypeid').val();
			if(ctid == 2){
				$('.fr-toolbar').hide();
			}else{
				$('.fr-toolbar').show();
			}
		});
	}
	{//button hide
		$("#emailtemplatesattupdatebutton").hide();
	}
	{//Tags Option Code
		$("#keywords").select2({
			tags:[],
			separator: ";",
			tokenSeparators: [";", " "],
		});
	}
	{
		$('#closeaddform').click(function(){
			$('#filenameattachdisplay').empty();
		});
	}
	//folder name grid reload
	$('#tab2').click(function(){
		folderdatareload();
		mailcreategrid();
	});
	$('#formclearicon').click(function(){
		froalaset(froalaarray);
		$("#formfields").empty();
		clearformgriddata();
		$("#keywords").select2('val','');
		$('#filenameattachdisplay').empty();
		cleargriddata('emailtemplatesattaddgrid2');
		$("#templatetypeid").val('3');
	});
	//close
	$('#closeaddoppocreation').click(function(){
		$('#dynamicdddataview').trigger('change');
	});
	{// For touch
		fortabtouch = 0;
	}
	{//Toolbar Icon Change Function View
		$("#cloneicon").click(function() {
			var datarowid = $('#mailcreategrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				folderrefreshgrid();
				folderdatareload();
				emailtemplatesattaddgrid2();
				//addslideup('emailtemplatescreationview','emailtemplatescreationformadd');
				addslideup('mailmastercreationview','mailmastercreationformadd');
				$(".addbtnclass").removeClass('hidedisplay');
				$(".updatebtnclass").addClass('hidedisplay');
				emailtemplatesnamecheck();
				froalaset(froalaarray);
				cleargriddata('emailtemplatesattaddgrid2');
				editformdatainformationshow(datarowid);
				$('#emaillisttypeid,#emailcampaigntypeid,#moduleid').attr('readonly',true);
				$("#formfields").empty();
				$("#crmfileinfoid").val('0');
				$("#templatetypeid").val('3');
				$("#emailtemplatesattingriddel2").removeClass('forhidereloadbtn');
				$(".forhidereloadbtn").click(function(){
					$("#formclearicon").hide();
				});
				$('#tab2').trigger('click');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				fortabtouch = 1;
				Materialize.updateTextFields();
				Operation = 0; //for pagination
			} else { 
				alertpopup("Please select a row");
			}
		});
		$("#reloadicon").click(function(){
			refreshgrid();
		});
		$( window ).resize(function() {
			maingridresizeheightset('mailcreategrid');
			setTimeout(function(){
			sectionpanelheight('emailtemplatesattoverlay');
			sectionpanelheight('emailtemplatesfoloverlay');
			},50);
		});
		$("#emailtemplatesattingriddel2").click(function(){
			var datarowid = $('#emailtemplatesattaddgrid2 div.gridcontent div.active').attr('id');
			if(datarowid) {		
				if(METHOD == 'UPDATE') {
					alertpopup("A Record Under Edit Form");
				} else {
					/*delete grid data*/
					deletegriddatarow('emailtemplatesattaddgrid2',datarowid);
				}
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	{//Folder option
		$("#emailtemplatesfoladdbutton").click(function() {
			$("#foldernamename").focusout();
			$('#emailtemplatesfoladdgrid1validation').validationEngine('validate');
			Materialize.updateTextFields();
		});
		$('#emailtemplatesfoladdgrid1validation').validationEngine({
			onSuccess: function() {
				emailtemplatesfolderinsertfun();
				Materialize.updateTextFields();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//Mail Master folder data fetch
		$("#emailtemplatesfolingridedit1").click(function() {
			var datarowid = $('#emailtemplatesfoladdgrid1 div.gridcontent div.active').attr('id');
			if (datarowid) {
				//resetFields();
				sectionpanelheight('emailtemplatesfoloverlay');
				$("#emailtemplatesfoloverlay").removeClass("closed");
				$("#emailtemplatesfoloverlay").addClass("effectbox");
				$("#emailtemplatesfolupdatebutton").show().removeClass('hidedisplayfwg');
				$("#emailtemplatesfoladdbutton,#emailtemplatesfolingridedit1").hide();
				emailtemplatesfolderdatafetch(datarowid);
				Materialize.updateTextFields();
				firstfieldfocus();
			} else {
				alertpopup('Please select a row');
			}
		});
		//Mail Master update
		$("#emailtemplatesfolupdatebutton").click(function() {
			$("#foldernamename").focusout();
			$('#emailtemplatesfoleditgrid1validation').validationEngine('validate');
			setTimeout(function(){
				Materialize.updateTextFields();
				firstfieldfocus();
			},100);
		});
		$('#emailtemplatesfoleditgrid1validation').validationEngine({
			onSuccess: function() {
				emailtemplatesfolderupdate();
				$("#emailtemplatesfolingridedit1,#emailtemplatesfolingriddel1").show();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//Mail Master folder delete
		$("#emailtemplatesfolingriddel1").click(function() {
			var datarowid = $('#emailtemplatesfoladdgrid1 div.gridcontent div.active').attr('id');
			if (datarowid) {
				deletefolderoperation(datarowid);
			} else {
				alertpopup('Please select a row');
			}
		});
		//folder reload
		$("#emailtemplatesfolingridreload1").click(function() {
			//resetFields();
			$("#foldernamename").val('');
			$("#description").val('');
			$("#setdefaultcboxid").attr('checked',false);
			$("#publiccboxid").attr('checked',false);
			$("#setdefault").val('No');
			$("#public").val('No');
			folderrefreshgrid();
			$("#emailtemplatesfolupdatebutton").hide().addClass('hidedisplayfwg');
			$("#emailtemplatesfoladdbutton,#emailtemplatesfolingriddel1").show();
			Materialize.updateTextFields();
		});
	}
	{//validation for Company Add  
		$('#dataaddsbtn').click(function() {
			$('#tab1').trigger('click');
			$("#foldernamename").removeClass('validate[required,maxSize[100],funcCall[foldernamecheck]]');
			$("#foldernameid").removeClass('validate[required]');
			$("#formaddwizard").validationEngine('validate');
		});
		jQuery("#formaddwizard").validationEngine({
			onSuccess: function() {
				newdataaddfun();
			},
			onFailure: function() {
				var dropdownid =['3','emailfoldernameid','emailcampaigntypeid','emaillisttypeid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
				$("#foldernamename").removeClass('validate[required,maxSize[100],funcCall[foldernamecheck]]');
				$("#foldernameid").removeClass('validate[required]');
			}
		});
	}
	{// Redirected form Home
		var fromhomevalue = sessionStorage.getItem("emailtemplatesaddsrc");    
		if(fromhomevalue == 'formhome'){
			setTimeout(function(){ 
			$("#addicon").trigger('click');
			sessionStorage.removeItem("emailtemplatesaddsrc");},100);
		}
	}
	{//update company information
		$('#dataupdatesubbtn').click(function() {
			$('#tab1').trigger('click');
			$("#foldernamename").removeClass('validate[required,maxSize[100],funcCall[foldernamecheck]]');
			$("#foldernameid").removeClass('validate[required]');
			$("#formeditwizard").validationEngine('validate');
		});
		jQuery("#formeditwizard").validationEngine({
			onSuccess: function() {
				dataupdateinformationfun();
			},
			onFailure: function() {
				var dropdownid =['1','emailfoldernameid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
				$("#foldernamename").removeClass('validate[required,maxSize[100],funcCall[foldernamecheck]]');
				$("#foldernameid").removeClass('validate[required]');
			}	
		});
	} 
	//module change events
	$('#moduleid').change(function() {
		var id = $(this).val();
		if(id != "") {
			relatedtodropdownload(id);
			$("#relatedtomoduleid,#formfields").select2('val','');
			$("#mergetext").val('');
		} else {
			$("#relatedtomoduleid,#formfields").select2('val','');
			$("#mergetext").val('');
		}
	});
	//Relatedto change events
	$('#relatedtomoduleid').change(function() {
		var relatedmoduleid = $(this).val();
		var moduleid = $('#moduleid').val();
		if(relatedmoduleid != "") {
			$("#formfields").select2('val','');
			$("#mergetext").val('');
			loadmodulefield(moduleid,relatedmoduleid);
		} else {
			$("#formfields").select2('val','');
			$("#mergetext").val('');
		}
	});
	//module fields change events
	$('#formfields').change(function() {
		if( $(this).val()!= "" ) {
			var parmodid = $('#moduleid').val();
			var fildmodid = $('#relatedtomoduleid').val();
			var fildmodtype = $('#relatedtomoduleid').find('option:selected').data('mergemoduletype');
			if(parmodid!='' && fildmodid!='') {
				var columnid=$(this).find('option:selected').data('id');
				var columnname=$(this).find('option:selected').data('colname');
				var mtext = '{'+columnid+'.'+columnname+'}';
				$('#mergetext').val(mtext);
			}
		} else {
			$('#mergetext').val('');
		}
		Materialize.updateTextFields();
	});
	{//Function For Check box
		$('.checkboxcls').click(function() {
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
	{//dynamic form to grid
		griddataid = 0;
		gridynamicname = '';
		var gridname = $('#gridnameinfo').val();
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		$('#emailtemplatesattaddbutton,#emailtemplatesattupdatebutton').click(function(){
			if($("#filenameattachdisplay").is(':empty')){
				alertpopup('Please Upload File...');
			} else {
				var i = $(this).data('frmtogriddataid');
				var gridname = $(this).data('frmtogridname');
				gridformtogridvalidatefunction(gridname);
				griddataid = i;
				gridynamicname = gridname;
				$("#"+gridname+"validation").validationEngine('validate');
				METHOD = 'ADD';
			}
		});
	}
	$('#printpreviewicon').click(function(){
		var datarowid = $('#mailcreategrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			$('#previewoverlayoverlay').fadeIn();
			$.ajax({
				url:base_url+'Mailmaster/templatecontentpreview',
				data:'datas=&templateid='+datarowid,
				type:'POST',
				cache:false,
				success: function(content) {
					var regExp = /a10s/g;
					var datacontent = content.match(regExp);
					if(datacontent !== null){	
						for (var i = 0; i < datacontent.length; i++) {
							content = content.replace(''+datacontent[i]+'','&nbsp;');
						}
					}
					$('#templatepreviewframe').attr('srcdoc',content);
				},
			});
		} else {
			alertpopup('Please select a row');
		}
	});
	$('#previewclose').click(function(){
		$('#previewoverlayoverlay').fadeOut();
	});
	//template type change
	$("#emaillisttypeid").change(function() {
		var typeid = $("#emaillisttypeid").val();
		$("#relatedtomoduleid,#formfields").empty();
		$("#relatedtomoduleid,#formfields").select2('val','');
		$("#mergetext,#emailtemplatesemailtemplate_editor").val('');
		if(typeid == 2) {
			$("#moduleid").select2('val',270);
			$("#moduleid").trigger('change');
			$("#moduleid,#mergetext").attr('readonly',true);
			$("#relatedtomoduleid").attr('readonly',true);
		} else if(typeid == 3) {
			var mid = $("#moduleid").val();
			if(mid == '270'){
				$("#moduleid").select2('val','');
				$("#moduleid,#relatedtomoduleid").attr('readonly',false);
			} else {
				$("#moduleid,#relatedtomoduleid").attr('readonly',false);
			}
		} else if(typeid == 4) {
			$("#moduleid").select2('val','');
			$("#moduleid").attr('readonly',true);
		}
	});
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,mailcreategrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			mailcreategrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
{// Main Grid Function
	function mailcreategrid(page,rowcount) {
		if(Operation == 1){
			var rowcount = $('#emailtemplatespgrowcount').val();
			var page = $('.paging').data('pagenum');
		} else{
			var rowcount = $('#emailtemplatespgrowcount').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		}
		var wwidth = $('#mailcreategrid').width();
		var wheight = $(window).height();
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.emailtemplatesheadercolsort').hasClass('datasort') ? $('.emailtemplatesheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.emailtemplatesheadercolsort').hasClass('datasort') ? $('.emailtemplatesheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.emailtemplatesheadercolsort').hasClass('datasort') ? $('.emailtemplatesheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = $('#viewfieldids').val();
		var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
		var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
		var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=emailtemplates&primaryid=emailtemplatesid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#mailcreategrid').empty();
				$('#mailcreategrid').append(data.content);
				$('#mailcreategridfooter').empty();
				$('#mailcreategridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('mailcreategrid');
				{//sorting
					$('.emailtemplatesheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.emailtemplatesheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#mailmasterpgnumcnt li .page-text .active').data('rowcount');
						var sortcol = $('.emailtemplatesheadercolsort').hasClass('datasort') ? $('.emailtemplatesheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.emailtemplatesheadercolsort').hasClass('datasort') ? $('.emailtemplatesheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						var sortpos = $('#mailcreategrid .gridcontent').scrollLeft();
						mailcreategrid(page,rowcount);
						$('#mailcreategrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('emailtemplatesheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						Operation = 0;
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						mailcreategrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#emailtemplatespgrowcount').change(function(){
						Operation = 0;
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = 1;
						mailcreategrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//onclick 
				$(".gridcontent").click(function(){
					var datarowid = $('#mailcreategrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				//Material select
				$('#emailtemplatespgrowcount').material_select();
			},
		});
	}
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function(e){
			resetFields();
			folderrefreshgrid();
			folderdatareload();
			emailtemplatesattaddgrid2();
			e.preventDefault();
			var datarowid = $(this).data('rowid');
			addslideup('mailmastercreationview','mailmastercreationformadd');
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			froalaset(froalaarray);
			$("#formfields").empty();
			$("#crmfileinfoid").val('0');
			$("#templatetypeid").val('3');
			$("#emailtemplatesattingriddel2").removeClass('forhidereloadbtn');
			$('#tab1').trigger('click');
			$('#tab2').trigger('click');
			setTimeout(function() {
				//transtion 
				$('#subformspan2').addClass('form-active');
			},100);
			
			$("#formclearicon").show();
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			fortabtouch = 0;
			setTimeout(function() {
				clearformgriddata();
				$("#emailfoldernameid").removeClass('readonly');
				$('#leadtemplatename').attr('readonly',true);  
				$('#emaillisttypeid,#emailcampaigntypeid').attr('readonly',false); 
				$("#primarydataid").val('');
				cleargriddata('emailtemplatesattaddgrid2');
				$("#filenameattachdisplay").empty();
			},100);
			firstfieldfocus();
			Materialize.updateTextFields();
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
		});
		$("#editicon").click(function(e){
			e.preventDefault();
			var datarowid = $('#mailcreategrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#tab2').trigger('click');
				cleargriddata('emailtemplatesattaddgrid2');
				emailtemplatesattaddgrid2();
				setTimeout(function(){
					addslideup('mailmastercreationview','mailmastercreationformadd');
					$(".addbtnclass").addClass('hidedisplay');
					$(".updatebtnclass").removeClass('hidedisplay');
					$("#emailtemplatesattaddbutton").removeClass('hidedisplay');
					froalaset(froalaarray);
					$('#emaillisttypeid,#emailcampaigntypeid,#moduleid').attr('readonly',true);
					editformdatainformationshow(datarowid);
					$("#formfields").empty();
					$("#crmfileinfoid").val('0');
					$("#templatetypeid").val('3');
					$("#emailtemplatesattingriddel2").addClass('forhidereloadbtn');
					$(".forhidereloadbtn").click(function(){
						$("#formclearicon").hide();
					});
					//For tablet and mobile Tab section reset
					$('#tabgropdropdown').select2('val','1');
					fortabtouch = 1;
					Materialize.updateTextFields();
					Operation = 1; //for pagination
				},100);
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#deleteicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#mailcreategrid div.gridcontent div.active').attr('id');
			if(datarowid){		
				$("#basedeleteoverlay").fadeIn();
				$('#primarydataid').val(datarowid);
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}	
		});
		$("#basedeleteyes").click(function() {
			var datarowid = $('#primarydataid').val(); 
			recorddelete(datarowid);
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#mailmasterpgnum li.active').data('pagenum');
		var rowcount = $('ul#mailmasterpgnumcnt li .page-text .active').data('rowcount');
		mailcreategrid(page,rowcount);
	}
	function folderrefreshgrid() {
		var page = $('ul#folderlistpgnum li.active').data('pagenum');
		var rowcount = $('ul.folderlistpgnumcnt li .page-text .active').data('rowcount');
		emailtemplatesfoladdgrid1(page,rowcount);
	}
}
//MailMaster folder  Grid
function emailtemplatesfoladdgrid1(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 1000 : rowcount;
	var wwidth = $('#emailtemplatesfoladdgrid1').width();
	var wheight = $('#emailtemplatesfoladdgrid1').height();
	//col sort
	var sortcol = $('.folderlistheadercolsort').hasClass('datasort') ? $('.folderlistheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.folderlistheadercolsort').hasClass('datasort') ? $('.folderlistheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.folderlistheadercolsort').hasClass('datasort') ? $('.folderlistheadercolsort.datasort').attr('id') : '0';
	$.ajax({
		url:base_url+"Mailmaster/mailmasterfoldernamegridfetch?maintabinfo=foldername&primaryid=foldernameid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=209',
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#emailtemplatesfoladdgrid1').empty();
			$('#emailtemplatesfoladdgrid1').append(data.content);
			$('#emailtemplatesfoladdgrid1footer').empty();
			$('#emailtemplatesfoladdgrid1footer').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('emailtemplatesfoladdgrid1');
			{//sorting
				$('.folderlistheadercolsort').click(function(){
					$('.folderlistheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('ul#folderlistpgnum li.active').data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					emailtemplatesfoladdgrid1(page,rowcount);
				});
				sortordertypereset('smsmasterheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					emailtemplatesfoladdgrid1(page,rowcount);
				});	
				$('#folderlistpgrowcount').change(function(){
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('.pvpagnumclass').data('pagenum');
					emailtemplatesfoladdgrid1(page,rowcount);
				});
			}
			//Material select
			$('#folderlistpgrowcount').material_select();
		},
	});
}
//new data add submit function
function newdataaddfun() {
	var amp = '&';
	//editor data
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditoradd(editorname);
	var gridname = $('#gridnameinfo').val();
	var gridnames = gridname.split(',');
	var datalength = gridnames.length;
	var noofrows=0;
	var addgriddata='';
	if(deviceinfo != 'phone'){
		for(var j=0;j<datalength;j++) {
			if(j!=0) {
				addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
				noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;;
			} else {
				addgriddata = getgridrowsdata(gridnames[j]);
				noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;;
			}
		}
	}else{
		for(var j=0;j<datalength;j++) {
			if(j!=0) {
				addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
				noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent div').length;;
			} else {
				addgriddata = getgridrowsdata(gridnames[j]);
				noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent div').length;;
			}
		}
	}
	var sendformadddata = JSON.stringify(addgriddata);
	var griddatapartabnameinfo = $('#griddatapartabnameinfo').val();
	var formdata = $("#dataaddform").serialize();
	var datainformation = amp + formdata;
	if(editordata != ""){
		$.ajax({
			url: base_url + "Mailmaster/newdatacreate",
			data: "datas=" + datainformation+"&griddatas="+sendformadddata+"&numofrows="+noofrows+"&griddatapartabnameinfo="+griddatapartabnameinfo,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE'){
					resetFields();
					$('#mailmastercreationformadd').hide();
					$('#mailmastercreationview').fadeIn(1000);
					clearformgriddata();
					refreshgrid();
					froalaset(froalaarray);
					alertpopup(savealert);
				} 
			},
		});
	} else {
		alertpopup('Please enter the values in template editor');
	}
}
{// Clear grid data
	function clearformgriddata() {
		var gridname = $('#gridnameinfo').val();
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		for(var j=0;j<datalength;j++) {
			cleargriddata(gridnames[j]);
		}
	}
}
//old information show in form
function editformdatainformationshow(datarowid) {
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementpartable = $('#elementspartabname').val();
	var resctable = $('#resctable').val();
	$.ajax(	{
		url:base_url+"index.php/Mailmaster/fetchformdataeditdetails?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable+"&resctable="+resctable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data)	{
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				$("#closeaddform").trigger('click');
				$('#mailmastercreationformadd').hide();
				$('#mailmastercreationview').fadeIn(1000);
				//For Touch
				smsmasterfortouch = 0;
			} else{ 
				var txtboxname = 'emailfoldernameid,emailtemplatesname,emailcampaigntypeid,subject,emaillisttypeid,addonsignature,emaildescription,moduleid,emailtemplatesemailtemplate_editorfilename,primarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
				var filename =data['emailtemplatesemailtemplate_editorfilename'];
				filenamevaluefetch(filename);
				setTimeout(function(){
					$("#moduleid").trigger('change');
					$("#subject").text(data['subject']);
				}, 500);
				emaildocumentdetail(datarowid);
				//For Keyboard Shortcut Variables
				addformupdate = 1;
				viewgridview = 0;
				addformview = 1;
			}
		}
	});
}
function emaildocumentdetail(docid) {
	if(docid!='') {
		$.ajax({
			url:base_url+'Mailmaster/emaildocumentdetailfetch?primarydataid='+docid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					loadinlinegriddata('emailtemplatesattaddgrid2',data.rows,'json');
					/* data row select event */
					datarowselectevt();
					/* column resize */
					columnresize('emailtemplatesattaddgrid2');
					var hideprodgridcol = ['crmfileinfoid','filename_fromid','filename_path'];
				}
			},
		});
	}
}
//editor value fetch
function filenamevaluefetch(filename) {
	$.ajax({
		url: base_url + "Mailmaster/editervaluefetch?filename="+filename,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			froalaedit(data,'emailtemplatesemailtemplate_editor');
		},
	});
}
//update old information
function dataupdateinformationfun() {
	var amp = '&';
	var gridname = $('#gridnameinfo').val();
	var gridnames = gridname.split(',');
	var datalength = gridnames.length;
	var noofrows=0;
	var addgriddata='';
	if(deviceinfo != 'phone'){
		for(var j=0;j<datalength;j++) {
			if(j!=0) {
				addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
				noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;;
			} else {
				addgriddata = getgridrowsdata(gridnames[j]);
				noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;;
			}
		}
	}else{
		for(var j=0;j<datalength;j++) {
			if(j!=0) {
				addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
				noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent div').length;;
			} else {
				addgriddata = getgridrowsdata(gridnames[j]);
				noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent div').length;;
			}
		}
	}	
	var sendformadddata = JSON.stringify(addgriddata);
	//editor data
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditoradd(editorname);
	var formdata = $("#dataaddform").serialize();
	var datainformation = amp + formdata;
	if(editordata != ""){
		$.ajax({
			url: base_url + "Mailmaster/datainformationupdate",
			data: "datas=" + datainformation+"&griddatas="+sendformadddata+amp+"numofrows="+noofrows,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					resetFields();
					$(".ftab").trigger('click');
					$('#mailmastercreationformadd').hide();
					$('#mailmastercreationview').fadeIn(1000);
					refreshgrid();
					clearformgriddata();
					$("#primarydataid").val('');
					froalaset(froalaarray);
					alertpopup(savealert);
				}
			},
		});
	} else {
		alertpopup('Please enter the values in template editor');
	}
	setTimeout(function() {
		var closetrigger = ["closeaddoppocreation"];
		triggerclose(closetrigger);
	}, 1000);
}
//delete old information
function recorddelete(datarowid) {
	var elementstable = $('#elementstable').val();
	var elementspartable = $('#elementspartabname').val();
    $.ajax({
        url: base_url + "index.php/Mailmaster/datainformationdata?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				mailcreategrid();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Deleted successfully');
            } else if (nmsg == "Denied")  {
				mailcreategrid();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Permission denied');
            }
        },
    });
}
//drop down values set for view
	function smsdropdownmodulevalset(ddname,dataattrname,otherdataattrname1,otherdataattrname2,dataname,dataid,otherdataname1,otherdataname2,datatable,whfield,whvalue) {
	$('#'+ddname+'').empty();
	$('#'+ddname+'').append($("<option></option>"));
	$.ajax({
		url:base_url+'Mailmaster/fetchmaildddatawithmultiplecond?dataname='+dataname+'&dataid='+dataid+'&datatab='+datatable+'&whdatafield='+whfield+'&whval='+whvalue+'&othername1='+otherdataname1+'&othername2='+otherdataname2,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#'+ddname+'')
					.append($("<option></option>")
					.attr("data-"+dataattrname,data[index]['datasid'])
					.attr("data-"+otherdataattrname1,data[index]['otherdataattrname1'])
					.attr("data-"+otherdataattrname2,data[index]['otherdataattrname2'])
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}	
			$('#'+ddname+'').trigger('change');
		},
	});
}
// Main view Grid
function emailtemplatesattaddgrid2() {
	//GRA to restrict grid reload and take width of hidden grid and set time out for chrome script loading soon issue
	setTimeout(function(){
	$('#emailtemplatescreationformadd').css('visibility','hidden').show();
	$('#emailtemplatescreationformadd #subformspan2').css('display', 'block !important').show();
	var wwidth = $('#emailtemplatescreationformadd #emailtemplatesattaddgrid2').width();
	$('#emailtemplatescreationformadd #subformspan2').css('display', 'block !important').hide();
	$('#emailtemplatescreationformadd').css('visibility','visible').hide();
	var wheight = $("#emailtemplatesattaddgrid2").height();
	$.ajax({
		url:base_url+"Mailmaster/localgirdheaderinformationfetch?tabgroupid=99&moduleid=209&width="+wwidth+"&height="+wheight+"&modulename=mailattach",
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#emailtemplatesattaddgrid2").empty();
			$("#emailtemplatesattaddgrid2").append(data.content);
			$("#emailtemplatesattaddgrid2footer").empty();
			$("#emailtemplatesattaddgrid2footer").append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('emailtemplatesattaddgrid2');
			var hideprodgridcol = ['crmfileinfoid','filename_fromid','filename_path'];
			gridfieldhide('emailtemplatesattaddgrid2',hideprodgridcol);
		},
	});
	},50);
}
function gridformtogridvalidatefunction(gridnamevalue) {
	jQuery("#"+gridnamevalue+"validation").validationEngine({
		onSuccess: function() {  
			var i = griddataid;
			var gridname = gridynamicname;
			var coldatas = $('#gridcolnames'+i+'').val();
			var columndata = coldatas.split(',');
			var coluiatas = $('#gridcoluitype'+i+'').val();
			var columnuidata = coluiatas.split(',');
			var datalength = columndata.length;
			/*Form to Grid*/
			if(METHOD == 'ADD' || METHOD == '') {
				formtogriddata(gridname,METHOD,'last','');
			} else if(METHOD == 'UPDATE') {
				var selectedrow = $('#'+gridname+' div.gridcontent div.active').attr('id');
				formtogriddata(gridname,METHOD,'',selectedrow);
			}
			/* Hide columns */
			var hideprodgridcol = ['crmfileinfoid','filename_fromid','filename_path'];
			/* Data row select event */
			datarowselectevt();
			clearform('gridformclear');
			$("#keywords").select2('val','');
			setTimeout(function(){
				$("#emailtemplatesattcancelbutton").trigger('click');
				fname_1060 = [];
				fsize_1060 = [];
				ftype_1060 = [];
				fpath_1060 = [];
				$("#filename_size").val('');
				$("#filename_type").val('');
				$("#filename_path").val('');
				$("#filename").val('');
				$("#filename").empty('');
				$("#crmfileinfoid").val('0');
				$('#filenameattachdisplay').empty();
			},100);
		},
		onFailure: function(){
			var dropdownid =['2','foldernameid','employeeid'];
			dropdownfailureerror(dropdownid);
		}
	});
}
{/* clear form */
	function clearformsol(cleardivid) {
		setTimeout(function() {
			$('#'+cleardivid+'').validationEngine('hideAll');
			$('#'+cleardivid+' .select2-choice').removeClass('error');
			$('#'+cleardivid+' :input').removeClass('error');			
			$(':input','#'+cleardivid)
			.not(':button, :submit, :reset, :hidden')
			.val('')
			.removeAttr('checked')
			.removeAttr('selected');
			$('#'+cleardivid+' input[type=hidden]').val('');
			$('#'+cleardivid+' textarea').val('');
			$('#'+cleardivid+' .chzn-select').select2('val','');
			$('#'+cleardivid+' .captionddse').select2('val','');
			$('#'+cleardivid+' .commenttxtarea').val('');
			$('#'+cleardivid+' .overlay .cctxt').val('');
		},50);
		$('#filenameattachdisplay').empty();
		$("#crmfileinfoid").val('0');		
	}	
}
{//Related modules drop down load 
	function relatedtodropdownload(id) {
		var modulename=$('#moduleid').find('option:selected').data('moduleidhidden');
		$('#relatedtomoduleid').empty();
		$('#relatedtomoduleid').append($("<option></option>"));
		$.ajax({
			url:base_url+'Mailmaster/relatedmoduleidget',
			data:'moduleid='+id,
			type:'POST',
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					$('#relatedtomoduleid').append($("<option></option>").attr("value",id).text(modulename));
					$.each(data, function(index) {
						$('#relatedtomoduleid')
						.append($("<option></option>")
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
					});
				}	
			},
		});
	}
}
//merge modulebased form fields load
function loadmodulefield(moduleid,relatedmoduleid){
	$('#formfields').empty();
	$('#formfields').append($("<option></option>"));
	$.ajax({
			url: base_url+"Mailmaster/modulefieldload",
			data:{moduleid:moduleid,relatedmoduleid:relatedmoduleid},
			dataType:'json',
			type: "POST",
			async:false,
			success: function(data) {	
				dropdownid='formfields';
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$("#"+dropdownid+"")
						.append($("<option></option>")
						.attr("data-key",data[index]['key'])
						.attr("value",data[index]['pid'])
						.attr("data-id",data[index]['pid'])
						.attr("data-colname",data[index]['label'])
						.text(data[index]['label']));
					});
				}	
			},
		});
}
{//folder operation
	function emailtemplatesfolderinsertfun() {
		var foldername = $("#foldernamename").val();
		var description = $("#description").val();
		var setdefault = $("#setdefault").val();
		var ppublic = $("#public").val();
		$.ajax({
			url:base_url+'Mailmaster/mailmasterfolderinsert',
			data:'foldername='+foldername+'&description='+description+'&setdefault='+setdefault+'&ppublic='+ppublic,
			type:'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					$("#foldernamename,#description").val('');
					$("#publiccboxid,#setdefaultcboxid").prop('checked',false);
					cleargriddata('emailtemplatesfoladdgrid1');
					$('#emailtemplatesfolupdatebutton').hide();
					$('#emailtemplatesfoladdbutton').show();
					folderrefreshgrid();
					folderdatareload();
					$("#emailtemplatesfolcancelbutton").trigger('click');
					alertpopup(savealert);
					Materialize.updateTextFields();
				}
			},
		});
	}
	//Edit data Fetch
	function emailtemplatesfolderdatafetch(datarowid) {
		$("#foldernameeditid").val(datarowid);
		$.ajax({
			url:base_url+'Mailmaster/mailmasterfolderdatafetch?datarowid='+datarowid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					var foldername = data.foldername;
					var description = data.description;
					$("#foldernamename").val(foldername);
					$("#description").val(description);
					var setdefault = data.setdefault;
					if(setdefault == 'Yes'){
						$("#setdefaultcboxid").prop('checked',true);
						$("#setdefault").val('Yes');
					} else {
						$("#setdefaultcboxid").prop('checked',false);
						$("#setdefault").val('No');
					}
					var ppublic = data.ppublic;
					if(ppublic == 'Yes'){
						$("#publiccboxid").prop('checked',true);
						$("#public").val('Yes');
					} else {
						$("#publiccboxid").prop('checked',false);
						$("#public").val('No');
					}
					$("#foldernameeditid").val(datarowid);
				}
			},
		});
	}
	//SMS templates folder add function
	function emailtemplatesfolderupdate(){
		var foldernameid = $("#foldernameeditid").val();
		var foldername = $("#foldernamename").val();
		var description = $("#description").val();
		var setdefault = $("#setdefault").val();
		var ppublic = $("#public").val();
		$.ajax({
			url:base_url+'Mailmaster/mailmasterfolderupdate',
			data:'foldername='+foldername+'&description='+description+'&setdefault='+setdefault+"&foldernameid="+foldernameid+'&ppublic='+ppublic,
			type:'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					$("#foldernamename,#description").val('');
					$("#publiccboxid,#setdefaultcboxid").prop('checked',false);
					folderrefreshgrid();
					cleargriddata('emailtemplatesfoladdgrid1');
					folderrefreshgrid();
					$('#dataaddsbtn').attr('disabled',false);
					folderdatareload();
					$("#emailtemplatesfoladdbutton,#emailtemplatesfolingriddel1").show();
					$("#emailtemplatesfolupdatebutton").hide();
					$("#emailtemplatesfolcancelbutton").trigger('click');
					alertpopup(savealert);
				}
			},
		});
	}
	//delete folder 
	function deletefolderoperation(id){
		$.ajax({
			url:base_url+'Mailmaster/folderdeleteoperation?id='+id,
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					folderrefreshgrid();
					$('#emailtemplatesfolingriddel1').attr('disabled',false);
					alertpopup("Deleted successfully");
				} else if(nmsg == 'DEFAULT') {
					alertpopup("Can't delete default records.");
					folderrefreshgrid();
					$('#solutionsfolingriddel1').attr('disabled',false);
				}
			},
		});
	}
	//Folder Drop Down load
	function folderdatareload() {
		$('#foldernameid,#emailfoldernameid').empty();
		$('#foldernameid,#emailfoldernameid').append($("<option></option>"));
		$.ajax({
			url:base_url+'Mailmaster/fetchdddataviewddval?dataname=foldernamename&dataid=foldernameid&datatab=foldername&moduleid=209',
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#foldernameid,#emailfoldernameid')
						.append($("<option></option>")
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
					});
					$.each(data, function(index) {
						if(data[index]['setdefault'] == 'Yes'){
							$('#foldernameid,#emailfoldernameid').select2('val',data[index]['datasid']).trigger('change');
						}
					});
				}
			},
		});
	}
}
//Email Template name check
function emailtemplatesnamecheck() {
	var emailtemplatesname = $('#emailtemplatesname').val();
	var primaryid = $("#primarydataid").val();
	var nmsg = "";
	if( emailtemplatesname !="" ) {
		$.ajax({
			url:base_url+"Mailmaster/emailtemplatesnameunique",
			data:"data=&emailtemplatesname="+emailtemplatesname+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Email Template already exists!";
				}
			} else {
				return "Email Template already exists!";
			}
		} 
	}
}
// foldernamecheck
function foldernamecheck() {
	var primaryid = $("#foldernameeditid").val();
	var accname = $("#foldernamename").val();
	var elementpartable = 'foldername';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Mailmaster/folderuniquenamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Folder name already exists!";
				}
			}else {
				return "Folder name already exists!";
			}
		} 
	}
}