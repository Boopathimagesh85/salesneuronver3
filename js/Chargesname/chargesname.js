$(document).ready(function() {	
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}	
	{//Grid Calling
		chargesnameaddgrid();
		firstfieldfocus();
		chargesnamecrudactionenable();
	}
	{//view by drop down change
		$('#dynamicdddataview').change(function(){	
			chargesnameaddgrid();
		});
	}
	//hidedisplay
	$('#chargesnamedataupdatesubbtn').hide();
	$('#chargesnamesavebutton').show();
	$("#istaxable").val('No');
	$('#calculationtypeid').change(function(){	
		var val=$(this).val();
		if(val == 3){
			var rate= parseFloat($('#value').val());
			if(rate > 100){
				$('#value').val(0);
			}
		}
	});
	{
		//keyboard shortcut reset global variable
		viewgridview=0;
	}
	{//Load the Data
		$('#cumulativetaxid').empty();
		$('#cumulativetaxid').append($("<option></option>"));
		$.ajax({
			url:base_url+'Chargesname/gettax',
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED'){
				} else {
					var newddappend="";
					var newlength=data.length;
					for(var m=0;m < newlength;m++) {
				newddappend += "<option value = '" +data[m]['id']+ "'>"+data[m]['name']+'('+data[m]['rate']+ "%)</option>";
					}
					//after run					
					$('#cumulativetaxid').append(newddappend);
				}		
			},
		});
	}
	{//validation for Chargename Add  
		$('#chargesnamesavebutton').click(function(e) {
			if(e.which == 1 || e.which === undefined) {
				var istax = $("#istaxable").val();
				var iscum = $("#cumulativetaxid").val();
				if((istax == 'Yes') && (iscum)){
					$("#chargesnameformaddwizard").validationEngine('validate');
					// For Touch
					masterfortouch = 0;	
				} else if(istax == 'No') {
					$("#chargesnameformaddwizard").validationEngine('validate');
					// For Touch
					masterfortouch = 0;	
				} else {
					alertpopup('Select Tax');
				}
				
			}	
		});
		jQuery("#chargesnameformaddwizard").validationEngine( {
			onSuccess: function() {
				$('#chargesnamesavebutton').attr('disabled','disabled');
				addchargedetailaddformdata();
			},
			onFailure: function() {
				var dropdownid =['2','additionalchargecategoryid','calculationtypeid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		$("#chargesnamereloadicon").click(function(){
			clearform('cleardataform');
			chargesnamerefreshgrid();
			$('#chargesnamedataupdatesubbtn').hide();
			$('#chargesnamesavebutton').show();
			$("#calculationtypeid,#taxmodeid").select2("readonly", false);
		});
		$( window ).resize(function() {
			innergridresizeheightset('chargesnameaddgrid');
			sectionpanelheight('chargesnamesectionoverlay');
		});
	}
	{//update Chargename information
		$('#chargesnamedataupdatesubbtn').click(function(e) {
			if(e.which == 1 || e.which === undefined) {
				var istax = $("#istaxable").val();
				var iscum = $("#cumulativetaxid").val();
				if((istax == 'Yes') && (iscum)){
					alertpopup('Select Tax');
				}else{
					$("#chargesnameformeditwizard").validationEngine('validate');
					// For Touch
					masterfortouch = 0;	
				}
			}
		});
		jQuery("#chargesnameformeditwizard").validationEngine({
			onSuccess: function(){
				addchargedetailupdateformdata();
			},
			onFailure: function(){
				var dropdownid =['2','additionalchargecategoryid','calculationtypeid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	{//Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');			
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
				if(name == 'istaxable'){
					$('#cumulativetaxiddivhid').show(); //hide the tax if its only applicable
				}		
			} else {
				$('#'+name+'').val('No');
				if(name == 'istaxable'){
					$('#cumulativetaxiddivhid').hide(); //hide the tax if its only applicable
				}
			}		
		});
	}
	$('#cumulativetaxiddivhid').hide(); //hide the tax if its only applicable
	{//filter work
		//for toggle
		$('#chargesnameviewtoggle').click(function() {
			if ($(".chargesnamefilterslide").is(":visible")) {
				$('div.chargesnamefilterslide').addClass("filterview-moveleft");
			}else{
				$('div.chargesnamefilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#chargesnameclosefiltertoggle').click(function(){
			$('div.chargesnamefilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#chargesnamefilterddcondvaluedivhid").hide();
		$("#chargesnamefilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#chargesnamefiltercondvalue").focusout(function(){
				var value = $("#chargesnamefiltercondvalue").val();
				$("#chargesnamefinalfiltercondvalue").val(value);
				$("#chargesnamefilterfinalviewconid").val(value);
			});
			$("#chargesnamefilterddcondvalue").change(function(){
				var value = $("#chargesnamefilterddcondvalue").val();
				if( $('#s2id_chargesnamefilterddcondvalue').hasClass('select2-container-multi') ) {
					chargesnamemainfiltervalue=[];
					$('#chargesnamefilterddcondvalue option:selected').each(function(){
					    var $cnvalue =$(this).attr('data-ddid');
					    chargesnamemainfiltervalue.push($cnvalue);
						$("#chargesnamefinalfiltercondvalue").val(chargesnamemainfiltervalue);
					});
					$("#chargesnamefilterfinalviewconid").val(value);
				} else {
					$("#chargesnamefinalfiltercondvalue").val(value);
					$("#chargesnamefilterfinalviewconid").val(value);
				}
			});
		}
		chargesnamefiltername = [];
		$("#chargesnamefilteraddcondsubbtn").click(function() {
			$("#chargesnamefilterformconditionvalidation").validationEngine("validate");	
		});
		$("#chargesnamefilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(chargesnameaddgrid,'chargesname');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}	
}
//Documents Add Grid
function chargesnameaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#chargesnameaddgrid").width();
	var wheight = $("#chargesnameaddgrid").height();
	var viewname = $('#dynamicdddataview').val();
	$('#viewgridheaderid').text(viewname);
	//col sort
	var sortcol = $("#chargesnamesortcolumn").val();
	var sortord = $("#chargesnamesortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.chargesnameheadercolsort').hasClass('datasort') ? $('.chargesnameheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.chargesnameheadercolsort').hasClass('datasort') ? $('.chargesnameheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.chargesnameheadercolsort').hasClass('datasort') ? $('.chargesnameheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#chargesnameviewfieldids').val();
	if(userviewid != '') {
		userviewid= '72';
	}
	var filterid = $("#chargesnamefilterid").val();
	var conditionname = $("#chargesnameconditionname").val();
	var filtervalue = $("#chargesnamefiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=additionalchargetype&primaryid=additionalchargetypeid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#chargesnameaddgrid').empty();
			$('#chargesnameaddgrid').append(data.content);
			$('#chargesnameaddgridfooter').empty();
			$('#chargesnameaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('chargesnameaddgrid');
			{//sorting
				$('.chargesnameheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.chargesnameheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#chargesnamepgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.chargesnameheadercolsort').hasClass('datasort') ? $('.chargesnameheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.chargesnameheadercolsort').hasClass('datasort') ? $('.chargesnameheadercolsort.datasort').data('sortorder') : '';
					$("#chargesnamesortorder").val(sortord);
					$("#chargesnamesortcolumn").val(sortcol);
					var sortpos = $('#chargesnameaddgrid .gridcontent').scrollLeft();
					chargesnameaddgrid(page,rowcount);
					$('#chargesnameaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('chargesnameheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					chargesnameaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});	
				$('#chargesnamepgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					chargesnameaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#chargesnameaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#chargesnamepgrowcount').material_select();
		},
	});	
}
function chargesnamecrudactionenable() {
	{//add icon click
		$('#chargesnameaddicon').click(function(e){
			sectionpanelheight('chargesnamesectionoverlay');
			$("#chargesnamesectionoverlay").removeClass("closed");
			$("#chargesnamesectionoverlay").addClass("effectbox");
			e.preventDefault();
			resetFields();
			chargecategorydatareload();
			Materialize.updateTextFields();
			firstfieldfocus();
			$("#istaxable").val('No');
			$("#calculationtypeid,#taxmodeid").select2("readonly", false);
			$('#chargesnamedataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#chargesnamesavebutton').show();
			$('#cumulativetaxiddivhid').hide();
		});
	}
	$("#chargesnameediticon").click(function(e){
		e.preventDefault();
		var datarowid = $('#chargesnameaddgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			resetFields();
			$(".addbtnclass").addClass('hidedisplay');
			$(".updatebtnclass").removeClass('hidedisplay');
			addchargedetailgetformdata(datarowid);
			Materialize.updateTextFields();
			firstfieldfocus();
				// For Touch
			masterfortouch = 1;	
			//Keyboard shortcut
			saveformview = 1;
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#chargesnamedeleteicon").click(function(e){
		e.preventDefault();
		var datarowid = $('#chargesnameaddgrid div.gridcontent div.active').attr('id');
		if(datarowid){		
			combainedmoduledeletealert('chargesnamedeleteyes');
			$("#chargesnamedeleteyes").click(function(){
				var datarowid = $('#chargesnameaddgrid div.gridcontent div.active').attr('id');
				addchargedetailrecorddelete(datarowid);
				$(this).unbind();
			});
		} else {
			alertpopup("Please select a row");
		}
	});
}
{//refresh grid
	function chargesnamerefreshgrid() {
		var page = $('ul#chargesnamepgnum li.active').data('pagenum');
		var rowcount = $('ul#chargesnamepgnumcnt li .page-text .active').data('rowcount');
		chargesnameaddgrid(page,rowcount);
	}
}
//new data add submit function
function addchargedetailaddformdata() {
    var formdata = $("#chargesnameaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	var compoundon= $('#cumulativetaxid').val();
    $.ajax( {
        url: base_url + "Chargesname/newdatacreate",
        data: "datas=" + datainformation+"&compoundon="+compoundon,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	$(".addsectionclose").trigger("click");
				resetFields();
				chargesnamerefreshgrid();
				$("#chargesnamesavebutton").attr('disabled',false);
				$('#cumulativetaxiddivhid').hide(); //hide the tax if its only applicable				
				alertpopup(savealert);
            }
        },
    });
}
{//Addform section overlay close
	$(".addsectionclose").click(function(){
		resetFields();
		$("#chargesnamesectionoverlay").removeClass("effectbox");
		$("#chargesnamesectionoverlay").addClass("closed");
	});
}
//old information show in form
function addchargedetailgetformdata(datarowid) {
	var chargesnameelementsname = $('#chargesnameelementsname').val();
	var chargesnameelementstable = $('#chargesnameelementstable').val();
	var chargesnameelementscolmn = $('#chargesnameelementscolmn').val();
	var chargesnameelementspartabname = $('#chargesnameelementspartabname').val();
	$.ajax(	{
		url:base_url+"Chargesname/fetchformdataeditdetails?chargesnameprimarydataid="+datarowid+"&chargesnameelementsname="+chargesnameelementsname+"&chargesnameelementstable="+chargesnameelementstable+"&chargesnameelementscolmn="+chargesnameelementscolmn+"&chargesnameelementspartabname="+chargesnameelementspartabname, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				chargesnamerefreshgrid();
			} else {
				sectionpanelheight('chargesnamesectionoverlay');
				$("#chargesnamesectionoverlay").removeClass("closed");
				$("#chargesnamesectionoverlay").addClass("effectbox");
				var txtboxname = chargesnameelementsname + ',chargesnameprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = chargesnameelementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
				var id=data['primarydataid'];
				$("#chargesnameprimarydataid").val(id);
				//Select TAX on charges
				if(data.compoundon != '' && data.istaxable == 'Yes'){
					var vcselectid=data.compoundon;
					var selectid = vcselectid.split(',');
					selectid = jQuery.grep(selectid, function(n){ return (n); });
					$('#cumulativetaxid').select2('val',selectid).trigger('change');
					$('#cumulativetaxiddivhid').show(); //hide the tax if its only applicable
				}
				$("#calculationtypeid,#taxmodeid").select2("readonly", true);
			}
		}
	});
	$('#chargesnamedataupdatesubbtn').show().removeClass('hidedisplayfwg');
	$('#chargesnamesavebutton').hide();
}
//update old information
function addchargedetailupdateformdata() {
	var formdata = $("#chargesnameaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	var compoundon= $('#cumulativetaxid').val();
    $.ajax({
        url: base_url + "Chargesname/datainformationupdate",
        data: "datas=" + datainformation+"&compoundon="+compoundon,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if(nmsg == 'TRUE') {
				$('#chargesnamedataupdatesubbtn').hide();
				$('#chargesnamesavebutton').show();
				$(".addsectionclose").trigger("click");
				$("#calculationtypeid,#taxmodeid").select2("readonly", false);
				resetFields();
				chargesnamerefreshgrid();
				$('#cumulativetaxiddivhid').hide(); //hide the tax if its only applicable
				alertpopup(savealert);
				// For Touch
				masterfortouch = 0;	
				//Keyboard short cut
				saveformview = 0;
            }
        },
    });	
}
//Record Delete
function addchargedetailrecorddelete(datarowid) {
	var additionalchargeelementstable = $('#chargesnameelementstable').val();
	var additionalchargeelementspartable = $('#chargesnameelementspartabname').val();
    $.ajax({
        url: base_url + "Chargesname/deleteinformationdata?additionalchargeprimarydataid="+datarowid+"&additionalchargeelementstable="+additionalchargeelementstable+"&additionalchargeelementspartable="+additionalchargeelementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	chargesnamerefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Deleted successfully');
            } 
        },
    });
}
//Charges name check
function chargesnamecheck() {
	var primaryid = $("#chargesnameprimarydataid").val();
	var accname = $("#additionalchargetypename").val();
	var elementpartable = $('#chargesnameelementspartabname').val();
	var nmsg = "";
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Charges name already exists!";
				}
			}else {
				return "Charges name already exists!";
			}
		} 
	}
}
{// Validate Percentage
	function validatepercentage(field) {
		var type =$('#calculationtypeid').val();
		var value=field.attr('id');
		var num=parseFloat($('#'+value+'').val());
		if(type == 3){
			if(num < 0 || num > 100)
			return "percent value 0%-100%";		
		}
	}
}
function chargecategorydatareload() {
	$('#additionalchargecategoryid').empty();
	$('#additionalchargecategoryid').append($("<option></option>"));
	$.ajax({
		url:base_url+'Chargesname/fetchdddataviewddval?dataname=additionalchargecategoryname&dataid=additionalchargecategoryid&datatab=additionalchargecategory',
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#additionalchargecategoryid')
					.append($("<option></option>")
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
				$.each(data, function(index) {
					if(data[index]['setdefault'] == 'Yes'){
						$('#additionalchargecategoryid').select2('val',data[index]['datasid']).trigger('change');
					}
				});
			}
		},
	});
}