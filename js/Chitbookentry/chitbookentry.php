<script>
$(document).ready(function() { 
	{//Foundation Initialization
		$(document).foundation();
	}
	chitcompletealert = $('#chitcompletealert').val(); //Alert message for completed chitbook - integrate company settings front end and backend
	chitbookentrygrid();
	prizeissuegrid();
	prizedescription('');
	chitentrygroupprintstatus = 0;
	chitentrysingleprintstatus = 0;
	variablemodeid = 0;
	averageamount = 0;
	averagemonthcheck = 0;
	defaultbankid = $('#defaultbankid').val();
	defaultcashinhandid = $('#defaultcashinhandid').val();
	chitauthrole = $('#chituserroleauth').val(); // Authorization role for chit
	chitauthrole = chitauthrole.split(",");
	<?php $userid = $this->Basefunctions->userid; ?>
	employeename = '<?php echo $this->Basefunctions->singlefieldfetch('employeename','employeeid','employee',$userid); ?>';
	employeeid = '<?php echo $userid; ?>';
	// chit book entry form validation
	$('#chitentrysubmit').click(function() {
		$('#stocktypeid,#paymentaccountid,#cardtypeid,#bankname,#referenceno,#referencedate,#approvalcode,#paymentamount').attr('data-validation-engine','');
		$("#chitbookentryaddwizard").validationEngine('validate');
	});
	jQuery("#chitbookentryaddwizard").validationEngine({
		onSuccess: function() {
			chitbookentryadd();
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
		}
	});
	{ // View by drop down change
        $('#dynamicdddataview').change(function(){
           chitbookentrygrid();
        });
    }
	// for date picker
	if(jQuery.inArray(loggedinuserrole,chitauthrole) != -1) {   //Userrole who has having permission to edit
		mindate = null;
		maxdate = 0;
		//$("#noofmonthsentry").prop('readonly',false);
	} else { //other role
		mindate =0;
		maxdate =0;
		//$("#noofmonthsentry").prop('readonly',true);
	}
	var dateformetdata = $('#chitbookentrydate').attr('data-dateformater');
	$('#chitbookentrydate').datetimepicker({
		dateFormat: dateformetdata,
		showTimepicker :false,
		minDate: mindate,
		changeMonth: true,
		changeYear: true,
		maxDate: maxdate,
		yearRange : '1947:c+100',
		onSelect: function () {
			var checkdate = $(this).val();
			if(checkdate != ''){
				$(this).next('span').remove('.btmerrmsg'); 
				$(this).removeClass('error');
			}
		},
		onClose: function () {
			$('#chitbookentrydate').focus();
		}
	});
	getcurrentsytemdate("chitbookentrydate");
	var dateformetdata = $('#chitreferencedate').attr('data-dateformater');
	$('#chitreferencedate').datetimepicker({
		dateFormat: dateformetdata,
		showTimepicker :false,
		minDate: mindate,
		changeMonth: true,
		changeYear: true,
		maxDate: null,
		yearRange : '1947:c+100',
		onSelect: function () {
			var checkdate = $(this).val();
			if(checkdate != ''){
				$(this).next('span').remove('.btmerrmsg'); 
				$(this).removeClass('error');
			}
		},
		onClose: function () {
			$('#chitreferencedate').focus();
		}
	});
	getcurrentsytemdate("chitreferencedate");
	$('#referencedate').datetimepicker({
		dateFormat: 'dd-mm-yy',
		showTimepicker :false,
		minDate: null,
		changeMonth: true,
		changeYear: true,		
		maxDate:'+1970/01/2',
			onSelect: function () {
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
					$(this).prev('div').remove();
				}
			},
			onClose: function () {
				$('#referencedate').focus();
			}
	}).click(function() {
		$(this).datetimepicker('show');
	});
	{// final main
		$("#chitbookno").change(function() {
			var chitbookno = $('#chitbookno').val();
			if(chitbookno != '') {
				getchitbookdetails(chitbookno);
			}
		});
	}
	{// other chitbook concept code dont remove
		/* //auto calculate ->chitbookamt/otherchitbookamount/monthentry
		$(".chitbookamountcalc").change(function(event){			
			setzero(['currentamount','chequeamount','cardamount','neftamount']);
			var fieldid = $(this).attr("id");
			var type = $('#'+fieldid+'').data('calc');
			if(type == 'chitbookno')
			{
				var chitbookno=$('#chitbookno').val();
				if(chitbookno != ''){
				  getchitbookdetails(chitbookno);
				}
				var  monthentry = $('#noofmonthsentry').val(); 
				var  otherchitbookno = ''; 
			} 
			else if(type == 'month')	
			{
				var monthentry=$('#'+fieldid+'').val();
				var  otherchitbookno = '';
				var  chitbookno = $('#chitbookno').val();
				
			}
			else if(type == 'otherchitbookno') 
			{
				var otherchitbookno=$('#otherchitbookno').val();;
				var  monthentry = $('#noofmonthsentry').val();
				var  chitbookno = $('#chitbookno').val();
				
			}
			if(chitbookno != ''){
			 getchitbookcurrentamount(otherchitbookno,chitbookno,monthentry);
			}
			setTimeout(function(){
					var currentamt=$('#currentamount').val();
					var chequeamt=$('#chequeamount').val();
					var cardamt=$('#cardamount').val();
					var cashamt=parseInt(currentamt)-(parseInt(chequeamt)+parseInt(cardamt));
					$('#cashamount').val(cashamt);
					$('#balanceamount').val(0);
					Materialize.updateTextFields();
			},25);
			
		}); */
		/* // chit rate details
	   $(".chitratecalc").change(function(event){			
			var fieldid = $(this).attr("id"); 
			var type = $('#'+fieldid+'').data('calc');
			if(type == 'chitbookno'){
				var chitbookno=$('#'+fieldid+'').val();
				var  otherchitbookno = ''; 
				var  chitbookentrydate = $('#chitbookentrydate').val();
              	if(chitbookno != '')	{		
				 getchitbookrate(otherchitbookno,chitbookno,chitbookentrydate);
				}
			} else if(type == 'otherchitbookno') {
				//var ='';
				var  chitbookno = $('#chitbookno').val();
				var  otherchitbookno = $('#otherchitbookno').val();
				//alert(otherchitbookno);
				var  chitbookentrydate = $('#chitbookentrydate').val();
				if(otherchitbookno != '' && chitbookno != ''){				
				  getchitbookrate(otherchitbookno,chitbookno,chitbookentrydate);
				}				
			}
			Materialize.updateTextFields();
		}); */
	}
	{ //stone setting Add
		$("#prizeissueaddicon").click(function(){
			sectionpanelheight('prizeissuesectionoverlay');
			$("#prizeissuesectionoverlay").removeClass("closed");
			$("#prizeissuesectionoverlay").addClass("effectbox");
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$("#prizeissuesectionclose").click(function(){
			$("#prizeissuesectionoverlay").removeClass("effectbox");
			$("#prizeissuesectionoverlay").addClass("closed");
		});
	}
	$( window ).resize(function() {
		sectionpanelheight('prizeissuesectionoverlay');
	});
	//Reload
	$("#reloadicon").click(function() {
		refreshgrid()
	});
	// chit book clearGridData
	$('#chitbookclearicon').click(function() {
		resetFields();
		setTimeout(function() {
			getcurrentsytemdate("chitbookentrydate");
			$('#noofmonthsentry').val(1);
			$('#employeename').val(employeename);
			$('#employeeid').val(employeeid);
			$('#printtemplate').select2('val','YES').trigger('change');
		},100);
	});
	// chit book add icon
	$('#addicon').click(function() {
		showhideiconsfun('addclick','chitbookentryformdiv');
        addslideup('chitbookentrygriddisplay', 'chitbookentryformdiv');
		$("#chitentrysubmit").show();
		$('.updatebtnclass,.addbtnclass').addClass('hidedisplay');
		$('#dataupdatesubbtn,#dataaddsbtn').hide();
		chitbookentryinnergrid();
		setTimeout(function(){
			$('#noofmonthsentry').val('1');
			$('#employeename').val(employeename);
			$('#employeeid').val(employeeid);
			$('#printtemplate').select2('val','YES').trigger('change');
			autogenmaterialupdate();
		},100);
		cleargriddata('chitbookentryinnergrid');
        getcurrentsytemdate("chitbookentrydate");
		$('#stocktypeid').select2('val',26).trigger('change');
		//For Keyboard Shortcut Variables
		viewgridview = 0;
		addformview = 1;
		$('#chitbookno').focus();
		/* if(loggedinuserrole == 2) {// admin role
			$('#noofmonthsentry').prop('readonly',false);
		} else {
			$('#noofmonthsentry').prop('readonly',true);
		} */
	});
	// chit book close icon
	var addcloseinfo = ["closeaddform","chitbookentrygriddisplay","chitbookentryformdiv"];
	addclose(addcloseinfo);
	// Delete chitbook entry
	$("#deleteicon").click(function() {
		var datarowid = $('#chitbookentrygrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			var monthentry = $('#chitbookentrygrid div.gridcontent div.active ul .monthentry-class').html();
			var chitbookno = $('#chitbookentrygrid div.gridcontent div.active ul .chitbookno-class').html();
			if(monthentry == 1){
				alertpopup('Unable to delete first month entry');
			} else {
				$.ajax({
					url: base_url + "Chitbookentry/chitentrycheck?chitbookno="+chitbookno+"&monthentry="+monthentry,
					dataType:'json',
					async:false,
					success: function(msg) {
						var nmsg =  $.trim(msg);
						if (nmsg == 'SUCCESS') {
							$("#basedeleteoverlay").fadeIn();
							$("#basedeleteyes").focus();
						} else {
							alertpopupdouble('Unable to delete this chit entry');
						}
					},
				});
			}
		} else {
			alertpopup("Please select a row");
		}	
	});
	//chitbook yes delete
	$("#basedeleteyes").click(function() {
		var datarowid = $('#chitbookentrygrid div.gridcontent div.active').attr('id'); 
		$.ajax({
			url: base_url + "Chitbookentry/chitbookentrydelete?primaryid="+datarowid,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				if (nmsg == "SUCCESS") {
					alertpopup('Deleted Successfully');
				} else {
					alertpopup('error');
				}
			},
		});
	});
	//Detailed view
	$("#detailedviewicon").click(function() {
		$("#chitentrysubmit").hide();
		resetFields();
		var datarowid = $('#chitbookentrygrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			//Function Call For Edit
			$('.updatebtnclass,.addbtnclass,.editformsummmarybtnclass').addClass('hidedisplay');
			$('#dataupdatesubbtn,#dataaddsbtn,#editfromsummayicon').hide();
			retrive(datarowid);
			showhideiconsfun('summryclick','chitbookentryformdiv');
			$('.otherchit').hide();
			$("#closeaddform").attr('disabled',false);
			Materialize.updateTextFields();		
		} else {
			alertpopup("Please select a row");
		}		
	});
	// Cancel chitbook entry
	$("#cancelicon").click(function() {
		var datarowid = $('#chitbookentrygrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			var monthentry = $('#chitbookentrygrid div.gridcontent div.active ul .monthentry-class').html();
			var chitbookno = $('#chitbookentrygrid div.gridcontent div.active ul .chitbookno-class').html();
			if(monthentry == 1) {
				alertpopup('Unable to cancel first month entry');
			} else {
				$.ajax({
					url: base_url + "Chitbookentry/chitentrycheck?chitbookno="+chitbookno+"&monthentry="+monthentry,
					dataType:'json',
					async:false,
					success: function(msg) {
						var nmsg =  $.trim(msg);
						if (nmsg == 'SUCCESS') {
							$('#basecanceloverlayforcommodule').fadeIn();
							$("#basecancelyes").focus();
						} else {
							alertpopupdouble('Unable to cancel this chit entry');
						}
					},
				});
			}
		} else {
			alertpopup("Please select a row");
		}	
	});
	// Chitbook Entry yes Cancel
	$("#basecancelyes").click(function() {
		var datarowid = $('#chitbookentrygrid div.gridcontent div.active').attr('id');
		$.ajax({
			url: base_url + "Chitbookentry/chitbookentrycancel?primaryid="+datarowid,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				refreshgrid();
				$('#basecanceloverlayforcommodule').fadeOut();
				if (nmsg == "SUCCESS") {
					alertpopup('Cancelled Successfully');
				} else  {
					alertpopup('Error on cancel! Please try again!');
				}
			},
		});
	});
	$("#basecancelno").click(function() {
		$('#basecanceloverlayforcommodule').fadeOut();
	});
	{//auto calculate ->balance amount
		$(".checkbalance").change(function(event) {
			setzero(['cashamount','chequeamount','cardamount','neftamount']);
			var chitlookupid = $("#chitlookupid").val();
			var fieldid = $(this).attr("id");
			var type = $('#'+fieldid+'').data('calc');
			var cashamount = $('#cashamount').val();
			var currentamount = $('#currentamount').val();
			var cardamount = $('#cardamount').val();
			var chequeamount = $('#chequeamount').val();
			var neftamount = $('#neftamount').val();
			var amount = parseInt(cashamount) + parseInt(cardamount) + parseInt(chequeamount)+ parseInt(neftamount);
			if(currentamount > 0) {
				if(variablemodeid == 0) {
					if(amount > currentamount) {
						$('#'+fieldid+'').val(0);
						alertpopup('Please enter the amount lessthan or equalto '+currentamount+'');
					} else {
						var balamt = parseInt(currentamount)-(parseInt(cashamount) + parseInt(cardamount) + parseInt(chequeamount)+ parseInt(neftamount)) ;
						$('#balanceamount').val(balamt);
					}
				} else if(variablemodeid == 1) {
					if(averagemonthcheck == 1) {
						var ccurrentamount = parseInt(cashamount) + parseInt(cardamount) + parseInt(chequeamount)+ parseInt(neftamount);
						if(ccurrentamount > averageamount) {
							$('#'+fieldid+'').val(0);
							alertpopup('Please enter the amount lessthan or equal to '+averageamount+'.');
						} else {
							$('#currentamount').val(ccurrentamount);
						}
					} else {
						var ccurrentamount = parseInt(cashamount) + parseInt(cardamount) + parseInt(chequeamount)+ parseInt(neftamount);
						$('#currentamount').val(ccurrentamount);
					}
				}
			}
			Materialize.updateTextFields();
		});
	}
	// date picker change reset filed
	{
		$('#chitbookentrydate').change(function() {
			clearform('changedate');
		});
	}
	// prize issue
	$("#gifticon").click(function() {
		addslideup('chitbookentrygriddisplay','prizeissuecreationform');
		$('.ftabprizeissuecreation').addClass('active');
		setTimeout(function() {
			getcurrentsytemdate("prizeissuedate");
			$('#printtemplateissue').select2('val','YES');
		},100);
		prizeissuegrid();
		$("#prizeissuegrid").trigger('reloadGrid');
		Materialize.updateTextFields();
		firstfieldfocus();
		//For Keyboard Shortcut Variables
		viewgridview = 2;
		addformview = 2;
	});
	$('#groupcloseaddform').click(function() {
		resetFields();
		addslideup('prizeissuecreationform','chitbookentrygriddisplay');
	});
	$('#prizeissuerefreshicon').click(function() {
		resetFields();
		prizeissuerefreshgrid();
		setTimeout(function() {
			getcurrentsytemdate("prizeissuedate");
			$('#printtemplateissue').select2('val','YES');
		},100);
		Materialize.updateTextFields();
	});
	//for date picker
	$('#prizeissuedate').datetimepicker({
		dateFormat: 'dd-mm-yy',
		showTimepicker :false,
		minDate:mindate,
		maxDate:maxdate,
		onSelect: function () {
			var checkdate = $(this).val();
			if(checkdate != ''){
				$(this).next('span').remove('.btmerrmsg'); 
				$(this).removeClass('error');
				$(this).prev('div').remove();
			}
		},
		onClose: function () {
			$('#prizeissuedate').focus();
		}
	}).click(function() {
		$(this).datetimepicker('show');
	});
	$('#issuechitbookno').change(function() {
		$('#typegift,#giftprize,#issuecomment').select2('val','');
		$('#giftprize').empty();
		$('#issueschemename,#chitbookamount,#issueschemeid').val('');
		var chitbookno=$(this).val();
		getschemedetails(chitbookno);
		Materialize.updateTextFields();
	});
	$('#typegift').change(function() {
		var gifttype=$(this).val();
		var schemeid=$('#issueschemeid').val();
		var amount=$('#chitbookamount').val();
		var chitbookno=$('#issuechitbookno').val();
		getprizedetails(schemeid,amount,gifttype,chitbookno);
		Materialize.updateTextFields();
	});
	$('#giftprize').change(function() {
		var prizeid=$(this).val(); 
		prizedescription(prizeid);
		var chitbookno=$('#issuechitbookno').val();
		checkprizeissue(prizeid,chitbookno);
		Materialize.updateTextFields();
	});
	// chit book entry form validation
	$('#prizeissuebtn').click(function() { 
		$("#prizeissuevalidate").validationEngine('validate');
	});
	jQuery("#prizeissuevalidate").validationEngine({
		onSuccess: function() { 
			prizeissuecreate();
		},
		onFailure: function() {
			var dropdownid =['2','typegift','giftprize'];
			dropdownfailureerror(dropdownid);
		}
	});
	//Delete prize issue
	$("#prizeissuedeleteicon").click(function() {
		var datarowid = $('#prizeissuegrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			combainedmoduledeletealert('prizeissuedeleteyes','Are you sure,you want to delete this prize issue data?');
			$("#prizeissuedeleteyes").click(function() {
				prizeissuedelete(datarowid);
				$(this).unbind();
			});
		} else {
			alertpopup(selectrowalert);
		}
	});
	// key event for chitbook entry
	$('#noofmonthsentry,#currentamount,#totalamount,#ratedetails,#cashamount,#chequeamount,#cardamount,#neftamount').keypress(function(e) {
	    if (e.which == 13) {
			$('#chitentrysubmit').trigger('click');
		}
	});
	$('#chitbookno').keypress(function(e) {
	    if (e.which == 13) {
			$('.fastsubmitchitentrybtn').focus();
		}
	});
	// print the chit entry group // Commented code - vishal instruction by kumaresan
	/* $('#chitentrygroupprinticon').click(function(){
		var datarowid = $('#chitbookentrygrid div.gridcontent div.active').attr('id'); 
		if(datarowid){
		    if(chitentrygroupprintstatus == 0){
			 chitentrygroupprint();
			}
			//chitentrygroupprintstatus = 1;
		}else {
			alertpopup(selectrowalert);
		}
	}); */
	// print the chit entry
	$('#chitentryprinticon').click(function() {
		var datarowid = $('#chitbookentrygrid div.gridcontent div.active').attr('id'); 
		if(datarowid) {
			chitentryprint();
		} else {
			alertpopup(selectrowalert);
		}
	});
	// Tag Template overlay and print
	$('#printpreviewicon').click(function() {
		//Tagtemplate function from Base JS
		tagtemplatesload(51);
		var datarowid = $('#chitbookentrygrid div.gridcontent div.active').attr('id'); 
		if(datarowid) {
			$("#tagtemplateoverlay").fadeIn();
		} else {
			alertpopup(selectrowalert);
		}
	});
	// reprint the prize issue
	$('#issueprinticon').click(function() {
		var datarowid = $('#prizeissuegrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			issuereprint();
		} else {
			alertpopup(selectrowalert);
		}
	});
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,chitbookentrygrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			chitbookentrygrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
	//TagTemplate Overlay Submit
	$("#tagtempprintinprinter").click(function() {
		var datarowid = $('#chitbookentrygrid div.gridcontent div.active').attr('id');
		var chitentryid=getgridcolvalue('chitbookentrygrid',datarowid,'chitentryid','');
		var templateid = $("#tagtemplateprview").find('option:selected').val();
		if(datarowid){
			$.ajax({
				url: base_url + "Chitbookentry/chitentryreprint?primaryid="+datarowid+"&templateid="+templateid,
				success: function(msg) {
					var nmsg =  $.trim(msg);
					if (nmsg == 'SUCCESS') {
						setTimeout(function() {
							$('#processoverlay').hide();
						},25);
						alertpopupdouble('Print Successfully');
					} else {
						setTimeout(function() {
							$('#processoverlay').hide();
						},25);
						alertpopupdouble(submiterror);
					}
				},
			});
		} else {
			alertpopup(selectrowalert);
		}
	});
	//TagTemplate Overlay Cancel
	$("#tagtempprintcancel").click(function() {
		$("#tagtemplateoverlay").fadeOut();
	});
	// Payment Stock Type trigger Change
	$("#stocktypeid").change(function() {
		var value = $(this).val();
		$('#cardtypeid-div,#bankname-div,#referenceno-div,#referencedate-div,#approvalcode-div').addClass('hidedisplay');
		$('#bankname,#referenceno').attr('data-validation-engine','');
		$('#accountid,#cardtypeid').select2('val',1).trigger('change');
		$("#paymentaccountid option").addClass("ddhidedisplay").attr('disabled', true);
		getcurrentsytemdate("referencedate");
		if(value == 26) { // Cash Payment
			$("#referenceno").attr('maxlength','');
			$("label[for='referenceno']").html("Number");
			$("#paymentaccountid option[data-accounttypeid=19]").removeClass("ddhidedisplay").attr('disabled', false);
			$("#paymentaccountid").select2('val',defaultcashinhandid).trigger('change');
		} else if(value == 27) { // Cheque Payment
			$('#bankname-div,#referenceno-div,#referencedate-div').removeClass('hidedisplay');
			$('#bankname').attr('data-validation-engine','validate[required]');
			$("#referenceno").attr('maxlength','6');
			$("label[for='referenceno']").html("Cheque No");
			$('#referenceno-div label').append('<span class="mandatoryfildclass">*</span>');
			$('#referenceno').attr('data-validation-engine','validate[required,custom[number]]');
			$("#paymentaccountid option[data-accounttypeid=18]").removeClass("ddhidedisplay").attr('disabled', false);
			$("#paymentaccountid").select2('val',defaultbankid).trigger('change');
		} else if(value == 28) { // D.D (Demand Draft) Payment
			$('#bankname-div,#referenceno-div,#referencedate-div').removeClass('hidedisplay');
			$('#bankname').attr('data-validation-engine','validate[required]');
			$("#referenceno").attr('maxlength','50');
			$("label[for='referenceno']").html("DD No");
			$('#referenceno-div label').append('<span class="mandatoryfildclass">*</span>');
			$('#referenceno').attr('data-validation-engine','validate[required,custom[number]]');
			$("#paymentaccountid option[data-accounttypeid=18]").removeClass("ddhidedisplay").attr('disabled', false);
			$("#paymentaccountid").select2('val',defaultbankid).trigger('change');
		} else if(value == 29) { // Card Payment
			$('#cardtypeid-div,#bankname-div,#referenceno-div,#approvalcode-div').removeClass('hidedisplay');
			$("#referenceno").attr('maxlength','50');
			$("label[for='referenceno']").html("Card No");
			$("#paymentaccountid option[data-accounttypeid=18]").removeClass("ddhidedisplay").attr('disabled', false);
			$("#paymentaccountid").select2('val',defaultbankid).trigger('change');
			$("#cardtypeid option").addClass("ddhidedisplay").attr('disabled', true);
			$('#cardtypeid').select2('val','');
			$("#cardtypeid option[data-cardgroupid=2]").removeClass("ddhidedisplay").attr('disabled', false);
		} else if(value == 79) { // Online Payment
			$('#cardtypeid-div,#bankname-div,#referenceno-div').removeClass('hidedisplay');
			$("#referenceno").attr('maxlength','50');
			$("label[for='referenceno']").html("Ref No");
			$("#paymentaccountid option[data-accounttypeid=18]").removeClass("ddhidedisplay").attr('disabled', false);
			$("#paymentaccountid").select2('val',defaultbankid).trigger('change');
			$("#cardtypeid option").addClass("ddhidedisplay").attr('disabled', true);
			$('#cardtypeid').select2('val','');
			$("#cardtypeid option[data-cardgroupid=3]").removeClass("ddhidedisplay").attr('disabled', false);
		}
	});
	{//Rule criteria cond create
		$('#chitentrydetailsubmit').click(function() {
			var paymentaccountid = $("#paymentaccountid").find('option:selected').val();
			var paymentstocktypeid = $("#stocktypeid").find('option:selected').val();
			if((paymentaccountid != '' || paymentaccountid != 1) && (paymentstocktypeid != '' || paymentstocktypeid != 1)) {
				var stocktypename = $("#stocktypeid").find('option:selected').data('stocktypenamehidden');
				var accountname = $("#paymentaccountid").find('option:selected').data('paymentaccountnamehidden');
				var cardtypename = $("#cardtypeid").find('option:selected').data('cardtypehidden');
				cardtypename = typeof cardtypename == 'undefined' ? '' : cardtypename;
				$('#stocktypename').val(stocktypename);
				$('#accountname').val(accountname);
				$('#cardtypename').val(cardtypename);
				$('#chitentryinnergridform').validationEngine('validate');
			} else {
				alertpopup('Please fill the corresponding fields');
			}
		});
		jQuery("#chitentryinnergridform").validationEngine({
			onSuccess: function() {
				var paymentstocktypeid = $("#stocktypeid").find('option:selected').val();
				if(paymentstocktypeid == 26 || paymentstocktypeid == 29 || paymentstocktypeid == 79) {
					$('#referencedate').val('0000-00-00');
				}
				formtogriddata('chitbookentryinnergrid','ADD','last','');
				clearform('chitentryinnergridform');
				$('#stocktypeid').select2('val',paymentstocktypeid).trigger('change');
				$('#paymentamount').val(0);
				$('#bankname,#referenceno,#approvalcode').val('');
				getcurrentsytemdate("referencedate");
				/* Data row select event */
				datarowselectevt();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
			}
		});
		//rule criteria condition delete
		$('#splitdetaildelete').click(function() {
			var ruleconrowid = $('#splittaginnergrid div.gridcontent div.active').attr('id');
 			if (ruleconrowid) {
				deletegriddatarow('splittaginnergrid',ruleconrowid);
			} else {
				alertpopup("Please select a row");
			}
			var count = $('#splittaginnergrid .gridcontent div.data-content div').length;
			if(count == 0) {
				$("#itemtagnumber").attr('readonly',false);
			}
		});
		$('#paymentamount').change(function() {
			var newamount = $(this).val();
			var prevamount = getgridcolvalue('chitbookentryinnergrid','','paymentamount','sum');
			var totalnetamount = (parseFloat(newamount)+parseFloat(prevamount)).toFixed(2);
			var chitbookbillamount = parseFloat($('#chitbookbillamount').val()).toFixed(2);
			if(parseFloat(totalnetamount) > parseFloat(chitbookbillamount)) {
				alertpopup('Entered amount is greater than scheme amount');
				$(this).val(0);
			}
		});
	}
});
{// View create success function
    function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
function chitbookentrygrid(page,rowcount) {
	var defrecview = $('#mainviewdefaultview').val();
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
	var wwidth = $("#chitbookentrygrid").width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol) {
		sortcol = sortcol;
	} else {
		var sortcol = $('.chitentryheadercolsort').hasClass('datasort') ? $('.chitentryheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.chitentryheadercolsort').hasClass('datasort') ? $('.chitentryheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.chitentryheadercolsort').hasClass('datasort') ? $('.chitentryheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = 51;
	var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
	var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
	var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=chitentrygroup&primaryid=chitentrygroupid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#chitbookentrygrid').empty();
			$('#chitbookentrygrid').append(data.content);
			$('#chitbookentrygridfooter').empty();
			$('#chitbookentrygridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('chitbookentrygrid');
			{//sorting
				$('.chitentryheadercolsort').click(function() {
					$("#processoverlay").show();
					$('.chitentryheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.chitentryheadercolsort').hasClass('datasort') ? $('.chitentryheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.chitentryheadercolsort').hasClass('datasort') ? $('.chitentryheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					var sortpos = $('#chitbookentrygrid .gridcontent').scrollLeft();
					chitbookentrygrid(page,rowcount);
					$('#chitbookentrygrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('chitentryheadercolsort',headcolid,sortord);
			}
			//onclick 
			$(".gridcontent").click(function() {
				var datarowid = $('#chitbookentrygrid div.gridcontent div.active').attr('id');
				if(datarowid) {
					if(minifitertype == 'dashboard') {
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			{//pagination
				$('.pvpagnumclass').click(function() {
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					chitbookentrygrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#chitentrypgrowcount').change(function() {
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					chitbookentrygrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			//Material select
			$('#chitentrypgrowcount').material_select();
		},
	});
}
// View condition Grid
function chitbookentryinnergrid(page,rowcount) {
	var wwidth = $("#chitbookentryinnergrid").width();
	var wheight = $("#chitbookentryinnergrid").height();
	$.ajax({
		url:base_url+"Chitbookentry/chitbookentryinnergridheaderfetch?moduleid=51&width="+wwidth+"&height="+wheight+"&modulename=chitbookentry innergrid",
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#chitbookentryinnergrid").empty();
			$("#chitbookentryinnergrid").append(data.content);
			$("#chitbookentryinnergridfooter").empty();
			$("#chitbookentryinnergridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('chitbookentryinnergrid');
		},
	});
}
// chitbook entry create
function chitbookentryadd() {
    /* if($('#ratedetails').val() != '') {
	    var res = $('#ratedetails').val().split('-'); 
	    if(res[2] == 0)
	    {
	    	alertpopup('Please add rate to purity for this scheme');
	    	return false;
	    } 
    } */
	var balanceamount = $('#balanceamount').val();
	var currentamount = $('#currentamount').val();
	var checktotalamount = getgridcolvalue('chitbookentryinnergrid','','paymentamount','sum');		
	if(currentamount != checktotalamount) {
		alertpopup('Please make payment without balance amount');
	    return false;
	}
	var criteriacnt = $('#chitbookentryinnergrid .gridcontent div.data-content div').length;
	var griddata = getgridrowsdata('chitbookentryinnergrid');
	var criteriagriddata = JSON.stringify(griddata);
	var cashamount = parseFloat(getgridcolvaluewithcontition('chitbookentryinnergrid','','paymentamount','sum','stocktypeid','26'));
	var cardamount = parseFloat(getgridcolvaluewithcontition('chitbookentryinnergrid','','paymentamount','sum','stocktypeid','29'));
	var chequeamount = parseFloat(getgridcolvaluewithcontition('chitbookentryinnergrid','','paymentamount','sum','stocktypeid','27'));
	var ddamount = parseFloat(getgridcolvaluewithcontition('chitbookentryinnergrid','','paymentamount','sum','stocktypeid','28'));
	var onlineamount = parseFloat(getgridcolvaluewithcontition('chitbookentryinnergrid','','paymentamount','sum','stocktypeid','79'));
	var prevnoofmonthentry = $('#noofmonthsentry').val();		
	var formdata = $("#chitbookentryform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	$('#processoverlay').show();
	$.ajax({
		url: base_url +"Chitbookentry/chitbookentrycreate",
		data: "datas=" + datainformation+"&cashamount="+cashamount+"&cardamount="+cardamount+"&chequeamount="+chequeamount+"&ddamount="+ddamount+"&onlineamount="+onlineamount+"&criteriagriddata="+criteriagriddata+"&criteriacnt="+criteriacnt,
		type: "POST",
		async:true,
		success: function(msg) { 
			if(msg == 'SUCCESS') {
				resetFields();
				setTimeout(function() {
					$('#processoverlay').hide();
					$('#chitbookno').focus();
					getcurrentsytemdate("chitbookentrydate");
					getcurrentsytemdate("chitreferencedate");
					$('#noofmonthsentry').val(prevnoofmonthentry);
					$('#noofmonthsentry').removeClass('error');
					$('#employeename').val(employeename);
					$('#employeeid').val(employeeid);
					$('#printtemplate').select2('val','YES').trigger('change');
					$('#stocktypeid').select2('val','26').trigger('change');
				},100);
				cleargriddata('chitbookentryinnergrid');
				averageamount = 0;
				averagemonthcheck = 0;
				$('#chitbookno').removeClass('error');
				$('#chitbooknoformError').hide();
			} else {
				$('#processoverlay').hide();
				alertpopup('error');
			}
		},
	});
}
// get chitbook current amount based on month entry
function getchitbookcurrentamount(otherchitbookno,val,monthentry) {
	chitbook_no = val;
	otherchitbook_id=otherchitbookno;
	// alert(otherchitbook_id);
	if(otherchitbook_id == ''){
		otherchitbook_id = 0;
	}
	month_entry = monthentry;
	$.ajax({
		url: base_url+"Chitbookentry/getcurrentamount",
		data:{chitbookno:chitbook_no,otherchitbookid:otherchitbook_id,monthentry:month_entry},
		dataType:'json',
		async:false,
		success: function(data) {
			if(data.closed == 1) {
				alertpopup('Chit entry Completed');
			} else {
				$('#bothschemeamount').val(data.currentamount);	
				$('#currentamount').val(data.currentamount);
				if(otherchitbook_id == '') {
					$('#currentmonth').val(data.currentmonth);
					$('#currentmonthdiv').removeClass("hidedisplay");
				} else {
					$('#currentmonthdiv').addClass("hidedisplay");
				}
				$('#currentmonth').val(data.currentmonth);
				$('#lastpaymentdate').val(data.lastpaymentdate);
				$('#totalamount').val(data.totalamount);
			}
		},
	});
}
// get rate details
function getchitbookrate(otherchitbookno,val,chitbookentrydate) {
	chitbook_no=val;
	otherchitbook_id=otherchitbookno;
	if(otherchitbook_id == '') {
		otherchitbook_id = 0;
	}
	$.ajax({
		url: base_url+"Chitbookentry/getratedetails",
		data:{chitbookno:chitbook_no,otherchitbookid:otherchitbook_id,chitbookentrydate:chitbookentrydate},
		dataType:'json',
		async:false,
		success: function(data) {
			$('#ratedetails').val(data);
		},
	});
}
// get chitbook details
/* function getchitbookdetails(chitbookno) {
	$('#otherchitbookno').empty();	
	$('#otherchitbookno').append($("<option></option>"));
	var datarowid=chitbookno;
	$.ajax({
		url: base_url + "Chitbookentry/getchitbookdetails?primaryid="+datarowid,
		dataType:'json',
		async:false,
		type: "POST",
		success: function(msg) {				
			var chitbookdetails = new Array();
			var str_array=msg['chitbooknoarray'];
			chitbookdetails.push(msg['chitbookdata']);
			if(msg['chitbooknamearray'] != '') {
			   chitbookdetails.push(msg['chitbooknamearray']);
			}
			$('#chitbookname').val(chitbookdetails);
			$('#chitlookupid').val(msg['chitlookupid']);
			if(msg == 'months completed') {
				alertpopup('No of Months Entry Completed');
				$('#chitbookno').val('');	
			} else if(msg == 'invalid') {
				alertpopup('Given chitbook number is invalid');
				$('#chitbookno').val('');	
			} else if(msg == 'cancel')   {
				alertpopup(''+chitbookno+' is cancelled');
				$('#chitbookno').val('');	
			}else if(msg == 'already')   {
				alertpopup(''+chitbookno+' is already make paid this month');
				$('#chitbookno').val('');	
			} 
			else {
				$('.otherchit').removeClass('hidedisplay');
				for(var i = 0; i < str_array.length; i++) {
					var str_array_split = str_array[i].split(',');   
					$('#otherchitbookno')
					.append($("<option></option>")
					.attr('data-chitbookid',str_array_split[1])
					.attr("value",str_array_split[0])
					.text(str_array_split[0]));
				}
				$('#accountname').val(msg['accountname']);
				$('#accountid').val(msg['accountid']);
				$('#schemeprefix').val(msg['schemeprefix']);
			}
		},
	});
} */
// get chitbook details
function getchitbookdetails(chitbookno) {
	$('#chitbookname,#chitlookupid,#accountid,#schemeprefix,#bothschemeamount,#currentamount,#lastpaymentdate,#currentmonth,#totalamount,#currentamount,#ratedetails').val('');
	$('#neftamount,#chequeamount,#cardamount').val('0');
	var  chitbookentrydate = $('#chitbookentrydate').val();
	$.ajax({
		url: base_url + "Chitbookentry/getchitbookdetails?primaryid="+chitbookno+"&chitbookentrydate="+chitbookentrydate+"&chitrestrictsamemonth="+$('#chitrestrictsamemonth').val()+"&chituserroleauth="+$('#chituserroleauth').val(),
		dataType:'json',
		async:false,
		type: "POST",
		success: function(msg) {
			if(msg['currentmonth'] == 'completed') {
				alertpopup('All Months Entry Completed.Last Payment Date :'+msg['lastpaymentdate']+' Total Paid : '+msg['totalamount']+'');
				$('#chitbookno').val('');	
			}else if(msg['currentmonth'] == 'dateless') {
				alertpopup('Current Date is less than the Last Payment Date :'+msg['lastpaymentdate']+' Total Paid : '+msg['totalamount']+'');
				$('#chitbookno').val('');	
			}else if(msg == 'months completed') {
				alertpopup('No of Months Entry Completed');
				$('#chitbookno').val('');	
			} else if(msg == 'invalid') {
				alertpopup('Given chitbook number is invalid');
				$('#chitbookno').val('');	
			} else if(msg == 'cancel') {
				alertpopup(''+chitbookno+' is cancelled');
				$('#chitbookno').val('');	
			} else if(msg == 'already') {
				alertpopup(''+chitbookno+' is already make paid this month');
				$('#chitbookno').val('');
			} else if(msg['activationdatedetails'] == 'NO') {
				alertpopup('You can make payment of '+chitbookno+' on or after '+msg['activationdate']+'.');
				$('#chitbookno').val('');
			} else {
				$('#chitbookname').val(msg['chitbookname']);
				$('#chitlookupid').val(msg['chitlookupid']);
				$('#accountid').val(msg['accountid']);
				$('#schemeprefix').val(msg['schemeprefix']);
				$('#bothschemeamount').val(msg.currentamount);	
				$('#currentamount,#chitbookbillamount,#paymentamount').val(msg.currentamount);
				$('#currentmonth').val(msg.currentmonth);
				$('#lastpaymentdate').val(msg.lastpaymentdate);
				$('#totalamount').val(msg.totalamount);
				$('#ratedetails').val(msg['chitratedetails']);
				variablemodeid = msg.variablemodeid;
				if(variablemodeid == 1 && (msg.averagemonths < msg.currentmonth)) {
					averageamount = parseInt(msg.totalavgamount);
					averagemonthcheck = 1;
				}
				$('#balanceamount').val(0);
				$('#prizehistorydetails').val(msg['prizehistorydetails']);
				$('#chitentrysubmit').focus(); 
				if(msg['pendingmonths'] == 0 && chitcompletealert == 1) {
					alertpopup(''+chitbookno+' completed all months payments');
				}
				Materialize.updateTextFields();
			}
		},
	});
}
function retrive(datarowid) {
	resetFields();
	addslideup('chitbookentrygriddisplay', 'chitbookentryformdiv');
	var elementsname=['chitbookentrydate','employeename','accountname','noofmonthsentry','currentamount','printtemplate','cashamount','cardamount','chequeamount','chequedetail','neftamount','neftdetail','carddetail','comments'];
	$('#otherchitbookno').empty();	
    $('#otherchitbookno').append($("<option></option>"));
	$.ajax({
		url:base_url+"Chitbookentry/chitbookentryretrive?primaryid="+datarowid, 
		dataType:'json',
		async:false,
		success: function(data) {
			var txtboxname = elementsname;
			var dropdowns = ['1','printtemplate'];
			textboxsetvaluenew(txtboxname,txtboxname,data,dropdowns);
			$('.otherchit').removeClass('hidedisplay');
			var otherchitbookno=data.chitbookno;
			var str_array = otherchitbookno.split(','); 
			var newddappend="";
			var newlength=str_array.length;
			for(var m=0;m < newlength;m++) { 
			    if(m == 0){
				    $('#chitbookno').val(str_array[m]);
					getchitbookdetails(str_array[m]);
				}else{
					newddappend += "<option value = '" +str_array[m]+ "'>"+str_array[m]+"</option>";
					//singlechit = 1;
				}
			}
			//after run
			//$('#otherchitbookno').append(newddappend);
			$('#otherchitbookno').select2('val',str_array);
			var singlechitbook = $('#chitbookno').val();
			var otherchitbook = $('#otherchitbookno').val();
				getchitbookcurrentamount(otherchitbook,singlechitbook,1);
				getchitbookrate(otherchitbook,singlechitbook,data.chitbookentrydate);
		}
	});
}
function getschemedetails(chitbookno) {
	$.ajax({
		url:base_url+"Chitbookentry/getschemedetails?primaryid="+chitbookno, 
		dataType:'json',
		async:false,
		success: function(data) {
			if(data == 'NO') {
				alertpopup('Please enter proper chitbookno');
			} else {
				$('#issueschemeid').val(data['schemeid']);
				$('#issueschemename').val(data['schemename']);
				$('#chitbookamount').val(data['amount']);
			}
		}
	});
}
function getprizedetails(schemeid,amount,gifttype,chitbookno) {
	$('#giftprize').empty();	
    $('#giftprize').append($("<option></option>"));
	$.ajax({
        url: base_url + "Chitbookentry/getprizedetails",
        data:{schemeid:schemeid,amount:amount,gifttype:gifttype,chitbookno:chitbookno},
		async:false,
		dataType:'json',
		success: function(msg) {
			if(msg == 'NO') {
				alertpopup('Not available gift or prize for this scheme amount');
			} else {
				for(var i = 0; i < msg.length; i++) {
					$('#giftprize')
					.append($("<option></option>")
					.attr('data-issuetype',msg[i]['type'])
					.attr('data-prizeamount',msg[i]['prizeamount'])
					.attr("value",msg[i]['chitgiftid'])
					.text(msg[i]['prizename']+'-'+msg[i]['type']));
				}
			}
		},
    });
}
function checkprizeissue(prizeid,chitbookno) {
	$.ajax({
        url: base_url + "Chitbookentry/checkprizeissue",
        data:{prizeid:prizeid,chitbookno:chitbookno},
		async:false,
		dataType:'json',
		success: function(msg) { 
			if(msg == 'YES') {
				var giftname = $('#giftprize').text();
				alertpopup('Already '+giftname+'');
				$('#giftprize').select2('val','');
				} else if(msg == 'NO'){
				
			}
		},
    });
}
// prizedescription automatic tokenziation
function  prizedescription(prizeid) {
	prizedesc = [];
	$.ajax({
		url: base_url + "Chitbookentry/prizedescription",
       	async:false,
		data:{prizeid:prizeid},
		cache:false,
		dataType:'json',
		success: function(data)  { 
			if(data == 'NO') {
				prizedesc='';
			} else {
				var mulval = data.split(',');
				prizedesc=mulval;
			}
		},
	});
	//prize description automatic tokenzation     
	$("#issuecomment").select2({
		tags: prizedesc,
		tokenSeparators: [',', ' '],
		tokenizer: function(input, selection, callback) {
			// no comma no need to tokenize
			if (input.indexOf(',') < 0 && input.indexOf(' ') < 0)
				return;
			var parts = input.split(/,| /);
			for (var i = 0; i < parts.length; i++) {
				var part = parts[i];
				part = part.trim();
				callback({id:part,text:part});
			}
		}
	});
}
// prize issue create
function prizeissuecreate() {
	var prizeamount=$('#giftprize').find('option:selected').data('prizeamount'); 
	var formdata = $("#prizeissueform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	setTimeout(function() {
		$('#processoverlay').show();
	},25);
	$.ajax({
        url: base_url +"Chitbookentry/prizeissuecreate",
        data: "datas=" + datainformation+"&prizeamount="+prizeamount,
		type: "POST",
        success: function(msg) {  
        	prizeissuerefreshgrid();
		    resetFields();
			setTimeout(function() {
				getcurrentsytemdate("prizeissuedate");
				$('#printtemplateissue').select2('val','YES');
				$('#issuechitbookno').focus();
			},100);
			if(msg == 'SUCCESS') {
				setTimeout(function() {
					$('#processoverlay').hide();
				},25);
				alertpopup(savealert);
			} else {
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopup(submiterror);
			}
		},
    });
}
{ // Prize entry View Grid
	function prizeissuegrid(page,rowcount) {
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 30 : rowcount;
		var wwidth = $('#prizeissuegrid').width();
		var wheight = $('#prizeissuegrid').height();
		/*col sort*/
		var sortcol = $("#prizeissuesortcolumn").val();
		var sortord = $("#prizeissuesortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.prizheadercolsort').hasClass('datasort') ? $('.prizheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.prizheadercolsort').hasClass('datasort') ? $('.prizheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.prizheadercolsort').hasClass('datasort') ? $('.prizheadercolsort.datasort').attr('id') : '0';
		var filterid = '';
		var userviewid =128;
		var viewfieldids =51;
		var footername = 'prizeissue';
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=prizeissue&primaryid=prizeissueid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&footername='+footername,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#prizeissuegrid').empty();
				$('#prizeissuegrid').append(data.content);
				$('#prizeissuegridfooter').empty();
				$('#prizeissuegridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('prizeissuegrid');
				{//sorting
					$('.prizheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.prizheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						var sortcol = $('.prizheadercolsort').hasClass('datasort') ? $('.prizheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.prizheadercolsort').hasClass('datasort') ? $('.prizheadercolsort.datasort').data('sortorder') : '';
						$("#prizeissuesortorder").val(sortord);
						$("#prizeissuesortcolumn").val(sortcol);
						var sortpos = $('#prizeissuegrid .gridcontent').scrollLeft();
						prizeissuegrid(page,rowcount);
						$('#prizeissuegrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('prizheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						prizeissuegrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#prizepgrowcount').change(function(){
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('#prev').data('pagenum');
						prizeissuegrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//Material select
				$('#prizepgrowcount').material_select();
			},
		});
	}
	{//refresh grid
		function prizeissuerefreshgrid() {
			var page = $('ul#chitbookentrypgnum li.active').data('pagenum');
			var rowcount = $('ul#chitbookentrypgnumcnt li .page-text .active').data('rowcount');
			prizeissuegrid(page,rowcount);
		}
	}
}
//delete prize entry information
function prizeissuedelete(datarowid) {	
	setTimeout(function() {
		$('#processoverlay').show();
	},25);
    $.ajax({
       	url: base_url + "Chitbookentry/prizeissuedelete?primaryid="+datarowid,
        data:{primaryid:datarowid},
		async:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			prizeissuerefreshgrid();
			$("#basedeleteoverlayforcommodule").fadeOut();
		    if (nmsg == 'SUCCESS') {
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopupdouble('Deleted Successfully');
            } else {
				setTimeout(function() {
					$('#processoverlay').hide();
				},25);
				alertpopupdouble('error');
            }
        },
    });
}
function chitentrygroupprint() {
	var datarowid = $('#chitbookentrygrid div.gridcontent div.active').attr('id');
	setTimeout(function() {
		$('#processoverlay').show();
	},25);
	$.ajax({
       	url: base_url + "Chitbookentry/chitentrygroupprint?primaryid="+datarowid,
        dataType:'json',
		async:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'SUCCESS') {
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopupdouble('Print Successfully');
            } else {
				setTimeout(function() {
					$('#processoverlay').hide();
				},25);
				alertpopupdouble(submiterror);
            }
        },
    });
}
function chitentryprint() {
	var datarowid = $('#chitbookentrygrid div.gridcontent div.active').attr('id');
	var chitentryid = getgridcolvalue('chitbookentrygrid',datarowid,'chitentryid','');
	setTimeout(function(){
		$('#processoverlay').show();
	},25);
	$.ajax({
       	url: base_url + "Chitbookentry/chitentryprint?primaryid="+datarowid+"&chitentryid="+chitentryid,
        dataType:'json',
		async:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'SUCCESS') {
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopupdouble('Print Successfully');
            } else {
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopupdouble(submiterror);
            }
        },
    });
}
function issuereprint() {
	setTimeout(function(){
		$('#processoverlay').show();
	},25);
	$.ajax({
       	url: base_url + "chitbookentry/chitbookentry/issuereprint",
        async:false,
		dataType:'json',
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'SUCCESS') {
				setTimeout(function() {
					$('#processoverlay').hide();
				},25);
				alertpopupdouble('Print Successfully');
            } else {
				setTimeout(function() {
					$('#processoverlay').hide();
				},25);
				alertpopupdouble(submiterror);
            }
        },
    });
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#chitbookentrypgnum li.active').data('pagenum');
		var rowcount = $('ul#chitbookentrypgnumcnt li .page-text .active').data('rowcount');
		chitbookentrygrid(page,rowcount);
	}
}
function autogenmaterialupdate() {
	if($('#noofmonthsentry').val().length > 0){
		$('#noofmonthsentry').siblings('label, i').addClass('active');
	} else {
	}
}
function tagtemplatesload(viewfieldids) {
	$('#tagtemplateprview,#printtemplatepreview').empty();
	$('#tagtemplateprview,#printtemplatepreview').append($("<option value=''></option>"));
	$.ajax({
		url:base_url+'Base/tagtemplatesloadddval?dataname=tagtemplatename&dataid=tagtemplateid&datatab=tagtemplate&viewfieldids='+viewfieldids+"&column=printername",
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#tagtemplateprview,#printtemplatepreview')
					.append($("<option></option>")
					.attr("data-foldernameidhidden",data[index]['dataname'])
					.attr("data-printername",data[index]['printername'])
					.attr("data-moduleid",viewfieldids)
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}
			$('#tagtemplateprview,#printtemplatepreview').trigger('change');
		},
	});
}
</script>